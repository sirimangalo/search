1
00:00:00,000 --> 00:00:09,120
okay good evening everyone welcome to our evening

2
00:00:09,120 --> 00:00:27,380
thank you

3
00:00:27,380 --> 00:00:56,580
It's more memorable versus from the Pali, it means the inner tangle and the outer tangle.

4
00:00:56,580 --> 00:01:08,980
This generation, the world, it's entangled with a tangle.

5
00:01:08,980 --> 00:01:24,180
And to this we ask Gautama, who will be able to untangle the tangle?

6
00:01:24,180 --> 00:01:30,580
And I'm thinking of tonight as the inner tangle and the outer tangle.

7
00:01:30,580 --> 00:01:42,180
And how we don't realize how connected they are.

8
00:01:42,180 --> 00:01:49,180
It's common to think of meditation as

9
00:01:49,180 --> 00:01:58,580
a hedonistic or escapist activity.

10
00:01:58,580 --> 00:02:06,380
We don't make the connection between the inner world and the outer world.

11
00:02:06,380 --> 00:02:11,780
Because we don't make the connection with the inner world and the outer world.

12
00:02:11,780 --> 00:02:15,780
We don't realize that how we treat ourselves

13
00:02:15,780 --> 00:02:26,980
or similar to how we treat others, how we treat others, affects our own peace of mind.

14
00:02:26,980 --> 00:02:29,380
We're not conscious of how we're treating ourselves.

15
00:02:29,380 --> 00:02:32,580
We're not conscious of how we treat others.

16
00:02:32,580 --> 00:02:34,980
We get entangled.

17
00:02:34,980 --> 00:02:44,380
We get entangled because we don't see clearly the tangle.

18
00:02:44,380 --> 00:02:48,780
We don't even look at it in those terms.

19
00:02:48,780 --> 00:02:52,780
We don't see ourselves getting caught up in anything.

20
00:02:52,780 --> 00:02:59,980
We fight.

21
00:02:59,980 --> 00:03:04,380
We manipulate others. We harm others.

22
00:03:04,380 --> 00:03:09,380
We compete with others.

23
00:03:09,380 --> 00:03:23,380
We see this in the outer tangle. We get caught up in competition.

24
00:03:23,380 --> 00:03:26,380
We see this nowadays. Everyone thinks they're right.

25
00:03:26,380 --> 00:03:29,780
There's so much conflict in the world.

26
00:03:29,780 --> 00:03:36,580
Quite interesting to see how people find myself caught in the middle of groups.

27
00:03:36,580 --> 00:03:41,780
Seemingly like-minded people on opposite sides of the debate.

28
00:03:41,780 --> 00:03:44,180
Or like-minded are not like-minded people.

29
00:03:44,180 --> 00:03:49,380
They are people. Whether they're like-minded or not.

30
00:03:49,380 --> 00:03:53,380
They're people on both sides of the debate.

31
00:03:53,380 --> 00:04:00,980
It's so easy to pick teams, right?

32
00:04:00,980 --> 00:04:04,380
Everyone thinks they're right.

33
00:04:04,380 --> 00:04:06,980
The problem is that somebody has to be wrong.

34
00:04:06,980 --> 00:04:10,980
When you have two groups with opposing views, one of them has to be wrong.

35
00:04:10,980 --> 00:04:18,580
Often it's both. Probably more likely it's both.

36
00:04:18,580 --> 00:04:22,980
But it's never equal like that.

37
00:04:22,980 --> 00:04:29,780
Because it's not equal you find something in one side.

38
00:04:29,780 --> 00:04:34,780
And you're sure is right. And it's important to be right.

39
00:04:34,780 --> 00:04:38,780
It's not to say that picking a side isn't a good thing.

40
00:04:38,780 --> 00:04:44,780
And two sides to everything doesn't mean that both sides are equal.

41
00:04:44,780 --> 00:04:48,780
It's a very important fallacy.

42
00:04:48,780 --> 00:04:50,780
That's not the tangle.

43
00:04:50,780 --> 00:04:57,780
You don't become entangled just because you have views and opinions and beliefs.

44
00:04:57,780 --> 00:05:00,780
It's because you think you're right.

45
00:05:00,780 --> 00:05:05,780
The problem is when you try to make other people think that you're right.

46
00:05:05,780 --> 00:05:07,780
It's always important to be right.

47
00:05:07,780 --> 00:05:15,780
It's not always wise to try and impress that upon others.

48
00:05:15,780 --> 00:05:17,780
It's quite a difference.

49
00:05:17,780 --> 00:05:20,780
This is the tangle. The Buddha was right.

50
00:05:20,780 --> 00:05:27,780
The Buddha said, I don't fight with the world. The world fights with me.

51
00:05:27,780 --> 00:05:32,780
And he didn't. He didn't.

52
00:05:32,780 --> 00:05:34,780
He didn't do what we often do.

53
00:05:34,780 --> 00:05:37,780
It's got caught up and embroiled in the issues.

54
00:05:37,780 --> 00:05:43,780
Find ourselves hating others. Find ourselves angry.

55
00:05:43,780 --> 00:05:48,780
Find ourselves in conflict with others.

56
00:05:48,780 --> 00:05:51,780
And how could that be right? How could it be right?

57
00:05:51,780 --> 00:05:57,780
How could it be good to be angry?

58
00:05:57,780 --> 00:06:02,780
How could it be good to hate someone no matter how evil they are?

59
00:06:02,780 --> 00:06:09,780
How could conflict possibly be good?

60
00:06:09,780 --> 00:06:18,780
You don't see that if you're not in tune with the inner world.

61
00:06:18,780 --> 00:06:23,780
And so what we come to see from meditation is that it helps not only untangle the inner tangle,

62
00:06:23,780 --> 00:06:31,780
but the outer tangle as well as inside, so outside.

63
00:06:31,780 --> 00:06:38,780
We start to realize that understanding our own minds is the first step.

64
00:06:38,780 --> 00:06:51,780
To reconciliation.

65
00:06:51,780 --> 00:06:57,780
That if we can come to be at peace with ourselves, we can wrestle with our own emotions

66
00:06:57,780 --> 00:07:06,780
and fear ourselves from the causes of suffering.

67
00:07:06,780 --> 00:07:19,780
We're able to relate to others free from causing suffering.

68
00:07:19,780 --> 00:07:27,780
Without causing suffering in others.

69
00:07:27,780 --> 00:07:37,780
So when we talk about karma, the cycle of karma is in three parts.

70
00:07:37,780 --> 00:07:43,780
What does the wheel, three parts?

71
00:07:43,780 --> 00:07:50,780
There's gileas, which is the filement, and there's kamma, which are the actions.

72
00:07:50,780 --> 00:07:53,780
And then there's vipaka, which is the results.

73
00:07:53,780 --> 00:08:00,780
And based on the results of karma and the results of the past, we get upset again.

74
00:08:00,780 --> 00:08:01,780
And then we have more kilesa.

75
00:08:01,780 --> 00:08:03,780
So it's a cycle.

76
00:08:03,780 --> 00:08:04,780
It's a vicious cycle.

77
00:08:04,780 --> 00:08:11,780
It's how the cycle of vengeance works.

78
00:08:11,780 --> 00:08:13,780
It's how wars are fought.

79
00:08:13,780 --> 00:08:30,780
That's how mental disease, mental illness takes root.

80
00:08:30,780 --> 00:08:37,780
And so our defilements inside help us understand our relationships with others in a new way.

81
00:08:37,780 --> 00:08:43,780
We look inside and see how awful anger is.

82
00:08:43,780 --> 00:08:46,780
We no longer want to be angry.

83
00:08:46,780 --> 00:08:49,780
We say how awful greed is.

84
00:08:49,780 --> 00:08:54,780
We start to see how our manipulations of others are not for their benefit.

85
00:08:54,780 --> 00:08:59,780
They're not rational, they're not deserved.

86
00:08:59,780 --> 00:09:02,780
I deserve this, I deserve that.

87
00:09:02,780 --> 00:09:06,780
They're just greedy.

88
00:09:06,780 --> 00:09:14,780
And see how our ego, when we see how attached we are to things, how controlling we are,

89
00:09:14,780 --> 00:09:17,780
even just of the breath.

90
00:09:17,780 --> 00:09:25,780
When you watch your stomach rising and falling and instead of watching it, you find yourself controlling it, forcing it.

91
00:09:25,780 --> 00:09:32,780
And you bang your head against that wall long enough and you start to let go.

92
00:09:32,780 --> 00:09:36,780
And miraculously, you're no longer a control freak.

93
00:09:36,780 --> 00:09:39,780
You stop trying to control others.

94
00:09:39,780 --> 00:09:43,780
You stop trying to be in charge, be the boss.

95
00:09:43,780 --> 00:09:47,780
You stop being overbearing and belligerent.

96
00:09:47,780 --> 00:09:56,780
You stop trying to force other people to see your side, to agree with you.

97
00:09:56,780 --> 00:10:02,780
But ridiculous things we do.

98
00:10:02,780 --> 00:10:06,780
You see people fighting passionately for good causes.

99
00:10:06,780 --> 00:10:10,780
They're right.

100
00:10:10,780 --> 00:10:14,780
So many people are right about what's going on in America right now.

101
00:10:14,780 --> 00:10:16,780
There's a lot of bad things going on.

102
00:10:16,780 --> 00:10:20,780
There's no question about that in my mind.

103
00:10:20,780 --> 00:10:24,780
But they get angry about it.

104
00:10:24,780 --> 00:10:36,780
You wonder how that could possibly help.

105
00:10:36,780 --> 00:10:42,780
As it informs our karma, we see how we heard ourselves.

106
00:10:42,780 --> 00:10:45,780
We see when we meditate, we watch our actions.

107
00:10:45,780 --> 00:10:51,780
We see how even just simple acts like walking, eating can be violent.

108
00:10:51,780 --> 00:10:55,780
You know, eating can be incredibly violent.

109
00:10:55,780 --> 00:11:03,780
If you don't understand that, you've never been mindful and watched how we stuff ourselves.

110
00:11:03,780 --> 00:11:11,780
How we force food on ourselves out of craving, out of desire.

111
00:11:11,780 --> 00:11:19,780
How by being stressed and unmindful, we create a very unpleasant situation.

112
00:11:19,780 --> 00:11:23,780
There's simple things like eating.

113
00:11:23,780 --> 00:11:25,780
Simple things like daily tasks.

114
00:11:25,780 --> 00:11:28,780
How we wash dishes.

115
00:11:28,780 --> 00:11:37,780
If we do it violently and then break dishes, or we cut ourselves, or we even tense our muscles.

116
00:11:37,780 --> 00:11:39,780
Why we throw our back out?

117
00:11:39,780 --> 00:11:42,780
Why we get a strain neck.

118
00:11:42,780 --> 00:11:49,780
Why we have back aches, why we have headaches, violence.

119
00:11:49,780 --> 00:11:54,780
Because of our defilements.

120
00:11:54,780 --> 00:12:00,780
We learn how to act in ways that are less violent.

121
00:12:00,780 --> 00:12:09,780
It's a microcosm to the general reality of society in the world.

122
00:12:09,780 --> 00:12:17,780
So that when we interact with others, we're not inclined to be violent.

123
00:12:17,780 --> 00:12:21,780
We've worked out our issues inside.

124
00:12:21,780 --> 00:12:25,780
We've also come to learn how to behave.

125
00:12:25,780 --> 00:12:32,780
How to do and say, how to speak and act in ways that are beneficial.

126
00:12:32,780 --> 00:12:37,780
That are helpful, that are conducive to peace and happiness.

127
00:12:37,780 --> 00:12:44,780
Why? Because we've learned, we've studied.

128
00:12:44,780 --> 00:12:49,780
And so the results are happiness.

129
00:12:49,780 --> 00:12:54,780
Even just the results inside.

130
00:12:54,780 --> 00:13:02,780
When you hate, even if you just hate yourself, the results are unpleasant.

131
00:13:02,780 --> 00:13:09,780
And they're not unpleasant, just inside, right? If you hate yourself, you're much more likely to hate others.

132
00:13:09,780 --> 00:13:14,780
We can have this cognitive dissonance where we hate ourselves, and yet we try to be very nice to others,

133
00:13:14,780 --> 00:13:16,780
but it doesn't last.

134
00:13:16,780 --> 00:13:18,780
It sorts to people.

135
00:13:18,780 --> 00:13:31,780
They can seem very self deprecating, deprecating self deprecatory.

136
00:13:31,780 --> 00:13:35,780
And you're trying their best to be kind to others, but eventually it comes through.

137
00:13:35,780 --> 00:13:40,780
You can't have so much hate inside for yourself.

138
00:13:40,780 --> 00:13:46,780
If you want to truly love others, you really have to love yourself.

139
00:13:46,780 --> 00:13:48,780
It's because of the attitude, right?

140
00:13:48,780 --> 00:13:50,780
You can't have this cognitive dissonance.

141
00:13:50,780 --> 00:13:54,780
You can have this dual duality inside.

142
00:13:54,780 --> 00:13:56,780
You can't be two types of people.

143
00:13:56,780 --> 00:14:03,780
You can pretend. You can have a Dr. Jekyll and Mr. Hyde's sort of personality for a while,

144
00:14:03,780 --> 00:14:05,780
but it's not sustainable.

145
00:14:05,780 --> 00:14:07,780
And so it spills over.

146
00:14:07,780 --> 00:14:12,780
We tend without realizing it'd be cruel to others.

147
00:14:12,780 --> 00:14:18,780
Or for greedy, we tend to try to manipulate others.

148
00:14:18,780 --> 00:14:27,780
If we're deluded inside, we tend to be arrogant and considered towards others.

149
00:14:27,780 --> 00:14:33,780
And so the results are unpleasantness in our relationships,

150
00:14:33,780 --> 00:14:42,780
our inability to make peace with our families, with our friends.

151
00:14:42,780 --> 00:14:52,780
Amazing how much it changes when you look inside and when you learn about yourself.

152
00:14:52,780 --> 00:14:54,780
Not because we're just a blame.

153
00:14:54,780 --> 00:14:56,780
It's not to say that the world is in a terrible place,

154
00:14:56,780 --> 00:15:02,780
and that there are important things you can do to help change it.

155
00:15:02,780 --> 00:15:06,780
But that's what's so baffling is that we don't seem to be doing things to change it.

156
00:15:06,780 --> 00:15:15,780
We just like to react. It's not baffling. It's just unfortunate.

157
00:15:15,780 --> 00:15:24,780
There's a clear cause, but it's quite amazing to watch and to see how it becomes so caught up,

158
00:15:24,780 --> 00:15:29,780
thinking that we're doing the right thing when we are cruel to others, when we're angry at others,

159
00:15:29,780 --> 00:15:35,780
when we hate others, when we shout and we fight and we post online about others.

160
00:15:35,780 --> 00:15:58,780
So when you learn to see through that, it's not to say that you don't act and don't work to change the world.

161
00:15:58,780 --> 00:16:05,780
The more it becomes, the more it comes naturally, that you do change the world.

162
00:16:05,780 --> 00:16:08,780
That you change it in a way that brings peace and happiness.

163
00:16:08,780 --> 00:16:13,780
Why? Because you know it brings peace and happiness. You've studied it.

164
00:16:13,780 --> 00:16:18,780
The inner tangle and the outer tangle. They are related.

165
00:16:18,780 --> 00:16:23,780
The meditation is the study of the inner tangle.

166
00:16:23,780 --> 00:16:30,780
And to see how tangles work.

167
00:16:30,780 --> 00:16:55,780
The inner tangle, the inner tangle, the inner tangle.

168
00:16:55,780 --> 00:17:03,780
The wise one cultivates concentration and wisdom.

169
00:17:03,780 --> 00:17:12,780
This biku, this one who sees the danger in the cycle of samsara.

170
00:17:12,780 --> 00:17:17,780
They will be able to untangle the tangle.

171
00:17:17,780 --> 00:17:20,780
The Buddha in his answer didn't make a distinction between the two.

172
00:17:20,780 --> 00:17:25,780
He didn't say the outer tangle should be tackled in this way and the inner tangle should be tangled in that way.

173
00:17:25,780 --> 00:17:31,780
They are one and the same. They are the same tangle.

174
00:17:31,780 --> 00:17:38,780
The meditation is not something that affects only your inner life. It changes everything in your life.

175
00:17:38,780 --> 00:17:49,780
It helps you untangle. It helps you become untangle to be straight, to be clear, to be upright.

176
00:17:49,780 --> 00:17:59,780
To be pure. To be free from entanglement.

177
00:17:59,780 --> 00:18:17,780
So, little food for thought. Little food for thought was the idea that this somehow relates to the issues of today.

178
00:18:17,780 --> 00:18:21,780
That are not anything new. There have always been the issues of today.

179
00:18:21,780 --> 00:18:31,780
And how we deal with them to help us look to the Buddha's teaching, to find some guidance and how to deal with issues, to deal with the world.

180
00:18:31,780 --> 00:18:38,780
Even just to deal with our families, with our friends, with our relationships.

181
00:18:38,780 --> 00:18:41,780
And to learn to see them in the right way. To see the tangle.

182
00:18:41,780 --> 00:18:44,780
To see how entangled we get.

183
00:18:44,780 --> 00:18:50,780
It's not important to be right. It's not important to try to make everyone see the way you do.

184
00:18:50,780 --> 00:18:55,780
It's not important to win.

185
00:18:55,780 --> 00:19:01,780
This is the point that we're always trying to win and trying to conquer.

186
00:19:01,780 --> 00:19:09,780
What we should be doing is trying to untangle to be free.

187
00:19:09,780 --> 00:19:16,780
There you go. There's the dangle for tonight. Thank you all for coming out. Have a good night.

