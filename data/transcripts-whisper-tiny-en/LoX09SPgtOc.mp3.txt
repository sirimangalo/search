so here I am in Sri Lanka first destination this is the monastery I'll be staying up for
the next few days while we have this conference on Buddhism the the thing I'd like to
share today I was I was thinking something I learned over the past two weeks in
Thailand while I was meditating and it didn't come the clarity of it came or the
inspiration for it came not from the meditation but from actually a deviation
or a diversion from the meditation there's a monk who is a relative of my
teacher his nephew or great nephew or whatever and he disrobed while I was
there which which is kind of disappointing it's but he's an interesting
monk and he he said something that really struck me that this really
surprised me actually he had the understanding that the point of of the Buddha's
teaching the point of the person of insight meditation is to suppress or cover
up the defilements of greed of anger and delusion to the point that they
become they they decompose basically is what he was saying they they
rot and when they rot then they I guess disappear and I immediately
argued with him but you know at the time it wasn't clear in my mind what
exactly he was saying and what what what what the importance it had but when I
went back to meditate and I'm putting it together with my meditation and the
fact that it seems so much more so much so clear that meditation is is is not
that or or insight meditation is not that the insight meditation is actually
taking the lid off the pot so to speak of our rotting mind our mind that the
Buddha said is he called the defilements asawa asawa means rotten and they
rot the mind they cause the mind to become pickled actually or or I don't
know what the right word would be and and so that they don't disappear they
rot the mind they're in their rotting causing the mind to be rotten that's not a
good thing it's not good you know it's not a compost pile where you know we're
talking about ourselves and our minds something that is it's important to purify
and to to make pure and so what I thought we're thinking of this morning you
know I was going to talk about this and I thought you know what is this in terms
of the Buddha's teaching and then I realized this is what the Buddha meant by
the middle way because and this is the profundity that we don't often
realize you know we hear the Buddha said don't get involved don't go to the
extreme of sensual attachment to sensual pleasures where you're following
happens we say okay fine I won't be a prince and so on I won't dedicate my
life to sensuality and then don't torture yourself we say okay so I won't go
off and and we'll go naked and stand in the sand and not eat and all these
ascetic practices that they had in India but it's so much it's so much more
immediate than that that's not what is meant by these these two extremes the
two extremes are how we approach reality sensual the addiction
essential pleasures is when the emotions come up whether it's greed or anger or
delusion we follow after them we you know when we want something which is
after when you have desire for something you go for you say okay I want it
therefore I should get it I should chase after the addiction to our attachment
to the torturing yourself is it's so perfectly clear and this is exactly what
this monk was doing it's what most of us do especially when we hear about
Buddhism we hear the Buddha taught and greed is bad anger is bad delusion is
bad and we get this idea that the Buddha was condemning people who had these
things condemning me the person for giving rise to these things you say okay
so in that point is this is the stress and the stop them from me when they
come up that's bad that's sinful that's evil but really this is not how the
Buddha taught the Buddha taught us to understand these things we thought
is to understand greed to understand anger to understand delusion and we don't
get this because we haven't reached the middle way we think if you let them
come up you're gonna follow them there's only two ways you either follow them
or you suppress them but this is the point the Buddha said there's a third
way you when they come up you don't have to follow them but you don't have to
suppress them they come up we say it's a problem if I don't suppress it I'm
going to follow it I'm going to to become greedy I'm going or I'm going to
to chase after and to become addicted to these things but it's not the case so
the truly person if you ask me from my understanding of the Buddha's teaching
it from my own practice is that these things come up and they can be really
scary because we think this is bad this shouldn't be happening you know you
want something it's really lustful for you your hate someone you find some
people find when they meditate suddenly they're full of hate they hate
everyone everything that comes out they're angry at everything but these are
the emotions that exist in our minds and normally we would be guilty about
them and suppress them and say that bad that's sinful that's wrong and they
just rot they don't go away they they pickle our mind they call it cause the
mind to become rotten and corrupted so the the this is this is the scary
truth of the Buddha the Buddha's teachings that you have to let these things
come up you have to be able to accept the evil that's inside of you you we
say it's evil the reason we say it's evil is because it's false as you're
suffering but that doesn't mean suppressing it is going to be going to answer
things it doesn't mean you can deny the existence you have to accept the
fact that inside of me there are things that are if you don't like the word
evil they're unwholesome or they're unskillful or they they hurt us and they
hurt other people and we should get rid of them but they're not going to go away
just by covering them up so this is this is what I really hit me talking to
this monk and through the meditation practice is that they're not they're not
going to to disappear by themselves they have to come up and you have to
accept them in the sense of accepting that they're there and and and take them
apart pull them apart piece by piece and see them for what they are because in
fact all that they are is a knot you know you say you look at this and you
say it's you know it's a knot it's tangled up my hands are tangled up but
really when you pull it apart there's no tangle it is disappeared when you
untie the knot all you have is a straight straight straight row the knot is
in fact a knot entity and this is a very important part of what the Buddha
meant by and that that of non-self that it's not only that we don't have
itself that's the point is that every phenomenon that arises has is not
is a non entity it's it's not something that exists in and of itself and it's
going to be in our mind you know forever or that we have to attack or fight once
we see it we see that it's it's nothing it's a material it arises and it
ceases and it has causes and conditions and if we don't give rise to the cause
the result won't happen so this is my thought for the day and I think it's a
very important teaching very important from the point of view of the past
and the meditation and even more so because it's the first thing that the Buddha
taught when he turned the wheel of the dhamma at Saranat and for the five
months these five first five people who are supposedly the first five people
to understand the Buddha's teaching and this is the first thing he taught he
said we may be away on top of the teaching and let's see what he taught these
are the two there are these two extremes that no one who has left behind or who
has gone forth in search of freedom should follow after this is the addiction
the sensuality or in the sense of chasing after the phenomena that arise and the
suppression or the torturing yourself they are suppressing and repressing
one's desire the addiction it is addiction the torturing yourself but this is how
we torture ourselves we feel guilty we hate ourselves we cause ourselves
suffering thinking that somehow this is you don't have to you don't have to stop
eating or stand on one leg or all these crazy things that they do in
did in the Buddha's time in India in order to torture yourself just by
hating yourself by feeling guilty about yourself that is directly a form of
torture and that's exactly what it's meant here and this is exactly what you
experience in meditation that you you go between these two extremes one
moment you're chasing after them this is great I love it the next moment you're
castigating yourself for this is evil this is bad I'm a bad person and so on and
anger and hatred arises and you torture yourself so thanks for tuning in this
is Sri Lanka
