1
00:00:00,000 --> 00:00:13,000
Okay, good evening, everyone. Welcome to our evening demo session.

2
00:00:13,000 --> 00:00:24,600
So the other talk that I'm set to give this month at this conference is on wrong mindfulness.

3
00:00:24,600 --> 00:00:33,600
I'm not sure if I've talked about this before, but certainly I haven't given this talk yet.

4
00:00:33,600 --> 00:00:41,600
So save you all the cost of coming out to the conference and trying to practice it tonight.

5
00:00:41,600 --> 00:00:46,600
I'm not the talk, but just when I'm sort of the sorts of things I'm going to talk about.

6
00:00:46,600 --> 00:00:55,600
To get a feel for what's important to say.

7
00:00:55,600 --> 00:00:58,600
So wrong mindfulness.

8
00:00:58,600 --> 00:01:10,600
When we talk about wrong mindfulness, where we're concerned about how the practice goes wrong.

9
00:01:10,600 --> 00:01:21,600
The practice can go wrong.

10
00:01:21,600 --> 00:01:30,600
And so the first thing to do is to talk about what we mean by this word mindfulness.

11
00:01:30,600 --> 00:01:35,600
We understand mindfulness that we can talk about how mindfulness can go wrong.

12
00:01:35,600 --> 00:01:42,600
It's an interesting choice of a topic. It wasn't the one I paid. It wasn't picked by me.

13
00:01:42,600 --> 00:01:46,600
But when I asked about what they meant, why did they choose this topic?

14
00:01:46,600 --> 00:01:51,600
They really meant how the practice can go wrong because this happens.

15
00:01:51,600 --> 00:01:59,600
People go and do meditation courses here and there and things can go wrong.

16
00:01:59,600 --> 00:02:05,600
It's never really happened for me, so I'm not trying to scare anyone.

17
00:02:05,600 --> 00:02:09,600
It's the sort of thing that happens to our students.

18
00:02:09,600 --> 00:02:15,600
It's interesting to talk about not our meditators are not perfect and things do go wrong,

19
00:02:15,600 --> 00:02:22,600
but we're here to correct them, which is not such a difficult thing to do.

20
00:02:22,600 --> 00:02:27,600
So it is important to know that the various ways can go wrong.

21
00:02:27,600 --> 00:02:30,600
It doesn't really mean that mindfulness can never be wrong in that sense,

22
00:02:30,600 --> 00:02:37,600
but I'll talk about that at the end of the talk a way that it could be seen to be wrong mindfulness.

23
00:02:37,600 --> 00:02:42,600
It's interesting. Well, there are four ways we can think of it.

24
00:02:42,600 --> 00:02:46,600
But first thing to do is to talk about what we mean by mindfulness.

25
00:02:46,600 --> 00:02:56,600
If we're going to talk about wrong mindfulness, we have to talk about mindfulness or even right mindfulness.

26
00:02:56,600 --> 00:03:05,600
Of course, this is important because mindfulness is just the word.

27
00:03:05,600 --> 00:03:09,600
It's an imprecise translation of the word sati.

28
00:03:09,600 --> 00:03:12,600
It's not the most literal translation out there.

29
00:03:12,600 --> 00:03:16,600
It's not a bad translation or a terrible translation,

30
00:03:16,600 --> 00:03:20,600
but it's not what is directly meant by the word sati.

31
00:03:20,600 --> 00:03:24,600
Sati means it's that he comes from the root sara, which means to remember.

32
00:03:24,600 --> 00:03:34,600
So it was used in a non Buddhist context to refer to being able to remember things that happened a long time ago.

33
00:03:34,600 --> 00:03:39,600
If you've got sati, you're a person with a good memory.

34
00:03:39,600 --> 00:03:46,600
You're a person who is sharp-minded in a sense.

35
00:03:46,600 --> 00:03:52,600
When the Buddha came along the idea was that you remember or you reflect on something,

36
00:03:52,600 --> 00:03:58,600
maybe reflect is a little closer to how it's used in a meditative sense.

37
00:03:58,600 --> 00:04:03,600
But it doesn't have anything to do with exactly mindfulness.

38
00:04:03,600 --> 00:04:07,600
It's the same sort of idea.

39
00:04:07,600 --> 00:04:10,600
But the best way to understand, in Theravada Buddhist,

40
00:04:10,600 --> 00:04:15,600
and the best way to understand something is to look up what we call the lakana di chatukha.

41
00:04:15,600 --> 00:04:21,600
I think I've actually gone over this, or we've gone over it in every senior market course.

42
00:04:21,600 --> 00:04:29,600
Lakana di chatukha is the fourfold qualities of something.

43
00:04:29,600 --> 00:04:38,600
All demos in Buddhism are given this set of four attributes.

44
00:04:38,600 --> 00:04:42,600
We're starting with the lakana, the characteristic.

45
00:04:42,600 --> 00:04:52,600
You have the characteristic of something and what's it like.

46
00:04:52,600 --> 00:04:56,600
The rasa, which is the function, is what it does.

47
00:04:56,600 --> 00:05:07,600
The pajupatana, which is the manifestation, which means how it presents itself.

48
00:05:07,600 --> 00:05:17,600
Then the pajutatana, which is the proximate course,

49
00:05:17,600 --> 00:05:21,600
what is it that causes that dhamma.

50
00:05:21,600 --> 00:05:25,600
So in the vsudi manga, it's got many, many lists of these.

51
00:05:25,600 --> 00:05:30,600
And, one list we have sati mentioned, which is,

52
00:05:30,600 --> 00:05:37,600
it's a really to pull out sati as the define it in this way.

53
00:05:37,600 --> 00:05:40,600
It's quite useful, I think, as you'll see.

54
00:05:40,600 --> 00:05:43,600
So the characteristic of sati is something called apilapana,

55
00:05:43,600 --> 00:05:50,600
which means non-wavering or wobbling.

56
00:05:50,600 --> 00:05:54,600
The characteristic of mindfulness.

57
00:05:54,600 --> 00:05:58,600
And here we're thinking of not just reflecting on something,

58
00:05:58,600 --> 00:06:03,600
but a state of mind that is reflective or that is remembering.

59
00:06:03,600 --> 00:06:07,600
We don't have a word in English for that,

60
00:06:07,600 --> 00:06:15,600
but it's mindful for that good better word.

61
00:06:15,600 --> 00:06:17,600
It doesn't waver from the object.

62
00:06:17,600 --> 00:06:24,600
So the ordinary mind is unstable, right?

63
00:06:24,600 --> 00:06:30,600
It flits here and there, and sati is something that grasps an object.

64
00:06:30,600 --> 00:06:33,600
Grasps in an English sense, right?

65
00:06:33,600 --> 00:06:36,600
When you're able to grasp a concept or something.

66
00:06:36,600 --> 00:06:43,600
So the grasping of an object is to really experience it fully.

67
00:06:43,600 --> 00:06:48,600
And perhaps mindful is good in that sense,

68
00:06:48,600 --> 00:06:57,600
because you fully, you get it fully in your mind.

69
00:06:57,600 --> 00:07:01,600
Your mind is fully aware of it.

70
00:07:01,600 --> 00:07:05,600
The function is asamosa.

71
00:07:05,600 --> 00:07:08,600
Asamosa means not forgetting.

72
00:07:08,600 --> 00:07:11,600
Because once you grasp something, when you grasp something,

73
00:07:11,600 --> 00:07:16,600
you remember it, you stay with it.

74
00:07:16,600 --> 00:07:22,600
So in an ordinary sense, this would mean being able to remember

75
00:07:22,600 --> 00:07:25,600
clearly things that happened in the past or even in the future.

76
00:07:25,600 --> 00:07:28,600
But from a meditative point of view,

77
00:07:28,600 --> 00:07:31,600
it means remembering the actual experience.

78
00:07:31,600 --> 00:07:35,600
So when you have pain, now we all,

79
00:07:35,600 --> 00:07:38,600
even not meditating when we have pain,

80
00:07:38,600 --> 00:07:41,600
we're aware that we have pain, right?

81
00:07:41,600 --> 00:07:46,600
But that awareness, that knowledge is lost,

82
00:07:46,600 --> 00:07:49,600
or that bear experience is gone in the next moment.

83
00:07:49,600 --> 00:07:52,600
Because in the next moment, we're judging it.

84
00:07:52,600 --> 00:07:54,600
We're disliking it.

85
00:07:54,600 --> 00:07:56,600
We're trying to figure out how to get rid of it.

86
00:07:56,600 --> 00:07:59,600
We're doing everything but experiencing it.

87
00:07:59,600 --> 00:08:02,600
And so in that sense, we've forgotten it.

88
00:08:02,600 --> 00:08:06,600
With mindfulness, you don't forget the object, right?

89
00:08:06,600 --> 00:08:07,600
And so when we say to ourselves,

90
00:08:07,600 --> 00:08:11,600
pain, pain, we're remembering it, we're reminding ourselves

91
00:08:11,600 --> 00:08:16,600
and the result, the consequences that we remember it.

92
00:08:24,600 --> 00:08:32,600
The manifestation is a raka,

93
00:08:32,600 --> 00:08:36,600
which means protection regarding

94
00:08:36,600 --> 00:08:39,600
it manifested as a guarded state,

95
00:08:39,600 --> 00:08:47,600
a state that is protected from defilement.

96
00:08:47,600 --> 00:08:51,600
So the mind that is aware of pain,

97
00:08:51,600 --> 00:08:54,600
the mind that is aware that is pain,

98
00:08:54,600 --> 00:09:01,600
is invincible, is impervious, is non-reactive.

99
00:09:01,600 --> 00:09:07,600
That's actually a somewhat specific manifestation.

100
00:09:07,600 --> 00:09:10,600
I mean, it's a characteristic of the experience

101
00:09:10,600 --> 00:09:12,600
that is important for Buddhists.

102
00:09:12,600 --> 00:09:14,600
But the other, it says,

103
00:09:14,600 --> 00:09:21,600
or the manifestation is as the state of confronting

104
00:09:21,600 --> 00:09:25,600
the objective fields or confronting the object,

105
00:09:25,600 --> 00:09:29,600
which is a little bit more clear about what we mean

106
00:09:29,600 --> 00:09:31,600
by the function of mindfulness.

107
00:09:31,600 --> 00:09:33,600
And it's interesting because

108
00:09:33,600 --> 00:09:37,600
when you think about it practically,

109
00:09:37,600 --> 00:09:41,600
this is what we're trying to do is confront our problems.

110
00:09:41,600 --> 00:09:43,600
Normally when we have a problem,

111
00:09:43,600 --> 00:09:45,600
we'll run away from it.

112
00:09:45,600 --> 00:09:47,600
Or when we're confronted by something attractive,

113
00:09:47,600 --> 00:09:49,600
we'll chase after it.

114
00:09:49,600 --> 00:09:53,600
We're unable to confront it, to stay with it.

115
00:09:53,600 --> 00:09:56,600
When something good comes,

116
00:09:56,600 --> 00:10:04,600
we're unable to rest without obtaining it.

117
00:10:04,600 --> 00:10:06,600
When something bad comes,

118
00:10:06,600 --> 00:10:10,600
unpleasant comes, we're unable to rest until it's gone.

119
00:10:12,600 --> 00:10:14,600
We are unable to face it.

120
00:10:14,600 --> 00:10:19,600
So this is clearly what we're talking about when we use this word,

121
00:10:19,600 --> 00:10:21,600
mindfulness or setting,

122
00:10:21,600 --> 00:10:24,600
is not trying to change.

123
00:10:24,600 --> 00:10:27,600
The idea that meditators get mistakenly

124
00:10:27,600 --> 00:10:29,600
is that when you say pain,

125
00:10:29,600 --> 00:10:31,600
pain, the pain is supposed to go away,

126
00:10:31,600 --> 00:10:33,600
you're going to go away.

127
00:10:33,600 --> 00:10:35,600
It's not supposed to go away,

128
00:10:35,600 --> 00:10:36,600
and you're going to go away.

129
00:10:36,600 --> 00:10:38,600
Sometimes it may.

130
00:10:38,600 --> 00:10:41,600
Sometimes the pain is caused by stress in the mind.

131
00:10:41,600 --> 00:10:42,600
So when you're mindful,

132
00:10:42,600 --> 00:10:44,600
it does go away,

133
00:10:44,600 --> 00:10:47,600
but not always.

134
00:10:47,600 --> 00:10:50,600
Now the point is to confront the pain

135
00:10:50,600 --> 00:10:53,600
and to straighten out our mind

136
00:10:53,600 --> 00:10:55,600
to strengthen our mind

137
00:10:55,600 --> 00:10:57,600
so that we're able to experience things

138
00:10:57,600 --> 00:10:59,600
and not forget them,

139
00:10:59,600 --> 00:11:05,600
not get lost in reactions, judgements.

140
00:11:17,600 --> 00:11:21,600
So that's the manifestation.

141
00:11:21,600 --> 00:11:24,600
The last one is the proximate cause.

142
00:11:24,600 --> 00:11:27,600
And this whole thing is really,

143
00:11:27,600 --> 00:11:29,600
actually this talk tonight.

144
00:11:29,600 --> 00:11:33,600
The content here is very important.

145
00:11:33,600 --> 00:11:35,600
It's very important for us to understand

146
00:11:35,600 --> 00:11:36,600
because we use this word

147
00:11:36,600 --> 00:11:38,600
and we say, oh, what do you practice the thing?

148
00:11:38,600 --> 00:11:40,600
Here we teach mindfulness, right?

149
00:11:40,600 --> 00:11:41,600
I always say,

150
00:11:41,600 --> 00:11:44,600
we're here to practice mindfulness meditation.

151
00:11:44,600 --> 00:11:49,600
So here you have an understanding of what that means.

152
00:11:49,600 --> 00:11:52,600
So the proximate cause of mindfulness

153
00:11:52,600 --> 00:11:53,600
and this is important

154
00:11:53,600 --> 00:11:55,600
because it answers the question,

155
00:11:55,600 --> 00:11:57,600
how do you be mindful?

156
00:11:57,600 --> 00:11:58,600
How do you come to be mindful?

157
00:11:58,600 --> 00:12:00,600
Proximate cause is something

158
00:12:00,600 --> 00:12:02,600
called Tirasanya,

159
00:12:02,600 --> 00:12:05,600
which I think many of you have heard me say before.

160
00:12:05,600 --> 00:12:08,600
Sunya means different things,

161
00:12:08,600 --> 00:12:15,600
but here it means the perception of an object.

162
00:12:15,600 --> 00:12:18,600
How you perceive it.

163
00:12:18,600 --> 00:12:22,600
So Sunya can mean the recognition of something you look at.

164
00:12:22,600 --> 00:12:25,600
You see a woman, you see a man,

165
00:12:25,600 --> 00:12:28,600
you see a tree, you hear something and you recognize.

166
00:12:28,600 --> 00:12:30,600
Sunya is what helps you recognize

167
00:12:30,600 --> 00:12:35,600
or is the recognition of the object.

168
00:12:35,600 --> 00:12:39,600
But it can also just mean the perception of something.

169
00:12:39,600 --> 00:12:41,600
And so that's the bare perception.

170
00:12:41,600 --> 00:12:43,600
When you see something,

171
00:12:43,600 --> 00:12:47,600
the perception of seeing that Sunya.

172
00:12:47,600 --> 00:12:50,600
So Tiras, Sunya is always there.

173
00:12:50,600 --> 00:12:53,600
Sunya is in every experience.

174
00:12:53,600 --> 00:12:56,600
But Tirasanya, it's a really interesting word

175
00:12:56,600 --> 00:12:58,600
that you don't hear mention that much.

176
00:12:58,600 --> 00:13:01,600
I bring it up quite a lot because I'm interested in it.

177
00:13:01,600 --> 00:13:08,600
But Tirasanya, Tiras means strong or firm,

178
00:13:08,600 --> 00:13:10,600
which is interesting because Sunya is already there.

179
00:13:10,600 --> 00:13:13,600
So all we're doing is strengthening the perception.

180
00:13:13,600 --> 00:13:16,600
What does that mean?

181
00:13:16,600 --> 00:13:20,600
If you want mindfulness to arise,

182
00:13:20,600 --> 00:13:25,600
it requires a strengthening of the perception.

183
00:13:25,600 --> 00:13:28,600
In the context of the other aspects of this description,

184
00:13:28,600 --> 00:13:31,600
it should be fairly clear.

185
00:13:31,600 --> 00:13:34,600
You're strengthening your experience of your strengthening,

186
00:13:34,600 --> 00:13:39,600
your mind, the mind that experiences it.

187
00:13:39,600 --> 00:13:45,600
Hence the reason why we repeat to ourselves pain.

188
00:13:45,600 --> 00:13:47,600
Because we have the perception of pain,

189
00:13:47,600 --> 00:13:51,600
all we're doing is reaffirming, affirming that.

190
00:13:51,600 --> 00:13:53,600
That's what a mantra does.

191
00:13:53,600 --> 00:13:55,600
A mantra is an ancient meditation technique,

192
00:13:55,600 --> 00:13:57,600
and that sets purpose,

193
00:13:57,600 --> 00:14:01,600
is to strengthen the perception of the object.

194
00:14:01,600 --> 00:14:03,600
So whether it be on a concept,

195
00:14:03,600 --> 00:14:06,600
or whether it be on reality,

196
00:14:06,600 --> 00:14:14,600
Tirasanya is the cause of the grasping of the object.

197
00:14:14,600 --> 00:14:18,600
Or in a practical, more mundane sense,

198
00:14:18,600 --> 00:14:21,600
it says, or the proximate cause,

199
00:14:21,600 --> 00:14:27,600
is just the foundations of mindfulness beginning with the body.

200
00:14:27,600 --> 00:14:29,600
So when we teach mindfulness,

201
00:14:29,600 --> 00:14:32,600
we try to bring up,

202
00:14:32,600 --> 00:14:35,600
usually have people read my booklet,

203
00:14:35,600 --> 00:14:39,600
and the first thing in the booklet is the four foundations of mindfulness.

204
00:14:39,600 --> 00:14:42,600
And that's what it's saying here,

205
00:14:42,600 --> 00:14:45,600
what's the proximate cause of mindfulness,

206
00:14:45,600 --> 00:14:51,600
while practicing the four foundations of mindfulness.

207
00:14:51,600 --> 00:14:54,600
And so these are the body being mindful of the body

208
00:14:54,600 --> 00:14:57,600
when the stomach rises and falls,

209
00:14:57,600 --> 00:14:59,600
when you move your hands or so on.

210
00:14:59,600 --> 00:15:01,600
When you walk, walking,

211
00:15:01,600 --> 00:15:03,600
walking, stepping, right, stepping,

212
00:15:03,600 --> 00:15:10,600
being mindful of the body that way is the cause of mindfulness.

213
00:15:10,600 --> 00:15:13,600
Wade and that number two is Wade and that,

214
00:15:13,600 --> 00:15:14,600
so being mindful of pain,

215
00:15:14,600 --> 00:15:15,600
if you feel pain,

216
00:15:15,600 --> 00:15:16,600
say pain,

217
00:15:16,600 --> 00:15:17,600
if you feel happy,

218
00:15:17,600 --> 00:15:18,600
say happy,

219
00:15:18,600 --> 00:15:23,600
happy or calm, calm.

220
00:15:23,600 --> 00:15:24,600
The mind is thinking,

221
00:15:24,600 --> 00:15:25,600
thinking about the past,

222
00:15:25,600 --> 00:15:26,600
your future,

223
00:15:26,600 --> 00:15:29,600
good thoughts, bad thoughts,

224
00:15:29,600 --> 00:15:32,600
being mindful of thoughts.

225
00:15:32,600 --> 00:15:33,600
And for us,

226
00:15:33,600 --> 00:15:35,600
the dumb are many different things.

227
00:15:35,600 --> 00:15:38,600
We have the emotions or the hindrances,

228
00:15:38,600 --> 00:15:41,600
liking, disliking,

229
00:15:41,600 --> 00:15:43,600
drowsiness, distraction, doubt,

230
00:15:43,600 --> 00:15:44,600
and so on.

231
00:15:44,600 --> 00:15:48,600
All mind states that arise,

232
00:15:48,600 --> 00:15:50,600
judgments that arise,

233
00:15:50,600 --> 00:15:56,600
or while states of mind that are not neutral,

234
00:15:56,600 --> 00:15:58,600
all of those states,

235
00:15:58,600 --> 00:16:01,600
which are hindrances.

236
00:16:01,600 --> 00:16:02,600
We have the senses,

237
00:16:02,600 --> 00:16:03,600
so seeing, hearing,

238
00:16:03,600 --> 00:16:04,600
spelling, tasting,

239
00:16:04,600 --> 00:16:05,600
feeling, thinking,

240
00:16:05,600 --> 00:16:07,600
being mindful of all that.

241
00:16:07,600 --> 00:16:10,600
So the four Satipatana are

242
00:16:10,600 --> 00:16:14,600
just a description of us and our experience.

243
00:16:14,600 --> 00:16:17,600
And the practice of mindfulness based on them,

244
00:16:17,600 --> 00:16:19,600
of course, is what leads to mindfulness.

245
00:16:27,600 --> 00:16:29,600
So that's the La Kana di Chatekah.

246
00:16:29,600 --> 00:16:32,600
That is, I think, a fairly good,

247
00:16:32,600 --> 00:16:36,600
all well-rounded explanation of mindfulness.

248
00:16:36,600 --> 00:16:38,600
The last thing this text says,

249
00:16:38,600 --> 00:16:40,600
which is also quite interesting.

250
00:16:40,600 --> 00:16:44,600
Well, it gives a description

251
00:16:44,600 --> 00:16:47,600
of how it should be seen.

252
00:16:47,600 --> 00:16:48,600
It says,

253
00:16:48,600 --> 00:17:07,600
it says,

254
00:17:07,600 --> 00:17:21,600
like a pillar in that it is well-established,

255
00:17:21,600 --> 00:17:24,600
stuck firmly established in the object.

256
00:17:24,600 --> 00:17:26,600
So the ordinary mind is,

257
00:17:26,600 --> 00:17:28,600
and the texts do go into detail about this.

258
00:17:28,600 --> 00:17:31,600
The ordinary mind is like a gourd,

259
00:17:31,600 --> 00:17:33,600
a pumpkin, let's say,

260
00:17:33,600 --> 00:17:36,600
a pumpkin floating on the water.

261
00:17:36,600 --> 00:17:40,600
So the ordinary mind is,

262
00:17:40,600 --> 00:17:44,600
you know, it's bounced about by the waves of experience,

263
00:17:44,600 --> 00:17:47,600
going with the current.

264
00:17:47,600 --> 00:17:52,600
The ordinary mind is very much susceptible to experience,

265
00:17:52,600 --> 00:17:56,600
to loss, to gain, to praise, to blame,

266
00:17:56,600 --> 00:18:01,600
and so on.

267
00:18:01,600 --> 00:18:06,600
But the mindful mind is like a pillar

268
00:18:06,600 --> 00:18:10,600
that's stuck in the bottom of the ground

269
00:18:10,600 --> 00:18:16,600
and the water can't buff it in.

270
00:18:16,600 --> 00:18:18,600
And it's not moved by the currents

271
00:18:18,600 --> 00:18:20,600
or the vicissitudes.

272
00:18:20,600 --> 00:18:24,600
The mind is not moved by the vicissitudes of life,

273
00:18:24,600 --> 00:18:29,600
whether it's blame or praise or fame or loss,

274
00:18:29,600 --> 00:18:34,600
or happiness suffering.

275
00:18:34,600 --> 00:18:36,600
The good or the bad,

276
00:18:36,600 --> 00:18:37,600
a good meditation session,

277
00:18:37,600 --> 00:18:39,600
a bad meditation session.

278
00:18:39,600 --> 00:18:44,600
The mindful mind is not buffeted by good or bad experiences.

279
00:18:44,600 --> 00:18:47,600
It's not caught up by experiences,

280
00:18:47,600 --> 00:18:50,600
so when seeing or hearing or smelling

281
00:18:50,600 --> 00:18:52,600
or tasting or feeling or thinking,

282
00:18:52,600 --> 00:18:55,600
there is no reaction,

283
00:18:55,600 --> 00:18:58,600
and that's no suffering.

284
00:18:58,600 --> 00:19:03,600
This right here, this is really the core of Buddhism,

285
00:19:03,600 --> 00:19:07,600
just this little paragraph.

286
00:19:07,600 --> 00:19:10,600
That's wonderful because I get feedback from people

287
00:19:10,600 --> 00:19:13,600
who tell me, I think last night,

288
00:19:13,600 --> 00:19:17,600
one of the questions was basically saying how great,

289
00:19:17,600 --> 00:19:19,600
how amazed they were, how remarkable it was,

290
00:19:19,600 --> 00:19:22,600
how well this works.

291
00:19:22,600 --> 00:19:23,600
I think so too.

292
00:19:23,600 --> 00:19:27,600
It's a big reason why I'm here repeating these things.

293
00:19:27,600 --> 00:19:30,600
On the internet.

294
00:19:30,600 --> 00:19:33,600
The last thing it says is,

295
00:19:33,600 --> 00:19:35,600
it should also be seen as,

296
00:19:35,600 --> 00:19:39,600
I think it should also be seen as,

297
00:19:39,600 --> 00:19:42,600
like the vārika,

298
00:19:42,600 --> 00:19:44,600
the vārika vya,

299
00:19:44,600 --> 00:19:46,600
like a doorkeeper,

300
00:19:46,600 --> 00:19:50,600
a gatekeeper.

301
00:19:50,600 --> 00:19:55,600
Because jakudvāra dīra kānātō,

302
00:19:55,600 --> 00:19:59,600
because it protects or guards,

303
00:19:59,600 --> 00:20:03,600
the doors starting with the eye,

304
00:20:03,600 --> 00:20:07,600
the eye door and so on.

305
00:20:07,600 --> 00:20:08,600
Mindfulness is often,

306
00:20:08,600 --> 00:20:09,600
I think I mentioned,

307
00:20:09,600 --> 00:20:12,600
the mindfulness is often likened to a doorkeeper,

308
00:20:12,600 --> 00:20:16,600
because all of our experiences come through the senses,

309
00:20:16,600 --> 00:20:18,600
the eye, the ear, the nose,

310
00:20:18,600 --> 00:20:21,600
the tongue, the body and the mind,

311
00:20:21,600 --> 00:20:31,600
and in a sense,

312
00:20:31,600 --> 00:20:35,600
this is how the defilements get to us.

313
00:20:35,600 --> 00:20:38,600
This is how the defilements enter our hearts.

314
00:20:38,600 --> 00:20:41,600
When you experience something and you're not mindful of it,

315
00:20:41,600 --> 00:20:43,600
so you're not guarding that door,

316
00:20:43,600 --> 00:20:45,600
then the defilements come,

317
00:20:45,600 --> 00:20:49,600
liking this, liking anger, frustration,

318
00:20:49,600 --> 00:20:52,600
addiction, worry, stress,

319
00:20:52,600 --> 00:21:00,600
all causes of suffering come.

320
00:21:08,600 --> 00:21:11,600
And so mindfulness is this guard.

321
00:21:11,600 --> 00:21:12,600
When you're mindful,

322
00:21:12,600 --> 00:21:14,600
you're mindful of the senses,

323
00:21:14,600 --> 00:21:16,600
and seeing it's just seeing

324
00:21:16,600 --> 00:21:19,600
a deep tumm-a-tām-a-tām-gāwī-sātī.

325
00:21:19,600 --> 00:21:20,600
When you say to yourself,

326
00:21:20,600 --> 00:21:22,600
seeing you're reminding yourself,

327
00:21:22,600 --> 00:21:24,600
when you remind yourself,

328
00:21:24,600 --> 00:21:27,600
there's no reaction.

329
00:21:27,600 --> 00:21:29,600
This is this.

330
00:21:29,600 --> 00:21:30,600
It's not good.

331
00:21:30,600 --> 00:21:31,600
It's not bad.

332
00:21:31,600 --> 00:21:32,600
It's not me.

333
00:21:32,600 --> 00:21:33,600
It's not mine.

334
00:21:33,600 --> 00:21:35,600
It is what it is.

335
00:21:35,600 --> 00:21:37,600
That's what mindfulness gives us.

336
00:21:37,600 --> 00:21:42,600
Mindfulness is a quality of that we cultivate,

337
00:21:42,600 --> 00:21:45,600
and that we become comfortable with

338
00:21:45,600 --> 00:21:46,600
familiar with,

339
00:21:46,600 --> 00:21:50,600
and that becomes part of our patterned behavior.

340
00:21:50,600 --> 00:21:53,600
It becomes a habit.

341
00:21:53,600 --> 00:21:56,600
And through the practice of mindfulness,

342
00:21:56,600 --> 00:21:58,600
of course, this is what leads to insight

343
00:21:58,600 --> 00:22:00,600
and insight, of course,

344
00:22:00,600 --> 00:22:02,600
is what leads to freedom.

345
00:22:02,600 --> 00:22:04,600
So this is the path.

346
00:22:04,600 --> 00:22:05,600
This is what the Buddha said,

347
00:22:05,600 --> 00:22:09,600
is the acayana-manga.

348
00:22:09,600 --> 00:22:12,600
I've talked for quite a bit,

349
00:22:12,600 --> 00:22:14,600
and what I think I'm going to do

350
00:22:14,600 --> 00:22:16,600
is I'm going to have to shorten that

351
00:22:16,600 --> 00:22:18,600
when I actually give my talk.

352
00:22:18,600 --> 00:22:20,600
Good to know.

353
00:22:20,600 --> 00:22:23,600
But I'm going to give the second half

354
00:22:23,600 --> 00:22:25,600
tomorrow, I think.

355
00:22:25,600 --> 00:22:27,600
If I'm here tomorrow,

356
00:22:27,600 --> 00:22:29,600
if I don't die in the meantime,

357
00:22:29,600 --> 00:22:33,600
I'll give the second half tomorrow.

358
00:22:33,600 --> 00:22:36,600
Okay, so thank you all for coming out.

359
00:22:36,600 --> 00:22:44,600
That's the demo for tonight.

360
00:22:44,600 --> 00:22:46,600
Let's see if there are any questions.

361
00:22:46,600 --> 00:22:52,600
If I can get to the questions.

362
00:22:52,600 --> 00:22:54,600
Well, it looks good.

363
00:22:54,600 --> 00:22:56,600
Okay.

364
00:22:56,600 --> 00:22:58,600
A bunch of questions.

365
00:22:58,600 --> 00:23:00,600
Oh, no, a bunch of...

366
00:23:00,600 --> 00:23:04,600
Okay.

367
00:23:04,600 --> 00:23:11,600
Questions?

368
00:23:11,600 --> 00:23:12,600
You guys can go.

369
00:23:12,600 --> 00:23:15,600
You better not sit and listen to these.

370
00:23:15,600 --> 00:23:18,600
They're not really related to the practice.

371
00:23:18,600 --> 00:23:21,600
Not always related to the practice.

372
00:23:21,600 --> 00:23:24,600
Should I only be mindful of my own actions

373
00:23:24,600 --> 00:23:26,600
throughout the day or try by focusing

374
00:23:26,600 --> 00:23:28,600
on the external environment as well,

375
00:23:28,600 --> 00:23:30,600
such as the immediate surroundings?

376
00:23:30,600 --> 00:23:33,600
Should I try to be mindful of whatever you can?

377
00:23:33,600 --> 00:23:35,600
I mean, if you're clear,

378
00:23:35,600 --> 00:23:38,600
what is the field or the...

379
00:23:38,600 --> 00:23:43,600
What's the word?

380
00:23:43,600 --> 00:23:47,600
Well, the boundaries of what mindfulness,

381
00:23:47,600 --> 00:23:50,600
because you can't be mindful of concepts.

382
00:23:50,600 --> 00:23:52,600
That's not part of our practice.

383
00:23:52,600 --> 00:23:55,600
I'll talk a little bit more about that tomorrow.

384
00:23:55,600 --> 00:23:57,600
But as long as it's reality,

385
00:23:57,600 --> 00:24:00,600
as long as it's an expression of reality,

386
00:24:00,600 --> 00:24:04,600
that's what you should be mindful of.

387
00:24:04,600 --> 00:24:07,600
So as far as the external environment,

388
00:24:07,600 --> 00:24:10,600
while you're aware of your experience of it.

389
00:24:10,600 --> 00:24:12,600
So seeing, you'd say seeing,

390
00:24:12,600 --> 00:24:15,600
if it's hearing, you'd say hearing, hearing,

391
00:24:15,600 --> 00:24:22,600
that kind of thing.

392
00:24:22,600 --> 00:24:24,600
In regards to letting go of control,

393
00:24:24,600 --> 00:24:29,600
and yet still doing the things you're responsible for.

394
00:24:29,600 --> 00:24:49,600
You're not actually asking a question.

395
00:24:49,600 --> 00:24:51,600
I think I know what you're getting at,

396
00:24:51,600 --> 00:24:53,600
but it's a little too complicated for me.

397
00:24:53,600 --> 00:24:58,600
And that kid's who may be pinpoint exactly what you want to know.

398
00:24:58,600 --> 00:25:01,600
You want to know about letting go of letting go,

399
00:25:01,600 --> 00:25:04,600
and yet still doing what is responsible.

400
00:25:04,600 --> 00:25:06,600
I mean, I can talk to that.

401
00:25:06,600 --> 00:25:10,600
Talk about that.

402
00:25:10,600 --> 00:25:16,600
What you're responsible for is just a duty.

403
00:25:16,600 --> 00:25:20,600
And it's part of what is most efficient.

404
00:25:20,600 --> 00:25:22,600
So that doesn't change.

405
00:25:22,600 --> 00:25:25,600
Just because you're very mindful doesn't mean you stop

406
00:25:25,600 --> 00:25:29,600
doing good things for people,

407
00:25:29,600 --> 00:25:32,600
because doing good things is generally the most efficient thing to do.

408
00:25:32,600 --> 00:25:38,600
It prevents a lot of stress and suffering and complications.

409
00:25:38,600 --> 00:25:46,600
But the rest of that, I don't really have much to speak about.

410
00:25:46,600 --> 00:25:49,600
Consumed by extreme guilt and self-hatred over a mistake,

411
00:25:49,600 --> 00:25:50,600
I made a long time ago.

412
00:25:50,600 --> 00:25:54,600
How do you overcome a horrible deed you committed?

413
00:25:54,600 --> 00:25:58,600
Well, you would be mindful of the guilt and self-hatred.

414
00:25:58,600 --> 00:26:00,600
I mean, it's a bad habit.

415
00:26:00,600 --> 00:26:04,600
So habits are hard to overcome.

416
00:26:04,600 --> 00:26:06,600
But it's part of our practice.

417
00:26:06,600 --> 00:26:08,600
None of it, none of it.

418
00:26:08,600 --> 00:26:09,600
None of this is really easy.

419
00:26:09,600 --> 00:26:12,600
It just takes time and a lot of work.

420
00:26:12,600 --> 00:26:15,600
But it's important to realize that it's not good to feel guilt.

421
00:26:15,600 --> 00:26:18,600
It's not good to hate yourself.

422
00:26:18,600 --> 00:26:20,600
So you can stop that.

423
00:26:20,600 --> 00:26:23,600
Stop actively encouraging it.

424
00:26:23,600 --> 00:26:26,600
But that's as much evil.

425
00:26:26,600 --> 00:26:28,600
I mean, that's real evil.

426
00:26:28,600 --> 00:26:34,600
It's evil as well as the evil thing you did.

427
00:26:34,600 --> 00:26:38,600
Is it wholesome to celebrate the wonders and beauty of life?

428
00:26:38,600 --> 00:26:41,600
No, it's kind of unwholesome.

429
00:26:41,600 --> 00:26:45,600
Because life is wretched and ugly.

430
00:26:45,600 --> 00:26:47,600
No, I don't know about that.

431
00:26:47,600 --> 00:26:52,600
Life is a tough one because life is just a concept.

432
00:26:52,600 --> 00:26:57,600
But our existence is pretty wretched.

433
00:26:57,600 --> 00:27:02,600
So if you're celebrating it, there might be a problem there.

434
00:27:02,600 --> 00:27:04,600
What do I mean by that?

435
00:27:04,600 --> 00:27:07,600
I talked about wretched a couple of days ago yesterday.

436
00:27:07,600 --> 00:27:10,600
No, a couple of days ago, I think.

437
00:27:14,600 --> 00:27:21,600
You get intoxicated by these things and it creates a pattern of attachment.

438
00:27:21,600 --> 00:27:23,600
It doesn't actually make you happier.

439
00:27:23,600 --> 00:27:24,600
And it's very contentious.

440
00:27:24,600 --> 00:27:29,600
I'm sure there are people who disagree very strongly with what I'm saying.

441
00:27:29,600 --> 00:27:32,600
Which, of course, is fine.

442
00:27:32,600 --> 00:27:34,600
The Buddha said,

443
00:27:34,600 --> 00:27:53,600
it's just like a royal chariot.

444
00:27:53,600 --> 00:28:05,600
Nutty.

445
00:28:05,600 --> 00:28:08,600
You see a beautiful kings chariot.

446
00:28:08,600 --> 00:28:11,600
And you can think how wonderful how marvelous it is.

447
00:28:11,600 --> 00:28:14,600
The world is like that all decked out.

448
00:28:14,600 --> 00:28:18,600
So many beautiful things, wonderful things.

449
00:28:18,600 --> 00:28:23,600
But the wise have no connection with it.

450
00:28:23,600 --> 00:28:27,600
If the world is such a beautiful place, why is there so much suffering?

451
00:28:27,600 --> 00:28:31,600
I mean, this is really the question, why is there so much evil?

452
00:28:31,600 --> 00:28:35,600
Is there so much wretchedness?

453
00:28:35,600 --> 00:28:39,600
Why are we not truly happy?

454
00:28:39,600 --> 00:28:41,600
Can the world make us happy?

455
00:28:41,600 --> 00:28:43,600
The answer is no.

456
00:28:43,600 --> 00:28:44,600
That's really the point.

457
00:28:44,600 --> 00:28:47,600
So our wretchedness really comes from trying to find happiness in the world.

458
00:28:47,600 --> 00:29:03,600
Happiness is possibly happy in the world, but you have to be above it, in a sense.

459
00:29:03,600 --> 00:29:11,600
You have to be independent, not susceptible to the changes of life.

460
00:29:11,600 --> 00:29:15,600
I mean, it's not that you don't experience the changes.

461
00:29:15,600 --> 00:29:24,600
But our problem is that when we experience them, we're not in tune with them.

462
00:29:24,600 --> 00:29:29,600
So I think we often mix this wonder and appreciation with being in tune with reality.

463
00:29:29,600 --> 00:29:34,600
The person who's in tune with reality, it's like a part of the landscape.

464
00:29:34,600 --> 00:29:35,600
Right?

465
00:29:35,600 --> 00:29:36,600
Here's the difference.

466
00:29:36,600 --> 00:29:41,600
It's romantic to think of someone standing there appreciating the mountain.

467
00:29:41,600 --> 00:29:46,600
Right? But the classical zen stories.

468
00:29:46,600 --> 00:29:51,600
Here we sit the mountain in me until only the mountain remains.

469
00:29:51,600 --> 00:29:55,600
Because eventually the enlightened being becomes a part of the landscape.

470
00:29:55,600 --> 00:30:00,600
So rather than the person miring it, they are a part of it.

471
00:30:00,600 --> 00:30:02,600
It's quite different, do you see?

472
00:30:02,600 --> 00:30:05,600
You don't become part of the landscape by admiring it.

473
00:30:05,600 --> 00:30:10,600
In fact, you become quite enamored and will often do your best to try and fix it and make it.

474
00:30:10,600 --> 00:30:13,600
Make it the way you want it to be.

475
00:30:13,600 --> 00:30:15,600
Control it, right?

476
00:30:15,600 --> 00:30:21,600
We love nature so much that we've destroyed it.

477
00:30:21,600 --> 00:30:27,600
Trying to make it perfect so we can have the perfect climate.

478
00:30:27,600 --> 00:30:34,600
We have destroyed our climate.

479
00:30:34,600 --> 00:30:37,600
Does meditation help with improving oneself?

480
00:30:37,600 --> 00:30:42,600
Yes, absolutely. That's what meditation is for.

481
00:30:42,600 --> 00:30:44,600
In one sense, I suppose.

482
00:30:44,600 --> 00:30:47,600
In a deeper level, it's for letting go of yourself.

483
00:30:47,600 --> 00:30:52,600
But that's an improvement.

484
00:30:52,600 --> 00:30:57,600
What do you think of meditation as a means to reaching mundane goals instead of reaching enlightenment?

485
00:30:57,600 --> 00:31:00,600
Or spiritual goals?

486
00:31:00,600 --> 00:31:04,600
Did the Buddha speak of this?

487
00:31:04,600 --> 00:31:07,600
I mean, it depends how mundane, right?

488
00:31:07,600 --> 00:31:09,600
It can't be used to get greedy.

489
00:31:09,600 --> 00:31:11,600
It can't be used to become ambitious.

490
00:31:11,600 --> 00:31:17,600
So being successful in business is probably not.

491
00:31:17,600 --> 00:31:19,600
Probably not.

492
00:31:19,600 --> 00:31:22,600
Impossible.

493
00:31:22,600 --> 00:31:25,600
But no, meditation improves things really.

494
00:31:25,600 --> 00:31:33,600
The problem with what you're referring to is that those things don't actually have any purpose or benefit.

495
00:31:33,600 --> 00:31:37,600
So because mindfulness leads to wisdom, it's going to show you that.

496
00:31:37,600 --> 00:31:43,600
You're going to see that those things are useless and certainly won't help you accomplish them.

497
00:31:43,600 --> 00:31:53,600
The only thing is that's a good thing because those things are not worth obtaining anyway.

498
00:31:53,600 --> 00:32:05,600
But I mean, as far as helping with health and general well-being, living your life more peacefully, I mean, it's very much a part of it.

499
00:32:05,600 --> 00:32:09,600
Okay, so that's all for the questions.

500
00:32:09,600 --> 00:32:10,600
That's all for tonight.

501
00:32:10,600 --> 00:32:12,600
Thank you all for coming out.

502
00:32:12,600 --> 00:32:27,600
Have a good night.

