1
00:00:00,000 --> 00:00:12,480
The Buddha said that patience can't be put among the Bodhika patients is the greatest form

2
00:00:12,480 --> 00:00:22,280
of torture, maybe not torture, but a sadicism or tapa means heat, but it was a word

3
00:00:22,280 --> 00:00:31,400
that was used for self-mortification or discipline, maybe discipline is the right word.

4
00:00:31,400 --> 00:00:40,880
Patience is the highest form of discipline, that we have all these other ways of forcing ourselves

5
00:00:40,880 --> 00:00:48,580
or tricking ourselves into bearing with unpleasantness, but the things surpasses true

6
00:00:48,580 --> 00:00:55,680
patience. And this I think really what you feel directly when you meditate, maybe this

7
00:00:55,680 --> 00:01:02,200
is one of the, for all those people that they're looking for, the benefits of meditation

8
00:01:02,200 --> 00:01:09,440
and find themselves in having a hard time figuring out the bad, the actual benefits

9
00:01:09,440 --> 00:01:17,600
of meditation. I think you can see this quite clearly and quite quickly is the patience

10
00:01:17,600 --> 00:01:24,120
that which you were unable to bear previously, you're now able to bear. I think the

11
00:01:24,120 --> 00:01:29,920
problem is when we look at benefits of meditation where we're often looking in the wrong

12
00:01:29,920 --> 00:01:53,560
place or expected.

