1
00:00:00,000 --> 00:00:05,000
So, welcome back to our study of the Dhamabanda.

2
00:00:05,000 --> 00:00:15,000
Today, we continue with verse number 29, which reads as follows.

3
00:00:35,000 --> 00:00:46,000
And when others are, when others are, who, when others sleep, is full of effort, full of energy.

4
00:00:46,000 --> 00:00:59,000
Just like a horse with swift, a swift-footed horse leaves behind a, a weak horse or a horse without strength on Balasana.

5
00:00:59,000 --> 00:01:10,000
So, the wise leave behind all others, leave behind those who are weak in the same way.

6
00:01:10,000 --> 00:01:17,000
So, this is, this was in regards to a fairly short story, but quite an interesting one.

7
00:01:17,000 --> 00:01:28,000
It has interesting implications on our practice in general, and our practice in a general sense of why we're doing what we're doing and the importance of what we're doing.

8
00:01:28,000 --> 00:01:42,000
So, the story goes that there were two, two friends who came in and heard the Buddha's teaching and became monks as a result of hearing the Buddha's teaching.

9
00:01:42,000 --> 00:01:50,000
They listened to it, and they realized that it was difficult to practice the Buddha's teaching as a lay person.

10
00:01:50,000 --> 00:02:03,000
So, they decided that they would undertake the formal training and to dedicate themselves to that, because they realized that it was something of use and a benefit.

11
00:02:03,000 --> 00:02:16,000
And it happened that one of them, having taken the meditation teachings of the Buddha, took it quite seriously and realized that this meditation was something that was a benefit.

12
00:02:16,000 --> 00:02:24,000
It was for the purposes of actually putting it into practice, and that it had some result, that was a benefit.

13
00:02:24,000 --> 00:02:27,000
So, he undertook quite strenuous practice.

14
00:02:27,000 --> 00:02:41,000
During the whole day, he would practice walking and sitting, and then at night he would do walking and sitting for the first watch, the first four hours until 10pm or so.

15
00:02:41,000 --> 00:02:50,000
And then he would lie down and sleep for four hours, and then at 2am he would get up, and the third part of the night from 2am to 6am.

16
00:02:50,000 --> 00:02:58,000
He would do walking and sitting again after he went for food and did his other affairs.

17
00:02:58,000 --> 00:03:03,000
Then he would come back and spend the day, day to day he would spend it in this way.

18
00:03:03,000 --> 00:03:06,000
So, he was quite intensive practice.

19
00:03:06,000 --> 00:03:14,000
The other monk was quite industrious as well, but in a different way.

20
00:03:14,000 --> 00:03:23,000
So, he would collect firewood and sweep the monastery and organize this and organize that.

21
00:03:23,000 --> 00:03:34,000
And then in the evening he would sit by the fire and talk with the lay people in the monastery, and the novices and all of the people who had come to listen to the teachings.

22
00:03:34,000 --> 00:03:39,000
He would sit around and joke with them and talk with them and chat with them.

23
00:03:39,000 --> 00:03:56,000
And he didn't get what his friend was doing, so he went to check on his friend later on, and he saw his friend at around 10pm going into his room or in his room lying down to sleep.

24
00:03:56,000 --> 00:04:04,000
And he went and said to him, what is this all you do all the time sleeping? Because it's all he saw. He never saw his friend doing anything but lying down to sleep.

25
00:04:04,000 --> 00:04:14,000
So, at 10pm he went in and he scolded him and he said, you say that you're going to practice strenuously and here you are lying down to sleep.

26
00:04:14,000 --> 00:04:16,000
And his friend didn't listen to him.

27
00:04:16,000 --> 00:04:19,000
He was saying, all you do is sit around and do nothing.

28
00:04:19,000 --> 00:04:29,000
Here he's thinking of himself like I collect firewood, I do all this work in the monastery and on top of that I entertain the lay people or whatever he was thinking.

29
00:04:29,000 --> 00:04:37,000
He thought he was industrious and his friend wasn't doing anything except walking and sitting and he didn't even really know what his friend was doing.

30
00:04:37,000 --> 00:04:42,000
Obviously he thought he just lay around and slept all the time.

31
00:04:42,000 --> 00:04:52,000
So, they stayed together like this for some time and eventually his story goes at the friend who was industrious or who was dedicated to the meditation practice.

32
00:04:52,000 --> 00:05:01,000
He became Naran, he became enlightened, he practiced to the point where he was able to free his mind from greed, from anger and from delusion.

33
00:05:01,000 --> 00:05:19,000
And it's no wonder considering the intensity of his practice just as someone who studies science or studies for a master's or a PhD if they work hard and they study and in no long time they'll be able to pass the exam.

34
00:05:19,000 --> 00:05:26,000
In the same way he put the effort in, he put the work in and so as a result he was able to become enlightened.

35
00:05:26,000 --> 00:05:35,000
His friend on the other hand gained nothing but he was very very industrious and so at the end of the period this reigns period I suppose.

36
00:05:35,000 --> 00:05:41,000
They went back to see the Buddha and the Buddha asked them so how did you spend your reigns? How did it go?

37
00:05:41,000 --> 00:05:45,000
And did you gain any attainment or something like that?

38
00:05:45,000 --> 00:05:52,000
And the monk who spent all his time sitting around sitting around doing nothing he said,

39
00:05:52,000 --> 00:06:01,000
could no one day, how could this one have gained anything? He was Buddha said have you been heedful?

40
00:06:01,000 --> 00:06:09,000
And he said, how could he be from in what way could he be considered heedful? He just sat around sleeping all the time.

41
00:06:09,000 --> 00:06:12,000
He's talking about the wonders and lighten.

42
00:06:12,000 --> 00:06:16,000
And then the Buddha looked and said, what about you?

43
00:06:16,000 --> 00:06:23,000
And he said, me, I spent all my time sweeping and gathering firewood and so on and so on and so on.

44
00:06:23,000 --> 00:06:31,000
And then he kind of realized and he said, and then I just sat around and talked with the other laypeople and said,

45
00:06:31,000 --> 00:06:34,000
but I didn't sleep, I didn't spend all my time sleeping.

46
00:06:34,000 --> 00:06:39,000
And the Buddha looked at him and he was very stern glance and said,

47
00:06:39,000 --> 00:06:46,000
I'm a Buddha sah. There's a Santikeh and so on and so on and so on. He said,

48
00:06:46,000 --> 00:06:51,000
you compared with my son and he's talking about the Arahan, the enlightened one.

49
00:06:51,000 --> 00:06:56,000
He's compared with my son, you're like a weak horse of little strength who gains nothing.

50
00:06:56,000 --> 00:07:02,000
He never gets to the goal. He has left you far behind and then he gave this verse.

51
00:07:02,000 --> 00:07:12,000
Appamato and so on. He's that person, a person who is hateful, who is actually practicing the meditation,

52
00:07:12,000 --> 00:07:19,000
who is mindful among the people who are mindless, who when other people sleep is full of energy.

53
00:07:19,000 --> 00:07:31,000
And he leaves this verse such a person leaves behind such people just as a swift horse leaves behind a weak horse.

54
00:07:31,000 --> 00:07:37,000
This such a person is called Sumedah, that's just one who is wise.

55
00:07:37,000 --> 00:07:45,000
So the reason in this story of these two people had quite different ideas of the proper way to live one's life.

56
00:07:45,000 --> 00:07:50,000
And I was thinking about this and at first it seems like quite a simple story.

57
00:07:50,000 --> 00:07:53,000
Like, oh yeah, yeah, yeah, be the one who will practice this meditation,

58
00:07:53,000 --> 00:07:57,000
but there's just a quite a bit more to this.

59
00:07:57,000 --> 00:08:06,000
And it points to the question of the benefit and the significance of what we're doing here.

60
00:08:06,000 --> 00:08:11,000
Because many people will ask, why are you here? What are you doing? What do you hope to gain from this?

61
00:08:11,000 --> 00:08:15,000
What benefit do you see in walking and sitting?

62
00:08:15,000 --> 00:08:19,000
And people will often be quite critical of what we do in fact.

63
00:08:19,000 --> 00:08:24,000
And say, you're sitting around doing nothing, wasting your life and on top of that,

64
00:08:24,000 --> 00:08:31,000
living off of the charity of others. So people will often criticize this fact that, why don't we get a real job?

65
00:08:31,000 --> 00:08:40,000
Why don't you let it go out and try to work in the world and see what it's like and do something of your life, for example.

66
00:08:40,000 --> 00:08:49,000
People, my father telling me to go get an education and people telling me to get a job and so on.

67
00:08:49,000 --> 00:08:57,000
So we have this question of, why don't we work like other people?

68
00:08:57,000 --> 00:09:08,000
Why don't we do something that is considered to be working and considered to be work in a worldly sense.

69
00:09:08,000 --> 00:09:12,000
One said, a man stopped me and said, oh, you don't work, do you?

70
00:09:12,000 --> 00:09:14,000
And I said, yes, I work. And he said, what do you do?

71
00:09:14,000 --> 00:09:19,000
Well, I teach and he wasn't all that impressed.

72
00:09:19,000 --> 00:09:23,000
But there's even more to it than that, even if a person doesn't teach.

73
00:09:23,000 --> 00:09:29,000
The question is, could a life as a meditator, if a person just simply spends their time meditating?

74
00:09:29,000 --> 00:09:37,000
Could this be considered a valid means of living one's life?

75
00:09:37,000 --> 00:09:45,000
And from a meditator point of view, as someone who has undertaken this, it seems like quite a simple question to answer.

76
00:09:45,000 --> 00:09:53,000
But it's one that has the world kind of suspicious of this sort of lifestyle.

77
00:09:53,000 --> 00:09:57,000
And so I think the question that we have to ask is, what does it lead to?

78
00:09:57,000 --> 00:10:01,000
The things that you do in your life, what do you get out of them?

79
00:10:01,000 --> 00:10:05,000
What does the world get out of them? What fruit do they bring?

80
00:10:05,000 --> 00:10:12,000
The theory behind much of the Buddhist teaching is in regards to the fruit of one's acts.

81
00:10:12,000 --> 00:10:18,000
If one acts in a certain way, the theory is that it will bring a certain fruit or the importance.

82
00:10:18,000 --> 00:10:24,000
The important point is that it will bring a certain fruit, not the quality or not the type of action that one does,

83
00:10:24,000 --> 00:10:26,000
but the fruit that it brings.

84
00:10:26,000 --> 00:10:33,000
So if a person does something and it brings suffering, it brings pain, it brings discomfort,

85
00:10:33,000 --> 00:10:41,000
it brings stress, it brings conflict, then no matter how industrious it is, we consider it to be a bad thing.

86
00:10:41,000 --> 00:10:46,000
And I think the logic there is hard to argue with.

87
00:10:46,000 --> 00:11:00,000
So this argument that has become quite pernicious amongst ordinary people is that a person is living a good life based on their industry.

88
00:11:00,000 --> 00:11:04,000
And so the Greeks, this was even something that the Greeks had to grapple with.

89
00:11:04,000 --> 00:11:09,000
And so they had this story of this man who pushes a boulder up the mountain.

90
00:11:09,000 --> 00:11:11,000
This is his hell, really.

91
00:11:11,000 --> 00:11:16,000
He has to push this boulder up the mountain, and when it gets to the top, it falls back down.

92
00:11:16,000 --> 00:11:29,000
And this is an allegory or a metaphor for ordinary life, for people who work in such a way that they're just getting nothing from an age.

93
00:11:29,000 --> 00:11:37,000
All that's happening is they're getting older and they're living a stressful life and not really making any benefit whatsoever.

94
00:11:37,000 --> 00:11:40,000
So you ask, what benefit is that sort of life?

95
00:11:40,000 --> 00:11:42,000
What benefit is a life of industry?

96
00:11:42,000 --> 00:11:48,000
If all a person does is push a rock up the hill for it to fall back down.

97
00:11:48,000 --> 00:12:00,000
So the argument that industry makes validity or makes one's life beneficial is obviously a false one.

98
00:12:00,000 --> 00:12:04,000
The question we have to ask is, what good does your life do?

99
00:12:04,000 --> 00:12:09,000
So many people think that their life, they say that their life brings benefit to society.

100
00:12:09,000 --> 00:12:11,000
They say you have to do your part in society.

101
00:12:11,000 --> 00:12:13,000
This is what people always say.

102
00:12:13,000 --> 00:12:18,000
They leave the world, people who go off and do meditation courses.

103
00:12:18,000 --> 00:12:25,000
They're just wasting their time and whether when they could be out, they're helping society.

104
00:12:25,000 --> 00:12:31,000
So the question we have to ask is, what is really a benefit to society?

105
00:12:31,000 --> 00:12:33,000
What is really a benefit to the world?

106
00:12:33,000 --> 00:12:37,000
And even we have to ask, what is really a benefit to oneself?

107
00:12:37,000 --> 00:12:41,000
Because there are people who spend a lot of their time trying to help others,

108
00:12:41,000 --> 00:12:48,000
physical comfort to others, people who are doctors or who build hospitals or who take care of.

109
00:12:48,000 --> 00:13:00,000
And when there's epidemics or when there's catastrophes, they go in and do this kind of relief efforts and so on.

110
00:13:00,000 --> 00:13:04,000
And spend all of their time and become stressed and become agitated.

111
00:13:04,000 --> 00:13:06,000
And as a result, they burn out.

112
00:13:06,000 --> 00:13:12,000
People who do, for example, social work, sometimes social workers can become quite burned out.

113
00:13:12,000 --> 00:13:17,000
Because they're neglecting their own benefit.

114
00:13:17,000 --> 00:13:20,000
So it is important for us to include our own benefit.

115
00:13:20,000 --> 00:13:25,000
But when we really look at what is a benefit both to ourselves and others,

116
00:13:25,000 --> 00:13:32,000
we have to ask whether the one's physical benefit or one's mental benefit is a real benefit.

117
00:13:32,000 --> 00:13:38,000
And what are the things that lead to true happiness and true peace for a person?

118
00:13:38,000 --> 00:13:47,000
What are the things that lead a person to truly find benefit or to the game benefit?

119
00:13:47,000 --> 00:14:00,000
So if we look at things like building up society and doing our part in terms of fighting wars and keeping some kind of physical stability,

120
00:14:00,000 --> 00:14:05,000
or physical peace, then we can see that there's benefit there.

121
00:14:05,000 --> 00:14:12,000
But that it's, when you compare it to a person's mental health, to a person's mental, well-being,

122
00:14:12,000 --> 00:14:15,000
is quite limited.

123
00:14:15,000 --> 00:14:24,000
So this is what we mean when we see that meditation has some benefit in the life of a meditator

124
00:14:24,000 --> 00:14:32,000
and the life of a person who undertakes to do nothing but developing their mind

125
00:14:32,000 --> 00:14:39,000
and developing insight, developing wisdom, and developing understanding about themselves and the world around them.

126
00:14:39,000 --> 00:14:42,000
Why it's a valid sort of lifestyle?

127
00:14:42,000 --> 00:14:50,000
So when people talk about us living off of charity or wasting our lives and not doing anything for the world,

128
00:14:50,000 --> 00:14:57,000
you have to really ask, what is it that brings benefit to the world or brings benefit in general?

129
00:14:57,000 --> 00:15:01,000
Are the people who give us charity, when they give us charity, what does it do for them?

130
00:15:01,000 --> 00:15:07,000
Does it bring them suffering? Does it bring them sadness? Does it bring them pain and conflict?

131
00:15:07,000 --> 00:15:10,000
And does it upset their minds to do this?

132
00:15:10,000 --> 00:15:14,000
And I think on that hand you'd have to say certainly it doesn't.

133
00:15:14,000 --> 00:15:19,000
On the other hand, if they found out that all we were doing was sitting around collecting firewood and sweeping

134
00:15:19,000 --> 00:15:27,000
and doing meaningless things, then in fact it might upset them, it might make them think twice about supporting us.

135
00:15:27,000 --> 00:15:34,000
But the reason that people give us the reason that people give us this very little bit of support to give us enough food to live for one day

136
00:15:34,000 --> 00:15:37,000
is because they do see benefit in what you do.

137
00:15:37,000 --> 00:15:43,000
They see that a person who develops themselves in this way is not only a benefit to themselves

138
00:15:43,000 --> 00:15:50,000
but is a noble example to the world. And in fact it can be a teacher, it can be someone who brings this sort of teaching together.

139
00:15:50,000 --> 00:15:59,000
So as you see when a person, when we set up this meditation center, then people come to practice and to our day and to continue on the teaching.

140
00:15:59,000 --> 00:16:08,000
And in fact if you've seen what's happened since the time of the Buddha, there's been quite a profound effect that has come into the world.

141
00:16:08,000 --> 00:16:21,000
So through not just ordaining, not just building up monasteries, not just being industrious, but through the actual development of from smile, the development of insight and wisdom and purity of mind.

142
00:16:21,000 --> 00:16:35,000
The point being that in order to help others, as has been said many times, in order to really bring benefit, you can't just go out and work and say this what I have inside, I'm going to bring and benefit the world.

143
00:16:35,000 --> 00:16:42,000
People go out in the world and think I have to do good deeds, I have to benefit the world and so on.

144
00:16:42,000 --> 00:16:52,000
But because of their own confusion inside, because of their own inner delusion and their own anger, their own greed and attachment, their own views, they twist things.

145
00:16:52,000 --> 00:16:58,000
So if someone has a certain religious belief, they will impose it on others.

146
00:16:58,000 --> 00:17:20,000
If they have a certain, any sort of belief, they will impose it on others. If they have attachment, they will twist and instead of helping, they will wind up working only for their own material benefit and following after their greed and meeting with a great amount of stress and suffering as well.

147
00:17:20,000 --> 00:17:37,000
If a person on the other hand, refrains from helping others, for the time being develops themselves inside, takes out this attachment, the versions, the fear and the worry and the stress that we have inside.

148
00:17:37,000 --> 00:17:52,000
And purifies one's mind, then just as the source of a river, when it becomes purest, is suitable to be drank by all the animals and all the beings down to the end of the river.

149
00:17:52,000 --> 00:18:06,000
So too, when one purifies one's mind, everything that one does, all of one's interactions, then when one goes out in the world or interacts with people, people who come to visit the monastery,

150
00:18:06,000 --> 00:18:17,000
or visit the meditation center, one will benefit them immensely, will react to them, will interact with them, will impart upon them.

151
00:18:17,000 --> 00:18:23,000
The importance of this purity, people will be able to see that this is a true path to peace.

152
00:18:23,000 --> 00:18:34,000
It's something that brings them, peace, happiness, and freedom from suffering as well, and encourages them to develop in that way.

153
00:18:34,000 --> 00:18:40,000
So the point being, in brief, is that we have to understand what is a real benefit.

154
00:18:40,000 --> 00:18:45,000
And so when people say, oh, you're just sitting around and doing nothing, well, we have to look quite a little bit deeper than that.

155
00:18:45,000 --> 00:18:53,000
And we have to be very careful not to get caught up in this idea, that industry, somehow, somehow, in and of itself, leads to benefit.

156
00:18:53,000 --> 00:19:10,000
If someone is industrious in killing and hurting and harming others, if a person works in these industries, chemical industries, pesticide industries, to kill living beings by the millions, and to harm the environment and so on.

157
00:19:10,000 --> 00:19:26,000
If people work in manufacturing weapons or manufacturing military supplies and so on, then you have to ask whether it's actually sort of things are actually benefiting the world.

158
00:19:26,000 --> 00:19:51,000
People work as politicians and work to drive fear into people of foreign powers and so on, and patriotism, and there's a lot of commercialism encouraging people to, when people work in the entertainment industry, encouraging people to buy music and to listen and to watch movies and so on.

159
00:19:51,000 --> 00:19:58,000
To spend all of their time in idle pursuits that don't truly lead to peace and happiness.

160
00:19:58,000 --> 00:20:03,000
And then you have to ask whether this sort of industry is actually of benefit to the world.

161
00:20:03,000 --> 00:20:05,000
Where has it brought the world?

162
00:20:05,000 --> 00:20:09,000
And where has all of the industry that we see in society brought the world?

163
00:20:09,000 --> 00:20:20,000
If you compare to when people were perhaps less industrious when we were like the monkeys, just sitting around scratching themselves and eating bananas in the forest and kind of...

164
00:20:20,000 --> 00:20:35,000
content with what they had or comparatively, and compared to where we are now with all of our stresses and concerns and wars and so on, are we really any better off?

165
00:20:35,000 --> 00:20:45,000
I wouldn't say we were better off or the monkeys were better off, but I will say that living a peaceful life, living a life that is...

166
00:20:45,000 --> 00:20:59,000
outside of this sort of busyness, this sort of hectic activity where your measure of success is very much how much stress you develop in your life and in the world around you.

167
00:20:59,000 --> 00:21:14,000
The life that leaves this behind in favor of non-interaction, non-participation, where we live in and of ourselves, in the world, as a part of nature.

168
00:21:14,000 --> 00:21:29,000
Instead of our happiness and our peace and our lives, depending on the environment and the company around us, where we sit around and chat like this monk, sitting around and chatting by the fire,

169
00:21:29,000 --> 00:21:39,000
thinking that somehow this was living his life in a good way, until he realized that, oh, the Buddha didn't the Buddha teach that that was the way of wasting your life.

170
00:21:39,000 --> 00:21:44,000
And, in fact, it was a waste of his life, because you see, as a result, he changed nothing about himself.

171
00:21:44,000 --> 00:21:51,000
In fact, the first thing he did when they went to the Buddha, scolded us. They are on, and the one who was enlightened.

172
00:21:51,000 --> 00:21:56,000
The one who had no thoughts of greed, no thoughts of anger, no thoughts of delusion,

173
00:21:56,000 --> 00:22:02,000
who had no thoughts of hatred or no bad thoughts towards others at all.

174
00:22:02,000 --> 00:22:14,000
He has just been sleeping on the time this guy is useless and far, which is quite a sign that he himself is still as far to go.

175
00:22:14,000 --> 00:22:18,000
So, this is, I think, something for us to reflect upon.

176
00:22:18,000 --> 00:22:26,000
I mean, it's obviously not going to be of much use when you're just walking back and forth, or sitting in meditation, then you should be focusing on reality.

177
00:22:26,000 --> 00:22:32,000
But, when the thought comes up, what am I doing here, or is this something that is a real benefit?

178
00:22:32,000 --> 00:22:38,000
Or, you think, if I could be off in the world doing this or doing that, to ask yourself, what is the result of that?

179
00:22:38,000 --> 00:22:43,000
There's things that we've done for so long, the way we've lived our lives, what is the benefit?

180
00:22:43,000 --> 00:22:47,000
What is the meaning? What has it done for us? And what has it done for the world around us?

181
00:22:47,000 --> 00:22:55,000
What has it done for other people? All of the pleasure and all of that happiness that we've been pursuing, all of the goals that we have been pursuing,

182
00:22:55,000 --> 00:23:01,000
trying to change the world, and so on. What has it really done? And what have we been neglecting?

183
00:23:01,000 --> 00:23:09,000
What is missing from the picture? And I think one of the very important things that is missing from the picture a lot of the time is this mental development.

184
00:23:09,000 --> 00:23:15,000
And this is what we have here. This is what we're trying to attain here, this is what we're trying to gain for ourselves.

185
00:23:15,000 --> 00:23:22,000
Because once one's mind is pure, it doesn't matter what one does, where one goes, how one that's one's life.

186
00:23:22,000 --> 00:23:29,000
One brings benefit to oneself and to others, and everything one does.

187
00:23:29,000 --> 00:23:38,000
So we can feel good about what we're doing, and we can think that this is something that is a real benefit.

188
00:23:38,000 --> 00:23:41,000
And this is important in the practice. It's important to feel good about what you're doing.

189
00:23:41,000 --> 00:23:51,000
It's important to see that we're not here, because someone else said meditation is good, or because somehow we have this concept of meditation being a good thing.

190
00:23:51,000 --> 00:24:01,000
Or because we were just running away from the world or something. We should be confident and happy that actually what we're doing is an incredible profound thing.

191
00:24:01,000 --> 00:24:17,000
We're developing ourselves. We're becoming a more pure individual. We're developing our minds to a higher level, so that we're able to see things with more clarity and more wisdom.

192
00:24:17,000 --> 00:24:29,000
So we have this vivid image of, first of all, these two men who went forth and lived their lives quite differently.

193
00:24:29,000 --> 00:24:41,000
And so we can ask ourselves, and this is a reminder to all of us that even though we have work to do, we shouldn't let it get in the way of our meditation, because this is not why we came here, what we came here to do.

194
00:24:41,000 --> 00:24:49,000
And then we have this other vivid image of two horses, one horse that is very fast and one horse that is very weak.

195
00:24:49,000 --> 00:25:00,000
And it's the weak horse that we compared to the person who sleeps all the time and sits around in chats all the time and spends all their time in idle pursuits.

196
00:25:00,000 --> 00:25:07,000
Even in terms of work that has to be done in the monastery, if we spend too much time on that, we'll come to a story later.

197
00:25:07,000 --> 00:25:17,000
The later of a monk who just swept all the time and everybody thought, oh, he must, maybe he's enlightened because here he is devoting himself so strenuously to sweeping.

198
00:25:17,000 --> 00:25:23,000
And the Buddha said, a person who sweeps, I call him a janitor, not a monk.

199
00:25:23,000 --> 00:25:29,000
So then they realize, oh, that's probably not a good sign that he sweeps up.

200
00:25:29,000 --> 00:25:35,000
So we have to put our effort into the meditation practice so that we can...

201
00:25:35,000 --> 00:25:41,000
Again, this isn't a teaching to compare ourselves to others and look down upon others.

202
00:25:41,000 --> 00:25:48,000
It's a teaching for all of us to catch up, to not get left behind.

203
00:25:48,000 --> 00:25:54,000
The point, because obviously there's Arahan, he wasn't the one doing the paddle tailing or doing the scolding or the comparing.

204
00:25:54,000 --> 00:25:56,000
It was the monk who was left behind.

205
00:25:56,000 --> 00:26:02,000
We have to work hard to get over that and we have to be careful not to compare ourselves together.

206
00:26:02,000 --> 00:26:09,000
Sometimes we think that although he's not meditating, I'll just stop as well.

207
00:26:09,000 --> 00:26:15,000
And then you don't realize that actually he was just meditating and he just finished or something.

208
00:26:15,000 --> 00:26:23,000
We have to be careful to do the work that we have to do and not get left behind.

209
00:26:23,000 --> 00:26:29,000
Like the weak horse, we should exert ourselves, especially in the meditation.

210
00:26:29,000 --> 00:26:32,000
And it doesn't mean that we have to push, push, push really hard.

211
00:26:32,000 --> 00:26:35,000
It means we have to be persistent and we have to be patient.

212
00:26:35,000 --> 00:26:40,000
Like the story of the tortoise and hair is appropriate.

213
00:26:40,000 --> 00:26:42,000
Notice two horses want to win the race.

214
00:26:42,000 --> 00:26:45,000
Well, who really wins the race is the one who is slow and steady.

215
00:26:45,000 --> 00:26:48,000
Not the one who pushes, pushes, pushes and then breaks.

216
00:26:48,000 --> 00:26:51,000
And as to take a break.

217
00:26:51,000 --> 00:26:56,000
But the important point that we have to work and we have to work during the meditation practice.

218
00:26:56,000 --> 00:26:58,000
And we should feel good about what we're doing.

219
00:26:58,000 --> 00:27:01,000
We should know that this is what the Buddha would have us do.

220
00:27:01,000 --> 00:27:11,000
And we should know that this is what is really, I think, objectively of benefit to ourselves in the world around us.

221
00:27:11,000 --> 00:27:16,000
It's something that is a potential cure for the troubles that exist in the world.

222
00:27:16,000 --> 00:27:21,000
All of the stress and all of the suffering, all of the problems in the world.

223
00:27:21,000 --> 00:27:25,000
They all come from the human mind or the minds of beings in general.

224
00:27:25,000 --> 00:27:33,000
And the meditation practice is that which has the potential to purify our minds and to bring us out of this.

225
00:27:33,000 --> 00:27:38,000
And by us, meaning the whole world, that simply by doing what we're doing here,

226
00:27:38,000 --> 00:27:43,000
we create something that is the way out for the whole world.

227
00:27:43,000 --> 00:27:50,000
And all of the people out there who are wasting away their lives living their lives in carelessness and uselessness.

228
00:27:50,000 --> 00:27:56,000
They're able to see this and they're able to see that here's something that could actually transform my life into something meaningful.

229
00:27:56,000 --> 00:28:01,000
Where I'm able to see things clearly, where I don't react to things with greed or anger,

230
00:28:01,000 --> 00:28:09,000
where I don't fight and have conflict, where I don't indulge in useless pursuits that bring me back just after pushing it up.

231
00:28:09,000 --> 00:28:12,000
They don't bring me back to the bottom again.

232
00:28:12,000 --> 00:28:18,000
And so these people will take this as an example and the whole world will begin to do things that are benefit.

233
00:28:18,000 --> 00:28:29,000
They will begin to help each other and to develop, help themselves develop their own minds and develop the world in such a way that that brings peace happiness and freedom from suffering.

234
00:28:29,000 --> 00:28:34,000
Whether it will be done or not is another point, another question.

235
00:28:34,000 --> 00:28:38,000
But here we have an answer to the question of why don't we go and get a job?

236
00:28:38,000 --> 00:28:41,000
Why don't we go out and work and work like real people do?

237
00:28:41,000 --> 00:28:43,000
People do in the real world.

238
00:28:43,000 --> 00:28:48,000
The point is that those people are living in the fake world. They're living in the world of illusion.

239
00:28:48,000 --> 00:28:52,000
Where you think there's some benefit from pushing a rock up a hill.

240
00:28:52,000 --> 00:28:56,000
And then when it goes back to the bottom, you start to push it up again.

241
00:28:56,000 --> 00:29:06,000
And here we're trying to stop that. We're trying to come back to the real world.

242
00:29:06,000 --> 00:29:10,000
The world where reality is the object of our attention.

243
00:29:10,000 --> 00:29:13,000
We're able to see things exactly truly as they are.

244
00:29:13,000 --> 00:29:16,000
But pushing a rock up a hill is just pushing a rock up a hill.

245
00:29:16,000 --> 00:29:19,000
There's no benefit from it.

246
00:29:19,000 --> 00:29:21,000
Whatever we do is just whatever we do.

247
00:29:21,000 --> 00:29:24,000
Walking back and forth is just walking back and forth.

248
00:29:24,000 --> 00:29:27,000
Eating is just eating. Talking is just talking.

249
00:29:27,000 --> 00:29:29,000
Sleeping is just sleeping.

250
00:29:29,000 --> 00:29:31,000
Everything is just what it is.

251
00:29:31,000 --> 00:29:39,000
When you get to that point, you're able to live without any sort of attachment or clinging.

252
00:29:39,000 --> 00:29:43,000
A hanger or depression or frustration or sadness.

253
00:29:43,000 --> 00:29:46,000
Kind of live without suffering.

254
00:29:46,000 --> 00:29:49,000
And to benefit others as well.

255
00:29:49,000 --> 00:29:51,000
So that's the teaching for today.

256
00:29:51,000 --> 00:29:55,000
I think it's a very important significance in this sense.

257
00:29:55,000 --> 00:29:57,000
So thank you all for listening.

258
00:29:57,000 --> 00:30:13,000
And tune in next time.

