1
00:00:00,000 --> 00:00:08,840
Hello everyone broadcasting live oops we know live hello everyone broadcasting live

2
00:00:08,840 --> 00:00:16,720
January 5th 2016 we just did a demo part of a video the apologize for yesterday

3
00:00:16,720 --> 00:00:25,560
I expected to be out for the morning right but then we we went for lunch

4
00:00:25,560 --> 00:00:34,640
after this meeting I went with this Chinese monk and I've been and then he took

5
00:00:34,640 --> 00:00:38,920
me up to see his ADA group piece of property up at Stovell and then he said

6
00:00:38,920 --> 00:00:43,640
you know is there anywhere else you want to go he had a free day I guess and I said

7
00:00:43,640 --> 00:00:46,400
oh I'm sure we could think of somewhere to go just joking and I started

8
00:00:46,400 --> 00:00:51,320
thinking and I said I wonder how far we are from my father turned out we were

9
00:00:51,320 --> 00:00:57,920
about an hour away from my father lives so I gave him a call and we drove

10
00:00:57,920 --> 00:01:06,720
out there and about an hour and my father's house

11
00:01:11,080 --> 00:01:17,480
and it was funny this Chinese monk he said that was the first Western

12
00:01:17,480 --> 00:01:25,000
you know non-Asian family he's he's ever he's ever was house he's ever been

13
00:01:25,000 --> 00:01:32,880
in which is you know he's he's been here 30 years in Canada that was quite an

14
00:01:32,880 --> 00:01:40,760
experience he's a nice guy we have different ideas of some but we don't talk

15
00:01:40,760 --> 00:01:49,000
about it we're good I thought I thought I thought I didn't think but I forgot

16
00:01:49,000 --> 00:01:54,240
it's that I meet him I is a Buddha I was confusing with Amaloki Teshvara

17
00:01:54,240 --> 00:01:59,400
there was a body set but Amitabaz actually a Buddha they had this idea that

18
00:01:59,400 --> 00:02:04,840
there's a Buddha up in the Pure Land who if you chant his name or her name

19
00:02:04,840 --> 00:02:13,120
then you can go to their Western Pure Land where all the Buddha's live or

20
00:02:13,120 --> 00:02:18,640
you all live so the Vespuddas or something I remember talking to a Chinese

21
00:02:18,640 --> 00:02:23,980
Nanda about this who came to practice with Vasana I think I really struck a

22
00:02:23,980 --> 00:02:27,560
chord with her it was really interesting because no one there had sort of

23
00:02:27,560 --> 00:02:31,320
challenged her faith and I kind of challenged it I said you know how is that

24
00:02:31,320 --> 00:02:36,080
different from Christianity and I think maybe she was even like well

25
00:02:36,080 --> 00:02:39,600
Christianity is also been they said well okay but how is that different from

26
00:02:39,600 --> 00:02:45,840
the flying spaghetti monster do you know what the flying spaghetti monster

27
00:02:45,840 --> 00:02:57,160
yes how is it different and I really really known it ever you know I'm equipped

28
00:02:57,160 --> 00:03:02,240
with all this ammunition that I've been really thinking and talking and

29
00:03:02,240 --> 00:03:07,800
learning about all this these different arguments that people have and we

30
00:03:07,800 --> 00:03:12,200
kind of threw her off guard I think it's interesting

31
00:03:12,200 --> 00:03:33,320
so tomorrow we have meditation today I started school again I don't know

32
00:03:33,320 --> 00:03:43,040
about this school thing my father's happy he's glad I'm doing it so there's

33
00:03:43,040 --> 00:03:54,320
that Ivan says I should do it this Chinese monk I don't know sort of have to

34
00:03:54,320 --> 00:04:00,560
finish this year out but maybe next year I can take more interesting more

35
00:04:00,560 --> 00:04:08,000
useful courses you know it's nice to learn languages we got we got those

36
00:04:08,000 --> 00:04:15,840
care of chips and stuff did you send that Robin oh yes well the the care of

37
00:04:15,840 --> 00:04:21,160
powder and the dandelion coffee and all that so we're hosting

38
00:04:21,160 --> 00:04:29,400
having her we're hosting this Hamilton interface group it's

39
00:04:29,400 --> 00:04:34,040
Emma say Hamilton interfaith peace group I think it's got what it's called so

40
00:04:34,040 --> 00:04:38,280
it's different religions coming together to talk about peace and to find

41
00:04:38,280 --> 00:04:41,960
ways to spread peace so every year they have a conference and we're planning a

42
00:04:41,960 --> 00:04:48,040
conference we're going to tell stories this year stories about peace and so

43
00:04:48,040 --> 00:04:52,920
this year our theme is the environment as well so it's going to be the idea

44
00:04:52,920 --> 00:04:57,120
is to talk about how the environment relates to peace it's not a very good

45
00:04:57,120 --> 00:05:02,400
thing but kind of you know there are peace there are aspects of peace that

46
00:05:02,400 --> 00:05:09,720
involve the environment I think there's an interesting there's interesting

47
00:05:09,720 --> 00:05:19,200
stories about it but the idea of there's a good one about being grateful and

48
00:05:19,200 --> 00:05:26,800
at least being mindful you know mindful that where our food comes from you know

49
00:05:26,800 --> 00:05:33,800
and mindful of our environment I think that's a big thing because you know it's

50
00:05:33,800 --> 00:05:38,440
it's lack of mindfulness lack of like just forgetting or not paying

51
00:05:38,440 --> 00:05:44,320
attention to what we're doing to the to the world and Buddhism believes that

52
00:05:44,320 --> 00:05:51,880
the word we've been degrading since we started sticking to this earth see we

53
00:05:51,880 --> 00:06:00,920
were like angels celestial beings and then we began to associate with this

54
00:06:00,920 --> 00:06:07,560
globe you know the earth and then taste from it and as we became more involved

55
00:06:07,560 --> 00:06:13,200
with it our bodies became coarser until we were stuck to and as we got stuck to

56
00:06:13,200 --> 00:06:24,440
it slowly became more and more course more and more physical until until today

57
00:06:24,440 --> 00:06:29,920
and so in the in modern times we tend to think the opposite right we think

58
00:06:29,920 --> 00:06:33,680
we're advancing you know we've built up all these neat tools and gadgets and

59
00:06:33,680 --> 00:06:38,440
so on and this is really advancing but at the same time we're ignoring the

60
00:06:38,440 --> 00:06:44,600
fact that the world around us is disgusting is is just unglier

61
00:06:44,600 --> 00:06:51,480
unglier everything right the rainforest we're cutting down the air that

62
00:06:51,480 --> 00:06:56,680
we're polluting the water we're polluting there's an island the size of

63
00:06:56,680 --> 00:07:02,160
Texas apparently of plastic and island of plastic the size of Texas somewhere

64
00:07:02,160 --> 00:07:09,240
on the Pacific Ocean something like that it's not more beautiful it's not

65
00:07:09,240 --> 00:07:18,720
more comfortable it's not more pleasant so the environment is interesting I'm

66
00:07:18,720 --> 00:07:22,600
just interested in the peace aspect I'm interested in being involved to talk

67
00:07:22,600 --> 00:07:29,760
about these about peace with these do my part you know anyway so we're hosting

68
00:07:29,760 --> 00:07:35,640
that and so we're trying to think of food as peace and mindfulness of food and

69
00:07:35,640 --> 00:07:39,680
that kind of thing the mindfulness that our food should be medicine and you

70
00:07:39,680 --> 00:07:45,680
can't just eat anything don't want it obsessed too much but without well

71
00:07:45,680 --> 00:07:51,520
okay we're doing it then we should get food no they actually they said they

72
00:07:51,520 --> 00:07:59,200
wanted to talk this week about food as peace because in a social level food and

73
00:07:59,200 --> 00:08:10,560
peace have are related slave labor and government manipulation international

74
00:08:10,560 --> 00:08:19,040
manipulation of markets turning countries into banana republics or

75
00:08:19,040 --> 00:08:23,240
growing nothing but sugar in the country so that no one has anything to eat in

76
00:08:23,240 --> 00:08:30,440
the country you know and taking all the land to grow food for luxury food for

77
00:08:30,440 --> 00:08:36,640
foreign entity they put all the people on one small part of the land and the

78
00:08:36,640 --> 00:08:41,640
rest of it is owned by the corporations this is how many of this central

79
00:08:41,640 --> 00:08:46,200
South American countries work I think whether used to anyone don't know how it

80
00:08:46,200 --> 00:08:56,600
any way do you have any questions we do all right let's go through a few

81
00:08:56,600 --> 00:09:01,720
okay and just another comment Samantha sent a carob snacks that look that

82
00:09:01,720 --> 00:09:06,080
actually look delicious unlike the rest of the stuff which looks a little

83
00:09:06,080 --> 00:09:13,200
health-foody but we do have questions if the universe has no purpose why does

84
00:09:13,200 --> 00:09:20,440
it exist I don't know that's the one question I don't I can't answer I have no

85
00:09:20,440 --> 00:09:29,920
idea why the universe no idea what can't figure it out but what we the answer

86
00:09:29,920 --> 00:09:34,680
we do have is what to do about it no it's just really more important I mean

87
00:09:34,680 --> 00:09:37,760
philosophers are struggled with that for the longest time it's why they

88
00:09:37,760 --> 00:09:43,520
came up with God I mean it's it's it's just don't understand the air go God that's

89
00:09:43,520 --> 00:09:48,320
the argument but that's always been the argument it's raining why is it

90
00:09:48,320 --> 00:09:56,880
raining must be God lightning what does that must be gone yeah and he's

91
00:09:56,880 --> 00:10:09,120
got sick must be God no that's that Satan you know or whatever you know you have must be this I did this

92
00:10:09,120 --> 00:10:14,960
chant and this person got healed of their sickness must be God

93
00:10:14,960 --> 00:10:21,280
they in this atheist call it the God of the gaps so for as long as there are gaps

94
00:10:21,280 --> 00:10:26,920
in our learning we can this is this is as long as God is going to last as we

95
00:10:26,920 --> 00:10:33,240
learn more we've got to disappear but it's not even that there are some

96
00:10:33,240 --> 00:10:38,440
things that we just can't know I think part of it is being inside the system

97
00:10:38,440 --> 00:10:43,480
we're good at figuring out systems that were not involved in but systems that

98
00:10:43,480 --> 00:10:51,280
we are involved in are problematic to understand I mean what we do know is that

99
00:10:51,280 --> 00:10:59,480
there's not there's no entity so the universe of course doesn't exist all that

100
00:10:59,480 --> 00:11:07,160
exists is this moment and this moment is not constantly changing

101
00:11:07,160 --> 00:11:16,400
the moment doesn't entity maybe exists the experience exists it's one

102
00:11:16,400 --> 00:11:18,400
experience one experience one experience one experience one experience one it's

103
00:11:18,400 --> 00:11:27,360
pretty would it be possible to take the daily dama audio and make a daily podcast

104
00:11:27,360 --> 00:11:32,600
from it it would be nice to listen to it on the go whenever I count to an

105
00:11:32,600 --> 00:11:38,600
in-liver sit down to watch YouTube I think we did pretty easy to create a you

106
00:11:38,600 --> 00:11:44,040
want to feed right and RSS feed or something yeah we did point out that there is

107
00:11:44,040 --> 00:11:51,880
the live archive so you can listen to it and mp3 format from there yeah it would

108
00:11:51,880 --> 00:11:55,240
be pretty easy to just put together a piece script if someone wants to put

109
00:11:55,240 --> 00:12:03,640
together a PHP script I can host it it's pretty easy I even have one I think just

110
00:12:03,640 --> 00:12:12,120
adapt it maybe that's pretty easy actually I've probably got the script

111
00:12:12,120 --> 00:12:14,760
that we need

112
00:12:15,560 --> 00:12:20,120
hello pante when I am at work can I note working working instead of each

113
00:12:20,120 --> 00:12:26,560
individual action it's not very useful you're not going to learn much that way

114
00:12:26,560 --> 00:12:40,680
it'll keep you kind of kind of present but yeah not exactly it's useful

115
00:12:40,680 --> 00:12:45,440
absolutely because you are working and that's what you're doing it keeps you

116
00:12:45,440 --> 00:12:51,080
present you so you will start to see things arising in CC it's not wrong per

117
00:12:51,080 --> 00:12:56,840
say but it's imprecise that word is not going to get you to know very clearly

118
00:12:56,840 --> 00:13:01,720
every movement it's not going to keep you on your toes and it's not going to be

119
00:13:01,720 --> 00:13:08,600
able to help you distinguish physical and mental it's blurry you see

120
00:13:08,600 --> 00:13:18,160
because working involves thoughts and intentions and so on it kind of blurs over

121
00:13:18,160 --> 00:13:20,720
too much

122
00:13:22,080 --> 00:13:27,440
hello pante I found Buddhist beliefs and teachings to be based on experience and

123
00:13:27,440 --> 00:13:31,440
are clear understanding of reality and so would like to know more about how

124
00:13:31,440 --> 00:13:35,840
rebirth is a confirmed occurrence I guess the question is how is everyone so

125
00:13:35,840 --> 00:13:39,560
sure rebirth happens at all

126
00:13:39,560 --> 00:13:43,480
this person the meditator

127
00:13:51,520 --> 00:14:01,040
right how is everyone sure rebirth occurs at all we're not sure most of us but

128
00:14:01,040 --> 00:14:05,480
then we're not sure death occurs either so the question is which one is more

129
00:14:05,480 --> 00:14:17,760
likely that's I think up for debate I think the reason where we lean more

130
00:14:17,760 --> 00:14:25,560
towards death being likely people in general and not Buddhist but people in

131
00:14:25,560 --> 00:14:31,040
general lean more towards the idea of death as opposed to continuation is because

132
00:14:31,040 --> 00:14:36,840
we think of the world as from in terms of entities insert an impersonal entity

133
00:14:36,840 --> 00:14:42,880
so we think of things as being broken we think of a light as being turned off

134
00:14:42,880 --> 00:14:49,440
right when you turn off your computer the power is like sucked out of it this

135
00:14:49,440 --> 00:14:54,280
is the idea that computers now off when you turn off your car the engine shuts

136
00:14:54,280 --> 00:15:04,120
down well it's about that's all first of all it's imprecise because when you

137
00:15:04,120 --> 00:15:12,360
shut off the engine nothing disappears it just things stop moving if it's

138
00:15:12,360 --> 00:15:18,520
saying but all the components are still there and the same goes with death when

139
00:15:18,520 --> 00:15:26,000
the person dies components don't change don't disappear but we think of the

140
00:15:26,000 --> 00:15:31,960
mind as like motion or something right it's not like motion this is the key

141
00:15:31,960 --> 00:15:37,760
yes the water does stop but that's all the physical and yes the body does shut

142
00:15:37,760 --> 00:15:44,680
down that's all physical it's really kind of meaningless because all of the

143
00:15:44,680 --> 00:15:51,000
components are still there and the human body slowly disperses but it

144
00:15:51,000 --> 00:15:56,600
that's all it is it's just disperses so the question is what happens to the mind

145
00:15:56,600 --> 00:16:01,440
does the mind disperse and so they argue that the mind is like motion or the

146
00:16:01,440 --> 00:16:08,800
mind is like heat or the mind is like sound but it's not really I mean all of

147
00:16:08,800 --> 00:16:12,880
those things are still products of matter there's still matter-based things

148
00:16:12,880 --> 00:16:18,920
they're still what you would call what John Locke would call secondary

149
00:16:18,920 --> 00:16:27,120
secondary qualities of matter they're not meant the ideas of them ideas of

150
00:16:27,120 --> 00:16:30,120
these things are something quite different with the experiences of these

151
00:16:30,120 --> 00:16:36,160
things that arise in the mind are totally different and so as Buddhists we lean

152
00:16:36,160 --> 00:16:40,320
towards the idea of continuation because we not because of this logic sort of

153
00:16:40,320 --> 00:16:46,520
logical argument but more importantly because of the argument of the

154
00:16:46,520 --> 00:16:53,520
primacy of the mind that the mind is mind comes first so we look at the

155
00:16:53,520 --> 00:16:59,200
universe in the terms of from the point of view of experience here and now and

156
00:16:59,200 --> 00:17:03,520
from the point of view of experience we don't have any reason to believe that

157
00:17:03,520 --> 00:17:10,000
the death of the body ends this experience I mean if you've meditated enough it's

158
00:17:10,000 --> 00:17:14,720
just becomes absurd to think that this would just suddenly stop for no reason

159
00:17:14,720 --> 00:17:19,760
because we can see it continuing you can see it not just continuing but giving

160
00:17:19,760 --> 00:17:24,920
birth to new branches as you like something as you want something as you strive

161
00:17:24,920 --> 00:17:30,120
for something or as you hate something as you reverse to something it it's

162
00:17:30,120 --> 00:17:35,040
organic and it's mutating all the time so to think that that should just stop

163
00:17:35,040 --> 00:17:40,120
is kind of a quaint sort of view from the point of view of an intensive meditator I

164
00:17:40,120 --> 00:17:44,840
mean it's easy for us to fall into that because we've been brought up with the

165
00:17:44,840 --> 00:17:51,160
idea that death is it some of us others of us were I don't know that I ever

166
00:17:51,160 --> 00:17:56,440
was I don't know if I ever thought that death was it maybe I did actually and

167
00:17:56,440 --> 00:18:00,440
maybe it scared me I remember when I was young I was scared some people

168
00:18:00,440 --> 00:18:05,520
don't I mean Christians are big on the one one rebirth kind of thing being

169
00:18:05,520 --> 00:18:11,040
reborn in heaven or hell and they're often excited to hear that there's more

170
00:18:11,040 --> 00:18:21,080
than one there's a possibility for more than one rebirth but yeah I mean if

171
00:18:21,080 --> 00:18:29,480
you if you don't do a lot of meditation it's really and I think when you

172
00:18:29,480 --> 00:18:33,680
start maintaining it becomes a little confusing because you don't see it

173
00:18:33,680 --> 00:18:39,560
and because of your views but I think that's all they are is their views of

174
00:18:39,560 --> 00:18:48,200
death anyway Buddhism is all about the present moment the key is past lives

175
00:18:48,200 --> 00:18:51,720
future lives don't really matter because they're past in future it's an

176
00:18:51,720 --> 00:18:56,720
important part of the philosophy because it encourages us to strive

177
00:18:56,720 --> 00:19:01,160
knowing that there are consequences to our actions but really that's all

178
00:19:01,160 --> 00:19:06,080
you need to get out of this is that our actions have consequences and if you

179
00:19:06,080 --> 00:19:11,240
act if you act as though your actions have consequences it doesn't matter

180
00:19:11,240 --> 00:19:16,440
whether you believe in rebirth or not because there's an implicit belief there

181
00:19:16,440 --> 00:19:20,520
even if you say well I don't but I'm still gonna act as though there are

182
00:19:20,520 --> 00:19:25,480
consequences rather than act like a jerk just because you can get away with it

183
00:19:25,480 --> 00:19:29,760
when you die well that's it you know it's the worst you're gonna die no there's

184
00:19:29,760 --> 00:19:35,080
there's actually much worse than that but if you if you act in terms of that's

185
00:19:35,080 --> 00:19:41,720
the worst that's going to happen then then it's problematic because you can

186
00:19:41,720 --> 00:19:46,320
act in unwholesome ways you're more likely to act in unwholesome ways it's

187
00:19:46,320 --> 00:19:52,160
actually unpleasant for you it creates problems for you when you act in

188
00:19:52,160 --> 00:19:58,640
unwholesome ways so the most important thing is to act as though your actions

189
00:19:58,640 --> 00:20:08,040
have consequences and and learn to better yourself to avoid unwholesome

190
00:20:08,040 --> 00:20:10,600
content

191
00:20:12,520 --> 00:20:17,880
Greetings Ponte my aunt told me she used to suffer from epilepsy when she was

192
00:20:17,880 --> 00:20:23,160
young and as a result was put on medication for her seizures by her mid-20s she

193
00:20:23,160 --> 00:20:27,400
was instructed to medicate every day for nearly three years after which time her

194
00:20:27,400 --> 00:20:31,960
epilepsy was cured she's now in her 60s and has not suffered from any seizure

195
00:20:31,960 --> 00:20:36,720
sense the doctor claimed through consistent dosing for periods of time the

196
00:20:36,720 --> 00:20:41,160
medication the medication would permanently change her brain chemistry which

197
00:20:41,160 --> 00:20:45,800
would please explain how the practices of formal insight meditation has

198
00:20:45,800 --> 00:20:49,200
similar effects if any

199
00:20:50,600 --> 00:20:57,520
but she I don't mean like for epilepsy no I think permanently changing brain

200
00:20:57,520 --> 00:21:02,400
chemistry would insight meditation my mother just sent me an article on it I

201
00:21:02,400 --> 00:21:07,320
didn't read it yeah yeah the mind everything changes your brain if you do

202
00:21:07,320 --> 00:21:12,560
something enough it changes your brain as to what medication does that's quite

203
00:21:12,560 --> 00:21:20,760
different medication does changes it more what's the word well holistically I

204
00:21:20,760 --> 00:21:26,520
guess I don't know what whatever the word is but it it's not pinpointing you

205
00:21:26,520 --> 00:21:32,480
know conscious activity like medication will alter certain pathways in the

206
00:21:32,480 --> 00:21:39,400
brain it's quite specific but in medication you know it pickles your brain so

207
00:21:39,400 --> 00:21:46,400
it's a different kind of change I don't I don't really know how it does it

208
00:21:46,400 --> 00:21:51,240
and it can target I suppose it can target certain parts but not really the

209
00:21:51,240 --> 00:22:02,040
brain you know so these SSRI's serotonin SRI's a certain tone and re-up take

210
00:22:02,040 --> 00:22:05,160
inhibitors these are a lot of these psychoactive drugs that people take

211
00:22:05,160 --> 00:22:12,840
brings I depression they're they're not specific they're not addressing

212
00:22:12,840 --> 00:22:16,360
the problem the problem part of the problem is the pathways in the brain the

213
00:22:16,360 --> 00:22:21,600
habits that have caused this the pathways in the brain to change but then you

214
00:22:21,600 --> 00:22:28,640
just soak that in this chemical that prevents the serotonin from from leaving

215
00:22:28,640 --> 00:22:34,880
the bloodstream I think or or something so that you're always have serotonin

216
00:22:34,880 --> 00:22:48,480
which is a really nice feeling drug but it's it's it's it involves the

217
00:22:48,480 --> 00:22:56,600
entire brain global so I think the word I'm looking for it's global so it's a little

218
00:22:56,600 --> 00:23:00,560
bit different meditation and addiction also addiction is very much a

219
00:23:00,560 --> 00:23:05,680
specific pathway or a set of pathways that you're strengthening so if you're

220
00:23:05,680 --> 00:23:10,640
mindful of your addiction it's totally weak and then and atrophy those

221
00:23:10,640 --> 00:23:32,560
connections they're simplified I'm not a neuroscientist

222
00:23:32,560 --> 00:23:37,480
Greetings all I wanted to check if one can have tea coffee or avoid it

223
00:23:37,480 --> 00:23:41,600
completely sets it can also be in the category of chemicals and drugs leading to

224
00:23:41,600 --> 00:23:46,000
some addiction yeah it's not about leading to addiction I mean addiction is

225
00:23:46,000 --> 00:23:52,580
problematic but the the texts specifically mentioned alcohol the

226
00:23:52,580 --> 00:23:56,560
Buddha specifically mentions alcohol because alcohol is something you can

227
00:23:56,560 --> 00:24:03,040
clearly it's hard to deny that it changes the way you think you know it

228
00:24:03,040 --> 00:24:11,120
prevents you it makes it very difficult for you to be mindful so that's more of

229
00:24:11,120 --> 00:24:14,840
the question does this drug make it difficult or impossible for you to be

230
00:24:14,840 --> 00:24:21,680
mindful coffee doesn't really do that

231
00:24:21,680 --> 00:24:33,560
in certain other drugs may not cigarettes probably doesn't make it difficult so

232
00:24:33,560 --> 00:24:40,040
the difference take the difference between cigarettes and alcohol alcohol

233
00:24:40,040 --> 00:24:44,600
intoxicates really your drunk cigarettes don't do that so they're addictive

234
00:24:44,600 --> 00:24:48,720
they're bad but they're probably not in the same they're definitely not in the

235
00:24:48,720 --> 00:24:52,200
same categories alcohol so they're probably not to be considered under the

236
00:24:52,200 --> 00:24:56,200
precept now the thing you have to understand about precepts is their vows you

237
00:24:56,200 --> 00:25:01,200
take you can take a precept I I undertake not to take cigarettes or I undertake

238
00:25:01,200 --> 00:25:07,320
not to drink coffee that's your precept it's a good one there's reasons why

239
00:25:07,320 --> 00:25:12,440
but generally five precepts that the Buddha recommended I don't think

240
00:25:12,440 --> 00:25:19,760
include either caffeine or cigarettes so I'd just say that there's anything good

241
00:25:19,760 --> 00:25:29,240
about cigarettes and coffee is 50-50 but there's nothing good about gambling

242
00:25:29,240 --> 00:25:37,080
either and it's not in the five precepts so they're just rooms that you think

243
00:25:37,080 --> 00:25:41,760
I don't think there don't think these just don't think these things are in the

244
00:25:41,760 --> 00:25:48,800
category of alcohol anybody you have a couple more because I'm it's 10

245
00:25:48,800 --> 00:25:54,280
o'clock I have light and homework I still have to do there are a couple more

246
00:25:54,280 --> 00:25:57,480
did you want to put those off until tomorrow I go through them quickly it's not

247
00:25:57,480 --> 00:26:01,520
put oh you know okay because some more I've never done my practice so let's put

248
00:26:01,520 --> 00:26:06,080
them off put them off until tomorrow okay you can bookmark that sure

249
00:26:06,080 --> 00:26:11,800
okay thanks Robin thanks everyone good night thank you

250
00:26:11,800 --> 00:26:36,800
bante good night

