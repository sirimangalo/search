1
00:00:00,000 --> 00:00:10,680
How do you feel, Mr. Fett, it's difficult or easy, how do you feel after you practice

2
00:00:10,680 --> 00:00:17,400
it's difficult or easy, difficult to practice or easy, if you don't.

3
00:00:17,400 --> 00:00:33,800
You know why I let you walk for a long time and let you sit for a long time.

4
00:00:33,800 --> 00:00:43,760
I know it's difficult for you because all of you are students, you should know the proper way,

5
00:00:43,760 --> 00:00:52,920
in the correct way because if you know the proper way and correct way, one day you can

6
00:00:52,920 --> 00:01:00,920
keep instruction to other people, after you go home, I believe your friends will ask

7
00:01:00,920 --> 00:01:07,040
all of you that, what you did in Thailand, because you learned meditation at Wat Pahat

8
00:01:07,040 --> 00:01:15,840
second five, and what you are doing, what you did, what is your experience, so you can

9
00:01:15,840 --> 00:01:24,520
explain to your friends that when you are coming here, what it's all you, how can you give

10
00:01:24,520 --> 00:01:32,800
you instruction, I explain to all of you before you start to practice, right?

11
00:01:32,800 --> 00:01:41,400
I try to explain to all of you for details how to do, how to start, how to practice,

12
00:01:41,400 --> 00:01:51,200
how to walk, how to sit, because meditation practice, I told you, I said that anyone

13
00:01:51,200 --> 00:02:00,800
can practice anywhere, any day, any time, not just for Buddhist, any religion can teach

14
00:02:00,800 --> 00:02:09,320
and practice, but Buddhist only can teach in meditation, the other never happen, they cannot

15
00:02:09,320 --> 00:02:15,320
teach because they never have experience about meditation, you can say that.

16
00:02:15,320 --> 00:02:24,480
So you don't worry that, if you practice meditation, I am not sure you, maybe some of

17
00:02:24,480 --> 00:02:32,960
you, Christianity, almost whatever, you don't worry about your religions, you just tell

18
00:02:32,960 --> 00:02:42,880
yourself that, now you are coming here to learning how to practice meditation, I told

19
00:02:42,880 --> 00:02:51,760
all of you that meditation, anyone can practice anywhere, any day, any time, even though

20
00:02:51,760 --> 00:02:58,240
you are walking on a road, normally, you walk on your street, normally, you can practice,

21
00:02:58,240 --> 00:03:05,120
how to do, you just try to remind for every step you are stepping, right, left, right, right,

22
00:03:05,120 --> 00:03:11,120
left, that is meditation, and even though you are standing and waiting for the bus, when

23
00:03:11,120 --> 00:03:16,720
you go to school, you standing, you standing, standing, standing, standing, be aware your

24
00:03:16,720 --> 00:03:24,680
body is standing, it means you try to practice already, or even though you are sitting in

25
00:03:24,680 --> 00:03:33,480
the airplane, right, sitting for, you can practice, you see, now you know how to do, right,

26
00:03:33,480 --> 00:03:40,240
so when you are walking, you just, in order to walk, right, right, right, left, right,

27
00:03:40,240 --> 00:03:45,960
left, and then when you are sitting, you sit, whatever you can, whatever you like, you

28
00:03:45,960 --> 00:03:50,400
are sitting, that is in the airplane, right, sitting for you, right, sitting for you, if you

29
00:03:50,400 --> 00:03:59,480
sit, hip, hip, hip, hip, hip, hip, hip, and at home, tonight, when you go to bed, after

30
00:03:59,480 --> 00:04:05,320
you let out your body on your bed, you can meditate too, just after you let out your body

31
00:04:05,320 --> 00:04:09,840
on your bed, you put your hands on the abdomen, right, sitting for you, right, sitting

32
00:04:09,840 --> 00:04:18,120
for you, until you fall asleep, see, you can practice anywhere, any day, you don't want

33
00:04:18,120 --> 00:04:31,360
to get in, anywhere, any day, any time, okay, so today at least you know how to practice

34
00:04:31,360 --> 00:04:42,440
and you know it is not easy to practice meditation, right, but please try to practice continue

35
00:04:42,440 --> 00:04:51,360
as the after you go home, because you know meditation practice, if anyone can practice

36
00:04:51,360 --> 00:05:00,920
every day, or if 15 minutes a day, it is very, very good for all of you, because of when

37
00:05:00,920 --> 00:05:07,760
you practice meditation, it means you try to develop your concentration, try to develop

38
00:05:07,760 --> 00:05:14,040
your mindfulness, if you have good mindfulness, if you have good concentration, you can

39
00:05:14,040 --> 00:05:24,600
have good memory, your memory will be very good, you try only 15 minutes before you go

40
00:05:24,600 --> 00:05:32,280
to bed, you just sitting, rising, falling, or 10 minutes of 15 minutes, in the morning

41
00:05:32,280 --> 00:05:41,240
before you go to work, before you go to school, you try to sit in quite the above 15 minutes,

42
00:05:41,240 --> 00:05:48,040
you know, if you cannot remember, rising, falling, you just, you can try, you can take

43
00:05:48,040 --> 00:05:54,320
me like, you can count in your breathing, you sit in your chair, because in your country

44
00:05:54,320 --> 00:05:59,560
I know you, you don't want to sit in like this, it's difficult for you, but you can sit

45
00:05:59,560 --> 00:06:07,040
on the chair, or on your bed, and then sitting like this, or like this, breathing in, breathing

46
00:06:07,040 --> 00:06:15,960
out, in out one, in out two, in out three, in out four, out to ten, and we count again,

47
00:06:15,960 --> 00:06:23,400
about 15 minutes, and then you, if you do like that, it means you can develop your mindfulness,

48
00:06:23,400 --> 00:06:33,040
you can develop your concentration, and then you can have good memory, okay, anyone has

49
00:06:33,040 --> 00:06:34,040
question?

50
00:06:34,040 --> 00:06:46,920
No more question, nothing, please, yes, because you should walk in before you are sitting,

51
00:06:46,920 --> 00:06:53,920
because if you just sit in, it's more difficult for you to have good concentration, because

52
00:06:53,920 --> 00:07:00,800
when you are walking, it means you try to develop your concentration, you know, but you

53
00:07:00,800 --> 00:07:07,040
have to try to pay attention, standing, we are aware of what is standing, or when is

54
00:07:07,040 --> 00:07:14,160
the body standing, because that moment you try to slow down your thought, right, you

55
00:07:14,160 --> 00:07:22,280
know, or this moment I'm standing, so standing, standing, standing, you are not thinking,

56
00:07:22,280 --> 00:07:27,960
but if you just sit in, your mind is always run away, it's difficult for you to have good

57
00:07:27,960 --> 00:07:35,640
concentration, so that is the reason I explained to you about walking first, I show you

58
00:07:35,640 --> 00:07:43,400
how to walk first, but most of meditation that they are teaching in Europe or America,

59
00:07:43,400 --> 00:07:49,920
they don't want to walk, just sitting, and they use the sitting question, it's very

60
00:07:49,920 --> 00:07:57,360
affordable, because they don't like to have suffering, they don't want to have the pain,

61
00:07:57,360 --> 00:08:05,600
so they take sitting question high and very affordable, but it is not good, because if

62
00:08:05,600 --> 00:08:12,840
you don't see, if you don't have experience about suffering, you cannot have experience

63
00:08:12,840 --> 00:08:19,720
about, you don't understand what is suffering, so you should know, when you are sitting

64
00:08:19,720 --> 00:08:26,880
meditation, if you are expecting, how do you feel, and then not the pain pain pain, because

65
00:08:26,880 --> 00:08:36,240
even pain, many kind of pain, sometimes your legs look like the pain, but sometimes it's

66
00:08:36,240 --> 00:08:43,240
resting, rising, rising, and then disappear, sometimes increase, increase, increase, and

67
00:08:43,240 --> 00:08:51,160
disappear, sometimes pain, increase, increase, and stay, for a long time, now, disappear,

68
00:08:51,160 --> 00:08:58,640
so even is pain, it's staying, not disappear, you try to understand how do you feel, sometimes

69
00:08:58,640 --> 00:09:07,360
you do like a fire, burn, relax, sometimes you feel like your leg is broken, sometimes

70
00:09:07,360 --> 00:09:17,360
you feel like your old body is broken now, but it's not, see, so you try to understand,

71
00:09:17,360 --> 00:09:25,920
and you know, when you practice meditation, the biggest benefit you get is, you know

72
00:09:25,920 --> 00:09:36,720
how to give up your suffering, for example, one day, now you are happy, you don't, you

73
00:09:36,720 --> 00:09:43,840
don't know how to, how what is suffering, but one day if you have mental problem, if you

74
00:09:43,840 --> 00:09:50,400
broken heart, for example, if you broken heart, you will be sad, you will be upset, you

75
00:09:50,400 --> 00:09:57,720
have to try to understand that everything is impermanent, and then you can overcome your

76
00:09:57,720 --> 00:10:07,440
suffering, you can overcome your painful, okay, try to understand, okay, before we stop,

77
00:10:07,440 --> 00:10:14,440
I will lead you to the radiation of loving kindness, just repeat up to me, okay, because

78
00:10:14,440 --> 00:10:23,280
of why, you know, because of, usually when we practice in the temple, every time, every

79
00:10:23,280 --> 00:10:29,880
day, three times a day, after we finish meditating, we have to do radiation of loving

80
00:10:29,880 --> 00:10:36,080
kindness, because we can share our every kindness to other people, for yourself and for

81
00:10:36,080 --> 00:10:44,360
other people, do, do your hands like this, please, and repeat

82
00:10:44,360 --> 00:11:02,200
as a me, a hum, sukito, homie, may I be happy, need to hold me, may I be free from suffering,

83
00:11:02,200 --> 00:11:25,640
may I be free from enmity, may I be free from hurtfulness, may I be free from troubles of

84
00:11:25,640 --> 00:11:42,840
body and mind, sukhi atara, paryha ravi, may I be able to protect my own happiness, this

85
00:11:42,840 --> 00:11:55,680
is for yourself, and then for other people, sapeze atara, whatever beings they are, sukita,

86
00:11:55,680 --> 00:12:16,440
may I be happy, need to hold me, may I be free from suffering, may I be free from enmity,

87
00:12:16,440 --> 00:12:40,440
may I be free from hurtfulness, may I be free from troubles of body and mind, sukhi atara,

88
00:12:40,440 --> 00:12:52,600
paryha rantu, may I be able to protect their own happiness, this is radiation of loving

89
00:12:52,600 --> 00:12:59,960
kindness, and then I live in you to do the dedication of marriage, because, you know, when

90
00:12:59,960 --> 00:13:07,080
you are practiced meditation, it means you are doing biggestly marriage for your life,

91
00:13:07,080 --> 00:13:13,640
biggestly marriage for your life, so you should share your marriage to other people, just

92
00:13:13,640 --> 00:13:28,840
repeat after me, the marriage performed by me, I am happy to share with my parents, my

93
00:13:28,840 --> 00:13:45,760
grandfather, grandmother, teachers, preceptors, lecturers, everybody, no limit, please accept,

94
00:13:45,760 --> 00:14:03,200
and receive the marriage, be dedicated to you, if you are suffering, may be free from suffering,

95
00:14:03,200 --> 00:14:14,320
if you are happy, may you be happy, more and more and more, forever, see this is a dedication

96
00:14:14,320 --> 00:14:24,280
of marriage, so if you are practicing at home, you should do, you don't want to say

97
00:14:24,280 --> 00:14:35,040
it pary, because there is pary language, you can say in your language, you can say

98
00:14:35,040 --> 00:14:45,600
in your language, and you can say, like, I did it here, my parents, my father, my father,

99
00:14:45,600 --> 00:14:53,800
my grandmother, my mother, my dad, in your language, see, it's very good for you, and

100
00:14:53,800 --> 00:14:58,500
if you dedicate, if you read the action of loving kindness every day, every day, your

101
00:14:58,500 --> 00:15:04,680
heart will be full of loving, loving kindness, it's very good, if you go anywhere, you

102
00:15:04,680 --> 00:15:11,560
can be saved, because your heart, in your mind, full of loving kindness, I would like to

103
00:15:11,560 --> 00:15:18,000
give you blessing, okay, you've got your hands now, show your handpiece, I give you blessing,

104
00:15:18,000 --> 00:15:24,000
any kind of marriage performed by me, I'm happy to share or married, perform by me to all

105
00:15:24,000 --> 00:15:32,120
of you, may you have, like, what I have, may all of you be put a power, my power, and

106
00:15:32,120 --> 00:15:39,120
the sake of how will we protect all of you, may all of you have, for blessings like

107
00:15:39,120 --> 00:15:47,440
a long life, beauty, happiness, and life for ever, may you have put it in safety to your

108
00:15:47,440 --> 00:15:54,360
country, everybody, okay, thank you very, very much, and I think I say, probably again,

109
00:15:54,360 --> 00:16:04,080
thank you, and now all of you.

110
00:17:04,080 --> 00:17:17,620
The prosperous marade and to spirit.

111
00:17:17,620 --> 00:17:28,320
Aw come that shall sparkling the rod with a sedum booming on the pyriz they first

