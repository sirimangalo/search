1
00:00:00,000 --> 00:00:04,920
Okay, one rather simple question about eating and meditation.

2
00:00:04,920 --> 00:00:09,960
For example, I am doing something which will take me half an hour, or hour to finish.

3
00:00:09,960 --> 00:00:15,400
And after that I am thinking to do some meditation, but during this half hour I become hungry.

4
00:00:15,400 --> 00:00:20,960
Should I eat and when I finish doing what I am doing meditate, or not eat and keep working,

5
00:00:20,960 --> 00:00:27,320
then do the meditation on hungry stomach, or maybe just have a snack.

6
00:00:27,320 --> 00:00:35,800
It is not exactly a simple question, there is a lot in there, that is a bit, I am not quite

7
00:00:35,800 --> 00:00:43,880
clear on the whole of the question.

8
00:00:43,880 --> 00:00:50,040
But one thing I would say, which is kind of smurmy, is that your work should be meditation

9
00:00:50,040 --> 00:00:52,920
and your eating should be meditation.

10
00:00:52,920 --> 00:00:56,520
So that is really where your answer, that is the most important part of the answer, it is

11
00:00:56,520 --> 00:00:59,080
not really smurmy at all.

12
00:00:59,080 --> 00:01:09,640
It is probably not the answer that most people would expect, but the meditation starts now.

13
00:01:09,640 --> 00:01:14,880
It doesn't start 30 minutes from now when you are going to meditate, because then you are

14
00:01:14,880 --> 00:01:21,920
wasting 30 minutes thinking it is okay, in 30 minutes I will meditate, or after this I will

15
00:01:21,920 --> 00:01:23,920
meditate.

16
00:01:23,920 --> 00:01:30,160
And during all that 30 minutes or hour you are wasting good meditation time.

17
00:01:30,160 --> 00:01:34,880
So when you work you should try to be mindful, when you eat you should try to be mindful.

18
00:01:34,880 --> 00:01:38,080
You should work on that.

19
00:01:38,080 --> 00:01:45,680
Why it is not such a smurmy answer, after all is that that will really help your formal meditation.

20
00:01:45,680 --> 00:01:50,280
If a person just does formal meditation and doesn't think at all about meditation during

21
00:01:50,280 --> 00:01:56,920
their daily life, the formal meditation becomes quite difficult and it is a constant uphill

22
00:01:56,920 --> 00:02:02,360
struggle, because it is out of tune with your ordinary reality.

23
00:02:02,360 --> 00:02:07,240
So you should try to constantly be bringing your mind back to attention, I mean even right

24
00:02:07,240 --> 00:02:15,200
now here we are sitting together, we should try to make it a mindful experience, being

25
00:02:15,200 --> 00:02:22,400
aware at least of our emotions, likes and dislikes, the distractions in the mind, the feelings

26
00:02:22,400 --> 00:02:25,640
that we have.

27
00:02:25,640 --> 00:02:30,080
If you are just listening here, because I am talking I have to do more action, but for some

28
00:02:30,080 --> 00:02:41,600
of you just listening you can just revert back to your meditation, unless you have questions.

29
00:02:41,600 --> 00:02:45,520
But as far as me making decisions as to when you should do eating and when you should

30
00:02:45,520 --> 00:02:52,520
do meditating, meditating on an empty stomach is interesting, can be useful, to help

31
00:02:52,520 --> 00:02:59,080
you to learn about hunger.

32
00:02:59,080 --> 00:03:05,640
But I'd say generally you need the food for the energy for the meditation, so better

33
00:03:05,640 --> 00:03:09,040
to have a little bit of food before you meditate.

34
00:03:09,040 --> 00:03:14,480
I don't meditate any time, whenever you meditate, meditate, get some time for it and do

35
00:03:14,480 --> 00:03:19,920
it, just don't procrastinate, it kind of sounds like you might be talking about procrastinating,

36
00:03:19,920 --> 00:03:29,240
which is dangerous, so it goes work for a half an hour and then I'll do, then I'll have

37
00:03:29,240 --> 00:03:32,440
a snack and then when I'm having a snack I think about all, then I have to go check

38
00:03:32,440 --> 00:03:37,520
Facebook or email and check email and then something comes up in email and I have to call

39
00:03:37,520 --> 00:03:42,000
this person or that person or do this or I have to go to the store and then you never

40
00:03:42,000 --> 00:03:45,080
meditate after all.

41
00:03:45,080 --> 00:03:53,280
That kind of thing is dangerous, so better to be clear with yourself that if you really

42
00:03:53,280 --> 00:04:03,200
need food and you're really hungry than eat or eat a little bit, but if you don't sometimes

43
00:04:03,200 --> 00:04:09,880
it's just procrastinating because meditation can be difficult and the mind will actually

44
00:04:09,880 --> 00:04:18,200
develop a version towards it if you're not careful and if you're not aware of the aversion

45
00:04:18,200 --> 00:04:23,200
if you're not looking at it and contemplating it as well, it can become a great hindrance

46
00:04:23,200 --> 00:04:28,800
and more than a hindrance it can actually trick you into every time you think of meditation

47
00:04:28,800 --> 00:04:33,000
your mind says, oh he's thinking of meditation again, let's divert his attention, hey

48
00:04:33,000 --> 00:04:42,000
look at that, that's nice, hey aren't you hungry, hey I want to go for burgers or something,

49
00:04:42,000 --> 00:04:47,280
my mind will do this to you because it doesn't want amenities like oh no don't put me back

50
00:04:47,280 --> 00:05:16,720
there, that's torture, so be careful of that one.

