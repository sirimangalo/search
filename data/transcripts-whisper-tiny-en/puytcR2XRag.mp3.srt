1
00:00:00,000 --> 00:00:05,000
Hi, next question comes from Jillian Nep.

2
00:00:05,000 --> 00:00:12,000
I'm a beginner to Buddhism and am in fact still doing my initial research on it.

3
00:00:12,000 --> 00:00:16,000
I would like to get a balanced view of the negative aspects of Buddhism.

4
00:00:16,000 --> 00:00:24,000
What are some of the things that have derailed both laypersons and monastics?

5
00:00:24,000 --> 00:00:30,000
Yeah, it's not often I get asked what are the negative aspects of my religion.

6
00:00:30,000 --> 00:00:33,000
I don't think there are any, honestly.

7
00:00:33,000 --> 00:00:44,000
Although I suppose I've gone over some of the apparent negative aspects of Buddhism in the sense of not being much funded parties,

8
00:00:44,000 --> 00:00:53,000
not being able to get along in society, having to segregate yourself, having to be careful.

9
00:00:53,000 --> 00:00:59,000
There's a lot of difficulty that comes from trying to be pure.

10
00:00:59,000 --> 00:01:11,000
It's not easy to purify your mind and to be solely a good person and to do away with all evil in the mind, which is really what we're trying to do.

11
00:01:11,000 --> 00:01:19,000
There are many people who would say that's impossible, mostly because they've never practiced meditation.

12
00:01:19,000 --> 00:01:26,000
So they would consider that to be a negative aspect of Buddhism.

13
00:01:26,000 --> 00:01:33,000
But I can't think of any, obviously I don't think there are any negative aspects to Buddhism.

14
00:01:33,000 --> 00:01:35,000
You're kind of asking two questions.

15
00:01:35,000 --> 00:01:46,000
The other one is, or I don't know if you're aiming at, but as far as what stops people from practicing Buddhism correctly,

16
00:01:46,000 --> 00:01:49,000
I guess that's the answer.

17
00:01:49,000 --> 00:01:55,000
There's nothing wrong with Buddhism, but people just don't understand it or don't practice it correctly.

18
00:01:55,000 --> 00:02:05,000
What stops people from practicing correctly, obviously our preconceived notions of what is right and what is wrong,

19
00:02:05,000 --> 00:02:09,000
which are not really based on truth and reality.

20
00:02:09,000 --> 00:02:28,000
What stops people from making progress, according to the Buddha, there are certain things that get in the way and prevent a person's progress as a lay person and as a monk.

21
00:02:28,000 --> 00:02:34,000
So maybe I'm not sure if this is exactly what you're asking, but I'll take the opportunity to talk about some of these things.

22
00:02:34,000 --> 00:02:40,000
For lay people, the first one is there's a requirement for faith.

23
00:02:40,000 --> 00:02:48,000
If people don't have a faith and confidence in what they're doing, it makes it very difficult for them to continue.

24
00:02:48,000 --> 00:02:55,000
Because obviously for most or for many lay people there's not the opportunity to realize all of the Buddha's teaching,

25
00:02:55,000 --> 00:02:57,000
even to study all of the Buddha's teachings.

26
00:02:57,000 --> 00:03:05,000
So there has to be some level of confidence in your leaders, in the religion.

27
00:03:05,000 --> 00:03:13,000
If you don't believe in the principles of Buddhism, it can be very difficult for you to progress.

28
00:03:13,000 --> 00:03:24,000
And I guess what that means really is if you have ideas that are contrary, then you won't even open your mind to the idea that the Buddha's teaching might be right.

29
00:03:24,000 --> 00:03:28,000
And I recognize this in many so-called Buddhists.

30
00:03:28,000 --> 00:03:40,000
They still believe in a delicious meal and preference for things in beauty, in sexuality and so on.

31
00:03:40,000 --> 00:03:46,000
Not only are they taking it, but they also believe that it's right to be attached to these things.

32
00:03:46,000 --> 00:03:51,000
And these are people who consider themselves to be Buddhists, even from birth.

33
00:03:51,000 --> 00:03:54,000
And that can be a real hindrance in the practice.

34
00:03:54,000 --> 00:03:57,000
The second one is morality.

35
00:03:57,000 --> 00:04:02,000
Obviously if you're an immoral person, it's very difficult to practice the Buddha's teaching.

36
00:04:02,000 --> 00:04:07,000
The third one is a belief in superstition.

37
00:04:07,000 --> 00:04:14,000
And this is common throughout the Buddhist world and the world in its entirety.

38
00:04:14,000 --> 00:04:20,000
Because the Buddha taught cause and effect, when people have different ideas of what is cause and what is effect,

39
00:04:20,000 --> 00:04:23,000
in terms of magical causes.

40
00:04:23,000 --> 00:04:26,000
If you wear this around your neck, somehow it's going to have a benefit.

41
00:04:26,000 --> 00:04:33,000
If you say this or say that, and somehow there's going to be a negative effect and so on.

42
00:04:33,000 --> 00:04:45,000
The belief in superstition, because it's contrary to the idea of karma and cause and effect, is another hindrance to, especially to lay people.

43
00:04:45,000 --> 00:04:56,000
Because they're not so close to the teaching, so they hear about this witch doctor or a fortune teller or so on.

44
00:04:56,000 --> 00:05:01,000
And they can get mixed up in terms of what is the Buddha's teaching.

45
00:05:01,000 --> 00:05:09,000
The fourth one is getting involved in other religions, and it's related to the last one.

46
00:05:09,000 --> 00:05:20,000
When people go in support, other religions, other religious doctrines, in terms of paying respect to these teachers,

47
00:05:20,000 --> 00:05:24,000
listening to what they have to say, and trying to get their opinion.

48
00:05:24,000 --> 00:05:33,000
Because they have a whole other outlook on life, and it confuses people.

49
00:05:33,000 --> 00:05:39,000
People say things like, all religions teach the same thing or so on, all religions are the same.

50
00:05:39,000 --> 00:05:45,000
It doesn't matter what religion you are, and so they will respect the teachings of all religions.

51
00:05:45,000 --> 00:05:49,000
It's not that we don't, in a sense, respect these teachings.

52
00:05:49,000 --> 00:05:53,000
In terms of, you know, you want to practice that way, that's fine, you believe this, that's fine.

53
00:05:53,000 --> 00:05:59,000
But, you know, to accept that belief is proper, that it actually is beneficial.

54
00:05:59,000 --> 00:06:06,000
You know, when those beliefs go against, the understanding of the Buddha is to what is right and what is wrong,

55
00:06:06,000 --> 00:06:09,000
can be hindrance to one's practice.

56
00:06:09,000 --> 00:06:18,000
Because if there is a conflict, and the final one is to support the Buddha's teaching.

57
00:06:18,000 --> 00:06:25,000
So to not spend time supporting teachings which are contrary to the Buddha's teaching,

58
00:06:25,000 --> 00:06:33,000
and to actually engage in supporting the Buddhist religion in terms of material support,

59
00:06:33,000 --> 00:06:40,000
in terms of, you know, spiritual support, and spreading the teachings, and so on.

60
00:06:40,000 --> 00:06:47,000
These, you know, when we fail to do that, and when we get involved in other religions,

61
00:06:47,000 --> 00:06:53,000
these are two things the Buddha said, you know, cause you to move away from the Buddha's teaching.

62
00:06:53,000 --> 00:07:01,000
Those things which are a hindrance or cause monks to get to become derailed, as you say,

63
00:07:01,000 --> 00:07:07,000
are the Buddha had four things that he said, especially for new monks,

64
00:07:07,000 --> 00:07:09,000
are going to be a real hindrance.

65
00:07:09,000 --> 00:07:14,000
And this is sort of an addition to what I was saying about how to become a monk earlier.

66
00:07:14,000 --> 00:07:18,000
I think it's useful to know about these as well.

67
00:07:18,000 --> 00:07:28,000
And the first one is not being able to stand the teachings or instruction,

68
00:07:28,000 --> 00:07:32,000
not being able to bear or being instructed, being told what to do.

69
00:07:32,000 --> 00:07:35,000
This is very common for new monks.

70
00:07:35,000 --> 00:07:38,000
When they're told they have to do this and have to do that,

71
00:07:38,000 --> 00:07:45,000
it's very easy for them to become angry and frustrated when they don't understand and don't agree.

72
00:07:45,000 --> 00:07:55,000
They aren't able to follow along because of their own ideas, their own views of what is right and what is wrong.

73
00:07:55,000 --> 00:08:03,000
Makes it very difficult for them to do things like walking back and forth or, you know, sitting for long periods of time.

74
00:08:03,000 --> 00:08:08,000
When we tell meditators they have to sit through the pain instead of trying to move around all the time.

75
00:08:08,000 --> 00:08:10,000
This can be very difficult for some people.

76
00:08:10,000 --> 00:08:11,000
They don't agree with it.

77
00:08:11,000 --> 00:08:19,000
When we tell people they have to let go, they can't cling and they can't chase after when we tell them not to, you know,

78
00:08:19,000 --> 00:08:23,000
not to make eye contact or look around or wonder or move quickly or so.

79
00:08:23,000 --> 00:08:35,000
It can be very difficult for people, especially for monks and for meditators who have come to the meditation center to stay.

80
00:08:35,000 --> 00:08:48,000
The second one is being addicted to, addicted to, addicted to simple,

81
00:08:48,000 --> 00:08:53,000
being lazy, I guess, is a good explanation of it.

82
00:08:53,000 --> 00:09:03,000
Being addicted to just eating and sleeping and lazing around laziness, I guess, is a good summary of it.

83
00:09:03,000 --> 00:09:07,000
Meditation in the Buddha's teaching does take a lot of effort.

84
00:09:07,000 --> 00:09:19,000
It's something that you have to work hard at because we're trying to change the very core of our being, the very core of the very core of how we look at the world,

85
00:09:19,000 --> 00:09:21,000
of how we see things.

86
00:09:21,000 --> 00:09:25,000
We're trying to change the way we look entirely, change the way we look at things.

87
00:09:25,000 --> 00:09:31,000
And this takes a lot of effort because we're so generally so off track.

88
00:09:31,000 --> 00:09:45,000
If we are lazy, if we think we can just sit around and eat and sleep and socialize and think that being a monk or being a meditator is somehow enough.

89
00:09:45,000 --> 00:09:51,000
If you come to stay at the monastery somehow, you're gaining something without even practicing.

90
00:09:51,000 --> 00:10:04,000
This can make it very difficult and it does in the end, lead one to feel like being a monk or being a meditator's pointless, going home and not gaining any benefit from it.

91
00:10:04,000 --> 00:10:11,000
The third one is being attached to happiness, being attached to central pleasures.

92
00:10:11,000 --> 00:10:24,000
So, you know, needing good food, needing nice clothing, needing a soft bed and so on, needing to listen to music and watch television.

93
00:10:24,000 --> 00:10:29,000
All of these things that monks are not allowed, monks and meditators are not allowed to do.

94
00:10:29,000 --> 00:10:40,000
When these addictions come up, if they're very strong, they can very easily lead one to become derailed and lose interest in the practice.

95
00:10:40,000 --> 00:10:44,000
Because of your one's addictions, one's attachments.

96
00:10:44,000 --> 00:10:53,000
When these come up, it leads to boredom, it leads to disinterest in reality and meditation, wanting something else, wanting more.

97
00:10:53,000 --> 00:11:02,000
And eventually leads one to leave the monks' life or to leave the meditation center.

98
00:11:02,000 --> 00:11:11,000
And the fourth one, specifically for monks, they say, is love for a woman.

99
00:11:11,000 --> 00:11:21,000
But I think it can easily, obviously, be flipped to talking about a female or even just a meditator in general.

100
00:11:21,000 --> 00:11:25,000
The sexuality, obviously, is a very strong attachment.

101
00:11:25,000 --> 00:11:48,000
And it's the primary reason why monks just wrote when the woman comes to the monastery and they fall in love and the monk decides that he's had enough and he's found a more desirable or more pleasurable path to follow.

102
00:11:48,000 --> 00:11:57,000
And this is the fourth danger. These dangers are something that one has to watch out for, especially this last one, have to be very careful about.

103
00:11:57,000 --> 00:12:10,000
I think it all depends on your appreciation of the Buddha's teaching and your appreciation of the life of the Buddha laid out for us.

104
00:12:10,000 --> 00:12:26,000
If you really believe in what the Buddha taught and if you really see the benefit in what the Buddha taught and if it really does bring you benefit, then I think these dangers are easily overcome.

105
00:12:26,000 --> 00:12:44,000
And the way you overcome them is by seeing the benefit, by understanding that what we're doing is something great, something powerful, something pure and something higher than all of these other desires, these other attachments, these other needs and wants.

106
00:12:44,000 --> 00:13:11,000
A higher than our views and our opinions, that what the Buddha taught is very much real. And the more we appreciate, the more we study and the more we practice the Buddha's teaching, the less chance there is that will become derailed, either as lay people or as monks as lay people will feel more and more attracted to helping and supporting, learning, studying and appreciating and following the Buddha's teaching.

107
00:13:11,000 --> 00:13:24,000
Thanks as well, we'll be able to overcome all adversity and all of our prior addictions and attachments and wants and needs and so on.

108
00:13:24,000 --> 00:13:44,000
So I hope that comes at least close to answering your question, thanks for giving me a chance to talk about these subjects, which I think are very important for Buddhists. Okay, that's all.

