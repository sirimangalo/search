1
00:00:00,000 --> 00:00:07,240
I am welcome back to our study of the Dhamapada. Today we continue on with verse number 60

2
00:00:07,240 --> 00:00:18,300
read story forms. Deega, jågaratåratte en ge drum yhhh

3
00:00:18,300 --> 00:00:10,520
Outdjnam. Deego Bállånes, au-sa shopra, sang sawro, sán good themung

4
00:00:10,520 --> 00:00:36,060
ag satisfaction. which means Deegatråhatå heartåratte. The night is long

5
00:00:36,060 --> 00:00:49,340
long for one who is alert or awake, one who is full of energy.

6
00:00:49,340 --> 00:01:01,580
Dijang Santasayu is in a league is long for one who is tired, who is weary.

7
00:01:01,580 --> 00:01:13,540
Virgobalana samsadu, samsara is long, wandering on is long for fools, satamamamam

8
00:01:13,540 --> 00:01:21,300
avijanatam who don't, not knowing the satam or the good dhamma, so we have three types

9
00:01:21,300 --> 00:01:27,460
of things that are long.

10
00:01:27,460 --> 00:01:40,100
The story behind this verse is regarding a certain man, but actually more regarding

11
00:01:40,100 --> 00:01:46,940
of the King Visainadi, but I think the name of it is Annette that Apurisa, what a story

12
00:01:46,940 --> 00:01:50,020
about, some man or other.

13
00:01:50,020 --> 00:01:55,260
The story goes, there was this man who had a very, very beautiful life that he kept

14
00:01:55,260 --> 00:02:04,260
up on the top floor of his house or whatever she was in the house and there was a festival

15
00:02:04,260 --> 00:02:13,580
and King Visainadi, when riding on his white elephant through the city, there are sunwise

16
00:02:13,580 --> 00:02:28,580
it says whatever that means, maybe that would be clockwise and as he was completing

17
00:02:28,580 --> 00:02:36,140
his journey around the city, he looked up on his elephant and saw this woman standing

18
00:02:36,140 --> 00:02:44,660
in the window at the top floor of her house and she stood there for a second and he looked

19
00:02:44,660 --> 00:02:56,060
at her and then she went back into her house and he was totally enchanted by this completely

20
00:02:56,060 --> 00:03:03,420
enchanted and taken away by this woman's beauty and vowed that she should be his wife,

21
00:03:03,420 --> 00:03:07,460
his queen.

22
00:03:07,460 --> 00:03:13,180
So he completed his trip around the city, went back to the palace and he called his

23
00:03:13,180 --> 00:03:18,900
minister up to see him, one of his attendants and said, did you see that woman there,

24
00:03:18,900 --> 00:03:22,140
near the end of the trip, he said, yes, I saw the woman.

25
00:03:22,140 --> 00:03:27,420
Go and find out if she has a husband and so they sent a man to the house and to go

26
00:03:27,420 --> 00:03:30,460
and find out what was up with this woman.

27
00:03:30,460 --> 00:03:37,980
Come back and says, yes, she's got a husband and so he says to him, okay, bring her, bring

28
00:03:37,980 --> 00:03:49,740
her husband here and they have the husband brought before the king and the husband somehow

29
00:03:49,740 --> 00:03:53,100
knows I think I can't remember exactly how it was really knows, he gets an idea because

30
00:03:53,100 --> 00:03:58,780
he knows how beautiful his wife is and so he knows there's something going on and he goes

31
00:03:58,780 --> 00:04:06,380
before the king and the king says, from now on I want you to be my servant and he says,

32
00:04:06,380 --> 00:04:11,940
oh, but I'm quite happy doing the work that I do in your majesty.

33
00:04:11,940 --> 00:04:16,220
Well, regardless from now on from this day forward you are to be my servant, of course

34
00:04:16,220 --> 00:04:21,620
if you don't listen to the king, he can cut off your head and so the king thinks to

35
00:04:21,620 --> 00:04:30,260
himself that he'll find some way to find fault in this man and cut off his head and take

36
00:04:30,260 --> 00:04:37,020
this guy's wife and he spends a lot of time trying to do that, he has him go on dangerous

37
00:04:37,020 --> 00:04:43,300
errands and fight in battles but he's so meticulous and so concerned and he's so aware

38
00:04:43,300 --> 00:04:50,060
that the king is out to get him, that he performs his job flawlessly and the king is

39
00:04:50,060 --> 00:04:53,300
unable to find any fault in him.

40
00:04:53,300 --> 00:05:01,300
So the king finds finally, he finds this scheme, he sends him on an errand to get, he says

41
00:05:01,300 --> 00:05:10,900
I'm going to bathe and I want this special red play and I think lotus flowers as well,

42
00:05:10,900 --> 00:05:18,620
white lotus flowers that can only be got in a location that is one yojina away, one

43
00:05:18,620 --> 00:05:22,620
yojina is about 12 kilometers.

44
00:05:22,620 --> 00:05:28,020
So he says, and I wanted there before my bathe which is in like an hour or two hours

45
00:05:28,020 --> 00:05:33,900
or not two hours but it's in a short time, short that he knows it would be very difficult

46
00:05:33,900 --> 00:05:34,900
to make it.

47
00:05:34,900 --> 00:05:43,460
So he goes back to his home and he gets his wife to make him some, or his wife is making

48
00:05:43,460 --> 00:05:46,700
the food, he says it's the rice ready and she said no it's not cooked yet so he takes

49
00:05:46,700 --> 00:05:50,700
some half cooked rice and puts it in a pot and carries it with him and it cooks well

50
00:05:50,700 --> 00:06:01,980
he's on the journey, he's running 12 kilometers and he gets to this place and I'd like

51
00:06:01,980 --> 00:06:09,740
to just say he finds the clay and the lotus flowers but it's not that easy, the story

52
00:06:09,740 --> 00:06:24,980
goes and so let's say the un-elaborated or un-extrapulated, no, what's the un, what do you

53
00:06:24,980 --> 00:06:32,740
call when you're in something you're not elaborate but you make it more fancy, exaggerated,

54
00:06:32,740 --> 00:06:37,820
the unexaggerated story, I think that's sort of, it's not, I think it's not but anyway,

55
00:06:37,820 --> 00:06:42,260
the un-exaggerated story is there, he got the clay and the lotus flowers that came back

56
00:06:42,260 --> 00:06:47,820
but the story in the down the five of goes, that the clay and the lotus flowers were

57
00:06:47,820 --> 00:06:57,020
regarded by dragons and so he knows this and he knows it's important, it's a neat story

58
00:06:57,020 --> 00:07:06,220
even though I'm not asking you to believe necessarily that this is what happened because

59
00:07:06,220 --> 00:07:10,180
he knew that this was going to be something very, very difficult to get and so he sat

60
00:07:10,180 --> 00:07:17,460
down to eat his rice and he took the best part of his meal and put it aside and he ate

61
00:07:17,460 --> 00:07:23,900
the rest or most of the rest and then a traveler was, he saw a traveler by the side, he was

62
00:07:23,900 --> 00:07:28,460
sitting by the side, he saw a traveler going by and he said, new friend, come here, I have

63
00:07:28,460 --> 00:07:34,860
set aside the best portion of my meal, please partake of this and he says, oh wow, that's

64
00:07:34,860 --> 00:07:41,580
very nice of you and he takes the food, eats the food and goes on his way and our hero

65
00:07:41,580 --> 00:07:46,460
takes the rest of the rice and a little bit that's left and he throws it into the water

66
00:07:46,460 --> 00:07:51,340
and then he makes a determination, he said, now I have done great merit a thousand times

67
00:07:51,340 --> 00:07:56,780
merit by giving to someone, giving my food to someone and I've done a hundred, gotten

68
00:07:56,780 --> 00:08:02,980
a hundred times merit for giving food to the fishes in the water.

69
00:08:02,980 --> 00:08:08,460
By the power of this merit, may I be worthy to receive this special red clay and these

70
00:08:08,460 --> 00:08:12,900
white lotus flowers, king of the dragon comes along and I can't remember this talk about

71
00:08:12,900 --> 00:08:18,420
something, forgot what it is and somehow he gets these lotuses, the thing is he gets the

72
00:08:18,420 --> 00:08:22,860
clay and the lotuses comes back to the king and the king thinking that he might be able

73
00:08:22,860 --> 00:08:28,740
to somehow actually get the clay and the lotuses from these dragons against all odds has

74
00:08:28,740 --> 00:08:33,380
them close to the gates to the city early that day, so he's got no hope and he stands

75
00:08:33,380 --> 00:08:38,740
outside the gates of the city and he throws the clay down and he throws the lotuses

76
00:08:38,740 --> 00:08:45,740
down and he says, I have brought these as I was required, the king and he shouts out that

77
00:08:45,740 --> 00:08:55,460
the king has framed me, set me up to be killed and no one heard him so he leaves them

78
00:08:55,460 --> 00:08:59,860
there and in fear of his life he goes to the monastery and he says, where will I go,

79
00:08:59,860 --> 00:09:07,420
oh, I'll go hang out with the monks, I mean duh, where do you go when you got nowhere

80
00:09:07,420 --> 00:09:08,420
else to go?

81
00:09:08,420 --> 00:09:16,500
He goes to the monastery and he falls asleep at the monastery and some corner of what would

82
00:09:16,500 --> 00:09:20,700
be Jita one, I guess.

83
00:09:20,700 --> 00:09:27,860
The king, having bathed and having gone back into his room, lay down and tried to sleep

84
00:09:27,860 --> 00:09:33,580
but found that he couldn't sleep, so he was up all night tossing and turning, thinking

85
00:09:33,580 --> 00:09:38,460
about this woman and how in the morning he was going to kill this man and take his wife

86
00:09:38,460 --> 00:09:46,580
and he was so full of this passion and just just rearing to go and that he couldn't sleep

87
00:09:46,580 --> 00:09:52,020
and so he was up all night and then in the morning the sun came up, thinking he had gotten

88
00:09:52,020 --> 00:09:57,620
no sleep, he, I know, so near the end of the night, just before the sun was going to come

89
00:09:57,620 --> 00:10:05,980
up, suddenly he heard a noise, did anyone hear this, heard this story, what noise he heard?

90
00:10:05,980 --> 00:10:32,780
Do sa, na, so four voices, four different voices he heard, do sa, na, so like that, it gave

91
00:10:32,780 --> 00:10:45,340
him the hippie geebies, imagine your king protected by all these, all your guards and fortifications

92
00:10:45,340 --> 00:10:52,380
and suddenly in the dark of the night you hear these four words, he freaked out, so in

93
00:10:52,380 --> 00:10:57,580
the morning he totally forgot about this man and he calls his ministers and he says look

94
00:10:57,580 --> 00:11:02,340
something really strange happened last night, I heard these three sounds, or these four

95
00:11:02,340 --> 00:11:12,420
sounds, or the four sounds, do sa, na, so and the brahmanas are like oh yeah we know

96
00:11:12,420 --> 00:11:17,300
what that means, that means you have to kill lots of animals, they had an occluded

97
00:11:17,300 --> 00:11:24,180
man but to hide their confusion they said of course of course yes this is terrible, your

98
00:11:24,180 --> 00:11:30,580
majesty you're going to die, is there any way of avoiding it, oh yes yes of course there

99
00:11:30,580 --> 00:11:37,940
is and you can count on us to know the cure, it's you must get 500 bulls and 500 horses

100
00:11:37,940 --> 00:11:46,300
and 500 sheep and 500 goats and kill them all and that of course will save your life,

101
00:11:46,300 --> 00:11:52,460
it's just how physics works, let's see what the thoughts of physics, there's an equation

102
00:11:52,460 --> 00:12:03,020
for it, 500 x plus 500 y and that's it and the king, his vicinity is not known for his

103
00:12:03,020 --> 00:12:14,300
erudition, is that a word, his smarts, neither am I, and so he believes them, I think

104
00:12:14,300 --> 00:12:17,820
there's them all up and everyone's going crazy trying to, he's just stealing people, I think

105
00:12:17,820 --> 00:12:23,180
it just confiscates people's livestock and everyone's so upset and getting in a big

106
00:12:23,180 --> 00:12:28,940
waffle and Malika she sees all this craziness going on Malika says queen and she goes to

107
00:12:28,940 --> 00:12:35,020
king vicinity and she says your majesty what's going on, oh my dear I was the most horrible

108
00:12:35,020 --> 00:12:44,540
thing, I heard these four noises in the night and in order to save my life these four deadly

109
00:12:44,540 --> 00:12:51,340
noises that are just going to somehow kill me and in order to save my life I've had to

110
00:12:51,340 --> 00:12:59,420
arrange a sacrifice, a great sacrifice of 1,000 2,000 animals and Malika looks at him and says

111
00:12:59,420 --> 00:13:10,780
you're an idiot, he says you're a simpleton, let me stay in back and he says okay what do you mean

112
00:13:10,780 --> 00:13:21,660
by that, and she says you heard four sounds and these these brahmins tell you that you're

113
00:13:21,660 --> 00:13:25,260
going to die and that you have to sacrifice, I mean what is the relationship between these four

114
00:13:25,260 --> 00:13:30,140
sounds and killing 2,000 animals, do you really see some sort of connection there, do you

115
00:13:30,140 --> 00:13:38,140
really, are you really that gullible? And he says well what do you think it means?

116
00:13:38,140 --> 00:13:41,900
He says I haven't a clue, it didn't mean but at least I'm ready to admit it, those brahmins

117
00:13:41,900 --> 00:13:46,380
haven't got a clue either, they're just making stuff up because it makes the money to perform

118
00:13:46,380 --> 00:13:52,220
these sacrifices. And he said well then what would you have me do? He says well find someone who

119
00:13:52,220 --> 00:13:58,940
knows the answer, well who knows the answer, well the Jata Wanna Buddha, I've been here with this guy

120
00:14:00,940 --> 00:14:06,940
and it's like you're right, I should go to see the fully enlightened Buddha and then so he says

121
00:14:06,940 --> 00:14:10,540
but you must come with me and so they go together, I think it's on his white elephant and

122
00:14:11,420 --> 00:14:19,820
she gets on her pink elephant and they go together to Jata Wanna and step down from their

123
00:14:19,820 --> 00:14:26,940
elephants and go into the monastery and go before the Buddha and sit down and pay respect to him

124
00:14:28,140 --> 00:14:33,660
and Buddha says so for what reason have you come your majesty and the king is so frightened

125
00:14:33,660 --> 00:14:38,380
and he doesn't talk, he doesn't say anything, so Malika says Malika speaks up and says

126
00:14:40,300 --> 00:14:51,180
this guy over here, he had saw these four sounds and then the brahmins told him

127
00:14:51,180 --> 00:14:57,660
Dalada Dada and he decided to do to follow along after them, please tell us of an herbal

128
00:14:57,660 --> 00:15:02,140
server, what is the meaning of the noises and so he says to King Pascinity what were the four noises

129
00:15:02,140 --> 00:15:11,020
and Pascinity says do sah nah so that's what I heard I think something like that

130
00:15:12,380 --> 00:15:18,540
and the Buddha says ah there's relax there's no danger to you your majesty

131
00:15:20,620 --> 00:15:25,420
what you heard well let me tell you a story he says

132
00:15:25,420 --> 00:15:33,980
sometime in the long in the in the in the past in the time of the Buddha Kasapah

133
00:15:35,500 --> 00:15:37,980
there were these four men and

134
00:15:40,140 --> 00:15:47,100
while everyone else was doing great good deeds and and performing wholesome acts

135
00:15:48,060 --> 00:15:52,780
giving gifts and keeping morality and practicing meditation and doing good things in the Buddha's

136
00:15:52,780 --> 00:15:59,740
dispensation these four guys looked at them and said man that's that's really tiresome to

137
00:15:59,740 --> 00:16:07,180
be engaged in wholesome acts like that helping other people these guys were also very rich

138
00:16:07,180 --> 00:16:13,100
and so they had lots and lots of money I think they had 100 goat decent gold or something

139
00:16:13,100 --> 00:16:17,900
they had a lot of money and so they sat down they said well what should we do with our money

140
00:16:17,900 --> 00:16:23,900
and one of them said well let's get lots of the choices rice is

141
00:16:25,180 --> 00:16:32,380
three that's the only three-year-old rice and curry or special fermented rice or aged rice somehow

142
00:16:34,060 --> 00:16:38,700
and then one said let's just have the nicest curries and let's just listen that in the fourth

143
00:16:38,700 --> 00:16:43,740
guy says he says you guys all have no imagination here's what we're going to do he says

144
00:16:43,740 --> 00:16:49,180
there is no woman out there I think this is what he says this is not Buddhist Buddhism this is

145
00:16:49,180 --> 00:16:53,500
the bad stuff so there's no woman out there who won't cheat on her husband for money

146
00:16:55,420 --> 00:17:00,940
so here's what we'll do we'll go and offer money to all the beautiful women in all the beautiful

147
00:17:00,940 --> 00:17:09,740
wives in the city and we'll commit adultery with them we'll bribe them to to to come and run away with

148
00:17:09,740 --> 00:17:18,700
us where they feel like oh that sounds like a good idea and so they decide that's what they're

149
00:17:18,700 --> 00:17:29,740
going to do with their money and they spend all their money on women and when they passed away

150
00:17:29,740 --> 00:17:40,460
they fell into they appeared into their body disappeared the course body disappears and they're

151
00:17:40,460 --> 00:17:49,100
reborn with a with a fine body in a boiling cauldron called the great cauldron I think

152
00:17:49,100 --> 00:17:54,620
is something the iron cauldron right it's the hell called the iron cauldron where it's full of

153
00:17:54,620 --> 00:18:01,660
molten lava maybe or just maybe water boiling oil maybe can't remember boiling oil maybe

154
00:18:03,900 --> 00:18:11,900
and it takes 30,000 I'm making this up I'm I'm not quite sure 30,000 years

155
00:18:13,500 --> 00:18:20,620
for them to sink to the bottom of the cauldron and then 30,000 years to float back up

156
00:18:20,620 --> 00:18:30,380
and then they take one breath and then they go back down and back up and they've been there

157
00:18:30,380 --> 00:18:36,860
for they've been there since the time of the buddha Kasapau which I don't know how long ago that

158
00:18:36,860 --> 00:18:42,220
was so they say there's a time frame a long long time and they're going to be there until the

159
00:18:42,220 --> 00:18:55,500
karma the results of their bad karma is expedited I said so what you heard is you just caught

160
00:18:55,500 --> 00:19:00,620
them at the moment when they were all at the top of the cauldron and they wanted to say something

161
00:19:00,620 --> 00:19:09,180
to you but they couldn't they wanted to each say a verse but all they could get out was the first

162
00:19:09,180 --> 00:19:18,860
the first syllable so he said the man who said do that was doji vita we have did the bad life

163
00:19:18,860 --> 00:19:23,740
we had lots of wealth and we never gave it away to anyone we never did good deeds with all

164
00:19:23,740 --> 00:19:31,500
of our money we wasted our time in our lives doji vita in a wrong life an evil life a life of evil

165
00:19:31,500 --> 00:19:42,220
sat was the second man's mean sat pita wasa sahase in for 60,000 years right 30 down and 30 up

166
00:19:43,260 --> 00:19:48,140
we've been boiling I guess that's what it was it would have been 60,000 years ago then I guess

167
00:19:48,780 --> 00:19:52,780
60,000 years and now finally they're at the top but then they have to go back down again there's

168
00:19:52,780 --> 00:20:00,540
not over it there's 60,000 years we've been boiling in this cauldron for the evil that we've done

169
00:20:00,540 --> 00:20:10,460
something like that the third guy not means not to not to not to I think there is no end

170
00:20:10,460 --> 00:20:16,700
not to until good don't at all where is the end good though from where can there be an end

171
00:20:16,700 --> 00:20:22,940
I don't see any end because we have done these evil deans there's no way out

172
00:20:22,940 --> 00:20:31,740
and the fourth one so it's gone from so a hung so hung so hung means I

173
00:20:34,700 --> 00:20:41,500
when I get out of here and then born as a human being again you can be darn sure that I'm

174
00:20:41,500 --> 00:20:49,900
gonna spend all my time doing good deeds giving charity keeping morality and practicing meditation

175
00:20:49,900 --> 00:20:59,100
so I said there's no harm to you your majesty wink wink unless you like the under the

176
00:20:59,100 --> 00:21:06,460
under but the between the lines is unless you do exactly what they did by committing adultery

177
00:21:06,460 --> 00:21:16,460
and it was just a just these four just these four poor souls who had fallen into hell

178
00:21:19,980 --> 00:21:24,300
and then they hear a voice off to the side saying venerable sir

179
00:21:28,780 --> 00:21:34,860
I now know the the I now know the true length of a league when they look over and there's

180
00:21:34,860 --> 00:21:37,740
excuse me saying he looks over and there's this man who is sleeping in the monastery

181
00:21:39,420 --> 00:21:43,100
and King for saying he looks at him and he shivers and he said to think I was going to kill

182
00:21:43,100 --> 00:21:49,260
this man for his wife I could have been in hell and he said venerable sir I now know the the length

183
00:21:49,260 --> 00:21:58,700
of the night and we'll just said well the length of the night is long the length of the

184
00:21:58,700 --> 00:22:07,260
eojana is quite long but even longer is the length of samsara for the for fools who don't see

185
00:22:07,260 --> 00:22:09,500
the true dhamma and so that's the verse he told

186
00:22:09,500 --> 00:22:35,500
so that's the story what's the meaning of the verse how does this verse apply to us

187
00:22:35,500 --> 00:22:43,100
well obviously we're not fools everyone is here interested in the Buddha's teaching

188
00:22:46,300 --> 00:22:51,740
but the reason we're not here is because we have heard and understand these teachings

189
00:22:51,740 --> 00:22:58,860
because we have these teachings and they we have them to remind us of the danger inherent in

190
00:22:58,860 --> 00:23:05,980
falling astray the mind is powerful and power can be used for good or evil it's not a game that

191
00:23:05,980 --> 00:23:13,020
you can just turn off the computer or the Xbox or whatever the press reset it can't just

192
00:23:13,020 --> 00:23:21,500
wipe the slate clean everything has its effect if you engage in evil deeds evil results come to

193
00:23:21,500 --> 00:23:27,980
you if you're constantly engaging in evil deeds it becomes a habit and that habit lasts with

194
00:23:27,980 --> 00:23:37,500
you your mind carries it cult of it becomes who you are we become the things that we do

195
00:23:41,740 --> 00:23:48,300
and then when we pass away we'd be born in we can be born in even help if we've done lots of

196
00:23:48,300 --> 00:23:59,260
lots of evil deeds and our minds are impure and so on so the the the meaning of the whole

197
00:23:59,260 --> 00:24:02,860
long as the night well long as the night isn't is actually an important Buddha's teaching

198
00:24:02,860 --> 00:24:11,580
because the Buddha said Buddha once was sleeping out in the open and on some with just his robes

199
00:24:11,580 --> 00:24:20,140
was sleeping on some dry leaves he was traveling somewhere and he got caught in the night and so he

200
00:24:20,700 --> 00:24:25,820
gathered together some leaves and put his robe one robe down put one robe on top of him in the

201
00:24:25,820 --> 00:24:32,300
middle of winter and this man came up okay remember who it was Brahman I think came up and said

202
00:24:32,300 --> 00:24:39,020
to an venerable sir that's cold those leaves earth in your robes are thin and it could snow

203
00:24:39,020 --> 00:24:46,300
tonight this is the between the 8th between the 8th is the 8th the 8th of one month and the 8th

204
00:24:46,300 --> 00:24:51,340
of another month the coldest two coldest months it was a coldest period it's called the between

205
00:24:51,340 --> 00:24:58,060
the 8th the period of the coldest period in India of the year they said it could snow this is

206
00:24:58,060 --> 00:25:02,300
during this period sometimes it snow so it was down near freezing the freezing point

207
00:25:03,980 --> 00:25:07,740
and the Buddha said to him well let me ask you he says how are you going to sleep how can you

208
00:25:07,740 --> 00:25:12,460
possibly sleep like that and the Buddha says to him he says let me ask you something

209
00:25:13,980 --> 00:25:19,420
suppose a person is full of lust now suppose a person sorry suppose there's a rich man

210
00:25:21,180 --> 00:25:30,380
on a but a down field what would it be like a comfort no what do you call these things

211
00:25:30,380 --> 00:25:37,180
when you call the things people sleep on a mattress no but there's something else

212
00:25:38,300 --> 00:25:42,220
futon futon I'm thinking I don't know anyway some expensive mattress something

213
00:25:43,340 --> 00:25:53,420
water bed maybe another standing with silk sheets or and pillows down down feather pillows and

214
00:25:53,420 --> 00:26:01,900
rich whatever embedding and the most softest delicate most comfortable bedding that there is

215
00:26:02,620 --> 00:26:07,500
warm quilts and comforters and so on let's suppose their mind is full of lust

216
00:26:08,860 --> 00:26:13,820
do you think they would sleep well that night and then ramen said no probably they'd be up

217
00:26:13,820 --> 00:26:17,020
all night passing and turning and what if their mind was full of anger

218
00:26:17,020 --> 00:26:22,860
well then no likewise if their mind was a little anger and hatred they also wouldn't sleep

219
00:26:22,860 --> 00:26:29,100
well night no matter what luxury they were sleeping and they would if their mind was full of

220
00:26:29,100 --> 00:26:38,940
delusion there'd be arrogance or conceit and self righteousness or even just ignorance thinking

221
00:26:38,940 --> 00:26:46,380
and ruminating and worrying for example doubting all night no then they would be up all night as

222
00:26:46,380 --> 00:26:52,620
well and we said well none of those three things are present in me and so for me to sleep here

223
00:26:52,620 --> 00:27:01,180
for one night isn't a big deal so it's a important teaching is sleep doesn't comfort peace

224
00:27:01,180 --> 00:27:05,420
doesn't come from luxury it doesn't come from without it comes from within you can have

225
00:27:05,420 --> 00:27:10,220
be surrounded by all the luxury you want if your mind is not pure you don't sleep well

226
00:27:12,940 --> 00:27:15,900
how long the league is well there's not much done by there I don't think

227
00:27:17,260 --> 00:27:23,260
but the point the point of the verse is compared to these things compared to some sara

228
00:27:23,260 --> 00:27:30,540
compared to the wandering on of fools a league in the night or nothing they're not even

229
00:27:30,540 --> 00:27:37,420
we're talking about because of fool a fool someone who does evil deeds with body

230
00:27:37,420 --> 00:27:42,780
with speech and with mind so they kill they steal they cheat

231
00:27:42,780 --> 00:27:51,660
adultery they hurt others they scam others they commit evil deeds by speech meaning they lie

232
00:27:51,660 --> 00:27:59,340
they cheat they lie they back by it and gossip about others and have harsh speak harshly

233
00:27:59,340 --> 00:28:05,180
to others and speak useless speech and in the mind they're full of anger agreed

234
00:28:05,900 --> 00:28:13,100
and delusion thoughts of thoughts of lust thoughts of desire thoughts of hatred thoughts of

235
00:28:13,100 --> 00:28:23,340
arrogance and conceit and so on because of all that they they're they're like a drunk person

236
00:28:23,340 --> 00:28:31,260
like a person who is so tired with the journey who is wandering through the wilderness

237
00:28:32,220 --> 00:28:39,500
looking for shade and they become so tired and and burnt out with heat stroke or

238
00:28:40,620 --> 00:28:44,860
dizzy with heat stroke that they don't even see a shady tree when they walk by it they're not even

239
00:28:44,860 --> 00:28:50,140
able to see the shade so to a fool is not able to see goodness they're not able to see the right

240
00:28:50,140 --> 00:28:54,460
path they're not able to find the right path and I'm able to see the danger in what they're doing

241
00:28:54,460 --> 00:28:59,820
like a person would have said like a person who is dazed and confused and has heat stroke

242
00:28:59,820 --> 00:29:05,420
and is walking down the path and at the end of the path there's a big pit full of burning

243
00:29:05,420 --> 00:29:11,740
blazing embers but they don't see it because they're too their mind is too dizzy

244
00:29:11,740 --> 00:29:20,460
now the the reason why they are so dizzy and why they're so

245
00:29:22,460 --> 00:29:27,020
in caught up and intoxicated is as the Buddha said satanamamamabhijana

246
00:29:27,020 --> 00:29:33,100
because they don't see the sandama so our practice as Buddhists is the most important practice

247
00:29:33,100 --> 00:29:39,500
for us is to see the sandama to see the truth you see the not just the truth but the good truth

248
00:29:39,500 --> 00:29:45,660
the important truth so we have three aspects to this one is bariati sandama

249
00:29:46,460 --> 00:29:49,740
two is pati pati sandama and three is pati veda sandama

250
00:29:51,900 --> 00:29:57,660
the bariati sandama means our study so as we study that when we when we hear these teachings

251
00:29:57,660 --> 00:30:01,900
about how these guys were in hell you might not believe it and you might think well that's ridiculous

252
00:30:01,900 --> 00:30:09,420
and how it doesn't exist so on you might believe such things but you might also start to understand

253
00:30:09,420 --> 00:30:14,940
in your mind and think oh well it really seems reasonable that there are consequences to our actions

254
00:30:14,940 --> 00:30:18,780
at least in this life we have pretty major consequences to evil if we do them

255
00:30:21,100 --> 00:30:28,940
and so by hearing this it it it gives you some pause sometimes some pause cause for reflection

256
00:30:28,940 --> 00:30:36,460
and so as a result you become less inclined to do say and think evil deeds evil of all kinds

257
00:30:36,460 --> 00:30:44,780
a fool who hasn't had this kind of learning is a disadvantage I mean just learning the five

258
00:30:44,780 --> 00:30:48,220
precepts if someone had taught them to me when I was younger explained them to me I think

259
00:30:48,940 --> 00:30:53,340
I would have been a lot better off they think a lot of people would be if they had heard about

260
00:30:54,460 --> 00:31:00,620
the the heard other people speaking badly about killing and stealing and talking about the

261
00:31:00,620 --> 00:31:05,500
dangers and dangers inherent in these things how it corrupts the mind and how that therefore leads

262
00:31:05,500 --> 00:31:11,580
to suffering but it's not enough because you can you still have no experience so you might have

263
00:31:11,580 --> 00:31:21,420
doubt you might decide that well it's your desire to do and say and think bad things is

264
00:31:21,420 --> 00:31:30,300
overwhelmed and when when you have desire you you aren't able to make yourself believe the

265
00:31:30,300 --> 00:31:35,660
things that you've studied because it's not strong it's not as strong as the desire the they have

266
00:31:35,660 --> 00:31:41,900
habitual inclination of the mind you know the chemicals that are working in the brain and so

267
00:31:44,380 --> 00:31:49,100
so the second part is patty patty sadama patty patty sadama helps you to see helps you to compare

268
00:31:49,100 --> 00:31:57,500
good and evil and it cultivates habits of good to counteract the evil so if you cultivate habits

269
00:31:57,500 --> 00:32:07,420
of wholesomeness including knowledge that the observation that evil leads leads to suffering

270
00:32:07,420 --> 00:32:12,140
so as you sit in meditation and you see how anger inflames the mind how greed inflames the mind

271
00:32:12,140 --> 00:32:21,900
of delusion boils the mind you start to incline away from evil leads and they get less they have

272
00:32:21,900 --> 00:32:28,380
less power over you and the wholesome you have an inclination towards wholesomeness through

273
00:32:28,380 --> 00:32:32,940
our through the practice this is all we're getting from walking back and forth and sitting still

274
00:32:32,940 --> 00:32:39,980
we're gaining these habits sorry it's not all we're getting this is the the the preliminary

275
00:32:39,980 --> 00:32:46,860
benefit the practical benefits but the third one is patty veda sadama is the real true seeing

276
00:32:46,860 --> 00:32:54,940
the proper seeing of the true dhamma which is when the mind enters into nibhana when there is

277
00:32:54,940 --> 00:33:02,540
the realization of the four noble truths and the mind lets go of experience and as a result

278
00:33:02,540 --> 00:33:11,820
sees everything seizes kings and experiential knowledge that there's nothing that arises that

279
00:33:11,820 --> 00:33:18,460
doesn't cease and so loses the desire to cling and to strive and to crave and to chase after

280
00:33:19,980 --> 00:33:27,260
evil or chase after any it chase after objects of desire objects of aversion to chase them away

281
00:33:31,100 --> 00:33:38,220
any desire to cling or get involved with anything so as a result one is naturally unenclined

282
00:33:38,220 --> 00:33:43,580
to commit evil leads knowing that they just lead to stress and suffering and

283
00:33:50,140 --> 00:33:58,060
wandering on that they just lead to mindless meaningless samsara this comes through the practice

284
00:33:58,060 --> 00:34:05,340
of the Buddha's teaching and so this is what we have to be thankful grateful and proud of

285
00:34:05,340 --> 00:34:15,660
as far as Buddhists are proud allowed to be proud that we're not fools and that we are engaging

286
00:34:15,660 --> 00:34:24,140
and trying and fighting the fight to try to cultivate wisdom and understanding and goodness in our

287
00:34:24,140 --> 00:34:32,540
hearts so that's verse number 60 and that's the story and the meaning as far as I can tell it

288
00:34:32,540 --> 00:34:37,660
so thank you all for tuning in I hope this has been useful and I wish you all to find peace

289
00:34:37,660 --> 00:35:07,500
happiness and freedom from suffering thank you have a good night

