Okay, welcome to our or my last Wednesday talk of the year, I'll be going next week, so
this is my going away, I'll be giving another talk probably on Sunday and that'll be the
last and then I'll be back in the new year, I guess, given I don't die or get carried away
by gypsies in the meantime, so today I thought I would go into a lot yesterday that I
might talk about emptiness, I was going to boil you all to tears with a translation of
an ancient Buddhist text, but as I read through it I thought now, that's two, that'd
be too painful, too cruel of me, so I'm going to go into something that's a little
more practical, or a little more easily digested, something that a lot of the ancient
these ancient texts are to be memorized because they're concise summaries of Buddhist
teaching, and then you can take your time to go over each part of them, and you can
study each part in great detail.
For those of us who are here just to get up the essence of the Buddhist teaching, it's
always better to just stick to practical teachings and to get right into the practice as
quickly as possible, to limit our theoretical knowledge to what is strictly necessary instead
of trying to get a comprehensive understanding of the Buddhist teaching because it really
is a lot, and much of it is most useful for someone who is teaching, because when you're
teaching you have to field other people's questions and deal with their problems and you
have to be able to deal with a wide variety of a far wider variety of conditions than
your own, when you're just looking to practice, rather than get a comprehensive understanding
you can skim through, or even rely on a teacher to explain to you the things that are
necessary to address your own concerns and difficulty.
So today I thought I would skim through the Buddhist teaching, use the Buddha's own technique
of skimming through and giving people an overview of his teaching.
Arguably the most commonly employed technique of the Buddha, it's said to be anyway whether
he actually taught the same thing over and over again, is debatable at the very least
this is the accepted outline of what the Buddha would have taught, so there's many times
where the people who put together the canon of the Buddha's teaching, don't know what
the Buddha taught exactly or didn't keep track of everything the Buddha taught because
a lot of it was repetition, but they would just say, and then he went there and taught
this basic outline, and then he went there and taught this basic outline, and so the
idea is that this is basically what he taught, and it gives us an understanding of what
is the Buddha's methodology and what is the thrust of his teachings, this general teaching
is called the Anupubikata, which means the talk in sequence, it's a teaching in an orderly
fashion, and this is an interesting characteristic of the Buddha's teaching that he wouldn't
just give theoretical knowledge, he would actually lead the listeners from A to Z, from
the beginning to end, so that as they listened their minds calmed down and they were
able to go deeper and deeper and attain stage in stages, the realization of the truth
as they were listening to the Buddha's teaching, so in the beginning he would start with
simple topics, and eventually as the person's mind loosened up and became calm, he would
get into gradually more and more subtle and profound topics, so with the Buddha would
always start with, or generally start with, in this case, is called Dana or Charity, this
is the first thing that the Buddha would often talk about, and it's considered to be a
basic religious teaching, charity, even in the Buddha's teaching, it's something we don't
hear a lot about, because they're always so quick to get to the meat, get to what is
really important, but the new thing about charity is that it loosens the mind up, just
by talking about it, by feeling good about our charitable acts, feeling good about ourselves,
this is what I mean by starting at something that is going to help us to feel good about
ourselves, charity is something that helps us to progress on the path because it makes
us feel good about ourselves, it makes us feel confident, it makes us feel calm, it does
the way with guilt, it does the way with craving, it makes us content, it makes us happy,
and therefore it's a very important support for our spiritual practice, so this is what
the Buddha, and in fact there's a lot of this in Buddha's teaching that we don't often
hear about, how the great benefit of giving, when you help other people, or when you give
something that is of use to them, this isn't talking specifically about money, it's more
talking about things that are actually used to people, giving food, giving care, you know, to food
banks, supporting your parents, or supporting your children, or supporting your friends,
giving good advice, and teaching people, the Buddha said the greatest gift is the gift
of truth, the gift of the Dharma, and so it doesn't have to even be material support,
but the point is to be charitable, what you have to give, it's an incredibly liberating
experience actually, it's one that should go hand in hand with meditation, the ability
to take the things that you have, and instead of hoarding, to always be sharing, and
this I'm told many, many of the thinkers of today are starting to see that this is really
the only way forward, the only way that we can hope to survive as a species is through
distribution of our wealth, through sharing, through supporting each other, as opposed
to competing, you know, because our competition is what's killing us, people talk about
how great competition is, I suppose it's great in so far as it's a, we act as a model
for each other, but that's not really competition, is it where we strive ourselves and we
egg each other on, that's one way we use competition, but that's not really what is the
meaning of the word competition, competition is trying to beat the other person, trying
to be better than them, trying to keep up is supporting each other, when we try to keep
up with each other and try to support each other in further development, egging each
other on, encouraging each other to go further, encouraging people to come up to our
level, to gain the things that we have gained, to learn the things that we have learned,
this is support and this is charity, and so I think this is, this is probably the most
forgotten or misunderstood spiritual practice, and I think part of this is, is because
of the abuse of the idea of charity by religious people, and I can say this as a
religious person, that the people who are extolling the virtues of charity are often
those who are expecting to receive it, so it's often why many, many times we don't talk
about charity, because it sounds like we're asking for something, which is really one
of the reasons why Buddhist monks are not allowed to, to hoard things, I'm not allowed
to touch money, I can tell you that donating money is, charity is good, no it's good for
your own spiritual benefit and so on, but I can do that because I don't touch money
myself, for instance, and so I think it's, it's a good thing to talk about, I do sometimes
happen to hoard things up, because there are certain things that aren't forbidden, like
I have, I have in a bag here, 20, 22, 20 or so toothbrushes, and it makes me feel really
good, because I'm going to take them to Thailand when I go, and I'm going to put them
in, in monks' bowls, which is the toothbrushes over there, and not nearly as good as
the toothbrushes over here, so I'm going to, when the monks go and I'm drowning, I'm going
to put toothbrushes in their bowls, for example, so certain things can be, can be hoarded
I suppose, but like the clothes that I wear, the clothes that, that you see on my character
are, are just about all the clothes that I own, anyway that's not even what we're talking
about here is it, we're talking about charity, giving is good, giving is great, I encourage
everyone to, to remember this, consider it part of your spiritual practice, when you see
someone on the side of the road begging for money, give a little, the, the Buddha said give
a little, if you have a little, give a little, if you have a lot, give a lot, it should
never arise the thought of not giving, the Buddha said, if people knew the great benefit
of charity, in the same way that he knew the great benefit of charity, he said, people
would not eat, they would not eat one meal without giving some of that food to someone
else, if there were someone else there, there to receive it, they were, they would never
eat without sharing, because such is the profound benefit of, of charity, when you give
food to someone, they get a food for one meal, or, or even for just part of a meal, but
the benefit to your own mind is profound, it's profound in this life in terms of the happiness
that comes to you, and it's profound in your spiritual development, it's profound in, in how
it will change the direction of your life, when you become a charitable person, you gain
many, many good qualities, both in this life and in future existence, your path is changed,
your mind is purified, if your mind is lightened, anyway, this is the first, the first part
of the Buddha's teaching, the first thing that he would talk about, it's an important
way to live our lives, to live our lives charitable, to live our lives in the principle
of generosity and interdependence, and supporting each other rather than competing, trying
to get the better of others, trying to hoard and to guard and to keep the hold on, because
that sort of life is going to be in harmony with spiritual development.
The second one coming right in line is of course morality, it's the next step in the spiritual
practice, once you're living in a charitable way and without clinging, without holding
on, the next step is to set yourself in moral principles.
This one is probably more understood, but still very poorly understood in Buddhist circles.
Again, there's this tendency to want to skip ahead to the meditation practice, and in
this case, there is no skipping ahead, and someone is not moral, is not able to even put
aside in moral behavior, and I'll get to this, I'll get the explanation of immorality.
If you're not able to keep basic moral principles, then mental development is impossible.
And this is because of how we define moral behavior, moral behavior is that behavior which
disturbs the mind, or which is intrinsically disturbing, as to do with a state of mind
that is disturbed, that has to do with those states of mind based on anger, those states
of mind based on greed and those states of mind based on delusion.
It's involved with hurting oneself or hurting other people.
It's the breaking up of the harmony of the universe or of one's reality, the destruction
of harmony.
So this is where we really start to get into religious practice, and people always
ask as Buddhism, as Buddhism or religion, is Buddhism a religion.
My answer is always invariably yes at this point, because of the meaning of the word
religion has nothing to do with God, and it has nothing to do with faith.
This is not the meaning of the word etymologically.
Buddhism has to do with adherence or religiosity in terms of the opposite of negligence.
There's this theoretical word religions, which is the opposite of negligent.
Buddhism is a religion in the sense that, or it's a religion for those people who take it
seriously.
Buddhism is religion in the sense that it should be taken seriously.
If you're practicing these things as a hobby, then I would say you're not really following
the Buddha's teaching, because the Buddha was very clear that this is something that
should be taken seriously, and this is a serious issue.
These things that the Buddha talked about, and the teachings that the Buddha gave the reality
as he explained it, show that we're not in a position to take these things lightly.
And in fact, it's our negligent behavior, our sort of lazy way of being that is setting
ourselves up for great suffering and is doing a lot to create this harmony in this world.
That's why we're not at peace with ourselves, because we're not getting to the root of
the situation of the experience, of the root of the matter.
And so this is where we start to understand that it's important that we take this seriously.
The Buddha's teaching is something that we should incorporate into our lives.
It should be a way of life.
And this doesn't mean that we have to accept everything or take on a specific mode of
specific mode of being, mode of dress, or start eating Indian food, or go to live in Asia,
or so on.
Because the Buddha's teaching was very simple principles, it didn't have to do with culture.
And this is what is incredibly saddening to see that so much Buddhism has become cultural.
And most Buddhists are unable to separate culture from religion.
The Buddhist religion is very little, it's these five things, really.
If you don't believe in charity, then there's not much to talk, there's not much to
say, there's not much likelihood that you're going to develop spiritually.
I think there's not much argument in terms of the benefits of these things.
And this is all that makes a good Buddhist, someone who is charitable, someone who is moral,
and so on.
But it does require this sense of urgency or seriousness that we should be able to keep certain
moral principles.
And there's not even that many of them.
Basic morality in Buddhism is just a few things.
There are the five basic moral principles, not killing, not stealing, not cheating, not lying,
and not taking intoxicants.
It's really pretty basic and you really have to see it as a practice, as a challenge.
Because people always try to come up with excuses and we would rather fit the Buddha's teaching
to our lives than to try to fit our lives to the Buddha's teaching.
And so here's, we should look at it more as a challenge than as an inconvenience.
People often say, oh, there are ants in my house and I have to get called the exterminator
and so on.
And this to me is the challenge to all of us.
When I have ants in my house, I live with them and I try to do my part to keep them in
their own place.
So I was not leaving food out, maybe even sprinkling baby powder in the places where
I wouldn't want them to want to travel and so on.
People say that they drink, but they only drink socially.
And to me, this is the sign that you're not taking this seriously.
This is a development.
It's a path.
It's a way of changing our lives and morality.
The great thing about morality is it makes it confident to make sure of yourself.
And in the same way of, as charity, it makes you feel happy, makes you feel calm.
The Buddha said, by keeping these precepts, by not killing, I used to be a hunter, I used
to hunt deer with my father when I was young, I actually killed a deer once, a baby deer
just like this one beside me, isn't that a wonderful thought?
When I first kept the precepts, I cried because I read the Buddha's teaching and how
the Buddha explained it.
He said, when you keep these precepts, when you vow to keep the precept, not to kill,
you give protection to all beings, all beings, wherever they be, are free from any danger
from you.
They have nothing to fear from you, and they will never suffer at your hand, and the
same for stealing, for cheating, for lying, and for taking intoxicants.
Of course, taking intoxicants is always the most difficult one.
It's actually the easiest one, because there's never any reason to drink alcohol in the
same way as there would be a reason to kill, there's a mosquito on your arm while you're
being attacked, something is sucking your blood, so as animals, we have this reaction
that we should kill that, but there's no animal on earth that reacts to things saying,
oh, I should go and drink alcohol, I should go and smoke pot, I mean, you don't see
chimpanzees wondering where they can get a pint of beer.
It really is the easiest one, because it's a total non sequitur.
It has nothing to do with our lives.
It's an addition to natural existence, it's something that we have created as an artificial
contract, we're drinking poison, we're taking something that our body doesn't deal with
in normal circumstances, and the point in terms of Buddhism is that the only effect that
it has on oneself is the dulling of the senses, it's the opposite of the development
of mindfulness, the development of clarity of mind, clarity of mind is synonymous with
true sobriety, because we're intoxicating ourselves all the time, you don't need alcohol
and drugs to intoxicate yourself, it can be intoxicated with a flower or a sassong,
but these are the extreme forms of behavior, taking drugs in alcohol, it's quite extreme,
it's an extreme form of intoxication, so this is really where it starts, we've often
had people coming to the meditation center, with the idea that they could smoke marijuana
or get high while they're practicing, and of course these things don't go hand in hand,
this is something that we have to take on in the beginning of our practice, morality
is a very powerful thing, it's something that calms the mind, it gives you direction,
it's the foundation of spiritual practice, and it comes next after a journey, so that's
the second, the third thing the Buddha talked about is another thing that we don't often
hear about, and it's still not very core in Buddhism and that's probably why we don't
hear about it, and this is talk about heaven, talk about paradise, and I suppose another
reason why we don't hear about it so much is that Buddhists today are not Buddhists
in the West today, are not really interested in heaven, I mean they've come from Judeo-Christian
tradition that try to shove heaven and hell down your throat, and they don't want anything
to do with it, and that's also probably why they don't want anything to do with morality,
but heaven, the talk about heaven is actually quite interesting for Westerners, I think,
because it opens, we have to adapt it here because we're not talking to people who generally
believe in things like heaven, but if you bear with me, the idea of heaven is dealing
with a broader issue of the nature of the universe, because most of us are stuck in this
idea that human existence, and I've talked about this before, human existence is the default,
it's the natural way, right, we're at the center of the universe, we still have this
in our minds, I mean think about it, the Christian church had this doctrine that we are
at the center of the universe, and we still have this idea, we think human, I am a human,
we identify ourselves with this body, we think this is who I am, and all that comes
with it, is me, is I, and so we develop all these stereotypes and narrow-minded ways
of looking at the universe, what is real, what is not real, you know, look at the material
sciences, how they're so sure that they haven't all figured out, right, the scientific
method, oh I've had terrible arguments about the scientific method, because obviously
meditation doesn't conform to the quote unquote scientific method, but the scientific
method is of course only only a material, it's a materialist method, I mean it can only
work in the physical sense, you can't, you can't apply the same principles like falsifiability
to meditation practice, right, I can't prove to you that I am, I'm experiencing anything,
I can't prove to you that I am realizing these insights and that I'm not just lying or
not just being deluded and so on, so the principles don't work, but the Buddha was not
an ordinary human being, I think we're all quite aware of that, he had some profound
understanding of the nature of reality that went beyond this, and this is really what
we're trying to, or what we do come to realize in the Buddha's teaching, that reality
is much more objective than simply the human understanding or the human existence.
Human existence actually is a contrivance, it's more like a prison, it's a filtering
or a contortion of the mind, fitting this vast and profound reality of who and what
we are into a very small box, very small compartment, I'm sure some of you have seen that
YouTube video of this brain scientist Jill, somebody or other, I'm sure some of you know
what I'm talking about, probably some of you don't, but this, this very famous video
of this woman who had a stroke, and as a result, her whole experience during the time
she was having a stroke was of the vast expanse of nature of the mind, and then suddenly
she'd come back to earth and she'd be worrying again about calling the doctor, and then
she'd forget it all, and she'd be like a being floating in the infinity of the cosmos,
and then back again, and she was explaining how the brain, the brain is what allows us to
judge and to worry, and so on, one of the hemispheres of the brain, I can't remember
which right brain I think, and to, you know, that, that, well obviously that wasn't
proof of proof for them of what I'm saying, but from a Buddhist point of view, from someone
who's looking at things from an experiential point of view, this is exactly the nature
of things, that the brain is something that is keeping the mind, we have created this
person for us, based on our, our narrow view of the universe, and our repeated and
more, and increasingly intense craving and attachment to things, you know, the evolution
of the mind has gone hand in hand with the evolution of the body, we've created this reality,
through successive incarnations, you know, every moment we cling further and further and
we, we drown ourselves further and further in this, this doctrine, you could say almost
of, of being who and what we are, you know, even separating into, into groups, of identifying
as male and female identifying as, by race, color, religion, social status, political views
and so on, when we create, we create so many, it's like we're building up walls, we're
building up these constructs and that's what the brain comes to be.
So heaven is, is, you know, it works really well for Hindus, but the profound nature
of this is, is helping us to, to overcome these barriers and to break down these conceptions
of reality, this is not all that there is, we are not our bodies, we are not just this,
we have created, we have come to this, this is what we have come to, people who have
near-death experiences are immediately transformed beings, they come back and they'll tell
you they're not afraid of death, they live their lives in peace, relatively, because
they know there's so much more than this, they've opened their minds, they've seen or
they've experienced the mind that is free from the confines of, the constraints of the
body.
And so what this all has to do with heaven and with the Buddha's teaching is, when
we leave this body we take our minds with us and that means we take everything about who
we are with us, we take all of our memories, all of our emotions, all of our attachments
and addictions with us.
At the moment when we die, we're lost, we're left without all of the things that we
hold dear, many people die crying, die afraid, because they're still clinging to the things
that they have to leave behind, or because they've engaged in certain activities that
have left them feeling guilty and afraid, you can't erase the things that you've done
from your mind.
It leaves a trace in your mind that mostly we're not aware of, if we haven't practiced
meditation, it's easy to forget about the deeds that we've done.
When I first went to practice meditation, it was very difficult for me, having led a fairly
immoral life, and it wasn't even that I was being told that it was immoral.
I really felt bad about the things that I had done.
I felt bad about the way I had treated my parents, I felt bad about the way I had treated
other beings, I felt confused and lost and quite quite out of harmony with the universe.
It took a long time to change those ways of thinking and to find a new way to be that
it was more harmonious, and this is really what the practice of the Buddha's teaching
is.
It's coming to a more heavenly state of existence to transform our existence into one
that is harmonious, one that is in tune with nature and tune with the way things are.
Heading towards greater and greater states of peace and harmony.
So heaven has a place in the Buddha's teaching, it has its place, the development of goodness,
the development of harmony.
If this was the only life that we had, and when we died there was nothing else, there's
no need to talk about things like heaven or even good deeds, in fact the immorality wouldn't
have that much place, because when you die you die, a good person dies, a bad person dies.
But that's really part of our doctrine, our misunderstanding of what we've come into, what
we've found ourselves, in fact reality is a much more profound, a much more broad thing.
Reality is quite profound, there's much more to it than just this one life.
And so there's much more work to be done, and it's really the only thing that makes
sense, because we are such a convoluted being.
We have so many conflicting states inside of us, we have so much to work out, and the
reality of it is that this is what we have to do, we can't just leave our business unfinished
when we die, it comes with us.
And we either go to a more harmonious state of being, or less harmonious state of being
dependent on the path, the way that we're headed.
We don't know where we're going when we die, but we're creating, we are the creator.
So Buddhism doesn't believe in a creator God, it doesn't believe in a permanent hell
or a permanent heaven, but it's the teaching of cause and effect that everything we do
has an effect.
And so I always joke that, whereas other religions teach what they call intelligent
design, some of you have probably heard of this theory, this name that they give to creationism,
they call it intelligent design, Buddhism believes in unintelligent design.
This is the Buddhist theory, that we have created, why the universe is, why the world
is the way it is, why humans are the way they are, because of our stupidity, because
we have, this is what we have come to, we have created the problems and the suffering
in the world, ourselves.
We are the authors of our own destiny.
And this is really an empowering teaching, this is a very important part of the Buddhist
teaching, is that we can choose where we want to go, we are the owners of our own destiny,
we makers of our own destiny, how do you say it?
If we develop ourselves, we will reap the fruits, for lazy we will also reap the fruits.
So, okay, anyway, this is just to hopefully give a broader understanding of the nature
of the universe, that it's a wonderful thing that we have all of this terrain
to explore, you know, you want to see what heavens like, do lots of good deeds, but more
just understanding that this isn't all there is to the universe, who we really are is
our good and bad deeds, our wholesome and unwholesome qualities of mind, we are not this
physical body in this brief moment of time.
So, it's at this point that the Buddha, once he started to talk about the nature of reality,
when he would start to turn into more core principles, and so the last two are talking
about the more core Buddhist principles of meditation practice.
Because once we understand that who we are is not this physical body, that it is indeed
the physical and mental states that are arising at every moment, even here sitting, watching
this computer, you probably weren't aware that many things are arising and ceasing are
coming and going, you probably felt some pain in your in your legs or in your back that
you had to you had to adjust, maybe you felt an itch in the body, maybe you were thirsty,
maybe you were distracted by something.
These are the realities that make up our existence, and this is where the Buddha's teaching
really starts.
This is how meditation is practice.
Through seeing, waking up and seeing what's really, really real, that the idea of human
being is really only a concept.
What is real is the experiences in the present moment and they're very different from
what we think, it's not a human being, we're not sitting in a room in front of a computer
screen.
In fact, we're experiencing phenomenon after phenomenon after phenomenon.
None of this has any inherent self-nature, so the computer screen in front of us is actually
just light touching our eye that is interpreted as the brain is being a computer screen
and so on.
The people that you see in front of you, you're looking at me, you see me, is actually
just light touching your eye and being interpreted by the brain.
It's in fact not even light touching the eye, apparently scientists have discovered that
when we look at something, we don't look at the whole picture.
We're lazy, or we've, it's not exactly lazy, we've developed, we're very good at extrapolating.
This is why you can look at an ink blot and see so many different things, you can look
up at the clouds in the sky and see faces or rabbits or whatever.
Why optical illusions work?
Because of how the brain works, we, we don't, you don't look at the whole computer screen,
you've got it memorized already.
You have this, this very basic refresh that refreshes the mental picture.
Let's see, it goes on in the mind.
So, the next important step for us to take in, in, in, in this succession, as we understand
morality, where we understand generosity, morality, and we start to understand what is
in nature of the world, is to, to, to start to examine and to analyze and to train ourselves,
to act accordingly, you know, when, when we believe that the world was made up of people
and, and society, then we had certain principles by which we lived our lives in terms
of trying to make money, trying to be successful, trying to find stability in life and get
married and have kids and so on and so on.
Once we realize that the, the universe is not of that sort, that it's actually a, a
much broader thing than we, than we start to act accordingly, we start to act to relate
to the universe on a phenomenological level.
We start to address the, the important issues, important issues are no longer money and jobs
and status and fame, pleasure in the world.
The issues are greed and craving and anger and depression and worry and fear that exist
in our minds even at this moment.
And this is why we meditate.
This is the importance of meditation that it addresses the issues in the same way that
getting a job addresses an issue in, in the conceptual world in society.
So well, we're not trying to encourage people to give up their jobs and so on, not necessarily,
but when we come here to, to practice meditation, where coming to look at the world in a different
way and to address a different issue, our issues here and now sitting in this dear part
together are not of that sort.
The issue that we're focusing on is the danger inherent in, in certain, certain qualities
that, that exist in our minds, that we are, we have in our minds those things that will
cause suffering and do causes suffering.
We are a danger to ourselves, that's why we're here, really.
The issue we're addressing is, is, is our own problem, the, the causes of suffering.
We have in our minds the potential to cause perfect peace and happiness, but also the potential
to cause great suffering.
This is a scary thing, the untrained mind is one's worst enemy, the Buddha said.
So this is the next issue that the Buddha would address, the dangers of, of attachment
and clinging.
Because in a worldly sense, there is no danger, right, attachment and clinging leads
you to get more and more, more and more attachment and clinging, I suppose.
But in, in a Buddhist sense, or in, from a point of view of ultimate reality, that clinging
is dangerous because there is no end, right, when we get what we want, we cling more, right,
and we're satisfied less, you know, you, you got last year's iPhone and that was great
until that, this year's iPhone came out, I don't know, I don't have a iPhone, but that's
what they say.
You listen to this song and it was great and now, now that song just doesn't do it,
we'll get the next, next greatest thing, something novel, something new.
So as long as we can get the things that we want, we're happy, but when we can't get them
we're not happy and because of the nature of it, we're, nature of it, we're just building
ourselves up to the fall, why we feel depressed, why we crash is because we build ourselves
up, we are not in harmony, we have no contentment.
The Buddha said contentment is the greatest possession, the greatest well, because nothing
you can get can really make you content, can really make you happy.
Contentment doesn't come from getting what you want, contentment comes from being happy
with, happy with yourselves, happy with the way things are.
Contentment comes from the acceptance of all of reality, the whole spectrum of reality.
So the Buddha would explain these, the dangers of sensuality, that this is what causes suffering,
this is what causes us to fight with our parents, to fight with our children, to fight
with our brothers and sisters, to fight with our friends.
What causes disharmony is our attachment.
We expect people to act in a way that pleases us.
We don't love each other, we cling to each other, expecting everyone else to make us
happy and when they don't we get angry, when they do something, say something at the
wrong time, in the wrong way, makes us upset.
This is why we go to war, why one country invades another, because of attachments, why
borders are drawn, and these ridiculous conflicts happen, right?
We drew the borders here, and it was in the middle of your people's civilization, so
now suddenly your enemies, why we, why there are conflicts in civil wars all over the world,
and they just strife, it's all clinging, addiction.
So the final thing that the Buddha would talk about is the subsequent benefits of giving
up our attachments.
We're always taught the benefits of craving in this world.
For a reality, that's what advertising is based on.
They want to show you a product, and convince you of how it's going to make you happy.
Convince you have the benefits of it, that this is worth wanting, this thing is attractive,
this thing is desirable.
When you want this, you're going to be happy, when you get it, you'll be happy.
And this is why we have this idea that happiness is to be found in beautiful sights and
pleasant sounds, and wonderful smells and delicious tastes, pleasant sensations.
This is where happiness is to be found, or in interesting and pleasing ideas in the mind.
Buddhism goes very much opposite to the way of the world, this is why it's very difficult
to, well, this is why we have to go in a progression like this, it's not easy to teach
people these things.
We don't want to hear this.
We want to hear about wonderful things, special things, high and lofty ideals.
Buddhism teaches us about reality and teaches us about ourselves, about the nature of our
minds, teaches us about what's going on here and now, and it teaches us that the ordinary
nature of reality is what is causing us suffering, our clinging, our diffusion, if you
will have consciousness, how we are so spread out so thin, right?
We have so many attachments, so many things that are clung to.
Our friends, our family, our society, and so on, that our state of being is not at peace
with ourselves.
So the practice that we're trying to undertake is to look at the state of mind in front
of us.
To look at our minds here and now to examine, we have these techniques, for instance,
this technique of noting, to observe the reality and to catch it and to name it, when
you're thinking, thinking, when you're feeling, feeling, feeling, when you have an emotion,
when you feel angry saying to yourself, angry, angry, just being here and now, being
with it, not clinging, not sending out our tentacles and becoming wrapped up, all of the
chaos of the world around us, to just be here and now.
This is called the benefits of not clinging, the benefits of renunciation.
Really core to the Buddhist teaching, Buddhism is a teaching to come out, to come back
to our center, to leave behind the conceptual world of humankind, and to live here and
now, in regards to the world in front of us, the experience of the world in front of us,
of reality in front of us, so I think I've talked for a long time here, of trying to go
over the basics of the Buddhist teaching, and I think that's a worthy subject.
These five things are a good broad summary of what the Buddha taught, going from being
a generous person, being a moral person, to broadening our horizons, in terms of the
nature of reality, and slowly giving up, seeing the disadvantages of clinging, of craving,
and the advantages of coming out, of renouncing, renouncing craving.
So thank you for bearing with me, and it was a long talk, but I hope it was of use to
you in some way, and I hope that you're able to use this and the other teachings,
of the Buddha, your own benefit and your development on the path, and the eventual realization
of true peace, happiness, and freedom from suffering, if you have any questions, and happy
to take them, otherwise have a good day.
