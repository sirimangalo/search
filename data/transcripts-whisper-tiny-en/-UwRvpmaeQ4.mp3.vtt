WEBVTT

00:00.000 --> 00:08.680
Meditating, I noticed that fabricating an eye, eye was feeling, etc., was untenable.

00:08.680 --> 00:11.160
There was only the experience.

00:11.160 --> 00:14.000
This seemed to clearly disprove the self.

00:14.000 --> 00:16.800
This notion is so fundamentally simple.

00:16.800 --> 00:21.000
Why then is it so elusive, my gratitude?

00:21.000 --> 00:27.640
Well, it's only elusive because we've cultivated a sense of self.

00:27.640 --> 00:35.440
We have a very strong sense of self as human beings.

00:35.440 --> 00:42.200
We are born with it in something we've cultivated lifetime after lifetime, but it's something

00:42.200 --> 00:45.000
we reaffirm throughout our life.

00:45.000 --> 00:54.840
The idea of this is mine, the idea of being given things, identifying with things.

00:54.840 --> 01:03.160
When you recognize something, you desire it, and so you cling to it, and you get the idea

01:03.160 --> 01:05.960
that you're somehow in control of it.

01:05.960 --> 01:15.640
We cultivate habits of misperception, and it's not just identification with self.

01:15.640 --> 01:16.640
It's greed.

01:16.640 --> 01:21.160
There's actually no, if you think about it logically, there's nothing different between pleasure

01:21.160 --> 01:25.000
and pain, and yet we prefer one and a bore the other.

01:25.000 --> 01:26.000
Why is that?

01:26.000 --> 01:33.320
It's through misapprehension, it's through misconception, misunderstanding, about reality.

01:33.320 --> 01:34.320
It's only habitual.

01:34.320 --> 01:37.400
It's actually only you that's like that.

01:37.400 --> 01:39.960
There are people out there who aren't like that.

01:39.960 --> 01:43.840
There are people who are actually born with very little attachment to self.

01:43.840 --> 01:52.200
And most of us aren't so lucky, but when you say it being elusive, it's only elusive

01:52.200 --> 02:02.400
to you and to most of us, but it's only an artificial, it's not natural to, it doesn't

02:02.400 --> 02:04.400
mean it's natural to believe in a self.

02:04.400 --> 02:09.760
It just means that we've gotten so lost and so caught up in misunderstanding that we actually

02:09.760 --> 02:12.000
are born like this.

02:12.000 --> 02:15.760
It comes back to the idea that being human is somehow natural.

02:15.760 --> 02:18.760
There's nothing natural about having 10 fingers and 10 toes.

02:18.760 --> 02:29.280
It's a ridiculous situation that we're in, that we have to mash up physical matter in certain

02:29.280 --> 02:35.720
types of physical matter in our mouths and then have it pass down into our chests and

02:35.720 --> 02:44.480
be digested in our abdomens and then excreted as smelly, awful, fecal matter.

02:44.480 --> 02:47.520
There's nothing natural about that.

02:47.520 --> 02:53.520
Our whole existence, that we should have to bump into each other in order to, to, to

02:53.520 --> 02:59.720
procreate, to, in order for someone to be born, there has to be two beings of certain

02:59.720 --> 03:11.600
types coming together and having orgasms and then the, the, the, on the off chance that

03:11.600 --> 03:13.320
an egg and a sperm are going to get together.

03:13.320 --> 03:16.880
I mean, it's just, it's a ridiculous situation that we find ourselves in there.

03:16.880 --> 03:20.160
There's nothing natural about any of this.

03:20.160 --> 03:28.800
So the view of self is kind of, it's one of the major glues that holds it all together.

03:28.800 --> 03:33.480
It's one of the, the major points where we could say this is where we went wrong and yet

03:33.480 --> 03:34.480
we keep affirming it.

03:34.480 --> 03:40.760
We keep making the mistake and augmenting the mistake and, and cultivating that mistake.

03:40.760 --> 03:42.920
Now for some people, it's not that case.

03:42.920 --> 03:47.920
They're, they're on the, they're on the path away from that, especially those people

03:47.920 --> 03:53.120
who practice the Buddhist teaching and practice mindfulness meditation.

03:53.120 --> 03:58.600
They find themselves seeing clearly and realizing, oh, this is horrible thing that

03:58.600 --> 04:06.920
we've gotten ourselves into the situation that we've got ourselves into due to our misapprehension.

04:06.920 --> 04:12.600
So it's in a, in short, it's just because of your habitual misunderstanding, but once

04:12.600 --> 04:16.320
you start to read about it, it just seems so obvious and you realize, oh, what an idiot

04:16.320 --> 04:17.680
I've been, right?

04:17.680 --> 04:21.560
So, so often when the Buddha would teach, at the end of the topic would say, it's like

04:21.560 --> 04:25.960
I was blind and now I could see it's like you've, something was turned over and you've

04:25.960 --> 04:31.280
just put it like a piece of furniture was tipped over and you've just righted it for me.

04:31.280 --> 04:34.600
And, and, and now everything seems right again.

04:34.600 --> 04:36.760
That was quite common in the Buddha's teaching.

04:36.760 --> 04:42.520
It was just like putting things back in order, straightening out ones, understanding about

04:42.520 --> 05:01.920
reality because we get crooked otherwise, we get lost and confused and tired but not.

