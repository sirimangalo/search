I'm not going to do a video broadcast this evening.
I have a midterm exam tomorrow on Mahayana Buddhism.
You have to talk about the Lotus Sutra.
We had an interesting class yesterday where I asked the professor,
where the Lotus Sutra situates itself,
where the Lotus Sutra recorded tradition.
When did it happen?
When was it when was it taught?
According to tradition, not according to history or it's called
Scholasticism.
The interesting part of his answer when he started talking about,
what is the Lotus Sutra?
Anyway, I don't know if it's not all that interesting.
I forget about it.
But tonight we have a quote about the five hundredths.
Let's talk about that instead.
We talk about the five hundredths as often in our practice,
in the practice of Buddhist meditation.
And I think they're often a source of suffering for people
who feel bad that they still have these things.
They feel guilty when they arise.
And that's not in and of itself wrong,
but it's not entirely beneficial.
It doesn't solve the problem to feel guilty or to feel bad about these things.
Because they are wrong, and they should be understood as
a problem.
Or for a person who thinks there's something good to be had,
there's some benefit to be had from these things.
That person is deluded.
A person who wants to be happy and yet actively engages
in cultivating the five hundredths is acting
inconsistent with their own desires,
but their own intention, their own goals.
These things are intrinsically injured,
hindrances.
They have intrinsic hindering potential.
There's no inships about it.
They're just not good to eat.
But I think this quote,
it's quotes like this that we should really look at
and take at face value.
We shouldn't go the extra step and say,
oh, when I feel anger, I should be upset at myself for that.
I should feel bad about that.
We should look at quotes like this and try to understand them,
try to have this sort of inspiration arise in us.
To see that they weaken our wisdom,
sense desires and obstruction and hindrance,
which enshrouds the mind and weak and twisted.
Ill will sloth and torpor,
restlessness and worry and doubt.
Obstructions and hindrance is which enshroud the mind
and weak and twisted.
Surely it is possible that after abandoning
these obstructions and hindrances which grow in and up
over the mind and weak and wisdom,
being strong in wisdom,
one should know one's only good.
The good of others, the good of both.
And attain that knowledge and vision
will be fitting noble ones and transcending human states.
And that's it.
I mean, it could easily lead you to
freak out because you realize you're on the wrong path
with all these hindrances inside.
That's not all that productive at all.
It's not the path, it's not the way to overcome them.
Once you understand this,
and once you see the problem in here and
or even just see things as they are,
you don't have to judge ever really.
But that's, I think, key in the practice.
So not only is it useless to judge bad things.
It's also not the way to get rid of them.
No, it's not only useless,
but where I was with that,
the way to be free from the hindrances
is to just look to see clearly.
You don't even have to, in the end,
realize they're bad, as I say.
It's not only, it doesn't lead to any benefit.
It's not the end result.
We are just trying to see things as hard.
When you see things as they are,
the hindrances don't actually arrive.
They arise based on misconception.
Misunderstanding.
Misperception.
Perversion of perception.
If you have, there's a beautiful thing in front of you,
and see it as it is, as seeing.
You see the reality of it.
There's no beauty in reality.
There's no appealing quality to reality.
There's no displacing quality either,
and there's no ugliness in nature either.
Those aren't intrinsic qualities of anything.
So when you see things, when you see clearly,
it's not arbitrary, and it's not specific to Buddhism,
it's not a belief based thing.
When you see things as they are,
the hindrances don't arrive.
And that's what we should focus on.
Now, I guess another thing about this sort of quote is,
it's a reflection, sort of a premeditative reflection,
because the thing is, the quote is,
surely it is possible.
Surely we can find a way.
So when you think like that, then it's an impetus
for you to practice meditation.
It's not actually meditation to be moaned negative aspects
of these hindrances.
It is useful to see the problem with it,
to see how when you engage in anger, on your gauge,
and greed, or when you engage in delusion,
it clouds your mind, makes you unable to see clearly.
But the great thing is, you don't have to feel guilty.
The practice isn't feeling guilty about it.
You just have to, in that moment,
when you realize that you're angry, or greedy, or debutant,
you just have to say to yourself,
liking, liking, just liking, actually remind yourself
of what you're experiencing.
Clear your mind.
Be clear about what you're experiencing.
And if there's no debate,
when you do that, your mind is clear.
And so the practice is quite,
it means right here in front of us.
It's not something you have to look to find.
You have to learn over a course of years,
right here in front of you.
So there should never be discouraged
if we have defilements, or that our defilements
are too many to think,
to entrench.
You just have to be present here and now.
And you have to try to always be present if you can do it.
Often, and if he didn't, it becomes a bit
true, and it changes who you are.
You come to see clearly.
The hindrance is, we can,
and your life improves, your mind improves,
your wisdom improves,
and eventually you come to know,
slowly you come to know.
In stages, the good your own,
what is your own benefit?
What is the benefit of others?
What is it for the benefit of both?
And you attain knowledge and vision of the moment.
Okay, so that's where I'm done with what for today.
I'm going to head off and do a little bit of study,
because I'm not a sitcher.
I'm going to be right.
