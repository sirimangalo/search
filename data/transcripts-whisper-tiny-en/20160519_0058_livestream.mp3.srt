1
00:00:00,000 --> 00:00:20,520
Good evening everyone, one broadcasting live, May 18th, 2016.

2
00:00:20,520 --> 00:00:39,560
The night's quote is from the Mindapan, and without context it's simply a description of

3
00:00:39,560 --> 00:00:53,840
the virtues of a virtuous person, ten virtues of a virtuous person.

4
00:00:53,840 --> 00:01:00,720
It's interesting to learn the context, it's not necessary, but it is interesting because

5
00:01:00,720 --> 00:01:17,320
the reason why the Mindapan is, if I haven't mentioned this before, a very large text, it's

6
00:01:17,320 --> 00:01:29,080
a sizable text, with questions and answers between King Melinda and Nagasena, who's in

7
00:01:29,080 --> 00:01:41,680
Arahat, and it's really a great book for getting some insight into the Buddha's stance on

8
00:01:41,680 --> 00:01:54,640
various issues and on dilemmas, which are presented by the text, by the canonical text.

9
00:01:54,640 --> 00:02:01,320
It's so valuable that the Burmese edition of the Deepika, I understand, includes the

10
00:02:01,320 --> 00:02:07,800
Mindapan in the Kundakani Kaya, even though it's much later.

11
00:02:07,800 --> 00:02:14,800
But it's definitely worth studying, worth reading if you have time.

12
00:02:14,800 --> 00:02:28,640
So this, the context here is discussing suicide, Nabikavi, Atan, Nangbati, which should

13
00:02:28,640 --> 00:02:48,880
not destroy oneself, whoever it is, should be dealt with according to the Dhamma, I think,

14
00:02:48,880 --> 00:02:57,360
which means if a monk kills himself, there's a pen, I think it's actually, I don't

15
00:02:57,360 --> 00:03:04,520
know, it's interesting, if he's dead then, well, Atan, a monk, I think if someone tries

16
00:03:04,520 --> 00:03:13,680
to kill themselves, then they should, they should, they're guilty of an offense.

17
00:03:13,680 --> 00:03:19,200
So I, as I understand, I'm reading the poly, and as usual, my poly's not great, but it

18
00:03:19,200 --> 00:03:26,440
looks like Melinda, if I remember correctly as well, he says, well, the Buddha says this,

19
00:03:26,440 --> 00:03:34,920
and he also says that Jadhyaya, Jaraia, Bia, Dino, and so on, he teaches the Dhamma for the

20
00:03:34,920 --> 00:03:42,360
Samu Jadaia, for the cutting off of these things of birth, old age, sickness, and death.

21
00:03:42,360 --> 00:03:47,320
So if he teaches the Dhamma for cutting off birth, old age, sickness, and death, what's

22
00:03:47,320 --> 00:03:48,840
wrong with killing yourself?

23
00:03:48,840 --> 00:03:52,120
Because if you kill yourself, these things only last up into death, and if you kill

24
00:03:52,120 --> 00:03:58,400
yourself, then you don't have these things. And I guess the idea is, why wouldn't an

25
00:03:58,400 --> 00:04:03,800
Arahan? Because an ordinary person, obviously, it's not an escape. If you kill yourself,

26
00:04:03,800 --> 00:04:11,560
you're just going to be born again. But why wouldn't an Arahan do it? I think that's

27
00:04:11,560 --> 00:04:23,600
what he's saying. And then he said, why, why can, why does he teach both of these? Why

28
00:04:23,600 --> 00:04:28,600
can these both be true? I think, just one, why does he say it's wrong for an Arahan to kill

29
00:04:28,600 --> 00:04:37,920
himself? And the Buddha said, and then Nagasena says, wow, this is because a sea lover,

30
00:04:37,920 --> 00:04:49,400
a virtuous person, see the sampano, Agadasamos atana, is like, and then he gives these

31
00:04:49,400 --> 00:04:58,480
10, these 10 similes. One who has virtuous is like an antidote for destroying the poisons

32
00:04:58,480 --> 00:05:06,560
of the filaments and beings. So if you have this sickness of greed, anger, and delusion,

33
00:05:06,560 --> 00:05:17,640
what does the antidote? Where do you find the cure to the defilements of the mind? If you

34
00:05:17,640 --> 00:05:23,600
have an anger problem, if you have an addiction problem, what is the cure? The cure is a virtuous

35
00:05:23,600 --> 00:05:29,840
person. And by this he means an Arahan, someone who has understood the truth and who has

36
00:05:29,840 --> 00:05:38,080
freed themselves from all greed, anger, and delusion. You find such a person there, the

37
00:05:38,080 --> 00:05:45,040
antidote. And if you partake of them in terms of listening to their teaching, associating

38
00:05:45,040 --> 00:05:50,040
with them, listening to their teaching, remembering their teaching, understanding their

39
00:05:50,040 --> 00:06:01,680
teaching, and putting their teaching into practice, you can cure yourself. They are the antidote.

40
00:06:01,680 --> 00:06:05,960
That's the first one. There are 10 similes. The second is they are a healing bomb for

41
00:06:05,960 --> 00:06:14,640
a laying the sickness of defilements and beings. So if you have the fever of defilements,

42
00:06:14,640 --> 00:06:23,480
or seen as a fever, or greed as a fever, it makes you hot and bothered. Anger as a fever

43
00:06:23,480 --> 00:06:36,760
makes you, it burns you out. Delusion is a fever, it makes you drunk and uncontrollable.

44
00:06:36,760 --> 00:06:46,080
And this is a healing bomb that cools and soothes and kills your sickness. Number 3 is

45
00:06:46,080 --> 00:06:54,280
like a precious gem because they grant all beings their wishes. So there is this special

46
00:06:54,280 --> 00:07:04,640
gem in Indian mythology, a wishful filling gem. So this comes up in Buddhism often as

47
00:07:04,640 --> 00:07:10,800
a simile, talking about the wishful filling jewel. It's jewel if you have this. It's

48
00:07:10,800 --> 00:07:16,280
like the genian is lamp, but in Indian mythology, it's a gem. And if you rub the gem or

49
00:07:16,280 --> 00:07:25,160
if you possess the gem, all your wishes come true. And so in our hunt and lighten being

50
00:07:25,160 --> 00:07:35,440
a virtuous being is like this gem because they give people what they want happiness. Sometimes

51
00:07:35,440 --> 00:07:41,800
we don't even know what we want. We think we want something and see that in our hunt obviously

52
00:07:41,800 --> 00:07:48,000
can't give you riches and so on and power, but they can give you what you need I guess

53
00:07:48,000 --> 00:07:55,720
you would say. We can't give you what you want necessarily. But what we want is always

54
00:07:55,720 --> 00:08:03,600
happiness. We want certain things because we think they bring us happiness. We want satisfaction.

55
00:08:03,600 --> 00:08:09,240
We're just deluded into thinking that things that are unsatisfying are going to satisfy us.

56
00:08:09,240 --> 00:08:15,840
We're deluded into thinking that things that are impermanent and things that are uncontrollable

57
00:08:15,840 --> 00:08:29,280
are stable and controllable. Then lighten being when we follow their teachings. We find

58
00:08:29,280 --> 00:08:34,400
what is stable, satisfying and controllable. Well, not controllable actually, but that

59
00:08:34,400 --> 00:08:46,160
is what I said. Stable and satisfying anyway. It's number 3, number 4 is like a ship for

60
00:08:46,160 --> 00:09:14,040
beings to go beyond the four floods. Overcoming the defoundment. Overcoming the

61
00:09:14,040 --> 00:09:23,880
Azuma. Aweja Azuma. Awejoga. I think that's it. So, karma, sensuality, the flood of sensuality.

62
00:09:23,880 --> 00:09:30,280
Bao gai is becoming the flood of becoming.

63
00:09:30,280 --> 00:09:48,840
I think the third one is views. The flood of views. The flood of ignorance. But in general,

64
00:09:48,840 --> 00:09:54,920
it's just another way of describing the filaments. But the idea of the imagery of the ocean

65
00:09:54,920 --> 00:10:01,000
is a common one that some sara is like an ocean that we all drown and we flounder around

66
00:10:01,000 --> 00:10:09,400
him and never seeing the shore. We're just lost in this vast ocean of nothingness and

67
00:10:09,400 --> 00:10:18,920
meaninglessness until we find a ship that can sail us. Because if you're just swimming around

68
00:10:18,920 --> 00:10:24,520
in the ocean, you just go around in circles, you get nowhere. But if you have a ship, you can

69
00:10:24,520 --> 00:10:31,400
cut through the ocean. You can go in whatever direction you like. You can find the shore.

70
00:10:35,000 --> 00:10:41,560
He is like a caravan leader for taking beings across the desert of repeated births or

71
00:10:41,560 --> 00:10:48,920
rebirth is like a desert. We're always thirsting. We're always hot with our defilements and

72
00:10:48,920 --> 00:10:55,640
our addictions and our embers and the suffering that comes from old-age sickness and death.

73
00:10:56,840 --> 00:11:02,840
We suffer like being in a desert. But a caravan leader can lead us through these things.

74
00:11:04,840 --> 00:11:10,680
He's like the wind. He's like the wind for extinguishing the three fierce fires in beings.

75
00:11:10,680 --> 00:11:18,680
This is greed, anger and delusions. The wind, they come in and they douse the fire. They blow the

76
00:11:18,680 --> 00:11:25,960
fire out with the power of their teachings. They're like a great rain cloud for filling

77
00:11:25,960 --> 00:11:32,520
beings with good thoughts. So that a great rain cloud fills up the lakes and rivers.

78
00:11:32,520 --> 00:11:51,160
Brings rain and waters the crops and cools the land. So to a great leader, a great teacher, a great

79
00:11:51,160 --> 00:11:56,920
being someone who has purified themselves. This is someone who is enlightened and why they should

80
00:11:56,920 --> 00:12:10,520
stay around because they have so much to offer. The world is dry without enlightened beings,

81
00:12:12,360 --> 00:12:18,600
without them. You can't grow anything. People don't grow spiritually, not in any meaningful way.

82
00:12:18,600 --> 00:12:25,160
Just go around in circles. Dry up like crops, without water.

83
00:12:29,480 --> 00:12:34,040
Like a teacher for encouraging beings to train themselves someone who's skilled. It's interesting

84
00:12:34,040 --> 00:12:37,960
that it's like a teacher. It's supposed to just being a teacher.

85
00:12:40,120 --> 00:12:47,800
Ah, jadiya, Samoam. But it's an interesting because in many ways, in enlightened followers,

86
00:12:47,800 --> 00:12:53,400
the Buddha aren't considered teachers. We don't consider ourselves to be teachers, per se.

87
00:12:54,440 --> 00:12:58,280
We're friends. We're good friends. Why? Because we give good things.

88
00:12:59,960 --> 00:13:09,960
We as in Buddhists who practice. We have good things to give.

89
00:13:09,960 --> 00:13:15,720
And so, I mean, enlightened being in Arahant has the greatest to give.

90
00:13:17,560 --> 00:13:23,160
And yeah, so we don't consider, we should never, even if one becomes enlightened,

91
00:13:23,160 --> 00:13:29,400
one would not consider oneself a teacher. But rather a friend, someone who offers advice and passes

92
00:13:29,400 --> 00:13:35,880
on the Buddha's teaching. But they're like a teacher because they do the same. They might as

93
00:13:35,880 --> 00:13:40,920
well be called the teacher because they encourage beings to train themselves.

94
00:13:43,640 --> 00:13:51,320
And finally, like a good guide for pointing out to beings the path to security. So we're lost,

95
00:13:51,320 --> 00:14:01,400
we're so lost. We're going in the wrong direction. We're running around in circles.

96
00:14:01,400 --> 00:14:07,800
We can't find the way out. We've ever been in a large forest. It's quite scary. You need to get

97
00:14:07,800 --> 00:14:16,440
lost. You need to lose your direction. And you're constantly misjudging. So it's very easy to go

98
00:14:16,440 --> 00:14:19,960
around in circles because you think, oh, maybe I have to go a little bit to the left and you

99
00:14:19,960 --> 00:14:25,080
constantly think that until you wind up back. I did this once. I'll end up back where I started

100
00:14:25,080 --> 00:14:32,760
because I actually walked in the circle. It's very hard to get out of the forest if you don't

101
00:14:32,760 --> 00:14:36,040
know where you're going. You've never been there before if you don't have direction.

102
00:14:38,200 --> 00:14:43,480
If you don't have a guide that someone who has been through the forest who knows the forest and

103
00:14:43,480 --> 00:14:58,280
who knows the way out of the forest, can guide you out of the forest. So that's our demo for tonight.

104
00:14:59,160 --> 00:15:03,320
Reasons why not to kill yourself. It seems like killing yourself wouldn't be,

105
00:15:04,280 --> 00:15:09,240
you can easily explain it potentially by just the idea that a

106
00:15:09,240 --> 00:15:12,760
and enlightened being would have no reason to kill themselves.

107
00:15:14,200 --> 00:15:17,240
Killing yourself would require desire for it to all end.

108
00:15:19,880 --> 00:15:25,240
So it seems like an hour wouldn't have any desire to stick around but they would just

109
00:15:26,440 --> 00:15:30,200
stick around and wait to let things work themselves out.

110
00:15:32,840 --> 00:15:36,440
But it seems like there's something a little more here like the idea that

111
00:15:36,440 --> 00:15:44,760
they do things. They eat arms, for example, why they don't just stop eating. They take arms because

112
00:15:44,760 --> 00:15:54,280
of this, because of some sense of duty or some rightness to passing on what they have gained.

113
00:15:57,320 --> 00:16:05,160
And it seems they do. Our hands seem to teach and pass on the teaching rather than just kill

114
00:16:05,160 --> 00:16:12,680
themselves. Hey, we got a whole bunch of people online. Good to see, good crowd. Thank you all for

115
00:16:12,680 --> 00:16:23,720
showing up. So it looks like the iOS app is still in the works. I'm going to try to proactively

116
00:16:23,720 --> 00:16:31,000
revamp this website. We've got a new version of it but I just haven't put any effort into

117
00:16:31,000 --> 00:16:36,840
getting it online. So I'm going to work with the person. But that also means probably that the

118
00:16:36,840 --> 00:16:43,720
iOS app has to be fixed for the new server. I'm not sure. I'm going to have to work together to

119
00:16:43,720 --> 00:16:51,000
make this own. Get it secure as well. The server has to, our whole server has to, apparently has to

120
00:16:51,000 --> 00:17:01,800
have some kind of, you know, the HTTPS thing. It's not, it's a bit beyond my pay grade but somebody

121
00:17:01,800 --> 00:17:08,600
knows how to do it. So we got all work together to make this happen. If anybody wants to volunteer

122
00:17:08,600 --> 00:17:14,680
in our organization in any way, we're always looking for volunteers. I think we are.

123
00:17:14,680 --> 00:17:23,400
Anyway, get in touch and if we need volunteers we'll let you know as usual. As per last night,

124
00:17:23,400 --> 00:17:30,360
we're still looking for a steward. So if anybody wants to come and stay with us for a while

125
00:17:30,360 --> 00:17:47,880
and starting in July, that'd be great. So we've got a couple of questions. Does anyone know

126
00:17:47,880 --> 00:17:57,800
what are the five scundas? We call them kandas in Pali. Pandas are aggregates and these are

127
00:17:57,800 --> 00:18:08,920
root by the form. Vedana means feeling. Sanyam means recognition, so on. Sankara is

128
00:18:12,200 --> 00:18:20,040
mental formations. And vinyana is consciousness. So when you see something, there is the physical

129
00:18:20,040 --> 00:18:27,320
aspect. There is the Vedana, the feeling about it. Pleasant, painful, neutral. There is a recognition,

130
00:18:27,320 --> 00:18:33,720
you recognize it as a cat or so on that you see. There is the mental formations. It means you

131
00:18:33,720 --> 00:18:41,480
judge it or you like it or dislike it. You examine it and you react to it. And vinyana just

132
00:18:41,480 --> 00:18:59,560
means the consciousness that the fact that you're aware. What does unsystematic intention mean?

133
00:19:01,080 --> 00:19:06,760
Unsystematic is just a loose translation. That's not really what I think that's you're referring to.

134
00:19:06,760 --> 00:19:15,320
Ayoni somanasikara. Ayoni so means wise. Ayoni so means unwise. So when you observe something

135
00:19:16,200 --> 00:19:21,400
and you react to it, this is when you like it or dislike it. When you let yourself get caught up

136
00:19:21,400 --> 00:19:28,200
in reactions to it. If you have yoni somanasikara, it means you see it wisely. You see it as

137
00:19:28,200 --> 00:19:33,560
impermanent suffering in non-self. You see it as it is impermanent unsatisfying and controllable.

138
00:19:33,560 --> 00:19:39,480
You don't get attached to it. You see it objectively. Just as something that arises. That's

139
00:19:40,280 --> 00:19:44,280
wise attention. But wise is probably better than systematic.

140
00:19:51,080 --> 00:19:59,480
Are all fears instances of instances of dosa? Yeah, binyana isn't fear. According to the

141
00:19:59,480 --> 00:20:05,720
visudimanga, binyana doesn't fear because fear is always associated with patina. Actually,

142
00:20:05,720 --> 00:20:11,080
it's not the visudimanga. I think it's the commentary or the sub commentary that visudimanga

143
00:20:11,880 --> 00:20:14,200
asks, does binyana fear? No, it doesn't.

144
00:20:17,560 --> 00:20:22,680
I think maybe the visudimanga is the one that says no, it doesn't fear. And then the commentary

145
00:20:22,680 --> 00:20:29,400
to the visudimanga says, why doesn't it fear? Because fear is always associated with patina with

146
00:20:29,400 --> 00:20:39,000
aversion. So it's not really fear. Binyana isn't fear. Binyana is seeing the danger. When you see

147
00:20:39,000 --> 00:20:45,720
the danger and being reborn, you see the danger and in attachment, that kind of thing.

148
00:20:47,560 --> 00:20:52,440
It's like when you see fire off in the distance, this is what the visudimanga says.

149
00:20:52,440 --> 00:20:56,520
You see fire off in the distance. You think, wow, if anyone falls into that fire,

150
00:20:56,520 --> 00:21:00,920
they're going to burn themselves, but you yourself don't. You're not afraid because it's all

151
00:21:00,920 --> 00:21:09,720
it's over there. So nyana itself isn't afraid, but during meditation, of course, you can be afraid.

152
00:21:09,720 --> 00:21:12,440
It's just that the fear isn't nyana the knowledge.

153
00:21:12,440 --> 00:21:23,320
These are for Canada six months. Usually you can come to Canada for six months if you want to

154
00:21:23,320 --> 00:21:28,760
extend it. I think there are ways to extend it, but there's probably a lot of paperwork involved.

155
00:21:28,760 --> 00:21:42,600
I just got back from New York today. We drove since morning.

156
00:21:50,680 --> 00:21:55,240
Saturday, Sunday, we've got something that Cambodian monastery.

157
00:21:55,240 --> 00:22:01,480
We might have another meditator tomorrow, we'll see, but I'm also going to try to make it over

158
00:22:01,480 --> 00:22:07,800
to my father's house. We'll see how that works. How am I going to work that? We all be around.

159
00:22:08,760 --> 00:22:12,840
Next weekend is ways that don't forget Mississauga if you're in the area.

160
00:22:12,840 --> 00:22:26,840
Saturday the 28th, come on out.

161
00:22:26,840 --> 00:22:37,640
Are there any other questions?

162
00:22:37,640 --> 00:23:00,040
When a monk or a aunt or so forth passes on, would they be mindful like when falling asleep?

163
00:23:00,040 --> 00:23:08,840
If I eat trying to be mindful of each moment of a moment of transition, when they pass on

164
00:23:08,840 --> 00:23:22,440
the art born again, as for a monk, well, monks can be of course corrupt, so it doesn't say

165
00:23:22,440 --> 00:23:28,840
anything just because someone is a monk. Let's ask if a meditator would be mindful,

166
00:23:28,840 --> 00:23:33,480
like when falling asleep, not like when falling asleep, because death isn't like falling asleep,

167
00:23:34,280 --> 00:23:42,760
death is like an out of body experience. The body stops working, so the mind leaves the body,

168
00:23:43,560 --> 00:23:53,960
stops working with the body. Sometimes it takes a lot, but the mind eventually is over a period of

169
00:23:53,960 --> 00:24:02,120
time, sees that the body is, it reacts to the fact that the body is no longer working properly

170
00:24:02,120 --> 00:24:07,080
and that's why it leaves. So you have either a near death experience, if the body starts working,

171
00:24:07,080 --> 00:24:10,760
again, or you have a death experience, if the body doesn't ever stop, start working,

172
00:24:11,640 --> 00:24:15,480
and then the mind goes on and does whatever, maybe back to be born again.

173
00:24:17,560 --> 00:24:23,480
But if you're mindful, it's just this process becomes a lot smoother and you're less likely to

174
00:24:23,480 --> 00:24:29,720
make improper choices as with everything in life. During the time of death, it's

175
00:24:31,880 --> 00:24:37,080
probably pretty quick. It's a lot that goes on and if you're not mindful, it's easy to get lost

176
00:24:37,080 --> 00:24:38,040
and go the wrong way.

177
00:24:44,600 --> 00:24:50,120
The 20th here is actually the 20th in Thailand, I think, as we said, maybe the 21st,

178
00:24:50,120 --> 00:24:57,880
first, some places, but the 28th is when we're having our celebration here. So it's just a big

179
00:24:57,880 --> 00:25:00,760
thing here in Canada, in Ontario.

180
00:25:00,760 --> 00:25:26,760
Okay, well, I'm going to go spend a bit of a long day, but I'm trying,

181
00:25:26,760 --> 00:25:32,760
well, this next little while is going to be a little bit erratic, but for the next week or so,

182
00:25:32,760 --> 00:25:38,920
we should be fairly stable. So I'll be back again tomorrow, most likely.

183
00:25:38,920 --> 00:26:02,920
Anyway, have a good night, everyone.

