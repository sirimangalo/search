So, today I will be starting the story from the Jataka tales of the Buddha and these
are a set of tales that are said to have been told by the Lord Buddha to the monks on various
occasions whenever there was a lesson to be taught or something similar happened, similar
to what had happened in the past.
The Lord Buddha would take it as an occasion to give a talk about how these things happen
regularly or sometimes as a way of pointing out how someone's behavior was similar to their
behavior in the past, but this wasn't the first time that they had done this.
And it usually starts with this is not only now did such and such happen, but in the
past time also it happened before.
But the Jataka which I would like to start today is a very special one.
It's one of the ten great Jataka great stories which come at the end of the book and are
much longer and more sort of profound and exemplify in the various perfections of the Lord
Buddha and so this one is an example of the perfection of wisdom.
So once the teacher were dwelling in Jataka, walked in on the monks who were sitting
in the Dhamasala discussing the Buddha's perfection of wisdom as one as follows, oh great
is the omniscient Buddha whose wisdom is vast, whose wisdom is broad, whose wisdom is great,
who is able to crush heretical doctrines, who is able to convert, who is able to teach
both brahmanas and ascetics, thieves, demons, angels, gods, is able to humble, is able
to ordain the vast multitude and establish them in sainthood, in nobility and while
they were discussing such the teacher came and asked them what it was they were talking,
what was the subject of their talk and when they explained the Lord Buddha as was his
way, said not only now, has the teacher been wise, not only now has he been able to do
such things but even before his wisdom was fully mature he was still able to convert,
still able to teach and with that he told a story of the past.
The story goes as follows that once upon a time there was a king named Vidaha who ruled
in a city called Mitila and the king had four ministers who instructed him in religious
and secular affairs named Sainaka, Bhukusa, Govinda and Devinda.
But on the day that the Bodhisattva, the Buddha to be, was conceived in his mother's womb,
there came to the king a very special and exciting dream.
He saw in his dream four pillars of fire blazing up in the royal courtyard as high
as the great walls surrounding the courtyard and in the middle of them.
There arose a flame merely the size of a firefly but as the flame grew brighter and
brighter it suddenly sped up into the air and exceeded the four pillars of fire around
it and rose up as high as the heavens and illumined the whole world where even the smallest
thing on sitting on the ground could be seen distinctly.
And then the world of men in the world of angels and all of the beings in all of the
10,000 world system came to worship this pillar of fire with garlands and incense.
The vast multitude passed through its flame without being singed even a hair on their skin.
The king was terrified when he saw this vision and woke up with a start and sat up for
the rest of the night wondering worrying about what this dream could pretend.
When dawn came the four wise men, the four ministers, as usual, came to attend on the
king and asked him whether he had a good sleep.
His immediate answer was, how can I possibly sleep well when I've had such a dream and
he explained his dream to them?
Then Seneca, who is the head minister, replied to calm the fears of the king, oh fear not,
no king, it is an auspicious dream that will be prosperous and when the king asked him
to explain how he could be sure he said, oh king, the meaning of the dream is this, a fifth
sage will be born who will surpass the four of us.
We four are like the four pillars of fire which reach the height of the meanwhile but
don't overcome it.
In the midst of us there will arise as it were a fifth pillar of fire who is unparalleled
and fills the earth in the heavens and is unequalled in the world of gods and men.
Excited the king, asked quickly, where is the sage at this moment?
Oh king, as this dream foretends on this day he is either taking conception in his mother's
womb or coming to be born on this day.
And from that time forward the king remembered Seneca's words.
Now it happens that at the four gates of Matula there were four market towns called the
east town, the south town, the west town, and the north town.
And in the east town there dwelt a certain rich man named Siri Wataka and his wife
was named Sumana Devi.
Now on that day when the king saw the vision the great being, the Bodhisattva, while he
was in the Dawatingsa heaven, found that he had come to the end of his life in the
angel realms and was indeed conceived in the womb of Sumana Devi.
And a thousand other angels also on the same day went forth from that heaven and were
conceived in the wombs of the women in the families of various wealthy merchants in
the village as well.
And 10 months later the ladies Sumana Devi gave birth to a child, gave birth to the Bodhisattva.
And the Bodhisattva was the color of gold.
Now at the moment that the Bodhisattva was coming forth from the womb it happened that
Sakha or Indra, the king of the gods, was looking down from his great throne up in the
Dawatingsa heaven and was wondering where his old friend had been reborn.
And when he realized that the Bodhisattva had been, was about to be born, had been conceived
10 months earlier and was about to be born.
He thought to himself that he would find some way to let his fame be known or let it be
known that a Buddha seed had been planted and had sprung into being.
And so he flew down in an invisible form and placed a piece of a medicinal herb in the
hand of the Bodhisattva as he was coming out of the womb.
And the Bodhisattva grasped the medicinal root and as he was coming out she felt as though
water had been passed from a sacred pot.
There was no pain of childbirth whatsoever.
And when they took the baby out and saw the piece of medicinal herb they said, what
is it that you've got?
And the Bodhisattva replied in all clarity having newly issued from the womb, it is a medicinal
plant mother and saying so he placed it in her hands and told her that you should take
it and give it to all who were afflicted with any sickness.
This is how the Bodhisattva got the name Osatat which means medicine, Maha Osatat which
means the great medicine or the great medicine child.
The mother was overjoyed and gave the plant immediately to Siruataka who had suffered
for seven years from heading.
The father was amazed that not only was he born with a medicinal herb in his hand but
also that he had talked coming out of the womb.
And they say it is not true of the Bodhisattva that in every birth he issues from the
womb speaking but only in very special birth such as the Visantra birth, the perfection
of giving where the Bodhisattva coming forth from the womb immediately asked his mother
is there anything in the house to give?
This was the beginning of his perfection of giving but in this birth it was to be the
perfection of wisdom and so he started with a medicinal herb coming forth from his womb.
And when Siruataka took the medicine he found that his headache of seven years had disappeared.
Past away like water from a lotus leaf and so transported with joy he exclaimed this is
a medicine, a wonderful medicine, a marvelous efficacy.
And the new spread and all people came to know that there was a wonderful medicine in
the hand in the house of Siruataka.
And they came from fire and wide and all who were given were healed of all sicknesses.
And the merchant then thought to himself that surely this man, this boy possessing great
merit was not born alone and so he looked out through the village.
He sent out words through the village to see if there had been anyone, any other children
born on that day who would be fellows of the Bodhisattva and he found that there were
a great many other children that had been born who had come down from the same angel
world on the same day and were born on the same at the same time.
And let's say say his common of people who have great merit, that's with the Buddha in
his last life, he was born on the same day as Yesodhara, on the same day as Chanana and
the same day as Anandana, on the same day as Kantaka, his horse and so on.
I'm not sure about Anandana but there were several of them.
So these boys came together and were friends for seven years while the boy grew up.
And on one day when he was playing with them in the village, some elephants and other animals
passed by and disturbed their games, trampling their playground and striking fear into the
hearts of the other children.
And sometimes there was rain and sometimes there was heat and one day there was an unseasonable
rainstorm and when the great being saw it he ran for shelter and all the children who
were unable to follow us quickly after him as they were running from the storm they became
bruised and cut and hurt falling over each other.
So the great being thought to himself, indeed there is something must be done about this
and we need a hall to play and he immediately begun to put together plans to build a great
hall, a playground where they could stand, sit and lie in times of wind in hot sunshine
or rain and he had each of his friends bring one piece of money.
And so the great being sent for a master carpenter and gave him the money, telling him
to build a hall in such and such a place.
The carpenter took the money, levelled the ground and cut posts and spread out the measuring
line and made up the plans.
But he couldn't understand what the great being meant or the great being's intention
to build a hall.
So the great being said to him that this wasn't correct, that his plans were not what
he meant, not what he had in mind.
And the master carpenter said this is the best I can do, I'm not able to make a hall
of the kind that you ask.
And so the body sat bed, took the line himself and drew him, drew out the plans himself
and it was done as if the carpenter of the gods had done it himself.
Then he said ask the carpenter will you be able to make it, make it dust?
And the carpenter said no indeed I haven't a clue, I wouldn't never be able to make such
a hall.
And he said will you be able to follow my instructions and the carpenter said certainly
if you instruct me I'll be able to do as you say.
So the great being arranged the hall in a wonderful wondrous manner, such that there was
a place for ordinary strangers, a place for the destitute, a place for destitute men,
a place for destitute women, lodging for strange priests, ascetics and brahmins.
Another lodging for all sorts of other sorts of men and another a place where foreign merchants
should stow their goods and all the apartments had doors opening outside.
There was a public place for sports and a court of justice and a hall for religious
assemblies.
And when the work was completed he summoned painters and having himself examine them, set
them to work at painting beautiful pictures so that the hall became like the hall of
the angels.
Still he thought that the palace was not yet complete.
I must have a tank constructed as well he thought, a pool upon.
And so he ordered the ground to be dug for an architect and having discussed it with
him and giving him money he made him construct upon with a thousand bends in the bank and
a hundred bathing guts.
The water was covered with the five kinds of lotus and was as beautiful as the lake
in the heavenly garden Nandana.
On its bank he planted various trees and had a park made like Nandana and near the
hall he established a public distribution of alms to holy men, whether they be ascetics
or brahmins and for strangers and for people from the neighboring villages.
These actions of his were blazed abroad everywhere and crowds gathered to the place.
And the great being would sit in the hall and discuss right and wrong and good and evil
and judge the cases of all petitioners who resorted there and gave his judgment on each
and it became like the happy time when a Buddha makes his appearance in the world.
Now it came to pass that at that time King Videha remembered his dream and he asked himself
where could the sage be at this time?
And so he sent out four Councillors to the four by the four gates of the city, bidding
them to find out where the sage was.
When they went up by the other three gates they saw no sign of the great being.
But when they went up by the eastern gate they saw the hall and its various buildings
and they felt sure at once that only a very wise being could have built this palace
or had it built.
And so they asked the people what architect built this hall and the people replied this
palace was not built by any architect not by their own power anyway it was built under
the direction of Mahosata, the son of the merchant Siriwata and how old is this boy?
He has just completed his seventh year.
The Councillor counted it all from the day that the king had his dream and he thought
to himself indeed this being fulfills the king's dream and so he sent a messenger with
this message to the king.
Your Majesty Mahosata, the son of the merchant Siriwata in the east market town who is
now seven years old, has had a hall of such wonder and a tank and a park of such splendor
to be made.
Shall I bring him into thy presence or not?
When the king heard this he was highly delighted and sent for Seneca his head minister
and relating the account he asked Seneca whether he should send for the sage.
But Seneca being envious of the inevitable fame of the Bodhisattva replied, oh king, a man
is not to be called a sage merely because he has halls built and such things be made.
Anyone can do such trivialities, this is but a little matter.
When the king heard his words he thought to himself that Seneca must have his reasons
for his decision and was silent.
So he sent back the messenger with a command that the counselor should remain for a time
in the place and carefully examine the sage.
The counselor remained there and carefully investigated the sage's actions and this
as follows is the series of tests or cases of examination that were conducted in regards
to Mahosata.
Number one, the peace of meat.
One day when the great being was going to the play hall a hawk carried off a piece of
flesh from the slab of a slaughterhouse and flew up into the air.
Some lad seeing it determined to make him drop it and pursued him.
The hawk flew in different directions and they looking up followed behind and wearyed
themselves flinging stones and other missiles and stumbling over one another.
Then the sage, the sage, the bodhisattva said to them, I will make him drop it and
they begged him to do so.
He told them to watch.
And then himself, without looking up, ran with the swiftness of the wind and trot upon
the hawk's shadow and then clapping his hands uttered a loud shout.
By his energy that shout seemed to pierce the bird's belly through and through and in
its terror he dropped the flesh and the great being knowing by watching the shadow that
it was dropped, caught it in the air before it reached the ground.
The people seeing the marvel made a great noise shouting and clapping their hands.
The minister hearing of it sent an account to the king telling him how the sage had by
such and such a means made the bird drop the flesh.
The king hearing this asked Sainaka whether he should thus summon the boy to the court.
Sainaka thought to himself from the time of his coming onward, I shall lose all of
my glory and the king will forget my very existence.
I must not let the boy come to this palace.
So in envy he said, he is not a sage for such an action as this.
This is only a small matter.
The king being impartial sent word that the minister should test him further where he was.
Number two, the cattle.
A certain man who dwelt in the village of Yawamajika.
Bought some cattle from another village and brought them home.
The next day he took them to a field of grass to graze and rode on the back of one of
the cattle.
Being tired he got down and sat on the ground and fell asleep and meanwhile a thief came
and carried off the cattle.
When he woke he saw not his cattle but as he gazed on every side he beheld the thief
running away.
Jumping up he shouted, where are you taking my cattle?
They are my cattle and I am carrying them to the place which I wish.
A great crowd collected as they heard the dispute.
In the sage heard the noise as they passed by the door of the hall he sent for them
both.
When he saw their behavior he had once knew which was the thief and which was the real
owner.
But though he felt sure he asked them what they were quarreling about.
He understood I bought these cattle from a certain person in such a village and I brought
them home and put them in a field of grass.
This thief saw that I was not watching and came and carried them off looking in all directions
I caught sight of him and pursued and caught him.
The people of such a village know that I bought the cattle and took them.
The thief replied this man speaks falsely they were born in my house.
The sage said I will decide your case fairly will you abide by my decision and they promise
so to abide.
Then thinking to himself that he must win the hearts of the people he first asked the thief.
What have you fed these cattle with and what have you given them to drink?
The thief said they have drunk rice grow and they have been fed on sesame flour and kidney
beans.
Then he asked the real owner who said my lord how could a poor man like me get rice grow
in the rest?
I fed them on grass.
The wise man caused the people to assemble and brought together and ordered panics seed
to be brought and ground in a mortar and moistened with water and given to the cattle.
They forth with vomited only grass.
He showed this to the assembly and asked the thief.
And though the thief were not, the man confessed that he was the thief.
The Bodhisattva said to him they did not commit such a sin henceforth and was prepared
to let the man go with such a warning.
But the attendants of the Bodhisattva carried the man away and cut off his hands and feet
and made him helpless.
Such were the times.
When the sage addressed him with words of good counsel, this suffering has come upon the
only in this present life but in the future life that would suffer great torment in the
different hills and therefore henceforth abandoned such practices.
And he taught him the five percent not to kill, not to steal, and the rest.
The counselor sent news to the king telling of the incident who asked Seneca but Seneca
as usual advised him to wait.
He was only in a fair about cattle and anybody could decide it.
The king being impartial sent the same command.
Number three, the necklace of thread.
A certain poor woman had tied together several threads of different colors and made them
into a necklace which she took off from her neck and placed on her clothes as she went
down to bathe in a tank which the wise men had caused to be made.
A young woman who saw this conceived a longing for it, took it up and said to her mother,
this is a very beautiful necklace, how much did it cost you to make?
I will make such a one for myself, may I put it on my own neck and ascertain its size?
The other gave her leave and she put it on her neck and ran off.
The elder woman seeing it, came quickly out of the water and putting on her clothes ran
after her and seized hold of her dress crying, you were running away with a necklace which
I made.
The other replied, I am not taking anything of yours, it is the necklace which I wear, it
is the necklace which I wear on my neck.
And a great crowd collected as they heard this.
The sage while he played with the boys heard them quarreling as they passed by the door
of the hall and asked them what the noise was about.
When you heard the quack cause of the quarrel he sent for them both and having known at
once, by her countenance which was the thief he asked them whether they would abide by
his decision.
On their both agreeing to do so he asked the thief what scent do you use for this necklace?
She replied, I always use, I always use sabasan kara, sabasan hara kara to sent it with.
When he asked the other who replied, how shall a poor woman like me get sabasan kara kara?
I always sent it with perfume made of piyangu flowers.
Then the sage had a vessel of water brought and put the necklace in it and he sent for
a perfume seller and told them to smell the vessel and find out of which smell it was.
The perfume seller directly recognized the smell of the piyangu flower and quoted the
stanza which has already been given.
And this is one of the stanzas of the jataka which in the original form are just a series
of verses.
So the first verse is, no sabasan hara kara it is only the kangu smells.
You wicked woman, young wicked woman, told the lie that truth the gamma tells.
And so the great being told the bystanders all the circumstances and asked each of them
respectively.
Art thou the thief, art thou not the thief, and made the guilty one confess.
And from that time his wisdom became known to all the people.
Number four, the cotton thread.
A certain woman who used to watch cotton fields was watching one day and she took some
clean cotton and spun some fine thread and made it into a ball and placed it on her lap.
As she went home she thought to herself, I will bathe in the great sage's tank and so
she placed the ball in her dress and went down into the tank to bathe.
Another woman saw it and conceiving a longing for it took it up saying, oh this is a beautiful
ball of thread.
Pray did you make it yourself?
So she lightly slapped her snap her figures and put it in her lap as if to examine it
more closely and walked off with it.
And the rest as I was told before.
When they were brought to the sage the sage asked the thief, when you made the ball what
did you put inside it?
She replied a cotton seed.
Then he asked the owner and she replied a tambaru seed.
When the crowd had heard what each had said, he untwisted the ball of cotton and found
a tambaru seed inside and forced the thief to confess her guilt.
The great multitude were highly pleased and shouted their applause at the way in which
the case had been decided.
Number 5 The Sun
A certain woman took her son and went down to the sage's tank to wash her face.
After she had bathed her son she laid him in her dress and having washed her own face
went to bathe.
At that moment a female goblin saw the child and wished to eat it.
So she took hold of the dress and said, my friend, this is a fine child, is he your son?
Then she asked if she might give him suck and on obtaining the mother's consent she took
him and played with him for a while and then tried to run off with him.
The other ran after and seized hold of her shouting, with her are thou carrying my child?
The goblin replied, why do you touch the child he is mine?
As they wrangled they passed by the door of the hall and the sage hearing the noise,
sent for them and asked what was the matter.
When he heard the story, although he knew it once, by her red unwinking eyes that one
of them was a goblin, he asked them whether they would abide by his decision on their
promising to do so.
They drew a line and laid the child in the middle of the line and bade the goblins,
ceased the child by the hands and the mother by the feet.
And he said to them, lay a hold of it and pull.
The child is hers who can pull it over to one side of the line.
They both immediately began to pull and the child being pained while it was pulled, uttered
a loud cry.
Then the mother with a heart which seemed ready to burst, let the child go instead weeping.
Knowing that her child was lost.
The sage then asked the multitude, is it the heart of the mother which is tender towards
the child or the heart of her who is not the mother?
They answered the mother's heart.
So then is she the mother who kept hold of the child or she who let it go?
They replied, she who let it go.
Do you know who she is who stole the child?
We do not know of sage.
She is a goblin.
She seized it in order to eat it.
When they asked how he knew that he replied, I knew her by her unwinking and red eyes
and by her casting no shadow and by her fearlessness and want of mercy.
Then he asked her what she was and she confessed that she was a goblin.
Why did you seize the child to eat it?
You blind fool he said.
You committed sin in old time and so were born as a goblin and now you still go on
committing sin blind fool that you are.
Then he exhorted her and established her in the five precepts and center away and the
mother blessed him and saying, may I still live long, my lord took her son and went
her way.
Number six, the black ball.
There was a certain man who was called Golakala, now he got the name Golak Ball from
his dwarfish size and Gala from his black color Golakala means black ball.
He worked in a certain house for seven years and obtained a wife such was the custom
of the time and she was named Golakala.
One day he said to her wife, please you cook me some sweet meats and food and we will
pay a visit to your parents.
At first she was opposed, she opposed the plan saying, what have I to do with parents now?
But after the third time of asking he induced her to cook some cakes and having taken
some provisions and a present for her parents, he settled on the journey with her.
In the course of the journey he came to a stream which was not really deep, but they
being both afraid of water and not knowing how to swim, they are not cross it and stood
on the bank instead.
Now a poor man named Diga Beatin came to that place as he walked along the bank and when
they saw him they asked him whether the river was deep or shallow.
Seeing that they were afraid of the water he told him that it was very deep and full
of voracious fish.
How them will you get across it?
I have struck up a friendship with the crocodiles and monsters that live here and therefore
they do not hurt me.
Oh do you take us with you they said?
Then he consented they gave him some meat and drink and when he finished his meal he asked
them what she should carry over first.
Take my wife first and then take me to Golakala.
Then the man placed the wife his wife on his shoulders and took the provisions in the present
and went down into the stream.
When he had gone a little way he crouched down and walked along in a bent posture, Golak
ala as he stood on the bank thought to himself, this stream must indeed be very deep
if it is so difficult for even such a man as Diga Beatin, it must be impossible for me.
When the other had carried the woman to the middle of the stream he said to her, lady
I will cherish you and you shall live bravely arrayed with fine dresses and ornaments
and men servants and maidservants.
What will this poor dwarf do for you?
Listen to what I tell you.
You listen to his words and cease to love her husband and being at once infatuated with
the stranger she consented saying, if he will not abandon me I will do as you say.
So when they reached the opposite bank they amused themselves and left Golakala bidding
him stay where he was.
While he stood there looking on they ate up the meat and drink and departed.
When he saw it he exclaimed they have struck up a friendship in a running away leaving
me here.
As he ran backwards and forwards he went a little way into the water and then drew back
again and fear.
But then in his anger at their conduct he made a desperate leap saying, let me live or die.
And when once fairly and he discovered how shallow the water was.
So he crossed it and pursued him and shouted you, we could thief with our thou carrying
my wife.
The other replied, how is she your wife she is mine and he seized him by the neck and
whirled him round and threw him off.
The other laid hold of Diga Talas hand and shouted, stop where are you going?
You are my wife whom I got after working for seven years in a house.
And as he thus disputed he came near the home, a great crowd collected.
The great being asked what the noise was about and having sent for them and heard what
he should said, he asked whether they would abide by his decision.
On their both agreeing to do so he sent for Diga Pity and asked him his name.
Then he asked him his wife's name, but he not knowing what it was mentioned some other
name.
Then he asked him the name of his parents and he told them, but when he asked in the name
of his wife's parents a man not knowing mentioned some other names.
The great being put his story together and had him removed.
Then he sent for the other and asked him the names of all in the same way.
He knowing the truth gave them correctly.
Then he had him removed and sent for Diga Tala and asked her what her name was and she gave
it.
Then he asked her her husband's name and she not knowing gave a wrong name.
Then he asked her her parents name and she gave them correctly, but when he asked her the
names of her husband's parents names, she talked at random and gave wrong names.
Then the sage sent for the other two and asked the multitude, does the woman's story agree
with Diga Pity or Golakala?
They replied with Golakala, then he pronounced the sentence.
This man is her husband, the other is a thief and when he asked the man, he was made
to confess that he had acted as the thief.
Number seven, the chariot.
A certain man who was sitting in a chariot, alighted from it to wash his face.
Now at that moment, Sakkad, the king of the gods, who often takes interest in the workings
of men, was considering how he could make known the power and wisdom of Mahosatah, the
Buddha to be.
So he came down in the form of a man and followed the chariot holding on behind the
man who sat in the chariot asked, why have you come?
Sakkad replied to serve you.
The man agreed in dismounting from the chariot, went aside at a call of nature.
Immediately Sakkad mounted the chariot and went off at all speed.
The owner of the chariot, his business done returned and when he saw Sakkad hide away with
the chariot, he ran quickly behind crying, stop, stop, where are you taking my chariot?
Now replied, your chariot must be another, this is mine.
Thus wrangling they came to the gate of the hall of the sage asked, what is this and
sent for him?
As he came by his fearlessness in his eyes which winked not, the sage knew that this
was Sakkad, the king of the gods, and the other was the owner.
Nevertheless he inquired the cause of the quarrel and asked them, will you abide by my decision?
They said yes, and he went on that.
I will cause the chariot to be driven and you must both hold on behind.
The owner will not let go the other will.
Then he told the man to drive the chariot and he did so the other's holding on behind.
The owner went a little way then being unable to run further he let go, but Sakkad went
on running with the chariot.
When he recalled the chariot, the sage said to the people, this man ran a little way and
let go.
The other ran out with the chariot and came back with it, yet there is not a drop of sweat
on his body, no panting.
He is fearless, his eyes winked not.
This is Sakkad, king of the gods.
Then he asked, are you king of the gods?
Sakkad answered yes, why did you come here to spread the fame of your wisdom, Osage?
Things that the sage don't do that kind of thing again, and Sakkad revealed his power
by standing poised in the air and praising the sage saying, why is judgment indeed?
And went back to the heavens.
Then the counselor, unsum and went to the king and said, oh great king, thus was the
chariot question resolved, and even Sakkad was subdued by him.
Why do you not recognize superiority in men?
Then the king asked Seneca, what say you Seneca shall we bring the sage here, Seneca replied,
but it's not all that makes a sage wait a while, I will test him and find out.
I'm going to stop there.
This ends the trials of the Bodhisattva, and next we get into the tests, how he was tested
by the king and the king's ministers.
