1
00:00:00,000 --> 00:00:02,540
So you don't have enough time to meditate, you always have time.

2
00:00:02,540 --> 00:00:05,840
You can meditate right now, you can meditate while you're eating,

3
00:00:06,800 --> 00:00:08,520
that will you're drinking a cup of tea.

4
00:00:13,660 --> 00:00:14,320
Too stinky.

5
00:00:21,760 --> 00:00:23,280
It was another part to the question.

6
00:00:23,280 --> 00:00:23,840
Okay, go for it.

7
00:00:24,920 --> 00:00:29,640
Having balanced posture while doing sitting meditation, I am a bit tall

8
00:00:29,640 --> 00:00:31,800
and when holding the hands close to the abdomen,

9
00:00:31,800 --> 00:00:33,920
with thumbs touching, there is a great strain

10
00:00:33,920 --> 00:00:36,680
on the leg muscles to maintain balance.

11
00:00:36,680 --> 00:00:39,080
So sitting meditation becomes an object of pain

12
00:00:39,080 --> 00:00:42,600
and strain only after, only,

13
00:00:42,600 --> 00:00:45,120
especially after five to 10 minutes.

14
00:00:45,120 --> 00:00:47,840
The only way for me to maintain it without losing balance

15
00:00:47,840 --> 00:00:51,160
is to place my hands on the side of my knees.

16
00:00:51,160 --> 00:00:53,600
Should I crunch it because you once mentioned

17
00:00:53,600 --> 00:00:55,680
it should be comfortable sitting.

18
00:00:55,680 --> 00:00:59,440
Well, you want to ease into it.

19
00:00:59,440 --> 00:01:01,680
You don't want to make it completely comfortable,

20
00:01:01,680 --> 00:01:04,480
but it sounds like you're in a position like I was,

21
00:01:04,480 --> 00:01:07,480
that it's totally unbearable.

22
00:01:07,480 --> 00:01:09,760
So you want to find a sort of a medium,

23
00:01:09,760 --> 00:01:13,480
not a happy medium, but it's somehow a middle way

24
00:01:13,480 --> 00:01:16,520
that's slowly easing you into it because it will get better

25
00:01:16,520 --> 00:01:18,120
and you will become more tolerant

26
00:01:18,120 --> 00:01:20,400
and eventually you should be able to sit on the floor

27
00:01:20,400 --> 00:01:22,000
if you keep up with it.

28
00:01:22,000 --> 00:01:25,280
But you want to sit with your bottom a little higher,

29
00:01:25,280 --> 00:01:27,200
sit with some padding under your knees,

30
00:01:27,200 --> 00:01:29,880
under your knees, keeping them propped up,

31
00:01:29,880 --> 00:01:32,240
do that in the beginning with the goal

32
00:01:32,240 --> 00:01:34,800
that you should slowly reduce the padding,

33
00:01:37,040 --> 00:01:38,360
which means you're going to have to,

34
00:01:38,360 --> 00:01:39,960
to some extent, put up with the pain.

35
00:01:39,960 --> 00:01:42,040
I mean, there's nothing wrong with meditating on pain.

36
00:01:42,040 --> 00:01:45,280
It's just, you don't want it to be too overwhelming.

37
00:01:45,280 --> 00:01:48,720
So reduce it and make it more manageable, certainly,

38
00:01:48,720 --> 00:01:51,560
but don't try and find a way to get rid of it.

39
00:01:51,560 --> 00:01:53,440
And certainly, don't force your legs down,

40
00:01:53,440 --> 00:01:55,880
putting your hands on your knees is probably

41
00:01:55,880 --> 00:01:57,920
too much forcing the issue.

42
00:01:59,480 --> 00:02:01,400
And when you say, just put one hand on top of the other,

43
00:02:01,400 --> 00:02:02,400
you don't have your thumbs,

44
00:02:02,400 --> 00:02:05,240
that changes naturally on top of each other's enough.

45
00:02:07,480 --> 00:02:10,520
So go easy on it, and if it gets too painful,

46
00:02:10,520 --> 00:02:13,240
you can always sit on a bench or a chair sometimes,

47
00:02:13,240 --> 00:02:16,120
but you should be working on getting to sit on the floor.

48
00:02:16,120 --> 00:02:21,120
It's a good line in the sand, or a goal to reach

49
00:02:21,120 --> 00:02:24,800
or a goal to, to reach, to look, look for.

50
00:02:24,800 --> 00:02:26,400
It's a very physical sort of goal,

51
00:02:26,400 --> 00:02:28,240
but it's an accomplishment,

52
00:02:28,240 --> 00:02:29,400
because once you're sitting on the floor,

53
00:02:29,400 --> 00:02:30,320
it says two things.

54
00:02:30,320 --> 00:02:33,000
It says that you've put up with pain,

55
00:02:33,000 --> 00:02:38,000
that you've successfully born with the pain,

56
00:02:38,200 --> 00:02:41,320
and, and two that you've started to relax,

57
00:02:41,320 --> 00:02:44,920
because to get in the proper sitting position on the floor,

58
00:02:44,920 --> 00:02:47,240
it doesn't, it doesn't come from stretching,

59
00:02:47,240 --> 00:02:49,280
it comes from relaxing.

60
00:02:49,280 --> 00:02:50,800
So you're pushing down on your knees,

61
00:02:50,800 --> 00:02:52,200
or all that, it's just stretching,

62
00:02:52,200 --> 00:02:53,680
it's not real relaxing.

63
00:02:54,960 --> 00:02:56,520
You want to get there naturally,

64
00:02:57,480 --> 00:02:59,440
think of it as kind of a goal.

65
00:02:59,440 --> 00:03:00,840
I think that's a good one,

66
00:03:00,840 --> 00:03:02,200
once we should all think about,

67
00:03:02,200 --> 00:03:05,040
or, you know, beginner meditators should all think about,

68
00:03:05,040 --> 00:03:06,920
get to this position, and get to it,

69
00:03:06,920 --> 00:03:09,640
and then use it as a, as a test,

70
00:03:09,640 --> 00:03:12,480
to sign if your knees are way up here, you know.

71
00:03:12,480 --> 00:03:15,080
Okay, well, once your knees get down here,

72
00:03:15,080 --> 00:03:17,040
you can feel like you've accomplished two things.

73
00:03:17,040 --> 00:03:20,080
One, you've accomplished pace and patience.

74
00:03:20,080 --> 00:03:22,680
And two, you've become more relaxed,

75
00:03:22,680 --> 00:03:25,040
less clinging, less stressing.

76
00:03:27,720 --> 00:03:30,480
Which is a big part of why the body of so tense.

77
00:03:35,480 --> 00:03:36,680
One day I haven't,

78
00:03:36,680 --> 00:03:39,840
haven't trouble meditating due to tinnitus,

79
00:03:39,840 --> 00:03:42,000
constant ringing in the hearers.

80
00:03:42,000 --> 00:03:43,880
Despite trying to see it as hearing,

81
00:03:43,880 --> 00:03:47,360
I feel that it is being a hindrance to the meditation.

82
00:03:47,360 --> 00:03:49,000
I was thinking about turning a fan on

83
00:03:49,000 --> 00:03:51,120
when I meditate to cover up their ringing,

84
00:03:51,120 --> 00:03:53,240
until I felt ready to go without it.

85
00:03:53,240 --> 00:03:55,640
But could this class more aversion to the ringing?

86
00:03:55,640 --> 00:03:57,560
Do you have any advice? Thank you.

87
00:03:58,680 --> 00:04:02,520
Yeah, that would generally class more, more aversion.

88
00:04:04,480 --> 00:04:05,760
There's nothing wrong with hearing,

89
00:04:05,760 --> 00:04:08,400
and in fact, that's a very powerful,

90
00:04:08,400 --> 00:04:09,960
because it's so constant,

91
00:04:11,240 --> 00:04:14,760
it's a very powerful meditation tool,

92
00:04:14,760 --> 00:04:16,920
something that you can focus on.

93
00:04:16,920 --> 00:04:19,160
And because it's something that you don't like,

94
00:04:20,520 --> 00:04:23,680
all the better, it helps you learn to overcome disliking.

95
00:04:25,240 --> 00:04:26,840
So it's a good challenge for you.

96
00:04:26,840 --> 00:04:28,800
I had a meditator hood, very,

97
00:04:28,800 --> 00:04:31,280
well, actually moderate tinnitus, I would think.

98
00:04:31,280 --> 00:04:32,640
I think it was not that bad,

99
00:04:32,640 --> 00:04:35,680
but it was fairly constant and at the same problem.

100
00:04:35,680 --> 00:04:37,480
I mean, it's not a really simple thing,

101
00:04:37,480 --> 00:04:39,080
because it's not gonna get better,

102
00:04:39,080 --> 00:04:41,800
and then you can go back to your meditation out.

103
00:04:41,800 --> 00:04:44,240
Probably gonna be there throughout your meditation.

104
00:04:44,240 --> 00:04:45,840
So you have to incorporate it,

105
00:04:45,840 --> 00:04:47,880
but you shouldn't see it as a hindrance.

106
00:04:47,880 --> 00:04:48,880
A hindrance to what?

107
00:04:50,080 --> 00:04:52,160
A hindrance to quiet, yes.

108
00:04:53,080 --> 00:04:56,000
But we're not concerned about physical quiet.

109
00:04:58,360 --> 00:05:00,320
Like, right now I have this,

110
00:05:00,320 --> 00:05:05,320
this worrying sound of the laptop,

111
00:05:06,560 --> 00:05:07,920
and that's constant, it's always there,

112
00:05:07,920 --> 00:05:08,960
but it doesn't bother me.

113
00:05:08,960 --> 00:05:13,960
And so, you know, some people can live their whole lives

114
00:05:16,040 --> 00:05:19,160
with very loud noise and doesn't bother them.

115
00:05:19,160 --> 00:05:21,560
If you go to Asia, it's quite impressive

116
00:05:21,560 --> 00:05:26,480
in some of these more highly dense, populated

117
00:05:26,480 --> 00:05:31,480
and less technologically advanced

118
00:05:31,480 --> 00:05:36,480
or less rich affluent countries.

119
00:05:36,480 --> 00:05:39,960
Where everyone is crammed in and loud noises

120
00:05:39,960 --> 00:05:41,920
and just craziness, you think,

121
00:05:41,920 --> 00:05:43,200
how do these people stand in?

122
00:05:43,200 --> 00:05:45,720
No one notices, no one is concerned by it,

123
00:05:45,720 --> 00:05:47,160
because it's ordinary for them.

124
00:05:48,880 --> 00:05:52,640
We, in our monastery every year, they have two weeks,

125
00:05:52,640 --> 00:05:57,640
they have this carnival on the monastery property,

126
00:05:59,000 --> 00:06:02,160
and they've got these walls of sound loudspeakers,

127
00:06:02,160 --> 00:06:07,160
blaring this electronic time music,

128
00:06:07,400 --> 00:06:11,120
the electronic, but like the drum loop beat thing,

129
00:06:12,480 --> 00:06:16,440
24 hours a day almost, 20 hours a day maybe.

130
00:06:18,200 --> 00:06:20,560
And so, I've had to do meditation courses through that,

131
00:06:20,560 --> 00:06:22,760
it's quite, quite challenging.

132
00:06:23,760 --> 00:06:26,160
That's worse because it's got a rhythm to it, right?

133
00:06:27,440 --> 00:06:30,160
Yeah, not bad, but there's also got words

134
00:06:30,160 --> 00:06:32,480
and music and singing to it.

135
00:06:34,760 --> 00:06:37,640
Tinnitus is real, it's sound.

136
00:06:37,640 --> 00:06:39,120
Look, read through the Buddha's teaching,

137
00:06:39,120 --> 00:06:40,640
how often do you hear the Buddha talk?

138
00:06:40,640 --> 00:06:44,640
Do you see the Buddha talking about sound hearing?

139
00:06:44,640 --> 00:06:48,360
Hearing is a part of the dhamma, it's not a hindrance,

140
00:06:48,360 --> 00:06:50,160
it's real, it's reality.

141
00:06:51,600 --> 00:06:53,400
And reality is what we're trying to understand,

142
00:06:53,400 --> 00:06:57,240
so that's a very useful meditation object.

143
00:06:57,240 --> 00:06:59,760
Consider yourself trying to consider yourself

144
00:06:59,760 --> 00:07:04,520
somehow lucky to have such an interesting meditation

145
00:07:04,520 --> 00:07:07,960
object to focus on, to challenge you.

146
00:07:09,400 --> 00:07:11,440
Certainly not a hindrance, it's not getting in the way

147
00:07:11,440 --> 00:07:15,520
of anything, it's like saying reality is a hindrance,

148
00:07:15,520 --> 00:07:16,520
it's basically what you're saying,

149
00:07:16,520 --> 00:07:20,120
is that you feel like reality is a hindrance to what?

150
00:07:20,120 --> 00:07:24,400
Reality is what we're trying to see, so see it.

151
00:07:27,520 --> 00:07:29,440
Even if you just, even if it's just been sitting there

152
00:07:29,440 --> 00:07:31,200
saying hearing for 10 minutes,

153
00:07:33,160 --> 00:07:37,480
there will be times where your mind gets comfortable with it,

154
00:07:37,480 --> 00:07:40,040
and then you can go back to the stomach, rising and falling,

155
00:07:40,040 --> 00:07:42,000
but if your mind gets caught up in the hearing

156
00:07:42,000 --> 00:07:44,400
and then go back to it, eventually your mind

157
00:07:44,400 --> 00:07:46,000
will become fairly comfortable with it,

158
00:07:46,000 --> 00:07:49,320
you shouldn't find it overwhelming,

159
00:07:49,320 --> 00:07:51,840
such that other things, you're not able to experience them,

160
00:07:51,840 --> 00:07:53,240
because it's just like,

161
00:07:54,640 --> 00:07:56,520
I mean if it's very loud,

162
00:07:56,520 --> 00:07:57,640
it'll take maybe more time,

163
00:07:57,640 --> 00:07:59,680
but it's just like environmental noise.

164
00:07:59,680 --> 00:08:01,880
Some people deal with very loud environmental noise,

165
00:08:01,880 --> 00:08:05,040
and you can meditate with very loud environmental noise,

166
00:08:05,040 --> 00:08:08,320
without always having your mind focused on the sound,

167
00:08:10,280 --> 00:08:11,520
it just takes practice.

168
00:08:15,960 --> 00:08:17,520
Covering it up would not be useful.

169
00:08:19,040 --> 00:08:22,160
That would be saying to yourself that you're reaffirming

170
00:08:22,160 --> 00:08:27,160
the dislike and the intolerance, just look good.

171
00:08:32,360 --> 00:08:33,400
Hiya, Tadaamu.

172
00:08:33,400 --> 00:08:35,120
I always wanted to become a monk,

173
00:08:35,120 --> 00:08:37,480
however I have a student loan.

174
00:08:37,480 --> 00:08:40,000
I have enough funds in the bank to repay it,

175
00:08:40,000 --> 00:08:42,400
however to access most of the funds

176
00:08:42,400 --> 00:08:45,640
I have to stay overseas for more than a year.

177
00:08:45,640 --> 00:08:49,200
I need to stay as a late meditator in the monastery for a year.

178
00:08:49,200 --> 00:08:51,880
This is so I can access my bank account

179
00:08:51,880 --> 00:09:04,840
to not break

180
00:09:10,840 --> 00:09:14,200
A year-long time visa is not easy to get.

181
00:09:14,200 --> 00:09:15,560
I think it was made up more difficult.

182
00:09:15,560 --> 00:09:17,200
I don't really know, maybe they made it easy now,

183
00:09:17,200 --> 00:09:20,560
that easier now that tourism's declining.

184
00:09:20,560 --> 00:09:26,320
If it's still declining, it's very difficult.

185
00:09:26,320 --> 00:09:28,320
There's no such visa, until you become a monk,

186
00:09:28,320 --> 00:09:34,800
you can't get a religious visa,

187
00:09:34,800 --> 00:09:39,560
unless you're a woman, women can get the eight precepts,

188
00:09:39,560 --> 00:09:42,160
none visa, you have to become a nun, shave your head.

189
00:09:42,160 --> 00:09:45,600
But I tried, we had one man who couldn't become a monk

190
00:09:45,600 --> 00:09:48,200
because he was Iranian,

191
00:09:48,200 --> 00:09:50,520
and he would never be able to go back to his country

192
00:09:50,520 --> 00:09:55,240
if he became a monk, and his mother wasn't going to give him permission as well.

193
00:09:55,240 --> 00:09:58,560
So we worked really hard to try and get him a nun visa.

194
00:09:58,560 --> 00:10:02,440
I went and argued with the people at the Buddhism Affairs office

195
00:10:02,440 --> 00:10:04,120
in Chiang Mai.

196
00:10:04,120 --> 00:10:05,600
I said, look, women can do it.

197
00:10:05,600 --> 00:10:07,680
Here's a guy, he wants to become a nun.

198
00:10:07,680 --> 00:10:09,200
He said, can't he become a nun?

199
00:10:09,200 --> 00:10:13,240
And I said, I had proof, I backed up by this Sri Lankan tradition

200
00:10:13,240 --> 00:10:17,760
of Anagarika, they wouldn't go for it.

201
00:10:17,760 --> 00:10:25,160
They said, no, no, because he already has a path for him,

202
00:10:25,160 --> 00:10:27,400
he can become a monk.

203
00:10:27,400 --> 00:10:30,760
And because men can become a monk, we don't allow that.

204
00:10:30,760 --> 00:10:36,560
It was all just arbitrary garbage, but that's such is politics.

205
00:10:36,560 --> 00:10:38,600
I mean, that's what you find is a lot of arbitrary rules

206
00:10:38,600 --> 00:10:40,400
that don't really make much sense.

207
00:10:40,400 --> 00:10:44,480
I mean, most of the time to get the visa,

208
00:10:44,480 --> 00:10:49,240
you need a letter of support from a monastery,

209
00:10:49,240 --> 00:10:51,120
but no monastery is going to give you that letter

210
00:10:51,120 --> 00:10:52,600
until you go and stay with them.

211
00:10:52,600 --> 00:10:55,360
So it's like a catch 22, I think.

212
00:10:55,360 --> 00:10:59,560
You have to go there to get the letter, but to stay there,

213
00:10:59,560 --> 00:11:01,520
you need the letter.

214
00:11:01,520 --> 00:11:05,840
So what you have to do is go, meditate, leave,

215
00:11:05,840 --> 00:11:09,440
or I mean, arrange to get the letter, leave, apply

216
00:11:09,440 --> 00:11:12,400
for the proper visa, go back.

217
00:11:12,400 --> 00:11:16,160
It's a two-step process.

218
00:11:16,160 --> 00:11:18,680
And the first step, you can only stay there.

219
00:11:18,680 --> 00:11:20,040
It depends what kind of visa you get.

220
00:11:20,040 --> 00:11:23,560
Some places, some countries, you'll only get a month.

221
00:11:23,560 --> 00:11:26,360
And Canada, I usually am able to get three months.

222
00:11:26,360 --> 00:11:28,760
Two months is easy, but three months I can apply for.

223
00:11:28,760 --> 00:11:32,800
And I've never been denied in Canada, a three month time visa.

224
00:11:32,800 --> 00:11:34,560
But I think I have heard of some people getting

225
00:11:34,560 --> 00:11:38,440
denied a three month, even in Canada.

226
00:11:38,440 --> 00:11:51,600
So I don't know if it doesn't sound like that.

227
00:11:51,600 --> 00:11:53,280
What would I advise you to do, I guess,

228
00:11:53,280 --> 00:11:55,320
let's ask that.

229
00:11:55,320 --> 00:11:57,960
You need to stay in the monastery for a year.

230
00:11:57,960 --> 00:12:00,720
It's a little bit too worldly for me to, hey, wait,

231
00:12:00,720 --> 00:12:02,520
this isn't a question I'm supposed to be asking.

232
00:12:02,520 --> 00:12:04,840
This is a worldly question.

233
00:12:04,840 --> 00:12:12,800
I'm sorry, but I can't go into too much detail there.

234
00:12:12,800 --> 00:12:16,640
Beyond what I already have, so that's unreasonable.

235
00:12:16,640 --> 00:12:19,440
Good luck.

236
00:12:19,440 --> 00:12:23,080
The Asian visa system is just messed up.

237
00:12:23,080 --> 00:12:25,000
I'm Asian, I guess that's a blanket statement,

238
00:12:25,000 --> 00:12:27,680
but I can't imagine it being any better in other countries

239
00:12:27,680 --> 00:12:29,080
from what I've seen.

240
00:12:29,080 --> 00:12:32,120
Burma might be a little bit, but it's still some of the regulations

241
00:12:32,120 --> 00:12:35,000
they have are just, I mean, some of these countries

242
00:12:35,000 --> 00:12:38,440
are strong military presence in Thailand.

243
00:12:38,440 --> 00:12:42,120
The police force runs the immigration.

244
00:12:42,120 --> 00:12:44,080
And it's weird because the police force

245
00:12:44,080 --> 00:12:46,200
is one of these political factions.

246
00:12:46,200 --> 00:12:50,880
It's really, it's in Sri Lanka.

247
00:12:50,880 --> 00:12:55,520
There's got these similar bureaucratic nightmare.

248
00:12:55,520 --> 00:12:57,480
I remember in Sri Lanka going back and forth

249
00:12:57,480 --> 00:13:00,000
between the Buddhism office and the immigration.

250
00:13:00,000 --> 00:13:02,640
I went back and forth five times, and it was funny

251
00:13:02,640 --> 00:13:05,480
because I go to one and they'd say, well, we want to help you.

252
00:13:05,480 --> 00:13:08,440
But the Buddhism offices, and they're the ones

253
00:13:08,440 --> 00:13:11,960
who are holding it, they've tied our hands.

254
00:13:11,960 --> 00:13:14,080
And I go to the Buddhism office, and I tell them,

255
00:13:14,080 --> 00:13:15,440
and they say, that's not true.

256
00:13:15,440 --> 00:13:16,440
We'll do whatever we can.

257
00:13:16,440 --> 00:13:18,560
We'd love to help you, but it's the immigration office

258
00:13:18,560 --> 00:13:20,520
that is tying our hand.

259
00:13:20,520 --> 00:13:24,640
And it's this classic, and they get passed around.

260
00:13:24,640 --> 00:13:25,680
It's like hot potato.

261
00:13:25,680 --> 00:13:29,840
I wouldn't have been fond if it weren't for the fact.

262
00:13:29,840 --> 00:13:32,000
Thank you for the first thing.

263
00:13:32,000 --> 00:13:33,000
Sorry?

264
00:13:33,000 --> 00:13:35,320
You're bursting all my hands.

265
00:13:35,320 --> 00:13:39,520
You're bursting all my bubbles of fat going to Asia

266
00:13:39,520 --> 00:13:42,880
would be so wonderful and so easy.

267
00:13:42,880 --> 00:13:47,280
Yeah, well, stick with me on first on your dangels.

268
00:13:47,280 --> 00:13:51,760
I'm sorry, but Canada is much more comfortable.

269
00:13:51,760 --> 00:13:53,360
I mean, Sri Lanka is a great place.

270
00:13:53,360 --> 00:13:54,960
Don't get me wrong.

271
00:13:54,960 --> 00:13:57,840
Thailand, I have some issues with.

272
00:13:57,840 --> 00:14:02,440
But Sri Lanka is a great place to live as a monk.

273
00:14:02,440 --> 00:14:03,720
But it's getting that visa.

274
00:14:03,720 --> 00:14:08,720
The bureaucracy is not fun, and it works much better

275
00:14:08,720 --> 00:14:09,560
if you're a dangels.

276
00:14:09,560 --> 00:14:15,160
What we're going to try to do with, no, this name,

277
00:14:15,160 --> 00:14:18,040
guy from Norway.

278
00:14:18,040 --> 00:14:19,680
Look at how bad I am with names.

279
00:14:19,680 --> 00:14:21,480
Yeah, I work in Europe.

280
00:14:21,480 --> 00:14:24,240
Alexander, that's him.

281
00:14:24,240 --> 00:14:25,680
If he does become a monk, we'll

282
00:14:25,680 --> 00:14:30,360
have him become a monk here, and then send him to Thailand.

283
00:14:30,360 --> 00:14:32,080
Or not, this is Sri Lanka.

284
00:14:32,080 --> 00:14:35,320
Maybe Thailand, but more likely Sri Lanka.

285
00:14:35,320 --> 00:14:38,120
Try and think of a place or contact a place

286
00:14:38,120 --> 00:14:42,120
where he'd be able to learn how to be a good monk

287
00:14:42,120 --> 00:14:44,520
in a monastery setting with lots of monks.

288
00:14:44,520 --> 00:14:46,080
Maybe Nisa, I wouldn't be a good one,

289
00:14:46,080 --> 00:14:47,560
because it's a meditation center.

290
00:14:47,560 --> 00:14:52,080
And he learned quite well how to become a really good monk there.

291
00:14:52,080 --> 00:14:57,120
That's right, impressive place, well organized.

292
00:14:57,120 --> 00:15:03,600
Maybe a little too regimented and ritualistic, but it's pretty good.

293
00:15:03,600 --> 00:15:10,000
And see, that's the thing, is once he's already a monk,

294
00:15:10,000 --> 00:15:11,920
they do a lot more for you.

295
00:15:11,920 --> 00:15:15,520
They're not so keen to help people who are not yet monks.

296
00:15:15,520 --> 00:15:18,280
But once you're a monk, they'll give you free visas,

297
00:15:18,280 --> 00:15:20,440
and yearly visas in Sri Lanka are actually quite easy.

298
00:15:20,440 --> 00:15:22,480
Once they're in the system.

299
00:15:22,480 --> 00:15:24,000
So again, it's quite arbitrary.

300
00:15:24,000 --> 00:15:29,840
The points that you get blocked at are kind of silly in that way,

301
00:15:29,840 --> 00:15:32,560
because certain aspects of it are often very easy,

302
00:15:32,560 --> 00:15:38,120
and certain aspects are just ridiculously hard and unreasonable.

303
00:15:38,120 --> 00:15:42,120
And then people just are too lazy to try and change things,

304
00:15:42,120 --> 00:15:43,920
or not too lazy, that's unfair.

305
00:15:43,920 --> 00:15:47,840
But structures, you know, structures are difficult to change,

306
00:15:47,840 --> 00:15:52,800
especially in a system that is somewhat conflicted, maybe?

307
00:15:52,800 --> 00:15:57,120
They've got conflict, and bureaucracy can be a nightmare.

308
00:15:57,120 --> 00:16:01,080
We know that, I mean, it's nice to see American bureaucracy.

309
00:16:01,080 --> 00:16:04,800
Honestly, the visa system in America is much more organized.

310
00:16:04,800 --> 00:16:07,120
I don't know so much more Canada, because I never needed one.

311
00:16:07,120 --> 00:16:10,240
But when I had to get a teaching visa for America,

312
00:16:10,240 --> 00:16:14,040
it was a breath of fresh air, which really surprised me.

313
00:16:14,040 --> 00:16:18,120
But I guess it makes sense, as America said,

314
00:16:18,120 --> 00:16:21,720
that's got a lot more money and wealth, and therefore organization.

315
00:16:27,200 --> 00:16:30,920
Anyway, that's all the questions, huh?

316
00:16:30,920 --> 00:16:31,720
That is.

317
00:16:31,720 --> 00:16:32,960
All right, well, it's good to see.

318
00:16:32,960 --> 00:16:34,240
Thank you for your questions.

319
00:16:34,240 --> 00:16:38,240
Please don't be shy, and I know I can sometimes

320
00:16:38,240 --> 00:16:40,280
be a little bit critical of certain questions,

321
00:16:40,280 --> 00:16:46,240
and I apologize for any untoward behavior.

322
00:16:46,240 --> 00:16:50,000
I appreciate everyone coming on here and meditating together,

323
00:16:50,000 --> 00:16:52,760
asking questions, even just watching on YouTube.

324
00:16:55,640 --> 00:16:56,520
That's all for tonight.

325
00:16:56,520 --> 00:17:04,240
Thank you, Robin, for your constant presence and help.

326
00:17:04,240 --> 00:17:06,920
Anything else, anything going on?

327
00:17:06,920 --> 00:17:07,680
We have a pumpkin.

328
00:17:07,680 --> 00:17:13,880
We got our pumpkin, and someone even sent tools to carve it.

329
00:17:13,880 --> 00:17:14,720
But now I'm not sure.

330
00:17:14,720 --> 00:17:17,520
Suddenly, I hesitated, because I was thinking about this Buddha image

331
00:17:17,520 --> 00:17:21,200
I saw in the pumpkin, I thought, is it really a good thing

332
00:17:21,200 --> 00:17:24,840
to put the image of your teacher on a pumpkin?

333
00:17:24,840 --> 00:17:26,640
The image of the Buddha on a pumpkin?

334
00:17:26,640 --> 00:17:29,880
It's actually not really, it's not

335
00:17:29,880 --> 00:17:33,600
it's somewhat in Congress, right?

336
00:17:33,600 --> 00:17:36,760
The pumpkin is not a place for a Buddha.

337
00:17:36,760 --> 00:17:37,600
What's going to rot?

338
00:17:37,600 --> 00:17:39,720
It's just going to rot.

339
00:17:39,720 --> 00:17:42,160
I mean, a Western cynical Buddhist would say, well, that's

340
00:17:42,160 --> 00:17:42,880
a great thing.

341
00:17:42,880 --> 00:17:43,960
It teaches you impermanence.

342
00:17:43,960 --> 00:17:46,080
But you don't get away with that in Asia.

343
00:17:46,080 --> 00:17:50,480
You want the Buddha to be as permanent and as stable as possible.

344
00:17:50,480 --> 00:17:54,200
Knowing full well that impermanence is a part of reality,

345
00:17:54,200 --> 00:17:59,520
you want to preserve the Buddha as best you can.

346
00:17:59,520 --> 00:18:01,560
So they make the Buddha out of materials that

347
00:18:01,560 --> 00:18:04,360
are going to last thousands of years.

348
00:18:04,360 --> 00:18:06,480
A Western cynical Buddhist might say, oh, yeah.

349
00:18:06,480 --> 00:18:09,160
Well, that's delusion.

350
00:18:09,160 --> 00:18:11,080
Cynical, a real cynical Buddhist might

351
00:18:11,080 --> 00:18:14,400
say that put making an image of the Buddha in the first place.

352
00:18:14,400 --> 00:18:16,360
This is sort of a me-type of Buddhist might

353
00:18:16,360 --> 00:18:19,320
say you're making images of the Buddha in the first place.

354
00:18:19,320 --> 00:18:21,960
It's problematic.

355
00:18:21,960 --> 00:18:24,360
It's not something the Buddha would want us to do.

356
00:18:24,360 --> 00:18:26,760
But certainly putting an image of the Buddha on a pumpkin

357
00:18:26,760 --> 00:18:28,240
is problematic.

358
00:18:28,240 --> 00:18:30,440
So I thought, well, what about the dhamma wheel?

359
00:18:30,440 --> 00:18:34,280
Is that how problematic is it to put a dhamma wheel on it?

360
00:18:34,280 --> 00:18:38,920
It's probably the same type of problematic, maybe not.

361
00:18:38,920 --> 00:18:41,000
I mean, I think the dhamma wheel is a little more simple,

362
00:18:41,000 --> 00:18:44,680
and you can say it's kind of a sign.

363
00:18:44,680 --> 00:18:46,440
I mean, there's a real wholesome descendant

364
00:18:46,440 --> 00:18:51,240
in the sense of proclaiming to the community.

365
00:18:51,240 --> 00:18:53,680
This house is a Buddhist house.

366
00:18:53,680 --> 00:18:56,080
We do a Buddhist halloween.

367
00:18:56,080 --> 00:18:58,840
And it's meant to be a sign for the kids

368
00:18:58,840 --> 00:19:03,120
to know that they can come and we will give them fruit leather.

369
00:19:03,120 --> 00:19:04,480
Do we have, did we get fruit leather?

370
00:19:04,480 --> 00:19:06,080
I don't know, I didn't check it.

371
00:19:06,080 --> 00:19:08,880
I think that Tino is a small, similar to her head.

372
00:19:08,880 --> 00:19:10,480
I had three boxes, they just put.

373
00:19:10,480 --> 00:19:14,040
I think the boxes of reasons, yeah, boxes of reasons,

374
00:19:14,040 --> 00:19:17,080
and fruit leather, and fruit shoes, and vegetable shoes.

375
00:19:17,080 --> 00:19:18,080
Yeah, OK.

376
00:19:18,080 --> 00:19:21,840
Yeah, that only helped me smash the vegetable shoes.

377
00:19:21,840 --> 00:19:23,960
Yeah, well, they're fruit and vegetable shoes,

378
00:19:23,960 --> 00:19:27,760
but I think they're things like carrots that are kind of sweet.

379
00:19:27,760 --> 00:19:31,400
I don't think there's any asparagus shoes.

380
00:19:31,400 --> 00:19:32,520
I don't care if it's cheap.

381
00:19:32,520 --> 00:19:37,520
They don't mean mommy, I've got to care, it's not bad.

382
00:19:37,520 --> 00:19:39,880
I don't know, it's not bad.

383
00:19:39,880 --> 00:19:43,840
Wouldn't cut it when I went to a Katrina.

384
00:19:43,840 --> 00:19:46,120
I gave out the little boxes of reasons,

385
00:19:46,120 --> 00:19:49,240
along with the broccoli, you know, they

386
00:19:49,240 --> 00:19:53,440
can take what they like, and it's a really happy to get those.

387
00:19:53,440 --> 00:19:55,440
Mommy, I've got to raise everybody else's given.

388
00:19:58,640 --> 00:20:02,320
But the demo we'll pump in last year, I thought, OK.

389
00:20:02,320 --> 00:20:05,760
But really beautiful, I hope you make it and to show it

390
00:20:05,760 --> 00:20:09,800
on air, because it's really pretty impressive.

391
00:20:09,800 --> 00:20:14,040
It's quite simple, but we have to patent it and publish

392
00:20:14,040 --> 00:20:15,000
the design.

393
00:20:15,000 --> 00:20:17,640
This tool comes with an extraction.

394
00:20:17,640 --> 00:20:19,400
You have a piece of paper, and you wet the paper,

395
00:20:19,400 --> 00:20:22,600
and you splat it on the pumpkin, and then you wrap the pumpkin

396
00:20:22,600 --> 00:20:24,480
in a surround wrap.

397
00:20:24,480 --> 00:20:27,440
And I guess that helps you when you're cutting something.

398
00:20:27,440 --> 00:20:33,680
Anyway, that's not, this isn't broadcast material stuff.

399
00:20:33,680 --> 00:20:34,880
What else?

400
00:20:34,880 --> 00:20:35,880
Nothing else.

401
00:20:35,880 --> 00:20:36,600
I think we're good.

402
00:20:36,600 --> 00:20:38,560
Tomorrow we see the manga, right?

403
00:20:38,560 --> 00:20:43,240
Hopefully, I might have a meeting at 2.30.

404
00:20:43,240 --> 00:20:45,080
If I can get a ride, I actually might go.

405
00:20:45,080 --> 00:20:48,600
So I'm actually kind of thinking that someone will

406
00:20:48,600 --> 00:20:52,000
call me with a ride, but maybe not.

407
00:20:52,000 --> 00:20:55,280
I'd come off comfortable staying and doing the receiving

408
00:20:55,280 --> 00:21:00,800
manga, but what I do have to is the Hamilton Interfaith

409
00:21:00,800 --> 00:21:02,080
Peace Group.

410
00:21:02,080 --> 00:21:06,600
So it's me putting on the Buddhist face.

411
00:21:06,600 --> 00:21:18,240
We also now have a interface advisory board for

412
00:21:18,240 --> 00:21:24,640
Amik Master, and yet to see whether it's actually going to be

413
00:21:24,640 --> 00:21:29,880
a force for any sort of Buddhist or non-Christian presence

414
00:21:29,880 --> 00:21:31,880
at Amik Master.

415
00:21:31,880 --> 00:21:34,560
Here's something I just got.

416
00:21:34,560 --> 00:21:39,120
Another email about the 10th Global Conference on Buddhism.

417
00:21:39,120 --> 00:21:43,760
So I have been invited to speak at the 10th Global Conference

418
00:21:43,760 --> 00:21:45,880
on Buddhism.

419
00:21:45,880 --> 00:21:49,200
It's a great privilege and honor to have you speak

420
00:21:49,200 --> 00:21:51,760
truth as I was sort of because they couldn't find anybody

421
00:21:51,760 --> 00:21:53,720
else, I think.

422
00:21:53,720 --> 00:21:57,120
And they're kind of on the board or rather kind of like, is he?

423
00:21:57,120 --> 00:21:58,600
Or is he?

424
00:21:58,600 --> 00:22:00,440
Toronto.

425
00:22:00,440 --> 00:22:01,840
10th Global Conference here.

426
00:22:01,840 --> 00:22:02,320
I'll show you.

427
00:22:02,320 --> 00:22:03,000
Can I scream?

428
00:22:03,000 --> 00:22:09,320
You can't see it, but I can show it to him, but I'm not

429
00:22:09,320 --> 00:22:11,160
going to get into it.

430
00:22:11,160 --> 00:22:15,120
So you will receive a second email shortly, explaining more

431
00:22:15,120 --> 00:22:17,080
details.

432
00:22:17,080 --> 00:22:18,520
There's a program.

433
00:22:18,520 --> 00:22:23,880
Let's see in the program.

434
00:22:23,880 --> 00:22:28,120
I give them two topics.

435
00:22:28,120 --> 00:22:30,080
So let's see what we have.

436
00:22:30,080 --> 00:22:32,120
So Bhante, sir, and Prala.

437
00:22:32,120 --> 00:22:35,400
Brahmalang, so is doing guided meditation, mindfulness

438
00:22:35,400 --> 00:22:41,240
meditation for 15 minutes.

439
00:22:41,240 --> 00:22:43,280
And here we are.

440
00:22:43,280 --> 00:22:44,280
Wrong mindfulness.

441
00:22:44,280 --> 00:22:48,680
So this is not the who?

442
00:22:48,680 --> 00:22:50,840
Wow, this is scary.

443
00:22:50,840 --> 00:22:53,160
Brahmalang, so is the first speaker.

444
00:22:53,160 --> 00:22:56,720
Dhamma Geebaham throws the second speaker.

445
00:22:56,720 --> 00:23:01,360
And yet the dhamma beaker is the third speaker.

446
00:23:01,360 --> 00:23:04,040
Wrong mindfulness in its consequences.

447
00:23:04,040 --> 00:23:05,440
That's not the topic I offered.

448
00:23:09,560 --> 00:23:11,520
OK.

449
00:23:11,520 --> 00:23:12,640
Whatever, you know.

450
00:23:12,640 --> 00:23:14,440
Whatever they want.

451
00:23:14,440 --> 00:23:18,000
So Brahmalang, so says, what is the right mindfulness?

452
00:23:18,000 --> 00:23:21,320
Dhamma Geebah says, teaches mindfulness methods

453
00:23:21,320 --> 00:23:23,360
used in Buddhist meditation.

454
00:23:23,360 --> 00:23:25,440
And then I have to talk about wrong mindfulness

455
00:23:25,440 --> 00:23:28,000
in its consequences.

456
00:23:28,000 --> 00:23:29,120
I can do that.

457
00:23:29,120 --> 00:23:32,200
It would be interesting.

458
00:23:32,200 --> 00:23:32,840
Right.

459
00:23:32,840 --> 00:23:34,200
And then session.

460
00:23:34,200 --> 00:23:36,520
And there's also Chiram.

461
00:23:36,520 --> 00:23:38,320
Yeah, this is in Toronto.

462
00:23:38,320 --> 00:23:43,320
This is on June 17th.

