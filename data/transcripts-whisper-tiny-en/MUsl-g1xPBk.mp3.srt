1
00:00:00,000 --> 00:00:04,420
This is the classic question on the fourth precept.

2
00:00:04,420 --> 00:00:09,040
Say you were strictly following the precepts, you see a terrified man run past you, then

3
00:00:09,040 --> 00:00:14,680
another man comes up to you with a knife and asks if you seen where the first man went.

4
00:00:14,680 --> 00:00:18,840
Although you know, although you know, would it be appropriate to lie and say you don't

5
00:00:18,840 --> 00:00:24,000
know, never seen him, or would you have to tell the truth?

6
00:00:24,000 --> 00:00:30,800
This has always given us an example of how, quote unquote, the precepts are a guide, not

7
00:00:30,800 --> 00:00:32,520
a rule.

8
00:00:32,520 --> 00:00:39,280
People believe that there are instances where breaking the precepts is appropriate, and

9
00:00:39,280 --> 00:00:40,280
they have all sorts.

10
00:00:40,280 --> 00:00:43,720
There's even better ones than this, you could come up with, but this is the classic

11
00:00:43,720 --> 00:00:44,960
one.

12
00:00:44,960 --> 00:00:50,320
But it was a silly one, and please don't take this personally because this is the

13
00:00:50,320 --> 00:00:51,720
classic question.

14
00:00:51,720 --> 00:00:55,960
And so someone that this was explained, I was sitting in a group and this monk was explaining

15
00:00:55,960 --> 00:01:03,800
this, and I said to him, well, couldn't you just, couldn't you just not say anything?

16
00:01:03,800 --> 00:01:08,240
And he was like, oh yeah, I guess you could.

17
00:01:08,240 --> 00:01:14,680
Because keeping the precepts doesn't mean you have to say stuff, keeping the precepts

18
00:01:14,680 --> 00:01:18,800
means, means not doing things.

19
00:01:18,800 --> 00:01:25,040
So suppose this guy even says, tell me where he went, or I'll beat you up.

20
00:01:25,040 --> 00:01:31,000
And you say, oh, you're welcome to beat me up if you like.

21
00:01:31,000 --> 00:01:34,200
It's really not really none of my concern, something like that.

22
00:01:34,200 --> 00:01:36,560
And then they might beat you up, and that's it.

23
00:01:36,560 --> 00:01:38,160
They might kill you, and so on.

24
00:01:38,160 --> 00:01:41,200
It doesn't mean that you have to tell a lie.

25
00:01:41,200 --> 00:01:46,360
It doesn't mean you have to tell the truth.

26
00:01:46,360 --> 00:01:48,080
That's really the proper thing to do.

27
00:01:48,080 --> 00:01:54,280
There's an interesting article I read recently by Sam Harris, and probably some of you

28
00:01:54,280 --> 00:01:58,000
know who Sam Harris is, he's an atheist.

29
00:01:58,000 --> 00:02:00,440
They don't like to be called that.

30
00:02:00,440 --> 00:02:03,080
I'm an atheist too, so.

31
00:02:03,080 --> 00:02:07,800
By atheist, I mean, he's someone who talks a lot about atheism, among other things, but

32
00:02:07,800 --> 00:02:15,400
it's something that he's well known for his views on theism and the silliness of it all.

33
00:02:15,400 --> 00:02:21,320
And he wrote an article recently about self-defense, and it got me thinking because he's

34
00:02:21,320 --> 00:02:31,480
going on and on about how you have to react, and you have to don't go along with things

35
00:02:31,480 --> 00:02:34,440
when you're being attacked, don't go along with it.

36
00:02:34,440 --> 00:02:37,480
And at first, I didn't agree with it, but I think some of it's agreeable.

37
00:02:37,480 --> 00:02:40,920
The self-defense is for sure allowable.

38
00:02:40,920 --> 00:02:45,760
You don't want to, if someone's trying to rape you or someone, you don't want to have

39
00:02:45,760 --> 00:02:51,160
them rape you because you're not going to agree with it.

40
00:02:51,160 --> 00:02:56,960
The reason why you don't want them to rape you is because it's bad for them, which many

41
00:02:56,960 --> 00:03:03,480
people find difficult to understand, but the reason why you don't want people to murder

42
00:03:03,480 --> 00:03:10,400
you is because it's bad for them, that's, I think, a really good excuse to let it happen.

43
00:03:10,400 --> 00:03:13,680
But they're a good excuse is because you want to live yourself so that you can do good

44
00:03:13,680 --> 00:03:18,440
stuff, and you think if I die that I don't know where I'm going to go, I'm not enlightened

45
00:03:18,440 --> 00:03:22,080
yet, and so better I stick around for a while to do more good deeds.

46
00:03:22,080 --> 00:03:27,120
There's lots of good reasons for defending yourself.

47
00:03:27,120 --> 00:03:41,160
But I think there still is some case for, especially in an extreme example, the example

48
00:03:41,160 --> 00:03:50,320
that I thought of was of this monk who was in Rajagaha, there's a teragat of a monk

49
00:03:50,320 --> 00:03:59,560
who was going walking through India barefoot, and he had his lay attendant with him carrying

50
00:03:59,560 --> 00:04:09,480
the money, and they decided that they would camp out on Gijakuta, Gijakuta Vultures Peak,

51
00:04:09,480 --> 00:04:15,600
which is a very famous place where the Buddha lived, and many of the disciples lived,

52
00:04:15,600 --> 00:04:22,480
but his great disciples lived, and it's kind of like the mount in the Sermon on the

53
00:04:22,480 --> 00:04:26,240
mount, and it's a very important place in Buddhism, so that's cool, we'll just camp

54
00:04:26,240 --> 00:04:32,080
out up here, big mistake, don't camp in Bihar, I don't know if it's better now, but

55
00:04:32,080 --> 00:04:42,880
the idea of camping in Bihar is ridiculous, tourists get, tourists get murdered in Bihar,

56
00:04:42,880 --> 00:04:51,640
it's a very poor province, and lots of bandits and so on, and so indeed they were confronted

57
00:04:51,640 --> 00:04:57,240
by bandits, actually Gijakuta is safe now I think, there's guys with guns guarding Gijakuta

58
00:04:57,240 --> 00:05:01,960
maybe because of things like this happening, but you know, anyway at night I don't think

59
00:05:01,960 --> 00:05:09,240
the guards stay, stick around, and so at night they were confronted by these bandits,

60
00:05:09,240 --> 00:05:21,680
and the lay attendant, the guy who was walking with him charged them and ran right, broke

61
00:05:21,680 --> 00:05:26,760
right through them, scuffled with them a little bit and ran away, and jumped down from

62
00:05:26,760 --> 00:05:34,840
the top of Gijakuta into the ravine, you know, like not breaking anything, but scratching

63
00:05:34,840 --> 00:05:44,920
himself terribly and bruising himself, and the monk sat there and they said we're going

64
00:05:44,920 --> 00:05:51,400
to kill you now, and what the monk said that he did is he just, they had a knife pointed

65
00:05:51,400 --> 00:05:57,360
at his neck, and he just lifted up his neck like this to give them the, give them his

66
00:05:57,360 --> 00:06:02,280
flesh, like okay if you're going to do it, go ahead and do it, and the point is that

67
00:06:02,280 --> 00:06:09,080
they didn't kill him, they, the point is that there's a power in that, there's a power

68
00:06:09,080 --> 00:06:21,200
in letting go that very often makes you immune to evil, and as a result it was, it would

69
00:06:21,200 --> 00:06:24,840
have been quite difficult for them to hurt him, it would have been quite easy for them

70
00:06:24,840 --> 00:06:29,120
to hurt the other guy because he was totally unmindful and he had lost his presence of

71
00:06:29,120 --> 00:06:35,360
the mind. They didn't kill him, and the guy that jumped down in the ravine, the story

72
00:06:35,360 --> 00:06:44,080
goes that he realized that he had left this monk defenseless, and so he climbed back up,

73
00:06:44,080 --> 00:06:48,520
and surrendered himself, and they had to give up all their clothes and robes and bowls

74
00:06:48,520 --> 00:06:53,880
and money and camera and everything, all they left him, the monk with was his lower

75
00:06:53,880 --> 00:07:02,840
robe, so like a sarong, but they left him alive, and somehow they survived and got out

76
00:07:02,840 --> 00:07:10,960
of it, and went to stay in a time monastery and had a happy ending in the end, but anyway

77
00:07:10,960 --> 00:07:18,840
that's not exactly the question you're asking, but you certainly don't have to lie, and

78
00:07:18,840 --> 00:07:24,240
in the case where you're forced to do something unethical, just don't do it, regardless

79
00:07:24,240 --> 00:07:29,720
of the consequences, because people always make up these things well, if you don't do

80
00:07:29,720 --> 00:07:35,440
it, they're going to do this, but the point is what you're saying is that if you don't

81
00:07:35,440 --> 00:07:41,560
do something unethical, somebody else is going to do something unethical, and you're

82
00:07:41,560 --> 00:07:49,200
not responsible for their unethical behavior, it's not your bad karma, if someone else

83
00:07:49,200 --> 00:07:57,320
does something bad, it would only be an intellectual exercise, if you said, I should do

84
00:07:57,320 --> 00:08:03,400
something bad, because otherwise these people are going to do something worse.

85
00:08:03,400 --> 00:08:09,440
The ethics of it are quite clear, you're not doing anything wrong, and so from a point of

86
00:08:09,440 --> 00:08:19,800
view of reality, there's no need ever to do something bad in order to avert badness,

87
00:08:19,800 --> 00:08:26,520
you're not responsible for other people's acts, and knowing that people go according

88
00:08:26,520 --> 00:08:27,840
to their karma and so on.

89
00:08:27,840 --> 00:08:32,720
It's quite difficult, I mean it's quite clear in my mind, but I have trouble expressing

90
00:08:32,720 --> 00:08:37,280
this, because I know people feel quite strongly like if your family was involved, people

91
00:08:37,280 --> 00:08:43,920
would be very much attached to their family, but it goes totally against reality actually,

92
00:08:43,920 --> 00:08:50,640
and it falls into this conventional reality of relationships and our attachment to other

93
00:08:50,640 --> 00:08:55,920
people, missing the whole point of how karma works and how people go according to their

94
00:08:55,920 --> 00:08:59,040
deeds and their relationships with each other.

95
00:08:59,040 --> 00:09:03,360
If someone's going to kill someone else, it has much more to do with their relationship

96
00:09:03,360 --> 00:09:12,800
as to individuals than your intervention and so on, so for sure there's never any reason

97
00:09:12,800 --> 00:09:26,440
to do unholesomeness, because that is your action, and that is your input into the universe,

98
00:09:26,440 --> 00:09:35,680
regardless of what someone else is going to do, anyway, so.

