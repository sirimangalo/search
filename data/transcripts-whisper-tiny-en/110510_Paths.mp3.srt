1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:15,000
Today I will be starting again my series on Buddhism, meditation in the Master of Life,

3
00:00:15,000 --> 00:00:23,000
answering questions that people have over YouTube and the Internet in general.

4
00:00:23,000 --> 00:00:32,000
Today the question comes in regards to choosing a path.

5
00:00:32,000 --> 00:00:45,000
The questioner asked about whether one can choose from the various religious traditions.

6
00:00:45,000 --> 00:00:55,000
You know based on one's interest, one is interested in this tradition, that tradition in many different editions.

7
00:00:55,000 --> 00:01:00,000
Is it possible or what is the result?

8
00:01:00,000 --> 00:01:11,000
What sort of result can one expect from choosing, picking and choosing the things that one finds useful from various religious traditions?

9
00:01:11,000 --> 00:01:19,000
And that really is the point in regards to results.

10
00:01:19,000 --> 00:01:26,000
Because a path, the meaning of the word path, is something that leads to a goal.

11
00:01:26,000 --> 00:01:35,000
And I think it's important to be clear that we have a goal in mind.

12
00:01:35,000 --> 00:01:44,000
Now if you have a specific goal in mind, then there is really no problem in picking and choosing.

13
00:01:44,000 --> 00:01:50,000
If you think this teaching or that teaching is going to help you towards that goal.

14
00:01:50,000 --> 00:01:55,000
So it's really going to depend on what sort of goal you have.

15
00:01:55,000 --> 00:02:04,000
Now the problem comes where there are many people who don't have a goal.

16
00:02:04,000 --> 00:02:12,000
And they might pick and choose simply based on their interests, simply based on what appeals to them at the time.

17
00:02:12,000 --> 00:02:19,000
And as a result, have no particular goal in mind.

18
00:02:19,000 --> 00:02:33,000
They in fact might claim that there is no subjective or objective reality or objective goal.

19
00:02:33,000 --> 00:02:44,000
And they might take us their belief for their view that reality is what it is.

20
00:02:44,000 --> 00:02:55,000
And it's eternal and all we have to do is to keep going, do what comes to us at the time.

21
00:02:55,000 --> 00:03:10,000
And that's going to lead us ever and ever again to a similar or different or changing, constantly changing state of existence.

22
00:03:10,000 --> 00:03:24,000
So and I think that whether it comes up as a view in one's mind, I think that sort of idea is prevalent among people who pick and choose from various religious traditions.

23
00:03:24,000 --> 00:03:31,000
They don't have a particular goal or path in mind.

24
00:03:31,000 --> 00:03:33,000
They're just doing what comes to them at the time.

25
00:03:33,000 --> 00:03:52,000
And so it may be laziness, it may just be not realizing the importance or not seeing any importance in finding a path or changing our state.

26
00:03:52,000 --> 00:03:57,000
Some people think that our state as it is is fine.

27
00:03:57,000 --> 00:04:14,000
The problem with this idea, the idea of this continuous cycle or eternal existence continuing on and just taking what comes to you at the time,

28
00:04:14,000 --> 00:04:29,000
is that then you put yourself in a position to have to accept the suffering and the unhappiness that exists in life.

29
00:04:29,000 --> 00:04:39,000
Now, by that, I don't simply mean the suffering that we're experiencing now, because obviously for most of us, it's a mixed bag.

30
00:04:39,000 --> 00:04:51,000
There's some happiness and some suffering, but if your understanding is that existence and reality is eternal and we just continue on and on as it is,

31
00:04:51,000 --> 00:04:56,000
then you have to look around you and see the sorts of lives that are possible.

32
00:04:56,000 --> 00:05:05,000
And it's clear that it's possible to experience a great amount of happiness, but it's also possible to experience a great amount of sorrow, a great amount of suffering.

33
00:05:05,000 --> 00:05:11,000
I mean, torture and worse.

34
00:05:11,000 --> 00:05:33,000
So if you think that there is some value in this experience, it's important to take a very close look and not just take this as a view because clearly even in our one life we've gone through a lot, most of us just to grow up,

35
00:05:33,000 --> 00:05:36,000
just to get to the point where we're more or less stable.

36
00:05:36,000 --> 00:05:41,000
And if we look back at all the suffering and all the lessons we had to learn,

37
00:05:41,000 --> 00:05:54,000
and then ask yourself whether you want to learn these lessons and then forget them and then have to learn them again and forget them on and on and on with no end in sight.

38
00:05:54,000 --> 00:06:03,000
I think most of us, if we're honest with ourselves, we would prefer a state of development.

39
00:06:03,000 --> 00:06:15,000
And I think in that sense, it's fine to pick and choose the things that you think are going to help you as long as you have an idea of what is going to be helpful for one's development.

40
00:06:15,000 --> 00:06:35,000
Now, the reasons why one might not simply pick a specific path, it's clear I think why there are many people who would rather not pick this religious or just in that religious tradition and so on.

41
00:06:35,000 --> 00:06:51,000
I think for some people it's not wanting to follow the crowd and we have this idea of individuality and we want to be special and we want to have our own way and it's an ego, a trip of sorts.

42
00:06:51,000 --> 00:07:13,000
But I think for other people it's quite discouraging to see all of these different paths all claiming to have the soul, the one and only way out of suffering, the one and only way to salvation to freedom, to everlasting happiness, to development.

43
00:07:13,000 --> 00:07:41,000
And so as a result we think better for me to take the good and not take something simply because it belongs to this religious tradition or that religious tradition, but take it because it seems after some introspection and even some experimentation that it seems to be helpful.

44
00:07:41,000 --> 00:07:43,000
Take those things that are helpful.

45
00:07:43,000 --> 00:07:47,000
Now this has its positives and its negatives.

46
00:07:47,000 --> 00:08:07,000
The negatives are first of all that your potential or mistake is greater because all of the very spiritual and religious traditions have been tried and tested and they do all lead to a fairly specific result.

47
00:08:07,000 --> 00:08:16,000
So at least to an extent you'd be better off to look and see where a specific tradition leads.

48
00:08:16,000 --> 00:08:27,000
A specific practice leads because it could be that there are two traditions that are both leading to the same place and there may be more than one tradition, one path.

49
00:08:27,000 --> 00:08:48,000
They may actually be the same path, you see. And in this sense it's important, I think one should never look at something in terms of a broad label like this is Christianity and therefore it's going to lead me in this direction, this is Buddhism and therefore it's going to lead me in this direction and so on and so on.

50
00:08:48,000 --> 00:09:04,000
It's important to look at the various teachers and the various traditions because even in Buddhism for example there are many different ways and ostensibly they lead in different directions to an extent.

51
00:09:04,000 --> 00:09:19,000
So I think rather than picking and choosing at random or based on one's own limited knowledge not having practice any of these paths.

52
00:09:19,000 --> 00:09:42,000
When we better off to look at the sort of people who practice a specific path and try out that path for a while to not not worry about collecting the things from various paths but to look at the paths and see where they lead and to develop a specific path.

53
00:09:42,000 --> 00:09:55,000
I would prefer that over picking and choosing and hoping to cobble together some sort of path that's going to lead you somewhere because the problem is as I said you've got no experience with it.

54
00:09:55,000 --> 00:10:19,000
You cobble together this this vehicle and you start going with it and you know it's going to after some time you might realize that it's not taking you it's not well made and it's not taking you in the direction you need to go but if you have this vehicle you see other people taking this vehicle and the results that they get are clear.

55
00:10:19,000 --> 00:10:29,000
And you'd be better off to in some sense to follow after them.

56
00:10:29,000 --> 00:10:47,000
Because you know and so you can do either you could you can choose to cobble together your own vehicle to make up your own path and basically starting a new religion or new religious tradition saying that this is my path and it's leading there.

57
00:10:47,000 --> 00:11:08,000
Or and you know hope for the best and and probably you know the chance the odds are against you unless you're a fairly special person or you know start looking at religious traditions I mean what we're talking about here is vehicles vehicles that lead us in a certain direction and.

58
00:11:08,000 --> 00:11:34,000
You know they don't all lead in the same direction so if you if you pick one you know and and take it where it's going to lead you or use it for that specific result and why do you need to take something from another vehicle I mean not to say that these things are not good but.

59
00:11:34,000 --> 00:11:40,000
You know there are reasons why people have come to a specific set of practices.

60
00:11:40,000 --> 00:11:46,000
A specific specific set of views beliefs and practices.

61
00:11:46,000 --> 00:12:02,000
So I would say it's expedient to pick a specific practice because it saves you a lot of the guests work a lot of the experimentation but if you're really fed up and you don't see that anyone vehicle any one path.

62
00:12:02,000 --> 00:12:13,000
Is leading you in the direction that you're looking to go then by all means I think you have to pick and choose and start your own way.

63
00:12:13,000 --> 00:12:31,000
And the only thing is that I would caution against this lack of direction where people you know put these things together sort of as an ego trip or kind of thinking that this is my path and then everyone looks at you and you can feel good about yourself because you've got.

64
00:12:31,000 --> 00:12:37,000
You've got your way and so on I think there's a lot of people like this who.

65
00:12:37,000 --> 00:12:48,000
In the end wind up going nowhere or not having any clear direction or any clear goal in mind so I hope that helps.

66
00:12:48,000 --> 00:12:53,000
I would recommend that you try at least try.

67
00:12:53,000 --> 00:13:09,000
The path that the Buddha laid down and that was been followed by millions of people up into the present day and to see where that leads you because as far as I can see it's a path that does lead where it says it leads.

68
00:13:09,000 --> 00:13:23,000
And can clearly be seen to lead where it says it leads as opposed to maybe maybe as another path that says it leads somewhere but but requires you to take it on faith that it does so.

69
00:13:23,000 --> 00:13:37,000
That actually the Buddha's teaching does lead to development that one can clearly verify and lead to ultimate freedom from suffering and happiness.

70
00:13:37,000 --> 00:13:41,000
So hope that helps thanks for the question of the best.

