1
00:00:00,000 --> 00:00:06,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:06,000 --> 00:00:16,000
Today we continue with verses 231 to 234, which read us follows.

3
00:00:36,000 --> 00:01:02,000
Today we continue with the Dhamapada and the Dhamapada.

4
00:01:02,000 --> 00:01:31,000
One should guard oneself from performing physical violence.

5
00:01:31,000 --> 00:01:38,000
One should be restrained physically.

6
00:01:38,000 --> 00:01:49,000
Having abandoned physical misconduct, one should engage in good physical conduct,

7
00:01:49,000 --> 00:01:55,000
positive physical conduct.

8
00:01:55,000 --> 00:02:00,000
One should guard oneself from performing verbal violence.

9
00:02:00,000 --> 00:02:03,000
One should be restrained verbally.

10
00:02:03,000 --> 00:02:13,000
Having abandoned verbal misconduct, one should engage in good verbal behavior.

11
00:02:13,000 --> 00:02:21,000
One should guard oneself from mental violence, from performing mental violence.

12
00:02:21,000 --> 00:02:24,000
One should be guarded mentally.

13
00:02:24,000 --> 00:02:38,000
Having abandoned mental misconduct, one should engage in good mental conduct.

14
00:02:38,000 --> 00:02:46,000
The wiser restrained physically, they are restrained verbally as well.

15
00:02:46,000 --> 00:02:58,000
The wiser restrained mentally, they indeed are well and fully restrained.

16
00:02:58,000 --> 00:03:07,000
These verses were taught in response to not so much a story as a simple event.

17
00:03:07,000 --> 00:03:20,000
In the Buddha's congregation, in the monastic order, there was a group of six monks.

18
00:03:20,000 --> 00:03:23,000
They became known as the group of six.

19
00:03:23,000 --> 00:03:30,000
They had a name for themselves.

20
00:03:30,000 --> 00:03:40,000
As a result of performing all manner of bad deeds,

21
00:03:40,000 --> 00:03:46,000
doing all sorts of things that were on becoming monastics.

22
00:03:46,000 --> 00:03:53,000
On this occasion, it said that they put on wooden clogs

23
00:03:53,000 --> 00:04:08,000
and carrying wooden stabs, stabs, staves in their hand sticks.

24
00:04:08,000 --> 00:04:15,000
Went to this big flat rock area and started walking back and forth on the rock,

25
00:04:15,000 --> 00:04:22,000
making an incredible noise.

26
00:04:22,000 --> 00:04:25,000
It's an example of the sorts of things they wouldn't get in.

27
00:04:25,000 --> 00:04:29,000
They weren't against the rules.

28
00:04:29,000 --> 00:04:37,000
It was as though they were trying to find things to do that would be wrong,

29
00:04:37,000 --> 00:04:42,000
but didn't technically break any rule.

30
00:04:42,000 --> 00:04:46,000
The Buddha asked, what's that noise?

31
00:04:46,000 --> 00:04:53,000
I think it was Ananda who said, well, it's the group of six, they're acting up again.

32
00:04:53,000 --> 00:05:00,000
The Buddha laid down a rule that you're not allowed to wear wooden shoes.

33
00:05:00,000 --> 00:05:10,000
He remarked on the event with these four verses.

34
00:05:10,000 --> 00:05:16,000
It's a simple, very simple story and very simple teaching in the verses,

35
00:05:16,000 --> 00:05:20,000
but it's a rather important one, a rather useful one.

36
00:05:20,000 --> 00:05:30,000
The much of the Buddha's teaching isn't esoteric or even profound in its content.

37
00:05:30,000 --> 00:05:41,000
You're talking about the body, speech and mind, that content isn't very profound,

38
00:05:41,000 --> 00:05:48,000
but it is an incredibly profound teaching in its simplicity.

39
00:05:48,000 --> 00:06:01,000
It works as a very important teaching from a practical perspective,

40
00:06:01,000 --> 00:06:07,000
because it provides a framework for us to understand our interactions with the world.

41
00:06:07,000 --> 00:06:17,000
The three doors, the bodily door, the verbal door, and the mental door.

42
00:06:17,000 --> 00:06:20,000
These are the ways in which we interact with the world.

43
00:06:20,000 --> 00:06:23,000
These are the ways in which karma is performed.

44
00:06:23,000 --> 00:06:31,000
The inclusion of the mental door itself is a profound teaching.

45
00:06:31,000 --> 00:06:42,000
It reminds us that the ethical implications of our acts or those acts that have ethical implications

46
00:06:42,000 --> 00:06:50,000
are not limited to acts of speech and body.

47
00:06:50,000 --> 00:06:54,000
These are the three ways in which we engage with the world,

48
00:06:54,000 --> 00:07:04,000
the ways in which we express our positive, our good and our bad tendencies.

49
00:07:04,000 --> 00:07:17,000
Yet they're quite simple that doesn't require a lot of complicated understanding.

50
00:07:17,000 --> 00:07:27,000
As a meditator, if you remember these three doors, it provides you with a good framework to cultivate mindfulness.

51
00:07:27,000 --> 00:07:31,000
How should you cultivate mindfulness while there's the physical?

52
00:07:31,000 --> 00:07:36,000
Whenever I do physically, there's the verbal when I speak.

53
00:07:36,000 --> 00:07:48,000
And there's the mental when I think when I fantasize or plan or remember.

54
00:07:48,000 --> 00:07:54,000
So the Buddha quite often would remind us of these three doors and use them as a teaching.

55
00:07:54,000 --> 00:08:02,000
We talk a lot about Ducharita and Sucharita, which the Buddha mentions in these verses.

56
00:08:02,000 --> 00:08:07,000
Ducharita do meaning bad and so meaning good.

57
00:08:07,000 --> 00:08:09,000
So we have things with the physical.

58
00:08:09,000 --> 00:08:12,000
There's the obvious ones of killing and stealing.

59
00:08:12,000 --> 00:08:15,000
These would be bad deeds.

60
00:08:15,000 --> 00:08:20,000
And on the other side of charity and generosity and helping others.

61
00:08:20,000 --> 00:08:28,000
Any action that is performed with love or kindness, compassion.

62
00:08:28,000 --> 00:08:34,000
With speech, again, the obvious ones, you have lying and insulting.

63
00:08:34,000 --> 00:08:40,000
You have gossiping, you have idle speech, useless speech.

64
00:08:40,000 --> 00:08:42,000
And on the other side, you have truth-telling.

65
00:08:42,000 --> 00:08:45,000
Someone, someone needs to know something, you tell them the truth.

66
00:08:45,000 --> 00:08:50,000
Useful, instructive speech.

67
00:08:50,000 --> 00:08:54,000
When someone could use advice, you give them advice.

68
00:08:54,000 --> 00:08:59,000
When someone can use encouragement, you give them encouragement.

69
00:08:59,000 --> 00:09:05,000
Even just complimenting people is often incredibly helpful for bolstering their confidence

70
00:09:05,000 --> 00:09:10,000
and setting themselves in the right direction, helping them to give up doubt.

71
00:09:10,000 --> 00:09:19,000
When you remind them of the good that they are doing.

72
00:09:19,000 --> 00:09:32,000
And mentally, obviously engaging in fantasy, fantasy about positive things, fantasy about negative things,

73
00:09:32,000 --> 00:09:42,000
emphasizing about harming others, harming yourself.

74
00:09:42,000 --> 00:09:51,000
From a mindfulness perspective, it's not just these obvious positive and negative acts.

75
00:09:51,000 --> 00:09:55,000
And that's where we get to the second aspect of this teaching.

76
00:09:55,000 --> 00:10:03,000
And that's the aspect of samwara or as the Buddha says, here samwuta, which means being guarded

77
00:10:03,000 --> 00:10:06,000
or restrained, sorry.

78
00:10:06,000 --> 00:10:11,000
Garded, restrained, rakka, rakka, yeah, the Buddha says here.

79
00:10:11,000 --> 00:10:17,000
Garded, samwara, samwuta, restrained.

80
00:10:17,000 --> 00:10:23,000
They mean it, basically, the same thing.

81
00:10:23,000 --> 00:10:32,000
That it's not simply about abstaining from killing and stealing.

82
00:10:32,000 --> 00:10:35,000
It's about guarding these doors.

83
00:10:35,000 --> 00:10:40,000
It's about having restraint in relation to these doors.

84
00:10:40,000 --> 00:10:43,000
It is restrained in regards to walking.

85
00:10:43,000 --> 00:10:50,000
There are restraint in regards to action in general so that you wouldn't go around making loud noises when you walk,

86
00:10:50,000 --> 00:10:55,000
which is in a monastic community or in any community really.

87
00:10:55,000 --> 00:10:58,000
It's incredibly unrestrained.

88
00:10:58,000 --> 00:11:01,000
It's a sign of a mind that is unrestrained.

89
00:11:01,000 --> 00:11:04,000
A mind that is untrained.

90
00:11:04,000 --> 00:11:08,000
A mind that is wild and untamed.

91
00:11:08,000 --> 00:11:15,000
A mind that is reckless.

92
00:11:15,000 --> 00:11:26,000
And so true, good and evil behavior, whether it be through action, through speech, or through mind,

93
00:11:26,000 --> 00:11:29,000
it's much more than just keeping precepts.

94
00:11:29,000 --> 00:11:31,000
So it involves keeping precepts.

95
00:11:31,000 --> 00:11:39,000
It's also that when we walk, we are guarded, we are restrained.

96
00:11:39,000 --> 00:11:42,000
It doesn't mean you have to be repressed.

97
00:11:42,000 --> 00:11:46,000
Restrained is a bit misleading sometimes.

98
00:11:46,000 --> 00:11:51,000
It makes you think you should be perhaps repressed in some way.

99
00:11:51,000 --> 00:11:54,000
A restrained means focused.

100
00:11:54,000 --> 00:11:56,000
It means limited.

101
00:11:56,000 --> 00:12:06,000
It means restricted to the objective, to objectivity.

102
00:12:06,000 --> 00:12:13,000
That when you walk, you restrict your mind to the act of walking, to the reality.

103
00:12:13,000 --> 00:12:22,000
When you talk, you restrict yourself to the truth and to the meaning,

104
00:12:22,000 --> 00:12:28,000
to an objective expression of reality.

105
00:12:28,000 --> 00:12:35,000
Why many of the Buddha's teachings don't seem so much like doctrinal points trying to convince you of something?

106
00:12:35,000 --> 00:12:43,000
It's because they're actually really on the deepest level, they're an expression of reality.

107
00:12:43,000 --> 00:12:52,000
The Buddha's teachings are nothing more than a description of reality.

108
00:12:52,000 --> 00:13:07,000
Mentally, it means engaging in an objectivity of mind, a clarity of mind.

109
00:13:07,000 --> 00:13:11,000
When you walk, and you know that you're walking,

110
00:13:11,000 --> 00:13:16,000
and not only know that you're walking, but that's all that's in the mind.

111
00:13:16,000 --> 00:13:32,000
The mind is present and only present. The mind doesn't become lost in judgment or bias or extrapolation about reality.

112
00:13:32,000 --> 00:13:44,000
The pure mind is the mind that is focused on the present, focused on the experience, focused on the reality.

113
00:13:44,000 --> 00:13:47,000
The Buddha taught these two things.

114
00:13:47,000 --> 00:13:54,000
The framework within which we understand, we interact with reality, and that is the three doors,

115
00:13:54,000 --> 00:14:00,000
and he taught restraint, how to behave in relation to these doors.

116
00:14:00,000 --> 00:14:04,000
He also taught five ways by which we practice restraint,

117
00:14:04,000 --> 00:14:10,000
which it's worth mentioning to give you an idea of what is meant by this word,

118
00:14:10,000 --> 00:14:15,000
or some Buddha.

119
00:14:15,000 --> 00:14:19,000
So the first is Sila, some Buddha.

120
00:14:19,000 --> 00:14:23,000
This is restraining ourselves using ethical precepts.

121
00:14:23,000 --> 00:14:28,000
This is what I was talking about, not killing, and not stealing,

122
00:14:28,000 --> 00:14:33,000
not speaking lies or speech that hurts others.

123
00:14:33,000 --> 00:14:39,000
We can restrain ourselves simply by

124
00:14:39,000 --> 00:14:46,000
vowing to keep determining our determination to keep ethical precepts.

125
00:14:46,000 --> 00:14:47,000
When we say,

126
00:14:47,000 --> 00:15:06,000
we don't know how to do it, we don't know how to do it.

127
00:15:06,000 --> 00:15:19,000
But just by keeping ethical precepts as a restraint, because it stops you, of course, from doing and saying,

128
00:15:19,000 --> 00:15:22,000
and even intending to do things.

129
00:15:22,000 --> 00:15:29,000
The second is Sati Samura, and this is where our practice comes in.

130
00:15:29,000 --> 00:15:34,000
Rather than just forcing ourselves to keep precepts, Sati keeps us so objective that we wouldn't do things that could ever be considered breaking an ethical precept.

131
00:15:34,000 --> 00:15:40,000
You can't want to harm another if your mind is pure.

132
00:15:40,000 --> 00:15:46,000
You can't want to steal or cheat or lie.

133
00:15:46,000 --> 00:15:51,000
The mind is too pure, too clear for that.

134
00:15:51,000 --> 00:15:54,000
Mindfulness keeps the mind.

135
00:15:54,000 --> 00:16:00,000
Mindfulness is the best guard, Sati Samura.

136
00:16:00,000 --> 00:16:04,000
It's not the best guard, it's an active guard.

137
00:16:04,000 --> 00:16:09,000
It's how we engage in guarding our activities as we practice.

138
00:16:09,000 --> 00:16:12,000
We don't repress our desire to do things.

139
00:16:12,000 --> 00:16:24,000
When we're aware of the desire, the desire doesn't lead to a need to fulfill the desire or the aversion, anger.

140
00:16:24,000 --> 00:16:32,000
The best is actually the next one, Nyana Samura, and that's what comes from Sati, because as you keep the mind objective,

141
00:16:32,000 --> 00:16:36,000
you start to understand, you get a clearer picture of reality.

142
00:16:36,000 --> 00:16:40,000
Walking is just walking, seeing is just seeing.

143
00:16:40,000 --> 00:16:44,000
Experience is impermanent, unsatisfying, uncontrollable.

144
00:16:44,000 --> 00:16:51,000
Our reasons for clinging, for reacting fall away.

145
00:16:51,000 --> 00:16:57,000
They become meaningless.

146
00:16:57,000 --> 00:17:03,000
We see our reactions, we see our bad behavior for what it is.

147
00:17:03,000 --> 00:17:10,000
Bad, we see it as causing suffering.

148
00:17:10,000 --> 00:17:13,000
That knowledge, Nyana Samura, Nyana means knowledge.

149
00:17:13,000 --> 00:17:20,000
The knowledge that comes from mindfulness is the best kind of guard.

150
00:17:20,000 --> 00:17:23,000
You really don't need to guard at that point.

151
00:17:23,000 --> 00:17:25,000
Your guard end.

152
00:17:25,000 --> 00:17:28,000
You're restrained by the truth.

153
00:17:28,000 --> 00:17:33,000
Someone who knows the truth, it might be surprising, but someone who knows the truth, they say it sets you free,

154
00:17:33,000 --> 00:17:36,000
but it also restrains you.

155
00:17:36,000 --> 00:17:39,000
Someone who knows the truth is incredibly restrained.

156
00:17:39,000 --> 00:17:42,000
There are so many things they just can't do.

157
00:17:42,000 --> 00:17:46,000
It seems kind of strange.

158
00:17:46,000 --> 00:17:51,000
But it's not an inability to do.

159
00:17:51,000 --> 00:17:57,000
It's an impossibility for them to ever do it.

160
00:17:57,000 --> 00:18:03,000
Impossible, because those things, there are many things that we might do

161
00:18:03,000 --> 00:18:07,000
that would require a delusion, that require ignorance.

162
00:18:07,000 --> 00:18:11,000
You can't engage in something that hurts you if you know it's going to hurt you.

163
00:18:11,000 --> 00:18:17,000
If you really know and understand.

164
00:18:17,000 --> 00:18:24,000
And the other two forms of Samura are Kanti Samura and Wiriya Samura.

165
00:18:24,000 --> 00:18:31,000
And these really have a lot to do with the meditation practice, so they can be included in the rest, in mindfulness.

166
00:18:31,000 --> 00:18:32,000
Really.

167
00:18:32,000 --> 00:18:36,000
Kanti means patience, effort, Wiriya means effort.

168
00:18:36,000 --> 00:18:38,000
And you can engage out of patience.

169
00:18:38,000 --> 00:18:41,000
You can be patiently restrained outside of the practice.

170
00:18:41,000 --> 00:18:49,000
But the best type of patience comes from mindfulness, comes from practice, comes from knowledge.

171
00:18:49,000 --> 00:18:54,000
You might say that it's a part of the practice that you need not only mindfulness, but also patience.

172
00:18:54,000 --> 00:18:59,000
And you need not only patience and mindfulness, but you also need effort.

173
00:18:59,000 --> 00:19:06,000
You can use effort alone to restrain yourself, forcing yourself, or pressing your desires as well.

174
00:19:06,000 --> 00:19:20,000
But the best type of effort, just as the best type of patience, is one that is engaged with mindfulness, is associated, accompanied by mindfulness.

175
00:19:20,000 --> 00:19:35,000
When you have this state of seeing things as they are, you use effort to continue and to refine systematically, bringing the mind back again and again to the present moment.

176
00:19:35,000 --> 00:19:46,000
Patience so that you don't get caught up and lost in your bad habits.

177
00:19:46,000 --> 00:19:55,000
Patiently bearing with desire, the desire to get lost, desire to follow after some central object or object.

178
00:19:55,000 --> 00:20:05,000
The aversion to run away from it, to avoid it, patience to stay with it, patience to be mindful to face it.

179
00:20:05,000 --> 00:20:10,000
Patience to face it, effort to face it.

180
00:20:10,000 --> 00:20:15,000
This is how you restrain the mind, to the Buddha said,

181
00:20:15,000 --> 00:20:24,000
the wise or restrained body, they are also restrained in speech.

182
00:20:24,000 --> 00:20:31,000
We talk about restraint, being restrained, how do you restrain yourself, those who are well and fully restrained,

183
00:20:31,000 --> 00:20:35,000
are restrained in these three ways.

184
00:20:35,000 --> 00:20:40,000
Restrained physically, restrained verbally, restrained mentally.

185
00:20:40,000 --> 00:20:50,000
That means applying mindfulness to our physical acts, walking ethically, to our verbal acts, speaking ethically,

186
00:20:50,000 --> 00:20:54,000
and to our mental acts, thinking ethically.

187
00:20:54,000 --> 00:21:00,000
Pure deans, pure speech, pure thoughts.

188
00:21:00,000 --> 00:21:10,000
It is very important from teaching that is very applicable to the meditation practice.

189
00:21:10,000 --> 00:21:12,000
It relates very much to the practice.

190
00:21:12,000 --> 00:21:18,000
Our engagement with the world can be summed up under these three headings, physical, verbal and mental,

191
00:21:18,000 --> 00:21:23,000
and all of them are determined by our state of mind, our intention.

192
00:21:23,000 --> 00:21:25,000
We intend to do something.

193
00:21:25,000 --> 00:21:27,000
We intend to say something.

194
00:21:27,000 --> 00:21:34,000
We even intend to think, our inclination, our state of mind influences our acts,

195
00:21:34,000 --> 00:21:39,000
not just in terms of killing and stealing and lying and cheating,

196
00:21:39,000 --> 00:21:42,000
but in terms of the quality of our actions.

197
00:21:42,000 --> 00:21:51,000
We might intend to help someone, but based on our greed, our anger, our delusion, arrogance, conceit.

198
00:21:51,000 --> 00:21:57,000
We express ourselves poorly, and we harm the person we're trying to help.

199
00:21:57,000 --> 00:22:02,000
Even when trying to help, if we're not guarded,

200
00:22:02,000 --> 00:22:07,000
even just walking down the street, we can do it unethically.

201
00:22:07,000 --> 00:22:14,000
If we don't hurt someone else, we hurt ourselves.

202
00:22:14,000 --> 00:22:21,000
And a reminder that even mentally, we can create violence.

203
00:22:21,000 --> 00:22:23,000
Mano pakopa.

204
00:22:23,000 --> 00:22:27,000
Pakopa is probably something more like anger.

205
00:22:27,000 --> 00:22:30,000
Remember, this is the anger chapter.

206
00:22:30,000 --> 00:22:35,000
This is the end of the anger chapter, but the idea is violence.

207
00:22:35,000 --> 00:22:37,000
Mental violence.

208
00:22:37,000 --> 00:22:38,000
We shouldn't engage in it.

209
00:22:38,000 --> 00:22:40,000
We should guard against it.

210
00:22:40,000 --> 00:22:44,000
What is mental violence thinking?

211
00:22:44,000 --> 00:22:48,000
You can think to hurt others, and you may not hurt them with your thoughts,

212
00:22:48,000 --> 00:22:50,000
but it will certainly dispose.

213
00:22:50,000 --> 00:22:57,000
You poorly towards them, so that later when you act towards them,

214
00:22:57,000 --> 00:23:03,000
it can be based very much, and it will be based very much on your state of mind.

215
00:23:03,000 --> 00:23:11,000
Or you might decide not to help, not to engage with someone out of anger,

216
00:23:11,000 --> 00:23:14,000
or out of greed, or out of delusion.

217
00:23:14,000 --> 00:23:18,000
So mental thoughts can prevent you from doing your duty towards others,

218
00:23:18,000 --> 00:23:22,000
doing what's proper.

219
00:23:22,000 --> 00:23:25,000
It can be very harmful to others, just our thoughts,

220
00:23:25,000 --> 00:23:29,000
but most importantly, of course, is how harmful it is to us.

221
00:23:29,000 --> 00:23:35,000
And by harming ourselves, of course, we harm others, because we are not pure in mind.

222
00:23:35,000 --> 00:23:39,000
We are unable to relate to others properly.

223
00:23:39,000 --> 00:23:41,000
We harm others.

224
00:23:41,000 --> 00:23:47,000
We fail to support others or to perform.

225
00:23:47,000 --> 00:23:51,000
The things that are right to perform are duties.

226
00:23:51,000 --> 00:23:57,000
We don't do the right thing, because our minds are in the wrong place.

227
00:23:57,000 --> 00:24:02,000
So a reminder of these three doors, and how they constitute the basis.

228
00:24:02,000 --> 00:24:08,000
I mean, it's really another way of looking at the Four Satipatana.

229
00:24:08,000 --> 00:24:11,000
When you speak even, being mindful when you speak,

230
00:24:11,000 --> 00:24:13,000
mindful of the movement of the lips,

231
00:24:13,000 --> 00:24:18,000
but mindful also of the intention to speak and the emotions behind your speech.

232
00:24:18,000 --> 00:24:21,000
When you want to say something after you've said something

233
00:24:21,000 --> 00:24:24,000
and how you feel about what you've said,

234
00:24:24,000 --> 00:24:30,000
when you're engaged in conversation, listening to other speak,

235
00:24:30,000 --> 00:24:35,000
physically, verbally, mentally, being ready, being present,

236
00:24:35,000 --> 00:24:39,000
being confined to what is good and true and right,

237
00:24:39,000 --> 00:24:42,000
and pure.

238
00:24:42,000 --> 00:24:45,000
That's the teaching here.

239
00:24:45,000 --> 00:24:47,000
And that's the Damapada for tonight.

240
00:24:47,000 --> 00:24:54,000
Thank you all for listening.

