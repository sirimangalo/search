1
00:00:00,000 --> 00:00:16,380
Here's the gravel and the sand, it's been moved already, and they're going to make the

2
00:00:16,380 --> 00:00:27,700
bathroom, two bathrooms, but I'll leave this as a little bit awkward because there's

3
00:00:27,700 --> 00:00:36,120
going to be a meditation hall right next to it. I thought that was a good thing, but it's

4
00:00:36,120 --> 00:00:41,440
sure that it's going to make noise and disturbance to the meditators. Also, we can use this

5
00:00:41,440 --> 00:00:51,160
area for part of the meditation hall, which means we have a bigger meditation area, and so we've

6
00:00:51,160 --> 00:01:03,580
moved our location to down here. When this is not where we're going to make meditation

7
00:01:03,580 --> 00:01:18,900
hall, this man, Dhamma Latana, and here's Manju, here's the workers, Garmini, Haritanda,

8
00:01:18,900 --> 00:01:30,740
Haritanda, Nima, oh, and these are the bees. There's many. The bees get in your ears apparently.

9
00:01:30,740 --> 00:01:35,360
Anyway, so this is going to be where the washroom is. It's kind of a nice spot. It's

10
00:01:35,360 --> 00:01:41,840
the lowest spot, so I think that's good for a washroom. And then here, we still have room

11
00:01:41,840 --> 00:01:51,560
for building, maybe a kuti here, Kuti, and maybe a kuti here. I don't know. Okay, so this

12
00:01:51,560 --> 00:01:59,360
is an update on our working progress. Today we have to drill into the stones every nine

13
00:01:59,360 --> 00:02:08,080
inches of a bolt has to go into the rock just to be able to make the foundation. That's

14
00:02:08,080 --> 00:02:33,360
what we're going to start today.

