1
00:00:00,000 --> 00:00:09,800
Okay, now the reason why we watch the abdomen during vipassana instead of the nostrils.

2
00:00:09,800 --> 00:00:21,200
Well, first of all, anepanasity doesn't require that you look at the nostrils.

3
00:00:21,200 --> 00:00:32,200
Another one who insists that anepanasity is focusing on the nostrils is really being incredibly

4
00:00:32,200 --> 00:00:43,040
dogmatic because there's no reason why the nostril is any more appropriate as a meditation

5
00:00:43,040 --> 00:00:55,840
object than the stomach, this is one thing. Now the qualifier here is that it seems to be

6
00:00:55,840 --> 00:01:02,920
that the Buddha was suggesting that we focus on the nostril. The parimukkang means around

7
00:01:02,920 --> 00:01:10,000
the mouth. Some people have said, no parimukkang means to the front. Parimukkang

8
00:01:10,000 --> 00:01:19,400
sattang upadapitwa. He sets up mindfulness either around the mouth, parimukkang or

9
00:01:19,400 --> 00:01:29,720
at the front puts mindfulness as the front because parimukkang means front or here the door.

10
00:01:29,720 --> 00:01:34,040
As many meanings, but parimukkawa, we don't know exactly what this word means. It's not

11
00:01:34,040 --> 00:01:44,160
used very often in anepitaka. But sure, it could very well mean that the Buddha said focus

12
00:01:44,160 --> 00:01:52,000
on the area around the mouth, the front of the face. By extension, people think it means

13
00:01:52,000 --> 00:01:56,880
the nose. It actually obviously doesn't mean the nose. To say the Buddha taught the nostrils

14
00:01:56,880 --> 00:02:02,320
is actually incorrect. He said focus around the mouth. He may have said, or he may have

15
00:02:02,320 --> 00:02:07,720
just said, put mindfulness to the front, which many people translated his meaning. I believe

16
00:02:07,720 --> 00:02:17,040
that's how the commentary understands it. Even if the Buddha had said, focus on your nostrils

17
00:02:17,040 --> 00:02:26,320
or in this case, either bikowai bikhu. Here amongst a monk does this. Doesn't mean that

18
00:02:26,320 --> 00:02:31,080
that's the only way or there's something special about the nose or there's something special

19
00:02:31,080 --> 00:02:36,680
about the mouth. It doesn't mean that there's anything wrong with focusing on the abdomen.

20
00:02:36,680 --> 00:02:43,040
I've had people say to me, oh yes, focusing on the abdomen is not the Buddha's teaching.

21
00:02:43,040 --> 00:02:49,560
Such a ridiculous comment and over and over. In fact, the majority of Sri Lankan people

22
00:02:49,560 --> 00:02:58,800
have been brainwashed into thinking this. By monks who have come to agree that yes, the Buddha

23
00:02:58,800 --> 00:03:09,160
didn't teach focusing on the abdomen. Focusing on the abdomen is not the Buddha's teaching.

24
00:03:09,160 --> 00:03:15,320
It is in fact a part of the Buddha's teaching quite explicitly. The Buddha does talk about

25
00:03:15,320 --> 00:03:23,480
the abdomen as a meditation object. The rising and falling of the abdomen is mentioned

26
00:03:23,480 --> 00:03:29,640
in the tapithika. But regardless, this shows incredible, if someone, this is not what you're

27
00:03:29,640 --> 00:03:35,320
saying. But for those people who say that, this, and I'm not trying to be judgmental and

28
00:03:35,320 --> 00:03:40,320
I'm not, I really, I, you know, it doesn't change my opinion of these people. Many of them

29
00:03:40,320 --> 00:03:48,560
are great people. But it shows great ignorance, willful ignorance of the Buddha's teaching.

30
00:03:48,560 --> 00:03:58,800
Because what is the abdomen? It's Gaiya Nupasana or it's the Rupa Kanda. It's the Dukasatcha.

31
00:03:58,800 --> 00:04:04,000
It's many, many, many, many, many parts. It's very much a core of the Buddha's teaching.

32
00:04:04,000 --> 00:04:07,240
How could you say the rising and falling of the abdomen is not the Buddha's teaching and

33
00:04:07,240 --> 00:04:11,240
watching and it's not the Buddha's teaching? This monk was trying to explain this to me

34
00:04:11,240 --> 00:04:19,000
and he said, look, have you never heard of the contemplation of the four elements? And he said,

35
00:04:19,000 --> 00:04:23,360
what do you mean the full? I said, why Odattu? You don't know. And he said, oh, well,

36
00:04:23,360 --> 00:04:27,800
if you focus on it as why Odattu, then yes, it's a part of the Buddha's teaching as well.

37
00:04:27,800 --> 00:04:35,920
What else would it be? It's why Odattu. It's, it's, so I am using pāori words. It's the,

38
00:04:35,920 --> 00:04:43,560
it's the air element. In this case, the pressure element, that part of the physical that

39
00:04:43,560 --> 00:04:49,600
is the experience of the pressure. We feel the pressure and the stomach. So this is,

40
00:04:49,600 --> 00:04:54,960
this is, I think, not exactly what you're asking, but it's important to understand.

41
00:04:54,960 --> 00:05:04,360
The actual reason, as I understand it, why Mahasisada and not even Mahasisada, but why

42
00:05:04,360 --> 00:05:11,600
his teacher and the teachers that were advocating this at the time that Mahasisada went

43
00:05:11,600 --> 00:05:21,640
to practice meditation is actually a really trivial point, but, but important in a, in a country

44
00:05:21,640 --> 00:05:28,360
like Burma. I'm not saying this is the only reason to focus on the stomach because I'll,

45
00:05:28,360 --> 00:05:39,080
I'll actually point out where the Buddha talked about focusing on the stomach, but the

46
00:05:39,080 --> 00:05:44,560
point is a bit trivial, but it's important, as I said, in a place like Burma, the point

47
00:05:44,560 --> 00:05:56,480
is Anapanasati, in the Visudhi manga, is called samata meditation. Anapanasati is not considered

48
00:05:56,480 --> 00:06:05,960
a part of Vipasana meditation. So, if one wants to begin by practicing Anapanasati, when

49
00:06:05,960 --> 00:06:12,260
then has to transfer over to mindfulness of the four dadoos, according to the Visudhi

50
00:06:12,260 --> 00:06:18,360
manga, which makes perfect sense. It may not be, some people totally disagree with it,

51
00:06:18,360 --> 00:06:25,560
but it, it's perfectly plausible explanation that focusing on the breath, Anapanasati, the

52
00:06:25,560 --> 00:06:34,760
in-breaths and the out-breaths, Anapanasati, or however it's translated, the mindfulness

53
00:06:34,760 --> 00:06:40,200
of the in and out breathing, is actually mindfulness of a concept, because nothing's going

54
00:06:40,200 --> 00:06:50,840
in. Your experience of the, of the reality is, is not of the, anything going in and coming

55
00:06:50,840 --> 00:06:57,720
out. Your experience is the sensation at the nose, the sensation on your lip, the sensation

56
00:06:57,720 --> 00:07:03,520
in your chest, the sensation at your stomach. That's the ultimate reality. If you're just

57
00:07:03,520 --> 00:07:14,120
focusing on the breath, you're focusing on a concept. And so, really Anapanasati is focusing

58
00:07:14,120 --> 00:07:18,960
on the in-breaths and out-breaths, and that's, that's conceptual. When you say in one, out,

59
00:07:18,960 --> 00:07:28,760
one into, out to, or even just counting, one, one, two, two, or one, two, and so on, you're

60
00:07:28,760 --> 00:07:34,000
focusing on something that doesn't exist. You're focusing on an aspect of the experience

61
00:07:34,000 --> 00:07:38,520
that is conceptual. Nothing's going in or out, not in your experience anyway. Your experience,

62
00:07:38,520 --> 00:07:43,560
as I said, is of the sensations. So, it's called samata meditation, and that seems

63
00:07:43,560 --> 00:07:54,040
quite reasonable to me. That being so, and considering that in, in the Visudhi Maga, I believe,

64
00:07:54,040 --> 00:08:01,400
it talks about focusing on the nostrils in, as being Anapanasati, or one example of Anapanasati.

65
00:08:01,400 --> 00:08:11,720
There are actually other examples. They didn't want to use that as the object of

66
00:08:11,720 --> 00:08:19,480
vipasana meditation. Even though clearly the sensations here can be the subject of the object

67
00:08:19,480 --> 00:08:24,680
of vipasana meditation, they thought, and this is what Mahasati actually says. He says,

68
00:08:24,680 --> 00:08:29,160
if I teach this, people will think I'm teaching samata meditation. So, the monks in

69
00:08:29,160 --> 00:08:37,000
Burma would say, well, yeah, Anapanasati, he's teaching samata. So, it was actually contrary

70
00:08:37,000 --> 00:08:42,120
to the argument that people post nowadays. So, the Mahasati said, of focus on the stomach, and people

71
00:08:42,120 --> 00:08:45,800
say, it's not the Buddha's teaching. They say, it's samata. There's many, many different things.

72
00:08:47,080 --> 00:08:54,200
But the reason for focusing on the stomach is because that clearly is the YODA to the air element,

73
00:08:55,000 --> 00:09:02,760
the experience of something becoming stiff, and then becoming flaccid. It's an ultimate reality.

74
00:09:02,760 --> 00:09:09,400
It's impermanent. It's unsatisfying, or unsatisfiable, or suffering, or however you want to

75
00:09:09,400 --> 00:09:15,400
translate. And it's uncontrollable. You will see these three things as you focus on it perfectly

76
00:09:15,400 --> 00:09:20,200
clearly. And this is why people don't like it. See, the truth of why people don't want to focus

77
00:09:20,200 --> 00:09:26,600
on the abdomen is because it's not fun. Anapanasati clearly can be samata.

78
00:09:26,600 --> 00:09:33,480
It's very, anyone who says, we pass in only, is totally wrong. Because when you focus on the

79
00:09:33,480 --> 00:09:38,040
nostril, when you focus on the breath going in and out, it's wonderful. You feel peaceful,

80
00:09:38,040 --> 00:09:44,440
you feel happy, and I have a break. No? It's samata meditation. Good, great, happiness.

81
00:09:45,640 --> 00:09:51,800
But it's not me passing that. Not as most people practice it. When you focus on the abdomen,

82
00:09:51,800 --> 00:09:56,840
you don't get that. It's not comfortable. For most people, it's quite unpleasant.

83
00:09:58,360 --> 00:10:05,320
You feel stressful. You find yourself forcing the breath. You're trying to make it

84
00:10:05,320 --> 00:10:09,080
smooth. You're trying to make it constant. It's not constant. Sometimes it disappears.

85
00:10:10,040 --> 00:10:18,920
Oh, how frustrating. But this is reality. And this is showing us our inability to accept reality,

86
00:10:18,920 --> 00:10:25,560
which is what totally the purpose of we pass in us. So anyone who can overcome these views

87
00:10:26,280 --> 00:10:31,000
and so many Buddhists have these views and have been given these views by their teachers and so on,

88
00:10:31,000 --> 00:10:36,360
you can overcome it and actually just practice it and try it. You'll be amazed.

89
00:10:36,360 --> 00:10:39,960
And this is why it's so great teaching people who have no clue about Buddhism because they say,

90
00:10:39,960 --> 00:10:45,160
okay, I'll try and say, wow, this works. And then they learn about all the different techniques,

91
00:10:45,160 --> 00:10:53,000
and they say, well, that's fine. I've got mine and it works. But that's really the reason.

92
00:10:53,000 --> 00:11:00,360
And it's a trivial, I think what I mean is that the exact reason is pretty trivial.

93
00:11:00,360 --> 00:11:04,200
I mean, we don't have that problem. We don't have a bunch of monks saying, oh, don't focus on

94
00:11:04,200 --> 00:11:11,400
the nose, that samata. We won't get that. But the point is clear that if you focus on the nose,

95
00:11:11,400 --> 00:11:21,560
no, if you focus, if you practice Anapana Sati, you will generally tend to follow the breath.

96
00:11:22,520 --> 00:11:27,080
And not only follow the breath, you'll tend to fall in the states of common tranquility.

97
00:11:27,640 --> 00:11:33,480
You'll be focused on this experience of the breath and you'll fall into common tranquility.

98
00:11:35,240 --> 00:11:40,760
If you can go beyond that and come to see the impermanence and so on and eventually focus on the

99
00:11:40,760 --> 00:11:47,720
reality of the experience, which is the sensation at the nostrils of the heat and the cold and so on,

100
00:11:47,720 --> 00:11:55,960
then it certainly can be the person. But when you focus on one point, whether it be the nostril

101
00:11:55,960 --> 00:12:03,160
or it be the stomach and you're clear about watching the experience and you're no longer focusing

102
00:12:03,160 --> 00:12:17,560
on the breath, then we pass in and becomes quite clear. So I think my favorite answer to this

103
00:12:17,560 --> 00:12:22,040
question, that's really, I think, should answer the question. But my favorite answer is to say that

104
00:12:22,840 --> 00:12:29,960
well, focusing on the abdomen is Anapana Sati. What is it if it's not mindfulness of breathing?

105
00:12:29,960 --> 00:12:34,920
But the point is it's only one kind of mindfulness of breathing. It's the kind which

106
00:12:35,560 --> 00:12:41,400
is focusing on the ultimate reality of breathing, which is the five aggregates.

107
00:12:42,200 --> 00:12:46,040
When you watch the stomach, there are the five aggregates. There is the Rupa aggregate,

108
00:12:46,040 --> 00:12:51,080
which is waio dato. There may even be the fire aggregate. If you feel heat or cold,

109
00:12:51,080 --> 00:13:00,520
this is Rupa dato, Rupa kanda. When you're in a kanda, you'll feel unpleasant sometimes,

110
00:13:00,520 --> 00:13:06,440
pleasant sometimes, you'll feel a neutral sensation sometimes. Sanya kanda, you will be aware this

111
00:13:06,440 --> 00:13:12,360
is rising, this is falling, you'll recognize it for what it is. Sankara kanda, sometimes you're judging

112
00:13:12,360 --> 00:13:18,680
it, knowing this is long, this is short, this is good, this is bad, liking it and disliking it.

113
00:13:18,680 --> 00:13:24,200
In vinyana kanda, vinyana kanda is the mind which goes to know the rising. You'll see the five

114
00:13:24,200 --> 00:13:28,600
aggregates. Once you look at them, you'll see the five aggregates as being impermanent, unsatisfying

115
00:13:28,600 --> 00:13:34,760
and uncontrollable, which is totally clear from the very outset. Anyone who has any doubt about

116
00:13:34,760 --> 00:13:40,200
the rising and falling has to be asked this question, are you not seeing impermanent suffering

117
00:13:40,200 --> 00:13:45,720
in non-self? Because it's impossible that you should look at the abdomen for even a short time

118
00:13:45,720 --> 00:13:53,400
and not see these three things. And therefore, it's perfectly, incredibly, you know,

119
00:13:53,400 --> 00:13:59,720
it's undeniable that by doing this, you're going to see the truth of life. That's our reason

120
00:13:59,720 --> 00:14:04,360
for watching the abdomen. It's an incredibly useful object for seeing things clearly,

121
00:14:05,240 --> 00:14:10,440
even though it may not lead to states of peace and calm. So, for those people who want the

122
00:14:10,440 --> 00:14:17,000
states of peace and calm, by all means it's a wonderful thing. If you have the time do it, take the

123
00:14:17,000 --> 00:14:23,640
time to develop calm as well. You might even get magical powers, be able to read people's minds,

124
00:14:23,640 --> 00:14:33,480
remember past lives and so on. Great. But in the end, you have to switch on the insight lens

125
00:14:33,480 --> 00:14:41,800
and focus on the feeling at the nose, the feeling at the stomach or so on. Okay. So, I hope

126
00:14:41,800 --> 00:14:46,440
that gives a fairly comprehensive answer to this popular question, one that's asked quite often by

127
00:14:46,440 --> 00:15:16,280
a diverse range of people.

