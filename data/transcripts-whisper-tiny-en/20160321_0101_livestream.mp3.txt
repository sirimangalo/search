Good evening everyone, broadcasting live.
March 20th.
I've cut my meditation short this evening.
It's been a long day, lots of different things.
Home work.
Anyway, here on time.
So today's quote is quite an interesting one.
Okay.
I'm going to go to the kitchen and I'm going to go to the kitchen and I'm going to go to the kitchen.
I'm going to go to the kitchen and I'm going to go to the kitchen and I'm going to go to the kitchen.
Thank you very much.
Thank you very much.
Thank you very much.
Thank you very much.
Thank you very much.
This is one seizes another.
It's a simple philosophy and claim but it has deep meaning.
You see, so science and ordinary people have this idea of a substratum, most religions as well.
Science is quite convinced that there is a substratum of reality.
One is usually quite convinced that there is a substratum of mentality and self-god.
There is a lot of postulation of various substratum.
That religion also does a better job according with mental reality sometimes than science.
Nonetheless, the tendency is to postulate substratum.
Some substratum means like a framework, like a mind, a body, a three-dimensional, four-dimensional reality that we live in.
Buddhism doesn't do that.
Buddhism has a very simple take on the universe that this moment is real, this moment is an experience, and then it ceases, and then this moment is real.
That moment, so when we talk about mind in terabyte of Buddhism, we mean that moment of mind, one moment of mind, one moment of experience, one moment of awareness of contemplation of an object, observation of an object, one moment of experience.
If you ask the question, who is right?
It's not really the appropriate question.
Scientists tend to scoff at Buddhism as being overly simple, simplistic, and they'll say, okay, well, maybe you're right, but you haven't said anything about reality.
You haven't figured out subatomic particles, or how to build an atom bomb, or have you explored the best reaches of the universe, solar system, and the galaxy, the known universe, and then you haven't really explored reality.
We can say, no, no, we haven't.
The question is to what end?
The question of all of this is to what end.
Religion makes all sorts of claims about reality, and we can ask what is the result?
If it were useful and positive to delude yourself, and make claims about reality that were not in accord with reality, that had no evidence,
not based on evidence, if that were somehow beneficial, then there wouldn't be much of a problem, most religions would do fine, but because they tend to not relate to actual experience and reality, they tend to rely more on belief.
They tend to be quite artificial and stilted or forced.
If you step back, or say you step out of a meditation course where you've been studying your own mind and what's really here and now,
and then you contemplate one of these religions, pick any religion or random, and you contemplate what they're actually saying.
It's shocking how absurd it always.
What are you talking about? What is this garbage?
So much belief. There was this guy, he said this, he claimed this, he did this, and he will save us.
Or there's this god up there, and we have to do his prey, or repeat his name, or chant this, or chant that, and he will save you.
So they run into lots of problems, and I think wise people would tend to stay away from most of the religions out there because they tend to be not very closely aligned to reality.
But science does a good job focusing on reality, aligning itself with experience.
They are able to run experiments that actually accord with the way things are, but they've got a different problem.
And we can argue about what is more real in the substratum, physical and even maybe mental reality, or experience. Which one is more real? We can argue about that, but it's not an important argument again to what end?
What has science given us? What has it brought us?
It's brought us all sorts of magical, wonderful things like the internet, the fact that I'm able to talk to people all around the world, kind of magical.
It's brought us all this, very powerful, this kind of knowledge.
But has it brought us happiness, has it brought us peace, has it brought us understanding?
And I mean a specific, I'm thinking of a specific type of understanding, really.
Has it brought us to understanding of ourselves? No.
None people who study science get angry, become addicted to things, they have a hard time dealing with ordinary experiences, and so they suffer.
They stress, they kill themselves, they drown their, soak their brains in alcohol, or drugs.
Because they can't deal with ordinary reality, right?
They study reality so much, but can't deal with it.
And then you have this Buddhist, I'd say, a meditator who focuses on their reality, who looks at their experience.
When they have emotions, they see them as emotions, when they have pain, they see it as pain.
The experiences of seeing their hearings, they see them for what they are.
They put aside any theories or thoughts about a some stratum about reality.
Like, oh, here I'm in this room, they even lose track of being in a room.
They just see the mind arising, and see seeing the mind arises, there's awareness of something.
And see, it says, immediately there's another mind, but it's totally different.
It's a whole new mind, whole new experience.
When they see this, they're able to see a reality or a way of looking at reality for themselves.
That leads to objectivity.
They're able to fully comprehend reality, I guess you can say.
Because science, you can study it, but you can't fully comprehend it.
You can't look at the wall and comprehend that that wall is made up of mostly empty space.
I need to make it a given example, but moreover, you're not able to comprehend all those things.
I guess I would say it's not even worth, it wouldn't even be worth it if we could.
And in fact, our comprehension of things is part of the problem.
Because when you think about concepts, you think about entities, you have to abstract something.
You have this mouse.
What happens when I think of this mouse?
There's something going on in my mind.
There's an experience.
There's an experience which says there is a mouse in your hand, but it has nothing to do directly with the experience of seeing the mouse.
I think the seeing and the feeling that created it.
It's all in the mind at that point.
There are minds arising in safety, leading, therefore, therefore, one to the other.
And I'm not aware of that.
So there's a reality that is being ignored, because instead I'm focused on the idea of the mouse.
So there's something that I'm missing.
And as a result, when likes and dislikes come up, I miss them as well.
So I like them now.
So I dislike it.
And that's what leads to frustration and addiction and so on.
So it leads us attached to things and people and experiences and leads us to get frustrated and upset and bored.
Disappointed when we don't get what we want.
So when we pay attention to that, if I'm picking up the mouse, I pay attention to the experience and the thinking and the judging,
then I am in a position to understand and to see the experience clearly and to not react.
Or to understand my reactions and to see how they're hurting me and to slowly give them up.
This is an important, an important quote to help us understand our focus should be.
Our focus should be on experiential reality.
This is what the Buddha talked about.
So here, what do we have? This also it is interesting.
The uninstracted world thing.
The puturjana.
The sutva.
The sutva bekabe puturjana.
Uninstracted puturjana.
Might experience revulsion towards this body.
It might become dispassionate towards it and be liberated.
It might even be liberated.
Because growth and decline are seen in this body.
It is seen and taken up.
He is saying it might be liberated from the body.
The body is revol. It is easy to see. It is revolting as you get old and if you get sick and so on.
It is possible for ordinary people to let go of it.
But the mind.
The mind we are unable to let go of.
This is the ordinary uninstracted world thing.
It is unable to experience revulsion towards it, unable to become dispassionate towards it and be liberated from it.
Because for a long time this has been held by him appropriate and grasped us.
This is me mine.
This I am, this is my self.
This is my self.
This is what we think of the mind.
Who doesn't think the mind is me and mine?
The body we can let go. When the mind is pleased or displeased, that's me, that's mine.
This is what we think of.
It becomes dispassionate.
It will be better to take. This is an interesting one.
Take the body as self because the body is standing for one year, for two years, for three, four, five, or ten years, for twenty, thirty, forty, fifty years, for a hundred years, or even longer.
But the mind and mentality and consciousness arises as one thing and this is the quote and ceases as another.
The mind is fighting, the mind arises and this is quickly.
And this is really what I'm talking about, is the body appears to be a thing.
If you think about the body, it's understandable that you would cling to it, but how can you cling to the mind?
This is a different nobody is exposing a different way of looking at reality from the point of view of the mind.
Because from the point of view of the mind, even the body arises and sees it.
And then he says, that there will be Kameh Sutula, a learned Ariya Saamako, disciple of the noble one.
Well, with wisdom, considers a tikshasamupada, the dependent origination.
If t, it must mean sati idhamo t, thus, when this exists, that comes to be, with the arising of this, that arises.
If t must mean sati idhamo t, it must subbada idhamo tatit. When this arises, there is that, with the arising of this, that arises.
He must mean, aseti with the non-existence of this, he doesn't know how t, there is not that.
He must n melo daí'mang idhamino jiri, Kumessa Sasashun of this, this, that is the distance.
India Didang there is say, ah dire bejap idea sankara sankara baj dirkmy interval and bhiti
cyopity comic bhita samabada dependent origination.
That's what he is saying here is, this is how experiential reality works.
Because we're ignorant, we give rise to our partialities.
We like some things in this language.
We chase after some things.
We chase away some things.
Because we don't see things clear.
It's a result we're born again and again.
And we have anyway leads to suffering.
And I'm going to detail with that.
The important point is that we look at reality
from the point of view of our experiences.
Because if you do that, there's really nothing.
You become invincible.
There's nothing that can empower over you.
Whether it be something pleasant that you want to.
There's no you, there's no thing.
You see it if you see it as an experience that arises.
There's something that upsets you or frustrates you.
That thing is just an experience.
You see it arising.
You see the experience.
What does it mean?
It's just an experience.
You see the frustration.
You see the upset.
Arise within ceases.
You stop hiding.
You stop running.
You stop living in conceptual reality.
Things and people and places.
You start living in reality in a true reality in a physical experience.
You start seeing what's going on underneath that.
A chrome plating of beautiful things and desirable things.
And ugly things and scary things.
And underneath it's all just experience.
Anyway, there's the quote for this evening.
That's the number for this evening.
Now as usual I'm going to put the quote.
I put the link to the hangout up.
If you would like to join and ask a question you're welcome to click the link.
Only if you have a question and only if you're in a respectful like no coming on naked
or smoking or drinking alcohol or eating food.
You should consider this to be a dumb hangout.
Just scare everybody away.
You can go the best I feel.
Thank you.
All right, nobody with burning questions sleeping to be to join the hangout.
And I think I'll say good night.
Thank you.
