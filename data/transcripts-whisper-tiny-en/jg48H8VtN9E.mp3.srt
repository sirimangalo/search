1
00:00:00,000 --> 00:00:05,000
Okay, next question comes from Luke Guelelees.

2
00:00:05,000 --> 00:00:08,500
Could you please talk about the two truths within Buddhist philosophy?

3
00:00:08,500 --> 00:00:13,300
Could you also articulate the difference between the Theravad of you on this Mahayana,

4
00:00:13,300 --> 00:00:18,000
on this and the Mahayana of you?

5
00:00:18,000 --> 00:00:26,000
I don't do well with questions regarding Theravad of Mahayana with the various schools of Buddhism.

6
00:00:26,000 --> 00:00:28,000
And I'd really rather not.

7
00:00:28,000 --> 00:00:29,000
I'm getting involved with it.

8
00:00:29,000 --> 00:00:31,000
First of all, because I don't feel qualified.

9
00:00:31,000 --> 00:00:35,500
Second of all, because I'd rather not separate Buddhism out into different parts.

10
00:00:35,500 --> 00:00:41,000
I'll tell you what I understand the Buddha to have taught, and you can decide whether it's

11
00:00:41,000 --> 00:00:46,000
or as various schools can decide whether it fits with their understanding.

12
00:00:46,000 --> 00:00:53,000
As I understand the Buddha taught, two parts to reality, and that's the conceptual and the ultimate.

13
00:00:53,000 --> 00:00:57,000
So the Buddha, when he would teach, he would talk in terms of concept.

14
00:00:57,000 --> 00:01:03,000
People, self, you know, you are your own refuge.

15
00:01:03,000 --> 00:01:06,000
You have to help yourself or so on.

16
00:01:06,000 --> 00:01:13,000
And he would talk about planes of existence like heaven and hell.

17
00:01:13,000 --> 00:01:19,000
He would use these as a conceptual way of helping people to understand reality.

18
00:01:19,000 --> 00:01:24,000
And then he also taught about ultimate reality, about the building blocks.

19
00:01:24,000 --> 00:01:32,000
So you could liken this to someone talking about biology as opposed to talking about physics

20
00:01:32,000 --> 00:01:34,000
or quantum physics.

21
00:01:34,000 --> 00:01:43,000
And these are two different realms, but there are two different ways of talking about the same thing.

22
00:01:43,000 --> 00:01:48,000
So biology talks about systems, which are actually just a concept.

23
00:01:48,000 --> 00:01:53,000
The liver, the heart, the brain, the organism.

24
00:01:53,000 --> 00:02:01,000
Because when you go get down to the ultimate reality, it's made up of various component parts.

25
00:02:01,000 --> 00:02:09,000
And the interesting thing is that it seems in both quantum physics and Buddhist meditation,

26
00:02:09,000 --> 00:02:15,000
we're dealing with two things in an ultimate reality sense, and that is the physical and the mental.

27
00:02:15,000 --> 00:02:18,000
The physical is the world as we understand it.

28
00:02:18,000 --> 00:02:21,000
The mental is the mind as we experience it.

29
00:02:21,000 --> 00:02:24,000
Our experience or the world around us.

30
00:02:24,000 --> 00:02:29,000
So when you see something, there is the light and the eye, which is physical,

31
00:02:29,000 --> 00:02:37,000
and then there is the mind, which knows, which is aware of the sight and that is mental.

32
00:02:37,000 --> 00:02:41,000
There's the sound in the ear, there's this smell in the nose, the taste in the tongue,

33
00:02:41,000 --> 00:02:50,000
the feeling in the body. These are all the parts, and then finally there's the thoughts and the awareness of the thoughts.

34
00:02:50,000 --> 00:03:02,000
So really all of ultimate reality can be broken down into these two component parts.

35
00:03:02,000 --> 00:03:05,000
That's what it's meant by the two truths.

36
00:03:05,000 --> 00:03:13,000
It's true that I am a human being, and I have hands, feet, organs, and so on.

37
00:03:13,000 --> 00:03:17,000
But there's only true in a conceptual sense. It's not actually real.

38
00:03:17,000 --> 00:03:21,000
These things do not actually exist. The body doesn't actually exist.

39
00:03:21,000 --> 00:03:29,000
In fact, the only reality from a Buddhist point of view is the experience of it.

40
00:03:29,000 --> 00:03:39,000
When you feel the pressure when you're sitting, of the back, and the legs touching the floor,

41
00:03:39,000 --> 00:03:46,000
this is only feeling. When you hear someone talking, it's only sound, and so on.

42
00:03:46,000 --> 00:03:57,000
It's not a being. The importance of this teaching is that it helps you to realign yourself with things with the way they are.

43
00:03:57,000 --> 00:04:03,000
It doesn't mean that when someone's talking to you, you ignore them because it's just sound touching the ear.

44
00:04:03,000 --> 00:04:14,000
But you understand it from the point of view of ultimate reality that it's a sound first, and the extrapolation of the being is just that is an extrapolation.

45
00:04:14,000 --> 00:04:24,000
As opposed to when you hear something, immediately focusing on the person and making judgments, getting angry, getting upset, and so on.

46
00:04:24,000 --> 00:04:37,000
You understand it from the point of view of the sound first, and you understand your reactions to it, and how the way you react to it changes your reality, changes your environment.

47
00:04:37,000 --> 00:04:42,000
So if you say no, see things in the person, obviously they're going to get even more angry.

48
00:04:42,000 --> 00:04:48,000
If you say nice things to them, then you can potentially solve the problem, solve the situation.

49
00:04:48,000 --> 00:05:03,000
When you feel pain in the body, you come to see it as pain first, as opposed to thinking of it as my leg is in pain, and that means something's wrong, and I have to move my leg.

50
00:05:03,000 --> 00:05:12,000
I have to stop meditating, or if it's a pain in the back, you have to see a masseuse, it's a pain in the head, you have to take a pain killer, so on.

51
00:05:12,000 --> 00:05:24,000
Instead of starting from the point of view of self, of ego, of me, of mine, of the head, the leg, you start from the point of view of it being pain.

52
00:05:24,000 --> 00:05:27,000
This is why we say to ourselves pain, pain, pain.

53
00:05:27,000 --> 00:05:39,000
Then you are able to formulate an answer to the question that it's posing, based on that reality, and the answer doesn't have to be moving your legs, stopping meditation,

54
00:05:39,000 --> 00:05:43,000
going in a masseuse, taking a pill.

55
00:05:43,000 --> 00:05:51,000
The answer comes to be that the answer comes to be that this is something that arises and ceases, it comes and goes.

56
00:05:51,000 --> 00:06:00,000
It's not hurting me, it's not causing affliction, it's not going to lead to my death, or dismemberment, or so on.

57
00:06:00,000 --> 00:06:03,000
It's going to go away, and when it's gone, I'll be happy.

58
00:06:03,000 --> 00:06:12,000
As a result, you don't get angry, you don't get upset, and you remain free from suffering for all intents and purposes.

59
00:06:12,000 --> 00:06:15,000
That's the understanding of two truths.

60
00:06:15,000 --> 00:06:27,000
The first truth is conceptual, this is talking in terms of entities of the self, the organs of the body, the body itself, objects, and so on.

61
00:06:27,000 --> 00:06:33,000
People places things, and then there's ultimate reality, which is the building blocks of reality. This is the second truth.

62
00:06:33,000 --> 00:06:41,000
This is the more ultimate truth, which is talking about things in terms of as they really are from an experiential level.

63
00:06:41,000 --> 00:06:48,000
Seeing, hearing, smelling, tasting, feeling, thinking, the physical and the mental parts of every experience.

64
00:06:48,000 --> 00:06:57,000
Did I answer your question? I hope so. Thanks for asking.

