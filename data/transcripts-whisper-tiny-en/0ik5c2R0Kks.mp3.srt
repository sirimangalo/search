1
00:00:00,000 --> 00:00:15,000
Good evening everyone broadcasting live May 6th.

2
00:00:15,000 --> 00:00:29,000
Today's quote is somewhat curious.

3
00:00:29,000 --> 00:00:36,960
And not quite, well we could body translation of course is preferred, but the word isn't

4
00:00:36,960 --> 00:00:46,320
quite false, it's um weepity, you know, weepity is failure, failings as people body

5
00:00:46,320 --> 00:01:03,840
translation, the weepity and somebody, weepity and somebody, weepity means lacking

6
00:01:03,840 --> 00:01:32,160
some somebody means attaining our feelings and our attainment, so the Buddha says and it's

7
00:01:32,160 --> 00:01:38,600
kind of, I wouldn't put too much on this, because the context of the quote is that they

8
00:01:38,600 --> 00:01:47,520
were not that just left and David that those course was the Buddha's cousin who um well

9
00:01:47,520 --> 00:01:54,760
he came to the Buddha and he said uh he said they're vulnerable sure you're getting

10
00:01:54,760 --> 00:02:04,520
old, so why don't you pass the the sun gone to me, all the the sun gone, and the Buddha said

11
00:02:04,520 --> 00:02:10,640
to him, I wouldn't even hand it over to Sariput and Mogulana let alone some insignificant

12
00:02:10,640 --> 00:02:18,280
little person like you, he says something really mean to him actually kind of um because

13
00:02:18,280 --> 00:02:23,120
David that is of control and so there's only one way it's going to end in the end the

14
00:02:23,120 --> 00:02:28,640
Buddha has to be firm, it's rare, because he's not usually like this, but with David

15
00:02:28,640 --> 00:02:35,880
that they had to be firm at all times, and so David had to go to her hips set and he tried

16
00:02:35,880 --> 00:02:45,280
to scheme up a way to uh to take control of the sun gone and so he came to the Buddha later

17
00:02:45,280 --> 00:02:51,360
and and asked him to instate five rules for monks that they should always live in the forest

18
00:02:51,360 --> 00:02:59,800
uh that they should only eat vegetarian food and things that actually were contentious

19
00:02:59,800 --> 00:03:06,200
and seemed reasonable, but the Buddha couldn't enforce and and he stopped short of forcing

20
00:03:06,200 --> 00:03:10,680
monks even though he praised living in the forest and even obviously eating meat

21
00:03:10,680 --> 00:03:15,280
had his problematic and there are types of meat that absolutely shouldn't be in the

22
00:03:15,280 --> 00:03:26,680
country um so he refused and they would use that as a grounds to start a schism he got

23
00:03:26,680 --> 00:03:33,880
monks on his side saying hey the Buddha clearly doesn't know what he's doing, but the monks

24
00:03:33,880 --> 00:03:39,760
he got on his side were actually new monks, their monks who didn't know better so they hadn't

25
00:03:39,760 --> 00:03:44,520
studied their mind, they hadn't really learned the intricacies of monastic life and so

26
00:03:44,520 --> 00:03:52,880
they thought is David that is uh David that was more serious, more serious than Buddha

27
00:03:52,880 --> 00:04:02,320
the Buddha seemed to be actually kind of lax and indulgent and so they went with David that

28
00:04:02,320 --> 00:04:07,480
but later the Buddha had started putting them on the land and go to see them and it's

29
00:04:07,480 --> 00:04:12,200
kind of funny how it turned out this is this sutta is actually before that but later on

30
00:04:12,200 --> 00:04:19,920
um sorry put in Mogulana went to see David that and David is at oh look here come the

31
00:04:19,920 --> 00:04:25,000
Buddha is too cheap disciples they're joining us as well I'm sorry put in Mogulana didn't

32
00:04:25,000 --> 00:04:31,400
say anything and so and so David that the said uh sorry put in Mogulana you teach the

33
00:04:31,400 --> 00:04:36,960
monks now I'm tired and so he pretended to be like the Buddha where the Buddha would say

34
00:04:36,960 --> 00:04:42,040
you know you teach and the Buddha would lie down and listen mindfully but instead David

35
00:04:42,040 --> 00:04:49,240
that delayed down and fell asleep and so uh sorry put in Mogulana taught the monks the

36
00:04:49,240 --> 00:04:56,320
truth and converted them all and they all went back to see the Buddha they went back

37
00:04:56,320 --> 00:05:02,440
to be with the Buddha once they realized what was right and what was wrong and David

38
00:05:02,440 --> 00:05:09,960
that is sort of second in command kicked David that in the chest to wake him up and was

39
00:05:09,960 --> 00:05:14,600
very angry and said look look at what's happened you I told you to be careful of these

40
00:05:14,600 --> 00:05:20,360
two and you didn't listen but he kicked him in the chest and it wounded him and that ended

41
00:05:20,360 --> 00:05:27,080
up I think being afraid of that was what killed him in the end he got sick after that

42
00:05:27,080 --> 00:05:35,400
coughed up blood but uh so this quote is somewhere during this whole fiasco when they

43
00:05:35,400 --> 00:05:44,360
were that that's just left and so the Buddha kind of to introduce the topic he says it's

44
00:05:44,360 --> 00:05:51,640
appropriate it's from time to time god in a column to talk about one's own failings

45
00:05:51,640 --> 00:06:02,120
not talking about but to but to we keep that to consider it is good that they be considered

46
00:06:03,160 --> 00:06:13,480
reflected upon and it's good to reflect upon the failings of others and you have to take

47
00:06:13,480 --> 00:06:20,200
this with a grain of salt it's um you know it's important not to get obsessed with the faults

48
00:06:20,200 --> 00:06:25,480
of others because the Buddha has said of course not pretty sung we know money not pretty sung

49
00:06:25,480 --> 00:06:31,560
guitar guitar don't know don't worry about when should not become obsessed with other people's

50
00:06:31,560 --> 00:06:38,680
faults but so here he's cautious he says call in a column from time to time it's useful right

51
00:06:38,680 --> 00:06:42,520
because if you look at David that as an example and if you talk about him and think about

52
00:06:42,520 --> 00:06:50,360
you're gonna say mmm that's not how I want to be that's something that leads to stress and suffering

53
00:06:52,040 --> 00:06:56,520
and he says it's good from time to time to think of your own attainments and the attainments

54
00:06:56,520 --> 00:07:02,120
of others but then he gets on to the meat of the Buddha which is actually more interesting

55
00:07:04,120 --> 00:07:10,920
he says well he says David that is going to help right that's what he says

56
00:07:10,920 --> 00:07:12,920
wait where is it

57
00:07:20,520 --> 00:07:28,520
there are these eight by eight let me read the English because because he was over no because

58
00:07:28,520 --> 00:07:33,560
he was overcome and obsessed by eight bad conditions David that is bound for the plain

59
00:07:33,560 --> 00:07:39,160
of misery bound for hell and he would remain there for an eon so the point is to introduce

60
00:07:39,160 --> 00:07:43,640
he's now going to talk about David at this feelings what David at did wrong

61
00:07:45,160 --> 00:07:51,800
and it's interesting because these eight things are a list of things that we have to be

62
00:07:51,800 --> 00:08:01,480
not obsessed with we cannot let overcome us the first is gain some people are obsessed with gain

63
00:08:01,480 --> 00:08:10,760
wanting money wanting possessions wanting to get things and when you get obsessed with that it

64
00:08:10,760 --> 00:08:20,360
overpowers you to inflames the mind overcome by loss when you don't get what you want when

65
00:08:20,360 --> 00:08:31,640
you lose what you what you like fame number three and Spain and people are obsessed with being

66
00:08:31,640 --> 00:08:39,320
famous people who post YouTube videos there they can be obsessed with people's

67
00:08:41,880 --> 00:08:47,320
how many subscribers they have or so we go on Facebook or obsessed by how many likes we get

68
00:08:47,320 --> 00:08:55,400
I kind of think by disrepute the opposite of fame you know obsessed when people have a bad

69
00:08:56,600 --> 00:09:05,160
image of you or when you become infamous or when no one knows who you are

70
00:09:05,160 --> 00:09:14,600
by honor I'm sure what the context here of honor is look at this

71
00:09:18,120 --> 00:09:26,120
sakara right yes that I guess sakara is similar to fame but it's

72
00:09:26,120 --> 00:09:38,680
um when people support up present you with things like respect and gifts and pray a praise

73
00:09:42,520 --> 00:09:47,480
honor yeah that's where the way it gets to an honor sakara means doing rightly

74
00:09:47,480 --> 00:09:54,840
and people are obsessed with other people praising them and honoring them

75
00:10:01,240 --> 00:10:02,760
rewarding them that kind of thing

76
00:10:08,440 --> 00:10:13,880
number seven by evil desires and it's obsessed by evil desires they were that they had the most

77
00:10:13,880 --> 00:10:20,040
evil desires he wanted to kill the Buddha he wanted to become he ended up trying to kill the

78
00:10:20,040 --> 00:10:25,960
Buddha he so wanted to be head of the monks head of the sangha so badly that he did evil things

79
00:10:26,920 --> 00:10:33,800
he was jealous of the Buddha he was covetous of the Buddha's position and kind of

80
00:10:33,800 --> 00:10:44,840
and then buried bad friendship so David that um David that was actually the bad friendship

81
00:10:44,840 --> 00:10:53,000
himself he ended up making friends with king bimissara's son who ended up trying ended up killing his father

82
00:10:53,000 --> 00:11:04,360
when jack dasapu came came um was actually able to kill his father

83
00:11:08,280 --> 00:11:16,280
but association with bad friends means if you're on a bad path it's not a good idea to find people who

84
00:11:16,280 --> 00:11:21,800
have your state your own faults right we say birds of a feather flock together well that's not

85
00:11:21,800 --> 00:11:28,440
always wise if you have faults then maybe it's better to associate with people who don't have

86
00:11:28,440 --> 00:11:35,560
those faults they can teach you something they can remind you they can challenge you right

87
00:11:36,520 --> 00:11:43,800
but but we we tend the other way right we tend to flock towards birds with the same faults

88
00:11:43,800 --> 00:11:53,560
we tend to flock towards if we if we are angry sort of person we tend to find solid solace in

89
00:11:53,560 --> 00:11:58,760
other people who are angry we don't seem so out of place for around people who are not angry we

90
00:11:58,760 --> 00:12:04,280
feel guilty all the time if you're bad so it's actually easier to be around angry people which is

91
00:12:04,280 --> 00:12:08,200
of course the most dangerous thing because you're just going to become more of an angry person

92
00:12:08,200 --> 00:12:14,920
greed your greedy person shouldn't be around other addicts or other greedy people

93
00:12:15,960 --> 00:12:19,880
if you're deluded if you're full of arrogance and conceit and so on

94
00:12:23,240 --> 00:12:27,400
they should be surrounded by humble people you should go to seek out humble people to teach you

95
00:12:27,400 --> 00:12:40,040
humility and then he says it is good for a beaker to overcome these ape things whenever they arise

96
00:12:41,800 --> 00:12:45,000
overcome see what the word he uses for overcome

97
00:12:45,000 --> 00:12:55,480
um we move he um we move yeah in the commentary talks um we move yeah this was an interesting word

98
00:12:57,000 --> 00:13:02,360
to uh against it just means overcome to be above

99
00:13:02,360 --> 00:13:19,320
um commentaries has a big bawithma mudithma yeah to subjugate to conquer one should conquer these

100
00:13:20,200 --> 00:13:25,320
conquer these things these are our enemies they would that there wasn't the enemy

101
00:13:25,320 --> 00:13:33,480
we would have listened to his enemy his enemy was that he's eight things

102
00:13:34,600 --> 00:13:39,640
which is obsession for these eight things I should say because of course gain is not a problem

103
00:13:39,640 --> 00:13:45,880
loss is not a problem it's not these things that are the problem except for evil wishes

104
00:13:45,880 --> 00:13:51,320
but the real problem is our obsession and this goes actually with even our emotions if you're an

105
00:13:51,320 --> 00:13:57,240
angry person that's not the biggest problem the biggest problem is your obsession with it when you

106
00:13:57,240 --> 00:14:05,080
let it consume you when you have desire this not the biggest problem biggest problem is you don't

107
00:14:05,720 --> 00:14:10,440
if you fail to address it fail to rise above it to overcome

108
00:14:10,440 --> 00:14:22,040
and you have drowsiness when you have distraction when you have doubt you have doubt about

109
00:14:23,240 --> 00:14:28,600
about his teaching or the practice and doubt isn't the biggest problem that you let it get to

110
00:14:30,360 --> 00:14:36,440
even these things they're they're bad but they're they really become bad when you follow them

111
00:14:36,440 --> 00:14:43,640
when you chase them when you make much of them and so with these eight things it's really the

112
00:14:44,440 --> 00:14:48,920
deep session but they were that there wasn't able to see them clearly

113
00:14:53,080 --> 00:14:58,840
the smart he had no and then he says why why should you not let them consume you and he says

114
00:14:58,840 --> 00:15:10,040
because there will be a we got a very la which is a fever burning

115
00:15:13,960 --> 00:15:19,400
those taints distressful and feverish that might arise in one who has not overcome

116
00:15:20,840 --> 00:15:25,800
gain and so on do not occur in one who has overcome it so you don't your mind doesn't become inflamed

117
00:15:25,800 --> 00:15:32,280
distressed stressed fatigued overwhelmed

118
00:15:35,400 --> 00:15:47,320
the Ahsawa is outpouring the emotions and the defilement the stresses

119
00:15:47,320 --> 00:15:57,240
they don't arise when you overcome them then you come to see them for what they are and discard

120
00:15:57,240 --> 00:16:01,080
them as an unproductive

121
00:16:04,760 --> 00:16:08,840
will I breathe in you should train yourself that smart you have victory

122
00:16:08,840 --> 00:16:16,360
they won't seek it to bang it should be trained us we will dwell having overcome

123
00:16:16,360 --> 00:16:25,480
and be boo yeah be boo yeah we have his sound we will dwell having overcome gain and loss and

124
00:16:26,360 --> 00:16:30,680
pain and infamy honor and dishonor

125
00:16:30,680 --> 00:16:39,640
the evil desires can evil friends

126
00:16:43,640 --> 00:16:47,160
evil friendship

127
00:16:50,360 --> 00:16:57,400
so good list good list it's almost the eight Loki a diamond that the last two would be happiness

128
00:16:57,400 --> 00:17:05,560
and suffering look and so good instead of pop it shut up pop them pop them it up

129
00:17:12,200 --> 00:17:18,200
anyway there's our dumb of her tonight don't be like them at that

130
00:17:20,200 --> 00:17:22,120
don't let these things over on you

131
00:17:22,120 --> 00:17:25,640
and any questions

132
00:17:25,640 --> 00:17:51,400
so you can you stay for a second you can go

133
00:17:51,400 --> 00:17:57,160
okay so we got a couple of questions first I want to say here's might have been

134
00:17:57,160 --> 00:18:03,720
have you met at these people have I showed you this is Michael

135
00:18:07,960 --> 00:18:15,240
Michael's living here now he's my when he's the steward here for for a while

136
00:18:15,240 --> 00:18:22,120
just finished his advanced course so he's done both courses so we'll have to do an interview

137
00:18:22,120 --> 00:18:27,640
with Michael to see what he thinks of the practice see how it helped him if it helped him

138
00:18:30,600 --> 00:18:36,360
and Sunday we're going to miss a saga together and then

139
00:18:36,360 --> 00:18:47,720
I have to go to New York we're going to drive to New York you will drive to New York

140
00:18:49,160 --> 00:18:54,760
would you be able to drive to New York how is it about crossing the border back and forth

141
00:18:54,760 --> 00:19:01,560
there's no problem isn't it what did they say when you came in did they ask you or make

142
00:19:01,560 --> 00:19:09,400
they didn't make trouble for you they don't they they didn't make trouble but now or I mean

143
00:19:09,400 --> 00:19:16,280
you know I mentioned your day even and then I was staying in a lot of scary you know many

144
00:19:16,280 --> 00:19:22,760
questions about that like suspicious or just curious a little bit oh we should talk about it

145
00:19:23,800 --> 00:19:29,560
there probably should give people letters in the future letters of recommendation and like letters

146
00:19:29,560 --> 00:19:37,640
of invitation oh weird yeah they did ask for that yeah yeah I think we've we've had this issue

147
00:19:37,640 --> 00:19:43,720
before I just sent a letter of invitation to someone in Ukraine wants to come and meditate so hopefully

148
00:19:43,720 --> 00:19:50,520
that works right so there's this monk I've been talking about he wants to go visit Beku Bodhi so he

149
00:19:50,520 --> 00:19:54,600
wants me to go with him and he was going to drive there and I said well I've got a driver

150
00:19:54,600 --> 00:20:08,360
we got a Prius right it's good good mileage so probably he'll pay for gas but yeah so that's

151
00:20:08,360 --> 00:20:16,840
that's a week and a half away the 17th Sunday we're going to Mississauga

152
00:20:16,840 --> 00:20:30,280
for way suck we suck okay so questions Larry says during sitting meditation can more than

153
00:20:30,280 --> 00:20:40,840
one hindrance be recognized concurrently yeah you won't have them both at once you can't have a

154
00:20:40,840 --> 00:20:46,760
version end craving concurrently I mean it feels like that because over time you say wow I was

155
00:20:46,760 --> 00:20:52,920
both angry and greedy but you can't have them both at the exact same moment so concurrently

156
00:20:52,920 --> 00:20:57,720
and then broad sense sure but in the absolute sense no in the absolute sense there's one

157
00:20:57,720 --> 00:21:02,040
at a time so whichever one is clear in that moment just focus on it

158
00:21:02,040 --> 00:21:14,840
doesn't really matter I wouldn't worry about which is which just whichever one is clear

159
00:21:14,840 --> 00:21:21,000
and in whatever moment focus on it you don't have to catch them all but again you can't have

160
00:21:21,000 --> 00:21:27,320
all of them at once except for the fact that a version to something is kind of like desire for it

161
00:21:27,320 --> 00:21:32,840
not to be right so you can argue that it's actually in that same moment it's the same thing

162
00:21:32,840 --> 00:21:39,400
you're saying the same thing you just like something means you wanted to be gone so it's not

163
00:21:39,400 --> 00:21:46,520
quite craving or it is some people call that we bow at them desire for non-existence

164
00:21:46,520 --> 00:21:58,120
some day sankara dukha does that hold true only for an enlightened beings no no it is not

165
00:21:58,120 --> 00:22:19,240
they are dukha dukha is there a characteristic means they are unsatisfying it it means they are

166
00:22:19,240 --> 00:22:26,680
not good it's the intrinsic characteristic it doesn't mean that they are painful or they

167
00:22:26,680 --> 00:22:34,360
they when they arise cause one mental suffering or even physical suffering that's not what

168
00:22:34,360 --> 00:22:39,720
dukha means you're dukha means unsatisfying or unable to satisfy

169
00:22:44,680 --> 00:22:50,040
unable to bring happiness it's not happiness remember these three characteristics are in opposition

170
00:22:50,040 --> 00:22:57,320
of what we think things are we think things are stable we think things are satisfying or

171
00:22:57,320 --> 00:23:05,560
able to satisfy us or productive of true happiness and we think things are ours or me or mine

172
00:23:06,920 --> 00:23:07,960
we can control them

173
00:23:13,560 --> 00:23:15,640
so this is just realizing that they're not that

174
00:23:15,640 --> 00:23:21,880
that they're and not in each and that they're not subka that they're not at that's what these three

175
00:23:21,880 --> 00:23:36,680
things mean they're actually a need just dukha and that can you go back to using the red room

176
00:23:36,680 --> 00:23:41,720
wow there's someone who acts to like that red no I can't go back to using the red room

177
00:23:41,720 --> 00:23:45,800
which is disliking disliking

178
00:23:49,640 --> 00:24:01,560
sadhu means good sadhu just means good

179
00:24:01,560 --> 00:24:15,080
well sadhu can in Hinduism sadhu is a or an India sadhu means a a it's a word also for a holy

180
00:24:15,080 --> 00:24:22,680
person like a monk or a recliff so they call us sadhus they would call me a sadhu but it

181
00:24:22,680 --> 00:24:28,360
literally means at least in poly I don't know in Sanskrit but in poly it's just used to mean

182
00:24:28,360 --> 00:24:32,360
good sadhu it's an exclamation it means it is good

183
00:24:37,480 --> 00:24:42,600
when something is good you say sadhu that thing is sadhu and poly would say

184
00:24:44,440 --> 00:24:52,520
tongue sadhu it is good you even say like that but then like sadhu it was used in the

185
00:24:52,520 --> 00:25:00,440
sutra right sadhu bikhui bikhu pannang la bang abhibuya abhibuya we hurry it is good

186
00:25:01,240 --> 00:25:04,280
if or it would be good if one were to do how I can

187
00:25:11,800 --> 00:25:18,200
but isn't suffering only mental no there's two kinds of suffering there's physical suffering

188
00:25:18,200 --> 00:25:23,320
mental suffering but that's again not what this means I don't know I think there's a lag so I'm not

189
00:25:23,320 --> 00:25:30,280
sure if you if that was after my explanation or before it but again that's not what dukha means

190
00:25:30,280 --> 00:25:49,240
here dukha means not happiness we're not conducive to happiness we're not productive with happiness

191
00:25:49,240 --> 00:25:55,720
where is robin robin's here I have just um I was doing video questions I was forcing people

192
00:25:55,720 --> 00:26:08,120
trying to get people to come on the hangout so didn't have any didn't have any need for someone to

193
00:26:08,120 --> 00:26:14,920
read I think there's a way at some time and then we just stopped everything changes this is what

194
00:26:14,920 --> 00:26:22,200
you have to realize don't get attached to things as they are try to change the format around

195
00:26:22,200 --> 00:26:34,520
to shake things up anyway yes that's all for tonight thank you all for tuning in

196
00:26:35,800 --> 00:26:40,680
tomorrow oh another thing is tomorrow at the university there's it's called May at Mac

197
00:26:41,960 --> 00:26:47,000
May at Mac it's the yearly McMaster open house and the peace studies department

198
00:26:47,000 --> 00:26:58,040
has has a booth and they've asked they've asked some of us to go and talk to new students about

199
00:26:58,040 --> 00:27:05,560
the peace studies department I'll take it as an opportunity to get people to sign up for the

200
00:27:06,200 --> 00:27:12,200
peace club as well but it'd be interesting or go as Buddhist monk to go and promote the peace

201
00:27:12,200 --> 00:27:20,280
studies department I think that'll be good I mean I do believe in it as not sure how obviously I

202
00:27:20,280 --> 00:27:26,760
don't studies is not my these kind of studies is not my priority but I want to encourage them and

203
00:27:26,760 --> 00:27:32,440
support I mean anyone who's talking about peace studying peace is just all good in my book

204
00:27:32,440 --> 00:27:48,360
so I'm happy to promote it anyway we're good day to everyone

