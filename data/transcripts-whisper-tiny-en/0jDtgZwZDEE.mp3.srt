1
00:00:00,000 --> 00:00:11,400
Okay, well everyone, welcome back to our question and answer series.

2
00:00:11,400 --> 00:00:18,800
Today's question is in regards to peaceful meditation practice.

3
00:00:18,800 --> 00:00:23,600
A restful was the word that was used to the question.

4
00:00:23,600 --> 00:00:39,200
So this person describes the state of practice that they're going through that is effortful.

5
00:00:39,200 --> 00:00:43,400
Takes a lot of effort for them to be mindful.

6
00:00:43,400 --> 00:00:53,400
They'd like to be mindful throughout the day, but it's quite tiresome being mindful of being mindful of the difficult things.

7
00:00:53,400 --> 00:01:06,200
And they look at these meditation masters whose demeanor appears quite peaceful.

8
00:01:06,200 --> 00:01:15,600
And it appears that they're able to stay mindful quite easily.

9
00:01:15,600 --> 00:01:20,000
So the question is, how are they able to do that?

10
00:01:20,000 --> 00:01:26,400
How do we know that we're on the right path towards that?

11
00:01:26,400 --> 00:01:34,600
So there's somewhat a sense of discouragement.

12
00:01:34,600 --> 00:01:45,200
That's a word feeling discouraged, I think.

13
00:01:45,200 --> 00:01:57,600
This in some sense relates to the last question about meditation being unpleasant.

14
00:01:57,600 --> 00:02:03,400
And so part of the answer is in regards to that, that's already been answered.

15
00:02:03,400 --> 00:02:16,600
But meditation can be quite difficult and it can be quite stressful. As I've said, it depends a lot on your state of mind

16
00:02:16,600 --> 00:02:28,600
because mindfulness meditation is designed to confront your mind states.

17
00:02:28,600 --> 00:02:41,200
So the first thing to talk about is this idea of peaceful meditation as a choice.

18
00:02:41,200 --> 00:02:56,800
One person chooses to practice meditation peacefully or manufacturers of peaceful meditation as sort of a starting point.

19
00:02:56,800 --> 00:03:08,600
This person, I think, from the sounds of it isn't a advanced meditation practitioner because they're comparing themselves to advanced meditation practitioners.

20
00:03:08,600 --> 00:03:20,600
And so there's this question about how do you practice peacefully, first of all.

21
00:03:20,600 --> 00:03:42,600
And so to some extent, I've always kind of tried to disabuse people of this notion that meditation should perhaps even could be peaceful for them.

22
00:03:42,600 --> 00:03:51,600
Because that can be a problematic sort of outlook to have, especially because it's not always possible.

23
00:03:51,600 --> 00:04:05,600
But to some extent, it is possible. There are ways by which you can enter into meditation practice and have it be smooth sailing.

24
00:04:05,600 --> 00:04:28,600
I think the most extreme method to achieve this would be to put aside meditation practice and spend lifetime after lifetime cultivating wholesome qualities, cultivating perhaps the ten perfections of the Buddha.

25
00:04:28,600 --> 00:04:45,600
Maybe not complete perfection, but recognizing that meditation is going to be quite stressful for me and so in order to make it easier, I would like to do all the preparatory work in advance, make myself a better person.

26
00:04:45,600 --> 00:04:50,600
Because I don't want to confront the crappy person that I am.

27
00:04:50,600 --> 00:04:54,600
So let's make myself a better person first. That's certainly possible.

28
00:04:54,600 --> 00:05:11,600
And it's recognized in Buddhism. It's recognized that for some people it's not that they're crappy people, it's that their hearts aren't really into a meditative lifestyle and so there's an encouragement for them to cultivate goodness.

29
00:05:11,600 --> 00:05:27,600
And so there's a twofold benefit of cultivating goodness. One, it makes meditation easier when you do decide to practice meditation if you're a better person, a nicer person, a kinder person.

30
00:05:27,600 --> 00:05:34,600
But also it makes you more inclined to practice meditation if you're a better person, a nicer person, a kinder person.

31
00:05:34,600 --> 00:05:47,600
I mean, it's often said that modern day people are quite selfish and spiritual modern day spiritual practitioners can often be quite selfish.

32
00:05:47,600 --> 00:05:56,600
I think we see this with some of the emphasis of compassion and loving kindness.

33
00:05:56,600 --> 00:06:05,600
It's not really in the ancient texts, it's this sense of love for yourself and kindness towards yourself.

34
00:06:05,600 --> 00:06:11,600
There's a sense that we need ourselves and so we should try to be kind to ourselves.

35
00:06:11,600 --> 00:06:27,600
And it cuts both ways because that's not what kindness and compassion are really for. They're meant to make you a nicer person, a kinder person towards others, a better person.

36
00:06:27,600 --> 00:06:41,600
And I think to some extent we get misled by this whole spiritual movement of thinking that we're special, we're perfect, you're great, just the way you are. That's not really the truth, we're broken.

37
00:06:41,600 --> 00:06:44,600
We're all crap, really.

38
00:06:44,600 --> 00:06:48,600
Just the fact that we were born as human beings is a sign that we're not perfect.

39
00:06:48,600 --> 00:06:59,600
We've still got craving as sort of a baseline. Our desire for the human existence has led us to be born as humans.

40
00:06:59,600 --> 00:07:13,600
And so much of our life is surrounds this sort of baseline desire for procreation, for romance, for sensual desire, and so on.

41
00:07:13,600 --> 00:07:20,600
So we bring that into spiritual practice while craving for pleasant spiritual states.

42
00:07:20,600 --> 00:07:26,600
And the problem is that's not in any way what leads to peaceful or even pleasant states.

43
00:07:26,600 --> 00:07:34,600
Happiness doesn't lead to happiness. It's goodness that leads to happiness and the same goes with peace.

44
00:07:34,600 --> 00:07:49,600
So goodness is something first and foremost to keep in mind. If your practice is difficult before everything else, goodness is going to play a preliminary role in that.

45
00:07:49,600 --> 00:07:58,600
Are you a kind person? Do others? Are you compassionate towards others? Or are you selfish? Are you clingy?

46
00:07:58,600 --> 00:08:09,600
Are you mean-hearted, mean-spirited? Do you have a lot of negative and unpleasant states of mind?

47
00:08:09,600 --> 00:08:27,600
So those sorts of things, it's kind of like, you know, we come to meditation thinking probably as you could point to as another problem.

48
00:08:27,600 --> 00:08:33,600
Is this idea of meditation as just another fix?

49
00:08:33,600 --> 00:08:40,600
Like these pills that we take. If you have anxiety or depression, we take pills as a cure.

50
00:08:40,600 --> 00:08:47,600
I take these pills, it's going to fix me. The pill is very special and it's good.

51
00:08:47,600 --> 00:08:51,600
And so we look at meditation like that. Well, here's this meditation.

52
00:08:51,600 --> 00:09:00,600
I was thinking of it kind of like, our expectation is, we practice meditation, we're buying a sports car.

53
00:09:00,600 --> 00:09:07,600
A meditation, I think, should be likened to a sports car in a way because it's not just an ordinary vehicle.

54
00:09:07,600 --> 00:09:12,600
Mindfulness meditation, we praise it as being some very special vehicle.

55
00:09:12,600 --> 00:09:16,600
So we're talking about a high-quality vehicle here, for sure.

56
00:09:16,600 --> 00:09:23,600
And when it's described as that, and we hear it described as this special thing, sort of a real fix, a real cure.

57
00:09:23,600 --> 00:09:27,600
The expect will just get in it and drive away and it'll be smooth sailing.

58
00:09:27,600 --> 00:09:35,600
When we get in and it feels more like we've bought a lemon and we've gotten in a crappy rundown vehicle.

59
00:09:35,600 --> 00:09:46,600
So it's probably better to describe this as us envisioning a sports car.

60
00:09:46,600 --> 00:09:49,600
And we see these other people driving these very special vehicles.

61
00:09:49,600 --> 00:09:54,600
This is the advanced meditators with their peaceful meditation practices.

62
00:09:54,600 --> 00:09:56,600
And we think, wow, I'll start doing that.

63
00:09:56,600 --> 00:10:03,600
And then we cobbled together all these parts that we have lying around.

64
00:10:03,600 --> 00:10:09,600
And we realized, oh, there's more to a sports car than just having four wheels in a frame.

65
00:10:09,600 --> 00:10:13,600
And we cobbled together this crappy vehicle.

66
00:10:13,600 --> 00:10:16,600
And that's where we start.

67
00:10:16,600 --> 00:10:19,600
And so you can fit all of the other stuff in here.

68
00:10:19,600 --> 00:10:32,600
All the good things that we've done in the past are like all the amenities, all the features, the optional features that you can get with your vehicle and all the comforts.

69
00:10:32,600 --> 00:10:35,600
It's surrounding it.

70
00:10:35,600 --> 00:10:46,600
But when we start off, we're starting off with a low quality vehicle.

71
00:10:46,600 --> 00:10:52,600
It's kind of like shooting an arrow through a bow.

72
00:10:52,600 --> 00:11:05,600
And if you have a perfect arrow and a perfect bow, even a beginner could shoot the arrow quite straight.

73
00:11:05,600 --> 00:11:07,600
But our mind is like this arrow.

74
00:11:07,600 --> 00:11:15,600
And if our mind is not straight, it's all crooked, you have to spend time straight.

75
00:11:15,600 --> 00:11:22,600
And so the first part then is about these things we can do.

76
00:11:22,600 --> 00:11:24,600
And sorry, there's just one more thing to talk about.

77
00:11:24,600 --> 00:11:33,600
And that's the practice of meditations that are designed to calm you down, that are designed to create peaceful states.

78
00:11:33,600 --> 00:11:39,600
And that's another potentially useful practice.

79
00:11:39,600 --> 00:11:48,600
Many people spend years practicing meditation just to calm them down as sort of a baseline.

80
00:11:48,600 --> 00:11:54,600
And it changes their baseline for when they do come and practice insight meditation.

81
00:11:54,600 --> 00:11:56,600
That's highly respected.

82
00:11:56,600 --> 00:11:58,600
I don't teach that.

83
00:11:58,600 --> 00:12:08,600
But it's a highly respected practice where you engage in intense states of trance and concentration before you ever think to practice insight meditation.

84
00:12:08,600 --> 00:12:19,600
Now what we practice is considered to be a little more feasible for the average person, especially in modern times.

85
00:12:19,600 --> 00:12:26,600
We're going off in the forest and practicing intense tranquility meditation.

86
00:12:26,600 --> 00:12:28,600
It's not really feasible.

87
00:12:28,600 --> 00:12:33,600
So we instead try to cultivate them together, tranquility and insight.

88
00:12:33,600 --> 00:12:46,600
At the same time, and hence the general malaise that goes along with that and people are complaining about it being not very peaceful.

89
00:12:46,600 --> 00:12:54,600
Because you're going to jumping right in with a crappy vehicle and you haven't done all the time required to tweak it or your arrow is not straight.

90
00:12:54,600 --> 00:12:58,600
And so you have to do a lot of straightening.

91
00:12:58,600 --> 00:13:03,600
And so that comes to the second part of what I wanted to talk about.

92
00:13:03,600 --> 00:13:18,600
And that's the actual practice, practicing with a substandard vehicle, the mind that is not yet restful.

93
00:13:18,600 --> 00:13:30,600
So to some extent, there's no avoiding this, you know, people who have beautiful minds who are by nature through lifetime after lifetime generally.

94
00:13:30,600 --> 00:13:33,600
I'm doing good deeds, often I have an easier time.

95
00:13:33,600 --> 00:13:38,600
But it doesn't mean they don't see the stuff inside that's problematic.

96
00:13:38,600 --> 00:13:45,600
And it's not to say that it's all smooth sailing.

97
00:13:45,600 --> 00:14:03,600
For the majority of people, this is going to be at least half the time where their practice is going to be stressful is going to be unpleasant.

98
00:14:03,600 --> 00:14:32,600
And so I've talked about that before about how we have to change our expectations and we have to learn to change from thinking that something's wrong with our practice when it's unpleasant to understand that that's a part of learning the learning process that the unpleasantness has causes.

99
00:14:32,600 --> 00:14:52,600
And by observing those unpleasantness and their causes and by observing the things that we react to more clearly that will change the response away from reacting.

100
00:14:52,600 --> 00:15:09,600
But there was one other thing that I wanted to say and that was just a realization that in the beginning a lot of our practice is not that we've gotten into a sports car and we just have to drive in the right direction.

101
00:15:09,600 --> 00:15:13,600
It really is that we're learning how to meditate.

102
00:15:13,600 --> 00:15:19,600
Preliminary meditation practice is described as learning how to meditate.

103
00:15:19,600 --> 00:15:33,600
And so let that sink in that for the first period of your practice and to some extent to some extent you could argue the whole of your practice is about learning how to practice.

104
00:15:33,600 --> 00:15:52,600
And it should never be seen as a smooth ride that you can just now coast. But it's always going to be adjusting. The process of enlightenment will and should surprise you every step of the way.

105
00:15:52,600 --> 00:16:08,600
In the sense that it's teaching you new things or new things about things. New things about new things in fact.

106
00:16:08,600 --> 00:16:28,600
It's teaching you what you're doing wrong in your practice to see what you're doing wrong. It's level after level of change and it's going to be constantly changing you fundamentally.

107
00:16:28,600 --> 00:16:40,600
So this analogy of constantly not only shooting the arrow through the bow but also constantly refining straightening the arrow.

108
00:16:40,600 --> 00:16:50,600
I mean straightening the arrow is the arrow being the mind and the fact that you're using the mind to learn about the mind you see.

109
00:16:50,600 --> 00:17:02,600
You're using your mind to fix the broken mind and then you say well that means you're using something that's broken to fix something that's broken you see.

110
00:17:02,600 --> 00:17:11,600
So in some ways it sounds actually impossible. It sounds like a hard and we have a trap here for Buddhists that they're doing something that's not possible.

111
00:17:11,600 --> 00:17:22,600
So I leave it up to you to investigate to see whether it is possible. But I wouldn't, I think that's just a silly logical paradox.

112
00:17:22,600 --> 00:17:30,600
If you're intellectual I see you can come up with that. But it's not really about using the mind of a broken thing to fix a broken thing.

113
00:17:30,600 --> 00:17:50,600
It's about that broken thing healing itself. It's about recognizing that you yourself are broken. It's a bit less intellectual than that. It's just the process of healing in some sense.

114
00:17:50,600 --> 00:18:03,600
But it really is healing the self healing. You're not using a perfect instrument to heal something that's broken. It's not like that.

115
00:18:03,600 --> 00:18:13,600
And so our practice is going to in the beginning very much be about simply learning how to practice and realizing that we're doing things wrong.

116
00:18:13,600 --> 00:18:22,600
And that's most important to allow us to avoid repeatedly making the same mistakes.

117
00:18:22,600 --> 00:18:29,600
In the beginning we get discouraged because it feels like our practice doing the way they told me to do it and it's just not working.

118
00:18:29,600 --> 00:18:36,600
And so we should rest assured that in the beginning we're doing it all wrong. And we're only learning how to do it.

119
00:18:36,600 --> 00:18:43,600
Like if I tell you to watch your stomach and say rising, falling, it's a very simple thing to tell you to do.

120
00:18:43,600 --> 00:18:52,600
But the quality of mind when you do that is going to be full of all sorts of preconceptions and delusions and it's going to be very forceful mostly.

121
00:18:52,600 --> 00:18:56,600
We're so used to when we apply our mind to something we want to control it.

122
00:18:56,600 --> 00:19:01,600
So when I tell you just to watch the stomach the mind isn't going to do that. It's going to be trying to control it.

123
00:19:01,600 --> 00:19:09,600
Instead of just watching, rising, falling, you're going to be trying to force it and you'll see that.

124
00:19:09,600 --> 00:19:16,600
And then you'll feel like after the question comes out, how do I stop myself from forcing?

125
00:19:16,600 --> 00:19:21,600
And there's no direct answer to that because that's the doing it wrong.

126
00:19:21,600 --> 00:19:26,600
The learning about how you do things wrong. And it's a very important part of it.

127
00:19:26,600 --> 00:19:32,600
Because meditation is not separate from our life.

128
00:19:32,600 --> 00:19:38,600
So when you ask this question, how does my meditation become peaceful?

129
00:19:38,600 --> 00:19:43,600
Will it be a peaceful person? Why is it that these advanced meditators can practice so peacefully?

130
00:19:43,600 --> 00:19:53,600
Well, that's the whole point. If your meditation is stressful, it is hard work.

131
00:19:53,600 --> 00:20:00,600
It means that your mind is suboptimal. Your mind is messed up.

132
00:20:00,600 --> 00:20:05,600
And great because that's why you're meditating.

133
00:20:05,600 --> 00:20:09,600
There should be no surprise that our meditation is difficult.

134
00:20:09,600 --> 00:20:15,600
It's about dealing with our mind. If your mind was perfect, you wouldn't need to meditate.

135
00:20:15,600 --> 00:20:20,600
If meditation were restful, it would be a sign that you're already at that end.

136
00:20:20,600 --> 00:20:27,600
It would be a sign that you're already enlightened. As I said, there are ways to make it more comfortable

137
00:20:27,600 --> 00:20:34,600
by doing a lot of the work in advance, by unscrewing up your mind, making your mind less messed up.

138
00:20:34,600 --> 00:20:38,600
In advance, in other more mundane ways.

139
00:20:38,600 --> 00:20:43,600
All of those ways will be and should be kept in mind during your practice.

140
00:20:43,600 --> 00:20:49,600
If you want to make the process of practice easier, you should also be generous.

141
00:20:49,600 --> 00:20:55,600
You should also be kind. You should aim to be ethical.

142
00:20:55,600 --> 00:21:00,600
Ethics is what I didn't mention. You should stop killing and stealing and lying and cheating.

143
00:21:00,600 --> 00:21:11,600
Taking drugs and alcohol, you should give up entertainment as best you can.

144
00:21:11,600 --> 00:21:18,600
You should try to eat less, sleep less, talk less.

145
00:21:18,600 --> 00:21:22,600
Facebook less.

146
00:21:22,600 --> 00:21:26,600
This is why doing meditation courses is great.

147
00:21:26,600 --> 00:21:32,600
When you go off and do a meditation course, you are forced into a situation

148
00:21:32,600 --> 00:21:39,600
where you're not able to engage in so many of these distracting and unwholesome.

149
00:21:39,600 --> 00:21:45,600
States and activities that are going to get in the way of your practice.

150
00:21:45,600 --> 00:21:49,600
The last part of the question, the real question that was asked,

151
00:21:49,600 --> 00:21:55,600
is how then do we become like these people who are advanced meditators

152
00:21:55,600 --> 00:22:00,600
and who have gotten to the point where their practice is peaceful,

153
00:22:00,600 --> 00:22:04,600
and how do we know that we're on the right path?

154
00:22:04,600 --> 00:22:09,600
The piece of someone who's practiced for a long time

155
00:22:09,600 --> 00:22:13,600
is different from the piece of someone who's, for example, just a good person,

156
00:22:13,600 --> 00:22:15,600
but never meditated.

157
00:22:15,600 --> 00:22:19,600
The piece of someone who's practiced a long time is deeper

158
00:22:19,600 --> 00:22:23,600
and it's more and more comprehensive.

159
00:22:23,600 --> 00:22:32,600
It comes from seeing all things with equanimity.

160
00:22:32,600 --> 00:22:39,600
It comes from the state of observing

161
00:22:39,600 --> 00:22:45,600
or the quality of seeing things just as they are.

162
00:22:45,600 --> 00:22:48,600
So what we're seeing is just seeing.

163
00:22:48,600 --> 00:22:50,600
Hearing is just hearing.

164
00:22:50,600 --> 00:22:53,600
Painful experiences are just painful experiences.

165
00:22:53,600 --> 00:22:58,600
And what that means is there's no reaction to them.

166
00:22:58,600 --> 00:23:02,600
Through our practice of mindfulness,

167
00:23:02,600 --> 00:23:05,600
when we note to ourselves for example seeing,

168
00:23:05,600 --> 00:23:10,600
when we say pain, pain, we're teaching ourselves

169
00:23:10,600 --> 00:23:12,600
what's happening.

170
00:23:12,600 --> 00:23:17,600
We're reminding ourselves of the reality of the experience.

171
00:23:17,600 --> 00:23:20,600
Our experience is made up of the reality of it

172
00:23:20,600 --> 00:23:23,600
and then all of our baggage.

173
00:23:23,600 --> 00:23:28,600
This is good, this is bad, this is me, this is mine.

174
00:23:28,600 --> 00:23:31,600
I like this, I don't like this, I agree with this,

175
00:23:31,600 --> 00:23:34,600
I disagree with this, all of the baggage.

176
00:23:34,600 --> 00:23:37,600
Which is not a part of the reality.

177
00:23:37,600 --> 00:23:41,600
Seeing is just seeing, that's the only part of it that's real.

178
00:23:41,600 --> 00:23:45,600
Even when you say I see a cat, the cat isn't part of the seeing.

179
00:23:45,600 --> 00:23:50,600
Cat is a concept in our mind related to having seen other things

180
00:23:50,600 --> 00:23:55,600
in the past that are a similar shape and so on.

181
00:23:55,600 --> 00:23:57,600
Makes similar sounds like that.

182
00:23:57,600 --> 00:23:59,600
Oh, it must be a cat.

183
00:23:59,600 --> 00:24:06,600
And so when we're mindful in this way,

184
00:24:06,600 --> 00:24:11,600
we start to see that all of the baggage

185
00:24:11,600 --> 00:24:17,600
that we carry around especially our reactions are unwarranted.

186
00:24:17,600 --> 00:24:22,600
We start to see on a more general level that the things we cling to

187
00:24:22,600 --> 00:24:27,600
and strive after are not worth clinging to our striving after.

188
00:24:27,600 --> 00:24:30,600
They're not stable in the way that we thought they were going to be.

189
00:24:30,600 --> 00:24:33,600
They're not satisfying in the way that we thought they were.

190
00:24:33,600 --> 00:24:38,600
They're not controllable in the way that we thought they were.

191
00:24:38,600 --> 00:24:45,600
They have no sense of being me or mine or for I.

192
00:24:45,600 --> 00:24:48,600
Seeing is really just seeing.

193
00:24:48,600 --> 00:24:53,600
And everything else about it just goes out the window.

194
00:24:53,600 --> 00:24:59,600
As you cultivate this, the description of the practice is not so many steps

195
00:24:59,600 --> 00:25:02,600
are so complicated.

196
00:25:02,600 --> 00:25:08,600
This is why insight is not some thesis that you have to write pages and pages about.

197
00:25:08,600 --> 00:25:13,600
It simply means seeing impermanence, suffering, and not self.

198
00:25:13,600 --> 00:25:15,600
It's a very simple thing.

199
00:25:15,600 --> 00:25:21,600
Those three qualities are sum up what they are not.

200
00:25:21,600 --> 00:25:26,600
All of this baggage that is unwarranted.

201
00:25:26,600 --> 00:25:32,600
This seeking stability, the seeking satisfaction,

202
00:25:32,600 --> 00:25:41,600
seeking self and control and possessiveness and so on.

203
00:25:41,600 --> 00:25:45,600
And as you see this more clearly, it just starts to be to resonate with you.

204
00:25:45,600 --> 00:25:51,600
This is the straightening of the arrow or this is the upgrading of the vehicle

205
00:25:51,600 --> 00:25:56,600
until eventually it does feel like you're in very much a sports car.

206
00:25:56,600 --> 00:26:03,600
And it is more clearly because whatever comes up,

207
00:26:03,600 --> 00:26:09,600
the mind sees it just for what it is and doesn't give rise to all the reactions and all the stress.

208
00:26:09,600 --> 00:26:18,600
Mindfulness should not be and is not by its own, by its simple nature, difficult.

209
00:26:18,600 --> 00:26:27,600
All of the difficulty comes from the qualities of our vehicle.

210
00:26:27,600 --> 00:26:32,600
We haven't changed the oil.

211
00:26:32,600 --> 00:26:39,600
We are using our shocks, our import condition.

212
00:26:39,600 --> 00:26:42,600
We don't have air conditioning and so on and so on.

213
00:26:42,600 --> 00:26:47,600
The tires are thread there.

214
00:26:47,600 --> 00:26:53,600
All of the aspects of our mind that are conspiring against us.

215
00:26:53,600 --> 00:26:58,600
And much of that it can be improved through meditation practice.

216
00:26:58,600 --> 00:27:07,600
The meditation is learning out of meditation very much in the beginning and to some extent all throughout our practice.

217
00:27:07,600 --> 00:27:14,600
The final state of meditation before enlightenment is called Anuloma.

218
00:27:14,600 --> 00:27:18,600
I've used this word before. Anuloma is the grain of wood.

219
00:27:18,600 --> 00:27:21,600
Wood has a grain to it. If you've ever cut wood, you know this.

220
00:27:21,600 --> 00:27:26,600
You can't cut firewood sideways because it has a grain.

221
00:27:26,600 --> 00:27:32,600
But if you cut the firewood down the top, it will split in half, which was a grain to the wood.

222
00:27:32,600 --> 00:27:36,600
Anul means, according in line with a following.

223
00:27:36,600 --> 00:27:39,600
Following the grain, going with the grain.

224
00:27:39,600 --> 00:27:42,600
And so the grain here is the grain of reality.

225
00:27:42,600 --> 00:27:46,600
Reality is that everything that arises ceases.

226
00:27:46,600 --> 00:27:55,600
It's the reality of impermanence, uncertainty, it's the reality of dissatisfaction or inability to satisfy suffering.

227
00:27:55,600 --> 00:28:05,600
That the clinging to anything and chasing after anything is stressful is unwarranted.

228
00:28:05,600 --> 00:28:12,600
And non-salve that everything is just what it is. There's nothing more to it.

229
00:28:12,600 --> 00:28:16,600
There's no entity, no self, there's one.

230
00:28:16,600 --> 00:28:19,600
It's when you finally see that.

231
00:28:19,600 --> 00:28:25,600
And it's surrounded by really a practice.

232
00:28:25,600 --> 00:28:30,600
What leads up to that moment is a practice that is so finely tuned

233
00:28:30,600 --> 00:28:34,600
that you're seeing everything with equanimity, as I said.

234
00:28:34,600 --> 00:28:39,600
And an advanced meditator is able to get to that state.

235
00:28:39,600 --> 00:28:46,600
And through the kind of like a feedback loop, it just resonates more and more.

236
00:28:46,600 --> 00:28:50,600
And you build up this stronger and stronger focus.

237
00:28:50,600 --> 00:28:57,600
That's really a natural focus. It's no longer effortful.

238
00:28:57,600 --> 00:28:59,600
Because it's nothing to do with intending.

239
00:28:59,600 --> 00:29:00,600
It's just seeing.

240
00:29:00,600 --> 00:29:05,600
When you see that everything is impermanent on satisfying and controllable,

241
00:29:05,600 --> 00:29:09,600
you don't cling to anything. You don't strive after anything.

242
00:29:09,600 --> 00:29:15,600
And so by seeing that again and again it gets stronger and stronger until it

243
00:29:15,600 --> 00:29:21,600
sparks, like heating up, rubbing two sticks together to reduce fire.

244
00:29:21,600 --> 00:29:31,600
Once it builds and builds and builds the heat, then there's the moment where the mine lets go.

245
00:29:31,600 --> 00:29:40,600
But the point being that moment is described as the moment when you actually are in line with reality.

246
00:29:40,600 --> 00:29:48,600
It means you're actually meditating. And so everything up to that point was just really learning how to meditate.

247
00:29:48,600 --> 00:29:52,600
And so expecting the meditation to start there is,

248
00:29:52,600 --> 00:29:58,600
to some extent, a misunderstanding of what meditation is and how meditation works.

249
00:29:58,600 --> 00:30:05,600
You only get the sports car, the perfect vehicle at the very last moment

250
00:30:05,600 --> 00:30:14,600
when you're already really good. So it's much less about a linear cruising

251
00:30:14,600 --> 00:30:21,600
as it is to an uphill forming of the vehicle that is the mine.

252
00:30:21,600 --> 00:30:30,600
And that's the observation of people who have already done that.

253
00:30:30,600 --> 00:30:34,600
You see them in that state. It's because of the work that they've done at your level.

254
00:30:34,600 --> 00:30:45,600
At your level, where it was difficult, at the beginner level, where it's stressful and hard to do.

255
00:30:45,600 --> 00:30:52,600
So some advice, I mean I've given now a bunch of advice, but a little more specific advice.

256
00:30:52,600 --> 00:30:58,600
If the meditation is tiring, this is in particular with this person to talk about.

257
00:30:58,600 --> 00:31:04,600
One of the things you want to do is to do is to be mindful of the fact that you're tired.

258
00:31:04,600 --> 00:31:07,600
And you may surprise them while that works sometimes. Often you think,

259
00:31:07,600 --> 00:31:12,600
I just can't meditate anymore except you're tired, then you say tired and it just disappears.

260
00:31:12,600 --> 00:31:22,600
And you gain strength again. But on the other side, a recognition that your practice is really going to be imperfect in the beginning.

261
00:31:22,600 --> 00:31:29,600
And be encouraged by the fact that you've actually done some work.

262
00:31:29,600 --> 00:31:35,600
When it does feel tiring, then that's a sign that you're starting to see how your mind, the state that your mind is in.

263
00:31:35,600 --> 00:31:42,600
You can take a break and come back and try again with the knowledge that eventually you're going to get better at it.

264
00:31:42,600 --> 00:31:48,600
Especially if you keep it up. Most especially if you take some time to do it intensively.

265
00:31:48,600 --> 00:31:55,600
Or if you do it systematically and continuously throughout your life.

266
00:31:55,600 --> 00:32:04,600
And if you accompany it with other good qualities, if you dedicate yourself to being a better person.

267
00:32:04,600 --> 00:32:13,600
Dedicate yourself to ways of being that are conducive to the meditation practice in all sorts of ways.

268
00:32:13,600 --> 00:32:20,600
So hopefully that answers the question. I mean, there's a lot in there that's reduced.

269
00:32:20,600 --> 00:32:26,600
So thank you all for coming out. Again, we're over 70 viewers on the live stream.

270
00:32:26,600 --> 00:32:32,600
If you'd like to ask questions, again, the link should be very soon in the description.

271
00:32:32,600 --> 00:32:40,600
You can go to our meditation site. I don't answer questions on YouTube for some very simple reasons.

272
00:32:40,600 --> 00:32:48,600
But it's free. There's no other reason.

273
00:32:48,600 --> 00:32:53,600
I have no ulterior motive in directing you to our meditation site.

274
00:32:53,600 --> 00:33:00,600
I suppose a minor ulterior motive is to encourage people to actually meditate.

275
00:33:00,600 --> 00:33:05,600
But there's no charge or get put on an email list or anything.

276
00:33:05,600 --> 00:33:08,600
You don't start to get advertising or something.

277
00:33:08,600 --> 00:33:20,600
If you go and sign up there, it's just, it separates the serious people from people who come on to ask,

278
00:33:20,600 --> 00:33:29,600
perhaps, flippant or irreverent questions.

279
00:33:29,600 --> 00:33:39,600
So thank you all for tuning in with your other best. Have a good day.

