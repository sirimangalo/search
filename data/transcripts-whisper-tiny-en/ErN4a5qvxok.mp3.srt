1
00:00:00,000 --> 00:00:03,000
I wish to get enlightened. Where do I start?

2
00:00:03,000 --> 00:00:06,000
I love the good, good simple questions.

3
00:00:06,000 --> 00:00:09,000
Well, start with my booklet on how to meditate.

4
00:00:09,000 --> 00:00:11,000
Look up, utadamo how to meditate.

5
00:00:11,000 --> 00:00:14,000
That's what I recommend to everyone.

6
00:00:14,000 --> 00:00:18,000
Again, it's to plug my own, to toot my own horn.

7
00:00:18,000 --> 00:00:23,000
But to explain that it's not really something that I've taught.

8
00:00:23,000 --> 00:00:26,000
It's just my explanation of what I've been taught.

9
00:00:26,000 --> 00:00:31,000
And since you're asking me, this is the best I can do for you.

10
00:00:31,000 --> 00:00:34,000
If you ask someone else, they'll give you quite a different response.

11
00:00:34,000 --> 00:00:37,000
But my response is what's in that book.

12
00:00:37,000 --> 00:00:43,000
It's the best way to start to become enlightened from my point of view.

13
00:00:43,000 --> 00:00:50,000
Having done that, having read the booklet, then you should make a decision whether you agree or disagree if you don't

14
00:00:50,000 --> 00:00:53,000
or you should put it into practice.

15
00:00:53,000 --> 00:00:57,000
Once you begin to practice the teachings in the booklet,

16
00:00:57,000 --> 00:01:04,000
then if you decide you agree and it seems plausible that this is the way to become enlightened,

17
00:01:04,000 --> 00:01:07,000
then there are other steps that should be taken.

18
00:01:07,000 --> 00:01:31,000
And you shouldn't undertake proper meditation course with proper guidance and go from there.

