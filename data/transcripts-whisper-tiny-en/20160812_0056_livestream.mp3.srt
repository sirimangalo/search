1
00:00:00,000 --> 00:00:11,280
Good evening and welcome to our live broadcast.

2
00:00:11,280 --> 00:00:25,040
Today we are continuing with the Ungutrani Kaya.

3
00:00:25,040 --> 00:00:35,720
I'm just going to go right to the next one because it's not terribly deep but it's interesting

4
00:00:35,720 --> 00:00:46,400
for those wondering about the role the heavens play in Buddhism, so apologies that this

5
00:00:46,400 --> 00:00:48,680
isn't a deep meditation teaching.

6
00:00:48,680 --> 00:00:55,040
I don't have any meditators here right now so we're just going to go on from number

7
00:00:55,040 --> 00:01:02,040
36 to number 37.

8
00:01:02,040 --> 00:01:12,080
Apparently in Buddhism the Buddhist cosmology the Buddha described various levels of angels,

9
00:01:12,080 --> 00:01:18,120
so there are Bhumadhi, which are angels that live around live on earth apparently.

10
00:01:18,120 --> 00:01:26,440
They live in trees or whatever they delight in the fairies, I guess, you normally think

11
00:01:26,440 --> 00:01:29,360
of the med.

12
00:01:29,360 --> 00:01:36,280
And then there are the successively higher planes of angels.

13
00:01:36,280 --> 00:01:42,640
There are the four great kings that if you maybe recognize some of you this from Chinese

14
00:01:42,640 --> 00:01:46,880
mythology, which gets it from Buddhism.

15
00:01:46,880 --> 00:01:52,040
Four great kings that look after the four directions, so apparently there are four angels

16
00:01:52,040 --> 00:02:00,760
who also are involved with the earth but they take care of the four quadrants of the

17
00:02:00,760 --> 00:02:01,760
earth.

18
00:02:01,760 --> 00:02:09,760
Interactions doesn't really work because earth is around so I don't quite understand

19
00:02:09,760 --> 00:02:10,760
it.

20
00:02:10,760 --> 00:02:25,400
There are apparently four great kings and they report to the angels of the tawatings which

21
00:02:25,400 --> 00:02:27,240
is the next level.

22
00:02:27,240 --> 00:02:34,440
And tawatings I guess the highest angel realm that's directly interested in the earth.

23
00:02:34,440 --> 00:02:41,440
tawatings at tawam means three and tings that means thirty, so it's the a, the ebb and of the

24
00:02:41,440 --> 00:02:42,440
thirty-three.

25
00:02:42,440 --> 00:02:51,240
It's called that because there were these thirty-three companions who went to heaven together

26
00:02:51,240 --> 00:02:53,240
and they're led by Sakka.

27
00:02:53,240 --> 00:02:58,920
Sakka is the king of the angels of the thirty-three, and so I guess a group of angels

28
00:02:58,920 --> 00:03:05,920
who live in their own realm but somehow take an interest in the earth.

29
00:03:05,920 --> 00:03:14,120
And so the four great kings report to them, four great kings are responsible for making

30
00:03:14,120 --> 00:03:20,160
sure that everything on earth is, I guess, going according to whatever divine plan that

31
00:03:20,160 --> 00:03:21,160
they have.

32
00:03:21,160 --> 00:03:29,080
Of all this sounds fairly theistic or deistic, well, I suppose you could call it deistic,

33
00:03:29,080 --> 00:03:34,120
although I'm not even sure quite what that word means, but there's, and there is a sense

34
00:03:34,120 --> 00:03:39,360
of that in the time of the Buddha people who are obviously worshiping these angels.

35
00:03:39,360 --> 00:03:51,080
And so the Buddha was acknowledging the existence of such angels, but what he denied was

36
00:03:51,080 --> 00:03:58,840
the efficacy of prayer or the importance of prayer, that sure you could, you could pray

37
00:03:58,840 --> 00:04:08,040
and, and beseech the gods for blessings or for mercy or so on, but there's no reason

38
00:04:08,040 --> 00:04:23,400
to think that they would comply, nor is there any benefit from, or any spiritual benefit

39
00:04:23,400 --> 00:04:30,440
to such activities.

40
00:04:30,440 --> 00:04:36,880
They certainly didn't have the power to enlighten one or to bring the highest benefit.

41
00:04:36,880 --> 00:04:45,400
And I guess that's really where Buddhism goes beyond a theistic religion, it's, it doesn't

42
00:04:45,400 --> 00:04:51,640
deny the fact that angels can, and what you might call gods can potentially help human

43
00:04:51,640 --> 00:04:52,640
beings.

44
00:04:52,640 --> 00:04:57,560
It's, it's not even the point to suggest that they, that God can't move mountains as

45
00:04:57,560 --> 00:05:06,240
they say in Christianity, which, anyway, it's that moving mountains wouldn't really

46
00:05:06,240 --> 00:05:15,240
help you if, if you pray to God and, and God were to answer all of your prayers, still

47
00:05:15,240 --> 00:05:20,640
wouldn't mean much because, well, the prayers that God can't answer are may I become enlightened,

48
00:05:20,640 --> 00:05:26,440
may I be free from suffering, may I be free from greed, anger and delusion, may I become

49
00:05:26,440 --> 00:05:32,640
a better person or just can't happen, doesn't come from the grace of God and the grace

50
00:05:32,640 --> 00:05:44,240
of God, sure, maybe you could get rich, you could live in luxury, but you couldn't go

51
00:05:44,240 --> 00:05:54,080
to heaven, God doesn't have the power to bring someone to heaven or prevent them from

52
00:05:54,080 --> 00:06:08,600
entering heaven, nor does any God or angel have the power to send someone to hell, not

53
00:06:08,600 --> 00:06:15,480
with some qualification because of course, beings can affect other beings, but the alternate

54
00:06:15,480 --> 00:06:22,760
power comes from within, if your mind is pure, no one can send you to hell, but if your

55
00:06:22,760 --> 00:06:27,400
mind is impure, it's possible to instigate, that's certainly possible that angels could

56
00:06:27,400 --> 00:06:36,680
manipulate you and trick you into doing deeds that would send you to hell or manipulate

57
00:06:36,680 --> 00:06:39,400
a person into sending them to heaven.

58
00:06:39,400 --> 00:06:44,240
So, I mean, it just means that there is an acknowledgement whether you believe this or

59
00:06:44,240 --> 00:06:52,440
not, that there are certain beings that are not quanted, not qualitatively different

60
00:06:52,440 --> 00:06:59,680
from human beings in the sense that they still have defilements and they still have life

61
00:06:59,680 --> 00:07:07,800
spans and they're still limited to affecting the physical realm.

62
00:07:07,800 --> 00:07:20,600
They can't actually manipulate another person's mind, not directly, but anyway, there

63
00:07:20,600 --> 00:07:27,800
is some order to the angel realms, for those of you who are interested and curious about

64
00:07:27,800 --> 00:07:28,800
these things.

65
00:07:28,800 --> 00:07:32,360
What's interesting about this suit, and what really makes it Buddhist, because obviously

66
00:07:32,360 --> 00:07:39,880
talking about angels isn't particularly Buddhist, is that sake is the Buddhist.

67
00:07:39,880 --> 00:07:45,560
The King of the Gods of the Thirty-Three is a follower of the Buddha and they're concerned

68
00:07:45,560 --> 00:07:57,440
about the activities of human beings, the goodness of human beings.

69
00:07:57,440 --> 00:08:00,160
It's a group, and it's not to say that all angels will be like this, but this group

70
00:08:00,160 --> 00:08:11,240
of angels, particularly, is concerned as to whether human beings are honoring religious

71
00:08:11,240 --> 00:08:18,600
people, honoring elders, behaving properly towards their father and mother, whether they're

72
00:08:18,600 --> 00:08:26,440
keeping holy days in the sense of having religious observances, spiritual practice on

73
00:08:26,440 --> 00:08:34,920
at least once a week, and doing good deeds, are they giving charity, or are they keeping

74
00:08:34,920 --> 00:08:37,800
morality, or are they practicing meditation?

75
00:08:37,800 --> 00:08:46,320
But it's kind of funny why they're interested in this, and somewhat self-serving or

76
00:08:46,320 --> 00:08:48,760
pragmatic, I guess.

77
00:08:48,760 --> 00:08:52,880
They're concerned with this because they say, if the four great kings report to them that

78
00:08:52,880 --> 00:08:58,040
human beings are doing all these things, then they get really happy because they say,

79
00:08:58,040 --> 00:09:02,800
oh, that means that's great, that means there'll be more angels in the future.

80
00:09:02,800 --> 00:09:14,600
More people coming, and we'll have a greater retinue, it's actually somewhat of a base

81
00:09:14,600 --> 00:09:23,640
desire to swell their ranks, and if they find out that people are doing bad things and

82
00:09:23,640 --> 00:09:27,800
not doing good things, then they're sad because there won't be as many angels in the

83
00:09:27,800 --> 00:09:28,800
future.

84
00:09:28,800 --> 00:09:35,800
I mean, for us it simply means it's a reminder that the only way to go to heaven really

85
00:09:35,800 --> 00:09:41,920
is to be a good person, is to have a pure mind, and all of that comes through doing good

86
00:09:41,920 --> 00:09:48,480
deeds, being good to other people, being respectful to other people, being kind, having

87
00:09:48,480 --> 00:09:55,200
a pure mind, not having ulterior motives or a crooked treacherous mind, not being obsessed

88
00:09:55,200 --> 00:10:02,600
with base desires or hatred or aversion or grudging, grudges or that kind of thing.

89
00:10:02,600 --> 00:10:14,120
If your mind can stay pure in this to file the world, then and only then can you

90
00:10:14,120 --> 00:10:23,760
free yourself from it, from the coarseness, from the baseless, from the mundane, to enter

91
00:10:23,760 --> 00:10:32,520
into something more pure, more peaceful, more sublime.

92
00:10:32,520 --> 00:10:41,320
And then there's something funny that Sakha, the King of the Angels, reminds the angels,

93
00:10:41,320 --> 00:10:46,400
says, the person who would be like me should basically do that, so good deeds.

94
00:10:46,400 --> 00:10:54,160
The person who would be like me should keep the holy, keep the holy day, keep the

95
00:10:54,160 --> 00:10:55,160
both at that.

96
00:10:55,160 --> 00:11:00,360
Which means this day where people keep, by the eight precepts.

97
00:11:00,360 --> 00:11:07,000
And the Buddha says, you know, when he said this, it wasn't well said, it wasn't a proper

98
00:11:07,000 --> 00:11:13,440
verse to say, not because it's not good to keep the religious precepts or to practice

99
00:11:13,440 --> 00:11:19,400
goodness and that kind of thing, but because Sakha, who should, who, no one should strive

100
00:11:19,400 --> 00:11:25,800
to be like Sakha, because Sakha is still not devoid of last hatred and delusion.

101
00:11:25,800 --> 00:11:32,280
And he says, but I say, the same thing a person would be like me should observe all of

102
00:11:32,280 --> 00:11:35,480
these things.

103
00:11:35,480 --> 00:11:41,480
Because I am, because, I know not me, sorry, it's not me, a bikhu who has, who has become

104
00:11:41,480 --> 00:11:44,920
an Harahan, someone who has become enlightened, not just the Buddha, but anyone who has

105
00:11:44,920 --> 00:11:50,200
become enlightened should therefore say, the person who would be like me, and who is

106
00:11:50,200 --> 00:11:54,800
fitting.

107
00:11:54,800 --> 00:12:05,640
So, I mean, this kind of puts angels in their place and it's really how is Buddhists,

108
00:12:05,640 --> 00:12:11,360
we look at the world and we don't see angels, most people will live their lives never

109
00:12:11,360 --> 00:12:14,440
having any evidence that these beings exist.

110
00:12:14,440 --> 00:12:25,720
But insofar as you might think that they do exist, there's still, there's still subject

111
00:12:25,720 --> 00:12:36,640
to the same laws and regulations of the universe that goodness, goodness is what leads

112
00:12:36,640 --> 00:12:43,160
to heaven and evils what leads to hell, these beings don't have the power to make people

113
00:12:43,160 --> 00:12:49,760
better people, but they are happy to know that people do, they rejoice as all good people

114
00:12:49,760 --> 00:12:50,760
should.

115
00:12:50,760 --> 00:12:58,880
I mean, we rejoice for we see more people meditating, it means there will be more meditators,

116
00:12:58,880 --> 00:13:05,120
it's in fact not a terribly bad thing to think because it makes our practice easier, makes

117
00:13:05,120 --> 00:13:09,480
it easier to do good deeds when other people are keen on good deeds.

118
00:13:09,480 --> 00:13:19,680
When everyone's doing unwholesome deeds and subsessed with the filaments, it becomes more

119
00:13:19,680 --> 00:13:29,360
difficult to do good things because you get sucked, you get drained by the negativity

120
00:13:29,360 --> 00:13:31,320
of others.

121
00:13:31,320 --> 00:13:43,640
Anyway, so that's a little bit of dhamma for today anyway, I'm just working anyway.

122
00:13:43,640 --> 00:13:45,400
That's all about that.

123
00:13:45,400 --> 00:13:49,320
Do we have any questions?

124
00:13:49,320 --> 00:13:52,320
We do Monday.

125
00:13:52,320 --> 00:14:00,320
Oh wait, one, sorry, one, we're just talking about I should mention it first.

126
00:14:00,320 --> 00:14:15,880
Just a reminder to people that we may have young people coming on this site and any sort

127
00:14:15,880 --> 00:14:28,280
of adult conversation about sexual intercourse or other things, I guess drugs, alcohol should

128
00:14:28,280 --> 00:14:38,640
be moderated or moderate, tempered, so just be mindful of where we are and the fact that

129
00:14:38,640 --> 00:14:48,080
we're not all of age, so not that anything inappropriate has been done or said, but

130
00:14:48,080 --> 00:15:01,480
just a reminder that to keep it PG or there's potential problems with talking about such

131
00:15:01,480 --> 00:15:09,120
things to people who are potentially underage, okay, that's all.

132
00:15:09,120 --> 00:15:14,920
Okay, in practical insight meditation, Mahasi Saya Dot talks about reflecting on certain

133
00:15:14,920 --> 00:15:20,920
things, but to also notice reflecting, to keep it to a minimum, but you seem to be saying

134
00:15:20,920 --> 00:15:25,440
it's not necessary to think or reflect on all during meditation and it's only necessary

135
00:15:25,440 --> 00:15:31,400
to teach, et cetera, so are you disagreeing with him or am I missing something and how

136
00:15:31,400 --> 00:15:38,920
is it possible to garner insight without reflecting on your experience?

137
00:15:38,920 --> 00:15:47,600
I don't think I said that it's not, like reflection has a place in terms of stepping back

138
00:15:47,600 --> 00:15:56,880
and reflecting on the quality of your practice, but the only benefit there is to adjust

139
00:15:56,880 --> 00:15:57,880
your practice.

140
00:15:57,880 --> 00:16:04,800
If your practice is going fine, I can see, you know, there's no benefit to reflecting

141
00:16:04,800 --> 00:16:12,680
on the things that you've learned, certainly reflecting is natural, that it's something

142
00:16:12,680 --> 00:16:18,400
you should be mindful of, but it's not the practice, and I mean, read what the Mahasi

143
00:16:18,400 --> 00:16:22,000
Saya Dot says about what we call nyana.

144
00:16:22,000 --> 00:16:33,920
Nyana is one of the ten Upa Kiles, when your knowledge, the knowledge is that you gain

145
00:16:33,920 --> 00:16:39,080
when you become obsessed with it, fixated on it, when you start thinking about it, that's

146
00:16:39,080 --> 00:16:43,280
actually to the detriment of your practice.

147
00:16:43,280 --> 00:16:50,480
So the question about how could insight arise if you don't reflect, I mean, that's not

148
00:16:50,480 --> 00:16:58,000
how insight is, insight is a direct realization of things, it's not reflection, when you

149
00:16:58,000 --> 00:17:03,840
see that things are arising and ceasing, that's insight, and the more you see that, the

150
00:17:03,840 --> 00:17:07,560
closer you'll get to letting go, once you see it clearly, you'll just have kind of like

151
00:17:07,560 --> 00:17:16,800
an epiphany where you're not even, not even a thought, but it's just an observation that

152
00:17:16,800 --> 00:17:20,560
everything that arises is ceases, and there's nothing worth clinging to, and in the mind

153
00:17:20,560 --> 00:17:28,480
let's go, there's no, there's no place in there for reflection, I think it's a misunderstanding

154
00:17:28,480 --> 00:17:34,320
of what insight is, insight is not a thought, insight is actually weepasana, busana means

155
00:17:34,320 --> 00:17:40,120
seeing, weet means clearly, or especially, or whatever you want to translate, weet really

156
00:17:40,120 --> 00:17:48,760
means with wisdom, bhanya, bhanya actually just means bhanya, knowledge, but complete, you

157
00:17:48,760 --> 00:17:52,440
know, when you know that this is rising, when you know that this is falling, that's actually

158
00:17:52,440 --> 00:18:04,760
wisdom, when you see it arising and ceasing, that's wisdom, that's all you need, I don't

159
00:18:04,760 --> 00:18:10,680
think Mahasya Saya does suggest otherwise, but you'd have to probably read the Burmese

160
00:18:10,680 --> 00:18:15,720
or be sure that it was properly translated, and then you have to understand that, you know,

161
00:18:15,720 --> 00:18:22,960
he generally makes allowances, you know, if he's actually advising people to stop and reflect

162
00:18:22,960 --> 00:18:26,560
and I'd be quite surprised.

163
00:18:26,560 --> 00:18:32,400
But on the other hand, there is an advice for stepping back and reflecting in so far as

164
00:18:32,400 --> 00:18:37,280
it helps you see whether your practice is correct, whether you're actually doing things

165
00:18:37,280 --> 00:18:43,080
properly and reflect on whether you're adequately, you know, you're really missing something

166
00:18:43,080 --> 00:18:48,160
or whether something could be adjusted, that's called the Mungsah, it's one of the four

167
00:18:48,160 --> 00:19:04,080
really deep onto one of the four roads to success.

168
00:19:04,080 --> 00:19:15,600
I don't know, I mean, they're just words, you'd have to see what the actual state was, someone

169
00:19:15,600 --> 00:19:33,080
who says, I don't care, I mean, it sounds like it actually exists, but it's just words

170
00:19:33,080 --> 00:19:40,600
and it's just a view and it's a lot of ego in the sense of, you know, I am above that

171
00:19:40,600 --> 00:19:47,200
kind of thing, but they're just words, I mean, when an experience occurs, are you actually

172
00:19:47,200 --> 00:19:54,440
an unreactive towards it?

173
00:19:54,440 --> 00:19:59,040
Because such people actually, when they are presented with something that does affect

174
00:19:59,040 --> 00:20:07,040
them, they react, the point is, not everyone is reactive towards everything, for some

175
00:20:07,040 --> 00:20:13,640
people, some things are not going to create a reaction, but that's, you know, it's not

176
00:20:13,640 --> 00:20:21,600
a sign that they are, I mean, apathetic, it's just a sign that they, that that thing

177
00:20:21,600 --> 00:20:30,040
is doesn't create, it doesn't have any kind of history with that person.

178
00:20:30,040 --> 00:20:35,360
So, you know, for some people, loud noises, it's obnoxious here in the west, if your neighbors

179
00:20:35,360 --> 00:20:38,120
are having a party, you get quite upset.

180
00:20:38,120 --> 00:20:43,280
In Asia, if your neighbors are having a party, people don't even seem to hear it, it's

181
00:20:43,280 --> 00:20:48,000
quite interesting to see some of the differences from culture to culture, the sort of things

182
00:20:48,000 --> 00:20:55,760
that bothers, bother people from different cultures.

183
00:20:55,760 --> 00:21:06,400
A general sense of apathy, I mean, there can be a delusion, delusion is generally associated

184
00:21:06,400 --> 00:21:11,120
with, with equanimity, and I guess that's really the technical answer, is that the questions

185
00:21:11,120 --> 00:21:19,360
whether it's associated with delusion, but the delusion would be kind of this distracted

186
00:21:19,360 --> 00:21:28,240
thought, if a person is engaged in mental fermentation, if they're thinking about things

187
00:21:28,240 --> 00:21:33,880
and pondering and wondering and confused and that kind of thing, that's delusion, and

188
00:21:33,880 --> 00:21:43,680
there's, there's equanimity with it.

189
00:21:43,680 --> 00:21:50,160
And then there's the, the other, the wholesome kind is with equanimity, one does something

190
00:21:50,160 --> 00:21:57,880
good or one calculates wholesomeness in the mind through meditation, but the equanimity,

191
00:21:57,880 --> 00:22:02,360
you know, I guess the point is the equanimity is the same, but it's nothing to do with

192
00:22:02,360 --> 00:22:03,360
equanimity.

193
00:22:03,360 --> 00:22:09,680
It's the delusion, whether in the mind there's ego or there's confusion or there's mental

194
00:22:09,680 --> 00:22:18,400
distraction, whether the mind is clear or the mind is, is clouded.

195
00:22:18,400 --> 00:22:15,220
Thank you, B

196
00:22:15,220 --> 00:22:20,220
Bumptay.

197
00:22:20,220 --> 00:22:32,240
Is there any point being grateful karma is going to manifest any ways, could or bad, how

198
00:22:32,240 --> 00:22:34,960
should we include gratitude in our practice?

199
00:22:34,960 --> 00:22:35,960
Absolutely.

200
00:22:35,960 --> 00:22:39,960
I mean, that's a, that's not a good outlook.

201
00:22:39,960 --> 00:22:45,000
I mean, you think, okay, I shouldn't be grateful because that person's going to get what's

202
00:22:45,000 --> 00:22:46,800
coming to them anyway.

203
00:22:46,800 --> 00:22:47,800
Who cares?

204
00:22:47,800 --> 00:22:50,120
Who cares whether to get what's coming to them or not?

205
00:22:50,120 --> 00:22:51,920
That's not the point.

206
00:22:51,920 --> 00:22:54,040
Gratitude is a wonderful state of mind.

207
00:22:54,040 --> 00:22:57,280
Gratitude is, we don't worry about other people.

208
00:22:57,280 --> 00:23:02,200
We're not grateful, so that it helps the other person, we're grateful, so that we don't

209
00:23:02,200 --> 00:23:09,560
become mean and terrible people who have no sense of appreciation for the good deeds of

210
00:23:09,560 --> 00:23:13,160
others.

211
00:23:13,160 --> 00:23:15,160
Gratitude is something that should come naturally.

212
00:23:15,160 --> 00:23:24,520
And if you find yourself ungrateful or unmindful of the good deeds of others, you should

213
00:23:24,520 --> 00:23:29,240
note that and try to understand why, is it because I'm selfish or self-absorbed?

214
00:23:29,240 --> 00:23:37,320
It's generally based in due to either one of the two, a strong sense of greed and or a

215
00:23:37,320 --> 00:23:45,200
strong sense of a strong delusion being self-absorbed and, or, you know, it can also come

216
00:23:45,200 --> 00:23:50,000
from anger, I suppose, if you're totally obsessed with your own suffering so that when

217
00:23:50,000 --> 00:23:58,560
people help you, you just fix and focus on your own problems and how it, how your own

218
00:23:58,560 --> 00:24:06,440
problems are solved or not solved, rather than thinking about the person who helped you.

219
00:24:06,440 --> 00:24:11,880
Gratitude is most apparent in those who have the least attachment.

220
00:24:11,880 --> 00:24:18,000
Those who are least concerned about their own well-being, right?

221
00:24:18,000 --> 00:24:22,960
People who are most able to appreciate the good things in others are those who have good

222
00:24:22,960 --> 00:24:28,040
things in themselves or those who aren't concerned about their own good things in the

223
00:24:28,040 --> 00:24:34,120
sense that they don't have feelings of guilt or worry, that people who have lots of unpleasant

224
00:24:34,120 --> 00:24:40,200
things inside tend to be too self-absorbed, worried about their own well-being, concerned

225
00:24:40,200 --> 00:24:45,840
and greedy or angry and frustrated and upset and deluded and conceded.

226
00:24:45,840 --> 00:24:49,360
You can see how all of those things get in the way of gratitude.

227
00:24:49,360 --> 00:24:54,360
So when all of those gone, it's very easy to appreciate the good things in others.

228
00:24:54,360 --> 00:25:01,840
You're able to be mindful of the truth and that's that this person helped you.

229
00:25:01,840 --> 00:25:08,560
I wouldn't try to cultivate it too much, I'd just be more aware as to whether you have it

230
00:25:08,560 --> 00:25:22,080
or not and if you don't have it, there's a sign that you've got something blocking it.

231
00:25:22,080 --> 00:25:25,120
Can you change the letter broadcast to a different time?

232
00:25:25,120 --> 00:25:27,680
Is there someone else who would want that?

233
00:25:27,680 --> 00:25:29,360
That's a good question.

234
00:25:29,360 --> 00:25:31,360
I can change it to another time.

235
00:25:31,360 --> 00:25:35,200
This time happens to be fairly convenient for me.

236
00:25:35,200 --> 00:25:40,720
Won't be convenient on Monday soon, but apart from that, I can't think of a better time

237
00:25:40,720 --> 00:25:41,720
for me.

238
00:25:41,720 --> 00:25:46,840
I could do it early afternoon, but that would probably be a bad choice for most of our

239
00:25:46,840 --> 00:25:54,080
viewers since most of our viewers are in North America, which I know doesn't really

240
00:25:54,080 --> 00:25:59,960
help those of you who are in Europe, but if you look at the meditation list, the majority

241
00:25:59,960 --> 00:26:08,600
of the USA, if you Canada and India and Europe, then we got a bunch of Mexico as well.

242
00:26:08,600 --> 00:26:13,480
Which I mean, the question is what is the cause, what is the effect, whether it's because

243
00:26:13,480 --> 00:26:24,760
we're broadcasting in the evening, that could just be because of the time, the Europeans

244
00:26:24,760 --> 00:26:29,880
aren't going to be on much anymore, except for Rustlin.

245
00:26:29,880 --> 00:26:39,160
Yeah, I just read down a little bit, it was because the person often has a lot of questions

246
00:26:39,160 --> 00:26:46,160
to ask during the live broadcast, but decided that he would be patient and use it as

247
00:26:46,160 --> 00:26:52,120
a practice, where you could get really fancy Monday and have Tuesday and Thursday's early,

248
00:26:52,120 --> 00:26:54,760
and Monday, Wednesday, Friday, and the normal time.

249
00:26:54,760 --> 00:27:00,640
I can't even keep track of my schedule as it is, I don't know which day it is, and most

250
00:27:00,640 --> 00:27:01,640
days.

251
00:27:01,640 --> 00:27:05,440
Keeping it simple is good too.

252
00:27:05,440 --> 00:27:11,120
But during my last meditation session, this suddenly felt like my legs were not covered

253
00:27:11,120 --> 00:27:12,120
by my skin.

254
00:27:12,120 --> 00:27:17,400
The legs still not like mine, but not the skin, it was sickening to my stomach, and I still

255
00:27:17,400 --> 00:27:20,440
prepped the same right.

256
00:27:20,440 --> 00:27:24,640
Well that's not practice, you see, that's that's experience.

257
00:27:24,640 --> 00:27:26,880
The practice is how you relate to that experience.

258
00:27:26,880 --> 00:27:34,360
Be very clear that these are two distinct things, what you experience and what arises

259
00:27:34,360 --> 00:27:40,200
based on your practice is not the practice, whether it be knowledge, or whether it be

260
00:27:40,200 --> 00:27:46,680
strange experiences like this, or even whether it be unpleasant experiences, defilements.

261
00:27:46,680 --> 00:27:51,880
If anger arises, that's not the practice, it's not like, oh, I'm very angry, I must

262
00:27:51,880 --> 00:27:55,200
be doing something wrong.

263
00:27:55,200 --> 00:28:00,040
Anger is an experience, it's a reaction, it is a bad thing, but don't worry about that,

264
00:28:00,040 --> 00:28:01,800
that's not your concern.

265
00:28:01,800 --> 00:28:05,040
What happened, your concern is what you're going to do about it.

266
00:28:05,040 --> 00:28:08,040
So when you have this experience, how are you going to react to it?

267
00:28:08,040 --> 00:28:14,400
In fact, they're disgusted, potentially problematic, because there's a version disliking.

268
00:28:14,400 --> 00:28:18,600
So that's not the proper reaction, but now that that's happened, you should say disliking,

269
00:28:18,600 --> 00:28:24,240
disliking or disgusted, disliking is probably better.

270
00:28:24,240 --> 00:28:29,240
If you're just aware of it, then you would say knowing, knowing, aware, or something.

271
00:28:29,240 --> 00:28:36,760
If it's a feeling, you might say feeling, feeling, then that's the practice.

272
00:28:36,760 --> 00:28:38,760
Then you'll say, you know, it comes and it goes.

273
00:28:38,760 --> 00:28:45,640
I think the problem is it's practice is terribly unremarkable.

274
00:28:45,640 --> 00:28:52,560
The practice itself is too simple, it's unremarkable.

275
00:28:52,560 --> 00:29:00,400
That's it, you just say, thinking, thinking, seeing, feeling, feeling, angry, angry.

276
00:29:00,400 --> 00:29:05,720
But it's made up for the fact that when you do that, there's fireworks, the things that

277
00:29:05,720 --> 00:29:12,160
you're observing are incredibly interesting, there's a real transformation that goes on.

278
00:29:12,160 --> 00:29:18,760
So be clear that the practice is incredibly simple, it's not much to it, but the experiences

279
00:29:18,760 --> 00:29:27,280
that you're going to have are diverse and very terribly exciting, if you let them be.

280
00:29:27,280 --> 00:29:34,680
Which, of course, you shouldn't, you should be mindful of it.

281
00:29:34,680 --> 00:29:39,680
Conting during eating meditation is the best practice to always keep your eyes open, or

282
00:29:39,680 --> 00:29:42,920
can the eyes be closed while chewing and falling.

283
00:29:42,920 --> 00:29:48,480
They certainly can be closed while chewing and falling, there's no rule, hard or fast,

284
00:29:48,480 --> 00:29:53,640
but if you're really keen on it, closing your eyes is a good thing.

285
00:29:53,640 --> 00:30:04,160
That's your focus on, focus more on the mouth and the throat, taste and so on.

286
00:30:04,160 --> 00:30:10,480
Were the current android apps still be working when the new cycle is left?

287
00:30:10,480 --> 00:30:11,480
No.

288
00:30:11,480 --> 00:30:17,720
You know, we probably could make it work if someone wanted to make a better Android app

289
00:30:17,720 --> 00:30:23,240
would be great, but on the other hand it might be nice just to use the website as it is

290
00:30:23,240 --> 00:30:28,240
because then we only have to work on one thing.

291
00:30:28,240 --> 00:30:33,240
But the new site looks a lot like the Android app, if you've seen it.

292
00:30:33,240 --> 00:30:41,560
So yeah, give up the Android app once the new site goes live.

293
00:30:41,560 --> 00:30:51,560
You know, there's an iOS app apparently, but I think that might work, see.

294
00:30:51,560 --> 00:31:00,360
And you still can sign in to the regular site on a phone, so it's just a matter of wanting

295
00:31:00,360 --> 00:31:01,360
to be portable.

296
00:31:01,360 --> 00:31:08,200
You can still lock your time in, just through a regular browser on the phone, done that.

297
00:31:08,200 --> 00:31:13,040
Well yeah, the new site will look a lot like the Android app anyway, it won't be as hard

298
00:31:13,040 --> 00:31:14,040
to do.

299
00:31:14,040 --> 00:31:19,320
Like this one you have to find a little button, but on the new one it'll be much more

300
00:31:19,320 --> 00:31:23,760
mobile friendly.

301
00:31:23,760 --> 00:31:28,240
My sitting meditation is usually stressful and it seems to be getting worse.

302
00:31:28,240 --> 00:31:31,320
It feels like I'm forcing myself to stay seated.

303
00:31:31,320 --> 00:31:34,360
Do I just continue to work through it?

304
00:31:34,360 --> 00:31:40,720
Yeah, I mean, this is a case where you might want to use my lungs as step back and reflect

305
00:31:40,720 --> 00:31:46,040
on what you might be doing wrong, what you might be missing, not doing wrong exactly, but

306
00:31:46,040 --> 00:31:50,840
the aversion is what you should be meditating on, you know, if you feel stressed and focus

307
00:31:50,840 --> 00:31:56,200
on the stress, it's not easy and this is where the fireworks happen, this is where the

308
00:31:56,200 --> 00:32:04,280
simple technique does a number on you, it really messes with you, challenges you, it

309
00:32:04,280 --> 00:32:12,240
is called, you know, it sounds like probably, you know, it sounds a bit, you're doing fine

310
00:32:12,240 --> 00:32:15,520
because you're cultivating patience.

311
00:32:15,520 --> 00:32:18,640
If it feels like you're forcing, then it's not that you're forcing, it's that you're

312
00:32:18,640 --> 00:32:23,600
missing something, that you're not actually being mindful of the aversion, try and

313
00:32:23,600 --> 00:32:28,640
be as mindful of the aversion as you can as quick, catch what's really happening because

314
00:32:28,640 --> 00:32:30,440
there's no such thing as meditation.

315
00:32:30,440 --> 00:32:35,280
There's no, you don't have sitting meditation, it doesn't exist.

316
00:32:35,280 --> 00:32:41,120
What happens when you sit down and close your eyes and then you watch the stomach now?

317
00:32:41,120 --> 00:32:47,480
The tendency probably is to try and force the stomach to rise and fall, which is an interesting

318
00:32:47,480 --> 00:32:52,680
thing, it's an interesting fact that your mind, you know, this is the case that your mind

319
00:32:52,680 --> 00:33:02,080
is doing that, you know, the things that your mind, that our mind does as what we're

320
00:33:02,080 --> 00:33:07,480
interested in, we're interested to learn how the mind works, to learn about the good

321
00:33:07,480 --> 00:33:13,000
things and the bad things in the mind, what we're doing right, what we're doing wrong,

322
00:33:13,000 --> 00:33:18,480
it sounds like probably like all of us, you're doing something wrong, so just keep watching

323
00:33:18,480 --> 00:33:29,920
and you'll slowly see, oh, I'm doing this all wrong, boy, I should just stop that.

324
00:33:29,920 --> 00:33:35,080
Can I do mindful of meditation without mantras?

325
00:33:35,080 --> 00:33:39,360
You can, but you'd have to find a different teacher and a different tradition, because

326
00:33:39,360 --> 00:33:44,960
some people say that you shouldn't use mantras, in our tradition we say you should and

327
00:33:44,960 --> 00:33:56,000
it's actually important to use mantras, so not in our tradition, sorry, I mean, why would

328
00:33:56,000 --> 00:33:57,000
you want to?

329
00:33:57,000 --> 00:34:00,560
I guess it's the question, of course, I understand why people want to, but to some

330
00:34:00,560 --> 00:34:04,960
leaves you shaking your head because they're so beneficial.

331
00:34:04,960 --> 00:34:11,880
That's the most powerful thing we have is this tool reminding ourselves that the best

332
00:34:11,880 --> 00:34:16,680
meditation technique I can think of, people who don't use them are missing out, don't

333
00:34:16,680 --> 00:34:18,880
know what they're missing out of.

334
00:34:18,880 --> 00:34:33,560
You're all kind of my questions, Monday, alright then, that's all for tonight, thanks Robin

335
00:34:33,560 --> 00:34:39,720
for your help, thanks everyone for coming out, for meditating and for your questions, have

336
00:34:39,720 --> 00:34:40,720
a good night.

337
00:34:40,720 --> 00:34:42,720
Thank you, Monday, good night.

