1
00:00:00,000 --> 00:00:07,760
Hi everyone, I'm a little bit sick today, so I wasn't thinking originally to make a video, but

2
00:00:08,400 --> 00:00:10,400
you know, sitting around the house

3
00:00:12,840 --> 00:00:18,500
So I thought I'd go into one question that has come up a lot and I've tried to stay away from it

4
00:00:20,560 --> 00:00:26,480
And that is how I got into Buddhism and ultimately became a monk, but I think yeah, it's probably worth

5
00:00:26,480 --> 00:00:32,880
Explaining and getting getting over because there's a lot of people wondering who I am

6
00:00:34,720 --> 00:00:36,720
whether they can

7
00:00:39,280 --> 00:00:44,020
Whether they can trust the things I say and so I'm wondering where I'm coming from what my background is

8
00:00:44,020 --> 00:00:54,340
So how I got in interested in Buddhism, I think actually relates to when I was young. I was one of those people who

9
00:00:56,020 --> 00:00:59,700
I think among many people who had a keen

10
00:01:04,260 --> 00:01:12,100
Interest in in in these big questions like why why do we all have to die and and and what is the meaning of life

11
00:01:12,100 --> 00:01:17,540
and trying to find a way of living that has more meaning than just

12
00:01:18,180 --> 00:01:21,940
living for the sake of living or or or just to get by I

13
00:01:23,060 --> 00:01:24,340
remember

14
00:01:24,340 --> 00:01:29,140
thinking about my family in them and just just realizing one day when I was about eight

15
00:01:30,180 --> 00:01:35,620
That you know everyone in my family is going to die that all the people I knew are going to die that this is a fact of life

16
00:01:35,620 --> 00:01:38,660
And it hit me really quite profoundly and it was something that

17
00:01:38,660 --> 00:01:46,820
You know pose questions in my mind. Why or or what do we do about this? There's nothing we can do

18
00:01:46,820 --> 00:01:48,820
It was a sense of hopelessness and

19
00:01:50,020 --> 00:01:52,980
I started getting this this idea that

20
00:01:55,300 --> 00:02:01,700
My understanding of life was that we you know, we go to school to get a job to get

21
00:02:01,700 --> 00:02:07,700
to collect money to get old or to retire and by the time we retire we're old and

22
00:02:08,500 --> 00:02:13,060
We've lived most of our life out and and haven't gained anything from it. So

23
00:02:14,020 --> 00:02:17,220
Even when I was young when I was I think maybe 11 or 12

24
00:02:17,780 --> 00:02:19,780
I was really set in this idea that

25
00:02:20,900 --> 00:02:26,100
The ordinary way of living with meaningless and I had to do something to find a way out

26
00:02:28,580 --> 00:02:29,620
and

27
00:02:29,620 --> 00:02:33,220
So I started looking into things like meditation when

28
00:02:34,100 --> 00:02:38,340
I was practicing martial arts and I remember incorporating meditation into it

29
00:02:38,740 --> 00:02:41,220
When I started doing a little bit of teaching martial arts

30
00:02:41,220 --> 00:02:45,700
I would teach people meditation as well and people really liked it and I thought it was great

31
00:02:47,300 --> 00:02:51,380
And I would practice meditation at home even lying down in bed. I would

32
00:02:52,260 --> 00:02:54,260
enter into these states of

33
00:02:54,260 --> 00:02:58,260
I've of intense concentration and

34
00:03:01,060 --> 00:03:04,900
You know it was something that was always with me even from that from that time

35
00:03:06,900 --> 00:03:10,900
And then someone handed me in school the the Daudeting which was a

36
00:03:11,940 --> 00:03:15,620
Very important book for me for most of my teenage years. It was

37
00:03:16,900 --> 00:03:18,900
It was sort of what set me on this path

38
00:03:19,460 --> 00:03:21,460
I instantly reading this book

39
00:03:21,460 --> 00:03:25,780
I was instantly converted and thought you know, I'm a Taoist and from then on

40
00:03:26,340 --> 00:03:30,180
When people asked I said I was a Taoist and this is what I believed in and

41
00:03:30,660 --> 00:03:34,660
This is what what resonated with me was this teaching in the Daudeting

42
00:03:36,500 --> 00:03:41,380
So I I tried to get through high school and did really well

43
00:03:41,380 --> 00:03:46,420
But hated it and asked repeatedly to to leave school to be taken out of school

44
00:03:46,420 --> 00:03:51,060
Through public school. I'd been home to school. Then it was only in high school that we went back and it was a

45
00:03:51,620 --> 00:03:57,540
Just wasn't doing it for me. I said this isn't the way to live our lives. This isn't for any purpose except getting into this

46
00:03:58,180 --> 00:03:59,380
wheel of

47
00:03:59,380 --> 00:04:01,380
samsara

48
00:04:02,420 --> 00:04:08,740
And so my parents moved me to an alternative school and then another alternative school and finally I finished high school

49
00:04:10,020 --> 00:04:12,020
And went on to university

50
00:04:12,020 --> 00:04:18,820
and then in university I decided that I'd had enough and I dropped out of my first year of university and

51
00:04:19,140 --> 00:04:22,340
You know, there was a lot of stuff going on here. I wasn't at all spiritual

52
00:04:22,340 --> 00:04:24,980
I was getting caught up in the teenage years and and

53
00:04:26,580 --> 00:04:30,100
He'd anism drugs sex rock and roll all of the

54
00:04:30,820 --> 00:04:36,020
These things that are supposed to bring happiness and they were making me miserable and I remember this was a terrible period of my life

55
00:04:36,340 --> 00:04:39,620
And it just built up and built up to the point where in university

56
00:04:39,620 --> 00:04:42,900
I said that's it tonight. I withdrew and

57
00:04:43,860 --> 00:04:45,860
tried to go find the place to go

58
00:04:47,300 --> 00:04:49,300
I'd find a way

59
00:04:49,380 --> 00:04:50,740
to

60
00:04:50,740 --> 00:04:52,740
Some sort of spiritual path to follow

61
00:04:53,540 --> 00:04:56,740
I was thinking of going to Central America to to do all the

62
00:04:57,380 --> 00:05:01,540
These crazy hallucinogenic drugs and the with the shamans and so on

63
00:05:02,340 --> 00:05:04,340
I was still at sort of in that mode and

64
00:05:04,340 --> 00:05:10,420
A little bit a little bit lost. I didn't know where to go but people kept pushing me to go to Thailand

65
00:05:10,420 --> 00:05:14,580
There was you know this person would talk about Thailand that person would talk about Thailand until finally

66
00:05:16,340 --> 00:05:21,460
One of my climbing my rock climbing our friends suggested that we go there to do some rock climbing

67
00:05:22,580 --> 00:05:27,860
So I took him up on that thinking you know, this would give me a chance to practice to follow Taoism to follow

68
00:05:27,860 --> 00:05:34,020
That these Taoist teachings off from Thailand. I'll make a trip to China or Japan maybe get into some zen or something

69
00:05:36,020 --> 00:05:38,020
I tried to find some

70
00:05:38,020 --> 00:05:43,060
Place and I was joking with people that I would you know my intention was to become a monk, but you know, I never really took it seriously

71
00:05:44,660 --> 00:05:49,700
I was looking for something and I you know, I thought I'll find the truth of find some

72
00:05:50,980 --> 00:05:52,980
Some spiritual path

73
00:05:53,300 --> 00:05:55,300
When I got to Thailand

74
00:05:55,300 --> 00:06:00,580
It wasn't really what I I I I had hoped for and I wasn't really even looking in Thailand

75
00:06:00,900 --> 00:06:02,900
I wasn't interested in Buddhism actually at all

76
00:06:03,860 --> 00:06:08,020
Except maybe Zen Buddhism. I was more interested in Taoism and I was thinking about going to China

77
00:06:08,420 --> 00:06:13,380
So I spent some time looking around and reading up on what's going on in China and where I could find

78
00:06:14,820 --> 00:06:21,060
Some kind of Taoist teachings and I didn't have much luck, but I bought a ticket to go to China

79
00:06:21,060 --> 00:06:27,060
I got my visa already and I was ready to go and then finally it just kind of hit me that no China is probably

80
00:06:28,100 --> 00:06:32,660
Not going to be the most spiritual places. It's a communist country at the time. It wasn't yet

81
00:06:33,700 --> 00:06:35,700
Even openly Buddhist

82
00:06:36,980 --> 00:06:38,980
It was a pretty

83
00:06:39,220 --> 00:06:44,340
It was a pretty scary I scary thing to just go into China not knowing anyone

84
00:06:45,300 --> 00:06:49,780
So I changed my mind and went to meditate in Thailand which turned out to be

85
00:06:49,780 --> 00:06:51,780
I think the right choice

86
00:06:52,100 --> 00:06:56,820
It helped me to understand what the Buddha what is Buddhism because you know when you read about

87
00:06:57,300 --> 00:07:00,260
Buddhism often it's you know people talking about suffering and

88
00:07:01,220 --> 00:07:03,220
That seems quite dry and dull and boring

89
00:07:04,100 --> 00:07:06,100
Especially sometimes the way it's presented

90
00:07:08,660 --> 00:07:15,460
But when I went to meditate and you know started actually practicing instead of just reading and theorizing and and

91
00:07:16,020 --> 00:07:18,020
Think and thinking about it

92
00:07:18,020 --> 00:07:19,460
I

93
00:07:19,460 --> 00:07:22,900
Really helped me to understand what was this thing I was looking for this this

94
00:07:23,860 --> 00:07:30,500
Talking about wisdom and you know it kind of seemed in the beginning that wisdom would be something that you talk about and you just get it

95
00:07:31,220 --> 00:07:37,940
But now once you started meditating wisdom became something that you you understand you you realize for yourself through

96
00:07:38,580 --> 00:07:41,300
Experience through through seeing for yourself

97
00:07:42,420 --> 00:07:44,420
So I spent a month there meditating

98
00:07:44,420 --> 00:07:48,740
and that changed my life. I would say that's probably

99
00:07:49,940 --> 00:07:51,940
the most important thing I've ever done

100
00:07:53,140 --> 00:07:54,900
and

101
00:07:54,900 --> 00:08:00,740
Then I went back. I was you know it was it was kind of 50 50 should I stay should I go and

102
00:08:01,060 --> 00:08:07,220
Someone suggested I should go back and get my education if my parents wanted me to so I followed that advice and went back and

103
00:08:07,220 --> 00:08:14,340
And spent the next two years. This is where you get into where you know might be coming a monk. So I'm just talking about my background

104
00:08:14,820 --> 00:08:16,020
but

105
00:08:16,020 --> 00:08:19,060
You know I want you to understand it's a little bit convoluted

106
00:08:19,060 --> 00:08:25,540
You may not become a monk in in exactly this way because it took me two years to get my parents

107
00:08:26,420 --> 00:08:28,420
you know almost

108
00:08:28,980 --> 00:08:30,420
Consent

109
00:08:30,420 --> 00:08:34,980
In Buddhism we have this rule that if you're going to become a monk your parents have to agree on it

110
00:08:34,980 --> 00:08:40,660
It's a useful rule because it's very difficult to meditate if you have problems with the people that are close to you

111
00:08:42,180 --> 00:08:44,580
So I spent two years trying to

112
00:08:46,260 --> 00:08:48,100
Get to the point where I'd be able to ordain

113
00:08:49,140 --> 00:08:53,620
I think in the beginning it wasn't really the the the major goal

114
00:08:55,140 --> 00:09:03,380
But as I studied and as I I meditated more and learned more about what the Buddha taught it just seemed like the right way to go

115
00:09:03,380 --> 00:09:06,740
It seemed like the the the answer the the

116
00:09:08,420 --> 00:09:10,420
the next step

117
00:09:10,420 --> 00:09:16,740
So I spent time learning about how to become you know what was the life of a monk and what are the rules of the monk?

118
00:09:19,540 --> 00:09:26,980
And I spent most of those two years actually in a Buddhist monastery in Canada. I was quite

119
00:09:28,020 --> 00:09:30,500
fortunate to be able to

120
00:09:30,500 --> 00:09:37,380
Stay at a Buddhist monastery while I was going to school so instead of renting an apartment getting a job whatever

121
00:09:37,380 --> 00:09:38,420
I had no money

122
00:09:38,420 --> 00:09:39,940
I went to a

123
00:09:39,940 --> 00:09:43,860
Cambodian Buddhist monastery this little church that they had bought and

124
00:09:44,420 --> 00:09:48,980
Started a Buddhist monastery and asked them if I could stay there and they were

125
00:09:49,460 --> 00:09:51,060
You know kind of like

126
00:09:51,060 --> 00:09:53,060
The Abbot said to me. It's so it said. It's okay

127
00:09:53,300 --> 00:09:57,140
And I learned after staying with him from from him that it's okay really means

128
00:09:57,140 --> 00:10:01,300
No, we'd rather that you don't but but it was his way of politely saying

129
00:10:02,980 --> 00:10:04,980
Probably not a good idea, but

130
00:10:05,620 --> 00:10:08,500
I was stubborn and he let me and we ended up getting along really well

131
00:10:08,500 --> 00:10:12,100
And he's now one of my very good friends even though we've kind of lost touch

132
00:10:12,420 --> 00:10:17,380
But I stayed with him there went to school and kept eight eight the eight Buddhist precepts

133
00:10:17,380 --> 00:10:21,540
I wouldn't eat in the evening so I was at university all day and I would only eat in the morning

134
00:10:22,420 --> 00:10:24,820
I didn't have a job so I would you know just get

135
00:10:24,820 --> 00:10:29,060
By the word there was a few days where I you know didn't have any food to eat

136
00:10:29,460 --> 00:10:33,860
It was a really a hard life, but it I felt like it was a training for me. So

137
00:10:35,460 --> 00:10:37,460
I think in that sense

138
00:10:37,860 --> 00:10:39,860
It's important to point out that

139
00:10:40,900 --> 00:10:43,940
You know sometimes it it it helps to

140
00:10:44,580 --> 00:10:45,620
Prepare yourself

141
00:10:45,620 --> 00:10:49,940
This is why they often suggest people to go and stay at a monastery for a year or so

142
00:10:50,420 --> 00:10:53,060
I kind of did this before I went to or day and I

143
00:10:53,060 --> 00:10:57,940
I was not really in a real monastic situation, but

144
00:10:59,540 --> 00:11:01,860
You know, I was kind of living the life keeping

145
00:11:02,900 --> 00:11:07,700
Some of the monk's roles in fact. I tried to keep as many of the monk's roles as I could even not as a monk

146
00:11:10,020 --> 00:11:15,460
But finally it became too much and I decided that that school wasn't going to work for me and

147
00:11:16,420 --> 00:11:18,420
So I went back to Thailand

148
00:11:19,300 --> 00:11:21,300
ordained as a monk

149
00:11:21,300 --> 00:11:29,300
And the only the only way I could manage to ordain as a monk is if I promised my parents that I would come back and continue to learn

150
00:11:29,300 --> 00:11:34,180
I said don't worry. I will become a monk, but I'll still come back and study so

151
00:11:36,260 --> 00:11:41,940
So I did I became a monk and then in the middle of the winter I came went back to Canada and

152
00:11:43,300 --> 00:11:45,300
went back to school

153
00:11:45,940 --> 00:11:47,940
as a monk and

154
00:11:47,940 --> 00:11:52,740
Unsurprisingly that didn't last long. I wasn't into it

155
00:11:53,540 --> 00:11:58,180
I had kind of had enough of school. I had enough of school many years ago actually, but

156
00:11:59,380 --> 00:12:03,860
It was sort of an agreement that I'd had with my father that I go back to school

157
00:12:04,580 --> 00:12:11,380
And so in the end I broke the agreement. I said I can't do it. It's not going to work. I have to

158
00:12:12,340 --> 00:12:14,340
have to follow my

159
00:12:14,340 --> 00:12:18,820
way and so I left

160
00:12:19,060 --> 00:12:24,740
Did about a half-year school hung out for a while did some stuff at the monastery help these novices

161
00:12:28,100 --> 00:12:30,100
And then went back to to Thailand

162
00:12:31,060 --> 00:12:33,300
That's my story on how I became a monk

163
00:12:34,740 --> 00:12:39,460
The actual ordination procedure was there's not much to say

164
00:12:39,460 --> 00:12:45,060
I think you know, it's important here is the path that leads up to it getting your

165
00:12:45,620 --> 00:12:49,540
life in order if you if you're this kind of person who believes in

166
00:12:50,260 --> 00:12:54,180
In in these sorts of things that you know that this is the

167
00:12:56,820 --> 00:13:01,060
The way out the this is the path that has real meaning and and

168
00:13:02,260 --> 00:13:04,260
benefit and

169
00:13:04,260 --> 00:13:06,260
purpose

170
00:13:06,260 --> 00:13:07,620
then

171
00:13:07,620 --> 00:13:11,780
You know do some meditation start start on the course don't just say okay

172
00:13:11,780 --> 00:13:16,100
I'm gonna go ordain it's a monk. I would say these people don't tend to make it very far

173
00:13:16,580 --> 00:13:20,900
If that's how you're starting out your spiritual life, you're you're probably just going to

174
00:13:21,540 --> 00:13:27,140
To give up and get frustrated and go home because you're not really ready for it. It's a big step

175
00:13:27,940 --> 00:13:29,940
for me it was two years

176
00:13:29,940 --> 00:13:36,100
of meditation study and and just generally living the monastic life

177
00:13:39,460 --> 00:13:42,820
And so if you can do that first I think it'd be greatly benefited

178
00:13:43,860 --> 00:13:48,260
when I when I decided when I the day came to be ordained there were

179
00:13:49,540 --> 00:13:52,580
18 of us who ordained and

180
00:13:52,980 --> 00:13:54,980
I'm the only one left they all

181
00:13:55,620 --> 00:13:56,980
most of them

182
00:13:56,980 --> 00:14:03,620
Disrogue within a week that standard but even the ones who had decided to stay on never made it there were three other

183
00:14:04,180 --> 00:14:06,180
two other Westerners and

184
00:14:06,180 --> 00:14:08,180
15 Thai people and

185
00:14:09,060 --> 00:14:11,060
They've all disrobed

186
00:14:11,300 --> 00:14:15,620
And you know, I felt ready for it for me. It wasn't a big deal. It was

187
00:14:16,900 --> 00:14:21,460
Well, it felt great. It was like a final release. Yes, I've made it

188
00:14:22,100 --> 00:14:25,300
But as for a change in lifestyle, it didn't change much

189
00:14:25,300 --> 00:14:32,700
There were a bunch more rules that I had to keep but other than that, you know, I had already really prepared myself for it

190
00:14:34,980 --> 00:14:40,580
That's my story. That's the first half of my story. The second half gets even crazier

191
00:14:41,540 --> 00:14:45,460
After I became a monk, but nobody's asked that question yet. So

192
00:14:47,780 --> 00:14:51,940
That's an answer to your question. How did I become a monk? I hope that covers the

193
00:14:51,940 --> 00:15:00,260
The the doubts that were in your mind are the answers to your question as you would have an answer to okay, thanks and

194
00:15:00,260 --> 00:15:24,740
the third person frecked a certain

