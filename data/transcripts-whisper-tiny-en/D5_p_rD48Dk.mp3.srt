1
00:00:00,000 --> 00:00:22,000
Good evening broadcasting live, September 23rd, 2015, it's been another long day.

2
00:00:22,000 --> 00:00:32,000
Today, we had our Wednesday group on the lawn, and I was there from 330 to 6.

3
00:00:32,000 --> 00:00:38,000
And I taught meditation about 4 times.

4
00:00:38,000 --> 00:00:49,000
It was a lot very good about coming at the same time, so I left it open, so I was teaching right up until 6.

5
00:00:49,000 --> 00:00:55,000
Small group, but new people, there were few new people.

6
00:00:55,000 --> 00:01:03,000
People have classes, so they come and they go according to their schedules.

7
00:01:03,000 --> 00:01:22,000
And our first meeting is next week.

8
00:01:22,000 --> 00:01:29,000
But today we have a quote, Robin, will you read the quote for us?

9
00:01:29,000 --> 00:01:34,000
Yes, I do not say that the attainment of profound knowledge comes straight away.

10
00:01:34,000 --> 00:01:44,000
On the contrary, it comes by gradual training, a gradual doing, a gradual practice.

11
00:01:44,000 --> 00:01:55,000
Yeah, I'm not clear on this profound knowledge thing.

12
00:01:55,000 --> 00:02:03,000
Don't get it.

13
00:02:03,000 --> 00:02:19,000
Because it should mean, what does Bhikkhu Bodhi says, final knowledge?

14
00:02:19,000 --> 00:02:22,000
Well, it's beyond me.

15
00:02:22,000 --> 00:02:30,000
Also, studying Latin, I think my language part of my brain is fried.

16
00:02:30,000 --> 00:02:35,000
Can we assume they're speaking of enlightenment?

17
00:02:35,000 --> 00:02:38,000
I think it's more like final attainment.

18
00:02:38,000 --> 00:02:43,000
The attainment of profound knowledge, don't get that.

19
00:02:43,000 --> 00:02:48,000
Unless I'm looking at it, unless this is the wrong number, this is it, because, yeah,

20
00:02:48,000 --> 00:02:50,000
Bhikkhu Bodhi says the same thing.

21
00:02:50,000 --> 00:02:57,000
I do not say that final knowledge is achieved all at once.

22
00:02:57,000 --> 00:03:01,000
Yeah, I'm just not of, I think it's my fault.

23
00:03:01,000 --> 00:03:06,000
But it's a very strange word.

24
00:03:06,000 --> 00:03:13,000
But gradual, so it's not about sitting in meditation and suddenly poof you're enlightened.

25
00:03:13,000 --> 00:03:23,000
It's about getting just perfect, getting to the perfect state of mind.

26
00:03:23,000 --> 00:03:29,000
And the perfect state of mind is not about what you experience.

27
00:03:29,000 --> 00:03:40,000
It has, in fact, the perfect state of mind is often associated with the worst experience.

28
00:03:40,000 --> 00:03:45,000
People who become enlightened when they die, it's very common thing.

29
00:03:45,000 --> 00:03:47,000
People who become enlightened when they're sick.

30
00:03:47,000 --> 00:03:48,000
It's a very common thing.

31
00:03:48,000 --> 00:03:52,000
People who become enlightened when they're in excruciating pain.

32
00:03:52,000 --> 00:03:54,000
It's very common.

33
00:03:54,000 --> 00:03:57,000
It's not like everyone who dies and gets sick.

34
00:03:57,000 --> 00:04:02,000
And has pain will become enlightened.

35
00:04:02,000 --> 00:04:18,000
But Ajentang talks about four types of summer CC, which means enlightenment at the same time.

36
00:04:18,000 --> 00:04:22,000
So you become enlightened at the same time.

37
00:04:22,000 --> 00:04:31,000
Your defilements are removed when you have,

38
00:04:31,000 --> 00:04:38,000
it's something to do when you have sick when you're sick when you die.

39
00:04:38,000 --> 00:04:43,000
And when you have great pain.

40
00:04:43,000 --> 00:04:48,000
And the fourth one is when you change your postures.

41
00:04:48,000 --> 00:04:59,000
But it's rare that while it's not rare, it's not as conducive to enlightenment when you experience pleasant and pleasurable states.

42
00:04:59,000 --> 00:05:05,000
So anyone who thinks they're close to enlightenment because meditation is comfortable and blissful.

43
00:05:05,000 --> 00:05:08,000
It will be comfortable for another reason.

44
00:05:08,000 --> 00:05:15,000
There will be still the arising and ceasing phenomena that are impermanence of freeing in themselves.

45
00:05:15,000 --> 00:05:21,000
But one will be perfectly quantumist about them, experiencing them, seeing them for what they are.

46
00:05:21,000 --> 00:05:26,000
This is this, this is this without any of the previous reactions.

47
00:05:26,000 --> 00:05:36,000
So the difference, how you know is it's just like your ordinary experience minus all the attachments, the reactions.

48
00:05:36,000 --> 00:05:38,000
So it'll be an ordinary experience.

49
00:05:38,000 --> 00:05:43,000
Like you become enlightened, just watching the stomach, rising, falling, and then it.

50
00:05:43,000 --> 00:05:49,000
You just see suddenly impermanence, suffering, or non-self, very clear.

51
00:05:49,000 --> 00:05:52,000
And the next moment, the mind lets go.

52
00:05:52,000 --> 00:06:04,000
So the gradual practice is transforming our experience, our ordinary experience from reactionary to non-reactionery.

53
00:06:04,000 --> 00:06:06,000
That's it.

54
00:06:06,000 --> 00:06:10,000
And we're from reactionary to objective.

55
00:06:10,000 --> 00:06:17,000
Once we affect that transformation, once we're completely objective, that last moment

56
00:06:17,000 --> 00:06:22,000
will be one of complete objectivity where we say, it's not worth it.

57
00:06:22,000 --> 00:06:24,000
And then another kill.

58
00:06:24,000 --> 00:06:25,000
No more cleaning.

59
00:06:25,000 --> 00:06:33,000
That's the past.

60
00:06:33,000 --> 00:06:34,000
About the habits.

61
00:06:34,000 --> 00:06:44,000
You know, the reason why our reactivity, our reaction, our reactivity, is a habit that we built up.

62
00:06:44,000 --> 00:06:51,000
And habits cannot be turned off, they must be worn away.

63
00:06:51,000 --> 00:06:54,000
They must be supplanted.

64
00:06:54,000 --> 00:07:00,000
So we have to cultivate a new habit, a habit of mindfulness.

65
00:07:00,000 --> 00:07:03,000
It just takes time.

66
00:07:03,000 --> 00:07:05,000
Enlightenment actually does happen in a moment.

67
00:07:05,000 --> 00:07:08,000
It's just that moment where you finally get it.

68
00:07:08,000 --> 00:07:14,000
But that moment doesn't, you don't just fall into it like a pit or a well.

69
00:07:14,000 --> 00:07:17,000
You have to get there like climbing a mountain.

70
00:07:17,000 --> 00:07:23,000
And when you get to the peak, that's it.

71
00:07:23,000 --> 00:07:31,000
So that's the idea.

72
00:07:31,000 --> 00:07:36,000
Have any questions?

73
00:07:36,000 --> 00:07:40,000
There were some questions earlier on this afternoon.

74
00:07:40,000 --> 00:07:45,000
There were, and there was one that kind of stood out.

75
00:07:45,000 --> 00:07:51,000
One of the meditators was asking about, we've been talking about the food basics cards yesterday.

76
00:07:51,000 --> 00:07:54,000
And as it turns out, there was a meditator question.

77
00:07:54,000 --> 00:07:56,000
Oh, that too.

78
00:07:56,000 --> 00:07:57,000
There's time.

79
00:07:57,000 --> 00:08:05,000
But just while I'm thinking of it, I was just meeting with the treasurer and some other people in the organization.

80
00:08:05,000 --> 00:08:14,000
And the treasurer suggested if anyone was interested in sending a food basics card, what they can do is just send it to the regular support page.

81
00:08:14,000 --> 00:08:19,000
And there's a part where you indicate what your donation is for.

82
00:08:19,000 --> 00:08:31,000
So you would just send it to serimonglow.org with an indication that it's for a food basics food card and the treasurer who lives right near a food basics will make sure that that's taken care of and sent to Monday.

83
00:08:31,000 --> 00:08:36,000
So I'll put that link on there.

84
00:08:36,000 --> 00:08:40,000
You have added privileges on the support page right?

85
00:08:40,000 --> 00:08:42,000
I believe so, yes.

86
00:08:42,000 --> 00:08:44,000
So I can update that as well.

87
00:08:44,000 --> 00:08:46,000
But there's another page.

88
00:08:46,000 --> 00:08:49,000
Now it's just going to put it just in the chat here.

89
00:08:49,000 --> 00:08:51,000
Oh.

90
00:08:51,000 --> 00:09:01,000
And on the comment on the YouTube video, but I'll put it on the support page as well.

91
00:09:01,000 --> 00:09:03,000
Nice little other question.

92
00:09:03,000 --> 00:09:04,000
Yes.

93
00:09:04,000 --> 00:09:10,000
Could you recommend the Mahasi side or writing which would elucidate the more technical aspects of Sati Patana practice?

94
00:09:10,000 --> 00:09:12,000
Thank you, Bhante.

95
00:09:12,000 --> 00:09:16,000
Do you have any writings that are technical like my book is technical?

96
00:09:16,000 --> 00:09:19,000
There's a couple that get close.

97
00:09:19,000 --> 00:09:25,000
But they don't go into detailed detail.

98
00:09:25,000 --> 00:09:27,000
Not sure quite why.

99
00:09:27,000 --> 00:09:37,000
We don't have one that like gives detailed explanation, but he goes fairly close like.

100
00:09:37,000 --> 00:09:42,000
There's a good one called practical Vipassana exercises.

101
00:09:42,000 --> 00:09:47,000
Factical.

102
00:09:47,000 --> 00:09:57,000
I don't think that's it.

103
00:09:57,000 --> 00:09:59,000
No, it's not that one.

104
00:09:59,000 --> 00:10:02,000
That one is one.

105
00:10:02,000 --> 00:10:23,000
There's basic something called basic and progressive, something basic and progressive or something.

106
00:10:23,000 --> 00:10:30,000
Practical insight meditation, basic and progressive stages.

107
00:10:30,000 --> 00:10:39,000
If I can find the epub, the epub or the PDF is somewhere.

108
00:10:39,000 --> 00:10:48,000
Oh, here's a good website that has them.

109
00:10:48,000 --> 00:11:01,000
PDF, but I think they are, see if I have it on static.

110
00:11:01,000 --> 00:11:10,000
No, I don't.

111
00:11:10,000 --> 00:11:17,000
Is that one practical Vipassana meditation exercises?

112
00:11:17,000 --> 00:11:23,000
There's practical Vipassana, but then there's something called practical insight.

113
00:11:23,000 --> 00:11:28,000
Meditation, basic and progressive stages.

114
00:11:28,000 --> 00:11:31,000
Here's one from archive.org.

115
00:11:31,000 --> 00:11:34,000
Oh, there's a good one.

116
00:11:34,000 --> 00:11:37,000
Archive.org has it.

117
00:11:37,000 --> 00:11:39,000
Let's see if it's any good.

118
00:11:39,000 --> 00:11:42,000
How do we do this?

119
00:11:42,000 --> 00:11:44,000
I think this is my book.

120
00:11:44,000 --> 00:11:47,000
I think this is my version.

121
00:11:47,000 --> 00:11:48,000
Wait just a second.

122
00:11:48,000 --> 00:11:49,000
No, maybe not.

123
00:11:49,000 --> 00:11:52,000
It's funny because I put together a book.

124
00:11:52,000 --> 00:11:56,000
Oh, and the.

125
00:11:56,000 --> 00:11:57,000
That's funny.

126
00:11:57,000 --> 00:12:02,000
No, it just looks like the one in particular.

127
00:12:02,000 --> 00:12:06,000
It's mostly good that the poly is all mixed up messed up.

128
00:12:06,000 --> 00:12:11,000
Anyway, here's one.

129
00:12:11,000 --> 00:12:12,000
Not that one.

130
00:12:12,000 --> 00:12:13,000
Rob in it's this one.

131
00:12:13,000 --> 00:12:16,000
I think they're two different books.

132
00:12:16,000 --> 00:12:18,000
This one seems pretty good.

133
00:12:18,000 --> 00:12:44,000
The archive.org version.

134
00:12:44,000 --> 00:13:08,000
I think I've realized about food basics.

135
00:13:08,000 --> 00:13:15,000
That should have come to my mind right away, but didn't.

136
00:13:15,000 --> 00:13:18,000
Is that I can't actually go shopping there.

137
00:13:18,000 --> 00:13:21,000
We're not allowed to go shopping.

138
00:13:21,000 --> 00:13:22,000
Right.

139
00:13:22,000 --> 00:13:25,000
And that's a problem I've had at certain restaurants is.

140
00:13:25,000 --> 00:13:29,000
Like there's a Tim Hortons in the university and there's a Starbucks as well.

141
00:13:29,000 --> 00:13:32,000
And I've got cards for both of those places.

142
00:13:32,000 --> 00:13:36,000
But the juice and the sandwiches or whatever.

143
00:13:36,000 --> 00:13:39,000
You have to pick them up by yourself.

144
00:13:39,000 --> 00:13:42,000
I still have to be given the food.

145
00:13:42,000 --> 00:13:44,000
It's another one of our rules.

146
00:13:44,000 --> 00:13:50,000
If a person doesn't give the food to me, I can't eat it.

147
00:13:50,000 --> 00:13:52,000
So.

148
00:13:52,000 --> 00:13:56,000
So maybe that's not the best idea after all.

149
00:13:56,000 --> 00:13:58,000
I was going to say something right away, but then I thought,

150
00:13:58,000 --> 00:14:01,000
well, maybe they probably have a deli, right?

151
00:14:01,000 --> 00:14:03,000
And then the deli they offer, they give it to you.

152
00:14:03,000 --> 00:14:07,000
And the deli is the best enemy because the food is already.

153
00:14:07,000 --> 00:14:09,000
Good.

154
00:14:09,000 --> 00:14:10,000
Prepared.

155
00:14:10,000 --> 00:14:11,000
It is.

156
00:14:11,000 --> 00:14:12,000
It is.

157
00:14:12,000 --> 00:14:13,000
Yes.

158
00:14:13,000 --> 00:14:22,000
I guess it's more expensive.

159
00:14:22,000 --> 00:14:24,000
But yeah, that wouldn't be it.

160
00:14:24,000 --> 00:14:29,000
All I can do is go to the deli.

161
00:14:29,000 --> 00:14:34,000
Unless someone went with me.

162
00:14:34,000 --> 00:14:39,000
But, you know, I mean, it's getting a little bit weird.

163
00:14:39,000 --> 00:14:42,000
I mean, what I want to move towards, of course,

164
00:14:42,000 --> 00:14:43,000
is going on Armstrong.

165
00:14:43,000 --> 00:14:47,000
That's the eventual key.

166
00:14:47,000 --> 00:14:52,000
When there's a community here and they want to keep me alive,

167
00:14:52,000 --> 00:14:57,000
they will feed me.

168
00:14:57,000 --> 00:15:04,000
But right now, our community is digital.

169
00:15:04,000 --> 00:15:13,000
We're not allowed to move food.

170
00:15:13,000 --> 00:15:16,000
I couldn't pick it up and put it on the register.

171
00:15:16,000 --> 00:15:19,000
If I touch it, if I touch the food, I can never,

172
00:15:19,000 --> 00:15:21,000
I commit an offense.

173
00:15:21,000 --> 00:15:23,000
If I touch food that hasn't been offered, apparently,

174
00:15:23,000 --> 00:15:26,000
I commit a very, very minor offense.

175
00:15:26,000 --> 00:15:33,000
And I'm not allowed to eat that food, even if it's later offered correctly.

176
00:15:33,000 --> 00:15:36,000
That food is then disqualified.

177
00:15:36,000 --> 00:15:43,000
If I even touch it.

178
00:15:43,000 --> 00:15:44,000
Yeah.

179
00:15:44,000 --> 00:15:45,000
I mean, there's no look.

180
00:15:45,000 --> 00:15:46,000
You can't find loopholes.

181
00:15:46,000 --> 00:15:53,000
That's not the point.

182
00:15:53,000 --> 00:15:58,000
It's kind of a, it's a bit of a board of gray area having someone

183
00:15:58,000 --> 00:16:01,000
offer it to you who isn't a donor.

184
00:16:01,000 --> 00:16:02,000
But that's okay.

185
00:16:02,000 --> 00:16:05,000
We considered, again, it's on behalf of the donor.

186
00:16:05,000 --> 00:16:09,000
So if I go to a restaurant with the card and the cashier hands me

187
00:16:09,000 --> 00:16:12,000
the sandwich, we consider they're doing it on behalf.

188
00:16:12,000 --> 00:16:14,000
I still love the person who donated.

189
00:16:14,000 --> 00:16:15,000
And that's fine.

190
00:16:15,000 --> 00:16:17,000
It's obviously not as awesome.

191
00:16:17,000 --> 00:16:19,000
If it's not as awesome as someone,

192
00:16:19,000 --> 00:16:24,000
hey, I'd like to give you the sandwich, but on behalf is allowed,

193
00:16:24,000 --> 00:16:31,000
I think.

194
00:16:31,000 --> 00:16:36,000
If food is on display, wouldn't it therefore be offered?

195
00:16:36,000 --> 00:16:37,000
No.

196
00:16:37,000 --> 00:16:40,000
No, there's a very technical explanation of what offered means.

197
00:16:40,000 --> 00:16:45,000
It has to be given actually physically given.

198
00:16:45,000 --> 00:16:49,000
Placed into the hands or something connected with the hands,

199
00:16:49,000 --> 00:16:53,000
receiving by the hands or something connected to the body.

200
00:16:53,000 --> 00:17:00,000
So the bowl is allowed because you're holding it.

201
00:17:00,000 --> 00:17:05,000
And the thing that it's connected to the body has to be

202
00:17:05,000 --> 00:17:06,000
liftable.

203
00:17:06,000 --> 00:17:08,000
Like a table is potentially okay.

204
00:17:08,000 --> 00:17:12,000
If you, right, so if you offer someone a table,

205
00:17:12,000 --> 00:17:15,000
you can lift the table and hand it to them.

206
00:17:15,000 --> 00:17:19,000
But it has to actually be offered.

207
00:17:19,000 --> 00:17:22,000
Like, like, do it.

208
00:17:22,000 --> 00:17:30,000
Given, given.

209
00:17:30,000 --> 00:17:32,000
On a plus note,

210
00:17:32,000 --> 00:17:37,000
I now have a picture of my teacher.

211
00:17:37,000 --> 00:17:40,000
Conveniently credit card sized.

212
00:17:40,000 --> 00:17:45,000
So I figured out how to transfer balance from one subway card to another.

213
00:17:45,000 --> 00:17:48,000
And so this is now just a picture of my teacher.

214
00:17:48,000 --> 00:17:50,000
It's no longer a subway card.

215
00:17:50,000 --> 00:17:52,000
And this one as well.

216
00:17:52,000 --> 00:17:53,000
So thank you.

217
00:17:53,000 --> 00:17:56,000
People who sent me pictures wouldn't recommend doing it again

218
00:17:56,000 --> 00:18:00,000
because it did have subway on it, but.

219
00:18:00,000 --> 00:18:04,000
Yeah.

220
00:18:04,000 --> 00:18:08,000
That was a good solution.

221
00:18:08,000 --> 00:18:10,000
Worked out.

222
00:18:10,000 --> 00:18:15,000
Yeah.

223
00:18:15,000 --> 00:18:17,000
So no questions tonight.

224
00:18:17,000 --> 00:18:21,000
One question.

225
00:18:21,000 --> 00:18:22,000
All right.

226
00:18:22,000 --> 00:18:28,000
Then I'm calling it a day because it's been a long one.

227
00:18:28,000 --> 00:18:30,000
Good night.

228
00:18:30,000 --> 00:18:32,000
Thank you, Bante.

229
00:18:32,000 --> 00:18:38,000
And oh, and we have, I didn't, did I mention that we have morning sessions now.

230
00:18:38,000 --> 00:18:41,000
7 a.m. to 7 30 might 7 a.m. and 7 30.

231
00:18:41,000 --> 00:18:46,000
So if you're looking to do a meditation course and you have to be committed.

232
00:18:46,000 --> 00:18:49,000
Go to the meet page.

233
00:18:49,000 --> 00:18:54,000
Meditation.serimungalow.org front slash appointment.php.

234
00:18:54,000 --> 00:18:57,000
But there's the meet link in the top.

235
00:18:57,000 --> 00:18:58,000
You'll see the schedule.

236
00:18:58,000 --> 00:19:00,000
Oh, it's mostly full already.

237
00:19:00,000 --> 00:19:05,000
But there's still some 7 a.m. slots.

238
00:19:05,000 --> 00:19:09,000
11 a.m. UTC.

239
00:19:09,000 --> 00:19:13,000
I'm sorry, that's about the best I can do time wise.

240
00:19:13,000 --> 00:19:16,000
Because I'm doing other things.

241
00:19:16,000 --> 00:19:18,000
Oh, one thing we can try.

242
00:19:18,000 --> 00:19:22,000
Let's see how many people we can get into the general purpose group lounge.

243
00:19:22,000 --> 00:19:26,000
So everybody go over to the meet page, the appointment page.

244
00:19:26,000 --> 00:19:27,000
Right.

245
00:19:27,000 --> 00:19:35,000
And then click on the green link that says click here to enter the general purpose group lounge.

246
00:19:35,000 --> 00:19:39,000
And then give it permission to use your webcam and mic.

247
00:19:39,000 --> 00:19:42,000
Hopefully some of you have webcams and mics.

248
00:19:42,000 --> 00:19:49,000
And I'm going to end this hangout and I'm going to go in there and Robin, if you can go in as well.

249
00:19:49,000 --> 00:19:51,000
I'm already in.

250
00:19:51,000 --> 00:19:57,000
As your webcam, but it may not let you display because you're in the hangout.

251
00:19:57,000 --> 00:19:59,000
It may not actually connect you.

252
00:19:59,000 --> 00:20:02,000
It seemed to be in both.

253
00:20:02,000 --> 00:20:04,000
It may be works.

254
00:20:04,000 --> 00:20:05,000
I don't know.

255
00:20:05,000 --> 00:20:07,000
Anyway, okay, I'm going to go over there.

256
00:20:07,000 --> 00:20:09,000
I'm going to bronze stop this broadcast.

257
00:20:09,000 --> 00:20:10,000
Thank you, Dante.

258
00:20:10,000 --> 00:20:24,000
So thank you, Robin.

259
00:20:24,000 --> 00:20:33,000
Good night.

