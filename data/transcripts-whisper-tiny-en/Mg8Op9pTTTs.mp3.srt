1
00:00:00,000 --> 00:00:08,500
That's a part of me that won't, sorry, that's a part of me that wants to give up every day.

2
00:00:08,500 --> 00:00:20,000
I like being depressed, but at the same time I know I shouldn't be like this, I don't know what to do.

3
00:00:20,000 --> 00:00:30,000
I want to give up, I assume you mean that in a bad way, like wants to kill yourself, wants to give up trying, and so on.

4
00:00:30,000 --> 00:00:34,000
Because actually giving up is a really good thing.

5
00:00:34,000 --> 00:00:40,000
I had one student once, and she was really difficult.

6
00:00:40,000 --> 00:00:45,000
And, well I pushed her, I made her really difficult for her.

7
00:00:45,000 --> 00:00:48,000
She came to me and she said I want to kill myself.

8
00:00:48,000 --> 00:00:52,000
I said good, good, kill yourself, and then keep practicing.

9
00:00:52,000 --> 00:00:55,000
And I said this to her, ended up saying this to her several times.

10
00:00:55,000 --> 00:00:59,000
She said, oh I want to die, yes, he has die first, and then continue practicing.

11
00:00:59,000 --> 00:01:00,000
Then start again.

12
00:01:00,000 --> 00:01:08,000
And then I would explain what I mean, you know, dying is giving up the idea of self, giving up all of this stuff that you're clinging to.

13
00:01:08,000 --> 00:01:09,000
Let go.

14
00:01:09,000 --> 00:01:11,000
Yeah, letting go is good.

15
00:01:11,000 --> 00:01:16,000
There was one monkey said, well you know the Buddha was pretty bummed out before he went for it.

16
00:01:16,000 --> 00:01:19,000
So it's not wrong to be depressed.

17
00:01:19,000 --> 00:01:22,000
You're depressed about things that are pretty depressing.

18
00:01:22,000 --> 00:01:35,000
Generally, depressed about our inability to fit in with people who are interested in what turned out to be quite useless and even harmful things.

19
00:01:35,000 --> 00:01:45,000
We're depressed that we can't succeed in a world where success is measured by our ability to defeat other people.

20
00:01:45,000 --> 00:01:57,000
We are depressed about the meaninglessness of a life and a world which turns existence, which turns out to be quite meaningless.

21
00:01:57,000 --> 00:01:59,000
And so giving up is great.

22
00:01:59,000 --> 00:02:10,000
Giving up in the same vein as someone would kill themselves is actually if channeled in the right direction is a great thing because killing yourself is in the answer.

23
00:02:10,000 --> 00:02:15,000
Unfortunately, when you die, you're just born again. The mind doesn't go anywhere. They're still clinging.

24
00:02:15,000 --> 00:02:25,000
It's not killing yourself. Killing yourself is a great thing because truly killing yourself is on the mental front.

25
00:02:25,000 --> 00:02:27,000
It's the killing of the self.

26
00:02:27,000 --> 00:02:41,000
The idea that there is some reality, the idea that this is who I am, this me who lives in society is somehow real.

27
00:02:41,000 --> 00:02:47,000
Killing yourself doesn't come from destroying the body. That's not killing the self.

28
00:02:47,000 --> 00:02:49,000
Killing the self is letting go.

29
00:02:49,000 --> 00:03:05,000
When you let go and when you live your life for a moment to moment without thinking about the past or the future, without worrying about who you should be or who you could be.

30
00:03:05,000 --> 00:03:14,000
Then you will have no suffering.

31
00:03:14,000 --> 00:03:19,000
So that's the first thing I would say.

32
00:03:19,000 --> 00:03:26,000
The second part, it's like I like being depressed. This is a danger because you get...

33
00:03:26,000 --> 00:03:30,000
We rather than solve the problem.

34
00:03:30,000 --> 00:03:42,000
We content ourselves with wallowing in self-pity, unconsciously reassuring ourselves that we're doing our duty.

35
00:03:42,000 --> 00:03:50,000
Being depressed is an answer, in a sense. It becomes our answer.

36
00:03:50,000 --> 00:03:55,000
Yes, yes. I know things are horrible. That's why I'm depressed.

37
00:03:55,000 --> 00:04:00,000
As though that were a viable solution for the problem.

38
00:04:00,000 --> 00:04:05,000
On that stage, you have no benefit in being depressed and you have to see that.

39
00:04:05,000 --> 00:04:12,000
This is all you have to do is meditate on the depression and you'll see how meaningless it is.

40
00:04:12,000 --> 00:04:13,000
You'll see how useless it is.

41
00:04:13,000 --> 00:04:19,000
Look at the depression. You'll see it disappear in a second, and you wonder like, what's wrong?

42
00:04:19,000 --> 00:04:26,000
We miss a lot about meditation. We don't realize that it's actually working because you meditate on it and then disappears and you think,

43
00:04:26,000 --> 00:04:28,000
no, now what?

44
00:04:28,000 --> 00:04:31,000
You don't get it. That actually, it just worked. The depression's gone.

45
00:04:31,000 --> 00:04:40,000
Move on. We cling and we're comfortable with things that become comfortable in our suffering.

46
00:04:40,000 --> 00:04:43,000
That's dangerous. You have to be very clear with yourself.

47
00:04:43,000 --> 00:04:49,000
Not to stop the depression or even to worry about it or be upset about the depression,

48
00:04:49,000 --> 00:04:56,000
but to be honest with yourself about the depression because being upset about the depression is your way of defending it.

49
00:04:56,000 --> 00:05:02,000
It's your way of keeping yourself in it. You can even say, yes, yes, I know I shouldn't be depressed.

50
00:05:02,000 --> 00:05:09,000
Yes, I'm such an awful person who makes you more depressed and therefore you need less inclined to change.

51
00:05:09,000 --> 00:05:12,000
That's the second part.

52
00:05:12,000 --> 00:05:18,000
The third part where you say, at the same time, I know it shouldn't be like this.

53
00:05:18,000 --> 00:05:23,000
Well, the question is, how should it be then?

54
00:05:23,000 --> 00:05:33,000
And once it is that way, how can you keep it that way?

55
00:05:33,000 --> 00:05:40,000
Because you can't force things to be in a certain way or another.

56
00:05:40,000 --> 00:05:47,000
You have two choices. One, think things should be otherwise than what they are or two deal with the way they are.

57
00:05:47,000 --> 00:05:56,000
And I don't think there's much doubt about which is the better way.

58
00:05:56,000 --> 00:06:03,000
There can be no comparison between wishing that things were otherwise than they are and dealing with things as they are.

59
00:06:03,000 --> 00:06:05,000
Coming to terms with things as they are.

60
00:06:05,000 --> 00:06:10,000
My teacher always reminds us that Buddha always reminded us, don't worry about the past.

61
00:06:10,000 --> 00:06:13,000
Don't worry about the future. Make the present moment the best it can be.

62
00:06:13,000 --> 00:06:19,000
This is how my teacher always puts it. Make the present moment the best it can be.

63
00:06:19,000 --> 00:06:24,000
Again and again he reminds us and that's really all it is.

64
00:06:24,000 --> 00:06:27,000
You don't have to solve the past. You don't have to solve the future.

65
00:06:27,000 --> 00:06:29,000
You don't have to make things better.

66
00:06:29,000 --> 00:06:34,000
Because when you cling to your idea of the way things are better, it will one day be like that.

67
00:06:34,000 --> 00:06:37,000
And then you'll lose it and then it will change again.

68
00:06:37,000 --> 00:06:40,000
You cling to it and then it won't be like that again.

69
00:06:40,000 --> 00:06:53,000
This very thing that what you have to do is learn to or remind yourself that see this as a lesson that things can change in this way.

70
00:06:53,000 --> 00:06:59,000
Things can be the way you wish and later not be the way you wish.

71
00:06:59,000 --> 00:07:07,000
And so admit to yourself that you need a better way.

72
00:07:07,000 --> 00:07:12,000
You can't rely on things to be the way you want.

73
00:07:12,000 --> 00:07:19,000
So whether you know that things, what you mean by that is that they should be, you should be like an enlightened being or not.

74
00:07:19,000 --> 00:07:25,000
The way to enlightenment is not by wishing or thinking about how things could be.

75
00:07:25,000 --> 00:07:28,000
That's about dealing with the way things are. So deal with your depression.

76
00:07:28,000 --> 00:07:32,000
Look at it. Live with study it.

77
00:07:32,000 --> 00:07:38,000
Study it like it were a project of yours. But you were depressed on purpose just so you could learn about it.

78
00:07:38,000 --> 00:07:42,000
They don't be depressed on purpose, but think of it as just how it should be.

79
00:07:42,000 --> 00:07:44,000
Yes, yes. This is important.

80
00:07:44,000 --> 00:07:48,000
It's important for me to be depressed so I can learn about depression.

81
00:07:48,000 --> 00:07:52,000
We're not to the extent that you encourage it because you shouldn't encourage anything.

82
00:07:52,000 --> 00:08:00,000
You should just learn about what you learn about what you understand about everything.

83
00:08:00,000 --> 00:08:04,000
Because what is is, and that's all that is.

