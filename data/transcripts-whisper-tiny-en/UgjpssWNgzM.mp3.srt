1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhampada.

2
00:00:05,000 --> 00:00:13,000
So today we are continuing with verse 254 and verse 255,

3
00:00:13,000 --> 00:00:17,000
which we thus follows.

4
00:00:17,000 --> 00:00:34,000
The Dhampada is the name of the Dhampada.

5
00:00:34,000 --> 00:01:03,000
There is no path in the sky.

6
00:01:03,000 --> 00:01:11,000
There is no summoner outside.

7
00:01:11,000 --> 00:01:16,000
People delight in Babancha.

8
00:01:16,000 --> 00:01:21,000
But the Buddhas are free from Babancha.

9
00:01:21,000 --> 00:01:25,000
There is no path in the sky.

10
00:01:25,000 --> 00:01:29,000
There is no summoner outside.

11
00:01:29,000 --> 00:01:34,000
There is no formation, no sankara.

12
00:01:34,000 --> 00:01:39,000
That is eternal.

13
00:01:39,000 --> 00:01:52,000
And there is no shaking or vassal, no wavering of the Buddhas.

14
00:01:52,000 --> 00:01:58,000
So these verses are supposed to have been a part of the teaching that the Buddha gave to Subhadha.

15
00:01:58,000 --> 00:02:09,000
So it may be the first story where the story doesn't have anything to do with the actual content of the verses.

16
00:02:09,000 --> 00:02:12,000
The story goes that,

17
00:02:12,000 --> 00:02:16,000
Well, Subhadha is a special case.

18
00:02:16,000 --> 00:02:18,000
He was in a past life.

19
00:02:18,000 --> 00:02:28,000
He was a brother with another of the Buddhist disciples, Kundana.

20
00:02:28,000 --> 00:02:32,000
So when the Buddha first decided who to teach,

21
00:02:32,000 --> 00:02:39,000
he thought of the five ascetics who had cared for him and followed him

22
00:02:39,000 --> 00:02:45,000
and depended on him for their teachings, but who had abandoned him.

23
00:02:45,000 --> 00:02:48,000
And he went to find them, and Kundana was the eldest of them.

24
00:02:48,000 --> 00:02:56,000
And when the Buddha first taught them, Kundana was immediately,

25
00:02:56,000 --> 00:02:58,000
it attained salt upon it.

26
00:02:58,000 --> 00:03:01,000
So he immediately understood the Buddha's teaching,

27
00:03:01,000 --> 00:03:04,000
and attained the first stage of enlightenment.

28
00:03:04,000 --> 00:03:06,000
He was the first.

29
00:03:06,000 --> 00:03:11,000
And that's apparently because he had this sort of,

30
00:03:11,000 --> 00:03:19,000
what we call Upanishaya, he had this basis for it from past lives.

31
00:03:19,000 --> 00:03:24,000
So his character type was the sort to do things first.

32
00:03:24,000 --> 00:03:32,000
And there's a story where he was brothers with the person who would eventually become Subhadha.

33
00:03:32,000 --> 00:03:42,000
And together they grew rice, and they were also followers of some Buddha,

34
00:03:42,000 --> 00:03:46,000
or maybe they were religious people,

35
00:03:46,000 --> 00:03:52,000
so they were into giving alms to religious mendicants.

36
00:03:52,000 --> 00:03:59,000
And as soon as the rice sprouted,

37
00:03:59,000 --> 00:04:08,000
and more fruit, green fruit, it's called young rice.

38
00:04:08,000 --> 00:04:13,000
Kundana, the man who was to become Kundana, said to his brother,

39
00:04:13,000 --> 00:04:18,000
I think we should give the first part of our crop.

40
00:04:18,000 --> 00:04:22,000
He was in a hurry and give to religious people

41
00:04:22,000 --> 00:04:24,000
because you can eat the young rice.

42
00:04:24,000 --> 00:04:28,000
I guess you can make milk out of it or something.

43
00:04:28,000 --> 00:04:31,000
But the other, his brother said,

44
00:04:31,000 --> 00:04:34,000
shook his head and said, you're crazy.

45
00:04:34,000 --> 00:04:39,000
That's not, who's ever heard of giving young rice?

46
00:04:39,000 --> 00:04:41,000
Wait until it's way until it's done.

47
00:04:41,000 --> 00:04:43,000
Wait until the end of the harvest.

48
00:04:43,000 --> 00:04:46,000
Once we've harvested it and portioned it out,

49
00:04:46,000 --> 00:04:52,000
we'll set a portion aside and we'll give that portion as charity.

50
00:04:52,000 --> 00:04:56,000
Kundana said, no way.

51
00:04:56,000 --> 00:04:59,000
I think we should do things right away.

52
00:04:59,000 --> 00:05:01,000
This is my intention is to give.

53
00:05:01,000 --> 00:05:03,000
Be the first to give.

54
00:05:03,000 --> 00:05:07,000
Give the first part, before you take for yourself,

55
00:05:07,000 --> 00:05:11,000
in order to counter avarice, greed, stinginess,

56
00:05:11,000 --> 00:05:13,000
miserliness.

57
00:05:13,000 --> 00:05:16,000
Give first," he said.

58
00:05:16,000 --> 00:05:20,000
His brother said, give last.

59
00:05:20,000 --> 00:05:23,000
And then, so this is what they did.

60
00:05:23,000 --> 00:05:26,000
And Kundana, the younger brother said,

61
00:05:26,000 --> 00:05:29,000
in that case, we split our field in half.

62
00:05:29,000 --> 00:05:31,000
You give me half the field.

63
00:05:31,000 --> 00:05:32,000
You take half the field.

64
00:05:32,000 --> 00:05:35,000
And all the what I want, you do what you want.

65
00:05:35,000 --> 00:05:36,000
And so they did.

66
00:05:36,000 --> 00:05:39,000
So they were both quite generous and spiritual people.

67
00:05:39,000 --> 00:05:43,000
And they were both supportive of religious people.

68
00:05:43,000 --> 00:05:46,000
But Subadha gave first.

69
00:05:46,000 --> 00:05:48,000
Kundana gave first.

70
00:05:48,000 --> 00:05:50,000
And Subadha gave last.

71
00:05:50,000 --> 00:05:55,000
And so they say, this is the reason why Kundana was the first

72
00:05:55,000 --> 00:05:58,000
to understand the Buddha's teaching, but Subadha.

73
00:05:58,000 --> 00:06:01,000
Subadha came to see the Buddha when the Buddha was about

74
00:06:01,000 --> 00:06:03,000
to pass away.

75
00:06:03,000 --> 00:06:05,000
He was sick.

76
00:06:05,000 --> 00:06:09,000
He was resting, recovering from his illness.

77
00:06:09,000 --> 00:06:13,000
And Ananda forbid it, he said.

78
00:06:13,000 --> 00:06:15,000
The blessed one is sick.

79
00:06:15,000 --> 00:06:20,000
The blessed one is weak, he's resting.

80
00:06:20,000 --> 00:06:21,000
No chance.

81
00:06:21,000 --> 00:06:22,000
But the Buddha said, no.

82
00:06:22,000 --> 00:06:24,000
The Buddha said, let Subadha in.

83
00:06:24,000 --> 00:06:29,000
He understood that this was the time for Subadha at the very end.

84
00:06:29,000 --> 00:06:31,000
And so Ananda led him in.

85
00:06:31,000 --> 00:06:35,000
And this shows up in the Paridhi Banasut.

86
00:06:35,000 --> 00:06:38,000
There's a different teaching.

87
00:06:38,000 --> 00:06:41,000
So I guess we can understand that he taught more than one thing,

88
00:06:41,000 --> 00:06:42,000
which is, that's OK.

89
00:06:42,000 --> 00:06:45,000
We'll just understand it that way.

90
00:06:45,000 --> 00:06:53,000
In this Kama-pa-da, it is said that he taught this verse as well.

91
00:06:53,000 --> 00:06:55,000
He's two verses.

92
00:06:55,000 --> 00:06:56,000
And that's that.

93
00:06:56,000 --> 00:07:00,000
Apparently he asked questions in the Buddha answered these

94
00:07:00,000 --> 00:07:04,000
in response to his questions.

95
00:07:04,000 --> 00:07:06,000
So that's the story.

96
00:07:06,000 --> 00:07:08,000
Now, the lesson, let's talk about lessons.

97
00:07:08,000 --> 00:07:11,000
Before I get into the actual verse, let's talk about lessons.

98
00:07:11,000 --> 00:07:13,000
Let's get from the story.

99
00:07:13,000 --> 00:07:15,000
Because I think there are two important things

100
00:07:15,000 --> 00:07:17,000
to just point out.

101
00:07:17,000 --> 00:07:19,000
It's an interesting story.

102
00:07:19,000 --> 00:07:23,000
But it does raise a couple of interesting points.

103
00:07:23,000 --> 00:07:26,000
The first is don't wait.

104
00:07:26,000 --> 00:07:29,000
There is fruit to doing things first.

105
00:07:29,000 --> 00:07:32,000
You can take that lesson from it.

106
00:07:32,000 --> 00:07:33,000
Most people would probably say,

107
00:07:33,000 --> 00:07:35,000
Kundana, you had the right of it.

108
00:07:35,000 --> 00:07:37,000
Because why would you wait?

109
00:07:37,000 --> 00:07:39,000
If this is the fruit, wouldn't it be great to be the first?

110
00:07:39,000 --> 00:07:43,000
Or if not the first, early, who wants to be the last

111
00:07:43,000 --> 00:07:47,000
and have to risk that, right?

112
00:07:47,000 --> 00:07:50,000
It shows the connection there, that there is a potential connection

113
00:07:50,000 --> 00:07:52,000
if you're procrastinating.

114
00:07:52,000 --> 00:07:53,000
Certainly that's a bad thing.

115
00:07:53,000 --> 00:07:57,000
I think it's the idea is that Subhata didn't procrastinate.

116
00:07:57,000 --> 00:07:59,000
It was just his way.

117
00:07:59,000 --> 00:08:04,000
And as a result, you really shouldn't ultimately say

118
00:08:04,000 --> 00:08:06,000
that one is better than the other.

119
00:08:06,000 --> 00:08:08,000
But you can take from what you will.

120
00:08:08,000 --> 00:08:12,000
If you want early results, early do things early.

121
00:08:12,000 --> 00:08:15,000
Certainly don't procrastinate.

122
00:08:15,000 --> 00:08:18,000
But the other thing that you can point to from this

123
00:08:18,000 --> 00:08:22,000
is just that it's the way of different people.

124
00:08:22,000 --> 00:08:29,000
And so you should feel reassured and encouraged

125
00:08:29,000 --> 00:08:33,000
that you've all come when you're not at the end of your life.

126
00:08:33,000 --> 00:08:35,000
And I'm not at the end of my life.

127
00:08:35,000 --> 00:08:40,000
And so we're in good shape to practice.

128
00:08:40,000 --> 00:08:45,000
And I'm in good shape to help you practice.

129
00:08:45,000 --> 00:08:50,000
But on the other hand, sometimes there can be a sense

130
00:08:50,000 --> 00:08:53,000
of discouragement that it might be too late.

131
00:08:53,000 --> 00:09:04,000
And you maybe missed an opportunity feeling that feeling

132
00:09:04,000 --> 00:09:09,000
discouraged when you see others who may be become proficient

133
00:09:09,000 --> 00:09:12,000
in the practice more quickly.

134
00:09:12,000 --> 00:09:15,000
For some practice is slow, for some it is quick.

135
00:09:15,000 --> 00:09:18,000
And there's no sense that even one is better than the other

136
00:09:18,000 --> 00:09:21,000
because to some extent it's just our way.

137
00:09:21,000 --> 00:09:24,000
Some people will take many lives.

138
00:09:24,000 --> 00:09:29,000
Even Mahamogalana took less time than Sariputa.

139
00:09:29,000 --> 00:09:31,000
The two Buddhist, two cheap disciples.

140
00:09:31,000 --> 00:09:34,000
And the one who has said to be the greater of the two,

141
00:09:34,000 --> 00:09:36,000
took longer.

142
00:09:36,000 --> 00:09:42,000
So the story of Subadha and Khodanya gives us this idea

143
00:09:42,000 --> 00:09:47,000
or is an example of this idea of everything in its time.

144
00:09:47,000 --> 00:09:49,000
People have different paths.

145
00:09:49,000 --> 00:09:53,000
And rather than comparing ourselves with conceit,

146
00:09:53,000 --> 00:09:56,000
thinking ourselves better or worse,

147
00:09:56,000 --> 00:10:03,000
feeling disappointed in ourselves or discouraged

148
00:10:03,000 --> 00:10:07,000
by our difficulties in the practice,

149
00:10:07,000 --> 00:10:09,000
to understand that things come in their time.

150
00:10:09,000 --> 00:10:11,000
Don't push too hard.

151
00:10:11,000 --> 00:10:14,000
Sometimes we try and force it and we get frustrated.

152
00:10:14,000 --> 00:10:18,000
When results don't come the way we want,

153
00:10:18,000 --> 00:10:23,000
you can just think of Khodanya and Subadha who got their results

154
00:10:23,000 --> 00:10:28,000
when they should, we don't know what our past karma is.

155
00:10:28,000 --> 00:10:33,000
So we try and work with what we have.

156
00:10:33,000 --> 00:10:34,000
So that's the story.

157
00:10:34,000 --> 00:10:37,000
Now the verse itself is, I guess,

158
00:10:37,000 --> 00:10:42,000
a little more profound and has some more profound lessons for us.

159
00:10:42,000 --> 00:10:47,000
It's a little, I think, difficult to understand the verse

160
00:10:47,000 --> 00:10:49,000
without making the connection.

161
00:10:49,000 --> 00:10:52,000
So there are some connections you have to make,

162
00:10:52,000 --> 00:10:54,000
especially with the first part.

163
00:10:54,000 --> 00:10:56,000
Now the first part of the verses,

164
00:10:56,000 --> 00:11:00,000
it repeats in the two verses, the first part is repeated.

165
00:11:00,000 --> 00:11:04,000
But the first quarter of the verse

166
00:11:04,000 --> 00:11:07,000
says that there is no path in the air,

167
00:11:07,000 --> 00:11:10,000
which taken by itself seems like a crazy thing to say.

168
00:11:10,000 --> 00:11:12,000
I mean, it's not wrong,

169
00:11:12,000 --> 00:11:15,000
but it seems like a complete non sequitur.

170
00:11:15,000 --> 00:11:17,000
What does this have to do with the Buddha's teaching?

171
00:11:17,000 --> 00:11:20,000
Why are you teaching this?

172
00:11:20,000 --> 00:11:25,000
So the way it should be understood,

173
00:11:25,000 --> 00:11:30,000
I think, is in conjunction with the second part.

174
00:11:30,000 --> 00:11:34,000
There's no path in the air.

175
00:11:34,000 --> 00:11:36,000
It's actually another way of saying

176
00:11:36,000 --> 00:11:39,000
that there is no summoner outside.

177
00:11:39,000 --> 00:11:42,000
A summoner outside may sound strange as well,

178
00:11:42,000 --> 00:11:45,000
but there's a simple explanation there.

179
00:11:45,000 --> 00:11:49,000
Outside is just a word that means outside of the Buddha's teaching.

180
00:11:49,000 --> 00:11:53,000
It's the way they often just especially in poetry

181
00:11:53,000 --> 00:11:59,000
refer to the meaning of the word as a part from our religion.

182
00:12:03,000 --> 00:12:07,000
A summoner, the word summoner is a word I leave untranslated

183
00:12:07,000 --> 00:12:09,000
just because I don't have a great word for it

184
00:12:09,000 --> 00:12:12,000
because I want you to understand that it's another

185
00:12:12,000 --> 00:12:13,000
fairly simple word.

186
00:12:13,000 --> 00:12:17,000
It just means a person who is spiritual.

187
00:12:17,000 --> 00:12:20,000
It's somewhere the word shaman comes from.

188
00:12:20,000 --> 00:12:22,000
So the Buddha used it.

189
00:12:22,000 --> 00:12:26,000
In some cases, just to refer to any religious person,

190
00:12:26,000 --> 00:12:28,000
but here is actually, of course,

191
00:12:28,000 --> 00:12:31,000
talking about something a lot deeper.

192
00:12:31,000 --> 00:12:35,000
So the Buddha says,

193
00:12:35,000 --> 00:12:38,000
there is no enlightened person.

194
00:12:38,000 --> 00:12:40,000
When he talks about summoner,

195
00:12:40,000 --> 00:12:43,000
he means there is no enlightened person outside.

196
00:12:43,000 --> 00:12:50,000
He is what in the Mahan Parnibanasu to the Buddha is said to have taught

197
00:12:50,000 --> 00:12:53,000
that the Buddha asked him whether other religions

198
00:12:53,000 --> 00:12:56,000
had enlightened beings in the Buddha said,

199
00:12:56,000 --> 00:13:00,000
there are no summoner outside of the Buddha's teaching.

200
00:13:00,000 --> 00:13:03,000
He said, in whatever sassana,

201
00:13:03,000 --> 00:13:06,000
there is the eight full noble path.

202
00:13:06,000 --> 00:13:10,000
When you have right view, right thought, and so on.

203
00:13:10,000 --> 00:13:15,000
Then there will be a soda Parnasakatagami Anagami Aran,

204
00:13:15,000 --> 00:13:17,000
there will be enlightened beings.

205
00:13:17,000 --> 00:13:24,000
But he said, I don't see any religion outside of Buddhism that has that.

206
00:13:24,000 --> 00:13:26,000
And this is where the first part comes in

207
00:13:26,000 --> 00:13:28,000
because this is, of course,

208
00:13:28,000 --> 00:13:31,000
unsurprising but not very reassuring to people

209
00:13:31,000 --> 00:13:33,000
who are new to Buddhism.

210
00:13:33,000 --> 00:13:36,000
Unsurprising because, well, it's what every religion says, right?

211
00:13:36,000 --> 00:13:39,000
Our religion is the best. Everybody else is wrong.

212
00:13:39,000 --> 00:13:43,000
We're right there, everyone else is wrong.

213
00:13:43,000 --> 00:13:47,000
Then it's so not very reassuring because it sounds like a boast.

214
00:13:47,000 --> 00:13:50,000
It sounds like an unsupported claim.

215
00:13:50,000 --> 00:13:56,000
It even can be disappointing to some people who want to find similarities in religions.

216
00:13:56,000 --> 00:14:01,000
And to some extent, it's a bit muddied in modern times

217
00:14:01,000 --> 00:14:06,000
because, of course, what we call Hinduism was influenced by Buddhism.

218
00:14:06,000 --> 00:14:10,000
It's not Buddhism, but it was certainly influenced by it.

219
00:14:10,000 --> 00:14:15,000
And because Buddhism is many different things now, right?

220
00:14:15,000 --> 00:14:23,000
There is even secular movements that drop on the Buddhist teaching.

221
00:14:23,000 --> 00:14:27,000
Psychotherapy, that doesn't claim to be religious,

222
00:14:27,000 --> 00:14:32,000
often incorporates the sorts of practices that we do.

223
00:14:32,000 --> 00:14:37,000
So it's muddied by the fact that mindfulness is now a part of modern discourse.

224
00:14:37,000 --> 00:14:42,000
Ordinary discourse, people talk about mindfulness, not in the original sense of the word,

225
00:14:42,000 --> 00:14:45,000
but as a meditation practice.

226
00:14:45,000 --> 00:14:50,000
As a means of cultivating objectivity in a meditative sense.

227
00:14:50,000 --> 00:14:55,000
But at the time of the Buddha, and even in modern times,

228
00:14:55,000 --> 00:14:59,000
if we talk about the differences between religions,

229
00:14:59,000 --> 00:15:03,000
Buddhism was exceptional and is exceptional.

230
00:15:03,000 --> 00:15:05,000
And it relates to the first part.

231
00:15:05,000 --> 00:15:08,000
That there is no path in the sky.

232
00:15:08,000 --> 00:15:16,000
I think that's a very powerful imagery to talk about the second part that

233
00:15:16,000 --> 00:15:18,000
just as there is no path in the sky.

234
00:15:18,000 --> 00:15:24,000
So too, you're not going to find an enlightened being outside of the Buddha's asana

235
00:15:24,000 --> 00:15:26,000
because they're just in the sky.

236
00:15:26,000 --> 00:15:31,000
It's not grounded as what we would say in English.

237
00:15:31,000 --> 00:15:34,000
So there are some obvious examples of this.

238
00:15:34,000 --> 00:15:39,000
The idea that the way to be free from suffering is belief in God, for example.

239
00:15:39,000 --> 00:15:44,000
Well, that's like saying, hey, look at that path in the sky.

240
00:15:44,000 --> 00:15:47,000
There's nothing there.

241
00:15:47,000 --> 00:15:49,000
There's no connection.

242
00:15:49,000 --> 00:15:51,000
There's no evidence.

243
00:15:51,000 --> 00:15:54,000
There's no basis for that.

244
00:15:54,000 --> 00:15:59,000
And you can say it's not grounded in reality.

245
00:15:59,000 --> 00:16:04,000
So what is exceptional about Buddhism is the grounding in experience.

246
00:16:04,000 --> 00:16:06,000
Right?

247
00:16:06,000 --> 00:16:12,000
Even take somewhat to meditation, or any tradition that talks about absorption,

248
00:16:12,000 --> 00:16:17,000
like transcendental paths, or a lot of Hindu meditative paths,

249
00:16:17,000 --> 00:16:23,000
that transcend the illusion of experience, for example.

250
00:16:23,000 --> 00:16:29,000
So because they are transcendental, they're dealing with a idea.

251
00:16:29,000 --> 00:16:37,000
They're dealing with still often God, but quite often dealing with a single object,

252
00:16:37,000 --> 00:16:40,000
a thing that you focus on.

253
00:16:40,000 --> 00:16:46,000
And they're not at all connected with actual experience,

254
00:16:46,000 --> 00:16:48,000
the things that lead to suffering.

255
00:16:48,000 --> 00:16:53,000
The idea is you escape it, and somehow that is a permanent freedom,

256
00:16:53,000 --> 00:16:56,000
but it's not because it doesn't address.

257
00:16:56,000 --> 00:17:08,000
It's not grounded in the issues of suffering and the cause of suffering.

258
00:17:08,000 --> 00:17:18,000
There's this story of an allegory, I guess, of a joke even,

259
00:17:18,000 --> 00:17:24,000
of someone who is outside looking for something.

260
00:17:24,000 --> 00:17:27,000
And someone comes and says, what are you doing?

261
00:17:27,000 --> 00:17:34,000
Oh, I lost my glasses, or I lost my contact lens, so they can't even see.

262
00:17:34,000 --> 00:17:40,000
And the person starts to help them and says, well, where exactly did you lose it?

263
00:17:40,000 --> 00:17:42,000
Oh, I lost it inside.

264
00:17:42,000 --> 00:17:44,000
Well, why are you looking out here for it?

265
00:17:44,000 --> 00:17:49,000
Oh, because the light is better out here.

266
00:17:49,000 --> 00:18:00,000
They use this as a term for looking in a place that is convenient.

267
00:18:00,000 --> 00:18:06,000
And it relates here because God is convenient.

268
00:18:06,000 --> 00:18:08,000
Some at a meditation is convenient.

269
00:18:08,000 --> 00:18:10,000
It's comfortable.

270
00:18:10,000 --> 00:18:14,000
You're not challenged when you're dealing with concepts.

271
00:18:14,000 --> 00:18:17,000
When you have something you can cling to.

272
00:18:17,000 --> 00:18:21,000
Even if it's just a color or a light, a flight candle flame,

273
00:18:21,000 --> 00:18:24,000
if someone focuses on that.

274
00:18:24,000 --> 00:18:29,000
You're not challenged in the way that you're challenged when you have to focus on your experiences,

275
00:18:29,000 --> 00:18:32,000
when you have to actually take pain as a meditation object.

276
00:18:32,000 --> 00:18:37,000
It seems absurd, horrifying that you should have to do that.

277
00:18:37,000 --> 00:18:39,000
Be present with your emotions.

278
00:18:39,000 --> 00:18:41,000
Don't run away from them.

279
00:18:41,000 --> 00:18:44,000
You don't try and escape them.

280
00:18:44,000 --> 00:18:46,000
It's not something we're used to.

281
00:18:46,000 --> 00:18:50,000
We're used to fight or flight, fix or fleeing.

282
00:18:50,000 --> 00:18:57,000
Make it better or find something better.

283
00:18:57,000 --> 00:19:00,000
So we look in all these different places,

284
00:19:00,000 --> 00:19:03,000
but it's like looking for a path in the sky.

285
00:19:03,000 --> 00:19:06,000
Looking for your contact lens in a place that you didn't lose it.

286
00:19:06,000 --> 00:19:09,000
It's like looking for a path in the sky.

287
00:19:09,000 --> 00:19:14,000
It's like looking for enlightenment outside of actual experience.

288
00:19:14,000 --> 00:19:23,000
So we can say with quite complete certainty.

289
00:19:23,000 --> 00:19:27,000
We can appreciate even as newcomers.

290
00:19:27,000 --> 00:19:31,000
You can appreciate the difference.

291
00:19:31,000 --> 00:19:38,000
You see immediately the difference between focusing on reality like the Buddha explained

292
00:19:38,000 --> 00:19:48,000
and trying to find freedom in some magical conceptual experience.

293
00:19:48,000 --> 00:19:51,000
So that's the first two parts.

294
00:19:51,000 --> 00:19:56,000
The third part, the third part of the first verse,

295
00:19:56,000 --> 00:20:00,000
or the third and fourth parts they go together,

296
00:20:00,000 --> 00:20:05,000
is that people delight in Babancha.

297
00:20:05,000 --> 00:20:08,000
And I didn't translate this one either.

298
00:20:08,000 --> 00:20:11,000
People delight in Babancha,

299
00:20:11,000 --> 00:20:15,000
but Buddhas are free from Babancha or tatagatas.

300
00:20:15,000 --> 00:20:18,000
Those who are thus gone, those who have gone in this way,

301
00:20:18,000 --> 00:20:23,000
those who have traveled this path are free from Babancha.

302
00:20:23,000 --> 00:20:27,000
And Babancha is a word that gives me trouble.

303
00:20:27,000 --> 00:20:37,000
I know there's a movement to sort of translate it as mental proliferation.

304
00:20:37,000 --> 00:20:41,000
But I don't want to go into too much detail.

305
00:20:41,000 --> 00:20:44,000
I think that's a great way to understand it.

306
00:20:44,000 --> 00:20:52,000
Babancha is translated sometimes as an obstacle or an obstruction of hindrance.

307
00:20:52,000 --> 00:20:56,000
It's translated as diffusion.

308
00:20:56,000 --> 00:20:59,000
And it's also translated as obsession.

309
00:20:59,000 --> 00:21:08,000
It appears to be getting entangled, getting caught up in something.

310
00:21:08,000 --> 00:21:10,000
It's not a good mind state.

311
00:21:10,000 --> 00:21:13,000
It's a state of entanglement, let's say.

312
00:21:13,000 --> 00:21:17,000
It means mental proliferation they translate it as.

313
00:21:17,000 --> 00:21:22,000
It's sort of a modern or a Western, maybe a modern anyway.

314
00:21:22,000 --> 00:21:25,000
We have to have translating it.

315
00:21:25,000 --> 00:21:27,000
What exactly it means.

316
00:21:27,000 --> 00:21:30,000
It definitely is a hindrance, so it's not a good thing.

317
00:21:30,000 --> 00:21:37,000
It seems to involve some crossing some line, so it's related to obsession.

318
00:21:37,000 --> 00:21:41,000
But people delight in it.

319
00:21:41,000 --> 00:21:44,000
So no one delights in hindrances.

320
00:21:44,000 --> 00:21:47,000
We delight in things that are hindrances.

321
00:21:47,000 --> 00:21:52,000
And Babancha is what comes after thinking.

322
00:21:52,000 --> 00:21:57,000
So when you think about things, you fall into Babancha,

323
00:21:57,000 --> 00:22:02,000
which means it makes sense to talk about it as this proliferation.

324
00:22:02,000 --> 00:22:12,000
It relates very much, I think, to the first part.

325
00:22:12,000 --> 00:22:17,000
How the Buddha's teaching is very grounded in reality.

326
00:22:17,000 --> 00:22:20,000
And it's what makes the practice of mindfulness.

327
00:22:20,000 --> 00:22:24,000
Let's say very different from even the practice of somewhat

328
00:22:24,000 --> 00:22:25,000
to meditation.

329
00:22:25,000 --> 00:22:31,000
So this is useful as a lesson for people on a meditation course.

330
00:22:31,000 --> 00:22:39,000
To get a sense of that distinction between objective observation of experience

331
00:22:39,000 --> 00:22:43,000
and this diffuse state of mind, the proliferation,

332
00:22:43,000 --> 00:22:48,000
which I think means that Babancha, a good definition of it,

333
00:22:48,000 --> 00:22:52,000
is making more out of things than they actually are.

334
00:22:52,000 --> 00:22:54,000
That's how I like to talk about it.

335
00:22:54,000 --> 00:22:57,000
I think it's an important part of the problem.

336
00:22:57,000 --> 00:23:02,000
And so I think it fits well with the idea of Babancha.

337
00:23:02,000 --> 00:23:06,000
Making more out of things than they actually are.

338
00:23:06,000 --> 00:23:09,000
It covers so much, right?

339
00:23:09,000 --> 00:23:13,000
If you like something,

340
00:23:13,000 --> 00:23:17,000
you absolutely have made more out of the thing than it actually is.

341
00:23:17,000 --> 00:23:23,000
There's nothing intrinsically likable about experiences.

342
00:23:23,000 --> 00:23:29,000
Seeing, hearing, smelling, tasting, feeling, thinking.

343
00:23:29,000 --> 00:23:31,000
They are what they are.

344
00:23:31,000 --> 00:23:34,000
Seeing is just seeing, hearing is just hearing.

345
00:23:34,000 --> 00:23:38,000
And it gives you an idea of why we practice mindfulness

346
00:23:38,000 --> 00:23:39,000
in the first place.

347
00:23:39,000 --> 00:23:43,000
Why are you repeating a mantra to yourself?

348
00:23:43,000 --> 00:23:47,000
It's because the word mindfulness, of course, the word sati

349
00:23:47,000 --> 00:23:53,000
actually translates to something more like remembering.

350
00:23:53,000 --> 00:23:55,000
And what do we mean by that?

351
00:23:55,000 --> 00:24:03,000
I mean remembering what things are instead of forgetting that

352
00:24:03,000 --> 00:24:07,000
and getting lost and what we think they are.

353
00:24:07,000 --> 00:24:11,000
This happy feeling is good.

354
00:24:11,000 --> 00:24:13,000
This painful feeling is bad.

355
00:24:13,000 --> 00:24:14,000
Good and bad.

356
00:24:14,000 --> 00:24:16,000
You've made more of it than it actually is.

357
00:24:16,000 --> 00:24:18,000
This feeling is me.

358
00:24:18,000 --> 00:24:20,000
This pain is mine.

359
00:24:20,000 --> 00:24:23,000
Yeah.

360
00:24:23,000 --> 00:24:29,000
Turning experiences into situations and situations into problems

361
00:24:29,000 --> 00:24:31,000
is making more of the things.

362
00:24:31,000 --> 00:24:34,000
So this is a problem, I think.

363
00:24:34,000 --> 00:24:43,000
It has to be acknowledged with labeling mental illness

364
00:24:43,000 --> 00:24:48,000
as depression or they say schizophrenia.

365
00:24:48,000 --> 00:24:50,000
It's not that it's wrong.

366
00:24:50,000 --> 00:24:52,000
And it's not that it's not descriptive.

367
00:24:52,000 --> 00:24:56,000
It's just that it can become a reification.

368
00:24:56,000 --> 00:24:58,000
You have created something.

369
00:24:58,000 --> 00:25:00,000
More than what is actually there.

370
00:25:00,000 --> 00:25:04,000
And this is something we have to catch ourselves with as meditators,

371
00:25:04,000 --> 00:25:07,000
making a narrative that I am in this situation.

372
00:25:07,000 --> 00:25:09,000
This has happened to me.

373
00:25:09,000 --> 00:25:10,000
This problem has arisen.

374
00:25:10,000 --> 00:25:15,000
When the actual reality is just moments of experience.

375
00:25:15,000 --> 00:25:18,000
And if you're able to see those moments of experience,

376
00:25:18,000 --> 00:25:20,000
the problem disappears.

377
00:25:20,000 --> 00:25:21,000
It was never there.

378
00:25:21,000 --> 00:25:22,000
It was Papancha.

379
00:25:22,000 --> 00:25:25,000
It was extra.

380
00:25:25,000 --> 00:25:29,000
It's not what's actually there.

381
00:25:29,000 --> 00:25:36,000
So this is the clear indication of the difference,

382
00:25:36,000 --> 00:25:38,000
whether people believe,

383
00:25:38,000 --> 00:25:40,000
agree with the Buddha or not.

384
00:25:40,000 --> 00:25:43,000
You can see there is this distinction

385
00:25:43,000 --> 00:25:48,000
that are very clear goal

386
00:25:48,000 --> 00:25:52,000
is to see things as they are right when this very powerful

387
00:25:52,000 --> 00:25:54,000
teaching the Buddha gave to Bahia.

388
00:25:54,000 --> 00:25:56,000
What is the simplest teaching you can give?

389
00:25:56,000 --> 00:25:58,000
Deep-tay-dita-mat-dang-bavi-sity.

390
00:25:58,000 --> 00:26:03,000
Let's see, just be seeing.

391
00:26:03,000 --> 00:26:06,000
Hearing what is heard, let it just be heard.

392
00:26:06,000 --> 00:26:11,000
No, nothing more, no more, no less.

393
00:26:11,000 --> 00:26:15,000
Let's meet Papancha.

394
00:26:15,000 --> 00:26:17,000
And it's powerful.

395
00:26:17,000 --> 00:26:19,000
It's perfect.

396
00:26:19,000 --> 00:26:21,000
It's pure.

397
00:26:21,000 --> 00:26:25,000
And it creates clarity in the mind.

398
00:26:25,000 --> 00:26:28,000
You'll see that a big part of the process of the practice

399
00:26:28,000 --> 00:26:33,000
is realizing things about yourself that you didn't know.

400
00:26:33,000 --> 00:26:40,000
And being forced to confront aspects of yourself

401
00:26:40,000 --> 00:26:44,000
that weren't really clear to you before.

402
00:26:44,000 --> 00:26:47,000
Getting a better sense of how your mind works.

403
00:26:47,000 --> 00:26:49,000
And of course, ultimately, then freeing yourself

404
00:26:49,000 --> 00:26:53,000
from bad habit, the aspects of your mind

405
00:26:53,000 --> 00:26:55,000
that are causing you stress and suffering.

406
00:26:55,000 --> 00:27:03,000
So that's the first verse.

407
00:27:03,000 --> 00:27:10,000
The second verse starts the same.

408
00:27:10,000 --> 00:27:15,000
Just as there are no paths in the sky,

409
00:27:15,000 --> 00:27:22,000
there is no way to enlightenment outside of

410
00:27:22,000 --> 00:27:27,000
the observation of experience or the clear vision

411
00:27:27,000 --> 00:27:32,000
of experience as it is.

412
00:27:32,000 --> 00:27:35,000
But then the second part changes.

413
00:27:35,000 --> 00:27:41,000
There is no sun-car, no formation that is eternal.

414
00:27:41,000 --> 00:27:43,000
But the Buddha is never waver.

415
00:27:43,000 --> 00:27:48,000
So these two are in contrast as well.

416
00:27:48,000 --> 00:27:54,000
And this is, of course, another important core aspect

417
00:27:54,000 --> 00:28:00,000
of our practice, the impermanence of formation.

418
00:28:00,000 --> 00:28:07,000
So the Buddha gives a contrast that

419
00:28:07,000 --> 00:28:14,000
you're looking for impermanence.

420
00:28:14,000 --> 00:28:18,000
That impermanence doesn't exist in experiences.

421
00:28:18,000 --> 00:28:20,000
It doesn't exist in formations.

422
00:28:20,000 --> 00:28:25,000
It exists in enlightenment.

423
00:28:25,000 --> 00:28:29,000
It's a very profound sort of idea

424
00:28:29,000 --> 00:28:32,000
because, of course, permanence is something that knowing

425
00:28:32,000 --> 00:28:39,000
or not, that we seek out in our lives,

426
00:28:39,000 --> 00:28:45,000
our ambitions, our goals.

427
00:28:45,000 --> 00:28:48,000
It's the highest goal in many religious traditions.

428
00:28:48,000 --> 00:28:51,000
This permanent state,

429
00:28:51,000 --> 00:28:58,000
that might be permanent communion with God, for example.

430
00:28:58,000 --> 00:29:03,000
The idea that no formation is permanent

431
00:29:03,000 --> 00:29:13,000
and it is therefore an important rejoinder

432
00:29:13,000 --> 00:29:18,000
to free us from following the wrong path.

433
00:29:18,000 --> 00:29:24,000
Of course, the most glaring is following a wrong religious path,

434
00:29:24,000 --> 00:29:27,000
thinking that if I follow this religious path,

435
00:29:27,000 --> 00:29:32,000
I'll find a permanent home for myself in heaven, for example.

436
00:29:32,000 --> 00:29:37,000
Or as God or something in certain religions.

437
00:29:37,000 --> 00:29:41,000
But, of course, it is much more practically important

438
00:29:41,000 --> 00:29:46,000
for ordinary life as well.

439
00:29:46,000 --> 00:29:52,000
A reminder that the things that we cling to are impermanent.

440
00:29:52,000 --> 00:29:57,000
Our families, our loved ones, our partners,

441
00:29:57,000 --> 00:30:02,000
our possessions, even our own bodies.

442
00:30:02,000 --> 00:30:07,000
And that when we cling to these things,

443
00:30:07,000 --> 00:30:13,000
the eventual loss is what actually brings us stress and suffering.

444
00:30:13,000 --> 00:30:20,000
You know, the impermanence of them is something we can never avoid.

445
00:30:20,000 --> 00:30:25,000
For meditators, of course, it takes on another level,

446
00:30:25,000 --> 00:30:32,000
with the same idea, but another order of magnitude,

447
00:30:32,000 --> 00:30:37,000
because we see this constantly,

448
00:30:37,000 --> 00:30:41,000
with anything that we try to cling to.

449
00:30:41,000 --> 00:30:44,000
You might have a pleasant experience one moment

450
00:30:44,000 --> 00:30:47,000
and really be happy with that

451
00:30:47,000 --> 00:30:51,000
and kind of reassured by it,

452
00:30:51,000 --> 00:30:54,000
complacent as a result of it.

453
00:30:54,000 --> 00:30:59,000
And then struggle when it disappears.

454
00:30:59,000 --> 00:31:02,000
So, I think there are two lessons here.

455
00:31:02,000 --> 00:31:04,000
I mean, these are things you should already know.

456
00:31:04,000 --> 00:31:07,000
We already know about the importance of seeing impermanence,

457
00:31:07,000 --> 00:31:15,000
but the first is to not cling to pleasant or unpleasant experiences.

458
00:31:15,000 --> 00:31:17,000
So, when we talk about Papancha,

459
00:31:17,000 --> 00:31:19,000
we're talking about the difference between concepts

460
00:31:19,000 --> 00:31:22,000
and ultimate reality, but once you make that distinction,

461
00:31:22,000 --> 00:31:25,000
this can be seen as the next step,

462
00:31:25,000 --> 00:31:30,000
where your experiences don't cling to them,

463
00:31:30,000 --> 00:31:33,000
the good ones, the bad ones.

464
00:31:41,000 --> 00:31:48,000
And the second part to be able to see the impermanence

465
00:31:48,000 --> 00:31:56,000
of all experiences to free you from any attachment.

466
00:32:00,000 --> 00:32:05,000
Senkhara sasatana ti, there is no eternal formation.

467
00:32:05,000 --> 00:32:13,000
It is probably the most disconcerting thing,

468
00:32:13,000 --> 00:32:20,000
because it's diametrically opposed to our ordinary inclination,

469
00:32:20,000 --> 00:32:24,000
try and find a refuge, find something you can depend on,

470
00:32:24,000 --> 00:32:25,000
some sort of stability.

471
00:32:25,000 --> 00:32:28,000
We come to meditation for that.

472
00:32:28,000 --> 00:32:33,000
So, we try to organize our practice and organize our minds

473
00:32:33,000 --> 00:32:35,000
and focus our minds.

474
00:32:35,000 --> 00:32:40,000
If only I can get my mind to a state that is stable, lasting,

475
00:32:40,000 --> 00:32:48,000
so on, and this shift, when you realize that that's not even the goal,

476
00:32:48,000 --> 00:32:50,000
is disconcerting.

477
00:32:50,000 --> 00:32:52,000
It's like a growing up, in a sense,

478
00:32:52,000 --> 00:32:55,000
because you have to be accountable.

479
00:32:55,000 --> 00:32:58,000
You have to be flexible.

480
00:32:58,000 --> 00:33:02,000
It's kind of like a good description of being an adult,

481
00:33:02,000 --> 00:33:07,000
of being able to adapt rather than depend,

482
00:33:07,000 --> 00:33:12,000
being impervious to the changes of life.

483
00:33:12,000 --> 00:33:15,000
I don't think it's a good description of most adults,

484
00:33:15,000 --> 00:33:18,000
but it is a kind of, there's a distinction,

485
00:33:18,000 --> 00:33:21,000
and you could say that someone who is still dependent,

486
00:33:21,000 --> 00:33:24,000
of course, is like a dependent infant.

487
00:33:24,000 --> 00:33:29,000
But we depend upon stability if things are like this,

488
00:33:29,000 --> 00:33:33,000
that is acceptable.

489
00:33:33,000 --> 00:33:44,000
And so, an important idea is this idea of lack of dependence,

490
00:33:44,000 --> 00:33:48,000
and you see, don't you, we had a deep dwelling independent.

491
00:33:48,000 --> 00:33:52,000
That's, I think, the important teaching here,

492
00:33:52,000 --> 00:33:57,000
and that is true eternity.

493
00:33:57,000 --> 00:34:03,000
There's an unshakeability.

494
00:34:03,000 --> 00:34:05,000
Not to Buddha and the Minjita.

495
00:34:05,000 --> 00:34:06,000
There's no Injita.

496
00:34:06,000 --> 00:34:10,000
Injita is like this wavering or vacillating,

497
00:34:10,000 --> 00:34:12,000
which is in contrast to formations.

498
00:34:12,000 --> 00:34:16,000
You can't find peace in things.

499
00:34:16,000 --> 00:34:21,000
Your peace has to be independent of experiences

500
00:34:21,000 --> 00:34:25,000
and things in general.

501
00:34:25,000 --> 00:34:28,000
So that I think is the meaning of these verses.

502
00:34:28,000 --> 00:34:32,000
That is the teaching that the Buddha gave

503
00:34:32,000 --> 00:34:35,000
to Subhana, one of his last teachings.

504
00:34:35,000 --> 00:34:40,000
And that's Dhamapada, verses 254 and 255.

505
00:34:40,000 --> 00:34:43,000
So thank you for the same.

506
00:34:43,000 --> 00:34:46,000
Sadhana, sadhana, sadhana.

