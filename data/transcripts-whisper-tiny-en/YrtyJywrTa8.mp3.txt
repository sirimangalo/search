Okay, so I've been sent some info by someone to like or sign various petitions against pages that show animal cruelty
Against military action against the monetary system, etc. We have social website
Please could you advise on how one should respond but for now I have offered back a link to the how to meditate series of video
I'm starting to get the feeling that
No, it's not the right way to start this
I don't know that I can answer everyone's questions. I don't know if I can really answer all questions. Which should I do in this?
situation
For I think a couple of reasons at least one that that a
Lot of them have context
You know it how you respond to someone
Depends a lot on who they are what they're like
and
Know what your duty is and what you're you know if you work for Facebook if you work for a social network for example
You might have a different duty or if you
You know depending on your situation as well
and the other one is that
These questions are very I mean the answer is
answer a lot of these is
Quit which you quit your job quit school drop out become a monk live in the forest and a star
Then you can then then when people send you things you can say well, I'm a monk so it doesn't concern me
So what I mean to say by that is that
You know the answers that I would give to all two questions like this are not really going to be the
being
the final answer
You know and what I mean is that there's just going to be an endless stream of questions like this
What do I do in this situation would because you're living in the complex complicated
Environment surround you know
Constantly bombarded by things that don't really make sense and don't have easy answers
That's just a general comment on
Questions of the nature. What should I do in this situation?
I mean if if you're asking me what should you do as a monk in the what should you do when you're meditating in the forest in a tiger
Starts growling at you. For example, then this is something that I think well. This is this is really what we have to deal with
But I'm gonna I'm gonna throw a caution out there against questions of the nature of which should I do in such a
situation because I get a lot of them and they don't really
feel
Totally comfortable
I'm answering but I can answer them, but I don't feel like the answers are really all that satisfying because they're not really the final answer
final answers
Quit leave get out get a run away
No, find a way to to simplify your life so that you don't have to deal with such complex situations
Um
But being sent stuff. I mean, I just don't reply mostly. I don't reply to so much of the things people send me
Which um, you know often feels a little bit a little bit
Unpleasant, you know people often send me questions and they're valid questions
Send me in about questions in the email
But I've developed some kind of natural filtration system in my mind that I just most of them
I just don't answer and then people send me a second one asking me why I didn't answer. I don't answer that
It says send me a third one and really start complaining then they start thinking okay, okay time to tell them
Explain to them that I just don't answer most of my emails
Because I'm among so I've got an excuse
Um, when you're living in society, you do kind of have a responsibility, you know
To I mean we all have kind of a responsibility to our fellow humans
Even I consider that in some sense. I have to help people who are looking for help
But um
I don't know. I guess if you're asking about what are my thoughts on things that are against things against
Animal cruelty against military action against the monetary system
I believe in in a lot of the principles. I just don't really believe in protesting. I believe in in
discussion
Um, but I think that includes discussion with people who
Always people who who support such things. You know if people who support the monetary system
I think debating with them is very
educational I'm
If I had the time I'd be you know interested in these sorts of debates of course you need to study and you need to that's the problem
Really is there so many issues
How do you know that they're all wrong right?
So these three are pretty
Well, the first two are pretty clear cut animal cruelty most
Developed people you know can especially Buddhists can
readily agree that it's wrong
Military action. We readily agree that it's wrong, but
You still have to study in order to realize that right so to know that animal cruelty is wrong you have to have done some
At least some study and analysis better yet have done some meditation to help you see clearly that it's wrong
Military action a lot of people think can in certain circumstances is necessary is ethical
ethically required
That non intervention is unethical for example
So you have to study up on that and you have to be clear study either through
Through study book study or through meditation practice to get your mind clear in regards to to that one
The monetary system is very complicated, you know, I don't even know
I mean, I have my thoughts on it and I'm I'm not really pro money, but
You know it's such a complicated issue
What does it mean to say you're against the monetary system that you want tomorrow to stop using money?
Of course there'd be chaos right does it mean that you want to move in this direction or is it some
some website that actually has a plan to
Encourage sharing and and and cooperation as opposed to competition and
Capitalism
Or is it just someone is saying down with this down with this and and just getting angry about things right pointing out how awful people are right
Just all these corrupt, you know, expose the corrupt business people
It doesn't really do that much you become part of the problem
When you fight against something you become part of I guess that's a good answer to this is that I would from my point of view
When you fight against something you become part of the problem
You can't win by
You can't really fight fire with fire you have to fight with patience
You have to fight with truth. You have to stand your ground. You have to say what's wrong
but
We should never get angry and you should never
become
Militant about it us and them and on
So how should you respond?
Kindly respond with love. I think it's the same as the proselytizing answer give them a muffin
It's gonna be my answer to everything
So send them a muffin
