1
00:00:00,000 --> 00:00:13,280
Okay, good evening broadcasting live and maybe somewhat darkened picture.

2
00:00:13,280 --> 00:00:25,800
I apologize, but it's all for the effect, you'll see when you see the YouTube video.

3
00:00:25,800 --> 00:00:47,720
So hopefully this works.

4
00:00:47,720 --> 00:01:02,520
Hello and welcome back to our study of the Dhanupada.

5
00:01:02,520 --> 00:01:15,240
Today we continue on with first number 98, which reads as follows.

6
00:01:15,240 --> 00:01:39,160
Whether in a villager in the forest, whether in the sea or on dry land, wherever an enlightened

7
00:01:39,160 --> 00:01:55,080
one dwells, that is a delightful abode.

8
00:01:55,080 --> 00:02:05,520
Then this verse was taught in regards to Ray Vata, Ray Vata, who was a relative of

9
00:02:05,520 --> 00:02:13,400
sorry Buddha, the youngest brother of sorry Buddha, so sorry Buddha had three sisters

10
00:02:13,400 --> 00:02:15,480
and three brothers.

11
00:02:15,480 --> 00:02:21,440
They all became monks, all the brothers became monks, became because all of the sisters

12
00:02:21,440 --> 00:02:29,280
became, Bhikkhuni is except for Ray Vata, who was too young and he was approaching his

13
00:02:29,280 --> 00:02:31,920
seventh birthday.

14
00:02:31,920 --> 00:02:39,280
So there was every expectation that he was going to follow in his older siblings' footsteps.

15
00:02:39,280 --> 00:02:44,160
Except the parents weren't so keen on this, and sorry Buddha's parents were brahmanas,

16
00:02:44,160 --> 00:02:49,200
and they actually were quite upset at sorry Buddha, before sorry Buddha died, he had to

17
00:02:49,200 --> 00:02:56,280
go back and stay with his mother and deal with her abuse, even though he was like the Buddha's

18
00:02:56,280 --> 00:03:02,680
chief disciple, he still had to go home and deal with his, his, his abusive his mother

19
00:03:02,680 --> 00:03:10,400
from being a Buddhist monk, as if it was something she disapproved of.

20
00:03:10,400 --> 00:03:16,440
And so they tried to figure out a way how they can stop Ray Vata from becoming a monk,

21
00:03:16,440 --> 00:03:22,200
so they wouldn't lose their last son to these heretic Buddhists.

22
00:03:22,200 --> 00:03:30,440
And so they found a young girl, also seven years old, because at seven years old, then

23
00:03:30,440 --> 00:03:38,360
you're allowed to ordain as a novice, so they knew that that's what was coming up.

24
00:03:38,360 --> 00:03:47,240
And so they found a seven-year-old girl, and agreed with the family of this girl to marry

25
00:03:47,240 --> 00:03:54,360
the two of them. And so they, as with this, was the tradition among the brahmanas,

26
00:03:54,360 --> 00:04:01,680
they had a arranged marriage between these two seven-year-old children.

27
00:04:01,680 --> 00:04:06,680
And so they were feeling quite proud of themselves, and they got the marriage all set

28
00:04:06,680 --> 00:04:11,640
up and it came time and it brought the two kids, I really didn't know anything about

29
00:04:11,640 --> 00:04:20,040
anything in two seven-year-olds when the fairly sheltered lives. And so it came, they came

30
00:04:20,040 --> 00:04:26,080
to the marriage and they, they did, actually I don't think they did the marriage, it was

31
00:04:26,080 --> 00:04:33,600
some kind of rehearsal or some kind of blessing ceremony before the marriage. I'm not sure

32
00:04:33,600 --> 00:04:39,360
I can't remember the exact details, it's not so important.

33
00:04:39,360 --> 00:04:46,040
They were, everyone was blessing the two children, and they were saying, may you, may

34
00:04:46,040 --> 00:04:53,160
you be as wise as her grand, as this girl's grandmother, I'm talking about this grandmother

35
00:04:53,160 --> 00:04:57,880
who is sort of the elder in the family, may you be as wise as her and may you live, may

36
00:04:57,880 --> 00:05:03,600
you both live to be as long as her. I'm very glad that was curious and he looked around

37
00:05:03,600 --> 00:05:07,920
and he asked, who is this grandmother that you're talking about? And they pointed over

38
00:05:07,920 --> 00:05:16,680
to this woman who was 120 years old and her teeth were falling out and she was bent and

39
00:05:16,680 --> 00:05:23,760
decrepit and barely able to walk or keep herself up. And Ravetta was kind of shocked

40
00:05:23,760 --> 00:05:29,480
at this site because he never seen someone quite so old or wrinkled or bent or out of shape.

41
00:05:29,480 --> 00:05:34,800
He said, that's what old people look like. They said, yes, they said, so wait, is that

42
00:05:34,800 --> 00:05:40,360
what she's going to look like when she's at age? And they said, oh, if she's lucky,

43
00:05:40,360 --> 00:05:46,960
if your wife is lucky to live that long, but usually people die out a lot earlier, die.

44
00:05:46,960 --> 00:05:52,080
And he thought about this and people, we have to die, we get old. He said, what's the

45
00:05:52,080 --> 00:05:58,520
point? And it hit him the way it hit so many people, including Sarri Puddha and all of his

46
00:05:58,520 --> 00:06:03,520
other siblings? And he said, this must have been, he said to himself, this must have been

47
00:06:03,520 --> 00:06:14,320
what my older brother saw. And suddenly had no desire to get married or live a lay life at

48
00:06:14,320 --> 00:06:26,400
all. And he became intent in his mind on leaving the home life and becoming the monk.

49
00:06:26,400 --> 00:06:32,920
But the family was having none of that. And so they took him back and they were guarding

50
00:06:32,920 --> 00:06:39,080
him quite carefully on the way back. So he pretended to have to go to the, after he

51
00:06:39,080 --> 00:06:47,800
had been to have diarrhea and he had to go and use the bush. And so he stopped and they

52
00:06:47,800 --> 00:06:58,040
watched him and watched him go into the bush, watched him come out again. And he saw that

53
00:06:58,040 --> 00:07:02,120
he didn't have any chance to escape. And so he did it again. He said, oh, no, I have to

54
00:07:02,120 --> 00:07:07,840
go again. Finally a third time he did it. And he said, not be right out. And so they left

55
00:07:07,840 --> 00:07:15,760
him and went back to the carriage. And as soon as they left he ran away. And he ran to

56
00:07:15,760 --> 00:07:21,880
a group of monks who were living fairly close by and asked them to ordain him. And they

57
00:07:21,880 --> 00:07:27,240
were, they said, look, kid, we don't just ordain people. You have to get permission from

58
00:07:27,240 --> 00:07:32,560
your parents and so on. You know, it's a real thing in real deal because we don't want

59
00:07:32,560 --> 00:07:38,880
to get in trouble with your family. And he said, well, then take me to my brother, Upa Tisa.

60
00:07:38,880 --> 00:07:45,200
And he said, we don't know anybody named Upa Tisa. And he said, my brother, sorry, he's

61
00:07:45,200 --> 00:07:48,640
in the, you guys know I'm a sorry put that, but he's my older brother. And immediately

62
00:07:48,640 --> 00:07:54,120
they said, you're sorry put this older brother or younger brother. And he said, yes, what's

63
00:07:54,120 --> 00:07:59,680
your name? My name is rewata. He said, oh, and actually, sorry, put the head told them

64
00:07:59,680 --> 00:08:05,760
about the what? And said, the different whatever comes to you and asks you to ordain him.

65
00:08:05,760 --> 00:08:10,960
You consider that he has his parents permission. I am his parents as far as this goes because

66
00:08:10,960 --> 00:08:16,280
his, his parents have wrong views. So what good are they? What uses it seeking their

67
00:08:16,280 --> 00:08:25,920
permission? It's an interesting sort of breaking of a rule, really, you know, ignoring

68
00:08:25,920 --> 00:08:37,040
or using some kind of special privilege of the chief disciple to sort of ignore that rule

69
00:08:37,040 --> 00:08:45,240
that the Buddha had laid down. It was good reason obviously, but it is a curious example

70
00:08:45,240 --> 00:08:53,120
in the rule, or not following the rule, embedding the rule to suit one's purpose. And

71
00:08:53,120 --> 00:08:58,920
so they ordained him. And he became an Arahant, I can't remember whether it was when he

72
00:08:58,920 --> 00:09:03,520
was ordained or afterwards, but he went to live off in the forest and rewata became

73
00:09:03,520 --> 00:09:10,800
known as the one who lives in the forest, some kind of special forest that he lived in.

74
00:09:10,800 --> 00:09:17,600
These, it is forest with all of these thorns. It would drop, I think it would drop these

75
00:09:17,600 --> 00:09:24,760
seeds or the seed balls that had thorns on them. I know in Thailand they had those anyway.

76
00:09:24,760 --> 00:09:34,920
I was staying up in a mountain in Thailand once and I got lost up in the mountains and

77
00:09:34,920 --> 00:09:39,880
I was wearing sandals and so I was climbing up and down trying to find my way back and

78
00:09:39,880 --> 00:09:45,200
I got so terribly scratched up because of these little, partially because of these thorns

79
00:09:45,200 --> 00:09:53,320
balls. So point being, he was someone who lived in a place that was very hard to get

80
00:09:53,320 --> 00:09:58,160
to, was very remote, was the sort of place that no one would bother him. So he was the

81
00:09:58,160 --> 00:10:03,600
kind of person who was looking just for solitude. And so he went there to a place where

82
00:10:03,600 --> 00:10:14,880
no one would come, or no one would come for other reasons, like looking for some kind

83
00:10:14,880 --> 00:10:23,480
of produce or some kind of edible or this kind of this or that. There was just not

84
00:10:23,480 --> 00:10:27,200
an accessible forest that people would want to go to. Nobody would go there for fun or

85
00:10:27,200 --> 00:10:33,120
sport or hunting. There was no reason to go there. So it was a perfect place for him to find

86
00:10:33,120 --> 00:10:39,840
peace. It was just the place that he decided to live. I could also have to do with past

87
00:10:39,840 --> 00:10:48,240
life karma. And so that's the story of Riwat as sort of in a nutshell. But the story

88
00:10:48,240 --> 00:10:58,480
of this verse is in regards to his dwelling and there's sort of this magical, I don't

89
00:10:58,480 --> 00:11:03,600
know when these magical happenings where the Buddha came to see Riwat and he brought all

90
00:11:03,600 --> 00:11:11,440
of the monks with him. He brought a large retinue of monks with him. And when they got there,

91
00:11:11,440 --> 00:11:17,620
Riwat that had transformed the place into a comfortable place to live for all these

92
00:11:17,620 --> 00:11:24,800
monks with cookies and everything. And they say he had done this with magical powers.

93
00:11:24,800 --> 00:11:30,880
At the very least you could say that he made it, he had the power of mind such that he's

94
00:11:30,880 --> 00:11:42,080
kind of hypnotized people or all the monks cover kind of unable to or were able to ignore

95
00:11:42,080 --> 00:11:47,440
the hardship because of his power of mind. He made it so that everyone was comfortable

96
00:11:47,440 --> 00:11:52,400
there. Let's say that. If we want to be vague about it. I mean, we can say literally in

97
00:11:52,400 --> 00:12:00,320
the text that says he created a monastery from his using his mind, whatever. That's not really

98
00:12:00,320 --> 00:12:12,800
the point. But it's how the story goes. And there were these two older monks who were, I think,

99
00:12:12,800 --> 00:12:19,440
criticizing Riwat by saying, how can he possibly meditate living in such opulent conditions?

100
00:12:19,440 --> 00:12:28,880
As they sort of got the sense that this was real, that it really was a overly comfortable

101
00:12:28,880 --> 00:12:33,040
place to live. They say, oh, Riwat, there's supposed to be this austere monk living off from

102
00:12:33,040 --> 00:12:38,160
the forest. What's he doing living like this and pretending to live off in seclusion.

103
00:12:38,160 --> 00:12:50,960
And so they had this, again, this criticism of the Iran's. And then all the monks left.

104
00:12:51,680 --> 00:12:57,760
And these two monks who had criticized Riwat then forgot ended up forgetting something

105
00:12:58,400 --> 00:13:03,840
in the monastery. And so they had to go back for it. And when they got back after all the monks

106
00:13:03,840 --> 00:13:07,760
was left, they couldn't find the monastery. And they were looking around and all they had,

107
00:13:07,760 --> 00:13:15,440
they saw their swords. It was just a horrible place. There was no monastery, nowhere, no pads, no

108
00:13:16,480 --> 00:13:21,680
comfortable place to live. And then they found their belongings hung up on a tree,

109
00:13:23,120 --> 00:13:27,280
not where they had left them. But someone had hung them up on the tree. I guess Riwat had put them

110
00:13:27,280 --> 00:13:36,400
there or whatever. And so they picked up their belongings all confused and went back on their way.

111
00:13:38,640 --> 00:13:43,120
And they traveled all the way back to Sawat team. When they got to Sawat team, they went to

112
00:13:43,120 --> 00:13:50,400
see Wisaka, because Wisaka was someone who would always give rice porridge to monks who were

113
00:13:50,400 --> 00:13:57,040
traveling. So whenever they've never, anyone, any monk reached Sawat team, they would go to see Wisaka

114
00:13:57,680 --> 00:14:02,320
and go to our house. Now Wisaka found these two monks coming in. So she thought that she'd ask them

115
00:14:02,960 --> 00:14:06,400
where they'd been. And she found that they'd been to Riwisaka. And she said, oh,

116
00:14:07,600 --> 00:14:11,120
I've heard, I've heard about the place where they was, is that they,

117
00:14:14,640 --> 00:14:19,600
I know what was it like. And they said, you've heard that it's a nice place to do.

118
00:14:19,600 --> 00:14:35,360
It's horrible. It's this forest of thorns and bushes. And so she's, oh, yeah, it's really such

119
00:14:35,360 --> 00:14:41,280
a remote place and hard to get to. And she got this sort of the sense that it was just a horrible

120
00:14:41,280 --> 00:14:45,680
horrible place that are a very difficult place, a very remote and uncomfortable place to live.

121
00:14:45,680 --> 00:14:51,520
And so then they went on the way. And then these two other monks who had been there with the Buddha

122
00:14:51,520 --> 00:14:59,520
and hadn't gone back and seen what it was really like. And they came to see Wisaka and told

123
00:14:59,520 --> 00:15:04,480
they're a completely opposite story. They said, oh, it's just a wonderful place. It's just the most

124
00:15:04,480 --> 00:15:12,240
comfortable, peaceful, pleasant place to live that you can imagine. And so now Wisaka was

125
00:15:12,240 --> 00:15:19,280
confused because not exactly confused, but she was, she knew something was up because she got into

126
00:15:19,280 --> 00:15:23,840
completely contradictory story about this place. And she thought, well, I'll go and ask the Buddha

127
00:15:24,480 --> 00:15:28,880
figuring that the Buddha would tell her if there was some, some kind of magic involved,

128
00:15:29,600 --> 00:15:35,120
something's going on here, something's fishy. Well, how are these two people, two groups of people

129
00:15:35,120 --> 00:15:40,400
seeing something completely different? So she went to see the Buddha expecting him to kind of

130
00:15:40,400 --> 00:15:46,240
explain it and he didn't explain it. She asked, how is it that some people found it very,

131
00:15:47,360 --> 00:15:55,040
how is it that one group of monks described it as being this place of hard, you know,

132
00:15:55,040 --> 00:16:01,520
thorn trees and just sounds like a very uncomfortable place to live. But then these two other

133
00:16:01,520 --> 00:16:06,720
monks said that, well, they were there. They felt quite comfortable. How can that be? And the Buddha

134
00:16:06,720 --> 00:16:12,000
said, oh, well, wherever our hands live, it's a beautiful place to be, whether it's in thorn trees

135
00:16:12,000 --> 00:16:20,480
in a thorn tree forest or whether it's in the village or whether it's in the middle of the ocean,

136
00:16:20,480 --> 00:16:28,240
bottom of the ocean or on dry land, wherever it might be. It's a great place because that

137
00:16:28,240 --> 00:16:34,400
it's where our hands live. So that's the verse today. That's the story and that's the that's the

138
00:16:34,400 --> 00:16:40,400
verse and that's the story behind it. There's not too much to say here about this verse

139
00:16:42,400 --> 00:16:49,280
or the story, really. But what we can say is the importance of being around and engaged with

140
00:16:49,280 --> 00:16:55,360
people who are meditating. And there's two things I guess we can say. One is this. The importance of

141
00:16:55,360 --> 00:17:02,480
being around people and how beneficial it is. How a place is worth living because there are people

142
00:17:02,480 --> 00:17:10,320
doing good things. If you're in a place where full of wicked evil mean people, it's going to be

143
00:17:10,320 --> 00:17:16,000
a real problem for you. And for your spiritual cultivation, spiritual development, there will always

144
00:17:16,000 --> 00:17:26,960
be this drag. You'll always be drawn into conflict and difficulty suffering. It makes it very hard

145
00:17:26,960 --> 00:17:34,480
to progress, very hard to find peace of mind and clarity of mind. It's much easier to be

146
00:17:34,480 --> 00:17:40,560
clouded in the mind. And the other thing is this idea that it doesn't matter where you live.

147
00:17:41,360 --> 00:17:47,440
The other side of it is you can live in the perfect place to meditate. You can live in a comfortable

148
00:17:47,440 --> 00:17:58,080
place. You can live deep in the forest. But without proper guidance or at least suitable

149
00:17:58,080 --> 00:18:04,720
friendship with people who are meditating, people who are doing good things, it's you're better off

150
00:18:04,720 --> 00:18:10,560
living in the city. But this is a clear place where the Buddha says it's not about living in the

151
00:18:10,560 --> 00:18:17,360
forest. It's much more about associating with the right people. So you shouldn't be feeling bad

152
00:18:17,360 --> 00:18:24,960
that you have to live in amongst society. The question is what sort of society you incline towards?

153
00:18:25,760 --> 00:18:36,720
What sort of friends you cultivate and associate with? Because we emulate, all of our learning

154
00:18:36,720 --> 00:18:41,920
comes from much of our learning and new outcomes from emulating others or taking from others,

155
00:18:41,920 --> 00:18:48,320
our sharing and our setting examples. So when we're surrounded by people who are a bad example,

156
00:18:48,320 --> 00:18:56,400
it's very difficult for us not to fall into old patterns and problematic patterns of behavior.

157
00:18:56,400 --> 00:19:06,160
So associate with people with the highest sort of person,

158
00:19:09,360 --> 00:19:15,120
dwell where there are our hands. So the upshot of this verse is that most of us are living in the

159
00:19:15,120 --> 00:19:24,800
wrong place. We have to find a place where we can associate with our hands. No, this isn't

160
00:19:24,800 --> 00:19:30,160
a many idea. It isn't to go around looking for someone or you and our hand or you and our

161
00:19:30,160 --> 00:19:35,680
is that person and our hand? Is that person and our hand? That's just a remark by the Buddha.

162
00:19:35,680 --> 00:19:40,480
Because it doesn't work that way. You can't just ask and people are going to tell you. It's not

163
00:19:40,480 --> 00:19:46,480
likely to happen. But what you will find is that when you are around and things get better not worse.

164
00:19:47,280 --> 00:19:53,760
You are progressing the practice. When you're around people who are also practicing people who

165
00:19:53,760 --> 00:20:00,000
have practice, people who know things about the practice and things get better not worse.

166
00:20:00,880 --> 00:20:06,560
So that's the general just that we should take away from it. We should not think of ourselves that

167
00:20:06,560 --> 00:20:12,480
we can become enlightened on our own without guidance. It's much easier, much more likely that we'll

168
00:20:12,480 --> 00:20:17,920
get on the wrong path and get a wrong understanding of enlightenment and easy to get lost.

169
00:20:17,920 --> 00:20:24,960
Mistake, what is not the path for, what is the path? It's very easy because it's a path we've

170
00:20:24,960 --> 00:20:34,000
never taken before. Having a guide is indispensable. So that's the number by the third day.

171
00:20:34,000 --> 00:20:36,800
Thank you all for tuning in. I'm sure you're all the best.

172
00:20:36,800 --> 00:20:46,080
I'm not sure how that looked actually, but well, we'll see.

173
00:20:46,080 --> 00:21:03,600
I'm going to excuse this new setup.

174
00:21:16,720 --> 00:21:21,440
This is why I need a film crew. We'll see. We'll do all these things. It's not really

175
00:21:23,040 --> 00:21:25,840
tense to me quite an ordeal.

176
00:21:28,160 --> 00:21:31,200
It's very consistent. We don't have to drag it from room to room.

177
00:21:34,000 --> 00:21:35,360
Just have to move stuff around.

178
00:21:38,560 --> 00:21:40,080
Yeah, it takes a quite convenient.

179
00:21:40,080 --> 00:21:50,640
This is the equipment that people have sent and donated. None of this was bought by us,

180
00:21:52,160 --> 00:21:55,280
so given head of charity.

181
00:21:59,760 --> 00:22:01,040
It's kind of awesome.

182
00:22:01,040 --> 00:22:08,240
It's nice to be able to put it to the news.

183
00:22:09,680 --> 00:22:14,240
Okay, so that's that now on to the next part to it. Do we have any question?

184
00:22:15,280 --> 00:22:15,600
Thank you.

185
00:22:17,840 --> 00:22:22,160
Hello, Bhante. I have always wondered what the benefits of meditating with others can be.

186
00:22:22,880 --> 00:22:27,600
Are there any special beneficial aspects that people should know about meditating with others?

187
00:22:27,600 --> 00:22:32,400
To me, it seems it does not matter whether you are doing it with someone or by yourself.

188
00:22:35,680 --> 00:22:42,400
Well, it does encourage you to meditate. It encourages you to stay focused when you're alone.

189
00:22:42,400 --> 00:22:47,200
It's much easier to sort of, oh, well, get up and do something else, but it's kind of embarrassing

190
00:22:47,200 --> 00:22:53,200
if you do that in the group. You're much more likely to be alert. So in that sense, it can be quite

191
00:22:53,200 --> 00:23:01,360
beneficial for most people, not for beginner meditators anyway. But it can also be a crutch,

192
00:23:01,360 --> 00:23:08,080
especially as you progress. So eventually you want to be able to meditate alone,

193
00:23:08,080 --> 00:23:13,600
eventually meditating alone is preferable. I mean, when we do courses in our tradition,

194
00:23:13,600 --> 00:23:19,200
we put everyone in their own room in their own cookies, their own huts, and in Thailand.

195
00:23:19,200 --> 00:23:25,520
And so it's all done alone. But you're in the meditation center and that's some big

196
00:23:25,520 --> 00:23:49,280
boost itself. It's really enough, actually.

197
00:23:49,280 --> 00:23:58,000
Dante, can you please explain what is absorption? It is mentioned frequently in the visibimaka.

198
00:24:03,280 --> 00:24:12,160
Absorption is what the translate Jana really just means meditation, but these are specific

199
00:24:12,160 --> 00:24:19,600
meditations that one enters into a specific meditation. So it's kind of like a trance where the

200
00:24:19,600 --> 00:24:27,280
mind is fixed on something. And there's two kinds of being fixed on something. There's

201
00:24:28,000 --> 00:24:34,560
Upa-chara, which means you're in the neighborhood of it means your mind is concentrated on something,

202
00:24:34,560 --> 00:24:45,120
but not yet absorbed in it. And then there's up and up. And I mean, it's attainment. So you attain

203
00:24:45,120 --> 00:24:52,640
you really get into the object, you penetrate into it. This is usually used to talk about

204
00:24:52,640 --> 00:25:01,840
some at the meditation. But Bhipasana has it as well. I mean, Bhayana is a kind of a Jana, an absorption

205
00:25:01,840 --> 00:25:03,120
opponent, somebody.

206
00:25:09,600 --> 00:25:17,040
Not a question, but a comment that someone has. Today I've submitted a test version of an iOS

207
00:25:17,040 --> 00:25:24,000
app that has the same functionality as this works. Yes, go ahead. It's supposed to bring that up for

208
00:25:24,000 --> 00:25:29,840
not at the moment. It's still pretty basic. You can start a meditation session, see the list of

209
00:25:29,840 --> 00:25:34,720
currently meditating persons and see the quote of the day. If someone wants to help testing

210
00:25:34,720 --> 00:25:40,000
the app, please send me an email. And that would be from the chat panel here. If you click on

211
00:25:40,000 --> 00:25:47,040
this name, you've got to see mail in this profile. Right. So yes, if you'd like to test it, go talk to

212
00:25:47,040 --> 00:25:54,320
him. That's an awesome thing that we'll have. If anyone would like to send me an iPhone or I think

213
00:25:56,000 --> 00:25:59,760
I could test it as well. Someone was going, why I'm bringing up is because we talked about this

214
00:25:59,760 --> 00:26:03,520
earlier and someone was actually going to do it and they were actually materialized. So

215
00:26:03,520 --> 00:26:12,080
if anyone is still has an old iPhone or something that they could do without, I'm not really keen

216
00:26:12,080 --> 00:26:23,520
on Apple products being so terribly proprietary as they are, but given that people use this,

217
00:26:23,520 --> 00:26:27,760
use them. It's useful to have an iOS app for sure.

218
00:26:27,760 --> 00:26:35,920
And you know, I find it useful to test it myself, but not really, not entirely necessary.

219
00:26:39,120 --> 00:26:42,720
And apparently it has to be higher than iPhone 4 to be supported.

220
00:26:48,800 --> 00:26:51,600
Higher than or iPhone 4 and higher.

221
00:26:51,600 --> 00:26:58,720
Well, earlier someone offered iPhone 4 and he said, I'm afraid iPhone 4 is not supported.

222
00:26:58,720 --> 00:27:05,920
The app uses iOS 8 and higher, but then down lower he said for and higher are supported. So

223
00:27:07,280 --> 00:27:12,880
it's a little unclear and he's offline now, but maybe send an email if you've got something

224
00:27:14,160 --> 00:27:14,560
close.

225
00:27:14,560 --> 00:27:22,400
Yeah, I mean, don't go to your way, but just before someone had something,

226
00:27:23,840 --> 00:27:30,480
they were going to send that it never materialized.

227
00:27:30,480 --> 00:27:42,160
Question with a link with the link is not coming up for me. So I'm not sure what the question is.

228
00:27:46,960 --> 00:27:49,440
Can't find where to send where to sign up.

229
00:27:50,800 --> 00:27:55,840
Neil, I'm not getting anything off that link either. So just say what you're trying to sign up for.

230
00:27:55,840 --> 00:28:03,040
The reporting schedule, you're probably using the wrong device. It looks like the device you're

231
00:28:03,040 --> 00:28:07,920
using probably isn't supported. It maybe it's an iPad or something. No, it doesn't look like

232
00:28:07,920 --> 00:28:13,360
that looks like actually a computer, doesn't it? Use a different browser I would suggest.

233
00:28:14,800 --> 00:28:20,320
No matter what the platform you're on, use a different browser. Is there Chrome for iOS?

234
00:28:20,320 --> 00:28:26,880
I don't think so. But it looks like that looks more like a notebook, isn't it?

235
00:28:34,960 --> 00:28:41,760
Yeah, Chrome does work for iOS. So iOS are Android or whatever you're using. Try using

236
00:28:41,760 --> 00:28:52,480
Chrome or Firefox. You're using Firefox.

237
00:28:54,480 --> 00:28:59,680
Okay. Thank you, Anthony. He clarified it's iPhone 4S, which is the minimum requirement.

238
00:29:00,320 --> 00:29:06,240
Sorry, not being an iPhone person. I didn't realize that was for us. That makes sense.

239
00:29:06,240 --> 00:29:11,120
iPhone 4 is altered in for us.

240
00:29:13,520 --> 00:29:18,000
Hmm. I don't know what's wrong with it for this person, why it's not working for them.

241
00:29:18,960 --> 00:29:23,840
I guess I may have actually, I know what it might be.

242
00:29:25,200 --> 00:29:28,640
If you can go to that page and refresh it a couple of times,

243
00:29:28,640 --> 00:29:43,600
you might be able to see something. Go back to the Meet page and hit Control F5 or just F5.

244
00:29:47,760 --> 00:29:53,040
I'm not getting anything. So I mean, it's not an error on the, when it controls Shift J,

245
00:29:53,040 --> 00:30:06,080
I think Control Shift J. That'll show your console, which will have errors. So they'll show

246
00:30:06,080 --> 00:30:10,080
any potential errors. Now often it shows a lot of stuff that's totally unrelated, but

247
00:30:10,080 --> 00:30:27,120
it may also show what the error is, why it's not showing up, but it works for me. So

248
00:30:28,320 --> 00:30:29,360
that's all that's important.

249
00:30:31,280 --> 00:30:34,240
I'll also important for the meditators meeting with you, I would think.

250
00:30:34,240 --> 00:30:43,440
I know I'm kidding, but I can, unless we have someone who can troubleshoot all of this and

251
00:30:43,440 --> 00:30:54,000
do customer service or users, when you call user support, that's not going to happen.

252
00:30:54,000 --> 00:31:06,480
You just have to, I don't know, it can't help.

253
00:31:12,480 --> 00:31:14,880
Okay, you got it. It was an ad block issue.

254
00:31:17,680 --> 00:31:19,680
Why would the ad block be doing that?

255
00:31:19,680 --> 00:31:25,440
It's interesting. Probably a JS block, a JavaScript block.

256
00:31:28,960 --> 00:31:33,520
This ad block won't, we don't have any ads, but you probably got some

257
00:31:33,520 --> 00:31:36,240
thing crazy like a JavaScript block or something.

258
00:31:37,360 --> 00:31:39,360
That's funny, why would it be blocking that?

259
00:31:42,400 --> 00:31:47,840
We're not doing any external requests, I don't think. Maybe we are.

260
00:31:47,840 --> 00:31:51,120
Anyway, glad you got that sorted out.

261
00:31:55,920 --> 00:32:02,720
Right, so it looks like we have, we're getting fewer questions, which is really actually quite

262
00:32:02,720 --> 00:32:08,080
inspiring because it means people have got all their answers.

263
00:32:08,080 --> 00:32:14,240
And that or they don't like the answers and they're looking elsewhere.

264
00:32:22,320 --> 00:32:25,920
Anyway, I guess that's it then. We'll come back again tomorrow.

265
00:32:27,520 --> 00:32:29,840
Thank you all for tuning in. Thanks Robin for your help.

266
00:32:30,640 --> 00:32:31,440
Thank you, Bante.

267
00:32:32,400 --> 00:32:33,600
Okay, good night.

268
00:32:33,600 --> 00:32:39,600
Good night.

