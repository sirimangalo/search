1
00:00:00,000 --> 00:00:02,000
Welcome back to Ask Among.

2
00:00:02,000 --> 00:00:06,000
Next question comes from Chocho who asks,

3
00:00:06,000 --> 00:00:08,000
What is intelligence?

4
00:00:08,000 --> 00:00:11,000
How can a person do to be intelligent?

5
00:00:11,000 --> 00:00:13,000
Is this the same as wisdom?

6
00:00:13,000 --> 00:00:16,000
What makes a person stupid?

7
00:00:16,000 --> 00:00:19,000
Some good questions.

8
00:00:19,000 --> 00:00:25,000
Intelligence and wisdom are different in the English sense

9
00:00:25,000 --> 00:00:28,000
or the Western sense of the word.

10
00:00:28,000 --> 00:00:32,000
In the Buddhist teaching there are three kinds of wisdom

11
00:00:32,000 --> 00:00:38,000
and one is memorization, the ability to remember facts.

12
00:00:38,000 --> 00:00:42,000
Simply remember things, not facts, but teachings.

13
00:00:42,000 --> 00:00:45,000
The ability to recite, say, the Buddhist teachings

14
00:00:45,000 --> 00:00:48,000
or to memorize lists of, you know,

15
00:00:48,000 --> 00:00:51,000
what are the three types of wisdom in this case?

16
00:00:51,000 --> 00:00:56,000
The second type of wisdom is through analyzing analysis, logic,

17
00:00:56,000 --> 00:01:05,000
through reasoning, like when you think about impermanence

18
00:01:05,000 --> 00:01:09,000
and the Buddha taught us to see and to understand impermanence

19
00:01:09,000 --> 00:01:12,000
because that helps you let go and not cling to things.

20
00:01:12,000 --> 00:01:13,000
This is great.

21
00:01:13,000 --> 00:01:15,000
Once you realize it's impermanent,

22
00:01:15,000 --> 00:01:17,000
you're not going to think it's so great.

23
00:01:17,000 --> 00:01:21,000
It's something that is not going to last forever

24
00:01:21,000 --> 00:01:24,000
and when it's gone, you'll feel suffering and so on.

25
00:01:24,000 --> 00:01:29,000
But simply thinking about this, this is a type of wisdom,

26
00:01:29,000 --> 00:01:32,000
the type of button you always say, which you can translate

27
00:01:32,000 --> 00:01:34,000
as wisdom.

28
00:01:34,000 --> 00:01:40,000
Reasoning, oh, yes, this place where I'm at,

29
00:01:40,000 --> 00:01:42,000
I'm not going to be able to stay here forever and so on.

30
00:01:42,000 --> 00:01:45,000
And this body I have, it's not going to be healthy

31
00:01:45,000 --> 00:01:47,000
for ever thinking in this way.

32
00:01:47,000 --> 00:01:52,000
I think it's clear that many Buddhists fall into this trap.

33
00:01:52,000 --> 00:01:57,000
It's something that many teachers bemoan that we often fall

34
00:01:57,000 --> 00:02:02,000
into this trap of thinking about the Buddha's teaching

35
00:02:02,000 --> 00:02:05,000
and reasoning and saying, yes, yes, I understand impermanence.

36
00:02:05,000 --> 00:02:08,000
You know, impermanence means look at all these things around us.

37
00:02:08,000 --> 00:02:11,000
They're all going to change and yesterday it was sunny,

38
00:02:11,000 --> 00:02:14,000
now it's cloudy and it was raining this morning

39
00:02:14,000 --> 00:02:16,000
and now it's not and so on.

40
00:02:16,000 --> 00:02:19,000
And suffering is a yes, yes, there's lots of suffering.

41
00:02:19,000 --> 00:02:23,000
I used, I had a toothache or a headache or so on.

42
00:02:23,000 --> 00:02:27,000
But this isn't, from a Buddhist point of view, true wisdom.

43
00:02:27,000 --> 00:02:31,000
True wisdom is this third type which comes from experience.

44
00:02:31,000 --> 00:02:35,000
And I think this is how we differentiate in English as well.

45
00:02:35,000 --> 00:02:39,000
The word intelligence could be used to mean the ability to memorize.

46
00:02:39,000 --> 00:02:44,000
It could also be used to more likely to mean the ability to rationalize

47
00:02:44,000 --> 00:02:52,000
or to the ability to consider and to use logic

48
00:02:52,000 --> 00:03:01,000
and to make arguments and so on to come to understand intellectually,

49
00:03:01,000 --> 00:03:03,000
in terms of the intellect.

50
00:03:03,000 --> 00:03:08,000
But wisdom, we would reserve for this in English for this third type,

51
00:03:08,000 --> 00:03:11,000
which means which is based on experience.

52
00:03:11,000 --> 00:03:15,000
We say old people are wise because of all their experience that they've had.

53
00:03:15,000 --> 00:03:18,000
They've gone through a lot and they've learned a lot from it.

54
00:03:18,000 --> 00:03:19,000
They've learned many lessons.

55
00:03:19,000 --> 00:03:23,000
They've seen many things and they've been able to spend time considering and so on.

56
00:03:23,000 --> 00:03:26,000
And so we consider them wise.

57
00:03:26,000 --> 00:03:31,000
So this is how we should approach these questions when we talk about what is intelligence?

58
00:03:31,000 --> 00:03:36,000
Intelligence is either the ability to memorize facts and so on

59
00:03:36,000 --> 00:03:46,000
or the ability to have a rational thought process where you're able to understand

60
00:03:46,000 --> 00:03:50,000
and to come up with answers and arguments and logic and so on.

61
00:03:50,000 --> 00:03:56,000
The ability of the mind to work, in the sense, maybe the ability of the brain

62
00:03:56,000 --> 00:04:02,000
to work, the processing power and ability and clarity of mind.

63
00:04:02,000 --> 00:04:08,000
It somehow goes hand in hand with wisdom because a person who is truly wise

64
00:04:08,000 --> 00:04:14,000
is going to obviously have a clearer mind and they're as a result going to be able to memorize things better.

65
00:04:14,000 --> 00:04:18,000
If your mind is all clouded and mixed up and confused,

66
00:04:18,000 --> 00:04:26,000
your ability to process information and to come up with logical arguments and explanations and so on

67
00:04:26,000 --> 00:04:29,000
is going to be greatly impaired.

68
00:04:29,000 --> 00:04:35,000
It's not that they're totally different, but they're not exactly the same.

69
00:04:35,000 --> 00:04:38,000
So what can a person do to be intelligent?

70
00:04:38,000 --> 00:04:43,000
Well, there are many different levels to this obviously.

71
00:04:43,000 --> 00:04:50,000
The best way is to meditate, to clear your mind.

72
00:04:50,000 --> 00:04:53,000
Once your mind is clear, things like intelligence come along.

73
00:04:53,000 --> 00:05:04,000
If your goal specifically is intelligence, then it's not really the most important thing.

74
00:05:04,000 --> 00:05:12,000
So in that sense, it's not really something that I'm interested in talking about.

75
00:05:12,000 --> 00:05:14,000
What is intelligence?

76
00:05:14,000 --> 00:05:19,000
So you can get a good job as an engineer or a doctor or a lawyer or so on.

77
00:05:19,000 --> 00:05:23,000
That's fine, but that's not what Ask a Monk is about.

78
00:05:23,000 --> 00:05:30,000
But if you want to be a better person, and if you want to be able to work better in the world,

79
00:05:30,000 --> 00:05:35,000
then I think there's no better cure than meditation.

80
00:05:35,000 --> 00:05:41,000
Although it is an interesting question as to why some people are born in this way and born in that way.

81
00:05:41,000 --> 00:05:51,000
And the one answer that the scientists have is genes and ancestry and so on, coupled with their environment.

82
00:05:51,000 --> 00:05:57,000
But that doesn't answer the question as to why a certain individual was born in a certain body.

83
00:05:57,000 --> 00:06:02,000
If we understand that at the moment of death, there's a rebirth.

84
00:06:02,000 --> 00:06:07,000
Why does the person get born in a specific birth?

85
00:06:07,000 --> 00:06:13,000
It's obvious why this person is intelligent or is unintelligent because of their parents and so on.

86
00:06:13,000 --> 00:06:21,000
But why did that person gravitate towards that environment at the moment of death or after they passed away?

87
00:06:21,000 --> 00:06:29,000
And the Buddha gave some specific teaching on this in regards to the various types of karma that one can perform.

88
00:06:29,000 --> 00:06:34,000
Why is why are certain people rich, why are certain people poor and so on?

89
00:06:34,000 --> 00:06:40,000
And the teaching on wisdom and stupidity or intelligence and stupidity has to do with learning.

90
00:06:40,000 --> 00:06:45,000
Has to do with the interest in learning because really everything has to do with your interest.

91
00:06:45,000 --> 00:06:52,000
If you're interested in anger, you're going to be born in a place full of chaos and suffering.

92
00:06:52,000 --> 00:06:56,000
If you're interested in greed, you're going to be born in a place of need and wanting.

93
00:06:56,000 --> 00:07:00,000
I've talked about these things before.

94
00:07:00,000 --> 00:07:13,000
But if you're interested in learning and if you're working, even if you suppose a person of low intelligence who isn't able to process arguments, their brain isn't functioning very well,

95
00:07:13,000 --> 00:07:20,000
it could be based on things they've done in the past life that have brought them to this situation.

96
00:07:20,000 --> 00:07:23,000
But if they apply themselves, then the intention.

97
00:07:23,000 --> 00:07:37,000
Once they're able to throw off this body and these genes and this organism, they'll gravitate towards a better one that's better able to process information.

98
00:07:37,000 --> 00:07:43,000
I think there's sort of a point there that just because you meditate doesn't make you brilliant.

99
00:07:43,000 --> 00:07:56,000
It's not going to give you a super high IQ necessarily because it depends on the ability of your organism to function, which can be quite heavy.

100
00:07:56,000 --> 00:08:07,000
It's possible that the clarity of mind can be impaired by the physical, by the chemicals and by this overwhelmingly heavy physical form that we have.

101
00:08:07,000 --> 00:08:20,000
So it's not to say that it's going to change your ability to memorize things or your inability to memorize things, your inability to think rationally.

102
00:08:20,000 --> 00:08:33,000
But whatever tools you're given you have, whatever body and brain and whatever ability you have can be greatly enhanced or used to its fullest.

103
00:08:33,000 --> 00:08:38,000
When I finished meditating, before I started meditating, I was in university.

104
00:08:38,000 --> 00:08:50,000
It wasn't because I was stupid, but it was because of my inability to organize my mind, organize my life and keep my mind clear.

105
00:08:50,000 --> 00:08:58,000
I was getting moderate grades, not terrible, but not overly impressive.

106
00:08:58,000 --> 00:09:04,000
Once I finished meditating, it wasn't really that my mind changed in any way.

107
00:09:04,000 --> 00:09:15,000
It was that I was able better to organize my thoughts and it's amazing how much better you are at organizing study time.

108
00:09:15,000 --> 00:09:18,000
You don't cram the way you used to.

109
00:09:18,000 --> 00:09:27,000
You know exactly how much information your mind can hold and you go in these spurts where your brain can take information and you put some information in it.

110
00:09:27,000 --> 00:09:45,000
And you're clearly aware when your brain can't take any more and you stop and you take a break and then you come back and so you're working at the optimum level instead of trying to force something that is against reality because of your inability to perceive things clearly.

111
00:09:45,000 --> 00:10:00,000
So the short story is that at the end of one year of study, there were, I took 11 courses in, I was supposed to take 10 courses.

112
00:10:00,000 --> 00:10:12,000
I took 11 courses and I got nine A plus's, one A and one A minus and the A minus is because the teacher didn't like Buddhists and she knew that I was Buddhist.

113
00:10:12,000 --> 00:10:17,000
I mean that was our argument but there was a Buddhist monk as well taking studies.

114
00:10:17,000 --> 00:10:24,000
I wasn't ordained at the time but he said oh it's because she doesn't like Buddhism and she was rather harsh on us.

115
00:10:24,000 --> 00:10:32,000
It was a religious studies course and she had studied other religions and was sort of intent on making us look bad.

116
00:10:32,000 --> 00:10:34,000
I don't know.

117
00:10:34,000 --> 00:10:48,000
It was nine A plus's, one A and one A minus, what can you say? It's not something that turned me into a genius but it certainly helped me organize my thoughts in a much better way than before.

118
00:10:48,000 --> 00:10:52,000
So I think that answers your question.

119
00:10:52,000 --> 00:10:57,000
What makes a person, the other question, what makes a person stupid is of course the opposite.

120
00:10:57,000 --> 00:11:08,000
People who are not interested in learning who, you know, those people who call you brainiacs or brainers or geeks or nerds or so on.

121
00:11:08,000 --> 00:11:12,000
People who set themselves on stupidity.

122
00:11:12,000 --> 00:11:17,000
People who take drugs and alcohol, alcohols are real give away.

123
00:11:17,000 --> 00:11:25,000
You're poisoning your brain and it's funny to me now because I was so intent on that before.

124
00:11:25,000 --> 00:11:32,000
That was before I went to meditate, that was our life, when alcohol was the greatest thing.

125
00:11:32,000 --> 00:11:39,000
And now to listen to people talk about alcohol as if it's a great thing and as if it's a normal thing, it's just ludicrous to me.

126
00:11:39,000 --> 00:11:43,000
It's funny how your intentions change.

127
00:11:43,000 --> 00:11:50,000
The idea of poisoning my mind, it's like sniffing glue really.

128
00:11:50,000 --> 00:11:55,000
That's stupid thing. So that's what leads you to be born stupid.

129
00:11:55,000 --> 00:12:04,000
It leads you to stupidity in this life but it changes your direction and it's what leads you to be born as animals.

130
00:12:04,000 --> 00:12:07,000
It's why your born is a dog or a cat.

131
00:12:07,000 --> 00:12:13,000
My teacher, one morning there was a cat sitting on his pillow when he gave a talk.

132
00:12:13,000 --> 00:12:21,000
And after chanting, this is what we're doing, morning chanting and after morning chanting, he always gives a talk.

133
00:12:21,000 --> 00:12:31,000
And he gave a talk about this cat. He said, you know, I look at this, looking at these cat, looking at animals.

134
00:12:31,000 --> 00:12:38,000
And he said, you know, I just look at this cat and you can see there's no wisdom there.

135
00:12:38,000 --> 00:12:45,000
And it really struck me because I couldn't see that. I look at cats and I see a cat.

136
00:12:45,000 --> 00:12:52,000
This was something that I thought about for quite a while and I was looking at cats and I really couldn't see it.

137
00:12:52,000 --> 00:12:58,000
Can you really tell from the outside that a cat is really stupid?

138
00:12:58,000 --> 00:13:05,000
And I think most people would deny this. No, no cats are very intelligent, dogs are very intelligent and so on.

139
00:13:05,000 --> 00:13:14,000
And it's something that is quite profound and I would challenge people to examine that.

140
00:13:14,000 --> 00:13:26,000
That's not really a part of your question but if you look at animals, you can see that they're on another level.

141
00:13:26,000 --> 00:13:35,000
They're thought process is very one pointed and they're not able to expand upon that.

142
00:13:35,000 --> 00:13:45,000
They're not able to, for instance, think about meditation. Their mind is very, very, very low, very simple and very stupid in a sense.

143
00:13:45,000 --> 00:13:51,000
When we say a dog is clever, it's smart because it can sit. We say, oh, look at my dog is so smart, it can sit.

144
00:13:51,000 --> 00:13:59,000
But if you had like even a three-year-old and that was all they could do, they were retired. They were mentally challenged or so on.

145
00:13:59,000 --> 00:14:07,000
I mean, you would say there's a problem with them. They're not normal.

146
00:14:07,000 --> 00:14:31,000
Just a point there that if you're really interested in those kind of things in interested in stupidity, interested in poisoning your mind and interested in entertainment, mindless entertainment, slapstick and comedy and so on, and not interested in bettering yourself and in learning more and so on,

147
00:14:31,000 --> 00:14:42,000
that this is one of the easiest ways to be born as a cat or a dog or other animal that is very much interested in such simple pleasures.

148
00:14:42,000 --> 00:14:50,000
Up to you. I know there are people who think being born a dog or a cat would be wonderful. I'm not one of those people.

149
00:14:50,000 --> 00:15:09,000
And this is Ask a Monk, so thanks for the question and all the best.

