1
00:00:00,000 --> 00:00:12,060
broadcasting live

2
00:00:12,060 --> 00:00:16,820
Forche

3
00:00:16,820 --> 00:00:44,100
And there we have a quote about the body, bodily purity.

4
00:00:44,100 --> 00:00:50,340
How do you clean the body, huh?

5
00:00:50,340 --> 00:00:51,340
How do you clean the body?

6
00:00:51,340 --> 00:01:01,380
It's a loaded question from Buddhist point of view.

7
00:01:01,380 --> 00:01:08,900
We were talking on Monday about, we've just been reading chapters 20 to 25 of the

8
00:01:08,900 --> 00:01:33,220
Lotus Citra. And in one of those chapters, there's a monk or a monk who, a man, somebody

9
00:01:33,220 --> 00:01:49,460
who lights their body on fire as an offering to the Buddha, burns themselves completely.

10
00:01:49,460 --> 00:02:04,420
And then there's another person who burns their forearms off their forearms completely.

11
00:02:04,420 --> 00:02:08,180
And then make some kind of a vow and they come back.

12
00:02:08,180 --> 00:02:20,260
I mean, honestly, I have less and less good to say about this Lotus Citra the more I read it.

13
00:02:20,260 --> 00:02:29,180
Got an interesting conversation going about the nature of the body according to Buddhism.

14
00:02:29,180 --> 00:02:39,900
Because a common thought or a train of thought for spiritual people, especially in the

15
00:02:39,900 --> 00:02:43,900
West, is that the body is somehow sacred.

16
00:02:43,900 --> 00:02:59,140
You know, it should work to purify the body, you should guard the body as a vessel.

17
00:02:59,140 --> 00:03:04,820
And so the idea that spiritual beings would use part of their body as an offering.

18
00:03:04,820 --> 00:03:12,180
I mean, it's actually common in Mahayana, even today, to offer like a finger, and you

19
00:03:12,180 --> 00:03:17,260
just stick a finger into a candle and burn it up, and it's an offering to the Buddha.

20
00:03:17,260 --> 00:03:21,700
So some kind of, so we are going whether it is an offering because you're not actually

21
00:03:21,700 --> 00:03:27,780
giving anybody, anything to anybody, nobody's benefiting from your burning of a finger,

22
00:03:27,780 --> 00:03:40,260
or whether it's just a measure of your determination, devotion, it's a measure of devotion.

23
00:03:40,260 --> 00:03:48,380
But the idea that you should do this, whether or not Buddhists like me would agree with

24
00:03:48,380 --> 00:03:58,060
that, with doing that, we at least can agree that you're using your body in some way

25
00:03:58,060 --> 00:04:06,700
to benefit someone else, or using your body in any way really, is a lot like using any

26
00:04:06,700 --> 00:04:08,380
other tool.

27
00:04:08,380 --> 00:04:10,980
We don't see the body as somehow special.

28
00:04:10,980 --> 00:04:18,340
I mean, it's categorically different from other things because there's experience

29
00:04:18,340 --> 00:04:28,140
surrounding it, direct experience, experience of pain and pleasure.

30
00:04:28,140 --> 00:04:37,580
But as an entity, the body is something you can give away, organ donation, blood donation,

31
00:04:37,580 --> 00:04:42,300
these sorts of things, I think, are very much in line with Buddhism.

32
00:04:42,300 --> 00:04:49,420
We talked about this in Thailand, how it's a giving blood, it's a great thing.

33
00:04:49,420 --> 00:04:53,860
All the monks and nuns I went with and we went together to the hospital to give blood

34
00:04:53,860 --> 00:05:05,860
because we considered that to be a great thing that we could do, whether it's right

35
00:05:05,860 --> 00:05:13,340
or wrong for monks and nuns to give blood, I'm not sure, but we all did it.

36
00:05:13,340 --> 00:05:23,340
But no, the idea is that the body is just another thing and more to the point, it's inherently

37
00:05:23,340 --> 00:05:27,940
impure, there's nothing pure about the body.

38
00:05:27,940 --> 00:05:34,300
If you want to get technical, it's a bit made up of the four elements and so there's

39
00:05:34,300 --> 00:05:40,020
nothing in disgusting about it, technically.

40
00:05:40,020 --> 00:05:53,060
But that being said, it's made up of all the numbers of things that are at the very

41
00:05:53,060 --> 00:06:00,660
least without the ability to provide satisfaction.

42
00:06:00,660 --> 00:06:07,940
But that in general, are considered repulsive, you're going to clean out the body when you

43
00:06:07,940 --> 00:06:17,820
have to get rid of all the blood and pus and snot and bile and urine and feces, in a

44
00:06:17,820 --> 00:06:28,940
conventional sense, the body is terribly impure.

45
00:06:28,940 --> 00:06:37,740
In another sense, in a sense that the Buddha is talking here, it is possible to purify

46
00:06:37,740 --> 00:06:38,740
the body.

47
00:06:38,740 --> 00:06:43,900
It's totally, totally not what you would most people would think, it's a completely different

48
00:06:43,900 --> 00:06:48,540
way of looking at purity and looking at the body.

49
00:06:48,540 --> 00:06:51,260
So how do you purify the body?

50
00:06:51,260 --> 00:07:06,820
Stop killing, periodion in purity, I have nothing to do with the nature of the thing.

51
00:07:06,820 --> 00:07:16,340
Purity in purity in regards to the physical realm, have everything to do with the use.

52
00:07:16,340 --> 00:07:21,180
Don't purify the body, don't use it to kill, don't use it to steal.

53
00:07:21,180 --> 00:07:35,060
Don't use it to commit sexual misconduct to commit adultery, don't commit immoral acts of

54
00:07:35,060 --> 00:07:38,540
body.

55
00:07:38,540 --> 00:07:44,100
Purity for the material realm is in the use you give.

56
00:07:44,100 --> 00:07:49,340
So this goes not only for the body, but it goes for all other things we use.

57
00:07:49,340 --> 00:07:55,060
It is in this way not categorically different, it's just another thing that we use.

58
00:07:55,060 --> 00:08:03,220
If you want to talk about purity of your possessions and purity of robes, are my robes

59
00:08:03,220 --> 00:08:04,220
pure?

60
00:08:04,220 --> 00:08:10,540
Well, they're 100% cotton as far as I know, but that's not what makes them pure.

61
00:08:10,540 --> 00:08:23,060
For pure, if I use them for pure, for pure purposes, if I use them for play or for fun

62
00:08:23,060 --> 00:08:29,980
or if I enjoy them or if I find robes that are only robes that are beautiful or pick

63
00:08:29,980 --> 00:08:38,620
my robes based on how beautiful and how comfortable and how pleasing they are, then that's

64
00:08:38,620 --> 00:08:45,140
the impure, no matter what they're made up.

65
00:08:45,140 --> 00:08:53,660
If I use my food, the food that I get, if I use it for enjoyment or for intoxication

66
00:08:53,660 --> 00:09:04,700
or for fat and thing up, I use it for excess, if it's impure food, that's what makes

67
00:09:04,700 --> 00:09:13,580
food impure, it's not because it's got this ingredient or that ingredient, or shelter

68
00:09:13,580 --> 00:09:19,060
or shelter is pure because of how you use it, to use it to hide your evil deeds or do

69
00:09:19,060 --> 00:09:29,100
you use it for seclusion, to cultivate meditation, medicine, do you use it to heal the body

70
00:09:29,100 --> 00:09:38,500
or do you use it to become addicted to painkillers or to avoid the problems, to avoid

71
00:09:38,500 --> 00:09:41,100
suffering?

72
00:09:41,100 --> 00:09:50,860
And by extension, everything else that we use, including the body, so what are you using

73
00:09:50,860 --> 00:09:53,380
the body for?

74
00:09:53,380 --> 00:10:00,860
Do you have this body as a tool or are you using it to do evil things or are you using

75
00:10:00,860 --> 00:10:11,220
it to gain purity or are you using it as a tool, as a vehicle, doing walking meditation,

76
00:10:11,220 --> 00:10:21,220
doing sitting meditation, are you using it for the right purposes, then it's pure, then the

77
00:10:21,220 --> 00:10:24,740
body is pure.

78
00:10:24,740 --> 00:10:40,940
So, simple dhamma for tonight, I'm sorry I haven't been all that.

79
00:10:40,940 --> 00:10:47,380
I really want to do these sessions under them often, and I'll try to get back into the

80
00:10:47,380 --> 00:10:52,620
video, but this month is going to be really, really busy.

81
00:10:52,620 --> 00:10:58,000
I made a mistake of taking two upper-year courses, even though I'm only taking three

82
00:10:58,000 --> 00:11:09,740
courses, two of them are, I've got two essays, two papers to write, and the piece symposium

83
00:11:09,740 --> 00:11:22,220
we're organizing, and yeah, so hopefully I'll still have time to do all this, but the next

84
00:11:22,220 --> 00:11:29,820
week is going to be pretty harsh, we have to buckle down and do some work, especially

85
00:11:29,820 --> 00:11:39,020
because I missed a week, anyway, none of you are concerned, I'll try and be here.

86
00:11:39,020 --> 00:11:48,740
As much as possible, maybe we'll do some more dhamma panda, but we'll see how it goes.

87
00:11:48,740 --> 00:11:58,540
Anyway, thank you all for tuning, oh wait, right, next part, I'm going to post the hangout,

88
00:11:58,540 --> 00:12:07,860
so anybody who wants to come on the hangout can come on, got news, tonight Robin just

89
00:12:07,860 --> 00:12:15,380
finished her course, she's here, maybe you know Robin's, but it's Robin's, we let her

90
00:12:15,380 --> 00:12:23,940
out of her room this, today, isn't she look happy, this is, she's still meditating, so I

91
00:12:23,940 --> 00:12:33,860
don't want to bother her, maybe we'll have a chance to talk before she leaves.

92
00:12:33,860 --> 00:12:42,740
So come on, if you have questions, come on the hangout, which is posted at meditation.seriamongalove.org,

93
00:12:42,740 --> 00:12:52,900
we've got 10 viewers on YouTube, small group, a lot of meditators, which is nice, long

94
00:12:52,900 --> 00:13:10,620
list of meditators tonight, all right, well if no one's coming on, I'm going to go, I'll

95
00:13:10,620 --> 00:13:30,340
be back tomorrow, have a good night, everybody.

