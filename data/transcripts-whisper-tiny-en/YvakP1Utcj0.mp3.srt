1
00:00:00,000 --> 00:00:03,600
Not knowing much about the differences between various traditions,

2
00:00:03,600 --> 00:00:07,600
I started to meditate at the local Shambala Center a while ago.

3
00:00:07,600 --> 00:00:12,400
This is a Tibetan tradition and the practice is primarily samata meditation.

4
00:00:12,400 --> 00:00:15,800
At home, however, I study and practice in the tradition you teach.

5
00:00:15,800 --> 00:00:21,400
I basically started doing the Vasana meditation there some months ago.

6
00:00:21,400 --> 00:00:29,000
Question, should I continue going there and doing my thing?

7
00:00:29,000 --> 00:00:42,000
I'm not aware of any Mahasi Sayyados center anywhere near where I live.

8
00:00:42,000 --> 00:00:57,000
It's hard to say if they let you practice, if they let you practice.

9
00:00:57,000 --> 00:01:05,000
If they let you practice as you do, and if you think is right,

10
00:01:05,000 --> 00:01:13,000
you seem to be quite dedicated to the Vasana meditation.

11
00:01:13,000 --> 00:01:25,000
Then you can continue to go there, but if they want you to practice their style of meditation

12
00:01:25,000 --> 00:01:33,000
or that technique of meditation, and you just go there because you have no other option,

13
00:01:33,000 --> 00:01:40,000
then it's kind of not so beneficial for both.

14
00:01:40,000 --> 00:01:47,000
But as I said, if they are fine with it when you practice a Vasana meditation

15
00:01:47,000 --> 00:01:54,000
while they do something else, then it can be beneficial to have a group to sit with

16
00:01:54,000 --> 00:02:02,000
and not to sit at home alone, just to have that effort to go and see people

17
00:02:02,000 --> 00:02:13,000
and to actually really sit in meditation instead of sitting on the sofa and watch some TV.

18
00:02:13,000 --> 00:02:29,000
So for that it would be good, but I wouldn't recommend to interrupt there to use them only.

19
00:02:29,000 --> 00:02:33,000
Especially if they have expectations and you're kind of pretending,

20
00:02:33,000 --> 00:02:35,000
because then it's a little bit done.

21
00:02:35,000 --> 00:02:41,000
It just creates worry in your own mind.

22
00:02:41,000 --> 00:02:46,000
I mean, you're welcome to send me an email, I can often locate the nearest Mahasi Center

23
00:02:46,000 --> 00:02:50,000
or something that I wouldn't recommend.

24
00:02:50,000 --> 00:02:56,000
And of course, close is all relative if you can put together enough money for a plane ticket here.

25
00:02:56,000 --> 00:03:19,000
We've got good days available.

