Okay, good evening everyone, broadcasting live, Aldio, we're going to do another dumb
fun video.
Hello and welcome back to our study of the Damapada.
Today we continue on with verse 125, which reads as follows.
Good evening and greetings from
who should attack or offend against the city, a person who is without fault up and with
a person who is soon to have pure and an anger that would fall to their blame.
The Meevabalaam patit bhapam, such a fud, that evil bhapah, patit goes back upon the Meevabalaam,
that false.
The evil goes back upon that false.
So if you throw dust against the wind, it comes back into your face, in the same way evil
done to someone who is blameless, comes back to baiting.
So quite memorable words, one of those things for us to keep in mind that it's a description
of the law of karma, that's what this refers to.
But it's even more because the idea is that evil doesn't harm the one who is the object
of evil, evil harms the one who performs the...
In sometimes in seeming magical ways, which is sort of what this story behind this
verse is about, so this story goes that there was a hunter named Koka, and he was...
He had...
The hunter was accompanied by a pack of hounds, so when he would go hunting, he would take
dogs with him and the dogs would pick up his prey or maybe even help him kill it, drag
it down for him to kill, and so he had trained him to be quite vicious.
One day when he was doing his hunting and the forest, he came across a monk, and somehow
he had this idea that seeing a monk was a bad woman, seeing a monk in the morning was...
This idea had been spread about, maybe by enemies of monks or enemies of Buddhism, and
he thought this was a bad woman to see a monk, and so the monk went on his way to...
For arms in the city, and the hunter went on his way to try and catch prey and churned up
the whole day, he caught nothing.
Meanwhile, the monk came back and he just lunged and was perhaps meditating in the forest,
and the hunter, on his way back to the forest, got nothing, he caught sighted the hunter
and got sighted the monk again, and was furious, and thought this monk, the reason, this
monk is the reason why I got nothing, somehow he had this idea, and so he yelled at the
monk and accused the monk of being the cause of his bad luck.
He brought his hounds out and he was ready to set his hounds on the monk, and the monk
begged him, pleaded for him not to do such a horrible thing, and he was innocent that
he had not done anything against the hunter.
Part of it may have been because Buddhist monks were known to be against killing, they
thought people to give up killing, and so as a result there was this idea that somehow
they were bad luck as a result, because it was something that they believed was wrong.
Anyway, this hunter refused to listen to the monk and set his dogs on the monk and
the monk immediately ran and climbed up a tree, and was standing over to one of the branches,
and so what the hunter did, I was so angry, he was really out to get this monk, and
so he took it one of his arrows, and he poked up at the monk's feet, and first the
monk's right foot, and the monk lifted up his right foot, and put his left foot down
instead, and the hunter poked his left foot, and really gouged his feet, and so then
the monk pulled up both feet, but he was in so much pain, and he started to lose mindful
ness, awareness of what he was doing, and he dropped his outer robe, the big robe that
would act as a blanket, and it fell on the hunter, and covered him, and immediately
the dogs turned and looked at the hunter and thought the monk is falling out of the tree,
and they turned on the hunter and ripped him to shreds and killed him, thinking they
were following the master's orders, because they smelled this robe, and the monk looking
down at what was going, he picked up a stick, and he threw it at the dogs, and the dogs
looked up and saw the hunter, the monk was still in the tree, and the scampered off into
the forest, confused, or maybe embarrassed at how foolish they had been, you know, it's
the story.
And the monk actually felt bad about this, he thought, well, you know, it's because
of me that this hunter died, it was really my fault, it was because I dropped this robe
on him, because it was unmindful, is that, do I have any fault in that, and of course
the Buddha said, no, no, be who you are, blameless, and moreover, this, he said, this isn't
the first time, and he told the story of the past that apparently in the past life, this
guy had the same sort of thing happened, and he was a cruel, terrible person, and the same
sort of thing happened, and he was a physician, and he was trying to sell his cures and
sell his services, and no one was buying it, and no one was sick, and he thought, well,
maybe I'll just make people sick, and get them to, then they'll need me, they'll need
my help, if they're sick, and so he took a poisonous snake, and he stuck it into tree,
and he told these boys to go and fetch him this, that there was a bird in there, in
this hole, and they should go and fetch it for him.
And so one of the boys stuck his hand in the tree, and grabbed the snake, thinking it
was this bird that he was going to catch, and when he realized it was a snake, threw
it, freaked out and was really quickly, and it hit the position in the head, and wrapped
around his head, and bit him, and killed him, kind of silly stories, but there's a
story of the hunter, and the hunter and the monk in the tree is a memorable one, how evil
can go wrong, how our deeds sometimes turn back on us, now there are different reasons
why we could say this happens, I mean the most obvious reason for our evil deeds to bring
evil upon ourselves is the effect that they have on our minds.
So anger, and greed, and delusion, the three developments make you care less, when you're
obsessed with something, or you're angry, it's bad karma first and foremost, because
of how it affects your mind, and because of how it changes you, and changes the actions
and the choices that you make.
Furthermore, it does affect the world around you, the second way it does I think is in
how others look at you, so in this case there were no other people involved, but by
disrupting the monk, he brought upon himself at least you could say some sort of chaos,
in which many things are possible, when you attack someone, sometimes they freak out
and it disturbs the order, but you know see I'm trying to get to the point where you
could actually see what this is, a result of his bad karma, because it doesn't seem
like it is, it seems like it was just a freak chance, and 9 out of 10 times, or 99 of 100
times, or 999 out of 1000 times, the hunter would have killed the monk in the tree.
What we want to say is that there is potentially things protecting people who are good, people
who are blameless.
I don't know that you can actually, I don't know that it's actually proper and true
to say that, we want to, and Buddhists like to try and say that, that there are things
protecting you.
I'm not so sure, I would agree that there are probably beings, like who knows, maybe
there was some angel or spirit involved with this, that pulled the robe off the monk
and dropped it on the hunter.
That makes sense to me, if such beings exist, of course, many, many people think it's ridiculous
that such beings would exist.
I'd agree that it seems kind of fair-fetched that such a being would just be there and
get involved, but hey, who knows, there's that kind of thing, and so that's a direct
result of karma, people don't, other beings don't appreciate when you're cruel to people
who don't deserve it.
And that sort of thing really is a part of karma.
Good people attract, others who would protect them, attract good friends, and so if you
try to harm a harmless being, you're less likely to succeed just because they're surrounded
by good people.
But I think you could also argue that the power of goodness has some sort of reverberations
in the universe, like it sets things up in a certain way.
I think you could argue that, I think you could point to ways in which it's true that
a person who has lots of good deeds, a person who's never done the evil deeds, is somehow
in a different sort of train or groove, you might say, from a person who is intended
on evil.
They're so opposite that they're almost living in different universes, and it's very hard
for them to come together now that's possible.
But I think you could point at that as being a reason why it takes really great amount
of effort to harm someone who is harmless.
I don't want to say it's certainly not magic.
And I think there's much, much more going on, and we often will, it's common for people
to underestimate the power of goodness and the power of evil, evil is something that shrinks
inward or it comes back upon you, good is something that's expensive and it's spreads.
So you can't, spreading evil is not the same as spreading good, it's much more likely
to come back upon you.
I think at any rate, I would argue that there's room for that, I think it's something
that is very hard to find proof or evidence even for, but I would say that there's room
for the idea that good protects goodness, protects good people, and people who try to do
evil towards good people for various reasons, have a harder time of it, and it's much
more likely to come back and bite into the face.
I mean, absolutely even when they get away with it, it's far worse, and that there's
no question that it eventually comes back, far worse on the person who's done an evil
deed towards someone who doesn't deserve it, who hasn't done anything to instigate it,
who is a blameless.
Like if this monk had gone around setting animals free, well then you could say maybe
somewhat metal some, or if he had purposefully wished or if he had done everything he couldn't
to ruin this hunter and to do harm him, he'd done something to warrant even littlest bit
of malice, then it would be different, you could argue, and it would be less of a weighty
crime, but to harm someone is harmless, you know it in your bones, you don't have to be
Buddhist to believe this, it's just far more horrific to harm someone, almost than nothing
to deserve it.
If for that reason, for sure it's going to affect your mind and make you blind to the
dangers that you're facing, and blind to your actions, and blind to the circumstances and
so on, I think if it argue more over that it's such a powerful act that it jars with
the universe, as a result is harder to perform, I'd like to think that there are things
that protect people's lives, just aspects of the universe that are fit together in
such a way that it's not as easy to kill someone as we might think, especially if they
don't deserve it.
The idea that, to some extent, most dealing, most deeds for good or for evil have something
to do with past karma, the idea that there is some sort of connection there, I don't
think all, I think it seems possible to create a new karma with someone, so if someone
is innocent, you can harm them, it's a very weighty karma, but you're starting something
new, and it's quite awesome that they will come back and harm you, even unintentionally,
like in this case, usually it takes lifetimes to do that, but anyway, these are aspects
of karma that are kind of magical and hard to understand or hard to believe, or people
don't think of them as true, but this isn't so important for our practice, what's important
for our practice is as with all of these sorts of stories, the consequences, at no question
there are consequences to our our evil deeds and our good deeds, and I think that's
exemplified here, well, if you're a good person, you don't deserve to be harmed, it's
harder for people to harm you, it's harder for people to wonder, it takes a really base
and evil person to do it, and you've got to wonder whether this monk was pleading with
this hunter to stop, for his own benefit or for the benefit of the hunter, because certainly
even death, even death by dog, by wild, rabid dogs, is preferable, we don't think of it
this way, but it's actually far preferable than to torture an innocent person, because
by being, the Buddha said many things like this, he said you should, you should rather be
killed and or tortured than to do an evil deed, because being killed and tortured doesn't
lead you to a bad future, doesn't hurt your mind, doesn't affect you in the way that
evil does, so I mean, it's a big part of meditation that a beginner has to come to understand
that pain in the meditation, when you're sitting in meditating and you feel pain, pain
is not your enemy, unpleasantness of all kinds, it's itching or heat or loud noise
or thoughts, nightmares, visions, etc. This is not the enemy, this is not the problem,
the problem is evil, what we call unwholesomeness, so greed, anger and delusion, these are
problems, they call, they bring about things like pain and stress and suffering, if
becoming to see that difference is very important, to see that it's the evil that we have
a problem with, so this quote is describing a very extreme sort of karma, the deed, the
evil deed that is performed towards someone who doesn't deserve it, but it brings up all
these issues, who is the person who really receives the effect of a deed, if you try
to harm someone, what they might be, a hurt or even killed, but that's it, it's actually
quite insignificant compared to the evil that comes back upon you, so this is throwing dust
in the wind, if you're a dust in the wind it comes back on you, evil deeds are very
much like that, don't think of it, right? I think if you could get away with it, hurting
someone else really bad for the person who you hurt or killed, it's actually very much
the other way, that's what this verse teaches, this is the claim that's being made, and
I think there's evidence, especially for meditators, that this is very much, I know the
evidence, is very much the case, it's just trying to get the idea that maybe there's even
more than just the obvious, the scars it leaves on your mind, because that's the worst,
but it seems that actually there are cases of being protected when harm someone, it actually
physically hurts you right then there, that's considered to be Dita dhamma reed in the
outcome, a comma that gives results in this life, right here and now, as opposed to the
indirect, where it changes you and those, the result you make different life choices and
other people make different life, different choices in regards to you and how they think
of you, and eventually it harms you in an indirect way, either way, this is all part
of the concept of good and evil, in Buddhism, in our meditation practice, we strive for
good, and we do it rationally, because we're told it's good, or because it's good in
another self, because we really believe that it's the way to peace, happiness, and freedom
from suffering, so that's our practice, and that's the dhamma pattern for tonight, thank
you for tuning in, wishing you all good practice, and peace, happiness, and freedom from
suffering, thank you.
Okay, we can now go live with a video, everyone broadcasting live, January 7, 2016, with
me tonight is Robin, broadcasting live, so if you have questions, you come on over to
meditation.ceremungalow.org and you can ask them there, no, we're not answering questions
on YouTube, today we had a meeting of the McMaster Buddhism Association executive, so we
are, our plan is to, we're going to have meetings weekly, and there's a second I have to
turn this stuff up, I forgot, it's great, we're good, so we're going to have meetings,
to just to get to know people, to bring people together, maybe we'll give some teaching
on Buddhism during the meeting, and we talked about these five-minute meditation lessons
in the table, have to put the banner up, Robin, I'm sorry, but something that order went
all wrong, because I've got rope and I've got a banner and there's no way to connect
the two together, no, like when I did it, when I did a mock order and went through it,
you'd ask me if I wanted to ram it, so our sticky things, you didn't get that, so did
you pick the sticky things up, I mean I didn't order it, I mean I just, I didn't decide
if you wanted grommet, so it just has to, we want your rope, or as it dies, so maybe I missed
something, can you just cut the holes in it, or would you ram it, it doesn't really work
though, and the best thing is to punch holes and make grommet, get grommet, maybe we should
do that, and probably what I was going to do is just use duct tape to pass it to the
holes, let's check on the website and see if this can be sent back, it looks like you have
the grounds for it, you know, it's not any time for next Tuesday, I know how to use
grommet, maybe we can go to the hardware store and get some grommets, it seems possible,
or like the velcro, sticky things on the back, but you don't have the prommets at the University
Summit, you know, that's interesting, but yeah, no, it's going to be in front of a window
actually, probably, if you big dowel, along dowel, what does that do, if you had a dowel,
I don't know how long the banner is, but if you had a dowel you could sew it on to it
and then it would be like a stick, put a string on it, put it in it, sew it, and make
it, you know, I think I'll use duct tape, duct tape sounds good, and we have questions tonight,
I don't know, I don't know, we've done videos on music, yes, let's leave it at that,
is it possible that someone in meditation who has eliminated all desire could be so absolutely
immersed in mindfulness of what things are, but he dies dehydrated for not drinking water,
do you know of any case in which this has happened, is this a person who's meditative with
it? I mean, it sounds like a bit of a misunderstanding of what mindfulness is, but maybe not,
I mean, okay, it's not really mindfulness that you're talking about, it's because mindfulness
can involve all of these awarenesses, it's more like focus, which you know can come from mindfulness,
can come from mindfulness practice, I don't think so, so mindfulness brings you
into the present moment, I mean, just off the cuff, I think technically I get what you're
saying, but really practically speaking, it's not the way it goes, I don't have the technical
aspect of why it is so, but the meditators are more aware of the wants and needs of the body.
Now they might consciously choose to ignore them, but they wouldn't suddenly say, oh, I'm
dehydrated and I didn't know it because I was just experiencing it as sensations or something.
That would require intense concentration that I don't think is really a part of insight meditation.
So mindfulness really helps you, you're super conscious of the fact that there's the dehydration
going on and all the aspects of knowing that it's being, that it's you being dehydrated
are all still likely to be there, but it's just the knowledge of the concepts in the mind
that those concepts could be absent, but I don't think they're more likely to be absent
just because you're mine. I don't think it goes to that extent in most cases, and that
I'm willing to entertain that it's possible. Near the hand, why the heck are you asking
such a question?
I have been wanting to go down the path of the Buddhist monk for years now. I have
even planned to go to Nepal to travel and learn the best I could before becoming a monk.
There is one thing troubling me though, is a relationship with another person having children.
I'm not with anybody, I'm just scared of getting it to a relationship and then later
I'm like hurting that person because I would like to leave and do as I wished before I become
a monk. I don't know if I'm overthinking this or not. This has been on my mind and I'm
having troubles except in meditation.
I'm quite understand, but I think it's like you're afraid of getting into a relationship
because you might become a monk. Is that right? It's not really... I don't know what
I have to say to you. I mean, so don't get into a relationship. What can I do for you?
If you mean like having trouble because you want to get into a relationship but you also
want to become a monk, well, good luck. Probably you want to become a monk. It's easy
to actually do it.
This is a question I actually asked to me from Ryan. I looked on an Expedia today and
round trip tickets to from Toronto to Bangkok started around $1,000. With that being said,
what do we need to do to get the ball rolling to buy Monday to get to see Ajahn Thong? Is
it just as simple as setting up another eukaryon campaign? Let me know whatever I can do
to help. It is and it isn't simple. Only in the timing of it is many of you know we rented
a house in the organization rented a house in September to set up four months during meditation
center and we did a fundraising drive which provided funds for about nine months of operation
at the monastery which was wonderful but it's not enough to keep the monster gun perpetually.
So we actually had planned for February to do another fundraising campaign to judge continued
support of the monster in meditation center to see if it can keep going perpetually and
that's really crucial because you know it was wonderful the initial support but what's
crucial is whether there's enough support to keep it going. So that's kind of what the
organization was gearing up to do to do another fundraising drive or the one stirring
meditation center. So you know what? Well the trip sounds wonderful. It's just the timing
of it is a little bit premature. After we do the second fundraising campaign and see if there
is continued support to keep the monster in meditation center value and that would be
you know a great time to talk about extra you know extra things like a trip to Asia which
it would obviously be a wonderful thing for Monday to go see a teacher and we see the blessing
and everything. It's just a little bit premature at this point I think but you know it's
certainly not up to me. It's a joint decision with the directors of Sermon Global International
which includes Monday so you can certainly chime in on that too. Yeah I don't want to talk about those
things. Leave it to you thank you. All things in good time we just need maybe just a little
bit of patience right now to see you know what what sort of support is available is continuing
support for the monster in meditation center. We'll be rolling that out in a couple of weeks. We had
planned it as a fall campaign and a spring campaign. The spring campaign beginning in February
and a couple of weeks. I guess I'd just say that it's hard to rationalize it you know flying
flinging yourself across the from one end of the earth to the other and the purpose isn't
you know it's not like my teacher doesn't know about this monster he doesn't know what I'm doing
I'm sure he'd like me to come. I would like to go but it doesn't really seem worth the
difficulty worth the trouble especially when we have other troubles here or challenges here that we're
facing. We actually don't know if we have troubles or challenges that are you know if you're
supporters your followers are very generous. We haven't we simply haven't asked what the set up for
fundraising campaign it was a limited online campaign. There's always a support page at the website
surmunglo.org but that doesn't tend to be something that people I think are very often so
well there's always an opportunity to support not many people actually go to the support page
at the surmunglo.org website so based on that we're just old and concerned because there hasn't
been much support in the past couple of months but we didn't have an active fundraising campaign
that was kind of out there and being promoted to ask people some. We don't know that there's a problem
there could be no problem you know a month from now come on straight but we funded for another six
months and all as well. I'm sure the organization would really like to send you to Asia to
see how John Tom is just a little bit you know timing. Yeah I mean I wasn't expecting to go
I said that I wasn't planning to go to Asia in the near future just kind of came up. Also there's
might be half part or partly paid for we'll see that would change things I suppose.
Yeah that would change things quite a bit because that would make it you know a very affordable
way to go over and see your teacher which would be wonderful. Right so there's that that's a
good reason you know if I got this opportunity where half of it's going to be paid for well
yeah it's better than waiting until we have to pay for all of it right? Yeah yeah okay anyway
that's all they're talking about money stuff too much where people think where that's what our
focus is it's not. Anyway it looks like that's it right? Yes. All right because we did
them up at it tonight so that's going to go up. Otherwise I think Saturday I'll try to do
the Buddhism 101 videos so I have to look and see where I left off many years ago
and pick that up or we left off try and get through that. I kind of left off because I was having
trouble figuring out how to address it from a modern perspective. I think it's the next one is
talking about heaven which I have to study it a little bit and try and get a grasp of exactly
what concept the Buddha is trying to trying to pass on and how to translate that from modern
viewers such that no because most people aren't thinking too much about heaven so how far do we
want to take that? Do we want to have people focus on the idea of heaven as a part of Buddhism?
I think so to some extent but I want to bring in the bigger concept, bigger context.
I think karma I think the next one is going to be about karma as a result
that the results of our good deeds. Why should we do good deeds?
Okay anyway good night everyone thank you all for tuning in thanks Robin for your help.
That's what you want. Thank you Bhante. Good night.
