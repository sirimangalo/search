1
00:00:00,000 --> 00:00:17,080
Good evening everyone from broadcasting live, May 20th.

2
00:00:17,080 --> 00:00:28,000
Today's quote is from the Majimalikaya, number 122, the Mahasun Yaptasup, which is

3
00:00:28,000 --> 00:00:41,880
a lot in it, and the very last thing the Buddha says is, I'm not going to treat you

4
00:00:41,880 --> 00:00:54,720
like wet clay.

5
00:00:54,720 --> 00:01:08,880
Thank you.

6
00:01:08,880 --> 00:01:28,880
Here we are, ta, ta, parakamizami, yatakumbakaro, amakai, amakamate, all right, nahwohang anandatatah, parakamizami.

7
00:01:28,880 --> 00:01:52,680
I will not treat you on anandatah kumakaro, amakai, amakamate, amakah means uncooked, un cooked, un cooked,

8
00:01:52,680 --> 00:02:05,280
or the unbaked clay, because the potter has to treat their clay with care, and the clay is wet.

9
00:02:05,280 --> 00:02:12,880
You have to, you have to be very careful with it, something very fragile, so the Buddha said,

10
00:02:12,880 --> 00:02:19,880
I am not going to treat you with as though you are fragile, means I am not going to go easy on you,

11
00:02:19,880 --> 00:02:38,880
I am not going to use kim gloves, I am not going to pam for you, I am not going to make it easy on you,

12
00:02:38,880 --> 00:02:48,880
so what he is saying, what he is making, he is by doing that, he is softening the blow of what he says.

13
00:02:48,880 --> 00:02:52,880
He is teaching, because the teaching is quite difficult as what he is saying.

14
00:02:52,880 --> 00:02:57,880
He has only our best interests, this is what he is saying.

15
00:02:57,880 --> 00:03:05,880
His meditation is tough, enlightenment to path to enlightenment does not an easy thing.

16
00:03:05,880 --> 00:03:13,880
He says repeatedly restraining you, I shall speak to you, repeatedly admonishing you, I shall speak to you.

17
00:03:13,880 --> 00:03:20,880
The sound core will stand the test.

18
00:03:20,880 --> 00:03:40,880
The sound core also means the core, but it also means that which has substance, the person has substance, they will stand.

19
00:03:40,880 --> 00:04:03,880
He is addressing monks who are spending time in society, are in danger of getting caught up in the world, and he wants to

20
00:04:03,880 --> 00:04:12,880
address them in such a way that they focus on things that are more saara, more beneficial.

21
00:04:12,880 --> 00:04:25,880
And so he offers them this teaching on voidness, this teaching on non-self really, that things are empty,

22
00:04:25,880 --> 00:04:32,880
all of the things that we strive for, all of the things that we cling to.

23
00:04:32,880 --> 00:04:36,880
They in the end don't have substance, they come and they go.

24
00:04:36,880 --> 00:04:43,880
We think of our belongings, our possessions, even our own bodies, and our own minds are unstable.

25
00:04:43,880 --> 00:04:52,880
We can't hold on to them, we can't keep them the way they are.

26
00:04:52,880 --> 00:05:06,880
He gives something that's hard to understand, but even hard to accept, let alone understand.

27
00:05:06,880 --> 00:05:21,880
Because we very much interested in and attached to the things in the world, in the ways of the world.

28
00:05:21,880 --> 00:05:34,880
And so even with meditation, we come with the idea that it's going to be pleasant and stable and controllable.

29
00:05:34,880 --> 00:05:38,880
We're going to find a way to control our minds.

30
00:05:38,880 --> 00:05:43,880
We didn't figure that controlling and letting go or two very different things.

31
00:05:43,880 --> 00:05:51,880
In fact letting go, it's much more difficult than two.

32
00:05:51,880 --> 00:06:00,880
Well, complete control being impossible, but the act of trying to control things is pretty simple.

33
00:06:00,880 --> 00:06:03,880
Just try and force ourselves this way.

34
00:06:03,880 --> 00:06:09,880
It's not very fruitful, but it's an easy way to just try and control your life.

35
00:06:09,880 --> 00:06:14,880
And then control everything, force everything to be the way you want it.

36
00:06:14,880 --> 00:06:17,880
In other words, try to always get what you want.

37
00:06:17,880 --> 00:06:20,880
Never get what you don't want.

38
00:06:20,880 --> 00:06:22,880
Letting go is quite different.

39
00:06:22,880 --> 00:06:24,880
Letting go is much more difficult.

40
00:06:24,880 --> 00:06:27,880
It's difficult just to even accomplish.

41
00:06:27,880 --> 00:06:32,880
It means not reacting.

42
00:06:32,880 --> 00:06:34,880
It's not judging.

43
00:06:34,880 --> 00:06:41,880
It's not clinging. It's not avoiding.

44
00:06:41,880 --> 00:06:46,880
It means learning to open yourself up.

45
00:06:46,880 --> 00:06:48,880
Open yourself up to what you're feeling.

46
00:06:48,880 --> 00:06:52,880
Open yourself up to what you're thinking.

47
00:06:52,880 --> 00:06:56,880
Opening yourself up to now what you're experiencing now.

48
00:06:56,880 --> 00:07:03,880
And being okay with that, whatever it is.

49
00:07:03,880 --> 00:07:05,880
Making your peace with it.

50
00:07:05,880 --> 00:07:07,880
So you stop trying to fix it.

51
00:07:07,880 --> 00:07:11,880
Stop trying to hold onto it.

52
00:07:11,880 --> 00:07:14,880
Stop trying to get what it is that you want.

53
00:07:14,880 --> 00:07:17,880
Your minds are screaming like little children all the time.

54
00:07:17,880 --> 00:07:18,880
I want this.

55
00:07:18,880 --> 00:07:19,880
I want that.

56
00:07:19,880 --> 00:07:20,880
I don't want this.

57
00:07:20,880 --> 00:07:22,880
I don't want that.

58
00:07:22,880 --> 00:07:27,880
Meditation is about growing up.

59
00:07:27,880 --> 00:07:37,880
It's about finally becoming mature in the sense of being strong and wise and capable.

60
00:07:37,880 --> 00:07:40,880
It's the most difficult thing you can do.

61
00:07:40,880 --> 00:07:45,880
A lot of blood sweat interiors along the way.

62
00:07:45,880 --> 00:07:47,880
Things are the Buddha says, I'm not going to make it.

63
00:07:47,880 --> 00:07:50,880
I'm not going to sugarcoat this for you.

64
00:07:50,880 --> 00:07:52,880
There's no way to sugarcoat this.

65
00:07:52,880 --> 00:07:55,880
We can soften the blow.

66
00:07:55,880 --> 00:08:02,880
We can remind you that it's important to see, well, that it's an important thing.

67
00:08:02,880 --> 00:08:05,880
It's a serious thing we're doing.

68
00:08:05,880 --> 00:08:10,880
Prepare you for it mentally, emotionally.

69
00:08:10,880 --> 00:08:14,880
But in the end, we have to go to the distance.

70
00:08:14,880 --> 00:08:18,880
If you want an experience that is pleasant and comfortable, that's easy.

71
00:08:18,880 --> 00:08:25,880
We can make a spa or something where people come and get massages or get it.

72
00:08:25,880 --> 00:08:33,880
Sit in the jacuzzi or hot spring or something.

73
00:08:33,880 --> 00:08:35,880
But it wouldn't solve anything.

74
00:08:35,880 --> 00:08:37,880
It wouldn't solve anything.

75
00:08:37,880 --> 00:08:40,880
Wouldn't fix your problems.

76
00:08:40,880 --> 00:08:42,880
Wouldn't lean to true peace.

77
00:08:42,880 --> 00:08:50,880
It wouldn't change anything.

78
00:08:50,880 --> 00:08:55,880
We can do comfortable meditation where we sit and think good thoughts.

79
00:08:55,880 --> 00:08:57,880
Well, in meditation, really.

80
00:08:57,880 --> 00:09:08,880
There's meditation of love and meditation on compassion and good meditation.

81
00:09:08,880 --> 00:09:18,880
But as well, they still don't lean to true change because they don't have this understand ourselves.

82
00:09:18,880 --> 00:09:24,880
I mean, the path to true enlightenment is not complicated.

83
00:09:24,880 --> 00:09:27,880
It's not hard to understand.

84
00:09:27,880 --> 00:09:32,880
It's not esoteric.

85
00:09:32,880 --> 00:09:39,880
It's not too extrusion.

86
00:09:39,880 --> 00:09:41,880
It's quite simple.

87
00:09:41,880 --> 00:09:42,880
It's just not easy.

88
00:09:42,880 --> 00:09:43,880
It's something you have to really be patient with.

89
00:09:43,880 --> 00:09:44,880
You don't have to push yourself.

90
00:09:44,880 --> 00:09:45,880
You don't have to force things.

91
00:09:45,880 --> 00:09:49,880
You don't have to stress about it.

92
00:09:49,880 --> 00:09:52,880
But you have to be patient.

93
00:09:52,880 --> 00:09:59,880
It's the kind of person who can sit through anything.

94
00:09:59,880 --> 00:10:03,880
You don't have to do anything.

95
00:10:03,880 --> 00:10:06,880
You just have to bear with everything.

96
00:10:06,880 --> 00:10:09,880
Once you can bear, then that means being objective.

97
00:10:09,880 --> 00:10:17,880
You've been seeing things as they are.

98
00:10:17,880 --> 00:10:19,880
It's all it takes.

99
00:10:19,880 --> 00:10:21,880
You just have to see things as they are.

100
00:10:21,880 --> 00:10:23,880
You don't have to find anything new,

101
00:10:23,880 --> 00:10:30,880
but it's just strange or esoteric.

102
00:10:30,880 --> 00:10:33,880
Just as they are, quite simple.

103
00:10:33,880 --> 00:10:36,880
And that's what's so rewarding about it.

104
00:10:36,880 --> 00:10:38,880
When you see things as they are,

105
00:10:38,880 --> 00:10:40,880
there's no one can take that away from you.

106
00:10:40,880 --> 00:10:43,880
No one can make you doubt that.

107
00:10:43,880 --> 00:10:50,880
Once you see your mind, once you see the nature of your experiences,

108
00:10:50,880 --> 00:10:53,880
into things that you run away from,

109
00:10:53,880 --> 00:10:55,880
once you see that they're not so scary,

110
00:10:55,880 --> 00:10:58,880
that they're not so attractive,

111
00:10:58,880 --> 00:11:01,880
they just are.

112
00:11:01,880 --> 00:11:05,880
But no one can make you cling to anything.

113
00:11:05,880 --> 00:11:12,880
You can't be forced into fleeing or forced into fear,

114
00:11:12,880 --> 00:11:17,880
anxiety or stress.

115
00:11:17,880 --> 00:11:22,880
Because you have understanding.

116
00:11:22,880 --> 00:11:25,880
That's all you have.

117
00:11:25,880 --> 00:11:26,880
You don't have fear.

118
00:11:26,880 --> 00:11:27,880
You don't have anger.

119
00:11:27,880 --> 00:11:30,880
You don't have addiction and attachment.

120
00:11:30,880 --> 00:11:33,880
Frustration, boredom, sadness.

121
00:11:33,880 --> 00:11:36,880
You just have understanding.

122
00:11:36,880 --> 00:11:40,880
Just see things clearly.

123
00:11:40,880 --> 00:11:42,880
What does that do for you?

124
00:11:42,880 --> 00:11:44,880
What are all the good things that come?

125
00:11:44,880 --> 00:11:45,880
What good is it?

126
00:11:45,880 --> 00:11:47,880
You don't have to work so hard.

127
00:11:47,880 --> 00:11:49,880
Why can't we just take it easy?

128
00:11:57,880 --> 00:11:58,880
When your mind is pure,

129
00:11:58,880 --> 00:12:01,880
when your mind is clear, what is the benefit?

130
00:12:01,880 --> 00:12:04,880
The true benefit is, is purity.

131
00:12:04,880 --> 00:12:07,880
The real benefit of meditation is purity.

132
00:12:07,880 --> 00:12:11,880
Because you can avoid the problem,

133
00:12:11,880 --> 00:12:16,880
you can avoid your anger and aversion,

134
00:12:16,880 --> 00:12:22,880
your greed and addiction.

135
00:12:22,880 --> 00:12:24,880
But only when you understand,

136
00:12:24,880 --> 00:12:28,880
when you understand things,

137
00:12:28,880 --> 00:12:31,880
can you become pure?

138
00:12:31,880 --> 00:12:34,880
The mind that is objective is the mind that is pure.

139
00:12:34,880 --> 00:12:36,880
The mind that just sees things as they are.

140
00:12:36,880 --> 00:12:37,880
It's quite simple.

141
00:12:37,880 --> 00:12:39,880
Here, now.

142
00:12:39,880 --> 00:12:41,880
It's not theoretical.

143
00:12:41,880 --> 00:12:43,880
It's not philosophical.

144
00:12:43,880 --> 00:12:44,880
It just is.

145
00:12:44,880 --> 00:12:46,880
Just sitting, sitting,

146
00:12:46,880 --> 00:12:48,880
walking, you're walking.

147
00:12:48,880 --> 00:12:50,880
Seeing, you're seeing.

148
00:12:50,880 --> 00:12:52,880
Your mind is very pure.

149
00:12:52,880 --> 00:12:53,880
With a pure mind,

150
00:12:53,880 --> 00:12:55,880
you're able to overcome sadness

151
00:12:55,880 --> 00:12:58,880
or a lamentation despair.

152
00:12:58,880 --> 00:13:02,880
You're able to come all sorts of mental sickness,

153
00:13:02,880 --> 00:13:05,880
like depression, anxiety, insomnia,

154
00:13:05,880 --> 00:13:10,880
phobia, anger, issues, addiction issues,

155
00:13:10,880 --> 00:13:12,880
all these things.

156
00:13:12,880 --> 00:13:15,880
You're able to overcome through the purity.

157
00:13:15,880 --> 00:13:20,880
It's like a pure water washing away all of these things.

158
00:13:20,880 --> 00:13:25,880
These things that seem so difficult to overcome.

159
00:13:25,880 --> 00:13:32,880
So complicated and so unsolvable, impossible.

160
00:13:32,880 --> 00:13:41,880
In fact, it simply washed away with the purity of the mind.

161
00:13:41,880 --> 00:13:44,880
When you do that, you don't suffer.

162
00:13:44,880 --> 00:13:46,880
You free yourself from all the suffering

163
00:13:46,880 --> 00:13:50,880
that comes from these mental and mental upsets.

164
00:13:50,880 --> 00:13:52,880
The problems in their mind,

165
00:13:52,880 --> 00:13:54,880
when they're gone, there's no suffering.

166
00:13:54,880 --> 00:13:56,880
Even if you're in great physical pain,

167
00:13:56,880 --> 00:13:58,880
you don't react to it.

168
00:13:58,880 --> 00:14:00,880
You're at peace, even with physical pain.

169
00:14:00,880 --> 00:14:02,880
Even that.

170
00:14:02,880 --> 00:14:05,880
Because you can't actually get what you want all the time.

171
00:14:05,880 --> 00:14:11,880
You can't actually ensure that you won't experience unpleasant things.

172
00:14:11,880 --> 00:14:14,880
So the only way to be free, to be truly at peace,

173
00:14:14,880 --> 00:14:16,880
is to learn to let go,

174
00:14:16,880 --> 00:14:19,880
to learn to stop reacting,

175
00:14:19,880 --> 00:14:23,880
to become pure in mind.

176
00:14:23,880 --> 00:14:26,880
This is called the right path.

177
00:14:26,880 --> 00:14:28,880
Does it puts you on the right path

178
00:14:28,880 --> 00:14:30,880
for whatever you do in your life?

179
00:14:30,880 --> 00:14:33,880
Or if you live your life, you do it right.

180
00:14:33,880 --> 00:14:36,880
You don't do it with angry mind,

181
00:14:36,880 --> 00:14:39,880
with a greedy mind, with a deluded mind.

182
00:14:39,880 --> 00:14:41,880
You do it with a clear and with a pure mind.

183
00:14:41,880 --> 00:14:43,880
Whatever you do.

184
00:14:43,880 --> 00:14:46,880
If you're a monk, or if you're a lay person,

185
00:14:46,880 --> 00:14:48,880
a millionaire, a millionaire,

186
00:14:48,880 --> 00:14:53,880
you do it right, with a pure mind.

187
00:14:53,880 --> 00:14:55,880
And so you're free.

188
00:14:55,880 --> 00:14:59,880
If I'm free, then you find freedom in this life.

189
00:14:59,880 --> 00:15:01,880
No suffering in this life.

190
00:15:01,880 --> 00:15:05,880
If you're born again, you're born in a place that is free.

191
00:15:05,880 --> 00:15:07,880
Pure.

192
00:15:09,880 --> 00:15:12,880
It's the freedom of mind.

193
00:15:12,880 --> 00:15:13,880
Whether you're in prison,

194
00:15:13,880 --> 00:15:15,880
or whether you're on top of a mountain,

195
00:15:15,880 --> 00:15:17,880
or wherever you are,

196
00:15:17,880 --> 00:15:19,880
you have the freedom of mind.

197
00:15:19,880 --> 00:15:25,880
No one can take that away from you.

198
00:15:25,880 --> 00:15:27,880
No one can enslave you.

199
00:15:27,880 --> 00:15:31,880
You can see you're at peace with yourself.

200
00:15:31,880 --> 00:15:33,880
You're free.

201
00:15:35,880 --> 00:15:39,880
So, we didn't talk too much about Mahasana.

202
00:15:39,880 --> 00:15:42,880
So, we need to sit there.

203
00:15:42,880 --> 00:15:45,880
It's a bit complicated, I think.

204
00:15:45,880 --> 00:15:47,880
Not really my forte to go through all that,

205
00:15:47,880 --> 00:15:51,880
but it goes through samata and we pass in.

206
00:15:51,880 --> 00:15:53,880
It's an interesting certificate to be sure.

207
00:15:53,880 --> 00:15:55,880
Definitely worth reading.

208
00:15:55,880 --> 00:15:58,880
But this quote sort of stands apart,

209
00:15:58,880 --> 00:16:03,880
and it's the point of the quote is to remind us

210
00:16:03,880 --> 00:16:05,880
that this is difficult.

211
00:16:05,880 --> 00:16:07,880
And that we have to push ourselves,

212
00:16:07,880 --> 00:16:09,880
and we have to be pushed.

213
00:16:09,880 --> 00:16:11,880
We have to accept these teachings.

214
00:16:11,880 --> 00:16:13,880
The Buddha said, if you love me,

215
00:16:13,880 --> 00:16:15,880
if you care for me,

216
00:16:15,880 --> 00:16:18,880
it's not about clinging to me.

217
00:16:18,880 --> 00:16:21,880
It's about following my teachings.

218
00:16:21,880 --> 00:16:24,880
If you don't listen to my teachings,

219
00:16:24,880 --> 00:16:30,880
that means that that's what it means to not really care.

220
00:16:30,880 --> 00:16:33,880
That's all we're concerned about is the point.

221
00:16:33,880 --> 00:16:35,880
If you follow the teachings,

222
00:16:35,880 --> 00:16:38,880
if you do the teaching, if you practice them.

223
00:16:38,880 --> 00:16:40,880
Because they're tough.

224
00:16:40,880 --> 00:16:44,880
That's what separates.

225
00:16:44,880 --> 00:16:47,880
Someone who cares from someone who doesn't care.

226
00:16:47,880 --> 00:16:49,880
It's not worthy of you,

227
00:16:49,880 --> 00:16:53,880
or Buddhist or something.

228
00:16:53,880 --> 00:16:56,880
It's about whether you care enough to put the teachings

229
00:16:56,880 --> 00:16:58,880
into practice.

230
00:16:58,880 --> 00:17:00,880
Difficult teaching.

231
00:17:02,880 --> 00:17:04,880
Anyway,

232
00:17:04,880 --> 00:17:08,880
so that's our bit of download for tonight.

233
00:17:08,880 --> 00:17:14,880
Let's see if we have a couple of questions,

234
00:17:14,880 --> 00:17:16,880
so I can answer them.

235
00:17:16,880 --> 00:17:18,880
You can go.

236
00:17:18,880 --> 00:17:20,880
That's all.

237
00:17:20,880 --> 00:17:24,880
I'm just going to answer people's questions.

238
00:17:30,880 --> 00:17:32,880
It's time,

239
00:17:32,880 --> 00:17:34,880
or time doesn't exist.

240
00:17:34,880 --> 00:17:36,880
Time is not a thing.

241
00:17:36,880 --> 00:17:39,880
Time,

242
00:17:39,880 --> 00:17:44,880
time is a quality of things.

243
00:17:54,880 --> 00:18:00,880
Did the Buddha and do our hands have no need for constant noting?

244
00:18:00,880 --> 00:18:05,880
They're being so awake and aware of your flight.

245
00:18:05,880 --> 00:18:08,880
Well, our hands see things objectively,

246
00:18:08,880 --> 00:18:11,880
so they don't have to practice seeing things objectively.

247
00:18:15,880 --> 00:18:17,880
So it just comes natural.

248
00:18:17,880 --> 00:18:20,880
When they see, they know it as seeing.

249
00:18:24,880 --> 00:18:27,880
I think that I occasionally recognize in permanence or no self,

250
00:18:27,880 --> 00:18:31,880
but I assume that the recognition is only intellectual.

251
00:18:31,880 --> 00:18:34,880
So how does one know when the realization of the permanence

252
00:18:34,880 --> 00:18:39,880
or no self is of a super mundane quality?

253
00:18:39,880 --> 00:18:44,880
Super mundane realization means you're entering to nibbana.

254
00:18:44,880 --> 00:18:49,880
Seeing and permanent suffering in no self means to see that the things,

255
00:18:49,880 --> 00:18:52,880
it's what you're seeing as you practice.

256
00:18:52,880 --> 00:18:54,880
You're seeing that you can't control,

257
00:18:54,880 --> 00:18:56,880
you can't force.

258
00:18:56,880 --> 00:19:00,880
You're seeing that trying to force things leads to suffering.

259
00:19:00,880 --> 00:19:04,880
The things are unstable that you're mind is chaotic.

260
00:19:04,880 --> 00:19:07,880
That the experiences you have are not certain.

261
00:19:07,880 --> 00:19:12,880
There's no warning for the way things change and come and go.

262
00:19:12,880 --> 00:19:15,880
Once you start to see that, you start to let go.

263
00:19:15,880 --> 00:19:18,880
You start trying to control,

264
00:19:18,880 --> 00:19:23,880
trying to force things because you see it's not feasible.

265
00:19:26,880 --> 00:19:29,880
It's not reasonable to think that you can control

266
00:19:29,880 --> 00:19:32,880
and that you can be in charge.

267
00:19:32,880 --> 00:19:35,880
You can keep things the way they are and make them satisfy.

268
00:19:42,880 --> 00:19:46,880
So once you see that enough, it becomes a truth.

269
00:19:46,880 --> 00:19:49,880
You see that you realize that this is the truth.

270
00:19:49,880 --> 00:19:52,880
In the beginning, it's just hinting at it.

271
00:19:52,880 --> 00:19:55,880
And you see it clear and clear and definely it hits you.

272
00:19:55,880 --> 00:19:58,880
That this is the reality that nothing's worth clinging to.

273
00:19:58,880 --> 00:20:00,880
And the mind stops clinging.

274
00:20:00,880 --> 00:20:06,880
And that's the super mundane realization because then there's freedom.

275
00:20:09,880 --> 00:20:12,880
What do you mean about not reacting to physical pain?

276
00:20:12,880 --> 00:20:14,880
Pain can be a hindrance of the mind.

277
00:20:14,880 --> 00:20:16,880
It overwhelms with intensity.

278
00:20:16,880 --> 00:20:17,880
Not so much emotional.

279
00:20:17,880 --> 00:20:19,880
It's very different.

280
00:20:19,880 --> 00:20:24,880
Well, no, actually pain is not a hindrance.

281
00:20:24,880 --> 00:20:26,880
Pain is just physical.

282
00:20:26,880 --> 00:20:29,880
There's a difference between pain and disliking pain.

283
00:20:29,880 --> 00:20:31,880
And that's what you have to find.

284
00:20:31,880 --> 00:20:34,880
Once you start disliking the pain, it's actually not a hindrance.

285
00:20:39,880 --> 00:20:42,880
It overwhelms with intensity.

286
00:20:42,880 --> 00:20:44,880
Not exactly.

287
00:20:44,880 --> 00:20:47,880
I mean, in the sense that it takes your attention, yes.

288
00:20:47,880 --> 00:20:50,880
So if it's very intense, then it'll be the only thing you experience.

289
00:20:50,880 --> 00:20:53,880
I mean, I suppose you could pass out from the intensity of it,

290
00:20:53,880 --> 00:20:55,880
but that's just a physical thing.

291
00:20:55,880 --> 00:20:59,880
If you're not passed out, then just be mindful of it.

292
00:20:59,880 --> 00:21:02,880
If you can do that, you can actually be at peace with it.

293
00:21:08,880 --> 00:21:10,880
Once the mind realizes something,

294
00:21:10,880 --> 00:21:13,880
how come one finds one's out-back thing with ignorance once again?

295
00:21:13,880 --> 00:21:16,880
Are the realizations also just as important?

296
00:21:16,880 --> 00:21:19,880
Yes, realizations are important.

297
00:21:19,880 --> 00:21:24,880
You need to gather enough of them so that it becomes a truth

298
00:21:24,880 --> 00:21:28,880
so that if you realize it as a truth, then your mind lets go because of it.

299
00:21:28,880 --> 00:21:32,880
The only thing that lasts is a realization of the environment.

300
00:21:32,880 --> 00:21:36,880
When you realize the environment, that changes you.

301
00:21:44,880 --> 00:21:48,880
Would you recommend drugs for intense persistent pain?

302
00:21:48,880 --> 00:21:50,880
I wouldn't recommend them.

303
00:21:50,880 --> 00:21:54,880
I mean, I'd understand if people take painkillers,

304
00:21:54,880 --> 00:22:01,880
sparring sparingly, but ideally you deal with the pain.

305
00:22:01,880 --> 00:22:05,880
If it's intense, then yes, you use them sparingly,

306
00:22:05,880 --> 00:22:09,880
but also try to meditate as best you can.

307
00:22:09,880 --> 00:22:28,880
If you're really into meditation, you don't need the painkillers.

308
00:22:39,880 --> 00:22:46,880
Okay, let's call it a night then.

309
00:22:46,880 --> 00:22:49,880
I wish you a good practice.

310
00:22:49,880 --> 00:23:13,880
I'll see you all soon.

