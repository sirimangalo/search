1
00:00:00,000 --> 00:00:22,360
Good evening everyone, welcome to our live broadcast evening dumb session.

2
00:00:22,360 --> 00:00:26,200
So last night, we started talking about mindfulness.

3
00:00:26,200 --> 00:00:32,280
And it was to answer the question of what is wrong mindfulness, but I ended up spending

4
00:00:32,280 --> 00:00:35,200
all the time talking about what is mindfulness.

5
00:00:35,200 --> 00:00:42,760
So tonight, briefly talking about how mindfulness goes wrong, should be quite interesting

6
00:00:42,760 --> 00:00:55,040
as meditators to know how the practice can go wrong.

7
00:00:55,040 --> 00:01:02,000
And the first thing I think to note is that mindfulness is never wrong.

8
00:01:02,000 --> 00:01:12,720
Mindfulness is so benign to say, sadhara, sadhara, so bhana, I can't remember the word,

9
00:01:12,720 --> 00:01:18,800
but it's a beautiful quality of mind.

10
00:01:18,800 --> 00:01:29,800
Mindfulness is common to all wholesome states, I think.

11
00:01:29,800 --> 00:01:37,640
So mindfulness itself can never be wrong and it's always useful.

12
00:01:37,640 --> 00:01:45,320
So when we talk about wrong mindfulness, we're not actually talking about mindfulness being

13
00:01:45,320 --> 00:01:46,320
wrong.

14
00:01:46,320 --> 00:01:52,440
We're talking more about the practice of mindfulness going wrong, but furthermore, the

15
00:01:52,440 --> 00:01:59,040
practice of mindfulness, it is quite difficult for it to go wrong, and I want to stress

16
00:01:59,040 --> 00:02:05,040
that because with other types of meditation, it actually is quite easy for it to go wrong.

17
00:02:05,040 --> 00:02:12,280
I'll talk a little bit about that maybe I'll wait and talk during the talk about it.

18
00:02:12,280 --> 00:02:16,320
But mindfulness is so simple, right?

19
00:02:16,320 --> 00:02:23,880
The first thing it's difficult to misunderstand the instruction, that it's difficult to

20
00:02:23,880 --> 00:02:30,320
be in doubt about what is being asked.

21
00:02:30,320 --> 00:02:40,040
And it's difficult to apply mindfulness and, well, it's really impossible to apply mindfulness

22
00:02:40,040 --> 00:02:46,240
and go too far, right?

23
00:02:46,240 --> 00:02:50,920
With other types of meditation, you have to be careful if you go too far, you can get

24
00:02:50,920 --> 00:02:56,440
out of your depths, but with mindfulness, you don't really ever get out of your depths.

25
00:02:56,440 --> 00:03:01,840
It can seem overwhelming, but it's in a different way, the overwhelming nature of it

26
00:03:01,840 --> 00:03:02,840
is real, right?

27
00:03:02,840 --> 00:03:10,440
It's just your own mind, whereas other types of meditation you can get lost, and because

28
00:03:10,440 --> 00:03:13,320
it's conceptual, we'll talk about that.

29
00:03:13,320 --> 00:03:22,480
The first way mindfulness can go wrong, let's bring this up, is, well, I'll spell the

30
00:03:22,480 --> 00:03:34,960
other, the four ways mindfulness can go wrong is one, unmindfulness, two misdirected mindfulness,

31
00:03:34,960 --> 00:03:50,520
three lapsed mindfulness, and four impotent or ineffective, ineffectual mindfulness, impotent

32
00:03:50,520 --> 00:03:55,840
might be the best word, see if I can get to this again.

33
00:03:55,840 --> 00:04:21,920
So, unmindfulness is, of course, the opposite.

