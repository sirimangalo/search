You
You
You
You
You
You
Right we're switched
That we were I thought I could I
Was I thought we were still at the
Introduction screen I switched over to the other to the rain screen and here I am fiddling around with your
Your video window
You
Good evening, oh wait, let's go
Mess up everything good evening everyone broadcasting live. Welcome to our live broadcast
There I managed to mess up
The whole introduction
You
So this evening we are looking at
I'm good learning guy book of threes suit a 150
150
156 in Bicobodys book 157 in the party
You're a jailer
Jailer go back
I don't know
Yeah, I did a good one
The three types of practice
This is a simple teaching. I don't know that there's that much to say about it, but it's worth noting
The Buddha mentions three types of practice
Ah gala, but deepada
The practice that is course or rough
Course may be the best
Nijama bhatipadha number two. Ijama means in jama means burning
The inflamed
Picobodys has blistering practice
And number three majima bhatipadha
The middle practice
These three types of practices is the course practice
The burning practice and the middle practice
Or moderate we don't really have a good translation of majima
Majima means middling or in the middle
but it can also mean moderate or
Just right
Center practice centered practice. Maybe that's a good one centered
But there's different connotations in English that probably don't
And don't hold up in the poly
So what are these three practices? The course practice is the practice of a person the Buddha says a
Person who has the view thus nutty kamis would do so
There is no fault in sensuality
There's nothing wrong with chasing after
Central pleasures hidden is them. It's all good. You only live once eat drink and be merry
That's the best way to live best way to live try to always get what you want
So kamis who bought
Bhatan apagenti
Such a person
In delches and sensual pleasures. This is called the course way of practice
So this type of practice is
probably
Probably the most common
Well, it's probably the most common of all
Types of practice the practice of indulging in sensuality
So what we do when we're children
and then we
Try to do it as much as possible
Generally as adults though. It's tempered by the knowledge that we can't always get what we want
And then we have to work for it
Realization that there's a price that comes to the things that we want, but we still try and pay the price and
Get what we want
I think there's nothing wrong with it
This is why it's very difficult to come and practice this type of meditation because
It's hard to see that there's anything wrong with
With just doing what you want, you know, why would you want to
Avoid or prevent the mind from giving rise to the desires when the desires are what lead into that pleasure
The pleasures are what make us happy
That's the first type of practice
second type of practice is
the burning practice and
Burning there's a there's a double meaning here because in India even before the Buddha. There was this practice called tapas
tapas means to burn
Talked about this before if you know anything about Indian
Yogis or
Sadus or Siddhis or Ishis this kind of thing
They practice tapas tapas means to burn up the defilements
The idea was you could torture yourself. You do something that's extreme and it would burn up all the defilements with the power of it
So
competition sports in modern times are just talking this morning about people who do gymnastics all the these Olympic
I don't watch the Olympics of course, but
We're talking with a woman who was watching and I was just saying, you know
Really these young children who are pushed into it and then it
They really push themselves quite hard and can can really hurt themselves
But there's a you know, but they push themselves very hard
People who work very hard. This is the kind of thing where people have the idea that the best way to live is to work really hard
Work yourself to the bone work your fingers to the bone
Work very very hard and that's the good life. You've done something worthwhile. You've worked really hard
I
Think that's along the same lines of burning you feel like you've you've burned the midnight oil
You've created energy you've focused energy on something and you've created something through the power of the burning of your energy
But of course the most extreme example is those people who actually do undertake to torture themselves
So I'm going to list a whole bunch here. He says someone goes naked
Someone licks their hands. I don't know what that's all about
apparently a practice not coming when asked so they wouldn't
Something to do with coming
Coming for food someone says, oh, please come and receive arms. They wouldn't if you didn't bring the food to them
They wouldn't eat it
They wouldn't stop some people not stopping when asked so if they say please stop and I give you some food
No, keep walking and if you don't just put it in their hands as they're walking
And the idea is to get very little to make it very hard on yourself because who's going to want to give to some guy who doesn't stop, right?
Let alone doesn't it doesn't come, you know, he doesn't even stop
He doesn't accept food brought or food specially made he received nothing from a pot from a bowl across a threshold across a stick across a pestle
From tweeting together, he wouldn't accept from two people all these crazy rules from a pregnant woman from a woman nursing a child
Because you know the idea would be you would be taking from the child I guess from a woman being kept by a man
So no, no one who's engaging in sexual activity
For more food is advertised so not if someone says all the food advert food here
Can't go
Where a dog is waiting if a dog is waiting. You don't want to take the food from the dog where flies are buzzing
So even if there are flies who are waiting to get the food you won't accept it
Accepting no fish or meat. That's an interesting one
It's one of the few examples of the Buddha actually sort of discusses this because it seems to be not a proper practice
But here's an interesting one the last one is he drinks no liquor wine or fermented brew
Just kind of baffling because that's actually proper practice. I'm not sure why that one's in there
But it may be that they think you know somehow by not drinking alcohol something special comes from
Comes from that because nothing special comes. It's just just a stupid thing to do
But it doesn't mean you're gonna become enlightened just because you're keeping that rule
Of course, you know, you don't keep the rule chances of becoming enlightened pretty stem
And it goes on and on
Seven houses seven more so lips on one saucer a day two saucer's a day
Takes food once a day once every seven days
He takes food and stated intervals he eats only type this certain type of food he wears only certain types of robes
Bark fabric
That's how interesting this is he even eat even might wear a covering made of owls wings
He pulls out his hair
There were actually people in the Buddha's time. I guess that were
The way they would go forth is they would get a special comb and it would
They would twist up your hair and get up
Apparently they would put two boards on your shoulders
Take a big pit and put you in the pit and then put two boards on your shoulders and stand on the boards
And you ain't got all your hair
Stands continuously rejecting seats anyway you get the idea
tormenting and mortifying the body. This is called the
Listering way I like the burning way of practice
What is the middle way of practice?
Well, we all know what that is, but that's spelled it out
It's actually actually doesn't mention the eight full noble path and here's an a good example of how
The eight full noble path. There's something that can be used synonymously for it and that's the four sati patana
Four sati patana are really the key to the eight full noble path and it's a way of it can be used to replace
Actually mentioning the whole path just talking about mindfulness
Guy a guy a no bus see we had at the he dwells being mindful of the body in
the body our top be effortful
energetic
Our top instantly comes from again over the root top which is tapas which is to do it burning
some pajano
Seeing clearly satima remembering
Recognizing you know remembering maybe
We may a low-key a big dad on the nest. I'm having given up or giving up
Greed and anger in the world
We didn't ask away the nano-passi we had a tea and we didn't as well
So the body is the first one
We didn't know any way didn't I pleasant we're gonna pleasant feeling an unpleasant feeling a neutral feeling?
Jit de jit ano passi we had a tea and dwells
with the mind
Seeing the mind as mind
That would be some pajano satima dame so dame man up a sea and the dame is all the many types of dame is the new
one are the dumbas the indria the kanda
Call the dumbas
dumbas well basically start with the
Starting with the hindrance is like in disliking drowsiness distraction down the faculties of seeing hearing smelling things
and feeling thinking breaking reality up in the dumbas
I
Interestingly he actually goes on
I didn't read on but the the next suit is this is one fifty seven
one fifty eight and replaces the four sati pajano
With another way of looking at the middle way and it's the four right efforts indie Bada
No before
Why doesn't it say indie Bada
It's an indie Bada, wait, but they're not the four right efforts
The turned down we
No
Turn down any
No, it's not
Why does he translate or am I looking at the wrong one?
Oh
Wait that was right that was 56 156 right I get it so 157 is
The four for right efforts so instead of the four sati pajano it could be framed in terms of the four right effort
Doesn't have to be the four sati pajano the four right efforts are
non arisen
Generates in interest in the non-arising among risen by a done wholesome states
The abandoning of a risen unwholesome states
The arising of under risen wholesome states and the maintenance of a risen wholesome state
Where they're non-decline increase expansion fulfillment by development
The right efforts now the four edi pada is another way so there's chanda
Which is you were to do succeed you need effort. Oh, no, you need interest. Sorry. Really. I need effort
Did that you need to keep your mind on the object on
The practice and we monks I you have to be able to investigate and discriminate
Or it could be understood as the five faculties
Sadha you need confidence really you need effort mindfulness concentration wisdom
Or it could be the seven-board jangas he actually gives a whole list and this is the this is actually
the
37-boardy pakya damas
So what we're seeing here is actually
the 37 there are these 37
Quality is that are you know all these lists when you add them up. It adds up to 37. You've got the force that you put on
You got the far right effort. So you got the four
basis for success
That's 12 then you got the five faculties. You got the five powers which are basically the five faculties
So it's the same list. So that's 22 and then you've got the seven factors of enlightenment mindfulness
discrimination energy
Rapture
tranquility concentration and equanimity
That's 29 and then you've got the eightfold noble paths right view right intention right speech right action right livelihood right effort
Right mindfulness right concentration
And what it means is each one of these lists
Can is what is a way of looking at the?
Prat path of practice. I was on reddit recently the past few days of
Just saw a saw something there's a Buddhism reddit and I was just looking and I saw that a
Through a thread about meditation and someone asked a question. So I answered it and I answered another one and
Someone was saying how
Donna is necessary for the path of path
And this is a good example of that Donna is not in here
We won't find Donna in any of these as far as I can see
I mean Donna is a practice that's useful, but it's not necessary. It's not essential not like these are it's in a different category
Mitta is another one people say mitta bawana is necessary. It's not really
You really only need all of these
And it's based on the force at the baton the only one that really gives actual
Like an actual object of meditation or an actual thing to do
Is the force at the baton and the rest of these are
The qualities really
So this is a good example of what the 37 Bodhi Bhakya Dhamma is what's necessary
That's based all on the satipatana really
Which is in turn basically the five aggregates
That's where our meditation should be based
Anyway, so I thought this was interesting because it's a not one of the few
Examples where the Buddha actually does talk about the middle way in this way as the between these two extremes of sensual indulgence and self torture
It's actually not something the Buddha talks about often, but it's it's probably
Got the highest ratio or the highest
relative you know
Comparatively speaking for the little that it's actually mentioned in the suitness. It's probably the most well-known
And I like related to how many compared to how often it's actually mentioned. It's it's really quite popular
Probably the most popular in that regard
For how little it's mentioned
And I think this is one of the few examples of where that's let's deserve
I mean mostly you would want to say
If the Buddha didn't talk about something very often, it's not very important. So
Like Donna is something that the Buddha talked about but not really
In fact, he did talk about it quite a bit or it's in the typical quite a bit, but not in the not in the way of
This is you know the essence of the path. It's just it's a good thing to do and talk quite often about
but
You know there there are practices that
Didn't really get all that much attention and so sometimes people pick up one suit and say oh this is what the Buddha taught when in fact
didn't teach it that often
Um
But this one I think is deserved. It was the first thing he taught
Even though the people he was teaching to were quite a special audience
but it also you know really captures
What captures the Buddha's practice himself? The Bodhisattva's practice how he became like
And it well sums up sort of the
The things we can do we can either ignore spiritual practice or we can practice all wrong
And there's only one
Path that cuts through both of these because usually they were either one or the other were either not practicing at all
Over practicing. We're only end up wasting our time
So the Buddha's teaching is the middle way
and
Really it's important to keep this in mind because otherwise you start to think that things like mate are
Are the path or Donna's the path and they're not really?
The path is
Forcettipatana
It's really quite clear and then all these other ones that you build off of that
Of course there are many other practices that can support that but they can never replace the forcettipatana because the forcettipatana are
Are the five pandas the five aggregates in their reality? I mean if you don't know what I'm talking about for those of you who aren't
familiar with all this terminology just means
Reality experience
Another way of look at it is seeing hearing smelling tasting feeling thinking because
Those are the six doors where the five aggregates arise
Learn about the five aggregates each one of them
Arises at the eye door the ear door the nose the tongue the body or the mind
So it's basically just talking about experience the point is the only way you can become enlightened is to
Metitate on experience and cultivate mindfulness based on reality
So that's our demo for tonight
That was
So
Got the technical side not very
Not off to a good start, but I think the demo went over okay
That's unmute Robin. I Robin. Good evening
Hello, birthday
We have some questions
We do
Is it also Tibetan monks that are from Nepal? What are your thoughts on Tibetan monks?
I don't think about the much at all, sorry
Sir in your booklet you mentioned noting gives rise to clear up and therefore clear awareness
What is the difference between a self-talk?
Be clear up
See clear awareness and the normal pop
I don't know what self talk is clear thought clear awareness
Well the clear thought would be a reference to septi the actual act of reminding yourself of the mantra
You know any type of mantra meditation is creating a clear thought and that leads to the clear awareness
When you use the clear thought
Creates clear awareness. I mean they can also be used synonymously
I'm just create I'm just using these words trying to explain
What we're trying to do because a normal thought and the last one you ask is a pretty good question
You can explain to me what the first one is
But a normal thought is a reaction thought
You know you see something and say oh that's nice or that's unpleasant or that's me that's mine
So instead we replace that with a clear thought. This is this
This is a nice. This isn't me. This is mine. This is C
And that's the difference it's clear because it's objective and it's clear because it's real
There's nothing nice about when you see something that's all
Or especially when you say this is me. This is a person
This is something that I this reminds me of something in the past or that could
Actually it will secure us some examples
For example, we are noting the site of a person at one level at one at what level do we stop
Sequence color and intensity of the light coming from the person. Therefore identifying just the solva of the person
Or be difference in frequency intensity from various parts of this body therefore identifying the parts of the body
See how the parts of the body
You stop it seeing when you see is stop it seeing
When you know that you're seeing that's enough. Don't worry about the particulars
You're overthinking it way overthinking it. That's actually quite impressive
Seeing is just seeing if you can go start to go deeper. You have to say thinking thinking
Hello, Pompey. I wonder if you could help me with my meditation practice
I'm trying to be mindful through the whole day, but it's a student. I have a hard time listening carefully to my teacher
All being mindful at the same time
Yeah, I am not able to manage to say hearing hearing hearing and listen carefully
Do you have any advice for that? Thanks in advance and please from Germany
And you're not gonna I mean it's not meditation if you're trying to learn something trying to study
You have to study
So you won't always be meditating try and find times when you can meditate
I mean there'll be times during the talk where you can remember that you're stressed or that you're
You have a headache or that you're tired or that you have pain in the in the body or that kind of thing or it feels hot or it feels cold
Or even just that you're sitting just remember that you're sitting
Or wanting wanting to be mindful. I'm worried about being mindful. I'm stressed about it
Question I feel like shouting this hard work
Bump it or lots of experiences of mildest mildish pain in my forehead
I have trouble discerning whether or not there is liking or disliking there as they are firmly mild
I'm actually quite unclear about the experience of disliking and liking
I get to the noting of say pain or anger or fear and I can't distinguish if there's liking or disliking
Is this like a clarity normal when beginning meditation?
Yeah, I mean you're probably again overthinking it and and there's worry
The mind is very tricky, you know by the time you're actually wondering about that
Your mind has tricked you in a new way you probably actually got it right
You know you said pain pain and then suddenly your mind starts to worry about did I do it right?
That's another habit
That's a habit that you have to be you have to catch and when you're worried about it or or frustrated about it or so on
You have to note that when you're doubting about it whatever that is
There's nothing you know, it's not magic. It's fine if you say pain that's fine
You don't have to know that you're disliking if you know that you're disliking then you'd say disliking
I'm trying to keep it's when you know that this is what you're experiencing just remind yourself
That's all it is
So that's the idea the idea is to stop your mind from making more of it
There's this thing called a punch at that I don't talk about nearly enough
And it really is the key to the whole problem
But it didn't talk about it much at all, but either but it's really a crucial little thing
But punches this extrapolating of things. I mean, I guess I do talk about it's the reaction
But it really means making more out of something than it actually is
And that's all we're trying to do by reminding ourselves we're
We're feeding the loop back into itself so it doesn't get anything it doesn't go outside of what it really is
But yeah, it's hard work. It's meant to be hard work. And that's a good sign if it got really easy
Then you'd have to wonder whether you were really meditating
I had one one meditator recently and I don't know if he's listening here. So apologies. I'm not trying to embarrass him
But for them, but
They were just doing sitting meditation and they were having a great time with it
And I didn't realize it and I said oh well you have to do walking and they said well
They do a little bit of walking but very little
I forced them to do walking and their whole practice has changed and suddenly it was very difficult and unpleasant
And the frustration is oh good now you get to see what's really going on now you get a real challenge
We want to make it hard. We want to make it challenging
So that we can become stronger. You know that's what you do when you train when you train in sports
I mean, what do they do they push themselves?
Try and see what they can handle but this is mentally
You know we're trying to push our minds to see how much we can handle maybe it's like what soldiers do
Push their mind so that they can be more tolerant
You know we just have a much more streamlined to be
Only very effective if soldiers were to put this into practice. It could probably be just the best soldiers
When I was tree planting
As a meditate, you know, I was doing meditation in the evenings and tree planting during the day and I was just like a machine
Because you're going to be so focused and undistracted by no distractions whatsoever
You can't find a live video on YouTube. What piece do you look for the live link?
Hmm
Can't find a live video on YouTube
I don't know. I don't ever watch it
But I have a link for you
Let me see. Where is it
It's uh, well, you just go to my channel. You should probably see it. No. I mean, it's not right in my channel
Yeah
When I go to YouTube to your page
I mean, it actually has a little red rectangle this is live now
So kind of jumps out as long as you run
Each time will be a good channel
Yeah, there's uh, I mean, it gives me a link on my dashboard to tell me to give that link to people
But I'm assuming if you just go to in fact, you know, it's funny
They've changed it now
But you can still go to the old one YouTube YouTube dot com
Front slash you to dumbo still works
If you go there, I'd just now redirects you to the new
Channel, but I've hopefully permanently got this
Uh, what do they call
Vanity URL or something YouTube dot com front slash you to dumbo. That's me
Facebook dot com front slash you to dumbo
Twitter dot com front slash you to dumbo
You missed one though, did you not get the first question? It's not there on your page
I did this one. I'm sorry, okay
How do we practice cheat the way past now when walking meditation?
It feels like only way can only pass on it as possible during walking meditation
It's the feeling of good movement is too strong to look after the mind
Where she threw a bus and a and if we do so with his mindfulness of walking process
Okay, you're you're missing your you're confusing way to know its group with with Gaia
What you're talking about is actually Gaia no passing. It sounds like you might have you might have a going to background
Because he talks a lot about sensations, and I think
Some of his students get it mixed up because it's not way to deny us is feelings of pleasure
Pain or calm
There's five types of feelings really. I mean
There's way to deny in there, but that's not what you're experiencing. What your experience are there are the dots of
The group but
Gaia no passing the movement of the body when you move your foot
That's Gaia
Anyway, I suppose that's not really your question
But you're asking how you how you practice JITA no bus well while you're doing that
There's I mean first of all you're aware of the mind being aware
You know the mind going out to the body
And you're aware that the mind is impermanent in the knowledge the knowing of the moving arises and seasons
so that the movement arises and ceases and the knowing arises and ceases the knowing doesn't last longer than the moving of course and
the
The knowing doesn't start until the movement starts
So they arise and they cease together
That's the mind
Of course you can also be mindful of the thoughts
So if you start if you're walking and you start thinking then you should stop walking
I mean you don't always have to this
But if you really get distracted then you would stop and stand still
And say thinking thinking that would be a good practice
But for sure as you're doing walking you're going to be aware of all four
Although, you know technically you don't need to be practicing Gaia no bus
So JITA no bus need have to stop
And that man who passed the night you'd stop and if you were angry or frustrated or if you wanted something or if you were tired or distracted or worried or so
Or if you see or hear or smell it there
In reference to self-talk it is the constant voice inside the head
I guess from your answer it is the normal thought
In reference to the noting question when you say seeing what am I seeing
Just the frequency and intensity of light coming out of the option
Is that all my question was that what uses it to just stop and holding the light?
Yeah, it's not about the frequency and intensity of the light. It's just the
So I think it's going a little bit to detail into it and it's seeing
You know, can you not distinguish seeing from hearing and you see something that seeing
But as to why that's useful. Well, that's you know, hopefully I hope that my book actually explained that but
And I explained it, you know, I explained it often
Noting helps you
You know prevents you from extrapolating it prevents you from judging
You only experience things as they are and you know react to them. It's a replacement for the reaction
That's what the use of it is
Noting seems to wake up my daughter when she's sleeping
Is this common and other than moving farther away? Do you have any suggestions?
That's really bizarre. I'm sorry. Don't you ask me this a long time ago. I never answered because
I don't really have an answer
I
Except to say that I can't it's it's difficult to believe
I guess I should just believe it, you know, you say it
but well
Have you thought that it might just be
Coincident? I guess I'm sure you've been through this and it's just too often to be coincidence, but
Guess there could be you know, there can also be some karma, you know involved. It might just be karma
Is there are stories about this where people are blocked from doing good things?
And every time they try to do something
Something gets in their way and it can just be some strange karma that you have
She might not even be feeling vibes
It's just the universe is set up to
You know, you've set up the universe your universe to to get in your way
it's a very interesting
Thing to hear about but you know you
Everyone gets this to some extent try to go to a meditation course and then something comes up
Or you do a meditation course and something terrible happens to you, you know, there's things that get in your way
Stop you from changing your paths
They they box you in we get boxed in by our karma
And it's quite common actually
Hey, you said that as well
Billy bizarre
Well, maybe you should meditate beside your your daughter and if she wakes up just
Meditate by looking after
Sounds like it's a karma that you have to work through. I mean that whether it is her actually
Recognizing in the mind somehow getting on a vibe and knowing that you're meditating
Which would be a bizarre reason to wake up
You know, it's still karma
It's the situation that you're in
So you have to do babysitting meditation
Baby care meditation child care meditation
I
Want it is so
Modification caused by the third-feder
See about the
Good question
Self-mortification
I mean, yes, many of those are
But the question is whether it has to be
I
Mean, I think that might not be the complete proper way of looking at it. It's not caused by that. See the bataparamasa is just one of the things
That
One one gives up as a soda partner, right?
But you could just as well say that it comes from
Wrong view we'll see the batapamas is
Just trying to think of there's a way that you could frame it that it's not
Yeah, I mean, I don't know that it you could say that while just practicing too hard
So if you meditate too much or push yourself too hard in meditation
It's not really see the batapamasa. See the batasila are
are
rules or precepts and but what that is
Like
Practices, so it's the negative in the positive not doing certain things that are nothing to do with the practice and doing things
And I'm nothing to do with the path
So I don't think that torturing yourself necessarily has to fall under one of those, but it usually does right
But simply torturing yourself because you can torture yourself
Torture yourself by pushing yourself too hard
Anyway, I don't I don't think that's not how I would look at that
See the batapamasa is something you give up but what causes these things are mohah
Usually mohah
Aritya
The third fetters caused by that fetters caused by aritya and so
Okay, what is the list of fetters? How many are listed for 10?
So the banding gets rid of
three
That's sakei a dt
wrong view of self or view and view of self basically see the batapamasa is
Wrong practice is attachment to wrong practice is practices that have nothing to do with the path thinking that they have to do with the bat
but both
precepts and practices
And we cheeky chamings doubt doubt about what is the right practice doubt about the Buddha
Anagan sakadagami weakens the next two fetters which are
Gama chanda and patika
Gama chanda is desire for
Sensuality and patika is a version
But they don't they don't cut them off an
Anagani cuts off Gama chanda and patika
And so this is based most greed and anger
Greed and anger relating to
Sensuality but an anagani can still get can still have attachment and I think a version
But it's a version and attachment to becoming
so it's
I think they are the me see
Baburaga we Baburaga
Mixing all this stuff
Ruparaga
Baburaga
Our hand gets rid of the other five
So the two of them are in regards to desire and aversion for becoming
And then the other three are conceit
Distraction and ignorance
So in our hunt only in our hunt get sort of conceit
Distraction and ignorance and desire and aversion towards becoming
It's kind of discouraging that you have your ignorance all the way through our hunt
I don't think I would do one of the first to go but
No, if you're still if you had if you had no ignorance you know, that's the key is
You had no ignorance if you if you had
Perfect knowledge and there was nothing that you were ignorant about
Or if you weren't ignorant about the form of a dress here, you would never give rise to to an awesomeness
Then thank you
As in torturing you're thinking it will lead to liberation on the solo question
And Doug's follow-up question was
With his daughter noting it's so frequent that his wife was one to meditate in the house
Curious how far away
So far away you have to be before this doesn't happen. It's so interesting
All right, I'm gonna end it there
Wishing Doug all the best in his meditation endeavors and his child-grearing endeavors
And his family endeavors
What do you think of the the
Darma wheel you can't see it I guess Robin
I don't see your eyes and Darma wheel right here
I'll have to check out YouTube right after
I
Actually, I just don't understand on YouTube. You can't actually see these live streams right away
Just get away. Yeah
Well, I don't know I see a delay of about five seconds me
No, I mean
When the live stream is over
You can't actually watch it for I think I don't know. I actually went to bed. It's probably an hour when I
They processed it
Okay, they do some
Yeah, I mean that's like it that's like that with all videos really
Any video that I upload all those you these are longer so they tend to take longer
Yeah, they process it they process it in different formats as well
Okay, I guess I just didn't notice it back with the previous version
Okay, so that's all for tonight. Thank you. Thank you Robin. Thanks everyone for
