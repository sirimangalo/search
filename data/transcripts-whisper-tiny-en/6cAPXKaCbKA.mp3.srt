1
00:00:00,000 --> 00:00:10,400
Hello sir, I have just started meditation though I find it very positive and calm

2
00:00:10,400 --> 00:00:18,440
but still I don't manage to maintain that calmness throughout the day sir. In the

3
00:00:18,440 --> 00:00:24,960
sleep I am having dreams related to my past life and when I woke up the next

4
00:00:24,960 --> 00:00:32,440
morning I used to think a lot about my past life maybe because I just had dreams

5
00:00:32,440 --> 00:00:38,120
related to my past experiences what should I do to maintain the calmness

6
00:00:38,120 --> 00:00:51,840
throughout the day. Well as the kids in the hall said the only way to be happy

7
00:00:51,840 --> 00:00:57,160
is to know you can't be happy all the time so I'll say the same for calm the

8
00:00:57,160 --> 00:01:07,160
only way to become is to know that you can't become all the time. It's impermanent

9
00:01:07,160 --> 00:01:15,040
what you're seeing is one of the important realities of life that we as

10
00:01:15,040 --> 00:01:23,000
human beings fail to appreciate, fail to accept. We're unable to accept impermanence on

11
00:01:23,000 --> 00:01:33,400
so many levels. We strive always for stability, for continuity. How much of our

12
00:01:33,400 --> 00:01:48,240
lives is spent trying to ward off change, trying to ward off loss. What you're

13
00:01:48,240 --> 00:02:03,760
talking about is just an extension of this idea of loss when a person

14
00:02:03,760 --> 00:02:10,280
clings to their possessions. You cling to your house or your car and then your

15
00:02:10,280 --> 00:02:19,520
car gets stolen or your house burns down. We guard so much against loss so really

16
00:02:19,520 --> 00:02:23,640
what you're saying is not categorically different and this is something

17
00:02:23,640 --> 00:02:31,000
important to realize this fear or this aversion to losing even a state of

18
00:02:31,000 --> 00:02:38,880
calm. This is an attachment. This is an inability to accept the reality of

19
00:02:38,880 --> 00:02:44,960
the situation and that's partiality and so in that sense that's what's

20
00:02:44,960 --> 00:02:52,080
stopping you from being calm. If you want to be truly at peace let's put

21
00:02:52,080 --> 00:02:56,080
aside the word calm because it brings up connotations of a feeling and as I

22
00:02:56,080 --> 00:03:02,560
said that's only one type of equanimity but if you accept and this is

23
00:03:02,560 --> 00:03:05,400
actually goes to the the charge that people level on us that we're just

24
00:03:05,400 --> 00:03:10,280
going to become my you know equanimity zombies right which is become all the

25
00:03:10,280 --> 00:03:16,880
time and like like stoners or something but that's really not the case that

26
00:03:16,880 --> 00:03:24,040
you actually are able to experience a whole spectrum of mind states and of

27
00:03:24,040 --> 00:03:28,160
experiences. Now you can't experience an enlightened person can't experience

28
00:03:28,160 --> 00:03:35,360
anger or greed so if you really like those ones you're in for trouble

29
00:03:35,360 --> 00:03:40,160
but don't worry as long as you have craving and people are always afraid of

30
00:03:40,160 --> 00:03:44,640
this you know this thing I mentioned and people are afraid of losing their

31
00:03:44,640 --> 00:03:48,880
craving well if you if you if you want it you know if you think it's a good thing

32
00:03:48,880 --> 00:03:52,280
you'll never let you how could you possibly let go of it what is there to be a

33
00:03:52,280 --> 00:03:58,000
afraid of. You can't just oops suddenly I lost my I didn't mean to let that go

34
00:03:58,000 --> 00:04:08,840
you know I still wanted that one it's not like that you know you give up what

35
00:04:08,840 --> 00:04:14,360
you're willing to give up and this is the equanimity that comes it comes to us

36
00:04:14,360 --> 00:04:19,760
we can still experience pain we can still experience even if we become an

37
00:04:19,760 --> 00:04:26,600
our hand we'll still be able to experience the whole range of of experiences so

38
00:04:26,600 --> 00:04:33,760
we won't feel calm all the time even an our hand will not feel a feeling of

39
00:04:33,760 --> 00:04:39,360
calm at all times sometimes they'll be happy but they'll either be happy or

40
00:04:39,360 --> 00:04:46,080
they'll they'll become and even that calm may not feel calm at times because

41
00:04:46,080 --> 00:04:49,360
the mind can still be active and there can still be thought in the mind

42
00:04:49,360 --> 00:04:53,320
no so it may not be perceived as a state of calm but it will be a state of

43
00:04:53,320 --> 00:05:00,520
equanimity the feeling will be one of equanimity but what is important is not

44
00:05:00,520 --> 00:05:07,400
the state of calm that you experience what important is your staying calm even

45
00:05:07,400 --> 00:05:14,560
when the calm disappears can you do that when the calm is gone are you still

46
00:05:14,560 --> 00:05:19,400
calm because it's on a different level you know are you are you judging the

47
00:05:19,400 --> 00:05:25,280
experience that you have now when you're not calm because when you're judging

48
00:05:25,280 --> 00:05:29,480
that that's what's causing you suffering it's craving that causes suffering if

49
00:05:29,480 --> 00:05:33,960
you look at the foreign obitrous the Buddha was quite clear done have

50
00:05:33,960 --> 00:05:43,920
yayang tan hapono bhavika nandiraga sahagata tatra binandini whatever craving that

51
00:05:43,920 --> 00:05:57,560
leads to further becoming and is associated with desire and so on this is the

52
00:05:57,560 --> 00:06:02,600
cause of suffering which finds delight in this or that or anything that is the

53
00:06:02,600 --> 00:06:09,200
cause of suffering even attachment to to to good to wholesome state is a

54
00:06:09,200 --> 00:06:20,880
cause as you can see for a cause of suffering I would like to say some thing

55
00:06:20,880 --> 00:06:32,080
about the the dreams yes good you said you're having dreams related to your

56
00:06:32,080 --> 00:06:41,440
past life and so on don't take that so serious it's nothing important it's

57
00:06:41,440 --> 00:06:50,680
just it may be dream about about a past life it may be a dream about experiences

58
00:06:50,680 --> 00:07:02,960
of the day that doesn't really matter we had that earlier it's past and gone and

59
00:07:02,960 --> 00:07:12,040
of course in in the sleep we we work out things that we dealt with in the past

60
00:07:12,040 --> 00:07:19,240
and we can't control that really so this is just going on and and maybe it's

61
00:07:19,240 --> 00:07:25,760
important for you to to work that out so just let it happen and be mindful

62
00:07:25,760 --> 00:07:32,720
that it happened you don't need to always become about it sometimes these

63
00:07:32,720 --> 00:07:42,000
things are scary then look at it or sometimes they are important then look at

64
00:07:42,000 --> 00:07:53,040
it so just push it push things away by wanting to become that's great danger to

65
00:07:53,040 --> 00:08:01,040
to to mental or to cover everything with with calmness is is not wanting to

66
00:08:01,040 --> 00:08:09,280
see reality as it is cause reality is not calm the only thing that helps is to

67
00:08:09,280 --> 00:08:18,760
develop equanimity as Banta said in the in the ends of earlier then you can

68
00:08:18,760 --> 00:08:24,920
become naturally but if you want to become Banta mentioned it already then you

69
00:08:24,920 --> 00:08:31,800
won't become because there's always that that grief for being wanting the

70
00:08:31,800 --> 00:08:41,440
wanting to be calm so when you when you take this all to serious and then

71
00:08:41,440 --> 00:08:51,320
want to to cover it up and one day might explode and you will not become it

72
00:08:51,320 --> 00:09:04,000
all yeah just to reiterate what I think is the important most important is

73
00:09:04,000 --> 00:09:11,640
that past is past no if you've had past life memories or so on so it's still

74
00:09:11,640 --> 00:09:16,760
past would have said a detail none what gummy I don't go back to the past so

75
00:09:16,760 --> 00:09:21,880
sitting around thinking about the past is it's not going to do much except

76
00:09:21,880 --> 00:09:48,280
cultivate the habit of thinking

