1
00:00:00,000 --> 00:00:04,000
Hey everyone, welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:12,000
Just a reminder that I'm still looking for videos of you practicing meditation,

3
00:00:12,000 --> 00:00:16,000
either doing the mindful frustration walking or sitting.

4
00:00:16,000 --> 00:00:20,000
As I talked about in my 200th video upload,

5
00:00:20,000 --> 00:00:24,000
so please post responses to that video, not this one,

6
00:00:24,000 --> 00:00:26,000
showing yourself meditating.

7
00:00:26,000 --> 00:00:29,000
There should be a link, I'll place it right here.

8
00:00:29,000 --> 00:00:37,000
And click on that link and post a video of yourself meditating

9
00:00:37,000 --> 00:00:40,000
as a response to that video.

10
00:00:40,000 --> 00:00:44,000
Okay, so today's question comes from minor players,

11
00:00:44,000 --> 00:00:45,000
who asks,

12
00:00:45,000 --> 00:00:50,000
I can get this heavy lost feeling and struggle to find my breath.

13
00:00:50,000 --> 00:00:54,000
I can only find my breath when I resolve to employ a lighter awareness,

14
00:00:54,000 --> 00:00:57,000
floating along in the way of the present moment.

15
00:00:57,000 --> 00:01:01,000
Is this sinking mind or do I just suck at this?

16
00:01:01,000 --> 00:01:03,000
You don't suck at this.

17
00:01:03,000 --> 00:01:06,000
Let's get that off the table.

18
00:01:06,000 --> 00:01:08,000
This is actually, I think,

19
00:01:08,000 --> 00:01:12,000
the answer I'm going to give is probably quite similar to an answer I've given before.

20
00:01:12,000 --> 00:01:17,000
I'm not sure to what video, but it seems there was a similar question.

21
00:01:17,000 --> 00:01:22,000
And this is, of course, a reoccurring question, one that comes up for most people.

22
00:01:22,000 --> 00:01:27,000
The first aspect is in regards to losing the breath.

23
00:01:27,000 --> 00:01:33,000
And we have this obsession that somehow we have to stay with the rising and the falling.

24
00:01:33,000 --> 00:01:35,000
This is our controlling mind.

25
00:01:35,000 --> 00:01:42,000
The mind that needs to control our experience of reality to control the way things are.

26
00:01:42,000 --> 00:01:46,000
This is the cause, or one of the causes of suffering are need to control.

27
00:01:46,000 --> 00:01:51,000
It's an attachment, the inability to accept things as they are.

28
00:01:51,000 --> 00:01:54,000
It's also a wrong meditation practice.

29
00:01:54,000 --> 00:01:57,000
But that's the case for everyone.

30
00:01:57,000 --> 00:01:59,000
We're learning, we're practicing.

31
00:01:59,000 --> 00:02:05,000
So what's happening here is you are expecting things to be a certain way.

32
00:02:05,000 --> 00:02:14,000
They're not that way, and you're trying to force them to be that way by implying some special technique of creating a different state of awareness.

33
00:02:14,000 --> 00:02:25,000
What you're neglecting to focus on is the experience as it is, this experience of the heavy feeling of the heavy lost feeling.

34
00:02:25,000 --> 00:02:30,000
Now, I guarantee that there's no lost feeling.

35
00:02:30,000 --> 00:02:39,000
There's a feeling of heaviness and maybe depression, which is based on anger, the disliking, the upset, the suffering.

36
00:02:39,000 --> 00:02:51,000
And then that's judged by the mind to mean that you're lost, to mean that you're bad, you suck, and so on.

37
00:02:51,000 --> 00:02:59,000
Or, however, it appears to you, this feeling that something's wrong, and there are many different feelings like that.

38
00:02:59,000 --> 00:03:04,000
This is the reality that's there in front of you.

39
00:03:04,000 --> 00:03:10,000
And this is what you should be focusing on, focusing on that feeling, and seeing it for what it is.

40
00:03:10,000 --> 00:03:13,000
This is the meditation practice. This is why we're practicing.

41
00:03:13,000 --> 00:03:16,000
To change the way we react to these things.

42
00:03:16,000 --> 00:03:24,000
Instead of seeing it as a feeling, as bad as something that is unacceptable that we have to change, that we have to fix.

43
00:03:24,000 --> 00:03:35,000
The practice is to see it for what it is, and be able to let it be, just as a feeling. And actually, once you can let it be, it doesn't last.

44
00:03:35,000 --> 00:03:39,000
These feelings are not.

45
00:03:39,000 --> 00:03:42,000
They have no power over us unless we give it to them.

46
00:03:42,000 --> 00:03:51,000
And what you're doing is giving it power, so every time it comes back, it becomes more and more of a monster.

47
00:03:51,000 --> 00:03:55,000
The more power you give to it, and the more importance you place on it.

48
00:03:55,000 --> 00:03:59,000
Once you see it for what it is, except for what it is, it's like untying a knot.

49
00:03:59,000 --> 00:04:03,000
It just disappears, and you should be able to see that.

50
00:04:03,000 --> 00:04:06,000
So, don't try to find your breath.

51
00:04:06,000 --> 00:04:08,000
When your breath is missing, it's because of this feeling.

52
00:04:08,000 --> 00:04:10,000
And this is what I talked about before.

53
00:04:10,000 --> 00:04:13,000
The reason your breath is not smooth, it's not there.

54
00:04:13,000 --> 00:04:20,000
It's not calm. It's because of this feeling, because there's something else that your mind is focusing on.

55
00:04:20,000 --> 00:04:24,000
There's something that is taking the mind and the body out of its ordinary state.

56
00:04:24,000 --> 00:04:28,000
And you should focus on that. There's no reason to go back to focus on the breath.

57
00:04:28,000 --> 00:04:31,000
Now, you've got something new, then something quite important.

58
00:04:31,000 --> 00:04:36,000
Something that has very much to do with the problems that exist in our minds and in our lives.

59
00:04:36,000 --> 00:04:39,000
So, good luck with that.

60
00:04:39,000 --> 00:04:43,000
And just to say that it's a sign that you're practicing correctly, that you're able to see these things.

61
00:04:43,000 --> 00:04:46,000
You're able to see that you can't control your breath.

62
00:04:46,000 --> 00:04:50,000
You have to employ this special technique. It doesn't last.

63
00:04:50,000 --> 00:05:00,000
You keep coming back again and again to this frustrated state of not being able to keep things calm and smooth and so on.

64
00:05:00,000 --> 00:05:05,000
So, try to learn to let go and to let be.

65
00:05:05,000 --> 00:05:07,000
And that's what the practice does for you.

66
00:05:07,000 --> 00:05:12,000
If you just say to yourself, depressed, depressed, heavy, heavy, even just feeling, feeling,

67
00:05:12,000 --> 00:05:18,000
you'll find that it clears up quite quickly and you're able to progress.

68
00:05:18,000 --> 00:05:22,000
You're able to see things in a new way and that's what the meditation is for.

69
00:05:22,000 --> 00:05:46,000
Okay, so thanks for the question. Keep them coming.

