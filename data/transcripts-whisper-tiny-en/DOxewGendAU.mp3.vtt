WEBVTT

00:00.000 --> 00:19.540
Good morning.

00:19.540 --> 00:28.440
Okay, good morning.

00:28.440 --> 00:56.440
The Buddha taught, he said, mom, bikhui, anato, we had it, don't live without a refuge.

00:56.440 --> 01:07.440
Do kho bikhui, anato, we had it.

01:07.440 --> 01:23.440
Someone who is without refuge lives in suffering.

01:23.440 --> 01:41.440
I don't think this is hard for us to appreciate or understand something that should be familiar to many of us.

01:41.440 --> 01:52.440
Those of us who have been in our lives without refuge understand how unpleasant it can be.

01:52.440 --> 02:13.440
It's true for many people that doesn't seem such an urgent need to find protection.

02:13.440 --> 02:25.440
At times life can be pleasant, stable, peaceful, and for those people at those times,

02:25.440 --> 02:36.440
it seems like there's not much need for any kind of protection or refuge.

02:36.440 --> 03:00.440
Only when suffering is calamity, disaster, accident, loss, sickness, conflict.

03:00.440 --> 03:13.440
Then we search for something to protect us from that, or we wish we'd had something that would protect us.

03:13.440 --> 03:19.440
Mostly of course we look outside, we look for an external refuge.

03:19.440 --> 03:30.440
And it's not that there isn't any external refuge, and the Buddha himself is our refuge.

03:30.440 --> 03:37.440
The Buddha and the Buddha and the Buddha are the Buddha for refuge.

03:37.440 --> 03:39.440
The Dhamma is our refuge.

03:39.440 --> 03:39.440
The Dhamma is our refuge.

03:39.440 --> 03:47.440
The Dhamma is our refuge.

03:47.440 --> 03:49.440
The Sangha is our refuge.

03:49.440 --> 03:50.440
The Sangha is our refuge.

03:50.440 --> 03:51.440
The Sangha is our refuge.

03:51.440 --> 03:56.440
The Sangha is our refuge.

03:56.440 --> 03:58.440
The Sangha is our refuge.

03:58.440 --> 04:06.440
But the Buddha also said Atahir, the Nona.

04:06.440 --> 04:12.440
One is one's own refuge.

04:12.440 --> 04:19.240
And the Buddha helps, the Dhamma helps, the Sangha helps, but really mostly only because

04:19.240 --> 04:25.600
we help ourselves.

04:25.600 --> 04:39.880
The Dhamma of the Sangha, the Buddha, help us to help ourselves, give us the tools for

04:39.880 --> 04:52.800
us to do the work, because there are many dangers in the world that no external support

04:52.800 --> 05:00.680
or help or refuge can protect us from.

05:00.680 --> 05:12.520
No parent or guardian, no relative can keep us, relative or friend can keep us from so many

05:12.520 --> 05:29.600
types of suffering, so many sources of danger, externally sickness, pain, and even more

05:29.600 --> 05:33.560
internally.

05:33.560 --> 05:48.160
No one can keep us from greed, anger, delusion, from depression, anxiety, fear, addiction,

05:48.160 --> 06:06.800
there's no greater refuge than our own mind, there's no other refuge, there's no real refuge

06:06.800 --> 06:16.640
outside of our own mind, and so when the Buddha said, don't live without refuge, he

06:16.640 --> 06:32.560
was mainly talking about making yourself a refuge, having the foresight to prepare yourself

06:32.560 --> 06:37.200
for danger.

06:37.200 --> 06:46.600
Not in the sense of anticipating danger, like sitting around waiting for danger to come.

06:46.600 --> 07:02.200
But changing your situation from one that is vulnerable, so that your happiness depends

07:02.200 --> 07:03.840
on circumstance.

07:03.840 --> 07:10.120
If things are like this, I'll be happy, as long as they stay like this, as long as they

07:10.120 --> 07:16.320
aren't like that, that's dependent.

07:16.320 --> 07:24.320
That's vulnerable, so one that is independent and invulnerable, whether things are like this

07:24.320 --> 07:40.760
or like that, I am at peace, that's invulnerable.

07:40.760 --> 07:43.920
So how do we make ourselves a refuge?

07:43.920 --> 07:49.920
How do we gain this invulnerability, this is independent?

07:49.920 --> 08:01.440
Well, again, having a refuge doesn't just mean internal support, the best refuge is your

08:01.440 --> 08:10.960
own mind, but part of that involves the refuge that comes from the support of others.

08:10.960 --> 08:19.200
There's other people, the support of others is also dependent on our mind, whether we

08:19.200 --> 08:27.880
are the sort of person worth protecting, worth supporting, whether other people feel comfortable

08:27.880 --> 08:31.880
supporting us.

08:31.880 --> 08:58.600
And Buddha listed 10 ways of creating refuge, 10 dhamma, 10 dhammas for building a refuge.

08:58.600 --> 09:08.040
They're called the nata karana dhamma.

09:08.040 --> 09:13.560
The first one is Sila, of course.

09:13.560 --> 09:21.600
So many of the lists start with Sila, morality ethics, this is a great refuge.

09:21.600 --> 09:39.800
It's a great description of Sila, a great explanation of the importance of Sila that it's

09:39.800 --> 09:40.800
a refuge.

09:40.800 --> 09:48.120
Sila is not a burden, it's not a burden to stop killing and stealing and lying and cheating

09:48.120 --> 09:56.760
and taking drugs, alcohol, hurting others, speaking harsh words and so on, it doesn't

09:56.760 --> 09:57.760
a burden.

09:57.760 --> 10:11.040
It's a great boon if we are able even to just know these things, even to have a sense of

10:11.040 --> 10:25.920
the importance of abstaining from reckless behavior and speech, harmful behavior and

10:25.920 --> 10:48.640
speech.

10:48.640 --> 10:55.800
It's a refuge, of course, because my breaking precepts by doing things that are

10:55.800 --> 11:03.520
important, well, first of all, I'm moral and unethical, of course, you're going to get into

11:03.520 --> 11:18.200
trouble, get into trouble with the law, but more practically, you get in trouble with

11:18.200 --> 11:19.200
people.

11:19.200 --> 11:29.240
On a very basic level, you get in trouble in your own mind, feeling guilty, cultivating

11:29.240 --> 11:40.200
habits of anger and greed and crookedness, real crookedness, you don't think straight

11:40.200 --> 11:46.200
anymore, you think in terms of manipulating others, not in terms of bringing peace and

11:46.200 --> 11:58.200
harmony, your sense of good is skewed, everything is crooked, it's a great danger.

11:58.200 --> 12:06.200
Siles are a very important part of refuge, it's a very important refuge, it's a very stable

12:06.200 --> 12:12.440
refuge, something we can depend on.

12:12.440 --> 12:24.600
The second one is Bahu Sutta, Bahu Sutta, Bahu Sutta, Bahu Sutta literally hearing, what

12:24.600 --> 12:31.680
say learning is what it really means, the Buddha's time, everything was learned through

12:31.680 --> 12:37.120
voice, very little writing.

12:37.120 --> 12:43.680
And today, you see, much of the dhamma has come full circle, for a long time it was all

12:43.680 --> 12:57.560
written, everything would have to be written down, I mean in terms of this mass production

12:57.560 --> 13:04.200
of dhamma, when I learned the dhamma, it was all, in the beginning it was all oral as

13:04.200 --> 13:11.200
well, and go to a meditation center, the teachings you get are all oral, someone explaining

13:11.200 --> 13:26.840
to you, but we never had this potential to speak, to record our voice, now it's very easy

13:26.840 --> 13:34.160
to find recorded teachings, but the meaning is learning, learning the dhamma from

13:34.160 --> 13:49.160
speech or from printed word, it doesn't matter, Bahu Sutta means having much learning.

13:49.160 --> 13:54.800
Now learning is of course something we trivialise to some extent in Buddhism, don't rely

13:54.800 --> 14:05.200
on learning, don't confuse learning with understanding, it's very different, you can

14:05.200 --> 14:10.560
know everything about the dhamma and know nothing about the dhamma at the same time, two

14:10.560 --> 14:21.240
very different things, but that being said it is a great refuge, without any learning

14:21.240 --> 14:29.240
you have no dhamma, you'd have no practice, practice is not something you can do, find

14:29.240 --> 14:35.800
on your own, without the learning, either you spend all the countless lifetimes the Buddha

14:35.800 --> 14:52.280
spent learning for yourself, or you listen to someone who has been there.

14:52.280 --> 15:05.760
And more importantly that the much learning, that learning gives you a framework within

15:05.760 --> 15:19.160
to grow within which to grow, you don't need to learn a lot to just practice, but to grow

15:19.160 --> 15:26.480
and to keep growing, you need a framework, many of the things we learn in the dhamma are

15:26.480 --> 15:34.120
not immediately applicable, even you learn about these ten, not that karana dhamma, you learn

15:34.120 --> 15:40.200
about all these things, some of them won't immediately be practical, but when you remember

15:40.200 --> 15:48.680
them and you come back to them, then you hear them again and again, even just reminding

15:48.680 --> 15:57.000
yourself of them, having them in your mind allows you to grow into them, learning is

15:57.000 --> 16:03.800
like that, memorizing, memorizing the Buddha's teaching is great, if you're able to distinguish

16:03.800 --> 16:11.880
between what you know and what you only heard, it can be a great protection, a great

16:11.880 --> 16:18.240
refuge, danger is when you confuse them of course, you learn everything and then think

16:18.240 --> 16:29.320
you know everything, then the last way you should learn, if we're able, we should learn

16:29.320 --> 16:39.400
them, we should take care to distinguish what we know and what we don't know, we can

16:39.400 --> 16:48.560
grow into what we don't yet know, the things we've heard that we don't yet know for ourselves.

16:48.560 --> 17:04.440
Number three is Kalyanamita, one should be Kalyanamita, Kalyanamita, one should be a good

17:04.440 --> 17:17.560
friend, one should have good friends, being a good friend is a great refuge, people

17:17.560 --> 17:31.640
appreciate when you're friendly, Kalyanamita is so appraised by the Buddha, friendliness

17:31.640 --> 17:41.200
makes for harmony and peace and a great refuge and we should surround ourselves with

17:41.200 --> 17:49.520
good friends, trying to associate with people who have good qualities, trying to incline

17:49.520 --> 18:03.040
our minds towards cultivating friendship with good people, Kalyanamita means beautiful,

18:03.040 --> 18:10.880
doesn't mean we have to avoid actively avoid people who are unethical and immoral

18:10.880 --> 18:19.760
and harmful, means we have to be able to distinguish and understand what a true friendship

18:19.760 --> 18:38.440
is, true friendship requires kindness, friendliness, requires trust and support, protection,

18:38.440 --> 18:58.640
purpose, a mutual activity of support, care and kindness, thoughtfulness, so a great

18:58.640 --> 19:08.320
refuge to have good friends of course.

19:08.320 --> 19:26.880
Number four, Suwaju, Suwaja, Suwaja means easy in this case, Suwaja is what Suwaja is, being

19:26.880 --> 19:43.040
easy to speak to, easy to admonish is the meaning, it's a great danger to us if we are

19:43.040 --> 19:56.120
intractable and amenable to change, close to any kind of criticism, reactionary to any kind

19:56.120 --> 20:11.080
of judgment or pointing out of our faults, unable to take criticism, so very grave weakness,

20:11.080 --> 20:18.680
great danger to friendship, a great danger to progress, to spiritual development,

20:18.680 --> 20:32.480
obstinency, obstinacy, stubbornness, pride, grave dangers, danger to most if not all people,

20:32.480 --> 20:43.160
well, anyone who is not yet enlightened, stubbornness, intractability, the Buddha talked

20:43.160 --> 20:48.600
about this, and he reminded us if you want help, you have to be easy to help.

20:48.600 --> 20:56.360
Like a patient, if a patient is difficult, won't take their medicine, won't acknowledge

20:56.360 --> 21:03.120
that they're sick, that's a common one, the most dangerous, not acknowledging that you're

21:03.120 --> 21:04.120
sick.

21:04.120 --> 21:10.400
Sometimes we know we're sick, but we don't want to appear sick, we don't let other people

21:10.400 --> 21:15.760
know that we're sick, we don't accept when other people tell us we're sick even though

21:15.760 --> 21:24.000
we know it, because it feels like a vulnerability, a weakness, the grave danger to keep

21:24.000 --> 21:32.120
it hidden, no one will help us, no one will feel comfortable or confident helping us,

21:32.120 --> 21:41.480
feel helpless because we're not letting them help them, help us, and as with the physical

21:41.480 --> 21:56.520
so with the mental, when we don't let other people remind us of our faults, of our mistakes,

21:56.520 --> 22:07.240
point out our mistakes, point out our faults, for not easy to admonish, it's a grave danger.

22:07.240 --> 22:20.000
So it's a great protection for us, a great refuge, if we are amenable to instruction.

22:20.000 --> 22:30.120
A good teacher will not get angry at a student for being stubborn, unruly, hard to teach,

22:30.120 --> 22:39.920
or likewise most teachers will not, a teacher will generally not feel comfortable providing

22:39.920 --> 22:49.800
instruction to someone who is unruly, intractable, to some extent it's pointless, it's

22:49.800 --> 23:04.000
a waste of energy to try and help someone who isn't willing to be helped, isn't open

23:04.000 --> 23:10.160
to being helped, someone who is stubborn and hard to teach, it's a great refuge for us

23:10.160 --> 23:25.000
if we are humble, easy to teach, it's a very important sort of quality that we have

23:25.000 --> 23:34.320
to develop, something we have to keep track of, our pride and stubbornness, we have

23:34.320 --> 23:42.920
to be mindful, we vigilant, you're noticing when it arises and not letting it overwhelm us

23:42.920 --> 23:51.600
and consume us and keep us from receiving instruction, from receiving support, from engaging

23:51.600 --> 24:00.160
in this Anya Manya what Janina, but it said Anya Manya whatcha, Anya Manya means

24:00.160 --> 24:08.600
other and other, it means one another, whatcha, again this whatcha, so whatcha, Anya

24:08.600 --> 24:16.240
Manya whatcha means, speaking to one another doesn't mean, doesn't mean only listening

24:16.240 --> 24:25.920
to our teachers, right, sometimes we feel indignant because someone tries to teach us but

24:25.920 --> 24:38.400
you're not my teacher, we shouldn't be indignant like that, shouldn't be angry when someone

24:38.400 --> 24:46.840
tries to point out our faults, we should consider carefully whether it's true or not

24:46.840 --> 24:55.640
true and if it's true we should thank the person or at least objectively understand that it's

24:55.640 --> 25:02.000
true.

25:02.000 --> 25:22.600
Number five is, well it means helping our fellows with their work.

25:22.600 --> 25:35.720
Being Guernia Nia Nia, Nita Datta and Dakohoti, being keen on supporting others in their

25:35.720 --> 25:44.760
work, so this is, the idea here is in a monastery, monks should take an interest in each

25:44.760 --> 25:54.480
other's work, there's duties and tasks, suppose one monk's Guernia's broken, there's

25:54.480 --> 26:01.120
a leak or something, everyone should come together to help, even in the times when monks

26:01.120 --> 26:14.600
spend their time in silent meditation they would still help each other silently, division

26:14.600 --> 26:22.160
dividing duties in monasteries and so on, so this is for monks but the same of course applies

26:22.160 --> 26:30.480
in any community, in any residence and the general principle is harmony, harmony and mutual

26:30.480 --> 26:43.960
support, this idea of everyone for themselves, that's of course an idea that is antithetical

26:43.960 --> 26:52.240
to any kind of community or harmony and there's a great strength that comes from communal

26:52.240 --> 27:02.840
harmony and it's en sukho san casasamaki, the community that is in harmony, sukho is happiness,

27:02.840 --> 27:17.040
it is happiness, happiness is a harmonious community, it's a great refuge, it's a great refuge

27:17.040 --> 27:23.160
to be supportive of each other, others of course will support you, the community will be

27:23.160 --> 27:34.520
in harmony, the community will grow in flourish, it will attract support and so this of course

27:34.520 --> 27:42.760
is something that is clear for religious institutions but the same goes for families and societies,

27:42.760 --> 27:50.120
our societies will flourish if we help each other, one thing you see in times of trouble

27:50.120 --> 27:59.400
in times of difficulty is the people who come together to help, to support each other,

27:59.400 --> 28:08.440
you see it brings out the best and the worst in people, you can see the best and how

28:08.440 --> 28:17.520
great a refuge that is for society as a whole, for the people on both sides providing

28:17.520 --> 28:30.960
each other with refuge, providing each other with support, number six, dhamma kamo, it's

28:30.960 --> 28:38.120
one of the few times the Buddha uses the word kama in a good way, kama here means love,

28:38.120 --> 28:54.880
dhamma kamo in love with the dhamma, dhamma kamo bhavanghoti, one who loves the dhamma,

28:54.880 --> 29:07.840
one who is in love with the dhamma is a developed person, is a high-minded individual,

29:07.840 --> 29:16.560
motivated person, so obviously the Buddha is not saying we should crave the dhamma or have

29:16.560 --> 29:26.120
any kind of lust or anything, passion, but he acknowledges this attraction, describes

29:26.120 --> 29:32.080
this state of attraction to the dhamma, someone who is intent upon the dhamma, there is

29:32.080 --> 29:43.440
a real feeling of zest zeal because of how pure and profound and perfect the dhamma is

29:43.440 --> 29:51.880
in so many ways, this is a great refuge, it would be a great detriment to us if we looked

29:51.880 --> 30:11.800
at the dhamma with this, with the aversion, if we were a verse or a phrase or displeased

30:11.800 --> 30:19.360
by the dhamma, that would be a great danger to us, and it doesn't have to just mean the

30:19.360 --> 30:25.060
teaching of the Buddha, of course, it's not just because the Buddha taught it, it's a

30:25.060 --> 30:30.680
grave danger for us to look at anything right as though it's wrong, it would be a grave

30:30.680 --> 30:38.880
danger for us to look down upon any kind of practice that is beneficial to have that

30:38.880 --> 30:45.160
sort of wrong view is to be very much on the wrong path, a wrong perspective that, of course,

30:45.160 --> 30:54.040
leads us down other paths, to not see what is important as important, what is unimportant

30:54.040 --> 31:02.000
as unimportant, to not see what is beneficial as beneficial as a grave danger, it is a great

31:02.000 --> 31:08.720
support for us not only to know the dhamma but to be in love with it, in the sense of

31:08.720 --> 31:21.600
appreciating it, having our mind leap at the idea of practicing and learning the dhamma,

31:21.600 --> 31:29.680
when the mind leaps at the idea of doing good deeds, jumps up, ready to act, that's a great

31:29.680 --> 31:35.600
protection, a great refuge for us, of course, because it keeps us on the right path, keeps

31:35.600 --> 31:49.840
us doing good things, it's only our inclination that keeps us away from danger, all bad

31:49.840 --> 32:10.440
inclinations, that's what will lead us to danger, number seven, sun-to-t, sun-to-t means contentment,

32:10.440 --> 32:19.440
contentment is a very great refuge, what's it a refuge from, it's a refuge from greed,

32:19.440 --> 32:28.280
what a danger it is, to be addicted, to need things, and not just things but more and

32:28.280 --> 32:36.840
more and more, whatever you get will not be enough, it could rain gold, and that would

32:36.840 --> 32:46.880
never be enough for one person, we hear about billionaires and how they have so much money,

32:46.880 --> 32:51.120
they don't know what to do with, and then we hear about billionaires becoming trillion

32:51.120 --> 32:59.240
theirs, it will never be enough, that's not how greed works, greed is of a nature to

32:59.240 --> 33:11.840
not ever have enough, there's no appeasement for greed, so being content with nothing is

33:11.840 --> 33:19.960
a very different way of going, it appears powerless in many ways, when you don't have

33:19.960 --> 33:33.600
your powerless, your powerless but you are impervious, person without wealth, without possessions,

33:33.600 --> 33:39.440
is without power in many ways, but they are also without suffering in so many ways, because

33:39.440 --> 33:48.800
they are impervious to loss, contentment doesn't mean not getting or having anything of

33:48.800 --> 34:02.160
course, but it means being content with whatever you get, and on a practice level that's

34:02.160 --> 34:08.120
a very important part of mindfulness, because it no longer applies to possessions and things

34:08.120 --> 34:15.880
it applies to experiences, contentment with whatever experience, not having your happiness

34:15.880 --> 34:25.680
and your peace depend on the vicissitudes of experience, the constant unpredictable nature

34:25.680 --> 34:33.160
of reality, if our happiness depended on that we would always be stressed and suffering,

34:33.160 --> 34:53.280
it is a refuge for us to be content, number seven, number eight, arada vitya, arada vitya,

34:53.280 --> 35:06.560
arada means like strong or unrelenting, effort is a great refuge because laziness is a great

35:06.560 --> 35:22.640
danger, as long as we have this vulnerability, this

35:22.640 --> 35:31.880
correctness, crookedness in the mind, any kind of unwholesumness in our mind, laziness

35:31.880 --> 35:40.000
allows it to breed and fester and grow.

35:40.000 --> 35:48.000
One important characteristic of the path to freedom or the right way to live is that

35:48.000 --> 35:51.280
it involves effort.

35:51.280 --> 35:57.160
That doesn't mean we have to force ourselves to do good things, in fact forcing ourselves

35:57.160 --> 36:04.640
to some extent is lazy, it's much easier to try and force yourself to meditate, for example,

36:04.640 --> 36:10.320
than to actually be mindful, it's a lazy way to be effortful, to just force yourself,

36:10.320 --> 36:17.640
push yourself, effort doesn't mean pushing yourself, it means doing it again and again,

36:17.640 --> 36:28.000
it's quite different from forcing, effort arises in a moment, it has to be coupled with

36:28.000 --> 36:42.640
mindfulness and so the effort to be mindful means the spark at every moment, reminding

36:42.640 --> 36:52.000
yourself, inclining your mind to be mindful, again and again and again that's effort.

36:52.000 --> 37:04.320
Effort for the most part is about being methodical, consistent, not giving up, not backing

37:04.320 --> 37:16.600
off, not slacking off, it's a great refuge to keep our minds in all wholesomeness, requires

37:16.600 --> 37:27.720
effort, repetitive effort, again and again, not letting ourselves become complacent with good

37:27.720 --> 37:36.720
things we've done in the past or what we might do in the future, but repetitive inclination

37:36.720 --> 37:41.920
of the mind to good things.

37:41.920 --> 37:50.440
Number nine is of course sett, sett is the second most important thing, so it's the second

37:50.440 --> 38:02.120
last one, sett is the basis of our practice, sett is the core of our practice, sett

38:02.120 --> 38:09.360
to remember, not things that happened in the past or what we have to do in the future,

38:09.360 --> 38:17.240
but to remember the present, to remember what's happening, to have the mind stay with

38:17.240 --> 38:25.920
the experience without getting caught up in extrapolation or reaction, the mind that

38:25.920 --> 38:35.560
is aware of what's happening right now in the body, the feelings, the mind, the dhamma,

38:35.560 --> 38:42.480
that being in the present moment is the ultimate refuge, when the Buddha boiled us all

38:42.480 --> 38:47.760
down, he ultimately said, when he said, be your own refuge, how do you do that?

38:47.760 --> 38:59.960
He said, practice the force, sett, patana, sett, mindfulness, remembrance, this is the

38:59.960 --> 39:08.360
greatest refuge, said, second most important because the last one is wisdom, so mindfulness

39:08.360 --> 39:21.080
is great because it protects us every moment, but it only protects us at the moment

39:21.080 --> 39:28.400
when we're mindful, if we start being mindful then we lose the protection, it's flawed

39:28.400 --> 39:42.880
that way, it's limited that way, it's not a flaw, it's a limit, satti protects the

39:42.880 --> 39:54.400
mind and that's not just a, it doesn't make it just a temporary solution, it means it's

39:54.400 --> 40:05.000
not the final solution, it's not the ultimate goal, satti is what leans us to the goal

40:05.000 --> 40:11.760
because protecting our mind in this way, protecting our mind from delusion, from the darkness

40:11.760 --> 40:28.120
of ignorance and wrong view and so on, creates clarity in the mind, it allows us to see

40:28.120 --> 40:37.080
clearly the nature of reality, the nature of experience, it allows us to see right from

40:37.080 --> 40:47.720
wrong and good from bad, happiness from suffering, that's the real goal of satti of mindfulness,

40:47.720 --> 40:55.960
yes, it's a great protection, but the real refuge, the ultimate refuge is wisdom, seeing

40:55.960 --> 41:02.720
things clearly and fully and completely as they are, and that's what comes with mindfulness,

41:02.720 --> 41:09.160
the protection that we give to our mind at every moment, brings us to the ultimate

41:09.160 --> 41:18.240
protection that doesn't fade away.

41:18.240 --> 41:30.480
When you see things as they are, banyaya parisudhjati, through wisdom, one is purified, purity

41:30.480 --> 41:50.040
and the freedom from any kind of stain or fault, all it takes is wisdom to be pure, and

41:50.040 --> 41:54.600
see that there's nothing worth clinging to, that everything that arises inside of us and

41:54.600 --> 42:03.320
the world around us is impermanent on satisfying and uncontrollable, not worth clinging to.

42:03.320 --> 42:13.600
Just that wisdom, just that, freezes from all kinds of danger, all of the dangers of clinging,

42:13.600 --> 42:20.360
all of the dangers of misapprehending things as stable, satisfying and controllable, all

42:20.360 --> 42:28.840
the problems that come from misunderstanding reality and clinging to reality as though it might

42:28.840 --> 42:41.400
provide us with stability, satisfaction or control, all that we do away with, just by seeing

42:41.400 --> 42:47.080
clearly wisdom is the greatest refuge, all ten of these provide a really good framework

42:47.080 --> 42:57.000
for Buddhist practice and Buddhist theory.

42:57.000 --> 43:06.040
So that's the dhamma for this morning, a reminder, a refresher, something new for those

43:06.040 --> 43:15.720
who haven't heard it before, the importance of making a refuge for yourself and the means

43:15.720 --> 43:19.640
by which to do that, thank you all for listening.

