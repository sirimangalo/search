Good evening everyone, welcome to our evening session. We all gather together locally
around the internet. To this end of the Dhamma, to discuss the Dhamma, to practice the Dhamma.
Today I got word of a word just yesterday. Last night I think about a talk,
I was being held at McMaster University, something about Buddhism in Myanmar. When I got there,
it was a pleasant surprise they were talking about our tradition, talking about our meditation
tradition that was rather involved, but much of it was based around the practice of mindfulness,
of insight based on the four foundations of mindfulness. I wrote you in a scholarly setting
I was quite incredible to people coming from University of Toronto and the speaker came
from University of Virginia, I think. It's quite encouraging and the talk was lectured
in the way you call it the speaker. It's quite encouraging as well. He talked a lot about
how mindfulness has evolved, the practice has evolved as it's moved and over time and the use
of the word mindfulness, how it's become such a catch word. There are issues and questions
of authenticity and keeping with the tradition, keeping with the original teachings. The
idea that the word mindfulness has come to mean something different over time. As a result,
meditation practice has changed and people's ideas of what it means to practice, but it's
teaching has changed. But nonetheless, to hear people and to see a picture of Mahasi Sayyada
upon the word is quite, quite heartwarming. Even just to hear people talk about him and to
talk about the use of insight meditation, the practice of insight meditation to have a chance
to talk about the discussion afterwards, got onto the topic of what is mindfulness and one
of the members of the audience was quite bold in his conception that mindfulness isn't
good or bad. Mindfulness just fixes your attention on an object or something or reinforces
I'm not actually quite clear what is. I don't think I quite agree with it, but the question
came up whether the idea was that a sniper, a killer, a soldier, can be quite mindful
of what they're doing. The cat can be quite, the cat idea was that it was quite focused.
And yet, if you know anything about the Abidhamma, these are wholesome mind states. So we
have this idea that concentration is wholesome, mindfulness is wholesome, confidence is
wholesome. So what about these people who are confident, these evil people who are confident
about their evil? People who are confident in scamming, manipulating, in oppressing others.
And so I had to give a bit of a correction, I thought to explain something that's interesting
to us because it's an interesting question of karma, what is karma? What is good karma
and bad karma? How can you say something is good karma when bad karma, when there's
so much, it's quite complicated. It actually sounds like some sort of magic, something
very magical or spiritual. When you kill, it sounds nice, actually. When you kill, it's
bad karma and bad things are going to happen to you. You're going to get retribution.
So if it takes on this sort of mystical sort of air, this idea that there's some
karmic power or force, but it's not exactly how it works. I mean, karma is, with much
of the Buddhist teachings, is really to be understood as a meditator, as someone practicing
mindfulness. Karmo was this concept of it was around before the Buddha. It became enlightened
when they had the wrong idea. So he was just explaining to them, what is actually potent?
And so what is actually potent is intention. And so at first you think, well, if you're
intent to kill, okay, then then killing is bad, but it's not even that simple.
Because every moment, if you think about it, when you intend to kill someone, that's
one intention, but the next moment you intend to pick up a knife, the next moment you intend
to go and find the person you're going to kill. And every moment, there's an intention.
It's not even exactly intention, it's your volition or your bent, your inclination at
that moment, your state of mind, really. Chitana. And so I was saying, well, my understanding
from the Abhidhamma, I didn't want to sort of preach as a meditation teacher, but I
can preach to all of you. So as we understand it as meditators, karma is every moment.
So if you say a killer has mindfulness, well, they don't have mindfulness at the moment
when they want to kill. At that moment, there's no mindfulness. They are. I'm actually
not sure. There's no wholesomeness. It's funny now, my Abhidhamma is not clear, but they
were saying that mindfulness is also always wholesome according to the Abhidhamma. I'm not
so sure. Look at this thing and get myself in trouble. Doesn't really matter. We're not
really interested in the technicalities, not right now. Please forgive me for being, for
being imperfect. But the point being, no action is hard to find an action that's entirely
wholesome or something, because wholesome really is referring to the individual that
which brings you happiness, peace, goodness, or, well, bring good things to you. In other
words, things that bring success. So a person's ability to, to successfully shoot someone else.
It actually requires moments of wholesomeness, moments where you are, where you are focused
and concentrated, present, confident. With the problem is, there's so much overarching and so
many moments of very strong ignorance, hatred, disregard for people, for life. You see?
And this is, this is true because we're not always thinking about killing a person, often
we're thinking about lifting up our gun and so on. It'd be mostly unwholesome, but there
will still be moments of wholesomeness. I mean, an easier example might be when you do
something good, when a person, when you give a gift to someone, you think, well, that's
wholesome, right? I say, giving is good. Not exactly. Not necessarily. Not entirely.
Remember when we used to give gifts to Adjantag, everyone is so worried. I have to do it
just right. I started thinking, you know, this worry is not wholesome. We're getting all this
stuff, sitting there, we have to sit around and wait for Adjantag to arrive and he's
always late. Mm, kind of grumpy. I think, well, that's not wholesome.
It's much more complicated, you see? Meditation, maybe people say, yes, I'm going to
meditate, oh, good. There's a lot of unwholesomeness that comes up for meditation. You see,
it's not something to be afraid of. It's not like, oh, well, I better not go and meditate
or I'll get angry, right? It's not magic. It's not a demon that you have to be afraid
of. It's science. It's really psychology. You know, there are things that tear your mind
apart, that remove your mind's ability to function, that deal it, debilitate, that
decompass it, that make you incapacitate you, weaken the mind, cripple the mind, shrink
the mind, but those are just moments and in meditation we're very much interested in studying
this, we're not going to be afraid of it. It's like you're studying diseases or you're
studying pain, you know, we want to understand it. So we're willing to allow ourselves,
give ourselves the room to be angry, give ourselves the space to commit on wholesome
karma, even as meditators. Meditation isn't entirely wholesome. There's a lot of unholesomeness,
an insight meditation, getting angry, it's just why people are skeptical about it sometimes
and they think you have to do with the Buddha said, you know, the Buddha was inclined
to have its meditators because it's much more wholesome to do some at the first focus
your mind and separate your mind away from unholesomeness. It takes a lot of time and a lot
of effort and a lot of ideal conditions. So nowadays it's much more difficult for people
whose minds are consumed and if so much ignorance that we're not even able to tell that
these are bad, these things are bad. That's much quicker and simpler for us to just
muddle through it and see directly, oh yeah, causing myself harm. So good and bad are
momentary, it's quite scary actually if you think about it our whole day, every day throughout
our day we're doing countless unholesome deeds, lots of wholesome deeds as well, hopefully.
But those deeds are just momentary means we have moments of wholesomeness and unholesomeness.
Karma is only really scary when it becomes a habit, a chin to come, it's called or when
it's extreme, when we let it get to the point where we do something, we have a moment
that's just so unforgivable, like killing your parents or hurting a Buddha that kind of
thing, dropping a rock on the Buddha. But that's when it's only really, you know, karma
is not something to be afraid of, something to understand, something to go beyond really.
It's also not something to get caught up in to think, I'm going to do lots of good karma
in that'll make me happy. We have to go beyond karma by studying it and this is, so it
is everywhere. So in everything we do when we give, there's good karma, there's bad karma
when we kill, there's mostly bad karma, but there's still going to be some moments potentially
of wholesomeness. It couldn't still be moments of wholesomeness. I don't want to get in
trouble here with these radical ideas, but it seems to me that there would be moments.
But the point is that only a meditator can truly understand this, when you're meditating
able to see that which causes you suffering, that which causes you peace. I remember when
I was hunting, when I was a teenager, and I was sitting up in a tree with a crow with
a bow. It was the most awful thing to think of now, and I had to sit for a long time
and wait, and I ended up being quite a spiritual experience. This bird came, landed really
right above my head, and listening to the birds, floating by, watching the sunset, watching
it get dark, just sitting alone in the forest up in a tree, starts to get dark. I didn't
quite get, before I got dark, these deer came out into the clearing, and that was, oh,
I'm going to get ready now, it's the time, but they turned out they were female and
young, and you're not allowed to kill them, and you're a special permit. So, okay, I knew
I was, and they came right up and they started eating from the tree, right? I was sitting
and then I was sitting here, and I was looking into the eyes of this female deer, and
she looked at me, and I looked at her, and she chewing, and it was awful to think back
that that was the sort of thing I was interested in, but just to point out, karma is not
so simple. You think of Angulimala, and all of his intent to kill, and then he ended
up becoming an our hunt, not because of all the killing, but because during that time,
he came to understand suffering. You could even argue that through doing evil deeds a
person realizes how awful they are. Sometimes it takes doing an evil theme. Sometimes it
takes someone to be addicted to drugs in order to know that drugs are wrong. I don't want
to give the impression that there's nothing wrong with evil, and this is a good thing
to do, but it's complicated. The Buddha said himself, understanding karma, only a Buddha
can really understand it, and even the understanding that we have is his status. It's not
so easy to understand. I talk about this. I thought it'd be good to talk about because
I want to impress upon you that this is a good way of describing what we're doing in
the practice. We're sorting it out. We're coming to see our habits, the karma that we've
built up as habits, and realizing that some of it's not useful, not beneficial. Some of
it's outright harmful. It's not even about asking whether this is a good deed or that
it's a good deed. It's about understanding mind-states and seeing for ourselves what's
a good mind-state, what's a bad mind-state. You can see clearly because this one leads
to suffering, this one leads to happiness. It's undeniable and it doesn't change. You can
ever get angry and then say, oh, that was fun. I'm angry and look at how much happiness
that brought me. Greed will never make you satisfied. It doesn't have that. It has a very
distinct nature that doesn't change. That's the law of karma. That's what we mean. It's
a law. It might as well be a law because it doesn't ever change. Anyway, so in meditation
just being mindful, watching, and seeing, we see three things. We see the defilement. We
see the karma. We see the result. This is the wheel of karma. We've never heard of this.
There's Kilesa, Kama, Vibhaka, or Jetana, Kilesa, Kama, Vibhaka. Then you have the unwholesome
mind-states that leads you to do things. It leads you to kill. It leads you to steal.
It's even to think bad thoughts. And then there's the result. So we see in meditation
how different mind-states bring different results. It's chaotic. It's not like it's
going to show it in some charge or something. You're going to be able to charge it. But
you're going to send. You'll start to see thinking a lot. Thinking a lot gives me a headache.
It has dilution. Frustration makes me tired. It makes me feel hot and sick inside. Greed
makes me feel kind of dirty or greasy or unpleasant. Makes me feel agitated. Makes me feel
hot as well. It burns us up inside. Then we start to naturally, naturally incline away
from bad karma, but also away from trying to make good karma. As we start to understand
karma more and more, we start to lose our ambitions. We start to lose the need, the drive
to do this or that. But we become more content and more resolved. It's not that we lose our
effort or become lazy. We become more intent on being, not becoming, not just being, not even
being, but in the sense of stopping, staying put and becoming very interested and focused
and all of our energy goes into stopping, into staying, into being, just being isn't
the right word, but what it means is not, not being, not striving, not reaching. In the
sense, striving to, striving to stop striving, striving to stop karma. Can you wait?
Some scattered thoughts. Interesting, very interesting topic. And again, just want to think
about how wonderful it is that meditation is taking off or has taken off and is really
part of the world. You know, we think of the world as being full of problems, full of
dangers, full of unwholesome is really. And there's a lot of, I guess, for lack of a better
world, where there's a lot of evil in the world, if you look at it that way. I don't
think it's so useful to dwell upon the evil. There's a lot of talk about activism and
getting involved. I'm not going to say anything. I'm going to criticize it, but I think
it's, there's room for arguably the, you know, the idea of focusing on the good, like
Pollyanna, sort of, if you read the book, Pollyanna, I was in a book. Yeah. You know,
sometimes if you focus on the good, it becomes your universe. And Buddhism, I think there's,
there's room to talk about this sort of attitude. If we think about all the, all the
meditation that goes on and we gain confidence and encouragement from that and we work
and we focus all of our efforts, not on changing the world or correcting people's
wrong views, but, but work to support people and to support the practice of meditation,
to encourage others to meditate, encourage the cultivation of right view. I think this
is what has, has had this great and lasting impact, bringing goodness to people. You know,
it's never going to fix the world, but it's certainly a good way to live. I think that's
the point. We can't fix the world. We can't change the world, but we can live well. We can
set ourselves in what's right. We can set ourselves on the right path. And we can have
our lives be an outpouring of goodness. There's this through many things, but really the
core of it is we do it through meditation. So it's awesome to see so many people interested
here and, and interested around the world. It sounds like from what we hear, we hear reports,
more and more of people interested in meditation, mindfulness. There you go. That's the
time of our tonight. Thank you all for tuning in.
