You
Okay, good evening. Welcome back. We're welcome. If you haven't joined one of these before.
Tonight, I will be answering questions. If there are any questions about meditation, Buddhism.
So if you'd like to answer, ask a question. Please type it in the chat. If you preface it with
at Nita Damobiku, then I'll know which are questions. Which is just idol chatter.
But there should be no idol chatter. I've turned off my video for these.
Because my intention is not to, but focus on myself, but rather focus on the teaching on the truth, hopefully.
If we can get at the truth. And on you, your focus should be on you.
So it's not about watching a video or even so much about hearing what I have to say.
You can use this session as an opportunity to look inward when you have questions about that.
That's the best sort of question. Questions on your inward path.
So close your eyes if you're not. If you don't have any questions.
And begin to look inside. I recommend that everyone interested in what I do. Take the opportunity to read our booklet on how to meditate.
That's online. It's free. All of what we do is free. No worries there.
But because I can't go over the booklet every time. So we have a booklet. I can't go over the information in there for everyone.
So I have kind of an expectation that you do the preliminary legwork.
And read the booklet and hopefully even begin to try to practice.
If you're really interested and find that it's something you think you might be beneficial.
We also recommend considering an at home meditation course.
Which I've increased during these times.
Because there's a lot more at home people.
Our center is closed to visitors at the moment.
Just as the Canadian borders are closed.
So we have the same two meditators here now as we're here in February.
And instead of having new meditators I'm teaching more at home courses.
If you want to sign up for one of those you can go to our website.
It's very mongolow.org. And find out more about the title of the video is wrong. Thank you.
It's the 18th of the day.
So I'm going to paste questions into our chat.
If you want to enter the video.
If you want to see the question then you should open your eyes and look at the screen once you know what the question is.
You can close your eyes again.
You can just keep it closed. I'll be rattling them off as they come up.
So first question.
How to stop the inner chatter in the mind when meditating?
So our initial goal is not to stop the mind or the chat inner chatter in the mind.
In fact, goals are something you should be careful of.
I'm wary of your whole way through the practice.
Goals are a way of thinking about things.
A way of thinking about reality, a way of thinking about thinking.
They're perspective. They're a part of a perspective that relates to changing.
The way things are rather than changing the way we are in relation to things.
So chat in mental chatter is a thing.
And the goal, if there's to be a goal, is to change the way we look at the chatter.
As a part of what you realize, as you meditate, you may not have started to realize that the chatter isn't really you.
It's not you chattering.
And so you're seeing that most likely, but you're not maybe the beginning we don't get that.
So after you see it enough, you really get that.
And so you don't ask, how can I stop it?
You can't stop something that isn't in your control.
You start changing the way you look at it, becoming less impassioned about it,
less disturbed by it.
When you're less disturbed, of course, that affects it because there is no instigation of it.
And so as a result indirectly, you solve the problem by not trying to solve it.
Because there's less chatter as a result.
Experiencing a lot of fear when meditating.
Often the whole session should I just continue to be mindful of it to overcome it.
Thank you.
Well, thank you for the question.
A lot of fear is, yes, yes, you shouldn't simply note to yourself afraid of it.
You shouldn't be trying to overcome it.
You should be trying to see it clearly, resigning yourself to even the fact that even the fear might stay.
Is that change as your perspective towards the fear? It's no longer a problem.
It's problems, of course, create fear, right? That's the problem with that.
But if it's intense, fear, you might want to look at other things as well.
Why is there such intense and prolonged fear?
Ultimately, it doesn't really matter whether it's intense or prolonged or anything.
It's still just an experience. The point is to stop wishing it away or trying to get rid of it.
Or anything, really.
But look at why you're afraid, for example.
There may not be a reason, but there may also be a reason.
Maybe you're having thoughts about the meditation. Maybe there's thoughts about other things.
Those thoughts make you afraid, so you should also be mindful of those thoughts.
If it's a thought, or maybe it sounds, maybe you're meditating in the jungle,
into your snakes or something, or anything.
Probably you're not meditating in the jungle and hearing snakes, but something like that.
Maybe you're afraid of the dark and you're meditating in your room in the dark, as well.
What is the consequence of following the eight or the five precepts?
I have read that it helps to progress on the spiritual path, but that is quite vague.
Could you elaborate?
So, what is the spiritual path? It's the paths of purification.
Purification means the purification of the mind, on the purification of understanding,
the purification of our psyche.
And the thing about the five precepts is they're about the most impure things you can ever do.
Some of them, anyway.
There are some things that are maybe not technically breaking the five precepts that are just as bad,
but they are among the worst.
And so, they're just bad for your psyche as the point, killing, stealing, lying, cheating, drugs and alcohol.
Not good for the psyche.
Not good for the practice.
So, it's more what are the consequences of not following?
Because you see, the five precepts are in a positive thing.
They're a negative in the sense of a not doing, right?
An abstention.
They're not negative in the sense of being bad, but they're negative in the sense of an abstain being something you don't do.
And so, it's more what are the consequences of doing those things that you shouldn't do.
Oh, wow, they're going to affect your mind.
The things themselves don't, but the state of mind involved in them are pretty severe, like the killing itself isn't wrong.
But the mind states involved with killing are very bad.
I think if you practice some mindfulness meditation, you'll come to see how bad they are.
Even things like drugs or alcohol which seem to some extent innocuous, because you're not hurting someone else.
They're by definition or by nature, they're removing yourself from ordinary reality, their creation of an altered state.
Drugs, it's a bit, you know, they're not all drugs are the same, alcohol certainly is pretty much the opposite of mindfulness.
It's very hard to be mindful or meditative at all when you're on alcohol.
Drugs, like things like marijuana, I think are pretty innocuous, but other drugs like psychedelics, which want to get into it.
Some people say they're good for this or that.
I've done them, they're interesting, but they don't, I'm quite sure they're not.
They're not good for mindfulness practice, they're too powerful, I suppose.
They affect on the mind as too distracting.
I've never tried, I haven't done them since I started meditating, so I can't say what it's like to try and be mindful on them,
but based on my experience, it wouldn't be fruitful.
They're birthday meditation.
The death meditation made me weep the other day, it was the first time I did it.
So is the question here, because the death meditation made you weep, you want the opposite?
Is that the idea, birthday would be something that would make you smile?
I'm not, I'm sorry, it sounds, I don't mean to be mocking, it's kind of funny though.
So first, it's not wrong that you weep, you wept.
Death meditation can be quite powerful, and so that's with any meditation you have to be careful.
That's why we say focus on, make as your main practice mindfulness,
because then the other meditations you do there, they're much more kept in line.
That should be the case with all other meditations you do.
That mindfulness be the backbone of your practice, and everything else will stay solid.
But see the problem with something like birthday meditation as an antidote to death meditation,
is that the whole point of death meditation is to shake you up, is to disturb you,
because we get quite complacent.
If you're not complacent, maybe you don't want, you don't need to do death meditation.
But the only thing that a meditation that makes you feel comfortable and pleasant
will do is keep you complacent, keep you in the rut that you're in.
If you're in a good place, well then you shouldn't need some kind of reassurance like that.
If you need the reassurance, maybe you have to adjust.
Meaning if you're dependent on such things as comfortable pleasant meditations,
then you should cultivate mindfulness to try and get to overcome that, that necessity.
So there's no real point in a burst day meditation in the sense of being something pleasant, I guess.
But, you know, birthday real birthday meditation will go in line with death meditation
in the sense of reminding you you're one you're closer to death.
So it should disturb you as well.
Birthday meditation is the reminder that one of one of the teachers in Thailand,
he actually didn't talk about birthdays.
He said, you know, you have a birthday and everyone thinks,
oh, I've gained a year and the truth is that's ridiculous.
You haven't gained a year.
You've lost a year.
That year's never coming back.
It's gone. You lost it.
So our birthdays should be one year closer to death day,
which is a very sort of Buddhist way of looking at it.
Yes, we're all morbid pessimists.
It seems, I suppose.
But for the reason of trying to wake us up, wake ourselves up,
because by and large we become complacent and ignorant in regards to the realities of things like death,
which is coming closer and closer every day, every moment.
So there's a question about ordaining and I tend to shy away from those,
but I don't really get the question, so I'm going to skip it as well.
I don't I don't really get it.
Sorry.
Do you see ghosts and other spiritual things when you meditate?
I'm not sure if this is actually a serious question or not.
You may see lots of things when you meditate.
They may be real.
They may be hallucinations.
Doesn't really matter.
In meditation and mindfulness meditation practice,
even when you see a person in front of you,
whether you see a person in front of you or an angel or a ghost inside of you,
the answer is still seeing seeing.
You're not concerned or focused on the being.
Your focus is on the experience and the experience there's no being.
There's just the seeing.
So you just say seeing seeing.
Oh, it looks like it was because this person's talking about meditation.
Is it worthwhile to train to be mindful during dreams and deep sleep?
I don't think I've ever had it.
I haven't had a lucid dream, I don't lucid dream like that.
Teacher said that as you practice, you stop dreaming, which I think bears out.
So keep that in mind.
In the beginning of practice, some people are very vivid dreams.
As their mind sort of sort themselves out, it can actually increase the vividity of your dreams of that sort.
So it might be worthwhile. I wouldn't spend too much time on it.
I would be concerned about what is causing the dreams, the lucid vivid dreams.
And if you practice mindfulness, then I think your dreams should settle down.
You won't have to worry too much about them.
I think if you are lucid in your dream, it's worth to be mindful if you can.
Okay, this is how do I copy this one.
Long question.
Is this normal?
So when I meditate, I felt nauseous and I had to go to my room.
I still get more nauseous, got more nauseous, so I tried to prevent it, didn't work.
It's normal.
So isn't normal if you've, some of you know it's not.
I tried to redirect such questions.
The answer is the question isn't whether it's normal.
Should I be concerned maybe? No, you shouldn't be concerned.
I mean, from a health perspective, nausea might be a problem.
But if it's specifically coming through meditation, then I wouldn't worry about it at all.
It actually is a fairly common majority of people experience it, but it is fairly common.
Especially beginners to experience nausea during meditation.
There are some physical changes that might come about for certain people.
It's just the kind of thing where you try and be patient and mindful with it.
That's with everything.
We're not trying to make it go away.
So trying to prevent things is not a part of the practice.
That's a problem because the attitude of trying to prevent things is problematic.
It creates this pattern of behavior where instead of dealing or facing experiences,
you're always running away from them or trying to change them, trying to fix them.
And that attitude is not very beneficial in the long run.
To try and learn to be patient with the nausea.
It's not life-threatening, so there's no real reason to try and get rid of it.
If you don't like it, if you're upset by it, try and load those things as well.
Would you speak a moment about setting the intention to meditate?
I've avoided serious meditation because they anticipate great sorrow and anger when they're right.
So this, I think, is a common problem for everyone, but it's quite common.
And I would suggest we're just looking at this.
It's a fairly good example of the problem.
That your focus here, your perspective is on the intention to meditate,
creating an intention to meditate.
You see that's lacking in you, right?
So you want to cultivate that.
And so that's sometimes a very good thing.
But I think quite often, it's not that you have no intention to meditate.
It's that you have something that's causing aversion to meditation.
And that, I think, is quite common.
I get that a lot with meditators, people on the at-home course, especially,
even in the intensive course, here at our center.
But you even explain it here, and this is a common thing to say,
there's some anticipation, some people anticipate pain.
We don't realize they're anticipating, but it's right there in their psyche.
There's a sort of a wounded animal feeling, sort of instinctual aversion.
You just think about meditation immediately.
There's an aversion that arises.
And that's common, because meditation forces us to look at things that we don't want to look at.
It's very, there's quite a reactist.
There can be quite a strong reaction in the beginning in a negative way.
So the real solution and the more powerful solution is to try and meditate on the aversion.
And in this case, the anticipation, the dread.
They're anticipating something bad.
And that's fine.
I mean, it's not fine, but that's not something that can never get in the way of you meditating,
because you can just meditate on the anticipation.
Try and take that as your object.
And it may be a matter of whittling away at it.
But as your mindful, if you say to yourself worrying or disliking or even anticipating,
if that's a clearest idea of what it is,
and just stay with it once it's gone.
And as it goes away, you'll be able to easily get back into formal meditation.
So the birthday person says it's their birthday.
Today we'll happy birthday.
Happy birthday.
May your birthday be an opportunity for you to reflect on the important things in life.
I think if anything, a birthday meditation would be besides what I said about the one day closer to death,
which is important,
take it as an opportunity to reflect on where you are, to reflect on your path.
So it's a marker stone.
Birthday is a marker.
So you take it as an opportunity to look over your direction in the past year.
It's a marker where we can sit back and reflect what we monks had to consider.
But it's a marker only so to reflect wisely on where we're headed.
So there's a few questions here that I would normally just skip over.
I think I'm going to skip over.
It might be disappointing to people to not get answers,
but I'd like to focus on practical questions about our meditation
or practical questions about Buddhism.
You're welcome to ask it again if you feel like, hey, I shouldn't have skipped that one.
No hard feelings, hopefully.
If your practice is good and you meditate two hours every day and you're mindful during the day,
is it realistic to become enlightened after a while?
I mean, absolutely it depends on who you are, where you are.
If you did that for an eternity, like lifetime after lifetime after lifetime if that's what you mean by a while,
then I think the answer is there's no question you would become enlightened after that kind of a while.
But if you mean after a few weeks of doing that, maybe it's less likely.
Very much depends on who you are.
And also two hours a day, if it's a good question because if you're mindful during the day,
the Buddha said, all it would take is seven days.
But that means you're really mindful during the day.
It means mindful the whole day.
And so you can sort of judge how close you are to being always mindful
if you're trying to be mindful throughout the day, you can get a sense of what percent of the time you actually are mindful
and then you get a sense of where you are.
But it's just too vague to say your practice is good, you're mindful during the day, you meditate two hours every day.
All of those statements are great and on face of it, you won't take long.
But what is it meant by your practice is good and it's usually is an independent observer.
We're making that claim or are you who are biased about your own practice making the claim?
And the meditating two hours does that mean you're mindful the entirety of those two hours or
you're doing the walking and sitting, but your mind is somewhere else.
And of course mindful during the day, what does that mean?
So it's not unthinkable.
I think it's much more likely that someone who undertakes intensive meditation practice
that does eight, ten, twelve even more hours of formal meditation every day
and is mindful in the times in between that it's much more likely that they will experience enlightenment
in no long time.
So you can think in some ways that's what everyone should be working up towards,
at least for a period of time taking time out of your life to do intense practice.
In the Mahasim method one notes every phenomenon during walking meditation,
in the Adjunctang's very Mongol method, you only note the stepping left stepping right.
I'm surprised at saying that. Are you saying that in the Mahasim method?
Well, you're walking. Well, the foot is moving. You know everything else?
Because I mean, first of all, Mahasim Sayyada was a monk who passed away and I think 1982.
So when you say the Mahasim method, you really should be saying what he said in his books
and I don't think in his books he said something like, well walking, you know, other things.
In his books he says things like separating the foot into the step into parts, which is what we do as well.
The first exercise is to say stepping left, stepping right.
But as we go through a chorus, we take the meditator through a second walking step, third, three part walking step and four and five and six.
But that's with guidance. That's not over the internet in a booklet or so.
And of course, if something else distracts you, absolutely. If you read my booklet, it...
Adjunctang's way is one of two. In walking, you can note everything, but you should stop walking first.
Stop walking, note whatever it is, start walking.
And the other way is you can just ignore small things during walking because it gets overwhelming after a while.
So practically speaking, yeah, you can just focus your attention on the walking.
But I mean, it's as much the Adjama-Hasim Sayada technique as anything else is.
There are many teachers who teach what they call the Mahasim Sayada technique.
And it's not to say that the way Mahasim Sayada did it is the only way to do it.
And I'm sure you'll see that if you focus on... If you go and study with other teachers, you'll see they're diverging...
Most if not all of them diverging from the way the Mahasim Sayada taught in some way or another.
So it's not a dichotomy like that. Adjunctang is...
His teaching is very much within the Mahasim Sayada tradition, but it's also very much his way of practicing.
Mindfulness.
Thank you.
13-11. I don't understand your question. I'm sorry.
If you could be a little clearer and more concise with it, you're actually asking.
Project.
Which meditation to choose object-based meditation or object-less meditation?
Nothing doing meditation.
Which is mindful this meditation. There's an object.
Yes, object-based, absolutely. But the object should be what is real.
I'll read my book like if you're interested.
What is your thoughts on? Any question that begins with what is your thoughts on is already suspect?
I mean, not suspect, but I'm not the sort of person.
I mean, the focus here is not on what I think about things so much.
It's on practical questions.
So if you'd read our booklet, you might come to realize that things like enhancing your meditation with sounds is not what we're about.
So the problem with that is you're trying to create some benefit by using sounds.
So that benefit is going to be like a nicer, more pleasant, more powerful, perhaps meditative experience.
And it's going to cultivate you in some sort of reliance on that.
You may not realize that. I would argue that absolutely it's going to create some sort of dependency.
The mindfulness is very, is stripping you bare.
You know, putting you in touch with the mundane, that which we mostly are mostly trying to get away from.
Which is ourselves, our ordinary experience. We find it boring. We find it unpleasant.
The mind is a terrible thing to taste.
And so any reason why you might be using binaural beats,
I'm not quite sure what that is, but I have an idea.
The only reason why you might be using it is probably part of the problem from a mindfulness perspective.
Trying to connect with and stay focused on ordinary experience.
Try to stay focused on ordinary experience.
On the other hand, if it is somehow a crutch, sometimes crutches can be useful in the beginning.
Just don't rely on them in the long term. If you acknowledge that it's a temporary thing until you can actually bear with ordinary reality,
which is a very hard thing to do, then by all means, eventually hopefully you won't need them anymore.
Don't need the crutch.
How does one measure themselves ready for an attempt to ordain?
Yes, so that's why I don't focus on ordination, then I'm not keen on such questions,
because absolutely many people who ordain are not ready for it.
And so your focus, your focus should never be on ordaining, should be on practicing.
And the ordination should simply be seen as something that removes an obstacle to your practice.
So if you don't have any obstacles to practice, meaning the amount that you'd like to practice, you can practice.
Then start there.
And once you get to the point where you're practicing so much that ordinary life just seems to get in the way, then ordain.
But it's not entirely fair, because you could just ordain and go to a place and pick up meditation,
but in this day and age we're so full of defilements that you want to at least clear out some of them and be assured that you'll be able to take on such a higher training of being a monk.
Because it is, I mean, it's higher in the worldly sense, higher in the sense of you have to give up a lot.
You give up money, you give up entertainment, you give up sex, you give up food, getting a food you want, for example.
You give up friendship to some extent, I mean, in the sense of socializing, I mean.
It's all external stuff. Being a monk is nothing internal.
There's another kind of being a monk which we all can do. We can all be monks.
Everyone here could be a monk just by being a monkish or a monking out in your mind.
Which means being very mindful.
When I experience a lot of anxiety, I feel a lot of tension in my chest and neck.
It gets worse during sitting meditation, meditating on these feelings makes it even worse. What can help?
Alright, this is a good example of the specific issue of anxiety because anxiety has some violent effects on the body, or can.
Now, important part of this solution, and we have to kind of look at it as a solution to some extent, is to separate the physical feelings from the mental feelings.
The tension in your chest and neck can never get worse. Why? Because it's not bad to begin with.
Anything physical, from a perspective of a meditator, has to be neutral.
It has to be neither good nor bad. You have to see that. You have to understand that if it gets stronger, it hasn't gotten worse.
And that's an important part of the solution because it's a perspective that has to change.
The perspective that it's bad and it's getting worse is absolutely what triggers anxiety.
It's what snowballs the anxiety is. What turns ordinary anxiety into a full-fledged panic attack.
It's not the only thing, but it's one of the main things, I would say.
So, whatever we were anxious about before becomes actually secondary.
I mean, it does keep coming back and instigate and push that and trigger us again.
But what leads into full panic is, I'm not sure always, but I have been in my experience.
And from what I can see, that's what creates the panic attacks.
That's the getting caught in the loop. It's like an engine, you know?
You have the spark plugs, but the spark plugs don't keep firing, right?
I'm not sure how an engine works now. I thought it was the spark plugs only fire in the beginning and then it's just a chain reaction.
But that's the idea. I don't know how engines work, but I know how the mind works.
Eventually, you don't even need the initial thing you were anxious about.
Only every so often when the engine stalls.
I think I don't know enough about engines.
But only enough, so when the anxiety stalls, then you need that thing to make your anxious again.
But for the most part, it just gives perpetuating itself with the physical.
So try and note the physical and don't give rise to...
Don't give any credence to the idea that it could get worse because it might get stronger, but that's not a problem.
And when you can gain that perspective and you're just able to say tens, tens,
then the anxiety has no hold, there's no purchase, can't increase.
You see, anxiety actually, once you look at the hang of mindfulness, anxiety is actually...
Believe it or not, it's quite easy to overcome. It's quite easy to deal with.
Let's say deal with because it might still come. It should still come because it's a habit.
But it won't bother you. It won't destroy you or cripple you.
Of course, when you're anxious, always say, also say to yourself, anxious, anxious, but even that, you shouldn't say it's just an experience.
As we just should, we never go to war or avoid a draft because of the karma consequences of killing, even if the war seems justified, I either stop and genocide.
No, because stopping in genocide is not... it's not the goal of Buddhism.
Self-defense, even perhaps depending other people.
But we draw the line on killing.
Because we're more concerned with our quality of mind than we are with the state of the world around us.
The world is in turmoil. People dying, people being killed.
And it has causes and conditions. It continues on and on and on. You save certain people from death.
Well, they're still going to die. They may go on to kill others and perpetuate.
And in their next life, they will just do it all over again and get killed again.
So changing the world isn't what we're about.
Because not because, but if you think about it, such open-ended questions, such questions are open-ended.
Or meaning the consequences of saying yes, or that you have to start drawing a line on some line in the sand, or committing to trying to save everyone from death.
And then you start to realize how futile that is.
The natural consequence of your thrust of your question, natural consequences that you ultimately have to spend all your time trying to save beings from death, not just humans, right?
Because then there's the genocide of animals that goes on all the time.
And then you start to realize how futile it is. And now some sorrow just goes on like that.
Stopping people from dying, probably not. The best choice of lives of livelihood.
But defending people, you know, in a sense of proportionate.
Someone tries to kill a friend of yours. You try and stop them.
Even if it means hurting them, perhaps. But we draw the line at killing. It's just extreme.
And problematic. Because no matter how good your intentions are, killing someone is going to be caught up with all sorts of.
Killing is not as easy as we think it is. It's not like a video game. Killing. It's quite different.
And it would shock you if you ever did it. How difficult it is and how truly traumatic it is for the person who does the killing.
The first time anyway, as you get more comfortable with it and your psyche gets more attuned with being a murderer, gets easier.
But at that point, though, you've been pretty bad in shape.
If I meditate and realize no self, what transcends?
There's no transcending. There's no transcending just like there's no self.
There's only letting go.
That's a question.
I'm sorry I'm slow, but you'll slow down.
That's slower than I don't mind.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
Thank you.
