1
00:00:00,000 --> 00:00:07,000
Okay, talking about the fifth precept, I've read the precepts a little different from the other four.

2
00:00:07,000 --> 00:00:13,000
Read this precept is a little different from the other four, which are always bad and always have bad needs for consequence.

3
00:00:13,000 --> 00:00:21,000
They say the bad is when we take the drug and become careless, which will lead the person to violate the other precepts.

4
00:00:21,000 --> 00:00:27,000
It almost sounds like it is okay to take drugs and drink alcohol if you're advanced enough to control yourself.

5
00:00:27,000 --> 00:00:32,000
And not violate any of the other precepts, but is it possible to become so advanced

6
00:00:32,000 --> 00:00:38,000
that intoxicants don't have effect on you, or even if they affect you, you're able to fully control yourself.

7
00:00:38,000 --> 00:00:41,000
Well, this precept is different from the other four.

8
00:00:41,000 --> 00:00:49,000
There's no question about that. That has to be admitted that it isn't directly harming another person.

9
00:00:49,000 --> 00:01:03,000
And it isn't an act in the same way that the other ones are, because it's not the act of drinking that is unethical.

10
00:01:03,000 --> 00:01:07,000
It's really the intention to intoxicate yourself, right?

11
00:01:07,000 --> 00:01:14,000
So the first thing I would say is that I disagree with this interpretation of the fifth one.

12
00:01:14,000 --> 00:01:25,000
The grammar doesn't really allow for it unless you want to be tricky and try to break it up and pretend that that's what it says.

13
00:01:25,000 --> 00:01:26,000
It's not what it says.

14
00:01:26,000 --> 00:01:46,000
It says Surah and Mariah, and Mudja, or Surah and Mariah are things that are Mudja and Bhamadatana.

15
00:01:46,000 --> 00:01:51,000
They are things which are a basis for intoxication.

16
00:01:51,000 --> 00:01:56,000
It means a place, really.

17
00:01:56,000 --> 00:02:08,000
Tana, but it's used metaphorically to refer to a base or something that something in the way.

18
00:02:08,000 --> 00:02:12,000
There are things that are there basis for negligence.

19
00:02:12,000 --> 00:02:20,000
And this is why, as I said, people get the idea that any base of negligence, any base of Bhamadat, which is the opposite of Uphamadat,

20
00:02:20,000 --> 00:02:25,000
is breaking the fifth preset, but it's not.

21
00:02:25,000 --> 00:02:26,000
It depends.

22
00:02:26,000 --> 00:02:29,000
The other thing to note is that the precepts are not commandments.

23
00:02:29,000 --> 00:02:32,000
This is what people fail to understand.

24
00:02:32,000 --> 00:02:36,000
I'm trying to say many things at once here, but I'm trying to keep it in order.

25
00:02:36,000 --> 00:02:39,000
Let's try to get it, try to get in order this.

26
00:02:39,000 --> 00:02:49,000
When I'm trying to explain this idea that the fifth precept is only specifically dealing with drugs and alcohol.

27
00:02:49,000 --> 00:03:00,000
To people who are trying to tell me that it was, it was an inclusive, including gambling.

28
00:03:00,000 --> 00:03:05,000
As I said, I said, ask them, what is the first preset?

29
00:03:05,000 --> 00:03:13,000
And they said, ham casa, which means don't kill animals.

30
00:03:13,000 --> 00:03:19,000
Or you are forbidden, basically, ham means to, means you're forbidden.

31
00:03:19,000 --> 00:03:24,000
But colloquial, it means don't.

32
00:03:24,000 --> 00:03:27,000
The word itself means you're forbidden to.

33
00:03:27,000 --> 00:03:32,000
I forbid you to, something like that.

34
00:03:32,000 --> 00:03:37,000
Yeah, the Buddha forbids you, or the precept forbids you to do that.

35
00:03:37,000 --> 00:03:40,000
And I said wrong.

36
00:03:40,000 --> 00:03:46,000
And they all, what's this crazy fool I'm talking about?

37
00:03:46,000 --> 00:03:47,000
Oh, no.

38
00:03:47,000 --> 00:03:51,000
They had faith in me, but it was, you know, what's he getting at?

39
00:03:51,000 --> 00:03:52,000
So he's getting clever here.

40
00:03:52,000 --> 00:03:56,000
And I said, anyone, anyone, ham, ham, that gee bit,

41
00:03:56,000 --> 00:03:58,000
they had all these different ways.

42
00:03:58,000 --> 00:04:00,000
Don't stop life.

43
00:04:00,000 --> 00:04:05,000
You're forbidden to cut life, that gee bit.

44
00:04:05,000 --> 00:04:10,000
So they had different interpretations, and I said, no, wrong.

45
00:04:10,000 --> 00:04:12,000
They're like, wrong, wrong.

46
00:04:12,000 --> 00:04:16,000
And I said, no, the precepts are not commandments.

47
00:04:16,000 --> 00:04:18,000
They're not forbidding anything.

48
00:04:18,000 --> 00:04:22,000
The precepts, and it's clear from the writing that people forget all the time,

49
00:04:22,000 --> 00:04:27,000
I undertake to refrain from these things.

50
00:04:27,000 --> 00:04:29,000
So you can undertake to refrain from anything.

51
00:04:29,000 --> 00:04:31,000
You can undertake to refrain from coffee.

52
00:04:31,000 --> 00:04:33,000
I undertake to refrain from coffee.

53
00:04:33,000 --> 00:04:37,000
There you've got a new precept, a new sixth precept, if you like,

54
00:04:37,000 --> 00:04:40,000
or I undertake to refrain from x, y, z.

55
00:04:40,000 --> 00:04:43,000
I undertake to refrain from beating up my younger brother.

56
00:04:43,000 --> 00:04:47,000
Well, it's not one of the five precepts, but I'm going to undertake it as a precept.

57
00:04:47,000 --> 00:04:48,000
Good for you.

58
00:04:48,000 --> 00:04:49,000
It's a good thing.

59
00:04:49,000 --> 00:04:55,000
Better not to beat up your little brother.

60
00:04:55,000 --> 00:05:02,000
So it's important to understand what the precepts are that they are a determinant.

61
00:05:02,000 --> 00:05:05,000
They are a determination not to do something.

62
00:05:05,000 --> 00:05:10,000
And so the fifth precept is a determination not to take drugs and alcohol.

63
00:05:10,000 --> 00:05:14,000
And you could interpret as, and other things which are similar,

64
00:05:14,000 --> 00:05:17,000
basis of intoxication.

65
00:05:17,000 --> 00:05:22,000
Manja, sura, maria, manja, bhamadatama.

66
00:05:22,000 --> 00:05:28,000
The things which are a basis for negligence in the same way that alcohol is.

67
00:05:28,000 --> 00:05:30,000
So not in the way that gambling is.

68
00:05:30,000 --> 00:05:34,000
If you want to take a precept against gambling power to you, I don't think the fifth precept,

69
00:05:34,000 --> 00:05:41,000
as it's traditionally understood, is that sort of determination.

70
00:05:41,000 --> 00:05:46,000
I think a person who breaks that, who does it, gamble is in for trouble.

71
00:05:46,000 --> 00:05:53,000
But they're not violating that precept if they've taken this determination

72
00:05:53,000 --> 00:05:55,000
that I'm not going to take drugs and alcohol.

73
00:05:55,000 --> 00:05:58,000
Well, I don't think gambling is going to break that determination

74
00:05:58,000 --> 00:06:00,000
because it's on a whole different level.

75
00:06:00,000 --> 00:06:11,000
Gambling, I think, is not nearly as deleterious, detrimental as taking drugs and alcohol.

76
00:06:11,000 --> 00:06:17,000
This is one thing.

77
00:06:17,000 --> 00:06:22,000
So how that relates to your question is that,

78
00:06:22,000 --> 00:06:26,000
well, that basically, I disagree with this interpretation.

79
00:06:26,000 --> 00:06:31,000
The idea that don't take them to the extent that they lead to negligence.

80
00:06:31,000 --> 00:06:35,000
I mean, the point that they're raising is that negligence is the real problem,

81
00:06:35,000 --> 00:06:37,000
not the drugs and alcohol.

82
00:06:37,000 --> 00:06:44,000
But you're taking this vow because you understand that they are bases of negligence.

83
00:06:44,000 --> 00:06:50,000
And anyone who's practiced meditation and then gone back and taken drugs or alcohol

84
00:06:50,000 --> 00:06:58,000
will tell you that even the smallest, I mean, if you take enough that it's not going to intoxicate you,

85
00:06:58,000 --> 00:07:03,000
then you're not taking enough to break the precept.

86
00:07:03,000 --> 00:07:07,000
As soon as you take even a small amount of any intoxicant,

87
00:07:07,000 --> 00:07:09,000
you become a little bit intoxicated.

88
00:07:09,000 --> 00:07:14,000
And it does affect your brain and affects your ability to think clearly, to think rationally.

89
00:07:14,000 --> 00:07:17,000
It has an effect on your mind.

90
00:07:17,000 --> 00:07:22,000
Now, as Matt said, in answer to your question, what's the point?

91
00:07:22,000 --> 00:07:30,000
And that's another important point to make is that the very intention to take drugs and alcohol is a negative one.

92
00:07:30,000 --> 00:07:37,000
The very intention to imbibe poison is something that is actually going to poison your body

93
00:07:37,000 --> 00:07:44,000
and affect your ability of the mind to function is a negative intention in the first place.

94
00:07:44,000 --> 00:07:53,000
And in light and being would never give rise to such an intention, why would they need to take drugs or alcohol?

95
00:07:53,000 --> 00:08:03,000
And the implication we're over is that they would abstain from it because of the very nature.

96
00:08:03,000 --> 00:08:05,000
But this is something that poisons the body.

97
00:08:05,000 --> 00:08:09,000
It's like, or it's something that poisons the mind as well.

98
00:08:09,000 --> 00:08:18,000
It's something that is done for the purpose of escaping.

99
00:08:18,000 --> 00:08:23,000
The idea that an enlightened being could drink alcohol,

100
00:08:23,000 --> 00:08:30,000
one monk was trying to tell me that a soda panda, because a soda panda apparently keeps all the precepts fully intact.

101
00:08:30,000 --> 00:08:33,000
We'll never break any one of the five precepts.

102
00:08:33,000 --> 00:08:41,000
They said, it will never reach their lips, it will magically, so there's, there'll always something will happen that it can't go into their mouth.

103
00:08:41,000 --> 00:08:50,000
He was telling me because I was in this Cambodian monastery and these laypeople had brought this rise, this sweet rise.

104
00:08:50,000 --> 00:08:54,000
So I've never had it since, I've never been presented with it since.

105
00:08:54,000 --> 00:08:56,000
And it was sweet rise, so it was nice.

106
00:08:56,000 --> 00:08:58,000
And so I started eating it.

107
00:08:58,000 --> 00:09:03,000
And I said, there's alcohol in this and the other monks, no, no, no.

108
00:09:03,000 --> 00:09:07,000
And the laypeople were right there and they were like, getting a little bit worried.

109
00:09:07,000 --> 00:09:09,000
And I said, no, there's alcohol.

110
00:09:09,000 --> 00:09:12,000
And I was a young monk, so I didn't think to be a little bit tactful about it.

111
00:09:12,000 --> 00:09:14,000
I said, no, there's alcohol in this.

112
00:09:14,000 --> 00:09:17,000
And the laypeople were like, oh, because they had brought this rise.

113
00:09:17,000 --> 00:09:20,000
And they said, no, no, no, there's no alcohol in there.

114
00:09:20,000 --> 00:09:21,000
So they were arguing.

115
00:09:21,000 --> 00:09:26,000
And I said, look, so I took a little bit more and I was like, look, how did they make this rise?

116
00:09:26,000 --> 00:09:32,000
And I said, well, he said, well, they put sugar in the rice and whatever milk of them.

117
00:09:32,000 --> 00:09:35,000
No, not milk, but I think just sugar in rice.

118
00:09:35,000 --> 00:09:37,000
And then they put it out in the sun.

119
00:09:37,000 --> 00:09:40,000
And it's like, this is alcohol.

120
00:09:40,000 --> 00:09:42,000
That's how you make alcohol.

121
00:09:42,000 --> 00:09:48,000
And then I got the laypeople really upset.

122
00:09:48,000 --> 00:09:50,000
And then the monk told me he said, oh, yes.

123
00:09:50,000 --> 00:09:54,000
I saw the band of it never touched their lips kind of thing.

124
00:09:54,000 --> 00:09:56,000
He was saying, I don't eat that stuff.

125
00:09:56,000 --> 00:09:58,000
And it's not that I think it's against the precepts.

126
00:09:58,000 --> 00:10:01,000
But I just naturally don't believe me.

127
00:10:01,000 --> 00:10:02,000
He wasn't a soda plant.

128
00:10:02,000 --> 00:10:05,000
I think he maybe he thought he was.

129
00:10:12,000 --> 00:10:18,000
So the idea, and Arahan to enlighten being might take alcohol, I don't know.

130
00:10:18,000 --> 00:10:23,000
I'm not going to argue that it couldn't happen that you could pry their lips open

131
00:10:23,000 --> 00:10:25,000
and force them to drink it.

132
00:10:25,000 --> 00:10:31,000
But to me, the idea that they would do so is to do something so absurd

133
00:10:31,000 --> 00:10:39,000
as to drink poison, especially the poison that affects your mental clarity.

134
00:10:39,000 --> 00:10:45,000
To me, it's an impossibility.

135
00:10:45,000 --> 00:10:47,000
Doesn't quite exactly answer your question.

136
00:10:47,000 --> 00:10:51,000
So your question is whether it might not have an effect on them.

137
00:10:51,000 --> 00:11:02,000
Well, it wouldn't, an enlightened being, if they did drink alcohol, it wouldn't have an effect on their ability to understand

138
00:11:02,000 --> 00:11:09,000
or it wouldn't have an effect on their understanding of reality.

139
00:11:09,000 --> 00:11:12,000
They would still, the mind would become clouded.

140
00:11:12,000 --> 00:11:17,000
But the cloudiness would be simply a cloudy mind.

141
00:11:17,000 --> 00:11:23,000
There would be no judging of it. It wouldn't be impossible for them to give rise to liking or disliking.

142
00:11:23,000 --> 00:11:31,000
Their mind would, the mind would still have a one-to-one relationship.

143
00:11:31,000 --> 00:11:34,000
Because the mind is at that point given up all partiality.

144
00:11:34,000 --> 00:11:43,000
It's given up such a great portion of existence, which is the negative,

145
00:11:43,000 --> 00:11:47,000
unhosted portion of existence. It can't arise again.

146
00:11:47,000 --> 00:11:50,000
So their mind would be cloudy, but it would just be cloudy.

147
00:11:50,000 --> 00:11:52,000
And that's if it could happen at all.

148
00:11:52,000 --> 00:11:55,000
And as I said, some people say it could never happen.

149
00:11:55,000 --> 00:12:17,000
It might do not.

