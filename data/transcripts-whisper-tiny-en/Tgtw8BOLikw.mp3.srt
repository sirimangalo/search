1
00:00:00,000 --> 00:00:06,680
Tomek has a question, what's your opinion on other schools? What do you think about learning

2
00:00:06,680 --> 00:00:13,200
the Dharma from Biku's practicing genres, or even people like Zen masters or Tibetan

3
00:00:13,200 --> 00:00:21,000
rimper shaves? For what point can this be beneficial?

4
00:00:21,000 --> 00:00:27,480
No, I think contentment is the most beneficial. We contend with whatever practice you have

5
00:00:27,480 --> 00:00:32,120
and we contend with the core of the Buddha's teaching. If you, I talked quite a while

6
00:00:32,120 --> 00:00:38,440
about this, or a bit about this anyway, and the last Mongol, I think, yeah, the last talk

7
00:00:38,440 --> 00:00:45,240
on the blessings, contentment, contentment with the core of the Buddha's teaching. So one, contentment

8
00:00:45,240 --> 00:00:49,880
with whatever tradition you find yourself in, but two, contentment with the core. So not

9
00:00:49,880 --> 00:00:55,800
worrying so much about developing this or that genre, this or that mental state or

10
00:00:55,800 --> 00:01:01,640
this or that attainment or this or that practice. Pick one tradition that sticks to the

11
00:01:01,640 --> 00:01:06,400
core of the Buddha's teaching and develop that. Don't be concerned with everything else,

12
00:01:06,400 --> 00:01:14,160
especially because in this day and age, our minds are so weak and so full of confusion and

13
00:01:14,160 --> 00:01:20,840
delusion and defilement that we have to really just take the essence of the Buddha's teaching

14
00:01:20,840 --> 00:01:26,240
as best we can before it disappears, or even finding a good teacher, someone who can lead

15
00:01:26,240 --> 00:01:32,960
you through the, even the core of the Buddha, even just the very basics is difficult. So

16
00:01:32,960 --> 00:01:37,760
we should be content with that. Contentment is the most beneficial. We contend with the

17
00:01:37,760 --> 00:01:45,280
core and content with one practice that is based on the core. And I think that would apply

18
00:01:45,280 --> 00:01:52,880
even in the Buddhist time that it's the contentment with the very essence of reality that

19
00:01:52,880 --> 00:01:57,720
is most important because that is the essence of Buddhism's contentment with reality

20
00:01:57,720 --> 00:02:06,040
as it is not needing to find more, not needing to make something of reality. So in that

21
00:02:06,040 --> 00:02:09,320
sense, it's going to be contentment all the way down, starting with contentment for

22
00:02:09,320 --> 00:02:14,560
a tradition into the contentment with the core of the tradition, the core of the practice,

23
00:02:14,560 --> 00:02:19,280
the core of the Buddha's teaching, and finally to contentment with everything that arises

24
00:02:19,280 --> 00:02:25,160
as it is to see it, come and to see it go into that cling to it, to not want for it, to

25
00:02:25,160 --> 00:02:46,240
come or want for it to go into, finally let go and become free, okay.

