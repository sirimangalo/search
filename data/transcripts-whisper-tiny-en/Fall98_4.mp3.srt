1
00:00:00,000 --> 00:00:06,000
The Dhammatalk, delivered by Vererobo, Osyllanda,

2
00:00:06,000 --> 00:00:12,000
at the Taghata meditation center, San Jose, California,

3
00:00:12,000 --> 00:00:18,000
for special retreat of November, 1998.

4
00:00:18,000 --> 00:00:38,000
Before we continue the discourse, I want to go back to the beginning.

5
00:00:38,000 --> 00:00:48,000
At the beginning of every discourse, as I said yesterday,

6
00:00:48,000 --> 00:00:57,000
there is an information as to where the discourse was delivered

7
00:00:57,000 --> 00:01:02,000
to whom and on what occasion.

8
00:01:02,000 --> 00:01:13,000
So, in that opening message, there is the word in the city,

9
00:01:13,000 --> 00:01:20,000
say, in the city, in Sawati or in Rajagaha and so on.

10
00:01:20,000 --> 00:01:31,000
The English translations just give us, say, in the city of Sawati

11
00:01:31,000 --> 00:01:36,000
or in Rajagaha and so on.

12
00:01:36,000 --> 00:01:43,000
But actually, the monasteries about a lift,

13
00:01:43,000 --> 00:01:47,000
about a lift in were outside the cities,

14
00:01:47,000 --> 00:01:50,000
not inside the cities.

15
00:01:50,000 --> 00:01:56,000
So, the commentaries explained that the locative case

16
00:01:56,000 --> 00:02:04,000
used in describing the location of the monasteries,

17
00:02:04,000 --> 00:02:18,000
indicate not in the city or inside the city,

18
00:02:18,000 --> 00:02:23,000
but in the vicinity of the city or near the city.

19
00:02:23,000 --> 00:02:29,000
So, it is better to translate them as near Sawati

20
00:02:29,000 --> 00:02:32,000
or near Rajagaha and so on.

21
00:02:32,000 --> 00:02:40,000
When they chose sites for monasteries,

22
00:02:40,000 --> 00:02:45,000
they chose a place which is not too far from the city,

23
00:02:45,000 --> 00:02:47,000
not too close to the city.

24
00:02:47,000 --> 00:02:52,000
So, it must be outside the city and where which has easy access

25
00:02:52,000 --> 00:02:54,000
to the city and so on.

26
00:02:54,000 --> 00:03:00,000
Now, the city of Sawati and the Gita or Na?

27
00:03:00,000 --> 00:03:13,000
The Gita or Na monasteries was built in the south of that city.

28
00:03:13,000 --> 00:03:23,000
So, there were two big monasteries near the city of Sawati.

29
00:03:23,000 --> 00:03:28,000
The first one is Gita or Na, built by Anatabintika

30
00:03:28,000 --> 00:03:32,000
and the second is Pubbarama, built by Wisaka.

31
00:03:32,000 --> 00:03:36,000
So, Pubbarama means the eastern park.

32
00:03:36,000 --> 00:03:40,000
So, that monasteries was in the east of the city

33
00:03:40,000 --> 00:03:45,000
and Gita or Na was on the south of the city.

34
00:03:45,000 --> 00:03:50,000
So, although this is not important for the understanding of the dhamma,

35
00:03:50,000 --> 00:04:00,000
it is still good to know that these expressions indicate

36
00:04:00,000 --> 00:04:07,000
and not in or inside the city but outside near the city.

37
00:04:07,000 --> 00:04:13,000
Exoted the monks to be heirs in dhamma and not to be heirs

38
00:04:13,000 --> 00:04:18,000
in material things and after explaining to them some reasons,

39
00:04:18,000 --> 00:04:25,000
he got up and went to his chamber.

40
00:04:25,000 --> 00:04:32,000
So, when the Buddha left, the venerable Sariboda took the responsibility

41
00:04:32,000 --> 00:04:36,000
of continuing the dhamma talk.

42
00:04:36,000 --> 00:04:49,000
Now, sometimes, commentaries give us some information not contained in the sodas

43
00:04:49,000 --> 00:04:52,000
and here the commentaries said,

44
00:04:52,000 --> 00:04:55,000
when it is said that Buddha left,

45
00:04:55,000 --> 00:05:04,000
Buddha did not bodily get up and left the assembly.

46
00:05:04,000 --> 00:05:10,000
He went out by way of mind.

47
00:05:10,000 --> 00:05:16,000
So, that means he just disappeared in his seat

48
00:05:16,000 --> 00:05:21,000
and then appeared in his room, in his chamber.

49
00:05:21,000 --> 00:05:28,000
And the reason the commentary gave was if Buddha

50
00:05:28,000 --> 00:05:35,000
bodily got up and went to his room then the community,

51
00:05:35,000 --> 00:05:39,000
the multitude of monks would have surrounded and followed him

52
00:05:39,000 --> 00:05:44,000
and so the audience would be scattered

53
00:05:44,000 --> 00:05:51,000
and one scattered then it would be very difficult to assemble them again.

54
00:05:51,000 --> 00:05:58,000
venerable Sariboda addressed the monks as friends monks

55
00:05:58,000 --> 00:06:02,000
and then they responded by saying friend

56
00:06:02,000 --> 00:06:10,000
and then Sariboda continued the dhamma talk.

57
00:06:10,000 --> 00:06:14,000
So, at first he asked them a question, friends,

58
00:06:14,000 --> 00:06:19,000
in what way do disciples of the teacher who lived,

59
00:06:19,000 --> 00:06:23,000
not follow him in the experience of seclusion?

60
00:06:23,000 --> 00:06:29,000
And in what way do disciples of the teacher who lived secluded,

61
00:06:29,000 --> 00:06:36,000
follow him in the experience of seclusion?

62
00:06:36,000 --> 00:06:42,000
Actually, he asked the question not because he did not know the answer.

63
00:06:42,000 --> 00:06:45,000
Actually, he was going to give the answer.

64
00:06:45,000 --> 00:06:51,000
Maybe he wanted his audience or he wanted his brother monks

65
00:06:51,000 --> 00:06:55,000
to pay close attention to his talk.

66
00:06:55,000 --> 00:06:58,000
So, he asked this question and then they answered,

67
00:06:58,000 --> 00:07:02,000
friend, we would have come from a very far place,

68
00:07:02,000 --> 00:07:07,000
far away place to learn from you the meaning of this statement.

69
00:07:07,000 --> 00:07:14,000
So, it would be good if you would explain the meaning of this statement to us.

70
00:07:14,000 --> 00:07:19,000
Having heard from you the monks will remember it.

71
00:07:19,000 --> 00:07:26,000
So, they requested him to explain the meaning.

72
00:07:26,000 --> 00:07:31,000
When they responded in this way, when a person was Sariboda

73
00:07:31,000 --> 00:07:36,000
once again bound them, saying friends,

74
00:07:36,000 --> 00:07:41,000
then listen, pay good attention.

75
00:07:41,000 --> 00:07:45,000
I will speak.

76
00:07:45,000 --> 00:07:48,000
When I looked at all things, I started fucking...

77
00:07:48,000 --> 00:07:54,000
I wondered, yes friend, and then Sariboda said,

78
00:07:54,000 --> 00:08:00,000
in what way do disciples of the teacher who lived secluded

79
00:08:00,000 --> 00:08:06,000
do not follow him in the experience of seclusion?

80
00:08:06,000 --> 00:08:10,000
Then this is the question and he gave the answer as

81
00:08:10,000 --> 00:08:16,000
he are disciples of the teacher who lived secluded

82
00:08:16,000 --> 00:08:22,000
do not follow him in the experience of seclusion?

83
00:08:22,000 --> 00:08:28,000
They do not abandon what the teacher tells them to abandon.

84
00:08:28,000 --> 00:08:40,000
And they go after getting abundant requisites,

85
00:08:40,000 --> 00:08:45,000
and they take the teachings of the Buddha lightly

86
00:08:45,000 --> 00:08:50,000
and they have given up the responsibility to practice for a

87
00:08:50,000 --> 00:08:58,000
statement of Nibana, what seclusion or the experience of seclusion?

88
00:08:58,000 --> 00:09:02,000
Now, there are three kinds of seclusion.

89
00:09:02,000 --> 00:09:08,000
Bodyless seclusion, mental seclusion,

90
00:09:08,000 --> 00:09:11,000
and the third is a tough one.

91
00:09:11,000 --> 00:09:19,000
Seclusion from Ubadis,

92
00:09:19,000 --> 00:09:24,000
seclusion from sources,

93
00:09:24,000 --> 00:09:32,000
seclusion from sources of both happiness and suffering.

94
00:09:32,000 --> 00:09:34,000
So there are three kinds of seclusion.

95
00:09:34,000 --> 00:09:40,000
The bodily seclusion means living alone.

96
00:09:40,000 --> 00:09:50,000
Now, Buddha always experienced this bodily seclusion,

97
00:09:50,000 --> 00:10:00,000
although he lived with hundreds of monks in the Jeddah or Namanastri,

98
00:10:00,000 --> 00:10:04,000
he lived in his own chamber alone,

99
00:10:04,000 --> 00:10:11,000
and he would come out only when he wanted to see something to the monks,

100
00:10:11,000 --> 00:10:16,000
or there is something to be done or to meet someone.

101
00:10:16,000 --> 00:10:24,000
If there is no such reason, then he would stay in his chamber alone,

102
00:10:24,000 --> 00:10:28,000
getting into the Samabadi.

103
00:10:28,000 --> 00:10:35,000
So, Buddha always experienced this seclusion of body,

104
00:10:35,000 --> 00:10:38,000
or body of seclusion.

105
00:10:38,000 --> 00:10:47,000
Mental seclusion means mind being away from mental defilements

106
00:10:47,000 --> 00:10:49,000
and mental hindrances.

107
00:10:49,000 --> 00:10:59,000
So, actually, the mental seclusion means getting into genres or Samabadi's

108
00:10:59,000 --> 00:11:05,000
lived in this seclusion, mental seclusion.

109
00:11:05,000 --> 00:11:20,000
It is said that Buddha spent his time entering into the fruition attainment,

110
00:11:20,000 --> 00:11:23,000
or Palasamabadi.

111
00:11:23,000 --> 00:11:29,000
Even when he was giving a talk,

112
00:11:29,000 --> 00:11:32,000
when the audiences are due to Saru,

113
00:11:32,000 --> 00:11:37,000
so during that time, maybe it is just a few seconds,

114
00:11:37,000 --> 00:11:41,000
he would enter into Samabadi and remain in the Samabadi

115
00:11:41,000 --> 00:11:44,000
until the time when the audience finished saying,

116
00:11:44,000 --> 00:11:46,000
saying, Saru, Saru, Saru.

117
00:11:46,000 --> 00:11:51,000
So, that much, he entered into Samabadi.

118
00:11:51,000 --> 00:11:58,000
So, he always lived in mental seclusion.

119
00:11:58,000 --> 00:12:06,000
First, seclusion is called Ka-ya-wie-wie-ka in Pali.

120
00:12:06,000 --> 00:12:10,000
And the second, Chita-wie-wie-ka.

121
00:12:10,000 --> 00:12:18,000
Now, the third, the third seclusion is Upadhi-wie-wie-ka.

122
00:12:18,000 --> 00:12:24,000
And this is a very difficult word to translate.

123
00:12:24,000 --> 00:12:30,000
It is translated in English as sub-stratum of life.

124
00:12:30,000 --> 00:12:33,000
I don't know what that is.

125
00:12:33,000 --> 00:12:38,000
So, we have to go back to the ancient, ancient commentaries.

126
00:12:38,000 --> 00:12:42,000
Now, in the ancient commentaries, it is said that there are

127
00:12:42,000 --> 00:12:58,000
four kinds of Upadhi's.

128
00:12:58,000 --> 00:13:13,000
Kama-Ubadhi, Kilesa-Ubadhi, a Bisan-Kara-Ubadhi, and what is the last one?

129
00:13:13,000 --> 00:13:17,000
Kanda-Ubadhi.

130
00:13:17,000 --> 00:13:21,000
The desirable objects of senses.

131
00:13:21,000 --> 00:13:25,000
There are desirable sides, sounds and so on.

132
00:13:25,000 --> 00:13:28,000
They are called Kama-Ubadhi.

133
00:13:28,000 --> 00:13:36,000
Because they are the receptacle of sources of enjoyment,

134
00:13:36,000 --> 00:13:43,000
sources of heaviness associated with attachment.

135
00:13:43,000 --> 00:13:46,000
So, they are called Upadhi.

136
00:13:46,000 --> 00:13:48,000
And they are also called Kama.

137
00:13:48,000 --> 00:13:54,000
So, we put the two names together and they are called Kama-Ubadhi.

138
00:13:54,000 --> 00:13:58,000
So, Kama-Ubadhi means just the objects, the desirable objects.

139
00:13:58,000 --> 00:14:05,000
We always encounter site, sounds and so on.

140
00:14:05,000 --> 00:14:08,000
Now, you know the word Kilesa, right?

141
00:14:08,000 --> 00:14:10,000
Mental defilements.

142
00:14:10,000 --> 00:14:15,000
So, Kilesa, mental defilements are called Upadhi.

143
00:14:15,000 --> 00:14:23,000
Because they are the sources of what?

144
00:14:23,000 --> 00:14:27,000
Happiness or suffering?

145
00:14:27,000 --> 00:14:31,000
They are sources of suffering, right?

146
00:14:31,000 --> 00:14:37,000
So, they are called Upadhi.

147
00:14:37,000 --> 00:14:52,000
And a Bisan-Kara means wholesomen and wholesome Kama.

148
00:14:52,000 --> 00:14:57,000
So, as a result of wholesome and wholesome Kama,

149
00:14:57,000 --> 00:15:08,000
you will be reborn in blissful states or maybe reborn in full, woeful states.

150
00:15:08,000 --> 00:15:25,000
So, the wholesome and wholesome Kama are called sources of the suffering of existence.

151
00:15:25,000 --> 00:15:30,000
So, you get one existence and you suffer there.

152
00:15:30,000 --> 00:15:37,000
Even if you are born in the celestial state and you suffer death.

153
00:15:37,000 --> 00:15:46,000
So, Kilesa, I mean wholesome Kama and wholesome Kama are called Upadhi.

154
00:15:46,000 --> 00:15:55,000
Because they are sources of suffering in existence.

155
00:15:55,000 --> 00:16:07,000
So, the wholesome and wholesome Kama are the source of suffering in existence.

156
00:16:07,000 --> 00:16:14,000
And Kilesa's mental defilements are source of suffering in woeful states.

157
00:16:14,000 --> 00:16:21,000
As a result of Kilesa's mental defilements, there is rebut in full woeful states.

158
00:16:21,000 --> 00:16:30,000
So, mental defilements are the source of suffering in full woeful states.

159
00:16:30,000 --> 00:16:37,000
Now, the aggregates are also called Upadhi.

160
00:16:37,000 --> 00:16:50,000
Because they are the source of the suffering that is contingent on them.

161
00:16:50,000 --> 00:17:02,000
Now, because we get this aggregate, say aggregate of meta and all five aggregates.

162
00:17:02,000 --> 00:17:15,000
Because we get these five aggregates, we suffer old age, disease, death, sorrow, limitation and so on.

163
00:17:15,000 --> 00:17:22,000
So, these sufferings are contingent upon these five aggregates.

164
00:17:22,000 --> 00:17:29,000
Therefore, the five aggregates are also called Upadhi.

165
00:17:29,000 --> 00:17:38,000
Among the four Upadis, the first one Kama Upadhi is the source of enjoyment.

166
00:17:38,000 --> 00:17:47,000
And the other Upadis, Kilesa, Abhisankara and Kanda are the sources of dukas suffering.

167
00:17:47,000 --> 00:17:55,000
Now, I think you understand the meaning of the word Upadhi.

168
00:17:55,000 --> 00:18:02,000
It is a receptacle or a source.

169
00:18:02,000 --> 00:18:06,000
So, I think we can call them source.

170
00:18:06,000 --> 00:18:10,000
Now, Upadhi, we wake up, right?

171
00:18:10,000 --> 00:18:17,000
We wake up in secretions, secretions from these four Upadis, what is that?

172
00:18:17,000 --> 00:18:35,000
No, no desirable sense objects, no keyless, no mental defilements, no aggregates, no Kama, what's that?

173
00:18:35,000 --> 00:18:37,000
Napa.

174
00:18:37,000 --> 00:18:45,000
So, Upadhi, we wake up means simply Napa.

175
00:18:45,000 --> 00:18:52,000
How did Buddha experience Upadhi we wake up?

176
00:18:52,000 --> 00:19:06,000
Buddha's and Arhan's are persons who had reached the highest stage of enlightenment, who had realized Nibhana.

177
00:19:06,000 --> 00:19:21,000
So, they can experience that Nibhana anytime they like, by entering into the attainment of fruition,

178
00:19:21,000 --> 00:19:24,000
or Palasama Padhi.

179
00:19:24,000 --> 00:19:41,000
So, when Buddha was in the Palasama Padhi, the fruition consciousness arose in his mind billions and billions of times.

180
00:19:41,000 --> 00:19:49,000
And at the same time, the fruition consciousness took Nibhana as object.

181
00:19:49,000 --> 00:19:56,000
So, Nibhana is a suggestion of all suffering.

182
00:19:56,000 --> 00:20:12,000
So, when the consciousness is taking the suggestion of suffering as object, then there is peacefulness,

183
00:20:12,000 --> 00:20:19,000
and that is what we call happiness, therefore, Arhan's and Buddhas.

184
00:20:19,000 --> 00:20:30,000
So, Buddhas and Arhan's can experience the Upadhi we wake up anytime they wish.

185
00:20:30,000 --> 00:20:39,000
And since they have this attained Nibhana, since they have access to this happiness,

186
00:20:39,000 --> 00:20:45,000
they always go for this happiness whenever they can.

187
00:20:45,000 --> 00:20:50,000
So, Buddha lived in seclusion.

188
00:20:50,000 --> 00:21:01,000
In these three kinds of seclusion, bodily seclusion, mental seclusion and seclusion from sources.

189
00:21:01,000 --> 00:21:12,000
The question, put by the venerable Saripoda, in what way do disciples of the teacher,

190
00:21:12,000 --> 00:21:19,000
who live secluded, follow him in the experience of seclusion?

191
00:21:19,000 --> 00:21:26,000
Then in that question, seclusion means all these three kinds of seclusion.

192
00:21:26,000 --> 00:21:32,000
But in his answer to this question, he said,

193
00:21:32,000 --> 00:21:40,000
disciples of the teacher, who lived secluded, do not follow him in the practice of seclusion.

194
00:21:40,000 --> 00:21:46,000
And they do not abandon what the teacher tells them to abandon.

195
00:21:46,000 --> 00:21:55,000
And they go after gaining requisites and they take the Buddha's teachings

196
00:21:55,000 --> 00:22:07,000
lightly, and they have given up responsibility or attempt to practice in order to reach Nibhana.

197
00:22:07,000 --> 00:22:18,000
So, in the answer, seclusion means the first sentence of this answer.

198
00:22:18,000 --> 00:22:25,000
Seclusion means just the first seclusion, bodily seclusion.

199
00:22:25,000 --> 00:22:27,000
Three sentences.

200
00:22:27,000 --> 00:22:32,000
The first sentence has to do with bodily seclusion.

201
00:22:32,000 --> 00:22:41,000
And the second sentence with mental seclusion and a bad sentence with seclusion from sources.

202
00:22:41,000 --> 00:22:49,000
But in the question, seclusion means all three seclusion answers.

203
00:22:49,000 --> 00:22:56,000
The first sentence was, they do not practice body seclusion.

204
00:22:56,000 --> 00:23:03,000
And the second sentence was, they do not abandon what the teacher tells them to abandon.

205
00:23:03,000 --> 00:23:07,000
That means they do not practice mental seclusion.

206
00:23:07,000 --> 00:23:14,000
And then the main thing in the third sentence is, they do not practice to gain Nibhana.

207
00:23:14,000 --> 00:23:20,000
So, they do not practice to gain seclusion from sources.

208
00:23:20,000 --> 00:23:36,000
So, the first seclusion that means if they live in a crowded place and if they

209
00:23:36,000 --> 00:23:42,000
communicate with each other, if they socialize with each other, then they are not following

210
00:23:42,000 --> 00:23:47,000
the Buddha in the practice of bodily seclusion.

211
00:23:47,000 --> 00:23:56,000
And if they do not practice meditation to get away from the mental defilements and mental

212
00:23:56,000 --> 00:24:01,000
hindrances, then they do not practice the mental seclusion.

213
00:24:01,000 --> 00:24:08,000
And if they do not practice Vipasana to attain Nibhana, then they do not practice that

214
00:24:08,000 --> 00:24:13,000
that's seclusion.

215
00:24:13,000 --> 00:24:21,000
And when a Voussari Buddha said, if they do not follow the Buddha in this practice

216
00:24:21,000 --> 00:24:25,000
of seclusion, then they will be blamed.

217
00:24:25,000 --> 00:24:32,000
They will be blamed for these three reasons that they do not practice body seclusion.

218
00:24:32,000 --> 00:24:39,000
They do not practice mental seclusion and they do not practice seclusion from sources.

219
00:24:39,000 --> 00:24:50,000
So, here the Voussari Buddha divided monks into three groups, the elder monks and middle-aged

220
00:24:50,000 --> 00:24:55,000
monks and younger monks.

221
00:24:55,000 --> 00:25:11,000
Now, in Vinya, those who are ten years and over as monks are called elder monks.

222
00:25:11,000 --> 00:25:21,000
And those who are from five years to nine years as monks are called middle-aged monks.

223
00:25:21,000 --> 00:25:29,000
And those who are new and who have not reached the fifth year, so that means one to four

224
00:25:29,000 --> 00:25:33,000
years are called new monks.

225
00:25:33,000 --> 00:25:42,000
So, there are three kinds of monks, new, middle and elder monks.

226
00:25:42,000 --> 00:25:53,000
So, from one to four years, new, five to nine years, middle and ten and above, elders.

227
00:25:53,000 --> 00:26:04,000
And then there is another distinction made, that is, if a monk has been twenty years and

228
00:26:04,000 --> 00:26:08,000
more as a monk, he is called the great elder.

229
00:26:08,000 --> 00:26:14,000
So, there are two kinds of elders, elders and great elders.

230
00:26:14,000 --> 00:26:25,000
So, we can call a monk who has been a monk for twenty years, a great elder of Mahatira.

231
00:26:25,000 --> 00:26:33,000
So, that is why you will see the word Mahatira in some books, when they give the name of

232
00:26:33,000 --> 00:26:35,000
the others, Mahatira.

233
00:26:35,000 --> 00:26:44,000
So, when you see the Mahatira, you can note that this monk is at least twenty years

234
00:26:44,000 --> 00:26:47,000
old as a monk.

235
00:26:47,000 --> 00:26:57,000
And they are to be blamed for these three reasons, because they do not practice.

236
00:26:57,000 --> 00:27:00,000
As body circulation, they do not practice.

237
00:27:00,000 --> 00:27:03,000
Mental circulation and they do not practice.

238
00:27:03,000 --> 00:27:13,000
Now, the opposite of that is, that of animals, I would say, I would put the question,

239
00:27:13,000 --> 00:27:21,000
in what we do disciples of the teacher, who lives, included, follow him in the practice

240
00:27:21,000 --> 00:27:22,000
of seclusion.

241
00:27:22,000 --> 00:27:30,000
Then, the answer is, they practice the inclusion of body of mind and of soses.

242
00:27:30,000 --> 00:27:39,000
So, when they practice all these three seclusion, then they will be praised just for

243
00:27:39,000 --> 00:27:46,000
these three reasons, because they practice body circulation, because they practice

244
00:27:46,000 --> 00:27:52,000
mental seclusion and because they practice seclusion from soses.

245
00:27:52,000 --> 00:28:02,000
Now, the Venamusari Buddha continued the teachings of the Buddha, but he did not mention

246
00:28:02,000 --> 00:28:10,000
the word dhamma-dhyada, I mean, dhamma heritage.

247
00:28:10,000 --> 00:28:18,000
But when he said that if you practice these three kinds of seclusion, you will be praised,

248
00:28:18,000 --> 00:28:27,000
you will be praised, so that means if you take the dhamma heritage, then you will be praised.

249
00:28:27,000 --> 00:28:39,000
So, that means he is indirectly encouraging the monks to take the heritage of dhamma.

250
00:28:39,000 --> 00:28:49,000
Because only when they take the heritage of dhamma, with they practice the three kinds of

251
00:28:49,000 --> 00:28:58,000
seclusion, especially the third seclusion, seclusion from soses, that means nibhana.

252
00:28:58,000 --> 00:29:13,000
So, although Venamusari Buddha did not use the word dhamma heritage, he was actually encouraging

253
00:29:13,000 --> 00:29:25,000
his brother monks to take the dhamma heritage, to practice meditation and to gain enlightenment.

254
00:29:25,000 --> 00:29:40,000
So, the Venamusari Buddha was mentioned when he talked about the mental seclusion.

255
00:29:40,000 --> 00:29:49,000
He mentioned or he said that the monks do not abandon what the teacher tells them to abandon

256
00:29:49,000 --> 00:29:55,000
and they abandon what the teacher tells them to abandon.

257
00:29:55,000 --> 00:29:58,000
Now he wanted to elaborate on that.

258
00:29:58,000 --> 00:30:03,000
What are the states that the teacher tells them to abandon?

259
00:30:03,000 --> 00:30:15,000
So, the Venamusari Buddha described, I mean, mentioned the mental states, one by one,

260
00:30:15,000 --> 00:30:26,000
is to be abandoned and also told them that there is this middle way to abandon these

261
00:30:26,000 --> 00:30:29,000
mental states.

262
00:30:29,000 --> 00:30:41,000
So, the Venamusari Buddha said, friends, greet is evil and hate or anger is evil.

263
00:30:41,000 --> 00:30:53,000
Now, there are sixteen mental states he pointed out and he paired two mental states each

264
00:30:53,000 --> 00:30:56,000
and so there are eight pairs and sixteen mental states.

265
00:30:56,000 --> 00:31:01,000
So, the first pair is of greet and hate.

266
00:31:01,000 --> 00:31:13,000
So, he said, greet is an evil state and hate or anger is an evil state and there is

267
00:31:13,000 --> 00:31:21,000
the middle way for the abandoning of greet and hate.

268
00:31:21,000 --> 00:31:28,000
Venamusari Buddha said, greet is evil and hate is evil.

269
00:31:28,000 --> 00:31:33,000
That means greet is unwholesome and hate is unwholesome.

270
00:31:33,000 --> 00:31:37,000
Why are they unwholesome?

271
00:31:37,000 --> 00:31:46,000
Because they themselves are blame-worthy and they give painful results.

272
00:31:46,000 --> 00:31:52,000
So, these are the two characteristics of being unwholesome state.

273
00:31:52,000 --> 00:31:58,000
They are blame-worthy and they give painful results.

274
00:31:58,000 --> 00:32:03,000
That is why they are said to be unwholesome or evil.

275
00:32:03,000 --> 00:32:13,000
Venamusari Buddha did not just mention these mental states but he pointed out also that there

276
00:32:13,000 --> 00:32:18,000
is a way, a middle way for the abandoning of these mental states.

277
00:32:18,000 --> 00:32:25,000
If he did not mention these, the monks would be at a loss what to do.

278
00:32:25,000 --> 00:32:34,000
So, he mentioned the middle way and he followed the Buddha in describing that middle way

279
00:32:34,000 --> 00:32:46,000
that it gives vision, it gives knowledge and it leads to a cessation of mental defilements.

280
00:32:46,000 --> 00:33:00,000
It leads to direct knowledge, it leads to penetrative knowledge and it leads to nibana.

281
00:33:00,000 --> 00:33:03,000
So, I hope you know the middle way.

282
00:33:03,000 --> 00:33:10,000
It is called the middle way because it does not approach the two extremes.

283
00:33:10,000 --> 00:33:26,000
The first extreme is indulging in sensual pleasures with the belief that they will get rid of,

284
00:33:26,000 --> 00:33:32,000
they could get out of samsara by enjoying the sensual pleasures.

285
00:33:32,000 --> 00:33:40,000
And the other extreme is inflicting pain upon oneself or self-modification.

286
00:33:40,000 --> 00:33:45,000
And the aim is also to get out of this samsara.

287
00:33:45,000 --> 00:34:00,000
But Buddha described these practices as unfruitful.

288
00:34:00,000 --> 00:34:02,000
I mean, profitless.

289
00:34:02,000 --> 00:34:13,000
So, they do not lead to, they do not lead to getting out of the samsara.

290
00:34:13,000 --> 00:34:22,000
So, and Buddha, before he became the Buddha, practiced both these practices.

291
00:34:22,000 --> 00:34:29,000
When he was a prince, he followed the first extreme and when he was in the forest

292
00:34:29,000 --> 00:34:34,000
and practicing meditation, he followed the second extreme.

293
00:34:34,000 --> 00:34:44,000
And when the second extreme did not give him the desired results,

294
00:34:44,000 --> 00:34:52,000
he reviewed his practice of self-modification and then came to the conclusion

295
00:34:52,000 --> 00:34:56,000
that it was a wrong practice.

296
00:34:56,000 --> 00:35:03,000
And he discovered the middle way, the right way,

297
00:35:03,000 --> 00:35:12,000
when he remembered an incident in his childhood.

298
00:35:12,000 --> 00:35:20,000
When he was just an infant, his father, King Sudurana, held a plowing ceremony.

299
00:35:20,000 --> 00:35:29,000
So, at the ceremony, he was left under the tree and the nurses went to see the performance of the king.

300
00:35:29,000 --> 00:35:34,000
And so, when he was alone, he set up and practiced meditation.

301
00:35:34,000 --> 00:35:41,000
And he got into first samsara and he remembered that it was very peaceful.

302
00:35:41,000 --> 00:35:49,000
So, when he remembered this, he decided that this is the real, I mean, the right way.

303
00:35:49,000 --> 00:35:58,000
So, he followed the right way, he practiced the right way and he became the Buddha.

304
00:35:58,000 --> 00:36:08,000
So, when he gave his first saman, he said monks, there are these two extremes that a recluse should not practice.

305
00:36:08,000 --> 00:36:17,000
And I have found or discovered the middle way and he taught the middle way to the five disciples.

306
00:36:17,000 --> 00:36:21,000
And what is this middle way?

307
00:36:21,000 --> 00:36:46,000
This is the noble path that has eight components, namely, right understanding, right thought, right speech, right action, right livelihood, right effort, right mindfulness, and right concentration.

308
00:36:46,000 --> 00:36:58,000
So, these eight components as a whole are called the noble path.

309
00:36:58,000 --> 00:37:02,000
Or, now they are called noble eightfold path.

310
00:37:02,000 --> 00:37:12,000
Eightfold means just having eight components, like a medicine composed of different ingredients.

311
00:37:12,000 --> 00:37:21,000
So, the middle way and the noble eightfold path are the same.

312
00:37:21,000 --> 00:37:28,000
So, sometimes it is called the noble eightfold path and sometimes it is called the middle way.

313
00:37:28,000 --> 00:37:39,000
And, as you all know, the noble eightfold path is the fourth noble truth.

314
00:37:39,000 --> 00:37:49,000
Now, among the four noble truths, the fourth noble truth is just this middle way of the eightfold path.

315
00:37:49,000 --> 00:37:59,000
Now, we must understand that the eightfold path is of two kinds.

316
00:37:59,000 --> 00:38:07,000
The real eightfold path and the preliminary eightfold path.

317
00:38:07,000 --> 00:38:22,000
The real eightfold path is the combination of these eight mental states that arise at the movement of enlightenment.

318
00:38:22,000 --> 00:38:30,000
So, at the movement of enlightenment, the path consciousness arises.

319
00:38:30,000 --> 00:38:41,000
Along with the path consciousness, these eight mental states and also some others arise.

320
00:38:41,000 --> 00:38:51,000
And, it is these eight mental states that really eradicate the mental defilements.

321
00:38:51,000 --> 00:39:11,000
So, when we say this is the path to Nibbana, we understand that the eight mental states at the movement of the path consciousness are the real path.

322
00:39:11,000 --> 00:39:22,000
Because, once a person is on that path, he is sure to reach Nibbana.

323
00:39:22,000 --> 00:39:33,000
Now, in order for the path consciousness to arise, and in order for these eight factors to arise, you have to do something.

324
00:39:33,000 --> 00:39:37,000
They will not arise by themselves.

325
00:39:37,000 --> 00:39:56,000
So, the practice that leads to the arising of the path consciousness and these eight mental states are also called eightfold path.

326
00:39:56,000 --> 00:40:06,000
They are not the real eightfold path, but since they lead to the real path, they are also called eightfold path.

327
00:40:06,000 --> 00:40:09,000
And, they are called preliminary.

328
00:40:09,000 --> 00:40:15,000
Because, they precede the real path.

329
00:40:15,000 --> 00:40:20,000
Without the preliminary path, there can be no real path.

330
00:40:20,000 --> 00:40:26,000
So, the preliminary path is also called the eightfold path.

331
00:40:26,000 --> 00:40:31,000
So, we have two kinds of eightfold path, the preliminary and the real.

332
00:40:31,000 --> 00:40:38,000
And, the real is super mundane, and the preliminary is mundane.

333
00:40:38,000 --> 00:41:00,000
So, in describing the middle way, the venables are able to hear, use some words to describe it, and they are that it gives vision, it gives knowledge and so on.

334
00:41:00,000 --> 00:41:04,000
So, these, let us call them qualities.

335
00:41:04,000 --> 00:41:15,000
These qualities can be applied to both the real path and the preliminary path.

336
00:41:15,000 --> 00:41:34,000
Now, at the moment of enlightenment, path consciousness arises.

337
00:41:34,000 --> 00:41:40,000
And that path consciousness takes Niberna as object.

338
00:41:40,000 --> 00:42:06,000
And, it is said that the path consciousness functions four things, understanding the first noble truth, abandoning the second noble truth, realizing the third noble truth, and developing the fourth noble truth.

339
00:42:06,000 --> 00:42:16,000
So, these are the four functions that path consciousness does at the moment of enlightenment.

340
00:42:16,000 --> 00:42:27,000
And, it does these four functions simultaneously, not one after another.

341
00:42:27,000 --> 00:42:33,000
These four functions are explained with an analogy of a lamp.

342
00:42:33,000 --> 00:42:50,000
So, you light a lamp and the moment you light a lamp, the lamp does the four functions simultaneously,

343
00:42:50,000 --> 00:43:03,000
dispelling darkness, producing light, burning the wick, and making the oil diminish.

344
00:43:03,000 --> 00:43:08,000
So, these functions, a lamp does simultaneously.

345
00:43:08,000 --> 00:43:19,000
In the same way, third consciousness does the four functions, understanding fully understanding the first noble truth,

346
00:43:19,000 --> 00:43:28,000
abandoning the second, realizing the third, and developing the fourth simultaneously.

347
00:43:28,000 --> 00:43:38,000
And, lightenman is called the realization of four noble truths, or sometimes realization of Niberna.

348
00:43:38,000 --> 00:43:44,000
Now, the path consciousness takes Niberna only as object.

349
00:43:44,000 --> 00:43:51,000
It does not take first noble truth as object.

350
00:43:51,000 --> 00:44:04,000
If I have aggregates as object, it does not take craving as object, it does not take the eight factors as object.

351
00:44:04,000 --> 00:44:07,000
It takes Niberna only as object.

352
00:44:07,000 --> 00:44:20,000
But, when it takes Niberna as object, it virtually understands the first noble truth,

353
00:44:20,000 --> 00:44:25,000
abandoning the second noble truth, and developing the third noble truth.

354
00:44:25,000 --> 00:44:35,000
Not that it takes them as object, because at one time only one object can be taken.

355
00:44:35,000 --> 00:44:50,000
But, when the path consciousness arises, taking Niberna as object, the other functions are also done.

356
00:44:50,000 --> 00:45:00,000
So, we say that at the moment of path consciousness, the four noble truths are realized.

357
00:45:00,000 --> 00:45:04,000
Let us see the qualities.

358
00:45:04,000 --> 00:45:09,000
The first quality given is, it gives vision.

359
00:45:09,000 --> 00:45:12,000
The eight noble, eight full path gives vision.

360
00:45:12,000 --> 00:45:17,000
It gives seeing what?

361
00:45:17,000 --> 00:45:20,000
The four noble truths, right?

362
00:45:20,000 --> 00:45:23,000
So, it gives seeing the four noble truths.

363
00:45:23,000 --> 00:45:33,000
That means, when you get the eight factors, when the eight factors arise, then you see Niberna.

364
00:45:33,000 --> 00:45:38,000
You see the four noble truths.

365
00:45:38,000 --> 00:45:41,000
It gives knowledge.

366
00:45:41,000 --> 00:45:42,000
Same.

367
00:45:42,000 --> 00:45:46,000
It gives knowledge of the four noble truths.

368
00:45:46,000 --> 00:45:57,000
And the third is, it leads to cessation of mental defilements.

369
00:45:57,000 --> 00:46:04,000
At the moment of enlightenment, mental defilements are eradicated.

370
00:46:04,000 --> 00:46:12,000
There are four stages of enlightenment, and at each stage, a little of the mental defilements are eradicated.

371
00:46:12,000 --> 00:46:22,000
And so, when a person reaches a fourth and final stage, all remaining mental defilements are eradicated.

372
00:46:22,000 --> 00:46:32,000
So, this noble eight full path leads to the eradication of mental defilements.

373
00:46:32,000 --> 00:46:46,000
It leads to the direct knowledge, again, the direct knowledge of the special knowledge of four noble truths.

374
00:46:46,000 --> 00:46:56,000
It leads to penetrative knowledge, here also, penetrating into the four noble truths.

375
00:46:56,000 --> 00:46:59,000
It leads to Niberna.

376
00:46:59,000 --> 00:47:05,000
It leads to the cessation of all aggregates.

377
00:47:05,000 --> 00:47:17,000
And in this way, these qualities are for the noble eight full path, or the super mundane path.

378
00:47:17,000 --> 00:47:27,000
The preliminary path, we will have different understanding.

379
00:47:27,000 --> 00:47:35,000
First, what is the preliminary path? It is Vipasana.

380
00:47:35,000 --> 00:47:41,000
So, without Vipasana, you do not reach the super mundane path.

381
00:47:41,000 --> 00:47:46,000
So, the Vipasana path is called preliminary path.

382
00:47:46,000 --> 00:48:08,000
So, gives seeing of vision, gives knowledge, and leads to eradication of mental defilements, and leads to direct knowledge, leads to penetrative knowledge,

383
00:48:08,000 --> 00:48:24,000
and leads to the cessation of aggregates.

384
00:48:24,000 --> 00:48:42,000
Okay. It says that it gives seeing, or it gives vision.

385
00:48:42,000 --> 00:48:50,000
So, what vision does Vipasana give?

386
00:48:50,000 --> 00:48:56,000
You have already experienced Vipasana, and you already know.

387
00:48:56,000 --> 00:49:03,000
Okay, I will leave it for tomorrow.

388
00:49:03,000 --> 00:49:28,000
Thank you.

