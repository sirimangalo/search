1
00:00:00,000 --> 00:00:13,000
Okay, um, it says what happens with the mind if someone becomes a once-returner or for non-returner?

2
00:00:13,000 --> 00:00:18,000
Just talk a little bit about these two stages.

3
00:00:18,000 --> 00:00:22,000
What happens to the mind?

4
00:00:22,000 --> 00:00:28,000
Isn't it?

5
00:00:28,000 --> 00:00:33,000
If someone becomes a once-returner or non-returner?

6
00:00:33,000 --> 00:00:41,000
I just need a question.

7
00:00:41,000 --> 00:00:50,000
Yeah, I mean, I could just answer it from the books, but I don't think that's really a satisfying answer

8
00:00:50,000 --> 00:01:00,000
and I don't think that's what they want to hear.

9
00:01:00,000 --> 00:01:09,000
A once-returner.

10
00:01:09,000 --> 00:01:26,000
Once-returner is someone who has really begun to turn away from sensuality, has become totally comfortable,

11
00:01:26,000 --> 00:01:34,000
not just comfortable, but has explored all avenues of sensual pleasure.

12
00:01:34,000 --> 00:01:42,000
Maybe explored isn't the right word. Has come to understand all avenues of sensual pleasure.

13
00:01:42,000 --> 00:01:54,000
Has come to see them objectively for what they are and is no longer frightened by their sexual or their sensual attractions.

14
00:01:54,000 --> 00:02:12,000
Has no longer disturbed by them and sees them simply as an old bad habit, or as one would see the remnants of a bad habit,

15
00:02:12,000 --> 00:02:25,000
like our usage of certain slang words that we just can't kick. They're just kind of hanging around and every so often they come out.

16
00:02:25,000 --> 00:02:31,000
And they've done the same with anger.

17
00:02:31,000 --> 00:02:47,000
And so as a result, these things are not strong in them. They still exist, and this person is not fully developed in their state of mind.

18
00:02:47,000 --> 00:03:01,000
The mind is still subject to, I guess you could say, even hypocrisy in the sense of doing things that are against one's own best wishes.

19
00:03:01,000 --> 00:03:30,000
So once you're turned to can still be married, of course they will never break the five precepts, but that's a soda pun also will not. They will seem, I mean, it's difficult to give this answer really because once you stray outside of the text, you're in a difficult territory.

20
00:03:30,000 --> 00:03:48,000
But they will seem to be quite pure, and it's common to think that everyone who has been practicing meditation for any time, any length of time, is an R-hot.

21
00:03:48,000 --> 00:04:07,000
Or eventually to think that they must be a non-returner or something. It's very difficult, I would say, for most people to tell the difference between one's return or non-returner.

22
00:04:07,000 --> 00:04:24,000
Once returner still has remnants of greed and anger, a non-returner has no remnants of aversion or essential attraction.

23
00:04:24,000 --> 00:04:39,000
They still have desire for becoming a desire for non-becoming desire to be this or to become that.

24
00:04:39,000 --> 00:04:57,000
So maybe they might go out of their way to teach people still. A non-returner, what you would still sense in them is the desire to do good deeds, help other people.

25
00:04:57,000 --> 00:05:19,000
You would still sense in them a sort of activity that would be absent from the R-hot. An R-hot would not have any feeling, you would not get any feeling from them that they were actively seeking anything out.

26
00:05:19,000 --> 00:05:28,000
It's fairly blatantly obvious if you spend some time with them that they have no activity.

27
00:05:28,000 --> 00:05:43,000
I guess the word I'm looking for is intention. They have no karmic intention in the sense of trying to seek out results, whereas an anigami would still have this subtle sense of trying to seek out results.

28
00:05:43,000 --> 00:06:02,000
The difference between an anigami and a sakita gami I want to turn in an on returner is that you would probably see the once returner laughing in a different way. An anigami might still laugh.

29
00:06:02,000 --> 00:06:15,000
They say an R-hot, some people say an R-hot would never laugh. I don't know about that. I don't like to say such things as someone even said that an R-hot would not yawn, which I think is going too far.

30
00:06:15,000 --> 00:06:26,000
We shouldn't attribute any physical characteristics as requirements to be enlightened.

31
00:06:26,000 --> 00:06:32,000
But you might have a different quality of laughter as a once returner.

32
00:06:32,000 --> 00:06:40,000
There would be more physical manifestation of desire.

33
00:06:40,000 --> 00:06:59,000
So you would find them getting excited and even flustered because of their desires, still you would find them more irrational than an anigami.

34
00:06:59,000 --> 00:07:11,000
So from time to time seeking out pleasures or enjoying pleasures or expressing their partiality for certain things, it might still come up.

35
00:07:11,000 --> 00:07:24,000
They have gone far in terms of seeing the problem with these things and they could never hold the belief that central pleasures are a good thing.

36
00:07:24,000 --> 00:07:31,000
By any means and it would be very subtle in a second to come.

37
00:07:31,000 --> 00:07:43,000
But it still might be evident. It should be still evident to a conscious and a careful observer that this person still has partialities.

38
00:07:43,000 --> 00:07:56,000
It still even has desires for beautiful sights and sounds and smells and so we can still get irritated when they see or hear a smell or taste or feel or think something that has been pleasant.

39
00:07:56,000 --> 00:08:00,000
So I would say that is some of the differences.

40
00:08:00,000 --> 00:08:10,000
The best thing to do would be to get an anigami here and a second to get me here and spend some time watching them and study here.

41
00:08:10,000 --> 00:08:13,000
This is the best we can do.

