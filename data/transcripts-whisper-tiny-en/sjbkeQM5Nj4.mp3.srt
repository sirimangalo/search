1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamupanda.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with verse 146, which reads as follows.

3
00:00:13,000 --> 00:00:41,000
What is this? What is this laughter?

4
00:00:41,000 --> 00:00:48,000
Wherefore the joy?

5
00:00:48,000 --> 00:00:53,000
Neetong pajalites a day when there is fire everywhere.

6
00:00:53,000 --> 00:00:57,000
When there is flames, there is always flames.

7
00:00:57,000 --> 00:01:01,000
Neetong, there is constant flame burning there.

8
00:01:01,000 --> 00:01:04,000
When there is constant burning.

9
00:01:04,000 --> 00:01:11,000
Undergari in the Onanda for one who is shrouded by darkness.

10
00:01:11,000 --> 00:01:14,000
Badipang nagavaseta.

11
00:01:14,000 --> 00:01:19,000
For those you who are shrouded by darkness.

12
00:01:19,000 --> 00:01:23,000
You don't seek out a light.

13
00:01:23,000 --> 00:01:30,000
A lamp. Badipa.

14
00:01:30,000 --> 00:01:34,000
This is another memorable story of the Onanda.

15
00:01:34,000 --> 00:01:37,000
There is two versions of it.

16
00:01:37,000 --> 00:01:42,000
There is the reasonable jotika version.

17
00:01:42,000 --> 00:01:50,000
Some might say over the top Dhamupanda version.

18
00:01:50,000 --> 00:01:55,000
I am not really sure which one to focus on.

19
00:01:55,000 --> 00:02:02,000
The Dhamupanda version is quite entertaining.

20
00:02:02,000 --> 00:02:10,000
Some people would argue it is not perhaps as accurate as the jotika version.

21
00:02:10,000 --> 00:02:15,000
What they have in common is that it is about the companions of Wisaka.

22
00:02:15,000 --> 00:02:18,000
Wisaka is a great Buddhist lay disciple.

23
00:02:18,000 --> 00:02:25,000
She had these friends.

24
00:02:25,000 --> 00:02:34,000
Wisaka was in an interesting position because she was married to a man who was not Buddhist.

25
00:02:34,000 --> 00:02:41,000
There is a long story about that.

26
00:02:41,000 --> 00:02:50,000
Sometimes marriage was often about securing connections between wealthy families and so she became a part of that.

27
00:02:50,000 --> 00:03:04,000
But because she was so humble she didn't even argue or she didn't.

28
00:03:04,000 --> 00:03:15,000
She didn't get upset about it at all.

29
00:03:15,000 --> 00:03:24,000
But then she had great trouble and was ended up getting in a real fight with her stepfather.

30
00:03:24,000 --> 00:03:31,000
Her father-in-law, her husband's father, he got very angry at her.

31
00:03:31,000 --> 00:03:34,000
It is a long story. It is not our story here.

32
00:03:34,000 --> 00:03:40,000
But the story we have here is of some of her friends who in this environment were perhaps,

33
00:03:40,000 --> 00:03:49,000
clearly not so inclined towards purity and goodness.

34
00:03:49,000 --> 00:03:53,000
Maybe they weren't such terrible people but they were perhaps misled.

35
00:03:53,000 --> 00:04:02,000
And all of their husbands got together on a drinking festival and got really roaring, stinking drunk.

36
00:04:02,000 --> 00:04:09,000
But the wives, of course, they had to do all the preparations and they weren't allowed to get drunk.

37
00:04:09,000 --> 00:04:13,000
Now the Dhammapada story says that they got drunk anyway.

38
00:04:13,000 --> 00:04:20,000
They convinced Wisaka to take them into this pleasure garden and they got drunk.

39
00:04:20,000 --> 00:04:29,000
When they could be away from the eyes of their husbands and they took strong drink, it says.

40
00:04:29,000 --> 00:04:34,000
And Wisaka asked them, what are you going to do?

41
00:04:34,000 --> 00:04:39,000
Your husbands are going to be upset if you go home, don't know you're drunk and sure enough when they got back,

42
00:04:39,000 --> 00:04:44,000
their husbands beat them all.

43
00:04:44,000 --> 00:04:46,000
And they didn't learn, this is the Dhammapada story.

44
00:04:46,000 --> 00:04:50,000
The Dhammapada story doesn't have any of this, it comes a bit later.

45
00:04:50,000 --> 00:04:53,000
But then they still wanted to get drunk.

46
00:04:53,000 --> 00:04:57,000
And so they asked Wisaka, they said, hey, that's going to garden again.

47
00:04:57,000 --> 00:04:59,000
I said, no, no, I'm not.

48
00:04:59,000 --> 00:05:02,000
I'm not having anything to do with you anymore.

49
00:05:02,000 --> 00:05:10,000
And they said, well, then take us to see that for me I'm Buddhist and I follow the Buddhist teaching.

50
00:05:10,000 --> 00:05:13,000
Strong drink is something that destroys your mindfulness.

51
00:05:13,000 --> 00:05:18,000
That's what's important about this story for us as meditators.

52
00:05:18,000 --> 00:05:21,000
And they said, well, then take us to see the Buddha.

53
00:05:21,000 --> 00:05:24,000
There's anything to get them out of the house, right?

54
00:05:24,000 --> 00:05:34,000
But they, so she took them to see the Buddha and they snuck, alcohol, jugs under their dresses, under their cloaks.

55
00:05:34,000 --> 00:05:39,000
And so they all showed up in the monastery of the Buddha.

56
00:05:39,000 --> 00:05:45,000
It would have been Jetawana, or been in India, it's a beautiful place.

57
00:05:45,000 --> 00:05:50,000
And so they sat down and the Buddha started preaching.

58
00:05:50,000 --> 00:05:55,000
They smuggled the alcohol out and they all got drunk.

59
00:05:55,000 --> 00:06:03,000
And so the Buddha started, the Buddha was preaching, but they couldn't hear the tipping back and forth.

60
00:06:03,000 --> 00:06:07,000
And then they started, and they got it in their heads that they would start dancing.

61
00:06:07,000 --> 00:06:13,000
And so they all got up and started dancing in front of the Buddha.

62
00:06:13,000 --> 00:06:19,000
And then the Dhammapada story says, again, this is where it gets a little bit exaggerated maybe.

63
00:06:19,000 --> 00:06:29,000
But it says there was a Mara Angel, a deity of the Mara realms, the evil, the mischievous realms.

64
00:06:29,000 --> 00:06:39,000
We decided it would be a good idea to get these women to act really improper and so she sort of possessed them

65
00:06:39,000 --> 00:06:45,000
or something and cut them all to strip off their clothes and dance around naked in front of the Buddha.

66
00:06:45,000 --> 00:06:47,000
Quite a sight.

67
00:06:47,000 --> 00:07:06,000
And so both stories make clear that the Buddha sent forth some sort of special magical power, some projection of color, of very dark rays of not lightness,

68
00:07:06,000 --> 00:07:11,000
but rays of darkness or dark blue light or something.

69
00:07:11,000 --> 00:07:25,000
And it frightened them. He shocked them back into sobriety somehow.

70
00:07:25,000 --> 00:07:35,000
And then he taught this verse.

71
00:07:35,000 --> 00:07:47,000
Why are you laughing? Wherefore the joy?

72
00:07:47,000 --> 00:07:56,000
When there is constant flame, it's burning constantly.

73
00:07:56,000 --> 00:08:03,000
The world, the commentary says it's referring to the word, the world is burning actually.

74
00:08:03,000 --> 00:08:11,000
So it sounds, I think, for the ordinary listener, it sounds kind of exciting.

75
00:08:11,000 --> 00:08:24,000
I imagine for the meditators there might be a twin, a slight thought of dancing, singing, maybe even alcohol.

76
00:08:24,000 --> 00:08:30,000
I suppose if you've started meditating alcohol, it's less and less appealing.

77
00:08:30,000 --> 00:08:36,000
As you start to see how messed up your mind already is without alcohol.

78
00:08:36,000 --> 00:08:41,000
But dancing and singing, they seem like wonderful things. It's exciting, right?

79
00:08:41,000 --> 00:08:52,000
It releases all sorts of pleasant chemicals in the brain and makes you feel lots of pleasure.

80
00:08:52,000 --> 00:09:01,000
We don't see the fire. Intoxication, it's an interesting topic.

81
00:09:01,000 --> 00:09:04,000
The alcohol is something that intoxicates you.

82
00:09:04,000 --> 00:09:12,000
We should talk about this because it's an issue in meditation, practice, the living of your life.

83
00:09:12,000 --> 00:09:14,000
What are the things that are going to support your practice?

84
00:09:14,000 --> 00:09:17,000
What are the things that are going to hinder it?

85
00:09:17,000 --> 00:09:24,000
Alcohol is one of the things that's the antithesis of meditation.

86
00:09:24,000 --> 00:09:27,000
It's the opposite.

87
00:09:27,000 --> 00:09:34,000
The meditation is about clearing your mind, trying to cultivate the habit of seeing things clearly as they are,

88
00:09:34,000 --> 00:09:44,000
and alcohol is for the explicit purpose of dulling and muddling things, so that you can't see things.

89
00:09:44,000 --> 00:09:52,000
Your ordinary reactions to things are dull.

90
00:09:52,000 --> 00:09:56,000
As a result, you never feel any pain or suffering.

91
00:09:56,000 --> 00:10:03,000
You're just able to do whatever you want, cultivate whatever bad habits you like.

92
00:10:03,000 --> 00:10:08,000
You get angry and there's no consequences because you're not aware of them.

93
00:10:08,000 --> 00:10:15,000
That's really the thing about alcohol is that it blinds you to the nature of your actions.

94
00:10:15,000 --> 00:10:20,000
I've told this story before, but there's one night.

95
00:10:20,000 --> 00:10:26,000
Remember explicitly, I mean, I used to get drunk and thinking back.

96
00:10:26,000 --> 00:10:31,000
I don't suppose I did a lot of terrible things, but there was one night that I remember explicitly.

97
00:10:31,000 --> 00:10:43,000
I was just feeling with the wrong crowd and ended up going around stealing other people's alcohol and waking people up

98
00:10:43,000 --> 00:10:54,000
and bothering people in the middle of the night and getting into all sorts of trouble and ended up getting seriously damaging friendships as a result.

99
00:10:54,000 --> 00:11:05,000
There's so many stories and the results of alcohol, abuse comes from alcohol and violence of all sorts.

100
00:11:05,000 --> 00:11:15,000
Of course, sexual promiscuity comes unwanted pregnancies, I imagine.

101
00:11:15,000 --> 00:11:24,000
But it cultivates all sorts of terrible bad habits. It allows your mind to freedom to be as evil as you want.

102
00:11:24,000 --> 00:11:30,000
You could be good under alcohol, I suppose. It's possible that under alcohol you could be quite nice to people.

103
00:11:30,000 --> 00:11:37,000
There are times, you know, the happy drunk who sits there and says, I love you, man.

104
00:11:37,000 --> 00:11:41,000
You could argue there's some wholesomeness going on there.

105
00:11:41,000 --> 00:11:47,000
There's a lot of delusion and wholesomeness.

106
00:11:47,000 --> 00:11:55,000
But it prevents you from being able to see clearly. It's a very essence of what it does.

107
00:11:55,000 --> 00:12:01,000
So first of all, that's something you have to be clear about.

108
00:12:01,000 --> 00:12:09,000
Things like drinking alcohol, most drugs as well are highly problematic for meditation practice.

109
00:12:09,000 --> 00:12:15,000
And I think it's an obvious one.

110
00:12:15,000 --> 00:12:18,000
It just makes things to distort it.

111
00:12:18,000 --> 00:12:23,000
People who do marijuana might argue I've done it before, I was never really.

112
00:12:23,000 --> 00:12:30,000
It a lot of marijuana for a time.

113
00:12:30,000 --> 00:12:37,000
Propomatic doesn't allow you to have a clear mind and to see things clearly as there.

114
00:12:37,000 --> 00:12:40,000
As men, these things are marijuana and alcohol.

115
00:12:40,000 --> 00:12:42,000
In general, they're meant to dull things.

116
00:12:42,000 --> 00:12:51,000
They're meant to bring positive states only and allow you free from the need to experience the harshness of reality.

117
00:12:51,000 --> 00:12:58,000
Because reality can become quite harsh if you're not well-trained and sharp as a diamond.

118
00:12:58,000 --> 00:13:06,000
It will cut you.

119
00:13:06,000 --> 00:13:12,000
So the first part, the other part is that it allows us to talk about the other kind of intoxication.

120
00:13:12,000 --> 00:13:20,000
It's the intoxication of the ordinary people, the intoxication that's present in all of us at all times.

121
00:13:20,000 --> 00:13:25,000
Not at all times, but throughout our lives.

122
00:13:25,000 --> 00:13:30,000
Regardless of what we're taking into our bodies, it's coming from our own minds.

123
00:13:30,000 --> 00:13:36,000
Just the chemicals in the brain, this is enough to intoxicate anyone.

124
00:13:36,000 --> 00:13:43,000
In Buddhism, the Buddha mentions, I think, four types of intoxication, or three types of intoxication,

125
00:13:43,000 --> 00:13:57,000
intoxication with youth, intoxication with health, and intoxication with life.

126
00:13:57,000 --> 00:14:00,000
I can't remember if there's a fourth bit.

127
00:14:00,000 --> 00:14:05,000
So young people, young people are intoxicated with youth.

128
00:14:05,000 --> 00:14:10,000
So it leads us to, to far more alcohol drugs in alcohol,

129
00:14:10,000 --> 00:14:16,000
and then, well, get to do far more than we would otherwise.

130
00:14:16,000 --> 00:14:18,000
Very young, right?

131
00:14:18,000 --> 00:14:28,000
And teenage years, some people never grow out of it, but when we're young, we're inclined to do all sorts of crazy things,

132
00:14:28,000 --> 00:14:31,000
thinking, we're never going to die, and there'll be no consequences.

133
00:14:31,000 --> 00:14:39,000
It's unfortunate because there's so much potential, and that's what you get intoxicated by, infinite potential.

134
00:14:39,000 --> 00:14:44,000
So do whatever you want, no consequences, but there are consequences.

135
00:14:44,000 --> 00:14:49,000
It's unfortunate part is when we're young, that's when we have the power.

136
00:14:49,000 --> 00:14:53,000
The Buddha said, this practice is not for old people.

137
00:14:53,000 --> 00:14:57,000
They have all these Buddhists nowadays, and even in the Buddhist time saying,

138
00:14:57,000 --> 00:15:04,000
oh, when I get old, then old practice, when I'm retired, right?

139
00:15:04,000 --> 00:15:08,000
It was a thing in the Buddhist time, even.

140
00:15:08,000 --> 00:15:12,000
Old men would go off into the forest and tend to the flame.

141
00:15:12,000 --> 00:15:15,000
It was about all they were good for us, pouring butter on the flame.

142
00:15:15,000 --> 00:15:22,000
It was really just practice. Forget about meditation.

143
00:15:22,000 --> 00:15:26,000
Some old people can be very good meditators. It's not the same, but the Buddha said,

144
00:15:26,000 --> 00:15:33,000
this is a hard practice if you've spent your life engaged in unholsiness.

145
00:15:33,000 --> 00:15:35,000
It gets more and more difficult.

146
00:15:35,000 --> 00:15:41,000
It's not to disparage people who are older than practice, but it's to wake up those people

147
00:15:41,000 --> 00:15:46,000
who are intoxicated with youth and think, oh, I've got lots of time.

148
00:15:46,000 --> 00:15:53,000
I'm sure you have lots of time, but lots of time spent doing cultivating unholsom habits

149
00:15:53,000 --> 00:15:57,000
as just setting yourself up for disaster.

150
00:15:57,000 --> 00:16:01,000
When you come around to do meditation, you've built up all these bad habits

151
00:16:01,000 --> 00:16:11,000
and you're no longer in a state to be able to meditate.

152
00:16:11,000 --> 00:16:20,000
We're intoxicated with health, healthy fit, healthy and fit.

153
00:16:20,000 --> 00:16:24,000
Thinking illness will never come to me.

154
00:16:24,000 --> 00:16:26,000
You can intoxicated by it.

155
00:16:26,000 --> 00:16:30,000
It doesn't really matter whether to talk about getting old or getting sick.

156
00:16:30,000 --> 00:16:33,000
Of course, that's the reality of it, right?

157
00:16:33,000 --> 00:16:40,000
You could get sick at any time and you're going to get old and youth will not last forever.

158
00:16:40,000 --> 00:16:51,000
But more to the point is just being attached and becoming intoxicated with youth and your health.

159
00:16:51,000 --> 00:17:01,000
Healthy people get so wrapped up in materialism, exercising, running, dancing, singing, right?

160
00:17:01,000 --> 00:17:08,000
You see, these women dancing and singing enjoying their bodies.

161
00:17:08,000 --> 00:17:10,000
So much that they became intoxicated.

162
00:17:10,000 --> 00:17:14,000
It's interesting because this is what you start to see in meditation.

163
00:17:14,000 --> 00:17:18,000
That reality is quite a bit harsher than we think it is.

164
00:17:18,000 --> 00:17:25,000
You have two options. You can spend your time intoxicating yourself either with external substances

165
00:17:25,000 --> 00:17:27,000
or just with the chemicals in your brain.

166
00:17:27,000 --> 00:17:29,000
Find ways to find pleasure again and again.

167
00:17:29,000 --> 00:17:31,000
It's intoxicating.

168
00:17:31,000 --> 00:17:33,000
It blinds you to the reality.

169
00:17:33,000 --> 00:17:42,000
You feel pain, you feel stress, go off and take your drug, shoot up in the brain.

170
00:17:42,000 --> 00:17:48,000
And it's repressed for a while. Of course, you get to worse the next time it comes

171
00:17:48,000 --> 00:17:51,000
and worse and worse until you become a drug addict.

172
00:17:51,000 --> 00:18:00,000
Whether it's external drugs or brain drugs, it's all the same.

173
00:18:00,000 --> 00:18:06,000
And then eventually it's compounded by the fact that you do get old and you do get sick.

174
00:18:06,000 --> 00:18:12,000
And then you're totally unprepared for death and you wind up a real mess.

175
00:18:12,000 --> 00:18:15,000
Or report as a dog or a pig or something.

176
00:18:15,000 --> 00:18:24,000
Really who knows when your mind is not clear.

177
00:18:24,000 --> 00:18:28,000
The third intoxication is intoxication.

178
00:18:28,000 --> 00:18:31,000
The intoxicated by life, intoxication with life.

179
00:18:31,000 --> 00:18:36,000
I'm alive, carpy deums, he's the day, the stupidest thing in the world.

180
00:18:36,000 --> 00:18:40,000
This precious thing called life, right? This precious life.

181
00:18:40,000 --> 00:18:45,000
And then you say, yeah, so let's do whatever we can to mess it up

182
00:18:45,000 --> 00:18:50,000
because we're going to die tomorrow, right? We could die anytime.

183
00:18:50,000 --> 00:18:53,000
Well, it makes sense if there's nothing after.

184
00:18:53,000 --> 00:18:59,000
If there's nothing after life, then no, it doesn't even make sense then.

185
00:18:59,000 --> 00:19:05,000
This is what I've talked about, is that even if we were to die and never be reborn again,

186
00:19:05,000 --> 00:19:10,000
it would make no sense to intoxicate yourself further.

187
00:19:10,000 --> 00:19:11,000
That's what we don't get.

188
00:19:11,000 --> 00:19:13,000
That's what you start to see in meditation.

189
00:19:13,000 --> 00:19:15,000
Meditation seems so stressful.

190
00:19:15,000 --> 00:19:18,000
Wouldn't it be nicer if we danced?

191
00:19:18,000 --> 00:19:26,000
When I was in Florida, someone was talking about laughter yoga or something, laughing yoga.

192
00:19:26,000 --> 00:19:34,000
I don't know, as a Buddhist, you sometimes have to bite your tongue with all these spiritual practices.

193
00:19:34,000 --> 00:19:43,000
Arguably, some of them are positive, but I think they get a little bit indulgent.

194
00:19:43,000 --> 00:19:51,000
When it'd be better, if I taught you all how to laugh and solve your problem by laughing, sounds more fun.

195
00:19:51,000 --> 00:19:57,000
But that's what meditation shows, you know, meditation, insight meditation shows you something different.

196
00:19:57,000 --> 00:20:08,000
It shows you the nature of reality, which shows you your experience.

197
00:20:08,000 --> 00:20:18,000
Oops.

198
00:20:18,000 --> 00:20:25,000
It teaches you to overcome your intoxication.

199
00:20:25,000 --> 00:20:41,000
So recently, the meditation shows you that.

200
00:20:41,000 --> 00:20:48,000
It's not actually happiness.

201
00:20:48,000 --> 00:20:54,000
When you're meditating, you can see all of your impurities we'd call them.

202
00:20:54,000 --> 00:21:01,000
But let's think of them as our attachments, our desires.

203
00:21:01,000 --> 00:21:06,000
And you're able to reflect objectively on it.

204
00:21:06,000 --> 00:21:10,000
And as you experience the other experience, the things that you want and the wanting of it,

205
00:21:10,000 --> 00:21:12,000
you can feel the stress.

206
00:21:12,000 --> 00:21:15,000
You can see how it's actually stressful.

207
00:21:15,000 --> 00:21:21,000
Like a dog, a hungry dog who sees a bone smeared with blood and things.

208
00:21:21,000 --> 00:21:29,000
Oh, I'll sit sheet my hunger with that bone, gnawing on the bone,

209
00:21:29,000 --> 00:21:33,000
and can gnaw in the bone forever and will never be satisfied.

210
00:21:33,000 --> 00:21:38,000
We'll always have this gnawing, stressful feeling of hunger.

211
00:21:38,000 --> 00:21:45,000
That's what you start to see in the meditation.

212
00:21:45,000 --> 00:21:54,000
And then, of course, the fact that there is life after death is compounds that because

213
00:21:54,000 --> 00:21:57,000
people who die when they're on...

214
00:21:57,000 --> 00:22:03,000
There's a story recently of someone who fell off a roof.

215
00:22:03,000 --> 00:22:12,000
They were on acid and LSD and fell off a roof.

216
00:22:12,000 --> 00:22:21,000
So, would you imagine what that would be like to die when you're on drugs?

217
00:22:21,000 --> 00:22:24,000
Death can come at any time.

218
00:22:24,000 --> 00:22:33,000
It's called genya marinang suui.

219
00:22:33,000 --> 00:22:34,000
So, that's the verse.

220
00:22:34,000 --> 00:22:36,000
That brings us to the verse.

221
00:22:36,000 --> 00:22:38,000
Verse has some interesting aspects.

222
00:22:38,000 --> 00:22:40,000
So, why laughter, why exaltation?

223
00:22:40,000 --> 00:22:41,000
This is this.

224
00:22:41,000 --> 00:22:43,000
There's a better translation.

225
00:22:43,000 --> 00:22:46,000
Here's the fun translation from the jotica.

226
00:22:46,000 --> 00:22:48,000
You cannot remember this.

227
00:22:48,000 --> 00:22:51,000
Is it rhymes?

228
00:22:51,000 --> 00:22:55,000
No place for laughter here, no room for joy.

229
00:22:55,000 --> 00:23:00,000
The flames of passion suffering worlds destroy.

230
00:23:00,000 --> 00:23:04,000
The flames of passion suffering worlds destroy.

231
00:23:04,000 --> 00:23:16,000
Why overwhelmed and why overwhelmed and darkest night I pray seeking no torch to light you on your way?

232
00:23:16,000 --> 00:23:22,000
It's a good question for all of us, not just these toxicated women,

233
00:23:22,000 --> 00:23:29,000
all of us living in the world, chasing after useless things,

234
00:23:29,000 --> 00:23:34,000
distracting ourselves until tragedy strikes.

235
00:23:34,000 --> 00:23:38,000
And then, it's too late we're unable to deal with it.

236
00:23:38,000 --> 00:23:43,000
We're untrained and prepared for it.

237
00:23:43,000 --> 00:23:51,000
Meanwhile, we live our lives content with mediocrity, with stressful lives,

238
00:23:51,000 --> 00:23:59,000
with interspersed with bits of happiness.

239
00:23:59,000 --> 00:24:01,000
We don't see the flame.

240
00:24:01,000 --> 00:24:03,000
We see only the light.

241
00:24:03,000 --> 00:24:05,000
Oh, pretty light.

242
00:24:05,000 --> 00:24:08,000
We don't see the burning.

243
00:24:08,000 --> 00:24:11,000
We want to give a whole suit that we'll discuss on burning.

244
00:24:11,000 --> 00:24:17,000
We're going to adhere, but we have the flames of defilement,

245
00:24:17,000 --> 00:24:26,000
the flames of suffering, the flames of old age sickness and death.

246
00:24:26,000 --> 00:24:30,000
There are consequences, and there are problematic.

247
00:24:30,000 --> 00:24:33,000
There's problematic.

248
00:24:33,000 --> 00:24:38,000
There's something problematic about craving and clinging and desire.

249
00:24:38,000 --> 00:24:40,000
That's what we start to learn in meditation.

250
00:24:40,000 --> 00:24:48,000
So you should be able to see as you stay here to continue to practice.

251
00:24:48,000 --> 00:24:50,000
We're in darkness, means we're in delusion.

252
00:24:50,000 --> 00:24:53,000
This is what intoxication does to you.

253
00:24:53,000 --> 00:24:55,000
This is what our desires and our attachments do.

254
00:24:55,000 --> 00:24:58,000
They blind us to which we're going on.

255
00:24:58,000 --> 00:25:03,000
They blind us to the present moment in the here and now.

256
00:25:03,000 --> 00:25:05,000
So mindfulness is this torch.

257
00:25:05,000 --> 00:25:08,000
The Buddha's teaching, but more specifically mindfulness.

258
00:25:08,000 --> 00:25:18,000
Mindfulness is this light that you shine in and it clears up all the darkness immediately.

259
00:25:18,000 --> 00:25:21,000
There's a wonderful thing about mindfulness practice.

260
00:25:21,000 --> 00:25:23,000
You don't have to actually cultivate it.

261
00:25:23,000 --> 00:25:25,000
You can start right now.

262
00:25:25,000 --> 00:25:33,000
You can apply it right now at any moment when you wake up and you say,

263
00:25:33,000 --> 00:25:39,000
what am I going to do? Be mindful at that moment.

264
00:25:39,000 --> 00:25:41,000
This is what you're cultivating.

265
00:25:41,000 --> 00:25:43,000
This is what we're working on here.

266
00:25:43,000 --> 00:25:54,000
There's this ability to manifest the presence of mind that keeps us here and now.

267
00:25:54,000 --> 00:26:01,000
It keeps us aware, alert, awake.

268
00:26:01,000 --> 00:26:08,000
It keeps us sober.

269
00:26:08,000 --> 00:26:09,000
So there you go.

270
00:26:09,000 --> 00:26:12,000
There's our Dhammapanda first for this evening.

271
00:26:12,000 --> 00:26:17,000
An intoxication.

272
00:26:17,000 --> 00:26:19,000
Thank you all for tuning in.

273
00:26:19,000 --> 00:26:26,000
I'll see you on the next one.

