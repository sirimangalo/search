1
00:00:00,000 --> 00:00:26,360
Okay, good morning everyone, broadcasting lives from Mayongsong, from the mountain, come

2
00:00:26,360 --> 00:00:49,440
near to undertake more intensive practice in the forest, something that I think is not

3
00:00:49,440 --> 00:01:09,960
well understood about Buddhist meditation, is that it's designed to change a person in a very

4
00:01:09,960 --> 00:01:30,160
fundamental way, it's not meant to merely return you to an ordinary state where you can go

5
00:01:30,160 --> 00:01:39,480
back and live your life as you would like, in many ways, will change the way you look at

6
00:01:39,480 --> 00:01:55,480
the world, in other words it's not designed to make you an ordinary person, it's designed

7
00:01:55,480 --> 00:02:16,080
to make you a somewhat special person, and so it can be quite shocking to realize the sort

8
00:02:16,080 --> 00:02:30,920
of the depths and the difficulty of the practice, difficult not only in the resistance to the

9
00:02:30,920 --> 00:02:52,080
practice but difficult in the resistance to change, a resistance to accept the magnitude

10
00:02:52,080 --> 00:03:18,720
of our misunderstandings about the world, magnitude of the depth of our attachments, that

11
00:03:18,720 --> 00:03:32,600
deep we have to go to actually become free from suffering, but on the other hand it's encouraging

12
00:03:32,600 --> 00:03:44,960
to realize how I'll profound the practice really is, and how powerful it is, it would

13
00:03:44,960 --> 00:03:54,600
have taught that it makes you, he said that the practice makes you a great being, you have

14
00:03:54,600 --> 00:04:14,040
this word Mahantata, in Sanskrit it would be Mahatma, Mahatma in this word in India that

15
00:04:14,040 --> 00:04:27,520
was used to describe Gandhi, but it was used to describe any great being, so if you think

16
00:04:27,520 --> 00:04:44,000
of it in those terms, there's very few people who don't admire the image of God's

17
00:04:44,000 --> 00:04:55,120
Gandhi or what, the ideal that he was supposed to represent, the greatness of mind, of

18
00:04:55,120 --> 00:05:10,280
being Mahatma.

19
00:05:10,280 --> 00:05:14,400
I think many people come, most people don't come to meditation thinking they're going

20
00:05:14,400 --> 00:05:24,120
to become someone great or someone famous or powerful or just want to become, I'd best

21
00:05:24,120 --> 00:05:29,760
want to become good people, but greatness doesn't have anything to do of course with

22
00:05:29,760 --> 00:05:40,600
power or fame or renown, you can be a great person living off in the forest, but it's

23
00:05:40,600 --> 00:05:50,520
important I think to think in this way because we shouldn't settle for mediocrity, meditation

24
00:05:50,520 --> 00:06:05,140
won't allow you to, we should see the benefit of being a great person, a great being

25
00:06:05,140 --> 00:06:15,960
of greatness, we should see the importance of true change and true spiritual enlightenment

26
00:06:15,960 --> 00:06:25,800
as opposed to just meditating to feel good, to avoid our problems or to escape our problems

27
00:06:25,800 --> 00:06:34,520
temporarily, if you want to succeed in meditation, you have to be open to change and true

28
00:06:34,520 --> 00:06:44,440
change, true transformation, so the Buddha described what it takes to become a great person

29
00:06:44,440 --> 00:06:55,400
and these are qualities that as meditators, we will do well to espouse and cultivate,

30
00:06:55,400 --> 00:07:02,640
said anyone who has these qualities is sure to become a great person, we'll hunt that

31
00:07:02,640 --> 00:07:25,520
as a great person, we'll become a great being a profound being, the first one is Anoka

32
00:07:25,520 --> 00:07:44,000
One has to be full of wisdom, and number two yoga bahu no, only full of yoga.

33
00:07:44,000 --> 00:07:54,600
Yoga here means energy or effort, and number three, weigh the bahu no.

34
00:07:54,600 --> 00:08:09,320
That means feeling will be full of feeling of zero or zest or interest.

35
00:08:09,320 --> 00:08:18,600
Asan tutti bahu no, will be full of discontent.

36
00:08:18,600 --> 00:08:31,760
Kita dulones, and not give up one's effort or duties, not give up the work or the task.

37
00:08:31,760 --> 00:08:36,600
Then we put it down.

38
00:08:36,600 --> 00:08:45,440
Then Kuzaleh su-dhammeh su-tari-jah-pah-daliti, to always strive to attain greater and greater

39
00:08:45,440 --> 00:08:52,440
whole su-das.

40
00:08:52,440 --> 00:09:04,640
These six things are very dumbest that we want to become a great being.

41
00:09:04,640 --> 00:09:13,240
So briefly, a lokabahu no means wisdom in the meditation practice we have to always be

42
00:09:13,240 --> 00:09:26,760
clear in our minds, to use the wisdom that we gain to continue, to be wise and clever, to

43
00:09:26,760 --> 00:09:39,200
be clear that the things that you experience are impermanent and unsatisfying uncontrollable,

44
00:09:39,200 --> 00:09:44,360
because this is what you're going to see when you practice.

45
00:09:44,360 --> 00:09:51,520
And if you can't recognize that, it becomes a source of stress and suffering and difficulty

46
00:09:51,520 --> 00:09:53,000
in the practice.

47
00:09:53,000 --> 00:10:00,840
When pain arises, the inability to see it as impermanent and unsatisfying, in the sense

48
00:10:00,840 --> 00:10:14,080
that you can't fix it, you can't make it better, it doesn't belong to you.

49
00:10:14,080 --> 00:10:19,080
You can't see this then it leads you to suffer and doubt about the practice and doubt

50
00:10:19,080 --> 00:10:21,080
about your ability to practice.

51
00:10:21,080 --> 00:10:28,960
The same goes with pleasant things if you experience pleasant sensations, pleasant experiences

52
00:10:28,960 --> 00:10:42,760
and if you can't see them as transient, as unsatisfying, because they're transient,

53
00:10:42,760 --> 00:10:50,280
because when they end and they're gone and all you're left with is your desire, the

54
00:10:50,280 --> 00:10:59,160
desire builds but the pleasure doesn't, the pleasure fades, you can't see this then you become

55
00:10:59,160 --> 00:11:10,400
discontent, more and more, wanting for pleasant sensations, pleasant experiences, less

56
00:11:10,400 --> 00:11:23,040
able to maintain and sustain them to a level that satisfies you.

57
00:11:23,040 --> 00:11:28,880
Aloka Maholo, you have to cultivate as much light, aloka means light, but it is light of wisdom,

58
00:11:28,880 --> 00:11:36,320
your mind has to be bright and clear, to cultivate this clarity of mind.

59
00:11:36,320 --> 00:11:44,240
The beginning when we practice our minds are very clouded and murky, we don't see so clearly.

60
00:11:44,240 --> 00:11:50,760
It's hard to tell what's right, what's wrong, it's hard to tell what is the point of the

61
00:11:50,760 --> 00:11:59,480
practice, but as you practice your mind becomes clearer, as your mind clears, you're able

62
00:11:59,480 --> 00:12:11,840
to see the difference, you're able to see the benefits of clarity and you're able to

63
00:12:11,840 --> 00:12:23,200
see the dangers in attachment and clinging and aversion, you see the causes and effect,

64
00:12:23,200 --> 00:12:38,040
that leads to what, you start to see how your mind works, and you can adjust accordingly.

65
00:12:38,040 --> 00:12:47,280
Yoga Maholo, you have to work hard, meditation doesn't meant to be easy, it's not something

66
00:12:47,280 --> 00:12:57,080
you can do when you feel like it, you should try to be persistent in the practice, you

67
00:12:57,080 --> 00:13:04,120
can do in a sense, have to push yourself to practice, otherwise laziness takes over

68
00:13:04,120 --> 00:13:13,120
and your mind falls back into all the comfortable patterns, which is much more comfortable

69
00:13:13,120 --> 00:13:35,200
and pleasant, but it doesn't stir up, it doesn't dig deep and uproot or bad habits.

70
00:13:35,200 --> 00:13:46,440
You have to sit really in a dukama jati, you overcome suffering through effort, to work at it,

71
00:13:46,440 --> 00:13:53,200
to work at it and you have to overcome your aversion to work, it makes us cranky when

72
00:13:53,200 --> 00:14:04,720
we have to work, it's unpleasant, you have to overcome that, you have to become strong,

73
00:14:04,720 --> 00:14:10,760
and natural, perfect state is actually quite energetic, but our ordinary state is usually

74
00:14:10,760 --> 00:14:21,560
much more lazy, and so it feels somehow stressful to have to work, because we'd much rather

75
00:14:21,560 --> 00:14:37,640
just lay us around and enjoy ourselves, but it's a trap you become more and more obsessed

76
00:14:37,640 --> 00:14:52,080
with the pleasantness of lying down and sitting in comfortable chairs and seats, a great

77
00:14:52,080 --> 00:15:02,960
person is able to work at an optimum level, it's very little rest, very little, very

78
00:15:02,960 --> 00:15:16,200
little comfort, here's the point is to be objective, if you're truly objective then

79
00:15:16,200 --> 00:15:27,280
you don't mind to be a state of comfort or discomfort, just embody you either way, but

80
00:15:27,280 --> 00:15:33,840
our ordinary state is only content with comfort and pleasure, and when we don't have it

81
00:15:33,840 --> 00:15:41,760
then we get upset and angry and frustrated, so clearly we're not objective, to be truly

82
00:15:41,760 --> 00:15:48,960
objective you have to actually work hard, work hard to change that, to bring your mind

83
00:15:48,960 --> 00:15:58,040
to a truly neutral ground, when you're able to be happy no matter what the situation,

84
00:15:58,040 --> 00:16:06,760
whether you have comfort or discomfort, wait about to work hard to overcome unwholesomeness

85
00:16:06,760 --> 00:16:15,140
in the mind, you have to work hard to cultivate wholesomeness, working yoga baholos,

86
00:16:15,140 --> 00:16:24,440
or yoga baholos, wait a baholomines, you have to wait a means feeling, it means putting

87
00:16:24,440 --> 00:16:38,400
your heart into something, you have to practice with your whole hardness, you can't just

88
00:16:38,400 --> 00:16:46,360
force yourself to practice, you can't just be here because someone told you that you

89
00:16:46,360 --> 00:16:54,960
should or because you think it's the right thing to do, if you don't really want to

90
00:16:54,960 --> 00:17:03,240
do it, it's very difficult to meditate, to become great, you have to be passionate about

91
00:17:03,240 --> 00:17:14,420
what you do, to some extent passionate about meditation, it just means you have to be clear

92
00:17:14,420 --> 00:17:21,800
in your mind about the benefits of meditation, to be very careful because the mind will

93
00:17:21,800 --> 00:17:35,720
trick you into thinking that you don't need to meditate or that it's too much work or that

94
00:17:35,720 --> 00:17:44,000
it's too stressful or so, and trick you into thinking that meditation is not the right

95
00:17:44,000 --> 00:17:58,760
way to go, you have to be clear in your mind the benefits, meditation helps to clear the

96
00:17:58,760 --> 00:18:13,880
mind that meditation helps you to root out all mental, upset mental illness or mental disturbances,

97
00:18:13,880 --> 00:18:20,280
it helps you overcome suffering, meditation clears up, the attachments that lead us to suffering

98
00:18:20,280 --> 00:18:32,360
that causes us to fall into stress and suffering, meditation leads you to the right path,

99
00:18:32,360 --> 00:18:41,840
allows you to live your life however you like without worry or stress or fear or anxiety

100
00:18:41,840 --> 00:18:50,760
without doubt gives you a clear mind and meditation leads you to freedom, we have to put

101
00:18:50,760 --> 00:18:55,720
our hearts into it, you can't do this half-hearted, you have to be very careful to

102
00:18:55,720 --> 00:19:14,420
uproot doubt and uncertainty and bevelance, the uncaring mind that is uncommitted to

103
00:19:14,420 --> 00:19:22,520
be committed to the practice, be in a battle, as unto the Baha'u'llow means you have to be

104
00:19:22,520 --> 00:19:28,360
full of discontent, so discontent here, there's two kinds of contentment, we have to

105
00:19:28,360 --> 00:19:35,680
of course cultivate contentment with our experiences, but it's a discontent, this means

106
00:19:35,680 --> 00:19:43,840
in regards to goodness, it should never be content with what you have with who you are,

107
00:19:43,840 --> 00:19:52,080
we content with, we content to keep the unwholesomeness of the existence of the mind saying

108
00:19:52,080 --> 00:19:59,200
oh it's not so bad, or the goodness saying oh it's good enough, if you want to become

109
00:19:59,200 --> 00:20:05,840
a great person and really succeed in the meditation you have to be willing to give up all

110
00:20:05,840 --> 00:20:16,320
unwholesomeness, be ready to cultivate perfect wholesomeness, the great thing is that it is

111
00:20:16,320 --> 00:20:25,000
possible, you can become a great person, it's there for us to do, it's there for us to

112
00:20:25,000 --> 00:20:39,160
attain, so characteristic of a great person is that they don't rest on their laurels,

113
00:20:39,160 --> 00:20:58,560
they continue to, a great person is recognized by their ability to see the smallest faults,

114
00:20:58,560 --> 00:21:06,000
an ordinary person ignores their great faults, and dismisses them as being inconsequential,

115
00:21:06,000 --> 00:21:16,560
as a great person is even disturbed by their small fault, the smallest fault, is a difference

116
00:21:16,560 --> 00:21:27,160
between an ordinary person and a great person, Anikita duro means not to ever put down

117
00:21:27,160 --> 00:21:35,920
the task, so to not take a break, in meditation there really is no need to take a break,

118
00:21:35,920 --> 00:21:42,320
there's no benefit in taking a break, because it doesn't mean that you have to be doing

119
00:21:42,320 --> 00:21:48,200
formal meditation, but it means there's no reason to give up mindfulness, because even

120
00:21:48,200 --> 00:21:55,960
when you're not meditating, no matter what you're doing you can always work at cultivating

121
00:21:55,960 --> 00:22:04,840
clarity of mind, when you're eating you can work to have a clear mind, you're taking

122
00:22:04,840 --> 00:22:12,440
a shower or brushing your teeth, when you're on the toilet you can be working to clear

123
00:22:12,440 --> 00:22:19,320
your mind, even when you lie down to sleep at night, you can be working to cultivate clarity

124
00:22:19,320 --> 00:22:25,040
of mind, there's no excuse, there's no reason, there's no benefit to stopping there.

125
00:22:25,040 --> 00:22:31,360
So at another, it's another quality of a great person that they don't, they don't give

126
00:22:31,360 --> 00:22:41,120
up, and they don't take breaks, they're constantly working to improve themselves.

127
00:22:41,120 --> 00:22:48,240
And the final one, Qousalaisu damesu dari chapat vaisi means not only are they working,

128
00:22:48,240 --> 00:22:55,800
it's not a linear progress, it's just one important point to understand about meditation

129
00:22:55,800 --> 00:23:07,440
that it's not something that you can just put your head down and plow on, you have to

130
00:23:07,440 --> 00:23:21,320
be adaptive, to be able to adapt, adaptable, to be ready to adapt, to new situations,

131
00:23:21,320 --> 00:23:31,920
utari chapat vaisi means working to further yourself, to greater goodness, always looking

132
00:23:31,920 --> 00:23:39,720
for new ways to improve yourself, and in meditation this is a special, take special importance

133
00:23:39,720 --> 00:23:48,200
because your conditions change, it's common for a meditator to become complacent thinking

134
00:23:48,200 --> 00:23:54,360
they've figured it out, they've come to understand how to meditate and so they work

135
00:23:54,360 --> 00:24:01,480
out a problem in the mind and then they try to apply the same technique to a new

136
00:24:01,480 --> 00:24:18,560
problem and find that the results are different, or become complacent, you find that meditation

137
00:24:18,560 --> 00:24:23,440
is like layers of an onion every, every time you figure something out you learn something

138
00:24:23,440 --> 00:24:29,720
new about yourself, there's something new and a little bit different to learn, there's

139
00:24:29,720 --> 00:24:46,320
always new and greater understanding to begin, and so we have to approach every experience,

140
00:24:46,320 --> 00:24:56,200
every meditation state with an open mind, with a beginner's mind they've seen, ready

141
00:24:56,200 --> 00:25:04,680
to learn something new, this quality of mind is important in meditation, the ability to adapt

142
00:25:04,680 --> 00:25:10,680
to a new situation, to not take it for granted, because it's very easy for a meditator

143
00:25:10,680 --> 00:25:17,040
to get into a rut, where they are meditating, but aren't progressing, because they're

144
00:25:17,040 --> 00:25:27,600
ignoring new stimuli, new information, they'll be ignoring, they'll be very confident

145
00:25:27,600 --> 00:25:33,080
in dealing with certain situations, certain emotions, but other emotions they've ignored

146
00:25:33,080 --> 00:25:40,200
and they've never really learned how to deal with, so it just takes, it'll be among

147
00:25:40,200 --> 00:25:50,200
the ability to discern and discriminate, not just work hard, but to be able to see, to

148
00:25:50,200 --> 00:26:04,200
adjust your work, according to the task at hand, working to reach a new level of understanding,

149
00:26:04,200 --> 00:26:12,120
it's not just effort, but you need wisdom and discernment, so some good demos, I think

150
00:26:12,120 --> 00:26:20,400
these are useful for all meditators, whether we're here in the forest, practicing intensive

151
00:26:20,400 --> 00:26:29,560
video, for people in the world to think about, and to cultivate, because in the end

152
00:26:29,560 --> 00:26:43,960
that's the goal, that's the way we're headed to greatness, something that is well worth

153
00:26:43,960 --> 00:26:55,560
appreciation, greatly appreciated to see so many people practice, to become better people,

154
00:26:55,560 --> 00:27:01,840
so this is, these are some of the demos that help us do it, help us become not only good

155
00:27:01,840 --> 00:27:27,080
people, but great, deep profoundly.

