1
00:00:00,000 --> 00:00:07,000
I don't think it's your fault. I think it's just me.

2
00:00:07,000 --> 00:00:13,000
I'm trying to say that I have issues with getting pervading thoughts to leave my mind.

3
00:00:13,000 --> 00:00:19,000
I have tried mantras and a lesson from the thought of it. It generally doesn't help.

4
00:00:19,000 --> 00:00:24,000
Right, because that's not what it's supposed to do. The thought isn't the problem.

5
00:00:24,000 --> 00:00:28,000
Your reaction to the thought is the problem. There's no problem with the thought

6
00:00:28,000 --> 00:00:34,000
staying, going, turning around in circles.

7
00:00:34,000 --> 00:00:38,000
That's inconsequential. What is consequential is how you react to it.

8
00:00:38,000 --> 00:00:43,000
The fact that you don't like the thought. The fact that you want the thought to go away, those are the problem.

9
00:00:43,000 --> 00:00:54,000
Thought is just reality. Once you see thought for what it is, you reject it. You have no use for it.

10
00:00:54,000 --> 00:01:03,000
The fact that you want the thought to go away is the problem. That's what's keeping it from actually going away

11
00:01:03,000 --> 00:01:11,000
as you have some tension there, which creates all sorts of neural pathways,

12
00:01:11,000 --> 00:01:32,000
strengthens neural pathways, and actually brings the thought back again.

