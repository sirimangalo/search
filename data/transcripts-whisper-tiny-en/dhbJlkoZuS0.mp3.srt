1
00:00:00,000 --> 00:00:08,000
Do you have any advice for making a gradual transition from a simple little life to the monastic life?

2
00:00:08,000 --> 00:00:14,000
Are the rules very strict? Is there a leeway with regards to a month's behaviors?

3
00:00:14,000 --> 00:00:16,000
It is singing dancing.

4
00:00:16,000 --> 00:00:19,000
You want to be a singing monk?

5
00:00:19,000 --> 00:00:20,000
No.

6
00:00:20,000 --> 00:00:22,000
Dancing monk?

7
00:00:22,000 --> 00:00:24,000
No, there's no leeway.

8
00:00:24,000 --> 00:00:28,000
If you can't give up singing and dancing, don't become a monk.

9
00:00:28,000 --> 00:00:32,000
A person should become a monk because they know that they have defilements,

10
00:00:32,000 --> 00:00:34,000
but they want to be free from them.

11
00:00:34,000 --> 00:00:40,000
If you don't want to be free from your defilements, don't become a monk.

12
00:00:40,000 --> 00:00:45,000
If you want to sing in dance, don't become a monk.

13
00:00:45,000 --> 00:00:47,000
Bad idea.

14
00:00:47,000 --> 00:00:51,000
But that's the second question.

15
00:00:51,000 --> 00:00:54,000
The third question.

16
00:00:54,000 --> 00:00:58,000
The second question, the rules very strict.

17
00:00:58,000 --> 00:00:59,000
The first question.

18
00:00:59,000 --> 00:01:04,000
How to make a gradual transition from a life to monastic life?

19
00:01:04,000 --> 00:01:05,000
Stop dancing.

20
00:01:05,000 --> 00:01:06,000
Stop singing.

21
00:01:06,000 --> 00:01:09,000
That's your first stop listening to music.

22
00:01:09,000 --> 00:01:11,000
Start keeping the eight precepts.

23
00:01:11,000 --> 00:01:12,000
It's really nice actually.

24
00:01:12,000 --> 00:01:13,000
There's like stepping stones.

25
00:01:13,000 --> 00:01:16,000
Start with a five precepts.

26
00:01:16,000 --> 00:01:19,000
Once you got them covered, go to the eight.

27
00:01:19,000 --> 00:01:21,000
Once you got them covered, go to the ten.

28
00:01:21,000 --> 00:01:27,000
Once you got them covered, go to the 227 plus plus plus plus plus.

29
00:01:27,000 --> 00:01:30,000
There's, I did a dhamapada verse on this, didn't they?

30
00:01:30,000 --> 00:01:32,000
Didn't they already do that one?

31
00:01:32,000 --> 00:01:36,000
This lay disciple who came in to the monastery.

32
00:01:36,000 --> 00:01:38,000
And they asked, which should I do?

33
00:01:38,000 --> 00:01:39,000
Where can I start?

34
00:01:39,000 --> 00:01:40,000
They said, keep five precepts.

35
00:01:40,000 --> 00:01:41,000
Okay, so he did that.

36
00:01:41,000 --> 00:01:42,000
Came back later.

37
00:01:42,000 --> 00:01:43,000
What should I do next?

38
00:01:43,000 --> 00:01:45,000
Come keep the eight precepts.

39
00:01:45,000 --> 00:01:46,000
Okay, he did that.

40
00:01:46,000 --> 00:01:48,000
What should I do next?

41
00:01:48,000 --> 00:01:52,000
So for ten precepts, and then they eventually he ordained some monks.

42
00:01:52,000 --> 00:01:53,000
So he did these things in order.

43
00:01:53,000 --> 00:01:56,000
And he became known as Anupuba.

44
00:01:56,000 --> 00:01:57,000
Anupuba.

45
00:01:57,000 --> 00:01:59,000
Anupuba guy or something like that.

46
00:01:59,000 --> 00:02:04,000
The one who practices in order and progression.

47
00:02:04,000 --> 00:02:06,000
So that's a good next step.

48
00:02:06,000 --> 00:02:07,000
Give up the singing dance.

49
00:02:07,000 --> 00:02:09,000
You can go for eight precepts.

50
00:02:09,000 --> 00:02:16,000
Once you can do that, then go on to becoming a novice monk.

51
00:02:16,000 --> 00:02:19,000
Once you become a novice monk, see if you can do that.

52
00:02:19,000 --> 00:02:22,000
And if you can do that, come on.

53
00:02:22,000 --> 00:02:25,000
Becoming a full monk.

54
00:02:25,000 --> 00:02:30,000
So if you're still dancing and singing, I'd say your lay life is not simple enough yet.

55
00:02:30,000 --> 00:02:35,000
You could make the jump to monastic life, but you'd have to be sure that you can give up

56
00:02:35,000 --> 00:02:38,000
dancing and singing and listening to music and so on.

57
00:02:38,000 --> 00:02:45,000
If you can't, give them up first and prepare yourself to become a monk.

