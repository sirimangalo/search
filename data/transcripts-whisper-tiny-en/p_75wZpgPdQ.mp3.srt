1
00:00:00,000 --> 00:00:08,000
Question, to extend the question about sleeping, I get tired when meditating, is it better to sleep in when I'm not sleepy than meditating?

2
00:00:08,000 --> 00:00:19,000
Well, sleepiness is actually a great object of meditation. It's something that is overlooked, I think, by many meditators.

3
00:00:19,000 --> 00:00:25,000
Yeah, sometimes you have to just fall asleep, but that kind of comes, the best thing is for that to come naturally.

4
00:00:25,000 --> 00:00:36,000
If you fall asleep, you fall asleep. When you're sleeping, first before you go to sleep, try to focus on the sleepiness, focus on the tired feeling.

5
00:00:36,000 --> 00:00:50,000
Because it's difficult in daily life or when you're not in a meditations center, because you really are exhausted a lot of the time from work and just thinking a lot and so much activity.

6
00:00:50,000 --> 00:01:02,000
But you can try it, focus on the tiredness, and you'll find that sometimes it gives you energy. Sometimes you're able to burst the bubble, so to speak, and it just disappears. Suddenly you're not tired anymore.

7
00:01:02,000 --> 00:01:08,000
And you'll find you have suddenly a lot of energy, and you're able to do a really good meditation center.

8
00:01:08,000 --> 00:01:25,000
Drowsiness is a hindrance, it's something that drags you down and stops you from meditating. If you're able to pierce it and overcome it, you find that suddenly your mind is clear and your practice goes a lot better, and meditation can be quite fruitful after that point.

9
00:01:25,000 --> 00:01:44,000
So, we should be careful not to forget about the state of mind that is fatigue or drowsiness, and forget that it's a part of the meditation. When it comes up, it's a valid objective of observation.

10
00:01:44,000 --> 00:01:58,000
It's a good one too. There's two ways. One way is you'll get a lot of energy by focusing on, by acknowledging, for example, tired and tired, and the others you'll just fall asleep.

11
00:01:58,000 --> 00:02:03,000
If you're really good, it'll be one or the other, and neither is a problem.

12
00:02:03,000 --> 00:02:10,000
The problem with sleeping first and then meditating is that sleep drains your awareness and your mindfulness.

13
00:02:10,000 --> 00:02:18,000
We try to have meditators sleep less and less, and sometimes they don't even sleep at all, they'll just do meditation.

14
00:02:18,000 --> 00:02:26,000
Because sleep is an uncontrolled state, it's actually just a construct. Sleep doesn't have any ultimate reality.

15
00:02:26,000 --> 00:02:38,000
We think of sleep as being a necessary part of reality, but it's only a conditioned state that we've developed as human beings, or as I suppose most animals have developed.

16
00:02:38,000 --> 00:02:47,000
But it's not a part of ultimate reality. It's nothing intrinsic in the mind.

17
00:02:47,000 --> 00:03:03,000
It's just an aspect of this part of this period of our existence that we have a very coarse body, and it falls asleep sometimes.

18
00:03:03,000 --> 00:03:13,000
During the time that we're asleep, the mind is in a specific state that's not very wholesome.

19
00:03:13,000 --> 00:03:21,000
It's very free to do what it wants and to make assumptions and conclusions.

20
00:03:21,000 --> 00:03:27,000
People will commit dastardly deeds in their dreams. It can happen.

21
00:03:27,000 --> 00:03:38,000
You can do all sorts of bad things, you can do good things, but sometimes you fight and kill and so on, depending on the state of mind.

22
00:03:38,000 --> 00:03:45,000
So it's possible actually to develop. It usually develops on wholesomeness when you sleep. Most people develop on wholesomeness.

23
00:03:45,000 --> 00:03:51,000
So when they wake up, the mind is not fresh, the mind is not clear, it's clouded and diluted.

24
00:03:51,000 --> 00:03:57,000
You can feel that. If you're in a meditation center, you can feel that after you sleep, you don't feel better.

25
00:03:57,000 --> 00:04:01,000
You feel actually, let's sharpen the less clear.

26
00:04:01,000 --> 00:04:08,000
So you have to sleep as human beings we do have to sleep.

27
00:04:08,000 --> 00:04:12,000
But that's the important point here, to try to sleep mindfully.

28
00:04:12,000 --> 00:04:18,000
Before you sleep, do meditation, whether it's lying meditation, saying to yourself, rising, falling,

29
00:04:18,000 --> 00:04:22,000
or whether it's some sitting meditation before you lie down.

30
00:04:22,000 --> 00:04:27,000
That'll help your sleep to be a lot more quiet and a lot clearer.

31
00:04:27,000 --> 00:04:33,000
You find you don't dream so much, your mind is not so distracted when you sleep,

32
00:04:33,000 --> 00:04:35,000
and you wake up a lot more refreshed.

33
00:04:35,000 --> 00:04:50,000
And you can actually find that sleep is much more beneficial, and it's not as much of a hindrance to your meditation.

