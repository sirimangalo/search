1
00:00:00,000 --> 00:00:11,000
Okay, we have five minutes left, so I'm going to ask that our three panel, the three of us say our last words and then call it quits for the year.

2
00:00:11,000 --> 00:00:20,000
So, Bill, can you give us your last words, what you'd like to say, but this evening and what you'd like to wish for the people out there?

3
00:00:20,000 --> 00:00:44,000
This has been great, you know, that we can, we're doing this this year, people that can come on and take part in your, what you're doing, separate from the comment areas that you would just kind of answer as best you could and get to as many as you could.

4
00:00:44,000 --> 00:00:55,000
And I was just glad to be part of it and learned a lot, and I hope everybody has progressed from it.

5
00:00:55,000 --> 00:00:58,000
Thank you very much too.

6
00:00:58,000 --> 00:01:03,000
Thank you for being here. Paul, are you still with us?

7
00:01:03,000 --> 00:01:05,000
I'm still here, Bante.

8
00:01:05,000 --> 00:01:10,000
Can I ask your last word? Okay.

9
00:01:10,000 --> 00:01:20,000
I've really enjoyed coming to these meetings and I wish you lots of like your travels.

10
00:01:20,000 --> 00:01:29,000
And on a more general kind of note, I just wish everyone to be peaceful, happy and well.

11
00:01:29,000 --> 00:01:33,000
Thank you.

12
00:01:33,000 --> 00:01:39,000
Yeah, so thanks everyone. This has been great. It's lots of fun. It's encouraging.

13
00:01:39,000 --> 00:01:50,000
It's a nice way of spreading the dhamma. It really helps.

14
00:01:50,000 --> 00:01:54,000
The sort of thing helps my work doesn't make more work for me.

15
00:01:54,000 --> 00:02:05,000
This kind of thing is what makes the world easier to live in and makes it easier to spread the Buddhist teaching because there's far more this sort of thing.

16
00:02:05,000 --> 00:02:23,000
For me, it's amazing the reach that you can gain and how you can actually feel the potential for making a small change even on a global level that you're actually able to participate in the global conversation

17
00:02:23,000 --> 00:02:31,000
to the extent that it may actually have change and affect the world.

18
00:02:31,000 --> 00:02:38,000
What that means, I don't know, but it's the work that we do and it's certainly a good work.

19
00:02:38,000 --> 00:02:44,000
So I'm going to get cut off here in a second, but I'd like to wish everyone good end of the year and a happy new year.

20
00:02:44,000 --> 00:02:54,000
And that you only put into practice these the teachings of the Buddha don't be content with just learning about them.

21
00:02:54,000 --> 00:03:04,000
Try your best to put them in practice and may everyone through their practice find true peace, happiness and freedom from suffering.

22
00:03:04,000 --> 00:03:06,000
Peace.

23
00:03:06,000 --> 00:03:08,000
Goodbye.

24
00:03:08,000 --> 00:03:15,000
Good bye.

