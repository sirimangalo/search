Hello everyone, welcome to our Wednesday.
I'm in Q&A session, a chance for us to come together as a community, practice, study, teach
the Dhamma. I'm joined today by Chris and Jim Olu, Max.
Coming together as a community as an important part of Buddhist practice, it's an important
part of the things that keep Buddhism alive, prevent Buddhism from fading away and by extension,
it's the sort of thing that works with any organization, of course.
Something we as people who are interested in seeing that our organization doesn't fade
away, something that we should be mindful and conscientious of, conscious of.
The Buddha talked about seven things that prevent the decline, prevent the decline.
He talked about the decline of a society, he talked about the decline of the community
of Buddhist practitioners, the Bikhus.
Bikhus can mean Buddhist monks, it can also mean anyone who has undertaken the practice
of the Buddhist teaching, who is inclined to free themselves from the dangers of samsara.
And so he laid down what he called the Radhaja Parihani, the Dhammas that prevent the
decline of a kingdom.
And then he went and talked to the monks about it, and he said, well, it's one thing to
talk about, the decline of a kingdom, but that's for the decline of our community.
There are seven things that will prevent that, and they call them the Bikhu, the Bikhu
Parihani and Dhammas, Parihani, the declining, fading away of Parihani.
There are seven things, starting with community, Buddha said, we should come together often.
We should meet together.
Staying alone, the problem is you end up falling prey to your own problems.
It's much easier to see the problems of others.
It's also much easier to take others as an example, because of our strengths and weaknesses
are different, because the external that we present to others is different from the internal.
It can help to be surrounded by others who meet with others, who are practicing on the
right path.
We tend to support people, we tend to support each other, and we come together in wholesome
endeavors like this.
So of course, meeting to do bad things would have the opposite effect if we met to go out
to parties or bars or whatever, but meeting together in a proper and positive in a wholesome
way like this, it's a very important part of our practice.
It's a big reason why we have these sessions to provide a community.
The second is harmony, so relating very much to community when we do join together, when
we meet together, we should come together on time, try not to come late, when you have
meetings, we should leave, we should perform our business in harmony, we should try our best
to be mindful and conscientious of each other, thoughtful, helpful, that's something great
about our community, we see a lot of, we see the positive effects of our practice,
sort of manifest in our organization, that way there's a great amount of harmony in
their organization, everyone is helpful and considered of each other.
There are, of course, going to be wrinkles, but we don't have any feuds or fights or
anything like that as far as I know, something to appreciate, the harmony of our many of
these sessions, this song on YouTube, to have a harmonious session on YouTube is a great
thing, it's a great accomplishment.
The third is integrity, integrity relates to the content of our practice, of our, our
communal endeavors, and this is a problem that communities sometimes fall prey to is losing
sight of the point, of the goal, of the focus, so integrity means in our endeavors, in
our engagements, we remember to maintain a focus on what's important, trying to separate
what's important from what's not important, always be conscious of why we're here, when
you come to this session, you should undertake it with, with a level of mindfulness, you
shouldn't be switching times between Facebook and whatever else there is out there, the
best you should close your eyes, take it as an opportunity to meditate in a group, in
a community, with guidance, with support.
We focus on meditation, our questions are very much restricted to what is important, as
best we can discern, we try to focus on those questions that are important, try to provide
answers that are practical and helpful, out of an interest in focusing on what's important,
maintaining integrity so we don't get off track.
The fourth is humility, humility relates to respect, so respect for the teacher, today
I'm in a position of a teacher during this session, it's important that everyone have
respect for that position, try not to step up as a teacher, or be disrespectful and that
sort of thing, be conscientious of the time and so on.
The respectful for each other, a teacher has to be conscientious for their students' needs,
they can't be arrogant or conceited, or overbearing, and to each other, respect for each
other's time, whenever we post something, is disrespectful as this helpful, is this appropriate,
it's this proper of my position, my role in this engagement, be humble, humility is important.
Number five is purity, no set of things that promote the well-being of a Buddhist community
would be complete without reference to purity, as I mentioned last time we met, I think
it was on purity, purity is the essence of the Buddhist teaching, so in all of our endeavors
and all of our activities we should strive for cleansing the mind from greed, anger,
delusion, check ourselves when we have them, try to prevent them through the practice
of mindfulness to the constant maintenance of mindful practice.
Number six is sanctity, sanctity relates to space mostly, sanctity involves respecting
space, it also involves appreciating space, and involves inclining towards positive spaces,
not letting ourselves get caught up in society, fine time to retreat into seclusion,
to create positive space for ourselves and for others.
So we can think of this channel as a sort of a space, our discord server as a kind
of a space, and we respect the sanctity of that, trying to make it as, make these online
spaces as helpful as possible, there's always going to be some challenges in dealing with
online space, but we try our best to make them positive spaces.
And finally hospitality, remembering to respect and welcome visitors, guests, newcomers
to the practice, and those who are already here to respect those who are here in our
space and to provide them with what they need.
So in real world, this means our building, our site here, we have rooms open and we welcome
people and we provide food and necessities, that's another great thing to appreciate
how supportive everyone is and how we're able to continue to offer this space through
the support of our local volunteers and our remote volunteers, our entire board of directors
and volunteers online who support this place, and also support the online community.
We have people welcome, we have people dedicated to welcoming newcomers to our discord
community, so we remember to support each other, not just spiritually, but even materially,
to support each other in terms of providing space and accommodating each other, supporting
each other in our practice, you don't have to teach others to support them, just providing
them with space and support, physically, materially, it's a great, great thing.
So that's it, there's seven things, it's a very good list, I don't know if I've actually
talked about them online recently or ever, but this is something that we can all and should
all appreciate, remember and put into practice, it's not a direct, directly, a teaching
on the practice, our practice is simply the practice of mindfulness, but as, as is true
in many ways, there are things that keep our practice alive, support our practice and these
seven things I think are a big part of that, keeping the community alive which will support
our practice and keep our practice progressing, so thank you everyone for taking part
in the community and maintaining the community volunteering, supporting the community
materially, it's all great stuff.
So that's the demo for tonight, if there are any questions, I'm happy to take them, seem
to have fewer questions lately, so we don't have to stay for the whole hour if there's
not a lot of questions, but I'm ready when you guys are.
Alright, let's begin.
When I'm meditating, sometimes I think about my mom's suffering from her illness and I end
up becoming emotional, I cry, do I simply note the feeling or stop and continue when it's
gone?
Well, you shouldn't ever really stop, but one thing you should notice is that there's
a progression there, so there's thinking and then there's emotion and then there's crying
and so you shouldn't just note the feeling, you should also note the thinking, you should
note the emotion, the sadness, as well as the crying, you should note that as well.
You can just say crying, if you're crying, you can not sad when you're sad and thinking
when you're thinking, try and just note it all, there's no benefit in stopping and conversely
there's great benefit in continuing and noting, you'll find that you're able to come to terms
with it much better, but note everything, not just one thing, not just the result when
it's already too late, try not thinking and sad as well.
How should one approach wanting to be mindful, is this considered a hindrance and should it
be noted?
I noticed it in meditation the other day, everything should be noted, so there's no question
there, it is considered a hindrance, if you want, sometimes you'll be frustrated as well.
It relates to the idea of self that somehow you can control it to be different than what
it is to sort of accelerate the progress, that's not really how it works, you just do
it or don't do it, and every time you remember to do it, just do it, you can't make it
any better, there's no way to speed up the process by wanting or desiring or pushing.
I don't have a private spot to meditate in my mom's apartment, would it be weird to just
go into my bathroom and try meditating?
I mean, do you think I'm asking if I think it would be weird, that's a strange sort of question,
weird isn't really a factor here, it would work fine, I'm sure if you went to the bathroom
and meditated.
It's also fine to practice in places that aren't necessarily private, provided that
other people are supportive, not talking or chatting, people are making noise, that's fine.
It's very hard when, it's usually very hard when people are talking, but as long as they're
not talking or watching television loudly or so on, you should be able to, in some instances,
practice, even though it's not private.
We've caught up to the question, Spunte, hmm, I don't have a lot of questions today.
Just a good sign, it's a sign that everyone knows everything already.
Well, if anyone is interested in taking our at-home course, recommend if you haven't
that you read our booklet on how to meditate and you can sign up for an at-home course,
maybe you'll have some questions then.
I can wait a little bit, maybe people are coming and have new questions.
I guess it's also, the holidays, right, people are preoccupied with other things.
What day is this?
The 23rd, probably people traveling and thinking about family and so on.
They say, if everyone, it's not worth it to infect your family members with a communicable
disease, especially your elder family members, if you can try and stay distant in cases
where you're not absolutely sure you're not going to infect them.
It's a part of the global community.
We have a responsibility to be conscientious of each other.
Not just do what we want, just because we think we know best.
I wish everyone a happy end of the year, the end of the year doesn't mean much for Buddhists.
This time of year doesn't mean anything, really, except to say that because we're counting
the end of the year, as the end of the year, we can think about the new year and think
about the old year and consider what we're planning for the new year, that it has some
new things in it, that we learn new things, doesn't just mean it's a new year, it should
be a new us as well, that we work towards greater knowledge and understanding and clarity
of mind.
Some more questions have come in, one thing, all right, as long as they keep coming, I can
keep answering.
I just became a Buddhist, would it be useful to come out to my family about this?
They are heavily Christian.
I mean, it's useful for people to know about Buddhism and to have to confront it, but it's
like having an allergic reaction to medicine or an aversion to medicine, it's both, and
the thing is, your family is going to be potentially quite upset, and so the question
is whether you can deal with the fallout of that, I would say generally it's a bad idea,
but I want to specify that technically in the long term, it's going to have, it could
have positive consequences if they're able to deal with it, so what I mean by allergic
reaction is some people just aren't able to take medicine, some people will never be able
to appreciate Buddhism, they're just too allergic to it, you have to be clear that your
parents are not those sorts of people, and often you'll find that they're able to appreciate
Buddhism, there's nothing bad about Buddhism, nothing offensive component, it's certainly
not necessary, it's not the kind of thing that you really have to do or else you're lying
or that sort of thing, it's not necessary to even call yourself Buddhist, there's no
such thing, it's just a concept, practice mindfulness and you can do that, it doesn't matter
what you call yourself, what happens on your online course, it's not really online except
that we meet, we have an audio talk every week, so you have to do at least an hour of
meditation a day, have walking, have sitting, you have to keep the five precepts, and we
meet once a week and every week it could give you a new exercise and we go through a course,
it's much more than you get from the booklet, the booklet is really just the basics,
the first step, but you need, you'll need to join our Discord server, we're now using
Discord for the meetings, and we need to use videos, so it's just an audio call, it's
free, so welcome to sign up, how can we regain the motivation to practice if we've been
away from the practice for a while, we'll just do it, there's really no secret, you can't
just wait for the desire to do it to come, you have to just do it, start being mindful,
you often have to separate the idea of meditation from actual mindfulness and just try
and be mindful, because mindfulness is something you can do right now, you should never
be away from that, and you find them more mindful, you're the more inclined and the more
easier it is for you to sit and walk, sometimes I do sitting meditation first, is this
okay, if you're going to do both, it's recommended to do walking first, there's really no reason
to do sitting first, but if you're just going to do sitting, you can just do sitting, that's
fine, but try and do walking first, it has benefits for sitting, can we take a second
at home course, because of the extenuating circumstances, since there's no possibility to
go on retreat for me, and I feel like I'd use some guidance, there is no second at home
course, I mean, feeling that you need the guidance isn't really a sign that you do need
it, you really need us to just continue, if you have questions or nothing, something
wasn't clear, you can get in touch, but really the point is that's the challenge, the
challenge is to live your life and cultivate mindfulness, you can do it right now, the effort
it took you to write that question, you could also have been mindful or hopefully you
were, but it takes, what I mean is it takes no more effort than coming here and asking
questions than it does to practice, if you have specific questions, well, that's what
this is for, but you really don't need another at home course, that's the point is to
make it, to go the next step now, and undertake the challenge of doing it on your own,
as you can, you can do it right now, sometimes when I know things, I get stuck on what
I noted, rather than focusing on the rising and falling, should I move past this, or
just practice, you're stuck on something, then you should note it, you should note that
you're stuck, I mean, stuck is not really what's happening, you have to figure out what's
happening, is there liking or disliking or worry or whatever, and note that.
The only time I find my mind can stay with meditation is when I am noting pain, is this
common? Common isn't of interest to us, if that's what's happening to you, that's what's
happening to you, but I wouldn't be so categorical about it, like it's probably not
true that you cannot note anything else, so you cannot stay with your meditation. Of course,
when there's something negative, the mind is, it's much easier for the mind to stick with
it, because the mind is obsessed with it, that's not necessarily a good thing, because
the mind is disliking, which is unwholesome. Meditation is challenging, no matter what
experiences you have, being truly mindful is going to be a challenge. It doesn't matter
whether it's common, you just deal with your condition, try and be patient and work towards
greater mindfulness, not just staying with the object, because when there's pain, of course
you're staying with it, but not in a very wholesome way, try and learn to cultivate clarity
of mind, then you'll find you can do that with everything.
But a Buddhist diet consists of veganism only, no. Can we see rising of the stomach as
arising and ceasing and falling as arising and ceasing? You should see them as rising
and falling, you shouldn't be too technical or theoretical about it. The answer is yes,
they are a rise, each one, of course, arises and ceases. I mean, it's quite simple. The
question is, the answer is pretty obvious, but the point, because we study it, you miss
the point, you see, when you study and get theoretical about it, if I can answer your question
by asking, does the rising of the stomach begin? Yes, does it end? Yes. Then, of course,
it's arising and ceasing. That's it. That's all there is to it. So don't be too fixated
on those ideas. Just when it's rising, try and note it from beginning to end. Let's
say falling, try and note it from beginning to end. What chance would be useful to chant
if one can't note? I don't understand if one can't note. I don't understand why.
Are there useful chants? I mean, chants are useful on a basic level. They're not going
to replace mindfulness. How long should one wait before attempting to ordain as a monk?
I can't answer that question. Ordaining as a monk is very personal and specific to an
individual. So you really have to talk to the person, the people who are involved with
your ordination and figure out what their instruction is. Should I be concerned that because
of some unwholesome past deeds, I was born into a society that is oblivious of the Buddhist
teaching and or lax asanga? I don't see what help it would be to be concerned. You can
be concerned in the sense of understanding the importance of those things. But really you're
in the position you're in and best advice is to just start from where you're at. You may
not become enlightened in this life very much because of your situation. A lot of people
are like that, but you can certainly get started. And with the internet, the fact that
you're able to get in touch with this far means you can get in touch with our community.
I wouldn't pay too much attention to your situation except to say it to realize that
well, to realize what's important, but to also realize how hard you're going to have
to work and appreciate the challenge. Don't let it make you discouraged or worry about it
or get sad about it, of course. I find it useful to do the Wim Hof method of breathing
before meditating. It gets me a bit more relaxed. Is this okay to use in conjunction with
my meditation? You can do what you want. If you're doing a course with us, I would ask
you not to. Should you? I wouldn't recommend it. I don't know what that is at all, but
I don't recommend any of anything else. No conditioning. See, getting yourself relaxed
is not the point. That means you're avoiding the stress. We want to learn about the stress
and understand the causes and the reasons for it. We want to appreciate stress for what
it is. Mindfulness is about facing things, not about making things easier. Make it easier
when you understand them when you become stronger by facing them, not by changing them
or avoiding them. During meditation, I get so calm and serene that I am left with nothing
to note. What next? When you're calm, you should note calm. There's something very clearly
to note, and that's the calm. If you like it, you should note that as well, but when
there's just calm to say calm, calm until it goes away. I've been dealing with
sloth for months. What should I note to get out of this lazy mind state? We're not trying
to get out of the states, per se, not directly. Just try and note lazy or tired. Lazy
can be a bit of a judgment. It's not really the experience. Try and note the experiences
of feeling tired or bored or uninterested and that sort of thing. Try and note what's
there, really. You don't have to pay attention to being lazy or the idea of being lazy.
Just try and note what you're experiencing. Could the first Jana arise during the practice
of insight meditation? The first Jana means different things. Different people explain it differently,
so I don't really want to go into it. I would say we don't really focus on those kind
of titles during our practice. I mean, the answer, I guess, technically is yes, and absolutely
is yes, but it's really hard to talk about these things because people have different
concepts and there are different kinds of Jana and that sort of thing. The orthodox is
that there are two kinds of Jana and you have to separate between them. Different shape
between them. How large of a hindrance are athletics, martial arts, to meditative progress?
How much of one at all I would say? I mean, there's going to be physical problems. There's
going to be mental problems. The physical problems are you be tired and tired makes it hard
to stay mindful. Mental problems are you going to get attached to it and egotistical
about it in the sense of having power and control over the body and that sort of thing
attached to the good feeling of being fit or that sort of thing. But they're very small.
I would say if you're mindful, if you do those things mindfully, they shouldn't have
much impact on an ordinary practice. They're the kind of things that you would be forbidden
to do during an intensive course. If you were to come to the center, you would have to
put them aside because their distractions and their problems both physically and mentally,
but because the intensive course is pretty intensive. Could every physical pain be overcome
by insight meditation? It depends what you mean by overcome. I mean, you can't get rid
of physical pain completely unless you get rid of the body. But overcoming it in terms
of not getting upset about it, that's the point. You can certainly do that through
the past and the meditation. Should we do other meditation too or is we past all we must do?
We past and I was all you must do, but there are other meditations that can be helpful.
You can be mindful of the Buddha, you can practice mid-kindness, you can practice mindfulness
of death and mindfulness of the foulness of the body. They're all supportive in different
ways. They're not necessary, but those four meditations are supportive of mindfulness
practice. What view should be taken toward lust? Well, you should see it as a problem
as unwholesome, as something to be vigilant about, but you just try and be mindful of it
when it arises. The most important thing is to be able to catch it and note it when it occurs,
note everything surrounding it as well, like the things you're lusting after, the thoughts
about it, the pleasure, pleasure involved with it, the frustration and disappointment when
you don't get it, that sort of thing. When noting, should we differentiate between doubt
in the practice or doubt about the dhamma and, for example, the most extreme opposite
doubt about useless, worldly things? No, you don't have to specify like that. The words
aren't that important. They're not magic that you have to find the right word for things.
They have something strikes you as doubt, just noted as doubt, that's enough. We're just
trying to see things as they are, rather than make more of them. You see, if you say doubt
and that's all it is, you're just reminding yourself that that's all it is.
Would you recommend doing Repassna before Samatha or the Samatha first? If we're going
to do it first and then use it as a support for Repassna? When I meditate, my perception
of reality becomes sometimes distorted. Is this normal? Mindfulness isn't concerned about
what is normal. In fact, you're going to find a lot of things abnormal, so become sort
of prepare yourself for that, become flexible. If that's your experience, then just try
to note it, try and figure out what you mean by distorted, what is happening, you're
seeing things, hearing things, feeling things. Are there emotions in that sort of thing?
Just try and know what's there. Don't be concerned with whether it's normal or not. Being
abnormal is a sign that of helping you see reality or the universe or experience. Life
is unpredictable. When we meditate enough, we hopefully will find the middle way, but that
seems like a bland, pointless place to be, so I find motivation to meditate hard. Am I
completely wrong about the middle way? Well, just trying to find motivation for meditation
is sometimes misleading. You can't rely on motivation like a thing that's going to push
you to do it more. You just have to do it. And the reason why you do it is nothing conceptual
about a middle way. It's about seeing things just as they are clearly. And there's nothing
bland about that. There's nothing pointless about that. What's pointless is our reactions
to things. And worse than pointless, harmful. And when you see how harmful they are, the
middle way looks pretty good by comparison. Middle way just means seeing things without
all of this harmful reaction. Where's the first place to begin when starting the journey?
Well, it depends really how you're asking that, but I guess I would just answer read our
booklet on how to meditate. That's probably the best advice I can give. And then if you
really are keen on beginning, the best way is not just to read the booklet, but to sign
up for an at-home course. You can do that on our website. There are links in the description.
All free. Take an at-home course and get started.
How big of a hindrance to meditation progress is learning a musical instrument. How about
reading classic books? Musical instrument will be more of a hindrance because music
relates very much to liking and pleasure. So there is going to be problems associated
with that. I mean, it's not a huge hindrance. It just, it is a hindrance. And for classic
books, I assume you mean classical fiction. Fiction is problematic. It can have a good
lesson in it. There are books like maybe some of Dostiovsky's books. I often refer to
crime and punishment, but even still limited. Mostly it's still for entertainment. If it's
for entertainment and if you drive entertainment, pleasure from it, that's going to become
addictive. I mean, it's not a serious problem. You don't have addiction support groups for
people who listen to music, for example, but in terms of the refinement of mind that on
the level of Buddhist practice, it's going to be a hindrance. And if you don't believe
me, you can find out for yourself, but I'm telling you, it's going to distract you from
your practice, prevent you from having a smooth and swift attainment of clarity and purity
of mind. I have trouble when I meditate. It feels like the feeling in my feet lock up.
May I have some advice. I don't quite understand that's not how we would say it in English,
I think, but if there's a feeling in your feet, just a feeling feeling. That's not really
a trouble. We just try to note everything. I don't know if you read the booklet on how to
meditate, but I recommend that. If you want to take an at home course, you can do that
maybe as well.
How can I understand and get rid of lust? Well, read our booklet, get started. It's a long
journey for most people. People are prepared for a journey and be patient and work.
I have schizophrenia and among medication, I meditate. Do you think I could handle the symptoms
delusions without medication? Yes, I think you could. I don't know your situation, so that's
probably an improper thing to say, but I mean, if the question is general can someone
who has schizophrenia, then the answer is yes. I've dealt with a couple of people now who
have schizophrenia, and they are able to. It's going to be limited and challenging. Let's
not say limited. It's going to be limited by your capacity to straighten out your perception
of the delusions. So let's not call them delusions. Let's call them illusions. Separate
delusion from illusion. You're going to have hallucinations, illusions, even thoughts.
Even the thoughts don't think of them as delusions. Delusion is when you believe them and try
and separate those two things. Do you believe that the thoughts, do you believe the voices
in your head, et cetera, whatever it is for you? Do you believe these things? That's where
the delusion is. We don't believe things in Buddhism. We perceive them and we try to see
them just as they are. So be clear about that Buddhist practice and mindfulness practice
issues any sort of belief. So you can keep that as an idea in your mind. Never believe
anything. Belief shouldn't play a part. It should just be experience and perception and
observation and clarity seeing things just as they are. That's all you need literally.
And you can do that with things that you see here here in your head. You can also do that
with the reaction. So when you're paranoid, when you're afraid, when you're clinging, when
you're reacting to things, you can note those things as well. Try and take them at those
emotions as objects. And when you see them clearly, they have no power over you.
What do we do if we get music stuck in our head? I note it, but the song does not go away
and it can get very annoying, making me frustrated. Here's the problem with music. So we're
not trying to make things go away. So that's not a problem. Noting it is fine. Note it. You're
not trying to make it go away. If after a long time, it doesn't go away, you can just try
and ignore it. When you're annoyed and frustrated, well, that's the kind of thing we want
to learn about how we get annoyed and frustrated and we want to change that. So try and
note annoyed and frustrated and learn to just see it as it is, experience it as it is.
What would be a good reason to read the Visudhi Magha? From front to back, and you'd have
to probably be a monk to want to undertake that sort of thing. That's maybe not fair.
I would say be an experienced meditator. I would recommend long-term meditators in our tradition
to undertake it. I think it's worth reading. I think it absolutely is. You don't really need
a special reason, but have a solid practice first. It won't be much help otherwise.
If we can cut up to the pace of questions, but let's stop there then. It was good. In the
end, we got a bunch. Some good questions too. I mean, good by good. Some that were a bit
new, I thought. We always get good questions. All right. Well, thank you, everyone. That's
all for questions then. Have a good night. You can talk again and chat. You can say sad.
You have other questions. You can bring them on Saturday or next week. Sadhguru.
Thank you.
