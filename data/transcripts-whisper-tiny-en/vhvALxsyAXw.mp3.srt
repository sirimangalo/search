1
00:00:00,000 --> 00:00:09,000
Hi, and here is number three in the top five reasons why everyone should practice meditation.

2
00:00:09,000 --> 00:00:21,000
The number three reason why everyone should practice meditation is because meditation is able to do away with both physical and mental suffering.

3
00:00:21,000 --> 00:00:28,000
So in the last video I talked a little bit about some of the ways meditation can overcome mental suffering.

4
00:00:28,000 --> 00:00:55,000
I didn't talk at all about physical suffering, but it's important to understand about mental suffering that not only does meditation help you to understand mental suffering for what it is instead of letting it escalate and snowball and become what we call depression or what we call anxiety or stress or all of these conditions which we end up taking medication for.

5
00:00:55,000 --> 00:01:06,000
Not only does it allow you to come back to realize that it's simply an unpleasant feeling in the mind which is based on some attachment to reality as conceptual and so on and so on.

6
00:01:06,000 --> 00:01:19,000
It also, meditation also helps us to overcome this very rudimentary, unpleasant feeling in the mind, a feeling of sadness, a feeling of anger, a feeling of unpleasantness in regards to the reality versus experiencing.

7
00:01:19,000 --> 00:01:33,000
Because at the moment when we experience things for what they are, we come to see that really there's nothing inside of ourselves during the world around us which is either good or bad, which is worth holding on to or worth pushing away.

8
00:01:33,000 --> 00:01:54,000
We come to see things for what they are as arising, staying with us for a short time and then disappearing, coming and going. And we see that everything both inside of us in the world around us is, we say it's ephemeral, it's something which is like a bubble, it comes and it goes and arises and it ceases.

9
00:01:54,000 --> 00:02:06,000
And when we see this, when we start to see things for what they are, we're using this clear thought and simply giving things than their true name, giving things their true importance.

10
00:02:06,000 --> 00:02:21,000
So when we see something saying that it's reminding ourselves that it's seeing, when we hear something reminding ourselves that we're hearing, instead of letting it become some noise or voice or insult or so on, something which is unpleasant to us.

11
00:02:21,000 --> 00:02:27,000
And the unpleasant feeling in the mind simply doesn't arise anymore.

12
00:02:27,000 --> 00:02:37,000
This is how meditation is able to do away with, very clearly do away with unpleasant mental sensations or mental sufferings.

13
00:02:37,000 --> 00:02:50,000
As for physical suffering, obviously in the beginning meditation is not able to do away with things like back aches or headaches or pain, legs, any kind of sicknesses which we have in the body.

14
00:02:50,000 --> 00:02:57,000
But it is true that over time meditation can do away with many, if not all of the physical sufferings which we face.

15
00:02:57,000 --> 00:03:09,000
First, many of the sicknesses in our body, all kinds of curable or even thought to be incurable diseases, have been cured through the practice of meditation.

16
00:03:09,000 --> 00:03:14,000
High blood pressure has been a very obvious one, of course.

17
00:03:14,000 --> 00:03:23,000
But things like cancer and other kinds of seemingly incurable diseases have been cured through the practice, especially of walking meditation.

18
00:03:23,000 --> 00:03:42,000
This is one way that meditation is able to do it with physical suffering, is that it's actually able to do away with sicknesses through the concentration, through the release as well of the mind, of the influence of the mind over the body, instead of stressing and clamping down on the body,

19
00:03:42,000 --> 00:03:50,000
and forcing it into this or that unnatural state, instead of allowing the body to be in its natural movement.

20
00:03:50,000 --> 00:04:03,000
And when we walk simply knowing that we're walking, when we sit simply knowing that we're sitting, when we move simply knowing that we're moving, reminding ourselves again and again of the ultimate reality and not letting ourselves get stressed about things.

21
00:04:03,000 --> 00:04:17,000
But on a deeper level, meditation is actually able to allow us to re-understand or understand in a new way the physical sensations which we experience.

22
00:04:17,000 --> 00:04:30,000
So when we experience what we normally call pain, instead of seeing it as an uncomfortable or a suffering sensation, we simply remind ourselves that it's this phenomenon which we call pain.

23
00:04:30,000 --> 00:04:40,000
So as we say to ourselves, pain, pain, pain, we come to realize that it's simply a sensation that arises in seasons, it's something that comes and goes.

24
00:04:40,000 --> 00:04:43,000
We come to see that it's no different from a happy feeling.

25
00:04:43,000 --> 00:04:47,000
It says just one other sensation that arises in seasons.

26
00:04:47,000 --> 00:04:54,000
And as we start to realize that the pain, the unpleasantness, the sharpness of the sensation disappears.

27
00:04:54,000 --> 00:05:02,000
And all that's left is a sensation, a very neutral, not pleasant, not unpleasant sensation.

28
00:05:02,000 --> 00:05:07,000
It pains in the back, pains in the hand, pains all over the body.

29
00:05:07,000 --> 00:05:15,000
People who are dying, people who are in a very difficult situation are thus able to do away with otherwise unbearable situations.

30
00:05:15,000 --> 00:05:21,000
And see them in a whole new light and be able to experience them simply for what they are.

31
00:05:21,000 --> 00:05:29,000
This, in a nutshell, is the third top of the top five reasons why everyone should practice meditation.

32
00:05:29,000 --> 00:05:36,000
And this is number three in the set of videos, and please look for the number two reason in the near or distant future.

33
00:05:36,000 --> 00:05:59,000
Thanks for tuning in.

