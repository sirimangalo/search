1
00:00:00,000 --> 00:00:02,500
Welcome back to Ask a Monk.

2
00:00:02,500 --> 00:00:05,000
Next question comes from

3
00:00:05,000 --> 00:00:10,000
Elect Maham, who asks

4
00:00:10,000 --> 00:00:14,000
I am wanting to learn to transcend my suffering

5
00:00:14,000 --> 00:00:16,000
and overcome my suffering,

6
00:00:16,000 --> 00:00:21,000
but I also wish to positively affect change in the world around me.

7
00:00:21,000 --> 00:00:24,000
How can I reconcile the idea of accepting this reality

8
00:00:24,000 --> 00:00:28,000
with my desire to make the world better for all?

9
00:00:28,000 --> 00:00:32,000
Good question.

10
00:00:32,000 --> 00:00:36,000
There's a few things that you have to keep in mind

11
00:00:36,000 --> 00:00:38,000
in this sort of situation,

12
00:00:38,000 --> 00:00:42,000
which I think is the situation that we all should be in,

13
00:00:42,000 --> 00:00:46,000
the desire to help ourselves and the desire to help others.

14
00:00:46,000 --> 00:00:50,000
The first is knowing your place in the world.

15
00:00:50,000 --> 00:00:56,000
I think for anyone who expects to

16
00:00:56,000 --> 00:01:01,000
change the world in its entirety

17
00:01:01,000 --> 00:01:04,000
to make the world a peaceful place.

18
00:01:04,000 --> 00:01:06,000
I think obviously,

19
00:01:06,000 --> 00:01:10,000
and I hope there's not many people in that sort of delusion,

20
00:01:10,000 --> 00:01:14,000
I think that there's no hope of ever

21
00:01:14,000 --> 00:01:17,000
making the world a peaceful place.

22
00:01:17,000 --> 00:01:19,000
I mean, to put it on to context,

23
00:01:19,000 --> 00:01:22,000
that eventually this world is going to,

24
00:01:22,000 --> 00:01:26,000
you know, the planet is going to fly into the sun

25
00:01:26,000 --> 00:01:32,000
or be burnt up, burnt to a crisp by the heat of the sun.

26
00:01:32,000 --> 00:01:35,000
So there's no hope.

27
00:01:35,000 --> 00:01:39,000
I mean, we can help the world and we can help other people,

28
00:01:39,000 --> 00:01:43,000
but in the end we have to give up and we have to let go.

29
00:01:43,000 --> 00:01:54,000
The key to finding a balance is in knowing what you can do

30
00:01:54,000 --> 00:01:57,000
and accepting the things that you can't do.

31
00:01:57,000 --> 00:02:00,000
So we want to be able to help people

32
00:02:00,000 --> 00:02:03,000
then we understand that we can help the people around us.

33
00:02:03,000 --> 00:02:05,000
We can help our family, we can help our friends,

34
00:02:05,000 --> 00:02:12,000
we can affect change in our sphere of influence.

35
00:02:12,000 --> 00:02:17,000
And we can change and expand that sphere of influence,

36
00:02:17,000 --> 00:02:20,000
but in the end it's always going to be limited.

37
00:02:20,000 --> 00:02:26,000
And well, I think that it's important to,

38
00:02:26,000 --> 00:02:30,000
it's important part of who we are to want to help others.

39
00:02:30,000 --> 00:02:32,000
I think that's the key,

40
00:02:32,000 --> 00:02:38,000
is that it should be simply a part of who we are.

41
00:02:38,000 --> 00:02:43,000
If someone has ambition to change the world

42
00:02:43,000 --> 00:02:45,000
and that's their goal,

43
00:02:45,000 --> 00:02:48,000
then I think that ambition is actually going to get in the way

44
00:02:48,000 --> 00:02:53,000
of their successful advancement as a human being

45
00:02:53,000 --> 00:02:57,000
because they're going to always be frustrated and upset

46
00:02:57,000 --> 00:03:04,000
by just by not being able to affect the sorts of change

47
00:03:04,000 --> 00:03:07,000
that they want to affect.

48
00:03:07,000 --> 00:03:10,000
I think the real answer to your question

49
00:03:10,000 --> 00:03:15,000
is that by changing yourself and by learning to accept

50
00:03:15,000 --> 00:03:19,000
and let go, you become a better person.

51
00:03:19,000 --> 00:03:23,000
And as a better person,

52
00:03:23,000 --> 00:03:26,000
you affect the change in the world around you

53
00:03:26,000 --> 00:03:29,000
that you would like to see.

54
00:03:29,000 --> 00:03:32,000
I think it was Gandhi who said that to be the change

55
00:03:32,000 --> 00:03:35,000
that you would like to see in the world.

56
00:03:35,000 --> 00:03:40,000
It's wrong for us to go about thinking

57
00:03:40,000 --> 00:03:44,000
that we're going to change other people who change the world

58
00:03:44,000 --> 00:03:50,000
when we ourselves are full of things that should be changed,

59
00:03:50,000 --> 00:03:53,000
are full of negative state, negative mind states,

60
00:03:53,000 --> 00:03:57,000
and negative emotions and so on.

61
00:03:57,000 --> 00:04:00,000
If we can't change those,

62
00:04:00,000 --> 00:04:03,000
then our acts in the world,

63
00:04:03,000 --> 00:04:06,000
in our sphere of influence, the being who we are,

64
00:04:06,000 --> 00:04:09,000
which is very much connected with the world around us,

65
00:04:09,000 --> 00:04:12,000
is going to have a negative impact on the world.

66
00:04:12,000 --> 00:04:15,000
It's going to have a negative impact on our minds

67
00:04:15,000 --> 00:04:19,000
and it's a negative impact on the minds of beings around us.

68
00:04:19,000 --> 00:04:24,000
If our mind is pure, as we work to purify our minds,

69
00:04:24,000 --> 00:04:32,000
we will naturally affect the beings and the world around us,

70
00:04:32,000 --> 00:04:36,000
whether we're even in the jungle,

71
00:04:36,000 --> 00:04:40,000
seemingly removed from the world.

72
00:04:40,000 --> 00:04:46,000
If a person is dedicated to clearing their mind,

73
00:04:46,000 --> 00:04:50,000
they can't help but have an effect on the world at large,

74
00:04:50,000 --> 00:04:55,000
and they have it in their own sphere of influence.

75
00:04:55,000 --> 00:04:58,000
So not everyone will be living in the forest

76
00:04:58,000 --> 00:05:00,000
for some people that's their sphere of influence,

77
00:05:00,000 --> 00:05:05,000
that their life naturally gravitates towards seclusion.

78
00:05:05,000 --> 00:05:10,000
For other people, it naturally gravitates towards society.

79
00:05:10,000 --> 00:05:14,000
The important thing is that whatever life you live,

80
00:05:14,000 --> 00:05:24,000
that you should work to purify your sphere of existence.

81
00:05:24,000 --> 00:05:28,000
And that includes your own mind and that includes helping other people.

82
00:05:28,000 --> 00:05:32,000
It should be a part of who you are.

83
00:05:32,000 --> 00:05:36,000
And in that sense, I don't think there's any conflict at all

84
00:05:36,000 --> 00:05:38,000
between helping yourself and helping other people.

85
00:05:38,000 --> 00:05:41,000
The Buddha said when you help yourself, you help others.

86
00:05:41,000 --> 00:05:43,000
When you help others, you help yourself.

87
00:05:43,000 --> 00:05:44,000
And it should be that way.

88
00:05:44,000 --> 00:05:48,000
You should never put the welfare of other people

89
00:05:48,000 --> 00:05:51,000
in front of your own welfare,

90
00:05:51,000 --> 00:05:54,000
thinking that you'll help other people in some way

91
00:05:54,000 --> 00:05:58,000
while sacrificing your own mental clarity.

92
00:05:58,000 --> 00:06:00,000
Because that's the point,

93
00:06:00,000 --> 00:06:04,000
is that you're going to only create tension and stress.

94
00:06:04,000 --> 00:06:09,000
You see all these organizations that I used to get involved in

95
00:06:09,000 --> 00:06:12,000
that are trying to affect change in the world

96
00:06:12,000 --> 00:06:14,000
and in third world countries and politically,

97
00:06:14,000 --> 00:06:17,000
and so on, trying to help people.

98
00:06:17,000 --> 00:06:21,000
And the people are in many ways stressed and miserable

99
00:06:21,000 --> 00:06:25,000
and get angry and fight and so on.

100
00:06:25,000 --> 00:06:27,000
There's a lot of suffering involved

101
00:06:27,000 --> 00:06:32,000
because they're putting the world in front of themselves.

102
00:06:32,000 --> 00:06:36,000
And as we can see in the world today, it's not working.

103
00:06:36,000 --> 00:06:40,000
The good that they're doing is not having any lasting impact.

104
00:06:40,000 --> 00:06:46,000
If you're able to build hospitals or feed the poor

105
00:06:46,000 --> 00:06:49,000
or eradicate this disease or that disease,

106
00:06:49,000 --> 00:06:54,000
it's like trying to stem the flow of a river

107
00:06:54,000 --> 00:06:56,000
with your bare hands.

108
00:06:56,000 --> 00:06:58,000
You're not going to get very far.

109
00:06:58,000 --> 00:07:00,000
You can see that we're not getting very far.

110
00:07:00,000 --> 00:07:04,000
There's no one in the world who's really going to,

111
00:07:04,000 --> 00:07:07,000
but it's impossible to really make the world

112
00:07:07,000 --> 00:07:08,000
the perfect place.

113
00:07:08,000 --> 00:07:12,000
And I can tell you, I'm here in nature

114
00:07:12,000 --> 00:07:15,000
and I'm surrounded by murderers

115
00:07:15,000 --> 00:07:19,000
and violence all the time.

116
00:07:19,000 --> 00:07:21,000
The ants are killing this or that

117
00:07:21,000 --> 00:07:24,000
and the lizards and the monkeys.

118
00:07:24,000 --> 00:07:32,000
The universe is quite very much out of our control really.

119
00:07:32,000 --> 00:07:37,000
And this is why I say we should work

120
00:07:37,000 --> 00:07:41,000
to purify our sphere of existence,

121
00:07:41,000 --> 00:07:43,000
our sphere of influence.

122
00:07:43,000 --> 00:07:47,000
And to go beyond that is to do something that's futile,

123
00:07:47,000 --> 00:07:49,000
something that's not going to have the results

124
00:07:49,000 --> 00:07:51,000
that we hope for.

125
00:07:51,000 --> 00:07:53,000
It takes us out of balance

126
00:07:53,000 --> 00:07:55,000
and therefore leads to stress

127
00:07:55,000 --> 00:07:57,000
and suffering both for ourselves

128
00:07:57,000 --> 00:07:58,000
and for other people,

129
00:07:58,000 --> 00:08:01,000
even though we might have limited success

130
00:08:01,000 --> 00:08:06,000
in terms of improving the states of existence

131
00:08:06,000 --> 00:08:08,000
for various people,

132
00:08:08,000 --> 00:08:13,000
for a temporary period of time.

133
00:08:13,000 --> 00:08:18,000
Helping people should really be a part of our spiritual development.

134
00:08:18,000 --> 00:08:21,000
It should be no difference

135
00:08:21,000 --> 00:08:24,000
between helping ourselves and helping others

136
00:08:24,000 --> 00:08:28,000
and they should both be the improvement of the world,

137
00:08:28,000 --> 00:08:30,000
the world that we live in,

138
00:08:30,000 --> 00:08:32,000
which is our sphere of existence.

139
00:08:32,000 --> 00:08:37,000
So I hope that helps and is not too convoluted.

140
00:08:37,000 --> 00:08:40,000
Thanks for the question and all the best.

