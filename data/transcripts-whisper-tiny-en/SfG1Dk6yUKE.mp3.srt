1
00:00:00,000 --> 00:00:01,000
Okay, ready.

2
00:00:01,000 --> 00:00:05,960
We can one overdose on Buddhist theory and Dhamma.

3
00:00:05,960 --> 00:00:14,000
It seems to me that most benefits come from meditation once the Buddhist basics are understood.

4
00:00:14,000 --> 00:00:22,720
Yes, well, if you've heard the latest video on my channel, our venerable teacher brought

5
00:00:22,720 --> 00:00:26,440
up a very good point and that is that there are two ways of study.

6
00:00:26,440 --> 00:00:32,400
There is studying from beginning to end and they call the end-up, which means in order.

7
00:00:32,400 --> 00:00:38,320
And then there is the studying to a sun-dot, which means with sun-dot means contentment,

8
00:00:38,320 --> 00:00:41,760
sun-toot-t, which means just enough.

9
00:00:41,760 --> 00:00:43,760
So there are two ways to study.

10
00:00:43,760 --> 00:00:48,600
If you want to study, learn the whole of the Buddhist teaching.

11
00:00:48,600 --> 00:00:53,920
If you want to really study, then you have to learn, really memorize as much as you can.

12
00:00:53,920 --> 00:00:58,720
And if you can memorize the whole of the three tepitikas, three tepitikas, then you memorize

13
00:00:58,720 --> 00:00:59,720
all three of them.

14
00:00:59,720 --> 00:01:08,320
If you can memorize one, then you memorize one and you discuss it and you analyze it and so on.

15
00:01:08,320 --> 00:01:13,720
But what he made clear is that for the meditation practice, all you need is the, for example,

16
00:01:13,720 --> 00:01:14,720
the force at Deepatana.

17
00:01:14,720 --> 00:01:17,720
If you can learn the force at Deepatana, that's enough.

18
00:01:17,720 --> 00:01:22,080
So the point being that in Buddhism, there is no requirement that you study all of the

19
00:01:22,080 --> 00:01:23,160
Buddhist teaching.

20
00:01:23,160 --> 00:01:27,280
And in fact, if you start studying the Buddha's teaching, you can see that.

21
00:01:27,280 --> 00:01:32,040
And I think it's difficult, especially as a practical meditator, to miss that, that the Buddha

22
00:01:32,040 --> 00:01:35,440
didn't teach the tepitika to anyone, right?

23
00:01:35,440 --> 00:01:38,560
He didn't say, okay, now we're going to start with the vinya pitika, sit down and hold

24
00:01:38,560 --> 00:01:39,560
onto your head.

25
00:01:39,560 --> 00:01:44,160
So we got a six-month course or five-year course, how long would it take to teach the

26
00:01:44,160 --> 00:01:45,560
whole tepitika?

27
00:01:45,560 --> 00:01:46,880
He taught one suta.

28
00:01:46,880 --> 00:01:50,760
And this is the joke I was joking, I say, okay, from now on, what we're going to do is

29
00:01:50,760 --> 00:01:54,440
I'm going to give one dhamma talk, and you have to memorize that dhamma talk, and that's

30
00:01:54,440 --> 00:01:58,560
your dhamma talk, and become enlightened on that.

31
00:01:58,560 --> 00:02:02,160
Because everyone's saying, where's the next video, where's the next video on that?

32
00:02:02,160 --> 00:02:07,320
If you read the suta, you see that all the Buddha would give one suta often.

33
00:02:07,320 --> 00:02:12,040
One discourse would be enough, and they'd take that discourse away and become enlightened

34
00:02:12,040 --> 00:02:13,040
based on it.

35
00:02:13,040 --> 00:02:20,400
Now, we obviously don't expect that, but we do expect that new focus more on the practice.

36
00:02:20,400 --> 00:02:25,160
Now, with the next option, Mahasim Saiyatta makes a really good point that I think is worth

37
00:02:25,160 --> 00:02:32,960
bringing up, is that, as he says, if you want to go it alone, if you don't have a teacher,

38
00:02:32,960 --> 00:02:38,040
if you don't have the opportunity to undertake a course where someone, by teacher means

39
00:02:38,040 --> 00:02:43,160
where you undertake a course with someone who is daily adjusting your practice and catching

40
00:02:43,160 --> 00:02:51,880
you when you're adjusting your understanding and pushing you back on track, when you're

41
00:02:51,880 --> 00:02:56,720
getting too overconfident to pull you back, when you're getting too underconfident

42
00:02:56,720 --> 00:03:07,280
or discovered, skeptical or unsure of yourself to encourage you to push you forward, when

43
00:03:07,280 --> 00:03:11,800
you get off track to keep you back on track, when you're on track to keep you on track.

44
00:03:11,800 --> 00:03:13,800
These are the four duties of a teacher.

45
00:03:13,800 --> 00:03:19,160
If you have someone like that, then all you need is the bare and bare minimum of theory.

46
00:03:19,160 --> 00:03:24,000
If you can memorize the force that the Patanas were to the four foundations of mindfulness,

47
00:03:24,000 --> 00:03:25,160
then that's more than enough.

48
00:03:25,160 --> 00:03:26,160
Some people don't even do this.

49
00:03:26,160 --> 00:03:28,320
They think they can go and meditate and don't do this.

50
00:03:28,320 --> 00:03:32,400
You need to memorize, at least, for example, the four foundations of mindfulness.

51
00:03:32,400 --> 00:03:36,800
If you're practicing mindfulness, everyone should memorize these four things.

52
00:03:36,800 --> 00:03:37,800
It's four words.

53
00:03:37,800 --> 00:03:39,960
Body, feelings, mind, damba.

54
00:03:39,960 --> 00:03:44,720
You can memorize those four words that's by my teacher's word and I'm standing by him.

55
00:03:44,720 --> 00:03:46,240
That's enough theory.

56
00:03:46,240 --> 00:03:50,200
If you want to go to loan on the other hand, as Mahasiciah says, then you've got your

57
00:03:50,200 --> 00:03:54,440
work cut out for you, then you really have to learn the whole the tepitika.

58
00:03:54,440 --> 00:03:58,440
You have to have everything because you've got to be ready to catch absolutely everything

59
00:03:58,440 --> 00:03:59,440
that might go wrong.

60
00:03:59,440 --> 00:04:03,480
Maybe that's overkill, not the whole tepitika, but you have to memorize.

61
00:04:03,480 --> 00:04:05,400
You have to know quite a bit.

62
00:04:05,400 --> 00:04:06,800
This is how Mahasiciah puts it.

63
00:04:06,800 --> 00:04:11,360
I think that's fair, that without a teacher, in order to not get distracted, you need

64
00:04:11,360 --> 00:04:14,160
a lot of theory.

65
00:04:14,160 --> 00:04:18,640
That can be gained from, you could get a halfway from reading books on meditation.

66
00:04:18,640 --> 00:04:23,320
They have commentaries, modern commentaries, modern meditation teachers, like someone who's

67
00:04:23,320 --> 00:04:25,920
mentioning Buddha Dasa has the handbook for mankind.

68
00:04:25,920 --> 00:04:33,080
You can follow that handbook and consider that to be a middle ground where it's kind of

69
00:04:33,080 --> 00:04:46,400
a teacher, but it's a little bit more theoretical to hopefully keep you in the realm

70
00:04:46,400 --> 00:04:48,760
of right practice.

71
00:04:48,760 --> 00:05:00,480
You said everything there is to say about that question.

72
00:05:00,480 --> 00:05:06,160
The other aspect of the question is in regards to the benefits, because if you ask what are

73
00:05:06,160 --> 00:05:13,920
the benefits to Buddhist theory and study versus the benefits of practice, well, there's

74
00:05:13,920 --> 00:05:17,560
not much to say.

75
00:05:17,560 --> 00:05:22,760
It's quite clear that if you eat the food you get full, if you just sit there looking

76
00:05:22,760 --> 00:05:24,720
at the menu, you don't get any.

77
00:05:24,720 --> 00:05:25,720
But it's true.

78
00:05:25,720 --> 00:05:34,800
Many people will start demogroups just to study and just to discuss and just to go over

79
00:05:34,800 --> 00:05:43,480
the texts and revel in the profundity of them, maybe even philosophizing and trying

80
00:05:43,480 --> 00:05:48,040
to relate them back to one's own life in a very intellectual manner.

81
00:05:48,040 --> 00:05:53,320
There are demogroups around the world that do that, and think that they're getting real

82
00:05:53,320 --> 00:05:54,320
benefit from it.

83
00:05:54,320 --> 00:05:59,040
They feel like it's really enriching their lives and that they are active Buddhist.

84
00:05:59,040 --> 00:06:02,720
These kind of people get amazed when they come to actually practice intensive meditation

85
00:06:02,720 --> 00:06:07,480
and they say, oh, so this is what impermanence means.

86
00:06:07,480 --> 00:06:14,720
It's not just about, I was young once and now I'm older or, yeah, yesterday was different

87
00:06:14,720 --> 00:06:18,760
from today or, wow, the next moment who knows what's going to come, maybe I'll break

88
00:06:18,760 --> 00:06:29,080
a leg or maybe I'll get run over by a car or no, they can see moment-to-moment, the development

89
00:06:29,080 --> 00:06:30,560
of insight.

90
00:06:30,560 --> 00:06:33,520
Mahasi Saita makes another good point about this.

91
00:06:33,520 --> 00:06:39,960
He says, some people say, well, couldn't you just study the 16 stages of knowledge?

92
00:06:39,960 --> 00:06:44,440
He teaches based on this commentary that goes through the 16 stages of knowledge.

93
00:06:44,440 --> 00:06:48,960
Can't you just teach in a study course, these 16 stages of knowledge and talk to your

94
00:06:48,960 --> 00:06:51,720
students about it and say, okay, so now we're at the first stage.

95
00:06:51,720 --> 00:06:56,240
Let's look at our body and say, is this body, yes, this is body, is this mind, yes, this

96
00:06:56,240 --> 00:06:57,240
is mind.

97
00:06:57,240 --> 00:06:59,800
Okay, now we're at the first stage of knowledge.

98
00:06:59,800 --> 00:07:04,040
And the second one, and Mahasi Saita even entertains it, he says, I could go with that.

99
00:07:04,040 --> 00:07:09,960
If they want to say this, I can go with that if they could say that they're developing

100
00:07:09,960 --> 00:07:14,680
right concentration, and it's clear that they don't have right concentration, because

101
00:07:14,680 --> 00:07:17,960
they're looking at the body and saying, yes, that's body and they're looking at the

102
00:07:17,960 --> 00:07:22,240
mind and saying, yes, that's mind, but they don't have the concentration that allows

103
00:07:22,240 --> 00:07:26,800
the mind to really and truly see that this is body, that this is mind and to be able

104
00:07:26,800 --> 00:07:28,440
to separate them up.

105
00:07:28,440 --> 00:07:34,800
So this is the category of a different, for most people this is, if you've studied Buddhism,

106
00:07:34,800 --> 00:07:40,680
you can see how critical the Buddha is of monks who just study, but for some people, they

107
00:07:40,680 --> 00:07:47,800
don't realize the difference between, for example, watching all of my YouTube videos, and

108
00:07:47,800 --> 00:07:52,160
watching one of the YouTube videos, that's how to practice meditation and actually meditating

109
00:07:52,160 --> 00:07:55,120
according to that, it's quite different.

110
00:07:55,120 --> 00:08:00,200
It's a lot easier to watch hours and hours of YouTube videos than it is to even do one

111
00:08:00,200 --> 00:08:06,920
hour of meditation practice, and for that reason, the benefit is on a whole other level

112
00:08:06,920 --> 00:08:12,240
as well, which again, from it.

113
00:08:12,240 --> 00:08:22,040
There is a story about monks, there was an older monk who just meditated and didn't study

114
00:08:22,040 --> 00:08:34,760
any dharma, and other monks called it him for that, and he said, well, I'm old, so I

115
00:08:34,760 --> 00:08:42,520
prefer to meditate, and then they told the Buddha, this monk is not studying, he should

116
00:08:42,520 --> 00:08:51,480
study the dharma, and the Buddha said as far as I remember that's fine for him just

117
00:08:51,480 --> 00:08:56,040
to meditate, and there's several stories similar to that.

118
00:08:56,040 --> 00:09:03,960
One of them even says, this is my son, who are you, the meaning of this, because one of

119
00:09:03,960 --> 00:09:08,040
them was an Arahan, the one who had actually meditated was an Arahan, and the one who's

120
00:09:08,040 --> 00:09:15,440
that he was still a world thing, who's basically, who are you to talk to my son that way?

121
00:09:15,440 --> 00:09:21,120
Because the son means someone who is actually inherited his Buddha's fortune, the Buddha's

122
00:09:21,120 --> 00:09:22,120
treasure.

