1
00:00:00,000 --> 00:00:07,000
One, is it possible to transfer the merit of good deeds to a departed person?

2
00:00:07,000 --> 00:00:15,000
Two, is it possible to reconnect with a departed person in any way?

3
00:00:15,000 --> 00:00:24,000
Well, you can, of course, transfer the merits of good deeds to a departed person.

4
00:00:24,000 --> 00:00:29,000
The question which I assume is what you're asking is whether it actually gets to them,

5
00:00:29,000 --> 00:00:36,000
which is another question entirely.

6
00:00:36,000 --> 00:00:40,000
I think the standard answer is that it may or it may not,

7
00:00:40,000 --> 00:00:48,000
but I think if we're going to be open-minded or kind of objective about it,

8
00:00:48,000 --> 00:00:55,000
that it probably always gets to the person in the following way,

9
00:00:55,000 --> 00:01:00,000
the standard way of understanding the transferring of merit

10
00:01:00,000 --> 00:01:08,000
is that maybe just maybe those beings are hanging around as ghosts

11
00:01:08,000 --> 00:01:13,000
and waiting for somebody to do something and dedicate it to them

12
00:01:13,000 --> 00:01:21,000
so they can rejoice in it and receive the merit of rejoicing in someone else's deeds.

13
00:01:21,000 --> 00:01:25,000
Which apparently can have profound effects on the psyche of a ghost

14
00:01:25,000 --> 00:01:33,000
because they are ethereal beings, they're fine material.

15
00:01:33,000 --> 00:01:38,000
Just as when angels get angry, they're very quick to fall from the state of being an angel,

16
00:01:38,000 --> 00:01:43,000
an angel can actually die by getting angry. They're so susceptible to emotions.

17
00:01:43,000 --> 00:01:51,000
So if a ghost really feels validly appreciative,

18
00:01:51,000 --> 00:01:56,000
like something has really been done for their benefit on their behalf,

19
00:01:56,000 --> 00:01:59,000
then they can very quickly receive the benefit of that.

20
00:01:59,000 --> 00:02:08,000
But why I think it's actually more widely applicable than just for ghosts

21
00:02:08,000 --> 00:02:15,000
is that there's no categorical difference between a ghost and a human being.

22
00:02:15,000 --> 00:02:21,000
It's just for more course of a existence, more course of a being.

23
00:02:21,000 --> 00:02:26,000
So the coarseness gets in the way of transferring merit,

24
00:02:26,000 --> 00:02:28,000
gets in the way of the person receiving it.

25
00:02:28,000 --> 00:02:36,000
But if you take the example of simply sending loving kindness to say another human being,

26
00:02:36,000 --> 00:02:47,000
there are many ways more than one way in which you can understand that the person receives the benefit of your loving kindness.

27
00:02:47,000 --> 00:02:51,000
I mean, people will often relate, and this is anecdotal,

28
00:02:51,000 --> 00:02:58,000
that after sending loving kindness, the person on the receiving end suddenly feels happy,

29
00:02:58,000 --> 00:03:00,000
even though they might be at a distance.

30
00:03:00,000 --> 00:03:06,000
So I don't know whether that would even hold in a clinical trial,

31
00:03:06,000 --> 00:03:17,000
like if they were to do a double-blind scientific study as to whether advanced meditators could actually send loving kindness

32
00:03:17,000 --> 00:03:20,000
and if you received in that way. But regardless,

33
00:03:20,000 --> 00:03:28,000
the most secular and rational way of understanding the transference of merit and loving kindness

34
00:03:28,000 --> 00:03:36,000
is the effect that the transferring has on the mind of the person who does the transferring.

35
00:03:36,000 --> 00:03:41,000
So suppose if the person is alive, which is the easiest case,

36
00:03:41,000 --> 00:03:45,000
and you send them loving kindness, or in this case, you dedicate merit to them.

37
00:03:45,000 --> 00:03:49,000
May this be for the benefit of all my relatives, and then you maybe even specify someone.

38
00:03:49,000 --> 00:03:56,000
May this be a great benefit to my mother or my father, who is angry at me and upset at me or so on.

39
00:03:56,000 --> 00:04:01,000
Then that changes the way you think of that person,

40
00:04:01,000 --> 00:04:09,000
which will have visceral and actual and empirical effect on your relationship with them the next time you meet.

41
00:04:09,000 --> 00:04:18,000
You'll find that suddenly they look and seem like a different person based on the changes that have gone on in your mind.

42
00:04:18,000 --> 00:04:28,000
But I would say it potentially goes further than that because every action, of course, has ripple effects in the universe around us.

43
00:04:28,000 --> 00:04:32,000
So whether it actually gets to the person and they feel happy because there's something you've done for them,

44
00:04:32,000 --> 00:04:36,000
it certainly will affect your life even before you meet the person.

45
00:04:36,000 --> 00:04:44,000
Now, after the person has died, it's only an extrapolation to suggest that if there is rebirth

46
00:04:44,000 --> 00:04:54,000
and if you are born based on karma, that you are far more likely to have a positive relationship with that person in the future,

47
00:04:54,000 --> 00:05:01,000
or to work out quicker, more quickly your negative relationship with the person based on sending them merit.

48
00:05:01,000 --> 00:05:17,000
So in a logical sort of secular but rationalist point of view, it's not difficult to see or it makes perfect sense.

49
00:05:17,000 --> 00:05:25,000
It makes more sense to suggest that there is a benefit that comes from dedicating merit to all beings.

50
00:05:25,000 --> 00:05:32,000
So it's goodness to do that, to share the goodness and to wish for other beings to rejoice just the way you register,

51
00:05:32,000 --> 00:05:39,000
because the biggest benefit of doing a good deed is how it makes you feel, is the effect that it has on your mind.

52
00:05:39,000 --> 00:05:51,000
So when you wish for other beings to get the benefit of your goodness, you're not saying, well, they become rich based on the money that I've given or so on.

53
00:05:51,000 --> 00:05:59,000
You're saying may they become generous, may they change their outlook.

54
00:05:59,000 --> 00:06:10,000
So the benefit comes from, according to Buddhism, the idea of them getting the benefit comes from their rejoicing in it,

55
00:06:10,000 --> 00:06:20,000
or comes from the change in their own minds, which can come about in many ways, including how you relate to the person as a result of giving the dedication.

56
00:06:20,000 --> 00:06:31,000
So there's two kinds of merit involved, the merit of giving, which changes your mind and the merit of rejoicing.

57
00:06:31,000 --> 00:06:46,000
There are often mentions made or determinations made when you give merit, you think, well, if the person is listening, if they're here right now as an angel or a ghost or whatever,

58
00:06:46,000 --> 00:07:03,000
may they receive the merit directly. If they're not, may the angels who are hanging around here rejoice in it and on my behalf go and tell that person, wherever that being, wherever they have been born and so on.

59
00:07:03,000 --> 00:07:16,000
They will make that sort of wish or prayer. So the first question, the second question, is it possible to reconnect with the departed person?

60
00:07:16,000 --> 00:07:32,000
Well, obviously not if they've been born as a human or an animal, right, except by meeting them by somehow finding them like the Tibetan llamas and monks seem to be very good, the astrologers tend to be very good at.

61
00:07:32,000 --> 00:07:44,000
I suppose they are a ghost, right, whether ways, you know, psychics are able to enter into states, Buddhist meditators are able to enter into states, where they're able to talk to ghosts.

62
00:07:44,000 --> 00:07:53,000
I mean, lots of meditators have told me that they're able to see and talk to ghosts, so whether it's real or not, I mean, as a Buddhist meditator, it makes sense to me.

63
00:07:53,000 --> 00:08:05,000
But I think on this, in this case, only if there are ghosts or an angel, then more likely if there are ghosts because angels don't tend to have much to do with humans, apparently.

64
00:08:05,000 --> 00:08:18,000
Humans are putrid and have a vile smell. An angel, they say an angel can smell a human being, but a thousand leagues away or something.

65
00:08:18,000 --> 00:08:28,000
They don't have much to do with that. That's why you see ghosts far more often than angels because the angels are quite turned off by theist.

