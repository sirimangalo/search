1
00:00:00,000 --> 00:00:16,520
Okay, so we're here at the Dharmikstupa and just I'll repeat what I would say.

2
00:00:16,520 --> 00:00:24,120
The Dharmiksakapawat in the Sutta, when the Buddha arrived at Isipatana and met the five

3
00:00:24,120 --> 00:00:38,520
monks, the five ascetic. They were skeptical. They said, here comes Gautama, that guy who gave up

4
00:00:38,520 --> 00:00:46,600
the holy life and look at him, he's fat and got his color back. And they said they weren't

5
00:00:46,600 --> 00:00:50,480
going to pay respect to him, but in the end they couldn't help themselves, but they were still

6
00:00:50,480 --> 00:00:57,760
rude until the Buddha asked them, you know, look. Have I ever spoken this way before? Have I ever

7
00:00:57,760 --> 00:01:04,520
claimed omniscience and perfect enlightenment before? Is it? No hate dang bunte, something like

8
00:01:04,520 --> 00:01:12,600
that which is then they used the word bunte, no indeed venerable sir. And then he said,

9
00:01:12,600 --> 00:01:18,320
now listen, I have something to tell you. And I think the legend goes, so that was the thing

10
00:01:18,320 --> 00:01:27,760
where they met him there and he brought them here to teach them, something like that.

11
00:01:27,760 --> 00:01:31,600
And then he taught the Dharmiksakapawat in the Sutta. It's called the Dharmiksakapawat in

12
00:01:31,600 --> 00:01:37,640
the Sutta. What that means, pawatana means the turning. Jakai is a wheel and Dharmi is the

13
00:01:37,640 --> 00:01:42,080
Dharmi. So the Dharmi wheel, the turning of the Dharmi wheel is what it's called. So it

14
00:01:42,080 --> 00:01:46,920
was given that name afterwards of course. It doesn't relate to the actual teaching in the

15
00:01:46,920 --> 00:01:53,200
world. It relates to the significance of it. That this was the moment where a teaching

16
00:01:53,200 --> 00:02:01,960
was actually given that caused someone to become a Sotapamur. For the first time to realize

17
00:02:01,960 --> 00:02:12,840
to have a real release where they became free from suffering. And I think it's fair to

18
00:02:12,840 --> 00:02:18,800
say that Dharmiksakapawat in Sutta is separated into two parts. And I think it's easy to see

19
00:02:18,800 --> 00:02:27,080
that because of who his audience was. And you could also generalize to say it was the

20
00:02:27,080 --> 00:02:34,200
accepted wisdom on the spiritual life the way that was. So it may not have been just

21
00:02:34,200 --> 00:02:39,240
for the monks, for five monks, but it does seem that the actual turning of the Dharmi was

22
00:02:39,240 --> 00:02:44,320
in the second part of the Sotapamur. Because the Buddha begins by talking about the middle

23
00:02:44,320 --> 00:02:50,000
way. And this has come to gain a lot of significance in Buddhism. We talk about the Buddha

24
00:02:50,000 --> 00:02:56,840
teaching the middle way. But honestly, you don't see the Buddha stressing the middle

25
00:02:56,840 --> 00:03:02,120
way that often. So it does appear to be something directed, particularly at this wrong,

26
00:03:02,120 --> 00:03:12,360
in a specific wrong view that because they knew that engaging in sensuality was wrong, then

27
00:03:12,360 --> 00:03:22,080
obviously the complete opposite of seeking out pain is going to be right. I mean, not

28
00:03:22,080 --> 00:03:27,160
actually that obviously, but understandably why you would think that especially if you're

29
00:03:27,160 --> 00:03:31,560
ignorant and unenlightened, which is really a part of the problem. It's quite simplistic

30
00:03:31,560 --> 00:03:39,360
to think, oh, okay. Well, if indulgences is bad, then the opposite should be the way to go.

31
00:03:39,360 --> 00:03:46,400
You see that even in the world, I think you could point to a lot of the not religious,

32
00:03:46,400 --> 00:03:57,840
but secular devoting oneself to work, devoting oneself to some even secular ambition as

33
00:03:57,840 --> 00:04:03,120
a means of directing one's energies. And you end up getting very stressed, but you have

34
00:04:03,120 --> 00:04:09,880
a sense that it's right, right? Business people often do. They are successful. And I think

35
00:04:09,880 --> 00:04:13,720
you could argue that to some extent, there's a lot of other reasons why that's wrong.

36
00:04:13,720 --> 00:04:19,680
But to some extent, that's self torture, where you say, you know, I know being lazy and

37
00:04:19,680 --> 00:04:26,320
indulgent in all these people who just live for sensuality. I know they've got it wrong,

38
00:04:26,320 --> 00:04:31,000
but I've got it right, because look what I'm, you make up all these stories that you tell

39
00:04:31,000 --> 00:04:40,040
yourself about why what you're doing is somehow better. So the religious form of that was

40
00:04:40,040 --> 00:04:44,880
the actual explicit torture that you'll see if you ever watch the movie Little Buddha,

41
00:04:44,880 --> 00:04:50,560
some of you may know have where they actually show some of that. But it's in the scriptures.

42
00:04:50,560 --> 00:04:55,440
This is in the ancient texts where they talk about lying on bed of nails. That was the

43
00:04:55,440 --> 00:05:04,360
common thing, standing on one leg, not eating. And the Buddha tried all of these things.

44
00:05:04,360 --> 00:05:11,160
And that's what he was doing for six years. And what eventually he came to was that he

45
00:05:11,160 --> 00:05:18,040
was, he couldn't go any further without killing himself. He pushed it to the extent

46
00:05:18,040 --> 00:05:25,320
where he said, if I were to continue, not even push hard right on thing, but just if

47
00:05:25,320 --> 00:05:32,480
I don't stop what I'm doing, I will die. I will die. And and that in and of itself

48
00:05:32,480 --> 00:05:40,400
isn't the nail in the final last nail in the coffin, but what really drove at home was

49
00:05:40,400 --> 00:05:51,400
I will die. And to this point, I have gained zero wisdom. I have gone no, not one step

50
00:05:51,400 --> 00:05:58,480
closer to understanding reality or how he would have put it to finding the deathless,

51
00:05:58,480 --> 00:06:07,360
to getting beyond old age sickness and death. That's the real point. And it's something

52
00:06:07,360 --> 00:06:13,920
that I think if you put some thought into it, you can see how a lot of religious practices

53
00:06:13,920 --> 00:06:19,000
like that, even Buddhist practice. So we're often hard in our tradition on people who

54
00:06:19,000 --> 00:06:24,120
practice a lot of summit that. And and I think to some extent, it's a fair criticism

55
00:06:24,120 --> 00:06:29,360
of some people who practice even summit to meditation for a long time. And it's quite

56
00:06:29,360 --> 00:06:36,480
comfortable and pleasant, but don't ever go further than that. And no wisdom comes from

57
00:06:36,480 --> 00:06:44,360
it. If you go even further, of course, and and more glaring is in other religions that

58
00:06:44,360 --> 00:06:51,600
value faith or value ritual. And this was certainly the case in the time of the Buddha,

59
00:06:51,600 --> 00:06:56,400
a lot of people valued their rituals, but didn't gain anything from it. Right? This whole

60
00:06:56,400 --> 00:07:04,400
idea of purification that we would have seen in in Varanasi, purification by bathing in

61
00:07:04,400 --> 00:07:08,920
the river Ganga, bathing. They go all the way up to the source of the Ganga river. And

62
00:07:08,920 --> 00:07:18,480
that would lead to purification. I mean, does it purify? Does it not purify is such a meaningless

63
00:07:18,480 --> 00:07:25,800
question? Because the results, the reality is that you're no less angry, no less greedy,

64
00:07:25,800 --> 00:07:31,680
no less deluded than before you bathed in the river Ganga. It was something I said to

65
00:07:31,680 --> 00:07:39,720
this woman who was asking about Christianity. I said, and it there's a story in the Jataka,

66
00:07:39,720 --> 00:07:46,360
where Sakha, who is basically the Buddhist equivalent of some sort of God, came down and

67
00:07:46,360 --> 00:07:51,280
said, wow, you're really, it was this dark skin. The body said to us, it's called the

68
00:07:51,280 --> 00:07:59,600
Kana Jataka, the black Jataka. So he's a black, skinned man. And Sakha really gets a feeling

69
00:07:59,600 --> 00:08:05,840
that this guy is really practicing quite powerfully. So he comes down and he tries, he's going to

70
00:08:05,840 --> 00:08:12,160
try to shake him a little bit and he says, oh, here's this black man and this is black and I don't

71
00:08:12,160 --> 00:08:18,720
like black and something like that. And basically insulting him that he's black. And his reply is

72
00:08:18,720 --> 00:08:24,080
something to be effective. You know, black is indeed black because of course black and white have

73
00:08:24,080 --> 00:08:30,560
this symbolism. And in fact, nobody has black skin. It looks darker so you call it black. But

74
00:08:30,560 --> 00:08:36,240
it's black is not, of course, in skin, black is indeed. And based on the wisdom, Sakha's like,

75
00:08:36,240 --> 00:08:41,280
okay, that's, you know, you've kind of passed the test and he says, I really appreciate you and I

76
00:08:41,280 --> 00:08:48,960
want to give you a boon, you know, which, what would you wish? And he says, his wish, he says,

77
00:08:48,960 --> 00:08:54,960
I wish that no one should be harmed by me. I wish that I should be have no hatred for any being

78
00:08:54,960 --> 00:09:01,680
and it's quite beautiful if you read the verses. But the point that relates to what we're

79
00:09:01,680 --> 00:09:12,480
talking, what I'm talking about is that the thrust of many of these religions is completely

80
00:09:12,480 --> 00:09:16,720
meaningless and useless. You can't get out of them what's really important. You can ask God

81
00:09:16,720 --> 00:09:22,160
for anything. But the question is, can God give you this? Can God make you so you have no anger?

82
00:09:23,760 --> 00:09:29,120
Some, I think religious people would say that yes, with faith, it helps. But we know as Buddhists

83
00:09:29,120 --> 00:09:35,360
that that's very superficial and kind of just a useful temporary repression of your hatred and

84
00:09:35,360 --> 00:09:42,080
delusion and so on. And that to really free yourself from these has to go deeper. Certainly,

85
00:09:42,080 --> 00:09:47,120
you can't just say, please God, let me not be angry, let me not be lustful. If that were the

86
00:09:47,120 --> 00:09:53,440
case, then all these priests, I bet they do try to pray away their pedophilia. I bet many of

87
00:09:53,440 --> 00:10:00,640
them do. Even though we say these are evil, evil people that they really do many of them feel

88
00:10:00,640 --> 00:10:06,240
guilty because it's very hard to be an evil, evil person and not feel some guilt. Some of them

89
00:10:06,240 --> 00:10:15,280
probably don't they're too far gone. But the prayers don't work. And we see that in also in Buddhist

90
00:10:16,080 --> 00:10:22,720
circles, just praying to the Buddha and doing chanting. Well, well, being a good thing

91
00:10:22,720 --> 00:10:33,520
is too basic and too shallow. So getting back to the suta, the Buddha pointed out this prevalent

92
00:10:33,520 --> 00:10:42,240
dichotomy that absolutely, there were many people who were engaged in base, useless practices

93
00:10:42,240 --> 00:10:51,520
of indulgence and sensuality. And that's wrong. He called it, he no gamo poto chaniko,

94
00:10:52,640 --> 00:11:01,040
this indulgence and sensuality. Heena means inferior, gama relates to the gama. The gama is the village.

95
00:11:01,040 --> 00:11:03,600
So gamo means it's the way of the villagers.

96
00:11:06,720 --> 00:11:14,800
And the third is poto chaniko, poto chaniko means the way of a poto jana. Jana is a person, poto

97
00:11:14,800 --> 00:11:21,680
poto means fall, I think. But it means poto jana is a Buddhist word that I think is just a Buddhist

98
00:11:21,680 --> 00:11:28,000
word. That means someone who is full of defilements, that's how the entomology goes. I'm not

99
00:11:28,000 --> 00:11:32,880
sure what it originally meant, but we use it to mean someone who is full of defilements. A

100
00:11:32,880 --> 00:11:35,120
world link is how it's often translated.

101
00:11:37,680 --> 00:11:42,880
So absolutely, there were people who are doing doing that and it was absolutely Heena, inferior,

102
00:11:42,880 --> 00:11:47,920
and it didn't lead to anything special. Gama, it's just the way of people who don't really have

103
00:11:47,920 --> 00:11:58,480
any sense of right or wrong. In poto chaniko, it's the way of the way of ordinary people. It was not

104
00:11:58,480 --> 00:12:03,520
necessarily evil. It's just, because remember, he's talking to a setting. So he's saying,

105
00:12:03,520 --> 00:12:06,560
he's telling them something they can agree with, like, yeah, this is the way of

106
00:12:07,360 --> 00:12:15,600
worldly people. And then he says, but there's this other way. And that's the torturing of oneself.

107
00:12:15,600 --> 00:12:24,800
Ata de la Matano yoga, yoga meaning getting caught up in or dedicating oneself

108
00:12:25,760 --> 00:12:33,520
to ata means the self, de la Matana, the state of making oneself suffer.

109
00:12:35,360 --> 00:12:40,960
And it's that this also, it's different. The words used are different. Duko

110
00:12:40,960 --> 00:12:46,160
it causes suffering. I mean, that's really the ridiculous part, right? Like, how could this

111
00:12:46,160 --> 00:12:56,720
possibly be useful? The result is explicitly a pain, you know? Duko anario anario, it's not the way

112
00:12:56,720 --> 00:13:01,360
of the Aria. And this would have been a word they'd be familiar with. This idea of an Aria, someone

113
00:13:01,360 --> 00:13:11,120
who is noble. Anattasanhito, useless. Ata means use or meaning or purpose, benefit.

114
00:13:12,160 --> 00:13:18,720
Sanhita means just connected with. Anattasanhita means it's not connected with any benefit.

115
00:13:22,480 --> 00:13:26,320
And he said, these two extremes, someone who has gone forth,

116
00:13:26,320 --> 00:13:37,440
Babaji taina nasevita, should not engage in ata. Duaime, bikouayanta, Babaji taina nasevita.

117
00:13:40,160 --> 00:13:47,360
And one other point that's made explicit here, not sayvita, but it should not be engaged in

118
00:13:47,360 --> 00:13:56,160
or are taken up, is that sometimes people say, well, moderation, right? You hear the middle way

119
00:13:56,160 --> 00:14:02,160
being moderation. In this sense, and really in every, in most senses where the Buddha used, talked

120
00:14:02,160 --> 00:14:09,200
about something called the Majima Pratibhada, the middle way. It didn't mean anything like moderation.

121
00:14:09,200 --> 00:14:16,080
It was absolutely not engaging in either. And so the middle way is like a razor's edge to some

122
00:14:16,080 --> 00:14:22,560
extent. And it's not exactly because it's more like the default, in a sense.

123
00:14:23,440 --> 00:14:27,840
You know, the problem isn't what we aren't capable of. The problem is what we are capable of,

124
00:14:27,840 --> 00:14:33,040
what we are inclined towards. We're inclined in both those directions. And once you give up those

125
00:14:33,040 --> 00:14:38,720
inclinations, what you have is something very pure and very simple, very peaceful.

126
00:14:38,720 --> 00:14:44,880
So that's the first part of the Suddhi. He says, having given up these two,

127
00:14:45,840 --> 00:14:52,640
the Buddha has found the middle way. Majima Pratibhada tatagatina, Adhisam Puddha.

128
00:14:54,880 --> 00:14:59,040
That leads to wisdom that leads to light. You should read the Suddha. Everyone should read it.

129
00:14:59,040 --> 00:15:04,160
It's one of the core Suddha. And then he says, what is that middle way? It's the

130
00:15:04,160 --> 00:15:10,000
hateful, noble path. Now, I think you have to cut it there and say, that's the first part. Because then he

131
00:15:10,000 --> 00:15:19,200
switches gears, in a sense, and turns the wheel of Dhamma. And turning the wheel of Dhamma,

132
00:15:19,200 --> 00:15:25,680
what does that mean? It means teaching the formidable truth. Now, the middle way is the fourth

133
00:15:25,680 --> 00:15:33,440
number of truths, right? So he's going to repeat, he actually repeats himself. But it's the framing of it,

134
00:15:33,440 --> 00:15:39,600
I think. Now he's going to frame his teachings around the formidable truth and emphasize that as

135
00:15:39,600 --> 00:15:45,280
the framework. And that's important, because that is the essence of Buddhism. You'll often hear

136
00:15:45,280 --> 00:15:49,120
that. People say, what are the core teachings of Buddhism in the form of a truth? It really is

137
00:15:49,120 --> 00:15:56,480
true. And I think a problem often lies in how they're taught. You read what the Buddha taught

138
00:15:56,480 --> 00:16:02,080
that famous book. And he says, I think he says, the Buddha taught that life is suffering or the

139
00:16:02,080 --> 00:16:05,040
first noble truth. And that's, of course, not what the first noble truth is.

140
00:16:08,000 --> 00:16:15,280
So the next part of the Suddha, he says, I'm going to forget the Pauli. But he says,

141
00:16:15,280 --> 00:16:23,600
these are, they're these formidable truths. And at the age of thought, oh, no, how does it go?

142
00:16:23,600 --> 00:16:28,960
No, he doesn't even say it like that. I don't think it's I dang bikho, Iki dang do kang

143
00:16:28,960 --> 00:16:37,360
ariyasachan, time bikho a. There is this noble truth of suffering. And so he just launches into

144
00:16:37,360 --> 00:16:43,200
teleformal clues. But what a lot of people don't ever hear and don't ever realize is that it's

145
00:16:43,200 --> 00:16:48,720
actually not a fourfold teaching. But that's not the extent of it. And that's not how he

146
00:16:48,720 --> 00:16:53,920
presents it. I think he's he goes first by telling the personal, the formidable truths.

147
00:16:55,440 --> 00:17:00,880
Right? What is it? This is the, yes, that's right. The first part is the first, the first

148
00:17:00,880 --> 00:17:07,520
noble truth is that jhadipidukha, birth is suffering, old ages suffering, death is suffering,

149
00:17:07,520 --> 00:17:14,240
sorrow, lamentation and despair or suffering. So he outlined some of the ways that we suffer. I mean,

150
00:17:14,240 --> 00:17:24,160
this is simple truths. But what's profound even there is how he's making that the essence of his

151
00:17:24,160 --> 00:17:28,400
religion. Right? It's kind of like a, and that's why people, I think even the Buddha,

152
00:17:28,400 --> 00:17:34,720
refers to himself as a physician because it's kind of like that. It's very clinical.

153
00:17:34,720 --> 00:17:41,280
And it's not to say that other religions don't talk about relieving suffering and finding

154
00:17:41,280 --> 00:17:46,160
happiness. I think, of course, to some extent they do it, that this is the core of

155
00:17:46,160 --> 00:17:53,440
Buddhism. It's not God. It's not faith. It's not ritual or anything. It's suffering.

156
00:17:55,120 --> 00:17:59,040
And he describes what it is. And then he says something that I think is also very profound

157
00:17:59,040 --> 00:18:05,920
that we always have to remember is that at the core, the five aggregates of clinging are suffering.

158
00:18:06,560 --> 00:18:11,200
And that's going to relate to the second noble truth, of course. Second noble truth is the cause

159
00:18:11,200 --> 00:18:18,240
of suffering. What is the cause of suffering? It's done, yayang, tan, tan, bono babika. That craving,

160
00:18:18,240 --> 00:18:24,000
that thirst that leads to further becoming. And that's becoming in terms of being reborn. It's

161
00:18:24,000 --> 00:18:28,720
also, you could say, becoming in terms of just doing things, you know, ambition leads you to

162
00:18:28,720 --> 00:18:34,160
take out a loan to get a job, to start a business or so on, or to get a car or whatever.

163
00:18:34,800 --> 00:18:40,240
Ambition is what leads us to get married, to have kids and so on. It's creating something,

164
00:18:42,640 --> 00:18:46,080
creating entanglement, getting the way that we get caught up in the world,

165
00:18:47,440 --> 00:18:53,440
and build things for ourselves. Because that's the cause of suffering because it leads, of

166
00:18:53,440 --> 00:19:02,320
course, the busyness and entanglement. Mostly it leads to the entanglement of needing to get it,

167
00:19:02,320 --> 00:19:06,880
and then not getting what you want. So where he says, not getting what you want is suffering,

168
00:19:06,880 --> 00:19:15,680
getting what you don't want is suffering, the third noble truth is the cessation of suffering.

169
00:19:17,360 --> 00:19:22,720
So with the cessation of craving, then there comes the cessation of suffering, which is important,

170
00:19:22,720 --> 00:19:28,720
because it's different from saying, here's suffering, and if you can just avoid all those things,

171
00:19:28,720 --> 00:19:32,480
or find a way to not have any of those things, and you don't suffer, and that's not the point of it.

172
00:19:32,480 --> 00:19:38,960
All of those things are suffering because of your craving for them, because of your craving for

173
00:19:38,960 --> 00:19:42,880
not getting them. You're craving for something else, you're craving for things to be a certain way.

174
00:19:42,880 --> 00:19:54,080
The fourth noble truth is, again, the path, and it's basically saying, how do you arrive at

175
00:19:54,080 --> 00:19:58,400
this state of being free from suffering, which is, of course, the full noble path.

176
00:20:04,640 --> 00:20:09,680
But then he goes into what I was getting at this idea that it's more than just a

177
00:20:09,680 --> 00:20:18,720
foreful teaching. He says, the first thing, the first thing that was important about the

178
00:20:18,720 --> 00:20:26,080
dominant the Buddha realized is that there is suffering, or just this understanding of suffering.

179
00:20:28,320 --> 00:20:33,280
But then he says, two more things about it, and so actually it's a 12-fold teaching.

180
00:20:33,280 --> 00:20:39,360
First he says, realization of this truth is an essential part of the path.

181
00:20:41,840 --> 00:20:48,560
The second part is what we call the kitsha. Kitsha just means something you have to do.

182
00:20:48,560 --> 00:20:52,400
It comes from the root kara, which relates to karma, which means an action, right?

183
00:20:52,400 --> 00:20:58,240
So kitsha is something, some action that should be done. So kitsha, what should you do in it

184
00:20:58,240 --> 00:21:02,160
in regards to it? And this is something that many of you have probably heard me talk about,

185
00:21:02,160 --> 00:21:07,600
and something that has to be stressed, that it's not about running away from suffering or escaping

186
00:21:07,600 --> 00:21:10,880
suffering. What are you supposed to do in regards to suffering? You're supposed to see

187
00:21:12,080 --> 00:21:20,720
understand completely. Pari nyaya. Pari nyaya, pari nybana. Pari means like it actually means

188
00:21:20,720 --> 00:21:27,040
like a circle, it's like a round, like fully, you know, from all angles kind of thing.

189
00:21:27,040 --> 00:21:35,680
Yaya just means should be known. So dukas suffering should be known from all angles or known

190
00:21:35,680 --> 00:21:43,760
completely, just understood thoroughly and fully understood. And the third part is, well,

191
00:21:43,760 --> 00:21:53,280
just a transformation of that into having done. So kata is, once you have done the kitsha,

192
00:21:53,280 --> 00:22:00,320
kata means an action that has been performed. So it's done. And that's once it is done,

193
00:22:00,320 --> 00:22:06,160
Pari nyaya, once he had realized that he had understood completely suffering,

194
00:22:07,440 --> 00:22:11,760
then he had done what needs to be done in regards to that. And each of the

195
00:22:11,760 --> 00:22:17,040
four noble truths is set down in this way. So there's the truth itself, kitsha and kata.

196
00:22:17,040 --> 00:22:22,160
If you want to say there's just two things, there's the truth and what you have to do about it.

197
00:22:23,200 --> 00:22:28,000
But knowing those two things, of course, is not enough. The third one, you have to actually have

198
00:22:28,000 --> 00:22:36,160
done it. Which is important if you, you know, we have to criticize, of course, the propensity to

199
00:22:36,160 --> 00:22:45,280
be content with knowing, with studying. You know, I know the four noble truths. And it's remarkable

200
00:22:45,280 --> 00:22:52,320
how often we gloss over the teachings, you know, wanting to hear about the four noble truths.

201
00:22:52,320 --> 00:22:57,360
Oh, I know the four noble truths. Unless you're an arahand you don't really know the four noble

202
00:22:57,360 --> 00:23:02,240
truths. And that's an important point to me. So kata, you have to actually have done the work.

203
00:23:03,680 --> 00:23:11,200
And in regards to suffering, you can't stress enough how important it is to align yourself with

204
00:23:11,200 --> 00:23:20,800
the fact that the kitsha is to know it completely. That's what we do with suffering. And that's

205
00:23:20,800 --> 00:23:27,360
why mindfulness is so essential. What is the function of mindfulness? To confront, not run away,

206
00:23:27,360 --> 00:23:37,280
not try and fix, not get caught up in, to confront. It's called Abimukha. We say Abimukha

207
00:23:37,280 --> 00:23:46,640
Baba. The manifestation of mindfulness is to confront the, our experiences.

208
00:23:49,040 --> 00:23:56,320
The second noble truth, the kitsha, the kitsha, it has to be done, is it has to be abandoned.

209
00:23:57,280 --> 00:24:03,200
And that relates back to the first noble truth. The point being, once you confront experiences

210
00:24:03,200 --> 00:24:14,000
without reacting to them, the craving has no hold. You can't take root because you have a pure

211
00:24:14,000 --> 00:24:20,240
state of consciousness. That's why our focus is on purity. It's on a pure state of mind,

212
00:24:20,240 --> 00:24:26,480
which is momentary. Like right now, having a pure awareness of the feeling of the air around

213
00:24:26,480 --> 00:24:35,200
you and the hard ground, the sounds, the sights, the smells. In those moments, there is a purity.

214
00:24:35,200 --> 00:24:42,800
And we try and build that as a cultivate that as our state of mind. That becomes strong.

215
00:24:43,440 --> 00:24:48,160
There is an inclination, like we were leaning in this direction, suddenly we were leaning in

216
00:24:48,160 --> 00:24:54,000
this direction and we were able to eventually fall in the right direction, which is away from

217
00:24:54,000 --> 00:25:05,760
suffering. The third noble truth, the kitsha is, the third noble truth being the cessation of

218
00:25:05,760 --> 00:25:11,520
suffering. The kitsha, what we have to do is we have to see it for ourselves. Such a katabha.

219
00:25:12,320 --> 00:25:18,960
Such a katabha is an interesting word. I think it means katabha, katabha is again karma,

220
00:25:18,960 --> 00:25:25,280
so you have to make it satachi, which I think means with your own eyes, literally.

221
00:25:26,880 --> 00:25:32,000
And that relates back to this idea of vipassana. Vassana means seeing, so it's this metaphoric or

222
00:25:33,040 --> 00:25:39,600
metaphorical seeing. It's not with your eyes, of course, but it's related to how you would see

223
00:25:39,600 --> 00:25:45,680
something instead of just believing it. It's just emphasizing the fact that you have to experience

224
00:25:45,680 --> 00:25:50,640
it for yourself. But it does drive home that point that there's a difference between

225
00:25:51,280 --> 00:25:57,040
knowing intellectually and having this idea, yeah, I know the formable truth, so look at how it's

226
00:25:57,040 --> 00:26:00,800
freed me from suffering. So I've realized the formable truth. It's just that knowledge.

227
00:26:05,040 --> 00:26:08,240
But the third noble truth is to be realized for yourself.

228
00:26:08,240 --> 00:26:15,760
The fourth noble truth, of course, the kitche is bawaitabha. It should be developed.

229
00:26:16,560 --> 00:26:21,760
So we developed the eight full noble path. It provides another very staple, important

230
00:26:23,120 --> 00:26:26,800
core part of the Buddhist teaching. I mean, it's the fourth noble truth, but it's also

231
00:26:28,000 --> 00:26:34,720
a very beautiful and often cited core description of what Buddhism is.

232
00:26:34,720 --> 00:26:44,720
It covers all the bases in a way. You have right view, right thought, right speech, right action,

233
00:26:45,440 --> 00:26:49,600
right livelihood, right effort, right mindfulness, and right concentration.

234
00:26:51,280 --> 00:26:57,760
And if you put all those together, once they become all well-developed and working in

235
00:26:57,760 --> 00:27:06,400
coordination, working in working together, then you free yourself from suffering.

236
00:27:07,200 --> 00:27:10,160
So that's the second part of the suta. The last thing he says

237
00:27:12,800 --> 00:27:20,560
in the suta is that until he realized the four noble truths with these three parts. So this

238
00:27:20,560 --> 00:27:31,760
12 dua da sakura, the making it into 12, I didn't say I was a Buddha. But once I realized these

239
00:27:33,440 --> 00:27:38,480
four noble truths with their three parts, meaning it actually done what needs to be done in regards

240
00:27:38,480 --> 00:27:44,320
to all four, then I said, among all beings in the world, that I was a Buddha.

241
00:27:44,320 --> 00:27:52,800
And when he said that, all the angels, the rest of the suta is describing how all the angels

242
00:27:52,800 --> 00:27:59,040
responded by saying the Buddha had turned the wheel of dharma that will never be unturned,

243
00:27:59,040 --> 00:28:06,400
cannot be unturned by any dua or brahma or mara. And then that's an important part of the symbolism

244
00:28:06,400 --> 00:28:11,120
of the turning the wheel because it's an inexorable turning. It's not something that you can turn

245
00:28:11,120 --> 00:28:16,640
back up, but the what the young cannot be, should never, could never be turned back.

246
00:28:18,320 --> 00:28:23,680
At that point, Kundanya, when he was, remember these five and the one of them was Kundanya,

247
00:28:23,680 --> 00:28:29,600
who was the one person at the Buddha's birth, who's held up one finger. Everybody else was two

248
00:28:29,600 --> 00:28:35,200
fingers, he held up one finger and one finger and said, he will definitely become a Buddha.

249
00:28:35,200 --> 00:28:42,400
So he actually was more confident than the others, that's kind of the idea. The other thing

250
00:28:42,400 --> 00:28:49,200
about Kundanya is apparently he was him and Subhadha, who were brothers in a past life.

251
00:28:50,320 --> 00:28:55,840
Now, Subhadha was the last person to become enlightened under the Buddha's teaching,

252
00:28:55,840 --> 00:28:59,840
under the Buddha's actual direct guidance. So before the Buddha passed in the Praynebana,

253
00:28:59,840 --> 00:29:06,640
he taught Subhadha. Kundanya was the first person and they in the past life were brothers

254
00:29:06,640 --> 00:29:11,600
and they decided to give a gift to a Buddha or a particular Buddha or something, I think a Buddha

255
00:29:11,600 --> 00:29:18,320
actually. And Kundanya said, let's give the first rice that we harvest, young rice. And these

256
00:29:18,320 --> 00:29:21,920
older brothers said, his other brother said, what are you crazy? Nobody gives that. You have to

257
00:29:21,920 --> 00:29:27,360
wait until the rice is fully developed and then you give that as a gift. But he wanted to give

258
00:29:27,360 --> 00:29:30,480
right away and he said, well, then we'll cut the field in half and you give whatever you want

259
00:29:30,480 --> 00:29:39,840
from yours. And so Kundanya gave young rice to the Buddha and Subhadha waited until it was very

260
00:29:40,560 --> 00:29:46,640
well developed and then he gave the dry them. It was good rice, but it was the fully developed

261
00:29:46,640 --> 00:29:52,640
rice grains, cooked them up, I guess, and gave them to the Buddha. As a result of those two

262
00:29:52,640 --> 00:29:56,800
farmers, one of them was the first person to understand the Buddha's teaching and one of them was the

263
00:29:56,800 --> 00:30:05,120
last. That's the story. But Kundanya, it says in the suit of that, at that moment when the Buddha

264
00:30:05,120 --> 00:30:11,760
was teaching, there are rows in Kundanya, the Dhamma Chaka, you know, the Dhamma Chaka, sorry,

265
00:30:11,760 --> 00:30:15,840
Chaka is not Chaka. Chaka means wheel, Chaka means eye, the eye of Dhamma.

266
00:30:15,840 --> 00:30:28,960
And that's the key point here, the Dhamma, whatever Dhamma arises,

267
00:30:28,960 --> 00:30:34,320
the Dhamma, whatever is of a nature to arise, all of that is of a nature to cease.

268
00:30:35,520 --> 00:30:42,400
That's a description of the realization of Nibana and that's what we call it cessation

269
00:30:42,400 --> 00:30:47,920
because there is an experience that is complete and utter cessation

270
00:30:49,440 --> 00:30:55,920
experience, I think you can say. The orthodox way of explaining it would be complete another

271
00:30:55,920 --> 00:31:03,200
cessation of suffering. And he had that experience and because it's so profound, it's considered to be

272
00:31:03,200 --> 00:31:12,640
the breach or the switch between Putu-jana and then Ariya Pughala. So at that point he was a

273
00:31:12,640 --> 00:31:21,760
Sotapana. And the Buddha looked at him and said, Anyasi Watabokundanya, Anyasi Watabokundanya,

274
00:31:21,760 --> 00:31:31,760
Anyasi, Anyasi means Anyasi. Anyasi, Anyasi, Anyasi, Anyasi, Anyasi,

275
00:31:31,760 --> 00:31:38,800
Anyasi, you know, Anyasi. You know now, you now know, or you have come to know.

276
00:31:38,800 --> 00:31:47,040
Anyanya, you have come to know. And so he was from that point on known as Anyasi, Anyasi,

277
00:31:47,040 --> 00:31:53,440
Anyasi, Anyasi, Anyasi, Anyayana. Anyayana, Anyayana,

278
00:31:53,440 --> 00:31:59,680
Anyayana, Anyayana, who knows. He was given a second name, Anyayana, who knows.

279
00:32:02,960 --> 00:32:07,040
I don't know how much time we have, how much time we're doing on battery, does that thing have a

280
00:32:07,040 --> 00:32:14,880
battery? Careful. 66 percent. Okay, we got lots of it. What time is it?

281
00:32:14,880 --> 00:32:21,360
Ten o'clock. Okay, we're going to be eating at 11, so there's much more to teach,

282
00:32:22,880 --> 00:32:30,560
and I think it all should come now. No, let me see. Right. One more thing that should be taught,

283
00:32:30,560 --> 00:32:34,080
and I won't go into too much detail, because I think we should do some meditation before lunch,

284
00:32:34,080 --> 00:32:49,680
right? Is that Kuntanya became a sotapana, and then he and the Buddha went out for arms,

285
00:32:49,680 --> 00:32:54,960
and they left the other four monks at the monastery to do meditate to practice, and they came back

286
00:32:54,960 --> 00:33:00,400
and brought food for the rest of them, and the Buddha meanwhile taught the others,

287
00:33:00,400 --> 00:33:06,640
and every day he was teaching all of them, until all five of them, day by day, five days later,

288
00:33:06,640 --> 00:33:11,600
became sotapana, and once they were all sotapana, then he taught what he's called,

289
00:33:11,600 --> 00:33:16,960
and that's the luck and the supa, and that means, of course, non-self, and so then he taught,

290
00:33:16,960 --> 00:33:23,920
and tried to help them see this idea that nothing has a core, that our way of looking at things

291
00:33:23,920 --> 00:33:33,280
as being entities, a bag here, a hand, a fist, and so on, is conceptual, and that a much more

292
00:33:33,280 --> 00:33:38,960
real way of looking at things, and the deeper reality is that things don't have a core,

293
00:33:38,960 --> 00:33:47,040
that there is no self, no soul in anything, that reality doesn't admit of such concepts,

294
00:33:47,040 --> 00:33:56,400
that reality is based on experience, and so it's this sort of change in perception, in world

295
00:33:56,400 --> 00:34:04,000
view, in your paradigm, where you start to see things just as being experientially, but I don't

296
00:34:04,000 --> 00:34:10,240
think I will teach the dhamma and the anatallakana supa, and maybe if you guys want we can do it in

297
00:34:10,240 --> 00:34:15,440
the van tomorrow or something, you can talk about it. What I'd like to do rather is,

298
00:34:15,440 --> 00:34:21,680
I mean, basically I give you the synopsis, the five aggregates are non-self, there we go.

299
00:34:21,680 --> 00:34:26,160
Why are they non-self? Because you can't control them, because they don't have any substance

300
00:34:26,160 --> 00:34:37,920
and so on, they're impermanent, and that's all, and I think what we should do now before lunch

301
00:34:37,920 --> 00:34:47,760
is we can do walking, we can do sitting, we can do as you like, and maybe we can do walking around

302
00:34:47,760 --> 00:34:54,720
being, we can go to as you like, that can be a sort of a walking meditation, and then come back

303
00:34:54,720 --> 00:35:04,560
here and do sitting, and I'm going to go for lunch. But this is the place, 2,500 and some years ago,

304
00:35:04,560 --> 00:35:11,680
where the Buddha taught the dhamma chair, and the sky opened up apparently, and you could see hell,

305
00:35:11,680 --> 00:35:17,760
and you could see heaven, and everybody was saying, here's the Buddha, the Buddha has taught,

306
00:35:17,760 --> 00:35:35,200
turned the way of the dhamma. Okay, so you can stop that, and we'll go on to do some meditation.

