1
00:00:00,000 --> 00:00:09,640
Since it's a complicated world to what extension monks and nuns, for example in the years

2
00:00:09,640 --> 00:00:20,000
of formative training, be aware of or learned in secular laws, enough so that they don't

3
00:00:20,000 --> 00:00:35,400
break them, I suppose, I wouldn't do to have monks breaking laws, otherwise I'm trying

4
00:00:35,400 --> 00:00:38,000
to think, what exactly does that mean?

5
00:00:38,000 --> 00:00:40,000
Yeah, that's really it, no?

6
00:00:40,000 --> 00:00:48,400
If you mean it's, should they know that what they're doing is against the law in this

7
00:00:48,400 --> 00:00:53,960
way or that way, then yeah, I think that's quite useful, depending on the law, I mean

8
00:00:53,960 --> 00:01:02,640
the whole copyright laws is maybe an exception, but I don't quote me on that, don't make

9
00:01:02,640 --> 00:01:12,960
a video and put it up on the internet with me saying that, but other than that, I don't

10
00:01:12,960 --> 00:01:19,520
can't think of a reason why they wouldn't need to know the laws.

11
00:01:19,520 --> 00:01:29,440
To my understanding, we should not learn, we should be probably be aware of the laws of a country,

12
00:01:29,440 --> 00:01:39,120
but we should not study it, because it is worldly studies and we should do that, I think

13
00:01:39,120 --> 00:01:54,600
that is so, and I think as a monk or nun, we have the rules, we have the party mocha, and

14
00:01:54,600 --> 00:02:03,240
when we keep it, when we take it serious, we hardly ever can break seriously any law of

15
00:02:03,240 --> 00:02:16,840
a country, I mean there are some laws like the former mentioned, but in general, if you

16
00:02:16,840 --> 00:02:24,920
keep your, if you keep your precepts as a monk, the party mocha, you can't break really

17
00:02:24,920 --> 00:02:39,280
the laws of a country, so there is not really the need to learn, secure, secure laws.

18
00:02:39,280 --> 00:02:49,160
Yeah, I mean the whole idea of premise that somehow the complexity of the world requires

19
00:02:49,160 --> 00:03:00,560
it, it's not really, not in, not in, not a sufficient reason to learn about that complexity.

20
00:03:00,560 --> 00:03:05,280
There's nothing wrong with learning complex subjects if they're inherently useful, like

21
00:03:05,280 --> 00:03:11,720
how to sow a Rome, for example, it's kind of complicated, but there's nothing wrong with

22
00:03:11,720 --> 00:03:18,080
learning, it's a good thing to learn because it, it supports your, you're monastically

23
00:03:18,080 --> 00:03:24,720
learning how to use a computer, for example, can be useful to spread the teaching to people

24
00:03:24,720 --> 00:03:38,800
who use computers, so the complexity is not, not sufficient, but there's nothing wrong

25
00:03:38,800 --> 00:03:43,520
with learning complex subjects to an extent, and maybe you could go even further and say

26
00:03:43,520 --> 00:03:49,600
that because of the complexity, we want to try to avoid that sort of thing as much as

27
00:03:49,600 --> 00:03:50,600
possible.

28
00:03:50,600 --> 00:03:55,120
I mean, hopefully you're not suggesting that monks and nuns should try to make their lives

29
00:03:55,120 --> 00:03:59,080
more complex as the world becomes more complex.

30
00:03:59,080 --> 00:04:02,640
We're trying to give up the world, then we're trying to give up the complexity, so

31
00:04:02,640 --> 00:04:07,280
our lives should be as simple as possible, especially when the world becomes more and

32
00:04:07,280 --> 00:04:13,680
more complicated, I would think that's a reasonable thing.

