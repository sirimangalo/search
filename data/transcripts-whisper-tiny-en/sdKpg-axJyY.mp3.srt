1
00:00:00,000 --> 00:00:10,400
Hi. So this is the fourth in my series on how to meditate. And by now, I've gone through

2
00:00:10,400 --> 00:00:18,800
all of the important points in basic meditation practice. All that's left to do is to explain

3
00:00:18,800 --> 00:00:24,800
a little bit about how to incorporate the Western meditation practice into your daily life

4
00:00:24,800 --> 00:00:31,600
and how to continue it on into the future. There are two important things to keep in mind

5
00:00:32,800 --> 00:00:41,280
when we are practicing meditation on a daily basis. The first one is to keep in mind the four

6
00:00:41,280 --> 00:00:48,720
postures of the body of standing, walking, sitting, and lying down, as well as the minor postures,

7
00:00:48,720 --> 00:00:57,600
bending, stretching, turning, moving, touching, grabbing, raising, chewing, brushing, brushing our

8
00:00:57,600 --> 00:01:04,400
teeth and so on, pushing our hair, going to the washroom, changing our clothes, whatever we do

9
00:01:04,400 --> 00:01:12,240
during the day, to use mindfulness as a means of keeping our level of meditative awareness throughout

10
00:01:12,240 --> 00:01:19,440
the day. So the most important, we take the major postures standing, walking, sitting, and lying

11
00:01:19,440 --> 00:01:27,760
down, and these are also the easiest for the average person to keep in mind to use as meditation

12
00:01:27,760 --> 00:01:32,880
objects. So wherever we are, whatever we're doing, when we're standing, we say to ourselves,

13
00:01:32,880 --> 00:01:39,840
standing, standing, standing, when we're walking, we say walking, walking. When we sit down,

14
00:01:39,840 --> 00:01:46,400
we say to ourselves, sitting, sitting, when we lie down, lying, lying, lying, lying,

15
00:01:46,400 --> 00:01:52,560
and lying meditation, we can use it at night if we have trouble sleeping, or if we have trouble

16
00:01:52,560 --> 00:02:00,720
doing walking or sitting meditation, we can instead use lying meditation and practice rising,

17
00:02:00,720 --> 00:02:05,760
falling, watch the rising and the falling of the abdomen, as well. Or as you can say to ourselves,

18
00:02:05,760 --> 00:02:11,280
lying, lying, lying, lying, standing, walking, sitting, and lying down, these are the four major

19
00:02:11,280 --> 00:02:17,200
postures, which we should be aware of throughout the day. The minor postures, such as bending,

20
00:02:17,200 --> 00:02:23,440
stretching, turning, and so on, we can also be mindful of really keen on the meditation practice.

21
00:02:23,440 --> 00:02:29,600
It can become very important that when we be mindful of every little movement, when we eat our

22
00:02:29,600 --> 00:02:36,240
food, raising, chewing, swallowing, and going, we try to be mindful of everything throughout the day.

23
00:02:37,280 --> 00:02:40,720
This is the first set. The second set of things is called the six senses.

24
00:02:41,680 --> 00:02:46,880
In the West, we're used to five senses, seeing, hearing, smelling, tasting, feeling, and thinking.

25
00:02:46,880 --> 00:02:53,200
In Buddhism, we have six senses. The sixth sense is called thinking. Thinking I've already gone over,

26
00:02:53,200 --> 00:02:58,320
so there's no need to explain it. When we think bad, good, past, future, whatever we're thinking about,

27
00:02:58,320 --> 00:03:03,200
we say to ourselves, thinking, thinking, thinking that the other five, I haven't yet talked about.

28
00:03:03,200 --> 00:03:09,680
So seeing, it happens at the eye. Normally, when we see things, we get absorbed in it, or we get

29
00:03:09,680 --> 00:03:15,840
attracted to it, or we get upset by it. So now we come to change our mind to straighten our

30
00:03:15,840 --> 00:03:22,080
mind out, so that our mind only understands that this is seeing, understands the reality of the

31
00:03:22,080 --> 00:03:27,600
phenomenon. So when we see something, we understand that it's really only seeing, we see to ourselves

32
00:03:27,600 --> 00:03:32,880
seeing, seeing, seeing, and this changes the mind, straightens the mind about the object.

33
00:03:32,880 --> 00:03:38,240
And we hear, we see hearing, hearing, don't just listen to it. We do ourselves hearing, hearing,

34
00:03:38,240 --> 00:03:43,600
just straighten out the mind. And we smell, good smell, bad, mostly smelling, smelling.

35
00:03:43,600 --> 00:03:49,920
And we taste it, sweet, sour, salty, and so on, tasting, tasting. When we feel something on the body

36
00:03:49,920 --> 00:03:56,000
hot, pulled, hard, soft, and so on, we say to ourselves, feeling, feeling, feeling. And when we think,

37
00:03:56,000 --> 00:04:02,320
thinking, as I said, past future good values, I think, so seeing, hearing, smelling, tasting,

38
00:04:02,320 --> 00:04:06,800
feeling, and thinking, and standing, walking, sitting, and lying down. These are the important

39
00:04:06,800 --> 00:04:11,200
things to keep in mind. We can use them throughout the day, and every day, back we should,

40
00:04:11,200 --> 00:04:15,360
we should practice some in the morning, practice, and in the evening, and during the day,

41
00:04:15,360 --> 00:04:20,160
if we're not able to continue practicing, we should still be mindful of these two sets of things.

42
00:04:20,160 --> 00:04:26,400
Another important point in meditation practice is that we have to keep a basic standard of

43
00:04:26,400 --> 00:04:32,080
morality. So for those of you who are interested in practicing, please listen up. I mean, it's

44
00:04:32,080 --> 00:04:37,520
important that we have five rules, which we never break if we intend for our meditation to

45
00:04:38,320 --> 00:04:44,480
succeed and to progress. One that we should not kill, even ants, mosquitoes, kill any living being,

46
00:04:44,480 --> 00:04:50,560
or hurt, or torture any living being around us. Two that we should not steal, do not take what is

47
00:04:50,560 --> 00:04:57,360
not given, what we do not permission to take. Three, we should not commit sexual misconduct,

48
00:04:57,360 --> 00:05:01,840
we should not have romantic involvement with people who already have romantic engagement,

49
00:05:01,840 --> 00:05:06,960
or who are under the protection of their parents or so on. Four, we should not, at a lie,

50
00:05:06,960 --> 00:05:10,960
so should not say things which are untrue with your intention that other people should

51
00:05:10,960 --> 00:05:18,160
misunderstand the truth. And five, we have to stop taking drugs and taking alcohol,

52
00:05:18,160 --> 00:05:22,880
these are things which cause the mind to be unmindful, take our mindfulness away from us,

53
00:05:22,880 --> 00:05:31,360
so we have to give these up as well. This is an essential point, an essential teaching,

54
00:05:31,360 --> 00:05:36,480
which we have to adopt if we intend for our meditation to progress for our minds to become

55
00:05:36,480 --> 00:05:44,800
fixed and calm for a wisdom to arise. So this is everything that is necessary for a basic

56
00:05:44,800 --> 00:05:50,960
meditation practice. Now, if your intention is to have your meditation progress to a higher level,

57
00:05:50,960 --> 00:05:58,880
and have insight and wisdom arise from now, it's important that you first practice,

58
00:05:58,880 --> 00:06:03,680
but also that you find a teacher who can guide you through the practice. So I wish you all

59
00:06:03,680 --> 00:06:08,240
everyone the best in your lives and the best in your meditation practice, and I hope you all

60
00:06:08,240 --> 00:06:15,040
find freedom from suffering. So listening, I thank you for listening, and thanks to everyone for

61
00:06:15,040 --> 00:06:21,520
commenting and for giving their support. Really, it was overwhelming to see how much support

62
00:06:21,520 --> 00:06:36,080
and how much interest there is in meditation, even on the internet. So thank you very much, and have a good day.

