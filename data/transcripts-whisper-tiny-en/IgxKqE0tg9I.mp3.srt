1
00:00:00,000 --> 00:00:18,000
Hello there YouTubers. I just wanted to make a short video today because I recently got a request from someone to provide them with the script for my how to meditate videos.

2
00:00:18,000 --> 00:00:27,000
And of course the videos were inscripted. They were sort of on the fly as you will.

3
00:00:27,000 --> 00:00:48,000
So I thought I'd put this out there as sort of an experimental project in case anyone was ambitious enough to take it on to provide me with a script for at least one of my meditation videos.

4
00:00:48,000 --> 00:01:00,000
If there's anyone out there who is appreciated the videos and thinks they're of some benefit to the world and could be of greater benefit to the world by being translated.

5
00:01:00,000 --> 00:01:16,000
If we could provide a English script for all of the videos to people to anyone who if I could provide a link to all of this to the script for all of the videos, then certainly they could be

6
00:01:16,000 --> 00:01:42,000
they could reach a much wider audience. So if there's anyone out there who would like to do that for one or even several of the videos, I'd suggest that if there's anyone who would like to they can take on one of the videos and provide me with a transcript of what's being said more or less exactly as it was said.

7
00:01:42,000 --> 00:01:49,000
Then I'd really appreciate it and it could be a great benefit to quite a few people.

8
00:01:49,000 --> 00:01:58,000
So please let me know. Probably the best way to get in touch with me is still via email though I don't check it that often or via the website.

9
00:01:58,000 --> 00:02:02,000
So I'll put both of those up here you should see it on the screen.

10
00:02:02,000 --> 00:02:14,000
My email is yutatamo at gmail.com and the contact form on the website is www.ceremungalow.org

11
00:02:14,000 --> 00:02:39,000
So again thanks in advance if there's anyone out there who would like to take this on and I hope to hear from you soon. Have a good day.

