1
00:00:00,000 --> 00:00:16,400
Good evening, everyone, broadcasting live, March 12th.

2
00:00:16,400 --> 00:00:24,160
Today's quote purports to me from Nigeria, but it's either not a direct quote or that's

3
00:00:24,160 --> 00:00:26,160
not the source.

4
00:00:26,160 --> 00:00:30,520
It doesn't really matter, it's a good quote.

5
00:00:30,520 --> 00:00:38,160
Hopefully it is something the Buddha said, but what we do have is a quote from the video

6
00:00:38,160 --> 00:00:52,960
or we do a jatika, we do a pandit and jatika, jatika, haven't really gone into the context of

7
00:00:52,960 --> 00:01:05,320
the quote, but it has to do with Raja was a thing, dwelling with the king, with a

8
00:01:05,320 --> 00:01:18,520
king, a person who is able to hang out with a king because kings can be difficult to deal

9
00:01:18,520 --> 00:01:21,200
with.

10
00:01:21,200 --> 00:01:26,840
So it's an admonishment.

11
00:01:26,840 --> 00:01:30,440
The quote that we have is actually as much more universal.

12
00:01:30,440 --> 00:01:41,480
It says, Ben like a bow and be as blind as bamboo and you will not be at odds with anyone.

13
00:01:41,480 --> 00:01:56,480
So the first part is, it's correct, we have tappo un un daro diro, chappo un daro diro, the wise

14
00:01:56,480 --> 00:02:15,480
bend like a bow, one so a peep, a kampa yay, and indeed like bamboo are supple or flexible.

15
00:02:15,480 --> 00:02:29,760
But delo mang no what the yah, one should not blow against the wind, sara and javasatim was

16
00:02:29,760 --> 00:02:34,760
a, and such a way one can dwell with the king.

17
00:02:34,760 --> 00:02:44,000
So I'm not entirely convinced that the wisdom of the saying, it sounds good, but I'm not

18
00:02:44,000 --> 00:02:51,400
entirely convinced that it's something the Buddha would have endorsed for his monks because

19
00:02:51,400 --> 00:02:55,920
it's retelling a story if this is where it comes from.

20
00:02:55,920 --> 00:03:05,920
It's retelling a story of how to live, how I think a father admonished his son to better

21
00:03:05,920 --> 00:03:14,400
research on it, but how to live with the king.

22
00:03:14,400 --> 00:03:18,320
And so when you live with the king, there are compromises that need to be made and that's

23
00:03:18,320 --> 00:03:23,000
why it might not be the best advice for us as spiritual practitioners.

24
00:03:23,000 --> 00:03:28,960
Now if you have to go live with the king, probably don't blow against the wind.

25
00:03:28,960 --> 00:03:35,360
I just mean to say that there's potential room for blowing against the wind, for standing

26
00:03:35,360 --> 00:03:48,200
strong, that you shouldn't always bend over backwards to try and please others.

27
00:03:48,200 --> 00:03:56,760
But I think there's an argument to be made that in the case of dealing with kings, at

28
00:03:56,760 --> 00:04:00,680
least in a conventional sense this is good advice, but a lot of the javasas are just

29
00:04:00,680 --> 00:04:05,360
that they're conventional advice.

30
00:04:05,360 --> 00:04:13,400
And sometimes it points to the necessity to compromise yourself and your spiritual practice

31
00:04:13,400 --> 00:04:23,680
in order to maintain worldly status and position and even harmony.

32
00:04:23,680 --> 00:04:30,680
A person who lives their life as a monk is going to have a hard time living in society

33
00:04:30,680 --> 00:04:39,720
and not going to, then maybe that's part of the interesting situation I find myself

34
00:04:39,720 --> 00:04:49,080
here in the west with that often gets in conflict with people who think that I'm going

35
00:04:49,080 --> 00:04:58,000
to act as an ordinary person, not that I'm not a person, but as a monk we live by different

36
00:04:58,000 --> 00:05:05,200
set of rules, different set of ideas.

37
00:05:05,200 --> 00:05:14,840
But if you are a rigid, which monks have to be in their practice of the rules, there's

38
00:05:14,840 --> 00:05:23,080
no way I can live with a king, for example, because you'd have to bend over, and be able

39
00:05:23,080 --> 00:05:29,880
to bend to his will and be blind and so on.

40
00:05:29,880 --> 00:05:36,000
But there is a way, of course, by which this verse can be understood as being universal

41
00:05:36,000 --> 00:05:42,720
and that's, in a sense, going with the flow, not purposefully trying to stir things up

42
00:05:42,720 --> 00:05:58,720
or cause problems, being intent upon harmony, samaan veratos, samaan garaati, one who puts

43
00:05:58,720 --> 00:06:04,720
aside one's own desires for the betterment of the group.

44
00:06:04,720 --> 00:06:10,760
There's a story of three monks who dwell together for the reigns and the Buddha asked

45
00:06:10,760 --> 00:06:15,520
them how they managed to live so well together and they've said, we just put aside

46
00:06:15,520 --> 00:06:27,280
what we wanted to do and thought about what it was best for the group or waited to see

47
00:06:27,280 --> 00:06:28,920
what the others wanted to do.

48
00:06:28,920 --> 00:06:51,360
So no one was pushing our obstinate in their own desires and their own intentions.

49
00:06:51,360 --> 00:06:58,800
And this is quite often a problem in religious circles where people are obstinate and

50
00:06:58,800 --> 00:07:08,480
unamenable to change.

51
00:07:08,480 --> 00:07:14,240
So in monasteries you often have a certain result conflict or icing and say with lay

52
00:07:14,240 --> 00:07:23,800
people there's a quote, I think it's from the somewhere in the diptica, the Buddha says

53
00:07:23,800 --> 00:07:34,320
or somebody says that among lay people conflict usually arises based on sensuality.

54
00:07:34,320 --> 00:07:38,600
So everybody wants a two more than one person wanting the same thing and fighting over

55
00:07:38,600 --> 00:07:44,680
and the kids fighting over things, married couples fighting over their needs and their

56
00:07:44,680 --> 00:07:53,160
wants and their friends and co-workers that kind of thing are fighting over and of greed

57
00:07:53,160 --> 00:07:59,320
mostly and of desire for the same things and competition and conflict of desires and that

58
00:07:59,320 --> 00:08:02,440
kind of thing.

59
00:08:02,440 --> 00:08:12,720
With religious people conflict usually arises based on views arguing over views.

60
00:08:12,720 --> 00:08:21,600
So this is how schisms arise, this is how religious conflict comes about and so on.

61
00:08:21,600 --> 00:08:30,880
But even simpler in a monastery among Quiloft and getting an opinion about something

62
00:08:30,880 --> 00:08:42,400
and sort of make you very angry because you have an opinion about other people and you're

63
00:08:42,400 --> 00:08:51,080
not able to tolerate the ways of others, sometimes sometimes simply as a matter of partiality

64
00:08:51,080 --> 00:08:57,520
you believe things should be done a certain way and they're not.

65
00:08:57,520 --> 00:09:07,480
But sometimes with good reasons, sometimes monks are breaking rules and so on.

66
00:09:07,480 --> 00:09:12,640
So in terms of principles I think you have to be careful with this kind of saying it's

67
00:09:12,640 --> 00:09:22,480
not proper to compromise your principles but sometimes you have to compromise your opinions

68
00:09:22,480 --> 00:09:29,400
you have to be willing to be blind and you have to be open-minded and sometimes even not

69
00:09:29,400 --> 00:09:36,440
as far as compromising your principles but as far as being accepting of others who

70
00:09:36,440 --> 00:09:38,200
make different principles.

71
00:09:38,200 --> 00:09:43,400
So monks who break rules for example, it doesn't really help the situation to get angry

72
00:09:43,400 --> 00:09:49,880
at them or to blame them or to scold them, ridicule them, vilify them.

73
00:09:49,880 --> 00:09:58,400
Which often happens, breaking rules also often happens and so it's an example of if we

74
00:09:58,400 --> 00:10:05,200
were to go around vilifying the monks who broke rules or we never get anything done and

75
00:10:05,200 --> 00:10:09,280
we just create more and more conflict, it wouldn't solve the problem so it had to be

76
00:10:09,280 --> 00:10:12,120
more creative than that.

77
00:10:12,120 --> 00:10:20,960
This is where being blind and bendable is quite useful.

78
00:10:20,960 --> 00:10:31,360
So I mean and of course that applies to any situation where you don't want to compromise

79
00:10:31,360 --> 00:10:36,600
your principles but you often have to be quiet about other people's seemingly lack of

80
00:10:36,600 --> 00:10:49,080
principle, seeming lack of principles or their rude behavior.

81
00:10:49,080 --> 00:10:50,400
You have to be wise.

82
00:10:50,400 --> 00:10:53,880
You have to think about what's going to solve the problem.

83
00:10:53,880 --> 00:10:59,120
It's always important to be right, it's not always wise to try and let other people know

84
00:10:59,120 --> 00:11:03,040
that you're right or convince other people that you're right.

85
00:11:03,040 --> 00:11:06,080
Sometimes it is, sometimes it's not.

86
00:11:06,080 --> 00:11:10,440
Mindfulness helps you decide.

87
00:11:10,440 --> 00:11:16,440
As usual mindfulness is the key, if you're mindful you don't have to worry too much about

88
00:11:16,440 --> 00:11:21,760
these things because you naturally bend like bamboo but you don't break.

89
00:11:21,760 --> 00:11:26,840
Bamboo is an interesting plant and it's a useful comparison here because if you ever

90
00:11:26,840 --> 00:11:32,480
tried to break bamboo like this, it doesn't happen, you're more likely to, well it eventually

91
00:11:32,480 --> 00:11:36,600
will break but you'll end up cutting your hands with some results that you've ever tried

92
00:11:36,600 --> 00:11:43,280
to cut a bamboo, snap bamboo, I grew up in Northern Ontario and he broke sticks like

93
00:11:43,280 --> 00:11:44,280
this.

94
00:11:44,280 --> 00:11:49,040
This is how you walk into the forest which broke sticks so when I tried to do that with

95
00:11:49,040 --> 00:11:56,560
a flimsy little piece of bamboo, so bamboo is a good example of bending but not breaking.

96
00:11:56,560 --> 00:12:04,800
That's a useful analogy that in most circumstances you should bend but in most circumstances

97
00:12:04,800 --> 00:12:09,840
I think you shouldn't break which should be compromising yourself for allowing people to

98
00:12:09,840 --> 00:12:12,680
really take advantage of you.

99
00:12:12,680 --> 00:12:20,240
Sometimes you have to give a little, let people go a little ways but to genuinely let people

100
00:12:20,240 --> 00:12:25,120
take advantage of you usually isn't to their bend a bit or your own and letting people

101
00:12:25,120 --> 00:12:33,040
walk all over you is just bad karma for the other person so it's not ideal in any sense.

102
00:12:33,040 --> 00:12:39,720
But there are times where if you're being forced into something or people are forcing

103
00:12:39,720 --> 00:12:46,400
their will upon you, sometimes you have to be patient and bend but don't break.

104
00:12:46,400 --> 00:12:50,320
Don't let it get to you, I think it's another aspect of this.

105
00:12:50,320 --> 00:12:56,320
Don't let other people's problems become your problems.

106
00:12:56,320 --> 00:13:08,920
This is like the Bodhisattva, Bodhisattva when he was reckless and these women came to listen

107
00:13:08,920 --> 00:13:13,960
to him, saw him in the forest and came to talk to him to listen to what he had to say

108
00:13:13,960 --> 00:13:22,520
and he taught them and then it turns out these were like the King's harm and the King

109
00:13:22,520 --> 00:13:28,760
came and saw them sitting at the feet of the Buddha and thought he was seducing his women

110
00:13:28,760 --> 00:13:38,520
and so he got his man to cut off his ears and cut off his hands and cut off his feet and

111
00:13:38,520 --> 00:13:44,280
Bodhisattva kept saying, that's my patience isn't in my feet, my patience isn't in my head

112
00:13:44,280 --> 00:13:53,320
and then he ended up killing him but the Bodhisattva, he got to the point, he actually

113
00:13:53,320 --> 00:14:03,680
didn't kill him outright, he just cut off all his limbs and ears and then the earth swallowed

114
00:14:03,680 --> 00:14:14,040
him up, the King then the Bodhisattva died but he died preaching patience which is an example

115
00:14:14,040 --> 00:14:20,800
of not letting someone else's problem become your problem and he never did, he never got

116
00:14:20,800 --> 00:14:25,040
angry or upset but he had no, you know it's not like he could get out of this, he was

117
00:14:25,040 --> 00:14:35,240
being held down and tortured so not having any way out he was just patient with it, didn't

118
00:14:35,240 --> 00:14:40,840
let it become his problem, that's I think an example we wouldn't normally associate

119
00:14:40,840 --> 00:14:47,040
with bending but not breaking, people thinking he should have struggled or he should

120
00:14:47,040 --> 00:14:54,760
have yelled at the King or but angry or don't do it, how bad it was or so on but I think

121
00:14:54,760 --> 00:15:05,600
it's that's a good example, this idea we shouldn't let it become your problem, anyway

122
00:15:05,600 --> 00:15:30,840
you want to say something, leaving tomorrow, it was the advanced course which was a little

123
00:15:30,840 --> 00:15:37,240
scary because I felt like I barely passed the foundation course, I didn't know what an advanced

124
00:15:37,240 --> 00:15:41,320
course would be and then you let me know that it's actually a review of the foundation

125
00:15:41,320 --> 00:15:47,320
course, so that was good, there's a little more comfortable this time around, it's good,

126
00:15:47,320 --> 00:15:55,360
a little more familiar, the worst is the magic as well, you know you don't know it's

127
00:15:55,360 --> 00:16:02,360
a work point, but the process is yet but you can see there's process everything that you

128
00:16:02,360 --> 00:16:09,060
stand, I don't know what the steps are, I don't know if you understand, but it tends to

129
00:16:09,060 --> 00:16:21,160
feel a little more systematic and of course, you know, that helped, okay, and then you

130
00:16:21,160 --> 00:16:30,720
want to say the group, no, wasn't there something you want to tell him about, you wanted

131
00:16:30,720 --> 00:16:40,160
to mention that you might need a story here, alright, you want to give it to, but I agree,

132
00:16:40,160 --> 00:16:49,160
so I should tell him about it, thank you, it would probably be better for you to get

133
00:16:49,160 --> 00:17:11,400
a description of what you're looking for, so yeah, we're looking for somebody who wants

134
00:17:11,400 --> 00:17:25,800
to come and live here. Why, why, why again? Well, it would definitely help if there

135
00:17:25,800 --> 00:17:30,600
was someone to just kind of take care of all the details, because you as the teacher and

136
00:17:30,600 --> 00:17:36,040
as someone here, you're not really a household, there's a household here, there's the

137
00:17:36,040 --> 00:17:43,120
need for someone to attempt all those details, like taking out the trash, you know, just

138
00:17:43,120 --> 00:17:50,320
general maintenance, cleaning, you know, looking after the meditators, when we have just

139
00:17:50,320 --> 00:17:55,560
little things we've been doing some organization, you're trying to put some labels on like

140
00:17:55,560 --> 00:18:00,680
where different supplies are located, so meditators can find them, but there'll still be,

141
00:18:00,680 --> 00:18:06,320
you know, questions and things and you're not all this here, you're a student. Yeah, I mean,

142
00:18:06,320 --> 00:18:11,000
there's certain things that monks shouldn't be doing, we're kind of circumscribed as to

143
00:18:11,000 --> 00:18:17,960
what we can do for laypeople. We're not allowed to work for laypeople and so having meditators

144
00:18:17,960 --> 00:18:22,880
puts us in a bit of an interesting situation because, well, on the one hand, they're very

145
00:18:22,880 --> 00:18:29,880
much like monks, but on the other hand, they're still laypeople and it shows, and as a result,

146
00:18:29,880 --> 00:18:36,200
we can't get too close. So yeah, taking out trash, that kind of thing and I could take

147
00:18:36,200 --> 00:18:41,080
out trash, but to do it for laypeople, you know, take out laypeople's trash, starts to get

148
00:18:41,080 --> 00:18:47,000
a little bit, you know, too close. So it would be good if, you know, meditators could do these

149
00:18:47,000 --> 00:18:52,520
kinds of things. Absolutely, it would help greatly, I think, yes, that's why I agree to

150
00:18:52,520 --> 00:19:00,480
it is because it would help greatly to have someone here to arrange many things. You know,

151
00:19:00,480 --> 00:19:13,680
we have issues around food. Everybody wants to come on and ask questions while we're talking.

152
00:19:13,680 --> 00:19:20,640
Be nice if someone could be here to make sure there's the food situation is going, okay,

153
00:19:20,640 --> 00:19:33,000
and bedding, and water. I mean, I can do some of these things, but not all of them.

154
00:19:33,000 --> 00:19:40,400
And yeah, it would just be good to have an extra pair of hands.

155
00:19:40,400 --> 00:19:45,040
Those little things, like the doorbell rings during the day, you know, the phone as well,

156
00:19:45,040 --> 00:19:46,040
and I'm not here.

157
00:19:46,040 --> 00:19:51,720
The doorbells ringing in your school meditators are trying to meditate it, just an extra pair

158
00:19:51,720 --> 00:19:53,560
of useful hands.

159
00:19:53,560 --> 00:19:55,560
Yeah.

160
00:19:55,560 --> 00:20:04,520
So if there's anybody out there who'd like to come and stay with us for an extended period

161
00:20:04,520 --> 00:20:14,600
of time, I think we could host one person on an extended basis. Now it would be an extensive

162
00:20:14,600 --> 00:20:22,120
bedding process, and you'd probably have to prove yourself as a meditator at least on

163
00:20:22,120 --> 00:20:31,400
a basic level. That's a meditator in our tradition, and that would sort of prove your sanity

164
00:20:31,400 --> 00:20:38,760
and I don't know what else, but I mean, it's not going to be rigorous, it's not like

165
00:20:38,760 --> 00:20:45,760
we're going to pass a test turning, but I just want to make sure that you're a good fit.

166
00:20:45,760 --> 00:20:51,760
I'm going to have to prove yourself as a meditator was on a basic level.

167
00:20:51,760 --> 00:20:55,960
So you got to turn off YouTube, you come on the hangout, you got to turn off the YouTube

168
00:20:55,960 --> 00:20:56,960
stream.

169
00:20:56,960 --> 00:20:57,960
OK.

170
00:20:57,960 --> 00:21:02,560
Never stream, you got open. Oh.

171
00:21:02,560 --> 00:21:04,560
You can lift.

172
00:21:04,560 --> 00:21:05,760
Turn off the wrong stream.

173
00:21:05,760 --> 00:21:08,760
That's easy enough to do.

174
00:21:08,760 --> 00:21:16,760
If you're listening to the audio or watching YouTube video, you have to turn it off

175
00:21:16,760 --> 00:21:18,760
before you come here.

176
00:21:18,760 --> 00:21:25,760
I don't think Tom has questions.

177
00:21:25,760 --> 00:21:39,760
Tom's just one of the people who comes on.

178
00:21:39,760 --> 00:21:59,760
So no questions tonight.

179
00:21:59,760 --> 00:22:00,760
All right.

180
00:22:00,760 --> 00:22:02,760
And have a good night, everyone.

181
00:22:02,760 --> 00:22:05,760
Thank you all for tuning in.

182
00:22:05,760 --> 00:22:10,760
See you tomorrow.

