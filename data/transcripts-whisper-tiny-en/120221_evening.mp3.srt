1
00:00:00,000 --> 00:00:16,320
Okay, so we're continuing on with list of one things.

2
00:00:16,320 --> 00:00:21,440
List of one and good because it's more about the teaching than about the list.

3
00:00:21,440 --> 00:00:32,040
You can focus on the meaning behind the teaching.

4
00:00:32,040 --> 00:00:33,840
It's actually in pairs most of this.

5
00:00:33,840 --> 00:00:43,320
So here we have a group of pairs where the Buddha is comparing things that are beneficial

6
00:00:43,320 --> 00:00:44,640
and things that are harmful.

7
00:00:44,640 --> 00:00:52,640
I think it's a fairly graphic example here.

8
00:00:52,640 --> 00:01:09,720
He says when you, I won't, the poly is too long, but he says, if you set up a straw, and

9
00:01:09,720 --> 00:01:14,560
you set it up in the wrong way, you set it up.

10
00:01:14,560 --> 00:01:20,080
Sign way, for example, then when you step on it, it won't cut your feet.

11
00:01:20,080 --> 00:01:21,080
Why is that?

12
00:01:21,080 --> 00:01:25,320
Because you didn't set it up upright.

13
00:01:25,320 --> 00:01:35,640
When you grab, you know, when you set it up in the wrong way.

14
00:01:35,640 --> 00:01:40,160
But if you set it up straight, then it can pierce the hand.

15
00:01:40,160 --> 00:01:44,760
So if you've ever been in a barn, you know how it is.

16
00:01:44,760 --> 00:02:02,760
Sometimes when you walk on the straw, it sticks into your feet, when it draws in blood.

17
00:02:02,760 --> 00:02:07,000
So it won't pierce into your feet if it's not sticking up, if it's not set up in the

18
00:02:07,000 --> 00:02:21,480
right way, and he says in the same way, this example he uses to compare actually to the

19
00:02:21,480 --> 00:02:22,480
opposite.

20
00:02:22,480 --> 00:02:27,480
So he's saying, it won't hurt your feet, but we think that's a good thing.

21
00:02:27,480 --> 00:02:33,480
But there's a kind of piercing that you can't do, and that you shouldn't do, and you

22
00:02:33,480 --> 00:02:41,280
can't do if you set it up in the wrong way.

23
00:02:41,280 --> 00:02:46,360
And that is, if your mind is set up in the wrong way, if your mind isn't upright, if your

24
00:02:46,360 --> 00:02:58,640
mind isn't sharp, then you put this as that person whose mind isn't set up properly.

25
00:02:58,640 --> 00:03:06,640
Deep chapani, he, day, night, with a mind that is set up in the wrong way, that they

26
00:03:06,640 --> 00:03:17,880
should pierce through ignorance and reach, nacence or knowledge, and realize Nibana for

27
00:03:17,880 --> 00:03:19,480
themselves.

28
00:03:19,480 --> 00:03:21,480
This is not possible.

29
00:03:21,480 --> 00:03:22,480
Why?

30
00:03:22,480 --> 00:03:27,160
Because they didn't set their mind up properly.

31
00:03:27,160 --> 00:03:34,360
And then he says in the same, in the opposite way, if you set up the straw, like a booby

32
00:03:34,360 --> 00:03:41,800
trap, I guess, and when someone steps on it, it pierces their feet, or if the straw is

33
00:03:41,800 --> 00:03:42,800
upright.

34
00:03:42,800 --> 00:03:50,240
If you have a cut and grass, they come from, we have these big fields or hay fields,

35
00:03:50,240 --> 00:03:53,720
and when they cut them, it looks very nice to walk through.

36
00:03:53,720 --> 00:04:01,120
So we'll walk through them barefoot, and it appears it actually does cut your feet.

37
00:04:01,120 --> 00:04:06,760
So in the Buddha's time, they would be walking through these fields, very graphic.

38
00:04:06,760 --> 00:04:09,080
For now, for us, it seems kind of silly.

39
00:04:09,080 --> 00:04:14,600
We don't have any idea, any concept of this, but if you grew up in the countryside, when

40
00:04:14,600 --> 00:04:21,960
they cut their hay, when they cut their hay or the straw, when you walk through it, very

41
00:04:21,960 --> 00:04:22,960
painful.

42
00:04:22,960 --> 00:04:28,400
Even when they cut the grass, when they cut the lawn, sometimes after many days of not cutting

43
00:04:28,400 --> 00:04:35,320
it, pain and grass goes really high, and when you cut it, it was very painful on the

44
00:04:35,320 --> 00:04:36,320
feet.

45
00:04:36,320 --> 00:04:41,520
And so the monks would be walking through these fields daily after people cut them.

46
00:04:41,520 --> 00:04:48,480
So just in the same way as it's piercing your feet, this is how the mind pierces through

47
00:04:48,480 --> 00:04:54,560
the grass, the mind that if you set the mind up straight, it will be able to pierce

48
00:04:54,560 --> 00:05:02,000
through it and realize, nays things.

49
00:05:02,000 --> 00:05:09,680
You'll be able to draw knowledge, because our mind is clouded, it's covered by this layer

50
00:05:09,680 --> 00:05:18,040
of ignorance, which stops us from seeing things clearly, and until you penetrate that,

51
00:05:18,040 --> 00:05:28,520
you can see in the meditation as you practice, if you pierce through and you understand

52
00:05:28,520 --> 00:05:36,440
what's going on in your mind, you understand why you're feeling, why you feel suffering

53
00:05:36,440 --> 00:05:44,920
and why you have attachment and so on, why you have stress and why you have suffering.

54
00:05:44,920 --> 00:05:50,880
So you realize the truth, you realize what's really going on inside.

55
00:05:50,880 --> 00:05:57,080
And you see how actually, how close to you this knowledge is, it's not something far

56
00:05:57,080 --> 00:06:01,200
away, it's not something you have to look up in a book, it's actually something that's

57
00:06:01,200 --> 00:06:04,640
quite obvious.

58
00:06:04,640 --> 00:06:13,080
And so you start to think how amazing it is that you weren't able to see this, but the

59
00:06:13,080 --> 00:06:17,200
reason you can't see it is because of the mind is set up in the wrong way, the mind

60
00:06:17,200 --> 00:06:24,040
isn't sharp enough to penetrate through the internet, and so it chases after delusion,

61
00:06:24,040 --> 00:06:42,560
it chases after illusion, and he goes on to say that in fact, it's such a wrongly

62
00:06:42,560 --> 00:06:50,080
a mind that is set up wrongly is so dangerous, that it's the mind that is set up wrongly

63
00:06:50,080 --> 00:06:57,240
that leads a person to hell, and the mind that's set up properly that leads a person

64
00:06:57,240 --> 00:07:10,480
to heaven, the mind is so central in our future, in our happiness, in our suffering,

65
00:07:10,480 --> 00:07:14,520
so we would come to realize in the meditation that actually it's not the body that's

66
00:07:14,520 --> 00:07:25,200
causing us suffering, it's not the things around us that are causing us suffering.

67
00:07:25,200 --> 00:07:37,960
We have a real change of the way we look at things, the idea that the mosquitoes can

68
00:07:37,960 --> 00:07:46,720
cause us suffering, or the leeches, or spiders, or snakes, or so on, they're so strong

69
00:07:46,720 --> 00:07:54,760
with us, the idea that other people can cause us suffering is so clear in our minds as

70
00:07:54,760 --> 00:08:03,720
being the truth, it's quite a change for us to see that actually it's our own minds

71
00:08:03,720 --> 00:08:09,880
that's causing us suffering, all of the pain that we feel in the body and all of the suffering

72
00:08:09,880 --> 00:08:14,720
that comes from the world around us, it's only really because of suffering when we take

73
00:08:14,720 --> 00:08:21,640
it up in the mind, take it up in the wrong way, another place that the Buddha says when

74
00:08:21,640 --> 00:08:28,480
you hold grass in your hand, by the blades, for example, that it can cut the hand, but

75
00:08:28,480 --> 00:08:32,840
if you grab it by the shaft, if you grab the grass in the right way, then it doesn't

76
00:08:32,840 --> 00:08:43,360
cut your hand, so when you grasp an experience, as it is, when you experience even right

77
00:08:43,360 --> 00:08:48,960
now sitting here, grasp it as it is, it can cause you suffering, even the hard floor,

78
00:08:48,960 --> 00:08:56,000
even the mosquitoes, and so on, of course, the mosquitoes you have to be careful, they

79
00:08:56,000 --> 00:09:04,520
carry diseases, not to say that you have to grasp it correctly, so that even when you're

80
00:09:04,520 --> 00:09:13,480
swatting the mosquitoes you're doing in mind for you, before people practice if they were

81
00:09:13,480 --> 00:09:21,520
not learned in the precepts or in the importance of compassion and kindness, they would

82
00:09:21,520 --> 00:09:23,720
be killing the mosquitoes.

83
00:09:23,720 --> 00:09:27,280
So this is often a reflex, you want to kill the mosquitoes, if you're not careful you

84
00:09:27,280 --> 00:09:36,840
can go back and just quickly kill the mosquitoes without thinking, because the way the

85
00:09:36,840 --> 00:09:52,800
mind is set up, and this mind can lead us to hell, the mind can lead us to heaven.

86
00:09:52,800 --> 00:09:58,160
One more thing he says that I think is useful, as he talks about the clarity of the mind.

87
00:09:58,160 --> 00:10:12,160
He says, just like a lake, just like a pond that is stirred up, suppose you have a pond,

88
00:10:12,160 --> 00:10:21,880
you take a stick, and you stir it up, or you may be walked through it, and the Buddha

89
00:10:21,880 --> 00:10:27,960
says it becomes money, it's not just stirred up, but it's money, like the puddles after

90
00:10:27,960 --> 00:10:35,600
the rain, the mud, when you walk through them, the mud, but when you leave them alone

91
00:10:35,600 --> 00:10:41,840
for a while they become clear and clear, and you can see inside of them.

92
00:10:41,840 --> 00:10:46,600
So he says, in this pool, when it's all muddy up you can't see anything, you can't

93
00:10:46,600 --> 00:10:51,800
see the fishes, you can't see them on a puddle anymore, but a big lake or so on.

94
00:10:51,800 --> 00:10:58,760
You can't see the fish, you can't see the animals, the shells, and so on.

95
00:10:58,760 --> 00:11:07,400
But when the pond is settled, and the water is clear, then you can see what's in it.

96
00:11:07,400 --> 00:11:18,120
So we have to think of this, and this is in the way of the mind as well, and we're coming

97
00:11:18,120 --> 00:11:31,600
here to clear our minds, to make our minds clear, to make our minds calm.

98
00:11:31,600 --> 00:11:36,640
But it's a common way of clarity, and the important thing is that you can see clearly, when

99
00:11:36,640 --> 00:11:43,680
your mind is calmed, it should be calm based on the experience.

100
00:11:43,680 --> 00:11:47,960
So another way is to just avoid the pond and go somewhere else, so you can't see anything

101
00:11:47,960 --> 00:11:48,960
here.

102
00:11:48,960 --> 00:11:49,960
Some go look somewhere else.

103
00:11:49,960 --> 00:11:54,360
In meditation, this is often the case people want to calm, but they don't care so much

104
00:11:54,360 --> 00:11:57,960
about the clarity, because clarity is quite difficult.

105
00:11:57,960 --> 00:12:04,080
You have to wait, and you have to focus on the mud, and actually wait for the pond to

106
00:12:04,080 --> 00:12:05,080
become clear.

107
00:12:05,080 --> 00:12:11,200
You can just want calm, you can avoid it and run away, and feel peace and calm thinking

108
00:12:11,200 --> 00:12:12,680
about something else.

109
00:12:12,680 --> 00:12:20,960
This is why people, we choose, always the easy way, sometimes people think drugs or medication,

110
00:12:20,960 --> 00:12:31,480
but a Hindu, for example, is become addicted to tymonol, because he has some problems in

111
00:12:31,480 --> 00:12:38,880
his mind, his mind is not settled in, so he gets headaches, and so he sent me some tymonol,

112
00:12:38,880 --> 00:12:46,880
and I said to him, this is what you are experiencing now, this is tymonol, is this good?

113
00:12:46,880 --> 00:12:53,000
No, this is not good, no, this is from tymonol, this is what that is, this is the easy

114
00:12:53,000 --> 00:13:02,080
way, this is what the easy way brings you.

115
00:13:02,080 --> 00:13:07,520
So the calm that we are looking for is a calm with clarity, it has to go together, you

116
00:13:07,520 --> 00:13:14,520
can't just want calm and peace otherwise, there is many ways to attain that, but it becomes

117
00:13:14,520 --> 00:13:20,680
attached, it becomes associated with clinging, you become partial to that state.

118
00:13:20,680 --> 00:13:27,720
The idea is to become calm in regards to the problems, to see the problems clearly as they

119
00:13:27,720 --> 00:13:28,720
are.

120
00:13:28,720 --> 00:13:34,520
When you have a problem with a person, when you have a problem with a place, you have a problem

121
00:13:34,520 --> 00:13:40,480
with a thing, you have problems with yourself, with your own body, with your own mind,

122
00:13:40,480 --> 00:13:47,560
they become calm in regards to them, to become, to understand them, what is going on now,

123
00:13:47,560 --> 00:13:55,400
to see them, to see them, to what they are, and to live with them, when you can calm your

124
00:13:55,400 --> 00:14:00,760
mind in regards to reality, then you can see clearly, you can see what is going on in your

125
00:14:00,760 --> 00:14:13,280
mind, and how the mind works for a moment to moment, and then you can penetrate, in this

126
00:14:13,280 --> 00:14:20,680
way, you penetrate, and through a deep deep, and you can think of the mud as being like

127
00:14:20,680 --> 00:14:26,120
the ignorance that comes from stirring up the mind, all of the ignorance that we have

128
00:14:26,120 --> 00:14:31,320
or all of the delusion, let's say the delusion that we have, is because our mind is constantly

129
00:14:31,320 --> 00:14:39,920
being stirred up, constantly being perturbed by external and internal factors, externally

130
00:14:39,920 --> 00:14:45,440
we have other people and places and things stirring our mind up, internally we have the

131
00:14:45,440 --> 00:14:58,560
environment stirring our mind up, this is why I talked about Mara, Mara can only be a problem

132
00:14:58,560 --> 00:15:04,160
for you, if you fall for his trap, Mara is like the fisherman, and he's got his hook,

133
00:15:04,160 --> 00:15:11,720
but the only reason the fish gets caught on the hook is because it takes the bait, then

134
00:15:11,720 --> 00:15:22,680
Buddha talked about Mara's bait, he attracts us in so many ways, and he comes to realize

135
00:15:22,680 --> 00:15:28,280
this in meditation, you realize that nothing can cause me suffering, there's nothing that

136
00:15:28,280 --> 00:15:33,920
can really hurt me, I'm doing this to myself, going again and again, chasing again and

137
00:15:33,920 --> 00:15:53,520
again after this bait and getting caught by the hook, so our practice is to see through

138
00:15:53,520 --> 00:16:04,560
this and to be clear in our minds in regards to Mara's bait, in regards to the suffering

139
00:16:04,560 --> 00:16:12,880
and what is the cause of suffering, to understand clearly and to be fully alert and

140
00:16:12,880 --> 00:16:24,080
aware of the experience in front of us, so just a little bit more food for thought, we can

141
00:16:24,080 --> 00:16:31,400
think about what we're doing as being the correct grasping of something, there's some sharp

142
00:16:31,400 --> 00:16:40,360
object piercing through the skin of ignorance and drying the blood of wisdom, it's very,

143
00:16:40,360 --> 00:16:46,960
very, very strong simile, the idea of piercing and drying blood, the thing related to piercing

144
00:16:46,960 --> 00:16:52,520
through ignorance, but again it was something that was quite visceral for them stepping

145
00:16:52,520 --> 00:17:00,880
on these straws walking through the fields for arms round, and then we have the muddy

146
00:17:00,880 --> 00:17:07,120
water, from minds like muddy water and we're trying to clear them, and there's the

147
00:17:07,120 --> 00:17:11,640
bullets and this is what leads to happiness here and happiness in the future, it's the

148
00:17:11,640 --> 00:17:19,800
muddy water and the cloud of delusion and ignorance that can send us to hell even here

149
00:17:19,800 --> 00:17:27,040
and now can cause us to do and say and think things that cause suffering for us for a

150
00:17:27,040 --> 00:17:38,840
long, long time, some things to think about, more on the mind, when you talk about one

151
00:17:38,840 --> 00:17:43,320
thing, well it's the most important, one thing is definitely the mind, so this is where

152
00:17:43,320 --> 00:17:48,360
we start in the angry, friendly kind, but the mind is the most important in our meditation

153
00:17:48,360 --> 00:17:56,920
as well, obviously, as I've now said far too many times, it should be, yeah,

154
00:17:56,920 --> 00:18:03,640
well established in your mind that, in your thoughts, that the mind is what we're focusing

155
00:18:03,640 --> 00:18:05,840
on.

156
00:18:05,840 --> 00:18:09,520
One thing that's interesting that I was saying today to a meditator on the internet

157
00:18:09,520 --> 00:18:16,800
to teaching, he was saying it's nice to be focusing on the mind as well because normally

158
00:18:16,800 --> 00:18:23,160
he only focuses on the breath and so now I was explaining him to focus on the mind as

159
00:18:23,160 --> 00:18:30,160
well and the hindrances, the emotions in the mind and he said it's good to notice them

160
00:18:30,160 --> 00:18:35,520
and I said, but it's okay really to focus on the stomach, sorry on the breath because he's

161
00:18:35,520 --> 00:18:41,880
doing breath meditation because this is like when you go into the forest, the hunter goes

162
00:18:41,880 --> 00:18:47,440
into the forest looking for deer, he doesn't go running around the forest looking for

163
00:18:47,440 --> 00:18:52,880
the deer, he goes and sits by the water hole because he knows that's where the deer have

164
00:18:52,880 --> 00:19:00,320
to come, and the same way when we practice, when we're focusing on the stomach or focusing

165
00:19:00,320 --> 00:19:04,240
on the breath, even though there's nothing special about these things, we're going to see

166
00:19:04,240 --> 00:19:08,640
how the mind works, so when we're talking about the mind being the most important thing,

167
00:19:08,640 --> 00:19:12,560
it doesn't mean we have to go looking where's my mind where's my mind and try to find

168
00:19:12,560 --> 00:19:19,680
it, we still focus on the body actually, even our intent is to change the mind, we focus

169
00:19:19,680 --> 00:19:26,480
on the body, because when we focus on the body that means our mind is on the body and

170
00:19:26,480 --> 00:19:35,840
it's this indirect observation of ourselves really, watching how we work, give us some

171
00:19:35,840 --> 00:19:43,680
task like they do in studies and sociology, maybe they do it, psychology, they put you

172
00:19:43,680 --> 00:19:52,080
in some situation and see how you react, see how you interact with the situation, so we're

173
00:19:52,080 --> 00:19:57,200
going to see how we interact with this very simple object and we find that it's quite interesting

174
00:19:57,200 --> 00:20:02,560
how we interact, how the mind works, walking back and forth, we start to wonder why am I walking

175
00:20:02,560 --> 00:20:08,800
back and forth, watching the breath, we start to try to control the breath, make it always smooth

176
00:20:08,800 --> 00:20:16,720
and make it always the same, make it always comfortable and we get fed up and frustrated when

177
00:20:16,720 --> 00:20:23,040
it's not the way we want and we become attached when it is the way we want and then go up and

178
00:20:23,040 --> 00:20:31,600
down, going in bad and so on, so even though we're focusing on the mind, we use the body as our

179
00:20:31,600 --> 00:20:37,280
tool to see how the mind works and this is important to understand that this is what's

180
00:20:37,280 --> 00:20:43,280
the meaning of all of these emotions and thoughts that come up when we practice, when you practice

181
00:20:43,280 --> 00:20:47,760
you start to think it's quite difficult and there's a lot of problems because this isn't right

182
00:20:47,760 --> 00:20:52,400
and that isn't right, all of that what's going on in the mind is what we're looking for,

183
00:20:52,400 --> 00:20:59,520
we're trying to understand this, we're trying to see what is causing us to suffer,

184
00:20:59,520 --> 00:21:09,440
which states of mind, which reactions are causing trouble for us, so we have to realize that it's

185
00:21:09,440 --> 00:21:12,880
not actually the body that's causing the problem, it's the mind that's causing the problem,

186
00:21:14,800 --> 00:21:19,280
to realize that the body, the changing in the body sometimes smooth, sometimes rough and so

187
00:21:19,280 --> 00:21:30,800
it's just the way it is, it's nature, there's nothing good or bad about any physical experience

188
00:21:32,160 --> 00:21:36,480
it's only when the mind picks it up and says this is me, this is mine, this is good, this is bad,

189
00:21:37,760 --> 00:21:45,920
that there becomes a problem, so mind is most important, what we do is use the body

190
00:21:45,920 --> 00:21:53,360
to come to understand the mind, this is our practice, so that's enough talking, now we can go on

191
00:21:54,480 --> 00:22:02,400
to actual practice, I guess we can go up to the roof and then maybe as long as it's not raining,

192
00:22:02,400 --> 00:22:18,320
we can meet up there.

