Hello and welcome back to our study in the Dhamupanda.
Today we continue on with verse number 110, which is as follows.
born to wasa satang energy with dus i lo assammo
divigang ji witang say yo
the oldpages
ish addang resa
empires
whoever should live
100 years
Unemoral and unconcentrated, unfocused, uncentered, unbalanced in mind.
A kahang jibitang say, one life, one day, a life of one day is greater.
See the one that's a giant, or for one who has morality, or is ethical, and who meditates.
So this verse was taught you with the story of Sankitya Samanera as the instigation.
But our story starts with a group of 30 monks, who, 30 men, living in sawati, who heard the Buddha teach the dhamma and decided to go forth.
There's this funny expression called urang dhamme, I think dhamme urang dhatua.
And the English translates it as yielded the breast, yielded the breast to his teaching.
I don't know who thought that would be a good translation of sassanera dhatua.
So having heard the teaching, they yielded the breast.
I don't know, maybe a hundred years ago these translations are a hundred years old, maybe a hundred years ago they thought that.
Maybe that was an expression a hundred years ago.
Sassanera in the sassanera, or to the sassanera, maybe to the teaching of the Buddha.
Urang is the chest.
But it can mean, we would say heart, not dhatua means having given.
So they gave their hearts, they gave up their hearts, or they gave themselves.
He yielded the breast.
It mean that it is kind of poetic.
Just don't never heard it before.
And so they became monks.
They started men and they went to the teacher and asked him,
what are we supposed to do in this religion?
And the Buddha taught us, he always did.
There are two aspects of the teaching.
There's the study aspect you have to learn.
And then there's the practical aspect, the insight, the practice of attaining insight.
And these guys were actually a little bit older when they were ordained.
And so they decided that it would probably be too difficult for them to undertake both duties.
So they requested permission to practice, sweep us in the insight meditation.
And so they went off, they prepared to head off into the forest.
And so they went to the Buddha and they asked permission to leave.
After they were five years of monk after being five years of monk,
they all were full-fledged monks who were independent of the teacher.
And so they, I mean, technically independent.
And so they were allowed to go off into the forest.
So they went to ask permission or to let the Buddha know.
And the Buddha looked at them and said, it's going to be thought to himself.
Is there going to be any trouble for them?
And with his profound insight, we saw there's going to be a danger.
There's going to be danger to these monks.
Going to be danger to these monks in regards to,
and here they translate an eater of broken meat.
I'm sure what that means.
Something broken, something, someone who eats leftovers,
maybe you could say, eat scraps, a scrap eater maybe.
It's going to be danger, danger, because there's danger in the forest.
Bandits and monsters and demons pretending to be cows.
There's lots of different things.
And we had yesterday we had a demon who was promised a little boy.
So again, with all these stories, the story isn't,
believing there's whether this story actually happened isn't the most important.
I'm telling a story.
But I don't have a problem thinking that maybe it happened.
I do admit that some of them seem a little bit outrageous,
but for the most part they're instructive and they're insightful and they're interesting.
But most important is the first, the teaching that we'll get to.
Anyway, the story goes, and this is an interesting story.
That's some interesting points to it.
The Buddha said they were going to get into some danger,
but that danger will be averted if they bring with them
the Samanera Samkita.
And if they do so, the danger will be removed and they will be able to practice and fulfill their goal.
So now the story continues with Samkita Samanera.
Now, Samkita Samanera, we have to give some background for him.
He was the son of a daughter of a rich man in Sabhatin,
and his mother died while he was still in the womb.
Somebody to see it.
She became second died.
And maybe not knowing that he was inside or thinking that he was dead.
I'm probably thinking he was dead and they didn't have the tools I suppose back then.
There was no ultrasound back then.
They burnt the body with him inside.
And how they would burn the bodies to just put them out on some wood and burn the body up.
Now, when you burn the body, it doesn't burn all the way through.
And the hips especially on women I think because there's generally some bulk there.
And so that's the one part that doesn't burn.
And so they have this stake that they actually stabbed the body and turned it over with.
And so they got this guy whose duty it is to stab and turn.
And on them, they let it burn for a while and then he's supposed to stab it and turn it and he stabbed it.
And he turned and the body sort of decomposed and it was on the ashes and so on.
And it exposed this baby boy and the stake had got him right here or somewhere in his eye.
I don't think it blinded him, but it struck the outside of his eye.
That's supposed to be how he got the name of San Quizha because they found him and they found him alive.
And they took him and grazed him and it was prognosticated that he would become a monk.
And indeed at seven years old he became an novice and became enlightened during his ordination ceremony.
When the razor first touched his head, he became enlightened when they started to shave.
And the commentary says that's the reason why he didn't die in his mother's womb.
These sort of strange things do happen, not exactly this.
This is I think a fairly unique case, but cases where people should have died, where babies shouldn't have survived.
Miracles, I mean it's not a miracle because it's not like it's coming from God or something like that.
But strange coincidences do happen, they're rare, and the skeptic would say, well they're just random, you know, they happen.
And of course so many different things will happen.
There's no reason to think that's unlikely occurrences will happen.
But we don't know exactly why they happen.
The commentary says why this happened is because he was destined to become an aran.
So there was some strength there. It wasn't destiny in the sense of pre-determinism.
But his karma was so strong.
His goodness was so strong that it protected him from death.
I mean it wasn't like exactly that the flames he was impervious to fire.
But the circumstances made it just so that the guy caught him.
I think actually the story goes on to say that it's a little bit worse than that actually.
I think I'm secularizing it a little bit.
Yeah, because the story says that the body itself burned, but then there was another body or something.
It's a little bit weird. I'm not going to endorse that.
It's simply because this is too much of an open forum.
But for the Buddhists among you, I don't want to insult you either.
If you're happy with the more religious, more magical version of the story that I apologize for,
watering it down a little bit.
But the story, again, not so important.
That aspect is certainly not important.
It's interesting how karma can protect you and how there are unseen forces
that sometimes protect us, unseen forces not meaning God.
Although it can be angels, as we've seen in last story,
I mean that if you have any inkling that angels might exist,
or beings that are not in the realm of human physics,
are able to go outside of it so that they're undetected.
So if they're angels and beings that we can't detect,
then the whole idea of them protecting people or being getting involved
or coming into the human sphere,
coming into the realm of which we can detect physically.
It doesn't seem that unreasonable.
But here it was just as karma that protected him.
So that's the story of San Gijasana,
because this is a sanka or something.
What is it? The sanku?
Sanku means a stake or a spike.
So he got the name San Gijas.
There's one who, I don't know what the ending is,
but the sanku maybe was Ikar or something,
because I Akis, sorry Akis, I would.
No, no.
Because his eye had been pierced with a stick,
sanku, making him the name San Gijas.
So he became an arhantan, that was that.
Now back to our story about the 30 men, the 30 monks.
But it said to them,
he said, well, they have to get this novice to go with them.
Somehow the Buddha saw that this novice,
because he's a very powerful sort of novice.
The whole not dying through fire thing kind of tipped,
kind of should tip you off to that.
So in order to facilitate this,
he said, go see the other, sorry, put them.
Because he started to put them before you leave.
They didn't want to say no,
they didn't want to really,
he wanted it to be more round about.
I think that you can see sort of the methodology here,
because if the Buddha says,
we'll bring the samanir sunkitsha with you,
this would cause problems.
You'll see how it would cause problems.
It's an interesting story.
The Buddha didn't say bring the sunkitsha with you,
and you'll see the reason why,
as we go through this story.
He says, go see, sorry, put them.
So they go to see, sorry, put them.
And, sorry, put is a little bit not puzzled,
but if the question arises in his mind,
well, why did the Buddha send them to me?
And somehow the Buddha,
sorry, put them,
whether he has some kind of magical insider
or super natural insider,
or just he's so profoundly wise.
He knew exactly what,
or he was able to guess,
or he was able to get the answer
as to what the Buddha was thinking.
And he said, do you have an office with you?
And they said, no, no, we don't have an office.
So, you know, yeah, it would be nice,
but it's not really necessary.
And, sorry, put is a bring sunkitsha with you.
I don't know, no, no.
He'll be a hindrance to us.
He'll get in our way,
and we got to look after a seven-year-old child
to come on.
What are we going to do with this seven-year-old kid?
We'll have to take care of him.
And, sorry, put a said look.
Did the Buddha or did he not send you to me?
Do you not think there was a reason?
You know, and now you're going to argue with me?
He said, the Buddha sent you to me,
and I'm telling you to bring sunkitsha.
So, you're going to bring sunkitsha with you.
I mean, it wasn't even forced them,
but he laid out the logic of it.
And they were like, okay,
well, I guess that's why the Buddha sent you to us,
sent us to you,
though they don't have a clue why,
because the Buddha never said there was a danger,
so they don't know that there's danger.
So, they took him,
and the 31 of them headed off to the forest,
and they found a suitable place,
a suitable community,
and the community asked them
what sort of place they were looking for.
They said, oh, we're just looking for a place
where we can be at ease, be at peace,
and practice our meditation.
And they said, well, then please stay here.
Here are our village.
You can stay in the forest nearby.
We'll build your good deeds.
We'll take care of you,
and if you do,
then we'll be able to learn the Gama as well.
We'll be able to hear your teachings, practice along.
And we'll be able to keep the five precepts
and send on and practice the opposite time,
which should be the full moon,
the empty moon that have these religious activities,
and it's nice that they like to have monks
or religious people around.
So they accepted,
and they built them good deeds
and walking meditation areas with the roof.
They don't have a walking path
or a meditation hall kind of thing,
and they did duties,
and ministered faithfully to their needs, it says.
And they made this deal among themselves
that they wouldn't,
they would never be together,
except on the days of doing the duties.
Now, every two weeks,
they had to get together to recite the body mocha,
to recite the rules of the sanga.
But apart from that,
let's stay apart when we go for alms around,
whatever we do,
except for in the early morning,
except for when we go on alms,
or in the evening when we wait,
when we do our duties to our teachers and so on,
to the elder.
Let's not be together,
let's always be apart.
So they spent all of their time alone
so that they could dedicate themselves to meditation.
And so you never see anyone talking,
the monastery was completely quiet throughout the day,
except for when they had to do their duties.
Or when they were eating of that kind of thing.
And so they're having made that agreement.
They entered into the rains this three month period
where you engage in meditation practice.
You don't go anywhere that kind of thing.
Now, during the rains,
it happened that there was a poor man
who had gotten sort of kicked out of his house
when his daughter went to live with her husband
and the husband had no use for an old man.
And the daughter didn't protect him,
didn't take care of him and they just kind of ignored him.
Nowadays, it would be the equivalent of sending them
to the old age home,
but now they don't have old age home,
so they just forget about you, I guess.
Nowadays, right back then,
they had no old age home,
so they would just forget about them.
And so that's what they did.
And so this guy became a beggar
and he came to the monks and he was begging for food.
And the monks offered him some of their food.
They didn't offer him anything special.
They just offered him whatever they had to eat.
And he looked at the food that they had given them each
each monk of the 30 gave him a little piece
and he looked at it and he thought,
this is better food than I got as a lay person.
He said, is this some kind of special day?
And they said, no, this is, no.
Why the special food?
This is the kind of food we get every day.
And this man suddenly the gear started turning in his head.
From the get-go, he didn't have the best intentions.
Remember, this is what the Buddha prognosticated,
you know, foretold, I didn't say.
That there would be trouble from a guy
with a bigger person who ate scraps.
And so this guy says to them,
hey, do you think maybe I could live in here
and take care of you?
Do you need someone to help you take care of you?
Or help sweeping or help cleaning or this kind of thing?
And so they let him stay.
And I guess he kind of said, you know,
I can let the dhamma as well.
And so they let him stay the monastery.
And eventually he didn't, he did, he gained their trust.
He wasn't bad-hearted, it doesn't seem.
He was a little bit greedy, perhaps,
and didn't have the intention.
Really, I don't think to learn the dhamma.
Not sure.
Seems like he didn't really learn the dhamma.
Because then one day he desired,
he wanted to go back to see his daughter.
And so without telling the monks he'd just up and left.
And on his way back through the forest,
he was grabbed by a group of,
by a large group of thieves, bandits.
So danger came to him.
And this large group of bandits took him to their hideout
and tied him up.
And then he watched as they began to sharpen knives
and prepare and alter a sacrificial altar.
And he was watching and I was wondering
what they're going to do.
They're going to have some kind of a sacrificial offering,
goats maybe.
They didn't see any goats,
they didn't see any cows.
And so he asked one of them as they were
walking by or something.
I see you're preparing a sacrifice,
but I don't see anything to be sacrificed.
And the men turned to him and said,
well, you're the sacrifice.
We're preparing this to sacrifice you.
And it's supposedly,
they said that they had made a vow.
And this was common in India
to make a vow to a spirit,
some kind of spirit,
that if they were able to,
well, not if they just sit whoever into this forest,
they will kill.
The first person turned to the forest
and they're making an offering with flesh and blood.
So the idea was that the spirit would then protect them, I think.
And the men started freaking out
and realized suddenly,
suddenly had this fear of death thrust upon him.
And they started freaking out
and pleading and begging.
And finally he came upon this idea.
He said to him, so he said to them, look,
I'm not who you want to perform a sacrifice.
I mean, what kind of a sacrifice am I?
I live off of scraps.
I eat broken meat as well as I said,
but I think it lives off of scraps.
He said, I mean, listen to me.
Just back there, always.
There are 30 monks,
monks, Buddhist monks,
Samana, Shamans,
real shamans, true shamans.
All of them were spiritually advanced beings
in a powerful means.
Surely, if you made a sacrifice of one of them,
that would be far more beneficial,
far more weighty, far more meaningful.
Can you imagine, you know,
talk about biting the hand that feeds you?
He says,
kill them, make an offering with their blood,
and your spirit will be pleased beyond measure.
And they thought, well, that's a good idea.
And these are what this guy makes,
says make sense.
And so they have him lead them to where the monks are.
And they get to the monastery,
the place where the monks reside,
and they don't see anybody, of course.
And they hear what are you lying to us.
There's no one here.
And he says ring the bell.
Ring the bell and they'll all come out.
And so he tells them to ring the bell.
And then he rings this bell.
And I didn't mention the bell.
The idea is they had this bell.
And when they heard the bell,
it would mean someone was sick,
or there was some emergency.
In the case of an emergency,
then we'll come together.
So they had this bell.
They rang the bell, and all the monks come out.
All the 30 monks,
and some kitchen,
and some an era come out.
And they see this large group of
fuckish-looking men,
and maybe women.
I don't know.
And they say,
they call them laypeople of Bhasaka.
Bhasaka.
Not exactly the title that they should be given,
but some polite title.
Why have you rang this bell?
And who struck this bell?
And the head of the themes,
the ringleader,
came to the front and said,
I struck it.
Well, what for?
And then he tells them.
We made a vow to a forest spirit
to kill someone,
and offer their flesh and blood as a sacrifice.
So we're going to take one of you with us.
Now, these guys were not like this silly beggar guy.
How it happened with them
is the head monk,
the elder monk,
stood turned to his brethren
and said, brothers,
when we have business of the sangha,
it falls on the elder
to make a decision.
I will go with these men
and all of you strive on with diligence.
And the second,
the second most senior monk,
steps forward and says,
it's the duty of the junior monk
to protect the senior monk.
I will take your place.
You stay here and lead these monks.
I will take your place and go.
And then the third senior monk came forth
and said,
let me protect you.
I will go with them.
You stay here.
And so on with the fourth and the fifth
and all the monks came forward and said,
till the last monk,
until the most junior monk said,
not one of them said,
you go,
not one of them would let another go.
So what do you think happened?
Suddenly, they hear this squeaky,
seven-year-old voice.
I'm not squeaky as well.
It gives an aura hunt after all.
This melodious and lightened voice
of San Kiipcha, Samanera,
pipe up and save honorable service.
I'm junior to you all.
Let me take care of you. Let me go.
And what did they say to them?
They say,
dude,
even if we all were going to die,
even if they were going to kill all of us,
we still wouldn't let you go.
And San Kiipcha says, why not?
They say,
listen,
Sarri put the gave you to us.
They put the interested us,
interested you to us,
trusted us with you,
with your well-being.
Can you imagine how it would look
if we came back without you?
Or if we came back having to tell Sarri
put it that we sacrificed you,
we abandoned you to a bunch of themes?
You think that,
how do you think that would look?
You think that would be respectful?
And San Kiipcha shakes his head and says,
Sarri put the vendor above,
the elder Sarri put the sent me with you.
Why do you think he sent me with you?
This is the reason.
He sent me because somehow they knew.
Wow, the Buddha knew actually.
It's not clear that Sarri put the new bit.
Sarri put a new with the Buddha was looking for.
I guess there was something special about San Kiipcha.
He seems to be very powerful in some way.
We'll see. Yeah, he's absolutely powerful.
He doesn't die.
But they can't believe they just are in awe
of this seven-year-old boy.
I mean, of course they have been from the beginning
because he's awesome.
He says,
the Buddha sent you to my preceptor,
my preceptor sent you.
He told you to bring me.
This is why I'm here.
I'm here to take care of you.
And he bows down to all of them and pays respect to them
and says,
Reverend Surs,
if I have been guilty of any fault,
please forgive me.
That's what you would do when you're taking leave of someone.
And so he took what was to be the final leave
of all these monks.
And they were all moved
with tears in their eyes
and mourning the loss.
They were profoundly moved.
Their eyes filled with tears
and their hearts, flesh trembled.
Their hearts, flesh trembled.
And so the chief elder,
before they left,
said to the head,
the head of the Thebes, he said,
look,
he's just a boy.
When you prepare for the...
Can you not let him know what you're going to do
before you do it?
They took him aside and asked him this.
And they had bandits.
Yeah, fine.
And so they took the guy,
they took the novice
and they sat him down
and said, you stay here
and we'll come and get you
when we need you.
And they put him
off to one side of the novice.
It's on getcha, of course,
as he would do.
Anytime he was left alone
went into meditation,
entered into maybe
new rhoda.
It was probably in a state
of freedom
in a state of immense patient.
It says he was in a state of trance,
so some John knows,
some sort.
And then the head bandit
took his sword,
and came up behind
some getcha quietly,
so he wouldn't frighten the boy.
And stabbed down with his sword
to kill him.
And the story goes
that the sword
bent back upon itself
like bent back on the hilt
wouldn't strike.
And he looked at this sword
and he couldn't believe
what was going on,
so he bends it back
and he strikes again.
And this time it actually shatters
the blame breaks into pieces.
There apparently is something
to certain transs,
that you can't be hurt
when you enter into a trance.
We'll have a story of a woman
who had a whole pot of butter
dumped on to her,
and was on her
because she was in a trance.
She was in a trance of love,
of kindness,
loving kindness.
But okay,
many of you will probably
be unhappy about that,
thinking why is he telling us
such silly stories?
Whatever.
Brakes.
And the great thing,
the great thing comes next.
He looks at this broken sword
and he looks at the novice
sitting perfectly peaceful
and he starts to shake.
And he shakes his head
and he says this sword.
This sword is an insensitive being
and it knows
better than to harm such
noble being as this.
Who am I?
How can I ignore?
How can I miss this?
What am I doing?
You can just walk him up
to the insanity of what he was about to do.
And he bows down to the novice
and he says,
please forgive me.
And then he asks,
he says,
venerable servant.
Reverend Sur,
we dwell in this forest
robbing people
and we have a very bad reputation.
We have a bad reputation
that if 100 people,
a caravan of 100 people
comes through the forest
if they see us,
they get afraid.
If two or three of them go
walking through the forest
and see us,
they get petrified with fear.
And you,
when you're old kids sit here
unmoved,
unperturbed.
And then the pronounce is actually
a verse of poetry
or that's how it's encapsulated here.
You tremble not nor fear.
And they more,
your appearance is tranquil.
Why weep you not
that such a horror
probably sounds better in the poly.
And San Quiche,
coming out of his genre,
replies in verse as well.
He says,
for someone who is free from the violence,
they don't get afraid.
When he says,
when they've given up existence,
they have no another,
neither elation or deflation,
neither fear or excitement,
neither good nor bad.
They're at peace.
They don't become afraid of things,
but they don't urine after things.
And his verse is,
where stanza is chief.
He, that is free from desire,
has no mental suffering.
Seer,
he that has rid himself
of attachment,
has passed beyond of fear.
A seer,
as a seer.
If the eye of existence
is destroyed as it should be in this life,
death is without terrorists,
and it's like the putting down of a burden.
Death is like the putting down of a burden.
Bottle, holy, bunch of kind of time.
Five aggregates are indeed a burden.
That's how enlightened beings sees it.
They're just carrying it around until they have,
until it's time to let it go.
And the head of the thieves,
it's just the power emanating from the seven-year-old boy.
He looks at him.
He turns and looks at his group of monks,
a group of thieves,
or a group of bandits.
And he said,
what do you guys want?
What do you guys want to do?
And they said,
what do you want to do?
You're the leader.
And he shakes and suddenly says,
I've witnessed such a miracle.
I have no more use for this life.
I'm going to become a monk.
And if I'm going to become an ascetic,
a shaman,
a summoner,
and follow this novice.
And the other guys are like,
us too.
That was easy, no?
This whole group of bandits
moved.
I mean, I think if anyone experiences such a miracle,
it can be quite moving because miracles like that don't.
You know, that doesn't happen that often,
where you do something like they were watching this,
when they've stored breaks over the flesh of this young boy.
And so they all become novices.
They can't become monks because, of course,
this isn't a monk,
but somehow he's able to ordain the most novices.
Maybe not officially.
But then he takes them back to see these monks
and lets them know.
And I don't even think he tells them that it's the bandits.
He comes to them and he says,
I've got some new monks with me.
I'm going to take them back to see the Buddha
and they say, oh, you've got some followers.
OK.
And so they...
I mean, I guess his point with going back
was to relieve the elders of their sorrow.
So they go back and...
And so that it won't be hindrance in their meditation
or feeling of guilt or sadness.
And so he goes and shows them that he's still alive
and that indeed he's got some followers.
And so he's asked permission to go back to see the Buddha
and they give permission.
And he goes back to see the Buddha.
And he tells the Buddha the story
and the Buddha turns to these ex-bandits.
And he says, is that true?
And they say, hey, that's true.
And the Buddha then teaches this verse.
He said, if you live a life for 100 years,
corrupt, immoral, unconcentrated,
un-imbalanced in mind, you know,
perturbed in your mind.
He can't even say, better is one day of life.
The life of only one day.
Better to live only one day.
Living ethical for ethical and meditating.
Now, curiously, that's not the end.
And curiously, he recites the verse one more time.
That's an odd story, actually.
Sunkitsha grows up, becomes a monk,
and ends up later on with a novice of his own.
And this novice gets captured by a large group of bandits as well.
And they threaten to kill him,
but they make him promise.
He teaches them the dhamma.
And so they let him go, but teaching the dhamma
it softens them up.
And they decide to let him go,
and he must vow never to tell anyone about us that we're here.
And he makes that promise.
Now, it's interesting because promises are not considered lying,
but there is something wrong with breaking your promise.
And this clearly shows that the commentary
is very, very anti-promise breaking.
Because this novice refuses to do it.
What happens is he's on his way home.
I think he's going to see his parents.
So his parents come out to meet him.
And somehow it doesn't specify,
but I'm assuming what happens is they want to go to the monastery
or they want to go, maybe they're taking him back to the monastery.
And so they're going to go through this forest where these bandits are.
And the novice goes with them,
but doesn't tell them that there are bandits up ahead.
And so he gets there.
And the bandits recognize him and say, you're back.
You brought these, you brought some people with you.
And they grab the other two people.
They grab his parents and they tie them up
and they abuse them and so on.
And the parents turn to the novice and they're shocked.
And they say, did you, you knew when you,
you led us to these bandits,
thinking that he was in collusion with them.
And the novice was quiet and then the bandits heard this
and they turned to the novice and they said,
is that really,
even to your parents, that's how much your,
your moral ethical principle is worth to you
and they were moved by it.
That's the story.
It's, it's, it's quite odd because to imagine doing that to your parents,
you know, most people would say,
well, dude, I'm going to break my promise from my parent.
Somehow I get the feeling.
I mean, you could defend it by saying that he,
that's why he went with them.
And he had the idea that he would fix things.
Because he did.
He taught the dumb man.
They all became, all the band,
these bandits as well became monks.
And they went to say the Buddha and the Buddha said,
there's this true and so on.
And again, the Buddha taught the same verse.
Both, so he taught this verse,
both to some kids, both to the followers of some kids,
and the followers to the novice,
who is a follower of some kids and some of them.
So that's our story.
What this verse means to us,
it's not giving us any deep, deep teaching,
but it's a very strong reminder.
Why we should be meditating?
Why we should be keeping morality?
And it reminds us what's important.
What we're going to see for the remainder of this chapter,
the next series,
about five or six verses,
it's all going to be very similar to this.
Better to live one day than to live 100 years.
And there's various factors that make a life valuable.
A life that is no matter how long it is,
is worthless.
And should not be seen as worth anything compared to
a very short life, a life of one day that is useful.
Why? Because it has benefit.
If you are mindful,
if you are focused, if you are ethical,
then death is not to be afraid.
Not to be feared.
Death comes and you go on to only good things.
It's people who have done bad deeds,
and cultivated all sorts of un also deeds
that should be afraid of death.
Because it comes and we're unprepared.
Who knows where we go.
So the dumb for today,
that determine the benefit,
our morality and concentration,
we could say, or focus, or meditation.
But samahita means kind of like focused.
Because sam is level.
Sam is like same sort of in English.
So it means have level to be level headed,
but it's kind of like when a camera lens gets into focus.
And it also implies kind of concentrated
in the sense of being focused on the single thing.
And focused on the present moment, for example.
And so these are the two defining factors.
If a person has these two things,
there's not much more that you need.
I mean, the thing about Buddhism,
there's not a lot of teachings in the end
when it comes right down to it.
I mean, there are tons of teachings,
and there are all words,
it's not to belittle and to minimize that.
But when it comes right down to it,
there's nothing about God that you have to think about.
There's nothing about the soul,
or there's not even a lot about karma,
or past lives, future lives.
It's very much about here and now.
It's not about rituals that you have to perform,
or certain things you have to abstain from.
The abstentions are very simple.
It's about ethics.
If you are ethical, not killing,
not stealing, not cheating, not lying,
not taking drugs or alcohol,
and if you practice meditation,
you've pretty much accomplished it.
You've pretty much set yourself
on the path to becoming a Buddhist
or becoming a successful practitioner of the Buddhist teaching.
So that's about all.
It's been more beneficial because of the result.
Even one moment of meditation
is in a whole other league from 100 years of immorality.
It's 100 years of immorality.
It's worse and worse and worse building,
it's worse and worse and worse karma,
leaving you to a bad place.
So that's our Dhamma God historian for this evening.
Thank you all for tuning in,
watching and keeping up with this series.
Please do remember to check out my other videos
because personally I find them a lot more informative
and useful, these ones are much more entertaining
and sort of, I guess, allegorical
or they provide morals or moral
or they are examples,
they provide examples of life
and insightful into the lives of Buddhist monks,
but they don't give you deep teachings
and think there are other videos out there
that might be more beneficial in that regard.
So don't become negligent just by watching videos
or listening to stories or even listening to the Dhamma,
it's all about practice.
Are you being moral or are you ethical?
If you're watching this drunk
or if you're planning to finish this
and then go out and commit all sorts of nefarious deeds
and that it's not really have much use
it would not really benefit from it.
So thank you for your practice.
Keep practicing, we should be all the best.
See you next time.
