1
00:00:00,000 --> 00:00:08,800
These four are said to be not born of anything, not caused by anything,

2
00:00:08,800 --> 00:00:12,000
why? Because there is no arising of arising.

3
00:00:12,000 --> 00:00:17,200
And the other two are the mere maturing and breaking up of what has arisen.

4
00:00:17,200 --> 00:00:26,600
That means they are just the phases, different phases of the material properties which have arisen.

5
00:00:26,600 --> 00:00:32,200
They are not, actually they are not separate material properties.

6
00:00:32,200 --> 00:00:37,000
So they are not said to be caused by anything,

7
00:00:37,000 --> 00:00:45,800
anything in any comma or consciousness or temperature or nutrient.

8
00:00:45,800 --> 00:00:51,000
But there is a passage which says the visible database, the sound base,

9
00:00:51,000 --> 00:00:57,400
older base, the fluid base, the tangible database, the space element, the water element,

10
00:00:57,400 --> 00:01:01,400
lightness of matter, melee ability of matter, wheeliness of matter,

11
00:01:01,400 --> 00:01:06,800
growth of matter, continuity of matter, these two, and physical food.

12
00:01:06,800 --> 00:01:09,400
These states are consciousness originated.

13
00:01:09,400 --> 00:01:12,400
So this is from the Abidhama text.

14
00:01:12,400 --> 00:01:17,000
So in Abidhama, I just said that growth of matter and continuity of matter are caused by,

15
00:01:17,000 --> 00:01:20,400
caused by, originated by consciousness.

16
00:01:20,400 --> 00:01:29,000
Likewise, there are passages where they are said to be caused by the other causes,

17
00:01:29,000 --> 00:01:30,400
like karma and so on.

18
00:01:30,400 --> 00:01:33,600
So how about that?

19
00:01:33,600 --> 00:01:39,200
And the answer is, actually they are not caused by anything.

20
00:01:39,200 --> 00:02:03,600
But they are evident when the, when the functions of karma and so on,

21
00:02:03,600 --> 00:02:09,200
are still said to be existing.

22
00:02:09,200 --> 00:02:14,800
Now, suppose there is a re-linking of rebirth.

23
00:02:14,800 --> 00:02:18,800
At the, at the first sub-moment of rebirth,

24
00:02:18,800 --> 00:02:23,200
and there is, there is karma born matter.

25
00:02:23,200 --> 00:02:29,400
Now, karma is always concerned about producing something.

26
00:02:29,400 --> 00:02:37,000
Only after it has produced something, let, give up this concern.

27
00:02:37,000 --> 00:02:41,800
So it is still concerned about producing results.

28
00:02:41,800 --> 00:02:52,000
So at the very moment of arising, its concern has not yet disappeared.

29
00:02:52,000 --> 00:03:00,000
And birth, or these two growth of matter and continuity of matter,

30
00:03:00,000 --> 00:03:02,400
are seen at that moment.

31
00:03:02,400 --> 00:03:08,200
So that is why they are said to be caused by karma, data and so on,

32
00:03:08,200 --> 00:03:14,400
although, in fact, they are not caused by anything.

33
00:03:14,400 --> 00:03:23,000
They are certainly because by, say, karma because the exercising of the function

34
00:03:23,000 --> 00:03:28,200
is still, yeah, a function of karma is still there.

35
00:03:28,200 --> 00:03:34,200
Karma's concern for producing material property

36
00:03:34,200 --> 00:03:40,800
is still exercising at the moment of genesis or at the moment of rising.

37
00:03:40,800 --> 00:03:44,200
So that is why they are said to be caused by karma.

38
00:03:44,200 --> 00:03:56,400
And the data with regard to the data, the same thing.

39
00:03:56,400 --> 00:04:02,600
Then we can't be consciousness aggregate.

40
00:04:02,600 --> 00:04:05,200
Oh, vinya nakanda.

41
00:04:05,200 --> 00:04:10,800
So among the remaining aggregates of whatever has the characteristic of being felt,

42
00:04:10,800 --> 00:04:13,800
no, it is not, not correct.

43
00:04:13,800 --> 00:04:18,000
Whatever has the characteristic of feeling should be understood,

44
00:04:18,000 --> 00:04:20,600
all taken to be together as the feeling aggregate.

45
00:04:20,600 --> 00:04:26,600
Not being felt, but feeling.

46
00:04:26,600 --> 00:04:30,000
He misunderstood the words.

47
00:04:30,000 --> 00:04:33,200
The party word is with the ego.

48
00:04:33,200 --> 00:04:41,200
And it looks like a passive first participle, the words.

49
00:04:41,200 --> 00:04:44,400
But it is not passive, past participle here.

50
00:04:44,400 --> 00:04:47,800
It is something like a verbal norm.

51
00:04:47,800 --> 00:04:57,200
Say, not, which is, that which is felt, but that which feels of just feeling.

52
00:04:57,200 --> 00:05:04,800
So here, the word we did means feeling, not being felt.

53
00:05:04,800 --> 00:05:09,600
If you know a little party, you can, you'll understand that.

54
00:05:09,600 --> 00:05:23,600
The suffix tah, t-a, the suffix tah is added to roots, to mean passive.

55
00:05:23,600 --> 00:05:35,000
So here, with the yita, so it, it could mean, which is felt, but here, it is used in

56
00:05:35,000 --> 00:05:40,800
the sense of just feeling, not being felt.

57
00:05:40,800 --> 00:05:48,120
So the characteristic of feeling should be understood, all taken together as the feeling

58
00:05:48,120 --> 00:05:50,600
aggregates.

59
00:05:50,600 --> 00:05:57,400
Because feeling aggregate is, that which, which as a feeling as characteristic, and that

60
00:05:57,400 --> 00:06:03,320
which is not felt, if it is felt, then there must be another feeling.

61
00:06:03,320 --> 00:06:14,360
So, and feeling here is explained in the sub commentary as experiencing the taste of object.

62
00:06:14,360 --> 00:06:20,040
So that is what we call feeling.

63
00:06:20,040 --> 00:06:28,560
We can even say, enjoying the taste of object.

64
00:06:28,560 --> 00:06:32,640
And now, the aggregate of consciousness.

65
00:06:32,640 --> 00:06:41,600
The aggregate of consciousness is described in this book, following the order given in the

66
00:06:41,600 --> 00:06:44,640
original abitama text.

67
00:06:44,640 --> 00:06:50,880
And this order is different from the order we are, we are familiar with.

68
00:06:50,880 --> 00:06:58,520
Because we are familiar with the order given in the manual of abitama.

69
00:06:58,520 --> 00:07:03,880
The order given in manual of abitama is different from what we are given in the original

70
00:07:03,880 --> 00:07:06,760
abitama text.

71
00:07:06,760 --> 00:07:13,400
And the visual numerator follows the order given in the original abitama text.

72
00:07:13,400 --> 00:07:18,120
That is why I made these notes.

73
00:07:18,120 --> 00:07:28,480
So this, this chart is the same as the dots, but just that the numbers are given here,

74
00:07:28,480 --> 00:07:36,040
so that you know, when I say number 31, you can just pinpoint it.

75
00:07:36,040 --> 00:07:47,080
And then, yeah, no, the other two sheets, or the other, the first this sheet, and this

76
00:07:47,080 --> 00:07:51,320
is something like a concholence.

77
00:07:51,320 --> 00:08:03,000
So the numbers given in this book are different from numbers given in the manual of abitama.

78
00:08:03,000 --> 00:08:16,040
Let us say, if you read the paragraph 83, here in that of the sense fear is hateful

79
00:08:16,040 --> 00:08:22,360
being classified according to joy, equity, knowledge, and prompting that is to say one

80
00:08:22,360 --> 00:08:25,200
when accompanied by Joachos, don't worry.

81
00:08:25,200 --> 00:08:37,200
This number one given in this book is number 31 in this chapter.

82
00:08:37,200 --> 00:08:40,680
Now you can identify it, right?

83
00:08:40,680 --> 00:08:43,560
So number two.

84
00:08:43,560 --> 00:08:48,600
So number two given in the part of purification is what?

85
00:08:48,600 --> 00:09:05,560
Number 32 in this, so it can help you to identify it.

86
00:09:05,560 --> 00:09:17,080
So the, the, the, the plan of the types of consciousness given in, in the part of purification

87
00:09:17,080 --> 00:09:23,360
is, as I said, according to the text, it, original text of abitama.

88
00:09:23,360 --> 00:09:29,800
And so they are classified in this way, the last, the last sheet.

89
00:09:29,800 --> 00:09:35,600
So first, Kusala, second, Akusala.

90
00:09:35,600 --> 00:09:37,800
And then third, abiakada.

91
00:09:37,800 --> 00:09:46,440
So first, these three major divisions, Kusala, wholesome, Akusala, unwholesome, and abiakada

92
00:09:46,440 --> 00:09:48,520
in determinate.

93
00:09:48,520 --> 00:09:55,560
And then Kusala is divided into kama wajara, belonging to sense fear, ruba wajara, belonging

94
00:09:55,560 --> 00:10:01,360
to, um, material sphere, a ruba wajara, belonging to in material sphere, and local trust

95
00:10:01,360 --> 00:10:03,080
to from one day.

96
00:10:03,080 --> 00:10:09,360
Then Akusala is divided into three, loba mula, that is accompanied by, uh, loba attachment,

97
00:10:09,360 --> 00:10:17,960
those are mula accompanied by those are, or having those are as a root and moha mula.

98
00:10:17,960 --> 00:10:25,840
And you, you can find the, in this translation in, in the, in part of purification.

99
00:10:25,840 --> 00:10:32,600
So, and I give the paragraph numbers, so you, you can find them easily in the book.

100
00:10:32,600 --> 00:10:38,840
And then abiakada, abiakada, abiakada, abiakada, abiakada is the most comprehensive.

101
00:10:38,840 --> 00:10:44,800
And abiakada is first divided into vibaka, and then down kiwiya.

102
00:10:44,800 --> 00:10:50,280
So vibaka, a result ends, and kiwiya, functioning.

103
00:10:50,280 --> 00:10:59,280
Then vibaka is subdivided into kama wajara, vibaka, and then going down, ruba wajara, vibaka,

104
00:10:59,280 --> 00:11:02,840
a ruba wajara, vibaka, local draviaka.

105
00:11:02,840 --> 00:11:09,760
And then kama wajara, vibaka is divided into kusala, vibaka, Akusala, vibaka.

106
00:11:09,760 --> 00:11:16,560
And then kusala vibaka is divided into aheetuga, without root, and saheetuga with root.

107
00:11:16,560 --> 00:11:19,200
So this is the plan.

108
00:11:19,200 --> 00:11:26,760
And then kiwiya is divided into kama wajara, ruba wajara, a ruba wajara.

109
00:11:26,760 --> 00:11:32,440
And then kama wajara kiwiya is divided into aheetuga, and saheetuga.

110
00:11:32,440 --> 00:11:41,240
So this is the order, given in the inside of purification.

111
00:11:41,240 --> 00:11:48,760
So there are altogether 89 types of consciousness explained in this chapter, not 121,

112
00:11:48,760 --> 00:11:56,360
but 89 and 121, as I said, because we be 8 supermandated types of consciousness.

113
00:11:56,360 --> 00:12:03,240
And then we multiply the 8 supermandated types of consciousness with 5 Janas, we get 40

114
00:12:03,240 --> 00:12:04,640
supermandated.

115
00:12:04,640 --> 00:12:09,720
So the number of changes become 121.

116
00:12:09,720 --> 00:12:18,680
If we take just, if we take supermandated consciousness, it's just 8, just 8, then we

117
00:12:18,680 --> 00:12:20,920
get 89 types of consciousness.

118
00:12:20,920 --> 00:12:27,720
So these 89 types of consciousness are described in this book.

119
00:12:27,720 --> 00:12:40,720
Now, in the footnote on page 507, does it discussion about the words nama, vinyana, monochita,

120
00:12:40,720 --> 00:12:53,480
these are synonyms, nama, vinyana, monocheta, and cetera, but say, while they are etymology

121
00:12:53,480 --> 00:12:58,920
can be looked up in the dictionary, one or two points need noting here, nama, rendered

122
00:12:58,920 --> 00:13:05,760
by mentality, when not used to refer to a name, is almost confined in the sense, considered

123
00:13:05,760 --> 00:13:12,640
to be expression, nama, ruba, mentality, materiality, as a food member of the dependent

124
00:13:12,640 --> 00:13:19,360
origin, where it comprises the 3 mental aggregates of feeling, perceptions, and formations,

125
00:13:19,360 --> 00:13:23,000
but not that of consciousness.

126
00:13:23,000 --> 00:13:28,080
I cannot agree with the word almost here.

127
00:13:28,080 --> 00:13:39,840
It's almost confined, actually, nama means, as it is said here, nama means 3 mental aggregates

128
00:13:39,840 --> 00:13:46,440
only when it is used in the dependent origin nation.

129
00:13:46,440 --> 00:13:58,520
So if it is used as a link independent origin nation, nama means feeling, perception, and formations,

130
00:13:58,520 --> 00:14:07,760
but in other places, nama means, cetera, and cetera, both, not cetera, cetera, only.

131
00:14:07,760 --> 00:14:12,920
So I do not like the word almost here.

132
00:14:12,920 --> 00:14:22,800
It is sometimes confined, something like that, not almost, but sometimes, only when given

133
00:14:22,800 --> 00:14:28,600
as a link in the dependent origin nation.

134
00:14:28,600 --> 00:14:54,200
If you go to chapter 18 and paragraph 8, chapter 18, paragraph 8, now, in material states,

135
00:14:54,200 --> 00:15:04,120
it just means, nama, and there, that is to say, the 81 kinds of mundane consciousness

136
00:15:04,120 --> 00:15:10,920
consisting of the two sets of five consciousness and so on.

137
00:15:10,920 --> 00:15:24,000
So nama means, both consciousness and mental factors, but in another, a bit of a text,

138
00:15:24,000 --> 00:15:29,920
nama also includes nibana.

139
00:15:29,920 --> 00:15:36,040
So nibana is also called nama, although it is not mental, nibana is something different

140
00:15:36,040 --> 00:15:44,720
from mental, the deep and materiality, but in pari, it is called nama.

141
00:15:44,720 --> 00:15:55,560
If you want to get confused, then just leave it alone, so let us take the nama means,

142
00:15:55,560 --> 00:16:01,680
mostly means, consciousness and mental factors together.

143
00:16:01,680 --> 00:16:10,360
But in the dependent origin nation, as a full member of the dependent origin nation, then

144
00:16:10,360 --> 00:16:22,240
there is the word nama, and their nama means, only the mental factors, you know why?

145
00:16:22,240 --> 00:16:31,920
Because there is consciousness above it, depending on ignorance, there are mental formations,

146
00:16:31,920 --> 00:16:36,040
depending on mental formations, there is consciousness, and depending on consciousness, there

147
00:16:36,040 --> 00:16:37,920
is nama and groupa.

148
00:16:37,920 --> 00:16:47,280
So when we say nama depends on consciousness, then nama cannot include consciousness, that

149
00:16:47,280 --> 00:16:55,440
is why nama is made to mean only feeling perception and mental formations in the dependent

150
00:16:55,440 --> 00:17:10,640
origin nation, but outside the dependent origin nation, nama always means, nama means,

151
00:17:10,640 --> 00:17:15,120
ceta and jidis is together.

152
00:17:15,120 --> 00:17:17,400
So nama, nama, nama, nama, nama, not ceta and ceto are all synonyms, and they just mean ceta.

153
00:17:17,400 --> 00:17:25,040
So nama means ceta, mano means ceta, jeta means ceta and ceto, and sometimes mean also ceta.

154
00:17:25,040 --> 00:17:39,040
And then, the footnote that is six, the explanation of kama wajirak, now, sense fear, here,

155
00:17:39,040 --> 00:17:47,240
there are two kinds of sense desire.

156
00:17:47,240 --> 00:17:53,760
Not that there are two kinds of sense desires, let us say, there are two kinds of kama.

157
00:17:53,760 --> 00:18:01,480
Now, please note the word kama, the pali wajirak, there are two kinds of kama.

158
00:18:01,480 --> 00:18:08,880
One is kama is a watu kama, you see the watu kama there.

159
00:18:08,880 --> 00:18:16,560
So one is watu kama, and the other is kile zakama, so there are two kinds of kama.

160
00:18:16,560 --> 00:18:28,040
The watu kama means objects of desire, the side sound, the smells, the trees and touch,

161
00:18:28,040 --> 00:18:31,000
so they are called watu kama.

162
00:18:31,000 --> 00:18:41,480
So watu kama means just the thing, the thing which is desired, so when it means these

163
00:18:41,480 --> 00:18:52,680
five things, then the word kama is to be known in the passive sense, desire, so something

164
00:18:52,680 --> 00:18:58,440
which is desire is called kama.

165
00:18:58,440 --> 00:19:11,400
When we mean kile zakama, kile zakama means mental defense, then it is active sense.

166
00:19:11,400 --> 00:19:19,200
Something that desires, so sense desire is defilement, kile zakama.

167
00:19:19,200 --> 00:19:21,440
So there are two kinds of kama.

168
00:19:21,440 --> 00:19:32,440
So whenever we use the wat kama, we must be sure that we mean watu kama or kile zakama.

169
00:19:32,440 --> 00:19:38,520
So whenever we find the watkama, we cannot translate it as sense desire.

170
00:19:38,520 --> 00:19:47,760
Sometimes I prefer the word sense objects, the objects of senses, objects of desire actually.

171
00:19:47,760 --> 00:19:53,400
So there are two kinds of kama, watu kama and kile zakama.

172
00:19:53,400 --> 00:20:00,240
Of these sense desire is objective basis, and that means just the objects, particularly

173
00:20:00,240 --> 00:20:08,160
arise as the five quads of sense desire, they are called five quads of sense objects, and

174
00:20:08,160 --> 00:20:19,280
they are simply side sound, smell, taste and touch.

175
00:20:19,280 --> 00:20:27,440
And sense desire is defilement which is craving desires, so here it is the active meaning.

176
00:20:27,440 --> 00:20:33,360
The sense fear is where these two operate together.

177
00:20:33,360 --> 00:20:43,240
Sense fear means that the world of human beings, the world of full, woeful states and

178
00:20:43,240 --> 00:20:46,120
the world of lower celestial beings.

179
00:20:46,120 --> 00:20:54,760
So that fear is called sense, sense fear because both watu kama and kile zakama operate

180
00:20:54,760 --> 00:20:57,400
here.

181
00:20:57,400 --> 00:20:58,400
But what is that?

182
00:20:58,400 --> 00:21:03,440
It is the 11 full sense desire becoming, have asura demand goes any more human beings

183
00:21:03,440 --> 00:21:06,520
and six sensual sphere heaven.

184
00:21:06,520 --> 00:21:11,760
So too with the fine material sphere and the immaterial sphere, taking fine material

185
00:21:11,760 --> 00:21:14,480
as craving for the fine material to and so on.

186
00:21:14,480 --> 00:21:22,640
So these are the explanation of the words, rupa vajra and arupa vajra.

187
00:21:22,640 --> 00:21:29,360
And local tries explain as something which crosses over from the world.

188
00:21:29,360 --> 00:21:32,320
So it is called local dara supramanday.

189
00:21:32,320 --> 00:21:37,440
So whenever we we find the word kama we must be careful not to translate it as sense

190
00:21:37,440 --> 00:21:41,280
desire, everywhere.

191
00:21:41,280 --> 00:21:48,960
Sometimes it means sense desire, sometimes it means sense objects or objects of desire.

192
00:21:48,960 --> 00:22:08,400
And on page 508, paragraph 80 before the paragraph 85, it is not a paragraph, but the

193
00:22:08,400 --> 00:22:17,480
number is given there as 85, there is an explanation of the word sankara for in this sense

194
00:22:17,480 --> 00:22:25,400
prompting, this is term for the prior effort exerted by himself or others.

195
00:22:25,400 --> 00:22:32,800
Now when we describe consciousness, we use the word sankara, see it is with sankara or

196
00:22:32,800 --> 00:22:35,680
without sankara.

197
00:22:35,680 --> 00:22:46,640
And the word sankara here means effort, the prior effort before doing something.

198
00:22:46,640 --> 00:22:50,120
So it is a special sense.

199
00:22:50,120 --> 00:22:55,800
I think you are familiar with the word sankara, sankara means those that are conditioned

200
00:22:55,800 --> 00:23:02,560
and also those that are conditioned.

201
00:23:02,560 --> 00:23:10,800
But here in this particular context, sankara means just prior effort.

202
00:23:10,800 --> 00:23:19,080
So with effort and without effort, that means with prompting and without prompting.

203
00:23:19,080 --> 00:23:28,240
So this is the definition of the word sankara and we should note this for explanation

204
00:23:28,240 --> 00:23:30,240
when you teach abroad.

205
00:23:30,240 --> 00:23:37,240
In this sense prompting is a term for the prior effort exerted by himself or others.

206
00:23:37,240 --> 00:23:45,000
And then the examples of the types of consciousness are given, we are not difficult to understand.

207
00:23:45,000 --> 00:23:53,200
And then in paragraph 86, at the end of that paragraph, the fist is associated with

208
00:23:53,200 --> 00:23:54,680
equanimity.

209
00:23:54,680 --> 00:24:03,040
And equanimity here means feeling, neutral feeling.

210
00:24:03,040 --> 00:24:08,600
You know there are pleasant feeling, unpleasant feeling and neither pleasant nor unpleasant.

211
00:24:08,600 --> 00:24:13,520
So the third one is called neutral feeling and the third one is called upika because

212
00:24:13,520 --> 00:24:22,280
the word upika can mean feeling, neutral feeling, all the real equanimity.

213
00:24:22,280 --> 00:24:37,480
So here equanimity means just neutral feeling, not so manasa, not domanasa.

214
00:24:37,480 --> 00:24:47,880
And then the numbers are given here and so when you see the numbers, you can check with

215
00:24:47,880 --> 00:24:56,600
the notes given today this sheet and then go to that sheet if you are already familiar

216
00:24:56,600 --> 00:25:05,040
with this, familiar with the dots.

217
00:25:05,040 --> 00:25:17,640
So on page 509, paragraph 991, when a man is happy and content, in placing wrong view

218
00:25:17,640 --> 00:25:22,720
foremost of the sort beginning, there is no danger in sense design and so on.

219
00:25:22,720 --> 00:25:27,560
I want to strike out the word in.

220
00:25:27,560 --> 00:25:31,120
He is not happy in placing wrong view foremost.

221
00:25:31,120 --> 00:25:37,080
He is happy in his content and he places wrong view foremost in his mind.

222
00:25:37,080 --> 00:25:41,840
That means with wrong view, he does something.

223
00:25:41,840 --> 00:25:48,200
So when a man is happy in content, placing wrong view foremost of the sort beginning,

224
00:25:48,200 --> 00:25:51,680
there is no danger in sense design and either in sense and so on.

225
00:25:51,680 --> 00:25:55,520
So the word in is not needed here.

226
00:25:55,520 --> 00:26:07,840
And then down the paragraph, bottom of the paragraph, but when the consciousness is devoid

227
00:26:07,840 --> 00:26:13,960
of joy in these four instances through encountering no excellence in these sense desires,

228
00:26:13,960 --> 00:26:21,360
here sense objects, not sense desires, sense objects which are desired, which are objects

229
00:26:21,360 --> 00:26:49,120
of desire and then, then page 510, paragraph 96, eye consciousness, eye consciousness

230
00:26:49,120 --> 00:26:54,040
means seeking consciousness, has the characteristic of being supported by the eye and

231
00:26:54,040 --> 00:26:57,120
cognizing visible data.

232
00:26:57,120 --> 00:27:01,240
Its function is to have only visible data as its object.

233
00:27:01,240 --> 00:27:13,360
That means the eye consciousness sees just the visible object and when it sees it just

234
00:27:13,360 --> 00:27:21,120
sees it and it does not see that it is blue or that it is red or whatever.

235
00:27:21,120 --> 00:27:27,880
Just the eye consciousness sees the object just as a visible object.

236
00:27:27,880 --> 00:27:37,840
And then we know that it is blue, it is green by monotor, by mindor, mindor, mindor thought

237
00:27:37,840 --> 00:27:42,320
process, not by the eye-doer thought process.

238
00:27:42,320 --> 00:27:50,440
So that is why it is a only visible data and not chara, shape and so on.

239
00:27:50,440 --> 00:27:54,600
And it is manifested as occupation with visible data.

240
00:27:54,600 --> 00:28:11,440
I think it is not acute, let me see, let us go to paragraph 107, about four lines.

241
00:28:11,440 --> 00:28:22,440
It is manifested as confrontation of visible data, the same words, so here also not occupation.

242
00:28:22,440 --> 00:28:32,400
It is being faced with visible data, confrontation means being faced with visible data,

243
00:28:32,400 --> 00:28:34,880
not occupation with visible data.

244
00:28:34,880 --> 00:28:40,920
And its approximate cause is departure of that means disappearance of the functional

245
00:28:40,920 --> 00:28:44,440
mind element that has visible data as its object.

246
00:28:44,440 --> 00:28:55,240
Now, in order to understand these passages, you have to understand at least the thought

247
00:28:55,240 --> 00:29:03,680
process, during the Obama class, I talk about thought processes.

248
00:29:03,680 --> 00:29:10,880
So if you have notes, please take those notes and look at the first thought process

249
00:29:10,880 --> 00:29:14,600
and then bring these passages.

250
00:29:14,600 --> 00:29:19,760
Because here, the eye consciousness, its approximate cause is the departure of the functional

251
00:29:19,760 --> 00:29:23,200
mind element that has visible data as its object.

252
00:29:23,200 --> 00:29:32,120
That means immediately before eye consciousness, what is there?

253
00:29:32,120 --> 00:29:33,120
Punch at water water.

254
00:29:33,120 --> 00:29:39,760
So disappearance of that functional mind element or disappearance of five-door advertising

255
00:29:39,760 --> 00:29:43,480
is proximate cause for eye consciousness.

256
00:29:43,480 --> 00:29:51,560
That is, if the functional mind element does not disappear, then eye consciousness cannot

257
00:29:51,560 --> 00:29:53,200
arise.

258
00:29:53,200 --> 00:29:59,560
So functional mind element disappears so that eye consciousness can arise.

259
00:29:59,560 --> 00:30:05,920
Therefore, disappearance of functioning and consciousness is approximate cause for eye

260
00:30:05,920 --> 00:30:07,760
consciousness.

261
00:30:07,760 --> 00:30:13,800
Something like when you are in a line, the disappearance of the man before is the approximate

262
00:30:13,800 --> 00:30:21,040
cause for you being there.

263
00:30:21,040 --> 00:30:50,120
Then the mind element as receiving or accepting and so on, then on page 500, beginning

264
00:30:50,120 --> 00:31:04,160
with page 513, paragraph 110, the 14 kinds of modes of 14 kinds of functions are explained.

265
00:31:04,160 --> 00:31:12,800
So these different types of consciousness have different functions.

266
00:31:12,800 --> 00:31:21,120
Types of consciousness have one function only, but others have maybe two functions, five

267
00:31:21,120 --> 00:31:22,640
functions and so on.

268
00:31:22,640 --> 00:31:30,640
So these 14 functions performed by different types of consciousness are explained beginning

269
00:31:30,640 --> 00:31:34,000
with paragraph 110.

270
00:31:34,000 --> 00:31:44,280
And these functions also, you need to have a diagram of the thought process, so looking

271
00:31:44,280 --> 00:31:52,880
at the thought process diagram and you read these passages and you will understand them.

272
00:31:52,880 --> 00:32:06,640
And on paragraph 111, about seven lines, eight lines down, entering upon the state of

273
00:32:06,640 --> 00:32:23,560
unus, that means being reborn as unus, so entering upon the state of simply means being reborn

274
00:32:23,560 --> 00:32:39,120
and the footnote 42, third line, sign of comma is the gift to be given that was a condition

275
00:32:39,120 --> 00:32:40,960
for the volition and so on.

276
00:32:40,960 --> 00:32:46,880
So after to be given, please add etc.

277
00:32:46,880 --> 00:32:57,640
The gift to be given etc that was a condition for the volition and footnote 44 with that

278
00:32:57,640 --> 00:32:59,120
same object.

279
00:32:59,120 --> 00:33:09,440
If comma is the life continuance, it's a mistake, relinkings object, not life continuance,

280
00:33:09,440 --> 00:33:27,920
relinking or rebut, so if comma is the relinking object, then it is that comma and so on.

281
00:33:27,920 --> 00:33:48,400
So this 14 moles of holding functions explained one by one and at the end, what do we end

282
00:33:48,400 --> 00:33:49,720
here.

283
00:33:49,720 --> 00:34:01,080
Now, paragraph 123, let's say, for the last life continuum consciousness of all in one

284
00:34:01,080 --> 00:34:08,800
becoming is called death duty because of falling from that becoming.

285
00:34:08,800 --> 00:34:24,160
Now, in one given life, relinking life continuum and death are of the same type of consciousness.

286
00:34:24,160 --> 00:34:33,320
Actually, relinking consciousness is a resultant consciousness and this consciousness repeats

287
00:34:33,320 --> 00:34:44,160
itself all through the life, but when after the first moment of relinking, it arises, it

288
00:34:44,160 --> 00:34:54,360
is called life continuum and at the end of life also, that type of consciousness arises

289
00:34:54,360 --> 00:35:04,780
and that we call death consciousness or to do so in one given life, the relinking, life

290
00:35:04,780 --> 00:35:10,040
continuum and death consciousness are of the same type of consciousness.

291
00:35:10,040 --> 00:35:22,080
So that is why it is for the last life continuum consciousness of all in one becoming is

292
00:35:22,080 --> 00:35:23,080
called death.

293
00:35:23,080 --> 00:35:31,280
So the life continuum, the last life continuum is, we call what we call death.

294
00:35:31,280 --> 00:36:00,880
So these three kinds of consciousness are just the same, one and the same type of consciousness.

295
00:36:00,880 --> 00:36:18,440
When I taught a bit of a chapter on data, take me about how many talks, six or seven

296
00:36:18,440 --> 00:36:37,040
talks, today we are doing it one hour, the next week, next week, the end of the chapter.

297
00:36:37,040 --> 00:37:07,040
So, feeling every gaze, perception every gaze, and then what the collision

298
00:37:07,040 --> 00:37:33,880
is, or not yet to the end, according to period, maybe about page five one and the

299
00:37:33,880 --> 00:38:00,440
eyes, classification of the five equities under 11 heads, after that.

300
00:38:00,440 --> 00:38:14,560
And do we have a note for, I handed a bit of a class, those will be handy, because next

301
00:38:14,560 --> 00:38:44,520
week it is pretty not a matter of state, so if you have those notes with you, next

302
00:38:44,520 --> 00:38:50,060
week.

