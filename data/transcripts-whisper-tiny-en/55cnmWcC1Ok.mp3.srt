1
00:00:00,000 --> 00:00:12,840
Good evening, everyone. Welcome to our live broadcast.

2
00:00:12,840 --> 00:00:28,440
Today's quote is from the Guttranikaya about Nakulapita and Nakulamata.

3
00:00:28,440 --> 00:00:44,440
These are curious couple who, elderly couple who went to see the Buddha and when they saw

4
00:00:44,440 --> 00:00:55,440
him they fell at his feet and called him son and asked him why he had gotten, why he had left

5
00:00:55,440 --> 00:01:02,120
them. And the Buddha explained that in past life, in their past lives they had been

6
00:01:02,120 --> 00:01:11,320
his parents for 500 births. He pitched to them and they became so dapanas and so this

7
00:01:11,320 --> 00:01:27,320
is a quote from a talk about it. I guess the whole point of it is how married couples

8
00:01:27,320 --> 00:01:33,800
should behave towards each other or how married couples are concerned with in regards

9
00:01:33,800 --> 00:01:45,440
to the Dhamma. So the idea here is this guy, the Nakulapita is dying and he is concerned for

10
00:01:45,440 --> 00:01:55,920
his, he is sick and is concerned for his wife and he is leaving behind is making the sickness

11
00:01:55,920 --> 00:02:10,220
worse. So he is actually dying. But the wife reassures him, reminds him of how well off

12
00:02:10,220 --> 00:02:16,040
she is and how she is really going to be okay. That's something that he shouldn't worry

13
00:02:16,040 --> 00:02:20,720
about and his worries subsides and his results against better. And then he goes to see

14
00:02:20,720 --> 00:02:29,520
the Buddha and the Buddha extols, virtues of this woman. I mean I'm not, it's nothing

15
00:02:29,520 --> 00:02:42,160
the most profound quote, but it gives some insight into small insight into the way

16
00:02:42,160 --> 00:02:49,640
enlightened couples that these two are so dapanas. So they call them enlightened to some

17
00:02:49,640 --> 00:03:06,320
extent, not fully enlightened, but on the first stage of enlightenment. So she says, do

18
00:03:06,320 --> 00:03:13,040
not die worrying. It's important. I mean if you want to go profound, this is the profound

19
00:03:13,040 --> 00:03:22,600
importance of this kind of situation that today worried is a dangerous thing. And we all

20
00:03:22,600 --> 00:03:31,120
have many things that would potential to make us worried when we pass away. So I'm doing

21
00:03:31,120 --> 00:03:40,600
a great job of coaching this man as he was dying to be reminded that something worried

22
00:03:40,600 --> 00:03:51,000
about is life was perhaps coming to an end, but facing that she would be alright and that

23
00:03:51,000 --> 00:03:55,680
she would be able to support herself and that she would continue the inner practice and

24
00:03:55,680 --> 00:04:01,840
that she was really already well established in the practice.

25
00:04:01,840 --> 00:04:11,400
So anyway, let's skip right ahead to questions. Questions about the verse I've happened

26
00:04:11,400 --> 00:04:17,440
to take him or the quote, I'm happy to take him, but I think we'll go on. It looks like

27
00:04:17,440 --> 00:04:25,600
we've got more chat than the chat hall. So I can't go back all the way, but scroll through

28
00:04:25,600 --> 00:04:35,480
this looking for questions. Doesn't YouTube give you money when you have a lot of subscribers

29
00:04:35,480 --> 00:04:38,840
and videos? Doesn't that mean you're making money on this? Isn't that against the

30
00:04:38,840 --> 00:04:45,160
rule? Is that against the rule? No, no, YouTube doesn't give you money just because

31
00:04:45,160 --> 00:04:52,800
you have subscribers and videos, but it does if you are a partner and if you add ads

32
00:04:52,800 --> 00:05:01,400
to your video, I am a partner, but which allowed me way back when to upload longer videos,

33
00:05:01,400 --> 00:05:08,400
but I disabled the ad revenue service. I don't know if you get ads on my videos or not,

34
00:05:08,400 --> 00:05:14,920
but if you do, we don't make any money into our organization. Doesn't I don't certainly,

35
00:05:14,920 --> 00:05:22,240
there's no money involved in YouTube. I know it could seem that way. There are even

36
00:05:22,240 --> 00:05:27,360
videos. There's actually one or two videos I think that have a flag for copyright because

37
00:05:27,360 --> 00:05:36,640
of some background, but background music, they're from a DVD or something. I think they

38
00:05:36,640 --> 00:05:43,600
may have ads associated with them, but that's because rather than take the videos down,

39
00:05:43,600 --> 00:05:50,240
the copyright owners are making money off of our videos. We couldn't just take them down,

40
00:05:50,240 --> 00:05:58,400
but whatever, not too concerned. The rest of the videos shouldn't have big ads in front

41
00:05:58,400 --> 00:06:02,080
of them. They may have ads on the page or something, but we don't make any money off of

42
00:06:02,080 --> 00:06:09,280
any of it. If you gain insight and maturity as you meditate about life, then when someone

43
00:06:09,280 --> 00:06:15,240
may be swears at you and I anger instead of responding with anger, wait, I think we've

44
00:06:15,240 --> 00:06:37,520
got to answer this. We haven't answered it. So we are all kind of. Right, remember

45
00:06:37,520 --> 00:06:44,520
others of it. Scroll the hand, oh, these are all the hungry ghosts, okay, they're never cut

46
00:06:44,520 --> 00:06:50,360
up. Should one note swallowing during meditation? Absolutely. If there's swallowing,

47
00:06:50,360 --> 00:06:58,240
one should not swallow. There's some people that then swallow reflex. Can you say swallowing?

48
00:06:58,240 --> 00:07:02,760
When we fall in love, emotionally with someone, can we consider this to be attachment?

49
00:07:02,760 --> 00:07:10,920
If yes, should we recognize it as attachment and let go of this feeling? I mean, what would

50
00:07:10,920 --> 00:07:17,320
you consider besides an attachment? Suppose the problem is we call it love. See, love is

51
00:07:17,320 --> 00:07:25,080
such an ambiguous word in this instance. I mean, love is not the whole story. When you fall

52
00:07:25,080 --> 00:07:30,400
in love, you're very much talking about attachment, but love itself is positive. Love itself

53
00:07:30,400 --> 00:07:39,240
is to wish well for someone. Attachment is wishing for someone to bring you happiness. It's

54
00:07:39,240 --> 00:07:51,000
the desire to be pleased. The attachment to the pleasure that comes from someone's or

55
00:07:51,000 --> 00:07:59,880
some things present. You love someone, you love something. Or yeah, the word we say, but

56
00:07:59,880 --> 00:08:06,600
the meaning is you enjoy seeing, hearing, smelling, tasting, feeling, and even thinking

57
00:08:06,600 --> 00:08:15,880
certain things. But love is love is different. It's just a word. There are certain meanings

58
00:08:15,880 --> 00:08:25,800
of the word love that are different. I mean, wishing for someone to be happy. Kindness,

59
00:08:25,800 --> 00:08:37,640
loneliness. Yes, shouldn't be morning. I was doing on upon us at the meditation. What

60
00:08:37,640 --> 00:08:44,040
will only happen is you should be afraid. Why is meditation impermanent? What can I do to

61
00:08:44,040 --> 00:08:50,680
not get freaked out during meditation? Well, let's see, we don't practice the same meditation

62
00:08:50,680 --> 00:08:54,480
that you were practicing. So the first thing I would suggest is that you read my booklet

63
00:08:54,480 --> 00:09:02,720
and once you remember, we could start practicing in that way. That's how you can overcome

64
00:09:02,720 --> 00:09:07,040
the fact that meditation is impermanent because you learn to let go and not concern

65
00:09:07,040 --> 00:09:13,080
yourself about the fact that it changes. You'll be less moved, less shaken by the vicissant

66
00:09:13,080 --> 00:09:21,760
tunes of experience. I understand that it is wholesome to help people, but what about people

67
00:09:21,760 --> 00:09:28,960
that seem to want to abuse our good will? Should we help them or not? Yes. Yes, I think

68
00:09:28,960 --> 00:09:39,040
we should. I mean, as with everything in Buddhism, it's up to you how far you want to take

69
00:09:39,040 --> 00:09:49,480
things. But in the end, the ideas to not be so concerned, not be concerned about pain,

70
00:09:49,480 --> 00:09:58,040
sickness, and even death. So if you're still worried about helping someone because it might

71
00:09:58,040 --> 00:10:04,200
backfire on you, as a person might not appreciate it or because they might take advantage

72
00:10:04,200 --> 00:10:13,280
of you, because of the unpleasant consequences of being a good person, that's an attachment

73
00:10:13,280 --> 00:10:20,160
and that worry is a cause for suffering and the reaction and the upset of being taken advantage

74
00:10:20,160 --> 00:10:26,560
of it. So due to attachment, the light and being isn't concerned about that. They help

75
00:10:26,560 --> 00:10:36,000
someone and if the person doesn't appreciate it, it's not really their concern. You don't

76
00:10:36,000 --> 00:10:46,360
think it influences their desire to help because they have a pure mind and moreover, if

77
00:10:46,360 --> 00:10:50,440
you're not enlightened, you're trying to have them to cultivate a pure mind. If you look

78
00:10:50,440 --> 00:10:58,600
at some of the joticles, there was this, I mean, believe these joticles are not, some

79
00:10:58,600 --> 00:11:05,640
of them are pretty far out in terms of believing them, but in terms of the moral, no matter

80
00:11:05,640 --> 00:11:11,600
what they're quite profound, whether you believe them to be true or not, the bodhisattva

81
00:11:11,600 --> 00:11:33,480
was a monkey. And if this man fell into a pit in the forest, remember, I think the story

82
00:11:33,480 --> 00:11:37,880
was even longer than this, but anyway, the man fell in a pit and this monkey came along

83
00:11:37,880 --> 00:11:43,320
and pulled him out of the pit. When the man got out of the pit, he bashed, he picked

84
00:11:43,320 --> 00:11:50,080
up a rock and bashed the monkey over the head and thinking he was a human, eat the monkey.

85
00:11:50,080 --> 00:11:56,600
I'm going to take the monkey for food and so he picks up the monkey, throws the monkey

86
00:11:56,600 --> 00:12:03,000
over his shoulder. This after the monkey had saved his life and saved him from this deep

87
00:12:03,000 --> 00:12:10,760
pit, from a hunter's pit or whatever. And then to top it off, the man's walking around

88
00:12:10,760 --> 00:12:17,240
in the forest and realizes he's lost. And the story goes that the monkey tells him,

89
00:12:17,240 --> 00:12:24,880
monkey is still alive, but bleeding out of his head and dying. And the monkey shows him

90
00:12:24,880 --> 00:12:29,320
the way to go, the way to get out of the forest. I mean, the whole point of the story

91
00:12:29,320 --> 00:12:35,240
and it's quite a beautiful story, how selfless and unconcerned the monkey is with his

92
00:12:35,240 --> 00:12:42,760
own well-being and even with the goodness of this person. He's only concerned solely

93
00:12:42,760 --> 00:12:50,960
with his own behavior and his own goodness. His own good needs to help someone. Their

94
00:12:50,960 --> 00:12:56,560
story is like that. It's not the only one. So I think generally the consensus, I don't think

95
00:12:56,560 --> 00:13:03,640
I'm alone in thinking. You don't worry so much about the goodness or even the appreciation

96
00:13:03,640 --> 00:13:09,600
of other people for our good deeds. We do good deeds because they're good. It kind of

97
00:13:09,600 --> 00:13:14,440
goes against a superficial understanding of karma that you do good deeds because they

98
00:13:14,440 --> 00:13:18,880
give you a good result. And so you're always focused on the result. In fact, that's not

99
00:13:18,880 --> 00:13:26,320
really how karma works. Karma is not being focused on the result per se. Except in terms

100
00:13:26,320 --> 00:13:32,280
of how the results, except in terms of the result that they have for your own mind, you

101
00:13:32,280 --> 00:13:36,040
do good deeds because of the effect that they have on your mind. You don't worry about

102
00:13:36,040 --> 00:13:47,600
when it makes you rich or powerful or even brings you pleasurable experiences. If our

103
00:13:47,600 --> 00:13:56,280
natural nature is to break away from some sorrow, how did we end up so attached to

104
00:13:56,280 --> 00:14:03,040
it? It's not our nature to break away from some sorrow. It's our nature to cling for

105
00:14:03,040 --> 00:14:10,280
the most part. I mean, some people do let go, but it's quite rare. It happens. It happens

106
00:14:10,280 --> 00:14:15,880
even without a Buddha where people who become enlightened on their own, but very, very rare.

107
00:14:15,880 --> 00:14:22,200
There's just so much in the universe, so much to cling to, that not clinging is a very

108
00:14:22,200 --> 00:14:31,680
rare thing. Is ambition bad? Generally, yes. I mean, you have to understand that bad

109
00:14:31,680 --> 00:14:39,720
in Buddhism. Buddhism, a lot of things are bad. Most things. Unfortunately, in the end,

110
00:14:39,720 --> 00:14:45,640
many, many things become unwholesome. But all that means is that they're conducive to rebirth.

111
00:14:45,640 --> 00:14:51,960
You consider something evil because it means you're going to be born again. If so, then it's

112
00:14:51,960 --> 00:14:58,320
evil. ambition is evil because it's likely to make you be born again. Which doesn't

113
00:14:58,320 --> 00:15:08,600
sound bad to most people, but unfortunately, it's a cause for more suffering. So eventually,

114
00:15:08,600 --> 00:15:14,240
one who becomes enlightened gives up any desire to be born again. As a result, they don't

115
00:15:14,240 --> 00:15:26,960
have ambition, per se, because they've seen there's nothing worth having ambition for,

116
00:15:26,960 --> 00:15:31,280
and then ambition is based on delusion. It's based on the idea that there's some benefit

117
00:15:31,280 --> 00:15:38,800
from obtaining things that are in the end not worth obtaining. So it's based on delusion,

118
00:15:38,800 --> 00:15:49,360
you know, these are such subtle and such refined things that you probably want to be able,

119
00:15:49,360 --> 00:15:54,560
you probably want to distinguish like the ambition to kill people. That's a very bad ambition.

120
00:15:55,360 --> 00:16:00,640
Emmission to meditate, but it's a fairly benign ambition. And in fact, in a worldly sense,

121
00:16:00,640 --> 00:16:02,640
it's beneficial because it means you're going to meditate.

122
00:16:02,640 --> 00:16:11,280
It's cultivating flexibility for half or full notice position or right application of effort

123
00:16:11,280 --> 00:16:18,960
or just a material pursuit. Well, there's different kinds of meditation out there. So in our

124
00:16:18,960 --> 00:16:29,680
meditation practice, no, it's not of any, nothing to do with effort. Cultivating flexibility in

125
00:16:29,680 --> 00:16:38,800
general isn't, see, it's artificial. You're forcing your body into a stretched state.

126
00:16:38,800 --> 00:16:44,480
I mean, I can do the full notice, but I've never really stretched for it. Not to any appreciable

127
00:16:44,480 --> 00:16:49,760
degree. And that sort of comes as a result of meditation, I would say, I mean, through meditating,

128
00:16:49,760 --> 00:16:58,000
body relaxes. And you can sit for a lotus, but it's not. The Buddha said, be mindful in any

129
00:16:58,000 --> 00:17:03,280
position you're in. It's not. The position isn't special, it's special for certain types of

130
00:17:03,280 --> 00:17:11,520
meditation that involve power, involve mental power, a lot of tranquility meditation. If you want to

131
00:17:11,520 --> 00:17:17,280
gain magical powers, you need a very strong state. Not to become enlightened. It's a different

132
00:17:17,280 --> 00:17:22,880
kind of strength. It's the strength of wisdom, which comes from being objective, not being

133
00:17:22,880 --> 00:17:30,560
partial to one state or another. So concerned about such things. When I'm meditating my

134
00:17:30,560 --> 00:17:34,960
attention shifts from my stomach to other objects, should I know that every time it shifts,

135
00:17:34,960 --> 00:17:39,040
or should I forget about it and stick with the stomach? The stomach's not special in any way.

136
00:17:39,040 --> 00:17:44,400
The stomach is an example. And it's an easy, obvious object that's always there. And a lot of

137
00:17:44,400 --> 00:17:50,560
people obsess about and fixate on the idea that this technique is all about the stomach. It's not.

138
00:17:50,560 --> 00:17:56,800
The stomach is just a really good base object. Everything else is equally valid as an object.

139
00:17:56,800 --> 00:18:02,000
Every other experience you have. So absolutely, you should focus on whatever it is.

140
00:18:02,000 --> 00:18:08,000
Try to come back to the stomach for the sole reason that you don't want to be jumping from one

141
00:18:08,000 --> 00:18:14,320
object to another and eventually fall into this sort of seeking out the next object. Come back

142
00:18:14,320 --> 00:18:21,280
so that you can be sure that you're not seeking out things to note. You're letting them happen

143
00:18:21,280 --> 00:18:28,800
of their own accord. It keeps you grounded. The stomach will keep you grounded. So it's going to

144
00:18:28,800 --> 00:18:37,680
happen back. A question regarding the sixth reasons. What do you suggest you be for upholding

145
00:18:37,680 --> 00:18:45,120
the sixth person with an unsociable sleep pattern? So you just sleep at three to four a.m.

146
00:18:45,120 --> 00:18:50,800
after the broadcast and wake up in the late morning. I simply push forward the time from noon to

147
00:18:50,800 --> 00:18:56,960
four p.m. As I'm still in the evening when I need two meals, I work at home and not even going to

148
00:18:56,960 --> 00:19:06,480
the next day. Sure. I mean, these are concepts. They're conventions. If you want to cultivate you,

149
00:19:06,480 --> 00:19:13,280
you know, I think the orthodox, like probably my teacher would say, well, you're not keeping the sixth

150
00:19:13,280 --> 00:19:18,560
preset just because it's tradition. I mean, you don't want to, you don't want to mess with

151
00:19:20,400 --> 00:19:26,960
conventions because if you start messing with them, then anything goes, right? So technically,

152
00:19:26,960 --> 00:19:32,320
no, you're not keeping the sixth preset. But what I would suggest is you create a new preset.

153
00:19:32,320 --> 00:19:38,800
You're keeping your eight precepts being being clear that you're not actually keeping the sixth

154
00:19:38,800 --> 00:19:43,840
preset. I mean, clear that your precepts are not real precepts. You're not trying to replace

155
00:19:43,840 --> 00:19:51,200
the Buddhist teaching or create your new set. But you're keeping your own practice in line with

156
00:19:51,200 --> 00:19:58,560
the Buddhist teaching. So it's an individual practice on your part. But you're keeping it because

157
00:19:58,560 --> 00:20:04,560
it's very much in line with the sixth preset. It's not the sixth preset and you shouldn't pretend

158
00:20:04,560 --> 00:20:11,200
that it is. But that's only, you know, it's such a technicality and it's just so that you can

159
00:20:11,200 --> 00:20:17,920
be faithful and out of reverence and appreciation for the Buddha. And you consider the precepts

160
00:20:17,920 --> 00:20:26,560
like a statue, a Buddha statue. So you worship the precepts. You venerate and you respect them.

161
00:20:26,560 --> 00:20:31,600
As a result, we don't want to mess with them, even though it's inconvenient. It's not possible

162
00:20:31,600 --> 00:20:37,520
for us to keep the sixth preset. We're going to keep another precept instead that's a practice

163
00:20:37,520 --> 00:20:43,840
that's similar to the sixth preset. Now, absolutely, it's fine. I mean, you understand the intent

164
00:20:43,840 --> 00:20:49,760
and you're just as good. There's, you know, psychologically as well, it's nice to be

165
00:20:49,760 --> 00:21:00,880
to respect the Buddha and his teaching, to the extent that we don't break the rules, even when

166
00:21:00,880 --> 00:21:07,200
it seems silly that we're keeping it. It's not the best practice to keep them, but we still respect

167
00:21:07,200 --> 00:21:13,120
them. As a result, we don't say, well, I'm keeping it. I'm just keeping it different and say,

168
00:21:13,120 --> 00:21:18,320
no, I'm not keeping the sixth preset. But I'm keeping something similar to it,

169
00:21:18,320 --> 00:21:28,000
that'll veneration for it. What do you think about Ramdas' teachings? I don't think about Ramdas'

170
00:21:28,000 --> 00:21:55,600
teachings. I don't even know anything about Ramdas' teachings. There's the best way to go about bringing

171
00:21:55,600 --> 00:22:01,760
a true domain to focus within the group as a visitor rather than a teacher, or claims,

172
00:22:01,760 --> 00:22:08,240
teravada. It has no affiliation with capital. Sangha teaches misguided and inaccurate versions

173
00:22:08,240 --> 00:22:14,480
of meditation. Well, don't be too quick to assume that someone teaches misguided

174
00:22:14,480 --> 00:22:19,600
meditation, just because they don't teach practice the same way as you do. There are many different

175
00:22:19,600 --> 00:22:23,360
kinds of interpretation. I mean, if you don't agree with someone's way of practicing,

176
00:22:23,360 --> 00:22:30,720
don't go to their group. It's the best reason, better than fighting with them or arguing.

177
00:22:32,480 --> 00:22:38,240
Start your own group. That is in your mind, not misguided.

178
00:22:41,920 --> 00:22:45,120
Have a book called the Dhammapada. Is this the same as Dharma?

179
00:22:45,120 --> 00:22:58,400
Have the Dhammapada is a group of verses taught by the Buddha and their stories accompanying

180
00:22:58,400 --> 00:23:07,600
the verses that talk about why the verse was spoken, the context, and there are many versions

181
00:23:07,600 --> 00:23:16,160
of it and translations into English, but it's originally in the Pali. Dhammapada is a Pali word.

182
00:23:16,160 --> 00:23:21,600
And it's the dharma of the Buddha. The dharma is a Sanskrit word. Dhamma is Pali.

183
00:23:22,240 --> 00:23:29,840
Pali is the language close to what the Buddha taught. As far as we know, he didn't speak

184
00:23:29,840 --> 00:23:36,640
Sanskrit. Sanskrit was an artificial sort of formalized version of the language that was being

185
00:23:36,640 --> 00:23:43,760
spoken. Dhamma is more technically correct than Dhamma. Dhamma is a corruption of the word

186
00:23:43,760 --> 00:23:51,440
Dhamma, but it's what people spoke. You know, like shepherd. The word shepherd comes from sheep

187
00:23:51,440 --> 00:24:07,360
herb, or sheep herb, or one who heard sheep, but we changed it to shepherd, and that kind of thing.

188
00:24:07,360 --> 00:24:26,880
Oh, 52 viewers. Hello, everyone. It's to say people turning in. Tomorrow, I think it's a bit

189
00:24:26,880 --> 00:24:33,040
ambiguous now because internet's not coming to the new place until Thursday. And we have

190
00:24:33,040 --> 00:24:43,200
internet here until then. We have this place until August. The problem is Wednesday is the day I

191
00:24:43,200 --> 00:24:49,040
have to move in. Wednesday is the beginning of the rains. So Wednesday, I will be at the new place.

192
00:24:50,160 --> 00:24:55,040
It means Wednesday, I'm pretty sure we won't have broadcast. Tomorrow, we might have broadcast.

193
00:24:56,480 --> 00:24:59,280
Tomorrow, I can come back here to do broadcast.

194
00:24:59,280 --> 00:25:06,160
We have a new meditator at the new center. I haven't even met him because I'm here at the old place.

195
00:25:06,160 --> 00:25:14,400
That's tomorrow, I have to go meet him and give him his first meeting with him in the next exercise.

196
00:25:17,120 --> 00:25:20,160
I have to think about that tomorrow night, we may not have

197
00:25:20,160 --> 00:25:29,520
may not have broadcast. Wednesday, we probably won't. And Thursday, we most likely will again.

198
00:25:35,840 --> 00:25:42,000
I was planning on recruiting from the group recruiting. What are you starting an army or a cult?

199
00:25:42,000 --> 00:25:49,200
Oh, you're starting a wrong group. Well, I mean, I wouldn't really start in

200
00:25:53,840 --> 00:26:00,080
probably the best thing you can do is do a meditation course first to settle yourself in the

201
00:26:00,080 --> 00:26:08,480
practice, finish a meditation course in whatever tradition in our tradition and then become a

202
00:26:08,480 --> 00:26:23,440
leader in that regard. You can still start a group. It's just not likely to be as effective if you

203
00:26:23,440 --> 00:26:33,680
don't have your own intensive meditation background. If you're unable to come into a course and

204
00:26:33,680 --> 00:26:43,120
sure start a group, just meditate together or ways to advertise like there's this meetup.com,

205
00:26:43,120 --> 00:26:50,800
I think it's a good one. It's not a different way as you can advertise. I wouldn't go like poaching

206
00:26:50,800 --> 00:27:05,440
other people's meditators. I don't know how kosher that is. What's the difference between

207
00:27:05,440 --> 00:27:09,680
formal meditation and just sitting down doing nothing in particular, just being mindful.

208
00:27:09,680 --> 00:27:20,480
What's the difference? When you sit down and be mindful, you're meditating.

209
00:27:22,240 --> 00:27:28,000
I mean, we have a formal meditation technique and it gets quite involved actually throughout the

210
00:27:28,000 --> 00:27:38,480
course. When you're doing formal meditation, it's a little more psychologically involved because

211
00:27:38,480 --> 00:27:45,600
you're intent upon it. You're not doing anything else. You just sit down and be a little bit

212
00:27:45,600 --> 00:27:52,800
mindful while the mind is still free to get lost in other things. There's not as much of a

213
00:27:53,920 --> 00:28:02,720
psychological focus or attachment to the practice. The mind is free to flip,

214
00:28:02,720 --> 00:28:08,560
free to decide, you know, or do something else. It's the determination. When you set your alarm

215
00:28:08,560 --> 00:28:18,720
for 30 minutes, that's kind of making an aditana. I'm going to sit for 30 minutes. That does

216
00:28:18,720 --> 00:28:24,480
something to your mind. There is a difference. That's the difference. It's the intention. You have

217
00:28:24,480 --> 00:28:38,880
the preliminary intention to do something and that gives you a power. It's great. Can give you a

218
00:28:54,480 --> 00:29:24,400
more questions? All right. I think tomorrow, so maybe not here tomorrow. Maybe

219
00:29:24,400 --> 00:29:28,480
the next time if you've got questions, you better ask them because probably won't have

220
00:29:28,480 --> 00:29:40,640
another chance before Thursday. Maybe tomorrow. Maybe not. Probably not. If I'm going to move

221
00:29:40,640 --> 00:29:48,800
tomorrow, maybe tomorrow is the, tomorrow is the Salah Pooja, which is the full moon in Salah,

222
00:29:48,800 --> 00:29:54,400
which is when the Buddha taught the Dhamma Chakapawatanasana, a very important discourse.

223
00:29:57,760 --> 00:30:00,320
Maybe I'll try to do a broadcast special broadcast.

224
00:30:03,680 --> 00:30:08,480
During walking meditation, I know sometimes I have double correctness, for example, turning,

225
00:30:08,480 --> 00:30:19,360
turning, thinking, thinking. I don't understand.

226
00:30:24,480 --> 00:30:29,840
Familiar with the Dhamma Chaya movement in Thailand, oh yes. It's familiar as I want to be.

227
00:30:29,840 --> 00:30:41,600
They have some very, very strange views. Anyone who's wondering about the Dhamma Chaya

228
00:30:41,600 --> 00:30:50,960
movement should really read this essay by this former monk, former Dhamma Chaya inner circle

229
00:30:50,960 --> 00:30:56,640
monk. I think he's not a monk even anymore, but man, he's got some interesting things to say,

230
00:30:56,640 --> 00:31:03,840
but Dhamma Chaya, I spent five hours listening to, we had a private interview with advice,

231
00:31:03,840 --> 00:31:09,120
had the Dhamma Chaya number two guy. Number one guy doesn't give that kind of thing, but number two

232
00:31:09,120 --> 00:31:17,680
guy, because it was involved with the project to do a documentary on meditation,

233
00:31:18,960 --> 00:31:24,800
and we wanted to do something broad about meditation in general all over the world, and the Dhamma

234
00:31:24,800 --> 00:31:30,880
Chaya guys were having nothing, nothing to do with it. They wanted it to just be about them,

235
00:31:30,880 --> 00:31:36,160
their meditation. So he spent five hours explaining to us the Dhamma Chaya practice,

236
00:31:37,120 --> 00:31:46,320
and it was strange to say the least. I mean, some of it wasn't bad, but they had this idea that

237
00:31:46,320 --> 00:31:55,280
the mind is this big, and it exists here in your center of gravity, and there's some crazy things.

238
00:31:59,760 --> 00:32:04,160
Two things happening at once, when I'm turning my feet in body thoughts, and they, well, there you go,

239
00:32:04,800 --> 00:32:10,560
you see that the mind is not under your control, it's impermanent, unsatisfying, and controllable.

240
00:32:10,560 --> 00:32:13,760
So good, because it helps you to let go, it's frustrating in the beginning,

241
00:32:13,760 --> 00:32:20,480
and that's good, because it challenges you to let go, so that you're stopping frustrated about it.

242
00:32:22,560 --> 00:32:26,080
It's a difficult situation, difficult situations are good for showing you

243
00:32:26,800 --> 00:32:31,040
your potential reactions to difficulty, to unpleasantness.

244
00:32:37,040 --> 00:32:39,760
What are the main differences between Taoism and Buddhism?

245
00:32:39,760 --> 00:32:44,320
I mean, they're both so complex, you know, what do you mean?

246
00:32:44,320 --> 00:32:48,800
Much Buddhism, much Taoism, do you mean the Tao-de-ching and the Deepitika?

247
00:32:49,520 --> 00:32:55,280
The Tao-de-ching is a pretty awesome book, I think, and it's very, it's much in line with Buddhism.

248
00:32:55,280 --> 00:33:01,600
There are some strange oddities in it, like talking about heaven and the masculine and the feminine,

249
00:33:01,600 --> 00:33:10,320
if that's how you translate it, it's not entirely Buddhist necessarily, but that's a good book.

250
00:33:12,080 --> 00:33:17,280
But Taoism, Taoism, kind of weird. I mean, there's a lot of alchemy in that kind of thing.

251
00:33:17,280 --> 00:33:33,200
I mean, I'm not really interested in answering that question, not on this one.

252
00:33:47,760 --> 00:34:01,920
All right, okay, I'm going to stop it there.

253
00:34:03,120 --> 00:34:07,200
Well, they'll see, maybe we'll have a special broadcast, so there's a new question.

254
00:34:08,720 --> 00:34:13,440
All right, one more, this is it. How do I cultivate empathy? Just being able to understand

255
00:34:13,440 --> 00:34:18,720
others feelings, especially negative ones. For example, someone else is being bullied.

256
00:34:19,360 --> 00:34:25,200
I know I should try to walk in their shoes instead of feeling contempt. Are there ways to go about

257
00:34:25,200 --> 00:34:32,640
this? It's kind of, we've answered this, I've answered this before, like yesterday, I think, but

258
00:34:34,960 --> 00:34:40,480
you don't have to worry so much about empathy. Concern yourself with letting go, letting go of the

259
00:34:40,480 --> 00:34:46,400
anger, letting go of the aversion towards the person. When you do that, and when you're in general

260
00:34:46,400 --> 00:34:55,040
mindful, be much more in tune with people's emotions and their defilements, be able to see

261
00:34:55,040 --> 00:35:01,600
people's problems, be much better able to handle them and understand them. Because you'll be

262
00:35:01,600 --> 00:35:10,240
present, you really will be alert and attentive to other people. And being free from your reactions,

263
00:35:10,240 --> 00:35:15,360
you'll be able to react appropriately. If someone wants something from me and me much more inclined

264
00:35:15,360 --> 00:35:21,680
to just give it to them, to help them, it's just the easy way. I mean, it's just natural why

265
00:35:21,680 --> 00:35:31,200
wouldn't you? Why fight? Why avoid the situation? So empathy is the kind of thing that

266
00:35:31,200 --> 00:35:36,880
comes from being enlightened, it comes from mindfulness, it comes from being pure of mind.

267
00:35:36,880 --> 00:35:41,760
So very much more about your own mental purity and things like empathy, compassion, and so on,

268
00:35:41,760 --> 00:35:50,720
we'll come from it. It just makes us new walking meditation whilst commuting, like on the bus.

269
00:35:50,720 --> 00:35:59,360
That's the commuting. I don't know how that could be possible in the train, maybe.

270
00:36:02,400 --> 00:36:07,280
Sure, don't worry too much about it. Walking meditation is good when you're doing a full day of

271
00:36:07,280 --> 00:36:17,360
meditation. That's really what it's best for, because you don't want to be sitting around all day.

272
00:36:17,360 --> 00:36:23,360
But if you're walking around all day anyway, you're just sitting in meditation easy now.

273
00:36:24,880 --> 00:36:29,600
On the other hand, walking meditation has its own benefits and it's not to be discarded,

274
00:36:31,040 --> 00:36:37,040
so it is good to do it. But if you're commuting, there wouldn't be too concerned about walking

275
00:36:37,040 --> 00:36:43,440
meditation. When you're walking to work, yeah, when you're walking to work, do them sort of a

276
00:36:43,440 --> 00:36:48,000
simple walking, like walking, walking, or right, like, I mean, that's the sort of natural thing you

277
00:36:48,000 --> 00:36:54,080
should do anyway. I didn't realize commuting meant walking, walking could mean commuting.

278
00:36:55,520 --> 00:37:02,160
Not really sure what that word originally meant, but in my mind, commuting means being on a vehicle.

279
00:37:02,160 --> 00:37:16,800
So it's always been hard to maybe a British, I mean British. British way of...

280
00:37:16,800 --> 00:37:32,720
Okay, enough. Thank you. Have a good night. See you all again soon.

