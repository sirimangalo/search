1
00:00:00,000 --> 00:00:28,800
Good evening everyone, broadcasting live, May 10th, May 10th.

2
00:00:28,800 --> 00:00:40,800
Today's quote is on the Buddha, the Dhamma, and the Sangha.

3
00:00:40,800 --> 00:00:52,400
It's from the Tarigatha actually, the verses of the elder monks.

4
00:00:52,400 --> 00:01:10,600
We have recorded verses that were spoken by various aarans as poetry and reflection on their attainments.

5
00:01:10,600 --> 00:01:12,800
And we have stories behind each one of these.

6
00:01:12,800 --> 00:01:25,200
So this one is from a monk called Teikitchakari, Teikitchakari.

7
00:01:25,200 --> 00:01:33,600
One who does Teikitchakari.

8
00:01:33,600 --> 00:01:38,800
Let's see if we can look at this.

9
00:01:38,800 --> 00:01:46,800
He was the son of a brahminsubandhu, so called because he brought safety into the world.

10
00:01:46,800 --> 00:01:52,800
He was brought safely into the world with the aid of physicians.

11
00:01:52,800 --> 00:02:02,800
I don't understand the word.

12
00:02:02,800 --> 00:02:11,000
When he was grown up, his father incurred the jealousy and suspicion of a minister, Chanaka,

13
00:02:11,000 --> 00:02:15,600
Chanaka, and Chandagutta, who had him through.

14
00:02:15,600 --> 00:02:18,400
His father was put in prison.

15
00:02:18,400 --> 00:02:24,400
And so the son Teikitchakari and his fright fled, and taking refuge with the forest,

16
00:02:24,400 --> 00:02:30,200
dwelling monk, entered the order, and dwelt in the open air, never sleeping in heedless

17
00:02:30,200 --> 00:02:44,200
of heat and cold.

18
00:02:44,200 --> 00:02:51,200
Many other reasons why people become monks.

19
00:02:51,200 --> 00:03:00,200
And Nagasena says, he's a Melinda-esque king, Melinda asks him, why did you become a monk?

20
00:03:00,200 --> 00:03:02,200
Did you become a monk to become an arrow?

21
00:03:02,200 --> 00:03:03,200
Well, that was young.

22
00:03:03,200 --> 00:03:08,400
It's kind of embarrassed the way he says it.

23
00:03:08,400 --> 00:03:15,200
Some people become monks for the wrong reason.

24
00:03:15,200 --> 00:03:21,200
But then find the right reason after they ordained.

25
00:03:21,200 --> 00:03:26,200
But it's an important part of society.

26
00:03:26,200 --> 00:03:44,200
I think to have this institution, this option for people who are...

27
00:03:44,200 --> 00:03:55,200
Who are at their stage, at a point in their lives, or in their journey, and some sorrow,

28
00:03:55,200 --> 00:04:09,200
that they are able and willing, that they are turned off by the world.

29
00:04:09,200 --> 00:04:22,200
Having this ability to choose to dedicate yourself to meditation and study and teaching

30
00:04:22,200 --> 00:04:30,200
of spiritual truths and the path to enlightenment and so on.

31
00:04:30,200 --> 00:04:36,200
I mean, this is not only are these sort of people important, people who have the time

32
00:04:36,200 --> 00:04:47,200
and energy and the knowledge and the practice to teach, but also for those who are seeking it.

33
00:04:47,200 --> 00:05:00,200
Those who can't find a place in the world who have an earnest wish to become a spiritual person

34
00:05:00,200 --> 00:05:08,200
to live a reclusive life.

35
00:05:08,200 --> 00:05:12,200
Anyway, so this monk's verses are about the Buddha, the demon, the Sangha.

36
00:05:12,200 --> 00:05:27,200
He says, Buddha, my young, Anusara, a person, no one should reflect with a good heart,

37
00:05:27,200 --> 00:05:35,200
with a pure heart on the Buddha who is immeasurable.

38
00:05:35,200 --> 00:06:04,200
Bhitya, Bhutasadhi, Roho, he said that the Buddha goes.

39
00:06:04,200 --> 00:06:15,200
One will always be always be uplifted, would be constantly uplifted.

40
00:06:15,200 --> 00:06:21,200
The same with the demon, the Sangha, these are just reflecting on the Buddha, the demon,

41
00:06:21,200 --> 00:06:30,200
the Sangha, just thinking about these things, just reminiscing, or revering and just that,

42
00:06:30,200 --> 00:06:39,200
not even practicing it, they are so powerful that even worshipping them is a great benefit.

43
00:06:39,200 --> 00:06:50,200
It makes you uplifted, undago, undago, makes you uplifted, satatat, continuously.

44
00:06:50,200 --> 00:06:57,200
So if you keep the Buddha, the demon, the Sangha in mind, you will never be depressed.

45
00:06:57,200 --> 00:07:08,200
There was one, I told the stories several times about this woman in the US who was in a very bad situation.

46
00:07:08,200 --> 00:07:16,200
So I was usually doing some meditation, but she was basically trapped in somebody else's house, not physically,

47
00:07:16,200 --> 00:07:22,200
but for all intents and purposes quite trapped, she was in some bad situation.

48
00:07:22,200 --> 00:07:31,200
They were sort of very fundamentalist, non-Buddhist religious people.

49
00:07:31,200 --> 00:07:39,200
So I told her also to recite some chanting to the Buddha in the demon and Sangha,

50
00:07:39,200 --> 00:07:42,200
just short chance and she had a Buddha image and she was doing this,

51
00:07:42,200 --> 00:07:46,200
and then the family confiscated the Buddha image.

52
00:07:46,200 --> 00:07:49,200
But the power of it, they were going to destroy this Buddha image,

53
00:07:49,200 --> 00:07:57,200
but the power of her practiced, and her devotion to the Buddha,

54
00:07:57,200 --> 00:08:03,200
coupled with their hatred and vilification of the Buddha.

55
00:08:03,200 --> 00:08:07,200
She had to find someone to take this Buddha image away, or they were going to destroy it.

56
00:08:07,200 --> 00:08:12,200
And so she found someone to take it, and when they came to take it, they talked to her,

57
00:08:12,200 --> 00:08:21,200
and out of mutual sort of appreciation, they found a job for her and she moved.

58
00:08:21,200 --> 00:08:24,200
All because of this Buddha image and because of her practice,

59
00:08:24,200 --> 00:08:29,200
she was able to buy chance meet these people who were also, I guess, Buddhist,

60
00:08:29,200 --> 00:08:37,200
or at least amicable towards Buddhism.

61
00:08:37,200 --> 00:08:42,200
And was able to leave the house right away.

62
00:08:42,200 --> 00:08:45,200
It was quite an inspiring story.

63
00:08:45,200 --> 00:08:50,200
All because of her chanting, the interactive concept.

64
00:08:50,200 --> 00:08:54,200
Oil and water can't mix, good and evil.

65
00:08:54,200 --> 00:09:03,200
It'll hard time staying in the same house.

66
00:09:03,200 --> 00:09:07,200
So it's important to, this is one of the protective meditations,

67
00:09:07,200 --> 00:09:10,200
meditating on the Buddha, the Dunman, the Sangha.

68
00:09:10,200 --> 00:09:22,200
Protects, keeps you safe, keeps your mind strong and vibrant, ecstatic, energetic.

69
00:09:22,200 --> 00:09:25,200
Something worth doing.

70
00:09:25,200 --> 00:09:29,200
Worth reading about the Buddha, reading about his life, worth going to

71
00:09:29,200 --> 00:09:34,200
India to see where the Buddha lived, all these things to make the Buddha a part of your life

72
00:09:34,200 --> 00:09:42,200
and a part of your conscious experience.

73
00:09:42,200 --> 00:09:47,200
So anyway, not too much to say about that.

74
00:09:47,200 --> 00:10:01,200
I have questions tonight.

75
00:10:01,200 --> 00:10:04,200
I'm going to just skip questions that don't have the queue in front,

76
00:10:04,200 --> 00:10:08,200
because otherwise it makes it so much easier to skim through.

77
00:10:08,200 --> 00:10:12,200
So they don't have the special queue in front, special question,

78
00:10:12,200 --> 00:10:17,200
this one is, well, I won't go look at them.

79
00:10:17,200 --> 00:10:20,200
The Buddha says I said his question was stupid.

80
00:10:20,200 --> 00:10:22,200
I didn't really say that today.

81
00:10:22,200 --> 00:10:26,200
Maybe I choked about it or something.

82
00:10:26,200 --> 00:10:32,200
Do you ever read great works from later Buddhist masters, like Nagarjuna?

83
00:10:32,200 --> 00:10:35,200
Nope, don't, not so much.

84
00:10:35,200 --> 00:10:41,200
But I have heard good things about Nagarjuna.

85
00:10:41,200 --> 00:10:43,200
I wouldn't be against reading it.

86
00:10:43,200 --> 00:10:48,200
I mean, I think that's the sort of Mahayana stuff that I'd actually be interested in.

87
00:10:48,200 --> 00:10:58,200
Suppose to East Asian Mahayana stuff, which is quite unpalatable to me.

88
00:10:58,200 --> 00:11:01,200
I imagine I'd probably have some critical,

89
00:11:01,200 --> 00:11:07,200
be fairly critical of Nagarjuna personally, but I don't know.

90
00:11:07,200 --> 00:11:12,200
I'm sure I'd find some of it impressive.

91
00:11:12,200 --> 00:11:17,200
I understand he was a fairly impressive person.

92
00:11:17,200 --> 00:11:22,200
No other questions?

93
00:11:22,200 --> 00:11:27,200
And a lot last night we must have gone through them all.

94
00:11:27,200 --> 00:11:31,200
Michael tells me one of my videos goes on Reddit.

95
00:11:31,200 --> 00:11:38,200
I'm on Reddit in the past few days or something.

96
00:11:38,200 --> 00:11:42,200
And it's very highly ranked or something like that.

97
00:11:42,200 --> 00:11:47,200
Under the meditation sun-breaded.

98
00:11:47,200 --> 00:11:51,200
Why did Anandah take so long to become an Rahaan?

99
00:11:51,200 --> 00:11:55,200
Because he was very busy with looking after the Buddha.

100
00:11:55,200 --> 00:12:00,200
He spent all this time caring for the Buddha that he didn't actually meditate much.

101
00:12:00,200 --> 00:12:03,200
And he was concerned about this himself that the Buddha said,

102
00:12:03,200 --> 00:12:05,200
don't be concerned after I be attained,

103
00:12:05,200 --> 00:12:11,200
by any bond that you will become an Rahaan.

104
00:12:11,200 --> 00:12:16,200
What's the name of the chanting you mentioned?

105
00:12:16,200 --> 00:12:30,200
Well, reflection on the Buddha that I'm in the Sangha.

106
00:12:30,200 --> 00:12:32,200
So if you're looking at any good Buddhist,

107
00:12:32,200 --> 00:12:37,200
Theravada Buddhist chanting, look, you'll find these three things.

108
00:12:37,200 --> 00:12:39,200
It'd be so Swagat, don't you?

109
00:12:39,200 --> 00:12:42,200
But if you want to look it up, it's in the Dajika Sutta.

110
00:12:42,200 --> 00:12:49,200
So the Buddha actually recited these as an example for us.

111
00:12:49,200 --> 00:12:57,200
The top of the banner or the banner protection or something.

112
00:12:57,200 --> 00:13:07,200
Then, then, then, then, Janga.

113
00:13:07,200 --> 00:13:13,200
A little Buddha, yes, I did see it. And in fact, when I was before I was a monk,

114
00:13:13,200 --> 00:13:21,200
my late teacher had me stitched together the Buddha story parts.

115
00:13:21,200 --> 00:13:23,200
A little Buddha.

116
00:13:23,200 --> 00:13:26,200
It has a good Buddha story of the Buddha.

117
00:13:26,200 --> 00:13:30,200
I mean, that's, again, that movie stops as soon as he becomes a Buddha.

118
00:13:30,200 --> 00:13:36,200
And so it's like, very so very Mahayana in a sense that doesn't care about the Buddha in his life.

119
00:13:36,200 --> 00:13:46,200
It's all about how to become a Buddha, which is disappointing from the point of view of those of us who appreciate the forty-five years that came after.

120
00:13:46,200 --> 00:13:56,200
Much more, maybe not much more, but consider it to be actually much more important.

121
00:13:56,200 --> 00:14:16,200
They always skipped the important part from my point of view.

122
00:14:16,200 --> 00:14:27,200
Yeah, the Buddha story part was not entirely quoting to the texts, but it wasn't bad.

123
00:14:27,200 --> 00:14:34,200
I mean, I'm a little older. I'm 37 yesterday, so 20 years ago isn't all that.

124
00:14:34,200 --> 00:14:40,200
But I guess I must have seen it after I became Buddhist.

125
00:14:40,200 --> 00:14:56,200
So some time in 2000, 2001, from...

126
00:14:56,200 --> 00:15:09,200
Keanu Reeves.

127
00:15:09,200 --> 00:15:12,200
All right, let's call it there.

128
00:15:12,200 --> 00:15:15,200
Wish you all a good night.

129
00:15:15,200 --> 00:15:16,200
Good practice.

130
00:15:16,200 --> 00:15:19,200
Hope, we got one more.

131
00:15:19,200 --> 00:15:25,200
So I think you would address the discussion of doing deeds without expecting a return in regards to last night's quoted.

132
00:15:25,200 --> 00:15:29,200
So I did, didn't I? Last night I talked about it.

133
00:15:29,200 --> 00:15:34,200
Maybe disagree, but I can do it again.

134
00:15:34,200 --> 00:15:40,200
If you do deeds without expecting anything in return, it's, you know, what good is it?

135
00:15:40,200 --> 00:15:47,200
Why are you doing them?

136
00:15:47,200 --> 00:15:57,200
I guess I suppose an Arahan acts in that way, but that's because they've done what needs to be done.

137
00:15:57,200 --> 00:16:01,200
For anyone who hasn't done what needs to be done, they have to do things expecting yourself.

138
00:16:01,200 --> 00:16:04,200
I'm looking for myself, you know, purposefully.

139
00:16:04,200 --> 00:16:09,200
So doing good deeds is purposeful, and Arahan has no purpose, so they do good deeds without.

140
00:16:09,200 --> 00:16:18,200
But they don't, they don't purposefully do, they just do things as a matter of course.

141
00:16:18,200 --> 00:16:23,200
Or you could put it that way that they do deeds without expecting anything, but that's the way of it.

142
00:16:23,200 --> 00:16:28,200
If someone who's already done what needs to be done, got done.

143
00:16:28,200 --> 00:16:36,200
We have not yet done what needs to be done, so that's why we do good deeds.

144
00:16:36,200 --> 00:16:43,200
But that's not even really the point of the quote, the quote isn't that you do, isn't saying that you do something expecting to rejoice.

145
00:16:43,200 --> 00:16:46,200
It's saying that when you do good deeds, you do rejoice.

146
00:16:46,200 --> 00:16:48,200
It's not quite even, yeah.

147
00:16:48,200 --> 00:16:57,200
When you feel happy, happiness comes from doing good deeds, whether you want it or not.

148
00:16:57,200 --> 00:17:05,200
And that's what it's saying, it's not saying, you should do good deeds wanting to feel happy.

149
00:17:05,200 --> 00:17:14,200
I mean, it's a way of explaining the difference between good and evil.

150
00:17:14,200 --> 00:17:16,200
Evil leads to suffering.

151
00:17:16,200 --> 00:17:19,200
It does sochity, it does sochity.

152
00:17:19,200 --> 00:17:31,200
They serve, they weep, they sorrow here, they're sad here, they're sad in the next life.

153
00:17:31,200 --> 00:17:39,200
Those who do evil deeds.

154
00:17:39,200 --> 00:17:43,200
Maybe that clears it up a little.

155
00:17:43,200 --> 00:17:45,200
Okay, you're welcome.

156
00:17:45,200 --> 00:17:50,200
I didn't really call your questions stupid, did I? I don't do that.

157
00:17:50,200 --> 00:17:59,200
I know I can be insensitive sometimes, I apologize.

158
00:17:59,200 --> 00:18:01,200
I blame it on my parents.

159
00:18:01,200 --> 00:18:03,200
I didn't go to public school.

160
00:18:03,200 --> 00:18:08,200
I was home schooled through my childhood, so I don't know how to deal with people.

161
00:18:08,200 --> 00:18:22,200
I've never been good with social morals and norms.

162
00:18:22,200 --> 00:18:24,200
I didn't say it was either.

163
00:18:24,200 --> 00:18:26,200
I don't believe you.

164
00:18:26,200 --> 00:18:34,200
I think you're making this up.

165
00:18:34,200 --> 00:18:38,200
But anyway, it's in the past, so I'm not going to dwell.

166
00:18:38,200 --> 00:18:41,200
I just should you.

167
00:18:41,200 --> 00:18:43,200
Okay, good night everyone.

168
00:18:43,200 --> 00:18:54,200
Thank you all for tuning in, wishing you all the best.

169
00:18:54,200 --> 00:18:57,200
Thank you.

