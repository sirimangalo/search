1
00:00:00,000 --> 00:00:26,000
Good evening, everyone broadcasting live.

2
00:00:26,000 --> 00:00:38,000
I'm going to have to live March 19th.

3
00:00:38,000 --> 00:00:54,000
I think everyone's doing everything's okay.

4
00:00:54,000 --> 00:01:23,000
Thank you.

5
00:01:23,000 --> 00:01:36,000
Alright, just reading through the comment.

6
00:01:36,000 --> 00:01:39,000
The night's quote.

7
00:01:39,000 --> 00:01:44,000
It's actually a series of quotes.

8
00:01:44,000 --> 00:01:49,000
Four verses from the Dumbo Panda.

9
00:01:49,000 --> 00:02:05,000
One of which we've actually studied in our Dumbo Panda verses.

10
00:02:05,000 --> 00:02:23,000
Not the faults of others, not what they have done, not what others have done and not done.

11
00:02:23,000 --> 00:02:36,000
I don't know what are we here, one should look upon one's self, look upon those of one's self.

12
00:02:36,000 --> 00:02:44,000
Then things done and not done.

13
00:02:44,000 --> 00:02:58,000
This is a theme that runs through a lot of Buddhism, understanding that the Buddha actually taught us to look after ourselves.

14
00:02:58,000 --> 00:03:05,000
I'm actually on face value, this one says something a little bit different.

15
00:03:05,000 --> 00:03:10,000
Don't go around blaming other people.

16
00:03:10,000 --> 00:03:18,000
Looking at the faults of others, criticizing others, but you could expand that to talk.

17
00:03:18,000 --> 00:03:32,000
Maybe about concerning worrying about others, trying to fix everyone else's problems.

18
00:03:32,000 --> 00:03:44,000
There's another Dumbo Panda verse actually, it might be one of these four.

19
00:03:44,000 --> 00:03:49,000
I think it's missing here.

20
00:03:49,000 --> 00:04:00,000
There's another one that's a Tanameva, Patamong, Patiwopi, Unevese, one should set one's self in what is right first.

21
00:04:00,000 --> 00:04:03,000
And only then one should one teach other.

22
00:04:03,000 --> 00:04:17,000
Why is one willing to file themselves, which is really the best explanation of what's going on.

23
00:04:17,000 --> 00:04:25,000
In some of these quotes, in order to help others, you really have to help yourself.

24
00:04:25,000 --> 00:04:43,000
And so there's this perception that it's selfish, self centered selfish, to go off in your room and practice meditation,

25
00:04:43,000 --> 00:04:52,000
to come to a meditation center, spend your time meditating apropos.

26
00:04:52,000 --> 00:04:57,000
Just this morning, something happened that I didn't really expect to happen in Canada.

27
00:04:57,000 --> 00:05:05,000
I suppose, thinking about it, it's happened many years ago, but it hasn't happened in many years.

28
00:05:05,000 --> 00:05:15,000
Someone in a van just down the street here was walking down the street and a white van.

29
00:05:15,000 --> 00:05:22,000
As it was driving by, someone shouted out, get a job.

30
00:05:22,000 --> 00:05:24,000
Couldn't really believe.

31
00:05:24,000 --> 00:05:27,000
It was a surprise.

32
00:05:27,000 --> 00:05:39,000
Someone told me I should get a job, which I think is apropos because there's a sense that spiritual practice is selfish.

33
00:05:39,000 --> 00:05:46,000
Lazy, you know.

34
00:05:46,000 --> 00:05:59,000
That shout, that experience this morning, actually, there's much more I could say on that, but not that road.

35
00:05:59,000 --> 00:06:06,000
Let's not go too far afield, but there is this sense.

36
00:06:06,000 --> 00:06:18,000
Which is a shame, it shows a misunderstanding of what is truly a benefit and where one can be of most benefit.

37
00:06:18,000 --> 00:06:22,000
How one can best benefit the world?

38
00:06:22,000 --> 00:06:29,000
If everyone were to better themselves, then it's the first thing you can say.

39
00:06:29,000 --> 00:06:35,000
If everyone were to work on themselves, everyone in the world, and who would need to help anyone else.

40
00:06:35,000 --> 00:06:47,000
The second thing you could say is that someone who has worked on themselves is in a far better position to help others.

41
00:06:47,000 --> 00:06:57,000
If I just think about all the horrible things I did to other people before I knew anything about meditation.

42
00:06:57,000 --> 00:07:07,000
I mean things I've done and a pleasantness I've created suffering I've caused.

43
00:07:07,000 --> 00:07:10,000
And people who try to help do this as well.

44
00:07:10,000 --> 00:07:26,000
You try to help and you just end up confusing this situation because you yourself are full of biases and delusions and opinions and views that are not really based on reality.

45
00:07:26,000 --> 00:07:41,000
Meditation is actually really the highest duty that we have.

46
00:07:41,000 --> 00:07:46,000
It's not an indulgence or laziness.

47
00:07:46,000 --> 00:07:51,000
Meditation is doing your duty.

48
00:07:51,000 --> 00:08:00,000
Just like you wouldn't walk into a crowded place if you haven't showered, you haven't cleaned your body.

49
00:08:00,000 --> 00:08:17,000
If you haven't cleaned your clothes, washed your clothes, you wouldn't go walking into a public place because there's a sense that you have a duty to be clean and not smell.

50
00:08:17,000 --> 00:08:21,000
It's unpleasant in this world.

51
00:08:21,000 --> 00:08:30,000
And the same goes far more important skill with our mind that we don't do our duty.

52
00:08:30,000 --> 00:08:36,000
We have a duty to clean our minds because we don't do this.

53
00:08:36,000 --> 00:08:47,000
We fight and we argue and we manipulate each other and we cling to each other.

54
00:08:47,000 --> 00:08:58,000
We're not ready to face the world, not not equipped to live in peace and harmony.

55
00:08:58,000 --> 00:09:06,000
We're defiled, we want to put it in very harsh and sort of negative way.

56
00:09:06,000 --> 00:09:10,000
In a very real sense, we are defiled until we clean ourselves.

57
00:09:10,000 --> 00:09:13,000
We shouldn't, we shouldn't get allowed to go in public.

58
00:09:13,000 --> 00:09:17,000
We shouldn't try to interact with other people.

59
00:09:17,000 --> 00:09:29,000
This is why people become monks go off in the forest because they have a job to do because they don't want to get a job.

60
00:09:29,000 --> 00:09:31,000
They have a job.

61
00:09:31,000 --> 00:09:33,000
They're doing the most important job.

62
00:09:33,000 --> 00:09:42,000
This is why we appreciate and welcome people who want to come and meditate because we feel they're doing the world of service.

63
00:09:42,000 --> 00:09:47,000
They're bettering the world just by learning about themselves.

64
00:09:47,000 --> 00:09:54,000
It's like I would say someone who keeps five precepts actually is giving a great gift to the world.

65
00:09:54,000 --> 00:09:58,000
They're not really doing something for themselves.

66
00:09:58,000 --> 00:10:03,000
They're bestowing freedom, freedom from danger.

67
00:10:03,000 --> 00:10:15,000
That no being on or in the universe has reason to fear this person when they stop killing and stealing and cheating and lying.

68
00:10:15,000 --> 00:10:21,000
They free so many beings from fear, all beings actually.

69
00:10:21,000 --> 00:10:43,000
No being in the universe has reason to fear that person.

70
00:10:43,000 --> 00:10:52,000
So helping oneself is very important.

71
00:10:52,000 --> 00:10:59,000
When one looks at another's faults and it's always that's trying to find this quote as well.

72
00:10:59,000 --> 00:11:15,000
When you're having them come to find it.

73
00:11:15,000 --> 00:11:43,000
When a person looks at their faults, so whether it's neat junk junk junk junk junk, and you know, always perceiving fault, always full of envy.

74
00:11:43,000 --> 00:11:51,000
You tell me what John is telling me that envy is there.

75
00:11:51,000 --> 00:11:54,000
It's not envy.

76
00:11:54,000 --> 00:12:06,000
What John is telling me is picking on others, really seeing faults in others.

77
00:12:06,000 --> 00:12:12,000
Faro and Janupasisa.

78
00:12:12,000 --> 00:12:15,000
For such a person, also what that's a identity.

79
00:12:15,000 --> 00:12:21,000
Such a person that also has the taints, grow, increase.

80
00:12:21,000 --> 00:12:39,000
Faro is also a far away from the ending of the developments.

81
00:12:39,000 --> 00:13:02,000
And then we skip to 159.

82
00:13:02,000 --> 00:13:12,000
If only you would do, right, if you would do, if oneself would do what one teaches others.

83
00:13:12,000 --> 00:13:22,000
So don't do what they're done with.

84
00:13:22,000 --> 00:13:27,000
Don't do what they're done with.

85
00:13:27,000 --> 00:13:38,000
All right.

86
00:13:38,000 --> 00:13:48,000
Well, trained oneself one could then train one should then train others.

87
00:13:48,000 --> 00:13:56,000
For it is difficult to train oneself atahikira, do the more.

88
00:13:56,000 --> 00:13:59,000
Self is difficult to tame.

89
00:13:59,000 --> 00:14:02,000
It's easy to give advice to others, right?

90
00:14:02,000 --> 00:14:07,000
Actually teaching is more easier than actually practicing.

91
00:14:07,000 --> 00:14:09,000
So people say, thank you to me.

92
00:14:09,000 --> 00:14:10,000
I say, no, thank you.

93
00:14:10,000 --> 00:14:13,000
The other one is doing all the work.

94
00:14:13,000 --> 00:14:15,000
I'm just teaching you.

95
00:14:15,000 --> 00:14:17,000
I'm just leading you on the path.

96
00:14:17,000 --> 00:14:23,000
You have the one who has to do the work for yourself.

97
00:14:23,000 --> 00:14:30,000
Even the Buddha just pointed the way.

98
00:14:30,000 --> 00:14:43,000
And the last one, which is, I think, probably the most.

99
00:14:43,000 --> 00:15:04,000
It's just the most inspiring.

100
00:15:04,000 --> 00:15:16,000
So atahikuto satima, so can be kuriha is same.

101
00:15:16,000 --> 00:15:30,000
Should I tell you, let's do all that.

102
00:15:30,000 --> 00:15:41,000
Self should guard oneself.

103
00:15:41,000 --> 00:15:56,000
It should examine oneself.

104
00:15:56,000 --> 00:16:00,000
When one is self-guarded and mindful.

105
00:16:00,000 --> 00:16:07,000
So atahikuto satima, so can be kuriha is same.

106
00:16:07,000 --> 00:16:10,000
It's a good name for a monk.

107
00:16:10,000 --> 00:16:15,000
Atahikuto, one who guards oneself.

108
00:16:15,000 --> 00:16:19,000
If you want a good poly name, there's a good one.

109
00:16:19,000 --> 00:16:24,000
Self-guarded means to guard your senses.

110
00:16:24,000 --> 00:16:29,000
And guard the eye, the ears, the nose, the tongue, the body, and the mind.

111
00:16:29,000 --> 00:16:35,000
So the six doors, it's a good description of the meditation practice.

112
00:16:35,000 --> 00:16:42,000
Because all of our experiences come through the six doors.

113
00:16:42,000 --> 00:16:52,000
And if you get caught up in any one of them, it leads to bad habits to put it simply.

114
00:16:52,000 --> 00:16:56,000
Because it becomes habitual, and it gets you caught up in it.

115
00:16:56,000 --> 00:16:59,000
It gets you off track.

116
00:16:59,000 --> 00:17:10,000
It gets you caught up in some kind of cycle of addiction or aversion or conceit or views or whatever.

117
00:17:10,000 --> 00:17:16,000
And needs to suffering, needs to stress.

118
00:17:16,000 --> 00:17:22,000
Because you don't see things clearly biased.

119
00:17:22,000 --> 00:17:29,000
And your biases create embarrassment and create stress and create conflict.

120
00:17:29,000 --> 00:17:39,000
Create disappointment and expectations.

121
00:17:39,000 --> 00:17:42,000
So the nice verses are all about yourself.

122
00:17:42,000 --> 00:17:46,000
Because that's where we focus.

123
00:17:46,000 --> 00:17:49,000
A river is only as pure as its source.

124
00:17:49,000 --> 00:17:54,000
All of our life and our interactions with the world around us.

125
00:17:54,000 --> 00:17:56,000
It's all dependent on our minds.

126
00:17:56,000 --> 00:17:58,000
Your state of mind is not pure.

127
00:17:58,000 --> 00:18:09,000
You can't expect to help others or be a good influence on others or have a good relationship with others.

128
00:18:09,000 --> 00:18:12,000
Work on yourself. It's the best thing you can do.

129
00:18:12,000 --> 00:18:15,000
And in fact, it's the only thing you need to do.

130
00:18:15,000 --> 00:18:17,000
Everything else comes naturally.

131
00:18:17,000 --> 00:18:21,000
Once your mind is pure, or the more your mind is pure,

132
00:18:21,000 --> 00:18:25,000
the better your relationship with others, your influence on others.

133
00:18:25,000 --> 00:18:30,000
Your impact on the world.

134
00:18:30,000 --> 00:18:32,000
And that's why we meditate.

135
00:18:32,000 --> 00:18:34,000
That's why we work on ourselves.

136
00:18:34,000 --> 00:18:37,000
That's why we study ourselves.

137
00:18:37,000 --> 00:18:42,000
That's why we try to free ourselves from suffering.

138
00:18:42,000 --> 00:18:47,000
And you can't help others if you're in pain and

139
00:18:47,000 --> 00:18:49,000
stressed about your own problems.

140
00:18:49,000 --> 00:18:52,000
But a person is truly happy.

141
00:18:52,000 --> 00:19:00,000
Your benefit to the world and to everyone they come in contact with.

142
00:19:00,000 --> 00:19:08,000
Alright, that's the quote for tonight.

143
00:19:08,000 --> 00:19:12,000
I'm going to post the Hangout if anyone has questions.

144
00:19:12,000 --> 00:19:17,000
You're welcome to click on the link and come and ask

145
00:19:17,000 --> 00:19:35,000
the Hangout for a few minutes to see if anyone has questions.

146
00:19:35,000 --> 00:19:37,000
This is the second broadcast today.

147
00:19:37,000 --> 00:19:59,000
Today was second-life day, so I broadcasted in virtual reality.

148
00:19:59,000 --> 00:20:09,000
The 20 people, only 20 people can listen to a second-life talk.

149
00:20:09,000 --> 00:20:14,000
It's actually a limit placed on the place by the company.

150
00:20:14,000 --> 00:20:15,000
It's quite expensive.

151
00:20:15,000 --> 00:20:19,000
Second-life is quite expensive for people who have land,

152
00:20:19,000 --> 00:20:22,000
like the Buddha Center.

153
00:20:22,000 --> 00:20:31,000
Steven, you're back, but you're still.

154
00:20:31,000 --> 00:20:35,000
Yep, I hear you.

155
00:20:35,000 --> 00:20:40,000
Do you have a question for me?

156
00:20:40,000 --> 00:20:56,000
Are you there?

157
00:20:56,000 --> 00:21:17,000
What a look there, you're here.

158
00:21:17,000 --> 00:21:27,000
Just because you say something about how people feel about meditation,

159
00:21:27,000 --> 00:21:34,000
that they think sometimes they're kind of wasting their time.

160
00:21:34,000 --> 00:21:39,000
I used to have a mental illness.

161
00:21:39,000 --> 00:21:42,000
I had depression and anxiety.

162
00:21:42,000 --> 00:21:45,000
So we have depression and anxiety.

163
00:21:45,000 --> 00:21:56,000
Naturally, I went through a paranoia state for one and a half months.

164
00:21:56,000 --> 00:22:06,000
At that time, all my people, all my family put out and superb me.

165
00:22:06,000 --> 00:22:24,000
But after the crisis passed, they think that it was necessary to do more things about it.

166
00:22:24,000 --> 00:22:38,000
Even so, I didn't want to leave taking pills.

167
00:22:38,000 --> 00:22:39,000
All the time.

168
00:22:39,000 --> 00:22:56,000
So I decided not to take pills anymore and start meditating more seriously.

169
00:22:56,000 --> 00:23:13,000
I think that sometimes we don't should pay attention on what other people said about living or jobs and stop taking medics.

170
00:23:13,000 --> 00:23:24,000
If we feel that meditating is going to help us, we should just do that.

171
00:23:24,000 --> 00:23:33,000
That's how I'm coming.

172
00:23:33,000 --> 00:23:36,000
Well, thank you.

173
00:23:36,000 --> 00:23:37,000
Hey, you're smoking?

174
00:23:37,000 --> 00:23:40,000
Who is this guy?

175
00:23:40,000 --> 00:23:42,000
I'm not convinced.

176
00:23:42,000 --> 00:23:45,000
Stephen, can you tell us a little bit about yourself?

177
00:23:45,000 --> 00:23:50,000
Why are you here?

178
00:23:50,000 --> 00:23:57,000
I don't know who this guy is, but I think smoking on our broadcast is done a lot.

179
00:23:57,000 --> 00:24:06,000
Apologies.

180
00:24:06,000 --> 00:24:09,000
We have rules like that.

181
00:24:09,000 --> 00:24:10,000
That's appropriate.

182
00:24:10,000 --> 00:24:12,000
That's not very respectful.

183
00:24:12,000 --> 00:24:21,000
I have to stop smoking if you want to join our broadcast.

184
00:24:21,000 --> 00:24:33,000
You have a question, sir.

185
00:24:33,000 --> 00:24:44,000
The reason I stick around is in case people have questions.

186
00:24:44,000 --> 00:25:07,000
In there.

