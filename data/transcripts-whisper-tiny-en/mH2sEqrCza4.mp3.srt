1
00:00:00,000 --> 00:00:18,000
Hi, so in this video I'll be talking a little bit about how we put into practice some of the concepts which I explained in the first video and then I'll give a short demonstration on how to practice meditation in the sitting position.

2
00:00:18,000 --> 00:00:26,000
Sitting meditation is something which is done as a basic meditation practice.

3
00:00:26,000 --> 00:00:32,000
It's something which anyone can do. You can do it either in the cross-legged sitting position. You can do it sitting on a chair.

4
00:00:32,000 --> 00:00:38,000
You can even apply the same technique to a lying position for someone who is not able to sit up.

5
00:00:38,000 --> 00:00:46,000
Then you rate the practice of sitting meditation is to watch the movements of the body when we're sitting still.

6
00:00:46,000 --> 00:00:53,000
So at the moment when we're sitting still the whole of our body is tranquil and there's no movement in the body.

7
00:00:53,000 --> 00:01:00,000
Except when the breath comes into the body and when the breath goes out of the body there's a movement in the stomach.

8
00:01:00,000 --> 00:01:06,000
And if you put your hand on your stomach you can feel this movement for people who have never practiced meditation it.

9
00:01:06,000 --> 00:01:10,000
It might seem unfamiliar or might be hard to find.

10
00:01:10,000 --> 00:01:18,000
But once you put your hand on the stomach you'll be able to see for yourself that when the breath goes into the body the stomach rises.

11
00:01:18,000 --> 00:01:22,000
Maybe just slightly but it does rise naturally.

12
00:01:22,000 --> 00:01:27,000
When the breath goes out of the body the stomach will naturally fall.

13
00:01:27,000 --> 00:01:32,000
If it's still hard to find even with your hand there you can try lying down on your back.

14
00:01:32,000 --> 00:01:45,000
But at any rate once you try this for some time you'll be able to see for yourself that in a natural state when the mind is not stressed when the mind is not worried or when the mind is not forcing the breath

15
00:01:45,000 --> 00:01:52,000
you'll find that the stomach naturally rises and naturally falls. And we're going to use this as our basic technique of meditation.

16
00:01:52,000 --> 00:01:59,000
Once we get better at it it'll become quite clear and we'll be able to use it quite well as our meditation object.

17
00:01:59,000 --> 00:02:05,000
So here when the belly rises we're simply going to say to ourselves rising.

18
00:02:05,000 --> 00:02:16,000
When the belly falls we're simply going to say to ourselves falling. Rising, falling, rising, falling.

19
00:02:16,000 --> 00:02:19,000
And then practically the method is as follows.

20
00:02:19,000 --> 00:02:24,000
We sit with the legs crossed or in any position which is comfortable as necessary.

21
00:02:24,000 --> 00:02:30,000
Traditionally we'll sit with one hand on top of the other and the thumb's touching at the tips.

22
00:02:30,000 --> 00:02:35,000
We sit with our back straight although it doesn't have to be perfectly straight if this is uncomfortable.

23
00:02:35,000 --> 00:02:42,000
Just as long as one is not bending over to the point where one isn't able to experience the breath.

24
00:02:42,000 --> 00:02:52,000
And with the eyes closed, again since we're focusing on the stomach we don't want to have our eyes open for anything that will distract us away from our object.

25
00:02:52,000 --> 00:03:00,000
And once the eyes are closed we simply say to ourselves rising, falling, rising, falling.

26
00:03:00,000 --> 00:03:08,000
Again we're not saying at the mouth it's important that even though I speak these words out loud that you understand we're creating this clear thought in the mind.

27
00:03:08,000 --> 00:03:11,000
And where is the mind? Well it should be with the stomach.

28
00:03:11,000 --> 00:03:14,000
So we're actually something like speaking into our stomach.

29
00:03:14,000 --> 00:03:20,000
We're creating this clear thought in the stomach where the mind is at that moment.

30
00:03:20,000 --> 00:03:28,000
And the practice of the practice is saying to ourselves in our mind rising, falling,

31
00:03:28,000 --> 00:03:35,000
and this can be carried out for many minutes, five minutes, ten minutes or so on.

32
00:03:35,000 --> 00:03:38,000
At the time when we're doing this this is the basic practice.

33
00:03:38,000 --> 00:03:43,000
It's important to be aware that we're still keeping track of the four foundations.

34
00:03:43,000 --> 00:03:48,000
The body, the feelings, the mind, and the mental states.

35
00:03:48,000 --> 00:03:52,000
So the body here we have in the rising and the falling that's clear.

36
00:03:52,000 --> 00:04:01,000
The feelings, well this is when we're sitting and we're watching the rising and falling our mind is carried away to one of the feelings which arise in the body.

37
00:04:01,000 --> 00:04:03,000
A feeling of pain for instance.

38
00:04:03,000 --> 00:04:10,000
When we're sitting in this position for long periods of time without moving it can create states of pain in the physical pain.

39
00:04:10,000 --> 00:04:15,000
So instead of getting upset or letting this become something which is going to create suffering for us,

40
00:04:15,000 --> 00:04:19,000
we take that as our meditation object. Remember we can use any four of these.

41
00:04:19,000 --> 00:04:22,000
Because all four of these are part of reality.

42
00:04:22,000 --> 00:04:24,000
We don't have to stay with the rising and falling.

43
00:04:24,000 --> 00:04:28,000
Instead now we're going to take our new friend which is the pain.

44
00:04:28,000 --> 00:04:31,000
And we're going to look at that and come to see it clearly.

45
00:04:31,000 --> 00:04:39,000
And so we focus on the pain and say to ourselves pain, pain, pain, pain,

46
00:04:39,000 --> 00:04:45,000
pain until it goes away. If we feel happy we can say to ourselves happy.

47
00:04:45,000 --> 00:04:47,000
Sometimes when we sit in meditation we feel very peaceful.

48
00:04:47,000 --> 00:04:52,000
We can say to ourselves peaceful or calm until that feeling goes away.

49
00:04:52,000 --> 00:04:57,000
When it comes away then we come back again to the rising and the falling of the stomach again.

50
00:04:57,000 --> 00:04:59,000
The third one is the thoughts.

51
00:04:59,000 --> 00:05:05,000
So when we're sitting sometimes we're watching the rising and falling but our mind starts to wander thinking about the past,

52
00:05:05,000 --> 00:05:10,000
thinking about the future, thinking about this, that good thoughts, bad thoughts, all sorts of thoughts.

53
00:05:10,000 --> 00:05:12,000
Any kind of thought might arise.

54
00:05:12,000 --> 00:05:17,000
Instead of letting our minds wander and drift away and lose track of reality,

55
00:05:17,000 --> 00:05:22,000
we bring our minds back to the reality of the thought and we say to ourselves thinking.

56
00:05:22,000 --> 00:05:26,000
As I explained in the first video, well here's where we're going to put it into use.

57
00:05:26,000 --> 00:05:31,000
Instead of focusing on the rising and falling alone when we start to think we say to ourselves thinking.

58
00:05:31,000 --> 00:05:37,000
We can say to ourselves thinking and thinking and it will go away automatically by itself

59
00:05:37,000 --> 00:05:40,000
and then we can come back to the rising and falling again.

60
00:05:40,000 --> 00:05:44,000
As for mental states when we have liking we like this or like that.

61
00:05:44,000 --> 00:05:51,000
When we don't like of disliking anger, frustration we can say angry, angry, frustrated, frustrated.

62
00:05:51,000 --> 00:05:57,000
When we feel lazy or tired or drowsy we can say to ourselves tired and tired and drowsy drowsy.

63
00:05:57,000 --> 00:06:03,000
When we feel distracted or worried we can say distracted or distracted or worried.

64
00:06:03,000 --> 00:06:11,000
When we have doubt arising in the mind we can say to ourselves doubting or confusion or so on.

65
00:06:11,000 --> 00:06:16,000
This fourth one are particularly things which create difficulty in the meditation.

66
00:06:16,000 --> 00:06:19,000
So actually our meditation should go quite smoothly.

67
00:06:19,000 --> 00:06:23,000
We should be able to watch the rising and falling more of the pain or so on.

68
00:06:23,000 --> 00:06:31,000
But because of the mental states, the states of liking or disliking of drowsiness, of distraction, of doubt,

69
00:06:31,000 --> 00:06:33,000
this is why our meditation cannot progress.

70
00:06:33,000 --> 00:06:37,000
So these ones are especially important to keep track of and to keep an eye on.

71
00:06:37,000 --> 00:06:45,000
When they arise we have to quickly catch them and bring the mind back again to the present moment.

72
00:06:45,000 --> 00:06:50,000
The benefits of this practice of meditation, what we should see as we practice.

73
00:06:50,000 --> 00:06:56,000
The first thing which we should see are that our mind starts to calm down and it does actually become more peaceful.

74
00:06:56,000 --> 00:07:00,000
Even though this isn't right away this isn't our goal.

75
00:07:00,000 --> 00:07:06,000
We can see that gradually as we practice, the first thing that happens is our mind does become calmer.

76
00:07:06,000 --> 00:07:08,000
Our mind does become happier.

77
00:07:08,000 --> 00:07:14,000
It does become lighter and it does become freer from the things which bind it to suffering,

78
00:07:14,000 --> 00:07:19,000
which keep it in these loops and these endless loops of suffering.

79
00:07:19,000 --> 00:07:24,000
The second thing we come to see is that we come to realize things about ourselves

80
00:07:24,000 --> 00:07:27,000
and we come to realize things about the world around us.

81
00:07:27,000 --> 00:07:32,000
When we come to understand that in ourselves we have certain things which we couldn't do without,

82
00:07:32,000 --> 00:07:39,000
and we come to see why suffering arises in our minds, in our hearts, why we do fall into suffering.

83
00:07:39,000 --> 00:07:41,000
We come to understand things.

84
00:07:41,000 --> 00:07:43,000
We understand about other people.

85
00:07:43,000 --> 00:07:46,000
When people get angry at us before we thought they were evil.

86
00:07:46,000 --> 00:07:50,000
Now we understand they have the same emotions which we have inside of ourselves.

87
00:07:50,000 --> 00:07:55,000
When we come to understand why people do and say and think the things that they do,

88
00:07:55,000 --> 00:08:00,000
we come to see that it's because they're just like us and we are just like them.

89
00:08:00,000 --> 00:08:06,000
The third thing that we come to see in the practice, the result which we should be able to see,

90
00:08:06,000 --> 00:08:09,000
is that we're more aware.

91
00:08:09,000 --> 00:08:11,000
We're more aware of the world around us.

92
00:08:11,000 --> 00:08:13,000
We're more aware of the people around us.

93
00:08:13,000 --> 00:08:17,000
We have the things inside of us that are arising and ceasing in the present moment.

94
00:08:17,000 --> 00:08:22,000
So when things come, when difficult situations arise instead of being caught off guard

95
00:08:22,000 --> 00:08:27,000
and falling into fear and anxiety and confusion and stress,

96
00:08:27,000 --> 00:08:30,000
we're able to take things much better and take things as they go.

97
00:08:30,000 --> 00:08:32,000
We're able to take sickness much better.

98
00:08:32,000 --> 00:08:37,000
We're able to take difficulty much better, even old age sickness death.

99
00:08:37,000 --> 00:08:41,000
We're able to take much better through the practice of meditation.

100
00:08:41,000 --> 00:08:47,000
The fourth thing is what we're really aiming for is we're aiming to get rid of our,

101
00:08:47,000 --> 00:08:53,000
to rid ourselves of the evils and the unpleasantness,

102
00:08:53,000 --> 00:08:56,000
the unwholesome states which exist in our minds.

103
00:08:56,000 --> 00:09:03,000
These states of anger, greed, delusion, anxiety, worry, stress, fear, arrogance,

104
00:09:03,000 --> 00:09:07,000
concede, all sorts of things which are unuseful for us,

105
00:09:07,000 --> 00:09:11,000
which are of no use, of no benefit to us or to other people.

106
00:09:11,000 --> 00:09:13,000
And in fact, they are worse than that.

107
00:09:13,000 --> 00:09:15,000
They create suffering.

108
00:09:15,000 --> 00:09:19,000
They create unhappiness and stress for us and for the people around us.

109
00:09:19,000 --> 00:09:23,000
Now I'd like to invite the viewers to practice together,

110
00:09:23,000 --> 00:09:27,000
say five minutes or ten minutes or however minutes is comfortable for you.

111
00:09:27,000 --> 00:09:30,000
We'll practice sitting meditation together.

112
00:09:30,000 --> 00:09:34,000
And this will end my second video on how to practice meditation.

113
00:09:34,000 --> 00:09:39,000
And I hope again that all of you, that this meditation will bring to you

114
00:09:39,000 --> 00:09:42,000
peace and happiness in your everyday life.

115
00:09:42,000 --> 00:10:06,000
So that's all for now and now I invite you to sit with me together in sitting meditation.

