1
00:00:00,000 --> 00:00:06,640
Instead of noting my heart pounding panic as feelings, I noted as hearing, it seems to

2
00:00:06,640 --> 00:00:07,640
help.

3
00:00:07,640 --> 00:00:08,640
What do you think?

4
00:00:08,640 --> 00:00:10,640
I can seem to hear my heart pounding.

5
00:00:10,640 --> 00:00:16,400
Yeah, just be careful, you're not too keen on it seems to help heart because you can get

6
00:00:16,400 --> 00:00:20,800
excited about it and then you think, okay, well, every time I say hearing hearing, it's

7
00:00:20,800 --> 00:00:22,920
going to work.

8
00:00:22,920 --> 00:00:25,200
There's no trick to enlightenment.

9
00:00:25,200 --> 00:00:28,480
You have to deal with every moment appropriately.

10
00:00:28,480 --> 00:00:34,720
And there's no single appropriateness, except mindfulness, but it's such a slippery thing.

11
00:00:34,720 --> 00:00:39,600
You have to, you know, hearing hearing is not going to answer all of your problems and

12
00:00:39,600 --> 00:00:43,600
it's not going to even answer that problem every time because the problems change and

13
00:00:43,600 --> 00:00:47,440
there's many things involved with them.

14
00:00:47,440 --> 00:00:52,960
Maybe next time it's going to be the feeling that that will be most distinct.

15
00:00:52,960 --> 00:00:53,960
And so that's really the answer.

16
00:00:53,960 --> 00:00:58,120
Whatever is most distinct, acknowledge that if it's hearing and acknowledge hearing, it doesn't

17
00:00:58,120 --> 00:01:02,360
matter what someone tells you to do, it matters what you're experiencing.

18
00:01:02,360 --> 00:01:06,280
So if I've told you before to acknowledge feeling the feeling, that's only if the feeling

19
00:01:06,280 --> 00:01:09,080
is most distinct.

20
00:01:09,080 --> 00:01:36,520
Whatever is most distinct, focus on that and remind yourself clearly of it as it is.

