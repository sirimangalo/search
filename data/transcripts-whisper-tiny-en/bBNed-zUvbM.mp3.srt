1
00:00:00,000 --> 00:00:06,300
Hi, next question comes from field hockey 007.

2
00:00:06,300 --> 00:00:10,840
I am a young 17 beginner interested in Buddhism.

3
00:00:10,840 --> 00:00:12,840
I don't have a center nearby.

4
00:00:12,840 --> 00:00:17,840
Should I seek a community on the internet or walk my own path and overcome my sense of loneliness

5
00:00:17,840 --> 00:00:19,480
through meditation?

6
00:00:19,480 --> 00:00:21,880
Would meditation help productivity and motivation?

7
00:00:21,880 --> 00:00:25,320
Well, there are several parts to this.

8
00:00:25,320 --> 00:00:29,200
Not having a center nearby can be a problem, but on the other hand, if you think about

9
00:00:29,200 --> 00:00:37,840
it nowadays, the world is getting smaller in the sense that it's a lot easier to travel

10
00:00:37,840 --> 00:00:44,240
around and being young can be a hindrance, but you'll find that eventually once you get

11
00:00:44,240 --> 00:00:50,600
things together that you should be able to collect together the funds necessary to make

12
00:00:50,600 --> 00:00:57,880
a trip, whether it be to a faraway place or a not so faraway place and undertake a

13
00:00:57,880 --> 00:01:00,440
meditation course.

14
00:01:00,440 --> 00:01:09,600
For most people, now I can't judge on your situation, I would say that there's only

15
00:01:09,600 --> 00:01:17,640
unlimited amounts of benefit you can gain from the internet unless you're really motivated.

16
00:01:17,640 --> 00:01:24,920
Now I have had one person who was able to pretty much finish a meditation course with

17
00:01:24,920 --> 00:01:30,960
me over the internet using Skype, but for the most part, I found that it can be quite

18
00:01:30,960 --> 00:01:35,520
difficult to do a course that way unless you're a person who has a lot of time and a lot

19
00:01:35,520 --> 00:01:41,040
of motivation in regards to the meditation.

20
00:01:41,040 --> 00:01:47,240
It's a much easier task to go away and find the meditation center and go away.

21
00:01:47,240 --> 00:01:52,400
You can look on the internet, find a place that looks like the kind of place that you'd

22
00:01:52,400 --> 00:02:03,880
like to go, looks reputable and well-established and open to foreigners, and at least take

23
00:02:03,880 --> 00:02:06,240
a meditation center, a meditation course.

24
00:02:06,240 --> 00:02:10,120
You'll find that when you come back, you've gained a lot and it's become a lot easier

25
00:02:10,120 --> 00:02:13,640
for you to live your life.

26
00:02:13,640 --> 00:02:17,680
Although the second part of your question, as regards to overcoming your sense of loneliness,

27
00:02:17,680 --> 00:02:21,080
that there's no escaping that, whether you're surrounded by people or whether you're

28
00:02:21,080 --> 00:02:25,720
living on a mountain, loneliness is something that we all have to deal with, and it comes

29
00:02:25,720 --> 00:02:31,960
not from being alone or from being near people, it comes from a sense of needing attention,

30
00:02:31,960 --> 00:02:40,040
of needing stimulus, of needing something, of needing acceptance and appreciation and the

31
00:02:40,040 --> 00:02:44,680
comfort and the pleasure that comes from the interaction with other people, and this is

32
00:02:44,680 --> 00:02:49,200
something we all have to do away with, and you'll find that you'll find that the more

33
00:02:49,200 --> 00:02:56,400
you meditate, and the more clear you are in your mind, the more confident and sure you

34
00:02:56,400 --> 00:03:01,520
become in yourself, and the more wisdom and understanding you gain, the less you need,

35
00:03:01,520 --> 00:03:06,680
the less you rely on other people to make you happy, the less you rely on others to give

36
00:03:06,680 --> 00:03:10,280
you the sense of comfort and appreciation and worth.

37
00:03:10,280 --> 00:03:16,400
You find a sense of self-worth, or you give up the sense of self, and so you don't have

38
00:03:16,400 --> 00:03:25,240
any sense of worth, or value, and you're able to live your life without such qualifiers,

39
00:03:25,240 --> 00:03:32,640
and without this need for constant interaction or constant stimulus.

40
00:03:32,640 --> 00:03:38,880
So regardless of whether you go to do a course, or whether you find a community, I would

41
00:03:38,880 --> 00:03:45,040
say a community on the internet is good, there are things you can gain from it, as far

42
00:03:45,040 --> 00:03:50,840
as being able to ask questions like this, or on the forums, but I wouldn't say that

43
00:03:50,840 --> 00:03:55,800
that's going to make you any less lonely, I would say only meditation, for sure, will help

44
00:03:55,800 --> 00:03:57,880
with that.

45
00:03:57,880 --> 00:04:02,000
As far as meditation, helping with productivity and motivation, well, there's two parts

46
00:04:02,000 --> 00:04:03,400
to this.

47
00:04:03,400 --> 00:04:09,480
Certainly meditation helps you to become more productive, helps you to become more efficient,

48
00:04:09,480 --> 00:04:14,040
and whatever you decide to do, you'll find yourself better able to do it in a shorter

49
00:04:14,040 --> 00:04:18,560
amount of time with less distractions, because you're much better focused, your mind is

50
00:04:18,560 --> 00:04:27,240
much clearer, you'll find that it has things that used to make, you used to find difficult

51
00:04:27,240 --> 00:04:30,240
to do, are now a breeze.

52
00:04:30,240 --> 00:04:36,840
But the other side of that is you become less interested, so motivation can be a problem.

53
00:04:36,840 --> 00:04:42,000
You find yourself doing things when they're necessary, but you won't find yourself interested

54
00:04:42,000 --> 00:04:49,120
or engaged in the work, if it happens to be an employment in the world outside, or study

55
00:04:49,120 --> 00:04:57,160
in a school, in the worldly matters, or subject matter, you'll find yourself getting

56
00:04:57,160 --> 00:05:03,680
bored, so they're getting disinterested, or unable to find or create any interest in it,

57
00:05:03,680 --> 00:05:07,320
and you'll find yourself shying away from this sort of thing, you wouldn't find it

58
00:05:07,320 --> 00:05:14,680
easy to become a big boss, or get ahead in the world, in worldly ways.

59
00:05:14,680 --> 00:05:19,680
On the contrary, you'll find yourself shying away from that, trying to find a simpler

60
00:05:19,680 --> 00:05:25,920
job, maybe one that pays less, but gives you more time to focus on things that you find

61
00:05:25,920 --> 00:05:31,040
truly important, and you find your motivation shifting, that your motivation shifts a lot

62
00:05:31,040 --> 00:05:39,920
more towards introspection, towards meditation, towards study, and towards helping other

63
00:05:39,920 --> 00:05:44,960
people, and less in terms of worldly things.

64
00:05:44,960 --> 00:05:51,000
So I hope that answers your question, thanks for asking, and good luck to you, hope you

65
00:05:51,000 --> 00:06:01,960
are able to find a place where you can start on your journey.

