1
00:00:00,000 --> 00:00:05,620
So, but to say that, I mean, it's really a technical argument because, yes, technically the

2
00:00:05,620 --> 00:00:11,500
genres are necessary, but there are different ways of going about that, and I've talked

3
00:00:11,500 --> 00:00:19,900
about this many times if you cultivate insight first, and then you end up in a place where

4
00:00:19,900 --> 00:00:28,140
the genres come as a result of insight, it's terribly dogmatic to think otherwise, and it's

5
00:00:28,140 --> 00:00:34,000
kind of ridiculous because the genres, as they're traditionally understood, are devoid

6
00:00:34,000 --> 00:00:37,480
of insight, which is, you know, what is the Buddha teach?

7
00:00:37,480 --> 00:00:41,680
He taught insight, he taught five aggregates, understanding and permanent suffering, and

8
00:00:41,680 --> 00:00:46,520
on such a can understand that, depending on how you understand the genres, but in many cases

9
00:00:46,520 --> 00:00:47,520
you can't understand that.

10
00:00:47,520 --> 00:00:57,720
I was just reading, I just got this book, there's a book, it's just reading it.

11
00:00:57,720 --> 00:01:11,840
This is by lay brassington, some American Americans to take it with a grain of salt.

12
00:01:11,840 --> 00:01:24,040
There's no control F on paper books unfortunately, but I just opened it to a page, I don't

13
00:01:24,040 --> 00:01:26,120
know, it's in the introduction here.

14
00:01:26,120 --> 00:01:31,560
So I sat with IF for five weeks, I learned John is six, seven, and eight, and was thoroughly

15
00:01:31,560 --> 00:01:36,400
enjoying running him up and running him back down, not too long after I gained some skill

16
00:01:36,400 --> 00:01:40,760
at doing this, I was again in an interview, and she said, now you must do insight practice

17
00:01:40,760 --> 00:01:44,760
in the same sitting after you do the genres.

18
00:01:44,760 --> 00:02:00,880
So the point is, and it goes on to do.

