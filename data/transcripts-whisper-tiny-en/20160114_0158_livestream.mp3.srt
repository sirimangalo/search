1
00:00:00,000 --> 00:00:16,520
Okay, good evening everyone, broadcasting live January 13th, 2016.

2
00:00:16,520 --> 00:00:42,120
Today's quote, today's quote is on the surface, it's about charity.

3
00:00:42,120 --> 00:01:01,480
It's a very strong, powerful statement about the power of charity, if we knew the benefit

4
00:01:01,480 --> 00:01:24,480
of giving gifts, even all we wouldn't eat without giving gifts.

5
00:01:24,480 --> 00:01:53,480
And this is because this is the power of the mind, the power of karma.

6
00:01:53,480 --> 00:02:13,040
So, giving a gift is much more than just the benefit that accrues to the person, it's even

7
00:02:13,040 --> 00:02:20,920
much more than the higher opinion that they'll then have for you.

8
00:02:20,920 --> 00:02:30,200
Giving gifts changes you, it's choosing a fork in the road, coming to a fork in the

9
00:02:30,200 --> 00:02:38,440
road and choosing a path, this is a description of karma, so the real issue here is

10
00:02:38,440 --> 00:02:48,000
the issue of karma, it's not just this is specifically about charity, which makes it a profound

11
00:02:48,000 --> 00:02:56,960
statement in its own right, reminds us of the importance of being charitable, and not just

12
00:02:56,960 --> 00:03:00,480
the importance but the greatness, you know, it's not like it should be an obligation, it's

13
00:03:00,480 --> 00:03:06,400
like if you only knew what good it did, it's something that really, really does good

14
00:03:06,400 --> 00:03:16,480
things for us, but it gives so much, gives more than, in first of all, it does give the

15
00:03:16,480 --> 00:03:21,680
goodness to another person, which is a great thing in itself, right, if you help someone

16
00:03:21,680 --> 00:03:34,440
else, you've done a great thing, you're great at happiness, for that person.

17
00:03:34,440 --> 00:03:42,760
And then the second thing it does is it and inclines them positively towards you, so that's

18
00:03:42,760 --> 00:03:54,680
another great thing, but even greater than all that is the effect that it has on your mind,

19
00:03:54,680 --> 00:04:09,680
making it kind and charitable, and more importantly able to let go, makes you less clingy,

20
00:04:09,680 --> 00:04:26,280
it reduces your attachments, it frees you from the stinginess, the miserliness, the desire,

21
00:04:26,280 --> 00:04:42,200
that addiction, so charity is good, but there's a deeper message here, there's two

22
00:04:42,200 --> 00:04:49,880
deeper, there's levels of deeper messages, so in deeper messages the message of karma

23
00:04:49,880 --> 00:04:56,960
that are actions have consequences, if they have consequences as no matter what, like

24
00:04:56,960 --> 00:05:06,400
if you don't look both ways before crossing the street, you get hit by a car or actions

25
00:05:06,400 --> 00:05:13,000
in and of themselves have consequences, even before the mind is factored in, right, there

26
00:05:13,000 --> 00:05:18,360
are some people who actually live in by this, they try and guess what's going to happen

27
00:05:18,360 --> 00:05:24,400
if they do this, or if they do that, some people become superstitious about it, if I walk

28
00:05:24,400 --> 00:05:36,520
under this ladder, they blow the consequences out of proportion, it's simple act, I'm walking

29
00:05:36,520 --> 00:05:51,800
where a black cat has walked, smashing a mirror, et cetera, but actions do have consequences,

30
00:05:51,800 --> 00:05:57,720
if you break something that belongs to someone else, you have to replace it for them, even

31
00:05:57,720 --> 00:06:06,360
if it wasn't intentional, becomes your responsibility, but that's not of course what

32
00:06:06,360 --> 00:06:16,000
me usually mean by karma because that's a fairly superficial result, the results sure

33
00:06:16,000 --> 00:06:27,160
they happen, but the results for that in that sense are quite limited, we're more interested

34
00:06:27,160 --> 00:06:35,240
in the results of mental action, mental volition behind our actions, are we doing it out

35
00:06:35,240 --> 00:06:39,760
of greed, are we doing it out of anger, because these have real meaning, this is where

36
00:06:39,760 --> 00:06:44,680
we're going to be really given from, or if our mind if we're doing something out of mindfulness

37
00:06:44,680 --> 00:06:54,480
for wisdom or compassion or love, then it's really power, if it's clear that you did something

38
00:06:54,480 --> 00:07:00,600
wrong as a mistake without realizing it, people are quick to forgive, but if you really

39
00:07:00,600 --> 00:07:13,440
meant to hurt someone, you're much less likely to be granted forgiveness, likewise if you

40
00:07:13,440 --> 00:07:18,560
give someone something, suppose you're going to throw something out and instead of throwing

41
00:07:18,560 --> 00:07:26,040
it out you say, hey do you want it, not much of a gift right, but if there's something

42
00:07:26,040 --> 00:07:35,160
that you really enjoy like you, you see that there's the last cookie and the jar and you're

43
00:07:35,160 --> 00:07:39,840
just looking at it and you're thinking I want that cookie, but then someone comes along

44
00:07:39,840 --> 00:07:46,840
and says oh wow there's one cookie left, can I have it, what do you do right, when you

45
00:07:46,840 --> 00:07:55,960
say to yourself, when you say to the person, yes you can have it, that's quite powerful,

46
00:07:55,960 --> 00:08:04,280
and we have parents do this for their children, sacrificing so much, some parents of course

47
00:08:04,280 --> 00:08:18,520
don't hold, be able to sacrifice like that, it's just more powerful, so a gift, not all

48
00:08:18,520 --> 00:08:29,960
gifts are the same, right, not all charities the same, but the deepest, the really interesting

49
00:08:29,960 --> 00:08:35,080
meaning or our interesting aspect of this sentence is actually not about karma or giving

50
00:08:35,080 --> 00:08:43,040
it all, it's about knowledge, what the Buddha is saying is you guys don't know the truth,

51
00:08:43,040 --> 00:08:51,080
he's looking at us and he's saying you guys are so blind, you people on this earth, it's

52
00:08:51,080 --> 00:08:59,720
actually a condemnation, a criticism of us, that we're blind and it's really, I mean this

53
00:08:59,720 --> 00:09:03,680
is one way of looking at it, of course I'm not claiming to know the Buddha's intentions

54
00:09:03,680 --> 00:09:10,400
for teaching and being able to read his intentions, but it can very easily be read, be

55
00:09:10,400 --> 00:09:18,400
understood as a challenge to us, wake up call, so this is an example, just as an example

56
00:09:18,400 --> 00:09:29,320
of how ignorant you people are, when the opportunity to give arises you don't give, that

57
00:09:29,320 --> 00:09:47,240
shows how ignorant you are because if you knew the benefits of giving you just give naturally

58
00:09:47,240 --> 00:09:52,240
and I suppose you could argue that what's the purpose of giving in the end, it doesn't

59
00:09:52,240 --> 00:09:56,440
really lead you to enlightenment does it, it's not really a practice because it doesn't

60
00:09:56,440 --> 00:10:02,440
lead to wisdom to give and so on, and so people argue this way, why did the Buddha talk

61
00:10:02,440 --> 00:10:13,360
about giving and so on, but really this, this, the meaning here is, I mean that really actually

62
00:10:13,360 --> 00:10:21,320
proves the point that this is just an example because you can say, well why, why talk

63
00:10:21,320 --> 00:10:28,680
about giving when giving is not really essential, but the point is, here you have people

64
00:10:28,680 --> 00:10:34,080
who want to be happy, who want pleasure, who want to go to heaven, or who, even if they

65
00:10:34,080 --> 00:10:39,800
don't believe in heaven or don't think about heaven, they want heaven, they want what

66
00:10:39,800 --> 00:10:45,040
heaven represents, they want states like heavenly states, you know pleasurable states

67
00:10:45,040 --> 00:10:51,240
in general, and yet they don't do anything to get them, that's what he's saying.

68
00:10:51,240 --> 00:10:59,240
Because pleasure only comes from goodness, from mundane, wholesome state, wholesome

69
00:10:59,240 --> 00:11:05,080
activity, here these people want to be happy, and yet they'll do all sorts of things

70
00:11:05,080 --> 00:11:11,600
to make themselves unhappy, if people don't know what leads to happiness, they don't

71
00:11:11,600 --> 00:11:16,960
do the things that leads to happiness, that's what he's saying, so the deeper, so it's

72
00:11:16,960 --> 00:11:24,720
really a deeper and quite interesting for meditators and non-meditators, like for meditators

73
00:11:24,720 --> 00:11:32,120
it reminds us of what's most important knowledge, wisdom, understanding, that's what

74
00:11:32,120 --> 00:11:41,200
we're aiming for, that's what we're working for, we're not, meditation isn't magic,

75
00:11:41,200 --> 00:11:48,200
it's not something that's going to transform you or something, you're not just going to

76
00:11:48,200 --> 00:11:53,840
break away from some sorrow, you're going to learn about some sorrow, you're going to learn

77
00:11:53,840 --> 00:12:02,320
about reality, you're going to learn about experience, you're going to see through the cloud

78
00:12:02,320 --> 00:12:08,200
of ignorance and delusion, and realize these things like wow, giving us such a powerful

79
00:12:08,200 --> 00:12:19,760
thing, morality, ethical ethics, such a powerful thing, meditation, such a power, see

80
00:12:19,760 --> 00:12:33,040
me, we don't see the way the Buddha sees, that's the key, once we see the way the Buddha

81
00:12:33,040 --> 00:12:45,240
sees, put us off, once we learn and we understand reality, and we free ourselves from

82
00:12:45,240 --> 00:12:56,880
suffering, and find your happiness, so just a little bit of dhamma for tonight, that's

83
00:12:56,880 --> 00:13:07,720
our quote, maybe tomorrow we'll do question and answer, maybe dhamma fanda, no, one

84
00:13:07,720 --> 00:13:16,600
we're doing one dhamma fanda a week, one question and answer, and one Buddhism 101,

85
00:13:16,600 --> 00:13:26,800
that's the religion, right here, tomorrow I'm giving more five minute meditation lessons

86
00:13:26,800 --> 00:13:31,360
and I'm going to try to get someone to take pictures so I can share with everybody, you

87
00:13:31,360 --> 00:13:37,680
get to see what it's like, it's really pretty awesome, I must have taught 50 people, I think

88
00:13:37,680 --> 00:13:48,920
I taught over 50 people how to meditate on yesterday, it's pretty good, gave about 50

89
00:13:48,920 --> 00:14:03,880
booklets on how to meditate, just about as well as I could have hoped, anyway, so that's

90
00:14:03,880 --> 00:14:33,720
all for tonight, thank you all for tuning in.

