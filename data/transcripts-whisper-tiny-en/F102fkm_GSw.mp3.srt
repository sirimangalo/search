1
00:00:00,000 --> 00:00:07,120
Can you talk about right concentration?

2
00:00:07,120 --> 00:00:17,680
Right concentration is any concentration, there's a quote of the Buddha, it's any wholesome,

3
00:00:17,680 --> 00:00:25,800
beneficial oneness, a single pointedness of mind that is free from the five hindrances,

4
00:00:25,800 --> 00:00:33,160
which are liking, disliking, drowsiness, distraction, and doubt.

5
00:00:33,160 --> 00:00:36,840
If the mind is free from those, then this is considered to be right concentration.

6
00:00:36,840 --> 00:00:42,960
Now, as I talked about yesterday, in terms of the Eightfold Noble Path, true right concentration

7
00:00:42,960 --> 00:00:49,040
is only that one moment or two moments or whatever, it's one moment where the mind

8
00:00:49,040 --> 00:00:56,960
is leaving Samsara or has entered into Nibbana for the first time.

9
00:00:56,960 --> 00:01:01,560
But we get to that point by abandoning the five hindrances.

10
00:01:01,560 --> 00:01:11,480
So at the moment of the Magañana, then the knowledge of the path, Maga means path,

11
00:01:11,480 --> 00:01:17,600
it's the moment when the Eightfold Noble Path comes into full effect, and at that moment

12
00:01:17,600 --> 00:01:22,720
we are said to have right concentration, and in the moments that follow, which are the moments

13
00:01:22,720 --> 00:01:36,080
of Apalanya where the person stays in Nibbana and experiences Nibbana freedom, that's right concentration.

14
00:01:36,080 --> 00:01:51,680
Simple answer, that's not going to go any further than that, keep it simple.

