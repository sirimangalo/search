1
00:00:00,000 --> 00:00:05,880
Misha says she's confused why music is not allowed for monks.

2
00:00:05,880 --> 00:00:11,320
If music is simply vibrations entering the ear drums and so is listening to the sounds

3
00:00:11,320 --> 00:00:18,720
of nature during meditation, what's wrong with using music to help the concentration?

4
00:00:18,720 --> 00:00:43,200
It's a sense pleasure, it's something that you go to, it's not really strictly for

5
00:00:43,200 --> 00:00:54,280
a bit too long to hear music at all, but to go to listen to music, that is not allowed.

6
00:00:54,280 --> 00:01:04,600
So if a monk happens to hear music where there is music where the monk is meditating then

7
00:01:04,600 --> 00:01:13,160
we acknowledge it, but to go and want to listen to music, that is the problem.

8
00:01:13,160 --> 00:01:20,480
Not the sound itself, when there is music it is, as you say, just vibrations entering

9
00:01:20,480 --> 00:01:23,200
the ear drums.

10
00:01:23,200 --> 00:01:35,520
Same as any other sound of nature or of material.

11
00:01:35,520 --> 00:01:41,440
The other thing is that you are mentioning what's wrong with using music to help the

12
00:01:41,440 --> 00:01:53,760
concentration, this would be, is that crutch, that word, this would be a crutch, you should

13
00:01:53,760 --> 00:01:59,360
have concentration without a crutch, that's a good concentration.

14
00:01:59,360 --> 00:02:08,640
When you only can concentrate with music and there is no music, then you're not able

15
00:02:08,640 --> 00:02:21,120
to concentrate, so always try to learn, to concentrate or meditate without any help, without

16
00:02:21,120 --> 00:02:27,760
any crutches.

17
00:02:27,760 --> 00:02:32,360
And another question you can ask is if music is simply vibrations then why do you need

18
00:02:32,360 --> 00:02:38,480
a specific meditation, why would you need a specific meditation?

19
00:02:38,480 --> 00:02:42,640
What is it about that certain meditation that helps you concentrate?

20
00:02:42,640 --> 00:02:49,640
When I was in Jamthang, there would be once a year, a two week festival in the parking lot

21
00:02:49,640 --> 00:02:54,320
of the monastery, big parking lot because it was a tourist, a track, the monastery, the

22
00:02:54,320 --> 00:03:00,120
temple area itself was a big tourist attraction, but unfortunately the meditation center

23
00:03:00,120 --> 00:03:07,320
was quite close to the temple, and they would have a huge stage set up and carnival attractions

24
00:03:07,320 --> 00:03:15,680
and then music was 24-7 for two weeks, this loud boom, boom, techno music or all sorts

25
00:03:15,680 --> 00:03:26,480
of music and so I didn't find it help with our concentration at all, but eventually you

26
00:03:26,480 --> 00:03:33,920
realize what you're saying, what you have this theory, you have an intellectual understanding

27
00:03:33,920 --> 00:03:40,920
that it's simply vibrations, but on the other hand you seem to be partial towards certain

28
00:03:40,920 --> 00:03:47,080
vibrations, that certain vibrations and certain states of mind are preferable to others,

29
00:03:47,080 --> 00:03:52,480
and this is what we try to change in meditation, we try to realize that it's actually not

30
00:03:52,480 --> 00:04:00,560
the case, that there is no difference between this vibration and that vibration, we try

31
00:04:00,560 --> 00:04:06,040
to do away with our partialities, and as Panyan, he said to be comfortable in any situation,

32
00:04:06,040 --> 00:04:14,480
I think the key is, you're choosing the wrong quality of mind here when you say concentration,

33
00:04:14,480 --> 00:04:19,080
because it has to be admitted that there are certain vibrations that probably can affect

34
00:04:19,080 --> 00:04:24,440
the brainwaves and induce a certain state of trends in the mind, there are people who

35
00:04:24,440 --> 00:04:31,720
have these brainwave CDs and it's not music, but some kind of audio that affects the brain

36
00:04:31,720 --> 00:04:39,400
and increases the theta waves or the beta waves or the gamma waves, whatever waves

37
00:04:39,400 --> 00:04:46,120
in the mind that should be increased and whichever should be reduced, it helps to do that.

38
00:04:46,120 --> 00:04:48,760
This isn't the goal of Buddhism, this isn't the goal of the Buddha's teaching, the

39
00:04:48,760 --> 00:04:58,680
goal is to see that everything, any state that arises will have to cease and so it's not,

40
00:04:58,680 --> 00:05:03,480
it's not of any benefit, if there are certain sounds that calm you, then yeah, it might

41
00:05:03,480 --> 00:05:10,680
be a benefit to, in the beginning, to help you to settle into the meditation, but it can

42
00:05:10,680 --> 00:05:16,360
become a crutch and not only that, because it's essential enjoyment, because it gives rise to

43
00:05:16,360 --> 00:05:26,440
some kind of pleasant physical sensation, just like pleasant foods, pleasant sights and so on,

44
00:05:27,560 --> 00:05:34,360
smells, no people use a roman therapy to calm themselves down, this is, if it does create calm,

45
00:05:34,360 --> 00:05:40,520
it's considered to be wrong calm, because it's calm based on partiality and based on a certain

46
00:05:40,520 --> 00:05:48,760
type of experience, it's not calm that is a loop from experience, so as long as the smell is there,

47
00:05:48,760 --> 00:05:55,320
you'll become when the smell goes away, you will be, when someone flatulates, for example, then

48
00:05:55,320 --> 00:06:02,920
you'll be upset again, or when you're listening to your calming music, and then someone comes in,

49
00:06:02,920 --> 00:06:08,280
storms into the room and starts yelling, you'll go get very angry at them instead of as,

50
00:06:08,280 --> 00:06:12,760
I'm trying to meditate, I'm trying to become here, you'll get up and yell at them,

51
00:06:14,200 --> 00:06:20,760
so you can argue as much as you want, but the fact that you're creating this habit of

52
00:06:22,920 --> 00:06:28,600
requirement for this to make you calm, is really setting you up for the impermanence of the

53
00:06:28,600 --> 00:06:36,040
situation, you will be less calm as a result when other sounds arise, when more unpleasant

54
00:06:36,040 --> 00:06:40,760
sounds arise, when you have to go to John Tongue for two weeks during your festival,

55
00:06:41,800 --> 00:06:46,520
and meditate through two weeks of technical music.

56
00:06:48,600 --> 00:06:53,080
It has a related question, should we go to this right away?

57
00:06:54,360 --> 00:06:55,640
Yeah, we can, sure.

58
00:06:57,240 --> 00:07:05,160
Could we use music as a meditation tool to study our attachment and reactions to music,

59
00:07:05,160 --> 00:07:07,720
whether it be pleasure or displeasure?

60
00:07:12,360 --> 00:07:20,200
I wouldn't recommend using any object of attachment in that way,

61
00:07:21,720 --> 00:07:26,760
actively, so if it means going on and turning on the music, just to see how you enjoy it,

62
00:07:26,760 --> 00:07:30,840
the only problem is that catch that you're giving rise to the volition

63
00:07:30,840 --> 00:07:37,800
to create unwholesome, unwholesome states of mind, you're intending to give rise to the

64
00:07:37,800 --> 00:07:44,360
the defilements. Now, using music that exists as a meditation tool is perfectly valid,

65
00:07:44,360 --> 00:07:49,560
when the music is there, that's exactly what you should do. If you love the music, you should

66
00:07:49,560 --> 00:07:55,880
be mindful of it. This is how I would suggest you should deal with all addictions, if it's even a

67
00:07:55,880 --> 00:08:03,800
sexual attraction, the best thing to do is to be able to go see or at least visualize in your

68
00:08:03,800 --> 00:08:09,080
mind, you may not want to stare at the person, but visualize or when the visualization comes up

69
00:08:09,720 --> 00:08:15,160
to acknowledge it, no, I mean, I don't mean staring, but when you see something that's attractive

70
00:08:15,160 --> 00:08:22,280
to you to use that as an object, not even necessarily look away, but to use it as an object

71
00:08:22,280 --> 00:08:28,120
to meditation. If you can, now the problem is with all of these things is that

72
00:08:30,520 --> 00:08:36,360
if your mindfulness isn't strong, you rather than give rise to to wholesome mind states, you'll

73
00:08:36,360 --> 00:08:43,480
just give rise to defilement on both sides, the environment of liking the music or liking the visual

74
00:08:43,480 --> 00:08:50,120
or whatever, and or getting angry at yourself or hating the fact that you're becoming addicted

75
00:08:50,120 --> 00:08:57,800
again or feeling guilty about it and worrying about it and afraid about it. So you can use these

76
00:08:57,800 --> 00:09:04,520
things and you should eventually use these things directly. The music should, if you're addicted to

77
00:09:04,520 --> 00:09:11,080
music, eventually you should take it directly as a meditation object, but for, especially for a

78
00:09:11,080 --> 00:09:17,000
beginner meditator, it's absolutely improper and it should be forbidden. Music should not be allowed

79
00:09:17,000 --> 00:09:27,880
near meditators because their minds are so untrained that they have no ability to do this instead

80
00:09:27,880 --> 00:09:32,360
as a result when they hear the music outside of their rooms, they'll become obsessed with it and

81
00:09:32,360 --> 00:09:37,160
they will destroy their concentration because they don't know how to say hearing hearing and so on.

82
00:09:37,160 --> 00:09:41,640
So in the beginning, absolutely meditators should be sheltered and only slowly, slowly,

83
00:09:41,640 --> 00:09:48,520
be given back their, their, their, the objects of, of attraction. Once their minds become strong

84
00:09:48,520 --> 00:09:54,520
and able to deal with the small things, like the, the basic desires for food and so on and,

85
00:09:54,520 --> 00:09:58,840
and being able to deal with sitting still for a long period. So I mean, that's really enough.

86
00:10:00,200 --> 00:10:04,600
I wouldn't suggest using all of these things for a beginner meditator.

87
00:10:04,600 --> 00:10:18,120
The same is, if you dislike the music, it has to be noticed as well and all, what, what Banta said

88
00:10:20,600 --> 00:10:28,440
is, is valid for, for disliking of music as well. When you, when there is music that you have to

89
00:10:28,440 --> 00:10:36,600
hear because it is there and you are in your room and can't go away while meditating, then you

90
00:10:36,600 --> 00:10:46,360
can of course, as well meditate on your reactions and on your displeasure and on your disliking.

91
00:10:47,960 --> 00:10:53,640
But that for beginner meditator, you want to still shelter them from it as if possible

92
00:10:53,640 --> 00:11:01,160
because it's very difficult and it makes people run away and it makes, it also can lead them to

93
00:11:01,160 --> 00:11:07,960
because, as, because they're not able to deal with it impartially, it leads them to repress

94
00:11:07,960 --> 00:11:13,240
and it can cause problems in their meditation practice. So it's something that you do have to be

95
00:11:13,240 --> 00:11:29,160
compassionate about in the beginning.

