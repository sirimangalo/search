1
00:00:00,000 --> 00:00:07,000
A good evening, we're continuing on with our study of the Dhamapada.

2
00:00:07,000 --> 00:00:12,000
Today we're looking at verse 200, which reads,

3
00:00:12,000 --> 00:00:19,000
sushu kang vatajivama, ye sang no gnatik in janam,

4
00:00:19,000 --> 00:00:24,000
biti bha kha bhavisama,

5
00:00:24,000 --> 00:00:32,000
deva abha sara yatha, which means,

6
00:00:32,000 --> 00:00:35,000
happily indeed we live,

7
00:00:35,000 --> 00:00:41,000
we who have nothing,

8
00:00:41,000 --> 00:00:45,000
we will eat, we will feed,

9
00:00:45,000 --> 00:00:50,000
off of rapture,

10
00:00:50,000 --> 00:00:56,000
like the angels in the abha sara realm,

11
00:00:56,000 --> 00:01:01,000
the abha sara angels,

12
00:01:01,000 --> 00:01:10,000
ones who have radiance and abha.

13
00:01:10,000 --> 00:01:13,000
So like this is the third, if you remember the last two verses,

14
00:01:13,000 --> 00:01:16,000
we're also sushu kang vatajivama,

15
00:01:16,000 --> 00:01:18,000
happily indeed we live.

16
00:01:18,000 --> 00:01:20,000
This is the sukhavagat,

17
00:01:20,000 --> 00:01:23,000
so it's talking about happiness.

18
00:01:23,000 --> 00:01:26,000
I think the Buddha was talking always about suffering,

19
00:01:26,000 --> 00:01:31,000
well, even the reason he talked about suffering was in order to find happiness,

20
00:01:31,000 --> 00:01:36,000
freedom from suffering.

21
00:01:36,000 --> 00:01:41,000
This first was taught in regards to a story about

22
00:01:41,000 --> 00:01:44,000
Mara, really.

23
00:01:44,000 --> 00:01:48,000
The Buddha was dwelling in panchas,

24
00:01:48,000 --> 00:01:52,000
you know, panchasala,

25
00:01:52,000 --> 00:01:59,000
a Brahmin village called panchasala.

26
00:01:59,000 --> 00:02:04,000
And he thought to himself,

27
00:02:04,000 --> 00:02:07,000
well, he thought, who can I teach today?

28
00:02:07,000 --> 00:02:14,000
And he had this vision of this large group of maidens, young women,

29
00:02:14,000 --> 00:02:18,000
kumarika, young, young women.

30
00:02:18,000 --> 00:02:24,000
And he thought, oh, they will understand the dhamma if they hear it.

31
00:02:24,000 --> 00:02:27,000
And so he went into the village on arms,

32
00:02:27,000 --> 00:02:31,000
but he didn't get anything.

33
00:02:31,000 --> 00:02:35,000
And the text says that Mara is about Mara.

34
00:02:35,000 --> 00:02:39,000
Mara came into the village and went into their hearts

35
00:02:39,000 --> 00:02:46,000
and tricked or somehow made it so that Nandam gave anything.

36
00:02:46,000 --> 00:02:51,000
People who would, I guess, otherwise have given.

37
00:02:51,000 --> 00:02:54,000
And the Buddha came out of the village, and Mara came to him

38
00:02:54,000 --> 00:02:58,000
and taunted him and said, oh, so you went into the village on arms

39
00:02:58,000 --> 00:03:03,000
and didn't get anything.

40
00:03:03,000 --> 00:03:06,000
Did you get anything?

41
00:03:06,000 --> 00:03:08,000
The Buddha said, oh, I got no, I didn't.

42
00:03:08,000 --> 00:03:12,000
So I'll go back into the village and try again then.

43
00:03:12,000 --> 00:03:14,000
And he thought, and Mara thought,

44
00:03:14,000 --> 00:03:16,000
if he goes back into the village again,

45
00:03:16,000 --> 00:03:24,000
I'll get people to be mean and nasty too.

46
00:03:24,000 --> 00:03:32,000
And at that time, this large group of young women

47
00:03:32,000 --> 00:03:34,000
came to the Buddha when he was sitting there.

48
00:03:34,000 --> 00:03:37,000
And so it was an opportunity to teach the dhamma.

49
00:03:37,000 --> 00:03:41,000
Mara was taunting him and said, oh, you must be really hungry.

50
00:03:41,000 --> 00:03:44,000
You've got nothing to eat.

51
00:03:44,000 --> 00:03:47,000
And the Buddha said, I have nothing.

52
00:03:47,000 --> 00:03:53,000
It's true, but it's not true that I will not feed.

53
00:03:53,000 --> 00:04:04,000
And so I have no hunger because I feed off the rapture of enlightenment.

54
00:04:04,000 --> 00:04:08,000
It's a very, very simple teaching.

55
00:04:08,000 --> 00:04:10,000
It's also a fairly well-known teaching.

56
00:04:10,000 --> 00:04:14,000
It's a memorable one.

57
00:04:14,000 --> 00:04:18,000
Because being without is a part of life,

58
00:04:18,000 --> 00:04:24,000
it's a reminder to us of a good attitude

59
00:04:24,000 --> 00:04:28,000
to have in the face of what really

60
00:04:28,000 --> 00:04:33,000
just amounts to a change of experience.

61
00:04:33,000 --> 00:04:37,000
That's the key here, and that's the key in Buddhism.

62
00:04:37,000 --> 00:04:40,000
So the real lesson here in this story,

63
00:04:40,000 --> 00:04:50,000
in the verse, is about not getting what you want.

64
00:04:50,000 --> 00:04:54,000
How hunger comes from not having something that you want

65
00:04:54,000 --> 00:05:00,000
or wanting something that you don't have.

66
00:05:00,000 --> 00:05:06,000
For some people, happiness depends upon

67
00:05:06,000 --> 00:05:11,000
great deal of things, not just on food.

68
00:05:11,000 --> 00:05:14,000
For some people, for addicts,

69
00:05:14,000 --> 00:05:18,000
without drugs, there's no happiness.

70
00:05:18,000 --> 00:05:21,000
For ordinary people, without the objects of the sense

71
00:05:21,000 --> 00:05:27,000
that delight us, there's no happiness.

72
00:05:27,000 --> 00:05:38,000
But even for people who are otherwise not addicted

73
00:05:38,000 --> 00:05:43,000
to sensual pleasure, without things like food

74
00:05:43,000 --> 00:05:46,000
or what we would call the necessities of life,

75
00:05:46,000 --> 00:05:52,000
there can be no happiness.

76
00:05:52,000 --> 00:05:59,000
And it really highlights the shift in perception

77
00:05:59,000 --> 00:06:02,000
that we undertake in our practice of mindfulness.

78
00:06:02,000 --> 00:06:04,000
What does mindfulness change?

79
00:06:04,000 --> 00:06:06,000
What good is mindfulness?

80
00:06:06,000 --> 00:06:08,000
What does it do?

81
00:06:08,000 --> 00:06:12,000
It changes the way you look at your life,

82
00:06:12,000 --> 00:06:14,000
at reality.

83
00:06:14,000 --> 00:06:17,000
Instead of looking at your experience,

84
00:06:17,000 --> 00:06:20,000
your situation, in terms of what you have

85
00:06:20,000 --> 00:06:26,000
and what you don't have.

86
00:06:26,000 --> 00:06:28,000
You come to see it of what you're experiencing.

87
00:06:28,000 --> 00:06:33,000
You come to see your reality in terms of experience.

88
00:06:33,000 --> 00:06:37,000
And the idea of getting or not getting having or not having

89
00:06:37,000 --> 00:06:42,000
disappears.

90
00:06:42,000 --> 00:06:45,000
And there's a quote, George Orwell,

91
00:06:45,000 --> 00:06:48,000
runs right a book by...

92
00:06:48,000 --> 00:06:53,000
He was writing memoirs of when he had nothing.

93
00:06:53,000 --> 00:06:59,000
He fell into great poverty living in Paris and London.

94
00:06:59,000 --> 00:07:04,000
And he said there's a great freedom that comes from

95
00:07:04,000 --> 00:07:09,000
when you reach rock bottom when you have nothing.

96
00:07:09,000 --> 00:07:14,000
Because you know there's no lower you can go.

97
00:07:14,000 --> 00:07:17,000
Your perspective changes.

98
00:07:17,000 --> 00:07:26,000
There's nothing to be afraid of because it can't get any worth.

99
00:07:26,000 --> 00:07:33,000
This is a reason why I think a good reason

100
00:07:33,000 --> 00:07:38,000
to think about becoming a Buddhist monk or leaving home

101
00:07:38,000 --> 00:07:45,000
and undertaking a reclusive life.

102
00:07:45,000 --> 00:07:47,000
Because it changes your perspective.

103
00:07:47,000 --> 00:07:51,000
And it brings into contrast what you have

104
00:07:51,000 --> 00:07:55,000
and what you don't have what you have

105
00:07:55,000 --> 00:07:59,000
and what you need to have to be happy.

106
00:07:59,000 --> 00:08:05,000
And you're able to see all the things that you require to be happy.

107
00:08:05,000 --> 00:08:11,000
You're able to change your perspective and work on these cravings.

108
00:08:11,000 --> 00:08:19,000
Work on this suffering.

109
00:08:19,000 --> 00:08:26,000
So people will say things like you need friends to be happy.

110
00:08:26,000 --> 00:08:30,000
You need possessions to be happy.

111
00:08:30,000 --> 00:08:34,000
You need something to entertain you in order to be happy.

112
00:08:34,000 --> 00:08:37,000
The very least you need food to be happy.

113
00:08:37,000 --> 00:08:45,000
The way our idea of happiness is in the externals.

114
00:08:45,000 --> 00:08:48,000
In things.

115
00:08:48,000 --> 00:08:50,000
Because we dwell in the realm of things.

116
00:08:50,000 --> 00:08:55,000
We think of me eating food is getting a thing called food

117
00:08:55,000 --> 00:08:57,000
and putting it in my body.

118
00:08:57,000 --> 00:09:00,000
We don't think of it as just an experience.

119
00:09:00,000 --> 00:09:05,000
We aren't able to see that eating food and the feeling of fulfillment

120
00:09:05,000 --> 00:09:14,000
that comes, the pleasure that comes from eating the food is all just experiences.

121
00:09:14,000 --> 00:09:18,000
And so we lack, well, we cultivate partiality

122
00:09:18,000 --> 00:09:26,000
and we lack the flexibility that comes from seeing reality as experience.

123
00:09:26,000 --> 00:09:32,000
When you're able to perceive food and friends and possessions

124
00:09:32,000 --> 00:09:37,000
and all the things that make us happy as simply experiences,

125
00:09:37,000 --> 00:09:39,000
you gain a flexibility.

126
00:09:39,000 --> 00:09:44,000
Well, you come to see that there's no difference between one experience

127
00:09:44,000 --> 00:09:45,000
and another.

128
00:09:45,000 --> 00:09:47,000
You can't cling to an experience.

129
00:09:47,000 --> 00:09:53,000
An experience can't be something that you wish for or want for.

130
00:09:53,000 --> 00:09:59,000
I mean, when to see the momentary experiences.

131
00:09:59,000 --> 00:10:03,000
You know, not talking about an experience in a conceptual sense

132
00:10:03,000 --> 00:10:07,000
but in a momentary sense, you can't cling to it

133
00:10:07,000 --> 00:10:12,000
because it arises in ceases and it has no quality

134
00:10:12,000 --> 00:10:19,000
that is good or bad or even desirable or undesirable.

135
00:10:19,000 --> 00:10:28,000
When you look at things in terms of experience,

136
00:10:28,000 --> 00:10:31,000
you're watching when we say to ourselves pain,

137
00:10:31,000 --> 00:10:37,000
pain, thinking, thinking when you watch in that way,

138
00:10:37,000 --> 00:10:47,000
you gain this flexibility, sort of adaptability or flexibility.

139
00:10:47,000 --> 00:11:03,000
The piece that comes from being content with whatever comes.

140
00:11:03,000 --> 00:11:05,000
So we can talk about contentment in things,

141
00:11:05,000 --> 00:11:09,000
but ultimately if we have craving, if we have attachment,

142
00:11:09,000 --> 00:11:12,000
there are things that we like and things that we don't like.

143
00:11:12,000 --> 00:11:15,000
A thing is something you can attach to.

144
00:11:15,000 --> 00:11:19,000
It's a concept in the mind,

145
00:11:19,000 --> 00:11:24,000
something that you can remember having.

146
00:11:24,000 --> 00:11:29,000
It's a thing that can provide stability,

147
00:11:29,000 --> 00:11:32,000
satisfaction, control.

148
00:11:32,000 --> 00:11:35,000
You can control things.

149
00:11:35,000 --> 00:11:38,000
You can keep things.

150
00:11:38,000 --> 00:11:47,000
You can be satisfied with things, comfortable furniture,

151
00:11:47,000 --> 00:11:55,000
pleasant food, electronic devices, people, places.

152
00:11:55,000 --> 00:12:06,000
All of these things can provide stability and satisfaction and control.

153
00:12:06,000 --> 00:12:09,000
But they don't really exist.

154
00:12:09,000 --> 00:12:15,000
The problem that we have with things is that

155
00:12:15,000 --> 00:12:21,000
they're dependent on the underlying experiences and experiences are unpredictable.

156
00:12:21,000 --> 00:12:23,000
When you talk about having food,

157
00:12:23,000 --> 00:12:27,000
what you really mean is having the potential to have experiences,

158
00:12:27,000 --> 00:12:30,000
moments of experience where you're eating,

159
00:12:30,000 --> 00:12:34,000
even where you're just seeing the food as an experience.

160
00:12:34,000 --> 00:12:36,000
These are unpredictable and controllable.

161
00:12:36,000 --> 00:12:44,000
Another important lesson of this story is about Mara,

162
00:12:44,000 --> 00:12:46,000
about evil.

163
00:12:46,000 --> 00:12:51,000
Whether or not you believe that there was this malevolent spirit

164
00:12:51,000 --> 00:12:55,000
taking over people's minds and hearts and making them stingy,

165
00:12:55,000 --> 00:12:58,000
stingy and as does exist.

166
00:12:58,000 --> 00:13:10,000
And manipulation where people's circumstances of your life are plotting against you,

167
00:13:10,000 --> 00:13:13,000
people plotting against you.

168
00:13:13,000 --> 00:13:17,000
Just chance, fate, plotting against you,

169
00:13:17,000 --> 00:13:20,000
your own bad karma coming back to haunt you.

170
00:13:20,000 --> 00:13:22,000
So many things.

171
00:13:22,000 --> 00:13:28,000
But evil is in particular something that we have to recognize,

172
00:13:28,000 --> 00:13:35,000
whether you recognize Mara as a being who came to the Buddha and taunted him.

173
00:13:35,000 --> 00:13:41,000
Going into his village and not getting any arms is a common experience for months.

174
00:13:41,000 --> 00:13:43,000
It certainly happens.

175
00:13:43,000 --> 00:13:49,000
And by extension, anything, any activity that we undertake,

176
00:13:49,000 --> 00:13:54,000
trying to obtain something that we want and not getting it,

177
00:13:54,000 --> 00:13:59,000
that's common to everyone.

178
00:13:59,000 --> 00:14:03,000
Often because of the evil intentions of others, stinginess,

179
00:14:03,000 --> 00:14:05,000
miserliness, not wanting to share,

180
00:14:05,000 --> 00:14:09,000
not wanting to give, not wanting to help,

181
00:14:09,000 --> 00:14:12,000
wanting you to be without, so they steal from you,

182
00:14:12,000 --> 00:14:18,000
or they break things of yours, or they hurt you with speech or actions.

183
00:14:18,000 --> 00:14:25,000
We think this body is sort of so much pleasure,

184
00:14:25,000 --> 00:14:30,000
but someone with bad intentions comes to you.

185
00:14:30,000 --> 00:14:34,000
They can do great harm and cause great suffering.

186
00:14:34,000 --> 00:14:37,000
All because of our attachment to things,

187
00:14:37,000 --> 00:14:44,000
the concept of something that is stable, satisfying, controllable.

188
00:14:44,000 --> 00:14:53,000
And in fact, reality is not like that.

189
00:14:53,000 --> 00:14:58,000
So this really speaks to the power of mindfulness,

190
00:14:58,000 --> 00:15:00,000
the power of wisdom,

191
00:15:00,000 --> 00:15:04,000
the power of seeing things just as they are,

192
00:15:04,000 --> 00:15:07,000
being able to see things just as they are,

193
00:15:07,000 --> 00:15:11,000
rather than react to them or conceive in them.

194
00:15:11,000 --> 00:15:15,000
Like this is food that I'm eating, this is my body that is healthy,

195
00:15:15,000 --> 00:15:23,000
that is full and nourished with food.

196
00:15:23,000 --> 00:15:25,000
The Buddha indeed, every day,

197
00:15:25,000 --> 00:15:27,000
went to nourish his body,

198
00:15:27,000 --> 00:15:29,000
but when he didn't receive nourishment,

199
00:15:29,000 --> 00:15:32,000
there was no suffering.

200
00:15:32,000 --> 00:15:37,000
The Buddha had a greater nourishment,

201
00:15:37,000 --> 00:15:41,000
and that's the nourishment of the present moment, the nourishment of mindfulness,

202
00:15:41,000 --> 00:15:44,000
the nourishment of enlightenment.

203
00:15:44,000 --> 00:15:51,000
Being enlightened means seeing things clearly.

204
00:15:51,000 --> 00:15:55,000
Being without food is simply a different sort of experience.

205
00:15:55,000 --> 00:15:59,000
It may make you incapable of doing certain things,

206
00:15:59,000 --> 00:16:05,000
but it simply means a different form of experience.

207
00:16:05,000 --> 00:16:07,000
The state where the body is weak,

208
00:16:07,000 --> 00:16:12,000
it's tired because of food is an equally valid experience to the state

209
00:16:12,000 --> 00:16:18,000
where the body is energetic and full of effort.

210
00:16:18,000 --> 00:16:22,000
For a person who is attached to things like food,

211
00:16:22,000 --> 00:16:25,000
nourishment can be a detriment.

212
00:16:25,000 --> 00:16:27,000
When you are too well fed,

213
00:16:27,000 --> 00:16:32,000
you can become intoxicated by the strength of the body.

214
00:16:32,000 --> 00:16:39,000
The chemicals in the body start to work and you feel aroused.

215
00:16:39,000 --> 00:16:42,000
I mean too much food can be a problem for people who are attached

216
00:16:42,000 --> 00:16:45,000
because it leads to addiction, it leads to...

217
00:16:45,000 --> 00:16:47,000
it can lead to belligerents,

218
00:16:47,000 --> 00:16:50,000
where people are well fed,

219
00:16:50,000 --> 00:16:54,000
much more capable of fighting with each other.

220
00:16:54,000 --> 00:16:56,000
Having less food can sometimes be good,

221
00:16:56,000 --> 00:17:01,000
and for certain, not getting what you want is always going to,

222
00:17:01,000 --> 00:17:06,000
and for becoming a monk or even a recluse of some sort.

223
00:17:06,000 --> 00:17:11,000
There's a great power and benefit to it because it teaches you.

224
00:17:11,000 --> 00:17:14,000
It gives you a new perspective.

225
00:17:14,000 --> 00:17:18,000
The life of someone who has everything they want gets everything they want

226
00:17:18,000 --> 00:17:24,000
is fraud with peril, the potential for losing what you get,

227
00:17:24,000 --> 00:17:29,000
what you have, and the potential for clinging to what you have

228
00:17:29,000 --> 00:17:34,000
and desiring more, becoming addicted to it,

229
00:17:34,000 --> 00:17:40,000
and needing it and being dissatisfied whenever you can't get it.

230
00:17:40,000 --> 00:17:44,000
The happiness of having nothing is a very powerful thing,

231
00:17:44,000 --> 00:17:52,000
and it's not that having things is a cause of suffering necessarily.

232
00:17:52,000 --> 00:17:59,000
But the power and the happiness that comes from not needing anything,

233
00:17:59,000 --> 00:18:05,000
being happy without things, having your happiness not depend on things.

234
00:18:05,000 --> 00:18:08,000
It's the highest sort of happiness there is.

235
00:18:08,000 --> 00:18:11,000
It's invincible.

236
00:18:11,000 --> 00:18:14,000
You have no vulnerability.

237
00:18:14,000 --> 00:18:17,000
A person whose happiness depends on things.

238
00:18:17,000 --> 00:18:23,000
My happiness depending on me going for food and getting food

239
00:18:23,000 --> 00:18:29,000
is very dangerous for me, setting myself up for suffering.

240
00:18:29,000 --> 00:18:33,000
Now, if everything, if I could control everything and everything,

241
00:18:33,000 --> 00:18:36,000
when does planned, that would be great.

242
00:18:36,000 --> 00:18:42,000
But this first highlight is a very important part of reality

243
00:18:42,000 --> 00:18:48,000
that is the potential for us not to get what we want, the potential for evil.

244
00:18:48,000 --> 00:18:50,000
We don't know what's going to happen.

245
00:18:50,000 --> 00:18:54,000
We don't know what our future is, but more specifically,

246
00:18:54,000 --> 00:18:57,000
we don't know when evil might hit.

247
00:18:57,000 --> 00:18:59,000
It could be in the form of another person.

248
00:18:59,000 --> 00:19:06,000
They might harm us, steal from us.

249
00:19:06,000 --> 00:19:08,000
We manipulate our lie to us,

250
00:19:08,000 --> 00:19:15,000
cheat us, might come in the form of the body, sickness,

251
00:19:15,000 --> 00:19:19,000
might come in the form of accidents,

252
00:19:19,000 --> 00:19:25,000
whether natural disasters.

253
00:19:25,000 --> 00:19:28,000
Certainly come in the form of our own defilements

254
00:19:28,000 --> 00:19:31,000
when we get angry and do things that harm ourselves,

255
00:19:31,000 --> 00:19:35,000
when we get greedy and become addicted and attach to things

256
00:19:35,000 --> 00:19:43,000
that create ambitions and actions that cause us suffering.

257
00:19:43,000 --> 00:19:45,000
Happy without anything.

258
00:19:45,000 --> 00:19:48,000
It's a real powerful statement.

259
00:19:48,000 --> 00:19:53,000
We can use it to apply to not getting anything in our lives,

260
00:19:53,000 --> 00:20:00,000
but we can always think of the Buddha

261
00:20:00,000 --> 00:20:03,000
when we don't get what we want.

262
00:20:03,000 --> 00:20:06,000
And remember that even when the Buddha had no food,

263
00:20:06,000 --> 00:20:09,000
which is something that for many of us is

264
00:20:09,000 --> 00:20:12,000
a very difficult thing psychologically,

265
00:20:12,000 --> 00:20:14,000
emotionally to be without,

266
00:20:14,000 --> 00:20:17,000
just the fear of not having enough food to eat,

267
00:20:17,000 --> 00:20:20,000
the stress of going hungry,

268
00:20:20,000 --> 00:20:26,000
worry about not being able to nourish your body.

269
00:20:26,000 --> 00:20:30,000
Think about the Buddha and how he said

270
00:20:30,000 --> 00:20:35,000
he had a greater nourishment than the food.

271
00:20:35,000 --> 00:20:40,000
His happiness wasn't dependent on getting what he wanted,

272
00:20:40,000 --> 00:20:44,000
because his way of looking at things was not in terms of

273
00:20:44,000 --> 00:20:46,000
things that you can get,

274
00:20:46,000 --> 00:20:50,000
in terms of experiences which frees you

275
00:20:50,000 --> 00:20:56,000
and liberates you from the slavery,

276
00:20:56,000 --> 00:21:04,000
really, of needing things.

277
00:21:04,000 --> 00:21:07,000
So that's the Dhammapada for tonight.

278
00:21:07,000 --> 00:21:34,000
Thank you all for listening, wish you all the best.

