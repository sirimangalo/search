Okay, good evening, everyone, broadcasting live, October 10th, 2015, let's try another
and then we'll pause the story tonight.
Let's give me a second.
Good evening, and welcome back to our study of the Dhamapada tonight.
We continue on with verse number 82, which reads as follows.
Just a leg, a gallery, though, is deep.
And now we know that is undisturbed, just as a calm, clear, deep leg, just like a calm,
clear, deep leg, even the manis of one.
So are those who have, having listened to the dhamma, or so are the wise, having listened
to the dhamma.
So to the wise, having listened to the dhamma, we proceed and become tranquil, become calm.
So the idea here is of a link that becomes calm, a person who listens to the dhamma becomes
calm, just like a calm, deep leg.
Being a hero is, that depth is used the same way it is in English to talk about a person's
depth in their wisdom.
So a wise person will be deep or found.
This story was, this verse was told in regards to a story about a woman named Kana and
her mother.
Kana was married to a man, and of course, she would have gone to live in her husband's house,
and one time she went back to visit her parents and stayed there for quite some time.
Eventually, her husband called for her and said, Mr. was missing her and asked her to return.
And Kana was preparing to return, but her mother said, oh, no, you can't go back empty-handed
after all this time.
You should bring something back to give to your husband as a gift.
So the mother was kind thinking, mad good intentions, and Kana herself was a kind person.
She had good intentions, so she was happy to do this.
But Kana was also beyond that, she was a generous and compassionate person, and so she
waited, and when the cake was ready, sorry, it was a cake, so the mother said, wait,
I'll bake you a cake, and you can bring the cake back to your husband.
So Kana waited, and when the cake was ready, she was preparing to leave, and then all
of a sudden the monk showed up, a Buddhist monk showed up at her door, and so she had
nothing else she thought, yeah, I'll give this cake to the monk, but a good opportunity
to give arms, and so she gave him the cake, which would have been a real fine for him,
because cake wouldn't be something that you would get, especially a fine cake here, that
this woman was ready to give to her husband.
So she gave him the cake, and went back to her mother and said, look, I gave the cake
to a monk, can you make me another one, and her mother was so fine with that, oh, that's
great, good for you.
Meanwhile this monk did something that he probably shouldn't have done, it was an older
fellow and a little bit excited about the cake, and so he went back to the monastery and
told the other old monks about it, they sit around a bunch of old guys, oh yeah, I got
a cake from this house, you got a cake, so a second monk heard this and he went to the house,
and he got there just as the cake was baked, and the woman was getting ready to return
to her husband, and she saw him coming, and again, felt obligated to give another cake,
so she gave the new cake to the second monk, and she was still happy, but she was like,
okay, well, it's good that we're giving the monks, and really better get going, so
she asked her mother, please, mother, one more time, that's all, just last time, one
more cake, and the money was like, oh, well, okay, very well, baked her a third cake.
Of course, the second monk went back to the monastery and told his friends, and a third
monk came, and getting a little bit frustrated at this point, but still not wanting to
turn anyone away empty handed, she gave a third cake to the third monk.
Meanwhile, her husband was kind of frustrated at this point, I think this all happened
on one day, it would sort of make more sense, it happened one per day, but I think it was
the same day, and so her husband sent a messenger and said, okay, wait, wait, wait, she's
coming, but I just have to wait a little bit longer, because I'm baking the second cake,
and then baking the third cake, and they baked the third cake in a fourth month, a third
monk came, right?
And we said that third monk came, baked the fourth cake, baked the third cake away,
baked the fourth cake, a fourth monk came, and at this point, the husband got fed up and
was told that he should just find another wife, and indeed that's what he did, he figured
that his wife was never coming home, and so he went and found a new wife.
Anyway, just a story of one of those things that happens, but the upshot of it was that
Kanan was not happy, and she blamed monks, that she felt sort of obligated to give food
to these monks, and you could say the monks were to blame the Buddha, in fact, used this
opportunity, or this situation, to create a rule, we have one of our rules in regards
to this story, and the rule is that you have to have to take a limited number of cakes,
if people are offering cake, and I guess by extension anything that's sort of a fine food,
there's a limit on what you can take, and when you get something like that, you have
to share it, whatever you get, with the other monks, it's not like I got some, so you
go and get your own cake, and everyone gets an entire cake for themselves.
Anyway, kind of previous prior to this being a gentle and kind person became a mean
and vicious person, towards the monks, anyway, and whenever she saw a monk, she would
reveal them, call them beggars, and greedy, and mean and nasty, so she would say
mean things to them, doesn't say what she actually said, but she said they have ruined
these monks have ruined my merry life, because her husband went and got another wife, husbands
can sort of do what they want to do, indeed that's what he did in a fickle sort of fellow.
The Buddha found out about this, and he went to see Kana, and he asked her, is it true
that this is happening, and she said, yes, they destroyed my marriage, and she said, well,
did they steal the cakes from me, or did you give them willingly?
I gave them to them, and they even asked you for cake, no, they didn't ask me for cake,
and so the Buddha just pointed out how silly she was being, and in that case, who was
to blame here, who was the one who was to blame, okay, well, it's true, I guess I am to blame,
and because of that, the Buddha, once she accepted that, the Buddha taught her the
dhamma, and she actually sitting there was able to put it into practice, and become
so dependent, and then she asked this forgiveness for being ruined to the monks, even
though you could argue that the monks didn't, weren't completely properly behaved, it's
true, they didn't even beg for anything, they just came to see whether they were giving
an indeed she kept giving, I'm up to her, she could have stopped and said no, on the other
hand it was kind of a neat thing to do, and say, well, either way, wasn't very neat
thing for her to revile and revile other people, right, and so this is why the Buddha taught
this verse, monks were talking about it, the Buddha taught about how she had been a mouse
in the past life, but I don't, I'm not going to get into the Jataka story, she reviled
a bunch of cats, and the cats ended up dying, there's not much point to the Jataka story
except she was reviling cats, then the Buddha taught the Jatata verse, he said, our mind
was turned in and became calm as a lake of still water, and then he taught the verse,
the Jatata being, rahado, gampiro, vipasana, and now we know, and so on, the money, sutwana
is interesting, the money means the dhammas, it's plural, and it's, it's a, it's a
neutral plural, but it probably doesn't mean much the most people, but it's an interesting
use of the word dhamma, but it means the dhammas, so truths, good teachings.
So how does this relate to our practice?
Well, immediately comes to mind for me as the, the whole concept of meditation interviews
that were we've been conducting, we have these weekly interviews of people who have been
making appointments on our meditation page, and then we have a Google Hangout one-on-one
and help people out, give them advice, and I've been doing this for a few weeks down, it's
worked really great, but before that I've done this in person, and you know, just having
someone there to remind you and to clarify your doubts is, it's quite a stabilizer for
your practice, you know, it's easy to get caught up, we get all worked out, about turning
mountains into moles, making mountains out of moles, turning something very small into something
very big, because it's very hard for us to see our faults, it's very hard for us to guide
ourselves, especially on something, especially on something we, that is new to us, something
unfamiliar, but hearing the dhamma is a great thing, and it's amazing how in the Buddhist
time, just hearing the dhamma was often enough to enlighten someone, and people often question
how this could be possible, but this is a part of it really, that the dhamma is something
that quiets your mind and focuses your mind, hearing it, is keeping it fresh, it's bringing
your attention back again and again to the truth, to the good things.
So if someone teaches about the force of di patana or someone teaches about insight,
someone teaches about the five aggregates, the nature of reality, at the same time the mind
of the listener becomes, not only calm, but becomes clear, so as able to see things clearly,
let me talk about how a meditator knows when they're sitting, that they're sitting, then
everyone listening and suddenly becomes aware of the fact that they're sitting and becomes
aware of the present moment, so the mind of the listeners brought back again and again
to these and other good things. On top of that, listening is kind of a special interaction
because we're listening live anyway, because you're forced to stay still. Those of you
listening at home, there's something lacking here, and you're still listening to the dhamma
potentially, but you have the ability to get up or talk to other people in the room, make fun
of me, check Facebook, make comments and so on because we can't see or hear you, but when you're
sitting in a, or something about sitting in front of a teacher, listening to them teach,
that makes it a meditation in itself, you can't move, you can't even respond, you just have to
receive or receive. As a result, becoming enlightened when listening to the dhamma talk is a very
viable thing, becoming so dependent, there's no reason to doubt that this was possible,
that the Buddha can teach and people would become enlightened while they were sitting there.
Sometimes all they needed was a little nudge and the Buddha was very good at nudging people
in the right direction.
So this is why we encourage listening to talks, that's why we encourage going to see people
who can teach through the dhamma, that's why we encourage undertaking courses under a teacher
as opposed to just reading about them. But even reading, you know, there's something just about
hearing the truth, having someone teach you good things or reading or learning about good things.
So the Buddha said better than a thousand words that are meaningless is one word when you hear it
that brings you peace. So quite a simple verse, a simple little story.
One other thing you could say about this in regards to the idea of how it can help an angry person
is often we wonder how to deal with conflict, how do I deal when someone's accusing me of things
shouldn't I fight back, right, when someone's attacking me shouldn't I defend myself,
how do I convince someone of my position and so on.
I think the key is in this verse, this is a good point as well that you teach that you say the truth,
when people hear the truth they become calm. You don't have to argue, you don't have to persuade,
you listen and if you're mindful and your heart is pure and clear,
when you speak it's the truth and just hearing the truth comes people.
All arguing doesn't solve things, defeating the other person doesn't
create peace, but hearing the truth creates peace.
When you give people the truth that they need to hear, the dumb, they're going to teach you.
It brings peace.
That's why mindfulness is so important in a conversation,
mindfulness is so important in our dealings with other people.
It's difficult, but it is something that we should always think about when we're approaching
a conversation, especially one that we know is going to be hostile or fraught with emotion.
We should bring mindfulness to the fore, but in Mukung, setting Mukha to a
bring mindfulness to the fore, first off, and then engage in our activities.
So anyway, that's dumbapada verse 82. Thank you all for tuning in and be well.
I forgot to start the audio feed apologies.
How do we do that?
Okay, the audio feed is no lie.
I forgot about that one, something you have to remember.
So, how are you rubbing?
I'm okay, but are you really? You had a long day. Are you tired?
No, I had a shorter day than I did yesterday.
Very busy still.
Okay.
Hit me with some questions.
Okay, going back to yesterday because I didn't finish.
My mind is very busy during walking meditation so much so that I would have to stop very frequently
to to note them all. Should I stop frequently and ultimately go away or let them go and keep walking?
If they're just minor distractions, yes, you can just let them go.
I mean, there's no hard and fast rule. Maybe sometimes it's better to stop. Maybe sometimes it's
better to keep going. You have to be clever and sort of get a sense of what's useful at the time.
It's not magic. It's not a video game that you have to figure out and then you get points or
something. It's you learning about reality. If you think it's not going to help you that much to
stop and look at thoughts, then you have permission not to stop. But if they're serious and real
distractions, certainly don't ignore them. You should stop and say if it's lots of little thoughts,
then you should stop and say distracted and distracted. Become aware of your state of mind.
One day I have read that the Buddha had talked about qualities that one could develop,
which would lead one to be reborn in higher realms. Could you please explain those qualities
that the Buddha talked about? Which ones are those?
Quality is leading to higher realms. Haven't you said, generosity?
There's four. Seelang-sutang-jagang-jagapanya, I think. But there's many lists, you know. I mean,
that's one list. That's one list. It's probably most well-known. Well, it's fairly well-known.
Seelang-jagang you need to have morality. Morality is something that leads you to heaven.
Sutang learning being well-learned. Must be a sutang as one is one. Jagang-jagang means generosity.
And Banya is wisdom. These are the Deva dhammas, I think.
Things that lead you to heaven.
If you were able to get a monastery with acreage, would it be acceptable for supporters to be
residents and do something like farming to support the monastery? I think I understood you to say
earlier that it would not be acceptable for monks to farm. But what about supporting residents?
Is there such a thing that could be acceptable?
It couldn't be acceptable. Not ideal. This is the thing a monastery should be a place where
people are engaged in meditation. The farm should be separate from the monastery.
But you could divide land into two parts. You have the meditation center on one part and then you
have the lay society on another part. And so people living in one part could farm and go to work
and do as they like. And then people in the other part went engaged in studying practice.
Couldn't do it that way. But this wouldn't be in the case where there was no other way to get
there was no way to get food because ideally you're using the space for meditation,
for meditation center.
When monks are at monastery, this is just my own question here. When monks are in monasteries in
Asia, do they get tasks to do like housekeeping and things like that?
I mean if it's a good monastery, most often they don't actually and they just do it when they
feel like it. It's pretty good. You know, many monasteries are good about keeping things clean,
but often they end up at the bigger monasteries end up hiring someone to do it because the monks
are not doing it. But if a monastery gets really big, it's in the monks couldn't possibly do it
because they're too busy and there's too much space, too much to do. So they do have to hire people.
But monks have duties. In my teacher's monastery, it's pretty good about giving everyone
and honestly it's not all that good or it wasn't. It may have gotten better, but it's hard to
give monks in Thailand anyway, duties in most monasteries because there's a sense that you can't
tell them what to do. There's this idea, sort of the feeling that you can't tell them what to do.
So the nuns, the white closed nuns all have duties and they're really well organized.
But the monks, but what I've seen not so much, it's unfortunate really they all should have duties.
Hi there, now this man as well, like the equivalent of the white closed nuns, or this
I'm in for well. We tried to do that for one of my meditators because nuns can get a special visa.
They wouldn't let a man become a nun. They said because they have men have the option of becoming
a bikka, but he couldn't because he's from Iran and his mother was still there and he could never
go back to Iran as a monk. So what happened? He went back to Iran.
I think it was more that his parents weren't even going to give him permission. That was another
part of it. I can't remember one of them, but I think he couldn't go back. It's hard. If we feel a
lot of love during Wipassana, are we doing it wrong? No. No wrong about Wipassana. Wipassana meditation
is trying to gain insight, hence the word Wipassana into what's going on. So if you have love,
then you try to gain insight into the nature of love. You can say feeling or love. And then you
see that love is impermanence suffering in oneself. Meaning love is just another experience. It
comes and it goes unsatisfying. It's not what we're looking for. It's not the goal. I mean,
nothing that your experience is. It's just about seeing things arise in season till you let go of
everything. Even love. When an insect flies near your ear or if you step near a snake,
your reptile brain kicks in. At that point, you lose all mindfulness for survival.
Should this be a natural thing? I do. How do you know?
I have a reptile brain. It's true that our brain sometimes kicks in, but
the mind has a part to play. And this is what mindfulness is. Mindfulness is the gatekeeper.
Your brain can tell you swatted, swatted. But if you're mindful, you can just say one thing,
one thing intending. You can note the intention and not do anything. There's no reptile
around. I mean, there is the brain that tells you do it to it. But there's a choice.
There's a moment where mindfulness can neutralize you.
Is it common to feel a lot of love terribly positive?
Not terribly. It's common to think about people who are dear to you, like your parents.
That's a big one where people will often don't say the majority of people who go through
a course at some point think about people like their parents. They have strong emotional ties to
and usually it's with love and with gratitude and that kind of thing.
If there's anything to be grateful for, which usually there is for most of us.
But it's not that it doesn't happen. It's just to say that it's a thing that happens to most
people and majority of people. Not well, you're practicing me personally. We often add it to
or we pass in a practice. We'll send love after a practice. We all beings be happy.
Hence we have this little heart there to signify that that's what you're doing.
Can we practice more than more we practice?
I don't know what that means. Oh wait, do we practice more than more we practice?
Can we practice more than more we practice more than more we practice more?
They don't really understand.
I think I understand, but maybe I don't, you know, do you have to build up to longer
maybe you have to build up to longer meditation sessions and things like that.
I still don't understand.
Well you can't start out meditating for hours at a time.
Oh I see. What the more you practice makes you able to do more?
I think that's what the question is. Do we have the ability to practice more or more intensely
than more we practice? Sure, isn't that sort of obvious?
Maybe if I got your question wrong, please resubmit it because
it might have been interpreting it wrong.
And the next question is why do some people get a heart instead of a V when they finish
meditation? Because they clicked on the check mark to signal that they're sending mecha.
Bunty, what is your opinion on guns?
There are things that you use to kill other things to kill other beings.
I'm not very good. Some people use them just for fun, right? Shooting targets.
So there's that, but no, but it's just for fun.
Some people, what else could they use guns for?
Maybe they use them to defend themselves, right? So you have a gun in your house
to defend yourself.
I mean, that would be one that I could, I could understand and say, okay, well,
there you have a valid point. But then the question is, would you kill the
killer person in self-defense? Because that's not ethical.
Maybe you just shoot them in the leg or something.
And then a bad idea, I think, having a gun because then you might end up shooting someone
given in self-defense or in defense of someone else, which
not the best way, not the best thing.
We're to do about subtle happiness or rather subtle contentment.
Can one feel content and not have the preference for it?
And you can feel strong happiness and not have a preference for it. Happiness isn't the problem.
It's the attachment. So if you're happy and you say, happy, happy, you can have strong
happiness. That's fine. But as soon as you start to like it, then you've got a problem.
And then you talk about mindfulness. What does it mean to be mindful when not meditating?
I think this is someone who hasn't read my book yet.
Chapter six.
It sounds like they don't even know what I mean by mindfulness.
Who is it? Where are they?
There's two minutes ago.
They're orange.
I've done a little meditation with this. Hi, John. I've got a picture.
Well, I would recommend you read my booklet all the way through because it answers this question.
It's a fair question because mindfulness is used for so many different things.
You hear it constantly, trying to learn a lot of different things.
I mean, do I? Maybe I don't use the word mindful as much in the book.
But the book's all about mindfulness, even if I don't use that word.
The word mindfulness isn't necessarily the best translation. It's just a common word that we use
as a catchphrase.
But I'm a little more precise in the book about the idea of creating a clear thought.
That's what we mean by being mindful to have a clear thought, clear awareness of something,
clear recognition. This is this, not anything else.
You hear that so much just in general conversation. People really mean being careful or being
thoughtful and you just get used to it. My father just sent me a link to an article in the
New York Times maybe just this evening about this guy who was complaining about evangelical
meditators, who proselytize and push meditation. When he says, I don't meditate,
trying to convert him to meditation. It's really a terrible article because then he goes on to say,
he talks about other ways you can gain mindfulness and explains them and it's just like,
we really don't know what mindfulness is. Not to blame the person because mindfulness is just
as you say used for so many things. So he was saying like drugs can, you can use drugs for
stress really. Because everyone says meditation is for stress relief. Well, it's a little deeper
than that. Meditation, ideally, is for changing your habit so you don't create stress in the
first place. And drugs won't do that.
For someone with no background, the idea that it's stress relief is that's kind of what brought
me to meditation initially because I was very stressed out. And the idea that something would
relieve this terrible stress that I was feeling was very appealing. And then it was only through time
and learning more and more and more that it became more. But that was definitely what drew me
to it initially. I mean, it really is the it is the truth. It is for stress relief. Dukka is
often translated as stress. So it's true. But it's it's not stress relief probably in the way you
were thinking of it or in the way most people think it's deeper than that. And I guess it's not
just that it goes beyond stress relief. Stress relief itself has to go beyond our ordinary thinking
about stress. It's not just turning the stress off. It's about changing our habit so we don't
create the stress in the first place. We're still stress relief. But it's long-term lasting
sustainable stress relief from an stress relief. Yeah, but something you have to learn through
the process. It doesn't make sense if you don't have any background in that. But there is
an argument to be made that it's simplistic to to say that it's meditate. What's meditation good
for stress relief calms you. You know, it's true. And in fact, it is sort of the final goal, in fact.
But we have to be knowledgeable enough to say it's not just like a drug, which is what my book
is all about. The start of the book is about. There's two kinds of meditation meditation that's
like a drug and meditation that's like a medicine. One makes you feel good and relieves your
symptoms. The other cures the disease.
One day last night when you were meditating, I noticed your head kept nodding. What is the
cause for that? I don't talk about my own meditation. I'm sorry. Plus, that means this person
wasn't meditating. If you were watching, you made the date. Well, no, they could watch the video.
It was recorded. Oh, that's true. That's true. Sorry. But still,
that's kind of perverse sitting there watching some of those meditating.
Are you working on a second book of meditation?
Yes. We've done how many chapters for chapters already? Two more in the works.
We'll see when hopefully this week. I'll do another one.
What's the easiest way for someone to to access that if they wanted to read it?
No, I go through your Google Plus page, but there's probably more direction.
No, it's uncertain. Mongol, I think. It wasn't until fairly recently, but if you go to
teachings, texts, how to meditate too.
It brings up a page and yes, there are four chapters.
Have one day, when should one increase meditation time and when should one decrease it?
Well, within reason, increase it when you have the time,
but it should be up to a point where you're fairly comfortable being mindful.
You know, you should push yourself a little bit, but don't push yourself too much so that it
becomes a chore and that you get negative conditioning towards meditation where you begin to
hate it because it's just stressful. So it should be comfortable, but push you a little bit,
challenge you a little bit. You have to get a sense of that. Now, when you're on a meditation
course, we'll just keep pushing you and pushing you and pushing you. I mean, we'll keep challenging
you so that the teacher will do that for you. If you're really interested in this kind of
thing and you're able to do at least an hour a day, you can sign up for one of our courses,
just sign up for an appointment slot and start practicing and I'll help you.
Progress, although I haven't been talking too much about times, I haven't told people
do this many minutes increase a little bit, but not so much because mostly it's up to their
schedule. How much did the Buddha know? Did he know everything? That's what they say.
But he didn't know everything. You see, that's not how it works. Knowing everything requires
infinite number of minds, right? Because only one mind can only know one thing. The point was
anything he thought about he could find the answer to with unobstructed knowledge.
What is the singing ball used for? I don't know, ask the Tibetans.
I don't use the singing ball. I have one here, but it's actually just to create a bell noise for
our meditation timer. I believe it is a singing ball to signal the start and end of the meditation.
I'm pretty sure it's a singing ball. I mean, it has that sound. Anything else?
Yeah, that's common. I use them to remain as a signal. Start and end of meditation.
I have been focusing my noting on feelings of pressure, stiffness,
lucidity, etc. Where did these fit into the four elements? And how should we note the other elements?
That's why you're not a pressure stiffness lucidity. That's the air element. That's the rising
and falling of the stomach is also the air element. Fire element is heat and cold, so you say
hot, hot, or cold, cold. We're just feeling feeling. And the hardness and softness is the air
element. So you feel it on when you're sitting, you feel it on the chair, you feel it on the floor,
carpet, or whatever you're saying, the hard floor that you're sitting on, then you can just note feeling.
I don't can hear you speaking through my speaker or school. Sorry about that.
I should do a noise cancellation, but it's not perfect. I heard Shins and Young talking about
meditation-induced derealization, depersonalization episode. Is this really a risk?
I've actually heard of this. Someone was arguing that the going technique leads to
depersonalization, derealization. I don't, I mean, we don't use those terms, but
I remember vaguely when we were talking about this, I looked it up and
part of it sounds okay. I mean, you're losing your ego, you're losing your sense of self.
Well, yeah, that's sort of the point, because then you lose your ego, your attachment to self.
But I think part of it was just sort of, I mean, yeah, anything's possible. The mind is a
powerful thing. If you abuse it, you can go crazy. It doesn't mean you shouldn't take a course
to learn how to train it in a proper way. It won't happen if you have a good guide and
are clear about where you're going, what you're doing. Certainly won't happen in this technique,
if you know it, if you're followed it teaching. We've had people go, I've seen people go crazy
because they didn't have good guidance and they totally misunderstood what was being taught.
So they'd start saying like wisdom, wisdom, wisdom. Do they use it? Do they use the mantra,
mantras or powerfuls? Maybe you'd use them to try and get what they wanted. I was saying,
wisdom, wisdom, wisdom. We're repeating their name over and over again. I mean, it's,
yeah, you could have yourself crazy if we do that.
If one experience is nodding of the head during sitting meditation,
it'd be just due to letting go in a profound way, or can it happen when the body is tired?
Thank you. But you know what? You haven't done any meditation. So look at the answer that one.
Maybe clever eye can be clever too. You have to ask that again when once you've done some
meditation with us. Sorry.
We're all caught up. We're all caught up.
We had 45 people doing our YouTube stream there for a second.
So I mean, we're doing this daily and having people listen to people watching. It's great that
we've got a community. It's great to see people. I had a conversation today with one of the
online meditators. He had been trying to create a meditation group in his area. He said he's heard
that people have tried this around universities thinking that university would be a good place.
He said something curious. He said it's almost impossible to do around the university.
It's almost impossible. And then he said, but to do it in a place where there is no university
is impossible. That was quite interesting. No, no, I'm feeling good about this place. Honestly,
I think I'm on universities and great place. It's great because of the turnover. You're always
having new people who you can help. It's like having a community of new blood always coming in.
So you may not reach people in a terribly profound way, but you're able to reach a lot of people
as they come and stay for four years and then go. And the other thing is that people I'm meeting
now will be around for another four years, right? So by the time we hit our fourth year,
we'll have people who really know that we're here. And then that will stay and new people
will constantly becoming. Whereas in another community, you have the people who you have in the
community and it's more static. So I'm sure we can do good things here. Even if it just means
giving people good advice or kind words or it's really neat being in Canada, honestly.
I mean, it's probably just because I'm familiar with the culture, but it's not really, you know,
I get every time we get off the bus. This really curious thing happens. 75% of the people who get
off the local public transit say thank you to the bus driver. Even if they're getting off at the
back of the bus, they shout out, thank you. And it's curious to me because, well, the person's just
doing their job. They're not really doing even favor, but it's kind of neat. So I started doing
it. No, every time I get off, I say thank you. So there's other than Canada too on the buses.
Maybe that and where you're from? Not so much, but I actually always do. When I get off a bus,
I always say thank you. I really am thankful that they were a safe driver and I made it to my
destination in one place. And that they're nice to you. I mean, part of it, I think is
sort of the appreciation that they're not being a jerk and because some bus drivers can be
very few, but you know, there's a potential to have a little bit. I was in,
it's funny in the US buses, because in some places are really weird. I was in
San Luis Obispo, I think. It's a long time ago, but I was somewhere in California.
And I stood up on the bus and he said sit down. I was like, there were two or three people on
the public bus and I stood up because I wanted to get a brochure that was in one of the pockets
or the timetable. And so I sat down and then he stopped at a stoplight and I got up again to get
him and he stopped the bus and he came over and he started. Anyway, I don't know where this story
was going, but it gets back to the point of kindness and there's that and there's people holding
open doors for each other and just being in a sort of in a society where you have the,
there's something about university students. Whatever you may else you may say about them is
there is a sense of love, friendliness, and harmony. It's not a work environment where people
are competing, right? We're helpful. You know, some guy came and asked me for my notes for one
of the classes, so I sent them to him by email and he was very thankful and people do this.
You know, they send each other their notes and help each other out and they're kind and hold
doors open for each other and really is a positive environment in those aspects.
Things probably the big one is the alcohol and drugs, more alcohol, but both. And you know,
just sort of being called a hedonism, hedonism, that's a big thing in the West with people around
that age especially, but there's good potential. I wonder if it's different with the evening
classes. I always had taken a lot of evening classes and it was a more an older group,
you know, mostly adults working during the day. Right. There are quite a few evening classes,
but I'm reluctant to take any because of this, because of what I'm doing in the evening,
there's a couple of good evening classes. They may have to just bite the boat and say,
one day a week, we're not having evening session.
I wonder if the students understand the benefits of meditation and even even the fact that
through meditation, it's, you know, your mind is clear and it's easier probably to study and
remember things. I've been thinking the same thing. I'm thinking maybe to create a poster that
that has the lists of benefits because we haven't done much of that. We've advertised meditation,
but we haven't given information as to why someone, why a student would be,
shouldn't be interested in meditation.
It's not maybe ultimate reality that grabs you right at first because you don't know enough to
be interested in ultimate reality. It's this more, you know, that it believes stress or that it
helps you to clear your mind. Great way. Okay. Anyway, that's it for tonight.
Thank you all for tuning in. Thank you, Robin for helping out. Thank you. Thank you.
