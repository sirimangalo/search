1
00:00:00,000 --> 00:00:06,180
When I sit and say to myself sitting, I have to do a quick body scan, at least of my legs

2
00:00:06,180 --> 00:00:09,720
and spine, in order to really know that I'm indeed sitting.

3
00:00:09,720 --> 00:00:14,240
Isn't that quick body scan somewhat contrary to a one-pointed mind?

4
00:00:14,240 --> 00:00:17,480
Is one label really enough?

5
00:00:17,480 --> 00:00:19,360
You don't really have to do that.

6
00:00:19,360 --> 00:00:27,040
I would say it's not really the object or exactly proper meditation, it's much more forceful.

7
00:00:27,040 --> 00:00:32,960
We could in the beginning and you could say that it's at least calming the mind down or

8
00:00:32,960 --> 00:00:33,960
focusing the mind.

9
00:00:33,960 --> 00:00:39,720
You guys remember, nothing, none of that's wrong in tranquility meditation.

10
00:00:39,720 --> 00:00:45,960
If your purpose is simply to calm the mind, you can force it, push it, need it, mold

11
00:00:45,960 --> 00:00:51,240
it into shape.

12
00:00:51,240 --> 00:00:54,000
Concentration meditation is not for the purpose of cultivating insight.

13
00:00:54,000 --> 00:01:00,520
So in a sense, we can use that at least in the beginning and so a meditator and vipassana

14
00:01:00,520 --> 00:01:07,920
meditation might calm themselves down by doing these body scans, by just knowing that

15
00:01:07,920 --> 00:01:08,920
they're sitting.

16
00:01:08,920 --> 00:01:12,280
In vipassana meditation, the point is not the sitting.

17
00:01:12,280 --> 00:01:15,760
Sitting is a description of those feelings that you have.

18
00:01:15,760 --> 00:01:20,360
The fact that you're aware of the tension in your back or the pressure on the floor, at

19
00:01:20,360 --> 00:01:23,720
that moment when you say sitting, that's enough.

20
00:01:23,720 --> 00:01:28,560
You're aware of that experience and you call that experience sitting.

21
00:01:28,560 --> 00:01:33,800
It's simply important to recognize this is a physical phenomenon, it's not so important

22
00:01:33,800 --> 00:01:36,360
to be aware that this is a sitting posture.

23
00:01:36,360 --> 00:01:40,360
Put your mind on the whole body, the entire body, say to yourself sitting.

24
00:01:40,360 --> 00:01:53,440
The recognition that that is a sitting posture comes on its own and comes unexpectedly.

25
00:01:53,440 --> 00:01:59,080
It's the sunya part, the recognition that this is sitting.

26
00:01:59,080 --> 00:02:04,400
Sunya is impermanent and so what you're seeing that leads you to say that you have to

27
00:02:04,400 --> 00:02:09,200
do this in order to push it in order to come is that it's non-self.

28
00:02:09,200 --> 00:02:10,800
It doesn't come automatically.

29
00:02:10,800 --> 00:02:11,800
Sometimes it does.

30
00:02:11,800 --> 00:02:16,040
You immediately know that you're sitting, you recognize, oh, this is a sitting posture.

31
00:02:16,040 --> 00:02:20,240
Sometimes you look and look and there's no being, this is sitting, it just never are

32
00:02:20,240 --> 00:02:21,560
aware that it's sitting.

33
00:02:21,560 --> 00:02:23,480
This is because it's not self.

34
00:02:23,480 --> 00:02:28,800
You can't force yourself to know that's what you're seeing and so that is the whole purpose

35
00:02:28,800 --> 00:02:31,840
of practicing and that's what you're going to find.

36
00:02:31,840 --> 00:02:35,880
You're going to find that sometimes you are aware of the feelings, sometimes you're

37
00:02:35,880 --> 00:02:38,880
aware of this feelings, sometimes you're aware of that feeling, sometimes you're aware

38
00:02:38,880 --> 00:02:44,400
of the recognition of the feeling and putting it together as sitting, sometimes you're not.

39
00:02:44,400 --> 00:02:48,200
That's the point of the practice, not to perfect it.

40
00:02:48,200 --> 00:02:53,040
This is key and this, I did a video on this that I think is most important, one that we should

41
00:02:53,040 --> 00:03:00,560
emphasize again and again that this is often people, often meditators very often come to

42
00:03:00,560 --> 00:03:06,200
the conclusion that they're practicing incorrectly or they're unable to practice because

43
00:03:06,200 --> 00:03:09,440
specifically because they're seeing what we want them to see.

44
00:03:09,440 --> 00:03:13,800
They're seeing impermanence, they're suffering, they're seeing non-self.

45
00:03:13,800 --> 00:03:21,320
I would say much more important is the practice than perfecting it, perfecting it's not

46
00:03:21,320 --> 00:03:51,160
going to happen, not in incitementation.

