1
00:00:00,000 --> 00:00:03,520
mind returning to nose or upper lip.

2
00:00:03,520 --> 00:00:10,160
What do you do if the mind continuously returns to a more familiar objects of meditation?

3
00:00:10,160 --> 00:00:13,360
For example, the tip of the nose or upper lip.

4
00:00:13,360 --> 00:00:19,120
Noting sensations caused by the breath that any parts of the body is not strong.

5
00:00:19,120 --> 00:00:24,160
Wisdom can be cultivated based on any real experience.

6
00:00:24,160 --> 00:00:29,600
The abdomen is favored due to its gross nature as opposed to the sensations

7
00:00:29,600 --> 00:00:34,720
that will care around the mouth which are most subtle and therefore favored

8
00:00:34,720 --> 00:00:36,880
tranquility meditation.

9
00:00:36,880 --> 00:00:44,560
As our tradition has developed specific techniques based on starting with the abdomen as a base,

10
00:00:44,560 --> 00:00:50,560
we ask meditators coming to our center to use it as their primary object.

11
00:00:50,560 --> 00:00:55,760
If meditators are accustomed to using another object of meditation,

12
00:00:55,760 --> 00:01:02,640
they should note the experiences that arise based on past habits when the mind returns

13
00:01:02,640 --> 00:01:04,400
to the other objects.

14
00:01:04,400 --> 00:01:12,720
For example, what old feeling of simply knowing that the mind has returned to the nose or mouth.

15
00:01:12,720 --> 00:01:17,360
Note the experience until the mind becomes this interested

16
00:01:17,360 --> 00:01:33,280
bear upon one should return to the abdomen and continue noting as usual.

