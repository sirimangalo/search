1
00:00:00,000 --> 00:00:05,000
Well, the budget can come here and teach to us this is really rare.

2
00:00:05,000 --> 00:00:16,000
Not a last, but I probably can only come in to make customer life, unfortunately it's gotten too busy, but it's always a great pleasure to come in.

3
00:00:16,000 --> 00:00:23,000
I'm going to be listening to your trips, so thank you.

4
00:00:35,000 --> 00:00:37,000
Is he staying?

5
00:00:37,000 --> 00:00:39,000
Mike, you know, I think so.

6
00:00:39,000 --> 00:00:46,000
He's going back right now. Nice going.

7
00:00:50,000 --> 00:00:57,000
Yeah.

8
00:00:57,000 --> 00:01:02,000
He's hit all about the book, which is the market.

9
00:01:02,000 --> 00:01:09,000
This book is the handbook for meditating months.

10
00:01:09,000 --> 00:01:19,000
So it was written by a month for the use of months to practice meditation.

11
00:01:19,000 --> 00:01:32,000
And this book was written on the scheme of three steps towards spiritual development.

12
00:01:32,000 --> 00:01:42,000
The first step is morality and second step is concentration and the third step wisdom.

13
00:01:42,000 --> 00:01:53,000
So we have gone through chapters on morality, concentration, and some chapters on wisdom too.

14
00:01:53,000 --> 00:02:12,000
So the first and second chapters deal with morality and then the other chapters until the 13th chapter deal with what is called concentration.

15
00:02:12,000 --> 00:02:23,000
So in these chapters, the 40 subjects of some mathematician or tranquility meditation are treated.

16
00:02:23,000 --> 00:02:36,000
And then with the 14th chapter, we come into the realm of what is called understanding of wisdom.

17
00:02:36,000 --> 00:02:53,000
And these chapters, chapter 14 to chapter 17, are in preparation for vipasana meditation.

18
00:02:53,000 --> 00:03:16,000
So before we practice vipasana meditation, according to this book, we should understand some of something about the aggregates, based sense basis, faculties, noble truths, and the dependent origination.

19
00:03:16,000 --> 00:03:36,000
So in order to go further from the 18th chapter, we will have to be familiar with at least the chapters from chapter 14, in chapter 17.

20
00:03:36,000 --> 00:03:54,000
Because the subjects treated in those chapters will be brought to bear upon what will be said in these chapters.

21
00:03:54,000 --> 00:04:00,000
And with the 18th chapter begins the real vipasana meditation.

22
00:04:00,000 --> 00:04:17,000
So the other chapter, the preceding chapters are just for preparation, like giving you information about these aggregates and so on, which you will contemplate on, which you will see through the practice of vipasana meditation.

23
00:04:17,000 --> 00:04:33,000
And vipasana meditation is described with reference to what is called purity, different stages of purity. And there are seven stages of purity described here.

24
00:04:33,000 --> 00:04:57,000
If you look at, for a graph one, then you see the reference back to, I think, chapter 14 here.

25
00:04:57,000 --> 00:05:10,000
But there are first two purities, purity of morals and purity of, purity of mind.

26
00:05:10,000 --> 00:05:13,000
So purification is called purification.

27
00:05:13,000 --> 00:05:17,000
So purification of virtue and purification of consciousness.

28
00:05:17,000 --> 00:05:40,000
Purification of virtue means keeping the precepts, not breaking them. And purification of consciousness means the practice of sopata meditation until one gets the neighborhood concentration and also the absorption concentration.

29
00:05:40,000 --> 00:05:46,000
So these are called purity of purification of consciousness.

30
00:05:46,000 --> 00:05:51,000
Yeah, consciousness really means some anti concentration.

31
00:05:51,000 --> 00:06:04,000
And then these two purification are called the roots of root purification, because they are like roots.

32
00:06:04,000 --> 00:06:14,000
On these two roots, the other purification will be built. So the other purification are what?

33
00:06:14,000 --> 00:06:33,000
A paragraph two. Purification of view purification by overcoming doubt purification by knowledge and vision of what is thought and what is not thought purification by knowledge and vision of the way and purification by knowledge and vision.

34
00:06:33,000 --> 00:06:38,000
They are called trunk purification.

35
00:06:38,000 --> 00:06:42,000
So all together there are seven purifications.

36
00:06:42,000 --> 00:06:50,000
So the first two purifications are dealt with in the chapters from 1 to 13.

37
00:06:50,000 --> 00:06:34,000
And then from 14 to 17 chapters are in preparation for the

38
00:06:34,000 --> 00:06:50,000
we pass on our meditation. And now this chapter deals with the first two purifications are dealt with in the chapters from 1 to 13.

39
00:06:50,000 --> 00:06:58,000
And then from 14 to 17 chapters are in preparation for the we pass on our meditation.

40
00:06:58,000 --> 00:07:07,000
And now this chapter deals with what?

41
00:07:07,000 --> 00:07:15,000
The purification of view. So this chapter is just the expression of purification of view.

42
00:07:15,000 --> 00:07:27,000
Purification of view really means having good, having a right view with regard to aggregates, bases and so on.

43
00:07:27,000 --> 00:07:38,000
So let's say a person practices we pass on our meditation.

44
00:07:38,000 --> 00:07:49,000
According if you follow the order in this book, you will first practice some time meditation and try to get and journals.

45
00:07:49,000 --> 00:07:58,000
Or it may not practice some time meditation at all but go direct to Vipasana.

46
00:07:58,000 --> 00:08:07,000
So first the definition defining of mentality of materiality.

47
00:08:07,000 --> 00:08:12,000
So defining of mentality of materiality is explained in this chapter.

48
00:08:12,000 --> 00:08:22,000
And defining of mentality materiality really means seeing during meditation mind and matter clearly.

49
00:08:22,000 --> 00:08:31,000
So seeing mind not mixed with matter and matter not mixed with mind.

50
00:08:31,000 --> 00:08:39,000
So seeing mind clearly and matter clearly. And that is here called defining of mentality and materiality.

51
00:08:39,000 --> 00:08:52,000
So in order to understand, in order to define mentality and materiality, we need to understand what is mind and what is matter.

52
00:08:52,000 --> 00:09:05,000
According to both the teachings and they are describing the chapter preceding chapters.

53
00:09:05,000 --> 00:09:14,000
Chapter 14.

54
00:09:14,000 --> 00:09:21,000
The watch in our mind, consciousness and mental states or mental factors are all mind.

55
00:09:21,000 --> 00:09:33,000
And matter is as you know the physical thing, physical properties in our bodies as well as outside things like tables, chairs, trees and so on.

56
00:09:33,000 --> 00:09:47,000
So first the paragraph three and following just cry.

57
00:09:47,000 --> 00:09:55,000
How a person whose vehicle is serenity or who practices some time meditation first.

58
00:09:55,000 --> 00:10:02,000
Define mind and matter and mind on mentality and materiality.

59
00:10:02,000 --> 00:10:09,000
And here one who wants to accomplish this if firstly his vehicle is serenity.

60
00:10:09,000 --> 00:10:17,000
That means he practices some sort of meditation first and gets Janas.

61
00:10:17,000 --> 00:10:24,000
So for him he should emerge from any fine material or immaterial Janas.

62
00:10:24,000 --> 00:10:34,000
So you know there are eight Janas, four material and four immaterial Janas.

63
00:10:34,000 --> 00:10:40,000
But accepting the base consisting of neither perception nor non-perception.

64
00:10:40,000 --> 00:10:44,000
That is the highest of the immaterial Janas.

65
00:10:44,000 --> 00:10:58,000
It is so subtle that it is very difficult for one who first defines mind and media to be able to define clearly.

66
00:10:58,000 --> 00:11:03,000
So it is accepted.

67
00:11:03,000 --> 00:11:14,000
So we have all the seven Janas here. So imagine from any of the fine material or immaterial Janas.

68
00:11:14,000 --> 00:11:18,000
Except the base consisting of neither perception nor non-perception.

69
00:11:18,000 --> 00:11:22,000
And he should descend according to characteristic function, etc.

70
00:11:22,000 --> 00:11:27,000
The Janas factors consisting of a pipeline to our probe.

71
00:11:27,000 --> 00:11:33,000
First he entered into the Janas if he has attained and then he emerges from the Janas.

72
00:11:33,000 --> 00:11:46,000
And then take the factors of Janas like initial application, sustained application and so on.

73
00:11:46,000 --> 00:11:56,000
So take the factors of Janas as the object of the personal meditation or take the other mental factors

74
00:11:56,000 --> 00:12:02,000
which accompany the Janas as the object of rib as a non-meditation.

75
00:12:02,000 --> 00:12:14,000
And he tries to see their characteristic function, manifestation and so on.

76
00:12:14,000 --> 00:12:25,000
Now whenever we study a thing in Abirona, we are to understand it with reference to four or three aspects.

77
00:12:25,000 --> 00:12:28,000
The first is characteristic. The second is function.

78
00:12:28,000 --> 00:12:38,000
And the third is manifestation or the mode of manifestation and the full is approximate cause.

79
00:12:38,000 --> 00:12:54,000
So he should contemplate on the factors of Janas or the other mental factors accompanying the factors of Janas with reference to their characteristic function

80
00:12:54,000 --> 00:13:01,000
and a mode of manifestation.

81
00:13:01,000 --> 00:13:13,000
When he has done so all they should be defined as mentality in the sense of bending because of pitch bending on to the object.

82
00:13:13,000 --> 00:13:29,000
So he contemplates on or makes himself aware of these men with the Janas factors and other mental states and taking maybe one by one

83
00:13:29,000 --> 00:13:36,000
and trying to understand the characteristic and so on of these mental states.

84
00:13:36,000 --> 00:13:47,000
Then he defines that this is a mentality, this is a mind, this belongs to mind.

85
00:13:47,000 --> 00:13:55,000
This is mental and then later on we will go to the matter.

86
00:13:55,000 --> 00:14:02,000
So first he defines them as mental, as nama and bali.

87
00:14:02,000 --> 00:14:09,000
So what the bali word nama, the literal meaning of the bali word nama is bending towards.

88
00:14:09,000 --> 00:14:12,000
So nama is that which bends towards the object.

89
00:14:12,000 --> 00:14:31,000
So when you close attention to the object during meditation you will, or at least you may, you will come to be aware that mine is like hitting the object or going towards the object.

90
00:14:31,000 --> 00:14:41,000
So you, you, you develop on say like it noise outside and you, you take the noise as an object and then you may have some other thoughts.

91
00:14:41,000 --> 00:14:47,000
And so your mind is going to that object and then to another object and another object and so on.

92
00:14:47,000 --> 00:15:00,000
So mine is that which bends towards the object and that is called, that is why it is called nama in, in bali.

93
00:15:00,000 --> 00:15:13,000
After defining mentality, he defines materiality or matter.

94
00:15:13,000 --> 00:15:22,000
So the foot, photograph, describe that.

95
00:15:22,000 --> 00:15:27,000
So then just as a man by following his neck that he has seen in his house finds his apple.

96
00:15:27,000 --> 00:15:39,000
So to this many little scrutinizes that mentality, he seeks to find out what its occurrence is supported by and he sees that it is supported by the matter of heart.

97
00:15:39,000 --> 00:15:47,000
Now according to the teachings in the abitama, every type of consciousness must have a physical basis.

98
00:15:47,000 --> 00:15:52,000
So seeing consciousness has the eye as a physical basis.

99
00:15:52,000 --> 00:16:02,000
And then there are some other types of consciousness which have heart, heart as a physical basis.

100
00:16:02,000 --> 00:16:14,000
So first he dwells upon the mind and then following the mind that he,

101
00:16:14,000 --> 00:16:23,000
that he discovers or he seeks to find out by what this mind is supported.

102
00:16:23,000 --> 00:16:41,000
And then he comes to realize that the heart is the support of the factors of Jana and then those concomitant with the factors of Jana.

103
00:16:41,000 --> 00:16:51,000
After that he decides as materiality the primary element which has the heart support and the remaining derived kinds of materiality that the elements has the support.

104
00:16:51,000 --> 00:17:03,000
Now there is a heart base and heart base is a dependent type of matter.

105
00:17:03,000 --> 00:17:15,000
And heart base depends upon the other great primaries. You know there are full great primaries or full great elements at the element, what the element for element and element.

106
00:17:15,000 --> 00:17:27,000
So the heart base, very small particle of the heart base, depends upon the full great elements.

107
00:17:27,000 --> 00:17:34,000
So he discovers these full great elements also.

108
00:17:34,000 --> 00:17:40,000
And he demands all that as materiality because it is molested by cold etc.

109
00:17:40,000 --> 00:17:49,000
That means it changes by heat cold, by hunger, by touch, by bytes of insects and so on.

110
00:17:49,000 --> 00:17:58,000
Now this is the different meaning of the what, Ruba. You see the what Ruba and Paragraph 4.

111
00:17:58,000 --> 00:18:14,000
So Ruba is so cold because it is molested or it changes. It changes with climate, it changes with hunger, it changes with thirst and so on.

112
00:18:14,000 --> 00:18:27,000
So he defines the base of the consciousness as Ruba as matter.

113
00:18:27,000 --> 00:18:43,000
So a button whose vehicle is samata or serenity or tranquility meditation defines first the mental things, the factors of Jana and its confidence.

114
00:18:43,000 --> 00:18:58,000
And then he tries to find the base of these mental states and then he finds the matter which is the base of these mental states.

115
00:18:58,000 --> 00:19:10,000
And then he finds it as Ruba. So during meditation he first dwells on the mental things.

116
00:19:10,000 --> 00:19:15,000
And then from those mundane he goes to material things.

117
00:19:15,000 --> 00:19:20,000
So first he defines the nama and then he defines Ruba.

118
00:19:20,000 --> 00:19:28,000
That is for a person who has serenity meditation as a vehicle.

119
00:19:28,000 --> 00:19:33,000
That means Ruba first practice is serenity meditation and it gets Jana.

120
00:19:33,000 --> 00:19:42,000
Now there are people who do not practice samata meditation, but just practice few Ruba samata meditations.

121
00:19:42,000 --> 00:19:49,000
For them since they have no Jana to dwell upon there is another matter.

122
00:19:49,000 --> 00:20:01,000
So Paragraph 5 and following Paragraph just cried his way of contemplating his way of defining

123
00:20:01,000 --> 00:20:07,000
a mentality and materiality.

124
00:20:07,000 --> 00:20:15,000
And also it was in whose vehicle is serenity can follow this matter too.

125
00:20:15,000 --> 00:20:27,000
Because it is up to him whether to begin with the mentality first and materiality later or

126
00:20:27,000 --> 00:20:31,000
to take materiality first and mentality later.

127
00:20:31,000 --> 00:20:42,000
But for a person whose vehicle is Vipasana he must begin with materiality first and then go to mentality.

128
00:20:42,000 --> 00:20:55,000
Because materiality or matter is easier to see for him than the mental states or types of consciousness.

129
00:20:55,000 --> 00:21:06,000
So one whose vehicle is pure inside or that same a force that means the person whose vehicle is serenity.

130
00:21:06,000 --> 00:21:13,000
He sends the phone elements in brief or in detail in one of the various ways given in the chapter of the definition of the phone elements.

131
00:21:13,000 --> 00:21:20,000
Now you have to go back to chapter 11.

132
00:21:20,000 --> 00:21:33,000
So this is the method of defining materiality and mentality.

133
00:21:33,000 --> 00:21:43,000
It is described in how many ways describe by way of full elements and then by way of 18 elements.

134
00:21:43,000 --> 00:21:58,000
And by way of 12 sense basis by way of aggregates and then by way of very brief defining.

135
00:21:58,000 --> 00:22:01,000
So different ways are given here.

136
00:22:01,000 --> 00:22:20,000
So the first is by way of the full grid elements actually the heading which we have on page 679 definition based on the four primaries should be here.

137
00:22:20,000 --> 00:22:33,000
It should be below paragraph 4.

138
00:22:33,000 --> 00:22:41,000
So only from there and the definition based on the full primary thinking.

139
00:22:41,000 --> 00:22:51,000
And then we have studying with materiality.

140
00:22:51,000 --> 00:23:06,000
So when the elements that we can clear in the correct essential characteristic firstly in the case of head hair originated the comma, they have become playing 10 instances of materiality with the body that does the full elements,

141
00:23:06,000 --> 00:23:30,000
the color, the flavor, the sensitive essence and life and body sensitivity.

142
00:23:30,000 --> 00:23:38,000
Here to their cost by comma, cost by consciousness, cost by temperature and cost by nutrition.

143
00:23:38,000 --> 00:23:51,000
And then these material properties are treated treated in groups, a group of 8, a group of 9, a group of 10 and so on.

144
00:23:51,000 --> 00:24:06,000
And these groups are actually not found in the text but in the commentaries we find that these and also in the manual of a bit of material properties treated in groups.

145
00:24:06,000 --> 00:24:12,000
So according to these groups the other is here explaining.

146
00:24:12,000 --> 00:24:22,000
So if you are not familiar with these passages, you difficult to understand or maybe confusing.

147
00:24:22,000 --> 00:24:48,000
Here, first, although it is said here that it is according to the full grid elements, we must understand that when describing the defining of full grid elements, the other is with reference to 32 parts of the body.

148
00:24:48,000 --> 00:25:05,000
So we have to go back to 32 parts of the body and remember which part is cost by which and so there are 32 parts of the body here.

149
00:25:05,000 --> 00:25:18,000
Here, body has native skin and so on, until you read what are the elements.

150
00:25:18,000 --> 00:25:25,000
Now, in the book said, had hair originated by comma.

151
00:25:25,000 --> 00:25:36,000
That hair is originated by comma and it is also originated by consciousness originated by temperature and originated by nutrition.

152
00:25:36,000 --> 00:25:39,000
It is caused by all full causes.

153
00:25:39,000 --> 00:25:56,000
So for the head hair caused by comma, there will be 10 instances of materiality with the body that it does.

154
00:25:56,000 --> 00:26:11,000
And because the sex dagger is present, there are another 10, that is the same 9 with sex instead of body sensitivity.

155
00:26:11,000 --> 00:26:16,000
And since the octave with nutritious essence L8, the long name, that is the full elements and cava would have driven nutrient essence originated by nutrition, nutrient.

156
00:26:16,000 --> 00:26:18,000
And that originated by temperature.

157
00:26:18,000 --> 00:26:35,000
And that originated by consciousness and present, there are another 24. So there is a total of 44 instances of materiality in the case of each of the 24 body weapons of full full origination.

158
00:26:35,000 --> 00:26:48,000
Now, because if you look at the causes, the bottom of the page, they go down first, they are temperature originated, they are caused by temperature caused by wood.

159
00:26:48,000 --> 00:26:55,000
And then tears, sweat, spittles, they are caused by temperature and consciousness.

160
00:26:55,000 --> 00:26:57,000
So they are caused by two causes.

161
00:26:57,000 --> 00:27:06,000
Fire that digest, there are four kinds of fire, fire elements and the last one is fire that digest what we eat, drink and so on.

162
00:27:06,000 --> 00:27:10,000
So that fire is comma-originated.

163
00:27:10,000 --> 00:27:16,000
If you have a good stomach, that means you have a good cava.

164
00:27:16,000 --> 00:27:18,000
If you have a good digestion.

165
00:27:18,000 --> 00:27:34,000
And then in an outbursts, inbursts, and outbursts are consciousness-originated because breathing is present only in those beings that have consciousness.

166
00:27:34,000 --> 00:27:43,000
And the rest, the remaining awe of full full orchid origination, so they are caused by all full causes.

167
00:27:43,000 --> 00:27:55,000
And then we take head hair and then since head hair is not in the above full groups, it is caused by full full causes.

168
00:27:55,000 --> 00:28:05,000
And for head hair caused by comma and there are some, a number of material properties.

169
00:28:05,000 --> 00:28:14,000
And then for head hair caused by consciousness, a number of properties.

170
00:28:14,000 --> 00:28:18,000
And then head hair caused by temperature.

171
00:28:18,000 --> 00:28:24,000
And for head hair caused by a new treatment, a number of material properties.

172
00:28:24,000 --> 00:28:32,000
And so all together, it's an incredible company.

173
00:28:32,000 --> 00:28:38,000
For the four instances of materiality, in the case of the done before, what are the parts of full origination?

174
00:28:38,000 --> 00:28:40,000
So there are all together for the four.

175
00:28:40,000 --> 00:28:46,000
But in the case of four memories where tears better than start with the originated temperature and consciousness,

176
00:28:46,000 --> 00:28:53,000
there are sixteen instances of materiality with the two objects with nutritious essence as eight in each.

177
00:28:53,000 --> 00:28:58,000
Now, they are, they are what we call inseparables.

178
00:28:58,000 --> 00:29:00,000
You may remember that one.

179
00:29:00,000 --> 00:29:02,000
In separable.

180
00:29:02,000 --> 00:29:07,000
Those material, material properties, which cannot be physically separated.

181
00:29:07,000 --> 00:29:10,000
There are always these eight.

182
00:29:10,000 --> 00:29:18,000
Even in the smallest particle of an atom, according to a bit of a, there are these eight material properties.

183
00:29:18,000 --> 00:29:29,000
The four great elements will be the other element, fire element, water element, fire element, air element.

184
00:29:29,000 --> 00:29:35,000
And then color, odor, flavor, and nutritious essence.

185
00:29:35,000 --> 00:29:46,000
So these eight are here called octets with nutrients, nutrients, nutritious essence as eight.

186
00:29:46,000 --> 00:29:49,000
It's not, it's not so long in environments.

187
00:29:49,000 --> 00:29:57,000
But if you translate it into English, it's, it becomes a long name octets with nutritious essence as eight.

188
00:29:57,000 --> 00:30:00,000
In Bali, it is Olja Tamaka.

189
00:30:00,000 --> 00:30:03,000
Five, five syllables.

190
00:30:03,000 --> 00:30:12,000
So in the case of the four namely, gourge, tongue, fuzz, and urine, which are originated by temperature, eight instances of materiality.

191
00:30:12,000 --> 00:30:19,000
Since they are caused by temperature only, there are only eight material properties.

192
00:30:19,000 --> 00:30:28,000
They complain in which in each with the octet with nutritious essence as eight, same in separables.

193
00:30:28,000 --> 00:30:33,000
And this in the first place is a method in the case of the that you do bodily aspects.

194
00:30:33,000 --> 00:30:39,000
But there are 10 more aspects that become clear when those that you do aspects have become clear.

195
00:30:39,000 --> 00:30:47,000
And as regards to these first being nine instances of materiality, that is, the octet with nutritious essence is eight plus life.

196
00:30:47,000 --> 00:30:53,000
They complain in the case of comma born part of heat that digest what is eaten, etc.

197
00:30:53,000 --> 00:31:01,000
That means for the, for the food of the four fire elements, there are nine.

198
00:31:01,000 --> 00:31:07,000
The, the insert nine inside eight inseparables and then one life principle.

199
00:31:07,000 --> 00:31:16,000
So there are nine there.

200
00:31:16,000 --> 00:31:19,000
Eight inseparables.

201
00:31:19,000 --> 00:31:25,000
Plus life principle is called, or we call it life principle, jiva.

202
00:31:25,000 --> 00:31:40,000
And likewise, nine instances of materiality that is, okay, octet with nutritious essence as eight plus sound.

203
00:31:40,000 --> 00:31:46,000
Yeah, sound, in the case of consciousness, born part of air consisting in breads and others.

204
00:31:46,000 --> 00:31:50,000
In breads and others, there are nine, nine material properties.

205
00:31:50,000 --> 00:31:53,000
Eight inseparables plus sound.

206
00:31:53,000 --> 00:31:57,000
And then 33 instances of materiality that is come up on life.

207
00:31:57,000 --> 00:32:01,000
And that, and the three octets with nutritious essence as eight.

208
00:32:01,000 --> 00:32:06,000
In the case of each of the remaining eight parts that are of full, full origination.

209
00:32:06,000 --> 00:32:12,000
They are the eight separables plus life principle.

210
00:32:12,000 --> 00:32:14,000
That is called life ended.

211
00:32:14,000 --> 00:32:18,000
And then three octets with nutritious essence as eight.

212
00:32:18,000 --> 00:32:22,000
That is the eight inseparables.

213
00:32:22,000 --> 00:32:33,000
So one life and three eight inseparables, nine plus 24 becomes 33.

214
00:32:33,000 --> 00:32:41,000
If you cannot count them, don't worry.

215
00:32:41,000 --> 00:32:49,000
In actual practice, you need to see all these, like mentioned here.

216
00:32:49,000 --> 00:32:51,000
It is very difficult.

217
00:32:51,000 --> 00:33:02,000
Even for those who have studied abitama, it is not so easy to see them clearly when they practice meditate.

218
00:33:02,000 --> 00:33:06,000
They may see just a few of them, not all of them.

219
00:33:06,000 --> 00:33:10,000
But here, everything is mentioned in detail.

220
00:33:10,000 --> 00:33:19,000
But it does not mean that when you practice with personal meditation, you must see all of all of these mentioned here.

221
00:33:19,000 --> 00:33:29,000
So what is important in real practice is to be able to see in your mind,

222
00:33:29,000 --> 00:33:32,000
clearly what you observe.

223
00:33:32,000 --> 00:33:42,000
Say, you are making notes of observing sometimes mental things and sometimes material things.

224
00:33:42,000 --> 00:33:49,000
For example, when you are concentrating on the breath, then you are concentrating on the matter,

225
00:33:49,000 --> 00:33:52,000
because breath is air and air is matter.

226
00:33:52,000 --> 00:33:57,000
When you are concentrating on your thoughts, then it is on the mind you are doing on.

227
00:33:57,000 --> 00:34:18,000
So you pre-attention to them, and then you try to see them clearly the breath is breath and your thoughts or your mental states as mental states.

228
00:34:18,000 --> 00:34:27,000
So when you can see them clearly without being mixed with any other thing,

229
00:34:27,000 --> 00:34:34,000
and then you are set to have got the knowledge of designing mind and matter.

230
00:34:34,000 --> 00:34:46,000
So you need not go through all these, taking the head hair and then trying to find out harmony,

231
00:34:46,000 --> 00:34:54,000
material properties are there in connection with their head hair and so on.

232
00:34:54,000 --> 00:35:07,000
But if you can, then it will be good for you to dwell on these and see them clearly.

233
00:35:07,000 --> 00:35:19,000
But as I said, the most important thing is first to see them clearly, and then to know that mind is the one that goes to the object.

234
00:35:19,000 --> 00:35:25,000
And the matter is one that does not go close.

235
00:35:25,000 --> 00:35:32,000
So if you see just that, and you are set to have that knowledge of mind and matter,

236
00:35:32,000 --> 00:35:38,000
a knowledge of designing, a knowledge of discriminating mind and matter.

237
00:35:38,000 --> 00:35:46,000
And this knowledge of designing mind and matter comes not at the beginning of your practice.

238
00:35:46,000 --> 00:35:54,000
Before you get to that stage, you have to get enough concentration.

239
00:35:54,000 --> 00:36:00,000
So in the beginning, what you are doing is trying to get concentration.

240
00:36:00,000 --> 00:36:10,000
So after you get concentration, after your mind can be on the object without being disturbed by mental hindrances,

241
00:36:10,000 --> 00:36:15,000
then you will begin to see things clearly.

242
00:36:15,000 --> 00:36:20,000
Now, sometimes people meditate, they think, oh, I can see clearly.

243
00:36:20,000 --> 00:36:23,000
I don't need too much concentration.

244
00:36:23,000 --> 00:36:27,000
The woman I sit and watch myself, I can see them clearly.

245
00:36:27,000 --> 00:36:35,000
But later on, when they come to see really clearly people, what I said, I saw clearly was nothing.

246
00:36:35,000 --> 00:36:37,000
Now, it is real.

247
00:36:37,000 --> 00:36:44,000
It is only now that I sit clearly, something like this.

248
00:36:44,000 --> 00:36:50,000
So if you cannot follow all these things, don't worry.

249
00:36:50,000 --> 00:37:00,000
So first, he got the material, material things in different ways.

250
00:37:00,000 --> 00:37:18,000
So after defining the ruba, defining the matter, he tries to define the mind or the mentality.

251
00:37:18,000 --> 00:37:22,000
So, for a graph, they describe that.

252
00:37:22,000 --> 00:37:29,000
Taking all these together under the characteristic of being molested, he sees them as materiality.

253
00:37:29,000 --> 00:37:37,000
When he had discerned materiality, thus, the immaterial states become plain to him in accordance with the sense doors,

254
00:37:37,000 --> 00:37:45,000
that is to see the 81 kinds of money, consciousness, and so on.

255
00:37:45,000 --> 00:37:50,000
And here, the different types of consciousness are mentioned.

256
00:37:50,000 --> 00:37:54,000
And if you remember, the chap does not consciousness,

257
00:37:54,000 --> 00:37:58,000
and you will easily understand this.

258
00:37:58,000 --> 00:38:02,000
And if you don't remember, don't want it.

259
00:38:02,000 --> 00:38:10,000
So, in practice, sometimes you see something and you are aware of that that's seeing consciousness.

260
00:38:10,000 --> 00:38:14,000
You hear something and you are aware of that hearing consciousness.

261
00:38:14,000 --> 00:38:19,000
So, when you are mindful of seeing consciousness, or hearing consciousness,

262
00:38:19,000 --> 00:38:23,000
then you are seeing mentality.

263
00:38:23,000 --> 00:38:32,000
And you also, you will not feel to see the consciousness, which just bends towards the object.

264
00:38:32,000 --> 00:38:38,000
That is why it is called Nama in Bali.

265
00:38:38,000 --> 00:38:46,000
So, when watching or when dealing upon types of consciousness,

266
00:38:46,000 --> 00:38:50,000
you do not dwell upon the super mundane types of consciousness,

267
00:38:50,000 --> 00:38:56,000
because super mundane types of consciousness and also manufacturer and others.

268
00:38:56,000 --> 00:39:01,000
Whatever a super mundane is not the domain of Vipasana.

269
00:39:01,000 --> 00:39:06,000
In Vipasana, you take only the mundane things as objects.

270
00:39:06,000 --> 00:39:14,000
Simply because you have not experienced yourself, the super mundane things.

271
00:39:14,000 --> 00:39:18,000
And what you have not experienced, you cannot see clearly.

272
00:39:18,000 --> 00:39:25,000
So, in Vipasana meditation, we are concerned only with what is mundane and not super mundane.

273
00:39:25,000 --> 00:39:32,000
So, the supermanning types of consciousness are excluded from the types of consciousness,

274
00:39:32,000 --> 00:39:40,000
which are the object of Vipasana meditation.

275
00:39:40,000 --> 00:39:49,000
So, all these types of consciousness and mental states, you define as mentality.

276
00:39:49,000 --> 00:39:56,000
So, you define materiality first and then you define mentality.

277
00:39:56,000 --> 00:40:09,000
This is by way of the full great elements with reference to actually 42 kinds of elements.

278
00:40:09,000 --> 00:40:18,000
And then next paragraph now, definition based on the 18 elements.

279
00:40:18,000 --> 00:40:26,000
Here also, you have to understand first the 18 elements, which are described in the previous chapters,

280
00:40:26,000 --> 00:40:32,000
an element of the eye, element of ear and so on.

281
00:40:32,000 --> 00:40:42,000
So, with regard to this also, the way of defining is, now when we see the eye element,

282
00:40:42,000 --> 00:40:51,000
we do not mean the eyeball, what we mean is the sensitivity sensitive part in the eye.

283
00:40:51,000 --> 00:40:58,000
So, the part of the eye where the image strikes.

284
00:40:58,000 --> 00:41:07,000
So, that sensitive part of the eye is what is called the eye here.

285
00:41:07,000 --> 00:41:14,000
So, the eyeball is not the, not the eye elements here.

286
00:41:14,000 --> 00:41:21,000
So, here is a, instead of taking the piece of flesh, very easy with white and black circles,

287
00:41:21,000 --> 00:41:26,000
having land and bread and fuss and the eye socket with this string of sinew,

288
00:41:26,000 --> 00:41:29,000
which the world terms and eye defines as eye element.

289
00:41:29,000 --> 00:41:35,000
The eye sensitivity of the kind described among the kinds of derived materiality in the description of the attributes,

290
00:41:35,000 --> 00:41:38,000
but in chapter 14.

291
00:41:38,000 --> 00:41:46,000
So, he defines that as eye element, not the whole eyeball for something in the eye,

292
00:41:46,000 --> 00:41:55,000
maybe someplace in the retina where the image strikes and through which we see things.

293
00:41:55,000 --> 00:42:00,000
And he does not define as eye element the remaining instances of materiality,

294
00:42:00,000 --> 00:42:03,000
which total 53 are so on.

295
00:42:03,000 --> 00:42:09,000
So, I will not button you with, with finding our words,

296
00:42:09,000 --> 00:42:14,000
53 or 43 or 45.

297
00:42:14,000 --> 00:42:18,000
And then the next is based on the 12th basis.

298
00:42:18,000 --> 00:42:21,000
There are 12th sense basis.

299
00:42:21,000 --> 00:42:25,000
If you understand the 18 elements, then 12th basis also you understand.

300
00:42:25,000 --> 00:42:29,000
It is another way of describing and the realities.

301
00:42:29,000 --> 00:42:33,000
When Buddha taught Buddha taught in different ways and sometimes he taught,

302
00:42:33,000 --> 00:42:42,000
and Nama and Rupa in 18 elements and sometimes in 12th basis,

303
00:42:42,000 --> 00:42:45,000
and sometimes 5th aggregates and so on.

304
00:42:45,000 --> 00:42:55,000
So, following these different ways of treating what we call the ultimate reality,

305
00:42:55,000 --> 00:42:58,000
we have different different methods here.

306
00:42:58,000 --> 00:43:04,000
So, we can use any of these methods to contemplate on the mind and matter.

307
00:43:04,000 --> 00:43:10,000
So, this paragraph itself is a definition based on the 12th basis.

308
00:43:10,000 --> 00:43:18,000
Here also, the eye base, ear base and so on.

309
00:43:18,000 --> 00:43:20,000
The eye base is the same as the eye element.

310
00:43:20,000 --> 00:43:28,000
So, the eye sensitivity only is the eye base and not the eye, not the eye ball and others.

311
00:43:28,000 --> 00:43:34,000
So, here he defines as eye base the sensitivity only,

312
00:43:34,000 --> 00:43:40,000
leaving of the 53 remaining instances of materiality in the middle of the eye element and so on.

313
00:43:40,000 --> 00:43:49,000
And then the next paragraph is definition based on the aggregates.

314
00:43:49,000 --> 00:43:59,000
Now, it is a little brief because you have to deal with only 5 aggregates instead of 12 or 18 and describe value.

315
00:43:59,000 --> 00:44:05,000
So, here another defines it more briefly than that by means of the aggregates.

316
00:44:05,000 --> 00:44:14,000
Now, you know the 5 aggregates.

317
00:44:14,000 --> 00:44:19,000
What is the first of the 5 aggregates?

318
00:44:19,000 --> 00:44:23,000
Root, root, right, yes, root, copper, yeah, that is root bar.

319
00:44:23,000 --> 00:44:30,000
So, that root bar is divided into 27 or 28 material properties,

320
00:44:30,000 --> 00:44:34,000
the full primary elements and then 24 depending on it.

321
00:44:34,000 --> 00:44:44,000
And they are chala, ura and so on.

322
00:44:44,000 --> 00:44:53,000
Now, there are 28 material properties.

323
00:44:53,000 --> 00:45:03,000
And then among them only the first 18 or here 17 are suitable for comprehension.

324
00:45:03,000 --> 00:45:06,000
That is suitable for meditating on.

325
00:45:06,000 --> 00:45:15,000
Although there are 28 material properties, the last 10 are not suitable for comprehension,

326
00:45:15,000 --> 00:45:18,000
not suitable for meditation.

327
00:45:18,000 --> 00:45:32,000
Simply because actually they are just the modes or some aspects of the 18 and the first.

328
00:45:32,000 --> 00:45:33,000
The first 18.

329
00:45:33,000 --> 00:45:37,000
So, the first 18 are the real material properties.

330
00:45:37,000 --> 00:45:40,000
That is why they are called root bar root bar.

331
00:45:40,000 --> 00:45:43,000
So, the real root bar.

332
00:45:43,000 --> 00:45:48,000
But in this book, only 17 am I right?

333
00:45:48,000 --> 00:45:53,000
17 instances of material that are consisting of the full primary and so on.

334
00:45:53,000 --> 00:45:58,000
That is because it leaves out the heart base.

335
00:45:58,000 --> 00:46:03,000
So, the others are not suitable for contemplation.

336
00:46:03,000 --> 00:46:11,000
And they are a bodily information for the information, the space element and the likeness,

337
00:46:11,000 --> 00:46:16,000
the malleability, wilderness, growth, continuity, aging and impermanence of materiality.

338
00:46:16,000 --> 00:46:20,000
So, they are mentioned in the chapter on five aggregates.

339
00:46:20,000 --> 00:46:29,000
So, if we take material properties as 27, then the first 17.

340
00:46:29,000 --> 00:46:53,000
Un suitable for comprehension and the last 10 are not.

