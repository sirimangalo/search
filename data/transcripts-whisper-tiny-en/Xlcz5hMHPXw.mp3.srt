1
00:00:00,000 --> 00:00:05,000
Why don't we remember our past lives?

2
00:00:05,000 --> 00:00:10,000
Well, let's give the simple answer first.

3
00:00:10,000 --> 00:00:15,000
The simple answer is because you don't have the power to remember things that far back.

4
00:00:15,000 --> 00:00:17,000
It's just that simple.

5
00:00:17,000 --> 00:00:21,000
But we have this idea while I was an adult and I should remember all those things,

6
00:00:21,000 --> 00:00:26,000
but this was before you were this, you know,

7
00:00:26,000 --> 00:00:31,000
feed us in your mother's womb, which you also don't remember.

8
00:00:31,000 --> 00:00:34,000
No, everyone can verify for themselves.

9
00:00:34,000 --> 00:00:36,000
They were in their mother's womb.

10
00:00:36,000 --> 00:00:46,000
It's not really, you know, there's ultrasounds and, you know,

11
00:00:46,000 --> 00:00:50,000
there's baby pictures of, you know, I have pictures of my birth.

12
00:00:50,000 --> 00:00:53,000
I was born in our house at home, not in a hospital,

13
00:00:53,000 --> 00:00:55,000
and we have pictures of it all.

14
00:00:55,000 --> 00:00:58,000
So I'm pretty clear that I came out of my mother's womb,

15
00:00:58,000 --> 00:01:00,000
but I don't remember it.

16
00:01:00,000 --> 00:01:04,000
No recollection whatsoever.

17
00:01:04,000 --> 00:01:07,000
So that's the short answer, is that it's just too far back.

18
00:01:07,000 --> 00:01:08,000
It's before all that.

19
00:01:08,000 --> 00:01:18,000
So the long answer is, or the long answer.

20
00:01:18,000 --> 00:01:22,000
Long answer is that you can remember your past lives.

21
00:01:22,000 --> 00:01:24,000
You just have to work at it.

22
00:01:24,000 --> 00:01:26,000
Just like with any kind of memory.

23
00:01:26,000 --> 00:01:29,000
Now we don't think of actually working at remembering things,

24
00:01:29,000 --> 00:01:31,000
because our minds don't work very well in general.

25
00:01:31,000 --> 00:01:33,000
When your mind is untrained,

26
00:01:33,000 --> 00:01:35,000
when you haven't tried to train your mind,

27
00:01:35,000 --> 00:01:38,000
you don't, you don't even think it possible.

28
00:01:38,000 --> 00:01:41,000
I think memory is something that comes by itself, right?

29
00:01:41,000 --> 00:01:43,000
Pops up.

30
00:01:43,000 --> 00:01:45,000
That times, but you can actually train your mind,

31
00:01:45,000 --> 00:01:48,000
and there are people who do it.

32
00:01:48,000 --> 00:01:51,000
We all heard of these people with photographic memories

33
00:01:51,000 --> 00:01:55,000
or people who memorize telephone books.

34
00:01:55,000 --> 00:01:59,000
They train themselves by memorizing telephone books,

35
00:01:59,000 --> 00:02:02,000
developing this memory.

36
00:02:02,000 --> 00:02:03,000
Monks do it as well.

37
00:02:03,000 --> 00:02:05,000
They memorize all of the Buddhist teaching.

38
00:02:05,000 --> 00:02:09,000
45 volumes or 47 volumes.

39
00:02:09,000 --> 00:02:12,000
And they brag about this,

40
00:02:12,000 --> 00:02:14,000
and we hear about these monks who memorize it all.

41
00:02:14,000 --> 00:02:16,000
And then I talked to one of them,

42
00:02:16,000 --> 00:02:17,000
or I talked to a monk about it,

43
00:02:17,000 --> 00:02:19,000
and he said, oh no, they never really remember it.

44
00:02:19,000 --> 00:02:21,000
Oh, that's just what they say.

45
00:02:21,000 --> 00:02:23,000
He said, you couldn't remember it all,

46
00:02:23,000 --> 00:02:26,000
but you get good.

47
00:02:26,000 --> 00:02:29,000
You know where everything is,

48
00:02:29,000 --> 00:02:33,000
because the point is just to be able to pass the exams.

49
00:02:33,000 --> 00:02:37,000
And the exam, the examiner can't ask you everything.

50
00:02:37,000 --> 00:02:40,000
So you just have to know what he's going to ask you,

51
00:02:40,000 --> 00:02:42,000
and you have to know where everything is,

52
00:02:42,000 --> 00:02:46,000
and basically get the gist of the entire diptica.

53
00:02:46,000 --> 00:02:49,000
Anyway, so they do this,

54
00:02:49,000 --> 00:02:55,000
and that's still quite an incredible accomplishment.

55
00:02:55,000 --> 00:02:59,000
So the mind can be trained in memory,

56
00:02:59,000 --> 00:03:03,000
but it can also be trained to remember things that you've forgotten.

57
00:03:03,000 --> 00:03:06,000
It can be trained to go backwards,

58
00:03:06,000 --> 00:03:10,000
but you have to work at it really, really hard.

59
00:03:10,000 --> 00:03:13,000
And the work is in your meditation.

60
00:03:13,000 --> 00:03:16,000
When you practice some at the meditation,

61
00:03:16,000 --> 00:03:18,000
you develop yourself as I was saying,

62
00:03:18,000 --> 00:03:20,000
switching objects,

63
00:03:20,000 --> 00:03:22,000
then switching genres,

64
00:03:22,000 --> 00:03:25,000
then switching objects in general.

65
00:03:25,000 --> 00:03:29,000
So you'd enter into the first five genres

66
00:03:29,000 --> 00:03:32,000
with the earth casino,

67
00:03:32,000 --> 00:03:35,000
and then you'd enter in the first five genres with the air casino,

68
00:03:35,000 --> 00:03:37,000
and then the water, the earth element,

69
00:03:37,000 --> 00:03:39,000
air element, water element,

70
00:03:39,000 --> 00:03:41,000
fire element,

71
00:03:41,000 --> 00:03:43,000
and then you'd switch.

72
00:03:43,000 --> 00:03:46,000
You'd go first-genre, third-genre, fifth-genre,

73
00:03:46,000 --> 00:03:47,000
or something like that.

74
00:03:47,000 --> 00:03:48,000
Then you'd switch objects,

75
00:03:48,000 --> 00:03:51,000
first-genre, earth element, first-genre, air element,

76
00:03:51,000 --> 00:03:54,000
first-genre, and you'd get,

77
00:03:54,000 --> 00:03:57,000
you know, you're actually playing this game,

78
00:03:57,000 --> 00:03:59,000
and it's scientific,

79
00:03:59,000 --> 00:04:01,000
the development, like check, check,

80
00:04:01,000 --> 00:04:03,000
and going back and forth.

81
00:04:03,000 --> 00:04:08,000
To the point that your mind is incredibly sharp

82
00:04:08,000 --> 00:04:11,000
and able to direct itself

83
00:04:11,000 --> 00:04:16,000
in any direction that it chooses.

84
00:04:16,000 --> 00:04:20,000
So you're able to develop these states of

85
00:04:20,000 --> 00:04:24,000
intense tranquility.

86
00:04:24,000 --> 00:04:28,000
And then what you do is you sit down,

87
00:04:28,000 --> 00:04:32,000
and you think of what you just did before you sat down.

88
00:04:32,000 --> 00:04:37,000
And then you think about what you just did before that.

89
00:04:37,000 --> 00:04:41,000
And then you think about what happened before that before that.

90
00:04:41,000 --> 00:04:43,000
And you go back like this.

91
00:04:43,000 --> 00:04:46,000
As soon as you get stuck,

92
00:04:46,000 --> 00:04:48,000
and can't remember anything,

93
00:04:48,000 --> 00:04:49,000
can't remember something,

94
00:04:49,000 --> 00:04:50,000
you stop,

95
00:04:50,000 --> 00:04:52,000
and you develop the genres again.

96
00:04:52,000 --> 00:04:53,000
You develop your meditation

97
00:04:53,000 --> 00:04:54,000
and bring it to the earth casino,

98
00:04:54,000 --> 00:04:57,000
the earth element, air element, fire element, water element.

99
00:04:57,000 --> 00:04:59,000
And there's these methods.

100
00:04:59,000 --> 00:05:00,000
It's in the,

101
00:05:00,000 --> 00:05:01,000
we see the manga,

102
00:05:01,000 --> 00:05:02,000
you can read about exactly.

103
00:05:02,000 --> 00:05:03,000
I don't remember exactly what they do,

104
00:05:03,000 --> 00:05:06,000
but it's something about going from one to the other

105
00:05:06,000 --> 00:05:07,000
and skipping,

106
00:05:07,000 --> 00:05:12,000
and then going backwards and forwards and so on.

107
00:05:12,000 --> 00:05:13,000
And you do that again,

108
00:05:13,000 --> 00:05:16,000
and then you come back and try and start all over again.

109
00:05:16,000 --> 00:05:17,000
Sit down.

110
00:05:17,000 --> 00:05:19,000
Remember the first thing you did before you sat down

111
00:05:19,000 --> 00:05:21,000
and the first thing before that.

112
00:05:21,000 --> 00:05:23,000
And with work,

113
00:05:23,000 --> 00:05:30,000
eventually you can go back days, months, years.

114
00:05:30,000 --> 00:05:32,000
Yeah, it's kind of like regressive.

115
00:05:32,000 --> 00:05:36,000
It's exactly, it's incredibly systematic.

116
00:05:36,000 --> 00:05:41,000
It's been systematized to the point where it's no longer just chance.

117
00:05:41,000 --> 00:05:42,000
And I know people do,

118
00:05:42,000 --> 00:05:45,000
I've heard of one man who took it a lazy route,

119
00:05:45,000 --> 00:05:47,000
and he was actually able to do this,

120
00:05:47,000 --> 00:05:51,000
and actually able to remember something that seemed like past life.

121
00:05:51,000 --> 00:05:53,000
But he did sort of just a hypnosis,

122
00:05:53,000 --> 00:05:57,000
where he didn't go one moment by one moment by one moment.

123
00:05:57,000 --> 00:05:59,000
But technically speaking,

124
00:05:59,000 --> 00:06:00,000
that's what you should do.

125
00:06:00,000 --> 00:06:02,000
If you really want to be clear about it,

126
00:06:02,000 --> 00:06:05,000
you have to be clear that it's going in sequence

127
00:06:05,000 --> 00:06:10,000
and remember back back back back back all the way to your birth.

128
00:06:10,000 --> 00:06:12,000
And then into the womb.

129
00:06:12,000 --> 00:06:16,000
To the point where you can remember what was going on in the womb,

130
00:06:16,000 --> 00:06:18,000
all the way back to the point of conception.

131
00:06:18,000 --> 00:06:20,000
Now at the point of conception,

132
00:06:20,000 --> 00:06:21,000
it's much more difficult.

133
00:06:21,000 --> 00:06:27,000
And this is why regressive hypnosis takes several sessions.

134
00:06:27,000 --> 00:06:32,000
But as the texts say, eventually,

135
00:06:32,000 --> 00:06:33,000
you can break through that.

136
00:06:33,000 --> 00:06:35,000
And once you've broken through to a past life,

137
00:06:35,000 --> 00:06:36,000
it gets easier.

138
00:06:36,000 --> 00:06:38,000
And then you can go back further and further

139
00:06:38,000 --> 00:06:40,000
and from one life to the next and so on.

140
00:06:40,000 --> 00:06:44,000
Because the familiarity is there.

141
00:06:44,000 --> 00:06:49,000
The technique has been learned

142
00:06:49,000 --> 00:06:53,000
and the mind becomes familiar with the technique.

143
00:06:53,000 --> 00:06:54,000
So there's the long answer.

144
00:06:54,000 --> 00:06:56,000
You can remember your past life.

145
00:06:56,000 --> 00:06:59,000
I mean, we don't remember them now as an easy answer

146
00:06:59,000 --> 00:07:01,000
because we can't remember that far back.

147
00:07:01,000 --> 00:07:04,000
But that's just because of the state of our minds

148
00:07:04,000 --> 00:07:08,000
because there's so much information that we're inundated with

149
00:07:08,000 --> 00:07:11,000
that we can barely remember what we did last week.

150
00:07:11,000 --> 00:07:14,000
I can barely remember what I did this morning.

151
00:07:14,000 --> 00:07:16,000
But if you think about it,

152
00:07:16,000 --> 00:07:19,000
you can develop it and you can bring it back.

153
00:07:19,000 --> 00:07:26,000
So.

