1
00:00:00,000 --> 00:00:10,960
It may be a relief to leave the foundness meditation behind and come to the recollection

2
00:00:10,960 --> 00:00:19,400
meditations because the six kinds of recollections, meditations mentioned and this chapter

3
00:00:19,400 --> 00:00:32,140
are something like devotional meditations because they are not like the other kinds of

4
00:00:32,140 --> 00:00:33,140
meditations.

5
00:00:33,140 --> 00:00:39,520
I mean, they can see the meditation or we pass on the meditation because you do a lot of

6
00:00:39,520 --> 00:00:46,360
reflection, you do a lot of thinking and you practice this kind of meditation.

7
00:00:46,360 --> 00:00:51,680
And although there are 10 recollections, they are divided into two chapters.

8
00:00:51,680 --> 00:01:03,840
So six are treated in this chapter and the remaining will be treated in the next chapter.

9
00:01:03,840 --> 00:01:06,920
So 10 recollections.

10
00:01:06,920 --> 00:01:15,240
Now, the barley word for recollection is a new study.

11
00:01:15,240 --> 00:01:24,040
Now, when you read, you saw the maga or other translation, sometimes you may find the language

12
00:01:24,040 --> 00:01:34,160
clear explaining and not much adding any new understanding to it because they are explaining

13
00:01:34,160 --> 00:01:35,160
the barley word.

14
00:01:35,160 --> 00:01:42,480
Now, here the barley word is annu and sati, you already know sati, sati is mindfulness

15
00:01:42,480 --> 00:01:52,120
of remembering and annu, annu is a prefix and so it has more than one meaning.

16
00:01:52,120 --> 00:02:01,600
And here, I think two meanings are given because it arises again and again, so that is

17
00:02:01,600 --> 00:02:10,680
the meaning of annu and you can be again and again or alternatively, the mindfulness that

18
00:02:10,680 --> 00:02:14,960
is proper now, annu can also mean proper.

19
00:02:14,960 --> 00:02:22,480
It is a proper mindfulness for a cleanse man who has gone full out of it since it august

20
00:02:22,480 --> 00:02:25,720
only in those instances where it should occur.

21
00:02:25,720 --> 00:02:37,200
So annu sati is defined as mindfulness which arises again and again or which is a proper

22
00:02:37,200 --> 00:02:42,880
mindfulness for those who have gone full, actually for any person.

23
00:02:42,880 --> 00:02:51,400
So a cleanse with a monk or a member of the santo, a cleanse man in this book, it would

24
00:02:51,400 --> 00:03:09,240
mean a monk, but the barley word kula poda means just anybody, just somebody who is that

25
00:03:09,240 --> 00:03:12,680
right, yes.

26
00:03:12,680 --> 00:03:22,320
So the first is the recollection of the Buddha, actually recollection of the attributes

27
00:03:22,320 --> 00:03:28,320
of the Buddha, of the qualities of the Buddha, not recollection of the Buddha himself actually,

28
00:03:28,320 --> 00:03:36,040
but it is, the word means literally means recollection of the Buddha.

29
00:03:36,040 --> 00:03:41,440
And this is a term for mindfulness with the enlightened one's special qualities, it is

30
00:03:41,440 --> 00:03:42,440
that.

31
00:03:42,440 --> 00:03:49,240
So when you practice this meditation, you concentrate or you reflect on the attributes

32
00:03:49,240 --> 00:03:52,920
of special qualities of the Buddha.

33
00:03:52,920 --> 00:03:58,480
So the object of this meditation, this reflection meditation is the attributes of the

34
00:03:58,480 --> 00:03:59,480
Buddha.

35
00:03:59,480 --> 00:04:02,160
So the next one is the recollection of the Dharma.

36
00:04:02,160 --> 00:04:12,080
Here also the recollection of the qualities of the Dharma, they will be treated in detail later.

37
00:04:12,080 --> 00:04:18,040
So this is a term for mindfulness with the special qualities of the laws being well proclaimed

38
00:04:18,040 --> 00:04:21,480
etcetera as its object.

39
00:04:21,480 --> 00:04:32,120
And here Dharma is translated as law and it will be explained in its proper place.

40
00:04:32,120 --> 00:04:43,720
That Dharma means the four parts, the four flusions, Nipana, and also the teachings

41
00:04:43,720 --> 00:04:44,720
of the scriptures.

42
00:04:44,720 --> 00:04:55,160
So there are 10 kinds of Dharma, four parts, four flusions, Nipana, and the teachings.

43
00:04:55,160 --> 00:05:06,440
One attributes have to do with all 10 and some, have to do with not all 10, maybe eight

44
00:05:06,440 --> 00:05:09,320
or four or whatever.

45
00:05:09,320 --> 00:05:11,560
So they will be explained later.

46
00:05:11,560 --> 00:05:14,400
The third is the recollection of the Sangha.

47
00:05:14,400 --> 00:05:27,360
And Sangha means a community of monks who follow Buddha's advice and who have attained

48
00:05:27,360 --> 00:05:30,240
to the enlightenment.

49
00:05:30,240 --> 00:05:35,880
So this is a term for mindfulness with community special qualities of being entered on the

50
00:05:35,880 --> 00:05:38,080
good way and so on.

51
00:05:38,080 --> 00:05:44,640
So when you practice this meditation, you concentrate on the qualities of the Sangha.

52
00:05:44,640 --> 00:05:48,680
The Sangha is well behaved, something like that.

53
00:05:48,680 --> 00:05:54,120
The next is recollection of virtue, recollection of one's moral contact, one's pure

54
00:05:54,120 --> 00:05:55,920
moral contact.

55
00:05:55,920 --> 00:06:02,840
So that's why it is a mindfulness with special qualities of virtues and toneness etcetera

56
00:06:02,840 --> 00:06:09,400
and toned and broken and blotched, something like that.

57
00:06:09,400 --> 00:06:18,080
And then next one is recollection of generosity and it is reflecting on one's own generosity

58
00:06:18,080 --> 00:06:22,480
and getting joy or happiness.

59
00:06:22,480 --> 00:06:32,120
Now with generosity, special qualities of free generosity, what is free generosity?

60
00:06:32,120 --> 00:06:44,480
Free generosity means giving without any expectations, without expecting anything

61
00:06:44,480 --> 00:06:45,480
in return.

62
00:06:45,480 --> 00:06:49,560
That is called free generosity in Bali.

63
00:06:49,560 --> 00:06:57,760
The Bali what is Murtasa, Changa and Chaga means giving and Murta means free, something

64
00:06:57,760 --> 00:06:58,760
like that.

65
00:06:58,760 --> 00:07:01,520
So that is, when you give, you just give.

66
00:07:01,520 --> 00:07:07,120
You don't expect any resource or anything from this act of killing.

67
00:07:07,120 --> 00:07:11,160
And how is it different from just generosity?

68
00:07:11,160 --> 00:07:17,200
I don't know what generosity means in English, is it giving, but you may you may expect

69
00:07:17,200 --> 00:07:27,160
some results, some good results from this killing, a good name or some something in the

70
00:07:27,160 --> 00:07:28,160
future.

71
00:07:28,160 --> 00:07:35,080
But yeah, you just give, because it is a good thing to do and you don't do not expect

72
00:07:35,080 --> 00:07:39,320
anything from this killing.

73
00:07:39,320 --> 00:07:50,680
Next one is recollection of deities and that means, recollection of the qualities of the

74
00:07:50,680 --> 00:07:51,680
deities.

75
00:07:51,680 --> 00:07:58,600
Before they were born as deities, they were human beings and they have the qualities of

76
00:07:58,600 --> 00:08:02,160
morality, qualities of generosity and so on.

77
00:08:02,160 --> 00:08:07,520
And these qualities are in me and so I am as good as they are, something like that.

78
00:08:07,520 --> 00:08:19,200
So comparing, taking deities as witness, one reflects on one's own good qualities and not

79
00:08:19,200 --> 00:08:27,840
to get, to get, to be proud of oneself, but just to rejoice in one's being, generous

80
00:08:27,840 --> 00:08:34,280
one's being of pure moral contact and so on.

81
00:08:34,280 --> 00:08:42,480
And the next one is recollection of deities, is it mindfulness with the termination of

82
00:08:42,480 --> 00:08:48,520
the life faculty as its object, it just, it simply means death, say, termination of the

83
00:08:48,520 --> 00:08:49,520
life faculty.

84
00:08:49,520 --> 00:09:02,080
It's a technical, technical speaking, we use the life faculty, it really means just death.

85
00:09:02,080 --> 00:09:04,120
Mindfulness occupied with the body.

86
00:09:04,120 --> 00:09:12,600
Now here, you have to understand the value words, value word is ka-ya-ga-ga-ta-sati, actually

87
00:09:12,600 --> 00:09:18,680
they are three words, but three words are joined together, compounded together.

88
00:09:18,680 --> 00:09:21,880
So they become one word.

89
00:09:21,880 --> 00:09:32,960
Now among these three words, the word ka-ga-ta, ka-ta means con, or ka-ta means just to be,

90
00:09:32,960 --> 00:09:35,080
to be somewhere.

91
00:09:35,080 --> 00:09:44,120
So in Pali, there is a difference between saying gone to the material body, or it is

92
00:09:44,120 --> 00:09:47,480
on the material body.

93
00:09:47,480 --> 00:09:50,320
So it is explained in two ways.

94
00:09:50,320 --> 00:09:56,800
It is gone to the material body that is analyzing the head hairs, etc.

95
00:09:56,800 --> 00:10:04,400
For it is gone into the body, I don't like this, I just want to say it is on the body,

96
00:10:04,400 --> 00:10:12,360
that means your mind is on the different parts of the body when you practice this meditation.

97
00:10:12,360 --> 00:10:21,360
The Pali word ka-ta, matimologically ka-ta means, say, gone to something, but it is another

98
00:10:21,360 --> 00:10:23,640
meaning that is to be.

99
00:10:23,640 --> 00:10:31,640
So the mindfulness gone to the parts of the body, or mindfulness which is on the parts

100
00:10:31,640 --> 00:10:35,720
of the body, they make the same thing.

101
00:10:35,720 --> 00:10:47,200
So that ka-ta is the same as in ta-ta, right, yes, so it could be well gone or well here, gone

102
00:10:47,200 --> 00:10:58,160
to, gone to, the word is ka-ya-ga-ta-ta-ta-ta, which is gone to ka-ya, I mean the body, or

103
00:10:58,160 --> 00:11:05,600
the d, which is on the ka-ya, on the body, but in terms of ka-ga-ta-ta, that ta-ga-ta means

104
00:11:05,600 --> 00:11:14,760
ta-ta means tas, in that way, and ka-ta means ka-ma, or b, so it could be the tas-ga-ga-ta

105
00:11:14,760 --> 00:11:21,760
tas-ga-na, but it could also mean the sir, or the tas-ga-na, yes, and you know, with

106
00:11:21,760 --> 00:11:28,120
these words, the common data is very fanciful, and they give many meanings to one word

107
00:11:28,120 --> 00:11:34,040
you will find this today, and sometimes you will say, the common data is not sure which

108
00:11:34,040 --> 00:11:41,760
is the correct meaning of the word, because it gives 5, 6, 7, 8 meanings to only one word,

109
00:11:41,760 --> 00:11:49,880
and this is, this is the ah, what we call, the ability to divide words into different parts

110
00:11:49,880 --> 00:12:01,520
and then explain them, they may not be actually natural, but still, they are, they are

111
00:12:01,520 --> 00:12:10,960
fond of doing this, especially when it comes to defining or explaining the names of the

112
00:12:10,960 --> 00:12:22,280
Buddha, I mean, and the words of the words which describe the Buddha. So ka-ga-ga-ta-ta-ta,

113
00:12:22,280 --> 00:12:30,600
now, a little further down, but instead of shortening the vowel, thus in the usual way,

114
00:12:30,600 --> 00:12:38,080
usual way, body-gone mindfulness, ka-ga-ta-ta-ta-ta-ta-ta, that means, according to parigrama, the

115
00:12:38,080 --> 00:12:45,760
vowel in the middle of the compound can be shortened, so if it is shortened, if it will

116
00:12:45,760 --> 00:12:54,040
be ka-ga-ta-ta-ta-ta-ta, and not ka-ta, so if he is explained, the common data is explaining

117
00:12:54,040 --> 00:13:03,680
this, here, the long vowel is not shortened, so it is ka-ga-ta-ta-ta-ta, and not ka-ga-ta-ta-ta-ta-ta-ta-ta-ta.

118
00:13:03,680 --> 00:13:17,640
The next one is mindfulness of breathing, that is play. And then the last one is, your collection

119
00:13:17,640 --> 00:13:25,560
is risen inspired by peace, is the recollection of peace. It really means, a recollection

120
00:13:25,560 --> 00:13:35,560
on the attributes of Nipana, not taking Nipana as direct object, but dwelling on its attributes,

121
00:13:35,560 --> 00:13:42,160
filling on the attributes of Nipana, qualities of Nipana. So, these are the six recollections,

122
00:13:42,160 --> 00:13:52,120
which will be treated in this chapter. So, the first one, now, before going to the first

123
00:13:52,120 --> 00:14:04,040
one, we have to understand the formula or the words descriptive of the Buddha. Just recently,

124
00:14:04,040 --> 00:14:15,920
Michael showed me the load of sutra. Now, actually, Mahayana sutra's fashions are on the

125
00:14:15,920 --> 00:14:23,400
polysutra. So, there are many similarities in describing, in the style of the language,

126
00:14:23,400 --> 00:14:30,360
although Mahayana Buddhism used, and still used a Sanskrit language, because Sanskrit and

127
00:14:30,360 --> 00:14:41,920
Bali are very close to each other. So, this set of the names or attributes of the Buddha

128
00:14:41,920 --> 00:14:52,440
are found both in Thiravada and Mahaya. And it's a very, this set of attributes, it's very

129
00:14:52,440 --> 00:15:00,720
popular with Thiravada Buddhism. I hope it is also with Mahayana Buddhists, because on

130
00:15:00,720 --> 00:15:11,720
a devotional side, we want to concentrate on the Buddha, and dwell on his attributes, it's

131
00:15:11,720 --> 00:15:24,960
like Christians afraid to God, or thinking of God, something like that. And this set of

132
00:15:24,960 --> 00:15:34,320
attributes is well known to almost everybody's. The Bali is E.T.P. Soba, Gawa, and so on.

133
00:15:34,320 --> 00:15:41,800
So, it is given here in the paragraph two, the second, second paragraph. That blessed one

134
00:15:41,800 --> 00:15:49,320
is such, since he is accomplished and so on. But the translation should not run like this.

135
00:15:49,320 --> 00:15:59,440
The blessed one is accomplished for such and such a reason. It should go like this. There

136
00:15:59,440 --> 00:16:06,080
are nine or ten attributes of the Buddha given here. And Buddha is accomplished for

137
00:16:06,080 --> 00:16:11,360
such and such and such and such and such and such a reason. Buddha is fully enlightened

138
00:16:11,360 --> 00:16:18,960
for such and such a reason and so on. And the reasons or the reasons will be explained

139
00:16:18,960 --> 00:16:31,520
in detail. And when, although for such and such a reason is not mentioned again and again

140
00:16:31,520 --> 00:16:38,440
in the formula, when you practice this meditation, you have to say this. Because sometimes

141
00:16:38,440 --> 00:16:45,720
you do not concentrate on all the ten or nine attributes. You pick up only one which

142
00:16:45,720 --> 00:16:50,560
you like best and then you concentrate on that one attribute and say it again and again

143
00:16:50,560 --> 00:16:57,320
and again in your mind. So, Buddha is accomplished for such and such a reason.

144
00:16:57,320 --> 00:17:00,640
Buddha is fully enlightened for such and such a reason.

145
00:17:00,640 --> 00:17:09,040
Buddha is endowed with virtue and conduct for such and such a reason and so on. And such

146
00:17:09,040 --> 00:17:18,440
and such a reason, the reason will be given in the exposition. So, in the paragraph

147
00:17:18,440 --> 00:17:28,880
three also, that blessed one is accomplished for such and such a reason. He is fully

148
00:17:28,880 --> 00:17:36,000
enlightened for such and such a reason and then does. He is blessed for such and such

149
00:17:36,000 --> 00:17:51,400
a reason. So, it should go like this. Such, such is accomplished.

150
00:17:51,400 --> 00:18:04,320
Yeah the what appears twice. The first occurrence of the blessed one refers to the Buddha

151
00:18:04,320 --> 00:18:12,160
and the second occurrence refers to his attribute, his quality. So, the blessed one is

152
00:18:12,160 --> 00:18:17,560
blessed because he is so and so, something like that.

153
00:18:17,560 --> 00:18:23,200
Now the first one, the first one you have to understand the Pali word. Now, Pali word

154
00:18:23,200 --> 00:18:34,720
is Araha. Araha. Now, this word is divided differently and then explained for the meaning

155
00:18:34,720 --> 00:18:39,600
of this word is given in five ways. Five meanings are given to this one word.

156
00:18:39,600 --> 00:18:51,760
So, the cutting up of the word is, the first one is Araha.

157
00:18:51,760 --> 00:19:03,240
And the full word is Araha, but we have to divide it into Araha and ha that is for the

158
00:19:03,240 --> 00:19:14,000
first meaning. Now, Araha means, oh I am sorry and the first is just Araha. So, for the

159
00:19:14,000 --> 00:19:21,760
first meaning, the word is Araha. And we do not have to look for a demology of this word

160
00:19:21,760 --> 00:19:30,760
in for this meaning. The word and this word means just far away or removed. So, here

161
00:19:30,760 --> 00:19:38,120
according to this meaning, Buddha is called Araha because he is far away from mental

162
00:19:38,120 --> 00:19:45,760
defilements. So, he stands utterly remote and far away from all defilements because he has

163
00:19:45,760 --> 00:19:53,600
expunged all trace of defilements. That means expunged all defilements without any trace.

164
00:19:53,600 --> 00:20:08,080
Then it is called Araha. Yeah, there is no. That means, originally it was

165
00:20:08,080 --> 00:20:19,520
Araha, but it was changed to Araha. It is a way of explaining, sometimes people,

166
00:20:19,520 --> 00:20:26,720
people pronounce words as they lie, not according to a demology or something. So, the

167
00:20:26,720 --> 00:20:30,360
pronunciation will change and so it becomes Araha.

168
00:20:30,360 --> 00:20:38,800
Sometimes, the word Araha, as the same meaning as the word Araha, Araha means to be far

169
00:20:38,800 --> 00:20:47,040
away. It is very confusing because it can also mean to be close to, but it is not given

170
00:20:47,040 --> 00:20:55,320
here fortunately. But in the sub commentary, they are given. So, the first one is to be

171
00:20:55,320 --> 00:21:02,600
away from, to be removed. That means, Buddha is removed from the mental defilements. And

172
00:21:02,600 --> 00:21:08,560
here, all trace of mental defilements, all trace is important. That means, when Buddha

173
00:21:08,560 --> 00:21:21,360
is a mental defilements, they abandon with all trace. So, there is not even a trace of

174
00:21:21,360 --> 00:21:28,080
mental defilements in the minds of the Buddha, but Arahas are different. Arahas also

175
00:21:28,080 --> 00:21:36,760
eradicate mental defilements, but they do not or they cannot eradicate all traces of

176
00:21:36,760 --> 00:21:42,260
mental defilements. Although, they are not mental defilements, they are something, some

177
00:21:42,260 --> 00:21:52,760
results of mental defilements, like a bad habit. You know, there was an Arahas who

178
00:21:52,760 --> 00:22:01,160
addressed any people, anybody he met, we could, we could person or something like that.

179
00:22:01,160 --> 00:22:07,920
So, he was born as a hiker's brahman for 500 consecutive lives. And so, he was used

180
00:22:07,920 --> 00:22:14,960
to that form of a dress. So, whenever he meets a person, he uses, hey, we could person,

181
00:22:14,960 --> 00:22:25,840
we are going or something. He was an Araha and he had no mental defilements, no dosa, no

182
00:22:25,840 --> 00:22:32,960
ill will, but still, he could not give up this habit. But, Buddha is a different.

183
00:22:32,960 --> 00:22:39,960
Then, Buddha's eradicate mental defilements, they eradicate, eradicate with all traces.

184
00:22:39,960 --> 00:22:51,880
So, not the singles or not the little trees remains with the Buddha. So, all trees of

185
00:22:51,880 --> 00:22:57,740
mental defilements, this is important. And that is what makes Buddha's different from

186
00:22:57,740 --> 00:23:05,360
Arahas, because they, both of them, eradicate all mental defilements, but the Buddha's

187
00:23:05,360 --> 00:23:13,760
eradicate with all traces, but Arahas means they are enhanced to not. So, they have some

188
00:23:13,760 --> 00:23:19,360
kind of, they trace, trace of mental defilements from meaning with them.

189
00:23:19,360 --> 00:23:29,840
Now, the second meaning is, according to the second meaning, the vision of the world is,

190
00:23:29,840 --> 00:23:40,800
what? Arahman has had that means just try to kill. So, Arahman also means that. So,

191
00:23:40,800 --> 00:23:53,600
Arahman's enemies and her means kill. So, one who has killed the enemies is called Araha.

192
00:23:53,600 --> 00:23:59,360
It could be called Araha, but the E is changed to I, and so it becomes Araha. That is

193
00:23:59,360 --> 00:24:08,920
how the formation of what is explained. And here, Arahman's work, the enemies, again,

194
00:24:08,920 --> 00:24:15,200
mental defilements. So, one who has eradicated mental defilements is called an Arahas.

195
00:24:15,200 --> 00:24:27,880
Now, the third meaning is, Araha, and ha. Araha means spokes, and ha means to destroy.

196
00:24:27,880 --> 00:24:36,440
So, one who has destroyed the spokes is called an Araha. Now, this wheel of the

197
00:24:36,440 --> 00:24:42,960
run of reburts with its hub, made of ignorance and craving for becoming. With its spokes

198
00:24:42,960 --> 00:24:48,400
consisting of formations of merit and the rest, with its rim of aging and death, which

199
00:24:48,400 --> 00:24:54,200
is joined to the chariot of the triple becoming by piercing it with the axle, made of

200
00:24:54,200 --> 00:25:01,960
the origin of kangas. Actually, here, the origin of kangas really means kangas, which are origins.

201
00:25:01,960 --> 00:25:08,360
They are not necessarily origin of kangas. The kangas themselves are origin. They are origin

202
00:25:08,360 --> 00:25:17,920
for some other mental defilements, has been revolving throughout time that has no beginning.

203
00:25:17,920 --> 00:25:24,240
All these wheel spokes were destroyed by him at the place of enlightenment. That is another

204
00:25:24,240 --> 00:25:29,840
body tree, as he stood firm with the feet of energy on the ground of virtue, wielding

205
00:25:29,840 --> 00:25:36,080
with the hand of faith, the acts of knowledge that destroys kamma. Do you mean this?

206
00:25:36,080 --> 00:25:41,280
I think this is pretty pretty. Because the spokes are just destroyed, he is a kamplish

207
00:25:41,280 --> 00:25:47,400
also. Now, in order to understand this passage, you have to look at the chariot, because

208
00:25:47,400 --> 00:25:59,120
it refers to the dependent origin nation. So, the second... Now, it is said here that, made

209
00:25:59,120 --> 00:26:07,200
of ignorance and of craving, it is a hub. Now, the whole... But it is just a mover is viewed

210
00:26:07,200 --> 00:26:16,920
as a wheel. So, in this wheel, the hub is ignorance and craving. Number one, ignorance

211
00:26:16,920 --> 00:26:26,600
and number eight craving. So, they are compared to the hub of the wheel. With its spokes,

212
00:26:26,600 --> 00:26:31,200
consisting of formations of merit and the rest. That means the number two, three, four,

213
00:26:31,200 --> 00:26:42,760
five, and so on. They are compared to spokes. If it was someone who could draw, I want

214
00:26:42,760 --> 00:26:54,240
him to draw a picture of this wheel and the carriage. Then, with its rim of aging and

215
00:26:54,240 --> 00:27:00,880
death. So, the rim is... The aging and death is compared to the rim, because they become

216
00:27:00,880 --> 00:27:09,960
last year. Aging and death means number twelve. We just joined the chariot of the triple

217
00:27:09,960 --> 00:27:19,480
becoming means triple existence. The sensual... Since we are existence, forms fear existence

218
00:27:19,480 --> 00:27:31,200
and formless fear existence. We are seeing it with the axle made of the origin of kangas.

219
00:27:31,200 --> 00:27:43,480
So, the kangas are compared to the axle. So, we have the spokes, the rim, and the axle.

220
00:27:43,480 --> 00:27:55,120
And above the wheel, there is the body of the chariot of carriage. So, when the spokes

221
00:27:55,120 --> 00:28:06,600
are destroyed and the whole thing is destroyed. So, Buddha was called a rahah. One who

222
00:28:06,600 --> 00:28:13,880
just tries the spokes, because he just tries the spokes of this wheel of existence. But it

223
00:28:13,880 --> 00:28:20,800
I mean, the dependent origin nation. Alternatively, it is the beginning of this round

224
00:28:20,800 --> 00:28:27,560
of reverse. That is called the wheel of round of rebut and so on. And the explanation here

225
00:28:27,560 --> 00:28:36,440
also has to do with the dependent origin nation. So, in the dependent origin nation, there

226
00:28:36,440 --> 00:28:44,120
are twelve. We call them links or the twelve factors. So, number one is the condition for

227
00:28:44,120 --> 00:28:50,240
number two and number two is condition for number three and so on. I say condition not

228
00:28:50,240 --> 00:28:55,640
the cause, because sometimes they are cause, sometimes they are not. But they are all

229
00:28:55,640 --> 00:29:00,280
conditions. So, one is condition for two, two is condition for three, three, four, and

230
00:29:00,280 --> 00:29:10,280
so on. So, if you have this in mind, then you can understand the following paragraphs.

231
00:29:10,280 --> 00:29:26,720
Or if you do not understand, you have to go to a bit of mind and read about dependent

232
00:29:26,720 --> 00:29:42,600
origin nation. Now, paragraph 16 explains how greening is condition for, say, comma becoming

233
00:29:42,600 --> 00:29:51,520
process and rebut process. I think that there is interesting. I shall enjoy sense desire.

234
00:29:51,520 --> 00:29:56,280
And with, with sense desire, cleaning as condition, he has miscontact himself in body speech

235
00:29:56,280 --> 00:30:06,200
and mind. Now, cleaning is a condition for good or bad comma, because I want to enjoy

236
00:30:06,200 --> 00:30:13,760
sense desires. Let us say, in a better existence, I want to be reborn in a better existence.

237
00:30:13,760 --> 00:30:21,200
I want to be reborn as a celestial being. So, I will do something so that I will be

238
00:30:21,200 --> 00:30:32,400
reborn there. Sometimes I happen to do what is wrong in body speech or mind. Sometimes

239
00:30:32,400 --> 00:30:40,240
we meet with, say, bad teachers and they may tell us that if you sacrifice a human being,

240
00:30:40,240 --> 00:30:51,120
then you will be reborn as if they were something like that. So, because of that cleaning,

241
00:30:51,120 --> 00:30:59,600
we do good or bad deeds. Here, in the first place, we do misconduct himself in body speech

242
00:30:59,600 --> 00:31:05,480
and mind. Going to the full film and of his misconduct, he appears in the state of loss.

243
00:31:05,480 --> 00:31:12,120
That means he, he is reborn in the full woeful states as an animal or in hell or as a

244
00:31:12,120 --> 00:31:18,240
ghost and so on. The comma, that is the cause of his reappearance there is comma process

245
00:31:18,240 --> 00:31:31,960
becoming. So, that belongs to here number 10 in the, in the links. The aggregates generated

246
00:31:31,960 --> 00:31:39,000
by the comma are rebut process becoming. That also is number 10. There are two kinds of

247
00:31:39,000 --> 00:31:46,280
number 10 factors. The one is action and the other is becoming. So, action really means

248
00:31:46,280 --> 00:31:53,160
quote or bad comma and becoming means the result of quote or bad comma. And actually,

249
00:31:53,160 --> 00:32:05,240
becoming in number 10 and number 11 are the same. The generation of the aggregate is birth.

250
00:32:05,240 --> 00:32:11,480
That means coming into being of the aggregates at the, at the moment of conception or at the

251
00:32:11,480 --> 00:32:17,160
moment of rebut and that is birth and they are maturing is aging and their dissolution is death.

252
00:32:17,160 --> 00:32:28,600
So, there is clinging and clinging is a condition for action or becoming and the becoming is

253
00:32:28,600 --> 00:32:38,120
conditioned for birth and decay and death. Then, I shall enjoy the delights of heaven

254
00:32:39,480 --> 00:32:45,720
and in the parallel manner, he contacts himself. Well, here, he did the right thing and so owing

255
00:32:45,720 --> 00:32:52,360
to the first woman of his good conduct, he reappears in a sensual sphere heaven.

256
00:32:53,160 --> 00:32:58,040
And here also, the comma that is the cause of his reappearance there is comma process becoming

257
00:32:58,040 --> 00:33:05,000
and the rest as before. That means the aggregates generated by the comma, but rebut process becoming

258
00:33:05,000 --> 00:33:10,920
and the generation of the aggregates is birth. Their maturing is aging, their dissolution is death.

259
00:33:10,920 --> 00:33:17,400
We have to understand that way. Sometimes, you want to be reborn as a Brahma, the highest unless

260
00:33:17,400 --> 00:33:22,440
you are being and then you do something and owing to the fulfillment of the development,

261
00:33:22,440 --> 00:33:31,720
he is reborn in one of these states and so on. Now, paragraph 20, understanding of

262
00:33:31,720 --> 00:33:38,760
discernment of conditions thus, ignorance is cause. Formations are causally arisen and both

263
00:33:38,760 --> 00:33:46,680
these states are causally arisen. Ignorance is a cause, but it is also dependent upon a condition.

264
00:33:46,680 --> 00:33:55,560
So, ignorance as well as formations is causally arisen, both are causally arisen. This is a knowledge

265
00:33:55,560 --> 00:34:03,400
of the causal relationship of states. Understanding of discernment of conditions thus in the first,

266
00:34:03,400 --> 00:34:12,760
so in the in the future, also ignorance is a cause and so on. Here in ignorance and formations

267
00:34:12,760 --> 00:34:23,080
are one summarization. In Burmese, we translated as a layer, two layers, three layers and so on.

268
00:34:24,040 --> 00:34:29,720
The Pali wad is some queba. Sankibar really means short of summarization,

269
00:34:29,720 --> 00:34:35,800
but in Burmese, we translated as a layer. So, there are four layers.

270
00:34:38,440 --> 00:34:45,000
Ignorance and formations are one summarization. So, in this chart, number one and number two,

271
00:34:48,440 --> 00:34:54,600
consciousness, mentality, materiality to six full base, contact and feeling are another. That means

272
00:34:54,600 --> 00:35:22,680
three, four, five, six, seven. They are one layer or one summarization.

273
00:35:22,680 --> 00:35:31,000
The first summarization is past. So, one and two belong to past time. The two middle ones are

274
00:35:31,000 --> 00:35:36,840
present. That means three, four, five, six, seven and then eight, nine. These two are present.

275
00:35:36,840 --> 00:35:43,480
But an aging of death are future. So, that is what we see. The dependent origin is in covers three

276
00:35:43,480 --> 00:35:53,960
lives, past, present and future lives. Well, if not as and formations are mentioned, then also

277
00:35:53,960 --> 00:35:59,640
craving clinging and becoming included too. So, those five states are the round of comma in the past.

278
00:35:59,640 --> 00:36:07,240
Now, in the past, there are only two factors, two links that is one and two. But when you say,

279
00:36:07,240 --> 00:36:15,800
when you mention one, you virtually mention, you look at this chart. When you mention one,

280
00:36:17,800 --> 00:36:25,480
you virtually mention eight and nine also, because they belong to the same here,

281
00:36:25,480 --> 00:36:37,640
present kilitia, round of kilitia. So, in reality, we say, when we say one and two, we mean one, two,

282
00:36:42,760 --> 00:36:51,240
when we say one, we mean one, eight, nine. And when we say two, we mean two, ten. So, when we

283
00:36:51,240 --> 00:37:01,720
say one and two, we mean one, two, eight, nine, ten. That is why there are five. In the first layer,

284
00:37:01,720 --> 00:37:06,280
in the first summarization, actually there are five, although two are mentioned.

285
00:37:09,320 --> 00:37:17,800
And the second, it's okay. And the third, when we say craving eight and nine, then we also mean

286
00:37:17,800 --> 00:37:25,560
number one. It is their vice versa. And when we say ten, we also mean two. So, when we say eight,

287
00:37:25,560 --> 00:37:36,600
nine, ten, we virtually mean one, two, eight, nine, ten. So, this way we get 20 conditions.

288
00:37:42,440 --> 00:37:46,680
And because the five beginning with consciousness are described under the heading of

289
00:37:46,680 --> 00:37:51,160
birth and aging and death, these five states are the round of comma result in the future.

290
00:37:52,040 --> 00:37:55,880
They make 20 aspects in this way. They are called 20 aspects.

291
00:38:00,920 --> 00:38:06,280
And here, there is one link between formations and consciousness. So, there are three links all together.

292
00:38:07,560 --> 00:38:14,440
First link is between two and three. Second link is between seven and eight. And third

293
00:38:14,440 --> 00:38:27,320
link is between ten and eleven. So, the Buddha knows all these. So, he is called Arahak.

294
00:38:27,320 --> 00:38:42,520
And then in paragraph 22, one, two, three, four, fifth line, three from the fourth. Knowledge is

295
00:38:42,520 --> 00:38:53,080
in the sense of that we know, actually it should be in the sense of that which knows, not

296
00:38:53,080 --> 00:38:59,960
being known. It looks like being known entirely, but the sub commentary explains is to me

297
00:39:00,920 --> 00:39:10,760
that which knows, not that which is known. So, being known should be changed to which knows.

298
00:39:11,400 --> 00:39:14,680
And understanding is in the sense of the act of understanding and so on.

299
00:39:14,680 --> 00:39:24,040
So, in order to fully understand it, you should have a knowledge of the defendant or

300
00:39:24,040 --> 00:39:38,520
recognition. And the next one is Arahak. Arahak means wordy. So, Buddha is wordy of accepting,

301
00:39:40,200 --> 00:39:45,240
accepting offerings. And so, he is called an Arahak.

302
00:39:47,960 --> 00:39:52,600
He recognizes of robes, etc. and of the distinction of being accorded home.

303
00:39:52,600 --> 00:39:58,120
It means actually it means just special honor. So, Buddha is wordy of the

304
00:39:58,120 --> 00:40:03,960
records of robes and so on. And also, he is wordy to accept special honor,

305
00:40:04,680 --> 00:40:14,200
offered to him by other beings. And then this paragraph explains that he was honored when he was

306
00:40:14,200 --> 00:40:21,960
alive. And even when he was dead, people continue to honor him by spending a lot of money and

307
00:40:21,960 --> 00:40:31,880
building monasteries and so on. So, here in late in the paragraph, and after the blessed one had

308
00:40:31,880 --> 00:40:39,320
finally attained Nibana, King Ahsoka renounced wealth to the amount of 96 million for his sake and

309
00:40:39,320 --> 00:40:50,520
founded 84,000 Monopoly. That means the renouncing of 96 million and founding of 84,000 monasteries

310
00:40:50,520 --> 00:41:01,400
are the same. That means he spends this much money to build 84,000 monasteries in India.

311
00:41:04,440 --> 00:41:11,400
And that's just to dedicate to the memory of the Buddha.

312
00:41:11,400 --> 00:41:23,560
And in paragraph 24, what is made by once they are cleverness? You see the words?

313
00:41:24,600 --> 00:41:36,280
In the world, who wants their cleverness? What's that? The Pali word means who thinks that they are wise

314
00:41:36,280 --> 00:41:47,800
although they are not really. Do these words come with the same meaning? So, people who thought

315
00:41:47,800 --> 00:41:55,240
they are clever, yet they do evil in secret, but Buddha is not like that. So, Buddha has no secret,

316
00:41:55,240 --> 00:42:05,880
therefore, Buddha is called here A plus Raha, the division of what is A plus Raha, means no

317
00:42:06,600 --> 00:42:12,920
and Raha means secret. So, Buddha has no secret, Buddha did not do anything secret, any

318
00:42:12,920 --> 00:42:30,040
wrong doing and secretly. So, this is called A Raha. So, the word Raha can be divided as Raha or Raha or Raha or Raha.

319
00:42:31,000 --> 00:42:32,280
So, this is clear words.

320
00:42:32,280 --> 00:42:40,200
So, this is the second one. He is fully enlightened, Samma Sambuddha.

321
00:42:42,120 --> 00:42:43,640
We can skip over it.

322
00:42:43,640 --> 00:42:55,240
Oh, yes. No, no, no, I remember. Now, that means, despite the knowledge, that this tries

323
00:42:55,240 --> 00:43:07,560
Kama really means, the knowledge of the food enlightenment, the food stage of enlightenment,

324
00:43:08,280 --> 00:43:15,000
knowledge which arises at the food stage of enlightenment. So, when a person becomes a rich,

325
00:43:15,000 --> 00:43:19,720
rich as the food stage of enlightenment, or in other words, when it doesn't becomes an

326
00:43:19,720 --> 00:43:30,680
other hand, he no longer accumulates any Kama, good or bad Kama. So, that is why he is called

327
00:43:33,000 --> 00:43:42,040
extinguisher of Kama. Well, a person becomes an other hand, even though he may do good things,

328
00:43:42,040 --> 00:43:51,080
he do not acquire Kama because his consciousness is not crucified at that time. His consciousness

329
00:43:51,080 --> 00:43:55,000
is a functional consciousness. So, that is what is meant here.

330
00:43:55,000 --> 00:44:15,800
That is, on phase 207, the wielding with the end of faith, the ex of knowledge that destroys Kama.

331
00:44:16,600 --> 00:44:22,840
So, that destroys Kama. The knowledge that destroys Kama. That means, when you get this knowledge,

332
00:44:22,840 --> 00:44:28,040
that means, when you become an other hand, you no longer acquire any Kama.

333
00:44:29,880 --> 00:44:34,920
You may do meritorious deeds, you may still do meritorious deeds, but they are not called

334
00:44:34,920 --> 00:44:44,040
kusala. They become kiriya, functional. So, gudas and other hands are said to have no kusala or

335
00:44:44,040 --> 00:44:53,960
kusala. They are those who rather increase both kusala and kusala, both wholesome and wholesome ex.

336
00:44:55,480 --> 00:45:06,360
So, I have not made not do any unwholesome ex, but he may do, which is wholesome to us, wholesome

337
00:45:06,360 --> 00:45:14,280
ex. But his ex are not said to be of wholesome time, but his ex are of a functional time,

338
00:45:14,280 --> 00:45:22,280
because they do not produce any results. That is why, here is said, the knowledge that destroys Kama.

339
00:45:22,280 --> 00:45:32,040
It really means the full stage of enlightenment or in other words, the full magga, the full magga,

340
00:45:32,040 --> 00:45:40,360
I mean, knowledge, which accompanies the full magga consciousness, of which arises with the

341
00:45:40,360 --> 00:45:47,720
full magga consciousness. I guess the reason I understand what this is saying is the reason

342
00:45:47,720 --> 00:45:54,600
they brought it up, is that they had the teacher, teachers in America who say that

343
00:45:54,600 --> 00:46:05,080
they don't create karma, but they are assuming that they are. They really don't create

344
00:46:06,440 --> 00:46:23,160
fresh karma. But they do not acquire fresh kusala karma or kusala karma, because their

345
00:46:23,160 --> 00:46:39,080
actions are all fruitless, not producing any fruit, any results. But not there.

