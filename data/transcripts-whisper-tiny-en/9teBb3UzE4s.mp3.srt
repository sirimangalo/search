1
00:00:00,000 --> 00:00:07,000
Hi, so today I'm going to talk a little bit about the word religion.

2
00:00:07,000 --> 00:00:15,000
This religion is a very interesting topic for me as a Buddhist monk.

3
00:00:15,000 --> 00:00:24,000
And I think it's one that's very misunderstood in the world, both by people who claim to be religious

4
00:00:24,000 --> 00:00:27,000
and by those who say they're not religious.

5
00:00:27,000 --> 00:00:39,000
We see that in general the word religion comes to be a dichotomy between whether you believe in God

6
00:00:39,000 --> 00:00:41,000
or do not believe in God.

7
00:00:41,000 --> 00:00:45,000
So in terms of religion we have the Theists and we have the Atheists.

8
00:00:45,000 --> 00:00:51,000
Then we have this sort of gray area in the middle where people are agnostic and we can see it all in some way

9
00:00:51,000 --> 00:00:54,000
or other has to do with belief in God.

10
00:00:54,000 --> 00:01:04,000
And I think people would be shocked when many people would be surprised at least to find that there's a entirely different way of looking at religion

11
00:01:04,000 --> 00:01:07,000
than having to do with whether you believe in God or not.

12
00:01:07,000 --> 00:01:14,000
In fact, it's clear that the word religion may have very little to do with belief in God.

13
00:01:14,000 --> 00:01:21,000
The word religion could simply mean being bound, being bound to a lifestyle, being bound to a way of living.

14
00:01:21,000 --> 00:01:24,000
We could have to do with monastic life.

15
00:01:24,000 --> 00:01:39,000
It could even simply come from the word religion as opposed to negligent, which means caution for negligence, the opposite of negligence, which is to be cautious and to be careful.

16
00:01:39,000 --> 00:01:48,000
And this is coming from a Buddhist perspective where we believe very much and being careful and in being bound to certain ethical principles.

17
00:01:48,000 --> 00:01:55,000
So we see that when the dichotomy is between being Theistic or Atheist, Atheistic.

18
00:01:55,000 --> 00:01:59,000
The whole idea of morals is somehow problematic.

19
00:01:59,000 --> 00:02:05,000
It's problematic for the Atheists in finding a reason not to do bad things.

20
00:02:05,000 --> 00:02:10,000
And as a result, their morality often becomes relative.

21
00:02:10,000 --> 00:02:13,000
So in certain instances one might not kill.

22
00:02:13,000 --> 00:02:20,000
In other instances one might kill because there's nothing intrinsically wrong with killing.

23
00:02:20,000 --> 00:02:30,000
Why? Because there's no belief in God who is the ultimate source of morality, according to both the Theists and Atheists.

24
00:02:30,000 --> 00:02:35,000
For the Theists, it presents a problem because you've got this dilemma.

25
00:02:35,000 --> 00:02:40,000
Is something wrong because God says it's wrong or does God say it's wrong because it's wrong?

26
00:02:40,000 --> 00:02:45,000
And either way you have a problem. If you say it's wrong just because God says it's wrong,

27
00:02:45,000 --> 00:02:52,000
or then the question is, you know, what if God says that it's okay to do this, to do that or to the other thing?

28
00:02:52,000 --> 00:03:00,000
Is it just because God says it's right or God says it's wrong? Even if it's clearly a wrong thing to do,

29
00:03:00,000 --> 00:03:03,000
does that make it right just because God says it's right?

30
00:03:03,000 --> 00:03:06,000
And if not, then what's the need for God?

31
00:03:06,000 --> 00:03:09,000
What is it that makes something wrong and what makes something right?

32
00:03:09,000 --> 00:03:14,000
And in the tradition that I follow, there's a whole other way of looking at this

33
00:03:14,000 --> 00:03:23,000
and it has to do with, of course, the meditation practice and the effect of things on a person's mind.

34
00:03:23,000 --> 00:03:29,000
The effect of killing and stealing and lying and cheating and taking drugs, taking alcohol,

35
00:03:29,000 --> 00:03:34,000
all of the effect these things have on who you are and the effect they have on the world around you.

36
00:03:34,000 --> 00:03:39,000
The effect they have on your relationships with other people, with other beings,

37
00:03:39,000 --> 00:03:44,000
and on your journey, your spiritual progress.

38
00:03:44,000 --> 00:03:48,000
So we look at religion as a very important thing.

39
00:03:48,000 --> 00:03:52,000
It's something that we bind ourselves to. We feel bound to.

40
00:03:52,000 --> 00:04:00,000
It's a way we live our lives and we live our lives in a very cautious way, trying to do everything based on wisdom,

41
00:04:00,000 --> 00:04:05,000
based on mindfulness, based on clear awareness of what we're doing.

42
00:04:05,000 --> 00:04:24,000
Is this a good thing to do? Am I doing this because I'm simply angry or because I'm addicted or because I'm looking down or I'm looking at other people condescending or jealous or stingy or have all these different unpleasant states of mind?

43
00:04:24,000 --> 00:04:29,000
Or am I doing this to help myself to help other people to create peace, to create harmony?

44
00:04:29,000 --> 00:04:34,000
And so this is how we judge, and this is how we live our lives.

45
00:04:34,000 --> 00:04:40,000
We live our lives in what we consider to be a very religious way, which has nothing to do with the question.

46
00:04:40,000 --> 00:04:47,000
This seemingly meaningless question, to ask the idea of whether I believe in God or not, don't believe in God is really ridiculous.

47
00:04:47,000 --> 00:04:51,000
It has nothing to do with what makes me a religious person.

48
00:04:51,000 --> 00:04:57,000
My religion is very much how I live my life, who I am, what I do, and my own spiritual progress,

49
00:04:57,000 --> 00:05:04,000
as well as the benefit that my life has on other people for their spiritual progress, and other beings.

50
00:05:04,000 --> 00:05:12,000
And so the belief in God is really nothing to do with religion, in my understanding of it.

51
00:05:12,000 --> 00:05:19,000
It's only one way of looking at religion, and it's very irrelevant to the way I look at religion.

52
00:05:19,000 --> 00:05:25,000
So it's just some thoughts for tonight, and I thought I'd share those with everybody, so thanks for tuning in.

53
00:05:25,000 --> 00:05:27,000
That was the best.

