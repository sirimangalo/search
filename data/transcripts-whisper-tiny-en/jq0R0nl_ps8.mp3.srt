1
00:00:00,000 --> 00:00:12,000
Hi, I'm welcome to Ask a Monk. In this series of videos, I'll be answering the questions people have in regards to the meditation practice, Buddhism, and the monks' life.

2
00:00:12,000 --> 00:00:25,000
So if you have a question, feel free to leave it either on my YouTube channel page or the Google Moderator page associated with Ask a Monk.

3
00:00:25,000 --> 00:00:32,000
And I will respond to you as I have time in video format.

4
00:00:32,000 --> 00:00:39,000
So you get a personal and direct answer to your question.

5
00:00:39,000 --> 00:00:44,000
And the great thing about this is everyone else gets the same answers.

6
00:00:44,000 --> 00:00:48,000
So rather than having to answer the same question again and again and again,

7
00:00:48,000 --> 00:01:00,000
answering it one time allows everyone to see the answer and those people who were maybe too shy or not able to put the question into words,

8
00:01:00,000 --> 00:01:06,000
are able to have many of the doubts and uncertainties in their minds, but to rest.

9
00:01:06,000 --> 00:01:16,000
So the rules are, it has to be a question, it has to be about either meditation, Buddhism, or the monastic life, something related to these topics.

10
00:01:16,000 --> 00:01:24,000
And you have to submit it to one of these two channels, either to my YouTube channel or to the Google Moderator page.

11
00:01:24,000 --> 00:01:27,000
And you don't have to leave your name if you go to the Google Moderator page.

12
00:01:27,000 --> 00:01:34,000
As far as I understand, you can put an anonymous question in.

13
00:01:34,000 --> 00:01:38,000
And I'll try to respond in the order I see fit.

14
00:01:38,000 --> 00:01:41,000
You'll find the answers are only on the Google Moderator page.

15
00:01:41,000 --> 00:01:47,000
So you have to go there to see if your question has been answered or to see which questions have been answered.

16
00:01:47,000 --> 00:01:55,000
If you have private questions that you'd like to ask personal questions or questions about your life in particular,

17
00:01:55,000 --> 00:01:59,000
then you're welcome to send me a private message and I will respond.

18
00:01:59,000 --> 00:02:03,000
If it's just a general question, something that you wouldn't mind sharing with other people,

19
00:02:03,000 --> 00:02:11,000
then please do leave it on the public forum for the benefit of other people as well.

20
00:02:11,000 --> 00:02:17,000
Thanks to everyone for tuning in and don't forget to subscribe to my YouTube channel for updates.

21
00:02:17,000 --> 00:02:22,000
You'll get the latest Ask Among answers to people's questions.

22
00:02:22,000 --> 00:02:38,000
Okay, all the best.

