1
00:00:00,000 --> 00:00:04,400
Hello, welcome back to Ask a Monk.

2
00:00:04,400 --> 00:00:18,040
Today's question is on whether I think that there is more suffering in America, this is

3
00:00:18,040 --> 00:00:26,360
the question, where it is almost impossible not to find food and I guess because we are

4
00:00:26,360 --> 00:00:34,280
the people in America had developed their society, or more suffering in a tribal society.

5
00:00:34,280 --> 00:00:37,280
This is the question.

6
00:00:37,280 --> 00:00:46,640
So first of all, I'd like to sort of expand the question because I think the important

7
00:00:46,640 --> 00:00:57,200
point is whether not whether America now or America when it was owned by the Native American

8
00:00:57,200 --> 00:01:00,800
or First Nations people is better.

9
00:01:00,800 --> 00:01:10,000
The question is whether there is more suffering to be found in a society with technological

10
00:01:10,000 --> 00:01:23,000
advancements and politics and government and roads and cars and computers and iPods and so on.

11
00:01:23,000 --> 00:01:39,000
Or whether there is more suffering in a lifestyle that is simple, unrefined, unadvanced,

12
00:01:39,000 --> 00:01:44,960
unorganized, unorganized on a level similar to a tribe.

13
00:01:44,960 --> 00:01:51,160
And it's a difficult question, but why it's difficult is because I think there's merits

14
00:01:51,160 --> 00:01:52,160
to both sides.

15
00:01:52,160 --> 00:02:02,520
I mean, I want to be able to say that material advancement in some way heightens people's

16
00:02:02,520 --> 00:02:14,880
ability or interest in things like science, things like objective investigation and analysis

17
00:02:14,880 --> 00:02:15,880
and so on.

18
00:02:15,880 --> 00:02:20,800
I mean, the education side of things, I want to be able to say that that's a good thing

19
00:02:20,800 --> 00:02:22,880
into some degree.

20
00:02:22,880 --> 00:02:29,960
But I think given the choice, I would have to say that there's probably less suffering

21
00:02:29,960 --> 00:02:36,280
to be found in a less advanced society.

22
00:02:36,280 --> 00:02:43,000
And I think we have to advance this beyond the tribal societies of, say, North America

23
00:02:43,000 --> 00:02:45,600
in olden times now.

24
00:02:45,600 --> 00:02:51,680
There are still some tribal societies, but not really to the extent that you would have

25
00:02:51,680 --> 00:02:53,800
found some time ago.

26
00:02:53,800 --> 00:02:58,760
Because I think this is a rather limited sort of way of looking at things.

27
00:02:58,760 --> 00:03:04,760
There are many societies or cultures or groups of individuals, groups of people living in

28
00:03:04,760 --> 00:03:11,960
the world who are living in limited technological advancement and so on.

29
00:03:11,960 --> 00:03:19,280
I think living in Thailand and living in Sri Lanka, I've been able to see some of these

30
00:03:19,280 --> 00:03:20,280
groups.

31
00:03:20,280 --> 00:03:24,760
I mean, Thailand, for example, is quite advanced in many ways.

32
00:03:24,760 --> 00:03:31,560
But in the villages, in the countryside, you'll still find people who are very technologically

33
00:03:31,560 --> 00:03:41,960
behind and living in fairly unorganized manners, in terms of political structure and so on.

34
00:03:41,960 --> 00:03:45,320
And you're in Sri Lanka even more so.

35
00:03:45,320 --> 00:03:54,560
You don't see the impact of modern society to such a great extent as you would in the

36
00:03:54,560 --> 00:03:55,560
West.

37
00:03:55,560 --> 00:04:05,560
And I think, beneficially, at least to some extent, that it has allowed people to be free

38
00:04:05,560 --> 00:04:07,720
from some of the sufferings.

39
00:04:07,720 --> 00:04:12,360
So stacking up the sufferings, I think the obvious suffering that was pointed out in the

40
00:04:12,360 --> 00:04:22,280
question is living in a tribal society or an uncivilized quote-unquote society, it's going

41
00:04:22,280 --> 00:04:27,400
to be more difficult to find food and I don't really think this is that big of a deal.

42
00:04:27,400 --> 00:04:33,600
In fact, I probably would care to wager that North America, it was a lot easier to find

43
00:04:33,600 --> 00:04:41,200
food before technology came into effect and now the food quality has gone down and people

44
00:04:41,200 --> 00:04:44,040
have to work hard for it and so on.

45
00:04:44,040 --> 00:04:49,000
I would say that anyway, that's maybe a bit speculative.

46
00:04:49,000 --> 00:04:54,400
But the real suffering, I think, that comes, well, it was being pointed here, pointed

47
00:04:54,400 --> 00:05:05,280
to here, is the lack of amenities, you know, living in rough conditions, having to put

48
00:05:05,280 --> 00:05:11,080
up with insects, having to put up with leeches, having to put up with snakes and scorpions,

49
00:05:11,080 --> 00:05:15,880
having to put up with mosquitoes because you don't have screens on your windows or you

50
00:05:15,880 --> 00:05:23,280
don't have the technology to prevent these things, having to put up with limited medical

51
00:05:23,280 --> 00:05:25,160
facilities and so on.

52
00:05:25,160 --> 00:05:32,360
There are a lot of sufferings that come with living in a simple manner and I think it's

53
00:05:32,360 --> 00:05:38,960
fair to point out that, you know, the monastic life is by its nature, the Buddhist monastic

54
00:05:38,960 --> 00:05:47,440
life is of a sort of simple life and as a result, there is a certain amount of suffering.

55
00:05:47,440 --> 00:05:50,280
This is why I think it was an interesting question and why I'd like to take some time

56
00:05:50,280 --> 00:05:56,520
to answer it because I have to personally go through some of the sufferings that, you

57
00:05:56,520 --> 00:06:01,080
know, the villagers here even have to go through, perhaps even more so than they have

58
00:06:01,080 --> 00:06:02,240
to go through.

59
00:06:02,240 --> 00:06:07,840
I might be going through some of the sufferings that you might expect from a tribal

60
00:06:07,840 --> 00:06:15,520
society because, you know, for me food is limited, I only eat one meal a day so you could

61
00:06:15,520 --> 00:06:21,400
say that's similar to the suffering that is brought about by having to hunt for your

62
00:06:21,400 --> 00:06:26,120
food, not knowing where you're going to get your next meal, not having a farm land or

63
00:06:26,120 --> 00:06:34,080
a grocery store or money that you can easily access these things with and having to

64
00:06:34,080 --> 00:06:41,760
put up with limited medical facilities, but I think the real key here is that, or the

65
00:06:41,760 --> 00:06:49,600
a real good way to answer this question is to point out the fact that so many people undertake

66
00:06:49,600 --> 00:06:56,880
this lifestyle on purpose and voluntarily and why would they do it if it was just going

67
00:06:56,880 --> 00:06:58,040
to lead to their suffering?

68
00:06:58,040 --> 00:07:02,120
And I think that's a question that a lot of people ask why do people decide to live in

69
00:07:02,120 --> 00:07:09,920
caves and where, you know, robes, you know, a single set of robes, et cetera, et cetera,

70
00:07:09,920 --> 00:07:15,360
et cetera, are eating only one meal a day, putting up with all of these difficulties.

71
00:07:15,360 --> 00:07:19,360
And I think it's because it's a trade-off.

72
00:07:19,360 --> 00:07:30,720
If you give up or if you want to be free from all of the stresses and all of the sufferings

73
00:07:30,720 --> 00:07:38,200
that are on the other side of the defense, then you have to, you can't expect to have

74
00:07:38,200 --> 00:07:39,200
all the amenities.

75
00:07:39,200 --> 00:07:40,240
I mean, they go hand in hand.

76
00:07:40,240 --> 00:07:46,040
The sufferings on the other side of a technologically advanced society are the stresses

77
00:07:46,040 --> 00:07:51,400
of having to work a job, you know, nine to five job, the stresses of having to think a

78
00:07:51,400 --> 00:07:56,520
lot of having to deal with people, crowds of people having to deal with traffic, having

79
00:07:56,520 --> 00:08:03,560
to deal with the complications that come with technology, like fixing your car and, you

80
00:08:03,560 --> 00:08:11,080
know, your house and all of the laws and having to deal with laws and lawyers and people

81
00:08:11,080 --> 00:08:13,480
suing you and so on.

82
00:08:13,480 --> 00:08:18,440
There's a huge burden of stress having to be worried about thieves, having to be worried

83
00:08:18,440 --> 00:08:26,240
about crooks, not crook butt, people cheating you, having to be worried about your

84
00:08:26,240 --> 00:08:32,200
boss, your co-workers, your clients and so on and a million other things.

85
00:08:32,200 --> 00:08:37,440
I mean, no matter what work you apply yourself to in an advanced society, there are more

86
00:08:37,440 --> 00:08:44,240
complications and as a result, more stress and as a result, more suffering.

87
00:08:44,240 --> 00:08:52,960
So I think the trade-off there is quite worth it in my mind because I'd like to, you

88
00:08:52,960 --> 00:08:57,200
know, the answer for me is quite clear because having put up with all these sufferings

89
00:08:57,200 --> 00:09:03,920
of having to have mosquitoes bite me and who knows what bite me and having to put up

90
00:09:03,920 --> 00:09:09,800
with leeches and you know, the worries that I have to put up with worrying about stepping

91
00:09:09,800 --> 00:09:15,880
on a snake, worrying about that forefoot lizard down below me and the monkey scrapping

92
00:09:15,880 --> 00:09:17,880
on my head and so on.

93
00:09:17,880 --> 00:09:25,240
I really don't feel upset or affected by these things, they're physical, a physical reality

94
00:09:25,240 --> 00:09:28,160
and they're much more physical than they are mental.

95
00:09:28,160 --> 00:09:34,600
I feel so much more at peace here living in the jungle which, you know, may look peaceful

96
00:09:34,600 --> 00:09:41,200
but it's actually quite dangerous and it keeps you on your toes in many ways, even during

97
00:09:41,200 --> 00:09:45,480
meditation, you have to do these little bugs that you can't even see them, they're

98
00:09:45,480 --> 00:09:53,240
worse than mosquitoes and they bite you but it's not a stress for me, not in the same

99
00:09:53,240 --> 00:09:59,560
way as living in, say, the big cities of Colombo, Bangkok, Los Angeles, the stress is

100
00:09:59,560 --> 00:10:04,960
that's involved there, you know, it's an order of magnitude greater.

101
00:10:04,960 --> 00:10:09,760
So I'm not sure if this exactly answers the question but this is how I would prefer

102
00:10:09,760 --> 00:10:20,160
to approach it, I think there's far less stress to be had from a simple lifestyle because

103
00:10:20,160 --> 00:10:30,760
the stress is much more physical than they are mental and the mental stress is far less.

104
00:10:30,760 --> 00:10:38,120
Another thing about living in a simple society in a simple lifestyle is that it's much

105
00:10:38,120 --> 00:10:43,240
more in tune with our programming physically, I mean we're still very much related to

106
00:10:43,240 --> 00:10:51,320
the monkeys and so this kind of atmosphere jives with us, seeing the trees, why people

107
00:10:51,320 --> 00:10:55,360
go to nature and they feel peaceful of a sudden is because of how, it's not because there's

108
00:10:55,360 --> 00:11:00,680
anything special there, it's because of what's not there, there's nothing jarring with

109
00:11:00,680 --> 00:11:06,440
your experience, no horns blasting, no bright lights, no beautiful pictures catching your

110
00:11:06,440 --> 00:11:14,520
attention, no ugly sights and so on, it's very peaceful, very calm, very natural, right?

111
00:11:14,520 --> 00:11:22,400
I mean that's why we're here and I think you'll find that in a traditional society.

112
00:11:22,400 --> 00:11:27,360
So I think, and I think this is borne out by the Buddhist teaching, it's borne out by

113
00:11:27,360 --> 00:11:32,920
the examples, obviously, of those people who have ordained us monks.

114
00:11:32,920 --> 00:11:36,600
There was a story in the typical, in the Buddhist teaching that we always go back to,

115
00:11:36,600 --> 00:11:42,720
I think in the commentaries actually, I'm not sure, well at least it's in the commentaries

116
00:11:42,720 --> 00:11:53,000
about this king, relative of the king who became a monk, Mahakapina and after he became

117
00:11:53,000 --> 00:11:57,000
a monk, he would sit under this tree, used to be a king, right?

118
00:11:57,000 --> 00:12:04,040
So after he became a monk, he would sit under a tree and go, a horse, a horse, a horse,

119
00:12:04,040 --> 00:12:09,440
a horse, which means, oh, what happiness, oh, what bliss, oh, what happiness.

120
00:12:09,440 --> 00:12:15,520
So you might say he was actually contemplating the happiness, he was looking at it and examining

121
00:12:15,520 --> 00:12:20,680
it in a much in the same way that we say to ourselves, happy, happy.

122
00:12:20,680 --> 00:12:26,160
But the monks heard him saying this and remarking this and they took it the wrong way,

123
00:12:26,160 --> 00:12:29,840
they took it to me and he was, for some reason, they took it that he must be thinking

124
00:12:29,840 --> 00:12:36,720
about his kingly happiness, he was remembering, oh, what happiness it was to be a king.

125
00:12:36,720 --> 00:12:39,680
And so they took him to the Buddha and the Buddha asked him, you know, what was it?

126
00:12:39,680 --> 00:12:47,640
And he said, no, when I was a king, I had to worry about all of these stresses and concerns.

127
00:12:47,640 --> 00:12:51,920
My life was always in danger of assassination, I had all this treasure that I had to guard

128
00:12:51,920 --> 00:12:58,120
all these ministers that I had to manage and, you know, to stop from cheating me and all

129
00:12:58,120 --> 00:13:02,120
the people who would come to me with their problems and all the laws I had to enforce

130
00:13:02,120 --> 00:13:04,720
and so on and so on.

131
00:13:04,720 --> 00:13:05,760
And I have none of that now.

132
00:13:05,760 --> 00:13:12,800
Now I have this tree, three robes, my bull, my tree and this is the greatest happiness

133
00:13:12,800 --> 00:13:15,320
I've ever known in my life, he said.

134
00:13:15,320 --> 00:13:20,840
So there's a story to go with this question.

135
00:13:20,840 --> 00:13:27,480
I think one final note is that regardless of the answer to this question, whether there's

136
00:13:27,480 --> 00:13:35,000
more suffering in either one, I think the point is that whatever lifestyle you can undertake,

137
00:13:35,000 --> 00:13:40,720
not that has the least amount of suffering, but which has the least amount of unwholesumness

138
00:13:40,720 --> 00:13:43,600
involved and that is the best lifestyle.

139
00:13:43,600 --> 00:13:47,600
So for those people, you know, who might be discouraged then by the fact that they have

140
00:13:47,600 --> 00:13:52,440
to live in a technologically advanced society as a Buddhist and they feel like they should

141
00:13:52,440 --> 00:13:57,000
be living in the forest, I don't mean to say that there's anything wrong with living

142
00:13:57,000 --> 00:13:58,000
in the city.

143
00:13:58,000 --> 00:13:59,000
I've lived in Los Angeles.

144
00:13:59,000 --> 00:14:05,880
I've gone on arms round in North Hollywood and in places, you know, in Bangkok, in Colombo.

145
00:14:05,880 --> 00:14:12,560
I've gone, I've lived in these places in fairly chaotic and stressful situations, but

146
00:14:12,560 --> 00:14:16,800
the point is to live your life in such a way that it's free from unwholesumness.

147
00:14:16,800 --> 00:14:23,400
So this is where the question gets interesting because many tribal societies are involved

148
00:14:23,400 --> 00:14:26,400
in what Buddhists would call great unwholesumness.

149
00:14:26,400 --> 00:14:31,400
Their way of lifestyle is hunting, as an example, and though they do it to serve, you

150
00:14:31,400 --> 00:14:39,680
could say it to survive, it's by nature caught up with these mistakes of cruelty, states

151
00:14:39,680 --> 00:14:50,880
of anger and delusion, and as a result, it keeps them bound to the wheel of life that they

152
00:14:50,880 --> 00:14:53,680
are going to have to take turns with the deer.

153
00:14:53,680 --> 00:14:56,880
You know, this is what the First Nations people believe.

154
00:14:56,880 --> 00:15:01,000
They still believe that, you know, you take turns.

155
00:15:01,000 --> 00:15:06,160
I hunt the deer this life and in the next life, I'm the deer and they hunt me and so on.

156
00:15:06,160 --> 00:15:10,240
There are these kinds of beliefs and this is really in line with the Buddha's teaching.

157
00:15:10,240 --> 00:15:16,720
So I mean, if you're fine with that, having to be hunted every other lifetime, every few

158
00:15:16,720 --> 00:15:19,840
lifetimes, then so be it.

159
00:15:19,840 --> 00:15:24,760
But this isn't the, from a Buddhist point of view, this isn't the way out of suffering.

160
00:15:24,760 --> 00:15:26,280
It's because there's a lot of suffering.

161
00:15:26,280 --> 00:15:30,720
I mean, tribes, of course, then there's tribal warfare and, you know, they're not free from

162
00:15:30,720 --> 00:15:32,280
all of this.

163
00:15:32,280 --> 00:15:37,120
And we're not free from it on the other side as living in technologically advanced societies.

164
00:15:37,120 --> 00:15:41,080
You know, living in Canada, I wasn't, you know, free from these things.

165
00:15:41,080 --> 00:15:42,080
I was hunting as well.

166
00:15:42,080 --> 00:15:44,920
I didn't need to, but I went hunting with my father.

167
00:15:44,920 --> 00:15:50,480
You know, we engage in a lot of unholesumness in advanced societies.

168
00:15:50,480 --> 00:15:52,920
So that's the most important.

169
00:15:52,920 --> 00:15:53,920
You can live in a city.

170
00:15:53,920 --> 00:15:54,920
You can live in nature.

171
00:15:54,920 --> 00:16:01,120
The reason why we choose to live in nature is, I think, overall, you know, the amount of

172
00:16:01,120 --> 00:16:05,920
peace that you have is the one side, but there's also a lot less unholesumness that you're

173
00:16:05,920 --> 00:16:13,200
free from having to deal with, you know, other people's unholesumness and having to see

174
00:16:13,200 --> 00:16:18,880
all these beautiful, attractive sites and wanting to get this wanting to get that being

175
00:16:18,880 --> 00:16:21,080
annoyed by people being upset by people.

176
00:16:21,080 --> 00:16:28,480
So you're able to focus much clearer on the very core aspects of existence, you know, what

177
00:16:28,480 --> 00:16:35,120
really is reality, without having to be, get caught up in unholesum mindset, stress and

178
00:16:35,120 --> 00:16:37,840
addiction and so on.

179
00:16:37,840 --> 00:16:38,840
And I think that's clear.

180
00:16:38,840 --> 00:16:44,320
I think being here in the forest is much better for my meditation practice than being

181
00:16:44,320 --> 00:16:45,800
in the city.

182
00:16:45,800 --> 00:16:51,520
But in the end, it's not the biggest concern, and I'm not afraid to live in the city if

183
00:16:51,520 --> 00:16:57,040
I have to, because, for me, the most important is to continue the meditation practice

184
00:16:57,040 --> 00:17:02,120
and to learn more about my reactions and my interactions with the world around me.

185
00:17:02,120 --> 00:17:05,480
And sometimes that can be tested by suffering.

186
00:17:05,480 --> 00:17:09,240
Again, the Buddha's teaching is not to run away from suffering.

187
00:17:09,240 --> 00:17:12,200
It's to learn and to come to understand suffering.

188
00:17:12,200 --> 00:17:16,080
When you understand suffering, you'll understand what is really causing suffering, that

189
00:17:16,080 --> 00:17:20,880
it's not the objects, it's your relationship and your reactions to them, your attachment

190
00:17:20,880 --> 00:17:25,600
to things being in a certain way and not being in another way, your attachment to the

191
00:17:25,600 --> 00:17:27,960
objects of the sense and so on.

192
00:17:27,960 --> 00:17:31,960
When you can be free from that, it doesn't matter where you live, whether it be in a cave

193
00:17:31,960 --> 00:17:34,160
or in an apartment complex.

194
00:17:34,160 --> 00:18:03,240
So there's the answer to your question, thanks, and have a good day.

