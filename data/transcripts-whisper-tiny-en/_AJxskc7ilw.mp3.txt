How can we reconcile our personal relationships, those with spouses and children especially,
which the Buddha teaches are an obstacle to enlightenment?
Well, this relates back to the idea of where our practice begins and the difference between our progress in practice and our idea of the practice,
because we generally get this idea that the Buddha taught us to give up all of these things,
which he did. But when we get that idea in our minds, our minds of course start to play all sorts of tricks on us,
and tell us what a horrible and teaches would a horrible thing that would be.
And so we either have to repress our intuition, which tells us how great these things are,
or we have to deny the Buddha's teaching and give it up.
So we have this conflict inside. We truly believe in our hearts that these relationships are important.
Well, ordinary people do. There are different levels as the Buddha talked about.
There's the level of views, so people who truly believe that these are beneficial.
Then there's the people who have seen through those views and come to see that they're actually not beneficial.
They're still core to them, or nothing intrinsically beneficial about them.
But they still think that they're beneficial. It still comes to them at some times that,
hey, this seems beneficial. It seems like a good thing.
Or maybe they think, boy, I'm so glad that I have a wife or a husband or a children or so on.
They still think like this.
And then there's the people who have even done the way with the thoughts, but they still give rise to the feelings.
The sun, yeah. When they see their children, they think, still think those are my children,
and when they see their husband or wife, this is my husband and my wife, and they think they feel happy about that.
And so this is where we're at, and this is where the conflict comes from, because we have this idea of how the Buddha,
we know that the Buddha taught that these are bad, and yet we still have these.
We still either believe them or we think them or we feel them, and generally it's all three for most people.
So the way to deal with this, the reconciliation of this is to think of the Buddha's teaching,
as I've said before, think of it like a sweater.
The Buddha's pointing out this loose thread on your sweater,
or maybe not the Buddha's pointing out, the Buddha's pointing out the idea of suffering,
and we can look at our lives, and we can see some loose thread in our life,
that no matter what we believe about things like relationships, we can see suffering.
So in our relationship, we see suffering.
You can ask yourself, are all of my relationships perfectly harmonious?
And when you say the answer is no, we have no trouble agreeing that that disharmony is a problem.
And this is totally a part from whether the relationship is useful or not useful as beneficial as leading to happiness or so on.
Whether our attachment to those people,
or we can see that we have certain attachments or certain problems in our mind that are giving rise to suffering,
regardless of the overall nature of the relationship.
So we deal with those, and this is like we start pulling on this loose thread in our sweater.
And what happens is, eventually, if you go far enough, you trace the causes right back to the root of our attachment and our partiality to certain individuals.
But you do that practically. For me to say this to you, it's not meant so that you then go back and think about it,
or you adopt this view.
The point is to stop thinking like this.
To stop thinking that your relationships are a problem or getting in the way of your relationship, getting in the way of your progress.
And deal with those parts of your relationship that are clearly causing your suffering.
This is in regards to the idea of our reconciling our idea that these relationships are positive.
On a practical side, there's still the question, which may be the question that you're asking,
of what to do then, what to do once you realize that these are getting in the way of enlightenment,
that these are causing you difficulty.
But I still think you have to look at it more practically and see that it's not the people,
and it's not the fact that you have to be around these people that is causing you the problem.
It's still our moment-to-moment time-to-time, occasional arising of defilement.
And those don't arise all the time.
Sometimes these people will not cause a suffering or stress,
or will not give us occasion to do nasty or say nasty things, or think nasty things.
But on occasion they do. On occasion their arises and their mind nasty thoughts.
We say nasty things, and we do nasty things, and we do nasty actions based on our relationship with other people.
This is really where we have to approach the problem.
And the unraveling of the sweater is not only in our mind.
You'll find that your whole life unravels, your family changes,
some members of your family or your social circle might leave or drift apart.
In fact, if you get far enough, you might eventually naturally drift apart from all the people that you know,
or many or most of the people that you know.
But the point I'm trying to make is that that comes naturally.
And it doesn't necessarily have to be a break.
There may come a time where you decide you want to break.
You say I've had enough, or I need a break, and I'm going to take a vacation,
take some time to go and do meditation on my own.
Travel to Sri Lanka and go and stay in some monks cave.
For example, but it doesn't necessarily have to come like that.
And through constant and diligent practice and applying yourself to the Buddhist teaching,
things will naturally change.
It naturally changes in your mind, so you don't ever have to feel some conflict inside your attachment to your family
and your attachment to the Buddhist teaching.
And also so that you don't have to worry or fret about your surroundings,
because our surroundings will change based on the state of our mind.
They might not change as quickly as our mind.
So our mind might begin to say this is not how I want to live my life,
and you're still stuck in a life, in living that life.
But through patience and through a clear understanding of where you want to head,
it will come in time.
The real problem is that we're generally impatient,
and we lose sight of the fact that everything changes.
And we haven't had enough experience in life, because life is quite short,
to show us that, you know, tomorrow things might change totally,
and a week from now and a month from now,
and really every year is different from the last and so on.
So that things are not, you know, the point being that we don't see impermanence clearly enough,
we don't realize that this situation that we find ourselves in is not static,
and it will change, and eventually we might,
well, it will change eventually based on our mind.
So if we're set on meditation eventually our lives become more meditative.
We hope that helps.
