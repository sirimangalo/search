1
00:00:00,000 --> 00:00:12,760
Okay, well welcome everyone, today I will be giving starting up again, hopefully, starting

2
00:00:12,760 --> 00:00:28,000
up at my regular weekly talk, and today's talk is on a very special subject.

3
00:00:28,000 --> 00:00:44,400
The auspicious night or the auspicious attachment, there are two translations of this.

4
00:00:44,400 --> 00:00:52,240
I'm sending a link out here and I hope it works, but if you want to read along, I think

5
00:00:52,240 --> 00:00:59,240
this link should pull up the PDF file for you.

6
00:00:59,240 --> 00:01:10,000
Okay, so if you want to follow along, it's not really necessary, I'm not going to go by

7
00:01:10,000 --> 00:01:11,000
this script.

8
00:01:11,000 --> 00:01:19,640
And the problem with English translations of the Buddha's teaching are that they sort

9
00:01:19,640 --> 00:01:28,720
of give away the repetitive nature of the polytext in a way that's not at all flattering

10
00:01:28,720 --> 00:01:37,960
because the polytexts are meant to be recited and the poly words and idioms are very

11
00:01:37,960 --> 00:01:39,720
poetic as well.

12
00:01:39,720 --> 00:01:44,760
So even reading the poly, the repetitions in the poly is not nearly as arduous as reading

13
00:01:44,760 --> 00:01:48,800
the corresponding English translations.

14
00:01:48,800 --> 00:01:56,040
But the outline is very clear, this isn't how anyone would teach in English if you

15
00:01:56,040 --> 00:02:03,120
were going to give this sort of talk, but it's a very cut and dry methodical sort of

16
00:02:03,120 --> 00:02:05,880
teaching.

17
00:02:05,880 --> 00:02:10,360
The greatest part of this today is the verses which are in fact poetic.

18
00:02:10,360 --> 00:02:18,040
I think even in the English that is kept.

19
00:02:18,040 --> 00:02:22,760
And I'm just going to read them in poly to you, not that anyone here probably will understand

20
00:02:22,760 --> 00:02:29,840
them or a few of you at any rate, but just just so you get an idea of how it sounds

21
00:02:29,840 --> 00:02:33,000
in the original poly.

22
00:02:33,000 --> 00:02:36,680
So we're starting at the verses here where it says, let not a person revive the past.

23
00:02:36,680 --> 00:02:41,120
And I'll go over it with you if you're not reading the PDF, but first let's listen to it

24
00:02:41,120 --> 00:02:43,080
in poly.

25
00:02:43,080 --> 00:03:09,800
And I'll go over it with you, and I'll go over it with you.

26
00:03:09,800 --> 00:03:20,480
And I'll go over it with you.

27
00:03:20,480 --> 00:03:31,760
And then I'll go over it with you, and I'll go over it with you.

28
00:03:31,760 --> 00:03:38,760
Mottanditang, dang wai patekarato di Santo a jikatem uni di

29
00:03:42,080 --> 00:03:44,920
which translates loosely as,

30
00:03:44,920 --> 00:03:49,320
let not a person revive the past or on the future build his hopes.

31
00:03:49,320 --> 00:03:53,640
For the past has been left behind and the future has not been reached.

32
00:03:53,640 --> 00:03:59,000
Instead, with insight, let him see each present or presently arisen state.

33
00:03:59,000 --> 00:04:02,040
Let him know that, let him know that, and be sure of it,

34
00:04:02,040 --> 00:04:04,880
invincibly, unshakably.

35
00:04:04,880 --> 00:04:08,800
Today, the effort must be made to moral death may come who knows.

36
00:04:08,800 --> 00:04:13,200
No bargain with mortality can keep him and his hordes away.

37
00:04:13,200 --> 00:04:17,480
But one who dwells thus ardently, relentlessly, by day and night.

38
00:04:17,480 --> 00:04:19,960
It is he, the peaceful sage, has said,

39
00:04:19,960 --> 00:04:23,640
who has one fortunate attachment.

40
00:04:23,640 --> 00:04:25,880
I'm going to say right off that I'm,

41
00:04:25,880 --> 00:04:30,680
I'm, ambivalent as to this translation of the word rata.

42
00:04:30,680 --> 00:04:33,400
Rata can also mean night.

43
00:04:33,400 --> 00:04:38,400
It's not really clear to me.

44
00:04:38,400 --> 00:04:43,120
But the commentaries say attachments, so we're going to go with that.

45
00:04:43,120 --> 00:04:44,640
I like the idea of thinking that this is,

46
00:04:44,640 --> 00:04:45,920
and this is the way they used to translate,

47
00:04:45,920 --> 00:04:51,920
and there's like one fortunate day.

48
00:04:51,920 --> 00:04:59,120
No, I think that's, I'm not sure about that.

49
00:04:59,120 --> 00:05:01,360
No, rata, rata means night,

50
00:05:01,360 --> 00:05:04,120
but rata can also mean attachment.

51
00:05:05,560 --> 00:05:10,080
But here, I just want to go with this theme of having one good day,

52
00:05:10,080 --> 00:05:15,040
because today is the day where all around the world,

53
00:05:15,040 --> 00:05:20,800
people are making videos and posting them to YouTube to be used

54
00:05:20,800 --> 00:05:24,760
in a documentary.

55
00:05:24,760 --> 00:05:28,040
And so you've got, I don't know, maybe thousands of people

56
00:05:28,040 --> 00:05:30,280
uploading these videos, and it's going to be put together

57
00:05:30,280 --> 00:05:35,200
by these award-winning directors,

58
00:05:35,200 --> 00:05:38,080
who are going to make a documentary about it.

59
00:05:38,080 --> 00:05:40,120
It's called Life in the Day.

60
00:05:40,120 --> 00:05:43,040
And so I thought, well, what a better theme

61
00:05:43,040 --> 00:05:47,200
from a Buddhist point of view, where we,

62
00:05:47,200 --> 00:05:49,280
this is what we're all about, is living,

63
00:05:49,280 --> 00:05:52,280
living life for the day, or living life for the moment.

64
00:05:52,280 --> 00:05:54,840
You know, this whole idea of carpe diem.

65
00:05:54,840 --> 00:05:58,160
Well, I would submit that Buddhism is the ultimate carpe diem.

66
00:05:58,160 --> 00:06:00,280
It's, the ultimate sees the day.

67
00:06:02,040 --> 00:06:05,960
And I think this suit is a very good example of that.

68
00:06:05,960 --> 00:06:07,840
And we are always quoting this suit,

69
00:06:07,840 --> 00:06:10,280
that don't go back to the past,

70
00:06:10,280 --> 00:06:11,800
don't follow after the past,

71
00:06:11,800 --> 00:06:13,400
don't worry about the future.

72
00:06:15,200 --> 00:06:18,320
The actual translation here would be don't doubt,

73
00:06:18,320 --> 00:06:20,200
or be concerned with the future.

74
00:06:21,120 --> 00:06:23,960
Because what is in the past has already gone,

75
00:06:23,960 --> 00:06:25,920
what is in the future has not yet come,

76
00:06:26,800 --> 00:06:29,040
living for the present moment, and seeing it clearly.

77
00:06:29,040 --> 00:06:33,320
This is an incredibly succinct summary

78
00:06:33,320 --> 00:06:34,760
of the Buddhist teaching,

79
00:06:34,760 --> 00:06:36,520
that we should live for the moment here and now.

80
00:06:36,520 --> 00:06:37,840
And it's a meditation teaching.

81
00:06:37,840 --> 00:06:42,840
It shows that Buddhism is not a theoretical religion.

82
00:06:43,280 --> 00:06:46,480
It's not a theoretical path where we have to study

83
00:06:46,480 --> 00:06:51,480
and we have to master all sorts of doctrines.

84
00:06:51,720 --> 00:06:53,560
When it boils right down to it,

85
00:06:53,560 --> 00:06:56,360
it's living in the moment, living in the here and now.

86
00:06:57,360 --> 00:07:01,680
Living in this moment, which most of us think

87
00:07:01,680 --> 00:07:03,200
we're already living in.

88
00:07:03,200 --> 00:07:04,360
And I've always talked about this,

89
00:07:04,360 --> 00:07:06,360
how we think we're in the present moment.

90
00:07:06,360 --> 00:07:08,040
We think we're here and now.

91
00:07:08,040 --> 00:07:11,280
We think we're ever present.

92
00:07:11,280 --> 00:07:14,560
And as a result, we think of that as a very mundane

93
00:07:14,560 --> 00:07:15,760
sort of accomplishment.

94
00:07:15,760 --> 00:07:16,720
We think so big deal.

95
00:07:16,720 --> 00:07:17,560
You're here, you're now.

96
00:07:17,560 --> 00:07:21,560
I'm here, I'm now, I'm stuck here, I'm stuck now.

97
00:07:23,040 --> 00:07:27,880
And so really often our paths and our goals

98
00:07:27,880 --> 00:07:32,120
are all to be free from the present moment.

99
00:07:32,120 --> 00:07:35,960
And we can see how our activities are all based on

100
00:07:37,000 --> 00:07:41,800
going after things that haven't come yet

101
00:07:41,800 --> 00:07:46,800
or worrying or fretting or dwelling on things

102
00:07:46,800 --> 00:07:48,920
that have happened in the past.

103
00:07:48,920 --> 00:07:51,800
And this should be immediately obvious

104
00:07:51,800 --> 00:07:56,320
to the average person that much of our life

105
00:07:56,320 --> 00:08:00,040
is living in the past or living in the future.

106
00:08:00,040 --> 00:08:02,920
And when disaster strikes,

107
00:08:04,320 --> 00:08:09,320
when we meet with some sort of calamity or misfortune,

108
00:08:09,320 --> 00:08:13,400
it can get to the point where we're never here.

109
00:08:13,400 --> 00:08:14,640
We're never now.

110
00:08:14,640 --> 00:08:16,680
When we're driving, when we're walking,

111
00:08:16,680 --> 00:08:20,400
when we're cleaning, doing daily chores,

112
00:08:23,440 --> 00:08:25,040
we're never here and we're never now.

113
00:08:25,040 --> 00:08:27,160
We're never with the moment.

114
00:08:27,160 --> 00:08:29,800
Right now when we're sitting here looking

115
00:08:29,800 --> 00:08:31,480
at the computer screen and watching me talk,

116
00:08:31,480 --> 00:08:33,800
you can ask yourself, are you really listening to me talk

117
00:08:33,800 --> 00:08:36,040
or are you doing a thousand things at once?

118
00:08:36,040 --> 00:08:37,840
Are you also checking your IMs?

119
00:08:37,840 --> 00:08:42,840
And looking at other websites and downloading this

120
00:08:43,040 --> 00:08:46,320
or downloading that, or are you really here in present?

121
00:08:46,320 --> 00:08:48,600
Do you know that you're sitting in the chair?

122
00:08:48,600 --> 00:08:50,800
Are you aware that you're sitting right now?

123
00:08:50,800 --> 00:08:54,080
Are you aware of the feelings that are arising in the body?

124
00:08:54,080 --> 00:08:56,760
Are you aware of the emotions that are arising in the mind?

125
00:08:58,560 --> 00:09:00,480
Because what happens is when these things come up,

126
00:09:00,480 --> 00:09:03,360
they drag us into the past and they drag us into the future

127
00:09:03,360 --> 00:09:06,800
and they drag us into concepts,

128
00:09:06,800 --> 00:09:08,880
they drag us into imagination.

129
00:09:08,880 --> 00:09:12,400
We suddenly start running away with us.

130
00:09:13,240 --> 00:09:15,200
Where we'll be sitting here,

131
00:09:15,200 --> 00:09:18,760
we'll be listening to, you may be listening to what I say,

132
00:09:18,760 --> 00:09:20,280
but then suddenly it hits you in a certain way,

133
00:09:20,280 --> 00:09:23,120
you like it or you don't like it and suddenly you're off.

134
00:09:23,120 --> 00:09:24,280
You're thinking about something,

135
00:09:24,280 --> 00:09:26,480
you're thinking about how it reminds you of this

136
00:09:26,480 --> 00:09:29,440
or reminds you of that, or maybe I start to get boring

137
00:09:29,440 --> 00:09:32,760
and you start to wander off and think of something

138
00:09:32,760 --> 00:09:36,360
totally unrelated, maybe someone walks into the room

139
00:09:36,360 --> 00:09:39,040
and distracts your attention or so on.

140
00:09:40,120 --> 00:09:44,600
So the teaching of the Buddha is to be present,

141
00:09:44,600 --> 00:09:46,120
is to know what's going on,

142
00:09:46,120 --> 00:09:47,320
is to be aware of what's going on.

143
00:09:47,320 --> 00:09:49,640
And that's really the essence of the meditation

144
00:09:49,640 --> 00:09:50,720
that we practice.

145
00:09:51,960 --> 00:09:53,520
When we start, simply we-

