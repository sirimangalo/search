Okay, good evening everyone, welcome to our evening dumber.
Tonight we're looking at Samawitaka, which is usually translated as right thought.
Recently I heard someone say, oh no, right thought is probably not a good translation.
And I almost changed it, I almost put something different, but I've changed it back.
Because it's simple, right thought is easy to remember.
It's not a mouthful like anything else.
And thought is an interesting word, it's a good word because of how many uses we have for it.
When we say right thought, immediately we think of the thought, right?
I like turtles, that's a thought example.
We have a thought arise and we think, well that's what we mean by thought.
But when you think of something, what you think about something, we also use that for thought.
I think Buddhism is good.
I think meditation is great.
That's a judgment we would say.
Or I'm thinking about suicide, I'm thinking of killing myself.
That's not just a thought, it's an inclination.
And it's the inclinations I think that we're most concerned with here.
This monk was right, that right thought is a dangerous translation, it's a problematic translation.
Because we're certainly not talking about thought.
And it gives the idea that a thought itself can be wrong.
I know if you have thought, if a thought arises in your mind,
I wonder what it would be like to kill myself or maybe the answer to life's problems is to kill myself.
A thought like that can arise.
Or you imagine doing something terrible and you think about killing someone and just the thought arises.
It can arise without any inclination on your part to do it.
You can be horrified by the thought or just find it ridiculous that you would think such a thing.
And that's important because the thoughts themselves are not a problem.
It's important to get across meditators often feel very guilty about their thoughts.
You think of something and you think, oh, that's so wrong to think that.
I shouldn't think that as a meditator.
That's not the problem.
What we're really concerned with here is your inclinations.
Are you inclined to kill yourself? Are you inclined to kill someone else?
If so, there's a problem.
Are you inclined to think of these things as problems?
Well, that's a problem as well.
It's our judgments and our reactions, really.
So this is what we talk about.
Some mawitaka is being free from having the opposite kinds,
having good kinds of inclinations,
inclining to met towards meditation, inclining towards being kind to others.
Good inclinations.
Good thought is the other member of the wisdom faculty,
the wisdom group of the eight full noble paths.
So we have right view and then right thought.
That's important to understand the distinction.
Right view is what you believe.
I believe this is right.
I believe this is wrong.
I should kill myself.
I should kill that person.
It's right to do.
Killing is good.
Stealing is good.
Getting angry, that's a good thing.
This is all wrong with you.
Right view is when you know anger that's not useful.
It's cause for suffering.
Desire that's also not useful.
That's your view when you know these things to be true.
It's actually a lot easier.
It's very difficult even to come to that view.
That's something we should all be quite happy about and confident
about the fact that we're in a state where we think meditation is good.
Right.
We have this view that meditation is beneficial and that getting caught up in the world is problematic.
It's much more difficult and it's a whole other thing to give up the inclination for those things.
Right.
You can know that a drug addict knows that the drugs are really, really a bad idea.
It doesn't mean they don't do drugs.
They're all the time doing them, all the time they're doing them, they know that this is wrong, this is a problem.
They still do the drugs because they don't have right thought yet.
They have right view, but they don't have the right inclination.
That's what we work on in meditation really.
It's where our meditation practice takes place.
Along the way, right view comes up and right view cuts off the inclination.
So in your first meditating, you don't have either.
But you work on the right inclination.
You incline yourself to understand things.
You incline yourself towards meditation.
You say, okay, well, I'll try this. Maybe it's a good thing.
The right inclination, and so you work at it.
You work at purifying your mind, stopping your reactions.
You work at being objective towards things.
And after you do that, eventually right view arises with the realization of Nibana.
Right view arises, you say, yes.
Yes, these things are good, and yes, those things are bad.
And that right view supports your right intention.
But then you have to work to work on the right intention again.
Only this time now you know that it's wrong.
But even someone who knows, someone who's seen Nibana still gets greedy,
still gets angry, still has wrong inclination, potentially,
until they work it out completely.
The Buddha likened it to someone who is wearing white,
white, clean, white clothes, and perfumed, and really concerned about cleanliness.
But then they're walking down this path,
and suddenly they say, bull elephant charging at them.
And they look off to the side of the path, and there's a cesspool.
The cesspools are, you know, there's feces, urine, and lots of lots of gross things.
And they jump into the cesspool to avoid the charging elephant.
The Buddha likened this sort of thing, a soda pan, and knows that it's wrong,
knows that these things are wrong.
But in order to get away from the Kilesa, the anger is overwhelming,
and the fears overwhelming, the greed is overwhelming, the desire and lust.
They do it.
But once they do it, they feel awful.
They feel dirty, they're disgusted.
They'll never say to themselves, that was a good idea.
Well, it breaks down there, because the person who jumped in the cesspool
probably it wasn't a good idea to avoid the elephant.
But I suppose you could go further and say it's like they knew
that the right thing to do would be to somehow tame the elephant.
But they weren't strong enough to tame that elephant.
So they jumped into the cesspool, and they know it wasn't a good thing.
It wasn't pleasant.
They're not happy about having jumped in the cesspool.
That's how it becomes as you practice.
It's important to understand the orderliness of this.
The first thing to go away is wrong view.
In the beginning, it's wrong.
You go away because you have faith.
I believe in what the Buddha taught.
And that gives you enough of confidence to cultivate right thought,
right intention.
But once you hit the Ariyamanga in the noble path,
wrong views cut on.
So never again will you think you're never doubt.
Is it right to get attached to things?
Is it right to desire after anything in the world?
You won't have that thought.
You won't have that view.
But you'll still desire those things.
You'll still get angry.
And these things will still come up.
You'll feel awful afterwards.
You'll see you.
The Buddha mentions three types of Samhavitaka.
I'm sorry.
Samhavitaka, I'm using the wrong transition.
Samhavitaka.
I'm mixing up my translations here.
The Pali word is not Vitaka.
It's the same thing, but Samhavitaka.
Samhavitit, Samhavitaka.
See what it teaches you.
Samhavitaka means the same thing, right thought.
So the three kinds of rights are
Nikama, Samhavitaka.
They're also called Vidaka, Samhaviras.
Nikama, Samhavitaka, Samhavitaka, Samhavitaka.
Nikama is the inclination to
pronounce, to give up the inclination to let go.
Abhayapada, Sankapa, is the inclination to not get angry.
In connection not to hate people or hate things.
And our wings are Sankapa is the inclination not to be cruel.
And I'm still not clear exactly the difference between Abhayapada and Abhayingsa,
but it's clear by Abhayapada and Mihingsa.
But Abhayapada means meta.
And the commentary says that not hating people,
not being angry towards people, is having love.
You're opposite of hate.
And non-cruelty is cut on its compassion.
So we're starting to think how that differs.
How it would differ to be angry at someone
and to be cruel towards someone.
And I think there are cases where it's different.
Sometimes you just have disdain or lack of compassion for someone.
You don't hate them.
But you don't care that they're suffering, right?
It doesn't bother you, it doesn't concern you.
It doesn't move you to stop what you're doing.
So that's Mihingsa.
You do something that is cruel, which is distorted, right?
Because we have this inclination not to suffer.
We wouldn't ever want someone to, it sounds cliche,
we wouldn't ever want someone to do that to us.
And it sounds like, well, that's just a nice philosophy.
But it actually, it makes sense.
You're not inclined towards these states for yourself.
It's not something that you think of,
hey, that's a good idea.
Let's hurt.
Let's be hurt.
Let's suffer.
The idea of it is repulsive towards you.
So to have that on the one hand and on the other hand,
to be perfectly fine with it happening to someone else,
there's a disconnect there.
It doesn't, it may not seem like it.
It seems like a natural thing, of course.
Dog eat dog, everyone for themselves.
But the interesting thing is, is when you meditate
and meditation isn't on these things.
Meditate means when you're mindful.
When you learn how to see just ordinary experience as it is,
a remarkable thing happens.
You are inclined, it balances out.
It comes into focus.
And you lose this selfishness,
which is, it's not real.
It's not a natural state.
It's not the ultimate balanced state
to be only concerned about yourself.
A person who is objective has no cruelty.
They see their own suffering and someone else's suffering
really has the same thing.
They're not inclined towards either,
and they're inclined to be compassionate.
And the same with loving.
They're inclined to be loving.
They're not inclined to be angry towards others.
They don't hate others.
They don't hate themselves.
So it's not a lot to say.
It's an important teaching.
It's quite simple.
It's important to understand this distinction.
It's thought and right view.
Some meditate, some, some kappa.
A right view is our beliefs.
A right thought is actually our in practice.
What we inclined towards.
But they're related, of course.
As I said, our right inclination leads to right view.
Right view leads, of course, to, to right inclination.
They work together.
So this together is the wisdom.
This is a person who has these two.
It's considered to be wise.
This is the wisdom, quite simple.
No.
The wisdom, the portion, the full noble path.
So questions.
Practicing formal meditation is one of the first things
you do in the morning.
Be better than waiting until after breakfast or other walking
activities.
But it's the kind of thing you should be doing during your
waking activities.
Ideally.
I can see where you're going with that.
I don't like to answer such questions because life is not
something you can organize.
It shouldn't be.
It should be something that is, well, you have to learn about
all aspects of life.
And so if I said, this is good.
You wouldn't want to neglect that.
So meditate when you can.
If it's, if it's not, seems natural to meditate before you
eat or before you eat, is that what you're saying?
Before you do things, anything in the morning, then
grade.
It feels natural to do those things.
One thing I could say is the commentary does make mention of
the fact that for monks, most especially, or probably
mainly for monks, it's better to eat before you meditate
because they'd have some rice grow in the morning
because it calms your body down and it gives you some strength
and it actually makes it easier to meditate.
I don't, I don't think that's something to rely upon the
commentary often says these things.
It's easy to understand, but it's a good example.
It can be easier after you eat and that's a good thing.
But what about before you eat and when it's difficult?
Are you going to ignore the difficult state?
Are you going to only be inclined towards the good one?
So that's what I mean by, if I answer one way, then you neglect
the other way.
So it's best to be natural about it and to be in tune with
your rhythm and to see where you have problems that need to be
worked out and slowly adjust them.
Don't just work towards that, which is easier convenient.
I mean, so the point of better and worse, not really that
valid that you want to eventually be mindful all the time.
That's cool.
Okay, no name is asking lots of questions, but it appears that
this person is practicing a different school of meditation.
So I'd stop you there and suggest that you read my book on how
to meditate and I think I'm only inclined to answer meditation
questions about our tradition because, and you're asking
questions whether I've experienced such things and I'm not really
interested in answering questions about my own practice.
But if you ask, how should one deal with something or what is the
advice about fear?
I didn't invite you to read my booklet first because I'm going
to, if I'm going to answer that, I'm going to base it on what's in
the booklet, base it on our tradition and the answers are in there,
but I can elaborate on it if you like.
So yeah, the whole problem with fear, I mean, I totally sympathize,
but my advice to you because you're coming to me for advice is to
read my booklet.
It sounds self-serving and self-promoting, but I wrote it for a
reason and I didn't take it from my own teachings.
I copied everything from what I had learned.
So it's not really my book.
That's what I believe is the answer to things like fear.
In mindful of the five aggregates, I find experience leads to
volition and thought, feeling and body.
Would it be better to start with mindfulness of the body instead of
experience in order to cut craving attachment?
Well, it's better to start with a body probably for a different
reason.
I think you may be complicating things a little too much.
Well, the body is just gross in a sense.
It's coarse.
It's easy to find.
It's not easy to find.
It's like a dog chasing its tail sometimes.
But the body is fairly easy to find.
And the old text, say, to what extent the body becomes clear
and you become clearly aware of the body, to that extent the mind also
becomes clear.
So simply focusing on the body, you already start to, you know,
you will as a result begin to observe the mind.
And then you jump off from there, focus on the mind.
If you start to think about something, if you start to judge
something, you start to feel something.
What kind of meditation is best to do in the morning?
Mindfulness meditation.
It's the best meditation to do any time.
But you can also look up the talk I did on the four
protective meditations, I think it's called.
Other types of meditation or something.
We've given some talks on that.
There are four types of meditation that are supportive of
insight meditation.
Maybe someday I'll do a talk on each one of those.
I think people would be very interested in those.
Okay, so that's all the questions for tonight.
Thank you all for tuning in.
Thank you.
