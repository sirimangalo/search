1
00:00:00,000 --> 00:00:09,840
I could say that would give us eternal life, eternal youth, eternal health, eternal anything.

2
00:00:13,440 --> 00:00:16,880
So there you go, that's the number for tonight.

3
00:00:19,440 --> 00:00:26,240
I think Robin has not decided not to join tonight to define

4
00:00:26,240 --> 00:00:37,760
and sit in these shoes, acts above and beyond, so we'll give her a night off.

5
00:00:37,760 --> 00:00:45,200
But nonetheless, I have a bunch of questions, so I'll answer them.

6
00:00:45,200 --> 00:01:00,400
Michael, you should go.

7
00:01:08,800 --> 00:01:13,040
During sitting meditation of rising, falling in practice of daily mindfulness,

8
00:01:13,040 --> 00:01:17,760
I begin to hear and feel my heart be with more clarity, identifying it with a

9
00:01:17,760 --> 00:01:24,640
mantra heartbeat, heartbeat, heartbeat, just a correct practice, say it's not terrible,

10
00:01:24,640 --> 00:01:28,480
but the heart is just a concept, what you're really experiencing is a feeling,

11
00:01:29,120 --> 00:01:31,680
so to be more correct, you'd want to say feeling and feeling.

12
00:01:34,480 --> 00:01:37,120
You can also say something like beating, beating, but it's

13
00:01:37,120 --> 00:01:42,240
a feeling works just as well.

14
00:01:45,680 --> 00:01:49,280
The experience of sitting lingers for longer than one noting period,

15
00:01:49,280 --> 00:01:52,720
should I continue to note sitting until that one experience ceases,

16
00:01:53,920 --> 00:01:59,520
or instead do one noting of it and move on, even if the experience hasn't ceased on its own.

17
00:01:59,520 --> 00:02:09,520
Yeah, just do it once, just do it once and move on, oh, I didn't click, right,

18
00:02:09,520 --> 00:02:36,000
just a click, I didn't work anyway, it's not, it wouldn't be wrong, but it starts to get a little bit

19
00:02:36,000 --> 00:02:46,160
ad hoc, a bit rough to stick to the program, it wouldn't worry if there's still a potential

20
00:02:46,160 --> 00:02:52,880
to focus on it more, just say sitting once and then move on to the next one, but that's for this,

21
00:02:52,880 --> 00:02:54,960
that specific technique that we give to you.

22
00:02:54,960 --> 00:03:09,360
When noting more steps and walking meditation is a better to slow our pace in order to make

23
00:03:09,360 --> 00:03:14,640
the noting easier, or should we strive to speed up the noting to match the original pace,

24
00:03:15,360 --> 00:03:20,400
each note should, I mean, the step itself will take longer,

25
00:03:20,400 --> 00:03:25,680
sure, but each movement should be an ordinary speed, it shouldn't be too quick or too slow,

26
00:03:26,320 --> 00:03:32,880
but because there's more steps in it, and yeah, it will take longer, a little bit anyway.

27
00:03:39,680 --> 00:03:45,200
I see you use the internet quite frequently, our all Buddhist monks allowed to use the internet

28
00:03:45,200 --> 00:03:52,640
to reasonably extend, really depends on where you are, your situation, there's obviously no rule

29
00:03:52,640 --> 00:04:00,480
about using the internet though, I can understand the potential desire to circumscribe access to

30
00:04:00,480 --> 00:04:15,280
the internet, there's no question that it's problematic.

31
00:04:15,280 --> 00:04:19,760
Once the environment is realized, how can I be certain that I will be released from my birth cycle

32
00:04:19,760 --> 00:04:25,600
for good? I don't know the depressed state, but it's as though I don't want to come back here

33
00:04:25,600 --> 00:04:36,320
for another life. I mean, the cause of rebirth is craving, the benefit of seeing nibana

34
00:04:36,320 --> 00:04:43,360
is it reduces your craving, as you've seen in true peace, and so you crave less and less

35
00:04:43,360 --> 00:04:54,480
and you're more objective and neutral, and so that's how you know that you're not going to be

36
00:04:54,480 --> 00:05:02,320
reborn is because you have no more craving left, no more desire.

37
00:05:07,920 --> 00:05:11,760
What is the difference between realization, awareness, and judgment? How do you know that

38
00:05:11,760 --> 00:05:15,360
what you're saying to yourself when meditate is not a judgment of your experience?

39
00:05:19,040 --> 00:05:28,240
Well, judgment would be adding something to it. If you feel pain and you say to yourself pain,

40
00:05:28,240 --> 00:05:33,520
pain, you're not adding anything to it, it is pain. Of course, if you say to yourself pain,

41
00:05:33,520 --> 00:05:37,120
pain, you're adding the dislike to it. What you're saying is this is bad,

42
00:05:37,120 --> 00:05:44,160
you're not saying it verbally, but you're cultivating that. There doesn't need to be a certain

43
00:05:44,160 --> 00:05:50,800
quality to your noting. It has to be neutral, but if you were to sit there and say bad, bad,

44
00:05:50,800 --> 00:06:15,280
well, that would be a judgment. A couple of questions there that I'm not going to bother posting

45
00:06:15,280 --> 00:06:23,680
or adding, but yeah, the questions here are meant to be about our tradition and for the benefit

46
00:06:23,680 --> 00:06:32,640
of people practicing our tradition. So the ideas are about our meditation practice or some

47
00:06:32,640 --> 00:06:43,600
general Buddhist principles. I'm happy to discuss to some extent, but yes, speculative or other

48
00:06:43,600 --> 00:06:51,040
sorts of questions, questions, what other types of practice? Not really the purpose. Anyway,

49
00:06:52,080 --> 00:07:01,920
hope that was useful. This giving of Dhamma, a little bit of a talk every night. It's good for

50
00:07:01,920 --> 00:07:07,680
the meditators to hear something and to give them a reason and remind them of why they're doing it.

51
00:07:07,680 --> 00:07:17,680
So thank you all for tuning in, wish to hear your all good practice. Have a good night.

