1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhammapana.

2
00:00:05,000 --> 00:00:14,000
It's more we continue on with first number 75, which reads as follows.

3
00:00:14,000 --> 00:00:21,000
Aānhyaandai, laabupanisa aṇaṇy min-barṬanga amini".

4
00:00:21,000 --> 00:00:33,000
A wa-me- incumb-abir-ayāya, b-um-ud-dasa ssāvā casaka, rana- beat, na-d vengeance.

5
00:00:33,000 --> 00:00:41,800
We welcome Man gardening Brohaha Titan, which means

6
00:00:41,800 --> 00:00:50,360
the path, or that which is a means to gain, to gain.

7
00:00:50,360 --> 00:00:54,760
That which is a means for gain, love that.

8
00:00:54,760 --> 00:01:01,760
That which is a means for gain, and that which leads to nibana.

9
00:01:01,760 --> 00:01:05,040
These two are Anya, meaning they are different.

10
00:01:05,040 --> 00:01:07,120
They are not the same.

11
00:01:07,120 --> 00:01:12,120
One way is that they are other than each other.

12
00:01:12,120 --> 00:01:18,120
They are apart from each other.

13
00:01:18,120 --> 00:01:27,120
Ava Mehtang Abhinya, Abhinya, for someone who understands this fully,

14
00:01:27,120 --> 00:01:33,120
or in an ultimate sense, deeply.

15
00:01:33,120 --> 00:01:39,120
Abiku, who is a Sabaka, a student of the Buddha,

16
00:01:39,120 --> 00:01:51,120
should not be overjoyed by Sakara, by worship or respect or offerings.

17
00:01:51,120 --> 00:01:55,120
Should not by gains.

18
00:01:55,120 --> 00:02:00,120
By that in that which one, by wealth and so on.

19
00:02:00,120 --> 00:02:09,120
Thinking about Vivekam Anubruhahi, thinking about or resolute upon Vivekah,

20
00:02:09,120 --> 00:02:12,120
which means seclusion.

21
00:02:12,120 --> 00:02:16,120
A person who is a student of the Buddha should not be inclined towards gain,

22
00:02:16,120 --> 00:02:21,120
or fame, or wealth, or so on.

23
00:02:21,120 --> 00:02:26,120
They should be inclined towards seclusion.

24
00:02:26,120 --> 00:02:32,120
The meaning and why these two are put together is because of the story,

25
00:02:32,120 --> 00:02:44,120
which has to do with Anubis, who has this obvious choice between great gain and wealth

26
00:02:44,120 --> 00:02:52,120
and prosperity in worldly sense, and chooses to leave it behind for the forest.

27
00:02:52,120 --> 00:03:00,120
The story goes that there was an ancient Brahmin in the time of the Buddha.

28
00:03:00,120 --> 00:03:11,120
The religion was Brahminism, and there were these priests who would oversee ritual ceremonies.

29
00:03:11,120 --> 00:03:17,120
And so this was a very poor Brahmin agent, and maybe forgetful anyway.

30
00:03:17,120 --> 00:03:23,120
Not very well-known, and maybe in a place where I guess with the advent of Buddhism,

31
00:03:23,120 --> 00:03:31,120
there wasn't much for the Brahmin's to do because people weren't all interested in rituals and ceremonies anymore.

32
00:03:31,120 --> 00:03:43,120
So he never had much food or much wealth, but he had great faith in Sariputa, the Buddha's chief disciple.

33
00:03:43,120 --> 00:03:50,120
And so Sariputa would go for alms to this Brahmin's house, but the Brahmin had nothing to give him.

34
00:03:50,120 --> 00:04:00,120
And so when you knew that Sariputa was coming, he would hide and avoid the elder out of embarrassment that he had nothing to give.

35
00:04:00,120 --> 00:04:09,120
And time and again, he would do this when he knew that Sariputa was coming, and then one day he was able to obtain

36
00:04:09,120 --> 00:04:15,120
a single portion of rice gruel and a piece of cloth.

37
00:04:15,120 --> 00:04:25,120
And immediately he thought that he had done some ceremony that had gotten this as a gift or payment for his services.

38
00:04:25,120 --> 00:04:34,120
And so he immediately thought that this would make a great offering for the elder who he hadn't been able to support.

39
00:04:34,120 --> 00:04:44,120
And so when the elder came to his adore, he leapt at the opportunity and started pouring the rice gruel into the elder's bowl.

40
00:04:44,120 --> 00:04:55,120
And he got halfway done and the elder covered his bowl up, signifying that he didn't want to take all the food from the Brahmin to leave some for him to eat as well.

41
00:04:55,120 --> 00:05:08,120
But the Brahmin refused and said, listen, my wish for this is to be happy in the next lie. I'm giving this for the purpose of my benefit, even at the expense of my well-being right now.

42
00:05:08,120 --> 00:05:11,120
So he was going to go hungry without eating.

43
00:05:11,120 --> 00:05:31,120
And so he pushed the elder to accept the entire meal, and then he gave him the piece of cloth, and he said through the power of this offering, may I obtain the realization, the same realization that you have gained when I'm sorry.

44
00:05:31,120 --> 00:05:44,120
And sorry, put a set a wrong hole to, may it be thus, may I believe that's what he said.

45
00:05:44,120 --> 00:05:58,120
And went on this way. Now when the Brahmin died out of his great reverence and respect for Sariput, he was born in the womb, sorry put to one of Sariput as chief female disciples.

46
00:05:58,120 --> 00:06:13,120
And they say during the time of her pregnancy, she had these strange cravings. They say pregnant people will have cravings for this or that.

47
00:06:13,120 --> 00:06:21,120
And I guess the understanding is that it has something to do or the belief I would ever have something to do with the child, the nature of the child.

48
00:06:21,120 --> 00:06:30,120
So they say she had cravings to give offerings to the monks all the time and go to listen to the dhamma. She had these strange cravings anyway.

49
00:06:30,120 --> 00:06:59,120
This boy is born and they bring him to the elder and the other names in Tissa. And he has such great karma or merit or goodness that he's stocked up just from this one act given with a pure heart and the commentary remarks that there's something special about giving when you're hard up yourself.

50
00:06:59,120 --> 00:07:10,120
There's a quote, Jack London, I think. He said, rich people don't know how to give. When a rich person gives, they don't really understand giving.

51
00:07:10,120 --> 00:07:18,120
They don't have a sense of what it means to really give. But a poor person understands what need is and therefore is able to give.

52
00:07:18,120 --> 00:07:37,120
And so there's generally a much higher sense of the import of the need. So as a result of this great wholesomeness that he cultivated just with the one act of sacrificing his own meal and his own possession to cloth.

53
00:07:37,120 --> 00:07:48,120
He was reborn with great merit. And when he went later on, he became an novice. When he was, I guess, seven years old, sorry, put to ordain him.

54
00:07:48,120 --> 00:08:04,120
And when he went for arms food, he would get hundreds of people coming to offer him food and so they called him and then he would give all this excess food that he got from the people.

55
00:08:04,120 --> 00:08:19,120
He would give it out to all the monks. And so they called him the gist of the arms giver because he was always going around giving arms to all the other monks who didn't award as lucky to have as much support as he did.

56
00:08:19,120 --> 00:08:31,120
And so that was his name for a while. And then in the winter, he got really cold and he noticed that the monks were without blankets and the monks were cold or huddling around fires and he said,

57
00:08:31,120 --> 00:08:38,120
well, why are you rubbing yourselves and why are you warming yourself by the fire? He said, well, it's winter, it's cold.

58
00:08:38,120 --> 00:08:46,120
And he said, well, why don't you just get a blanket? And the monks said, why is he easy for you to say he of great merit?

59
00:08:46,120 --> 00:08:53,120
He made me, it's easy for you to get a blanket but not easy for us. And he said, well, in that case, come with me.

60
00:08:53,120 --> 00:09:05,120
And so he rounded up all the monks and he took them into the, into sovereignty and immediately people came and gave him blankets and so he got hundreds and hundreds of blankets.

61
00:09:05,120 --> 00:09:12,120
And so then they called him the gist of the blanket giver and that was his name for a while.

62
00:09:12,120 --> 00:09:21,120
And this went on and he was able to obtain whatever he wanted but he was still quite unsatisfied.

63
00:09:21,120 --> 00:09:33,120
And he realized that if he were to stay in Saavati, he would never accomplish the goal of the holy life because there were too many visitors, too many of his relatives and people coming to see him.

64
00:09:33,120 --> 00:09:38,120
So he ran away and went off to live in the forest.

65
00:09:38,120 --> 00:09:45,120
And then he got a new name and so his new name and so by this time he had three names according to the commentary.

66
00:09:45,120 --> 00:09:52,120
His third name was Tissa the forest. So they had all these different names and they were trying to figure out which one to call him.

67
00:09:52,120 --> 00:09:57,120
Anyway, in the end he was called Tissa the forest dweller.

68
00:09:57,120 --> 00:10:14,120
But the story goes that while he was in the forest, he would say, whenever he came to see the people, he stayed so much to himself that he would never say much to the lay people.

69
00:10:14,120 --> 00:10:25,120
So this is sort of a common trait especially for monks intent upon attaining realization.

70
00:10:25,120 --> 00:10:33,120
He would just say, Suki Hoda, Dukkapamoochata, Dukkapamoochata.

71
00:10:33,120 --> 00:10:40,120
May you be happy, may you be well and may you be free from suffering.

72
00:10:40,120 --> 00:10:44,120
That was all he would say every day, same thing, same thing.

73
00:10:44,120 --> 00:10:50,120
And so they got started to get the feeling that this guy was, and this was all he was good for him.

74
00:10:50,120 --> 00:10:54,120
But you know, they were happy to take care of him.

75
00:10:54,120 --> 00:10:59,120
And then one day his preceptor, Sari Bhutta, came with a whole bunch of other monks.

76
00:10:59,120 --> 00:11:08,120
Actually they say he came with all the senior elder monks because this novice was a sort of a very special person.

77
00:11:08,120 --> 00:11:11,120
And then the Bhutta came.

78
00:11:11,120 --> 00:11:20,120
And anyway, the only point here is that at one point they wanted to ask him, they asked the Bhutta to give a talk.

79
00:11:20,120 --> 00:11:22,120
It was either the Bhutta or Sari Bhutta.

80
00:11:22,120 --> 00:11:25,120
I think maybe Sari Bhutta actually.

81
00:11:25,120 --> 00:11:27,120
Anyway, asked to give a talk.

82
00:11:27,120 --> 00:11:31,120
So they turned to Tisa and said, okay, Tisa, you give the talk.

83
00:11:31,120 --> 00:11:34,120
And the people were like, what's he going to say?

84
00:11:34,120 --> 00:11:40,120
This guy can't teach. He's never taught since the day he came to live in this forest.

85
00:11:40,120 --> 00:11:46,120
And then he gets up on the damasid and he gives a long and wonderful sermon.

86
00:11:46,120 --> 00:11:54,120
And everyone's a little bit confused and wondering why half the people are kind of upset because, right, it was just with Sari Bhutta.

87
00:11:54,120 --> 00:11:56,120
The Bhutta hadn't come yet.

88
00:11:56,120 --> 00:12:02,120
And half the people were upset because he's like, well, why is he staying here just quiet by himself

89
00:12:02,120 --> 00:12:05,120
and not talking to anyone, not teaching?

90
00:12:05,120 --> 00:12:07,120
When he can teach like this, why didn't he?

91
00:12:07,120 --> 00:12:09,120
So half the people were upset.

92
00:12:09,120 --> 00:12:14,120
At the other half of the people were, were overjoyed that they had such a wonderful monk staying with them.

93
00:12:14,120 --> 00:12:16,120
They hadn't realized what a wonderful monk he was.

94
00:12:16,120 --> 00:12:23,120
So people had divided as according to their character types.

95
00:12:23,120 --> 00:12:25,120
And then the Bhutta came.

96
00:12:25,120 --> 00:12:30,120
Because the Bhutta saw that this was going to be a problem and so he came to sort it out.

97
00:12:30,120 --> 00:12:38,120
And he pointed out how great it was that they had such a monk as there's someone who was intent upon a solitude.

98
00:12:38,120 --> 00:12:47,120
And as a result of his great qualities and his great devotion to the practice,

99
00:12:47,120 --> 00:12:56,120
he pointed out how all these great monks then came to visit him and pointed out to the lay people how lucky they were as a result.

100
00:12:56,120 --> 00:13:06,120
So this is the story of Tissa. As usual, it's sort of long and there's many aspects to it that don't quite relate to the verse.

101
00:13:06,120 --> 00:13:13,120
But the verse came about because the monks were amazed that Tissa was able to do this.

102
00:13:13,120 --> 00:13:19,120
For many of them, there wasn't a choice to live in such hardship.

103
00:13:19,120 --> 00:13:28,120
But for Tissa, he had relatives and friends and people who were devoted to him.

104
00:13:28,120 --> 00:13:34,120
And so if he had stayed in Sabati, he could have been gotten lots of almswud or blankets or whatever he wanted.

105
00:13:34,120 --> 00:13:48,120
And he had these other names for a reason because he was very full of great merit and was always able to live in luxury and get whatever he wanted all the time.

106
00:13:48,120 --> 00:13:52,120
He said, it's a difficult thing that he's done to go off in the forest and live in hardship.

107
00:13:52,120 --> 00:13:55,120
And the Buddha heard them and asked what they were talking about.

108
00:13:55,120 --> 00:14:00,120
And when they told them, he said, indeed, he's done a difficult thing.

109
00:14:00,120 --> 00:14:09,120
And then he told this verse. He said, there are two very different paths to choose from the path of Satkara,

110
00:14:09,120 --> 00:14:21,120
gain and fame and affluence and pleasure and the path to freedom.

111
00:14:21,120 --> 00:14:27,120
It's an important point and this is the point for our practice.

112
00:14:27,120 --> 00:14:32,120
It's a claim that we make that happiness doesn't come from gain.

113
00:14:32,120 --> 00:14:35,120
It doesn't come from getting what you want.

114
00:14:35,120 --> 00:14:42,120
It doesn't come from pleasure. It doesn't come from worldly pursuits.

115
00:14:42,120 --> 00:14:44,120
There are two different paths.

116
00:14:44,120 --> 00:14:49,120
The paths of gain and the path to new banda.

117
00:14:49,120 --> 00:14:56,120
When one sees this and the word the Buddha uses abhinaya, which abhinaya is like in a higher sense.

118
00:14:56,120 --> 00:14:58,120
So not just knowing this.

119
00:14:58,120 --> 00:15:00,120
It's not about knowing this intellectually.

120
00:15:00,120 --> 00:15:05,120
Because I think for those of us who are at least somewhat interested in spirituality,

121
00:15:05,120 --> 00:15:12,120
we all know this, that intellectually that we're never going to be satisfied.

122
00:15:12,120 --> 00:15:17,120
We've seen this. We've seen that we get and get and get and never satisfies us.

123
00:15:17,120 --> 00:15:21,120
But this isn't what the Buddha points out here.

124
00:15:21,120 --> 00:15:25,120
What he's talking about is a higher realization.

125
00:15:25,120 --> 00:15:28,120
You have to realize it for yourself.

126
00:15:28,120 --> 00:15:32,120
So in our practice, it's not about judging or prejudice.

127
00:15:32,120 --> 00:15:43,120
It's about observing and realizing and seeing how our wanting for specific experiences,

128
00:15:43,120 --> 00:15:55,120
wanting to feel pleasure, wanting to feel calm, wanting to see this or that or experience this or experience that.

129
00:15:55,120 --> 00:15:58,120
Seeing that this isn't the path, this doesn't lead us to happiness.

130
00:15:58,120 --> 00:16:00,120
It doesn't satisfy it.

131
00:16:00,120 --> 00:16:04,120
Seeking these things out actually leads to attachment to them.

132
00:16:04,120 --> 00:16:07,120
So we'll get a good experience in our practice.

133
00:16:07,120 --> 00:16:12,120
And then when we don't get it the next time we're frustrated and upset.

134
00:16:12,120 --> 00:16:16,120
And we see that this is like a microcosm for life.

135
00:16:16,120 --> 00:16:29,120
And by experiencing it firsthand how this pattern of wanting, getting and then expecting and being frustrated,

136
00:16:29,120 --> 00:16:35,120
it starts to change the way we look at the world from a very basic foundational level.

137
00:16:35,120 --> 00:16:40,120
And as a result that informs our whole life, when we take that out into the world,

138
00:16:40,120 --> 00:16:46,120
when wanting arises, we're not as quick to leap and to chase.

139
00:16:46,120 --> 00:16:51,120
Because we see that, we've seen firsthand the nature of it.

140
00:16:51,120 --> 00:17:00,120
We've watched and observed on a very moment, moment level, in a very basic level.

141
00:17:00,120 --> 00:17:05,120
So someone who sees this sort of person is a true disciple of the Buddha.

142
00:17:05,120 --> 00:17:10,120
And they give up and they are not delighted by gain.

143
00:17:10,120 --> 00:17:13,120
Again, it's not about not gaining anything.

144
00:17:13,120 --> 00:17:19,120
Although there's something to be said for living a very simple life.

145
00:17:19,120 --> 00:17:27,120
And obviously this, this novice did a great thing by living in life of hardship.

146
00:17:27,120 --> 00:17:33,120
There's often nothing better for the practice than to live without anything,

147
00:17:33,120 --> 00:17:39,120
to live maybe even without a place to live, to stay at the foot of a tree,

148
00:17:39,120 --> 00:17:47,120
to allow yourself to be flexible with whatever comes,

149
00:17:47,120 --> 00:17:53,120
eating only one meal a day, eating perhaps, very little some days.

150
00:17:53,120 --> 00:18:00,120
And putting up with insects and putting up with hardships,

151
00:18:00,120 --> 00:18:04,120
all sorts.

152
00:18:04,120 --> 00:18:09,120
But the point is not to be attached and not to be delighted by these things.

153
00:18:09,120 --> 00:18:16,120
So for many of us, this living often, living in the forest, isn't

154
00:18:16,120 --> 00:18:20,120
inconvenient to say the least.

155
00:18:20,120 --> 00:18:25,120
And on the other hand, we aren't gain, we aren't getting Sakara.

156
00:18:25,120 --> 00:18:28,120
Sakara means donations and gifts,

157
00:18:28,120 --> 00:18:31,120
and religious people might get because people have faith in them.

158
00:18:31,120 --> 00:18:38,120
So for many of us, this isn't exactly, you can't fit it exactly.

159
00:18:38,120 --> 00:18:44,120
But the word that we use is we wake someone who is resolute on seclusion.

160
00:18:44,120 --> 00:18:46,120
And there are three kinds of seclusion.

161
00:18:46,120 --> 00:18:48,120
And they don't really have anything to do with the forest.

162
00:18:48,120 --> 00:18:52,120
But the point of seclusion isn't really the hardship.

163
00:18:52,120 --> 00:18:56,120
It's that we don't involve ourselves in society.

164
00:18:56,120 --> 00:18:59,120
Because societies where all of the gain comes from,

165
00:18:59,120 --> 00:19:03,120
it's where all of our pleasure comes from.

166
00:19:03,120 --> 00:19:06,120
That's where we become intoxicated.

167
00:19:06,120 --> 00:19:12,120
And by with people, with sights and sounds and smells and tastes that are exotic.

168
00:19:12,120 --> 00:19:15,120
So seclusion, first is physical seclusion.

169
00:19:15,120 --> 00:19:19,120
Physical seclusion means taking yourself away from the busyness,

170
00:19:19,120 --> 00:19:24,120
away from those things that are going to entice you are going to overwhelm you.

171
00:19:24,120 --> 00:19:28,120
This is useful especially for calming the mind.

172
00:19:28,120 --> 00:19:35,120
It's not as useful for understanding or as important.

173
00:19:35,120 --> 00:19:39,120
But very much in the beginning, it can be necessary.

174
00:19:39,120 --> 00:19:41,120
Even for insight meditation.

175
00:19:41,120 --> 00:19:47,120
You have to seclude your body in order to begin to come to terms with reality.

176
00:19:47,120 --> 00:19:50,120
Some people will say you shouldn't seclude yourself.

177
00:19:50,120 --> 00:19:55,120
You should just be mindful in your daily life.

178
00:19:55,120 --> 00:19:57,120
Because that's real.

179
00:19:57,120 --> 00:20:00,120
If you take yourself away, you're just running away.

180
00:20:00,120 --> 00:20:01,120
But it's not really like that.

181
00:20:01,120 --> 00:20:03,120
Because again, we're looking at a microcosm.

182
00:20:03,120 --> 00:20:07,120
We're looking at the very fundamentals of reality.

183
00:20:07,120 --> 00:20:10,120
And those are not people, places or things.

184
00:20:10,120 --> 00:20:14,120
They're seeing, hearing, smelling, tasting, feeling, thinking.

185
00:20:14,120 --> 00:20:18,120
The problem with being in society and doing this is that we're overwhelmed.

186
00:20:18,120 --> 00:20:22,120
And we're much quicker to react and interact.

187
00:20:22,120 --> 00:20:25,120
Then we are to observe.

188
00:20:25,120 --> 00:20:29,120
So in order to observe, we have to turn the faucet off.

189
00:20:29,120 --> 00:20:35,120
Turn the water and the metaphorical source of our experiences off.

190
00:20:35,120 --> 00:20:38,120
And just turn it on just a little bit.

191
00:20:38,120 --> 00:20:39,120
One by one.

192
00:20:39,120 --> 00:20:43,120
And so this is why we start walking because we're walking back and forth

193
00:20:43,120 --> 00:20:45,120
and sitting watching the stomach.

194
00:20:45,120 --> 00:20:48,120
Because we're trying to do something very simple.

195
00:20:48,120 --> 00:20:55,120
To give a very basic appreciation of what does it mean to experience?

196
00:20:55,120 --> 00:20:57,120
How are the different ways we can experience something?

197
00:20:57,120 --> 00:20:59,120
What does it mean to experience one thing?

198
00:20:59,120 --> 00:21:03,120
What does it mean to react with anger and all these things?

199
00:21:03,120 --> 00:21:06,120
You see them one by one.

200
00:21:06,120 --> 00:21:09,120
And through our seclusion.

201
00:21:09,120 --> 00:21:11,120
The second seclusion is mental seclusion.

202
00:21:11,120 --> 00:21:17,120
When we seclude ourselves from the hindrances, liking, disliking, drowsiness,

203
00:21:17,120 --> 00:21:18,120
distraction, doubt.

204
00:21:18,120 --> 00:21:21,120
So we do this through the practice.

205
00:21:21,120 --> 00:21:24,120
And this can be done theoretically, can be done anywhere.

206
00:21:24,120 --> 00:21:30,120
But again, it is much easier when you're in a secluded place.

207
00:21:30,120 --> 00:21:36,120
And finally, seclusion from the second one is seclusion of the mind.

208
00:21:36,120 --> 00:21:41,120
So the mind that is secluded, but the third one is seclusion of the

209
00:21:41,120 --> 00:21:46,120
filements, or you could say seclusion from a rising seclusion

210
00:21:46,120 --> 00:21:55,120
from unwholesome tendencies, which is nibana or freedom from suffering.

211
00:21:55,120 --> 00:22:00,120
So the key is to live in these three seclusions.

212
00:22:00,120 --> 00:22:04,120
And so for lay people, there is this point that has to be made that we

213
00:22:04,120 --> 00:22:09,120
should be careful not to be overwhelmed by society or caught up by society.

214
00:22:09,120 --> 00:22:13,120
This was the decision that this came to.

215
00:22:13,120 --> 00:22:18,120
And that he was surrounded by people, people who are friendly and

216
00:22:18,120 --> 00:22:20,120
helpful and wonderful people.

217
00:22:20,120 --> 00:22:25,120
But he realized that he wouldn't be able to go the next step to rise

218
00:22:25,120 --> 00:22:32,120
above this worldly state, which can be quite pleasant at times.

219
00:22:32,120 --> 00:22:41,120
But in the end, ultimately unsatisfying.

220
00:22:41,120 --> 00:22:47,120
And so even for lay people, we can make this decision to take the

221
00:22:47,120 --> 00:22:52,120
time to find a place in our house that is quiet or leave the house

222
00:22:52,120 --> 00:23:00,120
to go practice outside or to go to a monastery or a meditation center

223
00:23:00,120 --> 00:23:02,120
from time to time.

224
00:23:02,120 --> 00:23:10,120
Even just to close our room and sit quietly alone.

225
00:23:10,120 --> 00:23:13,120
Avoiding the sort of thing that the Buddha is talking about.

226
00:23:13,120 --> 00:23:15,120
He was Sakara.

227
00:23:15,120 --> 00:23:21,120
Sakara is a specific word having to do with offerings.

228
00:23:21,120 --> 00:23:25,120
But the point is any kind of gain.

229
00:23:25,120 --> 00:23:30,120
So not getting intoxicated.

230
00:23:30,120 --> 00:23:32,120
And the thing that keeps us in society.

231
00:23:32,120 --> 00:23:35,120
One of the things that keeps us is our attachment to things,

232
00:23:35,120 --> 00:23:42,120
to getting new possessions in our car, in our house,

233
00:23:42,120 --> 00:23:47,120
money and relationships and all of these things,

234
00:23:47,120 --> 00:23:50,120
and become obsessed with them.

235
00:23:50,120 --> 00:23:53,120
All of this is in the same category.

236
00:23:53,120 --> 00:23:56,120
All of the things that we shouldn't be delighted with,

237
00:23:56,120 --> 00:24:00,120
shouldn't get caught up with, kind of by.

238
00:24:00,120 --> 00:24:03,120
So another simple teaching.

239
00:24:03,120 --> 00:24:09,120
But again, quite powerful, making a firm,

240
00:24:09,120 --> 00:24:19,120
taking a firm stand on the idea that you can dedicate yourself.

241
00:24:19,120 --> 00:24:22,120
You can consider yourself to be fully following the Buddha's teaching

242
00:24:22,120 --> 00:24:29,120
and still indulge and enjoy and get caught up in sensuality.

243
00:24:29,120 --> 00:24:34,120
So we have this war inside.

244
00:24:34,120 --> 00:24:37,120
And we understand that we have to come to

245
00:24:37,120 --> 00:24:39,120
as they are not compatible.

246
00:24:39,120 --> 00:24:44,120
One way is the way that leads to attaining the things

247
00:24:44,120 --> 00:24:45,120
that we desire.

248
00:24:45,120 --> 00:24:48,120
And the other way is the way to be free.

249
00:24:48,120 --> 00:24:51,120
To be free from desire and free from suffering.

250
00:24:51,120 --> 00:24:54,120
To be truly at peace with ourselves.

251
00:24:54,120 --> 00:24:57,120
We have to accept it because it's often

252
00:24:57,120 --> 00:25:00,120
people will often try to take the middle way,

253
00:25:00,120 --> 00:25:03,120
which is really having your cake and eating it too.

254
00:25:03,120 --> 00:25:06,120
You want to practice meditation and become enlightened,

255
00:25:06,120 --> 00:25:08,120
but you don't want to give anything up.

256
00:25:08,120 --> 00:25:10,120
You don't want to let go.

257
00:25:10,120 --> 00:25:12,120
It's like wanting to fly while you're still clinging,

258
00:25:12,120 --> 00:25:15,120
while the bird is still clinging onto the tree.

259
00:25:15,120 --> 00:25:18,120
Wanting to hold onto the tree and still fly,

260
00:25:18,120 --> 00:25:21,120
so we have to do one with the other.

261
00:25:21,120 --> 00:25:23,120
So that's the teaching for today.

262
00:25:23,120 --> 00:25:25,120
Thank you all for tuning in.

263
00:25:25,120 --> 00:25:49,120
See you all next time.

