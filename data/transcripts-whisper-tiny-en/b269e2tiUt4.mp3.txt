Hello and welcome back to our study of the Damapada. Today we continue on with
the first number 116 which reads as follows. A bit carried to Kalyani, papa jiptan
nivari, dandhanhi karotau puyinyan, papas mingramati mano, which means one should be exceedingly
swift in doing good things and in papa jiptan nivari in preventing or
restraining the mind into evil. So both in doing good and then restraining the
mind from doing evil, one should be exceedingly swift, should make haste for
he dandhan karotau puyinyan, for one who does good deeds slowly, papas mingramati mano, the
mind delights in evil. So like the English saying idol hands or the devil's
devil's workshop or so, if you don't make haste in doing good your mind will
delight in evil instead. This was told in regards to a rather touching story of
a brahman called aikasataka. Aikas, he was called aikasataka, that's how he's
remembered anyway, I'm pretty sure that wasn't his name, but aikasataka is
robe and aikasataka is cloth, aikas one. So he had one cloth, why was he called
that? Because the pertinent feature of this story is he and his wife each had
one lower robe to themselves, meaning so each one of them had a sort of like a
skirt to wear, which would have been common clothing to wear in the time in
India, but among the two of them, between the two of them, they only had one
upper robe, which would have been something like this that I'm wearing, which
should be something to cover their chest. And because it was considered
a proper to when you go in public to wear an upper robe and a lower robe and an
upper robe, they couldn't go out together. And when one of them went out for
the market or when they went to the monastery to listen to religious
discourses, when they went to listen to the Buddha or other teachers teaching,
one of them had to stay at home. They were exceedingly poor. They were so poor
that they only had one upper robe between the two of them. So it became sort of
rather important item in their family. Like this robe was all that was
allowing one of them to go out and the fact that they didn't have two of them
was clearly, if they could have afforded a second one, they certainly would have
tried to get a second one because it was causing great hardship. So one day
there was an announcement that there would be the Buddha would be giving
a preaching and be a special dhamma discourse. And these people were lucky enough
to be born in a time when there was a Buddha and they were lucky enough to be
born in a place during the time when there was a Buddha in a place where the
Buddha was teaching. They were lucky enough to be born as humans and they were
lucky enough. It seems to have actual interest in hearing what the Buddha said.
But then there was the problem. Mother was going to be teaching during the day
and teaching during the night that they both couldn't go to hear all the
teaching. So they had to take turns. And so he asked his wife, what will we do?
And that's why I said, I'll go during the day. And she took the robe and the upper
robe and had her lower robe on and went out and listened to the teaching. By
the time she came back it was getting on in the evening and she handed over the
upper robe and he put it on this brahmana kasatakat, put on his one robe and made
his way to the monastery and sat in the back and began to listen to the number.
And as he listened to the dhamma it is said that in a past life he was a follower
of the Buddha Deepankara. I know the Buddha weepasi, right? One of the Buddhas in
times gone by. And so it hit him rather strongly this teaching. It was like a
recognition of something familiar. And he felt himself just becoming ecstatic
with joy and rapture and faith. So he wasn't enlightened. He hadn't really come to
see the Buddha's teaching for himself. But he was just overwhelmed by the
greatness of this teaching. And as we often are, when we hear spiritual
teachings, you know, when you read wise teachings or when you listen to
talks about wise things, when you read the Buddha's teaching or hear talks
about it, often it's quite overwhelming how how impressive it is. And when we
hear about Sati Patanasut or when we hear about the Buddha's teaching on the
Eightfold Noble Paths or these kind of things and we see how pure it is. It gives
us, it makes us excited. It brings great faith and great encouragement.
And so he got this real sense of faith and devotion to the Buddha, which is
useful. You know, faith and devotion are useful qualities of mind because they
give you strength and power. The problem isn't with the qualities. The problem
is with how they're used and whether they are directed by wisdom. They're not
sufficient, of course, to bring about enlightenment, but they do give
encouragement. So this is why in all religions people with faith are able to do
great things in the sense of powerful things. They're able to support their
religion. They're able to religion is therefore able to build great
structures and some of the greatest architecture and most profound feats. The
pyramids, much of this is much much of the architecture and history has been
due to religion. If you go to any Buddhist country or Hindu country in India,
they're carving temples out of solid rock. So they would carve a temple out of a
single block of a cliff, roof and all, and carve the inside out. So great deeds
can be done with things like faith and devotion are not to be taken lightly.
But they're dangerous because, of course, with faith and devotion, you can kill,
you can cause terror and fear and suffering to others as well.
Of course, when two people believe very different things, it causes conflict, war
and soul. So it's powerful, it's the point. And so he gained this great sense of
strength, the inner strength. And it made him what, excuse me, it made him
think the almost unthinkable. He wanted to give a gift. He wanted to do
something out of appreciation and not just appreciation, really, as a
spiritual practice, because giving is a spiritual practice. And for lay people,
for people who are living in the world and have to work, I mean, probably he
had a job that he had to do that took up most of his time and obviously
didn't pay very well. And so as something he could do to better himself, he
wanted to give something out. He wanted to give something as a gift to the
Buddha. This was his idea of a religious practice. So the question is, what was
he going to give? Well, he obviously couldn't give his lower road because that
would make him indecent. He had no money, he had nothing but these two pieces of
cloth. And cloth, of course, was quite valuable at that time. So it was
considered an obvious choice of a gift. But all he had was this one, this one
upper road, but it was really the only thing he could give. But it was even
worse to give that because not only did he depend upon this upper road to keep
him to keep societal, educator, decorum. His wife also depended on it. And so
he had this dilemma in his mind because he thought, yes, I'll give my I should
give this piece of cloth because it's it's relatively valuable. And certainly
the Buddha could put it to use. But then he would thought this ridiculous. How
could I even think such a thing? It's not only I needed my wife also. And so he
had this one good thought. But then he had about a thousand, he had about a
thousand thoughts against it. He decided not to do it. He was sitting there
listening the first first part of the night. And he suppressed this desire to
give. And he sat back down. What a ridiculous idea. But then he continued to
listen to the Buddha's teaching and it just came to him that he really wanted to
do something. It seems like he wasn't really was really prepared to do any
meditation or develop himself spiritually. But this is often how it is. One of
the first spiritual practices for this reason is giving. Because when
people just that their minds aren't ready or they're there, they don't have
the understanding of what it means to meditate or why it would be useful to
meditate. When people here religious teachings, and if they're not yet inclined
to say, hey, I should practice that, I should put it into practice. Because
they don't have the requisite development that makes them think that. They're
mind instead. And it's just a basic spiritual practice. And clients were
giving. And if the theory is that through giving their mind lightens up and
they come to see the difference between clinging and letting go. And they
start to see how the mind is clinging. And they start to realize it's not really
about giving gifts. It's about the nature of your mind. Giving gifts is nice
because it helps you become a better person, but really. And that's what we then
lead them. So it's a gateway spiritual practice. You can say that leads people
onto the idea of higher spiritual practices. They realize that true giving is
giving up. And giving up really has nothing directly to do with an object. It
has much more to do with the mind. And the mind of course is something that can
be trained. And it's cultivated based on habits. And so then you start to
listen deeper to the Buddha's teaching. But it seems he wasn't able to do
that because he didn't become enlightened through any of this. And it's as
often as the case with the suit does they or with these stories, they tend
to leave the person behind and don't really talk about what would happen to
him. But the implication is that he didn't it seems he didn't ever become
enlightened through this. But then the less it was it was incredibly impressive
this that that he even had this thought. And then in the second watch of the
night, the second part of the night, the thought came up again because still
the Buddha was teaching and there's just such an incredible teaching and
he could feel how profound it was. And so the thought came up again and again
he was fighting. It was warring inside of him. This conflict should I give
shouldn't they give? And still he couldn't bring himself to give. With a good reason
for most of us this is really a no brainer. I think you can't. It's not proper.
But if you put himself put yourself in his place as someone who believed
that this was a good thing that giving was good and who was in such a state
that they had no other recourse to perform good deeds. He felt or it seemed to
him that there was very little he could do to do good deeds. So it was it was
either or it was either do this or resign yourself to not having a
spiritual practice to not doing anything of any benefit to anyone. Of course
it's actually not true. There are many ways that you can help other people
or how you can cultivate spiritual practice and so on. But I don't mean to look
down upon his his intention. It's actually quite impressive for most of us. We
would be too afraid to do such a thing. And I don't think it's wrong that he had
this idea. I don't think it would have been wrong to give. I mean you could argue
as many people have that giving beyond your means giving to the point that
it's uncomfortable or especially where it affects others. Like this was
not only going to affect him it was going to affect his wife. That there's
an argument to be made that it's improper and it's improper for people for
the monks for example to accept such gifts or to encourage such gifts. But you
know dealing with that's that's really a cerebral argument dealing with
ultimate reality and the incredible sacrifice of someone giving away
something so dear to them. Something so important to them. I mean putting aside
the fact that it may not be it wasn't only his and he didn't really have a
right to give it away. He should have actually or you could argue that he should
have consulted with his wife first because it was also hers. If you put that
aside it was pretty awesome that he came up with this idea anyway. It was a
good thing maybe not incredibly awesome but we had a great intention here and
yet he still didn't get recently but in the third watch the third part of
the night as the night was drawing to a close and soon it would be morning and
he would have to go home. Finally the warring of these two thoughts desire to
give and the fear of being without and of the consequences or the the
difficulties. The attachment to this cloth and the concern that he had
maybe you could say it was a bit of ego and he was concerned with how it would
look if he had to walk home and if he had to give up his only possession and
to walk home looking like a sort of like a beggar who didn't even have an
upper rope. Finally near the end of the night he stood up, pulled off his upper
robe and placed the dead the feet of the Buddha and then he shouted I have
conquered I have conquered and he said it three times I have conquered I have
conquered I have conquered and it so happened that in the crowd also listening
to the Buddha's teaching was the King, King the Sanadikosala who was the King of
the realm of sawati one of the big one of the great cities in the time of
the Buddha where the Buddha was staying and of course the kings don't like to
hear this from their subjects I have conquered I have conquered and so I'm
that's my understanding is that it was quite disconcerting to him to hear
someone in the crowd say I have conquered it's not really what a king wants to
hear and so he had someone to go find out who that person is and what they're
talking about what have they conquered and the King's men went to the
Brahmin and asked him what are you talking about what's this and he explained
he explained what happened and so they went back to the King and when the king
had heard it heard what he'd actually done that he had given over to the
Buddha his his one main possession in the world the King said it was a hard
thing to do with the Brahmin Dean that was not an easy thing he was impressed
and as these stories tend to go home and and really as reality tends to I mean
not not maybe as magnificently as this but good deeds are are quite often
rewarded especially when they're done without any thought of reward and it
turns out this Brahmin was a pretty awesome guy all-in-all and all and the King
was it wasn't bad either the King said decided that he would do a kindness
to do it something of kindness for this Brahmin who had so selflessly
sacrificed something that was really quite important to and so he gave two
pieces of cloth one for the wife and one for the Brahmin he has just men to get
two pieces of fine cloth probably quite expensive cloth to give so that this
man wouldn't have to go without without cloth but the Brahmin was on a roll so he
took these two pieces of cloth and offered them to the Buddha as well it seems
he had become quite attached to this idea of giving
well attachment or not but he became quite he had felt how great it was and
what a relief it was really I mean that's one of the things about giving
it's finally he didn't have this stress in his mind it was just give give
give up give up and once he had given up and and son to the bottom where he
had literally nothing like really one one robe just to cover his nakedness
you know how what else can go wrong so he gave both of the two robes to the
Buddha and the king saw this and he was impressed and so he gave him four
pieces of cloth and again the man gave this is kind of like a test well let's
see what happens if I give him for it or certainly his greed will take over
right but no he gave all four pieces of cloth to the Buddha so as the king
continued gave him eight pieces of cloth and again I guess that the guy gave
all eight pieces of cloth to the Buddha he gave him 16 pieces of cloth and gave
all 16 pieces of cloth to the Buddha finally the king was the king was impressed
enough he had had his men give them give over 32 pieces of cloth but this time
he said he ordered him as king keep two pieces of cloth for yourself you can
give the rest away if you like give one one for your wife and one for you and so
the man not wanting to go against the king did that and and kept the two for
himself on top of this the king very impressed by what by this brahman he had two
expensive blankets maybe they were made of silk or some kind of fur or
something something very very expensive and gave them gave two blankets to
the brahman and you know thinking well this would be some very precious gift of
course cloth at that time as I said was a valuable commodity especially fine
cloth and so he gave these as a token of his respect for this man one for him
and one for his wife but again the brahman he didn't say anything about
having to keep them so again the brahman took one of the cloths and offered it
to be put above the Buddha's bed as a canopy there's of course at that time
they wouldn't have had the same kind of building that we'd have so there would
be spiders and bugs and and even ticks and all sorts of dust and so on so you
needed the canopy over your bed to to block that out so we had this rich
blanket placed over the Buddha's bed in in the Gandhukudi and the Kudti and Jita
one and the other one he put as a canopy above where he offered arms so it
seems that he was offering arms every day as well and so when monks would come
for arms they would have a place and this blanket served as a canopy so he
he had completely given himself over to giving this was this had become his
spiritual practice and then one day the king was visiting the Buddha and he
saw this blanket being used as a canopy and it looked familiar and he said he
asked one of the monks or one of the attendants in the monastery who is who
gave that piece of cloth where did that come from and they said oh that came
for me cassette I gave it and he remembered he said I gave him that cloth and he
just shook his head and he was he was impressed beyond me on himself and so he
decided at that moment you know I have to do something really substantial
for this guy this guy should not be without this is someone who could do
good things with wealth and so he gave him what is called the gift of force for
elephants four horses four thousand gold coins four women four female slaves
and four most excellent villages that's apparently what they gave in those
times slaves were a thing though the Buddha doesn't seem to look very
favorably upon it upon them you know he wasn't really into this whole
social justice or change like people think he was seems to have tried very
much or not tried but seem to have stayed fairly much out of political and
social causes a lot of people tried to claim he was all for equality I mean
he really was in the sense of allowing anyone even outcasts to become
Buddhist monks that's a very very difficult a very radical thing to do at the
time outcasts were considered worthless or or inferior little better than
animals and the Buddha let them become spiritual monks we become spiritual
leaders but yeah anyway just it was a thing at the time so men had many wives
and they even had concubines as I understand so he got four female slaves king
percented he wasn't really sort of the best example of humanity he I mean he
wasn't enlightened they say that vicinity I think was going to become a
Buddha at some point in the future so he's in it for the long haul and he's so
he did sometimes did things and had policies and so on that were less than
stellar but nonetheless the point is the king gave this man great gifts that's
really the point to be taken from this the gift of force for many different
good things and so the monks as they were want to do began to talk about this
one day in the Hall of Dhamma saying how wonderful was this deed of
julas of aikasataka and it's wonderful that that right away it was repaid and
that within a very short time he was given this incredible gift this gift of
force and the teacher heard and and or would teacher walked in and asked them
what they were talking about and they told they told the Buddha and the
Buddha said it's not really all that spectacular he said the truth is he gave it
the very end of the night and if he had given in the second watch of the night
the second part of the night he would have gotten the power of it the power of
that gift would have gotten would have given much greater rewards the gift of
eight they said if he had given in the first watch of the night it would have
given the gift of sixteen so apparently that's I mean the Buddha is saying
that there is some kind of and many of these stories do tend to imply that
there's a kind of a strong power that's almost magical that out that causes
very quick results I think there's something about giving to someone like
the Buddha that's considered to be a great benefit I think you can
vote for this sort of thing most of us can't I mean if you give to someone who
is just an ordinary person say a person on the street who you know they're
in hard times what's great that you give to them there's no question but who you
could say isn't it isn't a great person great in the sense of really great like
a person a profound or a person who has a deep thoughts or does great things
or is beneficial to humanity that kind of thing you don't get to see the
results quickly at all beyond how they affect your mind because it'll make
you happy to believe help someone but it's not going to change the world it's
not going to bring about great things but if you give to someone who is
doing great things who is a great person if you support them and if you
revere them and so on there's kind of there's a magnification of an order of
magnitude greater because of the power of that person I remember when I was
living in Thailand reading a lot of stories like this and a lot of teachings
about charity and so on living as a monk and I was going on almost around
every day and as I've said I've mentioned the same many days I wouldn't get
very much but part of my thought process was well that's you know that's part
of me I was very fairly stingy as a person and fairly self-centered and
concerned only with my own benefit and grew up being worried about money and
not ever thinking about helping others I remember when I was a teenager we
was walking with a friend of mine and she saw a homeless person and
immediately went into a McDonald's to get him a hamburger and I didn't even
notice him and if I had noticed him I wouldn't have had any real inclination
to give him anything but she ran away without without hesitation and I wasn't
that sort of person so I with all these readings I was doing and sort of the
philosophy and the religious you could say religious belief that I had at the
time I was okay with that I was okay with not getting much but at the same time I
had the idea that there was something wrong with my outlook that I had to
do something about and I had to cultivate this intention to give and so I was
reading one book about the perfections about how someone should really
practice like jog this this idea of charity it's actually a religious
practice we studied in the Musudu Mungets one of the recollections when a
person recollects on their charity on their their goodness of giving it's
something that calms the mind it can actually lead to an absorbed meditative
state and it was talking about how when you go for arms which I would do
whether you get enough or not you should give the best of what you get to
someone else to the monks to older monks senior monks you know wise monks in
enlightened months and so what I started doing was even when I didn't get
enough food I would give the best I would take the best put it on the golden
colored tray and offer it to a Gentile my teacher and so it was just usually
just a little bit maybe some peanuts because he apparently had a he was
apparently it was something that he would eat and then sometimes it was a
piece of fruit but anything I got good and this was such a spiritual practice
you see because of course I was craving all these things and I was excited
when I would be excited when I get them and I could recognize that greed in
myself so to have this practice of very much like a kasataka you know this
so these are the things that that are important for me and then to give them
away you know and to watch how your mind deals with that and actually gets
kind of excited about it and free about it like oh and I I I'm free from
that desire free from that that addiction and you really you can feel it
it's not it's not a hardship at all it was actually quite quite fun and I
continued to do this and I watched as the as my situation changed and of
course it changed for other reasons you could say as I became more well-known
but within actually a fairly short time I would go to my teacher and I I came
up one day with a large golden tray covered in fruit and and drinks like
soy milk soy and drinks and and all sorts of the best of the best and it was
still this wonderful practice where I would eat what was enough to keep me
alive and what was you know healthy at that point but all the good stuff all of
the best stuff went on this tray and it was it became a daily ritual of mine
after receiving food has gives myself to give it all away all the good stuff
away it was just wonderful because I would look oh there's a mango I got a
mango oh no it's going on the tray and I offered it to the hint and one day I
went up and and he said this is almost round because it was quite impressive at
that point he said you got that on the home side I said oh and then he said
this is this is this is this is Bounya this is goodness it's because of
goodness or something I can't remember exactly what he said he may have said
something else I did that every day every day every day and I have another funny
story that's related to that because I was doing it every day you would give
me a blessing every day and because I was going up to see him and sitting and
listening and learning how to teach at that time other people would come and
give gifts because giving gifts to great teachers was of course a big thing
and so they people would come on their birthday or or on when they just felt
like they had bad luck and they wanted to get some good luck they would give
a gift and so these people came up and as as was usual he said to these people
yes people what's the occasion is it your birthday and they said no it's
not my birthday and I came at the same time with my train to offer because
then he wouldn't have to give a blessing just to me and I would get in on the
blessing that they were going to give and he turned to me he said so to the
visit your birthday and said they think they said no or they said yes I can't
remember and they turned to me and he looked at me and he said he's
no is born every day he said and laughed and then he accepted my gift and that
was that so it was a you know it's a very wise thing to say not not in
regards to giving gifts every day but it's a there's double meaning though the
other meaning is of course we're all born every day and it was a very it was
kind of cute anyway so I can relate and I think you you have to it's not
just that which is easily to explain I think there is a power that's hard for
us to to comprehend even so that it seems kind of magical but the power of
powerful people of people who have profound minds and getting involved with
them doing good deeds with them for them really can have very swift consequences
so that's the idea here is that giving to someone like the Buddha is a very
powerful thing such that if he had if he had had a full mind and a full
intention meaning when he first thought of it if he had just said to himself
well then give you know I want to give then I should give then he would have
gotten it would have been a much more powerful act and he would have been
rewarded in kind so it might sound like there's this becomes like a
banking or an economic thing you know are all about getting right what can
you get but that's not the point the point is to talk about consequences and
to talk about a relative the relative nature of deans and then it's it's quite
interesting story for that reason because it points out the difference
between second guessing between doing something with a whole heart and doing
something will still second guessing or holding back and this applies not
just a giving it applies to meditation it applies to morality it applies to
all aspects of our spiritual practice when you help people are you doing it
out of ego are you doing it or just out of desire to get something in return
and so on many different ways in which you can do a good deed and can
cultivate goodness goodness of course if it's not obvious being the core of
Buddhism I think it's not always made obvious we have all these intellectuals
seeming teachings like the hateful noble path and the foreign noble truths but
really it all comes down to goodness the cultivation of Bunya the attainment
of nibana is it depends how you look at it but it can be understood as being
the highest goodness the greatest goodness super mundane goodness so the
Buddha then said this he said you should be swift in doing good don't second
guess yourself he said elsewhere as we'll learn or as we've already gone over
I can't remember I think maybe in this in this chapter no we'll have one
should don't not don't want to be quick in doing good deans one should be keen
to do good deans and sorry it's not in the double pile of it else for the Buddha
said mom because we by it that Bunya and I don't be afraid because of good
deeds don't be afraid monks don't be afraid of good deeds monks because
Sukasetang bikowai adiwachanang adi-dang biryani happiness is another word for
goodness goodness is another word for happiness and so how this relates to our
practice I think is is quite important in regards to being as I said whole
hearted about our practices in general and understanding about the nature of
the mind how important the nature of the mind is giving a gift it had nothing to
do with that cloth and this is an important story we tell in regards to the
difference between the object and the mind it's not really about the object he
gave something that was actually probably fairly low value the cloth was
probably not a fine cloth probably wasn't worth much at all in a person like
the king would have maybe used it probably wouldn't have even used it to
wipe his feet and yet this was the most prized possession that this man had
and giving that you know raised his his status in society greatly and was
a very important spiritual practice that probably uplifted him made him feel
confident and powerful and would have given him a great base for meditation if
he had actually used it for that we don't have any indication that he did but
it gives us this sort of understanding or this idea about where true goodness
lies it lies in the mind if your mind is hesitant and if you if you have
reservations about any of the spiritual practice that you do it can severely
debilitate so practicing meditation is the same it doesn't mean you have to
believe blindly but if you don't believe if you have doubts about what you're
doing it's important you get them cleared either that or that you recognize
them as being unreasonable doubts which is also possible not because we doubt
something it's not always a sign that what we're doing is wrong we can doubt
things that are not worthy of doubt that is possible so it's one of two ways
either we get our doubts cleared up or we quit what we're doing you know we
find a way to resolve our doubts or else we recognize the doubts as being
unwarranted invalid and we we meditate on them so often we'll tell
meditators to say to yourself doubting doubting just remind yourself it's
only doubt to arise as it ceases when it's gone you'll feel better as they
say okay if you doubt the practice then forget about the practice stop practicing
because you shouldn't do something if you're doubting and instead say to yourself
doubting doubting doubting let's say but but let's say if you do that try
that and I sometimes have even had them do it right there in front of me so try
that right now close your eyes there's an doubting closer eyes they think to
themselves doubting they say how do you feel now I'm saying oh yeah I feel
better and it's actually the point we're not teaching you anything else we're
teaching you exactly that that things like doubt and so on are stressful so
there's nothing really to doubt once you see that being free from doubt is much
better while then you and climb towards that which is free from that which
doesn't lead you to doubt and that really doesn't that fact that being free
from doubt is better is enough if you believe that then you have nothing to
doubt because that's really all it is the real key to being in enlightenment
being is being free from doubt and the only way you can do that is if you see
clearly you can't just believe so mind is the most important the mind comes
first as we learned in the very first verse mano pabangama dama mano saitama
and only and in the second verse when you do a good deed with a pure mind that
should simple when you do a good mind with an impure do a deed with an impure
mind suffering follows you so a very nice story very nice verse that's the
dama pada for tonight thank you all for tuning in wishing you all happy and
fruitful spiritual practice you will
