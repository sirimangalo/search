1
00:00:00,000 --> 00:00:16,000
Okay, this is a question that has come up many, many times and I've kind of just been waiting for someone to come here and ask it because kind of as a policy, I haven't been answering questions elsewhere.

2
00:00:16,000 --> 00:00:22,000
People ask questions all over the place and answering them all is just just unorganized.

3
00:00:22,000 --> 00:00:45,000
So I assume that if you're really, if you are not able to find the correct place to answer the question, then it can't have been very important or it's some subjective way of weeding out or filtering out the amount of questions.

4
00:00:45,000 --> 00:00:54,000
We've limited to questions last year, but this has been asked several times on several of my videos. I think I've actually done videos on it.

5
00:00:54,000 --> 00:01:01,000
I did a video on drugs, right, meditating on drugs, but I think I didn't answer the questions efficiently.

6
00:01:01,000 --> 00:01:09,000
The question is, I know that drugs are forbidden, Buddhist practice, but can they bring some form of clarity and understanding?

7
00:01:09,000 --> 00:01:16,000
Psychedelic drugs are said to break down barriers and bring an understanding of the world.

8
00:01:16,000 --> 00:01:38,000
Now, I happen to have done Psychedelic drugs before and if you haven't done Psychedelic drugs, here's the sort of experiences that I had lying on my back in a field and looking up at the sky and saying, the sky, the heavens is my father and down at the earth and the earth.

9
00:01:38,000 --> 00:01:43,000
And the earth was my mother and they were fighting over me.

10
00:01:43,000 --> 00:01:50,000
They weren't killing each other, but it was a struggle. Who was going to get me?

11
00:01:50,000 --> 00:01:59,000
Because here I was lying on the earth in between them both and so on and so on and it was just like this profound feeling.

12
00:01:59,000 --> 00:02:09,000
The point I'm getting at is, I'd question whether it actually brings clarity and understanding about the world.

13
00:02:09,000 --> 00:02:15,000
Another experience I had all these gods coming in and using me, one at a time they would come in.

14
00:02:15,000 --> 00:02:22,000
There was the god with the Viking hat and the were all sorts of gods coming in and they were using my body.

15
00:02:22,000 --> 00:02:28,000
They would control me and they had power over me. I was sitting in my room alone and I was like, oh, I hope they know what they're doing.

16
00:02:28,000 --> 00:02:35,000
Just watching them one by one come in and take control of my body.

17
00:02:35,000 --> 00:02:41,000
Another time I was crazy crazy stuff.

18
00:02:41,000 --> 00:02:46,000
And it's too bad Nagasena is in here because he did way more than I did.

19
00:02:46,000 --> 00:02:55,000
We've compared experiences in his way up there, but he told me some of his experiences.

20
00:02:55,000 --> 00:03:01,000
I think the point is made quite clear, it was made quite clear in a recent video I watched.

21
00:03:01,000 --> 00:03:07,000
Someone mentioned this video by Bruce Grace and I think it's his name, Grace and this is his last name.

22
00:03:07,000 --> 00:03:12,000
That he gave at the UN and it's really an interesting talk that reminded me of something.

23
00:03:12,000 --> 00:03:18,000
Just something he said about how the brain can actually be seen as a filter.

24
00:03:18,000 --> 00:03:30,000
Because the thing that he highlighted was the fact that people who have out of body experiences are able to relate that even when the brain was dead,

25
00:03:30,000 --> 00:03:35,000
for all intents and purposes, the brain was not working.

26
00:03:35,000 --> 00:03:46,000
There wasn't enough brain activity to allow for what neurophysicists, with neuroscientists, would consider required for active thought.

27
00:03:46,000 --> 00:03:55,000
And yet they would report that during that time they were actually able to think much clearer than when they were in their brain.

28
00:03:55,000 --> 00:04:06,000
There's another video by Dr. Forget, very famous video, probably some of you have seen it.

29
00:04:06,000 --> 00:04:11,000
The name is on the tip of my tongue, but this doctor had a stroke.

30
00:04:11,000 --> 00:04:19,000
And she was a neuroscientist, I think, and half of her brain wasn't working.

31
00:04:19,000 --> 00:04:32,000
And during that time, she felt totally objective and clear and this just clear awareness because the judgmental side of her brain wasn't working.

32
00:04:32,000 --> 00:04:45,000
So where am I going with this? So obviously, the psychedelics that I've had experience with are psilocybin, these mushrooms that you can chew and get this strange experience.

33
00:04:45,000 --> 00:04:48,000
This is when I was younger when I was in my teenage years.

34
00:04:48,000 --> 00:04:54,000
And from my experience, it changes the way the mind works.

35
00:04:54,000 --> 00:04:59,000
I'm sure someone else with more information and study could tell you more about exactly what it does.

36
00:04:59,000 --> 00:05:04,000
But from a first-hand experience, that's right, Jill Taylor.

37
00:05:04,000 --> 00:05:13,000
First-hand experience, point of view, they seem to give you a heightened state of awareness.

38
00:05:13,000 --> 00:05:22,000
But you can feel something going on here in the whatever this is called, the hypothalamus is that this is the third eye.

39
00:05:22,000 --> 00:05:33,000
And you can feel the drug working, or I could, anyway, you can feel this kind of a lightning bolt, a shock or something.

40
00:05:33,000 --> 00:05:40,000
So it's doing something to the brain that is giving you this different state of awareness.

41
00:05:40,000 --> 00:05:51,000
It's not on the level of a pure state of awareness, like Dr. Jill Taylor was talking about, or that Dr. Bryson was talking about.

42
00:05:51,000 --> 00:06:01,000
And so before we even talk about whether those kind of clear states of awareness are in themselves useful,

43
00:06:01,000 --> 00:06:13,000
I would suggest that experimenting with psychedelic drugs, because of their ability only to change the state of the brain,

44
00:06:13,000 --> 00:06:23,000
and not free ones totally from the filter of the brain, or the constraints of the brain.

45
00:06:23,000 --> 00:06:29,000
They, on the other hand, simply change the state of awareness.

46
00:06:29,000 --> 00:06:40,000
I would say it's highly dubious as to whether this is going to lead to true wisdom, because it has just as much potential to mess you up.

47
00:06:40,000 --> 00:06:48,000
The last time I did mushrooms, it really messed up my mind, and Nagasena was confirming it, that the same thing happened to him.

48
00:06:48,000 --> 00:06:53,000
You feel like your mind becomes disjoint from reality.

49
00:06:53,000 --> 00:06:59,000
There's like a gap. It's like a scar in your brain that keeps you back from reality.

50
00:06:59,000 --> 00:07:03,000
You see everything going on still, but you're less connected to it.

51
00:07:03,000 --> 00:07:13,000
So it has the potential to cause problems. After that, I was paranoid quite often, and then when I would be taking marijuana,

52
00:07:13,000 --> 00:07:23,000
or so on just simple plain drugs, I would have a paranoid, paranoid, would make me paranoid, and so on.

53
00:07:23,000 --> 00:07:35,000
So I would say there is an argument to be made for out-of-body experiences, as giving states of clarity, because they're based on pure awareness,

54
00:07:35,000 --> 00:07:38,000
genetic concentration that the Buddha recommended.

55
00:07:38,000 --> 00:07:49,000
But I would say there are an infinite number of states that could be similar to this pure state of awareness that is acquired by letting go of the body,

56
00:07:49,000 --> 00:07:58,000
that are not so pure, and can have anger, can have greed, can have, of course, delusion in them.

57
00:07:58,000 --> 00:08:11,000
Therefore, I would raise the argument against using psychedelic drugs, because you don't have any reassurance that they're not just going to lead you down the wrong path,

58
00:08:11,000 --> 00:08:18,000
whereas with an out-of-body experience, you at least have the assurance that you're free from the brain.

59
00:08:18,000 --> 00:08:30,000
So you could raise the argument that that leads to some form of right view and understanding about the conditional state of the brain and of physical human experience,

60
00:08:30,000 --> 00:08:37,000
and it'll teach you people about a body experience has changed their lives. Your death experiences will tell you they're not afraid to die anymore,

61
00:08:37,000 --> 00:08:46,000
and suddenly they know the difference between good and evil, and so on, many, many different things having seen their life flash before their eyes and so on.

62
00:08:46,000 --> 00:09:07,000
So that would be my take on that form of drugs. I would say no, it's really not. I don't see the argument and I think it falls into this category of experiences where we think because it's natural or because it's a part of the world,

63
00:09:07,000 --> 00:09:16,000
and it somehow has its place and its purpose. And of course, we have to remember the world doesn't have purpose. The universe doesn't have purpose. That's illogical.

64
00:09:16,000 --> 00:09:26,000
To say it has purpose has to constrain it to some subset of reality, for reality to be complete and total.

65
00:09:26,000 --> 00:09:42,000
There is no possibility for purpose, meaning anything can happen, you can go in any direction. So there's no reason to believe that psychedelic drugs have their place and their purpose and are actually leading to objective understanding of reality.

66
00:09:42,000 --> 00:10:11,000
I don't have experience on my own with those kinds of drugs other than as an observer of other people who took it. I remember that a long time ago I was directing a theater piece and two of the actors came to the rehearsal under the influence of drugs.

67
00:10:11,000 --> 00:10:25,000
And they stood there on the scene and they thought they are so brilliant. And they stood there like giggling idiots. I can't find better descriptions.

68
00:10:25,000 --> 00:10:47,000
There was nothing going on. They were not able to to act or to remember anything clearly. Yeah, everything that we had done so far in rehearsals was even lost. And they thought it's really great what they are doing.

69
00:10:47,000 --> 00:11:05,000
And that was just a misperception. And I think that is what is happening when you are under drugs. You have some perceptions, but it's not the perception of reality as it really is.

70
00:11:05,000 --> 00:11:27,000
It is reality under the influence of drugs. And that's not the truth. That's not the reality. So yeah, I don't think I just want to assure what Buntis said. That's not the way.

71
00:11:27,000 --> 00:11:38,000
I think that's a very good point. Because that's so the case you think you are brilliant. That's what happens even with alcohol. You think you are witty and charming.

72
00:11:38,000 --> 00:11:50,000
And of course with drugs you think what you are having is the profound experience. All of the examples I gave you think this is something profound.

73
00:11:50,000 --> 00:12:04,000
But if you tell it to someone else, you sound kind of dumb. But I would say another point this brings up is an argument against trying to find wisdom in things like astral travel.

74
00:12:04,000 --> 00:12:25,000
And my video and astral travel many people said, wow, this guy doesn't know what he is talking about because astral travel is great. And I would raise the same argument that Panyani raised against, raised against psychedelic drugs, raised it for out of body experiences, even jonic experiences.

75
00:12:25,000 --> 00:12:36,000
Because there are many yogis or meditators and other traditions who enter into these states of rapture and bliss and come away with all sorts of wrong views.

76
00:12:36,000 --> 00:12:49,000
As she said, it doesn't give you the awareness of reality, which is to break apart the experience into its component parts. All of those experiences that I talked about were all conceptual.

77
00:12:49,000 --> 00:12:58,000
Even out of body experiences, even the clarity of mind that you have the total and absolute feeling of oneness, everything is one and so on, it's all constant.

78
00:12:58,000 --> 00:13:07,000
It's all just a thought. When you have a thought we are one which is what often people have when they have these other body experiences, that's just a thought that you have.

79
00:13:07,000 --> 00:13:16,000
It's the same kind of thing that when Christians will meditate they have, wow, this is God. I'm in the love of God or I'm with Jesus or so on.

80
00:13:16,000 --> 00:13:22,000
It makes them very, very, very confident because they're looking for this already.

81
00:13:22,000 --> 00:13:26,000
Then they find it and they say, that must be this, this must be that.

82
00:13:26,000 --> 00:13:35,000
It doesn't bring you any closer to understanding reality. I can't imagine it doing that.

83
00:13:35,000 --> 00:13:44,000
The best you can say of it is that it helps you to understand the conceptual and the contrived nature of human experience, how it can be altered.

84
00:13:44,000 --> 00:13:51,000
These drugs can bring you out of the human state to some other crazy, far out state.

85
00:13:51,000 --> 00:13:55,000
So it helps to open your mind up at any rate.

86
00:13:55,000 --> 00:14:16,000
When you meditate and have a deep meditation, peaceful meditation and have some bliss, that's so far beyond all that you can experience with drugs.

87
00:14:16,000 --> 00:14:27,000
And something else I forgot to mention is that was the conclusion I came to with drugs, even when I was on the trip.

88
00:14:27,000 --> 00:14:33,000
And this is when I had no idea about Buddhism or meditation, but I said to myself, I know this as a drug.

89
00:14:33,000 --> 00:14:40,000
You can feel that it's just a chemical that's working its magic on your brain.

90
00:14:40,000 --> 00:14:46,000
And I said, this is great and this is wonderful, but this is just a drug. It's not real.

91
00:14:46,000 --> 00:14:50,000
And you can feel the contrived, the artificial, the chemical nature of it.

92
00:14:50,000 --> 00:14:56,000
And so I made a vow to myself that I would try to attain that without the drugs, you know, whatever.

93
00:14:56,000 --> 00:15:00,000
I mean, it wasn't a worthwhile, I wouldn't do that now.

94
00:15:00,000 --> 00:15:06,000
But that was the feeling that that came even when I was on them, that this isn't real.

95
00:15:06,000 --> 00:15:21,000
And so yeah, for sure what you gain in meditation is objective and empirically far beyond what you gain from drugs.

