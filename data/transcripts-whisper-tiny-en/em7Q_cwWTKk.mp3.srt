1
00:00:00,000 --> 00:00:10,000
In this video, I would talk about some of the ways in which meditation can be introduced and incorporated into one's daily life.

2
00:00:10,000 --> 00:00:18,000
It's important that we understand meditation is for the purpose of changing one's life for the better.

3
00:00:18,000 --> 00:00:26,000
It's not enough to think that meditation is something you'll take on as a hobby or as something you'll do on the weekend.

4
00:00:26,000 --> 00:00:30,000
It's not something which is likely to bring fruit that way.

5
00:00:30,000 --> 00:00:40,000
Meditation is for the purpose of changing one's life, making one's life more productive for oneself and more helpful for other people,

6
00:00:40,000 --> 00:00:45,000
and more happy and more free from worry and stress and trouble.

7
00:00:45,000 --> 00:00:53,000
But if we're going to affect such a change, it's important that we introduce certain concepts into our life,

8
00:00:53,000 --> 00:00:56,000
the first one is virtue.

9
00:00:56,000 --> 00:01:04,000
That if we're going to live a life with a clear mind and a pure mind and a pure heart,

10
00:01:04,000 --> 00:01:12,000
it's important that there be certain things which we abstain from partaking of as a rule.

11
00:01:12,000 --> 00:01:18,000
And so as a rule, traditionally when we practice meditation, we undertake five rules.

12
00:01:18,000 --> 00:01:28,000
These five rules are one. We undertake not to kill. And some mosquitoes are any living, breathing, being any animal life form,

13
00:01:28,000 --> 00:01:39,000
which we know is there and which we see as a life, or telling someone else or encouraging someone else to kill or to commit suicide, for instance.

14
00:01:39,000 --> 00:01:47,000
The second rule is not to steal. We undertake as a rule not to steal, not to take things which the owner has not given us permission to take.

15
00:01:47,000 --> 00:01:53,000
Number three, we undertake not to commit adultery or sexual misconduct of any sort.

16
00:01:53,000 --> 00:02:08,000
So having romantic or sexual involvement with other people's partners or spouses or people who are engaged or otherwise involved with another person.

17
00:02:08,000 --> 00:02:21,000
The fourth rule which we take is not to not to tell lies, not to say things in order to deceive other people in order to confuse people and bring them further away from truth.

18
00:02:21,000 --> 00:02:28,000
And the fifth rule we undertake is not to take drugs or alcohol. That as a rule we will not take things.

19
00:02:28,000 --> 00:02:37,000
We will not indulge in things which will be a cause to muddle the mind or to darken the mind or confuse the mind.

20
00:02:37,000 --> 00:02:56,000
Again, in meditation we are trying to create a clear thought. These five things are things which will cause the mind to become disturbed, upset, stressed, worried, afraid, angry, dark, mean, evil and so on.

21
00:02:56,000 --> 00:03:09,000
There are things which confuse the mind which take away the clarity of mind which we are trying to create.

22
00:03:09,000 --> 00:03:14,000
For a person who is seriously keen on having the practice of meditation change their lives for the better. These five things are important.

23
00:03:14,000 --> 00:03:23,000
Something important for that one should seriously undertake and try to live up to in their lives.

24
00:03:23,000 --> 00:03:34,000
This is the first important point. The second important point is how to incorporate the actual formal practice into one's daily life.

25
00:03:34,000 --> 00:03:53,000
This takes things like moderation. We have to be moderate in our sleeping. We moderate in our eating. We moderate in our partaking of entertainment or diversion of all sorts.

26
00:03:53,000 --> 00:04:01,000
Because in the practice of meditation of course we want to incorporate it into our daily routine.

27
00:04:01,000 --> 00:04:09,000
So ideally when we wake up in the morning preferably we wake up maybe a half an hour earlier or an hour earlier depending on how much time we have.

28
00:04:09,000 --> 00:04:25,000
If we have a short time even just a half an hour we will give us enough time to perform mindful frustration three times to walking meditation for ten minutes and do sitting meditation for ten minutes.

29
00:04:25,000 --> 00:04:43,000
Or go to our room a half an hour earlier before we go to sleep. Even that half an hour in the morning and in the evening is a way to incorporate the meditation into our life and to create some real hope that it could have an effect on our lives and could change our lives for the better.

30
00:04:43,000 --> 00:04:55,000
But this requires things of course this requires moderation and sleeping but it will also require things like moderation and eating. Not eating in excessive amounts of food or food which is

31
00:04:55,000 --> 00:05:16,000
un conducive for mental development, fatty foods or junk foods or so on. These are things which we have to keep in mind that we are moderate in our eating and eat light meals which will allow our minds to be fresh and to be bright instead of being lazy and slothful and full of mental torpor and so on.

32
00:05:16,000 --> 00:05:25,000
And we have to be moderate in our indulgence in how we engage in activities apart from meditation.

33
00:05:25,000 --> 00:05:33,000
So if we spend all of our time watching TV or watching movies or going out to bars or parties or listening to music.

34
00:05:33,000 --> 00:05:47,000
Well instead of doing all of these things to such an excessive level that they become a very real routine in our lives. We start to take some of these things out which we can see though we do over and over again.

35
00:05:47,000 --> 00:05:57,000
We can see that these things don't bring us peace and happiness. But in the end they leave us only with a state of discontent needing more and more ever and again more and more.

36
00:05:57,000 --> 00:06:07,000
Until finally we come to a crisis where we can't get the things that we want. And when we can't get enough of the things which we want this is which creates suffering.

37
00:06:07,000 --> 00:06:16,000
It creates a state of sadness and despair in our lives. So instead of letting it come to that point we take some of these things out and replace them with meditation.

38
00:06:16,000 --> 00:06:23,000
So here we have practice in the morning, practice in the evening. Now we start to incorporate into our daily life as well.

39
00:06:23,000 --> 00:06:35,000
If we can find time in the afternoon to take a break for another half an hour that would be exceptional. We can do it three times during the day in normal daily life.

40
00:06:35,000 --> 00:06:42,000
But even though we can't, we take some of these things which are divergence out of our life.

41
00:06:42,000 --> 00:07:00,000
And instead we find a little bit of time five minutes, ten minutes and take it to be simply peaceful and quiet and close our eyes and watch the stomach rising and falling or watch our thoughts and create the clear thought about these thoughts changing our mind, bringing our mind back into the focus of the present moment.

42
00:07:00,000 --> 00:07:06,000
And we say to ourselves rising, falling and so on as I've explained in the other videos.

43
00:07:06,000 --> 00:07:22,000
And we do this, the important thing to note here is we do this by taking out of our lives certain things which are useless, which are fruitless, which in the end are simple diversion, which in the end will bring us no lasting peace and happiness.

44
00:07:22,000 --> 00:07:29,000
They only create a sense of lust and desire and craving in our minds which can never truly be fulfilled.

45
00:07:29,000 --> 00:07:39,000
This is the second point. The third point which we have to keep in mind is that when people ask how many hours a day we should be practicing.

46
00:07:39,000 --> 00:07:48,000
Well the answer is how many hours you sleep, subtract that from 24 and that's how many hours a day you should be practicing.

47
00:07:48,000 --> 00:08:03,000
The meaning here is even though we're not doing the mindful frustration or the walking or the sitting, we should be practicing mindfulness. Our mind should be clear and we should have a clear thought about what we are doing, what we are saying, what we are thinking at all times.

48
00:08:03,000 --> 00:08:21,000
And we do this by keeping track of our four postures, standing, walking, sitting and lying down. When we're standing we say to ourselves standing, standing, when we walk, walking, walking, walking, when we sit, sitting, sitting, when we lie down, lying, lying.

49
00:08:21,000 --> 00:08:29,000
When we bend, bending, when we stretch, stretching, when we turn, turning, when we reach for something, reaching, grasping and so on.

50
00:08:29,000 --> 00:08:37,000
And the more we can incorporate this into our lives, the more likely it is that the meditation will bring about clarity of mind.

51
00:08:37,000 --> 00:08:47,000
And our minds will become pure, our hearts will become pure, and our lives will start to improve and develop in a spiritual, in a wholesome, in a peaceful way.

52
00:08:47,000 --> 00:09:00,000
And also, besides the four postures, keeping guard on the six senses, the eye, the ear, the nose, the tongue, the body and the heart. The heart is the mind.

53
00:09:00,000 --> 00:09:07,000
So the eye, the ear, the nose, the tongue, the body and the heart, or the mind, heart, mind.

54
00:09:07,000 --> 00:09:23,000
By guarding these six senses, we create a sense of constant mindfulness, so that when we see something instead of liking it, or grasping for it, or being upset by it, instead we're simply aware of it.

55
00:09:23,000 --> 00:09:33,000
Seeing, we see seeing, seeing, hearing, hearing, smelling, tasting, feeling, thinking, when we say these things to ourselves, seeing, seeing and so on, we create the state of clarity of mind.

56
00:09:33,000 --> 00:09:52,000
Altogether, these are the three things which will create a lasting impression on our lives, and can hope to change our lives for the better, and create a state of peace and happiness for us in this life, and up into the point where we pass away.

57
00:09:52,000 --> 00:09:54,000
That's all today, thank you.

