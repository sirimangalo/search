1
00:00:00,000 --> 00:00:06,640
If we go to a meditation course in Sri Lanka, do we meditate 24-7?

2
00:00:09,640 --> 00:00:14,640
No, no, I had I have an anecdote about this one

3
00:00:15,840 --> 00:00:17,840
Why I was teaching

4
00:00:17,840 --> 00:00:19,840
I was a substitute teacher

5
00:00:22,800 --> 00:00:29,000
In a in a modest in a meditation center and chain my ones I got the chance to sub for

6
00:00:29,000 --> 00:00:32,840
the head monk who teaches foreigners

7
00:00:35,800 --> 00:00:43,480
and I found out that his way of teaching was based on on another teacher's method

8
00:00:45,000 --> 00:00:49,560
under our head teacher who taught that this monk taught that

9
00:00:50,760 --> 00:00:54,200
you should do a certain number of hours a day

10
00:00:54,200 --> 00:00:57,720
so in the beginning it would be eight hours

11
00:00:58,520 --> 00:01:02,200
in the beginning I think it would be like six hours six hours of meditation a day

12
00:01:04,040 --> 00:01:07,800
then the next day or after a couple of days you'd say today two more hours

13
00:01:08,520 --> 00:01:10,520
so today eight hours of meditation

14
00:01:12,360 --> 00:01:17,240
and every and and they were instructed that every day they should count how many hours they actually

15
00:01:18,600 --> 00:01:20,280
meditated and

16
00:01:20,280 --> 00:01:26,360
report to the teacher how many hours they meditated so when I got there I found all these foreign meditators

17
00:01:28,840 --> 00:01:31,720
I have to say totally stressed out and

18
00:01:32,760 --> 00:01:37,560
I they're feeling proud of themselves because of how many because they made the number of hours

19
00:01:37,560 --> 00:01:41,800
that they were supposed to do whether it was eight, ten, twelve, fourteen, sixteen, whatever

20
00:01:41,800 --> 00:01:51,880
or else feeling depressed and upset because they didn't make the formal meditation

21
00:01:53,560 --> 00:01:56,040
they didn't make the number of hours of formal meditation

22
00:01:56,920 --> 00:02:02,200
so they they were supposed to do eight hours of formal meditation and they only did seven or

23
00:02:02,200 --> 00:02:09,880
they only did six or so so at first they were you know the the assistant was asking me how many

24
00:02:09,880 --> 00:02:12,680
hours you're going to give them how many hours you're going to get because she knew that you're

25
00:02:12,680 --> 00:02:17,240
supposed to give them hours and she was afraid that I was changing the

26
00:02:20,280 --> 00:02:25,480
changing the for the teaching format so she asked me how many hours you're going to give them

27
00:02:25,480 --> 00:02:31,160
today this guy had done eight hours I think and he said to me today I did the I was supposed

28
00:02:31,160 --> 00:02:37,240
to do eight hours and I did eight hours and you know he was clearly stressed about this

29
00:02:37,240 --> 00:02:44,600
no this was not easy for him eight hours isn't so difficult but he was having a lot of trouble

30
00:02:46,040 --> 00:02:53,320
so I looked them in the eye and I said very good today I want you to do 18 hours of meditation

31
00:02:54,440 --> 00:03:02,760
and and his eyes got so wide and the industry stress level is blood pressure must come through the

32
00:03:02,760 --> 00:03:14,920
roof and I smiled at him and I said to him here's what I want you to do when you get up in the

33
00:03:14,920 --> 00:03:22,440
morning I want you to I want you to sleep not more than six hours and as soon as you get up in

34
00:03:22,440 --> 00:03:30,200
the morning I want you to say lying by maybe rising falling if you notice the stomach when you

35
00:03:30,200 --> 00:03:36,360
sit up I want you to say sitting sitting when you want to sit up you can say wanting wanting

36
00:03:36,360 --> 00:03:42,680
when you stand up you can say wanting to stand wanting to stand when you go to the washroom walking

37
00:03:42,680 --> 00:03:47,560
walking when you brush your teeth brushing brushing when you eat your food chewing chewing

38
00:03:49,080 --> 00:03:55,560
I want you to be mindful like this throughout the day practice as much formal meditation as you can

39
00:03:55,560 --> 00:04:04,840
comfortably but don't tell me how many hours a day of formal meditation you did I want to know

40
00:04:05,800 --> 00:04:11,320
I want you to be mindful from the moment you wake up to the moment you fall asleep 18 hours later

41
00:04:12,200 --> 00:04:17,720
and it doesn't mean you're always mindful but it means you're practicing so that's meditation

42
00:04:17,720 --> 00:04:26,440
you're trying to be mindful for 18 hours and you so you have six hours of break for six hours

43
00:04:26,440 --> 00:04:32,280
you can sleep and during that time you're not meditating and he was actually quite relieved by this

44
00:04:32,280 --> 00:04:39,640
it was something quite doable the other thing it did was when I arrived there I had been there

45
00:04:39,640 --> 00:04:43,400
for some while before and I was watching the foreign meditators and they weren't meditating

46
00:04:43,400 --> 00:04:49,480
they would do their power meditation one hour walking whenever sitting but when they weren't

47
00:04:49,480 --> 00:04:58,280
doing the formal meditation and they would sit around chatting or they would wander around and

48
00:04:58,280 --> 00:05:07,720
look at the look at the the flora and the fauna and watch the novices playing games with each

49
00:05:07,720 --> 00:05:15,880
other and driving the tractors and so on the novices driving the tractors but they weren't

50
00:05:15,880 --> 00:05:23,000
being mindful and so I just kicked but when I got them to do this 18 hour thing suddenly

51
00:05:23,880 --> 00:05:29,640
it was you know the the assistant who looked after the foreigners he said yeah he said suddenly

52
00:05:29,640 --> 00:05:35,080
suddenly they're not they're not causing trouble anymore I don't have to watch them all the time

53
00:05:35,080 --> 00:05:39,160
because they're too busy trying to be mindful they don't have any time to talk

54
00:05:40,520 --> 00:05:45,880
and that was part of I think it was part of my instruction is that whenever you do you try to be

55
00:05:45,880 --> 00:05:50,680
mindful so this means not talking with your friends during that time that you're eating or even

56
00:05:50,680 --> 00:05:56,760
just sitting around trying to stay to yourself and try to so I coached them on on day to day

57
00:05:56,760 --> 00:06:07,000
mindful or moment to moment mindfulness that's much more important it's much more important how

58
00:06:07,000 --> 00:06:15,640
much of your your waking hours your mindful that doesn't really answer your question your question

59
00:06:15,640 --> 00:06:21,880
was whether it ever gets to 24-7 or the answer I have to give is that sometimes you do practice

60
00:06:21,880 --> 00:06:29,080
this way 24 hours a day I don't know about the seven part maybe for a few days you can do 24 hours

61
00:06:29,800 --> 00:06:36,280
where you're trying to be mindful you can sleep get down to four hours sleep or not sleep at all

62
00:06:37,400 --> 00:06:43,000
depends on the meditator as you as you go on it gets a lot easier and you get less interested in

63
00:06:43,000 --> 00:06:48,520
things like sleep there are monks that will go without sleeping for days weeks months on end

64
00:06:48,520 --> 00:06:56,200
because they're practicing meditation sometimes you fall asleep sitting as well if you practice

65
00:06:56,200 --> 00:07:01,320
all night you may not be awake all night you may be but you're trying so you can count it so you

66
00:07:01,320 --> 00:07:07,000
sit there and you fall asleep for half an hour an hour or so but you can consider that trying

67
00:07:07,000 --> 00:07:11,800
no it's a part of your meditative experience you're learning about your inability to control

68
00:07:11,800 --> 00:07:23,640
your mind and your body it just shuts off by itself so I think that should give you some fairly

69
00:07:23,640 --> 00:07:30,360
good background on how we information on how we approach this sort of question of how much you

70
00:07:30,360 --> 00:07:46,280
have to practice meditation and I hope that answered your question

