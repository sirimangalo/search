1
00:00:00,000 --> 00:00:09,500
This control and illusion by control, I mean controlling the mind, it just thoughts.

2
00:00:09,500 --> 00:00:15,000
So if we can't really control the thoughts, then there's only observance, right?

3
00:00:15,000 --> 00:00:18,000
Then we have a choice, isn't it?

4
00:00:18,000 --> 00:00:21,000
We have each choice.

5
00:00:21,000 --> 00:00:27,000
And I see it, I don't quite get the grammar there.

6
00:00:27,000 --> 00:00:30,000
Then we have this choice, all we have is choice.

7
00:00:30,000 --> 00:00:33,000
I think that's how exactly I would answer it.

8
00:00:33,000 --> 00:00:36,000
If that's what you're saying, all we have is choice.

9
00:00:36,000 --> 00:00:40,000
We don't have control, control is an illusion, but we have choice.

10
00:00:40,000 --> 00:00:47,000
We can choose every moment to do something, to respond in a certain way.

11
00:00:47,000 --> 00:00:48,000
That's how I see it.

12
00:00:48,000 --> 00:00:50,000
I don't know if it's exactly true or not.

13
00:00:50,000 --> 00:01:00,000
But this is the best way for a limited human mind to come to terms with reality,

14
00:01:00,000 --> 00:01:04,000
Buddhist reality, because we deny control.

15
00:01:04,000 --> 00:01:11,000
And it turns out, quite quickly, it turns out to be false at any way.

16
00:01:11,000 --> 00:01:15,000
And yet we're not robots, we're not automatons.

17
00:01:15,000 --> 00:01:23,000
So it seems proper to say, and it's very much based on what appears to be the truth,

18
00:01:23,000 --> 00:01:24,000
that we have choice.

19
00:01:24,000 --> 00:01:31,000
There is a choice that can be made at every moment, and that creates.

20
00:01:31,000 --> 00:01:36,000
So this is a Buddha called present karma, karma here and now,

21
00:01:36,000 --> 00:01:38,000
that you create here and now.

22
00:01:38,000 --> 00:01:39,000
Actually, I don't know if the Buddha called it.

23
00:01:39,000 --> 00:01:40,000
This is how we understand it.

24
00:01:40,000 --> 00:01:42,000
You create karma in the here and now.

25
00:01:42,000 --> 00:01:47,000
At every moment you have the opportunity to create karma, to do, to intend this,

26
00:01:47,000 --> 00:01:53,000
or intend that, or to not intend anything at all.

27
00:01:53,000 --> 00:01:58,000
So yeah, I would say there's not just observance.

28
00:01:58,000 --> 00:02:01,000
There's also intention and choice.

29
00:02:01,000 --> 00:02:04,000
So you can choose not to observe.

30
00:02:04,000 --> 00:02:06,000
You can choose to try to force things.

31
00:02:06,000 --> 00:02:10,000
You can choose to, in other words,

32
00:02:10,000 --> 00:02:12,000
can choose to deal with things in properly,

33
00:02:12,000 --> 00:02:15,000
can choose to misunderstand things and say,

34
00:02:15,000 --> 00:02:16,000
I'm going to control this.

35
00:02:16,000 --> 00:02:18,000
I'm going to turn off my anger.

36
00:02:18,000 --> 00:02:21,000
I'm going to grow a third arm and just so on,

37
00:02:21,000 --> 00:02:41,000
things that are impossible.

