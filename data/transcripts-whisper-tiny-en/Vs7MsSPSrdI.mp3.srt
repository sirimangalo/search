1
00:00:00,000 --> 00:00:02,560
Hi, welcome back to Ask a Monk.

2
00:00:02,560 --> 00:00:08,560
The next question is actually a series of questions as follows.

3
00:00:08,560 --> 00:00:10,520
Is there a purpose in life?

4
00:00:10,520 --> 00:00:13,280
If so, what should be one's purpose in life?

5
00:00:13,280 --> 00:00:14,960
What does life want from us?

6
00:00:14,960 --> 00:00:17,080
Do you know yours yet?

7
00:00:17,080 --> 00:00:19,880
A purpose in life?

8
00:00:19,880 --> 00:00:22,960
A single purpose in life?

9
00:00:22,960 --> 00:00:29,920
Well, first of all, another important thing to keep in mind and to take as

10
00:00:29,920 --> 00:00:36,600
you know, to understand that the outset before we delve into this, you know, obviously

11
00:00:36,600 --> 00:00:47,240
very profound and important question, is that there is no specific rhyme or reason to

12
00:00:47,240 --> 00:00:53,440
existence whereby you could say, or no grand scheme, I guess you could say, plan.

13
00:00:53,440 --> 00:00:56,640
Buddhism doesn't believe in such a one, and this is something that I would encourage

14
00:00:56,640 --> 00:01:04,840
people on this path to get through their heads, because coming from a

15
00:01:04,840 --> 00:01:12,120
deistic, a theistic background, we're always trained and even socially, culturally, and so

16
00:01:12,120 --> 00:01:20,360
on, ethically, to, you know, think in terms of a grand scheme and an ultimate outcome and

17
00:01:20,360 --> 00:01:26,280
ultimate goal and, you know, some sort of plan that the universe has for us.

18
00:01:26,280 --> 00:01:33,320
This doesn't exist, this couldn't exist, it's not, it's not possible, it is not even

19
00:01:33,320 --> 00:01:40,560
logically possible if you take it to its, to its ultimate outcome, there is no logical

20
00:01:40,560 --> 00:01:46,280
possibility for it to be an ultimate reality.

21
00:01:46,280 --> 00:01:53,280
You could, because sure there could be schemes and plans by even gods and angels and so

22
00:01:53,280 --> 00:02:00,560
on, but those are all subjective, and they're based on that entity that beings, whims

23
00:02:00,560 --> 00:02:05,880
and desires and so on, and they have no objective truth to them.

24
00:02:05,880 --> 00:02:13,600
So what we have, rather, is a fairly chaotic mess, and that's what we know the universe

25
00:02:13,600 --> 00:02:21,520
to be, sounds like fun, no, that's what we live in, I mean some people and beings and even

26
00:02:21,520 --> 00:02:28,840
groups of beings could even be, you know, angels and gods and their followers and their

27
00:02:28,840 --> 00:02:35,920
believers and so on, set up systems, set up, like the earth is a sort of a system that

28
00:02:35,920 --> 00:02:42,720
we've set up, you know, mostly unknowingly, although there are, you know, beings who perhaps

29
00:02:42,720 --> 00:02:48,240
sort of understand what's going on and are working to arrange things or organize things

30
00:02:48,240 --> 00:02:53,000
or lead things in a certain direction based on their whim or their understanding of what's

31
00:02:53,000 --> 00:02:54,000
right.

32
00:02:54,000 --> 00:02:58,000
There are human beings who are doing probably very rich and powerful people who are kind

33
00:02:58,000 --> 00:03:04,280
of scheming to send the world in the right direction by their estimation.

34
00:03:04,280 --> 00:03:14,440
But you know, these are all, these are all simply objective, or subjective, there's no

35
00:03:14,440 --> 00:03:25,160
objectivity to them, the truth of reality, it has laws, it has a nature, you know, there

36
00:03:25,160 --> 00:03:32,440
is a nature to reality and you know, you could say it's very complex and very convoluted

37
00:03:32,440 --> 00:03:36,640
chaotic and so on, but there is some sort of nature to it.

38
00:03:36,640 --> 00:03:42,720
For instance, there is the mind and there is awareness, there is experience.

39
00:03:42,720 --> 00:03:50,680
This is our reality, our universe and our existence.

40
00:03:50,680 --> 00:04:00,200
So our life is a series of experiences and it's based on our views, it's based on our desires,

41
00:04:00,200 --> 00:04:08,440
it's based on our versions, our attachments, our, you know, ideas of what is right and

42
00:04:08,440 --> 00:04:11,360
wrong and good and bad and so on.

43
00:04:11,360 --> 00:04:14,120
And this is what we understand to be our life.

44
00:04:14,120 --> 00:04:21,600
So the idea that there could be a single purpose in life is, I just don't want to mislead

45
00:04:21,600 --> 00:04:22,600
you.

46
00:04:22,600 --> 00:04:28,160
If I say, well, this is the purpose in life, then I'm really not telling you the truth

47
00:04:28,160 --> 00:04:37,040
because you can do whatever you want, really, you can do nothing and there is no, this

48
00:04:37,040 --> 00:04:43,680
is an important point that there is no divine supreme being who is going to punish you.

49
00:04:43,680 --> 00:04:50,880
There is no part of reality that is going to say you're wrong or you're doing it wrong.

50
00:04:50,880 --> 00:04:55,560
What's going to happen is based on the nature of the universe, your reality is going

51
00:04:55,560 --> 00:04:56,560
to change.

52
00:04:56,560 --> 00:05:05,200
It's going to change based directly on your actions and your reactions to experience.

53
00:05:05,200 --> 00:05:12,200
So I think this is an important wake up call for us that we don't become complacent in thinking,

54
00:05:12,200 --> 00:05:18,040
well, you know, it'll all work out in the end or if it happened it must happen for a

55
00:05:18,040 --> 00:05:19,920
reason or so on.

56
00:05:19,920 --> 00:05:29,480
These are all very comforting but false comforts because there is no safety net.

57
00:05:29,480 --> 00:05:37,640
There is no, on the one hand, there is no condemnation, you know, people can condemn you

58
00:05:37,640 --> 00:05:39,320
but what does that mean?

59
00:05:39,320 --> 00:05:46,800
There is also no free ride so there is no salvation.

60
00:05:46,800 --> 00:05:51,000
So you get what you reap what you sow.

61
00:05:51,000 --> 00:05:55,440
If you go in a certain direction it will lead you in that direction, it will lead you

62
00:05:55,440 --> 00:05:59,320
to a certain goal that will lead you to a certain result.

63
00:05:59,320 --> 00:06:07,920
So I would say that the closest thing you can come to a purpose in life is to have your

64
00:06:07,920 --> 00:06:21,720
intentions lead you to the goal or have your actions lead you to the intended goal.

65
00:06:21,720 --> 00:06:26,920
And what I mean by this is that for many people, even though there's a desire to become

66
00:06:26,920 --> 00:06:33,920
happy, their actions actually in actual fact lead them to suffering.

67
00:06:33,920 --> 00:06:40,600
So there are people who engage in immoral deeds, deeds that we would consider to be unethical.

68
00:06:40,600 --> 00:06:48,000
And as a result of those deeds, they attain to suffering, they come to a horrible state.

69
00:06:48,000 --> 00:06:53,280
For that reason we call them unethical immoral, it's not because some God or some divine

70
00:06:53,280 --> 00:07:01,160
being or even some board of humans have decided that or a body of humans, a government

71
00:07:01,160 --> 00:07:04,280
or so on has decided that it's wrong.

72
00:07:04,280 --> 00:07:07,040
It's wrong because it's going to create suffering.

73
00:07:07,040 --> 00:07:10,760
It's great suffering for yourself, it creates suffering for other beings.

74
00:07:10,760 --> 00:07:14,160
It's contrary to your intentions.

75
00:07:14,160 --> 00:07:21,280
A person who wants to be happy should act in a way that leads them to happiness.

76
00:07:21,280 --> 00:07:35,280
But the only real way to get to this state where your intentions lead you to the goal,

77
00:07:35,280 --> 00:07:40,640
to their intended goals, is where you engage in ethical and you engage in moral and

78
00:07:40,640 --> 00:07:52,600
you engage in wise and pure deeds and speech and thoughts, because these are in line

79
00:07:52,600 --> 00:07:55,000
with their own reality.

80
00:07:55,000 --> 00:07:58,840
They're not a confusion or a delusion.

81
00:07:58,840 --> 00:08:08,640
It's not out of touch with reality, it's not saying, you know, X is going to lead me to

82
00:08:08,640 --> 00:08:17,040
why and to think that something is going to lead you to something that it's not, to think

83
00:08:17,040 --> 00:08:22,080
that if you plant apple seeds that you're going to get orange trees, or if you plant orange

84
00:08:22,080 --> 00:08:28,760
you're going to get apple trees, to think that by creating suffering, by creating chaos,

85
00:08:28,760 --> 00:08:36,600
by creating upset that somehow you're going to create peace and happiness and stability

86
00:08:36,600 --> 00:08:43,000
and people who commit all sorts of unwholesome deeds and speech and engage in unwholesome

87
00:08:43,000 --> 00:08:48,720
thought, they often do it because they think that the result is going to be happiness

88
00:08:48,720 --> 00:08:57,760
for themselves, by which they mean peace and stability, a state of being that is comfortable.

89
00:08:57,760 --> 00:09:00,040
When in fact, it's exactly the opposite.

90
00:09:00,040 --> 00:09:06,320
So to me, this is the closest you can come to a purpose in life, is to have your actions

91
00:09:06,320 --> 00:09:14,440
lead you to their desired consequence, and I think this is something that is perhaps difficult

92
00:09:14,440 --> 00:09:20,960
to understand if you haven't examined the nature of reality, and this has to do with

93
00:09:20,960 --> 00:09:23,440
karma, the law of cause and effect.

94
00:09:23,440 --> 00:09:26,960
Many people don't believe in this, and they don't believe because they haven't looked

95
00:09:26,960 --> 00:09:31,760
and they haven't studied, but when you look and you study reality, you come to see that

96
00:09:31,760 --> 00:09:42,120
it's really true that our actions are often, quite often, out of tune with our desired

97
00:09:42,120 --> 00:09:46,680
goal or desired consequences.

98
00:09:46,680 --> 00:09:52,520
So that answers the first question as far as a purpose for life.

99
00:09:52,520 --> 00:09:58,880
I would say that should be one's purpose in life, and the really the underlying meaning

100
00:09:58,880 --> 00:10:05,440
is that one's purpose should be defined happiness, one's purpose should be defined peace.

101
00:10:05,440 --> 00:10:14,040
But I think that really goes without saying, because at any rate, apart from I would suppose,

102
00:10:14,040 --> 00:10:22,640
you could imagine a psychopath or someone with physical brain damage or mental, serious

103
00:10:22,640 --> 00:10:30,320
mental problems, we all wish in some way for happiness, in some way for peace.

104
00:10:30,320 --> 00:10:35,320
Well, let's say specifically for happiness, we do things because we think they will bring

105
00:10:35,320 --> 00:10:41,800
us whatever it is that we understand to be happiness, that this will bring us some sort

106
00:10:41,800 --> 00:10:48,440
of positive, beneficial state, unless someone, I guess that would be a good definition

107
00:10:48,440 --> 00:10:56,520
of insanity as someone who doesn't, someone who actually wants to cause suffering, wants

108
00:10:56,520 --> 00:11:02,440
to do things that are caused for suffering, knowing, or thinking, doing things, thinking

109
00:11:02,440 --> 00:11:05,000
that they will cause suffering to oneself.

110
00:11:05,000 --> 00:11:08,880
You know, there are people, as I said, we all do things that cause a suffering, but we

111
00:11:08,880 --> 00:11:13,000
have some perverted idea that it's going to lead us to happiness.

112
00:11:13,000 --> 00:11:18,760
So I would say that we already have this, the point is to just get it, get the cause and

113
00:11:18,760 --> 00:11:24,400
effect in line with each other, so that we're not doing things and, you know, trying to

114
00:11:24,400 --> 00:11:30,240
reach a goal that's unattainable, or trying to reach an attainable goal through deeds

115
00:11:30,240 --> 00:11:37,120
and a path through means by which that goal is unattainable.

116
00:11:37,120 --> 00:11:42,160
What does life want from us, again I would say that this is something that we should

117
00:11:42,160 --> 00:11:53,240
try to overcome the idea that there is some plan or some organized path or way or rules

118
00:11:53,240 --> 00:11:56,960
even to our existence.

119
00:11:56,960 --> 00:12:00,160
And do I know my purpose yet?

120
00:12:00,160 --> 00:12:07,000
Yeah, I mean, I think that's why I became a Buddhist monk, it's, you know, studying,

121
00:12:07,000 --> 00:12:12,840
it's not because I was born into this or because my parents did it or because I thought

122
00:12:12,840 --> 00:12:17,120
it, you know, it was a hip thing to do and I was, you know, hey, everyone's becoming

123
00:12:17,120 --> 00:12:22,360
a monk or so, or even with the idea that I'd be doing something different, the reason

124
00:12:22,360 --> 00:12:30,840
I'm doing what I'm doing now and that I stuck with it is because even from the very beginning

125
00:12:30,840 --> 00:12:38,960
it struck me that this is a objective study and examination of reality for what it is and

126
00:12:38,960 --> 00:12:46,600
the more I've studied, the more I've examined this path and this teaching, the more it struck

127
00:12:46,600 --> 00:12:55,400
me to be that way, so the more intent I am on following it and obviously the more one follows

128
00:12:55,400 --> 00:13:02,880
this path, the more one understands reality and therefore, you know, it's not really even

129
00:13:02,880 --> 00:13:09,720
thinking of yourself as being Buddhist or having a certain Buddhist concept of the purpose

130
00:13:09,720 --> 00:13:15,520
of life, it's just coming closer and closer to an understanding of reality and the

131
00:13:15,520 --> 00:13:22,200
closer you come to that, the less you want to go against it or the less you do things that

132
00:13:22,200 --> 00:13:30,160
are against reality, so there's my take on this very important subject, the idea of the

133
00:13:30,160 --> 00:13:59,400
purpose of life and I hope that helps, thanks for the question.

