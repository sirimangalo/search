1
00:00:00,000 --> 00:00:04,800
Is it better to start following the path having a happy ego instead of doing it out of suffering?

2
00:00:05,600 --> 00:00:09,520
For instance, the Buddha had a good life before escaping before us. He never suffered personal.

3
00:00:12,640 --> 00:00:17,280
Is it better to have lots of suffering or only little suffering?

4
00:00:18,800 --> 00:00:22,080
I don't think it's um as I say in time might kill.

5
00:00:22,080 --> 00:00:28,000
It's nothing to do with it.

6
00:00:31,440 --> 00:00:37,040
It wasn't there a whole long time where Siddhartha

7
00:00:39,040 --> 00:00:45,920
he was fascinated and starved himself and denied himself of all kinds of things.

8
00:00:45,920 --> 00:00:52,560
That's because of that karma that he still had to even the Buddha still had to do away with

9
00:00:54,000 --> 00:00:55,200
according to the commentaries.

10
00:00:56,240 --> 00:01:01,040
Now I guess I can see sorry it is there is a connection there that you have to see suffering.

11
00:01:02,240 --> 00:01:05,200
That's the first you have to see that there's a problem but

12
00:01:05,200 --> 00:01:14,960
it and it's all suffering. The Buddha did suffer and he was surrounded by suffering.

13
00:01:17,920 --> 00:01:23,760
It just wasn't suffering from our point of view. Another thing you can think of that a person who has

14
00:01:23,760 --> 00:01:32,400
used to so much pleasure things that would not you know not phase most of us would cause intense

15
00:01:32,400 --> 00:01:40,720
displeasure and suffering so just seeing a sick person was shocked him not only because of his

16
00:01:40,720 --> 00:01:46,160
pampering but also because of his great wisdom more because of his great wisdom but still you

17
00:01:46,160 --> 00:01:51,040
can think that that's generally the case for rich people when a rich person sees a poor person it

18
00:01:51,040 --> 00:01:57,520
can be quite disturbing just to see someone who is dirty and poor and so on whereas if you're poor

19
00:01:57,520 --> 00:02:04,720
it's not such a big deal seeing dirty people seeing something something ugly something dirty and

20
00:02:06,960 --> 00:02:16,720
so I don't think living a life of pleasure makes you any less susceptible to suffering

21
00:02:16,720 --> 00:02:25,440
let's put it that way this is a real fallacy that it is misunderstanding in our mind we think that

22
00:02:25,440 --> 00:02:32,160
people like the Buddha must not have had any suffering well regardless of whether he did or not think

23
00:02:32,160 --> 00:02:37,920
of the state of being you know if you're talking just because he was rich think of the state of

24
00:02:37,920 --> 00:02:44,720
being a rich person from a Buddhist perspective there's nothing as you said there's no reason to

25
00:02:44,720 --> 00:02:57,120
think that they said for less no suffer from for far less than we will you know those

26
00:02:57,760 --> 00:03:08,080
hello people hello um hello bounty i we're recording now just let me finish this

27
00:03:08,080 --> 00:03:17,440
I don't know if anyone else has anything to say on that so I mean the idea of whether it's

28
00:03:17,440 --> 00:03:21,120
you're actually asking an interesting question which I wouldn't have thought and most people

29
00:03:21,120 --> 00:03:30,400
will say better to suffer a lot because then you then you really really want to become free

30
00:03:30,400 --> 00:03:39,520
from suffering but I don't see the correlation there everyone has suffering

31
00:03:42,400 --> 00:03:45,040
that's whether or not they have the wisdom to see it as suffering

32
00:03:47,040 --> 00:03:51,440
you know people can be surrounded people can be surrounded by dead people and still be horrible

33
00:03:51,440 --> 00:03:56,080
you know people who work in morbs and they you know I don't know if it actually happens but you

34
00:03:56,080 --> 00:04:02,960
hear rumors of necrophilia right these guys have worked in the morgue and they're surrounded

35
00:04:02,960 --> 00:04:12,080
by dead people and still get turned on by it this kind of thing so it's not you know it's not to

36
00:04:12,080 --> 00:04:18,320
say that that all you have to do is see lots of dead people or see lots of sick people

37
00:04:18,320 --> 00:04:26,880
um what you do see rather is that for example people who look after sick people it's easy to

38
00:04:26,880 --> 00:04:32,320
miss correlate I think because you see people who look after sick people can be very wise like

39
00:04:32,320 --> 00:04:41,200
nurses for example I had to deal with both nurses and doctors mostly nurses because when I was

40
00:04:41,200 --> 00:04:50,960
in America because many time women come to and men some men but more women go to America specifically

41
00:04:50,960 --> 00:04:57,600
to become nurses there's a nurse shortage in America and so everyone in Thailand is studying

42
00:04:57,600 --> 00:05:02,800
nursing I don't know many people many women in Thailand are studying nursing with the thought

43
00:05:02,800 --> 00:05:06,880
that they can go to America earn some money and come back home and support their parents and so

44
00:05:06,880 --> 00:05:13,840
I'm really good good intentions and so they make really good meditators and you really get a sense

45
00:05:13,840 --> 00:05:19,600
of their kindness and you know looking at the difference between nurses and doctors is interesting

46
00:05:19,600 --> 00:05:27,040
but only for another time but it has that has to do more with their their the good that they do

47
00:05:27,040 --> 00:05:32,240
working with sick people when you when you when you have these two things put together

48
00:05:32,240 --> 00:05:42,160
the association with suffering or and the cultivation of goodness which is perfectly exemplified by a

49
00:05:42,160 --> 00:05:55,520
nurse hospice care workers for example it has much more I wouldn't say to do with the goodness

50
00:05:55,520 --> 00:05:59,520
that you have the goodness in your mind no let's say it has to do with both you do have to

51
00:05:59,520 --> 00:06:06,000
experience suffering and you but you need the wisdom to understand it because look at the Buddha's

52
00:06:06,000 --> 00:06:15,520
example twenty nine years right he lived a life of luxury and didn't even think of going forth why

53
00:06:15,520 --> 00:06:23,520
because he still hadn't seen suffering so it is necessary and so to use the Buddha as an example

54
00:06:23,520 --> 00:06:28,160
of someone who started the path out of happiness he certainly didn't he he started out the path

55
00:06:28,160 --> 00:06:36,320
horrified by what he had seen modified by just totally bummed out there's one Buddhist teacher

56
00:06:36,320 --> 00:06:42,160
he was he was asked about depression he said you know I'm depressed you think I can practice

57
00:06:42,160 --> 00:06:47,200
meditation and he said well you know the Buddha was pretty bummed out when he went forth

58
00:06:48,240 --> 00:06:52,400
because he was really he was bummed out he realized that his life had been useless

59
00:06:52,400 --> 00:06:59,440
what he was doing was meaningless you know he kind of waking up out of the matrix he was

60
00:06:59,440 --> 00:07:08,240
realizing that he was in a bubble yeah it was it was an illusion what type so what makes the

61
00:07:08,240 --> 00:07:15,680
person see suffering two things you have to be presented with it and you have to have the wisdom

62
00:07:15,680 --> 00:07:22,080
you have to have the goodness so for all of us something that I personally don't mention nearly

63
00:07:22,080 --> 00:07:27,360
enough is is the importance of goodness the importance of doing good deeds the importance of helping

64
00:07:27,360 --> 00:07:35,600
people it's so undervalued in Buddhist circles the the simple deeds of like a nurse what

65
00:07:35,600 --> 00:07:41,760
a nurse does helping people it makes you it empowers you it makes you feel good about yourself

66
00:07:42,800 --> 00:07:47,280
clears your mind it it it takes out your selfishness and your greed

67
00:07:47,280 --> 00:07:54,400
and doing charitable acts and helping other people is incredibly important for allowing you

68
00:07:54,400 --> 00:08:00,240
to cultivate wisdom you know Buddhist Western Buddhist like to skip this all and just jump to the

69
00:08:00,240 --> 00:08:06,640
wisdom part so it's it's it's it's really dealing with things that they're not capable of

70
00:08:06,640 --> 00:08:12,240
comprehending you know you can be as brilliant an intellectual as you like but without the goodness

71
00:08:12,240 --> 00:08:23,600
and the the power of mind that comes from being a good person you you simply fall into views and

72
00:08:23,600 --> 00:08:33,040
speculation it has to come from the heart yeah so one thing is being a good following heart

73
00:08:33,840 --> 00:08:39,680
and the first one don't follow your heart don't follow your heart do never follow your heart

74
00:08:39,680 --> 00:08:49,040
never well I mean never tell people to follow their heart never never take that as a rule

75
00:08:50,560 --> 00:08:57,520
it's funny because it it actually damn die in Thai for example is a very very negative

76
00:08:58,400 --> 00:09:02,880
has very very negative connotations whereas in English following your heart is like what you're supposed to

77
00:09:02,880 --> 00:09:07,760
do

78
00:09:07,760 --> 00:09:16,400
and maybe the Judeo Christian tradition or no modern consumer consumerism no no I use that to quite

79
00:09:16,400 --> 00:09:23,280
opt into all your heart I'm sure that's not related or if it's related to the Christian

80
00:09:24,000 --> 00:09:28,800
I don't know exactly I think it's really the modern consumerism materialist

81
00:09:28,800 --> 00:09:34,960
follow your desires it's the same thing saying the same thing but I thought that in Thai

82
00:09:34,960 --> 00:09:40,880
hearts and mind the world like this in the same sort of not exactly

83
00:09:42,720 --> 00:09:47,040
keep it falling your heart would be a supernatural in your mind

84
00:09:49,360 --> 00:09:53,520
no but it's that in English that works in English but it doesn't work in Thai

85
00:09:53,520 --> 00:10:01,200
that means to go after to to it doesn't mean to follow in the sense of

86
00:10:02,400 --> 00:10:07,680
do yeah actually you could and I think they use it as a play on words damn do jai if you

87
00:10:07,680 --> 00:10:12,800
put the word do in there it makes sense if you follow looking at your heart or your mind

88
00:10:14,000 --> 00:10:19,280
yeah and Buddha said following your heart following your heart means listening to your heart

89
00:10:19,280 --> 00:10:28,160
letting it yeah and by the nose yeah Buddha said that the path is not on the sky

90
00:10:28,160 --> 00:10:40,640
but the path is not on your heart but the set it is set but yeah I don't have a certified

91
00:10:41,520 --> 00:10:47,360
for materials or goodness I just watched videos on YouTube and there was like

92
00:10:47,360 --> 00:10:55,920
we just closed you haven't heard that you know that's how how I started with

93
00:10:55,920 --> 00:11:03,520
the video the video the video didn't quite get out I remember that

94
00:11:04,320 --> 00:11:11,440
so okay so that was about being happy when you start the path or starting out of suffering

95
00:11:12,000 --> 00:11:17,120
I'm more for the suffering side but for people who have wisdom to see it I think you have to

96
00:11:17,120 --> 00:11:20,720
show people suffering it doesn't mean that you should hurt yourself or so on but you should

97
00:11:20,720 --> 00:11:26,480
be willing to put yourself in situations that open your mind up open your heart

98
00:11:28,960 --> 00:11:33,920
and if you just live in a bubble and stick with what's comfortable you'll never be challenged

99
00:11:33,920 --> 00:11:37,840
right this is the deal is that people are never challenged their beliefs are never challenged

100
00:11:37,840 --> 00:11:44,800
because everything around them is designed to support and confirm their needs so they live in a

101
00:11:44,800 --> 00:11:55,520
bubble no Dante one one thing if you are so dissatisfied about basically world the reality as it is

102
00:11:55,520 --> 00:12:04,640
it's so dissatisfied about it what like there's a lot of anger like and sometimes

103
00:12:04,640 --> 00:12:16,240
it's hard because once you become like you see this satisfaction then there's a lot of anger

104
00:12:16,240 --> 00:12:23,760
instead of peace you have to actually fight going to the extent that hey I don't want to be here

105
00:12:23,760 --> 00:12:29,920
I mean not actually I'm not talking about like material you can have money it's got nothing to do

106
00:12:29,920 --> 00:12:36,160
with money or material possessions all right you are if somebody like have a lot of money can

107
00:12:36,160 --> 00:12:43,760
become aware of the safety no problem there but but then you can say like well why I'm here

108
00:12:43,760 --> 00:12:49,680
and then I said hey I didn't choose it I'll rather commit suicide because I'm so dissatisfied

109
00:12:49,680 --> 00:12:56,640
about it really because everywhere is it satisfaction really every material being called

110
00:12:56,640 --> 00:13:07,760
dissatisfaction no the mind holds dissatisfaction okay that's where your problem is though

111
00:13:12,640 --> 00:13:24,560
wow well the better for example you want to let say you want to live in a sterile life

112
00:13:24,560 --> 00:13:31,440
everything is so you know in reality everything is so imperfect

113
00:13:32,480 --> 00:13:35,520
you want to live in a sterile environment and the one thing is the problem

114
00:13:37,040 --> 00:13:42,480
yeah but I just don't want to accept the imperfect world I don't want to live in imperfect world

115
00:13:42,480 --> 00:13:46,960
it would be perfectly just the problem it's going to be a problem

116
00:13:46,960 --> 00:13:51,840
your inability to accept is going to cause problems for you as far as I can see yeah I don't

117
00:13:51,840 --> 00:13:57,440
feel comfortable with accepting everything like so then that's your then that's a problem for you

118
00:13:59,120 --> 00:14:02,480
that's always going to be a cause for suffering for you because you're not comfortable

119
00:14:02,480 --> 00:14:08,080
you're not comfortable with what you'll have to be faced with yeah but why should I

120
00:14:08,080 --> 00:14:16,880
me myself I couldn't I didn't didn't how was the God if I was a God I would arrange this

121
00:14:16,880 --> 00:14:23,440
without my secret that's an interesting question why should you I mean only you have the answer to

122
00:14:23,440 --> 00:14:32,960
that you the romantic betted so leekman you've made your bed no lie in it actually that's not

123
00:14:32,960 --> 00:14:40,720
what I mean it's exactly as a man makes his bed so he lies in it the mensif betted so leekman

124
00:14:40,720 --> 00:14:49,200
leekman yes I practice that you know it's German

125
00:14:51,600 --> 00:14:57,200
I don't have an answer for why should you but you do you you have two choices you can

126
00:14:57,920 --> 00:15:02,080
become comfortable it's what you're not comfortable with or you can continue to be uncomfortable

127
00:15:02,080 --> 00:15:08,400
with it two choices you know even if if if if if I change myself and I've become better person

128
00:15:08,400 --> 00:15:13,600
it's it's it's it's kind of satisfying to think because you need satisfaction because you're going

129
00:15:13,600 --> 00:15:21,440
to be depressed if you're not going to be satisfied or happy about something I think so I think

130
00:15:21,440 --> 00:15:33,440
if you if you don't have any well connect if you don't have anything like you know anything positive

131
00:15:33,440 --> 00:15:44,720
about yourself in your life well so you become better well even if you become better person

132
00:15:44,720 --> 00:15:49,600
that's what they call a false dichotomy you you you don't have to be either you don't have to be

133
00:15:49,600 --> 00:15:57,120
either satisfied or depressed and you can just be at peace you can give up both extremes and

134
00:15:57,120 --> 00:16:03,200
and just be you know like when you see the depression you can take it for what it is become

135
00:16:03,200 --> 00:16:08,800
comfortable with it when comfortable in the sense of not worrying or becoming upset by it or even

136
00:16:08,800 --> 00:16:14,720
even being interested in it except it's just for what it is and when you feel satisfied or when

137
00:16:14,720 --> 00:16:20,240
you feel something that you think that'll make me satisfied then instead come to see it simply as

138
00:16:20,240 --> 00:16:28,240
it is not as truly desirable or pleasurable or satisfying but just as an experience

139
00:16:31,760 --> 00:16:37,360
okay so just kind of the emotional baggage of all of it so don't take it emotionally

140
00:16:37,920 --> 00:16:43,760
yeah right neither neither sometimes you'll be depressed the key to all of it to this

141
00:16:44,560 --> 00:16:49,440
very very common problem is to be okay with it don't be afraid of the depression

142
00:16:49,440 --> 00:16:56,400
whatever you're afraid of that's that's what's going to to destroy you when you're afraid of

143
00:16:56,400 --> 00:17:03,120
something you you give it power it only becomes a problem because you you think it's a problem

144
00:17:04,480 --> 00:17:09,840
when it's just depression it's just depression you know isn't that isn't that silly

145
00:17:10,640 --> 00:17:14,720
that's that it's such a you know people kill themselves over depression

146
00:17:14,720 --> 00:17:21,920
i'm not you know what i'm just depression i wouldn't say depression is much a shock or enough

147
00:17:21,920 --> 00:17:31,280
to be a problem anyway we're trying to be a topic maybe i should quit this video about

148
00:17:32,080 --> 00:17:37,760
um kind of a philosophical one about whether it's better to start off happy or suffering

149
00:17:38,720 --> 00:17:42,880
i think that what the key was for this person's question was whether you should

150
00:17:42,880 --> 00:17:48,400
i thought they asked it another way earlier someone did was whether you should

151
00:17:50,240 --> 00:17:55,440
enjoy life first kind of thing where they you should do you know become like the Buddha where

152
00:17:55,440 --> 00:17:59,440
you're rich and this is it sounds like a little bit of a

153
00:18:00,960 --> 00:18:06,080
takantuang it's funny i'm hurting the tile that i know you can on your own side

154
00:18:06,080 --> 00:18:13,440
kind of taking your own side and this well maybe i should go through life first really

155
00:18:13,440 --> 00:18:20,960
experience all the joys and the pleasures of life and it's really lying to yourself because

156
00:18:20,960 --> 00:18:26,400
that's what you're saying what you're talking about is not simply enjoying the pleasures of life

157
00:18:26,400 --> 00:18:33,760
it's cultivating it's trying to find more which is not just the pleasure it's the attachment and

158
00:18:33,760 --> 00:18:39,440
the desire very very dangerous so if there's that kind of thing where i should be really really

159
00:18:40,080 --> 00:18:44,880
happy first then you're just going to cultivate addiction which is just going to make it

160
00:18:44,880 --> 00:18:50,160
more difficult to follow the path so no i don't think that would be useful if that's part of your

161
00:18:50,160 --> 00:19:04,240
question we got a nurse out there it looks like nurses rock right next to teachers

162
00:19:05,600 --> 00:19:13,680
no i mean as this poor joke i mean like public teachers

163
00:19:13,680 --> 00:19:20,080
um and again it's not all i mean not all nurses are nice people i don't think but

164
00:19:20,880 --> 00:19:29,200
people who teach children public school teachers can be really radiant and wonderful people

165
00:19:29,200 --> 00:19:36,480
and make very good meditators i guess i have to couch that because i had some pretty bad teachers

166
00:19:36,480 --> 00:19:49,760
but i was a horrible student so i deserve it i mean as you get something out of it by helping

167
00:19:49,760 --> 00:19:56,880
people by giving people by by being concerned about other people you know teachers they do

168
00:19:56,880 --> 00:20:02,160
think about their students and they want their students to to gain something they're giving something

169
00:20:02,160 --> 00:20:09,040
so it's not to say that teachers will be coming light and there's on but it's that they have

170
00:20:09,040 --> 00:20:15,680
great potential because of the fortitude of mine that's required in order to do such a good thing

171
00:20:15,680 --> 00:20:22,240
for some more so these are just theories i mean i just pick up these theories in it see

172
00:20:22,240 --> 00:20:27,040
based on observations that could be totally false but it seems to be that nurses and teachers

173
00:20:27,040 --> 00:20:34,240
tend to be up there as friends who makes a good meditator yeah getting out of yourself is always

174
00:20:34,240 --> 00:20:42,240
a good thing you know to just get out of your own head and service helps to do that

175
00:20:44,240 --> 00:20:49,920
yeah yeah but it it does more than that i think it makes people strong

176
00:20:49,920 --> 00:21:00,400
there's this inherent quality of good karma this is karma karma changes you it fortifies the

177
00:21:00,400 --> 00:21:05,520
mind look at the middle of un has an interesting section about being able to send goodness to

178
00:21:05,520 --> 00:21:12,240
other people how you can you can actually send the benefits of good things that you've done to

179
00:21:12,240 --> 00:21:18,800
others and dedicated to them i'm wish for them pray for them to be happy prayer but you can't

180
00:21:18,800 --> 00:21:24,960
but King Melinda asks can you do that with bad deeds very interesting question uh who would have

181
00:21:24,960 --> 00:21:28,800
thought of such a thing if you do lots of bad deeds can you say okay i mean that person

182
00:21:29,520 --> 00:21:35,200
go to hell as a result of my bad deeds how would you answer such a question oh magazine

183
00:21:35,200 --> 00:21:45,520
that was brilliant of course and he said uh he said no because bad deeds collapse in on themselves

184
00:21:45,520 --> 00:21:52,240
they shrink they cause a person to shrink they don't good deeds expand outwards and bring power

185
00:21:52,240 --> 00:22:01,360
and and greatness so they can be expanded they can expand you but bad deeds cause you to implode

186
00:22:02,320 --> 00:22:06,080
and that's not possible to be shared

187
00:22:06,080 --> 00:22:14,800
cursed in people from your own bad deeds horrible though

