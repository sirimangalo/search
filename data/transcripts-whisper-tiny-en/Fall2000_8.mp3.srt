1
00:00:00,000 --> 00:00:23,840
For many years, I was a regular reader of the reader's digest magazine.

2
00:00:23,840 --> 00:00:32,920
And I remember one cartoon in one of the issues of reader's digest magazine.

3
00:00:32,920 --> 00:00:37,640
It may be 30 years old.

4
00:00:37,640 --> 00:00:48,760
So that cartoon was of a picture of a religious minister about to give a religious talk

5
00:00:48,760 --> 00:00:52,280
to the congregation.

6
00:00:52,280 --> 00:00:56,760
And the picture shows him from the back.

7
00:00:56,760 --> 00:01:08,400
And under the, what do you call this, podium, under the, uh, the, uh, microphone stand.

8
00:01:08,400 --> 00:01:15,600
There was a bag of course clubs.

9
00:01:15,600 --> 00:01:27,080
And the caption says, today I will be brief.

10
00:01:27,080 --> 00:01:37,680
So following that, following that cartoon, I think I will be brief today, but I'm not

11
00:01:37,680 --> 00:01:50,680
going to break off.

12
00:01:50,680 --> 00:01:58,600
I was, uh, searching for a subject to talk about tonight, and I, I, I did not know what to

13
00:01:58,600 --> 00:02:12,280
talk about, but fortunately, I got some topic from the interviews with the yogis.

14
00:02:12,280 --> 00:02:15,680
So some yogis ask me some questions.

15
00:02:15,680 --> 00:02:24,840
And when yogi even asked me to, to, uh, give the answer to her question, when I give

16
00:02:24,840 --> 00:02:26,920
it the math talk.

17
00:02:26,920 --> 00:02:36,120
So today's talk will not be on one subject only, but some questions, uh, the yogis ask

18
00:02:36,120 --> 00:02:41,640
me to explain.

19
00:02:41,640 --> 00:02:47,240
The first thing I want to talk about is about equanimity.

20
00:02:47,240 --> 00:02:54,920
Now, a yogi wants to know what equanimity is.

21
00:02:54,920 --> 00:03:11,880
The, uh, as used in Buddhist books is, uh, a neutrality, neutrality of mind.

22
00:03:11,880 --> 00:03:21,480
Neutrality means, hmm, not, not falling through to the either side.

23
00:03:21,480 --> 00:03:26,240
So to, to, to stay in the middle and not to fall to either side.

24
00:03:26,240 --> 00:03:34,520
And that is called equanimity in, in the Buddhist teachings.

25
00:03:34,520 --> 00:03:42,600
And in the teachings of the Buddha, equanimity is a mental state.

26
00:03:42,600 --> 00:03:54,200
It is many of you know one of the 52 mental factors taught in abidama.

27
00:03:54,200 --> 00:03:58,640
And this is called specific neutrality.

28
00:03:58,640 --> 00:04:09,120
So specific neutrality is a name for equanimity consisting in the equal balance of the associated

29
00:04:09,120 --> 00:04:10,520
states.

30
00:04:10,520 --> 00:04:20,760
Now the mental states arise together, there are two, two mental states, consciousness

31
00:04:20,760 --> 00:04:22,720
and mental factors.

32
00:04:22,720 --> 00:04:29,680
So consciousness and mental factors always arise together.

33
00:04:29,680 --> 00:04:38,880
So when they arise together, uh, they arise together and they do their respective functions.

34
00:04:38,880 --> 00:04:49,120
So for them to do their respective functions, probably, there is one mental state called

35
00:04:49,120 --> 00:04:51,320
specific neutrality.

36
00:04:51,320 --> 00:05:05,120
So this keeps the, the, the associated states of coordination mental states in equal balance.

37
00:05:05,120 --> 00:05:15,880
This specific neutrality, uh, takes many forms.

38
00:05:15,880 --> 00:05:26,320
Now when this specific neutrality arises in the mind of an errand, now it is called equanimity

39
00:05:26,320 --> 00:05:40,960
with six factors, now that means when a, when an errand says, sees a visible object,

40
00:05:40,960 --> 00:05:46,120
he is neither glad, nor sad about that object.

41
00:05:46,120 --> 00:05:52,560
So he, he is not moved by the quality of the object.

42
00:05:52,560 --> 00:06:02,280
So an errand can dwell in equanimity and be mindful and fully aware of the object, but

43
00:06:02,280 --> 00:06:09,160
he will not be glad or sad when he sees the object.

44
00:06:09,160 --> 00:06:14,080
So the same with the other kinds of objects, there are six kinds of objects.

45
00:06:14,080 --> 00:06:23,560
And that is why this equanimity is called equanimity with six, six like this.

46
00:06:23,560 --> 00:06:28,480
Actually equanimity regarding six kinds of objects.

47
00:06:28,480 --> 00:06:36,560
So whenever an errand sees or experiences any one of the six kinds of objects, he is

48
00:06:36,560 --> 00:06:44,720
able to keep his mind neutral, not liking, not disliking.

49
00:06:44,720 --> 00:06:55,240
So that kind of equanimity is called equanimity with six factors.

50
00:06:55,240 --> 00:07:07,080
Then the equanimity or neutrality is practiced towards beings, then it is called a divine

51
00:07:07,080 --> 00:07:10,120
abiding of Brahma wehara.

52
00:07:10,120 --> 00:07:19,920
So you know there are four kinds of Brahma wehara's loving-kindness, compassion, sympathetic

53
00:07:19,920 --> 00:07:25,000
joy and equanimity.

54
00:07:25,000 --> 00:07:32,040
Equanimity is neutrality towards all beings.

55
00:07:32,040 --> 00:07:45,720
That means when a person practices equanimity as a divine abiding, even when he sees a being

56
00:07:45,720 --> 00:07:51,560
in suffering, he will not feel compassion.

57
00:07:51,560 --> 00:07:59,640
And if he sees a person in prosperity, he will not feel happiness.

58
00:07:59,640 --> 00:08:08,240
So whether he sees a person in distress or in prosperity, his mind is always neutral.

59
00:08:08,240 --> 00:08:23,040
So that kind of neutrality is one equanimity, one form equanimity takes.

60
00:08:23,040 --> 00:08:32,000
And equanimity can take a form of factor of enlightenment.

61
00:08:32,000 --> 00:08:37,560
So equanimity is one of the seven factors of enlightenment.

62
00:08:37,560 --> 00:08:50,440
So there equanimity means, again, neutrality among the cognizant or associated states.

63
00:08:50,440 --> 00:09:02,880
That means when the equanimity is developed to the level of enlightenment factor, again,

64
00:09:02,880 --> 00:09:16,640
is mental state can keep other factors of enlightenment in proper balance and letting

65
00:09:16,640 --> 00:09:21,040
them do their own respective functions.

66
00:09:21,040 --> 00:09:30,520
And when this arises in a yogi, a yogi does not have to make efforts, say, to be mindful

67
00:09:30,520 --> 00:09:34,120
or to watch objects.

68
00:09:34,120 --> 00:09:45,120
So it is effortless awareness of the objects when this equanimity is developed to the

69
00:09:45,120 --> 00:09:50,160
enlightenment factor.

70
00:09:50,160 --> 00:10:02,040
And when it arises with Jana, it is called equanimity of Jana.

71
00:10:02,040 --> 00:10:13,120
Now you know there are different stages of Jana, the first Jana, second Jana and so on.

72
00:10:13,120 --> 00:10:28,920
In the fourth Jana, this factor is so developed that even in the bliss, even in the highest

73
00:10:28,920 --> 00:10:34,200
bliss of Jana, it is impartial.

74
00:10:34,200 --> 00:10:45,080
That means the equanimity is not partial to the bliss attained through Jana's.

75
00:10:45,080 --> 00:10:52,440
So Jana, the bliss attained through Jana's has said to be very refined and sublime.

76
00:10:52,440 --> 00:11:06,120
But even in such bliss, a yogi is impartial, even among these highest bliss, he is impartial.

77
00:11:06,120 --> 00:11:18,520
He will not like or dislike that kind of bliss or he will not be interested in that kind

78
00:11:18,520 --> 00:11:19,920
of bliss.

79
00:11:19,920 --> 00:11:26,840
So he tries to transcend bliss and enjoy what is called equanimity.

80
00:11:26,840 --> 00:11:38,040
And it is said that this equanimity is on a higher level than what we call suka of bliss.

81
00:11:38,040 --> 00:11:49,480
And when equanimity arises with the highest Jana, it is called equanimity regarding purifying.

82
00:11:49,480 --> 00:11:57,400
That means the highest Jana is the most purified of all Jana's.

83
00:11:57,400 --> 00:12:08,040
And when a yogi has reached the highest, the most purified Jana, he has no interest in purifying

84
00:12:08,040 --> 00:12:10,480
it because it is already purified.

85
00:12:10,480 --> 00:12:19,640
So the equanimity arising together with that highest Jana is called equanimity about purifying.

86
00:12:19,640 --> 00:12:29,680
That means he is indifferent in purifying because it is already purified.

87
00:12:29,680 --> 00:12:45,640
So equanimity is neutrality, impartiality, and being unmoved by the objects either desirable

88
00:12:45,640 --> 00:12:51,400
or undesirable.

89
00:12:51,400 --> 00:13:00,160
So just one equanimity is called six-factor equanimity, equanimity as it divine abiding and

90
00:13:00,160 --> 00:13:14,920
so on, depending on where it arises.

91
00:13:14,920 --> 00:13:38,160
So this equanimity, so this equanimity or neutrality yogis experience when the meditation

92
00:13:38,160 --> 00:13:43,240
is good, when they have good concentration.

93
00:13:43,240 --> 00:13:56,120
Now some people want to know whether this is nibana, now this is not nibana.

94
00:13:56,120 --> 00:14:04,280
Now in order to understand this, you need to understand what nibana is.

95
00:14:04,280 --> 00:14:12,280
And it is very difficult to talk about nibana because it cannot be described in the terms

96
00:14:12,280 --> 00:14:24,760
we are used to, because when we explain nibana, we use negative terms like cessation

97
00:14:24,760 --> 00:14:38,600
of dukha, cessation of suffering, or extinction of greed, hatred, and delusion and so on.

98
00:14:38,600 --> 00:14:53,240
Nibana is, let us say, a state, a state that can be taken as object but it is not a subject

99
00:14:53,240 --> 00:15:05,560
that means it does not take any object, but it is the object which is taken by the other states

100
00:15:05,560 --> 00:15:09,040
other mental states.

101
00:15:09,040 --> 00:15:20,800
And it is said that nibana is not made, not created, and unborn, and not conditioned.

102
00:15:20,800 --> 00:15:24,800
That is why it is difficult to understand and difficult to describe.

103
00:15:24,800 --> 00:15:30,400
So what is not conditioned has no beginning?

104
00:15:30,400 --> 00:15:37,280
Only when something is made or something is conditioned, it has a beginning.

105
00:15:37,280 --> 00:15:42,560
But since nibana is not made, not conditioned, it has no beginning.

106
00:15:42,560 --> 00:15:45,640
That is why it has no end.

107
00:15:45,640 --> 00:15:49,000
If it has beginning, it must have an end.

108
00:15:49,000 --> 00:15:58,920
Now, since it has no beginning, it has no end, and how can we perceive or think of something

109
00:15:58,920 --> 00:16:06,440
that has no beginning and no end, and still it is something that can be taken as object.

110
00:16:06,440 --> 00:16:16,800
So it is very difficult.

111
00:16:16,800 --> 00:16:23,200
In Nibana, Nibana is classified as an external object.

112
00:16:23,200 --> 00:16:31,360
So Nibana cannot be in the minds of beings.

113
00:16:31,360 --> 00:16:43,000
It is an external object that can be taken as object by some types of consciousness.

114
00:16:43,000 --> 00:16:53,680
And it is also said that Nibana is not a mental state, Nibana is not a matter.

115
00:16:53,680 --> 00:17:05,040
So no mental state, no matter, no earth element, and so on.

116
00:17:05,040 --> 00:17:19,840
So it is a state of peacefulness, or it is the extinction of mental refinement or extinction

117
00:17:19,840 --> 00:17:25,800
of five aggregates or extinction of suffering.

118
00:17:25,800 --> 00:17:39,520
As the extinction of suffering, it is a separate object that is taken as object by the

119
00:17:39,520 --> 00:17:54,120
heart, consciousness, and fruition consciousness.

120
00:17:54,120 --> 00:18:06,080
When using English, we can say Nibana is not a mental state.

121
00:18:06,080 --> 00:18:11,160
But if we use Pali, it is different.

122
00:18:11,160 --> 00:18:16,800
So I don't want you to be confused, but I must say this.

123
00:18:16,800 --> 00:18:27,840
Now, the word for mind in Pali is Nama, and A-M-A.

124
00:18:27,840 --> 00:18:38,920
And that Nama is defined as something that inclines towards the object.

125
00:18:38,920 --> 00:18:41,520
That is one definition.

126
00:18:41,520 --> 00:18:51,440
And the other definition is something that makes others to incline towards it.

127
00:18:51,440 --> 00:18:59,800
So the first definition is the ordinary definition, and the second definition is it involves

128
00:18:59,800 --> 00:19:02,640
causative sense.

129
00:19:02,640 --> 00:19:07,200
So the first definition is something that inclines towards the object.

130
00:19:07,200 --> 00:19:09,480
That means that takes the object.

131
00:19:09,480 --> 00:19:16,000
And the second definition is something that makes others incline towards it.

132
00:19:16,000 --> 00:19:21,400
So there are two definitions of the word Nama in Pali.

133
00:19:21,400 --> 00:19:34,200
Now, there are consciousness, mental states, material, properties of matter, and Nibana.

134
00:19:34,200 --> 00:19:38,640
Now, consciousness can take objects.

135
00:19:38,640 --> 00:19:46,720
That means consciousness inclines towards the object, and so consciousness is in Nama.

136
00:19:46,720 --> 00:19:53,880
And mental factors always arise with consciousness, and so along with consciousness they

137
00:19:53,880 --> 00:19:55,960
take object.

138
00:19:55,960 --> 00:20:05,760
So mental factors are also called Nama.

139
00:20:05,760 --> 00:20:11,520
And consciousness, one consciousness can be the object of another consciousness.

140
00:20:11,520 --> 00:20:17,280
That means consciousness can make another consciousness incline towards it.

141
00:20:17,280 --> 00:20:23,800
So by both definitions consciousness is in Nama.

142
00:20:23,800 --> 00:20:32,200
By both definitions the mental factors are Nama.

143
00:20:32,200 --> 00:20:35,440
Now we come to matter.

144
00:20:35,440 --> 00:20:37,560
We can take matter as object.

145
00:20:37,560 --> 00:20:45,080
So that means our mind inclines towards the object, but matter cannot take an object

146
00:20:45,080 --> 00:20:51,040
itself.

147
00:20:51,040 --> 00:21:08,760
So matter is no Nama, it does not take an object, but Nibana is called Nama, because

148
00:21:08,760 --> 00:21:14,240
it can make the consciousness inclines towards it.

149
00:21:14,240 --> 00:21:21,400
That means it can be taken as object by consciousness and mental factors.

150
00:21:21,400 --> 00:21:31,880
So if we use English we say Nibana is not a mental state, so it's plain.

151
00:21:31,880 --> 00:21:42,560
But if we use Pali we say Nibana is also a Nama, but it is not the same Nama as the consciousness

152
00:21:42,560 --> 00:21:44,840
and mental factors.

153
00:21:44,840 --> 00:21:55,200
So consciousness and mental factors are called Nama, according to both definitions.

154
00:21:55,200 --> 00:22:00,760
But Nibana is called Nama, according to the second definition only.

155
00:22:00,760 --> 00:22:15,320
So Nibana is included in Nama, but since Nibana is included in Nama we cannot say that Nibana

156
00:22:15,320 --> 00:22:15,040
is mental.

157
00:22:15,040 --> 00:22:19,440
So we should be careful about this.

158
00:22:19,440 --> 00:22:30,080
We should better not talk about Nibana as Nama at all.

159
00:22:30,080 --> 00:22:39,320
Now there is a saying in one discourse that we can find all four noble truths in this

160
00:22:39,320 --> 00:22:41,520
fathom long body.

161
00:22:41,520 --> 00:22:45,800
That means we can find all four noble truths in this body.

162
00:22:45,800 --> 00:22:50,320
And Nibana is the third of the noble truth.

163
00:22:50,320 --> 00:22:57,160
But actually we are not to understand that Nibana is in us.

164
00:22:57,160 --> 00:23:00,760
Nibana is in our mind or in our body.

165
00:23:00,760 --> 00:23:07,960
But it is said that Nibana can be found in this body because Nibana can be taken as object

166
00:23:07,960 --> 00:23:13,240
by our mind when we get enlightenment.

167
00:23:13,240 --> 00:23:22,640
So since it can be taken as object by the mind which is internal to us, Buddha said that

168
00:23:22,640 --> 00:23:28,960
all these four noble truths can be found in this body.

169
00:23:28,960 --> 00:23:38,440
So pointing out to that discourse we are not to say that Nibana can be in us.

170
00:23:38,440 --> 00:23:45,240
Nibana is internal because Abitama is very exact and strict and in Abitama it is classified

171
00:23:45,240 --> 00:23:50,800
as an external object.

172
00:23:50,800 --> 00:24:03,000
Although Nibana is mostly defined in negative terms, it is not a negative state.

173
00:24:03,000 --> 00:24:06,360
It is a positive state.

174
00:24:06,360 --> 00:24:13,960
Because it is a positive state, it can be taken as object by path consciousness and

175
00:24:13,960 --> 00:24:16,280
fruition consciousness.

176
00:24:16,280 --> 00:24:22,600
So we can compare Nibana to health.

177
00:24:22,600 --> 00:24:28,160
Now what is health?

178
00:24:28,160 --> 00:24:30,120
No disease.

179
00:24:30,120 --> 00:24:34,400
So absence of disease is what we call health.

180
00:24:34,400 --> 00:24:38,320
But it is not just the absence of disease, it is something positive.

181
00:24:38,320 --> 00:24:42,000
Although we cannot describe, say what health is.

182
00:24:42,000 --> 00:24:45,800
But we know and we experience health.

183
00:24:45,800 --> 00:24:51,520
So when we are healthy, we know that we have health and when we have diseases we know

184
00:24:51,520 --> 00:24:54,000
that we have no health.

185
00:24:54,000 --> 00:24:57,280
So health is a positive state.

186
00:24:57,280 --> 00:25:05,560
But if we are asked to explain what health is, we may just say it is absence of diseases.

187
00:25:05,560 --> 00:25:10,080
So in the same way Nibana is a positive state and then we say Nibana is the extinction

188
00:25:10,080 --> 00:25:21,360
of suffering and so on.

189
00:25:21,360 --> 00:25:28,160
Now the other question is about chitana of volition.

190
00:25:28,160 --> 00:25:33,760
Now chitana of volition is also a mental state.

191
00:25:33,760 --> 00:25:46,240
It is a mental state that organizes the other mental concomitant or that pushes or that urges

192
00:25:46,240 --> 00:25:54,520
the other mental concomitant to do their respective functions.

193
00:25:54,520 --> 00:26:04,520
And this chitana, although it is a mental state and it disappears immediately after it

194
00:26:04,520 --> 00:26:11,880
arises, it has the potential to give results in the future.

195
00:26:11,880 --> 00:26:23,720
So when it disappears, it leaves some kind of potential to give results in the continuity

196
00:26:23,720 --> 00:26:26,160
of a being.

197
00:26:26,160 --> 00:26:32,120
We do not know where this potential is stored.

198
00:26:32,120 --> 00:26:39,440
But when the conditions are met with, when the conditions are feverable, then the results

199
00:26:39,440 --> 00:26:41,760
are produced.

200
00:26:41,760 --> 00:26:56,680
So that chitana translated in English as volition is the one that produces results.

201
00:26:56,680 --> 00:27:01,280
And that chitana is what we call kama.

202
00:27:01,280 --> 00:27:14,920
So the word kama etymologically means a work doing or a deed.

203
00:27:14,920 --> 00:27:24,040
But actually technically kama is this chitana.

204
00:27:24,040 --> 00:27:31,480
But whenever you do something, this chitana always arises.

205
00:27:31,480 --> 00:27:39,960
So this chitana is always connected with deeds or works.

206
00:27:39,960 --> 00:27:46,880
And so the deeds come to be called kama.

207
00:27:46,880 --> 00:27:55,720
So now it is, if you ask, what is kama, you may say kama is a good or bad deed.

208
00:27:55,720 --> 00:28:03,200
But technically, kama is not good or bad deed, but the chitana of volition that arises

209
00:28:03,200 --> 00:28:08,880
in the mind when one does a good or bad deed.

210
00:28:08,880 --> 00:28:19,400
And that chitana has, as I said, the potential to give results in the future.

211
00:28:19,400 --> 00:28:29,360
It is thought in a bidhamma that chitana accompanies every type of consciousness.

212
00:28:29,360 --> 00:28:41,640
But not chitana concomitant with every type of consciousness is called kama.

213
00:28:41,640 --> 00:28:51,560
Chitana, that accompanies only the wholesome type of consciousness and unwholesome type

214
00:28:51,560 --> 00:28:55,800
of consciousness is called kama.

215
00:28:55,800 --> 00:29:01,680
Chitana can arise with the resultant consciousness.

216
00:29:01,680 --> 00:29:07,560
Chitana can arise with, say, what I call, functional consciousness.

217
00:29:07,560 --> 00:29:14,960
So when chitana arises with resultant consciousness, when it arises with functional consciousness,

218
00:29:14,960 --> 00:29:23,400
it is not called kama because it has no power to produce results.

219
00:29:23,400 --> 00:29:35,320
Only when it accompanies the wholesome consciousness or unwholesome consciousness is called kama.

220
00:29:35,320 --> 00:29:52,440
Only then can it produce results.

221
00:29:52,440 --> 00:30:02,760
And the results that arise are produced by chitana, the mental state, and not by the

222
00:30:02,760 --> 00:30:13,640
things, I mean, in the act of giving, and this is not the things that are offered that

223
00:30:13,640 --> 00:30:23,800
produce results, for example, you offer a set of rope to the sangha and according to the

224
00:30:23,800 --> 00:30:36,440
law of kama, you acquire kusala, wholesome kama when you offer a set of rope to the sangha.

225
00:30:36,440 --> 00:30:46,280
When the chitana accompanying your consciousness is kama, and it is this chitana that produces

226
00:30:46,280 --> 00:30:51,760
results in the future and not the things you offer.

227
00:30:51,760 --> 00:30:59,920
A rope, is a rope cannot produce anything, it is the product, but what produces results

228
00:30:59,920 --> 00:31:09,240
is not the thing that you offer, but the mental state that accompanies your consciousness

229
00:31:09,240 --> 00:31:19,040
when you make that offering.

230
00:31:19,040 --> 00:31:29,920
So chitana is described as the mental state that wills or that encourages the other mental

231
00:31:29,920 --> 00:31:33,880
states to do their respective functions.

232
00:31:33,880 --> 00:31:43,800
Now people want to know if chitana encourages others who encourage this chitana.

233
00:31:43,800 --> 00:31:49,960
Now chitana is compared to a chief pupil in the class.

234
00:31:49,960 --> 00:32:01,560
The chief pupil does his own work and at the same time he makes the other students work.

235
00:32:01,560 --> 00:32:27,880
So chitana encourages itself and it encourages other mental states.

236
00:32:27,880 --> 00:32:47,880
And another question is how consciousness arises in the next life.

237
00:32:47,880 --> 00:32:56,000
Now you all know that kama produces results.

238
00:32:56,000 --> 00:33:06,760
At the beginning of one life, especially of let us say human beings, at the beginning

239
00:33:06,760 --> 00:33:18,440
of life as a human being, there is produced a type of consciousness, or there arises

240
00:33:18,440 --> 00:33:30,000
a type of consciousness, which is produced by the kama in the past.

241
00:33:30,000 --> 00:33:43,080
According to the teachings of abitama, consciousness needs a physical base to arise,

242
00:33:43,080 --> 00:33:51,520
especially for human beings and other beings like animals.

243
00:33:51,520 --> 00:34:01,560
So if there is no physical base, consciousness cannot arise in these beings.

244
00:34:01,560 --> 00:34:11,640
So at the first moment in the life of a human being, as I said, there is produced a type

245
00:34:11,640 --> 00:34:18,720
of consciousness, but along with that type of consciousness, material, some material properties

246
00:34:18,720 --> 00:34:21,720
are also produced.

247
00:34:21,720 --> 00:34:31,200
So the arising of some material properties and the arising of the particular consciousness

248
00:34:31,200 --> 00:34:36,120
is what we call rebirth.

249
00:34:36,120 --> 00:34:47,680
So rebirth consists of a type of consciousness and some material properties, both results

250
00:34:47,680 --> 00:34:51,720
of kama in the past.

251
00:34:51,720 --> 00:35:01,640
Since there are material properties in the first arising of matter and new life, consciousness

252
00:35:01,640 --> 00:35:03,840
can arise.

253
00:35:03,840 --> 00:35:09,720
Using the material properties as a base.

254
00:35:09,720 --> 00:35:20,760
And so the consciousness in the previous life ceases with depth.

255
00:35:20,760 --> 00:35:30,840
So no consciousness or no material properties move in to the new life, the new life is

256
00:35:30,840 --> 00:35:35,880
a new life and the old life ends with depth.

257
00:35:35,880 --> 00:35:44,920
And then there is a new life or arising of the consciousness and material properties as

258
00:35:44,920 --> 00:35:50,200
the result of kama, that percentage in the past.

259
00:35:50,200 --> 00:36:03,480
So we are not to say how can consciousness arise, why after death the senses have decayed.

260
00:36:03,480 --> 00:36:10,600
So the consciousness that arises in the new life depends on a new material base in that

261
00:36:10,600 --> 00:36:29,800
new life and it has nothing to do with the sense organs in the first life.

262
00:36:29,800 --> 00:36:36,280
Now a new life is a life that is newly produced.

263
00:36:36,280 --> 00:36:46,680
So although let us call new life second life and then the past life first life.

264
00:36:46,680 --> 00:36:56,920
So although the second life is a new life, it is not totally disconnected with the first

265
00:36:56,920 --> 00:36:59,320
life.

266
00:36:59,320 --> 00:37:04,160
It is the result of the kama in the past life.

267
00:37:04,160 --> 00:37:10,680
So as cause and effect, they are related, they have some kind of relationship.

268
00:37:10,680 --> 00:37:22,720
But nothing from the old life or nothing from the first life moves into the second life.

269
00:37:22,720 --> 00:37:30,480
Although nothing goes from one life to another, there is a kind of relation or connection

270
00:37:30,480 --> 00:37:36,400
between the first life and the second life that is as cause and effect.

271
00:37:36,400 --> 00:37:48,400
So that is why the kama of this person gives results to this continuity only and not

272
00:37:48,400 --> 00:37:50,880
to another continuity.

273
00:37:50,880 --> 00:37:56,720
Otherwise there will be chaos in the law of kama.

274
00:37:56,720 --> 00:38:10,840
My kama giving results to you or your kama giving results to me, so that cannot happen.

275
00:38:10,840 --> 00:38:27,880
And one thing important to note about death and rebirth is that rebirth is not the result

276
00:38:27,880 --> 00:38:42,160
of death, that is important.

277
00:38:42,160 --> 00:39:06,000
Although death immediately follows death, rebirth is not the result of death, now as you

278
00:39:06,000 --> 00:39:12,120
know, rebirth is the result of kama in the past.

279
00:39:12,120 --> 00:39:21,000
Not the result of death, but immediately after death, there is rebirth.

280
00:39:21,000 --> 00:39:33,240
So we are never to say that rebirth is the result of death, but we can say that rebirth

281
00:39:33,240 --> 00:39:46,360
is conditioned by death in the sense that death disappears so that rebirth can arise.

282
00:39:46,360 --> 00:39:50,520
It is something like giving a place to another person.

283
00:39:50,520 --> 00:39:58,320
So I am sitting in this place and if I locate this place, then another person can sit

284
00:39:58,320 --> 00:40:00,160
in its place.

285
00:40:00,160 --> 00:40:03,840
So I am the condition for another person.

286
00:40:03,840 --> 00:40:14,760
So in that sense, death is a condition for rebirth, but that does not mean that rebirth

287
00:40:14,760 --> 00:40:21,280
is caused by death or rebirth is the product of death.

288
00:40:21,280 --> 00:40:30,920
So they are two different things, that belongs to the old life and rebirth belongs to the

289
00:40:30,920 --> 00:40:32,840
new life.

290
00:40:32,840 --> 00:40:48,240
So we must never say that rebirth is the result of death or death brings about rebirth.

291
00:40:48,240 --> 00:40:55,720
They just vacate each place so that rebirth can take place, rebirth means rebirth consciousness

292
00:40:55,720 --> 00:41:02,240
and material properties along with it.

293
00:41:02,240 --> 00:41:13,560
If you want to know more about the process of death than rebirth, you study a bitama.

294
00:41:13,560 --> 00:41:21,160
So only in a bitama, the process of death and rebirth are explained in detail.

295
00:41:21,160 --> 00:41:26,880
And only when you have the knowledge of a bitama, do you understand correctly, otherwise

296
00:41:26,880 --> 00:41:31,200
you may misunderstand or you may make mistakes.

297
00:41:31,200 --> 00:41:43,120
So if you want to know more about the process, and please study a bitama, okay, I thought

298
00:41:43,120 --> 00:41:52,880
I would be brief, that is why I told you about the cartoon, but now it is not brief.

299
00:41:52,880 --> 00:41:56,840
It is almost time, I mean, regular time.

300
00:41:56,840 --> 00:42:06,240
Now one more story, but something about the elephants, in our countries, they use elephants

301
00:42:06,240 --> 00:42:09,200
to hold logs, right?

302
00:42:09,200 --> 00:42:15,240
So elephants are trained to push the logs, and they also they are trained to stop when

303
00:42:15,240 --> 00:42:20,560
they hear the siren, when they hear the bell.

304
00:42:20,560 --> 00:42:27,240
So it is said that elephants would stop as soon as they hear the bell, even if there are

305
00:42:27,240 --> 00:42:32,040
about only six inches of the log to push, they will not push it.

306
00:42:32,040 --> 00:42:39,200
They will stop dead at the sound of the siren or the bell.

307
00:42:39,200 --> 00:42:49,400
So today I will be like an elephant.

308
00:42:49,400 --> 00:43:04,520
See you Saturday, Saturday, Saturday.

