1
00:00:00,000 --> 00:00:16,520
Okay, good morning broadcasting from Bangkok, actually across the river from Bangkok in one

2
00:00:16,520 --> 00:00:25,640
of those delightful, one of these delightful hidden sanctuaries that really Bangkok is full

3
00:00:25,640 --> 00:00:37,040
of, isn't famous for, but should be Bangkok's really interesting city, describing

4
00:00:37,040 --> 00:00:54,160
where I am in an area called Banquet, which is highly urbanized, and so there I'm in

5
00:00:54,160 --> 00:01:05,680
his close to the mall, it's called, or type people say, the mall, which is a huge, well,

6
00:01:05,680 --> 00:01:14,960
a fairly big shopping complex, I think it's the well-known sort of landmark, just down

7
00:01:14,960 --> 00:01:22,840
the road from where I am, but as you come closer this way, you get to, there's the market

8
00:01:22,840 --> 00:01:31,160
where I go and arms around, and then coming even closer is this huge, they call, Tesco Lotus,

9
00:01:31,160 --> 00:01:44,440
it's a Tesco is this UK brand, and they have a huge supermarket, which I guess is,

10
00:01:44,440 --> 00:01:52,600
I'm not sure exactly, I guess it's, it's got groceries, no food, but also that

11
00:01:52,600 --> 00:02:00,320
everything you could possibly need in it, so basically everything for daily living and then

12
00:02:00,320 --> 00:02:12,560
some is in this one store, so it's big, and it's all over Thailand, but I'm about 50 meters

13
00:02:12,560 --> 00:02:19,360
from it, so you'd think it would be the, I don't know, 50 meters I can't change, but no

14
00:02:19,360 --> 00:02:27,200
more than 100 meters I think, so you'd think that being the case, this would be a pretty

15
00:02:27,200 --> 00:02:40,040
awful place to live and reside, but I'm looking out my window at a solitary about two feet

16
00:02:40,040 --> 00:02:51,160
thick with being salah fruit and watching the guy pick salah flowers off the tree, salah,

17
00:02:51,160 --> 00:02:56,040
the salah tree is the tree that the Buddha passed away under and was actually born under

18
00:02:56,040 --> 00:03:04,120
as well, so they say, then if I turn and look I have, there's a little bird on the lawn

19
00:03:04,120 --> 00:03:13,800
by the pond, my good day is actually on top of this goldfish pond, and they've built

20
00:03:13,800 --> 00:03:22,360
it actually half over top of it, and you can hear the birds singing and I don't see, barely

21
00:03:22,360 --> 00:03:30,240
see the building off in the distance among the trees, when there was a flooding a few

22
00:03:30,240 --> 00:03:35,600
years back it was actually, before the flooding it was actually much more lush, this

23
00:03:35,600 --> 00:03:41,280
is quite lush as it is, but many of the trees died from the extended flooding, they just

24
00:03:41,280 --> 00:03:50,240
couldn't handle the trees that were here, couldn't handle the water, and so what I mean

25
00:03:50,240 --> 00:03:58,160
to say is this is somehow right behind the lotus shopping mall, there's this wonderful

26
00:03:58,160 --> 00:04:04,440
little refuge that you can hear the people off in the background, but that's just the

27
00:04:04,440 --> 00:04:14,200
people who live here and run this place, it's great people, tomorrow we'll be having

28
00:04:14,200 --> 00:04:21,600
the monthly arms giving, they just happened to make it for that, so they last night

29
00:04:21,600 --> 00:04:32,520
the owner of the place, a judge came and invited me to give the talk for tomorrow's

30
00:04:32,520 --> 00:04:36,480
arms giving, I'm not sure if that's going to go through, because there was already

31
00:04:36,480 --> 00:04:41,320
a monk who was scheduled to give the talk, but she said because I'm here she wants

32
00:04:41,320 --> 00:04:49,720
me to give it, no just because it's a special case, so we'll see, it's the thing about

33
00:04:49,720 --> 00:04:57,600
being a monastic tincture, you have to be ready to give a talk on a moment, notice people

34
00:04:57,600 --> 00:05:04,520
spring these things on you, when I was at John Tongue last year my teacher just a few

35
00:05:04,520 --> 00:05:12,000
hours before they were to give this weekly talk to like a hundred meditators at the center,

36
00:05:12,000 --> 00:05:20,920
he just says, I want you to give the talk to me, but it's like that, anyway, so this

37
00:05:20,920 --> 00:05:31,300
is where I am, trying to give Dhamma every day, I feel like I shouldn't give lots and

38
00:05:31,300 --> 00:05:37,480
lots of Dhamma otherwise I'll run those things to say, or it just gets too much, you

39
00:05:37,480 --> 00:05:44,520
know, people will wind up having to process lots of information, so I was trying to

40
00:05:44,520 --> 00:05:53,160
give like five minutes a day, but I don't know what's enough and what's too much, but

41
00:05:53,160 --> 00:05:58,520
I was thinking about talking about today was another aspect of the four foundations of

42
00:05:58,520 --> 00:06:04,320
mindfulness, why there are four, so the other thing that the commentary says about why

43
00:06:04,320 --> 00:06:11,320
there are four foundations of mindfulness is because they correspond nicely with these

44
00:06:11,320 --> 00:06:25,640
four perversions, and that's probably not the best word for the concept, we pass that,

45
00:06:25,640 --> 00:06:41,960
we means, in this case, outside or transformed in the sense of perverted, perversion in

46
00:06:41,960 --> 00:06:48,160
in English has a bad connotation, we're not talking about child molesters or lawyers

47
00:06:48,160 --> 00:06:59,960
or this kind of thing, sexual, the sexual, people who have convoluted sexual preferences

48
00:06:59,960 --> 00:07:04,880
is how we look at the perversion, but we mean it in a rather scientific sense, perverted

49
00:07:04,880 --> 00:07:17,200
in the sense of twisted, you know, out of line with reality is actually what we mean,

50
00:07:17,200 --> 00:07:23,960
so misconception might be a better word, perversion just as too many bad connotations,

51
00:07:23,960 --> 00:07:34,600
so the four misconceptions or misconceptions misunderstandings, and these four are the

52
00:07:34,600 --> 00:07:41,680
misconception in regards to things that are not beautiful, that they are beautiful, the

53
00:07:41,680 --> 00:07:50,880
misconception in regards to things that are not pleasant, that they are pleasant, and

54
00:07:50,880 --> 00:08:01,160
the misconception that those things that are not permanent and finally the misconception

55
00:08:01,160 --> 00:08:12,480
that those things that are not self are self, we have these four misconceptions or perversions

56
00:08:12,480 --> 00:08:21,760
of perception or mistaken perceptions, and they're not just perceptions, there are actually

57
00:08:21,760 --> 00:08:28,800
three levels of misconception, but I think perversion works better because it's more general

58
00:08:28,800 --> 00:08:36,320
because we're not talking about conception or perception just, we're instead talking

59
00:08:36,320 --> 00:08:46,080
about three levels of perverted perversion, I wish there was a better word because

60
00:08:46,080 --> 00:08:53,960
if you understand this not to be like a really, not to be based in the way we understand

61
00:08:53,960 --> 00:09:08,680
the word pervert or perversion, it's twisted, that's the problem with Buddhism, in many

62
00:09:08,680 --> 00:09:16,440
ways it's very much outside of our daily discourse, the whole path and many of the concepts

63
00:09:16,440 --> 00:09:25,080
are very much contrary to the way we live our lives, and as I said, the way we look at

64
00:09:25,080 --> 00:09:31,120
the world is actually very, very wrong according to Buddhism, our whole paradigm is wrong,

65
00:09:31,120 --> 00:09:40,280
so many of the words have to be made fresh, and as a result you wind up English is good

66
00:09:40,280 --> 00:09:48,200
for that because you can play with words, you can turn something that's normally a noun

67
00:09:48,200 --> 00:09:52,920
into a verb or a verb into a noun and that kind of thing, and this is what the Buddha

68
00:09:52,920 --> 00:09:58,320
did with Pauli as well, this is what was done with Pauli's, the ability to put words together

69
00:09:58,320 --> 00:10:08,080
and make up words like pervertedness or something like that, finding new words, and

70
00:10:08,080 --> 00:10:13,440
using old words in new ways, they have to be used in new ways and they have to be defined

71
00:10:13,440 --> 00:10:23,040
in new ways, they have to be brought back to their roots many times, communication is important,

72
00:10:23,040 --> 00:10:30,360
this is why the Buddha called it actually language, language is a mastery, the ability

73
00:10:30,360 --> 00:10:40,600
to master languages is an important skill, so let's stick with perversion for now, understanding

74
00:10:40,600 --> 00:10:47,720
that it's not this kinky, it's not meant in a kinky sense, although even the word kink

75
00:10:47,720 --> 00:10:59,440
is a good word because it suggests something not quite straight, crooked, but so the point

76
00:10:59,440 --> 00:11:09,520
being there are three levels of perversion, there's the perversion of perception, this

77
00:11:09,520 --> 00:11:18,840
refers just to the level of experience, so in regards to beauty, you perceive that something

78
00:11:18,840 --> 00:11:28,360
ugly is beautiful, the feeling comes in the mind, the sense of something being beautiful,

79
00:11:28,360 --> 00:11:34,760
the second level is the perversion of thought that in regards to something ugly, once

80
00:11:34,760 --> 00:11:47,800
the perception arises, one thinks, boy that's beautiful, that thing is quite desirable,

81
00:11:47,800 --> 00:11:56,240
and the third one, third level is the level of view, so once one thinks that something's

82
00:11:56,240 --> 00:12:05,680
beautiful then one acquires the view that this is beautiful, yes, indeed my thought is correct,

83
00:12:05,680 --> 00:12:13,200
that is beautiful, so that thing is indeed beautiful, it's the affirmation of the thought,

84
00:12:13,200 --> 00:12:19,480
that it's a distinct level, these three levels of understanding of the distinction is quite

85
00:12:19,480 --> 00:12:27,360
useful, because a sort of pattern, take for example a sort of pattern, doesn't have the view

86
00:12:27,360 --> 00:12:35,180
if something is for example beautiful, doesn't take things to be beautiful, or doesn't

87
00:12:35,180 --> 00:12:42,480
believe things to be beautiful, but a sort of pattern still has the potential to think

88
00:12:42,480 --> 00:12:47,320
that things are beautiful, in other words the thought might arise, not think but not

89
00:12:47,320 --> 00:12:51,760
think in the sense of believe, but think in the sense of the thought can still arise

90
00:12:51,760 --> 00:13:00,480
for some to find it, that's beautiful, once that thought arises, these sort of pattern

91
00:13:00,480 --> 00:13:07,920
will immediately, or not necessarily immediately, but will not ever give rise to the belief

92
00:13:07,920 --> 00:13:13,640
that that thought is right, and generally they will immediately respond to that thought

93
00:13:13,640 --> 00:13:22,160
saying, but that's a silly thought, or with mindfulness, recognize that that thought is coming

94
00:13:22,160 --> 00:13:27,920
from their old habits of attachment and addiction and so on, and of course they'll still

95
00:13:27,920 --> 00:13:36,120
have the perception, the perceptions will still arise, at the most base level that something

96
00:13:36,120 --> 00:13:42,720
is beautiful, the traction of things will still exist, can still arise in the sort of

97
00:13:42,720 --> 00:13:50,080
example, so these levels are levels of perversion really, the views are the worst, the

98
00:13:50,080 --> 00:13:56,920
most perverse, and perverse in the sense that they are a go against reality, and the view

99
00:13:56,920 --> 00:14:04,800
is the worst because it's the strongest, it's the one that's going to inform one's actions

100
00:14:04,800 --> 00:14:09,800
the most, one has the view that certain things are beautiful, certain things are pleasant,

101
00:14:09,800 --> 00:14:20,160
certain things are permanent, or cysts stable, certain things are controllable, or self,

102
00:14:20,160 --> 00:14:26,320
the view is going to inform one's actions, much more than thoughts or perceptions, although

103
00:14:26,320 --> 00:14:35,440
those as well will influence, not to the same degree as one views, so these three levels,

104
00:14:35,440 --> 00:14:42,040
it's also useful to help us discriminate between what we, especially as beginner meditators

105
00:14:42,040 --> 00:14:50,800
can influence, we may not be able to influence our perceptions as easily, but we can quite

106
00:14:50,800 --> 00:14:57,760
easily influence our views, and this can change quite easily, relatively speaking, compared

107
00:14:57,760 --> 00:15:01,840
to one's perception, so a meditator might find that for years or even for lifetimes

108
00:15:01,840 --> 00:15:09,600
they're still plagued by these perceptions and even thoughts that are go against reality,

109
00:15:09,600 --> 00:15:15,800
but quite quickly in the practice they should be able to give up their views, the views

110
00:15:15,800 --> 00:15:21,760
that these thoughts are right, and just these seeing these thoughts as habitual, as remnants

111
00:15:21,760 --> 00:15:29,560
of old habits of one's old view, so those are the three levels, this is quite a, so one

112
00:15:29,560 --> 00:15:37,080
of, I think one of the great important teachings that we have of the Buddha, these four

113
00:15:37,080 --> 00:15:43,200
perceptions and the three levels of perversion, something that all Buddhist meditators

114
00:15:43,200 --> 00:15:49,400
should know, so just briefly what do we mean by them and why do they correspond with

115
00:15:49,400 --> 00:15:56,840
the four foundations, it's because beauty and it's very much tied in with the body, so

116
00:15:56,840 --> 00:16:02,440
in your mindful of the body it helps you get rid of the perversion of beauty, seeing

117
00:16:02,440 --> 00:16:07,120
that the body is not actually this beautiful thing, and objectively the body is quite

118
00:16:07,120 --> 00:16:14,680
disgusting really, it's amazing that human beings are able to find the body beautiful,

119
00:16:14,680 --> 00:16:31,240
there are certain things that are in a gross sense, geometrically sound I suppose are simple

120
00:16:31,240 --> 00:16:50,920
or so in the sense the face can often be quite complementary, the aspects of the face or

121
00:16:50,920 --> 00:16:58,120
there's not much else, I mean the rest is the shape of the body is really just arbitrary

122
00:16:58,120 --> 00:17:02,360
and our attraction to it is completely arbitrary, it's based on lifetime after lifetime of

123
00:17:02,360 --> 00:17:11,320
built up habitual recognition as being something that brings pleasure, it's habitual

124
00:17:11,320 --> 00:17:15,440
belief that this is going to make me happy with the attraction to this, the association

125
00:17:15,440 --> 00:17:25,920
with it, the contact with it, but much of the body is actually disgusting, like the smell

126
00:17:25,920 --> 00:17:35,360
of the body, the feeling of the body, the texture, there's nothing here, if it was in a different

127
00:17:35,360 --> 00:17:42,040
form, there's nothing here that we would find at all desirable if it wasn't something

128
00:17:42,040 --> 00:17:53,880
we recognize as me, as mine, or as a source of potential sexual pleasure usually or comfort,

129
00:17:53,880 --> 00:18:02,000
emotional comfort in terms of the embracing and connecting and so on, kissing and hugging

130
00:18:02,000 --> 00:18:08,240
and all this, or even the love for family members that kind of thing, but all this recognition

131
00:18:08,240 --> 00:18:14,480
is really all that keeps us attached to it, there's no sugar and spice and all things

132
00:18:14,480 --> 00:18:27,440
nice inside, even the female body, unfortunately, but sorry to say, there certainly are

133
00:18:27,440 --> 00:18:33,200
no diamonds, there are many things in the world that are objectively more symmetrical

134
00:18:33,200 --> 00:18:41,560
and easier from an objective point of view to understand the attraction to diamonds, gold,

135
00:18:41,560 --> 00:18:49,080
things that are like snowflakes, maybe, but the human body is a bit like a cesspool,

136
00:18:49,080 --> 00:18:56,200
really, full of urine and feces and sweat and grease and blood and past, all things that

137
00:18:56,200 --> 00:19:03,360
none of us really want to see, or most of us are repulsed by, let the body sit around for

138
00:19:03,360 --> 00:19:12,400
a while without washing it, without doing all the things that we do to cover up, it's

139
00:19:12,400 --> 00:19:19,480
ugliness, and it becomes quite repulsive, this is from a conventional point of view,

140
00:19:19,480 --> 00:19:24,240
of course, objectively speaking, there's nothing repulsive even about a cesspool, a cesspool

141
00:19:24,240 --> 00:19:29,920
just is what it is, the smell of a cesspool only repulses us because of habit and the

142
00:19:29,920 --> 00:19:35,240
way the body is made up, it's designed to be repulsed by a cesspool, there's nothing

143
00:19:35,240 --> 00:19:43,200
different between a diamond and a cesspool, objectively speaking, but certainly it's easy

144
00:19:43,200 --> 00:19:48,480
in the conventional point of view to point out how the body is certainly not beautiful,

145
00:19:48,480 --> 00:19:52,160
so meditation helps us to see that once we're more objective, we're able to let go of

146
00:19:52,160 --> 00:20:00,400
this somewhat ridiculous attachment to the body. Mindfulness of the feelings is best for

147
00:20:00,400 --> 00:20:06,040
helping us overcome the perversion of pleasure, and in perverted sense that those things

148
00:20:06,040 --> 00:20:11,000
that aren't pleasurable are pleasurable, and once you watch the feelings you come to see

149
00:20:11,000 --> 00:20:23,640
that they're objectively just phenomena that arise and cease, and in fact they're incessant

150
00:20:23,640 --> 00:20:31,120
and they're invasive, you can't stop the feelings from coming, that pleasant feeling,

151
00:20:31,120 --> 00:20:34,360
sometimes unpleasant feeling sometimes, but even the pleasant ones because they arise

152
00:20:34,360 --> 00:20:41,280
and cease, arise, and cease, eventually want to get sick of them and realizes that these feelings

153
00:20:41,280 --> 00:20:50,280
are not really of any benefit, they're unpredictable and they're useless, they serve no benefit.

154
00:20:50,280 --> 00:20:55,880
Mindfulness of the feelings is useful for them. Mindfulness of the mind helps us to see

155
00:20:55,880 --> 00:21:02,560
impermanence, when we take the mind to be stable and we try to make the mind stable, we try

156
00:21:02,560 --> 00:21:07,200
to make reality stable, and in fact everything that we try to make stable is circumscribed

157
00:21:07,200 --> 00:21:17,200
by the fact that reality is based on the mind. When you talk about the table, the bed,

158
00:21:17,200 --> 00:21:25,640
all of the things around you, you're actually describing experiences that are circumscribed

159
00:21:25,640 --> 00:21:33,240
by the mind that only last as long as that one mind state that observes them, and you talk

160
00:21:33,240 --> 00:21:37,440
about anything in the world, you're actually talking about your experiences, and when you

161
00:21:37,440 --> 00:21:42,720
watch the mind you come to see that they're all circumscribed by the, they're all limited

162
00:21:42,720 --> 00:21:49,880
by the nature of the mind to arise and cease. So mindfulness of the mind helps, overcome

163
00:21:49,880 --> 00:21:57,960
this idea of permanence. Mindfulness of the dhammas helps overcome the perversion of thinking

164
00:21:57,960 --> 00:22:04,320
things that are not self or self. You're mindful of the various dhammas, various realities

165
00:22:04,320 --> 00:22:12,920
or the nature of reality. The dhammas in many ways describe how reality can be broken

166
00:22:12,920 --> 00:22:21,440
down into experience. So includes the hindrances which are basically the emotions, includes

167
00:22:21,440 --> 00:22:32,040
the six senses, the five aggregates, all the many things that make up reality. Mindfulness

168
00:22:32,040 --> 00:22:38,200
of these things helps overcome the idea that we are in control, especially of our emotions

169
00:22:38,200 --> 00:22:46,680
and our senses, and also that there's any core. It helps us realize that reality is just

170
00:22:46,680 --> 00:22:53,280
a bunch of experiences, that the five aggregates don't describe a being. You can't look

171
00:22:53,280 --> 00:22:58,640
inside the body and find the five aggregates, but the five aggregates describe a paradigm

172
00:22:58,640 --> 00:23:03,960
for reality that is based on experience. Each experience is made up of five things called

173
00:23:03,960 --> 00:23:13,000
the five aggregates that arise at the six senses. They're six types. So they're seeing

174
00:23:13,000 --> 00:23:18,680
experience, they're hearing experience, smelling, tasting, feeling, thinking experience.

175
00:23:18,680 --> 00:23:29,200
And that's it. There's no self in those souls. So this is another reason for having

176
00:23:29,200 --> 00:23:37,200
four foundations of mindfulness, quite a good one. And each of these four things, again,

177
00:23:37,200 --> 00:23:46,640
having three levels of intensity, quite useful to think about in our meditation, again,

178
00:23:46,640 --> 00:23:56,040
helping us to break down the breakdown, the objects of experience, break down our

179
00:23:56,040 --> 00:24:00,600
departments, break down our problems, help us to see what is the problem, help us to

180
00:24:00,600 --> 00:24:06,600
overcome the problem. So that way, start to look at reality as it truly is. And it's a means

181
00:24:06,600 --> 00:24:14,280
of helping overcome our misconceptions and misunderstandings. The things that actually cause

182
00:24:14,280 --> 00:24:20,840
a suffering when we're trying to find happiness. So there's the demo for today. Thank you all

183
00:24:20,840 --> 00:24:27,840
for tuning in and be well.

