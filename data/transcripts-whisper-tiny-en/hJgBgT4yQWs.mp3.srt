1
00:00:00,000 --> 00:00:07,000
Emma has a question, I don't have a Buddhist centre in my hometown, so I cannot meditate

2
00:00:07,000 --> 00:00:12,840
in a group or have a local centre. What can I do? Would it be useful for me to go in

3
00:00:12,840 --> 00:00:19,000
a retreat in a faraway centre, or would it be too much for a new practitioner?

4
00:00:19,000 --> 00:00:29,000
Hello, Emma. Well, I can only recommend to go to a centre if possible.

5
00:00:29,000 --> 00:00:34,000
The more important though is to find a good teacher.

6
00:00:34,000 --> 00:00:41,000
Someone you, you can face this. This is a bit difficult to find someone if you don't know anybody,

7
00:00:41,000 --> 00:00:46,000
if you don't have a group or can't find out. But I'm sure that one to you,

8
00:00:46,000 --> 00:00:52,000
some people who may know the internet may give you some good advice, but having a good teacher,

9
00:00:52,000 --> 00:00:59,000
some guys to a good retreat is very beneficial.

10
00:00:59,000 --> 00:01:07,000
Of course, more support you will have if you go to weekly meeting or monthly meeting,

11
00:01:07,000 --> 00:01:13,000
you can start your own group. This is also possible. If you can't go away,

12
00:01:13,000 --> 00:01:18,000
maybe you have other people interested or friends and just do a meditation,

13
00:01:18,000 --> 00:01:22,000
but to start off, it's good to go to the retreat.

14
00:01:22,000 --> 00:01:27,000
I mean, we find that even here, I find that even here that we have these group meetings

15
00:01:27,000 --> 00:01:33,000
and group meditations and forces us to stay on track very much.

16
00:01:33,000 --> 00:01:39,000
And another thing you can do is what's an idea. I mean, just throwing it out there,

17
00:01:39,000 --> 00:01:43,000
but some people that the Sri Lankan people have been really encouraging in doing,

18
00:01:43,000 --> 00:01:48,000
because Sri Lankan people abroad have just contacted me and said,

19
00:01:48,000 --> 00:01:52,000
hey, could you lead a course over Skype? We're going to get a group of people together.

20
00:01:52,000 --> 00:01:58,000
Could you give us a talk and lead us through meditation, guided meditations?

21
00:01:58,000 --> 00:02:02,000
I would be doing it sometimes once a week or so on.

22
00:02:02,000 --> 00:02:07,000
Next week, I'm doing one with a group in Houston, Texas, in America.

23
00:02:07,000 --> 00:02:15,000
That's an option to have, and not just for myself, it's an option to get teachings from monks around the world.

24
00:02:15,000 --> 00:02:20,000
I think, depending on what part of the world you're in and what sort of a group you have,

25
00:02:20,000 --> 00:02:28,000
there are teachers who you can contact and do internet-based teachings. It's quite easy.

26
00:02:28,000 --> 00:02:34,000
So that's an option. As far as going to a far-away center,

27
00:02:34,000 --> 00:02:42,000
which is what your question is, I certainly think it's not too much for a new meditator.

28
00:02:42,000 --> 00:02:47,000
I mean, you consider in Thailand. Thailand has many more tourists than Sri Lankan.

29
00:02:47,000 --> 00:02:50,000
It's very, I don't think we'll get any.

30
00:02:50,000 --> 00:02:54,000
We had only we've had any Sri Lankan tourists who just said, oh,

31
00:02:54,000 --> 00:02:58,000
I'm in Sri Lankan already, and could I come from meditation?

32
00:02:58,000 --> 00:03:01,000
We don't really get that here. But in Thailand, it was all the time.

33
00:03:01,000 --> 00:03:05,000
We'd take a cooking course, one week, meditation course, the next week.

34
00:03:05,000 --> 00:03:10,000
They were on the spiritual, or the tie tour.

35
00:03:10,000 --> 00:03:13,000
We wanted to get everything tie, including tie meditation.

36
00:03:13,000 --> 00:03:18,000
Great massage. Massage one week, cooking next week.

37
00:03:18,000 --> 00:03:21,000
Insight meditation, enlightenment the next week.

38
00:03:21,000 --> 00:03:27,000
This is the spiritual tourist.

39
00:03:27,000 --> 00:03:33,000
And so that's really what we've always expected, is to have newbies, newcomers,

40
00:03:33,000 --> 00:03:35,000
people who have no instruction.

41
00:03:35,000 --> 00:03:40,000
And sometimes that's actually the best, because they don't have preconceived notions.

42
00:03:40,000 --> 00:03:45,000
We sometimes have people who have sat meditation courses and other traditions,

43
00:03:45,000 --> 00:03:52,000
and have more trouble than a new meditator, because they have attachments and ideas and expectations,

44
00:03:52,000 --> 00:03:57,000
based on their other traditions and their other practices,

45
00:03:57,000 --> 00:04:01,000
which when we're talking about this, it becomes a habit.

46
00:04:01,000 --> 00:04:07,000
You don't have those habits, so you're a clean slate, and that makes it a lot easier.

47
00:04:07,000 --> 00:04:14,000
On top of that, what I found in our case is that what we get now

48
00:04:14,000 --> 00:04:17,000
are not beginner meditators anymore.

49
00:04:17,000 --> 00:04:21,000
Because you have our booklet, this booklet I put together on how to meditate.

50
00:04:21,000 --> 00:04:25,000
You have videos explaining the theory behind the meditation.

51
00:04:25,000 --> 00:04:30,000
You have our question and answer form, where people are asking questions about their meditation,

52
00:04:30,000 --> 00:04:32,000
or reading other people's questions.

53
00:04:32,000 --> 00:04:38,000
So what we've been getting now is people who are fully prepared with theory and even practice,

54
00:04:38,000 --> 00:04:44,000
and who have been practicing daily on their own, working up to the course.

55
00:04:44,000 --> 00:04:49,000
And you may not have that yet, but if, for example, you were thinking of coming here,

56
00:04:49,000 --> 00:04:55,000
you have all the resources necessary to get started and to get a huge head start,

57
00:04:55,000 --> 00:05:00,000
which has really made it incredibly easy for most of our meditators to get through the course,

58
00:05:00,000 --> 00:05:05,000
to live in a jungle, to put up with any sort of harsh environment or harsh conditions,

59
00:05:05,000 --> 00:05:08,000
because they've already prepared themselves.

60
00:05:08,000 --> 00:05:10,000
They've already got this great head start.

61
00:05:10,000 --> 00:05:16,000
So with one exception, I think everyone who's come here has had no trouble

62
00:05:16,000 --> 00:05:25,000
finishing the course because they've already become quite prepared before arriving.

63
00:05:25,000 --> 00:05:29,000
So I wouldn't be afraid of that in either sense.

64
00:05:29,000 --> 00:05:35,000
First of all, that we are perfectly equipped to deal with totally new meditators.

65
00:05:35,000 --> 00:05:38,000
And secondly, you don't have to be a new meditator.

66
00:05:38,000 --> 00:05:43,000
Start meditating now while you don't have a group while you don't have a meditation center.

67
00:05:43,000 --> 00:05:51,000
And once the other thing is that once you do go to a far away meditation center wherever it is and do a course,

68
00:05:51,000 --> 00:05:59,000
then how easy will it be if you've done a substantial course in an authentic environment,

69
00:05:59,000 --> 00:06:03,000
how easy will it be for you to start your own meditation group?

70
00:06:03,000 --> 00:06:10,000
I mean, not perfectly easy, but much easier than if you hadn't done it.

71
00:06:10,000 --> 00:06:18,000
And you'll find that you're actually able to bring people together and maybe over Skype, you know, contact someone like me

72
00:06:18,000 --> 00:06:26,000
who does these kind of crazy things and you don't get support even in your hometown.

73
00:06:26,000 --> 00:06:35,000
So, you know, many options, one should not despair in this day and age of being in a place without local resources.

74
00:06:35,000 --> 00:06:47,000
We're now the global village and I think there is some aspect of that that we have to take into Buddhism or get left behind and overwhelmed by it all.

75
00:06:47,000 --> 00:06:58,000
Because the world is not static and if Buddhism is not prepared to become a part of that change, then it will just come back and overwhelm us.

76
00:06:58,000 --> 00:07:07,000
And in the end Buddhism will be wiped out by people who have no clue about the benefits and the importance of the Buddha's teaching.

77
00:07:07,000 --> 00:07:12,000
On the other hand, if the Buddha's teaching is out there for the whole world, then we'll ride the wave.

78
00:07:12,000 --> 00:07:19,000
We'll be supported by the vast numbers of people who are involved.

79
00:07:19,000 --> 00:07:28,000
I mean, if you look, now there's like 1.5 million people who have watched the videos that we've been putting out.

80
00:07:28,000 --> 00:07:33,000
There's 7,000 people subscribed to our channel now.

81
00:07:33,000 --> 00:07:47,000
And on our own website, it's just daily people are adding their names or signing up for our website, joining in and taking part in the conversation.

82
00:07:47,000 --> 00:07:53,000
There is a real potential regardless of where you live to get involved.

83
00:07:53,000 --> 00:07:57,000
But simply put, yeah, I'd say go for it.

84
00:07:57,000 --> 00:07:59,000
Don't be afraid of being a newbie.

85
00:07:59,000 --> 00:08:04,000
Either take the time to both take the time to develop yourself.

86
00:08:04,000 --> 00:08:09,000
But even if you haven't had the time and you're ready to come to a meditation course, don't worry.

87
00:08:09,000 --> 00:08:15,000
We'll do all the work in preparing you and developing you in the practice.

88
00:08:15,000 --> 00:08:17,000
Of course, the longer you can say, the better.

89
00:08:17,000 --> 00:08:22,000
And if you're prepared enough, we'll probably invite you to our day in them.

90
00:08:22,000 --> 00:08:27,000
And you'll just never leave.

91
00:08:27,000 --> 00:08:49,000
It's something to add.

