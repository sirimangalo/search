1
00:00:00,000 --> 00:00:29,260
Okay, so good evening everyone, we are gathered here in the second life in the dear

2
00:00:29,260 --> 00:00:47,260
park, and now I've been asked, I've been asked about this by various people over the years,

3
00:00:47,260 --> 00:00:56,100
when I showed second life to my Thai supporters in Los Angeles, they, I think were somewhat

4
00:00:56,100 --> 00:01:08,600
amused at the concept of going into a digital world like this.

5
00:01:08,600 --> 00:01:15,160
Recently someone asked, doesn't this go against your principles of trying to understand

6
00:01:15,160 --> 00:01:22,360
reality, how can you, on the one hand, claim to be interested in understanding reality and

7
00:01:22,360 --> 00:01:37,360
on the other hand, engage in self-deception, the self-deception of this virtual reality?

8
00:01:37,360 --> 00:01:47,000
The answer is quite simple, I mean, I talked about this a couple of days a few days ago.

9
00:01:47,000 --> 00:02:02,220
There's no difference between virtual reality and so-called ordinary reality, so anyway

10
00:02:02,220 --> 00:02:13,820
tonight we are attempting to do a video record of the idea park, which just allows me

11
00:02:13,820 --> 00:02:26,900
to upload the talk to YouTube as well, anyway, neither here nor there, but yeah,

12
00:02:26,900 --> 00:02:32,020
so this is being recorded for YouTube, we're live here in the second life, and there's

13
00:02:32,020 --> 00:02:43,180
also a live, should be a live audio stream if everything's working as planned.

14
00:02:43,180 --> 00:02:56,100
The live audio stream will also be recorded and placed on the website, so tonight I thought

15
00:02:56,100 --> 00:03:08,020
I would talk about the good, I had a chat with my mother, a text chat with my mother this

16
00:03:08,020 --> 00:03:15,180
evening, she wants to get me a winter hat for Christmas, while I actually asked me what

17
00:03:15,180 --> 00:03:24,980
I wanted for Christmas, because I'll be going to visit her in Florida, so she was texting

18
00:03:24,980 --> 00:03:45,740
me links to winter hats on Amazon, and I asked her how things are in America these days,

19
00:03:45,740 --> 00:04:05,780
and she responded with mixed feelings, so we started talking about letting the world, letting

20
00:04:05,780 --> 00:04:16,580
the world's problems get to you, letting issues and conflicts get to you, how it's easy

21
00:04:16,580 --> 00:04:30,820
to let them overwhelm you, and we got to talking about the presidential candidates and

22
00:04:30,820 --> 00:04:56,100
how she had to unfriend people, because they were talking bad about her candidate, and

23
00:04:56,100 --> 00:05:03,220
there's been a lot of, as everyone knows, there's been a lot of this in the past, here

24
00:05:03,220 --> 00:05:13,780
and a half of vicious, what we call partisanship, but really just means hate-mongering

25
00:05:13,780 --> 00:05:25,140
and fear-mongering and demonizing, you know, terribly on all some stuff, we're always fighting

26
00:05:25,140 --> 00:05:27,380
against something, right?

27
00:05:27,380 --> 00:05:35,580
Seems too often were in society were anti-this and anti-that and protesting this and protesting

28
00:05:35,580 --> 00:05:45,900
that, and I told her, you know, I may not like someone, but I mean, I agree with someone,

29
00:05:45,900 --> 00:05:52,660
but I would never express hateful opinions of them or hateful thoughts towards them, so

30
00:05:52,660 --> 00:05:57,740
I said to my mother, but I was thinking about it, and I think the problem is our focus

31
00:05:57,740 --> 00:06:03,900
on the negative, and I think I told my mother, I said, you're such a positive person,

32
00:06:03,900 --> 00:06:09,900
because she really is, and sometimes I kind of, in my mind, anyway, criticize her for it,

33
00:06:09,900 --> 00:06:20,020
because I think, ah, you know, it's somewhat naive or self-decept-deceiving to always look

34
00:06:20,020 --> 00:06:23,700
on the bright side of life, right?

35
00:06:23,700 --> 00:06:30,980
But their aspects of positivity that I think are really important, it doesn't mean not acknowledging

36
00:06:30,980 --> 00:06:31,980
the bad.

37
00:06:31,980 --> 00:06:38,660
I mean, sometimes it does, and that's my fears that it can be ignoring the problems, right?

38
00:06:38,660 --> 00:06:44,660
Trying to find ways to, so my mother hasn't gone on the news and hasn't, she doesn't,

39
00:06:44,660 --> 00:07:01,260
yeah, anyway, but positivity also can refer to or aren't aspect of it, it's not getting

40
00:07:01,260 --> 00:07:06,900
mired down, caught up in the problem.

41
00:07:06,900 --> 00:07:12,500
It's quite interesting as a Buddhist, especially as a Buddhist monk to know where to draw

42
00:07:12,500 --> 00:07:19,020
the line, and I think that's the line is knowing the problems but not getting caught up

43
00:07:19,020 --> 00:07:20,020
in them.

44
00:07:20,020 --> 00:07:30,260
You want to help people, interesting thing about helping people is you never really help

45
00:07:30,260 --> 00:07:36,980
in a really ultimate sense or in a real, in a lasting way if you, by getting involved

46
00:07:36,980 --> 00:07:47,540
in the problems, to help you give practically is a practical value and sometimes important,

47
00:07:47,540 --> 00:07:56,180
but it's not actually the spiritual or the psychological or the help in an ultimate sense.

48
00:07:56,180 --> 00:08:04,500
You only get that by staying out of their problems and by working in the realm of absolute

49
00:08:04,500 --> 00:08:09,460
reality, what's really going on?

50
00:08:09,460 --> 00:08:16,180
Don't get mired down in things like politics, society, we're just talking this morning

51
00:08:16,180 --> 00:08:25,540
to a meditator about generosity and whether it's good karma or bad karma to give to

52
00:08:25,540 --> 00:08:27,860
say a drug addict.

53
00:08:27,860 --> 00:08:35,740
That's really a good question, I mean it's a question that often comes up, but goodness

54
00:08:35,740 --> 00:08:46,420
isn't there, goodness isn't in the actual act, it's in the mind, goodness is in ultimate

55
00:08:46,420 --> 00:08:56,860
reality, karma is based on your state of mind, your quality of your mind when you give,

56
00:08:56,860 --> 00:09:04,460
when you do any sort of goodness and so it comes not from actually, the goodness comes

57
00:09:04,460 --> 00:09:12,140
not from actually providing food to a homeless person, it comes from the qualities of

58
00:09:12,140 --> 00:09:23,020
mind that leads you to give to a homeless person and so it has very much to do with natural

59
00:09:23,020 --> 00:09:29,020
wisdom, not any intellectual idea of what this person might do with your gift, but a natural

60
00:09:29,020 --> 00:09:37,380
wisdom of what's appropriate at that moment, which often takes into account the situation,

61
00:09:37,380 --> 00:09:45,700
but it's very natural, there's a natural sense of the person, the recipients, goodness,

62
00:09:45,700 --> 00:09:51,700
and there's not no worry or concern for really annoying their goodness, but there's

63
00:09:51,700 --> 00:09:57,100
a natural sense and you may do the wrong thing, you may give to someone and they go out

64
00:09:57,100 --> 00:10:02,180
and buy drugs with it, but that's really not the point, the point is your sense of clarity

65
00:10:02,180 --> 00:10:09,060
of mind and purpose and intention, you know, the desire to help someone, often simply

66
00:10:09,060 --> 00:10:14,300
the desire to fulfill someone's request, someone asks you for money, you fulfill their

67
00:10:14,300 --> 00:10:29,820
request, in order to maintain harmony, maintain rightness, you give to little and the

68
00:10:29,820 --> 00:10:35,540
person gets upset, you give too much and the person feels entitled, right, if you give

69
00:10:35,540 --> 00:10:42,180
a lot to someone who doesn't deserve it, also loses the sense of harmony and the

70
00:10:42,180 --> 00:10:53,180
natural wisdom, there's a, when you're mindful, you can sense, to some extent, or you're

71
00:10:53,180 --> 00:11:01,580
in tune with your sensations, with your instincts, whatever they may be, and so you take

72
00:11:01,580 --> 00:11:13,020
them, you have clarity of mind, and that clarity of mind provides you with sense of what

73
00:11:13,020 --> 00:11:18,860
is good, and so you focus on the good, you don't actually have to concern yourself with

74
00:11:18,860 --> 00:11:28,420
the particulars of the person's condition beyond assessing their sincerity and getting

75
00:11:28,420 --> 00:11:43,760
a sense of how they strike you, how they play into this phenomenological analysis or

76
00:11:43,760 --> 00:11:53,700
estimation of the situation, of giving, that's somewhat of an interesting question, don't

77
00:11:53,700 --> 00:11:57,860
want to talk too much about that, though it's an interesting aspect, today what I wanted

78
00:11:57,860 --> 00:12:08,820
to go over is goodness, talk about goodness as a practice, and emphasize this as I think

79
00:12:08,820 --> 00:12:16,900
a proper way of understanding the Buddhist teachings, not to dismiss the problems, but

80
00:12:16,900 --> 00:12:25,500
to try and solve the problems, through goodness, right, goodness is, problem solving is

81
00:12:25,500 --> 00:12:32,180
one way of describing goodness, it's only good if there's a reason for it, and there's

82
00:12:32,180 --> 00:12:40,500
a benefit to it, and a problem or a challenge or situation being ameliorated or addressed,

83
00:12:40,500 --> 00:12:46,380
and so that's a sort of positivity that I think we could get around and put it in.

84
00:12:46,380 --> 00:12:52,100
For example, as I was saying about talking bad about people, even public figures hating

85
00:12:52,100 --> 00:13:07,020
them, denouncing them, remember the Buddha's words, he said, if a person's speech is no

86
00:13:07,020 --> 00:13:13,940
good, but their actions are good, or if their speech in actions are no good, but their

87
00:13:13,940 --> 00:13:20,220
speech is good, or if their actions in speech are no good, but their thoughts are good,

88
00:13:20,220 --> 00:13:27,140
he said, you should think of them like a cloth, if you find this cast off cloth on those

89
00:13:27,140 --> 00:13:32,420
side of the road, and part of it's rotten, you should tear off the rotten part, throw

90
00:13:32,420 --> 00:13:44,300
it away and focus on and carry and keep the good part, don't throw the whole robo away,

91
00:13:44,300 --> 00:13:51,500
or you should think of them like a pool of water, clear, nice pool of water with this

92
00:13:51,500 --> 00:13:58,940
layer of scum on top, but if you're really thirsty, what you do is you put your hands

93
00:13:58,940 --> 00:14:06,300
in the one, your part, you use it to part the scum and get at the clean water underneath.

94
00:14:06,300 --> 00:14:12,260
Of course, in India these days, I wouldn't recommend it.

95
00:14:12,260 --> 00:14:17,140
There's places in the world I wouldn't recommend it, where I grew up you could do that,

96
00:14:17,140 --> 00:14:21,860
and I guess back in the Buddha's time you could as well, where I grew up you could just

97
00:14:21,860 --> 00:14:31,380
go into the forest and get water, but the point being you dismiss to some extent, maybe

98
00:14:31,380 --> 00:14:40,420
not dismiss, but you put aside the bad aspects of the person because you're trying to solve

99
00:14:40,420 --> 00:14:45,300
a problem. It doesn't solve a problem, it's interesting to see what would go on in America

100
00:14:45,300 --> 00:14:51,060
in regards to these elections, especially in what goes on in the world in regards to religious

101
00:14:51,060 --> 00:15:00,420
conflict between Christians and Muslims and Jews, and even Buddhists and Hindus, and how we deal

102
00:15:00,420 --> 00:15:08,420
with our problems, how the Buddhists in Sri Lanka dealt with the Tamil problem, Tamil problem

103
00:15:08,420 --> 00:15:16,980
dealt with the Tamils, because you can of course react in many different ways. There are many

104
00:15:16,980 --> 00:15:28,420
different things you can do, but the old Buddhist joke comes to mind, not joke, but the twist

105
00:15:28,420 --> 00:15:39,460
is don't just do something, sit there, because anyone can do something, try and fix things,

106
00:15:39,460 --> 00:15:44,900
this is what happens, we say, do something, and then you try to give advice, or you try to solve

107
00:15:44,900 --> 00:15:54,660
the problem, and the real question is what works, and what does it mean to work? Does it work

108
00:15:54,660 --> 00:16:00,900
when you make your opponent angry? Does it work when you shame them? Does it work when you

109
00:16:00,900 --> 00:16:12,580
vilify them? Not what makes you feel good, not what gains you allies and a sense of righteousness,

110
00:16:14,180 --> 00:16:22,500
you know? A lot of protest is terribly righteous, self-righteous, makes you feel good about yourself,

111
00:16:22,500 --> 00:16:28,980
I'm on the right side, well, who cares? If you're on the right side of the war, it's still a war.

112
00:16:30,580 --> 00:16:34,500
If you're going to war, it's a failure, I would say, of both sides, it's a failure of everyone.

113
00:16:36,020 --> 00:16:44,820
So I thought about this confrontation on the bus, and I was somewhat concerned about my behavior,

114
00:16:44,820 --> 00:16:50,020
because I was like, did I really have gotten involved there? Probably. If I got involved like

115
00:16:50,020 --> 00:16:57,220
I had in America, I could have been sued for misconduct, because these two guys were yelling at

116
00:16:57,220 --> 00:17:02,820
each other on the bus, and there was a wheelchair in the middle of the situation, and I picked

117
00:17:02,820 --> 00:17:09,140
the wheelchair up and moved it just to try to solve the problem. But I look at that, and I kind of

118
00:17:09,140 --> 00:17:13,860
say, you know, it's got to be kind of like that. You've got these two sides fighting, and they're

119
00:17:13,860 --> 00:17:26,260
stressed, and there are practical issues that are increasing the stress. And sometimes it's really

120
00:17:26,260 --> 00:17:33,540
the focus on the practical issues, solving the problems. So rather than taking sides, I decided I

121
00:17:33,540 --> 00:17:39,220
would be helpful. You know, do a good deed, pick up this guy's wheelchair, and not that I took

122
00:17:39,220 --> 00:17:46,980
his side. He was more belligerent than the other guy, but to solve the problem, you know,

123
00:17:47,540 --> 00:17:53,300
to do something good. And sometimes that takes, because it was basically an ignoring of the conflict.

124
00:17:53,300 --> 00:18:00,500
I'm not that comfortable talking about myself in this way, but I think it was a good choice.

125
00:18:00,500 --> 00:18:07,140
And I think that's maybe not the best choice or exactly right, but that kind of thing,

126
00:18:07,140 --> 00:18:13,060
I couldn't figure out what to do to help these two people. How do you stop them from yelling at

127
00:18:13,060 --> 00:18:22,340
each other? And I think you don't. I think in many cases, this is the key, is side stepping,

128
00:18:23,060 --> 00:18:34,660
the conflict, and bettering everyone's situation. Because so often, if they had all focused on

129
00:18:34,660 --> 00:18:38,980
the task at hand, we could have all gotten to class on the time the guy in the wheelchair had an

130
00:18:38,980 --> 00:18:45,540
exam which he ended up missing. I think other people on the bus, the same thing, had exams that

131
00:18:45,540 --> 00:18:57,620
they were going to be late for. If we worked more on problem solving than conflict and winning

132
00:18:57,620 --> 00:19:07,300
wars, winning conflicts, right? Wars, I think, don't start because of genuine problems.

133
00:19:07,300 --> 00:19:15,220
They start because of the creation of problem and the focus on not the problem, but the conflict,

134
00:19:16,340 --> 00:19:21,380
conflict escalation in many ways. We escalate conflict. So it was really interesting to see how

135
00:19:21,380 --> 00:19:28,420
just for example this bus thing occurred. Guy in the wheelchair has a problem needs to get on the

136
00:19:28,420 --> 00:19:39,300
bus. Driver tries to do the good thing, so far no conflict, but the man starts to talk too much,

137
00:19:39,300 --> 00:19:46,340
I guess, and complain a little bit or push the bus driver a little bit. A guy in the back of

138
00:19:46,340 --> 00:19:53,060
the bus escalates the conflict and here we have a problem. Then the guy in the wheelchair totally

139
00:19:53,060 --> 00:19:58,820
blows it out of proportion and escalates it to the point of no return. Then the bus driver steps

140
00:19:58,820 --> 00:20:05,780
in and starts to make threats at the man in the wheelchair that he's going to call the supervisor.

141
00:20:05,780 --> 00:20:11,460
Of course, the man in the wheelchair buckles down. It was a comedy of errors in many ways. It

142
00:20:11,460 --> 00:20:20,580
was one thing wrong, done wrong after another, but this is an example. It exemplifies the problem

143
00:20:20,580 --> 00:20:29,620
of escalation, conflict escalation. Really a good study for peace studies. Good study in conflict

144
00:20:29,620 --> 00:20:44,180
escalation. Let's focus on goodness. What I really wanted to do is list out 10 sorts of goodness,

145
00:20:44,180 --> 00:20:48,900
but I'm not going to go into much detail because I don't want to take too much of your time. I think

146
00:20:48,900 --> 00:20:57,700
the general framework, the idea of focusing on goodness, is most important. Maybe I won't even

147
00:20:57,700 --> 00:21:03,300
go through the 10. Let's just talk about, let's talk about applying it to meditation. They

148
00:21:03,300 --> 00:21:12,980
were all sitting here, crossed I get intent upon the practice of meditation and betterment of

149
00:21:12,980 --> 00:21:24,900
ourselves. I think you can apply the same sort of framework, not focusing on trying to attack

150
00:21:24,900 --> 00:21:32,260
the problems. The most obvious sense, not trying to get rid of the problems. When you have pain,

151
00:21:32,980 --> 00:21:42,100
not opposing the pain, even when you have bad emotions, not opposing them. Even when you have evil

152
00:21:42,100 --> 00:21:48,980
inside that you know, I have such evil inside, not opposing that, but trying to figure out what

153
00:21:48,980 --> 00:21:54,420
actually works because it feels kind of good sometimes to hate yourself and berate yourself and

154
00:21:54,420 --> 00:22:00,100
feel guilty. It's kind of indulgent to feel guilty. Oh, I'm such a terrible person.

155
00:22:02,740 --> 00:22:09,460
I'll do it. It doesn't actually solve the problem. That's the key. I mean, it might make you feel

156
00:22:09,460 --> 00:22:14,740
better about yourself to hate yourself. Ironically, yes, yes, I know I'm a bad person, but I hate

157
00:22:14,740 --> 00:22:25,700
myself too. We do this, but it doesn't actually solve the problem. That's key. I think there's so much

158
00:22:25,700 --> 00:22:33,940
of how we react to things inside and then our meditation is simply habit because it makes us feel good.

159
00:22:36,980 --> 00:22:43,620
Rather than actually assessing whether it solves the problem, how we deal with pain by avoiding it

160
00:22:43,620 --> 00:22:59,860
by finding ways to fix it. We deal with uncomfortable meditation sessions or uncomfortable aspects

161
00:22:59,860 --> 00:23:15,300
of meditation by finding tricks or ways to avoid or circumvent them. My meditation is really much

162
00:23:15,300 --> 00:23:22,340
simpler than that and much more challenging as a result because it requires you to be honest,

163
00:23:22,340 --> 00:23:36,340
it requires you to, requires you to be quite perfect, natural. Meditation is like the growth of a

164
00:23:36,340 --> 00:23:45,300
tree. It doesn't come because you nail the trunk together and tie the branches on and glue the

165
00:23:45,300 --> 00:23:56,980
leaves to the branches. It evolves naturally and evolves from, again, this natural wisdom

166
00:24:00,260 --> 00:24:06,020
rather than trying to fix or construct something and evolves out of

167
00:24:06,020 --> 00:24:16,500
cultivating a pure natural state of mind. Natural is a very good way of understanding it

168
00:24:16,500 --> 00:24:24,900
because it feels more natural than our natural state or our natural state being our ordinary

169
00:24:24,900 --> 00:24:31,860
state of clinging to things, reacting to things. I think that's interesting and something

170
00:24:31,860 --> 00:24:37,220
that's missed because meditation can feel quite artificial in the beginning. We have a sense of

171
00:24:37,220 --> 00:24:42,740
trying to create an artificial state. We may not label it as such, but the states we think of are

172
00:24:43,540 --> 00:24:50,900
actually artificial. John, the trans, well, maybe that's a bad example, but they are still

173
00:24:50,900 --> 00:24:55,860
somewhat artificial, but there are many artificial states avoiding our problems.

174
00:24:55,860 --> 00:25:04,660
A mindfulness is an artificial, an artifice, or the practice is anyway. A true

175
00:25:04,660 --> 00:25:12,180
mind, actual mindfulness, when your mind is clear and you, as a result of the artificial practice,

176
00:25:12,180 --> 00:25:22,820
you become present. You're suddenly here. You're suddenly a matrix of experience.

177
00:25:29,620 --> 00:25:34,420
You're suddenly seeing and hearing and smelling and tasting and feeling and thinking and

178
00:25:34,420 --> 00:25:45,620
nothing else. You are the experiences. That natural state is what grows. It grows purity,

179
00:25:45,620 --> 00:25:57,220
it grows goodness. Our job is to cultivate, to protect this tree that's growing,

180
00:25:57,220 --> 00:26:03,540
to not let it fall over, which would mean falling into defilement.

181
00:26:05,860 --> 00:26:11,540
Does that's the thing? The tree is natural, but if you leave the tree alone, it won't grow.

182
00:26:12,180 --> 00:26:18,580
It will fall over, it will break, it will be eaten by animals, whatever analogy you want,

183
00:26:18,580 --> 00:26:24,500
whatever imagery you want to use. It won't grow. For most of us, it won't grow straight,

184
00:26:24,500 --> 00:26:31,700
it won't grow natural. It might very well die, which is why we cultivate meditation. We have a

185
00:26:31,700 --> 00:26:37,780
very strict set of practices that are quite artificial, quite methodical, systematic,

186
00:26:39,460 --> 00:26:42,740
but that's all just in protection of the tree. That's not the actual tree.

187
00:26:45,140 --> 00:26:52,740
Our practice of this mantra meditation is like any mantra meditation. It's an artifice. It's

188
00:26:52,740 --> 00:26:56,740
artificial. It's like the sticks that you use to prop up the tree.

189
00:27:06,340 --> 00:27:12,820
It's like the rope you use to tie the cow, the ox, the baby ox, when you're trying to train it.

190
00:27:12,820 --> 00:27:27,380
I wanted tonight's talk to be about this idea of focusing on the good. It doesn't mean ignoring

191
00:27:27,380 --> 00:27:36,260
the problems, but it means focusing on solutions, focusing on focusing on what's good about you

192
00:27:36,260 --> 00:27:45,220
as well, in the meditation, focusing on strengthening your goodness. Don't worry too much about

193
00:27:45,220 --> 00:27:57,380
the bad. Don't obsess over it. Why is that imbalance? Why shouldn't you just focus on

194
00:27:57,380 --> 00:28:04,340
everything equals because encouragement? When you focus on what's good about you, it is part of

195
00:28:04,340 --> 00:28:09,460
the path because we're trying to decrease, diminish, we're trying to free ourselves from the evil.

196
00:28:10,260 --> 00:28:13,140
So by not giving them as much power or obsession,

197
00:28:18,580 --> 00:28:21,540
there actually is a benefit to this imbalance.

198
00:28:21,540 --> 00:28:35,940
At least in so far as our outlook goes, because we will often step back from actually being

199
00:28:35,940 --> 00:28:43,060
mindful and say, is this working or we will remark upon how we feel and sometimes get quite

200
00:28:43,060 --> 00:28:48,260
discouraged when we see, oh, I have such evil inside, I have such chaos inside.

201
00:28:48,260 --> 00:28:51,860
I'm never going to be able to do this.

202
00:29:00,100 --> 00:29:09,300
And I think it's an argument to be made for focusing on

203
00:29:09,300 --> 00:29:20,260
and working on the good aspects. You have such goodness inside.

204
00:29:24,740 --> 00:29:28,900
If you compliment someone, they feel good. I read this on Facebook recently. It was something

205
00:29:28,900 --> 00:29:32,820
about calling someone beautiful, but it goes for compliments. If you compliment someone,

206
00:29:32,820 --> 00:29:42,260
they'll feel good for 10 minutes. If you criticize someone, they'll remember it forever.

207
00:29:45,540 --> 00:29:51,460
They'll feel bad all day. It can be very hard on ourselves.

208
00:29:56,180 --> 00:30:01,220
You can get encouragement from compliments. My teacher was big on compliments.

209
00:30:01,220 --> 00:30:07,140
Complimenting people was remarkable, really. How good he was it?

210
00:30:09,140 --> 00:30:14,260
Seeing the good in people, good he is, so he's not dead yet, I don't think.

211
00:30:16,900 --> 00:30:22,660
But just watching him, I think this is an important thing to remember.

212
00:30:24,180 --> 00:30:28,100
That's another form of goodness. Appreciating other people.

213
00:30:28,100 --> 00:30:31,220
We're appreciating ourselves as important too.

214
00:30:33,380 --> 00:30:36,100
Appreciating goodness, focusing on the good in people,

215
00:30:37,140 --> 00:30:41,620
not focusing on the evil. There's some interesting

216
00:30:43,220 --> 00:30:49,300
these ideas of working with people you don't like and expressing the opinion that

217
00:30:50,100 --> 00:30:54,340
if they do good things, you'll work with them. That's a very valid point.

218
00:30:54,340 --> 00:31:00,500
I'll be with you. The Buddha said similar things. I will agree with anyone.

219
00:31:01,620 --> 00:31:05,300
It doesn't matter who. If they say something true, if they say something right.

220
00:31:09,460 --> 00:31:14,580
So you don't consider the source not so much. You consider the nature of the

221
00:31:14,580 --> 00:31:28,020
nature of the speech, the nature of the thought. And you agree with what's right and you disagree

222
00:31:28,020 --> 00:31:32,180
with what's wrong. You don't make villains out of people or heroes out of other people.

223
00:31:34,820 --> 00:31:38,420
You focus on goodness, focus on the good. And you remember that

224
00:31:38,420 --> 00:31:45,700
I think a very poignant image of that of the Bodhi tree or the flower, the lotus flower.

225
00:31:47,300 --> 00:31:50,740
The Bodhi tree is an interesting one though because the Bodhi tree will grow anywhere.

226
00:31:52,100 --> 00:31:58,260
And most Bodhi trees, they grow to birdship, bird poop.

227
00:32:00,420 --> 00:32:06,260
And Thailand, the Bodhi trees go everywhere. But Bodhi trees are fig trees. So the birds eat the

228
00:32:06,260 --> 00:32:12,420
figs and spread their droppings around. And so as a result you'll see Bodhi trees growing out of

229
00:32:12,420 --> 00:32:19,460
the top of another tree. So there are certain trees that up in the crook of the middle of the

230
00:32:19,460 --> 00:32:26,260
tree. There's a huge Bodhi tree growing on top of it. What I mean to say is that Bodhi

231
00:32:26,260 --> 00:32:34,500
tree grows anywhere and it grows out of feces. Most plants do. They grow to manure. The beautiful,

232
00:32:34,500 --> 00:32:47,460
most beautiful flower requires manure to grow. I mean it's just imagery but it works to think about

233
00:32:49,620 --> 00:32:53,220
think about how unstained one can be in the midst of filth.

234
00:32:55,300 --> 00:33:01,300
The Bodhi used this kind of imagery often about being unstained or unsullied.

235
00:33:01,300 --> 00:33:09,620
Even in the midst of great filth, like water off the back of a lotus, water off a lotus or a

236
00:33:09,620 --> 00:33:16,660
mustard seed on the tip of a needle. I think he used that analogy to mean that it doesn't stick.

237
00:33:16,660 --> 00:33:31,860
The mustard seed does not stay on the needle. Can something like that?

238
00:33:31,860 --> 00:33:39,460
I firmly believe and this is a firmly Buddhist concept that even in the midst of great chaos,

239
00:33:39,460 --> 00:33:46,820
great trouble, great evil even. Whether it's in her evil or externally evil, great and wonderful

240
00:33:46,820 --> 00:33:55,860
things can still be done. Heaven can be found. Peace can be found. Nirvana can be found.

241
00:33:59,860 --> 00:34:03,620
There are, there was this movie we watched many, many years ago called,

242
00:34:03,620 --> 00:34:07,940
I think life is beautiful. It was an Italian film actually.

243
00:34:10,100 --> 00:34:14,740
How about how this guy in the, I mean Nazi Germany manages to make everything.

244
00:34:15,460 --> 00:34:22,340
It wasn't completely a Buddhist film but it's a very, very positive film. Not not positive but

245
00:34:25,140 --> 00:34:26,740
positive thinking type of film.

246
00:34:26,740 --> 00:34:34,900
But to some extent he did it for his son, tried to make his son, make sure his son was not

247
00:34:35,860 --> 00:34:39,780
traumatized by the horrors of the concentration camps.

248
00:34:42,820 --> 00:34:48,580
And so I'm not obviously completely on this idea of the power of positive thinking. It's

249
00:34:48,580 --> 00:34:56,660
can be quite dangerous, delusional or deluding. But there is some extent to which we have to

250
00:34:57,540 --> 00:35:03,940
should and benefit from focusing our attentions on the positive and on the cultivation of positive

251
00:35:03,940 --> 00:35:15,700
and learning to let go of the negative. And I think letting go of the negative also involves

252
00:35:15,700 --> 00:35:26,420
not taking it as seriously, an increased focus on the positive. I think there is an argument to

253
00:35:26,420 --> 00:35:35,780
be made for not taking everything equally and putting more focus, at least intellectually,

254
00:35:37,620 --> 00:35:44,340
at least in terms of our goals to focus on the positive. Focus on it. So the Buddha often did

255
00:35:44,340 --> 00:35:51,060
talk about goodness rather than focusing on the problem. What's wrong with the world?

256
00:35:52,980 --> 00:35:56,260
And I guess that's the point is you don't focus either on what's right in the world.

257
00:35:57,380 --> 00:36:02,340
You focus on what can be done to make the world a better place. And so the same goes with oneself.

258
00:36:03,780 --> 00:36:10,820
You don't focus on what's wrong with you. You don't focus on what's right either. You focus on

259
00:36:10,820 --> 00:36:24,660
goodness on doing the cultivation of good mindstands. There, okay. So that's the focus of tonight's

260
00:36:24,660 --> 00:36:32,340
talk. It's a little bit of dumb, I think. And I think we actually managed to record

261
00:36:32,340 --> 00:36:40,020
on three different mediums tonight.

262
00:36:42,660 --> 00:36:48,180
Oops, I'm still recording, which means that's showing up. All right. And we're playing around.

263
00:36:48,180 --> 00:37:04,100
Are there any questions?

264
00:37:18,980 --> 00:37:31,220
I'm on limited, limited screen space here. So I can't actually pull up

265
00:37:33,140 --> 00:37:42,420
the questions that are online, but anyone here has any questions? Otherwise, yeah, you're welcome.

266
00:37:42,420 --> 00:37:58,340
Thank you all for coming out. And this will be up on YouTube. Hopefully tonight.

267
00:38:03,780 --> 00:38:07,940
All right, then I'll say if you do have questions, bring them back another day and

268
00:38:07,940 --> 00:38:14,180
type them out and have them ready to answer because ready to ask because I don't know sitting

269
00:38:14,180 --> 00:38:26,500
here whether anyone is trying to trying to type it out frantically, type out a question.

270
00:38:27,380 --> 00:38:31,940
So don't get frantic, type it out in advance and just copy and paste it in if you can.

271
00:38:31,940 --> 00:38:41,780
All right, well, I'm glad it was helpful. Good food for thought for tonight. I wish you all

272
00:38:41,780 --> 00:39:06,020
a good night and see you again in the new future.

