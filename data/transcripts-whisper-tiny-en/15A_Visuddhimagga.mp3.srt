1
00:00:00,000 --> 00:00:09,120
So today's recollections are recollections of virtue, recollection of generosity, recollection

2
00:00:09,120 --> 00:00:23,240
of deities, and then the next chapter mindfulness of death, or recollection of death.

3
00:00:23,240 --> 00:00:28,200
So recollection of virtue, one who wants to develop the recollection of virtue should go

4
00:00:28,200 --> 00:00:33,520
into solitary retreat and recollect its own different kinds of virtue in their special qualities

5
00:00:33,520 --> 00:00:41,080
of being Anton et cetera, as follows.

6
00:00:41,080 --> 00:00:52,960
In the teachings of the Buddha, the virtue or moral conduct is much emphasized, and the

7
00:00:52,960 --> 00:01:03,640
adherence are instructed, or advised, to keep these precepts, very pure, so it is said

8
00:01:03,640 --> 00:01:10,280
that they should be Anton and Brent, and blotched, and modeled, and that they should

9
00:01:10,280 --> 00:01:14,960
be liberating, traced by the wise, not yet yet to, and conducive to concentration, be

10
00:01:14,960 --> 00:01:15,960
a described.

11
00:01:15,960 --> 00:01:22,680
Whenever a good sealer is described, Buddha used these words.

12
00:01:22,680 --> 00:01:35,360
And the commentaries, including the path of purification, explained how the sealer of the

13
00:01:35,360 --> 00:01:41,160
precepts are Anton and Brent, and blotched, and and modeled.

14
00:01:41,160 --> 00:01:56,240
So for late devotees, Buddha prescribed five precepts, and these are called precepts of

15
00:01:56,240 --> 00:02:01,840
a late devotee, and not killing, not stealing, no sexual misconduct, no telling lies, and

16
00:02:01,840 --> 00:02:06,040
no intoxicants.

17
00:02:06,040 --> 00:02:15,440
And these five precepts are called householders precepts, or sometimes they are called

18
00:02:15,440 --> 00:02:26,800
Nichesila, dominant precepts, that means to be kept always by the devotees, or by those

19
00:02:26,800 --> 00:02:33,320
who take refuge in the Buddha, the Dharma, and the Sangha.

20
00:02:33,320 --> 00:02:42,160
And since utmost purity is important here, a Buddha said, they must be Anton and Brent,

21
00:02:42,160 --> 00:02:43,840
and so on.

22
00:02:43,840 --> 00:02:52,200
Now, what is the ton, ton virtue or ton sealer?

23
00:02:52,200 --> 00:02:56,760
See, Brent and sealer, blotched sealer, motored sealer.

24
00:02:56,760 --> 00:03:02,680
So in order to understand you, you can read the paragraph 102.

25
00:03:02,680 --> 00:03:11,480
Now, research in mind, just these three steps, one, two, three, four, five.

26
00:03:11,480 --> 00:03:20,240
Now, if one, number one is broken, then the virtue is called ton virtue, or if number

27
00:03:20,240 --> 00:03:31,360
five is broken, it is called ton, but if number two is broken, or number three is broken,

28
00:03:31,360 --> 00:03:39,520
or number four is broken, and it is called, Brent, Brent, if they are not broken like

29
00:03:39,520 --> 00:03:42,560
that, it is called and Brent.

30
00:03:42,560 --> 00:03:52,200
So there is a little difference between these words, but in reality, the precepts are

31
00:03:52,200 --> 00:03:59,720
to be kept, to be kept intact.

32
00:03:59,720 --> 00:04:07,080
So killing and intoxication are actually a sense more serious, the first and the fifth.

33
00:04:07,080 --> 00:04:14,720
No, no, no, no, not that way, because here, the commentary is explaining the Anton and

34
00:04:14,720 --> 00:04:19,800
Brent, and, and, and, and blotched, and, and motels, how they are different when they are

35
00:04:19,800 --> 00:04:21,640
describing that way.

36
00:04:21,640 --> 00:04:28,200
So if one, one, number one is, is broken, or number five is broken, it is called ton, but

37
00:04:28,200 --> 00:04:33,560
if number two, or number three, or number four is broken, then it is Brent, and if number

38
00:04:33,560 --> 00:04:41,360
one and two, or one, two and three, or two and three, or two, three and four are broken,

39
00:04:41,360 --> 00:04:51,920
and they are called blotched, and then if number one and three are broken, or two and

40
00:04:51,920 --> 00:05:00,120
four are broken, and every other preset, and it is called motels.

41
00:05:00,120 --> 00:05:22,560
Okay, so maybe not simultaneously, but on one location, or maybe in a day.

42
00:05:22,560 --> 00:05:27,160
And in general they are Anton and Brent, and blotched, and motels, when they are, and

43
00:05:27,160 --> 00:05:33,840
damaged by the seven bones of sexuality, and they are described in sexual one, paragraph

44
00:05:33,840 --> 00:05:42,600
144, and in, in, in, in bum, in bum, and they are called minor, minor sex or something

45
00:05:42,600 --> 00:05:54,000
like that, not actually indulging in sexual act, but taking delight in looking at,

46
00:05:54,000 --> 00:06:03,520
pleasurable, looking at beautiful things, taking delight in listening to the voice,

47
00:06:03,520 --> 00:06:11,000
the sound of one who, whom you love, and so on, so they are called small or minor sexual

48
00:06:11,000 --> 00:06:18,960
acts, so they are here called seven bones of sexuality, and then they are not to be

49
00:06:18,960 --> 00:06:24,880
damaged by anger, and, and maybe, and the other even things.

50
00:06:24,880 --> 00:06:33,800
And the, what you should be liberating, liberate by freeing from the slavery of craving,

51
00:06:33,800 --> 00:06:40,640
that means, when you, when you practice C-value, you, you practice C-value, for C-value

52
00:06:40,640 --> 00:06:52,360
and others say again, not, not, for the sake of something not to get results and so on.

53
00:06:52,360 --> 00:06:56,520
And they are praised by the, why, because they are praised by such wise men as enlightened

54
00:06:56,520 --> 00:06:59,280
ones, because they are praised by Buddha and others.

55
00:06:59,280 --> 00:07:08,520
And they are not at here too, at here to hear means, they are not, they are not kept, motivated

56
00:07:08,520 --> 00:07:19,600
by craving or other unwholesome mental states like a wrong view.

57
00:07:19,600 --> 00:07:25,280
And because of the impossibility of misrepresenting death, there is this flaw in your

58
00:07:25,280 --> 00:07:36,120
virtues, that means, not really missed every hand, but impossibility of being accused, or

59
00:07:36,120 --> 00:07:43,800
being criticized, that there is this flaw in your virtues.

60
00:07:43,800 --> 00:07:50,920
They, they keep their silasu, so pure, so flawless, that nobody can say, there is a flaw

61
00:07:50,920 --> 00:07:54,000
here, there is a flaw there.

62
00:07:54,000 --> 00:08:01,760
So it is not, not actually missed every hand, and the word, the pali word there is given

63
00:08:01,760 --> 00:08:16,240
here, paramatum, now this word, sometimes the word derived from the same root, the word

64
00:08:16,240 --> 00:08:31,120
paramasa is used in, in sudhas, and paramasa means something like wrong view, paramasa, the

65
00:08:31,120 --> 00:08:42,960
literal meaning of paramasa is to do, what do you call this, to rub something like that,

66
00:08:42,960 --> 00:08:53,320
to stroke, to stroke, yeah, yeah, stroke, but it can also mean to take hold of.

67
00:08:53,320 --> 00:09:01,520
And mostly it is used for a wrong taking hold, I mean wrong view, but in the, in the

68
00:09:01,520 --> 00:09:10,840
later explanation, beginning with all, because of the impossibility of misrepresenting

69
00:09:10,840 --> 00:09:20,680
here, paramata, the pali word paramata means to accuse or to be accused, to be fine for

70
00:09:20,680 --> 00:09:29,080
it, so not misapprehending here, there there is this flaw in your virtue, they conducive

71
00:09:29,080 --> 00:09:33,920
to concentration, since they conducive to access concentration and absorption concentration,

72
00:09:33,920 --> 00:09:42,040
or to pass concentration and fruition concentration, so that is why virtue, pure virtue

73
00:09:42,040 --> 00:09:48,280
of pure moral contact is important, especially when we practice meditation, because

74
00:09:48,280 --> 00:09:54,280
when we practice meditation, we want to, we try to get concentration, because without

75
00:09:54,280 --> 00:10:04,040
concentration, without unification of mind or stillness of mind, we cannot hope to see the

76
00:10:04,040 --> 00:10:09,360
real thing, or we cannot hope to penetrate into the nature of things.

77
00:10:09,360 --> 00:10:16,800
So in order to be able to penetrate into the nature of things, we need somebody, we need

78
00:10:16,800 --> 00:10:24,560
unification of mind, and in order to get unification of mind or concentration of mind, we need

79
00:10:24,560 --> 00:10:33,400
what conjuges to concentration, which is a sealer of virtue, so the virtue well kept

80
00:10:33,400 --> 00:10:43,520
is conducive to concentration, because when we keep our precepts, we do not have any

81
00:10:43,520 --> 00:10:50,760
guilt feelings, we do not have any remorse, say if I have broken a rule, and I think

82
00:10:50,760 --> 00:10:55,360
about it again and again, then this remorse comes to me again and again, especially when

83
00:10:55,360 --> 00:11:02,280
I practice meditation, I am expected to be very pure, and now I have done something wrong

84
00:11:02,280 --> 00:11:08,400
and so on, so when this thought of guilt comes to us again and again, we cannot hope to

85
00:11:08,400 --> 00:11:17,880
get stillness of mind, that peacefulness of mind, that is why the pure virtue or moral

86
00:11:17,880 --> 00:11:24,840
contact is important, especially when we practice meditation.

87
00:11:24,840 --> 00:11:38,960
Now here, the Buddha's disciple kept those precepts, and Thon and Rant and so on, as long

88
00:11:38,960 --> 00:11:43,480
as he recollects his own virtues in their special qualities of being Anton etc. in this

89
00:11:43,480 --> 00:11:49,680
way, then on that occasion his mind is not obsessed by greed and so on, so when he recollects

90
00:11:49,680 --> 00:12:00,720
on his purity of sea, the purity of virtue, there can be no greed in his mind, no anger

91
00:12:00,720 --> 00:12:06,200
or hatred in his mind, there can be no delusion in his mind, and so his mind has rectitude

92
00:12:06,200 --> 00:12:12,960
that means of the thought, straightness on that occasion being inspired by virtue.

93
00:12:12,960 --> 00:12:17,040
So when he has suppressed the hindrances and the way already described, the general factors

94
00:12:17,040 --> 00:12:26,920
arise in a single conscious moment, that means at one moment the general factors arise,

95
00:12:26,920 --> 00:12:39,360
that means they become stronger, they become more mature, because general factors are always

96
00:12:39,360 --> 00:12:44,960
with us when we practice meditation, please initial application of mind, sustained application

97
00:12:44,960 --> 00:12:54,960
of mind and so on. So we experience these general factors from time to time, some of them

98
00:12:54,960 --> 00:13:01,640
from time to time, and the first and the second factors are almost always, but they become

99
00:13:01,640 --> 00:13:11,280
stronger and stronger as you progress. And so at one time they become very strong, but

100
00:13:11,280 --> 00:13:22,800
since there are, since owing to the profundity of the virtues special qualities or owing

101
00:13:22,800 --> 00:13:28,040
to his being occupied and recollecting special qualities of many souls, the genre is only

102
00:13:28,040 --> 00:13:36,320
accessed and does not reach absorption. So one cannot reach real genre through this practice

103
00:13:36,320 --> 00:13:44,440
of meditation. The genre here is the neighborhood concentration actually, not not the real

104
00:13:44,440 --> 00:13:54,960
genre, but the initial application and so on are called genre factors, whether they arise

105
00:13:54,960 --> 00:14:01,800
with the genre consciousness or arise with other types of consciousness. So that is

106
00:14:01,800 --> 00:14:08,380
the idea here called genre factors, but we are not to take that, this means the real

107
00:14:08,380 --> 00:14:16,960
genre, because the real genre cannot be obtained through the practice of his recollection

108
00:14:16,960 --> 00:14:26,560
to meditation, simply because the object is profound and also when you are thinking

109
00:14:26,560 --> 00:14:35,960
of many things that you do not get real strong concentration, not like, say, breathing

110
00:14:35,960 --> 00:14:43,120
meditation or other kinds of meditation, where you put your mind on one object only and

111
00:14:43,120 --> 00:14:48,200
keep it there as long as you can. But here you are actually recollecting or thinking

112
00:14:48,200 --> 00:14:58,680
of many things, so somebody cannot be too strong here. And that access genre itself is

113
00:14:58,680 --> 00:15:03,440
known as recollection of virtue too, because it arises with the virtue special qualities

114
00:15:03,440 --> 00:15:13,360
as that means. So sometimes we say recollection of virtue, but actually we mean the concentration,

115
00:15:13,360 --> 00:15:22,000
meaning the access genre, that means neighborhood concentration. And when a big who is devoted

116
00:15:22,000 --> 00:15:26,200
to this recollection of virtue, he has respect for the training, training as a man. He

117
00:15:26,200 --> 00:15:32,320
lived in communion with his fellows and the life of purity, he said, Sadulus in welcoming

118
00:15:32,320 --> 00:15:38,800
his divide of the fear of several approach and so on. Fear is really means danger, danger

119
00:15:38,800 --> 00:15:48,560
of several approach. When one's moral conduct is not not pure and there are five dangers,

120
00:15:48,560 --> 00:16:17,160
several approach, reproach of others, punishment, punishment and then unhappy destiny.

121
00:16:17,160 --> 00:16:40,200
One more, right? There will be five. Several approach and reproach of others and then punishment

122
00:16:40,200 --> 00:17:05,560
and then unhappy destiny. One more, I could remember this. No, come later. So these are the

123
00:17:05,560 --> 00:17:16,440
benefits of the practice of recollection, meditation. Next is recollection of generosity,

124
00:17:16,440 --> 00:17:23,880
it is one's own generosity. So who wants to develop the recollection of generosity should

125
00:17:23,880 --> 00:17:30,600
be naturally devoted to generosity and the constant practice of giving and sharing. Alternatively,

126
00:17:30,600 --> 00:17:36,120
if he is one who is studying the development of it, he should make the resolution from now on. When

127
00:17:36,120 --> 00:17:43,560
there is anyone present to receive, I shall not eat even the single mouthful without having given

128
00:17:43,560 --> 00:17:54,040
the gift. So without having given some portion of his food, he could not eat something like that.

129
00:17:54,040 --> 00:18:01,640
And that very day, he should give a gift by sharing according to his means and his ability

130
00:18:01,640 --> 00:18:07,480
to do those who have distinguished qualities. That means who are virtuous. When he has apprehended

131
00:18:07,480 --> 00:18:14,840
this sign in there, that means that means in giving, that means when he has taken into his mind

132
00:18:14,840 --> 00:18:19,720
or put his mind on the act of giving, he should go into solitude even free time.

133
00:18:19,720 --> 00:18:27,720
This own generosity in its special qualities of being free from staying forever, etc as follows.

134
00:18:29,640 --> 00:18:35,880
It is again for me, it is great to give for me that in a generation obsessed by this

135
00:18:35,880 --> 00:18:41,720
state of Everest, I abide with my heart free from Spain and Everest, and I'm freely generous and

136
00:18:41,720 --> 00:18:47,800
open-handed that I delight in recognition, expect to be asked and rejoice in giving and sharing.

137
00:18:47,800 --> 00:18:52,200
That is how you practice the collection of generosity.

138
00:18:55,000 --> 00:19:02,440
It is again for me, that means it is an advantage. I surely partake of those kinds of

139
00:19:02,440 --> 00:19:09,160
caring for a giver that has been recommended by the Blessed One as follows. The border often

140
00:19:09,160 --> 00:19:18,840
talked about the benefits of giving, so if I practice generosity, I will get the advantages.

141
00:19:19,400 --> 00:19:24,840
A man who gives life by giving food share, have life either divine or human.

142
00:19:24,840 --> 00:19:40,200
In one soldier who offers food, actually offers five things, life, duty, happiness,

143
00:19:40,200 --> 00:19:55,160
strength, intelligence, because if you don't have to eat, you will die and so on. In fact,

144
00:19:55,160 --> 00:20:02,360
by giving food, it doesn't give these five things. Since he gives these five things,

145
00:20:02,360 --> 00:20:13,240
he will get results in having a long life in his next life and having happiness and so on.

146
00:20:14,680 --> 00:20:23,400
And then the giver is loved and frequented by many, so it do not is loved and many people want to

147
00:20:23,400 --> 00:20:29,160
approach him. One who gives is ever loved according to the wise man's laws. In fact,

148
00:20:29,160 --> 00:20:39,560
what is meant here is a person who gets into the wise man's practice. Law here means just practice.

149
00:20:42,040 --> 00:20:48,200
You know, the word dhama is very difficult. If you stick to only one translation, you will

150
00:20:48,200 --> 00:20:55,960
miss the point. So every time you find the word dhama, you have to be discriminating,

151
00:20:55,960 --> 00:21:05,000
which kind of dhama is meant. So we cannot translate the word dhama by the word law always.

152
00:21:05,000 --> 00:21:09,000
Sometimes it means law and sometimes it means practice. Sometimes it means

153
00:21:10,680 --> 00:21:16,680
wholesome states and unwholesome states and so on. So here, one who gives is ever loved

154
00:21:16,680 --> 00:21:31,880
because it gets into the practice of wise man. It is great for me and obsessed by the state of

155
00:21:31,880 --> 00:21:40,760
Everest. That means people are stingy and so in this, among these people who are stingy,

156
00:21:40,760 --> 00:21:48,200
I am not stingy, I practice generosity and so on. And generation means beings, so-called

157
00:21:48,200 --> 00:21:58,120
owing to the effect of their being generated. Now, the party word is P-A-G-E, Pajya. And

158
00:21:58,120 --> 00:22:10,360
Yanamori always translates Pajya as generation. Pajya is- the party word Pajya is- is synonym for sata,

159
00:22:11,400 --> 00:22:27,560
it being. So Pajya means just being. But in Bali, it is used in singular number, although it means

160
00:22:27,560 --> 00:22:39,400
many people. I mean like people, like the English word people or a group. I don't know what people

161
00:22:39,400 --> 00:22:47,560
understand by the word generation. If you say generation, do you do you understand it? Do you mean

162
00:22:47,560 --> 00:22:58,040
the whole, you know, all beings, generation has several meetings in itself and it's getting

163
00:22:58,040 --> 00:23:04,440
buried, it's being generated. Yeah. It sometimes means the general, we say people have a generation

164
00:23:04,440 --> 00:23:11,160
that means of a 10 year stand, everybody that's called generation. Yes. It also has a time measurement

165
00:23:11,160 --> 00:23:29,480
generation. So I'm afraid that people might take that to mean, say, one generation,

166
00:23:29,480 --> 00:23:36,280
two generation of mankind and so on. But here, generation or the party word Pajya means simply

167
00:23:36,280 --> 00:23:45,720
beings. So we can translate it as people or beings, generation beings. So the meaning here is

168
00:23:45,720 --> 00:23:52,120
this, among beings, not the used beings. Among beings who are overcome by the state of Everest,

169
00:23:52,120 --> 00:23:58,200
which is one of the dark states that means a closer, I mean, and wholesome states that corrupt

170
00:23:58,200 --> 00:24:03,720
the natural transparency of consciousness and which is the characteristic and ability to

171
00:24:03,720 --> 00:24:16,360
share in one's own good fortune with others. So Everest is in tolerance of one's own good being

172
00:24:16,360 --> 00:24:27,160
shared by others. That means I have this book. I cannot bear to share this book with other

173
00:24:27,160 --> 00:24:34,520
presenters. If other doesn't come to use it, I don't like it. And that is what is called Everest in

174
00:24:34,520 --> 00:24:47,240
Abirama. It is not really stinginess. There is a very subtle difference because the party word

175
00:24:47,240 --> 00:24:58,440
much area is translated here as Everest. And much area is accompanied by Dosa, anchor, not by

176
00:24:58,440 --> 00:25:21,200
Loba. So stinginess means, what is stinginess? You don't want to give your thing to other questions. You are attached to it. So it may be caused by attachment, but it does not accompany attachment with much area. So

177
00:25:21,200 --> 00:25:42,760
much area, when there is much area, there is something like upset, being upset. I don't like my things to be used by others. For a particular person, it might be anger that's generating enough. And as a monk, say we depend on people. So I don't want my

178
00:25:42,760 --> 00:26:03,480
saboda to become the saboda of other monk, something like that. That is also much area. And then say I have a degree. Then I don't want others to get

179
00:26:03,480 --> 00:26:26,120
a degree. That's also much area. May this success happen only to me and not to us. So that is translated as Everest here. And then the

180
00:26:26,120 --> 00:26:55,320
common data brings in the soda. And in the soda, Bodha said, I lift the home life with my heart free. That is in response to a household, a household asking Bodha, say what to do, what kind of abiding he has to keep. That's why he says Bodha says, lift the home life.

181
00:26:56,920 --> 00:27:21,840
So it is not meant, that sentence is not meant for monks, because monks live homeless life. So the common data is here explaining just that. Bodha said, lift home life in this soda, because he was talking to a leap person and not to monks. So for monks, Bodha said, for monks, the common data just lives. Not lived the home

182
00:27:21,840 --> 00:27:51,680
life, but just lives. And then at the end of this paragraph, there are the meaning is I live overcoming. I think it should be overcoming. So you are ahead of the family. So you live governing the family or being the

183
00:27:51,680 --> 00:28:14,960
leader of the family. That is what is many part here. Over powering, super, super, not not supporting, but you are above the family, something that they are leading, leading, yeah, leading, and then

184
00:28:14,960 --> 00:28:36,320
the family. For a group from 112, about six, seven lines down, we may have to set a yeah, yeah, yoga is a reading, in which case the meaning is devoted to the two and so on.

185
00:28:36,320 --> 00:29:04,640
Now, yeah, yeah, yeah, yoga is another reading, not a reading, it is another reading. Sometimes we have different reading, since they are carried down by word of mouth and also by writing, we have some very angry reading. So this is another reading of the word, pali word, yajayoga.

186
00:29:04,640 --> 00:29:24,400
And so, if the reading is with j instead of c, then the meaning is a little different, because yajayoga means sacrificing. He is sacrificing means just killing. So actually, it means the same thing.

187
00:29:24,400 --> 00:29:43,400
And regression sharing and so on. So the others are not, not difficult to understand. This meditation also lead to only excess concentration and not to jana, real jana concentration.

188
00:29:43,400 --> 00:29:58,400
Then the recollection of deities. Now, in the recollection of deities, they are given different realms. And they are what we call the six realms of divas.

189
00:29:58,400 --> 00:30:15,400
And the first is the realm of the four kings. That is the first realm. The second is the dawatimsa, the realm of the 33. And the next is yajayama. They are going to divine bliss.

190
00:30:15,400 --> 00:30:30,400
And the next is to sita, the deities who are contented. And the next is nimmana-rati, those who delight in creating things for themselves and enjoy them.

191
00:30:30,400 --> 00:30:50,400
The next is parat nimmita vasavatil, who wields power over others' creations. Actually, they do not create things for themselves, but they have other deities create things for them and enjoy them.

192
00:30:50,400 --> 00:31:06,400
So the difference between the two is the nimmana-rati, the first ones, create things for themselves for their environment. But the next one will be more lazy.

193
00:31:06,400 --> 00:31:14,400
They don't create things for themselves, but they have others create for them.

194
00:31:14,400 --> 00:31:23,400
Like people nowadays, people don't want to do things, but they have other people make things for them and then they use them.

195
00:31:23,400 --> 00:31:31,400
And they are deities of Brahma's revenue. That means they are higher celestial beings, Brahma.

196
00:31:31,400 --> 00:31:42,400
And they are deities higher than that. You know, there are Brahma's of the first plane, second plane, third plane and fourth plane.

197
00:31:42,400 --> 00:31:58,400
So, at least there are fourth place of the Brahma's. And all those deities possess faith such that on dying here, they were reborn there and such faith is present in need to actually this recollection is,

198
00:31:58,400 --> 00:32:17,400
a recollection of one's own qualities actually. But, comparing one's own qualities with the qualities that deities have. So, these people, these beings are reborn before they are reborn as deities, they were human beings.

199
00:32:17,400 --> 00:32:29,400
When they were human beings, they have such qualities as faith and what, virtue, learning, generosity and understanding and kindness.

200
00:32:29,400 --> 00:32:46,400
And these qualities are in need to, something like that. And here also, there is kind of the discrepancy between what the

201
00:32:46,400 --> 00:32:56,400
commander is saying and what is said in the text. But, the commander is here, I will tell you what is meant.

202
00:32:56,400 --> 00:33:11,400
When Buddha taught to Mahana, Mahana was his cousin. So, when Buddha taught to Mahana Mahana, Buddha said,

203
00:33:11,400 --> 00:33:20,400
the noble disciple recollects the faith, virtue, learning, generosity and understanding that are both his own and those deities.

204
00:33:20,400 --> 00:33:40,400
So, if we take that literally, then it means that you must recollect on the qualities of the deities and then on the qualities in yourself.

205
00:33:40,400 --> 00:33:58,400
So, you must reflect on both qualities of both yourselves and the deities. But, the commander said that it is important that you just take the qualities of the deities as an example.

206
00:33:58,400 --> 00:34:11,400
But, actually, what you must do is dwell on the qualities of yourself. So, these qualities, those two possess, these qualities are reborn as devils and Brahma's and those qualities are in need.

207
00:34:11,400 --> 00:34:16,400
And so, I will also be able to point out something.

208
00:34:16,400 --> 00:34:34,400
So, he recollects his own special qualities, making the deities stand at witness us. That means just taking them as an example.

209
00:34:34,400 --> 00:34:50,400
For this Western person, this is quite different than even with change. He just said, quite different than recollecting the three treasures and generosity and virtue.

210
00:34:50,400 --> 00:35:08,400
Those are, I can easily see how they would be very efficacious and very useful objects in meditation. But, deities or gods.

211
00:35:08,400 --> 00:35:26,400
And other places, who says the question of whether their gods are not further, or whether they are existing or not, is not further one. That's not one of the useful questions or decisions.

212
00:35:26,400 --> 00:35:42,400
And yet, this is as an object as one of the six scenes in the world of place.

213
00:35:42,400 --> 00:36:03,400
According to the text, there are different kinds of questions, the beings, the human beings, lower than them, animals, beings in hell and beings in celestial realms also.

214
00:36:03,400 --> 00:36:18,400
So, here, we recollect on our own good qualities. Not on their qualities, but these qualities, they had these qualities and they are reborn there.

215
00:36:18,400 --> 00:36:25,400
So, these qualities are in need to, and so you dwell on your own good qualities.

216
00:36:25,400 --> 00:36:42,400
So, another way of saying it would be dwelling on the highest qualities that you perceive, whether they be deities or in yourself, that would be an object of meditation.

217
00:36:42,400 --> 00:36:58,400
But here, dwelling on one's own qualities, because if you dwell on the other's qualities, then it may be something like the recollection of the qualities of the Buddha and my sanga.

218
00:36:58,400 --> 00:37:08,400
But here, just as you dwell on your own virtue, sealer, so they all refer to oneself.

219
00:37:08,400 --> 00:37:19,400
So, it's like the collection of virtue, the collection of generosity and this one.

220
00:37:19,400 --> 00:37:29,400
So, first, you may dwell a little on the qualities of the tables, and then, for instance, you dwell on your own qualities.

221
00:37:29,400 --> 00:37:43,400
And here, also, you cannot get Jana, Jana concentration, just the excess or livable concentration.

222
00:37:43,400 --> 00:37:59,400
And then, in the general statements, on page 245, first lines of the law.

223
00:37:59,400 --> 00:38:02,400
When he is glad, happiness is born in him.

224
00:38:02,400 --> 00:38:12,400
Here, in the meaning inspires him, should be understood as a set of contentment inspired by the meaning, beginning.

225
00:38:12,400 --> 00:38:16,400
And there is less advantage, such sense he is and so on.

226
00:38:16,400 --> 00:38:24,400
Now, contentment is not a good translation for the body word, dochi here.

227
00:38:24,400 --> 00:38:32,400
In the mongala soda, there is a word, santukti, and that is contentment, santukti.

228
00:38:32,400 --> 00:38:35,400
Now, here, there is no sant, just tukti.

229
00:38:35,400 --> 00:38:43,400
And tukti is explained in the commentary as something like happiness.

230
00:38:43,400 --> 00:38:59,400
So, here, what is meant is the pleasure or joy inspired by the meaning, not contentment.

231
00:38:59,400 --> 00:39:04,400
And the law inspires him instead of contentment inspired by the text.

232
00:39:04,400 --> 00:39:10,400
Here, also, is it joy inspired by the text.

233
00:39:10,400 --> 00:39:14,400
The application of the law makes him glad he is set of both.

234
00:39:14,400 --> 00:39:24,400
Here, I think we should see.

235
00:39:24,400 --> 00:39:33,400
He gets joy connected with Dhamma, not the application of the law makes him glad.

236
00:39:33,400 --> 00:39:47,400
When he practices the collection meditation, he becomes joyful.

237
00:39:47,400 --> 00:39:54,400
He thinks of the words themselves and he thinks of the meaning of the words.

238
00:39:54,400 --> 00:40:02,400
And whether he keeps his mind on the words or the meanings, he gets joy from the collection of the words.

239
00:40:02,400 --> 00:40:07,400
And that joy is connected with the practice, connected with Dhamma.

240
00:40:07,400 --> 00:40:14,400
So, the last one is to be translated, not as the application of the law makes him glad.

241
00:40:14,400 --> 00:40:23,400
Something like he gets satisfaction or he gets joy connected with Dhamma.

242
00:40:23,400 --> 00:40:32,400
You can get happiness or joy by going after what we pleasure.

243
00:40:32,400 --> 00:40:37,400
But here, the joy you get is from the practice of meditation.

244
00:40:37,400 --> 00:40:42,400
So, we can see the application of the law is joyful.

245
00:40:42,400 --> 00:41:08,400
If we translate partly, literally, he gets joy depending on the meaning.

246
00:41:08,400 --> 00:41:12,400
He gets joy depending on the text.

247
00:41:12,400 --> 00:41:27,400
He gets joy which is connected with Dhamma of practice.

248
00:41:27,400 --> 00:41:40,400
And then the commentary said that these recollections can be practiced to perfection only by those who have gained enlightenment.

249
00:41:40,400 --> 00:41:55,400
Those who have not gained enlightenment can practice these recollections but they cannot practice through the highest level of perfection.

250
00:41:55,400 --> 00:42:00,400
The different tutors are quoted here.

251
00:42:00,400 --> 00:42:14,400
Now, the last paragraph, the venerable one, it seems, let me see.

252
00:42:14,400 --> 00:42:19,400
Still, though this is so, they can be brought to mind by an ordinary man.

253
00:42:19,400 --> 00:42:27,400
The ordinary man means to who have not yet enlightened man if he possesses these special qualities of these five values and the rest.

254
00:42:27,400 --> 00:42:37,400
For one is, we are already in the special qualities of the Buddha, etc., even according to he or the second-hand knowledge.

255
00:42:37,400 --> 00:42:45,400
His consciousness settles down in virtue of which the hindrances are suppressed and his supreme gladness initiates inside.

256
00:42:45,400 --> 00:42:51,400
And he even attains to our hands if like the other person you are who dwells at.

257
00:42:51,400 --> 00:42:55,400
Katak and Dakara is the name of a place.

258
00:42:55,400 --> 00:43:00,400
The venerable one, it seems, so a figure of the enlightened one created by Mara.

259
00:43:00,400 --> 00:43:09,400
You know, in Buddhism there is Mara, which is like Satan in Christianity.

260
00:43:09,400 --> 00:43:17,400
So, sometimes Mara is considered to be a person.

261
00:43:17,400 --> 00:43:25,400
But many people think that it is person-ified and wholesome state.

262
00:43:25,400 --> 00:43:27,400
But here it is taken as a person.

263
00:43:27,400 --> 00:43:36,400
So, this man met Mara and then he was, this man was very devoted to the Buddha.

264
00:43:36,400 --> 00:43:39,400
He was not born at the time of the Buddha.

265
00:43:39,400 --> 00:43:43,400
So, he wanted to see the Buddha.

266
00:43:43,400 --> 00:43:46,400
He wanted to see the likeness of the Buddha.

267
00:43:46,400 --> 00:43:51,400
And Mara was alive when Buddha was living.

268
00:43:51,400 --> 00:43:59,400
And so, he asked Mara to assume the form of the Buddha so that he could look at him.

269
00:43:59,400 --> 00:44:04,400
So, the Mara assumed the form of the Buddha.

270
00:44:04,400 --> 00:44:09,400
So, looking at the form of the Buddha we thought, how good this appears,

271
00:44:09,400 --> 00:44:13,400
but it has increased hate and delusion.

272
00:44:13,400 --> 00:44:19,400
That means, this man, this Mara has increased hatred and delusion in him.

273
00:44:19,400 --> 00:44:26,400
Even then, when he assumes the form of the Buddha, it is very, very beautiful, very inspiring.

274
00:44:26,400 --> 00:44:30,400
And what can the Blessed One's goodness have been like?

275
00:44:30,400 --> 00:44:39,400
So, the real Buddha, the real appearance of the Buddha might be thousands of times better than this.

276
00:44:39,400 --> 00:44:42,400
For, he was quite without greed, hate and delusion.

277
00:44:42,400 --> 00:44:45,400
He acquired happiness with the Blessed One as subject.

278
00:44:45,400 --> 00:44:54,400
And by augmenting his insight, that means developing his wipasana, he reached at a handshake.

279
00:44:54,400 --> 00:45:07,400
So, after practicing recollection with editions, one can go over a change to wipasana and practice wipasana meditation.

280
00:45:07,400 --> 00:45:15,400
So, wipasana meditation is trying to see the impermanence, suffering and soullessness of things.

281
00:45:15,400 --> 00:45:22,400
And so, it can take any, any state, anything as an object.

282
00:45:22,400 --> 00:45:32,400
Sometimes, they just take the joy, the, the good form, the recollection.

283
00:45:32,400 --> 00:45:37,400
So, they look, look upon joy as impermanence and so on.

284
00:45:37,400 --> 00:45:40,400
And practice wipasana meditation and develop wipasana.

285
00:45:40,400 --> 00:45:44,400
To get enlightenment.

286
00:45:44,400 --> 00:45:51,400
Now, the next one, mindfulness of death.

287
00:45:51,400 --> 00:46:05,400
Now, death is defined here as the interruption of the life faculty included within the limits of a single becoming or single existence.

288
00:46:05,400 --> 00:46:10,400
That means, in one life, a human life or an animal life or whatever.

289
00:46:10,400 --> 00:46:20,400
An interruption really means cessation or being cut off.

290
00:46:20,400 --> 00:46:28,400
So, stoppage or cessation of the life faculty, you know life faculty, givita and father.

291
00:46:28,400 --> 00:46:34,400
Something which keeps our bodies alive and that is called life faculty.

292
00:46:34,400 --> 00:46:38,400
And also, there is life faculty of mine.

293
00:46:38,400 --> 00:46:58,400
And there are two life faculty's physical.

