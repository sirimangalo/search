1
00:00:00,000 --> 00:00:24,600
Good evening, everyone broadcasting live April 14th.

2
00:00:24,600 --> 00:00:35,600
What is quote is from Brahma Aayu, Subtra Subta.

3
00:00:35,600 --> 00:00:42,600
Everyone was a Brahmin named Brahma Aayu.

4
00:00:42,600 --> 00:00:55,600
Living in Midhila, Midhila.

5
00:00:55,600 --> 00:01:03,600
Isn't that the...

6
00:01:03,600 --> 00:01:04,600
Let's see.

7
00:01:04,600 --> 00:01:08,600
Yeah, that is the capital.

8
00:01:08,600 --> 00:01:13,600
That's where...

9
00:01:13,600 --> 00:01:21,600
That's the residence of King Janaka in Janaka.

10
00:01:21,600 --> 00:01:29,600
And the Mahajana Kajatika and the Mahajana Kajatika.

11
00:01:29,600 --> 00:01:36,600
The Mahosadha Kajatika.

12
00:01:36,600 --> 00:01:37,600
See where...

13
00:01:37,600 --> 00:01:44,600
Where Midhila was.

14
00:01:44,600 --> 00:01:50,600
Generally identified with Janaka Pura, there is a place called Janaka Pura.

15
00:01:50,600 --> 00:01:54,600
It's interesting.

16
00:01:54,600 --> 00:01:58,600
Huh.

17
00:01:58,600 --> 00:02:09,600
I will even do it.

18
00:02:09,600 --> 00:02:24,600
Janaka Pura.

19
00:02:24,600 --> 00:02:27,600
I feel the kingdom.

20
00:02:27,600 --> 00:02:31,600
So Midhila is famous place.

21
00:02:31,600 --> 00:02:34,600
Nothing to do with that quote.

22
00:02:34,600 --> 00:02:37,600
It's still interesting.

23
00:02:37,600 --> 00:02:48,600
So Brahmin and Brahma Aayu sends his student Uptara to go and see the Buddha.

24
00:02:48,600 --> 00:02:56,600
And...

25
00:02:56,600 --> 00:03:01,600
The what's neat about this Sukhta is it gives some details about the Buddha's life.

26
00:03:01,600 --> 00:03:03,600
The Buddha's manner.

27
00:03:03,600 --> 00:03:06,600
Not really stick with you.

28
00:03:06,600 --> 00:03:12,600
So it talks about the 32 marks of a great man, which a great being, which are...

29
00:03:12,600 --> 00:03:15,600
Wow, they're all physical.

30
00:03:15,600 --> 00:03:20,600
So the Buddha had a specific physical shape.

31
00:03:20,600 --> 00:03:29,600
He was not like an ordinary human being in many different ways.

32
00:03:29,600 --> 00:03:31,600
But then it goes on.

33
00:03:31,600 --> 00:03:34,600
After the 32 marks it gets into interesting stuff.

34
00:03:34,600 --> 00:03:39,600
And so one of them is the quote that we have today, but there's several here.

35
00:03:39,600 --> 00:03:44,600
When he walks, he steps out with the right foot first.

36
00:03:44,600 --> 00:03:48,600
It does not extend his foot too far or put it down too near.

37
00:03:48,600 --> 00:03:56,600
So when we do walking meditation, we always tell the meditators to walk with the right foot first.

38
00:03:56,600 --> 00:03:58,600
And maybe this is a reason for it.

39
00:03:58,600 --> 00:04:07,600
It's kind of... There's obviously nothing special about the right foot, although it seems to be a habit of the Buddha.

40
00:04:07,600 --> 00:04:14,600
Every time you walk with the right foot first, you can think while I'm doing it the way the Buddha did it.

41
00:04:14,600 --> 00:04:16,600
Kind of give you some encouragement.

42
00:04:16,600 --> 00:04:18,600
Make you think of the Buddha.

43
00:04:18,600 --> 00:04:23,600
He walks neither too quickly nor too slowly.

44
00:04:23,600 --> 00:04:28,600
This is the answer that my teacher gives.

45
00:04:28,600 --> 00:04:33,600
When someone asks whether they should do walking meditation quickly or slowly.

46
00:04:33,600 --> 00:04:37,600
Should I slow down? Should I do it quickly?

47
00:04:37,600 --> 00:04:40,600
Not too quickly, not too slowly.

48
00:04:40,600 --> 00:04:43,600
He walks without his knees knocking together.

49
00:04:43,600 --> 00:04:46,600
He walks without his ankles knocking together.

50
00:04:46,600 --> 00:04:51,600
He walks without raising or lowering his thighs or bringing them together or keeping them apart.

51
00:04:51,600 --> 00:04:56,600
When he walks, only the lower part of his body oscillates.

52
00:04:56,600 --> 00:05:00,600
I mean, he does not walk with bodily effort.

53
00:05:00,600 --> 00:05:04,600
When he turns to look, he does so with his whole body.

54
00:05:04,600 --> 00:05:08,600
This is called the elephant gaze.

55
00:05:08,600 --> 00:05:12,600
Or it's one way of interpreting what is called the elephant gaze.

56
00:05:12,600 --> 00:05:22,600
The Buddha, he wouldn't often turn, but when he did, he would turn his whole body like an elephant.

57
00:05:22,600 --> 00:05:27,600
Because if an elephant is going to look behind itself, it has to turn all the way around.

58
00:05:27,600 --> 00:05:33,600
Another particular idiosyncrasy of a Buddha.

59
00:05:33,600 --> 00:05:44,600
He did this when he came out of Weiss, I think, three months before he entered into Parnia.

60
00:05:44,600 --> 00:05:49,600
He turns around and looks at Weiss.

61
00:05:49,600 --> 00:05:53,600
This is the last time I'm going to see this.

62
00:05:53,600 --> 00:05:59,600
He was never going to come back.

63
00:05:59,600 --> 00:06:03,600
He does not look straight up. He does not look straight down.

64
00:06:03,600 --> 00:06:05,600
He does not walk looking about.

65
00:06:05,600 --> 00:06:07,600
He looks a plow yoke's length before him.

66
00:06:07,600 --> 00:06:10,600
Beyond that, he has unhindered knowledge and vision.

67
00:06:10,600 --> 00:06:14,600
So he looks about six feet out.

68
00:06:14,600 --> 00:06:20,600
When he goes indoors, he does not raise or lower his body or bend it forward or back.

69
00:06:20,600 --> 00:06:24,600
He turns neither too far from the seat nor too near it.

70
00:06:24,600 --> 00:06:29,600
He does not lean on the seat with his hand. He does not throw his body onto the seat.

71
00:06:29,600 --> 00:06:34,600
So when he comes near a seat to sit upon, he turns to sit on it, but he does it.

72
00:06:34,600 --> 00:06:40,600
This is a guy having observed the Buddha and relating this back to his teacher,

73
00:06:40,600 --> 00:06:45,600
just how incredible the Buddha lives, just how impressive he was.

74
00:06:45,600 --> 00:06:48,600
Every little meticulous detail.

75
00:06:48,600 --> 00:06:58,600
And so it's a unique look. I think the only place where we really have this much detail about the Buddha.

76
00:06:58,600 --> 00:07:04,600
When seated indoors, he does not fidget with his hands. He does not fidget with his feet.

77
00:07:04,600 --> 00:07:08,600
He does not sit with his knees crossed. He does not sit with his ankles crossed.

78
00:07:08,600 --> 00:07:13,600
He does not sit with his hands holding his hand, holding his chin.

79
00:07:13,600 --> 00:07:18,600
When seated indoors, he is not afraid. He does not shiver in tremble. He is not nervous.

80
00:07:18,600 --> 00:07:22,600
Being unafraid, not shivering or trembling or nervous.

81
00:07:22,600 --> 00:07:27,600
His hair does not stand up and he is intent on seclusion.

82
00:07:27,600 --> 00:07:31,600
Intent on seclusion even when he is in a crowd.

83
00:07:31,600 --> 00:07:34,600
When surrounded by people, all he is thinking about his seclusion.

84
00:07:34,600 --> 00:07:37,600
His mind is focused on seclusion.

85
00:07:37,600 --> 00:07:42,600
And part of the implication is getting out of there as soon as possible.

86
00:07:42,600 --> 00:07:55,600
And at one point, the Buddha says that he teaches in whatever way he can to make the person leave as quickly as possible.

87
00:07:55,600 --> 00:08:01,600
Which is curious because you think that's kind of how rude of him to think like that.

88
00:08:01,600 --> 00:08:19,600
It's really kind of special to think of you because it's not about dismissing people.

89
00:08:19,600 --> 00:08:22,600
It's about working out your karma with someone comes to you.

90
00:08:22,600 --> 00:08:24,600
There's some kind of relationship there.

91
00:08:24,600 --> 00:08:29,600
And it's interesting, if you reject people, if you just reject them outright,

92
00:08:29,600 --> 00:08:34,600
they're going to come back. You're building up karma with them.

93
00:08:34,600 --> 00:08:38,600
And they will come back if not in this life in the next, but usually in this life they'll come back.

94
00:08:38,600 --> 00:08:41,600
And there's still be that tension.

95
00:08:41,600 --> 00:08:50,600
But to really get rid of someone, you have to work everything out so that you're free from that karma, the debt,

96
00:08:50,600 --> 00:08:58,600
or the concerns of people coming to accuse you or coming to tag you or even coming to ask you a question.

97
00:08:58,600 --> 00:09:05,600
If you don't satisfy them, there's tension there.

98
00:09:05,600 --> 00:09:11,600
You have to do your part. Sometimes if someone's yelling at you, you don't have to appease them.

99
00:09:11,600 --> 00:09:17,600
Sometimes like the Buddha said, you get angry at me, I don't get angry at you, you get to keep the anger.

100
00:09:17,600 --> 00:09:22,600
Just like you bring me a gift, I don't take the gift.

101
00:09:22,600 --> 00:09:29,600
But anyway, it's kind of a curious not how people would think of someone like the Buddha.

102
00:09:29,600 --> 00:09:32,600
They'd think, oh yes, he's so compassionate and he wants to help people.

103
00:09:32,600 --> 00:09:39,600
Not really. He would help people in whatever way he can to end it,

104
00:09:39,600 --> 00:09:49,600
to be free from that burden or that interaction.

105
00:09:49,600 --> 00:09:54,600
Because it's all about working yourself to be free.

106
00:09:54,600 --> 00:10:00,600
If you want to teach people how to be free, you have to be free yourself.

107
00:10:00,600 --> 00:10:06,600
So if you don't put your own freedom first, you're not teaching other people to be free.

108
00:10:06,600 --> 00:10:10,600
You're teaching other people to be trapped.

109
00:10:10,600 --> 00:10:14,600
You yourself have to be free to be able to teach it.

110
00:10:14,600 --> 00:10:20,600
So always concerned with seclusion.

111
00:10:20,600 --> 00:10:24,600
When he receives water, he does not raise, it talks about water.

112
00:10:24,600 --> 00:10:29,600
Again, there's a little thing. He washes the bull out without making a splashing noise.

113
00:10:29,600 --> 00:10:35,600
And it should read through his smidgey minica in number 91.

114
00:10:35,600 --> 00:10:51,600
When he receives rice, he turns, and he does not exceed the right amount of sauce in the mouth.

115
00:10:51,600 --> 00:10:57,600
He turns the mouthful over two or three times in his mouth and then swallows it.

116
00:10:57,600 --> 00:11:03,600
And no rice, kernel, enters his body, uncued, and no rice kernel remains in his mouth.

117
00:11:03,600 --> 00:11:08,600
So every single rice kernel is chewed before it goes down his throat.

118
00:11:08,600 --> 00:11:12,600
And no rice kernel remains in his mouth, and then he takes another mouthful.

119
00:11:12,600 --> 00:11:17,600
So not even a single rice kernel is that before he goes through the next box.

120
00:11:17,600 --> 00:11:22,600
He takes his food, experiencing the taste, though not experiencing greed for the taste.

121
00:11:22,600 --> 00:11:25,600
I wonder how this guy knew all this.

122
00:11:25,600 --> 00:11:31,600
He must have been very observant, or perhaps this has been embellished or extrapolated.

123
00:11:31,600 --> 00:11:34,600
The food he takes has eight factors.

124
00:11:34,600 --> 00:11:40,600
It is neither for amusement nor for intoxication nor for the sake of physical beauty and attractiveness,

125
00:11:40,600 --> 00:11:48,600
but only for the endurance and continuation of his body for the ending of discomfort for the assisting the holy life.

126
00:11:48,600 --> 00:11:52,600
He considers less shall I terminate old feelings without arousing new feelings.

127
00:11:52,600 --> 00:11:55,600
And I shall be healthy and blameless and live in comfort.

128
00:11:55,600 --> 00:12:01,600
This is actually the reflection that we make before we, in bigger bodies, is the same.

129
00:12:01,600 --> 00:12:05,600
This is the standard reflection of the properties of all the food,

130
00:12:05,600 --> 00:12:08,600
but the san kai only is opened by them, but they say,

131
00:12:08,600 --> 00:12:24,180
we need to go to the

132
00:12:24,180 --> 00:12:27,180
hankami no one to wait and no body.

133
00:12:27,180 --> 00:12:33,180
Yet Rajami and Bhavi sati on the watch at the end of the past.

134
00:12:33,180 --> 00:12:39,180
The watch at the past, so we have no chatty.

135
00:12:39,180 --> 00:12:43,180
We say that before we eat usually.

136
00:12:43,180 --> 00:12:45,180
There is a reflection.

137
00:12:45,180 --> 00:12:47,180
So he probably heard the Buddha say that.

138
00:12:47,180 --> 00:12:51,180
We know this is what we have in the body.

139
00:12:51,180 --> 00:12:56,180
And after he's eaten, detail, detail about eating,

140
00:12:56,180 --> 00:13:00,180
he walks in either in the past or in the past or in the past or in the past.

141
00:13:00,180 --> 00:13:05,180
His robe is worn either too high or too low.

142
00:13:05,180 --> 00:13:07,180
He watches his feet.

143
00:13:07,180 --> 00:13:10,180
He sits mindfully.

144
00:13:10,180 --> 00:13:16,180
He finds it on welfare, on his own welfare, on the welfare, on the welfare of others,

145
00:13:16,180 --> 00:13:20,180
on the welfare of both, even on the welfare of the whole world.

146
00:13:20,180 --> 00:13:22,180
And here's the quote that we have today.

147
00:13:22,180 --> 00:13:26,180
When he is gone to the monastery, he teaches the dhamma to an audience.

148
00:13:26,180 --> 00:13:29,180
He neither flatters nor berates his audience.

149
00:13:29,180 --> 00:13:32,180
He instructs urges and encourages it.

150
00:13:32,180 --> 00:13:35,180
It will talk purely on the dhamma.

151
00:13:35,180 --> 00:13:40,180
No, this isn't that cool.

152
00:13:40,180 --> 00:13:44,180
I don't think the quote is in the citizen.

153
00:13:44,180 --> 00:13:51,180
I think he's got the wrongs with the...

154
00:13:51,180 --> 00:13:56,180
Hmm.

155
00:13:56,180 --> 00:13:58,180
Oh, yeah, no, this isn't.

156
00:13:58,180 --> 00:13:59,180
Right?

157
00:13:59,180 --> 00:14:01,180
The speech has, let me see.

158
00:14:01,180 --> 00:14:03,180
Is that a quote?

159
00:14:03,180 --> 00:14:04,180
Yeah, has a quote.

160
00:14:04,180 --> 00:14:05,180
It's okay.

161
00:14:05,180 --> 00:14:07,180
Yeah, there's just this quote.

162
00:14:07,180 --> 00:14:10,180
You know, there are flatters nor berates the audience.

163
00:14:10,180 --> 00:14:14,180
He instructs urges, rouses, and encourages it with talk purely on the dhamma.

164
00:14:14,180 --> 00:14:17,180
The speech that issues from his mouth has eight qualities.

165
00:14:17,180 --> 00:14:22,180
It is distinct, intelligible, melodious, audible, ringing,

166
00:14:22,180 --> 00:14:25,180
euphonious, deep, and so on.

167
00:14:25,180 --> 00:14:30,180
But while his voice is intelligible as far as the audience extends,

168
00:14:30,180 --> 00:14:33,180
his speech does not issue out beyond the audience.

169
00:14:33,180 --> 00:14:35,180
And magical.

170
00:14:35,180 --> 00:14:38,180
When the people have been instructed, urged,

171
00:14:38,180 --> 00:14:41,180
housed and gladdened by him, they rise from the seats and depart,

172
00:14:41,180 --> 00:14:45,180
looking only at him and concerned with nothing else.

173
00:14:45,180 --> 00:14:47,180
That's an inspiring quote.

174
00:14:47,180 --> 00:14:57,180
And this whole suit has a lot about inspiration rather than instruction.

175
00:14:57,180 --> 00:15:02,180
A lot of it is about Buddha himself and about his great qualities

176
00:15:02,180 --> 00:15:06,180
and how impressive he was.

177
00:15:06,180 --> 00:15:16,180
As a sort of an aid to help us think and appreciate who the Buddha actually was.

178
00:15:16,180 --> 00:15:19,180
It's beautiful in that way.

179
00:15:19,180 --> 00:15:21,180
It's impressive.

180
00:15:21,180 --> 00:15:33,180
Having an impressive role model of examples is that there's a definite benefit to it.

181
00:15:33,180 --> 00:15:37,180
And so that's what we used to do like this for.

182
00:15:37,180 --> 00:15:45,180
On the other hand, in the end, the Buddha does come and teach some things.

183
00:15:45,180 --> 00:15:47,180
Let me see.

184
00:15:47,180 --> 00:15:50,180
What does he actually get around to teaching?

185
00:15:50,180 --> 00:15:51,180
Here we go.

186
00:15:51,180 --> 00:15:52,180
He asks the Buddha some questions.

187
00:15:52,180 --> 00:15:54,180
How does one become a Brahman?

188
00:15:54,180 --> 00:15:59,180
And how does one attain knowledge?

189
00:15:59,180 --> 00:16:02,180
Let's see if we can find the Pali.

190
00:16:02,180 --> 00:16:10,180
Here we go firstly go firstly to teach a job.

191
00:16:10,180 --> 00:16:19,180
He is proud.

192
00:16:19,180 --> 00:16:29,180
Thelocks of the Buddha are not that significant.

193
00:16:29,180 --> 00:16:37,220
They even, in the Buddhist time, they had all sorts of questions.

194
00:16:37,220 --> 00:16:38,460
How does one become a Brahmin?

195
00:16:38,460 --> 00:16:44,500
How does one become way too good when it was gone to Vedas, to knowledge?

196
00:16:44,500 --> 00:16:45,500
Wisdom.

197
00:16:45,500 --> 00:16:49,180
Devinger, how does one become triple knowledge?

198
00:16:49,180 --> 00:16:54,420
How is one called a Muni, scholar, sage?

199
00:16:54,420 --> 00:16:55,900
How does one become an Rahan?

200
00:16:55,900 --> 00:16:58,020
How does one attain completeness?

201
00:16:58,020 --> 00:17:01,020
What is that?

202
00:17:01,020 --> 00:17:10,300
What is the key for the full accomplishment?

203
00:17:10,300 --> 00:17:16,260
How is one of the silent, sage, Muni-chaboo-katam-ho-ti?

204
00:17:16,260 --> 00:17:24,340
The first one was a Muni-was-so-ti, which is a learning man.

205
00:17:24,340 --> 00:17:29,300
The Muni is a...

206
00:17:29,300 --> 00:17:32,300
How is one a Muni?

207
00:17:32,300 --> 00:17:35,460
How is one called a Buddha?

208
00:17:35,460 --> 00:17:39,940
So here is the Buddha's teaching.

209
00:17:39,940 --> 00:17:41,580
That's fine teaching.

210
00:17:41,580 --> 00:18:05,860
One who knows their past lives, who sees heaven and hell, who has attained the ending of

211
00:18:05,860 --> 00:18:06,860
birth.

212
00:18:06,860 --> 00:18:17,820
One who has accomplished in higher knowledge, Armenia, such a person is a Muni.

213
00:18:17,820 --> 00:18:33,300
They know the purification of mind or the pure mind, they know the pure mind.

214
00:18:33,300 --> 00:18:44,780
Tung Ragi, Sabasu, free from Raga, entirely, all together, all together, free from Raga,

215
00:18:44,780 --> 00:18:48,100
from passion, desire.

216
00:18:48,100 --> 00:18:55,380
But he in a Jati moderno, Brahma Jari, Jari Asa gave me.

217
00:18:55,380 --> 00:19:01,300
One is complete, who has abandoned birth and death.

218
00:19:01,300 --> 00:19:15,580
Through the holy life, one who has abandoned birth and death, one who has completed the

219
00:19:15,580 --> 00:19:21,100
holy life, become complete, come to the completion of the holy life.

220
00:19:21,100 --> 00:19:33,180
One who has gone to the farther shore, to the final, ultimate knowledge.

221
00:19:33,180 --> 00:19:36,460
Baragum means literally one who has gone to the other shore.

222
00:19:36,460 --> 00:19:44,420
But it's where the Buddha uses often in a metaphorical sense of one who has gone to the

223
00:19:44,420 --> 00:19:48,900
end, who has seen through to completion.

224
00:19:48,900 --> 00:19:56,860
Baba Dhamma, who has come to the end of all Dhammas, or who has come to know and come

225
00:19:56,860 --> 00:20:00,220
to be complete in regards to all Dhammas.

226
00:20:00,220 --> 00:20:15,620
Buddha, Tada, Deepo, Jati, then with us, with Tada, such a person is called the Buddha.

227
00:20:15,620 --> 00:20:25,580
And then the Buddha gave him more instruction, which is, Taka, Anupubing, Katankathe,

228
00:20:25,580 --> 00:20:32,620
and then he gave the Anupubikata, which we call the Anupubikata, teaching in order.

229
00:20:32,620 --> 00:20:38,780
So he taught him about Dhamma, about charity, sealer morality, sangha, about heaven, kama,

230
00:20:38,780 --> 00:20:49,860
Anang, Adi, noang, and the Adi, noang, okarang, sankele, sankele, sankele, sankele, this advantages,

231
00:20:49,860 --> 00:20:56,740
the degradation and the defilement, the impurity of sensuality.

232
00:20:56,740 --> 00:21:05,420
Nikami, Anisam, sanke, the benefits of pronunciation.

233
00:21:05,420 --> 00:21:13,860
Benjamin Akai is really pretty awesome, it's got 152 suit on all sorts of subjects, so you

234
00:21:13,860 --> 00:21:19,420
find all sorts of gems, and then all sorts of things.

235
00:21:19,420 --> 00:21:27,420
Anyway, there's some Dhamma for this evening.

236
00:21:27,420 --> 00:21:38,300
So we have two meditators, and they both finish today, did really well, I can certify

237
00:21:38,300 --> 00:21:44,460
both of them as having finished the course, which is awesome, not certified, but I can

238
00:21:44,460 --> 00:21:55,220
vouch for them having done a really good job in their course.

239
00:21:55,220 --> 00:22:01,900
And I finished two exams, wrote about Buddhism this morning, Buddhism in East Asia, the three

240
00:22:01,900 --> 00:22:03,060
questions.

241
00:22:03,060 --> 00:22:11,540
The new thing about it, our professor is, I think he's a practicing Buddhist, and he

242
00:22:11,540 --> 00:22:18,780
was conscious of the amount of stress that people have, so he gave us the exam in advance.

243
00:22:18,780 --> 00:22:23,620
He did the same with the midterm, but he gave us the final exam to prepare an advance.

244
00:22:23,620 --> 00:22:31,460
So it was fairly stress-free for most of the classes, as far as worrying about what's

245
00:22:31,460 --> 00:22:35,020
going to be on end, or about any surprises, or about what the study, it was quite clear

246
00:22:35,020 --> 00:22:40,260
where you had to study, it's just a matter of putting in the time.

247
00:22:40,260 --> 00:22:49,340
And so I wrote about practice and belief, and the first section was a short answer.

248
00:22:49,340 --> 00:22:57,580
So it was a shorter answer, a short essay about practice versus belief.

249
00:22:57,580 --> 00:23:09,780
Because in East Asia, in most religion, of course, the most obvious relationship between

250
00:23:09,780 --> 00:23:15,700
belief and practice is how our belief informs our practice, that's fairly standard.

251
00:23:15,700 --> 00:23:21,740
But I looked at the other ways as well, or I mentioned the other, how it's possible for

252
00:23:21,740 --> 00:23:27,340
one's practice, to influence one's beliefs in the sense that one can be forced into certain

253
00:23:27,340 --> 00:23:32,420
practices, or have influences, meaning one to practice.

254
00:23:32,420 --> 00:23:41,940
And in a way, for example, the state in China, in Korea, in Japan had a pretty heavy hand

255
00:23:41,940 --> 00:23:49,700
on Buddhist practitioners, and monks, and teachers, and so on.

256
00:23:49,700 --> 00:23:58,460
So they were often compelled to practice in certain ways, and this would have influenced

257
00:23:58,460 --> 00:24:01,140
peoples' beliefs.

258
00:24:01,140 --> 00:24:08,380
Also practices, worldly practices, or peoples' desires, and attachments, and views, and

259
00:24:08,380 --> 00:24:14,140
egos, and so on, would have affected peoples' beliefs, and how they interpreted the

260
00:24:14,140 --> 00:24:16,620
dilemma.

261
00:24:16,620 --> 00:24:22,740
Another way of looking at these two is how belief and practice can sometimes conflict.

262
00:24:22,740 --> 00:24:32,260
We've got this interesting thing, pure land Buddhism is really weird, not really impressed,

263
00:24:32,260 --> 00:24:39,740
but here's the idea of a pure land, see there was this guy, Genshin, Genshin, who said

264
00:24:39,740 --> 00:24:47,820
that reciting the Buddha's name, and in fact not our Buddha, but another Buddha, was the

265
00:24:47,820 --> 00:24:50,020
best practice.

266
00:24:50,020 --> 00:24:56,100
So he said, there's all these other practices, but reciting the name amitabaa is the best

267
00:24:56,100 --> 00:24:57,100
practice.

268
00:24:57,100 --> 00:25:02,140
Okay, so this is his idea.

269
00:25:02,140 --> 00:25:09,700
The next guy who came along was named Hounen, and Hounen said, Hounen privately, he didn't

270
00:25:09,700 --> 00:25:16,860
go public with it in the beginning, but he wrote a book in private that said that practice

271
00:25:16,860 --> 00:25:26,700
is useless, and the only thing that's effective is reciting the Buddha's name.

272
00:25:26,700 --> 00:25:30,660
Okay, so he's gone another step.

273
00:25:30,660 --> 00:25:35,820
The next guy who came along, Shinran, went even farther.

274
00:25:35,820 --> 00:25:40,700
If you read about these guys, it's like, oh dear, one further and further, it was like

275
00:25:40,700 --> 00:25:42,300
interpretation of Hounen.

276
00:25:42,300 --> 00:25:52,420
So Shinran says, not only is, practice is not just useless, practice is actually harmful.

277
00:25:52,420 --> 00:25:53,660
Why is practice harmful?

278
00:25:53,660 --> 00:26:07,620
And he said, his quote was that a good person, a good person will get, let me see, even

279
00:26:07,620 --> 00:26:16,780
a good person can get into knowledge, not the quote, but it twisted it.

280
00:26:16,780 --> 00:26:22,220
He said, it was basically saying that evil people have an easier time getting to the

281
00:26:22,220 --> 00:26:25,700
pure land than good people.

282
00:26:25,700 --> 00:26:26,700
And why is that?

283
00:26:26,700 --> 00:26:33,140
Because good people practice Buddhism, and by practicing Buddhism, they're showing that

284
00:26:33,140 --> 00:26:39,180
they don't have faith in a meter bath, because otherwise why would you practice?

285
00:26:39,180 --> 00:26:42,420
If you had perfect faith in a meter bath, you're just like, well, wait for a meter bath

286
00:26:42,420 --> 00:26:43,420
to save me.

287
00:26:43,420 --> 00:26:46,060
The meter bath will take me to the period.

288
00:26:46,060 --> 00:26:56,660
So his idea was that evil people, because they don't practice, because really scary stuff,

289
00:26:56,660 --> 00:26:57,660
actually.

290
00:26:57,660 --> 00:27:02,620
I mean, this is a quote with his, and it's really not Buddhism, as I understand it.

291
00:27:02,620 --> 00:27:07,260
So we had a little bit, we've had some debates about this, and in class, right before

292
00:27:07,260 --> 00:27:14,540
the exam, I said to one of them, I said, you know, I said, this really isn't Buddhism

293
00:27:14,540 --> 00:27:19,420
anymore, and she kind of smiled, and I said, I know, I mean, what I have to say is it's

294
00:27:19,420 --> 00:27:25,180
a different type of Buddhism, and she kind of smiled again, because we understand that,

295
00:27:25,180 --> 00:27:30,740
you know, it's like the no true Scotsman policy, if you ever look up the no true Scotsman

296
00:27:30,740 --> 00:27:34,580
policy, you can't just say someone's not a Buddhist, if they claim to be Buddhist, but

297
00:27:34,580 --> 00:27:43,180
I said, but here's the thing, Buddhism, if a religion arises, and when it arises, it

298
00:27:43,180 --> 00:27:55,060
's core doctrine is X, and then later, a new doctrine arises, not X. If the new doctrine

299
00:27:55,060 --> 00:28:01,540
takes not X, how can you call it the religion, the same religion, right?

300
00:28:01,540 --> 00:28:07,220
It's like, okay, you can adapt and reinterpret things, but if something is X, and then

301
00:28:07,220 --> 00:28:14,900
other, you know, this is very much not X here, then Buddhism, anyway.

302
00:28:14,900 --> 00:28:17,420
So I had to talk about these kinds of things.

303
00:28:17,420 --> 00:28:27,100
The second, the two longer questions, two longer essays were about decline, and the individual

304
00:28:27,100 --> 00:28:30,300
versus the state.

305
00:28:30,300 --> 00:28:37,620
So decline is, I mean, again, there's a much more related East Asian Buddhism than Buddhism

306
00:28:37,620 --> 00:28:41,700
as we practice it, or I practice it.

307
00:28:41,700 --> 00:28:52,020
So again, it's kind of scholarly, as opposed to practical, anyway, I talked about decline

308
00:28:52,020 --> 00:28:59,740
and how decline, what the decline of the Dharma is interesting to ask whether the Dharma

309
00:28:59,740 --> 00:29:11,020
is actually considered to be declining, or what's more understood to be the cases, people's

310
00:29:11,020 --> 00:29:12,540
ability to practice the Dharma.

311
00:29:12,540 --> 00:29:27,260
The Dharma doesn't change, the Dharma, but one exception is the fact that the Dharma becomes

312
00:29:27,260 --> 00:29:31,820
corrupt, you see, because we don't really know exactly what the Buddha taught.

313
00:29:31,820 --> 00:29:38,100
We claim that in teravada we have the original teachings, you know, we may not.

314
00:29:38,100 --> 00:29:42,620
But at any rate, it's supposing even that we did, well, you know, even those teachings

315
00:29:42,620 --> 00:29:49,300
are reinterpreted and are mixed with new teachings and replaced by new teachings and that

316
00:29:49,300 --> 00:29:52,300
kind of thing.

317
00:29:52,300 --> 00:29:57,180
So this is how the Dharma becomes corrupted, and the decline of the Dharma is when

318
00:29:57,180 --> 00:30:05,060
people stop paying attention to the deep teachings, and they promote surface teachings,

319
00:30:05,060 --> 00:30:11,420
like the lotus sutra or even worse, there are sutras like the sutra of humane kings,

320
00:30:11,420 --> 00:30:18,260
the golden light sutra that are very, very worldly and much more about empire building

321
00:30:18,260 --> 00:30:25,140
and state building, which is what I got into the second in the final question, but individual

322
00:30:25,140 --> 00:30:27,980
versus state.

323
00:30:27,980 --> 00:30:35,860
Individuals tend to affect religion in unpredictable ways, idiosyncratic, they come up with

324
00:30:35,860 --> 00:30:43,060
their own ideas and they create innovation.

325
00:30:43,060 --> 00:30:49,180
Whereas the state, on the other hand, is all about stifling and innovation, systematizing,

326
00:30:49,180 --> 00:30:55,740
controlling, manipulating, using for their own purposes.

327
00:30:55,740 --> 00:30:58,780
So they tend to be more predictable.

328
00:30:58,780 --> 00:31:05,580
Anyway, so that was my second exam.

329
00:31:05,580 --> 00:31:12,740
Yeah, it did really well on both exams and neither one was that difficult, as it turns

330
00:31:12,740 --> 00:31:19,740
out.

331
00:31:19,740 --> 00:31:22,020
When Sunday we had this storytelling thing, I don't think I've talked about that yet.

332
00:31:22,020 --> 00:31:27,060
It didn't really well, I told the story, I think I went way over time, I think we all kind

333
00:31:27,060 --> 00:31:28,060
of went way over time.

334
00:31:28,060 --> 00:31:32,300
My story was quite short, so I thought I'll have to talk about it, and I think I ended

335
00:31:32,300 --> 00:31:33,300
up talking to him.

336
00:31:33,300 --> 00:31:40,540
I was really well-appreciated, the First Nations guy said, why we said good words,

337
00:31:40,540 --> 00:31:52,820
I hope they can, it's quite a character.

338
00:31:52,820 --> 00:31:57,780
And tomorrow I'm off to New York, so I actually should probably go and start preparing

339
00:31:57,780 --> 00:32:02,620
for my trip, but I'll be gone for nine days.

340
00:32:02,620 --> 00:32:08,300
I would think I'll probably do, as long as they have internet, I'll do some audio

341
00:32:08,300 --> 00:32:13,620
broadcasts, so I'll try my best, depending on the time I don't, I think my schedule is mostly

342
00:32:13,620 --> 00:32:20,580
afternoon stuff, and I'm kind of taking it day by day, of course, but I should look

343
00:32:20,580 --> 00:32:27,260
at the New York schedule, as long as I'm not busy, I'll try to broadcast every night,

344
00:32:27,260 --> 00:32:28,260
just audio.

345
00:32:28,260 --> 00:32:33,220
So you can find the audio here at meditation.

346
00:32:33,220 --> 00:32:40,420
Anyway, thanks for tuning in everyone, have a good night, and we'll be well.

