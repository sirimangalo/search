1
00:00:00,000 --> 00:00:12,080
Welcome back to Ask amount. Today's question is on envy and competitiveness. When we acknowledge

2
00:00:12,080 --> 00:00:23,320
the fact that these states are common in the world, how can we hope to remedy them?

3
00:00:23,320 --> 00:00:29,760
So there are two different answers to this question depending on which way you look at it.

4
00:00:29,760 --> 00:00:38,720
If you're talking about remedying it within yourself, then it's a much easier question to

5
00:00:38,720 --> 00:00:45,720
answer. If you're talking about remedying it in the world, then it requires perhaps a little

6
00:00:45,720 --> 00:00:51,800
bit of extra explanation. But I'll try to go through both of these. As for remedying it in

7
00:00:51,800 --> 00:01:05,240
ourselves, the remedy for states like envy and competitiveness are a reassessment of our idea

8
00:01:05,240 --> 00:01:19,520
of what is worth, what has intrinsic worth, what is beneficial. I think too often we

9
00:01:19,520 --> 00:01:31,720
mistake the cause or we think of the things that we are competing for as intrinsically

10
00:01:31,720 --> 00:01:37,720
valuable. And so even though we might acknowledge the fact that envy and competitiveness

11
00:01:37,720 --> 00:01:49,480
are negative states, we limit our attempts to mitigate them or to eradicate them to the

12
00:01:49,480 --> 00:02:03,840
suppression or the limiting of our desires. So we agree that the things that we're competing

13
00:02:03,840 --> 00:02:12,280
for are worthwhile, but because everyone wants them, we have to either compete for them,

14
00:02:12,280 --> 00:02:18,760
which we think is wrong, or we have to suppress our desire for them, and we have to come

15
00:02:18,760 --> 00:02:27,400
to some sort of compromise where everyone gets part of what they would like to what

16
00:02:27,400 --> 00:02:37,600
they want. Now I think this is an unfortunate state of affairs and it's of course part

17
00:02:37,600 --> 00:02:47,160
of our inability to see things clearly that we have limited ourselves or our solutions

18
00:02:47,160 --> 00:02:57,120
to the problems of the world by our inability to reevaluate our position or to come out

19
00:02:57,120 --> 00:03:10,880
of this narrow-minded and subjective state of affairs. The Buddha said that we can cling

20
00:03:10,880 --> 00:03:19,800
to when we cling to something, it's either through our craving for sensuality for simple

21
00:03:19,800 --> 00:03:25,160
sensations, we want to see beautiful things, we want to hear beautiful sounds, we want

22
00:03:25,160 --> 00:03:32,760
to smell good smells and we want to taste delicious taste, we want to feel pleasant sensations

23
00:03:32,760 --> 00:03:43,480
and we want to have pleasant ideas or pleasant thoughts and this is the first way we can

24
00:03:43,480 --> 00:03:50,120
cling to something. The second way we cling to something is our desire to be something,

25
00:03:50,120 --> 00:03:56,920
our desire to attain some abstract notion wanting to be famous, wanting to be rich, wanting

26
00:03:56,920 --> 00:04:06,760
to be powerful and so on, wanting to be something or to have some state of affairs arise.

27
00:04:06,760 --> 00:04:13,640
The third way is by desiring for some state of affairs of some state to cease, desiring

28
00:04:13,640 --> 00:04:22,560
not to be something, I don't want to be this, I don't want to be that, not wanting to come

29
00:04:22,560 --> 00:04:34,440
into contact with certain states, not wanting to be poor, not wanting to be in this or

30
00:04:34,440 --> 00:04:45,480
that state or have this or that state of affairs to be present. Based on these three

31
00:04:45,480 --> 00:04:53,040
sorts of craving or desire, we come into conflict with others because of course the other

32
00:04:53,040 --> 00:05:00,000
people want the same sorts of things that we want or are also trying to attain the same

33
00:05:00,000 --> 00:05:05,600
sorts of things, we've come to accept that certain sensations, certain experiences are

34
00:05:05,600 --> 00:05:15,440
pleasant, we've come to accept that certain states of being are preferable, being famous,

35
00:05:15,440 --> 00:05:20,720
being rich, being powerful and so on and we've come to accept that certain states of affairs

36
00:05:20,720 --> 00:05:27,760
are inferior, certain ways of being like living in a cave or wearing rags or so on are inferior

37
00:05:27,760 --> 00:05:41,880
and we want to be free from these states. The true way for ourselves to overcome these

38
00:05:41,880 --> 00:05:51,240
is to see through them, to see that the sensations that we have are our experiences

39
00:05:51,240 --> 00:06:00,520
of the world are actually objective, they're neither good or bad and to be able to see

40
00:06:00,520 --> 00:06:06,440
through this partiality that when we see something it's merely seeing, when we hear

41
00:06:06,440 --> 00:06:11,520
something it's merely hearing, when we smell it's merely smelling. We have to actually

42
00:06:11,520 --> 00:06:19,440
overcome this addiction that we have for sensuality, this is the first way that we overcome

43
00:06:19,440 --> 00:06:28,400
these states. The second way, understanding that there is no intrinsic benefit in a specific

44
00:06:28,400 --> 00:06:35,040
state of affairs, coming to understand that being rich has no intrinsic value of being

45
00:06:35,040 --> 00:06:42,480
powerful, being famous, being the boss or being the head of a company or the head of a

46
00:06:42,480 --> 00:06:49,480
department or so on and so on, having lots of clothes, having a nice house, having a car,

47
00:06:49,480 --> 00:06:55,640
a beautiful car, a beautiful family, having lots of children and so on. There's nothing

48
00:06:55,640 --> 00:07:02,080
intrinsically positive or negative about these things, but based on our ability to see

49
00:07:02,080 --> 00:07:09,040
through them we become in tune with reality and we are able to accept things as they

50
00:07:09,040 --> 00:07:15,440
are. So when other people want certain things, we have no desire for those things and

51
00:07:15,440 --> 00:07:22,240
we're able to give them up, we're able to let them go. And this has a lot to do with ego,

52
00:07:22,240 --> 00:07:28,760
the ability to give up our ego, not having to be right, not having to be the winner to

53
00:07:28,760 --> 00:07:34,120
be successful, to be victorious, not having to fight with people. So when someone wants

54
00:07:34,120 --> 00:07:41,880
something, we don't have any need to be seen as the victor or to be something. When we're

55
00:07:41,880 --> 00:07:47,640
able to give it up, no matter if people ridicule us or call us a loser or call us a bomb

56
00:07:47,640 --> 00:07:54,680
or so on. These words really have no meaning, we have no desire for other people's

57
00:07:54,680 --> 00:08:01,360
praise, we have no desire for other people's approval, we have no desire for other people's

58
00:08:01,360 --> 00:08:07,800
envy or so on. We can see that these things have no intrinsic value. This also comes about

59
00:08:07,800 --> 00:08:11,760
through the practice of meditation. When you start to see, when you're able to break

60
00:08:11,760 --> 00:08:18,520
reality up into pieces and into its ultimate components, you can see that there is no meaning

61
00:08:18,520 --> 00:08:25,400
in being rich or famous or powerful or successful or so on. Even the praise of other people

62
00:08:25,400 --> 00:08:32,480
is simply sound coming from our ears and thoughts arising in our mind of how people love

63
00:08:32,480 --> 00:08:42,320
us and esteem us. It has no real lasting effect on our true peace and happiness. It brings

64
00:08:42,320 --> 00:08:52,240
no lasting peace. The same goes with our desire for things not to be wanting to be free

65
00:08:52,240 --> 00:08:59,600
from certain states. When we see that there is nothing intrinsically wrong with certain

66
00:08:59,600 --> 00:09:07,920
states of affairs like living in poverty or living in living as nobody or not having

67
00:09:07,920 --> 00:09:17,000
education or so on, any sort of state of affairs that is undesirable. We come to see that

68
00:09:17,000 --> 00:09:22,800
it's only undesirable because of our expectations, because of our partiality. If we're

69
00:09:22,800 --> 00:09:29,320
impartial, then we're able to live with anything when there are people in our lives that

70
00:09:29,320 --> 00:09:34,680
we'd rather be free from when there are states of affairs when we're sick or so on. We

71
00:09:34,680 --> 00:09:39,640
don't feel disturbed by these states or these people. We're able to live our lives in

72
00:09:39,640 --> 00:09:48,880
peace even with difficult situations or difficult phenomenon, phenomena, things that would

73
00:09:48,880 --> 00:09:54,360
cause other people suffering and difficult things that are accepted by the world to

74
00:09:54,360 --> 00:10:03,320
be negative states. We come to see rather that it is the attachment and the partiality,

75
00:10:03,320 --> 00:10:09,600
the anger and the greed and the delusion and the egoitism and so on that are truly negative

76
00:10:09,600 --> 00:10:15,440
and we desire to get rid of these. Through getting rid of these, there's no competition.

77
00:10:15,440 --> 00:10:21,320
I suppose some might say and I would probably agree to some extent that a certain level

78
00:10:21,320 --> 00:10:27,240
of competition in this regard is useful. Competing to get rid of the defilements. It comes

79
00:10:27,240 --> 00:10:31,840
in with meditators when people are meditating in a group. They find themselves wanting to

80
00:10:31,840 --> 00:10:37,640
necessarily show off, but at least keep up with the group. So you find yourself sitting

81
00:10:37,640 --> 00:10:44,720
straighter and longer and better and easier because of this so-called peer pressure.

82
00:10:44,720 --> 00:10:49,920
There are other people watching you or other people who are going to judge you and so on.

83
00:10:49,920 --> 00:10:54,880
And so to a limited extent, it's helpful, at least in the beginning. I would say in the long

84
00:10:54,880 --> 00:11:05,520
term even that has to be given up and practice should be done alone, specifically because

85
00:11:05,520 --> 00:11:11,680
of these difficulties that when we don't have competition we're lazy and so on. Because

86
00:11:11,680 --> 00:11:16,560
it's that laziness and that inertia that we're trying to overcome and if we're always

87
00:11:16,560 --> 00:11:21,680
reliant on a group, then we'll never have a chance to face these states and to overcome

88
00:11:21,680 --> 00:11:28,960
them even though it may be helpful in the beginning to have support. Eventually one should

89
00:11:28,960 --> 00:11:37,680
be able to go directly inside and not have one's new state of mind be dependent on external

90
00:11:37,680 --> 00:11:46,480
phenomena. As for eradicating these sorts of states in the world, well I guess the

91
00:11:46,480 --> 00:11:55,520
simple explanation is the, by extension, the explanation of two people that are of what is

92
00:11:55,520 --> 00:12:01,520
really valuable because we've come to value things that are useless, that are meaningless

93
00:12:01,520 --> 00:12:08,800
and we've given up our valuing of things that are useful like meditation, introspection,

94
00:12:08,800 --> 00:12:16,160
staying alone and spending time to yourself and so on. We esteem people who are

95
00:12:16,160 --> 00:12:22,880
gregarious, outgoing, who are fun and exciting and so on and people who are introspective

96
00:12:22,880 --> 00:12:32,800
thoughtful and so on are less esteemed by the masses at any rate and so I think this sort of

97
00:12:32,800 --> 00:12:40,720
paradigm shift needs to take place where we become less social and more introspective or at

98
00:12:40,720 --> 00:12:51,840
least more in tune with reality which really does put us as an island where even though we might

99
00:12:51,840 --> 00:12:58,640
be involved with other people, our relationships with other people are conceptual and there's

100
00:12:58,640 --> 00:13:04,000
nothing wrong with that sort of thing but in an ultimate sense we are alone, we were born alone,

101
00:13:04,000 --> 00:13:11,920
we'll die alone, we live our lives alone, our thoughts are our own and so on, to to a great extent

102
00:13:13,120 --> 00:13:19,760
we are solitary beings no matter we might be surrounded by loved ones, people who love us and so on

103
00:13:19,760 --> 00:13:26,080
and still be lonely, still be feel alone if we're not able to understand and to come to terms

104
00:13:26,080 --> 00:13:34,560
with the nature of our own minds and our own reality so I think it's obviously a difficult thing

105
00:13:34,560 --> 00:13:41,520
to do and in any way changing society but even changing society comes from within as you change

106
00:13:41,520 --> 00:13:47,680
yourself you become an example for others and the things that you say and the advice that you give

107
00:13:47,680 --> 00:13:57,440
the impression that you give to others is a great support for them to also find the results

108
00:13:57,440 --> 00:14:02,000
and to gain the sorts of results that you've gained through your practice so I hope that helps

109
00:14:02,000 --> 00:14:22,080
thanks for the question of the rest.

