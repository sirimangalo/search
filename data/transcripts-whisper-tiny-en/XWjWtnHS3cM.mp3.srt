1
00:00:00,000 --> 00:00:04,580
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,580 --> 00:00:10,340
Today we continue on with first number 93, which reads as follows.

3
00:00:10,340 --> 00:00:40,120
In thirdener, the English school is a very common effect laptop, we make mobile

4
00:00:40,120 --> 00:00:44,980
means.

5
00:00:44,980 --> 00:00:51,560
Yasasa wa barikina means one who's asa was, asa was the taints, it's one way of talking

6
00:00:51,560 --> 00:00:53,560
about defilements.

7
00:00:53,560 --> 00:01:06,560
So one who's mental defilements are completely cut off, who doesn't, who isn't dependent

8
00:01:06,560 --> 00:01:10,640
on food, who is independent in regards to food.

9
00:01:10,640 --> 00:01:13,160
Again, this is a verse about food.

10
00:01:13,160 --> 00:01:24,240
Sunyato animito jyawimoko yasago chiro, same as yesterday, for such a person, or for who, it

11
00:01:24,240 --> 00:01:31,600
actually isn't saying what I, what isn't exactly what I said, it's yasago chiro.

12
00:01:31,600 --> 00:01:42,680
So who's, so apart from being unattached to food, they also dwell in emptiness, the liberation

13
00:01:42,680 --> 00:01:46,400
through emptiness and the liberation through sinlessness.

14
00:01:46,400 --> 00:01:56,240
Akas, such a person, Akasay was tukantana, tukuntana, such a person just like a bird in the

15
00:01:56,240 --> 00:01:59,320
sky.

16
00:01:59,320 --> 00:02:03,880
It's hard to know their path, badam tasa dura naya.

17
00:02:03,880 --> 00:02:10,400
So yesterday it was hard to know their, got the, which means their destination, but here

18
00:02:10,400 --> 00:02:14,080
it's badam, which means their path.

19
00:02:14,080 --> 00:02:18,720
So a bird in the sky, you can't see where it's going, there's no sign that it's going

20
00:02:18,720 --> 00:02:25,200
here or going there, or in the where it's gone, right, you can't follow it, can't track

21
00:02:25,200 --> 00:02:26,600
a bird in the sky.

22
00:02:26,600 --> 00:02:32,280
In the same way, you can't track a person who is free, who has attained to freedom from

23
00:02:32,280 --> 00:02:35,640
suffering, attained to liberation.

24
00:02:35,640 --> 00:02:42,640
So that's the story again, that's the verse, again, it's a very short story.

25
00:02:42,640 --> 00:02:50,840
Story here is about Anurudha, that's an interesting story, one that I often think of,

26
00:02:50,840 --> 00:03:02,720
and that the ideal for Anurudha's monk is to search for robes in the garbage.

27
00:03:02,720 --> 00:03:10,320
Now you don't find too much cloth, cotton cloth in the garbage anymore, it's probably

28
00:03:10,320 --> 00:03:20,720
actually pretty difficult in most places, even in Asia, to find cotton cloth or any kind

29
00:03:20,720 --> 00:03:25,200
of cloth that could be useful to make a robe, that anyone's thrown out.

30
00:03:25,200 --> 00:03:29,240
I mean, stained isn't a problem, it's just to find a piece of cloth or enough pieces of

31
00:03:29,240 --> 00:03:35,040
cloth, because unless you're going to make a robe out of plastic bags, our garbage has

32
00:03:35,040 --> 00:03:39,320
changed to the point where it's hard, but this was the thing.

33
00:03:39,320 --> 00:03:45,240
So Anurudha, his robes got worn out and he was wandering around looking for cloth to make

34
00:03:45,240 --> 00:03:47,840
new robes.

35
00:03:47,840 --> 00:03:54,440
And so he was going from one rubbish heap to another wandering through the city, Wailuana,

36
00:03:54,440 --> 00:03:59,400
the city of Rajga.

37
00:03:59,400 --> 00:04:10,040
And it turns out that there was an ex-wife of his, his ex-wife decided that, or found

38
00:04:10,040 --> 00:04:17,800
out about this, and made the determination in her mind to get him some robes.

39
00:04:17,800 --> 00:04:22,760
Now you might ask, how did Anurudha have an ex-wife?

40
00:04:22,760 --> 00:04:24,800
If you don't know much about Anurudha, you might not ask.

41
00:04:24,800 --> 00:04:29,800
But if you know anything about Anurudha, you'll know he ordained when he was quite young.

42
00:04:29,800 --> 00:04:34,440
All the other saki and he's a relative of the Buddha, a cousin, I think.

43
00:04:34,440 --> 00:04:41,320
And all the other saki and princes were, were becoming monks, except for Anurudha and his

44
00:04:41,320 --> 00:04:44,560
older brother, Mahanama.

45
00:04:44,560 --> 00:04:48,960
And some Mahanama came to Anurudha and said, look, everybody else is ordaining.

46
00:04:48,960 --> 00:04:53,720
Every other family in our, in our clan is sending somebody out to ordain.

47
00:04:53,720 --> 00:04:59,200
So our family should have someone as a monk as well, just so we don't, you know, just

48
00:04:59,200 --> 00:05:05,000
to, to, for the glory, or, you know, just for the goodness of our family, Mahanama thought

49
00:05:05,000 --> 00:05:07,720
this was important.

50
00:05:07,720 --> 00:05:15,000
And so he said, so either you or I should become a monk and the other one will stay and

51
00:05:15,000 --> 00:05:23,280
become, will stay and, and become a rich householder and, and a businessman.

52
00:05:23,280 --> 00:05:27,960
And he said, wow, you know, I mean, becoming a monk looks really difficult.

53
00:05:27,960 --> 00:05:33,800
I think I'd prefer to stay Anurudha said, I think I'd prefer to stay as a layperson.

54
00:05:33,800 --> 00:05:38,600
Mahanama said, okay, well, then in that case, I'm going to have to teach you because Anurudha

55
00:05:38,600 --> 00:05:44,840
was really, if you remember the story about the Nati kings, which is a story for another

56
00:05:44,840 --> 00:05:47,280
time, I've already told that story.

57
00:05:47,280 --> 00:05:53,240
But Anurudha was an incredibly naive, he wasn't stupid, but he was totally naive in the

58
00:05:53,240 --> 00:06:02,520
purest sense and he just had no experience, no, he'd never been at any contact with hardship

59
00:06:02,520 --> 00:06:04,440
or, or difficulty.

60
00:06:04,440 --> 00:06:08,880
And so in his mother said, there are no cakes, he, he was confused and he thought she

61
00:06:08,880 --> 00:06:15,680
was saying, all I've got is these Nati cakes, Nati meaning there's, meaning there aren't.

62
00:06:15,680 --> 00:06:19,080
And so he said, well, bring me some Nati cakes.

63
00:06:19,080 --> 00:06:23,640
And so now he was, he thought, well, looking at how difficult it is to be a monk, it doesn't

64
00:06:23,640 --> 00:06:28,040
really look like something I'd be interested in.

65
00:06:28,040 --> 00:06:31,880
And so Mahanama sat him down and started explaining to him what he would have to do.

66
00:06:31,880 --> 00:06:36,080
Well, you know, first you have to gather the farmers together and you have to plant the

67
00:06:36,080 --> 00:06:41,680
rice and then you have to grow the rice and take care of it and guard off pests and, and

68
00:06:41,680 --> 00:06:47,080
so on and water, make sure the irrigation is, is proper and then you have to mill the

69
00:06:47,080 --> 00:06:53,120
grain and then you have to store it and then you have to bring it to the market and sell

70
00:06:53,120 --> 00:06:58,120
it and he went on and then just said, stop, stop, there's no way I'm doing it, he was just

71
00:06:58,120 --> 00:06:59,120
horrified.

72
00:06:59,120 --> 00:07:01,680
He said, there's no way I'm going to do that.

73
00:07:01,680 --> 00:07:08,200
Absolutely becoming a monk is far, far, far, far more.

74
00:07:08,200 --> 00:07:12,120
He couldn't believe the sort of thing and so it's a sort of a refreshing look because

75
00:07:12,120 --> 00:07:16,480
most of us are kind of, most people are kind of resigned to the fact, well, of course

76
00:07:16,480 --> 00:07:21,360
you have to work a nine to five job, but it's nice to have sort of an outside opinion

77
00:07:21,360 --> 00:07:27,080
of someone who hasn't done it and thinking, gee, is why wouldn't I just become a monk?

78
00:07:27,080 --> 00:07:30,720
Kind of refreshing because people think, oh, how difficult it would be to be a monk?

79
00:07:30,720 --> 00:07:35,240
You don't have to work nine to five, that's for sure.

80
00:07:35,240 --> 00:07:39,560
And you don't have to do all the things that I don't have to put up with so many of the

81
00:07:39,560 --> 00:07:46,360
things that they people put up with, of course there are many other challenges, obviously.

82
00:07:46,360 --> 00:07:53,800
But Anurudad, yes, so the point being that he had no wife, he was too young when he

83
00:07:53,800 --> 00:07:54,800
ordained.

84
00:07:54,800 --> 00:08:01,440
I wasn't, I guess, too young, but he had not a wife as far as I know.

85
00:08:01,440 --> 00:08:06,120
But this was the next wife from three lives ago, and I'm not sure how they know that,

86
00:08:06,120 --> 00:08:16,320
but it's been handed down as, how it was, so three lifetimes ago he had a wife.

87
00:08:16,320 --> 00:08:26,160
And she was reborn as a goddess, an angel, jalini, that's what it says.

88
00:08:26,160 --> 00:08:29,760
And she wanted to offer him a robe because he needed the robe and she thought, well, if

89
00:08:29,760 --> 00:08:35,080
I give it to him, he's not going to take it because he's intent upon getting a discarded

90
00:08:35,080 --> 00:08:36,800
robe.

91
00:08:36,800 --> 00:08:47,040
So she made this wonderful angelic robe and placed it underneath a refuse pile with just

92
00:08:47,040 --> 00:08:49,760
the corner sticking out.

93
00:08:49,760 --> 00:08:53,280
Anurudad was coming along and he came up to this pile and he saw the corner and he pulled

94
00:08:53,280 --> 00:09:00,080
it out and he thought, wow, what a, what a, he said, this is indeed a most remarkable

95
00:09:00,080 --> 00:09:10,160
refuse heap, and so he took the, I guess there were more than one, he took the robes out

96
00:09:10,160 --> 00:09:11,160
or the cloths out.

97
00:09:11,160 --> 00:09:15,760
It was just cloth, actually, and they would start with cloth and they would sew the robes

98
00:09:15,760 --> 00:09:17,320
themselves.

99
00:09:17,320 --> 00:09:21,280
And so that's when he did, he brought it back to the monastery and all the other monks

100
00:09:21,280 --> 00:09:22,280
helped.

101
00:09:22,280 --> 00:09:26,800
So they got together in the circle and they, it says that everybody got involved with

102
00:09:26,800 --> 00:09:34,560
making him a robe, Anurudad was just so well loved that they all got together and 500 monks

103
00:09:34,560 --> 00:09:41,040
with the Buddha as the head, at the head and the 80 sheep disciples came as well, all for

104
00:09:41,040 --> 00:09:51,040
the purpose of, no, not all, they didn't all get together, but they were all there.

105
00:09:51,040 --> 00:09:57,680
And the Mahakasabha was at the foot of the robe, I'll, I'll, sorry, put those in the middle

106
00:09:57,680 --> 00:10:00,160
and Anurudad was at the head.

107
00:10:00,160 --> 00:10:06,920
So these three powerhouse monks were involved, all right, and yes, 500 monks along with

108
00:10:06,920 --> 00:10:12,280
the 80 great disciples got involved, spinning out thread to sew with, right, they actually

109
00:10:12,280 --> 00:10:18,840
had to take old robes, I guess, and if I are take some kind of cotton resources to make

110
00:10:18,840 --> 00:10:27,160
thread out of, and the Buddha threaded the needle, Mahamogalana went and got supplies for

111
00:10:27,160 --> 00:10:33,080
them because he had magical powers, anyway, this is the story, it was a big to do, everyone

112
00:10:33,080 --> 00:10:39,240
got involved with making these, this set of robes up for Anurudad, which is an incredible

113
00:10:39,240 --> 00:10:49,960
image, you know, of this, this harmony and cooperation with all these enlightened beings.

114
00:10:49,960 --> 00:10:56,360
And the ex-wife Angel went off into the city and got everyone to bring food, stinking

115
00:10:56,360 --> 00:11:02,560
all these monks and they have to stay together and they have to be busy making the robes,

116
00:11:02,560 --> 00:11:08,960
so being good if everyone were to come, so she went in disguised as a human being and got

117
00:11:08,960 --> 00:11:14,600
everyone to bring arms to the monastery.

118
00:11:14,600 --> 00:11:19,880
But the point of this story and how it relates to our verse is that a problem arose in

119
00:11:19,880 --> 00:11:24,440
that there was too much food, which is often what happens with these big celebrations even

120
00:11:24,440 --> 00:11:29,280
in modern times, people get so excited that they bring lots and lots of food and they really

121
00:11:29,280 --> 00:11:32,920
want to do good deeds and so it's a great thing.

122
00:11:32,920 --> 00:11:40,080
But then what inevitably happens is someone complains and that's what happened here.

123
00:11:40,080 --> 00:11:45,760
Turns out there was a great heap of food remaining over after everybody had eaten.

124
00:11:45,760 --> 00:11:54,680
It was just even with 500 monks and 580 monks, just too much food because everyone was

125
00:11:54,680 --> 00:11:59,880
so keen to help hearing that this would a great opportunity to help this auspicious

126
00:11:59,880 --> 00:12:04,320
occasion. I mean that maybe it doesn't sound that auspicious, but it's a big deal when

127
00:12:04,320 --> 00:12:10,440
a monk gets a new robe and really you're clothing and enlightened being and so on.

128
00:12:10,440 --> 00:12:18,160
Not just any monk gets out of ruda, he was one of the 80 great disciples of the Buddha.

129
00:12:18,160 --> 00:12:25,240
So there was all this food that was just rotten, it went just went bad and some monks got

130
00:12:25,240 --> 00:12:32,120
offended and they looked at it and they thought, you know, this must be on ruda's doing.

131
00:12:32,120 --> 00:12:36,840
And they say he must have gone to his kinsmen and told them, hey, bring some food, bring

132
00:12:36,840 --> 00:12:44,160
such so much food and for everyone then they said, you know, clearly he didn't think

133
00:12:44,160 --> 00:12:53,800
beforehand and he wasted all this food and it took advantage of people's good will.

134
00:12:53,800 --> 00:12:59,600
It's amazing how people, it's the same as yesterday they get so involved with other people's

135
00:12:59,600 --> 00:13:05,280
affairs and want to criticize and attack them.

136
00:13:05,280 --> 00:13:08,400
And then so the teacher found out about it and he called them over and asked them what

137
00:13:08,400 --> 00:13:13,760
he was talking about and they told them and he said, look, do you have any evidence that

138
00:13:13,760 --> 00:13:24,400
Anruada did it and they said, yes, he actually said, yes, we do.

139
00:13:24,400 --> 00:13:29,360
He said, do you really think that the Anruada ordered this and they said, yes, of course

140
00:13:29,360 --> 00:13:35,000
we think that and the Buddha scolded them and said, there's no way Anruada could do that.

141
00:13:35,000 --> 00:13:40,560
There's no way Anruada would be so attached and I guess the implication is that he has

142
00:13:40,560 --> 00:13:48,360
some attachment to food, so he was greedy, you know, he wanted special food for all the monks

143
00:13:48,360 --> 00:13:54,160
or he wanted special food for himself and he didn't understand what was enough, he didn't

144
00:13:54,160 --> 00:13:59,960
know what was enough food, his greed overtook him obviously because there's no way he

145
00:13:59,960 --> 00:14:06,280
couldn't miscalculate it, he must have just gotten so greedy that he wanted extra food

146
00:14:06,280 --> 00:14:09,960
but, you know, the truth is he had nothing to do with it, it was to lay people who were just

147
00:14:09,960 --> 00:14:14,440
overzealous and this angel who went out of her way, he didn't even ask, of course, this

148
00:14:14,440 --> 00:14:19,080
angel to go and tell people to bring arms, probably would have had everyone go out and

149
00:14:19,080 --> 00:14:23,600
get arms themselves but when the food came so they ate what they couldn't and the rest

150
00:14:23,600 --> 00:14:31,560
while you can't blame them, they didn't ask for it.

151
00:14:31,560 --> 00:14:35,840
So then the Buddha taught this verse, he said, it couldn't have been Anruada would never

152
00:14:35,840 --> 00:14:41,520
have thought such a thing, he had no such greed, no such attachment and the implication

153
00:14:41,520 --> 00:14:47,040
is that he has rid himself of the ass of us, he has no, he has independent in regards

154
00:14:47,040 --> 00:14:57,840
to food, he dwells in selflessness and sinlessness and as a result and this is an

155
00:14:57,840 --> 00:15:01,080
interesting, it's interesting how it relates here because it says Padang Dasa and

156
00:15:01,080 --> 00:15:07,200
doranayang which means his track, when here it's interesting because this could refer

157
00:15:07,200 --> 00:15:11,640
to his state of mind, so these people are trying to think, you know, Anruada was like

158
00:15:11,640 --> 00:15:17,320
this Anruada is thinking this, this is what Anruada is like and he said, you guys have

159
00:15:17,320 --> 00:15:24,320
no clue, you're not able to tell Anruada's train of thought, this is the Padang, Padam

160
00:15:24,320 --> 00:15:31,680
means his track, just as you couldn't, if you looked at a bird, if you looked at the sky

161
00:15:31,680 --> 00:15:35,480
where a bird had traveled you wouldn't see which way the bird went, in the same way

162
00:15:35,480 --> 00:15:40,360
you don't know which way Anruada is going in his mind, you can't know Anruada's mind,

163
00:15:40,360 --> 00:15:46,920
it's very difficult and new people who are set upon criticism, yes, how could you

164
00:15:46,920 --> 00:15:52,520
know, he doesn't say that because he's not being critical of them, he's just chastising

165
00:15:52,520 --> 00:15:56,640
them and saying, how could you say that about my son?

166
00:15:56,640 --> 00:16:05,960
The Buddha would use the word my son for monks who were enlightened monks because it wasn't

167
00:16:05,960 --> 00:16:10,640
dependent on how long they'd been a monk or so on, he considered that anyone who became

168
00:16:10,640 --> 00:16:14,840
enlightened was his son, through his teaching, if they became enlightened he would call

169
00:16:14,840 --> 00:16:23,760
them his son, so he called that mamaputa, mamaputo, my son.

170
00:16:23,760 --> 00:16:27,160
So the verse is quite similar to yesterday, there's not that much more to talk about,

171
00:16:27,160 --> 00:16:32,440
we've talked about food, not being dependent on food, which is, you know, in the sense

172
00:16:32,440 --> 00:16:36,760
of being able to even go without it, if no food comes you sometimes won't eat and that

173
00:16:36,760 --> 00:16:40,440
happens, I mean it may have been they would just decide what were too busy, we won't even

174
00:16:40,440 --> 00:16:47,840
go and get food, so today we'll just all go without food, they might even do that, they

175
00:16:47,840 --> 00:16:52,800
might not, but as it often happens there's no need because lay people are in touch with

176
00:16:52,800 --> 00:16:56,560
the monks and when they find out and everybody wants to get involved, they think, oh well

177
00:16:56,560 --> 00:17:00,640
we'll bring food to the monastery and we've got, we can do that, we can support in that

178
00:17:00,640 --> 00:17:08,200
way, and they would do that, but we have the first part that's a little bit different

179
00:17:08,200 --> 00:17:16,120
right, this is not that much different, what was the first one was Sanitayo, who has no

180
00:17:16,120 --> 00:17:20,520
hoarding, so yeah it's quite different, this one is talking about one who has ridden oneself

181
00:17:20,520 --> 00:17:29,000
of the Asawa, Asawa is an interesting word, it has to do with the sawana stream, so Asawa

182
00:17:29,000 --> 00:17:36,520
is like the outpourings, you know, our mind is contained, but it leaks, so Asawa could

183
00:17:36,520 --> 00:17:43,480
be translated as the leaks, our mind leaks out, it leaks out because it's not contained,

184
00:17:43,480 --> 00:17:53,200
our mind is not stable, our mind is not calm, so it's like there's an overflowing, there's

185
00:17:53,200 --> 00:18:00,840
a rippling of waves and as there's an outpouring, this is, it's kind of a symbolic way

186
00:18:00,840 --> 00:18:10,920
or a metaphoric way, a mix of talking about the defilements, or it's an idiomatic usage

187
00:18:10,920 --> 00:18:19,520
of the word Asawa, but used by the Buddha, refers to, you know, meaning is that, as I said,

188
00:18:19,520 --> 00:18:25,520
defilements aren't outflowing, and when you cut those off, your mind is contained, that's

189
00:18:25,520 --> 00:18:31,360
the implication, so it means defilements, and by defilements, of course, we mean anything

190
00:18:31,360 --> 00:18:35,920
based on greed, based on anger, or based on delusion.

191
00:18:35,920 --> 00:18:41,760
So again, it's similar to last week, I'm not sure too much to say, this is obviously the

192
00:18:41,760 --> 00:18:46,200
sort of thing that, except that I've realized that the grammar wasn't exact what I was

193
00:18:46,200 --> 00:18:55,120
saying yesterday, how I thought that one who doesn't rely upon food is as a result,

194
00:18:55,120 --> 00:19:00,160
that in the two kinds of liberation, it's more like, not only do you have to be free

195
00:19:00,160 --> 00:19:11,280
from, or in addition to being free from reliance or dependence on food, such a person who

196
00:19:11,280 --> 00:19:19,920
on top of that is also, or maybe you could say, in line with that, is also dedicated

197
00:19:19,920 --> 00:19:29,520
to the two kinds of liberation, liberation through emptiness and liberation through sinlessness,

198
00:19:29,520 --> 00:19:35,760
which means they see it non-self, when they see non-self, then that adds to their, the

199
00:19:35,760 --> 00:19:41,160
likelihood that they're not going to cling to food, you know, the idea that I want to feed

200
00:19:41,160 --> 00:19:47,760
my body or worry about or concern about the body, when they see impermanence, they will

201
00:19:47,760 --> 00:19:54,360
not be worried about, or concerned about change, you know, not getting food, won't bother

202
00:19:54,360 --> 00:19:59,520
them, you know, the pain or the hunger of going without food isn't a deal because they're

203
00:19:59,520 --> 00:20:06,320
comfortable with experience as it is, change doesn't bother them, but not dependent on one

204
00:20:06,320 --> 00:20:08,280
condition.

205
00:20:08,280 --> 00:20:13,200
So in our meditation, we're going for a lot of these things, someone asked me today

206
00:20:13,200 --> 00:20:19,360
about how you give up your dependence on food and really it's like all other answers,

207
00:20:19,360 --> 00:20:24,960
all other questions the answer is still meditation, when you understand rather than trying

208
00:20:24,960 --> 00:20:33,000
to control, rather than trying to force yourself or feel guilty about or get angry at

209
00:20:33,000 --> 00:20:38,840
yourself, when you want to eat junk food or when you want to eat too much or that kind

210
00:20:38,840 --> 00:20:44,560
of thing, you have to change to study it, if it's wrong, well, then I should know it's

211
00:20:44,560 --> 00:20:46,160
wrong.

212
00:20:46,160 --> 00:20:51,560
If I really knew that it was wrong, then I wouldn't, for any reason, indulge in it.

213
00:20:51,560 --> 00:20:56,720
So we have to study, it means that we don't know enough yet, we're not clear in our minds

214
00:20:56,720 --> 00:21:03,640
enough, so we still like and want, when you study, then you see Sunyatta and you see Animita.

215
00:21:03,640 --> 00:21:09,560
And Sunyatta means it's not, it's void of any substance, it's not really a substantial

216
00:21:09,560 --> 00:21:10,560
happiness.

217
00:21:10,560 --> 00:21:16,960
Animita means it's not a stable happiness, so it can change without sign, without signal

218
00:21:16,960 --> 00:21:20,400
or warning.

219
00:21:20,400 --> 00:21:25,320
When you see these things and you give up your attachment, you'll see why really it is

220
00:21:25,320 --> 00:21:33,400
bad, useless, harmful, and then you'll stop, just naturally.

221
00:21:33,400 --> 00:21:39,320
So that's the demo pad of her today, and we'll stop there, thank you for tuning in, keep

222
00:21:39,320 --> 00:22:03,320
practicing, and keep up.

