1
00:00:00,000 --> 00:00:07,000
Many people interpreted that to me some at a time, he was alive, right?

2
00:00:07,000 --> 00:00:20,000
Because stopping the explained to me, keeping the mind still on the object.

3
00:00:20,000 --> 00:00:28,000
And saying the characteristics of saying the individual as a sense of things.

4
00:00:28,000 --> 00:00:38,000
If it means well-impition, then...

5
00:00:38,000 --> 00:00:45,000
We pass on again, also we call well-impition.

6
00:00:45,000 --> 00:00:52,000
Then we have lost some attack in this case.

7
00:00:52,000 --> 00:00:58,000
Because this last tetra deals with some...

8
00:00:58,000 --> 00:01:02,000
We pass on that only, fewer we pass on that, not some at a time.

9
00:01:02,000 --> 00:01:10,000
So the four tetra's, the first one deals with what?

10
00:01:10,000 --> 00:01:12,000
Some at a meditation.

11
00:01:12,000 --> 00:01:18,000
Because the commentary explains the full attaining of Janas.

12
00:01:18,000 --> 00:01:23,000
And then after only after the attaining Janas, one can change to we pass on that.

13
00:01:23,000 --> 00:01:33,000
And then the second and the third deal with what?

14
00:01:33,000 --> 00:01:37,000
Both some at a and we pass on that.

15
00:01:37,000 --> 00:01:40,000
Because they put into Janas...

16
00:01:40,000 --> 00:01:42,000
Janas.

17
00:01:42,000 --> 00:01:43,000
Yeah.

18
00:01:43,000 --> 00:01:46,000
And the fifth only we pass on that.

19
00:01:46,000 --> 00:01:48,000
So there is no Janas.

20
00:01:48,000 --> 00:01:53,000
I mean in the fourth, there is no Janas here.

21
00:01:53,000 --> 00:01:57,000
So the fourth deals only with we pass on that.

22
00:01:57,000 --> 00:02:00,000
And this fourth deals with what?

23
00:02:00,000 --> 00:02:05,000
The fourth of the foundations of mindfulness.

24
00:02:05,000 --> 00:02:07,000
Damma nupasana.

25
00:02:07,000 --> 00:02:11,000
So fourth tetra's the first deals with the contemplation on bodies,

26
00:02:11,000 --> 00:02:15,000
second on feeling, but on consciousness, and the fourth on damma.

27
00:02:15,000 --> 00:02:18,000
And there is four deals with pure insight.

28
00:02:18,000 --> 00:02:19,000
We pass on that only.

29
00:02:19,000 --> 00:02:26,000
But the other three deal with some at a and, and both some at an and we pass on that.

30
00:02:26,000 --> 00:02:30,000
But it's not necessary to go to Janas.

31
00:02:30,000 --> 00:02:31,000
Or that's right.

32
00:02:31,000 --> 00:02:32,000
Yeah.

33
00:02:32,000 --> 00:02:35,000
It is not necessary.

34
00:02:35,000 --> 00:02:50,000
And many modern modern orders tend to have to go deemphasize Janas.

35
00:02:50,000 --> 00:02:56,000
And one other say that since Janas are not mentioned in this order,

36
00:02:56,000 --> 00:03:00,000
they are not important.

37
00:03:00,000 --> 00:03:05,000
But when we study is so that then we have to,

38
00:03:05,000 --> 00:03:07,000
we have to follow the tradition.

39
00:03:07,000 --> 00:03:10,000
So tradition or the, the commentaries,

40
00:03:10,000 --> 00:03:18,000
explain these, these four tetra's as, and dealing with some at an and we pass on that.

41
00:03:18,000 --> 00:03:19,000
Yes.

42
00:03:19,000 --> 00:03:22,000
And Janas and we pass on that.

43
00:03:22,000 --> 00:03:27,000
And four Janas are mentioned in the Mahasatipatana.

44
00:03:27,000 --> 00:03:32,000
Although they are not mentioned in the Ana-banasatisota.

45
00:03:32,000 --> 00:03:42,000
So there are two soldiers in, in the whole of Paulicana that deal with breathing

46
00:03:42,000 --> 00:03:46,000
meditation and other mindfulness meditation.

47
00:03:46,000 --> 00:03:52,000
So in the Ana-banasatisota, no Janas are mentioned.

48
00:03:52,000 --> 00:03:57,000
But the, these passages are from that soldier.

49
00:03:57,000 --> 00:04:07,000
And so these passages are explained with reference to Janas and Vipasana in the ancient commentaries.

50
00:04:07,000 --> 00:04:14,000
So you cannot see that you do not find Janas in the, in these two soldiers.

51
00:04:14,000 --> 00:04:21,000
So in the Ana-banasatisota, Janas are not mentioned explicitly.

52
00:04:21,000 --> 00:04:23,000
But they are implied.

53
00:04:23,000 --> 00:04:30,000
But in the Mahasatipatana-banasota, Janas are mentioned when,

54
00:04:30,000 --> 00:04:38,000
when Buddha defined the noble hateful path.

55
00:04:38,000 --> 00:04:44,000
So we cannot say that Janas are put into the canal later.

56
00:04:44,000 --> 00:04:49,000
We have no, no proof of that.

57
00:04:49,000 --> 00:04:54,000
We may, we may carelessly say that, or anything,

58
00:04:54,000 --> 00:04:56,000
we don't like this.

59
00:04:56,000 --> 00:04:58,000
It's an interpolation.

60
00:04:58,000 --> 00:05:01,000
It's added by monks or something like that.

61
00:05:01,000 --> 00:05:06,000
But there can be no, for the call.

62
00:05:06,000 --> 00:05:18,000
No definite proof that a portion of the soldier is put into it later.

63
00:05:18,000 --> 00:05:29,000
So it is important to study such soldiers with the help of the ancient commentaries.

64
00:05:29,000 --> 00:05:35,000
And it's safe to follow them rather than going away from them.

65
00:05:35,000 --> 00:05:45,000
And, and interpreting in a way one, one, one lives and not following the,

66
00:05:45,000 --> 00:05:52,000
the ancient tradition.

67
00:05:52,000 --> 00:05:59,000
Now conclusion.

68
00:05:59,000 --> 00:06:14,000
So in the footnotes, on page 2015, through substitution of opposite qualities,

69
00:06:14,000 --> 00:06:15,000
and so on.

70
00:06:15,000 --> 00:06:24,000
There is some, some explanation about the abandonment or the,

71
00:06:24,000 --> 00:06:27,000
the relinquishment of giving up.

72
00:06:27,000 --> 00:06:30,000
Now, about the middle of the footnotes.

73
00:06:30,000 --> 00:06:35,000
And the giving up in this way is in the form of inducing non-occurrence.

74
00:06:35,000 --> 00:06:39,000
Now, when we say that, let us say,

75
00:06:39,000 --> 00:06:44,000
by the touch, maga, eradication, departments.

76
00:06:44,000 --> 00:06:50,000
We do not mean that it, it really eradicates department.

77
00:06:50,000 --> 00:06:57,000
What it meant is, it doesn't let the, the, the, the defalments to come up again.

78
00:06:57,000 --> 00:07:00,000
So non-occurrence.

79
00:07:00,000 --> 00:07:07,000
Because, the present departments cannot be described because they are already in our minds.

80
00:07:07,000 --> 00:07:10,000
And the past are already present.

81
00:07:10,000 --> 00:07:12,000
We don't have to do anything about them.

82
00:07:12,000 --> 00:07:13,000
They already call.

83
00:07:13,000 --> 00:07:16,000
And the future things are not yet come.

84
00:07:16,000 --> 00:07:21,000
So what the, the, the maga or the path, the eradicates is not the,

85
00:07:21,000 --> 00:07:27,000
the present or not the past or not the future defalments.

86
00:07:27,000 --> 00:07:32,000
But, the, the eradication of the maga,

87
00:07:32,000 --> 00:07:37,000
render them, what do you call that?

88
00:07:37,000 --> 00:07:39,000
Operative or not arising.

89
00:07:39,000 --> 00:07:43,000
So they, they will not arise again.

90
00:07:43,000 --> 00:07:49,000
It is like just trying to be potential of things.

91
00:07:49,000 --> 00:07:52,000
So it talks about it here.

92
00:07:52,000 --> 00:07:56,000
And this, this subject will be picked up later about,

93
00:07:56,000 --> 00:08:02,000
towards the end of the book.

94
00:08:02,000 --> 00:08:07,000
And, and paragraph 29.

95
00:08:07,000 --> 00:08:10,000
Second line, we, we have the,

96
00:08:10,000 --> 00:08:13,000
the words clear vision and deliverance.

97
00:08:13,000 --> 00:08:18,000
Here clear vision means fact maga.

98
00:08:18,000 --> 00:08:22,000
And deliverance means fruition.

99
00:08:22,000 --> 00:08:33,000
Hola.

100
00:08:33,000 --> 00:08:35,000
Okay then.

101
00:08:35,000 --> 00:08:40,000
And then this, the benefits of the,

102
00:08:40,000 --> 00:08:43,000
this kind of meditation, they are, they are also,

103
00:08:43,000 --> 00:08:45,000
all of them come from the same,

104
00:08:45,000 --> 00:08:47,000
so the anapanasity, so then.

105
00:08:47,000 --> 00:08:50,000
And then the story of how a month,

106
00:08:50,000 --> 00:08:55,000
who was, who was an arahat and who practiced anap,

107
00:08:55,000 --> 00:08:59,000
mindfulness of breathing meditation and who became an arahat.

108
00:08:59,000 --> 00:09:02,000
New, how he knew when he, when,

109
00:09:02,000 --> 00:09:04,000
thank you, when he would die.

110
00:09:04,000 --> 00:09:08,000
And then he asked his companions,

111
00:09:08,000 --> 00:09:11,000
how they, they had seen,

112
00:09:11,000 --> 00:09:14,000
an arahat and they knew bhana.

113
00:09:14,000 --> 00:09:17,000
And some say, in a sitting position.

114
00:09:17,000 --> 00:09:20,000
And he said, I will show you,

115
00:09:20,000 --> 00:09:22,000
he, he, he was going to die.

116
00:09:22,000 --> 00:09:25,000
He, he knew, he knew the very moment.

117
00:09:25,000 --> 00:09:29,000
And he, he was going to die.

118
00:09:29,000 --> 00:09:31,000
And so I will die,

119
00:09:31,000 --> 00:09:34,000
I will end a nay bhana walking.

120
00:09:34,000 --> 00:09:38,000
So he, he, he made a line in the,

121
00:09:38,000 --> 00:09:40,000
and he had to go.

122
00:09:40,000 --> 00:09:44,000
Maybe ambulatory, right?

123
00:09:44,000 --> 00:09:47,000
And then he said, I will go to the end of this,

124
00:09:47,000 --> 00:09:48,000
this space.

125
00:09:48,000 --> 00:09:51,000
And then when I come back and cross the line,

126
00:09:51,000 --> 00:09:53,000
I will die then.

127
00:09:53,000 --> 00:09:58,000
And he did, he did die as he said.

128
00:09:58,000 --> 00:10:03,000
So those who practiced anapanasity meditation

129
00:10:03,000 --> 00:10:07,000
can even know when their life falls

130
00:10:07,000 --> 00:10:09,000
will be stopped.

131
00:10:09,000 --> 00:10:12,000
When, when they are going to die.

132
00:10:12,000 --> 00:10:21,000
So this is also the, the benefits of anapanasity meditation.

133
00:10:21,000 --> 00:10:24,000
And in paragraph 2 and 41,

134
00:10:24,000 --> 00:10:27,000
there is the,

135
00:10:27,000 --> 00:10:32,000
the explanation of three kinds of finals,

136
00:10:32,000 --> 00:10:35,000
the final in becoming final in jhana and final in death.

137
00:10:35,000 --> 00:10:37,000
And with regard to final in death,

138
00:10:37,000 --> 00:10:39,000
there is a saying that,

139
00:10:39,000 --> 00:10:44,000
that is why they are a final in jhana.

140
00:10:44,000 --> 00:10:47,000
Those that arise along with the 16th consciousness,

141
00:10:47,000 --> 00:10:49,000
preceding the death consciousness,

142
00:10:49,000 --> 00:10:53,000
cease together with the death consciousness.

143
00:10:53,000 --> 00:10:55,000
Now,

144
00:10:55,000 --> 00:10:56,000
we,

145
00:10:56,000 --> 00:10:57,000
there are,

146
00:10:57,000 --> 00:11:00,000
there are four kinds of material properties,

147
00:11:00,000 --> 00:11:02,000
four kinds of matter.

148
00:11:02,000 --> 00:11:05,000
Those caused by gamma,

149
00:11:05,000 --> 00:11:08,000
those caused by Chita or mine,

150
00:11:08,000 --> 00:11:12,000
those caused by climate and those caused by food.

151
00:11:12,000 --> 00:11:16,000
Those caused by gamma must,

152
00:11:16,000 --> 00:11:23,000
must disappear at the same moment

153
00:11:23,000 --> 00:11:26,000
as the death consciousness.

154
00:11:26,000 --> 00:11:28,000
So at the last,

155
00:11:28,000 --> 00:11:30,000
last have movement of death consciousness,

156
00:11:30,000 --> 00:11:32,000
they must also disappear.

157
00:11:32,000 --> 00:11:40,000
Because the, the material properties caused by mine,

158
00:11:40,000 --> 00:11:44,000
cannot,

159
00:11:44,000 --> 00:11:49,000
cannot live after the moment of death.

160
00:11:49,000 --> 00:11:52,000
So,

161
00:11:52,000 --> 00:11:54,000
the,

162
00:11:54,000 --> 00:11:57,000
what we call life,

163
00:11:57,000 --> 00:12:04,000
or what is translated as title principle of title line,

164
00:12:04,000 --> 00:12:07,000
means that gamma,

165
00:12:07,000 --> 00:12:14,000
that gamma born quality in the material properties.

166
00:12:14,000 --> 00:12:18,000
So that gamma born quality in the material properties must,

167
00:12:18,000 --> 00:12:20,000
time with the death consciousness,

168
00:12:20,000 --> 00:12:23,000
must disappear with the death consciousness.

169
00:12:23,000 --> 00:12:29,000
Now, the life of a material property is 17 times

170
00:12:29,000 --> 00:12:33,000
that of the life of consciousness.

171
00:12:33,000 --> 00:12:37,000
Consciousness, last for only one big movement,

172
00:12:37,000 --> 00:12:41,000
or three sub movements, more movements.

173
00:12:41,000 --> 00:12:44,000
So,

174
00:12:44,000 --> 00:12:47,000
the material property,

175
00:12:47,000 --> 00:12:50,000
last for 17 big moments,

176
00:12:50,000 --> 00:12:53,000
several 17 big four moments.

177
00:12:53,000 --> 00:12:55,000
Since,

178
00:12:55,000 --> 00:12:58,000
the viral principle must,

179
00:12:58,000 --> 00:13:05,000
the soul or disappear at the last sub movement of death consciousness,

180
00:13:05,000 --> 00:13:07,000
it must have arisen

181
00:13:07,000 --> 00:13:10,000
how many moments before,

182
00:13:10,000 --> 00:13:12,000
16 moments before.

183
00:13:12,000 --> 00:13:15,000
So 16 plus death moments, 17 moments.

184
00:13:15,000 --> 00:13:18,000
So at the end of 17th moment,

185
00:13:18,000 --> 00:13:20,000
and they must,

186
00:13:20,000 --> 00:13:22,000
the life principle must disappear.

187
00:13:22,000 --> 00:13:24,000
That is why,

188
00:13:24,000 --> 00:13:26,000
the pre-CD,

189
00:13:26,000 --> 00:13:31,000
16 consciousness preceding the death consciousness.

190
00:13:31,000 --> 00:13:34,000
If you want to read more about it,

191
00:13:34,000 --> 00:13:35,000
you can read,

192
00:13:35,000 --> 00:13:38,000
in the sixth chapter of,

193
00:13:38,000 --> 00:13:42,000
the manual of a beta.

194
00:13:42,000 --> 00:13:43,000
When,

195
00:13:43,000 --> 00:13:47,000
when different material properties,

196
00:13:47,000 --> 00:13:50,000
finally,

197
00:13:50,000 --> 00:13:52,000
finally, disappear in your life.

198
00:13:52,000 --> 00:13:59,000
So now,

199
00:13:59,000 --> 00:14:02,000
we go to,

200
00:14:02,000 --> 00:14:09,000
recollection of peace.

201
00:14:09,000 --> 00:14:11,000
recollection of peace,

202
00:14:11,000 --> 00:14:12,000
really means,

203
00:14:12,000 --> 00:14:14,000
recollection of Nirvana.

204
00:14:14,000 --> 00:14:16,000
So one who wants to develop

205
00:14:16,000 --> 00:14:17,000
recollection of peace,

206
00:14:17,000 --> 00:14:19,000
mentioned next to mindfulness of breathing,

207
00:14:19,000 --> 00:14:21,000
should go into solitary retreat

208
00:14:21,000 --> 00:14:23,000
and regulate these special qualities of Nirvana.

209
00:14:23,000 --> 00:14:24,000
In other words,

210
00:14:24,000 --> 00:14:26,000
the stealing of all suffering

211
00:14:26,000 --> 00:14:28,000
has followed.

212
00:14:28,000 --> 00:14:29,000
Because in,

213
00:14:29,000 --> 00:14:30,000
in,

214
00:14:30,000 --> 00:14:31,000
so far as there are demos,

215
00:14:31,000 --> 00:14:33,000
whether formed or unformed,

216
00:14:33,000 --> 00:14:34,000
that means,

217
00:14:34,000 --> 00:14:35,000
conditioned or unconditioned,

218
00:14:35,000 --> 00:14:37,000
feeding away,

219
00:14:37,000 --> 00:14:39,000
is pronounced the best of them.

220
00:14:39,000 --> 00:14:41,000
So feeding away the best of them.

221
00:14:41,000 --> 00:14:42,000
That is to say,

222
00:14:42,000 --> 00:14:43,000
the,

223
00:14:43,000 --> 00:14:44,000
the disillusionment

224
00:14:44,000 --> 00:14:45,000
of energy,

225
00:14:45,000 --> 00:14:46,000
the elimination of thirst,

226
00:14:46,000 --> 00:14:47,000
the evolution of,

227
00:14:47,000 --> 00:14:48,000
the reliance,

228
00:14:48,000 --> 00:14:50,000
the termination of the realm,

229
00:14:50,000 --> 00:15:13,000
the destruction of

230
00:15:13,000 --> 00:15:16,000
consciousness.

231
00:15:16,000 --> 00:15:19,000
So don't give me headache.

232
00:15:19,000 --> 00:15:20,000
I have to,

233
00:15:20,000 --> 00:15:21,000
first,

234
00:15:21,000 --> 00:15:22,000
I have to find the passages in the,

235
00:15:22,000 --> 00:15:23,000
in the commentaries,

236
00:15:23,000 --> 00:15:25,000
and then

237
00:15:25,000 --> 00:15:27,000
take with them the translation.

238
00:15:27,000 --> 00:15:28,000
And then,

239
00:15:28,000 --> 00:15:31,000
sometimes it's very difficult to translate them.

240
00:15:31,000 --> 00:15:38,000
It's the way they are written.

241
00:15:38,000 --> 00:15:40,000
And one problem is,

242
00:15:40,000 --> 00:15:43,000
it's not my, not my mother tongue.

243
00:15:43,000 --> 00:15:45,000
So,

244
00:15:45,000 --> 00:15:47,000
it's, it's not so easy.

245
00:15:47,000 --> 00:15:48,000
Now,

246
00:15:48,000 --> 00:15:51,000
yeah.

247
00:15:51,000 --> 00:15:54,000
Dhamma,

248
00:15:54,000 --> 00:15:57,000
Dhamma comes from

249
00:15:57,000 --> 00:15:59,000
the root,

250
00:15:59,000 --> 00:16:01,000
the H-A-R,

251
00:16:01,000 --> 00:16:04,000
that means to hold.

252
00:16:04,000 --> 00:16:06,000
So,

253
00:16:06,000 --> 00:16:11,000
Dhamma,

254
00:16:11,000 --> 00:16:14,000
in such passages as Dhammas,

255
00:16:14,000 --> 00:16:16,000
that are concepts,

256
00:16:16,000 --> 00:16:21,000
even in non entity is the cause of Dhamma.

257
00:16:21,000 --> 00:16:24,000
The word Dhamma can mean anything in the world,

258
00:16:24,000 --> 00:16:27,000
including Nevada.

259
00:16:27,000 --> 00:16:32,000
Even the concepts are called Dhammas.

260
00:16:32,000 --> 00:16:35,000
Why are they called Dhammas?

261
00:16:35,000 --> 00:16:42,000
The D-D-R,

262
00:16:42,000 --> 00:16:45,000
what do you call?

263
00:16:45,000 --> 00:16:48,000
They are carried

264
00:16:48,000 --> 00:16:51,000
or they are

265
00:16:51,000 --> 00:16:54,000
hails, something like that.

266
00:16:54,000 --> 00:16:55,000
Dhamma,

267
00:16:55,000 --> 00:16:56,000
that kind of Dhamma,

268
00:16:56,000 --> 00:16:58,000
is excluded by his saying,

269
00:16:58,000 --> 00:17:00,000
Dhammas means individual essence.

270
00:17:00,000 --> 00:17:01,000
So,

271
00:17:01,000 --> 00:17:05,000
when it is said that Dhamma means individual essence,

272
00:17:05,000 --> 00:17:08,000
then the other,

273
00:17:08,000 --> 00:17:15,000
one wanted to exclude concepts from being called Dhamma here.

274
00:17:15,000 --> 00:17:20,000
So, here Dhamma means those that have individual essence

275
00:17:20,000 --> 00:17:23,000
and not concepts.

276
00:17:23,000 --> 00:17:26,000
Now,

277
00:17:26,000 --> 00:17:29,000
that kind of Dhammas excluded by his saying,

278
00:17:29,000 --> 00:17:31,000
Dhammas meant individual essence.

279
00:17:31,000 --> 00:17:33,000
The act of becoming,

280
00:17:33,000 --> 00:17:35,000
which constitutes exist,

281
00:17:35,000 --> 00:17:37,000
existingness,

282
00:17:37,000 --> 00:17:39,000
in the ultimate sense,

283
00:17:39,000 --> 00:17:41,000
is essence.

284
00:17:41,000 --> 00:17:43,000
It is with essence,

285
00:17:43,000 --> 00:17:46,000
thus it is an individual essence.

286
00:17:46,000 --> 00:17:48,000
Now,

287
00:17:48,000 --> 00:17:53,000
the Dhamma was subhawa.

288
00:17:53,000 --> 00:17:57,000
This word is also important word.

289
00:17:57,000 --> 00:18:00,000
Here,

290
00:18:00,000 --> 00:18:04,000
subhawa is defined.

291
00:18:04,000 --> 00:18:05,000
First,

292
00:18:05,000 --> 00:18:07,000
bhawa is defined.

293
00:18:07,000 --> 00:18:09,000
Bhawa means becoming.

294
00:18:09,000 --> 00:18:13,000
The act of becoming is bhawa.

295
00:18:13,000 --> 00:18:15,000
And something,

296
00:18:15,000 --> 00:18:18,000
which is with this act of becoming,

297
00:18:18,000 --> 00:18:20,000
is called subhawa.

298
00:18:20,000 --> 00:18:22,000
So, bhawa,

299
00:18:22,000 --> 00:18:25,000
the act of becoming means

300
00:18:25,000 --> 00:18:28,000
existingness in the ultimate sense.

301
00:18:28,000 --> 00:18:31,000
That means having the three moments of existence,

302
00:18:31,000 --> 00:18:33,000
arising,

303
00:18:33,000 --> 00:18:36,000
aging and disappearing.

304
00:18:36,000 --> 00:18:39,000
So, such things are called

305
00:18:39,000 --> 00:18:41,000
subhawa,

306
00:18:41,000 --> 00:18:45,000
because they are with bhawa.

307
00:18:45,000 --> 00:18:47,000
So, in this word,

308
00:18:47,000 --> 00:18:49,000
subhawa means with,

309
00:18:49,000 --> 00:18:52,000
and bhawa means the act of becoming.

310
00:18:52,000 --> 00:18:53,000
That is,

311
00:18:53,000 --> 00:18:54,000
let us say existence.

312
00:18:54,000 --> 00:18:56,000
So,

313
00:18:56,000 --> 00:18:58,000
which has their own existence?

314
00:18:58,000 --> 00:19:02,000
They are called subhawa.

315
00:19:02,000 --> 00:19:05,000
In other places,

316
00:19:05,000 --> 00:19:09,000
or in the footnote itself,

317
00:19:09,000 --> 00:19:13,000
subhawa is explained in another way.

318
00:19:13,000 --> 00:19:15,000
There,

319
00:19:15,000 --> 00:19:17,000
subhawa means,

320
00:19:17,000 --> 00:19:19,000
once all,

321
00:19:19,000 --> 00:19:21,000
and bhawa means,

322
00:19:21,000 --> 00:19:23,000
the essence or something like that.

323
00:19:23,000 --> 00:19:25,000
So, subhawa means,

324
00:19:25,000 --> 00:19:27,000
once all essence,

325
00:19:27,000 --> 00:19:31,000
or once all nature.

326
00:19:31,000 --> 00:19:32,000
All,

327
00:19:32,000 --> 00:19:34,000
it means,

328
00:19:34,000 --> 00:19:38,000
common essence of common nature.

329
00:19:38,000 --> 00:19:39,000
So,

330
00:19:39,000 --> 00:19:42,000
subhawa can mean,

331
00:19:42,000 --> 00:19:45,000
different things in different contexts.

332
00:19:45,000 --> 00:19:48,000
If it is used as a substantive,

333
00:19:48,000 --> 00:19:52,000
then it means something that has,

334
00:19:52,000 --> 00:19:55,000
becoming,

335
00:19:55,000 --> 00:19:57,000
or that has existence,

336
00:19:57,000 --> 00:20:00,000
can be ultimate sense.

337
00:20:00,000 --> 00:20:02,000
Something that has existence.

338
00:20:02,000 --> 00:20:04,000
In other places,

339
00:20:04,000 --> 00:20:06,000
it means just,

340
00:20:06,000 --> 00:20:09,000
once individual

341
00:20:09,000 --> 00:20:10,000
nature,

342
00:20:10,000 --> 00:20:12,000
or individual essence,

343
00:20:12,000 --> 00:20:14,000
or common essence,

344
00:20:14,000 --> 00:20:16,000
common nature.

345
00:20:16,000 --> 00:20:18,000
There are two kinds of nature,

346
00:20:18,000 --> 00:20:21,000
individual and common.

347
00:20:21,000 --> 00:20:26,000
Suppose,

348
00:20:26,000 --> 00:20:29,000
I am a gummies.

349
00:20:29,000 --> 00:20:33,000
So, gummies'ness is my individual essence,

350
00:20:33,000 --> 00:20:35,000
and I am a human being.

351
00:20:35,000 --> 00:20:38,000
And this is my common essence,

352
00:20:38,000 --> 00:20:42,000
common with the other beings.

353
00:20:42,000 --> 00:20:46,000
So, something like that.

354
00:20:46,000 --> 00:20:50,000
So, subhawa can mean these things.

355
00:20:50,000 --> 00:20:58,000
Here,

356
00:20:58,000 --> 00:21:00,000
so, we should translate,

357
00:21:00,000 --> 00:21:02,000
not individual essence,

358
00:21:02,000 --> 00:21:07,000
some things which have individual essence.

359
00:21:07,000 --> 00:21:10,000
Here, not essence.

360
00:21:10,000 --> 00:21:12,000
What is essence?

361
00:21:12,000 --> 00:21:14,000
It is an abstract now, right?

362
00:21:14,000 --> 00:21:15,000
Essence.

363
00:21:15,000 --> 00:21:19,000
It doesn't mean a substantive thing.

364
00:21:19,000 --> 00:21:20,000
No.

365
00:21:20,000 --> 00:21:24,000
So, something which has individual essence,

366
00:21:24,000 --> 00:21:27,000
or some things which have individual essence,

367
00:21:27,000 --> 00:21:29,000
are called dhammas here.

368
00:21:29,000 --> 00:21:31,000
And whether form or unform and so on,

369
00:21:31,000 --> 00:21:37,000
they are whether conditioned or unconditioned.

370
00:21:37,000 --> 00:21:47,000
And the long foot note.

371
00:21:47,000 --> 00:21:51,000
So, individual essence consisting in say,

372
00:21:51,000 --> 00:21:53,000
hardness as that of the earth,

373
00:21:53,000 --> 00:21:55,000
or touching as that of contact,

374
00:21:55,000 --> 00:21:57,000
is not common to all dhammas.

375
00:21:57,000 --> 00:21:59,000
The generality is the individual essence

376
00:21:59,000 --> 00:22:03,000
common to all consisting in common and so on.

377
00:22:03,000 --> 00:22:07,000
So, suppose,

378
00:22:07,000 --> 00:22:10,000
we have up element, right?

379
00:22:10,000 --> 00:22:13,000
Up element has individual essence

380
00:22:13,000 --> 00:22:16,000
and general realm of common essence.

381
00:22:16,000 --> 00:22:20,000
Its individual essence is hardness or softness.

382
00:22:20,000 --> 00:22:24,000
So, hardness or softness is the individual essence of

383
00:22:24,000 --> 00:22:27,000
earth element,

384
00:22:27,000 --> 00:22:30,000
not shared by other elements and dhammas,

385
00:22:30,000 --> 00:22:33,000
but it is in common end.

386
00:22:33,000 --> 00:22:36,000
So, its in common end is

387
00:22:36,000 --> 00:22:40,000
a common or general essence of it.

388
00:22:40,000 --> 00:22:41,000
So, in this way,

389
00:22:41,000 --> 00:22:51,000
we can have the specific or individual essence and general essence.

390
00:22:51,000 --> 00:23:19,000
And with regard to time and the footnotes,

391
00:23:19,000 --> 00:23:21,000
for example,

392
00:23:21,000 --> 00:23:24,000
as specified in the first paragraph of the dhammas underneath,

393
00:23:24,000 --> 00:23:28,000
and its non-existence as to individual essence.

394
00:23:28,000 --> 00:23:33,000
Yes, as the non entity before and after,

395
00:23:33,000 --> 00:23:38,000
the movement in which those cognizant and co-present dhammas occur,

396
00:23:38,000 --> 00:23:42,000
it is called the container fadikara.

397
00:23:42,000 --> 00:23:47,000
It is perceived symbolized only at the state of a

398
00:23:47,000 --> 00:23:51,000
dhammas.

399
00:23:51,000 --> 00:24:01,000
I wonder whether he understood this.

400
00:24:01,000 --> 00:24:09,000
Now, first,

401
00:24:09,000 --> 00:24:13,000
we have to have a little knowledge of a vidhamma.

402
00:24:13,000 --> 00:24:18,000
So, in the vidhamma,

403
00:24:18,000 --> 00:24:22,000
on such an occasion or at such a time,

404
00:24:22,000 --> 00:24:26,000
as I said, the first crucial idea arises.

405
00:24:26,000 --> 00:24:29,000
Now, at such a time,

406
00:24:29,000 --> 00:24:32,000
the word, at such a time, in English the words, right?

407
00:24:32,000 --> 00:24:34,000
In body one word.

408
00:24:34,000 --> 00:24:41,000
So, if the words are put in the logative case,

409
00:24:41,000 --> 00:24:46,000
it's on or in the logative case.

410
00:24:46,000 --> 00:24:51,000
Now, why is here,

411
00:24:51,000 --> 00:24:54,000
let me say,

412
00:24:54,000 --> 00:24:57,000
the poly what is there to use is samaya.

413
00:24:57,000 --> 00:25:02,000
And samaya can mean time or it can mean occasion.

414
00:25:02,000 --> 00:25:04,000
So, let us take time.

415
00:25:04,000 --> 00:25:06,000
It means time.

416
00:25:06,000 --> 00:25:10,000
So, why is time the location of consciousness?

417
00:25:10,000 --> 00:25:14,000
So, it is going to explain this,

418
00:25:14,000 --> 00:25:18,000
receptacle and container just means location.

419
00:25:18,000 --> 00:25:26,000
So, time is not existing according to a vidhamma.

420
00:25:26,000 --> 00:25:31,000
So,

421
00:25:31,000 --> 00:25:35,000
no time is determined by the kind of consciousness

422
00:25:35,000 --> 00:25:39,000
that is not existing as to individual essence.

423
00:25:39,000 --> 00:25:43,000
So, according to reality, time has no existence.

424
00:25:43,000 --> 00:25:45,000
So, time is not existing.

425
00:25:45,000 --> 00:25:47,000
But if it is determined by the jade,

426
00:25:47,000 --> 00:25:51,000
that means the time when the given jade arises.

427
00:25:51,000 --> 00:25:53,000
So, it is determined by the jade.

428
00:25:53,000 --> 00:25:56,000
Yes, it has been not entity.

429
00:25:56,000 --> 00:26:00,000
So, the following, the following are not so good.

430
00:26:00,000 --> 00:26:07,000
So, why can time be the location of jade?

431
00:26:07,000 --> 00:26:10,000
Time is not existence.

432
00:26:10,000 --> 00:26:13,000
And how can it be the location of jade?

433
00:26:13,000 --> 00:26:18,000
To the answer is,

434
00:26:18,000 --> 00:26:22,000
well, a consciousness arises.

435
00:26:22,000 --> 00:26:25,000
Before it arises, there is nothing.

436
00:26:25,000 --> 00:26:30,000
And after it dissolves, there is nothing.

437
00:26:30,000 --> 00:26:37,000
But when it is in the, during the three phases of existence,

438
00:26:37,000 --> 00:26:39,000
it is called existing.

439
00:26:39,000 --> 00:26:45,000
So, at that time, the consciousness is in existence.

440
00:26:45,000 --> 00:26:54,000
So, time is said to be a location or a skeptical of the consciousness.

441
00:26:54,000 --> 00:27:01,000
Actually, there is no time. Time is not existent or not existing,

442
00:27:01,000 --> 00:27:04,000
according to ultimate reality.

443
00:27:04,000 --> 00:27:08,000
Although it is not existent, it can be,

444
00:27:08,000 --> 00:27:12,000
it is said to be the location of jade.

445
00:27:12,000 --> 00:27:18,000
Because before the arising of jade, there is no jade.

446
00:27:18,000 --> 00:27:21,000
And after the dissolution of jade, there is no jade.

447
00:27:21,000 --> 00:27:26,000
But that is why time is said to be a location for jade.

448
00:27:26,000 --> 00:27:30,000
And Buddha said, at such a time, at such a time,

449
00:27:30,000 --> 00:27:35,000
and the first, so much data arises in support.

450
00:27:35,000 --> 00:27:38,000
What is saying all true for space?

451
00:27:38,000 --> 00:27:40,000
Space, space is all to mention, yeah.

452
00:27:40,000 --> 00:27:43,000
Space and time are said to be non-existent,

453
00:27:43,000 --> 00:27:48,000
according to abidama.

454
00:27:48,000 --> 00:27:55,000
So, the translation itself is not correct here.

455
00:27:55,000 --> 00:28:01,000
You know, you need to be very familiar with the language,

456
00:28:01,000 --> 00:28:07,000
because there are words that looks the same,

457
00:28:07,000 --> 00:28:11,000
but you have to understand differently.

458
00:28:11,000 --> 00:28:18,000
So, bhava and abhava, if the word abhava is misunderstood here,

459
00:28:18,000 --> 00:28:24,000
he translated as non-entity, and he gives a word abhava.

460
00:28:24,000 --> 00:28:31,000
But what it means is just non-existent.

461
00:28:31,000 --> 00:28:38,000
So, the time can be the location or receptacle of theta,

462
00:28:38,000 --> 00:28:51,000
because before the arising, or after the dissolution of theta

463
00:28:51,000 --> 00:28:57,000
and its confidence, they do not exist.

464
00:28:57,000 --> 00:29:02,000
So, it is like a reference, right?

465
00:29:02,000 --> 00:29:12,000
And the data arises at this time, or at that time.

466
00:29:12,000 --> 00:29:25,000
So, somehow.

467
00:29:25,000 --> 00:29:33,000
Now, and then, in the text itself, explanation of different words.

468
00:29:33,000 --> 00:29:42,000
Now, fading away and disillusionment of vanity and so on,

469
00:29:42,000 --> 00:29:49,000
and in the explanation of the words on coming to it,

470
00:29:49,000 --> 00:29:56,000
on coming to it are used, on coming to it, they are run and quit,

471
00:29:56,000 --> 00:29:58,000
or they fade away and so on.

472
00:29:58,000 --> 00:30:03,000
So, on coming to it really means all account of it,

473
00:30:03,000 --> 00:30:09,000
or on taking it as object.

474
00:30:09,000 --> 00:30:16,000
Now, fading away is not nibana,

475
00:30:16,000 --> 00:30:19,000
but nibana is described as with the words,

476
00:30:19,000 --> 00:30:22,000
which can mean fading away.

477
00:30:22,000 --> 00:30:27,000
The word family, we raga, is used here, and other words too.

478
00:30:27,000 --> 00:30:34,000
So, we raga is simply translated as fading away.

479
00:30:34,000 --> 00:30:37,000
But the actual fading away is not nibana,

480
00:30:37,000 --> 00:30:39,000
nibana is not fading away.

481
00:30:39,000 --> 00:30:47,000
Nibana is something, nibana, although it has no existence,

482
00:30:47,000 --> 00:30:50,000
it is.

483
00:30:50,000 --> 00:30:55,000
So, nibana is something.

484
00:30:55,000 --> 00:31:01,000
And when maka-chita arises,

485
00:31:01,000 --> 00:31:04,000
it eradicates mental refinement.

486
00:31:04,000 --> 00:31:09,000
So, it makes mental refinement fade away,

487
00:31:09,000 --> 00:31:15,000
but it arises only when it takes nibana as object.

488
00:31:15,000 --> 00:31:20,000
If it does not take nibana as object, it cannot arise.

489
00:31:20,000 --> 00:31:24,000
So, nibana is a condition for the maka-chita,

490
00:31:24,000 --> 00:31:27,000
part consciousness to arise.

491
00:31:27,000 --> 00:31:32,000
So, nibana is said to be instrumental

492
00:31:32,000 --> 00:31:40,000
in making different moments fade away by thought,

493
00:31:40,000 --> 00:31:41,000
by maka.

494
00:31:41,000 --> 00:31:44,000
So, it is to understood in this way,

495
00:31:44,000 --> 00:31:48,000
all these words, not just feeding away,

496
00:31:48,000 --> 00:31:52,000
and then termination of the wrong,

497
00:31:52,000 --> 00:31:54,000
and what are the other words?

498
00:31:54,000 --> 00:31:57,000
Extinction of something.

499
00:31:57,000 --> 00:32:02,000
Now, whenever I just said it means,

500
00:32:02,000 --> 00:32:05,000
nibana is instrumental in extinction

501
00:32:05,000 --> 00:32:08,000
of suffering something like that.

502
00:32:08,000 --> 00:32:31,000
And then,

503
00:32:31,000 --> 00:32:41,000
if you have a question,

504
00:32:41,000 --> 00:32:50,000
if you have a question,

505
00:32:50,000 --> 00:32:53,000
you can see it and so on.

506
00:32:53,000 --> 00:32:57,000
So, we are there to be understood in that sense.

507
00:32:57,000 --> 00:33:02,000
That is, just feeding, not just feeding away is nibana,

508
00:33:02,000 --> 00:33:07,000
but nibana is something which is instrumental

509
00:33:07,000 --> 00:33:16,000
in making defilements fade away by maka.

510
00:33:16,000 --> 00:33:32,000
Here is one, one, let me see.

511
00:33:32,000 --> 00:33:40,000
Something like,

512
00:33:40,000 --> 00:33:43,000
change of lineage.

513
00:33:43,000 --> 00:33:54,000
Can you see the word change of lineage somewhere?

514
00:33:54,000 --> 00:33:57,000
Middle of the word now.

515
00:33:57,000 --> 00:34:02,000
On phase 118.

516
00:34:02,000 --> 00:34:09,000
Now, what the other,

517
00:34:09,000 --> 00:34:14,000
I mean, the other, the sub-command you are saying is that

518
00:34:14,000 --> 00:34:17,000
change of lineage,

519
00:34:17,000 --> 00:34:20,000
that the,

520
00:34:20,000 --> 00:34:25,000
precedes immediately precedes the moment of fact.

521
00:34:25,000 --> 00:34:29,000
And that, that's it as called change of lineage.

522
00:34:29,000 --> 00:34:34,000
And that, that takes nibana as object, right?

523
00:34:34,000 --> 00:34:39,000
So, nibana, which is to be realized by,

524
00:34:39,000 --> 00:34:43,000
or which is to be seen by the moment of change of lineage,

525
00:34:43,000 --> 00:34:46,000
must be one that has to be profunded,

526
00:34:46,000 --> 00:34:50,000
supposing the nature of belonging to three periods of time.

527
00:34:50,000 --> 00:34:51,000
Nibana is timeless.

528
00:34:51,000 --> 00:34:59,000
Nibana does not belong to the present or first of future.

529
00:34:59,000 --> 00:35:06,000
So, in order for nibana to be realized by change of lineage,

530
00:35:06,000 --> 00:35:11,000
it must also, it must be, it must, it must,

531
00:35:11,000 --> 00:35:17,000
it must not belong to any of the three periods.

532
00:35:17,000 --> 00:35:24,000
That is what is meant there.

533
00:35:24,000 --> 00:35:32,000
Okay, now we come to the end of,

534
00:35:32,000 --> 00:35:36,000
you pass someone else actually.

535
00:35:36,000 --> 00:35:41,000
So, there are other, other descriptions of nibana,

536
00:35:41,000 --> 00:35:44,000
and you find them on page 320,

537
00:35:44,000 --> 00:35:46,000
because I shall teach you the informed,

538
00:35:46,000 --> 00:35:49,000
the true, the other show, the hard to see,

539
00:35:49,000 --> 00:35:52,000
the anti-caring, the lasting, the undiclassified,

540
00:35:52,000 --> 00:35:54,000
the dead, less, the auspicious, the saved,

541
00:35:54,000 --> 00:35:57,000
the muggle, the intact, the unaffected,

542
00:35:57,000 --> 00:36:00,000
the chewy, the islands, the shelter,

543
00:36:00,000 --> 00:36:04,000
and then there are more in the sinewgani gala.

544
00:36:04,000 --> 00:36:09,000
So, you can pick up some positive words from this,

545
00:36:09,000 --> 00:36:16,000
and say that nibana is also a positive state.

546
00:36:16,000 --> 00:36:20,000
And then benefits of,

547
00:36:20,000 --> 00:36:24,000
the practice of the contemplation

548
00:36:24,000 --> 00:36:28,000
on nibana or contemplation on fees.

549
00:36:28,000 --> 00:36:34,000
Okay, this can be the end of this chapter,

550
00:36:34,000 --> 00:36:38,000
each chapter.

551
00:36:38,000 --> 00:36:43,000
So, this, this, this, this class,

552
00:36:43,000 --> 00:36:47,000
and we, we have played for about, about it, man.

553
00:36:47,000 --> 00:36:50,000
Almost, almost.

554
00:36:50,000 --> 00:36:52,000
Now, the first, Thursday,

555
00:36:52,000 --> 00:36:54,000
and July, we'll meet back here.

556
00:36:54,000 --> 00:36:55,000
That's good.

557
00:36:55,000 --> 00:36:57,000
Can you hear my friends?

558
00:36:57,000 --> 00:36:58,000
Thank you.

559
00:36:58,000 --> 00:37:01,000
I will be good on the photo on the horizon.

560
00:37:01,000 --> 00:37:02,000
Oh, yes.

561
00:37:02,000 --> 00:37:03,000
Yeah.

562
00:37:03,000 --> 00:37:05,000
The interesting side of this.

563
00:37:05,000 --> 00:37:07,000
Oh, yeah, we got it.

564
00:37:07,000 --> 00:37:08,000
Okay.

565
00:37:12,000 --> 00:37:14,000
Thank you very much.

566
00:37:14,000 --> 00:37:15,000
Thank you.

567
00:37:15,000 --> 00:37:17,000
Thank you.

568
00:37:45,000 --> 00:37:48,000
.

569
00:37:48,000 --> 00:37:53,000
..

570
00:37:53,000 --> 00:37:55,000
..

571
00:37:55,000 --> 00:38:02,000
..

572
00:38:02,000 --> 00:38:05,000
...

573
00:38:05,000 --> 00:38:09,000
...

574
00:38:09,000 --> 00:38:13,000
...

575
00:38:13,000 --> 00:38:16,000
.

576
00:38:17,000 --> 00:38:20,000
.

577
00:38:21,000 --> 00:38:26,000
.

578
00:38:27,000 --> 00:38:30,000
.

579
00:38:30,000 --> 00:38:33,000
..

580
00:38:33,000 --> 00:38:36,000
..

581
00:38:36,000 --> 00:38:38,000
..

582
00:38:38,000 --> 00:38:41,000
..

