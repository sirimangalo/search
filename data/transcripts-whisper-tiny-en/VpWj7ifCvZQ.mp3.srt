1
00:00:00,000 --> 00:00:07,160
When meditating is it okay for the mind to move from thing to thing as long as you're a mindful of and note each different thing

2
00:00:07,760 --> 00:00:10,460
Yeah, except it has the tendency to

3
00:00:14,000 --> 00:00:19,620
If it's proactive where one you know, it has a tendency to become proactive where one is

4
00:00:20,120 --> 00:00:25,280
Looking for the next thing waiting for the next thing and then it becomes a cause for

5
00:00:25,280 --> 00:00:29,440
Distraction and diffusion of one's concentration

6
00:00:30,800 --> 00:00:34,120
so we try to encourage a a

7
00:00:36,240 --> 00:00:40,160
Structured approach where you have your response

8
00:00:40,880 --> 00:00:43,040
Like if you look at as a computer program, right?

9
00:00:43,520 --> 00:00:44,720
They have

10
00:00:44,720 --> 00:00:53,200
When when exercises you deal with it in this case you're mindful of it and then once it's finished return to

11
00:00:53,200 --> 00:01:02,200
The rising and falling for example our structure tends to be returned to the rising and falling so when something is finished rather than

12
00:01:03,200 --> 00:01:04,560
allowing

13
00:01:04,560 --> 00:01:10,840
The mind to go to the next thing or or choosing you it's kind if it becomes a choice

14
00:01:12,160 --> 00:01:19,520
Take the choice to come back to the the rising and falling don't wait for the next thing to come up if by the time you acknowledge one thing and it's gone

15
00:01:19,520 --> 00:01:23,280
Something else is already there. Then by all means you're welcome to

16
00:01:23,840 --> 00:01:28,960
You know, it's not a it's not magic where if you do it right you're going to become enlightened

17
00:01:28,960 --> 00:01:30,960
It's an exploration of of reality

18
00:01:31,840 --> 00:01:32,960
so

19
00:01:32,960 --> 00:01:37,280
The only problem with going from one thing one thing to the next is that you wind up without a base

20
00:01:37,520 --> 00:01:40,600
Not knowing what to do next when nothing comes up for example

21
00:01:40,760 --> 00:01:45,280
So the answer should always become back to the rising and falling until the next thing arises

22
00:01:45,280 --> 00:01:52,560
Just just for the purpose that it keeps your mind focused. It keeps your mind from getting lost and from giving rise to

23
00:01:54,640 --> 00:01:56,640
unwanted states of

24
00:01:57,440 --> 00:02:02,240
over-exertion, you know extreme energy where you become distracted and

25
00:02:02,960 --> 00:02:04,320
and

26
00:02:04,320 --> 00:02:08,560
Flutter to we're looking at the this this passage in the Yuganada suit to

27
00:02:08,560 --> 00:02:14,320
um where the what's it called dhamma dhamma something

28
00:02:15,600 --> 00:02:20,560
Uduce the word I can't remember the word exactly dhamma something dhamma dhamma

29
00:02:21,520 --> 00:02:23,520
again

30
00:02:24,160 --> 00:02:26,640
I forgot but it's it's

31
00:02:26,640 --> 00:02:28,640
um

32
00:02:29,920 --> 00:02:38,240
Distraction that comes from meditation, you know, you wind up meditating on all sorts of things and many different things come up and and you wind up getting

33
00:02:38,560 --> 00:02:45,920
Flustered in the meditation and so the Buddha said one way of people become enlightened is by focusing their meditations

34
00:02:46,000 --> 00:02:49,840
They they have good qualities of mind, but they're not able to focus them

35
00:02:51,440 --> 00:02:54,080
Properly because their mind is too diffuse

36
00:02:54,080 --> 00:02:56,080
so

37
00:02:56,080 --> 00:03:00,240
Try to be structured about it and always come back to a main object

38
00:03:00,240 --> 00:03:27,440
Not forcing it back, but when there's nothing left come back to the rising and falling

