1
00:00:00,000 --> 00:00:07,240
Hi everyone, I'm Adder, a volunteer for Siri-Mongolow International, the organization

2
00:00:07,240 --> 00:00:11,360
that supports you to Domobiku in his teachings.

3
00:00:11,360 --> 00:00:15,720
I'd like to take a few minutes to tell you all about some of the things our organization

4
00:00:15,720 --> 00:00:17,360
does.

5
00:00:17,360 --> 00:00:23,240
If you're watching this video, you're probably aware of the plethora of videos that

6
00:00:23,240 --> 00:00:28,120
Bonte U2 Domo posts on YouTube to share his teachings.

7
00:00:28,120 --> 00:00:31,560
You might not know about some of the other things we do.

8
00:00:31,560 --> 00:00:36,680
Feel free to visit SiriMongolow.org for more information.

9
00:00:36,680 --> 00:00:43,040
One thing that we offer is a live meditation center located in Ontario, Canada.

10
00:00:43,040 --> 00:00:48,360
If you'd like to schedule an intensive retreat under the guidance of Bonte U2 Domo,

11
00:00:48,360 --> 00:00:52,360
please visit our website to do so.

12
00:00:52,360 --> 00:00:59,400
Furthermore, Bonte offers online instruction in our Meditation Plus community.

13
00:00:59,400 --> 00:01:06,480
If you visit meditation.siriMongolow.org, you can schedule a time to meet weekly with Bonte

14
00:01:06,480 --> 00:01:11,760
U2 Domo and do an online meditation course.

15
00:01:11,760 --> 00:01:20,120
Additionally, the community hosts a chat room, our Ask Questions board, where you can ask

16
00:01:20,120 --> 00:01:26,520
the questions that U2 Domo responds to online, and there's also a meditation timer and

17
00:01:26,520 --> 00:01:31,560
a way to track your meditation progress.

18
00:01:31,560 --> 00:01:42,480
We also host a weekly Dhamma study group, and this happens in the online voice chat platform

19
00:01:42,480 --> 00:01:48,760
called Discord, and I'll have a link below for you to go there.

20
00:01:48,760 --> 00:01:55,120
Additionally, our Discord hosts a general room for discussion, and is a great place for our

21
00:01:55,120 --> 00:01:57,120
volunteers to connect.

22
00:01:57,120 --> 00:02:02,360
If you're interested in volunteering for the organization, feel free to get on Discord

23
00:02:02,360 --> 00:02:04,760
and get in touch with us.

24
00:02:04,760 --> 00:02:12,680
Finally, I want you all to know that our organization depends on the generous donations from

25
00:02:12,680 --> 00:02:14,280
our supporters.

26
00:02:14,280 --> 00:02:20,600
If you're interested in supporting SiriMongolow, please visit SiriMongolow.org slash

27
00:02:20,600 --> 00:02:22,560
support.

28
00:02:22,560 --> 00:02:23,560
Thank you all.

29
00:02:23,560 --> 00:02:50,800
See you next time.

