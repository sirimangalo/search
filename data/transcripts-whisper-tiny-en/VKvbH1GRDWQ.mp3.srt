1
00:00:00,000 --> 00:00:08,000
Any advice on how to avoid nihilism?

2
00:00:08,000 --> 00:00:15,000
Whether it's an interesting comment in the, on YouTube, today I think that someone,

3
00:00:15,000 --> 00:00:20,000
someone mentioned in nihilism and said that this kind of practice is basically nihilism

4
00:00:20,000 --> 00:00:24,000
and then someone said, well what's wrong with nihilism or not what's wrong with nihilism

5
00:00:24,000 --> 00:00:31,000
but what reason do you have to think that nihilism isn't the truth?

6
00:00:31,000 --> 00:00:43,000
And I guess, I guess the point is that the real question is that I would ask

7
00:00:43,000 --> 00:00:48,000
what do you mean by nihilism? Because the Buddha was accused of being nihilist

8
00:00:48,000 --> 00:00:53,000
and he said, well there's a way in which you could understand me to be nihilist

9
00:00:53,000 --> 00:00:59,000
because I teach for the annihilation, nihilism means the teaching annihilation

10
00:00:59,000 --> 00:01:03,000
and the Buddha teaches annihilation, he teaches the annihilation of greed,

11
00:01:03,000 --> 00:01:06,000
the annihilation of anger, the annihilation of delusion,

12
00:01:06,000 --> 00:01:14,000
the annihilation of, of all evil states, of all manifold unwholesome states

13
00:01:14,000 --> 00:01:18,000
and he said in that way you could understand me to be nihilist

14
00:01:18,000 --> 00:01:25,000
because I teach this annihilation. To get a little bit more directly to your question

15
00:01:25,000 --> 00:01:29,000
it's a, the framework on from what you're asking is wrong.

16
00:01:29,000 --> 00:01:35,000
It's the same as how do you avoid determinism because all of these, these questions,

17
00:01:35,000 --> 00:01:44,000
they assume a framework, a structure of the universe that isn't really there.

18
00:01:44,000 --> 00:01:50,000
There is no framework, there is no three-dimensional space that's all an extrapolation based on experience.

19
00:01:50,000 --> 00:01:55,000
Reality is practical. Reality is moment to moment experience.

20
00:01:55,000 --> 00:01:58,000
That doesn't change whether you hold this view or that view,

21
00:01:58,000 --> 00:02:01,000
but all of our views are an extrapolation from that.

22
00:02:01,000 --> 00:02:05,000
All that we have is a moment to moment experience.

23
00:02:05,000 --> 00:02:08,000
And the Buddha taught, changing this experience,

24
00:02:08,000 --> 00:02:16,000
taught us to change this experience in a good way, in a positive way to remove all of the unwholesomeness.

25
00:02:16,000 --> 00:02:24,000
He didn't teach any kind of view, he explained reality as it was in order to help people to understand it

26
00:02:24,000 --> 00:02:28,000
and to overcome their attachments to suffering basically

27
00:02:28,000 --> 00:02:32,000
and the things that lead them to suffer because he realized for himself

28
00:02:32,000 --> 00:02:40,000
what was the cause of suffering. And that's really it. You have to understand not only is Buddhism focused on the practical,

29
00:02:40,000 --> 00:03:04,000
Buddhism claims that practical is reality. Reality is only a practical thing.

