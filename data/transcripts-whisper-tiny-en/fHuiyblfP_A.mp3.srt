1
00:00:00,000 --> 00:00:08,000
When new Buddhists learn Buddhism should we learn poly first?

2
00:00:08,000 --> 00:00:15,000
No, no, no. You shouldn't learn poly first.

3
00:00:15,000 --> 00:00:24,000
No, there's no need. I mean language is not really a barrier.

4
00:00:24,000 --> 00:00:27,000
There's nothing wrong with the English language.

5
00:00:27,000 --> 00:00:33,000
It's not as nice as the poly language, but there's nothing wrong with it.

6
00:00:33,000 --> 00:00:40,000
I mean, even poly was coming from the point of...

7
00:00:40,000 --> 00:00:49,000
I had its roots in a lot of delusion, so they still used eye and possessiveness and so on.

8
00:00:49,000 --> 00:00:53,000
They kind of did. They had a different use of possession.

9
00:00:53,000 --> 00:00:58,000
Instead of saying, I have this, they would say, there is this.

10
00:00:58,000 --> 00:01:03,000
There is the this of me, or there is my this.

11
00:01:03,000 --> 00:01:07,000
But there's still very much possessiveness in it, so.

12
00:01:07,000 --> 00:01:09,000
And things like that are not really...

13
00:01:09,000 --> 00:01:11,000
Poly is not really a special language.

14
00:01:11,000 --> 00:01:18,000
So a translation of the Buddhist teachings is not going to keep you from understanding the teaching.

15
00:01:18,000 --> 00:01:23,000
So for someone who does know poly, it's very difficult to read translations.

16
00:01:23,000 --> 00:01:32,000
I find because immediately I want to know what word, what poly word they're referring to.

17
00:01:32,000 --> 00:01:37,000
So definitely, if the question is, is there a benefit in learning poly?

18
00:01:37,000 --> 00:01:45,000
I think there's an incredible benefit in learning poly because those are the exact words that we understand that the Buddha used.

19
00:01:45,000 --> 00:01:50,000
So it's not that there's anything wrong with the English words, it's just we don't know that those are the words that the Buddha would use.

20
00:01:50,000 --> 00:01:55,000
And in many cases we're sure that that's not the Buddha would use.

21
00:01:55,000 --> 00:02:04,000
And of course, everyone has their own opinion of which word the Buddha would have used or which word is the word that the Buddha meant.

22
00:02:04,000 --> 00:02:10,000
And it can even lead to different interpretations of the Buddha's teaching.

23
00:02:10,000 --> 00:02:15,000
So it's incredibly useful to learn poly.

24
00:02:15,000 --> 00:02:17,000
There's no question about it.

25
00:02:17,000 --> 00:02:19,000
But it's not going to stop you.

26
00:02:19,000 --> 00:02:22,000
It's certainly not going to stop you from practicing meditation.

27
00:02:22,000 --> 00:02:25,000
It's not going to stop you from becoming enlightened.

28
00:02:25,000 --> 00:02:29,000
And that's what's most important.

29
00:02:29,000 --> 00:02:33,000
Even though it's not your question, if the question,

30
00:02:33,000 --> 00:02:43,000
how should we go about learning poly and where should it come about in our practice?

31
00:02:43,000 --> 00:02:52,000
I would say after you get a solid practice and you are clear in your mind about the path that you're following.

32
00:02:52,000 --> 00:02:56,000
I mean, ideally, and I don't want to go throwing these words around,

33
00:02:56,000 --> 00:03:06,000
but ideally I would say one would want to become a soda pun before concerning oneself with learning poly.

34
00:03:06,000 --> 00:03:10,000
I mean, it's such a big thing to say, and most people are shocked by,

35
00:03:10,000 --> 00:03:13,000
or most people, some people would be shocked to hear such a thing.

36
00:03:13,000 --> 00:03:21,000
The idea of being so bold as to suggest, well, just to attain that lesser path,

37
00:03:21,000 --> 00:03:24,000
or something simple to it, become a soda pun.

38
00:03:24,000 --> 00:03:29,000
And there's no you could do it without poly, but I stand by it.

39
00:03:29,000 --> 00:03:33,000
I think this is ideal.

40
00:03:33,000 --> 00:03:37,000
Now, if you don't feel comfortable with such a proposition

41
00:03:37,000 --> 00:03:45,000
and think that becoming a soda pun is something that would take years or even your whole life to attain,

42
00:03:45,000 --> 00:03:49,000
then let's just put it once you're comfortable in your practice.

43
00:03:49,000 --> 00:03:56,000
You're sure you've got a practice that works for you, and you're quite under the impression that you're not going to change your practice,

44
00:03:56,000 --> 00:04:01,000
or you're not going to waver in your practice.

45
00:04:01,000 --> 00:04:06,000
So then, because then all that needs to be done is to continue the practice.

46
00:04:06,000 --> 00:04:11,000
Much of the studying in the meditation has been done,

47
00:04:11,000 --> 00:04:15,000
and there's really very little concern at that point.

48
00:04:15,000 --> 00:04:20,000
I mean, the point is as a soda pun, and then there's little concern that there's no concern that you're going to follow away from the path.

49
00:04:20,000 --> 00:04:28,000
So you want to have as little concern that you're going to follow away from the path first before you start to go into deep study of it.

50
00:04:28,000 --> 00:04:31,000
So set yourself on the path first.

51
00:04:31,000 --> 00:04:38,000
Once you've done that, then for the benefit of your practice, and for the benefit of people around you,

52
00:04:38,000 --> 00:04:42,000
and for the benefit of you, for your interactions with people around you,

53
00:04:42,000 --> 00:04:45,000
you're going to have an in-depth knowledge of the Buddha's teaching.

54
00:04:45,000 --> 00:04:48,000
It's going to benefit your own practice, it's going to benefit your life,

55
00:04:48,000 --> 00:04:54,000
allow you to live Buddhist life and interact with people around you in a Buddhist way.

56
00:04:54,000 --> 00:05:00,000
So by all means, learn as much as you can, including poly.

57
00:05:00,000 --> 00:05:07,000
And I was thinking about this recently.

58
00:05:07,000 --> 00:05:20,000
There are many ways that you can go about learning poly, but the way I learned poly seems to me to be one very valid means of becoming fluent in it.

59
00:05:20,000 --> 00:05:23,000
And that was, I took a year of Sanskrit.

60
00:05:23,000 --> 00:05:29,000
So if you have the chance to take a year of Sanskrit, I would recommend it.

61
00:05:29,000 --> 00:05:40,000
And I know it's a little bit round about, but Sanskrit is the formal sibling of poly.

62
00:05:40,000 --> 00:05:51,000
And it's kind of, I suppose, it's arguable, or it's argued.

63
00:05:51,000 --> 00:05:54,000
I don't really know this course.

64
00:05:54,000 --> 00:06:01,000
It's scholarly opinions on it, but I've heard that there's the idea that Sanskrit is actually newer than poly.

65
00:06:01,000 --> 00:06:09,000
Classical Sanskrit came after poly, historically, but be that as it may, Sanskrit is a much more formal language.

66
00:06:09,000 --> 00:06:15,000
And so if you can learn Sanskrit, poly makes much more sense.

67
00:06:15,000 --> 00:06:28,000
And you can see where the Sanskrit or where the poly words came from, because poly actually blurs many of the roots.

68
00:06:28,000 --> 00:06:30,000
So it's difficult to see.

69
00:06:30,000 --> 00:06:38,000
Like Nirvana, Vana is the root, I think.

70
00:06:38,000 --> 00:06:44,000
I don't know what the root is, but there you can actually see this, the prefix and the root near and Vana.

71
00:06:44,000 --> 00:06:49,000
In poly, it becomes Nirvana, which suddenly you can no longer see the near and the Vana.

72
00:06:49,000 --> 00:06:54,000
Or the neat in poly would be neat in Vana.

73
00:06:54,000 --> 00:06:59,000
But because the V becomes a V, this happens quite a lot.

74
00:06:59,000 --> 00:07:06,000
But when you learn Sanskrit, you have this technical structure, and it's so much more difficult.

75
00:07:06,000 --> 00:07:11,000
So then learning poly is, of course, much easier than learning Sanskrit.

76
00:07:11,000 --> 00:07:16,000
And another reason is that Sanskrit is actually still taught in some places in the world,

77
00:07:16,000 --> 00:07:19,000
whereas poly is very difficult to find.

78
00:07:19,000 --> 00:07:25,000
Of course, I actually took a course on poly after the Sanskrit, but it was totally, you couldn't compare the two,

79
00:07:25,000 --> 00:07:33,000
because the poly was so much watered down for people who were really not of the same caliber.

80
00:07:33,000 --> 00:07:39,000
When we took Sanskrit, there were 20 people in the class, and I think 10 of us passed.

81
00:07:39,000 --> 00:07:44,000
And that's what the teacher said. He said, when I first took Sanskrit,

82
00:07:44,000 --> 00:07:48,000
the teacher walked into the room on the first day and said, half of you are going to fail.

83
00:07:48,000 --> 00:07:54,000
And in our Sanskrit class, I think half of them either dropped out of fail.

84
00:07:54,000 --> 00:08:02,000
But I did great, and that was really keen on learning it, so it was not difficult at all.

85
00:08:02,000 --> 00:08:05,000
And so that's what I would recommend. If you can't do that, I mean,

86
00:08:05,000 --> 00:08:10,000
going through a poly, an introduction of poly, what book is not really going to give you an understanding of poly.

87
00:08:10,000 --> 00:08:19,000
You really do need to hunker down, I don't know what the, I think it's hunker down, hunker down, hunker down.

88
00:08:19,000 --> 00:08:29,000
You have to sit down and memorize. You have to start memorizing the paradigms memorizing.

89
00:08:29,000 --> 00:08:35,000
Yeah, just paradigm is really vocabulary, memorizing vocabulary would work.

90
00:08:35,000 --> 00:08:39,000
But the modern ways of learning poly are not so, not very helpful, in my opinion,

91
00:08:39,000 --> 00:08:46,000
because they go hodgepodge, and they don't give you a clear overview of anything.

92
00:08:46,000 --> 00:08:56,000
So you really have to learn the paradigms, like Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso, Puriso.

93
00:08:56,000 --> 00:09:01,000
Puriso, and then you have to learn the various forms of the word Puriso.

94
00:09:01,000 --> 00:09:06,000
And then Kanyak, and New York, and Nya, the various forms of the word Kanyak.

95
00:09:06,000 --> 00:09:13,000
I mean, it's kind of purist, but that's how they did it in ancient times,

96
00:09:13,000 --> 00:09:18,000
and that's how they do it in Buddhist countries, and it makes so much more sense.

97
00:09:18,000 --> 00:09:27,000
It's so much more beneficial over the long term for the student to have that clear theoretical understanding.

98
00:09:27,000 --> 00:09:35,000
Anyway, that wasn't your question, but in regards to learning poly, I would go about it in a very structured form,

99
00:09:35,000 --> 00:09:36,000
and really take the time.

100
00:09:36,000 --> 00:09:41,000
And it does answer your question, it helped answer your question, because it helps you see that it's a real big undertaking.

101
00:09:41,000 --> 00:09:50,000
Simply reading a book like Introduction to Poly, or New Course on Poly, or so, and I wouldn't go for it.

102
00:09:50,000 --> 00:10:00,000
I would do things the right way, so you get a clear understanding of the grammar.

103
00:10:00,000 --> 00:10:04,000
And then start doing translations once you've got the grammar under your belt.

104
00:10:04,000 --> 00:10:08,000
There's a lot of grammar, because then you really understand the words.

105
00:10:08,000 --> 00:10:11,000
You don't just look in a dictionary and say, this means this.

106
00:10:11,000 --> 00:10:14,000
Like the word setty, for example.

107
00:10:14,000 --> 00:10:19,000
People say, well, setty means mindfulness, and then some people say, oh, setty doesn't mean mindfulness and so on and so on.

108
00:10:19,000 --> 00:10:21,000
But it really doesn't.

109
00:10:21,000 --> 00:10:26,000
It really doesn't have anything to do with the word mindfulness, and you know that, because it comes from the word sara,

110
00:10:26,000 --> 00:10:28,000
which has nothing to do with mindfulness.

111
00:10:28,000 --> 00:10:36,000
Sara means to remember, or to recollect, or to have, you might say, to have some presence of mind,

112
00:10:36,000 --> 00:10:40,000
in terms of recollecting yourself.

113
00:10:40,000 --> 00:10:44,000
And so that's where they get the word mindfulness.

114
00:10:44,000 --> 00:10:51,000
But if you understand the house sara becomes setty, then you have a much better understanding.

115
00:10:51,000 --> 00:10:55,000
Some pajanya, the other one, some is the prefix.

116
00:10:55,000 --> 00:11:08,000
But is another prefix, and janya is some pajanya, janya would be to know.

117
00:11:08,000 --> 00:11:15,000
And then janya, I think, is the, or that suffix or something like that, that becomes janya, and so on.

118
00:11:15,000 --> 00:11:18,000
Some pajanya knows some pajanya.

119
00:11:18,000 --> 00:11:28,000
It helps you to really understand what the word means, some means full, or no, some, sorry, some means complete,

120
00:11:28,000 --> 00:11:43,000
or it can mean, yeah, you could say, collected, and pa means full, and janya means knowledge, janya means knowledge.

121
00:11:43,000 --> 00:11:52,000
Some pajanya, full and complete knowledge, fully and completely aware of something that some pajanya, for example.

122
00:11:52,000 --> 00:11:59,000
So, yeah, learning the various, learning the very depths of polygrammar is quite useful.

123
00:11:59,000 --> 00:12:03,000
And, you know, and then the other thing is how far you need to go with it.

124
00:12:03,000 --> 00:12:11,000
So, some people might just, might find it enough to learn the basics that are given in these introduction courses.

125
00:12:11,000 --> 00:12:17,000
Anyway, yeah, I don't start with poly, but get around to it someday.

