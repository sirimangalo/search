1
00:00:00,000 --> 00:00:07,000
So that was my arms round.

2
00:00:07,000 --> 00:00:11,000
Not a usual arms round, I suppose.

3
00:00:11,000 --> 00:00:15,000
And I don't always have people stop me.

4
00:00:15,000 --> 00:00:18,000
I don't always have police officers stop me.

5
00:00:18,000 --> 00:00:21,000
And so this was definitely unusual.

6
00:00:21,000 --> 00:00:23,000
It's almost as if I planned it.

7
00:00:23,000 --> 00:00:26,000
Having talked this morning about one of the things I'm

8
00:00:26,000 --> 00:00:29,000
going on quote unquote afraid of being police officers.

9
00:00:29,000 --> 00:00:33,000
And then right after that, being stopped by police officer.

10
00:00:33,000 --> 00:00:36,000
But he was really nice.

11
00:00:36,000 --> 00:00:38,000
He probably has heard from people.

12
00:00:38,000 --> 00:00:40,000
Or he's probably himself.

13
00:00:40,000 --> 00:00:44,000
And he claimed that there are other people wondering or concerned

14
00:00:44,000 --> 00:00:45,000
about who I am.

15
00:00:45,000 --> 00:00:49,000
But it's probably more him interested.

16
00:00:49,000 --> 00:00:51,000
And maybe even suspicious.

17
00:00:51,000 --> 00:00:58,000
But he was very friendly, overtly ostensibly.

18
00:00:58,000 --> 00:00:59,000
How do you say on the outside?

19
00:00:59,000 --> 00:01:01,000
He seemed friendly.

20
00:01:01,000 --> 00:01:07,000
And he just wanted to know what it is that I'm carrying around.

21
00:01:07,000 --> 00:01:09,000
And so I showed him my ops ball.

22
00:01:09,000 --> 00:01:14,000
And he said he didn't have a problem with what I'm doing.

23
00:01:14,000 --> 00:01:18,000
But he said I have to understand that people are going to be

24
00:01:18,000 --> 00:01:23,000
interested and knows anything new when people become curious.

25
00:01:23,000 --> 00:01:25,000
And so I don't think it's a big problem.

26
00:01:25,000 --> 00:01:29,000
I'm really going to write this letter to the local newspaper.

27
00:01:29,000 --> 00:01:31,000
Hopefully I'll write it this afternoon.

28
00:01:31,000 --> 00:01:35,000
And then you'll get a chance to.

29
00:01:35,000 --> 00:01:39,000
I'll read it to you this afternoon.

30
00:01:39,000 --> 00:01:43,000
Why we do arms round?

31
00:01:43,000 --> 00:01:47,000
Well, it's something that came from the Buddhist time.

32
00:01:47,000 --> 00:01:55,000
But without dragging up the past or talking about the tradition of it,

33
00:01:55,000 --> 00:01:59,000
there are some really obvious reasons as to why it's a useful thing

34
00:01:59,000 --> 00:02:01,000
or a beneficial thing to do.

35
00:02:01,000 --> 00:02:09,000
Obviously, the benefits to the monk himself is it should be quite obvious.

36
00:02:09,000 --> 00:02:13,000
There's incredible benefits to the recipient.

37
00:02:13,000 --> 00:02:20,000
I don't have to work for food or I don't have to make money.

38
00:02:20,000 --> 00:02:23,000
I don't touch money.

39
00:02:23,000 --> 00:02:25,000
I don't have to go cooking.

40
00:02:25,000 --> 00:02:27,000
I don't have to keep food.

41
00:02:27,000 --> 00:02:31,000
I mean, it seems a lot like taking advantage of people in that sense.

42
00:02:31,000 --> 00:02:33,000
And a lot of people think of it like that.

43
00:02:33,000 --> 00:02:38,000
I've heard people say that monks are lazy or so on.

44
00:02:38,000 --> 00:02:41,000
And so if we're just asking, what are the benefits?

45
00:02:41,000 --> 00:02:42,000
Why do you do that?

46
00:02:42,000 --> 00:02:46,000
And obviously, I don't think there's any question as to why one would do it

47
00:02:46,000 --> 00:02:48,000
if they were just concerned about their own benefit.

48
00:02:48,000 --> 00:02:53,000
I think the benefits for the people who are giving is probably less clear,

49
00:02:53,000 --> 00:02:58,000
especially for those people who aren't accustomed to giving something

50
00:02:58,000 --> 00:03:02,000
on a daily basis or giving to people that they might not know.

51
00:03:02,000 --> 00:03:08,000
But you really have to look at it in terms of being a religion.

52
00:03:08,000 --> 00:03:14,000
Because we see, and all other religions, people give exorbitant amounts of money

53
00:03:14,000 --> 00:03:21,000
to, or seemingly, exorbitant amounts of money to priests and ministers and so on,

54
00:03:21,000 --> 00:03:24,000
without a second thought.

55
00:03:24,000 --> 00:03:28,000
There's people who think that's wasteful as well.

56
00:03:28,000 --> 00:03:35,000
But if you compare the two, the difference between giving 10% of your salary

57
00:03:35,000 --> 00:03:40,000
to a priest or a minister, and just giving food,

58
00:03:40,000 --> 00:03:45,000
like a portion of the food that you make for yourself anyway,

59
00:03:45,000 --> 00:03:51,000
or in the case of restaurants, a portion of the food that they have on hand,

60
00:03:51,000 --> 00:03:55,000
which really doesn't cost that much to them anyway,

61
00:03:55,000 --> 00:04:00,000
it's really putting it on that sort of a scale,

62
00:04:00,000 --> 00:04:08,000
it makes it seem like it's not really of any burden to the people.

63
00:04:08,000 --> 00:04:13,000
On the other hand, it's in fact a wonderful thing for them to do.

64
00:04:13,000 --> 00:04:17,000
It's charity, and supporting someone who you believe in.

65
00:04:17,000 --> 00:04:20,000
If you believe in a monk, if you believe that they're doing something good,

66
00:04:20,000 --> 00:04:23,000
then to give them something is a wonderful thing for you.

67
00:04:23,000 --> 00:04:25,000
I've done it before myself.

68
00:04:25,000 --> 00:04:30,000
I do it often when I see monks, I put food on my birthday every year.

69
00:04:30,000 --> 00:04:34,000
I try to give arms to a group of monks.

70
00:04:34,000 --> 00:04:38,000
It's something that makes you feel good, giving to people.

71
00:04:38,000 --> 00:04:42,000
The Buddha himself was very clear about this.

72
00:04:42,000 --> 00:04:46,000
He said the benefits of getting food are one day of food,

73
00:04:46,000 --> 00:04:51,000
but the benefits of giving food are immeasurable.

74
00:04:51,000 --> 00:04:56,000
Hundreds of thousands of times better because the benefit that you gain,

75
00:04:56,000 --> 00:04:58,000
you keep with you and you keep it in your heart.

76
00:04:58,000 --> 00:05:01,000
When you do something good, when you help someone,

77
00:05:01,000 --> 00:05:04,000
when you give something, you get a much greater benefit

78
00:05:04,000 --> 00:05:06,000
in the person who receives.

79
00:05:06,000 --> 00:05:10,000
The arms are something that I've done for my whole monks life.

80
00:05:10,000 --> 00:05:15,000
I've done it everywhere, I've done it in forests, I've done it in cities,

81
00:05:15,000 --> 00:05:19,000
I've done it on a mountain, going up and down the mountain.

82
00:05:19,000 --> 00:05:24,000
I've walked many, many miles each day,

83
00:05:24,000 --> 00:05:27,000
even today walking in the sun.

84
00:05:27,000 --> 00:05:31,000
This is, I think it's hard to say that monks are lazy,

85
00:05:31,000 --> 00:05:36,000
considering the rigid set of rules that we have to abide by.

86
00:05:36,000 --> 00:05:39,000
That's part of the reason why we abide by the rules

87
00:05:39,000 --> 00:05:45,000
because we acknowledge that we don't want to be caught up in the world.

88
00:05:45,000 --> 00:05:50,000
Yet, we have no right to expect anything.

89
00:05:50,000 --> 00:05:55,000
We have no right to expect the luxuries of people who live in the world as a result.

90
00:05:55,000 --> 00:06:01,000
Sometimes it looks like I'm living in luxury even as a monk,

91
00:06:01,000 --> 00:06:05,000
so you have a computer and so on.

92
00:06:05,000 --> 00:06:09,000
But there are so many rules that I have to abide by

93
00:06:09,000 --> 00:06:14,000
that make it really impossible for me to become caught up

94
00:06:14,000 --> 00:06:18,000
in these things or to become,

95
00:06:18,000 --> 00:06:21,000
to live a luxurious life.

96
00:06:21,000 --> 00:06:24,000
The clothes I own are the clothes that you see on me.

97
00:06:24,000 --> 00:06:26,000
I don't have a second set.

98
00:06:26,000 --> 00:06:28,000
The bed I sleep on is the floor.

99
00:06:28,000 --> 00:06:31,000
The food I eat, I eat one meal a day.

100
00:06:31,000 --> 00:06:39,000
I really think it's not much that you could point a finger at

101
00:06:39,000 --> 00:06:45,000
to say that, oh, look at the monkey's getting some free food every day.

102
00:06:45,000 --> 00:06:49,000
Because that's the only food I eat and it's been given out of faith.

103
00:06:49,000 --> 00:06:56,000
It's been given purely with no desire for anything in return.

104
00:06:56,000 --> 00:07:00,000
Simply for the sheer pleasure of being able to give something,

105
00:07:00,000 --> 00:07:04,000
being able to help someone, being able to support them for another day.

106
00:07:04,000 --> 00:07:11,000
The Buddha said it's like a honey bee that goes and pollinates the flower

107
00:07:11,000 --> 00:07:18,000
and takes the pollen, harms neither themselves nor the flower.

108
00:07:18,000 --> 00:07:27,000
The arms round is not something exorbitant where we're asking for anything huge or out of hand.

109
00:07:27,000 --> 00:07:32,000
But it's enough to keep us alive and it's something that keeps other people spiritually alive.

110
00:07:32,000 --> 00:07:37,000
It's something that's a support for their own spiritual development and a support for our spiritual development.

111
00:07:37,000 --> 00:07:42,000
So that's in brief what it means to go on arms round.

112
00:07:42,000 --> 00:07:47,000
I often have my students follow me on arms round so they can understand it and appreciate it much more.

113
00:07:47,000 --> 00:08:00,000
I've had people from countries around the world follow me on arms round and literally break down and cry when they see the wonderful

114
00:08:00,000 --> 00:08:09,000
heartfelt gesture given by people, sometimes people who don't have much at all.

115
00:08:09,000 --> 00:08:15,000
I've been to places where people are themselves living in difficult situations

116
00:08:15,000 --> 00:08:23,000
and yet they always give something, give a little bit, something that's the food that they give is not a lot.

117
00:08:23,000 --> 00:08:27,000
The food that they have is not a lot but they want to share.

118
00:08:27,000 --> 00:08:31,000
They feel great and they feel happy and they're smiling.

119
00:08:31,000 --> 00:08:35,000
It's really a wonderful thing so I wouldn't change it.

120
00:08:35,000 --> 00:08:39,000
I myself am a generous person, I don't feel ashamed to say that.

121
00:08:39,000 --> 00:08:44,000
So I don't feel bad when I receive the gifts from other people because I myself give gifts.

122
00:08:44,000 --> 00:08:46,000
I don't think there's anything.

123
00:08:46,000 --> 00:08:52,000
I just somehow think that people often misunderstand or it's a foreign thing.

124
00:08:52,000 --> 00:08:54,000
This is why I'm being stopped by the police.

125
00:08:54,000 --> 00:08:57,000
So I think it's good to explain these sort of things.

126
00:08:57,000 --> 00:09:06,000
I hope that helped to clear some things up and make it clear a little bit more, a little bit more clear about what I'm doing and how I live my life.

127
00:09:06,000 --> 00:09:29,000
So that's the latest in this life in the day of a month.

