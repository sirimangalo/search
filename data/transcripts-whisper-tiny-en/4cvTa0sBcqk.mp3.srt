1
00:00:00,000 --> 00:00:12,000
There's a video where you talk about how to become and avoid anxiety, and you say that the important thing is how you react, but does this help remove the anxiety from our heads?

2
00:00:12,000 --> 00:00:14,000
Yes.

3
00:00:14,000 --> 00:00:15,000
Yes.

4
00:00:15,000 --> 00:00:29,000
The important thing is to separate the anxiety from the result, the resultant physical manifestations of anxiety.

5
00:00:29,000 --> 00:00:39,000
The manifestations of anxiety, the butterflies in your stomach, the tension in your shoulders, and the headache, and the fluttering of the heart, the beating of the heart, and so on.

6
00:00:39,000 --> 00:00:49,000
All of that is not anxiety, and that's really the problem even with meditation, because you'll say to yourself anxious, anxious, and the effects don't disappear.

7
00:00:49,000 --> 00:00:54,000
You still have butterflies in your stomach, you still have a heart beating fast and so on.

8
00:00:54,000 --> 00:01:00,000
But, and so as a result, you think it did nothing, and that you get anxious again, you say, oh no, it's not working.

9
00:01:00,000 --> 00:01:05,000
Oh no, I'm still anxious, and then you get more and more anxious, and it's snowballs.

10
00:01:05,000 --> 00:01:07,000
This is what it does if you're not meditating.

11
00:01:07,000 --> 00:01:11,000
You feel the effects in the body, you get more anxious, you get anxious, and the effects in the body get worse.

12
00:01:11,000 --> 00:01:17,000
You feel the effects in the body, and you get totally paralyzed, as a result.

13
00:01:17,000 --> 00:01:33,000
When you focus on the anxiety and focus on the effects of the body objectively, seeing them for what they are, you don't need the bodily manifestations to disappear.

14
00:01:33,000 --> 00:01:35,000
This is something that we don't get.

15
00:01:35,000 --> 00:01:40,000
You can be totally calm in the mind, and have your body freaking out.

16
00:01:40,000 --> 00:01:44,000
This is logically logical.

17
00:01:44,000 --> 00:01:53,000
So, you don't have to worry about what's going on in my heart is beating very fast, I must be anxious.

18
00:01:53,000 --> 00:01:56,000
Because, no, you can't stop that.

19
00:01:56,000 --> 00:02:00,000
It's something that's going to take a few minutes to calm down.

20
00:02:00,000 --> 00:02:06,000
But, immediately, you can do away with the anxiety, and it's important to see that, and it's important to make the distinction.

21
00:02:06,000 --> 00:02:10,000
Because, you know, I've given talks before where my heart was beating really, really fast.

22
00:02:10,000 --> 00:02:15,000
And, you know, I had butterflies in this stomach. I was really, one might say anxious.

23
00:02:15,000 --> 00:02:23,000
But, where I had caught the anxiety, and was acknowledging the feelings in the body, was clearly aware of them, and gave a great talk.

24
00:02:23,000 --> 00:02:33,000
And, was perfectly clear, and perfectly common, and happy, even though my voice was even shaking, because of the flustering in the body.

25
00:02:33,000 --> 00:02:45,000
But, that's really funny, which is going along with what you're saying, is what I've noticed, and is, like, myself and other people say,

26
00:02:45,000 --> 00:02:48,000
but I'm not supposed to be feeling like this.

27
00:02:48,000 --> 00:02:49,000
Right.

28
00:02:49,000 --> 00:02:53,000
Because, what do you mean, you're not supposed to be feeling like this?

29
00:02:53,000 --> 00:02:55,000
You know, this is how you're feeling.

30
00:02:55,000 --> 00:02:58,000
You know, it's like, with lifelong depression.

31
00:02:58,000 --> 00:03:06,000
You know, they're trying to always find that drug, or that therapy that's going to cure them.

32
00:03:06,000 --> 00:03:18,000
But, the whole, you know, maybe if they were to just realize, oh, this is the way I've always felt, maybe they can adjust with it better, and go on.

33
00:03:18,000 --> 00:03:23,000
Actually, that's what has taken place with myself.

34
00:03:23,000 --> 00:03:28,000
Oh, is, oh, but I'm not supposed to be feeling like this.

35
00:03:28,000 --> 00:03:35,000
I'm not, and my own sister had to say, hey, gummy, you've been feeling like this since you've been two years old, so have I.

36
00:03:35,000 --> 00:03:37,000
We inherited it from our parents.

37
00:03:37,000 --> 00:03:38,000
It's genetic.

38
00:03:38,000 --> 00:03:45,000
And it's like, oh, you mean, so I am supposed to be feeling like this?

39
00:03:45,000 --> 00:03:46,000
Yeah.

40
00:03:46,000 --> 00:03:51,000
And it's like, oh, okay, well, I'm going to choose to start feeling other things.

41
00:03:51,000 --> 00:03:57,000
And it, it allowed me that opening to acknowledging it.

42
00:03:57,000 --> 00:04:01,000
It's like, you know, the idea of like, oh, you're not supposed to feel pain.

43
00:04:01,000 --> 00:04:04,000
What do you mean, and I'm supposed to feel pain?

44
00:04:04,000 --> 00:04:08,000
The more you're saying that, the more you're going to feel the pain, obviously.

45
00:04:08,000 --> 00:04:19,000
And, you know, when it comes to like physical pain, we all know that when you start not resisting it, but settle into it, allow yourself to acknowledge it,

46
00:04:19,000 --> 00:04:24,000
you realize that the pain starts alleviating itself on its own.

47
00:04:24,000 --> 00:04:35,000
And, you know, I just had to make that comment there because when you can sit back and just say, oh, the chaos going on around me.

48
00:04:35,000 --> 00:04:38,000
Well, that's actually normal.

49
00:04:38,000 --> 00:04:40,000
That's Sam Sara.

50
00:04:40,000 --> 00:04:48,000
Okay, let me, let me just work on not thinking that this is all out of order.

51
00:04:48,000 --> 00:04:52,000
No, I just have to direct my own mind and just be calm in it.

52
00:04:52,000 --> 00:04:54,000
Because really, that's the case.

53
00:04:54,000 --> 00:04:56,000
We're living in chaos constantly.

54
00:04:56,000 --> 00:05:06,000
Whether it's internal or external, whether it's our own perception of our own pain or the perception of out around us.

55
00:05:06,000 --> 00:05:13,000
Maybe for a moment there we have that little bubble of where we feel everything's all right for them first.

56
00:05:13,000 --> 00:05:28,000
And part of the joy of practicing dharma is to go with those ups and downs and the burst and the depths of all these different experiences.

57
00:05:28,000 --> 00:05:33,000
I probably said a little too much, but I just had to.

58
00:05:33,000 --> 00:05:34,000
No, that's exactly.

59
00:05:34,000 --> 00:05:36,000
I'm with you there.

60
00:05:36,000 --> 00:05:46,000
And I think one of the best example of this that I can think of, especially as it relates to anxiety is insomnia.

61
00:05:46,000 --> 00:05:50,000
People are only in Somniac because they want to sleep.

62
00:05:50,000 --> 00:05:54,000
It's kind of, you know, that's obvious, right?

63
00:05:54,000 --> 00:05:56,000
Because I should be sleeping.

64
00:05:56,000 --> 00:05:57,000
I should be sleeping.

65
00:05:57,000 --> 00:05:58,000
Exactly.

66
00:05:58,000 --> 00:05:59,000
It's the worst.

67
00:05:59,000 --> 00:06:00,000
It's the worst form.

68
00:06:00,000 --> 00:06:03,000
I used to be in Somniac and, you know, I'd be up until 3 a.m.

69
00:06:03,000 --> 00:06:08,000
because I wanted to sleep because I thought I should be sleeping.

70
00:06:08,000 --> 00:06:13,000
Buddhism, the one of the first things, the quickest things that Buddhism is able to cure, you know,

71
00:06:13,000 --> 00:06:18,000
as far as things that need to be cured is insomnia.

72
00:06:18,000 --> 00:06:25,000
It's something that you, you know, all it takes is for the teacher to say, you know, you don't need to sleep.

73
00:06:25,000 --> 00:06:31,000
You would be far better off if you were to stay awake all night and be mindful, so do it.

74
00:06:31,000 --> 00:06:38,000
And, you know, I mean, it comes into play, especially when they do an intensive meditation course.

75
00:06:38,000 --> 00:06:43,000
But even people in daily life, when you tell them, well, then don't sleep.

76
00:06:43,000 --> 00:06:45,000
Stay up all night and meditate.

77
00:06:45,000 --> 00:06:46,000
And you say, good for you.

78
00:06:46,000 --> 00:06:48,000
I'm so, you know, so happy for you.

79
00:06:48,000 --> 00:06:55,000
I have to argue with my students to get them down to 6 hours of sleep at night.

80
00:06:55,000 --> 00:06:58,000
Oh, I could never do 6 hours of sleep.

81
00:06:58,000 --> 00:07:01,000
And then these insomnia acts for what I get 2-3 hours.

82
00:07:01,000 --> 00:07:02,000
And I say, great, good for you.

83
00:07:02,000 --> 00:07:04,000
More time for meditation.

84
00:07:04,000 --> 00:07:06,000
Meditate all night.

85
00:07:06,000 --> 00:07:10,000
And immediately, of course, they fall asleep and have no problems at all.

86
00:07:14,000 --> 00:07:19,000
But the idea that you just sleep more and so on, that you should be asleep.

87
00:07:19,000 --> 00:07:20,000
I thought that's...

88
00:07:20,000 --> 00:07:21,000
That conflict.

89
00:07:21,000 --> 00:07:22,000
Yeah.

90
00:07:22,000 --> 00:07:23,000
Stress.

91
00:07:23,000 --> 00:07:24,000
I mean, anxiety.

92
00:07:24,000 --> 00:07:29,000
It's pretty certainly as someone as to this and says,

93
00:07:29,000 --> 00:07:33,000
someone saying, can you talk about anxiety and how to deal with it, which I think we've done?

94
00:07:33,000 --> 00:07:38,000
And the second one, right above it, says, anxiety and insecurity.

95
00:07:38,000 --> 00:07:41,000
Insecurity, it might be an interesting topic.

96
00:07:41,000 --> 00:07:42,000
That's a good one.

97
00:07:42,000 --> 00:07:43,000
Yeah.

98
00:07:43,000 --> 00:07:45,000
It's a little bit different than anxiety, isn't it?

99
00:07:45,000 --> 00:07:46,000
It's...

100
00:07:46,000 --> 00:07:47,000
It's a...

101
00:07:47,000 --> 00:07:48,000
It's a...

102
00:07:48,000 --> 00:07:49,000
It's a...

103
00:07:49,000 --> 00:07:53,000
I don't know if it's a Western world problem largely, but there's a lot of insecurity I find in social circles that I run in.

104
00:07:53,000 --> 00:07:54,000
Yeah.

105
00:07:54,000 --> 00:07:56,000
I'm feeling that you are not...

106
00:07:56,000 --> 00:08:00,000
You're inadequate or worrying that you might be inadequate.

107
00:08:00,000 --> 00:08:01,000
Right.

108
00:08:01,000 --> 00:08:02,000
Yeah.

109
00:08:02,000 --> 00:08:07,000
Remembering things that you've done and, oh, I was such an idiot to do that.

110
00:08:07,000 --> 00:08:08,000
And...

111
00:08:08,000 --> 00:08:09,000
Yeah.

112
00:08:09,000 --> 00:08:13,000
You know, what they must think of me and so on.

113
00:08:13,000 --> 00:08:17,000
Where so much harder on ourselves than other people are, aren't we?

114
00:08:17,000 --> 00:08:20,000
You know, you've ever been to one of those situations where you just did something you thought must have...

115
00:08:20,000 --> 00:08:23,000
You know, everyone must think you're an idiot.

116
00:08:23,000 --> 00:08:26,000
And you just really ruined your relationship with everyone.

117
00:08:26,000 --> 00:08:28,000
And no one else really noticed.

118
00:08:28,000 --> 00:08:30,000
And they're kind of like, oh, yeah, whatever.

119
00:08:30,000 --> 00:08:32,000
And we're like killing ourselves.

120
00:08:32,000 --> 00:08:33,000
Yeah.

121
00:08:33,000 --> 00:08:34,000
Yeah.

122
00:08:34,000 --> 00:08:35,000
Exactly.

123
00:08:35,000 --> 00:08:36,000
We're thinking about ourselves, they're thinking...

124
00:08:36,000 --> 00:08:37,000
So they don't really care.

125
00:08:37,000 --> 00:08:38,000
It's an interesting thing that...

126
00:08:38,000 --> 00:08:39,000
That...

127
00:08:39,000 --> 00:08:40,000
That's gent...

128
00:08:40,000 --> 00:08:41,000
Tends to be the case.

129
00:08:41,000 --> 00:08:44,000
Are insecurities thinking, you know, how are they looking at us?

130
00:08:44,000 --> 00:08:45,000
They must really be judging us.

131
00:08:45,000 --> 00:08:46,000
People don't care.

132
00:08:46,000 --> 00:08:48,000
People don't really judge you.

133
00:08:48,000 --> 00:08:49,000
They judge you for a second.

134
00:08:49,000 --> 00:08:51,000
And then they go back to their own problems.

135
00:08:51,000 --> 00:08:53,000
Because they got lots of them.

136
00:08:53,000 --> 00:08:56,000
So how do we deal with insecurity?

137
00:08:56,000 --> 00:08:57,000
Is it just a...

138
00:08:57,000 --> 00:08:59,000
Like, almost like an insomnia?

139
00:08:59,000 --> 00:09:02,000
Is it a similar situation where it's okay to feel insecure?

140
00:09:02,000 --> 00:09:03,000
Or...

141
00:09:03,000 --> 00:09:04,000
Not an approach.

142
00:09:04,000 --> 00:09:05,000
Yeah.

143
00:09:05,000 --> 00:09:06,000
I mean, obviously you do, but I think it's much more difficult.

144
00:09:06,000 --> 00:09:08,000
Because it has to do with ego.

145
00:09:08,000 --> 00:09:10,000
Insecurities, you really have to...

146
00:09:10,000 --> 00:09:12,000
Insecurities, the core.

147
00:09:12,000 --> 00:09:15,000
It's part of the core, which is to give up self.

148
00:09:15,000 --> 00:09:20,000
Until you can be totally fine with being an idiot.

149
00:09:20,000 --> 00:09:24,000
You'll never be free from suffering.

150
00:09:24,000 --> 00:09:30,000
Until you can say something that just really makes you look stupid.

151
00:09:30,000 --> 00:09:32,000
And be fine with that.

152
00:09:32,000 --> 00:09:35,000
Because my teacher once did that.

153
00:09:35,000 --> 00:09:38,000
He said something that was really wrong.

154
00:09:38,000 --> 00:09:41,000
And, you know, it didn't face him.

155
00:09:41,000 --> 00:09:43,000
He was like, yes.

156
00:09:43,000 --> 00:09:46,000
Oh, yes, sorry, that was wrong.

157
00:09:46,000 --> 00:09:48,000
And just went on with it.

158
00:09:48,000 --> 00:09:52,000
Well, I got no problem saying stupid things.

159
00:09:52,000 --> 00:09:57,000
Of course, I guess we would have to say, you know,

160
00:09:57,000 --> 00:09:59,000
based on how you said that, it's kind of like a joke.

161
00:09:59,000 --> 00:10:02,000
I wonder if it's possible to be saying lots of stupid...

162
00:10:02,000 --> 00:10:04,000
You know, there are people who say lots of stupid things

163
00:10:04,000 --> 00:10:06,000
and are fine with it and aren't in life.

164
00:10:06,000 --> 00:10:07,000
Right.

165
00:10:07,000 --> 00:10:09,000
That's different, is it?

166
00:10:09,000 --> 00:10:10,000
Yeah.

167
00:10:10,000 --> 00:10:13,000
Well, but they don't have insecurity.

168
00:10:13,000 --> 00:10:15,000
There are people who...

169
00:10:15,000 --> 00:10:17,000
But that's not true, you know.

170
00:10:17,000 --> 00:10:21,000
Some people who, you know, I'm thinking of jokers,

171
00:10:21,000 --> 00:10:24,000
for example, people who just class clowns and so on,

172
00:10:24,000 --> 00:10:25,000
who thrive off of it.

173
00:10:25,000 --> 00:10:30,000
You know, deep down, are they really secure?

174
00:10:30,000 --> 00:10:33,000
An interesting topic, interesting question.

175
00:10:33,000 --> 00:10:34,000
That's interesting.

176
00:10:34,000 --> 00:10:36,000
Because I'm very much like...

177
00:10:36,000 --> 00:10:38,000
Sorry, what does it work?

178
00:10:38,000 --> 00:10:42,000
It's kind of like had that being in like school still.

179
00:10:42,000 --> 00:10:47,000
A lot of people, like around like this age are very, very insecure.

180
00:10:47,000 --> 00:10:52,000
Like self-conscious, like just what every little thing they do.

181
00:10:52,000 --> 00:10:55,000
And they're always thinking, like, what do people think of me?

182
00:10:55,000 --> 00:10:57,000
How do I look right now?

183
00:10:57,000 --> 00:11:00,000
And I mean, those stocks come to my mind as well.

184
00:11:00,000 --> 00:11:05,000
And I think like a fun, easy way to deal with that is just focus

185
00:11:05,000 --> 00:11:07,000
on what you're doing at the moment.

186
00:11:07,000 --> 00:11:09,000
You're thinking, it's really what you're doing.

187
00:11:09,000 --> 00:11:11,000
You're overthinking actually.

188
00:11:11,000 --> 00:11:13,000
And what else are you doing?

189
00:11:13,000 --> 00:11:15,000
Walking, sitting, whatever else?

190
00:11:15,000 --> 00:11:20,000
I think that's a good way to deal with being insecure or self-conscious.

191
00:11:20,000 --> 00:11:26,000
Because in reality, you're making a big story about what other people think of you.

192
00:11:26,000 --> 00:11:29,000
And really, yeah, they do have their own problems.

193
00:11:29,000 --> 00:11:34,000
They're, of course, like, you'll see it as you meditate more.

194
00:11:34,000 --> 00:11:39,000
That people really don't care about other people at all.

195
00:11:39,000 --> 00:11:42,000
They care more about...

196
00:11:42,000 --> 00:11:43,000
It's not so true.

197
00:11:43,000 --> 00:11:47,000
Some people are very, very altruistic and empathic.

198
00:11:47,000 --> 00:11:48,000
Some people actually care.

199
00:11:48,000 --> 00:11:49,000
Some people are.

200
00:11:49,000 --> 00:11:51,000
Yeah, but they don't care.

201
00:11:51,000 --> 00:11:55,000
People aren't even caring even about themselves.

202
00:11:55,000 --> 00:12:00,000
What they're doing is they're just perpetuating this myth in their mind.

203
00:12:00,000 --> 00:12:04,000
I mean, part of their myth could even be masochism.

204
00:12:04,000 --> 00:12:08,000
So we're constantly putting themselves down.

205
00:12:08,000 --> 00:12:14,000
So the idea that they're really caring about themselves, well, the term care,

206
00:12:14,000 --> 00:12:22,000
maybe that's not the case, but they're definitely constantly playing this mythology.

207
00:12:22,000 --> 00:12:27,000
And in that mythology, well, of course, other people really don't matter

208
00:12:27,000 --> 00:12:30,000
because they're not really dealing with the real person.

209
00:12:30,000 --> 00:12:36,000
They're dealing with the image of that person in contact in their myth of that.

210
00:12:36,000 --> 00:12:39,000
You're my daughter, you're my son, you're my mother.

211
00:12:39,000 --> 00:12:44,000
But very rarely are they saying, oh, I'm a person and you're a person.

212
00:12:44,000 --> 00:12:49,000
It's always in contact with this fabrication of how they want to create reality

213
00:12:49,000 --> 00:12:53,000
as opposed to natural order of reality.

214
00:12:53,000 --> 00:12:58,000
It's interesting too, we tell them we're caring in security,

215
00:12:58,000 --> 00:13:00,000
and they don't care.

216
00:13:00,000 --> 00:13:03,000
In some situations where you find something really does care,

217
00:13:03,000 --> 00:13:05,000
it helps with insecurity.

218
00:13:05,000 --> 00:13:08,000
In fact, it doesn't seem to have as much love around.

219
00:13:08,000 --> 00:13:12,000
And it seems like if you care as much as someone might not circumstance,

220
00:13:12,000 --> 00:13:15,000
it also seems to have an effect on insecurity.

221
00:13:15,000 --> 00:13:17,000
It doesn't seem to remove the purpose of that.

222
00:13:17,000 --> 00:13:22,000
But you have that person that comes in your life and they really, really care

223
00:13:22,000 --> 00:13:26,000
what your response, what the hell do they want from me?

224
00:13:26,000 --> 00:13:27,000
Sometimes.

225
00:13:27,000 --> 00:13:30,000
Well, what do they really want?

226
00:13:30,000 --> 00:13:35,000
I don't like them because they're too direct.

227
00:13:35,000 --> 00:13:37,000
They're too honest to me.

228
00:13:37,000 --> 00:13:42,000
I fear that.

229
00:13:42,000 --> 00:13:47,000
That's a common response, I've seen.

230
00:13:47,000 --> 00:13:56,000
The insecurity is in a part of a symptom of anxiety.

231
00:13:56,000 --> 00:13:59,000
No, I think it has ego more.

232
00:13:59,000 --> 00:14:02,000
I think it's very much of anxiety.

233
00:14:02,000 --> 00:14:04,000
What do you think about me?

234
00:14:04,000 --> 00:14:06,000
Oh, I feel that.

235
00:14:06,000 --> 00:14:09,000
What can you think about me?

236
00:14:09,000 --> 00:14:11,000
I suffer from anxiety.

237
00:14:11,000 --> 00:14:13,000
Very far from the anxiety.

238
00:14:13,000 --> 00:14:18,000
But I think the insecurity comes first and the anxiety comes after

239
00:14:18,000 --> 00:14:21,000
or the insecurity is talking about something bigger than anxiety.

240
00:14:21,000 --> 00:14:24,000
Much more ego-based.

241
00:14:24,000 --> 00:14:29,000
And the real problem there is, as Lu said it much better than I could,

242
00:14:29,000 --> 00:14:34,000
it's the miss that we're playing.

243
00:14:34,000 --> 00:14:36,000
It's all just whether you care about it.

244
00:14:36,000 --> 00:14:39,000
I don't think I was totally correct to say that no one cares.

245
00:14:39,000 --> 00:14:42,000
We don't normally care more about ourselves and others.

246
00:14:42,000 --> 00:14:45,000
Or we're more focused on it because some people are not.

247
00:14:45,000 --> 00:14:50,000
But regardless, as Lu said, if they're focused on their cells,

248
00:14:50,000 --> 00:14:55,000
focused on others, the problem is simply the myth.

249
00:14:55,000 --> 00:14:59,000
It's that we don't relate to each other as we really are.

250
00:14:59,000 --> 00:15:05,000
I was talking to data one of the meditators and she was just asking about something

251
00:15:05,000 --> 00:15:15,000
just curious off hand about this seeming kind of special power that she had to see or us.

252
00:15:15,000 --> 00:15:19,000
So she could look at someone and she could tell it wasn't their emotions,

253
00:15:19,000 --> 00:15:21,000
but she explained it.

254
00:15:21,000 --> 00:15:26,000
One example was how she was able to tell a person's fear of influence.

255
00:15:26,000 --> 00:15:28,000
How basically how powerful a person was.

256
00:15:28,000 --> 00:15:30,000
How big they were.

257
00:15:30,000 --> 00:15:33,000
She got the sense of a person being big and a person being small.

258
00:15:33,000 --> 00:15:36,000
I've heard a monk tell me the same thing in Thailand.

259
00:15:36,000 --> 00:15:39,000
He said he was able to see some people just filled the room.

260
00:15:39,000 --> 00:15:42,000
When they walked into the room, the whole room was full.

261
00:15:42,000 --> 00:15:46,000
And other people just seemed very, very small to them.

262
00:15:46,000 --> 00:15:51,000
And so she was asking about this in general and how it relates to what we're talking about.

263
00:15:51,000 --> 00:16:00,000
The answer I gave was the idea that it doesn't matter whether you can see people's state of mind,

264
00:16:00,000 --> 00:16:04,000
their emotions or who they are, because that's only who they are at that moment.

265
00:16:04,000 --> 00:16:08,000
And I said to her, well, you're going through this intensive question.

266
00:16:08,000 --> 00:16:10,000
You can see your own mind.

267
00:16:10,000 --> 00:16:12,000
It's not one train.

268
00:16:12,000 --> 00:16:15,000
It's not one coherent message.

269
00:16:15,000 --> 00:16:20,000
Who you are changes from moment to moment and in totally chaotic ways.

270
00:16:20,000 --> 00:16:26,000
So you might have built up this Sankara, this formation, this state, this habit.

271
00:16:26,000 --> 00:16:30,000
And you might have a totally contradictory habit.

272
00:16:30,000 --> 00:16:32,000
Evil people can suddenly do wonderful things.

273
00:16:32,000 --> 00:16:34,000
Why is that?

274
00:16:34,000 --> 00:16:46,000
So I guess the point is just how important it is to shift our understanding of beings.

275
00:16:46,000 --> 00:16:47,000
Right.

276
00:16:47,000 --> 00:16:55,000
And to point out how different it is, our ideas of people and the subsequent insecurity

277
00:16:55,000 --> 00:17:01,000
comes from thinking of people as people or as immutable entities, including ourselves,

278
00:17:01,000 --> 00:17:09,000
like I am this sort of person, to seeing the things as a moment to moment,

279
00:17:09,000 --> 00:17:15,000
as aggregates of moment to moment experience and to see people in that way.

280
00:17:15,000 --> 00:17:20,000
As it relates to listening to the person carefully and not having an idea of the person,

281
00:17:20,000 --> 00:17:25,000
like when I can rule it, or yourself spoke with my other child daughter.

282
00:17:25,000 --> 00:17:26,000
For sure.

283
00:17:26,000 --> 00:17:27,000
Is that related?

284
00:17:27,000 --> 00:17:31,000
I think so.

285
00:17:31,000 --> 00:17:36,000
Because the reason for listening closely should be that you know,

286
00:17:36,000 --> 00:17:39,000
you understand this isn't the same person that they were last time.

287
00:17:39,000 --> 00:17:42,000
I can't rely on my idea of...

288
00:17:42,000 --> 00:17:43,000
Oh, yes.

289
00:17:43,000 --> 00:17:45,000
Yeah.

290
00:17:45,000 --> 00:17:47,000
They could be totally different at this time and we are.

291
00:17:47,000 --> 00:17:56,000
This is something that is very under-appreciated how changeable we really are.

292
00:17:56,000 --> 00:17:59,000
How we could come up with something totally to the person who you see,

293
00:17:59,000 --> 00:18:01,000
oh, this is this person and you expect them to act in a certain way.

294
00:18:01,000 --> 00:18:08,000
And they may suddenly come up with some crazy past life habit that they've never exhibited before.

295
00:18:08,000 --> 00:18:09,000
They may.

296
00:18:09,000 --> 00:18:15,000
Of course, talking about past lives, which of course is taboo, but I don't care.

297
00:18:15,000 --> 00:18:19,000
That's what I want to know.

298
00:18:19,000 --> 00:18:22,000
That's your past life talking.

299
00:18:22,000 --> 00:18:24,000
One of them.

300
00:18:24,000 --> 00:18:25,000
One of them.

301
00:18:25,000 --> 00:18:29,000
Okay, so did we get anxiety?

302
00:18:29,000 --> 00:18:31,000
Are we all anxiety doubt?

303
00:18:31,000 --> 00:18:54,000
I'm going to quit.

