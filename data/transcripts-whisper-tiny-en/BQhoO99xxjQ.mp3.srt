1
00:00:00,000 --> 00:00:11,000
Hello and welcome to Monk Radio. This is August 19th, 2012 and these are the announcements for this week.

2
00:00:11,000 --> 00:00:22,000
First announcement is I have two wonderful two. Last week I had one. This week I have not one but two assistants.

3
00:00:22,000 --> 00:00:28,000
So welcome Desmond and Kapila who are in America in Japan.

4
00:00:28,000 --> 00:00:32,000
And they'll be helping to read the questions off.

5
00:00:32,000 --> 00:00:38,000
What I think what we'll do is have you take turns, so one person read one, the other person read the next.

6
00:00:38,000 --> 00:00:43,000
Save me from having to ask an answer as well.

7
00:00:43,000 --> 00:00:51,000
And then you're also welcome to join in and comment and even answer the questions as you feel comfortable.

8
00:00:51,000 --> 00:00:57,000
Next announcement is that we've added a Saturday to our broadcast schedule.

9
00:00:57,000 --> 00:01:02,000
Many of you are already aware and already took part in yesterday's session.

10
00:01:02,000 --> 00:01:11,000
But every week now on Saturday at the same time as this broadcast which is 2pm UTC

11
00:01:11,000 --> 00:01:18,000
or 2pm green which mean time I think is what it used to be called.

12
00:01:18,000 --> 00:01:22,000
We're going to have a Dhamma group online live broadcast.

13
00:01:22,000 --> 00:01:33,000
So it's going to be a group of people in our community who have been added to my elite circles.

14
00:01:33,000 --> 00:01:40,000
Now the people who have requested to be added to my circles and who I think would be a good part of the community.

15
00:01:40,000 --> 00:01:45,000
Not causing trouble and disturbance.

16
00:01:45,000 --> 00:01:52,000
People who have webcams, people who have fast internet connections.

17
00:01:52,000 --> 00:01:56,000
And people who are interested in sharing the Dhamma.

18
00:01:56,000 --> 00:02:02,000
So in these people we had yesterday six or seven people.

19
00:02:02,000 --> 00:02:05,000
I think most we had seven.

20
00:02:05,000 --> 00:02:09,000
A couple of them had slow internet connections so it didn't work out the best.

21
00:02:09,000 --> 00:02:15,000
For the most part it was very successful without much of a hitch.

22
00:02:15,000 --> 00:02:20,000
We have a agenda that we're following.

23
00:02:20,000 --> 00:02:29,000
We are sharing Dhamma that we've discovered over the week from the internet or from wherever.

24
00:02:29,000 --> 00:02:39,000
We're reading texts where taking presents which we should be doing tonight.

25
00:02:39,000 --> 00:02:42,000
We should have done that first, we'll do that after this.

26
00:02:42,000 --> 00:02:49,000
And meditating together and then having a tea time at the end.

27
00:02:49,000 --> 00:02:57,000
We had it yesterday in the form of chat at the end where we sit around and drink tea as though we were all sitting together.

28
00:02:57,000 --> 00:03:02,000
And that's broadcast lives if you want to just watch or welcome to come and watch as you're watching today.

29
00:03:02,000 --> 00:03:06,000
That would have been yesterday and will be the same time next week.

30
00:03:06,000 --> 00:03:08,000
So that's another announcement.

31
00:03:08,000 --> 00:03:14,000
The final announcement that I can think of is just a quick announcement that

32
00:03:14,000 --> 00:03:21,000
looking at my YouTube channel it looks like I'm about to hit 2 million views.

33
00:03:21,000 --> 00:03:28,000
It actually is kind of what I think about it doesn't mean so much to me,

34
00:03:28,000 --> 00:03:35,000
but I just thought it was a landmark that I should let everyone know.

35
00:03:35,000 --> 00:03:41,000
Thanks everyone for your interest and for getting involved in sharing Dhamma.

36
00:03:41,000 --> 00:03:48,000
If it weren't for so many people watching the videos and expressing their appreciation,

37
00:03:48,000 --> 00:03:51,000
probably would have stopped a long time ago.

38
00:03:51,000 --> 00:03:57,000
So thank you and here's to another million views.

39
00:03:57,000 --> 00:03:59,000
The next million views.

40
00:03:59,000 --> 00:04:01,000
Okay, that's all the announcements I have.

41
00:04:01,000 --> 00:04:03,000
Does anyone Larry also joined us now?

42
00:04:03,000 --> 00:04:04,000
Welcome Larry.

43
00:04:04,000 --> 00:04:06,000
He was there yesterday as well.

44
00:04:06,000 --> 00:04:11,000
Does anyone else have anything they'd like to announce?

45
00:04:11,000 --> 00:04:16,000
This isn't really going to be a group discussion so we're going to try to skip right ahead to

46
00:04:16,000 --> 00:04:20,000
taking people's comments and questions.

47
00:04:20,000 --> 00:04:47,000
So that's all for today.

