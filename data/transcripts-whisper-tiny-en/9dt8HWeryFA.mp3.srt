1
00:00:00,000 --> 00:00:10,320
Hi, in this video I'll be talking about some of the ways in which we can incorporate

2
00:00:10,320 --> 00:00:13,920
the meditation practice into our daily life.

3
00:00:13,920 --> 00:00:18,640
First of all, the first important thing to explain in terms of incorporating the meditation

4
00:00:18,640 --> 00:00:23,560
practice into our daily life are those things which it's going to be important for us

5
00:00:23,560 --> 00:00:29,240
to abstain from if our meditation practice is to be effective and is to have good results

6
00:00:29,240 --> 00:00:30,800
in our daily life.

7
00:00:30,800 --> 00:00:36,120
As we mentioned in the beginning, meditation is an equivalent to medication.

8
00:00:36,120 --> 00:00:39,800
And as we know with medication, there are always certain things which you can't take in

9
00:00:39,800 --> 00:00:46,120
conjunction with the medication, some certain things which when taken together with the medication

10
00:00:46,120 --> 00:00:55,760
will cause the medication to be ineffective or will be counter indicative to the results

11
00:00:55,760 --> 00:00:58,320
which we're hoping to get from the medication.

12
00:00:58,320 --> 00:01:01,440
So certain things when we take medication, there's always certain things we have to take

13
00:01:01,440 --> 00:01:06,000
out of our diet, but with meditation it's very similar.

14
00:01:06,000 --> 00:01:10,040
Meditation is something which is to bring about a certain state of clarity or a natural

15
00:01:10,040 --> 00:01:17,600
state of purity in the mind, to bring our minds back to this state of clarity and sobriety,

16
00:01:17,600 --> 00:01:21,200
which is free from suffering.

17
00:01:21,200 --> 00:01:25,040
So then there are certain actions, certain things which we can do by way of body or by

18
00:01:25,040 --> 00:01:30,280
way of speech which are going to be counter indicative, which are going to have the opposite

19
00:01:30,280 --> 00:01:35,280
effect, which are going to create a more clouded state of mind, which are going to bring

20
00:01:35,280 --> 00:01:40,320
about high states of anger, high states of addiction, high states of delusion, high states

21
00:01:40,320 --> 00:01:44,840
of clouded mind states.

22
00:01:44,840 --> 00:01:48,920
So these things we're going to have to take out of our diet, so to speak, if our

23
00:01:48,920 --> 00:01:51,240
meditation is to be effective.

24
00:01:51,240 --> 00:01:57,480
The first thing that we have to take out is not to kill, we have to make a promise to ourselves

25
00:01:57,480 --> 00:02:02,880
that we're not going to kill living beings, not to kill living ants or mosquitoes or any

26
00:02:02,880 --> 00:02:13,440
sort of living being, which breeds and which is in and of itself, doesn't wish to die.

27
00:02:13,440 --> 00:02:15,960
Number two, not to steal.

28
00:02:15,960 --> 00:02:21,400
So for meditation is to be effective, we have to be able to respect the possessions of other

29
00:02:21,400 --> 00:02:24,920
people and not to take things without permission.

30
00:02:24,920 --> 00:02:30,240
Number three, not to commit adultery or sexual misconduct to get involved in romantic relationships

31
00:02:30,240 --> 00:02:37,880
or sexual conduct, which is emotionally or spiritually damaging to other people.

32
00:02:37,880 --> 00:02:43,520
Number four, not to tell lies, not to deceive other people and take them away from their

33
00:02:43,520 --> 00:02:45,680
reality.

34
00:02:45,680 --> 00:02:50,800
And number five, not to take drugs or alcohol, and these are not to take anything really

35
00:02:50,800 --> 00:02:56,200
which is going to be intoxicating for our minds, things which create states of insubriety,

36
00:02:56,200 --> 00:03:00,320
take us away from our natural, pure, clear state of mind.

37
00:03:00,320 --> 00:03:06,360
These things are, it's very important that we actually abstain from them completely,

38
00:03:06,360 --> 00:03:09,960
if our meditation practices to be successful.

39
00:03:09,960 --> 00:03:15,400
There are certain other things which can be undertaken, can be partaken of, but must be

40
00:03:15,400 --> 00:03:19,600
partaken of in moderation if our meditation is to be successful.

41
00:03:19,600 --> 00:03:25,560
These things are not unwholesome, but when taken in conjunction with the meditation, they

42
00:03:25,560 --> 00:03:33,280
reduce the effect, they take away from the clarity of the mind when taken out of moderation.

43
00:03:33,280 --> 00:03:37,240
These are things like, first of all, eating.

44
00:03:37,240 --> 00:03:40,360
We have to be careful not to eat too much if we're always obsessed with food.

45
00:03:40,360 --> 00:03:45,560
This is one major addiction, it's something which creates sloth, which creates an unhealthy

46
00:03:45,560 --> 00:03:47,880
bodily state as well.

47
00:03:47,880 --> 00:03:51,760
It's something which is overall not conducive for meditation practices.

48
00:03:51,760 --> 00:03:58,880
This is overeating, we have to eat to stay alive, but we shouldn't live simply to eat.

49
00:03:58,880 --> 00:04:05,400
Another one is entertainment, watching television, watching movies, listening to music and

50
00:04:05,400 --> 00:04:11,040
so on, these are not unwholesome things, but when taken in excess, they create states

51
00:04:11,040 --> 00:04:15,760
of addiction and states of in sobriety actually in the mind, they take the mind out of

52
00:04:15,760 --> 00:04:22,240
its natural clarity using the internet, for instance, should be done in moderation.

53
00:04:22,240 --> 00:04:29,680
The third one is that as far as sleeping, now sleeping can be one addiction that we often

54
00:04:29,680 --> 00:04:34,720
overlook and we don't realize that actually we enjoy sleeping so much and whenever problems

55
00:04:34,720 --> 00:04:40,360
arise, we always find ourselves going to take a nap or trying to sleep more.

56
00:04:40,360 --> 00:04:45,760
Some people actually become insomniac because they are so obsessed about their sleep and

57
00:04:45,760 --> 00:04:49,440
we find that through the meditation practice, actually, we need very little sleep and

58
00:04:49,440 --> 00:04:53,480
a lack of sleep is not a real problem because our minds are so calm and so pure all the

59
00:04:53,480 --> 00:04:54,880
time.

60
00:04:54,880 --> 00:04:58,600
These are things which have to be taken in moderation and all together, these are things

61
00:04:58,600 --> 00:05:03,720
which we have to start to take out of our lives if the meditation is to be a successful

62
00:05:03,720 --> 00:05:05,640
part of our daily life.

63
00:05:05,640 --> 00:05:10,680
Now onto the question, how do we then incorporate the meditation directly into our daily

64
00:05:10,680 --> 00:05:11,680
life?

65
00:05:11,680 --> 00:05:13,240
And this we do in two ways.

66
00:05:13,240 --> 00:05:15,160
First of all, we focus on the body.

67
00:05:15,160 --> 00:05:20,640
Now the body in general, it's in one of the four major postures at all times.

68
00:05:20,640 --> 00:05:27,120
So either we're sitting or we're standing or we're walking or we're lying down.

69
00:05:27,120 --> 00:05:34,280
And we can use these four postures as sort of a very clear and very gross base for us

70
00:05:34,280 --> 00:05:39,200
to always find something to be mindful of, to be create a clear thought.

71
00:05:39,200 --> 00:05:42,600
So when we're walking, instead of simply walking and letting our minds wander, we can

72
00:05:42,600 --> 00:05:46,640
say to ourselves walking, walking, walking, walking.

73
00:05:46,640 --> 00:05:49,200
When we're standing, we can say standing, standing.

74
00:05:49,200 --> 00:05:54,840
When we sit, we can say sit things, sitting, when we lie down, lying, lying, lying.

75
00:05:54,840 --> 00:05:56,720
This is when we're not actually doing the meditation.

76
00:05:56,720 --> 00:06:01,120
We can undertake this practice at all times.

77
00:06:01,120 --> 00:06:05,320
Similarly with minor movements of the body, when we bend, we can say bending, when we

78
00:06:05,320 --> 00:06:09,240
stretch, stretching, when we move the hand moving, when we brush our teeth, brushing, when

79
00:06:09,240 --> 00:06:13,880
we eat food, chewing, chewing, and swallowing, swallowing, and so on.

80
00:06:13,880 --> 00:06:17,560
Any movement that we make in the body during the day, we can be mindful of.

81
00:06:17,560 --> 00:06:21,360
When we go to the washroom, when we take a shower, when we change our clothes, when we

82
00:06:21,360 --> 00:06:24,800
wash our clothes, whatever we do during the day, we can be mindful of that.

83
00:06:24,800 --> 00:06:29,960
As well, creating a clear thought based on the movements of the body.

84
00:06:29,960 --> 00:06:34,160
This is the first way of incorporating the practice directly into our daily life.

85
00:06:34,160 --> 00:06:43,600
The second way is we can use the senses, so seeing, hearing, smelling, tasting, and feeling.

86
00:06:43,600 --> 00:06:47,040
Normally when we see something, we either enjoy it or we get upset by it.

87
00:06:47,040 --> 00:06:51,000
Well, now we're going to start to use it to create a clear thought, not to create a judging

88
00:06:51,000 --> 00:06:56,840
thought. When we see something, just to know that we're seeing, say to ourselves, seeing,

89
00:06:56,840 --> 00:07:01,400
seeing, seeing. When we hear something instead of listening and enjoying or getting angry

90
00:07:01,400 --> 00:07:05,920
and upset by it, we say to ourselves, hearing. When we smell, we can say, smelling.

91
00:07:05,920 --> 00:07:10,880
When we taste, instead of enjoying it or getting repulsed by the taste, we can simply

92
00:07:10,880 --> 00:07:15,640
say to ourselves, tasting, tasting, and keep our mind clear. When we feel something on

93
00:07:15,640 --> 00:07:20,680
the body hot or cold, hard or soft or whatever feeling, we say to ourselves, feeling,

94
00:07:20,680 --> 00:07:27,400
feeling. These two ways are sort of a general practice, which we can undertake in our

95
00:07:27,400 --> 00:07:32,320
daily life, in a way of incorporating the meditation practice directly into our daily life.

96
00:07:32,320 --> 00:07:35,560
Of course, we can also be mindful of all of the things which I mentioned in the earlier

97
00:07:35,560 --> 00:07:41,960
videos, pain, for instance, or the emotions, anger, or greed, or so. But besides all of

98
00:07:41,960 --> 00:07:46,320
those, we can say as an addition, which once we've mastered the four foundations, which

99
00:07:46,320 --> 00:07:50,400
I mentioned in an earlier video, we can then add all of these things on as well as

100
00:07:50,400 --> 00:07:57,360
a way of incorporating the practice into our daily life. This is an explanation of meditation

101
00:07:57,360 --> 00:08:04,240
practice in daily life. And this concludes the set of videos on how to meditate. And I

102
00:08:04,240 --> 00:08:09,520
would like to thank you all for watching, taking the time to watch these videos. And I really

103
00:08:09,520 --> 00:08:14,360
truly sincerely hope that these videos bring peace and happiness and freedom from suffering

104
00:08:14,360 --> 00:08:18,320
to you and all of the people around you. Again, thank you for tuning in. If you'd like

105
00:08:18,320 --> 00:08:23,760
more information, you're welcome to contact us through the website and all the best here.

