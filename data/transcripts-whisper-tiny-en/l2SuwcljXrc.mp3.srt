1
00:00:00,000 --> 00:00:07,000
Bhante, how important is studying the Abhidhamma Pitaka to the practice of Yipasana?

2
00:00:07,000 --> 00:00:18,000
Some dismiss it by saying that the Abhidhamma was preached to the Devas and Brahma's and that the Suttapitaka is enough for us humans.

3
00:00:18,000 --> 00:00:41,000
I like to consider that the Devitaka in its entirety is really best used by a teacher to pass on the teaching.

4
00:00:41,000 --> 00:01:04,000
A person who reads the Dapitaka without a teacher, I would say whatever Dapitaka would be, is in danger of misunderstanding the teachings for so many reasons.

5
00:01:04,000 --> 00:01:09,000
One of course is the translation so unless you're accessing the poly there's always that.

6
00:01:09,000 --> 00:01:25,000
That you read a certain translation and it can throw you off based on your understanding of the words and the translators understanding of the poly and so on.

7
00:01:25,000 --> 00:01:44,000
But the teachings for example the Suttas can throw you off because some of them were directed at a particular audience and that audience may not be you and it may not be universal.

8
00:01:44,000 --> 00:01:55,000
So some people, I think the Buddha even said this, that some people mistake a specific teaching for universal teaching and some people mistake a universal teaching for a specific teaching.

9
00:01:55,000 --> 00:02:11,000
And so you'll say well the Buddha said this and that contradicts this and it can even lead of course to doubting the Buddha because you see in one place he says one thing in another place he seems to say another thing.

10
00:02:11,000 --> 00:02:32,000
With the Abhidhamma because it's so terse, in fact if you study the Abhidhamma, the potential for misunderstanding is great.

11
00:02:32,000 --> 00:02:42,000
Mostly I would say the potential for getting lost is great and getting discouraged and giving up.

12
00:02:42,000 --> 00:02:58,000
I wouldn't put too much emphasis on the importance of studying Abhidhamma unless you're a teacher who has been given the teachings by a teacher yourself and is interested in using these teachings to explain some of the experiences that come up in meditation,

13
00:02:58,000 --> 00:03:05,000
which the Abhidhamma is able to do, it's able to categorize and help us to stay on the straight and narrow for it.

14
00:03:05,000 --> 00:03:18,000
So it is possible that they work together in this way, most especially for a teacher because where the Suttapitika is vague and inexact, the Abhidhamma can sharpen it up.

15
00:03:18,000 --> 00:03:27,000
So they can work together and I guess if I was going to say anything that the three Pithikas should or do work very well together.

16
00:03:27,000 --> 00:03:38,000
Obviously many people say that Abhidhamma is not even the Buddha's teaching, some people say it goes against the Buddha's teaching even.

17
00:03:38,000 --> 00:03:47,000
But I would say it compliments the Suttas quite nicely because if you just read the Suttas you're reading talks that the Buddha gave to audience.

18
00:03:47,000 --> 00:03:57,000
If you're really Abhidhamma you're reading theory, very specific and exact theory, whether you agree with that theory or believe it was the Buddha's theory, that's another thing.

19
00:03:57,000 --> 00:04:16,000
And the Vinaya of course, without the Vinaya, just reading the Suttas without the Vinaya, deprives you of the base of morality, which is essential for keeping you on the right track and making sure that the meditation that you do cultivate is grounded in the Suttas.

20
00:04:16,000 --> 00:04:38,000
So I would say if you're going to read them all but don't certainly rely on the Pithikas much more rely on, well maybe not much more but at least as much rely on the teacher because then of course the other problem is how do you know the teacher is going by the Buddha's teaching.

21
00:04:38,000 --> 00:04:46,000
There's no easy answer to that but of course your question was simply the importance of studying the Abhidhamma.

22
00:04:46,000 --> 00:05:06,000
Whereas it's place I would say in helping to straighten or direct the Sutta Pithika and keep it from running a strain because you've got categories and you've got lots of very specific and exact guidelines for what is real and what fits where,

23
00:05:06,000 --> 00:05:09,000
what is wholesome, what is unwholesome and so on and so on.

24
00:05:09,000 --> 00:05:18,000
There's very strict guidelines that allow you to answer some of the questions that are posed by the Sutta Pithika for example.

25
00:05:18,000 --> 00:05:23,000
But in studying Vipasana, not really important unless you don't have a teacher.

26
00:05:23,000 --> 00:05:35,000
If you don't have a teacher at all and all you've got is the Pithika I would say all three of them go together and have an equal importance.

27
00:05:35,000 --> 00:05:46,000
Or have an importance how equal it is I can't say but are important and it seems to me fairly equal the importance of the three.

28
00:05:46,000 --> 00:06:09,000
I think if you give up the Abhidhamma you're running in danger of the ambiguity of the ambiguity of the Suttas.

