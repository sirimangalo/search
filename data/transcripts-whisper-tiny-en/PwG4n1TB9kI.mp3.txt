Okay, so the latest question comes from one of the commenters, asking whether I'd recommend
to go to a 10-day v-passenomititation course.
I guess the short answer here is no, because I wouldn't recommend something that I myself
didn't teach or practice, and generally a 10-day meditation, v-passenomititation course
has offered around the world is not something that I teach.
As for how it might be a problem, I also can't say, and understand that many courses require
you to sit for many hours a day, sometimes for an hour at a time, even for a beginner
meditator.
The way I teach is to do a course maybe two or three weeks, and some people might come
for a part of a course, because it'll come just for a week, which is fine, but a full course
for a beginner would take, say, three weeks, not more than that, but not much less, if
you've never practiced meditation.
And the way that works is we start the meditator off easily, and the whole course, the whole
progression is much more natural.
It's much more natural also because half of your meditation time is done in walking, whereas
many courses are done with sitting for eight to ten hours a day.
The practice that I teach is, according to the Buddha's teaching, that a monk or by extrapolation
anyone who's serious in meditation should spend half their time walking, half their
time sitting, so walk and sit in alternation for the whole of the day, and only breaking
for eating and for sleeping.
Now, for a beginner meditator, this is quite difficult, and so we start you off simply.
We have you do mindful frustration, ten minutes walking, ten minutes sitting, and you're
allowed to take a break, and do a courting as you see fit, or you feel capable.
You can take a break and come back, and we don't have very much group practice, generally
we'll do one or two group practices during the day, otherwise it's up to you to make
your own schedule and we expect you to practice as much as you can comfortably.
In this way, we slowly increase to the point where you don't even notice that all of a
sudden you're walking one hour and sitting one hour, and you also don't notice it because
you don't have to sit the whole of the day, you're allowed to get up and do walking,
you're required to get up and do walking meditation.
I would only recommend what I teach and what I practice and that is as I've explained
it to a three week course, where you do walking and sitting meditation in tandem together
and that you do it gradually, slowly building up to the point where you don't even notice
that you're suddenly practicing eight, ten, twelve, even more hours of meditation
in the day.
It's not difficult.
If you do it like this, this part isn't difficult, the hours aren't difficult.
It's a difficult course and it's difficult to meditate, but you don't feel like it's
an insurmountable obstacle.
You suddenly feel like a pro over the course of the training.
So I hope that helps.
You're always welcome to give contact me.
I can maybe give pointers as to courses in your area that I would recommend or maybe
give some more detailed analysis of the various courses that are offered out there.
You're welcome to contact me for more information, okay?
And if you have any more questions, just leave them.
See you.
