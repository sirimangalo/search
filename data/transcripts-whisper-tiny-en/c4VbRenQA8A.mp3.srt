1
00:00:00,000 --> 00:00:19,040
Hi, so welcome to my latest installment of this day.

2
00:00:19,040 --> 00:00:26,640
This is a weekly routine of mine, normally it would be tomorrow, but we're busy tomorrow

3
00:00:26,640 --> 00:00:31,640
so lucky for all of us. We get this, everyone gets to see this today.

4
00:00:31,640 --> 00:00:35,640
I give a talk in virtual reality.

5
00:00:35,640 --> 00:00:43,640
Now, I've always tried to keep a little bit up with the technologies of the modern time

6
00:00:43,640 --> 00:00:53,640
for the purposes of teaching, because things like webcam and internet,

7
00:00:53,640 --> 00:00:57,640
many of the technologies that come up can be quite useful in spreading the teaching,

8
00:00:57,640 --> 00:01:03,640
and I never thought that virtual reality would be such a one because, of course, it seems fake,

9
00:01:03,640 --> 00:01:06,640
and we're trying to learn about reality.

10
00:01:06,640 --> 00:01:09,640
But then I saw a video on it, and I thought, well, I wonder,

11
00:01:09,640 --> 00:01:14,640
because you can bring people together from all around the world if you use this technology.

12
00:01:14,640 --> 00:01:19,640
And so I checked it out kind of tentatively, and lo and behold,

13
00:01:19,640 --> 00:01:25,640
there is a Buddhist community in one of these virtual worlds.

14
00:01:25,640 --> 00:01:32,640
And so what I'm going to be showing you today is my weekly talk that I give in this virtual reality.

15
00:01:32,640 --> 00:01:37,640
So there's me that monk looking guy, and here we are sitting around the camper.

16
00:01:37,640 --> 00:01:42,640
I'll just zoom in and take you on a tour.

17
00:01:42,640 --> 00:01:50,640
So this is me. I've made an avatar that looks just like me, and using my real robes,

18
00:01:50,640 --> 00:01:57,640
and making a picture of them, or several pictures, and putting them together on my avatar.

19
00:01:57,640 --> 00:02:01,640
I've taken a picture of my sitting cloth, and there I am sitting on it.

20
00:02:01,640 --> 00:02:08,640
I've taken a picture. No, I've created this pose that lets me look like I'm meditating.

21
00:02:08,640 --> 00:02:13,640
And so I appear to be a monk there.

22
00:02:13,640 --> 00:02:20,640
But the neat thing here is that all of these other people sitting around the campfire are all around the world.

23
00:02:20,640 --> 00:02:23,640
And they're going to hear me give this talk in real time.

24
00:02:23,640 --> 00:02:27,640
And it's going to feel like we're here, we are sitting in a room together.

25
00:02:27,640 --> 00:02:34,640
So their attention is totally wrapped on what I'm saying because of the atmosphere.

26
00:02:34,640 --> 00:02:40,640
So it really is a good atmosphere for giving a talk, and it's amazing that you can give a talk to people from all around the world.

27
00:02:40,640 --> 00:02:44,640
And almost like seeing them, like being there with them.

28
00:02:44,640 --> 00:02:48,640
This woman is the organizer. She helps put this together.

29
00:02:48,640 --> 00:02:53,640
She's actually in real life. She's a PhD in New York.

30
00:02:53,640 --> 00:02:58,640
And the rest of these people I don't really know. I know this man vaguely.

31
00:02:58,640 --> 00:03:03,640
But the rest of these people are just here to hear me give a good talk.

32
00:03:03,640 --> 00:03:07,640
And here we are, these are the deer. Here we are in the deer park.

33
00:03:07,640 --> 00:03:15,640
So this area is a popular sort of based on a popular Buddhist motif of the deer park.

34
00:03:15,640 --> 00:03:23,640
Because they say the Buddha gave his first discourses, first sermon in a deer park.

35
00:03:23,640 --> 00:03:30,640
So that's what this is based on. It's a very common theme in Buddhism.

36
00:03:30,640 --> 00:03:36,640
Here's the Buddha image. And normally before I give the talk here, I'll show you. I stand up.

37
00:03:36,640 --> 00:03:47,640
Before I give the talk, I would go and just as a symbolic gesture, I'd pay respect to the Buddha.

38
00:03:47,640 --> 00:03:54,640
And here I am bowing down to the Buddha image. Virtually.

39
00:03:54,640 --> 00:03:58,640
All of this is not so important. What's most important is the fact that I can give the talk.

40
00:03:58,640 --> 00:04:06,640
It's nice to give it some structure so that we remember the things that are important.

41
00:04:06,640 --> 00:04:11,640
And then I go and sit here again. And here we are.

42
00:04:11,640 --> 00:04:16,640
Okay, so I'm just waiting probably in another ten minutes. I'll be giving a talk.

43
00:04:16,640 --> 00:04:22,640
And I'll record it here on the webcam as well.

44
00:04:22,640 --> 00:04:28,640
So everyone can see this as part of life in the day.

45
00:04:28,640 --> 00:04:56,640
Okay, so just another ten minutes. I'll be recording again.

