1
00:00:00,000 --> 00:00:07,480
Okay, I'm at my work and suddenly I realize there is a song playing in my mind.

2
00:00:07,480 --> 00:00:12,520
How do I put my full concentration on my work?

3
00:00:12,520 --> 00:00:20,720
It's difficult and you're realizing the difficulty.

4
00:00:20,720 --> 00:00:29,760
You're realizing the danger in attraction and maybe not maybe dangerous to hardware.

5
00:00:29,760 --> 00:00:39,240
The disadvantage or the downside of essential pleasure because guaranteed that song is

6
00:00:39,240 --> 00:00:44,320
not in your mind, it was not put in your mind by someone else and it's not just coming

7
00:00:44,320 --> 00:00:51,920
randomly into your mind, guaranteed it's there because in the past you were attracted

8
00:00:51,920 --> 00:00:57,600
to that song, either that or you're horrified by the song or it was a song that people

9
00:00:57,600 --> 00:01:01,360
may listen to and therefore it's coming back to you.

10
00:01:01,360 --> 00:01:07,240
But this is the power of attraction, the power of attraction, the power of aversion is that

11
00:01:07,240 --> 00:01:09,640
it creates an imprint on the mind.

12
00:01:09,640 --> 00:01:16,720
This is how we understand karma, that this is the result of your karma of listening to

13
00:01:16,720 --> 00:01:34,480
music when a person is enthused by music, interested in music, engaged in the enjoyment

14
00:01:34,480 --> 00:01:46,600
of music, then one will fall into these states or will receive the result which is

15
00:01:46,600 --> 00:01:51,640
repeating in the replying of the music based on the imprint that it's put in your brain.

16
00:01:51,640 --> 00:01:53,560
This is how memory works.

17
00:01:53,560 --> 00:01:57,080
You remember things that excite you.

18
00:01:57,080 --> 00:02:07,680
So from a Buddhist point of view based on the concepts of the theory of non-self, there's

19
00:02:07,680 --> 00:02:08,920
nothing you can do.

20
00:02:08,920 --> 00:02:16,120
You can try, you can work, you can force your mind to go to the work and it creates more

21
00:02:16,120 --> 00:02:30,440
stress and more karma and has more results and can turn you into a stressed tense individual

22
00:02:30,440 --> 00:02:37,480
because you're trying to force your mind to work or from a Buddhist, we would recommend

23
00:02:37,480 --> 00:02:43,560
rather, from a Buddhist point of view is to slowly learn to give up craving, to give up

24
00:02:43,560 --> 00:02:54,120
attachment and to give up the addiction to central pleasures which will then get in the

25
00:02:54,120 --> 00:02:57,320
way of things like work.

26
00:02:57,320 --> 00:03:04,080
If you can be free from your need to listen to music, you need to find enjoyment, it can

27
00:03:04,080 --> 00:03:14,800
only help things like focusing on your work, focusing on an activity.

28
00:03:14,800 --> 00:03:21,960
But the only qualifier there, of course, is that it being non-self and you're having

29
00:03:21,960 --> 00:03:32,200
already developed these Sankara's and these conditions in the mind that in the end, this

30
00:03:32,200 --> 00:03:36,800
is why in meditation, if you meditate enough in the end, you give up your work, you give

31
00:03:36,800 --> 00:03:45,200
up society because you realize it's based on impossible assumptions, it's based on incoherent

32
00:03:45,200 --> 00:03:55,840
assumptions or the idea that you can find stability in things like work and so you see

33
00:03:55,840 --> 00:03:57,440
that it's only causing stress.

34
00:03:57,440 --> 00:04:02,120
You have these deadlines and the only way to meet these deadlines, the only way to be successful

35
00:04:02,120 --> 00:04:07,920
in the world, is to have craving, is to force yourself, is to create more formational

36
00:04:07,920 --> 00:04:16,600
formations of focusing and of controlling the mind and therefore more suffering.

37
00:04:16,600 --> 00:04:22,840
So in the end, this is why a meditator will give up the world and will become a monk

38
00:04:22,840 --> 00:04:29,720
or at least live a simple life where they don't have deadlines or where they don't have

39
00:04:29,720 --> 00:04:37,720
the requirements to focus on something that the mind is not naturally inclined to focus

40
00:04:37,720 --> 00:04:38,720
on.

41
00:04:38,720 --> 00:04:43,720
So there will be more inclined to studying the Dhamma and doing things that are really

42
00:04:43,720 --> 00:04:49,520
peaceful and conducive to peace.

43
00:04:49,520 --> 00:04:53,640
In the meantime, when it does come up, what I would suggest is rather than focusing

44
00:04:53,640 --> 00:04:57,960
on your work, try to focus on the song for that moment because it only takes a few seconds

45
00:04:57,960 --> 00:05:02,440
to say hearing, hearing when you're hearing the song, if you like it, liking, liking, put

46
00:05:02,440 --> 00:05:07,800
down your work, take a break for just five, ten seconds, however long it takes to do away

47
00:05:07,800 --> 00:05:13,640
with that, then focus, you'll find your focus has improved when you're able to do that,

48
00:05:13,640 --> 00:05:18,040
focus on the song first and then go back to your work rather than forcing the mind,

49
00:05:18,040 --> 00:05:31,520
which is contrary to meditation and has negative effects on the mind.

