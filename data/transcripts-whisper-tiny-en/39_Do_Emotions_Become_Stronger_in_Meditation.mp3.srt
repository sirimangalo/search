1
00:00:00,000 --> 00:00:03,840
Do emotions become stronger in meditation?

2
00:00:03,840 --> 00:00:05,200
Question.

3
00:00:05,200 --> 00:00:07,800
When wanting arises when I meditate,

4
00:00:07,800 --> 00:00:11,080
I start meditating on the wanting by saying,

5
00:00:11,080 --> 00:00:16,080
wanting, wanting, but the wanting becomes stronger.

6
00:00:16,080 --> 00:00:18,720
It seems like I'm only suppressing the wanting

7
00:00:18,720 --> 00:00:21,560
rather than getting rid of it completely.

8
00:00:21,560 --> 00:00:24,480
How do I overcome this?

9
00:00:24,480 --> 00:00:27,080
Answer.

10
00:00:27,080 --> 00:00:29,000
What you are experiencing

11
00:00:29,000 --> 00:00:30,600
is actually a good thing

12
00:00:30,600 --> 00:00:32,120
because you have stopped suppressing

13
00:00:32,120 --> 00:00:35,000
or avoiding your emotions.

14
00:00:35,000 --> 00:00:38,520
That is why it presents itself so strongly.

15
00:00:38,520 --> 00:00:42,440
It is not as though meditation is cultivating desire,

16
00:00:42,440 --> 00:00:44,800
but you have a propensity to give rise

17
00:00:44,800 --> 00:00:46,720
to massive amounts of desire.

18
00:00:46,720 --> 00:00:49,600
That meditation makes you aware of.

19
00:00:49,600 --> 00:00:52,280
Normally, rather than dealing objectively

20
00:00:52,280 --> 00:00:54,720
with our habits of desire and aversion,

21
00:00:54,720 --> 00:00:58,800
we react to them, trying to appease or fix them.

22
00:00:58,800 --> 00:01:03,080
And that is why we do not experience them so strongly.

23
00:01:03,080 --> 00:01:07,840
It is like taking your garbage and throwing it in your closet.

24
00:01:07,840 --> 00:01:10,400
Every time you want to dispose of your garbage,

25
00:01:10,400 --> 00:01:12,480
you just throw it into your closet.

26
00:01:12,480 --> 00:01:13,840
Well, guess what?

27
00:01:13,840 --> 00:01:16,400
You end up with a closet full of garbage

28
00:01:16,400 --> 00:01:18,800
and you just keep ignoring the problem

29
00:01:18,800 --> 00:01:21,240
rather than dealing with it.

30
00:01:21,240 --> 00:01:23,720
Meditation is like opening the closet

31
00:01:23,720 --> 00:01:26,400
and having it all fall down on you.

32
00:01:26,400 --> 00:01:28,240
Your immediate perception is that

33
00:01:28,240 --> 00:01:30,200
it was wrong to open the door,

34
00:01:30,200 --> 00:01:33,240
that there is something wrong with all the meditation practice

35
00:01:33,240 --> 00:01:36,000
as it brings out all sorts of mental garbage.

36
00:01:36,000 --> 00:01:37,640
So you have a choice.

37
00:01:37,640 --> 00:01:39,720
You can keep it in the closet

38
00:01:39,720 --> 00:01:41,560
and keep throwing more in

39
00:01:41,560 --> 00:01:42,960
or you can clean out the closet

40
00:01:42,960 --> 00:01:45,480
and deal with the garbage directly when it comes to you.

41
00:01:46,760 --> 00:01:48,640
In that sense, there is nothing really wrong

42
00:01:48,640 --> 00:01:50,480
with the experiences that you are having.

43
00:01:51,960 --> 00:01:55,560
Another thing to note is that the desire itself

44
00:01:55,560 --> 00:01:58,560
is not likely what you experience as increasing.

45
00:01:59,920 --> 00:02:03,080
What you are experiencing are the physical manifestations

46
00:02:03,080 --> 00:02:04,520
of the desire in the body.

47
00:02:05,960 --> 00:02:07,960
A good example is with lust.

48
00:02:09,680 --> 00:02:11,960
99% of it is not desire.

49
00:02:12,920 --> 00:02:15,800
The chemical reactions, the bodily changes,

50
00:02:15,800 --> 00:02:17,520
the hormones and so on,

51
00:02:17,520 --> 00:02:19,960
are all physical manifestations of desire.

52
00:02:20,920 --> 00:02:22,960
This is why it is so important

53
00:02:22,960 --> 00:02:24,560
to be able to break up experiences

54
00:02:24,560 --> 00:02:26,320
into their component parts.

55
00:02:27,360 --> 00:02:29,240
When you meditate on desire,

56
00:02:29,240 --> 00:02:32,200
you are allowing your body to give rise to these states

57
00:02:32,200 --> 00:02:33,800
that normally you would suppress.

58
00:02:35,080 --> 00:02:37,920
Normally you would create a great tension in the body

59
00:02:37,920 --> 00:02:39,720
that would suppress many of the systems

60
00:02:39,720 --> 00:02:41,760
that are now given the chance to release.

61
00:02:42,880 --> 00:02:46,520
The chemicals will arise along with the physical pleasure

62
00:02:46,520 --> 00:02:50,280
and then the desire rises for more such pleasure.

63
00:02:50,280 --> 00:02:53,000
It can be that when you first start to meditate,

64
00:02:53,000 --> 00:02:56,200
the delusion and guilt is preventing you from seeing this,

65
00:02:56,200 --> 00:02:58,400
but when you meditate regularly,

66
00:02:58,400 --> 00:03:01,200
you will see that desire actually disappears

67
00:03:01,200 --> 00:03:02,520
and only the physical pleasure

68
00:03:02,520 --> 00:03:04,160
that would normally have been associated

69
00:03:04,160 --> 00:03:05,920
with the desire persists.

70
00:03:07,720 --> 00:03:11,840
Because of noting to yourself, wanting, wanting,

71
00:03:11,840 --> 00:03:16,640
as well as happy, happy or pleasure, pleasure,

72
00:03:16,640 --> 00:03:19,120
or even feeling, feeling,

73
00:03:19,120 --> 00:03:21,360
you experience bodily relief.

74
00:03:21,360 --> 00:03:24,680
Because there is less tension due to less desire,

75
00:03:24,680 --> 00:03:27,280
less guilt, and less anxiety, et cetera.

76
00:03:29,320 --> 00:03:31,240
A good way of understanding this

77
00:03:31,240 --> 00:03:33,440
is that you have previously been suppressing

78
00:03:33,440 --> 00:03:36,520
or restricting the flow of bodily systems,

79
00:03:36,520 --> 00:03:38,160
and that is what is now coming out.

80
00:03:39,720 --> 00:03:42,120
Desire is the moment when you like something.

81
00:03:43,480 --> 00:03:45,640
That is only a very small part,

82
00:03:45,640 --> 00:03:47,480
the mental part of the experience.

83
00:03:48,800 --> 00:03:50,480
Desire is where the mind cultivates

84
00:03:50,480 --> 00:03:52,920
partiality for an experience,

85
00:03:52,920 --> 00:03:54,600
deciding that it is appealing.

86
00:03:56,080 --> 00:03:59,400
It is where the mind categorizes experiences as positive.

87
00:04:00,760 --> 00:04:03,840
It is important to focus on the desire objectively,

88
00:04:03,840 --> 00:04:05,440
rather than trying to remove it.

89
00:04:06,600 --> 00:04:09,200
You must also be mindful of the pleasurable experiences

90
00:04:09,200 --> 00:04:09,880
in the body.

91
00:04:11,320 --> 00:04:14,720
Furthermore, when the wanting seems to become stronger,

92
00:04:15,720 --> 00:04:18,560
it can also be that you just see it more clearly.

93
00:04:18,560 --> 00:04:21,760
You begin to understand that it has been there all along,

94
00:04:21,760 --> 00:04:23,960
but you did not see it so clearly before.

95
00:04:25,600 --> 00:04:27,480
In meditation, we face things

96
00:04:27,480 --> 00:04:30,880
that we would normally avoid, ignore, or react to you.

97
00:04:32,800 --> 00:04:34,200
This is a very clear point

98
00:04:34,200 --> 00:04:36,600
that many people miss about the Buddha's teachings.

99
00:04:37,680 --> 00:04:39,720
If you read the Satyapatana sutra,

100
00:04:40,720 --> 00:04:42,040
the Buddha says,

101
00:04:42,040 --> 00:04:44,000
when you have central desire,

102
00:04:44,000 --> 00:04:45,440
knowing yourself,

103
00:04:45,440 --> 00:04:47,320
I have central desire.

104
00:04:47,320 --> 00:04:49,240
There is central desire in the mind.

105
00:04:50,520 --> 00:04:52,680
When central desire is not present,

106
00:04:52,680 --> 00:04:54,200
knowing yourself,

107
00:04:54,200 --> 00:04:56,160
central desire is not present.

108
00:04:57,480 --> 00:04:58,680
This is very important.

109
00:04:59,640 --> 00:05:02,320
This creates the equanimity that we are talking about.

110
00:05:03,760 --> 00:05:06,760
It allows you to see the progression of states.

111
00:05:06,760 --> 00:05:10,120
It allows you to see how greed works, an anger, and so on,

112
00:05:11,040 --> 00:05:12,680
and how the mind works in general.

113
00:05:13,760 --> 00:05:17,000
It may not be the central desire is becoming stronger,

114
00:05:17,000 --> 00:05:18,960
but that you are now more aware of it,

115
00:05:18,960 --> 00:05:21,240
and you're also admitting it to yourself.

116
00:05:21,240 --> 00:05:23,080
Where previously, you may have avoided it

117
00:05:23,080 --> 00:05:25,160
admitting it out of guilt or aversion.

118
00:05:26,360 --> 00:05:27,920
We might say,

119
00:05:27,920 --> 00:05:31,800
I'm not a greedy person, or I'm not an angry person.

120
00:05:33,200 --> 00:05:35,760
But that is because we suppress it all.

121
00:05:35,760 --> 00:05:39,160
And then in meditation, we see the anger or greed arise,

122
00:05:39,160 --> 00:05:48,240
and we blame it to rising on the meditation.

