1
00:00:00,000 --> 00:00:06,700
Hello and welcome back to Ask a Monk. Today's question is about drowsiness. So I was

2
00:00:06,700 --> 00:00:12,480
asked how to deal with falling asleep when meditating. Someone said they would

3
00:00:12,480 --> 00:00:15,840
like to be able to meditate and they've tried but they find themselves falling

4
00:00:15,840 --> 00:00:21,400
asleep every time. I think I've addressed this before but not comprehensively so

5
00:00:21,400 --> 00:00:29,680
I'd like to do that now as sort of referring back to the Buddha's teaching. Now

6
00:00:29,680 --> 00:00:35,640
the person who asked the question this time said that he would let he or she

7
00:00:35,640 --> 00:00:44,360
would like to use the meditation for the purpose of feeling rested so they

8
00:00:44,360 --> 00:00:49,280
want to be able to find rest but still be alert. And I'd like to caution that

9
00:00:49,280 --> 00:00:55,560
maybe the reason why you're falling asleep when you meditate because you've

10
00:00:55,560 --> 00:01:01,520
got a wrong understanding of what meditation should be. If your intention is to

11
00:01:01,520 --> 00:01:08,880
simply rest then falling asleep is probably the best way to do it. To an

12
00:01:08,880 --> 00:01:13,680
extent I mean I guess if that's the only the point is if that's the only

13
00:01:13,680 --> 00:01:18,000
intention that you go into the meditation with and that's the direction that

14
00:01:18,000 --> 00:01:23,600
your mind is going to go if you sit down and meditate and say okay I want to

15
00:01:23,600 --> 00:01:29,680
feel rested your mind is not going to develop energy it's not going to be

16
00:01:29,680 --> 00:01:36,120
awake and alert it's going to start to tend towards the only way that it

17
00:01:36,120 --> 00:01:39,720
knows how to rest and that is too falling asleep. There are better ways in

18
00:01:39,720 --> 00:01:44,560
meditation I think certainly does lead to much more of a rested mind state but

19
00:01:44,560 --> 00:01:49,800
the mind won't go in that direction unless you your intention and going to

20
00:01:49,800 --> 00:01:53,520
the meditation is to use it for the purpose of meditating and that is to

21
00:01:53,520 --> 00:01:58,080
understand reality and to be able to deal with the difficulties and the

22
00:01:58,080 --> 00:02:03,560
problems and to understand the difficulties and the problems that you have in

23
00:02:03,560 --> 00:02:08,600
your mind and in your life. If you go into the meditation with this mindset

24
00:02:08,600 --> 00:02:14,240
this sort of mindset then your mind is more likely to be alert and to be

25
00:02:14,240 --> 00:02:17,960
interested and you're more likely to be able to pay attention less likely to

26
00:02:17,960 --> 00:02:21,920
fall asleep so that's the first bit of advice I would give is to try to

27
00:02:21,920 --> 00:02:27,560
reevaluate what you expect to get out of meditation resting is certainly

28
00:02:27,560 --> 00:02:33,920
not the purpose of practicing meditation not the best aim and it it's

29
00:02:33,920 --> 00:02:37,680
going if that's the aim going into it it's as I said going to have these sorts

30
00:02:37,680 --> 00:02:43,520
of repercussions causing you to fall asleep. Nonetheless for those meditators who

31
00:02:43,520 --> 00:02:47,960
have made up their mind and come to understand what it is that meditation is

32
00:02:47,960 --> 00:02:53,120
for and are using it for the purpose of coming to understand reality as it is

33
00:02:53,120 --> 00:02:57,960
there are times especially when doing intensive meditation that you find

34
00:02:57,960 --> 00:03:02,560
yourself feeling drowsy and even falling asleep. So how do you deal with this?

35
00:03:02,560 --> 00:03:09,720
Well the Buddha gave seven specific techniques that one could use to overcome

36
00:03:09,720 --> 00:03:15,920
drowsiness and there are actually seven different sort of types of they deal with

37
00:03:15,920 --> 00:03:19,520
seven different types of drowsiness or at least a few different types of

38
00:03:19,520 --> 00:03:25,080
drowsiness each one is is unique and it's it's approach and so I'd like to in

39
00:03:25,080 --> 00:03:32,240
this case refer directly back to this it's the pajalayana sutta or pajalasutta

40
00:03:32,240 --> 00:03:37,360
or tapalasutta it's in the Angudrani kaya book of seven so we have seven

41
00:03:37,360 --> 00:03:43,680
seven ways of dealing with drowsiness. So the first way that the Buddha said is

42
00:03:43,680 --> 00:03:51,680
to change your object or to examine the object of your attention because one

43
00:03:51,680 --> 00:03:58,560
of the most obvious way obvious reasons why someone might become

44
00:03:58,560 --> 00:04:02,320
drowsies because their mind has begun to wander. When you're sitting in

45
00:04:02,320 --> 00:04:07,240
meditation you begin by focusing on a specific object but your mind starts to

46
00:04:07,240 --> 00:04:15,640
wander and slowly fall into a more trance-like state that that is bordering on

47
00:04:15,640 --> 00:04:21,920
sleep and eventually will lead one to fall asleep. Now when that is the case

48
00:04:21,920 --> 00:04:27,560
it's important to think about what it was that you were considering when

49
00:04:27,560 --> 00:04:32,560
you were when you were feeling drowsy and to change that to go back to

50
00:04:32,560 --> 00:04:39,040
focusing on the original object and to be careful not to let yourself fall

51
00:04:39,040 --> 00:04:44,320
into the reflection on these unspeculative thoughts that are going to

52
00:04:44,320 --> 00:04:48,400
lead your mind to wander. Often it's the case that will start to remember

53
00:04:48,400 --> 00:04:51,400
things that were worried about or concerned about and that will lead our

54
00:04:51,400 --> 00:04:57,520
minds to wander and speculate and eventually get tired and fall into fall

55
00:04:57,520 --> 00:05:04,040
asleep. So the Buddha said be careful not to give those sorts of thoughts any

56
00:05:04,040 --> 00:05:09,440
ground. Whatever thought it was that was causing you to feel tired to avoid

57
00:05:09,440 --> 00:05:17,880
that thought and not think might not develop that train of thought or state of

58
00:05:17,880 --> 00:05:23,160
mind. Of course the best way to do this is to come back to your meditation

59
00:05:23,160 --> 00:05:28,440
technique and especially to deal with as the Buddha said the the drowsiness

60
00:05:28,440 --> 00:05:35,000
itself instead of allowing the thoughts to build the drowsiness focus on the

61
00:05:35,000 --> 00:05:39,360
drowsiness and look at it. When you do that you're letting go of the cause of

62
00:05:39,360 --> 00:05:46,440
the drowsiness and the drowsiness will disappear by itself. Also the

63
00:05:46,440 --> 00:05:51,440
attention and the alert awareness of the drowsiness is the opposite and

64
00:05:51,440 --> 00:05:55,280
you'll find that the drowsiness as you watch it because of the change in the

65
00:05:55,280 --> 00:06:00,160
mindset the drowsiness itself goes away. So you can try to focus on the drowsiness

66
00:06:00,160 --> 00:06:05,720
itself which is quite useful but most important is to avoid the thoughts that

67
00:06:05,720 --> 00:06:13,280
were causing you to become drowsiness. The second way is to if this practical

68
00:06:13,280 --> 00:06:16,720
method doesn't work of reverting back to the practice and adjusting your

69
00:06:16,720 --> 00:06:23,640
practice you can go back to the teachings that the Buddha taught or that your

70
00:06:23,640 --> 00:06:30,720
teacher gave us on and to go over them in your mind to think about for instance

71
00:06:30,720 --> 00:06:35,920
the four foundations of mindfulness the body the feelings the mind the various

72
00:06:35,920 --> 00:06:42,160
dummies the hindrances of the emotions or so on focus on what it was that the

73
00:06:42,160 --> 00:06:47,120
Buddha taught or what it is that your meditation teacher taught and go over them

74
00:06:47,120 --> 00:06:53,520
also the body how do we focus on the body think about maybe ways that you are

75
00:06:53,520 --> 00:06:58,920
lacking and things that you are missing in terms of awareness of the body

76
00:06:58,920 --> 00:07:03,040
you know are you actually able to watch the movements of this stomach or be

77
00:07:03,040 --> 00:07:08,760
aware of the sitting position or the breath or whatever is your object are you

78
00:07:08,760 --> 00:07:11,680
actually aware of the feelings when there's a painful feeling are you

79
00:07:11,680 --> 00:07:16,200
actually paying attention to it for instance saying to yourself pain pain or

80
00:07:16,200 --> 00:07:20,560
if you feel happy or calm are you actually paying attention or are you letting

81
00:07:20,560 --> 00:07:27,240
it drag you down into a state of lethargy and a state of fatigue and and

82
00:07:27,240 --> 00:07:33,440
growziness and the same with the mind and the mind of just so so refer back to

83
00:07:33,440 --> 00:07:39,760
the teachings and think about them in your mind examine them and relate them

84
00:07:39,760 --> 00:07:46,120
back to your practice and compare your practice to the teachings and that if

85
00:07:46,120 --> 00:07:49,960
you do that first of all just thinking about these good things will will wake

86
00:07:49,960 --> 00:07:54,560
you up and and remind you of what you should be doing and second of all it

87
00:07:54,560 --> 00:07:59,680
will allow you to adjust your practice the third way is if that doesn't work or

88
00:07:59,680 --> 00:08:04,600
another way to deal with drowsiness is to actually recite the teachings and I

89
00:08:04,600 --> 00:08:08,960
know from experience that this is quite useful for example when you're

90
00:08:08,960 --> 00:08:16,320
driving I remember when I would be driving somewhere at night and trying to

91
00:08:16,320 --> 00:08:21,760
be mindful watching the road and steering turning seeing and emotions that

92
00:08:21,760 --> 00:08:27,440
arise and so on that I would find myself drifting because sometimes there

93
00:08:27,440 --> 00:08:31,880
would be excess concentration and not enough effort and maybe falling asleep

94
00:08:31,880 --> 00:08:35,960
when that happened I would actually recite the Buddhist teaching and we do

95
00:08:35,960 --> 00:08:42,280
this chant chants that we have of the Buddhist teachings we actually

96
00:08:42,280 --> 00:08:46,600
recite them so there's not it's not really an intellectual exercise but it's

97
00:08:46,600 --> 00:08:51,880
something akin to singing songs when people turn on the radio and sing along

98
00:08:51,880 --> 00:08:57,280
when they're driving late at night it's something that will wake you up but

99
00:08:57,280 --> 00:08:59,880
also of course because it's the Buddhist teachings it's something that will

100
00:08:59,880 --> 00:09:06,400
invigorate you and give you effort and give you the encouragement that you

101
00:09:06,400 --> 00:09:10,040
might need thinking about the Buddha thinking about his teachings thinking

102
00:09:10,040 --> 00:09:18,240
about the meditation practice and so on if that still doesn't work we the

103
00:09:18,240 --> 00:09:23,600
fourth method the Buddha said is to start to get physical and the Buddha said

104
00:09:23,600 --> 00:09:30,480
you pull your ears technique of waking up kind of stretching your cranium rub

105
00:09:30,480 --> 00:09:36,280
your arms rub your body massage yourself and maybe stretch a little bit to

106
00:09:36,280 --> 00:09:41,680
kind of wake you up to get the blood flowing and to give yourself a little bit of

107
00:09:41,680 --> 00:09:46,760
physical energy because that might be the cause your body might be tired or

108
00:09:46,760 --> 00:09:52,320
so on your body might be stiff for instance some people might even go so

109
00:09:52,320 --> 00:09:56,280
far as to practice yoga I think it could be a very useful technique to

110
00:09:56,280 --> 00:10:01,200
waking you up because it's a different technique and actually might lead in a

111
00:10:01,200 --> 00:10:05,720
different direction I wouldn't recommend extensive practice of yoga in

112
00:10:05,720 --> 00:10:13,600
combination with insight meditation but there's certainly nothing harmful in

113
00:10:13,600 --> 00:10:20,720
practicing it and certainly in moderation for the purposes of building the

114
00:10:20,720 --> 00:10:26,360
energy necessary to practice meditation but at any rate the Buddha said

115
00:10:26,360 --> 00:10:33,200
massaging and rubbing and pulling your ears and so on if that doesn't work

116
00:10:33,200 --> 00:10:37,640
another way to deal with drowsiness the Buddha said is to actually stand up

117
00:10:37,640 --> 00:10:43,360
and then go get some water or water on your face rub water into your eyes and

118
00:10:43,360 --> 00:10:48,000
look to all directions the Buddha said take a look around this is a way of

119
00:10:48,000 --> 00:10:52,920
kind of waking you up and stimulating your mind to sort of get rid of this

120
00:10:52,920 --> 00:10:58,240
heavy state of concentration look in all directions look all around you go and

121
00:10:58,240 --> 00:11:02,960
look around see what's going on and look up at the stars he said you should

122
00:11:02,960 --> 00:11:07,320
be drowsiness comes at night so go and look up at the sky look up at the

123
00:11:07,320 --> 00:11:12,960
stars look up at the constellations it's a way of breaking up this heavy

124
00:11:12,960 --> 00:11:17,880
concentration giving yourself more flexible state of mind to break to throw

125
00:11:17,880 --> 00:11:23,960
off the lethargy the drowsiness he said if you do that then it's possible that

126
00:11:23,960 --> 00:11:27,960
the drowsiness will will disappear and you'll be able to continue with your

127
00:11:27,960 --> 00:11:33,840
practice you can do walking meditation the Buddha said this is a part of this is

128
00:11:33,840 --> 00:11:38,280
to switch to doing walking meditation that's one of the reasons for walking

129
00:11:38,280 --> 00:11:43,440
meditation as well it allows you to develop effort and energy if you sit

130
00:11:43,440 --> 00:11:47,800
all the time it's easy to fall asleep if you do walking as well you'll find that

131
00:11:47,800 --> 00:11:53,520
when you do the sitting you you feel charged afterwards so this is the the

132
00:11:53,520 --> 00:12:00,080
fifth method the sixth method is a specific meditation technique and it's

133
00:12:00,080 --> 00:12:04,840
probably not applicable for most beginner meditators but if you've been

134
00:12:04,840 --> 00:12:08,960
practicing meditation for a while and I suppose even even as a beginner you

135
00:12:08,960 --> 00:12:17,120
could attempt it the technique is to think of day as night to resolve on the

136
00:12:17,120 --> 00:12:22,840
meditation or the awareness of light even though it's dark at night because

137
00:12:22,840 --> 00:12:27,160
of course night is the darkness is something that triggers our recollection of

138
00:12:27,160 --> 00:12:31,960
oh now it's time to fall asleep and causes us to start to feel drowsy our

139
00:12:31,960 --> 00:12:37,960
mind triggers the drowsiness routine based on the fact that it's dark so you

140
00:12:37,960 --> 00:12:42,880
trick yourself you tell yourself that it's light or you you envision you can

141
00:12:42,880 --> 00:12:47,880
close with your eyes closed you can imagine light or become aware of light I

142
00:12:47,880 --> 00:12:51,640
know there are monks who told me that you should do meditation with a bright

143
00:12:51,640 --> 00:12:56,240
light on and I suppose this could help as well I've tried it I don't it

144
00:12:56,240 --> 00:13:04,120
doesn't seem that useful but I suppose it could be at least to some extent but

145
00:13:04,120 --> 00:13:08,240
here the point is to actually get it in your mind that it's light that there is

146
00:13:08,240 --> 00:13:13,400
light because that will trigger the energy in the mind it's a way of stimulating

147
00:13:13,400 --> 00:13:18,640
energy he said thinking of though it's night to think of it as day and to get

148
00:13:18,640 --> 00:13:22,560
an idea in your mind if you're actually been practicing meditation for a while

149
00:13:22,560 --> 00:13:28,640
you can actually begin to envision in vision bright lights some people even

150
00:13:28,640 --> 00:13:33,680
get very distracted by these lights or colors or pictures or so on and we have to

151
00:13:33,680 --> 00:13:37,720
remind meditators not to get off track and to acknowledge them as seeing

152
00:13:37,720 --> 00:13:42,680
seeing to just remind yourself this is a visual stimulus it's not a magical

153
00:13:42,680 --> 00:13:47,520
sensation magical experience or certainly not the path that leads to enlightenment

154
00:13:47,520 --> 00:13:52,800
it's the wrong path but in this case it has a limited benefit of bringing about

155
00:13:52,800 --> 00:13:59,400
energy and effort it has its use even though it's not the path and that's the

156
00:13:59,400 --> 00:14:05,440
sixth method if that doesn't work the Buddha said lie down and the Buddha's way

157
00:14:05,440 --> 00:14:09,600
of lying down was to lie down on one on your side not in your stomach not in

158
00:14:09,600 --> 00:14:14,560
your back with the light on on one side and I'm not sure you can prop your head

159
00:14:14,560 --> 00:14:18,440
up or I don't think that's mentioned but the way I would often do it is to

160
00:14:18,440 --> 00:14:24,240
actually prop my head up on my elbow and it's a technique that I've I've seen

161
00:14:24,240 --> 00:14:30,720
other people use and monks use and it seems to very much keep you awake if

162
00:14:30,720 --> 00:14:34,720
you start to fall asleep your head starts to fall off your your arm and you wake

163
00:14:34,720 --> 00:14:38,600
up quite well it's also quite painful in the beginning it's something you

164
00:14:38,600 --> 00:14:44,280
have to develop it's a physical technique that you have to develop just like

165
00:14:44,280 --> 00:14:49,840
sitting cross-legged but at any rate lie down on one side and perhaps not

166
00:14:49,840 --> 00:14:54,920
propping the head up but at the very least resolving on getting up the Buddha

167
00:14:54,920 --> 00:14:59,000
said you're lying down you know you know you kind of given up at this point

168
00:14:59,000 --> 00:15:02,880
because you feel like you can't overcome the drowsiness so you're going to

169
00:15:02,880 --> 00:15:08,880
accept that you might fall asleep so you lie down and you say I'm going to get

170
00:15:08,880 --> 00:15:13,520
up in so many minutes or so many hours if it's time to sleep you say I'm

171
00:15:13,520 --> 00:15:18,760
going to sleep for four hours or however long you're going to sleep and I'm going

172
00:15:18,760 --> 00:15:23,280
to get up at such and such a time when you resolve think about before you go

173
00:15:23,280 --> 00:15:27,080
to sleep think only about that the fact that you're going to get up so that

174
00:15:27,080 --> 00:15:32,640
when that time comes you'll find that if you resolve in this way you're

175
00:15:32,640 --> 00:15:40,160
actually your mind is able to somehow wake you up at that time it's amazing

176
00:15:40,160 --> 00:15:45,560
how how advanced the mind really is that you'll find yourself waking up a few

177
00:15:45,560 --> 00:15:51,120
minutes before the hour that you were going to wake up or how many minutes

178
00:15:51,120 --> 00:15:56,360
you were going to sleep or so and then the Buddha said and then get up he said

179
00:15:56,360 --> 00:16:01,440
resolve in your mind I will not give into drowsiness and I will not become

180
00:16:01,440 --> 00:16:06,680
attached to the pleasure that comes from lying down because one of the things

181
00:16:06,680 --> 00:16:12,840
that we miss one of the great addictions that we miss is the addiction to sleep

182
00:16:12,840 --> 00:16:19,320
many people like to lie down for many many hours and sleep a lot and it's

183
00:16:19,320 --> 00:16:24,120
because of the physical pleasure that that comes from it but the problem with

184
00:16:24,120 --> 00:16:27,840
this physical pleasure is like all physical pleasures it's not permanent it's

185
00:16:27,840 --> 00:16:32,520
not lasting and it's not the case that the more you sleep the more happy you

186
00:16:32,520 --> 00:16:39,000
feel even fact feel more depressed and more lethargic and less energetic

187
00:16:39,000 --> 00:16:45,960
less bright and and and you know at peace with yourself you feel more

188
00:16:45,960 --> 00:16:53,440
distracted less awake and less alert so you know the Buddha said this is this

189
00:16:53,440 --> 00:16:59,040
last one you have to you have to make it you can't just lie down and say I'm

190
00:16:59,040 --> 00:17:03,480
going to go to sleep and fall asleep you have to be strict about it that I'm

191
00:17:03,480 --> 00:17:07,600
going to set myself how long I'm going to sleep and then when I wake up I'm

192
00:17:07,600 --> 00:17:12,800
going to get up and go on with my practice the Buddha said this is the way that a

193
00:17:12,800 --> 00:17:17,440
person should deal with drowsiness so rather than give my own thoughts on it or

194
00:17:17,440 --> 00:17:20,920
something that's vaguely based on the Buddha's teaching there you have

195
00:17:20,920 --> 00:17:31,280
directly as best as I could translate and paraphrase and expand upon it the

196
00:17:31,280 --> 00:17:36,320
Buddha's teaching on how to deal with drowsiness so thanks for the question

197
00:17:36,320 --> 00:17:38,840
from that help

