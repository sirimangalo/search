1
00:00:00,000 --> 00:00:07,360
How can one deal with social pressure, status, money, put upon oneself to get the

2
00:00:07,360 --> 00:00:13,960
expectations of close relatives and friends fulfilled? How can I explain to them

3
00:00:13,960 --> 00:00:24,760
the ideas of Buddhism? Explaining Buddhism to others is very difficult, can be

4
00:00:24,760 --> 00:00:33,080
done. The best thing is to practice yourself and be a source of information

5
00:00:33,080 --> 00:00:39,480
for people. So you should not try to impose Buddhism on others, proselytize.

6
00:00:39,480 --> 00:00:50,040
The most effective form of proselytization, proselytizing that I've seen is

7
00:00:50,040 --> 00:00:57,120
the Mormons, I think. Probably the Jehovah's Witnesses are the same, but the

8
00:00:57,120 --> 00:01:03,720
Mormons, they don't push their religion on you. They just come visit you and

9
00:01:03,720 --> 00:01:10,040
then they just sit there and talk to you and let you ask them questions. That's

10
00:01:10,040 --> 00:01:13,200
what it seems to me or maybe they're just afraid of me so they don't but

11
00:01:13,200 --> 00:01:16,160
they would just sit there and I'd be able to put all my questions to them and

12
00:01:16,160 --> 00:01:25,240
test them out and scrutinize. They end up being very approachable and you

13
00:01:25,240 --> 00:01:31,640
likeable and you can see that that's really, that seemed to be how they got

14
00:01:31,640 --> 00:01:36,360
the good word out there, just by being kind and approachable, being nice

15
00:01:36,360 --> 00:01:43,960
people, by being real people and by being fallible, by not pretending to be

16
00:01:43,960 --> 00:01:50,160
all-knowing omnipotent beings and with all the answers. That's really

17
00:01:50,160 --> 00:02:02,440
where once you become self-righteous or become sure of yourself, then people

18
00:02:02,440 --> 00:02:07,840
then you really lose people, dogmatic and so on. But when you talk about your

19
00:02:07,840 --> 00:02:12,320
experiences rather than pretending or trying to be Buddha, trying to say I

20
00:02:12,320 --> 00:02:19,040
have to be Buddha, in order in order to help these people and answer their

21
00:02:19,040 --> 00:02:24,360
questions, just answers best you can say. Well I really don't know. For example

22
00:02:24,360 --> 00:02:29,800
the best example we have is Asaji who was an Arhan and yet when he was asked

23
00:02:29,800 --> 00:02:35,720
after becoming an Arhan five days into his practice, after being asked by

24
00:02:35,720 --> 00:02:42,440
Bhatisa, you know, what is the Buddha taught? He said the first thing he said is no I'm I'm only newly gone forth

25
00:02:42,440 --> 00:02:47,240
I don't have a good comprehensive understanding of this teaching which you

26
00:02:47,240 --> 00:02:50,840
know is true that he didn't because only a Buddha can have that kind of

27
00:02:50,840 --> 00:02:56,240
understanding. So he hadn't gained all this knowledge that he would gain by being

28
00:02:56,240 --> 00:03:04,000
in close contact with the Buddha for over time, even even as an Arhan. So it's

29
00:03:04,000 --> 00:03:09,480
important to be fallible to to to not pretend to be something you're not and to

30
00:03:09,480 --> 00:03:13,720
just talk about why you've practiced Buddhism and what benefit you see in it.

31
00:03:13,720 --> 00:03:19,080
You know and explain that yeah yeah you're not you don't really know but you've

32
00:03:19,080 --> 00:03:23,760
talked to these people and and it really is you know it's logical as reasonable

33
00:03:23,760 --> 00:03:31,880
it it seems to work you know when you put it into practice you do seem to feel

34
00:03:31,880 --> 00:03:37,200
happier and more peaceful which is really what we're trying to gain. When you

35
00:03:37,200 --> 00:03:42,200
clear your mind up at that moment when you're able to be clear in mind when

36
00:03:42,200 --> 00:03:52,640
you're able to give up worldly pursuits and give up the requirements that are

37
00:03:52,640 --> 00:03:56,680
put on you by society then you really find true peace and happiness and so as a

38
00:03:56,680 --> 00:04:03,400
result you find it difficult to be ambitious about things which you know are

39
00:04:03,400 --> 00:04:11,360
really going to just bring you stress and suffering and dissatisfaction and

40
00:04:11,360 --> 00:04:19,480
ultimately disappointment. So talking openly because really what you're

41
00:04:19,480 --> 00:04:23,400
dealing with is other people's neuroses other people's brainwashing and

42
00:04:23,400 --> 00:04:27,960
conditioning that these things are good. When you deal with people who have

43
00:04:27,960 --> 00:04:33,720
expectations for you while those expectations were pushed into them were

44
00:04:33,720 --> 00:04:38,080
ingrained into them by society, by parents sometimes they were beaten as

45
00:04:38,080 --> 00:04:42,360
children, our parents you know may have had a horrible child you know very

46
00:04:42,360 --> 00:04:47,400
repressive childhoods that have caused them to be as a result repressive

47
00:04:47,400 --> 00:04:55,720
towards us and demanding of us and you know friends are totally in

48
00:04:55,720 --> 00:05:00,720
indoctrinated by society so they they always come back and tell us how

49
00:05:00,720 --> 00:05:05,360
we're indoctrinated we had a woman everyone said to brainwashed all these

50
00:05:05,360 --> 00:05:08,760
people you know who are totally brainwashed by society into thinking and

51
00:05:08,760 --> 00:05:13,320
believing such ridiculous things that they're irrational and that they'll

52
00:05:13,320 --> 00:05:18,280
even accept as being irrational like someone came and said he knows that

53
00:05:18,280 --> 00:05:23,440
attachments are wrong you know this is Buddha taught this and I know it but

54
00:05:23,440 --> 00:05:28,040
still we have these and so and then they can turn around and call us brainwashed

55
00:05:28,040 --> 00:05:32,640
right when really if they're honest with themselves they can see that

56
00:05:32,640 --> 00:05:41,400
they're brainwashed so be honest and be demanding and not be expecting too

57
00:05:41,400 --> 00:05:48,000
much because with given the fact that these are are really mental states that

58
00:05:48,000 --> 00:05:50,400
they have to deal with the only thing that's going to really help them is

59
00:05:50,400 --> 00:05:56,200
meditation and people people who are meditative people who are letting go not

60
00:05:56,200 --> 00:06:00,480
people who are putting expectations on them so it would be wrong for you to

61
00:06:00,480 --> 00:06:05,160
expect them to give up their expectations but you should be a sounding board

62
00:06:05,160 --> 00:06:13,800
like a blotter almost you you pick up and just just take in their like a vacuum

63
00:06:13,800 --> 00:06:21,000
you take in their stresses and their requirements and you listen and you accept

64
00:06:21,000 --> 00:06:25,000
them but you don't react and you know and when they get angry at you and call

65
00:06:25,000 --> 00:06:32,960
you a loser a bum and so on except it and react with it rationally and and

66
00:06:32,960 --> 00:06:36,120
as though they were talking about someone else someone came to you and said

67
00:06:36,120 --> 00:06:41,360
that guy's a bum well you'd be able to act react rationally that person is

68
00:06:41,360 --> 00:06:45,760
useless and I want that person to this and that you'd be able to look and say

69
00:06:45,760 --> 00:06:52,480
well you know sometimes you know people you know you can't force people to do

70
00:06:52,480 --> 00:06:56,120
things or whatever or this is just leading you to suffering if they don't do

71
00:06:56,120 --> 00:06:59,880
this you're just going to suffer so so kind of taking yourself out of the

72
00:06:59,880 --> 00:07:05,720
picture which is accomplished by giving up cellofins on

