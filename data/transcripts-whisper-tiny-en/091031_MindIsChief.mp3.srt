1
00:00:00,000 --> 00:00:22,100
Namato, nata nata, yes, that is so welcome everyone to today's online dhamma talk.

2
00:00:22,100 --> 00:00:38,100
Today I'm recording not only live on the internet, but also broadcasting into our new virtual reality platform.

3
00:00:38,100 --> 00:00:41,100
Virtual reality is interesting.

4
00:00:41,100 --> 00:00:51,100
It first seemed like a fairly silly idea, especially the idea of sitting in meditation and virtual reality.

5
00:00:51,100 --> 00:01:15,100
But I think as a means to sort of gather people together and give people a sort of a vision of some sort of community, which we're often lacking as Buddhists in the modern era being scattered across the globe.

6
00:01:15,100 --> 00:01:33,100
I think it has quite a bit of potential and it seems to have proved itself in my experiences using the virtual realities which have been set up, like second life and so on.

7
00:01:33,100 --> 00:01:44,100
But the great thing about what we set up now is that we're going to make a fully and completely Buddhist virtual reality.

8
00:01:44,100 --> 00:01:55,100
And I think this has an even greater potential so that when people want to connect with a Buddhist community, they know exactly where to go.

9
00:01:55,100 --> 00:02:04,100
And so I hope that starting in the New Year will be able to have that up and running.

10
00:02:04,100 --> 00:02:14,100
But for those of you who are not on virtual reality, I may talk so I'm going to still be broadcast over the internet on my website.

11
00:02:14,100 --> 00:02:20,100
And for those of you who aren't able to listen in there, I'm going to record them.

12
00:02:20,100 --> 00:02:29,100
And having recorded them, you can listen to them at your leisure, also download it from the website.

13
00:02:29,100 --> 00:02:34,100
So today I thought I would start something a little bit different.

14
00:02:34,100 --> 00:02:44,100
I've been giving talks and doing suited to study courses and even telling stories.

15
00:02:44,100 --> 00:02:52,100
I thought I would do something a little bit similar to the suited to study, except now we won't be studying Suta.

16
00:02:52,100 --> 00:02:55,100
A discourse will be studying verses.

17
00:02:55,100 --> 00:03:00,100
And these are the verses of the Dhamabada.

18
00:03:00,100 --> 00:03:18,100
The Dhamabada is a classic Buddhist text which contains some of the most pithy statements of the Lord Buddha, those statements, which were thought to have profound meaning.

19
00:03:18,100 --> 00:03:27,100
And so I'm going to start today with the first two verses of the Dhamabada and the first set of verses in the Dhamabada are in pairs.

20
00:03:27,100 --> 00:03:30,100
So that's why we have two here today.

21
00:03:30,100 --> 00:03:34,100
And these verses with the Pali are as follows.

22
00:03:34,100 --> 00:03:44,100
Manopu, Bhankama, Dhamma, Manosaita, Manomia, Manasaji, Badutena, Paasatiwa, Karotiwa.

23
00:03:44,100 --> 00:04:00,100
And it's translated as Mind is the forerunner of all states, all Dhammas.

24
00:04:00,100 --> 00:04:06,100
Mind is their chief, and they are mind-made.

25
00:04:06,100 --> 00:04:19,100
If one speaks or acts, speaks or acts with a wicked mind, then suffering follows one, just as the wheel follows the hoof of the ox that pulls it.

26
00:04:19,100 --> 00:04:30,100
So this is a statement of the Lord Buddha that the mind is the most important in everything that we do in the production of karma.

27
00:04:30,100 --> 00:04:48,100
The second verse is almost the same, it just ends, Bhasatiwa, Karotiwa, Manasaji, Bhasani, Paasatiwa, Karotiwa, Dattonang, Sukamunweti, Chayawa, Chayawa, Anat, Bahini.

28
00:04:48,100 --> 00:05:04,100
Like a shadow that never leaves when one acts or speaks with a pure heart, a bright heart, Manosa, a bright mind, happiness follows one, just as a shadow that never leaves.

29
00:05:04,100 --> 00:05:15,100
So whether it's good or evil, these two verses are explaining that it's the mind that qualifies a action.

30
00:05:15,100 --> 00:05:29,100
And this was a profound statement, it's profound even today, but in the Buddha's time it was quite profound on a religious level because the doctrine of karma, of course, was a hotly debated issue.

31
00:05:29,100 --> 00:05:49,100
And mostly it was debated over which actions of speech or body were going to be most potent and were going to have an effect, were going to give results.

32
00:05:49,100 --> 00:06:02,100
There was no thought that somehow it should be the mind that was most important. For most people it was simply karma was a ritual, karma actually means action.

33
00:06:02,100 --> 00:06:12,100
So as I've said before, if you want to look at it one way, the Buddha didn't teach the doctrine of karma, he thought against it.

34
00:06:12,100 --> 00:06:22,100
He thought that it wasn't the actions which were most important. It was the mind behind them.

35
00:06:22,100 --> 00:06:29,100
And this is what he's saying in these two verses.

36
00:06:29,100 --> 00:06:49,100
And so this radically changed how people looked at this idea of karma, people having in the Vedas and even the Upanishads it was considered to be that certain actions, yoga for instance, there were certain asanas that were powerful and so on, certain mantras that had power to them.

37
00:06:49,100 --> 00:07:04,100
And there's a funny story that I always tell about how this monk he was in Singapore giving a dhamatak and he was asked,

38
00:07:04,100 --> 00:07:21,100
is it enough to chant the name, is it enough to chant namu amitafa to go to the Pure Land? Is that enough to go to the Pure Land? Or is that enough to be a good Buddhist? And he gave a story first about his mother and her son,

39
00:07:21,100 --> 00:07:36,100
or they've given the story before. But then he said, there's no text in the Mahayana which says that if you chant the namu amitafa, you're going to be reborn in the Pure Land.

40
00:07:36,100 --> 00:07:42,100
There's not one text, this is what he said. Now I'm not familiar with these texts, so I'm going to go by his authority.

41
00:07:42,100 --> 00:08:01,100
He said, there may be some people who try to say that this is the case, but there's no Kormahayana text that says this. The Buddha never said that. He said, and the Buddha said, chanting the Buddha never said that if you chant namu amitafa, you're going to be reborn in the Pure Land.

42
00:08:01,100 --> 00:08:22,100
What he did say is if you chant namu amitafa with a pure mind, you'll be reborn in the Pure Land. It also follows from that, or it's also true that if you chant Coca-Cola with a pure mind, you'll be reborn in the Pure Land.

43
00:08:22,100 --> 00:08:37,100
And this was a hit with the Singaporean audience. But it's also very indicative of the sort of way that Buddhists look at Karmah, that we're not.

44
00:08:37,100 --> 00:08:47,100
There's no special words that we can use or special actions that we can hold on to that are going to somehow bring us to peace and happiness and freedom from suffering.

45
00:08:47,100 --> 00:08:52,100
Since all of these things are qualities of the mind.

46
00:08:52,100 --> 00:08:59,100
So the stories behind these verses are quite good examples as well.

47
00:08:59,100 --> 00:09:03,100
The first verse about when you do a bad deed, it follows you.

48
00:09:03,100 --> 00:09:11,100
It's a story of a blind elder who had become enlightened at the same moment as going blind.

49
00:09:11,100 --> 00:09:18,100
So he was practicing so strenuously and he actually needed some rest to lie down and rest his eyes.

50
00:09:18,100 --> 00:09:22,100
And because he didn't rest, because he didn't lie down, he went blind.

51
00:09:22,100 --> 00:09:39,100
But as he was practicing and he realized that his eyes were dying, his eyes were becoming destroyed through his practice, that he actually was able to give up his attachment to everything and able to let go and become free from suffering.

52
00:09:39,100 --> 00:09:48,100
So for this reason, he was called Jakupala, one who protects his eyes, which is quite a funny play on words.

53
00:09:48,100 --> 00:09:53,100
Because the truth is he let his eyes become destroyed.

54
00:09:53,100 --> 00:09:59,100
But the eye that he protected was the eye of Dhamma.

55
00:09:59,100 --> 00:10:14,100
So his inner eye, his inner understanding, his inner vision was protected and he was most resolute in his protection of that vision.

56
00:10:14,100 --> 00:10:16,100
So he was given the name Jakupala.

57
00:10:16,100 --> 00:10:24,100
And after he was enlightened, one night he was doing walking meditation late at night or in the early morning.

58
00:10:24,100 --> 00:10:31,100
And it had rained the night before, it had rained all night and as the rain was letting up, there were these insects, this little flying insects,

59
00:10:31,100 --> 00:10:43,100
termites or something, that fly around and end up giving up their wings and just sort of flailing about on the ground and the puddles and so on.

60
00:10:43,100 --> 00:10:49,100
And as he was doing walking meditation, he ended up stepping on many of these insects without knowing it.

61
00:10:49,100 --> 00:10:57,100
And in the morning, when the other monks came around, they asked the monks in the monastery, they said,

62
00:10:57,100 --> 00:11:02,100
who was doing walking meditation in this area and they said it was Jakupala.

63
00:11:02,100 --> 00:11:08,100
And when they found that out, they were shocked and they said, this monk has been committing murder.

64
00:11:08,100 --> 00:11:13,100
It has been killing these living beings and they went to see the Buddha.

65
00:11:13,100 --> 00:11:21,100
And the Buddha said to them, he said, my son Jakupala is totally innocent of this.

66
00:11:21,100 --> 00:11:23,100
He didn't know that he was doing.

67
00:11:23,100 --> 00:11:26,100
He didn't know that those insects were dying.

68
00:11:26,100 --> 00:11:28,100
He had no clue.

69
00:11:28,100 --> 00:11:30,100
Addy known, he wouldn't have done it.

70
00:11:30,100 --> 00:11:36,100
And then he gave this verse, that mind is what makes something an evil act.

71
00:11:36,100 --> 00:11:40,100
If one acts or speaks with an evil mind, this is when suffering follows.

72
00:11:40,100 --> 00:11:44,100
This is when it's considered a bad karma.

73
00:11:44,100 --> 00:11:48,100
And the second verse is the opposite.

74
00:11:48,100 --> 00:11:55,100
So the first verse indicates that no matter how evil a deed might appear, if it doesn't have the mind,

75
00:11:55,100 --> 00:12:04,100
if it doesn't come from one's intentions, if it isn't intended, then it has no fruit.

76
00:12:04,100 --> 00:12:14,100
It can't bear any fruit in the person's mind, and so therefore it's not considered a bad deed.

77
00:12:14,100 --> 00:12:27,100
And this also is something that we can use to explain how we can approach the idea of killing or murder in Buddhism.

78
00:12:27,100 --> 00:12:32,100
So when we understand that when we know that there is a living being present,

79
00:12:32,100 --> 00:12:36,100
then we actively make effort to try to end its life.

80
00:12:36,100 --> 00:12:38,100
And then we succeed.

81
00:12:38,100 --> 00:12:40,100
This is when it's considered murder.

82
00:12:40,100 --> 00:12:44,100
But when suppose we're walking down the street and we step on an ant, this is what many people ask.

83
00:12:44,100 --> 00:12:48,100
If you didn't know that the ant was there, then it's not considered murder.

84
00:12:48,100 --> 00:12:51,100
If you had no clue that it died, then you could die.

85
00:12:51,100 --> 00:12:52,100
You could pass away.

86
00:12:52,100 --> 00:12:55,100
You could live your life and be totally at ease with yourself.

87
00:12:55,100 --> 00:12:59,100
And it will never bring any unwholesome consequence to you.

88
00:12:59,100 --> 00:13:03,100
Because you hadn't a clue, you hadn't the intention.

89
00:13:03,100 --> 00:13:08,100
You could have been totally mindful as this monk was walking back and forth.

90
00:13:08,100 --> 00:13:17,100
And still the being has to die because of its own karma, because of the nature of the universe, or its way in life.

91
00:13:17,100 --> 00:13:27,100
And for instance, for things like vegetarianism, when people ask why Buddhists can eat meat, and to bat, they eat meat,

92
00:13:27,100 --> 00:13:32,100
and the teravada countries, they eat meat, why they can do this.

93
00:13:32,100 --> 00:13:37,100
This goes back to our understanding of what is a good and bad karma,

94
00:13:37,100 --> 00:13:43,100
what is an active karma, and what is an act which is currently inactive.

95
00:13:43,100 --> 00:13:46,100
And again, it has to do with our intention.

96
00:13:46,100 --> 00:13:51,100
When you eat something that's already dead, there's no intention to kill,

97
00:13:51,100 --> 00:13:55,100
there's no intention to hurt other living beings.

98
00:13:55,100 --> 00:14:05,100
And this is kind of a signature of Buddhism across the board, I think, is this idea that

99
00:14:05,100 --> 00:14:09,100
Buddhist practice is not an intellectual practice.

100
00:14:09,100 --> 00:14:14,100
You can't intellectualize it and think about it and expect to get some result.

101
00:14:14,100 --> 00:14:22,100
You can't say, I'm going to be moral by intellectualizing, or I'm going to develop a morality through

102
00:14:22,100 --> 00:14:26,100
an intellectual basis. It has to come from the heart.

103
00:14:26,100 --> 00:14:36,100
It comes from an understanding that this is a living being, and this heart felt

104
00:14:36,100 --> 00:14:43,100
desire to see that being free from suffering, and to wish for them to not fall into

105
00:14:43,100 --> 00:14:48,100
the kind of suffering that comes about from murder.

106
00:14:48,100 --> 00:14:54,100
When you eat something that's already dead, this certainly isn't the case.

107
00:14:54,100 --> 00:15:01,100
The mind can be completely free from guilt, free from worry, free from anger, free from greed.

108
00:15:01,100 --> 00:15:09,100
In the same way as if one is to eat other inanimate food stuffs.

109
00:15:09,100 --> 00:15:18,100
For instance, eating pesticide-ridden carrots, or vegetables, or fruits, or using all sorts of things,

110
00:15:18,100 --> 00:15:27,100
which may have, on an intellectual level, may have ethically questionable origins,

111
00:15:27,100 --> 00:15:32,100
using all sorts of things that, if you were to intellectualize it and look on a societal level,

112
00:15:32,100 --> 00:15:35,100
these things may not be ethically sound to use.

113
00:15:35,100 --> 00:15:41,100
In fact, many of the things we use are ethically questionable coffee, for instance,

114
00:15:41,100 --> 00:15:47,100
simply drinking coffee is an ethically questionable act on an intellectual level,

115
00:15:47,100 --> 00:15:50,100
but morality doesn't cover this.

116
00:15:50,100 --> 00:15:52,100
Buddhist morality isn't concerned with this.

117
00:15:52,100 --> 00:15:57,100
We're concerned only with the state of one's heart when one doesn't, when one does something,

118
00:15:57,100 --> 00:16:03,100
and if one acts or speaks with the pure heart as we're going to see in the next verse,

119
00:16:03,100 --> 00:16:08,100
then no matter how small it is, it brings happiness.

120
00:16:08,100 --> 00:16:11,100
No matter how small the physical action,

121
00:16:11,100 --> 00:16:19,100
simply the good intention in the mind nullifies any physical act,

122
00:16:19,100 --> 00:16:23,100
so people who are superstitious and think that doing this or that act,

123
00:16:23,100 --> 00:16:27,100
simply by the ritual act, it's going to bring a bad luck, for instance,

124
00:16:27,100 --> 00:16:30,100
walking under a ladder or so on.

125
00:16:30,100 --> 00:16:35,100
This, the Lord Buddha was totally against and totally denied.

126
00:16:35,100 --> 00:16:37,100
So what's the second verse about?

127
00:16:37,100 --> 00:16:43,100
The second verse, the story behind it is that this boy who was very ill

128
00:16:43,100 --> 00:16:46,100
and his father wouldn't call a doctor,

129
00:16:46,100 --> 00:16:50,100
and because his father was a millionaire who was very, very stingy.

130
00:16:50,100 --> 00:16:54,100
And so instead of calling an a doctor, he went to the doctor and asked him,

131
00:16:54,100 --> 00:16:59,100
asked the doctors what sort of medicine they would prescribe for such and such an illness.

132
00:16:59,100 --> 00:17:02,100
And when the doctor said this or that medicine,

133
00:17:02,100 --> 00:17:07,100
so he went and found the medicine himself and tried to administer it to his son.

134
00:17:07,100 --> 00:17:11,100
As a result of his bumbling efforts, the son got worse,

135
00:17:11,100 --> 00:17:14,100
to the point where the son was about to die,

136
00:17:14,100 --> 00:17:18,100
at which point the father realized that he had to call a doctor,

137
00:17:18,100 --> 00:17:22,100
but it was too late and the boy was on his deathbed.

138
00:17:22,100 --> 00:17:28,100
And so the father, seeing that the boy was on his deathbed,

139
00:17:28,100 --> 00:17:32,100
what did he do rather than console him and keep him in the quiet indoors of the house?

140
00:17:32,100 --> 00:17:39,100
He thought to himself, oh dear, now if people come to see him,

141
00:17:39,100 --> 00:17:44,100
to say goodbye to him and to try to console him and comfort him,

142
00:17:44,100 --> 00:17:47,100
they're going to come into that mansion and see all of my treasure,

143
00:17:47,100 --> 00:17:53,100
and then they're going to be angry and jealous and want a share of my treasure.

144
00:17:53,100 --> 00:17:59,100
So he took the boy and had his bed moved outside of the house

145
00:17:59,100 --> 00:18:05,100
so that if people came to visit, they wouldn't be able to come in the house and see all of his treasure,

146
00:18:05,100 --> 00:18:09,100
all of his wealth.

147
00:18:09,100 --> 00:18:14,100
And so this was kind of a terrible thing for a father to do to his son,

148
00:18:14,100 --> 00:18:18,100
but it ended up being a great luck for the boy,

149
00:18:18,100 --> 00:18:21,100
because on that morning the Lord Buddha was going on arms round,

150
00:18:21,100 --> 00:18:27,100
and he went on arms round past the millionaire's mansion.

151
00:18:27,100 --> 00:18:30,100
And the boy was dying, lying on his deathbed,

152
00:18:30,100 --> 00:18:35,100
and he was looking in the direction that the Buddha was walking,

153
00:18:35,100 --> 00:18:37,100
and the Buddha stopped and looked at him.

154
00:18:37,100 --> 00:18:44,100
And when the boy looked up at the Buddha and was just so full of awe and inspiration,

155
00:18:44,100 --> 00:18:50,100
simply seeing the calm and peaceful countenance of the Lord Buddha,

156
00:18:50,100 --> 00:18:53,100
he thought to himself,

157
00:18:53,100 --> 00:18:57,100
wow, I've never had the chance to see such a beautiful,

158
00:18:57,100 --> 00:18:59,100
such a wonderful person.

159
00:18:59,100 --> 00:19:04,100
So he tried to lift up his hands to his chest and pay respect to the Buddha,

160
00:19:04,100 --> 00:19:06,100
but even that he couldn't do.

161
00:19:06,100 --> 00:19:12,100
He was so weak and so close to death that all he could do was in his mind make this intention,

162
00:19:12,100 --> 00:19:16,100
trying to lift his hands up to his chest and pay respect to the Buddha,

163
00:19:16,100 --> 00:19:21,100
but his mind was so full of happiness and joy, just at seeing such a beautiful,

164
00:19:21,100 --> 00:19:30,100
such a perfect, such a advanced spirit truly in light and being

165
00:19:30,100 --> 00:19:34,100
that when he died he was born in heaven.

166
00:19:34,100 --> 00:19:36,100
He was born as an angel.

167
00:19:36,100 --> 00:19:42,100
And then the story goes that he came down and talked with his father

168
00:19:42,100 --> 00:19:53,100
and talked with the Buddha and showed that just by making this determination

169
00:19:53,100 --> 00:19:56,100
in his mind and creating this state of clarity,

170
00:19:56,100 --> 00:20:00,100
this state of peace, this state of happiness and joy in his mind,

171
00:20:00,100 --> 00:20:04,100
he was able to be reborn in the heavenly world.

172
00:20:04,100 --> 00:20:06,100
And so the monks asked, how is this possible?

173
00:20:06,100 --> 00:20:10,100
How is it possible that without doing anything,

174
00:20:10,100 --> 00:20:14,100
simply by making this determination in his mind,

175
00:20:14,100 --> 00:20:16,100
by wanting to pay respect,

176
00:20:16,100 --> 00:20:21,100
by wanting to do something to show his admiration for the Buddha,

177
00:20:21,100 --> 00:20:24,100
he was born in heaven.

178
00:20:24,100 --> 00:20:29,100
And this is when the Lord would give this one verse,

179
00:20:29,100 --> 00:20:35,100
which is Manasad-Jipasaniyana,

180
00:20:35,100 --> 00:20:40,100
who is a diva, who is a diva, and one acts with a pure mind,

181
00:20:40,100 --> 00:20:48,100
or speaks with a pure mind, happiness follows like a shadow that never leaves.

182
00:20:48,100 --> 00:20:53,100
So what we're going to see throughout all of these verses,

183
00:20:53,100 --> 00:20:56,100
and what we should see throughout all of the Buddha's teaching,

184
00:20:56,100 --> 00:21:04,100
is that the idea of purifying one's mind holds predominance over everything else,

185
00:21:04,100 --> 00:21:06,100
that we can do, or we can say good deeds,

186
00:21:06,100 --> 00:21:08,100
we can try to help other people,

187
00:21:08,100 --> 00:21:12,100
we can build ourselves up,

188
00:21:12,100 --> 00:21:14,100
or make ourselves look like this or that,

189
00:21:14,100 --> 00:21:17,100
or make ourselves out to be this or that,

190
00:21:17,100 --> 00:21:20,100
or do whatever we want externally.

191
00:21:20,100 --> 00:21:29,100
And it won't nearly have the fraction of the effect

192
00:21:29,100 --> 00:21:33,100
of simply sitting down, closing one's eyes,

193
00:21:33,100 --> 00:21:37,100
and trying to look and see and understand one's own mind,

194
00:21:37,100 --> 00:21:41,100
and the way one's hard works,

195
00:21:41,100 --> 00:21:45,100
and the good and the bad things that exist in our minds,

196
00:21:45,100 --> 00:21:48,100
trying to develop our minds to keep our minds from

197
00:21:48,100 --> 00:21:51,100
straying off into worry and doubt,

198
00:21:51,100 --> 00:21:56,100
and anger and fear, and craving and addiction,

199
00:21:56,100 --> 00:22:01,100
and eco, and all of these unwholesome unpleasant,

200
00:22:01,100 --> 00:22:04,100
undesirable minds states,

201
00:22:04,100 --> 00:22:07,100
and trying to develop in our minds states of peace

202
00:22:07,100 --> 00:22:11,100
and trying to quality states of clarity,

203
00:22:11,100 --> 00:22:15,100
and trying to understand how the mind works

204
00:22:15,100 --> 00:22:18,100
to give rise to these unwholesome states.

205
00:22:18,100 --> 00:22:21,100
What is it that we're not seeing clearly?

206
00:22:21,100 --> 00:22:27,100
What is it that is going on to create these unwholesome minds states?

207
00:22:27,100 --> 00:22:31,100
Coming to look at the phenomena that arise,

208
00:22:31,100 --> 00:22:32,100
whether it be things that we see,

209
00:22:32,100 --> 00:22:35,100
or the things that we hear,

210
00:22:35,100 --> 00:22:39,100
whatever phenomena arise at the sixth census,

211
00:22:39,100 --> 00:22:45,100
coming to see them clearly so that they don't give rise to these unwholesome minds states.

212
00:22:45,100 --> 00:22:47,100
So meditation, for this reason,

213
00:22:47,100 --> 00:22:50,100
has the greatest predominance and Buddhism

214
00:22:50,100 --> 00:22:53,100
to sitting down, closing our eyes,

215
00:22:53,100 --> 00:22:56,100
or during any action we do during the day.

216
00:22:56,100 --> 00:23:01,100
Meditation doesn't have to be just a sitting exercise.

217
00:23:01,100 --> 00:23:04,100
Well, we're focusing on the mind.

218
00:23:04,100 --> 00:23:09,100
We can use the body as our sort of our intermediary,

219
00:23:09,100 --> 00:23:12,100
because the mind is what's doing the looking.

220
00:23:12,100 --> 00:23:15,100
So if the mind starts to focus on the body,

221
00:23:15,100 --> 00:23:20,100
it becomes clear, the mind's relationship with the objects

222
00:23:20,100 --> 00:23:23,100
becomes clear by itself.

223
00:23:23,100 --> 00:23:25,100
We simply make effort in the mind

224
00:23:25,100 --> 00:23:30,100
to focus on the body, and then we can see how the mind works.

225
00:23:30,100 --> 00:23:32,100
And when we try to focus on the body,

226
00:23:32,100 --> 00:23:36,100
then we see the mind wandering away.

227
00:23:36,100 --> 00:23:44,100
We try to keep the mind straight, the mind starts to get all bent out of shape.

228
00:23:44,100 --> 00:23:46,100
When we try to keep the mind in the present moment,

229
00:23:46,100 --> 00:23:53,100
it goes back to the past or goes ahead to the future.

230
00:23:53,100 --> 00:23:58,100
When we come to see how our mind is,

231
00:23:58,100 --> 00:24:01,100
we come to see all of the anger, how our mind becomes angry,

232
00:24:01,100 --> 00:24:03,100
at certain things that we don't like,

233
00:24:03,100 --> 00:24:05,100
and we're able to see the arising of the anger.

234
00:24:05,100 --> 00:24:10,100
We're able to watch the state or the experience

235
00:24:10,100 --> 00:24:13,100
that gives rise to the emotion.

236
00:24:13,100 --> 00:24:15,100
When we have wanting or addiction,

237
00:24:15,100 --> 00:24:18,100
we're able to see the experience that gives rise to it.

238
00:24:18,100 --> 00:24:22,100
When we see things as they are,

239
00:24:22,100 --> 00:24:23,100
we see that they're coming and going.

240
00:24:23,100 --> 00:24:26,100
We don't give rise to this liking or this disliking.

241
00:24:26,100 --> 00:24:30,100
We look at the experience of seeing or hearing or smelling,

242
00:24:30,100 --> 00:24:32,100
or tasting or feeling of thinking,

243
00:24:32,100 --> 00:24:34,100
and we see it for what it is.

244
00:24:34,100 --> 00:24:37,100
When the mind becomes pure,

245
00:24:37,100 --> 00:24:40,100
everything we do with the pure mind

246
00:24:40,100 --> 00:24:43,100
can be considered to be a good karma.

247
00:24:43,100 --> 00:24:46,100
Everything we do with an impure mind,

248
00:24:46,100 --> 00:24:49,100
no matter how good we might think it to be,

249
00:24:49,100 --> 00:24:53,100
if the mind is impure at the moment that we're doing it,

250
00:24:53,100 --> 00:24:55,100
it's considered to be a bad karma,

251
00:24:55,100 --> 00:24:59,100
and it will bring unwholesome unpleasant results.

252
00:25:01,100 --> 00:25:05,100
This is something that we see often when we do good deeds.

253
00:25:05,100 --> 00:25:07,100
We try to help people.

254
00:25:07,100 --> 00:25:10,100
You can see it often either in your soul for another people.

255
00:25:10,100 --> 00:25:12,100
We're trying to do these good deeds,

256
00:25:12,100 --> 00:25:14,100
and we're setting out to,

257
00:25:14,100 --> 00:25:17,100
we have this intention to help people,

258
00:25:17,100 --> 00:25:22,100
whether it be to give to charity or set up a charity

259
00:25:22,100 --> 00:25:26,100
or even to teach and to spread Buddhism.

260
00:25:26,100 --> 00:25:29,100
Then we find ourselves getting angry or getting conceited,

261
00:25:29,100 --> 00:25:37,100
or getting even greedy or attached to gain or fame or so on.

262
00:25:37,100 --> 00:25:40,100
It turns out to be a very unwholesome act,

263
00:25:40,100 --> 00:25:42,100
or at least a very mixed act,

264
00:25:42,100 --> 00:25:44,100
whether it's some wholesomeness,

265
00:25:44,100 --> 00:25:48,100
and there's also a great deal of unwholesomeness.

266
00:25:48,100 --> 00:25:50,100
As I've said before,

267
00:25:50,100 --> 00:25:54,100
we can look at ourselves as like a spring,

268
00:25:54,100 --> 00:26:01,100
a mountain spring flowing down to the rivers and valleys below.

269
00:26:03,100 --> 00:26:06,100
If the spring is impure,

270
00:26:06,100 --> 00:26:09,100
if it's impure at the source,

271
00:26:09,100 --> 00:26:13,100
then no matter how strong it is or no matter how far the water goes,

272
00:26:13,100 --> 00:26:15,100
even all the way to reach the ocean,

273
00:26:15,100 --> 00:26:19,100
it's going to be impure all the way down the way.

274
00:26:20,100 --> 00:26:23,100
Whatever animals come to try to drink from it,

275
00:26:23,100 --> 00:26:24,100
they won't be able to drink,

276
00:26:24,100 --> 00:26:26,100
they won't be able to get any nourishment,

277
00:26:26,100 --> 00:26:29,100
any refreshment from it,

278
00:26:29,100 --> 00:26:31,100
because of its impurity,

279
00:26:31,100 --> 00:26:34,100
and they'll become sick and suffer as a result.

280
00:26:34,100 --> 00:26:37,100
The same goes for all of our actions,

281
00:26:37,100 --> 00:26:40,100
if our actions are impure in the heart,

282
00:26:40,100 --> 00:26:41,100
and we say to ourselves,

283
00:26:41,100 --> 00:26:42,100
I'm going to go out and do this,

284
00:26:42,100 --> 00:26:43,100
I'm going to go out and do that,

285
00:26:43,100 --> 00:26:46,100
help people in this way or that way.

286
00:26:46,100 --> 00:26:50,100
We find that we only end up creating more suffering

287
00:26:50,100 --> 00:26:52,100
because our minds are impure,

288
00:26:52,100 --> 00:26:54,100
because in our minds we still have anger,

289
00:26:54,100 --> 00:26:55,100
we still have greed,

290
00:26:55,100 --> 00:26:57,100
we still have resentment and so on.

291
00:26:59,100 --> 00:27:02,100
But if we work hard to purify our minds

292
00:27:02,100 --> 00:27:05,100
to get rid of these unwholesome states,

293
00:27:05,100 --> 00:27:09,100
we come to see clearly about things as they are.

294
00:27:09,100 --> 00:27:13,100
Then no matter how weak or how small the spring is,

295
00:27:13,100 --> 00:27:15,100
any animal that drinks,

296
00:27:15,100 --> 00:27:17,100
they're from,

297
00:27:17,100 --> 00:27:19,100
it can gain refreshment,

298
00:27:19,100 --> 00:27:21,100
it can gain satisfaction,

299
00:27:21,100 --> 00:27:23,100
because of the purity of the water,

300
00:27:23,100 --> 00:27:25,100
because of the purity of the source,

301
00:27:25,100 --> 00:27:27,100
and so the same with our actions,

302
00:27:27,100 --> 00:27:29,100
when our minds are pure,

303
00:27:29,100 --> 00:27:31,100
everything that we do,

304
00:27:31,100 --> 00:27:32,100
everything that we say,

305
00:27:32,100 --> 00:27:34,100
it doesn't really matter what we do

306
00:27:34,100 --> 00:27:37,100
or what we say or how we try to help people.

307
00:27:37,100 --> 00:27:39,100
Simply our being present

308
00:27:39,100 --> 00:27:41,100
has an effect on other people,

309
00:27:41,100 --> 00:27:44,100
and they're able to take us as an example,

310
00:27:44,100 --> 00:27:46,100
they're able to learn from us,

311
00:27:46,100 --> 00:27:48,100
they're able to gain from us.

312
00:27:48,100 --> 00:27:50,100
People want to be around us,

313
00:27:50,100 --> 00:27:53,100
want to associate and be friends

314
00:27:53,100 --> 00:27:55,100
and learn from us.

315
00:27:55,100 --> 00:27:58,100
The more we are able to purify our minds,

316
00:27:58,100 --> 00:28:01,100
people want to associate with us

317
00:28:01,100 --> 00:28:08,100
and gain benefit from us.

318
00:28:08,100 --> 00:28:11,100
So this is a very core teaching of the Lord Buddha,

319
00:28:11,100 --> 00:28:13,100
and this is the very beginning of the Dhammapada,

320
00:28:13,100 --> 00:28:16,100
the teaching on the mind,

321
00:28:16,100 --> 00:28:18,100
the importance of the mind,

322
00:28:18,100 --> 00:28:19,100
and how the mind comes first,

323
00:28:19,100 --> 00:28:21,100
how everything comes from the mind,

324
00:28:21,100 --> 00:28:23,100
whether it be good or bad deeds,

325
00:28:23,100 --> 00:28:25,100
whether it be good things or bad things,

326
00:28:25,100 --> 00:28:28,100
it all comes from the mind.

327
00:28:28,100 --> 00:28:32,100
So that's the Dhammapada I would like to give on this occasion.

328
00:28:32,100 --> 00:28:34,100
That's the talk for today.

329
00:28:34,100 --> 00:28:37,100
This is our first talk broadcasting in Buddha verse here,

330
00:28:37,100 --> 00:28:39,100
I am sitting in the amphitheater

331
00:28:39,100 --> 00:28:42,100
with two listeners,

332
00:28:42,100 --> 00:28:47,100
so we're going to take a snapshot here,

333
00:28:47,100 --> 00:29:02,100
and we can post this on the internet.

