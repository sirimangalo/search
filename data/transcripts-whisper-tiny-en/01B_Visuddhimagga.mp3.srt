1
00:00:00,000 --> 00:00:10,000
not yet come to virtue itself. We are an introductory part of the discussion.

2
00:00:10,000 --> 00:00:28,000
I think next time it will be better when you have read the passages. Today it's just listening to something you are not familiar with.

3
00:00:28,000 --> 00:00:34,000
So, you may be laughing some places.

4
00:00:34,000 --> 00:00:50,000
So, this is just the introductory talk before the other really explains what virtue is and then what is the meaning of the word,

5
00:00:50,000 --> 00:01:00,000
the sealer and what is what you and so on. So, he proceeded in this order.

6
00:01:00,000 --> 00:01:06,000
So, first he put questions and then he gives answers to these questions one by one.

7
00:01:06,000 --> 00:01:12,000
So, the first question is what is virtue? What is sealer?

8
00:01:12,000 --> 00:01:22,000
And the answer is it is the stage beginning with volition present in one who abstains from killing living things etc.

9
00:01:22,000 --> 00:01:28,000
So, when we abstain from killing living things or abstain from stealing and so on,

10
00:01:28,000 --> 00:01:36,000
a consciousness arises in our minds, right? What kind of consciousness, wholesome or unwholesome?

11
00:01:36,000 --> 00:01:41,000
When we abstain from it, wholesome. So, together with that wholesome consciousness,

12
00:01:41,000 --> 00:01:46,000
mental states or mental factors arise.

13
00:01:46,000 --> 00:01:52,000
Among these mental factors there is one which is called volition, chiedena.

14
00:01:52,000 --> 00:01:57,000
So, here the answer is what is virtue? Chiedena is virtue.

15
00:01:57,000 --> 00:02:05,000
So, when you say virtue, we mean chiedena and chiedena which accompanies the type of consciousness which arises in our minds

16
00:02:05,000 --> 00:02:13,000
when we abstain from killing, stealing and so on.

17
00:02:13,000 --> 00:02:18,000
For this he said in the Baddisambida, it is the name of a book, Baddisambida.

18
00:02:18,000 --> 00:02:27,000
It is, in our countries we include this in the text in the Surabirada.

19
00:02:27,000 --> 00:02:36,000
But, actually, something like Appentics with Sotapiraka.

20
00:02:36,000 --> 00:02:47,000
And the Baddisambida was not thought by the Buddha, but by the Venerable Saripoda, his chief disciple.

21
00:02:47,000 --> 00:02:58,000
And, the commentator, the Venerable Buddha goes ahead, much respect for this book, Baddisambida.

22
00:02:58,000 --> 00:03:05,000
He quotes from this book very often throughout this book, the Hishis book that we saw the mother.

23
00:03:05,000 --> 00:03:12,000
So, what is virtue? There is virtue as volition, virtue as consciousness-concomitant, virtue as restraint.

24
00:03:12,000 --> 00:03:19,000
virtue as non-aggression. This is what is said in that book, Baddisambida, Marga.

25
00:03:19,000 --> 00:03:27,000
So, in that book it is that volition is virtue.

26
00:03:27,000 --> 00:03:30,000
Mando-factors are virtue.

27
00:03:30,000 --> 00:03:34,000
Restrain is virtue. Non-transgression is virtue.

28
00:03:34,000 --> 00:03:41,000
So, what you can mean any of these things?

29
00:03:41,000 --> 00:03:46,000
Consciousness-concomitant just means what?

30
00:03:46,000 --> 00:03:50,000
According to our understanding, Chaitisiggas.

31
00:03:50,000 --> 00:03:53,000
So, sometimes Chaitisiggas are called virtue.

32
00:03:53,000 --> 00:03:56,000
Sometimes, restraint.

33
00:03:56,000 --> 00:04:01,000
We have evidences among the fifty-two Chaitisiggas.

34
00:04:01,000 --> 00:04:06,000
You remember them? Right speech, right action, right livelihood.

35
00:04:06,000 --> 00:04:08,000
So, they are called restraint here.

36
00:04:08,000 --> 00:04:10,000
And then there may be other restraints too.

37
00:04:10,000 --> 00:04:16,000
Marga as non-transgression and non-transgression is also called virtue.

38
00:04:16,000 --> 00:04:19,000
So, you know, you know, volition.

39
00:04:19,000 --> 00:04:25,000
Volition arises with the consciousness when we abstain from something.

40
00:04:25,000 --> 00:04:33,000
Marga as consciousness-concomitant is the abstinence in one who attains from killing living in so on, abstain from killing living things and so on.

41
00:04:33,000 --> 00:04:41,000
So, here consciousness-concomitant means abstainances, three abstainances.

42
00:04:41,000 --> 00:04:44,000
So, sometimes three abstainances are called virtue.

43
00:04:44,000 --> 00:04:51,000
So, sometimes Chaitina is called virtue, sometimes three abstainances are called virtue.

44
00:04:51,000 --> 00:05:01,000
And then, in another way, virtue as volition is the seven volition that accompany the first seven of the ten causes of action.

45
00:05:01,000 --> 00:05:06,000
Come in one who abandons the killing of living beings and so on.

46
00:05:06,000 --> 00:05:11,000
Now, they are a ten, what we call ten causes of action.

47
00:05:11,000 --> 00:05:30,000
That means abstaining from killing, abstaining from stealing from sexual misconduct, from lying, from backbiting, from harsh speech

48
00:05:30,000 --> 00:05:35,000
and from talking nonsense.

49
00:05:35,000 --> 00:05:40,000
So, this seven, and then we'll come to the three later.

50
00:05:40,000 --> 00:05:45,000
So, when the others say seven, he meant these.

51
00:05:45,000 --> 00:05:49,000
So, abstain from killing and so on.

52
00:05:49,000 --> 00:05:57,000
So, three misconduct by body and four misconduct by speech.

53
00:05:57,000 --> 00:05:59,000
So, they are seven.

54
00:05:59,000 --> 00:06:06,000
These seven kinds of volition are called volition here or stealer.

55
00:06:06,000 --> 00:06:16,000
What you ask consciousness-concomitant is the three remaining states consisting of non-covetousness, non-ill will and right view.

56
00:06:16,000 --> 00:06:28,000
These three plus the seven mentioned above are called ten causes of the wholesome action.

57
00:06:28,000 --> 00:06:36,000
So, non-covetousness, non-ill will and right view or right understanding.

58
00:06:36,000 --> 00:06:38,000
Stayed in the way beginning.

59
00:06:38,000 --> 00:06:43,000
Abandoning cover doesn't as it dwells with mine, free from cover doesn't as and so on.

60
00:06:43,000 --> 00:06:47,000
Now, what you ask restraint should be understood here as restraint in five years.

61
00:06:47,000 --> 00:06:50,000
There are five kinds of restraints mentioned in the soldiers.

62
00:06:50,000 --> 00:06:59,000
The first is restraint by the rules of community.

63
00:06:59,000 --> 00:07:03,000
Rules of community really means rules or months.

64
00:07:03,000 --> 00:07:06,000
So, there are 227 rules or months.

65
00:07:06,000 --> 00:07:15,000
So, when a man keeps these rules and he has to restrain his bodily actions and speech of verbal actions.

66
00:07:15,000 --> 00:07:20,000
So, there is a strain according to the rules of party mocha.

67
00:07:20,000 --> 00:07:25,000
Restraint by mindfulness, mindfulness is sometimes called restraint.

68
00:07:25,000 --> 00:07:28,000
Restraint by knowledge and understanding on knowledge is restraint.

69
00:07:28,000 --> 00:07:31,000
Restraint by patience and restraint by energy.

70
00:07:31,000 --> 00:07:33,000
Here in restraint by the party mocha is this.

71
00:07:33,000 --> 00:07:39,000
He is furnished, fully furnished with this party mocha restraint.

72
00:07:39,000 --> 00:07:47,000
Restraint by mindfulness is the, he got the eye faculty and thus upon restraint of the eye faculty.

73
00:07:47,000 --> 00:07:57,000
That is, when you see something, you try to just see it and not go to liking it or just liking it.

74
00:07:57,000 --> 00:08:07,000
So, if you can just see it and avoid from, avoid the unwholesome mental states from arising,

75
00:08:07,000 --> 00:08:12,000
then you are said to be restraining the eye, restraining the eye faculty.

76
00:08:12,000 --> 00:08:18,000
In fact, restraining the eye faculty doesn't mean closing your eyes and not look at things.

77
00:08:18,000 --> 00:08:22,000
But you look at things you see, but you do not get akusala from seeing.

78
00:08:22,000 --> 00:08:25,000
That is, restraining the eye.

79
00:08:25,000 --> 00:08:39,000
So, in order to, in order not to be attached to things you see or not to be upset by things you see,

80
00:08:39,000 --> 00:08:42,000
you have to keep mindfulness.

81
00:08:42,000 --> 00:08:47,000
So, that is why mindfulness is called restraint here.

82
00:08:47,000 --> 00:08:56,000
And knowledge is also called restraint.

83
00:08:56,000 --> 00:09:00,000
As termed by means of mindfulness, the currency means akusala.

84
00:09:00,000 --> 00:09:03,000
So, these akusala are termed by mindfulness.

85
00:09:03,000 --> 00:09:06,000
When there is mindfulness, they cannot arise.

86
00:09:06,000 --> 00:09:08,000
They are struck or something like that.

87
00:09:08,000 --> 00:09:12,000
Restraint of currents, eye-proof claim by understanding, they are jammed.

88
00:09:12,000 --> 00:09:17,000
So, by understanding, they are totally subdued and suppressed.

89
00:09:17,000 --> 00:09:22,000
So, here understanding is called restraint.

90
00:09:22,000 --> 00:09:26,000
And the use of requisites is here combined with this.

91
00:09:26,000 --> 00:09:29,000
That means use of requisites is also called restraint here.

92
00:09:29,000 --> 00:09:33,000
Now, use of requisites means this is for mind.

93
00:09:33,000 --> 00:09:42,000
Months have to use requisites with reflection, with understanding.

94
00:09:42,000 --> 00:09:48,000
That means whenever I put on this rope, I have to say to myself, or I have to make a reflection,

95
00:09:48,000 --> 00:09:58,000
I use this rope just to what of cool, to what of heat and to what of pipes by insects, something like that.

96
00:09:58,000 --> 00:10:08,000
And when we eat something also, you say, I eat and not to take pride in my strength, not to make myself beautiful,

97
00:10:08,000 --> 00:10:15,000
but I eat this just to be able to practice odors, teachings and so on.

98
00:10:15,000 --> 00:10:24,000
And also, when we use the we are a dwelling place, we have to reflect that I use this just for

99
00:10:24,000 --> 00:10:28,000
protection from heat and coal and so on.

100
00:10:28,000 --> 00:10:33,000
And when you take medicine also, I take medicine just to get rid of disease.

101
00:10:33,000 --> 00:10:40,000
So, months have to do this whenever they make use of these full requisites.

102
00:10:40,000 --> 00:10:47,000
And if there is no understanding or no knowledge, then you cannot do it.

103
00:10:47,000 --> 00:10:56,000
The use of requisites is also called restraint by knowledge.

104
00:10:56,000 --> 00:10:59,000
What is called restraint by patients?

105
00:10:59,000 --> 00:11:06,000
This is one who bears cold and heat.

106
00:11:06,000 --> 00:11:16,000
So, we bear cold and we are patient with heat, we are patient with thirst, we are patient with hunger and that is called restraint.

107
00:11:16,000 --> 00:11:22,000
By patients, actually, patients itself is restrained here.

108
00:11:22,000 --> 00:11:30,000
And restraint by energy means he does not endure a thought of sense desires when it arises.

109
00:11:30,000 --> 00:11:43,000
That means you have to make effort or you have to have energy to endure such thoughts.

110
00:11:43,000 --> 00:11:49,000
So, when as soon as such thoughts arise in your mind, you just stop it.

111
00:11:49,000 --> 00:11:57,000
And that is called a restraint by energy because without energy, you cannot do that.

112
00:11:57,000 --> 00:12:07,000
So, you have to be alert and you have to make effort to stop as soon as these thoughts arise in your mind.

113
00:12:07,000 --> 00:12:13,000
And purification of livelihood is here, combined with this.

114
00:12:13,000 --> 00:12:16,000
Purification of livelihood is also meant for months.

115
00:12:16,000 --> 00:12:19,000
So, months must have a very pure livelihood.

116
00:12:19,000 --> 00:12:23,000
That means, month must not work to earn money.

117
00:12:23,000 --> 00:12:37,000
But month must not tell fortunes or refunds so as to get something from people.

118
00:12:37,000 --> 00:12:41,000
So, if we do that then our livelihood is said to be in pure.

119
00:12:41,000 --> 00:12:50,000
So, in order to have pure livelihood, what must we do?

120
00:12:50,000 --> 00:12:54,000
We must go out for arms.

121
00:12:54,000 --> 00:12:56,000
So, we must make effort.

122
00:12:56,000 --> 00:12:59,000
We must, we must have energy.

123
00:12:59,000 --> 00:13:04,000
That is the only purity of livelihood for months.

124
00:13:04,000 --> 00:13:18,000
So, months must not work to earn money or month must not ask people to bring food to them.

125
00:13:18,000 --> 00:13:24,000
So, they must go out for arms.

126
00:13:24,000 --> 00:13:27,000
I am in a very different country.

127
00:13:27,000 --> 00:13:33,000
And so, we months cannot do that in this country go out for arms.

128
00:13:33,000 --> 00:13:39,000
So, now we have to ask people to bring food to the monastery.

129
00:13:39,000 --> 00:13:49,000
So, purification of livelihood means you must depend on yourself.

130
00:13:49,000 --> 00:13:52,000
You must rely on your foot muscles.

131
00:13:52,000 --> 00:14:01,000
That means you walk and you do collect food from house to house and then return to the monastery.

132
00:14:01,000 --> 00:14:08,000
So, this by full restraint and the abstinent in Klansman, who does evil from any chance of

133
00:14:08,000 --> 00:14:12,000
a transgression method should all be understood to be virtue as restraint.

134
00:14:12,000 --> 00:14:14,000
And virtue is non-transgression.

135
00:14:14,000 --> 00:14:19,000
That is just non-transgression, not breaking the rules by body or speech.

136
00:14:19,000 --> 00:14:22,000
Of precepts of virtue that have been undertaken.

137
00:14:22,000 --> 00:14:25,000
So, once you take the precepts, you keep them.

138
00:14:25,000 --> 00:14:27,000
You do not break them.

139
00:14:27,000 --> 00:14:31,000
And precepts here are of body and of speech.

140
00:14:31,000 --> 00:14:41,000
Now, we must understand that Shiva has to do with bodily actions and verbal actions,

141
00:14:41,000 --> 00:14:45,000
not thoughts.

142
00:14:45,000 --> 00:14:50,000
You may think of killing a living being, but so long as you do not kill it,

143
00:14:50,000 --> 00:14:52,000
then you are kidding that rule entered.

144
00:14:52,000 --> 00:14:56,000
You are not breaking that rule.

145
00:14:56,000 --> 00:15:02,000
You may think of telling a lie to another person, but so long as you do not tell a lie,

146
00:15:02,000 --> 00:15:05,000
you are not breaking that rule.

147
00:15:05,000 --> 00:15:16,000
So, the sealer is to control the bodily and verbal actions of a person.

148
00:15:16,000 --> 00:15:27,000
We have a precept which is do not harbor ill will.

149
00:15:27,000 --> 00:15:33,000
It is included in samadhi in Taylor whatever it is in you.

150
00:15:33,000 --> 00:15:39,000
Now, samadhi is for control of mind, control of thoughts.

151
00:15:39,000 --> 00:15:50,000
Just thinking something bad does not constitute the breaking of rules,

152
00:15:50,000 --> 00:16:00,000
because these rules control the actions of the body and actions of the mother of the speech.

153
00:16:00,000 --> 00:16:09,000
So, although it is not good to have an wholesome thoughts,

154
00:16:09,000 --> 00:16:14,000
still, if you do not do it with your body or with your speech,

155
00:16:14,000 --> 00:16:17,000
then you are still keeping these rules.

156
00:16:17,000 --> 00:16:21,000
You are not breaking these rules.

157
00:16:21,000 --> 00:16:29,000
So, in Taylor whatever it is in, sealer is for bodily and verbal actions to control them.

158
00:16:29,000 --> 00:16:33,000
And samadhi is for control of mind.

159
00:16:33,000 --> 00:16:42,000
And paneer is for eradication of mental defigements.

160
00:16:42,000 --> 00:16:48,000
Although it is not a precept in Taylor whatever Buddhism,

161
00:16:48,000 --> 00:16:57,000
and non-ill will is the same as do not have thoughts of a question or thoughts of hatred.

162
00:16:57,000 --> 00:17:02,000
That is included in the three, three courses of actions of mind.

163
00:17:02,000 --> 00:17:07,000
Non-coverseness, non-ill will and right view.

164
00:17:07,000 --> 00:17:17,000
They are included in the comma of mind.

165
00:17:17,000 --> 00:17:23,000
We have three kinds of comma, bodily comma, verbal comma and mental comma.

166
00:17:23,000 --> 00:17:34,000
So, we are included in mental comma.

167
00:17:34,000 --> 00:17:41,000
Now, what is the meaning of the word sealer?

168
00:17:41,000 --> 00:17:48,000
It may not be interesting to those who do not, who are not interested in Pali.

169
00:17:48,000 --> 00:18:00,000
But now, the word sealer is explained here as meaning composing or upholding.

170
00:18:00,000 --> 00:18:05,000
So, the word sealer means composing or upholding.

171
00:18:05,000 --> 00:18:08,000
Composing what?

172
00:18:08,000 --> 00:18:15,000
It is either a coordinating, meaning non-inconsistency of bodily action,

173
00:18:15,000 --> 00:18:19,000
et cetera, due to virtueness, virtueness.

174
00:18:19,000 --> 00:18:27,000
Now, non-inconsistency really means non-scattering of one's actions.

175
00:18:27,000 --> 00:18:34,000
That means if we do bad actions, then our actions are said to be scattered, not coordinated.

176
00:18:34,000 --> 00:18:39,000
Yeah, I think scattered is better than inconsistency.

177
00:18:39,000 --> 00:18:52,000
Consistence means it must not be different from others or something like that.

178
00:18:52,000 --> 00:18:56,000
Your consistent means you do this thing always.

179
00:18:56,000 --> 00:19:00,000
It can be a bad habit.

180
00:19:00,000 --> 00:19:02,000
It doesn't vary.

181
00:19:02,000 --> 00:19:04,000
That's right, yeah.

182
00:19:04,000 --> 00:19:10,000
It is not scattered, your actions are not scattered when you have sealer.

183
00:19:10,000 --> 00:19:19,000
And the other meaning is to uphold meaning a state of basis owing to its serving as foundation for profitable states.

184
00:19:19,000 --> 00:19:24,000
Only when you have sealer can you have wholesome mental states.

185
00:19:24,000 --> 00:19:26,000
So, sealer is something like upholding.

186
00:19:26,000 --> 00:19:29,000
So, these are the two meanings of sealer.

187
00:19:29,000 --> 00:19:35,000
For those who understand etymology, that means grammarians, admit only these two meanings.

188
00:19:35,000 --> 00:19:38,000
Others, however, comment on the meaning here in the way beginning.

189
00:19:38,000 --> 00:19:41,000
The meaning of virtueness is the meaning of hat.

190
00:19:41,000 --> 00:19:46,000
The meaning of virtueness is the meaning of cool sealer that is playing upon the word.

191
00:19:46,000 --> 00:19:53,000
You know, the bollywood sealer is close to the vatsirah and also close to the vatsirah.

192
00:19:53,000 --> 00:19:55,000
So, they may explain it in this way.

193
00:19:55,000 --> 00:19:59,000
But it is not accepted by the commented here.

194
00:19:59,000 --> 00:20:05,000
And what are the characteristics, function, manifestation and approximate cause of sealer?

195
00:20:05,000 --> 00:20:11,000
I think you are familiar with these characteristics and so on.

196
00:20:11,000 --> 00:20:20,000
Whenever we have to understand something, we have to understand that by way of characteristic and so on.

197
00:20:20,000 --> 00:20:25,000
Only, only then do you understand this thoroughly.

198
00:20:25,000 --> 00:20:33,000
And especially when you practice meditation, you come to understand things sometimes by characteristic.

199
00:20:33,000 --> 00:20:36,000
That means you see the characteristic of it sometime.

200
00:20:36,000 --> 00:20:37,000
Sometimes you see the function.

201
00:20:37,000 --> 00:20:41,000
Sometimes you see manifestation and sometimes you see approximate cause.

202
00:20:41,000 --> 00:20:45,000
So, with reverence to these four, we have to understand things.

203
00:20:45,000 --> 00:20:49,000
So, sealer is also to be understood in these four aspects.

204
00:20:49,000 --> 00:20:51,000
So, what is its characteristic?

205
00:20:51,000 --> 00:20:57,000
That means what is its individual essence or its nature?

206
00:20:57,000 --> 00:21:08,000
So, the characteristic of it is just composing the same as the one I mentioned above.

207
00:21:08,000 --> 00:21:13,000
Just as visibleness is the characteristic of the visible database.

208
00:21:13,000 --> 00:21:18,000
Even when analyzed into these various categories of blue, yellow, etc.

209
00:21:18,000 --> 00:21:22,000
Because even when analyzed into these categories, it does not exceed the usefulness.

210
00:21:22,000 --> 00:21:28,000
So, also this same composing described above as the coordinating of bodily action, etc.

211
00:21:28,000 --> 00:21:35,000
And as the foundation of profitable states is the characteristic of what you even when analyzed into the various categories of origin, etc.

212
00:21:35,000 --> 00:21:45,000
Because even when analyzed into these categories, it does not exceed the state of coordination and foundation.

213
00:21:45,000 --> 00:21:51,000
Now, visible database means just the visible object.

214
00:21:51,000 --> 00:21:56,000
Among the 28 material properties, there is one thing which can be seen.

215
00:21:56,000 --> 00:21:59,000
And that is the only thing which can be seen by our eyes.

216
00:21:59,000 --> 00:22:03,000
And that is translated as form or visible object.

217
00:22:03,000 --> 00:22:13,000
Now, that visible object, the characteristic of that visible object is just visibleness.

218
00:22:13,000 --> 00:22:16,000
That it can be seen.

219
00:22:16,000 --> 00:22:18,000
So, that is its characteristic.

220
00:22:18,000 --> 00:22:28,000
Although we can say that visible object is blue, yellow, red, white and so on, they may differ in these aspects.

221
00:22:28,000 --> 00:22:31,000
But according to characteristics, they are only one.

222
00:22:31,000 --> 00:22:38,000
They are only one, visible data, which is a characteristic of visibleness.

223
00:22:38,000 --> 00:22:45,000
So, in the same way, we may describe Silla as Chitana is Silla.

224
00:22:45,000 --> 00:22:47,000
Chititi Sika is Silla.

225
00:22:47,000 --> 00:22:48,000
The constraint is Silla.

226
00:22:48,000 --> 00:22:50,000
Non-straint is Silla.

227
00:22:50,000 --> 00:22:58,000
But, however, very different kinds of Silla may be the characteristic.

228
00:22:58,000 --> 00:23:04,000
The common nature of all different kinds of Silla is just composing.

229
00:23:04,000 --> 00:23:12,000
So, it is the characteristic of composing.

230
00:23:12,000 --> 00:23:16,000
And then, what is its function?

231
00:23:16,000 --> 00:23:24,000
Now, here, the word function or the poly word rasa has two meanings.

232
00:23:24,000 --> 00:23:29,000
One is action or function and the other is achievement.

233
00:23:29,000 --> 00:23:36,000
Sometimes, the function as action is used and sometimes, function as achievement is used.

234
00:23:36,000 --> 00:23:40,000
But here, both are mentioned.

235
00:23:40,000 --> 00:23:48,000
So, what is the function of Silla?

236
00:23:48,000 --> 00:23:50,000
Stopping misconduct.

237
00:23:50,000 --> 00:23:51,000
It is its function.

238
00:23:51,000 --> 00:23:55,000
So, when you have Silla, you do not break the precepts.

239
00:23:55,000 --> 00:23:57,000
And so, you do not do misconduct.

240
00:23:57,000 --> 00:24:04,000
So, stopping misconduct is its function as its nature.

241
00:24:04,000 --> 00:24:10,000
And blamelessness is its function as achievement.

242
00:24:10,000 --> 00:24:17,000
That means, when you achieve purity of Silla, then you are blameless.

243
00:24:17,000 --> 00:24:22,000
So, blamelessness is actually the outcome of the purity of Silla.

244
00:24:22,000 --> 00:24:32,000
So, blamelessness is also said to be the function of Silla.

245
00:24:32,000 --> 00:24:42,000
So, the poly word rasa has two meanings, function or action and achievement.

246
00:24:42,000 --> 00:24:53,000
And it is manifestation.

247
00:24:53,000 --> 00:24:54,000
Now, what you so so see those who know itself as purity will show.

248
00:24:54,000 --> 00:24:59,000
And for approximate course, they tell the pair, conscience and shame as well.

249
00:24:59,000 --> 00:25:05,000
Now, the manifestation of Silla is just purity.

250
00:25:05,000 --> 00:25:11,000
When we concentrate on Silla, it appears to us as purity.

251
00:25:11,000 --> 00:25:14,000
So, Silla is real purity.

252
00:25:14,000 --> 00:25:20,000
So, purity is the manifestation of Silla and virtue.

253
00:25:20,000 --> 00:25:25,000
And its approximate cause is conscience and shame.

254
00:25:25,000 --> 00:25:33,000
Its approximate cause is shame, actually shame and fear.

255
00:25:33,000 --> 00:25:40,000
It is shame to do wrong things, shame to do unwholesome things.

256
00:25:40,000 --> 00:25:47,000
And fear to do unwholesome things, because we will if we do unwholesome things, if we do,

257
00:25:47,000 --> 00:25:52,000
if we are more content is not pure, we will get bad results.

258
00:25:52,000 --> 00:25:59,000
So, this shame and fear, these two are called approximate cause of Silla.

259
00:25:59,000 --> 00:26:08,000
So long as people have this conscience and shame of shame and fear, evil will keep precepts.

260
00:26:08,000 --> 00:26:17,000
And the moment these two leave people, then people will do anything they like.

261
00:26:17,000 --> 00:26:24,000
Did you find what proximate causes?

262
00:26:24,000 --> 00:26:29,000
There are two kinds of causes, and near cause and far cause.

263
00:26:29,000 --> 00:26:33,000
So, it is near cause.

264
00:26:33,000 --> 00:26:40,000
That is right, yes.

265
00:26:40,000 --> 00:26:47,000
Okay.

266
00:26:47,000 --> 00:26:54,000
So, I have gone on in nine pages.

267
00:26:54,000 --> 00:27:10,000
So, next week, I don't know how you can get each copy of the book.

268
00:27:10,000 --> 00:27:12,000
Can you make copies of them?

269
00:27:12,000 --> 00:27:15,000
Sir, because there are some of the pages.

270
00:27:15,000 --> 00:27:16,000
Yes.

271
00:27:16,000 --> 00:27:20,000
So, that they can read before they come to the class.

272
00:27:20,000 --> 00:27:25,000
The last year is in the reading room.

273
00:27:25,000 --> 00:27:27,000
There is at least one copy.

274
00:27:27,000 --> 00:27:34,000
Maybe I will try to see that there are two copies on reserve.

275
00:27:34,000 --> 00:27:35,000
You can.

276
00:27:35,000 --> 00:27:37,000
And I will zero-ox some.

277
00:27:37,000 --> 00:27:38,000
Maybe the first chapter.

278
00:27:38,000 --> 00:27:39,000
Yes.

279
00:27:39,000 --> 00:27:46,000
So, please read about 20 or 30 pages for next week.

280
00:27:46,000 --> 00:27:53,000
You can read the class and we can discuss.

281
00:27:53,000 --> 00:28:04,000
So, I didn't mention very much what the role of the city model was in study in the terabyte

282
00:28:04,000 --> 00:28:10,000
of countries, but I have heard that if you want to practice Buddhism, they often say

283
00:28:10,000 --> 00:28:14,000
you need two things, you need a cave and a copy of this book.

284
00:28:14,000 --> 00:28:15,000
That is right.

285
00:28:15,000 --> 00:28:16,000
Yes.

286
00:28:16,000 --> 00:28:25,000
It's held in very high esteem in terabyte countries.

287
00:28:25,000 --> 00:28:33,000
It's next to the Buddha, actually.

288
00:28:33,000 --> 00:28:53,000
And this is the Domus edition of the book in Paul.

289
00:28:53,000 --> 00:28:59,000
Is this the first volume of the book?

290
00:28:59,000 --> 00:29:04,000
No, not many questions.

291
00:29:04,000 --> 00:29:09,000
It's not really meant for the matter.

292
00:29:09,000 --> 00:29:14,000
Does it happen to you please for like, you know, ordinary thoughts?

293
00:29:14,000 --> 00:29:15,000
Yes.

294
00:29:15,000 --> 00:29:16,000
No.

295
00:29:16,000 --> 00:29:17,000
So, what is the relevant?

296
00:29:17,000 --> 00:29:18,000
Okay.

297
00:29:18,000 --> 00:29:23,000
The first two chapters you have to be patient with, because it deals with the practice,

298
00:29:23,000 --> 00:29:27,000
I mean, the rules for months and practices for months.

299
00:29:27,000 --> 00:29:38,000
We can, we can eject some of the statements in this chapter and the next chapter to seal

300
00:29:38,000 --> 00:29:41,000
up our lead people.

301
00:29:41,000 --> 00:29:47,000
So, months are, months are, excited to be very strict in keeping their rules.

302
00:29:47,000 --> 00:29:52,000
They're not to have broken, even a small rule.

303
00:29:52,000 --> 00:30:00,000
And it is said later on we will find that seeing danger in the smallest, smallest transcription

304
00:30:00,000 --> 00:30:01,000
or something.

305
00:30:01,000 --> 00:30:08,000
So, in the same way, if you're going to practice meditation, then you have to, you have

306
00:30:08,000 --> 00:30:09,000
to clear the basis.

307
00:30:09,000 --> 00:30:15,000
You have to establish a firm foundation of moral purity first.

308
00:30:15,000 --> 00:30:25,000
That means, as a lay person, you take precepts, at least five precepts, and keep them.

309
00:30:25,000 --> 00:30:28,000
So, you are not to break anyone of these rules.

310
00:30:28,000 --> 00:30:32,000
You are, you are, you are to keep them in touch.

311
00:30:32,000 --> 00:30:42,000
And you, you, you, you may be exerted not to break any of these, these rules, even though,

312
00:30:42,000 --> 00:30:46,000
you, your life is in danger or something like that.

313
00:30:46,000 --> 00:30:55,000
So, we can adapt the admonition of the adverse given in this chapter to the practice of

314
00:30:55,000 --> 00:30:56,000
lay people too.

315
00:30:56,000 --> 00:31:02,000
But this chapter deals with monk, monk behavior, monk sealer.

316
00:31:02,000 --> 00:31:11,000
And I'm afraid you will find many improper contacts and resort mentioned here.

317
00:31:11,000 --> 00:31:18,000
So, you know, how monks are, how do you go?

318
00:31:18,000 --> 00:31:28,000
You clever in, in, in acquiring things for them, and clever in talking, not, not direct

319
00:31:28,000 --> 00:31:35,000
the lie, but something like that, because that, yeah, something like white lives.

320
00:31:35,000 --> 00:31:49,000
So, and one thing, you got about these, these commentaries is, they give many stories.

321
00:31:49,000 --> 00:31:55,000
So, we, we can learn from these stories.

322
00:31:55,000 --> 00:32:02,000
So, from the third chapter onwards, it's, it's for those who practice meditation, whether

323
00:32:02,000 --> 00:32:08,000
you're a leepers in or a monk, although the emphasis is on the monks, because monks are

324
00:32:08,000 --> 00:32:11,000
those who practice meditation more than lay people.

325
00:32:11,000 --> 00:32:13,000
But it is no longer true.

326
00:32:13,000 --> 00:32:21,000
Lay people are also very interested in meditation now, especially in, in, in Burma.

327
00:32:21,000 --> 00:32:31,000
About the turn of the century, monks became interested in the practice of wipasana meditation,

328
00:32:31,000 --> 00:32:42,000
meditation, and also give chance to leave people to practice wipasana meditation.

329
00:32:42,000 --> 00:32:49,000
Formerly, leave people don't, didn't, did not think that they could practice much meditation,

330
00:32:49,000 --> 00:32:55,000
because they had, they had things to do in the war.

331
00:32:55,000 --> 00:33:01,000
Even, even though you are a monk, if you want to practice meditation, then they think

332
00:33:01,000 --> 00:33:07,000
you have to go to a very secluded place, and be there alone in practice meditation.

333
00:33:07,000 --> 00:33:14,000
But the, the teacher of my teacher, Mahas is here, the teacher of Mahas is here.

334
00:33:14,000 --> 00:33:26,000
Was one of the pioneers of the re-kinning of interest in the practice of meditation,

335
00:33:26,000 --> 00:33:28,000
both by month and lay people.

336
00:33:28,000 --> 00:33:37,000
And I think he, he, he was the first who, who taught meditation to lay people,

337
00:33:37,000 --> 00:33:44,000
and who accepted, lay people to come to the monastery and practice intensive meditation,

338
00:33:44,000 --> 00:33:49,000
like we do now.

339
00:33:49,000 --> 00:33:55,000
His, his personal name was Unarada.

340
00:33:55,000 --> 00:34:00,000
But he was known as Mingun Ziduun Seyara.

341
00:34:00,000 --> 00:34:05,000
His name was mentioned in Janabonika's book.

342
00:34:05,000 --> 00:34:08,000
They had a Buddhist meditation.

343
00:34:08,000 --> 00:34:11,000
Yeah.

344
00:34:11,000 --> 00:34:19,000
So now, many lay people practice, we pass on our meditation in Burma,

345
00:34:19,000 --> 00:34:23,000
and also in this country, now people are very interested in meditation.

346
00:34:23,000 --> 00:34:30,000
So I think this is a good sign of a good friend, Buddhism is going towards,

347
00:34:30,000 --> 00:34:35,000
because after all, practice is what counts.

348
00:34:35,000 --> 00:34:39,000
Just understanding, just knowing theoretically, we will not have as much.

349
00:34:39,000 --> 00:34:47,000
We have to put this theoretical knowledge to practice.

350
00:34:47,000 --> 00:34:54,000
And the study and practice, I think, should go together.

351
00:34:54,000 --> 00:35:00,000
Just study, we will not have as much in understanding.

352
00:35:00,000 --> 00:35:09,000
And something you, you, you see through the practice of meditation

353
00:35:09,000 --> 00:35:17,000
helps you to understand, to have, to have deeper understanding of what you know from books.

354
00:35:17,000 --> 00:35:23,000
So these two should go together, practice and meditation.

355
00:35:23,000 --> 00:35:25,000
Okay, thank you, though.

356
00:35:25,000 --> 00:35:48,000
Thank you.

