1
00:00:00,000 --> 00:00:28,960
Good evening everyone, welcome to our weekly question and answer session.

2
00:00:28,960 --> 00:00:42,400
So we only had 13 questions and we now have 11 questions which means good questions.

3
00:00:42,400 --> 00:00:58,080
Some long questions which I wanted to say without that's probably a bad thing because

4
00:00:58,080 --> 00:01:02,880
it means maybe you're thinking a lot but I actually know you have to explain

5
00:01:02,880 --> 00:01:12,320
sometimes not all questions are one sentence questions so that's okay but

6
00:01:12,320 --> 00:01:21,040
be careful your questions don't get too theoretical. So first question is

7
00:01:21,040 --> 00:01:32,560
from Robin says a layperson becomes aware of a monk acting very strangely posting

8
00:01:32,560 --> 00:01:35,840
extensive statements online implying that members of a particular

9
00:01:35,840 --> 00:01:40,320
religion have poisoned and sick and him and many many others. Are you talking

10
00:01:40,320 --> 00:01:47,520
about me? When I first read this I thought oh I wonder if she's talking about

11
00:01:47,520 --> 00:01:55,320
no it was just just kidding but I knew often criticize other religions I

12
00:01:55,320 --> 00:02:01,480
don't think that's what you're referring to here. I may try not to be too

13
00:02:01,480 --> 00:02:06,560
critical but I for those that I'm familiar with I mean even the Buddha was

14
00:02:06,560 --> 00:02:11,320
critical of other religions so it's just a matter of criticism it's not

15
00:02:11,320 --> 00:02:15,680
necessarily a wrong thing. The Buddha was quite harsh in his criticism of

16
00:02:15,680 --> 00:02:20,000
other religious teachers so the ones who had it all wrong the ones who were

17
00:02:20,000 --> 00:02:25,840
fools they called fools. The teachings that were foolish, he called foolish

18
00:02:26,840 --> 00:02:36,680
and so on. But so your question is what should a layperson do? I mean we should

19
00:02:36,680 --> 00:02:45,320
broaden this out to ask which should a layperson do? Well first let's address your

20
00:02:45,320 --> 00:02:48,680
question as being which should a layperson do when they see a monk do

21
00:02:48,680 --> 00:02:56,480
something that you know generally is problematic let's say could be harmful to

22
00:02:56,480 --> 00:03:01,160
the monk, could be harmful to others. In this case it appears to be harmful on a

23
00:03:01,160 --> 00:03:08,040
broader social scale you know what it's doing to society giving adding fuel to

24
00:03:08,040 --> 00:03:14,400
an already tense ethnic situation and giving people wrong ideas. Which should a

25
00:03:14,400 --> 00:03:20,080
layperson do? I think it's a bit of a red herring I want to say that a better

26
00:03:20,080 --> 00:03:28,600
question is what should a layperson do when a person is engaging in this kind

27
00:03:28,600 --> 00:03:36,640
of activity because I think it's important that we don't exempt monks from the

28
00:03:36,640 --> 00:03:41,800
consequences of bad behavior or the consequences of their behavior. I don't

29
00:03:41,800 --> 00:03:52,440
think it serves any purpose to ignore the bad deeds of a monk. So you might want

30
00:03:52,440 --> 00:03:57,040
to out of respect for the Buddha and the Sangha, the Buddha, the Dhamma and the

31
00:03:57,040 --> 00:04:04,080
Sangha. Treat the monk with respect when you address the problem but that

32
00:04:04,080 --> 00:04:08,120
doesn't mean you should address the problem any differently. Maybe instead of

33
00:04:08,120 --> 00:04:14,320
saying hey you stop that you might say hey venerable sir don't do that. Something

34
00:04:14,320 --> 00:04:21,440
like that would be a little polite because the politeness comes from your

35
00:04:21,440 --> 00:04:28,080
respect for the robes. But the question is whether you really should ever tell

36
00:04:28,080 --> 00:04:33,400
someone don't do that and I don't think that's all that different. I mean you

37
00:04:33,400 --> 00:04:41,320
might there are different roles like you'd be more in your in the right to tell

38
00:04:41,320 --> 00:04:46,600
your child hey don't do that then you would be to tell your parent hey don't do

39
00:04:46,600 --> 00:04:55,800
that. So I think in general we just have to I will as far as I'm going to

40
00:04:55,800 --> 00:05:04,600
interest in answering this question I would you know bear it down or boil it down to the two great

41
00:05:04,600 --> 00:05:09,960
question is we have a few questions but first we'll address the rightness. What

42
00:05:09,960 --> 00:05:14,520
is the right thing to do? So there are two kinds of rightness and that's all I'm

43
00:05:14,520 --> 00:05:18,160
going to say about that part of the question and the one is of course the

44
00:05:18,160 --> 00:05:25,640
rightness right for what? So right for attaining enlightenment. So how do you know

45
00:05:25,640 --> 00:05:32,000
it's right as if it is supportive of your development and your practice leading

46
00:05:32,000 --> 00:05:41,080
to enlightenment. The other kind of right is a duty, a right based on duty. So

47
00:05:41,080 --> 00:05:45,920
it's right to treat your parents with respect. It's right to care for your

48
00:05:45,920 --> 00:05:51,680
children. It's even right you could say to have some social responsibility,

49
00:05:51,680 --> 00:05:56,280
political involvement that sort of thing. These are all right in a

50
00:05:56,280 --> 00:06:01,120
conventional sense and they're not ultimately right. So they can be

51
00:06:01,120 --> 00:06:09,440
intervened by someone practicing to become enlightened. They are, in the

52
00:06:09,440 --> 00:06:17,840
word, they are superseded by the practice of enlightenment. So they don't

53
00:06:17,840 --> 00:06:22,160
actually have any intrinsic rightness but there's a rightness to them

54
00:06:22,160 --> 00:06:29,840
because they create a orderly society and a society that is ideally you know

55
00:06:29,840 --> 00:06:34,040
if done right it's supportive of the meditation practice. I mean

56
00:06:34,040 --> 00:06:39,440
contravening them tends to lead to chaos and disturbance, distractions, stress

57
00:06:39,440 --> 00:06:46,240
and so on. It leads to greater potential for theorizing and defilements. So

58
00:06:46,240 --> 00:06:50,280
your other part does the monastic song have a policy on mental illness. Yes

59
00:06:50,280 --> 00:06:54,600
there are rules about mental illness. Amongst was mentally ill can't be

60
00:06:54,600 --> 00:07:00,240
held responsible for their actions but that means they've gone crazy.

61
00:07:00,240 --> 00:07:09,160
The person is mentally sick in terms of they have bad paranoia's or so on.

62
00:07:09,160 --> 00:07:14,760
That is a really good question. It's a shame that they got our day and I think

63
00:07:14,760 --> 00:07:20,240
that there might be even a encouragement for them to disrobe because they

64
00:07:20,240 --> 00:07:25,480
clearly can't benefit or they can't carry out their activities. I mean you

65
00:07:25,480 --> 00:07:31,040
can't force a monk to disrobe. I mean there are cut in Buddhist countries

66
00:07:31,040 --> 00:07:35,480
they do but it's not according to the Vinaya but I think it would be encouraged

67
00:07:35,480 --> 00:07:41,280
and perhaps that monk would eventually be expelled from the Sangha if

68
00:07:41,280 --> 00:07:46,440
they couldn't maintain. I mean because the idea would be are they breaking rules

69
00:07:46,440 --> 00:07:51,040
or not breaking rules. If they're breaking rules then that's where it

70
00:07:51,040 --> 00:07:56,120
would become a problem. I mean in this case it sounds like this person is perhaps

71
00:07:56,120 --> 00:08:03,440
paranoid, perhaps delusional, perhaps breaking small minor rules but honestly I

72
00:08:03,440 --> 00:08:08,000
wouldn't take it so seriously. There are far worse things that a monk can do

73
00:08:08,000 --> 00:08:15,280
and that from what I hear monks in certain places do do. So simply voicing a

74
00:08:15,280 --> 00:08:20,160
political opinion or opinion about religions or claiming things. I mean if

75
00:08:20,160 --> 00:08:23,440
he's lying that's breaking a rule he's saying things that are not actually

76
00:08:23,440 --> 00:08:32,360
happening and it's not just a deluded belief but that's really something for

77
00:08:32,360 --> 00:08:37,800
the monastic community. I mean the only one who has a responsibility to

78
00:08:37,800 --> 00:08:44,200
address these monks as a duty is the other monk so as a lay person it's not

79
00:08:44,200 --> 00:08:52,560
your duty to do that. So that's where the right comes in. I mean there's

80
00:08:52,560 --> 00:08:57,080
really no no occasion that I can think of where someone who has very little

81
00:08:57,080 --> 00:09:06,880
relation to you. Where they're the instruction or the addressing of their

82
00:09:06,880 --> 00:09:13,280
problems is going to support you towards your enlightenment and so it's not

83
00:09:13,280 --> 00:09:18,200
right in that sense. But as a duty it's also not right because as a lay person

84
00:09:18,200 --> 00:09:22,040
you really have no connection with them in terms of this is a person I should

85
00:09:22,040 --> 00:09:27,760
take care of except you know to support them to give them food and so on and you

86
00:09:27,760 --> 00:09:31,080
do that for the sun guys a whole and you do that for the monks who are

87
00:09:31,080 --> 00:09:38,320
behaving properly. So with holding food that's the big one that lay people

88
00:09:38,320 --> 00:09:42,520
engaged and if a monk is acting inappropriate it's called overturning the

89
00:09:42,520 --> 00:09:48,120
ball. Isn't it? No that's the other way around. Where a monk overturns the

90
00:09:48,120 --> 00:09:53,680
ball in the sense that they don't accept food from lay people. For lay people

91
00:09:53,680 --> 00:09:58,600
they just withhold food and that's a sign that they don't support this monk

92
00:09:58,600 --> 00:10:03,920
or their behavior. And so the other part of the question can a monk be forcibly

93
00:10:03,920 --> 00:10:07,340
disrobed, forced to disrobed. The only thing that can force a monk to

94
00:10:07,340 --> 00:10:12,920
disrobed is breaking the four great rules and being crazy unfortunately isn't

95
00:10:12,920 --> 00:10:18,640
one of them. It's interesting I can't remember exactly what his

96
00:10:18,640 --> 00:10:22,520
said or can't think of anything particularly that said about what to do if a

97
00:10:22,520 --> 00:10:27,680
monk is clinically insane. It might be something to look at but that's a

98
00:10:27,680 --> 00:10:31,280
monastic thing again not something that lay people should be concerned with.

99
00:10:31,280 --> 00:10:36,040
It's just like you shouldn't be concerned with the pope or his dealings

100
00:10:36,040 --> 00:10:40,360
or you shouldn't be concerned with the Dalai Lama or you shouldn't be concerned

101
00:10:40,360 --> 00:10:47,760
with any things that don't concern you. There's no duty there and so it's

102
00:10:47,760 --> 00:10:56,080
not even a functional right to do it. I think I mean I know there is a

103
00:10:56,080 --> 00:10:59,240
connection because you see you call yourself Buddhist and you call this monk

104
00:10:59,240 --> 00:11:03,880
Buddhist and so you think why I have a relationship but really I wouldn't make

105
00:11:03,880 --> 00:11:07,560
too much of that. I would think well this person is in another group of people

106
00:11:07,560 --> 00:11:11,840
or the monks and I know that what I do for monks has given food but I'm not

107
00:11:11,840 --> 00:11:16,000
gonna give him food to that monk because he's not behaving properly. That's

108
00:11:16,000 --> 00:11:24,280
about it. There's not much reason to do much else that I can think of.

109
00:11:24,280 --> 00:11:29,040
In fact sometimes doing things is what cause you know it aggravates the

110
00:11:29,040 --> 00:11:35,280
situation because then you get conflict and you get more stress and

111
00:11:35,280 --> 00:11:42,120
suffering. If I meet close relatives they tell me many times that I talk so

112
00:11:42,120 --> 00:11:46,200
little. I don't feel the need to join their chatter about other people and

113
00:11:46,200 --> 00:11:49,680
stuff that doesn't really concern me. I don't want to tell them because it

114
00:11:49,680 --> 00:11:52,880
would hurt them. Is it enough to just be mindful and still and don't speak or

115
00:11:52,880 --> 00:11:58,400
should I participate and just being careful about right speech? Well the thing

116
00:11:58,400 --> 00:12:03,000
about meditation is the people you surround yourself will change and so you

117
00:12:03,000 --> 00:12:07,280
might not want to surround yourself with close relatives because what does it

118
00:12:07,280 --> 00:12:14,840
mean? What does the word mean? It's just a concept really. It's a description

119
00:12:14,840 --> 00:12:22,880
that's a label for a particular set of circumstances. None of what's really

120
00:12:22,880 --> 00:12:28,440
have much to do with enlightenment or happiness or freedom from suffering. So you

121
00:12:28,440 --> 00:12:35,600
know there is power in family having a close family is a great blessing that

122
00:12:35,600 --> 00:12:39,560
would have said. There's a conventional thing. There's no question that having

123
00:12:39,560 --> 00:12:44,000
relatives who will support you unconditionally is a good thing. In a worldly

124
00:12:44,000 --> 00:12:53,840
sense but there's no closeness beyond that. Real closeness only comes from

125
00:12:53,840 --> 00:13:02,560
closeness of attitude, behavior, personality type, that sort of thing, ambitions,

126
00:13:02,560 --> 00:13:11,800
goals. But you get a lot. What's the word you attract more flies with honey than

127
00:13:11,800 --> 00:13:18,600
vinegar? It's not quite the proper saying here but there's no reason to be

128
00:13:18,600 --> 00:13:28,120
morose or to be gloomy or to be that's the word. I guess it's more of an

129
00:13:28,120 --> 00:13:37,320
external thing. There's no need to project this sort of zombie state even

130
00:13:37,320 --> 00:13:43,580
though that's a description for a true meditative state. You can be approachable

131
00:13:43,580 --> 00:13:51,440
and peaceful. A good thing to remember is to smile once in a while. Even if you

132
00:13:51,440 --> 00:13:55,920
don't feel happy, smiling is a communication. It's communicating to the

133
00:13:55,920 --> 00:14:01,840
person what you feel inside. I feel okay and when you smile it shows them. So

134
00:14:01,840 --> 00:14:10,800
remember smiling is a form of communication like that. But yeah there's no

135
00:14:10,800 --> 00:14:15,360
reason. You say there's no need to join in the chatter but there's also no need

136
00:14:15,360 --> 00:14:22,200
to not join in some way. Yes you may not want to talk at all but smiling and

137
00:14:22,200 --> 00:14:28,960
nodding and offering opinions if that's what's called for. People are having

138
00:14:28,960 --> 00:14:31,640
a conversation and they say something that you disagree with you don't

139
00:14:31,640 --> 00:14:36,440
have to jump in. But if it gets to the point where there's an expectation

140
00:14:36,440 --> 00:14:41,360
that you're a part of the conversation then you know speak when it's appropriate.

141
00:14:41,360 --> 00:14:46,200
People want your opinion and give it. If you don't have an opinion say you don't

142
00:14:46,200 --> 00:14:51,560
have an opinion smile and nod your head.

143
00:14:53,560 --> 00:14:58,040
Again there's no hard and fast rule. The hard and fast rule is to be mindful.

144
00:14:58,040 --> 00:15:02,720
Beyond that everything else is going to be complicated and it's going to depend

145
00:15:02,720 --> 00:15:07,280
very much on many different conditions and situations.

146
00:15:07,280 --> 00:15:11,480
Well say reading a book how should we be mindful? What should we be mindful?

147
00:15:11,480 --> 00:15:15,440
And how should we incorporate the force at the baton as well while reading?

148
00:15:15,440 --> 00:15:22,640
Let this question and I think quite simply if your intention is to read

149
00:15:22,640 --> 00:15:28,600
your intention is not to be mindful. It's unavoidable that we have to do things

150
00:15:28,600 --> 00:15:33,320
like read but because it's a mental activity you can't be mindful when you're

151
00:15:33,320 --> 00:15:38,920
actively reading. Now in between the reading you can pop in and out of mindfulness

152
00:15:38,920 --> 00:15:43,640
and it's good to take breaks from the reading for that reason. Read for a bit

153
00:15:43,640 --> 00:15:48,840
and then do some meditation for a bit. Bring back your mindfulness but it's a

154
00:15:48,840 --> 00:15:54,240
different mental activity. So you can't be mindful all the time unless you give

155
00:15:54,240 --> 00:15:58,400
up all that. If you give up reading during the time when you give up other

156
00:15:58,400 --> 00:16:05,920
kinds of mental activity, it's wide during meditation. We say things like

157
00:16:05,920 --> 00:16:13,320
reading are to be limited and hopefully abandoned during the course.

158
00:16:13,320 --> 00:16:20,880
Okay I feel desired express frustration, disappointment or anger because I feel

159
00:16:20,880 --> 00:16:25,800
it conveys the actual magnitude and importance of the situation. So the idea

160
00:16:25,800 --> 00:16:34,160
is that the idea presented by this questioner is that there's something that

161
00:16:34,160 --> 00:16:39,160
needs to be said or the anger conveys something right. Like I'm right you're

162
00:16:39,160 --> 00:16:46,040
wrong and only the anger will disappointment frustration will convey that.

163
00:16:46,040 --> 00:16:51,040
We'll make it clear to you. It's like when someone's just behaving like an

164
00:16:51,040 --> 00:16:54,440
idiot you slap them right because the slap is the only thing that's going to

165
00:16:54,440 --> 00:16:59,120
get them to wake up. The idea like a jerk like if someone's really being me

166
00:16:59,120 --> 00:17:06,040
and this is I'm not proposing this. I'm just saying this is what people do. The

167
00:17:06,040 --> 00:17:11,080
anger carries a certain amount of wisdom, right. I disagree with this the whole

168
00:17:11,080 --> 00:17:16,800
premise here and I mean I think you're not saying that you're a bad person's

169
00:17:16,800 --> 00:17:22,080
stupid for asking. It's clear that you're asking my advice. I think you don't

170
00:17:22,080 --> 00:17:26,720
reconcile it. I don't think there's really wisdom associated with the anger. I

171
00:17:26,720 --> 00:17:35,920
don't. I think it's a sign of wrong-headedness to try and convince other people

172
00:17:35,920 --> 00:17:39,640
that you're right and they're wrong. Even if you are right and they're wrong

173
00:17:39,640 --> 00:17:44,440
what's the point? So your last part you say if I genuinely feel that there's a

174
00:17:44,440 --> 00:17:47,920
wrong in the world. Oh there are lots of wrongs in the world and you're not

175
00:17:47,920 --> 00:17:55,680
going to fix that and you getting angry about it isn't going to fix it.

176
00:17:55,680 --> 00:18:01,280
It's going to fix the world. Just going to add more anger. It's going to, it

177
00:18:01,280 --> 00:18:07,920
debases the conversation. If the good people get angry, they're debased. It's

178
00:18:07,920 --> 00:18:14,880
what is called stupid to their level. Not to their entire level but in some

179
00:18:14,880 --> 00:18:19,720
way it is. In the way that you're giving rise to unholesome mind states,

180
00:18:19,720 --> 00:18:29,440
mind states that are desirous of the harm to others. You want to stress them

181
00:18:29,440 --> 00:18:37,960
out. I mean what you have to get rid of is your need to change the world. You

182
00:18:37,960 --> 00:18:42,520
need to fix the world. You need to convince other people. Otherwise you'll

183
00:18:42,520 --> 00:18:49,080
never be free from things like anger and greed as well and delusion. How can

184
00:18:49,080 --> 00:18:54,240
we express ourselves to convey that wrong to others? I don't see a need to convey

185
00:18:54,240 --> 00:18:58,680
the wrong to others. I ask your opinion, give them your opinion. It's much more

186
00:18:58,680 --> 00:19:03,120
powerful really because it shows that you are in a hold of your realm. These

187
00:19:03,120 --> 00:19:08,520
people who are fussing and obsessing and fighting over things. To be outside of

188
00:19:08,520 --> 00:19:16,800
that and to be able to not be moved by that is a very powerful thing that can

189
00:19:16,800 --> 00:19:30,960
move people. The right to people anyway. Here's a question that I think. It's been on

190
00:19:30,960 --> 00:19:37,960
my mind for a while. This is just, oh, it's gone. I pushed the wrong button. Did I

191
00:19:37,960 --> 00:19:44,320
delete it? No, it's an answer, right? So this person is Thai and they recently

192
00:19:44,320 --> 00:19:52,040
developed a VR virtual reality application, but even more averse immersive. So

193
00:19:52,040 --> 00:19:56,800
I don't live in Thailand. I can give you my email. My email is just

194
00:19:56,800 --> 00:20:00,400
youtadamo at gmail.com, but don't tell anybody because then people will try and

195
00:20:00,400 --> 00:20:08,480
email me. You can email me. I don't answer my emails. I think I've put it

196
00:20:08,480 --> 00:20:13,160
out on the internet before. I get too many emails and I don't answer most of

197
00:20:13,160 --> 00:20:18,360
them. My defense is I'm a monk and no monks probably shouldn't have email

198
00:20:18,360 --> 00:20:23,920
addresses in the first place, but I do and I just don't answer them in all of

199
00:20:23,920 --> 00:20:28,760
them. Most of them. So don't expect an answer if anyone needs to come to

200
00:20:28,760 --> 00:20:37,280
email me. But this one, see, when I was in Thailand maybe last time or the time

201
00:20:37,280 --> 00:20:42,520
before my uncle lives in Thailand and it's one of the reasons I originally went

202
00:20:42,520 --> 00:20:49,960
his company had purchased a virtual reality or one of the people working there

203
00:20:49,960 --> 00:20:55,560
had purchased a virtual reality system. Like the real deal with the headset

204
00:20:55,560 --> 00:21:04,480
and everything. And so I they showed it to me and I tried it out and I ended up on

205
00:21:04,480 --> 00:21:10,960
a mountain top cliff and I was sitting across, I actually sat down on the

206
00:21:10,960 --> 00:21:18,120
carpet and the air condition was blowing on me so there was a breeze and I was

207
00:21:18,120 --> 00:21:22,240
looking out at the cliff and there was a little dog and I could throw a stick

208
00:21:22,240 --> 00:21:31,840
for it. But it was a little bit impressive and it made me think and I talked

209
00:21:31,840 --> 00:21:38,640
about this, the idea of having a virtual reality meditation session. I've

210
00:21:38,640 --> 00:21:45,440
talked about this I think before on the internet but I'm interested in what

211
00:21:45,440 --> 00:21:49,800
you have and maybe someday we'll have a virtual reality set here and I can do

212
00:21:49,800 --> 00:21:55,120
virtual reality conversations with my students or I can record virtual

213
00:21:55,120 --> 00:22:02,240
I've looked at you can record virtual reality videos and then people who watch

214
00:22:02,240 --> 00:22:08,200
them can have it be immersive. So there's a little benefit to that idea. I mean

215
00:22:08,200 --> 00:22:14,600
of course the technological requirements are very steep still I think I think I

216
00:22:14,600 --> 00:22:23,320
don't know and it's fake right but there's nothing less real about seeing

217
00:22:23,320 --> 00:22:28,520
fake trees than there's about seeing real trees from Buddhist perspective so

218
00:22:28,520 --> 00:22:34,760
the environment of it I think there's potential for being in a virtual reality

219
00:22:34,760 --> 00:22:38,560
then you can just escape your room and all the clutter in it and all the

220
00:22:38,560 --> 00:22:45,080
things that distract you and listen to someone sit there and teach you and

221
00:22:45,080 --> 00:22:49,280
watch them do walking meditation I mean you can do virtual reality walking

222
00:22:49,280 --> 00:22:53,960
meditation demonstrations and so on.

223
00:22:53,960 --> 00:23:07,120
I'm interested mildly interested. Okay questions about the book that this person

224
00:23:07,120 --> 00:23:15,640
has two questions. See clearly how we are causing should we

225
00:23:15,640 --> 00:23:21,840
consciously be aware of the causing so we cause problems by our reaction so when

226
00:23:21,840 --> 00:23:27,040
you say to yourself angry angry what I mean by causing seeing how you're

227
00:23:27,040 --> 00:23:32,640
causing is seeing the problem with the anger seeing that anger is not useful

228
00:23:32,640 --> 00:23:39,040
when you say to yourself angry angry it's almost indirect because all it

229
00:23:39,040 --> 00:23:45,040
does is create objectivity but the objectivity that you create from being

230
00:23:45,040 --> 00:23:49,720
mindful is going to allow you to see the repeated anger for what it is all

231
00:23:49,720 --> 00:23:54,120
this comes up as a result of this comes up as a result oh it comes up as a

232
00:23:54,120 --> 00:24:01,240
result again and watching that you're going to see how wrong it is to get

233
00:24:01,240 --> 00:24:04,160
angry just because it's stressful because it's useless because it's

234
00:24:04,160 --> 00:24:08,840
unhelpful so you see it again and again and again and that's how you're going

235
00:24:08,840 --> 00:24:13,840
to understand. I wouldn't and you've got another question here but

236
00:24:13,840 --> 00:24:17,880
understanding so let's just lump them together if I can so but

237
00:24:17,880 --> 00:24:22,440
understanding that they also cause they are also the cause for suffering

238
00:24:22,440 --> 00:24:29,440
and stress I wouldn't put too much emphasis I mean the word

239
00:24:29,440 --> 00:24:33,720
understand means probably less than you think it does you're looking for some

240
00:24:33,720 --> 00:24:37,400
kind of epiphany or that kind of thing there are some epiphanies but it's more

241
00:24:37,400 --> 00:24:41,840
just seeing things as they are I mean anger is stressful they're you are

242
00:24:41,840 --> 00:24:46,680
causing stress to yourself unless you're enlightened there is stress in

243
00:24:46,680 --> 00:24:51,800
your in your makeup you know you react to things improperly you you leap

244
00:24:51,800 --> 00:24:57,240
out after things you want you push away run away from and to try and destroy

245
00:24:57,240 --> 00:25:03,040
the things you don't want you get arrogant you get conceded and all this so

246
00:25:03,040 --> 00:25:07,440
observing and learning to be objective is going to show you all of that it's

247
00:25:07,440 --> 00:25:14,520
also going to show you the nature of the things that you react that they're

248
00:25:14,520 --> 00:25:18,840
not worth reacting to so they're seeing both but it's just from watching

249
00:25:18,840 --> 00:25:24,840
and you that this noting technique is just a means of allowing us to watch

250
00:25:24,840 --> 00:25:31,880
keeping us objective and making us objective creating this

251
00:25:31,880 --> 00:25:51,880
rectitude of mind the straightness of mind there was a time when I give talks

252
00:25:51,880 --> 00:25:58,520
every night am I going to do that again not not now I think talking too much

253
00:25:58,520 --> 00:26:06,320
and teaching too much is hello is problematic because it focuses too much

254
00:26:06,320 --> 00:26:15,800
attention on the listening on the theory on the thinking and and also it's

255
00:26:15,800 --> 00:26:26,000
just it's not my idea of where I want to go and to teach too much and you

256
00:26:26,000 --> 00:26:29,960
could become a professor in the university you could become a teacher who

257
00:26:29,960 --> 00:26:38,760
teaches every day not really how I see myself okay question about the

258
00:26:38,760 --> 00:26:41,760
domo part of this is nice when I give a video and then people come and ask

259
00:26:41,760 --> 00:26:48,720
questions about it for for clarification so the question is about this word

260
00:26:48,720 --> 00:26:58,120
I think it was we tina what's the word parallok kasa we tina parallok kasa

261
00:26:58,120 --> 00:27:03,560
something we tina or whatever the word was just means thrown away discarded so

262
00:27:03,560 --> 00:27:08,240
it's not asking whether it's not addressing the question of whether this

263
00:27:08,240 --> 00:27:13,160
person believes in an afterlife or not what it means is when you lie it's one

264
00:27:13,160 --> 00:27:19,120
of those things that that is the act of throwing away your future whether you

265
00:27:19,120 --> 00:27:23,960
believe in it or not the result is the same so whether this person believes or

266
00:27:23,960 --> 00:27:33,200
doesn't deserve relevant means they've thrown it away so it and it may be

267
00:27:33,200 --> 00:27:40,680
ambiguous that that that may even mean that they have no belief in it right

268
00:27:40,680 --> 00:27:45,240
that they've they've discarded it so the reason they're okay with lying is

269
00:27:45,240 --> 00:27:50,520
because I don't I don't think so but I get what you're saying like they're

270
00:27:50,520 --> 00:27:53,320
lying because they don't believe in an afterlife they've thrown away that

271
00:27:53,320 --> 00:27:58,440
idea and that's what lead but it's not exactly right for the context because

272
00:27:58,440 --> 00:28:04,720
it's just any kind of lie these these these this woman wasn't lying to the

273
00:28:04,720 --> 00:28:10,640
Buddha because she didn't believe in an afterlife necessarily but I think it's

274
00:28:10,640 --> 00:28:15,680
ambiguous because that's a part of it she can't have any strong knowledge of an

275
00:28:15,680 --> 00:28:21,160
afterlife or else you wouldn't do something so horrible although you know

276
00:28:21,160 --> 00:28:26,600
there are that's not even true because there are sects of Christianity I think

277
00:28:26,600 --> 00:28:32,800
and Islam that believe in afterlife but do horrible things in support of it

278
00:28:32,800 --> 00:28:38,160
or have done horrible things in support of the afterlife right so I don't

279
00:28:38,160 --> 00:28:43,040
think that's what it's referring to I think it simply means they've thrown away

280
00:28:43,040 --> 00:28:49,360
their future according to Buddhism are we allowed to lie to save an innocent

281
00:28:49,360 --> 00:28:54,280
person's life you're allowed to do anything you want Buddhism is not about

282
00:28:54,280 --> 00:29:04,280
disallowing things but there's consequences the Buddha recommend or the

283
00:29:04,280 --> 00:29:15,480
Buddha laid down a precept not to lie but it's voluntary that's voluntary

284
00:29:15,480 --> 00:29:25,000
anyone who wants to practice the Buddha's teaching should not lie if they

285
00:29:25,000 --> 00:29:28,640
want to say they're following the Buddha's teaching if they're lying they're

286
00:29:28,640 --> 00:29:35,640
not following the Buddha's teaching

287
00:29:38,160 --> 00:29:41,040
can meditation I'm only gonna answer the first part of this question because

288
00:29:41,040 --> 00:29:44,360
I don't have anything about are you meditating correctly it's

289
00:29:44,360 --> 00:29:48,680
no read my booklet you want my advice read my booklet that's all I have to

290
00:29:48,680 --> 00:29:57,680
say only teach one thing don't look to me for advice on anything else but

291
00:29:57,680 --> 00:30:02,000
can meditation alleviate anxiety mood disorders under depression yes absolutely

292
00:30:02,000 --> 00:30:07,160
try and read my booklet practicing that way I can guarantee that if you do it

293
00:30:07,160 --> 00:30:13,400
properly it should help with your depression and anxiety and so on how can one

294
00:30:13,400 --> 00:30:19,400
move we've got new questions sneaky how can one move beyond

295
00:30:19,400 --> 00:30:27,680
syllabata parama sir become a sotapana okay these are new questions that's

296
00:30:27,680 --> 00:30:32,760
not fair I haven't gone over them they haven't been vetted yet so new

297
00:30:32,760 --> 00:30:41,640
questions can answer next week yes that's fair I think because they also have

298
00:30:41,640 --> 00:30:45,600
to be voted on once we get lots of questions the good ones rise to the top

299
00:30:45,600 --> 00:30:52,640
I'm less likely I do sometimes but I'm less likely to delete questions that

300
00:30:52,640 --> 00:30:58,320
have lots of uploads but I will I still will

301
00:31:04,720 --> 00:31:11,680
Buddhas and avatars just you Buddha's not an avatars just you know but as a

302
00:31:11,680 --> 00:31:23,040
word for someone who is enlightened video output low okay that's all for tonight

303
00:31:23,040 --> 00:31:44,240
thank you all have a good night

