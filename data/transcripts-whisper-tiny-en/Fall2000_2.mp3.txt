Today, we continue our talk on the four foundations of mindfulness.
Now, the benefit of the practice of mindfulness is shown by the words removing covetousness
and grief in the world.
So this is the benefit of the practice.
So when a yogi practices mindfulness and he is able to remove covetousness and grief, he
is able to prevent covetousness and grief from arising in his mind.
With regard to removing, the commentaries mentioned three kinds of removing.
On the first kind, we will call momentary removing.
In the second, temporary removing and the third total removing.
Momentary removing means removing just at the moment when the mindfulness is present.
Now this removing is called momentary removing because it can remove only at the moment
when the mindfulness is arising.
Now, suppose there is darkness and then you turn on the switch and there is light.
So the moment there is light, darkness is dispelled.
But the moment you turn off the switch again, the darkness comes back.
So in the same way, when the removing is just for a moment, like when you are mindful,
the mental hindrance cannot arise, but when you are not mindful, there is no mindfulness
in your mind, the mental defalments come back.
So such removing by actually substituting the mental defalments with some wholesome mental
states is called momentary removing.
The second kind of removing, temporary removing is preventing the mental defalments from
arising for some time or for a longer period of time, longer than a moment.
Now when, let us see, there is a pond and in the water there are water plants.
Now, it buffalo would wait through the water and through the water plants.
So when the buffalo goes through the water, the plants, the water plants are divided.
And they will not come back together again for some time.
Usually after some time, they will come back slowly and meet together.
So in the same way, the temporary abandonment is abandonment for some time so that the
mental defalments will not come back again for some time.
The third kind of removing is total removing means, removing the mental defalments once
and for all so that they do not arise in the mind of a yogi again or anymore.
The first kind of removing a yogi achieves when he practices mindfulness or wipasana meditation.
Now when he practices mindfulness or wipasana meditation, he makes effort and he tries to
be mindful of the object and as the mindfulness grows in strength, he begins to see the
objects clearly and he begins to see the objects one after another coming and going and
also he begins to see objects arising and disappearing and so he knows that the objects
are impermanent and suffering in the sense that they are afflicted by arising and disappearing
and he also knows that there is no control over these objects arising and disappearing
and also there is no substance in it.
So when he sees in this way, he is preventing the mental defalments from arising at that
moment or when he is seeing in this way, mental defalments do not get chance to arise
in his mind.
So while he is practicing mindfulness and while he experiences the clear comprehension,
he is said to be removing the mental defalments momentarily.
So at every moment of mindfulness practice, a yogi is achieving this first kind of removal
of mental defalments moment by moment.
But as soon as a yogi stops the practice of mindfulness, these mental defalments will get
chance to arise in his mind again.
The second kind of removal, a yogi achieves when he gets Jana.
Now Jana is the highest state of consciousness and with Jana, these mental defalments can
be pushed away for some period of time.
The mental defalments removed by a Jana experience may not come back to the yogi for some
time.
The third kind of removal is achieved when a yogi reaches enlightenment.
Now, at the moment of enlightenment, as I said before, a type of consciousness called
heart consciousness arises and that thought consciousness takes Nibana as object and it removes
or eradicates mental defalments.
Now, there are four stages of enlightenment and at the first stage, some mental defalments
are eradicated and at the second stage, the remaining mental defalments are made weaker
and at the third stage, some more mental defalments are eradicated or removed and at the
fourth stage, all the remaining mental defalments are eradicated.
So once it are eradicated, these mental defalments will not arise again in that person.
So that removal is called the total removal.
So when a yogi practices Vipasana meditation he achieves momentary removal of mental defalments
and when a yogi gains Jana he achieves the temporary removing and when he gains enlightenment
he achieves the total removing.
Now, Mahasi Shiro explained here that during the practice of Vipasana meditation, the
temporary removal can be experienced.
So he explained in this way, first a yogi practices mindfulness on objects and then
he, his mindfulness become stronger and he gains good samadhi and he begins to see the
true nature of things that is he begins to see the impermanent nature, suffering nature
and non-soul nature of things.
So when he sees the objects clearly in this way, he is said to be achieving momentary
removal of mental defilements.
Now when he has gained experience in achieving momentary removal, his mind will become so
refined, his samadhi, his concentration, so strong that even if he, even if he is not
mindful of an object, the mental defilements do not arise on these objects.
Normally if a yogi does not practice mindfulness on an object, then the mental defilements
will arise regarding that object.
But now this yogi has gained so much experience in momentary removal that even if he does
not practice mindfulness on an object, no mental defilements will arise in him.
Even if he stopped meditating, his mind is so pure that no mental defilements will arise
in his mind.
So a yogi may think that I have eradicated mental defilements or no gross mental defilements
can arise in my mind now.
So that the ability to prevent mental defilements from arising even when they are not
made the object of mindfulness is what we call temporary abandonment or temporary removal
in the practice of vipassana.
Again please note that removing here means not giving the mental defilements chance to
arise.
And that is achieved when a yogi is mindful and when he clearly comprehends the objects.
So being mindful and clearly comprehending a yogi is said to be removing the mental defilements.
So that means he does not have to make special effort to remove a mental defilement.
But when he is mindful of the object, when he comprehends the object clearly that means
when he sees the true nature of things, then he is at the same time said to be removing
the mental defilements.
And the words in the world should be understood according to where they are mentioned.
So in the number one sentence, in the world means in the body.
And in number two sentence, in the world means in the feelings, in number three sentence,
in the world means in the consciousness.
And in number four, sentence in the world means in the Dharma objects.
So in number one sentence, Buddha said, if he could dwells contemplating the body in
the body.
So the word body is repeated and that is for the observation to be precise.
So the commentary explains that contemplating or observing the body in the body means, observing
the body in the body and not observing and the feeling in the body, consciousness in the
body and Dharma objects in the body.
So it is to show the precision of observation that the word body is mentioned twice.
In this sentence, also the word body is to be understood according to the context.
Now in the discourse on the four foundations of mindfulness, the body contemplation is described
in 14 different ways.
The first section is the mindfulness of breathing.
Now the second is the mindfulness of the postures of the body.
And the third is called the section on clear comprehension that is mindfulness of smaller
activities like going forward, going back, stretching, bending and so on.
And then there is a section on repulsiveness of the body or 32 parts of the body.
And then there is a section on the four material elements.
And next there is section on actually nine sections on symmetry contemplations.
So all together there are 14 kinds of contemplation of the body.
And so regarding the first section which is a section on breathing, the body means breathing.
In the second section where the postures of the body are mentioned, then the postures of
the body are called body and so on.
So the body may mean sometimes the whole body, sometimes just the breathing in and breathing
out or stepping forward, going back and so on.
So when you are practicing mindfulness of breathing in and out and you are doing the
contemplation on the body and when you are mindful of the postures, sitting, standing,
walking and lying down, you are practicing contemplation on the body.
And when you are mindful of going forward, going back, stretching, bending, eating and
so on, you are practicing contemplation in the body.
So there are all together 14 kinds of contemplation of the body taught in this discourse.
Now the last nine contemplations of the body are called symmetry contemplations.
Actually that is the contemplation on the different conditions of the dead body.
So first it becomes swollen or festered and then there are other conditions of the dead
body until it is reduced to bones and powder.
So the symmetry contemplations mean contemplation on the conditions of the dead body, say
laughter after the person is dead.
Now when you are mindful of the rising and falling movements of the abdomen, you are doing
the contemplation on the body that is mindfulness of the full mindfulness of the air element.
Now you all know that there are four material elements, the element of earth, the element
of water, the element of fire and the element of air.
Among these four, the rising and falling movements belong to the air element.
Because they are actually caused by the breathing in and out and so they are called air
element and air element has the characteristic of extending and it has a function of moving.
So when you concentrate not on the abdomen actually but on the movements then you are concentrating
on the air element, on the function of air element.
So when you practice mindfulness of the rising and falling of the movement, you are actually
practicing the mindfulness of the full element here, mindfulness of the air element.
Now the second contemplation, contemplation of feelings is a mental state.
Whenever we experience an object, whenever we see an object or we hear an object, we smell
an object and so on.
There is the mental state feeling arising along with the consciousness.
So the mental state feeling has the characteristic of experiencing or enjoying the taste
of the object.
So please note that feeling here is a mental state.
But whenever we talk about feeling, our mind is always good to the physical thing.
Say when you say pain, then we always go to the material thing that is called pain.
And when we are mindful of pain, actually we are mindful of the feeling of that pain.
Pain is not mental state, pain is a physical state.
But this contemplation on the feeling is called mental contemplation.
So contemplation of feeling is the contemplation of a mental state which is called feeling.
And that arises in our mind, not in the physical body.
But it is so associated with, so connected with the sensations in the body that in practice,
we just take these two as one.
So when we say, when you have pain, concentrate on the pain and be mindful of it, and
that means concentrate on the experience in your mind of that sensation and not that sensation
actually.
So the sensation I mean something that is in the body.
Now when the material properties in the body are in good state doing fine, then we don't
feel any pain.
But when we feel pain, that means the material properties in the body have become deteriorated
or they have changed.
So that is what we call pain or sensation.
And the experience of that pain in our mind, experience in our mind of that pain is what
we call feeling.
So contemplation on feeling is a mental contemplation or contemplation of a mental object
and not of the physical object.
Basically there are three kinds of feelings, pleasant and pleasant and neutral.
And each of these three can be worldly and unworthy.
So worldly feeling means feelings, you experience when you are doing worldly things, when
you are enjoying good things and so on.
And worldly feeling means the feelings that you experience when you are doing meritorious
tea, when you are practicing meditation and so on.
So with the three basic feelings, we say that there are nine kinds of feeling and there
are nine kinds of feeling taught in the discourse on the foundations of mindfulness.
And whatever feeling there is, we are to be mindful of that feeling.
That is why when there is a feeling of pain, we try to be mindful, saying pain, pain,
pain, when there is stiffness and we try to be mindful of that stiffness, saying stiffness,
stiffness, stiffness and so on.
Actually whatever feeling there is, whether it is a pleasant feeling or unpleasant feeling
or a neutral feeling, we are to be mindful of that feeling.
Because if we do not practice mindfulness on feeling, we may have wrong notions of the feeling,
that feeling lies for some time, that feeling is good and so on.
So in order to see the true nature of feeling, we have to be mindful of the feeling.
And contemplation on feeling is not to get rid of these feelings.
Now when there is pain, you are instructed to be mindful of that pain or to make mental
notes as pain, pain and so on.
And many yogis think that it is to get rid of that pain that you have to be mindful
of it, saying pain, pain, pain.
But what the Buddha taught is different.
So Buddha said, for the full understanding of these three kinds of feelings, you practice
the foundations of mindfulness.
So when there is feeling and you try to be mindful of that feeling, that means you are
trying to see the true nature of that feeling.
You are trying to see what feeling is and you are trying to see that that feeling arises
and disappears and so that feeling is implemented and so on.
So it is in order to understand fully in this way that you have to be mindful of feelings,
not to get rid of pain or some unpleasant sensations.
So when you practice feeling contemplation, you make effort and you try to be mindful
and you get concentration and you see the feelings clearly or you comprehend the feeling
clearly.
So when you are mindful of the feeling, when you are clearly comprehending the feeling, you
are actually removing mental defilements regarding that feeling.
When the feeling is good, coverlessness or craving can arise.
So when you have a good feeling, you want it to last for a long time and you are attached
to it.
And when the feeling is painful and you have what is called grief, you have ill will
or you have anger, that is if you do not practice mindfulness.
So when you practice mindfulness, then they will not get chance to arise in your mind.
Now even when the feeling is bad or unpleasant, craving can arise.
Not with regard to that unpleasant feeling, but when there is an unpleasant feeling,
you long for a pleasant feeling, you want it to go away and you want to have the pleasant
feeling arise in you.
So even when the feeling is unpleasant, the attachment or craving can arise.
That is if you do not practice mindfulness.
Now we come to the third foundation of mindfulness.
Now you are practicing meditation and you try to be mindful of the breath or the movements
of the abdomen.
Then after some time, you are mind wanders and you are aware of it and so you make notes
as wandering, wandering, wandering.
Now, you are doing contemplation of consciousness when you are noting your mind like that,
wandering, wandering, wandering.
Now, in order to understand consciousness as thoughts in Buddhism, you need to know abidhamma.
According to abidhamma, there are two things that are called namma or mind.
And abidhamma is thought to be a combination of consciousness and mental factors.
And it is defined as the awareness of the object.
It is a very simple awareness of the object and not the awareness we use in our talk on
Vipasana meditation, so when we talk about Vipasana meditation, we say we try to be aware
of the object and that awareness is actually a mindfulness.
But here, consciousness, when we say consciousness is the awareness of the object, it is
just a mere awareness of the object, not actually at that moment, not actually knowing
whether it is, it is white or black or whatever, but just the awareness of the object.
It is a very simple awareness of the object and that is called consciousness or chita
in pali and abidhamma.
And this awareness of the object or consciousness is always accompanied by mental factors.
Now, there are many mental factors, actually there are 52 that accompany different types
of consciousness.
Now, attachment is a mental factor, hate or anger is a mental factor, delusion or ignorance
is a mental factor, jealousy is a mental factor.
And on the good side, the confidence is a mental factor, mindfulness is mental factor,
understanding is mental factor, mental factor, so there are many mental factors.
Some are good mental factors, some are bad mental factors and some are common to both
good and bad.
So the consciousness is always accompanied by some of these mental factors.
And when the consciousness is accompanied by good mental factors, then we call that consciousness,
good consciousness or wholesome consciousness.
When the consciousness is accompanied by bad mental factors and it is called unwholesome
consciousness and so on.
Although consciousness and mental factors arise at the same time, consciousness is said
to be their leader.
Consciousness is said to be the full run of these mental factors because when there is
no consciousness there can be no mental factors, no mental factors can arise when there
is no consciousness.
So consciousness is said to be their full run or leader.
This consciousness is always with us, so long as we are living, we are never without consciousness.
Then when we are first asleep in a deep sleep there is still consciousness of some kind.
So we can never be without consciousness as long as we are alive.
So awareness of the consciousness of the consciousness is called contemplating contemplation
on the consciousness or the third foundation of mindfulness.
So when a yogi makes effort and tries to be mindful of the consciousness as his mindfulness
becomes stronger and as he gets concentration he begins to see the consciousness clearly
and its characteristics and so on.
So when he is seeing the consciousness, the true nature of consciousness then he is said
to be removing mental defilements regarding that consciousness.
That means regarding that consciousness he will not have attachment to the consciousness
or anger to the consciousness.
Then fourth foundation of mindfulness is contemplation on the damas, on the damas objects.
Now when we explain the discourses that are originally in Pali and when we explain them
in English sometimes we have problems with the translation.
Some words actually cannot be translated.
One of such words is the word dhamma.
The word dhamma has many meanings.
It means differently in different places.
So we cannot give just one meaning of the word dhamma.
Now when you say dhamma saranan kachami I go to dhamma for refuge then dhamma means the
Buddha realized when he became the Buddha and also his teachings.
Sometimes dhamma means nature.
Sometimes it means learning and so on.
So there are many meanings of the word dhamma and so we cannot have just one English translation
for the word dhamma in all places.
And here also the word dhamma cannot be translated into English.
Many translators translate it as mind object or mental objects but I think that is not
accurate.
Mind object because there are many things that are called dhamma and in this discourses
on the four foundations of mindfulness if you go to the section on the contemplation
of the dhamma you will find that the five hindrances are called dhamma, five aggregates
of clinging are called dhamma and the six external bases and six internal bases are called
dhamma, seven factors of enlightenment are called dhamma and the four noble troops are called
dhamma.
So to cover all these how can we translate the word dhamma?
Some of them are mental and some of them are material so we cannot call them mental objects
or mind objects.
So it is better to leave it as it is and explain when we use the word dhamma in a certain
context.
So here also we cannot translate this word into any language.
So we say contemplation on the dhamma in the dhamma objects.
So if we want to understand what dhamma means here you go to the section on the contemplation
of the dhamma and there we find those mental hindrances and so on.
So in this discourses, mental hindrances and others there are all the five groups.
They are called dhamma like the word dhamma.
The word cheetah which is translated here as consciousness is another difficult word to translate.
But here many others translate the word cheetah as consciousness.
Now here also for one of a better word we have to use the word consciousness for cheetah.
But again it is not accurate.
Now as you now know we are with consciousness always even when we are asleep we are with
consciousness according to dhamma.
But people would say when you are in deep sleep you are unconscious or when you are
fine that you are unconscious.
So this word also is not actually accurate for the word cheetah in pali but we have to use
some English word for this word and so the word consciousness is used.
So when we use the word consciousness it is important that we define it.
We tell people what we mean by the word consciousness.
Just using the word consciousness will not be enough because people may misunderstand.
So here in a bithamma the English word consciousness is used for the word cheetah because
there is no other better word for the word cheetah.
Now the objects represented by the word dhamma are many and the objects that are called
dhamma in this discourse overlap with the other objects of body contemplation, feeling
contemplation and consciousness contemplation.
So suppose you are practicing meditation and somebody make a noise and you are angry.
So how would you note angry or anger?
If you are making notes angry angry I think you are doing the third foundation of mindfulness
because I am angry means my mind is accompanied by anger so it is very delicate.
But when you are contemplating making notes as anger, anger you are doing the dhamma object
contemplation or the fourth foundation of mindfulness.
Because now you are taking anger which is one of the mental hindrances as the object
of your attention.
Although it is not important to try out I mean to find out what contemplation you are doing
at the moment.
It is still good to know say what contemplation you are doing but please do not try to find
out what contemplation you are doing at the moment.
It will take you away from the real practice of mindfulness and drag you into thinking.
So sometimes it is difficult to see what contemplation we are doing, contemplation of consciousness
or contemplation of the dhamma objects.
But whatever object it is so long as you are mindful then you are doing the practice
of mindfulness meditation.
So you do not have to find out what contemplation you are doing at the moment.
Later on you may think about it later on means after the practice of meditation.
Now this is the four foundations of mindfulness in brief so if you want to know more details
I must refer you to the discourse on the foundations of mindfulness.
But what we learn from this passage is that we must make effort, we must practice mindfulness
so that we clearly comprehend or we clearly see the objects.
That means we see the true nature of the objects.
Only when we clearly comprehend the objects, only when we see the objects as impermanence,
suffering and non-soul, will we be able to prevent covetousness and grief from arising
in our minds.
So in order to prevent them from arising in our minds we need to comprehend the objects
clearly and in order to see the objects clearly we need to practice mindfulness and for
the practice of mindfulness we must make an effort.
So if we make effort and if we practice mindfulness we will not fail to comprehend the
objects clearly.
So when we comprehend the objects clearly then we will be removing covetousness and grief
at the same time.
So let us all make effort to be mindful, so as to comprehend the objects clearly and remove
covetousness and grief in the world.
The
