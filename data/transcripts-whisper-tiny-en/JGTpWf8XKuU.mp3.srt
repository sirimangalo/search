1
00:00:00,000 --> 00:00:01,600
Go ahead.

2
00:00:01,600 --> 00:00:08,160
How come right concentration is always portrayed as Jana?

3
00:00:08,160 --> 00:00:14,040
Because right concentration is the Jana's.

4
00:00:14,040 --> 00:00:20,680
But this is, you know, obviously a fairly controversial subject.

5
00:00:20,680 --> 00:00:27,600
But first of all, the question is, it starts with an incorrect premise,

6
00:00:27,600 --> 00:00:32,480
because the Jana's are not always portrayed as, you say, always?

7
00:00:32,480 --> 00:00:37,480
They're not always, sorry, the right concentration is not always portrayed as the Jana's.

8
00:00:37,480 --> 00:00:42,040
There is the case, at least one place where the Buddha says,

9
00:00:42,040 --> 00:00:51,040
any single pointedness of mind that has the ability to suppress the hindrances

10
00:00:51,040 --> 00:00:52,960
that is free from the five hindrances.

11
00:00:52,960 --> 00:00:55,400
This is right concentration.

12
00:00:55,400 --> 00:01:01,000
So by making that admission, he's obviously, I would say at the very least,

13
00:01:01,000 --> 00:01:06,320
he's broadening what our understanding of the word Jana should be.

14
00:01:06,320 --> 00:01:12,440
That it's not this or that state, but it's any state that is free from the hindrances.

15
00:01:12,440 --> 00:01:14,560
Because when you look at ultimate reality,

16
00:01:14,560 --> 00:01:18,080
Adjana is still only moment to moment mind states.

17
00:01:18,080 --> 00:01:22,920
So you could enter into something that could be considered a Jana in one moment.

18
00:01:22,920 --> 00:01:26,760
That moment could be a wholesome moment.

19
00:01:26,760 --> 00:01:31,640
If your mind has wholesomeness in it, you could say, well, you've entered into a Jana.

20
00:01:31,640 --> 00:01:41,960
You see, in an ultimate sense, it boils down to what's the components of that mind.

21
00:01:41,960 --> 00:01:44,800
How the path works is, well, there are obviously different ways.

22
00:01:44,800 --> 00:01:47,240
If you read the suitors, you can see the Buddha explaining this

23
00:01:47,240 --> 00:01:50,280
that some people start with heavy concentrations

24
00:01:50,280 --> 00:01:57,840
or not they have strong concentrations, so they enter into what could clearly be considered Jana.

25
00:01:57,840 --> 00:02:01,880
And then after that, they either take the Jana factors, they start to pull it apart

26
00:02:01,880 --> 00:02:04,880
and they can feel the happiness and they become aware of the happiness

27
00:02:04,880 --> 00:02:07,440
as being impermanent, suffering in oneself.

28
00:02:07,440 --> 00:02:11,000
And then they let go, they're able to let go as a result.

29
00:02:11,000 --> 00:02:16,920
Either that or they go back and look at how the Jana is based on the root by Nama.

30
00:02:16,920 --> 00:02:20,920
So they can, if it's mindfulness and breathing, they become aware of the body,

31
00:02:20,920 --> 00:02:24,040
movements of the body, the stomach, rising and falling.

32
00:02:24,040 --> 00:02:29,000
So this is one way is to go with Sama to first and then go on with Vipasana.

33
00:02:29,000 --> 00:02:32,960
If you're practicing Vipasana, then it's kind of like they develop together

34
00:02:32,960 --> 00:02:36,480
instead of first developing Sama and then Vipasana.

35
00:02:36,480 --> 00:02:39,200
They come together in the Jana moment

36
00:02:39,200 --> 00:02:43,600
because see, the eight full normal paths technically is only one moment.

37
00:02:43,600 --> 00:02:47,600
That's Manganyan, which is only one thought moment.

38
00:02:47,600 --> 00:02:52,520
The eight full normal path in a technical sense is only one moment.

39
00:02:52,520 --> 00:02:57,600
It's the path moment and that moment is what separates the mind,

40
00:02:57,600 --> 00:03:03,600
which breaks the mind away, where the mind breaks away, pulls away from Samsara.

41
00:03:03,600 --> 00:03:09,200
And then the next moment is Palañana, with the mind is in Nibana.

42
00:03:09,200 --> 00:03:12,800
So at that path moment, that's Jana.

43
00:03:12,800 --> 00:03:17,040
It's either the first Jana, the second Jana, the third Jana, or the fourth Jana.

44
00:03:17,040 --> 00:03:19,480
But it's a Lokudra Jana.

45
00:03:19,480 --> 00:03:21,520
Everyone has to enter into Jana.

46
00:03:21,520 --> 00:03:24,800
There's no way to go to Nibana without entering into the Jana,

47
00:03:24,800 --> 00:03:28,840
but that's the ultimate extreme.

48
00:03:28,840 --> 00:03:34,520
At the very least, at that moment, you have to enter Jana.

49
00:03:34,520 --> 00:03:37,960
So consider, either way, there's a buildup to Jana,

50
00:03:37,960 --> 00:03:41,200
whether you build up the Jana first and then develop wisdom

51
00:03:41,200 --> 00:03:45,880
or you build up the Jana until that very moment when wisdom and concentration

52
00:03:45,880 --> 00:03:49,720
are both perfect and morality as well,

53
00:03:49,720 --> 00:03:55,160
where morality, concentration, and wisdom come to perfection at the same time.

54
00:03:55,160 --> 00:03:57,880
And then enter into Jana at that moment.

55
00:03:57,880 --> 00:04:06,800
So it's misleading for someone to say that one way teaches that you can enter into Nibana without Jana.

56
00:04:06,800 --> 00:04:14,440
Another way teaches that you have to enter into Jana first or so on.

57
00:04:14,440 --> 00:04:18,960
The texts are quite clear that, first of all, there are many ways of developing one first

58
00:04:18,960 --> 00:04:19,960
and then the other.

59
00:04:19,960 --> 00:04:23,200
First, you gain wisdom, and then your mind quiets down.

60
00:04:23,200 --> 00:04:26,160
Or first, your mind quiets down, and then there's wisdom.

61
00:04:26,160 --> 00:04:28,320
Or you do them together or so on.

62
00:04:28,320 --> 00:04:31,280
But in the end, at that moment, everyone enters into Jana.

63
00:04:31,280 --> 00:04:33,920
It's just not a summit of Jana.

64
00:04:33,920 --> 00:04:35,960
And it's also not a Vipasana Jana.

65
00:04:35,960 --> 00:04:39,720
Because the commentaries understand there to be three types of Jana.

66
00:04:39,720 --> 00:04:45,360
Actually, Samantha Jana, Vipasana, Jana, and Locutra Jana, a super mundane,

67
00:04:45,360 --> 00:04:48,640
super mundane, meaning having Nibana as its object.

68
00:04:48,640 --> 00:04:53,280
So at that moment, one enters into a Locutra Jana.

69
00:04:53,280 --> 00:04:57,520
That's why right concentration, that's probably the easiest explanation as to why

70
00:04:57,520 --> 00:05:01,440
the eight full noble path concentration factors consider to be the Jana's

71
00:05:01,440 --> 00:05:05,920
because it's a Locutra Jana that is being talked about.

72
00:05:05,920 --> 00:05:11,760
The eight full noble path can also be understood as a pubangamanga, the precursor path,

73
00:05:11,760 --> 00:05:14,040
which is the path that you're developing up to that point.

74
00:05:14,040 --> 00:05:17,560
But all you're doing up into that point is developing the factors.

75
00:05:17,560 --> 00:05:20,880
I think we talked about this before that, you know, developing morality and

76
00:05:20,880 --> 00:05:23,600
seclusion or something, you can do that.

77
00:05:23,600 --> 00:05:25,760
But eventually, they all have to come together.

78
00:05:25,760 --> 00:05:30,760
It's called magasamanga, which means they become harmonious.

79
00:05:30,760 --> 00:05:33,920
So in the beginning, someone may have strong morality, but weak wisdom and weak

80
00:05:33,920 --> 00:05:38,040
concentration or developing one or the other.

81
00:05:38,040 --> 00:05:40,200
Eventually, they all have to come together.

82
00:05:40,200 --> 00:05:43,360
So the pubangamanga, you can enter into samata Jana, you can enter into

83
00:05:43,360 --> 00:05:50,320
Vipassana Jana, back and forth, as long as you're cultivating all eight of these

84
00:05:50,320 --> 00:06:00,920
qualities until the point where you enter into a Locutra Jana, which I'm sorry.

85
00:06:00,920 --> 00:06:05,560
The quote that I gave actually may have been incorrect, because I think there's a

86
00:06:05,560 --> 00:06:06,840
quote of that sort.

87
00:06:06,840 --> 00:06:10,840
But the quote I'm thinking of actually says, right concentration is any

88
00:06:10,840 --> 00:06:14,200
concentration that is accompanied with the other seven path factors.

89
00:06:14,200 --> 00:06:16,680
I'm sorry, that's what it says.

90
00:06:16,680 --> 00:06:20,680
But the commentaries are all talking about how the five hindrances have to be

91
00:06:20,680 --> 00:06:21,520
suppressed.

92
00:06:21,520 --> 00:06:25,480
I believe the Sutta says, the one that I'm thinking of says, it has to be

93
00:06:25,480 --> 00:06:27,760
accompanied with the other seven path factors.

94
00:06:27,760 --> 00:06:38,720
If it's accompanied with wisdom, with samadity, samasam kapah, samawajasamakamantasamawayamasamakam

95
00:06:38,720 --> 00:06:59,680
ati, if it's accompanied by these seven factors, just count it off eight there, seven

96
00:06:59,680 --> 00:07:03,480
factors, then it's right concentration.

97
00:07:03,480 --> 00:07:09,480
So samadha Jana actually is not right concentration.

98
00:07:09,480 --> 00:07:12,240
If you're talking about the Jana's that arise according to the Visudhi

99
00:07:12,240 --> 00:07:16,120
manga when it talks about samadha meditation, that's not right concentration

100
00:07:16,120 --> 00:07:19,160
in this sense, because it doesn't have the other seven factors.

101
00:07:19,160 --> 00:07:24,480
So samadha Jana doesn't have the rest of these perfected.

102
00:07:24,480 --> 00:07:28,080
Otherwise, as a person who practiced as samadha alone would enter into

103
00:07:28,080 --> 00:07:33,200
the band, this is a low-key Jana.

104
00:07:33,200 --> 00:07:39,920
Only at that moment of magganjana are all eight path factors fulfilled.

105
00:07:39,920 --> 00:08:07,040
I don't know if you have anything to add to that, it's a fairly technical question.

