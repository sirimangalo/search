1
00:00:00,000 --> 00:00:16,000
Okay, can we recollect on Amicha Dukka and Anatha during our meditation, for example, where pain appears?

2
00:00:16,000 --> 00:00:21,000
You can, but it's not Vipasana.

3
00:00:21,000 --> 00:00:27,000
It's entirely call it Vipasana, because Nik means thinking.

4
00:00:27,000 --> 00:00:32,000
I'm recollecting on impermanence, suffering, and Anatha.

5
00:00:32,000 --> 00:00:42,000
You actually do see some instances of this in the Topitika where the Buddha has you recollect as something is being impermanent.

6
00:00:42,000 --> 00:01:07,000
I don't want to say absolutely not, but the general, I think we have to interpret that carefully, because the observation that we get from meditation is that when people do that, they don't have actual experiences of impermanence, suffering, and non-stop.

7
00:01:07,000 --> 00:01:12,000
The truth of Anicha Dukka and Anatha is that you see them in every moment.

8
00:01:12,000 --> 00:01:17,000
Everything is impermanence, suffering, and non-stop. They don't really exist as entities.

9
00:01:17,000 --> 00:01:24,000
They are the realization that comes as a result of seeing things as they are.

10
00:01:24,000 --> 00:01:28,000
And if you notice, they're all really the absence of something.

11
00:01:28,000 --> 00:01:41,000
It's destroying the illusion of permanent stability, satisfaction, and controllability.

12
00:01:41,000 --> 00:01:43,000
So when you see, for example, you watch the stomach rising.

13
00:01:43,000 --> 00:01:48,000
At the moment of seeing rising, you're seeing impermanence, suffering, and not self.

14
00:01:48,000 --> 00:01:55,000
You're seeing something that is inherently impermanence, suffering, and not self.

15
00:01:55,000 --> 00:02:17,000
And so the wisdom that we're looking for is as a result of breaking away from our delusions, our ideas that these things are permanent, that these things are satisfying, that these things are controllable.

16
00:02:17,000 --> 00:02:30,000
So you'll see something very simple, that the rising and falling is going of its own accord, that it's changing, that it's unsatisfying, that it's uncontrollable.

17
00:02:30,000 --> 00:02:35,000
You see these things, and it will jar with your expectations.

18
00:02:35,000 --> 00:02:44,000
It will jar with your intentions, your desires to make it stable, your desires to make it satisfying, your desires to make it controllable.

19
00:02:44,000 --> 00:02:58,000
And it's that jarring, that is the awakening that occurs, the change in the mind, the realization, seeing things that you never saw before.

20
00:02:58,000 --> 00:03:13,000
It has to come by itself, and it really should come from intensive meditation practice, where you dedicate yourself to seeing things from moment to moment.

21
00:03:13,000 --> 00:03:17,000
That's impermanence, suffering, and not self.

22
00:03:17,000 --> 00:03:21,000
Basically, we pass in it as the result of the meditation.

23
00:03:21,000 --> 00:03:23,000
It's not the practice that we do.

24
00:03:23,000 --> 00:03:29,000
So everyone's always complaining about we pass in the meditation, saying, where did the Buddha teach we pass in a meditation?

25
00:03:29,000 --> 00:03:31,000
You can't practice, we pass in it.

26
00:03:31,000 --> 00:03:32,000
And they're right.

27
00:03:32,000 --> 00:03:39,000
We don't call it, we pass in a meditation, because we want people to practice, we pass in it, and we want people to practice inside.

28
00:03:39,000 --> 00:03:45,000
We call it, we pass in a meditation, inside meditation, because that's what we hope people to get from it.

29
00:03:45,000 --> 00:03:50,000
We pass in a meditation is actually the practice of the four foundations of mindfulness.

30
00:03:50,000 --> 00:03:59,000
So if you look at the four foundations of mindfulness, the Satipatanasutta, you won't see the Buddha telling you to contemplate on impermanence, suffering, and on self.

31
00:03:59,000 --> 00:04:05,000
You'll see him telling you to contemplate on things as they are, which is very common.

32
00:04:05,000 --> 00:04:14,000
You'll see the Buddha talking about Yatab, Buddha in the desolate, seeing things, knowing, and seeing things as they are.

33
00:04:14,000 --> 00:04:19,000
And that is impermanence, suffering, and on self.

34
00:04:19,000 --> 00:04:26,000
Once you can see things clearly as they are, you will see impermanence suffering.

35
00:04:26,000 --> 00:04:28,000
You'll see that you can't control things.

36
00:04:28,000 --> 00:04:30,000
You'll see that things don't go according to your wishes.

37
00:04:30,000 --> 00:04:34,000
You'll see that clinging to them causes your suffering, and so on and so on.

38
00:04:34,000 --> 00:04:41,000
And you'll slowly, slowly give up and let go of things that you cling to.

39
00:04:41,000 --> 00:04:49,000
And that is, as a result of seeing them clearly as they are, of seeing that they're not what you expect them to be.

40
00:04:49,000 --> 00:04:53,000
They're not permanent, they're not satisfying, they're not controlled.

41
00:04:53,000 --> 00:04:57,000
So we pass in a, is the result, not the practice, just as with some into meditation.

42
00:04:57,000 --> 00:04:59,000
You don't practice tranquility.

43
00:04:59,000 --> 00:05:03,000
You practice some meditation technique in order to bring about tranquility.

44
00:05:03,000 --> 00:05:05,000
These are the two different kinds of meditation.

45
00:05:05,000 --> 00:05:08,000
It's not people say, how can you split them up easily?

46
00:05:08,000 --> 00:05:10,000
One leads to one thing.

47
00:05:10,000 --> 00:05:11,000
One leads to another.

48
00:05:11,000 --> 00:05:13,000
Certain meditations don't lead to insight.

49
00:05:13,000 --> 00:05:20,000
Certain meditations don't directly lead to tranquility in a worldly sense.

50
00:05:20,000 --> 00:05:30,000
They might only lead to the cessation, the experience of realizing the Bana, which is, of course, the greatest tranquility of all.

51
00:05:30,000 --> 00:05:36,000
So that's the separation. Does that make sense?

52
00:05:36,000 --> 00:05:39,000
Is that the answer to your question?

53
00:05:39,000 --> 00:05:44,000
I wouldn't worry too much about, we pass in a very more about Satyapatana.

54
00:05:44,000 --> 00:06:08,000
That's the practice.

