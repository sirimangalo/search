1
00:00:00,000 --> 00:00:06,000
Experience the physical reality with a very harsh and unpleasant and ugly.

2
00:00:06,000 --> 00:00:14,000
Everything is, you know, the physical form is unpleasant.

3
00:00:14,000 --> 00:00:20,000
When you're satiated, everything is wonderful and the physical form is,

4
00:00:20,000 --> 00:00:23,000
is comfortable. It's pleasing.

5
00:00:23,000 --> 00:00:35,000
And it says, when you're aware of this, you can see how reality changes for a moment moment

6
00:00:35,000 --> 00:00:43,000
and how we experience it one moment. It's very different from how we experience it the next.

7
00:00:43,000 --> 00:00:52,000
And permanence. Start to see that, thereby, we can't find satisfaction or real happiness

8
00:00:52,000 --> 00:00:56,000
by clinging to things or by trying to fix things.

9
00:00:56,000 --> 00:01:05,000
Because they're...

