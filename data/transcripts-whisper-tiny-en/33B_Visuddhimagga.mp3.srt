1
00:00:00,000 --> 00:00:04,500
So, I forgot the ear, I forgot the nose, tongue, and so on.

2
00:00:04,500 --> 00:00:08,700
So, this is the order given in without the maga.

3
00:00:08,700 --> 00:00:14,000
It is a little different, the order is a little different from that given in the manual

4
00:00:14,000 --> 00:00:19,000
of a beat-up.

5
00:00:19,000 --> 00:00:26,200
And then, the common data explains the words in Paoli, Chaku, Sauta, and so on.

6
00:00:26,200 --> 00:00:31,200
But they are referred back to the pieces.

7
00:00:31,200 --> 00:00:41,200
For the last three, so please look at the last three on the sheet, a full column,

8
00:00:41,200 --> 00:00:47,200
I shall something, and final knowledge, faculty, final law, faculty, and so on.

9
00:00:47,200 --> 00:00:55,200
So, we have understanding faculty, I shall et cetera, faculty, final knowledge, faculty,

10
00:00:55,200 --> 00:00:57,200
and final law, faculty.

11
00:00:57,200 --> 00:01:05,200
Now, the last three are also understanding, or punya.

12
00:01:05,200 --> 00:01:11,200
So, understanding is also punya, and the last three are also punya.

13
00:01:11,200 --> 00:01:23,200
And the first one, I shall et cetera, is punya at the moment of first maga.

14
00:01:23,200 --> 00:01:33,200
And final knowledge, faculty, means punya at the moment of first fruit,

15
00:01:33,200 --> 00:01:44,200
and then second path fruit, third path fruit, and food path.

16
00:01:44,200 --> 00:01:55,200
And final Noah, faculty, is the last or the fourth fruit consciousness,

17
00:01:55,200 --> 00:02:01,200
arising with the fourth fruit consciousness.

18
00:02:01,200 --> 00:02:05,200
So, the first one is first maga, first path.

19
00:02:05,200 --> 00:02:15,200
The middle one is the sixth in between.

20
00:02:15,200 --> 00:02:23,200
And the last one is the fourth fruit.

21
00:02:23,200 --> 00:02:27,200
So, they are given a different name here.

22
00:02:27,200 --> 00:02:33,200
It is, I shall et cetera, faculty, final knowledge, faculty, and final law of faculty.

23
00:02:33,200 --> 00:02:38,200
So, they are explaining, and paragraph three.

24
00:02:38,200 --> 00:02:50,200
And then the one, the endria, endria with, with, whose English translation is faculties,

25
00:02:50,200 --> 00:02:58,200
is explaining paragraph four, and paragraph five.

26
00:02:58,200 --> 00:03:10,200
Now, the, when explaining the word, endria, the paluette endria,

27
00:03:10,200 --> 00:03:21,200
the common data just follows the, the, the aphorism given in the Sanskrit grammar by panini.

28
00:03:21,200 --> 00:03:28,200
You, you can find the aphorism at the bottom of the page, five, six, zero.

29
00:03:28,200 --> 00:03:31,200
There you see a compare panini, five, two, ninety-three, ingredient,

30
00:03:31,200 --> 00:03:38,200
entra-lingan, entra-dristan, entra-dristan, entra-dristan, entra-dristan,

31
00:03:38,200 --> 00:03:40,200
entra-dastamity-wa.

32
00:03:40,200 --> 00:03:49,200
So, this is the aphorism found in the Sanskrit grammar of panini.

33
00:03:49,200 --> 00:04:00,200
And when explaining the word angria here, the, the, the common data, autocassar followed this, this aphorism,

34
00:04:00,200 --> 00:04:08,200
only the last one is not taken, entra-dastam.

35
00:04:08,200 --> 00:04:14,200
And the word, in the word, endria, there's a word in, in, in, in, in, in, right.

36
00:04:14,200 --> 00:04:22,200
So, it is explained here that, that the word, endra, or endra, means, go to bed, comma,

37
00:04:22,200 --> 00:04:28,200
cusola, or a cusola, comma, or it means the Buddha.

38
00:04:28,200 --> 00:04:32,200
So, you, you, you, you, if you understand this, then you will understand all.

39
00:04:32,200 --> 00:04:39,200
So, the word, in, in, in the word, endria, we have what the word, endra, endra, endra,

40
00:04:39,200 --> 00:04:44,200
and endra is explained here to me, to things, right.

41
00:04:44,200 --> 00:04:49,200
One is what, cusola, or a cusola, comma.

42
00:04:49,200 --> 00:04:53,200
And the other is the Buddha.

43
00:04:53,200 --> 00:04:59,200
And then there is the yah, right, or the yah, in the word, endria.

44
00:04:59,200 --> 00:05:06,200
So, the word, the, the suffix, yah, is explained to me many things.

45
00:05:06,200 --> 00:05:16,200
That is what here, and the mark prepared by, and then taught by, seen by, first,

46
00:05:16,200 --> 00:05:18,200
that by.

47
00:05:18,200 --> 00:05:29,200
Now, when it is the mark, when we take, the, the, the, the, the, the, the, the endra means

48
00:05:29,200 --> 00:05:34,200
comma, the mark of comma, is called faculty here.

49
00:05:34,200 --> 00:05:39,200
Because, by looking at the, once again, we can say, he had good comma in the past,

50
00:05:39,200 --> 00:05:41,200
or he had bad comma in the past.

51
00:05:41,200 --> 00:05:46,200
Because, good comma creates, good eyesensitivity, and bad comma creates,

52
00:05:46,200 --> 00:05:48,200
bad eyesensitivity.

53
00:05:48,200 --> 00:05:54,200
Say, some people don't, don't have to use eye glasses all their lives,

54
00:05:54,200 --> 00:05:57,200
but some people have to use from childhood.

55
00:05:57,200 --> 00:06:02,200
So, that depends on their comma, because the eyesensitivity is the result of comma.

56
00:06:02,200 --> 00:06:07,200
So, we, we, we, we ordered bad comma in the first, as long as the,

57
00:06:07,200 --> 00:06:10,200
the eyesensitivity is concerned, because we have to use glasses.

58
00:06:10,200 --> 00:06:12,200
But you are a good comma.

59
00:06:12,200 --> 00:06:14,200
Well, just to the eyes.

60
00:06:14,200 --> 00:06:22,200
So, this is, so we, we know good or bad comma from the, I, I, here, and so on.

61
00:06:22,200 --> 00:06:26,200
So, they are called mark of, they could be good or bad comma.

62
00:06:26,200 --> 00:06:29,200
But here, it is translated as mark of a ruler.

63
00:06:29,200 --> 00:06:32,200
The ruler here means good or bad comma.

64
00:06:32,200 --> 00:06:34,200
The second meaning is what?

65
00:06:34,200 --> 00:06:36,200
Prepared by comma.

66
00:06:36,200 --> 00:06:39,200
That means made by, or caused by comma.

67
00:06:39,200 --> 00:06:45,200
The ruler here means comma also.

68
00:06:45,200 --> 00:06:52,200
Now, the third meaning is the introt by.

69
00:06:52,200 --> 00:06:57,200
So, taught by, it is not taught by comma, but taught by Buddha.

70
00:06:57,200 --> 00:07:02,200
So, the third meaning is taught by, that which is taught by the Buddha.

71
00:07:02,200 --> 00:07:05,200
And the fourth meaning is what?

72
00:07:05,200 --> 00:07:07,200
Seem by.

73
00:07:07,200 --> 00:07:09,200
Again, Seem by the Buddha.

74
00:07:09,200 --> 00:07:16,200
And then the last meaning is, first start by, so first start by the Buddha.

75
00:07:16,200 --> 00:07:23,200
That means, hmm, ruler, which is in this cultivation of domain and sum in his cultivation

76
00:07:23,200 --> 00:07:25,200
of development.

77
00:07:25,200 --> 00:07:33,200
Cultivating, cultivation of domain simply means he takes them as object.

78
00:07:33,200 --> 00:07:38,200
And cultivation of development means he develops them.

79
00:07:38,200 --> 00:07:53,200
So, Buddha, Buddha, cultivated, these, these, these, these, these are cultivated by the Buddha.

80
00:07:53,200 --> 00:07:57,200
And that is why they are called, say, cultivated by the Buddha.

81
00:07:57,200 --> 00:07:58,200
I mean, interior.

82
00:07:58,200 --> 00:08:04,200
So, how, how did, Buddha cultivate their, Buddha cultivate, cultivated them in two ways.

83
00:08:04,200 --> 00:08:07,200
One is, taking them as object.

84
00:08:07,200 --> 00:08:18,200
That means when Buddha, take Nirvana as object and entered into, entered into attainment.

85
00:08:18,200 --> 00:08:25,200
And it means he is fostering Nirvana by way of taking it as object.

86
00:08:25,200 --> 00:08:33,200
And at other times, he, he may be practicing, or he may be in some other attainment.

87
00:08:33,200 --> 00:08:40,200
And so, in that case, he is cultivating his and the cultivation of development.

88
00:08:40,200 --> 00:08:50,200
So, Buddha, Buddha reacts or acts towards Dharma in two ways, taking as object and also making

89
00:08:50,200 --> 00:08:52,200
them happen in his mind.

90
00:08:52,200 --> 00:08:55,200
So, first start by a ruler.

91
00:08:55,200 --> 00:08:57,200
That means first start by the Buddha.

92
00:08:57,200 --> 00:09:01,200
So, there are also going to harmony meanings, actually, five meanings.

93
00:09:01,200 --> 00:09:05,200
The first meaning is what, mark of karma.

94
00:09:05,200 --> 00:09:08,200
The second meaning is prepared by karma.

95
00:09:08,200 --> 00:09:12,200
The third meaning is taught by the Buddha.

96
00:09:12,200 --> 00:09:15,200
The fourth meaning is seen by the Buddha.

97
00:09:15,200 --> 00:09:20,200
The fifth meaning is first start by the Buddha.

98
00:09:20,200 --> 00:09:23,200
Now, this should be B, right?

99
00:09:23,200 --> 00:09:38,200
In about the middle of the paragraph, five.

100
00:09:38,200 --> 00:09:55,200
Oh, A, B, A, D, B, C, E. No, I think, I think he is following the sequence given there

101
00:09:55,200 --> 00:09:58,200
in paragraph four, A, B, C, and so on.

102
00:09:58,200 --> 00:10:07,200
So, his is also correct.

103
00:10:07,200 --> 00:10:16,200
So, being the mark of a ruler, having been prepared by a ruler, that means by karma.

104
00:10:16,200 --> 00:10:23,200
That taught by a ruler, by the Buddha, seen by the Buddha, and first that by the Buddha,

105
00:10:23,200 --> 00:10:24,200
right?

106
00:10:24,200 --> 00:10:28,200
And then the others are not difficult to understand, say, characteristic and so on, and

107
00:10:28,200 --> 00:10:38,200
as to the order.

108
00:10:38,200 --> 00:10:43,200
There is only division of the life faculty, only life faculty has division.

109
00:10:43,200 --> 00:10:45,200
The others are only one.

110
00:10:45,200 --> 00:10:51,200
For that is too full as the material life faculty and the immaterial life faculty.

111
00:10:51,200 --> 00:10:59,200
You know, GVDA, there are two kinds of GVDA.

112
00:10:59,200 --> 00:11:02,200
Nama, GVDA, and Ruba, GVDA, the mental GVDA and physical GVDA.

113
00:11:02,200 --> 00:11:06,200
They are of, it is of two kinds and the others are only one time.

114
00:11:06,200 --> 00:11:10,200
There is no division of the others.

115
00:11:10,200 --> 00:11:15,200
And then their function.

116
00:11:15,200 --> 00:11:23,200
Their function is actually, since they are called faculties or Andrea, that means they are predominant.

117
00:11:23,200 --> 00:11:26,200
So, they make others follow their ways.

118
00:11:26,200 --> 00:11:29,200
That is what is meant by what they do.

119
00:11:29,200 --> 00:11:35,200
Andrea means making other people follow your follow his ways.

120
00:11:35,200 --> 00:11:37,200
That is Andrea.

121
00:11:37,200 --> 00:11:42,200
Exercising his wish over others.

122
00:11:42,200 --> 00:11:48,200
So, here the life faculty's function is too caused by its own keenness, slowness, etc.

123
00:11:48,200 --> 00:11:56,200
The occurrence of eye consciousness and associated states, etc. in a mode parallel to its own.

124
00:11:56,200 --> 00:12:01,200
That means making them follow its own mode.

125
00:12:01,200 --> 00:12:08,200
That means when you have a good eye sensitivity, you have good eye consciousness.

126
00:12:08,200 --> 00:12:13,200
When you have eye sensitivity, you have good eye consciousness.

127
00:12:13,200 --> 00:12:18,200
So, the keenness or, I don't know the slowness is good here.

128
00:12:18,200 --> 00:12:20,200
It should be dullness.

129
00:12:20,200 --> 00:12:27,200
So, keenness and dullness, etc.

130
00:12:27,200 --> 00:12:40,200
So, the keenness and dullness of the eye consciousness is governed by the keenness and dullness of eye sensitivity.

131
00:12:40,200 --> 00:12:45,200
That is why eye sensitivity has something like an authority over eye consciousness.

132
00:12:45,200 --> 00:12:47,200
That is how it functions.

133
00:12:47,200 --> 00:12:59,200
So, the meaning of Andrea of faculty is to exercise authority over.

134
00:12:59,200 --> 00:13:10,200
And then to go down, the different functions are mentioned and they are not difficult to understand.

135
00:13:10,200 --> 00:13:18,200
So, about last four lines, that of the final knowledge faculty is both to attenuate and abandon,

136
00:13:18,200 --> 00:13:26,200
respectively, last ill, etc. and to subject coolness and states to its own mastery.

137
00:13:26,200 --> 00:13:36,200
Now, final knowledge means the knowledge between the first manga and the last, the last fruit.

138
00:13:36,200 --> 00:13:41,200
You know, first manga, first fruit, second manga, second fruit, and so on.

139
00:13:41,200 --> 00:13:52,200
So, the first manga and the understanding, concomited with the first part is called, I shall

140
00:13:52,200 --> 00:13:55,200
and so on.

141
00:13:55,200 --> 00:14:09,200
And then the knowledge or understanding, concomited with first fruit until the fourth manga is called,

142
00:14:09,200 --> 00:14:10,200
final knowledge.

143
00:14:10,200 --> 00:14:19,200
So, the final knowledge faculty is both to attenuate and abandon, respectively, last ill will, etc.

144
00:14:19,200 --> 00:14:33,200
Attenuate refers to the second cut, because the second part does not eradicate any more mental

145
00:14:33,200 --> 00:14:38,200
defilements, but it makes the remaining mental defilements weaker and weaker.

146
00:14:38,200 --> 00:14:40,200
So, it attenuates.

147
00:14:40,200 --> 00:14:48,200
So, attenuates is for second manga and abandon is for third and fourth manga.

148
00:14:48,200 --> 00:14:58,200
Because third manga abandons, what you call, sensual desire and ill will all together.

149
00:14:58,200 --> 00:15:04,200
And then the food manga eradicates the remaining mental defilements.

150
00:15:04,200 --> 00:15:13,200
So, attenuate refers to second manga, abandon, refers to third and fourth matters.

151
00:15:13,200 --> 00:15:20,200
And that of the final knowing, that is the last one, faculty is both to abandon and devour

152
00:15:20,200 --> 00:15:25,200
and all functions, because it has already done its own own own duty.

153
00:15:25,200 --> 00:15:30,200
And so, there is no more to be done.

154
00:15:30,200 --> 00:15:36,200
And as to playing, that means which faculty belong to which play.

155
00:15:36,200 --> 00:15:43,200
So, the IEL knows turn body and so on.

156
00:15:43,200 --> 00:15:47,200
They belong to sense fear, kama wajara, because they are all rubas.

157
00:15:47,200 --> 00:15:51,200
The main faculty, life faculty, and economic difficulty.

158
00:15:51,200 --> 00:15:56,200
And the faculties of faith, energy, mentalness, concentration and understanding are included

159
00:15:56,200 --> 00:15:58,200
in the four planes.

160
00:15:58,200 --> 00:16:01,200
That means they belong to all four planes.

161
00:16:01,200 --> 00:16:08,200
kama wajara, ruba wajara, aluba wajara, and local tras.

162
00:16:08,200 --> 00:16:24,200
Because, mind faculty, life faculty and so on, are, mind faculty's consciousness.

163
00:16:24,200 --> 00:16:30,200
And life faculty and others are, major states or key diseases.

164
00:16:30,200 --> 00:16:34,200
So, they are included in all four planes.

165
00:16:34,200 --> 00:16:41,200
That the joy faculty is included in three planes, somanasa.

166
00:16:41,200 --> 00:16:47,200
Namely, sense fear, find materials fear and super mundane, not in materials sphere.

167
00:16:47,200 --> 00:16:54,200
The last three are super mundane only.

168
00:16:54,200 --> 00:16:59,200
The last we mean, I shall come to know and so on.

169
00:16:59,200 --> 00:17:03,200
This is how the exposition will be known here as to play.

170
00:17:03,200 --> 00:17:10,200
So, according to this, we understand that high faculty belongs to sense fear.

171
00:17:10,200 --> 00:17:13,200
Here, faculty belongs to sense fear and so on.

172
00:17:13,200 --> 00:17:16,200
And mind faculty belongs to four planes and so on.

173
00:17:16,200 --> 00:17:19,200
So, four planes means four spheres, right?

174
00:17:19,200 --> 00:17:24,200
kama wajara, ruba wajara, and find materials fear, aluba wajara, and materials fear.

175
00:17:24,200 --> 00:17:29,200
And local dry is also called as fear, not included here.

176
00:17:29,200 --> 00:17:32,200
It comprises only of three, right?

177
00:17:32,200 --> 00:17:38,200
kama wajara, ruba wajara, and aruba wajara.

178
00:17:38,200 --> 00:17:43,200
Now, if you look at the chai, I mean, this shi,

179
00:17:43,200 --> 00:17:47,200
let us go through the basis, elements and faculties.

180
00:17:47,200 --> 00:17:59,200
So, in order to, in order to please its listeners or susceptibility of the listener,

181
00:17:59,200 --> 00:18:03,200
order two in different ways.

182
00:18:03,200 --> 00:18:09,200
One and the same thing is called a base, an element, a faculty, and so on.

183
00:18:09,200 --> 00:18:18,200
So, here, I have colored them.

184
00:18:18,200 --> 00:18:23,200
So, I base, I element, and I fairly, I want at the same thing.

185
00:18:23,200 --> 00:18:27,200
Yeah, actually, I sensitivity.

186
00:18:27,200 --> 00:18:28,200
Do the same thing?

187
00:18:28,200 --> 00:18:30,200
Yeah, they mean the same thing.

188
00:18:30,200 --> 00:18:32,200
So, the same color.

189
00:18:32,200 --> 00:18:33,200
Yeah.

190
00:18:33,200 --> 00:18:37,200
So, sometimes it is called I base, sometimes I element,

191
00:18:37,200 --> 00:18:40,200
sometimes I forgotten, but the same thing.

192
00:18:40,200 --> 00:18:44,200
So, here, no-time worry base, here, no-time worry,

193
00:18:44,200 --> 00:18:48,200
element, here, no-time worry, faculty, same thing.

194
00:18:48,200 --> 00:18:52,200
And then, let's go to visible database.

195
00:18:52,200 --> 00:18:56,200
visible database, visible data element, same thing.

196
00:18:56,200 --> 00:19:00,200
Sound, audio, flavor, tangible data base, and tangible data element.

197
00:19:00,200 --> 00:19:02,200
They mean the same thing.

198
00:19:02,200 --> 00:19:07,200
But, mine piece, it is a little different.

199
00:19:07,200 --> 00:19:16,200
Now, mine piece means all changes, right?

200
00:19:16,200 --> 00:19:23,200
So, in the elements, mine piece is divided into eight.

201
00:19:23,200 --> 00:19:33,200
Mine element, and then I consensus every minute, so on.

202
00:19:33,200 --> 00:19:34,200
Right?

203
00:19:34,200 --> 00:19:36,200
Oh, I mean, seven.

204
00:19:36,200 --> 00:19:38,200
Seven.

205
00:19:38,200 --> 00:19:40,200
Seven.

206
00:19:40,200 --> 00:19:41,200
Seven.

207
00:19:41,200 --> 00:19:45,200
It is called, we need another value.

208
00:19:45,200 --> 00:19:49,200
Seven consists, we are called seven consciousness elements.

209
00:19:49,200 --> 00:19:54,200
So, mine piece is divided into seven elements, here.

210
00:19:54,200 --> 00:19:59,200
Mine element, I consensus every minute, so on.

211
00:19:59,200 --> 00:20:08,200
So, mine element means, again, the three in my chart number 28, 18, and 25.

212
00:20:08,200 --> 00:20:11,200
So, these are mine elements, right?

213
00:20:11,200 --> 00:20:17,200
18, I mean 28, 18, and 25.

214
00:20:17,200 --> 00:20:25,200
And then I consensus element means, in the chart, number 13, and 20.

215
00:20:25,200 --> 00:20:30,200
Here, consciousness element, 14, and 21.

216
00:20:30,200 --> 00:20:33,200
Nose, 15, and 22.

217
00:20:33,200 --> 00:20:35,200
Turn, 16, and 23.

218
00:20:35,200 --> 00:20:40,200
Body, consciousness element, 17, and 24.

219
00:20:40,200 --> 00:20:45,200
All the others are mine consciousness element.

220
00:20:45,200 --> 00:20:59,200
And, among the faculties, mine faculty and mine base are the same.

221
00:20:59,200 --> 00:21:06,200
So, mine base means, all consciousness and mine faculty also means, all consciousness.

222
00:21:06,200 --> 00:21:09,200
Now, we can't do tamar, mandal database.

223
00:21:09,200 --> 00:21:12,200
I want to call it tamar, please.

224
00:21:12,200 --> 00:21:19,200
So, tamar, base, and tamar, the tamar element are the same.

225
00:21:19,200 --> 00:21:24,200
But, in faculties, they are mentioned in different ways.

226
00:21:24,200 --> 00:21:32,200
So, from an entity, faculty, masculinity, faculty, life faculty, and pleasure means,

227
00:21:32,200 --> 00:21:43,200
tamar, pain means, dukar, joining soma nasa, grief means, dorma nasa, mental.

228
00:21:43,200 --> 00:21:49,200
The first pleasure and pain of body, pleasure and body pain, joy and grief, and

229
00:21:49,200 --> 00:21:51,200
equanimity and mental.

230
00:21:51,200 --> 00:21:55,200
And then, faith, faculty, that means just faith, energy,

231
00:21:55,200 --> 00:21:58,200
mentalness, concentration, and understanding.

232
00:21:58,200 --> 00:22:02,200
And then, they are the other three kinds of understanding also.

233
00:22:02,200 --> 00:22:10,200
So, they all belong to what, tamar elements or tamar base, no color.

234
00:22:10,200 --> 00:22:19,200
You may color a red or any other color, or you can just leave them as they are.

235
00:22:19,200 --> 00:22:28,200
So, they, from an entity, belong to what, what element, tamar element, what, base,

236
00:22:28,200 --> 00:22:31,200
tamar base, and so on.

237
00:22:31,200 --> 00:22:38,200
So, this way, you have a clear provision of bases, elements, and faculties.

238
00:22:38,200 --> 00:22:52,200
So, when you, when you read the wizarding manga again, and please have it ready and prefer to

239
00:22:52,200 --> 00:22:53,200
it.

240
00:22:53,200 --> 00:22:54,200
Okay.

241
00:22:54,200 --> 00:22:59,200
We can go a little into the truths.

242
00:22:59,200 --> 00:23:02,200
Satya.

243
00:23:02,200 --> 00:23:10,200
So, the probably what Satya is translated as truth here, and then, meaning will come later.

244
00:23:10,200 --> 00:23:15,200
So, here, as to class, the meanings of the truths of suffering, etc.

245
00:23:15,200 --> 00:23:22,200
Analyzes four in each case that are real, not unreal, not otherwise, and must be penetrated

246
00:23:22,200 --> 00:23:24,200
by those penetrating suffering, etc.

247
00:23:24,200 --> 00:23:26,200
According, as it is said.

248
00:23:26,200 --> 00:23:33,200
There are said to be four meanings each of the truth.

249
00:23:33,200 --> 00:23:43,200
So, suffering's meaning is oppressing, being formed, burning, and changing.

250
00:23:43,200 --> 00:23:48,200
So, these four meanings belong to the first truth.

251
00:23:48,200 --> 00:23:58,200
And then, to the second truth, what?

252
00:23:58,200 --> 00:24:00,200
Accumulating souls, bondage, and impending.

253
00:24:00,200 --> 00:24:10,200
And then, the third truths, meaning, escape, seclusion, being unformed, and deadlessness.

254
00:24:10,200 --> 00:24:18,200
And the fourth truth, meaning is outlet, cause, seeing, and predominant.

255
00:24:18,200 --> 00:24:26,200
So, these are the four meanings of the part.

256
00:24:26,200 --> 00:24:35,200
Likewise, suffering's meaning of oppressing, meaning of being formed, meaning of burning, meaning

257
00:24:35,200 --> 00:24:38,200
of change, its meaning of penetration to, and so on.

258
00:24:38,200 --> 00:24:43,200
So, suffering, etc. should be understood according to the four meanings analyzed in each

259
00:24:43,200 --> 00:24:44,200
case.

260
00:24:44,200 --> 00:24:52,200
So, they are the meanings of the first four, meaning of tooker.

261
00:24:52,200 --> 00:25:02,200
When you look at tooker, when you concentrate on tooker, you see that it is oppressing,

262
00:25:02,200 --> 00:25:08,200
and it is being formed, it is burning, or it burns us, and it is also changing, and so on.

263
00:25:08,200 --> 00:25:13,200
And then, as to derivation, and division by character, etc.

264
00:25:13,200 --> 00:25:27,200
So, the derivation of the word dukra, samudya, niroda, and niroda gamini, bitybita, are given in paragraph 16, 17, 18, and 19.

265
00:25:27,200 --> 00:25:42,200
And they may be fanciful, but if you take the lighting, finding about the words, I think

266
00:25:42,200 --> 00:25:44,200
it will be interesting.

267
00:25:44,200 --> 00:25:53,200
So, tukra is said to be coming from due and car, right, in the 16, and due is bad, and

268
00:25:53,200 --> 00:26:03,200
car is what about?

269
00:26:03,200 --> 00:26:11,200
Dukra, the word dukra is met with a sense of vibe, a bed of wire, and then car means empty.

270
00:26:11,200 --> 00:26:19,200
So, something which is bad or something which is vibe and empty is called dukra.

271
00:26:19,200 --> 00:26:24,200
It refers to this vibe, because it is the heart of many dangers, and it is empty because

272
00:26:24,200 --> 00:26:30,200
it is devoid of the lustiness, beauty, pleasure, and self, conceived by rush people.

273
00:26:30,200 --> 00:26:36,200
So, it is called dukra, bedness, suffering pain, because of vialness and emptiness.

274
00:26:36,200 --> 00:26:48,200
Now, if we do not go further and apply it to dukra, it is alright, but if we try to explain

275
00:26:48,200 --> 00:26:55,200
dukra, in this way, we run into difficulties, because the word dukra happiness, right?

276
00:26:55,200 --> 00:27:04,200
Dukra says, let us assume it is good, but car, if car is to mean devoid of lustiness,

277
00:27:04,200 --> 00:27:11,200
devoid of beauty, devoid of pleasure, devoid of self, then what about nibana?

278
00:27:11,200 --> 00:27:21,200
Nibana is called the highest dukra, nibana ever lasts forever, nibana could be said to be

279
00:27:21,200 --> 00:27:27,200
beauty, nibana is pleasure, only nibana is not self.

280
00:27:27,200 --> 00:27:37,200
So, if we apply similarly the explanation given here to dukra, then we run into trouble.

281
00:27:37,200 --> 00:27:42,200
So, it is just an explanation of the word.

282
00:27:42,200 --> 00:27:45,200
And Samurya has three parts.

283
00:27:45,200 --> 00:27:48,200
Sam, good, ayya.

284
00:27:48,200 --> 00:27:59,200
So, Sam has the meaning of concos or coming together, and good, or good, means rising up,

285
00:27:59,200 --> 00:28:08,200
coming up, so to up. And then ayya, he knows the reason.

286
00:28:08,200 --> 00:28:13,200
So, he is coming together rising up reason.

287
00:28:13,200 --> 00:28:18,200
That is the meaning of the word Samurya, the second noble truth, original suffering.

288
00:28:18,200 --> 00:28:26,200
And this second truth is the reason for the arising of suffering when combined with remaining conditions.

289
00:28:26,200 --> 00:28:36,200
So, reason for the arising when combined with remaining conditions.

290
00:28:36,200 --> 00:28:45,200
When it comes together with other conditions, that means it is not the only condition of dukra.

291
00:28:45,200 --> 00:28:51,200
But it is a prominent condition of dukra, because there are other conditions to like ignorance.

292
00:28:51,200 --> 00:28:59,200
So, without the technology, they know no craving. So, craving is said to be here, the origin of dukra.

293
00:28:59,200 --> 00:29:03,200
But it does not mean that craving is the only origin of dukra.

294
00:29:03,200 --> 00:29:08,200
But it is a prominent one. That is why it is called the origin of dukra.

295
00:29:08,200 --> 00:29:16,200
But the others like ignorance is also the origin of dukra.

296
00:29:16,200 --> 00:29:23,200
So, when it comes into combination with other conditions, it produces dukra.

297
00:29:23,200 --> 00:29:30,200
So, it is the reason of cause for the arising of suffering when combined with the remaining conditions.

298
00:29:30,200 --> 00:29:37,200
And then the next one is knee rhoda, the third noble truth, nibana.

299
00:29:37,200 --> 00:29:46,200
But here, knee rhoda is divided into knee and rhoda. And knee means no absence.

300
00:29:46,200 --> 00:29:52,200
And rhoda means a prison. Rhoda really means restriction.

301
00:29:52,200 --> 00:29:56,200
So, when you are in prison, you are restricted.

302
00:29:56,200 --> 00:30:03,200
So, nibana is opposite of that, no restriction, freedom.

303
00:30:03,200 --> 00:30:14,200
So, nibana is called dukra, knee rhoda. But it is just translated as cessation of suffering.

304
00:30:14,200 --> 00:30:22,200
And then, knee rhoda, gami, nibada. So, that is the way leading to cessation.

305
00:30:22,200 --> 00:30:28,200
The fourth noble truth is called dukra, knee rhoda, gami, nibada.

306
00:30:28,200 --> 00:30:34,200
So, the way leading to the cessation of suffering.

307
00:30:34,200 --> 00:30:46,200
And then, the part in what area such is explained in four ways, paragraph 20, 21, and 22.

308
00:30:46,200 --> 00:30:55,200
Now, paragraph, according to paragraph 21, the area, let us say area, area truth.

309
00:30:55,200 --> 00:31:02,200
Area truth means truth to be penetrated by areas.

310
00:31:02,200 --> 00:31:10,200
A real penetrable truth becomes real truth.

311
00:31:10,200 --> 00:31:13,200
That is paragraph 20.

312
00:31:13,200 --> 00:31:18,200
Paragraph, according to paragraph 21, the first part of paragraph 21,

313
00:31:18,200 --> 00:31:27,200
a real truth means the truth of the area, and the area means the Buddha, the truth of the Buddha.

314
00:31:27,200 --> 00:31:30,200
A real truth, a Buddha's truth.

315
00:31:30,200 --> 00:31:38,200
And the third one, middle of paragraph 21, beginning with all, or alternatively.

316
00:31:38,200 --> 00:31:51,200
So, according to that passage, area truth means truth that make people into areas.

317
00:31:51,200 --> 00:31:55,200
That means when you penetrate the truth, you become an area.

318
00:31:55,200 --> 00:32:00,200
So, a truth that makes people into areas.

319
00:32:00,200 --> 00:32:03,200
And the fourth one is paragraph 22.

320
00:32:03,200 --> 00:32:08,200
That means real truth, a real subject.

321
00:32:08,200 --> 00:32:11,200
A real subject means real truth.

322
00:32:11,200 --> 00:32:14,200
The truth with this real, which is not unreal and so on.

323
00:32:14,200 --> 00:32:21,200
So, the real subject is explained in four ways.

324
00:32:21,200 --> 00:32:28,200
This usually translated as noble truth, right?

325
00:32:28,200 --> 00:32:35,200
But noble truth means, here, according to paragraph 22,

326
00:32:35,200 --> 00:32:38,200
noble here means truth.

327
00:32:38,200 --> 00:32:44,200
The true truth, or real truth.

328
00:32:44,200 --> 00:32:51,200
And then, as the division by character, et cetera and so on.

329
00:32:51,200 --> 00:32:55,200
And then paragraph 24 has to meaning tracing out of the meaning.

330
00:32:55,200 --> 00:32:58,200
As to meaning firstly, what is the meaning of truth?

331
00:32:58,200 --> 00:33:02,200
Now, the word such a truth is here explained.

332
00:33:02,200 --> 00:33:08,200
And in this explanation, the last line of the page, with the eye of understanding,

333
00:33:08,200 --> 00:33:11,200
is not misleading like an illusion.

334
00:33:11,200 --> 00:33:13,200
And misleading really means untrue.

335
00:33:13,200 --> 00:33:16,200
It's not untrue like an illusion.

336
00:33:16,200 --> 00:33:21,200
The word illusion is the transmission of the Pali word maria,

337
00:33:21,200 --> 00:33:25,200
M-A-Y-D, which can mean magic.

338
00:33:25,200 --> 00:33:27,200
So, magic is untrue.

339
00:33:27,200 --> 00:33:32,200
So, when you go to, magic shows, they show you

340
00:33:32,200 --> 00:33:36,200
things that are untrue, but you think they are true.

341
00:33:36,200 --> 00:33:37,200
So, like magic.

342
00:33:37,200 --> 00:33:41,200
Deceptives like a mirror and, or undistcoverable,

343
00:33:41,200 --> 00:33:44,200
like the self of the sectarian and so on.

344
00:33:44,200 --> 00:33:47,200
And then, the paragraph 25,

345
00:33:47,200 --> 00:33:50,200
there is no pain but is affliction and not.

346
00:33:50,200 --> 00:33:52,200
That is not pain afflict.

347
00:33:52,200 --> 00:33:56,200
The meaning is there is no dukha, which does not afflict.

348
00:33:56,200 --> 00:34:00,200
And there is nothing other than dukha that afflicts.

349
00:34:00,200 --> 00:34:03,200
Something like that.

350
00:34:03,200 --> 00:34:07,200
And then, paragraph 26, tracing out the meaning.

351
00:34:07,200 --> 00:34:12,200
Here, the meaning of the word sacha.

352
00:34:12,200 --> 00:34:19,200
I mean, the meanings of the word sacha are given in this paragraph.

353
00:34:19,200 --> 00:34:21,200
So, sacha can mean what?

354
00:34:21,200 --> 00:34:26,200
The first one.

355
00:34:26,200 --> 00:34:28,200
Vable truth.

356
00:34:28,200 --> 00:34:29,200
So, verbal truth.

357
00:34:29,200 --> 00:34:34,200
So, saying something true can be, it's also called sacha.

358
00:34:34,200 --> 00:34:38,200
And then, the next one is abstinence from lying.

359
00:34:38,200 --> 00:34:40,200
It's also called sacha.

360
00:34:40,200 --> 00:34:44,200
And the third one is what?

361
00:34:44,200 --> 00:34:49,200
True as views. So, view is also called truth.

362
00:34:49,200 --> 00:34:53,200
And in such passages as truth is one, there is no sacha again.

363
00:34:53,200 --> 00:34:55,200
It is truth in the ultimate sense.

364
00:34:55,200 --> 00:34:57,200
Both nibana and the path.

365
00:34:57,200 --> 00:35:00,200
And then, in such passages as of the four truths,

366
00:35:00,200 --> 00:35:02,200
how many are profitable?

367
00:35:02,200 --> 00:35:03,200
It is noble truth.

368
00:35:03,200 --> 00:35:05,200
These are the four truths.

369
00:35:05,200 --> 00:35:08,200
And here, it is proper as noble truth.

370
00:35:08,200 --> 00:35:12,200
This is how the exposition should be understood as to tracing other meaning.

371
00:35:12,200 --> 00:35:22,200
So, sometimes, the commentary has the habit of showing us many meanings of the word.

372
00:35:22,200 --> 00:35:27,200
And then, at the end, it's among these meanings.

373
00:35:27,200 --> 00:35:29,200
And this is proper here.

374
00:35:29,200 --> 00:35:31,200
Of that is proper here.

375
00:35:31,200 --> 00:35:38,200
That means, given the meanings, the word can denote and then,

376
00:35:38,200 --> 00:35:43,200
choosing the one appropriate here.

377
00:35:43,200 --> 00:35:47,200
So, here also, such a can mean verbal truth.

378
00:35:47,200 --> 00:35:50,200
It can mean abstaining from lying and so on.

379
00:35:50,200 --> 00:35:55,200
But here, what is proper is just noble truth.

380
00:35:55,200 --> 00:36:02,200
And then, S-27, paragraph 27.

381
00:36:02,200 --> 00:36:09,200
S-27 is neither less nor more.

382
00:36:09,200 --> 00:36:14,200
No fewer than four and no more than four noble truths.

383
00:36:14,200 --> 00:36:19,200
And the first, quotation cannot be traced.

384
00:36:19,200 --> 00:36:23,200
But second, quotation is from Sayyutani Gaiak.

385
00:36:23,200 --> 00:36:25,200
And then, S to the order.

386
00:36:25,200 --> 00:36:28,200
And then, they are not difficult.

387
00:36:28,200 --> 00:36:31,200
And then, S to expounding birth and so on.

388
00:36:31,200 --> 00:36:40,200
Now, here, from here comes the detailed exposition of the four noble truths.

389
00:36:40,200 --> 00:36:44,200
Now, the noble truths are described by the Buddha as,

390
00:36:44,200 --> 00:36:48,200
see, the first one is noble truth of suffering, right?

391
00:36:48,200 --> 00:36:52,200
So, it is expounded as, but it's suffering,

392
00:36:52,200 --> 00:36:55,200
aging is suffering, death is suffering, sorrow,

393
00:36:55,200 --> 00:37:01,200
lamentation, pain, grief and despair are suffering, association with the unloved suffering,

394
00:37:01,200 --> 00:37:02,200
separation from the loved suffering.

395
00:37:02,200 --> 00:37:04,200
And not to get what one wants is suffering.

396
00:37:04,200 --> 00:37:09,200
In short, the five aggregates has objects of cleaning are suffering and so on.

397
00:37:09,200 --> 00:37:13,200
So, these explanations you find in the first summer,

398
00:37:13,200 --> 00:37:18,200
the setting and motion of the wheel of time are also.

399
00:37:18,200 --> 00:37:22,200
And then, he explains them in detail.

400
00:37:22,200 --> 00:37:28,200
So, we will not come to the end of this explanation tonight.

401
00:37:28,200 --> 00:37:53,200
I think, let's stop here.

