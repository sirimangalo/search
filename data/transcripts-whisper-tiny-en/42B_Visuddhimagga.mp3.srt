1
00:00:00,000 --> 00:00:11,000
So when he sees resin, so with the paragraph one hundred and so on, he explains that

2
00:00:11,000 --> 00:00:18,000
with the truth, even on origination and others.

3
00:00:18,000 --> 00:00:23,000
So when he sees resin fall in the two ways, according to condition and according

4
00:00:23,000 --> 00:00:28,000
to instant death, the truth of origination becomes evident to him.

5
00:00:28,000 --> 00:00:33,000
The truth of origination means the second noble truth.

6
00:00:33,000 --> 00:00:35,000
The origin of suffering.

7
00:00:35,000 --> 00:00:39,000
So the truth of origination becomes evident to him through seeing rice

8
00:00:39,000 --> 00:00:45,000
according to condition, going to his discovery of the pro-genica.

9
00:00:45,000 --> 00:00:54,000
So when he sees the rice and fall in two ways, according to condition and according to instant,

10
00:00:54,000 --> 00:01:01,000
then the second truth becomes evident to him.

11
00:01:01,000 --> 00:01:05,000
The truth of suffering becomes evident to him through seeing rice and fall

12
00:01:05,000 --> 00:01:06,000
according to instant.

13
00:01:06,000 --> 00:01:17,000
Now here, according to instant, going to his discovery of the suffering due to birth.

14
00:01:17,000 --> 00:01:22,000
Now, we should see suffering which is birth.

15
00:01:22,000 --> 00:01:27,000
Suffering and birth are same here, not due to birth.

16
00:01:27,000 --> 00:01:35,000
The truth of suggestion becomes evident to him through seeing fall according to condition

17
00:01:35,000 --> 00:01:40,000
or into his discovery of the non-arising of things produced by conditions

18
00:01:40,000 --> 00:01:42,000
when the accommodations do not arise.

19
00:01:42,000 --> 00:01:48,000
The truth of suffering becomes evident to him through seeing fall according to instant.

20
00:01:48,000 --> 00:01:57,000
Going to his discovery of the suffering which is death.

21
00:01:57,000 --> 00:02:03,000
And his seeing of rice and fall becomes evident to him at the truth of the fact that

22
00:02:03,000 --> 00:02:07,000
this is the mundane fact going to abolition of confusion about it.

23
00:02:07,000 --> 00:02:17,000
So, all the full noble truths become evident to him through seeing rice and fall

24
00:02:17,000 --> 00:02:26,000
of the aggregates in two ways, according to condition and according to instant.

25
00:02:26,000 --> 00:02:32,000
And then the dependent origin nation and forward order becomes evident to him

26
00:02:32,000 --> 00:02:37,000
through seeing rice according to condition, going to his discovery that

27
00:02:37,000 --> 00:02:40,000
when this exists, that comes to be.

28
00:02:40,000 --> 00:02:45,000
The dependent origin nation in a reverse order becomes evident to him through seeing fall,

29
00:02:45,000 --> 00:02:50,000
according to condition or into his discovery that when this does not exist,

30
00:02:50,000 --> 00:02:52,000
that does not come to be.

31
00:02:52,000 --> 00:02:57,000
Dependently, recent states become evident to him through seeing rice and fall,

32
00:02:57,000 --> 00:02:59,000
according to instant.

33
00:02:59,000 --> 00:03:03,000
So, it is the discovery of the characteristic of the form.

34
00:03:03,000 --> 00:03:08,000
For the things who zest of rice and fall are formed in conditionally arisen.

35
00:03:08,000 --> 00:03:19,000
So, seeing five aggregates, seeing the rice and fall of five aggregates in these ways

36
00:03:19,000 --> 00:03:26,000
may have understand more clearly the dependent origin nation in forward order in reverse order

37
00:03:26,000 --> 00:03:32,000
and also the states mentioned in the dependent origin nation,

38
00:03:32,000 --> 00:03:35,000
which are called dependent reason states.

39
00:03:35,000 --> 00:03:43,000
That means actually all that are mentioned in the dependent origin nation.

40
00:03:43,000 --> 00:03:48,000
Everything mentioned in the dependent origin nation arise from some other conditions.

41
00:03:48,000 --> 00:03:53,000
So, they are all dependent on the reason states.

42
00:03:53,000 --> 00:03:59,000
Now, the method of identity becomes evident to him through seeing rice,

43
00:03:59,000 --> 00:04:02,000
according to condition and so on.

44
00:04:02,000 --> 00:04:10,000
Do you remember the full method with which to understand the dependent origin nation?

45
00:04:10,000 --> 00:04:29,000
You go back to the chapter 17, chapter 17, paragraph 3, 0, 9.

46
00:04:29,000 --> 00:04:32,000
Then there are four methods of feeding in meaning here.

47
00:04:32,000 --> 00:04:38,000
They are method of identity, the method of diversity, the method of uninteresting

48
00:04:38,000 --> 00:04:41,000
method, and the method of inelectable regularity.

49
00:04:41,000 --> 00:04:49,000
So, these are the four methods by which we should understand dependent origin nation

50
00:04:49,000 --> 00:04:52,000
or understand things.

51
00:04:52,000 --> 00:05:03,000
So, these methods are also become evident to him who sees rice and fall of aggregates in two ways.

52
00:05:03,000 --> 00:05:09,000
So, the method of identity becomes evident to him through seeing rice,

53
00:05:09,000 --> 00:05:16,000
according to condition, going to his discovery of unbroken continuity in the connection of caution fruit.

54
00:05:16,000 --> 00:05:25,000
So, there is a connection of cost and fruit or cost and effect.

55
00:05:25,000 --> 00:05:38,000
So, when one sees the rising, according to condition, that means because there is this collision and there is the fruit.

56
00:05:38,000 --> 00:05:44,000
Because this is a collision, there is a fruit of it and so on.

57
00:05:44,000 --> 00:05:50,000
So, when he sees this, he understands it by way of the method of identity.

58
00:05:50,000 --> 00:05:59,000
That means there is a continuation of cost and effects.

59
00:05:59,000 --> 00:06:02,000
And then he more thoroughly abandoned the annihilation view.

60
00:06:02,000 --> 00:06:07,000
When he sees this, he abandoned the annihilation view.

61
00:06:07,000 --> 00:06:11,000
That means he abandoned the view that he being it annihilated as death.

62
00:06:11,000 --> 00:06:18,000
No more nothing arises after death.

63
00:06:18,000 --> 00:06:26,000
So, he abandons this view when he sees a rising according to condition.

64
00:06:26,000 --> 00:06:33,000
Because there is the condition and that which arises depending on that condition.

65
00:06:33,000 --> 00:06:38,000
So, this condition and fruit connection goes on and on and on.

66
00:06:38,000 --> 00:06:44,000
So, there is some kind of identity in this continuity.

67
00:06:44,000 --> 00:06:52,000
The method of, the method of diversity becomes evident to him through seeing rise according to instant.

68
00:06:52,000 --> 00:06:54,000
At every instant, there is a rising.

69
00:06:54,000 --> 00:07:01,000
So, only to his discovery that each state is new as it arises.

70
00:07:01,000 --> 00:07:13,000
Although there is some kind of identity, I mean some kind of continuity, everyone is a new one.

71
00:07:13,000 --> 00:07:29,000
So, since everything is new at every moment and there is no common entity in the states.

72
00:07:29,000 --> 00:07:46,000
So, when he sees a rising according to instant, then he can abandon the eternity view that things are permanent.

73
00:07:46,000 --> 00:07:51,000
Because at every moment there is a new thing arising.

74
00:07:51,000 --> 00:08:00,000
And so, and all things disappearing, so there can be no permanency in these states, so he sees that.

75
00:08:00,000 --> 00:08:08,000
The method of uninterestedness becomes evident to him through seeing rise and fall according to condition.

76
00:08:08,000 --> 00:08:14,000
Only to his discovery of the inability of states to have mastery exercised over them.

77
00:08:14,000 --> 00:08:18,000
Then he more thoroughly abandons the self view.

78
00:08:18,000 --> 00:08:28,000
If it is to be called self, then it must be able to exercise its authority over it.

79
00:08:28,000 --> 00:08:38,000
But now, we just rise and fall, the states just rise and fall.

80
00:08:38,000 --> 00:08:49,000
And nobody can exercise authority over them to be permanent or whatever.

81
00:08:49,000 --> 00:08:59,000
So, when he sees rising and falling according to condition, then he is able to abandon the self view.

82
00:08:59,000 --> 00:09:02,000
And the view that there is a permanent self.

83
00:09:02,000 --> 00:09:10,000
The method of intellectual regularity becomes evident to him through seeing rise according to condition.

84
00:09:10,000 --> 00:09:15,000
Point to his discovery of the arising of his fruit from the suitable conditions are there.

85
00:09:15,000 --> 00:09:21,000
Then he more thoroughly abandons the moral efficacy of action view.

86
00:09:21,000 --> 00:09:30,000
That means whatever you do, does not amount to come up something like that.

87
00:09:30,000 --> 00:09:44,000
So, he is able to abandon that view because he sees the arising of aggregates or rising of states according to conditions.

88
00:09:44,000 --> 00:09:48,000
Because of this condition there is this fruit.

89
00:09:48,000 --> 00:09:54,000
And this fruit is from this condition only and not from any other condition.

90
00:09:54,000 --> 00:10:06,000
And so, he is able to abandon the wrong view that even though you do something,

91
00:10:06,000 --> 00:10:11,000
you do not know what you are doing, anything, any come up or whatever.

92
00:10:11,000 --> 00:10:16,000
Now, the characteristic of not self becomes evident to him through seeing rise according to condition.

93
00:10:16,000 --> 00:10:29,000
And according to his discovery, states have no curiosity or we have finished the full matters in which we have to be given an origin nation.

94
00:10:29,000 --> 00:10:31,000
Now, characteristics.

95
00:10:31,000 --> 00:10:39,000
So, the states of no curiosity really means no effort of their own.

96
00:10:39,000 --> 00:10:44,000
And that there exist in depends upon conditions.

97
00:10:44,000 --> 00:10:54,000
Now, in this paragraph, there are two things which we should note and that is characteristic of individual essence.

98
00:10:54,000 --> 00:10:57,000
It is down the paragraph.

99
00:10:57,000 --> 00:11:01,000
And the characteristic of what is formed.

100
00:11:01,000 --> 00:11:03,000
I want you to understand these two.

101
00:11:03,000 --> 00:11:09,000
Characteristic of individual essence and characteristic of what is formed.

102
00:11:09,000 --> 00:11:24,000
Characteristic of individual essence means characteristic of the characteristics which they do not share with any other state.

103
00:11:24,000 --> 00:11:28,000
Like, say contact.

104
00:11:28,000 --> 00:11:34,000
Contact has the characteristic of impinging on the object.

105
00:11:34,000 --> 00:11:41,000
And that characteristic is of contact only and not of feeling, not of perception and so on.

106
00:11:41,000 --> 00:11:47,000
So, we are called individual characteristics of individual essence.

107
00:11:47,000 --> 00:11:56,000
And the characteristic of what is formed means the impermanence, suffering and no, no, no, no self nature.

108
00:11:56,000 --> 00:12:00,000
We are called characteristic of what is formed.

109
00:12:00,000 --> 00:12:05,000
They are actually common characteristic of conditions phenomena.

110
00:12:05,000 --> 00:12:10,000
So, everything that is conditioned has these three characteristics.

111
00:12:10,000 --> 00:12:15,000
The arising, presence and the solution.

112
00:12:15,000 --> 00:12:27,000
And the only comes to see both characteristic of individual essence and the characteristic of what is formed when he practices a three-person annotation.

113
00:12:27,000 --> 00:12:35,000
So, these two become evident to him.

114
00:12:35,000 --> 00:12:42,000
Going to his discovery of the non-existence of fall at the instance of rise and the non-existence of rise at the instance of fall.

115
00:12:42,000 --> 00:12:46,000
When there is rising, there is no falling, when there is no falling, there is no rising.

116
00:12:46,000 --> 00:12:55,000
So, when he discovers this, then the characteristic of what is formed and characteristic of individual essence becomes clear to him.

117
00:12:55,000 --> 00:13:07,000
Now, here only rise and fall are mentioned and not the intermediate states, right?

118
00:13:07,000 --> 00:13:17,000
So, the footnote says that it will, the inclusion of only rise and fall here is because this kind of knowledge occurs as seeing only rise and fall.

119
00:13:17,000 --> 00:13:23,000
Not because of non-existence of the instance of presence.

120
00:13:23,000 --> 00:13:30,000
Because in Vipasana, the yogis see only rising and falling.

121
00:13:30,000 --> 00:13:33,000
That is why rising and falling are mentioned here.

122
00:13:33,000 --> 00:13:37,000
Not because the intermediate states is non-existent.

123
00:13:37,000 --> 00:13:42,000
There is intermediate states with this called presence.

124
00:13:42,000 --> 00:13:56,000
But in Vipasana, Malaysia, and only rising and disappearing are seen by yogis, that is why these two are mentioned.

125
00:13:56,000 --> 00:14:09,000
When the different truth has placed in dependent origin, as in methods and characteristics have become evident to him thus, then formations appear to him as potentially renewed.

126
00:14:09,000 --> 00:14:18,000
I want to say always new, instead of saying, perpetually renewed, because they are not renewed actually.

127
00:14:18,000 --> 00:14:24,000
At every moment there is a new new phenomenon existing.

128
00:14:24,000 --> 00:14:29,000
Not something is renewed.

129
00:14:29,000 --> 00:14:39,000
Then how do you think they stay the same?

130
00:14:39,000 --> 00:14:42,000
Then how do you think they stay the same?

131
00:14:42,000 --> 00:14:46,000
How do they know that how does the glass know to keep staying glass if it is continued?

132
00:14:46,000 --> 00:14:47,000
Yeah.

133
00:14:47,000 --> 00:15:01,000
In the place of the old one, which has disappeared, a new one takes its place, of the same nature.

134
00:15:01,000 --> 00:15:05,000
Yeah, because we talked about that they have the nature.

135
00:15:05,000 --> 00:15:19,000
Yeah, of the same nature, of the same kind, but let us say something like, no, not one glass, say many glasses.

136
00:15:19,000 --> 00:15:24,000
So when one glass disappears, then we put another glass there.

137
00:15:24,000 --> 00:15:37,000
It is, it is, we can say the same in sense that the one disappear is a glass and the one which is now here is also a glass.

138
00:15:37,000 --> 00:15:39,000
But they are different.

139
00:15:39,000 --> 00:15:42,000
The one is removed and the other is called in this place.

140
00:15:42,000 --> 00:15:48,000
So in the same way, one material probably disappears and another takes its place.

141
00:15:48,000 --> 00:15:53,000
Another of the same kind, or similar kind.

142
00:15:53,000 --> 00:16:06,000
So these states, it seems, being previously an arisen arise and being arisen they see.

143
00:16:06,000 --> 00:16:12,000
And they are not only perpetually renewed or they are not only always new, but they are also shot lines.

144
00:16:12,000 --> 00:16:22,000
Like two drops at sunrise, like a bubble on water, like a line drawn on water, like a mustard seat on an false point, like a lightning flesh.

145
00:16:22,000 --> 00:16:31,000
And they appear without coke, like a contouring tree, like a mirage, like a dream, like a circle of a falling firebrand.

146
00:16:31,000 --> 00:16:38,000
And this, similarly, is not mentioned in the courses, so we cannot face that.

147
00:16:38,000 --> 00:16:44,000
Like a goblin city, like fraud, like a planting front and so on.

148
00:16:44,000 --> 00:16:49,000
No, you, goblin city, you do goblin city.

149
00:16:49,000 --> 00:17:01,000
It is like, like a city created by goblins or ghosts or some spirits.

150
00:17:01,000 --> 00:17:07,000
So sometimes you, even yourself in a house, in a big building.

151
00:17:07,000 --> 00:17:18,000
And then next morning, you find yourself lying on the ground, on the ground and nothing, nothing of the house you experience during the night could be seen.

152
00:17:18,000 --> 00:17:24,000
So that kind of experience happened to many people.

153
00:17:24,000 --> 00:17:28,000
I don't know if they have been here, too.

154
00:17:28,000 --> 00:17:31,000
So they are called goblin city.

155
00:17:31,000 --> 00:17:37,000
That means just in cities or whatever.

156
00:17:37,000 --> 00:17:44,000
Made to appear as real to human beings.

157
00:17:44,000 --> 00:17:50,000
Maybe in the movies, to you, you find something like this.

158
00:17:50,000 --> 00:17:56,000
It was in the house and he enjoys the food or whatever in the house.

159
00:17:56,000 --> 00:18:04,000
And in the morning, he finds himself lying on the ground, something like that.

160
00:18:04,000 --> 00:18:08,000
That is called goblin city.

161
00:18:08,000 --> 00:18:14,000
Then, at this point, he has a thing, tender inside knowledge.

162
00:18:14,000 --> 00:18:17,000
Now, it is still tender.

163
00:18:17,000 --> 00:18:23,000
Called contemplation of rise and fall, which has become established by my family filling the 50 characteristics in this manner.

164
00:18:23,000 --> 00:18:30,000
Only what is subject to fall arises and to be a risen necessity takes fall.

165
00:18:30,000 --> 00:18:34,000
I don't think that is acceptable.

166
00:18:34,000 --> 00:18:37,000
Only what is subject to fall arises.

167
00:18:37,000 --> 00:18:44,000
And what is a risen naturally fall, something like that?

168
00:18:44,000 --> 00:18:48,000
Well, the attainment of this, he is known as a beginner of insight.

169
00:18:48,000 --> 00:18:54,000
Only when you are changed through this stage, you call a beginner insight.

170
00:18:54,000 --> 00:18:58,000
Before that, you are not a beginner.

171
00:18:58,000 --> 00:19:04,000
You are not an official beginner insight.

172
00:19:04,000 --> 00:19:11,000
Now, imperfections of insight are impediment of insight.

173
00:19:11,000 --> 00:19:20,000
So when a yogi reaches this stage, then the ten imperfections of impurities of insight arise in him.

174
00:19:20,000 --> 00:19:32,000
So these are the obstacles to progress, obstacle to attainment, obstacles to enlightenment.

175
00:19:32,000 --> 00:19:40,000
Because if a yogi takes them to be enlightenment, then he would not practice any father.

176
00:19:40,000 --> 00:19:42,000
And so he will not get to enlightenment.

177
00:19:42,000 --> 00:19:52,000
So these are called imperfections of impurities of insight.

178
00:19:52,000 --> 00:19:56,000
And there are ten of them.

179
00:19:56,000 --> 00:20:03,000
And they are given as one elimination to knowledge, three ruptures, happiness that means

180
00:20:03,000 --> 00:20:11,000
PD, four tranquility, five bliss pleasures, Sukha, six resolution, seven exertions, eight assurance,

181
00:20:11,000 --> 00:20:16,000
I would say, establishment, and nine equanimity and ten attachment.

182
00:20:16,000 --> 00:20:25,000
So these ten things happen arise to a person who has reached this stage,

183
00:20:25,000 --> 00:20:35,000
this stage of contemplating of price and fall.

184
00:20:35,000 --> 00:20:43,000
And these do not arise either in a noble disciple who has reached penetration of the truth.

185
00:20:43,000 --> 00:20:47,000
That means, who has gained enlightenment?

186
00:20:47,000 --> 00:20:54,000
Or in persons, airing in virtue, virtue, neglectful of their meditation, subject, and idolats.

187
00:20:54,000 --> 00:21:05,000
So they arise only to those who are practicing meditation,

188
00:21:05,000 --> 00:21:11,000
who gives to the right cause, devote themselves continuously to his meditation subject,

189
00:21:11,000 --> 00:21:15,000
and is a beginner of insight.

190
00:21:15,000 --> 00:21:24,000
So these ten will not arise in noble ones, and also in those who do not practice meditation.

191
00:21:24,000 --> 00:21:32,000
And they are there at ten.

192
00:21:32,000 --> 00:21:37,000
And then these ten are explained in one by one, right?

193
00:21:37,000 --> 00:21:41,000
So let's go to illumination.

194
00:21:41,000 --> 00:21:46,000
Now, illumination is illumination due to insight.

195
00:21:46,000 --> 00:21:51,000
When it arises, they meditate out things such illumination never arose any before.

196
00:21:51,000 --> 00:21:53,000
I have surely reached the path, based fruition,

197
00:21:53,000 --> 00:21:58,000
thus it takes what is not a path to be the path, and what is not fruition to be future.

198
00:21:58,000 --> 00:22:02,000
When it takes what is not a path to be a path, and what is not prudent to be future,

199
00:22:02,000 --> 00:22:07,000
the cause of this insight is interrupted, because he will not like this anymore.

200
00:22:07,000 --> 00:22:12,000
He drops his own basic meditation subject, and sits just enjoying the illumination.

201
00:22:12,000 --> 00:22:18,000
Now, this illumination arises in one vehicle illuminating only as much as the city is sitting on.

202
00:22:18,000 --> 00:22:22,000
In another, the interior, obviously, in another, the exterior of his room,

203
00:22:22,000 --> 00:22:29,000
in another, the whole monastery, or a quarter leaf, a half leaf, a leaf, two leaves, three leaves.

204
00:22:29,000 --> 00:22:34,000
In another vehicle, it arises, making a single light from that surface up to the ground of water.

205
00:22:34,000 --> 00:22:40,000
But in the Blessed One, it arose, eliminating the 10,000 false world elements.

206
00:22:40,000 --> 00:22:42,000
And there is a story.

207
00:22:42,000 --> 00:22:45,000
Now, please be the footnote.

208
00:22:45,000 --> 00:22:54,000
Illumination due to insight is a luminous material within, originated by insight consciousness.

209
00:22:54,000 --> 00:23:01,000
And that originated by temperature, belonging to his own continuity that meets in his body.

210
00:23:01,000 --> 00:23:11,000
Of this, that originated by insight consciousness is bright, and is found only in the metadata's body.

211
00:23:11,000 --> 00:23:16,000
The other kind is independent of his body.

212
00:23:16,000 --> 00:23:18,000
I don't want to say that.

213
00:23:18,000 --> 00:23:30,000
The other kind leaves his body, and spreads, all around, according to the power of his knowledge.

214
00:23:30,000 --> 00:23:34,000
And it is manifest to him only, not true.

215
00:23:34,000 --> 00:23:36,000
It becomes manifest.

216
00:23:36,000 --> 00:23:40,000
I would say, it is manifest to him only, and he sees anything material in the place touched by it.

217
00:23:40,000 --> 00:23:51,000
So, the Illumination of this material is not only the material in the place touched by it.

218
00:23:51,000 --> 00:24:01,000
So, the Illumination is manifest to him only, and he sees anything material in the place touched by it.

219
00:24:01,000 --> 00:24:05,000
So, the Illumination is manifest only to him.

220
00:24:05,000 --> 00:24:09,000
Others won't see the Illumination.

221
00:24:09,000 --> 00:24:14,000
And he sees anything in material in the place touched by it.

222
00:24:14,000 --> 00:24:19,000
But he would see things illuminated by that bright light.

223
00:24:19,000 --> 00:24:26,000
And then the sub commentary raises a question.

224
00:24:26,000 --> 00:24:34,000
Does he see with the eye consciousness from mind consciousness?

225
00:24:34,000 --> 00:24:41,000
And then he said, it should be a mind consciousness, seeing with mind consciousness.

226
00:24:41,000 --> 00:24:46,000
That means he does not see with his eyes, with his physical eyes.

227
00:24:46,000 --> 00:24:56,000
But with his mind, he sees things touched by or eliminated by that bright light.

228
00:24:56,000 --> 00:25:03,000
And then the story of two elders.

229
00:25:03,000 --> 00:25:05,000
And next is knowledge.

230
00:25:05,000 --> 00:25:08,000
Knowledge is knowledge due to insight.

231
00:25:08,000 --> 00:25:12,000
As he is estimating and judging material in material states,

232
00:25:12,000 --> 00:25:17,000
knowledge that is unerring, unerring means undeterred.

233
00:25:17,000 --> 00:25:23,000
King, incisive and very sharp arises in him, like a lightning flesh.

234
00:25:23,000 --> 00:25:27,000
And then beauty, rapturous happiness, is happiness due to insight.

235
00:25:27,000 --> 00:25:32,000
But at that time, the five kinds of happiness, namely minor happiness,

236
00:25:32,000 --> 00:25:35,000
momentary happiness, showering happiness, uplifting happiness,

237
00:25:35,000 --> 00:25:40,000
and pervading rapturous happiness arise in him feeling his whole body.

238
00:25:40,000 --> 00:25:47,000
You remember the five mentioned earlier in the chapters,

239
00:25:47,000 --> 00:25:53,000
chapter four, paragraph 94 and so on.

240
00:25:53,000 --> 00:26:01,000
And tranquility.

241
00:26:01,000 --> 00:26:05,000
And please resolution, exertion, and then assurance.

242
00:26:05,000 --> 00:26:08,000
Assurance is literally established.

243
00:26:08,000 --> 00:26:10,000
Assurance really means mindfulness.

244
00:26:10,000 --> 00:26:18,000
His mindfulness is very strong at this point.

245
00:26:18,000 --> 00:26:23,000
And then equanimity is about insight and equanimity in adverbing.

246
00:26:23,000 --> 00:26:29,000
For equanimity about insight, which is neutrality about formations is strongly in him at the time.

247
00:26:29,000 --> 00:26:34,000
So does equanimity in adverbing in the mind, or not it is also.

248
00:26:34,000 --> 00:26:39,000
So does equanimity in adverbing in the mind, or not.

249
00:26:39,000 --> 00:26:46,000
That means equanimity in adverbing in the mind, or also arises strongly in him.

250
00:26:46,000 --> 00:26:47,000
And then attachment.

251
00:26:47,000 --> 00:26:55,000
That is real attachment.

252
00:26:55,000 --> 00:27:02,000
Now, paragraph 124.

253
00:27:02,000 --> 00:27:07,000
Here, illumination, etc. are called imperfections because they are the basis for imperfections.

254
00:27:07,000 --> 00:27:10,000
Not because they are chemically unprofitable.

255
00:27:10,000 --> 00:27:12,000
Not because they are aquasalash.

256
00:27:12,000 --> 00:27:16,000
But attachment is both an imperfection and the basis for imperfection.

257
00:27:16,000 --> 00:27:21,000
So the last one is real aquasalash because it is lower attachment.

258
00:27:21,000 --> 00:27:25,000
The other nine are not aquasalash.

259
00:27:25,000 --> 00:27:27,000
But they are ground for aquasalash.

260
00:27:27,000 --> 00:27:29,000
They are conditions for aquasalash.

261
00:27:29,000 --> 00:27:36,000
Because when the yogi has an illumination that you will be attached to is a combination.

262
00:27:36,000 --> 00:27:38,000
And so on.

263
00:27:38,000 --> 00:27:44,000
And as this is the amount you tend, but with the different ways of taking them, they come to study.

264
00:27:44,000 --> 00:27:54,000
So by false view, by concepts,

265
00:27:54,000 --> 00:28:03,000
and then by attachments, by will of attachment, they become thirty.

266
00:28:03,000 --> 00:28:08,000
So each one can be taken by will of false view.

267
00:28:08,000 --> 00:28:13,000
By will of concept and by will of attachment.

268
00:28:13,000 --> 00:28:17,000
And then, it's useful many data.

269
00:28:17,000 --> 00:28:21,000
It's not not deceived by these.

270
00:28:21,000 --> 00:28:24,000
So he sees that they are impermanent and so on.

271
00:28:24,000 --> 00:28:26,000
And they are not the right.

272
00:28:26,000 --> 00:28:36,000
They are not the right path to attachment, to enlightenment.

273
00:28:36,000 --> 00:28:42,000
So he decides here that the illumination and so on are not the right path.

274
00:28:42,000 --> 00:28:48,000
But the practice of whippers are only is the right path.

275
00:28:48,000 --> 00:28:55,000
He decides this, and when he decides this and this knowledge is establishing in him,

276
00:28:55,000 --> 00:29:01,000
then he says you have reached the knowledge and vision of what is the path and what is not the path.

277
00:29:01,000 --> 00:29:12,000
So he, it wasn't a yogi after reaching the contemplation of reason for will encounter these impediments.

278
00:29:12,000 --> 00:29:20,000
And when he encounter these impediments, he must be able to see them as impediments

279
00:29:20,000 --> 00:29:24,000
and not as the signs of enlightenment and so on.

280
00:29:24,000 --> 00:29:30,000
If he takes them to be signs of enlightenment, then he will stop there and he will not go any further.

281
00:29:30,000 --> 00:29:41,000
And so he will be deprived of progress in insight and enlightenment.

282
00:29:41,000 --> 00:29:48,000
And then at this point, the three truths are defined and so on.

283
00:29:48,000 --> 00:30:07,000
So this is the end of the chapter 20 and the whippers and knowledge called contemplation of...

284
00:30:07,000 --> 00:30:11,000
It is not the end of contemplation of rise and form.

285
00:30:11,000 --> 00:30:15,000
We love to go further with the contemplation of rise and form.

286
00:30:15,000 --> 00:30:18,000
But one purity ends here.

287
00:30:18,000 --> 00:30:23,000
That is the purification by knowledge and vision of what is the path and what is not the path.

288
00:30:23,000 --> 00:30:30,000
Because the yogi is practicing contemplation on rise and form.

289
00:30:30,000 --> 00:30:38,000
When he reaches to a sudden level of the contemplation of rise and form, this impediment came.

290
00:30:38,000 --> 00:30:49,000
When he is disturbed by these impediments, he could not see rise and form really very clearly.

291
00:30:49,000 --> 00:30:55,000
So he has to practice again to see rise and form clearly.

292
00:30:55,000 --> 00:31:01,000
So the contemplation of rise and form does not end here.

293
00:31:01,000 --> 00:31:04,000
It will go over to the next chapter.

294
00:31:04,000 --> 00:31:11,000
But the purification of the knowledge and vision of what is the path and what is not the path ends here.

295
00:31:11,000 --> 00:31:18,000
So there is overlapping of purity and whippers and knowledge.

296
00:31:18,000 --> 00:31:27,000
So if you look at this hand out, you will see it clearly.

297
00:31:27,000 --> 00:31:56,000
Could you explain some way that we can work with seeing rise and form as it, like reaching reaching and then falling and picking up rising or trying to be looking at the consciousness?

298
00:31:56,000 --> 00:32:04,000
At first, you do not try to see the rising and form.

299
00:32:04,000 --> 00:32:10,000
We try to get questions to reach or put concentration.

300
00:32:10,000 --> 00:32:17,000
So we try to be mindful of what is happening at the present moment.

301
00:32:17,000 --> 00:32:29,000
And then later on, the arising and disappearing of things you observe will become evident for you.

302
00:32:29,000 --> 00:32:37,000
So what is important is that you get necessary amount of concentration.

303
00:32:37,000 --> 00:32:46,000
Unless your concentration gets better, then you will come to see the rising and form.

304
00:32:46,000 --> 00:32:49,000
First, we have to see the thing clearly, right?

305
00:32:49,000 --> 00:32:52,000
And then after that we say, this is arising, this is falling.

306
00:32:52,000 --> 00:32:55,000
Or it is not arising, it is not falling.

307
00:32:55,000 --> 00:33:20,000
But before we see the arising and forming, we have to see the thing itself.

308
00:33:20,000 --> 00:33:29,000
Is it possible to get the tapes from this class, copies of the tapes from this class to study as we go along?

309
00:33:29,000 --> 00:33:31,000
Yeah.

310
00:33:31,000 --> 00:33:34,000
How can we go?

311
00:33:34,000 --> 00:33:38,000
Could you explain copies of the ones that we need for just this class?

312
00:33:38,000 --> 00:33:39,000
Do you agree?

313
00:33:39,000 --> 00:33:43,000
From, I know, the last three classes we just started three times ago.

314
00:33:43,000 --> 00:33:46,000
Thank you.

315
00:33:46,000 --> 00:33:53,000
This is for the center.

316
00:33:53,000 --> 00:33:58,000
This is going to take this.

317
00:33:58,000 --> 00:34:05,000
The other thing who is absent today, she always takes the same time.

318
00:34:05,000 --> 00:34:07,000
Oh, you make one?

319
00:34:07,000 --> 00:34:17,000
If this is for the center.

