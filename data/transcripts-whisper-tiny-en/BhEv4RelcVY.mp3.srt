1
00:00:00,000 --> 00:00:02,000
Let's go. I trust you.

2
00:00:02,000 --> 00:00:06,000
Okay, want to help you are well. My biggest fear is to lose my mother.

3
00:00:06,000 --> 00:00:08,000
Lose my mother to death.

4
00:00:08,000 --> 00:00:10,000
I know we have to accept it.

5
00:00:10,000 --> 00:00:14,000
She is not mine, but the fear is still haunting me even in my dreams.

6
00:00:14,000 --> 00:00:30,000
Please help me understand with your words. Thank you so much.

7
00:00:30,000 --> 00:00:34,000
Well, to love your mother is a good thing.

8
00:00:34,000 --> 00:00:36,000
There's good there.

9
00:00:36,000 --> 00:00:39,000
I think you kind of have to separate that out.

10
00:00:39,000 --> 00:00:44,000
And this is what people who have strong sense of family aren't able to do.

11
00:00:44,000 --> 00:00:55,000
And so no matter how they claim that they understand concepts of non-self and detachment,

12
00:00:55,000 --> 00:01:04,000
they really do feel that it's good to be attached to their parents deep down.

13
00:01:04,000 --> 00:01:11,000
It's very much a part of their idea of themselves to be afraid of losing your mother.

14
00:01:11,000 --> 00:01:13,000
So you're not afraid to lose your mother.

15
00:01:13,000 --> 00:01:15,000
Maybe it's a good way to approach this.

16
00:01:15,000 --> 00:01:17,000
You're not afraid of losing your mother.

17
00:01:17,000 --> 00:01:22,000
This fear arises from time to time,

18
00:01:22,000 --> 00:01:29,000
probably with some frequency.

19
00:01:29,000 --> 00:01:32,000
Fear doesn't haunt you, fear arises.

20
00:01:32,000 --> 00:01:37,000
Thinking of it in these terms will help you to align your mind in order to deal with it.

21
00:01:37,000 --> 00:01:40,000
It will also help you to see clearly what's going on.

22
00:01:40,000 --> 00:01:44,000
It's not just the fear. There's also a love of your mother.

23
00:01:44,000 --> 00:01:46,000
Gratitude towards your mother.

24
00:01:46,000 --> 00:01:49,000
Attachment to your mother.

25
00:01:49,000 --> 00:01:52,000
All of those things are also present.

26
00:01:52,000 --> 00:01:55,000
And those things probably fuel the fear.

27
00:01:55,000 --> 00:01:58,000
Well, most definitely fuel the fear.

28
00:01:58,000 --> 00:02:07,000
And so all of those are the constituents.

29
00:02:07,000 --> 00:02:11,000
And they're all being held together by this view of self.

30
00:02:11,000 --> 00:02:13,000
You see how these defilements work.

31
00:02:13,000 --> 00:02:14,000
They play different roles.

32
00:02:14,000 --> 00:02:16,000
The view of self holds it together.

33
00:02:16,000 --> 00:02:20,000
So you say, this is me afraid to lose my mother.

34
00:02:20,000 --> 00:02:24,000
But actually, once you let go of that and you say, okay, fear arose,

35
00:02:24,000 --> 00:02:26,000
you see, oh, that's only this part.

36
00:02:26,000 --> 00:02:29,000
There's also the attachment. There's also the love.

37
00:02:29,000 --> 00:02:31,000
And you're able to separate them out.

38
00:02:31,000 --> 00:02:36,000
And in the end, you are able to free yourself

39
00:02:36,000 --> 00:02:38,000
in the bad ones because you see they're the real problem.

40
00:02:38,000 --> 00:02:41,000
You see clearly, that's not helping me.

41
00:02:41,000 --> 00:02:43,000
Okay, that I know is bad.

42
00:02:43,000 --> 00:02:45,000
And so you remove that from the equation.

43
00:02:45,000 --> 00:02:48,000
And when we're left with is only the good stuff.

44
00:02:48,000 --> 00:02:53,000
So there's love, there's gratitude, there's compassion,

45
00:02:53,000 --> 00:02:58,000
and joy and equanimity.

46
00:02:58,000 --> 00:02:59,000
There's wisdom.

47
00:02:59,000 --> 00:03:00,000
There's insight.

48
00:03:00,000 --> 00:03:04,000
And your relationship with your mother becomes wholesome.

49
00:03:04,000 --> 00:03:11,000
And there's no sense of fear or sadness in regards to loss of your mother.

50
00:03:11,000 --> 00:03:14,000
Okay, so remove the idea of self.

51
00:03:14,000 --> 00:03:16,000
Get just the idea of this as a risen.

52
00:03:16,000 --> 00:03:18,000
And you'll be able to break it up into its part

53
00:03:18,000 --> 00:03:21,000
to see the many parts and as a result of the other.

54
00:03:21,000 --> 00:03:25,000
That's insight meditation in a nutshell.

