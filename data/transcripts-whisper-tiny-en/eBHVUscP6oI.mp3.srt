1
00:00:00,000 --> 00:00:17,080
Good evening, everyone, welcome to our live podcast.

2
00:00:17,080 --> 00:00:24,080
I'm leaving Harios all next time.

3
00:00:47,080 --> 00:01:02,080
Hey, Robin, you're not here yet.

4
00:01:02,080 --> 00:01:09,080
I got caught in this storm. Lots of trees down on the road.

5
00:01:09,080 --> 00:01:14,080
Are you coming alone or is your husband with you?

6
00:01:14,080 --> 00:01:24,080
I'm alone this time, I'm working this week.

7
00:01:24,080 --> 00:01:43,080
Okay.

8
00:01:43,080 --> 00:01:50,080
And that should be there.

9
00:01:50,080 --> 00:01:53,080
Using the wrong microphone.

10
00:01:53,080 --> 00:01:56,080
Okay.

11
00:01:56,080 --> 00:02:05,080
So, the latest quote is about

12
00:02:05,080 --> 00:02:10,080
eating.

13
00:02:10,080 --> 00:02:14,080
Now it's about livelihood.

14
00:02:14,080 --> 00:02:22,080
You read at the end, it's about how one makes one's living.

15
00:02:22,080 --> 00:02:30,080
Specifically, how religious people make their living with religious people focus on.

16
00:02:30,080 --> 00:02:48,080
There are religious practices that are low religious practices that are not worthy of one who is

17
00:02:48,080 --> 00:02:53,080
one who is seeking to become free from suffering.

18
00:02:53,080 --> 00:03:03,080
They're not worthy of a Buddhist monk at any rate.

19
00:03:03,080 --> 00:03:10,080
So, there's looking down, and there's looking up, looking down, is looking at the signs,

20
00:03:10,080 --> 00:03:24,080
that mundane signs, looking up, is looking at divine signs, also not good.

21
00:03:24,080 --> 00:03:33,080
There are those who act as messengers and errand boys.

22
00:03:33,080 --> 00:03:42,080
This is one thing that monks aren't allowed to do or not allowed to run errands for people.

23
00:03:42,080 --> 00:03:48,080
Because there's, you know, we are living up the charity of others.

24
00:03:48,080 --> 00:04:02,080
We have given up financial security and song,

25
00:04:02,080 --> 00:04:09,080
solubility, and that's the word.

26
00:04:09,080 --> 00:04:13,080
In favor of the homeless life.

27
00:04:13,080 --> 00:04:19,080
And so, we rely upon charity.

28
00:04:19,080 --> 00:04:26,080
Now, if we start working for people,

29
00:04:26,080 --> 00:04:35,080
first of all, it's going against our determination to be free from all that.

30
00:04:35,080 --> 00:04:44,080
But it's also setting one up for the obligation.

31
00:04:44,080 --> 00:04:48,080
So, you ingratiate yourself with people by doing the errands,

32
00:04:48,080 --> 00:04:55,080
and then they give you food, and then they come to,

33
00:04:55,080 --> 00:04:58,080
giving in a different way, rather than being charity,

34
00:04:58,080 --> 00:05:02,080
it's wages.

35
00:05:02,080 --> 00:05:05,080
The dynamic breaks down.

36
00:05:05,080 --> 00:05:18,080
It's no longer religious thing, it's employment.

37
00:05:18,080 --> 00:05:25,080
I'm going to sparse palmistry. I don't know how that works as another category.

38
00:05:25,080 --> 00:05:26,080
But that's looking at all.

39
00:05:26,080 --> 00:05:30,080
So, the going of errands is looking at the four directions,

40
00:05:30,080 --> 00:05:33,080
even looking at the four directions.

41
00:05:33,080 --> 00:05:40,080
It says a symbol, a symbolism.

42
00:05:40,080 --> 00:05:47,080
Meaning, and even looking in four directions is a metaphor.

43
00:05:47,080 --> 00:05:50,080
And really, just like I said,

44
00:05:50,080 --> 00:05:55,080
there's a go between or an errand boy.

45
00:05:55,080 --> 00:05:58,080
And looking at the points between,

46
00:05:58,080 --> 00:06:04,080
there's a metaphor for palmistry.

47
00:06:04,080 --> 00:06:25,080
And so, that has to do with the body, maybe.

48
00:06:25,080 --> 00:06:41,080
I seek my food rightly and rightly and do I eat it after I have sought it.

49
00:06:41,080 --> 00:06:51,080
So, rightfully means without expectation,

50
00:06:51,080 --> 00:06:57,080
without insinuation or ingratiation.

51
00:06:57,080 --> 00:07:04,080
It's simply because people want to give.

52
00:07:04,080 --> 00:07:10,080
And in return, leading life that is pure,

53
00:07:10,080 --> 00:07:14,080
and setting an example,

54
00:07:14,080 --> 00:07:18,080
and offering teaching, religious teaching,

55
00:07:18,080 --> 00:07:24,080
which is the one thing that monks and religious people should do for the people.

56
00:07:24,080 --> 00:07:30,080
They offer them teachings that allow them to become more religious and more spiritual.

57
00:07:30,080 --> 00:07:36,080
Offering them apart in the spiritual life.

58
00:07:36,080 --> 00:07:43,080
Hmm.

59
00:07:43,080 --> 00:07:53,080
Sort of an interesting quote from Manastic,

60
00:07:53,080 --> 00:08:01,080
but not much else.

61
00:08:01,080 --> 00:08:08,080
We have questions today.

62
00:08:08,080 --> 00:08:11,080
Let's look at some questions.

63
00:08:11,080 --> 00:08:13,080
I'm not ramen disappeared,

64
00:08:13,080 --> 00:08:22,080
because I want my name.

65
00:08:22,080 --> 00:08:32,080
Let's see. Let's see.

66
00:08:32,080 --> 00:08:34,080
Too much chatting we lost on the questions.

67
00:08:34,080 --> 00:08:36,080
Too much chatting.

68
00:08:36,080 --> 00:08:40,080
What do you think this is a chat box?

69
00:08:40,080 --> 00:08:44,080
Meditators. It shouldn't be chatting.

70
00:08:44,080 --> 00:08:54,080
Some monks sleep in the floor on a bed made or a bed made of rock.

71
00:08:54,080 --> 00:08:58,080
Sleep on the carpet.

72
00:08:58,080 --> 00:09:02,080
Sleeping on a bed made of rock.

73
00:09:02,080 --> 00:09:06,080
I don't know how healthy that would be.

74
00:09:06,080 --> 00:09:08,080
Actually, it would be quite unhealthy.

75
00:09:08,080 --> 00:09:22,080
The monks were supposed to sleep on simple mattresses, but mattresses were allowed.

76
00:09:22,080 --> 00:09:28,080
Sleeping on the floor is good on a carpet or something.

77
00:09:28,080 --> 00:09:33,080
You don't sleep too much.

78
00:09:33,080 --> 00:09:39,080
In this head, we should guard our mind.

79
00:09:39,080 --> 00:09:43,080
What instance of us is used to guard the mind?

80
00:09:43,080 --> 00:09:47,080
Is it the mind itself, the consciousness?

81
00:09:47,080 --> 00:09:56,080
Yes, the mind is what guards the mind.

82
00:09:56,080 --> 00:10:02,080
Better to meditate after waking up or after breakfast.

83
00:10:02,080 --> 00:10:13,080
You've got mentions that eating is useful for meditation, especially in the morning,

84
00:10:13,080 --> 00:10:25,080
because it's hard to focus and the stomach is not settled until you've had some orange or oatmeal for breakfast.

85
00:10:25,080 --> 00:10:32,080
The rice, orange, or oatmeal, that kind of thing.

86
00:10:32,080 --> 00:10:42,080
It's going to be a simple plain breakfast to calm the stomach.

87
00:10:42,080 --> 00:10:49,080
The part is that.

88
00:10:49,080 --> 00:10:59,080
I think it might be in the dutangas, I'm not sure.

89
00:10:59,080 --> 00:11:05,080
So eating first is useful.

90
00:11:05,080 --> 00:11:14,080
Are the hindrance is viewed as hindering equanimity or hindering concentration or hindering any peaceful positive qualities of mind states?

91
00:11:14,080 --> 00:11:18,080
I think specifically it's about hindering concentration.

92
00:11:18,080 --> 00:11:23,080
I mean, in general they are hindrances, they hinder everything.

93
00:11:23,080 --> 00:11:32,080
They're in general bad, but I think probably the orthodox answer would be.

94
00:11:32,080 --> 00:11:39,080
The word hindrance is used to indicate the hindering of concentration.

95
00:11:39,080 --> 00:11:44,080
That's the hindering of wisdom as well because concentration means to wisdom.

96
00:11:44,080 --> 00:11:51,080
Or focused, you might say rather than concentration.

97
00:11:51,080 --> 00:11:56,080
But they hinder positive qualities in mind states in general.

98
00:11:56,080 --> 00:12:00,080
They'll even hinder success in life in a mundane sense.

99
00:12:00,080 --> 00:12:10,080
You can't succeed if you have these five or it's harder to succeed with them.

100
00:12:10,080 --> 00:12:16,080
Knee does not mean nibana, some kind of gut it.

101
00:12:16,080 --> 00:12:18,080
No, no, no.

102
00:12:18,080 --> 00:12:21,080
Maybe that's a common tear answer.

103
00:12:21,080 --> 00:12:24,080
Knee just means out or away.

104
00:12:24,080 --> 00:12:29,080
What are namis to ward?

105
00:12:29,080 --> 00:12:31,080
That's not the real entomology.

106
00:12:31,080 --> 00:12:34,080
Maybe the commentary talks about that.

107
00:12:34,080 --> 00:12:44,080
It will give you the real entomology.

108
00:12:44,080 --> 00:13:10,080
If you come from the verb nibara, it's a verb, it's nothing to do with nibana.

109
00:13:10,080 --> 00:13:23,080
Let's get the commentary.

110
00:13:23,080 --> 00:13:32,080
Knee, what are namis?

111
00:13:32,080 --> 00:13:34,080
Simple word.

112
00:13:34,080 --> 00:13:39,080
Nothing to do with nibana.

113
00:13:39,080 --> 00:13:45,080
There's nibana isn't about the nis, it's a good example.

114
00:13:45,080 --> 00:13:57,080
The nia at the beginning is a prefix meaning out or away or out from.

115
00:13:57,080 --> 00:14:08,080
Near means without or out of or away.

116
00:14:08,080 --> 00:14:11,080
Water means it's like to ward.

117
00:14:11,080 --> 00:14:16,080
It's probably the same as the English ward to ward away something.

118
00:14:16,080 --> 00:14:27,080
So nibana means to ward away and a hindrance is something that keeps you away, keeps you from.

119
00:14:27,080 --> 00:14:46,080
The nibana is away from or out of bondage, free from bondage, kind of thing.

120
00:14:46,080 --> 00:15:05,080
The move isn't happening yet.

121
00:15:05,080 --> 00:15:16,080
Can you accept boxes of food and tea from different countries?

122
00:15:16,080 --> 00:15:19,080
I can't accept food.

123
00:15:19,080 --> 00:15:26,080
I also can't accept money.

124
00:15:26,080 --> 00:15:37,080
So the best way is through our organization.

125
00:15:37,080 --> 00:15:40,080
You can go to our support page and look at ways to support there.

126
00:15:40,080 --> 00:15:46,080
As far as food for me, I've been accepting gift cards to subway or peanut pit or Starbucks or Tim Hortons.

127
00:15:46,080 --> 00:15:51,080
And these gift cards can all be purchased online.

128
00:15:51,080 --> 00:16:01,080
If you purchase them online, the company sends me an e-gift or sends a card in the mail or something, different ways they do that.

129
00:16:01,080 --> 00:16:09,080
But I'm not sure how much I'll be using that in the future because where we're living, there are not any convenient stores like that.

130
00:16:09,080 --> 00:16:27,080
We'll either hopefully try and get a steward, someone to come and stay and prepare food, or I'll go to the local, we'll try and set something up with the local restaurant so that I have food there.

131
00:16:27,080 --> 00:16:38,080
Hopefully we'll be able to stay alive a little longer.

132
00:16:38,080 --> 00:16:47,080
Okay, please use the green or orange button to preface your questions.

133
00:16:47,080 --> 00:16:57,080
Otherwise, I won't be able to find them.

134
00:16:57,080 --> 00:17:09,080
Could you please speak to how the lay person can make a living when guidelines to follow for those that do touch money?

135
00:17:09,080 --> 00:17:26,080
Yeah, I mean, on the one hand, you have to understand that there aren't going to be too many concrete guidelines because again, it's not so much about what you do but about how you do it.

136
00:17:26,080 --> 00:17:41,080
That being said, there are some basic concrete guidelines, but they're very much common sense from a Buddhist point of view, so any livelihood that breaks the five precepts is wrong.

137
00:17:41,080 --> 00:18:10,080
Any livelihood that directly involves substances that harm individuals is wrong, so selling poisons, selling weapons, selling animals, selling poisons, weapons, living beings, humans, I think just before.

138
00:18:10,080 --> 00:18:18,080
I can't remember these things.

139
00:18:18,080 --> 00:18:35,080
Usually, can't actually poison, weapons, living beings, and toxicants, I guess, is the fifth, right?

140
00:18:35,080 --> 00:18:44,080
This is all wrong. Anything that's involved, but it becomes down to some extent common sense.

141
00:18:44,080 --> 00:18:55,080
What you're doing is involved in harming, or if you have to do harmful things to make a living, then it's problematic.

142
00:18:55,080 --> 00:19:08,080
I guess I would say many types of livelihood are indirectly involved in unhosting things, or involved in unhosting things that are mild.

143
00:19:08,080 --> 00:19:26,080
Even entertainment isn't considered the greatest livelihood, because it's involved in what we call unhost and desire, there's desire involved, playing with people's emotions.

144
00:19:26,080 --> 00:19:38,080
You use quantum physics to support Buddhism as Buddhism believe in a single universe or a multiverse or other.

145
00:19:38,080 --> 00:19:56,080
Buddhism doesn't have too many beliefs, and it isn't focused on beliefs, Buddhism is much more about the truth of reality, which is very much in the present moment here and now.

146
00:19:56,080 --> 00:20:09,080
Our universe has no science, our universe is not singular, multiple, our universe is an experience.

147
00:20:09,080 --> 00:20:15,080
That's a series of experiences.

148
00:20:15,080 --> 00:20:21,080
That's what our tradition of Buddhism focuses on.

149
00:20:21,080 --> 00:20:28,080
I don't think too much about them.

150
00:20:28,080 --> 00:20:33,080
I don't think about them too much.

151
00:20:33,080 --> 00:20:44,080
It's okay to directly try to notice the elements when I know it as a rising and falling, again, a mental picture to belly rising and falling.

152
00:20:44,080 --> 00:20:55,080
If you can just say tense, if you want, but if you get a mental picture, you just say seeing, seeing.

153
00:20:55,080 --> 00:21:06,080
It's fine that it for it not to be, for it not to be amenable to your will when you see the pictures you see.

154
00:21:06,080 --> 00:21:14,080
I mean, a Buddha didn't require you to say things like earth or air, so on.

155
00:21:14,080 --> 00:21:16,080
He said, do kachanto wagatamiti, bhajamiti.

156
00:21:16,080 --> 00:21:19,080
When going, when knows, I go.

157
00:21:19,080 --> 00:21:21,080
I'm walking.

158
00:21:21,080 --> 00:21:25,080
When you say walking, you get a picture of your feet, no.

159
00:21:25,080 --> 00:21:33,080
Walking is meant to be a description of the feeling and the elements.

160
00:21:33,080 --> 00:21:40,080
They used to say walking, we used to sit things on.

161
00:21:40,080 --> 00:21:46,080
I wouldn't worry too much about that.

162
00:21:46,080 --> 00:21:52,080
But if you do see the image, you would see seeing.

163
00:21:52,080 --> 00:21:59,080
When mind stops in climbing, forming words and starts to black out rhythmically.

164
00:21:59,080 --> 00:22:03,080
So we forced it to form the noting word, we don't force it.

165
00:22:03,080 --> 00:22:17,080
If the mind starts to black out, whatever that means, you would say knowing, knowing, or feeling, feeling of calm, calm, quiet, right?

166
00:22:17,080 --> 00:22:21,080
You should use a word based on what you're experiencing.

167
00:22:21,080 --> 00:22:23,080
The word is a reminder.

168
00:22:23,080 --> 00:22:25,080
It's that deep.

169
00:22:25,080 --> 00:22:27,080
It's cultivation of mindfulness.

170
00:22:27,080 --> 00:22:35,080
You're reminding yourself, it's just this, so that you don't react to it.

171
00:22:35,080 --> 00:22:41,080
When a kama is being committed, can there be situations when the role of free will becomes insignificant?

172
00:22:41,080 --> 00:22:48,080
For example, a person has a view that killing animals is okay, and if a mosquito bites him, causing anger to arise.

173
00:22:48,080 --> 00:22:54,080
But he's still able to refrain from killing it, given that no other factors intervene.

174
00:22:54,080 --> 00:23:08,080
And that's very, I don't think you're making things from over the complicated.

175
00:23:08,080 --> 00:23:17,080
If the mosquito bites causes anger to arise, one that's failed and being mindful of the feeling.

176
00:23:17,080 --> 00:23:23,080
If the feeling comes and one knows it as a feeling, there will be no anger arise.

177
00:23:23,080 --> 00:23:26,080
If anger arises, it's already too late.

178
00:23:26,080 --> 00:23:37,080
Depending on one state of mind, that anger might result in killing me, be animal.

179
00:23:37,080 --> 00:23:40,080
But there's required more than anger.

180
00:23:40,080 --> 00:23:47,080
There's required wrong view, I think, I should think, because it's sort of on a wooden.

181
00:23:47,080 --> 00:23:58,080
It couldn't arise to the thoughts of that killing is the right answer.

182
00:23:58,080 --> 00:24:07,080
Here you ought to about what I have to intervene without wholesome, wholesome mind state one.

183
00:24:07,080 --> 00:24:12,080
It's not that it didn't intervene in time for the anger, but the anger is only one mind state.

184
00:24:12,080 --> 00:24:20,080
If anger there has to be the thoughts, I should kill this, the thought, kill.

185
00:24:20,080 --> 00:24:28,080
And that thought has to have, once that thought arises, there has to be a response, an anger and a delusion response.

186
00:24:28,080 --> 00:24:31,080
That's not.

187
00:24:31,080 --> 00:24:39,080
So without, it's about whether he or he ought to intervene at that point.

188
00:24:39,080 --> 00:24:44,080
They've already missed the chance to stop the anger from arising.

189
00:24:44,080 --> 00:25:11,080
But anger isn't enough, it requires the thought to kill and the reaction to that.

190
00:25:11,080 --> 00:25:23,080
All right, I'm going to give it a rest there for tonight.

191
00:25:23,080 --> 00:25:31,080
I'm going to do my meditation, skip my evening meditation, and I'm going to do it now.

192
00:25:31,080 --> 00:25:34,080
One more question.

193
00:25:34,080 --> 00:25:41,080
I remember hearing that you said we practiced Satipatana, but I also heard you say that we practiced hanapana Sati.

194
00:25:41,080 --> 00:25:42,080
What do you mean?

195
00:25:42,080 --> 00:25:48,080
It's a combination.

196
00:25:48,080 --> 00:26:13,080
Anapana Sati means mindfulness, Sati is Satipatana.

197
00:26:13,080 --> 00:26:17,080
Anapana means the breath.

198
00:26:17,080 --> 00:26:23,080
It's both Anapana Sati and Satipatana.

199
00:26:23,080 --> 00:26:25,080
In the future, do you plan on going back to Thailand?

200
00:26:25,080 --> 00:26:28,080
Oh, I don't think too much about the future.

201
00:26:28,080 --> 00:26:39,080
In the future, I plan on moving to Stony, to Hamilton, about ten minutes down the road.

202
00:26:39,080 --> 00:26:48,080
Is it fair to say that an Arahan has the perfect free will since they are not swayed by the department?

203
00:26:48,080 --> 00:26:50,080
I don't know.

204
00:26:50,080 --> 00:27:01,080
I don't like using terms like free will or determinism.

205
00:27:01,080 --> 00:27:11,080
I wouldn't say such a thing.

206
00:27:11,080 --> 00:27:15,080
I would say an Arahan has perfect freedom.

207
00:27:15,080 --> 00:27:16,080
Besides the fact that they still have to live, still have to eat, still have to suffer physically.

208
00:27:16,080 --> 00:27:28,080
Besides that, they're very much free.

209
00:27:28,080 --> 00:27:35,080
It's exercising and other presumed health habits, just another attachment.

210
00:27:35,080 --> 00:27:40,080
Well, there's some extent you could argue that it's functional.

211
00:27:40,080 --> 00:27:43,080
Exercise to some limited extent is functional.

212
00:27:43,080 --> 00:27:47,080
But for the most part, exercise is because we eat too much.

213
00:27:47,080 --> 00:27:48,080
We didn't eat too much.

214
00:27:48,080 --> 00:27:53,080
We wouldn't need to exercise nearly as much as most people would do.

215
00:27:53,080 --> 00:27:58,080
Place sports and that kind of thing, because of too much food.

216
00:27:58,080 --> 00:28:07,080
If you ate just enough to survive, you wouldn't need to do much exercising to keep yourself healthy.

217
00:28:07,080 --> 00:28:13,080
That being said, for some exercise is useful.

218
00:28:13,080 --> 00:28:17,080
And some amount of healthiness is reasonable.

219
00:28:17,080 --> 00:28:26,080
And that's a good practice, but often we become very much obsessed with health.

220
00:28:26,080 --> 00:28:28,080
And that's a problem.

221
00:28:28,080 --> 00:28:33,080
You try to be too healthy, too obsessed with always being healthy.

222
00:28:33,080 --> 00:28:37,080
Then you won't be able to deal with sickness.

223
00:28:37,080 --> 00:28:42,080
I'll be very much immersed and concerned and afraid of sickness.

224
00:28:42,080 --> 00:28:45,080
Shouldn't be afraid of getting sick.

225
00:28:45,080 --> 00:28:53,080
And you should be ready and able to deal with sickness when it comes.

226
00:28:53,080 --> 00:28:59,080
And so obsessing about health is somewhat counterproductive.

227
00:28:59,080 --> 00:29:01,080
Okay, I'm really going this time.

228
00:29:01,080 --> 00:29:02,080
Good night everyone.

229
00:29:02,080 --> 00:29:03,080
Have a good night.

230
00:29:03,080 --> 00:29:23,080
See you next time.

