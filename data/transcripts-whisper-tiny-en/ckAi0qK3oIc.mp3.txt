I've been thinking about asking for a girl's number who's been staring at me, who have
been terrified of even with meditation since I fear her, this is a question, should I
confront her or would that pursue backfire?
Giving dating advice, I can get in trouble here, if I give you advice as to how to
hook up with someone, I can, it's a serious offense of the Vinay.
Guys, they can't tell you how to get the girl.
Someone's already given an answer, be brave and talk to her, girls love guys who are
confident.
Yeah, if I said something like that, I can be serious, in serious trouble for the Vinay.
Just to let you know, that's a difficult question to answer.
They shouldn't fear people at all, and you could create a harmonious relationship outside
of romance, if you were to talk to her, that's where I would go with it.
I think the best advice you can give to someone is to create friendships, whether romance
occurs or not, it's not really going to make you happy, in fact it often makes you miserable,
but friendship will never, true friendship will never make you miserable, love for people
will never make you miserable, but you have to be able to separate love from attachment.
So what we think of as love is really love attachment, or it's too mixed together.
So yeah, I think that should help you with your fear as well.
If you looked at it as a chance to make a friend, or a chance to cultivate loving kindness,
if you aren't afraid of losing something, you see, because why are you afraid?
Because of how you look, because of how she'll think of you, because of you don't know
how to act or so on, that's the big deal, that's the big deal with everything, why
fear comes up, because you're afraid of failure, you're afraid of looking bad, you're
afraid of people laughing at you.
The biggest lesson I learned as a teacher was to stop being afraid of people laughing
at me and criticizing me, it's such a wonderful thing to have people saying bad things
about you, it's just so liberating, because there's only one answer to say, who cares?
Someone calls you a buffalo, just turn around and see if you've got a tail, if you've
got a tail, okay, you're a buffalo, if you don't, why are you excited?
And that's really the deal, is many times the criticism is valid, people criticize you,
and it's valid, it's like, wow, well, yes, that's the truth, and if it's not, it's
not, but that's all it is, if you say something really stupid and you act like an idiot
and the girl realizes, oh God, this guy is socially inept, okay, so you're socially inept,
who cares?
Really, that's the question, what is the problem with being incompetent, useless, meaningless?
I think that's the most liberating thing is to realize that you have used this you are,
to realize that you're, you have no value whatsoever, I didn't think of a talk on
that one, I've certainly talked about it before, I think that's a wonderful
thing, to realize that you have no value, it's go so contrary to what we're
taught, modern society is you are valuable, you are special,
but wasn't there a talk, someone posted a talk recently this guy gave an
involuntary an address, where I don't remember what it was about or if it was even Buddhist,
but there was one part that was kind of intriguing, he says, you're all taught that you're
special, and I want to tell you you're not, and so the whole talk was about how you're
not special, because if you're special then you've got a reputation to make to you, you've
got something to live up to, something to cling to, you're not special, you're just a lump
of flesh on a rock, big rock, you're a motor corner of the galaxy, mostly harmless, that's
all we are, so when you can think like that, then you should have no trouble, when you
have no expectations and no desires, that's where the best friendships come, when you have
no desires and no expectations, even for friendship, where you give people the opportunity
to benefit from your presence for you, give people your service and your help as they ask
for it, and as their need becomes apparent, but don't look for anything more, and don't
cling to them and don't pull them and don't try to keep them or hold them or anything
that this is so much suffering and stress, Brahmua, I'm Brahmua, so he said something quite
clever about this, he said, he asked a question, who is the most important person?
And his answer was, you know, I think I disagree that actually the most important person
is yourself, and if all of us, because the Buddha clearly said that you're to look after
your own behavior, for example, do your own work, and that's what has been actually
benefits others, but he certainly has a point when he says the most important person
is whoever you're with, and it's an important point in the sense of not clinging to beings
or people and saying, this is who I have to help, this is who I love, this is who I want
to be with and so on, and be with whoever you're with, when you're with someone, be with
them and you're with someone else, be with someone else, rather than cultivating friendships,
cultivate friendship, there you go, there's a quote for you, rather than cultivating
friendships, cultivate friendship, friendliness, be the kind of friend that you'd like to
have, be the friend to everyone, and then you can't go wrong, I mean, and then this girl's
staring is meaningless, like, well, if she wants to stare at me good for her, maybe that
makes her happy, I don't know, or then you write us none of my business, but if she needs
my help, I'm there to give it, if she doesn't, well, there certainly will be someone who
does, but when you focus on this girl and lay the woman female being, it limits you, and
it takes away your energies and it creates all sorts of stress and fear, which is not
good for either of you, I would say go up and talk to her and and create a more, more
wholesome behavior, there's someone staring at you, I mean, well, it's kind of a reason
to start a conversation, but as a Buddhist monk, I'm bound to answer in terms of platonic
relationship, it's sort of, they don't have anything to recommend in terms of romance, and
much too much to, to sort of try to dissuade you from, much to speak against it, from experience
and from practice and from theory, romance is really not where it's at.
