1
00:00:00,000 --> 00:00:02,640
Hi, I'm welcome back to Ask a Monk.

2
00:00:02,640 --> 00:00:07,600
I'm going to be heading off to Thailand at the end of this month.

3
00:00:07,600 --> 00:00:16,420
So, you probably won't have any videos from me until the end of the year, maybe around

4
00:00:16,420 --> 00:00:20,720
Christmas, I'll be back.

5
00:00:20,720 --> 00:00:25,400
Just an update, if you're keeping up with what's going on, we've been trying to get this

6
00:00:25,400 --> 00:00:33,640
forest meditation center off the ground, and the difficulty is that a lot of the people

7
00:00:33,640 --> 00:00:40,880
who have been meditating with me are, you know, living in the city, they don't have

8
00:00:40,880 --> 00:00:48,560
the same kind of desire to get out of the city and into the countryside, so this place that

9
00:00:48,560 --> 00:00:55,560
we found wonderful and beautiful that I was raving about didn't get the best reception,

10
00:00:55,560 --> 00:01:09,240
dangerous, far too much, too difficult and so on, but in my heart I think that's the

11
00:01:09,240 --> 00:01:13,800
kind of place that we're looking, that I'm looking for rather than finding a place in

12
00:01:13,800 --> 00:01:21,440
the city, which should be a lot easier, but in the end become cramped and actually

13
00:01:21,440 --> 00:01:26,120
make it quite difficult to run meditation, of course, is there, you know, I'll be going

14
00:01:26,120 --> 00:01:30,640
off to Thailand and I'll be in the most rugged of conditions and I don't think there's

15
00:01:30,640 --> 00:01:35,240
anything that you could find here in America that could compare to some of the things

16
00:01:35,240 --> 00:01:41,000
that we go through in the forest of Thailand, so just kind of re-adjusting and trying

17
00:01:41,000 --> 00:01:47,760
to figure out who's actually interested in a genuine forest center, which should probably

18
00:01:47,760 --> 00:01:53,240
be, yeah, a little bit rugged and maybe you have to put up some difficulty with some

19
00:01:53,240 --> 00:02:00,640
difficulty, but that's really a part of what we're doing here is to be able to give up

20
00:02:00,640 --> 00:02:09,000
our attachments to self and to comfort, to things like personal security and safety,

21
00:02:09,000 --> 00:02:11,040
our fears, our phobians.

22
00:02:11,040 --> 00:02:15,760
A lot of it's just things like being afraid of ghosts and just afraid of the dark and

23
00:02:15,760 --> 00:02:20,320
afraid of the quiet and afraid of the forests and so on, which was why we go into the forest

24
00:02:20,320 --> 00:02:25,560
and Buddha said that the forest is very difficult if you're not able to give up these

25
00:02:25,560 --> 00:02:30,680
things and that's why we go into the forest to test ourselves out, and test ourselves

26
00:02:30,680 --> 00:02:34,520
out whether we can give these things up.

27
00:02:34,520 --> 00:02:41,960
Just as monks we go and stay in the cemeteries or where they burn dead bodies and so on,

28
00:02:41,960 --> 00:02:47,480
just to test ourselves, to help ourselves, let go.

29
00:02:47,480 --> 00:02:53,760
So yeah, I'm going away for a couple of months and it's kind of extended, I was originally

30
00:02:53,760 --> 00:02:58,480
going to go for just a short time, but I've extended it because this project is probably

31
00:02:58,480 --> 00:03:03,840
going to take a little bit longer than I thought some of the key supporters are kind

32
00:03:03,840 --> 00:03:08,560
of backing out or putting on an ultimatum, either find a place in the city or we're

33
00:03:08,560 --> 00:03:09,760
not interested in.

34
00:03:09,760 --> 00:03:17,520
So rather than compromise to that extent, I'd rather put it off and wait until I can really

35
00:03:17,520 --> 00:03:23,000
pull people around to the idea of finding something that's a little more ideal and closer

36
00:03:23,000 --> 00:03:25,200
to a little bit before.

37
00:03:25,200 --> 00:03:29,280
Either that or we find a new place that we can all agree on.

38
00:03:29,280 --> 00:03:36,880
So just an update there and you know that I'll probably be doing some videos in Thailand

39
00:03:36,880 --> 00:03:43,960
because the environment really helps out with that, makes a nice backdrop for the DVD

40
00:03:43,960 --> 00:03:53,840
that I did as you can see, but I'll probably have those videos up when I get back doing

41
00:03:53,840 --> 00:03:58,640
some ask amongst stuff there if I bring some questions along or maybe I can check my email

42
00:03:58,640 --> 00:04:02,720
and get questions during the time in Thailand and so on.

43
00:04:02,720 --> 00:04:04,720
Okay, so anyway, onto the question.

44
00:04:04,720 --> 00:04:09,760
This next question is from Nang Chan Bodhi.

45
00:04:09,760 --> 00:04:13,840
Can you describe the difference between Buddhist meditation and the silent sitting meditation

46
00:04:13,840 --> 00:04:19,080
techniques used in Chigong healing or medicine practices?

47
00:04:19,080 --> 00:04:26,640
Could the Chigong meditation be thought of as a guided or suggestive or even maybe shamanic?

48
00:04:26,640 --> 00:04:36,640
It's hard for me to comment on meditations outside of the Buddhist tradition or even

49
00:04:36,640 --> 00:04:44,320
outside of my specific type of Buddhist meditation.

50
00:04:44,320 --> 00:04:51,320
Generally if it's something that has to do with what I do then I'll talk about it.

51
00:04:51,320 --> 00:04:57,280
If it doesn't relate to what I do then I try not to pass judgment.

52
00:04:57,280 --> 00:05:01,880
So I got called out for not recommending certain meditation courses and that's just because

53
00:05:01,880 --> 00:05:05,960
I don't practice that way and so I don't know why I would recommend it.

54
00:05:05,960 --> 00:05:10,360
It's not that I think there's anything wrong with it, it's just not what I do so it's

55
00:05:10,360 --> 00:05:13,000
not for me to recommend it.

56
00:05:13,000 --> 00:05:22,720
So for me to talk about Chigong meditation, from the point of view of what I do, these types

57
00:05:22,720 --> 00:05:28,040
of meditation are meditation to create something.

58
00:05:28,040 --> 00:05:33,840
Now one of the ways to look at what are the key differences between meditations and the

59
00:05:33,840 --> 00:05:38,680
meditation that I teach and what we do is not to create anything except a clear understanding

60
00:05:38,680 --> 00:05:39,960
of what's already there.

61
00:05:39,960 --> 00:05:45,600
So whatever arises you're just going to look at that and it's more like taking away.

62
00:05:45,600 --> 00:05:49,360
A lot of people complain about practicing for a while, not getting anywhere, nothing's

63
00:05:49,360 --> 00:05:55,040
happening and it's funny because you can always point out well what do you mean nothing's

64
00:05:55,040 --> 00:05:56,040
happening?

65
00:05:56,040 --> 00:05:57,560
What does that mean in your mind?

66
00:05:57,560 --> 00:06:00,360
How do you feel when you get this feeling that nothing's happening?

67
00:06:00,360 --> 00:06:04,160
Well you feel bored or so well then there is something very important there and it's

68
00:06:04,160 --> 00:06:08,200
a state of discontent with reality.

69
00:06:08,200 --> 00:06:18,520
So Chigong is as I understand it a way of altering the state of reality to create a

70
00:06:18,520 --> 00:06:21,680
state of health.

71
00:06:21,680 --> 00:06:25,560
As I understand it, you see you talk about Chigong healing, so it's a method of healing,

72
00:06:25,560 --> 00:06:27,800
the body.

73
00:06:27,800 --> 00:06:32,960
I personally believe that there are forces that run through the body or in reality that

74
00:06:32,960 --> 00:06:36,960
can be harnessed and you can feel this in meditation.

75
00:06:36,960 --> 00:06:40,880
Sometimes you get this great power and strength inside but it's not what I teach and

76
00:06:40,880 --> 00:06:44,160
it's not what I'm involved in.

77
00:06:44,160 --> 00:06:50,880
My practice and what I encourage rather people is to let go is to be able to put up with

78
00:06:50,880 --> 00:06:54,960
sickness, to put up with things as they are.

79
00:06:54,960 --> 00:07:00,120
There's a reason why you're here, it's your path, it's where you're going and to the extent

80
00:07:00,120 --> 00:07:06,200
that you can put up with them, you'll be far better off spiritually.

81
00:07:06,200 --> 00:07:10,200
I don't want to detract from these practices, if someone is sick and they want to go

82
00:07:10,200 --> 00:07:14,120
see a Chigong healer fine, if you want to go see a Western doctor fine, if you want to

83
00:07:14,120 --> 00:07:20,240
go see a Chinese doctor fine, if that's your thing great and if you feel that it's

84
00:07:20,240 --> 00:07:25,640
going to support your spiritual practice to be healthy and so on, fine.

85
00:07:25,640 --> 00:07:30,360
I would say that in some cases that's probably true, if you have a debilitating illness

86
00:07:30,360 --> 00:07:35,400
and you feel that it's getting in the way of your practice or I mean it can even get

87
00:07:35,400 --> 00:07:39,880
in the way of you living your life, getting a job and so on, then these things can be great

88
00:07:39,880 --> 00:07:43,360
and they're a real help in a worldly sense.

89
00:07:43,360 --> 00:07:48,120
But from a spiritual point of view I don't really see the benefit, if you talk about any

90
00:07:48,120 --> 00:07:52,840
direct spiritual gain that you get from it because you're still creating and you're still

91
00:07:52,840 --> 00:07:57,640
holding on, you're still clinging to the idea of being healthy and so on.

92
00:07:57,640 --> 00:08:03,480
This meditation is on another level, it's the letting go, the ability to be sick and still

93
00:08:03,480 --> 00:08:09,200
be content to get old and still be content to die and still be content.

94
00:08:09,200 --> 00:08:17,160
So there's no idea that we should live longer or be healthier so on, you know, have some

95
00:08:17,160 --> 00:08:22,520
spiritual strength in our body or so on, we're trying to accept things as they are

96
00:08:22,520 --> 00:08:31,480
and to let go and in the end really enlightenment is if you truly become enlightened, you

97
00:08:31,480 --> 00:08:36,400
really have no interest in living on and I would say by extension the more you practice

98
00:08:36,400 --> 00:08:42,640
meditation, really the less interest you have in being strong, being healthy, being long

99
00:08:42,640 --> 00:08:48,920
lived and so on, you don't really have any desire for anything, you become less clingy

100
00:08:48,920 --> 00:08:57,480
and you don't pass a judgment on things, so you're able to live free or now, the argument

101
00:08:57,480 --> 00:09:01,720
that people generally use is that, yeah, but until you become enlightened, you know, you

102
00:09:01,720 --> 00:09:04,680
need to be healthy and you need this and this and this.

103
00:09:04,680 --> 00:09:12,200
To some extent, yes, but I would guarantee that for most people it's still an attachment

104
00:09:12,200 --> 00:09:16,640
and it gets to the point where we're worried about our health, where we're concerned

105
00:09:16,640 --> 00:09:23,400
that we're not getting enough of this, enough of that and we're concerned that we're unhealthy

106
00:09:23,400 --> 00:09:30,760
when we get sick and bothers us and so on. If you can separate yourself from this mental

107
00:09:30,760 --> 00:09:37,480
upset and rationally say that, yeah, okay, I have to go see a doctor because otherwise

108
00:09:37,480 --> 00:09:41,080
I'm not going to be able to do meditation or otherwise it's just going to get in the

109
00:09:41,080 --> 00:09:47,280
way of my concentration and so on and fine, yeah, I think I'm power to you, but there's

110
00:09:47,280 --> 00:09:52,160
a great difference between this sort of healing and what we're doing, so I hope that's

111
00:09:52,160 --> 00:09:56,320
clear. Thanks everyone for tuning in. Thanks everyone for your support.

112
00:09:56,320 --> 00:10:05,760
Some people actually send some donations to our nonprofit organization and many people

113
00:10:05,760 --> 00:10:09,320
are offering their support. Some people are actually saying they're going to show up if

114
00:10:09,320 --> 00:10:13,360
we're ready. Some people saying, hey, I'm coming in January, I will be ready by then.

115
00:10:13,360 --> 00:10:18,320
I'm not sure at this point. January, probably you can come by, but you might just be staying

116
00:10:18,320 --> 00:10:24,200
in an apartment underneath the one I'm in or even in the same apartment as me. Well,

117
00:10:24,200 --> 00:10:28,280
we look for a place, but power to you, if you're coming out, you can be a support and

118
00:10:28,280 --> 00:10:32,800
I can show people can say, see, see, here's people who really want to practice people

119
00:10:32,800 --> 00:10:39,120
who see this, this, this, you know, going this for idea before its meditation and are

120
00:10:39,120 --> 00:10:44,000
so gung-ho that they're willing to come and put up with me in this apartment. So you're

121
00:10:44,000 --> 00:10:50,880
welcome to come by. We have a couple of apartments that we can probably request on a short

122
00:10:50,880 --> 00:10:57,240
term if you're interested. Okay, so keep in touch. Let me know if you're coming, if

123
00:10:57,240 --> 00:11:02,840
you'd like to help and so on. If you'd like to help, you can click on our website,

124
00:11:02,840 --> 00:11:16,080
www.seremungalow.org. And again, thanks for the questions. If you have questions, keep

