1
00:00:00,000 --> 00:00:13,000
Is cultivating a habit of noting that doesn't correspond to a special mindfulness of the phenomenon, a step in the wrong direction?

2
00:00:13,000 --> 00:00:28,000
Also, as you progress with Mahasim noting, does it become difficult for one to be mindful without noting?

3
00:00:28,000 --> 00:00:38,000
Not sure if the first question means really what I understand.

4
00:00:38,000 --> 00:00:49,000
So to answer as I understand your question is, yes, it would lead you in a wrong direction.

5
00:00:49,000 --> 00:01:03,000
You note everything that is arising in the present moment, what is the most prominent that which is most prominent to you.

6
00:01:03,000 --> 00:01:26,000
So that is what you know, what you focus on as long as it lasts and what you think that is the question.

7
00:01:26,000 --> 00:01:45,000
So they are asking about noting in some other way. Perhaps it means noting budho budho or perhaps it means probably just speculative in terms of is it really important to have the noting the label correspond in some way to the object?

8
00:01:45,000 --> 00:02:01,000
Well, the label should respond to what is going on, as I said, it has to be done in the present moment.

9
00:02:01,000 --> 00:02:17,000
So when there is the rising of the abdomen, there should be the noting of the rising of the abdomen and when there is hearing there should be the noting of the hearing and not something else.

10
00:02:17,000 --> 00:02:44,000
If you say something else, when you hear actually something and you say budho, it is not noting the present moment. So it wouldn't really make sense and it wouldn't really make sense to when you have a hearing and you say something else or you don't know it at all.

11
00:02:44,000 --> 00:02:54,000
That wouldn't really make sense. Or if you hear something and say if you hear a dog and say dog, I think that is a practical example that we often get.

12
00:02:54,000 --> 00:03:11,000
And for the second question, I don't know if it really becomes difficult to be mindful without noting.

13
00:03:11,000 --> 00:03:18,000
I wouldn't say it is difficult. It becomes a habit and then it's easy.

14
00:03:18,000 --> 00:03:30,000
So you train it and then your mind works that way. It takes a while to make your mind work that way.

15
00:03:30,000 --> 00:03:52,000
But once it does, it's natural and not difficult anymore. So the part when you experience it as difficult is only lasting a while and that how long that lasts depends on how much you practice.

16
00:03:52,000 --> 00:04:03,000
So the more you practice, the faster it becomes a habit and the easier it gets quick.

17
00:04:03,000 --> 00:04:23,000
Mindfulness becomes a habit. If you are doing anything that cultivates mindfulness then it can only make being mindful easier.

18
00:04:23,000 --> 00:04:33,000
You won't feel your lips when you are speaking. But when you are mindful and you can notice it.

19
00:04:33,000 --> 00:04:42,000
And usually you don't feel the foot when you are walking. But with walking meditation we train to feel it.

20
00:04:42,000 --> 00:04:54,000
So then you can't hardly walk without being mindful on something.

21
00:04:54,000 --> 00:05:07,000
And then your mind as an effect becomes much calmer because you are just with what is going on and not with thousands of other things going,

22
00:05:07,000 --> 00:05:12,000
pushing and rushing through your mind.

23
00:05:12,000 --> 00:05:29,000
One thing about, I had some thoughts on this, just some observations on the non-corresponding, non-corresponding labeling or noting because it has a broad subject.

24
00:05:29,000 --> 00:05:37,000
Why we have a word at all is because of our understanding of the word sati, which we always translate as mindfulness.

25
00:05:37,000 --> 00:05:44,000
The word sati comes from the root sara which means to remember or to recall something.

26
00:05:44,000 --> 00:05:50,000
So the Buddha himself explained mindfulness as when walking one knows I am walking.

27
00:05:50,000 --> 00:05:59,000
Of course in Pali it is one word, a gachami, which means I am walking. It is really first person I am walking or I walk.

28
00:05:59,000 --> 00:06:06,000
You don't have to say I, you can just say gachami, which means a first person undergoing walking.

29
00:06:06,000 --> 00:06:13,000
When standing one knows I am standing. When there is a painful feeling, one knows there is a painful feeling.

30
00:06:13,000 --> 00:06:23,000
So this, bringing to mind or remembering the object for what it is, as the Buddha used the word pati sati mata.

31
00:06:23,000 --> 00:06:29,000
He said, the way you do it is you have pati sati mata and you see toji, you do well.

32
00:06:29,000 --> 00:06:34,000
Not clinging to anything or not dependent on anything.

33
00:06:34,000 --> 00:06:45,000
Remembering things just for what they are sati, pati sati mata. Remembering things just as they are.

34
00:06:45,000 --> 00:06:52,000
Because that creates a clear awareness of the object as it is, it brings the mind closer to that object.

35
00:06:52,000 --> 00:07:01,000
The word doesn't really matter what it is, as long as the word has the effect of bringing the mind closer to the object as it is.

36
00:07:01,000 --> 00:07:09,000
One, bringing it closer to the object. Two, keeping it on the object. Three, preventing proliferation based on the object.

37
00:07:09,000 --> 00:07:18,000
It has these three qualities to it. If you don't use the word, your mind will not ever, or will be hard pressed to fully fix on the object.

38
00:07:18,000 --> 00:07:21,000
This is why people practice Buddha. This is why they practice Om.

39
00:07:21,000 --> 00:07:28,000
Because the word focuses the mind on the object, whatever that object's spiritual or mundane might be.

40
00:07:28,000 --> 00:07:43,000
When you say to yourself, pain, pain, the mind is drawn to the pain. It gravitates towards the pain naturally.

41
00:07:43,000 --> 00:07:51,000
So obviously if you were to say something like, for example, some people would do this, if you feel pain, they would say happy, happy, happy.

42
00:07:51,000 --> 00:07:56,000
Because they want to feel happy, or they would say no pain, no pain, no pain, because they want to have no pain.

43
00:07:56,000 --> 00:08:01,000
So ask yourself, what does that do? Does that bring the mind to see the pain clearly for what it is?

44
00:08:01,000 --> 00:08:11,000
No. What it does is create a version in the mind and create a desire in the mind and the tendency in the mind to recoil from the pain.

45
00:08:11,000 --> 00:08:18,000
Which the point is people see that as the goal, or as the practice. That's why they take medication and so on.

46
00:08:18,000 --> 00:08:26,000
So we have this idea that meditation is going to be another drug that allows us to get away from pain. If we have pain, we say no pain, no pain.

47
00:08:26,000 --> 00:08:29,000
Some people do this.

48
00:08:29,000 --> 00:08:35,000
If you use a word that is not related at all, it's just going to create confusion.

49
00:08:35,000 --> 00:08:41,000
Of course, I doubt that you're seriously considering that as a meditation practice.

50
00:08:41,000 --> 00:08:50,000
Like if you feel pain to say apple, apple, apple, or something. You might say some other object as a means similar to no pain to get your mind on to something else.

51
00:08:50,000 --> 00:08:55,000
If there's pain, suddenly start, for people who have anger, for example, valid meditation technique.

52
00:08:55,000 --> 00:09:03,000
You have anger in the mind and you say to yourself, 19998979695 counting down from 100.

53
00:09:03,000 --> 00:09:05,000
Eventually you forget about the anger.

54
00:09:05,000 --> 00:09:09,000
So it's a means of getting rid of the anger, but it doesn't teach you about the anger.

55
00:09:09,000 --> 00:09:17,000
It's the crucial difference between what we do and what many other meditation, mantra meditation practices do.

56
00:09:17,000 --> 00:09:23,000
We're actually using the mantra to bring us closer to the object as opposed to avoiding the mundane reality.

57
00:09:23,000 --> 00:09:32,000
But the really interesting part is when people use spiritual mantras to describe mundane phenomena.

58
00:09:32,000 --> 00:09:40,000
And this happens in Buddhism. So please, I don't want to come across as too critical or anything.

59
00:09:40,000 --> 00:09:44,000
But as an observation, you can see where it tends to lead people.

60
00:09:44,000 --> 00:09:49,000
For example, there are teachers. From their point of view, they're totally correct.

61
00:09:49,000 --> 00:09:57,000
But there seems to be some suspicion and perhaps some correlation here that they watch the breath and say,

62
00:09:57,000 --> 00:10:05,000
Buddha is the name of our teacher, but it also means one who knows.

63
00:10:05,000 --> 00:10:12,000
And they realize this. So they watch the breath saying, one who knows, one who knows, one who knows.

64
00:10:12,000 --> 00:10:18,000
And some of the teachers that I've heard will actually come to a realization that the breath is the one who knows,

65
00:10:18,000 --> 00:10:26,000
and there is the one who knows, and there is this mind that is permanent and stable and lasts forever and just knows.

66
00:10:26,000 --> 00:10:30,000
And I've been told that, you know, that's just a figure of speech.

67
00:10:30,000 --> 00:10:36,000
But if it's not a figure of speech, it seems quite suspicious to me that the mind that there could be a mind that lasts forever.

68
00:10:36,000 --> 00:10:43,000
There could be a knowing of anything that lasts forever because that knowing had to arise from something.

69
00:10:43,000 --> 00:10:47,000
It arose out of nothing. It came into being at some point.

70
00:10:47,000 --> 00:10:51,000
And therefore, meaning Buddha said, all consciousness is impermanent.

71
00:10:51,000 --> 00:10:54,000
Any knowing that could arise would be impermanent.

72
00:10:54,000 --> 00:11:00,000
But this seems to be a potential problem that comes from mixing mundane and spiritual.

73
00:11:00,000 --> 00:11:06,000
The idea of some one who knows or just knowing with the actual breath,

74
00:11:06,000 --> 00:11:12,000
instead of saying breathing or feeling in the body with the actual sensations are,

75
00:11:12,000 --> 00:11:14,000
or as we say, rising, falling.

76
00:11:14,000 --> 00:11:20,000
There's another group that does, they use something similar to Buddha,

77
00:11:20,000 --> 00:11:25,000
but they say sama-arahang, sama-arahang, which is just another name for the Buddha.

78
00:11:25,000 --> 00:11:27,000
But sama means right.

79
00:11:27,000 --> 00:11:34,000
Arahang means worthy, or they could say enlightened, rightly enlightened.

80
00:11:34,000 --> 00:11:40,000
And at the same time, or maybe not at the same time, but they also somehow throughout it develop,

81
00:11:40,000 --> 00:11:41,000
that's right.

82
00:11:41,000 --> 00:11:42,000
They do it.

83
00:11:42,000 --> 00:11:49,000
They develop this vision of a crystal ball, a little light of some sort,

84
00:11:49,000 --> 00:11:52,000
at different parts of the body, and all the way through the breath,

85
00:11:52,000 --> 00:11:56,000
from one end of the breath to the other, all the way down to just above the navel.

86
00:11:56,000 --> 00:12:05,000
And I've heard some incredibly different things from this group.

87
00:12:05,000 --> 00:12:10,000
They will say, and eventually you realize that this is the mind,

88
00:12:10,000 --> 00:12:13,000
or this is the truth, or this is the ultimate reality.

89
00:12:13,000 --> 00:12:20,000
This is the body of the dhamma, and it exists here at the last base,

90
00:12:20,000 --> 00:12:23,000
where you lead this imaginary crystal.

91
00:12:23,000 --> 00:12:26,000
Eventually, you see this crystal right above your navel,

92
00:12:26,000 --> 00:12:30,000
and they say this is the center of gravity, which is the middle way, therefore.

93
00:12:30,000 --> 00:12:34,000
It's in the middle of the body, so because it's the center of gravity,

94
00:12:34,000 --> 00:12:36,000
it's the middle way, and that's where the mind lives.

95
00:12:36,000 --> 00:12:41,000
And they say the mind is this big, and so on.

96
00:12:41,000 --> 00:12:44,000
And if you listen to them, it's really what's going on,

97
00:12:44,000 --> 00:12:47,000
is they're bringing together these different concepts.

98
00:12:47,000 --> 00:12:52,000
The crystal ball, some are a hung, and the breath.

99
00:12:52,000 --> 00:12:56,000
And the mind, so as a result, the mindfulness that comes in the mind,

100
00:12:56,000 --> 00:13:04,000
is a mindfulness of this view that this is the right truth,

101
00:13:04,000 --> 00:13:07,000
that is this crystal ball at the center of the body,

102
00:13:07,000 --> 00:13:10,000
where the breath ends.

103
00:13:10,000 --> 00:13:14,000
So that, I think, is some, you know,

104
00:13:14,000 --> 00:13:17,000
you can see that as a criticism, or a potential argument,

105
00:13:17,000 --> 00:13:19,000
or a debate, or a controversial point.

106
00:13:19,000 --> 00:13:24,000
But as an example, just from one point of view,

107
00:13:24,000 --> 00:13:29,000
this is a potential, something that may be a problem,

108
00:13:29,000 --> 00:13:34,000
if people are not perfectly clear, impartial,

109
00:13:34,000 --> 00:13:37,000
and precise about the noting.

110
00:13:37,000 --> 00:13:42,000
And therefore, we can see the importance of using the correct word, I think.

111
00:13:42,000 --> 00:13:45,000
Okay.

112
00:13:45,000 --> 00:13:49,000
Aug, it will always be the concept,

113
00:13:49,000 --> 00:13:58,000
and you can't explain that to somebody,

114
00:13:58,000 --> 00:14:03,000
and you can't experience this dog.

115
00:14:03,000 --> 00:14:04,000
Right.

116
00:14:04,000 --> 00:14:07,000
But you can experience hearing.

117
00:14:07,000 --> 00:14:09,000
That's a very important, something that I didn't mention,

118
00:14:09,000 --> 00:14:15,000
is that, yeah, the difference between,

119
00:14:15,000 --> 00:14:17,000
I'm recording actually.

120
00:14:17,000 --> 00:14:19,000
I missed the first part.

121
00:14:19,000 --> 00:14:22,000
The difference between concepts and reality.

122
00:14:26,000 --> 00:14:28,000
That's a whole other issue, actually, really,

123
00:14:28,000 --> 00:14:31,000
because the concept can be an adequate object of meditation.

124
00:14:31,000 --> 00:14:35,000
In fact, I was teaching dog meditation in my first kid's video.

125
00:14:35,000 --> 00:14:37,000
The first kid's video, you'll learn how to meditate on a dog,

126
00:14:37,000 --> 00:14:39,000
saying dog, dog, dog.

127
00:14:39,000 --> 00:14:41,000
But then you're focusing on a picture of a dog.

128
00:14:41,000 --> 00:14:43,000
You're not focusing on the sound of the dog.

129
00:14:43,000 --> 00:14:45,000
You could actually, focusing on a dog bark,

130
00:14:45,000 --> 00:14:47,000
the sound of a dog barking, and say to yourself,

131
00:14:47,000 --> 00:14:51,000
dog, because in the mind there will arise a picture of a dog.

132
00:14:51,000 --> 00:14:54,000
So it is actually a valid meditation technique.

133
00:14:54,000 --> 00:14:56,000
It won't lead you to an understanding of reality.

134
00:14:56,000 --> 00:14:58,000
Why? Because it's not real.

135
00:14:58,000 --> 00:15:00,000
It's something that is arisen in your mind.

136
00:15:00,000 --> 00:15:02,000
It is concept of a dog.

137
00:15:02,000 --> 00:15:04,000
The reality of it is a vision in the mind,

138
00:15:04,000 --> 00:15:07,000
a sound of the ear, the experience,

139
00:15:07,000 --> 00:15:15,000
which really does occur.

140
00:15:15,000 --> 00:15:20,000
So the importance of using a word like hearing hearing hearing hearing

141
00:15:20,000 --> 00:15:23,000
is that it pairs away the concepts.

142
00:15:23,000 --> 00:15:25,000
Eventually, in the beginning it doesn't.

143
00:15:25,000 --> 00:15:27,000
In the beginning the mind is still caught up in concepts,

144
00:15:27,000 --> 00:15:29,000
still not really practicing correctly, but eventually it.

145
00:15:29,000 --> 00:15:31,000
The mind pairs away everything.

146
00:15:31,000 --> 00:15:34,000
Because again and again you're saying to it,

147
00:15:34,000 --> 00:15:36,000
no, it's not a dog. It's just hearing.

148
00:15:36,000 --> 00:15:38,000
No, it's not a dog. It's just hearing.

149
00:15:38,000 --> 00:15:40,000
But it's a dog. No, it's just hearing.

150
00:15:40,000 --> 00:15:42,000
But it's a nice dog. No, it's just hearing.

151
00:15:42,000 --> 00:15:45,000
But I like the sound. No, it's just sound.

152
00:15:45,000 --> 00:15:48,000
And eventually the mind says, oh yes, it's just sound.

153
00:15:48,000 --> 00:15:50,000
It's just hearing.

154
00:15:50,000 --> 00:15:52,000
So this is the importance of it.

155
00:15:52,000 --> 00:15:54,000
If you say dog, dog, dog, you'll never get to that point.

156
00:15:54,000 --> 00:15:57,000
You'll never pair away all of the liking and disliking

157
00:15:57,000 --> 00:16:00,000
and the concepts of it.

158
00:16:00,000 --> 00:16:02,000
Because you'll never get down to what is actually there.

159
00:16:02,000 --> 00:16:04,000
And the mind will never say, oh, it's just a dog.

160
00:16:04,000 --> 00:16:05,000
The mind will never say that.

161
00:16:05,000 --> 00:16:07,000
Because the mind will never, it will keep looking,

162
00:16:07,000 --> 00:16:09,000
where's the dog? Where's the dog?

163
00:16:09,000 --> 00:16:12,000
You can make it pretend, but it's like, okay, a dog.

164
00:16:12,000 --> 00:16:15,000
But it will never get this realization

165
00:16:15,000 --> 00:16:18,000
because that's not the experience.

166
00:16:18,000 --> 00:16:22,000
So yeah, very important to know the difference

167
00:16:22,000 --> 00:16:25,000
between concepts and reality, which is difficult.

168
00:16:25,000 --> 00:16:29,000
I think even just talking about it doesn't give it to most people.

169
00:16:29,000 --> 00:16:32,000
If you've never practiced intensive meditation,

170
00:16:32,000 --> 00:16:38,000
I've seen many people give their confusion on this subject.

171
00:16:38,000 --> 00:16:40,000
What do you mean this is just a concept?

172
00:16:40,000 --> 00:16:42,000
What do you mean this is just reality?

173
00:16:42,000 --> 00:16:44,000
It's true that if you've never really experienced it,

174
00:16:44,000 --> 00:16:48,000
it seems quite confusing and hard to tell the difference.

175
00:16:48,000 --> 00:16:53,000
But that's really just a sign that we are still dwelling in ignorance.

176
00:16:53,000 --> 00:16:56,000
For a person who's meditating, it's hard to understand

177
00:16:56,000 --> 00:16:59,000
how you could not tell the difference between

178
00:16:59,000 --> 00:17:02,000
what are you actually experiencing and the concept.

179
00:17:02,000 --> 00:17:06,000
It takes time, it takes the mind to settle down.

180
00:17:06,000 --> 00:17:11,000
It takes the mind to stop spinning around and stop adding

181
00:17:11,000 --> 00:17:14,000
our projections and our views onto everything

182
00:17:14,000 --> 00:17:17,000
and just seeing things as they are to be able to say,

183
00:17:17,000 --> 00:17:18,000
oh, this is really here.

184
00:17:18,000 --> 00:17:20,000
And all that stuff is not really here.

185
00:17:20,000 --> 00:17:24,000
All that stuff is arising in my mind.

