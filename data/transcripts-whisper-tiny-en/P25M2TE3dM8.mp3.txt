When I sit and say to myself sitting, I have to do a quick body scan, at least of my legs
and spine, in order to really know that I'm indeed sitting.
Isn't that quick body scan somewhat contrary to a one-pointed mind?
Is one label really enough?
You don't really have to do that.
I would say it's not really the object or exactly proper meditation, it's much more forceful.
We could in the beginning and you could say that it's at least calming the mind down or
focusing the mind.
You guys remember, nothing, none of that's wrong in tranquility meditation.
If your purpose is simply to calm the mind, you can force it, push it, need it, mold
it into shape.
Concentration meditation is not for the purpose of cultivating insight.
So in a sense, we can use that at least in the beginning and so a meditator and vipassana
meditation might calm themselves down by doing these body scans, by just knowing that
they're sitting.
In vipassana meditation, the point is not the sitting.
Sitting is a description of those feelings that you have.
The fact that you're aware of the tension in your back or the pressure on the floor, at
that moment when you say sitting, that's enough.
You're aware of that experience and you call that experience sitting.
It's simply important to recognize this is a physical phenomenon, it's not so important
to be aware that this is a sitting posture.
Put your mind on the whole body, the entire body, say to yourself sitting.
The recognition that that is a sitting posture comes on its own and comes unexpectedly.
It's the sunya part, the recognition that this is sitting.
Sunya is impermanent and so what you're seeing that leads you to say that you have to
do this in order to push it in order to come is that it's non-self.
It doesn't come automatically.
Sometimes it does.
You immediately know that you're sitting, you recognize, oh, this is a sitting posture.
Sometimes you look and look and there's no being, this is sitting, it just never are
aware that it's sitting.
This is because it's not self.
You can't force yourself to know that's what you're seeing and so that is the whole purpose
of practicing and that's what you're going to find.
You're going to find that sometimes you are aware of the feelings, sometimes you're
aware of this feelings, sometimes you're aware of that feeling, sometimes you're aware
of the recognition of the feeling and putting it together as sitting, sometimes you're not.
That's the point of the practice, not to perfect it.
This is key and this, I did a video on this that I think is most important, one that we should
emphasize again and again that this is often people, often meditators very often come to
the conclusion that they're practicing incorrectly or they're unable to practice because
specifically because they're seeing what we want them to see.
They're seeing impermanence, they're suffering, they're seeing non-self.
I would say much more important is the practice than perfecting it, perfecting it's not
going to happen, not in incitementation.
