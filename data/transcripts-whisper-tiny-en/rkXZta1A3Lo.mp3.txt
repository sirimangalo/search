Good evening everyone, welcome to another Wednesday session.
We come together, not just another session, we come together on this rare opportunity.
Of course we have this rare opportunity every week, but it can be misleading and don't realize
how rare an opportunity this is.
To get here tonight we all had to be born at this time.
A Buddha had to arise in the world, it's not every day or every period in history when you have
a Buddha, someone who understood the four normal truths and was able to teach them.
There's no reason to do it, there's always the reason to do it, but there's no path for understanding
it, why we do it, doesn't let us get what we want, it doesn't help us to get what we want,
it doesn't help us to fix our problems, it doesn't help us to control things, all of the
goals and ambitions in our life, doesn't help with any of those, you need someone who can explain
to us the truth of life, the truth that all of our wants, our desires and our attachments
and our delusions about control and me and mine, they're all the cause of suffering.
Teach us that it's not our experiences, it's our reactions, our interpretation, our perspective
on them, that makes us suffer, teaches us to see things just as they are, to not discard
our perception as being hopelessly bound with emotion and subjectivity, but to teach
us how to be objective, how to be equanimous with a clear mind, and this rare opportunity
Buddhism didn't start at the beginning of time, Buddhism started with the Buddha, only
2,500 years ago, and the way it's going now, it's not going to last so much longer,
it'll come a time when no one knows how to practice the Buddha's teaching, eventually
there will come a time when no one's even heard of the Buddha, or even knew what it was
he taught, not only that we have to be born as a human, there are many beings that are
fortunate enough to be here at this time, maybe even came in contact with the Buddha, but
they weren't able to understand the teachings, of course, 5 billion humans on the earth,
it's not that big, it's not that many compared to all of the animals and insects, all
the beings in hell, and the heavens who are ignorant, who are caught up in the pleasures
of heaven and so on, and we have to be physically connected to get here tonight, you have
to be physically able to be here, in many ways we make this ridiculously easy, and we have
to be easy, you don't have to be physically here in the room with me to hear my answers
to your questions, but you still need some physical connection, you need to have internet,
but device to connect to the internet, you need to have had physical connection that led
you to know about this session, but the most important good fortune we've all had to
be here is the presence of mind that wants to be here rather than somewhere else, if
taken time out of our busy lives, our lives that are so busy with work and with pleasure,
with distraction and obsession, ambition, worry, fear, so many distractions, and we've
decided to come here and discuss the Dhamma, so welcome everyone, you have my appreciation
that you're all here, tonight is a question and answer session for those of you who
are here for the first time, the beginning here you can post what you want in chat as long
as it's kind and thoughtful and mindful, you can post questions as well, but they'll come
a time where we're coming up to it, where I say stop and then we require the chat to
only be used for questions and everybody stop chatting and start focusing their attention
on their own experience and on questions if you have them, and you're going to ask questions
in chat, anything that's not a question, then we'll be deleted by Chris who is here to
help, and Chris will ask the questions, I will answer them, and you all can sit back, close
your eyes and just be with us here and now, and be with your own experience here and
now, so I'm ready to begin, Chris, if you're ready, I'm ready, let's begin.
No more chatting in the chat please, only questions regarding Q&A bad things caused by meditation,
how would one specifically cease the arising of a sense of a meditative self when in the
meditative practice, a meditative self, quite clear on what that means, but let me just
in general first of all, we don't specifically cease things, cease on their own accord, ceasing
the arising of things comes from wisdom, something will cease to arise, well they cease for
various reasons, they cease to arise when the causes and conditions for them to arise are no
longer produced, which is pretty obvious, right?
But that's the case with things like a sense of self, a sense of self won't arise when
the causes and conditions for that sense of self are no longer produced.
In regards to self, well we have a sense of self based on our perception of things,
our perspective, our perspective is based on the presumption that we are in charge, that
there is a self that is in control, that is experiencing every single experience, but
mindfulness teaches you otherwise, it teaches you that experience is our momentary, whatever
it is we are, it's a little more than a concept and the reality is, the reality continues
without any difference to our views or opinion, and we see reality just as it is, there's
no room or any reason for a sense of self in that sense no longer arises, but you don't
ever specifically do anything, you just generally and methodically, consistently cultivate
mindfulness that allows you to see things as they are, and you see things as they are,
the causes and conditions that are productive of bad things no longer arise because you've
seen them as bad as a cause for suffering and based on ignorance, based on delusion and
so on.
That comes naturally, there's no need or reason or benefit to trying to seek out what's
good and bad or formulate ideas of what's good and bad that will always be artificial
and unsustainable because it has no basis in your experience.
I often find myself noting wanting, wanting is a certain amount of restraint necessary
when applying the practice to daily life, for example, resisting the desire to eat too much.
It wouldn't say necessary, maybe helpful is a better way of looking at it, it's good
to have some kind of regimen or boundaries set for yourself that are reasonable because
as you know with things like dieting and so on, unreasonable demands are often easily
broken, leading to discouraged, to being discouraged.
So there are things like the eight precepts where you will need in the morning, but
wouldn't worry too much about resisting the desire to eat too much.
Ultimately the practice comes down to being mindful.
There are two sides, you know, there's the restraint and then there's the exertion, the
things you incline yourself to do and the things you restrain yourself from doing.
So most important is restrain yourself from killing, stealing, cheating, lying, drugs, alcohol,
that sort of thing.
There's lots of other things like gambling and gorging and even entertainment and sex is
good to take off the table if you can manage it, so it's up to you to find what precepts,
what regimen or rules you're comfortable keeping and you should have the idea that you
can slowly increase those, restrict those boundaries further and further based on your
practice, based on how far you come in your practice, which is separate, I've actually
seen that those things are bad for you and giving them up, so two sides are the practice.
I have an exam coming up tomorrow and I'm procrastinating, should I note everything that
I do to break free from my procrastination problem?
You should note everything, I mean you should note something all the time, it wouldn't
be too obsessed with one thing or another but it is good to note that you're procrastinating
because that's a sign that there's probably something that you're overlooking, failing
to note, often easy to note to the things that are not so meaningful, but the things
like procrastination which are meaningful, often easy to overlook, it's more comfortable
to not try to face them because it's habitual, so not taking note of having taken note
of that, yes, good to try and figure out what are the states of experience related
to procrastination, procrastination because it can be because you enjoy something else,
can be because you don't like what you do is that you have to do, very common read, that's
probably sums it up, it's liking or disliking, so noting those is a very good start and
you can note thoughts that lead to those things and I mean it's all in the practice already
you don't have to make a special case for things like procrastination, but again you
do have to take note of the weak spots, the things that you're probably not taking
seriously enough that you notice are getting slipping through the cracks and you're just
not confronting them the way that you should, but I wouldn't, sorry just one last thing,
I wouldn't break, try to ever try to break free or look at something as a problem, those
perspectives are not useful and can be problematic, harmful even, try and just note which
again, realizing that you've probably been missing some things, just to see clearly not
to fix or control, but to straighten, is it okay if the mantra seems still in the mind
as a result of fixating most of one's attention on the meditation object or should an
equal amount of attention be placed on clearly reciting the mantra?
Any question that starts like this is probably an improper way of looking at it, so if
the mantra seems dull, whatever that, I mean the mantra doesn't probably seem dull but
you have a dull feeling in your mind when you use the mantra, whatever that might be, then
you would note that, note that dullness, you could say dull but you should maybe say feeling
or something like that.
Second thing, it doesn't matter what something is a result of, don't pay too much attention
about that. Third, fixating your mind on something or trying to do things like you say
measuring something, somehow I'm not quite sure what the last part of this means, but
somehow measuring an equal amount doesn't mean anything either. I mean it's overthinking
the practice really, just do it, and as you do it you'll start to learn how to do it,
you'll see what you're doing wrong, you'll see how you're trying to control and fixate.
If you find yourself fixating, just note it, you find it dull, et cetera, just note that.
You don't have to add anything to the practice, we have to subtract all of our inclinations
and tendencies, but the misunderstanding is that something should be okay, we're not concerned
whether something is okay, it just is what it is, and if it is like that, you should
note it. How will I know wisdom, if and when I find it, how will I know I'm going in
the right direction? Wisdom just means clarity, I mean seeing things as they are.
You'll start to realize that you were seeing things poorly, not really seeing things at all,
you'll start to see how your mind works in ways that you never really paid much attention
to or enough attention to. That's wisdom, you'll see the nature of things, and that's
how you know you're going the right direction, basically it's one way of describing it,
maybe things more clearly.
Someone cut me off the other day, and I noted my anger and feelings, I could not get
the image of the driver out of my head, so I began loving kindness, is this wrong use
of loving kindness? No, that's fine, it's not as deeply profound as trying to be mindful
of the anger and the image. You did note the anger and the feelings, that's good, but
you should also note the image, because that confronts the reality that it's not under
your control, it helps you observe and appreciate this truth. The truth is that you
can't get the image out of your head. Normally we look at that as being a problem, it's
actually the nature of reality that we are not in charge of our minds. It's easy to dismiss
that and go on to try and fix things. By focusing and observing that repeatedly, you start
to teach yourself to be in tune with this reality that you're not in charge, that we can't
control. The best thing is just to say seeing, seeing, learning about your anger, learning
about your feelings, trying to understand how it all works and doesn't work and how it
causes stress and suffering and so on. Loving kindness is like best used for when it's
just overwhelming and you're just like, I can't even be mindful, this is too much. And it's
just best used in general, you know, try and use friendliness, cultivate these states as a
means of being friendly. Can one be mindful while indulging in entertainment, TV, games,
or are these inherently unmindful? Any mind that is described as indulging is not going
to be mindful. Is it possible to be mindful during those things, sure? Because it's
momentary, but you're going to have a much bigger problem with the addiction and enjoyment,
the addiction basically, the craving and clinging and needing, wanting, that you can't
be mindful at the same time. Those are delusion based states, you can be mindful of them
after they happen. It's probably a good way to deal with it, not the liking, not the wanting,
not the disliking when you don't go what you want, etc. Mindful is moments, so an activity
isn't mindful or unmindful. As you be more mindful, you'll start to see the nature of
your activities more clearly. You'll start to choose more positive, more beneficial activities.
How do I stop doubting so much about the teaching due to my materialist past? It distracts
me in my meditation and learning in the Dhamma. Well, it's an important lesson for you
there because you can't stop it. In fact, someone asks this sort of question when they
feel like they can't stop it. That's a part of the Dhamma, the realization that you can't
control your mind. In your materialist past, you probably used doubt as a weapon. You
probably used skepticism, thinking you were in charge of it. You know, there's nothing
wrong with healthy skepticism, of course. Even doubt can sometimes be, it's not a good
thing. Let's explain doubt in this case or let's explain good thing. Sometimes bad things
can be good things. They're not good things. But by good thing and Buddhism, we often
simply mean that which has a positive impact on the mind or is a positive influence or
is a positive quality of that mind. So negative qualities of mind can be indirectly helpful.
They're not directly helpful to that state of mind. But doubt, for example, can help
you realize that something's wrong. It's good if you believe in some of the crazy religious
theories that exist. Doubt is a catalyst for you to stop believing in those things.
Now, the doubt isn't good for the mind, but it's a good thing you gave rise to some doubt
otherwise you might just have blind faith. It actually isn't quite like that because
a person who has blind faith in anything, even the Buddha's teaching, is always going
to have doubt. It's not like you could ever avoid it. You can avoid it for some time. And
you can work hard and ignore it. But the important lesson here is that the doubt is not under
your control. Something that we build as a habit often. We doubt things that are true.
You'll probably find that you'll see something and realize something about yourself. And
in the next day, you might even doubt that you actually experienced that you did experience
it, something that you did experience. And then you'll doubt something you realized. And
this is why realizations and insights are not really the goal or the path. And we say,
oh, I had this profound realization that it just changed the way I think. It might find
that a week later you're doubting it and thinking maybe you were wrong and so on.
So the path, what is the path, what is the goal, is to see clearly how your mind works.
When you have the doubt, not trying to stop it, you're trying to see it clearly like
everything else. Part of seeing it clearly is realizing that, oh yeah, I see. I'm not really
in charge of this doubt. It's not really me who is doubting it. It can be a really ego
trip sometimes for a skeptic, you know, to doubt things. And you say, I don't believe
that. These silly people who believe these things are, or it might not even be in reference
to others. You can feel confident and proud of yourself. Good. I haven't lost my
doubt. I haven't lost my skepticism. Doubt is a crippling state of mind. It's not good
for you. Faith is a very powerful, positive mind state. Problem is we put faith in bad things
and so those very positive qualities of mind and that being used for very bad purposes.
It's very powerful how religion makes people believe certain things and when they believe
them they can do such horrible things in the name of that religion and they're very powerful
in doing it, meaning they don't ever waver from their horrible goal, horrible practice,
the killing and whatever. Because they have a very powerful mind. So if you can cultivate
faith, if you can have faith in your experience in yourself, well, not yourself, I guess,
in what you see, it can come to be reassured by repetitive observation. It's why we have
faith in science because they do experiments in airtight conditions. They have an airtight
methodology that just makes you have very little doubt and then they do it again and they
get the same result and you say that repetition, that makes me think that the first time
wasn't just a fluke. So when you do that in practice and you repeatedly observe, you
start to start to develop faith and confidence. But it comes from right from things like
doubt, even seeing that the doubt is not under your control, it gives you faith at least
in that which is no small thing, faith in the fact that faith in your observation that reality
is not really under our control. We're only causing stress and suffering by trying to
control and fix and direct our minds, whatever way we want them. When going about mundane
tasks mindfully, do you know the general activity or go in depth? For example, when
washing the dishes, do I note washing, washing, washing, or do I need to detail each
action? There's not really a hard and fast rule here. Washing, washing is good. If you're
doing it slow enough, it's better to note the individual actions, but really only if you're
doing it slow enough. If it's something you have to do very fast, there's probably not much
benefit from saying very quickly, like lifting, placing, moving, so you should try and do
things slower a bit, and that will give you the opportunity to really be present and clear,
but there's no harm and there's no, it's not useless to say to yourself, washing, washing.
It's just as you say a little more vague and general. It's still useful. Keeps your
present and keeps you objective, so washing is just washing. Is it okay to meditate five
minutes throughout the whole day? Plus staying mindful, of course. I don't mind. I'm not
going to stop you. Maybe you're asking, is it enough? Which is also hard to say because
then enough for what? Though you might say five minutes a day is probably not enough to
make any meaningful change, and it would be easy to get discouraged, so it's probably
not enough. That being said, even those five minutes, just think what it took for you to
even think about doing that. How courageous you had to be to decide that you were going
to face your mind and your experience for five minutes. That's a very wholesome positive
inclination. It's a good for you. It won't get you very far in a hurry, but it will change
your life goals, right? Changes your outlook on life when you start to include meditation.
As soon as you put those five minutes in your day, and when you make an intention to be mindful
during the day, it changed the way you look at things. So that's where something. It's not worth
nearly as much as doing say an hour a day. I should be a lot better. So trying, if you can,
try and work up to an hour a day. I don't know if you've done the at-home meditation course that
we offer, but you might consider trying that. It's good to have a teacher to help you with those
courageous intentions.
I find my mind perpetually reiterating by itself, impermanence, suffering, nonself. Is this wrong?
So the same answer as the person who asked, is this okay? Although in this case,
I don't remember what the other case, but in this case, you're likely the implication here is that
you're likely encouraging this or feeling good about it or liking it or thinking that it's a good
thing, and you shouldn't do that, or you should note if you're doing that, liking it or so on.
Because that's not the practice. If you're reiterating it, you can say saying or
thinking I guess is what it is. If you hear those words in your head, you might say hearing,
but it's most likely thinking and just say thinking. There's no great benefit to that.
There's a small benefit. Reminding yourself of that is a very basic sort of practice.
Something that kids could use. If you have kids, you can teach them that.
What I'm thinking of is that they have these prayer beads. When you flick a prayer bead,
you can say any tongue, dukang, anata, any tongue, dukang, anata, one, two, three, the beads.
It's not the practice, and it's not really that profound or useful, but it's a good basic
reminder. It's a great thing to have that sense, that that's the nature of reality, to align
your views in that way in a mundane sense, but it's no replacement for actual mindfulness,
and it's just a distraction or a crutch in the long term. It's going to impede your progress
as you rely on it. What role does quietness play in the meditation practice,
can it become a crutch? It can. There's nothing wrong with being in a quiet place,
but seeking out a quiet place, liking the quiet place. I assume you're talking about external
quiet, because if you're talking about internal quiet, it's more like a good result when your
mind starts to get more quiet, but you should still know that quiet, quiet, you should never
get clinging to it or anything like that, but the same goes with internal or external, don't cling to
anything. Nothing is worth clinging to. Things become a crutch when you start to rely on them,
and you start to gravitate around them, where it's unacceptable if they're not there, that kind of
thing. Something wrong with being in quiet all the time, even. You just have to cultivate the
qualities of mind that allow you to be mindful, irrespective of whether you're in quiet.
So in quiet, you can develop all those states, but you have to develop them so that when there
is loud noises, it's all the same to you, hearing, hearing. How does one properly note experiences
that start and stop within a short amount of time? It seems like one's mindfulness, either lapses
in between the experiences, or the mind becomes too agitated.
Well, if you mean that that are incessantly arising and ceasing, you don't have to note each
arising and ceasing. Just try and note in general what's happening. But just because something
ceases, it doesn't, I mean, it's always going to technically be that way. You'll note something
just after it's ceased. Anyway, so it's not a real problem. Just try and note it after it's
ceases. But if there's lots of them continuously, you don't have to note each and everyone, just note
generally. Something like once per second is the advice, but you shouldn't have a metronome or
anything. It's just that's a general guide for you to understand what we mean. Anything much quicker
than that is just going to just agitate you as you say. But if you feel agitated, note that as well.
My father is attempting to quit smoking and has been easily irritable. I feel stressed and upset
when he lashes out. How can I help him and myself with these feelings that arise? I mean, the simplest
way is to do some meditation. If you haven't read our booklet, read that. If you have, you might
be interested in doing the Atow meditation course. If you haven't done that. Once you've done that,
I mean, you become a good example to him. Whatever that's worth. It's often worth a lot to the
people who you're close to. When people are irritable and you're not irritated back, that's a great
change in your dynamic, right? That's going to change your world, change the course of your
relationship. And beyond that, I would say I would say in conjunction with that, once your
relationship starts to change, you might suggest to him that he try the meditation because it
really can help you quit smoking.
What method of meditation do you recommend for a beginner? We have a booklet that teaches a
beginner how to meditate. You can find it on our website. There should be a link in the description
and it's now on the screen. If you're really interested in learning more about the technique,
you can take our Atow meditation course, which is also on our website. Everything is free. We
don't charge for stuff. Don't worry about that. There's no catch either. It always
sounds like there must be something, right? When I first suggested, it's like, what's the cost?
And then when I say it's free, what's the catch? There isn't a catch. This is Buddhism or Buddhists.
The catches were doing it for herself. I'm doing it for me and not you. It's great for me to be
so helpful. It's great for Chris to be helpful. It's great for all the volunteers. It's a part of
our religious practice. I mean, Sikhs are a lot like this, I think. It's a part of their religious
practice to be helpful. Lots of Christians, Muslims, Jews, it's a thing religious people do.
It's one of the good things of religion. It's one of those things that brings religions together
because there's a lot of this in religion, whatever all the other bad stuff aside. One good thing
is those people in religion who try to help others and help themselves. Like really make themselves
better people. I have severe PTSD from sexual trauma I experienced with two former partners.
I think somehow things that happen to me are being encompassed. Is this possible?
I don't understand the word encompassed in this context. Do you understand that, Chris?
Without any more information, Bantay, I just assume it could be a question about Kama.
It sounds like repressed or something like that. I mean, the question isn't all that. I don't
know that question is the important question. The question here is this possible. It kind of
hints at the fact that you might be barking up the wrong tree because here we have.
Mindfulness shows you what's real. It's something that we don't
we aren't familiar with and it's hard to get a grasp of the fact that meditation is actually
going to give you knowledge. We're actually taught in modern scientific parlance to believe that
you can't actually know anything. You can only know that something isn't true. You can disprove
things, but knowledge is only the accumulation of evidence. But in meditation you come to know
things. It's very simple things. The reason why we overlook it is because the things you can know
are not very many. But you can know seeing. You can know that seeing is seeing. You can know that
hearing is hearing. You can know that stress is stress. Trauma is trauma. You can know your memories
or memories. You can know your emotions or emotions or feelings or feelings. Those are the
things you can know. When we look at what happens, what happens is limited to those basic building
blocks. So the things that happened to you and the things that are happening to you now
are the building blocks. Are these basic, simple realities? And mindfulness teaches you, gives you a
way, a method, a means of facing those experiences and understanding what's really going on.
What's going on behind this label of PTSD, sexual trauma? Because I mean, those are real,
but they're only real in a conceptual sense. You can conceive of having been sexually traumatized.
You can conceive of having PTSD. But you can't experience those things. What you're experiencing
is more simple, more basic. It's not to try to trivialize it. That's not the point. It's that
there's something more basic going on, basic building blocks that are not trivial that can be very
intense. But the intensity is only in the experiences, it's only in the moments. And you can face those
because they're not harmful. They are not dangerous. They are not the cause of suffering. The cause
of suffering is our incapacity. I mean, it's our improper interaction with those experiences
based on our incapacity to face them based on our misunderstanding of them and our
misunderstanding of the nature of reality and the way the mind works and how to deal with things
right by trying to control them or avoid them or fix them, etc. All of that is wrong.
Try to just see and face and understand. Read the booklet if you haven't. That might help.
But I can't really. That didn't really answer the question. Hopefully it helped point you in a
what I think might be a better direction. How can I break free from materialist or physicalist
thought patterns and beliefs? Is this the same person as before? I think it's the same answer as
before. I was just kind of joking. It doesn't really matter if it is. We don't try to break free
from things. You see, definitely recommend trying and the meditation practice.
Doesn't matter what kind of thought patterns they are, we don't try to break free from them.
This is your question could be generalized and the answer would be the same
because different people will ask this question in different ways. They'll say how do I break free
from PTSD? Thoughts relating to PTSD, right? Trauma that I experienced. How do I break free
from my desire to eat lots of food? How do I break free from my destructive thoughts and negative
thoughts and positive thoughts and how do I break free from my thoughts thinking too much?
That's not how this works. Try and see your thoughts as a rising and ceasing and it's much
more basic what happens. What happens is not oh I should stop thinking in this way. It's much more
like oh dear me, I'm thinking far too much and just thinking is just incessant and it's not
nearly as useful or beneficial as I thought. There's nothing to do with the content of the thoughts.
It's just the way I go about living my life is just all wrong I think too much.
In Thailand it's I think I think the type I think I think type people think don't think enough
in general it's a it's a gross generalization but I think there's often a
just following mindlessly sometimes that that can be discouraging but on the other hand
what what what I found in Thailand that everyone noticed about people like me is that we think
way too much and so it was a common thing that we'd hear kithmak you think you think a lot
think too much keep my good bye so that doesn't matter there's no judgment going on about whether
you think too much or not enough just watch and look and you'll see and you'll start to change
you'll start to probably think less especially you know if you it's not just a Thai western
thing or an eastern western thing it's I met a lot of people who are uneducated living I was living
in rural area but anyone who's who's who's greatly educated thinks a lot and that can be a real
hindrance towards seeing clearly because it distracts you
it's not doesn't mean you should hate it or be upset by it means you should
try to see more clearly than as you watch and see the thoughts more clearly you'll you'll
start to incline towards thinking less most likely hopefully some people will start to incline
towards thinking a little more means considering whether what they're doing actually has meaning
or whether it's just blind faith can you provide some examples of activities that promote a wholesome
state of mind as opposed to activities that promote addiction I'm unsure of what else to do
outside of meditation practice well if you're not a monk then there's lots of things you can do
you could find an occupation that is helpful to other people you could take up activities like
helping out at food banks helping out at a monastery you could join our volunteer group if you
want we got a people who are volunteering and they're finding things to do and what I said about
our volunteer group and I'm not I'm not sure how well it was received but I said it we shouldn't
look for things projects for people or projects and then and then try to find people who it's
not like we shouldn't look for people to do our projects right if we have a project and say okay
now we have to find people to do this it should be more there are people who want to do things
we should give them suggestions so we have these projects like make we're making a book and
we got audio podcasts now and stuff like that there's a group looking at building a monastery
we don't we don't need to make projects we need to fulfill the needs you know
and so if you if you can think of something good to do just do it you know find good things to do
if you live in a monastery as a monk or not as a monk it's much easier there's often routines
duties often it's a lot of meditation but if you don't have anything to do just do more
meditation because ultimately that's the top of the pile the top of the heap of wholesomeness is
mindful this so you wanted examples let me I didn't I don't know that I gave you so I did give
some but I wanted to talk a little more like like just be for example being charitable giving to
charity working at a charity being ethical taking taking precepts keeping precepts keeping the
eight precepts sometimes studying studying the Buddha's teaching reading the Buddha's teaching
going to groups that study the Buddha we have a study group on Saturday morning if you want to join
our group going online and engaging and asking questions and answering questions
there's a lot of a lot of activities that you can do there are some there's something called
the ten kusla kama pata which means the ten means of cultivating wholesomeness
and I'm sure I did a talk on it I'm not going to go into them here you can probably google them
and if you can't find them you can come on our discord we have a discord server that
I encourage everyone to consider joining if you're if you're if you're sincere and interested
can join your community and talk there
I sometimes worry I will die before enlightenment and the effort will be for not I know ultimately
that's selfish and not what this is all about but do you have any words of wisdom in this regard
I don't think it's selfish I'm not sure why you're using that word I mean I don't know what
you're going through but on face value it doesn't seem selfish it it seems you know it's
I mean worry is always going to be delusion so I could say it's delusional this is not a good
no in English that doesn't mean would I mean it to mean it's misguided because there's no
benefit to worrying right and it's also misguided because it won't be for not effort is not forever
for not it it creates upanishaya at the very least it's going to change the course of future lives
any practice you do in this life will propel you further and closer to Buddhism in the future
to enlightenment in the future so you can stop worrying for that reason but on a deeper level
worry isn't something you should try to get rid of per se it's something you should try to
understand so when you're worried just say to yourself worried worry
but there's nothing wrong with wanting to become enlightened before you die it can be distracting
if you fixate on it but it can also lead you to work harder we shouldn't be all we shouldn't be
complacent and thinking oh it's okay some lifetime I'll become enlightened I'll just do a
little bit of meditation no you should work as though your head was on fire because there's no
guarantee that you're gonna ever become enlightened for for all that I just said there's no guarantee
until you see the truth till you experience nibana for yourself
you never know what the future might bring but it will not be for not
it increases your capacity and potential your potential to eventually become enlightened
so focus as always focus more on the practice than on the expectations of what might come in the
future when you're worried like that just say worry but do consider to do meditation courses
and especially intensive courses where you can really really work on yourself
so I think we're done can point me to any you know any of the you know what question the important
questions if there none of those left just end it I have one more in this tier go for it would
one eventually stop using mantras to completely experience a selfless or ego detached state
I mean the answer basically is no but you're using words that I would
just probably use in a different I would put this sentence together a little differently as
all the answer is generally no and probably what you I mean may not even agree with but
at the very least don't understand the way I think I understand that the mantras are actually
conducive towards cultivating selflessness and what you call ego detached state
because the mantras fix the mind or focus the mind on the experience itself so the
mind states that come as a result are objective and impersonal that they don't make reference
to the soul or the self or ego so that's their very goal and purpose but that being said
and where I kind of hedged or hesitated a little bit on it is because you do eventually stop
using mantras and that's when you're experiencing Yibana when you when you experience the state
that is detached from not just ego but detached from everything during that state there is no mantra
that's when you stop all right thank you all great such another great great moment in our
history together sad you can all talk again say what you like in chat as long as it's kind
and thoughtful and well-intentioned
so I do it is good
scheduled for Saturday at 3 p.m. 3 p.m. Eastern time we always go by wherever I am in the world
I'm not sure if we could do it any other way well yeah it's just easiest for me Eastern time
maybe see some of you there have a good day everyone have a good night
