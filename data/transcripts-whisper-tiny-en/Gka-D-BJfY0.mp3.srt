1
00:00:00,000 --> 00:00:05,000
Okay, good evening. Welcome back to our study in the Dhamabana.

2
00:00:05,000 --> 00:00:14,000
Today we're looking at verse 171, which reads as follows,

3
00:00:14,000 --> 00:00:38,000
Come and look at this world decked out, just like a king's chariot.

4
00:00:44,000 --> 00:00:54,000
We're in the fool's fall, or get caught up in.

5
00:00:54,000 --> 00:01:05,000
Not too san gooey down at them, but the wise, there is no connection for the wise.

6
00:01:05,000 --> 00:01:15,000
It's one of these famous verses in the Buddhist world.

7
00:01:15,000 --> 00:01:21,000
So the story, not much of a story, it's in fact a repeat.

8
00:01:21,000 --> 00:01:34,000
And you wonder about these repeats, why it is that two people have the same story, and it's acknowledged that they have the same story.

9
00:01:34,000 --> 00:01:38,000
Not sure how that works.

10
00:01:38,000 --> 00:01:44,000
But the story is, the same as Santiti, Santiti was a minister of the king.

11
00:01:44,000 --> 00:01:49,000
And Advaya Kumar is actually a prince, the son of the king.

12
00:01:49,000 --> 00:01:56,000
But he pleased the king and the king gave him great reward.

13
00:01:56,000 --> 00:02:02,000
And part of the reward was a dancing girl.

14
00:02:02,000 --> 00:02:14,000
So I don't know how old Advaya was at the time, but it may have been his first experience.

15
00:02:14,000 --> 00:02:20,000
Maybe he was a teenager or something, and he assumed he was a little older than that.

16
00:02:20,000 --> 00:02:27,000
Maybe he was young, and he fell in love with this dancing girl,

17
00:02:27,000 --> 00:02:32,000
a woman who had a woman who had been hired to dance for him.

18
00:02:32,000 --> 00:02:44,000
And she, if it's the same in the story of Santiti and I guess in the story of Advaya as well.

19
00:02:44,000 --> 00:02:48,000
She didn't take good care of herself.

20
00:02:48,000 --> 00:02:58,000
It's sort of the strict regimen for dancing women they had to starve themselves, I think.

21
00:02:58,000 --> 00:03:03,000
She fastened for seven days to have a graceful body.

22
00:03:03,000 --> 00:03:06,000
It sounds a bit extreme, but there you have it.

23
00:03:06,000 --> 00:03:12,000
People do extreme things, and they're forced into extreme situations.

24
00:03:12,000 --> 00:03:18,000
And so she died, the same thing as with Santiti.

25
00:03:18,000 --> 00:03:22,000
And he grieved over that maybe it was a common thing.

26
00:03:22,000 --> 00:03:30,000
Because it was so common for them to starve themselves, that it was also a common thing for them to drop dead.

27
00:03:30,000 --> 00:03:37,000
Makes you wonder how the system continued when all these dancing women kept dying.

28
00:03:37,000 --> 00:03:44,000
But there you have it.

29
00:03:44,000 --> 00:03:53,000
And Advaya immediately thought he was grieved at the loss of this woman.

30
00:03:53,000 --> 00:03:57,000
And he immediately thought of the Buddha.

31
00:03:57,000 --> 00:03:58,000
He was the son of Bhimissara.

32
00:03:58,000 --> 00:04:03,000
Bhimissara was a great patron of Buddhism, a great supporter of the Buddha.

33
00:04:03,000 --> 00:04:12,000
One of the first famous lay disciples in that time.

34
00:04:12,000 --> 00:04:23,000
He was a great patron in the first major supporter of Buddhism in the world.

35
00:04:23,000 --> 00:04:32,000
In terms of providing a monastery, he was the one who donated the bamboo grove.

36
00:04:32,000 --> 00:04:36,000
And so immediately Advaya thought of the Buddha.

37
00:04:36,000 --> 00:04:38,000
And this is a common thing as well.

38
00:04:38,000 --> 00:04:48,000
When the going gets tough, it's common for ordinary people, ordinary in a neutral sense.

39
00:04:48,000 --> 00:04:55,000
People who had no special thoughts about religion or religious practice or meditation.

40
00:04:55,000 --> 00:05:00,000
It's a common thing for them to suddenly become exceptional.

41
00:05:00,000 --> 00:05:05,000
Because they do think about things that people normally don't think about.

42
00:05:05,000 --> 00:05:12,000
Someone who has suffered a great disturbance in their life is more likely to.

43
00:05:12,000 --> 00:05:29,000
Well, obviously more likely to consider a larger picture or consider a more nuanced or profound answer to life.

44
00:05:29,000 --> 00:05:35,000
If the going is, if it's going well, if everything is going well,

45
00:05:35,000 --> 00:05:46,000
it's more or less likely to or more likely to be content with a simple explanation or simple answers, a simple philosophy.

46
00:05:46,000 --> 00:05:52,000
One of the simplest is try your best to always get what you want.

47
00:05:52,000 --> 00:05:58,000
And of course, in cases like this, it becomes clear that that's not only not possible,

48
00:05:58,000 --> 00:06:00,000
but highly problematic.

49
00:06:00,000 --> 00:06:04,000
It's a cause of great stress and suffering.

50
00:06:04,000 --> 00:06:09,000
And so when you lose something that you hold dear,

51
00:06:09,000 --> 00:06:18,000
especially when that something is actually a someone which makes it so much more,

52
00:06:18,000 --> 00:06:24,000
stressful, more upsetting to put it lightly.

53
00:06:24,000 --> 00:06:38,000
When this happens, it's a real cause for reflection and questioning your outlook.

54
00:06:38,000 --> 00:06:43,000
Realizing that it's not really going to work.

55
00:06:43,000 --> 00:06:45,000
This is a bad choice of philosophy.

56
00:06:45,000 --> 00:06:47,000
And so you ask questions.

57
00:06:47,000 --> 00:06:57,000
So it's this sort of thing that obviously it happens, and it's a major cause for people to look towards religion

58
00:06:57,000 --> 00:07:04,000
or appropriately meditation and mental development,

59
00:07:04,000 --> 00:07:10,000
realizing that their minds are ill-equipped to deal with suffering.

60
00:07:10,000 --> 00:07:12,000
And so I'll buy a thought.

61
00:07:12,000 --> 00:07:16,000
Now's a good time to go see the Buddha.

62
00:07:16,000 --> 00:07:20,000
And the Buddha said to him,

63
00:07:20,000 --> 00:07:22,000
Yes, it's true.

64
00:07:22,000 --> 00:07:26,000
There's a great loss for a human being to lose another human being.

65
00:07:26,000 --> 00:07:30,000
And they held dear.

66
00:07:30,000 --> 00:07:35,000
But the truth is, this has been happening since time immemorial.

67
00:07:35,000 --> 00:07:43,000
Every, every lifetime we cry over lost.

68
00:07:43,000 --> 00:07:55,000
How many tears we shed if you think in terms of

69
00:07:55,000 --> 00:08:03,000
within the framework of Buddhist cosmology or theory,

70
00:08:03,000 --> 00:08:08,000
with the idea that we're reborn through infinity.

71
00:08:08,000 --> 00:08:13,000
No discernible beginning or end.

72
00:08:13,000 --> 00:08:20,000
Then the number of tears that we've shed is greater than all the water and all the ocean.

73
00:08:20,000 --> 00:08:23,000
Imagine that.

74
00:08:23,000 --> 00:08:26,000
Think of how many tears you shed in this life so far.

75
00:08:26,000 --> 00:08:29,000
How many tears have I shed so far?

76
00:08:29,000 --> 00:08:31,000
Wonder how much that would be.

77
00:08:31,000 --> 00:08:33,000
Would it feel a cup?

78
00:08:33,000 --> 00:08:36,000
Let's see.

79
00:08:36,000 --> 00:08:38,000
One cup of tears.

80
00:08:38,000 --> 00:08:39,000
How many?

81
00:08:39,000 --> 00:08:42,000
How much sadness does it take for one cup of tears?

82
00:08:42,000 --> 00:08:44,000
Probably more than that.

83
00:08:44,000 --> 00:08:48,000
Even in my short life.

84
00:08:48,000 --> 00:08:50,000
Probably more than a cup.

85
00:08:50,000 --> 00:08:52,000
Let's say one cup.

86
00:08:52,000 --> 00:08:53,000
I don't know.

87
00:08:53,000 --> 00:08:54,000
One just guess.

88
00:08:54,000 --> 00:08:57,000
The average person cries one cup.

89
00:08:57,000 --> 00:09:00,000
Does that seem like two, like not enough?

90
00:09:00,000 --> 00:09:01,000
Two cups?

91
00:09:01,000 --> 00:09:02,000
Ten cups?

92
00:09:02,000 --> 00:09:06,000
How many cups of tears do you cry in a lifetime?

93
00:09:06,000 --> 00:09:09,000
It's a very good question, I think.

94
00:09:09,000 --> 00:09:11,000
I mean, I don't think of it.

95
00:09:11,000 --> 00:09:17,000
But if you think, wow, those tears represent.

96
00:09:17,000 --> 00:09:19,000
It's not such an important question.

97
00:09:19,000 --> 00:09:29,000
But in the context, it's important because then

98
00:09:29,000 --> 00:09:33,000
you think of an ocean full of tears.

99
00:09:33,000 --> 00:09:38,000
How much sadness is required for a notion?

100
00:09:38,000 --> 00:09:41,000
How much sadness is required for a small pond?

101
00:09:41,000 --> 00:09:45,000
It's already quite impressive, but imagine an ocean of tears.

102
00:09:45,000 --> 00:09:46,000
How much sadness?

103
00:09:46,000 --> 00:09:51,000
And that's how much less than that still doesn't even approach

104
00:09:51,000 --> 00:09:56,000
the amount of sadness that we've gone through.

105
00:09:56,000 --> 00:09:59,000
A lifetime after a lifetime after a lifetime.

106
00:09:59,000 --> 00:10:02,000
Which isn't supposed to be depressing.

107
00:10:02,000 --> 00:10:05,000
It's in fact supposed to be liberating and make you realize,

108
00:10:05,000 --> 00:10:07,000
what are we doing?

109
00:10:07,000 --> 00:10:11,000
Is it really going to help me to keep doing this?

110
00:10:11,000 --> 00:10:14,000
Is this really the way to approach life?

111
00:10:14,000 --> 00:10:18,000
Crying about, well, not just crying about,

112
00:10:18,000 --> 00:10:23,000
but putting myself in a situation where I'm going to cry

113
00:10:23,000 --> 00:10:26,000
where I'm liable to tears.

114
00:10:26,000 --> 00:10:29,000
It was said basically this, you said,

115
00:10:29,000 --> 00:10:33,000
you've cried so much, lifetime after a lifetime.

116
00:10:33,000 --> 00:10:37,000
Over this woman, you've cried every lifetime.

117
00:10:37,000 --> 00:10:42,000
And then he says, only foolish people.

118
00:10:42,000 --> 00:10:47,000
And it's okay because we're all foolish people.

119
00:10:47,000 --> 00:10:49,000
That's how we started life.

120
00:10:49,000 --> 00:10:53,000
It's okay that this isn't meant to be critical.

121
00:10:53,000 --> 00:10:57,000
Or it's critical, but it's not something we should take personally.

122
00:10:57,000 --> 00:10:59,000
That's something he should take personally.

123
00:10:59,000 --> 00:11:02,000
He said, don't be a fool basically.

124
00:11:02,000 --> 00:11:05,000
Ma sochi, don't cry.

125
00:11:05,000 --> 00:11:11,000
Balan, jnanan, sun, sea, dhanan, tan, amethan.

126
00:11:11,000 --> 00:11:18,000
What does that mean?

127
00:11:18,000 --> 00:11:23,000
Sea of grief, right?

128
00:11:23,000 --> 00:11:25,000
Balan, jnanan, anyway.

129
00:11:25,000 --> 00:11:33,000
This sea of grief, ocean of grief is the realm of fools.

130
00:11:33,000 --> 00:11:36,000
And then he taught this verse.

131
00:11:36,000 --> 00:11:39,000
So it's a famous verse, I think.

132
00:11:39,000 --> 00:11:44,000
It's one that I know quite well.

133
00:11:44,000 --> 00:11:48,000
It's quite powerful, the imagery of the royal chariot.

134
00:11:48,000 --> 00:11:50,000
This is how we see the world.

135
00:11:50,000 --> 00:12:00,000
This is the epitome of intoxication, of investment, excitement.

136
00:12:00,000 --> 00:12:03,000
Get so invested in things.

137
00:12:03,000 --> 00:12:07,000
When we're born, we begin to make sense of the world.

138
00:12:07,000 --> 00:12:11,000
And then we're presented with all these bright and colorful

139
00:12:11,000 --> 00:12:19,000
and wonderful sensations, and we quickly learn pleasure and pain

140
00:12:19,000 --> 00:12:27,000
and are encouraged and latch onto the concept of clinging,

141
00:12:27,000 --> 00:12:37,000
the concept of chasing after, seeking out what will make you happy.

142
00:12:37,000 --> 00:12:41,000
So as we grow up, it's our toys.

143
00:12:41,000 --> 00:12:44,000
Remember a kid when you get your first toy.

144
00:12:44,000 --> 00:12:46,000
And then your next toy.

145
00:12:46,000 --> 00:12:50,000
And then when you hear that Santa Claus gives you toys.

146
00:12:50,000 --> 00:12:53,000
And then you send away a letter in this hole.

147
00:12:53,000 --> 00:12:59,000
Santa Claus thing again coming back to it's all rubbish.

148
00:12:59,000 --> 00:13:02,000
It's the wrong way of going about things.

149
00:13:02,000 --> 00:13:06,000
It's nice that we want to give things to our loved ones.

150
00:13:06,000 --> 00:13:13,000
We want to share with them, but really we should be much more concerned about

151
00:13:13,000 --> 00:13:19,000
helping each other and giving each other what we need and giving each other what we want

152
00:13:19,000 --> 00:13:32,000
or encouraging, especially in children, encouraging the desire for possessions.

153
00:13:32,000 --> 00:13:36,000
It's funny, you know I'm in Christmas isn't, it's all anyway.

154
00:13:36,000 --> 00:13:41,000
I don't want to get to a side track, but it's only a Christian concept, I think.

155
00:13:41,000 --> 00:13:44,000
So you go to either, like you go to Thailand, for example,

156
00:13:44,000 --> 00:13:50,000
and talk about Christmas, but they don't celebrate it.

157
00:13:50,000 --> 00:13:54,000
Not, well, they've started to a little bit, but not in the same way.

158
00:13:54,000 --> 00:13:59,000
And growing up Jewish, it was, you know, maybe you got some money for Hanukkah.

159
00:13:59,000 --> 00:14:08,000
Just because all the Christian kids were getting gifts, but anyway.

160
00:14:08,000 --> 00:14:19,000
The point and why this is interesting is because this cultivation and this encouragement and excitement

161
00:14:19,000 --> 00:14:26,000
over possession, over belongings, over the world, wanting this, wanting that.

162
00:14:26,000 --> 00:14:37,000
It starts with toys, games, and then we hit puberty and then it becomes a sexuality,

163
00:14:37,000 --> 00:14:42,000
you know, objects of sexuality, the human body.

164
00:14:42,000 --> 00:14:48,000
It comes at a huge attraction, and that's where we find a Baye.

165
00:14:48,000 --> 00:14:57,000
It fell into this realm of intense attraction that is, must be so ingrained in us.

166
00:14:57,000 --> 00:15:03,000
You know, think about how many times you've had sexual intercourse,

167
00:15:03,000 --> 00:15:06,000
a lifetime after, a lifetime after, a lifetime.

168
00:15:06,000 --> 00:15:14,000
How, how refined our sense of appreciation of sexual pleasure,

169
00:15:14,000 --> 00:15:19,000
or the sexual stimulation or physical, yeah, sexual stimulation,

170
00:15:19,000 --> 00:15:23,000
lifetime after, lifetime after that.

171
00:15:23,000 --> 00:15:27,000
That we just jump right into it as soon as the body's ready.

172
00:15:27,000 --> 00:15:33,000
It's like we're just waiting for puberty and then puberty comes okay, I remember this.

173
00:15:33,000 --> 00:15:43,000
It's like, it's like you've planted a plant and now it's grown fruit and now you can eat the fruit.

174
00:15:43,000 --> 00:15:51,000
Because we just jump right into it, immediately forming romances and falling in love.

175
00:15:51,000 --> 00:15:55,000
And, and being taught that this is life, this is the world.

176
00:15:55,000 --> 00:16:04,000
We teach each other, we teach our children, the society, our society is composed as a society survives.

177
00:16:04,000 --> 00:16:09,000
Our culture, our religion, our nationality depends upon it.

178
00:16:09,000 --> 00:16:13,000
People stopped having romance and sexual intercourse.

179
00:16:13,000 --> 00:16:23,000
You know, I mean, it, it helps us grow and populate and become more powerful.

180
00:16:23,000 --> 00:16:30,000
You know, they talk about having sons, having children, but it's always generally been sons.

181
00:16:30,000 --> 00:16:35,000
Because physical strength, I guess, was a thing.

182
00:16:35,000 --> 00:16:41,000
And so sons were generally more physically strong, whatever.

183
00:16:41,000 --> 00:16:47,000
And so it becomes a narrative that we teach ourselves.

184
00:16:47,000 --> 00:17:01,000
Yes, fall in love and get married and all these customs we have like marriage and so on.

185
00:17:01,000 --> 00:17:05,000
So we become caught up in so many ways.

186
00:17:05,000 --> 00:17:15,000
Romance and as with this story is the biggest one and becoming so attracted to two worldly pleasures.

187
00:17:15,000 --> 00:17:25,000
There are so many, some people become attached to their car and have this beautiful new car and then it gets in an accident.

188
00:17:25,000 --> 00:17:31,000
We can attach to our children how strong is the attachment of parents to their children.

189
00:17:31,000 --> 00:17:41,000
And then when you're a child, when your child changes, simply changes, not even get sick or dies.

190
00:17:41,000 --> 00:17:51,000
Or if that happens as well, that simply changes how angry, how hard it is for us to deal with when our children.

191
00:17:51,000 --> 00:18:00,000
No longer look up to us or respect us or if they become wicked and bad people.

192
00:18:00,000 --> 00:18:06,000
How hurtful it is, how hurtful it can be when our children are reckless, when our children hurt themselves,

193
00:18:06,000 --> 00:18:10,000
when our children refuse to follow the path we set out for them.

194
00:18:10,000 --> 00:18:13,000
How angry we become.

195
00:18:13,000 --> 00:18:18,000
We could talk for days about the ways we get caught up in the world.

196
00:18:18,000 --> 00:18:22,000
We could get caught up in these narratives and these stories.

197
00:18:22,000 --> 00:18:26,000
And often just caught up in the shininess of it all.

198
00:18:26,000 --> 00:18:28,000
How beautiful it all is.

199
00:18:28,000 --> 00:18:34,000
So in this case, it may very well have been the case that this, you have to ask,

200
00:18:34,000 --> 00:18:36,000
goodbye, did you really know this woman?

201
00:18:36,000 --> 00:18:37,000
It's interesting.

202
00:18:37,000 --> 00:18:39,000
We never hear the story of the dancing girl.

203
00:18:39,000 --> 00:18:45,000
Was she like, we don't hear anything about her personality,

204
00:18:45,000 --> 00:18:48,000
what is she? She was a woman who danced.

205
00:18:48,000 --> 00:18:51,000
That's what was important about her.

206
00:18:51,000 --> 00:18:54,000
That's all we hear.

207
00:18:54,000 --> 00:18:56,000
And so it's quite likely that that's all he knew.

208
00:18:56,000 --> 00:19:00,000
He didn't know her personality or her likes or dislikes.

209
00:19:00,000 --> 00:19:08,000
He was just caught up in, he was in love with her body most likely and her skill.

210
00:19:08,000 --> 00:19:17,000
In the way of dancing to tempt the eye and to be suggested to tempt the mind,

211
00:19:17,000 --> 00:19:23,000
there are ways of dancing that are seductive for the call.

212
00:19:23,000 --> 00:19:27,000
So he was seduced quite,

213
00:19:27,000 --> 00:19:43,000
abling by this woman.

214
00:19:43,000 --> 00:19:47,000
And so this is what this verse was about.

215
00:19:47,000 --> 00:19:53,000
So the basic lesson is talking about, I think it's important to talk about how we get caught up,

216
00:19:53,000 --> 00:19:55,000
how we fall into the world.

217
00:19:55,000 --> 00:19:57,000
Mired in it.

218
00:19:57,000 --> 00:19:59,000
So caught up.

219
00:19:59,000 --> 00:20:05,000
We get spun around and then we get old and wonder how we got here in the first place.

220
00:20:05,000 --> 00:20:11,000
But by then it's time to die and then we can do it all over again.

221
00:20:11,000 --> 00:20:17,000
And the second part of the lesson is,

222
00:20:17,000 --> 00:20:20,000
what's I think equally important,

223
00:20:20,000 --> 00:20:22,000
especially for us as meditators,

224
00:20:22,000 --> 00:20:27,000
is that the wise don't have any connection with the world.

225
00:20:27,000 --> 00:20:29,000
And that's a simple statement.

226
00:20:29,000 --> 00:20:36,000
You can take it in a conventional sense to just be a support of the Buddhist philosophy,

227
00:20:36,000 --> 00:20:37,000
not to cling.

228
00:20:37,000 --> 00:20:41,000
But I think there's an important,

229
00:20:41,000 --> 00:20:44,000
it's important to stress exactly how,

230
00:20:44,000 --> 00:20:51,000
I mean this takes special importance in Buddhism which places such a high emphasis

231
00:20:51,000 --> 00:20:54,000
on wisdom.

232
00:20:54,000 --> 00:20:59,000
And the point is that no, it's not that because you agree with the Buddha that it makes you wise.

233
00:20:59,000 --> 00:21:08,000
The point is that people that clinging to things is not just because

234
00:21:08,000 --> 00:21:12,000
you've decided to make that your life and

235
00:21:12,000 --> 00:21:14,000
or even decided that that's your path.

236
00:21:14,000 --> 00:21:16,000
It's that you don't understand it.

237
00:21:16,000 --> 00:21:22,000
We cling to things because we don't understand them.

238
00:21:22,000 --> 00:21:30,000
It's not that you haven't studied Buddhism or you have to learn that the Buddha said it's wrong.

239
00:21:30,000 --> 00:21:35,000
But a desire for a thing is based on a lack of understanding of that thing,

240
00:21:35,000 --> 00:21:41,000
proper understanding, objective understanding of Buddhist understanding.

241
00:21:41,000 --> 00:21:48,000
And so this is one way of describing the very core of

242
00:21:48,000 --> 00:21:51,000
Buddhist mental development.

243
00:21:51,000 --> 00:21:57,000
It's not to take on beliefs or views and it's wrong to cling.

244
00:21:57,000 --> 00:22:02,000
It's about studying objectively studying the things that you cling to,

245
00:22:02,000 --> 00:22:09,000
studying your clinging, studying your desire for things.

246
00:22:09,000 --> 00:22:13,000
And coming to the logical conclusion, which happens to be that they're not worth clinging to.

247
00:22:13,000 --> 00:22:17,000
There's nothing in the world worth clinging to.

248
00:22:17,000 --> 00:22:19,000
It's not something intellectual.

249
00:22:19,000 --> 00:22:25,000
It's something you have to spend time debating with yourself

250
00:22:25,000 --> 00:22:30,000
or analyzing rationally.

251
00:22:30,000 --> 00:22:33,000
It's something that you see quite clearly through the practice.

252
00:22:33,000 --> 00:22:42,000
Watching yourself engage in desire, watching yourself acquire the things you desire.

253
00:22:42,000 --> 00:22:46,000
Observing mindfully the process of clinging

254
00:22:46,000 --> 00:22:52,000
shows you without any doubt that it's not worth clinging to.

255
00:22:52,000 --> 00:22:57,000
This is what is meant by the wise to have no connection.

256
00:22:57,000 --> 00:22:59,000
It's not belief.

257
00:22:59,000 --> 00:23:00,000
It's not conviction.

258
00:23:00,000 --> 00:23:05,000
It's not effort on about pushing yourself so hard that you break free.

259
00:23:05,000 --> 00:23:08,000
It's not about breaking your habits.

260
00:23:08,000 --> 00:23:12,000
It's about studying your habits, analyzing them.

261
00:23:12,000 --> 00:23:22,000
It means methodically observing them again and again.

262
00:23:22,000 --> 00:23:31,000
If you're intent enough upon that sort of practice,

263
00:23:31,000 --> 00:23:35,000
you'll see for yourself the nature of reality,

264
00:23:35,000 --> 00:23:38,000
you'll become free from suffering.

265
00:23:38,000 --> 00:23:41,000
The idea is that it's not the world that's causing us suffering.

266
00:23:41,000 --> 00:23:45,000
It's not the loss of a loved one that causes us suffering.

267
00:23:45,000 --> 00:23:54,000
It's our reaction to our experiences.

268
00:23:54,000 --> 00:23:57,000
It's not even just our reaction.

269
00:23:57,000 --> 00:24:04,000
It's our setting ourselves up to react because that's the thing when sadness comes.

270
00:24:04,000 --> 00:24:09,000
You can't say, OK, I know this is going to be a bad thing,

271
00:24:09,000 --> 00:24:11,000
so I'm just not going to react this time.

272
00:24:11,000 --> 00:24:13,000
You can't do that.

273
00:24:13,000 --> 00:24:18,000
No, no, this thing I loved, yes, reacting to its loss would be bad.

274
00:24:18,000 --> 00:24:20,000
I'm not going to do that.

275
00:24:20,000 --> 00:24:23,000
It doesn't even work that way.

276
00:24:23,000 --> 00:24:25,000
It'll become overwhelmed by it.

277
00:24:25,000 --> 00:24:28,000
That's the nature of clinging.

278
00:24:28,000 --> 00:24:33,000
The learning process is learning this truth.

279
00:24:33,000 --> 00:24:35,000
Clinging is not to your benefit.

280
00:24:35,000 --> 00:24:38,000
It doesn't lead to greater good.

281
00:24:38,000 --> 00:24:45,000
It leads only to stress and suffering.

282
00:24:45,000 --> 00:24:57,000
That's the benefit of what a wisdom a wise person doesn't suffer.

283
00:24:57,000 --> 00:25:04,000
Quite simply, they free themselves in no more tears.

284
00:25:04,000 --> 00:25:07,000
They free themselves from suffering.

285
00:25:07,000 --> 00:25:09,000
So that's the demo part of her tonight.

286
00:25:09,000 --> 00:25:38,000
Thank you all for listening.

