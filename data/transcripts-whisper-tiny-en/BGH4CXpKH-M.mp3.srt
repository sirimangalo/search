1
00:00:00,000 --> 00:00:06,200
Hello, if one is committed to the teravada tradition but only has the possibility to join

2
00:00:06,200 --> 00:00:10,640
Mahayana, Vajrayana, meditation centers in the area.

3
00:00:10,640 --> 00:00:15,800
Should he or she continue practicing along the teravada lines of practice or join the

4
00:00:15,800 --> 00:00:21,040
center nevertheless?

5
00:00:21,040 --> 00:00:26,680
I don't think that it's an either or, of course, you can continue practicing teravada

6
00:00:26,680 --> 00:00:34,040
and still attend a Buddhist center in a different tradition.

7
00:00:34,040 --> 00:00:39,560
If it's in your area, I know this question comes up a lot because teravada Buddhist

8
00:00:39,560 --> 00:00:49,360
centers aren't as prevalent as those from Tibet or Zen or however, but you'd be surprised

9
00:00:49,360 --> 00:00:52,880
how often you find a sympathetic ear at some of these places.

10
00:00:52,880 --> 00:00:59,640
So first of all, you might find a teacher who is versed in the practice that you've

11
00:00:59,640 --> 00:01:00,640
done.

12
00:01:00,640 --> 00:01:12,080
In fact, we're attending a meditation group in the Tibetan tradition, but the head llama

13
00:01:12,080 --> 00:01:14,680
was teaching from the Visudimanga.

14
00:01:14,680 --> 00:01:20,800
Now that may be a little bit rare, but he was quite amenable to our practice and actually

15
00:01:20,800 --> 00:01:26,600
came and did a meditation, the head llama came and did a meditation course with me and

16
00:01:26,600 --> 00:01:32,320
was looking to learn to teach the meditation practice in this tradition.

17
00:01:32,320 --> 00:01:42,160
But even that not being the case, I think it's quite possible for you to attend these

18
00:01:42,160 --> 00:01:48,320
centers or at least ask permission to use these centers.

19
00:01:48,320 --> 00:01:55,400
I know some places, it depends on the place, some can probably be quite xenophobic or controlling

20
00:01:55,400 --> 00:02:01,560
and will maybe even forbid you from practicing other meditations in their center, but

21
00:02:01,560 --> 00:02:07,200
if you're just sitting quietly alone, I know at least in the teravada tradition, people

22
00:02:07,200 --> 00:02:12,720
are generally quite amenable to have you use their center as a place to practice your

23
00:02:12,720 --> 00:02:15,120
own teaching.

24
00:02:15,120 --> 00:02:23,760
So you can go along with it and join in on the group, chanting or whatever, and keep

25
00:02:23,760 --> 00:02:27,920
up your own practice.

26
00:02:27,920 --> 00:02:32,800
I mean, I guess it's obviously not a replacement for a teacher in the tradition that

27
00:02:32,800 --> 00:02:38,720
you're following, but there's certainly no harm in it, and often you'll meet people of

28
00:02:38,720 --> 00:02:46,200
like mine and you'll find that other people who attend these centers may have experience

29
00:02:46,200 --> 00:02:51,480
and in the practice that you do in a similar situation where they're just looking for

30
00:02:51,480 --> 00:02:59,160
a center, but they actually practice something else and so you'll often find the sympathetic

31
00:02:59,160 --> 00:03:06,880
here and community, which is important, you'll find something there.

32
00:03:06,880 --> 00:03:11,440
So definitely I recommend people go to these centers, check them out, and you'll find

33
00:03:11,440 --> 00:03:16,920
that they vary in quality, some of them might be quite authentic and some might be simply

34
00:03:16,920 --> 00:03:18,920
commercial ventures and so on.

35
00:03:18,920 --> 00:03:45,760
So check it out and for sure I would recommend that sort of thing.

