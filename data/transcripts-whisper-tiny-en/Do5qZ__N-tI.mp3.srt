1
00:00:00,000 --> 00:00:05,000
Why do Muslims seem to dislike us so much?

2
00:00:05,000 --> 00:00:13,000
I've tried to explain to a Muslim friend that we do not worship idols, but it seems to fall on deaf ears.

3
00:00:13,000 --> 00:00:18,000
It's actually the two issues here.

4
00:00:18,000 --> 00:00:25,000
I guess the implication that you're making is that Muslims dislike us because we worship idols.

5
00:00:25,000 --> 00:00:30,000
And Muslim people are very much against worshiping idols.

6
00:00:30,000 --> 00:00:32,000
Well, you know what?

7
00:00:32,000 --> 00:00:35,000
The problem is we do worship idols.

8
00:00:35,000 --> 00:00:38,000
Sad to say,

9
00:00:38,000 --> 00:00:41,000
but a lot of Buddhists do worship idols.

10
00:00:41,000 --> 00:00:45,000
They think of the Buddha as a God,

11
00:00:45,000 --> 00:00:50,000
and more especially when they don't think of the Buddha as a God.

12
00:00:50,000 --> 00:00:56,000
But there are people who wear Buddha images around their necks,

13
00:00:56,000 --> 00:00:59,000
thinking that it's going to protect them.

14
00:00:59,000 --> 00:01:01,000
And that's an understatement.

15
00:01:01,000 --> 00:01:02,000
Scott, you know this, I think.

16
00:01:02,000 --> 00:01:05,000
I think this is all familiar to you.

17
00:01:05,000 --> 00:01:19,000
That there are people out there who pay millions of dollars for a single small Buddha image.

18
00:01:19,000 --> 00:01:24,000
Yeah, I'm going to get to that.

19
00:01:24,000 --> 00:01:27,000
Actually, it is quite a generalization.

20
00:01:27,000 --> 00:01:29,000
But don't worry, I'm not going to let it.

21
00:01:29,000 --> 00:01:35,000
We will get there.

22
00:01:35,000 --> 00:01:42,000
So, I mean, there is a valid criticism there that the Sri Lankan people,

23
00:01:42,000 --> 00:01:48,000
they look down upon this in a good thing.

24
00:01:48,000 --> 00:01:54,000
They say, you don't wear a Buddha image around your neck.

25
00:01:54,000 --> 00:01:55,000
That's an image of the Buddha.

26
00:01:55,000 --> 00:01:57,000
And the Thai people are all like, yeah,

27
00:01:57,000 --> 00:02:00,000
let's buying and selling these Buddha images.

28
00:02:00,000 --> 00:02:03,000
And they have this way of feeling them.

29
00:02:03,000 --> 00:02:05,000
You touch the Buddha image with your thumb.

30
00:02:05,000 --> 00:02:08,000
There was this German man who claimed to be able to do it.

31
00:02:08,000 --> 00:02:13,000
So he was showing me touching the Buddha image.

32
00:02:13,000 --> 00:02:17,000
And he was saying, this one is powerful.

33
00:02:17,000 --> 00:02:18,000
This one is not powerful.

34
00:02:18,000 --> 00:02:19,000
This one has this kind of power.

35
00:02:19,000 --> 00:02:20,000
This one is that kind of power.

36
00:02:20,000 --> 00:02:23,000
It's a Thai people have this word kang kang.

37
00:02:23,000 --> 00:02:30,000
I think it is kang, which means powerful kang.

38
00:02:30,000 --> 00:02:33,000
And so they know which one is powerful and which one is not.

39
00:02:33,000 --> 00:02:35,000
Nowadays, I don't even know if it's still a fad,

40
00:02:35,000 --> 00:02:41,000
but when I was in Thailand, there was this disgusting fad of...

41
00:02:41,000 --> 00:02:43,000
It wasn't even Buddha images anymore.

42
00:02:43,000 --> 00:02:48,000
It was this angel called Jetukam.

43
00:02:48,000 --> 00:02:54,000
Jetukam, which is a fictitious being that apparently that is supposed to...

44
00:02:54,000 --> 00:02:55,000
It was just made up.

45
00:02:55,000 --> 00:02:57,000
There's even a story about where it was made up.

46
00:02:57,000 --> 00:03:04,000
It was made up based on a Hindu based on not even exactly,

47
00:03:04,000 --> 00:03:09,000
but based on a Hindu deity.

48
00:03:09,000 --> 00:03:16,000
That's supposed to guard this J idea, this pagoda in Thailand.

49
00:03:16,000 --> 00:03:19,000
And it became this huge fad.

50
00:03:19,000 --> 00:03:23,000
And people got rich off of these things.

51
00:03:23,000 --> 00:03:30,000
Because people can be so dumb and would buy them.

52
00:03:30,000 --> 00:03:35,000
So even monasteries became quite wealthy by having special edition.

53
00:03:35,000 --> 00:03:37,000
It was like those pong.

54
00:03:37,000 --> 00:03:39,000
You remember pong?

55
00:03:39,000 --> 00:03:43,000
Those little bottle caps?

56
00:03:43,000 --> 00:03:46,000
Collecting pong?

57
00:03:46,000 --> 00:03:49,000
I think it was called pong.

58
00:03:49,000 --> 00:03:52,000
You know, it became the vegan collector's items.

59
00:03:52,000 --> 00:03:54,000
And they were these round dits.

60
00:03:54,000 --> 00:03:56,000
They looked like pong, actually.

61
00:03:56,000 --> 00:03:57,000
And you would collect them.

62
00:03:57,000 --> 00:04:02,000
And so they had special edition and all of this garbage.

63
00:04:02,000 --> 00:04:04,000
People say the same about Buddha images.

64
00:04:04,000 --> 00:04:11,000
They have this huge ceremony to bless and to

65
00:04:11,000 --> 00:04:13,000
christen a Buddha image and make it powerful.

66
00:04:13,000 --> 00:04:19,000
And they put all sorts of spells over it and all sorts of stuff.

67
00:04:19,000 --> 00:04:26,000
Why I'm even talking about this is because there's really a point that has to be made.

68
00:04:26,000 --> 00:04:28,000
Yeah, I'm really kind of beating around the bush.

69
00:04:28,000 --> 00:04:31,000
I'm sorry, but there's a point that has to be made here.

70
00:04:31,000 --> 00:04:37,000
Is that the...

71
00:04:37,000 --> 00:04:38,000
Well, they have...

72
00:04:38,000 --> 00:04:44,000
There's really a point there because the Buddha never taught us to make images of anything.

73
00:04:44,000 --> 00:04:46,000
You know, he wasn't critical.

74
00:04:46,000 --> 00:04:49,000
It was like, yeah, big deal, make an image, don't make an image.

75
00:04:49,000 --> 00:04:53,000
The question ever came up.

76
00:04:53,000 --> 00:05:00,000
There's a famous story and one that we always tell is that the first Buddha images

77
00:05:00,000 --> 00:05:03,000
were Greek.

78
00:05:03,000 --> 00:05:12,000
The records that we have of Buddhist India are that they never made Buddha images.

79
00:05:12,000 --> 00:05:16,000
They would never put the Buddha, and in fact they avoided it.

80
00:05:16,000 --> 00:05:20,000
So when they would want it to tell a story and they would inscribe it on rocks,

81
00:05:20,000 --> 00:05:22,000
the kings would do this.

82
00:05:22,000 --> 00:05:25,000
Instead of putting the Buddha, they would put a wheel of the dhamma,

83
00:05:25,000 --> 00:05:30,000
they would put a Bodhi leaf, or they would put just an empty.

84
00:05:30,000 --> 00:05:34,000
You know, when they want to show the Buddha on the Bodhisattva,

85
00:05:34,000 --> 00:05:38,000
writing this horse, they would put just the empty horse.

86
00:05:38,000 --> 00:05:41,000
Because they would never...

87
00:05:41,000 --> 00:05:44,000
It was never done.

88
00:05:44,000 --> 00:05:53,000
And so the point, the point I'm trying to make is that...

89
00:05:53,000 --> 00:05:58,000
We really have a problem there, and that we really are worshipping idols.

90
00:05:58,000 --> 00:06:02,000
And that should really stop.

91
00:06:02,000 --> 00:06:08,000
And of course the defense that's always used that I haven't used is that

92
00:06:08,000 --> 00:06:13,000
we use the Buddha image in order to recollect the Buddha,

93
00:06:13,000 --> 00:06:18,000
which I think can be a valid excuse.

94
00:06:18,000 --> 00:06:27,000
But it sets you up for a lot of danger, especially when dealing with people who believe

95
00:06:27,000 --> 00:06:35,000
grave and images are sin and punishable by death or so.

96
00:06:35,000 --> 00:06:41,000
So okay, so let's get back, try to get back, relate this back to the question.

97
00:06:41,000 --> 00:06:50,000
Because not all Muslims dislike us.

98
00:06:50,000 --> 00:07:03,000
But it is true, I think, that there is definitely a bad feeling between

99
00:07:03,000 --> 00:07:08,000
what is essentially an atheistic religion.

100
00:07:08,000 --> 00:07:16,000
And the atheistic religions.

101
00:07:16,000 --> 00:07:22,000
I mean Hinduism is, I guess, an exception because in Hinduism they don't exactly worship the gods.

102
00:07:22,000 --> 00:07:29,000
They interact with them, and there's general belief that one can become God.

103
00:07:29,000 --> 00:07:35,000
Judaism also doesn't have so much trouble because in Judaism they can argue with God.

104
00:07:35,000 --> 00:07:42,000
And God isn't, God is very much a part of the universe.

105
00:07:42,000 --> 00:07:48,000
But he doesn't play this central role in what it means to be Jewish.

106
00:07:48,000 --> 00:07:51,000
So Jewish people can often come in practice meditation.

107
00:07:51,000 --> 00:07:56,000
But I think there's no beating around the bush that Muslims, people who are

108
00:07:56,000 --> 00:08:04,000
practicing Muslims, do have a problem with just as Christians do with people who don't believe

109
00:08:04,000 --> 00:08:07,000
in God.

110
00:08:07,000 --> 00:08:18,000
And that's aggravated when we take some graven image or idol, as you say, an idol,

111
00:08:18,000 --> 00:08:25,000
as a substitute.

112
00:08:25,000 --> 00:08:27,000
So then that's the ultimate blasphemy.

113
00:08:27,000 --> 00:08:33,000
It's like putting a piece of rock on the same level as the most important thing in the universe,

114
00:08:33,000 --> 00:08:36,000
in their God.

115
00:08:36,000 --> 00:08:44,000
So regardless of, I mean, obviously it's a silly thing to do for us to be paying so much attention to these images.

116
00:08:44,000 --> 00:08:49,000
But there's a very valid point there that it's going to get us into a lot of

117
00:08:49,000 --> 00:08:54,000
heat with those people who feel very strongly with God unnecessarily.

118
00:08:54,000 --> 00:09:01,000
I think, and why this is actually an important point is because this is part of what helped wipe out Buddhism in India.

119
00:09:01,000 --> 00:09:03,000
And Muslims came to Indian.

120
00:09:03,000 --> 00:09:10,000
It wasn't just because they were Muslims, but because of their militaristic sort of people.

121
00:09:10,000 --> 00:09:13,000
But Muslim Islam told them that this was wrong.

122
00:09:13,000 --> 00:09:23,000
And so it was one of the first things to go with these idol worshipers, which were the Buddhists who had their Buddha images.

123
00:09:23,000 --> 00:09:29,000
Hinduism was much better, much more like a chameleon, much better to fit, easier to fit in,

124
00:09:29,000 --> 00:09:31,000
and much harder to wipe out.

125
00:09:31,000 --> 00:09:40,000
The Buddhism is pretty easy to wipe out, especially when you have these Buddha images and temples and so on.

126
00:09:40,000 --> 00:09:45,000
If monks were just people wearing rags living in the forest,

127
00:09:45,000 --> 00:09:48,000
I think it'd be a lot harder to wipe them out.

128
00:09:48,000 --> 00:09:58,000
But when they're sitting ducks, getting fat off of donations living in opulent golden palaces,

129
00:09:58,000 --> 00:10:04,000
not a hard target to hit.

130
00:10:04,000 --> 00:10:12,000
And I think that can be extrapolated to a more modern example.

131
00:10:12,000 --> 00:10:22,000
Buddhism is not making much headway on a monastic level into the rest of the world.

132
00:10:22,000 --> 00:10:32,000
Because Buddhists, when they build a Buddhist monastery, first of all, they call it a temple and they have their priests.

133
00:10:32,000 --> 00:10:42,000
And second of all, the way they go about it, for instance, time on the stairs, they build it out of gold, as much gold as possible.

134
00:10:42,000 --> 00:10:50,000
And rich, beautiful carpets and huge Buddha images and lots of ceremonies and so on.

135
00:10:50,000 --> 00:11:00,000
It's an affront really to people's sensibilities.

136
00:11:00,000 --> 00:11:05,000
People go there and they're certainly not looking for that.

137
00:11:05,000 --> 00:11:11,000
It's making it very difficult for us to bring Buddhism to people by...

138
00:11:11,000 --> 00:11:19,000
It's not just overkill, it's total misrepresentation of the Buddha's teaching because gold has no place in the monastery.

139
00:11:19,000 --> 00:11:23,000
A monk can't even touch gold.

140
00:11:23,000 --> 00:11:26,000
Touching gold is an offense.

141
00:11:26,000 --> 00:11:38,000
Touching jewels, touching anything, any precious material like that is an offense.

142
00:11:38,000 --> 00:11:43,000
So it's a real danger.

143
00:11:43,000 --> 00:11:52,000
And I think the Buddha images are a part of that. We don't need Buddha images to spread Buddhism.

144
00:11:52,000 --> 00:11:56,000
The Dhamma, which was spread Buddhism.

145
00:11:56,000 --> 00:12:03,000
And yes, you have the excuse that it helps us to remember the Buddha and think about him.

146
00:12:03,000 --> 00:12:07,000
So I'm not saying that a Buddha image is totally negative.

147
00:12:07,000 --> 00:12:21,000
But our obsession with them and our placing them, because if in that case, why not have a Buddha image placed somewhere in its own place?

148
00:12:21,000 --> 00:12:31,000
Instead of putting it up in front of an altar for you to worship and give donations to it and so on.

149
00:12:31,000 --> 00:12:39,000
You'll have it up as a reminder or something that you can go and look at.

150
00:12:39,000 --> 00:12:43,000
It's certainly not the most important thing.

151
00:12:43,000 --> 00:12:52,000
The Buddha's root pakaya was not his most important kaya, his most important body.

152
00:12:52,000 --> 00:13:00,000
I think that I had something else to say, but I think that's saying quite a bit already.

153
00:13:00,000 --> 00:13:07,000
But there's the danger in the Buddha image.

154
00:13:07,000 --> 00:13:11,000
It's really in a front to people who believe other things.

155
00:13:11,000 --> 00:13:17,000
And that's really the point is we don't have to be so obvious about who we are.

156
00:13:17,000 --> 00:13:24,000
As I said, a monk living in the forest really is not a threat to anyone.

157
00:13:24,000 --> 00:13:32,000
They still might go out and kill them because, oh, these people don't believe in God.

158
00:13:32,000 --> 00:13:39,000
But there's not so much of a problem there.

159
00:13:39,000 --> 00:13:44,000
The other part of the answer, I guess that I should say is it's because these people have wrong views.

160
00:13:44,000 --> 00:13:53,000
And when people have wrong views, they tend to be, what to say, judgmental, but here I am judging them.

161
00:13:53,000 --> 00:13:57,000
But as people hold on to views, they believe this is right and nothing else is right.

162
00:13:57,000 --> 00:14:03,000
And so they're apt to criticize anyone who just disagrees.

163
00:14:03,000 --> 00:14:06,000
They're not just criticized. They might even persecute.

164
00:14:06,000 --> 00:14:08,000
And it's not just the Muslims.

165
00:14:08,000 --> 00:14:14,000
The Christians were horrible about it. The Spanish Inquisition, the Crusades.

166
00:14:14,000 --> 00:14:16,000
It has to do with belief.

167
00:14:16,000 --> 00:14:22,000
The stronger you believe, the less tolerant you are of people who disagree.

168
00:14:22,000 --> 00:14:27,000
So the word atheist is just a shock to people.

169
00:14:27,000 --> 00:14:53,000
It's a four-letter word.

