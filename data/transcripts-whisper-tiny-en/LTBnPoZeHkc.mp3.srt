1
00:00:00,000 --> 00:00:10,800
Hello, Bhante, Buddhism is a religion that is made to make one individual the path to enlightenment.

2
00:00:10,800 --> 00:00:15,280
How can it be that you don't hear about Buddhists that become enlightened, Arahat, or

3
00:00:15,280 --> 00:00:17,280
Arabuddha, thanks?

4
00:00:17,280 --> 00:00:20,600
Well, you're not talking to the right people, I guess.

5
00:00:20,600 --> 00:00:26,440
There's a lot of people out there who do talk about people becoming enlightened, Arahats,

6
00:00:26,440 --> 00:00:33,440
Arabuddha, but I imagine in Israel you wouldn't hear so much, but the other thing, I mean

7
00:00:33,440 --> 00:00:36,800
the obvious answer here is that people don't talk about it, and enlightened being is

8
00:00:36,800 --> 00:00:41,480
incredibly disenclined to talk about their own enlightenment.

9
00:00:41,480 --> 00:00:51,200
Even the Sotapana is someone who has just begun on the path to enlightenment has given up

10
00:00:51,200 --> 00:01:04,800
that idea, and is incredibly disenclined towards bragging or announcing their enlightenment.

11
00:01:04,800 --> 00:01:09,360
So even Buddhists don't hear about it, and we generally speculate about our teachers and

12
00:01:09,360 --> 00:01:10,360
so on.

13
00:01:10,360 --> 00:01:14,280
And often that speculation goes overboard and suddenly everybody's in Arahat, everybody's

14
00:01:14,280 --> 00:01:16,640
enlightened.

15
00:01:16,640 --> 00:01:20,640
The reason you don't hear about people becoming Buddha is because it's apparently

16
00:01:20,640 --> 00:01:23,480
not possible at this point.

17
00:01:23,480 --> 00:01:26,640
I think it would be impossible for two reasons.

18
00:01:26,640 --> 00:01:33,800
One is that the Buddha's sassana is still here, and two, so the world is not currently

19
00:01:33,800 --> 00:01:37,800
capable of supporting a private Buddha, in particular.

20
00:01:37,800 --> 00:01:44,000
And two, a perfectly enlightened Buddha could not arise in this age.

21
00:01:44,000 --> 00:01:45,000
This is theory.

22
00:01:45,000 --> 00:01:49,440
I mean, it may all be wrong, I think it's just what the commentaries say, but the theory

23
00:01:49,440 --> 00:01:54,960
is that there's too many defilements, and their life span is too short at this point

24
00:01:54,960 --> 00:01:56,200
for a Buddha to arise.

25
00:01:56,200 --> 00:01:58,320
The Buddha would not arise at this time.

26
00:01:58,320 --> 00:02:05,720
The other thing is, first of all, we have a prophecy of when the next Buddha is coming,

27
00:02:05,720 --> 00:02:11,200
and we have an understanding that it's a very rare thing for a Buddha to arise.

28
00:02:11,200 --> 00:02:15,520
So the chances, even without that prophecy of a Buddha arising in this day and the

29
00:02:15,520 --> 00:02:20,320
nature, or any time soon, it's very, very rare because we just had one.

30
00:02:20,320 --> 00:02:22,240
And there's some kind of gravity involved, I would say.

31
00:02:22,240 --> 00:02:26,080
I would speculate that anybody who was even close to becoming a Buddha had to have been

32
00:02:26,080 --> 00:02:28,840
drawn towards this Buddha.

33
00:02:28,840 --> 00:02:33,920
Like Mahakataya and I think Mahakataya and I think, if I remember, or it was Kasypa, one

34
00:02:33,920 --> 00:02:38,640
or the other, was destined to become a Buddha, had made a determination to become a Buddha,

35
00:02:38,640 --> 00:02:39,640
and gave it up.

36
00:02:39,640 --> 00:02:44,800
I can't remember what she wanted was, say, bad teacher, I don't know my stuff.

37
00:02:44,800 --> 00:02:53,120
But one of them, and they gave it up when they met the Buddha, because they realized

38
00:02:53,120 --> 00:02:57,080
that it was much better for them to become enlightened here and now, and pass on the

39
00:02:57,080 --> 00:03:22,200
teachings, help spread the teachings here and now.

