1
00:00:00,000 --> 00:00:07,680
uses the word, no, like, we pass in a means, or pass in as a word to use when you see something,

2
00:00:07,680 --> 00:00:14,000
when you look at something and you see it, but what's useful about it, of course,

3
00:00:14,000 --> 00:00:20,160
even in some ways more than wisdom, is that it reminds us that this isn't intellectual,

4
00:00:22,080 --> 00:00:26,720
you have to see with wisdom, but you have to see for yourself another verb the body uses is,

5
00:00:26,720 --> 00:00:40,080
such a karate, such a karate means to see for oneself, to know independently of belief or

6
00:00:40,880 --> 00:00:49,600
rationalization or logic, to really know for oneself, to experience really.

7
00:00:49,600 --> 00:01:01,920
But so what do we mean when we talk about we pass in, what it refers to, and so in brief,

8
00:01:01,920 --> 00:01:06,320
it refers to seeing the three characteristics, impermanence, suffering, non-self.

9
00:01:08,160 --> 00:01:14,880
This is what we pass in a meditation should show you, and it's the way it's phrased

10
00:01:14,880 --> 00:01:24,000
often without much explanation. You can make this the teaching a little confusing.

11
00:01:24,000 --> 00:01:53,840
It's not like you're going to look, and you're going to continue to...

