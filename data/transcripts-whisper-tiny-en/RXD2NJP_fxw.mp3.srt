1
00:00:00,000 --> 00:00:04,500
It's easy to understand experience, may continue after death.

2
00:00:04,500 --> 00:00:16,000
However, how can we know where experience continues, such as in heaven or a hell, or the animal realm, etc.

3
00:00:18,000 --> 00:00:24,000
Well, it seems it's most like, I mean, I appreciate that you've said it in this way.

4
00:00:24,000 --> 00:00:39,000
I mean, it's nice to hear that there are actually people who are getting it, getting this point that it doesn't seem unreasonable, whether you've experienced past memories or past lives or so on or not.

5
00:00:39,000 --> 00:00:47,000
It seems to be quite in line with experience and reality to consider that the mind continues on.

6
00:00:47,000 --> 00:00:55,000
Knowing as we do that, that reality is a good based on experience.

7
00:00:55,000 --> 00:01:16,000
So, it seems also quite logical and reasonable to suggest that it's going to continue wherever, in whatever state, that most, in whatever state most describes the nature of the mind.

8
00:01:16,000 --> 00:01:27,000
So, if the mind is full of greed, then one would expect to have some consequences of that, if the mind is full of anger, full of delusion or so on.

9
00:01:27,000 --> 00:01:36,000
I haven't even talked before on the Buddhist idea of where people go based on their acts and their deeds.

10
00:01:36,000 --> 00:01:46,000
If a person, and so I can go over this as well, if a person is full of anger, this is what leads them to be born in hell because there is a great amount of suffering in the mind.

11
00:01:46,000 --> 00:01:53,000
So, the mind becomes on fire and in some sort of hellish state.

12
00:01:53,000 --> 00:02:01,000
The mind is full of greed, one is in a wanting state, one ends up in a state of ultimate wanting as a ghost.

13
00:02:01,000 --> 00:02:15,000
This is where one is most likely to arrive, a state of want, the state of need, these ghosts that wailing and clinging to things, hunting a house because they're very much attached to the house and to the people and so on.

14
00:02:15,000 --> 00:02:30,000
If the mind is very much full of delusion, then one is most likely to be born as an animal because the mind is clouded and deluded and in a state of...

15
00:02:30,000 --> 00:02:41,000
only able to appreciate the very simple aspects of life, like eating, sleeping and sexual intercourse, fighting and so on.

16
00:02:41,000 --> 00:02:47,000
And so as a result, one is most likely to be born as what we call animals.

17
00:02:47,000 --> 00:02:57,000
These beings that are only able to appreciate the bear basic, basic, simplest experiences of life.

18
00:02:57,000 --> 00:03:06,000
Now, if a person is able to clear their mind to the extent that they're not saying or doing things based on...

19
00:03:06,000 --> 00:03:20,000
If they're able to be free from greed, anger and delusion to a certain level to the extent that they're not saying or doing things based on them, so their mind is pure to that extent, that is what allows one to be born as a human being based on morality.

20
00:03:20,000 --> 00:03:35,000
If they're able to keep their minds clear to the extent that they're able to control their physical and verbal acts, so they don't kill, they don't steal, they don't lie, they don't cheat and so on.

21
00:03:35,000 --> 00:03:52,000
If they're born as humans. Now, in the human realm, it's further subdivided. One is born rich or poor based on one's charity, one's...

22
00:03:52,000 --> 00:04:07,000
That's the wrong word. This desire to see gain, to have people get things, impresses on the mind this idea of getting this idea of receiving.

23
00:04:07,000 --> 00:04:23,000
And so, as a result, one is born rich. People who are born wise because they have gone out and asked questions and seeked out, sought out wise people.

24
00:04:23,000 --> 00:04:28,000
People who are born stupid because they don't, people who are born poor because they didn't give and so on.

25
00:04:28,000 --> 00:04:40,000
People who are given to killing have a short life because they're fixated on the idea of ending life and so as a result, when they're born as a human later on they will be born.

26
00:04:40,000 --> 00:04:46,000
If they're able to still be born as a human, they will be born with a short life.

27
00:04:46,000 --> 00:04:58,000
If they're given to torturing or hurting other beings, then when they're born as a human, if they're born as a human, they'll be born sick with poor health.

28
00:04:58,000 --> 00:05:14,000
How are we doing for? If they're born in high society, it's because they were humble and unconceded and they were given to respecting other people, respecting mother, respecting father, respecting teachers.

29
00:05:14,000 --> 00:05:26,000
So, as a result, they have this idea of respecting their mind. So, they're born in a high society or in a high position. People who are born in a low position is because they are not.

30
00:05:26,000 --> 00:05:30,000
People who are born famous.

31
00:05:30,000 --> 00:05:38,000
They can't remember all of them. There's all together eight. I got through six, didn't they? I think I got through six. I got through a bunch, five.

32
00:05:38,000 --> 00:05:48,000
Anyway, it's in the julakama vibangasut, I think, or maha, julakama vibangasut, there's two of them. That should be read.

33
00:05:48,000 --> 00:05:54,000
This should be memorized in a way that I haven't, obviously.

34
00:05:54,000 --> 00:06:00,000
But those are ways that you're born. That's in a human realm. Then, to be born as an angel, it's because of great goodness.

35
00:06:00,000 --> 00:06:11,000
Mahak was alive. A person is given to doing great good deeds, profound and exalted deeds, like giving up all of their possessions,

36
00:06:11,000 --> 00:06:20,000
or devoting their life to helping others and to dedicating themselves to goodness. They become angelic, so they're born as an angel.

37
00:06:20,000 --> 00:06:29,000
If a being is given over to an intensive tranquility or transcendental meditation, then they're born as a God because their mind is so.

38
00:06:29,000 --> 00:06:38,000
Exalted and universal that they're born and exalted in all encompassing state of Godhood.

39
00:06:38,000 --> 00:06:45,000
And if one practice we pass in a meditation, then there's no clinging, and so one passes away. There is no rebirth.

40
00:06:45,000 --> 00:06:51,000
That's where the mind goes.

41
00:06:51,000 --> 00:07:01,000
There you go.

