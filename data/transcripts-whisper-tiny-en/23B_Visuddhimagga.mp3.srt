1
00:00:00,000 --> 00:00:05,040
By the words, beginning with the disappearance of perceptions and so on, it's meant what?

2
00:00:08,320 --> 00:00:14,640
To all sense non-attention to all sense fear consciousness, to all come ourjira consciousness.

3
00:00:17,280 --> 00:00:24,800
So, in fact, when a person enters into the Aruba Vajrajana, there are no

4
00:00:24,800 --> 00:00:32,400
rupa Vajra consciousness and no kama Vajra consciousness. So, somehow in some and not

5
00:00:32,400 --> 00:00:40,320
paying attention to some, he enters into the immaterial jana. So, when he is in the immaterial

6
00:00:40,320 --> 00:00:46,720
jana, there are no rupa Vajra janas and also no kama Vajra consciousness. I mean, sense fear

7
00:00:46,720 --> 00:00:57,680
consciousness. Actually, when one gets to the, gets the Aruba Vajra jana, then the rupa Vajra

8
00:00:57,680 --> 00:01:09,840
janas just up here. So, this is a place and boundary space and this is not difficult to understand

9
00:01:09,840 --> 00:01:20,800
because it is called unbounded because it is neither an end as it's arising or end as it's

10
00:01:20,800 --> 00:01:26,480
disappearing, no arising, no disappearing. That is why it is called boundless.

11
00:01:29,840 --> 00:01:34,880
He enters 24, paragraph 24. He enters upon and draws in the basis,

12
00:01:34,880 --> 00:01:40,080
which is based consisting of boundless space. Now, we must examine this because

13
00:01:41,760 --> 00:01:47,200
based consisting of boundless space is not an accurate translation. What do you mean? What do you

14
00:01:47,200 --> 00:01:57,360
understand by the based consisting of boundless space? It really means first Aruba Vajra jana

15
00:01:57,360 --> 00:02:08,640
and it's concomitant. So, please look at the notes. The first Aruba Vajra jana is called

16
00:02:09,840 --> 00:02:19,120
Arkasana jana, it's a long, long name. So, that word is composed of two parts Arkasana jana

17
00:02:19,120 --> 00:02:32,320
and Aryata jana. So, Arkasana jana and the paragraph sign. My printer cannot print design,

18
00:02:32,320 --> 00:02:39,760
what do you call that? Smaller than or derived from. So, I use that in my computer but the printer

19
00:02:39,760 --> 00:02:53,840
doesn't print it. So, it should be what's on this? Let's get it. It is derived from, it means.

20
00:02:53,840 --> 00:03:01,120
So, Arkasana jana is derived from Arkasana jana. This is the polygrama. So, Arkasana jana

21
00:03:01,120 --> 00:03:06,800
and Arkasana jana have the same meaning, namely, home boundless space. Now, Aryata jana. The

22
00:03:06,800 --> 00:03:15,040
word Aryata means a base in the sense of habitat here. So, Jana jita and its concomitant

23
00:03:15,040 --> 00:03:21,920
are called Arkasana jana because they have boundless space as their habitat. That means

24
00:03:21,920 --> 00:03:28,880
as their object. Not the base but something that has the base. That is what is meant by the word

25
00:03:28,880 --> 00:03:37,040
Arkasana jana. So, the translation is not accurate. The base consisting of boundless space.

26
00:03:37,040 --> 00:03:47,280
Actually, something which has boundless space as a base and base here means object.

27
00:03:47,280 --> 00:03:59,280
So, this is similar to the two senses in which you talked about sana being both.

28
00:03:59,280 --> 00:04:09,680
Yes, because the word Arkasana jana belongs to a kind of compound where the compound

29
00:04:09,680 --> 00:04:15,520
would signify something other than those denoted by the individual words but qualified

30
00:04:15,520 --> 00:04:23,760
by them, thereby denoting possession, habitation and so on. This is a polygrama. The individual

31
00:04:23,760 --> 00:04:32,160
words mean something and the compound word means another thing connected with them but not them.

32
00:04:33,360 --> 00:04:39,120
So, the 29 pounds I think that is a good example. So, there is a city called 29 pounds in Southern

33
00:04:39,120 --> 00:04:46,320
California. Why is that city called 29 pounds? Because there are 29 pounds there.

34
00:04:46,320 --> 00:04:52,880
Maybe there are more than 29 pounds now but that is another thing. So, the city called 29

35
00:04:52,880 --> 00:05:00,080
pounds means a city where they are at 29 pounds. So, the word 29 pounds does not mean 29 pounds

36
00:05:00,640 --> 00:05:08,800
but it plays a city where they are at 29 pounds. So, 29 is 29 and pounds is pumped

37
00:05:08,800 --> 00:05:19,920
but when these words denote or use as a compound, they do not mean 29 pounds but it plays

38
00:05:21,840 --> 00:05:29,120
which has where they are at 29 pounds. In the same way, Arkasana and Ayadana are here.

39
00:05:29,120 --> 00:05:34,960
Arkasana and Ayadana means boundary space and Ayadana means base but when they are

40
00:05:34,960 --> 00:05:41,440
compounded and when they become a compound now, they do not mean boundary space as a base

41
00:05:41,440 --> 00:05:47,920
but they mean something which has boundary space has a base and base here means object.

42
00:05:49,440 --> 00:06:01,280
So, it is not a base but something which has that base. That is important. So, we will have to

43
00:06:01,280 --> 00:06:10,640
understand that whenever we see base consisting of boundary space because the other trees are

44
00:06:10,640 --> 00:06:19,200
the three genres are also the same. So, not the base but something that has the base

45
00:06:22,480 --> 00:06:28,560
and the word Ayadana here is sometimes it means a habitat

46
00:06:28,560 --> 00:06:36,240
where something is born or where something is found originated

47
00:06:37,040 --> 00:06:43,040
like the base for the deities. You know that the base for the deities but what is that?

48
00:06:47,280 --> 00:06:55,440
Actually, it means in our countries we offer some things to the deities. I mean

49
00:06:55,440 --> 00:07:03,200
the spirits and then we build houses for them. So, that is called the Ayadana or just

50
00:07:03,200 --> 00:07:09,040
a shrine. A shrine for all deities is called the Ayadana in Pali. So, Ayadana is that.

51
00:07:09,600 --> 00:07:13,520
Happy that means just a place for him. They also live in trees.

52
00:07:13,520 --> 00:07:26,240
Oh yes, they do. Is that called a spirit house? Yes, it is a spirit house something like that.

53
00:07:26,560 --> 00:07:36,800
He enters the born and dwells in that they are not difficult. So, in the paragraph 24

54
00:07:37,520 --> 00:07:42,320
about the middle of the paragraph that boundary space is the base in the sense of habitat

55
00:07:42,320 --> 00:07:49,200
for the Jana. Whose nature is it to be associated with it? That is not accurate.

56
00:07:50,080 --> 00:07:56,720
So, we should strike out those words whose nature it is to be associated with it.

57
00:07:56,720 --> 00:07:59,840
And then replace them with other words.

58
00:08:02,080 --> 00:08:08,240
With its associated states. So, the sentence you read

59
00:08:08,240 --> 00:08:16,320
in the sense of habitat for the Jana. With its associated states.

60
00:08:18,560 --> 00:08:26,400
You will find these words in paragraph 49. So, please go to paragraph 49

61
00:08:26,400 --> 00:08:43,920
and then you will see them there.

62
00:08:47,920 --> 00:08:51,440
Beginning? Yeah, with its associated states.

63
00:08:51,440 --> 00:09:01,840
So, the Pali word is the same. So, here he misunderstood the Pali word Dhamma.

64
00:09:03,920 --> 00:09:08,560
I have told you that the word Dhamma is very difficult to translate and you cannot translate

65
00:09:08,560 --> 00:09:14,480
that word with one word every time you meet it. So, Dhamma here means just the Dhamma,

66
00:09:14,480 --> 00:09:21,520
the states, mental states or sometimes maybe physical states. But here mental states.

67
00:09:21,520 --> 00:09:29,760
But he took Dhamma to mean nature here. But it does mean nature here. Sometimes it means nature

68
00:09:29,760 --> 00:09:36,560
in the words like, why are Dhamma, Sankara? That means the formations have the nature of

69
00:09:36,560 --> 00:09:44,320
this integrating the nature of the sphere. In that context, the word Dhamma means nature.

70
00:09:44,880 --> 00:09:51,120
But here, Sankara is your familiar with Abirama.

71
00:09:52,960 --> 00:09:59,440
Reading Pali books. You will be familiar with this word. So, Sankara means associated states.

72
00:09:59,440 --> 00:10:06,640
So, the Jana with its associated states is all Akasana and Jajajana. So, not only Jana, but with

73
00:10:07,440 --> 00:10:11,760
the associated states also are called Akasana and Jajajana.

74
00:10:17,200 --> 00:10:25,200
Now, the second one, after the base consisting of boundless consciousness, we will have to

75
00:10:25,200 --> 00:10:37,360
modify that too. So, when you want to get the second Arubav Jajana, you find for the first Arubav

76
00:10:37,360 --> 00:10:42,640
Jajana and so on. And then you practice meditation again and then you get second Jana.

77
00:10:42,640 --> 00:10:48,080
So, the text is by completely surmounting the base consisting of boundless space that means

78
00:10:48,640 --> 00:10:55,040
by completely surmounting the first Arubav Jajana. And then you practice unbounded consciousness

79
00:10:55,040 --> 00:11:00,960
and then end as a born and was in the base consisting of boundless consciousness. That means

80
00:11:04,400 --> 00:11:11,600
it is a description of second Arubav Jajana. So, the second Arubav Jajana takes first Arubav Jajana

81
00:11:11,600 --> 00:11:18,320
check the S object. That is why here unbounded consciousness and bounded consciousness.

82
00:11:18,320 --> 00:11:30,160
So, you want to surmount the first Arubav Jajana. But you dwell upon it, you pay attention to

83
00:11:30,160 --> 00:11:34,960
you and say unbounded consciousness and bounded consciousness. And then you surmount it.

84
00:11:34,960 --> 00:11:49,600
Now, the second, and here the word is Winyan and Jajana. So, please look at the notes.

85
00:11:51,120 --> 00:12:01,440
Winyan and Jajana. So, Winyan and Jajana and Jajana. So, Winyan and Jajana comes from Winyan and Jajana.

86
00:12:01,440 --> 00:12:08,560
And that also is a compound of Winyan and Jajana. And then Jajana means boundless

87
00:12:08,560 --> 00:12:13,520
and Winyan means consciousness. So, boundless consciousness. Here boundless consciousness means

88
00:12:13,520 --> 00:12:21,440
the first Arubav Jajana. Now, Jajana means the base in the sense of habitat. Here, Jajana

89
00:12:21,440 --> 00:12:26,640
Jajana and it's concomitant are called Winyan and Jajana because they have boundless first Arubav

90
00:12:26,640 --> 00:12:34,400
Jajana as they are habitat or as they are object. So, it is not not the base, but it is the Jajana

91
00:12:34,400 --> 00:12:39,120
and it's concomitant which are called Winyan and Jajana.

92
00:12:39,120 --> 00:12:58,400
And then, paragraph 30, for it is said in the Vibanga and bounded consciousness. He gives attention to

93
00:12:58,400 --> 00:13:04,080
that same space provided by consciousness. He provides boundlessly hands and bounded consciousness is

94
00:13:04,080 --> 00:13:13,280
said. But in that passage, Tang Yewa, Agasang, Winyan and Vibinyan inapudan, the instrumental

95
00:13:13,280 --> 00:13:21,120
case by consciousness must be understood in the sense of accusative. Although, although the case

96
00:13:22,320 --> 00:13:29,440
is instrumental, the meaning is not instrumental, the meaning is accusative.

97
00:13:29,440 --> 00:13:42,560
So, actually, what is meant here is not space provided by consciousness, but consciousness

98
00:13:42,560 --> 00:13:48,960
by reading the space, that is the actual meaning you have to take. So, literally translated,

99
00:13:48,960 --> 00:13:55,920
it means space provided by consciousness, but the actual meaning here is

100
00:13:55,920 --> 00:14:04,160
consciousness by reading the space, because this second Argo Jajana takes the first consciousness,

101
00:14:04,160 --> 00:14:12,640
Argo Jajana consciousness as object, not space as object. So, here by consciousness must be understood

102
00:14:12,640 --> 00:14:29,280
in the accusative sense. That means, he gives attention to consciousness which provides

103
00:14:29,280 --> 00:14:44,800
the space, or providing the space. So, and then Winyan and Jajana explain here, but should understand,

104
00:14:44,800 --> 00:14:52,080
according to the explanation I have given. Now, the third one, nothingness, now we come to

105
00:14:52,080 --> 00:15:01,840
nothingness. The third one takes the absence or nothingness of the first as an object.

106
00:15:03,280 --> 00:15:07,840
After getting the second, he wants to go on to the third. So, in order to get the third

107
00:15:07,840 --> 00:15:18,720
Jajana, he breaths upon the absence of the first Argo Jajana. When he gets the second Argo Jajana,

108
00:15:18,720 --> 00:15:28,560
the first is gone. So, after getting the second Argo Jajana, the first is said to be non-existent.

109
00:15:29,200 --> 00:15:36,320
So, when he wants to get to the third Argo Jajana, he takes that absence or nothingness of

110
00:15:36,320 --> 00:15:42,800
the first Argo Jajana as object, and that is conceptualized, nothingness,

111
00:15:42,800 --> 00:15:53,520
it is real nothingness, not like a void of permanency, happiness, and so on. In many places,

112
00:15:54,880 --> 00:16:01,760
when the word void or sumia is used in pari, it means it is void of permanency and so on.

113
00:16:02,400 --> 00:16:11,520
But here, the word used is void also or secreted and so on. But actually, what is

114
00:16:11,520 --> 00:16:18,880
man here is that nothingness, that non-existent or absence of the first Argo Jajana.

115
00:16:20,080 --> 00:16:29,840
So, going on the first absence of the first Argo Jajana, he says the void void or secreted,

116
00:16:29,840 --> 00:16:35,360
secreted. So, he drops upon in again and again, and then he casts the third Argo Jajana.

117
00:16:35,360 --> 00:16:45,840
So, there is a similar here, suppose it means it is a community of breakthroughs and so on,

118
00:16:47,360 --> 00:16:54,160
and then the text and commentary. By completely surmounting the base consisting of boundless

119
00:16:54,160 --> 00:16:59,360
consciousness, that means the second Argo Jajana, aware that there is nothing, he enters

120
00:16:59,360 --> 00:17:08,240
upon and verse in the base consisting of nothingness. By surmounting the base consisting of

121
00:17:08,240 --> 00:17:12,880
boundless consciousness, here to the Jajana is called the base consisting of boundless consciousness

122
00:17:12,880 --> 00:17:20,080
in the way already stated and its object is also so called two. So, here boundless

123
00:17:20,080 --> 00:17:32,880
consciousness means the Jajana as well as the object of Jajana, and it is the base in the sense

124
00:17:32,880 --> 00:17:40,640
locality of these species as Cambodia is the base of Hodges. Cambodia is a country maybe in the

125
00:17:40,640 --> 00:17:52,720
northwest of India, and good Hodges are said to Argo, he raised there. So, you get good Hodges

126
00:17:52,720 --> 00:18:03,840
when you go to Cambodia. So, it is like nowadays, what do you say about Hodges? Well,

127
00:18:03,840 --> 00:18:14,880
what about Hodges? It is a very expensive Hodges. Arabian Hodges? Sometimes, sometimes, but the

128
00:18:14,880 --> 00:18:22,720
thoroughbreds that are internal. But from the country they come, where they could be found,

129
00:18:22,720 --> 00:18:39,920
Arabian Hodges or something like that. So, Cambodia is a country or a place where good Hodges are

130
00:18:39,920 --> 00:18:53,680
raised, where good Hodges come from. So, in the same way here, the word base or I Jajana is used.

131
00:18:53,680 --> 00:19:00,320
Now, the third Argo Jajana and its accompaniment is called I King Jajana, Jajana. So,

132
00:19:00,320 --> 00:19:10,160
on the seat down to the bottom, I King Jajana and I Jajana. I King Jajana comes from I King Jajana.

133
00:19:11,440 --> 00:19:18,960
Now, the word King Jajana is translated as owning in this book, paragraph 39.

134
00:19:18,960 --> 00:19:35,040
But, actually, King Jajana is here explained in this sub-commentary as just nothing.

135
00:19:36,080 --> 00:19:43,040
Now, this word King Jajana has two meanings. One meaning is concern. It is a concern for something

136
00:19:43,040 --> 00:19:50,240
that is also called King Jajana. So, one who has no concern for anything, one who has no worries

137
00:19:50,240 --> 00:19:58,640
is called A King Jajana. A, an, an, an, an, an is described as A King Jajana because he has no worries,

138
00:19:58,640 --> 00:20:05,520
he has nothing to be concerned about. That is one meaning. And here, King Jajana, the same word,

139
00:20:05,520 --> 00:20:16,800
means nothing whatsoever, not concern or not owning. So, here, one, one, one you say,

140
00:20:16,800 --> 00:20:24,320
A King Jajana, that means nothing, nothing, nothing. So, it means a state of having nothing or

141
00:20:24,320 --> 00:20:29,760
simply nothingness, which here means nothingness or absence of the first Arupa or Jajana.

142
00:20:29,760 --> 00:20:34,800
I don't know, I mean, B is the same. And Jajana Jajana, each comment are,

143
00:20:34,800 --> 00:20:39,680
concomitant are called A King Jajana Jajana because they have nothingness or absence of the

144
00:20:39,680 --> 00:20:46,720
first Arupa Jajana as they are evident or as they are object. So, the third Arupa or Jajana takes

145
00:20:46,720 --> 00:20:54,880
the nothingness of or the absence of the first Arupa or Jajana. So, it is not, not just a base, but

146
00:20:54,880 --> 00:21:01,680
something that has nothingness as a base, nothingness as an object. So, these three words are the same.

147
00:21:04,240 --> 00:21:09,520
The first Arupa or Jajana, second Arupa or Jajana, that Arupa or Jajana, the poly words, belong to the same

148
00:21:09,520 --> 00:21:18,800
kind of compound. So, they, as a compound, they mean something other than those denoted by the

149
00:21:18,800 --> 00:21:28,960
individual wise. Now, the last one is different. So, the last one is called neither perception nor

150
00:21:28,960 --> 00:21:36,720
non-perception. It's very strange expression, neither perception nor non-perception.

151
00:21:38,480 --> 00:21:45,760
So, you find for with the third Arupa or Jajana and then you dwell upon the third Arupa or Jajana.

152
00:21:45,760 --> 00:21:52,480
It is called, it is before something like that. And it is explained here,

153
00:21:55,520 --> 00:22:08,960
where? Later on. So, you practice, you take the third Arupa or Jajana as an object and then you

154
00:22:08,960 --> 00:22:15,440
say it is called, it is peaceful, it is peaceful, and then say you gain the fourth Arupa or

155
00:22:15,440 --> 00:22:18,160
Jajana.

156
00:22:28,480 --> 00:22:31,360
And the fourth Arupa or Jajana is called, neither,

157
00:22:31,360 --> 00:22:49,840
or Jajana, or Jajana. Neither perception nor non-perception. That word is explained in two ways.

158
00:22:49,840 --> 00:22:55,200
The first, number one and number two. Do you see the difference between number one and number

159
00:22:55,200 --> 00:23:06,800
two on the sheet, second sheet. Did the poly words? Do you see the difference?

160
00:23:06,800 --> 00:23:34,880
Do you see the difference? Where? The last A, right. So, number one, in number one, the last A is

161
00:23:34,880 --> 00:23:43,920
short. Number two, the last A is long. Very good. And that is the only, if you saw this, you would

162
00:23:43,920 --> 00:23:54,000
know that, if you are reading the poly? Yes. They can mean quite two different things,

163
00:23:55,200 --> 00:24:00,720
the one with short A and the one with long A. So, number one. Now,

164
00:24:00,720 --> 00:24:09,840
the last A is long. So, the last A means having no sanja means not having no sanja.

165
00:24:09,840 --> 00:24:18,800
So, I forgot to put the sanja. Not having no sanja. Now, here Jajana,

166
00:24:18,800 --> 00:24:29,200
Jajana, and it is called, the last sanja. Not the last sanja, the last sanja. Because we have

167
00:24:29,200 --> 00:24:40,400
no gross sanja. No, they are completely without subtle sanja. Sanja means perception and sanja

168
00:24:41,600 --> 00:24:50,720
means something that has perception. So, here Jajana Chita, and it is recommended as a

169
00:24:50,720 --> 00:25:00,960
called, Neewa sanja, nana sanja. Because they do not have gross sanja yet, they are not completely

170
00:25:00,960 --> 00:25:14,320
without subtle sanja. So, here, Neewa sanja, nana sanja means the Jajana and its concomitance.

171
00:25:14,320 --> 00:25:30,720
So, in this compound, Ayatana, it is the same as the Neewa sanja, nana sanja. So, the one is the

172
00:25:31,680 --> 00:25:40,080
adjective of the other. The one qualifies the other. So, Ayatana here, by base,

173
00:25:40,080 --> 00:25:46,560
I meant the Jajana Chita and its concomitance. They are called base because they are included

174
00:25:46,560 --> 00:25:53,200
respectively in the mana base and tama base, among the trust bases. Now, there are trust bases

175
00:25:53,200 --> 00:26:01,440
stored in the vidama. And two of them are manajata, mana base, and tama yata, nana tama base. Manajata,

176
00:26:01,440 --> 00:26:08,240
mana base means just types of consciousness. And tama base means Chita seekers and some kind of

177
00:26:08,240 --> 00:26:17,520
rubas and others. Neewa sanja is also included here. So, here, Neewa sanja, nana sanja, nana sanja,

178
00:26:17,520 --> 00:26:24,480
and Ayatana, both mean the same thing. Neewa sanja, nana sanja means

179
00:26:26,000 --> 00:26:32,640
Jajana Chita and its concomitance, and Ayatana also means Jajana Chita and its concomitance.

180
00:26:32,640 --> 00:26:46,080
So, it is different. This compound is different. So, the Jajana Chita and its concomitance

181
00:26:46,080 --> 00:26:50,240
are called Neewa sanja, nana sanja, nana because they have no gross sanja,

182
00:26:50,240 --> 00:26:54,160
nor do they not have subtle sanja. And at the same time, they are bases.

183
00:26:54,160 --> 00:27:05,200
So, this compound is different from the other three compounds. And like the three previous

184
00:27:05,200 --> 00:27:10,480
words, the word, Neewa sanja, nana sanja, nana is another kind of compound where the whole compound

185
00:27:10,480 --> 00:27:15,040
means what is signified by the individual members, one qualifying the other.

186
00:27:15,040 --> 00:27:30,480
Second meaning, Neewa sanja, nana sanja, nana sanja, nana sanja, nana sanja,

187
00:27:30,480 --> 00:27:36,080
Ayatana base. So, here, sanja is called Neewa sanja, nana sanja, nana sanja,

188
00:27:36,080 --> 00:27:42,160
the whole word means sanja here, because it is neither full sanja,

189
00:27:42,160 --> 00:27:49,680
nor non sanja. And at the same time, it is a base. It cannot function as full sanja,

190
00:27:50,560 --> 00:27:57,760
but it is still sanja. So, it is called Neewa sanja, nana sanja. It is sanja, but it doesn't

191
00:27:58,800 --> 00:28:05,360
function as a full, full-fledged sanja. Although it does not function as full-fledged sanja,

192
00:28:05,360 --> 00:28:10,640
it is still sanja, very subtle sanja. So, it is called Neewa sanja, nana sanja,

193
00:28:10,640 --> 00:28:19,360
and ayatana is the means the same thing. So, here, sanja is called Neewa sanja, nana sanja,

194
00:28:19,360 --> 00:28:24,480
ayatana, because it is neither full sanja, nor non sanja. And at the same time, it is a base,

195
00:28:25,040 --> 00:28:31,840
or it is included in the nana base, or it is nana base. But here, the presentation is done in

196
00:28:31,840 --> 00:28:38,240
terms of sanja, which means sanja here is representative of nana and not sanja only.

197
00:28:38,240 --> 00:28:42,240
Therefore, we are to understand that Neewa sanja, nana, sanja, nana means

198
00:28:42,240 --> 00:28:48,480
sanja cheater, and it is concomitant, not sanja only. So, it is very complicated here,

199
00:28:48,480 --> 00:28:55,520
explanations. The whole word means sanja, but we must understand that it is not only sanja,

200
00:28:55,520 --> 00:29:14,160
which is meant, but sanja, along with other mental states. So, in this full aruba ojrejara,

201
00:29:14,160 --> 00:29:26,960
sanja becomes so subtle that it cannot function as a full sanja. But since there is still

202
00:29:27,600 --> 00:29:37,040
subtle form of sanja, we cannot call it no sanja at all. So, it is neither sanja nor no sanja.

203
00:29:37,040 --> 00:29:50,640
And the sanja is given, right? The object is sanja, the object. No, the object of this

204
00:29:50,640 --> 00:29:59,200
sanja is the third aruba ojrejara. It is the fourth aruba ojrejara. This fourth aruba ojrejana

205
00:29:59,200 --> 00:30:08,720
is called Neewa sanja, nana, sanja, nana. Although, the word is sanja, we must understand that

206
00:30:08,720 --> 00:30:13,520
it means not only sanja, but sanja and other mental states.

207
00:30:19,680 --> 00:30:24,400
And the similes are given here, the smelling of quail on the bowl, and then

208
00:30:24,400 --> 00:30:32,960
to a teacher and his trouble going on a journey, and teacher wanting to take part and so on.

209
00:30:45,840 --> 00:30:50,480
But in this context, what is perception function? It is the perception of the

210
00:30:50,480 --> 00:30:56,480
bathing of the object, and it is the projection of dispersion. If that attainment and its

211
00:30:56,480 --> 00:31:04,160
objects are made the objective feel of inside. What does that mean? It is the perceiving of the

212
00:31:04,160 --> 00:31:11,680
object, and it is the production of dispersion if that attainment and its objects are made

213
00:31:11,680 --> 00:31:16,820
the objective field of insight. Now perception, sanya.

214
00:31:16,820 --> 00:31:21,360
Sanya's function is to perceive the object. So its function here is

215
00:31:21,360 --> 00:31:27,520
perceiving the object and the and the other function is to become the object of

216
00:31:27,520 --> 00:31:35,080
we persona. So that is what is meant here. It is the production of

217
00:31:35,080 --> 00:31:40,880
dispersion if made the objective field of insight. That means it can it can

218
00:31:40,880 --> 00:31:46,000
produce dispersion if it just made the object of we persona. So when you practice

219
00:31:46,000 --> 00:31:53,880
we persona you can you can watch sanya itself. And you see sanya rising in

220
00:31:53,880 --> 00:32:01,120
disappearing and so you become dispersion to a sanya. But it is not able to

221
00:32:01,120 --> 00:32:05,080
make the function of perceiving decisive. That's the heat element in

222
00:32:05,080 --> 00:32:09,440
third water is not able to make the function of burning decisive and it is not

223
00:32:09,440 --> 00:32:13,880
able to produce dispersion by treatment of its objective field with insight in

224
00:32:13,880 --> 00:32:24,600
the way that precipitation is in the case of other attainments. In the

225
00:32:24,600 --> 00:32:31,960
rapid water there is heat element but it is not able to burn or to heat. So in

226
00:32:31,960 --> 00:32:40,760
the same way there is sanya here but it cannot function in a decisive way or

227
00:32:40,760 --> 00:32:49,800
in it cannot function fully. There is in fact no beku. He will be

228
00:32:49,800 --> 00:32:53,240
below reaching dispersion by comprehension of aggregates connected with the

229
00:32:53,240 --> 00:32:56,760
base consisting of another perception or non-possession unless he has

230
00:32:56,760 --> 00:33:01,080
already done his interpreting with other aggregates. So there is no nobody.

231
00:33:01,080 --> 00:33:08,840
Even if he is Sariboda capable of reaching dispersion by comprehension of

232
00:33:08,840 --> 00:33:13,240
aggregates connected with the base consisting of neither perception and

233
00:33:13,240 --> 00:33:16,680
non-possession unless he has already done his interpreting with other

234
00:33:16,680 --> 00:33:23,880
aggregates. That means he must have practiced we persona meditation and then

235
00:33:23,880 --> 00:33:34,320
contemplating on the impermanence suffering and soulless nature of things of

236
00:33:34,320 --> 00:33:43,640
all phenomena grouped by group one by one. So only after that could be

237
00:33:43,640 --> 00:33:52,560
capable of reaching dispersion otherwise no. So even if he is Sariboda

238
00:33:52,560 --> 00:34:00,080
he will not be able to reach and that what is missing here so we should

239
00:34:00,080 --> 00:34:07,720
insert the words there is in fact no beku even if he is the

240
00:34:07,720 --> 00:34:15,040
vulnerable Sariboda even if he is the vulnerable Sariboda so capable of

241
00:34:15,040 --> 00:34:22,880
reaching dispersion and so on. Then next and next and furthermore when the

242
00:34:22,880 --> 00:34:27,120
vulnerable Sariboda or someone very wise and naturally gifted with insight as he

243
00:34:27,120 --> 00:34:31,280
was is able to do so even he has to do it by means of comprehending by

244
00:34:31,280 --> 00:34:38,280
groups and here also the meaning is something like that one who has

245
00:34:38,280 --> 00:34:45,960
practiced we persona in a usual way that means beginning with contemplating on

246
00:34:45,960 --> 00:34:53,080
the on the aggregates and so on. Even such a person who is a wise one who is as

247
00:34:53,080 --> 00:35:11,360
wise as Sariboda should or is able to do so even he has to do it by means of

248
00:35:11,360 --> 00:35:17,880
comprehension by groups. So even one has practiced

249
00:35:17,880 --> 00:35:24,720
we persona meditation and in the usual way beginning with comprehension of

250
00:35:24,720 --> 00:35:32,120
the the the comprehension of the information and so on of different mind and

251
00:35:32,120 --> 00:35:39,560
matter. So even such a person has to do it by means of comprehension by groups

252
00:35:39,560 --> 00:35:48,720
not comprehension by individual states. So comprehending if I

253
00:35:48,720 --> 00:35:52,960
groups the whole take the whole group as the object of meditation not

254
00:35:52,960 --> 00:36:00,560
individual members in this way. So it seems that these states not having been

255
00:36:00,560 --> 00:36:05,200
come to be having come to be the advantage and not by means of actual

256
00:36:05,200 --> 00:36:10,600
direct insight into states one by one as the arise such as the subtlety that

257
00:36:10,600 --> 00:36:21,880
this attainment reaches. So even a one is an experienced practitioner

258
00:36:21,880 --> 00:36:32,480
we persona one can do so only by groups and not go into state one by one and

259
00:36:32,480 --> 00:36:35,480
this meaning should be illustrated by the seemingly of the word on the road that

260
00:36:35,480 --> 00:36:43,480
is the same similar seemingly of the smearing the bow with the

261
00:36:43,480 --> 00:36:55,520
oil. So he enters upon and so on. Now come the general so the general

262
00:36:55,520 --> 00:37:13,920
explanation is first is what? The first is that the the

263
00:37:13,920 --> 00:37:25,400
Arugabjara Janas have to surmount the object rather than the fact

264
00:37:25,400 --> 00:37:33,320
channel factors that that means Arugabjara Janas have different objects then

265
00:37:33,320 --> 00:37:37,140
different channel factors because they have the same number of

266
00:37:37,140 --> 00:37:43,280
channel factors two channel factors all of them have two channel factors so when you

267
00:37:43,280 --> 00:37:49,200
want to reach the higher Arugabjara Janas you have to surmount the object and

268
00:37:49,200 --> 00:37:57,480
not the not the factors that means you you do you cannot eliminate the

269
00:37:57,480 --> 00:38:02,360
channel factors but you have to surmount the object that means you have to take

270
00:38:02,360 --> 00:38:11,280
another object that is the first one and the second 59 is just a

271
00:38:11,280 --> 00:38:20,200
seemingly of how they are subtle one one more than the other and there is

272
00:38:20,200 --> 00:38:32,840
seemingly of a cloth a very very thin cloth a wing maybe one arms two arms as

273
00:38:32,840 --> 00:38:41,160
and so on. So the this means that they are the second is more subtle than the

274
00:38:41,160 --> 00:38:50,080
first and the third is more subtle than the second and so on and then the next thing

275
00:38:50,080 --> 00:39:03,640
61 it's also it's seemingly given as some people holding on to some other

276
00:39:03,640 --> 00:39:09,960
thing and then another man comes and hold on to him so so these are not

277
00:39:09,960 --> 00:39:19,960
difficult to understand then the last one the next one is 64 and here the

278
00:39:19,960 --> 00:39:25,160
third the fourth Arugabjara Janas takes the third as object and find you want to

279
00:39:25,160 --> 00:39:31,720
attain the full Arugabjara Janas you say oh this third Arugabjara Janas is good it is

280
00:39:31,720 --> 00:39:38,400
peaceful it is good you contemplate like that then if you say it is good it is

281
00:39:38,400 --> 00:39:52,400
good you are just to it or you like it right but the answer here is no because

282
00:39:52,400 --> 00:39:57,280
you find for with the king but there is no other way to do it you just follow

283
00:39:57,280 --> 00:40:08,240
him 64 it takes this for it's object since there is no other one as good as many

284
00:40:08,240 --> 00:40:15,280
penamolic king whose fault they see for livelihood so you cannot get away from

285
00:40:15,280 --> 00:40:18,960
the king because his livelihood depends on the king so although he doesn't like

286
00:40:18,960 --> 00:40:36,040
him and then the last one 66 also gives you how one has to hold on to or take

287
00:40:36,040 --> 00:40:41,800
as object the third one the one who mounts a loofy stare leans on its

288
00:40:41,800 --> 00:40:48,680
railing for the prop so when you mount a stare then you have to lean on the prop one

289
00:40:48,680 --> 00:40:54,280
who climbs and every pick leans on the mountains very top so when you go up a

290
00:40:54,280 --> 00:41:01,360
mountain then you have to hold on to the top as one who stands on the correct

291
00:41:01,360 --> 00:41:06,800
heads leans or support on its own knees sometimes you have to lean on your own

292
00:41:06,800 --> 00:41:14,240
knees each general rests on that below for so it is with each of these so in

293
00:41:14,240 --> 00:41:27,440
this way one general depends on another so on so this is the end of the fourth

294
00:41:27,440 --> 00:41:36,680
photo of the channel after you rush through the explanations are sometimes very

295
00:41:36,680 --> 00:41:43,520
informed and what explanations also because there is as I said some can be

296
00:41:43,520 --> 00:41:49,160
guilty in many party words so you can you can make a word means something other

297
00:41:49,160 --> 00:41:57,440
than what seems what it seems to me one we say that if this is a

298
00:41:57,440 --> 00:42:02,680
adjectival compound that I would say no it is another kind of compound and I

299
00:42:02,680 --> 00:42:11,640
can explain it so they are likely ambiguity of how you watch and you know we

300
00:42:11,640 --> 00:42:19,280
we have lost the accent entirely but there was accent in older Sanskrit

301
00:42:19,280 --> 00:42:26,440
Vedic Sanskrit or older Sanskrit so in the older Sanskrit they differentiate

302
00:42:26,440 --> 00:42:35,200
words by accent say one and the same word can be the adjectival compound or

303
00:42:35,200 --> 00:42:40,400
another kind of compound but the word is the same but they have different

304
00:42:40,400 --> 00:42:45,240
accent say accent in all the first word or the second word so by accent they

305
00:42:45,240 --> 00:42:52,560
differentiate that this is a digital compound and not any other but in later

306
00:42:52,560 --> 00:42:59,220
Sanskrit and in party we have lost that accent and so when there is no

307
00:42:59,220 --> 00:43:04,520
accent there is no way of ascertaining this is this only and not the not the

308
00:43:04,520 --> 00:43:12,560
other so there comes the confusion about words so I can explain it in in any way

309
00:43:12,560 --> 00:43:22,360
I like if I am familiar with it is curvatical and it's relations so many

310
00:43:22,360 --> 00:43:27,320
words are explained in that way it will come under is so the word rupa sanya is

311
00:43:27,320 --> 00:43:35,720
made to mean the genres and also the objects so it is it is true that he said

312
00:43:35,720 --> 00:43:59,920
yeah it is a play upon words

313
00:44:05,720 --> 00:44:35,360
yes next chapter I think not all but you see

314
00:44:35,720 --> 00:44:44,440
but they are most of them about 32 parts of the world again so I think they

315
00:44:44,440 --> 00:45:00,480
are not so interesting but say about until page 395 or 1096 two lines up there

316
00:45:00,480 --> 00:45:18,840
a paragraph in 783 that's about 20

