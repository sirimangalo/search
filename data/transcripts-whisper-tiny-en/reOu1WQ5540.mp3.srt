1
00:00:00,000 --> 00:00:11,600
Why is there so much emphasis on following which tradition or which lineage of praxing Buddhism?

2
00:00:11,600 --> 00:00:17,440
If the dharma is taught by Gautama Buddha, should we only emphasize and refer as Gautama

3
00:00:17,440 --> 00:00:22,840
Buddha teaching instead of lineage in tradition?

4
00:00:22,840 --> 00:00:27,120
May I start with a short command on it?

5
00:00:27,120 --> 00:00:38,840
This is because we have defiled minds, just that we need to be right, that we have the wanting

6
00:00:38,840 --> 00:00:43,760
to be right, the wanting to know better than others.

7
00:00:43,760 --> 00:00:54,960
This is how a lineage and tradition evolves, that some people meet and say, let's do it that

8
00:00:54,960 --> 00:01:02,480
way, and everything who does otherwise is out of that lineage, is out of that tradition.

9
00:01:02,480 --> 00:01:08,000
And then they start their own tradition and their own lineage according to what they think

10
00:01:08,000 --> 00:01:17,160
is right, and the thing is that they insist on both of course, both groups or all the

11
00:01:17,160 --> 00:01:26,920
groups that come up insist that they are right.

12
00:01:26,920 --> 00:01:38,760
So since the Buddha, Gautama Buddha is not living anymore, there is no being that can say,

13
00:01:38,760 --> 00:01:46,400
this is the truth, this is what has been said, this is what the Buddha said.

14
00:01:46,400 --> 00:01:56,160
So but now are many, many people with defiled, more or less defiled minds there, telling

15
00:01:56,160 --> 00:02:01,680
this has been said, that has been said, and want to be right.

16
00:02:01,680 --> 00:02:09,640
So that's why there are lineages and lineages and traditions.

17
00:02:09,640 --> 00:02:21,120
If we could agree on what were the Buddha's teaching or what were the practice of meditation

18
00:02:21,120 --> 00:02:29,040
that the Buddha really told to practice, then there would probably be only one tradition

19
00:02:29,040 --> 00:02:31,000
and one lineage.

20
00:02:31,000 --> 00:02:41,680
I'd like to add a couple of things, and then as it goes a little bit beyond that, because

21
00:02:41,680 --> 00:02:48,960
you have to understand, like in reference, for example, the Tibetan Buddhism, many aspects

22
00:02:48,960 --> 00:02:52,640
of Tibetan Buddhism, I don't know about all of it, and I always don't like to talk about

23
00:02:52,640 --> 00:02:57,320
even a country's Buddhism, but many of the types of Buddhism that you find in Tibet don't

24
00:02:57,320 --> 00:03:06,960
even refer so much to the teachings of the Buddha, or about following the teaching the

25
00:03:06,960 --> 00:03:15,680
45 years of the Buddha's teaching, they will instead strive to become Buddhas by themselves,

26
00:03:15,680 --> 00:03:17,160
and still call this Buddhism.

27
00:03:17,160 --> 00:03:26,680
So it even goes beyond simply disagreement over what the Buddha said, it's disagreement over

28
00:03:26,680 --> 00:03:30,040
in what way we relate to the Buddha.

29
00:03:30,040 --> 00:03:36,440
And so you might say, well, that's just because of defilements, but then it really goes

30
00:03:36,440 --> 00:03:37,440
by.

31
00:03:37,440 --> 00:03:51,080
So at the very least, there's room on a subjective level to suggest that there are different

32
00:03:51,080 --> 00:03:53,440
ways of approaching the Buddha.

33
00:03:53,440 --> 00:03:58,000
So you can say, you talk about the Gautama Buddha's teaching, well, some people don't

34
00:03:58,000 --> 00:03:59,400
see that as the most important.

35
00:03:59,400 --> 00:04:04,640
They take Gautama's Buddha's example as the core of Buddhism, and not even, no, and actually

36
00:04:04,640 --> 00:04:07,120
many of them will not even refer to Gautama Buddha.

37
00:04:07,120 --> 00:04:12,040
In fact, there are schools of Buddhism that don't refer so much, anyway, to him, they refer

38
00:04:12,040 --> 00:04:15,880
to Avalokitesh for a Buddha.

39
00:04:15,880 --> 00:04:21,960
Amitafas in China, they call him, who is in the Western Pure Land, who, if you chant his

40
00:04:21,960 --> 00:04:25,760
name with the pure mind, he'll be born in his realm and become a Buddha there.

41
00:04:25,760 --> 00:04:30,440
So, I mean, someone from our tradition might just say, well, that's just because of the

42
00:04:30,440 --> 00:04:33,800
defilements of those people and the ignorance that they say that.

43
00:04:33,800 --> 00:04:38,240
But subjectively, there is room for different interpretations.

44
00:04:38,240 --> 00:04:43,840
And so on one level, someone can say, well, yes, the Buddha taught all this, but rather

45
00:04:43,840 --> 00:04:48,440
than follow his teachings, I'm going to become a Buddha.

46
00:04:48,440 --> 00:04:53,720
And they might, therefore, start a tradition, encouraging other people to do that and

47
00:04:53,720 --> 00:04:58,040
saying, yeah, that's all the kombudas together, which seems to be the case in certain

48
00:04:58,040 --> 00:05:13,680
instances, where this is even more clearly the case is within a harmonious, sort of

49
00:05:13,680 --> 00:05:19,480
super tradition.

50
00:05:19,480 --> 00:05:20,120
So, within a group of people who follow the same text, who follow the same teachings

51
00:05:20,120 --> 00:05:29,040
and who can agree on what are the Buddha's teachings, I would say, I would submit the argument

52
00:05:29,040 --> 00:05:33,560
that it still might be possible to have different traditions and lineages, like you might

53
00:05:33,560 --> 00:05:41,720
consider Sariputas lineage to be a specific type of practice that was suitable for people

54
00:05:41,720 --> 00:05:48,680
who had high intelligence and Mogulana's lineage for those people who were more inclined

55
00:05:48,680 --> 00:05:49,680
towards magical powers.

56
00:05:49,680 --> 00:05:54,800
You know, there's the story of the Buddha upon Gijakuta, Vulture's peak, and looking down

57
00:05:54,800 --> 00:05:56,560
and watching the monks go and arms round.

58
00:05:56,560 --> 00:06:04,800
And he said, you see those monks following Sariputta, that's because they're all really

59
00:06:04,800 --> 00:06:07,160
engaged in the development of wisdom.

60
00:06:07,160 --> 00:06:10,960
And you see those monks following Mogulana, that's because the reason they follow him

61
00:06:10,960 --> 00:06:16,200
is because they are very keen on magical powers.

62
00:06:16,200 --> 00:06:20,240
And you see those ones following Ananda, that's because they're all, those monks are all

63
00:06:20,240 --> 00:06:25,200
Bajusutta, they're all ones who are keen on memorizing the texts.

64
00:06:25,200 --> 00:06:30,720
And you see those ones following Devadatta, that's because they're all corrupt and evil.

65
00:06:30,720 --> 00:06:37,800
And so he said something like birds of a feather flock together.

66
00:06:37,800 --> 00:06:47,680
And so, while you could consider that, for example, in our tradition, we call, even let's

67
00:06:47,680 --> 00:06:52,360
say within the Mahasi Sayada tradition, there's group of people who follow the same type

68
00:06:52,360 --> 00:06:53,360
of practice.

69
00:06:53,360 --> 00:06:58,120
Because you could make the argument that we don't find anything wrong with, say, Adjan

70
00:06:58,120 --> 00:07:07,680
Chaz, tradition, they have a perfectly adequate and successful interpretation of the Buddha

71
00:07:07,680 --> 00:07:10,800
's teaching or tradition of practice based on the Buddha's teaching.

72
00:07:10,800 --> 00:07:11,800
We could say that.

73
00:07:11,800 --> 00:07:12,800
I don't really know.

74
00:07:12,800 --> 00:07:15,400
Could be the case, maybe not.

75
00:07:15,400 --> 00:07:18,720
We certainly don't attack them or say they're wrong.

76
00:07:18,720 --> 00:07:23,400
And we say they're still the same, they take the same body of texts.

77
00:07:23,400 --> 00:07:28,920
And they seem to be practicing in a way that is in line with those texts.

78
00:07:28,920 --> 00:07:35,520
But even within the Mahasi Sayada group, every monastery that you go to, not even every country,

79
00:07:35,520 --> 00:07:40,760
every monastery that you go to, even within a monastery, within the monastery that I say

80
00:07:40,760 --> 00:07:47,600
that, every teacher in that monastery had a different way of teaching.

81
00:07:47,600 --> 00:07:51,040
And you could cynically say that it's because of the defilements of all of them, but

82
00:07:51,040 --> 00:07:55,440
one, all but my teacher, everybody else is just because they have defilements.

83
00:07:55,440 --> 00:08:00,800
But I don't think it necessarily has to be the case because the other argument was that

84
00:08:00,800 --> 00:08:04,240
it has to do with the needs of the students or the nature of the students.

85
00:08:04,240 --> 00:08:08,520
But it can also have to do with the, I would look at the teacher.

86
00:08:08,520 --> 00:08:12,680
So for example, the teacher of Tuchabotilla, a really good example.

87
00:08:12,680 --> 00:08:14,000
How did he teach Tuchabotilla?

88
00:08:14,000 --> 00:08:19,400
This monk who thought he was the best of the best because he could memorize the whole

89
00:08:19,400 --> 00:08:20,400
to Pitika?

90
00:08:20,400 --> 00:08:24,000
He said, go run to the pond.

91
00:08:24,000 --> 00:08:27,480
And so he had this guy with all his robes on, go and run to the middle of pond and floating

92
00:08:27,480 --> 00:08:29,840
there in the pond and he'll come back out.

93
00:08:29,840 --> 00:08:32,120
And then he sent them back down into the pond again.

94
00:08:32,120 --> 00:08:33,120
Why?

95
00:08:33,120 --> 00:08:34,120
Because he was seven years old.

96
00:08:34,120 --> 00:08:35,120
He was seven year old.

97
00:08:35,120 --> 00:08:36,120
I think he was an Arahant.

98
00:08:36,120 --> 00:08:42,120
This seven year old Arahant, but this is how they think they, they run to the pond.

99
00:08:42,120 --> 00:08:48,240
And then he's taught him in the middle of the pond about, but this, uh, there's Antil.

100
00:08:48,240 --> 00:08:50,280
And the lizard goes into the Antil.

101
00:08:50,280 --> 00:08:52,280
What do you do to get it out?

102
00:08:52,280 --> 00:08:56,200
Plug up all the holes but one and then watch that hole and grab it.

103
00:08:56,200 --> 00:08:59,920
And he said in the same way, this is how you watch your mind.

104
00:08:59,920 --> 00:09:04,920
You close all the other senses and just watch the mind wherever the mind goes.

105
00:09:04,920 --> 00:09:09,040
The means don't worry about what you see, what you hear, what you smell.

106
00:09:09,040 --> 00:09:10,040
Just look at where the mind is.

107
00:09:10,040 --> 00:09:13,160
If the mind goes to the eye, then you just see your hearing.

108
00:09:13,160 --> 00:09:17,960
So you close them all and you're only concerned with where the mind is going, not with

109
00:09:17,960 --> 00:09:20,280
anything else.

110
00:09:20,280 --> 00:09:27,360
So, so totally different way of teaching from, from, you might say another Arahant.

111
00:09:27,360 --> 00:09:29,680
The technique would be quite different.

112
00:09:29,680 --> 00:09:36,080
So you have, you know, some, some people in Thailand who, we have this practice of mindful

113
00:09:36,080 --> 00:09:40,640
frustration and my teacher teaches it one way, but I went to another center and they teach

114
00:09:40,640 --> 00:09:41,640
in another way.

115
00:09:41,640 --> 00:09:43,040
They have this really kind of crazy.

116
00:09:43,040 --> 00:09:45,920
I mean, I don't, I don't go for it to me, it seems crazy, but they have another way of

117
00:09:45,920 --> 00:09:46,920
doing mindful frustration.

118
00:09:46,920 --> 00:09:48,320
There's nothing wrong with it.

119
00:09:48,320 --> 00:09:52,800
It's kind of crazy, but it's still, it's a lot, I did it and I was doing it and you can

120
00:09:52,800 --> 00:09:55,320
be mindful just the same.

121
00:09:55,320 --> 00:09:57,680
We do these different parts of the walking step.

122
00:09:57,680 --> 00:10:01,240
And some people say you do lifting, placing, lifting straight up.

123
00:10:01,240 --> 00:10:04,280
Some people say you lift forward and then place it straight down.

124
00:10:04,280 --> 00:10:12,960
So there's a million different ways of, of, of practicing the teaching and probably as

125
00:10:12,960 --> 00:10:18,560
Paul and you said, many of them just come from defilements, but there is certainly room

126
00:10:18,560 --> 00:10:29,320
for a subjective reason, which is that simply because we're, we're all, we're all coming

127
00:10:29,320 --> 00:10:37,200
from different places that all, even all, I'm not, not to speak of us students, but to

128
00:10:37,200 --> 00:10:43,200
speak of, say, the hypothetical hour hunts, those people who are, who are fully enlightened,

129
00:10:43,200 --> 00:10:45,120
they're still quite different.

130
00:10:45,120 --> 00:10:50,800
Even Buddhas are different in certain respects, most respects are the same, but some Buddhas

131
00:10:50,800 --> 00:10:53,320
don't teach the party mocha, for example.

132
00:10:53,320 --> 00:10:57,760
So would you say, so, but you, you can't say that, that it's because of the defilements of

133
00:10:57,760 --> 00:11:02,000
that Buddha that he didn't teach the party mocha is because of his situation and because

134
00:11:02,000 --> 00:11:07,360
of the students and that came to him and so on.

135
00:11:07,360 --> 00:11:09,560
What is the same will be certain concepts.

136
00:11:09,560 --> 00:11:14,000
So the, what will be based on defilements is whether they're actually teaching based on

137
00:11:14,000 --> 00:11:18,840
the four noble truths, but to just sum upada, whether they're, they're clear in terms

138
00:11:18,840 --> 00:11:19,840
of causing the fact.

139
00:11:19,840 --> 00:11:24,240
So if someone starts to say that, know the Buddha taught that our hunts have angry

140
00:11:24,240 --> 00:11:31,800
anger and delusion, then you say defilements, and you can check that one out, this is wrong.

141
00:11:31,800 --> 00:11:35,040
But if someone says, well, you should prostrate like this, or you should prostrate like

142
00:11:35,040 --> 00:11:39,120
that, or you should do Anapana Santi, or you should, you know, you should watch at

143
00:11:39,120 --> 00:11:42,000
the nose, or you should watch at the stomach, or someone.

144
00:11:42,000 --> 00:11:50,280
There's room for all of that, I think, in the differences of, we saw what it was, or

145
00:11:50,280 --> 00:11:56,520
Wasana, which means the things that we bring into this life.

146
00:11:56,520 --> 00:12:09,280
Maybe even the, the religions of, of a country before Buddhism came to that country still

147
00:12:09,280 --> 00:12:19,040
have a, an influence on, on how it developed, like in Tibet there, I heard was kind

148
00:12:19,040 --> 00:12:32,880
of ghost religion with shamanism called, and that, that, that there still has an influence

149
00:12:32,880 --> 00:12:43,200
and in China, there was something different going on, so I think this can be taken in, in

150
00:12:43,200 --> 00:13:10,440
consideration as well.

