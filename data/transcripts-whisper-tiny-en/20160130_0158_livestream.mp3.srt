1
00:00:00,000 --> 00:00:28,000
Good evening, everyone, broadcasting live January 29th, 2016.

2
00:00:28,000 --> 00:00:49,000
Today's quote, today's quote is about fairly mundane things.

3
00:00:49,000 --> 00:00:57,000
If you're noticing a pattern, a lot of these quotes are Buddhism light.

4
00:00:57,000 --> 00:01:11,000
It's a bit worrisome because it's easy to get complacent if you think of Buddhism as a social philosophy to help people live well in the world.

5
00:01:11,000 --> 00:01:26,000
It's easy to become complacent thinking you can just be generous and do good deeds in the world, and that's enough.

6
00:01:26,000 --> 00:01:34,000
Not that this is not a Buddhist teaching, it's just an emphasis on worldly aspects of the teaching.

7
00:01:34,000 --> 00:01:45,000
It's not for our purposes, it's problematic because we're all here interested in the core teachings of the Buddha.

8
00:01:45,000 --> 00:01:55,000
It's hard to get to the core teachings if you keep talking about good teachings, but good basic teachings like charity.

9
00:01:55,000 --> 00:02:06,000
Living a good household life doesn't talk about how to address the issues.

10
00:02:06,000 --> 00:02:17,000
But one thing this quote does mention is to be modest about your wealth and to not get upset when it goes away.

11
00:02:17,000 --> 00:02:30,000
And so that at least hints at a mental development because it's easy to say don't get upset when you lose the things you love,

12
00:02:30,000 --> 00:02:33,000
but it's really impossible in practice.

13
00:02:33,000 --> 00:02:39,000
If you love something, you will be upset when it goes away. You can't stop that.

14
00:02:39,000 --> 00:02:43,000
When you don't get what you want, there's a yearning for it.

15
00:02:43,000 --> 00:03:00,000
When the yearning is unsatisfied, you're suffering.

16
00:03:00,000 --> 00:03:15,000
And so this begins to hint at the need for practice.

17
00:03:15,000 --> 00:03:21,000
To make your mind such that when you're touched by the vicissitudes of life that changes in life,

18
00:03:21,000 --> 00:03:34,000
the impermanence of life touched by good and evil touched by pleasant and unpleasant.

19
00:03:34,000 --> 00:03:48,000
When the mind doesn't waver, when the mind doesn't clean, doesn't push or pull,

20
00:03:48,000 --> 00:03:58,000
we have to find the practice that leads us to this.

21
00:03:58,000 --> 00:04:01,000
The theory is quite simple.

22
00:04:01,000 --> 00:04:05,000
That experience itself isn't capable of causing a suffering.

23
00:04:05,000 --> 00:04:09,000
It's our reactions to our experiences that cause us suffering.

24
00:04:09,000 --> 00:04:15,000
If we didn't react, if we saw things objective, we wouldn't cling to them.

25
00:04:15,000 --> 00:04:20,000
If we saw them objectively, we wouldn't suffer from them.

26
00:04:20,000 --> 00:04:27,000
Someone's yelling at you. It's only sound until you let it get to you.

27
00:04:27,000 --> 00:04:32,000
If you have great pain, it's only pain until you let it get to you.

28
00:04:32,000 --> 00:04:40,000
Hot and cold and hard and soft.

29
00:04:40,000 --> 00:04:48,000
Amazing what as a meditator you can bear and you can endure without suffering.

30
00:04:48,000 --> 00:04:58,000
When I first went to meditate, we had to sleep on the hardwood floor, which is the very thing that.

31
00:04:58,000 --> 00:05:03,000
I mean, it's not like I was in great hardship, but this was one thing that I remember.

32
00:05:03,000 --> 00:05:09,000
If you don't have much fat on you, sleeping like that is very painful.

33
00:05:09,000 --> 00:05:16,000
But you know, one night I slept under a park bench and it was really comfortable on the cement.

34
00:05:16,000 --> 00:05:25,000
In bitter cold as well.

35
00:05:25,000 --> 00:05:27,000
There was a story behind that.

36
00:05:27,000 --> 00:05:36,000
But I always tell this story about this park bench because there was so much.

37
00:05:36,000 --> 00:05:44,000
So I was really complicated situation and people were pushing and bullying.

38
00:05:44,000 --> 00:05:49,000
And I just walked out and went and stayed under a park bench.

39
00:05:49,000 --> 00:05:59,000
And it felt so great because there was no, nobody, no, I didn't have to.

40
00:05:59,000 --> 00:06:12,000
Concerned myself with politics or opinions and telling people that the point here is.

41
00:06:12,000 --> 00:06:15,000
It's all in the mind.

42
00:06:15,000 --> 00:06:21,000
Some people.

43
00:06:21,000 --> 00:06:30,000
But it said even in a comfortable bed, a comfortable chair in great misery, great suffering, great stress.

44
00:06:30,000 --> 00:06:38,000
Their minds are filled with lust or filled with anger, with the delusion.

45
00:06:38,000 --> 00:06:40,000
There's three bad things.

46
00:06:40,000 --> 00:06:44,000
There's three things the poison are right.

47
00:06:44,000 --> 00:06:52,000
Our minds are full of them.

48
00:06:52,000 --> 00:06:55,000
It doesn't matter what comforts we feel, comforts we get.

49
00:06:55,000 --> 00:07:02,000
We're still suffering.

50
00:07:02,000 --> 00:07:04,000
So our practice is quite simple.

51
00:07:04,000 --> 00:07:13,000
We just remind ourselves of what things are, keeps us objective.

52
00:07:13,000 --> 00:07:19,000
When you do it, you can see some people doubt this technique and that doubt gets in a way.

53
00:07:19,000 --> 00:07:22,000
They don't give it an honest try.

54
00:07:22,000 --> 00:07:33,000
As soon as you give it an honest try, you can see in that moment the clarity, the mind is clear, your mind, pure.

55
00:07:33,000 --> 00:07:36,000
It's amazing teaching people five-minute meditation.

56
00:07:36,000 --> 00:07:41,000
I bet a lot of them will forget about it, but you give them one moment.

57
00:07:41,000 --> 00:07:44,000
You give them that minute.

58
00:07:44,000 --> 00:07:47,000
Where are they?

59
00:07:47,000 --> 00:07:54,000
They come out of like a one-minute meditation where we do it at the end, just about one minute.

60
00:07:54,000 --> 00:07:58,000
And they rave about it, because they've seen it.

61
00:07:58,000 --> 00:08:00,000
They've seen that moment.

62
00:08:00,000 --> 00:08:04,000
It takes us one moment.

63
00:08:04,000 --> 00:08:10,000
So when you doubt about the practice, when you're not sure, you should scold yourself.

64
00:08:10,000 --> 00:08:13,000
Because if you've been practicing it all, you've seen the moment.

65
00:08:13,000 --> 00:08:15,000
You've seen that moment.

66
00:08:15,000 --> 00:08:17,000
You've had that one moment.

67
00:08:17,000 --> 00:08:20,000
So later when you doubt, is this really helping?

68
00:08:20,000 --> 00:08:23,000
You should call yourself foolish for thinking.

69
00:08:23,000 --> 00:08:27,000
If you call yourself out on it, absolutely.

70
00:08:27,000 --> 00:08:33,000
When they trick ourselves, is this really good?

71
00:08:33,000 --> 00:08:43,000
It's like a person, this happens, you know, when a person gets.

72
00:08:43,000 --> 00:08:52,000
When we get free from the illness, and then we become reckless again.

73
00:08:52,000 --> 00:08:58,000
We get a terrible illness, and we suffer terribly.

74
00:08:58,000 --> 00:09:04,000
So when we get free from it, we act as though it never happened.

75
00:09:04,000 --> 00:09:09,000
We know to admit our vulnerability to the illness.

76
00:09:09,000 --> 00:09:17,000
We don't work to try to change our, to train our mind.

77
00:09:17,000 --> 00:09:29,000
We're very good at forgetting.

78
00:09:29,000 --> 00:09:34,000
We should try to practice remembering.

79
00:09:34,000 --> 00:09:38,000
And so if you want to know real progress in the practice, think about the moments.

80
00:09:38,000 --> 00:09:46,000
In this moment, when I mindful, see the result.

81
00:09:46,000 --> 00:09:49,000
It takes us a moment.

82
00:09:49,000 --> 00:09:52,000
So that's a little bit for tonight.

83
00:09:52,000 --> 00:10:02,000
Tomorrow at 3 p.m. Eastern Time, mid-12 p.m., mid-day, second lifetime.

84
00:10:02,000 --> 00:10:05,000
I'll be giving it a talk.

85
00:10:05,000 --> 00:10:10,000
So in second life, at the Buddha Center, so you know what that is.

86
00:10:10,000 --> 00:10:12,000
See you there.

87
00:10:12,000 --> 00:10:14,000
Have a good night.

88
00:10:14,000 --> 00:10:17,000
Thank you.

