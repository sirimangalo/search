1
00:00:00,000 --> 00:00:22,500
Good evening everyone, welcome to our live broadcast.

2
00:00:22,500 --> 00:00:35,500
Today we're looking at a good tourniquy, a book of 5, 6 to 48.

3
00:00:35,500 --> 00:00:41,500
Alabanea Tanasupa.

4
00:00:41,500 --> 00:00:50,500
Alabanea Tanah is a Tanda means a place.

5
00:00:50,500 --> 00:00:57,500
But here it means a state or a case.

6
00:00:57,500 --> 00:01:06,500
Alabanea means unattainable.

7
00:01:06,500 --> 00:01:09,500
Unattainable.

8
00:01:09,500 --> 00:01:11,500
So there are five.

9
00:01:11,500 --> 00:01:18,500
Bantimani, Alabanea Tanah.

10
00:01:18,500 --> 00:01:26,500
There are these five unattainable states and things that are unattainable.

11
00:01:26,500 --> 00:01:33,500
Somerny, Novabramani, Novabramani, Novabraman, Novabraman, Novabraman, Novabraman.

12
00:01:33,500 --> 00:01:40,500
I want to tell you how to look at me by anyone in this world, by a recluse or a brahmin,

13
00:01:40,500 --> 00:01:48,500
no matter how religious or spiritually powerful they may be, or even if they're an angel

14
00:01:48,500 --> 00:01:57,500
or a demon or a god, can't obtain.

15
00:01:57,500 --> 00:02:16,500
No matter how powerful you are, no matter who you are, certain things that we can't get.

16
00:02:16,500 --> 00:02:27,500
And these four are five, in regards to things that are of a nature to get old, Jarada,

17
00:02:27,500 --> 00:02:32,500
Magiri, may they not get old.

18
00:02:32,500 --> 00:02:34,500
You can't have that.

19
00:02:34,500 --> 00:02:37,500
You can't have that.

20
00:02:37,500 --> 00:02:45,500
I won't come true, no matter what you do.

21
00:02:45,500 --> 00:02:52,500
You can't have that, no matter what it is.

22
00:02:52,500 --> 00:03:01,500
You can't have that.

23
00:03:01,500 --> 00:03:18,460
Mark, number three, Marana dhamma mahi, that which is of nature to die may it not die.

24
00:03:18,460 --> 00:03:29,500
Kaya dhamma mahi yi, that which is of nature too, be destroyed, may it not be destroyed.

25
00:03:29,500 --> 00:03:37,500
Kaya dhamma mahi, that which is subject to decay may it not decay.

26
00:03:37,500 --> 00:03:46,500
Nasana dhamma mahi nasi, number five, that which is subject to destruction.

27
00:03:46,500 --> 00:03:52,500
So Kaya is more like decay or wasting away.

28
00:03:52,500 --> 00:04:09,500
And destruction or nullification.

29
00:04:09,500 --> 00:04:15,500
The point being here, the interesting thing about this is actually worth.

30
00:04:15,500 --> 00:04:38,500
This is very much worth stopping at and coming over is that no matter how hard we try we can't fix and we can't perfect life.

31
00:04:38,500 --> 00:04:51,500
There are certain realities that we have to face that we'll never be able to obtain a state of being whereby we always get what we want.

32
00:04:51,500 --> 00:04:58,500
We always have what we want so we can keep what we want and only what we want.

33
00:04:58,500 --> 00:05:08,500
So we aren't faced with realities that are unpleasant to us.

34
00:05:08,500 --> 00:05:19,500
That reality is not based on our desires.

35
00:05:19,500 --> 00:05:29,500
This is an ordinary person, someone who doesn't realize this, people living in the world.

36
00:05:29,500 --> 00:05:38,500
For them when it's subject to old age, it grows old, their body gets old, their loved ones grow old.

37
00:05:38,500 --> 00:05:50,500
Our sessions age.

38
00:05:50,500 --> 00:05:53,500
Because they don't see that this isn't part of nature.

39
00:05:53,500 --> 00:06:01,500
We were talking about last night about funerals because we're not clearly aware of this as being a part of nature.

40
00:06:01,500 --> 00:06:04,500
We suffer because of old age.

41
00:06:04,500 --> 00:06:08,500
We suffer because of sickness, we suffer because of death.

42
00:06:08,500 --> 00:06:10,500
We suffer unnecessarily.

43
00:06:10,500 --> 00:06:22,500
We suffer based not simply on the nature of the experience but based on our expectations and our delusions that it could somehow be otherwise.

44
00:06:22,500 --> 00:06:36,500
We were very hard to avoid death, to avoid old age, to avoid sickness.

45
00:06:36,500 --> 00:07:00,500
All right, I'm obsessed with you.

