1
00:00:00,000 --> 00:00:06,280
Your teaching makes Buddhism clear and accessible to me, maybe partly because there's something

2
00:00:06,280 --> 00:00:08,840
Western in your perspective.

3
00:00:08,840 --> 00:00:13,360
So I wonder how individual or standardized teaching is for Buddhists.

4
00:00:13,360 --> 00:00:18,720
I mean, in Hinduism, grow is pretty much defined and religion, but in Christianity, preaching

5
00:00:18,720 --> 00:00:22,640
can follow pre-made outlines, just looking for some context.

6
00:00:22,640 --> 00:00:26,160
How personal is your take on the Buddhist teaching?

7
00:00:26,160 --> 00:00:29,680
That's interesting.

8
00:00:29,680 --> 00:00:39,120
I don't personally think I'm very Western in my outlook.

9
00:00:39,120 --> 00:00:47,040
I know there are many Western teachers who have adapted the Buddhist teaching or even

10
00:00:47,040 --> 00:00:50,080
discarded parts of the Buddhist teaching.

11
00:00:50,080 --> 00:01:01,160
And not only that, but even more so have discarded the conventional ways of teaching.

12
00:01:01,160 --> 00:01:11,760
Because the Buddha's teaching comes with an incredibly rigid set of teaching methods.

13
00:01:11,760 --> 00:01:16,760
And of course, not saying this is how you have to teach, but maybe rigid isn't the

14
00:01:16,760 --> 00:01:17,760
word.

15
00:01:17,760 --> 00:01:24,040
And how to teach the Buddha's teaching, it's all laid out, and it's mind boggling,

16
00:01:24,040 --> 00:01:26,640
how detailed it is.

17
00:01:26,640 --> 00:01:35,000
There are books this thick, huge books on how the Buddha's teaching is to be expounded,

18
00:01:35,000 --> 00:01:37,000
and how the Buddha laid out his teaching.

19
00:01:37,000 --> 00:01:43,240
These are common terms, explaining how it is that Buddhism is to be spread and was spread

20
00:01:43,240 --> 00:01:46,600
by the Buddha.

21
00:01:46,600 --> 00:01:52,120
And there are a lot of Western teachers who throw these out the window, who say things

22
00:01:52,120 --> 00:01:57,440
like the Abhidhamma Distorts is a distortion of the Buddha's teaching came later.

23
00:01:57,440 --> 00:02:02,960
People who say the commentators were not meditating, they were just study monks and had

24
00:02:02,960 --> 00:02:09,720
no clue about meditation and ended up with wrong ideas and so on.

25
00:02:09,720 --> 00:02:15,360
So for me, all of that and much, much more is what it means to have a Western outlook

26
00:02:15,360 --> 00:02:20,200
on the Buddha's teaching, and I don't have that, I don't think.

27
00:02:20,200 --> 00:02:33,240
I do have, personally, I admit that it's possible that some of the things that are passed

28
00:02:33,240 --> 00:02:38,400
off as the Buddha's teaching may not have been actually taught by the Buddha, but I don't

29
00:02:38,400 --> 00:02:43,560
go any further than that, and I don't think that's going beyond what, well even, for

30
00:02:43,560 --> 00:02:49,640
example, the Mahasizaya that would say, of course, he was quite a bit more wise and of course

31
00:02:49,640 --> 00:02:52,320
educated.

32
00:02:52,320 --> 00:02:56,800
But even he would say at times that this or that is maybe not the Buddha's teaching or

33
00:02:56,800 --> 00:03:01,720
maybe it was extrapolated later or there's something that's a bit of a problem and maybe

34
00:03:01,720 --> 00:03:08,120
there's a mistake being made and so on.

35
00:03:08,120 --> 00:03:16,280
So personally, I take quite seriously the commentaries and of course the Abhidhamma and

36
00:03:16,280 --> 00:03:22,280
all of the Topitika and I start from the basis that it most likely is either with the Buddha

37
00:03:22,280 --> 00:03:25,160
taught or perfectly in line with the Buddha taught.

38
00:03:25,160 --> 00:03:32,000
I start more from that vantage point and personally feeling that I have good reason for

39
00:03:32,000 --> 00:03:44,240
that because any modern commentator, any Western author or any author on Buddhism, trying

40
00:03:44,240 --> 00:03:51,800
to explain Buddhism, is a commentator, is creating a new commentary, you can't but be

41
00:03:51,800 --> 00:03:52,800
a commentator.

42
00:03:52,800 --> 00:03:58,120
So when a person says, oh, don't focus on the commentary is only focus on the Topitika

43
00:03:58,120 --> 00:04:02,160
and the Topitika tells you to do this and this and this, so practice this and that's

44
00:04:02,160 --> 00:04:07,080
and that's, they're just creating a new commentary, they're taking the Buddha's teaching

45
00:04:07,080 --> 00:04:12,840
and explaining it in a specific way, they're interpreting it and saying, see, therefore

46
00:04:12,840 --> 00:04:17,840
the commentary is wrong but that's exactly what the commentary did.

47
00:04:17,840 --> 00:04:25,640
So personally, it makes much more sense to me from my point of view to take a much more

48
00:04:25,640 --> 00:04:37,160
ancient and more likely to be close to what the Buddha taught and to be taught by enlightened

49
00:04:37,160 --> 00:04:42,200
people because we aren't getting more enlightened people in the world as time goes on,

50
00:04:42,200 --> 00:04:50,120
I wouldn't say we're getting fewer enlightened people and more diverse, wrong ideas about

51
00:04:50,120 --> 00:04:53,480
what the Buddha taught rather than the other way around.

52
00:04:53,480 --> 00:05:00,240
So you know, much better should we focus, should we accept flawed, potentially flawed

53
00:05:00,240 --> 00:05:06,000
ancient teachings than quite likely flawed modern teaching.

54
00:05:06,000 --> 00:05:12,280
Anyway, not this isn't what you ask but just trying to point out that as far as the

55
00:05:12,280 --> 00:05:18,720
core goes, I don't even know how this is important when just talking about myself, aren't

56
00:05:18,720 --> 00:05:19,720
they?

57
00:05:19,720 --> 00:05:22,920
But I think it's important to me because it's important, this outlook is important and

58
00:05:22,920 --> 00:05:30,120
it's something that I feel fairly strongly about that there's far too much freedom of

59
00:05:30,120 --> 00:05:36,960
the core essence of Buddha liberty taken in regards to the core essence of Buddhism people

60
00:05:36,960 --> 00:05:43,800
are very quick to discard and to change and to alter, to suit modern times or specific

61
00:05:43,800 --> 00:05:53,120
societies and the far too quick to use this excuse or many different excuses to either

62
00:05:53,120 --> 00:05:58,840
discard some of the Buddha's teaching or to say it wasn't the Buddha's teaching based

63
00:05:58,840 --> 00:06:03,440
on their own views and opinions and interpretations of the demo saying, oh, it couldn't

64
00:06:03,440 --> 00:06:11,080
have been like that, that's wrong, no, that's not according to my meditation practice,

65
00:06:11,080 --> 00:06:19,000
my meditation practice shows me something different, therefore this teaching must be wrong.

66
00:06:19,000 --> 00:06:24,320
And I think that's important to answering your question because there's a big difference

67
00:06:24,320 --> 00:06:32,160
between taking the same wine and putting it in new wine bottles as opposed to taking

68
00:06:32,160 --> 00:06:39,440
new wine and putting it in new wine bottles, this is something Christian saying, Jesus is supposed

69
00:06:39,440 --> 00:06:44,440
to put new wine in new bottles or something like that and discarded the old wine I don't

70
00:06:44,440 --> 00:06:45,920
remember.

71
00:06:45,920 --> 00:06:55,880
So I often find myself in that position where I'm taking ancient Buddhist teachings

72
00:06:55,880 --> 00:07:05,920
like strictly from the books and kind of disguising them or packaging them quite differently.

73
00:07:05,920 --> 00:07:14,480
And I think this is what makes it seem like one's teaching is adapted or is personal

74
00:07:14,480 --> 00:07:20,280
or is an interpretation or interpretative.

75
00:07:20,280 --> 00:07:27,640
But there is a line that's very difficult to see but that's important not to cross because

76
00:07:27,640 --> 00:07:32,400
it's hard to see the difference between an ancient teaching that is expressed, suppose

77
00:07:32,400 --> 00:07:37,800
it were expressed perfectly in a modern western context, perfectly in the sense of without

78
00:07:37,800 --> 00:07:45,640
distorting any of the meaning and the teaching in a modern context or packaging that

79
00:07:45,640 --> 00:07:53,280
is actually a deteriorating or a perversion even of that ancient teaching.

80
00:07:53,280 --> 00:07:59,760
And I strive to obviously not do the latter and I think there's, as I said, much too

81
00:07:59,760 --> 00:08:09,200
much freedom so I think that sort of addresses the general subject that you're talking

82
00:08:09,200 --> 00:08:17,360
about about whether a teaching should be or whether it is personal or whether it's a part

83
00:08:17,360 --> 00:08:21,560
of the orthodoxy or an orthodox way of teaching.

84
00:08:21,560 --> 00:08:26,440
I'm more inclined towards teaching in an orthodox manner, I wouldn't be very interested

85
00:08:26,440 --> 00:08:31,280
in teaching something that was based on my own practice, for example.

86
00:08:31,280 --> 00:08:36,840
If I can use my own practice as an example of my teacher's practice or the Buddha's

87
00:08:36,840 --> 00:08:41,640
teaching, then great and if it can reaffirm it then great.

88
00:08:41,640 --> 00:08:50,440
But I would much rather, especially since it seems to have been quite beneficial for

89
00:08:50,440 --> 00:08:58,640
myself and for other people, to take the very orthodox Buddhist teachings and to teach

90
00:08:58,640 --> 00:09:07,840
in the very, very same way that my teacher taught me but interpreting and translating it

91
00:09:07,840 --> 00:09:13,400
into words that other people can hear, like a good example is this booklet that I wrote

92
00:09:13,400 --> 00:09:19,240
and nowhere in the booklet, maybe in the footnotes.

93
00:09:19,240 --> 00:09:23,640
In the footnotes, it doesn't tell you that this is what the Buddha taught or this comes

94
00:09:23,640 --> 00:09:24,840
from this student or that student.

95
00:09:24,840 --> 00:09:28,760
I think in the footnote I mentioned a couple of places but actually it's all of things

96
00:09:28,760 --> 00:09:35,040
that either I've heard my teacher tell me in list form or else it's something that is

97
00:09:35,040 --> 00:09:37,560
taken from the actual students themselves.

98
00:09:37,560 --> 00:09:41,120
I think there's an important distinction there and I wouldn't want to have.

99
00:09:41,120 --> 00:09:46,240
I hope that my take on Buddhism is not personal because to me that's dangerous, I don't

100
00:09:46,240 --> 00:09:54,240
feel qualified obviously to have the perfect take on Buddhism, so I'd much rather be

101
00:09:54,240 --> 00:10:03,920
really a catalyst or conduit for what I feel to be the pure teachings of Buddha.

102
00:10:03,920 --> 00:10:07,840
I hope that helps.

103
00:10:07,840 --> 00:10:23,840
You said it all?

