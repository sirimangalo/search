1
00:00:00,000 --> 00:00:23,600
When we are going to overcome the dangers of Samsara, there is another teaching which is

2
00:00:23,600 --> 00:00:38,520
the wall or the fortress. We have seven dhammas or seven teachings which become a fortress

3
00:00:38,520 --> 00:00:46,320
for us, a guard for us during the time that we practice, a way for us to overcome or to avoid

4
00:00:46,320 --> 00:01:00,840
the dangers, the 12 kinds of danger which we have to face in the future. These seven things

5
00:01:00,840 --> 00:01:07,920
when we practice them, they will be both a guard against the dangers and will be a means

6
00:01:07,920 --> 00:01:21,560
for us to escape or to go beyond the will go beyond ever having to meet with such dangers

7
00:01:21,560 --> 00:01:41,680
again. Number one, guarding our hands, watching our hands or keeping watch on the movements

8
00:01:41,680 --> 00:01:52,520
of our hands, the things which our hands do. Number two, patas anyetho, guarding our feet. Number

9
00:01:52,520 --> 00:02:12,520
three, sanyatatamo, guarding ourselves. Number four, a chatatato, guarding our minds,

10
00:02:12,520 --> 00:02:40,720
guarding what is within guarding our internal self. Number six, sousamajito being internally composed

11
00:02:40,720 --> 00:02:57,120
of well composed inside. Number seven, ako santu sitho being content with being alone.

12
00:02:57,120 --> 00:03:10,680
Number seven, seven protections, a guard or a fortress or a wall which keeps danger out,

13
00:03:10,680 --> 00:03:19,480
keeps suffering away and which leads us to become free from all suffering in the future.

14
00:03:19,480 --> 00:03:24,680
Hatas anyetho means guarding what we do with our hands because most of the good and evil

15
00:03:24,680 --> 00:03:29,880
which we do in the physical realm is with our hands. When we kill it's normally with

16
00:03:29,880 --> 00:03:37,000
our hands, when we steal it's normally with our hands and so on. Many bad things come from

17
00:03:37,000 --> 00:03:42,280
our hands but also in terms of meditation simply watching what our hands are doing because

18
00:03:42,280 --> 00:03:50,520
we use them more often than any part of our physical body or we use them more than most

19
00:03:50,520 --> 00:04:03,080
parts of our physical body. And so we watch what are we doing with our hands when we're eating,

20
00:04:03,080 --> 00:04:24,360
when we're acting, whatever we do, being aware of what our hands of our hands. Most important

21
00:04:24,360 --> 00:04:33,240
guard, more of the most importantly, guard means guard next. Evil unwholesome acts of body that

22
00:04:33,240 --> 00:04:38,600
are done with our hands. This is something which of course protects us from many of the dangers

23
00:04:38,600 --> 00:04:49,640
of guilt or of punishment for doing for wrongdoing. But it's also important for meditators that we

24
00:04:49,640 --> 00:04:56,360
are aware of the movements of our hands, when we're eating, when we're washing our clothes

25
00:04:56,360 --> 00:05:02,200
or brushing our teeth or so on, being aware of what our hands are doing and we can make

26
00:05:02,200 --> 00:05:15,000
acknowledgement as we move. And then padas anetho means watching our feet. So as we act mostly

27
00:05:15,000 --> 00:05:21,800
with our hands, we move around mostly with our feet. So we have to be careful about where we go

28
00:05:21,800 --> 00:05:30,360
and what we're doing. Why are we doing the things we're doing? Be careful not to let our feet

29
00:05:30,360 --> 00:05:42,280
take us into a place which is inappropriate. Let our feet do the walking. It might take us to make

30
00:05:42,280 --> 00:05:50,520
it easily take us to a place which is inappropriate. And most often for meditators when we walk,

31
00:05:53,400 --> 00:06:00,680
it's easy for our minds to wander into what they see and into the new experiences as we walk

32
00:06:00,680 --> 00:06:05,400
around. So it's important that when we're walking as meditators that we're aware that we're walking,

33
00:06:05,400 --> 00:06:13,960
wherever we walk, when we walk around the center or in ordinary life, when we walk around

34
00:06:13,960 --> 00:06:21,400
our various destinations that we're aware of our feet, walking, walking, walking. Keep our minds

35
00:06:21,400 --> 00:06:26,600
in with the present moment so that they don't get caught up by the new experiences, the phenomena

36
00:06:26,600 --> 00:06:36,920
which arise which come in as we walk around as we go to a new place. If we guard our feet in

37
00:06:36,920 --> 00:06:45,800
this way, we can overcome any dangers as well. We don't let ourselves be taken away into places

38
00:06:45,800 --> 00:06:50,920
which are inappropriate. Guarding our feet doesn't just mean the feet, it means guarding our minds

39
00:06:50,920 --> 00:06:59,960
and guarding the place where we go, not letting ourselves go into the wrong place, but it also

40
00:06:59,960 --> 00:07:07,320
means guarding against all evil deeds which can be done with the feet. And the number three,

41
00:07:07,320 --> 00:07:15,240
why just unmute the watching our speech? So in ordinary life, this means guarding against

42
00:07:15,240 --> 00:07:23,720
telling lies or harsh speech, divisive speech or useless speech. Guarding against all evil

43
00:07:23,720 --> 00:07:30,200
deeds which are done when we say bad things to people or about people behind their backs,

44
00:07:31,880 --> 00:07:39,000
when we try to deceive people or hurt other people with speech. But in meditation practice,

45
00:07:39,000 --> 00:07:46,520
it goes even further and of course meditators are expected to talk as little as possible,

46
00:07:46,520 --> 00:07:53,080
to talk only as necessary when it's important to speak and speak when it's not necessary to speak,

47
00:07:53,080 --> 00:08:05,640
there's no reason except in the pleasure which comes from speaking, we avoid the refrain from

48
00:08:05,640 --> 00:08:14,280
such speech. The reason being was as we talk and talk and if we tend to speak too much,

49
00:08:14,280 --> 00:08:19,880
we find ourselves becoming distracted and find more and more thoughts arising and our minds

50
00:08:19,880 --> 00:08:25,480
are not able to calm down and this can be a danger. This is a way to fall into some of the dangers.

51
00:08:26,760 --> 00:08:31,000
It's easy for us to get lost back in the world, to get caught up in the world again,

52
00:08:31,000 --> 00:08:40,280
based on our speech, based on simply talking more than is required, find ourselves talking about

53
00:08:40,280 --> 00:08:45,800
things which are unrelated to meditation and suddenly we get caught up in a whirlpool or caught

54
00:08:45,800 --> 00:08:56,520
up by a shark or crocodile or something. As I talked about the dangers for someone who is on a boat,

55
00:08:56,520 --> 00:09:06,680
the same of the dangers for wonders is swimming across the ocean, swimming across the ocean of

56
00:09:06,680 --> 00:09:16,600
some sara. We have to watch our hands, our feet and our mouth and number four we have to watch

57
00:09:16,600 --> 00:09:23,000
ourselves. This means apart from these three things we have to watch our whole self. We have to

58
00:09:23,000 --> 00:09:31,400
watch our eyes, guard our eyes, for meditators the best thing and for monks of course as well

59
00:09:31,400 --> 00:09:40,360
is that we watch the ground where we're walking. And Mahasizaya does and even though you

60
00:09:41,640 --> 00:09:51,400
have perfectly good vision, you should act as if you're blind. Even if we can hear perfectly,

61
00:09:51,400 --> 00:09:55,160
we should act as if we're deaf. Even though we have such strength of body,

62
00:09:56,280 --> 00:10:05,400
this is guarding ourselves. We give up this strength of body and we move slowly, we act as if we're

63
00:10:05,400 --> 00:10:16,520
sick, as if we're an old person with very little strength. We act with very little strength,

64
00:10:16,520 --> 00:10:23,720
thinking that if we were to get caught up in our strength or get caught up in rapid movements,

65
00:10:24,520 --> 00:10:30,680
we would lose our concentration in our mindfulness. It could be a danger for us in our practice.

66
00:10:32,120 --> 00:10:37,480
Of course there are many evils which come from not guarding oneself. If we don't guard where we

67
00:10:37,480 --> 00:10:44,920
look, if we don't guard what we do with our body, we can end up in great danger,

68
00:10:44,920 --> 00:10:52,680
both in terms of suffering and in terms of attraction, we can fall into desire for things that we

69
00:10:52,680 --> 00:11:01,240
see or in intoxication with our own strength. We have to guard ourselves and be aware of our

70
00:11:01,240 --> 00:11:07,880
body as it's moving when we bend, we acknowledge bending when we stretch stretching, when we turn

71
00:11:07,880 --> 00:11:16,280
around turning, when we reach for something reaching and the sixth sense is seeing, hearing,

72
00:11:16,280 --> 00:11:24,280
smelling, tasting, feeling, I think this is guarding ourselves. Number five is being internally,

73
00:11:24,280 --> 00:11:32,040
guarding ourselves internally means guarding the heart. So once we have a watch,

74
00:11:32,040 --> 00:11:39,320
we have a good hold on on all of the external parts of what makes us who we are,

75
00:11:40,040 --> 00:11:46,680
then we guard also internally, we guard our mind because even if we're not doing anything bad with

76
00:11:46,680 --> 00:11:53,640
our hands or our feet or any other part of the body, what our speech, if our mind is distracted

77
00:11:53,640 --> 00:12:04,280
or is angry or is lustful, greedy or deluded, we have wrong view or conceit or holding on to

78
00:12:04,280 --> 00:12:09,880
wrong ideas or holding on to ourselves, judging ourselves or other people.

79
00:12:12,280 --> 00:12:17,160
These can be of course a great danger for us, we lead us into great suffering and we'll of

80
00:12:17,160 --> 00:12:25,720
course later lead us to do bad things in the future by speech or action. So we have to guard

81
00:12:25,720 --> 00:12:30,600
internally and when our mind goes off into bad things, we have to catch it. When we like something,

82
00:12:30,600 --> 00:12:37,240
we say liking, liking, if we don't like something, we say disliking, disliking. We feel drowsy or

83
00:12:37,240 --> 00:12:43,720
lazy, lazy, lazy, lazy, if we feel acted, distracted. We have doubts down about this or that or

84
00:12:43,720 --> 00:12:50,280
ourselves or the practice, doubting, doubting. Guarding ourselves, guarding ourselves internally

85
00:12:51,240 --> 00:12:57,720
from falling into evil and wholesome states. And number six, Susemahito means

86
00:12:59,480 --> 00:13:05,560
developing, not only should we guard our minds against unwholesome states, we should develop in

87
00:13:05,560 --> 00:13:13,160
our minds all sorts of wholesome states, meaning we should practice tranquility, meditation,

88
00:13:13,160 --> 00:13:19,080
and we should practice insight meditation. Or if we have only short time, then we can practice

89
00:13:19,080 --> 00:13:25,560
simply insight meditation. But at any rate, we have to practice to calm our minds and to develop

90
00:13:25,560 --> 00:13:30,680
insight and characteristics which are impermanence, suffering and non-self.

91
00:13:32,360 --> 00:13:41,160
This is Susemahito, we develop a sense of awareness, of calm and peaceful and equanimus,

92
00:13:41,160 --> 00:13:49,960
equanimus awareness, where we don't look at things as good or bad, we don't have any more judgment

93
00:13:49,960 --> 00:13:56,760
for things our mind is balanced and as well composed and as very focused internally. Seeing

94
00:13:56,760 --> 00:14:03,320
everything is simply phenomena which arise and sees, and sees that every moment. We're not holding

95
00:14:03,320 --> 00:14:10,360
onto anything as permanent as satisfying or as so. This is the development of the practice of the

96
00:14:10,360 --> 00:14:17,240
four foundations of mindfulness which we're undertaking. And the last weapon or the last

97
00:14:18,200 --> 00:14:24,280
guard which we have is being content with seclusion, being happy, being alone.

98
00:14:26,520 --> 00:14:35,000
When we are content with staying to ourselves without getting involved or

99
00:14:35,000 --> 00:14:43,640
actively involved with other people, this is the time when we can really come to see the truth

100
00:14:43,640 --> 00:14:50,280
about ourselves. But if we're thinking about or talking to or being around other people, it's

101
00:14:50,280 --> 00:14:56,280
very difficult for us to see our own emotions or our own problems because we're always looking at

102
00:14:56,280 --> 00:15:03,400
the faults and the problems of other people. And as long as we look at the faults and the problems

103
00:15:03,400 --> 00:15:08,360
of other people, it's very difficult for us to come to terms with our own faults and our own

104
00:15:09,000 --> 00:15:22,120
problems. For this reason the blood buddha was always very appreciative of solitude,

105
00:15:22,120 --> 00:15:33,720
finding a place and finding time to look inside to see ourselves. And the blood buddha was always

106
00:15:35,320 --> 00:15:45,080
praising in solitude whenever we could find time or find a place to be alone and not have to

107
00:15:45,080 --> 00:15:52,280
fall in with getting involved with people through love or lust, through desire or attraction

108
00:15:53,320 --> 00:15:59,480
or out of anger or frustration that other people trying to change them or improve them

109
00:16:00,680 --> 00:16:08,440
or fix their problems and their faults. When we stay to ourselves then we only have our

110
00:16:08,440 --> 00:16:15,640
own problems to deal with and this makes it very easy for us, we're much easier for us to improve

111
00:16:15,640 --> 00:16:20,600
ourselves. This is a weapon if we get involved with other people that can be a great danger,

112
00:16:21,160 --> 00:16:27,880
they'll be a cause for a great danger to arise. So these seven things they go along with the teaching

113
00:16:27,880 --> 00:16:35,400
on the 12 dangers, these are our weapons guarding our hands, really truly watching what our

114
00:16:35,400 --> 00:16:41,240
hands are doing, guarding our feet, watching where we're going, guarding our speech and trying

115
00:16:41,240 --> 00:16:48,120
not to say a lot or say things which are an important. Regarding ourselves the rest of our body,

116
00:16:48,920 --> 00:16:55,960
guarding our mind internally and developing our mind in terms of tranquility and wisdom to

117
00:16:55,960 --> 00:17:04,920
insight and keeping to ourselves, staying in solution, one of our greatest weapons is a wee

118
00:17:04,920 --> 00:17:14,440
wake, a wood tap, the weapon of solitude. So this is the teaching that I've given for today.

119
00:17:14,440 --> 00:17:30,360
Now we'll continue with mindful frustration walking in Sydney.

