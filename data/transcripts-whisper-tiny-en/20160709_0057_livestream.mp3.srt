1
00:00:00,000 --> 00:00:25,260
Good evening everyone, broadcasting live July 8th, today's quote about sickness, two

2
00:00:25,260 --> 00:00:37,240
kinds of sickness. It's a nice little chord, two kinds of sickness, physical and mental.

3
00:00:37,240 --> 00:01:00,400
Guy, go to Rook, go, J. Desi, go to Rook, and he says, there are differences with physical

4
00:01:00,400 --> 00:01:15,400
sickness. You see people without physical sickness for days, weeks, months, years on end.

5
00:01:15,400 --> 00:01:27,400
But nobody escapes mental sickness even for a moment, except for those who are in light.

6
00:01:27,400 --> 00:01:37,400
Mental illness is something that most people that are plagued by, a moment after, a moment,

7
00:01:37,400 --> 00:01:55,400
day in and day out. It's a type of sickness that everyone carries with them.

8
00:01:55,400 --> 00:02:04,400
So in Buddhism, mental illness isn't simply the extreme cases of clinical depression,

9
00:02:04,400 --> 00:02:16,400
or anxiety, OCD, or phobias, psychoses, all of these things are just extreme.

10
00:02:16,400 --> 00:02:31,400
Extreme to the extent that an ordinary and instructed person can recognize them and latch

11
00:02:31,400 --> 00:02:44,400
onto them and identify them as a problem. But most of the time we don't recognize and identify

12
00:02:44,400 --> 00:03:01,400
our mental illnesses from what they are, just illness and discord, distress,

13
00:03:01,400 --> 00:03:10,400
to become the meditate. Many people, when they begin to meditate for the first time,

14
00:03:10,400 --> 00:03:19,400
it's a revelation, a realization that our minds aren't quite as healthy as we thought they were.

15
00:03:19,400 --> 00:03:48,400
That our minds are plagued by illness, greed, illness of anger, illness of division, illnesses.

16
00:03:48,400 --> 00:03:57,400
That really is the illness and the problem that we should focus on and should understand Buddhism

17
00:03:57,400 --> 00:04:06,400
to focus on more so than simply suffering.

18
00:04:06,400 --> 00:04:08,400
More important is the cause of suffering.

19
00:04:08,400 --> 00:04:19,400
The suffering will, we can live with it, but it is possible to be at peace with what we would normally term suffering.

20
00:04:19,400 --> 00:04:28,400
There's no peace for one who has mental illness. The suffering comes because we have mental illness.

21
00:04:28,400 --> 00:04:35,400
It's really suffering. It becomes suffering because of our mental illness.

22
00:04:35,400 --> 00:04:46,400
Because of our greed, our anger, our division.

23
00:04:46,400 --> 00:04:50,400
We'll put in another place that's on a subject of sickness.

24
00:04:50,400 --> 00:04:58,400
That's about four types of sickness. There's sickness from utu, which means the environment.

25
00:04:58,400 --> 00:05:19,400
Sickness from food, ahara, sickness from the mind, jita, or jita, sickness from karma.

26
00:05:19,400 --> 00:05:29,400
Four types of sickness, so another little more detail. Physical sickness is either from the environment,

27
00:05:29,400 --> 00:05:36,400
or from what is around you, or from what you take inside of you.

28
00:05:36,400 --> 00:05:50,400
So things like viruses and radiation and environmental distress and environmental discomfort, these would be utu.

29
00:05:50,400 --> 00:05:58,400
Ahara, from food, what we eat and drink causes a sickness.

30
00:05:58,400 --> 00:06:02,400
These are the causes of physical sickness.

31
00:06:02,400 --> 00:06:13,400
Mental sickness caused by the mind or caused by karma, which is basically the same thing, but karma means in the past.

32
00:06:13,400 --> 00:06:21,400
So you've done nasty things in the past.

33
00:06:21,400 --> 00:06:27,400
As a result, you have physical and mental impairment.

34
00:06:27,400 --> 00:06:35,400
Why people are born with certain disabilities.

35
00:06:35,400 --> 00:06:47,400
Why we suddenly get cancer, or any debilitating disease, multiple sclerosis, etc.

36
00:06:47,400 --> 00:06:58,400
Not to mention the diseases that come from our greed.

37
00:06:58,400 --> 00:07:07,400
The fourth one from the mind means simply the sickness that comes from being mentally sick, from being angry.

38
00:07:07,400 --> 00:07:12,400
Anytime we're angry, this is anger, this is considered a mental illness.

39
00:07:12,400 --> 00:07:17,400
The fact that we have anger is considered to be a mental illness.

40
00:07:17,400 --> 00:07:24,400
When we are addicted to things, when we want things, just wanting something, it's considered to be mental illness.

41
00:07:24,400 --> 00:07:26,400
It's a wrong in the mind.

42
00:07:26,400 --> 00:07:29,400
It's a cause of suffering for the mind.

43
00:07:29,400 --> 00:07:35,400
It's a cause of stress and distress.

44
00:07:35,400 --> 00:07:41,400
Anytime we have delusion, arrogance, conceit.

45
00:07:41,400 --> 00:07:48,400
Or the fact that we have these in our mind, this is a cause for illness.

46
00:07:48,400 --> 00:07:55,400
It's a type of illness.

47
00:07:55,400 --> 00:07:59,400
This is the dumb of the Buddha.

48
00:07:59,400 --> 00:08:01,400
This is how we look at things.

49
00:08:01,400 --> 00:08:04,400
It's kind of depressing, I suppose.

50
00:08:04,400 --> 00:08:10,400
I've actually had people say to me that it's not really fair to call these things illness when they're all manageable.

51
00:08:10,400 --> 00:08:20,400
As though there's a categorical difference between someone who is clinically depressed and someone who is just depressed.

52
00:08:20,400 --> 00:08:21,400
There isn't.

53
00:08:21,400 --> 00:08:24,400
There's not a categorical difference.

54
00:08:24,400 --> 00:08:29,400
You can tell you create a category because it's just a matter of degree.

55
00:08:29,400 --> 00:08:42,400
You can eventually, the perpetuation, the feedback loop that becomes so strong that it becomes unmanageable.

56
00:08:42,400 --> 00:08:50,400
You dislike something or you depress about something and that makes you depressed and more depressed and depressed.

57
00:08:50,400 --> 00:08:56,400
You enter into this feedback loop where you're actually feeding the depression with depression.

58
00:08:56,400 --> 00:09:00,400
You get angry and then you're angry at the fact that someone's made you angry.

59
00:09:00,400 --> 00:09:06,400
You just do when you're anger and it becomes unmanageable.

60
00:09:06,400 --> 00:09:13,400
Anxiety, you become anxious that you're anxious, afraid of your fear.

61
00:09:13,400 --> 00:09:21,400
You just feed them and by feeding them you end up clinical.

62
00:09:21,400 --> 00:09:31,400
It's just a matter of degree and so certainly something that we should be concerned about and we should do it with the gym towards.

63
00:09:31,400 --> 00:09:44,400
So it doesn't become unmanageable.

64
00:09:44,400 --> 00:09:53,400
There are no questions today.

65
00:09:53,400 --> 00:09:58,400
I wonder if we have questions from yesterday or anything.

66
00:09:58,400 --> 00:10:03,400
I'm going to go have a longer argument here.

67
00:10:03,400 --> 00:10:11,400
I haven't read it but I don't know about the talk.

68
00:10:11,400 --> 00:10:14,400
Why is entertainment dangerous?

69
00:10:14,400 --> 00:10:20,400
Understanding the nature of it being impermanence, suffering, and on-south what makes it dangerous?

70
00:10:20,400 --> 00:10:23,400
Attachment.

71
00:10:23,400 --> 00:10:32,400
You've got some good talk, I suppose.

72
00:10:32,400 --> 00:10:35,400
I'm not going to read through it all.

73
00:10:35,400 --> 00:10:46,400
I really think you should put the cue before the question otherwise it's hard to go through here.

74
00:10:46,400 --> 00:10:56,400
I'm not going to go back to all that talk to find questions.

75
00:10:56,400 --> 00:11:02,400
So to make a question, click on the yellow or green question mark.

76
00:11:02,400 --> 00:11:07,400
That puts the right tang before your question and I can easily identify them.

77
00:11:07,400 --> 00:11:20,400
Command satisfying lack of progress and meditation, my lack of competence and seeing the hindrance is being due to come.

78
00:11:20,400 --> 00:11:28,400
You haven't dealt about your practice, dealt about your progress because that's a hindrance.

79
00:11:28,400 --> 00:11:33,400
It's a doubting doubting.

80
00:11:33,400 --> 00:11:40,400
It may be not easy to understand what is the progress but if you're practicing correctly it's hard to imagine that you're not progressing.

81
00:11:40,400 --> 00:11:59,400
You're not wanting to see yourself clearly and refine your character.

82
00:11:59,400 --> 00:12:05,400
When faced with such continued adversary violence, racism in the world today,

83
00:12:05,400 --> 00:12:15,400
how do we teach others to come to understand the same benefits of meditation?

84
00:12:15,400 --> 00:12:28,400
Without seeing it unconcerned with our non-reaction.

85
00:12:28,400 --> 00:12:53,400
I find that it's considered unjust or cruel not to have a fighting opinion when existing in a world that demands outrage over every negative situation that arises.

86
00:12:53,400 --> 00:13:00,400
Well, yes, society has messed up nothing new there.

87
00:13:00,400 --> 00:13:04,400
We esteem all the wrong things.

88
00:13:04,400 --> 00:13:10,400
We esteem addiction for its worth.

89
00:13:10,400 --> 00:13:19,400
We certainly esteem opinions, views, we esteem outrage.

90
00:13:19,400 --> 00:13:29,400
We esteem for things that in the end don't turn out to be all that useful.

91
00:13:29,400 --> 00:13:35,400
We get caught up in things that end up being inconsequential.

92
00:13:35,400 --> 00:14:00,400
We see what is fundamental or essential as being unessential or unessential as being essential.

93
00:14:00,400 --> 00:14:07,400
These are on the Ghatseh.

94
00:14:07,400 --> 00:14:13,400
Meacha gojara sankapi.

95
00:14:13,400 --> 00:14:23,400
Ghatseh sankapi, they don't attain such people don't come to it as essential

96
00:14:23,400 --> 00:14:28,080
Because they're always feeding on the wrong,

97
00:14:28,080 --> 00:14:29,400
feeding in the wrong pasture.

98
00:14:33,280 --> 00:14:35,480
They let their mind wander and minds wander

99
00:14:35,480 --> 00:14:37,240
in the wrong pastures.

100
00:14:37,240 --> 00:15:00,240
So to answer your question, it's not easy to teach yourself, let alone others.

101
00:15:00,240 --> 00:15:05,240
This is why we focus on our own.

102
00:15:05,240 --> 00:15:07,240
You become an example to others.

103
00:15:07,240 --> 00:15:10,240
You'll see a better way.

104
00:15:10,240 --> 00:15:17,240
You can see a way to free themselves and to help the world.

105
00:15:17,240 --> 00:15:21,240
But only if you become an example.

106
00:15:21,240 --> 00:15:30,240
So work on yourself.

107
00:15:30,240 --> 00:15:36,240
If you hold fast, if you hold fast to the fact that you're not

108
00:15:36,240 --> 00:15:44,240
outraged at things, there will be people who appreciate that

109
00:15:44,240 --> 00:15:49,240
and who esteem that and who are affected positively by that.

110
00:15:49,240 --> 00:15:59,240
Those people who get angry at you, well, it says more about them than about you.

111
00:15:59,240 --> 00:16:08,240
Sometimes better to let people who are raging, let them rage.

112
00:16:08,240 --> 00:16:20,240
Yes, but to be patient with an unmoved.

113
00:16:20,240 --> 00:16:25,240
It's really kind of silly to get out ranged about everything when this is sort of thing.

114
00:16:25,240 --> 00:16:29,240
Outrageous things have been sort of the norm of humanity.

115
00:16:29,240 --> 00:16:39,240
To be surprised by them is naive and to fight for that to change.

116
00:16:39,240 --> 00:16:45,240
It's much better to accept it as part of the human condition and to work with it.

117
00:16:45,240 --> 00:16:50,240
In the sense of actually trying to change, not getting angry at the fact

118
00:16:50,240 --> 00:16:54,240
that that's part of humanity, that there's evil in humanity.

119
00:16:54,240 --> 00:17:00,240
But to work on good, to better yourself to better those around you.

120
00:17:00,240 --> 00:17:04,240
To help people to come to let go of their evil inside.

121
00:17:04,240 --> 00:17:09,240
Help people to see that getting angry at bad things is making things worse.

122
00:17:09,240 --> 00:17:15,240
It's just cultivating a habit of anger.

123
00:17:15,240 --> 00:17:17,240
You just end up burnt out.

124
00:17:17,240 --> 00:17:20,240
You don't believe this theory.

125
00:17:20,240 --> 00:17:26,240
Look at those people who get burnt out from activism who get burnt out from social justice.

126
00:17:26,240 --> 00:17:31,240
They don't end up helping people not in any meaningful way.

127
00:17:31,240 --> 00:17:33,240
Well, it's maybe not true.

128
00:17:33,240 --> 00:17:38,240
They do often end up because of the anger.

129
00:17:38,240 --> 00:17:41,240
They often end up because of their determination.

130
00:17:41,240 --> 00:17:43,240
They'll end up changing the system.

131
00:17:43,240 --> 00:17:47,240
You don't even be angry to change the system.

132
00:17:47,240 --> 00:17:53,240
Some of the civil rights movement in America, for example, that we hear about.

133
00:17:53,240 --> 00:17:55,240
A lot of it wasn't very angry.

134
00:17:55,240 --> 00:18:02,240
It was a matter of just having sit-ins and sitting in the wrong place on the bus.

135
00:18:02,240 --> 00:18:05,240
It's to be adamant.

136
00:18:05,240 --> 00:18:08,240
I mean, you still might argue from a Buddhist point of view.

137
00:18:08,240 --> 00:18:21,240
But that sort of thing is much more consumable to a Buddhist.

138
00:18:21,240 --> 00:18:27,240
The Buddha himself gives silent protesters this one case where this king was going.

139
00:18:27,240 --> 00:18:28,240
It's a long story.

140
00:18:28,240 --> 00:18:37,240
And basically, this guy was a bastard son of one of the Buddha's relatives.

141
00:18:37,240 --> 00:18:42,240
And so they wouldn't recognize him.

142
00:18:42,240 --> 00:18:46,240
They didn't want to anger him, so they pretended to recognize him.

143
00:18:46,240 --> 00:18:49,240
And they wouldn't even let him eat at the same table with them.

144
00:18:49,240 --> 00:18:51,240
They were so proud.

145
00:18:51,240 --> 00:19:01,240
And this king got so angry that he was about to kill all the Buddhist relatives.

146
00:19:01,240 --> 00:19:07,240
So he went with his army one time, and the Buddha intercepted him.

147
00:19:07,240 --> 00:19:10,240
And the Buddha was sitting there under a tree.

148
00:19:10,240 --> 00:19:13,240
But the tree had no leaves or very few leaves.

149
00:19:13,240 --> 00:19:16,240
It was a very, it was an almost dead tree.

150
00:19:16,240 --> 00:19:22,240
And the king was marching by, and he was told that the Buddha was sitting there.

151
00:19:22,240 --> 00:19:27,240
And so he got down and went to see the Buddha.

152
00:19:27,240 --> 00:19:34,240
And then he said to the Buddha, he actually had some faith in the Buddha, even though he was a more

153
00:19:34,240 --> 00:19:38,240
mongering king.

154
00:19:38,240 --> 00:19:41,240
And he said to the Buddha, what are you doing?

155
00:19:41,240 --> 00:19:43,240
Better most are sitting under this tree with no shade.

156
00:19:43,240 --> 00:19:45,240
Why don't you find a better tree?

157
00:19:45,240 --> 00:19:48,240
And the Buddha said, oh, that's okay.

158
00:19:48,240 --> 00:19:53,240
I live under the cool shade of my relatives.

159
00:19:53,240 --> 00:19:58,240
So he didn't even go to the king and don't look your wrong, stop it.

160
00:19:58,240 --> 00:20:09,240
And instead he thought to remind the king that his relatives had good in them.

161
00:20:09,240 --> 00:20:17,240
They may have been pompous jerks, but they were still people.

162
00:20:17,240 --> 00:20:23,240
They were still a support to humanity in some way, a support to the Buddha in some way.

163
00:20:23,240 --> 00:20:34,240
So the king, he realized the Buddha benefits from his relatives.

164
00:20:34,240 --> 00:20:40,240
So he turned around, called off the war, except he couldn't keep down his anger.

165
00:20:40,240 --> 00:20:45,240
So he ended up heading off to war again, and again, the Buddha did the same thing.

166
00:20:45,240 --> 00:20:48,240
The third time, it's really interesting story.

167
00:20:48,240 --> 00:20:52,240
The third time the king says, look, I'm just going to do it.

168
00:20:52,240 --> 00:20:57,240
And the Buddha sort of reflected on the situation and didn't go.

169
00:20:57,240 --> 00:21:01,240
The other side of it wasn't going to come to a good end.

170
00:21:01,240 --> 00:21:04,240
There was nothing he could do to stop it.

171
00:21:04,240 --> 00:21:10,240
And many of the Buddha's relatives were wiped out, some majority of them.

172
00:21:10,240 --> 00:21:13,240
Because the Buddha just let it happen.

173
00:21:13,240 --> 00:21:15,240
And the end he felt he couldn't.

174
00:21:15,240 --> 00:21:17,240
Because he would have had to get angry.

175
00:21:17,240 --> 00:21:19,240
He would have had to get upset.

176
00:21:19,240 --> 00:21:28,240
In the sense that you can't stop people from dying.

177
00:21:28,240 --> 00:21:32,240
You can't stop bad things from happening.

178
00:21:32,240 --> 00:21:36,240
What we have to stop is things like anger.

179
00:21:36,240 --> 00:21:38,240
And it's getting angry doesn't help.

180
00:21:38,240 --> 00:21:42,240
It sends the wrong message.

181
00:21:42,240 --> 00:21:45,240
You know, much more important is to send the message.

182
00:21:45,240 --> 00:21:47,240
Stop killing each other.

183
00:21:47,240 --> 00:21:53,240
To actually try and stop one killing or one catastrophe.

184
00:21:53,240 --> 00:21:55,240
It's not all that important.

185
00:21:55,240 --> 00:21:58,240
It's beings are in cycle.

186
00:21:58,240 --> 00:22:00,240
They're born in dying, born in dying.

187
00:22:00,240 --> 00:22:04,240
It's part of a circle of life.

188
00:22:04,240 --> 00:22:12,240
But the mind doesn't die from it.

189
00:22:12,240 --> 00:22:13,240
It dies every moment.

190
00:22:13,240 --> 00:22:18,240
But the mind of a person keeps going.

191
00:22:18,240 --> 00:22:27,240
Anyway, sickness.

192
00:22:27,240 --> 00:22:32,240
All right, we're into questions.

193
00:22:32,240 --> 00:22:35,240
There's only one or two questions.

194
00:22:35,240 --> 00:22:39,240
Must have answered all the questions last night.

195
00:22:39,240 --> 00:22:47,240
In that case, I have a good evening, everyone.

196
00:22:47,240 --> 00:22:49,240
Tomorrow I might be busy.

197
00:22:49,240 --> 00:22:51,240
I should be here.

198
00:22:51,240 --> 00:22:53,240
I should be here.

199
00:22:53,240 --> 00:22:56,240
But if I'm not here some days, my apologies.

200
00:22:56,240 --> 00:22:58,240
I should be here.

201
00:22:58,240 --> 00:23:01,240
Anyway, have a good night.

202
00:23:01,240 --> 00:23:03,240
Thank you.

