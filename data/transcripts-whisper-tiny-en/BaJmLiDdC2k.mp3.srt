1
00:00:00,000 --> 00:00:02,800
Hi, and welcome back to Ask a Monk.

2
00:00:02,800 --> 00:00:08,900
Today, we have a question from German 1184.

3
00:00:08,900 --> 00:00:12,800
Yutadama, how much emphasis does your tradition put on book learning?

4
00:00:12,800 --> 00:00:18,900
In Tibet, the practice seems to have much memorization of root texts and commentaries

5
00:00:18,900 --> 00:00:20,900
and the debating upon them.

6
00:00:20,900 --> 00:00:23,100
Some people approach the practice in this way.

7
00:00:23,100 --> 00:00:27,500
What do you think?

8
00:00:27,500 --> 00:00:37,000
Well, I think that no one would disagree that book learning has its place.

9
00:00:37,000 --> 00:00:45,600
I've often complained that even in the tradition that I follow, in many of the traditions

10
00:00:45,600 --> 00:00:53,300
of Buddhism, there is too much of an emphasis on textual study as an end more than

11
00:00:53,300 --> 00:00:59,300
a means.

12
00:00:59,300 --> 00:01:08,100
Ideally, we see textual study as similar to studying a roadmap, learning about the

13
00:01:08,100 --> 00:01:18,000
paths, about the way we're going to go, or a modern example would be learning how to drive

14
00:01:18,000 --> 00:01:19,000
a car.

15
00:01:19,000 --> 00:01:24,300
So, you can study a roadmap as much as you want, you can learn how to drive, you can learn

16
00:01:24,300 --> 00:01:29,700
everything, all the tricks of the road, everything there is to know about traveling,

17
00:01:29,700 --> 00:01:34,700
but if you never take the journey, you'll never get anywhere.

18
00:01:34,700 --> 00:01:38,700
I think this has to be said about textual study.

19
00:01:38,700 --> 00:01:50,200
The other is some argument to be made for the intellectual appreciation of the teachings,

20
00:01:50,200 --> 00:02:01,200
the understanding of the teaching intellectually, and the ability to use the teachings directly

21
00:02:01,200 --> 00:02:08,500
as you're studying to examine yourself, and there is, to a certain level, this is possible.

22
00:02:08,500 --> 00:02:14,200
It's possible that as you're reading a teaching or as you're listening to a talk that

23
00:02:14,200 --> 00:02:20,600
you're actually also reflecting on the teachings and using them practically.

24
00:02:20,600 --> 00:02:27,000
But as I said, I think there are many traditions and not exactly traditions.

25
00:02:27,000 --> 00:02:33,000
I would say monasteries, teachers, being more specific than talking about any specific

26
00:02:33,000 --> 00:02:40,400
type of Buddhism, but there are many Buddhists who like to get together in a group and debate

27
00:02:40,400 --> 00:02:53,000
or study or discuss a teaching, and think of that as a fundamental part of their practice.

28
00:02:53,000 --> 00:03:02,000
It's useful to an extent, but if you find a group or a teacher who is only going so

29
00:03:02,000 --> 00:03:06,800
far as to give textual study, then you have to say that there is something missing

30
00:03:06,800 --> 00:03:09,600
from the teaching.

31
00:03:09,600 --> 00:03:16,200
In the end, studying, as an end, simply makes you better at studying.

32
00:03:16,200 --> 00:03:23,100
The more you study, the better you are memorizing, the better you are, and logic, being

33
00:03:23,100 --> 00:03:31,200
able to assimilate teachings and make sense of words and syntax and so on.

34
00:03:31,200 --> 00:03:35,400
You assimilate it into your brain and get the concepts, and it can make you a good teacher,

35
00:03:35,400 --> 00:03:43,800
it can make you very confident of yourself because of your learning.

36
00:03:43,800 --> 00:03:48,400
If you're good at debate, when you practice debate, it just makes you better at debate,

37
00:03:48,400 --> 00:03:55,400
it makes you more skilled at argument, at winning people over, at explaining things to people,

38
00:03:55,400 --> 00:04:00,200
at being able to reply to criticisms and so on.

39
00:04:00,200 --> 00:04:07,900
But until you practice meditation, the only way you're going to get better at understanding

40
00:04:07,900 --> 00:04:09,800
reality is to look at reality.

41
00:04:09,800 --> 00:04:16,200
Neither of these things study, debate, or any of the other associated activities helps

42
00:04:16,200 --> 00:04:18,400
you to understand reality.

43
00:04:18,400 --> 00:04:23,000
The only way you can get better at seeing reality for what it is, is to look at it, just

44
00:04:23,000 --> 00:04:24,600
to train yourself.

45
00:04:24,600 --> 00:04:29,200
So the meditation is a very specific action where we're training ourselves to see things

46
00:04:29,200 --> 00:04:30,400
as they are.

47
00:04:30,400 --> 00:04:31,400
It's a training.

48
00:04:31,400 --> 00:04:38,480
It's not intellectual, it's not a realization that comes to you.

49
00:04:38,480 --> 00:04:42,600
It's a change in the way you look at things, so instead of seeing things as me as mine

50
00:04:42,600 --> 00:04:50,200
is good as bad, you actually see them, and you're actually able to experience something

51
00:04:50,200 --> 00:04:55,000
simply as it is, when you see something, you only see it, when you hear something, you

52
00:04:55,000 --> 00:05:00,600
only hear it, when you feel pain, there's only the pain, when you feel happy, there's

53
00:05:00,600 --> 00:05:03,200
only the happiness and so on.

54
00:05:03,200 --> 00:05:08,000
And so it's a skill rather than, it's nothing that can come from someone else, it's nothing

55
00:05:08,000 --> 00:05:09,700
someone else can teach you.

56
00:05:09,700 --> 00:05:13,800
They can only teach you the way of developing the skill and it's up to you to develop

57
00:05:13,800 --> 00:05:17,600
this skill until you're able to do it for yourself.

58
00:05:17,600 --> 00:05:26,600
Once you're able to see reality for what it is, only then can you say that you'll actually

59
00:05:26,600 --> 00:05:32,800
be free from suffering, there's only one way to realize the truth of the Buddha's teaching

60
00:05:32,800 --> 00:05:35,500
and that's through the practice.

61
00:05:35,500 --> 00:05:37,400
So I hope that makes sense.

62
00:05:37,400 --> 00:05:42,200
I think it's generally understood that this is the case, that there are three parts to

63
00:05:42,200 --> 00:05:43,200
the Buddha's teaching.

64
00:05:43,200 --> 00:05:48,200
There's study and you study for the purpose of practicing and you practice for the purpose

65
00:05:48,200 --> 00:05:49,200
of realization.

66
00:05:49,200 --> 00:05:54,800
So there's the study, the practice and the realization.

67
00:05:54,800 --> 00:06:04,100
So I would say there's no place for the accumulation of book knowledge, knowledge is for

68
00:06:04,100 --> 00:06:05,100
the purpose of practice.

69
00:06:05,100 --> 00:06:11,080
If it's not practical, if it's not something that you intend to use either or to help

70
00:06:11,080 --> 00:06:17,080
someone else, then it really isn't the Buddhist learning.

71
00:06:17,080 --> 00:06:22,080
Buddhist learning is for the purpose of putting it into practice, for the purpose of

72
00:06:22,080 --> 00:06:23,780
realizing the truth.

73
00:06:23,780 --> 00:06:27,960
If it's not something that leads you to realize the truth, then it's also not considered

74
00:06:27,960 --> 00:06:30,880
to be proper learning in a Buddhist sense.

75
00:06:30,880 --> 00:06:31,880
Okay, so I hope that helps.

76
00:06:31,880 --> 00:06:48,680
Thanks for the question.

