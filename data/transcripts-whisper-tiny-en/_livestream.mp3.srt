1
00:00:00,000 --> 00:00:03,000
The flavoring of the defilements.

2
00:00:09,300 --> 00:00:13,000
In Poly they say we are slave and dasa, dasa.

3
00:00:14,700 --> 00:00:18,000
And they say tat, tat comes from dasa.

4
00:00:20,800 --> 00:00:24,300
And tat, ginni, tat, dasa, ginni, tasa, dasa.

5
00:00:24,300 --> 00:00:28,300
The defile, a slave of defilements.

6
00:00:38,300 --> 00:00:41,300
When you cultivate this objectivity,

7
00:00:42,300 --> 00:00:47,300
this clarity, this simplicity of mind,

8
00:00:47,300 --> 00:00:50,300
you just see things as they are.

9
00:00:50,300 --> 00:00:56,300
No more making stuff up, no more playing games,

10
00:00:57,300 --> 00:01:02,300
no more deluding yourself about permanence,

11
00:01:02,300 --> 00:01:05,300
satisfaction, control,

12
00:01:05,300 --> 00:01:09,300
three things we want in life, no more of that.

13
00:01:09,300 --> 00:01:13,300
No, let it come, let it go,

14
00:01:13,300 --> 00:01:16,300
watch it come, watch it go.

15
00:01:16,300 --> 00:01:21,300
Free yourself, free yourself.

16
00:01:26,300 --> 00:01:28,300
That's the teaching of the Buddha.

17
00:01:28,300 --> 00:01:30,300
So there you go, that's a half an hour.

18
00:01:33,300 --> 00:01:35,300
Anybody have any questions?

19
00:01:36,300 --> 00:01:38,300
What would good group relate?

20
00:01:38,300 --> 00:01:49,300
So we're broadcasting audio as well, hopefully.

21
00:01:54,300 --> 00:01:57,300
The ambient noise is too loud.

22
00:01:57,300 --> 00:02:00,300
See these volumes things, okay, let's see.

23
00:02:00,300 --> 00:02:02,300
The birds are loud.

24
00:02:02,300 --> 00:02:05,300
My microphone must be quiet.

25
00:02:05,300 --> 00:02:10,300
Let me see here.

26
00:02:13,300 --> 00:02:14,300
This one.

27
00:02:16,300 --> 00:02:17,300
There.

28
00:02:17,300 --> 00:02:18,300
I can't tell.

29
00:02:18,300 --> 00:02:24,300
I mean, the levels here are really not well thought out.

30
00:02:24,300 --> 00:02:26,300
They're not working properly.

31
00:02:27,300 --> 00:02:30,300
So I can't tell which levels are what?

32
00:02:30,300 --> 00:02:35,300
Anyway, I've turned that level way down.

33
00:02:36,300 --> 00:02:39,300
If I could just turn off the second life sound,

34
00:02:39,300 --> 00:02:40,300
you don't really need it.

35
00:02:40,300 --> 00:02:43,300
I thought it would be kind of fun.

36
00:02:47,300 --> 00:02:50,300
But it was too late I've already given the whole talk.

37
00:02:50,300 --> 00:02:53,300
I've probably ruined it with that loud noise.

38
00:02:53,300 --> 00:03:00,300
I think when I do this, it gets quieter and hoping.

39
00:03:16,300 --> 00:03:19,300
Yeah, the live stream thing is not really working.

40
00:03:19,300 --> 00:03:21,300
I think it's our server.

41
00:03:21,300 --> 00:03:24,300
I think the new server we got is kind of crappy.

42
00:03:24,300 --> 00:03:52,300
Yeah, it's dropped off and that means it's recorded like four parts to it.

43
00:03:52,300 --> 00:03:54,300
That shouldn't do that.

44
00:03:54,300 --> 00:03:55,300
There's something wrong with this server.

45
00:03:55,300 --> 00:03:58,300
I think I'm going to go back to broadcasting on our old server.

46
00:03:58,300 --> 00:04:00,300
The Amazon server.

47
00:04:00,300 --> 00:04:08,300
And they'd beg our IT team to look closely at why this new server is not working very well.

48
00:04:08,300 --> 00:04:09,300
It's a new company.

49
00:04:09,300 --> 00:04:15,300
And the new company is not as good as Amazon.

50
00:04:15,300 --> 00:04:22,300
I think it could also be that there's just too many people listening.

51
00:04:30,300 --> 00:04:37,300
I'll try to go back to the old server and see how that goes.

52
00:04:37,300 --> 00:04:41,300
Because we never had a problem with the old server.

53
00:04:41,300 --> 00:04:48,300
And I think there were so many people listening to it.

54
00:04:56,300 --> 00:05:00,300
If you want to do, of course, at our center,

55
00:05:00,300 --> 00:05:05,300
we're pretty booked up until February, I think.

56
00:05:05,300 --> 00:05:18,300
So on our website, there's a calendar. You can go and look at the calendar and see when there's space.

57
00:05:18,300 --> 00:05:32,300
I think, right? I'm told there's a calendar schedule.

58
00:05:32,300 --> 00:05:40,300
So, you don't really have room until it looks like February 1st.

59
00:05:40,300 --> 00:05:44,300
No, not February 1st, because there's more people.

60
00:05:44,300 --> 00:05:51,300
They won't really have room.

61
00:05:51,300 --> 00:06:01,300
What's going on here?

62
00:06:01,300 --> 00:06:08,300
Why is Ian Hawkins, something is wrong?

63
00:06:08,300 --> 00:06:12,300
Who put Ian Hawkins down for like a little feel at him?

64
00:06:12,300 --> 00:06:18,300
In the community state?

65
00:06:18,300 --> 00:06:44,300
But yeah, you have to book February, February, February, I think.

66
00:06:44,300 --> 00:06:49,300
Oh, I made a mistake. I put someone down when there's no room for them.

67
00:06:49,300 --> 00:06:54,300
See? This is what happens when you put me in charge of all this stuff.

68
00:06:54,300 --> 00:06:58,300
That's enough.

69
00:06:58,300 --> 00:07:00,300
So we're overbooked.

70
00:07:00,300 --> 00:07:11,300
I have to fix that.

71
00:07:11,300 --> 00:07:18,300
It's the eighth so you can go and look at it.

72
00:07:41,300 --> 00:07:50,300
You can go and look at it.

73
00:07:50,300 --> 00:08:19,300
You can go and look at it.

74
00:08:19,300 --> 00:08:44,300
You can go and look at it.

75
00:08:44,300 --> 00:09:11,300
You can go and look at it.

76
00:09:11,300 --> 00:09:36,300
Right, sorry, I'm distracted here.

77
00:09:36,300 --> 00:09:40,300
With the time change, all the time is always 9pm here.

78
00:09:40,300 --> 00:09:51,300
So you have to look up on Google, ask Google what time is it in Toronto and figure when 9pm is going to be in Toronto?

79
00:09:51,300 --> 00:09:53,300
9pm Toronto time.

80
00:09:53,300 --> 00:10:08,300
It's changed now because it's changed for some of you because Toronto time has changed as of today.

81
00:10:08,300 --> 00:10:19,300
The second life clients all have it in PST, Pacific Standard Time, or Pacific Time.

82
00:10:19,300 --> 00:10:28,300
It's the time in Los Angeles, so it'll be at 6pm every evening.

83
00:10:28,300 --> 00:10:32,300
How long is the online course?

84
00:10:32,300 --> 00:10:39,300
There's no set time, it takes as long as it takes, we try to meet once a week.

85
00:10:39,300 --> 00:10:43,300
Can I give you a new exercise every week?

86
00:10:43,300 --> 00:10:57,300
Probably takes about 12 weeks I guess, probably around 12 weeks.

87
00:10:57,300 --> 00:11:16,300
14 weeks, finish the whole course and then after you finish that then you have to come and spend at least 10 days doing the rest of the course intensively.

88
00:11:16,300 --> 00:11:20,300
That's the end person part, at least 10 nights.

89
00:11:20,300 --> 00:11:27,300
The Sunday study group, we're studying the MECD manga, we're getting near the end now.

90
00:11:27,300 --> 00:11:36,300
All we do is read through it, mainly I add some comments once in a while.

91
00:11:36,300 --> 00:11:59,300
But most of it is just reading.

92
00:11:59,300 --> 00:12:14,300
Thank you very much.

93
00:12:14,300 --> 00:12:37,300
Thank you.

94
00:12:37,300 --> 00:13:03,300
Thank you.

95
00:13:03,300 --> 00:13:29,300
Thank you.

96
00:13:29,300 --> 00:13:37,300
Alright, so the study course is moving to Discord, I think.

97
00:13:37,300 --> 00:13:42,300
We've been using Mumble, we're going to try a new platform.

98
00:13:42,300 --> 00:13:45,300
Mumble's been having some problems in it.

99
00:13:45,300 --> 00:13:50,300
It's just hard to get to use, it requires an app and so on.

100
00:13:50,300 --> 00:13:58,300
Discord is just going to the website and as long as you're using Windows or Mac, so you're not using Linux.

101
00:13:58,300 --> 00:14:04,300
You can talk to each other.

102
00:14:04,300 --> 00:14:11,300
You have to sign up for an account.

103
00:14:11,300 --> 00:14:27,300
But that might now become our group volunteer platform as well, where we meet to talk about my organization and our organization and all of our activities.

104
00:14:27,300 --> 00:14:33,300
So because we have channels, we can set up special channels for this or that.

105
00:14:33,300 --> 00:14:50,300
We'll have an exact channel for people who are involved in running the organization.

106
00:14:50,300 --> 00:15:13,300
And it also has a desktop app if you want to use that.

107
00:15:13,300 --> 00:15:42,300
All right.

108
00:15:42,300 --> 00:15:49,300
Well, if there's no other questions, maybe I'll head out.

109
00:15:49,300 --> 00:16:00,300
Today I finished a bibliography, an annotated bibliography that's going to outline my study of the word religion.

110
00:16:00,300 --> 00:16:05,300
Maybe I'm eventually going to write a thesis paper on the word religion.

111
00:16:05,300 --> 00:16:10,300
Because it's really disappointing how we use this word and abuse this word.

112
00:16:10,300 --> 00:16:14,300
I'm just going to talk about that tonight. Maybe I'll talk about it with you all tomorrow.

113
00:16:14,300 --> 00:16:15,300
Now I'm not here tomorrow.

114
00:16:15,300 --> 00:16:16,300
Mondays I have off.

115
00:16:16,300 --> 00:16:20,300
Mondays I'm in class, so don't come here tomorrow.

116
00:16:20,300 --> 00:16:21,300
I won't be here.

117
00:16:21,300 --> 00:16:22,300
Someone else might be.

118
00:16:22,300 --> 00:16:24,300
It might be something else going on.

119
00:16:24,300 --> 00:16:26,300
I think there is.

120
00:16:26,300 --> 00:16:27,300
But Tuesday.

121
00:16:27,300 --> 00:16:36,300
Tuesday I'll tell you all about my study of the word religion and why it's important.

122
00:16:36,300 --> 00:16:41,300
I'm relatively important.

123
00:16:41,300 --> 00:16:43,300
Okay, good night everyone.

124
00:16:43,300 --> 00:16:52,300
I'm wishing you all the best.

125
00:16:52,300 --> 00:17:21,300
Thank you.

