1
00:00:00,000 --> 00:00:06,800
I'm going to make sure so you will hear and get this as dark ice.

2
00:00:06,800 --> 00:00:09,680
It's getting from the knowledge put to the knowledge put, so it's okay.

3
00:00:11,920 --> 00:00:17,920
Good evening everyone, broadcasting live at a January 2nd 2016

4
00:00:19,840 --> 00:00:26,880
with me today is Robin from Connecticut and Anthony from Toronto.

5
00:00:26,880 --> 00:00:33,840
Hello. Anthony is on our board of directors, aren't you?

6
00:00:33,840 --> 00:00:36,000
Yeah, who's our board of directors?

7
00:00:36,000 --> 00:00:39,440
Me, Robin, I'm trying to think of anybody else.

8
00:00:39,440 --> 00:00:41,760
You're open, it's on our, so the three of us are standing?

9
00:00:42,720 --> 00:00:47,360
Yes, well, as the three of us, all the directors are here.

10
00:00:47,360 --> 00:00:48,160
I didn't know that.

11
00:00:50,640 --> 00:00:54,160
Jeff, Jeff helps a lot, but so far he has escaped being.

12
00:00:54,160 --> 00:00:57,920
He was talking about getting back on the board, but I don't know where that stands right now.

13
00:00:58,880 --> 00:01:02,640
Jeff is in Winnipeg, so it makes it a little difficult.

14
00:01:03,680 --> 00:01:07,040
But okay, so we have our board of directors here, Anthony is our treasurer.

15
00:01:08,000 --> 00:01:13,760
Robin is secretary. Secretary, is that what we need?

16
00:01:15,040 --> 00:01:15,760
And I'm the monk.

17
00:01:17,520 --> 00:01:18,480
The president monk.

18
00:01:18,480 --> 00:01:20,240
I'm the president.

19
00:01:23,760 --> 00:01:27,360
So I thought Anthony came today, so I thought I would invite him up here.

20
00:01:28,480 --> 00:01:31,520
We were just talking about punctuality.

21
00:01:34,560 --> 00:01:36,320
All right, Wes, how did this come up?

22
00:01:38,160 --> 00:01:40,880
My mother was asked me this question about whether

23
00:01:42,240 --> 00:01:46,000
what I thought about being punctual and whether it's

24
00:01:46,000 --> 00:01:50,160
whether I thought it was important for people to be untied.

25
00:01:51,440 --> 00:01:55,200
And I said, no, that's an interesting question.

26
00:01:58,000 --> 00:02:02,880
I used to think, I mean, I used to really appreciate people who had this sense of

27
00:02:04,400 --> 00:02:05,280
punctuality.

28
00:02:06,320 --> 00:02:09,040
Like the head monk at Dehisuthep, when I was there,

29
00:02:09,040 --> 00:02:16,080
remember going, we were going for this some kind of lunch.

30
00:02:17,680 --> 00:02:20,560
And so I was invited and a bunch of other monks were invited.

31
00:02:21,360 --> 00:02:23,680
And he said, we'll meet at the, it's at 11.

32
00:02:24,560 --> 00:02:26,320
So we've got to be, be there.

33
00:02:27,440 --> 00:02:30,560
We've got to meet at the elevator at 11 or the escalator.

34
00:02:30,560 --> 00:02:32,320
There's a big escalator down the mountain.

35
00:02:34,320 --> 00:02:37,280
And I got there 10 minutes before 11.

36
00:02:37,280 --> 00:02:43,200
And the head monk got there about five minutes before 11.

37
00:02:43,200 --> 00:02:47,840
And then at 11 o'clock, the other monks came in and he scolded them.

38
00:02:47,840 --> 00:02:50,080
And he said, look, when we say we have to meet at 11,

39
00:02:50,080 --> 00:02:52,160
you have to be here 10 minutes before 11.

40
00:02:55,760 --> 00:02:58,800
At the time, I thought, oh, that's really, I appreciate that.

41
00:02:59,760 --> 00:03:01,760
But I think I've changed the way I look at it.

42
00:03:01,760 --> 00:03:05,520
I mean, Adjantong was really hard to have around time,

43
00:03:05,520 --> 00:03:08,560
but that's, that goes without saying because he's so busy.

44
00:03:08,560 --> 00:03:14,800
And, but looking at that gives you an example of another way to live.

45
00:03:16,240 --> 00:03:21,200
You can't really control and not trying to control.

46
00:03:21,200 --> 00:03:28,960
I mean, not trying to control just doing your best to fit as much in as you can.

47
00:03:28,960 --> 00:03:35,760
It doesn't, it wouldn't work in modern society, I suppose.

48
00:03:35,760 --> 00:03:37,920
And I was so my argument with my mother was basically,

49
00:03:37,920 --> 00:03:42,720
you know, it's just the way our society is set up, the way we live our lives by the clock.

50
00:03:42,720 --> 00:03:47,600
And I think there's an argument for us not really needing to

51
00:03:47,600 --> 00:03:55,200
that our society could have been set up even in the complex, to support the complex systems that it does

52
00:03:55,200 --> 00:04:03,840
without this need for nine to five jobs and meetings and so on.

53
00:04:05,280 --> 00:04:08,080
I mean, think of what email has done, right?

54
00:04:08,080 --> 00:04:12,800
Email, instant messaging, like look at our slack,

55
00:04:13,600 --> 00:04:15,360
we are communications.

56
00:04:16,400 --> 00:04:20,640
We don't need to have meetings for that.

57
00:04:20,640 --> 00:04:22,480
We don't have to call people even.

58
00:04:22,480 --> 00:04:26,800
We leave a message and then whenever the person gets the message, then they answer it.

59
00:04:29,600 --> 00:04:33,280
It may be that we're even moving away from this punctuality idea because

60
00:04:34,400 --> 00:04:37,520
we don't really, I don't know, I guess I don't live in the world.

61
00:04:37,520 --> 00:04:41,120
Because it's maybe a few years people will start seeing it as a necessary.

62
00:04:42,640 --> 00:04:44,880
Yeah, never enough.

63
00:04:44,880 --> 00:04:50,560
I just, I remember being in Sri Lanka and it's just incredible that how

64
00:04:50,560 --> 00:04:55,360
people who live in, in the forest, in huts

65
00:04:57,280 --> 00:05:01,280
and we're talking about village villagers who really lived in very very simple conditions.

66
00:05:01,280 --> 00:05:02,720
Some of them had dirt floors.

67
00:05:04,400 --> 00:05:09,200
And they'd say like when you ask them when you come in, they say tomorrow

68
00:05:11,200 --> 00:05:12,560
and they wouldn't come tomorrow.

69
00:05:12,560 --> 00:05:18,960
And this happened again and again and again and again and people would say, okay, I'll come there

70
00:05:18,960 --> 00:05:23,200
tomorrow and I started joking that I don't know what this word means.

71
00:05:23,200 --> 00:05:26,160
The word in Singapore is hate that means tomorrow.

72
00:05:26,720 --> 00:05:27,920
What does this word mean?

73
00:05:28,720 --> 00:05:31,040
And the workers said it means or someone said it means tomorrow.

74
00:05:31,040 --> 00:05:35,280
I said no, it doesn't because these workers have been saying this to me every day.

75
00:05:35,280 --> 00:05:41,360
Every time they come, they never come tomorrow.

76
00:05:42,400 --> 00:05:46,960
But it would be the same thing like I'll see even an hour and then three hours later they arrive

77
00:05:50,000 --> 00:05:50,960
really incredible.

78
00:05:50,960 --> 00:05:52,400
So it really changes your sense.

79
00:05:52,400 --> 00:05:54,080
You've become kind of surreal.

80
00:05:54,080 --> 00:05:59,200
This sense of, no, there'll be some nebulous time in the future.

81
00:05:59,200 --> 00:06:01,360
You could never really play on or wait for anyone.

82
00:06:01,360 --> 00:06:08,480
You just go about your life and hope that that serendipity and the synchronicity of it sort of works out.

83
00:06:11,440 --> 00:06:14,320
My mother said, no, what if you have a doctor's appointment?

84
00:06:14,320 --> 00:06:17,360
I said, well, doctors are notoriously late.

85
00:06:18,160 --> 00:06:25,920
That was like, well, what if you have something to do afterwards?

86
00:06:25,920 --> 00:06:31,280
Like if I have somewhere to be and I say to meet me half an hour before that and if you don't

87
00:06:31,280 --> 00:06:37,440
meet me on time, then I'm going to be late for me other meeting and she said, sometimes it's

88
00:06:37,440 --> 00:06:43,600
just, you need to be on time and I said, and yet the world continues to turn and she kind of

89
00:06:43,600 --> 00:06:43,840
laughed.

90
00:06:45,680 --> 00:06:53,040
But maybe my point being really, and we may make far big, we tend to make far bigger deals

91
00:06:53,040 --> 00:06:54,560
of these things than they actually are.

92
00:06:54,560 --> 00:07:02,240
Our petty lives, I mean, I don't live in the world, so I can say and I understand that the

93
00:07:02,240 --> 00:07:11,360
systems we've set up in many ways require punctuality, but it is a philosophical, philosophically

94
00:07:11,360 --> 00:07:13,680
interesting, I don't know, it's an interesting question.

95
00:07:16,320 --> 00:07:19,280
I think we could all do to be more laid back about it time.

96
00:07:19,280 --> 00:07:27,680
And certainly what you can pinpoint is someone who gets angry or irritated or upset when someone

97
00:07:27,680 --> 00:07:30,640
is late, they're wrong, that is wrong.

98
00:07:32,480 --> 00:07:38,240
Now you can scold someone, you can do it mindfully and you could argue that that's important,

99
00:07:38,240 --> 00:07:42,560
they punctuality is important, you have to let it be known that this won't be tolerated,

100
00:07:42,560 --> 00:07:50,960
but when people actually stand there and look at their watch and fumes on, that's where we have

101
00:07:50,960 --> 00:07:55,280
to, that's where the work has to be done, that has to be fixed.

102
00:07:59,840 --> 00:08:02,480
If you've gotten to send some of these people over to Sri Lanka.

103
00:08:04,880 --> 00:08:09,520
Nice to get really irritated when doctors were late, as they always are, you're sitting there for

104
00:08:09,520 --> 00:08:14,000
so long, but now they meditate and I don't mind, they can be as late as they want to be.

105
00:08:15,200 --> 00:08:15,760
It's okay.

106
00:08:22,560 --> 00:08:23,200
Questions?

107
00:08:24,400 --> 00:08:24,720
Yes.

108
00:08:35,120 --> 00:08:38,640
Sorry, there's a lot of talking, just to find the first question.

109
00:08:38,640 --> 00:08:42,720
I have a private talk tomorrow, what website do I go to to video chat?

110
00:08:45,840 --> 00:08:47,040
I think we've already talked.

111
00:08:49,840 --> 00:08:56,560
Just for anyone else, you just click on the Meet button and click on the actual box that has

112
00:08:56,560 --> 00:09:00,480
your name, right? Is that how a meeting is launched?

113
00:09:00,480 --> 00:09:08,960
Sorry, I didn't quite catch that. For people who are signing up for the meetings for the first time,

114
00:09:08,960 --> 00:09:15,200
how do they meet with you? They just click on the Meet button and then click on the box that has

115
00:09:15,200 --> 00:09:21,040
their name on it. Is that how it works? No, there'll be a different button will pop up that says

116
00:09:22,320 --> 00:09:28,400
start hanging out or something like that, call maybe, and then you have to invite me.

117
00:09:28,400 --> 00:09:33,040
So far, it's working pretty well. Oh, Christopher outlined it.

118
00:09:38,000 --> 00:09:49,920
Hey, someone's swearing here. Someone's having trouble with Mara.

119
00:09:49,920 --> 00:09:59,360
Some people had their time zone set wrong. That's funny. It would show the time wrong.

120
00:10:00,000 --> 00:10:04,800
And then you'll never get a button. Mara.

121
00:10:06,720 --> 00:10:12,000
This doesn't have a question icon, but it seems like a question. I just read a quotation from the

122
00:10:12,000 --> 00:10:16,880
Buddha. It said, doubt everything, find your own life. So would it be wrong to doubt the Buddhist

123
00:10:16,880 --> 00:10:28,640
religion? Yeah, that's not a Buddha quote. I call BS on that one.

124
00:10:30,720 --> 00:10:35,120
Yeah, check it out. Let's check it out. You want to call, you want to look it up? Take a

125
00:10:35,120 --> 00:10:48,880
Buddha quote. Take Buddha quotes. If anyone's interested, go to fake

126
00:10:48,880 --> 00:10:57,360
Buddha quotes.com. But it's here. Yeah, outside. Don't everything find your own life,

127
00:10:57,360 --> 00:11:05,520
a yes, that is a fake Buddha quote. It's in several books, mostly from the 21st century,

128
00:11:05,520 --> 00:11:14,080
21st century. Like that is a very new books. Also founded in a 1991 book,

129
00:11:15,840 --> 00:11:22,080
Armageddon Musical, attributed to the Buddha. Unfortunately, I don't know where rank and picked up

130
00:11:22,080 --> 00:11:33,440
the idea that this was for the Buddha. Find your own life.

131
00:11:33,440 --> 00:11:53,440
I missed the site. I should come back and check it out again. What's he up to?

132
00:11:53,440 --> 00:12:03,280
Is he still doing this? Yeah, December 11th was the slightest one. He's still at it. And he gets

133
00:12:03,280 --> 00:12:09,920
such black from people. People get so upset and say, why are you being so critical?

134
00:12:12,800 --> 00:12:17,280
Well, it's funny. If you see enough memes, you'll see the same quote. It's a Buddha quote. It's

135
00:12:17,280 --> 00:12:23,520
you know, a Gandhi quote. It's an Abraham Lincoln quote. It's kind of funny. Yeah, Gandhi gets

136
00:12:23,520 --> 00:12:28,320
risk quoted a lot as well. But not nearly as much as the Buddha in Einstein. But no one

137
00:12:28,320 --> 00:12:35,600
as much as the Buddha, I don't think. Sure. I saw another one recently that I'm not sure about,

138
00:12:35,600 --> 00:12:42,240
but it was posted by a monk or a beakuni. So I left it. I was going to say, give me a,

139
00:12:42,240 --> 00:12:52,160
but it's pretty close to what I remember. What was it? I can't.

140
00:12:56,080 --> 00:12:59,120
Something very close and it may just be a different version.

141
00:13:06,560 --> 00:13:10,480
So would it be wrong to doubt the Buddha's religion? Well, doubt is always problematic.

142
00:13:10,480 --> 00:13:16,800
Doubt is not a good thing. Sometimes you need, no, not need, but sometimes

143
00:13:19,360 --> 00:13:22,880
I suppose there's a difference between doubting and questioning.

144
00:13:24,800 --> 00:13:30,960
So questioning can include acknowledging that you don't know, right?

145
00:13:30,960 --> 00:13:39,840
Acknowledging that something is a belief, rather than a knowledge with stability, the distinguish between

146
00:13:39,840 --> 00:13:41,360
that. So that is useful.

147
00:13:44,720 --> 00:13:53,680
Skepticism, right? Skepticism isn't about doubting everything, about requiring proof. Skepticism

148
00:13:53,680 --> 00:14:02,800
is starting ahead. This might be true and it might be false, starting at a neutral position,

149
00:14:03,680 --> 00:14:06,800
not taking sides, and then assessing based on evidence.

150
00:14:09,040 --> 00:14:18,080
So when people go around saying prove it, prove it, prove it. That's only useful when

151
00:14:18,080 --> 00:14:24,240
the evidence, when there's no evidence to support a claim.

152
00:14:26,000 --> 00:14:31,680
So you have to start at, might be true, it might be false, and then you look at the evidence

153
00:14:32,560 --> 00:14:34,880
and see what the evidence supports.

154
00:14:39,760 --> 00:14:46,000
Anyway, the point is, I wouldn't use the word doubt. Doubt everything is really ridiculous.

155
00:14:46,000 --> 00:14:52,160
I mean, the Buddha would never use that word doubt. Anything that translates as doubt is bad.

156
00:14:53,600 --> 00:15:00,320
But to question, and maybe the word question isn't even quite right, but to suspend

157
00:15:01,280 --> 00:15:06,800
judgment and to be able to see the difference between a belief and a knowledge is very important.

158
00:15:06,800 --> 00:15:20,560
And belief is considered good in the sense that it's powerful when you believe something.

159
00:15:21,360 --> 00:15:27,200
So I guess in that sense, doubt isn't always bad, but it's always bad for the person.

160
00:15:27,200 --> 00:15:36,640
So faith is good in a mundane sense, because it makes you strong. When you believe something,

161
00:15:36,640 --> 00:15:41,680
it allows you to do it. But if you believe in doing evil deeds, that's actually quite bad.

162
00:15:42,320 --> 00:15:47,200
Even though it's good, the faith is good in the sense that it gives you the power to do all those

163
00:15:47,200 --> 00:15:53,520
evil deeds. But it's quite bad, actually. And so doubt would be the same if you're told

164
00:15:53,520 --> 00:15:58,160
that you must do lots of evil deeds. And then you doubt that. Well, that's a good thing.

165
00:15:58,160 --> 00:16:05,600
Even though the doubt cripples you, it's good that it cripples you. So

166
00:16:11,120 --> 00:16:16,400
the problem there is that if you're just dealing with faith and doubt,

167
00:16:18,640 --> 00:16:22,240
you sort of have a sense of what should I have faith and what should I doubt? And that's where

168
00:16:22,240 --> 00:16:29,760
knowledge comes in. Wisdom comes in or experience comes in. If you don't have knowledge in wisdom,

169
00:16:29,760 --> 00:16:37,200
then your faith and your doubt are just going to be random or based on your preference or totally

170
00:16:37,200 --> 00:16:45,520
arbitrary. But once you cultivate wisdom, then you doubt or you disbelieve things that have no

171
00:16:45,520 --> 00:16:54,720
basis like the idea of God or the soul or whatever, a teapot orbiting the sun.

172
00:16:57,920 --> 00:17:02,880
And you have faith in those things that are rooted in experience.

173
00:17:02,880 --> 00:17:11,920
Like evil leads to suffering, good leads to happiness and so on.

174
00:17:17,120 --> 00:17:22,560
What is the mind state of Mara the devil? How is he existing in the high realms while being

175
00:17:22,560 --> 00:17:33,280
such an immoral character? And is there any hope for his enlightenment?

176
00:17:35,280 --> 00:17:38,800
Maybe skip down a lot because I can't see that question, but I didn't see it before.

177
00:17:40,320 --> 00:17:45,920
It's so cute before that. Five hours ago, yes.

178
00:17:45,920 --> 00:18:00,640
Oh, did he skip the other? Oh, they're okay. So yeah, I saw it right. Mara isn't

179
00:18:01,520 --> 00:18:06,960
necessarily immoral, although I suppose that is one way of looking at it. The angels are a weird

180
00:18:06,960 --> 00:18:13,600
bunch and Buddhist cosmology. They apparently go to war with the Asuras, you know, they're really

181
00:18:13,600 --> 00:18:19,360
killing each other or something. I don't know, at least fighting and they end up kicking the Asuras

182
00:18:19,360 --> 00:18:25,600
out. It's all, this is all vengeance. I don't know whether it's true or not, whether it really

183
00:18:25,600 --> 00:18:31,920
happens. I never can't remember ever being in heaven. So I can't say what it's like.

184
00:18:31,920 --> 00:18:35,680
Well, I suppose it's just as weird, probably a lot weirder than the human world.

185
00:18:35,680 --> 00:18:48,720
So yeah, apparently there is a group of Mara's. See, the thing about Amara is if you notice in this,

186
00:18:48,720 --> 00:18:55,440
in the discourses, Mara was often encouraging the Bodhisatta and other monks and other meditators

187
00:18:56,480 --> 00:19:04,240
to do good deeds, but to not practice meditation, so to give charity and so on.

188
00:19:04,240 --> 00:19:09,200
Because this is the kind of thing that would lead them to heaven. And Mara likes that. Mara is

189
00:19:09,200 --> 00:19:23,760
a part of the Bodharnimita, a very nimita, sawati. So what do you think? I think he's part of

190
00:19:23,760 --> 00:19:34,800
those, which means people who delight or who are happy about the creation's feathers. So they

191
00:19:34,800 --> 00:19:41,840
want other people to do things, to cultivate, they like all sorts of things, to build skyscrapers,

192
00:19:41,840 --> 00:19:49,280
and to go to war even perhaps. They just like becoming, they like watching other people,

193
00:19:49,280 --> 00:19:54,800
they like they encourage other people. So they encourage people to follow their dreams and

194
00:19:54,800 --> 00:20:00,800
sawati. Mara is the one that encourages you to follow your dreams and think big and sawati.

195
00:20:01,440 --> 00:20:09,760
We ambitious, they like ambition. So what are called him evil? What are called him or the

196
00:20:09,760 --> 00:20:22,800
you know, this Mara that was chasing him, evil. But in a Monday sense, he generally doesn't seem

197
00:20:22,800 --> 00:20:28,240
to be evil, he seems to just not want people to leave samsara. It's kind of sad for him,

198
00:20:29,040 --> 00:20:35,920
some reason when people leave because then those people are not. I don't know anyway, they like

199
00:20:35,920 --> 00:20:40,160
to encourage people to stay in samsara and to do, to create things.

200
00:20:43,360 --> 00:20:44,960
Apparently we are for their amusement.

201
00:20:50,560 --> 00:20:57,920
I had skipped some questions prior for me. My question is how do you lose Sakaya Diddy without

202
00:20:57,920 --> 00:21:02,960
also having shed mana, for example, aren't both states interdependent in a way?

203
00:21:02,960 --> 00:21:10,560
And a little bit more is, I mean, how do you become a sothapana without having conceit?

204
00:21:10,560 --> 00:21:12,160
It just doesn't make sense to me.

205
00:21:12,960 --> 00:21:17,920
Sothapana has no view of self because they've seen

206
00:21:21,440 --> 00:21:26,560
they've seen the break. They've seen how everything and they've come perfectly clear about

207
00:21:26,560 --> 00:21:33,520
how everything arises in seasons. But it doesn't really change that much else, like

208
00:21:34,560 --> 00:21:37,680
they still get angry, they still get greedy, and all of that is based on

209
00:21:39,600 --> 00:21:42,240
delusion. They still have a lot of delusion.

210
00:21:44,880 --> 00:21:50,480
And you can see this just a part of that, conceit is a judgment, judging yourself is better,

211
00:21:50,480 --> 00:21:57,520
worse than other people. It's just like a judge other things, things outside of yourself. They can

212
00:21:57,520 --> 00:22:02,560
still get arrogant. Well, not so much. They're generally humble, they're generally not conceited.

213
00:22:03,840 --> 00:22:13,840
What you might note that I think the orthodoxy is that there are nine types of conceit,

214
00:22:13,840 --> 00:22:20,240
a person might esteem themselves as better than someone else,

215
00:22:21,920 --> 00:22:27,520
equal to someone else or worse than someone else. And they might, in fact, be better than

216
00:22:27,520 --> 00:22:32,240
someone else. They might, in fact, be equal to someone else. And they might, in fact, be worse than

217
00:22:32,240 --> 00:22:38,880
someone else. So three times three makes nine types of conceit. For each of the three

218
00:22:38,880 --> 00:22:46,560
beliefs or conceptions and the three realities, if you have the permutations make nine,

219
00:22:48,320 --> 00:22:53,840
a sort of panic can only give rise to three of them, meaning if a sort of panic is

220
00:22:56,480 --> 00:23:02,880
better than someone in some way, then they esteem themselves as better. If they're equal

221
00:23:02,880 --> 00:23:09,840
to someone than they esteem themselves as being equal, if they're inferior than they esteem

222
00:23:09,840 --> 00:23:14,560
themselves as being inferior, don't quote me on that. It's kind of in a while. And obviously,

223
00:23:14,560 --> 00:23:25,280
details are not really me forte. But we see demagogues into detail about that. I should really

224
00:23:25,280 --> 00:23:31,760
look kind of. Maybe that's my understanding of it. So the sort of panic does get rid of the wrong

225
00:23:31,760 --> 00:23:42,560
sort of conceit, I think. But they can still feel good about being better than someone,

226
00:23:43,760 --> 00:23:50,080
the arrogant potentially, though not that much. But it's kind of like the same way, how can they

227
00:23:50,080 --> 00:23:55,920
get greedy and angry if they don't have a view of self, right? But it's mostly considered to be

228
00:23:55,920 --> 00:24:02,960
just leftover. It's slowly working its way out. It's like the power that is given rise to these

229
00:24:02,960 --> 00:24:07,200
things. The power is still there. It takes time for that to work itself.

230
00:24:07,200 --> 00:24:27,680
Did that also answer about losing Sakaya Ditti without also having shadmana?

231
00:24:27,680 --> 00:24:38,240
I thought it did. Oh, yeah, Sakaya Ditti is view of self. Manai is conceit.

232
00:24:41,040 --> 00:24:46,320
So view is different from conceit. Conceit is just a steaming something. View is actually

233
00:24:46,320 --> 00:24:51,200
there is a self. I believe there is a self. This is self.

234
00:24:51,200 --> 00:24:57,760
Wrong conception can still arrive.

235
00:25:06,880 --> 00:25:12,080
I have one question regarding sitting meditation. If you are thinking rising, falling as you

236
00:25:12,080 --> 00:25:18,080
focus on your abdomen and respiration, you are indeed thinking, but should not meditation be a

237
00:25:18,080 --> 00:25:22,240
thoughtless state of mind? No, there's no reason to think that

238
00:25:24,000 --> 00:25:29,920
thoughts are a part of meditation. It's the third sepipatama or it's a part of it anyway.

239
00:25:29,920 --> 00:25:33,840
It's also in the fourth sepipatama. So thoughts are very much a part of reality.

240
00:25:34,560 --> 00:25:40,720
It's just absolutely a very big misconception about meditation that you should, but there are

241
00:25:40,720 --> 00:25:45,760
many meditations that tell you to do just that, but it's a misconception that all meditation,

242
00:25:45,760 --> 00:25:51,520
especially Buddhist meditation, should be less. Of course, many Buddhist meditations are,

243
00:25:51,520 --> 00:25:57,520
but mindfulness meditation, or meditation to understand reality, which is the core and really

244
00:25:58,480 --> 00:26:06,560
the end of the goal of the practice that you have to take requires thought,

245
00:26:08,240 --> 00:26:13,440
both the examination of thought and the response. Some mindfulness is about creating a clear

246
00:26:13,440 --> 00:26:19,680
thought because thought isn't a problem, but judgmental thought is a problem when you like

247
00:26:19,680 --> 00:26:26,880
something or dislike something that's a problem. So we're just trying to replace that with a clear

248
00:26:26,880 --> 00:26:27,200
thought.

249
00:26:32,800 --> 00:26:37,600
If you find your mind restless at night, what are some good ways to help in relaxation

250
00:26:37,600 --> 00:26:44,320
and clearing the mind and the body for the best rest?

251
00:26:47,280 --> 00:26:50,000
Well, maybe read my booklet on how to meditate.

252
00:26:55,040 --> 00:27:02,400
I guess one thing I would say is don't worry so much about relaxing. If your mind is awake,

253
00:27:02,400 --> 00:27:07,680
you're restless and just meditate. Say yourself restless, restless and be okay with it,

254
00:27:07,680 --> 00:27:32,960
because the more upset you get about it, the more restless and anxious you become.

255
00:27:37,680 --> 00:27:48,160
Last month, you talked about different styles of chanting and how this Sri Lankan style resonated

256
00:27:48,160 --> 00:27:52,960
with you. I was watching a seminar on past life memory that was given at a Buddhist conference,

257
00:27:53,600 --> 00:28:00,000
and there was a fellow named Dhamma Wuran, who at the age of two or three could spontaneously

258
00:28:00,000 --> 00:28:05,680
chant in palli at two o'clock in the morning. His foster father recorded some of these chants

259
00:28:05,680 --> 00:28:11,520
and brought a monk to verify that it was palli. Not only did they verify it, they also identified

260
00:28:11,520 --> 00:28:15,600
the sutas, and the monk said his style of chanting is no longer around these days.

261
00:28:17,200 --> 00:28:23,520
Goes on and on a little more, but the question is, have you heard these chants? And if so,

262
00:28:23,520 --> 00:28:28,080
does his style bring out the meaning of the sutta more than any other present chant?

263
00:28:28,080 --> 00:28:33,840
I've never heard this. I've heard of this boy, but I've never heard this.

264
00:28:35,200 --> 00:28:42,880
Never heard his chanting. The chanting is just, I mean, the best is something fairly simple.

265
00:28:46,560 --> 00:28:51,520
I don't know. I mean, there's nothing magical about it. It's just words.

266
00:28:51,520 --> 00:28:59,200
I suppose it's quite remarkable that someone at two or three could recite, you know,

267
00:28:59,760 --> 00:29:00,800
in palli, wouldn't it be?

268
00:29:04,240 --> 00:29:05,200
That's remarkable.

269
00:29:07,600 --> 00:29:10,880
Yeah, I saw a video on YouTube. I'm not sure if it was that particular

270
00:29:11,840 --> 00:29:17,200
two or three-year-old, or it was just someone who maybe had learned it, but it was a very small

271
00:29:17,200 --> 00:29:21,440
child and he was chanting in palli, it was kind of cool. It wasn't very long, though.

272
00:29:29,680 --> 00:29:31,280
I believe that's all the questions.

273
00:29:35,120 --> 00:29:35,520
All right.

274
00:29:40,160 --> 00:29:45,040
Anthony, questions from me? Anything to say?

275
00:29:45,040 --> 00:29:51,600
I'll talk about any announcements. How's our organization do it?

276
00:29:52,800 --> 00:29:59,760
Our organization has been on vacation for two weeks. We haven't had meetings for a couple

277
00:29:59,760 --> 00:30:05,440
weeks, but we'll be meeting again tomorrow to see what the needs of yourself, the monastery and

278
00:30:05,440 --> 00:30:10,480
meditation center are. Did the banner arrive for the five-minute meditation?

279
00:30:10,480 --> 00:30:16,960
Yes, but the things that they're supposed to be clips didn't come with it.

280
00:30:17,680 --> 00:30:21,840
So they're in a different container or something and they're going to be here by the seventh.

281
00:30:22,560 --> 00:30:29,120
Well, there were two options. There was one option for those plastic tags. I forgot what they're

282
00:30:29,120 --> 00:30:39,840
called. The plastic loops that you put up or a rope. So I ordered the rope because I thought

283
00:30:39,840 --> 00:30:45,200
the plastic, I forgot what they're called, but you put them up and that's how you put up a banner

284
00:30:45,200 --> 00:30:50,800
permanently. And I figure this is a banner you probably have to take down in between.

285
00:30:50,800 --> 00:30:55,600
The ordered ropes, whatever ropes. A rope. An actual rope.

286
00:30:56,240 --> 00:30:57,760
But how do you connect the rope to it?

287
00:31:00,720 --> 00:31:01,920
It's just a piece of vinyl.

288
00:31:01,920 --> 00:31:08,640
Yes, it has, I think it has holes in it and you put the rope really.

289
00:31:14,480 --> 00:31:16,240
Anyway, worst case will be stuck there.

290
00:31:19,280 --> 00:31:23,120
I've got a frame. We've got a good frame to put it on.

291
00:31:25,120 --> 00:31:26,240
So hopefully that'll work.

292
00:31:27,360 --> 00:31:31,520
When you do put it up, will it be permanently placed there? Do you have to take it down every

293
00:31:31,520 --> 00:31:32,480
day or every week?

294
00:31:34,720 --> 00:31:38,640
Yeah, so the plastic loop things won't work. I forgot what they're called, but they

295
00:31:40,160 --> 00:31:43,040
they hold things up, but you have to cut them down to take them off.

296
00:31:46,960 --> 00:31:48,080
I can't think what it's called.

297
00:31:49,200 --> 00:31:50,560
Oh, the ties.

298
00:31:51,280 --> 00:31:51,360
Yes.

299
00:31:52,720 --> 00:31:53,360
Table ties.

300
00:31:56,080 --> 00:31:57,280
That's not the real name of it.

301
00:31:57,280 --> 00:32:01,200
I don't think so.

302
00:32:01,200 --> 00:32:03,360
Zip ties, maybe that's what they're called.

303
00:32:06,320 --> 00:32:08,400
But I guess they're for cables.

304
00:32:09,520 --> 00:32:11,760
I worked with this one place to be called from cable ties.

305
00:32:14,960 --> 00:32:18,400
Yeah, I know them by another name, but I think we're all talking about the same thing.

306
00:32:20,240 --> 00:32:21,760
Yeah, it's those white plastic things.

307
00:32:22,400 --> 00:32:26,160
Yeah, you have to cut through them when you're done using them.

308
00:32:26,160 --> 00:32:31,760
So I got like what what the text of vinyl or whatever this thing is that we have.

309
00:32:32,480 --> 00:32:35,680
There's there was something but a decent option.

310
00:32:37,440 --> 00:32:41,040
The sticks to them with that has a little loop coming out that you hook into.

311
00:32:41,920 --> 00:32:46,800
And there was a grommet option, I think grommets from like more expensive, but they're better for outdoors.

312
00:32:48,880 --> 00:32:49,760
I didn't get anything.

313
00:32:50,800 --> 00:32:53,440
So like I think there's another box coming to help there's.

314
00:32:53,440 --> 00:33:02,640
Okay, yeah, hopefully that will explain because as I said, there were there were two add-on items either the rope or the the ties.

315
00:33:06,000 --> 00:33:07,040
I don't really need rope.

316
00:33:07,040 --> 00:33:07,680
We got rope.

317
00:33:08,080 --> 00:33:11,040
Okay, I need something to connect it to the thing.

318
00:33:17,120 --> 00:33:20,080
Okay, so meeting tomorrow at 12 and one.

319
00:33:20,080 --> 00:33:24,480
I'm meeting for volunteers at one and for study group that too.

320
00:33:25,920 --> 00:33:27,120
The admin is at 12.

321
00:33:30,080 --> 00:33:30,960
You're not invited.

322
00:33:32,160 --> 00:33:34,400
They're rocking this with internet.

323
00:33:36,160 --> 00:33:40,640
Although we although we would love to have more volunteers, you know, active in that.

324
00:33:41,280 --> 00:33:42,320
What is the admitting meeting?

325
00:33:43,520 --> 00:33:48,800
Well, at least in the volunteer, the general volunteer meeting and then for people that are

326
00:33:48,800 --> 00:33:54,160
really interested, it would be nice to have more people on the admin team.

327
00:33:55,120 --> 00:34:00,000
That's the team that does all the accounting and kind of the the business end of.

328
00:34:00,000 --> 00:34:02,640
Maybe people who are more experienced with that would help.

329
00:34:03,200 --> 00:34:05,600
Yes, basically we do what we do.

330
00:34:05,600 --> 00:34:09,600
So that month they doesn't have to be overly involved in financial matters.

331
00:34:09,600 --> 00:34:13,440
So we try to take on all the the financial bookkeeping.

332
00:34:13,440 --> 00:34:17,920
By laws, different business aspects of.

333
00:34:18,800 --> 00:34:27,200
I mean, you guys do a lot of work that I would have that I couldn't do, but I can't do it because I'm too busy.

334
00:34:29,600 --> 00:34:33,840
And also some of it, you know, is really against the windy, right?

335
00:34:34,320 --> 00:34:36,400
Yeah, that's not how you used to.

336
00:34:38,160 --> 00:34:40,320
So we're going on just great.

337
00:34:40,320 --> 00:34:43,520
Yes.

338
00:34:43,520 --> 00:34:44,640
Oh, thank you, Ryan.

339
00:34:46,000 --> 00:34:49,840
So zip tie, zip straps, zip ties, I think.

340
00:34:51,760 --> 00:34:52,560
Something like that.

341
00:34:53,840 --> 00:35:02,000
And for Joshua asking how to join the study group, there's a Facebook page study group with

342
00:35:02,000 --> 00:35:10,400
you to download be cool. And that gives the information. Basically, it's on mumble, an audio chat program.

343
00:35:11,280 --> 00:35:14,080
And I'll link you all the information.

344
00:35:14,080 --> 00:35:21,280
There's a web page on the website as well on the serimungalow.org website with all the information.

345
00:35:21,280 --> 00:35:25,520
But we meet on mumble audio chat and we take turns reading the visudimaga.

346
00:35:25,520 --> 00:35:32,960
It's a nice group. We've been together nearly a year now. I believe it started in February of

347
00:35:32,960 --> 00:35:38,720
last year, making our way through the visudimaga. I think we're on chapter 10 now.

348
00:35:38,720 --> 00:35:55,120
And there's a question, what is skeptical doubt in the context of the five hindrances?

349
00:35:55,120 --> 00:36:04,560
Okay. And doubt is when you're not sure of something.

350
00:36:08,400 --> 00:36:15,280
And that that's a hindrance because it prevents you from moving forward with whatever

351
00:36:15,280 --> 00:36:20,320
this you're doing. Now, the commentary, I think, is a little bit less forgiving about it.

352
00:36:20,320 --> 00:36:26,880
And you know, that's general. And it's more like doubt about the Buddha and doubt about the

353
00:36:26,880 --> 00:36:32,800
dharma, doubt about the sun. And I would say it can be any kind of doubt, which is it's just that

354
00:36:32,800 --> 00:36:38,160
doubt about the Buddha, the dharma, the sun goes actually technically a problem.

355
00:36:38,160 --> 00:36:41,520
So we're saying you can doubt about bad things. And that's kind of a good thing.

356
00:36:41,520 --> 00:36:50,720
Or you can doubt about whether Paris is the capital of France. It's not really an important

357
00:36:50,720 --> 00:36:56,400
doubt. But if you doubt about the meditation practice, well, that's a hindrance, a real hindrance,

358
00:36:56,400 --> 00:37:03,600
because it's going to prevent you from progressing in the practice.

359
00:37:03,600 --> 00:37:18,960
Okay. That's all. Have a good night, everyone. Thanks for having me for joining me and

360
00:37:18,960 --> 00:37:26,160
thank you, Anthony, for improving the camera. Thank you, Bantin Anthony. Good night, everyone.

361
00:37:26,160 --> 00:37:34,560
Good night.

362
00:37:56,160 --> 00:37:58,160
you

363
00:38:26,160 --> 00:38:28,160
you

364
00:38:56,160 --> 00:38:58,160
you

365
00:39:26,160 --> 00:39:28,160
you

366
00:39:56,160 --> 00:39:58,160
you

367
00:40:26,160 --> 00:40:28,160
you

368
00:40:56,160 --> 00:40:58,160
you

369
00:41:26,160 --> 00:41:28,160
you

370
00:41:56,160 --> 00:41:58,160
you

371
00:42:26,160 --> 00:42:28,160
you

