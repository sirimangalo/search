1
00:00:00,000 --> 00:00:17,320
Because this is coming on the heels of the preliminary or the first set of verses from

2
00:00:17,320 --> 00:00:28,320
the Damabada, Manopubangamada, that all damas are preceded by the mind. Mano Saita, Manomia,

3
00:00:28,320 --> 00:00:37,960
they are governed by the mind, formed by the mind, actually made by the mind that all

4
00:00:37,960 --> 00:00:53,320
damas, all reality. If it's going to come about in reality, it has to come about via the mind.

5
00:00:53,320 --> 00:01:11,400
But this is really because of how we look at reality. As followers of the Buddha's teaching.

6
00:01:11,400 --> 00:01:24,400
But it's not really because the Buddha taught it. It's because of a measured and

7
00:01:24,400 --> 00:01:32,200
reasoned acceptance of the Buddhist teaching as being true. That being a truth that actually

8
00:01:32,200 --> 00:01:41,480
what is real is experience. There's a lot of debate over what is real about arguments

9
00:01:41,480 --> 00:01:54,280
about this with people. But what is real? And it really, it's all like the blind man and

10
00:01:54,280 --> 00:02:02,160
the elephant talking about things we've never seen. Most of the time that's what it is.

11
00:02:02,160 --> 00:02:09,600
Because you can philosophize and come up with all sorts of ideas about what is reality.

12
00:02:09,600 --> 00:02:15,640
And you can come up with all sorts of arguments why something isn't real or why it's impossible

13
00:02:15,640 --> 00:02:25,960
to verify whether something is real or not. This whole thing about verification is really

14
00:02:25,960 --> 00:02:33,000
quite funny and we've painted ourselves into a corner in modern science because we can't

15
00:02:33,000 --> 00:02:43,080
ever know anything for sure. And it's actually become a boast that the only way to truly

16
00:02:43,080 --> 00:02:59,480
verify something is to come close by third person or by secondhand really in information

17
00:02:59,480 --> 00:03:06,520
through instruments and through impartial experiments. But you can never really know anything.

18
00:03:06,520 --> 00:03:18,160
In fact, trying to come to know something directly is scorned upon, scorned, scorned

19
00:03:18,160 --> 00:03:31,040
upon because of the potential for subjectivity. It's not objective to pay heed to

20
00:03:31,040 --> 00:03:42,280
one's observations. And there's some measure of truth there. But it's not seeing the forest

21
00:03:42,280 --> 00:03:49,480
for the trees because it's throwing the baby out with the bathwater or whatever you want

22
00:03:49,480 --> 00:03:53,720
to say. It's missing something. And it's really throwing the baby out with the bathwater

23
00:03:53,720 --> 00:04:02,200
because you want to get rid of all of these subjective observations like a person who

24
00:04:02,200 --> 00:04:08,080
in the case of a religious person who sees something or hears something or experience

25
00:04:08,080 --> 00:04:18,240
with something and says this is God or this is Nibana or this is this or this is that

26
00:04:18,240 --> 00:04:32,320
and gives it some subjective meaning. You want to avoid that and unfortunately what people

27
00:04:32,320 --> 00:04:44,320
do as a result is disregard it simply because it is a individual perception. They discard

28
00:04:44,320 --> 00:04:56,160
it because it was personally observed. And in fact, we normally won't accept in the

29
00:04:56,160 --> 00:05:02,960
scientific community or as I understand they won't accept even if a thousand or a million

30
00:05:02,960 --> 00:05:10,840
people had individual observations. But there has to be some way for a third party

31
00:05:10,840 --> 00:05:17,400
to observe what's going on. And the third party has to be controlled and so on in order

32
00:05:17,400 --> 00:05:22,800
for it to be acceptable. So therefore, take the whole realm of personal experience out

33
00:05:22,800 --> 00:05:31,720
of the discussion of what is real, which is quite unfortunate. When really all you have

34
00:05:31,720 --> 00:05:40,080
to do is separate subjective experience from an objective experience. And it may mean that

35
00:05:40,080 --> 00:05:46,600
you can't ever prove to someone else what is reality but you can certainly prove to yourself

36
00:05:46,600 --> 00:05:50,920
what is real. If a person wants to know what is real, if they want to prove to someone

37
00:05:50,920 --> 00:05:59,480
else and or be have it proved to them by someone else's experience then it's really impossible

38
00:05:59,480 --> 00:06:07,960
because all it takes is a person to lie or to misrepresent in some way or other their

39
00:06:07,960 --> 00:06:13,880
experience. But if you want to know the truth, if you want to, for those of us who are seeking

40
00:06:13,880 --> 00:06:26,560
the truth, we don't have to use laboratory and rats and experiments or subjects and other

41
00:06:26,560 --> 00:06:34,760
people and so on, we can use our own experience as long as we understand what is objective

42
00:06:34,760 --> 00:06:39,200
experience and that's really what we do in Buddhism. So it's really actually quite ideal

43
00:06:39,200 --> 00:06:48,200
and it's perfect. This is why I have chosen this path because it's really quite clearly

44
00:06:48,200 --> 00:06:59,480
the past to understand and to experience reality for what it is objectively.

45
00:06:59,480 --> 00:07:10,200
So objective reality as we should be aware is experience. So when you see God or when

46
00:07:10,200 --> 00:07:21,640
you hear God or when you experience the love of God and so on, there's a factual verifiable

47
00:07:21,640 --> 00:07:28,920
part or aspect of that experience and then there's this subjective projected extrapolated

48
00:07:28,920 --> 00:07:34,160
part of the experience and that's the case really with everything. All of our experiences

49
00:07:34,160 --> 00:07:42,680
are like this. Our experience right now is made up of the objective portion and the subjective

50
00:07:42,680 --> 00:07:48,600
portion and the objective portion can't ever get you in trouble. It can never be dangerous

51
00:07:48,600 --> 00:07:54,200
for you to see or hear or smell or taste or feel or think something. That's the objective

52
00:07:54,200 --> 00:08:02,000
part. If someone yells at you or says nasty things to you, if all you understood it to

53
00:08:02,000 --> 00:08:10,480
be was a sound or a sight or a thought or so on and that's how you perceived it. Someone's

54
00:08:10,480 --> 00:08:18,080
yelling at you and you perceive it as sound coming to your ear. If you had a pain in

55
00:08:18,080 --> 00:08:27,040
the body and you just perceived it as a sensation, if a thought came up, a memory came

56
00:08:27,040 --> 00:08:32,760
up and you just perceived it as a memory, as a thought, just thought for what it is.

57
00:08:32,760 --> 00:08:38,920
If you were really aware of reality, if you're really in tune with things as they were,

58
00:08:38,920 --> 00:08:48,040
there'd be no problem. But the real problem is that this is insufficient for the

59
00:08:48,040 --> 00:08:57,920
cravings that exist in the mind. Our mind would never go for such a way of life and this

60
00:08:57,920 --> 00:09:04,680
is why meditation is quite difficult because the mind isn't content to simply live what

61
00:09:04,680 --> 00:09:14,400
is really a peaceful and happy life of just being with reality. The mind has many addictions

62
00:09:14,400 --> 00:09:23,280
and cravings and habits that lead it to chase after things that are not really leading

63
00:09:23,280 --> 00:09:33,080
to that, not really in the way of happiness or peace. And it all comes down to our ignorance.

64
00:09:33,080 --> 00:09:40,600
We've never really, we've come out of nothing really. It's not that we've fallen from

65
00:09:40,600 --> 00:09:47,040
a pure state into an impure state. We never really had any clue about purity or impurity.

66
00:09:47,040 --> 00:09:54,200
These are things that have evolved and these are things that were discovered by the Buddha

67
00:09:54,200 --> 00:09:58,560
that there is such a thing as purity, that there is such a thing as enlightenment. It's

68
00:09:58,560 --> 00:10:06,600
something not before understood by any of us, not ever experienced before by any of

69
00:10:06,600 --> 00:10:12,520
us. It's some way of putting together the building blocks of experience in a way that

70
00:10:12,520 --> 00:10:24,560
is peaceful in a way that is complete and harmonious and free from stress and suffering,

71
00:10:24,560 --> 00:10:30,320
free from contradiction really if you want to get right down to it. Because most of us live

72
00:10:30,320 --> 00:10:35,480
in a state of contradiction, we want things to be a certain way and yet we act in such

73
00:10:35,480 --> 00:10:39,840
a way that they don't come out that way. We want to be happy and we act in such a way

74
00:10:39,840 --> 00:10:47,800
that leads to our suffering. So we have this contradiction internally, which is really the

75
00:10:47,800 --> 00:10:55,800
logically, the fallacy with our way of life, for the fallacy of the argument, if it

76
00:10:55,800 --> 00:11:01,640
were put as an argument, if we were to argue for this way of life and this path that

77
00:11:01,640 --> 00:11:11,000
we've chosen as human beings to chase after central pleasures and run away from pains

78
00:11:11,000 --> 00:11:22,880
and displeasure, the problem is that it doesn't truly lead us in the direction that we

79
00:11:22,880 --> 00:11:28,960
are intending. So for us to say that this path leads us to happiness and leads us away

80
00:11:28,960 --> 00:11:36,720
from suffering is wrong, is incorrect, therefore we have a problem. So Buddhism is merely

81
00:11:36,720 --> 00:11:45,280
correcting the argument and correcting our views really. If you want to put it on a very

82
00:11:45,280 --> 00:11:55,160
scientific and objective level, then what we're talking about is removing the contradiction,

83
00:11:55,160 --> 00:12:04,760
where we act and we live our lives in such a way where we actually get the expected

84
00:12:04,760 --> 00:12:16,480
result, which is peace and happiness. The good things come to us, the things that make us

85
00:12:16,480 --> 00:12:30,440
happy, the things that bring us peace. And this is what we mean by the ignorance and

86
00:12:30,440 --> 00:12:36,360
versus wisdom, because a person who is ignorant, they're unable to see what is to their

87
00:12:36,360 --> 00:12:43,680
benefit and they're unable to see what is to their detriment. And so the Buddha eschews

88
00:12:43,680 --> 00:12:54,000
the whole notion of morality that something could be immoral or unjust or so on. And he

89
00:12:54,000 --> 00:13:00,680
does so in a way that I think no one else has ever been able to do to this extent by saying

90
00:13:00,680 --> 00:13:06,840
that actually those, by being able to have his cake and eat it too really, where he's

91
00:13:06,840 --> 00:13:19,240
able to give up any idea of something being objectively immoral and yet be perfectly moral

92
00:13:19,240 --> 00:13:26,200
and show that actually a utilitarian morality is also an absolutist morality, where a person

93
00:13:26,200 --> 00:13:37,040
who is acting just in their own best interest is also the perfect humanitarian. This

94
00:13:37,040 --> 00:13:47,840
is the beauty, the perfection of what we're practicing here. That's the ideal, isn't it?

95
00:13:47,840 --> 00:13:52,240
Because on the one hand, we always have this debate of whether it's better to be objectively

96
00:13:52,240 --> 00:14:02,920
moral, even though it's going to make you suffer or whether you should be morally utilitarian

97
00:14:02,920 --> 00:14:12,920
and act for your own best interest at the risk of hurting others. So which is better.

98
00:14:12,920 --> 00:14:18,680
And here we're able to avoid both. That's the ideal, I mean that would be the ideal in

99
00:14:18,680 --> 00:14:23,480
any case that you would be able to do everything in your own best interest but never

100
00:14:23,480 --> 00:14:33,320
heard anyone else. And the great thing is that we find this to be the truth that when a

101
00:14:33,320 --> 00:14:39,040
person acts truly in their own best interest does things that are truly in their own best

102
00:14:39,040 --> 00:14:43,600
interest, then they're helping other people. They wind up helping other people. And when

103
00:14:43,600 --> 00:14:51,160
a person helps other people, they wind up or when a person acts and at any rate acts appropriately

104
00:14:51,160 --> 00:14:55,360
towards other people. Now maybe that for some people that doesn't mean going out of your

105
00:14:55,360 --> 00:15:04,200
way to help them but it means that they're not harming and the maintaining of peace and

106
00:15:04,200 --> 00:15:15,760
harmony between yourself and the other people and others. This person winds up finding

107
00:15:15,760 --> 00:15:21,480
peace and happiness themselves through their harmony. I mean it's actually quite obvious

108
00:15:21,480 --> 00:15:27,200
and it's something that we should already readily be able to see, especially if we are

109
00:15:27,200 --> 00:15:35,360
practicing meditation. But actually the problem is people don't practice meditation

110
00:15:35,360 --> 00:15:44,360
and so they come up with all these theories and ideas and so many. This is by the Buddha

111
00:15:44,360 --> 00:15:52,560
put such great emphasis on views and the importance of overcoming view, wrong view. Something

112
00:15:52,560 --> 00:16:06,040
that the Buddha talks about a lot really and we shouldn't ignore that. A big part of

113
00:16:06,040 --> 00:16:13,240
what the Buddha was teaching is for us to overcome view wrong view. And really he uses

114
00:16:13,240 --> 00:16:18,200
these two words interchangeably. Sometimes he says wrong view but sometimes he just

115
00:16:18,200 --> 00:16:25,080
simply says view. And really the point is that anything that is simply a view is wrong.

116
00:16:25,080 --> 00:16:39,120
It's wrong to hold views. If it's not a personally experienced reality then it's not yet

117
00:16:39,120 --> 00:16:47,040
adequate. It has no benefit. It can have some benefit, I suppose. But it has no intrinsic

118
00:16:47,040 --> 00:16:57,800
value to the mind. It's not going to lead the mind in a good, in a positive direction.

119
00:16:57,800 --> 00:17:04,080
Not on a moment to moment level. I mean you can hold views that are in line with reality

120
00:17:04,080 --> 00:17:09,320
and those can benefit you to an extent. But they can't really change the nature of the

121
00:17:09,320 --> 00:17:15,320
mind. They can't make us truly good people. We can pretend and people are able to keep

122
00:17:15,320 --> 00:17:20,800
the precepts and pretend to be good people and seem to be very good people and are morally

123
00:17:20,800 --> 00:17:28,800
and ethically very good people. But they are conflict inside because it's simply an

124
00:17:28,800 --> 00:17:33,120
intellectual view. They hold this view that this is wrong and that's wrong. And yet they

125
00:17:33,120 --> 00:17:39,080
want to do those things. They have these natural urges to engage in those things when

126
00:17:39,080 --> 00:17:46,880
something unpleasant comes or an insect or an ant or a creature comes that is unpleasant,

127
00:17:46,880 --> 00:17:52,520
the natural urges to want to hurt and to kill and to harm the other being. And yet

128
00:17:52,520 --> 00:18:02,080
they suppress this. So there's a benefit to that. But it's not a intrinsic benefit where

129
00:18:02,080 --> 00:18:10,200
it actually changes the mind and the mind is actually becomes more at peace with itself

130
00:18:10,200 --> 00:18:17,120
because the mind is actually in great conflict. And this is what we see in religious circles,

131
00:18:17,120 --> 00:18:23,120
not just in the Catholic church. Though that's been a victim or the Catholic church has

132
00:18:23,120 --> 00:18:30,240
been not a victim has been a victim. Well, it's kind of a victim. I guess it's not the

133
00:18:30,240 --> 00:18:44,000
quite the victim of this bad publicity of these horrible people who became priests and

134
00:18:44,000 --> 00:18:54,280
how the atmosphere of the priesthood has actually become a place that is conducive towards

135
00:18:54,280 --> 00:19:02,000
incredibly immoral states of mind and activities. But the Catholic church isn't the

136
00:19:02,000 --> 00:19:10,440
only one. And I'm afraid really for Buddhism because, well, I'm sorry, not afraid for

137
00:19:10,440 --> 00:19:19,160
Buddhism, but it's not even really afraid. It's kind of sad to know that in the future

138
00:19:19,160 --> 00:19:24,720
there's probably going to be more and more as Buddhism becomes more and more mainstream.

139
00:19:24,720 --> 00:19:33,040
We're already seeing the hints of this, no stories of alleged sexual abuses and so on.

140
00:19:33,040 --> 00:19:40,480
And the same old coverups and the same old ignoring of the facts and so on. You see elsewhere

141
00:19:40,480 --> 00:19:47,680
because it's a bad publicity and no one wants bad publicity. But the problem isn't really

142
00:19:47,680 --> 00:19:57,520
these bad people. It isn't the individuals. It's the society and it's the structure that

143
00:19:57,520 --> 00:20:03,080
holds to good principles. Even you could say like really the Catholic church is full

144
00:20:03,080 --> 00:20:09,400
of good principles. I wouldn't say there's much in the Catholic church, the institution

145
00:20:09,400 --> 00:20:19,720
that in principle is bad. It's just become lip service instead of actually following the

146
00:20:19,720 --> 00:20:26,080
things that Christ told His followers to practice. It becomes all ceremony and ritual

147
00:20:26,080 --> 00:20:33,280
and lip service. Like we have this ceremony here where we take the precepts. But for

148
00:20:33,280 --> 00:20:38,760
most Buddhists nowadays it's the ritual that's important. They don't ever have a thought

149
00:20:38,760 --> 00:20:45,640
of keeping the precepts. I once gave the five precepts to a group of Buddhists in Canada.

150
00:20:45,640 --> 00:20:53,960
It was the most shocking experience I've had with a group of Buddhists yet. They invited

151
00:20:53,960 --> 00:20:59,800
us for lunch to their house and they did this quite often. That's why we go to different

152
00:20:59,800 --> 00:21:04,000
people's houses. We went to this one people's house and I think I was the one who gave

153
00:21:04,000 --> 00:21:11,600
the precepts. I gave them the five precepts and then they offered food to the monks in the

154
00:21:11,600 --> 00:21:15,000
monks aid and it was about a half an hour after we'd given precepts and they brought out

155
00:21:15,000 --> 00:21:23,000
a case of beer and started passing the beer out with the monks sitting there. That was

156
00:21:23,000 --> 00:21:29,440
the most shocking thing I'd ever seen as a monk. I ended up seeing a lot more shocking

157
00:21:29,440 --> 00:21:35,200
things than that later on but at that time I was a new monk and it was quite shocking.

158
00:21:35,200 --> 00:21:40,960
I mean and just think of it that not that Buddhists should drink alcohol because most

159
00:21:40,960 --> 00:21:49,920
of many of them do it sadly. But that they should do it during the same occasion that

160
00:21:49,920 --> 00:21:55,720
they had just taken these five training roles that were obviously meaningless to them.

161
00:21:55,720 --> 00:22:05,680
And with the monks sitting right there who were obviously, it was quite incredible.

162
00:22:05,680 --> 00:22:14,200
But this is how it's become. It's become just a culture. It's no longer a religion. In fact,

163
00:22:14,200 --> 00:22:19,440
religion is really a good thing because religion is where you stick to the principles. That's

164
00:22:19,440 --> 00:22:24,680
what religion means to bind or to be bound to certain principles, doing certain things

165
00:22:24,680 --> 00:22:31,120
and not doing certain things. But when it's just culture, it's just for fun really. It's

166
00:22:31,120 --> 00:22:39,960
just for immediate pleasure and recognition of the familiar. So I'm quite, this is getting

167
00:22:39,960 --> 00:22:49,360
off an attention a bit, but I can be quite critical of cultural Buddhism. I think it's

168
00:22:49,360 --> 00:22:56,120
a mistake. Buddhism shouldn't be culture and we shouldn't mix culture and Buddhism. And

169
00:22:56,120 --> 00:23:01,800
I think most monasteries out there are aptly named temples. And if you ever hear of something

170
00:23:01,800 --> 00:23:07,440
being called the temple, I would avoid it. A Buddhist temple. I'm sorry to say there

171
00:23:07,440 --> 00:23:14,120
are many Buddhist temples out there and I would avoid them because Buddhism wasn't made

172
00:23:14,120 --> 00:23:26,320
for temples, for praying and making wishes and hoping to get material gain and so on.

173
00:23:26,320 --> 00:23:32,520
When you hear monks being called priests, avoid them or try to correct them. They shouldn't

174
00:23:32,520 --> 00:23:41,960
be called priests. But I once got a set of monks' robes and it was in a box for export

175
00:23:41,960 --> 00:23:49,000
to America as they've started exporting them. And it's set on the box Buddhist priest suit.

176
00:23:49,000 --> 00:23:57,360
It was a horrible thing to read that there should be a Buddhist priest suit. It's kind

177
00:23:57,360 --> 00:24:04,240
of a boring, that's a Buddhist monk. Anyway, this is often a tangent. But it's actually

178
00:24:04,240 --> 00:24:10,120
not really the important point here is that we are simply playing lip service to these

179
00:24:10,120 --> 00:24:15,760
views. And this is, this is in a sense wrong view. In fact, I believe the Buddha said

180
00:24:15,760 --> 00:24:21,600
or someone, if I'm not wrong, it was the Buddha, if I am wrong, it was some great Buddhist

181
00:24:21,600 --> 00:24:36,600
commentator or teacher who said that wrong, wrong view that is wrong view is easier to

182
00:24:36,600 --> 00:24:48,960
correct than right view. A view, a view that is a view that is out of line with the truth,

183
00:24:48,960 --> 00:24:56,400
such a person who holds such a view is easier to correct, is easier to teach and is much

184
00:24:56,400 --> 00:25:02,000
more likely to gain progress on the path than a person who holds a view that is a view

185
00:25:02,000 --> 00:25:15,440
that is in line with the truth. Because when you talk to a person who holds a view that

186
00:25:15,440 --> 00:25:22,000
is in line with the truth, it goes in one year and out the other, really. You can tell

187
00:25:22,000 --> 00:25:28,720
them about the dangers of samsara and the dangers of defilements and the dangers of ignorance

188
00:25:28,720 --> 00:25:32,160
and on wholesomeness. And they'll just say, yes, yes, yes, yes, I agree, yes, and they'll

189
00:25:32,160 --> 00:25:36,640
be able to cite this and that and quote this and that and everything is so familiar to

190
00:25:36,640 --> 00:25:43,160
them. I guess I know, I know. And it really doesn't affect them at all. You can give

191
00:25:43,160 --> 00:25:48,440
a talk after a talk and they'll just say sadhosa, do great, great, wonderful, wonderful.

192
00:25:48,440 --> 00:25:53,640
But then they go home and they don't practice it. I mean, if you look at these Buddhists,

193
00:25:53,640 --> 00:26:04,640
they hold all these views and then they drink beer in front of the monks. And yet, if

194
00:26:04,640 --> 00:26:15,000
you talk to a person who holds a wrong view and if you're able, if you just explain

195
00:26:15,000 --> 00:26:20,640
to them and if you're, if you're a good teacher, I mean, it's not easy, but it really

196
00:26:20,640 --> 00:26:25,480
doesn't, in many cases, it really doesn't take much. If all it is is a view, then it doesn't

197
00:26:25,480 --> 00:26:31,320
take much. If it's also a clinging and a craving and it's something that's tied up with

198
00:26:31,320 --> 00:26:35,800
their own addictions and their own versions and their own anger and hatred or so on, then

199
00:26:35,800 --> 00:26:40,720
it's quite difficult. But if it's just holding a wrong view, it's actually quite easy to

200
00:26:40,720 --> 00:26:48,480
talk them out of it. Because it's out of line with reality, that's the great thing here.

201
00:26:48,480 --> 00:26:53,480
We're not, we're not trying to convince people to believe something like missionaries

202
00:26:53,480 --> 00:26:59,400
of a certain, of various religions. We're just trying to point out reality. And if their

203
00:26:59,400 --> 00:27:05,760
view is, is in line with reality, then we've got nothing to say about it. But when it's

204
00:27:05,760 --> 00:27:10,600
out of line with reality, we can just point that out to them. So it's always fun to talk

205
00:27:10,600 --> 00:27:16,600
to people of other religions. It's fun and it's a challenge really. Because I'm not

206
00:27:16,600 --> 00:27:26,920
a Buddha and I'm not perfect. But it can be fun when you're able to show them the contradictions

207
00:27:26,920 --> 00:27:40,120
inherent in their stance and their position, the fallacies and the assumptions. And the

208
00:27:40,120 --> 00:27:45,720
best way I always just ask them questions about the view, to help them to examine and explore

209
00:27:45,720 --> 00:27:51,840
their own view. You should never attack people's views, but always help them to explore.

210
00:27:51,840 --> 00:27:56,400
Right? This is, this is objective. This is the best way. And people will respect you

211
00:27:56,400 --> 00:28:01,600
for it and people will be actually willing to change their views. But when you attack people

212
00:28:01,600 --> 00:28:08,960
for their views, they will be more than likely to cling to them. It's stronger than

213
00:28:08,960 --> 00:28:16,440
they did, actually. So the best way to teach people is to help them to explore this with

214
00:28:16,440 --> 00:28:21,240
the Buddha. Did you would ask people questions? Well, this, you know, this is this and this

215
00:28:21,240 --> 00:28:32,480
and this and what is this really leading you to happiness and so on. And that's really

216
00:28:32,480 --> 00:28:39,480
the point, is it is it leading us to peace and is it leading us to happiness. But the

217
00:28:39,480 --> 00:28:44,680
only way we can answer this is through objective experience. We can't intellectually answer

218
00:28:44,680 --> 00:28:50,280
because it's so easy to lie to yourself and convince yourself that you're happy. So many

219
00:28:50,280 --> 00:28:59,040
people do this and they're lied to. We're being lied to by society and by our cultures.

220
00:28:59,040 --> 00:29:05,320
We've told that this is right and this is sufficient. It's enough for us to be an

221
00:29:05,320 --> 00:29:12,800
upstanding moral citizen. The politicians tell you what to do. The movie stars tell you

222
00:29:12,800 --> 00:29:18,920
what to do. The novels and the books that we read are teachers in school tell us what

223
00:29:18,920 --> 00:29:38,040
to do. And so we're taught what is right. But the only way we can really come to be

224
00:29:38,040 --> 00:29:47,960
right is for seeing it for ourselves. This is what meditation is for. The meditation

225
00:29:47,960 --> 00:30:05,520
we come to observe objectively. The reality of our experience. Right here and now it's

226
00:30:05,520 --> 00:30:13,720
all here in front of us. We've got the building blocks all here. We've just contorted

227
00:30:13,720 --> 00:30:20,960
them all into some very strange and awkward position that has left us all tied up

228
00:30:20,960 --> 00:30:28,760
in knots. All we have to do is examine. Take a closer look at what we've done. Untie

229
00:30:28,760 --> 00:30:37,000
the knots and figure out how the building blocks work and build only those structures

230
00:30:37,000 --> 00:30:46,480
that are conducive towards peace, happiness and freedom from suffering or not build

231
00:30:46,480 --> 00:30:56,240
construct constructs at all. Just let it be. But I would say it's not entirely passive

232
00:30:56,240 --> 00:31:02,640
that our practice should be interactive. We should act when it's appropriate to act.

233
00:31:02,640 --> 00:31:09,040
Like when it's appropriate to speak, do when it's, you know, think what's appropriate

234
00:31:09,040 --> 00:31:17,280
to think of. Have a behavior that is appropriate. I think appropriate is the best word

235
00:31:17,280 --> 00:31:31,760
to use. Shouldn't say right or shouldn't say acting purposefully or so on intending

236
00:31:31,760 --> 00:31:38,280
to do lots of good deeds. The person shouldn't be, shouldn't really have intentions

237
00:31:38,280 --> 00:31:44,320
per se. They should just act appropriate. They shouldn't intend for certain consequences

238
00:31:44,320 --> 00:31:55,200
expecting or hoping or wishing. They should only act appropriately. This comes with understanding.

239
00:31:55,200 --> 00:32:04,320
You understand how things work. Everything falls apart. All of our constructs in there.

240
00:32:04,320 --> 00:32:17,440
Knots. They all become untied and untangled and everything flows smoothly and naturally.

241
00:32:17,440 --> 00:32:28,680
The way it goes according to it's like the laws of gravity. It just flows and flows.

242
00:32:28,680 --> 00:32:35,880
So this is, this is all just talk, but hopefully it does clear up some things. I know from

243
00:32:35,880 --> 00:32:41,200
most of you these things are fairly clear already. The most important is for us to just

244
00:32:41,200 --> 00:32:46,400
keep practicing. But it's always good to be reminded of these things because it alerts us

245
00:32:46,400 --> 00:32:52,000
to the reality that is ever present all around us. It reminds us what's going on. It reminds

246
00:32:52,000 --> 00:32:59,520
us to pay attention. This is really what we're doing here. It's reminding ourselves to

247
00:32:59,520 --> 00:33:09,200
pay attention. So that's, I think, enough talk for now. Now we can continue on with maybe

248
00:33:09,200 --> 00:33:16,040
15 minutes of meditation and then we'll quit for the day. So I'm going to turn the

249
00:33:16,040 --> 00:33:17,280
video to the audio off.

