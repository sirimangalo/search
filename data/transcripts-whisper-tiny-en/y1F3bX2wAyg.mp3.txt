Okay, next question.
I had an experience with the abdomen or the rising and falling game to look like waves
in the water, rising and falling.
Is this trying to cool the demeditation to make such a resemblance?
Yeah, it very well could be considered tranquility meditation.
Technically, it's probably not because tranquility meditation is where you are developing
tranquility based on an object.
You're picking an object and you're trying to focus on it and gain tranquility.
What it sounds like rather you're getting into is the result of meditation.
So because of your practice of insight meditation or whatever meditation you're practicing,
this comes up, this experience arises.
So whether you're going to practice tranquility or insight depends on how you approach it.
If you begin to look at the waves and you examine the waves and you're supposed to use
the word rising and falling based on the waves and the wave rises and you see rising
and wave falls and you see falling, then it could be tranquility.
But if you look at the feeling as being a sensation and you see the peace and you see the
tranquility for what it is, so you say to yourself, calm, calm, or you have this feeling
of the wave and you say feeling, feeling, if you like it and you say liking, then it
can be inside because you'll see that it's changing and it's not permanent and so on.
The problem is that at a certain point in your practice, if you're practicing correctly,
all of these sort of mystical experiences will arise.
So some people will feel great bliss and rapture, some people will feel great calm and
tranquility, some people will get seemingly profound insights and some people will get great
faith or great energy or so on, many, many different things will have clarity of mind
and all sorts of seemingly good things will come and as a result one will stop practicing.
I've talked about this before, I think some of you have seen the video I did on what
the meditation is not, and it's not all of these things.
So the seeing the waves in the ocean is not meditating, but it comes from meditating for
it to be meditation again, or the meditate, insight meditation, you have to focus on the
reality of it, and often that's not enough because the mind is clinging to it, often
you have to put it aside or tell your mind stop and so on.
