1
00:00:00,000 --> 00:00:14,240
Okay, so many evening on Wednesdays I'm answering questions, so you're welcome to listen.

2
00:00:14,240 --> 00:00:20,760
These are questions that have been asked through the website, so we're trying to keep them

3
00:00:20,760 --> 00:00:26,400
about meditation practice, and it should be interesting for you here, practicing in our

4
00:00:26,400 --> 00:00:40,880
center. The night's question is about the role or the place of thinking about your practice,

5
00:00:40,880 --> 00:00:59,520
reflecting, analyzing, thinking about whether you're doing your practice right, analyzing your

6
00:00:59,520 --> 00:01:09,720
practice, that sort of thing. And of course, there is room for this. This is something that

7
00:01:09,720 --> 00:01:16,160
you even see in the texts, where the Buddha, the Bodhisatta, when he was trying to become

8
00:01:16,160 --> 00:01:23,600
enlightened, he had to think to himself, wait, what if I were to do it this way instead?

9
00:01:23,600 --> 00:01:31,240
The correct is practice, he had to think, and you see this about the monks like Anandah,

10
00:01:31,240 --> 00:01:45,200
who had to correct this practice, or the general sense that it's a part of our practice,

11
00:01:45,200 --> 00:01:54,760
or practice isn't just mindfulness, of course. There are many other qualities like ethics and

12
00:01:54,760 --> 00:02:05,080
learning the technique of the practice and that sort of thing. There are many practicalities

13
00:02:05,080 --> 00:02:11,960
like livelihood, how are you going to eat? Where are you going to sleep? What are you going

14
00:02:11,960 --> 00:02:24,600
to wear? One of these practicalities is the correcting of one's practice, the analysis

15
00:02:24,600 --> 00:02:31,920
of one's practice. It's called, we monks, we monks, I mean wisdom really, but in regards

16
00:02:31,920 --> 00:02:40,240
to a task that you undertake, we monks, I mean the ability to discern right from wrong

17
00:02:40,240 --> 00:02:47,000
and proper, and proper, effective, ineffective, that sort of thing. Being able to correct

18
00:02:47,000 --> 00:02:52,840
your activities, whether it's meditation or not, you need to be able to analyze and

19
00:02:52,840 --> 00:03:01,320
refine and correct in order to succeed in whatever you do. You can't just push, plow

20
00:03:01,320 --> 00:03:07,760
ahead, sometimes there's correcting that needs to be done.

21
00:03:07,760 --> 00:03:15,920
So in regards to thinking about meditation and correcting your meditation, there are, I would

22
00:03:15,920 --> 00:03:25,480
say, three things that are the criteria for answering whether you can and you shouldn't

23
00:03:25,480 --> 00:03:33,800
think about your practice. The first one is that there is something wrong. So what often

24
00:03:33,800 --> 00:03:41,200
happens is meditators feel like there's something wrong or perceive that there's something

25
00:03:41,200 --> 00:03:46,640
wrong with their practice when there actually isn't, or they misperceived it. This is wrong

26
00:03:46,640 --> 00:03:50,560
with their practice, but instead they say that is wrong with their practice. What I mean

27
00:03:50,560 --> 00:03:55,160
is, for example, they feel pain and they think that's something wrong, or their mind is

28
00:03:55,160 --> 00:03:59,520
chaotic and they think that's something wrong, or they can't control their breath, their

29
00:03:59,520 --> 00:04:07,200
stomach, say, and that's something wrong. And those aren't wrong, those are actually

30
00:04:07,200 --> 00:04:18,080
insights or observations that we want to make about reality that it is in constant,

31
00:04:18,080 --> 00:04:26,760
impermanent, unpredictable, that it is unpleasant at times, unsatisfying really all the

32
00:04:26,760 --> 00:04:40,720
time, and uncontrollable. But there are wrong practice. Wrong practice would be when you're

33
00:04:40,720 --> 00:04:50,240
not being mindful, when you're being mindful of some things, but not being mindful of

34
00:04:50,240 --> 00:04:58,880
something else. Of course, the problems in our practice are wrong is when we're practicing

35
00:04:58,880 --> 00:05:05,680
with anger, like boredom, for example, when we're practicing with doubt, confusion,

36
00:05:05,680 --> 00:05:13,440
worry, fear, craving, when we're practicing out of desire for pleasant states or desire

37
00:05:13,440 --> 00:05:22,120
for enlightenment, focusing on even the goal. If you obsess about enlightenment or cling

38
00:05:22,120 --> 00:05:32,240
to enlightenment, well, even that becomes an object of distraction, diversion and drags

39
00:05:32,240 --> 00:05:42,320
you down away from enlightenment. So there has to be a problem. It can't be that the

40
00:05:42,320 --> 00:05:54,160
practice is unpleasant or so on. If it can't, we have to be clear that our experiences

41
00:05:54,160 --> 00:05:58,280
are not going to be a problem. They're not going to, there's nothing wrong with the practice

42
00:05:58,280 --> 00:06:02,160
just because of what we experience. What's wrong with the practice is how we're relating

43
00:06:02,160 --> 00:06:08,080
to it. Most often not being mindful of something, and that leads to the second aspect of

44
00:06:08,080 --> 00:06:13,440
what you should think about your practice, and that is not only there has to be something

45
00:06:13,440 --> 00:06:19,960
wrong, but you have to be aware of what's wrong. So this is another thing that goes wrong

46
00:06:19,960 --> 00:06:26,400
with practice is we don't realize, and this is where I think thinking about your practice

47
00:06:26,400 --> 00:06:32,120
can be quite valuable, because if you're practicing being mindful of trying to be mindful

48
00:06:32,120 --> 00:06:39,240
of the stomach, and the foot, and feelings, and so on, you just plow ahead and be mindful

49
00:06:39,240 --> 00:06:44,360
of what you know to be mindful of. It's quite common for you to start to miss something.

50
00:06:44,360 --> 00:06:47,360
You start to feel bored, for example, and you're trying to be mindful, but you're so

51
00:06:47,360 --> 00:06:56,120
bored that it's hard, or you're having a lot of doubt. You're trying to push as much and

52
00:06:56,120 --> 00:07:00,120
do as much as you can, but you really can't put the effort in because you're bored,

53
00:07:00,120 --> 00:07:09,720
or because you're not convinced that it's useful, or you really want to want to do it,

54
00:07:09,720 --> 00:07:18,760
and you like to do it, but you're lazy. And so you're ignoring some part of the practice

55
00:07:18,760 --> 00:07:27,880
is quite common, ignoring disliking of pain, boredom of the practice, craving for something,

56
00:07:27,880 --> 00:07:31,540
and you're not mindful of those things, maybe even just the liking of the practice, if it

57
00:07:31,540 --> 00:07:37,600
feels good, and you like it, but you ignore the liking and your mindful of everything else.

58
00:07:37,600 --> 00:07:42,080
Your mindfulness is reduced and eventually becomes ineffective, and you're not really being

59
00:07:42,080 --> 00:07:48,360
mindful because you're just enjoying it, because the enjoyment builds. And of course,

60
00:07:48,360 --> 00:07:57,640
things to disappointment when eventually the objects and the experience changes.

61
00:07:57,640 --> 00:08:03,600
But stepping back and thinking about your practice often gives you the perspective to say,

62
00:08:03,600 --> 00:08:08,760
oh, yes, there I was meditating, but I was missing the boredom, or I was missing the

63
00:08:08,760 --> 00:08:15,840
doubt, or I was mindful of usually the five hindrances, because some wonder another of them

64
00:08:15,840 --> 00:08:31,040
becomes a real drag on your practice. And the third qualities that you have to correct

65
00:08:31,040 --> 00:08:37,600
the practice. And where this is most important is not just you have to do something about

66
00:08:37,600 --> 00:08:43,160
it, but you have to do the correct thing, because it's quite common as well, once we perceive

67
00:08:43,160 --> 00:08:47,960
a problem with our practice, something wrong with our practice, we correct it in the wrong

68
00:08:47,960 --> 00:08:54,320
way, or we do something that's actually not only ineffective, but often problematic.

69
00:08:54,320 --> 00:09:01,000
Like if you're bored, for example, well, then maybe I'll go and enjoy myself, or find

70
00:09:01,000 --> 00:09:04,840
some way to be more pleasant, I'll go and talk to someone and I'm bored, so I'll take

71
00:09:04,840 --> 00:09:10,000
a break and go and avoiding the challenge of dealing with boredom, which is actually just

72
00:09:10,000 --> 00:09:17,120
a version, it's an anger-based line state. Have you have your doubt well, I'll go and

73
00:09:17,120 --> 00:09:22,000
read something, or that sort of thing, rather than challenging yourself facing the doubt

74
00:09:22,000 --> 00:09:32,600
and being mindful of it. So it is, of course, possible that we just ignore it, we have

75
00:09:32,600 --> 00:09:38,880
a problem, we ignore it, that's right. But it's also possible that we try to solve,

76
00:09:38,880 --> 00:09:45,720
try to fix our practice and do everything wrong. So I would say there are sort of three

77
00:09:45,720 --> 00:09:54,280
ways you can fix your practice, and that is the ways that are problematic, that actually

78
00:09:54,280 --> 00:10:02,840
make it worse, ways that are provisionally useful, and ways that are all, and fixing of

79
00:10:02,840 --> 00:10:12,560
your practice, that's ultimately useful. So ways that are actually making it worse is where

80
00:10:12,560 --> 00:10:19,560
you avoid the problem, or you attack one problem with another problem, like you're really

81
00:10:19,560 --> 00:10:24,720
negative about the practice, so you try to find a way to make it more enjoyable, right?

82
00:10:24,720 --> 00:10:29,800
You don't like some things when you're trying to find a way to like it, or you have

83
00:10:29,800 --> 00:10:37,440
a doubt, so you find ways to build up confidence. Actually, that isn't so bad at talking

84
00:10:37,440 --> 00:10:50,720
about that sort of a provisional way, but you might force yourself to believe, for example.

85
00:10:50,720 --> 00:11:00,760
We might very commonly avoid is in this category, so you have disliking of pain, so you

86
00:11:00,760 --> 00:11:06,880
find ways to not have pain, for example. You don't like walking meditation, so you just

87
00:11:06,880 --> 00:11:14,400
sit all the time. That sort of thing. But I think what's really interesting are that

88
00:11:14,400 --> 00:11:21,600
provisionally useful means of fixing your practice. I think that's what a lot of people

89
00:11:21,600 --> 00:11:30,280
often are curious about, because the ultimately beneficial way is, of course, just to be mindful

90
00:11:30,280 --> 00:11:35,720
of what you're not being mindful of. When you step back and look and you realize how well

91
00:11:35,720 --> 00:11:45,680
the problem is, I'm ignoring this one. So really, the ultimate solution to wrong practice

92
00:11:45,680 --> 00:11:50,960
that you should use this introspection, sort of stepping back and analyzing your practice

93
00:11:50,960 --> 00:12:01,360
to pinpoint to identify. The solution is to be mindful of that. That's it. It's quite simple.

94
00:12:01,360 --> 00:12:09,600
But there are many, many sort of provisional, useful techniques, and they involve not trying

95
00:12:09,600 --> 00:12:16,280
to fix the problem or avoid the problem, but to make the problem more bearable or more

96
00:12:16,280 --> 00:12:25,440
endurable or less severe. So if you're very angry, love can be sending love and compassion

97
00:12:25,440 --> 00:12:31,520
can be a very useful sort of provisional tool. It doesn't help you learn about anger, but

98
00:12:31,520 --> 00:12:36,800
it can make the anger more manageable. If you have lust or desire, you can look at the body

99
00:12:36,800 --> 00:12:42,240
and the parts of the body and think about them. And that's quite useful. But provisionally,

100
00:12:42,240 --> 00:12:48,120
it doesn't help you learn about lust and desire. It doesn't help you understand that reality.

101
00:12:48,120 --> 00:12:53,560
It isn't the same as for example, saying liking, liking, or wanting, wanting, or observing

102
00:12:53,560 --> 00:13:01,280
the object of your desire, seeing, seeing, or hearing, or thinking. If you have doubt,

103
00:13:01,280 --> 00:13:07,640
you can think about the Buddha or fear as well. People have fear living in the forest, afraid

104
00:13:07,640 --> 00:13:13,720
of ghosts or monsters or animals. You can think about the Buddha and it gives you courage.

105
00:13:13,720 --> 00:13:21,600
It gives you energy, it gives you faith and confidence in yourself. But provisionally, it

106
00:13:21,600 --> 00:13:25,800
doesn't help you learn about doubt and learn about the states that lead to doubt and that sort

107
00:13:25,800 --> 00:13:37,360
of thing. Which the Buddha talked about in the Satipatana Sutta. And there are rather provisionally

108
00:13:37,360 --> 00:13:45,280
useful techniques to deal with things like being tired. So if you're tired, you can get up and

109
00:13:45,280 --> 00:13:52,160
do walking meditation, of course, but you can also look at the light or go outside, splash water

110
00:13:52,160 --> 00:14:01,120
on your face. So there are, I would say, an infinite idea as you can, ways you can come up with

111
00:14:01,120 --> 00:14:08,240
to support your practice. But it comes down to your reasoning for doing these things. If your

112
00:14:08,240 --> 00:14:16,000
reason is to avoid the problem, then you end up creating a habit of avoidance. So they

113
00:14:16,000 --> 00:14:21,520
should never be, when you step back and look at your practice, you should never be looking

114
00:14:21,520 --> 00:14:27,120
for a way to fix and get away from the problem. You should be looking for a way to manage

115
00:14:27,120 --> 00:14:38,160
it. And like a strategist, like a war, warlord, a marsh, what do you call a general in the

116
00:14:38,160 --> 00:14:44,240
war? You have to look at the battlefield and not just rush in, but you have to figure out strategically

117
00:14:44,240 --> 00:14:50,000
when to retreat and when to move forward in which ways to move forward. Because our goal in the

118
00:14:50,000 --> 00:14:55,600
meditation is ultimately to just be mindful of the objects. But if there's a lot of pain, for example,

119
00:14:56,960 --> 00:15:03,440
you don't say, I'll make it so there's no pain. But you say, I'll retreat and I'll make it

120
00:15:03,440 --> 00:15:07,680
so that the pain is manageable and I can actually be mindful of it with the idea and

121
00:15:07,680 --> 00:15:16,160
mind that eventually I just sit through the pain, even at full force. If you have anger and you say,

122
00:15:16,160 --> 00:15:21,840
well, I can't deal with this anger too much. Then you step back and you send love for a bit.

123
00:15:22,880 --> 00:15:27,360
And you find that the anger is reduced and then you can deal with it and eventually your goal

124
00:15:27,360 --> 00:15:33,040
as the generals go on the war is eventually to defeat the enemy, not to always be retreating.

125
00:15:33,040 --> 00:15:38,400
But sometimes you have to retreat and come back at it from a different angle.

126
00:15:40,880 --> 00:15:49,280
So absolutely there is room for thought, for thinking about your practice. But it should not be

127
00:15:50,720 --> 00:15:57,440
as a means to fix and to change things so that you don't have to deal with your problems.

128
00:15:57,440 --> 00:16:05,600
And the other thing I guess I would say is that if you're going to think about your practice,

129
00:16:05,600 --> 00:16:10,400
thinking itself can obviously become a problem. And I think this is the person who has the

130
00:16:10,400 --> 00:16:14,880
question was fairly well aware of this. And this can be a problem where you actually

131
00:16:15,840 --> 00:16:21,360
substitute practice with thinking about the practice and it becomes a habit where you're constantly

132
00:16:21,360 --> 00:16:28,640
analyzing and as a result doubting. I would say where it becomes a problem, you'll be able to see

133
00:16:28,640 --> 00:16:34,880
if you're mindful because the problems arise, it arises in the same way as it wouldn't

134
00:16:34,880 --> 00:16:39,840
practice. You'll start to incorporate doubt, not only are you thinking about your practice,

135
00:16:39,840 --> 00:16:47,680
but you're doubting or you're confused or you're worried or you're wanting your desire.

136
00:16:47,680 --> 00:16:51,520
And you start to get into a habit of thinking about your practice because I'm going to find the

137
00:16:51,520 --> 00:16:55,840
right way. I'm going to find a way to make it better. I'm going to find a way to make it perfect.

138
00:16:59,440 --> 00:17:04,640
So thinking about your practice is useful. It should be done within fairly strict parameters,

139
00:17:04,640 --> 00:17:10,960
understanding that ultimately the solution is the ultimate solution you should come to is

140
00:17:10,960 --> 00:17:14,240
the mindful of those things that you catch that you weren't being mindful of.

141
00:17:14,240 --> 00:17:21,680
And you're thinking about the practice allows you to pinpoint those objects, those aspects of

142
00:17:21,680 --> 00:17:30,560
your experience that you were ignoring, that you were avoiding. So that's an answer to that question,

143
00:17:30,560 --> 00:17:36,800
the question of whether and where and how and to what extent thinking about your practice

144
00:17:36,800 --> 00:17:46,800
is useful and beneficial. Thank you.

