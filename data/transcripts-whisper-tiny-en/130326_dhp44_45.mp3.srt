1
00:00:00,000 --> 00:00:05,000
Okay, hello and welcome back to our study of the Damapada.

2
00:00:05,000 --> 00:00:12,000
Today, we will be continuing on with verses 44 and 45,

3
00:00:12,000 --> 00:00:16,000
and the Para verses which go as follows.

4
00:00:16,000 --> 00:00:19,000
Ko imang pattawing which is a tea,

5
00:00:19,000 --> 00:00:22,000
yam alo kang imang sadeva kang.

6
00:00:22,000 --> 00:00:25,000
Ko damapadang su desitang,

7
00:00:25,000 --> 00:00:31,000
kusulopupamir pattasati.

8
00:00:31,000 --> 00:00:35,000
Ko imang pattawing which is a tea,

9
00:00:35,000 --> 00:00:38,000
yam alo kang imang sadeva kang.

10
00:00:38,000 --> 00:00:43,000
Ko damapadang su desitang,

11
00:00:43,000 --> 00:00:48,000
kusulopupamir pattasati.

12
00:00:48,000 --> 00:00:54,000
Which means a question and answer the first verses.

13
00:00:54,000 --> 00:00:57,000
Ko imang pattawing which is a tea,

14
00:00:57,000 --> 00:01:03,000
who will overcome this world, yam alo kang imang sadeva kang.

15
00:01:03,000 --> 00:01:06,000
Who will overcome this world that is the,

16
00:01:06,000 --> 00:01:09,000
who will overcome this earth, sorry.

17
00:01:09,000 --> 00:01:12,000
Who will overcome this earth, this world,

18
00:01:12,000 --> 00:01:15,000
of the yam and the Lord of Death,

19
00:01:15,000 --> 00:01:20,000
along with the angels sadeva kang.

20
00:01:20,000 --> 00:01:23,000
Ko damapadang su desitang,

21
00:01:23,000 --> 00:01:25,000
kusulopupamir pattasati.

22
00:01:25,000 --> 00:01:28,000
Who will bring to perfection this path of dhamma,

23
00:01:28,000 --> 00:01:31,000
or this practice of dhamma that is well thought,

24
00:01:31,000 --> 00:01:44,000
just as a skillful person might bring to perfection a bouquet of flowers.

25
00:01:44,000 --> 00:01:48,000
And then the answer is seiko which means a trainee,

26
00:01:48,000 --> 00:01:50,000
or one who is training,

27
00:01:50,000 --> 00:01:53,000
one who trains themselves

28
00:01:53,000 --> 00:01:56,000
will overcome this earth,

29
00:01:56,000 --> 00:01:59,000
this world of yamma,

30
00:01:59,000 --> 00:02:01,000
together with the angels,

31
00:02:01,000 --> 00:02:03,000
together with its angels,

32
00:02:03,000 --> 00:02:08,000
or all celestial beings.

33
00:02:08,000 --> 00:02:11,000
The trainer will bring to perfection this path of dhamma,

34
00:02:11,000 --> 00:02:15,000
which is well-expounded just as a skillful person

35
00:02:15,000 --> 00:02:22,000
would bring to perfection a bouquet of flowers.

36
00:02:22,000 --> 00:02:30,000
So this is actually something

37
00:02:30,000 --> 00:02:34,000
it's quite a profound question and answer,

38
00:02:34,000 --> 00:02:37,000
something that we can use here to

39
00:02:37,000 --> 00:02:40,000
just to expound the dhamma

40
00:02:40,000 --> 00:02:44,000
or as a basis for teaching the dhamma.

41
00:02:44,000 --> 00:02:48,000
But the story, as there's a story behind all of these,

42
00:02:48,000 --> 00:02:52,000
first we have to say where this verse came from,

43
00:02:52,000 --> 00:02:55,000
the story is that there were,

44
00:02:55,000 --> 00:02:58,000
it's actually a two paragraph story,

45
00:02:58,000 --> 00:03:01,000
the story goes that there were a bunch of monks

46
00:03:01,000 --> 00:03:05,000
who were sitting around talking about the earth,

47
00:03:05,000 --> 00:03:09,000
and as monks are people as well,

48
00:03:09,000 --> 00:03:14,000
they talk tends to wonder if they're not careful.

49
00:03:14,000 --> 00:03:17,000
And so they're talking wandered into worldly subjects,

50
00:03:17,000 --> 00:03:20,000
where they began to talk about the earth.

51
00:03:20,000 --> 00:03:23,000
And so they were talking about how the earth in this district

52
00:03:23,000 --> 00:03:27,000
was black, and the earth in that district was brown,

53
00:03:27,000 --> 00:03:30,000
and how this district was a very muddy earth,

54
00:03:30,000 --> 00:03:37,000
and this district had gravelly and sandy earth.

55
00:03:37,000 --> 00:03:40,000
And the Buddha came in and asked them

56
00:03:40,000 --> 00:03:42,000
what they were talking about, and they told them,

57
00:03:42,000 --> 00:03:44,000
and they said that, he said,

58
00:03:44,000 --> 00:03:49,000
he used this verse as a reminder as a means of instilling

59
00:03:49,000 --> 00:03:52,000
some sort of sense of urgency in them.

60
00:03:52,000 --> 00:03:54,000
He said, well, that's the external earth,

61
00:03:54,000 --> 00:04:01,000
but it really has no purpose for you to consider,

62
00:04:01,000 --> 00:04:04,000
to concern yourselves with,

63
00:04:04,000 --> 00:04:06,000
when compared to the inner earth,

64
00:04:06,000 --> 00:04:08,000
the earth that is inside of you.

65
00:04:08,000 --> 00:04:11,000
And so the real earth is this world of death,

66
00:04:11,000 --> 00:04:18,000
the physical realm that is subject to change,

67
00:04:18,000 --> 00:04:25,000
and is aging and fading away,

68
00:04:25,000 --> 00:04:32,000
and this body that will eventually go back to the earth.

69
00:04:32,000 --> 00:04:34,000
The death that is ahead of us,

70
00:04:34,000 --> 00:04:38,000
or this world which is encompassed from the world,

71
00:04:38,000 --> 00:04:42,000
from the lower realms all the way up to the angel realms.

72
00:04:42,000 --> 00:04:45,000
So Yamaloka is considered to mean

73
00:04:45,000 --> 00:04:47,000
the lower realms of the hell realms,

74
00:04:47,000 --> 00:04:50,000
the animal realms, the ghost realms,

75
00:04:50,000 --> 00:04:51,000
the Deva Lokas.

76
00:04:51,000 --> 00:04:53,000
The Deva kang means the angel realms

77
00:04:53,000 --> 00:04:55,000
means the human realms, the angels,

78
00:04:55,000 --> 00:05:00,000
and the gods, and so on, but those three.

79
00:05:00,000 --> 00:05:07,000
So he said,

80
00:05:07,000 --> 00:05:10,000
this is what you should be focusing on.

81
00:05:10,000 --> 00:05:13,000
Here you have something that is much more important,

82
00:05:13,000 --> 00:05:15,000
and you should be concerning yourself

83
00:05:15,000 --> 00:05:21,000
with how to understand this earth.

84
00:05:21,000 --> 00:05:23,000
And so he gave this verse.

85
00:05:23,000 --> 00:05:28,000
He said, who can overcome this earth?

86
00:05:28,000 --> 00:05:31,000
He said, I made them think about this.

87
00:05:31,000 --> 00:05:34,000
Think about the Dhamma in this way,

88
00:05:34,000 --> 00:05:35,000
and said, who will do this?

89
00:05:35,000 --> 00:05:37,000
Who will be able to overcome this?

90
00:05:37,000 --> 00:05:41,000
Certainly not the people who sit around talking about use the subject.

91
00:05:41,000 --> 00:05:43,000
Who will be able to overcome this earth?

92
00:05:43,000 --> 00:05:47,000
Will there be the lower realms or the higher realms?

93
00:05:47,000 --> 00:05:49,000
Who will be able to put into practice

94
00:05:49,000 --> 00:05:52,000
and bring to perfection this path of Dhamma,

95
00:05:52,000 --> 00:05:55,000
this path of truth?

96
00:05:55,000 --> 00:05:58,000
The path to realize the truth,

97
00:05:58,000 --> 00:06:03,000
just as a skillful person would bring to perfection

98
00:06:03,000 --> 00:06:08,000
or make a perfect bouquet of flowers.

99
00:06:08,000 --> 00:06:11,000
Well, I think this is useful

100
00:06:11,000 --> 00:06:15,000
because it's quite profound

101
00:06:15,000 --> 00:06:19,000
because it asks the question of how of who is it

102
00:06:19,000 --> 00:06:24,000
or how can we address

103
00:06:24,000 --> 00:06:28,000
the problem that we're faced with?

104
00:06:28,000 --> 00:06:38,000
This suffering that we have ahead of us,

105
00:06:38,000 --> 00:06:40,000
the old aim, sickness and death,

106
00:06:40,000 --> 00:06:45,000
how do we address the issue of being mortal

107
00:06:45,000 --> 00:06:50,000
or how do we address the issue of being subject to suffering?

108
00:06:50,000 --> 00:06:55,000
And once we understand what is the path,

109
00:06:55,000 --> 00:06:57,000
who is it or what do we need to do

110
00:06:57,000 --> 00:07:01,000
in order to perfect the practice

111
00:07:01,000 --> 00:07:06,000
and perfect the path to become free from suffering?

112
00:07:06,000 --> 00:07:10,000
And the Buddha makes a direct definite statement

113
00:07:10,000 --> 00:07:13,000
as to what it requires,

114
00:07:13,000 --> 00:07:15,000
what it will require for us to do that

115
00:07:15,000 --> 00:07:17,000
and this is the training

116
00:07:17,000 --> 00:07:20,000
that a person who trains themselves

117
00:07:20,000 --> 00:07:22,000
is one who will be able to overcome the earth

118
00:07:22,000 --> 00:07:29,000
and will be able to overcome all waged sickness and death.

119
00:07:29,000 --> 00:07:30,000
A person who trains their mind,

120
00:07:30,000 --> 00:07:32,000
who trains themselves in morality,

121
00:07:32,000 --> 00:07:34,000
who trains himself in concentration,

122
00:07:34,000 --> 00:07:37,000
who trains themselves in wisdom,

123
00:07:37,000 --> 00:07:41,000
this is a person who will be able to overcome

124
00:07:41,000 --> 00:07:45,000
all waged sickness and death.

125
00:07:45,000 --> 00:07:52,140
This is a person who will be able to put into practice the Buddha's teaching who will be able to put in who will be able to bring to perfection

126
00:07:52,520 --> 00:07:54,520
in the Buddha's teaching

127
00:07:55,080 --> 00:07:57,960
We are able to bring to perfection this path of Dhamma

128
00:08:04,600 --> 00:08:09,480
The the distinction that we have to make is between the mundane world and the

129
00:08:09,480 --> 00:08:14,840
ultimate reality, right? This is where the Buddha points them in the right direction

130
00:08:16,840 --> 00:08:18,840
An ordinary person will

131
00:08:19,240 --> 00:08:24,120
Tend to think that there is no way to overcome all-dage sickness and death. These are part of life

132
00:08:24,120 --> 00:08:31,640
You think well, that's part of the bargain you you get to enjoy life this life that you have and you have to

133
00:08:32,920 --> 00:08:36,680
Accept the fact that it's going to end in all-dage sickness and death

134
00:08:36,680 --> 00:08:42,440
And so we think in conventional sense just as these monks were thinking about the conventional earth

135
00:08:42,440 --> 00:08:44,440
So it's actually quite an apt story

136
00:08:44,840 --> 00:08:46,840
because it

137
00:08:46,840 --> 00:08:52,360
There's a parallel in how we look at our lives as well. We look at life as being

138
00:08:53,320 --> 00:08:59,320
Sort of a one-shot even people who claim to believe in in the afterlife and so on will tend to

139
00:09:00,040 --> 00:09:02,040
focus all of their efforts

140
00:09:02,040 --> 00:09:04,040
or most of their efforts on

141
00:09:04,680 --> 00:09:06,200
finding

142
00:09:06,200 --> 00:09:09,320
Pleasure and and avoiding suffering in this life

143
00:09:10,120 --> 00:09:13,560
Trying their best to be free from sickness find trying

144
00:09:14,680 --> 00:09:16,680
Their best to obtain

145
00:09:16,760 --> 00:09:18,760
Central gratification in this life

146
00:09:20,680 --> 00:09:22,920
Without without making any thought of

147
00:09:24,520 --> 00:09:29,960
Of the broader the bigger picture of the ultimate reality. What's really going on and what's really happening?

148
00:09:29,960 --> 00:09:35,640
Happening they realize that actually old-age sickness and death is

149
00:09:36,520 --> 00:09:40,520
Not just an inevitability of life. It's a constant reoccurrence

150
00:09:41,320 --> 00:09:46,120
And it comes with a cause or it comes because of a cause the cause is birth

151
00:09:47,800 --> 00:09:50,200
And that no, it's not enough to put up with

152
00:09:51,320 --> 00:09:53,320
Old-age sickness and death because that's

153
00:09:54,280 --> 00:09:56,280
Like one wave crashing against the shore

154
00:09:56,280 --> 00:10:00,280
If you're not able to overcome your

155
00:10:04,280 --> 00:10:06,440
The the things that lead you to be born again

156
00:10:06,680 --> 00:10:13,080
Then you have to put up with this again and again and again so putting up with it with it isn't enough or accepting it isn't enough

157
00:10:14,680 --> 00:10:16,680
Where we we create

158
00:10:17,640 --> 00:10:19,640
The cause for it to to occur

159
00:10:20,440 --> 00:10:22,760
Again and again for for eternity

160
00:10:22,760 --> 00:10:27,480
With our addiction with our attachment with our

161
00:10:28,120 --> 00:10:30,120
Concern for the mundane

162
00:10:31,320 --> 00:10:33,960
With all of our attachments all of our

163
00:10:35,240 --> 00:10:39,240
Obsessions all of our worries and our cares on a conventional level

164
00:10:40,200 --> 00:10:42,600
We lose sight of what of the ultimate reality

165
00:10:43,720 --> 00:10:46,600
We lose sight of what's really going on or what we're really doing

166
00:10:46,600 --> 00:10:52,920
We lose sight of the cycle the the bigger picture that what we're creating here is actually a cycle. It's not a one shot

167
00:10:52,920 --> 00:10:54,520
It's not linear

168
00:10:54,520 --> 00:11:00,040
It's not like birth goes to death in the line and then that's it. It's a cycle so birth goes to death

169
00:11:00,920 --> 00:11:02,920
Which leads to birth again

170
00:11:03,240 --> 00:11:08,760
Just like waves crashing against the shore one after another after another forever

171
00:11:08,760 --> 00:11:15,400
So the the Buddha points out that

172
00:11:17,400 --> 00:11:21,640
Something needs to be something needs to change about the way we look at the world

173
00:11:22,920 --> 00:11:24,920
We can't let ourselves

174
00:11:25,960 --> 00:11:31,080
Follow after our habits and our you know following our heart for example

175
00:11:31,080 --> 00:11:37,560
Something that we have something has to change about how we look at the world how we look at our lives

176
00:11:38,920 --> 00:11:44,760
This is what happens when someone comes to practice the Buddha's teaching when we listen to the dhamma when we

177
00:11:45,240 --> 00:11:47,240
Read the Buddha's teaching

178
00:11:47,640 --> 00:11:51,000
We start to affect this change. We realize that there's much more going on

179
00:11:51,880 --> 00:11:53,880
Then meets the eye

180
00:11:53,880 --> 00:12:01,880
That the things that we cling to are not actually bringing us happiness. They're creating a habit of clinging

181
00:12:03,480 --> 00:12:06,920
That the the things that we bear with are not actually

182
00:12:08,120 --> 00:12:11,880
It's not actually out of patience. It's actually with a lot of suffering

183
00:12:13,720 --> 00:12:18,440
And cultivating an aversion towards things when when when we escape from pain

184
00:12:19,320 --> 00:12:21,640
by taking medication or by

185
00:12:21,640 --> 00:12:25,800
Avoiding it. We actually cultivate

186
00:12:26,680 --> 00:12:29,000
Greater and greater aversion towards the pain

187
00:12:30,520 --> 00:12:34,440
So these are not the we start to realize that this is not the way out of suffering

188
00:12:34,840 --> 00:12:38,920
We realize this when we begin to train ourselves starting with morality

189
00:12:41,000 --> 00:12:43,000
We start to realize that

190
00:12:43,000 --> 00:12:45,000
Realize these habits are

191
00:12:45,240 --> 00:12:47,240
Well habit forming and are

192
00:12:47,240 --> 00:12:52,920
Magnified the more we cultivate them

193
00:12:54,760 --> 00:12:59,240
And so we undertake morality we undertake to prevent ourselves to inhibit to

194
00:13:00,360 --> 00:13:03,160
Stop ourselves from following after the habits

195
00:13:05,560 --> 00:13:07,560
We train ourselves to

196
00:13:08,680 --> 00:13:14,120
To be patient when we would otherwise chase after something we want something instead of chasing after we

197
00:13:14,120 --> 00:13:17,960
Stop ourselves and bring ourselves back to the experience of it

198
00:13:18,920 --> 00:13:25,160
When you want something you're very little involved with that actual thing and you're much more involved with the thoughts and the pleasures that

199
00:13:25,720 --> 00:13:27,720
arise in the mind

200
00:13:28,280 --> 00:13:30,280
And when you're able to bring yourself back you

201
00:13:31,720 --> 00:13:36,200
This is the first step is bringing the mind back to reality. It's called morality

202
00:13:37,800 --> 00:13:41,000
True morality is in the mind when you bring your mind back to

203
00:13:41,000 --> 00:13:47,800
What's really happening what's really going on with the essence of the experience

204
00:13:48,600 --> 00:13:50,600
When you see something and you want it

205
00:13:52,200 --> 00:13:55,320
You look at the seeing and you look at the feeling and you look at the wanting

206
00:13:56,760 --> 00:13:58,760
And you see that it's

207
00:13:59,320 --> 00:14:02,280
Really just impersonal phenomena rising and ceasing

208
00:14:03,240 --> 00:14:07,400
You break it apart and you see that it's because for stress because for suffering

209
00:14:07,400 --> 00:14:10,200
Not a cause for happiness and contentment

210
00:14:11,880 --> 00:14:15,640
And you become content without you become tent with or without

211
00:14:17,640 --> 00:14:23,800
And you create so you you create what we call morality and as a result of the morality you begin to develop

212
00:14:24,120 --> 00:14:26,920
Concentrations so you train yourself further in

213
00:14:27,800 --> 00:14:31,160
Once you've called it a morality you train yourself to keep the mind

214
00:14:31,720 --> 00:14:35,960
Once you've brought the mind back to the present you train yourself to keep the mind focused on the present

215
00:14:35,960 --> 00:14:41,240
When you see something you train yourself focus on seeing when you hear something hearing when you smell

216
00:14:42,360 --> 00:14:48,120
You train yourself to focus on the body on the feelings on the thoughts on the emotions and the senses

217
00:14:49,080 --> 00:14:51,960
You train yourself to focus on reality so you cultivate

218
00:14:52,920 --> 00:14:54,920
concentration as a result

219
00:14:56,680 --> 00:14:58,680
not only

220
00:14:59,720 --> 00:15:02,600
Not only are you pulling your mind back but your mind begins to

221
00:15:02,600 --> 00:15:07,160
Be distant-clined to actually leave the present reality

222
00:15:08,840 --> 00:15:12,840
So all of us sitting here now we're sitting you're sitting listening to this dog

223
00:15:15,000 --> 00:15:20,440
We have two things going on we have the ultimate reality which is where we think we are where we'd like to be

224
00:15:20,920 --> 00:15:25,240
We'd like to be here in an ultimate reality sounds kind of

225
00:15:26,920 --> 00:15:31,240
Technical or specialty it actually just means this what we think we're in at all times

226
00:15:31,240 --> 00:15:36,120
So the reality of seeing the reality of hearing smelling tasting feeling

227
00:15:36,760 --> 00:15:38,760
The reality of our experience

228
00:15:39,000 --> 00:15:43,640
But something else that's going on is all the activity in the mind the judgments the

229
00:15:44,760 --> 00:15:48,120
Projections the extrapolations the identifications

230
00:15:49,240 --> 00:15:55,480
All of the concepts that arise of people in places and things all of the past and the future. This is all conceptual

231
00:15:56,200 --> 00:15:58,200
That's nothing to do with the ultimate reality

232
00:15:58,200 --> 00:16:00,200
So we begin to

233
00:16:00,840 --> 00:16:05,720
The training that we're looking for is to keep ourselves with the reality keep ourselves here

234
00:16:06,440 --> 00:16:12,280
Here we all are sitting together the training is the train is to keep ourselves here and now

235
00:16:15,720 --> 00:16:19,480
Once we do that, this is the the training in concentration

236
00:16:20,280 --> 00:16:22,200
The third training the training in wisdom

237
00:16:22,920 --> 00:16:25,880
Is that once we keep ourselves here we begin to understand

238
00:16:25,880 --> 00:16:29,880
Here we can understand reality and we begin to see the distinction

239
00:16:30,600 --> 00:16:32,600
between reality and illusion

240
00:16:33,320 --> 00:16:35,320
We see how there's no

241
00:16:35,320 --> 00:16:41,560
There's nothing essentially good or bad about anything we see that all of the people in places and things even our own body

242
00:16:42,040 --> 00:16:44,040
It's just a concept

243
00:16:44,040 --> 00:16:46,680
That experiences arise and sees to come and go

244
00:16:47,800 --> 00:16:49,800
and because of wisdom

245
00:16:49,800 --> 00:16:51,800
we begin to

246
00:16:52,520 --> 00:16:54,520
Need less and less

247
00:16:54,520 --> 00:17:01,960
Effort to focus ourselves and less and less effort to bring our minds back our minds are less inclined to move away and

248
00:17:03,320 --> 00:17:07,400
More inclined towards focusing more in inclined towards

249
00:17:08,280 --> 00:17:10,440
Peace and calm and clarity of mind

250
00:17:11,400 --> 00:17:17,080
Not just because we pull our minds back or try to keep our minds fixed but because we understand

251
00:17:18,360 --> 00:17:20,360
the uselessness of

252
00:17:20,840 --> 00:17:22,840
Chasing after illusion

253
00:17:22,840 --> 00:17:30,280
This is the the training in wisdom. We train ourselves not only to bring our minds back, not only to force our minds to stay

254
00:17:31,000 --> 00:17:33,000
but to

255
00:17:34,360 --> 00:17:35,960
have our minds

256
00:17:35,960 --> 00:17:37,880
understand

257
00:17:37,880 --> 00:17:41,960
The benefit of staying the benefit of being in the present moment

258
00:17:42,440 --> 00:17:47,640
Take ourselves how much stress is involved with running away and chasing after illusions

259
00:17:48,440 --> 00:17:50,440
Chasing after concepts

260
00:17:50,440 --> 00:17:54,120
So these are the three trainings and this the Buddha said is how one

261
00:17:55,480 --> 00:17:59,560
Overcomes both overcomes the bad things in life and cultivates the good things

262
00:17:59,800 --> 00:18:05,160
So the first part is overcomes the world we chase it deep overcomes the the earth

263
00:18:05,640 --> 00:18:09,240
Overcomes all the all the troubles and all the difficulties and all the

264
00:18:09,800 --> 00:18:14,840
stresses and sufferings that are in air and in life and how one cultivates the dhamma banda

265
00:18:15,320 --> 00:18:18,200
the path of truth in the path of

266
00:18:18,200 --> 00:18:19,560
of

267
00:18:19,560 --> 00:18:21,560
Righteousness

268
00:18:22,680 --> 00:18:27,640
And brings it to perfection when you have all of the Buddha's teachings or all of the good things in the world

269
00:18:27,960 --> 00:18:35,560
How do you bring them to perfection? This is the concise summary of how to do that is one becomes a trainee when trains oneself

270
00:18:37,400 --> 00:18:40,600
So one one way of understanding Buddhism is it's a training

271
00:18:40,600 --> 00:18:47,720
It's not something that you can wish for hope for or something that you can be when you put on robes or so and

272
00:18:48,360 --> 00:18:50,360
It's something that you train in

273
00:18:51,000 --> 00:18:55,400
Just as we train our bodies and we train our minds in a world they sense to

274
00:18:56,040 --> 00:18:58,360
To become proficient at this skill or that skill

275
00:18:58,920 --> 00:19:04,920
So too we train our minds we train our hearts in the dhamma in wholesomeness

276
00:19:04,920 --> 00:19:12,600
And we train them out of unwholesome. There's unskillful states states that cause stress and suffering for us and others

277
00:19:14,360 --> 00:19:16,360
So this is the

278
00:19:16,360 --> 00:19:18,360
teaching in these two verses quite a

279
00:19:19,800 --> 00:19:23,240
sort of pithy sort of summary of how one

280
00:19:25,080 --> 00:19:30,520
brings one brings the Buddha's teaching or brings all good states to perfection just as one

281
00:19:30,520 --> 00:19:34,920
A skillful person went a skillful flower maker

282
00:19:36,680 --> 00:19:42,360
Florist went and put together a bouquet of flowers so all of the good things in our mind

283
00:19:43,320 --> 00:19:45,320
flower and

284
00:19:47,320 --> 00:19:51,640
We become in a sense perfect through our

285
00:19:54,680 --> 00:19:58,920
Abandoning of evil and wholesome states and through the cultivation of good states

286
00:19:58,920 --> 00:20:05,000
So this is the teaching in this verse and that's the dhamma Pandas. So very much

287
00:20:06,680 --> 00:20:12,280
Very much a practical teaching. This is obviously something that we have to use in our meditation. It's something that

288
00:20:14,280 --> 00:20:16,760
You know the the the highest training in Buddhism

289
00:20:17,720 --> 00:20:19,720
Is here it comes from

290
00:20:19,960 --> 00:20:21,000
these three

291
00:20:21,000 --> 00:20:22,520
comes from the

292
00:20:22,520 --> 00:20:26,360
meditative practice of morality the meditative practice of concentration

293
00:20:26,360 --> 00:20:30,920
Keep bringing the mind back keeping the mind there and the meditative concentrate meditative

294
00:20:31,720 --> 00:20:36,840
Practice or training a wisdom of understanding the understanding that keeps you

295
00:20:38,040 --> 00:20:40,840
Out of illusion and delusion keeps the mind from

296
00:20:41,800 --> 00:20:43,720
Wandering keeps the mind in

297
00:20:45,000 --> 00:20:47,000
Intune with reality

298
00:20:47,000 --> 00:20:50,600
That's the teaching for today on verses 44 and 45

299
00:20:51,320 --> 00:20:53,320
Thank you for tuning in and I hope

300
00:20:53,320 --> 00:20:59,080
You're able to put this into practice and follow the path of follow the dhamma Panda on your own

