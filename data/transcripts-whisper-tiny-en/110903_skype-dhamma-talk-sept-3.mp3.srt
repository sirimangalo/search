1
00:00:00,000 --> 00:00:12,400
Okay, well welcome everyone. Thank you for inviting me and I guess we'll just jump right

2
00:00:12,400 --> 00:00:20,920
into it. The talk tonight or the session tonight is based around as it really always

3
00:00:20,920 --> 00:00:32,240
is the Sati Bhattana Sutta or the Buddha's teaching on the four foundations of mindfulness.

4
00:00:32,240 --> 00:00:44,520
In the Sutta the Buddha said, A. K. A. N. O. A. and Bhikkhu. This O. monks, O. U. O. Bhikkhu's and Bhikkhu

5
00:00:44,520 --> 00:00:54,280
could mean anyone who realizes the importance of practicing meditation. Oh, you see the

6
00:00:54,280 --> 00:01:00,280
importance of mental training, importance of mindfulness. We'll see the danger in being

7
00:01:00,280 --> 00:01:10,280
negligent. A. K. A. N. O. A. and A. N. A. N. This path is the one way. This is how the Buddha

8
00:01:10,280 --> 00:01:16,240
started his discourse. And so I think it's important to understand what we mean by the one

9
00:01:16,240 --> 00:01:27,040
way. Because this is the very beginning of the discourse. It's a very important part. It's

10
00:01:27,040 --> 00:01:34,120
a very bold claim. And unfortunately I think it hits many people the wrong way because

11
00:01:34,120 --> 00:01:41,480
it's a claim made by all religious doctrines. We claim to have the only way. And so I

12
00:01:41,480 --> 00:01:46,080
want to clarify this a little bit. That really, this isn't saying that Buddhism or the

13
00:01:46,080 --> 00:01:53,760
Buddha's idea is the only idea that's right. Because the path that the Buddha talks about

14
00:01:53,760 --> 00:02:00,640
is the path of being mindful. And what this really means is by paying attention and by

15
00:02:00,640 --> 00:02:07,640
paying proper attention. And so I think it should be quite agreeable to us all to think

16
00:02:07,640 --> 00:02:15,360
that if you don't pay proper attention, you can never really come to understand reality.

17
00:02:15,360 --> 00:02:23,640
Now the Buddha's teaching rests on the theory that understanding reality is enough to bring

18
00:02:23,640 --> 00:02:29,440
about freedom from suffering. So if we have an argument with this, then we might think

19
00:02:29,440 --> 00:02:38,600
this is not the right way. But if we can at least appreciate the theory that understanding

20
00:02:38,600 --> 00:02:43,960
reality is the way to be find true peace and happiness, then we really shouldn't have

21
00:02:43,960 --> 00:02:52,880
a problem with the idea that examining reality and looking at reality is the only way.

22
00:02:52,880 --> 00:02:57,520
Because if you don't look, you'll never see. So the teaching here is to really come

23
00:02:57,520 --> 00:03:04,840
to understand reality. And the Buddha said, this is the only way to five goals. There are

24
00:03:04,840 --> 00:03:12,240
five things that this path leads to. It's the only way. It's the way that leads only

25
00:03:12,240 --> 00:03:21,040
to these things that can't lead you away from them. It's the way of the one. It's the

26
00:03:21,040 --> 00:03:26,160
way that was taught only by the Buddha, for example. There are many meanings of the word

27
00:03:26,160 --> 00:03:34,000
oh, only way. So the five things that the Buddha said this leads to, this path leads

28
00:03:34,000 --> 00:03:45,640
to are number one. It leads to purity. Number two, it leads to a mental health. Number

29
00:03:45,640 --> 00:03:55,800
three, it leads to freedom from suffering. Number four, it leads to, it leads you to the right

30
00:03:55,800 --> 00:04:04,360
way of living, the right way or the right path. And number five, it leads you to the goal,

31
00:04:04,360 --> 00:04:15,000
which is nibhana or release. So I'd just like to take a little bit of time, very short

32
00:04:15,000 --> 00:04:21,640
time you're doing, explain what these five mean and then we'll jump right into the meditation.

33
00:04:21,640 --> 00:04:26,480
So the first one is that it leads to the purification of beings. And here's specifically

34
00:04:26,480 --> 00:04:32,880
the purification of our minds, because when you see things as they are, the theory goes

35
00:04:32,880 --> 00:04:37,240
and it's not really theory. It's actually practice and what you can see from yourself through

36
00:04:37,240 --> 00:04:42,040
the meditation practice is that once you come to see things as they are, you'll never

37
00:04:42,040 --> 00:04:50,120
become defiled in the mind. It really should make perfect sense to us. When you see things

38
00:04:50,120 --> 00:04:57,560
as they are, you'll never do something that would cause you suffering. If you say, I don't

39
00:04:57,560 --> 00:05:02,680
want to suffer, then the only reason why you could do something that leads you to suffers

40
00:05:02,680 --> 00:05:06,920
because you don't understand what you're doing. You don't understand the nature of the

41
00:05:06,920 --> 00:05:16,040
act, the nature of the experience, and you aren't clearly aware and interacting with the

42
00:05:16,040 --> 00:05:23,800
experience in a way that leads to your own happiness and well-being. So as we look and explore

43
00:05:23,800 --> 00:05:35,240
and come to understand our moment-to-moment interactions with reality, our minds will naturally

44
00:05:35,240 --> 00:05:40,200
give rise only to those states of mind that lead to our happiness because we will realize

45
00:05:40,200 --> 00:05:54,200
that the states that lead to unhappiness are of that type, that the states which defile the

46
00:05:54,200 --> 00:05:59,720
mind lead us to unhappiness. We'll realize this is not a good way to behave.

47
00:06:02,600 --> 00:06:06,760
This is really the most important point because once your mind is pure, then all the other

48
00:06:06,760 --> 00:06:12,760
benefits come. But to understand what benefits come from purifying the mind, we have these

49
00:06:12,760 --> 00:06:19,320
other four results. The second one is that you have great mental health. You don't have mental

50
00:06:19,320 --> 00:06:26,760
sicknesses like depression or anxiety. And I use the word mental sickness loosely. It just

51
00:06:26,760 --> 00:06:37,480
means anything, any kind of discomfort in the mind. If you feel a person who was stressed or

52
00:06:37,480 --> 00:06:45,480
worried or depressed or a person who is mourning the loss of a loved one or some sort of loss,

53
00:06:45,480 --> 00:06:57,000
a person who is angry by nature or addictions, people who are addicted to this or addicted to

54
00:06:57,000 --> 00:07:04,600
anything, it allows you to abrute these because your mind is not giving rise to the unwholesome

55
00:07:04,600 --> 00:07:13,160
minds states that develop into habits. So it's also able to root out all of our bad habits of

56
00:07:13,160 --> 00:07:19,720
mind. Sometimes ones we think are a part of who we are and are incurable. People who are depressed,

57
00:07:19,720 --> 00:07:24,680
think it's a part of who they are, people who are stressed, think it's a part of who they are.

58
00:07:24,680 --> 00:07:29,560
But when you meditate, when you look carefully, you see that actually you're perpetuating it.

59
00:07:30,360 --> 00:07:35,800
And all you have to do is to give up and change your way of looking at things, come to see things

60
00:07:35,800 --> 00:07:43,880
objectively and then every experience will be a creation of wholesome tendency and will counteract

61
00:07:43,880 --> 00:07:51,160
and eventually overcome the bad habits. The third benefit will help us to overcome suffering.

62
00:07:52,200 --> 00:07:58,760
So this doesn't necessarily mean we will have no suffering because until we reach

63
00:07:58,760 --> 00:08:05,400
nibana, we will always have some kind of suffering. But the Buddha is suffering up into two parts,

64
00:08:05,400 --> 00:08:12,360
physical suffering and mental suffering. So we get rid of these one by one. The first one actually we

65
00:08:12,360 --> 00:08:18,280
get rid of is the mental suffering because physical suffering is something that as long as you

66
00:08:18,280 --> 00:08:24,120
have a body you have to deal with. But we have to understand that this doesn't mean we have

67
00:08:24,120 --> 00:08:32,280
to suffer in the mind. A person can be in pain and still actually be quite at peace with themselves.

68
00:08:32,280 --> 00:08:35,720
They may not be happy, but they won't be upset either.

69
00:08:38,920 --> 00:08:43,400
In fact, it's possible for a person to be quite happy even when the body is in physical pain.

70
00:08:43,400 --> 00:08:56,280
This is because of our understanding of the experiences being simply a feeling. I'm seeing it

71
00:08:56,280 --> 00:09:04,200
simply for what it is and not giving rise to any partiality because normally when we experience

72
00:09:04,200 --> 00:09:10,280
things we will judge it. When the reason why we don't, why we are uncomfortable when pain arises

73
00:09:10,280 --> 00:09:18,680
is because we're partial against it. We have this partiality in our mind. We don't like it.

74
00:09:19,800 --> 00:09:26,040
Now if we're able to give up that disliking, then we'll never have to worry about pain again.

75
00:09:26,040 --> 00:09:28,600
This is hopefully what you're going to see when we start to practice.

76
00:09:30,840 --> 00:09:37,800
The fourth benefit is that it helps you to attain the right path. Again, we come across

77
00:09:37,800 --> 00:09:42,920
this idea of the right path, but what this means is the right way of living your life,

78
00:09:42,920 --> 00:09:47,160
whatever that may be, whatever your life may be. It doesn't mean that you have to become a monk

79
00:09:47,160 --> 00:09:53,480
or leave your home or your family, but it means that when you live your life,

80
00:09:53,480 --> 00:09:58,760
you will start to do things and respond and react and interact with the world in a much more

81
00:09:58,760 --> 00:10:04,120
wholesome way. You'll find yourself doing the right thing and saying the right thing and helping

82
00:10:04,120 --> 00:10:12,520
other people much more than before you find yourself less selfish. You'll find yourself suffering

83
00:10:12,520 --> 00:10:17,400
a lot less and living a life that is to your own benefit and for the benefit of others around you,

84
00:10:17,400 --> 00:10:22,760
simply because you see things clearly. Of course, wisdom is a wonderful thing. It helps you in all

85
00:10:22,760 --> 00:10:29,960
situations and it problems that arise you're able to see clearly what is the way to resolve them

86
00:10:29,960 --> 00:10:38,280
and so on. The fifth benefit is that it allows us to find true freedom of mind. It leads us to a

87
00:10:38,280 --> 00:10:43,560
state where there are no defilements, where there is no suffering. Ultimately, it leads us to

88
00:10:43,560 --> 00:10:51,240
total freedom from anything, any arisen suffering, which we call nibana.

89
00:10:53,400 --> 00:10:58,600
These are the benefits of what we're going to do. What is this path?

90
00:10:58,600 --> 00:11:03,880
This is the selling. No, here we have the marketing. Buddha was a very good marketer in the

91
00:11:03,880 --> 00:11:09,080
Suta. He got everyone's attention by saying how great this would be. At first, it was the only path

92
00:11:09,080 --> 00:11:12,840
and then it leads to these great things. But then the question is, well, what is it?

93
00:11:14,360 --> 00:11:19,320
So many of you might already know this is the four foundations of mindfulness and before we

94
00:11:19,320 --> 00:11:23,720
continue, I'd like to have everyone remember these four. If you can remember them with me,

95
00:11:23,720 --> 00:11:30,280
I'd like to have everyone if possible recite these out loud. So ask you all to repeat after me.

96
00:11:31,640 --> 00:11:42,920
I'll say them one by one and then you repeat them. Body, feelings, mind,

97
00:11:42,920 --> 00:12:05,240
mind, dhamma, body, feelings, mind, dhamma, body, feelings, mind,

98
00:12:05,240 --> 00:12:27,160
mind, dhamma. Now, can you say it? Again, one more time.

99
00:12:27,160 --> 00:12:41,320
Okay, so this is the way we teach in Buddhist culture. And I think it's a good way to teach

100
00:12:41,320 --> 00:12:45,800
because it may seem kind of silly to just be repeating words, but now you have them in your mind.

101
00:12:46,520 --> 00:12:49,720
Now you can always remember back what are the four foundations of mindfulness?

102
00:12:50,760 --> 00:12:54,600
And really, my teacher would always say this is a very important.

103
00:12:54,600 --> 00:13:01,000
It's not something we should take lightly because here we have really the aca and the maga.

104
00:13:01,000 --> 00:13:08,760
We have memorized it now. The core of the Buddha's path, the path that the Buddha taught.

105
00:13:08,760 --> 00:13:12,040
We haven't memorized quite easily actually. You don't have to go memorizing the whole

106
00:13:12,040 --> 00:13:17,560
tepitika. So I do try to keep these in mind. Now I've left one untranslated and I'll explain

107
00:13:17,560 --> 00:13:25,400
what that is. The body refers to our entire physical form from the bottoms of our feet to the top

108
00:13:25,400 --> 00:13:33,640
of our head. This is body. Our awareness of it is called mindfulness of the body. So when we walk

109
00:13:33,640 --> 00:13:40,760
being aware of the sensation of the body moving, when we sit being aware of the pressure against

110
00:13:40,760 --> 00:13:47,480
the floor and one leg touching the other and our hands touching our legs, the stiffness in the back,

111
00:13:49,400 --> 00:13:55,000
the stiffness in the stomach when it rises and when it falls and so on.

112
00:13:56,920 --> 00:14:00,840
The feeling of our tongue against the roof of our mouth, all of these things are physical

113
00:14:01,560 --> 00:14:07,240
experiences. And this is what we're going to use to, this is the first thing we're going to

114
00:14:07,240 --> 00:14:12,600
use to learn about who we are and what is reality. Because of course the physical is a very important

115
00:14:12,600 --> 00:14:22,520
part of reality. The feelings number two refers to basic feelings, not talking about emotions yet,

116
00:14:24,120 --> 00:14:30,760
but talking about our impression or our experience of the physical. Maybe it's pleasure,

117
00:14:30,760 --> 00:14:37,000
maybe it's pain, or maybe it's just a neutral sensation. So these are the three types of feeling.

118
00:14:37,000 --> 00:14:42,840
We have happy feelings, unhappy feelings in neutral or pleasant, unpleasant and neutral feelings.

119
00:14:45,080 --> 00:14:49,080
Sometimes when you're sitting, you'll feel pain in the back. Sometimes you'll be sitting in

120
00:14:49,080 --> 00:14:55,160
you feel pleasure or happiness. Sometimes you'll be sitting and you'll feel calm. So we'll come to

121
00:14:55,160 --> 00:15:01,240
see how these change and we'll come to learn to view them objectively and not become attached to

122
00:15:01,240 --> 00:15:07,560
the happy feelings or upset about the unpleasant ones. As we'll see that they're not under our control

123
00:15:09,320 --> 00:15:13,800
and it's useless to become upset about them. It's useless to become attached to them as well.

124
00:15:15,400 --> 00:15:21,880
The third is the mind. The mind is that which thinks sometimes we're focused, sometimes we're

125
00:15:21,880 --> 00:15:26,680
unfocused. The mind is all these thoughts that are swirling around inside of us.

126
00:15:26,680 --> 00:15:32,200
We'll be sitting here listening to me, but suddenly your mind wanders, thinking about your home,

127
00:15:32,200 --> 00:15:36,600
thinking about your family, and so on, thinking about work.

128
00:15:39,400 --> 00:15:46,360
Number four is the dhamma. In your dhamma, we're going to make it easy and just say it's the

129
00:15:46,360 --> 00:15:53,480
teaching of the Buddha. But the dhamma here, the Buddha said, okay, so once you focus on these other

130
00:15:53,480 --> 00:15:59,080
three, then you start to see the dhamma. You start to see the truth. You start to see the Buddha's

131
00:15:59,080 --> 00:16:04,360
teaching. The first thing that you'll see unfortunately is what the Buddha taught our bad things.

132
00:16:05,240 --> 00:16:12,360
Because our mind generally is not very well trained. So we have a lot of bad things. And these

133
00:16:12,360 --> 00:16:18,440
are dhammas. The Buddha said, this is a teaching that the Buddha gave on those things which are

134
00:16:18,440 --> 00:16:24,760
bad for us. Bad for us means just in an objective way. They're not immoral, or they may be. But the

135
00:16:24,760 --> 00:16:30,120
point is not that they are immoral. The point is that they cause harm to our mind. They cause our

136
00:16:30,120 --> 00:16:39,720
minds to be unfocused, to be unsteady, and unclear, unhappy as well. So these we have to remember,

137
00:16:39,720 --> 00:16:45,480
this is under the teaching of the Buddha, the dhamma. The first group is called the niwara. These are

138
00:16:45,480 --> 00:16:52,120
the bad things. The hindrances. Good name for them is the hindrances. And so you have to remember

139
00:16:52,120 --> 00:16:56,840
these as well. If I can ask everyone to repeat after me with these, because these are also very

140
00:16:56,840 --> 00:17:01,240
important to keep in mind when you meditate. So I'll say them one by one and then you repeat after

141
00:17:01,240 --> 00:17:06,200
me. I'm going to give them very simple names. For those of you who already know them, you might know

142
00:17:06,200 --> 00:17:27,480
them by more difficult names. Liking, disliking, drowsiness, distraction, doubt. Liking, disliking,

143
00:17:27,480 --> 00:17:42,200
drowsiness, distraction, doubt. Okay, so these are the five hindrances. You have to keep these in

144
00:17:42,200 --> 00:17:50,280
mind. You're liking something, disliking, drowsiness, distraction, doubt. This is what we mean by

145
00:17:50,280 --> 00:17:55,640
the four foundations of mindfulness. This is going to be our path and our practice during the time

146
00:17:55,640 --> 00:18:00,360
that we meditate. So now I've gone on even a little overtime. I'd like to jump right into the

147
00:18:00,360 --> 00:18:05,960
meditation. Ask everyone to begin to put these into practice and I'll lead you through it.

148
00:18:07,800 --> 00:18:13,160
So please get comfortable in a meditation position and let's see how are we going to learn

149
00:18:13,160 --> 00:18:29,240
to put these into practice. I'll go through them one by one. First of all, the body.

150
00:18:31,720 --> 00:18:37,320
Now remember, this is all how we experience things. So it's not thinking about our body and our shape

151
00:18:37,320 --> 00:18:45,160
and how we look from the outside or something. We're talking in terms of our experience of the body.

152
00:18:46,840 --> 00:18:50,440
And when you're sitting still, your experience of the body is only a few different things.

153
00:18:52,920 --> 00:18:57,960
You may at certain times become aware of the position that you're sitting in. As I said,

154
00:18:57,960 --> 00:19:02,040
the pressure in the back, the pressure against the floor and so on.

155
00:19:02,040 --> 00:19:10,760
But what is most likely to be obvious and what was always recommended by the Buddha

156
00:19:11,400 --> 00:19:16,920
and by many meditation teachers was the breath. The breath is always there.

157
00:19:17,720 --> 00:19:19,480
Even when you're asleep, you're still breathing.

158
00:19:21,960 --> 00:19:25,000
And so we can become aware of the sensation that the breath

159
00:19:25,000 --> 00:19:32,840
makes when it enters the body. You can be aware at the tip of the nose. There's a sensation.

160
00:19:33,640 --> 00:19:35,960
You can be aware in the chest. There's a sensation.

161
00:19:37,720 --> 00:19:45,640
In this tradition, we focus on the breath as it changes the feeling in this stomach.

162
00:19:45,640 --> 00:19:48,760
It creates a sensation in our abdomen.

163
00:19:48,760 --> 00:19:56,840
So the abdomen rises. This is the mindfulness of the breath to be aware of the breath

164
00:19:58,040 --> 00:20:03,880
in the abdomen. When it rises, we'll say to ourselves, rising.

165
00:20:05,400 --> 00:20:11,720
And when it falls, we'll say to ourselves, falling, rising.

166
00:20:11,720 --> 00:20:17,560
We're just going to use this word. The word is a mantra.

167
00:20:19,800 --> 00:20:25,480
And the mantra here is not some mystical or magical thing, but this mantra is focused on

168
00:20:25,480 --> 00:20:33,240
just the name or the recognition of what is going on. What is this that we're experiencing?

169
00:20:35,400 --> 00:20:39,560
When you experience the stomach rising, you recognize that's rising.

170
00:20:39,560 --> 00:20:46,200
What we're trying to do is create an objective state of mind because by just watching it,

171
00:20:46,200 --> 00:20:51,640
our mind is full of all sorts of subjective states, liking it or disliking it,

172
00:20:53,240 --> 00:21:01,400
judging it or thinking about it. But when we remind ourselves what's really going on,

173
00:21:01,400 --> 00:21:07,880
we're able to see it as just that. Instead of saying, this is good, this is bad, we say,

174
00:21:07,880 --> 00:21:16,680
this is rising. And we close the loop, so there's no room for the extrapolation for us to make

175
00:21:16,680 --> 00:21:23,640
more out of it than it actually is. When the stomach falls, we say ourselves, this is falling,

176
00:21:25,400 --> 00:21:35,560
rising, falling. This is about the simplest meditation technique one could ever give.

177
00:21:35,560 --> 00:21:41,400
So I'd like to have everyone try this now and just say it silently, this isn't out loud.

178
00:21:42,360 --> 00:21:46,920
Now you're just saying the words in your stomach actually, your mind is in your stomach and that's

179
00:21:46,920 --> 00:21:54,040
where you're repeating this. When it rises, you recognize rising. When it falls, you recognize

180
00:21:54,040 --> 00:22:09,960
falling. Remind yourself, this is rising, this is falling.

181
00:23:24,520 --> 00:23:38,920
And the first thing you should realize as you start to do this is there's a lot more than just

182
00:23:38,920 --> 00:23:47,080
the rising going on. The body may be fairly still, but the mind most likely isn't. And so you

183
00:23:47,080 --> 00:23:52,760
can hopefully appreciate why there is not just the rising as the one object, because sometimes there

184
00:23:52,760 --> 00:23:58,760
are other things that take our awareness away. And either we can push them away or we can

185
00:23:58,760 --> 00:24:04,040
accept them and include them in our understanding of reality. And this is what the Buddha taught,

186
00:24:04,040 --> 00:24:10,600
this is why he taught four foundations of mindfulness. So the second one, the feelings,

187
00:24:11,400 --> 00:24:19,400
if and when you feel happy or calm or in pain, mostly in the beginning people feel a lot of pain,

188
00:24:19,400 --> 00:24:29,080
if and when you feel pain or aching or so on, you have to remind ourselves that it's just a sensation

189
00:24:30,760 --> 00:24:38,440
to change the way we look at it. So that this disliking is really not necessary, our aversion to the

190
00:24:38,440 --> 00:24:47,160
experience. It's very difficult to understand. And only when you repeatedly put this into practice,

191
00:24:47,160 --> 00:24:50,840
that you can finally come to see that actually that there's nothing wrong with the pain at all.

192
00:24:52,840 --> 00:24:58,840
With the method that we use is to remind ourselves, I'm not saying this is bad or this is a problem

193
00:24:58,840 --> 00:25:05,320
or I have to this, I have to that. I just remind ourselves this is pain. So we say to ourselves,

194
00:25:05,320 --> 00:25:17,160
pain, pain, pain, pain, pain, pain reminding ourselves again and again, hey, this is just pain. And

195
00:25:17,160 --> 00:25:25,560
the mind will slowly begin to accept. But that's what this is, this is pain. We're aching aching or so.

196
00:25:26,440 --> 00:25:29,880
Actually keep the mind with this sensation, keep it there until it goes away.

197
00:25:29,880 --> 00:25:35,080
The breath isn't special. It's not that we have to stay with the breath as much as possible.

198
00:25:35,080 --> 00:25:41,640
When there's something else, focus on that. When there's nothing else, come back again to the breath.

199
00:25:42,600 --> 00:25:49,000
If you feel happy, focus on the happiness. Because on the other hand, it's very easy to get

200
00:25:49,000 --> 00:25:58,040
attached to happiness or to calm. When you're attached to it, well, then eventually it goes away

201
00:25:58,040 --> 00:26:02,600
and you're looking for it again. Eventually you can only sit when you feel this peace and

202
00:26:02,600 --> 00:26:07,720
happiness and you become very disappointed when your meditation doesn't bring you these

203
00:26:07,720 --> 00:26:15,880
states. So it's an addiction. It's a partiality. It's not being able to accept all of reality.

204
00:26:17,560 --> 00:26:23,560
So when we look at it and say happy, happy or calm, calm, we come to let it go as well and realize

205
00:26:23,560 --> 00:26:29,880
that as long as our happiness and calm is dependent on certain states,

206
00:26:32,280 --> 00:26:34,440
we'll never truly be happy or at peace.

207
00:26:42,040 --> 00:26:45,720
Whereas long as you have these feelings, you can focus on them and when they're gone,

208
00:26:45,720 --> 00:26:54,440
come back again to the rising and the falling.

209
00:27:03,800 --> 00:27:08,440
If the stomach isn't clear, if the rising motion of the stomach isn't clear, it's only because you

210
00:27:08,440 --> 00:27:16,360
haven't done this before. You can put your hand there, put a hand on your stomach.

211
00:27:19,480 --> 00:27:22,440
When you do this at home, you can try lying on your back in the beginning.

212
00:27:23,400 --> 00:27:26,680
Well, help you get a sense of the motion of the rising and the falling.

213
00:27:32,520 --> 00:27:37,000
You're not trying to force or control the breath. We're just trying to be aware of it.

214
00:27:37,000 --> 00:27:39,000
However, it occurs.

215
00:28:07,000 --> 00:28:33,000
If you want to take a look at it, you can do it as long as you can do it as long as you can do it as long as you can do it as long as you can do it.

216
00:28:37,000 --> 00:29:00,120
This is the second in regards to the feelings. The third foundation is the mind and you'll

217
00:29:00,120 --> 00:29:10,760
find that actually this is in some cases even more difficult to contain, more difficult to deal with,

218
00:29:11,560 --> 00:29:14,280
more of an issue than the pain in some cases.

219
00:29:17,240 --> 00:29:22,680
In the beginning, it can be difficult to catch the mind. You shouldn't feel discouraged by this.

220
00:29:22,680 --> 00:29:29,400
If you continue to practice and do it again and again and mostly rely on patience,

221
00:29:30,600 --> 00:29:34,200
you find that eventually you're able to come to terms with the workings of the mind,

222
00:29:35,240 --> 00:29:40,760
and you're able to be aware of the thoughts as they arise. Sometimes you can even catch them when they first arise.

223
00:29:40,760 --> 00:29:53,720
So again, we're not trying to stop the mind from thinking just as we're not trying to stop the

224
00:29:53,720 --> 00:29:59,080
body from breathing. It's the nature of the mind to think just as it's the nature of the body to breathe.

225
00:30:00,440 --> 00:30:04,760
So when there is thinking, just be aware of the fact that you're thinking and try to keep it at

226
00:30:04,760 --> 00:30:13,480
just thinking instead of judging or wanting it to be some other way or so on.

227
00:30:17,560 --> 00:30:24,120
Remind yourself this is thinking until you're self-thinking thinking.

228
00:30:26,760 --> 00:30:30,520
In a way, this one is easy because it doesn't last. As soon as you say thinking,

229
00:30:30,520 --> 00:30:36,360
the old thinking is gone right away. Sometimes in fact, you can't even say thinking. You just know that

230
00:30:36,360 --> 00:30:41,640
it's gone. You know that you're worth thinking. In which case, you can become aware of the state of

231
00:30:41,640 --> 00:30:48,520
knowing, say to yourself, knowing, and then come back again to the rising and falling. This will allow

232
00:30:48,520 --> 00:30:53,480
you to keep it just at thinking so that whenever you think it's only thinking,

233
00:30:53,480 --> 00:31:03,480
just as the Buddha said, when you think, this is only thinking.

234
00:31:54,360 --> 00:32:01,400
And the fourth is, as I said, the Dhamma, the teachings of the Buddha.

235
00:32:03,000 --> 00:32:07,960
So eventually it will become necessary for you to learn all about many of the teachings of the

236
00:32:07,960 --> 00:32:15,160
Buddha. But we should really go in order when we'll study the Buddha's teaching. We shouldn't just

237
00:32:15,160 --> 00:32:21,960
dive in and start reading many different things. Not unless we've done some pretty serious meditation

238
00:32:21,960 --> 00:32:29,160
already. When you begin to start meditating, you should focus only on those teachings that are

239
00:32:29,960 --> 00:32:37,160
appropriate for you at that stage. And this is best given in the form of one-on-one teachings

240
00:32:37,160 --> 00:32:44,200
from a teaching. But in general, we need the basics or first-the-four foundations of mindfulness.

241
00:32:44,200 --> 00:32:52,920
This is actually a Dhamma of sorts. But the second thing that we need is, as I said, the hindrances.

242
00:32:53,640 --> 00:32:57,400
And eventually there will be more and more. And if you read the sutta, you can see what are the

243
00:32:57,400 --> 00:33:05,240
other things that are most important. But the new one and other are very important in the beginning.

244
00:33:05,240 --> 00:33:09,880
And hopefully you can see that a little bit right now that actually many of these may arise,

245
00:33:09,880 --> 00:33:21,240
liking, or wanting, disliking, or frustration, or upset or sadness,

246
00:33:24,840 --> 00:33:35,800
the drowsiness or tired fatigue in the mind or body. Distraction where your mind is not focused

247
00:33:35,800 --> 00:33:43,320
and doubt where you're not sure you doubt yourself or you doubt the practice or solid.

248
00:33:49,240 --> 00:33:52,200
So when these come up, it's important that we're mindful and aware of them,

249
00:33:54,040 --> 00:33:57,880
so that we can come to see through them and realize that they're only a misunderstanding.

250
00:33:57,880 --> 00:34:05,160
They're based on improper attention, misunderstanding of the way things are.

251
00:34:06,600 --> 00:34:11,320
Once we see the way things are, in regards to these emotions or mind states,

252
00:34:12,200 --> 00:34:13,320
we'll be able to give them up.

253
00:34:17,880 --> 00:34:22,360
So when there's liking, just say to yourself, liking, liking, and just look at it and see

254
00:34:22,360 --> 00:34:28,360
it for what it is. This is liking. If you have disliking, say disliking, disliking, disliking.

255
00:34:29,880 --> 00:34:34,520
If you feel drowsy or tired, say drowsy, drowsy or tired, tired.

256
00:34:38,360 --> 00:34:42,200
If you feel distracted, say to yourself, distracted, distracted,

257
00:34:42,200 --> 00:34:54,760
worried, worried. If you have doubt or confusion, say doubting or confused, confused or so on.

258
00:34:59,640 --> 00:35:03,080
We're not judging these, we're trying to give up the judging involved with them.

259
00:35:03,960 --> 00:35:07,480
So when you see them, just see them for what they are. This is this, this is that.

260
00:35:07,480 --> 00:35:13,480
It doesn't make me an evil person because I have these inside, but there's no good that comes

261
00:35:13,480 --> 00:35:16,680
from these. And if you look closely, you'll be able to see that.

262
00:35:20,280 --> 00:35:24,360
Once they're gone, come back again to the rising and the falling of the stomach as well.

263
00:35:27,640 --> 00:35:32,040
So now we'll go for another 10 minutes and I won't do any talking. We can try to put these

264
00:35:32,040 --> 00:35:42,040
four into practice intensively for the next 10 minutes.

265
00:40:02,040 --> 00:40:28,040
And then we'll go for another 10 minutes and then we'll go for another 10 minutes and then we'll go for another 10 minutes.

266
00:40:32,040 --> 00:40:58,040
And then we'll go for another 10 minutes and then we'll go for another 10 minutes and then we'll go for another 10 minutes.

267
00:41:02,040 --> 00:41:06,040
And then we'll go for another 10 minutes.

268
00:41:32,040 --> 00:41:36,040
We'll go for another 10 minutes.

269
00:42:02,040 --> 00:42:22,040
We'll go for another 10 minutes and then we'll go for another 10 minutes.

270
00:42:22,040 --> 00:42:48,040
And then we'll go for another 10 minutes and then we'll go for another 10 minutes.

271
00:42:48,040 --> 00:43:14,040
And then we'll go for another 10 minutes and then we'll go for another 10 minutes and then we'll go for another 10 minutes.

272
00:43:14,040 --> 00:43:40,040
And then we'll go for another 10 minutes and then we'll go for another 10 minutes and then we'll go for another 10 minutes.

273
00:43:40,040 --> 00:43:43,040
.

274
00:43:43,040 --> 00:43:48,040
..

275
00:43:48,040 --> 00:43:51,040
..

276
00:43:51,040 --> 00:43:54,040
..

277
00:43:54,040 --> 00:43:57,040
..

278
00:43:57,040 --> 00:44:00,040
...

279
00:44:00,040 --> 00:44:03,040
...

280
00:44:03,040 --> 00:44:06,040
...

281
00:44:06,040 --> 00:44:09,040
..

282
00:44:09,040 --> 00:44:12,040
..

283
00:44:12,040 --> 00:44:15,040
..

284
00:44:15,040 --> 00:44:18,040
..

285
00:44:18,040 --> 00:44:21,040
..

286
00:44:21,040 --> 00:44:24,040
..

287
00:44:24,040 --> 00:44:27,040
..

288
00:44:27,040 --> 00:44:30,040
..

289
00:44:30,040 --> 00:44:33,040
..

290
00:44:33,040 --> 00:44:36,040
..

291
00:44:36,040 --> 00:44:37,040
..

292
00:44:37,040 --> 00:44:38,040
..

293
00:44:38,040 --> 00:44:43,040
..

294
00:44:43,040 --> 00:44:46,040
..

295
00:44:46,040 --> 00:44:49,040
..

296
00:44:49,040 --> 00:44:52,040
..

297
00:44:52,040 --> 00:44:55,040
..

298
00:44:55,040 --> 00:44:58,040
..

299
00:44:58,040 --> 00:45:03,040
..

300
00:45:03,040 --> 00:45:06,040
..

301
00:45:06,040 --> 00:45:07,040
..

302
00:45:07,040 --> 00:45:12,040
..

303
00:45:12,040 --> 00:45:17,040
..

304
00:45:17,040 --> 00:45:22,040
..

305
00:45:22,040 --> 00:45:25,040
..

306
00:45:25,040 --> 00:45:32,040
..

307
00:45:32,040 --> 00:45:35,040
..

308
00:45:35,040 --> 00:45:36,040
..

309
00:45:36,040 --> 00:45:41,040
..

310
00:45:41,040 --> 00:45:46,040
..

311
00:45:46,040 --> 00:45:51,040
..

312
00:45:51,040 --> 00:45:56,040
..

313
00:45:56,040 --> 00:46:03,040
..

314
00:46:03,040 --> 00:46:04,040
..

315
00:46:04,040 --> 00:46:10,040
..

316
00:46:10,040 --> 00:46:16,040
..

317
00:46:24,040 --> 00:46:24,040
..

318
00:46:24,040 --> 00:46:24,040
..

319
00:46:24,040 --> 00:46:24,040
..

320
00:46:24,040 --> 00:46:24,060
..

321
00:46:24,040 --> 00:46:25,040
..

322
00:46:25,040 --> 00:46:26,040
..

323
00:46:26,040 --> 00:46:26,040
..

324
00:46:26,040 --> 00:46:26,040
..

325
00:46:26,040 --> 00:46:27,040
..

326
00:46:30,040 --> 00:46:30,540
..

327
00:46:30,540 --> 00:46:31,540
..

328
00:46:31,540 --> 00:46:32,540
..

329
00:46:32,540 --> 00:46:33,540
..

330
00:46:33,540 --> 00:46:33,540
...

331
00:46:33,540 --> 00:46:36,540
Send our good thoughts to other beings,

332
00:46:36,540 --> 00:46:39,540
send me a task and loving kindness,

333
00:46:39,540 --> 00:46:44,540
and make any wishes or aspirations that we might like to make.

334
00:46:44,540 --> 00:46:46,540
Just for a moment we can take that,

335
00:46:46,540 --> 00:46:50,540
the time to do that, and then finish off.

