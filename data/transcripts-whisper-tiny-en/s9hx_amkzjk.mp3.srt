1
00:00:00,000 --> 00:00:16,520
So, all together that is the basic teaching in the past and the meditation.

2
00:00:16,520 --> 00:00:23,600
The other things which we have to look for, once we start practicing, now it's important

3
00:00:23,600 --> 00:00:31,200
that by this time you've actually done a few minutes of sitting meditation and are considering

4
00:00:31,200 --> 00:00:37,440
the idea of doing 5, 10, 15, 20 minutes a day of this.

5
00:00:37,440 --> 00:00:41,360
People who have already started practicing but haven't had any guidance, tried to do

6
00:00:41,360 --> 00:00:46,160
at least 15, 20 minutes at a time sitting meditation.

7
00:00:46,160 --> 00:00:53,600
When these are for people who have practiced before, people who haven't practiced

8
00:00:53,600 --> 00:00:58,000
start with 5 minutes when you feel comfortable trying to move up to 10 minutes.

9
00:00:58,000 --> 00:01:03,320
But before we get that far, I'd like to teach a little bit about walking meditation because

10
00:01:03,320 --> 00:01:05,920
it's very important to do walking meditation as well.

11
00:01:05,920 --> 00:01:10,400
I'll talk about that in the next series, in the next episode in this series.

12
00:01:10,400 --> 00:01:15,800
In this episode, I'd like to give a few pointers to the second part, to the second

13
00:01:15,800 --> 00:01:22,800
part of the second episode, and I'd like to give a few pointers now, once you've started

14
00:01:22,800 --> 00:01:27,560
to practice before foundation to mindfulness, how is it that you actually say mindful?

15
00:01:27,560 --> 00:01:33,560
We can see that mindfulness has something to do with using a word, a mindfulness that

16
00:01:33,560 --> 00:01:38,600
a word that means to remember or to remind yourself, so we use the word to remind ourselves

17
00:01:38,600 --> 00:01:44,840
of the bare reality of the phenomenon, so when we feel pain it's only pain when we feel

18
00:01:44,840 --> 00:01:49,680
happy it's only a feeling, it's only a happy feeling, and we don't identify with it as

19
00:01:49,680 --> 00:01:54,520
me or mine, it's only happy, it's only pain, and we can see this as a profound effect

20
00:01:54,520 --> 00:01:58,440
on how we look at the things around us.

21
00:01:58,440 --> 00:02:03,400
But there are four important points that we have to keep in mind when we're practicing

22
00:02:03,400 --> 00:02:07,560
the four foundations of mindfulness, there's probably a lot of points, but I've come up

23
00:02:07,560 --> 00:02:11,080
with four, based on what Mikey sure taught me.

24
00:02:11,080 --> 00:02:16,040
The first one is that we have to stay in the present moment.

25
00:02:16,040 --> 00:02:21,160
We can not remind the things which have happened in the past or things which are going

26
00:02:21,160 --> 00:02:26,920
to happen in the future, this is not reality, those things have either disappeared already

27
00:02:26,920 --> 00:02:30,400
or haven't come yet in either case, they don't exist.

28
00:02:30,400 --> 00:02:36,200
So when we say rising, rising or falling, we have to say it as the belly is actually rising

29
00:02:36,200 --> 00:02:43,000
when it's rising, rising at the same time as falling at the same time, so when we feel

30
00:02:43,000 --> 00:02:47,000
pain it has to be the moment when there's pain and so on, it has to be done in the

31
00:02:47,000 --> 00:02:48,000
present moment.

32
00:02:48,000 --> 00:02:56,000
Number one, number two, it has to be carried out continuously, this will be more important

33
00:02:56,000 --> 00:03:00,440
when we do walking, we have to do walking and sitting together, but continuously means

34
00:03:00,440 --> 00:03:05,440
we also have to incorporate it into our lives if we really expect to have any kind of success

35
00:03:05,440 --> 00:03:09,400
and this is great for people living in the world, because you meet with all sorts of

36
00:03:09,400 --> 00:03:14,800
stressful and upsetting unpleasant situations and that we have to deal with, so when you

37
00:03:14,800 --> 00:03:20,320
feel the five hindrance of liking, disliking, drowsiness, distraction or doubt come up in your

38
00:03:20,320 --> 00:03:24,280
daily life, it's most important that your mindful of some bend as well, when you feel

39
00:03:24,280 --> 00:03:29,120
pain or aching is important that you take a moment, even headaches, you take a moment

40
00:03:29,120 --> 00:03:35,240
to be mindful of the headache to get your mind centered and calm again, so that even

41
00:03:35,240 --> 00:03:41,760
though the pain and the aching is still there, you're not upset by this number two, it

42
00:03:41,760 --> 00:03:46,280
has to be carried out continuously, so try to incorporate the four foundations of mindfulness

43
00:03:46,280 --> 00:03:48,600
into your daily life.

44
00:03:48,600 --> 00:03:55,760
Number three, mindfulness has to be accompanied by three factors, one you need effort, by

45
00:03:55,760 --> 00:03:59,080
the time when you say rising, falling, it's not just a magical word that you say in your

46
00:03:59,080 --> 00:04:05,200
mind and both something happens, by the time when you say rising, you have to send

47
00:04:05,200 --> 00:04:09,640
your mind out again and again and again, whatever arises we have to be mindful of it,

48
00:04:09,640 --> 00:04:13,920
when we feel pain, we have to send our mind out, don't be lazy, when we feel tired, don't

49
00:04:13,920 --> 00:04:22,160
just give up and go to sleep and so on, we need effort, number one, number two, we need

50
00:04:22,160 --> 00:04:27,600
awareness, we need to be aware, when we say rising, we can't just say it up in our mind,

51
00:04:27,600 --> 00:04:32,880
we have to go send the mind out to the stomach and say, in the stomach we say it's rising

52
00:04:32,880 --> 00:04:37,800
and in the stomach we say fall, when we have pain, we send the mind out, we're actually

53
00:04:37,800 --> 00:04:44,880
aware of the pain, this is called sampatanya, and number three, we need mindfulness, and

54
00:04:44,880 --> 00:04:52,280
mindfulness is not simply knowing the object, this is called sampatanya, seti, setima means

55
00:04:52,280 --> 00:04:57,280
to remind ourselves about the true nature of the object, this is only rising, this is

56
00:04:57,280 --> 00:05:02,840
only falling, this is only pain, this is only anger and greed and so on, they're not

57
00:05:02,840 --> 00:05:08,800
mean, they're not mine, they're not good, they're not bad, this is the third important

58
00:05:08,800 --> 00:05:17,520
thing, we need these three factors in our mindful meditation, number four, we need to balance

59
00:05:17,520 --> 00:05:21,920
the mental faculties, so I'm giving lots of lists here, all these things are available

60
00:05:21,920 --> 00:05:27,160
on one of my websites, I'll try to post links to the text versions of this, if you want

61
00:05:27,160 --> 00:05:34,160
to go read, four or five faculties are things that exist in the mind, there's confidence,

62
00:05:34,160 --> 00:05:39,720
effort, mindfulness, concentration, and wisdom, these are important, good things that exist

63
00:05:39,720 --> 00:05:45,400
in the mind, confidence and wisdom have to go together, if we have lots of wisdom, it

64
00:05:45,400 --> 00:05:50,520
leads to doubt, if we have lots of confidence, it leads to greed, until that wisdom and

65
00:05:50,520 --> 00:05:55,520
confidence are balanced, people have much faith but no wisdom, they get these things

66
00:05:55,520 --> 00:06:02,440
to agree, have greed, people have lots of wisdom but no confidence, it leads them to doubt.

67
00:06:02,440 --> 00:06:06,280
Effort and concentration have to balance together, people have a lot of effort but no

68
00:06:06,280 --> 00:06:12,320
concentration, it leads them to be distracted, people who have lots of concentration but

69
00:06:12,320 --> 00:06:18,720
no effort, it leads them to be lazy or drowsy, with the fall asleep in their meditation,

70
00:06:18,720 --> 00:06:25,720
so these have to be balanced together, and mindfulness is with balance, so when you feel

71
00:06:25,720 --> 00:06:29,240
drowsy, you have to say drowsy, when you feel distracted, you have to say distracted

72
00:06:29,240 --> 00:06:35,320
and you feel greedy, you have to say greed, when you feel doubt, you have to figure

73
00:06:35,320 --> 00:06:40,160
something, doubt thing, doubt thing, in this way the faculties become balanced, don't let

74
00:06:40,160 --> 00:06:46,160
one of them become unbalanced and this is a good way to figure out what is the problem

75
00:06:46,160 --> 00:06:50,360
if you come across problems in your meditation, you can say I have too much concentration

76
00:06:50,360 --> 00:06:56,000
on enough effort, then get up and be walking and stand and so on, I have lots of wisdom,

77
00:06:56,000 --> 00:07:01,160
I don't need to read anymore but I need confidence, so I have to practice to really learn

78
00:07:01,160 --> 00:07:07,160
it for myself and get confidence, these are the basics of meditation, and these are actually

79
00:07:07,160 --> 00:07:11,160
advanced points in the consideration for meditative, so once you start in practicing,

80
00:07:11,160 --> 00:07:16,160
you should keep these four points in mind during your practice, and now I'd like to do a

81
00:07:16,160 --> 00:07:21,160
guided sitting meditation, not really guided, we'll just do another five minutes, those

82
00:07:21,160 --> 00:07:26,160
your eyes watch the stomach rising and falling, only now incorporate all of these things

83
00:07:26,160 --> 00:07:31,160
I've said, at least the four foundations of mindfulness, feelings, mind, body, feelings,

84
00:07:31,160 --> 00:07:37,160
mind, and come all together, we'll do it, you can get pause and do it for five minutes,

85
00:07:37,160 --> 00:07:44,160
you can do it for as long as you like, you can pause and when you're ready, and pause

86
00:08:37,160 --> 00:09:01,160
and listen up, another important thing, another important point in meditation is once

87
00:09:01,160 --> 00:09:09,360
finished sitting practice that we extend, thought of gratitude towards our parents and our

88
00:09:09,360 --> 00:09:16,960
teachers, and thought the goodwill towards all those beings that we might have any sort

89
00:09:16,960 --> 00:09:25,760
of anger or a over-vanged towards, and that we wish well-being to all beings and the

90
00:09:25,760 --> 00:09:33,760
universe. This is optional, but considered a useful and important tool for any meditator

91
00:09:33,760 --> 00:09:39,160
after we finish meditation that we send some kind of good thoughts to all those to people

92
00:09:39,160 --> 00:09:44,760
who have helped us, who have heard us in the pop. Okay, please tune in next time for

93
00:09:44,760 --> 00:09:51,760
hopefully for instruction on walking meditation.

