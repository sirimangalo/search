1
00:00:00,000 --> 00:00:07,000
So we have a question from Emma about how your family is reacting to your ordination

2
00:00:07,000 --> 00:00:10,000
for semidry, I guess.

3
00:00:10,000 --> 00:00:21,000
I guess one of the first things that arises in my mind is why someone would ask this question

4
00:00:21,000 --> 00:00:29,000
is ordination perhaps something you're thinking of and maybe your family, maybe there's fear

5
00:00:29,000 --> 00:00:31,000
that your family would not approve.

6
00:00:31,000 --> 00:00:34,000
I think it's big for a lot of people.

7
00:00:34,000 --> 00:00:42,000
My family, my parents are Catholic and my dad actually said,

8
00:00:42,000 --> 00:00:45,000
oh it's good, you're going the Buddhist route for the nun thing,

9
00:00:45,000 --> 00:00:51,000
I used to, when I was 16 I decided I wanted to be a Catholic nun.

10
00:00:51,000 --> 00:00:56,000
And to be a Catholic nun you have to be a novice for I think five or seven years

11
00:00:56,000 --> 00:01:01,000
lifetime commitment and to break it is a big deal.

12
00:01:01,000 --> 00:01:06,000
But with the Buddhist nun my dad thinks, oh she can easily disrobe and it's no big deal

13
00:01:06,000 --> 00:01:08,000
and this is a temporary thing.

14
00:01:08,000 --> 00:01:11,000
My mom thinks this is a temporary thing.

15
00:01:11,000 --> 00:01:17,000
I'm in a phase and so they're trying to be supportive.

16
00:01:17,000 --> 00:01:22,000
My dad's actually coming out for the ordination, which is a big deal.

17
00:01:22,000 --> 00:01:27,000
But I think he wants to travel as well.

18
00:01:27,000 --> 00:01:30,000
They're trying to be supportive.

19
00:01:30,000 --> 00:01:35,000
They don't really understand Buddhism on an experiential level

20
00:01:35,000 --> 00:01:39,000
because at least in this lifetime they haven't given meditation a try

21
00:01:39,000 --> 00:01:44,000
but they're interested in learning about it and asking me questions

22
00:01:44,000 --> 00:01:50,000
and my brothers and sisters, I think for them it's more like being so far away from home.

23
00:01:50,000 --> 00:01:53,000
It's not so much what I'm doing.

24
00:01:53,000 --> 00:01:58,000
It's like my sister just gets really worried, which isn't here for me

25
00:01:58,000 --> 00:02:01,000
and my brother's fine.

26
00:02:01,000 --> 00:02:05,000
My other sister, it's just more like being away from the family

27
00:02:05,000 --> 00:02:11,000
and I guess when I actually ordained I'll be even a bit more off the wire

28
00:02:11,000 --> 00:02:18,000
for at least some time probably so it's more of like missing the missing factor

29
00:02:18,000 --> 00:02:24,000
but they're very supportive and which is a great, great blessing.

30
00:02:24,000 --> 00:02:27,000
Are you going to say something, Bante?

31
00:02:27,000 --> 00:02:31,000
I was just familiar.

32
00:02:31,000 --> 00:02:37,000
Well, I'm not in my twenties anymore so I've been talking about it for a long time

33
00:02:37,000 --> 00:02:43,000
and the last time I saw them just a couple of weeks ago

34
00:02:43,000 --> 00:02:50,000
they accepted it and I was surprised myself and no discussion.

35
00:02:50,000 --> 00:02:52,000
It was fine.

36
00:02:52,000 --> 00:02:55,000
I was very happy about that.

37
00:02:55,000 --> 00:03:00,000
The only one in the time comes.

38
00:03:00,000 --> 00:03:08,000
But after I had lived in the Zen Center in the monastery for a whole year

39
00:03:08,000 --> 00:03:13,000
so they were quite prepared for this.

40
00:03:13,000 --> 00:03:23,000
Yeah, I think it was the same with me and it's maybe something that's not really something that we don't realize until it happens

41
00:03:23,000 --> 00:03:30,000
is that in the end what's far more important for our parents is the communication

42
00:03:30,000 --> 00:03:34,000
and the fact that we're going to lose us.

43
00:03:34,000 --> 00:03:43,000
But for my parents it was much more losing me than about having me change my religion or something.

44
00:03:43,000 --> 00:03:52,000
It seems to me that there is something deep down that your parents will gravitate more towards that

45
00:03:52,000 --> 00:03:57,000
and the okay, okay, become Buddhists if you want but don't let me never see you again.

46
00:03:57,000 --> 00:04:09,000
I think if it was a problem you could leverage that and say nothing and run away from that.

47
00:04:09,000 --> 00:04:11,000
That was the internet.

48
00:04:11,000 --> 00:04:34,000
Okay, stop.

