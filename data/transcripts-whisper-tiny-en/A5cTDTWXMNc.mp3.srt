1
00:00:00,000 --> 00:00:05,360
Hi, everyone. I'm Skip, a volunteer for Sir Mangolo International, the organization that supports

2
00:00:05,360 --> 00:00:10,720
Bonta UtoDamo in his teaching efforts. Bonta UtoDamo will be turning 40 on May 9th,

3
00:00:10,720 --> 00:00:15,680
and we would like to show our gratitude and appreciation by offering him a gift, the gift of

4
00:00:15,680 --> 00:00:21,920
diligent practice. On May 9th, we are asking any who wish to please join us in meditating

5
00:00:21,920 --> 00:00:26,800
in honor of Bonta that day, perhaps dedicating more time than we usually do to our practice,

6
00:00:26,800 --> 00:00:32,880
that specific day. We are also posing a challenge for any and all who wish to join us, 40 hours

7
00:00:32,880 --> 00:00:39,200
of meditation by May 9th, in honor of 40 years of life of our teacher. If you begin the day,

8
00:00:39,200 --> 00:00:44,800
this video is posted, the 40 hours equates to an average of 96 minutes of meditation each day.

9
00:00:45,440 --> 00:00:52,240
Join the meditation plus community at meditation.seramangolow.org to log your hours and chat with

10
00:00:52,240 --> 00:00:56,960
other meditators. Thank you for helping celebrate Bonta UtoDamo's birthday through the gift

11
00:00:56,960 --> 00:01:26,800
of diligent practice. May UV well, may UV happy.

