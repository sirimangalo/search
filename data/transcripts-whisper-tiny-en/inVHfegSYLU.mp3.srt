1
00:00:00,000 --> 00:00:02,000
Okay, go ahead

2
00:00:02,240 --> 00:00:15,760
From contact feeling from feeling craving does every contact always follow with the feeling is it okay to focus more on the liking or just liking than the feeling or is the liking just liking actually the feeling

3
00:00:17,440 --> 00:00:19,440
Thank you, Robin

4
00:00:20,520 --> 00:00:22,520
Okay, so contact

5
00:00:23,760 --> 00:00:25,760
contact actually

6
00:00:25,760 --> 00:00:27,760
arises

7
00:00:27,760 --> 00:00:33,080
See one thing you have to understand about patitya samupada dependent origination

8
00:00:33,440 --> 00:00:38,720
Is it doesn't refer to a linear it isn't linear like?

9
00:00:39,720 --> 00:00:42,560
Is it not a temporal sequence where?

10
00:00:43,320 --> 00:00:47,160
First x that first a then b then c then d it's it's not chronological

11
00:00:48,600 --> 00:00:55,880
And and that the actual actually the the limitation of dependent origination people this is why the Buddha said

12
00:00:55,880 --> 00:01:01,840
About dependent origination he scolded on and I who said oh, it seems quite quite easily understandable

13
00:01:01,840 --> 00:01:07,640
And the Buddha said what are you crazy? He said I didn't say that he said come now and I don't say that

14
00:01:07,640 --> 00:01:12,600
This is a very difficult thing to understand. It's very profound very hard to understand

15
00:01:13,400 --> 00:01:15,400
dependent origination

16
00:01:16,040 --> 00:01:19,720
And so it is scolded on and offer even suggesting that it's easy

17
00:01:19,720 --> 00:01:28,520
The reason being that there are 24 different kinds of of causality of causal relationships and they're being employed here in different

18
00:01:29,880 --> 00:01:31,880
cases so

19
00:01:32,200 --> 00:01:39,440
To say that contact creates feeling doesn't mean that first there's contact and then there's feeling contact and feeling are a part of the same thing

20
00:01:39,680 --> 00:01:44,120
experience, but the point is without contact you can't have feeling so the Buddha's

21
00:01:45,560 --> 00:01:47,560
explaining that using that as

22
00:01:47,560 --> 00:01:57,000
As the teaching so ever at every at every experience of seeing hearing smelling tasting feeling or thinking the five aggregates all arise

23
00:01:57,880 --> 00:02:04,960
I'm sorry except in the case of hmm in the case of the mind. It's not necessary because of the thinking. It's not necessary

24
00:02:06,160 --> 00:02:09,520
But the first five senses anyway, there's the physical

25
00:02:10,280 --> 00:02:12,280
there's the

26
00:02:12,280 --> 00:02:12,960
feeling

27
00:02:12,960 --> 00:02:17,520
We're waiting on it. There's sannya there's sankara and there's vinya and

28
00:02:22,000 --> 00:02:26,240
Although even with the pandas, it's not quite so clear because sannya and sankara

29
00:02:29,360 --> 00:02:33,760
Only arise don't arise at every moment. So you have to go to the abidama

30
00:02:33,760 --> 00:02:39,360
This is the point the the 24 types of causality and all the the explanations of physical and mental

31
00:02:39,360 --> 00:02:47,200
realities. So in an ordinary experience, there are 17 thought moments one one group a one one vision

32
00:02:47,840 --> 00:02:50,080
17 thought moments and that's a process of

33
00:02:50,880 --> 00:02:52,800
experiencing

34
00:02:52,800 --> 00:02:58,640
discerning discriminating and deciding and reacting to the experience

35
00:02:59,600 --> 00:03:01,600
so

36
00:03:01,600 --> 00:03:04,640
the experience itself requires contact and

37
00:03:04,640 --> 00:03:10,160
And each one of those 17 thought moments is going to have redina in it feeling in it

38
00:03:13,280 --> 00:03:15,280
That's technically speaking so

39
00:03:16,000 --> 00:03:22,000
So that's how you have to understand those two that actually contact and feeling don't don't be so don't don't don't don't

40
00:03:22,560 --> 00:03:25,840
mistake that it's chronological because it's not

41
00:03:27,520 --> 00:03:29,520
but

42
00:03:29,520 --> 00:03:37,040
And so ever actually every mind state has way denied it has a feeling in it. Whether it's just a neutral feeling when you first see something

43
00:03:37,360 --> 00:03:39,600
Or whether it's pleasant or unpleasant feeling

44
00:03:40,160 --> 00:03:45,440
A pain pleasant or painful feeling when you like or dislike or when you have pain or pleasure in the body

45
00:03:48,240 --> 00:03:53,920
But okay, that's that's the first question. The second question is focusing more on liking or disliking than the feeling

46
00:03:53,920 --> 00:03:59,040
Okay, this is a switching gears. We're talking about something practical and

47
00:03:59,680 --> 00:04:03,680
As far as the force that di patana goes, there's no sense that one object is

48
00:04:04,640 --> 00:04:05,840
more

49
00:04:05,840 --> 00:04:12,800
Is better to focus on than another now the commentary does say something that certain people should focus on certain

50
00:04:12,800 --> 00:04:14,240
Sati patanas

51
00:04:14,240 --> 00:04:16,880
so in this in this case a person

52
00:04:17,840 --> 00:04:19,200
with

53
00:04:19,200 --> 00:04:23,840
Raghacharita a person who is passionate who has had a lot of anger a lot so greed

54
00:04:25,440 --> 00:04:28,640
Should focus on we don't know should focus on the feeling a

55
00:04:29,760 --> 00:04:36,240
Person who is did teach at it who is more in in delusion in involved with delusion and views

56
00:04:36,960 --> 00:04:40,400
wrong view the sort of more cerebral sort of person

57
00:04:40,400 --> 00:04:48,160
Should focus more on the dumbness. So should focus more on liking disliking drowsiness distraction doubt

58
00:04:49,360 --> 00:04:51,360
the hindrances

59
00:04:52,240 --> 00:04:54,240
So that would be the

60
00:04:54,240 --> 00:04:59,840
Deciding factor is it okay to focus more on the other only in this case where you you realize that you're

61
00:05:00,560 --> 00:05:01,760
more

62
00:05:01,760 --> 00:05:07,120
Your problem is more the the cerebro side or your problem is more the visceral sort of

63
00:05:07,120 --> 00:05:10,000
Feeling side to read another side

64
00:05:12,160 --> 00:05:19,760
And is liking disliking actually the feeling know they are two very different things liking and disliking are accompanied by specific feelings, but

65
00:05:20,560 --> 00:05:22,560
the feelings

66
00:05:22,560 --> 00:05:24,560
Certain feelings may

67
00:05:24,880 --> 00:05:31,280
Arise separate now the only exception is with dominant set so a painful mental feeling is

68
00:05:31,280 --> 00:05:39,120
It's not synonymous, but can only arise you could for all intents and purposes call it synonymous with

69
00:05:40,000 --> 00:05:42,800
Disliking because it only arises with disliking

70
00:05:44,160 --> 00:05:50,800
Disliking and the painful mental feeling are for all intents and purposes one and the same now

71
00:05:51,600 --> 00:05:58,240
Courtney the abi dhamma going to the the the orthodoxy. They are not they are two separate aspects of the mind state

72
00:05:58,240 --> 00:06:05,520
Which makes sense because liking disliking is a judgment painful mental feeling is a is a feeling still a little bit

73
00:06:06,480 --> 00:06:08,480
I'm still a little bit

74
00:06:08,480 --> 00:06:10,480
suspicious

75
00:06:10,720 --> 00:06:12,720
Practically speaking they are

76
00:06:12,720 --> 00:06:14,880
Really one in the same a negative mental

77
00:06:15,920 --> 00:06:23,040
Interpretation of something but on the other hand a positive mental feeling isn't necessarily liking so a happiness

78
00:06:24,000 --> 00:06:26,000
mental mental pleasure

79
00:06:26,000 --> 00:06:32,080
Can arise with wholesomeness when a person called the mixed wholesomeness it can even arise with

80
00:06:33,120 --> 00:06:35,440
With a functional mind and Arahad can have

81
00:06:36,320 --> 00:06:39,600
Pleasant mental feeling, but they can't have an unpleasant mental feeling

82
00:06:40,560 --> 00:06:41,920
Interesting

83
00:06:41,920 --> 00:06:45,840
It's it's it's just more complicated than that according to the

84
00:06:45,840 --> 00:06:55,840
Texts

