Hello welcome back to Ask a Monk. Today I will be answering a question from the new forum
ask.surrimundalud.org which is our replacement for the Ask a Monk platform. The new
forum one thing I'd like to say for those people who are getting involved in it is that
is two things. First of all, that the content on the form should be Buddhist. So I'm probably
going to do a little bit of moderating or at least, you know, posting notices for questions
that are asked or answers that are out of line with the Buddhist teaching. I would prefer
that, all right, I think I'm going to insist that questions and answers both be of a Buddhist
nature. So we're not looking for answers from other schools. The other thing is, I'm probably,
I haven't 100% decided this, but I'm pretty sure that we're going to require or limit the questions
and answers to Theravada Buddhism or what is in line with the sort of things that I teach because
Buddhism is, of course, a broad subject. So this isn't meant to be as far as I've sort of
designed it or have it. The concept in my mind is that it's not meant to be a broad-based
forum, so there's probably going to be some moderating going on and notifications
because I'd rather, you know, it's not supposed to be so broad. It's just a chance for the group of
people who are interested in the things that I teach and practice and the sort of similar things
to what I practice and teach, not just my students, but people who are into the same sort of things
in a fairly narrowly defined circle to ask and to answer questions. That's my hope anyway.
I'm, you know, in my change. So, okay, today's question is of interest to me specifically
because I happened in the car driving here in California and now back in Moore Park, California,
I'll be leaving tomorrow for Canada. To over here a news bulletin bulletin that Stephen Hawking,
the renowned theoretical physicist, has said that he's not afraid of death, which is a good thing,
but he says that he doesn't believe in rebirth because to him the brain is a
machine is just a machine, a computer, and he doesn't believe in rebirth. That's just a
fairy tale for people who are afraid of the dark. That's the quote. So, because I think Stephen
Hawking is a very well-known person and very well-respected in his field, I thought it would be
important to talk about the concept of rebirth. So, the question today is going to be,
this is going to be posted as a response to it, so you can read it up there for yourself,
is basically in regards to rebirth, but it also touches on the idea of Nirvana or Nirvana,
and so I want to address both of these topics. The gist of it is that many people will
not see a reason for exerting themselves in the meditation practice. Don't see a reason to
try and change who they are and to work hard to purify their mind. See, the reason being that
an impure person can theoretically gain the same sorts of happiness and peace in this life,
perhaps even more, and as much more it is potentially in this life than a person who struggles
and strives for some theoretical enlightenment. So, the one thing in the question that I really
think needs to be addressed is where the person, I assume, identifies themselves as a skeptical
materialist or is referring to people who are skeptical materialists. And the idea that they don't
take anything on faith and therefore these promises of the future, bliss or future happiness or
future enlightenment seem rather shallow, even the attainment of those things seem meaningless.
And the person even talks about suicide, he says, well, wouldn't suicide, he or she,
he says, wouldn't suicide obtain the same result. So, for this reason I want to talk about
Buddhist understanding of rebirth and of Buddhist understanding of enlightenment and Nirvana.
First, I've explained my understanding of rebirth in another video. It's a video on the nature
of reality. So, if you Google that or look up under my videos, the nature of reality, you'll get
my understanding of what is the Buddhist concept of rebirth. And basically, and this also touches
on Stephen Hawking's claim, it's not that we believe in rebirth, it's that we don't believe
in death. So, when Stephen Hawking says that he doesn't believe in reincarnation or rebirth,
that's just a fairytale because he sees things from a very empirical point of view. He's actually
going against both what a meditator or a person who is studying the nature of experience,
what they experience. And he's also going against many, many accepted principles in theoretical
physics or many points that should be accepted. And why they should be accepted is because it
was people, the people who began to study quantum physics who developed these. If you look at
orthodox quantum physics, people like Niels Bohr, Heisenberg, the people who put together the
foundations of quantum physics, what they did with quantum physics is came to see that reality
isn't an objective state, that three-dimensional or four-dimensional space, or space time,
is theoretical. It's something that we postulate, we say that this exists, we say that the brain
exists, we say that the body exists. But even the brain, as we understand it, is only a grouping
of potential states that require consciousness, require our observation without our
examination of reality. You can't even talk about, this is according to the original
orthodox interpretation of quantum physics, you can't even begin to talk about the
standards in the world around us. So when Stephen Hawking talks about death,
we have to ask him, what is the death of what? It's the death of what sort of death is he talking
about, he's talking about the death of a theoretical object, something that only exists in the
minds of human beings. It's something that we have created as a theory, and we can never prove
that it exists. And of course, this is the nature of material science. So on the other hand,
from conversely, what is empirical is the consciousness is our experience. So when we look at
the theoretical world around us, this three-dimensional or four-dimensional space time,
then we can see things dying. We can see the brain falling apart, we can see the body falling
apart, we can see something that was working together as a system, no longer working together as a
system. But all that we're talking about is this theoretical system or theoretical
matrix of energy, of electrons and subatomic particles working together and coming together
in a sort of a wave and then falling apart, again and again and again and doing this in order.
And it's all theoretical. It's something that we have built on this science around.
But what we experience in reality is the sites and sounds and smells and tastes and feelings
and thoughts. We experience things. There's the physical side of experience and there's the
mental side of experience and this continues on incessantly. And it is also governed by natural
laws. The laws of cause and effect for example. So when you, the intent, the mental intention,
to do things, to become something, to change things, to act and to speak and to think,
has an effect on what comes next. It creates the next moment. And these laws are easily verifiable
by anyone who practices, not practices as a specific meditation, but one who studies them.
If you study the empirical reality in front of you, this experience, right now you're experiencing
things. If you study this, you can use this technique that I have been spreading and all of your
label things as they are. It's not a subjective practice. It's not a religious practice. When you
see something, you say you're self-seeing. So you label it as seeing to straighten your mind in
regards to the object that it is what it is. This is seeing, this is hearing, this is smelling.
When you feel pain, you say you're self pain and you know it for what it is. Your mind starts to
become clear. Your mind starts to straighten and you're able to see things as they are.
If you do this, you'll be able to see cause and effect. You'll see what it is that's causing yourself
for which causing you happiness. You'll see what's causing you to speak, what's causing you to
think, what's causing you to act. You'll see the cause and effect and you'll see that this is the
way things work. So to say, and this is the catch, this is the main point, to say that at the moment
of death, there is nothing, is really to postulate a violation of the experience, the laws of
experience. The reality that we are experiencing right here and the laws that are easily verifiable,
if not codifiable, because it's not like material science where you can put a number to it.
Not unless you're really, really, really good. Apparently the Buddha was able to put
numbers on these things and say how many in this and how many of this, how many of that,
followed by this, followed by that. But you can see the way things, the general way things work,
to postulate death, to postulate a moment where that stops happening requires some explanation
and requires a leap of faith, actually. So this is a way of turning the tables on the materialists
and saying to them, well, you're not the skeptics, you're the faith believers, you're the believers,
you believe in something that is theoretical, you believe in a theoretical death that is somehow
influencing reality in a way that is not experiential, that you've never experienced,
you don't know anyone else who has experience, you can't verify it with anyone else,
and you can't verify it inside yourself. So there's a belief going on there, you may very well
be right. It may be that at the moment of physical death, there is a mental death as well.
But that's a violation, or yeah, it's a violation of the laws of experiential reality
of cause and effect, because all of the causes at the moment of death then don't have an effect.
And so there's something going on there, there's something that has to be explained there.
Now in Buddhism, we only postulate one example of this, or one instance where this is possible,
and that is where no more causes are created, where the mind has given up that which causes,
and that's what Nurban is, that's what's so special about this. That's why it's such an important
thing. It's like, you know, we're positing something that requires an explanation, that
is an exception. Nurban is not just something all well, then you die, and that's it.
Nurban is, well, because unless you reach this very special state, death means nothing.
There's no end to our experience, to the seeing, the hearing, the smelling, the tasting,
the feeling of thinking, to becoming, to wandering on and on and on and on again.
And this is experientially verifiable. I mean, there are actually a lot of evidence to support
this. But the point that I'm trying to make is you don't need that evidence. The evidence of
out-of-body experiences, of near-death experiences, of past-like memories. That's really interesting,
and I encourage people to look at that, to say that, oh yeah, there actually is some
consistency here. You know, this isn't just a theory, but there's something here that's
pointing in that direction. But without any of that evidence, it's the default to assume
that the experience continues on, because in order to say that physical death is
the mental death, you have to enter into the theoretical realm of four-dimensional spacetime,
which has seemed to be dependent on observation, until, because the theory goes that until a scientist
performs an experiment, you can't even talk about the result that they're going together. You
can't even talk about the things that they're experimenting on. It requires the scientist to make
a decision, which experiment they're going to perform, which basically in practical terms means
that until there is intention, until there is the focus, the mind which is paying attention to
something, you can't even talk about that which it's paying attention to. This is exactly in line
with the Buddhist teaching, and this is quantum physics, orthodox quantum physics, and the theory.
So it's interesting to hear Stephen Hawking talking like this, but it's not surprising because
there are quite a lot of physicists who side with them, and they have many theories that have gone
beyond orthodox quantum physics. Now there's even string theory and so on. But the point being
that this is all theoretical, the accepted fact is that the only thing that's empirical is our
experience, our experimentation, our observations of reality, the rest of it, all of our conclusions,
and so on are all theoretical. So I think that should clear some of it up here, but just to
the corollary from this, having to do with why one should strive, takes a little bit more
explaining why one should strive for enlightenment. I think there's at least a little bit of impetus
out there already, there should be, in the fact that if you don't strive, death isn't going to
bring any change, you're going to be the same person that you are now after you die. So in more
so, you're going to be going along the course of cause and effect. So if you're a person who
kills, deals, lies, cheats, takes drugs and alcohol and dilutes yourself and so on, guess what?
The effects of that are going to become the causes for future effects.
It's a chain, and I think this should wake people up to some of the terrible things that we do,
and really it really needs to be said, this sort of thing, because we're violating it so
terribly. We believe that at the moment of death, that's it. Just put it off. It's okay, you can do a
little bit of this stuff, these bad things, thinking that in the end, we all die, and just sort of
rushing towards death. When you die, you'll find some sort of relief route. Well, guess what? That's
a theory. It's based on belief. It's not sustainable by the facts, and it's not true. It's not the
nature of reality. The nature of reality has experience that continues on, and it's based on cause
and effect. I mean, it makes so much sense. Irrigardless of any sort of proof or any sort of
experience, any sort of evidence to back it up, it's so easy to see, and it's so easy to understand,
and it makes such sense of the universe. Something that was previously so full of questions,
why, and how, and what, and what is the meaning of life, and so on, becomes so simple. It suddenly
makes sense. Why are some people rich? Why are some people poor? Why is there suffering in the
world, and so on, and so on? Suddenly, all of these questions of why are we here, and why is there
the universe? Suddenly becomes so clear. For many points of view, it's easy to understand why this
might be the case. Why we should practice to better ourselves is for a better future. This is one
way of looking at it. But there's something that's much more fundamental, a reason for practicing
that's much more fundamental than I'd like to explain, and the explanation is that I use the
analogy of a sweater. When we begin to practice, the reason why we begin to practice is because
we have suffering. We have something in our lives that is causing us pain, causing us discomfort.
So we start with that, and this is analogous to pulling on the sweater. Because you like the
sweater. You think the sweater's fine. It's just this one thread that's causing problems. So you pull
on it. And as you pull on the thread, the whole sweater starts to unravel. And so finally, there's
no sweater left. You pull and you pull and you pull and one thing leads to another and suddenly
it's ungone. Proper practice of the Buddha's teaching is in this way. There is no leap of faith.
There is no practice that is out of proportion to the observations or the need to practice.
You practice according to the need, according to the pain and the suffering that you have in your
lives. The result is to finding more and more and more, you know, one problem leads to another,
one realization leads to another. You realize that the cause of suffering is not as simple as
you thought. It's not getting rid of the headaches or getting rid of the backaches or the tooth
things or getting rid of this, you know, you had a problem with a person or relationship that ended
or so on. You lost your job. It's not about just getting your job back about getting the relationship
back about fixing this or fixing that. Once you start to really see the reasons why you're suffering,
you want to practice more and more and your practice becomes much more part of your life.
Because you see that the answers are much more fundamental than you thought.
Until finally, you realize that there is nothing worth clinging to. There is no stable reality
in this universe. That brings up another point that I wanted to talk about. The idea of
Buddhist practice being suicide and the end of a self. This is very clearly discussed in the
Buddhist teaching and with a very simple response that only for a person who postulates a self
is there any possibility of suicide or of death of that self. So the reason why we are afraid
of enlightenment, we're afraid of Nirvana or we think of it as something like suicide is because
we postulate an existence, a being that exists to life, that is. In Buddhism, we don't do that.
In Buddhism, we understand experience to be experienced. We don't postulate an experienced
sir of being a self that experiences. I think it may have been mentioned in your state that you
in your question that you don't believe in the self. I'm not sure if it's in there, but someone
was talking about how it's only for people who believe in the self and that's absolutely
I think a proper thing to say. Without a self, Nirvana is not the death of a self because there
was no self to begin with. It's a death of this concept that there was a self. Nirvana is simply
the ending of a rising which each rising is impermanent suffering in non-self. It's something that
in and of itself has no insistence. It comes and goes. It's something that's instantaneous
anyway. Nirvana is simply when this doesn't happen anymore. When there is freedom from all of
this phenomenon, the arising of things. And that is something that comes naturally. It's something
that comes through an understanding of reality and giving up of our attachment to things that
are causing us suffering. So I hope this is helped. And I think the reasons why you meditate and
read suit this is because you find them wholesome and interesting. That's the best reason to do it.
I think any kind of, as people have said in the answers already, any kind of intellectual
arguments about why you should practice and why you should, or about Nirvana and some hypothetical
state that you haven't experienced for yourself. Because the other thing about Nirvana is the
reason why we postulate is because the people who have practiced have experienced it. Once you
experience it, then you start talking about it. You say, yeah, there's this thing called Nirvana
and it's the freedom from suffering. And so then you start to teach this sort of thing because you
have experienced it. We don't start there and say, okay, there's Nirvana and I'm going to try to
reach it. You start practicing because you have suffering. And once you practice and practice,
you eventually reach Nirvana and then you can say for yourself that this is the ultimate
freedom from suffering. You've gone along and reduced and reduced and reduced your suffering
until you reach Nirvana. And in that moment, there is no suffering. When you realize it,
you say, that's the goal and you strive for that and you work harder to obtain that only because
you realize it and only because you know for yourself that that's what it is, not out of faith,
not because I said it because the Buddha said it. So practicing just to better yourself is the
best reason already. As far as talking about Arahans and why Arahans won't commit suicide,
I don't, I can't think of any instance of where an Arahan committed suicide. There may be one
because it may be that an Arahan says to himself, here I'm sick and I'm a burden on my friends
and so on. Therefore, I have no reason to live. I don't know that they would actually, I've never
heard what an Arahan would actually commit suicide. But it may be the case that it's in there because
to me, what an Arahan, what an enlightened being, what it wouldn't do is for quite a bit of
speculation. But mostly an Arahan would not commit suicide because they're not interested in it.
They don't have any attachments anymore. But as for a person who hasn't become enlightened,
they might commit suicide because they still have an attachment and they feel the suffering and
they don't want to suffer anymore. They also have the wrong view and wrong idea that by killing
themselves somehow they will attain some kind of peace. Now as I hope I've at least given an
interpretation of reality, that that points out that that's not the case and whether you agree
with what I've said or not is totally up to you. And I hope this is helped to give some
background and shed some light on a teravada Buddhist interpretation or teravada Buddhist
explanation of things like rebirth and nirvana, which I think neither requires
belief and certainly the practice and meditation doesn't require belief. It requires a little bit
of faith that what you're going to gain, that you're going to gain something out of the practice
in the beginning. And then once you start to practice, you gain something. So then your faith
was well rewarded and is no longer required. And the more you practice, the more you gain.
When you stop gaining, you can stop practicing. If the practice somehow doesn't bring you
any good results, then you're obviously bound to stop. And so the only reason people really
succeed in meditation is because they continue to gain benefit. And that's really the wonderful
thing about meditation practice and experiential examination and study of reality.
It's not the more you do it, the more benefit you gain and the more you want to do it.
So I hope that helps. This has been an answer to this sort of question. And hopefully to
counteract some of the, what would believe to be the wrong understanding that is going to be
created by such a famous person's wrong use. So maybe you think I wrong use, but to reach their own.
So all the best, let me take care.
