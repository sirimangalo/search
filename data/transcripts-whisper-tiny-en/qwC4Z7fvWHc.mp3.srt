1
00:00:00,000 --> 00:00:03,200
One said that anger leads to being reborn ugly.

2
00:00:03,200 --> 00:00:07,760
Is it okay to use that as motivation not to be angry?

3
00:00:11,200 --> 00:00:14,400
Or to be beautiful, I get the dilemma.

4
00:00:18,160 --> 00:00:21,040
I mean, is this kind of thing that helps you

5
00:00:21,040 --> 00:00:27,520
on a very coarse level understand karma, understand the way things are, that the mental realm also

6
00:00:27,520 --> 00:00:32,480
has a play in reality that there are causal relationships.

7
00:00:32,480 --> 00:00:36,080
There are consequences to your actions.

8
00:00:36,080 --> 00:00:40,240
I mean, one thing I've said before about karma is even if it were totally false

9
00:00:40,960 --> 00:00:49,040
that the next life has any bearing on the next life or that there is the next life.

10
00:00:51,280 --> 00:00:56,080
Even if that were the case, it's an incredible effect on humanity

11
00:00:56,080 --> 00:01:02,560
to believe it and to follow according to it, because if you follow that, you wouldn't be angry, right?

12
00:01:02,560 --> 00:01:10,800
So, that alone should strike a chord in people and help to see that maybe just maybe there's

13
00:01:10,800 --> 00:01:12,880
a hope that the universe makes sense.

14
00:01:12,880 --> 00:01:17,360
I mean, that's really the wonderful thing about Buddhism is that it takes away depression.

15
00:01:17,360 --> 00:01:21,360
It takes away the feeling of meaninglessness, the feeling of hopelessness,

16
00:01:21,360 --> 00:01:30,000
and it shows you that the universe does make complete sense if you can understand the universe.

17
00:01:30,960 --> 00:01:37,760
We have the power to understand and to see the truth of reality,

18
00:01:38,480 --> 00:01:40,240
and it makes very good sense.

19
00:01:40,240 --> 00:01:43,120
So people who are born ugly, there's a reason why they're born ugly.

20
00:01:43,120 --> 00:01:50,000
It's not just, I think better, that's a better reason to look at this teaching

21
00:01:50,000 --> 00:01:57,360
in terms of not getting upset and depressed and angry at God or just totally

22
00:01:59,120 --> 00:02:03,920
depressed. We're thinking that there's no meaning to the universe when you see good people

23
00:02:03,920 --> 00:02:12,880
suffering and bad people prospering or when you see certain people suffering a lot and certain

24
00:02:12,880 --> 00:02:17,200
people suffering only a little bit, some people ugly, some people beautiful and so on.

25
00:02:17,200 --> 00:02:27,360
That if you are meticulous about it, if you look in detail at reality,

26
00:02:27,360 --> 00:02:33,200
you come to see that actually it's perfectly rational and miraculous in fact,

27
00:02:34,000 --> 00:02:38,560
that people who are beautiful do deserve it. There's something that they did to get there.

28
00:02:38,560 --> 00:02:45,440
And it ain't lasting. Yeah, I mean, then there's the other side of me.

29
00:02:46,400 --> 00:02:55,600
Yeah, right. Beautiful at 18, but beauty queens that are beautiful when they're young

30
00:02:56,240 --> 00:03:02,800
and then become, you know, yes, they start looking in the mirror and they don't feel so beautiful.

31
00:03:02,800 --> 00:03:12,240
They, they've lost a lot of power socially too. It's, you know, to be beautiful is also a curse

32
00:03:13,360 --> 00:03:14,880
because it's temporary.

33
00:03:20,640 --> 00:03:28,720
Yeah, I think it's sad how, how fixated people become on that aspect of it. Oh, you mean,

34
00:03:28,720 --> 00:03:36,720
I can become beautiful if I'm just nice. And then of course, they're born beautiful and nice

35
00:03:36,720 --> 00:03:42,320
and rich and, and so on, even born in heaven and then they're like, yeah, I got what I wanted

36
00:03:42,320 --> 00:03:46,000
and they totally forget about all the good deeds they did become miserable people.

37
00:03:50,160 --> 00:03:53,680
So unless you're looking beyond, the problem is unless you're looking beyond that you're going

38
00:03:53,680 --> 00:03:59,120
to be satisfied, but then once you're satisfied, but you'll stop and you'll become a miserable person.

39
00:04:00,000 --> 00:04:00,640
Mean person.

40
00:04:03,360 --> 00:04:05,680
And it really is the truth. You can see it.

41
00:04:10,320 --> 00:04:17,440
You got to realize that almost any of these very old bitter people that you see that are,

42
00:04:17,440 --> 00:04:25,520
you know, bitter angry. At one time, they were very happy, beautiful young people. Something happened

43
00:04:25,520 --> 00:04:48,560
that long the way.

