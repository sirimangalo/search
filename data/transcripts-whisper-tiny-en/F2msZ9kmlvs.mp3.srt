1
00:00:00,000 --> 00:00:26,000
Good morning, Rick, good morning, Shranda, good morning, Internet.

2
00:00:26,000 --> 00:00:39,000
We tried yesterday to do a video on friendliness.

3
00:00:39,000 --> 00:00:51,000
I talked for quite a while, but nobody heard the two people here heard, and then we stopped.

4
00:00:51,000 --> 00:01:00,000
This morning we'll try again to talk about friendliness.

5
00:01:00,000 --> 00:01:05,000
The word many Buddhists are familiar with is meta.

6
00:01:05,000 --> 00:01:12,000
Often translated as loving kindness, kindness, love.

7
00:01:12,000 --> 00:01:18,000
Not often translated as friendliness, but that's literally what it means.

8
00:01:18,000 --> 00:01:22,000
Meta comes from the word mitta, which means friend.

9
00:01:22,000 --> 00:01:28,000
Mitta is a friend, person who is your mitta is a friend.

10
00:01:28,000 --> 00:01:36,000
Meta just turns it into a noun, friendliness or friendship.

11
00:01:36,000 --> 00:01:46,000
But here, particularly means friendliness, which I think is a good translation because love, of course,

12
00:01:46,000 --> 00:01:54,000
it's own connotations that I don't think are really appropriate.

13
00:01:54,000 --> 00:02:04,000
And kindness, as well as its own implications in terms of action, which may days end in action.

14
00:02:04,000 --> 00:02:13,000
Of course, if you have meta, your actions are going to be friendly, your speech will be friendly,

15
00:02:13,000 --> 00:02:18,000
your actions in speech aren't the mitta.

16
00:02:18,000 --> 00:02:27,000
And if we confuse them like that, then it's easy to get caught up in the actions in the speech,

17
00:02:27,000 --> 00:02:33,000
speaking friendly words, doing friendly things, but maybe our minds are not pure.

18
00:02:33,000 --> 00:02:41,000
If you're forcing yourself to do and say things that you think are kind,

19
00:02:41,000 --> 00:02:53,000
without friendliness in your heart, well, it's not sustainable, it's not really genuine.

20
00:02:53,000 --> 00:03:02,000
Friendliness, at least the idea that we're trying to get when we talk about mitta.

21
00:03:02,000 --> 00:03:12,000
Mitta is a well-known Buddhist concept, practice.

22
00:03:12,000 --> 00:03:16,000
I don't spend a lot of time talking about it.

23
00:03:16,000 --> 00:03:21,000
I think in our tradition, teachers generally don't.

24
00:03:21,000 --> 00:03:28,000
Because in the traditional sense, it's not necessary.

25
00:03:28,000 --> 00:03:31,000
It's not strictly speaking necessary.

26
00:03:31,000 --> 00:03:37,000
Which means to say there are many good things that aren't necessary.

27
00:03:37,000 --> 00:03:45,000
Good things that are helpful and supportive and generally practical and beneficial that we should engage in.

28
00:03:45,000 --> 00:03:48,000
But we're not necessary.

29
00:03:48,000 --> 00:04:00,000
So often we focus solely on what is necessary and I'll leave it to the practitioners or individual interactions between teachers and students to.

30
00:04:00,000 --> 00:04:07,000
Fill in the blanks.

31
00:04:07,000 --> 00:04:14,000
But mitta is something that is one of the few things that is generally beneficial,

32
00:04:14,000 --> 00:04:27,000
that you can pretty much recommend without reservation.

33
00:04:27,000 --> 00:04:38,000
It's most commonly prescribed for dealing with anger.

34
00:04:38,000 --> 00:04:52,000
So for people who are anger issues or holding grudges or with any kind of animosity in their hearts or ill will towards others.

35
00:04:52,000 --> 00:04:57,000
Irritability, that sort of thing.

36
00:04:57,000 --> 00:05:00,000
Mitta is an antidote for ill will.

37
00:05:00,000 --> 00:05:05,000
It reframes our relationships with others.

38
00:05:05,000 --> 00:05:14,000
Are we thinking about people we interact with?

39
00:05:14,000 --> 00:05:22,000
Sometimes our perception of others can be hostile to say the least.

40
00:05:22,000 --> 00:05:24,000
Mitta helps to reframe that.

41
00:05:24,000 --> 00:05:43,000
It helps us to experience what it would be like to be friendly, to think kindly about others.

42
00:05:43,000 --> 00:05:58,000
Mitta is generally beneficial for meditation practice, that's probably the best reason to undertake practice.

43
00:05:58,000 --> 00:06:09,000
It's of course practically useful in terms of changing the way our relationships function.

44
00:06:09,000 --> 00:06:19,000
The way we engage with others, if we change our minds and reframe our perception of others.

45
00:06:19,000 --> 00:06:30,000
Good, look at them differently, that if thinking about them as adversaries or enemies,

46
00:06:30,000 --> 00:06:35,000
think of them as beings who suffer and wish for happiness.

47
00:06:35,000 --> 00:06:43,000
Change of so many things about our lives, people perceive us differently.

48
00:06:43,000 --> 00:07:01,000
Our actions, our speech, this is...

49
00:07:01,000 --> 00:07:05,000
It has an effect on others.

50
00:07:05,000 --> 00:07:11,000
When we're kind, it changes their perception of us.

51
00:07:11,000 --> 00:07:17,000
Of course there are health benefits to being kind and friendly,

52
00:07:17,000 --> 00:07:21,000
to have a friendly state of mind.

53
00:07:21,000 --> 00:07:30,000
None of the ill health associated with stress and anger and holding grudges,

54
00:07:30,000 --> 00:07:36,000
which is a very unpleasant and healthy sort of mind state.

55
00:07:36,000 --> 00:07:43,000
Mitta mentioned other benefits like sleeping happily,

56
00:07:43,000 --> 00:07:49,000
sukho, supatim, sukho, patim, pujate, waking up happy,

57
00:07:49,000 --> 00:07:56,000
having pleasant dreams.

58
00:07:56,000 --> 00:08:01,000
And being generally protected, Mitta is considered a protection.

59
00:08:01,000 --> 00:08:05,000
Protects us from danger, from harm.

60
00:08:05,000 --> 00:08:09,000
There's a power to the friendly mind state.

61
00:08:09,000 --> 00:08:17,000
It's... it's concentrated for us.

62
00:08:17,000 --> 00:08:26,000
It's a... it's a mind state that pulls the mind together,

63
00:08:26,000 --> 00:08:30,000
keeps the mind together and coherent,

64
00:08:30,000 --> 00:08:35,000
and keeps the mind strong and healthy.

65
00:08:35,000 --> 00:08:41,000
So there are many ways to practice Mitta.

66
00:08:41,000 --> 00:08:47,000
And one reason for not giving real formal instruction is because of...

67
00:08:47,000 --> 00:08:51,000
it's really a general basic practice,

68
00:08:51,000 --> 00:08:58,000
and it shouldn't really be formalized as a practitioner of mindfulness.

69
00:08:58,000 --> 00:09:01,000
You shouldn't really try to formalize it.

70
00:09:01,000 --> 00:09:04,000
It can become too wrote otherwise.

71
00:09:04,000 --> 00:09:07,000
It's meant to be an expression of friendliness.

72
00:09:07,000 --> 00:09:11,000
It should come from the heart naturally.

73
00:09:11,000 --> 00:09:15,000
It could... and should, I think, to some extent be ad hoc in the sense that

74
00:09:15,000 --> 00:09:20,000
you just make it up as you go based on the circumstances.

75
00:09:20,000 --> 00:09:23,000
It's most commonly prescribed, as I said,

76
00:09:23,000 --> 00:09:25,000
when you have ill-will towards others.

77
00:09:25,000 --> 00:09:30,000
So if there's someone... you have anger towards...

78
00:09:30,000 --> 00:09:34,000
direct it towards them.

79
00:09:34,000 --> 00:09:40,000
It's often used when you're afraid of something.

80
00:09:40,000 --> 00:09:46,000
I've had people explain that when they go to a new place

81
00:09:46,000 --> 00:09:50,000
and stay overnight in a new place,

82
00:09:50,000 --> 00:09:55,000
one of the first things they'll do is send Mitta to any spirits living in the place.

83
00:09:55,000 --> 00:09:59,000
For sure it helps them overcome any kind of fear of ghosts,

84
00:09:59,000 --> 00:10:03,000
but also if there are any spirits who are watching out

85
00:10:03,000 --> 00:10:07,000
they're more likely to be friendly,

86
00:10:07,000 --> 00:10:15,000
be disposed in a friendly way when they understand that you also are friendly.

87
00:10:15,000 --> 00:10:19,000
There was a group of monks in the Buddha's time.

88
00:10:19,000 --> 00:10:22,000
The Buddha taught what is called the Kalyana-Mitta-Suta.

89
00:10:22,000 --> 00:10:24,000
It's a very famous suta.

90
00:10:24,000 --> 00:10:27,000
They're about Mitta.

91
00:10:27,000 --> 00:10:31,000
He taught it when these monks were having trouble living in the forest

92
00:10:31,000 --> 00:10:33,000
and the spirits didn't like them.

93
00:10:33,000 --> 00:10:35,000
They didn't want them to stay.

94
00:10:35,000 --> 00:10:39,000
So they chanted the Mitta-Suta, Kalyana-Mitta-Suta.

95
00:10:39,000 --> 00:10:43,000
The Kalyana-Mitta-Suta is the Kalyana-Mitta-Suta.

96
00:10:43,000 --> 00:10:47,000
Kalyana-Mitta is the Kalyana-Mitta-Suta.

97
00:10:47,000 --> 00:10:59,000
They chanted this and the spirits were friendly.

98
00:10:59,000 --> 00:11:15,000
So one of the common ways to send Mitta is just based on our relationship with others.

99
00:11:15,000 --> 00:11:19,000
We start with something easy and move on to the more difficult.

100
00:11:19,000 --> 00:11:23,000
If you want to do it systematically, start with yourself.

101
00:11:23,000 --> 00:11:26,000
This is usually a good advice.

102
00:11:26,000 --> 00:11:31,000
Commentaries say that sending Mitta to yourself isn't really effective,

103
00:11:31,000 --> 00:11:34,000
but it's a good example.

104
00:11:34,000 --> 00:11:36,000
That's how they put it.

105
00:11:36,000 --> 00:11:39,000
It's a good way to frame it, a way to begin it.

106
00:11:39,000 --> 00:11:43,000
Put yourself in the right frame of mind.

107
00:11:43,000 --> 00:11:46,000
So let's spend a moment just wishing ourselves happiness.

108
00:11:46,000 --> 00:11:48,000
May I be happy?

109
00:11:48,000 --> 00:11:51,000
May I be free from suffering?

110
00:11:51,000 --> 00:11:56,000
The other thing about Mitta is it comes hand in hand with what we call karuna,

111
00:11:56,000 --> 00:11:59,000
which means compassion.

112
00:11:59,000 --> 00:12:01,000
So may I be happy?

113
00:12:01,000 --> 00:12:06,000
It's Mitta, sending Mitta to yourself, friendlyness to yourself.

114
00:12:06,000 --> 00:12:09,000
May I be free from suffering is actually karuna.

115
00:12:09,000 --> 00:12:20,000
It's compassion, reflecting on suffering and wishing for others not to have it.

116
00:12:20,000 --> 00:12:28,000
And again, the words should be up to you in whatever language and whatever voice you choose.

117
00:12:28,000 --> 00:12:30,000
May I be free from suffering?

118
00:12:30,000 --> 00:12:35,000
May I find peace?

119
00:12:35,000 --> 00:12:40,000
There are many, you can look some up, there are many chants that you can use as reference,

120
00:12:40,000 --> 00:12:49,000
but most importantly, it should be from the heart.

121
00:12:49,000 --> 00:12:55,000
You can throw Muddita in there, may they never lose what they've gained.

122
00:12:55,000 --> 00:13:01,000
May they gain and keep all good things.

123
00:13:01,000 --> 00:13:08,000
May all good things come to them.

124
00:13:08,000 --> 00:13:10,000
So wish it for yourself first?

125
00:13:10,000 --> 00:13:11,000
May I be happy?

126
00:13:11,000 --> 00:13:13,000
May I be free from suffering?

127
00:13:13,000 --> 00:13:17,000
May I find peace?

128
00:13:17,000 --> 00:13:24,000
And then you think about people you love, people you are friendly towards.

129
00:13:24,000 --> 00:13:29,000
They say you should be careful sending to someone who you're attracted to

130
00:13:29,000 --> 00:13:34,000
because that gives you the wrong state of mind, not the one we're looking for.

131
00:13:34,000 --> 00:13:41,000
So generally send it to maybe family members or friends.

132
00:13:41,000 --> 00:13:47,000
May they all be happy and free from suffering?

133
00:13:47,000 --> 00:13:51,000
May they find peace?

134
00:13:51,000 --> 00:14:01,000
And pick your own words, though.

135
00:14:22,000 --> 00:14:27,000
If you have trouble with that, go back to yourself.

136
00:14:27,000 --> 00:14:31,000
Wish for yourself to be happy and free from suffering.

137
00:14:31,000 --> 00:14:41,000
And in this way you start to be a better at it.

138
00:14:41,000 --> 00:14:45,000
Become more proficient, go back and forth between yourself and those you love.

139
00:14:45,000 --> 00:14:52,000
And when you're good at that, then move on to people who you're indifferent towards.

140
00:14:52,000 --> 00:14:56,000
People you know, but don't know very well perhaps.

141
00:14:56,000 --> 00:15:14,000
Maybe coworkers or classmates, the post-person neighbors, people you see on the street.

142
00:15:14,000 --> 00:15:17,000
Maybe people you don't even know.

143
00:15:17,000 --> 00:15:19,000
May they be happy.

144
00:15:19,000 --> 00:15:22,000
It's more difficult really.

145
00:15:22,000 --> 00:15:24,000
But you work at it.

146
00:15:24,000 --> 00:15:32,000
When you're not able to do that, you go back to sending to those who you love or even to yourself.

147
00:15:32,000 --> 00:15:43,000
Now you've got three bases and you try to get to the point where you can be friendly towards all three groups.

148
00:15:43,000 --> 00:15:52,000
Equally.

149
00:15:52,000 --> 00:15:54,000
This is the systematic way.

150
00:15:54,000 --> 00:16:04,000
You don't have to be that systematic, but if you know the systematic ways, then you can do a more basic practical practice on your own.

151
00:16:04,000 --> 00:16:09,000
More informal practice on your own.

152
00:16:09,000 --> 00:16:18,000
And once you're able to send to these different people, then send to those people you don't like.

153
00:16:18,000 --> 00:16:20,000
Those you have problems with.

154
00:16:20,000 --> 00:16:25,000
Those you've been angry towards or who have been angry towards you.

155
00:16:25,000 --> 00:16:29,000
And try and create a friendly state of mind towards them.

156
00:16:29,000 --> 00:16:31,000
Something new.

157
00:16:31,000 --> 00:16:32,000
May they be happy.

158
00:16:32,000 --> 00:16:34,000
May they be free from suffering.

159
00:16:34,000 --> 00:16:36,000
May they find peace.

160
00:16:36,000 --> 00:16:39,000
May all good things come to them.

161
00:17:06,000 --> 00:17:09,000
Thank you.

162
00:17:09,000 --> 00:17:36,000
Thank you.

163
00:17:36,000 --> 00:17:57,000
One of the great things about Manta is it helps you see past the evil of other people.

164
00:17:57,000 --> 00:18:02,000
Because wishing for them to be happy doesn't mean wishing they can get away from their evil.

165
00:18:02,000 --> 00:18:12,000
It helps you realize that a big part of the problem of the evil of ourselves and others is how much suffering it causes for us.

166
00:18:12,000 --> 00:18:19,000
You couldn't possibly be happy or peace or free from suffering if you have evil in your heart.

167
00:18:19,000 --> 00:18:41,000
So any unwholesomeness in others is we refrain it as a part of their suffering.

168
00:18:41,000 --> 00:18:50,000
We come to see that wishing them happiness is really wishing for them to be free from evil.

169
00:18:50,000 --> 00:18:56,000
And it reframes the whole situation instead of seeing that person as evil.

170
00:18:56,000 --> 00:19:02,000
We realize that there will be sat with unwholesomeness.

171
00:19:02,000 --> 00:19:16,000
It's an affliction on them.

172
00:19:16,000 --> 00:19:28,000
And eventually the practice if you want to get into it is to be able to send love with very decent friendliness to all four groups.

173
00:19:28,000 --> 00:19:34,000
For how many? Yes, all four groups, yourself and the three other groups.

174
00:19:34,000 --> 00:19:36,000
Equally.

175
00:19:36,000 --> 00:19:38,000
But you don't have to go into that much detail.

176
00:19:38,000 --> 00:19:41,000
Spend a little time in this way is a very good way.

177
00:19:41,000 --> 00:19:43,000
That's a classical way.

178
00:19:43,000 --> 00:19:45,000
There's another way and there are other ways.

179
00:19:45,000 --> 00:19:52,000
But another very good one, one that I'm more comfortable with.

180
00:19:52,000 --> 00:20:00,000
That I use a little more frequently is the ever-expanding, loving,

181
00:20:00,000 --> 00:20:03,000
or ever-expanding friendliness.

182
00:20:03,000 --> 00:20:11,000
Trying to get used to using this word friendliness.

183
00:20:11,000 --> 00:20:16,000
And this is where you start with yourself and then you expand out really physically.

184
00:20:16,000 --> 00:20:22,000
It's simpler. I think it's less difficult conceptually.

185
00:20:22,000 --> 00:20:24,000
And in many ways it's more powerful.

186
00:20:24,000 --> 00:20:32,000
It's also very much the way the Buddha would teach.

187
00:20:32,000 --> 00:20:37,000
So you start with yourself, may I be happy, may I be free from suffering, may I find peace.

188
00:20:37,000 --> 00:20:41,000
May all good things come to me.

189
00:20:41,000 --> 00:20:51,000
And then you move on to the people in the room with you or in the building with you.

190
00:20:51,000 --> 00:20:52,000
Different ways of doing it.

191
00:20:52,000 --> 00:20:59,000
You can start with just humans and then wish for animals and then wish for spirits and so on and so on.

192
00:20:59,000 --> 00:21:02,000
It's going to get very complicated if you want to get into it.

193
00:21:02,000 --> 00:21:09,000
But let's just say all beings in this room may they all be happy.

194
00:21:09,000 --> 00:21:13,000
And about humans, animals, spirits.

195
00:21:13,000 --> 00:21:40,000
All of them may they all find peace in place, may they all be free.

196
00:21:43,000 --> 00:21:54,000
Expand it to the building or beyond the building to the neighborhood.

197
00:21:54,000 --> 00:21:56,000
Start.

198
00:21:56,000 --> 00:22:02,000
It gets more difficult when you start to think about all the beings in this neighborhood.

199
00:22:02,000 --> 00:22:05,000
Your mind starts to expand.

200
00:22:05,000 --> 00:22:11,000
It's more difficult, more challenging, but therefore also it strengthens the mind to train in this way.

201
00:22:11,000 --> 00:22:18,000
You become stronger to be able to think about generally all beings in this area.

202
00:22:18,000 --> 00:22:21,000
May they all be happy.

203
00:22:21,000 --> 00:22:24,000
May they all find peace and bliss.

204
00:22:24,000 --> 00:22:38,000
May all good things come to them.

205
00:22:38,000 --> 00:22:45,000
You start to see how mindfulness is very useful for directing the mind in whatever way we wish

206
00:22:45,000 --> 00:22:48,000
because the mind is clearer and pure.

207
00:22:48,000 --> 00:22:55,000
One of the benefits of mindfulness practice is an increased capacity for things like

208
00:22:55,000 --> 00:23:13,000
made karuna. It's easier once you've cultivated mindfulness.

209
00:23:13,000 --> 00:23:24,000
And then you move on from the neighborhood to the city, the county or wherever you happen to be.

210
00:23:24,000 --> 00:23:34,000
We all beings in this whole area be happy and free from suffering.

211
00:23:34,000 --> 00:23:43,000
Depending where you are, you might move right to the province or state.

212
00:23:43,000 --> 00:23:45,000
You start to expand outward.

213
00:23:45,000 --> 00:23:54,000
Visualizing is helpful. Visualize, think of the whole area, think of all the beings there.

214
00:23:54,000 --> 00:23:56,000
May they all be happy.

215
00:23:56,000 --> 00:24:00,000
All those beings, you don't even know you've never even met.

216
00:24:00,000 --> 00:24:03,000
May they all be happy and free from suffering.

217
00:24:03,000 --> 00:24:17,000
May they fall or find peace.

218
00:24:17,000 --> 00:24:19,000
You move on to the country.

219
00:24:19,000 --> 00:24:24,000
May all beings in this country be free from pain and suffering.

220
00:24:24,000 --> 00:24:27,000
They all find peace and bliss and be happy.

221
00:24:27,000 --> 00:24:38,000
Well, free from enmity, free from disease.

222
00:24:38,000 --> 00:24:41,000
Free from danger and fear.

223
00:24:41,000 --> 00:24:49,000
Free from hunger and thirst. May they all be happy.

224
00:24:49,000 --> 00:24:53,000
All beings in this world eventually spread it to the world.

225
00:24:53,000 --> 00:24:58,000
Think of the whole world, encompass the world with your mind.

226
00:24:58,000 --> 00:25:11,000
Be all beings on earth, humans, animals, spirits, angels and demons and everything in between.

227
00:25:11,000 --> 00:25:20,000
May they all be happy and well.

228
00:25:20,000 --> 00:25:36,000
And you spread it to the whole universe, all the gods and angels and celestial beings included.

229
00:25:36,000 --> 00:25:42,000
Maybe any other beings on other planets, if they exist.

230
00:25:42,000 --> 00:25:54,000
May all beings be happy without limit, up a manja without limit.

231
00:25:54,000 --> 00:26:03,000
May they all be happy and well.

232
00:26:03,000 --> 00:26:07,000
It can be very challenging because of the magnitude of course.

233
00:26:07,000 --> 00:26:12,000
There are many ways to break it down, as I said, you can break it into categories.

234
00:26:12,000 --> 00:26:24,000
You can even break it up into adults, children, males, females, just to be able to visualize, conceptualize easier.

235
00:26:24,000 --> 00:26:28,000
You can break it into directions as a common one.

236
00:26:28,000 --> 00:26:33,000
All people north of me, in front of me, in the direction in front of me.

237
00:26:33,000 --> 00:26:45,000
All beings, all people, all animals, to my right side, to my left side behind me, above me, below me.

238
00:26:45,000 --> 00:26:51,000
You can get quite complicated or quite involved at least.

239
00:26:51,000 --> 00:27:01,000
And again, it's not something we should get too involved in because it is no substitute for mindfulness.

240
00:27:01,000 --> 00:27:07,000
But because friendliness is an important quality and a good support for mindfulness.

241
00:27:07,000 --> 00:27:14,000
And because mindfulness makes us more friendly, more capable of cultivating friendliness,

242
00:27:14,000 --> 00:27:23,000
we should all think and consider and to some extent incorporate this into our practice as we're comfortable.

243
00:27:23,000 --> 00:27:27,000
And in a way that's comfortable.

244
00:27:27,000 --> 00:27:40,000
And many benefits, including the benefit for our practice, to keep us at ease and smoothly.

245
00:27:40,000 --> 00:27:45,000
Progressing on our path towards freedom from suffering.

246
00:27:45,000 --> 00:27:47,000
So that's the number for this morning.

247
00:27:47,000 --> 00:28:00,000
Thank you all for listening. Have a good day.

