Hello everyone, hello broadcasting live, February 7th, 2016.
Today's quote is about well-spoken speech.
A word is well-spoken if it is spoken at the right time, spoken in truth, spoken gently,
spoken about the goal or spoken about things that are meaningful and spoken with love.
So I'm in quite actually quite a simple teaching.
I'm useful.
A lot of the teachings like this are just useful reminders of
how the importance of goodness.
They said, tone, if you read through the Buddha's teaching, this is really
all you see, I mean, there's no, there's no reading between the vines, there's no hidden
agenda, so some religious movements and practices have a reason for why, why would we care
about words that are ill-spoken or well-spoken?
Because of them being good, you know, good for goodness sake, for example.
That's not even quite exact because it's not for goodness sake.
It's good because it's good, goodness is for goodness.
goodness really is the certainly essential quality in Buddhist practice.
And so when we talk about the goal or when we talk about the theory behind Buddhist practice,
it really is just a natural outcome of cultivation of goodness.
So in meditation, when we talk about the goal being Nirvana or we talk about it, cessation
of this kind of thing, it really is just an, it's the same thing that we're talking about
when we say practicing to give up our addictions, our aversion, our ego, practicing to
give up on wholesomeness, that's really it, that's all we're talking about.
So whether it be an action with a body, whether it be speech, or whether it be just thoughts
in the mind, it really is just a matter of cultivating goodness.
So I'm not too much to say tonight, we're doing, holding a peace symposium at McMaster
and so I just had a meeting this evening about that and potentially starting a peace
association at McMaster.
And now that's what we're talking about, because it's curious that Buddhism is as much
about peace work as it is about religion.
So in university I find myself involved as much in peace studies as I am in religion.
That goes to the point of what I was just saying, how Buddhism is just all about goodness,
it's all about peace, not about Buddhism, you know, there's nothing really specifically
Buddhist about the practice, as much as if it's about goodness, as much as it's about
peace, as much as it's about wisdom, it's about universal qualities of the mind really.
So it was specifically talking about peace, this quote about peace, speech, it's specifically
talking about speech, you know, this is not, it's more about the mental intention
behind the speech, I mean this is all this important, it's our mind that informs our
speech, it's our mind that informs our actions, and so the big part of our practice really
the main thrust of our practice is being conscientious and remembering ourselves, keeping
track of our minds and track of our volitions and our intentions, why we do things, how
we do things, the state of our minds when we do things, when we say things, and all
this has to do with, not just Buddhism, but it has to do with peace, and it has to do
with goodness.
Also it, I just got word that our online project is doing well, which is awesome, looks
like we're probably here to stay, anyway, not too much tonight though, nobody's coming
on the hangout I guess to ask questions, post the link in our meditation page, so I guess
we'll just call it a night, thank you all for coming on, thanks for coming out to
meditate, got a good list tonight, have a good night everyone.
