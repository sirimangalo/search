1
00:00:00,000 --> 00:00:04,000
What is the importance of the contemplation of Dukkha for meditation?

2
00:00:04,000 --> 00:00:08,000
Dukkha is the essence of Buddhist meditation.

3
00:00:08,000 --> 00:00:12,000
Dukkha is the essence of Buddhist meditation.

4
00:00:12,000 --> 00:00:23,000
Dukkha is the kinder, full realization of Dukkha is the first noble truth.

5
00:00:23,000 --> 00:00:27,000
That's what leads you to give up the second noble truth which is clinging.

6
00:00:27,000 --> 00:00:32,000
When you see Dukkha, and it doesn't mean thinking about Dukkha,

7
00:00:32,000 --> 00:00:35,000
it means actually contemplating those things that are Dukkha.

8
00:00:35,000 --> 00:00:39,000
And when you see them as Dukkha, when you are able to see that they are unsatisfying,

9
00:00:39,000 --> 00:00:43,000
then you will give them up.

10
00:00:43,000 --> 00:00:45,000
People give up the second noble truth, realize that they are noble truth.

11
00:00:45,000 --> 00:00:57,000
This is the path which is the fourth noble truth.

