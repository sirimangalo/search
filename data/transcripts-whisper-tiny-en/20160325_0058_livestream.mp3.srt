1
00:00:30,000 --> 00:00:55,800
Good evening, everyone, broadcasting live, March 24th, today's quote is from Novakadisut.

2
00:00:55,800 --> 00:01:21,000
So, what could it was unhappy as a layperson because, hey, it's very happy to see the Buddha.

3
00:01:21,000 --> 00:01:29,600
And, as a layperson, he didn't have the opportunity to see the Buddha often.

4
00:01:29,600 --> 00:01:35,000
The Buddha was, of course, very beautiful.

5
00:01:35,000 --> 00:01:36,200
And, so, what could it be?

6
00:01:36,200 --> 00:01:47,000
It was one of these people who was enamored by the Buddha's features.

7
00:01:47,000 --> 00:01:57,400
And, so, he would go to see the Buddha quite often, but he wasn't happy because, as a layperson,

8
00:01:57,400 --> 00:02:03,400
he didn't have the opportunity to see the Buddha as often as he liked.

9
00:02:03,400 --> 00:02:07,600
So, he became a monk.

10
00:02:07,600 --> 00:02:17,600
But, this sorta says that he was actually quite sick.

11
00:02:17,600 --> 00:02:27,680
And, so, he asked his fellow monks, go find the Buddha, tell them I'm sick, and please see if

12
00:02:27,680 --> 00:02:31,600
he'll come visit me.

13
00:02:31,600 --> 00:02:42,200
And, the Buddha comes, and he's ecstatic, and he tries to get up, and the Buddha says, don't

14
00:02:42,200 --> 00:02:49,200
get up, it's okay, I'll sit down in one of the seats.

15
00:02:49,200 --> 00:03:04,000
And, having sat down here, he said to them, he gave the standard question.

16
00:03:04,000 --> 00:03:20,400
Can you stand it, what can you bear it?

17
00:03:20,400 --> 00:03:32,400
Are you able to live with this sickness?

18
00:03:32,400 --> 00:03:57,660
Can you stand it up, you throw under the

19
00:03:57,660 --> 00:04:04,660
So he's asking, are you well, basically?

20
00:04:04,660 --> 00:04:15,620
And Vakali says, nammae vante, kamaniyam, naya paniyam, this, I can't bear it, I can't

21
00:04:15,620 --> 00:04:18,980
stand it.

22
00:04:18,980 --> 00:04:27,260
Balhami dukavidhana, abi kamanti, my painful feelings are getting worse, not decreasing.

23
00:04:27,260 --> 00:04:36,100
They're getting stronger, not weaker, it's the Buddha realizes well, he's possibly not

24
00:04:36,100 --> 00:04:47,700
going to get over this and he says, well, then, Vakali, do you have anything you regret?

25
00:04:47,700 --> 00:04:52,300
You have any worries or regrets about the past, anything that's bothering you?

26
00:04:52,300 --> 00:05:06,620
And he says, yes, Vante, I have some unresolved, many, not a few, not a few worries.

27
00:05:06,620 --> 00:05:16,220
And he says, well, Vakali is it in regards to your morality and he says, no, I have no problem

28
00:05:16,220 --> 00:05:18,340
with my morality.

29
00:05:18,340 --> 00:05:26,660
He says, well, if it's not about your morality, then what are you worried about?

30
00:05:26,660 --> 00:05:31,060
Making the point that really, you know, anything else you could be worried about is not

31
00:05:31,060 --> 00:05:33,700
really important at this point.

32
00:05:33,700 --> 00:05:42,380
I say, on your deathbed, is your morality pure, or is your mind pure, or you have good

33
00:05:42,380 --> 00:05:43,380
intention?

34
00:05:43,380 --> 00:05:50,660
It was could be wrong, he says, well, I haven't seen the Buddha in a long time.

35
00:05:50,660 --> 00:05:55,340
I don't get to see him after him being sick, I'm not able to go and see the Buddha in

36
00:05:55,340 --> 00:05:56,340
there.

37
00:05:56,340 --> 00:06:03,700
I want to go and see you, and my body doesn't let me do it.

38
00:06:03,700 --> 00:06:05,180
And how does the Buddha respond?

39
00:06:05,180 --> 00:06:13,340
He says, along Vakali, enough Vakali, kinte, imina, purtukahi, nadee, tena,

40
00:06:13,340 --> 00:06:27,820
what reason do you want to see this awful, repugnant rotten body?

41
00:06:27,820 --> 00:06:30,460
What good is it for you to see this body?

42
00:06:30,460 --> 00:06:33,900
Vakali had it all wrong, you know.

43
00:06:33,900 --> 00:06:40,260
You thought thinking about the Buddha and thinking about his physical form, it's not

44
00:06:40,260 --> 00:06:44,660
what thinking about the Buddha is, you know.

45
00:06:44,660 --> 00:06:52,940
This is why we kind of have sort of an ambivalence towards images.

46
00:06:52,940 --> 00:07:04,220
People think a Buddha image allows you to think about the Buddha, and yes, it can, but

47
00:07:04,220 --> 00:07:08,700
only as a starting point, the image itself is in the Buddha.

48
00:07:08,700 --> 00:07:13,340
The image can remind you of the Buddha, and you see the Buddha all right, the Buddha,

49
00:07:13,340 --> 00:07:18,100
and you start thinking about the qualities of the Buddha, not the physical qualities, the

50
00:07:18,100 --> 00:07:21,180
mental qualities.

51
00:07:21,180 --> 00:07:29,940
He was perfectly self enlightened, endowed with both knowledge of the truth and conduct

52
00:07:29,940 --> 00:07:30,940
according to the truth.

53
00:07:30,940 --> 00:07:39,660
It means he practiced what he preached, the incomparable trainer of humans and angels, well

54
00:07:39,660 --> 00:07:49,140
gone, all these kind of things worthy of gifts and all things, just a worthy sort of person,

55
00:07:49,140 --> 00:07:54,180
blessed enlightened, and then he said, the Buddha says,

56
00:07:54,180 --> 00:08:23,180
the Buddha is one with a dhamma, it's the dhamma that you have to get to.

57
00:08:23,180 --> 00:08:33,420
To see the Buddha, you have to see the dhamma, and it's one in the same.

58
00:08:33,420 --> 00:08:39,540
And so he tries to teach workily about the truth because workily has this idea of the beautiful

59
00:08:39,540 --> 00:08:45,620
body of the Buddha as being something pleasant, something he can find happiness in, and

60
00:08:45,620 --> 00:08:48,220
he says, what do you think work?

61
00:08:48,220 --> 00:08:58,540
The Buddha is this form permanent or impermanent, so he has to admit, well, a nitham panti.

62
00:08:58,540 --> 00:09:04,220
This is what you could look at this as a form of reporting, like we do these courses

63
00:09:04,220 --> 00:09:10,180
and we meet with the teacher once a day, and the Buddha did this as well, yes, the person.

64
00:09:10,180 --> 00:09:16,580
Tunging manya siv bhakha, the root pang nithang wa, a nithang wa, what do you think of form?

65
00:09:16,580 --> 00:09:22,820
Is it permanent or impermanent, stable or unstable, constant or incontinent, a nithang

66
00:09:22,820 --> 00:09:23,820
bhanti?

67
00:09:23,820 --> 00:09:29,980
Because it's changing all the time, not only are we getting older and our bodies changing,

68
00:09:29,980 --> 00:09:34,700
but the experience of the body only arises in sisis.

69
00:09:34,700 --> 00:09:40,940
What we think of as the body is made up of experiences that arise in sis, whose experience

70
00:09:40,940 --> 00:09:45,220
are changing all the time.

71
00:09:45,220 --> 00:09:49,860
So it's unstable, there's nothing that you can hold on to, this is why it can change

72
00:09:49,860 --> 00:09:59,940
so quickly and suddenly the form is changed, cut yourself, you hurt yourself, you get sick,

73
00:09:59,940 --> 00:10:04,340
the whole experience can change and then you're at a loss because you weren't prepared

74
00:10:04,340 --> 00:10:08,380
for that, you're clinging to something this way, you want to see the beautiful body of

75
00:10:08,380 --> 00:10:13,380
the Buddha, then he gets sick and he can't come and see the Buddha.

76
00:10:13,380 --> 00:10:23,380
He loses the root part that he wants to see, and he says, so what is young bhanti-tang

77
00:10:23,380 --> 00:10:28,820
dukong, this is actually from the Anatala Kannasut, it's a standard teaching, young bhanti-tang

78
00:10:28,820 --> 00:10:33,700
dukong wa sukong, tung sukong wa.

79
00:10:33,700 --> 00:10:40,220
What is impermanent is that suffering or is it happiness?

80
00:10:40,220 --> 00:10:49,660
This is the idea that the inconsistency of it means it can't fulfill your desire, it won't

81
00:10:49,660 --> 00:10:54,820
go into accordance with your desires, you want it to be, it doesn't come, you want it

82
00:10:54,820 --> 00:11:01,860
to go, it doesn't go, it's not in sync with our desires, so we suffer as a result, we

83
00:11:01,860 --> 00:11:14,820
get stressed as a result, this is where young bhanti-tang dukong wa parinam, tung dukong

84
00:11:14,820 --> 00:11:27,860
wa parinam, what is impermanence and suffering and subject to alteration, subject to change,

85
00:11:27,860 --> 00:11:34,460
tung sukong wa parinam, tung sukong wa parinam, is it proper for that to say, to say

86
00:11:34,460 --> 00:11:45,860
about that, a tung mama, this is mine, a so amasumi, this I am, a so meata, this is myself,

87
00:11:45,860 --> 00:11:51,700
these are the three types of clinging with, clinging with views, clinging with conceit,

88
00:11:51,700 --> 00:11:58,580
clinging with craving, a tung mama, this is mine, that's clinging with craving, some things

89
00:11:58,580 --> 00:12:09,100
we cling to because we want them, a so amasumi, this I am, that's clinging with conceit,

90
00:12:09,100 --> 00:12:16,980
so we think of I am this, I am that, I am strong, I'm smart, I'm beautiful, or I'm ugly,

91
00:12:16,980 --> 00:12:35,980
I'm stupid, etc, weak, this is conceit, clinging to d, to characteristics, a so meata,

92
00:12:35,980 --> 00:12:43,260
a so meata, this is myself, that's a view clinging with view, so we cling to the idea

93
00:12:43,260 --> 00:12:49,340
that this is me, this is an entity, this is a thing, the self exists, the soul exists,

94
00:12:49,340 --> 00:12:56,820
I can control things, I can control my body because this body is myself, those are views

95
00:12:56,820 --> 00:13:11,260
we cling to reviews, a tung mama, a so amasumi, a so meata, and so when you're

96
00:13:11,260 --> 00:13:33,260
basically repeats the anata, like an assertive, and I'm vocally having heard this, ask

97
00:13:33,260 --> 00:13:40,700
him, ask him, well in the Buddha left, and vocally ask his friends to lift him up and carry

98
00:13:40,700 --> 00:13:52,780
him to Isegila, Isegila, because he was ready to die and he said, how can one like me think

99
00:13:52,780 --> 00:13:59,580
of dying among the houses, he didn't want to die in the house, he died as a monk, so they

100
00:13:59,580 --> 00:14:22,660
took him to Isegila and ask him stuff happened, it's not really important, anyway let's

101
00:14:22,660 --> 00:14:28,980
get on to more of a lesson, some lesson here, but you know I mean I think the overall lesson

102
00:14:28,980 --> 00:14:39,460
is about the teaching, not the teacher right, it's easy to focus on person place and

103
00:14:39,460 --> 00:14:52,460
thing without focusing on our own wisdom and our own understanding, nobody else can lead

104
00:14:52,460 --> 00:15:00,820
us to enlightenment, it's kind of in general the attachment to form, you can attach to the

105
00:15:00,820 --> 00:15:07,700
form of meditation, say okay I have to do an hour of meditation or I have to do so many

106
00:15:07,700 --> 00:15:14,340
hours of meditation, without actually meditating, you know, you're so focused on the

107
00:15:14,340 --> 00:15:21,300
idea of the form that you forget the present moment, so it goes with all kinds of form,

108
00:15:21,300 --> 00:15:28,660
you can focus so much on a teacher or even on their words, you can like what they say,

109
00:15:28,660 --> 00:15:36,580
without actually keeping in mind putting into practice that kind of thing, I mean it's

110
00:15:36,580 --> 00:15:40,980
not to say that we all do this, but we have to be careful not to, people like walkily

111
00:15:40,980 --> 00:15:47,820
got into an extreme watching the Buddha not listening to his teachings, eventually he

112
00:15:47,820 --> 00:15:56,180
became enlightened, something about teachers and people as well, you know a lot of the

113
00:15:56,180 --> 00:16:01,820
Buddha's teaching is about, I think it was a couple of nights ago not being critical of

114
00:16:01,820 --> 00:16:09,500
others, you know looking at your own faults, sometimes we go to a monastery or meditation

115
00:16:09,500 --> 00:16:13,460
centre and all we can see are the faults of others, it's common, I mean a lot there's

116
00:16:13,460 --> 00:16:21,700
a lot of corruption in monastery, so it's not without its justification but it doesn't

117
00:16:21,700 --> 00:16:28,180
really help, on the other hand we sometimes put people up on a pedestal and when we

118
00:16:28,180 --> 00:16:36,180
find out they're human well, we lose, we become discouraged which you know we really shouldn't,

119
00:16:36,180 --> 00:16:44,140
we'll be focused on the teachings, very much more important, you can ask yourself where

120
00:16:44,140 --> 00:16:50,380
these teachings go ahead and the teachings are good, stick by them, see the dhamma, don't

121
00:16:50,380 --> 00:17:05,620
worry too much about seeing the Buddha, simple teaching, something to think about, so I can

122
00:17:05,620 --> 00:17:14,940
post the hangout, here I welcome, if anybody has any questions, I understand that I'm quite

123
00:17:14,940 --> 00:17:22,660
limiting my online presence and I think some people are complaining they want to ask questions

124
00:17:22,660 --> 00:17:29,660
and they can't come on here, I understand that, I just don't have a means of sort of filtering

125
00:17:29,660 --> 00:17:43,100
and I'm doing the best I can but I think you can't help everyone, so if you really want

126
00:17:43,100 --> 00:17:49,340
help from me you got to make the effort to come here or sign up for one of the slots

127
00:17:49,340 --> 00:18:01,220
for an online meditation course and actually do some meditating, anyway, I don't

128
00:18:01,220 --> 00:18:07,020
know if there are any questions, I'm happy to answer if you come on the hangout, otherwise

129
00:18:07,020 --> 00:18:21,420
it's going to get fairly hectic the next week and a half, next 20-30 years probably,

130
00:18:21,420 --> 00:18:28,340
yeah, next week and a half probably is going to be a bit rough, I may not be able to

131
00:18:28,340 --> 00:18:38,060
broadcast every night, okay, I'm going to say good night, good night everyone, thank

132
00:18:38,060 --> 00:18:57,780
you for coming out and meditating together, have a good night.

