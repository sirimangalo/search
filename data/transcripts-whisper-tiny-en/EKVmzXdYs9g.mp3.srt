1
00:00:00,000 --> 00:00:11,520
Good evening, everyone, and welcome to our live demo broadcast, and our nightly demo for

2
00:00:11,520 --> 00:00:17,880
those of you here in Hamilton.

3
00:00:17,880 --> 00:00:47,160
Today, I thought to talk about fear, and by it, and by it's fear, danger, and by it, and by

4
00:00:47,160 --> 00:01:02,320
fear, some of this terror, those of you who are still there to turn on the news, I'm sure

5
00:01:02,320 --> 00:01:32,120
are inundated by fear-mongering, we call terrorism, the intentional cultivation of fear in others.

6
00:01:32,120 --> 00:01:47,720
Sometimes there are other reasons for other goals, but quite often, it's quite common now

7
00:01:47,720 --> 00:02:01,920
to see the single-minded purpose of psychological warfare or terrorism.

8
00:02:17,720 --> 00:02:47,680
The first thing for us to understand about terror and terrorism is that being psychological

9
00:02:47,680 --> 00:02:58,920
warfare, of course, it relies upon the victim, it relies upon the vulnerability of the

10
00:02:58,920 --> 00:03:16,640
victim, and the sensitivity of the victim doesn't work against people who are not afraid.

11
00:03:16,640 --> 00:03:29,280
And obviously, it seems somewhat trite or simplistic to suggest that part of the solution

12
00:03:29,280 --> 00:03:37,520
is to not be afraid, but it's important to understand, it's really important to understand

13
00:03:37,520 --> 00:03:54,840
as a Buddhist, the mechanics of suffering, the mechanics of conflict, as we know, if you

14
00:03:54,840 --> 00:04:00,960
go deeply into the meditation practice, even physical pain is only true suffering when

15
00:04:00,960 --> 00:04:21,360
you let it upset you, even physical pain, even true direct physical violence is only truly

16
00:04:21,360 --> 00:04:34,000
successful when the victim is psychologically vulnerable, but terrorism, which seems to

17
00:04:34,000 --> 00:04:43,600
usually involve a lot of physical violence, it's conducted in such a way as to have the

18
00:04:43,600 --> 00:04:51,320
overwhelming collateral damage psychologically, so, of course, not to minimize the terrible

19
00:04:51,320 --> 00:05:07,200
tragedy of death, the physical pain that comes from these acts of terror, but to remark

20
00:05:07,200 --> 00:05:17,800
upon what makes them so effective, that's how it scares people.

21
00:05:17,800 --> 00:05:26,000
And of course, I don't think it's the solution, it's not the entire solution for us to

22
00:05:26,000 --> 00:05:36,160
stop being afraid of such things, but it is worth remarking that between being afraid

23
00:05:36,160 --> 00:05:49,200
of them and being not afraid of them, not being afraid of or frightened by acts of gross

24
00:05:49,200 --> 00:06:02,240
and horrific violence as far preferable to be unshaken, to be impervious.

25
00:06:02,240 --> 00:06:13,080
It's far preferable, not just for one's own self, but for one's ability to react properly

26
00:06:13,080 --> 00:06:24,640
and to consider the situation wisely.

27
00:06:24,640 --> 00:06:29,040
In the Buddhist time, I mean, I can't think of any, and there were no suicide bombers

28
00:06:29,040 --> 00:06:36,320
in the Buddhist time, there were no violent shootings, I can't think of any violent killings

29
00:06:36,320 --> 00:06:42,680
of innocent individuals simply for the purpose of cultivating terror or targeting a specific

30
00:06:42,680 --> 00:06:43,680
group.

31
00:06:43,680 --> 00:06:52,320
Imagine if I thought for a while I could come up with some, but there are specific acts

32
00:06:52,320 --> 00:07:04,240
of terrorism, mild acts that give you the general, I still give the general idea of

33
00:07:04,240 --> 00:07:12,920
responses and means of coping with or dealing with fear.

34
00:07:12,920 --> 00:07:16,840
First of all, but fear in general, we have the dudjika sutra, which is a very important

35
00:07:16,840 --> 00:07:23,280
sutra, really, not in terms of having any core value for leading one to enlightenment,

36
00:07:23,280 --> 00:07:31,080
but it's very important in creating Buddhist culture and creating a Buddhist outlook

37
00:07:31,080 --> 00:07:42,520
on life, as it deals directly with fear, and it deals directly with a triple gem, this

38
00:07:42,520 --> 00:07:48,760
is three powerful objects of reflection, the Buddha, the Dhamma and the Sangha.

39
00:07:48,760 --> 00:07:55,320
So the Buddha said, he talked about this war and he was telling a story about Indra, how

40
00:07:55,320 --> 00:08:01,040
Indra said, well, if you're afraid in battle on the angels fighting against the, whenever

41
00:08:01,040 --> 00:08:07,720
the other guys were, the non-angels as sura as they called them, fighting against them.

42
00:08:07,720 --> 00:08:14,960
He said, if you're afraid, look at my banner, look at the dudjika at the top of my

43
00:08:14,960 --> 00:08:19,400
banner, you'll see the flag, and when you see my standard there and you know that I haven't

44
00:08:19,400 --> 00:08:25,480
fallen, it will give you courage and your fear will disappear, and he said, if you don't

45
00:08:25,480 --> 00:08:32,200
look at mine, well, look at this, this general and that general and these God, look at

46
00:08:32,200 --> 00:08:43,280
their banners, and the Buddha said, he can say this all he wants, but truth is, they look

47
00:08:43,280 --> 00:08:50,960
at his banner, maybe the fear will disappear, maybe it won't, because Indra is not a very

48
00:08:50,960 --> 00:08:55,400
good role model, he's not himself free from fear, he's not someone that when you think

49
00:08:55,400 --> 00:09:02,400
of him, your mind is calm and you have a good example and a reminder of right and wrong,

50
00:09:02,400 --> 00:09:09,600
an example of a pure and unshaken individual that's not afraid of anything, to remind

51
00:09:09,600 --> 00:09:16,720
you that this is the best way, they said, but if you, I say to you monks, if you ever

52
00:09:16,720 --> 00:09:23,440
often the forest and you get afraid, or let's say those of us who are living in society

53
00:09:23,440 --> 00:09:29,280
and we're afraid, he said, think of the Buddha, and this is where we actually get these

54
00:09:29,280 --> 00:09:34,760
main chants, he said, think of all the qualities of the Buddha, and so if you ever hear

55
00:09:34,760 --> 00:09:39,960
Buddhist chant, think in the teravada tradition, in any country, they all recite these,

56
00:09:39,960 --> 00:09:49,800
they'd be so Bhagavada, or having some, some Buddha and so on, he said, or if you don't

57
00:09:49,800 --> 00:09:59,320
think about me, think about the Dhamma, and think about the Dhamma, think about the Sangha.

58
00:09:59,320 --> 00:10:06,400
So I mean, not to suggest that that's a solution to terrorism or it's a way for us to be

59
00:10:06,400 --> 00:10:16,200
free from fear of gross physical violence, but on the other hand, as Buddhists as Buddhist

60
00:10:16,200 --> 00:10:21,800
meditators, and even as people who are not Buddhists, but just as meditators, the idea

61
00:10:21,800 --> 00:10:33,480
of remembering the idea of recollecting yourself and remembering the path that we're

62
00:10:33,480 --> 00:10:38,920
on, remembering those who have trod the path, remembering the ones who have taught the

63
00:10:38,920 --> 00:10:49,680
path, remembering their greatness, their nobility, their freedom from fear gives us a good

64
00:10:49,680 --> 00:10:57,400
grounding and reminds us that fear is not useful, doesn't help us, it makes us a victim.

65
00:10:57,400 --> 00:11:07,320
And if we can free ourselves from our reactions, the really terrorism loses a lot of its

66
00:11:07,320 --> 00:11:13,840
strength, a lot of its power, so certainly it is part of the solution, I think.

67
00:11:13,840 --> 00:11:20,800
But this concept, I mean this is a key concept in Buddhism, the idea that reactions are

68
00:11:20,800 --> 00:11:30,720
the problem, it also applies to terrorists, those who create terror, they create terror

69
00:11:30,720 --> 00:11:41,880
because we have goals, we have ambitions, evil, goals, evil ambitions, or else evil means,

70
00:11:41,880 --> 00:11:49,280
evil intentions, evil minds, evil minds, states, we have this inside, you know, when a bully

71
00:11:49,280 --> 00:11:54,720
picks on someone weaker than them, when older siblings frighten their younger siblings

72
00:11:54,720 --> 00:12:00,720
when parents scare their children, yell at them, shout at them, raise their fists,

73
00:12:00,720 --> 00:12:09,640
or even hit them, all of this is terrorism, part of it is terrorism, sometimes it's just

74
00:12:09,640 --> 00:12:22,880
the desire to inflict pain, sometimes it's the desire to frighten.

75
00:12:22,880 --> 00:12:30,720
All of this is reaction as well, it's based on reaction, it's based on an inability

76
00:12:30,720 --> 00:12:40,840
to beat peace, so I mean really the solution, if one can call it that because there's

77
00:12:40,840 --> 00:12:50,880
no question then it's not likely to be successful not anytime soon, but the work that

78
00:12:50,880 --> 00:13:03,120
we do to fix and to solve these problems is to teach, so a lot of terrorism is based

79
00:13:03,120 --> 00:13:14,960
on antagonism, enmity towards between groups where the just groups is what we're seeing

80
00:13:14,960 --> 00:13:29,720
now we see Islam, Islam is not Islam, but Muslims are very angry and not just Muslims,

81
00:13:29,720 --> 00:13:40,640
something people from these countries, Muslim countries in general, very angry, angry at

82
00:13:40,640 --> 00:13:47,520
Christians, angry at Americans, there have been religious wars going back centuries,

83
00:13:47,520 --> 00:13:58,480
millennia maybe, angry against Jews, and then you have the other way, Americans are

84
00:13:58,480 --> 00:14:09,200
and many Europeans, Canadians have anger and antagonism towards Muslims, towards people who

85
00:14:09,200 --> 00:14:16,240
come from Muslim countries regardless of whether they're Muslim or not, so we have racism,

86
00:14:16,240 --> 00:14:25,320
we have whatever it is to be prejudiced against another person's religion, this is all

87
00:14:25,320 --> 00:14:35,920
reactions, right, we have going back generations, this bad blood, where we can't stop this

88
00:14:35,920 --> 00:14:44,320
cycle, people from needs with these religious or cultural ethnic backgrounds are fighting

89
00:14:44,320 --> 00:14:54,480
with each other, white against black, against brown, against red, yellow, Muslim, against

90
00:14:54,480 --> 00:15:12,600
Christian, against Jew, Buddhist, against Hindu, we haven't learned to just be, we haven't

91
00:15:12,600 --> 00:15:18,440
learned objectivity, we haven't learned to experience life without reacting, without

92
00:15:18,440 --> 00:15:37,560
judging, filling up these prejudices and these cruel intentions, the real solution, it bears

93
00:15:37,560 --> 00:15:46,200
repeating, that suffering comes from our reactions, not from our experiences, we can

94
00:15:46,200 --> 00:15:52,360
learn to just experience things as they are, we let go of them, we wouldn't cling, we'd

95
00:15:52,360 --> 00:16:02,600
fly away, we've all of our suffering behind, and then whatever happened, we have two other

96
00:16:02,600 --> 00:16:16,160
stories, the first one is about these monks who went off into the forest to practice

97
00:16:16,160 --> 00:16:29,120
meditation, and the angels up in the trees had to come down, they were Buddhist, I guess,

98
00:16:29,120 --> 00:16:33,440
they were, they were probably not Buddhist, but they would have been respectful towards

99
00:16:33,440 --> 00:16:38,000
recklessness, and when the monks went into the forest, the angels, oh we have to come down

100
00:16:38,000 --> 00:16:43,840
from the trees, they were tree angels or sprites or whatever, and they had to leave their

101
00:16:43,840 --> 00:16:48,760
homes up in the trees, because out of respect for the monks, kind of respect for these

102
00:16:48,760 --> 00:16:54,920
recklessness, I guess, some sort of, you know maybe it was because Indra had instituted

103
00:16:54,920 --> 00:17:02,400
from the high heavens, because he was Buddhist, maybe he had said well you have to, the

104
00:17:02,400 --> 00:17:06,920
monks go into the forest, it's a law in the angel world, maybe I don't know, but they

105
00:17:06,920 --> 00:17:12,080
came down and it kind of irked them, they weren't really happy about it, so they thought

106
00:17:12,080 --> 00:17:21,240
well, what can we do to get these monks to leave our area, and so all day and night they

107
00:17:21,240 --> 00:17:28,680
cultivated fear, they gave, they sent these visions to the monks of headless bodies and

108
00:17:28,680 --> 00:17:38,200
bodyless heads, and gruesome ghosts and apparitions of all sorts and sounds and so on, and

109
00:17:38,200 --> 00:17:42,000
the monks were unable to be, unable to focus, they were totally out of their minds,

110
00:17:42,000 --> 00:17:48,400
freaking out and they said we can't stay here, and so they went back to the Buddha, and

111
00:17:48,400 --> 00:17:54,200
the Buddha said to them oh well, first time you went to the forest you didn't have a weapon,

112
00:17:54,200 --> 00:18:00,880
a weapon to fight this terror, this terrorism, this is one, a good example of Buddhist

113
00:18:00,880 --> 00:18:10,240
terrorism, it's wild I know there was no, there was some sort of significant, can you imagine

114
00:18:10,240 --> 00:18:16,560
when it was being in the horror film that these terrible visions, they didn't know

115
00:18:16,560 --> 00:18:22,200
what was going on, they thought these were actually demons, maybe able to get them, and

116
00:18:22,200 --> 00:18:29,360
the Buddha said we need a weapon to fight this, and he taught them what we call the

117
00:18:29,360 --> 00:18:42,640
Kārṇīyamītāsita, Kārṇīyamītāsita, they said go back and as you walk into the forest

118
00:18:42,640 --> 00:18:51,660
chant this, the sūtā and loving Kārṇīyam, the kusaleyana, the tān, the tān, the pṛṣṇītā,

119
00:18:51,660 --> 00:19:05,460
Kārṇīyamītāsita, the hārṇīyam, the chanṇīyāsita, typical kusaleyanaMs, thefiction of his

120
00:19:05,460 --> 00:19:13,580
I have to chant it, and I don't know, it's a basic type of one to speak it up time, we all

121
00:19:13,580 --> 00:19:30,060
beings will be happy in their minds.

122
00:19:30,060 --> 00:19:41,620
Which speaks of one sort of conventional way of dealing with antagonism, dealing with

123
00:19:41,620 --> 00:19:42,620
enmity.

124
00:19:42,620 --> 00:19:53,220
A good way to change reactions is to apply the opposite and confronted by hate, reply with

125
00:19:53,220 --> 00:19:54,220
love.

126
00:19:54,220 --> 00:20:01,380
It's conventional, it's not a deep Buddhist teaching, but it is a Buddhist teaching.

127
00:20:01,380 --> 00:20:06,620
That when you supplant something with its opposite, you are able to change the course

128
00:20:06,620 --> 00:20:08,420
of events.

129
00:20:08,420 --> 00:20:13,660
It takes work, it takes effort, it's not something that's sustainable over the long term,

130
00:20:13,660 --> 00:20:20,540
it takes effort to constantly have a loving attitude when people are throwing hate at you.

131
00:20:20,540 --> 00:20:30,420
But it worked, these monks, what they did is they taught, it was a teaching, it wasn't

132
00:20:30,420 --> 00:20:40,380
just sending love to these angels, they were chanting it, and it was a reminder, a reminder

133
00:20:40,380 --> 00:20:49,380
of the suffering that comes from these bad intention, the intent to cause fear, in the best

134
00:20:49,380 --> 00:20:59,220
way we can, the best way we can help, we can defeat our enemies as to give them what

135
00:20:59,220 --> 00:21:08,260
is most precious, if you give them knowledge and wisdom, get them truths.

136
00:21:08,260 --> 00:21:18,700
The other example, that's a very small sort of insignificant example, but it speaks

137
00:21:18,700 --> 00:21:27,100
to the larger picture of the context of these acts, it's, I think there's more than one

138
00:21:27,100 --> 00:21:35,620
actually, there's examples of Mara, so the monks would be sitting in meditation and suddenly

139
00:21:35,620 --> 00:21:48,380
an ox would come along and walk up near where the bulls, their ceramic bulls were stacked.

140
00:21:48,380 --> 00:21:52,220
The monks would freak out, oh we have to get up, there's this big ox coming, it's going

141
00:21:52,220 --> 00:21:57,100
to break all our bulls, so they all got agitated and the Buddha said to them, that's not

142
00:21:57,100 --> 00:22:03,900
an ox, that's Mara, apparently this happens several times in different ways, Mara would

143
00:22:03,900 --> 00:22:10,980
do anything he couldn't to cause fear, and I only bring that insignificant example up,

144
00:22:10,980 --> 00:22:22,780
you know, it seems kind of a strange thing to happen, but to sort of think about what aspect

145
00:22:22,780 --> 00:22:32,060
of Samsara we're talking about here, fear is, is Mara still me, fear is part of this

146
00:22:32,060 --> 00:22:42,100
part of Samsara, those who delight in chaos, those who delight in suffering, there are

147
00:22:42,100 --> 00:22:56,140
angels, human beings who are bent on this, and just to be clear that we don't want to

148
00:22:56,140 --> 00:23:01,380
be one of those people, we don't want to be involved with that part of Samsara, it's

149
00:23:01,380 --> 00:23:06,220
a part of Samsara, that will probably always be around, probably not something that's ever

150
00:23:06,220 --> 00:23:18,260
going to disappear completely, it might end and then swell, but the universe is a big

151
00:23:18,260 --> 00:23:28,820
place, and so really our goal is to, well the Buddha said, keep your minds calm, keep

152
00:23:28,820 --> 00:23:39,580
your mind set, keep your mind subjective, so bring it up as well because it's really the

153
00:23:39,580 --> 00:23:45,900
instruction to meditate, a reminder to us that many things will come to disturb our state

154
00:23:45,900 --> 00:23:53,780
of mind, both the immeditation and out, and they're the problem, it's Mara, I think it's

155
00:23:53,780 --> 00:23:58,740
Mara, it's Satan, it's that part of Samsara that wants to pull us back in, that doesn't

156
00:23:58,740 --> 00:24:05,740
want to see us free, doesn't want to see us happy, doesn't want to bring it to allow us

157
00:24:05,740 --> 00:24:20,740
peace, so our practices too, free ourselves from the hooks, from the grasp, free ourselves

158
00:24:20,740 --> 00:24:31,180
from the vulnerability to these forces, forces of Mara, and to help others, in the best

159
00:24:31,180 --> 00:24:38,100
way we can help we can overcome terrorists, it's by freeing them from the need, helping

160
00:24:38,100 --> 00:24:44,820
them be free from this need to torture others and thereby harm themselves, corrupt their

161
00:24:44,820 --> 00:24:56,220
own like, sentence themselves to great suffering, there you go, just some thoughts on fear,

162
00:24:56,220 --> 00:25:04,740
terrorism, think of someone apropos, there's the demo for tonight, thank you all for

163
00:25:04,740 --> 00:25:19,740
tuning in.

