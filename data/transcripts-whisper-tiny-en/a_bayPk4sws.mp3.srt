1
00:00:00,000 --> 00:00:06,000
Why couldn't karma be explained away by probability?

2
00:00:06,000 --> 00:00:09,000
I don't know those questions that makes me think.

3
00:00:09,000 --> 00:00:14,000
Well, thanks a lot.

4
00:00:14,000 --> 00:00:16,000
Oh, I mean, it's a good question.

5
00:00:16,000 --> 00:00:20,000
These are the kind of questions that are very difficult to answer.

6
00:00:20,000 --> 00:00:32,000
You know, I'm a very, I'm just going to say, we are all very poor in the wisdom faculty in this day and age.

7
00:00:32,000 --> 00:00:35,000
So our ability to understand reality.

8
00:00:35,000 --> 00:00:45,000
The funny thing is that the funny thing about reality is you can, at the very core of your experience,

9
00:00:45,000 --> 00:00:49,000
misunderstand reality, you can totally see things wrong.

10
00:00:49,000 --> 00:00:56,000
You can actually experience something as pleasurable that is suffering.

11
00:00:56,000 --> 00:00:59,000
That you are actually suffering.

12
00:00:59,000 --> 00:01:09,000
You can be totally experiencing a displeasure and be clear in your mind that is pleasurable.

13
00:01:09,000 --> 00:01:16,000
Because you're not seeing clearly, because you're not looking carefully at it.

14
00:01:16,000 --> 00:01:21,000
But because you're not examining it.

15
00:01:21,000 --> 00:01:28,000
So as a result, many things seem clear to us.

16
00:01:28,000 --> 00:01:37,000
Things like Newtonian mechanics, Newtonian physics seem perfectly clear to us.

17
00:01:37,000 --> 00:01:41,000
It seems to be exactly the way the world works.

18
00:01:41,000 --> 00:01:46,000
Until what Einstein came along with him.

19
00:01:46,000 --> 00:01:54,000
And then Einstein himself rejected quantum physics when it came about.

20
00:01:54,000 --> 00:02:05,000
And now quantum physicists are rejecting the natural,

21
00:02:05,000 --> 00:02:15,000
the next step of the existence of the mind and the existence of intention.

22
00:02:15,000 --> 00:02:23,000
And there's only one really good quantum physicist that I know of.

23
00:02:23,000 --> 00:02:31,000
In the sense that he's outspoken and incredibly, just incredibly bold in the things that he says.

24
00:02:31,000 --> 00:02:36,000
Who makes a very, very persuasive argument in my mind.

25
00:02:36,000 --> 00:02:39,000
Of course, I'm not a quantum physicist.

26
00:02:39,000 --> 00:02:46,000
But he is, and he's an expert in something called Bell's Theorem.

27
00:02:46,000 --> 00:02:48,000
He's a known expert.

28
00:02:48,000 --> 00:02:58,000
And he's written many articles and explains how quantum physics

29
00:02:58,000 --> 00:03:19,000
doesn't take into account the mind and leaves explicitly in its orthodox

30
00:03:19,000 --> 00:03:21,000
in the original form.

31
00:03:21,000 --> 00:03:29,000
The Copenhagen, there's a Copenhagen, whatever it's called, orthodox quantum physics.

32
00:03:29,000 --> 00:03:36,000
That bore and Niels Bohr and Heisenberg and Pauli Wolfgang Pauli and so on.

33
00:03:36,000 --> 00:03:45,000
That they came up with explicitly leaves the mind out and says that the mind is not in the realm of quantum physics

34
00:03:45,000 --> 00:03:49,000
or not in the realm of quantum probability.

35
00:03:49,000 --> 00:03:56,000
So what it means is, according to this, an explanation of quantum physics

36
00:03:56,000 --> 00:04:01,000
or explanation of how things appear or how things work,

37
00:04:01,000 --> 00:04:05,000
how our observations work, require our observation.

38
00:04:05,000 --> 00:04:10,000
The point was that the quote from Niels Bohr paraphrase is something like,

39
00:04:10,000 --> 00:04:18,000
science is no longer an explanation of what is out there, of entities out there.

40
00:04:18,000 --> 00:04:25,000
It has now become an explanation of our observation, the human observation

41
00:04:25,000 --> 00:04:32,000
or the observer's observation, which presupposes the existence of the observer

42
00:04:32,000 --> 00:04:35,000
or the existence of an observation.

43
00:04:35,000 --> 00:04:44,000
So the observation itself collapses the quantum probability wave

44
00:04:44,000 --> 00:04:48,000
or something like that. I'm not a quantum physicist.

45
00:04:48,000 --> 00:04:52,000
And I can only understand the general concepts of it.

46
00:04:52,000 --> 00:04:56,000
But really, really recommend looking at this guy's articles

47
00:04:56,000 --> 00:05:00,000
because the wonderful thing as a terabyte of Buddhist that I like about him

48
00:05:00,000 --> 00:05:03,000
is he's orthodox. That's just while we take the,

49
00:05:03,000 --> 00:05:07,000
we don't go for these new fangled ideas that people come up with

50
00:05:07,000 --> 00:05:10,000
unless they're well based on facts.

51
00:05:10,000 --> 00:05:14,000
And nowadays, you have so many different interpretations of quantum physics

52
00:05:14,000 --> 00:05:22,000
and theories and so on that people have lost the very philosophical

53
00:05:22,000 --> 00:05:28,000
and kind of down to earth, realistic, reasonable,

54
00:05:28,000 --> 00:05:34,000
and brilliant, you might say, formations of people like

55
00:05:34,000 --> 00:05:42,000
Boren and Pauli and whoever I can remember all these.

56
00:05:42,000 --> 00:05:50,000
Guys who put it in this way where the mind was the beginning.

57
00:05:50,000 --> 00:05:54,000
The observation was the beginning of probability.

58
00:05:54,000 --> 00:05:58,000
You can't even talk about probability until you answer the question,

59
00:05:58,000 --> 00:06:04,000
what is the mind doing? What is the action of the mind?

60
00:06:04,000 --> 00:06:08,000
Which experiment in their vocabulary?

61
00:06:08,000 --> 00:06:13,000
Which experiment has the experiment decided to look at?

62
00:06:13,000 --> 00:06:16,000
Are they going to look at this slit or are they going to look at that slit?

63
00:06:16,000 --> 00:06:19,000
Because if they look at this slit, they're going to see it go through this slit.

64
00:06:19,000 --> 00:06:23,000
They look at that slit. They're going to see it go through that slit or something like that.

65
00:06:23,000 --> 00:06:29,000
No, this whole shorting is cat where he ridicules it

66
00:06:29,000 --> 00:06:33,000
and says that, well, then if you have this gun and so on,

67
00:06:33,000 --> 00:06:35,000
anyway, I'm not going to go into it all.

68
00:06:35,000 --> 00:06:40,000
But the point was that even Einstein totally rejected this.

69
00:06:40,000 --> 00:06:50,000
That there could be any idea of probability using the word probability.

70
00:06:50,000 --> 00:06:53,000
Any randomness, he said, God doesn't play dice.

71
00:06:53,000 --> 00:06:56,000
This is his famous quote.

72
00:06:56,000 --> 00:07:00,000
So we're even going further than that, right?

73
00:07:00,000 --> 00:07:08,000
And saying that there is actually something outside of even probability.

74
00:07:08,000 --> 00:07:14,000
Because really, probability is a bit of nonsense.

75
00:07:14,000 --> 00:07:23,000
And I think that's what Einstein was pointing to is that it is kind of nonsensical to talk about probability.

76
00:07:23,000 --> 00:07:29,000
Probability, I think, I mean, this is really a hardcore philosophical question,

77
00:07:29,000 --> 00:07:37,000
but it seems to me to be something that still presupposes Newtonian mechanics.

78
00:07:37,000 --> 00:07:49,000
It still presupposes some ability to calculate some dice or billiard balls

79
00:07:49,000 --> 00:07:55,000
for the lottery.

80
00:07:55,000 --> 00:08:03,000
And so I guess for that reason, it kind of scientists kind of latch onto this sort of thing.

81
00:08:03,000 --> 00:08:09,000
Yeah, well, probability, that's still something that we can get our minds around.

82
00:08:09,000 --> 00:08:21,000
And they're not able to, whereas the idea of a mind which you can't quantify,

83
00:08:21,000 --> 00:08:28,000
which you can't fit into an equation, which just drives them nuts.

84
00:08:28,000 --> 00:08:35,000
And it makes most of us, sets our head spinning.

85
00:08:35,000 --> 00:08:43,000
The whole idea of free will, for example, just totally impossible to fathom.

86
00:08:43,000 --> 00:08:46,000
How can you imagine such a thing?

87
00:08:46,000 --> 00:08:48,000
It's something that I've grappled with as well.

88
00:08:48,000 --> 00:08:53,000
You know, that's a questioner who has this analytical mind.

89
00:08:53,000 --> 00:09:01,000
So I'm going to get back to my own old dogma, or my own old spiel,

90
00:09:01,000 --> 00:09:04,000
which is that reality is experience-based.

91
00:09:04,000 --> 00:09:09,000
This is the root.

92
00:09:09,000 --> 00:09:11,000
It's not the ultimate conclusion.

93
00:09:11,000 --> 00:09:13,000
It's where you have to start.

94
00:09:13,000 --> 00:09:20,000
And this is the very powerful thing that quantum physics originally showed us.

95
00:09:20,000 --> 00:09:24,000
And people like Niels Bohr and the people around him.

96
00:09:24,000 --> 00:09:31,000
What they showed us was that this is where we have to start.

97
00:09:31,000 --> 00:09:35,000
We have to start at experience, at the mind.

98
00:09:35,000 --> 00:09:41,000
We can't begin to talk about the world around us, probability, and so on,

99
00:09:41,000 --> 00:09:46,000
until we take into account the actions of the mind,

100
00:09:46,000 --> 00:09:51,000
or we presuppose, or actually until we take the mind out of the equation,

101
00:09:51,000 --> 00:09:54,000
until we say the mind chose this.

102
00:09:54,000 --> 00:09:57,000
Once we say that, then we can, from a physics point of view,

103
00:09:57,000 --> 00:10:05,000
explain the physics, explain how the physical realm would react to that.

104
00:10:05,000 --> 00:10:12,000
And this is what this PhD, his name is Henry Stapp,

105
00:10:12,000 --> 00:10:16,000
STAPP, if you want to look him up on the internet, excellent.

106
00:10:16,000 --> 00:10:20,000
He's got some excellent articles on the page.

107
00:10:20,000 --> 00:10:26,000
And it seems like a brilliant and wise individual.

108
00:10:26,000 --> 00:10:32,000
He even goes so far, which may make people think less wise than I do,

109
00:10:32,000 --> 00:10:38,000
goes so far as to talk about how, to express the belief that

110
00:10:38,000 --> 00:10:46,000
or the understanding that quantum physics doesn't seem to negate the possibility of

111
00:10:46,000 --> 00:10:52,000
reincarnation, or the transmigration of the mind,

112
00:10:52,000 --> 00:10:56,000
or the continuation of the mind after death, let's put it there.

113
00:10:56,000 --> 00:11:04,000
So given an understanding, or, you know,

114
00:11:04,000 --> 00:11:09,000
my, you could say, believe, does not really believe

115
00:11:09,000 --> 00:11:14,000
my way of looking at the world that experience is reality.

116
00:11:18,000 --> 00:11:21,000
Probability doesn't mean anything.

117
00:11:21,000 --> 00:11:30,000
It doesn't have any relation to experience.

118
00:11:30,000 --> 00:11:34,000
Experience goes as it goes.

119
00:11:34,000 --> 00:11:41,000
There are decisions made, and there are results that come from those decisions.

120
00:11:41,000 --> 00:11:44,000
And this we call karma.

121
00:11:44,000 --> 00:11:50,000
It's actually, karma actually doesn't exist either.

122
00:11:50,000 --> 00:11:57,000
Karma is just an explanation of the natural laws of reality.

123
00:11:57,000 --> 00:12:02,000
You know, it's the scientific explanation of what is really real, which is experience.

124
00:12:02,000 --> 00:12:04,000
How does experience work?

125
00:12:04,000 --> 00:12:10,000
Well, it works in very, you could say, rigid,

126
00:12:10,000 --> 00:12:16,000
very rigid, you know, it works based on, based on very rigid laws of cause and effect.

127
00:12:16,000 --> 00:12:22,000
You can't do a deed based on, you know, stressful mind states,

128
00:12:22,000 --> 00:12:25,000
and not give, have it give rise to stress.

129
00:12:25,000 --> 00:12:32,000
So greed, anger, delusion, these cannot possibly give rise to happiness or any good things.

130
00:12:32,000 --> 00:12:37,000
They only have the, because of the very nature, they are stressful,

131
00:12:37,000 --> 00:12:44,000
they are unpleasant, they are unpeaceful, they are disturbing,

132
00:12:44,000 --> 00:12:48,000
the framework of reality.

133
00:12:48,000 --> 00:12:50,000
So as a result, they disturb.

134
00:12:50,000 --> 00:12:56,000
It's just like, you know, if you throw, if you throw a stone in a pool of water,

135
00:12:56,000 --> 00:12:59,000
there's nothing it can do except make ripples.

136
00:12:59,000 --> 00:13:02,000
That's, that's what karma means.

137
00:13:02,000 --> 00:13:04,000
Karma is cause and effect.

138
00:13:04,000 --> 00:13:07,000
So you're looking at, I would say from asking this question,

139
00:13:07,000 --> 00:13:10,000
you're looking at things from the wrong point of view.

140
00:13:10,000 --> 00:13:15,000
And you're looking at it from a point of view of Western science,

141
00:13:15,000 --> 00:13:22,000
that measures results and makes observations based on those results.

142
00:13:22,000 --> 00:13:26,000
This is not, doesn't have any basis on in reality.

143
00:13:26,000 --> 00:13:30,000
The reason why they do that is because they're dealing with physical external objects

144
00:13:30,000 --> 00:13:33,000
that don't really exist, that aren't real.

145
00:13:33,000 --> 00:13:36,000
They're just supposed to be out there somewhere.

146
00:13:36,000 --> 00:13:41,000
And as a result, they can't be experienced, and they don't have anything to do with experience.

147
00:13:41,000 --> 00:13:47,000
They don't have anything to do with reality, and therefore one has to simply measure the experiences

148
00:13:47,000 --> 00:13:54,000
that one gets and extrapolate those experiences to explain away those objects.

149
00:13:54,000 --> 00:14:00,000
So when you say karma versus probability, you know, thinking, you know,

150
00:14:00,000 --> 00:14:05,000
it's just a coincidence or it's a probability that these people, you know,

151
00:14:05,000 --> 00:14:07,000
get this result and those people get that result.

152
00:14:07,000 --> 00:14:13,000
Whereas karma isn't like that at all, karma just says our experiences change,

153
00:14:13,000 --> 00:14:15,000
our later experiences.

154
00:14:15,000 --> 00:14:19,000
And experience now will have an effect on experience later.

155
00:14:19,000 --> 00:14:25,000
The quality of a certain experience will affect the quality of a later experience.

156
00:14:25,000 --> 00:14:29,000
So I hope that helps.

157
00:14:29,000 --> 00:14:39,000
It probably doesn't totally answer the question, but it's a explanation.

