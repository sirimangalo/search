1
00:00:00,000 --> 00:00:24,360
In Buddhism we follow what is called the pithika, the three baskets, it's a large body of

2
00:00:24,360 --> 00:00:42,000
text. That is said to contain the teaching of the Buddha and his close disciples in what

3
00:00:42,000 --> 00:00:56,160
you can call the purest or the closest to original form. And so we can understand how

4
00:00:56,160 --> 00:01:09,680
important it is that our practice be in line with, if not in compass all, if not in compass

5
00:01:09,680 --> 00:01:18,200
all of the pithika, at least be in line with the pithika. And yet we see that often

6
00:01:18,200 --> 00:01:25,960
this isn't the case. Why this isn't the case is because most Buddhists don't bother

7
00:01:25,960 --> 00:01:39,800
to read the pithika. And for the most part, the most Buddhist will take word of mouth

8
00:01:39,800 --> 00:01:46,760
accounts of what is the Buddha's teaching, often relying on their teachers, even just relying

9
00:01:46,760 --> 00:01:57,920
on parents or friends or school teachers. But even among those people who have read the

10
00:01:57,920 --> 00:02:04,960
pithika, there are then those people who, the majority of people who have studied the

11
00:02:04,960 --> 00:02:12,880
pithika haven't practiced the Buddha's teaching have never put it into practice. The majority

12
00:02:12,880 --> 00:02:16,840
of people who practice the Buddha's teaching who actually come to meditate haven't

13
00:02:16,840 --> 00:02:29,200
read the scriptures, the pithika, or even large pieces of it. It's hard to find someone

14
00:02:29,200 --> 00:02:40,560
who has both studied the pithika in practice, meditation, Buddhist meditation, or made some

15
00:02:40,560 --> 00:02:55,920
attempt at practicing according to the pithika. And so we have kind of this quandary

16
00:02:55,920 --> 00:03:03,240
that we're dealing with many different ways of approaching Buddhism, often simply because

17
00:03:03,240 --> 00:03:10,520
of not reading me the text or not following texts, following someone who isn't following

18
00:03:10,520 --> 00:03:18,040
the text. And I tell you, if you haven't practiced meditation, the pithika have the

19
00:03:18,040 --> 00:03:26,400
texts of the pithika, they don't have much meaning, they won't have much significance

20
00:03:26,400 --> 00:03:35,680
in one's mind. They won't appear to be of any value or any use. They will appear dense

21
00:03:35,680 --> 00:03:41,200
and confusing. But the amazing thing about the Buddha's teaching is that the

22
00:03:41,200 --> 00:03:50,520
more you practice, the more alive and appealing and indeed wonderful and amazing, are

23
00:03:50,520 --> 00:03:58,440
the Buddha's teaching in the pithika, which is a sign both that the Buddha's teaching

24
00:03:58,440 --> 00:04:04,800
is the meditation practice is something very profound and that the teachings in the

25
00:04:04,800 --> 00:04:17,280
pithika are something very profound. And so it doesn't serve our purpose to rely on

26
00:04:17,280 --> 00:04:21,800
someone who has simply read the texts because for the most part, they don't understand

27
00:04:21,800 --> 00:04:28,480
them. They think they understand them and they understand the letter but not the meaning.

28
00:04:28,480 --> 00:04:41,520
And so they wind up applying it to very ordinary things, saying that the Buddha was a

29
00:04:41,520 --> 00:04:51,760
socialist or someone who was trying to revolutionize the social order or they say that

30
00:04:51,760 --> 00:05:02,200
the Buddha was teaching us how to live our lives in ordinary everyday sense and so on.

31
00:05:02,200 --> 00:05:10,600
And they won't ever get around to teaching profound subjects like insight meditation.

32
00:05:10,600 --> 00:05:19,320
On the other hand, if someone simply practices meditation, often without a teacher or perhaps

33
00:05:19,320 --> 00:05:33,560
with a teacher but in a formal setting, then it's quite likely that 90% of the time get

34
00:05:33,560 --> 00:05:40,360
off track. You could say even 100% of the time without really formal instruction if

35
00:05:40,360 --> 00:05:46,760
one hasn't read great amounts of the text. It would be very likely that they will get

36
00:05:46,760 --> 00:05:56,680
off track. But the curious thing here is that reading the text themselves without practicing

37
00:05:56,680 --> 00:06:07,720
is a difficult task. And so then if practicing without reading is a dangerous task or

38
00:06:07,720 --> 00:06:16,120
a dangerous practice, then how do we go about approaching this dilemma? How do we go about

39
00:06:16,120 --> 00:06:20,040
approaching the Buddha's teaching? And of course the answer is to have a teacher, to have

40
00:06:20,040 --> 00:06:26,360
someone who has done both. And so what we're relying on really is not just the text but it's

41
00:06:26,360 --> 00:06:33,640
the people who have carried the texts from the time of the Buddha and have practiced according

42
00:06:33,640 --> 00:06:41,640
to them. It's also possible to read the text for oneself and practice according to them but

43
00:06:41,640 --> 00:06:48,200
this takes a very special individual. There have been people who have done this in history when

44
00:06:48,200 --> 00:06:52,360
they didn't have a teacher or in a time when there were very few or maybe even no teachers,

45
00:06:53,480 --> 00:07:00,840
they were able to apply the texts according to their core message. But for the most part,

46
00:07:00,840 --> 00:07:06,040
people who do this simply get off track, get lost and don't know what to do next, don't know where

47
00:07:06,040 --> 00:07:14,200
to go. And so the most recommended path is to have a teacher who can explain the text.

48
00:07:17,000 --> 00:07:23,800
And at the same time it's important to say that there's no need in that case to actually read

49
00:07:23,800 --> 00:07:31,560
the texts themselves. Because what you'll find is that most of the texts were given at some point

50
00:07:31,560 --> 00:07:46,200
of the Buddha's 45 years as a Buddha to various a large, broad variety of audiences with

51
00:07:46,200 --> 00:07:54,200
different needs, different interests, different inclinations. And so he taught different things

52
00:07:54,200 --> 00:07:59,480
for different people. Now in this sense it's useful for saying a teacher or it's even useful for

53
00:07:59,480 --> 00:08:05,320
a meditator to see all the different approaches. But on the other hand, not all of the teachings

54
00:08:05,320 --> 00:08:12,760
will necessarily be suitable for an individual meditator at any given time. And so to read all of

55
00:08:12,760 --> 00:08:18,120
them, simply for the purposes of meditating is both overkill and perhaps even misleading because

56
00:08:19,480 --> 00:08:24,120
some of the texts might even seem to contradict each other having been given for different people

57
00:08:24,120 --> 00:08:34,840
at different times. But on the other hand, it's very important that our practice and the

58
00:08:34,840 --> 00:08:41,080
practice that our teacher gives to us is according to Natipitika. And so often there should be

59
00:08:41,080 --> 00:08:47,720
citations given where does this teaching come from? For instance, the four foundations of mindfulness.

60
00:08:47,720 --> 00:08:53,720
It's important to make clear that this isn't something we just made up or that was made up

61
00:08:54,280 --> 00:09:02,120
by a commentator or a disciple of the Buddha. It's actually the key practice that the Buddha

62
00:09:02,120 --> 00:09:12,120
recommended for his students mindfulness of the body, the feelings, the mind, and the dumbas.

63
00:09:14,600 --> 00:09:19,400
Of course, there are other practices that are in Natipitika and they're given for individual

64
00:09:19,400 --> 00:09:31,880
meditators. But by enlarged the key, you could say,

65
00:09:31,880 --> 00:09:35,640
the core practice of the Buddha is the four foundations of mindfulness and they made this clear

66
00:09:35,640 --> 00:09:40,440
in the great great discourse on the four foundations of mindfulness as well as the

67
00:09:40,440 --> 00:09:45,640
discourse on the four foundations of mindfulness. The same, almost the same discourses found in two

68
00:09:45,640 --> 00:09:54,920
places as well as a huge section of the large section of the Sanyuta Nikaya devoted directly

69
00:09:54,920 --> 00:10:00,280
to the four foundations of mindfulness. As far as meditation practice goes, the four foundations

70
00:10:00,280 --> 00:10:07,000
of mindfulness is the most. As far as I can think, it's the most often referred to,

71
00:10:08,040 --> 00:10:15,000
most widely taught. Of course, there are other teachings that are maybe more often referred to

72
00:10:15,000 --> 00:10:22,920
such as the teachings on the six senses or the five aggregates or the eight full noble path

73
00:10:22,920 --> 00:10:27,640
or the four noble truths. But none of those are specifically meditation practices.

74
00:10:27,640 --> 00:10:34,520
So when we do our meditation, we can be rest assured that the four foundations of mindfulness,

75
00:10:34,520 --> 00:10:40,600
mindfulness of the body, for instance, when we say rising, falling of the abdomen or walking,

76
00:10:40,600 --> 00:10:46,120
stepping rise, stepping left. That we're practicing according to the Buddha's teaching. When we

77
00:10:47,160 --> 00:10:53,000
pay attention to the feelings, pain or 18 or soreness, happy feelings,

78
00:10:53,000 --> 00:10:59,720
neutral feelings, saying to ourselves, pain, pain or happy, happy or calm, that we're

79
00:10:59,720 --> 00:11:05,000
practicing according to the Buddha's teaching. When we watch the mind, thoughts, good thoughts,

80
00:11:05,000 --> 00:11:14,760
bad thoughts, past thoughts, good thoughts, then we're paying attention to the mind,

81
00:11:14,760 --> 00:11:20,680
we are practicing according to the Buddha's teaching. Or when we focus on the many dumbness,

82
00:11:20,680 --> 00:11:33,240
the many groups of phenomena or of mind states or of qualities of mind or specific teachings

83
00:11:33,240 --> 00:11:40,680
that the Buddha gave, when we're mindful of these realities. For instance, the 500 says, when

84
00:11:40,680 --> 00:11:46,120
we say liking, liking or disliking, disliking, drowsy, drowsy, distracted, doubting.

85
00:11:46,120 --> 00:11:52,760
When we acknowledge these, we can see that we're practicing according to the Buddha's teaching,

86
00:11:52,760 --> 00:11:58,360
as the Buddha said. When one of these things is present, we know this is present,

87
00:11:58,360 --> 00:12:03,880
or when we're walking, we know walking, when we're sitting, we know we're sitting,

88
00:12:05,080 --> 00:12:10,760
we know to ourselves, I am sitting, I am walking, or simply put in English walking, walking,

89
00:12:10,760 --> 00:12:18,440
sitting, sitting. When there's a painful feeling, present one knows there is a painful feeling,

90
00:12:18,440 --> 00:12:28,280
present or simply pain, pain, and so on. So this is one thing we can rest assured. But the thing

91
00:12:28,280 --> 00:12:36,360
I wanted to talk about tonight, besides simply giving this sort of idea, is to give a core,

92
00:12:36,360 --> 00:12:46,360
sort of a core overview of the tepitika. As we have this huge body of texts, three baskets of

93
00:12:46,360 --> 00:12:58,920
84,000 teachings are somewhere in that number. How do we make sense of this song? How do we

94
00:12:58,920 --> 00:13:03,160
understand, what is it that the Buddha taught? Okay, so four foundations of mindfulness is

95
00:13:03,160 --> 00:13:09,720
somewhere near the core, but what exactly is the core? How do you sum up the Buddha's teaching?

96
00:13:10,360 --> 00:13:14,280
And there are many ways to do this. I'm only going to talk about one, and I'm going to give

97
00:13:14,280 --> 00:13:20,280
what is the accepted summary. You could summarize the Buddha's teaching in the four noble truths,

98
00:13:20,280 --> 00:13:23,720
because really everything the Buddha taught was based on the four noble truths.

99
00:13:25,320 --> 00:13:30,440
But there's another way of summing up the Buddha's teaching that is much more practical,

100
00:13:30,440 --> 00:13:36,680
not to say that the four noble truths are not a very important teaching, but on a practical

101
00:13:36,680 --> 00:13:41,400
level. How do we put the four noble truths into practice? How do we put the Buddha's teaching into

102
00:13:41,400 --> 00:13:47,080
practice? And for this, we looked at the Buddha's last words, because before the Buddha passed away,

103
00:13:47,720 --> 00:13:55,240
he said, upper-matin sampad-hata, which is variously translated as something like

104
00:13:55,240 --> 00:14:04,440
strive on with diligence, or strive on with heedfulness.

105
00:14:07,720 --> 00:14:12,440
And this was all he said. Before this, he said that all formations are subject to cease.

106
00:14:13,000 --> 00:14:18,600
Everything is subject to cease, which was basically using himself as an example, that even he passes away,

107
00:14:18,600 --> 00:14:23,640
even Buddha's pass away, that everything is subject to cease. Everything that arises

108
00:14:23,640 --> 00:14:32,360
is subject to cease. Then he said, upper-matin sampad-hata, two words, which is strive on or

109
00:14:33,560 --> 00:14:41,240
actually a literal translation would be fill yourself up with, or come to full attainment of

110
00:14:41,240 --> 00:14:49,960
heedfulness, upper-mata, which is the opposite of bhamada, which means intoxication or heedlessness

111
00:14:49,960 --> 00:14:57,240
or negligent. So being this idea, being heedful, in one sense, being mindful, and I'll get to

112
00:14:57,240 --> 00:15:02,760
that in a second, because actually the two are very much related. But why we know this is the core

113
00:15:02,760 --> 00:15:08,920
of the Buddha's teaching? Well, besides it being his last words, and therefore having great

114
00:15:08,920 --> 00:15:17,640
significance, we have the commentators appraisal of the tepitika in regards to this teaching of

115
00:15:17,640 --> 00:15:26,200
upper-mata. And so the commentators says that when you boil down all of the Buddha's teachings,

116
00:15:26,200 --> 00:15:33,400
all of the tepitikas in their entirety, sacalampi, he tepitikas, the whole entirety of the

117
00:15:33,400 --> 00:15:38,360
three tepitikas, the three baskets, which are the Buddha, what's in the Buddha's teaching?

118
00:15:38,360 --> 00:15:48,040
When you boil it down, when you compact it down, it is all for the purpose of pointing out

119
00:15:48,680 --> 00:15:54,920
the Buddha's called upper-mata, bhamada, bhamada, bhamada, bhamada, eva ota-dhi.

120
00:15:54,920 --> 00:16:09,320
It boils down to the path of heedfulness, the path of diligence, the path of mindfulness,

121
00:16:09,320 --> 00:16:16,920
if you will. Now why we can say the path of mindfulness is because of what I'd like to talk to,

122
00:16:16,920 --> 00:16:24,040
to talk about them, exactly what the Buddha said is upper-mata. And there are four things that

123
00:16:24,040 --> 00:16:28,200
are upper-mata. This is something that I think is important to talk about,

124
00:16:28,200 --> 00:16:32,760
important to let everyone know, what is the state of mind that we're trying to attain?

125
00:16:33,800 --> 00:16:38,360
How should we approach our meditation? We meditate what is important to keep in mind.

126
00:16:39,080 --> 00:16:44,840
And if we keep in mind these four principles, then we have an understanding of what state of

127
00:16:44,840 --> 00:16:49,480
mind we're looking for. Of course, we're still practicing the four foundations of mindfulness. But

128
00:16:49,480 --> 00:16:55,480
here we have four things that are sort of a way of keeping ourselves in check.

129
00:17:00,520 --> 00:17:10,440
The first one is a quote, a quote, a no, a payap, a no, sorry, taking a different set. A payap, a no,

130
00:17:10,440 --> 00:17:16,280
which is basically means a quote, a no, it's the same thing. It means not having freedom from

131
00:17:16,280 --> 00:17:24,360
ill will, not wishing ill of others. Basically means not being angry, not getting angry.

132
00:17:25,480 --> 00:17:31,560
A payap, a no, a sadha, sadha, sadha, sadha means always being mindful.

133
00:17:32,920 --> 00:17:34,360
So mindfulness is in there.

134
00:17:36,680 --> 00:17:44,680
A chattang sousamahito, number three, which means being internally composed,

135
00:17:44,680 --> 00:17:49,880
being sort of, you could say, level-headed or composed, having an internal

136
00:17:51,720 --> 00:17:54,440
composition or being well-composed internally.

137
00:17:57,080 --> 00:17:59,560
And then number four, a bita, when they see kam.

138
00:18:03,000 --> 00:18:12,840
Training oneself to overcome or to leave behind greed, to leave behind ones clinging or craving.

139
00:18:12,840 --> 00:18:21,800
And then some of the sadha, a payap, a man to a team, which have in the easy kam, a payap, a man to a

140
00:18:21,800 --> 00:18:27,080
team, which a team. This is called a payap, a man to a team. This is called a language.

141
00:18:28,040 --> 00:18:30,120
Just a chance, non-linguagence.

142
00:18:33,400 --> 00:18:38,280
So these four things are sort of a very important teaching that we should keep in mind.

143
00:18:38,280 --> 00:18:46,040
A payap, a no, is we should be free from any kind of anger towards other being.

144
00:18:46,680 --> 00:18:50,520
So when we're practicing this is something that we're working on and anger towards not just

145
00:18:50,520 --> 00:18:55,160
beings, but all things in general. This is something that we should work out in the very beginning

146
00:18:55,160 --> 00:19:11,000
and try to give up our resentment and our frustrations and our intolerance of unpleasant situations

147
00:19:11,000 --> 00:19:16,760
or unpleasant individuals and unpleasant interactions that we should try to

148
00:19:16,760 --> 00:19:27,080
free our minds from this anger, hatred or frustration, even things like boredom or sadness.

149
00:19:28,360 --> 00:19:31,960
The Buddha said this is one thing you can kill and sleep soundly.

150
00:19:33,960 --> 00:19:38,200
There was one time a Brahman came to the Buddha. He was very angry because his wife was Buddhist

151
00:19:38,200 --> 00:19:45,640
and she was making a nuisance of herself at home when he had Brahman guests over. She would say,

152
00:19:46,840 --> 00:19:53,320
say, the Buddha's name and so on. When she tripped and fell, she would say,

153
00:19:53,320 --> 00:19:59,080
oh Buddha, oh Buddha helped me and said, oh my god, oh my Buddha.

154
00:20:00,920 --> 00:20:06,040
And so her guests would get upset and leave. And so he went to see the Buddha and got angry

155
00:20:06,040 --> 00:20:15,640
and you said to the Buddha, what can you kill and rest at peace? What would you kill if you

156
00:20:15,640 --> 00:20:19,640
wanted to rest at peace? Basically, what he's saying is he's ready to beat the Buddha.

157
00:20:23,160 --> 00:20:29,400
And the Buddha, so he said, ting katwag, sukang sati, having killed what? Can you sleep at ease?

158
00:20:29,400 --> 00:20:39,000
The Buddha said, kotang katwag, sukang sati. He threw it back in his face. He said, by killing anger,

159
00:20:40,840 --> 00:20:46,840
it's what you can kill. Anger is what you can kill and live in peace, rest at peace.

160
00:20:49,720 --> 00:20:55,480
And then he asked, kotang katwag, he said, ting katwag, so ting katwag, having killed what?

161
00:20:55,480 --> 00:21:06,680
Will you not be remorseful? And the Buddha said the same thing, kotang katwag, sukhati,

162
00:21:07,720 --> 00:21:19,000
when you destroy anger, you don't regret. It's the one thing that you can kill, it's the one

163
00:21:19,000 --> 00:21:23,960
thing that you can destroy. Normally we think of destroying those things that make us angry.

164
00:21:23,960 --> 00:21:30,440
The Buddha said, really, it's not possible, it's not the way of peace and happiness.

165
00:21:30,440 --> 00:21:37,160
You have to kill this, it's inside of yourself, which is the anger, the upset about your situation.

166
00:21:38,600 --> 00:21:42,600
This is the first one. The second one is satasat, though, this one we already understand.

167
00:21:42,600 --> 00:21:47,320
Sata means always, sato means one with mindfulness, always being one who is mindful.

168
00:21:47,320 --> 00:21:54,920
And you could say that this is, as I said, mindfulness and apamada are very closely related.

169
00:21:54,920 --> 00:22:05,160
So there's another point where the Buddha said, said, sapphire, a wipawasa, apamato ti wuchati,

170
00:22:06,680 --> 00:22:14,360
being always, never without, never being, without mindfulness. This is called apamada.

171
00:22:14,360 --> 00:22:21,400
So the constant mindfulness, where we're constantly aware when we're walking, walking,

172
00:22:21,400 --> 00:22:26,360
when we're sitting, sitting, when we're singing, we're singing, singing, hearing, hearing,

173
00:22:26,360 --> 00:22:31,000
we're clearly aware of everything, peace by peace as it comes one by one, of everything.

174
00:22:31,800 --> 00:22:36,040
So here we are sitting here and there's many things going on. And you can see that

175
00:22:36,040 --> 00:22:44,920
things arising and ceasing coming and going, pain and aching of boredom or so on, thoughts coming.

176
00:22:45,800 --> 00:22:49,960
And so grabbing all of these things and catching them one by one in their attention and just seeing

177
00:22:49,960 --> 00:22:57,880
them clearly, coming to see everything as it comes and goes clearly, knowing it for what it is,

178
00:22:57,880 --> 00:23:05,400
this is mindful. This is the state of constant mindfulness, where we're aware again and again

179
00:23:05,400 --> 00:23:10,600
where we send our mind out again and again to mote, to know every moment that's going on in that

180
00:23:10,600 --> 00:23:16,680
moment, to see it clearly for what it is. This simple descent is called apamada.

181
00:23:20,680 --> 00:23:28,360
This is number two. Number three, it's a dang sussamahito means to keep our sounds, keep our cool,

182
00:23:28,360 --> 00:23:33,800
you could say, to be internally composed. This is the feeling that you get when you really

183
00:23:33,800 --> 00:23:40,280
are meditating. When you've been practicing for some days, even a week, two weeks, and suddenly you

184
00:23:40,280 --> 00:23:50,520
start to settle in and you find your balance. In the beginning, it's very tumultuous.

185
00:23:51,320 --> 00:23:59,400
It's very turbulent. There's good things coming and so we like them. There's bad things coming

186
00:23:59,400 --> 00:24:05,240
and so we run away from them and we find ourselves back and forth. Our meditation is not

187
00:24:06,280 --> 00:24:10,920
quiet. It's not stable. It's not happy. It's not peaceful.

188
00:24:13,720 --> 00:24:20,200
And as we continue, we start to work these first-the-course defilements and then slowly the

189
00:24:20,200 --> 00:24:25,400
more subtle defilements until we find ourselves in this sort of balanced state of mind where

190
00:24:25,400 --> 00:24:30,040
good things don't affect us and bad things don't affect us. In fact, we fail to see things

191
00:24:30,040 --> 00:24:36,200
as good or bad. We simply see them from what they are coming and going. We're able to live as it

192
00:24:36,200 --> 00:24:44,600
were on the waves. As they go up and down, we feel like we're going, we're smooth riding.

193
00:24:45,320 --> 00:24:47,640
This is the state of meditation. Sort of what we're aiming for.

194
00:24:47,640 --> 00:24:54,440
To be impartial. To have a balanced state of mind.

195
00:24:55,960 --> 00:25:00,120
This is number three. And number four is the training to overcome

196
00:25:01,640 --> 00:25:04,600
greed, which is of course the opposite of anger. So they actually go together.

197
00:25:05,400 --> 00:25:11,800
But the Buddha placed emphasis here that the idea is not to not be greedy. It's to train ourselves

198
00:25:11,800 --> 00:25:16,520
out of it because really that's the core of the training. Anger is something, it's actually quite

199
00:25:16,520 --> 00:25:21,560
easy to see that it's a bad thing. Much easier to see that anger is a bad thing. But it's our

200
00:25:21,560 --> 00:25:26,760
greed that we really have to work on and go deeper on and really come to see and to give up our

201
00:25:26,760 --> 00:25:32,920
clinging to everything. To give up all of our wants and all of our needs. To not be

202
00:25:35,800 --> 00:25:43,320
in discontent at all times. To actually be content with reality as it is, however it is.

203
00:25:43,320 --> 00:25:50,440
And to not become attached to it. So that when it changes we can also accept the changes.

204
00:25:51,320 --> 00:25:56,920
To simply be aware of things as they are. And this takes a training, seek cognizant where the

205
00:25:56,920 --> 00:26:01,880
Buddha is. And here we're training to look at things. We're examining the things of the objects

206
00:26:01,880 --> 00:26:07,240
of our desire. So it's opposed to when we know we're addicted to something running away from it.

207
00:26:07,240 --> 00:26:15,400
We examine the process of addiction. In some cases this does require us to leave behind the

208
00:26:15,400 --> 00:26:23,000
object of addiction for instance with drugs or alcohol. But in most cases we can actually at the

209
00:26:23,000 --> 00:26:33,800
moment of partaking in some simple addiction like games or television or people or sensuality

210
00:26:33,800 --> 00:26:41,000
food, for instance, junk food. A good example. We can just watch ourselves as we're

211
00:26:41,000 --> 00:26:47,000
partaking and slowly, slowly we nurse ourselves off of it. Slowly so you realize that this is

212
00:26:47,000 --> 00:26:53,000
a process of suffering, this addiction, that there's no need to be addicted to these things.

213
00:26:53,000 --> 00:26:57,080
And then it's actually bringing us great stress and suffering. The little bit of happiness

214
00:26:57,080 --> 00:27:04,440
that we get is actually totally removed from the addiction itself. And so even though there may

215
00:27:04,440 --> 00:27:11,160
be some amount of happiness in partaking in these things, it comes not because we're addicted,

216
00:27:11,160 --> 00:27:20,040
but simply because we're overtaking. And because of how small an amount of happiness it is,

217
00:27:20,600 --> 00:27:25,160
we can see that being addicted to these things is actually a great amount of suffering. When we

218
00:27:25,160 --> 00:27:31,720
don't get what we want, we suffer. So we learn to just live with things as they are and we learn

219
00:27:31,720 --> 00:27:40,680
to find happiness and peace in all things of all types and from all sides of the spectrum of reality.

220
00:27:42,760 --> 00:27:50,200
So in a basic sense we're talking about a state of mindfulness and observation of good and bad

221
00:27:50,200 --> 00:27:56,440
phenomena as they arise. When bad things arise learning to not get angry at them and not see them

222
00:27:56,440 --> 00:28:03,160
as bad. To give up this idea that certain phenomena are bad or unpleasant. And to broaden our

223
00:28:03,160 --> 00:28:09,560
horizons and get out of this idea of being a human being stuck to this one birth and so on.

224
00:28:09,560 --> 00:28:17,640
To start to see reality as an experience or a series of experiences of seeing hearing,

225
00:28:17,640 --> 00:28:26,600
smelling, tasting, feeling, thinking. A whole flux of experience and to see that it comes and it

226
00:28:26,600 --> 00:28:33,400
goes and to not become attached to it. To be like a bird flying as we fly through this stream of

227
00:28:33,400 --> 00:28:40,920
experience and to really live. This is something where when you understand and you come to

228
00:28:40,920 --> 00:28:50,760
attain this state or progress along the path that you start to remind starts to become freed up

229
00:28:50,760 --> 00:28:57,560
and you feel like it's like a bird flying not clinging to anything. And so this is sort of

230
00:28:57,560 --> 00:29:03,320
what we're aiming for and this is what I hope to impress upon everyone. Actually when you look at

231
00:29:03,320 --> 00:29:11,960
it it's a very simple thing to do. All of the Buddha's teaching are in the end for the purpose

232
00:29:11,960 --> 00:29:16,760
of following this path. For the purpose of bringing people to follow this path, this wave

233
00:29:17,960 --> 00:29:24,920
constant alert and alert, attention and awareness of reality as it comes and goes,

234
00:29:24,920 --> 00:29:32,360
as it arises and ceases through the tool or the process or the practice of mindfulness.

235
00:29:34,040 --> 00:29:37,400
So that in the end we can come to see things clearly and not be

236
00:29:39,320 --> 00:29:44,680
diluted or confused by them. So that's the number that I thought I would give today and now of

237
00:29:44,680 --> 00:29:52,040
course comes the time where we put it into practice. We start to watch the reality as it comes

238
00:29:52,040 --> 00:29:56,520
and we should be mindful for a moment to moment to moment to our best of our ability.

239
00:29:56,520 --> 00:30:26,360
So first we'll do the mindful frustration and then mocking and then sitting.

