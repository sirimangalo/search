1
00:00:00,000 --> 00:00:17,000
Okay, good evening. We should now be broadcasting live with a new microphone live from second life

2
00:00:21,000 --> 00:00:23,000
and Hamilton Ontario

3
00:00:23,000 --> 00:00:32,000
So we got in a car accident today.

4
00:00:36,000 --> 00:00:40,000
We were hit. I was in a van coming back.

5
00:00:41,000 --> 00:00:46,000
Today I went to Mississauga for the Katina Donna.

6
00:00:46,000 --> 00:00:51,000
Katina said

7
00:01:06,000 --> 00:01:12,000
And I was asked to give a short talk on

8
00:01:12,000 --> 00:01:14,000
I thought about

9
00:01:16,000 --> 00:01:18,000
Katina or whatever.

10
00:01:19,000 --> 00:01:20,000
It said 10 minutes.

11
00:01:20,000 --> 00:01:22,000
So I thought about, okay, how can I

12
00:01:23,000 --> 00:01:25,000
What can I talk about in 10 minutes?

13
00:01:26,000 --> 00:01:28,000
And then right before I get to talk, they said

14
00:01:29,000 --> 00:01:33,000
And now I think of the English version by Yuta Damu

15
00:01:34,000 --> 00:01:36,000
Maybe seven minutes.

16
00:01:36,000 --> 00:01:41,000
So I have to give a talk in seven minutes.

17
00:01:42,000 --> 00:01:47,000
Which I guess I've gotten better at doing a lot of necessity

18
00:01:48,000 --> 00:01:53,000
because everything gets started late and they try to do everything right before the monks have to eat.

19
00:01:53,000 --> 00:02:03,000
Does this sound quiet?

20
00:02:06,000 --> 00:02:08,000
Someone says the sound is quiet.

21
00:02:09,000 --> 00:02:11,000
Maybe better now.

22
00:02:11,000 --> 00:02:16,000
Hmm.

23
00:02:18,000 --> 00:02:21,000
Right.

24
00:02:23,000 --> 00:02:26,000
All these levels to be adjusted.

25
00:02:26,000 --> 00:02:30,000
Doesn't make much sense to me, but

26
00:02:31,000 --> 00:02:34,000
Better is better.

27
00:02:34,000 --> 00:02:43,000
So yeah, I give a seven minute talk on, I talked about the Katina Pinkham, the Katina Donna.

28
00:02:44,000 --> 00:02:49,000
Katina is a symbol of harmony.

29
00:02:50,000 --> 00:02:51,000
It's kind of meaningless.

30
00:02:51,000 --> 00:02:54,000
So it's kind of insignificant, technically speaking,

31
00:02:54,000 --> 00:02:59,000
because all you're doing is showing one robe.

32
00:02:59,000 --> 00:03:07,000
That's all Katina is just the sewing of a single robe.

33
00:03:10,000 --> 00:03:22,000
But the way it's set up, it was established after there was a group of monks who were traveling by foot, of course.

34
00:03:22,000 --> 00:03:28,000
And they were doing their best to get to the Buddha by the rains, by the beginning of the rains.

35
00:03:28,000 --> 00:03:30,000
And of course that would have been a big thing.

36
00:03:30,000 --> 00:03:33,000
Lots of monks would want to spend the rains with the Buddha.

37
00:03:33,000 --> 00:03:41,000
But these monks were unable to make it all the way to a bit of a sting and the rains started early.

38
00:03:42,000 --> 00:03:49,000
It was hard for them to travel and then the day of the enter into the rains, which we came and they hadn't made it all the way to the Buddha's monastery.

39
00:03:49,000 --> 00:03:59,000
So they were forced to, instead, stay some distance from the Buddha.

40
00:03:59,000 --> 00:04:09,000
They spent three months there and after three months, the day of the exiting of the rains, they rushed to the Buddha to see him.

41
00:04:10,000 --> 00:04:15,000
So in the Buddha, actually I think there's different interpretations as to why he did it.

42
00:04:15,000 --> 00:04:28,000
But for whatever reason, we saw this as an opportunity to reason to create this symbolic gesture at the end of the rains.

43
00:04:31,000 --> 00:04:43,000
The monks should all gather together to express their harmony, to celebrate and to show their harmony.

44
00:04:43,000 --> 00:04:46,000
It was somewhat of a test actually.

45
00:04:46,000 --> 00:04:48,000
The test on two different levels.

46
00:04:48,000 --> 00:04:58,000
It's a test of the monk's ability to inspire support from the laypeople to the extent that they give them robes cloth.

47
00:04:58,000 --> 00:05:03,000
As it's required to have gotten robes cloth without asking.

48
00:05:04,000 --> 00:05:08,000
And then it's a symbol of the ability of the monks to work together.

49
00:05:08,000 --> 00:05:12,000
They haven't killed each other by the end of the rains.

50
00:05:12,000 --> 00:05:14,000
They're still talking to each other.

51
00:05:14,000 --> 00:05:17,000
It's an encouragement to harmony.

52
00:05:17,000 --> 00:05:23,000
They have to work together to sew the continuous robe.

53
00:05:24,000 --> 00:05:30,000
They have to decide which monk is going to receive the robe and they have to work together to sew it.

54
00:05:31,000 --> 00:05:34,000
It takes at least five monks to do it.

55
00:05:34,000 --> 00:05:39,000
And five monks for it to succeed.

56
00:05:41,000 --> 00:05:56,000
As I talked a little bit about how great it was there for it and why it's probably the most important holiday in the Buddhist year.

57
00:05:56,000 --> 00:06:07,000
But it symbolizes, it symbolizes a harmonious practice of Buddhism and it's a real gift giving ceremony.

58
00:06:07,000 --> 00:06:21,000
It's a chance for everyone to express their intention towards goodness.

59
00:06:21,000 --> 00:06:29,000
And then I went over the four protective meditations.

60
00:06:29,000 --> 00:06:33,000
I said, if we're going to do good deeds, it's such a good thing that we're doing.

61
00:06:33,000 --> 00:06:37,000
But if we're going to do good deeds, you have to start with the mind.

62
00:06:37,000 --> 00:06:44,000
So it's actually common in Buddhist thought that before you do a good deed, you should meditate.

63
00:06:44,000 --> 00:06:51,000
And so before we practice in the two minutes, we have left here.

64
00:06:51,000 --> 00:06:53,000
We shouldn't take up some meditations.

65
00:06:53,000 --> 00:06:58,000
I probably went over, I probably ended up talking for about ten minutes.

66
00:06:58,000 --> 00:07:04,000
And so I had everyone close their eyes and I went through it very briefly, went through the four before protective meditations with them.

67
00:07:04,000 --> 00:07:07,000
And so we have to think about the Buddha.

68
00:07:07,000 --> 00:07:16,000
While we're doing this, it has to do with the Buddha sassana, our respect and our reverence for the Buddha and his teachings.

69
00:07:16,000 --> 00:07:26,000
We think of the Buddha our mind because purely remember why we're doing this, why we're offering, why we're giving a gift, why we're meditating, why we're doing a good thing.

70
00:07:26,000 --> 00:07:30,000
I'm doing it because this is the way of the Buddha.

71
00:07:30,000 --> 00:07:36,000
We have this reassurance that the way that we practice is the way of the enlightened ones.

