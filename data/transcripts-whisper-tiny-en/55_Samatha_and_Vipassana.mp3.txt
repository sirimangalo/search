other types of meditation, kune, samata and vipassana.
Question, I started with samata meditation as I read a lot that you have to call me a mind before
starting to practice vipassana. What is your interpretation of the differences between these two
and which should be practiced first? Answer, samata means tranquility, vipassana means seeing clearly.
The Buddha used these terms together and individually as qualities of mind.
When you talk about samata meditation as something separate from vipassana meditation,
you are taking the discussion into a post-canonical realm. You are taking it out of the realm of what
the Buddha actually said and into the realm of interpretation. I follow the orthodox interpretation
that does talk about samata meditation and vipassana meditation, so I do not mind talking about this.
For those people who advise to practice coming your mind first, samata meditation and then practicing
to see clearly after vipassana meditation, this means first entering into a trance wherein you can see
one thing very clearly. This is only possible if that one thing is a concept. Since concepts
are lasting entities, the mind can focus on a slang. Understanding the distinction between
reality and concept is important. It allows us to determine whether a meditation practice will
lead to a trance state or a clear understanding of reality. Focusing on concepts will not help you
understand reality because you are focusing on something you have created in your mind.
It is not real. It does not have any of the qualities of ultimate reality.
It is not the same as watching your stomach rise and fall because when you watch your stomach rise
and fall, it changes constantly. The movements of the stomach are impermanent on satisfying
and uncontrollable. Watching the stomach helps you to see reality as it is and that is difficult.
It is much easier and pleasant to create a concept in your mind. For example, you can close your eyes
and utter your third eye in the center of your forehead. Imagine a collar and you say to yourself
blue, blue, or red, red, or white, white. Just focusing on the collar you have imagined.
Once you can fix your mind, only on the collar, thinking of nothing else,
that is considered to be an attainment of the fruit of samata meditation.
Many religious traditions, especially in India, make use of this sort of practice for the purpose
of gaining spiritual powers like remembering past lives, reading people's minds, seeing
heaven and hell, seeing the future, and leaving your body. To make use of samata meditation
for the subsequent development of Vipassana, you simply apply the principles of
Satipatana practice to the samata experiences. For example, you can note the focused mind
as focused, focused. If the experience is pleasurable, you can note happy, happy. If you feel calm,
calm, calm. If the experience involves lights or colors, you note seeing, seeing,
etc. and other results of the mindful interaction, the experience will change from seeing the stable,
satisfying and controllable concept to the impermanent, unsatisfying and uncontrollable reality.
Because samata meditation involves the cultivation of poor,
fundamental faculties, when you switch to Vipassana meditation,
your mind is readily able to see clearly the nature of reality.
For someone who does not take the time to practice samata meditation first,
Vipassana meditation can be a challenge because the mind does not have the prior training
or on more pleasurable objects. You have to face pain in the body,
distractions in the mind and all kinds of emotions without any of the peace and calm of samata
meditation. For the most part, this means only that the meditation practice is less pleasurable,
not slower or less effective. In fact, by starting with Vipassana meditation,
one avoids the potential pitfalls of becoming complacent or considered about,
once samata-based meditative attainment, as their training of the mind begins with ultimate
reality directly. The difference between the Vipassana meditation and samata meditation is quite
profound. Samata meditation is for the purpose of calming the mind where as Vipassana meditation
is for the purpose of seeing clearly. This does not mean that Vipassana meditation does not lead
to tranquility. You should find that through the practice of Vipassana meditation,
you are much calmer in general. Vipassana meditation is differentiated from samata meditation not
because it does not involve tranquility, but because tranquility is not the goal. Whether you feel calm
or not is not practically important. All that is important is that you see your state of calm
or distress clearly. Samata meditation cannot lead directly to enlightenment
because it is not focused on reality. The only way that it can be used to attain enlightenment
is if you follow it up as stated with Vipassana meditation. The strength of mind gained from
practicing samata meditation allows you to see clearer than you would have otherwise.
Since the same can be said of developing mental fortitude, starting with Vipassana meditation directly,
the only real benefit to practicing samata meditation first is the state of peace and calm
and maybe spiritual powers if you dedicate yourself to it to a great degree.
It is in fact clear from the text that starting with Vipassana is the quicker option of the two.
Among one's approach to Buddha and said,
I am old. I do not have much time left and my memory is not good. I am not able to learn everything.
Please teach me the basics of the path. The Buddha told him that first he should establish himself
in an intellectual acceptance of right view and purify his morality to make sure he is ethical
and moral in this behavior. Once he has done that, he has done enough to begin practicing
the four foundations of mindfulness. There is no talk about first focusing the mind or
entering into any meditative state before focusing on reality. In mindfulness, one
accesses meditative states naturally, entering into them based on reality. Even just focusing
under rising falling can be considered an absorbed state for the moment that one is observing it.
When you say stepping right, stepping up, invoking meditation,
if your mind is clearly aware of the movement, you are in an absorbed state for that moment.
So you are cultivating both samata and Vipassana at once. You are tranquil and you are also seeing
flirty.
