1
00:00:00,000 --> 00:00:10,560
In this video, I'm going to teach you how to practice meditation.

2
00:00:10,560 --> 00:00:13,200
What is meditation?

3
00:00:13,200 --> 00:00:16,400
Meditation means to focus your mind on something.

4
00:00:16,400 --> 00:00:23,200
It means to clear your mind, to calm it down, and to clean out all bad thoughts.

5
00:00:23,200 --> 00:00:28,400
When you practice meditation, you try to focus on something with all your attention, not

6
00:00:28,400 --> 00:00:34,280
letting your mind wander or get mixed up in bad thoughts or feelings.

7
00:00:34,280 --> 00:00:36,640
Why practice meditation?

8
00:00:36,640 --> 00:00:40,960
First, meditation makes you happy.

9
00:00:40,960 --> 00:00:46,800
When your mind is focused and clear, you will forget about all your problems and just

10
00:00:46,800 --> 00:00:48,000
be yourself.

11
00:00:48,000 --> 00:00:54,720
You'll stop thinking about the past, stop thinking about the future, and be happy with

12
00:00:54,720 --> 00:00:58,880
what you have here and now.

13
00:00:58,880 --> 00:01:07,040
Second, meditation can teach you many things, some people who practice meditation learn

14
00:01:07,040 --> 00:01:11,880
how to leave their bodies and fly through the air.

15
00:01:11,880 --> 00:01:18,560
Some are able to read other people's minds, and some can see things far, far away

16
00:01:18,560 --> 00:01:21,760
that no one else can see.

17
00:01:21,760 --> 00:01:27,520
But most important of all, meditation teaches you about yourself and the people around

18
00:01:27,520 --> 00:01:28,920
you.

19
00:01:28,920 --> 00:01:35,640
It teaches you how your mind works, so you don't get confused or mixed up about anything

20
00:01:35,640 --> 00:01:38,200
in your life.

21
00:01:38,200 --> 00:01:46,080
Third, meditation makes your mind sharp and clear, so you can live your life better.

22
00:01:46,080 --> 00:01:49,640
When your mind is clear, you're ready for anything.

23
00:01:49,640 --> 00:01:54,440
You'll know how to talk to your friends, family, and even people you meet for the first

24
00:01:54,440 --> 00:01:58,880
time, without feeling shy or embarrassed.

25
00:01:58,880 --> 00:02:03,480
You'll be able to study and remember things much better, and work hard helping your

26
00:02:03,480 --> 00:02:09,920
family, friends, and yourself in whatever work you have to do.

27
00:02:09,920 --> 00:02:14,320
Finally, meditation makes you a better person.

28
00:02:14,320 --> 00:02:18,760
When your mind is clear, you won't get angry about something you don't like, because

29
00:02:18,760 --> 00:02:22,920
you understand that's just the way it is.

30
00:02:22,920 --> 00:02:29,720
You won't get greedy about things you like, because you'll be happy with what you have.

31
00:02:29,720 --> 00:02:34,640
And you won't be confused or attached to anything, because you'll understand everything

32
00:02:34,640 --> 00:02:38,880
about yourself and the whole world around you.

33
00:02:38,880 --> 00:02:39,880
Sound good?

34
00:02:39,880 --> 00:02:42,360
Okay, let's start.

35
00:02:42,360 --> 00:02:45,880
First, we need something to focus on.

36
00:02:45,880 --> 00:02:50,360
That should be something simple, something easy for you to think of.

37
00:02:50,360 --> 00:02:54,280
For now, we'll start with a cat.

38
00:02:54,280 --> 00:03:01,760
All you have to do is look at the cat and say to yourself, cat, that's it.

39
00:03:01,760 --> 00:03:09,480
Just say to yourself again and again, cat, cat, cat.

40
00:03:09,480 --> 00:03:11,800
You don't even have to say it out loud.

41
00:03:11,800 --> 00:03:15,120
Just say it in your mind while you look at the cat.

42
00:03:15,120 --> 00:03:21,120
Try it now.

43
00:03:21,120 --> 00:03:29,120
Got it?

44
00:03:29,120 --> 00:03:37,600
Okay, now, close your eyes and think about a cat and say to yourself, cat, just like before.

45
00:03:37,600 --> 00:03:48,120
See this time, see the cat in your mind, cat, cat, cat.

46
00:03:48,120 --> 00:04:02,080
Go ahead, close your eyes and try it now.

47
00:04:02,080 --> 00:04:03,080
Done?

48
00:04:03,080 --> 00:04:07,560
Okay, let's try a dog next.

49
00:04:07,560 --> 00:04:29,040
Look at the dog and say to yourself, dog, dog, dog, say it slowly, don't rush.

50
00:04:29,040 --> 00:04:33,480
Now close your eyes and try to see the dog in your mind.

51
00:04:33,480 --> 00:04:40,960
Say dog, dog, dog, and think of a dog.

52
00:04:40,960 --> 00:04:45,880
If you can't see it in your mind, just open your eyes and look again.

53
00:04:45,880 --> 00:04:55,480
Peeking is allowed.

54
00:04:55,480 --> 00:05:03,560
Okay, now let's try our family members.

55
00:05:03,560 --> 00:05:08,960
Think of your mother or someone you love very much like your mother.

56
00:05:08,960 --> 00:05:14,880
If they are there with you, you can look at them and say to yourself, mother, or whatever

57
00:05:14,880 --> 00:05:24,480
name you call them, mother, mother, mother, again and again.

58
00:05:24,480 --> 00:05:46,040
Once you can do it without looking, close your eyes, think of them and say mother, mother,

59
00:05:46,040 --> 00:05:51,720
mother, or whatever name you call them, just repeat that.

60
00:05:51,720 --> 00:06:05,480
Thinking of them at the same time.

61
00:06:05,480 --> 00:06:11,680
Next think of your father or someone you love like a father.

62
00:06:11,680 --> 00:06:18,400
Same thing, if they are there or if you have a picture of them, just look and say father

63
00:06:18,400 --> 00:06:27,880
or dad or whatever name you have for them until you can do it without looking, father,

64
00:06:27,880 --> 00:06:43,440
father, father.

65
00:06:43,440 --> 00:06:59,360
Then close your eyes and do the same thing, father, father, father.

66
00:06:59,360 --> 00:07:09,040
Okay, now let's try something a bit different.

67
00:07:09,040 --> 00:07:12,600
Now let's focus on ourselves.

68
00:07:12,600 --> 00:07:15,160
To start you can look at yourself.

69
00:07:15,160 --> 00:07:30,520
Look at your legs, your arms, your body, and say to yourself, me, me, me, me, just repeat

70
00:07:30,520 --> 00:07:50,520
it to yourself while looking at your body, then close your eyes and do the same thing.

71
00:07:50,520 --> 00:08:10,400
Think of how you are sitting and say to yourself, me, me, me.

72
00:08:10,400 --> 00:08:20,960
Once you get the hang of it, you can just think to yourself sitting, sitting, sitting, thinking

73
00:08:20,960 --> 00:08:29,600
about yourself sitting still.

74
00:08:29,600 --> 00:08:40,720
If you want, you can try it lying down and say to yourself lying lying or if you want,

75
00:08:40,720 --> 00:09:00,560
you can walk back and forth, saying walking, walking, walking.

76
00:09:00,560 --> 00:09:07,040
All of these are ways to practice meditation.

77
00:09:07,040 --> 00:09:10,760
Another good object of meditation is the breath.

78
00:09:10,760 --> 00:09:20,640
Try this, sit comfortably or even lie down and put your hands on your stomach.

79
00:09:20,640 --> 00:09:31,880
You should feel your stomach rising and falling when your breath comes in and goes out.

80
00:09:31,880 --> 00:09:41,880
When your stomach rises, just say to yourself, rising, and when it falls, say falling,

81
00:09:41,880 --> 00:09:53,800
rising, falling, rising, falling, rising, falling.

82
00:09:53,800 --> 00:09:59,960
Again, you don't have to say it out loud.

83
00:09:59,960 --> 00:10:22,560
Just put your mind on your stomach and think to yourself, rising, falling as it happens.

84
00:10:22,560 --> 00:10:27,040
The truth is, you can meditate on just about anything.

85
00:10:27,040 --> 00:10:39,360
If you feel happy or sad, you can say to yourself, happy, happy, or sad, sad.

86
00:10:39,360 --> 00:10:44,280
And you won't get upset or stressed out about bad feelings.

87
00:10:44,280 --> 00:10:50,720
If you're thinking about something, you can just say thinking, thinking.

88
00:10:50,720 --> 00:10:54,960
And you won't be afraid of bad thoughts anymore.

89
00:10:54,960 --> 00:11:00,480
If you want something, you can say one thing, one thing.

90
00:11:00,480 --> 00:11:03,480
And you won't be sad if you don't get it.

91
00:11:03,480 --> 00:11:09,080
If you feel angry, you can just say angry, angry.

92
00:11:09,080 --> 00:11:16,160
And you won't need the yell or scream or hurt yourself or other people.

93
00:11:16,160 --> 00:11:21,240
If you can't find anything to meditate on, just go back and look at the stomach, rising

94
00:11:21,240 --> 00:11:22,840
and falling.

95
00:11:22,840 --> 00:11:34,720
And say to yourself, rising, falling like before, or pick something like a cat or a dog

96
00:11:34,720 --> 00:11:38,320
or someone you love and focus on them instead.

97
00:11:38,320 --> 00:12:08,280
Go ahead and pick one object and let's practice together.

98
00:12:08,280 --> 00:12:20,400
By now, you should feel like your mind is more focused than before.

99
00:12:20,400 --> 00:12:24,720
In the beginning, it might be difficult because you're learning something new.

100
00:12:24,720 --> 00:12:29,680
But if you practice it again and again, you will find your mind calming down and clearing

101
00:12:29,680 --> 00:12:39,400
up and you will begin to feel peaceful, happy and free from suffering more than ever before.

102
00:12:39,400 --> 00:12:42,480
This is how to practice meditation.

103
00:12:42,480 --> 00:12:46,520
If you want to try it by yourself, check out the next video where you can practice all

104
00:12:46,520 --> 00:13:05,160
of these techniques all by yourself.

