1
00:00:00,000 --> 00:00:06,000
Welcome back to today's question comes from self-mourd's pilot.

2
00:00:06,000 --> 00:00:13,000
Hello, it seems that the current society is basing the promotion of material attachment on scarcity.

3
00:00:13,000 --> 00:00:16,000
Since we are most attached to that which is scarce,

4
00:00:16,000 --> 00:00:21,000
do you think that this attachment can also be overcome by creating abundance?

5
00:00:21,000 --> 00:00:32,000
First of all, I would say we are not most attached to that which is scarce.

6
00:00:32,000 --> 00:00:38,000
Our attachment becomes most evident when things are scarce.

7
00:00:38,000 --> 00:00:44,000
When the things that we are attached to are present, we don't notice the attachment

8
00:00:44,000 --> 00:00:47,000
because it leads to pleasure and to happiness.

9
00:00:47,000 --> 00:00:51,000
This is the curious thing about human beings.

10
00:00:51,000 --> 00:00:56,000
When you don't get what you want, you immediately are aware of your attachment

11
00:00:56,000 --> 00:01:02,000
and are aware of the situation you remember it.

12
00:01:02,000 --> 00:01:09,000
As a result, we build up in our minds ideas that we never get what we want.

13
00:01:09,000 --> 00:01:12,000
When you want something, it never comes and we think,

14
00:01:12,000 --> 00:01:17,000
oh, I want this and then we don't get it and we think, okay, I'm jinxing myself or so on.

15
00:01:17,000 --> 00:01:26,000
We get these ideas of when you pay attention to something, you jinx yourself or so on.

16
00:01:26,000 --> 00:01:32,000
We get this idea of coincidence because when we do get what we want, we don't notice it.

17
00:01:32,000 --> 00:01:37,000
We don't realize that we were lucky.

18
00:01:37,000 --> 00:01:44,000
We get these ideas of having bad luck or so on from our bad experiences and we easily remember them.

19
00:01:44,000 --> 00:01:52,000
As a result, we might think that we're only attached to those things that we can't get.

20
00:01:52,000 --> 00:01:54,000
When we get things, there's no attachment.

21
00:01:54,000 --> 00:02:00,000
In fact, there is a growth in attachment similarly to a drug addict.

22
00:02:00,000 --> 00:02:05,000
When they get the drug, it increases the addiction in the mind and in the brain.

23
00:02:05,000 --> 00:02:10,000
When they don't get the drug in the other hand, it's actually less of an addiction.

24
00:02:10,000 --> 00:02:16,000
In that sense, scarcity is actually building up resistance to the addiction.

25
00:02:16,000 --> 00:02:21,000
When you don't get what you want, you have to deal with the addiction.

26
00:02:21,000 --> 00:02:25,000
You have to deal with the suffering that comes from not getting what you want.

27
00:02:25,000 --> 00:02:30,000
In that sense, it builds character and it builds resistance to the addiction.

28
00:02:30,000 --> 00:02:36,000
I would say actually contrary to the idea that scarcity leads to addiction.

29
00:02:36,000 --> 00:02:38,000
I would say abundance leads to addiction.

30
00:02:38,000 --> 00:02:40,000
In fact, that's really what's happened in this world.

31
00:02:40,000 --> 00:02:48,000
We live in a planet which is abundant, which has a abundance of natural resources.

32
00:02:48,000 --> 00:02:51,000
As a result, that leads to greed and to laziness.

33
00:02:51,000 --> 00:02:59,000
As we become lazy, we are unable to sustain a standard practice of distribution.

34
00:02:59,000 --> 00:03:07,000
We all want an easier way to get the things that we like.

35
00:03:07,000 --> 00:03:13,000
We become more and more attached to them and we need them here and we need them now.

36
00:03:13,000 --> 00:03:24,000
Instead of having ordinary food and we need special food or we need to have summer foods in the winter, winter foods in the summer, whatever.

37
00:03:24,000 --> 00:03:28,000
We need to have more and more exotic foods.

38
00:03:28,000 --> 00:03:36,000
The reason for this being that the addiction cycle is one of diminished returns.

39
00:03:36,000 --> 00:03:42,000
When you get what you want, there's a stimulus that arises in the brain.

40
00:03:42,000 --> 00:03:47,000
These chemicals are received by the receptors.

41
00:03:47,000 --> 00:03:54,000
When that sequence occurs, the receptors are weakened and the stimulus is weakened.

42
00:03:54,000 --> 00:03:57,000
As a result, the next time you get what you want.

43
00:03:57,000 --> 00:04:03,000
If you get the same level, it's not as enjoyable, so you need more.

44
00:04:03,000 --> 00:04:10,000
At the time when we're always getting what we want, we're actually increasing our desire for the object.

45
00:04:10,000 --> 00:04:25,000
We need a higher amount of stimulus and a more immediate, a more sustained or quicker fix, quicker high for the same amount of pleasure.

46
00:04:25,000 --> 00:04:28,000
We can't sustain that.

47
00:04:28,000 --> 00:04:36,000
The only thing we could possibly sustain is the only state of happiness that could possibly be sustained is the giving up of desire.

48
00:04:36,000 --> 00:04:45,000
I think scarcity helps us to do that when we remove ourselves from the object of our desire.

49
00:04:45,000 --> 00:04:51,000
Then we're able to see the attachment clearly and see it for what it is.

50
00:04:51,000 --> 00:04:56,000
I know there's this idea going around that all it would take is abundance.

51
00:04:56,000 --> 00:05:00,000
I think that comes from the fact that we can see in the world that there is an abundance.

52
00:05:00,000 --> 00:05:04,000
There really is enough food to feed everybody, for example.

53
00:05:04,000 --> 00:05:11,000
The reason it's not that way is because of the attachment to things that are abundant.

54
00:05:11,000 --> 00:05:13,000
When you can get things, you become attached.

55
00:05:13,000 --> 00:05:18,000
You don't want to share them with other people and you cling more and more and more and more.

56
00:05:18,000 --> 00:05:21,000
It doesn't sustain them all.

57
00:05:21,000 --> 00:05:26,000
That's why we see war and conflict and revolution and so on.

58
00:05:26,000 --> 00:05:30,000
Why the world is not able to live in peace happiness and harmony.

59
00:05:30,000 --> 00:05:53,000
That's my answer, thanks for the question.

