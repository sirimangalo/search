1
00:00:00,000 --> 00:00:13,920
So today's question was about studying in university, the meditator asked, how to continue

2
00:00:13,920 --> 00:00:21,160
on at university when it's currently boring.

3
00:00:21,160 --> 00:00:28,320
This person finds that perhaps as they meditate more, that it's harder to stay focused

4
00:00:28,320 --> 00:00:29,320
on.

5
00:00:29,320 --> 00:00:37,200
The school is boring.

6
00:00:37,200 --> 00:00:41,040
And so the question is whether they should just note boring, boring, and of course the simple

7
00:00:41,040 --> 00:00:47,160
answer is yes, just say to yourself, boring, bored, bored, so you wouldn't say boring,

8
00:00:47,160 --> 00:00:52,760
boring, because that's not being mindful, that's a judgment.

9
00:00:52,760 --> 00:00:55,320
You would say to yourself, bored, bored, because that's true.

10
00:00:55,320 --> 00:01:01,040
I'm bored, but to say that something is boring is not actually true.

11
00:01:01,040 --> 00:01:07,560
It's a judgment, it's not mindfulness, and important to understand that distinction.

12
00:01:07,560 --> 00:01:13,760
But there's a deeper issue here, and so there's two parts to it.

13
00:01:13,760 --> 00:01:25,240
The first part is that there are many things that fall on the side of worldly affairs.

14
00:01:25,240 --> 00:01:28,280
That get in the way of meditation practice.

15
00:01:28,280 --> 00:01:41,360
There's not things that are intrinsically distracting, like entertainment, or oversleeping,

16
00:01:41,360 --> 00:01:43,280
overeating, that sort of thing.

17
00:01:43,280 --> 00:01:52,520
And there's some few things that are intrinsically distracting, mostly involved with indulging

18
00:01:52,520 --> 00:02:04,720
sensual pleasure, but because it diverts your attention away from finding a higher state

19
00:02:04,720 --> 00:02:14,200
of mind and you become distracted and intoxicated by the enjoyment, right, addiction, really.

20
00:02:14,200 --> 00:02:20,000
It's like when you have stress, when you have a state or ordinary state is one that's

21
00:02:20,000 --> 00:02:27,280
subjected or vulnerable to stress.

22
00:02:27,280 --> 00:02:32,960
But when you engage in any kind of addiction, the ordinary addictions like entertainment,

23
00:02:32,960 --> 00:02:40,600
let's say, you leave that state of stress.

24
00:02:40,600 --> 00:02:52,440
You're no longer conscious of the need to, or the reality of stress and suffering.

25
00:02:52,440 --> 00:03:02,640
It's pleasant, and so you forget all about reality and objectivity, and there's no capacity

26
00:03:02,640 --> 00:03:07,920
to cultivate mindfulness and clarity.

27
00:03:07,920 --> 00:03:13,160
Because you're happy, you're enjoying everything's good, there's no problem, and of

28
00:03:13,160 --> 00:03:16,840
course that feeds your addiction, which makes things worse.

29
00:03:16,840 --> 00:03:24,880
The only way you can really help overcome your attachment is to not engage in it, is to live

30
00:03:24,880 --> 00:03:30,440
with them and to understand what it means to be attached, it means to be addicted, right.

31
00:03:30,440 --> 00:03:37,360
But that's not what this talk is about, so beyond that, there are things that don't

32
00:03:37,360 --> 00:03:45,880
necessarily stop you from being mindful, but they sure get in the way that the worldly

33
00:03:45,880 --> 00:03:52,720
affairs, and that's not the word we use, the word we use is obstacles or impediments.

34
00:03:52,720 --> 00:04:00,000
They're not bad or evil things, they're just very hard to do mindfully.

35
00:04:00,000 --> 00:04:05,440
Of course, as you guess, one of them is studying, there's ten of them, and so it's worth

36
00:04:05,440 --> 00:04:11,320
enumerating them, because you could find many others, but it's a good sort of overview

37
00:04:11,320 --> 00:04:12,640
just to talk about what they are.

38
00:04:12,640 --> 00:04:24,200
So the first one is dwelling, your home life, chores around the home.

39
00:04:24,200 --> 00:04:30,880
People who have big houses, luxurious houses, there's some difficulty involved in maintaining,

40
00:04:30,880 --> 00:04:38,080
you have to clean the house, you have to cut your lawn, you have to pay your mortgage

41
00:04:38,080 --> 00:04:45,480
and so on, that sort of thing, but also luxury, living in a luxury is the combination

42
00:04:45,480 --> 00:04:52,200
when you have a television, if you have a computer, if you have internet, if you have

43
00:04:52,200 --> 00:04:57,080
just the luxurious bed, comfortable couches, this sort of thing.

44
00:04:57,080 --> 00:05:03,520
In fact, I would say, on that front, most modern houses are problematic, because just

45
00:05:03,520 --> 00:05:09,040
the comfort of having a nice couch, a nice bed, makes you complacent and lazy, you don't

46
00:05:09,040 --> 00:05:16,720
have to dwell with ordinary aches and pains because you have these nice plush couches.

47
00:05:16,720 --> 00:05:21,920
Everything you've found, just the things that allow you to enter into a very comfortable

48
00:05:21,920 --> 00:05:29,400
and pleasant state of being, which is actually addictive, it makes it harder for you to

49
00:05:29,400 --> 00:05:36,160
say sit upright, to stand, to walk, for long periods of time, you get eight key and so

50
00:05:36,160 --> 00:05:41,880
you go and sit down or lie down again.

51
00:05:41,880 --> 00:05:51,800
The second one is family, which isn't quite what it sounds like.

52
00:05:51,800 --> 00:05:59,480
These are actually for monks and this involves, let's say, some sort of specific type

53
00:05:59,480 --> 00:06:08,480
of socializing, or you get involved with society, let's say getting, if we adapted to

54
00:06:08,480 --> 00:06:13,080
lay people, it would mean getting involved in other people's affairs, getting involved

55
00:06:13,080 --> 00:06:21,760
with society, going out to meet people or keeping correspondence, it's not that to socialize

56
00:06:21,760 --> 00:06:28,680
to be involved in society and going out for dinner with friends or keeping in touch or

57
00:06:28,680 --> 00:06:33,960
going to visit your neighbors or sort of say, I actually can be quite wholesome at times,

58
00:06:33,960 --> 00:06:44,280
but can also be distracting when you have social affairs, let's just put this one up to

59
00:06:44,280 --> 00:06:50,000
socializing in general, because for a monk, this would mean getting involved with lay

60
00:06:50,000 --> 00:06:57,240
families, right? You learn about their affairs and you talk to them and you chat and spend

61
00:06:57,240 --> 00:07:03,840
a lot of time socializing, right? Just with a lot of idle chatter can be quite an obstacle

62
00:07:03,840 --> 00:07:10,320
because you forget about your practice and you're taking outside of yourself, right?

63
00:07:10,320 --> 00:07:18,480
Mindfulness very much requires us to be here in our own space, to be present, to not

64
00:07:18,480 --> 00:07:24,040
be, have our minds outside, to involve with other people's conceptual problems and issues

65
00:07:24,040 --> 00:07:29,880
and stories and hearing about how someone's trip went or what they're going to do,

66
00:07:29,880 --> 00:07:37,400
their plans are easy to get caught up in society. It's funny, I hear many Sri Lankan people

67
00:07:37,400 --> 00:07:41,440
go back to Sri Lanka thinking they'll do some meditation when they're there and they

68
00:07:41,440 --> 00:07:49,520
have no time because of course this family is so important in society that they caught

69
00:07:49,520 --> 00:07:57,400
up going to weddings, going to funerals, just going and visiting, having to visit other

70
00:07:57,400 --> 00:08:05,480
people, right? The third one is gain or you could say possessions as well, but gain is

71
00:08:05,480 --> 00:08:14,440
just a big one, right? Our salary, our income, worrying about finances, let's say, just

72
00:08:14,440 --> 00:08:23,600
generally speaking, having to worry about our possessions, our car, our telephone worrying

73
00:08:23,600 --> 00:08:32,200
about needing a new computer or a new telephone or a new this, new clothes, nice clothes,

74
00:08:32,200 --> 00:08:39,280
worrying about affording the things that we need and want and just worrying about

75
00:08:39,280 --> 00:08:48,760
having enough food to live. It can be quite an impediment to have to worry about money.

76
00:08:48,760 --> 00:08:55,880
I mean, I think this goes without saying, I don't want to understate how much of a burden

77
00:08:55,880 --> 00:09:01,960
of money can be. There's not much to say, it's just, it's one of those things that obsesses

78
00:09:01,960 --> 00:09:08,000
and consumes the mind thinking about how much money I have, how much money I'm making.

79
00:09:08,000 --> 00:09:11,600
And then of course by extension, the things that we buy, it's not just money, it's

80
00:09:11,600 --> 00:09:16,200
for monks, of course we don't have money, but it's still very easy to get caught up in possessions

81
00:09:16,200 --> 00:09:20,560
wanting this, wanting that, hoping I get this, hoping I get that, even just hoping what

82
00:09:20,560 --> 00:09:30,240
food I get. So an important, important thing to consider. I'm just going to enumerate

83
00:09:30,240 --> 00:09:37,440
them and then we'll talk a little bit about what to do, right, or the ramifications.

84
00:09:37,440 --> 00:09:44,080
The third one, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah,

85
00:09:44,080 --> 00:09:57,560
ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah,

86
00:09:57,560 --> 00:10:08,640
ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah, ah ah, ah move 150.

87
00:10:08,640 --> 00:10:12,640
or just in generally helping them out.

88
00:10:12,640 --> 00:10:15,640
Suppose you have friends who you're helping out,

89
00:10:15,640 --> 00:10:17,640
and they have problems and you give them advice

90
00:10:17,640 --> 00:10:20,640
or problems and maybe you help them physically

91
00:10:20,640 --> 00:10:24,640
or with material or this sort of thing.

92
00:10:24,640 --> 00:10:27,640
People we support, people we help.

93
00:10:27,640 --> 00:10:31,640
It's a great thing, but even still can get in the way of your meditation.

94
00:10:31,640 --> 00:10:33,640
It's very hard to focus on your own practice

95
00:10:33,640 --> 00:10:38,640
when you're always constantly involved.

96
00:10:38,640 --> 00:10:41,640
It's very easy to get caught up in wanting to get involved

97
00:10:41,640 --> 00:10:46,640
and help other people, which can very much get in the way of your own practice.

98
00:10:46,640 --> 00:10:49,640
The fifth one is Gama, which doesn't mean karma.

99
00:10:49,640 --> 00:10:52,640
It means work.

100
00:10:52,640 --> 00:10:55,640
Yes, work gets in our way.

101
00:10:55,640 --> 00:11:00,640
So, simply having a career, having a full-time job,

102
00:11:00,640 --> 00:11:04,640
what we're told, this is a important part of life.

103
00:11:04,640 --> 00:11:07,640
Even just practically speaking in order to get the things we need,

104
00:11:07,640 --> 00:11:12,640
the things we want, and even the things we need to get a job.

105
00:11:12,640 --> 00:11:14,640
But definitely, it gets in the way.

106
00:11:14,640 --> 00:11:17,640
There's no avoiding the fact that having a full-time job

107
00:11:17,640 --> 00:11:20,640
outside of the meditation practice,

108
00:11:20,640 --> 00:11:24,640
it doesn't make it easy to be mindful all the time.

109
00:11:24,640 --> 00:11:27,640
And if someone were to say,

110
00:11:27,640 --> 00:11:31,640
how do you do all these things and still be mindful as well?

111
00:11:31,640 --> 00:11:33,640
The answer is, of course,

112
00:11:33,640 --> 00:11:36,640
but practically speaking, you really can't.

113
00:11:36,640 --> 00:11:39,640
And if you really want to dedicate yourself,

114
00:11:39,640 --> 00:11:42,640
you have to find a way to overcome this

115
00:11:42,640 --> 00:11:47,640
or put it behind you and put it aside.

116
00:11:47,640 --> 00:11:51,640
Even just work you have around the house.

117
00:11:51,640 --> 00:11:53,640
It's very easy to get caught up in doing work.

118
00:11:53,640 --> 00:11:56,640
It's easy for monks to get caught up in doing work.

119
00:11:56,640 --> 00:11:58,640
It doesn't necessarily need to be done.

120
00:11:58,640 --> 00:12:01,640
It's sweeping the monastery day and night.

121
00:12:01,640 --> 00:12:05,640
Making sure everything is perfectly clean and neat and orderly.

122
00:12:05,640 --> 00:12:08,640
And it's important to do these are good things.

123
00:12:08,640 --> 00:12:10,640
It's important to sweep. It's important to clean.

124
00:12:10,640 --> 00:12:13,640
It's important to make sure everything's running smoothly.

125
00:12:13,640 --> 00:12:16,640
But it's very easy to get caught up in it.

126
00:12:16,640 --> 00:12:20,640
The Buddha discouraged monks from working too much,

127
00:12:20,640 --> 00:12:23,640
from being caught up in even work.

128
00:12:23,640 --> 00:12:26,640
Because it's a distraction.

129
00:12:26,640 --> 00:12:28,640
Eventually it becomes a replacement.

130
00:12:28,640 --> 00:12:30,640
I could meditate.

131
00:12:30,640 --> 00:12:33,640
That's difficult to be easier if I just did some cleaning.

132
00:12:33,640 --> 00:12:35,640
That I can do. That I'm good at.

133
00:12:35,640 --> 00:12:37,640
Much easier.

134
00:12:37,640 --> 00:12:40,640
Much more difficult to be mindful when you're working, of course.

135
00:12:40,640 --> 00:12:44,640
Because it's more complicated.

136
00:12:44,640 --> 00:12:47,640
So that's five.

137
00:12:47,640 --> 00:12:51,640
The sixth one is Adhana, which means travel.

138
00:12:51,640 --> 00:12:56,640
Travel can be an impediment to the practice quite easily.

139
00:12:56,640 --> 00:13:02,640
Some monks are very fond of travel and get caught up in going here and going there.

140
00:13:02,640 --> 00:13:06,640
It's interesting how this has become a practice.

141
00:13:06,640 --> 00:13:10,640
I'm not going to be overly critical, but I would want to question people who think

142
00:13:10,640 --> 00:13:15,640
that walking long distances is a part of the Buddha's teaching.

143
00:13:15,640 --> 00:13:17,640
Because it's really not.

144
00:13:17,640 --> 00:13:20,640
Certainly in the time of the Buddha they had to walk long distances.

145
00:13:20,640 --> 00:13:24,640
But it was important to recognize that that can get in the way of your practice.

146
00:13:24,640 --> 00:13:30,640
It's harder because it's a stress on the body.

147
00:13:30,640 --> 00:13:36,640
And it's also distracting when you're traveling and having to familiarize yourself

148
00:13:36,640 --> 00:13:40,640
with your new surroundings and with the changing surroundings.

149
00:13:40,640 --> 00:13:46,640
So this idea of traveling as a meditation practice, it can actually be a distraction from the practice.

150
00:13:46,640 --> 00:13:52,640
Much more difficult is to stay put in a quiet place and actually confront the problems.

151
00:13:52,640 --> 00:13:57,640
It's very easy to try and run away from things by constantly moving.

152
00:13:57,640 --> 00:14:01,640
There's stories about monks who can't take it at one monastery.

153
00:14:01,640 --> 00:14:05,640
So they move to a new monastery until I'm moving.

154
00:14:05,640 --> 00:14:10,640
We're moving this month.

155
00:14:10,640 --> 00:14:17,640
Something we'll have to be aware of and try and be very mindful as we move.

156
00:14:17,640 --> 00:14:19,640
Yati is number seven.

157
00:14:19,640 --> 00:14:22,640
So this is actually your relatives.

158
00:14:22,640 --> 00:14:25,640
Your children, your parents.

159
00:14:25,640 --> 00:14:31,640
This is one I get asked a lot is how to practice with your relatives, particularly children.

160
00:14:31,640 --> 00:14:35,640
I get parents who come and ask and say, you know, you talk about being mindful.

161
00:14:35,640 --> 00:14:38,640
But what can we do when we have kids?

162
00:14:38,640 --> 00:14:44,640
So we really have to understand that you can't have it both ways.

163
00:14:44,640 --> 00:14:46,640
You can't have your cake and eat it too.

164
00:14:46,640 --> 00:14:48,640
You have to have one or the other.

165
00:14:48,640 --> 00:14:52,640
And it's not to say that people with kids can't be mindful and even become enlightened.

166
00:14:52,640 --> 00:15:01,640
But you have to recognize that it's an impediment and power to you if you can be mindful and doing it.

167
00:15:01,640 --> 00:15:04,640
But you have to recognize that there's no easy solution.

168
00:15:04,640 --> 00:15:16,640
You can't be perfectly, practically speaking, perfectly practicing mindfulness when you're caught up in the concerns of your family.

169
00:15:16,640 --> 00:15:22,640
Now it's possible in certain circumstances that you can arrange things so that you are able to get away.

170
00:15:22,640 --> 00:15:26,640
And it's not even physically, but you have to also mentally get away.

171
00:15:26,640 --> 00:15:32,640
I remember once I had a woman come to me many years ago now and she said,

172
00:15:32,640 --> 00:15:38,640
you know, I was having real problems with her son and she hadn't practiced meditation yet.

173
00:15:38,640 --> 00:15:45,640
Because I wondered if I could help her son because he was really misbehaving and really hard to deal with.

174
00:15:45,640 --> 00:15:49,640
And I said, sure, I'll help you come and meditate until I told her.

175
00:15:49,640 --> 00:15:52,640
That's the way I could help us for her to come and do a meditation course.

176
00:15:52,640 --> 00:15:56,640
And we talked about it and eventually she did come and do a meditation.

177
00:15:56,640 --> 00:15:58,640
She did one course, two courses, I think.

178
00:15:58,640 --> 00:16:03,640
And after the second course she came and I said, how's your son doing it? I said, I don't care, let him do what he said, let him do his thing.

179
00:16:03,640 --> 00:16:11,640
He was actually grown, he was a young adult at the time, so she didn't need to take care of him physically.

180
00:16:11,640 --> 00:16:21,640
But she gave up the mental attachment and worry and concern, which actually in the end we tend to think is to the benefit of both parties.

181
00:16:21,640 --> 00:16:27,640
Not just you become negligent, but you are less worried and less controlling and less concerned.

182
00:16:27,640 --> 00:16:36,640
But either way, no question that this is an impediment that you have to learn about, not just children, but other relatives.

183
00:16:36,640 --> 00:16:44,640
Getting involved in their affairs, worrying about their problems, having to help them with your problems, with their problems.

184
00:16:44,640 --> 00:16:49,640
I mean, sometimes it's the right thing to do to help them, but there's no question that it's an impediment.

185
00:16:49,640 --> 00:16:58,640
So we try and find ways to at least temporarily put these things aside. In fact, most of these are, if not all of them, are unavoidable.

186
00:16:58,640 --> 00:17:12,640
It's just a matter of putting them aside, finding time to take out of our schedules to engage in meditation full-time and wholeheartedly.

187
00:17:12,640 --> 00:17:23,640
It's not like you should say, well, try not to get sick is good advice.

188
00:17:23,640 --> 00:17:28,640
But also be aware that sickness will get in the way of your practice, and there's not often nothing you can do about that.

189
00:17:28,640 --> 00:17:32,640
When you're sick, it can be a real impediment.

190
00:17:32,640 --> 00:17:39,640
But the other thing I would say, it's not hopeless, especially in this example when you're sick.

191
00:17:39,640 --> 00:17:43,640
It very easily becomes an impediment, all of these.

192
00:17:43,640 --> 00:17:51,640
But there are many examples where they are not impediments, where you have relatives, and you're even closer to your relatives or your children, you're close physically.

193
00:17:51,640 --> 00:17:56,640
But you're just not mentally caught up in it. You're able to separate yourself.

194
00:17:56,640 --> 00:18:07,640
I mean, the real practice is ultimately being able to be free from mental engagement and worry, well, to file an injury or reaction,

195
00:18:07,640 --> 00:18:11,640
so that when you have interactions with people, you're able to be mindful.

196
00:18:11,640 --> 00:18:14,640
It's just very hard to do practically.

197
00:18:14,640 --> 00:18:19,640
And especially when you're just starting out, you really should take time to just avoid all of these things.

198
00:18:19,640 --> 00:18:21,640
Get away from your family.

199
00:18:21,640 --> 00:18:26,640
Find a time when you're really healthy, and give everything else.

200
00:18:26,640 --> 00:18:28,640
Put everything else aside.

201
00:18:28,640 --> 00:18:33,640
But also, eventually finding ways to make these a part of your practice.

202
00:18:33,640 --> 00:18:39,640
The higher practice is going to be when, eventually, you can deal with these things.

203
00:18:39,640 --> 00:18:46,640
It's important to recognize that in the beginning, you really can't, but then eventually learning how to make them so they're not impediments.

204
00:18:46,640 --> 00:18:50,640
So the relatives are not an impediment, so that sickness isn't an impediment.

205
00:18:50,640 --> 00:18:57,640
So that traveling, you're very mindful, even when you're traveling, it can be a very good practice.

206
00:18:57,640 --> 00:19:03,640
Number nine is gantau, which means study.

207
00:19:03,640 --> 00:19:06,640
So this is the one that was mentioned.

208
00:19:06,640 --> 00:19:08,640
Study is up there.

209
00:19:08,640 --> 00:19:16,640
It's among the hardest, I would say, because your mind is unavoidably engaged otherwise.

210
00:19:16,640 --> 00:19:18,640
Studying is not being mindful.

211
00:19:18,640 --> 00:19:26,640
You're trying to learn facts, you're trying to learn information or even interpret information.

212
00:19:26,640 --> 00:19:28,640
Find ways to organize information.

213
00:19:28,640 --> 00:19:31,640
There's a lot of mental activity involved with studying.

214
00:19:31,640 --> 00:19:34,640
Now, you very much can use mindfulness.

215
00:19:34,640 --> 00:19:42,640
I mean, I think one of the good things about one of the few good things that really came out of studying for me was,

216
00:19:42,640 --> 00:19:55,640
having this knowledge or this ability to say, to show this firsthand knowledge of how the mindfulness helps you organize your mind.

217
00:19:55,640 --> 00:20:01,640
It helps you stay focused when you have work that has to be done.

218
00:20:01,640 --> 00:20:11,640
So I got incredibly good grades and got an award and a medal and all these things just for basically getting not perfect, but very, very good grades.

219
00:20:11,640 --> 00:20:16,640
And a lot of it has to do with just being able to be mindful and not getting overwhelmed and not getting bored.

220
00:20:16,640 --> 00:20:26,640
And the boredom is a reaction that's based on anger. It's a disliking statement, not anger, but what we would call the anger category,

221
00:20:26,640 --> 00:20:31,640
the category of disliking or negative reactions.

222
00:20:31,640 --> 00:20:32,640
And so it's on you.

223
00:20:32,640 --> 00:20:34,640
The boredom is your problem.

224
00:20:34,640 --> 00:20:40,640
And if you can overcome it, if you can change the way you look at things, you're able to do many things without getting bored.

225
00:20:40,640 --> 00:20:56,640
Many things that would be very difficult for you to force yourself to do because that's what it would be is forcing and turning it into instead just being and doing and being a piece with the doing.

226
00:20:56,640 --> 00:21:07,640
So definitely, definitely, you have to put aside your studies if you want to really get involved with meditation practice, but when it's unavoidable,

227
00:21:07,640 --> 00:21:13,640
and to the extent that it's unavoidable, you try and find ways to be mindful and doing.

228
00:21:13,640 --> 00:21:19,640
Number ten and final one is magic.

229
00:21:19,640 --> 00:21:28,640
And so this applies more, I think, to really advance meditators, but not always, you know, because supernatural, let's say,

230
00:21:28,640 --> 00:21:41,640
super normal states, but it definitely applies specifically to meditators. Because super normal things are very much a part of the meditation,

231
00:21:41,640 --> 00:21:49,640
what do we call a paradigm, I suppose, or the part of life, part of reality that is meditation.

232
00:21:49,640 --> 00:21:55,640
When we think of meditations, what I'm trying to say, it was very much caught up in supernatural.

233
00:21:55,640 --> 00:22:00,640
When you engage in meditation, there are many things that happen that we would consider supernatural.

234
00:22:00,640 --> 00:22:13,640
Even just seeing things bright lights or pictures or images or hearing voices or, you know, and it goes on, like people talk about levitating or feel like they're feeling like they're levitating.

235
00:22:13,640 --> 00:22:24,640
But hearing things that are far away or seeing things that are far away or remembering things that happen a long time ago or even in past lives, right?

236
00:22:24,640 --> 00:22:30,640
And all these many things that we would consider very much super not super natural.

237
00:22:30,640 --> 00:22:38,640
And they can be a real distraction, it's important to recognize because this is a part of what we think of, or the world of meditation, let's say.

238
00:22:38,640 --> 00:22:44,640
Yeah, they're very much a part of what can distract us from what's more important.

239
00:22:44,640 --> 00:22:47,640
And important about these are not the path.

240
00:22:47,640 --> 00:22:57,640
This is not part of what we're trying to do of the practice that leads to freedom from suffering.

241
00:22:57,640 --> 00:23:02,640
So very much, very easily becomes an impediment.

242
00:23:02,640 --> 00:23:10,640
So the important point here is that all of these 10 things can and do obstruct our practice without being evil things in and of themselves.

243
00:23:10,640 --> 00:23:14,640
Often there are things that we have to do as a part of our lives.

244
00:23:14,640 --> 00:23:20,640
But an important part of our practice is always going to be structuring our lives around what's essential.

245
00:23:20,640 --> 00:23:27,640
None of these things, I think, are something that we have to put our attention on as being essential.

246
00:23:27,640 --> 00:23:40,640
I go, I have to fix this or I have to engage in this, involve myself in order to find the ultimate goal of life.

247
00:23:40,640 --> 00:23:42,640
None of them are that.

248
00:23:42,640 --> 00:23:45,640
And that's important because we're often taught that they are.

249
00:23:45,640 --> 00:23:52,640
Like, family is important, or study is important, or money is important, or even travel is important.

250
00:23:52,640 --> 00:23:55,640
Any of these things, even magical powers are important.

251
00:23:55,640 --> 00:24:02,640
Many teachers will, some teachers, many non-butters teachers will say.

252
00:24:02,640 --> 00:24:05,640
And it's important to realize that that's not the case.

253
00:24:05,640 --> 00:24:12,640
And try and find ways to, for many of these, put them aside, do away with them.

254
00:24:12,640 --> 00:24:20,640
And I think ultimately to change our lives so that we don't have to get caught up in these things.

255
00:24:20,640 --> 00:24:27,640
Because these questions about how do I practice the way you teach well doing this or that or the other thing.

256
00:24:27,640 --> 00:24:31,640
And really the best answer, the key answer is that you really can't.

257
00:24:31,640 --> 00:24:37,640
You have to find a way for many of these things to at least put them aside temporarily.

258
00:24:37,640 --> 00:24:41,640
And, you know, do your best when it's inevitable.

259
00:24:41,640 --> 00:24:44,640
Of course, it doesn't mean give it up as hopeless.

260
00:24:44,640 --> 00:24:52,640
But be aware, be aware that it's not the same to engage with all these things and try and be mindful.

261
00:24:52,640 --> 00:24:56,640
It's never going to be as effective, especially in the beginning.

262
00:24:56,640 --> 00:25:01,640
As it would be to take time out, give up all these things.

263
00:25:01,640 --> 00:25:07,640
Go off to a meditation center and really dedicate yourself to the practice.

264
00:25:07,640 --> 00:25:09,640
So that's the first part.

265
00:25:09,640 --> 00:25:15,640
The second thing that I wanted to say is that maybe I've already started saying it is that these things get,

266
00:25:15,640 --> 00:25:19,640
that meditation gets in the way of doing these things as well.

267
00:25:19,640 --> 00:25:24,640
And so when this person says, you know, I feel very bored.

268
00:25:24,640 --> 00:25:27,640
This is another consequence that you have to be aware of in the meditation.

269
00:25:27,640 --> 00:25:32,640
That not only do these things get in the way of meditation, but you'll find that the more you meditate,

270
00:25:32,640 --> 00:25:37,640
the harder it is to engage in the same way as you did before.

271
00:25:37,640 --> 00:25:42,640
So this restructuring, this changing of your life in many ways happens naturally.

272
00:25:42,640 --> 00:25:49,640
And it's important to recognize and be aware and listen to that.

273
00:25:49,640 --> 00:25:55,640
Because none of these things are ultimately important to have ultimate importance.

274
00:25:55,640 --> 00:25:59,640
And that's the great thing about meditation is that it shows you that.

275
00:25:59,640 --> 00:26:02,640
It shows you everything you really need to know.

276
00:26:02,640 --> 00:26:04,640
It shows you what's important.

277
00:26:04,640 --> 00:26:07,640
It shows you what's right. It shows you what's good.

278
00:26:07,640 --> 00:26:09,640
It teaches you.

279
00:26:09,640 --> 00:26:16,640
And so the conflict that people find, you know, I want to study and I want to meditate,

280
00:26:16,640 --> 00:26:19,640
the more I meditate, the less they want to study.

281
00:26:19,640 --> 00:26:22,640
How do I keep studying when I just don't want to do it anymore?

282
00:26:22,640 --> 00:26:27,640
And that's the point is the meditation is starting to teach you that that's actually not ultimately important.

283
00:26:27,640 --> 00:26:34,640
And then you might say, well, then if I don't study, how can I, you know, just get a job and live my life?

284
00:26:34,640 --> 00:26:36,640
That's, of course, true.

285
00:26:36,640 --> 00:26:44,640
In one sense, of course, there are other ways to do without education, but there's nothing really fair about it.

286
00:26:44,640 --> 00:26:47,640
Not like I'm telling you, I don't have all the answers.

287
00:26:47,640 --> 00:26:51,640
If you go and do study, it's going to get in the way of your practice.

288
00:26:51,640 --> 00:26:55,640
There's no question now, does that mean you should drop out of school and become a monk?

289
00:26:55,640 --> 00:26:56,640
Maybe, maybe not.

290
00:26:56,640 --> 00:27:04,640
In fact, it might be very bad idea to do that because you just wouldn't be able to be capable of being a monk or even living in the forest.

291
00:27:04,640 --> 00:27:08,640
It's not an easy thing to do.

292
00:27:08,640 --> 00:27:13,640
But it's the fact of life, the fact of reality that these things get in the way.

293
00:27:13,640 --> 00:27:16,640
Eventually, you're going to be less and less interested in doing it.

294
00:27:16,640 --> 00:27:19,640
And for many people that can be a real challenge.

295
00:27:19,640 --> 00:27:25,640
You know, if you don't go to school, if you don't get a job, how do you even survive?

296
00:27:25,640 --> 00:27:29,640
Of course, society makes that very difficult.

297
00:27:29,640 --> 00:27:33,640
And it very much depends on your past karma.

298
00:27:33,640 --> 00:27:38,640
Sometimes your past karma just makes it very, very difficult for you to even find a place to practice meditation.

299
00:27:38,640 --> 00:27:40,640
There's nothing fair about it.

300
00:27:40,640 --> 00:27:47,640
I'm not God with all the answers and Buddhism isn't one of those religions that's going to offer you this solution.

301
00:27:47,640 --> 00:27:49,640
It can be very, very difficult.

302
00:27:49,640 --> 00:27:51,640
It's something we have to realize.

303
00:27:51,640 --> 00:27:53,640
I mean, there usually is a path open.

304
00:27:53,640 --> 00:28:00,640
And often, even if we don't see it, being mindful can change our lives very profoundly in a good way

305
00:28:00,640 --> 00:28:07,640
and help us to find a way forward, to be more mindful and to have opportunities,

306
00:28:07,640 --> 00:28:10,640
to engage more with meditation practice.

307
00:28:10,640 --> 00:28:16,640
But sometimes it requires real sacrifices just because of the situation we were even born into.

308
00:28:16,640 --> 00:28:20,640
We have to find a way to restructure.

309
00:28:20,640 --> 00:28:26,640
Doesn't mean we have to become Buddhist or leave home or this sort of things.

310
00:28:26,640 --> 00:28:29,640
But I think these 10 things are a good guideline.

311
00:28:29,640 --> 00:28:34,640
So the sorts of things that we have to listen when our mind starts to say,

312
00:28:34,640 --> 00:28:44,640
it's time to stop putting so much emphasis or involvement or focus or driver ambition on these things.

313
00:28:44,640 --> 00:28:46,640
So two sides.

314
00:28:46,640 --> 00:28:48,640
The point is they aren't compatible.

315
00:28:48,640 --> 00:28:51,640
They get in each other's way.

316
00:28:51,640 --> 00:28:54,640
Worldly things and the dhamma.

317
00:28:54,640 --> 00:29:01,640
It's important to understand that this isn't a path that is going to let you live your life as normal.

318
00:29:01,640 --> 00:29:06,640
The life we live as human beings, even just being a human being, is contrived.

319
00:29:06,640 --> 00:29:08,640
It's artificial.

320
00:29:08,640 --> 00:29:15,640
Being a human being is something that has been created a lifetime after lifetime after lifetime,

321
00:29:15,640 --> 00:29:19,640
as generation after generation physically and mentally.

322
00:29:19,640 --> 00:29:26,640
We've created this reality of being a human being, but there's nothing special or right about it.

323
00:29:26,640 --> 00:29:31,640
That's a very sort of theistic way of looking at things that we were created for some purpose,

324
00:29:31,640 --> 00:29:32,640
and we really weren't.

325
00:29:32,640 --> 00:29:39,640
We just we muddled our way into this and it's very contrived and has nothing to do with ultimate purpose,

326
00:29:39,640 --> 00:29:45,640
which goes beyond this life or this state of being human.

327
00:29:45,640 --> 00:29:51,640
There's nothing meaningful or important about the way we have structured our lives,

328
00:29:51,640 --> 00:29:54,640
the way society tells us to live our lives.

329
00:29:54,640 --> 00:30:04,640
There's much that gets in the way of actually seeing what is really true and real and important and essential.

330
00:30:04,640 --> 00:30:06,640
There you go.

331
00:30:06,640 --> 00:30:12,640
Some thoughts on the relationship between the worldly affairs and the meditation practice.

332
00:30:12,640 --> 00:30:14,640
Thank you all for tuning in.

333
00:30:14,640 --> 00:30:29,640
Thank you so much.

