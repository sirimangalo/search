1
00:00:00,000 --> 00:00:09,000
Hi, welcome back to Ask a Monk. Today's question comes from BD951 who says,

2
00:00:09,000 --> 00:00:16,000
Can you explain what an insight is in regards to meditation? Is it a thought that arises that causes you to let go?

3
00:00:16,000 --> 00:00:25,000
Or does it occur at a deeper level? Also, when a person reaches salt up on a state of mind, is that a distinct event in time?

4
00:00:25,000 --> 00:00:33,000
Two questions. Okay, first question. Now, what is an insight?

5
00:00:33,000 --> 00:00:51,000
Well, there are two levels of insight. The first one being the thought level, where one has a sort of an epiphany realization on an intellectual level.

6
00:00:51,000 --> 00:00:58,000
And the second type is a meditative experience of reality for what it is.

7
00:00:58,000 --> 00:01:04,000
A realization of the way things are based on meditation practice.

8
00:01:04,000 --> 00:01:12,000
It's the second type that is really an answer to your question in regards to meditation practice.

9
00:01:12,000 --> 00:01:20,000
It's an experience. It's not an intellectual thought. There is no need for thought because one sees things as they are.

10
00:01:20,000 --> 00:01:29,000
I can go briefly through the insights. There are 16 insights that one realizes in the practice of a meditation.

11
00:01:29,000 --> 00:01:35,000
If one does a intensive course, it can be expected that you get through several of these.

12
00:01:35,000 --> 00:01:43,000
If the course is long enough and comprehensive enough, you can possibly go through all 16.

13
00:01:43,000 --> 00:01:53,000
The first one is called Naamarupapareshitaya, which is the insight into the separation between body and mind that all beings are separated into two parts.

14
00:01:53,000 --> 00:02:04,000
One being the physical and the other being the mental that there is no self, no soul, but only these two realities of the physical and mental phenomena that arise.

15
00:02:04,000 --> 00:02:14,000
Number two is realizing the nature of these two things, how they work together, that they work in terms of cause and effect.

16
00:02:14,000 --> 00:02:22,000
Number three is called Samasandayana. It's seeing things as they are knowledge of comprehension.

17
00:02:22,000 --> 00:02:31,000
I believe it's translated as starting to see impermanent suffering in non-self, the three characteristics inherent in everything.

18
00:02:31,000 --> 00:02:47,000
Why nothing in the world is worth clinging to, starting to see the things that we thought were impermanent or impermanent, the things that we thought were satisfying, can't satisfy us because they're impermanent, and that they're not self, they're not under control.

19
00:02:47,000 --> 00:02:58,000
And we can't force reality to be this way or that as we might wish. Number four is Naamayana, the knowledge of arising and ceasing.

20
00:02:58,000 --> 00:03:03,000
And this is starting to really be able to break reality down into the arising and ceasing.

21
00:03:03,000 --> 00:03:10,000
So rather than seeing things as good and bad and so on, we simply see them as arising and ceasing coming and going.

22
00:03:10,000 --> 00:03:28,000
Number five is knowledge of cessation, so starting to see that everything ceases, as opposed to watching things arise and see starting to really see that there's nothing in the world that's impermanent and starting to accept the fact that everything has to cease.

23
00:03:28,000 --> 00:03:42,000
Number six is knowledge of the danger or fearsomeness by Naamayana, which starts to realize that there's a problem here that if everything ceases then really we can't cling.

24
00:03:42,000 --> 00:03:51,000
Number seven is knowledge of the disadvantages of clinging, starting to see the suffering that's caused by it.

25
00:03:51,000 --> 00:04:06,000
Number eight is knowledge of disenchantment or dispassion, where Nibitaya, number one becomes disenchanted with the reality, starting to realize that it's only the same old things over and over again.

26
00:04:06,000 --> 00:04:15,000
There's nothing in the world that's just really unique, exceptional, special in any way, in the end it's all just seeing, hearing, smelling, tasting, feeling and thinking.

27
00:04:15,000 --> 00:04:21,000
Starting to turn away and realize that happiness doesn't lie in the experiences that we have.

28
00:04:21,000 --> 00:04:44,000
Number nine is Muunjidukamayatayana, knowledge of desire for release or freedom, wanting to be free, wanting to get away and starting to actually turn away in another direction and seek happiness, seek peace inside, no longer looking into external objects for peace and happiness.

29
00:04:44,000 --> 00:04:56,000
Number ten is this following this idea and actually looking inward, but disenchantment means going over everything that you've been watching in the meditation.

30
00:04:56,000 --> 00:05:08,000
Again, in this time giving it a closer look, trying to see where exactly you're clinging to rather than where you can find happiness.

31
00:05:08,000 --> 00:05:15,000
Number eleven is Sankarubekayana, knowledge of equanimity.

32
00:05:15,000 --> 00:05:29,000
Once you change the way you look at things, instead of trying to find happiness in the reality, starting to see where you're clinging, you're able to work out the clinging, work out your attachments, work out the kings in the mind.

33
00:05:29,000 --> 00:05:40,000
And then your mind starts to calm down and feel equanimity in regards to all things. It sees everything equally and it's very finely tuned.

34
00:05:40,000 --> 00:05:44,000
And this is the height of insight in a worldly sense.

35
00:05:44,000 --> 00:05:51,000
After Sankarubekayana, there arises two, there arises several knowledges in succession.

36
00:05:51,000 --> 00:06:06,000
Next one is Anulomayana, where the mind gets to the finest point. It's like the consummation of insight where it's so finely tuned that you're able to see things exactly as they are.

37
00:06:06,000 --> 00:06:20,000
This is really the insight that we're going for. Anulomayana means to come to the middle or to finally get it and go with the grain as opposed to going against the grain.

38
00:06:20,000 --> 00:06:27,000
That we're finally going with the stream of reality and in tune with the way things are.

39
00:06:27,000 --> 00:06:30,000
And that's just a very brief moment in time.

40
00:06:30,000 --> 00:06:37,000
The next one is Gokribuyano, where one changes one's lineage, they say.

41
00:06:37,000 --> 00:06:45,000
Changes one's state of mind from being an ordinary human, being one who realizes the truth.

42
00:06:45,000 --> 00:06:56,000
The next moment is called Gokribuyano, where one attains the path, where one's mind actually goes inward, not following a path outward.

43
00:06:56,000 --> 00:07:04,000
The Gokribuyano is the path inward, where the mind enters on to the path to enlightenment just for a moment.

44
00:07:04,000 --> 00:07:09,000
Then there's Palayana, which is the realization of the fruit of the path.

45
00:07:09,000 --> 00:07:16,000
Where one sees, realizes Nivana or Nivana for the first time.

46
00:07:16,000 --> 00:07:33,000
And then number 16 is called, but the way Kana, where one reflects upon this, comes out of the realization of Nivana and starts to reflect on the results, seeing what has changed in the mind as a result of realizing Nivana.

47
00:07:33,000 --> 00:07:42,000
So the second question, the realization of Sotapana is the moment of realizing Nivana, the 14th stage of insight.

48
00:07:42,000 --> 00:07:58,000
Once one attains that state, one is said to be the first type of holy person, the first type of enlightened being, which is the Sotapati manga, the realization of the path of Sotapana.

49
00:07:58,000 --> 00:08:08,000
Then the next moment, one attains Palayana, one is the second type of holy person, which is one who has realized Sotapati Pala or fruition.

50
00:08:08,000 --> 00:08:13,000
From that point on, one is considered to be a Sotapana, one who has entered the stream.

51
00:08:13,000 --> 00:08:23,000
Because that realization has fundamentally changed the way the person looks at the world, there's no going back to an ordinary state of being.

52
00:08:23,000 --> 00:08:30,000
So one is considered to have entered the stream, so to speak, and will be born a maximum of seven more lifetimes.

53
00:08:30,000 --> 00:08:41,000
So this is pretty technical and very Buddhist, and I don't always go into things on such a technical level in my videos.

54
00:08:41,000 --> 00:08:56,000
But okay, so thanks for the opportunity, and there it is, those are the 16 stages of knowledge, readers digest version.

