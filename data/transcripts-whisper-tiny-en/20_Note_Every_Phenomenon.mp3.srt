1
00:00:00,000 --> 00:00:03,000
Note every phenomenon.

2
00:00:03,000 --> 00:00:10,000
Is it necessary to note every phenomenon that occurs during walking meditation?

3
00:00:10,000 --> 00:00:14,000
No, given the dynamic nature of walking meditation,

4
00:00:14,000 --> 00:00:19,000
it can actually be a hindrance to try to note everything while in motion.

5
00:00:19,000 --> 00:00:24,000
A general rule of thumb is to ignore small disturbances,

6
00:00:24,000 --> 00:00:29,000
like bleeding thoughts, sounds or sensations.

7
00:00:29,000 --> 00:00:35,000
Simply bring the mind back to focus on the foot without interrupting the movements of the body.

8
00:00:35,000 --> 00:00:41,000
If the disturbance is significant enough to keep the mind away from the foot,

9
00:00:41,000 --> 00:00:44,000
then stop, bring the feet together,

10
00:00:44,000 --> 00:00:48,000
noting, stopping, stopping,

11
00:00:48,000 --> 00:00:56,000
and note the phenomenon in the standing position until it either goes away or the mind loses interest,

12
00:00:56,000 --> 00:01:23,000
and then resume the walking as normal.

