1
00:00:00,000 --> 00:00:11,280
Now, the first definition is what is meant here, but there are other kinds of dead.

2
00:00:11,280 --> 00:00:17,280
So, dead is termination cutting off, in other words the other hand termination of suffering

3
00:00:17,280 --> 00:00:25,960
of the wrong is not intended here, because when an other hand dies, he dies and then there

4
00:00:25,960 --> 00:00:34,120
is no more rebirth for him. So, that kind of death is not meant here, nor is momentary death

5
00:00:34,120 --> 00:00:39,320
that means at every moment there is death, because there is one movement of thought and

6
00:00:39,320 --> 00:00:44,280
then it dies and then another movement of thought and so on. So, we are dying and being

7
00:00:44,280 --> 00:00:50,880
reborn every moment, that kind of death is called momentary death, the momentary dissolution

8
00:00:50,880 --> 00:00:57,480
of formations, nor the death of conventional metaphorical usage in such expression as the

9
00:00:57,480 --> 00:01:10,480
tree is dead, the matter is dead and so on. In the research, the mother is dead,

10
00:01:10,480 --> 00:01:24,200
the iron is dead actually, not metal, iron is dead. Now, there is some kind of elkami in

11
00:01:24,200 --> 00:01:33,040
the West as well as in the East and these people, what do you call it, elkamist and these

12
00:01:33,040 --> 00:01:40,800
people try to do something to be, to be metal, to make them beneficial to people and so

13
00:01:40,800 --> 00:01:53,200
there is a saying in Bhamism. If you can kill, if you can kill the iron, you can feed

14
00:01:53,200 --> 00:02:03,000
the whole country, I don't know what that means, that means if you can make iron produce

15
00:02:03,000 --> 00:02:11,720
something maybe or there is a saying that if you can kill the iron, then you can turn

16
00:02:11,720 --> 00:02:24,320
lead into gold and you can cure diseases. They try to burn these methods and make them

17
00:02:24,320 --> 00:02:32,680
into ashes and they mix them with their honey and so on and they treat people and they

18
00:02:32,680 --> 00:02:45,960
will believe that such ashes can treat many kinds of diseases. So, when we say the iron

19
00:02:45,960 --> 00:02:54,600
is dead, the cover is dead and we are using conventional usage. So, that kind of dead

20
00:02:54,600 --> 00:03:03,340
is not meant here. So, what is, what is meant here is dead is just end of one life. As

21
00:03:03,340 --> 00:03:09,640
in dead ends here, it is of true kinds and that is to say timely death and untimely death.

22
00:03:09,640 --> 00:03:14,120
Timely death comes about with the exhaustion of metal or with the exhaustion of a lifespan

23
00:03:14,120 --> 00:03:21,760
of with both. Untimely death comes about through the cover that interrupts or that cut off

24
00:03:21,760 --> 00:03:32,040
the other life-pro-inducing comma. Now, death through exhaustion of metal, that means that

25
00:03:32,040 --> 00:03:39,720
death through the exhaustion of the force of comma. That means, suppose the lifespan

26
00:03:39,720 --> 00:03:51,880
is now 100 years. But, it was in whose comma, cannot give him 100 years, may die before

27
00:03:51,880 --> 00:03:58,040
he reaches 100 years. So, he may die of age of 50, 40 or even younger. So, when he dies

28
00:03:58,040 --> 00:04:09,480
in that way, he said to die through exhaustion of merit or exhaustion of force of comma.

29
00:04:09,480 --> 00:04:17,480
That is why now people, many people die before they reach to the end of their life span.

30
00:04:17,480 --> 00:04:26,360
Sometimes people have very strong comma and they, they, they, they, they comma could make

31
00:04:26,360 --> 00:04:34,880
them live say 1000 years, many hundreds of years. But, if, if people, if they are born

32
00:04:34,880 --> 00:04:42,000
in a, at a time when human beings live only 100 years, then they will die at the end

33
00:04:42,000 --> 00:04:50,080
of their span. So, such death is called death through exhaustion of a lifespan. They may

34
00:04:50,080 --> 00:04:57,040
have comma which can make them live more than 100 years. But, since they are born in the

35
00:04:57,040 --> 00:05:02,720
time when people live only 100 years, they have to die at the age of 100 years. So, that

36
00:05:02,720 --> 00:05:15,080
kind of death is called an exhaustion of a lifespan. The death through exhaustion of both

37
00:05:15,080 --> 00:05:27,760
is they, he wasn't, has a comma which can cause him to live 100 years. And he, he, he

38
00:05:27,760 --> 00:05:34,720
is reborn at a time when people live 100 years. And he died, he died at the age of 100

39
00:05:34,720 --> 00:05:44,080
years. So, that is death through both. And the last one is untimely death. That is mostly

40
00:05:44,080 --> 00:05:49,280
tragic death. The time for the death of those whose continuity is interrupted by comma

41
00:05:49,280 --> 00:05:54,280
capable of causing them to fall from their place at that very moment as in the case of

42
00:05:54,280 --> 00:06:00,560
Doucimara, Kalaburaga, etc., or for the death of those whose life continuity is interrupted

43
00:06:00,560 --> 00:06:08,160
by a source of weapons, etc. due to previous karma. Now, sometimes people do a very

44
00:06:08,160 --> 00:06:14,920
heinous offense or crime. I mean, killing an other hand or something like that. And such

45
00:06:14,920 --> 00:06:30,120
people sometimes die immediately. Here is given Doucimara. So, he, he tried to kill Saripoda.

46
00:06:30,120 --> 00:06:38,520
You know, wow, wow. One day Saripoda shaved his head and so his head was very smooth and

47
00:06:38,520 --> 00:06:47,120
shining. So, Doucimara wanted to hit him. And so he, he hit him on the head. And at that

48
00:06:47,120 --> 00:06:58,120
time, Saripoda was in, in, in Jhana and in Samapati. So, nothing happened to Saripoda. But

49
00:06:58,120 --> 00:07:05,520
because of that bad karma, he died in immediately and Doucimara, something like that.

50
00:07:05,520 --> 00:07:15,840
And Kalaburaga means a king who tried, who killed a saint, who practiced patience. So,

51
00:07:15,840 --> 00:07:21,480
there's a saint called Khandiwara. He was, he was very famous and once he came to the city

52
00:07:21,480 --> 00:07:27,400
and sat in the garden and then he, he fresh a grove of the king. And so the king was

53
00:07:27,400 --> 00:07:37,880
asleep and so his queens and concubines went to this, to, to listen to Doucimara. Now,

54
00:07:37,880 --> 00:07:46,280
when the king woke up, he, he didn't see his wives and so he looked, looked around and

55
00:07:46,280 --> 00:07:53,160
then when he wished he, he, he, he, he asked, what are you? So, I'm a, I'm a helmet. What

56
00:07:53,160 --> 00:08:00,160
do you practice? I practice patience. What is patience? Patience being forbearing the whatever

57
00:08:00,160 --> 00:08:05,400
people do to you. Then he said, let us see your patience. So, he called his, what

58
00:08:05,400 --> 00:08:11,640
you call executioner and let, let them first cut off the hands of the, see it and ask him,

59
00:08:11,640 --> 00:08:19,800
are you still patient? He said, patience does not, does not lie in the hands. Then we,

60
00:08:19,800 --> 00:08:27,000
he had the, the feet cut off and he asked him again instead. I don't, I, I'm not angry

61
00:08:27,000 --> 00:08:37,160
with you. I'm not upset. Patience is here. So, he kicked him and he went away. So, when

62
00:08:37,160 --> 00:08:43,440
the king went away, the, the general heard about that and so he rushed to the, to be

63
00:08:43,440 --> 00:08:52,440
safe, to be helmet and ask him to be angry with the, with the king. So, he said, please

64
00:08:52,440 --> 00:08:58,080
be angry with the king. Because if you get angry with the king, then he will suffer less.

65
00:08:58,080 --> 00:09:04,520
But, but the, the say said, people like me do not get angry. Let him live long or something

66
00:09:04,520 --> 00:09:14,240
like that. But, because of that, that offense, he was, he was followed by the earth. He

67
00:09:14,240 --> 00:09:23,520
was consumed by the earth. So, such, such death is called untimely death. They, they have

68
00:09:23,520 --> 00:09:30,760
first gamma and they have lifespan, but they are crime is so, so, so bad that they had

69
00:09:30,760 --> 00:09:37,760
to die. And sometimes they need to be, to die in an accident. But, he's, I call untimely death.

70
00:09:37,760 --> 00:09:45,920
But, but this, about it, he got angry, his karma would be less. How, his action was the

71
00:09:45,920 --> 00:09:54,240
same action? Yeah. Now, how would his karma, why, be less? Because somebody got angry,

72
00:09:54,240 --> 00:10:09,200
which is, yes. But, it is, it is like, with regard to wholesome, wholesome acts. It

73
00:10:09,200 --> 00:10:18,800
is like, showing seed on a, and a field, which is further and which is not further. So,

74
00:10:18,800 --> 00:10:32,000
when the person, and also, they be killing, the, the effect or result of killing, differs

75
00:10:32,000 --> 00:10:39,640
with the person, with the, with the, with the being who is killed. So, if the person

76
00:10:39,640 --> 00:10:46,080
killed is virtuous, then there is more aqueous are. And if the person, or being killed

77
00:10:46,080 --> 00:10:52,400
is larger, then there is more aqueous land. So, there are variations in degree of offense,

78
00:10:52,400 --> 00:11:01,920
with regard to, the virtue of the person being killed. And also, whether there is more

79
00:11:01,920 --> 00:11:09,080
effort needed to kill. So, to kill an end, and to kill an elephant. I'd not say, I

80
00:11:09,080 --> 00:11:16,600
mean, not, they, they do not have the same degree of aqueous, allow unhoseners, because

81
00:11:16,600 --> 00:11:22,440
to kill, to kill an elephant, you have to make much effort. So, the more effort there

82
00:11:22,440 --> 00:11:30,200
is, the more aqueous land. So, what you are saying is that the, the generalist, misunderstood

83
00:11:30,200 --> 00:11:36,760
of this thing. No, the general, the general, the general did not misunderstand him. But,

84
00:11:36,760 --> 00:11:45,040
the general wanted to, may be, often less damaging to the king, because if the monk, I

85
00:11:45,040 --> 00:11:53,720
mean, if the, if the monk, if the monk gets into his land, if he does less, less virtuous.

86
00:11:53,720 --> 00:11:58,280
But, it's a funny example, because it's a little bit of difference in my, in my hair, but

87
00:11:58,280 --> 00:12:10,440
a mangano approach might be, to be convinced, to help the king. He might be angry at him,

88
00:12:10,440 --> 00:12:16,760
but that, in a sense, is a plan, virtue. I mean, because he's, once the, betterment

89
00:12:16,760 --> 00:12:23,040
of the king, the king's punishment, not to be so bad, and that's the argument that, that's

90
00:12:23,040 --> 00:12:30,240
being given to him, it's a kind of a funny, virtuous argument. Because he would only

91
00:12:30,240 --> 00:12:37,240
be convinced by virtue, that, to help the king, they would be angry. It wouldn't be out

92
00:12:37,240 --> 00:12:44,200
of, anger, out of actual anger. It would be, I don't know, I don't know, I don't

93
00:12:44,200 --> 00:12:54,600
know, but this, I don't know. But when, when he, he, he gets angry, then he, he is, it, it

94
00:12:54,600 --> 00:13:00,960
will amount to breaking some wealth or something, say, I will not angry, whatever, people

95
00:13:00,960 --> 00:13:07,920
do to me. So he, he might have, he might have made that far. And so he kept it, he kept

96
00:13:07,920 --> 00:13:14,920
it, until he stepped. So it's, another theoretical question. He said something about some

97
00:13:14,920 --> 00:13:20,920
people who has the karma to live 1,000 years. He never was living in a hundred years.

98
00:13:20,920 --> 00:13:33,920
What happened to the man in the city? I'm kind of a credit. I wish he could.

99
00:13:33,920 --> 00:13:47,920
No. In order for karma to, to ripen, to give results, we need different conditions.

100
00:13:47,920 --> 00:13:53,920
Sometimes the time of the ripening of the karma, the place of the ripening of the karma,

101
00:13:53,920 --> 00:14:00,240
the condition of the ripening of the karma and so on. So when karma gives results, it

102
00:14:00,240 --> 00:14:14,240
depends on these, these conditions. So when, you know, Mahazi Sarah in one of his talks

103
00:14:14,240 --> 00:14:25,560
said, I'm afraid that many Bali's people will be reborn in United States. Why? Because

104
00:14:25,560 --> 00:14:31,880
they do a lot of meritorious deeds. And so these, especially, these meritorious deeds

105
00:14:31,880 --> 00:14:45,200
will give them great results. And bhamma is a poor country. And so when the karma is able

106
00:14:45,200 --> 00:14:53,480
to make him a billionaire, then bhamma cannot prove him a millionaire, something like that.

107
00:14:53,480 --> 00:15:01,560
So in order for the karma to give full results, then it needs favorable conditions,

108
00:15:01,560 --> 00:15:08,480
favorable effort made by the person, and then time and many, many, many things.

109
00:15:08,480 --> 00:15:15,480
So is that possible that someone who dies young? Can you take, can you ever take that

110
00:15:15,480 --> 00:15:22,480
as a sign of good karma? I mean, if they have a more favorable rebirth having died young.

111
00:15:22,480 --> 00:15:31,480
But to, to die young is the result of not so good karma. I mean, not to live long.

112
00:15:31,480 --> 00:15:36,680
Let's say, okay. Let's say if they kept on, you know, their life was so miserable.

113
00:15:36,680 --> 00:15:41,480
Someone who's born in, you know, in a ghetto or something, there's no chance that they're

114
00:15:41,480 --> 00:15:45,680
ever going to contact with a dharma or something. Yeah. And they die young.

115
00:15:45,680 --> 00:15:53,080
So they would have a better chance in another environment, or is it always a sign of that

116
00:15:53,080 --> 00:15:54,080
karma?

117
00:15:54,080 --> 00:16:02,680
Not bad karma, but people are born as human beings as a result of wholesome karma.

118
00:16:02,680 --> 00:16:08,680
Let us call them wholesome karma. But this wholesome karma has a different capability,

119
00:16:08,680 --> 00:16:15,680
different ability, different, different ability, different power. So some kamma can even only

120
00:16:15,680 --> 00:16:24,680
say 10 years, some wholesome karma can give only 10 years. Then the other kamma may be able

121
00:16:24,680 --> 00:16:33,680
to give them 20 years, 30 years, and so on. So the, whenever a person is reborn as a human

122
00:16:33,680 --> 00:16:40,680
being, it is said to be, is that it will be born as a result of good karma. But that

123
00:16:40,680 --> 00:16:53,680
good karma may be able to give them only 10 years, 20 years, and so on. So that was

124
00:16:53,680 --> 00:17:01,680
not as a result of bad karma, but as a result of good karma, which is not good enough

125
00:17:01,680 --> 00:17:12,680
to make him live longer. So in other words, that children that die young, that don't have

126
00:17:12,680 --> 00:17:18,680
as much good karma as someone that's looked at 85 or not.

127
00:17:18,680 --> 00:17:40,680
Then the collection on death, I think they are not so difficult, how to reflect on death

128
00:17:40,680 --> 00:17:54,680
in different ways for a graph, say, a graph of seven. When some, some exercise, it merely

129
00:17:54,680 --> 00:18:00,680
in this way, that means that will take place, that will come, I will die one day or something

130
00:18:00,680 --> 00:18:06,680
like that. Their hindrances get suppressed, their mindfulness becomes established with

131
00:18:06,680 --> 00:18:13,680
death as its object and the meditation subject reaches access. Those are gifted people.

132
00:18:13,680 --> 00:18:19,680
But if, if one finds that it does not get too far, should do his recollection of death

133
00:18:19,680 --> 00:18:26,080
in eight ways, and that is, that's having the appearance of a murderer as the ruin of

134
00:18:26,080 --> 00:18:34,380
success. Here, ruin of, not the ruin of success, but success and failure. And three

135
00:18:34,380 --> 00:18:41,380
by comparison, four as to sharing the body with many, that means we have to share a body

136
00:18:41,380 --> 00:18:48,880
with other warms or insects or germs and all these things, something like that, and then

137
00:18:48,880 --> 00:18:55,180
has to be afraidly of life as signless as to the limitedness of the extent and as to

138
00:18:55,180 --> 00:19:01,880
the shortness of the moment. So we can reflect on death in different ways.

139
00:19:01,880 --> 00:19:20,380
And at the end, towards the end of the recollection, there is a long foot note discussing

140
00:19:20,380 --> 00:19:42,580
Panyati, something we will discuss next week. But here, today, let me see, there are

141
00:19:42,580 --> 00:19:53,580
many examples given in this, and we cannot go to every reference, because on page 250,

142
00:19:53,580 --> 00:20:01,080
there are many references given in the foot note six. Say, Mahasamaja, Manda, to Mahasu,

143
00:20:01,080 --> 00:20:14,880
Manda, Manda, Jadila, and so on. And then, oh yes, some of these, these persons mentioned

144
00:20:14,880 --> 00:20:26,380
in these pages are found in, mostly in Jadagar, Jadagar tales, and also in other soldiers.

145
00:20:26,380 --> 00:20:36,000
And then, for a graph 19, Vasudhi, or Baladhi, and so on. Mostly, they are taken from

146
00:20:36,000 --> 00:20:43,880
Hindu books, actually, in the Mahabhartha, with these names appear. And so there is some

147
00:20:43,880 --> 00:21:00,280
kind of, some kind of relationship between Hindu stories and Buddhist stories. Some Hindu

148
00:21:00,280 --> 00:21:11,800
stories are told as Buddhist stories in the Jadagar. So these persons, Vasudhi, Balad

149
00:21:11,800 --> 00:21:17,080
they were Bhimas in a Buddhist Tiva and Chandu Raksoa. They are mentioned in one of the

150
00:21:17,080 --> 00:21:25,200
Jadagar's also, but I think they originally came from the Hindu sources. And the Mahabhartha,

151
00:21:25,200 --> 00:21:35,000
there are five brothers, and two leaders, I thought, again, among them, so on. But the main

152
00:21:35,000 --> 00:21:44,160
point here is, such persons who are of great marriage, who are of great strength, who

153
00:21:44,160 --> 00:21:52,600
poses super normal powers, even the Buddha had to die. And so there is no one to say

154
00:21:52,600 --> 00:22:01,040
we will not die. So comparing ourselves with these persons, we can reflect on death. So

155
00:22:01,040 --> 00:22:14,280
that will come to a fun day. And this recollection also has to be done with wisdom and

156
00:22:14,280 --> 00:22:29,120
with understanding. Otherwise, let me see, if he exercises his parabhartha, if he exercises

157
00:22:29,120 --> 00:22:36,120
his attention unwisely in recollecting the possible death of an able person's soul arises,

158
00:22:36,120 --> 00:22:40,800
just in a mother on recollecting the death of a beloved child's evil. And blackness

159
00:22:40,800 --> 00:22:47,640
arises in recollecting the death of a Giset's evil. As an enemy, they are recollecting

160
00:22:47,640 --> 00:22:52,760
the death of their enemies. And no signs of urgency arise on recollecting the death of

161
00:22:52,760 --> 00:23:00,760
a neutral people, as happens in a cox burner on seeing the dead body. And anxiety arises

162
00:23:00,760 --> 00:23:06,440
on recollecting once of death, as happens in a timid person on seeing a mother with a

163
00:23:06,440 --> 00:23:14,360
quiet spirit. In all that, there is neither mindfulness or sense of urgency nor knowledge.

164
00:23:14,360 --> 00:23:18,760
So he should look here and there at beings that have been killed have died and referred

165
00:23:18,760 --> 00:23:25,000
to the death of beings already dead, but formally seeing and enjoying good things doing

166
00:23:25,000 --> 00:23:29,800
so with mindfulness, with the sense of urgency and with knowledge, after which he can

167
00:23:29,800 --> 00:23:37,360
exercise his attention and the way beginning death will take place. So when you practice

168
00:23:37,360 --> 00:23:47,560
this kind of marriage, you have to be very careful. And it is, it is amazing that thinking

169
00:23:47,560 --> 00:24:07,400
of death makes you less afraid of death. And also the recollection on death can reduce to

170
00:24:07,400 --> 00:24:18,000
a very great degree, your pride or attachment. You may have experienced something, say, you

171
00:24:18,000 --> 00:24:26,000
are very sick over here and you told you, you are going to die. In that case, you don't

172
00:24:26,000 --> 00:24:35,400
have any attachment, you don't have any anger or whatever, you don't have any pride in yourself.

173
00:24:35,400 --> 00:24:47,040
You are very, very worried about it, not proud, very humble, right? And you are like

174
00:24:47,040 --> 00:24:52,680
an other hand at a time, because you don't want anything, you do not, you are not attached

175
00:24:52,680 --> 00:25:00,240
to anything at that time. So recollection on death is a very, very good weapon in, I

176
00:25:00,240 --> 00:25:12,280
will go fighting against attachment, hatred, pride and others. And when death really comes,

177
00:25:12,280 --> 00:25:22,440
say, you will be able to flee with more calmness than those who do not practice this kind

178
00:25:22,440 --> 00:25:31,440
of meditation. That is why Buddha said, months must practice every day with recollection

179
00:25:31,440 --> 00:25:42,160
of the Buddha, loving kind of meditation, and powerness of the body, meditation, and

180
00:25:42,160 --> 00:25:54,880
then it is meditation. The recollection of the Buddha, the loving kind of that, the

181
00:25:54,880 --> 00:26:05,960
tolerance of the body, the connection of death. Oh, one thing I want to say is, for

182
00:26:05,960 --> 00:26:13,480
after the fine, has to be limitedness of the extent. The extent of human life is short

183
00:26:13,480 --> 00:26:21,600
now. One who lives long, lives a hundred years, more or less. But what Buddha meant here

184
00:26:21,600 --> 00:26:30,960
is, one who lives long, lives a hundred years, or a little more, not more or less, or

185
00:26:30,960 --> 00:26:40,160
a little more. That means immediately, to be 120 years, 150 years, 106 years, so on, not

186
00:26:40,160 --> 00:26:49,600
more or less. So one who lives long, lives a hundred years, or a little more. Then there

187
00:26:49,600 --> 00:27:03,840
is two, two, and it is not there. Okay, I think that's all. So we will discuss

188
00:27:03,840 --> 00:27:11,600
cognitive next to it, and then go to mindfulness of the Buddha, go into the, I mean the section

189
00:27:11,600 --> 00:27:31,600
on mindfulness of the body. After the cage, now, 200 and 70, not many days.

190
00:27:31,600 --> 00:28:00,440
All right.

191
00:28:00,440 --> 00:28:05,440
.

192
00:28:05,440 --> 00:28:10,440
.

193
00:28:10,440 --> 00:28:15,440
.

194
00:28:15,440 --> 00:28:20,440
.

195
00:28:20,440 --> 00:28:25,440
..

196
00:28:25,440 --> 00:28:28,440
..

