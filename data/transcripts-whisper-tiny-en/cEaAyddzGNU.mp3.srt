1
00:00:00,000 --> 00:00:13,240
So good evening and welcome to our Dhamma session tonight.

2
00:00:13,240 --> 00:00:37,880
The topic for tonight is for lack of a really good translation, it's the Pali word ballat ballat.

3
00:00:37,880 --> 00:00:43,320
Ballat is often translated as power, but I want to go a little broader than that.

4
00:00:43,320 --> 00:00:45,560
Power is a good translation.

5
00:00:45,560 --> 00:00:47,680
I think it can also mean energy.

6
00:00:47,680 --> 00:00:59,080
Power gives a little bit of a specific connotation in English that may not be exact.

7
00:00:59,080 --> 00:01:09,200
There's a lot of meditators or people who practice meditation in various traditions talk

8
00:01:09,200 --> 00:01:21,080
a lot about energy.

9
00:01:21,080 --> 00:01:27,640
So I thought it'd be, that's one reason why it would be interesting to talk about energy

10
00:01:27,640 --> 00:01:31,800
and Buddhism, energy, power, these are these ideas because I think it's one and the same

11
00:01:31,800 --> 00:01:36,080
really.

12
00:01:36,080 --> 00:01:46,360
But another important reason is to just remind us the power and thereby the consequences

13
00:01:46,360 --> 00:01:56,440
of things, the potential.

14
00:01:56,440 --> 00:02:05,000
There is a very important part of our learning experience.

15
00:02:05,000 --> 00:02:09,640
There's a power to so many things, we talk about this in daily discourse and if you think

16
00:02:09,640 --> 00:02:22,520
about it, there's much to do with power that is of interest to Buddhists and meditators.

17
00:02:22,520 --> 00:02:33,360
Various types of power that exist, the Buddha talked a lot about different kinds of power.

18
00:02:33,360 --> 00:02:40,360
So the first powers that the Buddha talked about are physical power, like the power of

19
00:02:40,360 --> 00:02:41,920
the sun.

20
00:02:41,920 --> 00:02:47,600
The sun's a very powerful thing, without it we'd all be dead, we would have never been

21
00:02:47,600 --> 00:02:48,840
born.

22
00:02:48,840 --> 00:02:54,960
There would be no life on earth without the sun.

23
00:02:54,960 --> 00:03:01,000
Not in this form anyway, there might be angels, there would be spirits, but the sun is

24
00:03:01,000 --> 00:03:07,400
a powerful force in our lives.

25
00:03:07,400 --> 00:03:14,720
Power of the moon, the sun and the moon were like gods in ancient India, there were these

26
00:03:14,720 --> 00:03:41,360
super mundane forces, the moon has power over the earth, the tides, some people say our

27
00:03:41,360 --> 00:03:46,720
emotions, our moods can be even affected by the moon.

28
00:03:46,720 --> 00:03:53,240
And of course, the many physical powers on earth, there's the power of storms, the power

29
00:03:53,240 --> 00:04:10,200
of earthquakes, the powers that exist, the forces of gravity, if you jump out of a ten-story

30
00:04:10,200 --> 00:04:25,200
building, there's a lot of power that takes its toll on you, on your forces you, makes

31
00:04:25,200 --> 00:04:32,960
you fall to your death.

32
00:04:32,960 --> 00:04:44,440
And these may seem inconsequential in Buddhism, but they're not, of course, they're very

33
00:04:44,440 --> 00:04:49,680
important, it's important for us to remember that many things in this world are outside

34
00:04:49,680 --> 00:04:50,680
of our control.

35
00:04:50,680 --> 00:04:59,320
If you've ever been in a car accident, it's a common enough occurrence that many of us have been

36
00:04:59,320 --> 00:05:06,840
in a car accident, it gives you a sense, a small sense of the power that exists outside

37
00:05:06,840 --> 00:05:11,160
of our control.

38
00:05:11,160 --> 00:05:17,840
So used to being in control, right, even when we drive the car, when we drive the car we feel

39
00:05:17,840 --> 00:05:23,680
so much in control, when we get this false sense of security that the car is a part of

40
00:05:23,680 --> 00:05:29,520
us doing what we tell it, just like our body, right?

41
00:05:29,520 --> 00:05:34,240
But just like our body, it doesn't really do it, we tell it.

42
00:05:34,240 --> 00:05:40,680
I remember getting my first serious car accident was on an icy patch of road, spun totally

43
00:05:40,680 --> 00:05:49,360
out of control and almost hit a telephone pole, and just the power, when I remember so

44
00:05:49,360 --> 00:06:07,000
much, was the power, the force of gravity and the force of inertia.

45
00:06:07,000 --> 00:06:14,000
So many things out of our control.

46
00:06:14,000 --> 00:06:19,160
The way we live our lives trying to control, trying to set up our lives in a way that's

47
00:06:19,160 --> 00:06:29,440
always satisfying, that's always pleasant, it's not really tenable, it's not a very wise

48
00:06:29,440 --> 00:06:41,240
way to be, it's a great way to set yourself up for disappointment, set yourself up for

49
00:06:41,240 --> 00:06:53,120
trauma and stress and suffering.

50
00:06:53,120 --> 00:06:58,760
But granted it is one of the least important forces, it's important to realize that physical

51
00:06:58,760 --> 00:07:03,800
forces are out of control, but there are more important forces.

52
00:07:03,800 --> 00:07:08,520
If there weren't, we'd be in serious trouble indeed, because these things that are out

53
00:07:08,520 --> 00:07:14,000
of our control, if that was the most powerful force, we really wouldn't have any way

54
00:07:14,000 --> 00:07:20,760
of overcoming it.

55
00:07:20,760 --> 00:07:28,880
We can't prevent earthquakes, we can't prevent old age, we can't prevent death, physical

56
00:07:28,880 --> 00:07:51,720
forces out of our control.

57
00:07:51,720 --> 00:08:01,480
Or suppose someone humorous sounding forces, is the force of women, might be surprised

58
00:08:01,480 --> 00:08:08,440
to know that the Buddhist specific of the mentioned, the power of women, I think he even

59
00:08:08,440 --> 00:08:12,360
mentioned it when he was talking about the power of the sun, the power of the moon.

60
00:08:12,360 --> 00:08:19,160
I think the sun, the power of the sun, the power of the moon, the power of holy people,

61
00:08:19,160 --> 00:08:25,160
the magical powers, and then the power of women.

62
00:08:25,160 --> 00:08:31,080
But this gets into a different type of power, the power here is the power of manipulation,

63
00:08:31,080 --> 00:08:39,640
how women manipulate men's emotions and men manipulate women's emotions.

64
00:08:39,640 --> 00:08:43,800
And of course men manipulate men's, how we manipulate each other's emotions.

65
00:08:43,800 --> 00:08:47,120
Again, this is the reason he brings up women is because he's talking to a bunch of

66
00:08:47,120 --> 00:08:59,080
celibate, mainly heterosexual men, and in the hetero, I think it's called heteronormative

67
00:08:59,080 --> 00:09:06,920
society where women did have this power over men, even though they were generally powerless.

68
00:09:06,920 --> 00:09:14,840
If you want to turn the tables, you could say, well, the power of men is equally fearsome.

69
00:09:14,840 --> 00:09:24,320
They would beat their wives and control their wives, even rape their wives, but talking

70
00:09:24,320 --> 00:09:27,920
to men, he reminds them of the power of women.

71
00:09:27,920 --> 00:09:36,360
He walked into the village for arms and some woman begiles you and you're tormented with

72
00:09:36,360 --> 00:09:44,720
lust and desire and so on, but this gets into the more important power of the power

73
00:09:44,720 --> 00:09:49,640
because it's not actually the woman or the man or whoever has power.

74
00:09:49,640 --> 00:10:02,800
It's the power of the mind, starting with the power of defilement.

75
00:10:02,800 --> 00:10:12,560
You can control whether people are going to hurt you or manipulate you.

76
00:10:12,560 --> 00:10:27,560
You can control how you react, and so we have things like we talk about the power of

77
00:10:27,560 --> 00:10:28,560
positive thinking.

78
00:10:28,560 --> 00:10:33,720
Well, there's also the power of negative thinking, and this is where meditation really

79
00:10:33,720 --> 00:10:34,720
begins to work.

80
00:10:34,720 --> 00:10:40,640
It's what we begin to see in the very beginning of our meditation practice, is that much

81
00:10:40,640 --> 00:10:45,000
of who we are is made up of the patterns of behavior that we develop, cultivate, throw

82
00:10:45,000 --> 00:10:52,520
to our lives, good and bad.

83
00:10:52,520 --> 00:11:02,720
We cultivate much that's to our benefit, learning skills, amazing how the skills we can learn.

84
00:11:02,720 --> 00:11:07,280
Even just to speak is quite a quite an accomplishment, how we can string together all these

85
00:11:07,280 --> 00:11:12,520
complex words and grammar without even thinking.

86
00:11:12,520 --> 00:11:19,680
I'm able to use all the correct tenses and pronouns and word orders and synonyms and

87
00:11:19,680 --> 00:11:23,240
endonyms, big words, small words, all in the right order.

88
00:11:23,240 --> 00:11:27,040
It's quite incredible without even thinking.

89
00:11:27,040 --> 00:11:34,600
It's a pattern of behavior, it's a habit that I've cultivated, couldn't stop if I wanted

90
00:11:34,600 --> 00:11:55,960
to start saying the words in a different order, it would take a whole new habit to learn.

91
00:11:55,960 --> 00:12:01,160
But much more important than those kinds of habits, of course, are the habits of ethical

92
00:12:01,160 --> 00:12:02,160
nature.

93
00:12:02,160 --> 00:12:09,120
And this is where Buddhism really focuses its attention in the Abidhamma, for example,

94
00:12:09,120 --> 00:12:14,200
on our ethical habits.

95
00:12:14,200 --> 00:12:20,840
How we're habitually inclined to dislike pain, how we're habitually inclined to fear certain

96
00:12:20,840 --> 00:12:30,280
things, to hate and to like and to love and to be attracted and repulsed by certain things.

97
00:12:30,280 --> 00:12:47,640
How we're inclined to be arrogant, conceited, how we're inclined to be, how we're inclined

98
00:12:47,640 --> 00:12:51,040
to react.

99
00:12:51,040 --> 00:13:08,120
So it's a curious thing, a common question for meditators is that they practice correctly,

100
00:13:08,120 --> 00:13:14,360
and they are able to work out some of the problems in the mind, so they think, yes, I

101
00:13:14,360 --> 00:13:21,280
practice and my anger's gone, I'm no longer angry or I've practiced and I'm no longer

102
00:13:21,280 --> 00:13:30,000
lusting after this or that, but they wonder why it comes back, they're doing everything

103
00:13:30,000 --> 00:13:38,160
right and it worked, but then it didn't do anything, the same problems come back.

104
00:13:38,160 --> 00:13:42,280
So we have to understand, it's not magic and it's not like these are something bedded

105
00:13:42,280 --> 00:13:46,840
ingrained part of ourselves, that you're never going to be free from, you have to understand

106
00:13:46,840 --> 00:13:52,200
where they come from, if you want reassurance, that you are that meditation is doing something,

107
00:13:52,200 --> 00:13:57,360
you have to understand what these things are, you have to understand this concept of the

108
00:13:57,360 --> 00:14:06,560
power of cultivation, the power of cultivating certain habits, good habits, bad habits.

109
00:14:06,560 --> 00:14:11,560
So when you look at it that way, you understand that the anger and the greed and the delusion

110
00:14:11,560 --> 00:14:17,640
that's inside of us is something that we've cultivated as habit, it keeps coming back because

111
00:14:17,640 --> 00:14:27,720
it's an echo of past activities, past inclinations, and if we keep feeding it, it'll get

112
00:14:27,720 --> 00:14:31,880
stronger, it'll stay strong.

113
00:14:31,880 --> 00:14:36,680
But if we stop feeding it, it doesn't just disappear, there's a lot of power behind it.

114
00:14:36,680 --> 00:14:41,520
This is the most important power for a meditator to understand in the beginning, for some

115
00:14:41,520 --> 00:14:47,640
anyway, just to understand that meditating isn't just going to turn off your bad habits,

116
00:14:47,640 --> 00:14:55,280
but it's going to stop feeding them, it's going to cultivate opposing habits, habits

117
00:14:55,280 --> 00:15:11,160
of contentment, habits of clarity, wisdom, humility, patience, and by cultivating these,

118
00:15:11,160 --> 00:15:16,680
they have a power, this is the power of good things, so maybe we'll talk about the power

119
00:15:16,680 --> 00:15:20,760
of positive thinking, it's apparently this idea that if you think positively you get

120
00:15:20,760 --> 00:15:28,720
everything you want, which may be true, people claim that you can manifest all the objects

121
00:15:28,720 --> 00:15:33,000
of your desire, if you just have the right kind of thinking, but for a Buddhist point

122
00:15:33,000 --> 00:15:42,200
of view, it's not such a powerful thing, it's a dangerous thing, for the very reason

123
00:15:42,200 --> 00:15:51,360
that it doesn't, it increases your desire to get what you want, if you want to destroy

124
00:15:51,360 --> 00:16:00,240
someone completely, easiest way is to give them everything they want, because I'll take

125
00:16:00,240 --> 00:16:08,160
it, maybe not the easiest way, but it's the most sure way to destroy someone, because

126
00:16:08,160 --> 00:16:19,120
I'll keep taking and taking and taking and path to self-destruction, self-suffering work,

127
00:16:19,120 --> 00:16:22,680
suffering comes from not getting what you want, the only way you cannot get what you want

128
00:16:22,680 --> 00:16:28,880
is with wanting, the more you get, the more you want, the more you want, the more you

129
00:16:28,880 --> 00:16:44,440
suffer.

130
00:16:44,440 --> 00:16:56,640
So our meditation is about seeing these things, about seeing our, seeing that the power

131
00:16:56,640 --> 00:17:07,760
that exists in the mind, and about changing the habits that we have, that we've cultivated,

132
00:17:07,760 --> 00:17:16,880
that we, the things that we give power to, the things that we put our energy into, everything,

133
00:17:16,880 --> 00:17:21,680
when you work, when you have a job, when you work, cultivating, you're giving energy, so

134
00:17:21,680 --> 00:17:27,800
many habits, you're cultivating so many good and bad habits, when you, when you play games,

135
00:17:27,800 --> 00:17:39,040
so, when children play tag or hide and go seek, putting energy into, from very early

136
00:17:39,040 --> 00:17:52,720
age, we put energy into things and we cultivate habits, everything we do, we can't escape

137
00:17:52,720 --> 00:18:01,640
it, as a part of, part of the path is this realization where you start to get a little

138
00:18:01,640 --> 00:18:09,280
bit perturbed by, by your ignorance and how you've been, you've ignorantly been going

139
00:18:09,280 --> 00:18:18,360
about, cultivating lots of bad habits, and so, so wake up calm, where you start to decide

140
00:18:18,360 --> 00:18:27,040
to change your habits and to work towards cultivating good habits.

141
00:18:27,040 --> 00:18:35,200
Another power, I think, beyond this is, beyond the power of the habits is sort of hinting

142
00:18:35,200 --> 00:18:47,280
at it here is the power of wisdom, so ordinary meditation, when people talk about energy,

143
00:18:47,280 --> 00:18:51,320
this is where they get this idea that there's energy flowing to their body or this,

144
00:18:51,320 --> 00:18:59,640
you know, the energy that comes from meditation, this is much more to do with tranquility

145
00:18:59,640 --> 00:19:06,640
meditation, these types of meditation that can be identifiable by the power that they

146
00:19:06,640 --> 00:19:13,360
give to the mind, just powerful feelings really, and their powerful in the sense that

147
00:19:13,360 --> 00:19:19,520
they focus the mind and they tranquilize the mind and they stabilize the mind, they quiet

148
00:19:19,520 --> 00:19:25,920
the mind, they protect the mind, there are many, many good qualities to them.

149
00:19:25,920 --> 00:19:31,280
They even allow for other sorts of power for reading people's minds, for remembering

150
00:19:31,280 --> 00:19:40,600
your past lives, such sharp memory you can remember, even before you were born, all these

151
00:19:40,600 --> 00:19:51,800
magical powers that people, astral, travel, it's supposed to come from meditation.

152
00:19:51,800 --> 00:19:56,600
But that's just another one of these powers of, you know, habit, you work at it enough

153
00:19:56,600 --> 00:20:03,960
and it changes from makeup of your mind, but wisdom is somehow different, wisdom isn't

154
00:20:03,960 --> 00:20:08,160
like any other habit because it helps you understand your habits and it helps you see

155
00:20:08,160 --> 00:20:13,600
through them and to see that they're not your habits, that you're not in control rather

156
00:20:13,600 --> 00:20:19,320
than just creating new habits, you understand this through our, through the meditation

157
00:20:19,320 --> 00:20:28,680
of being mindful, you understand the habit creating activity and you start to, you start

158
00:20:28,680 --> 00:20:35,840
to see through it and see beyond it and realize there's no point to it.

159
00:20:35,840 --> 00:20:40,480
It's much more powerful, but it's a power that doesn't feel powerful.

160
00:20:40,480 --> 00:20:49,080
It feels rather peaceful, rather innocent in fact, and that's why mindfulness is such

161
00:20:49,080 --> 00:20:57,920
a, it's easy to be, much hard to explain, but I think when you practice, it's hard

162
00:20:57,920 --> 00:21:04,280
to avoid understanding this concept, it doesn't feel powerful, per se, but it feels powerful

163
00:21:04,280 --> 00:21:10,040
in another way, it feels powerful in the senses of being invincible, that no matter what

164
00:21:10,040 --> 00:21:15,240
comes when you're mindful, there's such a clarity and objectivity that nothing can hurt

165
00:21:15,240 --> 00:21:24,400
you, be it pain, be it, be it an earthquake, be it death, be it anger, greed or delusion,

166
00:21:24,400 --> 00:21:31,400
they just can't penetrate because the wisdom, the clarity of mind is too powerful, it's

167
00:21:31,400 --> 00:21:37,120
strong.

168
00:21:37,120 --> 00:21:42,760
It's been seeing things, seeing beyond habit creating karma, really we're talking about

169
00:21:42,760 --> 00:21:50,200
karma here, seeing through this and, and just seeing things as arising as you see, coming

170
00:21:50,200 --> 00:21:55,360
to understand that doesn't really matter what happened, in the end it's only just seeing

171
00:21:55,360 --> 00:22:00,000
hearing, smelling, tasting, feeling, thinking, whether you live in a mansion or on the

172
00:22:00,000 --> 00:22:13,880
side of the road, there's no categorical difference, it allows you to see through this,

173
00:22:13,880 --> 00:22:19,680
see through the power and be stronger, be more powerful than any force on earth or in

174
00:22:19,680 --> 00:22:28,680
heaven, power of wisdom, absolutely think more powerful than that, it's the power of

175
00:22:28,680 --> 00:22:36,760
nibana, enlightenment, freedom from suffering, because it's so powerful that it doesn't

176
00:22:36,760 --> 00:22:44,800
even matter if you stop meditating at that point, you're a kind that there's a categorical

177
00:22:44,800 --> 00:22:53,000
change, there's a change of lineage they call it, go through boo, becoming family, becoming

178
00:22:53,000 --> 00:23:03,680
a part of the noble clan, meaning that you've gone beyond some sorrow, even if you stop

179
00:23:03,680 --> 00:23:11,120
meditating entirely, there's this clarity of mind that doesn't leave, can't leave, like

180
00:23:11,120 --> 00:23:19,080
you're broken, it can never put it back together again.

181
00:23:19,080 --> 00:23:25,120
The power is important, energy is important, not in the way that I think a lot of meditators

182
00:23:25,120 --> 00:23:30,880
or meditation traditions talk about it, energy and Buddhism is just power, it's just how

183
00:23:30,880 --> 00:23:37,840
there are these powers in the world that are important to recognize and play an important

184
00:23:37,840 --> 00:23:48,640
part in our meditation practice, not taking them for granted, not ignoring them, the power

185
00:23:48,640 --> 00:23:54,800
of habits especially, because if you ignore it, you cultivate bad habits, even as meditator,

186
00:23:54,800 --> 00:24:02,040
habits of avoiding, habits of trying to fix things, habits of inclining towards certain

187
00:24:02,040 --> 00:24:08,680
states or being partial towards certain states, the only habit we should be creating

188
00:24:08,680 --> 00:24:23,280
is mindfulness, seeing everything clearly and being present at all times.

189
00:24:23,280 --> 00:24:29,160
So there you go, a little bit about power, if it's an interesting topic, I hope you found

190
00:24:29,160 --> 00:24:40,160
that beneficial, thank you all for tuning in, have a good night.

