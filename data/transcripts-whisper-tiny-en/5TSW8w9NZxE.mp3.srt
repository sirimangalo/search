1
00:00:00,000 --> 00:00:16,040
I already replied to my question with a past life recollections are reliable.

2
00:00:16,040 --> 00:00:22,680
I am confused that you said that in ultimate reality all that exists is mind and body.

3
00:00:22,680 --> 00:00:25,680
What do you mean?

4
00:00:25,680 --> 00:00:54,920
It is not a question that should be answered from an intellectual point of view.

5
00:00:54,920 --> 00:01:01,720
Because I can give you the answer and I will, but I want to preface it by saying that the only

6
00:01:01,720 --> 00:01:05,080
thing that is going to help is if you actually start looking at ultimate reality.

7
00:01:05,080 --> 00:01:11,800
This is the first thing you have to learn in meditation is what are the building blocks

8
00:01:11,800 --> 00:01:20,120
of reality and so now you know my answer and you should see what your answer is when you

9
00:01:20,120 --> 00:01:23,880
look at ultimate reality, what do you see?

10
00:01:23,880 --> 00:01:39,040
But the way you can understand my answer is in terms of how I define ultimate reality.

11
00:01:39,040 --> 00:01:42,200
What do I mean by ultimate reality?

12
00:01:42,200 --> 00:01:49,160
Because people have many different ways of defining the word reality.

13
00:01:49,160 --> 00:02:02,200
I define reality based on the first principle, the first language fails, the starting point

14
00:02:02,200 --> 00:02:03,680
of everything.

15
00:02:03,680 --> 00:02:13,720
If you do, even if you do experiments on third person, on impersonal, it is a quote-unquote

16
00:02:13,720 --> 00:02:19,040
objective reality, physical reality, the physical world out there, you are still

17
00:02:19,040 --> 00:02:24,960
required to approach it from the point of view of your own experiences.

18
00:02:24,960 --> 00:02:32,240
There is this neat thing about quantum physics that says that requires the experimenter.

19
00:02:32,240 --> 00:02:39,280
You have to know what the observation is before you can talk about what the results will

20
00:02:39,280 --> 00:02:41,120
be.

21
00:02:41,120 --> 00:02:48,360
Without knowing, without something being experienced, there is no way to talk about

22
00:02:48,360 --> 00:02:52,240
the outcome.

23
00:02:52,240 --> 00:03:03,000
This is the quandary that came up out of quantum physics as I understand it.

24
00:03:03,000 --> 00:03:09,400
So this is great evidence for the fact that reality has to start with experience.

25
00:03:09,400 --> 00:03:14,040
Which from Buddhist point of view, it is really a silly, from a person who has seen ultimate

26
00:03:14,040 --> 00:03:23,280
reality, it is really a silly, silly thing to even talk about, because obviously what

27
00:03:23,280 --> 00:03:29,480
exists right here right now is what exists, the experience is what exists, there is nothing

28
00:03:29,480 --> 00:03:33,640
else exists.

29
00:03:33,640 --> 00:03:38,800
The sun doesn't exist, Pluto doesn't exist, Mars doesn't exist, solar systems don't

30
00:03:38,800 --> 00:03:39,800
exist.

31
00:03:39,800 --> 00:03:45,400
These things are, they only exist up here, they exist in our mind, we think about them.

32
00:03:45,400 --> 00:03:53,800
We say, yes, that is the sun, that is the moon, this is grass, this is a tree and so on.

33
00:03:53,800 --> 00:03:55,160
But actually what is it?

34
00:03:55,160 --> 00:04:02,800
It is seeing, hearing, smelling, tasting, feeling and thinking, that is all that is real.

35
00:04:02,800 --> 00:04:11,200
These are the only aspects of ultimate reality, meaning they are the only things that can

36
00:04:11,200 --> 00:04:13,000
be experienced.

37
00:04:13,000 --> 00:04:16,920
All that can ever possibly be experienced is seeing, hearing, smelling, tasting, feeling

38
00:04:16,920 --> 00:04:20,000
and thinking.

39
00:04:20,000 --> 00:04:23,280
And what are the constituents of these six things?

40
00:04:23,280 --> 00:04:28,800
What are the constituents of these experiences of experience in general?

41
00:04:28,800 --> 00:04:34,000
If you observe it you will see that there are two constituents, one is the physical, one

42
00:04:34,000 --> 00:04:37,480
is the mental.

43
00:04:37,480 --> 00:04:42,440
So for example when the stomach rises, well the stomach is theoretically it is rising and

44
00:04:42,440 --> 00:04:48,280
falling all the time, but if the mind is not there there is no experience of it, so without

45
00:04:48,280 --> 00:04:54,000
the mental there is no experience of rising, but if the stomach doesn't rise then even

46
00:04:54,000 --> 00:04:59,640
if the mind is there there will still not be an experience of rising, without the body

47
00:04:59,640 --> 00:05:04,480
and the mind, the feeling of rising can't arise.

48
00:05:04,480 --> 00:05:10,640
If your eye is good and there is light touching the eye, but your mind is somewhere else,

49
00:05:10,640 --> 00:05:12,240
there is no seeing.

50
00:05:12,240 --> 00:05:15,560
If your eyes are not good and the mind is there, you still there is no seeing.

51
00:05:15,560 --> 00:05:19,960
If your eyes are closed, even you put your eyes that your mind there, you still don't

52
00:05:19,960 --> 00:05:21,640
see anything.

53
00:05:21,640 --> 00:05:25,400
If the lights are out, if you turn out the lights, you still don't see anything.

54
00:05:25,400 --> 00:05:29,280
So the physical and the mental are the two constituents of experience.

55
00:05:29,280 --> 00:05:36,280
That's what is meant by body and mind is all that exists, because we define existence

56
00:05:36,280 --> 00:05:40,600
as experience, and we're only whether you want to define existence as something else,

57
00:05:40,600 --> 00:05:42,560
we're not concerned with that.

58
00:05:42,560 --> 00:05:47,480
All that we're concerned with as Buddhists is the reality of experience.

59
00:05:47,480 --> 00:05:52,080
And to us is their equivalent reality and experience.

