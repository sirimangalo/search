1
00:00:00,000 --> 00:00:13,000
I hope the video, ok, video is up and that's the slot hunting on this life, good evening

2
00:00:13,000 --> 00:00:14,000
everyone.

3
00:00:14,000 --> 00:00:23,000
Welcome to the broadcasting live sometime in February.

4
00:00:23,000 --> 00:00:39,000
Now, interestingly, today we were just talking about Filiil Piyatim.

5
00:00:39,000 --> 00:00:50,000
When Buddhism went to China, it was a course of a big deal at the time.

6
00:00:50,000 --> 00:00:54,000
The other religions in China, Confruit Confucianism, no.

7
00:00:54,000 --> 00:00:57,000
It was very much in Filiil Piyatim.

8
00:00:57,000 --> 00:01:07,000
And so they had actually adapted the teachings and said things like,

9
00:01:07,000 --> 00:01:17,000
claimed somehow that the Buddha believed Filiil Piyatim was the way.

10
00:01:17,000 --> 00:01:20,000
So we really, really blew out a proportion.

11
00:01:20,000 --> 00:01:22,000
So we're talking about that.

12
00:01:22,000 --> 00:01:28,000
And we addressed the question of, if Filiil Piyatim is so important,

13
00:01:28,000 --> 00:01:31,000
how do they justify becoming month?

14
00:01:31,000 --> 00:01:34,000
And there's actually a defense of it.

15
00:01:34,000 --> 00:01:41,000
Filiil Piyatim, it is respect for your parents to become a month

16
00:01:41,000 --> 00:01:50,000
because you're working to liberate, you're working to attain the ultimate state

17
00:01:50,000 --> 00:01:55,000
or even more you're working to help others attain the ultimate state.

18
00:01:55,000 --> 00:02:02,000
I'm working to better the world.

19
00:02:02,000 --> 00:02:13,000
What, in fact, is an early teaching as well in the text that we follow.

20
00:02:13,000 --> 00:02:20,000
And there's the idea that becoming a monk or even practicing meditation

21
00:02:20,000 --> 00:02:25,000
is actually a great benefit to your whole family.

22
00:02:25,000 --> 00:02:33,000
And in most obvious sense, because it changes your relationships with them as you become more peaceful,

23
00:02:33,000 --> 00:02:49,000
more content, more compassionate and so on, more loving.

24
00:02:49,000 --> 00:02:58,000
Here we have a quote today about this. It's actually quite a strong, strongly worded quote.

25
00:02:58,000 --> 00:03:05,000
Some people have might find it objectionable, especially those who have bad relationships with their parents.

26
00:03:05,000 --> 00:03:08,000
And I can understand that.

27
00:03:08,000 --> 00:03:19,000
Again, the Buddha is most often not so much prescriptive or don't know what the right word is,

28
00:03:19,000 --> 00:03:26,000
but it's not about denouncing people who have found familial problems.

29
00:03:26,000 --> 00:03:32,000
But there is a sense that when parents are looked up to,

30
00:03:32,000 --> 00:03:37,000
and it doesn't mean unworthy whether they're worthy of it or not,

31
00:03:37,000 --> 00:03:48,000
although there is a sense of it being good order and good proper etiquette

32
00:03:48,000 --> 00:03:56,000
and useful for the order of society to respect your parents regardless of whether they deserve it.

33
00:03:56,000 --> 00:04:02,000
But I think there's much room to say that part of the greatness is when parents do deserve it.

34
00:04:02,000 --> 00:04:08,000
When parents do act honorably and do shelter their children,

35
00:04:08,000 --> 00:04:23,000
care for their children, teach their children, act as a good example towards their children for their children.

36
00:04:23,000 --> 00:04:35,000
But definitely children that I've argued with people in the past

37
00:04:35,000 --> 00:04:40,000
about this idea of whether we should feel grateful to our parents.

38
00:04:40,000 --> 00:04:51,000
And their argument was that, well, my parents wanted to have me.

39
00:04:51,000 --> 00:04:54,000
I didn't choose to be born, right?

40
00:04:54,000 --> 00:04:57,000
Was there a choice?

41
00:04:57,000 --> 00:05:01,000
No, Buddhism doesn't actually believe that it's interesting.

42
00:05:01,000 --> 00:05:04,000
It actually is our choice.

43
00:05:04,000 --> 00:05:07,000
It would be born.

44
00:05:07,000 --> 00:05:09,000
It's a joint effort.

45
00:05:09,000 --> 00:05:12,000
Parents have to have sexual intercourse.

46
00:05:12,000 --> 00:05:22,000
And oftentimes are aware that it's going to lead to pregnancy.

47
00:05:22,000 --> 00:05:25,000
So it is partly their fault.

48
00:05:25,000 --> 00:05:39,000
But without us being keen to be reborn, there wouldn't be rebirth.

49
00:05:39,000 --> 00:05:44,000
But more importantly, it's quite besides the point.

50
00:05:44,000 --> 00:05:46,000
Who made the effort?

51
00:05:46,000 --> 00:05:52,000
Like if someone does a nice thing for you without you asking,

52
00:05:52,000 --> 00:05:56,000
does that mean you shouldn't feel grateful if it helps you?

53
00:05:56,000 --> 00:06:01,000
So someone tries to do a good deed and you don't need their health.

54
00:06:01,000 --> 00:06:04,000
That's one thing.

55
00:06:04,000 --> 00:06:09,000
But if you say, well, my parents wanted to look after me,

56
00:06:09,000 --> 00:06:10,000
they wanted to do all these good things.

57
00:06:10,000 --> 00:06:11,000
It's really beside the point.

58
00:06:11,000 --> 00:06:15,000
If I want to do something nice for you and you need it,

59
00:06:15,000 --> 00:06:18,000
it doesn't mean you shouldn't feel grateful.

60
00:06:18,000 --> 00:06:20,000
And this isn't a logic.

61
00:06:20,000 --> 00:06:22,000
It isn't about logic.

62
00:06:22,000 --> 00:06:26,000
It's very much about karma.

63
00:06:26,000 --> 00:06:32,000
When someone does good things for you, then they care for you.

64
00:06:32,000 --> 00:06:34,000
And this changes.

65
00:06:34,000 --> 00:06:38,000
It just changes the scales, tips the balance.

66
00:06:38,000 --> 00:06:39,000
It's funny.

67
00:06:39,000 --> 00:06:43,000
Nowadays, we often have people who don't like it

68
00:06:43,000 --> 00:06:45,000
when others do good deeds for them.

69
00:06:45,000 --> 00:06:48,000
And as I've talked about, this can be

70
00:06:48,000 --> 00:06:52,000
karmically based as well.

71
00:06:52,000 --> 00:06:55,000
Because they haven't done good things for others.

72
00:06:55,000 --> 00:07:00,000
But the fear that I've heard is

73
00:07:00,000 --> 00:07:02,000
there's a sense that if I do something for you,

74
00:07:02,000 --> 00:07:03,000
then I'm going to owe you something.

75
00:07:03,000 --> 00:07:04,000
I'm going to have to do something.

76
00:07:04,000 --> 00:07:06,000
If you do something for me, I'm going to have to do something

77
00:07:06,000 --> 00:07:08,000
back for you.

78
00:07:08,000 --> 00:07:09,000
Don't want me to do it.

79
00:07:09,000 --> 00:07:17,000
Because there's always strings attached.

80
00:07:17,000 --> 00:07:21,000
Which is really a shame.

81
00:07:21,000 --> 00:07:26,000
But it's a shame that people don't want to do good things for others.

82
00:07:26,000 --> 00:07:30,000
It's kind of what it sounds like.

83
00:07:30,000 --> 00:07:34,000
But part of being a good person,

84
00:07:34,000 --> 00:07:36,000
something that we don't talk about.

85
00:07:36,000 --> 00:07:40,000
Part of being a good person is being able to accept

86
00:07:40,000 --> 00:07:43,000
good deeds that other people do for you.

87
00:07:43,000 --> 00:07:47,000
So allowing other people to help you.

88
00:07:47,000 --> 00:07:50,000
Why I think I can say that is because

89
00:07:50,000 --> 00:07:52,000
you're ties in with karma.

90
00:07:52,000 --> 00:07:54,000
If you've done good deeds to others.

91
00:07:54,000 --> 00:07:57,000
The only way you can be comfortable with this way,

92
00:07:57,000 --> 00:07:59,000
with other people doing good deeds for you,

93
00:07:59,000 --> 00:08:01,000
is if you've done good deeds for others.

94
00:08:01,000 --> 00:08:02,000
Or if you're a good person.

95
00:08:02,000 --> 00:08:06,000
A person who's not of a wholesome mind will have

96
00:08:06,000 --> 00:08:09,000
a lot of trouble accepting good deeds.

97
00:08:09,000 --> 00:08:10,000
Rather, which is fine.

98
00:08:10,000 --> 00:08:13,000
It's not fine, but it's understandable.

99
00:08:13,000 --> 00:08:15,000
Most of us are going to be like that.

100
00:08:15,000 --> 00:08:18,000
We're going to be uncomfortable because we haven't done

101
00:08:18,000 --> 00:08:23,000
if you haven't engaged in actively seeking out

102
00:08:23,000 --> 00:08:27,000
and dedicating yourself to a life of good deeds.

103
00:08:27,000 --> 00:08:32,000
It's hard to accept gifts for others who find it viscerally.

104
00:08:32,000 --> 00:08:34,000
Again, it's not logic.

105
00:08:34,000 --> 00:08:35,000
It's not reason.

106
00:08:35,000 --> 00:08:36,000
It's karma.

107
00:08:36,000 --> 00:08:38,000
It's the world.

108
00:08:38,000 --> 00:08:41,000
So when people do things for you,

109
00:08:41,000 --> 00:08:43,000
it does tip the scales.

110
00:08:43,000 --> 00:08:47,000
And it is something to be that ordinary people will

111
00:08:47,000 --> 00:08:51,000
rightly should not fear,

112
00:08:51,000 --> 00:08:54,000
but it's something stirring.

113
00:08:54,000 --> 00:08:56,000
Like, yeah, you will owe them.

114
00:08:56,000 --> 00:08:58,000
Not because they think you'll owe them,

115
00:08:58,000 --> 00:09:02,000
but it changes the scales.

116
00:09:02,000 --> 00:09:04,000
I think it's fair to say that.

117
00:09:04,000 --> 00:09:07,000
I think it's fair that we're at the only rational way

118
00:09:07,000 --> 00:09:08,000
to deal with that.

119
00:09:08,000 --> 00:09:11,000
It is to go crazy doing good deeds.

120
00:09:11,000 --> 00:09:13,000
Someone helps.

121
00:09:13,000 --> 00:09:19,000
If someone helps you feeling grateful for sure.

122
00:09:19,000 --> 00:09:23,000
And oftentimes working to settle the score.

123
00:09:23,000 --> 00:09:25,000
I mean, it does sound kind of cold and calculated,

124
00:09:25,000 --> 00:09:27,000
but I don't think it needs being.

125
00:09:27,000 --> 00:09:29,000
I don't think it should be.

126
00:09:29,000 --> 00:09:31,000
Because if it's just to settle the score,

127
00:09:31,000 --> 00:09:33,000
it's not really a good deed, right?

128
00:09:33,000 --> 00:09:35,000
I guess that's the thing.

129
00:09:35,000 --> 00:09:38,000
I do something good for you to you out of the goodness

130
00:09:38,000 --> 00:09:40,000
of my heart.

131
00:09:40,000 --> 00:09:43,000
And you say, oh, I don't want to owe this person something,

132
00:09:43,000 --> 00:09:45,000
so I should pay them back.

133
00:09:45,000 --> 00:09:47,000
You're not doing it out of the goodness of your heart,

134
00:09:47,000 --> 00:09:51,000
so you still haven't managed to, even the scales, right?

135
00:09:51,000 --> 00:09:53,000
It has to be because you want to.

136
00:09:53,000 --> 00:09:56,000
It has to be because you out of love, out of compassion.

137
00:09:56,000 --> 00:09:59,000
It's quite scary, actually.

138
00:09:59,000 --> 00:10:02,000
I don't think you'd think about it.

139
00:10:02,000 --> 00:10:08,000
But it could be quite scary if you're not prepared to do good deeds for others.

140
00:10:08,000 --> 00:10:10,000
It's kind of funny.

141
00:10:10,000 --> 00:10:13,000
Ordinary people should be afraid of good people.

142
00:10:13,000 --> 00:10:15,000
Because when they do good deeds,

143
00:10:15,000 --> 00:10:18,000
it's not certainly not like that.

144
00:10:18,000 --> 00:10:21,000
Because it's not something that you actually have to think about it.

145
00:10:21,000 --> 00:10:24,000
It just means you'll have a relationship with that person.

146
00:10:24,000 --> 00:10:27,000
And if you're not a very good person,

147
00:10:27,000 --> 00:10:32,000
you're going to end up in a position where the best thing you can do

148
00:10:32,000 --> 00:10:34,000
is to help that person.

149
00:10:34,000 --> 00:10:36,000
I mean, it's just karmically talking.

150
00:10:36,000 --> 00:10:39,000
So why I'm talking about this is in regards to our parents,

151
00:10:39,000 --> 00:10:41,000
because it really is that way.

152
00:10:41,000 --> 00:10:43,000
And you find that when you meditate.

153
00:10:43,000 --> 00:10:47,000
Meditators often miss their parents if they have a good relationship with their parents.

154
00:10:47,000 --> 00:10:50,000
If they have a bad relationship with their parents,

155
00:10:50,000 --> 00:10:52,000
it's going to hit them hard.

156
00:10:52,000 --> 00:10:54,000
If their parents have been abusive,

157
00:10:54,000 --> 00:11:01,000
it's not that I'm not saying that they will eventually learn to respect their parents.

158
00:11:01,000 --> 00:11:03,000
I mean, if someone's abusive,

159
00:11:03,000 --> 00:11:07,000
that's not something to brush off or to ignore.

160
00:11:07,000 --> 00:11:11,000
But I would still say that will hit them hardest

161
00:11:11,000 --> 00:11:16,000
because if a stranger is abusive towards you,

162
00:11:16,000 --> 00:11:19,000
that's one thing but your parents.

163
00:11:19,000 --> 00:11:29,000
It's so much more deeply embedded in the heart.

164
00:11:29,000 --> 00:11:39,000
So in all ways, it's one of the parents or one of the hardest things to deal with in meditation.

165
00:11:39,000 --> 00:11:43,000
As a result, it's good to make sure that before you come to meditate,

166
00:11:43,000 --> 00:11:45,000
you have a good relationship with your parents.

167
00:11:45,000 --> 00:11:49,000
If not, then expect to have to deal with it during your meditation and deal with it.

168
00:11:49,000 --> 00:11:53,000
I didn't have a very good relationship with my parents when I started meditating.

169
00:11:53,000 --> 00:11:57,000
And as a result, I remember walking out of my room.

170
00:11:57,000 --> 00:11:59,000
I started crying.

171
00:11:59,000 --> 00:12:03,000
And someone found out and they called the teacher

172
00:12:03,000 --> 00:12:05,000
and one of the teachers came to my room

173
00:12:05,000 --> 00:12:07,000
and knocked on the door.

174
00:12:07,000 --> 00:12:10,000
And she said, what's wrong? I said, I miss my parents.

175
00:12:10,000 --> 00:12:11,000
I miss you.

176
00:12:11,000 --> 00:12:13,000
Oh, very good, very good.

177
00:12:13,000 --> 00:12:19,000
It was kind of a sign of, she said, that's right view.

178
00:12:19,000 --> 00:12:23,000
I mean, it is something coming up from your meditation.

179
00:12:23,000 --> 00:12:25,000
Naturally, I hadn't thought about my parents.

180
00:12:25,000 --> 00:12:28,000
I had no thought that this was going to,

181
00:12:28,000 --> 00:12:33,000
they were going to play a part of my meditation at all.

182
00:12:33,000 --> 00:12:39,000
I'd done a fairly acrimonious relationship with my parents at times.

183
00:12:39,000 --> 00:12:43,000
A lot of people talk about that.

184
00:12:43,000 --> 00:12:49,000
So I think that gives some insight into why this quote is saying,

185
00:12:49,000 --> 00:12:55,000
what it's saying, how when parents are respected in the house,

186
00:12:55,000 --> 00:12:59,000
that is what it means to worship the gods.

187
00:12:59,000 --> 00:13:02,000
Worship Brahma.

188
00:13:02,000 --> 00:13:07,000
Worshiping your parents is worshiping Brahma.

189
00:13:07,000 --> 00:13:13,000
And it's kind of a dig at Hinduism as well.

190
00:13:13,000 --> 00:13:18,000
Like the language, or Brahminism, the religion of the time.

191
00:13:18,000 --> 00:13:22,000
Because they worshiped, they were talking about God and the Yadda Yadda.

192
00:13:22,000 --> 00:13:25,000
So the Buddha is saying, you know, real God,

193
00:13:25,000 --> 00:13:29,000
people who are really God's because they do create you, right?

194
00:13:29,000 --> 00:13:32,000
In a sense, they create your body,

195
00:13:32,000 --> 00:13:36,000
and they do answer your prayers, answer your wishes.

196
00:13:36,000 --> 00:13:38,000
This is if they do again.

197
00:13:38,000 --> 00:13:41,000
But for many of us, they do, and they have for many years.

198
00:13:41,000 --> 00:13:43,000
That's God, right?

199
00:13:43,000 --> 00:13:45,000
That's what God is supposed to do.

200
00:13:45,000 --> 00:13:46,000
They're powerful.

201
00:13:46,000 --> 00:13:49,000
They have complete power over you, right?

202
00:13:49,000 --> 00:13:51,000
When you're a child.

203
00:13:51,000 --> 00:13:57,000
And they exercise benevolence if they do as many do.

204
00:13:57,000 --> 00:14:01,000
And they care for you, many of them do.

205
00:14:01,000 --> 00:14:04,000
So if that's the case, if they do care for you,

206
00:14:04,000 --> 00:14:09,000
and they are benevolence and so on,

207
00:14:09,000 --> 00:14:10,000
then that's like Brahma.

208
00:14:10,000 --> 00:14:11,000
And they should be worshiped.

209
00:14:11,000 --> 00:14:13,000
And if they are worshiped.

210
00:14:13,000 --> 00:14:16,000
You know, maybe not as God's,

211
00:14:16,000 --> 00:14:20,000
but there is a sense that your parents are holy for that reason.

212
00:14:20,000 --> 00:14:22,000
They're not funny, no.

213
00:14:22,000 --> 00:14:25,000
Parents are more like God than God is because, of course,

214
00:14:25,000 --> 00:14:28,000
any gods that there might be, they don't spend much time.

215
00:14:28,000 --> 00:14:31,000
Obviously, they don't spend much time worrying about humans,

216
00:14:31,000 --> 00:14:34,000
no matter what theists say.

217
00:14:34,000 --> 00:14:36,000
Apparently you have as much,

218
00:14:36,000 --> 00:14:40,000
someone was saying you have as much statistically speaking

219
00:14:40,000 --> 00:14:44,000
or they've done experiments on prayer.

220
00:14:44,000 --> 00:14:46,000
And you have as much,

221
00:14:46,000 --> 00:14:48,000
that was he kind of funny, I guess.

222
00:14:48,000 --> 00:14:53,000
But what is it?

223
00:14:53,000 --> 00:14:57,000
He would have as much success praying to a jug of milk

224
00:14:57,000 --> 00:15:00,000
as he would praying to God.

225
00:15:00,000 --> 00:15:04,000
That was the quote, I think.

226
00:15:04,000 --> 00:15:09,000
But to your parents, in cases,

227
00:15:09,000 --> 00:15:12,000
no, there are parents who love their children so much

228
00:15:12,000 --> 00:15:16,000
that they do pretty much while they do many things with them,

229
00:15:16,000 --> 00:15:19,000
whatever's in their power to make them happy.

230
00:15:19,000 --> 00:15:25,000
So again, not exactly in deep teaching,

231
00:15:25,000 --> 00:15:28,000
but I think I touched what I touched upon shows

232
00:15:28,000 --> 00:15:30,000
that it's interesting for meditators.

233
00:15:30,000 --> 00:15:34,000
It is a part of the meditation that our relationships

234
00:15:34,000 --> 00:15:36,000
with people will come up.

235
00:15:36,000 --> 00:15:38,000
You have to deal with them in a good way to deal with them

236
00:15:38,000 --> 00:15:40,000
and send love, ask forgiveness.

237
00:15:40,000 --> 00:15:44,000
This does a mental note after you meditate even during your meditation.

238
00:15:44,000 --> 00:15:48,000
Obviously not to make it our core meditation

239
00:15:48,000 --> 00:15:50,000
or focused on insight.

240
00:15:50,000 --> 00:15:53,000
But insight will evoke these feelings

241
00:15:53,000 --> 00:15:56,000
and dig up all the emotions that kind of thing.

242
00:15:56,000 --> 00:16:00,000
All bad things we've done, things that have done,

243
00:16:00,000 --> 00:16:04,000
been done to us that we've still carry around us a grudge.

244
00:16:04,000 --> 00:16:06,000
And it's important we work them out.

245
00:16:06,000 --> 00:16:10,000
It doesn't mean we have to pretend that our parents are gods

246
00:16:10,000 --> 00:16:15,000
if they weren't godly or holy or worthy.

247
00:16:15,000 --> 00:16:18,000
But it doesn't mean we should work out especially

248
00:16:18,000 --> 00:16:23,000
since we have a very deep and long standing relationship

249
00:16:23,000 --> 00:16:27,000
with our parents and work them out

250
00:16:27,000 --> 00:16:30,000
may just mean to let them go.

251
00:16:30,000 --> 00:16:34,000
And to move on, stop obsessing over the bad things

252
00:16:34,000 --> 00:16:38,000
they've done to us if they've done bad things to us.

253
00:16:38,000 --> 00:16:42,000
So there you are, that's the quote for this evening.

254
00:16:42,000 --> 00:16:45,000
And I have got four people join me in the hangout.

255
00:16:45,000 --> 00:16:47,000
Welcome everyone.

256
00:16:47,000 --> 00:16:50,000
Well that's all for the number for tonight.

257
00:16:50,000 --> 00:16:52,000
So welcome to go home if there's any questions

258
00:16:52,000 --> 00:16:54,000
I'm happy to answer them.

259
00:16:54,000 --> 00:16:55,000
Only on the hangout though,

260
00:16:55,000 --> 00:16:57,000
so that means you gotta join the hangout

261
00:16:57,000 --> 00:16:58,000
and ask me live.

262
00:16:58,000 --> 00:17:01,000
That's the new rules.

263
00:17:01,000 --> 00:17:03,000
You guys can go.

264
00:17:03,000 --> 00:17:06,000
I've got three people sitting here as my audience.

265
00:17:06,000 --> 00:17:13,000
Second. Record numbers. Record turnout.

266
00:17:13,000 --> 00:17:15,000
Well I don't think I have audio just a second.

267
00:17:15,000 --> 00:17:18,000
You might be good at doing something really good.

268
00:17:18,000 --> 00:17:20,000
Just a second.

269
00:17:20,000 --> 00:17:28,000
Every time I'm on turn on volume.

270
00:17:28,000 --> 00:17:33,000
Now I should be able to hear you.

271
00:17:33,000 --> 00:17:38,000
I should be able to hear you and you should be able to hear

272
00:17:38,000 --> 00:17:41,000
each other on the live stream.

273
00:17:41,000 --> 00:17:42,000
Testing.

274
00:17:42,000 --> 00:17:45,000
I don't have a question but I.

275
00:17:45,000 --> 00:17:49,000
I have a question but all right.

276
00:17:49,000 --> 00:17:52,000
Here she has a question speak.

277
00:17:52,000 --> 00:17:53,000
Okay.

278
00:17:53,000 --> 00:17:55,000
So it's kind of,

279
00:17:55,000 --> 00:17:58,000
there's not really a complicated question.

280
00:17:58,000 --> 00:18:01,000
I was thinking about those people today

281
00:18:01,000 --> 00:18:05,000
who are like prolonging old schisms.

282
00:18:05,000 --> 00:18:09,000
Would they as well go to like the deepest hill?

283
00:18:09,000 --> 00:18:11,000
How is that?

284
00:18:11,000 --> 00:18:13,000
Or are they just twisting their own minds

285
00:18:13,000 --> 00:18:18,000
and like in the bad direction?

286
00:18:18,000 --> 00:18:21,000
The people who were who caused schisms?

287
00:18:21,000 --> 00:18:25,000
Well I mean old schisms and old schisms

288
00:18:25,000 --> 00:18:28,000
and then they hold to them and prolong them.

289
00:18:28,000 --> 00:18:32,000
They're like clinging to them so they're keeping them around.

290
00:18:32,000 --> 00:18:36,000
I see are you thinking specifically of Buddhism?

291
00:18:36,000 --> 00:18:37,000
Yep.

292
00:18:37,000 --> 00:18:40,000
Like Mahayana, Theravada, all that stuff.

293
00:18:40,000 --> 00:18:42,000
Yep.

294
00:18:42,000 --> 00:18:45,000
But off the top of my head it doesn't sound like it

295
00:18:45,000 --> 00:18:49,000
because they've been told this is Buddhism, right?

296
00:18:49,000 --> 00:18:50,000
Yeah.

297
00:18:50,000 --> 00:18:55,000
So if they were presented with inconvertible truth

298
00:18:55,000 --> 00:18:59,000
inconvertible inconvertible,

299
00:18:59,000 --> 00:19:03,000
there's a word that means non-controversial.

300
00:19:03,000 --> 00:19:06,000
You're unable to,

301
00:19:06,000 --> 00:19:08,000
like absolute proof,

302
00:19:08,000 --> 00:19:10,000
or maybe overwhelming at least,

303
00:19:10,000 --> 00:19:13,000
proof that this was not Buddhism,

304
00:19:13,000 --> 00:19:15,000
that this is not what,

305
00:19:15,000 --> 00:19:19,000
this is lies, say.

306
00:19:19,000 --> 00:19:21,000
If they found something old,

307
00:19:21,000 --> 00:19:24,000
this is lies and they still held to it.

308
00:19:24,000 --> 00:19:28,000
Then I'd say they'd start to be a problem.

309
00:19:28,000 --> 00:19:33,000
But it's quite different from a group being in harmony

310
00:19:33,000 --> 00:19:35,000
and following the Buddha's teaching

311
00:19:35,000 --> 00:19:38,000
and then breaking them up.

312
00:19:38,000 --> 00:19:41,000
You know, that's the big deal

313
00:19:41,000 --> 00:19:45,000
because that destroys Buddhism.

314
00:19:45,000 --> 00:19:47,000
I see.

315
00:19:47,000 --> 00:19:50,000
Okay.

316
00:19:50,000 --> 00:19:55,000
I mean, though there is work

317
00:19:55,000 --> 00:19:58,000
and it is good work to try and bring Buddhists

318
00:19:58,000 --> 00:20:01,000
of all kinds together and find our commonalities,

319
00:20:01,000 --> 00:20:06,000
that seems like a good thing to do without accepting

320
00:20:06,000 --> 00:20:09,000
what we'd consider wrong view.

321
00:20:09,000 --> 00:20:21,000
But I don't think it's schismitism to perpetuate

322
00:20:21,000 --> 00:20:25,000
what one understands to be Buddhism as Buddhism.

323
00:20:25,000 --> 00:20:30,000
It's unfortunate that some of what is not Buddhism gets.

324
00:20:30,000 --> 00:20:34,000
Like, words that we're learning a lot about in China,

325
00:20:34,000 --> 00:20:39,000
if we take this, for example, in China,

326
00:20:39,000 --> 00:20:40,000
they put words in the Buddhist mouth.

327
00:20:40,000 --> 00:20:42,000
It's pretty clear they just,

328
00:20:42,000 --> 00:20:48,000
because in order for their views to gain legitimacy,

329
00:20:48,000 --> 00:20:52,000
well, the only legitimate source of Buddhism was the Buddha

330
00:20:52,000 --> 00:20:57,000
for many schools, for most of us.

331
00:20:57,000 --> 00:21:04,000
And so they just think like clearly,

332
00:21:04,000 --> 00:21:06,000
as I said, they put in the Buddhist mouth

333
00:21:06,000 --> 00:21:08,000
like right after he became enlightened,

334
00:21:08,000 --> 00:21:09,000
he was sitting there and he said,

335
00:21:09,000 --> 00:21:12,000
Philadelphia is the way to, is the way.

336
00:21:12,000 --> 00:21:18,000
I'm like, well, it's like, well, that's so Chinese, right?

337
00:21:18,000 --> 00:21:20,000
Definitely not what the Buddha said.

338
00:21:20,000 --> 00:21:23,000
And pretty clearly, we don't actually have proof,

339
00:21:23,000 --> 00:21:28,000
but pretty clearly came from China.

340
00:21:28,000 --> 00:21:29,000
And that's bad.

341
00:21:29,000 --> 00:21:33,000
I would say that whoever did that.

342
00:21:33,000 --> 00:21:39,000
Now, you could argue that maybe they didn't really have a sense

343
00:21:39,000 --> 00:21:42,000
of who the Buddha was or maybe they thought everyone did this.

344
00:21:42,000 --> 00:21:45,000
Maybe they thought, well, the Buddha is just imaginary figure anyway.

345
00:21:45,000 --> 00:21:49,000
It really exists. People think that.

346
00:21:49,000 --> 00:21:51,000
Then they would think, well, it's also then it's all right.

347
00:21:51,000 --> 00:21:54,000
I'll just make up my own stories. They have their stories from India.

348
00:21:54,000 --> 00:21:56,000
I'll make my stories about the Buddha.

349
00:21:56,000 --> 00:21:59,000
It's just stories.

350
00:21:59,000 --> 00:22:02,000
So you could argue that there wasn't a malevolent intent

351
00:22:02,000 --> 00:22:04,000
or there wasn't the intent.

352
00:22:04,000 --> 00:22:06,000
I guess not like putting quotes in Gandhi's mouth

353
00:22:06,000 --> 00:22:09,000
who we know did exist, right?

354
00:22:09,000 --> 00:22:12,000
So that could be a mitigating factor.

355
00:22:12,000 --> 00:22:16,000
But as a Buddhist, I would never do that.

356
00:22:16,000 --> 00:22:20,000
What words in the Buddhist mouth?

357
00:22:20,000 --> 00:22:24,000
Okay, thank you, Brenton.

358
00:22:24,000 --> 00:22:27,000
Welcome.

359
00:22:27,000 --> 00:22:37,000
So what do we have?

360
00:22:37,000 --> 00:22:39,000
Tom.

361
00:22:39,000 --> 00:22:41,000
That's fine.

362
00:22:41,000 --> 00:22:42,000
Simon.

363
00:22:42,000 --> 00:22:44,000
At least why?

364
00:22:44,000 --> 00:22:45,000
I don't know.

365
00:22:45,000 --> 00:22:48,000
Lisa, do I?

366
00:22:48,000 --> 00:22:54,000
And Douglas, do I know Douglas?

367
00:22:54,000 --> 00:22:58,000
Who in Lisa in Douglas?

368
00:22:58,000 --> 00:23:07,000
Do you guys have my?

369
00:23:07,000 --> 00:23:10,000
Why are you all hanging out if you don't talk?

370
00:23:10,000 --> 00:23:11,000
Hello?

371
00:23:11,000 --> 00:23:12,000
Hi.

372
00:23:12,000 --> 00:23:13,000
I'm Doug.

373
00:23:13,000 --> 00:23:15,000
Hi, Doug.

374
00:23:15,000 --> 00:23:18,000
That's not Doug from Australia, is it?

375
00:23:18,000 --> 00:23:19,000
No.

376
00:23:19,000 --> 00:23:21,000
This is my first time.

377
00:23:21,000 --> 00:23:24,000
Okay.

378
00:23:24,000 --> 00:23:26,000
Do you have a question?

379
00:23:26,000 --> 00:23:28,000
Sure.

380
00:23:28,000 --> 00:23:33,000
I have a question about meditation.

381
00:23:33,000 --> 00:23:37,000
I have a lot of intense anxiety at work.

382
00:23:37,000 --> 00:23:40,000
And I've tried meditating during my break set work

383
00:23:40,000 --> 00:23:43,000
and I try mindfulness and breathing.

384
00:23:43,000 --> 00:23:47,000
But my anxiety and maybe my adrenaline is so high

385
00:23:47,000 --> 00:23:48,000
that I don't seem to calm.

386
00:23:48,000 --> 00:23:52,000
Be able to calm myself down with my focus of breathing.

387
00:23:52,000 --> 00:23:53,000
Is there another question?

388
00:23:53,000 --> 00:23:55,000
Don't use mindfulness of breathing.

389
00:23:55,000 --> 00:23:56,000
Okay.

390
00:23:56,000 --> 00:24:00,000
I mean, if you want, don't recommend it.

391
00:24:00,000 --> 00:24:02,000
Because it's summer time meditation.

392
00:24:02,000 --> 00:24:03,000
I don't know.

393
00:24:03,000 --> 00:24:05,000
Pennsylvania asked.

394
00:24:05,000 --> 00:24:06,000
What my advice?

395
00:24:06,000 --> 00:24:08,000
Because you're asking me.

396
00:24:08,000 --> 00:24:13,000
I'm allowed to be partisan because you passed me.

397
00:24:13,000 --> 00:24:15,000
I just know that I'm being a bit partisan when I say,

398
00:24:15,000 --> 00:24:17,000
don't use that.

399
00:24:17,000 --> 00:24:19,000
Look up.

400
00:24:19,000 --> 00:24:21,000
As you're asking me, look up.

401
00:24:21,000 --> 00:24:23,000
You did a demo.

402
00:24:23,000 --> 00:24:25,000
Dang.

403
00:24:25,000 --> 00:24:26,000
Good up this moment.

404
00:24:26,000 --> 00:24:27,000
Make sure.

405
00:24:27,000 --> 00:24:29,000
Removing anxiety completely.

406
00:24:29,000 --> 00:24:32,000
Meditation for anxious people.

407
00:24:32,000 --> 00:24:34,000
And there's three right there.

408
00:24:34,000 --> 00:24:36,000
My first three results on Google.

409
00:24:36,000 --> 00:24:38,000
Okay.

410
00:24:38,000 --> 00:24:40,000
I mean, I couldn't go over it again.

411
00:24:40,000 --> 00:24:43,000
But it's all been said.

412
00:24:43,000 --> 00:24:48,000
I mean, basically, the basic thing is it's not.

413
00:24:48,000 --> 00:24:50,000
Let me see.

414
00:24:50,000 --> 00:24:52,000
Anxiety is both physical and mental.

415
00:24:52,000 --> 00:24:54,000
And the physical aspects are an anxiety.

416
00:24:54,000 --> 00:24:57,000
Anxiety is in the physical that they bounce off each other.

417
00:24:57,000 --> 00:25:01,000
So being anxious creates physical experiences.

418
00:25:01,000 --> 00:25:04,000
Physical effects like this butterflies in your stomach.

419
00:25:04,000 --> 00:25:08,000
The heart beating, tension, et cetera, et cetera.

420
00:25:08,000 --> 00:25:10,000
And that makes you anxious.

421
00:25:10,000 --> 00:25:11,000
Because you say, oh, my gosh, I'm anxious.

422
00:25:11,000 --> 00:25:12,000
You're not actually.

423
00:25:12,000 --> 00:25:14,000
Anxiety is just a moment.

424
00:25:14,000 --> 00:25:16,000
And then there's the physical experience.

425
00:25:16,000 --> 00:25:18,000
And then you get anxious about that.

426
00:25:18,000 --> 00:25:25,000
I was even realizing that it makes you anxious.

427
00:25:25,000 --> 00:25:29,000
You get anxious that you're going to be anxious.

428
00:25:29,000 --> 00:25:32,000
You think about something and say, oh, I can't.

429
00:25:32,000 --> 00:25:35,000
What if I, you know, you say, what if I freeze up?

430
00:25:35,000 --> 00:25:36,000
Yeah.

431
00:25:36,000 --> 00:25:38,000
I have to go and talk, give a talk.

432
00:25:38,000 --> 00:25:39,000
Oh, man.

433
00:25:39,000 --> 00:25:41,000
Am I going to mess it up?

434
00:25:41,000 --> 00:25:43,000
Am I going to freeze because I'm so anxious?

435
00:25:43,000 --> 00:25:46,000
And that makes you anxious.

436
00:25:46,000 --> 00:25:48,000
So it's a snowballing.

437
00:25:48,000 --> 00:25:51,000
And insight meditation breaks that.

438
00:25:51,000 --> 00:25:54,000
When you're able to know the anxious anxious or not,

439
00:25:54,000 --> 00:25:55,000
then note the physical as well.

440
00:25:55,000 --> 00:25:57,000
I mean, watch those videos that I've done.

441
00:25:57,000 --> 00:25:59,000
I'm sure one of them is good.

442
00:25:59,000 --> 00:26:02,000
On YouTube, did you see?

443
00:26:02,000 --> 00:26:03,000
Yeah.

444
00:26:03,000 --> 00:26:05,000
What just Google, you two demo anxiety?

445
00:26:05,000 --> 00:26:06,000
OK.

446
00:26:06,000 --> 00:26:12,000
I've also got just a shameless plug.

447
00:26:12,000 --> 00:26:20,000
Video.ceremungalow.org has all my many of my videos categorized.

448
00:26:20,000 --> 00:26:24,000
So under mental, it's probably under mental issues or something.

449
00:26:24,000 --> 00:26:29,000
Mental issues, there's anxiety.

450
00:26:29,000 --> 00:26:33,000
Someone in the hangout in the chat has already put something up.

451
00:26:33,000 --> 00:26:36,000
No, nobody.

452
00:26:36,000 --> 00:26:38,000
But yeah, look up.

453
00:26:38,000 --> 00:26:43,000
If you ever want, if you ever looking for something about a specific subject,

454
00:26:43,000 --> 00:26:45,000
we don't see anxiety there.

455
00:26:45,000 --> 00:26:50,000
I don't know.

456
00:26:50,000 --> 00:26:56,000
I'm guessing for another name for it.

457
00:26:56,000 --> 00:26:58,000
Yeah, it's funny.

458
00:26:58,000 --> 00:27:04,000
I'm surprised there isn't one.

459
00:27:04,000 --> 00:27:12,000
Well, we better add it.

460
00:27:12,000 --> 00:27:14,000
Maybe it's in a different spot.

461
00:27:14,000 --> 00:27:19,000
What else?

462
00:27:19,000 --> 00:27:20,000
OK.

463
00:27:20,000 --> 00:27:23,000
Well, you can Google it anyway.

464
00:27:23,000 --> 00:27:25,000
OK.

465
00:27:25,000 --> 00:27:30,000
Thank you.

466
00:27:30,000 --> 00:27:32,000
Oh, in under fear.

467
00:27:32,000 --> 00:27:34,000
It's all lumped under fear.

468
00:27:34,000 --> 00:27:36,000
It shouldn't be probably.

469
00:27:36,000 --> 00:27:43,000
But there's, yeah, the videos are all under fear.

470
00:27:43,000 --> 00:27:50,000
OK.

471
00:27:50,000 --> 00:27:57,000
OK.

472
00:27:57,000 --> 00:28:02,000
So did that answer?

473
00:28:02,000 --> 00:28:03,000
That did answer.

474
00:28:03,000 --> 00:28:06,000
Like, it was just about anxiety, right?

475
00:28:06,000 --> 00:28:09,000
What type of meditation I could use?

476
00:28:09,000 --> 00:28:13,000
If you haven't, yeah, read my booklet on how to meditate.

477
00:28:13,000 --> 00:28:14,000
OK.

478
00:28:14,000 --> 00:28:17,000
Yeah, maybe that's even better than the videos that I did,

479
00:28:17,000 --> 00:28:19,000
because if you haven't read the booklet,

480
00:28:19,000 --> 00:28:21,000
that's where you got to start with this community.

481
00:28:21,000 --> 00:28:23,000
With talking to me.

482
00:28:23,000 --> 00:28:25,000
OK.

483
00:28:25,000 --> 00:28:32,000
It's not so much doctrine as it is giving you meditation techniques.

484
00:28:32,000 --> 00:28:33,000
OK.

485
00:28:33,000 --> 00:28:39,000
But then, specifically, the videos on anxiety,

486
00:28:39,000 --> 00:28:42,000
because they're also very practical explaining how you'd use the technique

487
00:28:42,000 --> 00:28:45,000
to deal with anxiety, which is an interesting case.

488
00:28:45,000 --> 00:28:48,000
Because, as I said, much of it is physical when we think it's all anxiety.

489
00:28:48,000 --> 00:28:49,000
We think I'm anxious.

490
00:28:49,000 --> 00:28:51,000
A lot of it is not anxiety.

491
00:28:51,000 --> 00:28:53,000
A lot of it is the physical effects of anxiety,

492
00:28:53,000 --> 00:28:55,000
which then perpetuate the cycle.

493
00:28:55,000 --> 00:28:57,000
They make you anxious.

494
00:28:57,000 --> 00:28:59,000
Oh, my gosh, I'm anxious.

495
00:28:59,000 --> 00:29:04,000
We'll get more anxious.

496
00:29:04,000 --> 00:29:08,000
Thank you.

497
00:29:08,000 --> 00:29:10,000
What about Lisa?

498
00:29:10,000 --> 00:29:16,000
What is Lisa?

499
00:29:16,000 --> 00:29:24,000
Lisa has no bike, I guess.

500
00:29:24,000 --> 00:29:29,000
All right. Well, thanks guys for actually joining up.

501
00:29:29,000 --> 00:29:34,000
Good to see people here.

502
00:29:34,000 --> 00:29:37,000
I guess that's all for coming.

503
00:29:37,000 --> 00:29:38,000
Good night.

504
00:29:38,000 --> 00:29:39,000
Good night.

505
00:29:39,000 --> 00:29:42,000
Oh, I'll just just shout out to thank everyone.

506
00:29:42,000 --> 00:29:45,000
I don't know if I can even say that they can't actually.

507
00:29:45,000 --> 00:29:47,000
I know what you're talking about.

508
00:29:47,000 --> 00:29:49,000
You're welcome.

509
00:29:49,000 --> 00:29:53,000
I can't even say thank you, I don't think.

510
00:29:53,000 --> 00:29:55,000
Anyway, good night.

511
00:29:55,000 --> 00:29:56,000
Good night.

512
00:29:56,000 --> 00:30:00,000
Thank you all for being so supportive of our community.

513
00:30:00,000 --> 00:30:02,000
You can say that.

514
00:30:02,000 --> 00:30:03,000
It's really awesome.

515
00:30:03,000 --> 00:30:05,000
I mean, it looks like I did mention it didn't I?

516
00:30:05,000 --> 00:30:13,000
I mean, it looks like we're, we're, we're moving.

517
00:30:13,000 --> 00:30:14,000
So good night.

518
00:30:14,000 --> 00:30:31,000
Good night, contact.

