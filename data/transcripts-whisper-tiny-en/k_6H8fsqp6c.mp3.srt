1
00:00:00,000 --> 00:00:15,920
Good evening everyone, welcome to our evening dhamma.

2
00:00:15,920 --> 00:00:23,240
I've been thinking a bit about society.

3
00:00:23,240 --> 00:00:35,440
I find myself in society to a limited extent, and in going to university I meet people

4
00:00:35,440 --> 00:00:48,440
who I might otherwise not meet, get a taste of the diversity of character types in the

5
00:00:48,440 --> 00:00:56,940
world. It's quite a blessing to live in the monastery in the meditation center, to meet

6
00:00:56,940 --> 00:01:05,960
only with wonderful people who are keen on bettering themselves, do not have to worry

7
00:01:05,960 --> 00:01:17,560
about things like lying or cheating or stealing or manipulating, to live in a world

8
00:01:17,560 --> 00:01:28,520
that is for the most part drama free, free from all the problems associated with ambition

9
00:01:28,520 --> 00:01:40,360
and passion and arrogance and conceit and so on, not that we all don't, in the meditation

10
00:01:40,360 --> 00:01:49,160
center that there aren't defilements, but they're on a whole greater level of refinement

11
00:01:49,160 --> 00:01:53,360
or subtle level.

12
00:01:53,360 --> 00:02:04,880
So when you go out in the world and get to see a taste of the unmindful society and the

13
00:02:04,880 --> 00:02:21,160
unmindful environment, not just unmindful, but filled with people whose views are far

14
00:02:21,160 --> 00:02:40,180
from mine. I think either way, whether it's whether we talk about a Buddhist society or

15
00:02:40,180 --> 00:02:47,840
whether we're talking about society in general, the greater non-Buddhist or pluralistic

16
00:02:47,840 --> 00:03:08,880
society, global society, our intentions remain the same generally. It's to get along, to

17
00:03:08,880 --> 00:03:23,360
have peace, to find happiness. I think in general, there are movement in societies to try

18
00:03:23,360 --> 00:03:30,640
and find some sort of peace and happiness. Sometimes it can be rather selfish for people

19
00:03:30,640 --> 00:03:39,400
in power, not so concerned with the unwashed masses and the concerned with their own

20
00:03:39,400 --> 00:03:43,920
peace, their own happiness and so maintaining the peace means oppressing people, that kind

21
00:03:43,920 --> 00:03:55,080
of thing. But let's put it this way as Buddhists, whether we live in the meditation center

22
00:03:55,080 --> 00:04:04,280
or whether we live in society, our goal remains the same, just to get along. To have a peaceful

23
00:04:04,280 --> 00:04:21,360
society, a kind and compassionate society. They're the same principles that we aspire

24
00:04:21,360 --> 00:04:28,920
to here in the meditation center, of course, hopefully much more successfully in the

25
00:04:28,920 --> 00:04:38,600
meditation center. Unless something for us to remember, something for us to think of, and

26
00:04:38,600 --> 00:04:46,880
something maybe even to see as a positive outcome of our meditation practice. The Buddha

27
00:04:46,880 --> 00:04:52,880
called, the Buddha talked about something called the sarnia dhamma. I talked about this before.

28
00:04:52,880 --> 00:04:59,840
I think I'm inevitably going to repeat myself again and again as I make more and more videos.

29
00:04:59,840 --> 00:05:15,760
The sarnia dhamma is that, dhamma is that make us think well of each other, or that

30
00:05:15,760 --> 00:05:32,400
make us worthy of friendship and then allow us to think fondly of each other and to have

31
00:05:32,400 --> 00:05:49,840
a peaceful and harmonious society or community. Six sarnia dhammas, simple teaching,

32
00:05:49,840 --> 00:06:02,160
something for us to keep in mind. The first one is bodily acts of kindness. Now with

33
00:06:02,160 --> 00:06:10,400
kindness of any sort, this isn't the practice of Ibhasana, but in many ways the outcome

34
00:06:10,400 --> 00:06:17,760
of a practice of insight meditation. Something for us to strive to strive for is to

35
00:06:17,760 --> 00:06:28,560
be a kinder person to do things. When we live in a society where everyone is observant

36
00:06:28,560 --> 00:06:36,000
and caring and outgoing, noticing when there's a need and working to fulfill that need,

37
00:06:38,080 --> 00:06:48,880
kind with kindness, someone who is hungry, feed them food, someone who is poor, give them money,

38
00:06:48,880 --> 00:06:59,600
someone who is scared and provide them with shelter and so on, someone who is confused

39
00:06:59,600 --> 00:07:02,400
to provide them with insight, with wisdom.

40
00:07:12,240 --> 00:07:17,680
So the first one is bodily acts of kindness. This is actually doing things that benefit others.

41
00:07:17,680 --> 00:07:26,560
Giving them food, giving them help, helping people, helping old people across the street,

42
00:07:26,560 --> 00:07:32,160
that sort of thing, helping your parents, helping your grandparents, helping your children,

43
00:07:32,160 --> 00:07:40,800
helping your friends. This is how we'd expect it to work in a monastery as well,

44
00:07:40,800 --> 00:07:48,080
monks helping each other. The story of these monks who didn't talk to each other, they would only

45
00:07:48,080 --> 00:07:54,000
talk once every five days they would get together and the rest of the time they wouldn't open

46
00:07:54,000 --> 00:08:00,560
their mouths. If there was a chore they would just back in with their hands and not say anything,

47
00:08:00,560 --> 00:08:12,160
just use the hands to back into them. When it was time to eat, the first person there would set

48
00:08:12,160 --> 00:08:20,240
out the seats for eating and eat. The last person to come back from arms round would clean up

49
00:08:21,760 --> 00:08:26,560
and there was no communication except once every five days they would get together and recite

50
00:08:26,560 --> 00:08:31,280
and memorize the Buddha's teaching go over it again and again and talk about it.

51
00:08:38,640 --> 00:08:45,920
But acts of kindness. The second one of course is words of kindness. Words spoken out of kindness.

52
00:08:45,920 --> 00:08:54,800
So it's not just kind words like, may you be happy, but it can be advice, but given

53
00:08:54,800 --> 00:09:02,400
out of kindness. Any speech, the intention of which is the benefit of the other.

54
00:09:04,800 --> 00:09:08,960
Let me speak with thoughtfulness. We don't speak, try not to speak,

55
00:09:10,080 --> 00:09:16,640
but then it's out of a desire to hurt, to harm, out of a desire to assert our dominance,

56
00:09:16,640 --> 00:09:27,680
right arrogance and conceit, to cultivate fear or jealousy. How much of our speech is just designed

57
00:09:27,680 --> 00:09:34,880
to make us look good, impress others, which of course is just designed in the same way to make

58
00:09:34,880 --> 00:09:44,480
them feel inferior. I think it's innocent to want people to look up to you,

59
00:09:44,480 --> 00:09:57,920
but all it does is make them encourage them to resent. Not a very wholesome, it's quite selfish.

60
00:10:03,200 --> 00:10:08,480
Kindness is praising others when you see someone doing something good to be happy for

61
00:10:08,480 --> 00:10:15,840
them to encourage them to compliment them in a true way, not flattering, trying to make them

62
00:10:15,840 --> 00:10:23,680
like you, but affirming the good in them that they might be wavering about. Did I do a good thing?

63
00:10:23,680 --> 00:10:39,440
Good for you, you did a good thing. That's kind of kindness of speech.

64
00:10:39,440 --> 00:10:44,160
The third one, of course, kindness of thoughts. We think of each other kindly.

65
00:10:44,160 --> 00:10:54,560
That which leads to all to the other two, to all acts and deeds of kindness. Of course,

66
00:10:55,120 --> 00:11:02,240
has to be thoughts of kindness because it's actually possible to be kind with body and speech

67
00:11:02,240 --> 00:11:12,240
and yet hate someone or be very corrupt in mind because we can suppress it, but it's not enough,

68
00:11:12,240 --> 00:11:16,320
and it's not true, and it's not real, it's not sincere, right?

69
00:11:19,680 --> 00:11:23,680
Much more important than our acts and our speeches is our thoughts and our

70
00:11:25,200 --> 00:11:35,280
intention, our state of mind. That's what we see in meditation now. Ultimately, we're going to become

71
00:11:35,280 --> 00:11:43,680
whatever we think about. We're going to not what we think about. We're going to become physically

72
00:11:43,680 --> 00:11:51,280
and verbally our acts and our speech are going to come inevitably from our mind. If our mind is

73
00:11:51,280 --> 00:11:58,080
impure, inevitably speech and acts are going to be impure, going to be corrupt, going to be

74
00:11:58,080 --> 00:12:09,680
cruel, unpleasant, the cause for stress and suffering. So even just cultivating thoughts of love,

75
00:12:09,680 --> 00:12:16,560
I mean, this is a great sort of artificial way of cultivating positive thoughts, thinking about

76
00:12:16,560 --> 00:12:23,680
all the people around you may be happy, may be free from suffering. And then, of course,

77
00:12:23,680 --> 00:12:34,960
cultivating insight, more natural, more sustainable, more true because it makes you a nicer person.

78
00:12:34,960 --> 00:12:40,960
It helps you see the disadvantages of being a cruel and manipulative and unpleasant person.

79
00:12:43,120 --> 00:12:47,200
You don't have to believe it, you don't have to pretend, you don't have to force yourself to be good.

80
00:12:47,200 --> 00:12:53,600
And if you're trying to force yourself to be pure, to be free, to be enlightened, if you're

81
00:12:53,600 --> 00:12:58,400
pushing for it, you can know that you're actually quite far from enlightenment and not going in

82
00:12:58,400 --> 00:13:05,280
the right direction. So only when you stop forcing, when you start looking, when you begin to see,

83
00:13:06,720 --> 00:13:11,360
when you begin to see the difference between right and wrong and good and bad without any

84
00:13:11,360 --> 00:13:19,360
reference to anyone's beliefs or teachings, it's only then that you can truly become a good person

85
00:13:21,760 --> 00:13:27,200
and have pure thoughts that are natural and since here.

86
00:13:27,200 --> 00:13:44,080
The fourth is, you know, the fourth is sharing your possession, let's say communion, let's

87
00:13:44,080 --> 00:13:52,000
make it more general. So I refer to anything from what you get, share it with the community,

88
00:13:52,000 --> 00:13:58,560
and taxation, voluntary taxation, I suppose. People don't like many people or there are some

89
00:13:58,560 --> 00:14:05,360
people who are critical of taxation saying it's like forced. I don't look at it so much as

90
00:14:05,360 --> 00:14:11,920
forced taxation is an agreement that we've come to as a society that we haven't abolished it

91
00:14:11,920 --> 00:14:21,680
is ideally because we agree that we should be sharing. If I am very lucky and very fortunate

92
00:14:21,680 --> 00:14:30,160
and even very good at what I do and I become very wealthy as a result, I should share.

93
00:14:32,160 --> 00:14:37,680
I think many of the things the social and Canada anyway, we have a lot of a social net

94
00:14:40,320 --> 00:14:42,480
that prevents people from suffering terribly.

95
00:14:42,480 --> 00:14:53,040
And many of those things are what we consider to be important as a society which is a really

96
00:14:53,040 --> 00:15:00,080
good thing to see. It's really encouraging and heartwarming. We have this principle of charity

97
00:15:00,080 --> 00:15:10,160
and, well, just charity but communion. Or if I get very happy and very lucky and very fortunate,

98
00:15:10,160 --> 00:15:14,960
I don't do it alone and I bring everyone else with me to some extent.

99
00:15:20,320 --> 00:15:31,360
Communion means seeing the community as important. On the one hand, in Buddhism, we are very

100
00:15:32,640 --> 00:15:37,760
focused on the self. At the end of me, we'll bat them on, but you'll pay anyway, say,

101
00:15:37,760 --> 00:15:43,360
set yourself in what's right to what's in your own best interest.

102
00:15:52,560 --> 00:15:58,640
But what's in your best interest, of course, is a harmonious community and

103
00:16:01,920 --> 00:16:05,840
not only that, but how can you be concerned about your own best interest if you're not

104
00:16:05,840 --> 00:16:15,280
supportive of other people seeking out what's in their own best interest, if you deny other

105
00:16:15,280 --> 00:16:20,560
people the opportunity to find peace, happiness and freedom from suffering, how could you find

106
00:16:20,560 --> 00:16:28,400
it? You're not supportive insofar as it's in your sphere of activity.

107
00:16:28,400 --> 00:16:36,800
So one of the greatest aspects of harmony is communion.

108
00:16:40,800 --> 00:16:47,680
Community, the commune, communal resources, communal benefits,

109
00:16:47,680 --> 00:17:04,320
communal happiness. The fifth is the purity of ethics, a common purity of ethics,

110
00:17:05,920 --> 00:17:14,720
meaning quite obviously that to have a harmonious community, you need laws and everyone has to be

111
00:17:14,720 --> 00:17:22,960
keeping those laws. You need good rules and the rules need to be kept.

112
00:17:25,840 --> 00:17:30,640
You see this very clearly in Buddhist monastic circles because we have

113
00:17:32,320 --> 00:17:35,280
a monastery is an interesting place because you have people

114
00:17:35,280 --> 00:17:45,680
in dealing with the very innermost poisons in their mind, right? They're dealing with their

115
00:17:45,680 --> 00:17:53,920
mental issues. They don't have a lot of outlet for them and so it can be quite a stressful activity

116
00:17:54,640 --> 00:18:02,160
as until you learn to be without chasing after the things that you want and avoiding and

117
00:18:02,160 --> 00:18:14,400
running away from the things that you don't want and you have such a diverse group of people

118
00:18:15,520 --> 00:18:20,080
whose reasons for being together have nothing to do with anything like friendship or

119
00:18:21,040 --> 00:18:27,040
common history and often in the monastery monks will come together from very different backgrounds.

120
00:18:27,040 --> 00:18:36,560
And so there's a great, in monasteries, surprisingly to some, there's a great potential for conflict

121
00:18:36,560 --> 00:18:44,000
and strife for these reasons and everyone's dealing with emotional issues. It's not a good time to

122
00:18:44,000 --> 00:18:49,520
be around other people who are also dealing with emotional issues so rules become increasingly

123
00:18:49,520 --> 00:18:57,600
important. And so we have this very rigid, in fact strict set of rules pretty much for that reason.

124
00:18:59,440 --> 00:19:05,360
You have such charged environment and such a diversity of backgrounds

125
00:19:07,040 --> 00:19:14,640
and garments, right? So it's like it's in some ways a clashing of karma because

126
00:19:14,640 --> 00:19:21,440
our narrative that we've built up, that's led us here, is often very different from where we find

127
00:19:21,440 --> 00:19:26,640
ourselves in the monastery. I was supposed to be, I don't know what I would personally, I was

128
00:19:26,640 --> 00:19:37,040
supposed to probably be a mathematician or something and I've cut that off. I could probably

129
00:19:37,040 --> 00:19:44,960
quite a successful mathematician or I don't know, even an author, many of the different things

130
00:19:44,960 --> 00:19:49,840
in the world and cut them all off went to begin a month. And you see that, you have stories like

131
00:19:49,840 --> 00:19:55,680
this, everyone had their paths. Anyway, the point being that when you come together, there's a

132
00:19:55,680 --> 00:20:04,160
need for rules. The same thing goes for society. There's such a diversity of opinions and backgrounds

133
00:20:04,160 --> 00:20:17,520
and potentials, capabilities. And until or as long as you things aren't sorted out, you need rules.

134
00:20:20,320 --> 00:20:24,800
The harmony comes from keeping rules of being laws and having good laws.

135
00:20:27,280 --> 00:20:32,240
There are people who believe that laws should be abandoned, they think there should be no

136
00:20:32,240 --> 00:20:39,760
laws. There should be no drug laws. And I can sympathize with that because

137
00:20:40,640 --> 00:20:44,800
drug laws only serve to put more people in jail because there are people doing drugs.

138
00:20:46,720 --> 00:20:55,040
It doesn't take away the fact that doing heroin and unlimited extent, I would say marijuana,

139
00:20:55,680 --> 00:20:59,520
alcohol, these are not good things for society in my opinion.

140
00:20:59,520 --> 00:21:06,320
And it's much more complicated than that. It doesn't mean we should do away with all laws or rules.

141
00:21:06,320 --> 00:21:11,680
It means we have to have a better way of actually ensuring that the rules are kept,

142
00:21:13,920 --> 00:21:21,920
that we're able to build a society where we have an order, whereby people don't fall into

143
00:21:21,920 --> 00:21:30,320
behavior that is conducive to conflict, either inner conflict or conflict in society.

144
00:21:32,160 --> 00:21:38,880
And eventually, ideally, a society that encourages people to become wiser, to become better,

145
00:21:38,880 --> 00:21:44,800
to become more pure, more free, more happy.

146
00:21:44,800 --> 00:22:01,840
And the final one is having common purity of view, so this relates to what I've been talking about,

147
00:22:01,840 --> 00:22:08,960
not only do we need rules, but to have a pure society, a harmonious society,

148
00:22:08,960 --> 00:22:14,080
you really do need to some extent, a community of common purpose.

149
00:22:16,320 --> 00:22:22,960
Most of Canada is an agreement that providing health care to all people is a good thing.

150
00:22:24,720 --> 00:22:34,800
And having that right view of kindness and support and caring for our society,

151
00:22:34,800 --> 00:22:40,560
I could say that's an important part of creating the harmony. If you look at America,

152
00:22:40,560 --> 00:22:48,720
there's incredible disharmony. America is very much divided down party lines, and probably

153
00:22:48,720 --> 00:22:57,840
there's some intent behind that. You'll be intent to create a divide and conquer a divided

154
00:22:57,840 --> 00:23:08,800
society. I don't want to get into it, but when you've got such ideology, ideological diversity,

155
00:23:08,800 --> 00:23:12,640
that's where conflict arises. So this recent in Canada, it's come up recently,

156
00:23:13,520 --> 00:23:21,200
there's been a move to, I mean, I'd say it's fairly radical, but this move to try and

157
00:23:21,200 --> 00:23:33,840
criminalize, or criminalize hate, and not just hate, but physical and verbal acts of hate.

158
00:23:35,680 --> 00:23:43,360
And to apply some fairly broad criteria for what we understand is hate. So if you deny a person's

159
00:23:43,360 --> 00:23:50,480
identity, gender identity is the big hot topic now, that can be considered a hate speech,

160
00:23:51,520 --> 00:23:59,680
or an act of a hate crime. I mean, if it was done with malicious intent, it's not like

161
00:24:00,640 --> 00:24:08,560
policing speech, it's not what everyone thinks it is. If you are doing so in a dismissive

162
00:24:08,560 --> 00:24:17,200
fashion where you really and truly have a bit complicated and perhaps radical in a sense,

163
00:24:17,200 --> 00:24:22,800
but it's caused a great division in our society, I think. I mean, on the one hand, there's

164
00:24:23,520 --> 00:24:33,600
one side, there's people who are quite militaristic in their push for their own rights.

165
00:24:33,600 --> 00:24:42,640
You know, push for their own, what the other side calls agenda, the liberal, extremist agenda.

166
00:24:46,640 --> 00:24:55,520
I mean, in the end, there's some real good there trying to make sure that people feel

167
00:24:55,520 --> 00:25:04,240
and have this sense of self-worth and sense of grounding in who they are and that they're not

168
00:25:04,800 --> 00:25:13,600
left out of the conversation, that they're not dismissed as or forced to be something they're not,

169
00:25:13,600 --> 00:25:23,040
that they can have this space to breathe, but it does get quite

170
00:25:24,800 --> 00:25:32,640
vadment and angry, which is not conducive to progress and harmony and not when it gets to that

171
00:25:32,640 --> 00:25:39,600
level, I think. It can be so much better done if it was tempered and

172
00:25:39,600 --> 00:25:51,040
associated with kindness. On the other side, there is an incredibly violent corrupt form of

173
00:25:51,680 --> 00:25:59,360
hatred and bigotry that is cloaked in the guise of free speech, but really it's just about

174
00:26:00,160 --> 00:26:02,720
hating people who are different than people who have

175
00:26:02,720 --> 00:26:12,240
a different way of looking at the world. We see this in society and we see this in monasteries,

176
00:26:12,240 --> 00:26:21,040
the story of many stories of monks beating on monks and monks attacking monks and monks fighting

177
00:26:21,040 --> 00:26:33,760
with monks. We have stories going back to the Buddhist time, which is sort of thing.

178
00:26:33,760 --> 00:26:40,960
Right now we're talking about view. We have stories of monks arguing with monks. We have

179
00:26:40,960 --> 00:26:44,800
divisions from the time of the Buddha right after the Buddha passed away. There was right away

180
00:26:44,800 --> 00:26:50,720
a split and it's split and it's split and it's split and now we've got forms of Buddhism that

181
00:26:50,720 --> 00:26:58,800
I don't even recognize as anything to do with Buddhism. Very different forms. I've got splits among

182
00:26:58,800 --> 00:27:04,400
communities. I've been in, I was in a monastery once where there were three groups of monks. They

183
00:27:04,400 --> 00:27:12,240
were had a split and in the monastery there were three communities and they didn't interact with

184
00:27:12,240 --> 00:27:26,240
each other. They skimmed against each other. It was quite impressive in a way, I guess.

185
00:27:29,680 --> 00:27:36,480
Having the same view for us as meditators, this means really having a view of the

186
00:27:36,480 --> 00:27:42,640
four noble truths. That gives you the greatest harmony. When you see that and there's none of this,

187
00:27:44,160 --> 00:27:48,800
there would never be a question about identity. There would never be a question about

188
00:27:50,800 --> 00:27:58,160
a clique or a group or a division of the sangha. Just wouldn't have any sense or meaning or

189
00:27:58,160 --> 00:28:07,600
purpose. Not to mention all the anger and greed involved and just have disappeared.

190
00:28:10,160 --> 00:28:15,680
It's important that we're clear and that we have these debates in society. It's important that we

191
00:28:15,680 --> 00:28:22,560
debate. It's important that we discuss. It's important that we listen to each other.

192
00:28:22,560 --> 00:28:32,240
It's one thing that being in the university has taught me is how to listen. How to listen to

193
00:28:32,240 --> 00:28:37,360
people who I might disagree with. When it gives you an interesting perspective on why people

194
00:28:37,360 --> 00:28:42,560
think things they do and it helps you understand how people get to the views that they get to,

195
00:28:42,560 --> 00:28:48,560
even though you might completely disagree with their views. You at least can understand what

196
00:28:48,560 --> 00:28:58,160
that makes them and where they come from. By listening and by talking, we can

197
00:28:59,840 --> 00:29:09,440
break through and come to an understanding with each other. There's many factors involved.

198
00:29:09,440 --> 00:29:16,800
In rules, you need kindness, you need a desire for truth and desire for harmony.

199
00:29:16,800 --> 00:29:26,640
The worst enemy to harmony, of course, is the desire for conflict. If society is keen to

200
00:29:28,160 --> 00:29:36,640
hurt and to cause harm and to disrupt, it doesn't matter how well-intentioned you might be

201
00:29:36,640 --> 00:29:46,560
clear, how clear you might be in your mind you're never going to succeed in bringing harmony

202
00:29:46,560 --> 00:29:56,400
to society. That's a sort of view in itself. The view, the view that harmony and peace are

203
00:29:56,400 --> 00:30:03,280
worth working towards, the view that the true goal should be happiness and should be peace,

204
00:30:03,280 --> 00:30:08,000
that there is no happiness without peace, not peace and people who come.

205
00:30:11,440 --> 00:30:13,280
Once you have peace, you have happiness.

206
00:30:17,520 --> 00:30:21,600
So there you go. Those are the sara near the month. They're dumbness that we should all

207
00:30:22,400 --> 00:30:28,400
recollect and they help us think fondly with each other. They help make the community

208
00:30:28,400 --> 00:30:38,400
a harmonious community. So thank you all for tuning in. Have a good night.

