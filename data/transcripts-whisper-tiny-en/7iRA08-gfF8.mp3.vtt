WEBVTT

00:00.000 --> 00:07.200
Hello, welcome back to Ask a Monk. This next question is a pretty good example, I think,

00:07.200 --> 00:17.600
of how not to ask questions on this forum. And that's because it's specifically, well,

00:17.600 --> 00:26.000
it seems quite clearly to be a presentation of one's own philosophy in the form of a question,

00:26.000 --> 00:31.200
because here we have a premise and a question I'll read it to you. I want to ask about

00:31.200 --> 00:37.600
non-duality, when one doesn't distinguish the two realms of Samsara and Nirvana, then there is no

00:37.600 --> 00:44.400
goal. So my question is, is wanting to transcend reverb selfish? But what you have here is a complete

00:44.400 --> 00:50.000
philosophy that exists in Buddhism. Buddhism, there are many philosophies and they're not all

00:50.000 --> 00:59.440
compatible. This one I happen to not agree with. And this is a complete presentation of this

00:59.440 --> 01:11.600
philosophy. And it's clearly so because the premise and the conclusion that is presented in the

01:11.600 --> 01:18.560
premise, which the implicit conclusion being that striving for the end of reverb is selfish,

01:18.560 --> 01:28.880
it has nothing to do with each other unless accepting the greater framework of this philosophy.

01:29.600 --> 01:38.320
So it seems clear that the purpose of this post is not to ask a question, it's to present a philosophy,

01:38.320 --> 01:44.000
which is not really appropriate. Now, I'm not sure this is the case. It may be that the person

01:44.000 --> 01:49.520
asking is really interested in really things that I believe that these two things are

01:49.520 --> 01:58.960
indistinguishable and somehow thinks that that effects has some effect on the question as to

01:58.960 --> 02:06.240
whether strengthening reverb with the selfish. I don't see it. So briefly I'll go over this,

02:06.240 --> 02:12.000
but this isn't the kind of question that I'm interested in asking. I would like to be answering

02:12.000 --> 02:21.120
questions about meditation and that really help you understand the Buddhist teaching and

02:22.880 --> 02:27.600
understand the meditation practice and improve your meditation practice and help you along the

02:27.600 --> 02:37.520
path. So, but anyway, the premise, why I disagree with this philosophy, the idea that some

02:37.520 --> 02:45.440
sara and nirvana are indistinguishable is just a solpism. It's a theory. It's a view. It has

02:45.440 --> 02:50.400
something to do with reality. If you experience some sara, some sara is one thing. This is some sara

02:50.400 --> 02:55.120
seeing the hearing, smelling the tasting and feeling and thinking. It's theorizing of suffering

02:55.120 --> 03:01.760
and the pursuit of suffering. nirvana is the opposite. It's the non-arising of suffering,

03:01.760 --> 03:12.080
the non pursuit of suffering. The reason why people are unable to distinguish it is because they

03:12.080 --> 03:17.760
haven't experienced it. Everything they've experienced is the same. They practice meditation and

03:17.760 --> 03:23.120
their experiences come and go and so they relate that back to some sara and it also comes and goes

03:23.120 --> 03:27.120
and everything comes and goes and they say nothing is different. So they're unable to distinguish

03:27.120 --> 03:33.040
one from the other, which is correct really because everything is indistinguishable except nirvana.

03:33.040 --> 03:38.960
nirvana. Everything else arises in sieces and this is why the Buddha said nothing is worth

03:38.960 --> 03:49.360
clinging to because it all arises in sieces. So, it's basically equating two opposites or

03:49.360 --> 03:57.680
confusing two opposites. It's like saying that night and day are indistinguishable, light and

03:57.680 --> 04:09.920
darkness are indistinguishable. Theoretically, you can come up with a theory that says that is the

04:09.920 --> 04:15.600
case but doesn't affect the reality that light is quite different from darkness and that's

04:15.600 --> 04:23.440
objectively so nirvana and samsara are objectively so as well. No matter what theory or philosophy

04:23.440 --> 04:31.200
you come up with because you can come up with any philosophy you like and it doesn't affect,

04:31.200 --> 04:38.720
it doesn't affect on reality. As to the question, I'm assuming that the implication here is that

04:38.720 --> 04:51.520
somehow that premise implies that or leads to the conclusion, supports the conclusion that

04:52.080 --> 04:59.840
freedom or the desire wanting to transcend rebirth is selfish. I'm assuming that's what's implied here.

04:59.840 --> 05:08.720
So, I can talk about that. Selfishness only really comes into play and Buddhism as I understand it

05:10.400 --> 05:14.960
in terms of clinging. So, if you cling to something and you don't want someone else to have it,

05:16.080 --> 05:19.600
if you ask me for something and I don't want to give it to you, it's only because I'm clinging

05:19.600 --> 05:28.000
to the object. There's no true selfishness that arises. It's a clinging. It's a function of the

05:28.000 --> 05:33.520
mind that wants an object. You don't think about the other person. What we call selfishness is

05:33.520 --> 05:42.400
this absorption in one's attachment and only by overcoming that can you possibly give up something

05:42.400 --> 05:47.360
that is a benefit to you and we're able to do that as we grow up. It's not because we become, well,

05:47.360 --> 05:57.920
it could be because we become more altruistic but that is not the way of the Buddha, that's not

05:57.920 --> 06:03.760
the answer because this idea of altruism is just another theory. It's another mind game and there

06:03.760 --> 06:08.080
are people who even refute this theory and say, you know, how can you possibly work for the

06:08.080 --> 06:15.520
benefit of other people? But the Buddha's teaching doesn't go either way. There's no idea that

06:16.400 --> 06:23.600
working for one's benefit is better than working for one's own benefit. You act in such a way

06:23.600 --> 06:28.800
that brings harmony. You act in such a way that leads to the least conflict. You act in such

06:28.800 --> 06:36.640
a way that is going to be most appropriate at any given time and it's not a intellectual exercise.

06:36.640 --> 06:41.280
You don't have to study the books and learn what is most appropriate though. That can help

06:42.400 --> 06:49.680
for people who don't have the experience. You understand the nature of reality through your

06:49.680 --> 06:54.960
practice and therefore are able to clearly see what is of the greatest benefit? What is the most

06:54.960 --> 06:59.360
appropriate at any given time? And it might be acting for your own benefit. It might be acting

06:59.360 --> 07:08.000
for someone else's benefit but the point is not either or. The point is the appropriateness of the

07:08.000 --> 07:15.760
act. So if someone comes and asks you for something many things might come into play. It depends on

07:15.760 --> 07:21.200
the situation. If you ask me for something, I might give it to you even though it might cause me

07:21.200 --> 07:25.760
suffering. There may be many reasons for that. It could be because you've helped me before. It could

07:25.760 --> 07:32.640
be because I am able to deal with suffering. I know that you're not. I have practiced meditation and

07:32.640 --> 07:37.520
I am able to be patient and so on. I can see that you're not. I can see that giving to you

07:37.520 --> 07:42.960
will lead to less suffering overall because for me it's only physical suffering for you. It's mental

07:42.960 --> 07:47.680
and so on. I might not give you any of it because I might see that you are not going to use it

07:47.680 --> 07:51.680
or I might see that if you're without it, you're going to learn something. You're going to learn

07:51.680 --> 07:58.800
patients and so on. There are many responses. This is talking about enlightened person. If a

07:58.800 --> 08:04.080
person is enlightened, they will act in this way. They will never rise to them. My benefit,

08:04.080 --> 08:10.240
you're a benefit because there's no clinging and that's the only time that selfishness arises.

08:10.240 --> 08:21.760
So the idea that wanting to transcend rebirth is selfish doesn't come into play. It has nothing to do

08:21.760 --> 08:26.560
with anyone else. The only way you could call it selfish is if you start playing these mind games

08:26.560 --> 08:34.240
and logic and it's an intellectual exercise where you say, well, if you did stay around you could

08:34.240 --> 08:40.560
help so many people and therefore it is selfish to, you know, but it's just a theory, right? It

08:40.560 --> 08:45.200
has nothing to do with the state of mind. The person could be totally unselfish and if anyone asked

08:45.200 --> 08:49.760
them or something, they would help them. But they don't ever think, hmm, boy, I should stick around

08:49.760 --> 08:55.440
otherwise it's going to be selfish because there's no clinging. There's no attachment to this

08:55.440 --> 09:01.440
state or that state. The person simply acts as is appropriate in that instance. So

09:01.440 --> 09:10.240
the realization of freedom from rebirth or the attainment of freedom from rebirth is of the

09:10.240 --> 09:16.080
greatest benefit to a person. If a person wants to strive for that, then that's a good thing. It

09:16.080 --> 09:26.640
creates benefit. If a person decides to extend their or not work towards the freedom from

09:26.640 --> 09:30.800
rebirth and they want to be reborn again and again, then that's their prerogative. The Buddha never

09:30.800 --> 09:38.320
laid down any laws of nature in this regard because there are none. There is simply the cause

09:38.320 --> 09:46.400
and effect. If a person decides that they want to extend their existence and come back again and

09:46.400 --> 09:51.280
again and be reborn again and again things are going to help people then that's fine or that's

09:51.280 --> 09:59.360
their choice. That's their path. There's no need for any sort of judgment at all. If a person

09:59.360 --> 10:04.800
decides that they want to become free from suffering in this life and not come back,

10:04.800 --> 10:11.440
be free from rebirth, then that's their path as well. There's only the cause and effect. So a

10:11.440 --> 10:15.520
person who is reborn again and again and again, will have to come back again and again and

10:15.520 --> 10:24.720
again. It's questionable as to how much help they can possibly be given that they are not yet

10:24.720 --> 10:37.760
free from clinging because a person who is free from clinging would not be reborn. The person,

10:37.760 --> 10:45.760
actually the person who does become free from rebirth, who does transcend rebirth, so to speak,

10:46.960 --> 10:51.760
is able to help people because their mind is free from clinging and therefore they are able to

10:53.760 --> 10:57.680
provide great support and help and great insight. People, the Buddha is a good example.

10:57.680 --> 11:09.120
Now there is the question of whether the Buddha and enlightened beings do come back because there

11:09.120 --> 11:14.400
are even philosophies that believe a person who is free from clinging still is reborn, but that's

11:14.400 --> 11:22.240
not really the question here. So it's getting a little bit too much into other people's philosophies

11:22.240 --> 11:28.320
and I don't want to create more confusion than is necessary. So the point here is that we work for

11:28.320 --> 11:37.200
benefit, we work for welfare, we do it's appropriate, or we try to, this is our practice,

11:37.200 --> 11:43.840
and then enlightened being is someone who is perfect at it, who doesn't have any thought of self

11:43.840 --> 11:54.480
or other, and is not subject to suffering when they pass away from this life, they are free

11:54.480 --> 12:04.800
from all suffering and there's no more coming back, no more rising. So basically I just wanted to

12:04.800 --> 12:15.440
say that this is not the sort of question that I'm hoping to entertain and I think probably in

12:15.440 --> 12:19.760
the future I'm just going to skip over because I got a lot of questions and I'm going to try to

12:19.760 --> 12:25.280
be selective, I've been quite random actually in choosing which questions to answer because it's

12:25.280 --> 12:36.320
difficult to sort them, I've got a lot of them and Google doesn't make it easy by any means, so I'm

12:36.320 --> 12:44.880
going to try to skip and go directly to those ones that I think are going to be useful for people.

12:44.880 --> 12:52.320
Okay so anyway thanks for tuning in so it's been another episode of Ask a Monk and wishing you

12:52.320 --> 12:55.840
all peace happiness and freedom from suffering.

