1
00:00:00,000 --> 00:00:09,160
Okay, so welcome everyone to our weekly televised internet broadcast

2
00:00:09,160 --> 00:00:16,600
Montgradio announcements for this week. Well, we just came back yesterday

3
00:00:16,600 --> 00:00:27,160
evening from Anodadapura to see the Srimahamboditri, which is a descendant from

4
00:00:27,160 --> 00:00:33,360
the original tree in Montgraya and was actually returned back, the one that

5
00:00:33,360 --> 00:00:42,760
was returned back to Montgraya when the one in Montgraya was killed. So very

6
00:00:42,760 --> 00:00:48,240
special place and we also went to Polonarua to see some ancient

7
00:00:48,240 --> 00:00:55,120
distant sites and I was able to give two talks and take two videos so they went

8
00:00:55,120 --> 00:01:03,840
up today. If you haven't seen them, they're now on YouTube. And the workers

9
00:01:03,840 --> 00:01:10,840
have started now on two new, new rooms. So we're soon going to have two new

10
00:01:10,840 --> 00:01:14,840
rooms and this evening the carpenter came so he's going to put a roof on

11
00:01:14,840 --> 00:01:23,840
the meditation hall by the Boditri and then we won't be able to do our

12
00:01:23,840 --> 00:01:31,600
rain meditation anymore and we'll be dry and we'll be able to be able to

13
00:01:31,600 --> 00:01:36,480
do chanting up there and so on. We'll be more certain, be cool during the day

14
00:01:36,480 --> 00:01:45,120
though, we'll have a cool place during the day as well. Otherwise, Kat is

15
00:01:45,120 --> 00:01:52,160
leaving tomorrow though. I'm going to get my visa tomorrow hopefully leaving the

16
00:01:52,160 --> 00:01:58,400
next day and hopefully leaving the next day you hear that. I do hope for

17
00:01:58,400 --> 00:02:03,600
getting the visa. Hopefully I can leave this warm place where they make me

18
00:02:03,600 --> 00:02:14,520
stay in tents and your fan got destroyed twice. I have no electricity and you still

19
00:02:14,520 --> 00:02:20,160
have no electricity. So Kat, maybe you can tell us how was your stay here. What's

20
00:02:20,160 --> 00:02:29,040
this place like? Is it tolerable for meditation? Yeah, it's very suitable for

21
00:02:29,040 --> 00:02:35,760
meditation, I think. How was the tent? The tent was an excellent

22
00:02:35,760 --> 00:02:41,840
mountain. You'd recommend it to people thinking about bringing a tent here or

23
00:02:41,840 --> 00:02:46,480
coming to stay in one of our tents. Yeah, you come and stay in the tent close to

24
00:02:46,480 --> 00:02:56,160
nature. What do you think of Sri Lanka? Is this your first time in Sri Lanka?

25
00:02:56,160 --> 00:03:02,080
Yeah, it's my first time in Sri Lanka. Sri Lanka is a beautiful country and

26
00:03:02,080 --> 00:03:07,360
yeah, I don't know. I think it's a bit like, I don't know if people might not like

27
00:03:07,360 --> 00:03:12,480
this but I see it a bit like cross-jean Thailand in India in that it's

28
00:03:12,480 --> 00:03:17,680
sort of similar scenery to India but much more relaxed. You can feel that it's a

29
00:03:17,680 --> 00:03:22,480
Buddhist country. It's more of a feel like Thailand but actually even more

30
00:03:22,480 --> 00:03:27,120
relaxed in Thailand. People are friendlier. I was telling you I went to

31
00:03:27,120 --> 00:03:32,880
Canada the other day and two buses there and three buses back and no one would

32
00:03:32,880 --> 00:03:41,040
let me pay for the bus. They didn't want any money for me. Yeah, very friendly people.

33
00:03:41,040 --> 00:03:46,800
What did you think of Anoradapura? Very spiritual place, I think.

34
00:03:46,800 --> 00:03:51,120
So we stayed there. I didn't mention actually after I had a blog post, but

35
00:03:51,120 --> 00:03:57,280
we stayed overnight in front of the JITA one. So we stayed in JITA one and the

36
00:03:57,280 --> 00:04:03,280
ancient monastery of JITA one. We actually camped out in this ruined monastery

37
00:04:03,280 --> 00:04:12,000
under the JITA and some of the, as I mentioned in one of the videos, the

38
00:04:12,000 --> 00:04:15,680
people were coming by. Even during the night they were coming by to go and pay

39
00:04:15,680 --> 00:04:19,840
respect to the Bodhi train walk right past where we were meditating and

40
00:04:19,840 --> 00:04:24,720
if they stopped any of you that they stopped to talk to me and asked me

41
00:04:24,720 --> 00:04:29,920
where the tree was and who I was and where I was from.

42
00:04:29,920 --> 00:04:33,760
One man even even asked me what I thought what I think of Buddhism

43
00:04:33,760 --> 00:04:38,400
mentioned. It's a funny question to ask of a Buddhist monk really.

44
00:04:38,400 --> 00:04:42,640
What do you think is Buddhism and good religion?

45
00:04:45,440 --> 00:04:48,800
What did you think of Paul and Arua?

46
00:04:48,800 --> 00:04:55,520
Also a very interesting place. The parts we went to not as spiritual

47
00:04:55,520 --> 00:05:03,600
as Andrada poor, but yet the the reclining Buddha was very impressive

48
00:05:03,600 --> 00:05:07,040
as an interesting history.

49
00:05:07,920 --> 00:05:13,680
So tomorrow we're going to get these, we're going to try once more

50
00:05:13,680 --> 00:05:20,800
third times the charm to get visas for our two

51
00:05:20,800 --> 00:05:25,360
initiates. I don't know how you say, we're applicants for our donation.

52
00:05:25,360 --> 00:05:28,080
Aspirants.

53
00:05:30,160 --> 00:05:38,000
And so yeah, so if there's anyone out there who's watching this and

54
00:05:38,000 --> 00:05:43,680
happens to have some connection with someone who can help us please please

55
00:05:43,680 --> 00:05:48,560
where we're not here to do harm to the country. We're just looking to

56
00:05:48,560 --> 00:05:54,400
continue to practice and study and teach and spread the Buddhist teaching

57
00:05:54,400 --> 00:05:58,960
and Sri Lanka is for I think all of us are feeling that Sri Lanka is the

58
00:05:58,960 --> 00:06:03,760
place to do it. So we're just hoping to be able to stay

59
00:06:03,760 --> 00:06:08,000
and to grow our community.

60
00:06:08,400 --> 00:06:11,760
So that's all for announcements. Anyone have anything else to say?

61
00:06:11,760 --> 00:06:15,200
How's the I'm going to give a summary of the construction

62
00:06:15,200 --> 00:06:19,440
yourself? What's the construction going yourself? Well thank you, you just mentioned it,

63
00:06:19,440 --> 00:06:23,760
which is having two of your rooms made and

64
00:06:23,760 --> 00:06:27,600
that's basically it at the moment. There's not much more happening there.

65
00:06:27,600 --> 00:06:30,880
The way there is, we're doing our new path you know.

66
00:06:30,880 --> 00:06:36,480
So it's raining every day at the moment and there's one part where

67
00:06:36,480 --> 00:06:40,160
it rains heavily. We have problems and hopefully that'll be done

68
00:06:40,160 --> 00:06:49,600
next two or three days. That's about it. So maybe that was the kitchen going.

69
00:06:53,040 --> 00:06:58,240
Kitchen is good. Our house uh

70
00:06:59,440 --> 00:07:06,000
there's not much to talk about there but uh we're we're trying to set up but

71
00:07:06,000 --> 00:07:09,120
routine for the the meditators can you explain the sort of things that

72
00:07:09,120 --> 00:07:15,680
meditators can expect if they come here the food and the drinks and so on.

73
00:07:15,680 --> 00:07:22,480
I'm trying to keep it simple. Just making sure that there's

74
00:07:22,480 --> 00:07:27,760
enough proteins sometimes on arms round. We don't get

75
00:07:27,760 --> 00:07:35,680
to many beings of doll or soy. They eat soy beans here so

76
00:07:35,680 --> 00:07:41,280
I've started cooking some doll and soy some beans

77
00:07:41,280 --> 00:07:46,480
to we've had a full house lately of meditators and

78
00:07:46,480 --> 00:07:52,880
just want to make sure that um you know people who are working hard are

79
00:07:52,880 --> 00:08:00,480
getting enough nutrition. It's just protein and also some brown rice.

80
00:08:00,480 --> 00:08:06,320
You get a lot of white rice and westerners tend to be a bit

81
00:08:06,320 --> 00:08:10,800
picky towards the brown rice. Because you get brown rice

82
00:08:10,800 --> 00:08:15,520
uh because we could get brown or red rice and actually we can get a rice cooker

83
00:08:15,520 --> 00:08:20,640
if we can clean these here. That's something we didn't think of. I mean if it is

84
00:08:20,640 --> 00:08:24,480
Paul and Yarnie thought of that. She thought. I mean to you a rice cooker so easy

85
00:08:24,480 --> 00:08:26,960
and then you just put rice push the button in the next cooker.

86
00:08:26,960 --> 00:08:31,280
And then I don't have to get up at 3.30. We don't anyway don't get up.

87
00:08:31,280 --> 00:08:35,440
So okay but then in the morning what sort of things do we give to the meditators

88
00:08:35,440 --> 00:08:39,200
and and specifically where do we get our food from for the meditators? You've

89
00:08:39,200 --> 00:08:42,400
already mentioned it but just to make it clear where do we

90
00:08:42,400 --> 00:08:46,000
where do they get their food and what sorts of food and then in the evening

91
00:08:46,000 --> 00:08:49,280
what can they drink and what do we have for them to drink and so on?

92
00:08:49,280 --> 00:08:54,160
Well so I think technically the meditators are supposed to wait till

93
00:08:54,160 --> 00:08:58,720
they have sent up to eat anything besides

94
00:08:58,720 --> 00:09:05,360
juice or sugar sugar water drinks. But we have

95
00:09:05,360 --> 00:09:09,200
made oats quick oats. We're trying to make that

96
00:09:09,200 --> 00:09:13,440
a sort of an available staple but don't don't expect it

97
00:09:13,440 --> 00:09:17,920
but we'll try to have it available soon. If you want to make a

98
00:09:17,920 --> 00:09:25,120
breakfast of oatmeal that should be available and then

99
00:09:25,120 --> 00:09:30,560
I've been making kind of a rice congee for the monks

100
00:09:30,560 --> 00:09:36,640
the Buddha allowed the monks to have some rice congee before they go on

101
00:09:36,640 --> 00:09:40,320
alms around because it's quite a track I think today. I actually

102
00:09:40,320 --> 00:09:45,200
timed it it was an hour and I guess there's a

103
00:09:45,200 --> 00:09:49,520
route that's even longer than that so yeah.

104
00:09:49,520 --> 00:09:54,000
Stop doing that one because now we're only two of us so

105
00:09:54,000 --> 00:09:59,760
sorry excuse for not doing that one. Yeah and on alms around

106
00:09:59,760 --> 00:10:05,920
there's quite an assortment of Sri Lanka seems to

107
00:10:05,920 --> 00:10:10,720
be into biscuits so we get a lot of biscuits I'm not sure if that was

108
00:10:10,720 --> 00:10:17,920
brought from the English or what happens to me.

109
00:10:17,920 --> 00:10:22,400
They put tea leaves in the food so even though we're not supposed to

110
00:10:22,400 --> 00:10:25,920
have any caffeine here um they're still getting

111
00:10:25,920 --> 00:10:30,320
tea leaves in the food um I don't think it probably has

112
00:10:30,320 --> 00:10:35,120
much effect I don't know. The food is the food is fairly spicy

113
00:10:35,120 --> 00:10:42,160
you know and almost entirely vegetarian so there's a little bit of fish.

114
00:10:42,160 --> 00:10:47,840
I would say that um I I speak for myself but I only notice

115
00:10:47,840 --> 00:10:52,160
the food being really spicy maybe the first week or two so I'd say if you

116
00:10:52,160 --> 00:10:57,040
stick it out the spicyness shouldn't affect you and no one's really been

117
00:10:57,040 --> 00:11:00,800
getting sick from the food so that's a big plus.

118
00:11:00,800 --> 00:11:05,360
So Paul and the other announcement is that Paulian Paulian is not here because she's sick

119
00:11:05,360 --> 00:11:10,400
but she has stomach problems and it's chronic so

120
00:11:10,400 --> 00:11:14,560
that's more a personal thing and then with the food or the water here but if

121
00:11:14,560 --> 00:11:19,120
you have a weak stomach you you know you kind of have to put up with it so

122
00:11:19,120 --> 00:11:22,880
but it's really only been her no everyone else's I mean sometimes a little bit of

123
00:11:22,880 --> 00:11:27,280
diarrhea but I got that in Canada as well it's a part of

124
00:11:27,280 --> 00:11:33,680
life and that's the other thing is um the food is mostly vegetarian they

125
00:11:33,680 --> 00:11:41,360
sometimes offer fish um and then some people come I'm almost every day

126
00:11:41,360 --> 00:11:47,680
people come and bring food to the monastery as well so there's always

127
00:11:47,680 --> 00:11:57,680
enough food available as Cathy would say it has so much food almost every day

128
00:11:57,680 --> 00:12:04,000
this is this is the quote of Cathy oh my god so much food over that

129
00:12:04,000 --> 00:12:07,760
we're kind of concerned that the meditators may not be getting enough protein

130
00:12:07,760 --> 00:12:13,680
enough um I mean maybe that's just a obsession of ours but

131
00:12:13,680 --> 00:12:18,880
we're kind of agreed to supplement a little bit of protein so as you already said

132
00:12:18,880 --> 00:12:24,000
how about in the afternoon what are we doing now for the afternoon?

133
00:12:24,000 --> 00:12:30,320
so we also get offered a lot of local fruits and so there's

134
00:12:30,320 --> 00:12:34,960
quite a lot of farmland around so we've been trying

135
00:12:34,960 --> 00:12:46,320
to get juiced fresh fruit juiced and strained has to be strained for the monks

136
00:12:46,320 --> 00:12:52,400
but it's not enough and it takes quite a bit of fruit to make juice so

137
00:12:52,400 --> 00:12:58,240
we're trying to buy non-sugar-y drinks for the meditators

138
00:12:58,240 --> 00:13:05,680
you know added sugar drinks so right now we have pomegranate 100% it's very concentrated

139
00:13:05,680 --> 00:13:14,000
and apple juice um and hopefully that'll last for a while

140
00:13:14,000 --> 00:13:19,120
um problem is that's all important but we you know we

141
00:13:19,120 --> 00:13:22,800
they point us to have something for the meditators in the afternoon the Buddha

142
00:13:22,800 --> 00:13:27,280
allowed meditators to have the monks to have fruit juice in the

143
00:13:27,280 --> 00:13:31,360
afternoon so you'll find that it's just enough

144
00:13:31,360 --> 00:13:36,400
and the afternoon we wouldn't want to have milk or

145
00:13:36,400 --> 00:13:40,960
something more solid because it makes you tired and it

146
00:13:40,960 --> 00:13:45,600
tends to lead to extravagance and so on but juice you find it's just enough

147
00:13:45,600 --> 00:13:48,400
to keep you going but not enough to make you

148
00:13:48,400 --> 00:13:53,040
intoxicated with your strength so

149
00:13:53,040 --> 00:13:58,800
could you um because I found this very interesting when I came here I didn't know that there was a rule

150
00:13:58,800 --> 00:14:02,160
about the boiled vegetables leaves and why we don't drink tea in

151
00:14:02,160 --> 00:14:05,920
afternoons here could you say something about that

152
00:14:05,920 --> 00:14:10,960
well boiled boiled vegetables is

153
00:14:10,960 --> 00:14:14,560
I think it's just too they understood too much nutrient it becomes much

154
00:14:14,560 --> 00:14:21,040
like a food or else it's just too too much work to

155
00:14:21,040 --> 00:14:25,440
boil things I'm not sure but boiled boiled vegetables are not allowed only

156
00:14:25,440 --> 00:14:29,760
fresh pressed vegetable or fruit juice

157
00:14:29,760 --> 00:14:35,280
it's allowed yeah it's probably because there's too much you know when you

158
00:14:35,280 --> 00:14:38,320
boil something you take all the nutrients out of it or

159
00:14:38,320 --> 00:14:46,480
you know it gained some of the particles of it I don't know

160
00:14:46,480 --> 00:14:53,840
and tea well tea is a medicine so you could take it if you were sick and if

161
00:14:53,840 --> 00:14:59,680
there were some sickness that tea cured but

162
00:15:03,360 --> 00:15:08,800
it's not a it's not a juiceable leaf no you can't

163
00:15:08,800 --> 00:15:12,080
and I guess that's the thing is you can't you can't blend up tea leaves and get

164
00:15:12,080 --> 00:15:16,160
juice from them so when you boil them you're taking something out of

165
00:15:16,160 --> 00:15:20,480
them that you wouldn't get from from a juicer

166
00:15:20,480 --> 00:15:23,840
it's on a different level

167
00:15:25,680 --> 00:15:29,280
and the fact that it's not for the sugar there's no sugar in them it's just

168
00:15:29,280 --> 00:15:33,840
for the drugs tea is for the drug that's in it the caffeine

169
00:15:33,840 --> 00:15:38,080
drug or and also you can say antioxidant sort of but

170
00:15:38,080 --> 00:15:41,040
it's not the same thing the point is to have something that has natural

171
00:15:41,040 --> 00:15:48,160
fruit sugars is that's about we tried to stay away from

172
00:15:48,160 --> 00:15:53,600
they call stimulants I think that's great use what I mean when I was

173
00:15:53,600 --> 00:15:57,360
when I was teaching or when I was studying at jump tongue and

174
00:15:57,360 --> 00:16:02,000
with a lot of foreign meditators I saw two meditators

175
00:16:02,000 --> 00:16:07,760
when we when we asked them to stay up on they they they would

176
00:16:07,760 --> 00:16:12,640
they would go and and every hour have a cup of coffee

177
00:16:12,640 --> 00:16:15,920
so they ended up having over the course of the night like eight cups of coffee

178
00:16:15,920 --> 00:16:20,000
and when crazy two meditators who actually went insane

179
00:16:20,000 --> 00:16:23,920
in part because of the amount of caffeine that they had and

180
00:16:23,920 --> 00:16:30,480
you know it's really very much against the the the whole point of the thing

181
00:16:30,480 --> 00:16:36,160
is to push you in to to try to you know test you and train you and

182
00:16:36,160 --> 00:16:40,000
if you're cheating and and using stimulants to stay away

183
00:16:40,000 --> 00:16:43,040
it's not likely that you're going to get the results that are intended your

184
00:16:43,040 --> 00:16:49,040
mind will never have before to to develop the cultivation of the

185
00:16:49,040 --> 00:16:54,000
insight and mindfulness just won't be there because you're

186
00:16:54,000 --> 00:16:58,640
you're you're you're you're just using stimulants to

187
00:16:58,640 --> 00:17:07,280
enhance your ability to stay awake and then of course you crash in the morning

188
00:17:07,280 --> 00:17:11,840
not in the same the same way as caffeine caffeine is a steroid I think or

189
00:17:11,840 --> 00:17:17,520
it has a very chemical effect on the body sugar just gives energy

190
00:17:17,520 --> 00:17:22,160
I mean yeah you could say anything's a drug all food is in that sense of drug but

191
00:17:22,160 --> 00:17:25,840
their caffeine is on a different level because it's specifically

192
00:17:25,840 --> 00:17:30,000
stimulating so I don't know some part of the body

193
00:17:30,000 --> 00:17:32,240
I don't know when I have caffeine I'm

194
00:17:32,240 --> 00:17:36,640
pricky and when I used to have coffee I could really feel it suddenly it

195
00:17:36,640 --> 00:17:42,640
feel all bubbly and and excited and so it's the whole body starts to

196
00:17:42,640 --> 00:17:49,440
kick into a higher gear sugar doesn't do that to me

197
00:17:49,440 --> 00:17:56,640
if I have sugar yeah I get more into the careful and white sugar as well as

198
00:17:56,640 --> 00:18:01,360
there's no sugar in there anyway so this is the the point was to just talk a

199
00:18:01,360 --> 00:18:04,960
little bit about the sort of things that meditators can expect

200
00:18:04,960 --> 00:18:10,560
when they come here and the sort of lifestyle that they'll have

201
00:18:10,560 --> 00:18:19,840
we have someone asked how do we make sure we get enough protein how do you exercise

202
00:18:19,840 --> 00:18:24,240
don't worry about it in the end everyone dies

203
00:18:24,240 --> 00:18:30,000
sickness is a good good meditation object

204
00:18:31,680 --> 00:18:35,280
malnutrition malnutrition is a good thing

205
00:18:35,280 --> 00:18:42,080
if you if you're over if you if you're over nutrition you will

206
00:18:42,080 --> 00:18:46,640
over nourished then you will give rise to all sorts of

207
00:18:46,640 --> 00:18:51,680
lusts and impassioned it's not just lust but anger and people will

208
00:18:51,680 --> 00:18:55,280
become very become very passionate this is why in some religions

209
00:18:55,280 --> 00:18:58,960
are actually fast they'll stop eating because they think that's a spiritual

210
00:18:58,960 --> 00:19:06,080
practice of suppressing the defilements through not eating at all and

211
00:19:06,080 --> 00:19:09,280
they'll do it for days and days and and they'll think that that's somehow

212
00:19:09,280 --> 00:19:13,440
beneficial to them it's actually not it's an extreme and it's

213
00:19:13,440 --> 00:19:17,280
missing the point that the you need to see the defilements you need to

214
00:19:17,280 --> 00:19:21,600
understand them you need to to work out the habits involved with them

215
00:19:21,600 --> 00:19:25,280
and you can't do that if you're if you're so weak that they don't even arise

216
00:19:25,280 --> 00:19:28,560
but on the other hand you can't do it when they're so strong that you're

217
00:19:28,560 --> 00:19:32,640
forced to follow after them so you have to be very careful with food actually

218
00:19:32,640 --> 00:19:35,840
my teacher always would go on and on about food and

219
00:19:35,840 --> 00:19:40,000
and how important is moderation and food is so important not on and on but

220
00:19:40,000 --> 00:19:43,040
every day again and again he would bring this up

221
00:19:43,040 --> 00:19:45,600
of course then I found out that the reason was probably because some of the

222
00:19:45,600 --> 00:19:50,320
monks were eating in the evening so it was his way of hinting at them that

223
00:19:50,320 --> 00:19:55,120
it's not cool but it is quite important you shouldn't eat too

224
00:19:55,120 --> 00:20:00,240
little and you shouldn't eat too much you should eat just enough to stay alive

225
00:20:00,240 --> 00:20:06,240
and to function normally and so that your emotions come up but that they're

226
00:20:06,240 --> 00:20:11,440
not so strong that you can't deal with them and you can't examine them and

227
00:20:11,440 --> 00:20:27,200
and come to understand them okay

