1
00:00:00,000 --> 00:00:07,000
Okay, the next question comes from German 1184.

2
00:00:07,000 --> 00:00:13,000
My question is how to overcome sexual urges being 100% celibate.

3
00:00:13,000 --> 00:00:17,000
And then in your own experiences, how successful are the months?

4
00:00:17,000 --> 00:00:19,000
Okay, first question.

5
00:00:19,000 --> 00:00:21,000
How to overcome sexual urges?

6
00:00:21,000 --> 00:00:29,000
Obviously the most difficult attachment to overcome for a human being.

7
00:00:29,000 --> 00:00:34,000
In general, I can accept that there are probably people for whom it's not the most difficult,

8
00:00:34,000 --> 00:00:37,000
but I would say for most people it is.

9
00:00:37,000 --> 00:00:44,000
And obviously the simple way to deal with it that we're all aware of is the repression of it.

10
00:00:44,000 --> 00:00:48,000
And that's how we would normally deal with that such things.

11
00:00:48,000 --> 00:00:50,000
Obviously this doesn't work.

12
00:00:50,000 --> 00:00:56,000
Obviously, I think it's readily apparent after some time that this doesn't do much.

13
00:00:56,000 --> 00:00:58,000
That's how we all start.

14
00:00:58,000 --> 00:01:02,000
We start by repressing the things which we want to be free from.

15
00:01:02,000 --> 00:01:07,000
We just refuse to acknowledge them and refuse to even look at them.

16
00:01:07,000 --> 00:01:13,000
And as a result, it's like pushing down on poison as gas and it starts to seep up through the cracks.

17
00:01:13,000 --> 00:01:24,000
And it can be really nasty at that point as we see with many religious figures who end up molesting small children and so on and so on.

18
00:01:24,000 --> 00:01:32,000
But if we really look at the Buddha's teaching and this took me quite a while to do,

19
00:01:32,000 --> 00:01:39,000
it's interesting how we can have a theoretical acceptance of teaching like the Buddha's teaching.

20
00:01:39,000 --> 00:01:45,000
And not really see how it relates to our practice or not be able to incorporate it into our practice.

21
00:01:45,000 --> 00:01:49,000
And it took me a long time to realize that that's exactly what the Buddha is talking about.

22
00:01:49,000 --> 00:01:54,000
But the Buddha taught something very profound that's called dependent origination.

23
00:01:54,000 --> 00:01:57,000
That's many people are familiar with.

24
00:01:57,000 --> 00:02:02,000
But to actually use it in your meditation practice is a whole other thing.

25
00:02:02,000 --> 00:02:06,000
And it's something that it's very important to explain and talk about.

26
00:02:06,000 --> 00:02:12,000
Dependent origination says that the arising of craving, it comes in a sequence.

27
00:02:12,000 --> 00:02:14,000
It doesn't just come up by itself.

28
00:02:14,000 --> 00:02:20,000
It comes from certain things. It starts with briefly speaking or where we can deal with.

29
00:02:20,000 --> 00:02:23,000
It starts with a contact between the eye and the object.

30
00:02:23,000 --> 00:02:25,000
So we can start there.

31
00:02:25,000 --> 00:02:30,000
If we can catch it there at the contact with the eye where we see something,

32
00:02:30,000 --> 00:02:32,000
then we can do away with the craving.

33
00:02:32,000 --> 00:02:35,000
That's one way to be free of it.

34
00:02:35,000 --> 00:02:42,000
The technique that I found most helpful is to go back and forth between the various parts of the attachment.

35
00:02:42,000 --> 00:02:45,000
First, there's the contact.

36
00:02:45,000 --> 00:02:49,000
Sometimes you'll be able to focus on seeing something that is enjoyable.

37
00:02:49,000 --> 00:02:54,000
In this case, seeing a woman or seeing something that is or even thinking.

38
00:02:54,000 --> 00:02:56,000
It comes up in the mind.

39
00:02:56,000 --> 00:02:59,000
The second thing is the feeling that it brings.

40
00:02:59,000 --> 00:03:01,000
It brings a pleasurable feeling.

41
00:03:01,000 --> 00:03:09,000
Even before you have some kind of lust or so on, there's the feeling of pleasure that arises.

42
00:03:09,000 --> 00:03:14,000
Here, we're not repressing this and we haven't yet come to any idea that this is wrong.

43
00:03:14,000 --> 00:03:20,000
The amazing thing that happens is once you accept the feelings that you have,

44
00:03:20,000 --> 00:03:23,000
you accept the pleasure that comes from the sexual urge.

45
00:03:23,000 --> 00:03:30,000
It comes from seeing something which you have a memory of being pleasurable.

46
00:03:30,000 --> 00:03:37,000
Going through the cycle of addiction again, once you accept that, it loses all of its power.

47
00:03:37,000 --> 00:03:42,000
When you say to yourself, happy, happy, pleasure, pleasure, or even lust,

48
00:03:42,000 --> 00:03:44,000
which is the third thing.

49
00:03:44,000 --> 00:03:50,000
Once there's the feeling, then there's the craving for wanting or lust, or whatever.

50
00:03:50,000 --> 00:03:55,000
Once you go back and forth and accept these things, you're seeing, seeing, or thinking, thinking,

51
00:03:55,000 --> 00:03:59,000
and then you have the feeling and then you have the liking of it.

52
00:03:59,000 --> 00:04:02,000
You go back and forth and you just watch them for what they are.

53
00:04:02,000 --> 00:04:07,000
It'd be amazed at what happens and it's not as though overnight it's gone,

54
00:04:07,000 --> 00:04:12,000
but it totally changes the playing field.

55
00:04:12,000 --> 00:04:17,000
It turned something that was an incredibly profound issue,

56
00:04:17,000 --> 00:04:22,000
something that makes it impossible for many people to become a monk,

57
00:04:22,000 --> 00:04:28,000
become something laughable, something that has no power over you.

58
00:04:28,000 --> 00:04:35,000
That's really it with all of our addictions and all of our versions as well.

59
00:04:35,000 --> 00:04:38,000
They only have power over us because we give them that power.

60
00:04:38,000 --> 00:04:43,000
Once we accept them for what they are and give them their audience and say, what are you?

61
00:04:43,000 --> 00:04:46,000
It turns out there's something very insignificant or very simple in any way.

62
00:04:46,000 --> 00:04:49,000
They are what they are and they don't mean anything.

63
00:04:49,000 --> 00:04:53,000
They don't have any significance other than the reality of what they are.

64
00:04:53,000 --> 00:04:58,000
As a result, you find the sexual urge disappears.

65
00:04:58,000 --> 00:05:02,000
That's in brief how you overcome the sexual urge.

66
00:05:02,000 --> 00:05:06,000
The other question, how successful are monks?

67
00:05:06,000 --> 00:05:09,000
I'm not really supposed to be talking.

68
00:05:09,000 --> 00:05:13,000
I wouldn't really be able to comment on how successful monks are.

69
00:05:13,000 --> 00:05:18,000
Obviously, if they're unsuccessful, they shouldn't be monks.

70
00:05:18,000 --> 00:05:24,000
But it's important to understand that many monks nowadays might not practice meditation

71
00:05:24,000 --> 00:05:27,000
to any great degree.

72
00:05:27,000 --> 00:05:30,000
They often become monks for the wrong reason.

73
00:05:30,000 --> 00:05:34,000
Also, nowadays it's hard to get in touch with a good teacher.

74
00:05:34,000 --> 00:05:37,000
Often people have good intentions and become a monk,

75
00:05:37,000 --> 00:05:40,000
but because of non-interaction with the qualified teacher,

76
00:05:40,000 --> 00:05:43,000
they find themselves struggling for a long time.

77
00:05:43,000 --> 00:05:47,000
They can often find themselves falling away from the monks life.

78
00:05:47,000 --> 00:05:52,000
It's obvious that most people are not going to be able to get all the way

79
00:05:52,000 --> 00:05:57,000
to reach Nibane in this life.

80
00:05:57,000 --> 00:06:02,000
But I would say, as far as being successful, unsuccessful,

81
00:06:02,000 --> 00:06:05,000
they're giving it a shot and they're working on it.

82
00:06:05,000 --> 00:06:08,000
Just like lay people who go to do meditation courses,

83
00:06:08,000 --> 00:06:13,000
they're able to keep it for a certain length of time.

84
00:06:13,000 --> 00:06:17,000
I would say it would be very wrong if a monk were unable to be celibate

85
00:06:17,000 --> 00:06:19,000
and yet stayed a monk.

86
00:06:19,000 --> 00:06:24,000
And I imagine it's easy to see that that sort of thing probably happens.

87
00:06:24,000 --> 00:06:28,000
But when they're unsuccessful and then they just row,

88
00:06:28,000 --> 00:06:30,000
I think this happens quite a bit.

89
00:06:30,000 --> 00:06:32,000
And in fact, it's always been the most scary thing.

90
00:06:32,000 --> 00:06:35,000
It's like you're out on the battlefield and you got in your hand

91
00:06:35,000 --> 00:06:37,000
and everybody's dying around you.

92
00:06:37,000 --> 00:06:39,000
It's a scary thing because you think,

93
00:06:39,000 --> 00:06:43,000
what am I going to be next?

94
00:06:43,000 --> 00:06:45,000
And that's sort of similar to being a monk.

95
00:06:45,000 --> 00:06:48,000
You see people just roping all around you.

96
00:06:48,000 --> 00:06:50,000
And I certainly have seen that.

97
00:06:50,000 --> 00:06:53,000
And so it's obviously an incredibly challenging thing.

98
00:06:53,000 --> 00:06:57,000
This isn't, we aren't pretending that this is something easy

99
00:06:57,000 --> 00:06:58,000
that we're doing.

100
00:06:58,000 --> 00:07:02,000
But I hope that some of what I've said helps both lay people

101
00:07:02,000 --> 00:07:08,000
and monks to find our way, to help us find our way to become

102
00:07:08,000 --> 00:07:11,000
free from this incredible attachment.

103
00:07:11,000 --> 00:07:14,000
And the upside is if you want to know,

104
00:07:14,000 --> 00:07:17,000
once you start to deal with these things,

105
00:07:17,000 --> 00:07:19,000
it really does bring peace, happiness,

106
00:07:19,000 --> 00:07:25,000
and an uplifting sense of freedom from this incredible burden,

107
00:07:25,000 --> 00:07:29,000
having to seek out things which obviously have no meaning

108
00:07:29,000 --> 00:07:32,000
and no inherent benefit.

109
00:07:32,000 --> 00:07:33,000
Okay, I hope that helps.

110
00:07:33,000 --> 00:07:38,000
Thanks for the question.

