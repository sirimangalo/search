1
00:00:00,000 --> 00:00:17,840
Today's talk, I thought I would talk about truth, because I think this is a fairly relevant

2
00:00:17,840 --> 00:00:26,160
topic for a place like Second Life, because we're dealing with a lot of illusion and

3
00:00:26,160 --> 00:00:35,400
potential falsehood. So I think it's an important subject and something for us all to think

4
00:00:35,400 --> 00:00:55,960
about. We come into a place like Second Life, and I think there are two related but different

5
00:00:55,960 --> 00:01:07,280
aspects of this place. And one is the illusion. One is illusion and the other is falsehood.

6
00:01:07,280 --> 00:01:21,080
So illusion can often be innocuous. It doesn't have to be an immoral thing. There's

7
00:01:21,080 --> 00:01:29,680
a lot of illusion here that you might say is in many ways positive. The illusion of

8
00:01:29,680 --> 00:01:34,680
being able to see each other, for instance, the illusion of us sitting here together.

9
00:01:34,680 --> 00:01:44,960
In many ways have a benefit. It can give us this sense of community and can really direct

10
00:01:44,960 --> 00:01:56,280
our attention towards the task at hand. It allows us to envision things and to often create

11
00:01:56,280 --> 00:02:03,800
and work out things in a way that might otherwise not be possible. Illusion is something

12
00:02:03,800 --> 00:02:10,600
which we have in our daily life. The illusion of solidity is all around us. And it's

13
00:02:10,600 --> 00:02:20,320
totally necessary if we didn't perceive things as an object, as what they were, if we didn't

14
00:02:20,320 --> 00:02:29,560
put things together as being an entity or that entity in terms of food or in terms

15
00:02:29,560 --> 00:02:44,720
of dangers, then we wouldn't be able to survive. So Buddhism is very much about truth but

16
00:02:44,720 --> 00:02:49,040
doesn't deny the importance of concepts, the importance of illusion, or the importance

17
00:02:49,040 --> 00:03:01,920
of the constructs. So in many ways second life is simply a construct. The problem of course

18
00:03:01,920 --> 00:03:07,400
with second life comes when we get into falsehoods. When we start to become something that

19
00:03:07,400 --> 00:03:13,960
we're not, when we hide things that we are, when we start running away from reality and

20
00:03:13,960 --> 00:03:23,440
trying to portray ourselves as something other than what we are. And I'd like to explore

21
00:03:23,440 --> 00:03:32,640
this topic a little bit. I think it's open to some debate but I think there are some moral

22
00:03:32,640 --> 00:03:39,960
issues here. And by moral I don't mean anything that's objectionable. I'm never talking

23
00:03:39,960 --> 00:03:44,400
about something that's objectionable to other people. I'm talking about something which causes

24
00:03:44,400 --> 00:03:52,480
suffering and mental upset for the individual who undertakes it. Things are immoral in Buddhism

25
00:03:52,480 --> 00:03:58,040
not because of what other people think of you or how other people judge you but they're

26
00:03:58,040 --> 00:04:02,960
immoral because of the suffering they cause for you. Because of how they warp your mind.

27
00:04:02,960 --> 00:04:09,800
Because of how they take you away from reality and take you away from the truth. So a lot

28
00:04:09,800 --> 00:04:17,000
of it seems innocuous it seems to be something that's just a game, just for fun. And it's

29
00:04:17,000 --> 00:04:23,520
an interesting thing about games that they are generally not innocuous. If you think

30
00:04:23,520 --> 00:04:29,560
back to when we used to play board games, when we used to play monopoly, I don't know

31
00:04:29,560 --> 00:04:36,560
if anyone still plays monopoly anymore. Probably we'd now play virtual monopoly or something

32
00:04:36,560 --> 00:04:41,520
like that. 3D monopoly. I don't know. No, I'm sure there are still people who play it.

33
00:04:41,520 --> 00:04:53,080
But back when we used to play monopoly we would get very competitive. It's certainly not

34
00:04:53,080 --> 00:04:58,640
innocuous. Sports, even more so. It becomes a bravado thing. I remember when we used to

35
00:04:58,640 --> 00:05:05,280
play sports and it was very macho and the adrenaline gets going and so on. When you see

36
00:05:05,280 --> 00:05:10,480
I'm from Canada so we have a big hockey tradition up there and it's certainly not just

37
00:05:10,480 --> 00:05:19,600
just a game. They beat each other and it's part of the game. So these are good examples

38
00:05:19,600 --> 00:05:24,480
that we should then bring over into something like second life. When we start looking at

39
00:05:24,480 --> 00:05:29,840
it as a game or when we use that as an excuse, I mean most of us I think don't see it

40
00:05:29,840 --> 00:05:38,320
simply as a game we see it as a life, as a part of our lives. But I think the in many ways

41
00:05:38,320 --> 00:05:57,840
we rationalize our falsehoods in this way. We use the game mentality or we evoke this. Oh it's

42
00:05:57,840 --> 00:06:06,080
just it's just for pretend. And again the problem here is that it can often take us away from

43
00:06:06,080 --> 00:06:16,560
what is real and it makes things easy. It allows us to escape many of our problems. So we have

44
00:06:16,560 --> 00:06:21,280
problems in real life. Well look at it on second life and none of those problems exist.

45
00:06:21,280 --> 00:06:35,200
And I think this is indicative of or not indicative. This exists in all parts of our discourse

46
00:06:35,200 --> 00:06:46,080
with our interaction with the computer. The computer is a tool that has become a means of getting

47
00:06:46,080 --> 00:06:57,760
very quick and very direct pleasure. We have this very easy gratification system. And I think

48
00:06:57,760 --> 00:07:04,320
it's exemplified by the iPhone or the iPad. Now I've used them a little bit but I'm not thinking

49
00:07:04,320 --> 00:07:09,360
of when I use them. I'm looking at how it's arisen and how the interface works. I mean you have

50
00:07:09,360 --> 00:07:16,000
these big buttons. It's like a toy. It's like a child's toy. I mean way back when Windows

51
00:07:16,000 --> 00:07:21,360
was so cool because it was so many. There were all these intricate parts to it. But now it's become

52
00:07:21,360 --> 00:07:30,400
push. Someone showed me recently an iPhone application with a cat and I'm sure some of you have

53
00:07:30,400 --> 00:07:37,920
seen this app if you're into the iPhone. It's a cat and what do you do? You speak and it speaks

54
00:07:37,920 --> 00:07:45,440
it back only in cat voice. And if you rub the cat, it laughs and if you push it, if you poke it, it

55
00:07:45,440 --> 00:07:59,680
groans. Computers have become an easy way around many of the difficulties in life. An easy way

56
00:07:59,680 --> 00:08:06,880
to forget about some of the real, very real problems. And what they do then is they create

57
00:08:06,880 --> 00:08:16,000
pleasure, stimulus and addiction. Because the real problem that comes with getting what you want

58
00:08:20,160 --> 00:08:26,960
is that then you are cultivating want. You're cultivating desire. Then whenever you don't get what

59
00:08:26,960 --> 00:08:32,560
you want or whenever it's not present, you have to seek it out. And you wanted it more and more

60
00:08:32,560 --> 00:08:37,600
and more. You're trying to get it again and again and again.

61
00:08:40,800 --> 00:08:50,880
And I think this relates directly to our creating of secondary personalities or becoming who we

62
00:08:50,880 --> 00:08:58,400
really would like to be here and trying to be something that we're not. And I'm not saying that

63
00:08:58,400 --> 00:09:04,480
this is indicative of everybody's life in second life. I think there's a lot of good that can be

64
00:09:04,480 --> 00:09:17,920
done here because you are freed from many of the unnecessary stigma and many of the problems

65
00:09:17,920 --> 00:09:23,920
that exist in real life. So you are able to get along a lot better and you are able to

66
00:09:23,920 --> 00:09:31,280
get closer to people in many ways. I'm only bringing this up. I think it's an important thing

67
00:09:31,280 --> 00:09:39,920
for us to look at because there is an aspect of our lives. That aspect of our existence here

68
00:09:41,360 --> 00:09:51,920
that somehow leads us to dualism leads us to leading a dual life where we get to the point where

69
00:09:51,920 --> 00:09:59,120
we have to make a choice. Are we going to be the same person as we are in real life or are we

70
00:09:59,120 --> 00:10:05,920
going to become a different person? And I would like to recommend that we don't separate these two.

71
00:10:05,920 --> 00:10:11,520
And why this really came to me is because looking around or walking around, I always get this

72
00:10:11,520 --> 00:10:18,320
sort of fear of talking to people or this hesitancy to really get in and talk to people because

73
00:10:18,320 --> 00:10:24,560
you don't really know who they are. They could be a man in real life and in a woman's body in

74
00:10:24,560 --> 00:10:34,080
this life. They could be old and in a young person's body or so on. And you run the risk of getting

75
00:10:34,080 --> 00:10:44,000
into some kind of fantasy relationship. And I found that I was really relieved and

76
00:10:44,000 --> 00:10:52,640
excited when I would read people's profiles telling me who they were in real life and saying,

77
00:10:53,200 --> 00:10:58,880
this is who they really were. And not presenting themselves here as something other than what

78
00:10:58,880 --> 00:11:06,240
they are in real life, but using second life as a means to extend their real life and to meet

79
00:11:06,240 --> 00:11:13,360
people that they wouldn't be able to meet in real life. Truth is a very important thing in Buddhism.

80
00:11:16,080 --> 00:11:22,320
And there's a reason for this because truth and wisdom, truth and understanding is what sets

81
00:11:22,320 --> 00:11:32,320
you free from suffering. The suffering and the problems in our life are very much like knots

82
00:11:32,320 --> 00:11:39,120
in a rope. The only problem with the rope is that it's tied up in knots. There's nothing wrong with

83
00:11:39,120 --> 00:11:49,680
the quality or the material. There's nothing wrong with the rope. You can take the most wonderful

84
00:11:49,680 --> 00:11:56,240
rope and when you tie it up into knots, it becomes useless. And our mind is the same. Our life

85
00:11:56,240 --> 00:12:05,200
is the same. We tie ourselves up in these knots and in these tangled webs that we weave. And this

86
00:12:05,200 --> 00:12:14,240
has an effect on our minds. This takes away from our purity and takes away from our peace and

87
00:12:14,240 --> 00:12:22,560
our happiness. And personally, I would much rather see the real thing that we are who we are and

88
00:12:22,560 --> 00:12:27,680
that we come to look at who we are. And we'll be careful that we don't run away from these things.

89
00:12:29,520 --> 00:12:36,000
I think this is important here for going to use second life as a tool to develop in the Buddhist

90
00:12:36,000 --> 00:12:52,400
practice. So truth is a lot like untying the knots or truth is like keeping yourself untying

91
00:12:52,400 --> 00:12:58,560
or keeping yourself from getting entangled. When you see things as they are, this is untying the

92
00:12:58,560 --> 00:13:06,160
knots of existence. Our knots, our ties are what keeps us attached to things, what keeps us attached

93
00:13:06,160 --> 00:13:11,440
to our addictions, what keeps us attached to our versions, our depressions, our fears and our worries.

94
00:13:11,440 --> 00:13:22,960
All of these things they come from untruth, they come from the lies that we tell ourselves

95
00:13:22,960 --> 00:13:28,560
and the lies that we tell other people when we believe things to be other than what they are,

96
00:13:29,600 --> 00:13:31,760
when we see things as being more than what they are.

97
00:13:36,080 --> 00:13:40,240
And so while there's nothing wrong with the illusion of second life, you know,

98
00:13:40,240 --> 00:13:44,880
take a second to look for yourself. What are you looking at here? What are you looking at? You're

99
00:13:44,880 --> 00:13:49,920
looking at a piece of glass or plastic and some brightly colored lights.

100
00:13:53,040 --> 00:14:00,560
These are just pixels that you're looking at. That's the reality of it. And in fact,

101
00:14:00,560 --> 00:14:06,800
it's not even out there, it's at the eye. What you're seeing is being processed on the skin of the eye

102
00:14:06,800 --> 00:14:11,760
or inside the eye, or even in the middle of the brain, because that's where it's being processed.

103
00:14:12,720 --> 00:14:15,680
We don't actually see at the eye we see in the middle of the brain somewhere.

104
00:14:21,120 --> 00:14:26,160
And there's nothing wrong with using these illusions to, okay, to settle ourselves down and verify

105
00:14:26,160 --> 00:14:32,160
for ourselves. Where are we? Here we are sitting together, listening to a meditation talk

106
00:14:32,160 --> 00:14:37,120
around the campfire in front of the Buddha, in a deer park. There's nothing wrong with that.

107
00:14:37,120 --> 00:14:43,120
That's very good preliminary. But if that becomes then your life, where you say, boy, I really

108
00:14:43,120 --> 00:14:48,080
enjoy being in this deer park and this really just makes my day, well then you've got a problem.

109
00:14:48,080 --> 00:14:53,280
You're living a falsehood. We use the illusions for a purpose.

110
00:14:55,680 --> 00:15:00,400
But in the end, the most important for us is going to be to see the truth.

111
00:15:00,400 --> 00:15:06,400
And to look and to see, well, why is it that I enjoy being in this deer park?

112
00:15:07,280 --> 00:15:14,320
Why is it that these pixels on this screen are able to create this, this pleasure in my brain?

113
00:15:15,040 --> 00:15:19,840
They're able to stimulate my brain and give rise to certain chemicals that then make me feel

114
00:15:19,840 --> 00:15:25,520
pleasure. And then we start getting into the truth. Then we start to penetrate the truth.

115
00:15:25,520 --> 00:15:33,040
So we don't have to deny the illusion and say, no, no, I'm not sitting in a deer park.

116
00:15:34,000 --> 00:15:37,840
But we have to go the next step and say, oh, what happens? What's happening here? What's

117
00:15:37,840 --> 00:15:45,680
really happening? And we have to look and we have to take it apart to be able to say to ourselves,

118
00:15:45,680 --> 00:15:53,040
this is what's real. This is the truth. But this reason it's very important that we don't

119
00:15:53,040 --> 00:16:00,320
get caught up in falsehood. We don't get caught up in the illusion.

120
00:16:04,560 --> 00:16:10,800
So the meditation practice is a tool that we use to see things as they are. It's a tool that

121
00:16:10,800 --> 00:16:16,560
we use to break away from the illusion and to see the illusion for what it is. This is illusion,

122
00:16:16,560 --> 00:16:23,920
this is real. The reality is this. Everything outside of that is illusion. It has its place,

123
00:16:23,920 --> 00:16:32,000
but it only goes so far. And the way we do this, as I've said again and again, is to remind ourselves

124
00:16:34,480 --> 00:16:39,200
this word that we have, we say mindfulness. The meaning of mindfulness is to remind yourself,

125
00:16:40,400 --> 00:16:45,680
you're reminding yourself of the reality of the situation, the reality of the experience. What's

126
00:16:45,680 --> 00:16:50,480
really happening here? Not what do I think is happening? What do I believe? What is my view? What is

127
00:16:50,480 --> 00:16:59,040
my religion? What is my way of looking at things? What's real? And when we started to do that,

128
00:16:59,040 --> 00:17:03,600
we'll see many things that we wouldn't otherwise be able to see. We'll see many things going on

129
00:17:03,600 --> 00:17:09,440
in our minds right here. You see, you're not actually sitting at this campfire. You're here in the

130
00:17:09,440 --> 00:17:18,720
room all alone, maybe you're lonely, maybe you're depressed, maybe you don't like what I'm saying

131
00:17:18,720 --> 00:17:24,880
and you're upset, maybe you're sitting on a hard chair and it starts to get uncomfortable,

132
00:17:24,880 --> 00:17:30,880
maybe you're sitting on a soft chair and your back starts to hurt, maybe it's too hot,

133
00:17:30,880 --> 00:17:40,560
maybe it's too cold, maybe you're thirsty, maybe you're hungry, maybe you're sick, maybe you're

134
00:17:40,560 --> 00:17:47,520
at peace with yourself. There could be many different things happening inside, happening in your

135
00:17:47,520 --> 00:17:54,000
experience. And so what we do in the meditation practice is we remind ourselves of what's happening,

136
00:17:54,000 --> 00:18:01,840
we remind ourselves of what these things are, of what's really going on. Suppose you'd like

137
00:18:01,840 --> 00:18:07,040
what I'm saying, then you say to yourself, like King, like King, and you just remind yourselves,

138
00:18:07,040 --> 00:18:18,640
oh no, I'm liking something. If you feel happy, say happy, if you don't like what I'm saying,

139
00:18:18,640 --> 00:18:26,160
you say, just like King, just like King, unhappy, unhappy, sad, sad, bored, bored.

140
00:18:29,120 --> 00:18:33,280
See, because I can't make you bored and I can't make you happy, you can only do that for yourself.

141
00:18:34,560 --> 00:18:39,280
You're not hearing me speak. You guys, you can't even hear the words I'm saying.

142
00:18:39,280 --> 00:18:48,560
What you hear is that electronic representation, or actually it's an analog representation

143
00:18:48,560 --> 00:18:55,040
of an digital representation of my analog transmission.

144
00:19:00,240 --> 00:19:05,360
And where it's occurring is that you're ear and where you're processing and it's in your mind.

145
00:19:05,360 --> 00:19:14,480
So how can I do anything to you? I can make you hear noise if I go, if I make a noise,

146
00:19:15,040 --> 00:19:22,320
if I clap my hands, I can make sure that you all heard that. If you were all paying attention,

147
00:19:22,320 --> 00:19:27,280
then most likely you all heard that. But I can't make you feel happy or unhappy about it.

148
00:19:27,280 --> 00:19:30,720
I can't say, I can't say something and everyone's going to like what I say.

149
00:19:30,720 --> 00:19:36,160
And I also can't say something to you and make you feel angry about it.

150
00:19:36,800 --> 00:19:39,280
I can't call you bad names and be sure that you're going to be upset,

151
00:19:40,400 --> 00:19:45,520
though if you're an ordinary person, I could probably, could probably do a good, good job at it.

152
00:19:46,960 --> 00:19:52,960
It's not in my power. The reason I can make you angry is because we're mostly,

153
00:19:52,960 --> 00:20:06,720
most of us prone to anger. Most of us don't like to be criticized and so on.

154
00:20:11,600 --> 00:20:15,520
But in the end, the reality of it is you're making yourself happy or unhappy.

155
00:20:17,360 --> 00:20:21,920
You're hearing what I say, you're processing it and you're saying, oh, that's nice or oh,

156
00:20:21,920 --> 00:20:27,760
that sucks. You're doing that to yourself. And we do that with this with everything.

157
00:20:28,320 --> 00:20:33,040
There's nothing intrinsically wonderful about sitting here staring at some pixels that

158
00:20:33,040 --> 00:20:42,560
happen to look like a deer and uh, uh, uh, mouswa, a tree and an ocean and a Buddha.

159
00:20:43,360 --> 00:20:49,760
There's nothing special about it. But we make it, we make it special. We say, oh, this is wonderful.

160
00:20:49,760 --> 00:20:57,200
We feel very happy. When someone yells at you and says nasty things, there's nothing nasty

161
00:20:57,200 --> 00:21:03,920
about the sound. The worst it can do is break your ear drums if it's really, really loud.

162
00:21:04,720 --> 00:21:06,400
And even that, there's nothing wrong with that.

163
00:21:09,040 --> 00:21:13,040
Until it gets in your mind, oh man, now I can't hear anymore. Make your drums requested.

164
00:21:13,040 --> 00:21:20,720
When you hear me say nasty things, you say, oh, what a jerk. What nasty things he says.

165
00:21:22,080 --> 00:21:23,040
You do it to yourself.

166
00:21:32,080 --> 00:21:40,560
So meditation is taking that power away, taking that reflex away from away from the equation.

167
00:21:40,560 --> 00:21:49,920
When we see something, instead of saying this is good, the one is this, two is three good,

168
00:21:49,920 --> 00:21:56,640
this is good. We turn it back on ourselves. When we say this is this,

169
00:21:58,800 --> 00:22:06,560
we're seeing is seen, seeing this is seeing this. We remind ourselves,

170
00:22:06,560 --> 00:22:12,960
read mind. We bring the mind back. We don't let the mind go forward. Seeing is what?

171
00:22:13,520 --> 00:22:21,200
Seeing, hearing is what? Hearing. So we say to ourselves, seeing, seeing. When we hear something,

172
00:22:21,200 --> 00:22:31,280
hearing, pain is pain. Anger is anger. Greed is greed. It is what it is.

173
00:22:31,280 --> 00:22:39,840
It can't escape that. The things in our mind, in our body, and in the world around us,

174
00:22:39,840 --> 00:22:50,000
they can't escape their real nature. It's the one thing we can be sure of,

175
00:22:51,680 --> 00:22:55,920
is that they are what they are. It's the one thing we can be sure of, and this is interesting

176
00:22:55,920 --> 00:23:01,200
in meditation, because in the many religious traditions, people claim to see this or see that.

177
00:23:03,200 --> 00:23:09,360
And scientists and atheists and skeptics in general are always like, what can you be sure

178
00:23:09,360 --> 00:23:15,840
that what you saw, that that's what you really saw? Can you be sure some of the I saw God?

179
00:23:16,800 --> 00:23:18,640
You say, well, how do you know it was God?

180
00:23:18,640 --> 00:23:28,880
I felt, you know, like I was in heaven, or these people say, I saw a bright light at the end of

181
00:23:28,880 --> 00:23:33,440
the tunnel. This guy says, that's because you have a tunnel vision, you morrow.

182
00:23:35,120 --> 00:23:38,000
Sorry. That's a quote. Those weren't my words.

183
00:23:42,720 --> 00:23:48,560
No, I actually do believe in this tunnel, seeing a tunnel and a bright light at the end,

184
00:23:48,560 --> 00:23:52,400
because there's a lot more to than just to it than just seeing a tunnel,

185
00:23:52,400 --> 00:23:55,760
and there's some very interesting clinical studies that have been done

186
00:23:57,040 --> 00:24:03,840
talking that have been documented, yes. It's a good example in the sense that

187
00:24:05,520 --> 00:24:09,840
you're interpreting that as being the path, the stairway to heaven, or something.

188
00:24:11,200 --> 00:24:14,640
You don't really know that. And interesting about that.

189
00:24:14,640 --> 00:24:20,640
But interesting about it is if you look at the in say, the Tibetan tradition from what I

190
00:24:20,640 --> 00:24:26,000
understand, and I don't know a lot about the Tibetan tradition, is that these bright lights

191
00:24:26,000 --> 00:24:32,960
are our negative. They're a bad thing. And that's very Buddhist. It's a really good

192
00:24:32,960 --> 00:24:38,720
important teaching. When you die, you're going to see bright lights. Don't chase after them.

193
00:24:38,720 --> 00:24:45,360
Don't go near them. Don't cling to them. Because those bright lights are your mother's womb,

194
00:24:46,560 --> 00:24:57,280
the next rebirth. Don't go into the light. So you see, it's very easy to interpret things

195
00:24:57,280 --> 00:25:03,040
in a certain way and say, this is good, this is bad. The reason they say that is you're going

196
00:25:03,040 --> 00:25:08,080
to cling to it. And you don't know what that light is. It could be a human womb. It could be a dog

197
00:25:08,080 --> 00:25:11,360
womb. It could be a deer womb. I don't know.

198
00:25:20,560 --> 00:25:26,960
That's a very good example in terms of interpreting things, the things that we experience in

199
00:25:26,960 --> 00:25:33,040
meditation or near-death experiences and so on. They are what they are. And this is how a Buddhist

200
00:25:33,040 --> 00:25:37,840
gets out of that. What is different about Buddhism? Buddhism has these experiences. We have these

201
00:25:37,840 --> 00:25:44,480
experiences of bliss and seeing bright lights and seeing angels and so on. How do we avoid

202
00:25:44,480 --> 00:25:52,320
this dilemma, the religious dilemma of proving that we see what we see?

203
00:25:54,560 --> 00:25:58,480
And I think I've already answered this and it should be clear that Buddhists don't go beyond the

204
00:25:58,480 --> 00:26:04,320
seeing. What you can confirm is that you're seeing something. No one can take that away from

205
00:26:04,320 --> 00:26:08,480
you and say, no, no, you didn't see anything. There was no experience of seeing there.

206
00:26:12,400 --> 00:26:17,440
And this is important because in science they will say that. They will say you didn't see anything.

207
00:26:17,440 --> 00:26:26,240
Why? Because your eyes aren't working. Your brain wasn't working. There are cases of people who

208
00:26:26,240 --> 00:26:34,640
went into cardiac arrest for minutes and had near-death experiences. Now this is impossible

209
00:26:34,640 --> 00:26:40,720
from scientific point of view because you're going to brain death after something like 20 seconds.

210
00:26:40,720 --> 00:26:50,640
There's zero brain activity. Zero. Something like that. I'm not a scientist and so I don't know.

211
00:26:50,640 --> 00:26:58,880
I'm not in Western terms anyway. But I'm reading the scientific studies and after about 20

212
00:26:58,880 --> 00:27:05,280
seconds, after about five seconds, your brain dead. You aren't able to think according to science,

213
00:27:05,280 --> 00:27:11,360
the brain isn't able to function. But after 20 seconds, there's no activity whatsoever.

214
00:27:11,360 --> 00:27:14,480
It's not only you're not able to think, but the brain is just not functioning.

215
00:27:15,520 --> 00:27:20,480
And yet people will have these experiences. And what makes them interesting is because people

216
00:27:20,480 --> 00:27:26,640
will will recount afterwards when they come back listening to the doctor's talk,

217
00:27:27,280 --> 00:27:34,560
watching the doctors do things, seeing the doctors operate on them. There's just one

218
00:27:35,520 --> 00:27:45,440
patient who passed away or was passing away and came back and claimed to see the surgeon flapping

219
00:27:45,440 --> 00:27:56,000
his arms like a chicken. Well, she was out. Now this doctor had a peculiar habit of flapping

220
00:27:56,000 --> 00:28:08,240
his hands like a chicken because he would call disinfect his hands before operating. And then he

221
00:28:08,240 --> 00:28:15,120
placed them on his chest and give instructions with his elbows because he was in order to make

222
00:28:15,120 --> 00:28:20,080
sure that he wouldn't touch anything. And so it looked like he was flapping himself like

223
00:28:20,080 --> 00:28:23,680
a chicken. So he was doing this well after she had gone into brain day.

224
00:28:29,440 --> 00:28:35,680
So there's a very strong case for the idea that seeing is seeing, that we can be sure when you

225
00:28:35,680 --> 00:28:40,560
see something that should be considered seeing. Whatever science wants to say that no, it's just

226
00:28:40,560 --> 00:28:46,800
an illusion. Well, an illusion of what or an illusion in what form. How can you say that it's

227
00:28:46,800 --> 00:28:51,360
not seeing when there's an experience of seeing? So this is sort of a roundabout, but it's

228
00:28:51,360 --> 00:28:58,640
talking, trying to explain what I'm talking about here in terms of reality, in terms of the

229
00:28:58,640 --> 00:29:04,720
truth. Truth is very important in Buddhism. And that's the lesson that I would like to

230
00:29:04,720 --> 00:29:10,960
impart today. I think I've gone for 30 minutes now. So I'm just going to stop there. If anyone

231
00:29:10,960 --> 00:29:17,200
has any questions, it can take a little bit of time. I'd also like to announce that I'm now taking

232
00:29:17,200 --> 00:29:27,920
questions over the internet. I have this new project going on on the internet on YouTube called

233
00:29:27,920 --> 00:29:36,400
Ask a Monk, where if you have any questions on Buddhism or meditation, you can log on to my

234
00:29:37,280 --> 00:29:42,400
YouTube channel. And that's, you know, give that here.

235
00:29:45,600 --> 00:29:50,800
And once you, if you, if you leave questions, I'll answer them in the form of videos. So that way

236
00:29:50,800 --> 00:29:56,240
other people can hear them as well. And if you want to go directly to the, the, the, the, the

237
00:29:56,240 --> 00:30:03,360
questions, they're, they're at the second link. But you can read, you can ask questions directly

238
00:30:03,360 --> 00:30:11,600
on my YouTube channel. So that's probably the best, the best, having you to go. Okay. So,

239
00:30:13,920 --> 00:30:21,200
sorry, I just got turned off my speech. So the second link there is to,

240
00:30:21,200 --> 00:30:27,120
if you want to go directly to the, the list of questions, but you can ask questions on my

241
00:30:27,120 --> 00:30:33,840
YouTube channel as well. So that's probably the best link to go. Okay. So that's all for the speech

242
00:30:33,840 --> 00:30:51,360
today.

