1
00:00:00,000 --> 00:00:26,620
Good evening, everyone.

2
00:00:26,620 --> 00:00:52,920
We're testing live merch nights.

3
00:00:52,920 --> 00:00:56,560
There we have a quote about the body.

4
00:00:56,560 --> 00:01:04,060
The bodily purity.

5
00:01:04,060 --> 00:01:10,260
How do you clean the body, huh?

6
00:01:10,260 --> 00:01:11,260
How do you clean the body?

7
00:01:11,260 --> 00:01:21,340
It's a loaded question from Buddhist point of view.

8
00:01:21,340 --> 00:01:26,360
We were talking on Monday about, we've just been reading

9
00:01:26,360 --> 00:01:46,960
chapters 20 to 25 of the Lotus sutra and in one of those chapters, there's a monk,

10
00:01:46,960 --> 00:02:02,160
a man, somebody who lights their body on fire as an offering to the Buddha, and burns

11
00:02:02,160 --> 00:02:16,840
themselves completely, and then there's another person who burns their forearms off

12
00:02:16,840 --> 00:02:28,280
their forearms completely, and then make some kind of a vow, and they come back, it's

13
00:02:28,280 --> 00:02:29,280
good.

14
00:02:29,280 --> 00:02:40,280
Honestly, I have less and less good to say about this Lotus sutra the more I read it.

15
00:02:40,280 --> 00:02:46,120
Got an interesting conversation going about the nature of the body according to Buddhism.

16
00:02:46,120 --> 00:02:59,880
Because a common thought or a train of thought for spiritual people, especially in the

17
00:02:59,880 --> 00:03:04,080
west, is that the body is somehow sacred.

18
00:03:04,080 --> 00:03:19,880
It should work to purify the body, to guard the body as a vessel, and so the idea that

19
00:03:19,880 --> 00:03:25,960
spiritual beings would use part of their body as an offering, it's actually common in

20
00:03:25,960 --> 00:03:33,960
Mahayana, even today, to offer a finger, and you just stick a finger into a candle and

21
00:03:33,960 --> 00:03:40,880
burn it up, and it's an offering to the Buddha, so we are going whether it is an offering

22
00:03:40,880 --> 00:03:44,760
because you're not actually giving anybody anything to anybody, nobody's benefiting

23
00:03:44,760 --> 00:03:54,800
from your burning of a finger, or whether it's just a measure of your determination,

24
00:03:54,800 --> 00:04:06,800
it's a measure of devotion, but the idea that you should do this, whether or not Buddhists

25
00:04:06,800 --> 00:04:15,440
like me would agree with, that we're doing that, we at least can agree that you're using

26
00:04:15,440 --> 00:04:25,760
your body in some way to benefit someone else, or using your body in any way really is

27
00:04:25,760 --> 00:04:28,400
a lot like using any other tool.

28
00:04:28,400 --> 00:04:34,600
We don't see the body as somehow special, and it's categorically different from other

29
00:04:34,600 --> 00:04:44,080
things because there's experiences surrounding it, direct experience, experience of pain

30
00:04:44,080 --> 00:04:56,640
and pleasure, but as an entity, the body is something you can give away, organ donation,

31
00:04:56,640 --> 00:05:02,360
blood donation, these sorts of things I think are very much in line with Buddhism.

32
00:05:02,360 --> 00:05:09,680
We talked about this in Thailand, how it's a giving blood, it's a great thing, all the

33
00:05:09,680 --> 00:05:15,280
monks and nuns I went with and we went together to the hospital to give blood because we considered

34
00:05:15,280 --> 00:05:27,080
that to be a great thing that we could do, whether it's right or wrong for monks and

35
00:05:27,080 --> 00:05:35,680
nuns to give blood I'm not sure, but we all did it, but no, the idea is that the body

36
00:05:35,680 --> 00:05:46,920
is just another thing, and more to the point, it's inherently impure, there's nothing pure

37
00:05:46,920 --> 00:05:54,200
about the body, if you want to get technical, it's made up of the four elements and so

38
00:05:54,200 --> 00:06:04,400
there's nothing disgusting about it, technically, but that being said, it's made up of

39
00:06:04,400 --> 00:06:17,360
all the numbers of things that are, you know, at the very least without the ability to

40
00:06:17,360 --> 00:06:26,400
provide satisfaction, but that in general are considered repulsive, and you're going to

41
00:06:26,400 --> 00:06:32,400
clean up the body when you have to get rid of all the blood and pus and snot and bio

42
00:06:32,400 --> 00:06:49,480
and urine and feces, you know, conventional sense, the body is terribly impure, but in another

43
00:06:49,480 --> 00:06:58,560
sense, in the sense that the Buddha is talking here, it is possible to purify the body,

44
00:06:58,560 --> 00:07:04,520
it's totally not what most people would think, it's a completely different way of looking

45
00:07:04,520 --> 00:07:18,680
at purity and looking at the body, so how do you purify the body, stop killing, right?

46
00:07:18,680 --> 00:07:28,840
I have nothing to do with the nature of the thing, purity and impurity in regards to the

47
00:07:28,840 --> 00:07:39,000
physical realm, have everything to do with the use, don't purify the body, don't use it

48
00:07:39,000 --> 00:07:48,040
to kill, don't use it to steal, don't use it to commit sexual misconduct to commit adultery,

49
00:07:48,040 --> 00:08:04,240
don't commit immoral acts of body, purity for the material realm is in the use you give,

50
00:08:04,240 --> 00:08:09,360
so this goes not only for the body, but it goes for all other things we use, the body

51
00:08:09,360 --> 00:08:15,240
is in this way not categorically different, it's just another thing that we use, if you

52
00:08:15,240 --> 00:08:24,120
want to talk about purity of your possessions and purity of robes, are my robes pure,

53
00:08:24,120 --> 00:08:30,920
they're 100% cotton as far as I know, but that's not what makes them pure, they're pure

54
00:08:30,920 --> 00:08:44,560
if I use them for pure purposes, if I use them for play or for fun or if I enjoy them

55
00:08:44,560 --> 00:08:51,400
or if I find robes that are only robes that are beautiful, pick my robes based on how

56
00:08:51,400 --> 00:09:00,400
beautiful and how comfortable and how pleasing they are, then that's impure, no matter

57
00:09:00,400 --> 00:09:10,600
what they're made of, if I use my food, the food that I get, if I use it for enjoyment

58
00:09:10,600 --> 00:09:24,480
or for intoxication or for fattening up, I use it for excess, it's impure food, that's

59
00:09:24,480 --> 00:09:31,320
what makes food impure, it's not because it's got this ingredient or that ingredient,

60
00:09:31,320 --> 00:09:38,920
a shelter, shelter is pure because of how you use it, to use it to hide your evil deeds

61
00:09:38,920 --> 00:09:48,720
or do you use it for seclusion, to cultivate meditation, medicine, do you use it to heal

62
00:09:48,720 --> 00:09:57,800
the body, or do you use it to become addicted to painkillers or to avoid the problems

63
00:09:57,800 --> 00:10:08,840
to avoid suffering, and by extension everything else that we use, including the body,

64
00:10:08,840 --> 00:10:13,520
so what are you using the body for?

65
00:10:13,520 --> 00:10:21,160
You have this body as a tool or you're using it to do evil things, or are you using it

66
00:10:21,160 --> 00:10:31,400
to gain purity, and using it as a tool, as a vehicle, and doing walking meditation,

67
00:10:31,400 --> 00:10:41,320
doing sitting meditation, are you using it for the right purposes, then it's pure, then

68
00:10:41,320 --> 00:11:01,280
the body is pure, so simple dhamma for tonight, and sorry I haven't been all that, I

69
00:11:01,280 --> 00:11:08,280
really want to do these sessions, and do them often, and try to get back into the video,

70
00:11:08,280 --> 00:11:14,680
but this month is going to be really, really busy, I made a mistake of taking two upper

71
00:11:14,680 --> 00:11:22,440
year courses, even though I'm only taking three courses, two of them got two essays,

72
00:11:22,440 --> 00:11:40,840
two papers to write, the piece symposium we're organizing, so hopefully I'll still have

73
00:11:40,840 --> 00:11:46,680
time to do all this, but the next week is going to be pretty harsh, you have to buckle

74
00:11:46,680 --> 00:11:55,400
down and do some work, especially because I missed the week, anyway, none of you are

75
00:11:55,400 --> 00:12:04,680
concerned, I'll try and be here, as much as possible, and we'll do some more dhamma

76
00:12:04,680 --> 00:12:17,160
now, but we'll see how it goes, anyway, thank you all for tuning, oh wait, next part, I'm

77
00:12:17,160 --> 00:12:26,360
going to post the hangout, so anybody who wants to come on the hangout can, you got

78
00:12:26,360 --> 00:12:31,680
news tonight, Robin just finished her course, she's here, then you know Robin's,

79
00:12:31,680 --> 00:12:41,680
you know Robin's letter out of her room this, today, isn't she look happy, this is,

80
00:12:41,680 --> 00:12:47,680
she's still never taking, so I don't want to bother, maybe we'll have a chance to talk

81
00:12:47,680 --> 00:12:59,880
before she leaves, so come on, if you got questions, come on the hangout, which is

82
00:12:59,880 --> 00:13:06,880
posted at meditation.ceremungalove.org, we've got 10 viewers on YouTube, this small group,

83
00:13:06,880 --> 00:13:17,880
I'm not a meditator, it's just nice, we're in a long list of meditators for me, alright,

84
00:13:17,880 --> 00:13:35,880
well, if no one's coming on, I'm going to go, I'll be back tomorrow, we'll get

85
00:13:35,880 --> 00:13:52,880
ready, everybody.

