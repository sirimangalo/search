1
00:00:00,000 --> 00:00:06,500
Namato Ratanata Yasai.

2
00:00:06,500 --> 00:00:08,700
Welcome everyone.

3
00:00:08,700 --> 00:00:16,440
Today we're broadcasting here live from what Thai, Los Angeles broadcasting over the

4
00:00:16,440 --> 00:00:24,440
internet via my web blog.

5
00:00:24,440 --> 00:00:34,240
And today I will be starting again to give talks at 1pm, so this is our first 1pm talk this

6
00:00:34,240 --> 00:00:37,640
week.

7
00:00:37,640 --> 00:00:45,400
Today's talk I thought I would talk about Dharma or Dharma, the teaching of the Buddha in

8
00:00:45,400 --> 00:01:02,800
the working world or how it relates to as it relates to work and so this is a topic which

9
00:01:02,800 --> 00:01:12,200
we can deal with on various levels and mainly we have the talking about work in terms

10
00:01:12,200 --> 00:01:17,240
of working in the world and then we have work in terms of working in the Dhamma, working

11
00:01:17,240 --> 00:01:25,600
in the Buddhist religion, for instance say working as a monk or working as a nun.

12
00:01:25,600 --> 00:01:33,680
And I do see quite often that people try to separate these two out and I think this is one

13
00:01:33,680 --> 00:01:47,240
way of approaching the topic of work, how the Buddha Dhamma relates to work is to separate

14
00:01:47,240 --> 00:01:55,560
out one's working life and one's Dharma practice.

15
00:01:55,560 --> 00:02:05,000
And another way of going about it is to try and bring one's life so that there is no

16
00:02:05,000 --> 00:02:12,440
difference between work and Dharma or work in Dharma, work in practice.

17
00:02:12,440 --> 00:02:17,480
And so I'd like to sort of try to deal with these different ways of looking at how

18
00:02:17,480 --> 00:02:30,240
to live our lives in the world or how to make our Dharma practice fit with our work life.

19
00:02:30,240 --> 00:02:33,400
And I think this is a question for people who aren't Buddhist as well, it's a question

20
00:02:33,400 --> 00:02:38,240
for all people how to make their public and their private life or their work and their

21
00:02:38,240 --> 00:02:43,880
home life mesh together or how to juggle the two.

22
00:02:43,880 --> 00:02:48,120
So I think it's an interesting topic to discuss.

23
00:02:48,120 --> 00:02:53,640
First when we talk about anything in Buddhism we have to look at it in terms of the

24
00:02:53,640 --> 00:03:01,520
8 fold noble path or the three trainings which form the core of Buddhism.

25
00:03:01,520 --> 00:03:16,520
So how we often answer the question of how to fit one's livelihood into Dharma practice

26
00:03:16,520 --> 00:03:25,000
or the Buddha's teaching, we often answer it by referring to morality which is the first part

27
00:03:25,000 --> 00:03:29,240
of the path of the Buddha.

28
00:03:29,240 --> 00:03:35,320
So we will talk about the various ways that livelihood is incorrect or incompatible

29
00:03:35,320 --> 00:03:39,120
with Buddhism.

30
00:03:39,120 --> 00:03:46,120
And often we never go beyond this, but so well it's important to start here, start

31
00:03:46,120 --> 00:03:52,360
with this as our basis, as in all things, start with morality as the base, it's important

32
00:03:52,360 --> 00:03:54,760
that we then look beyond this.

33
00:03:54,760 --> 00:03:58,680
And so I'd like to say that I'm going to start here and explain this, but this certainly

34
00:03:58,680 --> 00:04:00,680
isn't where we want to stop.

35
00:04:00,680 --> 00:04:06,120
I think it's a shame that often this is the only aspect of livelihood that ever gets

36
00:04:06,120 --> 00:04:11,200
discussed is that these things that you shouldn't do like as though if you didn't do this

37
00:04:11,200 --> 00:04:16,080
in this and this then you would be somehow poof you would be a good Buddhist or you

38
00:04:16,080 --> 00:04:21,360
would be guaranteed to progress on the path.

39
00:04:21,360 --> 00:04:27,720
The basis of right livelihood is morality because livelihood of course is an external

40
00:04:27,720 --> 00:04:34,160
thing, it's generally a physical and a verbal undertaking where we undertake certain

41
00:04:34,160 --> 00:04:41,920
acts or certain speech for the purpose of feeding ourselves or the purpose of providing

42
00:04:41,920 --> 00:04:45,720
our necessities.

43
00:04:45,720 --> 00:04:56,480
And so as the very basis, in all things, it's the very basis of our entrance into Buddhism.

44
00:04:56,480 --> 00:05:00,360
There are certain things which we're going to have to cut off.

45
00:05:00,360 --> 00:05:05,400
This is for, in the case for instance, of people who had never practiced Buddhism before.

46
00:05:05,400 --> 00:05:06,720
Where do you start?

47
00:05:06,720 --> 00:05:08,880
You want to start taking up meditation?

48
00:05:08,880 --> 00:05:14,040
Well, what are the first things that you have to adjust in order that your meditation

49
00:05:14,040 --> 00:05:15,040
will progress?

50
00:05:15,040 --> 00:05:19,120
And of course, morality is the first thing that we have to adjust.

51
00:05:19,120 --> 00:05:26,360
There are certain things, actions in certain speech that we have to give up.

52
00:05:26,360 --> 00:05:34,120
And in livelihood, this is doubly so because livelihood is sort of the one thing that we

53
00:05:34,120 --> 00:05:37,840
have to carry out and continue in our lives.

54
00:05:37,840 --> 00:05:44,680
We may not have the opportunity to kill or to steal for other reasons, but these can

55
00:05:44,680 --> 00:05:54,120
become very obvious or frequent in the case of livelihood.

56
00:05:54,120 --> 00:06:03,280
So for people who are hunters or are butchers or are thieves, for instance, people who

57
00:06:03,280 --> 00:06:18,480
are prostitutes, even sales people who lie or use deception to gain their livelihood.

58
00:06:18,480 --> 00:06:25,600
All of these things are really worth noting, whereas we normally just talk about morality

59
00:06:25,600 --> 00:06:28,760
in terms of the things in general that we shouldn't do.

60
00:06:28,760 --> 00:06:36,600
It's worth noting that our livelihood is a place where we're going to really have to examine

61
00:06:36,600 --> 00:06:49,840
and see where and look at it as an obvious place for improvement because it's a

62
00:06:49,840 --> 00:06:56,920
place, it's a sphere of our life that's going to lead us to do and to say bad things

63
00:06:56,920 --> 00:07:00,520
on a regular basis, potentially.

64
00:07:00,520 --> 00:07:06,200
So in order to sell our product or in order to carry out our lives because it's something

65
00:07:06,200 --> 00:07:19,360
we do repetitively, it's very common that we're repetitively breaking the precepts, so

66
00:07:19,360 --> 00:07:27,760
if it's not killing or stealing, then it's lying or cheating and so on, saying bad things

67
00:07:27,760 --> 00:07:32,840
using speech in an improper way on a regular basis.

68
00:07:32,840 --> 00:07:36,120
And this is the basis of morality, a basis of right livelihood.

69
00:07:36,120 --> 00:07:40,000
So when people ask, what's a good job for me?

70
00:07:40,000 --> 00:07:45,880
Is this a good job or how can I make a living and still practice the Dharma or practice

71
00:07:45,880 --> 00:07:47,880
the Buddha's teaching?

72
00:07:47,880 --> 00:07:55,760
And this is the first answer, is that as long as we're able to keep to the five precepts

73
00:07:55,760 --> 00:08:01,080
and our livelihood doesn't lead us to do bad, do things which are immoral or say things

74
00:08:01,080 --> 00:08:06,400
which are immoral, then it's really not against Dharma, against practice of the Buddha's

75
00:08:06,400 --> 00:08:08,360
teaching.

76
00:08:08,360 --> 00:08:17,560
So for instance, we work in an office and we're a secretary or we are a office manager or

77
00:08:17,560 --> 00:08:25,840
something that really has no moral consequences.

78
00:08:25,840 --> 00:08:29,480
Then we can say, well, this is a livelihood which is pure.

79
00:08:29,480 --> 00:08:34,760
This is a livelihood which is not going to be a hindrance in our practice of the Buddhist

80
00:08:34,760 --> 00:08:37,840
teaching on the level of morality.

81
00:08:37,840 --> 00:08:45,880
And so as I say, often we stop there in terms of livelihood, but I really think this is

82
00:08:45,880 --> 00:08:56,600
some one area that deserves much deeper attention because in the end livelihood is really

83
00:08:56,600 --> 00:09:02,840
how just about living our lives and even monks have a livelihood of sorts.

84
00:09:02,840 --> 00:09:07,080
But it's clear that even someone who in the world is carrying out their livelihood in

85
00:09:07,080 --> 00:09:12,920
the moral ethical, quote unquote, moral ethical way, is still carrying out a very different

86
00:09:12,920 --> 00:09:16,040
sort of livelihood than say a monk or a nun.

87
00:09:16,040 --> 00:09:25,120
So the difference here is worth looking at and worth coming to understand, coming to discuss

88
00:09:25,120 --> 00:09:30,240
and to what we're thinking about.

89
00:09:30,240 --> 00:09:34,920
And the difference here is not so much on the level of morality, but it comes to be on

90
00:09:34,920 --> 00:09:38,560
the level of concentration and on the level of wisdom.

91
00:09:38,560 --> 00:09:41,160
So we have these other two parts of the Buddha's teaching.

92
00:09:41,160 --> 00:09:49,520
Lively in this sense, we can see it isn't just an issue of morality that when we work

93
00:09:49,520 --> 00:09:57,120
in the world, what we're dealing with is a real and clear disruption of our concentration,

94
00:09:57,120 --> 00:10:05,840
of our level of mental, quietude and mental calm, that we're stressed out, even with

95
00:10:05,840 --> 00:10:10,160
things which are quote unquote, moral.

96
00:10:10,160 --> 00:10:16,840
And why I say quote unquote is because really it's a kind of immorality in the mind,

97
00:10:16,840 --> 00:10:22,240
and it's going on in the mind, the mind is still giving rise to greed and to anger and

98
00:10:22,240 --> 00:10:25,920
to stress and to worry and to depression.

99
00:10:25,920 --> 00:10:28,120
All of these negative minds states are still arising.

100
00:10:28,120 --> 00:10:35,440
This is a normal part of a moral ethical lifestyle on the lay level.

101
00:10:35,440 --> 00:10:39,720
So when we're talking about livelihood as being moral as being perfectly right and a part

102
00:10:39,720 --> 00:10:45,720
of the Buddha's teaching, it really on a deeper level isn't so.

103
00:10:45,720 --> 00:10:51,280
And this is where people start to realize that they have to make a decision or they have

104
00:10:51,280 --> 00:11:06,720
to find some way to compromise or to come to terms with this seeming dichotomy or this

105
00:11:06,720 --> 00:11:07,720
split.

106
00:11:07,720 --> 00:11:14,960
There's the worldly life where you have to eat, you have to provide for your necessities

107
00:11:14,960 --> 00:11:19,200
and then there's the dumb mothers, the practice of meditation and sometimes these two

108
00:11:19,200 --> 00:11:23,120
can seem very incompatible, you know, you work nine to five and at five o'clock you get

109
00:11:23,120 --> 00:11:27,720
home and you're trying to meditate, but you're so stressed out that it can be very difficult

110
00:11:27,720 --> 00:11:34,000
to meet with any progress on the path and it just feels like you're taking three steps

111
00:11:34,000 --> 00:11:37,720
forward, two steps back, if that.

112
00:11:37,720 --> 00:11:43,440
So this is where people will often have to make a decision and I think it's, I people

113
00:11:43,440 --> 00:11:52,160
are, we're often too quick to say there must be a way to, to say have your cake and

114
00:11:52,160 --> 00:11:59,400
eat it too in this sense to stay as an ordinary person working in an office and still

115
00:11:59,400 --> 00:12:04,280
expect to find real progress on the path.

116
00:12:04,280 --> 00:12:11,120
I think more realistically people come to the realization that due to their situation they

117
00:12:11,120 --> 00:12:17,200
may have debt, so they may have responsibilities that they aren't able to ordain.

118
00:12:17,200 --> 00:12:23,760
They aren't able to devote their lives to the practice of the Buddha's teaching and

119
00:12:23,760 --> 00:12:25,160
so they stay as lay people.

120
00:12:25,160 --> 00:12:30,720
But I think there's a lot of people also who don't realize the great benefit or this

121
00:12:30,720 --> 00:12:37,680
great path which exists and this is the renunciation of the home life.

122
00:12:37,680 --> 00:12:48,560
People might often think that it's just a luxury that a very few people have or it's something

123
00:12:48,560 --> 00:12:54,760
that is taking advantage of other people or so on.

124
00:12:54,760 --> 00:13:04,560
The life of say a monk or a nun is in its ideal form, it's the giving up of all of

125
00:13:04,560 --> 00:13:08,200
our luxuries.

126
00:13:08,200 --> 00:13:14,880
The reason we're able to do away with this nine to five work ethic and we're able to

127
00:13:14,880 --> 00:13:23,000
be free from this need to stress and need to bother is because we've given up everything.

128
00:13:23,000 --> 00:13:30,280
We've given up money, we've given up possessions, we've given up all attachments in the

129
00:13:30,280 --> 00:13:37,240
home life, that we aren't able to seek out the entertainments that other people have.

130
00:13:37,240 --> 00:13:41,240
We aren't able to seek out the luxuries that other people have.

131
00:13:41,240 --> 00:13:44,120
Our food intake is one meal a day.

132
00:13:44,120 --> 00:13:52,280
We have all of these rules, our clothes, our rags, our cloth, simple cloth and everything

133
00:13:52,280 --> 00:13:53,880
else that we use.

134
00:13:53,880 --> 00:13:59,160
It's only for the purpose of spreading and teaching and helping other people so we're

135
00:13:59,160 --> 00:14:06,960
not allowed to even say have a computer for the purpose of surfing the net, for instance.

136
00:14:06,960 --> 00:14:15,600
It has to be for the purpose of spreading the Buddha's teaching.

137
00:14:15,600 --> 00:14:28,280
We take on a life of great discipline and it's a great work and it's a livelihood that

138
00:14:28,280 --> 00:14:33,880
is undertaken and as a result we're able to do away with this.

139
00:14:33,880 --> 00:14:47,840
There's this stress and this immorality in the mind or this distraction which is inherent

140
00:14:47,840 --> 00:14:53,920
in the lay life, in the life of trying to make a living in the world that is done away

141
00:14:53,920 --> 00:14:59,520
with.

142
00:14:59,520 --> 00:15:02,640
At the level of concentration we can't just come to say that my job is hurting anyone.

143
00:15:02,640 --> 00:15:13,520
Often here is where people will try to find a livelihood in the world that is also in

144
00:15:13,520 --> 00:15:27,160
line with the Dharma.

145
00:15:27,160 --> 00:15:30,200
Use it as a livelihood.

146
00:15:30,200 --> 00:15:36,280
Some people will be meditation teachers and receive donations, monetary donations or even

147
00:15:36,280 --> 00:15:39,160
charge for their meditation services.

148
00:15:39,160 --> 00:15:45,080
Some people go one step down and we'll do things which are generally useful.

149
00:15:45,080 --> 00:15:50,920
People wanting to become doctors or nurses.

150
00:15:50,920 --> 00:15:57,960
Even people who teach something similar to meditation, maybe psychology or social workers

151
00:15:57,960 --> 00:16:03,680
or motivational speakers or so on, thinking that this is something which is generally

152
00:16:03,680 --> 00:16:10,440
useful for people and so it's somehow in line with the Dharma.

153
00:16:10,440 --> 00:16:16,720
I think there's some merit here for some people to find this sort of a livelihood.

154
00:16:16,720 --> 00:16:23,200
I think it can be a way of compromising or coming to a compromise where you don't have

155
00:16:23,200 --> 00:16:31,000
to become a monk or a nun and really give up this luxury of being able to buy things,

156
00:16:31,000 --> 00:16:39,440
being able to have the things and being able to get whatever you want when you want it.

157
00:16:39,440 --> 00:16:45,960
But still being able to practice the Dharma or being in line with the Buddha's teaching.

158
00:16:45,960 --> 00:16:53,600
But the problem is you're always going to run into this sort of greed or this necessity

159
00:16:53,600 --> 00:17:02,040
to beg almost or to hint or to insinuate until you're able to give up, see, because being

160
00:17:02,040 --> 00:17:07,000
a monk, you're at this, you've taken it to the extreme where if you don't get food,

161
00:17:07,000 --> 00:17:13,760
you don't eat and it's really just not possible if you're still living in the world because

162
00:17:13,760 --> 00:17:18,440
you have rent, you have a car, you have all these overhead expenses.

163
00:17:18,440 --> 00:17:24,200
The idea behind becoming a monk or a nun is doing away with all overhead expenses.

164
00:17:24,200 --> 00:17:32,560
So you don't own anything, you don't have any debts, you live wherever, there's a place

165
00:17:32,560 --> 00:17:34,400
to stay.

166
00:17:34,400 --> 00:17:38,960
So in a place that people have created for you, you live in a meditation center which

167
00:17:38,960 --> 00:17:44,120
was solely for the purpose of people to come to meditate, for people to come and meditate.

168
00:17:44,120 --> 00:17:49,840
You couldn't say go out and have your own private house or private dwelling where you

169
00:17:49,840 --> 00:17:57,080
could just stay and live because this is overhead, then you need to pay the rent and

170
00:17:57,080 --> 00:18:00,680
you need to pay the utilities and so on.

171
00:18:00,680 --> 00:18:07,400
Becoming a monk or a nun is, if necessary, you live on the side of the street, you live

172
00:18:07,400 --> 00:18:10,800
in a park or you live in a forest or so on.

173
00:18:10,800 --> 00:18:16,600
We run into trouble in places like America, so we have to kind of fudge it a little bit.

174
00:18:16,600 --> 00:18:22,800
But the same principle remains that staying, you're staying in the meditation center,

175
00:18:22,800 --> 00:18:28,160
you're staying in the monastery where that people have created for the purpose of spreading

176
00:18:28,160 --> 00:18:37,200
Buddhism, you don't have your own house, it's a public place, it's a place where for the

177
00:18:37,200 --> 00:18:44,680
purpose and for the benefit of all people, sort of like the equivalent of a lay meditation

178
00:18:44,680 --> 00:18:52,000
teacher having to live in the meditation center and not having their own private house

179
00:18:52,000 --> 00:18:57,880
or private home.

180
00:18:57,880 --> 00:19:07,160
And so as a monk or a nun, if you don't have support and if it comes to the point

181
00:19:07,160 --> 00:19:15,320
where it seems like there's not going to be even enough food donated or any of your

182
00:19:15,320 --> 00:19:20,400
requisites, then you simply do without and this is a frequent, you know, it's likely being

183
00:19:20,400 --> 00:19:26,400
a homeless person if anyone who's ever been in that position and they can understand

184
00:19:26,400 --> 00:19:33,440
how it is for anyone who's never been in that position, it can be quite a difficult situation

185
00:19:33,440 --> 00:19:39,640
but it's quite different from, say, trying to live an ordinary life as, say, a dharma

186
00:19:39,640 --> 00:19:46,400
teacher or one who is teaching or helping to spread the Buddhist teaching because you always

187
00:19:46,400 --> 00:19:56,320
have to insinuate or hint at the very least if not outright begging or asking or charging

188
00:19:56,320 --> 00:20:00,200
for your services.

189
00:20:00,200 --> 00:20:05,520
And this is again something which is going to bother one's concentration, it creates feelings

190
00:20:05,520 --> 00:20:11,960
of guilt and it can create feelings of stress and anxiety, always wondering or worrying

191
00:20:11,960 --> 00:20:18,560
or whether you're going to get enough funds to pay for your necessities and so on.

192
00:20:18,560 --> 00:20:25,320
So what we're talking about here is as being a monk or nun is going the next step where

193
00:20:25,320 --> 00:20:37,800
we give up, even to that extent we give up all of our necessities, all of our requirements,

194
00:20:37,800 --> 00:20:45,240
all of our attachments to the home life and we become homeless.

195
00:20:45,240 --> 00:20:51,360
So in terms of concentration we can see that if we're really going to gain a very strong

196
00:20:51,360 --> 00:20:59,520
state of focus and concentration and we have to start to change many more things about

197
00:20:59,520 --> 00:21:02,600
who we are and how we live our lives.

198
00:21:02,600 --> 00:21:12,000
Many people can do this by finding a simple life and maybe a life which is in line with

199
00:21:12,000 --> 00:21:22,400
the dharma but in the end it's much more complicated or complex or subtle than simply saying

200
00:21:22,400 --> 00:21:26,800
I won't make my livelihood in this way or in that way and there are many people who think

201
00:21:26,800 --> 00:21:35,600
that they can do this or that job and still be able to help people or do something good

202
00:21:35,600 --> 00:21:38,720
and beneficial for other people.

203
00:21:38,720 --> 00:21:43,640
On a moral level of morality this may be true but on the level of concentration it's going

204
00:21:43,640 --> 00:21:53,520
to be much more difficult and so this is I think a real reason why people might decide

205
00:21:53,520 --> 00:22:01,640
to actually leave their jobs behind and become a monk or a nun, although it's not necessary

206
00:22:01,640 --> 00:22:07,200
and I suppose it's worth mentioning that it is possible to gain some level of concentration

207
00:22:07,200 --> 00:22:14,000
it's just something which is going to be much more difficult and cause a lot more unnecessary

208
00:22:14,000 --> 00:22:20,320
stress and difficulty because of the amount of work that we have to do.

209
00:22:20,320 --> 00:22:27,800
Finally on the level of wisdom when we talk about livelihood we can look at it in terms

210
00:22:27,800 --> 00:22:37,960
of wisdom or the wisdom which we're gaining in our lives or our understanding and

211
00:22:37,960 --> 00:22:45,440
see so the problem with livelihood living in the world is that it often has a lot to

212
00:22:45,440 --> 00:22:52,640
do with creating states of ego and states of delusions, states of attachment to self and

213
00:22:52,640 --> 00:23:02,360
conceit and when we examine our livelihood in terms of wisdom we have to see that we're

214
00:23:02,360 --> 00:23:11,200
actually doing and saying things which are more or less futile or useless and actually

215
00:23:11,200 --> 00:23:15,600
in many ways just creating more and more delusion for people.

216
00:23:15,600 --> 00:23:19,720
So for instance when we sell things we're often selling things to people that they don't

217
00:23:19,720 --> 00:23:31,400
really need when we work in an office we're often being part of the economy and helping

218
00:23:31,400 --> 00:23:38,360
people to get things that they don't need or do things maybe for questionable ends

219
00:23:38,360 --> 00:23:48,280
and so on and that well it may not be breaking precepts and it may not even be bothering

220
00:23:48,280 --> 00:23:50,040
our concentration.

221
00:23:50,040 --> 00:23:58,720
It's in the end creating the basis for both of these that simply the wrong working in

222
00:23:58,720 --> 00:24:06,760
an office that working for this company or that company working for one of the big retailers

223
00:24:06,760 --> 00:24:19,080
or so on, that we end up fostering or encouraging these very things we're encouraging

224
00:24:19,080 --> 00:24:28,240
stress, we're encouraging greed and we're encouraging anger and hatred and fear

225
00:24:28,240 --> 00:24:30,360
and worry and so on.

226
00:24:30,360 --> 00:24:34,320
We're being a part of this and we're creating it in ourselves as well because of the

227
00:24:34,320 --> 00:24:41,680
very nature of the job that maybe we can go through our work being at peace with ourselves

228
00:24:41,680 --> 00:24:48,360
but in the end it's a job which is not bringing real peace and happiness to people, it's

229
00:24:48,360 --> 00:24:52,360
not helping people to become closer to the Buddhist teaching, it's not helping people

230
00:24:52,360 --> 00:24:55,120
to become free from suffering.

231
00:24:55,120 --> 00:25:11,600
And so we have to look at really the basis of what we do, the amount of delusion involved

232
00:25:11,600 --> 00:25:21,840
with our work and we can see the amount of time that we have to spend on this earth

233
00:25:21,840 --> 00:25:30,600
and how we're spending it, how we're using this time and what benefit it's bringing

234
00:25:30,600 --> 00:25:37,400
to ourselves and to other people and that actually through the encouraging people to

235
00:25:37,400 --> 00:25:45,880
get and to chase after and to build up their egos and their conceits and their desires

236
00:25:45,880 --> 00:25:52,120
and their aversion and so on, we're actually creating delusion in the world through our

237
00:25:52,120 --> 00:25:59,480
work, we're becoming a part of these organizations and part of society and part of the economy

238
00:25:59,480 --> 00:26:09,400
and part of the government and so on, we're just another cog in the wheel and this is

239
00:26:09,400 --> 00:26:14,680
really at the basis of why the world is in such a mess that we're getting very much

240
00:26:14,680 --> 00:26:23,680
caught up with systems and structures which are creating suffering and this is something

241
00:26:23,680 --> 00:26:28,160
which then is going to lead to more and more and this is the reason why then we have

242
00:26:28,160 --> 00:26:33,680
these stressful jobs and this is the reason why we then do and say bad things.

243
00:26:33,680 --> 00:26:37,800
So wisdom is actually the core, it's the root of the problem which we're trying to root

244
00:26:37,800 --> 00:26:46,640
out, we're trying to cut out, so in terms of livelihood, this is something that is kind

245
00:26:46,640 --> 00:26:53,480
of the deepest level that we can look at our work where we, when we're working in the

246
00:26:53,480 --> 00:26:59,880
world for instance, that we might decide for ourselves that it's time to simplify things,

247
00:26:59,880 --> 00:27:06,280
that rather than getting a big house and a nice car and a good retirement plan, we might

248
00:27:06,280 --> 00:27:11,920
decide to live a much simpler life which might be much more out really difficult and not

249
00:27:11,920 --> 00:27:24,000
as secure but is in the end a lot more noble, a lot more peaceful, a lot more based

250
00:27:24,000 --> 00:27:29,120
on wisdom and it's going to give us great concentration and allow us to live moral and

251
00:27:29,120 --> 00:27:31,360
ethical lives.

252
00:27:31,360 --> 00:27:37,920
We really have to decide for ourselves and sometimes it takes a compromise, there are many

253
00:27:37,920 --> 00:27:43,760
people out there who are not going to be able to leave the home life but there are also

254
00:27:43,760 --> 00:27:50,600
many people who do change their livelihood, I had one student who was a biologist and

255
00:27:50,600 --> 00:27:55,400
he used to cut up rats before he started meditating and once he started meditating he

256
00:27:55,400 --> 00:27:59,280
realized that he couldn't do that anymore and now he works for the government, he does

257
00:27:59,280 --> 00:28:05,400
something for the government, he was able to change his livelihood to that extent.

258
00:28:05,400 --> 00:28:15,640
I would encourage I think a greater sort of change of lifestyle and the ultimate sort

259
00:28:15,640 --> 00:28:22,480
of perfection of a lay of a livelihood for someone who has not become a monk is epitomized

260
00:28:22,480 --> 00:28:30,960
in the Buddhist canon, the Buddhist Bible I guess if you will.

261
00:28:30,960 --> 00:28:36,600
With this man who makes pots and he goes down to the river and he gets loose clay that

262
00:28:36,600 --> 00:28:47,120
was dug up by rats or fall, caved in by the rain or so and washed up by the river and

263
00:28:47,120 --> 00:28:53,920
he gathered up the clay and he would go into the forest and collect wood that had fallen

264
00:28:53,920 --> 00:29:01,680
down and he would mold the clay into pots and fire them up using the wood that the discarded

265
00:29:01,680 --> 00:29:09,360
dead wood and he would make these simple pots and put them by the side of the road and

266
00:29:09,360 --> 00:29:15,480
sit there all day as people walk by and people would walk by and they would ask him

267
00:29:15,480 --> 00:29:20,640
how much is that pot, how much is that pot and whatever pot they point to do he would just

268
00:29:20,640 --> 00:29:27,200
say well you leave some beans, leave some rice, whatever you think it's worth and take

269
00:29:27,200 --> 00:29:33,240
whatever you like and this is how he made his livelihood, you know just living by the

270
00:29:33,240 --> 00:29:42,680
side of the road or maybe living in a simple dwelling and selling these pots and so while

271
00:29:42,680 --> 00:29:47,120
we couldn't do something exactly like this here in the west you still need or in the

272
00:29:47,120 --> 00:29:52,640
modern world you still need to pay rent and so on and so on.

273
00:29:52,640 --> 00:29:59,360
We take this as an example and we live our lives trying to emulate this kind of idea

274
00:29:59,360 --> 00:30:05,480
or even better to emulate the life of a monk where we know we have to make some compromises

275
00:30:05,480 --> 00:30:11,520
and we will never be able to live a pure life unless we are able to make the jump and

276
00:30:11,520 --> 00:30:21,880
give up our attachments to the world but we make steps in this direction so rather than

277
00:30:21,880 --> 00:30:28,440
thinking we want to become the manager or the boss or the head or you know become rich

278
00:30:28,440 --> 00:30:34,000
and make a fortune or become famous or become a big shot we're content with something

279
00:30:34,000 --> 00:30:38,720
very simple and something which the simpler the better and if it means we have to leave

280
00:30:38,720 --> 00:30:47,360
our house and get a smaller apartment or we have to know sell our car and get a cheaper

281
00:30:47,360 --> 00:30:54,720
car or so on these many different things we have to give up some of our luxuries then

282
00:30:54,720 --> 00:30:59,320
I think this is the steps that we have to take and these are the steps in the right

283
00:30:59,320 --> 00:31:11,320
direction and to the point where we are able to do away with all of our burdens all

284
00:31:11,320 --> 00:31:17,120
of our things that are required of us in the world and are able to make the leap and

285
00:31:17,120 --> 00:31:26,280
become a monk or a nun or become a full-time meditator or go to live in a monastery even

286
00:31:26,280 --> 00:31:31,560
some people who don't ordain but go to live and help out in a monastery or help out

287
00:31:31,560 --> 00:31:36,880
in a meditation center help to build a meditation center I know many people who aren't

288
00:31:36,880 --> 00:31:45,440
able to ordain but they're working very hard to build up meditation centers in their

289
00:31:45,440 --> 00:31:51,520
area so that once they can build it up then they'll be able to give up this sort of

290
00:31:51,520 --> 00:31:59,480
a life so finding a way to get closer and closer to the Dharma and not sort of fooling

291
00:31:59,480 --> 00:32:05,920
ourselves into thinking that we can have our cake and eat it too I think I would advocate

292
00:32:05,920 --> 00:32:11,640
that we should be trying to change our lifestyles and trying all the time to get closer

293
00:32:11,640 --> 00:32:18,080
and closer step by step to lifestyle where we're living the Dharma and I think I'm justified

294
00:32:18,080 --> 00:32:23,800
in this I think many people they're going to say it's pretty biased to just say you

295
00:32:23,800 --> 00:32:29,400
have to become a monk or you have to become a nun or you have to leave the home life

296
00:32:29,400 --> 00:32:37,600
but you know we're on this earth for 80 years maybe nobody lives to be 100 anymore

297
00:32:37,600 --> 00:32:46,120
I don't think very few anyway 80 years at most let's say and you know then that's

298
00:32:46,120 --> 00:32:51,080
it then we don't know where we're going we don't know what happens and here we are chasing

299
00:32:51,080 --> 00:32:56,840
after nothing chasing after something that is meaningless living our lives running around

300
00:32:56,840 --> 00:33:04,000
in a hamster wheel for nothing that in the end we will have to leave behind and not

301
00:33:04,000 --> 00:33:10,760
know where we're going not know what comes next and dying in uncertainty with the mind

302
00:33:10,760 --> 00:33:18,600
which is still distracted and unfocused and unclear and impure that we still have many

303
00:33:18,600 --> 00:33:25,640
things about ourselves that we feel unresolved inside that there are many things about

304
00:33:25,640 --> 00:33:33,480
ourselves that we don't understand and that we will live our lives we will die in a confused

305
00:33:33,480 --> 00:33:42,400
state of mind so it's just sort of some thoughts on how we should live our lives and

306
00:33:42,400 --> 00:33:46,480
sort of what livelihood means in a real sense that we're not just talking about what

307
00:33:46,480 --> 00:33:50,680
are the right kinds of work that we can do in lay life that aren't going to be against

308
00:33:50,680 --> 00:33:55,840
aren't going to be wrong or sinful for instance in Buddhism we're actually talking

309
00:33:55,840 --> 00:34:04,560
about changing the way we live our lives and giving up our burdens giving up our attachments

310
00:34:04,560 --> 00:34:10,480
more and more to the point where we're able to free ourselves from all suffering so thank

311
00:34:10,480 --> 00:34:16,000
you all I think I'm recovering still from this flu but hopefully over the next few days

312
00:34:16,000 --> 00:34:22,400
I'll be able to give more and more talks thank you all for tuning in and if anyone would

313
00:34:22,400 --> 00:34:27,720
like to discuss I'm happy to stick around for a little bit longer but that's all for today

314
00:34:27,720 --> 00:34:55,880
thanks for coming bye

