1
00:00:00,000 --> 00:00:05,000
I'm not a Buddhist, but I find meditation to be an incredibly useful skill.

2
00:00:05,000 --> 00:00:12,000
How important do you think traditions of Buddhism are outside of meditation?

3
00:00:12,000 --> 00:00:16,000
I've sort of answered this question.

4
00:00:16,000 --> 00:00:25,000
I don't know in various places, probably not clearly.

5
00:00:25,000 --> 00:00:30,000
They're not intrinsically important.

6
00:00:30,000 --> 00:00:38,000
I would categorize them as beneficial, potentially beneficial.

7
00:00:38,000 --> 00:00:42,000
Let's even say that.

8
00:00:42,000 --> 00:00:48,000
They can be potentially deleterious if used in the wrong way, if misunderstood.

9
00:00:48,000 --> 00:00:58,000
If you believe that the rights and rituals that Buddhists perform are intrinsically valuable,

10
00:00:58,000 --> 00:01:02,000
then you start to have a problem.

11
00:01:02,000 --> 00:01:12,000
If you think that any Buddhist practice,

12
00:01:12,000 --> 00:01:18,000
and giving charity or even practicing meditation, let's go so far.

13
00:01:18,000 --> 00:01:25,000
Morality, meditation, if you think any of these things are intrinsically valuable,

14
00:01:25,000 --> 00:01:27,000
then you've got a problem.

15
00:01:27,000 --> 00:01:30,000
This is, I think, not widely understood.

16
00:01:30,000 --> 00:01:33,000
Certainly not in cultural Buddhism.

17
00:01:33,000 --> 00:01:41,000
Cultural Buddhists are generally under the misunderstanding that these things are intrinsically beneficial.

18
00:01:41,000 --> 00:01:45,000
People in general in the world are, if whatever religion they belong to,

19
00:01:45,000 --> 00:01:49,000
or even people are not more, especially people who are not religious,

20
00:01:49,000 --> 00:01:55,000
tend to think that things that are not intrinsically valuable are intrinsically valuable.

21
00:01:55,000 --> 00:02:04,000
There's a Suta, the Salopamasut, I can't remember the Mahasaropamasut,

22
00:02:04,000 --> 00:02:11,000
or the Jullasaropamasut, in the Midgimanikaya.

23
00:02:11,000 --> 00:02:18,000
Anyway, one of them talks about this exact issue,

24
00:02:18,000 --> 00:02:26,000
what is and what is not truly a benefit.

25
00:02:26,000 --> 00:02:32,000
This isn't exactly answering questions, but I think it's important

26
00:02:32,000 --> 00:02:35,000
in terms of providing a context for the answer,

27
00:02:35,000 --> 00:02:44,000
that even keeping, you know, you might say giving gifts or the idea of charity,

28
00:02:44,000 --> 00:02:48,000
morality, concentration, tranquility,

29
00:02:48,000 --> 00:02:53,000
even Vipasana insight, the Buddha said these are not the core.

30
00:02:53,000 --> 00:02:56,000
These are not truly, these are not the heart wood,

31
00:02:56,000 --> 00:03:03,000
they're not the sara, the true essence of Buddhism.

32
00:03:03,000 --> 00:03:06,000
So, if we want to talk about traditions,

33
00:03:06,000 --> 00:03:10,000
we can tentatively place them,

34
00:03:10,000 --> 00:03:13,000
you might say, below the charity,

35
00:03:13,000 --> 00:03:14,000
rung on the ladder.

36
00:03:14,000 --> 00:03:21,000
So, you have this rung below charity in that traditional practices.

37
00:03:21,000 --> 00:03:29,000
Because traditional practices or rituals can serve to be a framework

38
00:03:29,000 --> 00:03:33,000
within which you can develop charity.

39
00:03:33,000 --> 00:03:37,000
So, you have these Buddhist holidays, for example.

40
00:03:37,000 --> 00:03:42,000
This helps develop charity, the idea of charity, it also helps,

41
00:03:42,000 --> 00:03:48,000
you know, to develop respect and humility and gratitude

42
00:03:48,000 --> 00:03:53,000
for the Buddha, I'm thinking, it helps to develop morality.

43
00:03:53,000 --> 00:03:56,000
It's an opportunity for people to cultivate morality,

44
00:03:56,000 --> 00:03:58,000
to try to keep the aid precepts,

45
00:03:58,000 --> 00:04:01,000
if they can't keep them every day,

46
00:04:01,000 --> 00:04:03,000
then they try to keep them on the Buddhist holiday.

47
00:04:03,000 --> 00:04:08,000
This is a good thing about a Buddhist holiday, for example.

48
00:04:08,000 --> 00:04:11,000
It's a good chance to calm their minds.

49
00:04:11,000 --> 00:04:14,000
It's a good chance to develop insight.

50
00:04:14,000 --> 00:04:20,000
And it's even a good chance to develop that which is of true benefit.

51
00:04:20,000 --> 00:04:23,000
It's even possible that on a Buddhist holiday,

52
00:04:23,000 --> 00:04:30,000
it can help you to develop insight that leads to enlightenment.

53
00:04:30,000 --> 00:04:36,000
So, I would place all traditions on this tentative,

54
00:04:36,000 --> 00:04:44,000
this tentative, lowest rung on the beneficial ladder.

55
00:04:44,000 --> 00:04:50,000
And so, I just want to finish the explanation

56
00:04:50,000 --> 00:04:54,000
that none of these things are intrinsically valuable.

57
00:04:54,000 --> 00:04:59,000
So, more so Buddhist traditions,

58
00:04:59,000 --> 00:05:04,000
when you say that morality, none of these things are actually intrinsically beneficial.

59
00:05:04,000 --> 00:05:09,000
They're all only for the purpose of bringing out that which is of true benefit,

60
00:05:09,000 --> 00:05:11,000
which is enlightenment or nibhana.

61
00:05:11,000 --> 00:05:19,000
So, the problem comes when you have these traditions that,

62
00:05:19,000 --> 00:05:25,000
well, the biggest problem comes when you have traditions that don't do anything,

63
00:05:25,000 --> 00:05:31,000
that don't help you develop morality or concentration or wisdom or even charity.

64
00:05:31,000 --> 00:05:34,000
So, therefore, don't lead you closer to the goal.

65
00:05:34,000 --> 00:05:37,000
Some traditions can actually lead people farther from the goal.

66
00:05:37,000 --> 00:05:40,000
Traditions of horoscopes.

67
00:05:40,000 --> 00:05:45,000
And there are many, you might say, use this tradition.

68
00:05:45,000 --> 00:05:49,000
So, for example, we have a new well that we dug.

69
00:05:49,000 --> 00:05:52,000
And they put these lamps up around the well.

70
00:05:52,000 --> 00:05:54,000
I'm not sure why.

71
00:05:54,000 --> 00:05:58,000
But it's some kind of tradition and it must have to do with angels and so on.

72
00:05:58,000 --> 00:06:02,000
Which, just an excellent example,

73
00:06:02,000 --> 00:06:06,000
you may be able to twist it and you may actually be able to find something beneficial.

74
00:06:06,000 --> 00:06:09,000
But I would say in many cases, these sorts of traditions,

75
00:06:09,000 --> 00:06:12,000
especially when they come to be thought of as Buddhist traditions,

76
00:06:12,000 --> 00:06:15,000
can actually be deleterious.

77
00:06:15,000 --> 00:06:21,000
They lead to wrong views and wrong practices.

78
00:06:21,000 --> 00:06:27,000
But even practices, even traditions that encourage the development of charity,

79
00:06:27,000 --> 00:06:31,000
morality, concentration, even wisdom.

80
00:06:31,000 --> 00:06:41,000
If they are reinforcing the idea that these things have intrinsic value,

81
00:06:41,000 --> 00:06:45,000
then they can be somewhat problematic.

82
00:06:45,000 --> 00:06:47,000
I mean, they're not evil.

83
00:06:47,000 --> 00:06:50,000
They're not something that you have to scold people about.

84
00:06:50,000 --> 00:06:56,000
But it's something that you might want to politely remind people that this isn't the goal.

85
00:06:56,000 --> 00:06:59,000
So what I'm thinking of especially is charity.

86
00:06:59,000 --> 00:07:02,000
So people will have these traditions of giving charity.

87
00:07:02,000 --> 00:07:05,000
And thinking that this is my Buddhist practices, this practice of charity.

88
00:07:05,000 --> 00:07:08,000
And they never think of what is charity for.

89
00:07:08,000 --> 00:07:12,000
The farthest they might get is that charity leads me to go to heaven and so on.

90
00:07:12,000 --> 00:07:20,000
And so they never get closer or they don't get much closer anyway to the actual goal to that,

91
00:07:20,000 --> 00:07:23,000
which is a real benefit.

92
00:07:23,000 --> 00:07:27,000
So in short, as I said in the beginning,

93
00:07:27,000 --> 00:07:36,000
I don't think that you could think of them as being important, but beneficial.

94
00:07:36,000 --> 00:07:41,000
And I think there's a difference because there are many beneficial things.

95
00:07:41,000 --> 00:07:48,000
Things that you could do that could benefit your practice that I don't think you would go so far as to say they're important.

96
00:07:48,000 --> 00:07:52,000
The only things that are truly important are morality, concentration, and wisdom.

97
00:07:52,000 --> 00:07:59,000
Because those are the things that lead you to release and knowledge of, of release, knowledge of freedom.

98
00:07:59,000 --> 00:08:06,000
Apart from that, there are beneficial things that you can engage in and you don't have to engage in.

99
00:08:06,000 --> 00:08:10,000
For some people they're beneficial, for some people they're useless, for some people they're even deleterious.

100
00:08:10,000 --> 00:08:14,000
So for some people, traditions just get in their way.

101
00:08:14,000 --> 00:08:24,000
For some people, for some people it's the only opportunity they have, for the ordinary Sri Lankan villager.

102
00:08:24,000 --> 00:08:28,000
It's sometimes the only excuse that they have to come and practice meditation.

103
00:08:28,000 --> 00:08:32,000
So they have here a day off and they're not expected to work, they're not expected to do anything.

104
00:08:32,000 --> 00:08:38,000
So now finally they have a chance to practice and cultivate good deeds.

105
00:08:38,000 --> 00:08:44,000
And I would say there are various traditions that are like this that are a reminder for people.

106
00:08:44,000 --> 00:08:54,000
Just like having a schedule, having these things that, that when, for example, before you eat to offer food to the Buddha,

107
00:08:54,000 --> 00:09:01,000
it's a reminder for the person of the Buddha's teaching and of the Buddha's greatness and so on.

108
00:09:01,000 --> 00:09:05,000
They can potentially lead people to closer to the practice.

109
00:09:05,000 --> 00:09:18,000
So I wouldn't say important in the sense that you're missing something but beneficial in the sense that they may possibly help you to encourage you in your practice.

110
00:09:18,000 --> 00:09:23,000
You know, having a Buddha image, bowing down to the Buddha image once in a while, these can be beneficial things.

111
00:09:23,000 --> 00:09:34,000
It can be. I've talked about the Buddha image before and how ambivalent about it, or I have two different takes on the Buddha image that it can actually be dangerous.

112
00:09:34,000 --> 00:09:45,000
But it can certainly be beneficial. I know from, for me, the Buddha image was a very important support when I was first learning about Buddhism.

113
00:09:45,000 --> 00:10:00,000
When I was first practicing meditation, because my mind was in quite an chaos and just seeing the Buddha image, at one point in my meditation course, really brought me back and gave me the confidence that I needed to continue my practice.

114
00:10:00,000 --> 00:10:05,000
This was the Buddha image important, actually. I would say for me it was quite important.

115
00:10:05,000 --> 00:10:19,000
If I hadn't seen the Buddha image, I probably would have still finished the course, but it was an important beneficial experience for me that did change my practice.

116
00:10:19,000 --> 00:10:27,000
Just psychologically gave me the support, reminded me about, you know, this is not something simple that I'm doing and encouraged me.

117
00:10:27,000 --> 00:10:36,000
I gave myself up to the Buddha and it made me think, I don't have to worry anymore. It can be as much pain as much pain as there will be.

118
00:10:36,000 --> 00:10:43,000
I don't have to cling to it. It's not me. It's not mine. It's now the Buddha. It belongs to him now.

119
00:10:43,000 --> 00:10:54,000
I felt freed from my feelings of guilt and all the things that I had done which were immoral and improper and so on.

120
00:10:54,000 --> 00:10:58,000
So I think they can be useful.

