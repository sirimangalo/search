1
00:00:00,000 --> 00:00:08,960
Okay, good evening, everyone, because there's no one watching it, but you'll see this

2
00:00:08,960 --> 00:00:11,840
in the recording as well.

3
00:00:11,840 --> 00:00:28,520
So, again, we're looking at the relationship between Buddhism and nature.

4
00:00:28,520 --> 00:00:41,040
And so the second way of looking at nature from a Buddhist perspective is I think in terms

5
00:00:41,040 --> 00:00:55,000
of what we say in English, the nature of things, of human nature, or even just things as having

6
00:00:55,000 --> 00:01:11,880
a nature as having an existence, a presence, and so I'll explain what I mean by that

7
00:01:11,880 --> 00:01:12,880
one.

8
00:01:12,880 --> 00:01:21,360
It's maybe a little hard to understand, but the nature of things, the nature of things that

9
00:01:21,360 --> 00:01:42,640
relates to our understanding of things being stable, being constant, predictable, dependable,

10
00:01:42,640 --> 00:01:50,760
and democratic, right, so what is the nature of things?

11
00:01:50,760 --> 00:01:56,400
Well, physics will tell you things about the nature of things and so on science as much

12
00:01:56,400 --> 00:02:09,200
to say, Buddhism has much to say, but we go far beyond that as I think an issue that

13
00:02:09,200 --> 00:02:21,520
we have to face and understand in Buddhism is that we take, well, nature, again, the

14
00:02:21,520 --> 00:02:32,520
inspiration of these few talks was the concern for our environment, for the climate,

15
00:02:32,520 --> 00:02:45,080
for nature as a concept, as a thing to be protected and concerned about, the thing that

16
00:02:45,080 --> 00:02:56,320
is being destroyed by human greed and corruption, by us, destroying nature in many ways.

17
00:02:56,320 --> 00:03:05,640
So that's another thing that we have to look at in that regard is that being a part

18
00:03:05,640 --> 00:03:06,800
of nature.

19
00:03:06,800 --> 00:03:17,360
The nature of things is to be changeable, is that this concept of nature, trees, the mountains,

20
00:03:17,360 --> 00:03:25,080
the weather, the air, the water is all malleable.

21
00:03:25,080 --> 00:03:32,440
It doesn't have a nature, if anyone asks you about nature, when you go into the forests

22
00:03:32,440 --> 00:03:39,240
or to the seaside and you talk about how peaceful and how calming and how wonderful

23
00:03:39,240 --> 00:03:51,240
nature is, that kind of nature, this nature as a thing, as opposed to the human, it's just

24
00:03:51,240 --> 00:04:01,280
a concept in our minds and the reality is changing.

25
00:04:01,280 --> 00:04:02,760
Reality is susceptible to change.

26
00:04:02,760 --> 00:04:08,120
There's no such thing as the nature of things in that sense.

27
00:04:08,120 --> 00:04:15,400
Our ideas and the nature of things give us a sense of stability that is illusory, that isn't

28
00:04:15,400 --> 00:04:16,400
there.

29
00:04:16,400 --> 00:04:25,640
I mean, part of the suffering from climate change is going to be the simple change, having

30
00:04:25,640 --> 00:04:34,640
to deal with change, and sometimes the change is so drastic that it leads to direct suffering,

31
00:04:34,640 --> 00:04:52,800
flooding, wildfires, tornadoes, simple drought and loss of livelihood and so on.

32
00:04:52,800 --> 00:05:03,480
But in general, speaking in general, our illusions about the nature of things, things

33
00:05:03,480 --> 00:05:11,160
having in nature is a cause of a great suffering, a needless suffering, whereas if we could

34
00:05:11,160 --> 00:05:18,360
see the changing nature of things, human nature as well, I mentioned that, it's important

35
00:05:18,360 --> 00:05:27,000
to talk about, because we think of human nature as being stable, static.

36
00:05:27,000 --> 00:05:32,920
I'm an angry person, I'm a depressed person.

37
00:05:32,920 --> 00:05:38,120
What did I read about depression?

38
00:05:38,120 --> 00:05:50,160
One of our professors or someone in the university recently said that there's only

39
00:05:50,160 --> 00:06:05,960
a 25% difference between the effects of medication and the effects of a placebo for people

40
00:06:05,960 --> 00:06:11,920
with moderate or mild depression.

41
00:06:11,920 --> 00:06:14,760
So we talk about being depressed.

42
00:06:14,760 --> 00:06:21,680
I have depression and we think of it as an illness, as a disease, as something to be treated

43
00:06:21,680 --> 00:06:29,240
with medication due to what we call a chemical imbalance.

44
00:06:29,240 --> 00:06:31,080
Chemical imbalance is something static.

45
00:06:31,080 --> 00:06:40,640
You can't wish it away, right, it's a part of who you are.

46
00:06:40,640 --> 00:06:47,840
Yet we find that these medications that are meant to change that chemical balance are

47
00:06:47,840 --> 00:06:58,800
not nearly as effective as they think, or they're not much more effective than, in some

48
00:06:58,800 --> 00:07:03,640
cases, in some sense, just wishing it away with placebo effect.

49
00:07:03,640 --> 00:07:08,360
I mean, the effects of meditation would be quite similar, I think, to the placebo effect

50
00:07:08,360 --> 00:07:12,960
in terms of changing the state of mind.

51
00:07:12,960 --> 00:07:17,200
The human nature is malleable.

52
00:07:17,200 --> 00:07:24,280
Someone else recently said that up until recently we thought the mind was static that

53
00:07:24,280 --> 00:07:31,000
after seven years old, the brain didn't change.

54
00:07:31,000 --> 00:07:35,200
Of course, we know that's false, that's false.

55
00:07:35,200 --> 00:07:46,880
And adult blood brains can change our malleable, and so calling it a disease and thinking

56
00:07:46,880 --> 00:07:55,280
of it as a part of you, a part of who you are, is incorrect, and who you are, and your

57
00:07:55,280 --> 00:08:01,480
human nature, as malleable, is changeable, is impermanent.

58
00:08:01,480 --> 00:08:05,160
Even our physical nature is impermanent.

59
00:08:05,160 --> 00:08:14,840
This body is just a shell or a husk, like a sand castle, that we build up, I think of those

60
00:08:14,840 --> 00:08:23,800
nine months in the womb that we spent kneading and molding and tweaking, no, not, of course,

61
00:08:23,800 --> 00:08:30,400
you can't get the mind created the fetus, but certainly they had to play the part of those

62
00:08:30,400 --> 00:08:44,160
nine months in tweaking it, and, of course, throughout life, tweaking as well, changing.

63
00:08:44,160 --> 00:08:53,520
This physical form is also temporary, so the true nature of things, so the question is,

64
00:08:53,520 --> 00:09:00,360
is there a true nature of things, is there something that is stable and constant?

65
00:09:00,360 --> 00:09:08,440
And there is something that you can say is always the nature of things, and the nature

66
00:09:08,440 --> 00:09:15,880
of things is to change.

67
00:09:15,880 --> 00:09:21,520
And that's more than just a clever thing to say, because, of course, it's basically saying

68
00:09:21,520 --> 00:09:28,600
that the nature of things is to not have a constant nature, but it's more than that, because

69
00:09:28,600 --> 00:09:36,720
understanding of impermanence, understanding of the unpredictable nature of things, is a stable

70
00:09:36,720 --> 00:09:40,560
state, the understanding of it in me.

71
00:09:40,560 --> 00:09:49,280
When you understand that law of nature, that aspect of nature, it's a very powerful thing.

72
00:09:49,280 --> 00:09:55,360
It changes the way you look at the world, it changes the way you interact with the world.

73
00:09:55,360 --> 00:10:02,880
It allows you to let go, to be free from the suffering that comes from change, right?

74
00:10:02,880 --> 00:10:10,120
So it is an important, stable reality, to understand thoroughly, so that you gain the

75
00:10:10,120 --> 00:10:17,840
stability of understanding, the stability that comes from being able to adapt, the stability

76
00:10:17,840 --> 00:10:30,520
of flexibility in the sense.

77
00:10:30,520 --> 00:10:38,080
Understanding impermanence, understanding, suffering, the suffering that comes from clinging

78
00:10:38,080 --> 00:10:40,840
to things, right?

79
00:10:40,840 --> 00:10:48,880
Everything is changing, then clinging to stability, clinging to something, just like clinging

80
00:10:48,880 --> 00:11:01,800
to a raft in the fast moving river, and it just gets swept away, clinging to, clinging

81
00:11:01,800 --> 00:11:09,360
to a sinking ship.

82
00:11:09,360 --> 00:11:21,120
About being flexible is not clinging, flexibility implies, or the adaptability or the understanding

83
00:11:21,120 --> 00:11:27,080
of impermanence, implies a lack of attachment, so we hear about non-attachment in Buddhism

84
00:11:27,080 --> 00:11:33,360
and not clinging and not wanting or craving or even liking anything, and yes, it implies

85
00:11:33,360 --> 00:11:42,000
that, implies that happiness can't come from clinging or craving, holding on to anything

86
00:11:42,000 --> 00:11:46,240
like anything, it can't come from things.

87
00:11:46,240 --> 00:11:55,600
Your happiness has to be independent of things, things that it was only nature is to change.

88
00:11:55,600 --> 00:12:01,120
Understanding of non-self that you can't control things, you can't stop this process.

89
00:12:01,120 --> 00:12:09,600
The process of change is not me, it's not mine.

90
00:12:09,600 --> 00:12:27,040
In fact, in fact, our struggle to be satisfied, to find pleasure, changes things themselves,

91
00:12:27,040 --> 00:12:35,040
changes, changes things, it creates impermanence, not only does it create suffering because

92
00:12:35,040 --> 00:12:41,760
of change, but it creates change, I'm thinking now, again, back to the deal environment,

93
00:12:41,760 --> 00:12:53,360
as I mentioned in the last video, our craving, our clinging, think of an addict, an

94
00:12:53,360 --> 00:13:01,920
addict, gains pleasure from a drug, which of course changes the brain, makes it harder

95
00:13:01,920 --> 00:13:07,560
to get pleasure from the same drug and leads to greater and greater addiction.

96
00:13:07,560 --> 00:13:11,480
This is true with so many things, so it's true with any addiction, to even simple things

97
00:13:11,480 --> 00:13:21,800
like food, television, music, the brain is elastic and you change the brain.

98
00:13:21,800 --> 00:13:28,440
Not only do we change our brains, we change our environment, our incessant drive to find

99
00:13:28,440 --> 00:13:37,600
pleasure and satisfaction is changing the world around us, it's destroying the environment.

100
00:13:37,600 --> 00:13:46,120
Even our consumerism, our consumption of water, our consumption of oil, plastic, all

101
00:13:46,120 --> 00:13:47,120
of these things.

102
00:13:47,120 --> 00:13:52,600
Which is why I think it's fair to say, but it should be environmentally conscious in

103
00:13:52,600 --> 00:14:02,120
terms of understanding the relationship between our greed and our suffering, the suffering

104
00:14:02,120 --> 00:14:11,880
caused by an environment that's degraded, our clinging, our craving, even our control,

105
00:14:11,880 --> 00:14:27,720
our intent of our drive to control, to cling, to maintain cling distability.

106
00:14:27,720 --> 00:14:34,240
All of that is creating destruction, it's creating stress, it creates systems that are

107
00:14:34,240 --> 00:14:45,760
oppressed, other beings, oppressed, other humans.

108
00:14:45,760 --> 00:14:53,080
We create suffering, because we don't see the nature of things, we don't see that we

109
00:14:53,080 --> 00:15:00,520
our behavior causes suffering, there is a nature of things, it's quite simple in that

110
00:15:00,520 --> 00:15:03,160
way.

111
00:15:03,160 --> 00:15:08,440
That's what the impermanence really is, the cornerstone of it all.

112
00:15:08,440 --> 00:15:17,680
Being impermanent, nothing is satisfied, you can find satisfaction or happiness in things.

113
00:15:17,680 --> 00:15:26,400
And that being so they are not me, they are not mine, they are not under my control, the

114
00:15:26,400 --> 00:15:38,440
idea of self is just a delusion, the idea of me and mine, our trap that leads us to

115
00:15:38,440 --> 00:15:41,760
only to greater suffering.

116
00:15:41,760 --> 00:15:49,400
So there you go, more on the nature of things, more on Buddhism in nature, one more video,

117
00:15:49,400 --> 00:15:55,040
one more aspect of nature I think, and then we'll be done with that.

118
00:15:55,040 --> 00:16:02,480
And I'm not making videos that often, I don't know when the next one will be, and I expect

119
00:16:02,480 --> 00:16:10,560
that in the new year I'll be making more videos, maybe even next month, but this month

120
00:16:10,560 --> 00:16:20,040
is somewhat occupied with other things, so thank you all, have a good night, thank you

121
00:16:20,040 --> 00:16:27,080
for tuning in, 43 people, spur of the moment on a Wednesday night, I'm very much appreciated.

122
00:16:27,080 --> 00:16:57,040
So have a good night, we'll show you the best.

