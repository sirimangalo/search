1
00:00:00,000 --> 00:00:25,560
okay good evening everyone welcome to our evening dumb

2
00:00:25,560 --> 00:00:38,720
must session at an interesting

3
00:00:38,720 --> 00:00:46,320
very soon something interesting happened on the bus again today

4
00:00:46,320 --> 00:00:51,240
buses are of course interesting place as a woman spilt her coffee on me this

5
00:00:51,240 --> 00:01:07,120
evening drop the whole mug between me and another woman but the man in the

6
00:01:07,120 --> 00:01:20,280
wheelchair got back on the same guy and asked the best driver why he

7
00:01:20,280 --> 00:01:25,640
why his white when why he had was growing his beard out whether he was

8
00:01:25,640 --> 00:01:32,760
growing it out for the 25th he said an Indian man I think he really didn't

9
00:01:32,760 --> 00:01:39,080
quite understand what Christmas was he thought this bus driver with the white

10
00:01:39,080 --> 00:01:47,200
beard was growing it out for for Christmas so made me think maybe we should

11
00:01:47,200 --> 00:02:00,640
talk about culture cultures an interesting topic so in this context

12
00:02:00,640 --> 00:02:09,440
culture is a very conceptual thing as to do with not only people but groups

13
00:02:09,440 --> 00:02:17,680
of people that's not so interesting for meditators so keeping it in the

14
00:02:17,680 --> 00:02:25,800
spirit of meditation there are some interesting aspects of culture what it

15
00:02:25,800 --> 00:02:31,520
boils down to on an individual level comes down to what I've been talking

16
00:02:31,520 --> 00:02:41,920
about recently and this idea of the personality and part of our job as

17
00:02:41,920 --> 00:02:47,440
meditators is to recognize therefore our culture what part of us is

18
00:02:47,440 --> 00:02:57,680
culture and so these are good examples how in modern globalized science

19
00:02:57,680 --> 00:03:16,200
society we find culture culture is clashing we say how people get in in the

20
00:03:16,200 --> 00:03:24,880
way of us understanding each other cultures culture is something

21
00:03:24,880 --> 00:03:34,000
incidentally as a that I've felt quite strongly as a Western Buddhist monk

22
00:03:35,200 --> 00:03:45,600
it's funny as soon as you when you become a monk and you you especially when

23
00:03:45,600 --> 00:03:51,120
you don't use money you become very close to the culture and what she

24
00:03:51,120 --> 00:04:02,480
live very much you see the culture no only wait like similar to how any other

25
00:04:02,480 --> 00:04:12,200
homeless beggar would you no longer privileged you no longer have any status in

26
00:04:12,200 --> 00:04:20,320
society so you get to see the very drugs of society and so you wind up very

27
00:04:20,320 --> 00:04:24,720
much caught up in culture and as a foreigner that can be quite difficult so I

28
00:04:24,720 --> 00:04:28,720
felt culture quite strongly it's interesting there's not quite

29
00:04:28,720 --> 00:04:38,560
interesting it's often tied up with things like nationalism it's very much

30
00:04:38,560 --> 00:04:45,760
caught up in the past and a identity that's very much caught up in our

31
00:04:45,760 --> 00:04:56,160
in a history it's about identity and in ethnicity skin color there's so

32
00:04:56,160 --> 00:05:04,720
many gender all of these are very strong

33
00:05:06,240 --> 00:05:11,440
have a very strong impact on our relationships and on and constantly on our

34
00:05:11,440 --> 00:05:31,600
lives we're having a debate over this new movement to try and get to create a

35
00:05:31,600 --> 00:05:36,960
obligation in society to call people by whatever gender they prefer to be

36
00:05:36,960 --> 00:05:44,560
called by not just male or female but could be any number of constructed

37
00:05:44,560 --> 00:05:48,880
genders which is interesting as a Buddhist because we would agree that

38
00:05:48,880 --> 00:05:52,880
gender is somewhat of a construct

39
00:06:00,080 --> 00:06:03,920
it's physical besides the physical there's nothing there's nothing mental

40
00:06:03,920 --> 00:06:10,000
about gender so you can decide to be whatever gender you want to be

41
00:06:10,000 --> 00:06:15,200
thoughts max of egoism and identification and and actually the point that's

42
00:06:15,200 --> 00:06:23,040
how it all ties in here culture culture can be quite a terrible thing

43
00:06:23,840 --> 00:06:29,680
and for the most part I would argue it is a negative thing

44
00:06:29,680 --> 00:06:37,360
now where culture is useful is where it protects

45
00:06:37,360 --> 00:06:41,520
culture can sometimes be an extension of religion

46
00:06:41,520 --> 00:06:46,160
so you'll often see Buddhist culture can be quite beautiful can be

47
00:06:46,160 --> 00:06:49,360
a lot of problems with many Buddhist countries that I've lived in just as

48
00:06:49,360 --> 00:06:53,200
there are problems everywhere but there's some very beautiful things that

49
00:06:53,200 --> 00:07:00,480
you find in these cultures that are not exactly Buddhism there are many

50
00:07:00,480 --> 00:07:05,600
methods and means of performing good deeds a lot of charity work is

51
00:07:05,600 --> 00:07:11,760
systematized simplified

52
00:07:15,760 --> 00:07:21,680
and this result becomes a very much a part of people's lives

53
00:07:21,680 --> 00:07:27,280
they've assimilated into who they are I am someone who gives

54
00:07:27,280 --> 00:07:31,360
we are cultures one of giving for example

55
00:07:31,360 --> 00:07:35,760
our culture is one of keeping moral precepts

56
00:07:35,760 --> 00:07:39,600
our culture is one of meditation

57
00:07:42,160 --> 00:07:47,120
cultures a lot like an an eggshell or a casing in this way

58
00:07:47,120 --> 00:07:54,880
it's a protector protects religion because it becomes tradition

59
00:07:54,880 --> 00:07:58,480
religion religious views and beliefs practices

60
00:07:58,480 --> 00:08:05,680
experiences become enshrined in the culture that's where it's useful

61
00:08:06,240 --> 00:08:10,800
the problem is we begin to take culture as religion

62
00:08:10,800 --> 00:08:17,040
so we begin to take traditions the practices that we've

63
00:08:17,040 --> 00:08:28,480
called the we've manufactured we take them to be in and of themselves

64
00:08:28,480 --> 00:08:37,040
important I was just thinking about this now I just had a shower because

65
00:08:37,040 --> 00:08:40,240
I like to shower before I teach the dhamma and I was thinking what a silly thing

66
00:08:40,240 --> 00:08:45,440
but it's a gesture but you see this is this sort of thing how this

67
00:08:45,440 --> 00:08:51,200
can can become a ritual so I do it because it feels like well it's respectful to

68
00:08:51,200 --> 00:08:54,640
when you're teaching the dhamma it's actually in the text this is a thing

69
00:08:54,640 --> 00:08:58,480
before teaching they would they would bathe

70
00:08:59,360 --> 00:09:04,320
so keeping with tradition and

71
00:09:05,040 --> 00:09:09,520
out of respect for the dhamma because I hadn't showered today

72
00:09:09,520 --> 00:09:14,960
I had a shower but it's silly because you know Buddhism is not

73
00:09:14,960 --> 00:09:23,280
at all about physical cleanliness you could argue that it somehow makes you

74
00:09:23,280 --> 00:09:27,280
gives you good concentration we often have our meditators to

75
00:09:27,280 --> 00:09:31,280
forgo showering when they get to the really intense parts we tell them to

76
00:09:31,280 --> 00:09:37,280
stop showering because it it it resets things you know it did it's a comfort

77
00:09:37,280 --> 00:09:42,080
it's a way of it's not such a big deal here in in in the West specifically

78
00:09:42,080 --> 00:09:47,280
talking about showering but if you'd ever lived in Southeast Asia you

79
00:09:47,280 --> 00:09:51,840
understand the the importance of it because if you don't shower

80
00:09:51,840 --> 00:09:56,320
twice a day people start to think there's something wrong with you

81
00:09:56,320 --> 00:09:59,440
even if you don't smell

82
00:10:00,640 --> 00:10:05,120
it's a big part of the discourse have you showered yet

83
00:10:05,120 --> 00:10:12,080
it's a big part of Thai society anyway so telling a Thai person not to

84
00:10:12,080 --> 00:10:15,840
shower is has a lot worse than other thing any other thing you could tell

85
00:10:15,840 --> 00:10:18,480
them not to do

86
00:10:21,600 --> 00:10:29,040
well maybe not but you see this is culture culture can be good

87
00:10:29,040 --> 00:10:35,280
but it should never be mistaken for religion

88
00:10:35,280 --> 00:10:39,040
religion being the views and

89
00:10:41,680 --> 00:10:46,560
being the content behind the reasons why you're doing it

90
00:10:46,560 --> 00:10:51,200
our rituals should not be more important than the purpose for them

91
00:10:51,200 --> 00:10:59,120
so but anyway that mostly is about culture on the

92
00:10:59,120 --> 00:11:03,600
conventional level but I thought it's interesting because it boils down it

93
00:11:03,600 --> 00:11:11,360
ties into a person the inner workings of a person's mind

94
00:11:14,480 --> 00:11:18,320
we come into the meditation practice with an identity

95
00:11:18,320 --> 00:11:25,760
so much cultural baggage even if we think we don't and often we think we don't

96
00:11:25,760 --> 00:11:29,440
Sri Lankan people don't think they are specifically

97
00:11:29,440 --> 00:11:35,840
somehow odd for being Sri Lankan it's just the ordinary way of being

98
00:11:35,840 --> 00:11:40,880
except when you're not a Sri Lankan or Thai or Canadian

99
00:11:40,880 --> 00:11:44,480
I like to think as a Canadian I don't have I'm not cultured but

100
00:11:44,480 --> 00:11:52,080
oh boy we have a real culture it's funny I was walking down the hallway and

101
00:11:54,080 --> 00:11:57,600
it's so much it's so wonderful as a Canadian because it's just

102
00:11:57,600 --> 00:12:03,520
it's our culture when when you turn because the hallways and these

103
00:12:03,520 --> 00:12:07,600
narrow hallways and all the students coming out of their classes

104
00:12:07,600 --> 00:12:11,120
and so if you turn a corner and there's your facing someone

105
00:12:11,120 --> 00:12:15,760
you say sorry and they say sorry and then you go on your way that's the

106
00:12:15,760 --> 00:12:19,360
culture we both apologize it's Canadian it really is

107
00:12:19,360 --> 00:12:25,120
every time and so it was funny I did this once and I said sorry

108
00:12:25,120 --> 00:12:29,760
and this she was Asian if you had Asian features

109
00:12:29,760 --> 00:12:34,800
and she said she said something like

110
00:12:34,800 --> 00:12:38,960
it's okay or something like that but like it was

111
00:12:38,960 --> 00:12:42,240
and it was so it was so jarring but it was like she was trying to reassure me

112
00:12:42,240 --> 00:12:45,760
and I'm like no I mean I'm really not upset it's just this was what

113
00:12:45,760 --> 00:12:50,240
I didn't say that was just quick a quick exchange but it was the wrong exchange

114
00:12:50,240 --> 00:12:51,520
she gave the wrong answer

115
00:12:55,920 --> 00:12:59,200
so I'm definitely called we're all definitely cultured

116
00:12:59,200 --> 00:13:03,040
but for an Asian person or even an American this is absurd

117
00:13:03,040 --> 00:13:05,680
we didn't do anything why are you apologizing

118
00:13:05,680 --> 00:13:09,760
when I do it in America it's it's quite funny the response they're like

119
00:13:09,760 --> 00:13:16,880
what sorry but now they know you must be from Canada

120
00:13:18,240 --> 00:13:22,240
so we come with this this is who we are our personalities

121
00:13:22,240 --> 00:13:26,560
most of us are very much have a lot of a lot invested in our personalities

122
00:13:26,560 --> 00:13:29,600
it's very important to us

123
00:13:29,600 --> 00:13:36,240
when did we bring this to religion we say well I believe

124
00:13:39,040 --> 00:13:42,400
as a Buddhist it's quite irks from the here people say I believe as though it was

125
00:13:42,400 --> 00:13:46,000
meaningful it's like who cares and I really don't care what you believe it's

126
00:13:46,000 --> 00:13:51,920
meaningless I mean that's the modern society is all about what you believe

127
00:13:51,920 --> 00:13:56,800
it's about expressing your beliefs and having a plurality of beliefs

128
00:13:56,800 --> 00:14:01,520
Buddhism is like rubbish throw it all out

129
00:14:01,520 --> 00:14:05,680
whatever beliefs you could possibly espouse

130
00:14:06,480 --> 00:14:12,720
is meaningless now the Buddha was was quite pointed in this he said

131
00:14:15,040 --> 00:14:20,560
when you say I believe something what you're doing a good thing

132
00:14:20,560 --> 00:14:24,560
you're doing a good thing because you're not saying this is true

133
00:14:24,560 --> 00:14:29,680
if you say I believe x it's better than saying x is true

134
00:14:29,680 --> 00:14:34,080
that's not better it's it's less committed right

135
00:14:34,080 --> 00:14:38,960
but it's a clever thing that he did to say this I mean this is the point is that

136
00:14:38,960 --> 00:14:43,440
when I say I believe x it's really quite meaningless

137
00:14:43,440 --> 00:14:49,440
as far as x it's quite meaningful in terms of understanding you

138
00:14:49,440 --> 00:14:56,000
what a person believes has a lot more about them than than it does about what is true

139
00:15:01,040 --> 00:15:04,480
of course Buddhism is much more interested in what you've learned

140
00:15:04,480 --> 00:15:12,320
and what you know and by no we mean by having learned it by having seen it

141
00:15:13,120 --> 00:15:16,960
the emphasis on the term vipassana meaning to see clearly

142
00:15:16,960 --> 00:15:21,600
it's nothing to do with belief it's nothing to do with conjecture

143
00:15:23,120 --> 00:15:27,520
and consequently it's nothing to do with who you are it's nothing to do with your personality

144
00:15:29,920 --> 00:15:33,760
meditation should not reaffirm who you are it should tear it down

145
00:15:35,280 --> 00:15:38,960
it's a challenge the very core of who you are it's quite disconcerting

146
00:15:39,840 --> 00:15:42,000
to have your ego ripped out from under you

147
00:15:42,000 --> 00:15:46,960
but that's what meditation does that's what it's for

148
00:15:48,720 --> 00:15:51,840
and so I've often argued I think there's an argument that could be made that

149
00:15:55,280 --> 00:15:58,080
the only culture Buddhist should have is Buddhist culture

150
00:16:00,560 --> 00:16:01,760
and culture shouldn't be

151
00:16:01,760 --> 00:16:15,440
shouldn't be Canadian or American or Sri Lankan

152
00:16:18,800 --> 00:16:24,160
and I think it's too harsh because whenever whatever culture you're in

153
00:16:25,440 --> 00:16:28,640
when dealing with ordinary people you have to conform to their culture

154
00:16:28,640 --> 00:16:35,840
but I think that goes with this point is that a really good Buddhist can fit into any culture

155
00:16:35,840 --> 00:16:39,040
because the only culture they have of their own is Buddhist culture

156
00:16:43,600 --> 00:16:46,080
and I don't know what that means I don't know if that means anything

157
00:16:46,640 --> 00:16:48,480
Buddhist culture may mean no culture

158
00:16:50,240 --> 00:16:51,280
I think there are certain

159
00:16:51,280 --> 00:16:58,960
certain customs of the Buddha and customs of the followers of the Buddha

160
00:17:02,320 --> 00:17:09,840
but it's quite different from having your own mental culture your own personality

161
00:17:09,840 --> 00:17:18,400
and so when we all look at our

162
00:17:20,880 --> 00:17:24,000
when we look at our cultures when we look at our personalities

163
00:17:24,000 --> 00:17:29,120
we have to understand that none of this is at all related to the practice you can't bring

164
00:17:29,120 --> 00:17:41,760
this to the practice you can't practice to promote this or to enhance your own personality

165
00:17:43,200 --> 00:17:47,600
and in the end the clinging to personality gets in the way of our meditation practice

166
00:17:50,080 --> 00:17:53,680
so all this I'm practicing for this reason or that reason or

167
00:17:53,680 --> 00:18:00,160
I one meditator today said to me I prefer this as I think a good example

168
00:18:02,080 --> 00:18:06,160
because it's meaningless what you prefer it's like I don't really care what you prefer

169
00:18:06,880 --> 00:18:11,040
it has no bearing on your practice or should have no bearing on your practice whatsoever

170
00:18:11,040 --> 00:18:15,200
except the fact that you prefer it which is an interesting state and of itself

171
00:18:15,200 --> 00:18:22,880
and understand and so the real thing is asking ourselves what our personality means

172
00:18:24,160 --> 00:18:29,040
there may be aspects of our personality that are beneficial that are helpful useful for us

173
00:18:31,920 --> 00:18:39,040
but not because there are they should be good because they're good they shouldn't be good

174
00:18:39,040 --> 00:18:45,680
because there are and so we have to examine we don't have to reject who we are but we have to

175
00:18:45,680 --> 00:18:52,560
deconstruct it because it's only made up of moments moments of experience that become habits

176
00:18:53,680 --> 00:18:59,360
and like any habit they can be built up and they can be torn down what's important is to see

177
00:18:59,360 --> 00:19:06,000
clearly so we can just discern and differentiate the good habits from the bad habits that's what we do

178
00:19:06,000 --> 00:19:17,200
in meditation so I think we should all be quite careful in our our

179
00:19:20,560 --> 00:19:27,840
investigation and understanding our own culture and how we've been cultured and indoctrinated

180
00:19:27,840 --> 00:19:35,680
and what we take for granted a lot of people come to spirituality with baggage and with

181
00:19:35,680 --> 00:19:40,560
ideas about what meditation should be it should be comfortable it should be pleasing it should

182
00:19:40,560 --> 00:19:48,160
be calming should be easy it should be easy I think that's the worst one why should it be easy

183
00:19:53,440 --> 00:19:58,240
why should it be easy why should happiness be easy right this is what we know the greatest

184
00:19:58,240 --> 00:20:07,120
fallacy of of the modern world I think is that happiness should be easy and we act in such a way

185
00:20:07,120 --> 00:20:14,160
we think we're getting happiness all the time we're getting happier and happier why is this

186
00:20:14,160 --> 00:20:21,360
look at how easy it is to get happiness turn on the television its happiness turn on the computer

187
00:20:21,360 --> 00:20:30,640
its happiness turn on the music its happiness open the fridge its happiness lie in your bed its

188
00:20:30,640 --> 00:20:37,840
happiness so many happiness's so we all should be really really happy in society right

189
00:20:40,240 --> 00:20:46,720
we're not so funny because we think we are we pretend that we are if you ever asked us

190
00:20:46,720 --> 00:20:52,800
do these things lead to happiness you know do these things make you happy of course they do

191
00:20:55,920 --> 00:20:58,800
but I think if you ask the same people often are you happy

192
00:21:00,560 --> 00:21:07,200
I might not admit it but to you but be quite clear that they're actually not all that happy

193
00:21:07,200 --> 00:21:22,720
some of the most intent upon central gratification are often the most depressed even suicidal

194
00:21:27,440 --> 00:21:34,080
I went through a teenage years intent upon complete debauchery in all ways

195
00:21:34,080 --> 00:21:40,640
and I've never been so unhappy that was the most unhappy period of my life well it's the

196
00:21:40,640 --> 00:21:46,800
teen years as well but there's nothing happy about the debauchery about chasing after central

197
00:21:46,800 --> 00:21:50,000
pleasures of all kinds

198
00:21:50,000 --> 00:22:02,160
so anyway in this and in all things we should not take anything for granted

199
00:22:03,760 --> 00:22:08,480
it was fun I was when I was in Los Angeles I did this often with the Thai people and one man he said

200
00:22:09,680 --> 00:22:16,640
he caught me he caught the trend of my thinking someone would say something and I'd say are you sure

201
00:22:16,640 --> 00:22:23,360
I did this several times and he caught me and he said oh oh when he says are you sure you know

202
00:22:23,360 --> 00:22:30,880
there's a problem because then I would challenge it Thai culture can be very fixed and set in

203
00:22:30,880 --> 00:22:37,200
ways that it's not always beneficial you know Socrates I think would do this it's not that he

204
00:22:37,200 --> 00:22:44,480
didn't agree with you is that he he wondered why you why you took it for granted so he would

205
00:22:44,480 --> 00:22:51,360
ask you questions about it the Buddha would do this as well you don't have to tell people

206
00:22:51,360 --> 00:22:57,120
that what they're doing is wrong that's not the point I mean the bigger point is people don't

207
00:22:57,120 --> 00:23:19,040
question we often don't question we take things for granted this is right this is who we are

208
00:23:19,040 --> 00:23:24,240
people say if you eat meat it's just like killing so we have this big problem as Buddhists

209
00:23:24,240 --> 00:23:30,000
because we're not strictly vegetarians I'm terrified of Buddhism anyway to baton Buddhism as well

210
00:23:30,000 --> 00:23:40,800
I think well if you if you if you don't kill how can you eat meat things like that I mean

211
00:23:43,760 --> 00:23:47,520
why I bring that one up is because when you get into a conversation trying to explain it you

212
00:23:47,520 --> 00:23:51,920
can see that the way people look at things it's just so different and we the way we

213
00:23:51,920 --> 00:23:54,560
the way we look at the world

214
00:23:58,800 --> 00:24:03,600
well the way we look at the world is as Buddhist is quite different from the way ordinary people

215
00:24:03,600 --> 00:24:08,960
look at the world we're not concerned about people die about being dying we're concerned about

216
00:24:08,960 --> 00:24:15,040
the act of killing when I eat meat I'm not killing I'm not doing anything harmful

217
00:24:15,040 --> 00:24:22,960
so the I mean what this points to is the idea that in this example that

218
00:24:25,840 --> 00:24:31,120
ethics is something intellectual that you have to think about what's right and what's wrong

219
00:24:31,120 --> 00:24:35,600
Buddhism you don't think about what's right and what's wrong you just have a wholesome mind state

220
00:24:36,960 --> 00:24:40,240
when your mind is wholesome that's also that's ethical

221
00:24:40,240 --> 00:24:47,440
anyway lots of examples of it

222
00:24:51,360 --> 00:24:56,000
I thought this would be a good thing to point out to us how we take certain things for granted

223
00:24:56,000 --> 00:25:01,200
and how we come with come to the practice of all sorts of baggage that ties into this idea of

224
00:25:02,640 --> 00:25:05,760
meditation as being something separate from our personality

225
00:25:05,760 --> 00:25:12,480
because personality is all tied up with ego and identification and as you practice you start

226
00:25:12,480 --> 00:25:18,480
to tear that down you start to see it for what it is as a torture device something that tortures us

227
00:25:20,160 --> 00:25:28,000
by forcing us to take certain positions and to get angry and belligerent when people threaten our

228
00:25:28,000 --> 00:25:39,920
us or question us or I was thinking again about this guy in the wheelchair the first time when

229
00:25:41,040 --> 00:25:45,680
when the guy responded and that's really it there's always going to be people who do stupid

230
00:25:45,680 --> 00:25:59,200
things you know who act in belligerent ways but it's up to it the real problem comes when we

231
00:25:59,200 --> 00:26:09,360
when we respond we all have culture and our culture conflicts with each with other people

232
00:26:09,360 --> 00:26:14,560
and our culture conflicts with our experience meaning what we want things to be how we want

233
00:26:14,560 --> 00:26:22,240
things to be as often different from how they are and how what we want is often based on

234
00:26:22,880 --> 00:26:26,960
well it's usually based on our personality our habits including our culture

235
00:26:30,080 --> 00:26:33,760
and so we're always going to come up with these conflicts the question is how we react

236
00:26:33,760 --> 00:26:39,600
the Buddha said this for this reason when someone gets angry it's worse the worse

237
00:26:39,600 --> 00:26:46,800
it's bad to be angry of course but the person who replies with anger does a worse thing

238
00:26:50,960 --> 00:26:57,120
because they create the problem this is the way it goes with all things you're always going

239
00:26:57,120 --> 00:27:02,400
to have things like anger when you meditate you'll get angry at times you'll get upset at times

240
00:27:02,400 --> 00:27:10,400
that's bad it's not good it's it's stressful it causes you suffering but what's worse is when

241
00:27:10,400 --> 00:27:25,440
you reply to it get upset about it anyway these are some thoughts on culture and on the baggage

242
00:27:25,440 --> 00:27:33,280
that we come to the meditation practice with so quite useful to keep in mind that who we are

243
00:27:34,800 --> 00:27:40,880
who we are should have no impact on the meditation except to be studied as an object of

244
00:27:40,880 --> 00:27:49,200
meditation because we are the subject in in-site meditation with other meditations the subject

245
00:27:49,200 --> 00:27:56,560
or the object might be something external to ourselves no in this one we are studying ourselves

246
00:27:58,320 --> 00:28:04,960
we are both the lab technician and the lab rat

247
00:28:04,960 --> 00:28:17,200
so you learn about yourself you learn about your culture your views your beliefs and so on

248
00:28:25,600 --> 00:28:30,880
this is what leads us to freedom this is what leads us to peace

249
00:28:30,880 --> 00:28:36,000
once we're able to let go of who we are

250
00:28:40,880 --> 00:28:46,160
anyway so there you go there's the demo for tonight the few thoughts on culture

251
00:28:48,000 --> 00:28:55,760
thank you all for coming out apologies that we're still stuck in second life someone commented

252
00:28:55,760 --> 00:29:02,400
today that they don't like these second life videos oh there are some good sides to second life

253
00:29:02,400 --> 00:29:10,160
we've actually got a community here you guys can go it's nice to be able to see that i've got an

254
00:29:10,160 --> 00:29:14,240
audience you can do the real-time chatting

255
00:29:14,240 --> 00:29:28,240
well at the same time we are recording so people can watch on youtube or they can listen

256
00:29:28,240 --> 00:29:48,480
we have live audio and then the audio is recorded to the website

257
00:29:48,480 --> 00:29:53,760
so if anyone has any questions i'm happy to answer them if you want to comment if you have

258
00:29:53,760 --> 00:30:04,400
thoughts on culture of your own culture is such an interesting topic it's interesting when

259
00:30:04,400 --> 00:30:09,680
you've traveled the world and seen other people's cultures there's a difference between

260
00:30:10,720 --> 00:30:15,920
people who have left their own culture and people who haven't not not you know life changing

261
00:30:15,920 --> 00:30:21,680
different but there is something something you see when you've seen other people's cultures

262
00:30:21,680 --> 00:30:25,440
and realize how your own culture is not the only way people live

263
00:30:31,200 --> 00:30:35,600
it feels like we hold the terror about a culture apart from other types of Buddhism is this wrong

264
00:30:39,120 --> 00:30:43,520
well it's the you know there's a problem right because other people call themselves Buddhism and

265
00:30:43,520 --> 00:30:50,240
they teach things that we say well that's not that's not the same as what we teach

266
00:30:52,880 --> 00:31:02,000
i mean there is a problem sometimes where we yes we we do we cling to our school is being

267
00:31:02,000 --> 00:31:10,560
different from your school and then it becomes an identity right it's just another problem and

268
00:31:10,560 --> 00:31:18,960
so what you find is that the similarities we are blind to the similarities

269
00:31:21,360 --> 00:31:27,040
it's funny when sometimes when i meet other types of Buddhists you can just feel there's

270
00:31:28,400 --> 00:31:35,600
there's this sort of ego and i mean i even have have friends who it feels like they're looking

271
00:31:35,600 --> 00:31:41,200
down upon me because i'm at the wrong type of Buddhist you know and we do this as well and terror

272
00:31:41,200 --> 00:31:47,200
about a Buddhism is the only pure Buddhism yeah etc etc but the Buddha wasn't like that

273
00:31:48,080 --> 00:31:54,640
the Buddha was if people say thing he was about the teachings right

274
00:31:56,800 --> 00:32:01,280
if what they teach is true i agree with it what they teach is false that which they teach is true

275
00:32:01,280 --> 00:32:06,960
well i agree with that that which they teach is false well i disagree with that so in that

276
00:32:06,960 --> 00:32:12,880
sense we would differentiate ourselves based on the teachings i'm calling yourself a terror about

277
00:32:12,880 --> 00:32:19,440
a Buddhist is like a like all identities that it has a practical value because you don't have to

278
00:32:19,440 --> 00:32:28,240
explain i believe a b c d f g you can just say i'm a terror about a Buddhist and for many people

279
00:32:28,240 --> 00:32:34,800
that's already meaningful you know that's a set of beliefs already grouped together you're in a

280
00:32:34,800 --> 00:32:42,560
useful way so i mean with all things it's about how you're with things like this it's about

281
00:32:42,560 --> 00:32:49,920
how you use it and how you relate it as to having a terror about an actual culture though

282
00:32:49,920 --> 00:33:00,320
i mean there isn't one really there's a Thai Buddhist culture there's a Sri Lankan Buddhist

283
00:33:00,320 --> 00:33:08,160
culture and they're quite different there's a Cambodian and Laos are fairly similar to Thai

284
00:33:08,160 --> 00:33:22,640
they're still different and then there's Burmese so you know it's the few aspects of actual

285
00:33:22,640 --> 00:33:31,440
terror about a culture we might have are relating to our privileging of the one Buddha historical

286
00:33:31,440 --> 00:33:48,800
Buddha but not even so much the appreciation of the arahant as a as a fallen goal

287
00:33:51,440 --> 00:33:56,160
I mean just our Buddhism does terror about a Buddhism does have a specific flavor to it

288
00:33:56,160 --> 00:34:03,360
next semester i'm enrolled in introduction of Buddhism and today we just got this syllabus

289
00:34:04,640 --> 00:34:09,520
i don't know if i'm actually going to fall through with it because it says and i got to take

290
00:34:09,520 --> 00:34:13,920
the it's a teacher's a friend of mine so but he's a Tibetan Buddhist and so the syllabus says

291
00:34:13,920 --> 00:34:26,800
we'll be studying Buddhism as it's practiced in as it as it appeared in India Tibet China and Japan

292
00:34:26,800 --> 00:34:35,440
i think like they just missed a whole half the picture what about Southeast Asia there'll be

293
00:34:35,440 --> 00:34:42,720
nothing from Sri Lanka they'll miss that whole tradition of from Sri Lanka is that is that

294
00:34:42,720 --> 00:34:50,320
the idea here it's just going to be unfortunate i'd like to talk to him about it

295
00:34:56,880 --> 00:35:01,520
yeah terror about the Buddhism I guess does have its own culture some extent

296
00:35:07,120 --> 00:35:11,760
as i said i mean i think the real and the simple answer is that as with all cultures like

297
00:35:11,760 --> 00:35:16,640
Canadian culture i think there's some really good things about it but they shouldn't be good

298
00:35:16,640 --> 00:35:22,080
things because they're Canadian because i'm Canadian they should be good because they're good

299
00:35:22,080 --> 00:35:27,680
and there are a lot of things about Canadian culture that i might think as a Canadian are yes

300
00:35:27,680 --> 00:35:32,320
i'm proud of those things but then as a Buddhist i think wait a minute that's wrong

301
00:35:33,520 --> 00:35:38,880
like our penchant for drinking strong beer for example yes there are beers stronger than the

302
00:35:38,880 --> 00:35:48,160
Americans we're proud of that some Buddhist not so much so same goes with say teravada culture

303
00:35:48,160 --> 00:35:54,000
it's not that it's teravada that it's good it's good because it's good so we would say there

304
00:35:54,000 --> 00:36:01,040
are probably aspects of teravada culture that are good or at the very least harmless

305
00:36:01,040 --> 00:36:10,800
when there are others that could potentially be harmful

306
00:36:14,560 --> 00:36:17,360
guess if you want a better answer you'd have to give me something specific

307
00:36:17,360 --> 00:36:28,000
no other questions

308
00:36:37,280 --> 00:36:42,800
yeah culture that's a good topic i think good thing to think about there's aspects that tie

309
00:36:42,800 --> 00:36:48,240
into our own practice maybe tomorrow we'll take on science sciences the other of the three

310
00:36:48,240 --> 00:36:57,200
i have this trifectia or this triad science religion and culture three very interesting things

311
00:37:00,720 --> 00:37:02,240
tomorrow we'll attack science

312
00:37:02,240 --> 00:37:10,960
i suppose science is what i don't know we'll talk about science tomorrow

313
00:37:14,000 --> 00:37:15,520
and then we'll try to put it all together

314
00:37:18,720 --> 00:37:21,520
all right so have a good night everyone thank you all for tuning in

315
00:37:21,520 --> 00:37:31,520
we sure all good practice

