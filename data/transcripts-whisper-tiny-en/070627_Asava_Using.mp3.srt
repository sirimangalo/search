1
00:00:00,000 --> 00:00:15,720
So, today I'll continue with the talk on the Ahsava, the fountains which cause the mind

2
00:00:15,720 --> 00:00:21,680
to ferment, which cause the mind to go sour.

3
00:00:21,680 --> 00:00:29,360
And it's again simply a poetic way of describing the fountains which exist in our minds.

4
00:00:29,360 --> 00:00:34,560
The things in our mind which bring about suffering, which bring about discomfort, which

5
00:00:34,560 --> 00:00:36,640
bring about evil and wholesome states.

6
00:00:36,640 --> 00:00:44,640
All of those things which are evil and wholesome states, which exist in our hearts.

7
00:00:44,640 --> 00:00:52,400
And the third part of Lord Buddha's talk is on how to remove the Ahsava through what

8
00:00:52,400 --> 00:01:03,520
they say when they're through using, and this means using the requisite, the things which

9
00:01:03,520 --> 00:01:14,080
are required to keep us alive and to keep us living in a state of comfort and ease and

10
00:01:14,080 --> 00:01:20,040
peace.

11
00:01:20,040 --> 00:01:28,960
And these four things are these things which are requisite for meditators and for monks,

12
00:01:28,960 --> 00:01:31,600
for people who have left the home life.

13
00:01:31,600 --> 00:01:41,560
These things are a sign or a symbol of what it means to be one who has left the home life.

14
00:01:41,560 --> 00:01:46,040
So it's important, first of all, to understand why, what it means, what are the things

15
00:01:46,040 --> 00:01:58,800
that are truly necessary and how these things are used and how they will be, how they

16
00:01:58,800 --> 00:02:01,960
come into play in the Buddha's teaching.

17
00:02:01,960 --> 00:02:08,120
We came back to think of when the Lord Buddha himself first went forth before he was a Buddha

18
00:02:08,120 --> 00:02:15,960
when he was simply an unenlightened buddhisapva and he was searching for the truth.

19
00:02:15,960 --> 00:02:25,800
So the first thing he did was left behind his home life, and this is the first symbolic

20
00:02:25,800 --> 00:02:35,160
gesture which we undertake on the path to become free from suffering, whether it be

21
00:02:35,160 --> 00:02:41,320
as a monk or as a nun, this is the first symbolic gesture of the Lord Buddha was to cut

22
00:02:41,320 --> 00:02:50,840
up his hair, shave up his beard, give up his princely attire, and put on a set of rag

23
00:02:50,840 --> 00:03:02,200
loops, a set of rags or cast away cloth, and take up life in the forest without a home

24
00:03:02,200 --> 00:03:17,040
of possessions, and the life which the Bodhisattva and the Buddha to be lived with six years

25
00:03:17,040 --> 00:03:32,280
is marked by this lack of luxury, lack of possessions, objects which might lead to falling

26
00:03:32,280 --> 00:03:40,000
back into essential desire for essential pleasures.

27
00:03:40,000 --> 00:03:43,760
And so the Lord Buddha, when he became enlightened, he established for the monks in the

28
00:03:43,760 --> 00:03:47,200
nuns a way to live.

29
00:03:47,200 --> 00:03:52,000
And this is important because often we might think being a monk or being a nun is more

30
00:03:52,000 --> 00:03:57,480
about rules, about how many precepts you have in the rules which you have to follow

31
00:03:57,480 --> 00:04:05,120
which are different from ordinary people, but in truth in the time when the Lord Buddha

32
00:04:05,120 --> 00:04:10,760
went when the Bodhisattva went forth, from that time for many years the Lord Buddha had

33
00:04:10,760 --> 00:04:22,840
established no rules, and it was only in the time when one monk, mistakenly or deludedly,

34
00:04:22,840 --> 00:04:29,400
went back and slept with him, had sexual intercourse with his ex-wife as a monk.

35
00:04:29,400 --> 00:04:33,920
At that point the Lord Buddha decided that it was time to lay down some rules because

36
00:04:33,920 --> 00:04:42,960
the people who had come to be ordained as monks and his under his guidance were no longer

37
00:04:42,960 --> 00:04:45,440
completely pure.

38
00:04:45,440 --> 00:04:53,440
And so at that time he began to lay down rules as the cause arrived, as someone did something

39
00:04:53,440 --> 00:04:59,920
more and more inappropriate, the Lord Buddha would lay down more and more rules.

40
00:04:59,920 --> 00:05:06,320
But the key, we can see that these rules then are not the key to what it means to be a

41
00:05:06,320 --> 00:05:10,320
recloser, to what it means to be one who has gone forth.

42
00:05:10,320 --> 00:05:13,920
We can see that the key is the giving up of the home life.

43
00:05:13,920 --> 00:05:21,000
This is called Bapata, the going forth, or the leaving behind, and then going out from

44
00:05:21,000 --> 00:05:22,960
the home life.

45
00:05:22,960 --> 00:05:29,480
And so we leave behind all of the shelter of our parents, of our loved ones, of our

46
00:05:29,480 --> 00:05:39,840
beloved possessions, of our comfort and our safety, and our culture, and all of the things

47
00:05:39,840 --> 00:05:49,440
which are tying us down and keeping us from seeing objectively, things which lead us down

48
00:05:49,440 --> 00:05:58,440
the wrong path, and lead us and deceive us, or make us forget the truth of our lives.

49
00:05:58,440 --> 00:06:01,360
Make us forget the truth of suffering.

50
00:06:01,360 --> 00:06:11,680
Make us forget the need to find freedom, the things which intoxicate us, and lead us

51
00:06:11,680 --> 00:06:22,800
to get lost, and get stuck in the swamp, or get stuck in the rounds of rebirth, and

52
00:06:22,800 --> 00:06:27,160
the ocean of rebirth.

53
00:06:27,160 --> 00:06:31,800
When we leave all of these things behind, we have only according to the Lord Buddha's

54
00:06:31,800 --> 00:06:35,680
teaching, we have only four things which we keep us as our own.

55
00:06:35,680 --> 00:06:40,360
Of course, in this day I need there are many more things which we will use even as monks

56
00:06:40,360 --> 00:06:47,000
and nuns, but the point of these four things, even though even in the Buddha's time,

57
00:06:47,000 --> 00:06:49,960
there were many more things which the monks would use.

58
00:06:49,960 --> 00:06:57,000
The point of these four requisites is to hold these things up as the core of our life,

59
00:06:57,000 --> 00:07:03,560
as homeless people, that these four things are our way of living.

60
00:07:03,560 --> 00:07:07,120
That no matter what, we will always stick by these four things.

61
00:07:07,120 --> 00:07:14,120
So the first one is food, and the second one is clothing, the third one is shelter, and

62
00:07:14,120 --> 00:07:22,000
the fourth one is medicine, that in the end all of the things which we might use, even

63
00:07:22,000 --> 00:07:30,040
to the point now when we use computers, when we use vehicles, when we ride in vehicles,

64
00:07:30,040 --> 00:07:33,200
when we use telephones, or so on.

65
00:07:33,200 --> 00:07:36,720
Even though we might use all of these things, we don't consider them to be a part of

66
00:07:36,720 --> 00:07:37,720
our lives.

67
00:07:37,720 --> 00:07:44,360
There are things which we use maybe to spread the Buddha's teaching or to communicate

68
00:07:44,360 --> 00:07:53,720
or keep in touch with the rest of the world, which is changing and evolving in its own way.

69
00:07:53,720 --> 00:07:58,960
But we still maintain these four things as our training, as our way of life, which is

70
00:07:58,960 --> 00:08:02,480
indeed different from the way of life of other people.

71
00:08:02,480 --> 00:08:08,080
And the difference lies in these four things, the first being the food.

72
00:08:08,080 --> 00:08:12,840
As homeless people, we aren't able to go and buy our own food, we aren't able to choose

73
00:08:12,840 --> 00:08:14,840
what we want.

74
00:08:14,840 --> 00:08:26,200
We take it as a vow, or as a practice, that we accept whatever food we get, whatever food

75
00:08:26,200 --> 00:08:28,800
goes into our bowls, we eat that food.

76
00:08:28,800 --> 00:08:37,920
And we don't choose food because it's delicious or so on, we try to be accommodating

77
00:08:37,920 --> 00:08:41,080
and be unattached to our food.

78
00:08:41,080 --> 00:08:47,320
In the Lord Buddha, in fact, said that we should accept whichever scraps of food people

79
00:08:47,320 --> 00:08:51,960
put in our bowls, and he was referring to the arms round for monks nowadays, they still

80
00:08:51,960 --> 00:08:56,840
go through the village and go on arms, arms round.

81
00:08:56,840 --> 00:09:03,800
But it can also mean that we take whatever food is given to us, be it in the monastery

82
00:09:03,800 --> 00:09:05,800
or outside of the monastery.

83
00:09:05,800 --> 00:09:10,400
Of course monks are also allowed to take food in the monastery, but they have to reflect

84
00:09:10,400 --> 00:09:11,400
equally.

85
00:09:11,400 --> 00:09:18,360
They have to reflect wherever they get their food from, reflect that this is simply

86
00:09:18,360 --> 00:09:28,760
for continuing my life, for getting rid of hunger and avoiding overeating, for living

87
00:09:28,760 --> 00:09:34,600
in peace, for living in ease, for the continuation of this body, for the continuation

88
00:09:34,600 --> 00:09:38,600
of the holy life.

89
00:09:38,600 --> 00:09:42,840
And in this way, we won't be blamed more than you will be able to live in peace, we won't

90
00:09:42,840 --> 00:09:48,320
be burdened by food, but we won't be burdened in the way of other people as far

91
00:09:48,320 --> 00:09:57,360
as cooking, keeping, storing, and all of the other necessities which are required as a

92
00:09:57,360 --> 00:10:03,200
result of keeping, storing, and cooking food for ourselves.

93
00:10:03,200 --> 00:10:08,520
Instead, we take whatever donations and, of course, if we get no food, we don't eat and

94
00:10:08,520 --> 00:10:15,240
we are prepared and equally happy to live our day without food, if that if there is

95
00:10:15,240 --> 00:10:18,400
nothing being offered.

96
00:10:18,400 --> 00:10:24,000
This is the first requisite which we use it, and we use it in such a way that it doesn't

97
00:10:24,000 --> 00:10:30,280
lead to more defilements to arise in our minds, maybe we take food to be simply something

98
00:10:30,280 --> 00:10:34,480
which keeps us alive and allows us to continue practicing.

99
00:10:34,480 --> 00:10:35,480
This is the first one.

100
00:10:35,480 --> 00:10:42,480
The second one is clothing, and so as monks and nuns, we are separate from ordinary people

101
00:10:42,480 --> 00:10:52,840
in that we wear a simple, un-inexpensive cast-off piece of cloth, depending on the color

102
00:10:52,840 --> 00:10:58,120
nowadays you see many different colors for the monks and for nuns nowadays there are also

103
00:10:58,120 --> 00:10:59,920
many different colors.

104
00:10:59,920 --> 00:11:09,040
The color, of course, is not important, and in actuality we would simply dye it some dark

105
00:11:09,040 --> 00:11:18,320
off color, we would avoid colors which are beautiful, which are attractive, but whatever

106
00:11:18,320 --> 00:11:24,320
color the point is that it is a cast-off, it is a simple cloth which is inexpensive, and

107
00:11:24,320 --> 00:11:28,720
in fact for the monks they make it a point of cutting it into pieces first, and sewing

108
00:11:28,720 --> 00:11:35,840
it together in pieces, and for the, for some nuns you might also see the same thing.

109
00:11:35,840 --> 00:11:43,040
But in the end it is simply a piece of cloth which does the bare minimum of protecting us

110
00:11:43,040 --> 00:11:49,680
from heat, protecting us from cold, protecting us from the bite, and the sting of mosquitoes

111
00:11:49,680 --> 00:11:56,360
and insects, and all sorts of animals, small creatures.

112
00:11:56,360 --> 00:12:01,960
And of course for covering up the parts of the body, which are same for the parts of

113
00:12:01,960 --> 00:12:11,160
the body, which have evolved through time to be a part of, that part of life, which

114
00:12:11,160 --> 00:12:23,960
is, which is shameful, which is a cause for shame, which has to do with sexuality,

115
00:12:23,960 --> 00:12:29,480
which has to do with sensuality, according to the Lord Buddhist teaching and ancient

116
00:12:29,480 --> 00:12:33,680
times before the world was perfectly evolved.

117
00:12:33,680 --> 00:12:40,440
We were beings living outside of the earth, and we were beings like angels up in, if

118
00:12:40,440 --> 00:12:46,280
it's an outer space, and it was in a time before there were planets really evolved after

119
00:12:46,280 --> 00:12:49,480
the big bang, but before the earth had solidified.

120
00:12:49,480 --> 00:12:58,920
And at that time beings were neither male or female, and there were no sexual organs, reproductive

121
00:12:58,920 --> 00:12:59,920
organs.

122
00:12:59,920 --> 00:13:05,160
But at the time when the earth solidified and beings began to become certain coarser and

123
00:13:05,160 --> 00:13:13,000
began to ingest and consume coarser and coarser food to the point where they evolved

124
00:13:13,000 --> 00:13:20,320
into the species which we now find on earth, they had also developed craving for sensuality

125
00:13:20,320 --> 00:13:28,560
and eventually what became known as sexuality or the copulation between male and female

126
00:13:28,560 --> 00:13:35,120
or nowadays, even between male and male and female and female, and in between animals

127
00:13:35,120 --> 00:13:39,000
of different species even to this day.

128
00:13:39,000 --> 00:13:44,120
And this is brought about by those parts of the body, certain parts of the body, which

129
00:13:44,120 --> 00:13:56,880
as Buddhist monks or nuns, we cover up and we keep closed and we try to move away from

130
00:13:56,880 --> 00:14:01,640
and not let them become a part of our lives, so that those parts of our body which have

131
00:14:01,640 --> 00:14:08,760
evolved to become a part of this rebirth cycle, so that we will again and again and

132
00:14:08,760 --> 00:14:15,920
again be attached to whatever form of being we come to be born as, we come to do away

133
00:14:15,920 --> 00:14:23,760
with this and to become free from this desire and this necessity, this addiction to these

134
00:14:23,760 --> 00:14:24,760
parts of the body.

135
00:14:24,760 --> 00:14:33,800
And of course these parts of the body tend to be kind of be repulsive and disgusting

136
00:14:33,800 --> 00:14:38,040
and they cause for shame.

137
00:14:38,040 --> 00:14:44,840
And this is the second item which we use, the second requisite which we use and by using

138
00:14:44,840 --> 00:14:51,400
it in this way we don't use it for beautification or for provoking sexual desire or sensual

139
00:14:51,400 --> 00:14:53,320
desire and other people.

140
00:14:53,320 --> 00:15:01,120
We don't use it as a means for beautifying or intoxicating ourselves or other people.

141
00:15:01,120 --> 00:15:05,560
And in this way we are able to do away with greed, with the desire, with the lust which

142
00:15:05,560 --> 00:15:10,600
might arise in our minds otherwise, and when we are able to do away with this lust, of

143
00:15:10,600 --> 00:15:16,200
course we live at peace, we live at ease, we live without the fire, without the burning

144
00:15:16,200 --> 00:15:32,240
fire inside the need and the success of the eventual suffering of the refuted desire, not

145
00:15:32,240 --> 00:15:42,240
getting what we want the suffering which comes when we are able to obtain the thing which

146
00:15:42,240 --> 00:15:46,360
we desire.

147
00:15:46,360 --> 00:15:49,680
This is the second requisite which we use.

148
00:15:49,680 --> 00:15:56,480
The third requisite is shelter and so of course everyone in the world needs some sort of

149
00:15:56,480 --> 00:16:08,040
shelter to keep them free from the elements and the requirements for shelter are that

150
00:16:08,040 --> 00:16:16,400
it should keep us free from heat, free from cold, free from the bite and the sting of mosquitoes

151
00:16:16,400 --> 00:16:24,840
and other small creatures and most importantly that it should bring about a state of

152
00:16:24,840 --> 00:16:36,400
seclusion that we should not be interrupted or we should not be bothered by society or

153
00:16:36,400 --> 00:16:45,200
by other people, that we should not have to engage as a result of our dwelling in conversation,

154
00:16:45,200 --> 00:16:47,920
in interaction with other people.

155
00:16:47,920 --> 00:16:53,200
For the purpose that we might come to understand ourselves we might come to know and

156
00:16:53,200 --> 00:16:59,080
to realize the truth about ourselves in the world around us.

157
00:16:59,080 --> 00:17:08,800
And so we use shelter simply as a means of creating a conducive management where as

158
00:17:08,800 --> 00:17:14,400
other people might use shelter for the purpose of doing bad things or for the purpose

159
00:17:14,400 --> 00:17:20,600
of luxury or the purpose of indulgence and disorder that pleasure.

160
00:17:20,600 --> 00:17:27,400
The Lord Buddha recommended that we live at the foot of a tree, that we go and find a tree

161
00:17:27,400 --> 00:17:36,440
with big leaves and with good shelter from the sun and the rain and we live at the foot

162
00:17:36,440 --> 00:17:40,640
of the tree and practice meditation either walking or sitting at the foot of the tree.

163
00:17:40,640 --> 00:17:45,720
And in this way it was that the Lord Buddha himself became enlightened so it's considered

164
00:17:45,720 --> 00:17:56,920
to be the, the, the, the, the bare necessity for shelter is standing the foot of a tree.

165
00:17:56,920 --> 00:18:02,800
The Lord Buddha also allowed us, allowed monks and nuns to live in, in, huts to live in

166
00:18:02,800 --> 00:18:03,800
buildings.

167
00:18:03,800 --> 00:18:09,960
He said this problem as long as we use these things and understood how to use them.

168
00:18:09,960 --> 00:18:14,840
So when we use these things correctly when we use them for their correct purpose and when

169
00:18:14,840 --> 00:18:20,160
it wouldn't use our meditation, how does a place for social gathering or as a place to

170
00:18:20,160 --> 00:18:29,400
accumulate belongings or as a place to indulge in secret activities and secret indulgences

171
00:18:29,400 --> 00:18:31,400
or breaking preceptors or so on.

172
00:18:31,400 --> 00:18:38,440
We use it instead for seclusion and for as a place where we might be able to practice

173
00:18:38,440 --> 00:18:45,080
again with us and I can decide and to see clearly about ourselves and the world around us.

174
00:18:45,080 --> 00:18:48,800
Then in this way we'll be able to become free from our attachment.

175
00:18:48,800 --> 00:18:54,240
Again, this is important as we have to consider ourselves to be homeless, like we might

176
00:18:54,240 --> 00:18:58,840
have a kutti, we might have a hut, we might have a place to live but we don't hold on

177
00:18:58,840 --> 00:19:03,880
to it or take it to be me or to be mine or we don't use it in a way that an ordinary person

178
00:19:03,880 --> 00:19:09,400
might use it having all of the many belongings which ordinary people might have.

179
00:19:09,400 --> 00:19:23,960
We use it instead for the purpose of practicing and keeping all sorts of dhamma books or

180
00:19:23,960 --> 00:19:32,120
books on the Lord Buddha's teaching or so on but not for the purpose of any sort of indulgence

181
00:19:32,120 --> 00:19:36,920
and worldly affairs.

182
00:19:36,920 --> 00:19:38,200
This is the third requisite.

183
00:19:38,200 --> 00:19:47,160
The fourth requisite which is a method through using it we can become free from the Asava

184
00:19:47,160 --> 00:19:49,400
is medicine.

185
00:19:49,400 --> 00:19:54,880
The medicine is very important because nowadays we find that medicine is actually being

186
00:19:54,880 --> 00:20:03,080
used to as a means to avoid the necessity of purifying our minds, avoid the necessity

187
00:20:03,080 --> 00:20:06,720
of practicing and nowadays it even goes beyond medicine.

188
00:20:06,720 --> 00:20:18,200
It's come to all sorts of many different means nowadays there are meditation tapes and

189
00:20:18,200 --> 00:20:28,200
meditation videos and meditation sounds, meditation techniques which allow us to avoid

190
00:20:28,200 --> 00:20:30,960
the necessity of purifying our minds.

191
00:20:30,960 --> 00:20:37,240
I've had many people ask me about these tapes and videos and even just audio CDs with

192
00:20:37,240 --> 00:20:44,520
sounds which create a meditative state of enlightenment they say.

193
00:20:44,520 --> 00:20:48,880
When these fall into the same category as some sort of medicine these are not to be something

194
00:20:48,880 --> 00:20:58,280
which will cure the two of us of our suffering so whether it be a pill which we ingest

195
00:20:58,280 --> 00:21:06,280
or whether it be a tape which we listen to or whether it be any sort of mechanism which

196
00:21:06,280 --> 00:21:12,760
is meant to bring about eternal peace and happiness.

197
00:21:12,760 --> 00:21:17,240
We have to understand the benefit of these things and the limit of these things, the

198
00:21:17,240 --> 00:21:22,440
benefit is that they do bring peace and happiness even many different kinds of medicine

199
00:21:22,440 --> 00:21:28,280
or verbal medicine or even Western medicine can bring about freedom from sickness, freedom

200
00:21:28,280 --> 00:21:35,240
from discomfort, acupuncture, massage and so on.

201
00:21:35,240 --> 00:21:39,320
But none of these things can claim to be eternal because they can always be over-rided

202
00:21:39,320 --> 00:21:46,280
by a future action and since we can heal our back only to go out and heal our back from

203
00:21:46,280 --> 00:21:52,400
any kind of back ache or back problem only to go out and throw our back out again or hurt

204
00:21:52,400 --> 00:21:57,200
our back in a different way or when we pass away from this life to be reborn somewhere

205
00:21:57,200 --> 00:22:03,280
else with the same bad back depending on the karma which we have performed in bad

206
00:22:03,280 --> 00:22:04,280
lives.

207
00:22:04,280 --> 00:22:09,080
That's an example when we have sicknesses in this way or that way and we find a way to overcome

208
00:22:09,080 --> 00:22:10,080
them.

209
00:22:10,080 --> 00:22:14,040
We can only hope to ever hope to have come them to the point where we pass away from

210
00:22:14,040 --> 00:22:15,040
this life time.

211
00:22:15,040 --> 00:22:20,080
When we are born again we will create a new body, the new body will be created based

212
00:22:20,080 --> 00:22:24,120
on these very same structures which exist in our mind.

213
00:22:24,120 --> 00:22:31,960
These very same genetic structures which we have built up and are carrying forth with us

214
00:22:31,960 --> 00:22:36,920
even to the point of death and beyond.

215
00:22:36,920 --> 00:22:41,880
And so it's important to realize that anything which brings about a state of peace and

216
00:22:41,880 --> 00:22:47,360
comfort or happiness in the body or in the mind that it can't hope to be eternal.

217
00:22:47,360 --> 00:22:54,440
It can't hope to be permanent without something else which is permanent and the permanent

218
00:22:54,440 --> 00:22:57,760
cure in this case is wisdom.

219
00:22:57,760 --> 00:23:05,840
When we see and when we know the truth about suffering and the cause of suffering then

220
00:23:05,840 --> 00:23:11,480
anything which comes to us whether it be an unpleasant sensation in the body or an unpleasant

221
00:23:11,480 --> 00:23:16,320
sensation in the mind will be able to deal with it and understand what are the causes

222
00:23:16,320 --> 00:23:23,280
and conditions of suffering that when we see or hear or smell or taste or feel something

223
00:23:23,280 --> 00:23:35,440
unpleasant that it is the consecutive, the next step which is our

224
00:23:35,440 --> 00:23:41,520
craving, our attachment, our desire for it to be this way or that way for something to

225
00:23:41,520 --> 00:23:46,320
continue on or for something to cease, it is that itself which leads to suffering not

226
00:23:46,320 --> 00:23:48,160
the object itself.

227
00:23:48,160 --> 00:23:53,680
The objects of the sense are simply things which arise and cease and have no inherent

228
00:23:53,680 --> 00:23:59,240
power to make us suffer and they come to see that sickness or affliction whether it exists

229
00:23:59,240 --> 00:24:04,360
in the body or in the mind that it can't actually lead us to suffer because it itself

230
00:24:04,360 --> 00:24:14,200
is not a cause for suffering, the cause for suffering is in our reaction to this thing.

231
00:24:14,200 --> 00:24:19,480
And so the Lord Buddha has prescribed that we use medicine for the purpose of bringing

232
00:24:19,480 --> 00:24:24,120
about coming for doing away with painful feelings which might get in the way of meditation

233
00:24:24,120 --> 00:24:26,680
practice.

234
00:24:26,680 --> 00:24:34,080
But in the end we use them simply for what they are, we use them as a means to continue

235
00:24:34,080 --> 00:24:38,520
our practice, that if there is something which is getting in the way of our practice

236
00:24:38,520 --> 00:24:44,560
or would possibly get in the way of our practice in the future that we take whatever

237
00:24:44,560 --> 00:24:51,400
means necessary to prevent it or to ameliorate the condition, to allow us to continue

238
00:24:51,400 --> 00:24:58,000
on our path to become free from suffering and this is the fourth necessity.

239
00:24:58,000 --> 00:25:01,360
So these four things, this is what the Lord Buddha called Bhati you see, you will not

240
00:25:01,360 --> 00:25:06,080
have to apply that by using these four things in the correct way, the four requisites

241
00:25:06,080 --> 00:25:13,160
of a Buddhist monk or nun, that these can be a means of becoming free from the Asava.

242
00:25:13,160 --> 00:25:17,960
This is a means which we should try whether we are monks or nuns or people living in

243
00:25:17,960 --> 00:25:18,960
the world.

244
00:25:18,960 --> 00:25:24,240
We should try to emulate this as we can, that there may be if we are a lay person living

245
00:25:24,240 --> 00:25:28,800
in the world that may be very difficult for us to come to this state.

246
00:25:28,800 --> 00:25:36,000
We can't go around wearing rag robes, we can simply eat the food from Armstrong, we can

247
00:25:36,000 --> 00:25:46,360
live at the foot of a tree or in the forest as a monk or an nun, we can live our lives

248
00:25:46,360 --> 00:25:55,360
in the same exact way, but we can still emulate the life of a monk or a nun.

249
00:25:55,360 --> 00:26:01,640
We can try our best to give up the things which are not necessary for us using all of

250
00:26:01,640 --> 00:26:07,640
the things around us, which we find ourselves caught up in, simply for the purpose which

251
00:26:07,640 --> 00:26:10,640
they are meant for and nothing more.

252
00:26:10,640 --> 00:26:18,960
Not letting them intoxicated or create states of central desire, central lust, which would

253
00:26:18,960 --> 00:26:25,560
later be a cause for action and subsequent suffering for us, that whatever food we eat,

254
00:26:25,560 --> 00:26:28,840
we eat it mindfully and we simply use it.

255
00:26:28,840 --> 00:26:34,000
In the end, of course, it comes down only to mindfulness, whatever we use when we put on

256
00:26:34,000 --> 00:26:39,440
our robes, we put them on mindfulness, when we wear the mindfulness, when we like them

257
00:26:39,440 --> 00:26:43,520
or we attach to them or when we don't like them, when we become aware of this state of

258
00:26:43,520 --> 00:26:50,000
mind that it's something which is arising in us and is impermanent, is suffering itself

259
00:26:50,000 --> 00:26:54,880
and not holding on to them, and when we're able to let go in this way, when we're able

260
00:26:54,880 --> 00:27:00,280
to see clearly in this way about the things which we use and not becoming attached or

261
00:27:00,280 --> 00:27:06,920
excited or repulsed by them, simply using them for the purpose which they're meant for,

262
00:27:06,920 --> 00:27:10,080
and we can become free from our attachments to this world.

263
00:27:10,080 --> 00:27:14,840
It's another important point, is that not only are we dealing with the six senses, we're

264
00:27:14,840 --> 00:27:20,120
also dealing with conceptual things, the things which we use, one which we wear, which

265
00:27:20,120 --> 00:27:29,760
we consume, which we reside in, we're using them for the correct purpose and nothing else.

266
00:27:29,760 --> 00:27:35,120
When we use them and consider carefully as we use them, then we will find ourselves

267
00:27:35,120 --> 00:27:40,440
using them in the correct way and in a way that will lead us to peace and to happiness

268
00:27:40,440 --> 00:27:44,200
and freedom from suffering in this very life.

269
00:27:44,200 --> 00:27:50,320
So this is another important point for us to keep in mind in our practice, and that's

270
00:27:50,320 --> 00:28:20,160
all happening now, and do some actual practice, and we'll be walking in some way to go.

