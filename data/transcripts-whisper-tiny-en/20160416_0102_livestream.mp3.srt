1
00:00:00,000 --> 00:00:06,000
Good evening, everyone.

2
00:00:06,000 --> 00:00:21,000
We're broadcasting live from New York City, April 15, 2015.

3
00:00:21,000 --> 00:00:41,000
Today's quote,

4
00:00:41,000 --> 00:00:50,000
The fool may be known by his deans, likewise the wise one may be known by his deans.

5
00:00:50,000 --> 00:00:58,000
Wisdom is manifested by one's deans.

6
00:00:58,000 --> 00:01:08,000
I'm assuming the word deans is a translation of karma, a karma.

7
00:01:08,000 --> 00:01:15,000
And so you have to be somewhat careful about what is being said here.

8
00:01:15,000 --> 00:01:26,000
But isn't actually saying physical deans is saying their mental volition,

9
00:01:26,000 --> 00:01:31,000
the reasoning behind everything they do and say,

10
00:01:31,000 --> 00:01:37,000
the intention.

11
00:01:37,000 --> 00:01:51,000
Because people can do great things, fools can do good deeds.

12
00:01:51,000 --> 00:01:57,000
You can't know the difference unless you're able to tell the intentions behind their deeds.

13
00:01:57,000 --> 00:02:02,000
So a fool might be charitable and helpful to others,

14
00:02:02,000 --> 00:02:08,000
but they may do it for selfish reasons.

15
00:02:08,000 --> 00:02:11,000
And that's a foolish thing to do.

16
00:02:11,000 --> 00:02:15,000
So you couldn't say that person is wise,

17
00:02:15,000 --> 00:02:25,000
just because they're just because they're doing good deeds now.

18
00:02:25,000 --> 00:02:36,000
It is a good sign if a person is doing and saying good things often.

19
00:02:36,000 --> 00:02:40,000
It's a good sign that they probably have good intentions,

20
00:02:40,000 --> 00:02:47,000
but as we've seen before with some of the teachings that

21
00:02:47,000 --> 00:02:55,000
it's possible for a person to think one thing, say one thing,

22
00:02:55,000 --> 00:03:03,000
and do something else.

23
00:03:03,000 --> 00:03:10,000
So you have to be somewhat circumspect.

24
00:03:10,000 --> 00:03:17,000
But what he's saying here is if someone says they're enlightened

25
00:03:17,000 --> 00:03:26,000
or says they is able to spout the Buddha's teachings,

26
00:03:26,000 --> 00:03:33,000
the talk is cheap, as soon as whether they're actually practicing.

27
00:03:33,000 --> 00:03:37,000
But if they're not moral, if they're not focused,

28
00:03:37,000 --> 00:03:46,000
seeing clearly that's how you know them to be foolish.

29
00:03:46,000 --> 00:03:54,000
Even though they talk a good game, that's talk is cheap.

30
00:03:54,000 --> 00:03:56,000
Much easier to teach others.

31
00:03:56,000 --> 00:03:58,000
Much easier to...

32
00:03:58,000 --> 00:03:59,000
It's a skill.

33
00:03:59,000 --> 00:04:01,000
To teach is a skill.

34
00:04:01,000 --> 00:04:05,000
But once you've learned it, that's all it takes.

35
00:04:05,000 --> 00:04:08,000
You can teach.

36
00:04:08,000 --> 00:04:19,000
But to actually practice, this is the challenge.

37
00:04:19,000 --> 00:04:22,000
So I'm happy to be here in New York.

38
00:04:22,000 --> 00:04:29,000
I'm going to have several sessions that are mainly practice-based,

39
00:04:29,000 --> 00:04:36,000
which is the best.

40
00:04:36,000 --> 00:04:38,000
We'll just be a lot of talk.

41
00:04:38,000 --> 00:04:42,000
We'll be talking about meditation and teaching meditation

42
00:04:42,000 --> 00:04:55,000
and practicing meditation together all the way.

43
00:04:55,000 --> 00:05:04,000
We're going to teach the Bhakshati, the Thatitam Hino, Bhamidata.

44
00:05:04,000 --> 00:05:06,000
It is karma that...

45
00:05:06,000 --> 00:05:11,000
It is our deeds that...

46
00:05:11,000 --> 00:05:17,000
the vibes are soft, the vibes are beings.

47
00:05:17,000 --> 00:05:26,000
But the most important division between all beings, not just humans.

48
00:05:26,000 --> 00:05:29,000
It wasn't race.

49
00:05:29,000 --> 00:05:31,000
It isn't of course social status.

50
00:05:31,000 --> 00:05:37,000
It doesn't color the skin.

51
00:05:37,000 --> 00:05:42,000
It isn't education.

52
00:05:42,000 --> 00:05:48,000
It doesn't gender.

53
00:05:48,000 --> 00:05:50,000
It doesn't size.

54
00:05:50,000 --> 00:05:51,000
It's karma.

55
00:05:51,000 --> 00:06:00,000
The Thatitam Hino, Bhamidata, that makes us refined and coarse.

56
00:06:00,000 --> 00:06:06,000
So in the history of humanity, for example,

57
00:06:06,000 --> 00:06:21,000
we've often had the idea that certain groups of humans are more refined.

58
00:06:21,000 --> 00:06:23,000
Like that.

59
00:06:23,000 --> 00:06:29,000
That was at the Romans.

60
00:06:29,000 --> 00:06:38,000
All these other groups of people were barbarians.

61
00:06:38,000 --> 00:06:44,000
The Europeans who went to study Indian religion,

62
00:06:44,000 --> 00:06:54,000
they thought these Indians were in the found savages.

63
00:06:54,000 --> 00:07:01,000
So they thought they were quite surprised to learn about the quite sophisticated religions of India.

64
00:07:01,000 --> 00:07:06,000
Because people were living like savages.

65
00:07:06,000 --> 00:07:09,000
They thought these people are coarse.

66
00:07:09,000 --> 00:07:16,000
So they had this theory that Asia has been in decline.

67
00:07:16,000 --> 00:07:22,000
It used to be a refined place, but it's become savage.

68
00:07:22,000 --> 00:07:24,000
The humans do this.

69
00:07:24,000 --> 00:07:26,000
The Asians, of course, they can dial in.

70
00:07:26,000 --> 00:07:31,000
They think of Western Europeans as barbarians and savages.

71
00:07:31,000 --> 00:07:40,000
It's all across the board.

72
00:07:40,000 --> 00:07:45,000
But it said, well, this is industrialization, modernization,

73
00:07:45,000 --> 00:07:52,000
sophistication, all these things. This is not how you call someone refined, of course.

74
00:07:52,000 --> 00:08:01,000
We find our courses in our actions, our ethics, our performing ethical actions.

75
00:08:01,000 --> 00:08:07,000
Are we ethical? Or are we unethical, basically?

76
00:08:07,000 --> 00:08:16,000
The quality of our behavior.

77
00:08:16,000 --> 00:08:18,000
Does our meditation such a great thing?

78
00:08:18,000 --> 00:08:22,000
Because meditation is the purest form of behavior.

79
00:08:22,000 --> 00:08:26,000
It cultivates such wholesome habits in the mind,

80
00:08:26,000 --> 00:08:30,000
and thereby in the body.

81
00:08:30,000 --> 00:08:34,000
It's an activity that is totally pure.

82
00:08:34,000 --> 00:08:45,000
If you practice it regularly, it's like injecting those of purity into your life.

83
00:08:45,000 --> 00:08:50,000
It's kind of surprising, I think, that when you begin,

84
00:08:50,000 --> 00:08:59,000
and you wonder, well, how is this helping me?

85
00:08:59,000 --> 00:09:03,000
Walking back and forth and sitting down?

86
00:09:03,000 --> 00:09:06,000
It's surprising how it does have an effect.

87
00:09:06,000 --> 00:09:09,000
I think, in the beginning, it's quite confusing as to, well,

88
00:09:09,000 --> 00:09:15,000
why is this doing such wonders to my surprising how it's changing?

89
00:09:15,000 --> 00:09:18,000
Because it's pure.

90
00:09:18,000 --> 00:09:24,000
We underestimate the power of a pure thought.

91
00:09:24,000 --> 00:09:26,000
With a pure mind.

92
00:09:26,000 --> 00:09:32,000
There's not just clear in that moment, it's creating clarity of mind.

93
00:09:32,000 --> 00:09:34,000
It's influencing our life.

94
00:09:34,000 --> 00:09:36,000
This is karma.

95
00:09:36,000 --> 00:09:38,000
It affects us.

96
00:09:38,000 --> 00:09:41,000
It changes us.

97
00:09:41,000 --> 00:09:47,000
Purity, it's like spending time washing your...

98
00:09:47,000 --> 00:09:54,000
Washing, cleaning your house, or washing your body.

99
00:09:54,000 --> 00:09:56,000
It has an effect, and then your pure.

100
00:09:56,000 --> 00:09:59,000
The pure in the mind, which is, of course,

101
00:09:59,000 --> 00:10:06,000
a far more value than a pure body or a pure clean house.

102
00:10:06,000 --> 00:10:14,000
So, meditation is a real test.

103
00:10:14,000 --> 00:10:19,000
Can someone meditate if they're able to meditate, if they...

104
00:10:19,000 --> 00:10:24,000
Are able to see the benefits of meditation?

105
00:10:24,000 --> 00:10:27,000
That's a test of whether they're refined.

106
00:10:27,000 --> 00:10:30,000
That's the best test.

107
00:10:30,000 --> 00:10:38,000
While they're able to accept and appreciate an activity that is pure.

108
00:10:38,000 --> 00:10:48,000
As opposed to being obsessed with impure behaviors and addiction.

109
00:10:48,000 --> 00:10:50,000
Anyway, I'll try to give...

110
00:10:50,000 --> 00:10:54,000
Try to come on for a few minutes every night just to say hello.

111
00:10:54,000 --> 00:10:57,000
It's good to see everyone still meditating.

112
00:10:57,000 --> 00:11:00,000
I'm gonna do my meditation right now, I think.

113
00:11:00,000 --> 00:11:25,000
Do my...

