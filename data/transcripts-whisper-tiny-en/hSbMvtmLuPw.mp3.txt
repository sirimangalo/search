short name. How does one help another not to be terrified of the thought of the ultimate
uselessness of all things? It's terrifying. Well, I mean you can talk about how actually
they are quite useful. Everything has a use. You know, like money has a use. Alcohol
has a use. Everything has its use. The question is whether those uses or those purposes
are beneficial or detrimental. So I did a video on this and it might take on it that I think
is whether it's perfectly, perfectly honest or not. It's quite useful. I mean, I just mean
some people might disagree, but the point being that when we say that the world has no
purpose or life, life, let's say life has no purpose, we're not. It's not something negative.
It's actually something liberating because the Christians, for example, say that the purpose
of life is belief in Jesus Christ, is to gain faith in Jesus Christ. And if that's the
case, then we're all really in big trouble, right? And we've got very little choice.
There's not much we can do with our lives except, you know, our lives become a question
of whether we're worshiping Jesus Christ or whether we're we have belief in Jesus Christ
or not, nothing else matters. You have no freedom, no, if, you know, if the what Muslims
are right, and then then belief in Allah is the only thing that matters. Trying to think
of other crazy religions, most of the Eastern religions are pretty non crazy, really. Hinduism,
Hinduism gets you stuck in whatever I don't know, not really. But as soon as you have a
purpose, the point is you have a purpose, you get stuck. If the purpose of life is to make
lots of money, then, you know, that's all you've got is whether you make money or not,
saying that there's no purpose in life is completely liberating. You can do what you want.
If you want to become a mass murderer, go for it. There's nothing intrinsically stopping
from doing it. The only problem with such a thing is that it goes against your own intentions.
You do it thinking it will make you happy or it will be a benefit to you. And it actually
is to your detriment. The fact that these things contradict each other, we're not worried about
purpose because we're trying to find benefit or clarity or simplicity and so on. So I think explaining
it in that way that it actually brings freedom and allows you to worry less about what is right
and what is wrong or what is the way and actually focus on what brings peace, happiness,
and freedom from suffering and what doesn't. What actually makes sense is maybe the best thing.
What makes sense supersedes the idea of having a predefined purpose,
which would of course be completely arbitrary. There's no reason for us to believe that faith
in Jesus Christ is any better than faith in Allah. Nobody looks like they have anything to say
on this. What would you do if someone was afraid of, you know, maybe we can be more general
being afraid of letting go, one meditator right now who is much better now. She said to me how
much better she feels and then I finally looked at her with this question in my mind and it was
amazing to think. What did she look like when she first came what did she look like now and you
can see that she's a different person. She's radiant, she's much more alive, much less a character
and much more a human being. But in the beginning she was very afraid of letting go, afraid of or
she had these questions about how can I convince myself to let go.
One classic answer that I would always give is that you never let go of the things that you
don't want to let go of. So maybe that goes along with purpose as well. If you believe that
x, y, or z is the purpose of life, then well don't believe us that it's not. But certainly
don't just take your own blind faith in it. If you want, if you believe that there's a purpose,
look at that purpose and try to understand it. If it's just a fear, if the person gets the
fact that there is no purpose to life, but they're just afraid of it, well then the best thing
is to look at the fear. Don't worry about whether there is or isn't a purpose. Look at this fear
that you have and try to understand what's there as opposed to what's not there.
I just think purpose has no place in an ultimate reality and it's a long and short of it.
Nobody else? Stop there.
