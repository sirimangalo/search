Good evening everyone.
Remember on casting live, March 25th.
Tonight's quote is about the mind.
It's from the Eka-kani-pata, the Book of Ones,
when you are good or any kind.
There's a whole section about the mind, Eka-dhamma.
Nahang-bikhuei, Nahang-bikhuei, Aun-yang-e kah-dhamma-m-pie,
Samonupasani.
I don't see one other, any other dhamma.
which is so, which, when untrained, undeveloped, is a
a company, a company, a company, a consumer, a camera, a car, something that you can or should
or should be able to their eyes, amenable to being done, the sense of being used, the
sense of workable, because the translation of our friend uses, so, something that is undeveloped,
there's nothing else when you neglect it, it's so useless, so harmful, so unwieldy.
But, luckily, in the next verse, now, a company, a company, a company, a company, a company,
I don't say one, the other, dhamma, young yewang bhavitan, that, which, when developed,
a company, a company, a company, is willy, malleable, like malleable, I think it's not
literal, but it gets the idea across, yata yidang bikhavitan, jitang bikhavitan, a company
yawang bikhavitan, the mind bikhavitan, because that is bhavitan, that is developed, bhav
means to be bhavitan means to become more to be to progress, so, bhavitan comes from
a company, young hote, hote is a company, a company used, and it goes on, there's more
there, nahang bikhavitan, so, and yang yewang bhavitan, mahato anataya, samwatati, there's no
one thing that, when undeveloped, works for the, for a great anataya, anataya means,
aataya means purpose or benefit, so, anataya means, not just lack of benefit at harm, really,
leads to so many things that are useless, leads to useless things, great uselessness,
maybe, great, but it's actually more like great, loss in the sense of harm, guess what
that one thing is, the untrained mind, jitang bikhavitan, when the mind is untrained, mahato
anataya samwatati, it works for the, great detriment, really, is what you see, but there's
nothing, when developed, that is, mahato anataya, samwatati goes for the great benefit, great
purpose, a bhato bhata, there's no bhato bhata, bhata mahato bhata, mahato bhata, mahato bhata, mahato
hmm I don't want you to get par to the next one, I didn't understand, the mind
which is developed, which is par to bhata na, it doesn't like the translation.
That's basically the same thing.
Yeah, the rest is pretty much the same.
It just has synonyms for bavita, which really means bavita and baholikha, which is a common
coupling made much of basically, or not made much of.
So the mind is that one thing that is of greatest use and greatest harm.
And it goes back to the first verse of the Dhammapada, which is easy to miss the significant
sense, but it's a minopubangamma, dhamma, minosita, minomaya, all things have mind as their precursor.
Mind as their chief, mind as their maker, all things, I guess better, all things are
preceded by the mind, are ruled by the mind, are formed by the mind.
I think there's some qualifier there because it's not, it's actually, there's a specific
context in terms of good dhammas and bad dhammas, like, in terms of morality and immorality,
goodness and evil, that kind of thing, maybe a specific type of dhamma, but all through
the Buddha's teaching, there runs this undercurrent, sort of the implicit understanding
that the mind is the base, that there is no universal reality outside of the mind.
This is just a characteristic of matter, space doesn't actually exist, it only comes
in the being when there's the concept of matter, it's not the most fundamental aspect
of reality, and more fundamental than that is the experience of the mind, and so the
mind becomes very important because the mind works according to orderly nature and
laws and then follows order and order, there's the order of the mind.
So if you do bad things, bad things follow, if you do good things, good things follow.
We don't think of the mind as all that useful or we don't, we underestimate, let's put
it that way, we underestimate the value of the mind in general, just talking generally
in society, and so when we use these things, these things have become our mind, rather
than remember things, we just put them in our calendar rather than study things, we just
rely on Google whenever we need to know something or Wikipedia, even our daily functioning
rather than organizing things in our mind, we've got them organized on our smartphone.
The smartphone is when you build a smartphone while, it's the most malleable thing
there is, you can swipe and you can touch and you can pinch, there's nothing better than
a smartphone, right?
So we've outsourced a lot of our brain power to some extent anyway.
Some people outsource more than others, I'm just thinking, some people aren't interested
in using their mind, other people are interested, and obviously some people are interested
in developing their mind through the practice of meditation.
Not a lot though, most of us don't, we underestimate the importance of the mind, usefulness
of the mind, and underestimate the power of the mind, and what is actually thinking
here, the kinds of things, what he's thinking, but he's got to be very much a part of
this is the powers of the mind to see the future, to remember past lives, to do all sorts
of crazy things, and to free oneself, the power of the mind to free oneself from suffering,
to become invincible, the power of the mind to let go of all causes for suffering, so
that nothing that is bothersome bothers one.
So our smartphones can be as useful as you like, but they're never going to rival the
mind in any way shape or form, I think that's fairly obvious, that it's a sad thing to
say that our smartphones are on the same level as our minds or our computers or anything
or iPod, iPad, nothing to do with the power of the mind to harm or to help, the mind
is what tortures us, mind is, and part of what he's saying is useless, the mind, the ordinary
mind is useless, what can you do with it, you try and work, you can board, you try and
study, you get distracted, you try and do good things, you get upset and annoyed and sad
and burnt out useless mind, but what he's saying is that, it's just because it's untrained,
it's not, you don't feel bad that your mind is, you don't feel bad about yourself that
your mind is this way or that way, that your mind is unknowable, unwieldy, you just have
to realize, well, it's through neglect and all you have to do is stop neglecting it to start
training it, working on it, doing something with it, you know, like people say, get off
the couch and go exercise, right, it's all you gotta do, except for meditation, it's more
like, get back on that cabbage and exercise, get back on that sitting mat, don't just
do something, sit there, funny things like that, but meditation is a strenuous exercise
for the mind and for the brain I think as well, there's end for the body, there's no
question, meditation is a very strenuous sort of thing, people lose a lot of weight meditating,
they don't need as much either, but it's a lot of sweating, a lot of straining, a lot
of crying, blood sweat and tears, the work that we do to train our minds, it's the most
rewarding work there is, you can train your body, okay, great, so you've got a body that
can do this or do that, they used to take karate, never really saw the point, I was just
in it because I was afraid of getting beat up, but when I really liked about karate
was the mental part and I got disgusted when we were sparring, this was late in the time
when I was close before I quit and the head guy in the dojo and I were sparring, actually
the sense was away, so he and I were sort of leading the class, I think, I was a brown
belt, I don't think I ever deserved the brown belt, but I was just there long enough
instead of eventually they gave it, and we were sparring and I was out of three points,
I got two points, I got, I can't remember, so you got first to three points, I got
two points on them and he freaked out and he just went ballistic on me, I said, Jesus man,
it's just a game and he just freaked out and he won, he actually won the mat and he was
really happy, but I lost all it, I was disgusted by the whole thing, and I think karate
is martial arts can, there's a point to me telling that is that it's all in the mind,
martial arts is a body training, part of it is a body training and it's possible to do martial
arts without proper mental training, so you have to train your mind somehow, you train
your mind to work with your body, but you haven't really trained your mind, and I think
the greatest martial arts, lots and lots of martial arts, teachers and practitioners would
say no, no, no, your mind has to be calm, anyone who freaks out like that or in a place
mind game, it's just awful, yeah, it's a really practicing martial arts, but that's the
point, the difference between training the body and training the mind, someone can be
very much trained in the body, but not have a very well trained mind, not to say, and
I'm not putting down martial arts, in fact, it's probably very good for disciplining the
mind, but it only goes so far, because it's not, it's a body thing, the only training you're
doing in your mind is kind of like a trick, how to keep your mind focused, how to keep
your mind disciplined, that kind of thing, it's not really training, it's not training the
mind in the way that the Buddha talked about, bahavita, Buddha's bahavana is something
quite different, you know, that's a kind of a bahavana, but it's very superficial, you know,
anyone, animals can train their minds to sit still, dogs can do it right, they go on their
point and something, it's not real training, where you train your mind to see things as they
are, you train your mind to be objective, you're never going to angry or upset, you
are totally up a root, the potential, change the nature of your mind in something much
deeper. Anyway, by the way, with martial arts, practitioners must agree that you need to
train your mind as well, mind is most important, oh, that's what our sensei was French,
French Canadian, I don't know if he doesn't anymore, but nice guy, and he used to say all
the time, he said, mind first and the body will follow, it's a really nice guy, he taught
us meditation, he was probably my first meditation teacher, mind first and the body will follow,
he was a good guy, he's a good guy, I've seen him in a long time. Anyway, there you go, that's
the dumb of her tonight, really good quote, where I remember him, there's a Buddha quote,
if ever you heard one, not nothing fake about that, there's the link to the hangout
if anybody wants to come on, posting it at meditation.seriumungala.org, you want to
come on if anybody has questions? I mean, people do send me questions, I think must be
every day, I don't keep track because I don't answer any of them, I guess so many questions.
And you know, often it's, my life is crappy, how can I fix it? And you know, some of
you know my opinion on these questions, it's not really, I don't have the answer, there's
no answer to that. I mean, or this person is not, person asking this question is probably
not going to, I mean they're not asking the right question, in my opinion really the
only right question is how do I meditate? The answer is read my book, but lots of questions.
People are having a difficult time, you know, but that doesn't mean I can put myself
in a position I answer all these questions. I can't, I'm not in a position to help any
of you. My booklets there, if you want to learn how to meditate, that's the answer, that's
the path. I'm not to be arrogant to meditate, that booklet, this booklets just one of
many, but it teaches what I was taught, and I believe that what I was taught works. I
don't believe, I claim that what I was taught is the Buddha's teaching and it's authentic
enough to need you to train your mind and for you from all of life's problems. It's
really that simple. It's not easy, but it's pretty simple. So, you know, a lot of questions
are either too unrelated to meditation or misguided in the sense that they're not being
stressed. I mean, there's a, they're looking for a pill, basically, right? Looking for an answer,
a quick fix. How can I fix my problems? No, you can't fix your problems. You have to
stop trying to fix things. That's the problem. That's one of the problems, one of the
big ones. So, I don't know. I'm just talking because people trying to get a hold of me and
I'm stubbornly holding on to the fact that I'm a Buddhist monk, as my excuse not to have
a 24-7 public presence. Something awesome happened that I'm assuming my, I'm pretty sure
many of you are aware of. Robin, are you out there? You're going to come on here and say
something? I don't know if I should say something. I can say it as well. No, Robin. Well,
as you all know, some of you know, I was dragging my feet on the idea of going back to Asia
this year, last year. But Robin thinks I should go and so she put together an online
campaign to get me a ticket without, well, maybe that'll work. And it didn't even take
24 hours and they've raised over $2,000. I think it was like more than it was actually
trying to raise. So, I guess I got to go to Thailand. And Sri Lanka. And here.
Who is that? I don't know. I don't know. I don't know.
Oh, me, school. I don't know. Hello.
Oh, it's Joanna. Yeah, you're enough to YouTube video before you join the hangout. Yeah,
I'm just getting the hang of it. You're talking about meditation. And I've been really sick.
I mean, I've been sick on and off for like over a month now. But I was, I went to the doctor
and I got, he told me I had pneumonia, which explains a lot. So, and I, that kind of threw
me for a loop. I just, anyway. And I know I should be meditating, but I'm having, I'm really
having a hard time meditating feeling so crappy part of my language. Do you have any suggestions
as to how to meditate when one is not, when one is ill? Well, yeah, I mean, it's still
a reality. So you can still meditate on it. Sickness is actually a good thing to meditate on.
But I don't know what. Okay. What about tomorrow on nothing? Tomorrow, tomorrow, nothing?
Okay, it's asking. Oh, no, you don't have to. But if you do, let me know. Otherwise,
I'll go out. I'll just go out. Let's go out every day. Hi, Robin.
I hear you. Hi, Funte. Can you hear me, okay? Yep. Sorry, I meditate upstairs, not too close
to the computer. So, wow. Yeah, that, that was awesome. We, I put out a small campaign
early this morning, mid, mid morning or so. And within a couple of hours, there was enough
support to bless here within a couple of hours. There was enough support to support your trip
to Thailand in Sri Lanka. So that's awesome. Just wanted to say thank you to everyone who
shared it, contributed to it, wished as well. That's awesome. Thank you, everyone. I can't
actually comment on giving money. So, no, that's why I'm commenting. I'm just saying it. I can't
say thank you. But I can say thank you. Thank you very much, everyone. So when do you think
I'm grateful for the ticket and appreciative of the ticket? That'll be a great thing because
I'll be able to go and promote our monastery to our teacher. There is someone who would like
to join the hangout. I'm just posting the link to him now.
Hello, let's have a
Bontay.
Bontay.
Yeah.
Did you finalize your trip to Manhattan or to New York?
Hello, what's there?
Hello, Robin.
Hello, Abi.
Welcome.
Thank you.
Thank you for your link.
I was finding difficult to join in Hangout.
Hi, I'm going to New York April 15th
and returning.
It's turning April 24th.
What venue will you be?
I don't know.
No, where will you be staying?
I think that this Sri Lankan temple
in Queens?
I don't know.
Are you going to let us know when you know
or you don't want people to visit you?
Absolutely.
You know, I'm not sure if they're waiting for me or not.
I'm, you know, this is finals.
I'm writing an essay on the Lotus Sutra.
I have to study for my peace studies exam.
I've got a lot and practice coming that I have to finish for Monday.
And then we've got the actual peace symposium
that we have to organize for April 5th.
And then right in that, you know, I'm going to be busy up
until I step on that plane.
Pretty much.
You're going to land in JFK.
Look, Wardia.
JFK.
You said Sri Lankan temple?
I'm sorry?
You said a Sri Lankan temple?
Yes.
They mentioned that.
I wonder if there's a lot of them in New York?
I think that's true.
I mean, I'll let you guys know if I know when I know.
But I mean, I'll probably put something on Facebook.
But I haven't earned anything yet.
I'm sure I'll have internet welcome there so you can email me or you can talk on.
You can let you know, Wardia.
That's cool.
Does anybody have a question?
Getting back to when you're sick and you're meditating,
should you do something differently?
Well, you can do lying meditation.
I mean, I want to do too much walking meditation, although if you can do it, it's good for the body.
You shouldn't push too hard.
It's not easy.
Yeah, well, I completely didn't do it yesterday.
I felt kind of bad.
I felt bad because my emotions were getting the better of me today.
It was probably because I didn't meditate.
And then today I was going to skip it because I just got lethargic.
But I think I'm going to just get off and then and just do the lies.
So the lies, you lay in the just lay down with your palms up.
Anyway, that you like just like comfortably in mind.
Rising, falling, watch the stomach.
Okay, thank you.
Good night.
Welcome.
Good night.
Hello.
Hello.
Hello.
Hello, went here.
Yeah.
Can I call you, call you on a telephone?
What's it about?
The first practice, what if you got this in practice?
Again, the meditation.
Yes, went in meditation.
Sure.
Yeah, you can call me.
It's about meditation.
I am having the problem for the internet connection.
That's why I'm choosing the option for a telephone.
Okay.
Is there any contact numbers on the website?
There is nothing.
I have posted a email on the website also.
Hmm.
Robin, do you have a number?
Or the monastery?
Yes, I think so.
Can you send a turn?
What to do?
What to do to remove the fear from your heart, from someone's heart?
Have you read my booklet on how to meditate?
Yes, I have read it.
Well, that's the practice that I recommend.
I guess I can say it's not about removing the fear from your heart.
That's not how it works.
It's about learning to see the fear objectively and to stop cultivating it.
It's just to have it.
And if you start to look at it in a new way, you'll stop cultivating the habit.
This is the only way to break the habit to observe the habit keenly, closely.
Well, as I lay out in the booklet, that's the meditation practice that I teach.
So when you're afraid, you would say to yourself, I'm afraid.
But it isn't just to sit in front of Anna Pranavati because...
This is the only question now.
If you read my booklet, I teach what's in the booklet.
I don't want to talk about Anna Pranavati.
Not what I teach, I'm sorry.
Sorry, I didn't really mean to interrupt you, but go ahead.
And then there's this Stephen person, Stephen person who's all black.
But there's someone there and they're listening to it.
Hi, Stephen.
What's up every night? It doesn't say anything.
I know it was smoking one day, which I'm not trying to be a smoking, but I have to put down.
No, they're good.
Probably smoking.
No smoking on a Buddhist hangout.
I don't know why.
I mean, it's not that it's not even bopping anti-smoking.
It's just like if you were to sit there eating while we were on the hangout.
I mean, you can make an exception for drinking water or something, drinking tea or juice or whatever.
You know something.
But...
It's a little bit like going to a monastery.
Rachel, Rachel, is it your monastery?
Yeah, I mean, I don't mean to make a big deal out of it, but I think it's a rule.
I think, dude, if you're going to come on the hangout,
you shouldn't be smoking during the hangout.
You're probably looking. You should take your hat off. I knew this.
You know, in university, the kids come to learn meditation with their hats on.
I tell them to take their hats off.
One gave her the cowboy hat.
They told them they said, one woman had a hat on and she wouldn't take it off.
She said, he was having a bad hair day.
Did you suggest she become a monk?
She wouldn't have to worry about bad hair days.
Well, I can shave that for you.
No.
No, there's some...
There's got to be some protocol.
It's a part of mindfulness.
Being mindful enough to be respectful,
to understand what it is that you're doing.
It's not...
There's lots of things like that. I don't eat.
I usually take my robe off when I'm in my room,
but I won't eat without something, without some kind of cloth on.
Adjantang said that.
He said, at least put your shoulder cloth on when you eat.
These people have given you this food.
I had a respect for their food,
because we often go around with just our lower robe on in our rooms.
That's a monk thing to do.
I mean, that's more of a tie thing than anything,
but it's the same as that sort of idea,
where you have protocol and respect.
It's a good thing.
People just like to do away with protocol and respect.
What's kind of like when you go to visit someone,
you wouldn't bring your own food instead of there
and eat your food in someone else's house.
But we don't visit each other too much anymore,
so we kind of forget these nice cities.
Anyway, I guess that's it for our hangout.
Well, come back.
When do you feel be going to Thailand and Sri Lanka?
Some people started to ask me.
Yeah, we should probably work that out.
I need to go to get a ticket probably for this day.
You know, the trip to New York was a mistake
because I actually have a meditator.
I had a meditator coming during that time,
and luckily he could change his ticket trip.
I have to be very careful about my bookings.
This is the problem.
Probably don't.
So the 28th of the moment is 8th.
Okay.
The 28th of May is the Buddha's birthday celebration
in Toronto, the Mississauga.
So I have to be there for that.
But I think fairly soon after that would probably be a good idea.
So if someone was potentially interested in going to meet you there
or something, probably June, would that be the best chance?
Or anything?
Today, I can't arrange that.
I'm sorry, that's not part of this trip.
Maybe I'm assuming in May I'll have more breathing room
and more time to think about that.
But for right now, please don't ask me about coming to Thailand with me.
I don't know what it's going to look like.
I'm going to have to sit down at some point
while I guess I'm going to have to give you guys some dates.
But probably just give you dates for Thailand
and I'm going to have to figure out Sri Lanka with Sangha.
I don't know.
I guess I can do a month.
Maybe just a month of June.
But I think I've already promised something in June or July.
I don't remember.
I have to check that out.
But that's actually probably not.
I can probably skip that if I have to.
Anyway, maybe the month of June will be in Asia.
Mid-July is already the rain.
I should be back while in time for that.
But that makes sense.
No, the whole of June.
Why am I thinking?
What's going on in June?
That probably works.
Something like June 1st, June 30,
it says September 1st.
June 1st, like 30th, something like that?
That's good.
I don't think there are any meditators booked that far ahead either.
So that works out well.
I think there's meditators through May.
So that works out well.
Right.
We have to check that.
Yeah, okay.
If nobody's booked to that works.
Oh, that's right.
There's the whole UK thing.
That was the monkey wrench.
I want me to go to UK.
I asked if a week would be okay and they said a week would be okay.
And we could be enough if that's all I can do.
So maybe on the way back from Thailand and Sri Lanka.
Would you have time to go to the UK then?
Before the rain.
Yeah, but then it's just lots.
It's just more time away than maybe I should cut back on the.
Well, no, I think a month in Asia is anything less than that.
It doesn't make a lot of sense.
Although, you know, I'm just going to see my teacher.
That would be a chance to take a break though.
Could hide.
I don't know.
Kind of in beautiful and probably shouldn't stay away too long.
Maybe I shouldn't go to Sri Lanka.
Maybe I'll talk to some good about that.
Okay, well, let's talk in the next few days to figure this all out.
You don't need to do this online.
All right, have a good night everyone.
Thank you, Bhantay.
Thank you.
