1
00:00:00,000 --> 00:00:29,000
Good evening everyone, broadcasting live April 7th.

2
00:00:29,000 --> 00:00:41,000
Today's quote, it's actually got quite a bit in it.

3
00:00:41,000 --> 00:00:46,000
You know, it's a simple message.

4
00:00:46,000 --> 00:00:57,000
This is from the Inti-Wutaka.

5
00:00:57,000 --> 00:01:07,000
The Buddha says, even should one, Sankar, Tikkarnay, J.P.B.Kari.

6
00:01:07,000 --> 00:01:14,000
Even if one should the corner of my robe,

7
00:01:14,000 --> 00:01:19,000
go ahead to a grass, take hold of you.

8
00:01:19,000 --> 00:01:23,000
Even if one having taken hold of the corner of my robe,

9
00:01:23,000 --> 00:01:32,000
pitito pitito anuban-do-assa should follow after me,

10
00:01:32,000 --> 00:01:36,000
pitito pitito follow behind my back.

11
00:01:36,000 --> 00:01:39,000
One after the other.

12
00:01:39,000 --> 00:01:49,000
Ade-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-pa-de-ton-ton-ton-tonton.

13
00:01:49,000 --> 00:01:57,000
Stepping in my footsteps.

14
00:01:57,000 --> 00:02:02,000
Putting down his feet in my footsteps.

15
00:02:02,000 --> 00:02:06,000
So literally getting about just about as close as you could to the Buddha,

16
00:02:06,000 --> 00:02:09,540
following him everywhere.

17
00:02:09,540 --> 00:02:27,500
So Jahoti and if he is a vidja, a vidjaalu with craving, with great desires, full of desire.

18
00:02:27,500 --> 00:02:44,400
Thi, tim, basara, gung, asu, tib, basara, gung, with sharp, extreme lust for sensuality.

19
00:02:44,400 --> 00:02:57,040
Bia, pan, titu, with a mind full of ill will, remind of ill will, angry at other people,

20
00:02:57,040 --> 00:03:03,840
hating other people with only bitter thoughts for others.

21
00:03:03,840 --> 00:03:12,800
Badut, manasang, oh, corrupted, with corrupted mental formations,

22
00:03:12,800 --> 00:03:20,960
so thoughts that are corrupted, thinking to harm others and to manipulate others,

23
00:03:20,960 --> 00:03:29,640
to oppress and imbues others.

24
00:03:29,640 --> 00:03:38,760
Murtasati, with confused mindfulness, confused recognition,

25
00:03:38,760 --> 00:03:42,760
the reaction reacting to things in a confused way,

26
00:03:42,760 --> 00:03:48,640
without wisdom, without clarity, when things arise reacting,

27
00:03:48,640 --> 00:03:56,400
based on dark, like being in the dark, based on habit, based on ignorance,

28
00:03:56,400 --> 00:04:00,840
so it would give rise to greed and anger.

29
00:04:00,840 --> 00:04:09,760
Asapajana, without full comprehension, without sun, full,

30
00:04:09,760 --> 00:04:20,480
the sound is like comprehensive, but is full, jar, is knowledge.

31
00:04:20,480 --> 00:04:29,160
That full knowledge, asama, ito, without concentration or focus,

32
00:04:29,160 --> 00:04:35,520
without levelheadedness, so disturbed,

33
00:04:35,520 --> 00:04:44,880
with their mind is not impartial, objective, able to see things as they are.

34
00:04:44,880 --> 00:04:47,840
As I said, there's a lot in here, because it looks like a simple quote,

35
00:04:47,840 --> 00:04:53,400
but each of these words has very important meaning to them.

36
00:04:53,400 --> 00:05:07,080
We bunted ito, it's just more desire, we bunted the wandering mind, confused mind, we bunted it,

37
00:05:07,080 --> 00:05:13,720
we bummed it to roam the strain.

38
00:05:13,720 --> 00:05:19,160
We bunted it with the wandering mind, the mind that doesn't stay focused,

39
00:05:19,160 --> 00:05:25,240
the mind that goes hither and thither, thinking about this and that.

40
00:05:25,240 --> 00:05:34,040
Park, a team real, with unguarded, untrained,

41
00:05:34,040 --> 00:05:38,600
sort of run of the mill, maybe it doesn't mean that,

42
00:05:38,600 --> 00:05:48,040
but Park, a team can mean ordinary, but here it means unguarded or untrained senses.

43
00:05:48,040 --> 00:05:53,400
So the senses are our doors, by which we experience things,

44
00:05:53,400 --> 00:05:59,240
but the problem is we don't just experience things, we react to them.

45
00:05:59,240 --> 00:06:04,040
So this is untrained, we're based on habit and memory and experience,

46
00:06:04,040 --> 00:06:13,800
we react to a very specific way that often is a cause for great suffering for us.

47
00:06:13,800 --> 00:06:21,080
Then others, a cause for bad karma.

48
00:06:21,080 --> 00:06:29,800
So all of these things, so this, imagine this person holding onto the Buddha's

49
00:06:29,800 --> 00:06:37,960
coat tails, so to speak, walking in his footsteps,

50
00:06:37,960 --> 00:06:43,720
that he's got all these things, he's full of greed, as many desires,

51
00:06:43,720 --> 00:06:48,520
as craving or lust for sensuality, as ill will,

52
00:06:48,520 --> 00:06:53,720
as a corrupted thoughts, mental activity.

53
00:06:53,720 --> 00:07:02,520
This confused, is with a muddled awareness,

54
00:07:02,520 --> 00:07:07,720
without comprehension and understanding, without focus,

55
00:07:07,720 --> 00:07:14,440
or somehow, without this kind of balance to calm mind,

56
00:07:14,440 --> 00:07:22,200
we bend that detour, it's wandering mind, and with untrained faculties.

57
00:07:22,200 --> 00:07:24,680
So what about him? What about her?

58
00:07:24,680 --> 00:07:33,000
Atakoso arakawa, such a person, well, such a person,

59
00:07:33,000 --> 00:07:39,160
arakawa, my home, is very far from me,

60
00:07:39,160 --> 00:07:46,440
my home, to me, as far away from me,

61
00:07:46,440 --> 00:07:53,560
is as low very far from me, a huncha tasa in me,

62
00:07:53,560 --> 00:07:56,920
from him, from her.

63
00:07:56,920 --> 00:08:06,920
Thang di sahayta, what's the cause of that? Thang di sahayta, what's the cause of that?

64
00:08:06,920 --> 00:08:12,120
Thang di sahayta, what's the cause of that? Thang di sahayta, bikhu napasati.

65
00:08:12,120 --> 00:08:18,520
For the dhamma, because that bikhu, that monk, doesn't say,

66
00:08:18,520 --> 00:08:30,520
not seeing the dhamma, they don't see me.

67
00:08:30,520 --> 00:08:36,520
So the meaning here is that, in two ways,

68
00:08:36,520 --> 00:08:40,520
having all those things is a sign, of course, of not seeing the dhamma.

69
00:08:40,520 --> 00:08:44,520
It's really sort of the definition of not seeing the dhamma,

70
00:08:44,520 --> 00:08:50,520
because seeing the dhamma is to see things as they are,

71
00:08:50,520 --> 00:08:59,520
and to live life impartially, objectively wisely,

72
00:08:59,520 --> 00:09:02,520
and all of these things are not that.

73
00:09:02,520 --> 00:09:07,520
But also, the way of explaining it is that these things stop us

74
00:09:07,520 --> 00:09:09,520
from seeing truth.

75
00:09:09,520 --> 00:09:13,520
You can't see the dhamma if you have all of these things.

76
00:09:13,520 --> 00:09:18,520
Maybe you'll live with the Buddha.

77
00:09:18,520 --> 00:09:22,520
That's why I had Jantong said my teacher said,

78
00:09:22,520 --> 00:09:28,520
because he's very famous in Thailand or the fairly famous in Thailand,

79
00:09:28,520 --> 00:09:31,520
and I guess now he's very, very famous,

80
00:09:31,520 --> 00:09:39,520
but they were coming to visit him thousands of people on his birthday,

81
00:09:39,520 --> 00:09:44,520
just pouring in.

82
00:09:44,520 --> 00:09:47,520
And so he said,

83
00:09:47,520 --> 00:09:50,520
well, going to see noble people,

84
00:09:50,520 --> 00:09:54,520
going to see going to live with them,

85
00:09:54,520 --> 00:09:57,520
or even wait on them,

86
00:09:57,520 --> 00:10:00,520
or care for them, or attend to them as their student,

87
00:10:00,520 --> 00:10:05,520
and as their attendant.

88
00:10:05,520 --> 00:10:08,520
It's not the same as becoming a noble one yourself.

89
00:10:08,520 --> 00:10:10,520
It's a simple thing to say.

90
00:10:10,520 --> 00:10:13,520
It's one of those quotes that I've got published in the past year.

91
00:10:19,520 --> 00:10:23,520
So this is an important quote,

92
00:10:23,520 --> 00:10:27,520
and an important sort of teaching in the context of religion,

93
00:10:27,520 --> 00:10:29,520
because a lot of people end up doing that.

94
00:10:29,520 --> 00:10:32,520
They think that by associating with good people,

95
00:10:32,520 --> 00:10:33,520
that's enough.

96
00:10:33,520 --> 00:10:37,520
And the Buddha did say that associating with good people

97
00:10:37,520 --> 00:10:39,520
is the whole of the whole of life.

98
00:10:39,520 --> 00:10:41,520
But the thing about it is that

99
00:10:41,520 --> 00:10:44,520
wise people tend to say things like this.

100
00:10:44,520 --> 00:10:46,520
They say, you know, look.

101
00:10:46,520 --> 00:10:49,520
It's not about following me around.

102
00:10:49,520 --> 00:10:51,520
It's about changing yourself.

103
00:10:51,520 --> 00:10:54,520
Of course, the great thing about associating with wise people

104
00:10:54,520 --> 00:10:57,520
is that they teach these things.

105
00:10:57,520 --> 00:11:01,520
They push you to become a better person.

106
00:11:01,520 --> 00:11:05,520
They don't let you walk around.

107
00:11:05,520 --> 00:11:15,520
They're holding on to their robes.

108
00:11:15,520 --> 00:11:18,520
And then there's the idea about being far

109
00:11:18,520 --> 00:11:22,520
and being near to the Buddha, which

110
00:11:22,520 --> 00:11:29,520
kind of has greater implications

111
00:11:29,520 --> 00:11:33,520
in terms of the idea of space.

112
00:11:33,520 --> 00:11:40,520
And the concept of reality, what is real?

113
00:11:40,520 --> 00:11:51,520
We think of space as being important, location being important,

114
00:11:51,520 --> 00:11:58,520
when in fact it's really just a part of the impression

115
00:11:58,520 --> 00:12:00,520
that we have of things.

116
00:12:00,520 --> 00:12:05,520
Someone on Facebook today, an old friend from high school,

117
00:12:05,520 --> 00:12:10,520
he posted, he said, what was it?

118
00:12:10,520 --> 00:12:13,520
There's nothing wrong with my brain.

119
00:12:13,520 --> 00:12:16,520
The anxiety I'm experiencing is just a reflection

120
00:12:16,520 --> 00:12:22,520
of all the things that are messed up in the world.

121
00:12:22,520 --> 00:12:25,520
And I replied, and I said, the problem is that the world

122
00:12:25,520 --> 00:12:28,520
is all in your brain.

123
00:12:28,520 --> 00:12:30,520
Because it's really true.

124
00:12:30,520 --> 00:12:32,520
It's not even in the mind.

125
00:12:32,520 --> 00:12:34,520
The mind gets everything from the brain,

126
00:12:34,520 --> 00:12:40,520
which projects everything and creates things.

127
00:12:40,520 --> 00:12:43,520
They did a study that said, when you look at something,

128
00:12:43,520 --> 00:12:45,520
you don't look at the whole object.

129
00:12:45,520 --> 00:12:47,520
So we think in the room around us,

130
00:12:47,520 --> 00:12:49,520
there's all these objects that we're seeing.

131
00:12:49,520 --> 00:12:52,520
But in fact, we only see very small bits of it.

132
00:12:52,520 --> 00:12:57,520
We only see enough to be able to create the picture in our minds.

133
00:12:57,520 --> 00:13:03,520
And they did tests on people that they were able to trick them

134
00:13:03,520 --> 00:13:06,520
into not being able to see something on the pictures.

135
00:13:06,520 --> 00:13:08,520
They changed something, but you couldn't see it.

136
00:13:08,520 --> 00:13:10,520
Because you weren't actually seeing the picture.

137
00:13:10,520 --> 00:13:20,520
You weren't actually seeing the light was only the impetus

138
00:13:20,520 --> 00:13:29,520
or the spark that then the instigation that leads the mind

139
00:13:29,520 --> 00:13:33,520
to create the image.

140
00:13:33,520 --> 00:13:38,520
Anyway, the point of how that relates is the world is really

141
00:13:38,520 --> 00:13:42,520
all in our heads, all in our minds,

142
00:13:42,520 --> 00:13:44,520
because you have to say.

143
00:13:44,520 --> 00:13:52,520
And so this is how, I mean, this kind of a theory,

144
00:13:52,520 --> 00:13:54,520
some people would challenge it and say,

145
00:13:54,520 --> 00:13:56,520
that's not true, the world does exist out there

146
00:13:56,520 --> 00:13:59,520
whether we experience it or not.

147
00:13:59,520 --> 00:14:04,520
But under this theory, the idea of some mind being

148
00:14:04,520 --> 00:14:12,520
a primary, this is how the whole idea of rebirth

149
00:14:12,520 --> 00:14:15,520
and karma works.

150
00:14:15,520 --> 00:14:18,520
Because we create these experiences.

151
00:14:18,520 --> 00:14:21,520
We create these existences.

152
00:14:21,520 --> 00:14:28,520
So it's how we understand our relationships with others,

153
00:14:28,520 --> 00:14:35,520
why we're brought into contact with the people around us,

154
00:14:35,520 --> 00:14:43,520
people who we meet in life.

155
00:14:43,520 --> 00:14:47,520
It's because we see, we see with them,

156
00:14:47,520 --> 00:14:52,520
we see them, we see in the same way as them.

157
00:14:52,520 --> 00:14:54,520
And so to be close to the Buddha,

158
00:14:54,520 --> 00:14:58,520
the only way to be close to the Buddha is to see in the way he sees.

159
00:14:58,520 --> 00:15:01,520
To see things the way he sees them.

160
00:15:01,520 --> 00:15:05,520
Because that the way you see things, the way you look at things,

161
00:15:05,520 --> 00:15:07,520
the way you react to things.

162
00:15:07,520 --> 00:15:09,520
That's the whole of who you are.

163
00:15:09,520 --> 00:15:11,520
That's what makes you who you are.

164
00:15:11,520 --> 00:15:16,520
It's what determines your life, your future.

165
00:15:16,520 --> 00:15:19,520
So really that's all we're doing in meditation,

166
00:15:19,520 --> 00:15:24,520
learning to see things, the way the Buddha saw things,

167
00:15:24,520 --> 00:15:27,520
which is as they are, why we call them the Buddha

168
00:15:27,520 --> 00:15:31,520
because he awoke to things as they are.

169
00:15:31,520 --> 00:15:35,520
He came to understand things as they are.

170
00:15:35,520 --> 00:15:38,520
We claim that we don't see things as they are.

171
00:15:38,520 --> 00:15:40,520
We react to them.

172
00:15:40,520 --> 00:15:42,520
I mean, we see things as they are, of course,

173
00:15:42,520 --> 00:15:44,520
but we do much more than that.

174
00:15:44,520 --> 00:15:47,520
As soon as you see something as it is,

175
00:15:47,520 --> 00:15:51,520
you get lost in how you want it to be,

176
00:15:51,520 --> 00:15:54,520
or how you want it not to be.

177
00:15:54,520 --> 00:15:58,520
We can think of it, what it makes you think of.

178
00:15:58,520 --> 00:16:02,520
Cut in the past, future.

179
00:16:02,520 --> 00:16:06,520
You're not seeing things as they are.

180
00:16:06,520 --> 00:16:10,520
Because we don't see that, we can see the Buddha.

181
00:16:10,520 --> 00:16:13,520
Very far from the Buddha, whether we're right next to him.

182
00:16:13,520 --> 00:16:14,520
And then he says,

183
00:16:14,520 --> 00:16:25,520
we "... suppose It was a monk,

184
00:16:25,520 --> 00:16:31,520
a man, living 100 meow Chau electricity.

185
00:16:31,520 --> 00:16:39,160
It was like 100 meow Chau electricity andportation,

186
00:16:39,160 --> 00:16:48,560
of our distances. So Johoti, and then he is not any of those things. So Anabhijayalu,

187
00:16:48,560 --> 00:16:57,960
not with many desires, Kami. So Anatimbasara. Actually, I'm not sure which from the Kami

188
00:16:57,960 --> 00:17:10,360
who goes with Anabhijayalu, Kami. So maybe it's that way. Not with many desires in regards.

189
00:17:10,360 --> 00:17:19,760
No, I think it's Anabhijayalu, Kami. So Anatimbasara, not with sharp lust, with strong lust in regards

190
00:17:19,760 --> 00:17:37,560
to sensuality. Anabhya panabhya panadhita panadhita. Anabhya panadhita, without ill will. Appadhuktam manasam kapou,

191
00:17:37,560 --> 00:17:53,080
with uncorrupted mental formations, upatitasati, with established mindfulness, with the mind

192
00:17:53,080 --> 00:17:58,840
that is able to grasp things. The way of grasping, what you say in English, we use the word

193
00:17:58,840 --> 00:18:04,920
grasp in two ways. Because if you grasp something, it means you understand that not like grasping,

194
00:18:04,920 --> 00:18:12,120
something like grasping onto something, or grasping in general. When you grasp something,

195
00:18:12,120 --> 00:18:16,120
means you understand it. And that's what we do in the meditation. Every moment trying to grasp,

196
00:18:16,120 --> 00:18:21,240
it just for a moment, just grasp that seeing. And you grasp it and you say,

197
00:18:21,240 --> 00:18:28,120
yes, that's seeing. Instead of saying, that's good, that's bad, that's me, that's mine, and so on.

198
00:18:28,120 --> 00:18:41,640
Sampajana, fully comprehending or full awareness, full knowledge, full and right knowledge.

199
00:18:41,640 --> 00:18:48,600
Sammajito, well balanced or well focused. A kangajito,

200
00:18:48,600 --> 00:19:04,920
with one point, with a one-pointed mind. Sammajito is trained, restrained senses.

201
00:19:04,920 --> 00:19:19,000
Atakososanti gave, my person is as though, in my presence, a hanjatasa, in me and his tangji

202
00:19:19,000 --> 00:19:25,840
sahita, which is the cause, the manhisobi kebabe kupasati. For that manh

203
00:19:25,840 --> 00:19:47,760
kupasati, seeing the dhamma, he sees me. There you go. That's the dhamma for tonight.

204
00:19:47,760 --> 00:19:59,520
Sammajito, anyone have any questions? I think I won't make you come on and hang out.

205
00:19:59,520 --> 00:20:08,800
I'll just go back to text questions. You know what I was thinking? What we could do is, instead of

206
00:20:08,800 --> 00:20:14,800
like doing questions and videos about their answers, is we could set up some kind of page where

207
00:20:14,800 --> 00:20:22,720
people could vote on topics, and just like one word topics, and do a video on this,

208
00:20:23,840 --> 00:20:29,040
and people could vote on it, but I can do a video on top, and certain topics.

209
00:20:29,040 --> 00:20:47,040
Maybe that would work well. Someone has to make such a page. There's any questions, I'll take

210
00:20:47,040 --> 00:21:04,800
text questions tonight. Or those of you over at YouTube, you have to find our meditation page.

211
00:21:07,920 --> 00:21:11,680
And they have to be live, unfortunately, I'm not going to go digging back through the

212
00:21:11,680 --> 00:21:20,400
past chat, and find common, did, comment.

213
00:21:42,640 --> 00:21:56,720
About New York. Well, I posted some events on Facebook. So, I shared some events that were shared

214
00:21:56,720 --> 00:22:05,840
with me. So, I think that's what you got to do is go to those events. Let me see.

215
00:22:05,840 --> 00:22:21,280
There's a April 16th event at Rockaway Beach, 90-16 Rockaway Beach Boulevard,

216
00:22:22,400 --> 00:22:31,520
wait, 1, 1, 6, 9, 3 Rockaway Beach Queens, 3 PM1 April 16th. I don't know. I mean, I'm just getting

217
00:22:31,520 --> 00:22:39,360
these as you guys are. So, I didn't plan any of this. And then I'm doing a neon meditation,

218
00:22:41,200 --> 00:22:52,640
7 PM at Light Bright Neon, 232 Third Street, number C-102 Brooklyn. That's April 19th, 7 PM.

219
00:22:52,640 --> 00:23:03,680
I think like 16th and 17th and 18th, there's other stuff that I'm not actually

220
00:23:05,360 --> 00:23:09,440
I'm not actually the teacher all these days, I don't know, because these are the only ones I got.

221
00:23:09,440 --> 00:23:21,360
16th, 19th and the 20th, I don't know, but the 21st, that's Dharma Punks, I think. No.

222
00:23:26,800 --> 00:23:32,480
Yeah, Dharma Punks in Manhattan. I don't know, you have to find the Dharma Punks.

223
00:23:32,480 --> 00:23:39,360
Yeah, please. A little yoga Dharma and wellness, that's a place I think.

224
00:23:39,360 --> 00:24:01,120
And then April 22nd, fourth green Brooklyn, the lucky lotus yoga and vegan cafe.

225
00:24:01,120 --> 00:24:14,400
And these are all on my page. So, let's see if we can go look at them.

226
00:24:14,400 --> 00:24:27,840
How is the mind so powerful yet? So, I think you're trying to say Naive.

227
00:24:32,240 --> 00:24:36,320
Naive is not about that way, but that's okay. I think you're saying Naive.

228
00:24:36,320 --> 00:24:45,680
Well, powerful doesn't mean that it's in control of the power, right?

229
00:24:47,840 --> 00:24:51,200
The mind is not in control of its own power, that's really the problem.

230
00:24:54,960 --> 00:24:56,160
The powerful doesn't mean good.

231
00:24:56,160 --> 00:25:09,280
So, the mind ends up, I mean the mind can be very wise. The mind can be very smart, clever,

232
00:25:10,480 --> 00:25:19,200
and so on, but it can also be very naive. So, it twists itself up and ends up creating

233
00:25:19,200 --> 00:25:28,720
its own creating problems for itself, because it has no direction. The mind lacks is training.

234
00:25:37,760 --> 00:25:45,840
Training is a very specific thing to see. The mind that is just going randomly

235
00:25:45,840 --> 00:25:53,440
is very unlikely to even think about training itself. So, as a result, you see that most of us

236
00:25:54,640 --> 00:26:00,720
don't think of training ourselves. I really didn't. I was looking, I went to Asia looking for

237
00:26:00,720 --> 00:26:06,400
wisdom looking for insight, but didn't think I'd have to work for it. Most people don't.

238
00:26:06,400 --> 00:26:11,200
Most people who come to meditate don't even think of it as work. They think, yeah, meditation,

239
00:26:11,200 --> 00:26:18,000
that'll be like a pill I can swallow and I'll just sit down on my mat and poof on the happy,

240
00:26:18,000 --> 00:26:26,320
right? They're like, oh crap, this is tough. A lot of people don't like the meditation that I teach

241
00:26:26,320 --> 00:26:30,400
when I first teach it. You can't teach this to everyone. Not everyone's going to appreciate it.

242
00:26:31,200 --> 00:26:36,640
I'm sure that'll be the case in New York. There'll be many people who are not very happy about

243
00:26:36,640 --> 00:26:41,360
what I'm teaching. I mean, they won't say it, but it won't stick with most people.

244
00:26:42,720 --> 00:26:50,880
So, people want comfort. They want an escape. It's hard to get people to grow up and learn to

245
00:26:50,880 --> 00:26:57,440
look, learn to look at their problems. That's going to be harsh language, but I don't mean,

246
00:26:57,440 --> 00:27:04,320
I try to, yeah, don't take that too hard. It won't be too critical, but it really is the case.

247
00:27:04,320 --> 00:27:12,160
I'm not condescending or looking down on such people. I was the same. We're all in this position

248
00:27:12,160 --> 00:27:18,800
where we just have to grow up. We have to start really taking the bull by the horns and

249
00:27:18,800 --> 00:27:24,720
stop running away and stop fooling ourselves. We're going to be able to just ignore the problems

250
00:27:24,720 --> 00:27:34,480
only. We're going to escape our problems. We have to really go facing your problems is the hardest thing.

251
00:27:36,880 --> 00:27:44,240
Kind of went off on tangent there, but the mind is just complex, I suppose.

252
00:27:44,240 --> 00:27:57,040
And as a result, it kind of does do itself. It's like the ocean is powerful, I suppose you could

253
00:27:57,040 --> 00:28:03,520
say, but most of the time the ocean doesn't do anything. You say the ocean is powerful. What

254
00:28:03,520 --> 00:28:09,120
the heck does that mean? You go to the ocean. Ooh, it's not doing anything powerful, right?

255
00:28:09,120 --> 00:28:15,920
But then you have a tsunami, right? Or you have a hurricane, and then you see the power of the ocean.

256
00:28:19,760 --> 00:28:26,480
When the ocean is in its natural state, the mind of its natural state is diffuse. It's full of

257
00:28:26,480 --> 00:28:36,080
contradictions that use up all its energy. So if you train your mind, you have all this energy

258
00:28:36,080 --> 00:28:39,200
that's very powerful. They have to concentrate it.

259
00:28:59,440 --> 00:29:05,120
So I'm not sure if you're an English native speaker, but the words grow up in English,

260
00:29:05,120 --> 00:29:14,080
it's an idiom, it doesn't mean get older. It means stop being chosen now, stop being immature.

261
00:29:15,440 --> 00:29:23,840
There's nothing to do with age. Grow up means stop being immature.

262
00:29:23,840 --> 00:29:42,480
Anyway, if that's all, I'll say goodnight, I have still got stuff. This Sunday we're having a

263
00:29:44,400 --> 00:29:48,320
storytelling, if some of you know about the storytelling thing we're having on Sunday,

264
00:29:48,320 --> 00:29:54,240
at the spectator auditorium, if any of you are local, you're welcome to come one over. I'll be

265
00:29:54,240 --> 00:30:00,000
telling a story from the Jotica about a bird. It's the one I told in Second Life recently, I think.

266
00:30:00,960 --> 00:30:09,760
It's about a bird that stays in its tree, even after the tree kind of dries up,

267
00:30:09,760 --> 00:30:20,960
dies, and it stays there at a gratitude to the tree. The event is about relating

268
00:30:22,720 --> 00:30:28,080
religion to the environment, because there's the issues of climate change, so it's how do our

269
00:30:28,080 --> 00:30:33,360
religions really deal with climate change, right, to the environment and protection of the

270
00:30:33,360 --> 00:30:40,480
environment. The moral of this story is about contentment drilling, which is Buddhism's

271
00:30:41,120 --> 00:30:46,960
probably the best way that Buddhism relates to environmental protection and so on is

272
00:30:50,000 --> 00:30:58,960
not being greedy, not taking more than is appropriate, not going to seek out,

273
00:30:58,960 --> 00:31:09,120
and also to be appreciative, so the idea of appreciating the environment in this bird

274
00:31:10,320 --> 00:31:17,200
stays with the tree and appreciates it and it's content with it, so it's about, the Buddha tells

275
00:31:17,200 --> 00:31:24,720
the story to a monk who gets kind of upset because a village burns down, so you're staying

276
00:31:24,720 --> 00:31:30,240
and depending on this village, for the rains retreat, the village burned down, I think,

277
00:31:31,440 --> 00:31:36,720
so he was very hard for him to get food, and so he told us the Buddha and the Muslim,

278
00:31:37,920 --> 00:31:44,080
don't just go by food, food isn't a good indicator, when you get the requisite, so it's not a

279
00:31:44,080 --> 00:31:54,160
good indicator of what a good place is. If you can meditate, well, there you should stay there,

280
00:31:55,200 --> 00:32:00,800
and then he told the story of this bird that stayed with this tree because it was content,

281
00:32:02,560 --> 00:32:07,600
even when the tree died. It's a little bit longer than you have to hear me tell the story,

282
00:32:07,600 --> 00:32:14,880
but it's just the simple thing. So I got to prepare for that kind of, and then I got two exams

283
00:32:14,880 --> 00:32:22,320
next week, and then I'm done, and then off to New York, and then it's already almost May,

284
00:32:22,320 --> 00:32:28,640
and then in May we've got one event, the Buddha's birthday in Mississauga, and then in June,

285
00:32:28,640 --> 00:32:38,560
I'm off to Asia for months, and then back here for the rains. Lots of people interested in

286
00:32:38,560 --> 00:32:47,040
coming to meditate all the way into November, which is a bit awkward because I can't think that far

287
00:32:47,040 --> 00:32:57,680
ahead. I mean, because in the past I've had trouble thinking far ahead, now it seems pretty stable,

288
00:32:57,680 --> 00:33:02,880
so maybe we can start tentatively accepting long-term reservations. I just

289
00:33:03,600 --> 00:33:12,560
hesitant to do so because plans change the futures uncertainty. I could end up moving back to Asia

290
00:33:12,560 --> 00:33:22,640
or something, not likely. I think we can start to be a little more accepting of future plans

291
00:33:22,640 --> 00:33:32,880
for this sort, these days. But we maybe need a bigger place where we have to start thinking about

292
00:33:32,880 --> 00:33:40,400
finding a bigger house in the area. I know it's not nice to have to move, but this house really

293
00:33:40,400 --> 00:33:45,760
isn't. I mean, it's fairly big, it's just not well laid out for a meditation center. Their houses

294
00:33:45,760 --> 00:33:51,040
and close to the university that have like six bedrooms because they were built and they were

295
00:33:51,040 --> 00:33:56,400
renovated to house students, so they're trying to fit as many students in the house as you can,

296
00:33:56,400 --> 00:34:03,680
which kind of fits our situation. They've got six bedroom houses that are fairly reasonable.

297
00:34:05,840 --> 00:34:11,200
We should maybe talk about that anyway. Anyway, that's all for tonight.

298
00:34:11,200 --> 00:34:21,200
Thank you guys. Thanks for coming out. Have a good night.

