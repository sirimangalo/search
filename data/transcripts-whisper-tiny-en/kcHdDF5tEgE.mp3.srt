1
00:00:00,000 --> 00:00:08,600
Meditation changes everything.

2
00:00:08,600 --> 00:00:13,280
The world around us has always been out of our control, but it's never been more evident

3
00:00:13,280 --> 00:00:17,960
than now.

4
00:00:17,960 --> 00:00:23,200
In the face of so much, how can we keep our minds steady?

5
00:00:23,200 --> 00:00:39,040
Again, anger, loneliness, despair, the cure for these can only be found within ourselves.

6
00:00:39,040 --> 00:00:45,720
This is the goal of Sir among the low international, free and person meditation retreats.

7
00:00:45,720 --> 00:00:52,840
In a world broken up and isolated by the pandemic, we continue offering courses and building

8
00:00:52,840 --> 00:01:04,880
community online while taking this opportunity to invest in a new future.

9
00:01:04,880 --> 00:01:11,000
To build a center of our own, a refuge for the mind, a place to free oneself and find

10
00:01:11,000 --> 00:01:23,240
footing in a shaky world.

