Why do we focus on the abdomen?
The abdomen is a physical phenomenon.
It is the elements of motion,
while you are the two as described in Buddhist texts.
Mindfulness of breathing,
anapana sati is technically considered
tranquility, samatha, meditation,
while analysis of the elements,
chatura daktu wa wa tana,
is considered the basis of insight meditation.
All the elements are experienced directly.
The breath itself is a concept.
Although watching the abdomen can be thought of as mindfulness of breathing,
it differs from watching in and out breathing in long, important way.
The dakta often leads to tranquility,
rather than directly to seeing the nature of reality.
While the former is sat family in ultimate reality
and thus, when you see it,
to seeing it clearly for what it is.
