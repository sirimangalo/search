1
00:00:00,000 --> 00:00:13,200
Hi, today I thought I would talk about some of the things that meditation isn't.

2
00:00:13,200 --> 00:00:25,960
Now I get a lot of questions about what to do when exercises or descriptions of experiences

3
00:00:25,960 --> 00:00:34,480
and meditation which seem to be special or somehow outside of the normal course of meditation

4
00:00:34,480 --> 00:00:43,720
and that's outside of what we should expect one to deal with in an ordinary fashion.

5
00:00:43,720 --> 00:00:51,640
Often when people experience their things like lights or collars or pictures, visions,

6
00:00:51,640 --> 00:00:59,360
sometimes people experience very quiet, calm states or very focused and intense states,

7
00:00:59,360 --> 00:01:08,600
blissful states, states of rapture, similar as you would find in other religious traditions.

8
00:01:08,600 --> 00:01:15,760
Sometimes there will be the arising of knowledge on a mundane level, I mean to understand

9
00:01:15,760 --> 00:01:23,280
things about one's life or so and sometimes there will be great levels of energy,

10
00:01:23,280 --> 00:01:28,800
kind of like a static electricity that arises in the mind.

11
00:01:28,800 --> 00:01:34,440
All sorts of many special states or states would be labeled as special and so I just wanted

12
00:01:34,440 --> 00:01:41,600
to deal with them here briefly, sort of to give a bit of a warning to people, not regarding

13
00:01:41,600 --> 00:01:45,920
these states themselves because there's nothing dangerous about any of these states in and

14
00:01:45,920 --> 00:01:53,080
of themselves but a warning not to get misled by these states and that thinking that they

15
00:01:53,080 --> 00:02:02,720
are indeed somehow special, somehow higher, somehow the way, the path or the goal of meditation

16
00:02:02,720 --> 00:02:08,760
because whatever special state arises, it in the end will eventually disappear, it will

17
00:02:08,760 --> 00:02:14,640
go away, so if we hold on to it as being special in whatever way and it's going to lead

18
00:02:14,640 --> 00:02:20,560
us to an imbalance in our perception and in our acceptance of reality, when in fact these

19
00:02:20,560 --> 00:02:28,680
states are all simply natural, ordinary states of mind, it's just that they're not ordinary

20
00:02:28,680 --> 00:02:33,520
to people who have in practice meditation, if you have in practice meditation these states

21
00:02:33,520 --> 00:02:43,280
will seem to be somehow outside of mundane reality, it's just that in fact the reason

22
00:02:43,280 --> 00:02:47,960
that we don't experience them is that our ordinary states of reality are not meditative

23
00:02:47,960 --> 00:02:53,200
states of reality, that's the only reason when these things come up, they are ordinary,

24
00:02:53,200 --> 00:02:59,960
they're ordinary to a meditative state and they should thus be treated as any other meditative

25
00:02:59,960 --> 00:03:05,160
experience, so just as when we have say pain in the legs or so on and we would say to

26
00:03:05,160 --> 00:03:12,000
ourselves pain and just treat it as what it is, the same when we see visions or when

27
00:03:12,000 --> 00:03:17,760
we obtain special, what we would call special experiences, when we see visions we should

28
00:03:17,760 --> 00:03:22,280
say to ourselves seeing, as if we were seeing something ordinary in our everyday life,

29
00:03:22,280 --> 00:03:28,600
just say to ourselves seeing, seeing until the vision goes away, just using it as an

30
00:03:28,600 --> 00:03:33,480
object of meditation and coming to see it clearly as it is, if we feel happy, states of

31
00:03:33,480 --> 00:03:40,320
bliss or so on, we can say to ourselves happy, happy, blissful, if we feel calm or quiet,

32
00:03:40,320 --> 00:03:46,040
we can say to ourselves calm, just reminding ourselves of exactly what's going on, so

33
00:03:46,040 --> 00:03:54,360
that we don't get caught up and become entranced or enchanted by these simple ordinary

34
00:03:54,360 --> 00:04:01,400
meditation states, that happen from occur from most people, I think this is one of the

35
00:04:01,400 --> 00:04:07,920
most often asked questions that I get and so I think it's probably worth it to write

36
00:04:07,920 --> 00:04:12,840
it, to make this sort of video to explain what's going on here, that these states

37
00:04:12,840 --> 00:04:18,240
they come from meditations, specifically from mostly coming from high states of concentration

38
00:04:18,240 --> 00:04:22,520
and it's a perfectly natural thing to occur, there's no reason to be afraid of them,

39
00:04:22,520 --> 00:04:30,200
but there's certainly no reason to become attached to them or to make more of them than

40
00:04:30,200 --> 00:04:34,760
they actually are and to think that somehow they are the path that we should follow and

41
00:04:34,760 --> 00:04:39,640
pursue and encourage and develop, they certainly aren't, the path that we should follow

42
00:04:39,640 --> 00:04:47,000
and pursue and encourage and develop is the acceptance, the awareness and the acknowledgement

43
00:04:47,000 --> 00:04:54,920
of things, the clear awareness and understanding of things as they are, so whatever arises

44
00:04:54,920 --> 00:05:02,240
we see it for what it is and not liking it or disliking it or becoming partial or intoxicated

45
00:05:02,240 --> 00:05:07,280
by it and of course that's exactly what happens with these special states if we're not

46
00:05:07,280 --> 00:05:13,320
careful, so for this reason I think it's important to sort of warn everybody, not that

47
00:05:13,320 --> 00:05:18,640
something dangerous is going to happen, it is possible truly that if you follow after any

48
00:05:18,640 --> 00:05:24,520
state, it can lead you to states of insanity and you could drive yourself crazy chasing

49
00:05:24,520 --> 00:05:30,960
after and developing these states, people who develop special states of trance or so on can

50
00:05:30,960 --> 00:05:37,200
actually, I mean there are cases where they would actually, so to speak, drive themselves

51
00:05:37,200 --> 00:05:41,960
crazy, there's nothing dangerous with the states in and of themselves, it's the attitude

52
00:05:41,960 --> 00:05:48,240
of the meditator that becomes so wound up and so caught up and so interested in keen

53
00:05:48,240 --> 00:05:55,480
and attached to something that it ends up releasing in an explosion of temporary insanity,

54
00:05:55,480 --> 00:05:59,480
so there is that warning and most of what's going to happen instead is people will just

55
00:05:59,480 --> 00:06:05,080
become afraid and think that it's something too much or they don't understand it, these

56
00:06:05,080 --> 00:06:10,800
shink things should be understood as they are, that there's nothing dangerous or special

57
00:06:10,800 --> 00:06:15,040
in and in these things end up themselves and we shouldn't treat them like that, or for

58
00:06:15,040 --> 00:06:20,920
sure we lead to suffering and discontent when they disappear or suffering when we chase

59
00:06:20,920 --> 00:06:27,400
after them and therefore drivers are as crazy or any number of things could happen,

60
00:06:27,400 --> 00:06:31,960
so thanks for tuning in, this is just a little bit of explanation on what the meditation

61
00:06:31,960 --> 00:06:38,880
is not, it's not any special state that we might experience, it's the clear awareness

62
00:06:38,880 --> 00:06:44,160
and understanding of all states and the impartiality which comes there from, so thanks

63
00:06:44,160 --> 00:06:51,160
for tuning in and hope to see you all commenting and sending your comments in, thanks for

64
00:06:51,160 --> 00:07:21,000
tuning in, talk to you later.

