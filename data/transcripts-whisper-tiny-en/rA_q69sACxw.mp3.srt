1
00:00:00,000 --> 00:00:03,000
The Buddha never once said that life is suffering.

2
00:00:03,000 --> 00:00:08,000
He never once made that accusation of that statement.

3
00:00:08,000 --> 00:00:12,000
In fact, what the Lord Buddha said that there is suffering,

4
00:00:12,000 --> 00:00:17,000
this is the first truth that He wanted to impress upon us,

5
00:00:17,000 --> 00:00:19,000
is that there is suffering.

6
00:00:19,000 --> 00:00:22,000
And for spiritual people,

7
00:00:22,000 --> 00:00:25,000
I think this is a sort of a given,

8
00:00:25,000 --> 00:00:27,000
something that we understand in life,

9
00:00:27,000 --> 00:00:33,000
that it's this suffering or the other problem with ordinary experience

10
00:00:33,000 --> 00:00:36,000
that we're trying to overcome,

11
00:00:36,000 --> 00:00:38,000
or we're trying to rise above,

12
00:00:38,000 --> 00:00:39,000
or we're trying to go beyond,

13
00:00:39,000 --> 00:00:43,000
we're trying to get to a state of peace and happiness,

14
00:00:43,000 --> 00:00:46,000
and freedom from suffering.

15
00:00:46,000 --> 00:00:50,000
But the ramifications for at least for ordinary people

16
00:00:50,000 --> 00:00:55,000
is to wake us up to this fact of life

17
00:00:55,000 --> 00:00:58,000
that most of the time running away from.

18
00:00:58,000 --> 00:01:01,000
People who are not generally on a spiritual path

19
00:01:01,000 --> 00:01:04,000
are very often running away from suffering.

20
00:01:04,000 --> 00:01:07,000
And probably all of us here are in some ways

21
00:01:07,000 --> 00:01:09,000
running away from suffering,

22
00:01:09,000 --> 00:01:12,000
even from just simple physical suffering.

23
00:01:12,000 --> 00:01:14,000
For instance, when we have a headache,

24
00:01:14,000 --> 00:01:18,000
we'll always try to take a pill to get rid of it

25
00:01:18,000 --> 00:01:19,000
when we have a backache,

26
00:01:19,000 --> 00:01:22,000
we'll go to see a masseuse.

27
00:01:22,000 --> 00:01:26,000
When we're sitting here and we suddenly start to have pains and aches,

28
00:01:26,000 --> 00:01:29,000
or itches, we'll have to adjust the body.

29
00:01:29,000 --> 00:01:30,000
When we have mental suffering,

30
00:01:30,000 --> 00:01:32,000
we're also trying to run away from it.

31
00:01:32,000 --> 00:01:33,000
We don't want to learn about it.

32
00:01:33,000 --> 00:01:34,000
We don't want to understand it.

33
00:01:34,000 --> 00:01:36,000
We don't want to know it's even there.

34
00:01:36,000 --> 00:01:39,000
So we go to the doctor and they prescribe us medication

35
00:01:39,000 --> 00:01:43,000
to help us to not have to experience this mental suffering,

36
00:01:43,000 --> 00:01:49,000
whether it be sadness or depression or anxiety or fear or worry or stress.

37
00:01:49,000 --> 00:01:54,000
We've got to find a cure for it to get rid of it.

38
00:01:54,000 --> 00:01:56,000
And it's not to say that there is no cure,

39
00:01:56,000 --> 00:01:57,000
and that's not a good thing.

40
00:01:57,000 --> 00:01:59,000
Yes, it's a good thing to get rid of all of these things.

41
00:01:59,000 --> 00:02:03,000
But we never look for the cure in understanding these things.

42
00:02:03,000 --> 00:02:07,000
There's a very little out there that says

43
00:02:07,000 --> 00:02:10,000
the real cure is in understanding these things,

44
00:02:10,000 --> 00:02:12,000
or that you have to look at them

45
00:02:12,000 --> 00:02:16,000
and you have to be there with these things to cure them.

46
00:02:16,000 --> 00:02:18,000
We always go to the opposite direction.

47
00:02:18,000 --> 00:02:21,000
We always say the cure is to be rid of it.

48
00:02:21,000 --> 00:02:22,000
Don't go near it.

49
00:02:22,000 --> 00:02:27,000
We just fire away from that experience as possible.

50
00:02:27,000 --> 00:02:30,000
So the Buddha, he explained that we

51
00:02:30,000 --> 00:02:33,000
to overcome these things, we have to understand them.

52
00:02:33,000 --> 00:02:37,000
And that really all of our suffering is a misunderstanding.

53
00:02:37,000 --> 00:02:42,000
It's based on not understanding what's going on when we suffer.

54
00:02:42,000 --> 00:02:45,000
Because suffering exists, it's there.

55
00:02:45,000 --> 00:02:48,000
And we have choices we can run away from it.

56
00:02:48,000 --> 00:02:53,000
We can act upon it, or we can come to understand it.

57
00:02:53,000 --> 00:02:55,000
And I would say there are only these three.

58
00:02:55,000 --> 00:02:56,000
Normally we say there are two.

59
00:02:56,000 --> 00:02:58,000
There's fight or flight.

60
00:02:58,000 --> 00:03:01,000
And I've always found that an interesting dichotomy

61
00:03:01,000 --> 00:03:04,000
and I've always thought of how Buddhism stands right in the middle

62
00:03:04,000 --> 00:03:08,000
and it's not running away from the problem,

63
00:03:08,000 --> 00:03:10,000
but also not acting upon it.

64
00:03:10,000 --> 00:03:13,000
So when you're angry or when you're confronted

65
00:03:13,000 --> 00:03:20,000
with a difficult situation, you can either repress the excitement,

66
00:03:20,000 --> 00:03:24,000
the anger, the frustration, or you can act out upon it.

67
00:03:24,000 --> 00:03:28,000
Now, the Buddha explained a third method of simply just watching it.

68
00:03:28,000 --> 00:03:31,000
So when you're angry, we're not trying to get rid of the anger

69
00:03:31,000 --> 00:03:33,000
or replace it with anything else.

70
00:03:33,000 --> 00:03:38,000
We're going to look at it and we're going to see what's going on here.

71
00:03:38,000 --> 00:03:40,000
We're going to try to understand it.

72
00:03:40,000 --> 00:03:42,000
And as a result, we're going to be able to say to ourselves,

73
00:03:42,000 --> 00:03:45,000
you know, this is really not a good thing.

74
00:03:45,000 --> 00:03:48,000
This is really not something that's beneficial to me.

75
00:03:48,000 --> 00:03:53,000
And if it is beneficial, then we'll be able to see that as well.

76
00:03:53,000 --> 00:03:57,000
We're not trying to be a dogmatist where we say that this is bad.

77
00:03:57,000 --> 00:03:58,000
It's good.

78
00:03:58,000 --> 00:04:02,000
What we're trying to do is to look and to see for ourselves, is it good?

79
00:04:02,000 --> 00:04:04,000
Is there something good about anger?

80
00:04:04,000 --> 00:04:08,000
Is there something good about a greed, about attachment?

81
00:04:08,000 --> 00:04:10,000
Because if there is, then we should keep it.

82
00:04:10,000 --> 00:04:12,000
We should develop it.

83
00:04:12,000 --> 00:04:17,000
It's a very open sort of practice.

84
00:04:17,000 --> 00:04:19,000
So what did the Lord Buddha say is suffering?

85
00:04:19,000 --> 00:04:21,000
He said, suffering exists.

86
00:04:21,000 --> 00:04:23,000
I think in many ways, we already know what is the suffering,

87
00:04:23,000 --> 00:04:29,000
not getting what you want to suffering, getting things that you don't want.

88
00:04:29,000 --> 00:04:34,000
Disappointment, sadness, depression.

89
00:04:34,000 --> 00:04:37,000
All of these things are these real sufferings,

90
00:04:37,000 --> 00:04:41,000
just aches and pains. This is a kind of suffering that exists.

91
00:04:41,000 --> 00:04:45,000
But the Lord Buddha said, the real suffering is clinging.

92
00:04:45,000 --> 00:04:49,000
This is really the truth of suffering that the Lord Buddha taught.

93
00:04:49,000 --> 00:04:52,000
Is that clinging or attachment?

94
00:04:52,000 --> 00:04:54,000
This is a suffering.

95
00:04:54,000 --> 00:04:56,000
This is where suffering exists.

96
00:04:56,000 --> 00:05:00,000
None of these things that cause a suffering can really do so

97
00:05:00,000 --> 00:05:02,000
unless we allow them to.

98
00:05:02,000 --> 00:05:05,000
There's nobody else in this world who can cause a suffering.

99
00:05:05,000 --> 00:05:07,000
It's our mind.

100
00:05:07,000 --> 00:05:10,000
That's the eye that exists in this world.

101
00:05:10,000 --> 00:05:16,000
This is my outlet is here.

102
00:05:16,000 --> 00:05:19,000
This is the universe that we think of the universe as one.

103
00:05:19,000 --> 00:05:21,000
This outlet is me, and what is that?

104
00:05:21,000 --> 00:05:25,000
This is my interaction with these experiences.

105
00:05:25,000 --> 00:05:28,000
When there's pain in the body, for instance.

106
00:05:28,000 --> 00:05:30,000
It's a very simple one.

107
00:05:30,000 --> 00:05:36,000
It's very hard to find the clarity of mind

108
00:05:36,000 --> 00:05:41,000
that can simply see the pain for what it is and not be upset by it.

109
00:05:41,000 --> 00:05:46,000
This is exactly what we're trying to do in the meditation practice

110
00:05:46,000 --> 00:05:49,000
that the Buddha taught.

111
00:05:49,000 --> 00:05:53,000
We're trying to see that actually there's nothing about the pain that's negative at all.

112
00:05:53,000 --> 00:05:58,000
It's our decision that we make in our minds that this pain is bad.

113
00:05:58,000 --> 00:06:00,000
This is a negative thing.

114
00:06:00,000 --> 00:06:04,000
This is suffering for me.

115
00:06:04,000 --> 00:06:09,000
And so this is the first normal truth that the Lord Buddha taught is that it's our clinging.

116
00:06:09,000 --> 00:06:12,000
Everything, all of these things that we say are suffering, yes, they are suffering,

117
00:06:12,000 --> 00:06:17,000
but why they are suffering for us is because we're holding on to it.

118
00:06:17,000 --> 00:06:18,000
It's the first one.

119
00:06:18,000 --> 00:06:21,000
The second one is, why are we holding on to it?

120
00:06:21,000 --> 00:06:23,000
What's the cause that leads us to hold on?

121
00:06:23,000 --> 00:06:28,000
This is the truth of the cause of suffering.

122
00:06:28,000 --> 00:06:30,000
Some people say that the Buddha was like a doctor.

123
00:06:30,000 --> 00:06:34,000
He had a sickness and he prescribed the cure to it.

124
00:06:34,000 --> 00:06:38,000
He knew what the problem was and what was the cause.

125
00:06:38,000 --> 00:06:43,000
And he said, what is the path that leads to freedom from sickness?

126
00:06:43,000 --> 00:06:46,000
Freedom from the problem.

127
00:06:46,000 --> 00:06:49,000
And so this is very much how the four normal truths are set out.

128
00:06:49,000 --> 00:06:52,000
There's the cause of suffering and then the cause of freedom from suffering

129
00:06:52,000 --> 00:06:53,000
and the path that leads.

130
00:06:53,000 --> 00:07:00,000
The cause of suffering is desire or craving, we say.

131
00:07:00,000 --> 00:07:04,000
The word desire is very tricky and there's many people who would argue

132
00:07:04,000 --> 00:07:06,000
that there are many good kinds of desire.

133
00:07:06,000 --> 00:07:09,000
And I would be one of those people who would say that your desire

134
00:07:09,000 --> 00:07:11,000
to come here tonight was a good desire.

135
00:07:11,000 --> 00:07:15,000
But I also say that it's a very semantic sort of desire

136
00:07:15,000 --> 00:07:19,000
because as soon as you got here that desire ceased.

137
00:07:19,000 --> 00:07:23,000
Because you achieved your goal and it was more of an intention

138
00:07:23,000 --> 00:07:25,000
than you could say in desire.

139
00:07:25,000 --> 00:07:28,000
Now what we mean by desire here is this one thing of certain things

140
00:07:28,000 --> 00:07:31,000
or partiality towards certain experiences.

141
00:07:31,000 --> 00:07:34,000
Because remember we're dealing with ultimate reality here.

142
00:07:34,000 --> 00:07:36,000
And reality is one, it is this.

143
00:07:36,000 --> 00:07:39,000
My reality, my experience of reality,

144
00:07:39,000 --> 00:07:41,000
there's only one experience of reality.

145
00:07:41,000 --> 00:07:44,000
I'm not having two different experiences of reality

146
00:07:44,000 --> 00:07:47,000
and I never will have two different experiences of reality.

147
00:07:47,000 --> 00:07:50,000
Only one, it is this way.

148
00:07:50,000 --> 00:07:55,000
So given that, my desire for things to be a different way

149
00:07:55,000 --> 00:07:58,000
than what it is is a cause for suffering for me.

150
00:07:58,000 --> 00:08:00,000
Why? Because it's not that way.

151
00:08:00,000 --> 00:08:03,000
And what happens when things are not the way you want

152
00:08:03,000 --> 00:08:05,000
and you're not getting what you want.

153
00:08:05,000 --> 00:08:07,000
And we just said that that's suffering.

154
00:08:07,000 --> 00:08:09,000
It's a clinging.

155
00:08:09,000 --> 00:08:12,000
First you want it to be a certain way.

156
00:08:12,000 --> 00:08:15,000
It's not that way and suddenly you cling to that at the fact

157
00:08:15,000 --> 00:08:17,000
that it's not the way you want it to be.

158
00:08:17,000 --> 00:08:21,000
You're not able to accept the reality of the situation.

159
00:08:21,000 --> 00:08:25,000
It's important to stress the fact that this craving only exists

160
00:08:25,000 --> 00:08:28,000
wanting things to be in a certain way

161
00:08:28,000 --> 00:08:30,000
or they must be this, they must be that.

162
00:08:30,000 --> 00:08:32,000
Only comes from misunderstanding.

163
00:08:32,000 --> 00:08:34,000
This understanding that they are not that way

164
00:08:34,000 --> 00:08:37,000
or the misunderstanding things for what they are.

165
00:08:37,000 --> 00:08:39,000
Not seeing things clearly for what they are.

166
00:08:39,000 --> 00:08:42,000
And this is where meditation is so important.

167
00:08:42,000 --> 00:08:44,000
Meditation is not going anywhere or becoming anything.

168
00:08:44,000 --> 00:08:47,000
It's simply seeing things for what they are.

169
00:08:47,000 --> 00:08:50,000
Accepting things and saying this is what it is.

170
00:08:50,000 --> 00:08:52,000
Instead of saying I wish it were like that

171
00:08:52,000 --> 00:09:16,000
or I wish it weren't like this.

