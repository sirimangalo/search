1
00:00:00,000 --> 00:00:07,000
Okay, good evening everyone.

2
00:00:07,000 --> 00:00:29,000
Hopefully we've got everything working now.

3
00:00:29,000 --> 00:00:37,000
After a few days away,

4
00:00:37,000 --> 00:00:44,000
meditators are diligently at work.

5
00:00:44,000 --> 00:00:47,000
Coming to the end of the practice,

6
00:00:47,000 --> 00:00:53,000
the big finale.

7
00:00:53,000 --> 00:01:00,000
It's a time when it's useful to look back on where we've come

8
00:01:00,000 --> 00:01:11,000
and get a sense there for thereby of where we're going.

9
00:01:11,000 --> 00:01:16,000
And useful I think for everyone else to get a sense of

10
00:01:16,000 --> 00:01:22,000
what it is we're going through when we do the meditation course.

11
00:01:22,000 --> 00:01:26,000
Get a sense of the path that we're practicing.

12
00:01:26,000 --> 00:01:28,000
So I thought I'd do that in brief.

13
00:01:28,000 --> 00:01:31,000
Go through that with you.

14
00:01:31,000 --> 00:01:33,000
Also just as encouragement,

15
00:01:33,000 --> 00:01:38,000
how far you've come with great things you've done.

16
00:01:38,000 --> 00:01:41,000
It's my job to give you encouragement.

17
00:01:41,000 --> 00:01:44,000
So maybe you think while I'm just saying that,

18
00:01:44,000 --> 00:01:47,000
but really it's true.

19
00:01:47,000 --> 00:01:53,000
And the power that you can feel and that you can see

20
00:01:53,000 --> 00:01:55,000
that you gain from the meditation practice.

21
00:01:55,000 --> 00:01:57,000
That doesn't come from nowhere.

22
00:01:57,000 --> 00:02:00,000
It comes from all the hard work you've done.

23
00:02:00,000 --> 00:02:02,000
First and morality.

24
00:02:02,000 --> 00:02:09,000
The first step on the path is to regulate your actions

25
00:02:09,000 --> 00:02:13,000
and your speech.

26
00:02:13,000 --> 00:02:17,000
Just by not engaging in all sorts of distracting

27
00:02:17,000 --> 00:02:22,000
and we call on wholesome activities,

28
00:02:22,000 --> 00:02:27,000
those activities which distract the mind.

29
00:02:27,000 --> 00:02:32,000
Bring the mind out of a state of peace.

30
00:02:32,000 --> 00:02:37,000
Those activities that involve desire, addiction,

31
00:02:37,000 --> 00:02:44,000
aversion, conceit, arrogance, views.

32
00:02:51,000 --> 00:02:53,000
It's quite eye-opening when you go out into the world

33
00:02:53,000 --> 00:02:57,000
after doing a meditation course and you realize

34
00:02:57,000 --> 00:03:06,000
just how constantly we're bombarded by challenging situations

35
00:03:06,000 --> 00:03:10,000
and bombarded by other people's defilements

36
00:03:10,000 --> 00:03:14,000
and stimuli that encourage our own defilements.

37
00:03:14,000 --> 00:03:19,000
So it's great work that you're doing to come here

38
00:03:19,000 --> 00:03:23,000
and just to stay here, to put yourself in a position

39
00:03:23,000 --> 00:03:28,000
and where you're not engaging in all those things.

40
00:03:28,000 --> 00:03:32,000
And moreover to regulate your activities

41
00:03:32,000 --> 00:03:36,000
so that it's as clear and as peaceful

42
00:03:36,000 --> 00:03:41,000
and as focused as possible.

43
00:03:41,000 --> 00:03:43,000
This is why we're talking about running

44
00:03:43,000 --> 00:03:47,000
better not to go for a run while you're here.

45
00:03:47,000 --> 00:03:52,000
But so many things, so many distractions

46
00:03:52,000 --> 00:03:54,000
we've done away with.

47
00:03:54,000 --> 00:04:00,000
Just by maintaining this routine of walking

48
00:04:00,000 --> 00:04:04,000
and sitting throughout the day,

49
00:04:04,000 --> 00:04:06,000
so we call morality.

50
00:04:06,000 --> 00:04:08,000
This is the Buddhist sense of morality.

51
00:04:08,000 --> 00:04:12,000
It's not about killing and stealing, not exactly.

52
00:04:12,000 --> 00:04:16,000
And there are many things we can say are immoral.

53
00:04:16,000 --> 00:04:21,000
But morality really comes down to the focus of the mind

54
00:04:21,000 --> 00:04:25,000
through the focus of the body

55
00:04:25,000 --> 00:04:30,000
by acting and speaking in such ways

56
00:04:30,000 --> 00:04:37,000
that we remove the opportunity

57
00:04:37,000 --> 00:04:48,000
or the impulse to do unwholesome actions.

58
00:04:51,000 --> 00:04:54,000
So that's the first step because it focuses your mind.

59
00:04:54,000 --> 00:04:57,000
So the second step is this focus that you gain.

60
00:04:57,000 --> 00:05:02,000
It was what you're doing every moment that your mind falls.

61
00:05:02,000 --> 00:05:07,000
You're cultivating this focus, focusing your attention

62
00:05:07,000 --> 00:05:14,000
on that moment, one moment after one moment.

63
00:05:14,000 --> 00:05:19,000
And as it becomes habitual, as you become comfortable with it

64
00:05:19,000 --> 00:05:28,000
or accustomed to it, it gains a power.

65
00:05:28,000 --> 00:05:33,000
There's a strength in repetition.

66
00:05:33,000 --> 00:05:37,000
The more and more often you repeat something,

67
00:05:37,000 --> 00:05:39,000
the more it becomes a habit,

68
00:05:39,000 --> 00:05:44,000
the more it becomes a part of you.

69
00:05:44,000 --> 00:05:49,000
The more powerful it becomes.

70
00:05:49,000 --> 00:05:53,000
So you gain this momentum and this is what we call concentration.

71
00:05:53,000 --> 00:05:57,000
It may not feel concentrated because your mind is still trying to play tricks

72
00:05:57,000 --> 00:06:00,000
on you and there's lots of distractions.

73
00:06:00,000 --> 00:06:02,000
But you have a power.

74
00:06:02,000 --> 00:06:07,000
You have a true concentration in the sense that you're able to stay present.

75
00:06:07,000 --> 00:06:14,000
It's like riding on a bull, this rodeo bull.

76
00:06:14,000 --> 00:06:18,000
The challenge is to stay on.

77
00:06:18,000 --> 00:06:23,000
And so it feels like you're flailing wildly,

78
00:06:23,000 --> 00:06:27,000
but you're staying on the bull.

79
00:06:27,000 --> 00:06:29,000
And that's the reality.

80
00:06:29,000 --> 00:06:36,000
Reality is not a peaceful, still forest pool.

81
00:06:36,000 --> 00:06:40,000
Reality is in constant, unpredictable.

82
00:06:40,000 --> 00:06:43,000
It's unsatisfying, it's stressful.

83
00:06:43,000 --> 00:06:52,000
It's uncontrollable.

84
00:06:52,000 --> 00:06:55,000
And so it's not about what we experience,

85
00:06:55,000 --> 00:06:57,000
it's about how we react to it.

86
00:06:57,000 --> 00:07:00,000
Our concentration is about not reacting to our experiences,

87
00:07:00,000 --> 00:07:04,000
learning how to be focused naturally

88
00:07:04,000 --> 00:07:10,000
on whatever we experience.

89
00:07:10,000 --> 00:07:13,000
After focus comes this whole course of wisdom.

90
00:07:13,000 --> 00:07:17,000
And that's what you've really been getting out of this course.

91
00:07:17,000 --> 00:07:21,000
Once you become focused, you start to see things about yourself.

92
00:07:21,000 --> 00:07:23,000
First, you start to see who you are.

93
00:07:23,000 --> 00:07:24,000
What does it mean by the self?

94
00:07:24,000 --> 00:07:28,000
What do we mean when we talk about myself?

95
00:07:28,000 --> 00:07:34,000
You start to see what's really inside what you're really made of.

96
00:07:34,000 --> 00:07:38,000
You see that there's physical and there's mental.

97
00:07:38,000 --> 00:07:43,000
And really what we're made up of it is experiences.

98
00:07:43,000 --> 00:07:47,000
We have experiences of seeing and hearing and smiling and tasting

99
00:07:47,000 --> 00:07:51,000
and feeling and thinking and it's physical and mental realities

100
00:07:51,000 --> 00:07:56,000
coming together to form experience.

101
00:07:56,000 --> 00:08:08,000
We start to see how the experiences work together,

102
00:08:08,000 --> 00:08:11,000
how some experiences lead to other experiences.

103
00:08:11,000 --> 00:08:14,000
An experience of pain leads you to get upset

104
00:08:14,000 --> 00:08:22,000
and the experience of upset leads you to suffer.

105
00:08:22,000 --> 00:08:27,000
You see how mindfulness leads to calm leads to clarity,

106
00:08:27,000 --> 00:08:30,000
leads to purity.

107
00:08:30,000 --> 00:08:35,000
You see how something desirable leads to wanting,

108
00:08:35,000 --> 00:08:44,000
which leads to clinging, which leads to suffering.

109
00:08:44,000 --> 00:08:50,000
And you start to see really in general that nothing is really worth clinging to.

110
00:08:50,000 --> 00:08:57,000
And this is the slow and steady realization that you come to clear and clear

111
00:08:57,000 --> 00:09:01,000
that you've really come to at this point in the course.

112
00:09:01,000 --> 00:09:03,000
You've seen everything.

113
00:09:03,000 --> 00:09:05,000
Your mind has done all its tricks.

114
00:09:05,000 --> 00:09:11,000
Well, it's done most of its major tricks anyway.

115
00:09:11,000 --> 00:09:16,000
And you're really starting to see that none of it's worth clinging to,

116
00:09:16,000 --> 00:09:25,000
trying to fix, trying to control, not worth it.

117
00:09:25,000 --> 00:09:29,000
And so at this point, it's really just about refining your practice.

118
00:09:29,000 --> 00:09:33,000
All of these stages of knowledge that you've come through means,

119
00:09:33,000 --> 00:09:38,000
you've come to see the things you thought were stable, satisfying, controllable,

120
00:09:38,000 --> 00:09:47,000
unstable, unpredictable, unsatisfying, uncontrollable.

121
00:09:47,000 --> 00:09:55,000
It's not worth even trying to control them, trying to own them, trying to be them.

122
00:09:55,000 --> 00:10:04,000
And your mind pulls back and you become equanimous.

123
00:10:04,000 --> 00:10:12,000
Once you get to this point, well, now you can see where we're at,

124
00:10:12,000 --> 00:10:18,000
where we're at, where we're going.

125
00:10:18,000 --> 00:10:25,000
We talk about samsara and how the cause of suffering is our craving.

126
00:10:25,000 --> 00:10:32,000
Well, the cause of our clinging to samsara and clinging to all of this is craving.

127
00:10:32,000 --> 00:10:36,000
And once you become equanimous, once you let go, the mind really lets go.

128
00:10:36,000 --> 00:10:43,000
So this experience of nibana or nirvana is really just when the mind is not clinging anymore.

129
00:10:43,000 --> 00:10:45,000
It's not reaching anymore.

130
00:10:45,000 --> 00:10:49,000
It's not seeking anymore.

131
00:10:49,000 --> 00:10:52,000
At the end of that seeking, there's the cessation of suffering.

132
00:10:52,000 --> 00:10:57,000
There's an experience of nibana.

133
00:10:57,000 --> 00:10:59,000
That's where we're going.

134
00:10:59,000 --> 00:11:04,000
Nibana is something you can experience just for a short time, a few moments even,

135
00:11:04,000 --> 00:11:07,000
but it really changes your perspective.

136
00:11:07,000 --> 00:11:12,000
It's not magic, but it is a kind of a magic or it seems magical.

137
00:11:12,000 --> 00:11:16,000
Because once you see that, then you know what true peace is.

138
00:11:16,000 --> 00:11:26,000
And all this other happiness and pleasure that we seek in the world just doesn't seem meaningful anymore.

139
00:11:26,000 --> 00:11:32,000
At this point, you realize that you're at a mistake to cling, but once you see nibana,

140
00:11:32,000 --> 00:11:36,000
it's not intellectual anymore.

141
00:11:36,000 --> 00:11:38,000
Your mind is really clear.

142
00:11:38,000 --> 00:11:46,000
You know in your heart, you're no doubt in your mind that sabedamana along the beanie ways.

143
00:11:46,000 --> 00:11:54,000
I know dhamma indeed is worth clinging to.

144
00:11:54,000 --> 00:11:58,000
So that's the path in brief.

145
00:11:58,000 --> 00:12:02,000
A little bit of encouragement.

146
00:12:02,000 --> 00:12:09,000
I'll try to try to come back every night and give you a little bit of encouragement.

147
00:12:09,000 --> 00:12:14,000
But thanks for coming out and appreciation for all your practice.

148
00:12:14,000 --> 00:12:23,000
You can go back and meditate.

149
00:12:23,000 --> 00:12:42,000
We have some questions on the website.

150
00:12:42,000 --> 00:12:47,000
So recommended middle-length discourses and the great disciples.

151
00:12:47,000 --> 00:12:53,000
Recommend reading the others. Yes, the long discourses and numerical discourses.

152
00:12:53,000 --> 00:12:57,000
Yes, recommend all of those.

153
00:12:57,000 --> 00:13:01,000
But they're a little harder to get into as all.

154
00:13:01,000 --> 00:13:06,000
So the middle-length discourses is easier to get into.

155
00:13:06,000 --> 00:13:11,000
But once you've finished it, the long discourses is fairly easy.

156
00:13:11,000 --> 00:13:19,000
It's just they're quite a bit longer. The numerical discourses I don't expect most people will read through it entirely.

157
00:13:19,000 --> 00:13:24,000
There's a lot of repetition and it's a lot of lists and it's very big.

158
00:13:24,000 --> 00:13:28,000
What you call anthology discourses, I'm not sure what that is.

159
00:13:28,000 --> 00:13:32,000
But it's probably the sun-guitinica, the connected discourses.

160
00:13:32,000 --> 00:13:38,000
Connected discourses is just huge and a lot of repetition.

161
00:13:38,000 --> 00:13:43,000
But the bicabodet has a good job of keeping that down.

162
00:13:43,000 --> 00:13:54,000
And I definitely recommend it all. It's just takes time to study.

163
00:13:54,000 --> 00:14:04,000
I think that's really about all I have on the shelf. That's interesting.

164
00:14:04,000 --> 00:14:08,000
Okay, I spoke about teaching as being efficient.

165
00:14:08,000 --> 00:14:12,000
And the question is how does there's no wanting the other people to be happy?

166
00:14:12,000 --> 00:14:16,000
No, there's no wanting other people to be happy.

167
00:14:16,000 --> 00:14:19,000
How does compassion relate to this?

168
00:14:19,000 --> 00:14:23,000
Compassion is a state of mind. When we talk about the Buddha's compassion,

169
00:14:23,000 --> 00:14:27,000
we're talking about the compassion that led him to become a Buddha.

170
00:14:27,000 --> 00:14:31,000
But once he became a Buddha,

171
00:14:31,000 --> 00:14:36,000
the compassion didn't drive him anymore.

172
00:14:36,000 --> 00:14:43,000
And actually true compassion doesn't really drive you.

173
00:14:43,000 --> 00:14:48,000
True compassion is what allows you to help people.

174
00:14:48,000 --> 00:14:51,000
But it's not that you're compassionate,

175
00:14:51,000 --> 00:14:56,000
so you go around trying to change everyone, trying to teach everyone.

176
00:14:56,000 --> 00:15:03,000
So when the Buddha was invited to teach, it was compassion that made him say,

177
00:15:03,000 --> 00:15:06,000
but it's a functional compassion.

178
00:15:06,000 --> 00:15:09,000
It's just a part of his enlightenment. It's very natural.

179
00:15:09,000 --> 00:15:14,000
Compassion is maybe just a word that describes that aspect of being enlightened.

180
00:15:14,000 --> 00:15:17,000
That leads one to not be cruel.

181
00:15:17,000 --> 00:15:21,000
Because that's what compassion is. It's really just not being cruel.

182
00:15:21,000 --> 00:15:29,000
When someone is suffering you act in such a way as to free them.

183
00:15:29,000 --> 00:15:33,000
When they approach you and they want help,

184
00:15:33,000 --> 00:15:37,000
you do what you can to help them based on their wish.

185
00:15:37,000 --> 00:15:39,000
Now the question you have is,

186
00:15:39,000 --> 00:15:42,000
what about the Buddha telling all these arrogance to go forth and teach?

187
00:15:42,000 --> 00:15:47,000
I mean, the only reason he did that is because he made a promise to spread the teaching.

188
00:15:47,000 --> 00:15:51,000
So it's still based on this idea that people are asking,

189
00:15:51,000 --> 00:15:53,000
people are looking for it.

190
00:15:53,000 --> 00:15:55,000
It has to be that way.

191
00:15:55,000 --> 00:15:57,000
I mean, for two reasons, first of all,

192
00:15:57,000 --> 00:16:01,000
because it's the nature of an enlightened being not to cling,

193
00:16:01,000 --> 00:16:03,000
so you have no desire for anything.

194
00:16:03,000 --> 00:16:06,000
But also because if you desire to teach,

195
00:16:06,000 --> 00:16:09,000
it doesn't really desire to spread the dhamma.

196
00:16:09,000 --> 00:16:12,000
It's problematic.

197
00:16:12,000 --> 00:16:20,000
There's not a sincerity of reception,

198
00:16:20,000 --> 00:16:24,000
as there is.

199
00:16:24,000 --> 00:16:26,000
Yesterday I was teaching.

200
00:16:26,000 --> 00:16:30,000
It was actually quite good at the Wasek celebration.

201
00:16:30,000 --> 00:16:34,000
It was quite... people were quite appreciative.

202
00:16:34,000 --> 00:16:38,000
But there's always people who seem to be taking it for granted.

203
00:16:38,000 --> 00:16:43,000
And I wanted to be there and they're like, okay, so you want to teach me how to meditate,

204
00:16:43,000 --> 00:16:47,000
or something like, are you going to teach me how to meditate?

205
00:16:47,000 --> 00:16:50,000
Well, if you want me to, I mean,

206
00:16:50,000 --> 00:16:57,000
I guess I put myself out there, guess yesterday.

207
00:16:57,000 --> 00:17:01,000
But people seem to think that it's somehow something I want to do.

208
00:17:01,000 --> 00:17:04,000
It's not really something I'm not talking.

209
00:17:04,000 --> 00:17:10,000
I shouldn't talk too much about myself, but the one thing isn't the thing.

210
00:17:10,000 --> 00:17:15,000
It's that people are looking for it.

211
00:17:15,000 --> 00:17:17,000
And as I said that it's efficient.

212
00:17:17,000 --> 00:17:19,000
It's just a great way to live your life,

213
00:17:19,000 --> 00:17:25,000
and very helpful for your own practice.

214
00:17:25,000 --> 00:17:27,000
Explain that bad habits are dangerous

215
00:17:27,000 --> 00:17:31,000
because they increase the tendency to like or dislike.

216
00:17:31,000 --> 00:17:35,000
More functional habits, cleaning the counter when it's dirty.

217
00:17:35,000 --> 00:17:37,000
Is there any danger connected to those habits?

218
00:17:37,000 --> 00:17:39,000
No.

219
00:17:39,000 --> 00:17:42,000
I mean, not theoretically, but practically,

220
00:17:42,000 --> 00:17:47,000
most of our functional habits are associated with likes and dislikes,

221
00:17:47,000 --> 00:17:49,000
and those are the habits.

222
00:17:49,000 --> 00:17:52,000
Those are the aspects of the habit that we want to change.

223
00:17:52,000 --> 00:17:54,000
So a habit, as you're thinking of it,

224
00:17:54,000 --> 00:18:00,000
is many, many different habits as we would think of them in Buddhism.

225
00:18:00,000 --> 00:18:04,000
So the habit of cleaning may have habitual anger associated with it.

226
00:18:04,000 --> 00:18:07,000
Why do I have to clean, or this is smelly, or this is dirty,

227
00:18:07,000 --> 00:18:10,000
or so on, boredom, maybe?

228
00:18:10,000 --> 00:18:13,000
Or maybe there's a liking of it.

229
00:18:13,000 --> 00:18:16,000
Do you like the cleanliness of the place?

230
00:18:16,000 --> 00:18:20,000
You like the idea of having a clean house and so on.

231
00:18:20,000 --> 00:18:25,000
And so those become habitual, and they build those are the bad habits.

232
00:18:25,000 --> 00:18:28,000
But the habits you're talking about are a little bit more complicated

233
00:18:28,000 --> 00:18:30,000
than the complex habits.

234
00:18:30,000 --> 00:18:31,000
So they can change.

235
00:18:31,000 --> 00:18:35,000
You can purify that so that you still clean the counter.

236
00:18:35,000 --> 00:18:42,000
But you do it free from the habit of disliking or liking or so on,

237
00:18:42,000 --> 00:18:47,000
or attachment, any sort.

238
00:18:47,000 --> 00:18:51,000
Is it fair to say that Upadana requires Dittiwi Palasa?

239
00:18:51,000 --> 00:18:53,000
No, I don't know where you're going.

240
00:18:53,000 --> 00:18:57,000
Sankadhi is not practical question.

241
00:18:57,000 --> 00:18:59,000
I don't have an answer.

242
00:18:59,000 --> 00:19:03,000
I need to now be done with Sankadhi to give you the answers to this question.

243
00:19:03,000 --> 00:19:11,000
I think.

244
00:19:11,000 --> 00:19:16,000
I'm clicking close on these.

245
00:19:16,000 --> 00:19:22,000
I answered the question.

246
00:19:22,000 --> 00:19:27,000
When searching, you can't find any information regarding mudras.

247
00:19:27,000 --> 00:19:31,000
Well, we don't teach mudras in my tradition.

248
00:19:31,000 --> 00:19:35,000
Vija Pachea Sankara means that all kamas are caused by ignorance.

249
00:19:35,000 --> 00:19:36,000
Yes.

250
00:19:36,000 --> 00:19:38,000
Including the crucial kamas.

251
00:19:38,000 --> 00:19:39,000
Yes.

252
00:19:39,000 --> 00:19:41,000
How do you explain the aid to kamas?

253
00:19:41,000 --> 00:19:42,000
I don't know.

254
00:19:42,000 --> 00:19:45,000
You have to talk to an amidama scholar again.

255
00:19:45,000 --> 00:19:48,000
But in a general practical sense,

256
00:19:48,000 --> 00:19:56,000
an aura hunt has no karmic potency.

257
00:19:56,000 --> 00:20:02,000
The things that they do, and they have a special kind of mind that is just purely functional.

258
00:20:02,000 --> 00:20:08,000
I'm doing this because it's really just because I'm doing it honestly.

259
00:20:08,000 --> 00:20:13,000
But see, the thing is you can't have that mind with evil deeds

260
00:20:13,000 --> 00:20:18,000
because they're too complicated, they're too perverse.

261
00:20:18,000 --> 00:20:21,000
That's the theory.

262
00:20:21,000 --> 00:20:24,000
It's all fairly technical.

263
00:20:24,000 --> 00:20:25,000
Okay.

264
00:20:25,000 --> 00:20:30,000
Well, I answered most of them deleted a couple.

265
00:20:30,000 --> 00:20:33,000
That's our questions for tonight.

266
00:20:33,000 --> 00:20:35,000
So thank you all for coming out.

267
00:20:35,000 --> 00:20:37,000
Let's see you all.

268
00:20:37,000 --> 00:20:38,000
I guess tomorrow.

269
00:20:38,000 --> 00:20:41,000
I was thinking maybe cutting down the days that I give talks,

270
00:20:41,000 --> 00:20:45,000
but just give a short talks now.

271
00:20:45,000 --> 00:20:46,000
Okay.

272
00:20:46,000 --> 00:21:15,000
Have a good night.

