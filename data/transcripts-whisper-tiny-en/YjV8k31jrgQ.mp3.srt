1
00:00:00,000 --> 00:00:04,000
How different is Chanda from Dhanha?

2
00:00:04,000 --> 00:00:07,000
There's Kama-tanda and then there's Kousalachanda.

3
00:00:07,000 --> 00:00:10,000
Obviously, Kama-tanda involves craving.

4
00:00:10,000 --> 00:00:12,000
But how does Kousalachanda work?

5
00:00:12,000 --> 00:00:15,000
Is it fair to say Chanda is mere inclination?

6
00:00:15,000 --> 00:00:19,000
So we'll be referring the Dhamma as opposed to gossip.

7
00:00:19,000 --> 00:00:22,000
Chanda is like your inclination.

8
00:00:22,000 --> 00:00:25,000
Is that what you said? Yes, inclination.

9
00:00:25,000 --> 00:00:28,000
The truth is these are just words.

10
00:00:28,000 --> 00:00:31,000
Here's a good example of where the Abidhamma helps.

11
00:00:31,000 --> 00:00:36,000
Because the Abidhamma will categorize all of the realities.

12
00:00:36,000 --> 00:00:40,000
And in the base of the Abidhamma, Dhanha,

13
00:00:40,000 --> 00:00:42,000
neither of these things actually exist.

14
00:00:42,000 --> 00:00:46,000
Dhanha and Chanda are not in the, as far as I've said,

15
00:00:46,000 --> 00:00:49,000
in the Abidhamma, except maybe where they're categorized

16
00:00:49,000 --> 00:00:54,000
or where they're explained away as one of the realities.

17
00:00:54,000 --> 00:00:58,000
The reality is Loba. It's a Jaitasika.

18
00:00:58,000 --> 00:01:00,000
Loba is unwholesome.

19
00:01:00,000 --> 00:01:03,000
So the point is that when you say Kousalachanda,

20
00:01:03,000 --> 00:01:06,000
you're talking about a mind that doesn't have any Loba,

21
00:01:06,000 --> 00:01:09,000
which is a desire.

22
00:01:09,000 --> 00:01:11,000
But again, these are just words.

23
00:01:11,000 --> 00:01:13,000
And you have to say Loba and you have to figure out

24
00:01:13,000 --> 00:01:16,000
what are the characteristics of Loba?

25
00:01:16,000 --> 00:01:17,000
And it's not logical.

26
00:01:17,000 --> 00:01:19,000
It's not about logical reason.

27
00:01:19,000 --> 00:01:21,000
It's about realize, it's about experience.

28
00:01:21,000 --> 00:01:24,000
So you will experience certain minds as having something

29
00:01:24,000 --> 00:01:28,000
called Loba as having this kind of pull that is

30
00:01:28,000 --> 00:01:30,000
inclining you towards something,

31
00:01:30,000 --> 00:01:33,000
or dragging you towards something,

32
00:01:33,000 --> 00:01:35,000
exciting you in regards to something.

33
00:01:35,000 --> 00:01:41,000
And each of the Jaitasikas have different qualities.

34
00:01:41,000 --> 00:01:44,000
Chanda and Dhanha are not actual realities.

35
00:01:44,000 --> 00:01:47,000
So the best way to answer these sorts of questions is to go

36
00:01:47,000 --> 00:01:51,000
to the Abhidhamma and say look,

37
00:01:51,000 --> 00:01:53,000
if you want to talk about these things,

38
00:01:53,000 --> 00:01:56,000
you have to give me the specific example of a mind.

39
00:01:56,000 --> 00:01:58,000
And then we can say, is that mind have Loba,

40
00:01:58,000 --> 00:02:00,000
or does that mind not have Loba?

41
00:02:00,000 --> 00:02:02,000
Because it's possible to have a Chanda,

42
00:02:02,000 --> 00:02:06,000
which is Dhanmachanda, or Kousalachanda,

43
00:02:06,000 --> 00:02:14,000
which has no Loba in it, or Moha in it, either.

44
00:02:14,000 --> 00:02:18,000
Dhanmachanda is always Loba.

45
00:02:18,000 --> 00:02:19,000
When we talk about Dhanmachanda,

46
00:02:19,000 --> 00:02:22,000
we're always talking about Loba as far as I know.

47
00:02:22,000 --> 00:02:24,000
But again, it's just a word that means thirst,

48
00:02:24,000 --> 00:02:25,000
as far as I understand.

49
00:02:25,000 --> 00:02:29,000
So it's semantics.

50
00:02:29,000 --> 00:02:31,000
It depends how you use the word.

51
00:02:31,000 --> 00:02:34,000
We said thirst for meditation practice

52
00:02:34,000 --> 00:02:37,000
could potentially be wholesome,

53
00:02:37,000 --> 00:02:40,000
depending on which Jaitasikas are,

54
00:02:40,000 --> 00:02:41,000
depending on the Abhidhamma.

55
00:02:41,000 --> 00:02:45,000
So again, the importance or the usefulness of the Abhidhamma.

56
00:02:45,000 --> 00:03:12,000
One useful name.

