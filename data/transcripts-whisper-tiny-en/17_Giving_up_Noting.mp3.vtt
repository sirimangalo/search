WEBVTT

00:00.000 --> 00:03.360
Giving up noting.

00:03.360 --> 00:09.240
In the tradition I have been practicing, Bhante Gunna Ratana has told me not to note

00:09.240 --> 00:16.000
mental phenomena such as thinking, itching, et cetera, because it is likely you will become

00:16.000 --> 00:22.520
fixated on the words and fixated on the mental noting, but I have noticed you teach this

00:22.520 --> 00:23.680
practice.

00:23.680 --> 00:29.680
Would you eventually have people deviate from this practice to examine the mental phenomena

00:29.680 --> 00:32.240
and the sensation without notation?

00:32.240 --> 00:33.240
No.

00:33.240 --> 00:38.640
People often ask if there will be a point when I tell them to stop noting, and the answer

00:38.640 --> 00:39.640
is no.

00:39.640 --> 00:41.120
There is no such point.

00:41.120 --> 00:45.000
There are so many different opinions about noting meditation.

00:45.000 --> 00:48.720
Some people say noting is samata meditation.

00:48.720 --> 00:56.800
Not noting is vipassana.

00:56.800 --> 01:02.760
I have heard people say noting inhibits or prevents you from developing concentration.

01:02.760 --> 01:07.280
I have heard people say noting gives you too much concentration.

01:07.280 --> 01:12.240
Everyone has an opinion about it, and it is quite funny to listen to all these opinions.

01:12.240 --> 01:14.200
The Buddha's teaching is A.E.

01:14.200 --> 01:23.080
If it does cause you to fixate on the words, then you have come and sing, and you should

01:23.080 --> 01:25.400
go and see another teacher instead.

01:25.400 --> 01:28.960
I have never seen that for myself or with my students.

01:28.960 --> 01:34.600
If you are fixating on the words, then you are not practicing meditation, and you should

01:34.600 --> 01:36.520
acknowledge the fixating.

01:36.520 --> 01:39.800
If you are investigating, say investigating.

01:39.800 --> 01:45.280
In situations where you think you are fixating, or you feel you are fixating, it is more

01:45.280 --> 01:49.200
important to look at the feeling and the judgment of the practice.

01:49.200 --> 01:51.720
That is almost always the problem.

01:51.720 --> 01:57.280
It may be that your practice is fine, but then doubt arises because we are generally prone

01:57.280 --> 01:58.560
to doubt things.

01:58.560 --> 02:03.120
When the doubt comes up, acknowledge the doubting, and you will see that just like everything

02:03.120 --> 02:04.920
else, it disappears.

02:04.920 --> 02:09.480
How can you fixate on the words if you are watching the doubting, and you are saying to

02:09.480 --> 02:12.200
yourself, doubting, doubting.

02:12.200 --> 02:15.840
After noting, you will notice that the doubting has ceased.

02:15.840 --> 02:19.240
Isn't that impermanence what we are trying to see?

02:19.240 --> 02:24.280
If all you can see is words, you have a problem with your understanding of the practice.

02:24.280 --> 02:30.080
I remember when I first started, all I saw were words in my head as a very visual person.

02:30.080 --> 02:35.760
If I noted, seeing, seeing, there would be those words in my head, my initial practice

02:35.760 --> 02:37.240
was crazy practice.

02:37.240 --> 02:41.880
I heard them talking about the middle way, and so I tried to envision this line down

02:41.880 --> 02:45.040
the middle of my body, and I was trying to open this up.

02:45.040 --> 02:50.040
I saw this white line, and I was like trying to get into the middle way somehow.

02:50.040 --> 02:55.880
The teacher at the time said, practice is like a hammer, and then I got this big headache,

02:55.880 --> 03:01.000
and so I started to visualize my brain, and I was imagining hitting it with the hammer

03:01.000 --> 03:03.160
trying to break it open.

03:03.160 --> 03:06.600
Everything the teacher said to me, I was just visualizing it.

03:06.600 --> 03:10.640
It was pretty terrible, but I had nothing to do with the noting practice, and everything

03:10.640 --> 03:13.800
to do with my understanding or lack thereof.

03:13.800 --> 03:19.520
Once I started appreciating the noting as simply a reminder meant to prevent reactions,

03:19.520 --> 03:24.320
I was able to separate the craziness of my head from the practice that was leading me

03:24.320 --> 03:25.400
to tame it.

03:25.400 --> 03:30.280
If it were the case that noting made you fixated on the words themselves, then mantra

03:30.280 --> 03:35.480
meditation in general would be useless, and the word mantra would never have come into

03:35.480 --> 03:36.920
popular usage.

03:36.920 --> 03:43.520
In reality, however, a mantra does have power and does focus your mind on an object,

03:43.520 --> 03:45.240
whatever the object might be.

03:45.240 --> 03:50.080
If noting caused you to fix the same on the words, then the fisudimaga would be wrong

03:50.080 --> 03:53.680
when it describes it again and again, the use of these words.

03:53.680 --> 03:59.000
According to the fisudimaga, when you practice the earth kasina, you focus on a disc

03:59.000 --> 04:05.680
of earth and repeat one of the various names of earth like fat devi or rumi, repeating it a hundred

04:05.680 --> 04:08.200
times or a thousand times.

04:08.200 --> 04:12.920
The fisudimaga is absolutely clear about the use of a mantra.

04:12.920 --> 04:19.200
When you practice the white kasina, you repeat to yourself white, white, white.

04:19.200 --> 04:23.720
When you practice meta meditation, you repeat a mantra of kindness.

04:23.720 --> 04:29.720
When you practice the 32 constituent parts, you say the name of each part of the body

04:29.720 --> 04:35.240
in order, or you pick one like focusing on the hair and saying, hair, hair.

04:35.240 --> 04:40.200
This is accepted meditation practice since ancient times, and with good reason because

04:40.200 --> 04:43.440
mantras have great power and benefit to the mind.

04:43.440 --> 04:49.360
When the Buddha said, get chanto, bhagga, chami, di, bhagga, nati, which means when

04:49.360 --> 04:56.360
going, when knows I am going, it is not when knows the going, when knows fully and completely

04:56.360 --> 04:57.760
I am going.

04:57.760 --> 05:01.280
So there arises a clear thought about the experience.

05:01.280 --> 05:02.960
This is not something new.

05:02.960 --> 05:09.360
The Buddha taught such practice, and the fisudimaga describes explicitly that this is what

05:09.360 --> 05:11.160
is meant by meditating.

05:11.160 --> 05:16.640
Meditation means to focus one's attention on the object, just as it is without diversity

05:16.640 --> 05:22.320
of thought, to see white as white, to see blue as blue, to see the earth as earth, and so

05:22.320 --> 05:23.320
on.

05:23.320 --> 05:53.160
Nature is the tool used to see things this way.

