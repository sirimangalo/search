1
00:00:00,000 --> 00:00:07,000
Hello, fellow YouTubers.

2
00:00:07,000 --> 00:00:14,000
Today I'm here to give you an update on our DVD video offering on how to meditate.

3
00:00:14,000 --> 00:00:22,000
Now, in one of my earlier videos, I made it clear that we had this DVD available to

4
00:00:22,000 --> 00:00:27,000
ship to people around the world who are interested in learning how to meditate.

5
00:00:27,000 --> 00:00:37,000
Indeed, that's what we've been doing for the past year, is mailing these out to people around the world who are interested in our work.

6
00:00:37,000 --> 00:00:46,000
The DVD we've been offering for free, although we have made it clear that we're required to recoup our costs,

7
00:00:46,000 --> 00:00:51,000
the cost of production, which we estimate to be around $5 per disk.

8
00:00:51,000 --> 00:00:58,000
Now, that's probably going to go down as we recover the cost for, say, the first 1000 disks.

9
00:00:58,000 --> 00:01:03,000
But we're still required to pay for the shipping for each disk.

10
00:01:03,000 --> 00:01:10,000
So if anyone would like to donate for the cost of the disks, you're certainly welcome to do so on our website.

11
00:01:10,000 --> 00:01:17,000
But today I'm here to announce that not only are we giving out the disk on how to meditate,

12
00:01:17,000 --> 00:01:23,000
but we've made a second disk, which includes my videos on why everyone should meditate.

13
00:01:23,000 --> 00:01:28,000
So now we have a two disk set, which we're shipping to locations around the world,

14
00:01:28,000 --> 00:01:31,000
and giving out to people who come to visit us as well.

15
00:01:31,000 --> 00:01:43,000
We have a great number of DVDs, which we've just printed, and we'll be printing continuously for as long as there is interest in our work and giving them out for free.

16
00:01:43,000 --> 00:01:49,000
So if you would like a free DVD, or if you would like to sponsor the donation process,

17
00:01:49,000 --> 00:01:54,000
if you would like to become a donor and support our work, come on entirely.

18
00:01:54,000 --> 00:02:03,000
And then you're welcome to do so, either to request a DVD or to support our work at our website on this very munkle of paint.

19
00:02:03,000 --> 00:02:10,000
I'd like to thank everyone for your interest in our work, and I appreciate all the feedback and comments that everyone's been giving.

20
00:02:10,000 --> 00:02:26,000
And I look forward to spreading and helping to spread the Buddhist teaching and looking forward to getting people's help and people's continued commitment in both the practice and in supporting the practice of meditation.

21
00:02:26,000 --> 00:02:39,000
So thank you all who have already supported our practice, and also thank you for those who have sent us feedback to receive the DVD and have sent us feedback on how well it was helping.

22
00:02:39,000 --> 00:02:45,000
Then to find peace, happiness, and freedom from suffering.

23
00:02:45,000 --> 00:02:50,000
So please don't be shy and feel free to send us a note asking for a DVD.

24
00:02:50,000 --> 00:02:54,000
Please include your mailing address, otherwise we have no way to send it to you.

25
00:02:54,000 --> 00:02:56,000
And thanks again, thanks.

26
00:02:56,000 --> 00:03:01,000
You all for tuning in to our videos, and look forward to hearing from you in the future.

27
00:03:01,000 --> 00:03:11,000
Have a good day.

