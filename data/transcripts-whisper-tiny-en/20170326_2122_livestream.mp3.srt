1
00:01:00,000 --> 00:01:03,840
Okay, well not a lot.

2
00:01:03,840 --> 00:01:08,720
Let's go through them then and those who have asked them will just have to come if they

3
00:01:08,720 --> 00:01:19,560
want to get the answers and they're not here, then they'll have to go and listen to the

4
00:01:19,560 --> 00:01:20,560
audio recording.

5
00:01:20,560 --> 00:01:21,560
Hopefully this is being recorded.

6
00:01:21,560 --> 00:01:29,960
I'm not sure how the new system works, but I think it's being recorded.

7
00:01:29,960 --> 00:01:31,280
Okay, so I'm going to go through these.

8
00:01:31,280 --> 00:01:32,280
What is contemplating?

9
00:01:32,280 --> 00:01:36,720
Because I've been noting my mind, is there a difference between the practice between

10
00:01:36,720 --> 00:01:41,400
just discarding an object by noting it or really looking at it, observing it through

11
00:01:41,400 --> 00:01:45,800
every reaction of the mind, so it's a lot of considered contemplating.

12
00:01:45,800 --> 00:01:52,200
I don't know, I mean, contemplating is just a word, it's not really a word that I use

13
00:01:52,200 --> 00:01:57,960
in my vocabulary to talk about what we do, so I think it depends how you define it.

14
00:01:57,960 --> 00:02:04,120
If that's how you define it and so you define it as to whether there's a difference between

15
00:02:04,120 --> 00:02:10,520
just discarding an object by noting it and actually contemplating it or I don't know

16
00:02:10,520 --> 00:02:16,160
doing something else, yes, there's quite a difference.

17
00:02:16,160 --> 00:02:21,480
Mindfulness is meant to grasp the object, so you're meant to see it quite clearly, but

18
00:02:21,480 --> 00:02:26,040
then discard it completely, I mean you're not meant to do anything with it, you're

19
00:02:26,040 --> 00:02:31,720
meant to move on to the next thing, because that experience is actually gone, you can't

20
00:02:31,720 --> 00:02:42,040
sit there and sit there and examine it like a gem or a diamond, no, it's gone, it's just

21
00:02:42,040 --> 00:02:47,480
a moment, every experience, so the only way you can see a momentary nature of experience

22
00:02:47,480 --> 00:02:53,440
is if you let it go, it's gone, there's nothing else you can do with it, all the other

23
00:02:53,440 --> 00:02:58,760
contemplation stuff comes when it's already gone, so it's just mental stuff, it's not

24
00:02:58,760 --> 00:03:01,080
useful.

25
00:03:01,080 --> 00:03:05,840
When desires come up, I try to note them as wanting wanting, sometimes my mind is still

26
00:03:05,840 --> 00:03:13,360
on the object or desire, should it not be on the mind state of wanting itself, it should

27
00:03:13,360 --> 00:03:21,520
be in all, there's three parts, there's the Rupa, Vedana and Dunha, Rupa is the form, the

28
00:03:21,520 --> 00:03:28,040
thing that you desire, Vedana is the pleasure and Dunha is the desire for it, all three

29
00:03:28,040 --> 00:03:35,480
of those are important, it's important to be mindful of all three, they come up, there

30
00:03:35,480 --> 00:03:42,120
is increased restlessness and I try to move on to noting that, so you can note all of

31
00:03:42,120 --> 00:03:53,360
these, I mean you should note all of these, oh I'm supposed to wait, what can I do, I

32
00:03:53,360 --> 00:04:00,960
don't know, I'm just deleting them, when investigating the rise, arising and passing away

33
00:04:00,960 --> 00:04:09,720
of sensations, I often see it's beginning and it's end, I don't see any middle section,

34
00:04:09,720 --> 00:04:14,240
there isn't one, the middle section would be just a different sensation, the length of

35
00:04:14,240 --> 00:04:20,680
the sensation, yeah I wouldn't worry about it, there is some controversy I think surrounding

36
00:04:20,680 --> 00:04:28,720
that, is there really a resistance state or is it just arising and ceasing and the

37
00:04:28,720 --> 00:04:36,360
commentaries are pretty clear, there is an existing state, but I think it's just technical,

38
00:04:36,360 --> 00:04:47,600
I don't think there's anything, I don't think there's anything, I wouldn't worry too

39
00:04:47,600 --> 00:04:58,360
much about this, I have many issues from the past that seem to be dredged up, I would

40
00:04:58,360 --> 00:05:03,360
describe these as regrets, so frequent thoughts and it seems that I am often chewing on

41
00:05:03,360 --> 00:05:11,680
them, what regrets is a negative, it's a disliking, so it means there's a disliking associated

42
00:05:11,680 --> 00:05:21,120
with it, there's a thought, a disliking, an anger of sorts, and maybe sadness or so on

43
00:05:21,120 --> 00:05:28,240
as well, but sadness is really just anger, but you cannot, so you're not sad, sad, angry,

44
00:05:28,240 --> 00:05:33,960
disliking, disliking, if you feel guilty and say guilty and guilty, I mean all of these

45
00:05:33,960 --> 00:05:39,280
you have to work through, it's not something that disappears overnight, but it can be

46
00:05:39,280 --> 00:05:50,280
helped, I mean it's a habit, so we build new habits over time, please send videos, that's

47
00:05:50,280 --> 00:06:01,520
not a question, I mean there's a link at the top here to our booklet, that would be the

48
00:06:01,520 --> 00:06:12,120
best way, is there a time period between passing away and rebirth, I mean it doesn't,

49
00:06:12,120 --> 00:06:19,360
I don't really know, but what's important is that we're born and die every moment, so

50
00:06:19,360 --> 00:06:28,040
there can be no in between, wherever we are we're in some form or other, there's no

51
00:06:28,040 --> 00:06:34,840
bardo like the Tibetans say, I mean whatever that bardo is, it's a realm, so I'm not

52
00:06:34,840 --> 00:06:41,840
surely how it's portrayed in Tibetan Buddhism, but in that time one is something somewhere,

53
00:06:41,840 --> 00:06:46,880
and so one isn't in between existence, one has already passed away from one existence

54
00:06:46,880 --> 00:06:54,760
and is in some new existence, maybe as a spirit or a ghost, that's quite common, I mean

55
00:06:54,760 --> 00:06:59,080
I think it's maybe a little more complicated than the Theravad and texts seem to make it

56
00:06:59,080 --> 00:07:07,360
out, although it also seems that for some people they do just pass away from this realm

57
00:07:07,360 --> 00:07:12,760
and are born as though they fell asleep and woke up in another realm, for some people

58
00:07:12,760 --> 00:07:21,080
it seems that they stick around, for some beings they might stay in the old realm, and for

59
00:07:21,080 --> 00:07:29,280
some there's a dreamlike state, which they call the bardo, but I don't know, I mean

60
00:07:29,280 --> 00:07:35,520
I guess that is a good point, could you call that another realm, the end is that the

61
00:07:35,520 --> 00:07:47,320
end is that it doesn't matter, it's existing, there's a rising and ceasing, so the

62
00:07:47,320 --> 00:07:56,520
Theravad attacks describe that as Nimita, where you have the story go through, you know

63
00:07:56,520 --> 00:08:01,400
this whole thing about feeling like there's a tunnel and you're moving towards the light

64
00:08:01,400 --> 00:08:08,400
of that kind of thing, in these common descriptions of by people who have had it, and

