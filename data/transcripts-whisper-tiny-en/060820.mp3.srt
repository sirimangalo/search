1
00:00:00,000 --> 00:00:11,000
So today, I'll talk about, I think, number 11 to 14.

2
00:00:11,000 --> 00:00:16,000
Remember, because it's almost.

3
00:00:16,000 --> 00:00:30,000
And that I will tell you.

4
00:00:30,000 --> 00:00:42,060
Care for all one's father's mother,

5
00:00:42,060 --> 00:00:54,580
one's mother, put the darass as a sankahou, to take care of one's family, one's spouse

6
00:00:54,580 --> 00:01:10,100
and one's children, and that will not be a common dar, whatever actions are unconfused

7
00:01:10,100 --> 00:01:27,100
or unentangled, which are clear and free, free from any actions which are free and

8
00:01:27,100 --> 00:01:44,100
are clear, but these are blessings in the highest sense, but that they care of one's father

9
00:01:44,100 --> 00:01:55,860
and mother is something we can see, there's a very important role in the Lord Buddha's

10
00:01:55,860 --> 00:02:08,700
teaching, even as a bodhisattva, before the Lord Buddha had become full in it, in birth

11
00:02:08,700 --> 00:02:16,340
after birth he would look after his parents, or something he had realized early on was

12
00:02:16,340 --> 00:02:32,700
most important, and even once he became an enlightened Buddha, he continued to look after

13
00:02:32,700 --> 00:02:44,860
his parents and think of his parents, and his disciples carried it on the tradition,

14
00:02:44,860 --> 00:02:54,500
it would go in the pass away, he knew that he was old and was ready to pass away, and

15
00:02:54,500 --> 00:03:03,420
he got very sick, and he knew that his time had come, but he knew that he couldn't, he

16
00:03:03,420 --> 00:03:10,540
shouldn't pass away before he had gone back to teach his mother, almost the right path

17
00:03:10,540 --> 00:03:21,420
and the right way to go, and so even though he was sick, he went back to visit his mother

18
00:03:21,420 --> 00:03:37,660
in Nalanda, and even his mother was, his mother was a Brahmin, who lived in ritual

19
00:03:37,660 --> 00:03:50,500
sacrifice, and that the Lord Buddha's teaching was just a bunch of heresy, Sarah put

20
00:03:50,500 --> 00:03:57,140
that out of respect for his mother, in order to support for his mother about his mother,

21
00:03:57,140 --> 00:04:08,300
that was the truth of the Buddha's teaching.

22
00:04:08,300 --> 00:04:13,100
You can see Teravana's mother and father is not just something that is just within a

23
00:04:13,100 --> 00:04:18,100
worldly sense, it's most important in terms of the practice of the Dhamma, whatever

24
00:04:18,100 --> 00:04:29,580
problems we have with our mother and our father will always come back to haunt us during

25
00:04:29,580 --> 00:04:32,780
the practice.

26
00:04:32,780 --> 00:04:41,020
When we practice, most important is to send all of the goodness that we have in our hearts

27
00:04:41,020 --> 00:04:48,540
and all of the good deeds that we do as in the practice of meditation, all the goodness

28
00:04:48,540 --> 00:04:53,180
which comes from the practice of meditation, it's most important that we dedicate the

29
00:04:53,180 --> 00:05:15,620
goodness to our mother and father, something which makes our hearts calm and peaceful and allows us to practice with the attention.

30
00:05:15,620 --> 00:05:21,940
And when we have a chance, we have we should try to find a way to go back and repair our

31
00:05:21,940 --> 00:05:29,580
parents or all the good things that they've done to us, Lord Buddha said it's very difficult

32
00:05:29,580 --> 00:05:35,700
to repay one's parents, because what they've done for us is something that we cannot

33
00:05:35,700 --> 00:05:46,940
redo for them, not in this life, because they were our first teachers, they were our

34
00:05:46,940 --> 00:06:01,340
protectors, supporters, without ever asking for repayment, and so the Lord Buddha taught

35
00:06:01,340 --> 00:06:06,940
that the only way we could really do something that would equal or would be able to rival

36
00:06:06,940 --> 00:06:15,940
the good deeds that they had done for us is if we bring the Dhamma to them.

37
00:06:15,940 --> 00:06:26,940
There's no amount of support we can give to them that could equal the support that they have given to us, except in the Dhamma, except in the truth.

38
00:06:26,940 --> 00:06:36,940
If we bring to them the reality of them to come to see what are good deeds, what are bad deeds,

39
00:06:36,940 --> 00:06:40,940
what is the fruit of bad deeds, what is the fruit of good deeds?

40
00:06:40,940 --> 00:06:52,940
Teach them to have faith in truth, listen to come to see the truth, teach them to have morality

41
00:06:52,940 --> 00:07:01,940
to avoid evil deeds, teach them how to practice we pass in a meditation.

42
00:07:01,940 --> 00:07:13,940
This is a way the Lord Buddha said to repay one's plan.

43
00:07:13,940 --> 00:07:32,940
This is a high blessing because if the support gives it as in the mind, the support it gives to the mind.

44
00:07:32,940 --> 00:07:40,940
As I said, the person who has never supported their parents has done bad things to one's parents,

45
00:07:40,940 --> 00:07:52,940
we find it very difficult to practice meditation.

46
00:07:52,940 --> 00:07:59,940
The blessing which comes from having supported one's parents is the great peace and happiness.

47
00:07:59,940 --> 00:08:08,940
The knowing that one is paid back to people who are very difficult to pay back.

48
00:08:08,940 --> 00:08:19,940
But one brings them goodness and happiness in their hearts.

49
00:08:19,940 --> 00:08:29,940
This is a great blessing and the greatest in the highest sense.

50
00:08:29,940 --> 00:08:39,940
Next is two blessings.

51
00:08:39,940 --> 00:08:48,940
One is looking after one's children, one is looking after house.

52
00:08:48,940 --> 00:09:03,940
These are two blessings which are blessings in worldly sense.

53
00:09:03,940 --> 00:09:09,940
As far as children they have children, they can very easily have children.

54
00:09:09,940 --> 00:09:21,940
Because in Buddhism the word child is used in many different ways.

55
00:09:21,940 --> 00:09:34,940
There is the child that comes through the body.

56
00:09:34,940 --> 00:09:41,940
This is the biological child and it is the biological parent of the child.

57
00:09:41,940 --> 00:09:52,940
In this case it is not like they are not frequent where a monk or a nun is such a child.

58
00:09:52,940 --> 00:10:10,940
They could have from their past life as a life person.

59
00:10:10,940 --> 00:10:16,940
Then there is the child who is a child of the village.

60
00:10:16,940 --> 00:10:23,940
It is living in the same village.

61
00:10:23,940 --> 00:10:31,940
There is an ancient proverb which says that it takes a whole village to raise a child.

62
00:10:31,940 --> 00:10:39,940
This is what this kind of child refers to.

63
00:10:39,940 --> 00:10:55,940
Where the whole village looks after all the children, where we look after children as our own because they live in the same village or live in the same area.

64
00:10:55,940 --> 00:11:01,940
And then the third kind is one who comes to us as a student.

65
00:11:01,940 --> 00:11:13,940
Either someone else's parents bring them to us to look after because we are a teacher or else they come on their own accord.

66
00:11:13,940 --> 00:11:17,940
This becomes our child in the sense of our student.

67
00:11:17,940 --> 00:11:29,940
Someone who we teach at a practice or the patient who teach at a practice with our Buddhist teacher.

68
00:11:29,940 --> 00:11:39,940
And looking after children is the most important thing as well.

69
00:11:39,940 --> 00:11:44,940
Someone else is given to us all the goodness we have inside.

70
00:11:44,940 --> 00:11:58,940
Someone else has helped us to get whatever benefit we can from the spiritual life.

71
00:11:58,940 --> 00:12:10,940
And if we keep to ourselves and don't pass it on, we will end right there.

72
00:12:10,940 --> 00:12:18,940
We find the world will not be able to sustain the goodness.

73
00:12:18,940 --> 00:12:22,940
There is no one to continue the goodness.

74
00:12:22,940 --> 00:12:30,940
Even here in Thailand.

75
00:12:30,940 --> 00:12:37,940
Maybe 50 years ago there was not so much we best in a practice in Thailand.

76
00:12:37,940 --> 00:12:51,940
Until somebody put a chance in the habit of what Mahatad had a wish because he had studied so much.

77
00:12:51,940 --> 00:13:07,940
So much of the Buddhist teaching and he had established so much study of the Buddhist teaching and monastery and all around Thailand even.

78
00:13:07,940 --> 00:13:15,940
But he said what Thailand was missing was meditation practice.

79
00:13:15,940 --> 00:13:23,940
If they didn't have meditation practice there would be no one, there would be no enlightened beings in the country.

80
00:13:23,940 --> 00:13:29,940
There would be no one who could really become free from suffering.

81
00:13:29,940 --> 00:13:35,940
There was no practice, there was no strong concentrated practice every person in a meditation.

82
00:13:35,940 --> 00:13:43,940
There was no one teaching how to practice every person in a meditation.

83
00:13:43,940 --> 00:13:52,940
So he helped to find the teacher and to establish the teaching here in Thailand.

84
00:13:52,940 --> 00:13:58,940
And now we can see Thailand is becoming famous for repassing the meditation.

85
00:13:58,940 --> 00:14:01,940
Watch on town.

86
00:14:01,940 --> 00:14:07,940
It is quite well known.

87
00:14:07,940 --> 00:14:19,940
What lampoon, what Mahatad, what ample one what we make us on this many places around the country

88
00:14:19,940 --> 00:14:23,940
where one can practice repassing the meditation.

89
00:14:23,940 --> 00:14:25,940
Millions of people practicing.

90
00:14:25,940 --> 00:14:38,940
Maybe not millions and thousands and thousands of people practicing.

91
00:14:38,940 --> 00:14:49,940
This is a kind of way of looking after one's children, looking after the generations to come.

92
00:14:49,940 --> 00:15:05,940
The people who come after we are gone to pass on the teaching to keep the world in peace and harmony.

93
00:15:05,940 --> 00:15:12,940
This is one way of explaining Bhutasanga.

94
00:15:12,940 --> 00:15:22,940
But in the world also we can most important that we look after our children or biological

95
00:15:22,940 --> 00:15:25,940
to know the children around us.

96
00:15:25,940 --> 00:15:34,940
But here when we talk about this in the setting of your passing the meditation, we talk about passing on and spreading the teaching,

97
00:15:34,940 --> 00:15:37,940
looking after other people.

98
00:15:37,940 --> 00:15:47,940
Right now we are coming to look after ourselves first, but when we have a chance then we can go and look after other people.

99
00:15:47,940 --> 00:15:54,940
Going to help other people to become free from suffering.

100
00:15:54,940 --> 00:16:03,940
And third blessing is looking after one's spouse, specifically in the worldly sense.

101
00:16:03,940 --> 00:16:15,940
You have to find an equivalent for the recluse and for the monk and for the nun.

102
00:16:15,940 --> 00:16:24,940
Looking after one's spouse is another important establishment in Bhutasanga.

103
00:16:24,940 --> 00:16:37,940
Of course it's just a concept, there's no ultimate reality and the need for a spouse, but in the Bhutas time and as well, in the present time I think it is.

104
00:16:37,940 --> 00:16:46,940
A very important establishment for people who live or living in the world.

105
00:16:46,940 --> 00:17:01,940
Even those followers of the Lord Bhuta who became Anakami, they could still be married, still have a wife, still have a husband.

106
00:17:01,940 --> 00:17:07,940
But they would become disinterested in all sexual activity and all romantic activity.

107
00:17:07,940 --> 00:17:13,940
But they could still live something like brother and sister.

108
00:17:13,940 --> 00:17:27,940
Looking at it is the most helpful and supportive environment to live as husband and wife.

109
00:17:27,940 --> 00:17:30,940
One is to live in the world.

110
00:17:30,940 --> 00:17:35,940
It's very difficult to live alone.

111
00:17:35,940 --> 00:17:42,940
You have to take care of the house which you live in and you have to go out and try to find money, try to find support.

112
00:17:42,940 --> 00:17:49,940
To do both of these things at once is quite difficult.

113
00:17:49,940 --> 00:17:57,940
We have the establishment of the husband and wife.

114
00:17:57,940 --> 00:18:03,940
One whichever one to stay home, whichever one to go on.

115
00:18:03,940 --> 00:18:14,940
Find the support to keep people's life going.

116
00:18:14,940 --> 00:18:21,940
So we support each other by being faithful with one another.

117
00:18:21,940 --> 00:18:29,940
By respecting one another, by taking care of our respective duties.

118
00:18:29,940 --> 00:18:35,940
This is a great blessing in the world.

119
00:18:35,940 --> 00:18:45,940
So we have to do monks and nuns get along if they don't have this kind of support, monks and nuns have to rely only on themselves.

120
00:18:45,940 --> 00:18:48,940
But they also rely on each other.

121
00:18:48,940 --> 00:18:56,940
The monks rely on the other monks, the nuns rely on the other nuns.

122
00:18:56,940 --> 00:19:04,940
They have some kind of very large family as a result of their hardination.

123
00:19:04,940 --> 00:19:07,940
That wherever the monks go, they can find support from other monks.

124
00:19:07,940 --> 00:19:10,940
Wherever the nuns go, they can find support from other nuns.

125
00:19:10,940 --> 00:19:15,940
And even the other way, the monks find support from nuns and nuns find support from monks.

126
00:19:15,940 --> 00:19:20,940
They can support each other insofar as is appropriate.

127
00:19:20,940 --> 00:19:32,940
This is the third blessing.

128
00:19:32,940 --> 00:19:37,940
The fourth one is whatever those deeds which are free and clear.

129
00:19:37,940 --> 00:19:42,940
An aquala, aquala, means fused or entangled.

130
00:19:42,940 --> 00:19:48,940
It could be entangled with deceit or treachery or evil.

131
00:19:48,940 --> 00:19:55,940
It could be confused with delusion or laziness.

132
00:19:55,940 --> 00:20:02,940
It's whatever work we have to do.

133
00:20:02,940 --> 00:20:10,940
We do it with a clear mind.

134
00:20:10,940 --> 00:20:14,940
At the time of the Lord Buddha, there was a teaching called karma.

135
00:20:14,940 --> 00:20:26,940
And action, karma means that every action we do as karma is potential to bring fruits.

136
00:20:26,940 --> 00:20:29,940
Bring other suffering or happiness.

137
00:20:29,940 --> 00:20:35,940
And so nowadays people say the Lord Buddha taught the law of karma.

138
00:20:35,940 --> 00:20:41,940
And it is true that he taught a law of karma.

139
00:20:41,940 --> 00:20:48,940
But if you're really going to call it a law of karma, it's actually the Lord Buddha didn't teach the law of karma.

140
00:20:48,940 --> 00:20:56,940
He thought that it was not that the law of karma was a false law.

141
00:20:56,940 --> 00:21:01,940
Because according to the Lord Buddha, karma is not the most important thing.

142
00:21:01,940 --> 00:21:07,940
Action is not the most important thing.

143
00:21:07,940 --> 00:21:15,940
But rather than say it like that to all the people who believed in karma as a very important thing.

144
00:21:15,940 --> 00:21:18,940
The Lord Buddha was very clever.

145
00:21:18,940 --> 00:21:21,940
He tried to explain what is real karma.

146
00:21:21,940 --> 00:21:25,940
What constitutes a real action which will really bring ourselves.

147
00:21:25,940 --> 00:21:28,940
The Lord Buddha said it is the intention.

148
00:21:28,940 --> 00:21:32,940
It is intention which is the deed.

149
00:21:32,940 --> 00:21:40,940
The intention itself is the deed.

150
00:21:40,940 --> 00:21:45,940
And so when we act with the impure heart, with the heart of greed or anger or delusion,

151
00:21:45,940 --> 00:21:48,940
this is a deed which is called Akulai.

152
00:21:48,940 --> 00:21:57,940
Confused or I'll mix up with evil.

153
00:21:57,940 --> 00:22:10,940
If we just act without any special intention,

154
00:22:10,940 --> 00:22:15,940
it's not true that every action will bring some kind of karma result.

155
00:22:15,940 --> 00:22:19,940
It's only when we have greed or when we have anger or when we have delusion.

156
00:22:19,940 --> 00:22:24,940
So when we walk and we step on an ant without seeing the ant,

157
00:22:24,940 --> 00:22:30,940
we can't say that that's going to bring evil results to us.

158
00:22:30,940 --> 00:22:36,940
But when we have anger in our minds, we kill, we steal,

159
00:22:36,940 --> 00:22:38,940
we do evil things.

160
00:22:38,940 --> 00:22:44,940
For sure this will bring evil results.

161
00:22:44,940 --> 00:22:48,940
First it will bring evil results to our minds.

162
00:22:48,940 --> 00:23:01,940
And later on it will bring revenge and more suffering to it.

163
00:23:01,940 --> 00:23:10,940
And in the opposite sense, whatever deeds we do are accompanied with mindfulness,

164
00:23:10,940 --> 00:23:19,940
and intention to help, and intention to bring freedom from suffering, happiness, and peace.

165
00:23:19,940 --> 00:23:27,940
These deeds will surely bring good results in this life and in the next life.

166
00:23:27,940 --> 00:23:31,940
This is why it's most important that we develop our minds.

167
00:23:31,940 --> 00:23:34,940
Establish our minds in goodness.

168
00:23:34,940 --> 00:23:36,940
See in our minds there are two sides.

169
00:23:36,940 --> 00:23:41,940
There are good things in our minds and there are bad things in our minds.

170
00:23:41,940 --> 00:23:44,940
We always have the opportunity to choose.

171
00:23:44,940 --> 00:23:51,940
Do we want to follow after bad things or do we want to develop ourselves in good things?

172
00:23:51,940 --> 00:23:58,940
When we come to practice we pass into this we made the choice to develop ourselves in good things.

173
00:23:58,940 --> 00:24:03,940
And to get rid of all the bad things in our minds.

174
00:24:03,940 --> 00:24:08,940
So we come to practice we pass in the meditation.

175
00:24:08,940 --> 00:24:11,940
It's the greatest blessing.

176
00:24:11,940 --> 00:24:16,940
It brings all the blessings to us.

177
00:24:16,940 --> 00:24:21,940
Our minds become free from confusion or entanglement.

178
00:24:21,940 --> 00:24:30,940
And everything we do, everything we do is become either a good karma or no karma.

179
00:24:30,940 --> 00:24:36,940
It's never any more bad karma.

180
00:24:36,940 --> 00:24:41,940
Our minds are free from greed, free from anger, free from delusion.

181
00:24:41,940 --> 00:24:50,940
So when we act we will act in a better desire to help other people,

182
00:24:50,940 --> 00:24:54,940
to support other people, to help ourselves, to support ourselves,

183
00:24:54,940 --> 00:24:59,940
to bring only peace and happiness to the world.

184
00:24:59,940 --> 00:25:02,940
This is the 14 blessing number 14.

