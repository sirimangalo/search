1
00:00:00,000 --> 00:00:22,400
Okay. Good evening, everyone. Welcome to our evening

2
00:00:22,400 --> 00:00:41,200
down the session. Tonight we're answering questions, I'm answering questions.

3
00:00:41,200 --> 00:00:50,120
This week is there's a lot of extra work for learning Chinese and a learning

4
00:00:50,120 --> 00:01:00,160
French trying to finish up the last of a bachelor degree that I've been trying to get for

5
00:01:00,160 --> 00:01:21,640
10 years, 19 years. So unfortunately that means I can't put 100% into teaching or

6
00:01:21,640 --> 00:01:32,200
practicing. And I think I think this week I might just not be broadcasting after

7
00:01:32,200 --> 00:01:41,360
tonight. Which means not broadcasting for a while actually. So I'm going to say

8
00:01:41,360 --> 00:01:49,360
that until further notice, broadcasts are suspended after tonight. I think as

9
00:01:49,360 --> 00:01:52,480
I'm going to visit my father in December and then I'll be going to visit my

10
00:01:52,480 --> 00:02:02,040
mother after that. And I'll be back January 3rd. So I would expect that January 3rd

11
00:02:02,040 --> 00:02:10,200
is when I'll start picking up broadcasting. But I do have some idea to continue

12
00:02:10,200 --> 00:02:16,360
to do evening talks here and formally for our meditators. But I think not to

13
00:02:16,360 --> 00:02:25,360
broadcast. It's just too much complication. So for our local audience, let's say

14
00:02:25,360 --> 00:02:34,760
I'll continue Monday, Wednesday, Friday. One Monday, Wednesday, Saturday. You can come

15
00:02:34,760 --> 00:02:40,200
up here but just direct my teaching to you guys and I won't be distracted by

16
00:02:40,200 --> 00:02:45,480
the internet and it won't be long. But I'll just hopefully be related to

17
00:02:45,480 --> 00:02:51,800
your progress in the course. Because the whole reason for giving these

18
00:02:51,800 --> 00:02:55,760
talks is the primary reason is for you guys. It's for people who are coming here

19
00:02:55,760 --> 00:03:09,120
and putting in the extra effort, the supreme effort to realize the core of the

20
00:03:09,120 --> 00:03:21,960
Buddha's teaching in this very life. So anyway, go with that for now and

21
00:03:21,960 --> 00:03:28,560
everything always changes. So we'll see how things change. Okay, so the first

22
00:03:28,560 --> 00:03:34,760
question here got 11 upvotes. It's not going to get my upvote. It's not a bad

23
00:03:34,760 --> 00:03:40,600
question. In fact, a lot of the questions that I I guess complain about are

24
00:03:40,600 --> 00:03:46,000
good questions. It's just I'm hoping generally that they're good questions

25
00:03:46,000 --> 00:03:49,920
that have been answered by my booklet. And this question is a little bit

26
00:03:49,920 --> 00:03:56,480
different. I didn't remove it because it got to be addressed. But the question

27
00:03:56,480 --> 00:04:05,480
is whether one, whether this person's situation can be solved through meditation.

28
00:04:05,480 --> 00:04:13,600
And I can't answer that question. That part of the question is is not a good

29
00:04:13,600 --> 00:04:23,280
question. I mean, it's I think it's a good question. It's just not one that I

30
00:04:23,280 --> 00:04:27,400
or anyone really besides the Buddha. It's my failing that I can't answer that.

31
00:04:27,400 --> 00:04:33,560
I have no idea. I have no clue because I don't have that sort of level of

32
00:04:33,560 --> 00:04:39,400
wisdom that requires something pretty profound to be able to see someone

33
00:04:39,400 --> 00:04:44,920
else's path and assess whether they are going to benefit from something. But to

34
00:04:44,920 --> 00:04:49,440
be fair, I don't think there are many are aunts who could answer that question

35
00:04:49,440 --> 00:04:56,480
either. It's a very good question. It's just perhaps it's a not fair because

36
00:04:56,480 --> 00:05:06,560
or it's beyond my kin anyway, beyond my ability to respond. But but related to

37
00:05:06,560 --> 00:05:13,320
that is whether a version can be helped by meditation. And I mean, that's

38
00:05:13,320 --> 00:05:18,080
where I would have hoped that the meditation booklet made it fairly clear,

39
00:05:18,080 --> 00:05:22,960
made a fairly clear claim that meditation helps with things like a version

40
00:05:22,960 --> 00:05:34,800
or just this problem. And then there's other questions here. So whether you

41
00:05:34,800 --> 00:05:42,200
should try and be mindful or whether you should quit your job, I guess again,

42
00:05:42,200 --> 00:05:51,440
I'm going to say that I'm not really it's not really my not really my jurisdiction

43
00:05:51,440 --> 00:05:58,160
to answer that question. It's up to you. Your life is your life. I can lay down

44
00:05:58,160 --> 00:06:03,800
the facts that if you find a way to meditate more by leaving your job if that

45
00:06:03,800 --> 00:06:12,800
helps, you'll get better results. But is it possible to meditate and work? It's possible.

46
00:06:12,800 --> 00:06:16,480
Is it going to work for you? I don't know. It depends whether you can make it work,

47
00:06:16,480 --> 00:06:25,360
whether the meditation is potent. It works. I'll make that strong claim. But whether

48
00:06:25,360 --> 00:06:29,520
it's going to work for you, I don't know. That's completely up to you and

49
00:06:29,520 --> 00:06:35,520
whether you actually put it into practice. Could you make it work? I mean, is it

50
00:06:35,520 --> 00:06:40,280
possible to make it work? Absolutely. It's possible to make it work. Is it easier if

51
00:06:40,280 --> 00:06:47,680
you quit your job? Well, it depends. It depends on whether that allows you to meditate

52
00:06:47,680 --> 00:06:50,960
more and to put more focus on your meditation or whether it means you're

53
00:06:50,960 --> 00:06:58,800
struggling to survive and living under a bridge and that sort of thing.

54
00:06:58,800 --> 00:07:03,080
And I don't have any general comments about work aversion, which is, I'm supposed to push

55
00:07:03,080 --> 00:07:07,880
this. Don't have any general comments about work aversion because it's dealing with

56
00:07:07,880 --> 00:07:14,400
aversion, which is really, I'd say read the book if you haven't. And if you have the answer,

57
00:07:14,400 --> 00:07:19,480
the answer that you want is through your practice. Put it into practice and get the answers

58
00:07:19,480 --> 00:07:31,000
that way. Every morning I practice, we pass in the meditation and my body parts start

59
00:07:31,000 --> 00:07:36,560
to move automatically, like I'm dancing. Should I let my body do whatever it likes or should

60
00:07:36,560 --> 00:07:42,840
I control it? Yeah, generally, I mean, you try to note it and say yourself feeling, feeling

61
00:07:42,840 --> 00:07:48,360
or shaking, shaking even. And if it doesn't stop, you would sort of say to yourself in

62
00:07:48,360 --> 00:07:54,320
your mind, stop and make a firm determination to make it stop. But it's just practically

63
00:07:54,320 --> 00:08:01,320
because the mind and the brain are organic things and sometimes they need a good whack

64
00:08:01,320 --> 00:08:07,720
to get them back like a skipping record. You just give it a hit. So just firmly say to

65
00:08:07,720 --> 00:08:14,680
yourself, stop and see if that works. Usually that works. I mean, you'd want it to stop

66
00:08:14,680 --> 00:08:23,360
practically speaking because it's distracting and it involves certain mind states that

67
00:08:23,360 --> 00:08:34,760
get caught up in it, usually. Usually, I mean, if it's a purely physical act and then controlling

68
00:08:34,760 --> 00:08:40,200
it isn't necessary, what we would hope is that through the meditation practice, your nervous

69
00:08:40,200 --> 00:08:44,840
system would work itself out because it's connected to the brain, which is connected to

70
00:08:44,840 --> 00:08:52,760
the mind. And so we'd expect that after a while it would stop. You know, you'd heal,

71
00:08:52,760 --> 00:08:58,600
you'd be healed, but not always, not necessarily. I mean, strange situations exist. I mean,

72
00:08:58,600 --> 00:09:04,640
we have people practicing meditation, who have had strokes, people who have multiple

73
00:09:04,640 --> 00:09:08,640
muscular dystrophy, that sort of thing. I mean, it's not going to heal any of this.

74
00:09:08,640 --> 00:09:17,080
And so such people have to practice in a specific way. So if you have epileptic seizures,

75
00:09:17,080 --> 00:09:20,920
for example, then you'd have to just try and be as mindful as you could during the seizure

76
00:09:20,920 --> 00:09:32,080
if you're still conscious. So it's purely physical and there's no need to try and control

77
00:09:32,080 --> 00:09:38,960
it. Okay, this is a lot of lengthy questions, which is not bad, but it's bad for me because

78
00:09:38,960 --> 00:09:46,160
they have to try and read them. So this is a question about different types of traditions

79
00:09:46,160 --> 00:09:52,240
and confuse the bewildered by all the different lineages and traditions. So it doesn't

80
00:09:52,240 --> 00:09:56,600
matter which one you choose, basically, right? Or is it not important? How do you find the

81
00:09:56,600 --> 00:10:05,080
one for you? So let's step back and look at the situation. I would say we've got people

82
00:10:05,080 --> 00:10:14,640
with various levels of understanding, right? So some paths are going to be inferior to others.

83
00:10:14,640 --> 00:10:22,160
I think generally speaking or broadly speaking, I think that is true. But of course, it's

84
00:10:22,160 --> 00:10:26,920
also true that they're going to be different and that there are potentially different

85
00:10:26,920 --> 00:10:32,560
traditions or different, let's not say traditions, different methods that are not inferior

86
00:10:32,560 --> 00:10:37,280
to each other. What I just don't want to see is where we say, oh, well, they're all leading

87
00:10:37,280 --> 00:10:41,240
to the same destination. So it doesn't matter which one you pick. No, it could very well

88
00:10:41,240 --> 00:10:47,520
be that one is generally speaking and generally speaking is because it's always specific

89
00:10:47,520 --> 00:10:52,000
to the individual. One, some individual could practice a very useless technique or something

90
00:10:52,000 --> 00:10:55,880
that's useless to most people. And by practicing it, that person becomes enlightened

91
00:10:55,880 --> 00:11:01,680
much quicker than any other way, simply because of how complicated the whole process is.

92
00:11:01,680 --> 00:11:08,200
But generally speaking, you know, if you were to somehow be able to test people for enlightenment

93
00:11:08,200 --> 00:11:13,880
and you could do a sort of statistical analysis, you could see that while this path is

94
00:11:13,880 --> 00:11:19,480
much more efficient, of course, that's a difficult, near impossible because how do you

95
00:11:19,480 --> 00:11:23,560
tell who got enlightened, right? How do you tell whether someone got results and how do

96
00:11:23,560 --> 00:11:31,400
you measure results? Very difficult. I mean, there are ways to observe, but they're imperfect

97
00:11:31,400 --> 00:11:36,240
and they're highly subjective because everyone says their practice is the best, right?

98
00:11:36,240 --> 00:11:43,080
And it can't be true. I would say, I guess you'd probably want to argue that most really

99
00:11:43,080 --> 00:11:51,720
popular paths are similarly efficacious or perhaps a little bit inferior or superior,

100
00:11:51,720 --> 00:11:57,360
but not with any statistical significance and you could really not. It's not easy to tell

101
00:11:57,360 --> 00:12:05,040
because it still very much depends on the individuals who practice, right? So to that extent,

102
00:12:05,040 --> 00:12:12,200
it doesn't matter so much. Like if you take the two big Vipassana groups that call themselves

103
00:12:12,200 --> 00:12:17,560
Vipassana, you've got the Mahasi group and the going group. And I think you'd have to

104
00:12:17,560 --> 00:12:25,240
say that they're fairly equally efficacious. I mean, I'm suspicious about the going group

105
00:12:25,240 --> 00:12:29,880
personally, but that's because I belong to the Mahasi group. I really don't know enough

106
00:12:29,880 --> 00:12:39,040
about the going path to be an expert and say, I have an expert opinion on it, but I tend

107
00:12:39,040 --> 00:12:48,560
to favor my own path. But I don't think my opinion is particularly valid except to my followers

108
00:12:48,560 --> 00:12:53,120
who really appreciate it. But from an objective point of view, you'd have to say, well,

109
00:12:53,120 --> 00:13:00,520
no, he's biased because that's his technique and it's someone he knows. But if you're at

110
00:13:00,520 --> 00:13:05,960
went since you're asking me, I do have to say, I would recommend our tradition, right? How do

111
00:13:05,960 --> 00:13:10,760
you know which one's best for you? Well, is it Mahasi tradition? Then it's the best for you

112
00:13:10,760 --> 00:13:17,000
just because that's what I think. But being a little more objective and even having said that,

113
00:13:17,000 --> 00:13:21,560
I would say there's nothing really wrong with the going good tradition. They're not teaching bad

114
00:13:21,560 --> 00:13:28,440
things. Except when they say that the Mahasi tradition, the noting aspect, the use of a mantra,

115
00:13:29,480 --> 00:13:34,520
gets in the way of your concentration or whatever it is that they say, I don't know. They tend to

116
00:13:34,520 --> 00:13:39,640
say bad things about it, which, well, I would say right back, it's better. It's better to use a

117
00:13:39,640 --> 00:13:45,960
mantra. It's more powerful, more efficacious. These kind of arguments, they don't do walking

118
00:13:45,960 --> 00:13:52,360
meditation. You didn't ask specifically about a tradition, but this is an example. I would say,

119
00:13:52,360 --> 00:13:56,040
well, ours is better because it uses walking meditation. So these are things you want to look out

120
00:13:56,040 --> 00:14:02,120
for. Well, one does walking, one doesn't do walking. It's an interesting aspect of the different

121
00:14:02,120 --> 00:14:13,880
traditions and so on. I encourage you to try the Mahasi tradition and see how powerful and useful

122
00:14:13,880 --> 00:14:23,480
it is, but it doesn't matter. Within those two traditions, it doesn't matter so much, but you could

123
00:14:23,480 --> 00:14:29,880
also find a tradition that teaches all sorts of weird things. One of my professors many years

124
00:14:29,880 --> 00:14:35,800
ago at McMaster said at the university, he said a friend of his, he was Buddhist and he said a

125
00:14:35,800 --> 00:14:43,320
friend of his, had a meditation teacher who got all his students together to practice a

126
00:14:43,320 --> 00:14:53,080
meditation to destroy their competition, like another meditation group. So there's that sort of

127
00:14:53,080 --> 00:14:59,720
tradition and hopefully you're not into that, but it exists apparently. No, not all traditions

128
00:14:59,720 --> 00:15:04,200
are alike and yes, it certainly doesn't matter because the word meditation could mean it

129
00:15:04,200 --> 00:15:09,880
pretty much anything to do with the mind. Again, I didn't push that button.

130
00:15:15,960 --> 00:15:20,920
Okay, this one is a good technical question and it's a little bit roundabout, but

131
00:15:22,920 --> 00:15:27,480
when should you note thinking, thinking, or see, when should you note the senses,

132
00:15:27,480 --> 00:15:34,120
right, the objective experience, and when should you note the subjective experience or the

133
00:15:34,120 --> 00:15:39,320
reaction, right, so you see something and then you like it or dislike it, when should you note

134
00:15:39,320 --> 00:15:46,120
which? There's no cut and dried response or answer. It's not like now you should do this

135
00:15:46,120 --> 00:15:51,880
and now you should do that. Really, the answer is generally whatever's clearest. It doesn't matter

136
00:15:51,880 --> 00:15:57,160
so much. It's not magic whereby if you get the right sequence of noting somehow you become

137
00:15:57,160 --> 00:16:02,520
enlightened. It's about learning about yourself and all these aspects or aspects of yourself.

138
00:16:02,520 --> 00:16:11,960
Now, after a while, hopefully your reactions become less. I mean, if you come and do an intensive

139
00:16:11,960 --> 00:16:16,440
course, you have very strong reactions, but by the end of the course your reactions are pretty much

140
00:16:16,440 --> 00:16:22,200
gone and you're very, very objective. So it's going to change over time. The hindrance is the

141
00:16:22,200 --> 00:16:27,480
reactions of liking and disliking are going to be reduced because you're going to see how they're

142
00:16:27,480 --> 00:16:33,720
stressful, how they cause you stress, how they're useless, unhelpful, unbeneficial, harmful, even.

143
00:16:35,720 --> 00:16:40,840
But I wouldn't worry about such questions. I just try to be mindful of whatever you can

144
00:16:40,840 --> 00:16:45,800
because it's not easy to be mindful continuously. When you have these doubts, what you should do

145
00:16:45,800 --> 00:16:51,160
is be mindful of them as well, doubting, doubting. And there's force at the baton for a reason,

146
00:16:51,160 --> 00:16:56,520
they're all valid. It's not one now, another one later. It's whatever one is clearest at that

147
00:16:56,520 --> 00:17:06,440
moment and it doesn't really, there's no magical formula. I was very hurt by the unfair words of

148
00:17:06,440 --> 00:17:14,760
a person I hold in the highest steam. Is it wrong speech to speak about bad things basically

149
00:17:14,760 --> 00:17:22,920
that other people do right? So someone, I'm sorry, I'm not 100% clear, but it's fairly clear

150
00:17:22,920 --> 00:17:29,400
what you're asking is that when someone does something really bad, should you talk about it? It

151
00:17:29,400 --> 00:17:33,880
is talking about it wrong because it could hurt that person's reputation. I mean, it's a really

152
00:17:33,880 --> 00:17:47,080
good reason and so on. So I think the answer has to be that speech is never wrong. There's no

153
00:17:47,080 --> 00:17:53,160
such thing as wrong speech in an ultimate sense. This is why we're studying the Abidamma now

154
00:17:53,160 --> 00:17:58,840
to understand that when we say wrong speech, we're only really concerned with the intention behind

155
00:17:58,840 --> 00:18:05,560
it. If I say so-and-so is a real jerk, I just said that. So-and-so is a really terrible person.

156
00:18:07,080 --> 00:18:15,000
Those are just words. There's nothing wrong with them. I just said them, right? But if I said that

157
00:18:15,000 --> 00:18:26,920
about, if I said so-and-so kills babies and eats them, right? If that person doesn't actually kill

158
00:18:26,920 --> 00:18:33,320
babies and eats them, then that's a lie. So that's a sort of a wrong speech because my intention,

159
00:18:33,320 --> 00:18:37,800
not because of actually what I said, but because my intention was to mislead the audience.

160
00:18:38,680 --> 00:18:44,440
If I said it about the person, suppose they didn't ever do this, and I said it about them

161
00:18:44,440 --> 00:18:51,320
with a malicious intent hoping that my audience would really feel horrified about that person.

162
00:18:51,320 --> 00:18:58,440
And, you know, say feel, you know, just be totally turned off and hate them basically, right?

163
00:18:58,440 --> 00:19:04,920
React violently and shun them and think of them less, think less of them. Then that wish and that

164
00:19:04,920 --> 00:19:11,320
desire is horrific, right? It's a terrible desire that I have. It's really incredibly corrupt.

165
00:19:11,960 --> 00:19:16,200
But it's that intention that's behind it. The words themselves are meaningless, right?

166
00:19:16,200 --> 00:19:21,880
Then you may say, well, I've just used bad speech because I really maybe grossed some people

167
00:19:21,880 --> 00:19:27,720
out when I said talked about someone killing babies. And there's something to that. You could

168
00:19:27,720 --> 00:19:34,600
argue that I'm being insensitive, right? Because I know that my audience is going to react violently,

169
00:19:34,600 --> 00:19:39,000
even though I can say, well, it's their fault. They're not being mindful. But that's not fair

170
00:19:39,000 --> 00:19:44,920
because I know that my audience is going to react violently. So perhaps that was wrong speech

171
00:19:44,920 --> 00:19:51,000
in the sense that I wasn't sensitive, right? There is a sense of certain sensitivity

172
00:19:51,720 --> 00:19:56,440
where you don't want to just say whatever. You know, we talk about being objective and so it

173
00:19:56,440 --> 00:20:02,920
seems like Buddhists should just be okay with everything. But there is a sense of being sensitive to

174
00:20:02,920 --> 00:20:10,440
people's feelings as well. But all of this has to do with one state of mind, right? One's clarity

175
00:20:10,440 --> 00:20:15,080
of mind. If one is, I mean, the Buddha talked about eating babies, in fact, killing babies and

176
00:20:15,080 --> 00:20:22,680
eating them in certain circumstances for the very purpose of shock value to remind his followers

177
00:20:23,480 --> 00:20:33,400
about how about not to remind them, but to give them a sense of how they should,

178
00:20:33,400 --> 00:20:37,960
how our steer they should be basically, how unattached they should be to food.

179
00:20:37,960 --> 00:20:42,040
It's a long story. I've told it before. Some of you have heard it, but

180
00:20:44,760 --> 00:20:49,080
the point being that speech is never wrong. It's the intention behind it. So if you tell someone

181
00:20:49,080 --> 00:20:53,960
about someone else's bad behavior, it really depends what your intentions are. Why are you saying that?

182
00:20:54,840 --> 00:20:59,880
Is it because you want this person to get angry at that person? Then that's a bad intention.

183
00:20:59,880 --> 00:21:08,920
That's called divisive speech. It's the intention to create disharmony.

184
00:21:10,680 --> 00:21:16,360
If it's for the purpose that that person might get help, you want some help to help the

185
00:21:16,360 --> 00:21:22,520
person who's done the bad thing, then that's potentially a wholesome intention. The intention to help

186
00:21:22,520 --> 00:21:28,520
is always wholesome, right? Of course, it could be based on delusion. Maybe the person doesn't need

187
00:21:28,520 --> 00:21:33,880
help. Maybe the person didn't do anything wrong. In your case, it's most likely that they did,

188
00:21:33,880 --> 00:21:38,040
from the sounds of it, it was pretty extreme, but there are cases where we can be wrong.

189
00:21:38,680 --> 00:21:43,400
So then our intentions are based on delusion, which is also bad. There's lots of badness.

190
00:21:44,280 --> 00:21:49,880
It sounds like you gave fries to some unwholesome, this bike being sad and being revolted.

191
00:21:50,680 --> 00:21:54,360
Those are negative emotions, so you shouldn't certainly act out on them.

192
00:21:54,360 --> 00:22:01,160
The best thing to do in that case is to be mindful of them. You don't need to fix things.

193
00:22:03,000 --> 00:22:08,840
If suppose this person is doing bad things and it's affecting other people, this person is

194
00:22:08,840 --> 00:22:15,320
embezzling money or suppose it's, let's say, just one example of the top of my head,

195
00:22:15,320 --> 00:22:21,080
this person is an investor. What do you call one of these people who invest money with?

196
00:22:21,080 --> 00:22:27,000
They're cheating people, right? Then one of your friends is going to go invest money with them

197
00:22:27,000 --> 00:22:33,560
and you say, well, it'd be better if you don't invest money with them because they cheat.

198
00:22:35,080 --> 00:22:39,480
Then you can say that's a good thing because you're trying to help the person.

199
00:22:40,440 --> 00:22:43,240
But it very much depends on what are your intentions at that time.

200
00:22:44,120 --> 00:22:46,840
If you get worried about the other person investing their money,

201
00:22:46,840 --> 00:22:53,720
maybe it's a bad thing, but if your intention is to make sure that bad deeds aren't done,

202
00:22:54,520 --> 00:22:59,320
then that's good, right? So if this person has the potential to do awful things,

203
00:23:00,360 --> 00:23:06,520
to try and to some extent prevent that is always going to be, to some extent it's going to be wholesome

204
00:23:06,520 --> 00:23:11,960
because your intention is for the prevention of evil.

205
00:23:11,960 --> 00:23:20,840
But yeah, I mean, very much focus on your reaction. You don't have to fix the world.

206
00:23:20,840 --> 00:23:24,920
You don't have to fix anything but your own mind. The world's never going to be fixed.

207
00:23:24,920 --> 00:23:29,080
There's always going to be more and more people with more and more defilements.

208
00:23:34,040 --> 00:23:37,640
You don't have to keep silent though. It's the words aren't wrong. It's not going to be wrong

209
00:23:37,640 --> 00:23:42,440
speech just to say something. It's all going to be about your intention and often it's messy, right?

210
00:23:44,600 --> 00:23:48,120
You may not get it right. You may say something wrong and then you'll learn from it.

211
00:23:50,360 --> 00:23:56,440
It's always easiest not to say anything. So generally speaking, it's a good rule of thumb

212
00:23:56,440 --> 00:24:00,200
is that if you don't know, if you're not confident that it's right to speak,

213
00:24:00,200 --> 00:24:07,640
staying silent is better for your mental health.

214
00:24:11,480 --> 00:24:15,080
When it's not possible, so there's different interpretations of the Buddha's words.

215
00:24:15,080 --> 00:24:19,320
When it's not possible to get things cleared up by simply asking an Arhan, well yeah sure,

216
00:24:19,320 --> 00:24:25,160
but who is an Arhan? And who's going to say who's an Arhan? Because lots of different people

217
00:24:25,160 --> 00:24:30,280
seem to think that their teachers are Arhan, or even that they themselves are Arhan.

218
00:24:30,280 --> 00:24:36,680
So when perhaps they're not, is the first answer. The second answer said even an Arhan

219
00:24:36,680 --> 00:24:40,760
can't clear up the Buddha's words because Arhan's don't know all the things that the Buddha knew.

220
00:24:41,560 --> 00:24:46,680
So even an Arhan could get something wrong or just doesn't know the answer. Is this right?

221
00:24:46,680 --> 00:24:58,520
Is this what the Buddha taught? Is it not? An Arhan to me not even know. Nice try though. Wouldn't

222
00:24:58,520 --> 00:25:05,480
be great. The only way we'd be to have a Buddha. The appreciation reverence come under mid.

223
00:25:05,480 --> 00:25:16,600
So by appreciation you're not, you're talking about towards a monk, right?

224
00:25:19,960 --> 00:25:22,760
So reverence, where does reverence come under?

225
00:25:22,760 --> 00:25:37,880
Reverence, I think comes under confidence, sadha, right? I mean it's sadha we use

226
00:25:38,520 --> 00:25:43,960
in Buddhist, in general Buddhism to talk about someone who has faith in someone else,

227
00:25:43,960 --> 00:25:51,160
but even from an Abhidhamma point of view it's confidence. You know it's a sense of strength in your

228
00:25:51,160 --> 00:25:57,000
mind. When you think about the Buddha your mind becomes strong. If you have great faith in the

229
00:25:57,000 --> 00:26:01,400
Buddha your mind becomes strong. It's the same with God. If you have great faith in Jesus Christ

230
00:26:01,400 --> 00:26:07,080
or Allah or Vishnu or whatever Krishna your mind is going to become strong when you think of them.

231
00:26:08,280 --> 00:26:12,360
It's sadha, this sort of strengthening faculty.

232
00:26:12,360 --> 00:26:21,080
I'm meditating and practicing for eight to nine months.

233
00:26:21,080 --> 00:26:27,400
Thank you. You're still with us. I'm very glad. How do you deal with people if they try to force you?

234
00:26:27,400 --> 00:26:29,960
So the question is people are say you're acting like a zombie?

235
00:26:33,960 --> 00:26:41,480
Well, the world is a dream. The universe is like a dream. Being a human is like a dream.

236
00:26:41,480 --> 00:26:47,240
It's all artificial. There was a monk who told the Buddha he was going back to live with

237
00:26:48,040 --> 00:26:51,560
some very, very vicious people. The Buddha said wow they're very vicious. What are you going to do

238
00:26:51,560 --> 00:26:56,920
if they yell at you? He said well I'll just think to myself these people are very, very kind,

239
00:26:56,920 --> 00:27:02,760
very, very, very patient and that they don't hit me. Then Buddha said well what if they hit you?

240
00:27:03,720 --> 00:27:08,440
They said well then I'll think to myself these people are very, very nice and that they don't

241
00:27:08,440 --> 00:27:13,720
break my bones. And the Buddha said well what if they break your bones? He said well they're not

242
00:27:13,720 --> 00:27:19,720
think to myself these people are very, very nice and that they don't kill me. And then the Buddha

243
00:27:19,720 --> 00:27:24,600
says what if they kill you? And he said well then I'll think to myself many enlightened beings

244
00:27:24,600 --> 00:27:32,760
have wished for someone. I'm not wished exactly. I've just waited for death and now I have found

245
00:27:32,760 --> 00:27:38,440
death and so I will be free from suffering as a result of the death so it won't bother me.

246
00:27:41,240 --> 00:27:50,280
How that relates to your situation is eventually you have to just put up with abuse. You have to

247
00:27:50,280 --> 00:27:55,800
put up with lots of things. People might hit you. People might throw you in jail. I was put in jail

248
00:27:55,800 --> 00:28:01,480
as a monk and I don't think I was doing anything wrong. I wasn't. Eventually I was exonerated

249
00:28:01,480 --> 00:28:08,840
completely but I've still thrown in jail. I told my teacher and he said oh that's what it's like

250
00:28:08,840 --> 00:28:13,480
in non-Buddhist countries basically as what he said. That's their wealth. That's their way of welcoming

251
00:28:13,480 --> 00:28:22,840
you. He was not free. He was not very sympathetic. He was like what do you expect when you go to

252
00:28:22,840 --> 00:28:36,280
countries like that? Maybe I shouldn't say such things on the internet but the world is a jungle.

253
00:28:36,280 --> 00:28:42,120
It's a jungle out there and if it's even more so when you start to be mindful because most people

254
00:28:42,120 --> 00:28:48,840
aren't mindful and I think that's important to say is that most people not being mindful

255
00:28:48,840 --> 00:28:56,680
makes it very difficult to live in the world. It's not we think of ordinary people as being

256
00:28:56,680 --> 00:29:01,400
normal right? They're just normal people. They don't do anything wrong but when you become more

257
00:29:01,400 --> 00:29:05,720
sensitive as you practice meditation and it's much harder to live amongst than because

258
00:29:07,400 --> 00:29:17,000
you're moving because you're more sensitive. You realize how it affects you and how wrong it is

259
00:29:17,000 --> 00:29:21,240
that things people do before you can just go along with it. Hey we're going to go at drinking okay

260
00:29:21,240 --> 00:29:28,280
I'll come with you or hey we're going to go dancing let's go dancing. You can't just go along

261
00:29:28,280 --> 00:29:35,720
with that anymore because it's just futile because you don't have the delusion that leads people

262
00:29:35,720 --> 00:29:46,840
to dance for example and you get in trouble for these so hopefully that helps a little bit. So

263
00:29:46,840 --> 00:29:52,360
Sahibu just says that Sunyupadana Kanda is dukka. I thought that perception there was not yet

264
00:29:52,360 --> 00:29:57,880
clinging right. Okay let's be clear this is an important question.

265
00:29:57,880 --> 00:30:07,240
So what you're thinking is that suffering comes from reacting to things right. If I get

266
00:30:07,240 --> 00:30:13,400
if I like something or want something it's going to lead me to suffering. So how could Sunyah

267
00:30:13,400 --> 00:30:21,720
therefore be a problem? Sunyah is not a problem. Sunyah is a problem in another way. It's a

268
00:30:21,720 --> 00:30:27,400
problem in the way that everything in the world is a problem. Everything in the world is a problem

269
00:30:27,400 --> 00:30:34,280
if you cling to it. That's one way of explaining it right. There's another way but the first way

270
00:30:34,280 --> 00:30:39,880
of explaining it is that if you cling to Sunyah it's going to cause you problems right. The cause

271
00:30:39,880 --> 00:30:50,200
of suffering is craving is desire is some kind of reaction to it an attachment to it or an attachment

272
00:30:50,200 --> 00:30:59,480
to something about it. So those things that we cling to are not worth clinging to. They're not

273
00:30:59,480 --> 00:31:06,760
satisfying. That's how we get this word. They're not able to make you happy and the implication is

274
00:31:06,760 --> 00:31:11,480
that they're not able to make you happy by clinging to them. That's why we call them dukka. That's

275
00:31:11,480 --> 00:31:18,040
one way of understanding it. When you call something suffering what you mean to say is that that

276
00:31:18,040 --> 00:31:26,680
thing is not something that you should dive into and try and turn into a cause for happiness

277
00:31:26,680 --> 00:31:32,360
because that thing is unable to make you happy. That's one way of it. So that's the understanding.

278
00:31:32,360 --> 00:31:41,880
But the second way of understanding it is that all things that arise are suffering, meaning they

279
00:31:41,880 --> 00:31:52,600
have nothing good about them. It's almost the same thing actually. But what I'm thinking of

280
00:31:52,600 --> 00:32:00,120
is because the text often talk about the rise and fall as being a bad thing. Everything is

281
00:32:00,120 --> 00:32:11,000
momentary and that momentaryness just makes it worthless. And worthless does mean not worth clinging

282
00:32:11,000 --> 00:32:18,040
to. But there is sort of an intrinsic dukka-ness, like a sense of being dukka in the sense of being

283
00:32:18,040 --> 00:32:24,920
like garbage, specifically because of the rise and fall. And maybe it's just a more refined way of

284
00:32:24,920 --> 00:32:31,240
saying not worth clinging to. The better explanation is this thing arises and ceases. So of course

285
00:32:31,240 --> 00:32:38,520
it's not worth clinging to because it's not going to last. It's nothing. It's a moment. And so

286
00:32:38,520 --> 00:32:47,080
sunya being a moment is nothing. It's useless, pointless. It's not a thing. It's useless, pointless,

287
00:32:47,080 --> 00:32:58,040
or it's a moment, a moment of experience that's meaningless. There's dukka. It's considered to be

288
00:32:58,040 --> 00:33:08,760
stressful. It's not something that you can rely upon. It's not something that you can rest

289
00:33:08,760 --> 00:33:20,920
upon. It's not something that you can hold on to. So it's bad. The idea is to compare it to

290
00:33:20,920 --> 00:33:30,200
nibana, which is something you can rest upon, is something you can rely upon. Is something that is

291
00:33:31,400 --> 00:33:41,800
stable. The instability, right? Like if you have a chair and it's unstable, such that when you sit

292
00:33:41,800 --> 00:33:47,960
on it, you might fall over. They might break, or you might tip over. Then that's not a good chair,

293
00:33:47,960 --> 00:33:56,760
that's a bad chair. It's dukka in that sense. Everything that arises and ceases is dukka in that sense.

294
00:33:56,760 --> 00:34:03,880
Nanyatrung dukka sambodhi nanyatrung dukka nyurunjati. Nothing but suffering arises. Nothing but suffering

295
00:34:03,880 --> 00:34:22,600
passes away. Is impermanence depend on intensity of craving? I don't understand. Does this mean

296
00:34:23,800 --> 00:34:29,400
someone has more craving, things get more impermanent? I would say that's a fair way of putting it.

297
00:34:29,400 --> 00:34:34,760
If someone has less craving, things are going to be more stable, more predictable, more

298
00:34:35,800 --> 00:34:41,880
satisfying, ironically. Whereas if someone has more craving, things become more chaotic,

299
00:34:42,600 --> 00:34:51,560
more stressful, more complicated. So if that's what you're asking, then the answer is yes.

300
00:34:51,560 --> 00:35:05,080
And that's all the questions tonight. I guess that's all for tonight then.

301
00:35:05,080 --> 00:35:34,920
Thank you all for coming out. Have a good night.

