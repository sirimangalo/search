Okay, good evening everyone, welcome to our evening talk.
We have a new meditator today from Winnipeg who is actually an old meditator.
He's finally found the time to coordinate and do the full foundation course in our tradition.
At the same time we're losing a meditator, a graduate who has just finished the foundation course
passed with flying colors, graduated and moving on, but he'll be back.
The dhamma is addictive, well not really is it.
That's the problem with the dhamma actually is that it's not addictive.
It's the opposite of clinging and so you don't create an addiction towards it.
But you do create an attraction, it's kind of a paradox in a sense.
You do somehow become addicted to it, addicted through wisdom.
It appears though you're an addict, person can't stop meditating, you must be addicted to it.
Which is just a fool's way of showing their misunderstanding.
The inability to understand the lack of alarm to superficial, just a superficial comment.
Wisdom is a lot like addiction in that way.
When you know something's right, you can't avoid it.
When you know something's wrong, you can't take part in it.
So congratulations for that.
I propose tonight's talk, as tonight's talk I was going to talk.
I thought the topic would be desire, but there's a little bit more to it.
I'm going to talk about the general concept of the path.
Meaning the path of desire versus the path of renunciation.
Is there a way of understanding our options?
Because this is a true dichotomy.
You can't, there's no in between.
It's one or the other.
Mahasi Sayedas says they're diametrically opposed.
I don't know what the actual Burmese words he used are probably not exact translation, but
this is their opposites.
You can't do both.
Or rather, they lead an opposite direction, so most of us find ourselves in the middle being
pulled in both direction.
Sometimes engaging in enjoying nice sensations.
You might have pleasant sights or sounds or smells or tastes or feelings.
You might engage in them repeatedly.
But they're not complementary, it's the point.
So the context is we had an interesting class last night on the theory of the study of religion.
That was on the question of whether phenomena like sports, sports, loyalty, team loyalty,
or fandom, like the Trekkies or the Jedi Knights.
Our guest speaker was a put down Jedi Knight on the census as his religion.
He explained why that wasn't actually the word, I think.
Not obvious reasons for doing it, but the discussion was whether certain phenomena that appeared
to be quite religious or at least involving a great amount of sincerity and dedication,
whether they could be called religious.
As I won't go into great details, an interesting conversation, a couple of points that came up were...
Well, the main point was that they don't touch upon anything meaningful.
Could you argue that something has to touch upon what is ultimately meaningful for it to be a religion?
And so I mean, I was the one who posed this and I suggested that you could argue that something is only a religion
and this is what the courts often argue.
There's been lots of challenges to the tax exempt status of religions,
and that it should include things like smoking marijuana or it should include vegan movements,
veganism or raw food movements.
It should include the Church of the Flying Spaghetti Monster, for example.
But they said that these various movements don't have cohesive doctrine that is sincerely held regarding ultimate questions of ultimate concern.
The meaning of life, etc.
Which in the end I think is an unfair assessment, but that's not the point.
The point in the end was that loyalty to a sports team may seem quite religious.
And so I said, well, I can understand, I can accept that it might be a religion, but it's a pretty silly religion.
It's the point.
And this is the interesting point of it all.
Not what is a religion and what is not a religion, but the fact that we can rank religions.
We're not allowed to, and this is sort of the real problem with the word religion is that
call something a religion and suddenly you have to respect it.
Suddenly it's equal to all other religions and so the concern with allowing something to be called
a religion is that well, then you have to respect it and who wants to think of
fan worship of Captain Kirk as the same as the worship of Jesus Christ or Buddha or so on.
I want to be able to distinguish, I don't think I'm setting up a straw man.
I think many people want to distinguish because they consider religion to be something in a category of its own.
Two e-gennarists, that's Latin.
It's a fancy Latin phrase that means of its own type, something in its own, in a category of its own.
And so where this is all going is the recognition that there are many paths.
But denying that all paths lead to the same goal, that all paths are equally valid, that all paths should be considered equally,
be given the same sort of attention.
And so that led today during my French lesson, I was talking to my French tutor and I gave him a lesson in Buddhism at the end, trying to explain it in French and then switching to English.
And his question was why shouldn't you engage in enjoyable things if they make you happy?
And he was trying to struggle with it and to argue, but be careful because he didn't want to threaten my religion.
He obviously didn't agree with me, but he didn't want to get into her that just debate.
So he phrased how things must be or that you can't do things, because all things you do must be, must advance you as a person.
I said, well, no, actually, it's not like that.
It's a question of what you want out of life and where you're heading.
I think this is what is unfamiliar to a lot of ordinary people is that there exists paths, there exist methods,
that lead beyond this ordinary state of existence that most people are intellectually comfortable with.
Most people are intellectually comfortable with suffering.
They are physically or practically comfortable with it.
They moan and complain like everybody else, but if you ask them, hey, wouldn't you like to change?
The way the world works?
I mean, really, if you ask them, would you like to practice meditation?
I've got this practice where you could become completely free from all the sufferings of life.
Most people, I think their initial reaction is, okay, suffering is okay.
I'm okay with this.
But really, what's saying that is the desire they have and an attachment,
there are many attachments to pleasures and to ambitions and so on.
And then the ego behind it, the identification, so the idea of leaving behind some sorrow,
the idea of nibana is frightening, horrific even, abhorrent to most people, to not be born again.
I mean, actually, many people are struggling under the delusion that we only have one life.
And to deny this, the pleasures of this life is one life.
How horrible would that be? What a horrific thought.
So it's important to clarify, we're not talking about giving up happiness in order to give up suffering.
We're not talking about an even trade here.
These aren't on an equal footing.
When we talk about the ordinary path of desire and diversion,
the ordinary path of desire, aversion, anger, delusion, that most people are on.
We have all these things and engage in them regularly.
This is a very coarse path.
This is a path that is fraught with problems and is working on a level of constant delusion,
of constant confusion and darkness, lack of clarity.
The mind for such an individual, for most individuals, is a sleep, diffused, distracted,
muddied, muddled, all these things.
Of course, constantly agitated, constantly excited,
and fraught with worry and fear and anxiety and stress.
Even when seeking out the objects of pleasure or even when attaining them,
there is a distraction, there is lack of clarity in the mind.
And so really all we're talking about in Buddhism is not giving anything up,
per se, not directly.
It's about a path that is on a higher level and higher in a very pejorative sense,
or whatever the opposite or pejorative is.
It really is a pejorative lower.
That's even a phrase. It's a really, truly lower path to seek out central pleasure.
He no gamo, pouto, pouto, genico, analeo, another sanchito,
but called it inferior.
Gamo, the way of the village.
All we're doing in meditation practice is creating a higher state of consciousness,
a greater sense of clarity.
And through the increase of clarity, things change.
We refine our pursuits.
We discard the pursuits that are clearly causing us stress and suffering and problem.
Clearly now that our minds are more clear.
It really is that simple.
It's not a matter of deciding, should I give up happiness in order to be free from suffering?
It's not at all like that.
In fact, greater happiness comes.
Our refined, pure, exalted, blissful, wonderful sense of happiness comes.
Of course, my meditators are all like, yeah, right.
I'm here torturing people in Canada, and then I talk about bliss and happiness.
Well, our graduates know.
Our graduates can tell you.
It's a great struggle to rise above the, rise above the dirt, the merc,
to break free from the chains, the chains of desire.
But it's a struggle unequivocally worthwhile.
And this isn't just a claim that I make him, and this is really how we should understand
the practice and how we should be clearly evident from the practice that we do.
We're not judging.
We're not even trying to change ourselves.
Beyond the simple change of seeing our experiences, even our own minds, and our own habits, clearly.
The wheat puss in the, seeing clearly.
It's not some hard to understand concept.
Things arise and cease.
The ordinary state of mind is to, to fair blindly grasping and clinging to anything,
and cultivating habits with no rhyme or reason.
It's like turning on the light.
When you shine the light in the darkness disappears.
When you're able to see it quite easily.
And suddenly, quite clear, come all the things that were previously mysterious.
Why do I suffer?
Why am I depressed?
How do I free myself from desire and anger and aversion?
I'll become clear.
So there you go.
I mean, that's about all I had to say tonight, just some food for thought.
And I guess including the encouragement, not to be, not to lose, lose sight of this,
or to lose faith, or to be caught up in the pursuit of desire,
the pursuit of ordinary happiness,
to engage in, and pursue, clarity of mind,
ability to see things as they are more clearly than before.
It allows us to refine our happiness and to create greater states of peace
and goodness in our hearts and to free ourselves from all the troubles that come from
evil and wholesome mind states.
So good for you, everyone.
Keep up the good work.
All right, there you go.
That's the dhamma for tonight.
Thank you all.
If anyone has any questions, I'm happy to answer them.
Thank you.
Thank you for coming, everyone.
I guess there are no questions.
If there are no questions, we'll say goodnight then.
And then there's a question.
He started by mentioning meditation as addicting,
and so my question is when did the Buddha mention the path as being beautiful in the beginning,
in the middle, and as we most often do not focus in the beauties of life?
Yeah, the word isn't quite beautiful.
It's caliana.
Caliana means good, bright, or beautiful would be so bad.
Super.
Super would be beautiful.
But caliana, I can't remember the etymology,
but it's a specific word.
We use it for a friend, like a caliana mitta is a beautiful friend,
but there's nothing to do with their good looks.
It's just a wonderful friend, an awesome friend.
So caliana means good in the beginning,
and that's often translated in that way.
It's good in the beginning, good in the middle.
It's the bhayana, brahma chari
Thang sa bhayan jianang,
Kayvala paripun nang par RM
And it is the brahma chari fast, that is,
raw complete, in both its letter and its meaning,
meaning that all the teachings are there,
and the meaning is perfect,
Gave a parypundang totally and completely complete, parysudang and pure.
The practice isn't addictive, I was trying to say it seems addictive, because people
who engage in it, it leads you on, the more you gain, the more you are the inclination
to gain or to strive for gain, because of wisdom, not because of attachment.
Gave a parypundang.
Gave a parypundang.
I've posted a bug report for the broadcasting software that I use.
Other people are confirming the bug, and they say just wait for the next version of this
software to come out, unless I can figure out how to compile it, which is a mystery
to me how that works, but there are ways of getting the software.
Anyway, so at some point soon the point is I'll probably be doing live YouTube broadcasts again.
Which doesn't mean we can't do second life as well, because I can always log into second life and just minimize it.
The thing about Linux, it seems like I can port the audio to four different sources at once, which is kind of cool.
I don't know if that happens in Windows as well, but anyway, let's end it there then.
Thank you all for coming out.
It's great to have a crowd every night.
We'll try to be back here again tomorrow.
