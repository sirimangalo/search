1
00:00:00,000 --> 00:00:03,560
Does an enlightened being feel any pain?

2
00:00:03,560 --> 00:00:09,760
I saw this video when Tikkun duk burned himself and never moved.

3
00:00:09,760 --> 00:00:13,280
He sat still and seemed not at all bothered by this.

4
00:00:13,280 --> 00:00:17,480
Well, there's many ways to be free from pain.

5
00:00:17,480 --> 00:00:23,560
A person doesn't have to be enlightened and I don't know who this person is, but I imagine

6
00:00:23,560 --> 00:00:25,760
he didn't consider himself to be enlightened.

7
00:00:25,760 --> 00:00:31,120
I don't know, I don't imagine enlightened being with set themselves on fire.

8
00:00:31,120 --> 00:00:38,720
And I assume he didn't claim to be enlightened, I don't know who he is.

9
00:00:38,720 --> 00:00:44,120
But there are ways to overcome pain and there are ways to keep yourself from feeling pain,

10
00:00:44,120 --> 00:00:47,920
mostly having to do, mostly based on some of the practice.

11
00:00:47,920 --> 00:00:53,600
An enlightened being does feel pain, but they aren't bothered by it.

12
00:00:53,600 --> 00:01:00,000
And that being the case, they may still move out of the way when they feel pain, because

13
00:01:00,000 --> 00:01:03,080
they're acting naturally.

14
00:01:03,080 --> 00:01:06,880
Tranquility meditation, transcendental meditation, is in many ways unnatural.

15
00:01:06,880 --> 00:01:10,240
It's not our habit, it's not our way of being human.

16
00:01:10,240 --> 00:01:15,440
You're not born a human because you've practiced a lot of tranquility meditation.

17
00:01:15,440 --> 00:01:20,200
The states that monks will enter into and there's a monk who burnt his, I heard about

18
00:01:20,200 --> 00:01:24,640
he burnt, I think a small finger, he stuck his little finger into a candle flame and just

19
00:01:24,640 --> 00:01:27,280
burnt his entire finger to the ground.

20
00:01:27,280 --> 00:01:36,200
He wanted to make an offering to the Buddha and he offered his pinky finger apparently.

21
00:01:36,200 --> 00:01:40,960
And my teacher was talking about this and he said, you know, that's real strength in

22
00:01:40,960 --> 00:01:41,960
mind.

23
00:01:41,960 --> 00:01:44,920
He's able to separate the mind from the body.

24
00:01:44,920 --> 00:01:49,120
And if your mind is focused enough, pain is really not a big deal, it's not something

25
00:01:49,120 --> 00:01:54,920
that bothers you, if your mind is fixed and focused and these monks have very fixed and

26
00:01:54,920 --> 00:02:02,440
focused attention.

27
00:02:02,440 --> 00:02:05,320
So there's two different states here.

28
00:02:05,320 --> 00:02:09,920
One is where you don't even feel the pain and that's in tranquility meditation.

29
00:02:09,920 --> 00:02:14,160
And the other is where you feel the pain but you don't respond and that's totally different

30
00:02:14,160 --> 00:02:20,040
and in light and on our hand I can't imagine them burning their finger unless it happened

31
00:02:20,040 --> 00:02:24,880
to be a habit or something that they had developed over time because it's not natural

32
00:02:24,880 --> 00:02:26,640
and they don't have any reason to do it.

33
00:02:26,640 --> 00:02:31,720
The reason when they say their robes are on for any reason to put the fire out, it's

34
00:02:31,720 --> 00:02:50,960
natural, it's their habit, it's a part of their being to do that.

