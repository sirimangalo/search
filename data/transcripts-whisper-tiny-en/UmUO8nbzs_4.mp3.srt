1
00:00:00,000 --> 00:00:06,480
One day, is it useful to have prior knowledge about the 16 Vipassan and Yana when doing

2
00:00:06,480 --> 00:00:13,680
Vipassana, or is it better to discover them accidentally as you meditate by accident?

3
00:00:13,680 --> 00:00:16,600
Not exactly by accident, but I know what you mean.

4
00:00:16,600 --> 00:00:23,800
It's not just, it's the goal, the real reason for practicing, so it wouldn't be by accident,

5
00:00:23,800 --> 00:00:28,920
but it would be spontaneously.

6
00:00:28,920 --> 00:00:38,560
In fact, teachers in our tradition might teach you being one of them discouraged strongly

7
00:00:38,560 --> 00:00:43,800
the idea of learning about the 16 Vipassan and Yana before you practice, that it can actually

8
00:00:43,800 --> 00:00:51,480
be a strong hindrance in your practice because you tend to expect and anticipate and identify

9
00:00:51,480 --> 00:01:02,680
every experience with some Yana or other or you anticipate and wait for and become agitated

10
00:01:02,680 --> 00:01:14,760
and worried and paranoid and rightly so because your mind is still a mess in the beginning

11
00:01:14,760 --> 00:01:20,160
and so it latches onto anything and it makes connections when they're not there and it can

12
00:01:20,160 --> 00:01:27,160
be a real hindrance if you know too much about the Yana's, about the 16 Vipassan, 16

13
00:01:27,160 --> 00:01:33,280
stages of insight, so yeah, not something you want to delve deeply in, simply knowing

14
00:01:33,280 --> 00:01:38,880
about them isn't a big deal, but certainly it should be kept in mind that when you undertake

15
00:01:38,880 --> 00:01:43,960
a meditation course, the best thing is to put it all aside, forget about it, even for

16
00:01:43,960 --> 00:01:49,440
advanced meditators, it's best to leave yourself under the guidance of the teacher and focus

17
00:01:49,440 --> 00:01:55,840
your own efforts on the force at the Patanas, so we separate meditation into two aspects,

18
00:01:55,840 --> 00:02:00,600
the practice and the goals, the 16 Vipassan and Yana's are not the practice, they are

19
00:02:00,600 --> 00:02:08,080
the goals, they are the attainments, they come from the practice, so that's an incredible

20
00:02:08,080 --> 00:02:14,280
or an crucial distinction, you don't practice the goals, you don't practice insight, so

21
00:02:14,280 --> 00:02:19,000
we talk about practicing Vipassan and meditation, we're not actually practicing Vipassan

22
00:02:19,000 --> 00:02:27,480
we're practicing mindfulness based on the five aggregates based on ultimate reality for

23
00:02:27,480 --> 00:02:39,640
the arising of insight as the goal, as the reason for practicing, so that I think if

24
00:02:39,640 --> 00:02:46,320
you can make that distinction and set yourself on the four foundations of mindfulness and

25
00:02:46,320 --> 00:02:52,240
watching the five aggregates, then I don't think it matters what you know, when you have

26
00:02:52,240 --> 00:02:56,160
the realization, when you're aware of something, you just know that you're aware of it

27
00:02:56,160 --> 00:03:24,000
knowing, knowing, thinking, thinking, so remind yourself and let it go.

