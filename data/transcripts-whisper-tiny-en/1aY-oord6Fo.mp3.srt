1
00:00:00,000 --> 00:00:06,200
I have a question, what is the best course of meditation for someone that is very irritable

2
00:00:06,200 --> 00:00:09,000
and suffers from an anxiety disorder?

3
00:00:09,000 --> 00:00:14,000
I have trouble keeping still and doing sitting meditation. Any advice?

4
00:00:22,000 --> 00:00:29,800
You can start by doing lying meditation, a person who has a anxiety order

5
00:00:29,800 --> 00:00:46,800
will do much better in lying meditation, much better in sitting meditation than in walking meditation

6
00:00:46,800 --> 00:00:53,800
and much better in lying meditation than an ordinary person.

7
00:00:53,800 --> 00:00:58,800
For most people lying meditation put them to sleep, but for someone with anxiety disorder

8
00:00:58,800 --> 00:01:03,800
will actually serve to balance the energy.

9
00:01:03,800 --> 00:01:09,800
So if you find that do sitting meditation for as long as you can,

10
00:01:09,800 --> 00:01:21,800
and then when you are not able to sit any longer, try to acknowledge the desire

11
00:01:21,800 --> 00:01:31,800
to move, acknowledge the distractions in the mind, acknowledge the anxiety,

12
00:01:31,800 --> 00:01:38,800
the mental upset, and then when it becomes overwhelming,

13
00:01:38,800 --> 00:01:44,800
then do lying, lie down into lying meditation, then just the same rising falling,

14
00:01:44,800 --> 00:01:54,800
watching the stomach, just the same, you can lie on your back and forth.

15
00:01:54,800 --> 00:02:02,800
But really, if you are really serious about meditation, no matter what your condition is,

16
00:02:02,800 --> 00:02:06,800
the best advice one can give is to go and do a meditation course.

17
00:02:06,800 --> 00:02:16,800
Most of the conditions we have are the most important conditions

18
00:02:16,800 --> 00:02:19,800
to do away with are the ones on the surface.

19
00:02:19,800 --> 00:02:25,800
The conditions like anxiety may be with you for a long time,

20
00:02:25,800 --> 00:02:30,800
even after you do several meditation courses, all you might find is it gets weaker and weaker

21
00:02:30,800 --> 00:02:33,800
and simpler and simpler.

22
00:02:33,800 --> 00:02:37,800
But the first thing is to learn how to gain the skills,

23
00:02:37,800 --> 00:02:42,800
to learn how to deal with the anxiety and so on,

24
00:02:42,800 --> 00:02:45,800
and to straighten your mind out about the anxiety,

25
00:02:45,800 --> 00:02:51,800
about what it is, about who it is, in the sense of not being anybody's.

26
00:02:51,800 --> 00:02:56,800
And for that, you really need a meditation environment.

27
00:02:56,800 --> 00:02:58,800
You need a course.

28
00:02:58,800 --> 00:03:05,800
So it really doesn't matter what your condition is.

29
00:03:05,800 --> 00:03:21,800
Try to find time to go and undertake a meditation course with a proficient teacher.

30
00:03:21,800 --> 00:03:25,800
And the more time the better a person with anxiety,

31
00:03:25,800 --> 00:03:30,800
the best thing is to give them a long course because you don't want to give them deadlines.

32
00:03:30,800 --> 00:03:34,800
And you don't want them to be thinking about deadlines and so on.

33
00:03:34,800 --> 00:03:37,800
You want to take it slow and easy.

34
00:03:37,800 --> 00:03:41,800
But in the beginning, any relying meditation can be quite useful.

35
00:03:41,800 --> 00:04:03,800
So I wouldn't do recommend that for anxiety.

