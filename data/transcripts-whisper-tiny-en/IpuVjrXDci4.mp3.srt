1
00:00:00,000 --> 00:00:11,000
I wonder if you could talk about depression. Is it an energy? I understand anger is an energy, but depression. Do you think it is an illness, energy, or mindset?

2
00:00:11,000 --> 00:00:19,000
Hope you don't mind me asking. Anger being an energy. No, anger isn't an energy.

3
00:00:19,000 --> 00:00:39,000
Okay, Buddhist theory breaks things down. They even said that in our tradition they said that the Buddha was a, we budge a wad in someone who splits things up, or dissects things, breaks things up into its constituent parts.

4
00:00:39,000 --> 00:00:48,000
That's what best described the Buddha. It's an interesting quote, but that ended up being the orthodox interpretation of the Buddha's teaching.

5
00:00:48,000 --> 00:00:54,000
The Buddha broke things up instead of allowing for entities to exist.

6
00:00:54,000 --> 00:01:03,000
And so we do this with emotions, and I think it's very important to do it with emotions. I agree with this interpretation.

7
00:01:03,000 --> 00:01:08,000
Anger, at its very essence, is simply disliking. It's a judgment.

8
00:01:08,000 --> 00:01:18,000
The state of being angry doesn't just contain that judgment. It contains judgment after judgment after judgment after moment after moment of judgment.

9
00:01:18,000 --> 00:01:34,000
And the results of those judgments, which is a lot of energy, a lot of stress, a lot of pain, heat in the body. It can be associated with headaches in the head, shaking in the body, red in the face, blood pumping, heart pumping.

10
00:01:34,000 --> 00:01:43,000
But these are all results and habits that have become a part of the system of what we call the body.

11
00:01:43,000 --> 00:01:55,000
What we call the body is simply the habits of the mind that have become lifetime after lifetime, become what we call human.

12
00:01:55,000 --> 00:02:04,000
So, that's about anger. Now, depression is a little bit more intricate, but it can still be broken down into its constituent parts.

13
00:02:04,000 --> 00:02:08,000
And the only way to deal with it is to break it down into its constituent parts.

14
00:02:08,000 --> 00:02:22,000
We talked about this. I've talked with this before in regards to addiction, pornography, or talking about how you can break it down in regards to the looking at something beautiful or attractive and then feeling good about it.

15
00:02:22,000 --> 00:02:27,000
And then one, and then as a result, wanting that feeling or a tad craving that feeling.

16
00:02:27,000 --> 00:02:30,000
Once you create the feeling, then you act for it again.

17
00:02:30,000 --> 00:02:38,000
So, if you break it up into these, or you act to bring it about again, when you act to bring it out again, you see it again.

18
00:02:38,000 --> 00:02:43,000
When you see it again, then there's the good feeling, and then there's more liking, and you have this feedback loop.

19
00:02:43,000 --> 00:02:51,000
Once you break it up into these parts, they're seeing the feeling and the liking, then you can go back and forth between these three.

20
00:02:51,000 --> 00:02:56,000
And you can come to see what you're doing to yourself and what's causing this offering.

21
00:02:56,000 --> 00:03:00,000
And see that there's actually no entity. There's actually nothing to it.

22
00:03:00,000 --> 00:03:08,000
The seeing is meaningless. The pleasant feeling is also meaningless, and the craving is also meaningless.

23
00:03:08,000 --> 00:03:13,000
There's no reason for one to lead to the other, or one to necessitate the other.

24
00:03:13,000 --> 00:03:16,000
There's no logical relationship.

25
00:03:16,000 --> 00:03:21,000
So, with depression, you can break it up in a similar way.

26
00:03:21,000 --> 00:03:28,000
So with depression, you have, and I'm not an expert on this, but thinking back to my depression, you have, of course, anger.

27
00:03:28,000 --> 00:03:36,000
The base of depression is still anger. It's what we call anger, meaning the state of disliking, the partiality.

28
00:03:36,000 --> 00:03:43,000
Because you're not happy about something. It can often be a thought.

29
00:03:43,000 --> 00:03:49,000
And it's generally a reoccurring thought.

30
00:03:49,000 --> 00:03:54,000
It's a perception. It can be a perception based on your current situation.

31
00:03:54,000 --> 00:03:59,000
It can be a memory based on something that is passed. It can be something about the future.

32
00:03:59,000 --> 00:04:05,000
That looks bleak and unappealing and so on.

33
00:04:05,000 --> 00:04:15,000
But then one thing that binds together these emotions that makes us call them depression is the habit.

34
00:04:15,000 --> 00:04:27,000
How it becomes habitual. It's a simmering sort of anger that becomes this habit, where we turn inward on ourselves,

35
00:04:27,000 --> 00:04:34,000
and react to the anger. We don't like something. We're not happy about something.

36
00:04:34,000 --> 00:04:42,000
And we react by closing down, by repressing, by mulling, by fuming.

37
00:04:42,000 --> 00:04:49,000
So the difference with anger, with different kinds of anger, isn't the disliking, it isn't the anger itself.

38
00:04:49,000 --> 00:04:57,000
It's how we deal with it. Or it's how we react to it. Some people react to bad things by getting furious about them.

39
00:04:57,000 --> 00:05:00,000
Some people react to anger by getting depressed about them.

40
00:05:00,000 --> 00:05:13,000
The furious means that person has the tendency to fight. The depression means that person has the tendency to retract within themselves and go back and obsess over them.

41
00:05:13,000 --> 00:05:22,000
It's quite intricate, but must be dealt with by breaking it down into its constituent part.

42
00:05:22,000 --> 00:05:40,000
There is the disliking, and then there is the pain or the sadness. Depression is often related with sadness and this brooding sort of feeling.

43
00:05:40,000 --> 00:05:47,000
But you have to be able to catch the disliking, and then catch the feeling, and then catch the thought.

44
00:05:47,000 --> 00:05:53,000
You have to be able to break it up into these three parts. Whatever it is that's making you depressed.

45
00:05:53,000 --> 00:05:58,000
Find that, and it could be many things for many people. It is a number of things.

46
00:05:58,000 --> 00:06:03,000
But whenever one of those things comes up, you say thinking, thinking, remembering, remembering, or something.

47
00:06:03,000 --> 00:06:09,000
Or if it's seeing, or hearing, you see something that makes you depressed, and seeing it again and again makes you depressed.

48
00:06:09,000 --> 00:06:24,000
So say to yourself, seeing, seeing, when there's the unhappiness, you say unhappy, or pain, pain, or sadness, and when you dislike it, disliking, disliking.

49
00:06:24,000 --> 00:06:31,000
That's the way with everything. So I don't think it helps to think of whether it's an energy or illness or mindset.

50
00:06:31,000 --> 00:06:39,000
It's a conglomeration that becomes a bitual of various things.

51
00:06:39,000 --> 00:06:48,000
One important thing that you could look up is the teaching on the Buddha, Patitya Samupada, which is basically what I was explaining in brief.

52
00:06:48,000 --> 00:07:08,000
That, Patitya, Patitya, Patitya, the six senses lead to contact. Patitya, wait, and now wait in the contact with the senses, seeing, hearing, smelling, tasting, feeling, thinking, leads to feelings, happy feelings, unhappy feelings, neutral feelings.

53
00:07:08,000 --> 00:07:21,000
Wait in the Patitya, so craving, wanting something to be, wanting something to not be, which would be anger, comes from the feelings.

54
00:07:21,000 --> 00:07:31,000
So when you break them up in this way, and when you get rid of your ignorance about this, you come to see what you're doing, and you come to see how it's leading only to suffering.

55
00:07:31,000 --> 00:07:40,000
You're able to change the habit, because the habits are based on misunderstanding. It's because you think that it's going to make you happy.

56
00:07:40,000 --> 00:07:52,000
It's because you think that it's the right way or because of a habit, because you are conditioned to act in that way, because you're not realizing, you don't see what you're doing to yourself, that you do it.

57
00:07:52,000 --> 00:08:02,000
Once you see what you're doing to yourself, you don't, you stop doing that, you stop hurting yourself.

