1
00:00:00,000 --> 00:00:05,040
Is there Buddhist astrology? Can planets really affect our reality?

2
00:00:09,360 --> 00:00:13,920
Remember, you're asking a terror about the Buddhist monk and his friend.

3
00:00:17,360 --> 00:00:26,000
So, I mean, to ask such a broad question, like that, I can't answer properly. I couldn't, but

4
00:00:26,000 --> 00:00:41,920
you know, it's going to be one of those questions where I say, from our tradition, from the point of view of our tradition, the Buddha was

5
00:00:41,920 --> 00:01:01,920
against using astrology, relying on even prognostication, predictions about the future, thinking about the future, is not.

6
00:01:01,920 --> 00:01:17,520
Of interest, monks are not allowed to learn astrology. We're only allowed to learn astronomy to the extent that we can predict, not necessarily calculate the quarters, so that we know when the

7
00:01:17,520 --> 00:01:30,920
the full moon is going to be, when the empty moon is going to be. But I'll go on a limb and say that the Buddha didn't deny it, and probably would have accepted the validity of

8
00:01:30,920 --> 00:01:50,920
the Indian astrology. They even use it. I mean, many people do use it in Thailand. There are astrologers in Thailand, and I've talked to them about it. They've done my horoscope, and it's always an interesting to listen.

9
00:01:50,920 --> 00:02:04,920
I would say, just speculating that it's not that planets affect our reality. It's that there's an interconnectedness to reality. It's all one system.

10
00:02:04,920 --> 00:02:32,920
So if you look at one facet of reality or one aspect of the universe, you can recognize patterns that on a microscopic level or on a local level actually do have an effect of some sort.

11
00:02:32,920 --> 00:03:01,920
Or are mirrored on a local level. So humans might, human behavior might change based on the positions of planets. That being said, I would say it's probably, I would guess that it's about 50% delusion and 50% actual recognition at best.

12
00:03:01,920 --> 00:03:23,920
Because a lot of astrology is just a real positive affirmation. I don't know what the term is, but some sort of recognition, the wishful thinking you want to apply to you.

13
00:03:23,920 --> 00:03:37,920
Now, there was an interesting article, an interesting video that I think I even shared out on the internet with Michael Schurmer, this guy who does skeptic magazine, and he interviewed a...

14
00:03:37,920 --> 00:03:47,920
What's the word that they called Ayurvedic astrologers? What are these Indian astrologers called? People who study this guy who studied Indian Hindu astrology.

15
00:03:47,920 --> 00:04:07,920
And I did an experiment where they had 10 people, something like 10 people, and these 10 people were to give their birth date, and I think their gender.

16
00:04:07,920 --> 00:04:25,920
I don't even know if they gave their gender, but their birth date may have been the only thing that he got. So he got the birth date, and then he recorded a video where he gave a horoscope for each of the 10 people.

17
00:04:25,920 --> 00:04:40,920
And then they got all these 10 people together, and they put this guy behind a two-way mirror so he could watch and see what's going on so that they didn't see him. And they played back the video one person for each person, one by one.

18
00:04:40,920 --> 00:04:55,920
And the things he said about them, they found about a 70 percent, something like a 70 I think to 80 percent validity or correlation.

19
00:04:55,920 --> 00:05:06,920
So some things were the person would say, no, that's not like me, but 70-80 percent of the things that were said were recognized as being correct.

20
00:05:06,920 --> 00:05:16,920
Except for the last two people, because this is the interesting part of the experiment, the experiment is that they switched the two last people's birth dates.

21
00:05:16,920 --> 00:05:29,920
And so when it came to the last two people, he gave the horoscope for the last person to the second last person and gave the horoscope for the second last person to the last person.

22
00:05:29,920 --> 00:05:43,920
And the correlation was like 20 percent or 30 percent. It was exact opposite. Then they told these two people that this is what they'd done, and then flipped them around again and right away got back up to 70-80 percent.

23
00:05:43,920 --> 00:06:05,920
It's a small experiment, but the idea that these things couldn't have an effect, it seems to be well documented in a pseudo-scientific way, whether it would actually survive rigorous scientific testing or something else.

