1
00:00:00,000 --> 00:00:09,720
Today's talk will be on the basics of Vibhasana.

2
00:00:09,720 --> 00:00:20,320
I have talked about this many times here and also in many places, but it is always good

3
00:00:20,320 --> 00:00:34,400
to hear what you have heard before. So that way you can confirm your understanding

4
00:00:34,400 --> 00:00:42,720
and also you can clarify some points that are not clear to you at the first hearing.

5
00:00:42,720 --> 00:00:54,520
And those who have not heard it, it will be a guide for them when they practice Vibhasana.

6
00:00:54,520 --> 00:01:02,400
So those who are practicing Vibhasana meditation should know what Vibhasana is.

7
00:01:02,400 --> 00:01:14,120
So the word Vibhasana is a paly word and it is composed of two parts, a wheat and pasana.

8
00:01:14,120 --> 00:01:22,800
So wheat means in various ways or in many ways and pasana is seeing.

9
00:01:22,800 --> 00:01:31,040
So Vibhasana means seeing in various ways or seeing in many ways.

10
00:01:31,040 --> 00:01:41,800
And seeing in many ways means seeing that mind and matter are impermanent, suffering and

11
00:01:41,800 --> 00:01:49,800
non-soul. Or in other words, it is seeing the three general characteristics of all

12
00:01:49,800 --> 00:02:12,440
conditions phenomena. Now those things that are taught especially in Vidhama have what

13
00:02:12,440 --> 00:02:21,680
are called characteristics. Now Vidhama there are taught consciousness or types of consciousness

14
00:02:21,680 --> 00:02:31,720
and mental states and material properties and each of these has its own characteristic

15
00:02:31,720 --> 00:02:38,600
function, manifestation and proximate cause.

16
00:02:38,600 --> 00:02:47,600
And there are two kinds of characteristics, general characteristics and specific or individual

17
00:02:47,600 --> 00:02:59,640
characteristics. So when we practice Vibhasana meditation, we see both these characteristics.

18
00:02:59,640 --> 00:03:11,680
Now general characteristic means characteristic, characteristics that are common to all units

19
00:03:11,680 --> 00:03:19,400
of dhamma and specific characteristic is individual characteristics.

20
00:03:19,400 --> 00:03:29,600
Now about say 50 people are assembled here in this hall and we are all human beings. But

21
00:03:29,600 --> 00:03:44,040
there are those who are Vietnamese, Chinese, Americans, Burmese, Indonesian and so people

22
00:03:44,040 --> 00:03:54,960
belonging to different nations. Now our being human beings is common to all of us.

23
00:03:54,960 --> 00:04:03,360
So our being human beings is the general characteristic of ours. And then we each of

24
00:04:03,360 --> 00:04:11,520
us has his own characteristic as a Vietnamese, as an American and so on. So being American,

25
00:04:11,520 --> 00:04:21,560
being a Vietnamese or being Burmese is specific characteristics. So in the same way, those

26
00:04:21,560 --> 00:04:29,960
that are taught in Vidhama have these two kinds of characteristics, characteristics that

27
00:04:29,960 --> 00:04:41,080
are peculiar to specific dhamma and also characteristic that is common to all things that

28
00:04:41,080 --> 00:04:58,280
are taught. Now all conditions, things, all conditions phenomena have these three characteristics.

29
00:04:58,280 --> 00:05:07,960
Now conditions means things that are conditioned by some other things that are made by

30
00:05:07,960 --> 00:05:17,080
or produced by some other thing. So conditioning here means both producing and supporting.

31
00:05:17,080 --> 00:05:24,240
So things that are produced or things that are supported by some other things are called

32
00:05:24,240 --> 00:05:30,560
conditioned things. And all conditions, things have these three characteristics because

33
00:05:30,560 --> 00:05:37,320
you make something and that comes into being and that is the beginning of that. When there

34
00:05:37,320 --> 00:05:43,280
is a beginning, there is an end. This is the national law and nobody can interfere with

35
00:05:43,280 --> 00:05:51,440
that law. So when there is a beginning, there is an end and everything that is conditioned

36
00:05:51,440 --> 00:06:04,640
has a beginning. So it has an end also. Therefore, all conditions phenomena arise and

37
00:06:04,640 --> 00:06:12,240
then disappear. So since they arise and disappear, they are impermanent and since they

38
00:06:12,240 --> 00:06:23,360
are impermanent, they are dukha or suffering and they are non-so. So the three characteristics

39
00:06:23,360 --> 00:06:29,840
of impermanent suffering and non-so are the general characteristic of all conditions

40
00:06:29,840 --> 00:06:43,760
phenomena and it is a function of Vipasana to see or to know these three general characteristics.

41
00:06:43,760 --> 00:06:53,920
When you watch the breath, you make notes of the breath as in, out, in, out. You are actually

42
00:06:53,920 --> 00:07:06,360
watching the air element and the air element has the characteristic of extending. When

43
00:07:06,360 --> 00:07:14,320
an air is put in the balloon and balloon becomes extended and that extending is the characteristic

44
00:07:14,320 --> 00:07:22,840
of air element. And this air element arises and disappears and so it is impermanent

45
00:07:22,840 --> 00:07:35,200
and so on. And so impermanence, suffering and non-so nature of the air element is its general

46
00:07:35,200 --> 00:07:48,480
characteristic. So the air element, the specific characteristic of the air element is extending

47
00:07:48,480 --> 00:07:56,480
and its general characteristic, the statistics are impermanence, suffering and non-so.

48
00:07:56,480 --> 00:08:09,440
So with regard to every thing that is taught in the bitama, there are always these two kinds

49
00:08:09,440 --> 00:08:20,800
of characteristics. Since Vipasana is seeing the three characteristics, only when you see

50
00:08:20,800 --> 00:08:29,920
the three characteristics through your practice, can you see that you are practicing Vipasana?

51
00:08:29,920 --> 00:08:39,520
Although you see the three characteristics, you are not in the Vipasana proper. You are in the

52
00:08:39,520 --> 00:09:01,320
vicinity of Vipasana, but not in the Vipasana proper. So only when you see the three characteristics,

53
00:09:01,320 --> 00:09:12,440
practice of Vipasana, there are different stages of knowledge. The Yogi, who practices

54
00:09:12,440 --> 00:09:23,480
Vipasana, first sees mind and matter clearly, or he is able to delimit mind and matter.

55
00:09:23,480 --> 00:09:28,840
That means he is able to differentiate between mind and matter. This is mind, this is

56
00:09:28,840 --> 00:09:38,280
medicine and so on. After that, he reaches a stage where he sees the conditionality of things or

57
00:09:38,280 --> 00:09:50,200
he sees the causes of the mind and matter. During these two stages, he is not seeing the three

58
00:09:50,200 --> 00:10:01,960
characteristics yet. Only when he reaches the third, let us call it third stage, does he begin to

59
00:10:01,960 --> 00:10:11,800
see the three characteristics? That stage is comprehension, stage of comprehension of mind and

60
00:10:11,800 --> 00:10:24,600
matter, and then later he comes to see the arising and disappearing of objects and so on. So among

61
00:10:24,600 --> 00:10:33,960
these stages, strictly according to the definition, the first two stages are not yet Vipasana.

62
00:10:33,960 --> 00:10:45,080
That is why in the book called Abitamata Sangaha, they are not included. These two stages of

63
00:10:45,080 --> 00:10:56,360
knowledge are not included in the 10 Vipasana knowledge. But in the commentaries, they are also

64
00:10:56,360 --> 00:11:05,640
called Vipasana knowledge. Although strictly according to the definition, they are not yet Vipasana

65
00:11:06,520 --> 00:11:15,480
since they lead to Vipasana and since they are in the vicinity of Vipasana, they are also called Vipasana

66
00:11:15,480 --> 00:11:31,400
knowledge. So now we know what Vipasana is. Vipasana is seeing the three general characteristics

67
00:11:31,400 --> 00:11:42,680
of all conditions phenomena. Now what do we do when we practice Vipasana? We observe

68
00:11:42,680 --> 00:11:56,600
all we watch. All we pay attention. All we make mental notes. All we try to be mindful.

69
00:11:57,720 --> 00:12:04,520
So when we practice Vipasana, we try to be mindful. All we practice mindfulness.

70
00:12:04,520 --> 00:12:14,600
So the practice of mindfulness is sometimes said to be observation or watching or paying attention

71
00:12:14,600 --> 00:12:24,760
or making mental notes. So we use different expressions but we mean the same thing. So sometimes

72
00:12:24,760 --> 00:12:33,320
we observe the objects. That means we try to be mindful of the object. We try to be fully aware

73
00:12:33,320 --> 00:12:41,880
of the object. Sometimes we watch the object. That also means we try to be fully aware of the object.

74
00:12:41,880 --> 00:12:47,320
Sometimes we pay attention or we make mental notes. Now making mental notes is also

75
00:12:47,960 --> 00:12:57,800
to go along with mindfulness. Not just mechanically making notes. So when we say make mental notes,

76
00:12:57,800 --> 00:13:09,960
we mean make mental notes with mindfulness. When we observe the object, when we are mindful of the

77
00:13:09,960 --> 00:13:19,880
object, we will not fail to see the object clearly. And when we see the object clearly,

78
00:13:19,880 --> 00:13:31,560
we will also see that that object arises and disappears. So when we see the arising and disappearing

79
00:13:31,560 --> 00:13:41,160
of an object, we know that that object is impermanent. And when we know that object is impermanent,

80
00:13:41,160 --> 00:13:48,200
we also know that that object is suffering. Suffering in the sense of being oppressed by

81
00:13:48,200 --> 00:13:56,680
arising and disappearing. And when we know that the object is impermanent and suffering,

82
00:13:56,680 --> 00:14:03,800
we also know that it is another. It is another means we have no control over this.

83
00:14:05,640 --> 00:14:15,240
It is in substantial and it is not soul or self. So just by observing the object,

84
00:14:15,240 --> 00:14:30,520
we can know the three general characteristics of the object. So when we practice Vipasana,

85
00:14:30,520 --> 00:14:40,920
we just observe the object. We just try to be mindful of the object. In the beginning stages,

86
00:14:40,920 --> 00:14:47,640
it may not be real Vipasana, but when we begin to see the three characteristics, then we are

87
00:14:47,640 --> 00:14:57,240
right in Vipasana. So when we practice Vipasana, what we do is just be mindful of the object.

88
00:14:57,240 --> 00:15:04,760
Sometimes it is helpful to make mental notes. That is why we always say make mental notes,

89
00:15:04,760 --> 00:15:14,600
make mental notes. So making mental notes help people to keep their mind on the object.

90
00:15:14,600 --> 00:15:24,200
It is like using a pin to put a note on the wall. So you want to put the note on the wall and then

91
00:15:24,200 --> 00:15:33,160
you use a pin. Something like that, we use mental notes so that we can put our mind on the object.

92
00:15:33,160 --> 00:15:46,440
But sometimes making mental notes may interfere with the noteings or with the practice.

93
00:15:46,440 --> 00:15:53,480
So if making mental notes become an obstacle, then we don't have to make mental notes.

94
00:15:53,480 --> 00:16:05,160
We just try to be mindful of the object. Sometimes it is better just to be mindful of the object

95
00:16:05,880 --> 00:16:13,960
than making mental notes because sometimes making mental notes can interfere with your understanding.

96
00:16:13,960 --> 00:16:23,000
So you have to use your own discretion. If it helps you to be fully aware of the object,

97
00:16:23,000 --> 00:16:30,040
then you make mental notes. If making mental notes interferes with your practice,

98
00:16:30,680 --> 00:16:36,760
then you don't have to make them but just try to be really mindful of the object.

99
00:16:44,040 --> 00:16:48,920
Now what must we observe when we practice Vipasana?

100
00:16:48,920 --> 00:16:57,560
The answer can be given in two ways. So if we want to impress people,

101
00:16:58,520 --> 00:17:06,600
we will use the words used in the books and we say we must observe the five aggregates of cleaning.

102
00:17:07,800 --> 00:17:08,920
Now this is the answer.

103
00:17:08,920 --> 00:17:20,120
But to give this answer to those who are not familiar with the teachings of the Buddha,

104
00:17:21,400 --> 00:17:29,080
it is like creaks to them. Aggregates of cleaning. They have heard this words, but they may not know

105
00:17:29,080 --> 00:17:42,440
exactly what they mean. Now let me explain in this term. Now when Buddha analyzed the world,

106
00:17:43,320 --> 00:17:53,160
he analyzed it into five groups. They are called five aggregates and these five aggregates are

107
00:17:53,160 --> 00:18:03,080
the aggregate of matter, aggregate of filling, aggregate of perception, aggregate of

108
00:18:03,080 --> 00:18:13,240
mental formations and aggregate of consciousness. Now beings are analyzed into these five aggregates

109
00:18:13,240 --> 00:18:25,480
and inanimate things are just the aggregate of matter. So all things and beings taken together,

110
00:18:27,640 --> 00:18:32,920
they are all these groups of five aggregates.

111
00:18:32,920 --> 00:18:44,440
And these five aggregates, some of these five aggregates are the object of cleaning and some

112
00:18:44,440 --> 00:18:54,680
are not. So here the term five aggregates of cleaning means five aggregates that are the objects

113
00:18:54,680 --> 00:19:08,680
of cleaning or the five aggregates we can cling to. Before you understand the five aggregates

114
00:19:10,200 --> 00:19:18,760
of cleaning, you need to know cleaning. Now what is cleaning? In a bit of my, I just thought that

115
00:19:18,760 --> 00:19:31,240
cleaning consists of at least two things, attachment and wrong view. So these two things are

116
00:19:31,240 --> 00:19:42,200
called cleaning. Sometimes they are, the cleaning is translated as grasping. So taking firmly.

117
00:19:42,200 --> 00:19:55,720
Now we can cling to the objects by attachment or we can cling to them by wrong view.

118
00:19:56,440 --> 00:20:03,880
Now if we see something and we are attached to it, we like it, we take whole of it with our

119
00:20:03,880 --> 00:20:13,800
mind and that means we cling to that object by attachment. And if we also have the wrong view about

120
00:20:13,800 --> 00:20:25,000
that thing that it is permanent, then we are clinging to it by wrong view also. So these two things,

121
00:20:25,000 --> 00:20:33,880
these two mental states, the attachment and wrong view are called cleaning and anything

122
00:20:34,760 --> 00:20:42,200
that can be the object of cleaning are called the aggregates of cleaning. So here

123
00:20:42,200 --> 00:20:58,360
aggregates of cleaning means five aggregates that are the object of cleaning. Now everything in

124
00:20:58,360 --> 00:21:06,360
the world is the object of cleaning. So we can cling to anything in the world. We may be attached

125
00:21:06,360 --> 00:21:15,160
to the thing or we may have wrong view about the thing. And so the five aggregates of cleaning

126
00:21:15,160 --> 00:21:24,520
really means everything in the world that we can cling to by way of attachment or by way of wrong

127
00:21:24,520 --> 00:21:42,120
view. And we cling to them when we experience them, when we have contact with them. And we have

128
00:21:42,120 --> 00:21:51,800
contact with them through the senses, through the eyes, ears, nose, tongue, body and touch.

129
00:21:51,800 --> 00:22:02,280
So we can answer this question in other words, not using the words five aggregates of cleaning.

130
00:22:02,280 --> 00:22:14,120
So we may say that we must observe what becomes evident when we see here smell, taste, touch,

131
00:22:14,120 --> 00:22:26,280
or think. We see something. And when that thing is evident, then we observe that object.

132
00:22:26,280 --> 00:22:37,480
We hear something, some sound. And that becomes evident. And we observe that sound or we

133
00:22:37,480 --> 00:22:47,240
observe or we try to be mindful of hearing. And we smell some sort of bed smell. And when

134
00:22:47,240 --> 00:22:59,320
becomes evident, then we observe the smell or we observe the smelling and so on. So the answer

135
00:22:59,320 --> 00:23:12,200
which everybody can understand is that we must observe what becomes evident when we see here smell,

136
00:23:12,200 --> 00:23:13,640
taste, touch and think.

137
00:23:13,640 --> 00:23:33,400
So in brief, we must observe everything, everything we experience. But there is one thing to note

138
00:23:33,400 --> 00:23:43,080
here, that everything must belong to the ultimate reality. And that everything must not be a concept.

139
00:23:43,800 --> 00:23:52,280
Now there are two things in the world, concept and reality. Concepts are said to be non-existent.

140
00:23:52,280 --> 00:24:01,800
They exist only in our imagination. So concepts have no existence of their own.

141
00:24:01,800 --> 00:24:10,600
And so they do not have the three characteristics. So when we practice wibasana meditation,

142
00:24:11,560 --> 00:24:18,920
we do not take concepts as the object of wibasana. We take the ultimate reality. That means we

143
00:24:18,920 --> 00:24:27,240
take the mind, we take the material properties as the object of wibasana. But we do not take the

144
00:24:27,240 --> 00:24:37,400
concepts as the object of wibasana simply because we cannot see the three characteristics of

145
00:24:38,360 --> 00:24:44,840
the concept. Because they do not possess these three characteristics.

146
00:24:44,840 --> 00:25:04,280
Suppose you are concentrating on the breads. And if you see the breads as concepts,

147
00:25:04,280 --> 00:25:19,080
if you see the breads as something in the form of a stick or something that goes into the body

148
00:25:19,080 --> 00:25:27,480
through the nose and then comes out again. If you see them that way, if you see them as breads

149
00:25:27,480 --> 00:25:35,000
as concepts, then you are not doing wibasana on the breads. You are doing summer meditation

150
00:25:35,000 --> 00:25:45,400
on the breads. Because in most kinds of summer meditation, it is a concept that the object of

151
00:25:45,400 --> 00:25:56,120
meditation. But if you concentrate on the air element, that is the bread, then you are doing

152
00:25:56,120 --> 00:26:02,920
wibasana. So when you are concentrating on the air element, it's moving or it's

153
00:26:04,440 --> 00:26:13,800
extending characteristic, then you are doing wibasana on the breads. So whether you are doing

154
00:26:13,800 --> 00:26:24,200
summer or wibasana may depend on how you observe it. Now if you take the breads as concepts,

155
00:26:24,200 --> 00:26:32,120
then you are doing summer meditation on the breads. But if you try to see the air element in the

156
00:26:32,120 --> 00:26:40,600
breads, not the breads themselves, then you are doing the wibasana meditation on the breads.

157
00:26:41,240 --> 00:26:50,920
So when you practice wibasana meditation, you take the ultimate realities as object and not the

158
00:26:50,920 --> 00:26:59,800
concepts. When you see a man, how would you make mental notes?

159
00:27:03,320 --> 00:27:12,120
Man, man, man, or seeing, seeing, seeing. Now if you make notes, man, man, man, then you are

160
00:27:12,120 --> 00:27:20,680
concentrating on the concept. A concept does not exist. So what we call

161
00:27:20,680 --> 00:27:29,960
man is just a combination of mind and matter. So what is real in a man is just mind and matter,

162
00:27:29,960 --> 00:27:38,520
not the man. So if we make notes as man, man, man, man, we are not doing wibasana. That is why

163
00:27:40,360 --> 00:27:47,720
when we give instructions, we say, make notes as seeing, seeing, seeing. Now seeing, seeing,

164
00:27:47,720 --> 00:27:54,920
seeing means, say, there is seeing consciousness in, in my mind or something like that. So I am,

165
00:27:54,920 --> 00:28:01,640
when I say seeing, seeing, seeing, I am aware of the seeing consciousness in my mind. Or

166
00:28:02,520 --> 00:28:10,760
if you concentrate on the visible object in the man and not, not the, not the whole man,

167
00:28:10,760 --> 00:28:17,320
then you are concentrating on the ultimate reality. Because what we call man is composed of

168
00:28:17,320 --> 00:28:24,520
very small particles of matter that, that, that ultimate reality. So if we concentrate on, on the,

169
00:28:25,320 --> 00:28:35,800
on the visible object, visible data in, in, in the man, or visible data in the man, then we are

170
00:28:35,800 --> 00:28:41,880
doing wibasana. But if we take the man as a whole and say, man, man, man, then we are not doing

171
00:28:41,880 --> 00:28:57,800
wibasana because we are taking the, the concept as an object. So now we know, we must observe

172
00:28:58,360 --> 00:29:05,960
the five aggregates of cleaning when we practice wibasana meditation. Now, why do we have to observe

173
00:29:05,960 --> 00:29:14,840
the five aggregates of cleaning? We have to observe them so that we do not cling to them.

174
00:29:18,920 --> 00:29:27,320
So in order not to cling to them, we have to observe the five aggregates of cleaning.

175
00:29:27,320 --> 00:29:45,880
Then why do we want to avoid cleaning to them? Now, when we cling to them, that means we are

176
00:29:45,880 --> 00:29:53,800
attached to them. And when we are attached to them, we want to possess them. We want to take them

177
00:29:53,800 --> 00:30:04,680
as our own. So we will do anything to possess that thing or we will act. So when we act, sometimes

178
00:30:04,680 --> 00:30:12,920
we may act in a right way or sometimes we may act in a wrong way. Whether we act in a right way

179
00:30:12,920 --> 00:30:23,160
or wrong way, when we act, we accumulate comma because we may do something wholesome or something

180
00:30:23,160 --> 00:30:36,040
and wholesome. So when there is comma as action, there is the result as reaction.

181
00:30:37,000 --> 00:30:44,760
And the result of comma is rebut in the future. And rebut is nothing but suffering.

182
00:30:44,760 --> 00:30:56,520
Now, we want to get rid of suffering. So if we want to get rid of suffering, we must

183
00:30:56,520 --> 00:31:14,920
avoid something that causes suffering. And here, the rebut as aggregates are the result of the

184
00:31:14,920 --> 00:31:23,880
action or comma in the past. And that action or comma is conditioned by cleaning. So whenever

185
00:31:23,880 --> 00:31:30,920
there is cleaning, there will always be action. And when there is action, there is always

186
00:31:30,920 --> 00:31:41,880
be reaction or there will always be result. And the result is nothing but suffering. And so

187
00:31:41,880 --> 00:31:50,520
in order to avoid suffering, in order to get free from suffering, we have to avoid cleaning

188
00:31:50,520 --> 00:31:57,240
to the objects. So in order to avoid cleaning to the objects, we have to observe.

189
00:32:03,080 --> 00:32:14,360
Suppose we hear somebody talk about the pleasures in the celestial realm. So when we hear the

190
00:32:14,360 --> 00:32:22,280
pleasures in the celestial realm, then we want to be reborn in the celestial realm. And we don't

191
00:32:22,280 --> 00:32:28,440
let us say, we don't know what to do to be reborn in the celestial realm. And so we go to a teacher.

192
00:32:29,800 --> 00:32:39,080
And that teacher happens to be a person who had wrong view. And so he may tell us,

193
00:32:39,080 --> 00:32:46,120
oh, you sacrifice a lamb or sacrifice an animal, then you will be reborn in the celestial world.

194
00:32:46,760 --> 00:32:55,800
And taking his advice, we may sacrifice an animal. And when we sacrifice an animal, we are doing

195
00:32:55,800 --> 00:33:03,480
the action. And that action is according to the teachings of the Buddha, unwholesome action,

196
00:33:03,480 --> 00:33:17,240
unwholesome camera. So unwholesome camera will give not desirable or not pleasurable results,

197
00:33:17,240 --> 00:33:22,840
but it will give us painful results. That is one thing.

198
00:33:22,840 --> 00:33:34,200
Then we go to a teacher and he is a good teacher. And he may instruct us to do good things,

199
00:33:34,200 --> 00:33:42,040
to do marriage, to practice charity and so on. So following his advice, we do charity,

200
00:33:42,040 --> 00:33:49,880
say we do make donations and so on. And we acquire merit. And that is Kuzawa, a wholesome act.

201
00:33:49,880 --> 00:33:59,880
So wholesome camera always gives pleasurable, I mean desirable results,

202
00:34:00,680 --> 00:34:08,680
pleasant results. So as a result of our good camera, we may be reborn in celestial world.

203
00:34:09,720 --> 00:34:19,320
Whether we are reborn in hell or in celestial world, there is always dukkah. There is always suffering,

204
00:34:19,320 --> 00:34:32,440
because celestial world is also not permanent. So the clinging leads to action,

205
00:34:32,440 --> 00:34:39,640
clinging leads to wholesome or unwholesome camera, and wholesome and unwholesome

206
00:34:39,640 --> 00:34:49,400
camera leads to or produce good results. So we do not want to get these results because

207
00:34:50,840 --> 00:34:59,960
these results are in the ultimate analysis suffering. And we want to get rid of or get away from

208
00:34:59,960 --> 00:35:14,360
suffering. So in order not to have rebirth in any existence, we try not to have clinging,

209
00:35:14,360 --> 00:35:21,640
we try not to cling to the objects. And in order not to cling to the objects, we have to watch

210
00:35:21,640 --> 00:35:32,600
them or we have to observe them. We have to be mindful of them.

211
00:35:35,320 --> 00:35:42,840
So we have gone through the questions, what is Vipasana? What do we do when we practice Vipasana?

212
00:35:42,840 --> 00:35:49,800
What must we observe when we practice Vipasana? And why do we have to observe them?

213
00:35:49,800 --> 00:36:00,440
Now the question is when are we to observe? Now we are to observe them when they are present.

214
00:36:01,400 --> 00:36:11,000
Now we observe them so that we see the true nature of them. We see the specific characteristic

215
00:36:11,000 --> 00:36:23,960
and the general characteristic of these objects. So in order to see the specific characteristic

216
00:36:23,960 --> 00:36:33,160
and general characteristic of an object, that object must be present. That object must be,

217
00:36:33,160 --> 00:36:40,120
let us say before our eyes or something like that. If it is not present with us,

218
00:36:40,920 --> 00:36:50,360
we cannot observe it. We may recall seeing something, something we have seen in the past.

219
00:36:51,080 --> 00:36:58,200
But that recollection is not as vivid as the thing that we are seeing right now.

220
00:36:58,200 --> 00:37:10,360
And only the thing that is with us, we can examine. We can watch observe and we can see it clearly

221
00:37:10,920 --> 00:37:20,840
and we can see its features or characteristics clearly. So it is important that we observe the

222
00:37:20,840 --> 00:37:32,040
five aggregates of clinging when they are present. That is in the most stages of Vipasana.

223
00:37:35,640 --> 00:37:41,160
Suppose there is a thunderstorm and then there is lightning,

224
00:37:41,160 --> 00:37:54,680
lightning flashes now and then. But you will see the lightning only when it flashes.

225
00:37:56,440 --> 00:38:03,160
If you look at the sky before it flashes, you will not see the lightning. And if you look at

226
00:38:03,160 --> 00:38:12,120
the sky after it flashes, you will not see the lightning. So you will not be able to answer what

227
00:38:12,120 --> 00:38:24,680
you dislike. Simply because you did not see it. Only when it happens to flash and you are looking at

228
00:38:24,680 --> 00:38:36,040
the sky, you will see the lightning. And you will be able to describe what it is like. It is in

229
00:38:36,040 --> 00:38:45,160
the shape of the lines and all these things. So only when you look at the sky when the lightning

230
00:38:45,160 --> 00:38:53,480
flashes do you see the lightning. If you look at the sky before it flashes or after it

231
00:38:53,480 --> 00:39:00,760
flashes, you do not see it. You will be able to see it. There was lightning or there will be

232
00:39:00,760 --> 00:39:06,200
lightning. But you do not see it. So in order to see the lightning and in order to see what it is,

233
00:39:06,200 --> 00:39:11,880
what it looks like and so on, you have to be watching the sky when the lightning strikes.

234
00:39:11,880 --> 00:39:20,760
So in the same way, in order to see the true nature of things, in order to see the object

235
00:39:20,760 --> 00:39:27,160
clearly and its characteristics and so on, it has to be present.

236
00:39:27,160 --> 00:39:49,880
Now, you may want to ask, are we not to observe the past objects and future objects at all

237
00:39:49,880 --> 00:39:59,800
when we practice Vibasana? Now, in the stage of Vibasana where the three characteristics

238
00:39:59,800 --> 00:40:07,400
are comprehended, there is a kind of Vibasana called inferential Vibasana.

239
00:40:07,400 --> 00:40:23,720
That means you infer the impermanent suffering and non-soul nature of an object you have not seen

240
00:40:25,000 --> 00:40:32,680
from the impermanent nature, suffering nature and non-soul nature of the thing you have seen.

241
00:40:32,680 --> 00:40:43,160
Now, in that way, you practice inferential Vibasana. But that inferential Vibasana can come only

242
00:40:43,160 --> 00:40:50,280
after the direct Vibasana. Only after you have seen the object as the present object as

243
00:40:50,280 --> 00:40:57,560
impermanent can you infer that just as this is impermanent, this present thing is impermanent.

244
00:40:57,560 --> 00:41:04,040
So must be the things in the past and so will be the things in the future.

245
00:41:04,040 --> 00:41:16,680
So in this way, you can understand that not only the present objects, but also the past and future

246
00:41:16,680 --> 00:41:28,440
objects are impermanent, but that is not direct seeing, that is a kind of contemplating, a kind of

247
00:41:28,440 --> 00:41:37,400
thinking. That kind of thinking is allowable during that stage of Vibasana knowledge.

248
00:41:37,400 --> 00:41:49,720
And during that stage, a yogi can use his own knowledge of a Vidhamma to define different objects

249
00:41:49,720 --> 00:42:01,400
as impermanent suffering and non-soul. So the present object is the most important object for the

250
00:42:01,400 --> 00:42:12,680
Vibasana yogis, but it does not mean that they must not take the past object and future objects

251
00:42:12,680 --> 00:42:31,960
as object at all. So when we practice Vibasana meditation, we concentrate on the object which is present.

252
00:42:31,960 --> 00:42:45,800
And Vibasana deals with what is prominent, because what is prominent is easy to be noticed.

253
00:42:46,840 --> 00:42:56,760
And in the beginning, when the concentration and understanding are not yet strong,

254
00:42:56,760 --> 00:43:08,840
we should concentrate on what is evident. And we should not be looking for what is difficult to

255
00:43:08,840 --> 00:43:15,880
see. We should not try to find out what is difficult to see and then try to see it. That is

256
00:43:15,880 --> 00:43:28,600
impossible when the concentration and understanding are still not strong. So what is important for

257
00:43:29,320 --> 00:43:42,760
the practitioners of Vibasana is to be mindful of the object at the present moment or to observe

258
00:43:42,760 --> 00:44:01,160
the object at the present moment. And to be mindful of the object at the present moment

259
00:44:01,160 --> 00:44:14,280
is actually taught by the Buddha. So Buddha said, do not let the first come back to you,

260
00:44:15,720 --> 00:44:27,320
do not long for the future. But among who watches the object at the present moment

261
00:44:27,320 --> 00:44:39,320
will not be carried away by craving or wrong view. And so to observe the object at the present

262
00:44:39,320 --> 00:44:51,560
moment is actually taught by the Buddha and all practitioners of Vibasana follow that advice.

263
00:44:51,560 --> 00:44:58,680
And so when we practice, we try to be mindful of the object at the present moment. And following

264
00:44:58,680 --> 00:45:08,920
the statement in the commentaries, we try to be mindful of what is evident, what is prominent

265
00:45:08,920 --> 00:45:19,800
at the present moment. In the sub-commentary on Vishudimaga, it is said, Vibasana contemplation

266
00:45:22,600 --> 00:45:30,280
is according to what is evident. So that means Vibasana deals with what is evident.

267
00:45:30,920 --> 00:45:37,880
So we take what is evident as the object of Vibasana. That is why we take the rising and falling

268
00:45:37,880 --> 00:45:44,040
of the movement as the object of Vibasana or the breath as the object of Vibasana.

269
00:45:44,040 --> 00:45:51,400
So they are not difficult to see these objects are in the breath or the movements of the abdomen.

270
00:45:53,240 --> 00:46:03,560
So we begin with what is easy to see, what is evident at the present moment,

271
00:46:03,560 --> 00:46:14,360
and as the concentration and understanding becomes stronger, then we will be able to see

272
00:46:15,560 --> 00:46:24,600
subtle things also. There are some more questions about the basics of Vibasana

273
00:46:24,600 --> 00:46:31,960
and I think we will continue tomorrow.

