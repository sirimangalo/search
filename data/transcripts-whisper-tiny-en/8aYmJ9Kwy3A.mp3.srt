1
00:00:00,000 --> 00:00:07,000
Monday, based on many of your videos, I don't think you don't like summit meditation

2
00:00:07,000 --> 00:00:08,480
at all.

3
00:00:08,480 --> 00:00:11,440
From what I know, there are two clear ways for enlightenment.

4
00:00:11,440 --> 00:00:15,240
Summit of First and Repassana, or Repassana first and then summit.

5
00:00:15,240 --> 00:00:18,080
What do you think about this?

6
00:00:18,080 --> 00:00:24,920
Actually there are, according to the Buddha, four ways to enlightenment.

7
00:00:24,920 --> 00:00:33,120
Repassana first, then Repassana first and then summit, summit and Repassana together, or

8
00:00:33,120 --> 00:00:36,600
the settling of the mind in regards to the Dhammas.

9
00:00:36,600 --> 00:00:44,480
So one has uncertainty or sort of a kind of a mental disturbance about the Dhamma and

10
00:00:44,480 --> 00:00:48,960
one settles one's mind in regards to the Dhamma.

11
00:00:48,960 --> 00:00:52,280
The Buddha said, those are the four ways people can become an Arahat.

12
00:00:52,280 --> 00:00:56,840
Everyone who comes to the Buddha as an Arat saying that they've become an Arahat does

13
00:00:56,840 --> 00:01:00,320
it by one of these four paths.

14
00:01:00,320 --> 00:01:05,360
So that's the answer to your question, or that's the answer, that's a correction to your

15
00:01:05,360 --> 00:01:06,360
question.

16
00:01:06,360 --> 00:01:14,520
What you know is wrong, they're actually for, according to the Buddha, so it's incomplete.

17
00:01:14,520 --> 00:01:18,200
Let's address the comment at the beginning.

18
00:01:18,200 --> 00:01:23,960
You think I don't like summit the meditation at all.

19
00:01:23,960 --> 00:01:30,600
Well that may be true, I may be guilty of that, but if I am judging as an outsider, I would

20
00:01:30,600 --> 00:01:36,440
say that that's wrong with me, because not liking summit the meditation is a bad thing.

21
00:01:36,440 --> 00:01:44,760
Now I have to be careful about what I say, but I tend to discourage people from summit

22
00:01:44,760 --> 00:01:48,720
meditation, I definitely admit that.

23
00:01:48,720 --> 00:01:53,440
But it's a light discouragement, and I don't ever say to someone, don't practice it.

24
00:01:53,440 --> 00:02:00,480
I try to focus on the benefits of vipassana meditation.

25
00:02:00,480 --> 00:02:02,320
There's two reasons I guess for that.

26
00:02:02,320 --> 00:02:11,160
The first one is it's what I know, so my focusing on it and sort of putting it up higher

27
00:02:11,160 --> 00:02:14,480
than summit to meditation.

28
00:02:14,480 --> 00:02:24,240
It's because by saying that, so if I have person A here, and I want to bring them here,

29
00:02:24,240 --> 00:02:28,120
the best way for me to get them there is to start talking about the benefits of inside

30
00:02:28,120 --> 00:02:33,600
meditation, because if I start talking the benefits of summit to meditation, I'm not

31
00:02:33,600 --> 00:02:35,280
going to be able to follow through with that.

32
00:02:35,280 --> 00:02:38,680
If they say, oh great, so can you teach me summit to meditation?

33
00:02:38,680 --> 00:02:43,200
I'm not bringing them to the goal, because I can't, I don't teach that.

34
00:02:43,200 --> 00:02:47,520
I will end up saying, I'm sorry, I don't teach that, you'll have to go somewhere else.

35
00:02:47,520 --> 00:02:53,320
So quicker for me is to promote vipassana meditation.

36
00:02:53,320 --> 00:02:59,920
But the other reason is, that's complicated, but I've talked about it before, and the

37
00:02:59,920 --> 00:03:09,520
first reason, obviously, is it seems to take longer and require more to do summit to first?

38
00:03:09,520 --> 00:03:14,200
So three things, it seems to take longer, and I'm not, you know, we'll have a big argument

39
00:03:14,200 --> 00:03:18,760
about that, but I think generally there's a case that can be made for it taking longer,

40
00:03:18,760 --> 00:03:25,680
requires more, and has a greater potential for getting the meditator lost.

41
00:03:25,680 --> 00:03:28,240
So these are the three disadvantages of summit to meditation.

42
00:03:28,240 --> 00:03:36,360
Earlier I talked about the benefits, you know, it's more complete, it's stronger, it

43
00:03:36,360 --> 00:03:41,360
is more complete and stronger, more, so let's talk about those.

44
00:03:41,360 --> 00:03:46,680
Okay, more complete and stronger, more complete means you have the potential to enter

45
00:03:46,680 --> 00:03:52,840
into highest states of calm and tranquility, where you can sit stiff as a board, where

46
00:03:52,840 --> 00:04:01,560
you can have great bliss, great equanimity, and more complete in the sense that you are

47
00:04:01,560 --> 00:04:06,320
able to cultivate magical powers, so you can read people's minds, remember past lives,

48
00:04:06,320 --> 00:04:08,600
all sorts of fun stuff.

49
00:04:08,600 --> 00:04:18,880
More powerful, one's ability to enter into cessation, gets more powerful, so once you've

50
00:04:18,880 --> 00:04:23,880
cultivated samata, as soon as you switch to insight often, very quickly, you're able

51
00:04:23,880 --> 00:04:31,040
to cultivate insight meditation, you're entering into Nibana, as soon as you switch, it

52
00:04:31,040 --> 00:04:37,440
takes very little time, and it can be very strong, so you can actually enter into cessation,

53
00:04:37,440 --> 00:04:43,840
Nibana for hours or days or so, so with the power of the summit, those are the two

54
00:04:43,840 --> 00:04:44,840
benefits.

55
00:04:44,840 --> 00:04:50,400
The three disadvantages it takes longer, because you have to first cultivate meditation

56
00:04:50,400 --> 00:04:56,360
based on a concept, which has nothing to do with reality, and then you have to afterwards

57
00:04:56,360 --> 00:05:04,600
cultivate meditation based on reality, so you have to switch, it's two steps, and they're

58
00:05:04,600 --> 00:05:10,800
different, because based on a concept, you're actually avoiding the situation, you're

59
00:05:10,800 --> 00:05:17,440
suppressing the defilements by not focusing on them, by focusing elsewhere, so it takes

60
00:05:17,440 --> 00:05:25,960
longer, it requires more, so in order to do that, you need to seclude yourself, you

61
00:05:25,960 --> 00:05:31,440
need to find a quiet spot, sometimes people can't do that, you need, ideally, to be in

62
00:05:31,440 --> 00:05:38,680
the forest, to be away from any kind of noise, any kind of disruption, be by yourself,

63
00:05:38,680 --> 00:05:42,440
so it takes more time, requires more, and the potential for getting lost, because you're

64
00:05:42,440 --> 00:05:48,600
in the world of concept, so if you don't ever make the change, you can spend years,

65
00:05:48,600 --> 00:05:58,560
lifetimes, practicing meditation without ever making the final step, and this is,

66
00:05:58,560 --> 00:06:07,560
obviously, the case, there's, we can show that through pointing out individual meditators,

67
00:06:07,560 --> 00:06:12,320
we can show that in the diptica, it's clear that this summit of meditation isn't enough,

68
00:06:12,320 --> 00:06:21,760
it leads you to the Brahma world, where it led the Buddhist to the Bodhisattas to teachers.

69
00:06:21,760 --> 00:06:33,200
So for those reasons, those reasons coupled with the fact that I don't have the, we don't

70
00:06:33,200 --> 00:06:39,640
have that, so we generally don't have that much time, we don't have that great resources

71
00:06:39,640 --> 00:06:47,080
of the forest, and so on, and I don't have the ability, I suppose I could, I don't have

72
00:06:47,080 --> 00:06:53,840
the training, I actually, I probably could do fine teaching Samata and Vipassana, but don't

73
00:06:53,840 --> 00:06:59,800
have the technical training to lead people first through Samata and then through Vipassana.

74
00:06:59,800 --> 00:07:06,240
Therefore, we don't do it, we focus more on, on insect meditation.

75
00:07:06,240 --> 00:07:12,200
Now, Samata is great, it's a wonderful thing, it quiets your mind, it calms your mind.

76
00:07:12,200 --> 00:07:18,160
I don't think I really dislike it, in fact, in our tradition, there's a sense of maybe

77
00:07:18,160 --> 00:07:19,160
even jealousy.

78
00:07:19,160 --> 00:07:22,800
You know, it's like, oh, I wish I could get more in the summit than meditation, but

79
00:07:22,800 --> 00:07:26,360
we're strict with ourselves, we say, no, we're going to focus on this, because we don't

80
00:07:26,360 --> 00:07:32,840
have the time, we don't have the resources, we want to streamline it, especially in this

81
00:07:32,840 --> 00:07:40,960
day and age, where time and resources are of the essence, and it's our best attempt at

82
00:07:40,960 --> 00:07:47,080
getting the most people across, you know, like, you want to get people off the Titanic,

83
00:07:47,080 --> 00:07:54,080
when it's sinking, well, you have to improvise, you have to find ways to get them off.

84
00:07:54,080 --> 00:08:00,120
So, I don't think I don't like Samata, I'm pretty sure I don't feel that way.

85
00:08:00,120 --> 00:08:06,760
And I was young, I did practice it, sort of not knowing what it was, but it's very different.

86
00:08:06,760 --> 00:08:11,120
And what I really take issue with is people who don't see the difference.

87
00:08:11,120 --> 00:08:14,600
People who think they're practicing Vipasana, but are actually practicing Samata, and

88
00:08:14,600 --> 00:08:18,320
that's tough, because you can't say that to someone.

89
00:08:18,320 --> 00:08:22,240
You can't just say to someone, you're practice sucks, you know, your practice is inferior

90
00:08:22,240 --> 00:08:28,240
to my practice, and everyone's going to say that their practice is better, but you know

91
00:08:28,240 --> 00:08:34,640
it, you're clear in your mind that this is conceptual, you're not going to reach enlightenment

92
00:08:34,640 --> 00:08:38,520
that way, and so there's this muddling of the two.

93
00:08:38,520 --> 00:08:43,200
So this is why we try to make it clear, that's not going to reach nibana, and be very

94
00:08:43,200 --> 00:08:44,880
clear about that.

95
00:08:44,880 --> 00:08:49,120
And so that's the issue that we always have, is people who try to muddle it.

96
00:08:49,120 --> 00:08:52,600
The other issue, of course, is people who say there's only one way, you need to practice

97
00:08:52,600 --> 00:09:02,480
the janas in order to attain enlightenment, I've talked about that on the Buddhism.stackexchange.com,

98
00:09:02,480 --> 00:09:08,080
which again I think is a better sort of platform for those kind of questions, but anyway,

99
00:09:08,080 --> 00:09:09,080
good question.

100
00:09:09,080 --> 00:09:10,080
Thank you for it.

101
00:09:10,080 --> 00:09:11,080
It's kind of nice.

102
00:09:11,080 --> 00:09:16,120
So it's not often I get one that that calls me out on something like that, so thank

103
00:09:16,120 --> 00:09:17,120
you.

104
00:09:17,120 --> 00:09:31,640
And.

