1
00:00:00,000 --> 00:00:09,240
Session or nibana? Is this to be understood as being experiencing something? Do we always

2
00:00:09,240 --> 00:00:18,640
experience something? Technically, yes, it is the experience of something. Technically there

3
00:00:18,640 --> 00:00:34,640
is a mind which experiences nibana. But it is a bit technicality because what does it mean

4
00:00:34,640 --> 00:00:48,400
to say? The mind has been released. Experience is something that mind hasn't really

5
00:00:48,400 --> 00:00:55,820
the ability to experience anything. It is like a knot that has been untied. There is no

6
00:00:55,820 --> 00:01:16,460
knot left. It is technically an experience but really it is the mind that is like a fire

7
00:01:16,460 --> 00:01:25,340
that has gone out. It has no no seeing, no hearing, no smelling, no tasting, no feeling,

8
00:01:25,340 --> 00:01:33,220
no thinking. It has been released. And of course the question, do we always experience

9
00:01:33,220 --> 00:01:40,020
something? There is not really a we that is experiencing. There is experience and that

10
00:01:40,020 --> 00:01:52,060
experience arises in cases. But this is an experience of non-arising, experience of

11
00:01:52,060 --> 00:01:59,060
sensation. You said it all?

