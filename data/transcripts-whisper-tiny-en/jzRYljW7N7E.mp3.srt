1
00:00:00,000 --> 00:00:09,520
Hi, everyone.

2
00:00:09,520 --> 00:00:13,760
This is Adder, a volunteer with Siri-Mongolow International.

3
00:00:13,760 --> 00:00:19,040
I would like to take a moment today to make a specific call for volunteers.

4
00:00:19,040 --> 00:00:24,120
At the moment, we do not have a house steward scheduled to be at our center after the

5
00:00:24,120 --> 00:00:26,440
14th of August.

6
00:00:26,440 --> 00:00:31,300
Our center is able to operate without a steward and has done so in the past, but the

7
00:00:31,300 --> 00:00:36,240
logistics of this have only grown more difficult with the pandemic.

8
00:00:36,240 --> 00:00:40,920
The responsibilities of a steward involve preparing meals for the students and Bantayu

9
00:00:40,920 --> 00:00:47,600
to Damo, taking care of household chores, and welcoming meditators to the center.

10
00:00:47,600 --> 00:00:52,200
This volunteer position is open only to those who have completed a live course in our

11
00:00:52,200 --> 00:00:57,920
tradition, though interested individuals may come to complete a course and stay on

12
00:00:57,920 --> 00:01:00,240
as a steward afterwards.

13
00:01:00,240 --> 00:01:04,120
For the sake of propriety, we seek a mail steward.

14
00:01:04,120 --> 00:01:09,560
If you are currently in Canada and would like to serve our organization in this manner,

15
00:01:09,560 --> 00:01:14,840
while living simply and abiding by the eight precepts, please visit our contact form or

16
00:01:14,840 --> 00:01:17,360
send a message on our Discord server.

17
00:01:17,360 --> 00:01:26,080
Thanks, may you be well.

