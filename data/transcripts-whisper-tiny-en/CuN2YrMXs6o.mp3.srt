1
00:00:00,000 --> 00:00:04,580
Okay, welcome back to our study of the Dhamupana.

2
00:00:04,580 --> 00:00:15,820
Today we are looking at verses 188 to 192, which read as follows.

3
00:00:15,820 --> 00:00:34,920
Number one, let's see what works for you.

4
00:00:34,920 --> 00:00:41,920
Naitang saranamma gamma sambha dukha pamujati.

5
00:00:41,920 --> 00:00:48,920
Yojabud hanja dhamma ncha, sang hanja saranang kato.

6
00:00:48,920 --> 00:00:55,920
Jattari ariya sattjani sambha panya ya passati.

7
00:00:55,920 --> 00:01:01,920
Dukha nukha sambha dandukha sattjati kamang.

8
00:01:01,920 --> 00:01:06,920
Ariang jatt, ariang jattanki kanma gang.

9
00:01:06,920 --> 00:01:09,920
Dukupa sambha gamminan.

10
00:01:09,920 --> 00:01:12,920
Yitanko saranang kamang.

11
00:01:12,920 --> 00:01:14,920
Yitanko saranamu taman.

12
00:01:14,920 --> 00:01:21,920
Yitanko saranamma gamma sambha dukha pamujati.

13
00:01:21,920 --> 00:01:24,920
So it's a lot five verses altogether.

14
00:01:24,920 --> 00:01:26,920
There's not a lot in there.

15
00:01:26,920 --> 00:01:33,920
It's a simple, straightforward, but it's quite powerful teaching, I think.

16
00:01:33,920 --> 00:01:36,920
So this teaching is in regards to refuges.

17
00:01:36,920 --> 00:01:39,920
The translation is as follows.

18
00:01:39,920 --> 00:01:41,920
Bahumwe saranang yanti.

19
00:01:41,920 --> 00:01:47,920
Many go indeed for refuge, saranang.

20
00:01:47,920 --> 00:01:50,920
Babatani wanani chah.

21
00:01:50,920 --> 00:01:54,920
To mountains and to forests.

22
00:01:54,920 --> 00:02:01,920
Aramarukha jaythyani, to ashrams, to trees, to jaythias.

23
00:02:01,920 --> 00:02:04,920
The jaythias are shrine, I guess.

24
00:02:04,920 --> 00:02:17,920
Nowadays, we use it to mean this statue or large marker in Buddhism, a jaythia or pagoda.

25
00:02:17,920 --> 00:02:19,920
Manu sambha yatajita.

26
00:02:19,920 --> 00:02:24,920
People go to these places for refuge when they are terrified by fear.

27
00:02:24,920 --> 00:02:28,920
Yitanko saranang kaman, this is not a safe refuge.

28
00:02:28,920 --> 00:02:30,920
Yitankasaramamu taman.

29
00:02:30,920 --> 00:02:32,920
This is not the highest refuge.

30
00:02:32,920 --> 00:02:35,920
Not an ultimate refuge.

31
00:02:35,920 --> 00:02:40,920
Yitankasaramamma sambha dukha pamu jati.

32
00:02:40,920 --> 00:02:43,920
Going to such a refuge.

33
00:02:43,920 --> 00:02:51,920
Such a place for refuge. One doesn't free oneself completely from all suffering.

34
00:02:51,920 --> 00:02:56,920
Yitanka budhha jaythyan jaythyan jaythyan katou.

35
00:02:56,920 --> 00:03:04,920
But who goes for refuge to the Buddha, the dhamma and the sangha.

36
00:03:04,920 --> 00:03:15,920
Jatari ariya sajani sambha panya yapasati sees the four noble truths with right wisdom,

37
00:03:15,920 --> 00:03:18,920
or rightly with wisdom.

38
00:03:18,920 --> 00:03:23,920
Dukha nukha sambha dandukha sajati kaman saffering,

39
00:03:23,920 --> 00:03:31,920
the arising of suffering, the cessation or the cessation of suffering.

40
00:03:31,920 --> 00:03:40,920
Ariyan jaythyan kan-gagang, and the eightfold noble path to kupasamanga mean them.

41
00:03:40,920 --> 00:03:45,920
Eightfold noble path which leads to the tranquilizing of suffering,

42
00:03:45,920 --> 00:03:48,920
or the cessation of suffering.

43
00:03:48,920 --> 00:03:55,920
Yitankasarananamegh, this indeed is the safe refuge.

44
00:03:55,920 --> 00:03:59,920
Yitankasaramamu taman, this is a refuge in the ultimate sense.

45
00:03:59,920 --> 00:04:00,920
sense.

46
00:04:00,920 --> 00:04:08,160
Aetan saranamagama, sabhadukha pumujati, having gone to such a refuge, one phrase oneself

47
00:04:08,160 --> 00:04:14,200
for no suffering.

48
00:04:14,200 --> 00:04:28,520
These verses were taught in regards to agidata, an ascetic, but his story goes that in the

49
00:04:28,520 --> 00:04:41,280
time before the Buddha, he was the minister or advisor to the king of the Kosula country.

50
00:04:41,280 --> 00:04:47,560
When the king died, he became advisor to King Pascinity Kosula, who was the king of Kosula

51
00:04:47,560 --> 00:04:49,560
at the time of the Buddha.

52
00:04:49,560 --> 00:04:55,840
Kosula was one of the kingdoms at that time.

53
00:04:55,840 --> 00:05:02,880
And the story goes that Pascinity was very good to this agidata, out of reverence for his father

54
00:05:02,880 --> 00:05:06,640
and reverence for him as his father's advisor.

55
00:05:06,640 --> 00:05:11,640
He took him as his own advisor, but held him up as an equal to himself.

56
00:05:11,640 --> 00:05:16,720
So normally when a king would sit higher than everyone else, he would sit with agidata,

57
00:05:16,720 --> 00:05:23,280
he would have agidata sit equal to him and treat him with great respect as his advisor.

58
00:05:23,280 --> 00:05:31,480
Agidata thought to himself something that's a little bit interesting, because even though

59
00:05:31,480 --> 00:05:37,600
Pascinity was treating him very well, he thought to himself, well, kings are unpredictable.

60
00:05:37,600 --> 00:05:40,640
And today he's treating me well, but who knows what the future will bring.

61
00:05:40,640 --> 00:05:46,480
Maybe one day he'll have a whim and he'll decide he no longer has need for me.

62
00:05:46,480 --> 00:05:52,080
I think there's some underlying context here, because Pascinity was not enlightened being

63
00:05:52,080 --> 00:05:53,440
by any means.

64
00:05:53,440 --> 00:06:00,000
He had a lot of his own attachments and partialities and so on, and he appears to have been

65
00:06:00,000 --> 00:06:03,080
a little bit of a rough sort of character.

66
00:06:03,080 --> 00:06:08,560
And so I imagine some of the things he saw Pascinity doing, killing and be heading and

67
00:06:08,560 --> 00:06:12,800
going to war and hit the greed and so on.

68
00:06:12,800 --> 00:06:19,640
The whim, Pascinity was a good person, I think, in some ways, but it wasn't a great person.

69
00:06:19,640 --> 00:06:28,720
And so Agidata probably seeing this thought to himself, and he realized, and I think even

70
00:06:28,720 --> 00:06:42,880
deeper, on a deeper level, well, there's a teaching here, and it's this realization

71
00:06:42,880 --> 00:06:54,960
that there's a lot of suffering involved and potential to living in lay life, living in any situation

72
00:06:54,960 --> 00:06:59,440
where you have to put up with the whims of others.

73
00:06:59,440 --> 00:07:08,560
When you have a boss, when you have a master, when you have even just fellows in life competition

74
00:07:08,560 --> 00:07:17,480
and relationships and society, all the many people we have to meet with in society are

75
00:07:17,480 --> 00:07:24,720
some good, some bad, but there's many challenges and potential for great suffering.

76
00:07:24,720 --> 00:07:33,200
And so he probably started to realize all of this, that this life as a minister was not

77
00:07:33,200 --> 00:07:34,800
all that was cracked up to be.

78
00:07:34,800 --> 00:07:40,520
There was still something missing, and there was still this insecurity.

79
00:07:40,520 --> 00:07:46,320
He would have seen all of the suffering that comes to the people who commit bad deeds or

80
00:07:46,320 --> 00:07:52,800
people who were victims of others bad deeds, he would have seen how the king sometimes

81
00:07:52,800 --> 00:07:59,800
made mistakes or followed his own partiality and did bad things, bad deeds.

82
00:07:59,800 --> 00:08:05,880
And he would have started, he seemed to have started to realize that life in this

83
00:08:05,880 --> 00:08:08,600
sphere is not safe.

84
00:08:08,600 --> 00:08:20,960
There's always the potential for suffering for someone to harm me or to manipulate me in

85
00:08:20,960 --> 00:08:21,960
various ways.

86
00:08:21,960 --> 00:08:26,760
So he decided that this was no longer the life for him and he asked permission to leave

87
00:08:26,760 --> 00:08:35,160
the kingdom and he ended up shaving off his hair and beard and putting on a robe or some

88
00:08:35,160 --> 00:08:40,840
such thing and not becoming a Buddhist monk, but going off and becoming an ascetic.

89
00:08:40,840 --> 00:08:45,520
And they say he took a whole bunch of people with him and ended up being a fairly well-established

90
00:08:45,520 --> 00:08:52,240
teacher, not as an advisor to the king, but as a teacher to people who were also interested

91
00:08:52,240 --> 00:08:56,760
in leaving the household life.

92
00:08:56,760 --> 00:09:03,160
And the way he would instruct people, based on this sort of wisdom that he had gained,

93
00:09:03,160 --> 00:09:10,800
which I think is perfectly valid and valuable, this wisdom that the living in society

94
00:09:10,800 --> 00:09:13,640
living amongst people is fraught with danger.

95
00:09:13,640 --> 00:09:20,400
He's always subject to their whims and proclivities and their defilements, your defilements

96
00:09:20,400 --> 00:09:26,200
and the conflicting of defilements, the conflicting of un also mind states, if you are

97
00:09:26,200 --> 00:09:30,640
if I want something and you want something we fight over and if I'm prone to anger and

98
00:09:30,640 --> 00:09:35,200
you're prone to anger, we will fight and so on.

99
00:09:35,200 --> 00:09:40,200
So he, based on this, he taught his students to leave society.

100
00:09:40,200 --> 00:09:50,120
He said, true refuge is on the mountains, it's in the forest, it's in under the trees and

101
00:09:50,120 --> 00:09:57,080
so on and so on, in these lonely places, leave behind people, leave behind society and

102
00:09:57,080 --> 00:10:03,920
you'll find refuge at the top of a mountain or deep in the jungle.

103
00:10:03,920 --> 00:10:09,760
And he taught his students one other thing that's sort of incidental to the story that

104
00:10:09,760 --> 00:10:17,960
he said, anytime when you're living alone, you give rise to some unwholesome action or

105
00:10:17,960 --> 00:10:26,280
speech or thought, take a jar and go down to the river and scoop some sand into the jar

106
00:10:26,280 --> 00:10:32,480
and bring it and dump it right in this one place and they designated a place where they

107
00:10:32,480 --> 00:10:37,160
would all come and bring this sand and they would have a pot full of sand for every time

108
00:10:37,160 --> 00:10:38,160
they did something wrong.

109
00:10:38,160 --> 00:10:50,240
It was kind of something, some cultural, some teaching method to help people to recognize

110
00:10:50,240 --> 00:10:57,120
and admit their failings and their faults, which I think is also quite wholesome.

111
00:10:57,120 --> 00:11:03,840
And eventually, as these people were off in the forest and I guess he didn't have anything

112
00:11:03,840 --> 00:11:07,760
much more profound than that to teach them, well, absolutely they were going to still

113
00:11:07,760 --> 00:11:10,400
give rise to unholesome list.

114
00:11:10,400 --> 00:11:15,520
And so this second pile started to accumulate and it got bigger and bigger until eventually

115
00:11:15,520 --> 00:11:20,920
it became a shrine, it became a very special place and people would come and worship it

116
00:11:20,920 --> 00:11:27,840
and the story goes that there was a dragon living there, some kind of a snake king in

117
00:11:27,840 --> 00:11:36,280
one of our previous stories, decided to live there and was living in this sand pile.

118
00:11:36,280 --> 00:11:41,240
And then comes along the Buddha, so this is the introduction to the story, this ascetic

119
00:11:41,240 --> 00:11:49,240
teacher of ascetics was off living on his own preaching about the refuge of the solitary

120
00:11:49,240 --> 00:11:56,400
places and so the Buddha sent Mogulana and he said to Mogulana, go and teach this guy

121
00:11:56,400 --> 00:12:03,600
what's wrong with what he's saying and Mogulana went and the story goes that he ended

122
00:12:03,600 --> 00:12:10,000
up, but he asked for permission to stay with these guys, he was going to spend some time

123
00:12:10,000 --> 00:12:14,040
there and maybe question them about their views and tell them about his own views and

124
00:12:14,040 --> 00:12:15,280
so on.

125
00:12:15,280 --> 00:12:21,800
But they wouldn't let him stay, again that they said there's no room for you, I don't

126
00:12:21,800 --> 00:12:32,240
know if he was afraid or just didn't like Buddhism or something, didn't like these ascetics,

127
00:12:32,240 --> 00:12:36,880
these disciples of the ascetic kotama, but they wouldn't let him stay and he said well what

128
00:12:36,880 --> 00:12:42,680
about that sand pile can they stay on that sand pile and he said oh no there's a great naga

129
00:12:42,680 --> 00:12:47,320
dragon living there and he said oh that's fine I'll go and stay with him and he said

130
00:12:47,320 --> 00:12:54,880
oh that dragon is very temperamental, he'll surely kill you and Mogulana had magical powers

131
00:12:54,880 --> 00:13:00,080
so in the morning anyway that part of the story, really inconsequential to our teaching

132
00:13:00,080 --> 00:13:08,320
I mean this is the fantastical part that I fully am willing to accept that many people

133
00:13:08,320 --> 00:13:14,440
will not believe that part so absolutely optional you don't have to concern yourself

134
00:13:14,440 --> 00:13:20,360
with that but the story goes that Mogulana went and subdued the dragon and then in the

135
00:13:20,360 --> 00:13:25,200
morning he was sitting and they came to the sand pile and he's still alive or if this

136
00:13:25,200 --> 00:13:30,440
snake if the dragon killed him but he was sitting on top of the sand pile and the dragon

137
00:13:30,440 --> 00:13:40,720
was the snake extended its hood like a cobra over a Mogulana and then that day the Buddha

138
00:13:40,720 --> 00:13:46,160
came and Mogulana got down and the Buddha went up and sat on the sand pile and he talked

139
00:13:46,160 --> 00:13:52,240
to Agidata, Agidata came and talked to him and he talked to Agidata and said what is it you

140
00:13:52,240 --> 00:14:00,440
teach and Agidata told him I teach this this is what I teach my students so barring the

141
00:14:00,440 --> 00:14:06,240
whole dragon part it's a simple story of a monk often an ascetic often the forest teaching

142
00:14:06,240 --> 00:14:13,520
his students to find true refuge in solitude it's quite simple the Buddha said that's not true

143
00:14:13,520 --> 00:14:18,680
solitude that's not true refuge it's not safe you're telling these people they're going

144
00:14:18,680 --> 00:14:23,120
to be safe from the fountains they bring all their defilements with them and so he taught

145
00:14:23,120 --> 00:14:33,320
these verses so from the story I think one important lesson we can gather is what Agidata

146
00:14:33,320 --> 00:14:40,520
got right and that is the nature of the household life and how powerful it is to leave

147
00:14:40,520 --> 00:14:49,440
behind the household life you give up so much you give up a lot of wealth and pleasure and

148
00:14:49,440 --> 00:14:56,840
ease and comfort but you gain a great amount of safety and security not not real and true

149
00:14:56,840 --> 00:15:01,120
safety and security we'll get to why obviously that should be fairly obvious why this guy

150
00:15:01,120 --> 00:15:07,600
had the wrong idea but there is a certain amount of safety and security that comes because

151
00:15:07,600 --> 00:15:13,560
there's no fears there's no murderers there's no temptations there's no addictions

152
00:15:13,560 --> 00:15:19,280
right if you live off in the forest maybe these guys were living off of fruits and nuts

153
00:15:19,280 --> 00:15:24,320
and what leaves whatever they could gather in the forest to living a very simple life

154
00:15:24,320 --> 00:15:29,280
probably they had simple robes some of them even more just tree bark they had special

155
00:15:29,280 --> 00:15:36,160
they had from certain trees you could get this sort of soft cloth from the tree bark like

156
00:15:36,160 --> 00:15:40,440
you just cut the tree bark off and where it's something like that maybe they would

157
00:15:40,440 --> 00:15:46,240
go naked even and these were all very simple ways of living it was a way of answering

158
00:15:46,240 --> 00:15:54,160
this question of how you escape how you find security when it seems like anywhere you

159
00:15:54,160 --> 00:16:03,720
go in in society any path that you pick is fraught with danger and uncertainty the falling

160
00:16:03,720 --> 00:16:18,680
prey and being a victim the whims of other people and of society in general and so this

161
00:16:18,680 --> 00:16:23,680
is an important teaching it's an important thing to realize that there are ways and there

162
00:16:23,680 --> 00:16:29,480
are alternatives and there is a way of living a choice we can make to live our life more

163
00:16:29,480 --> 00:16:39,640
simply and there's a great security that comes from that but the verse teaching is suppose

164
00:16:39,640 --> 00:16:48,360
more core and more important it's the difference between finding safety security piece happiness

165
00:16:48,360 --> 00:16:54,400
in physical things in your physical location right people come to our meditation center sometimes

166
00:16:54,400 --> 00:17:00,800
thinking that they're going to be able to leave behind all of their problems quite often

167
00:17:00,800 --> 00:17:04,800
to be honest people will come and think that meditation should be quite peaceful and

168
00:17:04,800 --> 00:17:10,760
comfortable and pleasant they should be able to leave behind all the suffering of life and

169
00:17:10,760 --> 00:17:15,920
people become monks for the same reason I often get requests from people to give information

170
00:17:15,920 --> 00:17:21,200
they want help with becoming a monk and I tend to dismiss it because too often we see

171
00:17:21,200 --> 00:17:26,840
people who are focused on becoming a monk have the wrong attitude the rather inclination

172
00:17:26,840 --> 00:17:36,800
is to run away and they end up complaining and suffering and not feeling not being

173
00:17:36,800 --> 00:17:49,120
able to fit in or live or become comfortable as a monk because they're unable to face

174
00:17:49,120 --> 00:17:53,840
the suffering and the problems that exist inside and that's the point is you come here

175
00:17:53,840 --> 00:17:59,640
to a meditation center to face your problems not to leave them behind it become a monk

176
00:17:59,640 --> 00:18:06,240
to face life to face your problems so going to the going to the mountains or to the

177
00:18:06,240 --> 00:18:12,360
forests or to a tree or anywhere and thinking that you're going to find safety there is

178
00:18:12,360 --> 00:18:21,480
ridiculous it's in fact in a deep sense it's the opposite of what's going to happen

179
00:18:21,480 --> 00:18:26,720
a person who goes off to the forest without any guidance is going to have a large pile

180
00:18:26,720 --> 00:18:34,080
of sand if that's the thing they're going to have done a lot of bad deeds of action speech

181
00:18:34,080 --> 00:18:40,280
and thought there's nothing to temper them they might even go crazy because they have

182
00:18:40,280 --> 00:18:47,280
to face themselves when you go off to the forest off into the mountain the first part

183
00:18:47,280 --> 00:18:53,160
of what I said is true you gain some security from the outside and what's great about

184
00:18:53,160 --> 00:18:59,680
it and why the Buddha recommended it certainly as a part of the practice is because it

185
00:18:59,680 --> 00:19:04,760
gives you the opportunity to face your demons you can't face them in society you're constantly

186
00:19:04,760 --> 00:19:10,800
bombarded by other people you're forced to engage with and to get involved with their

187
00:19:10,800 --> 00:19:17,400
problems in their situation so facing your own demons is very difficult when you're living

188
00:19:17,400 --> 00:19:23,640
in society and that's the reason why one should leave not because one wants to run away

189
00:19:23,640 --> 00:19:30,360
maybe but because one wants to do good work hard work the hardest work perhaps and that's

190
00:19:30,360 --> 00:19:37,720
the work of purifying your own mind fighting your own demons fighting your demon not running

191
00:19:37,720 --> 00:19:45,080
away from them and so that's why the Buddha said these are not these things are not a refuge

192
00:19:45,080 --> 00:19:50,240
you can't take refuge in these things and hope to these places and hope to be safe though

193
00:19:50,240 --> 00:19:54,600
the true refuge is the Buddha the Dhamma and the Sangha and particularly the four

194
00:19:54,600 --> 00:19:58,920
noble truths as you notice that's what the Buddha talked about here he doesn't just talk

195
00:19:58,920 --> 00:20:05,240
about it doesn't just mention the three refuges the Buddha the Dhamma and the Sangha he specifically

196
00:20:05,240 --> 00:20:11,560
brings out the four noble truths which I think is important in worth commenting on but the second

197
00:20:11,560 --> 00:20:19,240
part is the teaching that the three refuges the Buddha the Dhamma and the Sangha are truly safe

198
00:20:19,240 --> 00:20:27,800
and ultimate refuge the Buddha is a refuge the Buddha is a refuge in a physical sense on a superficial

199
00:20:27,800 --> 00:20:34,600
level because living with the Buddha is a great support for your practice if any of us had the

200
00:20:34,600 --> 00:20:45,880
opportunity to be with the Buddha and were open to it and had the great capacity in our hearts

201
00:20:46,520 --> 00:20:52,520
to listen to the Buddha's teaching and it would be a great support it would be a great refuge to

202
00:20:52,520 --> 00:20:59,720
have the the honor and the opportunity to hear the Buddha's teaching and to follow his teaching

203
00:20:59,720 --> 00:21:04,920
directly it would be a great refuge but that's not of course the in the deeper sense taking refuge

204
00:21:04,920 --> 00:21:10,040
in the Buddha is a psychological thing it's a mental thing when you intend to when you say to

205
00:21:10,040 --> 00:21:16,360
yourself I will follow the teachings of the Buddha and when someone goes to the Buddha and

206
00:21:16,360 --> 00:21:25,640
except puts themselves in the position of a student lowers themselves and puts the Buddha up

207
00:21:25,640 --> 00:21:33,480
your my teacher and makes a promise or an intention to do what the Buddha tells them to do to

208
00:21:33,480 --> 00:21:39,160
follow his teachings to practice accordingly that's what it means to take refuge in the Buddha

209
00:21:40,040 --> 00:21:45,000
it doesn't mean you have to you know value your life to the Buddha you know be convert to

210
00:21:45,000 --> 00:21:51,000
Buddhism that's not what I'm saying it's the sort of thing we do when a person comes to do a

211
00:21:51,000 --> 00:21:56,360
meditation course here in a traditional setting here we haven't started doing it but I thought

212
00:21:56,360 --> 00:22:02,040
about making it optional we would do a traditional opening ceremony where one would take the

213
00:22:02,040 --> 00:22:08,280
refuges and the precepts but more importantly one would ask for the meditation practice please

214
00:22:08,280 --> 00:22:14,440
teach me and would say I give myself over to the Buddha for the duration of this course I give

215
00:22:14,440 --> 00:22:20,600
myself over I'm no longer going to follow my own whims I'm no longer going to be in charge I'm not

216
00:22:20,600 --> 00:22:27,320
going to have any say I'm going to listen follow and practice whatever you tell me to do you're

217
00:22:27,320 --> 00:22:34,440
putting yourself in that position as a part of what it means to take refuge you really whole

218
00:22:34,440 --> 00:22:45,560
heartedly say I go to the Buddha I'm I'm putting myself under their care under their protection

219
00:22:46,840 --> 00:22:54,440
the dhamma is the refuge of course is the most central part of the Buddhist teaching is that

220
00:22:54,440 --> 00:23:01,080
the practice itself we practice the dhamma in order to realize the dhamma this is the

221
00:23:01,080 --> 00:23:10,840
great the most central refuge how is the dhamma the refuge because this is what truly leads to safety

222
00:23:10,840 --> 00:23:17,720
it's it's a safety in the path in the practice and it's a safety in the result the safety in the

223
00:23:17,720 --> 00:23:32,520
practice is you start you are kept in a fortress you are kept safe safe from your own

224
00:23:32,520 --> 00:23:38,680
defilements from your own unwholesome thoughts when you when your give rise to anger and your

225
00:23:38,680 --> 00:23:45,160
mindful it's able to cut off and prevent you from shouting or hurting other people with speech

226
00:23:45,160 --> 00:23:51,560
or action it's even capable of preventing anger from arising or greed from arising or arrogance

227
00:23:51,560 --> 00:24:00,680
can see through all these things from arising if you're skilled and capable of being mindful in

228
00:24:00,680 --> 00:24:05,400
the present moment and you're able to cut it off and you know when you have pain for example and

229
00:24:05,400 --> 00:24:11,880
you say to yourself pain pain there's no reaction to it no anger need to arise when you see

230
00:24:11,880 --> 00:24:19,000
something you like or feel something pleasant there's a feeling feeling or seeing or whatever it

231
00:24:19,000 --> 00:24:24,680
might be and you don't give rise to the unwholesome yes so the dhamma the practice of it is a great

232
00:24:24,680 --> 00:24:30,760
refuge and when it leads to the wisdom that it leads to but it's had some up and yaya passat

233
00:24:30,760 --> 00:24:38,040
you and you see with wisdom is is the ultimate refuge is the the highest and most supreme refuge

234
00:24:38,040 --> 00:24:43,560
that's freedom from suffering because what happens as you start to see things clearly and are

235
00:24:43,560 --> 00:24:51,000
more objective is you start to let go you start to see the difference between clinging to things

236
00:24:51,000 --> 00:24:58,840
which leads to suffering and objective equanimus observation which keeps you free from suffering

237
00:25:00,680 --> 00:25:04,600
and you see that more clearly and more clearly and to finally your mind let's go there's a

238
00:25:04,600 --> 00:25:14,760
realization nothing's worth clinging to and you let go the sangha is the refuge as I think

239
00:25:14,760 --> 00:25:20,680
I'm underappreciated the sangha is it's an important refuge it's an important one of these three

240
00:25:21,720 --> 00:25:27,240
it's the most important for us but even in the time of the Buddha the Buddha was constantly

241
00:25:27,240 --> 00:25:35,320
reminding us students about the importance of the sangha because it's the sangha that carries

242
00:25:35,320 --> 00:25:40,520
all out and protects and supports the Buddhist teaching supports the practice that's not

243
00:25:41,080 --> 00:25:43,960
doesn't even have to be the Buddhist teaching just the practice of goodness

244
00:25:45,080 --> 00:25:50,360
sangha means community it's the community of people who have practiced rightly who are

245
00:25:50,360 --> 00:25:59,560
able to help others practice who are practicing together as a support, as a model, as an example

246
00:25:59,560 --> 00:26:10,440
for others and those who provide advice and encouragement and those who simply remember the

247
00:26:10,440 --> 00:26:15,000
Buddha's teaching and pass them on and protect them those who keep the Buddha's teaching

248
00:26:15,000 --> 00:26:21,320
writing them down and memorizing them, chanting them and teaching them and so on

249
00:26:22,920 --> 00:26:28,440
all of this is the sangha and it's a great refuge it's a refuge because of the teachings

250
00:26:28,440 --> 00:26:33,880
that we get just the information alone all of the books that we have on the Buddha's teaching

251
00:26:33,880 --> 00:26:42,200
it's all because of the sangha and so the sangha is providing this to us their support their

252
00:26:42,200 --> 00:26:50,760
protection keeping us in the realm of right view by giving us teaching was that teacher right

253
00:26:50,760 --> 00:26:59,400
view how to see things in the right way it's a great support I think more deeply there's also

254
00:27:00,360 --> 00:27:08,280
the idea of the teacher so we don't call it I don't call myself a teacher anything but a person

255
00:27:08,280 --> 00:27:14,360
who gives teaching and someone we should take refuge in and so we during the meditation course

256
00:27:14,360 --> 00:27:24,360
we put ourselves in there in their protection as well so we say I give myself over to you

257
00:27:25,400 --> 00:27:30,440
I will follow the things you say that sort of thing and by doing that that's a great protection

258
00:27:30,440 --> 00:27:39,400
for us it gives a certain sincerity and whole hardness and freedom from fear or doubt or worry

259
00:27:40,120 --> 00:27:46,280
just having someone you know the great difference between practicing on your own at home trying

260
00:27:46,280 --> 00:27:54,200
to decide for yourself what is the correct way to practice and having someone who will guide you

261
00:27:54,200 --> 00:28:00,760
lead to it's night and day really anyone who's done these courses sometimes it seems magical how

262
00:28:00,760 --> 00:28:07,080
easy it is when you have a teacher how much easier it is how much more powerful and how much

263
00:28:07,080 --> 00:28:14,200
further you can go when you have someone guiding you so taking that as our refuge just coming to

264
00:28:14,200 --> 00:28:19,640
do a meditation course or finding a teacher somewhere it's a great refuge if they're teaching

265
00:28:19,640 --> 00:28:25,720
if they're actually the Sangha and they're teaching the Buddha's teaching the last part of

266
00:28:25,720 --> 00:28:38,280
the verse or the last point in the set of verses is the Four Noble Truths now it said that the

267
00:28:38,280 --> 00:28:43,640
Buddha tried always to include the Four Noble Truths in his teaching because it's the core

268
00:28:43,640 --> 00:28:52,520
message it's the essence of what the Buddha taught and if you if you miss that you haven't actually

269
00:28:52,520 --> 00:28:57,880
taught the Buddha's teaching if you teach something off to the side or that that's not touching

270
00:28:57,880 --> 00:29:02,440
upon at least in some way the Four Noble Truths you haven't actually gotten to the point you've

271
00:29:02,440 --> 00:29:07,640
missed that which will lead people to freedom from suffering because that's what the Four Noble

272
00:29:07,640 --> 00:29:15,560
Truths are but in another sense the Four Noble Truths are a refuge the Buddha doesn't say that

273
00:29:15,560 --> 00:29:21,560
exactly but as the core of the Dhamma it's clear that they are a refuge suffering is a refuge

274
00:29:21,560 --> 00:29:28,600
suffering as a teaching is a refuge suffering it's not a refuge but the understanding of suffering

275
00:29:28,600 --> 00:29:33,560
is the greatest refuge when you see what it is that causes you suffering when you see what it is

276
00:29:33,560 --> 00:29:41,960
that is going to hurt you if you cling to it then of course that's the greatest refuge because

277
00:29:41,960 --> 00:29:46,680
then you don't cling to it so this is the idea and Buddhism of why we suffer is because we cling

278
00:29:46,680 --> 00:29:54,120
to things the second Noble Truths the cause of suffering is craving or clinging and so when

279
00:29:54,120 --> 00:29:59,320
you start to see that these things that we cling to are suffering are stressful when you see that

280
00:29:59,320 --> 00:30:05,560
are trying to control things when you see that are stressing out over things or getting angry about

281
00:30:05,560 --> 00:30:09,960
things or clinging to things or needing things when you see that that is suffering you'll

282
00:30:09,960 --> 00:30:15,160
then go on it you stop you're no longer engaged in those activities why because they cause you

283
00:30:15,160 --> 00:30:26,760
suffering so the First Noble Truths just understanding suffering is a great refuge the Second Noble

284
00:30:26,760 --> 00:30:32,840
Truths is a refuge but when you when it's abandoned abandoning craving of course means no

285
00:30:32,840 --> 00:30:38,120
suffering it's what comes from seeing the First Noble Truth clearly the Third Noble Truth is

286
00:30:38,120 --> 00:30:43,720
is nibhana freedom from suffering so it is the highest refuge and the Fourth Noble Truth is

287
00:30:43,720 --> 00:30:51,160
the path so again getting back to practice as a refuge just having a path of practice is a huge

288
00:30:51,160 --> 00:30:56,920
safety again as I said it's like having a fortress so the path which leads the cessation from

289
00:30:56,920 --> 00:31:04,280
suffering is the ultimate practical refuge because it not only would not only it leads to nibhana

290
00:31:04,280 --> 00:31:10,680
but because it protects us it's a way of life it's a way of being that is simple pure and perfect

291
00:31:11,800 --> 00:31:13,160
if you can put it into practice

292
00:31:13,160 --> 00:31:26,040
so that's the dhammapada for today really a reminder that the greatest refuge is our practice

293
00:31:26,040 --> 00:31:37,320
and our commitment to I guess the organization in a sense the organization or whatever word you

294
00:31:37,320 --> 00:31:46,120
want to use religion maybe that includes and the Buddha the dhamma and the sangha but it includes

295
00:31:46,120 --> 00:31:55,960
them as a program by which we practice to free ourselves from suffering there's no great refuge

296
00:31:55,960 --> 00:32:03,880
that comes from being here or being there having this or having that even just being at a meditation

297
00:32:03,880 --> 00:32:11,080
center or becoming among does not a true refuge refuge has to be deeper true refuge true safety

298
00:32:11,800 --> 00:32:17,240
another place the Buddha said it's the four foundations of mindfulness so in the Eightfold Noble

299
00:32:17,240 --> 00:32:23,880
Path the path that leads to freedom from suffering we have the four foundations of mindfulness sort

300
00:32:23,880 --> 00:32:30,760
of as the practical aspect there what you engage in in a practical sense when you cultivate

301
00:32:30,760 --> 00:32:36,600
meditation when you're doing walking or sitting you're cultivating the four foundations of mindfulness

302
00:32:36,600 --> 00:32:43,160
and all the other parts of the Eightfold Noble Path but it means you make yourself a refuge

303
00:32:43,160 --> 00:32:48,440
the Buddha said at the he at the no not all one should be one's own protector or refuge

304
00:32:50,360 --> 00:32:52,440
and the way you do that is through your practice

305
00:32:52,440 --> 00:33:05,800
so another good teaching and very core teaching in the Buddha's dhamma so that's the dhamma

306
00:33:05,800 --> 00:33:26,680
the Buddha for tonight thank you all for listening

