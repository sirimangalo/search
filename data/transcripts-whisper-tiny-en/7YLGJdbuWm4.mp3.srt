1
00:00:00,000 --> 00:00:12,000
I come back to ask among today's question comes from JKIX22 who says, what is the

2
00:00:12,000 --> 00:00:16,720
best way when you're starting out not to get frustrated? I can't seem to keep my mind

3
00:00:16,720 --> 00:00:29,800
quiet enough. One thing to say is that the best way to start out is to find a

4
00:00:29,800 --> 00:00:38,200
meditation center where you can undergo intensive meditation course because

5
00:00:38,200 --> 00:00:44,160
that'll help you to work out a lot of the issues that you have, the

6
00:00:44,160 --> 00:00:49,080
frustration, the things that lead to frustration when you're with the teacher,

7
00:00:49,080 --> 00:00:57,240
you get a lot of direct explanation and the intensity of the practice really

8
00:00:57,240 --> 00:01:01,800
allows you to overcome so much of what would get in your way in the beginning.

9
00:01:01,800 --> 00:01:10,600
There's such a power to the practice that rather than being frustrated by

10
00:01:10,600 --> 00:01:20,040
the seeming endless stream of hindrance and difficulty. You feel encouraged by

11
00:01:20,040 --> 00:01:27,040
the fact that in a matter of days or weeks you're able to overcome it.

12
00:01:27,040 --> 00:01:33,000
Barring that or even when you do go to do a meditation course, it's important to

13
00:01:33,000 --> 00:01:42,040
understand that you're really in a sense answering yourself. The reason that

14
00:01:42,040 --> 00:01:49,400
you're frustrated is because in this case or in generally there's a need to

15
00:01:49,400 --> 00:01:53,920
keep the mind quiet. There's a judgment of the mind. The mind is not quiet,

16
00:01:53,920 --> 00:02:03,880
that's bad, therefore I'm frustrated and therefore there's something wrong.

17
00:02:03,880 --> 00:02:08,120
And that's really the problem, that's what we come to see in meditation. We're

18
00:02:08,120 --> 00:02:12,640
not practicing meditation to attain a specific state of being where the mind

19
00:02:12,640 --> 00:02:21,880
is always calm and peaceful. We're trying to create a sense of ease and peace,

20
00:02:21,880 --> 00:02:28,240
but lasting peace rather than peace from its peace with, where we're able to be

21
00:02:28,240 --> 00:02:34,400
at peace with ourselves, at peace with the mind, where we're no longer judging

22
00:02:34,400 --> 00:02:42,280
the state of mind. What we learn in the meditation was down to

23
00:02:42,280 --> 00:02:46,040
three things. We learn that everything inside of ourselves and in the world

24
00:02:46,040 --> 00:02:51,160
around us is changing. There's nothing which is stable, which is going to

25
00:02:51,160 --> 00:02:56,360
always be in a certain way, is always going to exist or is never going to

26
00:02:56,360 --> 00:03:02,920
arise. Everything is uncertain and unstable and impermanent. The second thing

27
00:03:02,920 --> 00:03:07,600
is that both inside of ourselves and in the world around us, everything is

28
00:03:07,600 --> 00:03:13,040
necessarily unsatisfying because it's impermanent, because nothing lasts. So

29
00:03:13,040 --> 00:03:16,320
there's no one thing that you can hold on to and say that's going to make me

30
00:03:16,320 --> 00:03:21,560
happy. There's nothing in this world that can be the object of your

31
00:03:21,560 --> 00:03:29,240
happiness. True happiness has to be unrelated to objects, unrelated to

32
00:03:29,240 --> 00:03:34,320
external phenomena, unrelated to a specific experience. Happiness has to be a

33
00:03:34,320 --> 00:03:43,080
contentment that is able to experience all things equally as they are. And the

34
00:03:43,080 --> 00:03:48,600
third thing that we realize in the meditation is that both inside of ourselves

35
00:03:48,600 --> 00:03:55,640
and in the world around us, there's nothing that belongs to us. There's no

36
00:03:55,640 --> 00:04:01,200
thing in the world that we can say is under our control that is truly ours

37
00:04:01,200 --> 00:04:06,600
because at best we're borrowing it or we're in control of it for a short

38
00:04:06,600 --> 00:04:13,400
time and we're only in control of it in a very limited sense in terms of

39
00:04:13,400 --> 00:04:22,080
giving rise to certain effects based on on the uncertain causes. We can't force

40
00:04:22,080 --> 00:04:27,720
our minds to always be quiet. When we try, we find we only create tension and

41
00:04:27,720 --> 00:04:32,480
suffering. We can't force pain to never arise. We can't force happiness to

42
00:04:32,480 --> 00:04:36,640
stay. We can't force things to always be the way we want. It's because we

43
00:04:36,640 --> 00:04:41,080
don't see these three things because we think that the world around us or

44
00:04:41,080 --> 00:04:46,000
certain things are permanent, that certain things are satisfying in

45
00:04:46,000 --> 00:04:54,520
certain things are controllable or under our control, that we give rise to

46
00:04:54,520 --> 00:04:59,080
frustration, that we're not satisfied, that we're always seeking and searching and

47
00:04:59,080 --> 00:05:05,960
wanting and needing, that we're not truly happy. So really the answer to your

48
00:05:05,960 --> 00:05:14,040
question is that through the practice, you will see that yes, you can't keep

49
00:05:14,040 --> 00:05:17,840
your mind quiet and that's a good thing and when you realize, it's not a good

50
00:05:17,840 --> 00:05:23,320
thing. It's a good thing to realize that because when you realize that and you

51
00:05:23,320 --> 00:05:27,800
realize that that's the nature of it rather than a problem, when you change, when

52
00:05:27,800 --> 00:05:32,560
your mind shifts, because right now you're in this conflict state and this is

53
00:05:32,560 --> 00:05:36,760
what happens when you meditate. It comes in conflict with the way you think

54
00:05:36,760 --> 00:05:40,240
things should be. You think that your mind should be controllable. You think

55
00:05:40,240 --> 00:05:46,520
you should be able to say stop thinking and the mind quiet's down. And now

56
00:05:46,520 --> 00:05:50,640
you're seeing that reality is not that way. You're seeing the truth and it's

57
00:05:50,640 --> 00:05:56,160
it's clear from what you say. You can't keep your mind quiet. The problem is

58
00:05:56,160 --> 00:06:01,480
you have this judgment saying that it's not quiet enough. And you know, the

59
00:06:01,480 --> 00:06:06,640
question that you have to ask is quite enough for what? Quite enough for your

60
00:06:06,640 --> 00:06:13,280
standards, for your judgment, for your need and the problem is not the noise in

61
00:06:13,280 --> 00:06:16,960
the mind. The problem is your judgment of it is the fact that we say to

62
00:06:16,960 --> 00:06:26,240
ourselves, it's not acceptable. It's not quiet enough. Once you can accept the

63
00:06:26,240 --> 00:06:31,600
fact that it's a part of nature and it's created by your way of life, the

64
00:06:31,600 --> 00:06:38,680
things you have done to make it that way, then you start to realize that it's

65
00:06:38,680 --> 00:06:45,120
not really a problem or say, or it's not your problem in the sense that you

66
00:06:45,120 --> 00:06:49,680
have to do something to fix it. It's just naturally like that. And in fact, there

67
00:06:49,680 --> 00:06:55,800
is nothing particularly wrong with an active mind. That's just the nature of it.

68
00:06:55,800 --> 00:07:01,080
The problem is that we feel the need for it to be in a different way. Once you can

69
00:07:01,080 --> 00:07:06,440
see it and see it for what it is and let it be the way it is, you won't become

70
00:07:06,440 --> 00:07:11,040
frustrated. So in a sense, you're actually practicing well and it's a sign

71
00:07:11,040 --> 00:07:16,040
that you're actually doing things properly, the fact that you're seeing, the fact

72
00:07:16,040 --> 00:07:19,240
that you're seeing that things are not the way that you thought they were. You're

73
00:07:19,240 --> 00:07:24,880
seeing that the mind is not under your control. And slowly, as you practice in

74
00:07:24,880 --> 00:07:30,720
this way, you overcome your frustration right now, you're fighting. And as you

75
00:07:30,720 --> 00:07:34,400
fight on and on and on, your mind slowly starts to give in, starts to say, okay,

76
00:07:34,400 --> 00:07:38,920
okay, okay, let it be the way it is. I can't control it. And that's just the

77
00:07:38,920 --> 00:07:43,040
nature. That's not a problem. That's the way it is. And then you stop trying to

78
00:07:43,040 --> 00:07:47,840
force things. You stop trying to control things. And that's really a key to the

79
00:07:47,840 --> 00:07:53,440
realization of enlightenment and Buddhism. So I hope that helps. And I'd like

80
00:07:53,440 --> 00:07:57,640
to encourage you because from my point of view, your practice is quite clear

81
00:07:57,640 --> 00:08:02,840
and you're on the right path. So just keep going and slowly and patiently, you'll

82
00:08:02,840 --> 00:08:08,000
be able to overcome this frustration. Try to say to yourself, frustration, sorry,

83
00:08:08,000 --> 00:08:12,640
frustrated, frustrated, or angry, angry. If you're thinking a lot, just

84
00:08:12,640 --> 00:08:16,200
say to yourself, distracted, distracted, so that you get to the point where you're

85
00:08:16,200 --> 00:08:42,200
able to accept it and move on, okay, all the best.

