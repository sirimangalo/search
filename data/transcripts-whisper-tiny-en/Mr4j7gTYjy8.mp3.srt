1
00:00:00,000 --> 00:00:05,000
Those who are new can have a hard time to understand Nirvana.

2
00:00:05,000 --> 00:00:08,000
It can take multiple lives to achieve enlightenment.

3
00:00:08,000 --> 00:00:09,000
But we live now.

4
00:00:09,000 --> 00:00:13,000
How do you explain Buddhism is different from other hypnosis,

5
00:00:13,000 --> 00:00:15,000
theorem, like religion, et cetera,

6
00:00:15,000 --> 00:00:17,000
just to give the mind some sense of purpose?

7
00:00:20,000 --> 00:00:25,000
Well, first of all, giving the mind some sense of purpose is a good thing.

8
00:00:25,000 --> 00:00:30,000
But just giving the mind some sense of purpose implies

9
00:00:30,000 --> 00:00:35,000
that it actually doesn't lead to that purpose.

10
00:00:35,000 --> 00:00:39,000
It doesn't achieve the purpose that it inclines towards.

11
00:00:39,000 --> 00:00:41,000
That's not the case in Buddhism.

12
00:00:41,000 --> 00:00:45,000
The purpose that it gives you is actually something you can achieve

13
00:00:45,000 --> 00:00:47,000
in this lifetime.

14
00:00:47,000 --> 00:00:48,000
Now, you may not.

15
00:00:48,000 --> 00:00:50,000
It may, as you say, take lifetimes.

16
00:00:50,000 --> 00:00:53,000
But that's, I think, in consequential.

17
00:00:53,000 --> 00:00:59,000
The difference is, in the sense that your question is being asked,

18
00:00:59,000 --> 00:01:02,000
those other systems, whichever they might be,

19
00:01:02,000 --> 00:01:08,000
that provide a sense of purpose, but do nothing else.

20
00:01:08,000 --> 00:01:14,000
Our fault, you know, have no development.

21
00:01:14,000 --> 00:01:19,000
What you're saying is that there are systems that don't actually lead to any result.

22
00:01:19,000 --> 00:01:24,000
Now, first of all, having a sense of purpose can be highly empowering.

23
00:01:24,000 --> 00:01:26,000
So there's that, and I assume you acknowledge that.

24
00:01:26,000 --> 00:01:28,000
But Buddhism, of course, does much more than that.

25
00:01:28,000 --> 00:01:31,000
Buddhism does lead you to Nirvana.

26
00:01:31,000 --> 00:01:34,000
I'll be it potentially quite slowly.

27
00:01:34,000 --> 00:01:40,000
For others, it can happen in this life.

28
00:01:40,000 --> 00:01:52,000
So there's really no comparison between whatism actually does lead you there.

29
00:01:52,000 --> 00:01:58,000
So the question is missing that element.

30
00:01:58,000 --> 00:02:03,000
Now, as to not being able to understand Nirvana,

31
00:02:03,000 --> 00:02:06,000
I think there's another element to this question.

32
00:02:06,000 --> 00:02:16,000
It has to do with people's inability to really strive for Nirvana.

33
00:02:16,000 --> 00:02:17,000
You're not quite asking that.

34
00:02:17,000 --> 00:02:27,000
But there is that sense of what is it that we're aiming for.

35
00:02:27,000 --> 00:02:34,000
And in that sense, usually this is a valid statement that,

36
00:02:34,000 --> 00:02:37,000
yes, indeed, the biggest problem with Buddhist practice,

37
00:02:37,000 --> 00:02:42,000
or not the biggest problem with Buddhist practice,

38
00:02:42,000 --> 00:02:48,000
is the ideation,

39
00:02:48,000 --> 00:02:52,000
the conceptualization of Nirvana as something,

40
00:02:52,000 --> 00:02:55,000
as a thing, often a very scary thing,

41
00:02:55,000 --> 00:02:59,000
something that, oh, that is, I'll be gone.

42
00:02:59,000 --> 00:03:03,000
What will happen to me if I achieve Nirvana?

43
00:03:03,000 --> 00:03:10,000
But in fact, Nirvana means freedom.

44
00:03:10,000 --> 00:03:14,000
It's a way of saying, well, it's a specific way of saying freedom

45
00:03:14,000 --> 00:03:17,000
in the sense of freeing yourself, letting go.

46
00:03:17,000 --> 00:03:20,000
So Nirvana is not being bound up.

47
00:03:20,000 --> 00:03:22,000
That's literally what it means.

48
00:03:22,000 --> 00:03:27,000
So it simply refers to the culmination of letting go,

49
00:03:27,000 --> 00:03:37,000
which for the meditator, it's quite clearly evident to be a good thing.

50
00:03:37,000 --> 00:03:41,000
As we see that the things that we're holding on to are causing a suffering,

51
00:03:41,000 --> 00:03:45,000
we see that our desires are causing us suffering.

52
00:03:45,000 --> 00:03:50,000
Our aversion is causing us suffering.

53
00:03:50,000 --> 00:03:53,000
All of these things, our delusion, our arrogance,

54
00:03:53,000 --> 00:03:56,000
our conceit, all of these things are causing us suffering.

55
00:03:56,000 --> 00:03:59,000
So we see that this kind of Nirvana in the sense of letting go,

56
00:03:59,000 --> 00:04:01,000
that's all it means.

57
00:04:01,000 --> 00:04:03,000
There's a good thing.

58
00:04:03,000 --> 00:04:06,000
Nirvana simply means that final,

59
00:04:06,000 --> 00:04:08,000
where you just say enough,

60
00:04:08,000 --> 00:04:11,000
and you let go, and the mind is,

61
00:04:11,000 --> 00:04:14,000
it's like the mind is here,

62
00:04:14,000 --> 00:04:17,000
all the experiences are around it,

63
00:04:17,000 --> 00:04:20,000
and so the mind keeps going out to different things.

64
00:04:20,000 --> 00:04:23,000
Nirvana is when the mind is enough, and it stops.

65
00:04:23,000 --> 00:04:25,000
It doesn't go anywhere.

66
00:04:25,000 --> 00:04:30,000
It doesn't run away and leave it centered.

67
00:04:30,000 --> 00:04:33,000
It becomes perfectly centered.

68
00:04:33,000 --> 00:04:37,000
In a sense, there's a cessation,

69
00:04:37,000 --> 00:04:40,000
so there's no thinking, there's no awareness,

70
00:04:40,000 --> 00:04:44,000
in that sense, but it shouldn't be scary.

71
00:04:44,000 --> 00:04:47,000
It's just a no more.

72
00:04:47,000 --> 00:04:49,000
Enough.

73
00:04:49,000 --> 00:04:51,000
That's all.

