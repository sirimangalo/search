1
00:00:00,000 --> 00:00:12,560
So, it is a night where we will be going over some fundamental limitation practice, giving

2
00:00:12,560 --> 00:00:26,560
basic instruction and meditation practice by request and that is all get in a meditative

3
00:00:26,560 --> 00:00:33,080
mode.

4
00:00:33,080 --> 00:00:43,480
So, we start talking about meditation, it is clearly not a theoretical subject.

5
00:00:43,480 --> 00:00:48,160
This is not a talk that you should listen to intellectually.

6
00:00:48,160 --> 00:00:59,320
It is a teaching that you should put into practice.

7
00:00:59,320 --> 00:01:15,640
The practice of meditation really follows this sequence of trainings as laid down by

8
00:01:15,640 --> 00:01:18,080
the Buddha.

9
00:01:18,080 --> 00:01:26,440
So, the Buddha said there are three trainings that we should train ourselves in.

10
00:01:26,440 --> 00:01:39,720
And, Pauli, we call this sea lassamadi and panya, in English we might say morality,

11
00:01:39,720 --> 00:01:43,400
concentration and wisdom.

12
00:01:43,400 --> 00:01:55,920
But, these are misleading or it is difficult to understand these without explaining these

13
00:01:55,920 --> 00:02:05,160
terms in detail because misunderstanding arises based on the words themselves.

14
00:02:05,160 --> 00:02:14,280
For example, so with morality we think of certain deeds that we should perform and concentration

15
00:02:14,280 --> 00:02:25,720
we think of focusing the mind on a single object or a single point and keeping it in one

16
00:02:25,720 --> 00:02:44,520
place and wisdom we think of as intellectual thought or ideas, concepts, theories and so on.

17
00:02:44,520 --> 00:02:54,600
These are mundane morality, mundane concentration and mundane wisdom, in meditation it is

18
00:02:54,600 --> 00:03:05,440
on a more ultimate or a more experiential level so it has to do with our experience, morality

19
00:03:05,440 --> 00:03:12,520
plays a part in our existence right here and now even when we don't have an opportunity

20
00:03:12,520 --> 00:03:30,840
to do or say bad things, true morality is the mind that avoids or refrains from what we

21
00:03:30,840 --> 00:03:42,920
might call evil or unwholesumness.

22
00:03:42,920 --> 00:03:51,880
We might, let's put it phrase in terms of reality and say morality is the refraining from

23
00:03:51,880 --> 00:04:01,520
or the limiting one's attention to ultimate reality, to experience so when the mind

24
00:04:01,520 --> 00:04:08,520
wanders off into concepts we bring it back to experience, to reality, to what's really

25
00:04:08,520 --> 00:04:14,920
being experienced right so when we're thinking it's all just thinking but we instead of

26
00:04:14,920 --> 00:04:20,800
seeing it as thinking we see it as this reality or that as concepts as people in places

27
00:04:20,800 --> 00:04:28,080
and things that we're thinking about we don't realize that we're thinking.

28
00:04:28,080 --> 00:04:33,080
Concentration is the focusing in the Buddha's teaching the ultimate or the highest concentration

29
00:04:33,080 --> 00:04:38,960
is the focusing on ultimate reality, to not focusing on one single thing but focusing

30
00:04:38,960 --> 00:04:47,560
on your own experience having a focused mind, the mind that is in focus so seeing things

31
00:04:47,560 --> 00:04:53,840
as they are not how we want them to be or how we don't want them to be, just seeing them

32
00:04:53,840 --> 00:05:06,560
as they are.

33
00:05:06,560 --> 00:05:13,440
And wisdom is rather than any kind of theory about reality or the universe or the past

34
00:05:13,440 --> 00:05:21,040
or the future or our life or life in general it's understanding of experience, understanding

35
00:05:21,040 --> 00:05:27,600
of reality which is really quite simple actually it means seeing reality, knowing reality

36
00:05:27,600 --> 00:05:36,480
for what it is and something that we already think we have but we don't really understand

37
00:05:36,480 --> 00:05:39,960
reality and understand our own experiences.

38
00:05:39,960 --> 00:05:46,320
If we did why would our experiences cause trouble for us?

39
00:05:46,320 --> 00:05:50,520
Why would we do and say and think things that were to our detriment?

40
00:05:50,520 --> 00:06:01,280
We wouldn't if we really understood what we were doing this is the idea behind wisdom,

41
00:06:01,280 --> 00:06:11,840
behind the training or training ourselves to go into or to enter into every experience

42
00:06:11,840 --> 00:06:17,040
with wisdom that prevents us from making bad choices.

43
00:06:17,040 --> 00:06:20,520
So this is the base of the teaching.

44
00:06:20,520 --> 00:06:29,480
First we have to bring the mind back or keep the mind from leaving the present moment

45
00:06:29,480 --> 00:06:35,760
or leaving reality in front of us and we have to focus on the reality to see it as it

46
00:06:35,760 --> 00:06:41,520
is and then we have to see and to know it and to understand it as it is.

47
00:06:41,520 --> 00:06:46,720
This is the progression of our practice.

48
00:06:46,720 --> 00:06:51,040
The actual practice, the technique of the practice is what I was asked to talk about today

49
00:06:51,040 --> 00:06:58,200
is based on the four foundations of mindfulness which is it's actually a subjective teaching

50
00:06:58,200 --> 00:07:04,240
of the Buddha, there's nothing in reality that you can point to and say these four foundations

51
00:07:04,240 --> 00:07:05,240
of mindfulness.

52
00:07:05,240 --> 00:07:10,240
It's actually kind of a strange sounding title.

53
00:07:10,240 --> 00:07:18,160
It means the four categories of experience that you can establish mindfulness on.

54
00:07:18,160 --> 00:07:23,480
It's important to have categories or it's important to be able to name out what is ultimate

55
00:07:23,480 --> 00:07:30,720
realities so that you know what is within limits and what is outside of limits for

56
00:07:30,720 --> 00:07:34,160
beginning meditative is important.

57
00:07:34,160 --> 00:07:36,240
So we take these four to be limits.

58
00:07:36,240 --> 00:07:43,680
Anything outside of these is not an object of meditation, it's not something we should focus

59
00:07:43,680 --> 00:07:45,640
on.

60
00:07:45,640 --> 00:07:53,320
And by separating them into four, it's easy to tell what's what, or it's easier to recognize

61
00:07:53,320 --> 00:08:01,200
things as they are rather than just lumping them all together into one category of experience.

62
00:08:01,200 --> 00:08:03,040
So we break experience up like this.

63
00:08:03,040 --> 00:08:09,360
It's quite useful especially for a beginner meditator, you give them four things and they

64
00:08:09,360 --> 00:08:11,560
have something to work with.

65
00:08:11,560 --> 00:08:17,440
So these four are actually the most important thing for a beginner meditator to learn.

66
00:08:17,440 --> 00:08:20,440
So we often have people even memorize them.

67
00:08:20,440 --> 00:08:24,680
I'll go through them, so it should be fairly easy to remember.

68
00:08:24,680 --> 00:08:34,680
These are the body, the feelings, the mind and the dumbness or the reality and it's a little

69
00:08:34,680 --> 00:08:39,480
bit of a, have to explain the force one a little bit.

70
00:08:39,480 --> 00:08:42,360
But the first three are fairly self-explanatory.

71
00:08:42,360 --> 00:08:48,520
The first one is the body and it doesn't, or not exactly the body, it means body or bodily

72
00:08:48,520 --> 00:08:50,960
experiences.

73
00:08:50,960 --> 00:08:53,960
Because the body is still just a concept.

74
00:08:53,960 --> 00:09:03,560
But in regards to the body or bodily experiences, in regards to body, there are bodily experiences.

75
00:09:03,560 --> 00:09:07,320
There is the experience of sitting, for example, what is the experience of sitting while

76
00:09:07,320 --> 00:09:15,080
there's pressure in the back and there's a softness on the seat that we're sitting on,

77
00:09:15,080 --> 00:09:20,400
there's attention in the legs, and there are various aspects to the sitting, there's even

78
00:09:20,400 --> 00:09:28,040
a heat in the body that comes from the contact with the seat and is generated from the

79
00:09:28,040 --> 00:09:32,040
body itself, or there's cold from the air around us.

80
00:09:32,040 --> 00:09:37,840
There's a physical experience and we sum that all up by sitting sitting, but actually

81
00:09:37,840 --> 00:09:43,000
sitting doesn't really exist, it's just a name for a certain experience.

82
00:09:43,000 --> 00:09:52,160
And they're standing, they're walking, they're lying down, these are the basic aspects

83
00:09:52,160 --> 00:10:00,280
of physical experience, bending, stretching, turning, reaching, grasping, eating, chewing,

84
00:10:00,280 --> 00:10:06,480
swallowing, brushing your teeth, washing your hair, using the toilet.

85
00:10:06,480 --> 00:10:09,000
All of this is physical experience.

86
00:10:09,000 --> 00:10:15,080
You can even meditate on the toilet, the Buddha himself said it.

87
00:10:15,080 --> 00:10:24,240
If you're clearly aware of the physical experience, this is body.

88
00:10:24,240 --> 00:10:26,880
One of the things that we focus on is the stomach.

89
00:10:26,880 --> 00:10:32,320
It's a good example because it's a part of the body that's moving or that's a bodily

90
00:10:32,320 --> 00:10:40,760
experience that is always present, or most of the time is clearly perceptible.

91
00:10:40,760 --> 00:10:47,560
When the stomach rises, we just recognize it as rising, and when it falls, as falling,

92
00:10:47,560 --> 00:10:54,760
this is a basic meditation exercise, using the body.

93
00:10:54,760 --> 00:11:01,000
The second one, by feelings, we don't mean mental judgements or so, and we mean simple

94
00:11:01,000 --> 00:11:09,200
sensations, painful sensations, pleasant sensations and neutral sensations.

95
00:11:09,200 --> 00:11:12,640
So it can be pain in the body, this is an obvious one.

96
00:11:12,640 --> 00:11:19,720
If you recognize the pain as pain, you remind yourself it's pain, this is mindfulness of

97
00:11:19,720 --> 00:11:23,040
the feelings.

98
00:11:23,040 --> 00:11:28,280
If you feel happy and you say, you remind yourself if it does happy and keep your mind

99
00:11:28,280 --> 00:11:36,680
focused on it as it's there, or pleased, or pleasure, or even just feeling, or if you

100
00:11:36,680 --> 00:11:50,680
feel calm and you remind yourself calm, calm, this is mindfulness of the feelings.

101
00:11:50,680 --> 00:11:55,120
When you're thinking about the past or the future, and you have thoughts, this is the

102
00:11:55,120 --> 00:12:12,680
mind when you're thinking, dreaming, or planning, or fantasizing, or reminiscing, or

103
00:12:12,680 --> 00:12:18,520
any kind of discursive thought, any kind of thought at all, the meditation isn't trying

104
00:12:18,520 --> 00:12:22,320
to get rid of the thought, it's not trying to get rid of anything.

105
00:12:22,320 --> 00:12:26,320
This is, as I said before, this is kind of like a study.

106
00:12:26,320 --> 00:12:31,560
Remember what we're trying to gain here, we're trying to gain first morality.

107
00:12:31,560 --> 00:12:38,880
So when thinking arises, well, that's still reality, but when you get lost in it, when

108
00:12:38,880 --> 00:12:45,240
you start to perceive it as being an issue, or a problem, or a story, or a fantasy, then

109
00:12:45,240 --> 00:12:46,680
you're outside of reality.

110
00:12:46,680 --> 00:12:51,640
So we have to catch the thoughts and see them just as thinking, we remind ourselves

111
00:12:51,640 --> 00:12:56,440
thinking, thinking, and try to train the mind to only see and as thinking, this is bringing

112
00:12:56,440 --> 00:13:01,800
the mind back to reality, this is morality, bringing the mind back to reality, and then

113
00:13:01,800 --> 00:13:07,320
once you see the reality, see the thoughts arising and see, seeing this is your focus,

114
00:13:07,320 --> 00:13:13,440
your focus on them as they arise and see, and the wisdom is seeing that thoughts arise

115
00:13:13,440 --> 00:13:17,440
and see some coming to understand that thoughts arise and see if they're not me, they're

116
00:13:17,440 --> 00:13:21,680
not mind, they're not under my control, they're not worth pursuing, they're not worth

117
00:13:21,680 --> 00:13:28,560
clinging to, they're just reality, they come in the going.

118
00:13:28,560 --> 00:13:39,640
It's this realization of the pure state of awareness in the present moment, not judgmental

119
00:13:39,640 --> 00:13:54,000
and not distracted or lost or confused or muddled by thoughts and by concepts.

120
00:13:54,000 --> 00:13:58,480
So these three are pretty self-explanatory, you can go back and forth with them.

121
00:13:58,480 --> 00:14:04,520
The fourth one isn't mysterious or anything, it just means it's kind of like et cetera or

122
00:14:04,520 --> 00:14:19,600
extras or sets of the Buddha's teaching you might even say that progress one further

123
00:14:19,600 --> 00:14:22,160
towards enlightenment.

124
00:14:22,160 --> 00:14:31,320
So the first set is the five hindrances, they're emotive states that will arise and get

125
00:14:31,320 --> 00:14:38,200
in your way and keep you from seeing clearly, so they're kind of extra from the other

126
00:14:38,200 --> 00:14:39,200
three.

127
00:14:39,200 --> 00:14:44,400
So it means in the mind there will be liking and disliking, you'll be focusing on the

128
00:14:44,400 --> 00:14:48,560
body, watching the stomach rise and fall or the feelings or so.

129
00:14:48,560 --> 00:14:51,840
And then there will be a liking or a disliking, maybe you'll feel bored or maybe you'll

130
00:14:51,840 --> 00:15:00,160
feel scared or maybe you'll feel frustrated or even a great anger can come up if it's

131
00:15:00,160 --> 00:15:02,480
stuck inside.

132
00:15:02,480 --> 00:15:05,520
On the other hand, you might be liking, you might like the meditation or there might

133
00:15:05,520 --> 00:15:13,080
be wanting, wanting food or wanting pleasure, wanting this or that or liking some experience

134
00:15:13,080 --> 00:15:18,320
that arises, maybe some thoughts or fantasies arise and you like them or some pleasant

135
00:15:18,320 --> 00:15:23,760
feelings arise in the body and you like them or you hear something and you like that.

136
00:15:23,760 --> 00:15:32,640
Now this liking and disliking are things that take us out of the present moment, take us

137
00:15:32,640 --> 00:15:39,240
out of reality, you're no longer studying, you're no judging and you're cultivating habits

138
00:15:39,240 --> 00:15:40,240
of judgment.

139
00:15:40,240 --> 00:15:48,000
This liking doesn't lead just to getting, it leads to more liking, it's habitual, disliking

140
00:15:48,000 --> 00:15:53,640
is the same, it leads to anger, it's liking, you have more you give rise and give

141
00:15:53,640 --> 00:16:04,760
rise, when more you cling to the disliking, the more disliking there will be, the more angry

142
00:16:04,760 --> 00:16:06,520
a person you become.

143
00:16:06,520 --> 00:16:14,040
So these are hindrances, they stop us from seeing things clearly, they cloud the mind,

144
00:16:14,040 --> 00:16:22,840
they lead the mind out of reality and into a muddled state, they confuse the mind, they

145
00:16:22,840 --> 00:16:31,080
tire the mind, they tax the mind's strength and they prevent one, the most important is

146
00:16:31,080 --> 00:16:35,360
they prevent you from seeing clearly, so again we're not judging them, but we're learning

147
00:16:35,360 --> 00:16:40,120
about them, we're studying, we're learning how they come and how they go, what makes

148
00:16:40,120 --> 00:16:44,080
them arise, what makes them cease, we're coming to understand everything about them,

149
00:16:44,080 --> 00:16:45,480
that's all we need.

150
00:16:45,480 --> 00:16:53,280
As I said, the theory is that once you and the observation, the observation is that once

151
00:16:53,280 --> 00:17:00,800
you understand something, once you understand something to be to your detriment, you abandon

152
00:17:00,800 --> 00:17:11,960
it without thought, without even having to consider, without effort, simply through understanding

153
00:17:11,960 --> 00:17:22,280
the mind, when understanding arises in the mind, understanding of the problems with negativity

154
00:17:22,280 --> 00:17:32,320
or with clinging in any case, then there will arise no clinging, the mind will change,

155
00:17:32,320 --> 00:17:37,560
the mind changes based on the habit.

156
00:17:37,560 --> 00:17:42,880
The liking, disliking in other words, drowsiness or laziness, so this is a hindrance because

157
00:17:42,880 --> 00:17:51,240
it keeps you from seeing things, keeps you from jumping, from going out to the object,

158
00:17:51,240 --> 00:17:59,520
from perceiving the object clearly, distraction when the mind is not focused, so laziness

159
00:17:59,520 --> 00:18:04,920
or this data feeling laziness too much focus or too much concentration, distraction

160
00:18:04,920 --> 00:18:09,880
is when you don't have enough concentration, we have to balance, let's find the balance

161
00:18:09,880 --> 00:18:17,360
where we're not too focused, laziness actually isn't a lack of energy, it's a excess

162
00:18:17,360 --> 00:18:22,160
of concentration, so you can actually pull yourself back and you can cultivate the energy

163
00:18:22,160 --> 00:18:31,880
that's missing by seeing it, by noting to yourself drowsy and drowsy, distraction, noting

164
00:18:31,880 --> 00:18:36,680
to yourself distracted and distracted or worried if you're worried and your mind is not

165
00:18:36,680 --> 00:18:45,760
able to stay firm, is a worried worried, if you have doubt or confusion, say to yourself

166
00:18:45,760 --> 00:18:52,080
doubting, doubting, the whole, the underlying technique here is just this noting, this

167
00:18:52,080 --> 00:18:57,920
reminding yourself, the best we don't understand is reminding yourself because we're trying

168
00:18:57,920 --> 00:19:04,040
to use something that's broken to fix itself, our mind is, in a sense, broken, by broken

169
00:19:04,040 --> 00:19:10,040
means we do things that cause our suffering, we give rise to unwholesome thoughts, thoughts

170
00:19:10,040 --> 00:19:16,160
that are not to our benefit or to our detriment, if you're not such a person then we're

171
00:19:16,160 --> 00:19:19,760
considered even lightened and you don't have to practice, but this is the purpose of

172
00:19:19,760 --> 00:19:27,160
meditating to change or to remove or to understand at least what it is that we're doing

173
00:19:27,160 --> 00:19:36,600
to ourselves, but to do this we have to use the mind, the mind which is subjective,

174
00:19:36,600 --> 00:19:40,880
which is judgmental, which is partial, so how do we break out of that?

175
00:19:40,880 --> 00:19:47,520
We need some means of coming outside of our own habits, our own mode of behavior and

176
00:19:47,520 --> 00:19:54,120
this is with the reminding us, we have this objective tool that never changes, you can't

177
00:19:54,120 --> 00:19:58,760
trick yourself into using some other word and when you feel pain saying happy, happy

178
00:19:58,760 --> 00:20:03,560
or something like that, if you understand the technique, you have to say to yourself pain,

179
00:20:03,560 --> 00:20:09,920
you have to remind yourself and you have to confirm in yourself a firm pain, the firm

180
00:20:09,920 --> 00:20:15,440
the reality of it, it's similar to this positive affirmation stuff, it's totally different

181
00:20:15,440 --> 00:20:22,560
but it's a similar concept and it's kind of counterintuitive, we think that what you should

182
00:20:22,560 --> 00:20:28,920
be saying is happy, happy or no pain, no pain, when you feel angry you should be saying

183
00:20:28,920 --> 00:20:37,440
love, love, to try to counter it, this is if you were trying to create some state, some

184
00:20:37,440 --> 00:20:44,400
opposite state, we're not trying to do that, we're trying to understand or to focus on

185
00:20:44,400 --> 00:20:53,160
the bad stuff and it seems counterintuitive, why would you focus on the bad stuff, why would

186
00:20:53,160 --> 00:21:04,880
a doctor focus on the sickness, same sort of thing, we have a problem, our mind is not

187
00:21:04,880 --> 00:21:09,960
tweaked correctly, it needs to be adjusted, so we have to look and see where it needs

188
00:21:09,960 --> 00:21:17,000
to be adjusted, but we have to do it objectively and that's what this word does, so with

189
00:21:17,000 --> 00:21:22,640
the body we can just focus on the sitting and say to ourselves, sitting or we can focus

190
00:21:22,640 --> 00:21:28,240
on the stomach, the basic exercise we give to meditators who come for an intensive course

191
00:21:28,240 --> 00:21:36,120
to watch the stomach rise, sing and falling and just remind themselves, rise, sing, fall,

192
00:21:36,120 --> 00:21:42,360
and of course you don't say these words out loud, you're just in your mind reminding yourself,

193
00:21:42,360 --> 00:21:52,200
so as to cut off or preclude any judgment, any thoughts of liking or disliking the experience

194
00:21:52,200 --> 00:22:02,600
or identifying with the experience or any discursive or extraneous thought, so when you

195
00:22:02,600 --> 00:22:07,680
feel pain and you say pain, pain, this is learning about not just the pain but also

196
00:22:07,680 --> 00:22:14,360
learning about our reactions to it, and once you see that your reactions are the problem

197
00:22:14,360 --> 00:22:19,080
and it's actually you're disliking the pain that's the problem, you come to see that pain

198
00:22:19,080 --> 00:22:23,400
is really just pain and as you say to yourself, pain, pain, you're teaching yourself

199
00:22:23,400 --> 00:22:29,600
something, whereas without looking you would be upset about the pain, once you look at

200
00:22:29,600 --> 00:22:35,200
it and understand it as pain, you realize for yourself it is just pain, there's nothing

201
00:22:35,200 --> 00:22:40,480
good or bad about it, and the same with happy feelings as well instead of clinging to

202
00:22:40,480 --> 00:22:46,720
them and wishing for them and hoping for them and feeling depressed and longing for them

203
00:22:46,720 --> 00:22:52,400
when they're gone, we see them just as happy feelings and we realize there's no benefit

204
00:22:52,400 --> 00:22:57,400
that comes from clinging to them, they might be good but as soon as you say that they're

205
00:22:57,400 --> 00:23:01,280
good you start clinging and when you cling then you suffer when they're gone, you have

206
00:23:01,280 --> 00:23:07,560
this partiality where your happiness depends on them, so we stop that, we know them just

207
00:23:07,560 --> 00:23:12,840
as happiness and you're thinking and then you say to yourself thinking and thinking and

208
00:23:12,840 --> 00:23:17,000
thoughts as well have no power over you, good thoughts, bad thoughts, good thoughts don't

209
00:23:17,000 --> 00:23:26,240
take you away bad thoughts, don't upset you, and even with the hindrance is once we say

210
00:23:26,240 --> 00:23:33,320
to ourselves liking, liking or disliking, disliking, disliking, disliking, this is the means

211
00:23:33,320 --> 00:23:38,160
of understanding these things, sometimes it feels like they get worse when you acknowledge

212
00:23:38,160 --> 00:23:42,320
them, you say to yourself angry, angry, sometimes it feels like you get more angry, but

213
00:23:42,320 --> 00:23:48,320
this is how it should be in the beginning because we have anger inside of us and our reaction

214
00:23:48,320 --> 00:23:54,200
to anger is to get angry, we have to see this, we have to see what we're doing to ourselves,

215
00:23:54,200 --> 00:23:58,840
it's actually the best thing for us once we watch the anger and we come upset by it,

216
00:23:58,840 --> 00:24:03,960
once we realize how horrific this anger is and how it can build and so on, this is exactly

217
00:24:03,960 --> 00:24:08,400
what we need to see, it should build, it should get out of hand so that we can see how

218
00:24:08,400 --> 00:24:14,240
out of hand gets and really want to change, deep down the mind will, this will affect

219
00:24:14,240 --> 00:24:20,040
the mind when it sees how strong this anger is, the problem with anger is that it comes

220
00:24:20,040 --> 00:24:28,080
up, the problem is that we don't understand it when it arises and so we follow it, we

221
00:24:28,080 --> 00:24:35,040
make use of it, we use it as a premise by which to act, I'm angry therefore I should hit

222
00:24:35,040 --> 00:24:39,880
you or therefore I should yell at you, there's before I should do something nasty for

223
00:24:39,880 --> 00:24:46,720
you towards you, we don't ever understand the anger, let it come, this is important, it's

224
00:24:46,720 --> 00:24:51,440
important to teach your mind the lesson, it's going to get angry, well let it see what

225
00:24:51,440 --> 00:25:02,280
anger is, look at the anger, let it eat its own dog food, so they say, so these are the

226
00:25:02,280 --> 00:25:08,520
four foundations, in briefs this is all you need to, no depractist meditation, it's really

227
00:25:08,520 --> 00:25:12,880
almost enough, there's only one other thing that I'd add and that's the next set in

228
00:25:12,880 --> 00:25:19,880
the dumbness, is the senses, so other things that might come up are seeing, hearing,

229
00:25:19,880 --> 00:25:25,120
smelling, tasting, feeling and thinking, thinking we already have but the other five senses

230
00:25:25,120 --> 00:25:35,200
we have to note as well, seeing and hearing, all of these senses can give rise to partiality

231
00:25:35,200 --> 00:25:40,840
or conceptual thought, we see something we begin to think about it, when we hear something

232
00:25:40,840 --> 00:25:47,160
we begin to think about it, so we get lost from the meditation, we lose our morality in

233
00:25:47,160 --> 00:25:53,280
a sense, because we lose our morality, we have no concentration, no focus, we have no focus,

234
00:25:53,280 --> 00:25:57,960
we lose our wisdom, we're in the realm of delusion, where it's very easy to give rise to

235
00:25:57,960 --> 00:26:04,080
judgment and partiality, so we bring ourselves back, when we see, we catch it just at the

236
00:26:04,080 --> 00:26:11,360
door and say to ourselves, seeing, seeing, when we hear something, hearing, hearing, when

237
00:26:11,360 --> 00:26:17,080
we smell or taste or feel, is it smelling, smelling, tasting, feeling, feeling and when

238
00:26:17,080 --> 00:26:23,600
we think, of course we're not thinking, and in this way we've kind of covered all of

239
00:26:23,600 --> 00:26:29,600
our basements, there's nothing that could arise as long as we stay, the Buddha was emphatic

240
00:26:29,600 --> 00:26:36,200
about this, in fact, he said, if you stay within this boundary of the four foundations

241
00:26:36,200 --> 00:26:42,240
of mindfulness, nothing can get you, tomorrow I can't get you, evil cannot reach you,

242
00:26:42,240 --> 00:26:49,280
no trouble can come to you, it's a very powerful statement and it seems hard to believe,

243
00:26:49,280 --> 00:26:54,520
until you understand what is reality and you realize that all of your problems are outside

244
00:26:54,520 --> 00:27:00,760
of this, that there's no problems in any of these things, there's no issues in any of these

245
00:27:00,760 --> 00:27:09,080
things, the problem is in identifying with the making more of them than they are projecting,

246
00:27:09,080 --> 00:27:18,760
extrapolating upon them, once they are what they are and that's all they are, there

247
00:27:18,760 --> 00:27:26,440
is no potential for suffering, there's no potential for clinging, there's no potential

248
00:27:26,440 --> 00:27:35,440
for expectation, so this is all the practice for these, it's a training and a studying

249
00:27:35,440 --> 00:27:41,440
and a learning process, you want to be learning as much as you can, it's not something

250
00:27:41,440 --> 00:27:45,840
you should be, meditation isn't something you should be using to escape, it should be

251
00:27:45,840 --> 00:27:52,480
a laboratory where you study the things, you don't dare study anywhere else, people who

252
00:27:52,480 --> 00:27:59,000
have anxiety or phobias or depression or anger, they don't dare to look at these things,

253
00:27:59,000 --> 00:28:05,200
anything to get away from them, how can I cure them, this is why people start to take pills,

254
00:28:05,200 --> 00:28:08,960
take medication, it's apparently a very common thing and guess we don't realize how

255
00:28:08,960 --> 00:28:15,960
common it is, but probably a lot of the people we know are on some sort of medication

256
00:28:15,960 --> 00:28:22,920
for one or another mental illness, we often think maybe it's only me but I'm not taking

257
00:28:22,920 --> 00:28:33,800
them but obviously, but it's quite a common thing and we do this trying to get away

258
00:28:33,800 --> 00:28:41,880
from them, so meditation is a way to stop that, it's a controlled means of approaching

259
00:28:41,880 --> 00:28:49,560
these issues, these problems, you can see we are sitting on a mat, we can let them

260
00:28:49,560 --> 00:29:00,160
arise without much to fear, if we have anger issues then we can let anger arise because

261
00:29:00,160 --> 00:29:07,320
there's no one around to get angry at, if we have fear or worry and now we can be afraid

262
00:29:07,320 --> 00:29:13,520
and worried because it's really inconsequential, we can look at the worry, we don't have

263
00:29:13,520 --> 00:29:19,440
to act out on it and we don't have to take some kind of medication to get over it, it's

264
00:29:19,440 --> 00:29:24,560
like we want these things to come up that we've never allowed ourselves to look at, so

265
00:29:24,560 --> 00:29:33,560
it's quite an exploration, it's something very unique and not very well understood, we usually

266
00:29:33,560 --> 00:29:43,920
think a meditation is some kind of retreat or an escape, certainly isn't, it's an advance,

267
00:29:43,920 --> 00:29:52,480
we are taking a leap into the unknown where we've never really gone before and that

268
00:29:52,480 --> 00:29:58,720
is this unknown isn't something mysterious, it's who we are, it's our everyday life that

269
00:29:58,720 --> 00:30:07,080
we never really look at, it's like tuning the machine or our automobile going under the

270
00:30:07,080 --> 00:30:15,800
hood, we drive the car every day, we often don't spend enough time tuning it or cleaning

271
00:30:15,800 --> 00:30:22,680
it, if you go around in your car and you never change the oil, eventually it burns, burns

272
00:30:22,680 --> 00:30:31,480
up, you know, an engine trouble, it's exactly how the mind is, we don't spend the time,

273
00:30:31,480 --> 00:30:36,600
we spend all their time using our mind, we use our mind for so many things but we don't

274
00:30:36,600 --> 00:30:43,320
spend enough time servicing the mind, so this is kind of this servicing, removing all

275
00:30:43,320 --> 00:30:50,160
the bad stuff and cultivating all the good stuff, so that's basically how to meditate, this

276
00:30:50,160 --> 00:30:54,800
is sitting meditation, there's also, you can do it walking, so when you walk, I've given

277
00:30:54,800 --> 00:30:59,520
instructions in my booklet, you can read that, when you walk, you know the right foot is

278
00:30:59,520 --> 00:31:06,960
moving, step being right, it's just a technique, the, the, the, or it's a structure,

279
00:31:06,960 --> 00:31:12,400
the technique is the same, the note thing is reminding yourself is the same, if you use this

280
00:31:12,400 --> 00:31:16,080
technique of reminding yourself, you can even use it in daily life when you're sitting

281
00:31:16,080 --> 00:31:21,880
in a car, when you're sitting on the bus, so when you're in at work and you want to take

282
00:31:21,880 --> 00:31:26,000
a break, you can do a five minute meditation break, you can, when you're eating, you can

283
00:31:26,000 --> 00:31:33,200
do eating meditation, chewing, chewing, swallowing, tasting, tasting, anything, it become

284
00:31:33,200 --> 00:31:40,320
meditation, there's, the, the formal aspect of it isn't so important, it's just a sort

285
00:31:40,320 --> 00:31:49,640
of, in the sense of retreat, from, from the whole spectrum of experience, to, to keep, keeping

286
00:31:49,640 --> 00:31:54,440
it to just a couple of objects, so walking back and forth is quite simple, gives you

287
00:31:54,440 --> 00:32:02,800
a much, much more controlled environment in which to study the experience, otherwise the

288
00:32:02,800 --> 00:32:08,360
technique is simply this, reminding yourself and based on these four foundations of mindfulness

289
00:32:08,360 --> 00:32:18,200
that Buddha taught is for, for aspects of existence, aspects of experience, okay, so let's

290
00:32:18,200 --> 00:32:23,920
take that as a basic understanding of meditation and we can try to, you know, do our, we'll

291
00:32:23,920 --> 00:32:43,040
do our daily meditation and try to put this into practice.

