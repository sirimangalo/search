Okay, so here we are at the, okay, so here we are today at the reclining Buddha, you can
pull in the rua, and we've come all this way to pay respect to our teacher, our leader,
and we might even say our Lord and Master, the one who guides us, and guides our way and
who we look up to and who we dedicate our lives to.
And this is quite a significant step that we've taken, it's quite a significant thing
that we've done to do this, to take the Buddha as our guide, even, or more as our teacher,
as our Lord and Master, or the one who we look to for all of our guidance and all of our answers
and so on.
It's the sort of thing that you hear about and other religious traditions as well.
And for many people it's quite a turn-off to hear this sort of thing promoted, they'll even
think that to do something like this is to follow someone out of blind faith and to rely
on someone else for your salvation, to rely on someone else for your future.
And people will think rather than one should look to oneself, and indeed the Buddha was
clear that we should look to ourselves for salvation.
So the reason that the answer to as to why we take the Buddha as our refuge as our guide,
as our Master, as our teacher even, has to be explained in some detail.
And the Buddha himself was quite frank and open about this question, and quite specific in
his answers to this question, because he did say, at the he at the no-not-till, you should
be your own refuge.
We should be our own salvation, so we shouldn't look to a Buddha image to save us.
The question is, then why then do we make such impressive, magnificent, stone carvings
of the Buddha? Why do we even bother? Or why do we bother coming all this way to pay
respect to them? What is the significance that they have for us? What is the significance
that the Buddha has for us, before to find the answers for ourselves?
And there's a story in the Buddha's time of a Brahmin and Janusoni, who had heard many
people saying that they took the Buddha as their refuge, and that they held the Buddha
in such high esteem that they didn't even feel confident themselves proclaiming his virtues.
And so the Brahmin Janusoni, he came and he told us to the Buddha, he said, well, these
people are saying things like, you are incomparable and so on, that they have this great
respect for you, they see kings bowing down to you, paying respect to you, they see you
being respected by laypeople, by people of all caste, they see people coming in ordaining
in the multitudes under you. And so they therefore think that you are, they therefore
believe that you are enlightened, that you are the Buddha, the perfectly enlightened one
worthy of following one who knows all and sees all and has full understanding and wisdom,
complete and perfect enlightenment. And the Buddha said, well, this isn't really how you
come to know that the Buddha is fully enlightened, this isn't really how you come to know
the truth of the Buddha. And this isn't a good enough reason to take the Buddha as your
guide and as your teacher or even to dedicate your life, for certain not to dedicate your
life to someone, just because you've heard other people esteem him or you see other people
respecting him and bowing down to him and so on. He said, he said, it's as though he
gives the example of an elephant. If you're walking in the forest and you see an elephant's
footprint and it's a big elephant, it's a big footprint. He said, well, an ordinary person
might think, well, that's a bull elephant, that's a male, the head of the herd elephant.
And he said, but a skilled hunter wouldn't think that, a skilled elephant hunter wouldn't
jump to that conclusion because he would think that, well, there are even female elephants
who are big and have big feet and so on. And so this might be one of them, this might
be any sort of elephant. And if you see the leaves picked off the trees or the branches
broken high up, or if you see this or that sign, he said, none of these would lead a skilled
elephant hunter to think that this would jump to the conclusion and to be certain that
this was a bull elephant. So only when they see the bull elephant and then see its tusks
and they see its tail and its ears and all the parts of the body that they would come
to understand that this is indeed a bull elephant. And so he said in the same way whether
this, whether people pay respect to him or people bow down to him or people say this
or that about him or he is skilled in teaching or so on, one shouldn't for that reason
come to think that this is a perfectly enlightened one. The reason why we take the Buddha
as our refuge and why we have such high esteem for him is because of the dhamma, because
of our own practice and our realization of the dhamma, even basic realization that
what the Buddha said is true. Last night when we were meditating in Anuradapura and during
the night a group of people came walking by and they said, oh they saw there was a monk
sending a meditation and so one of them came up and kind of interrupted me and my meditation
said, hello, where are you from? I said I am from Canada and he said, so what do you think
of Buddhism? I said, well I am a monk, it is kind of obvious but I think of Buddhism and
I said, what do you think of Buddhism? He said, Buddhism is the way, it is the best way
to freedom and I said, oh so you practice meditation? No, I don't practice and I said then
how do you know that Buddhism is the perfect way to freedom? I think he was quite embarrassed
actually because really the truth is how could you know that it is the perfect way to
freedom when he doesn't ever practice or has never practiced the Buddha's teaching.
This is dangerous, it is dangerous for us to jump to conclusions and it is not something
that protects the Buddha's teaching at all, it is something that you can protect Buddhism
by just having faith, you can protect anything by having faith and if you have faith in the Buddha
and you believe till your face turns blue, that the Buddha is enlightened, it doesn't help
you and it doesn't help the Buddha, it doesn't help Buddhism, it just makes it look like something
that we follow out of blind faith and so the Buddha gave a long explanation of how it is that
someone comes to realize that for themselves and have perfect faith that the Buddha is
indeed a fully enlightened being. So he said here and enlightened wonder rises in the world,
and then someone from a good home or someone from a poor home, someone from this class or that
class goes and listens to the Buddha's teaching and then they hear the Buddha's teaching and
they become confident and they become interested in the Buddha's teaching and the more they
listen the more they realize that this sort of teaching can't be practiced in the household life
but they don't for that reason, they shouldn't for that reason come to the conclusion that
this isn't enlightened but they interest that they have when they hear the teachings that makes
them decide that they can practice this teaching in the household life. This happens quite often,
I've taught many people over Skype and this is what you find because a Skype teaching, a Skype
course, an internet based course, is quite unique in that the person is in general not totally
committed to the path, they might never even think to come out to Sri Lanka or come to our
monastery or go even go to a meditation center near them but they have problems in their life and
so they're genuinely interested in the meditation. Once they begin to practice, it's quite
amazing even over the internet to see the change that overcomes them, that they realize
how impossible this is, that they're not able to fully dedicate themselves to this practice
and more than that they realize that the work that needs to be done, the answer to their problems
cannot come while they're here living in the world the life or it's very difficult and quite often
in the beginning when they said I could never come to Sri Lanka, I could never come to the monastery
in the end they start making plans for when they're going to buy their ticket
because of the change that overcomes them and how they realize how deep and how important
are the solutions to their problems, how important it is to solve these problems and how deep
the problems are, how difficult they are to really solve but also how possible it is.
This is the hope that they get when they actually see for themselves through the practice.
So here someone goes forth, but another way of looking at this is this is a here a person
undertakes to dedicate themselves to the practice, whether they do it at home or whether they do it
in a monastery or wherever, they undertake to leave the world, to leave the world behind and not
worry about their job or their family or their children or their husband their wife, their
their possessions, everything they put it all behind, in favor of learning about themselves
and learning about reality and coming to see the truth of life.
And so the first thing they do is they undertake morality and the Buddha uses an example here,
he's using the highest example and that's the example of a monk and more than just a monk,
he talks about actually the full course of practice.
And this is in the Trula Hatipad open, where he gives a complete discussion of what it means
to be a meditative, the perfect way. So we should understand this to be just an example because
there are many aspects of this path even to the point of ordaining that are not completely necessary.
Even though ordaining is quite useful and of course many of these practices are quite useful,
they're not all of them necessary. But here the perfect example is this person decides to shave off
their head, their hair, and beard if they have a beard and put on a robe and go forth
and undertake the Buddha's teaching in earnest because they see that as the Buddha said it can't be
or as it says in the suit does it can't be, can't be practiced just like a polished shell,
it can't be practiced perfectly pure, there will always be spots and stains and torn edges,
it will not be a perfectly smooth practice if they stay in the home life because they'll have
to deal with money and they'll have to deal with bargaining for things and so on.
Today we got an interesting bargain here in the tourist area.
It's not important, it's the person decides to go forth and the first thing they do is keep
morality and Buddha said they went through many of the things they stopped doing, they were
framed from taking life, they were framed from stealing, they were framed from cheating,
they were framed from lying, they were framed from harsh speech, from divisive speech,
from useless speech, they were framed from so many minor things that monks are not allowed to do,
they were framed from buying and selling, they were framed from eating in the afternoon,
they were framed from wearing luxuries, wearing ornaments and beautifications,
they were framed from raw food, storing food, cooking food, they were framed from many,
many things and it goes through this, they were framed from gambling, they were framed from
playing games, they were framed from fortune telling and so on and all of these things that other
recklessness, other religious people might still engage in. They give this up and so their
mind becomes calm, they take up this practice of morality and he said the first thing they do is
take up these precepts, it's not to do this, not to do that. The second thing is they become
content with their, they become content with just their robes in their bowl and the Buddha gives
this exposition of what it means to be perfectly content and he says that we should be content
with just our robes in bowl. This is the purity of the monks life, it's not just about all these
rules, the rules are there in place there to keep our minds, to focus our minds, to not to give
us many, many rules that we have to worry about. It's to tell us that all of that is often to give
us a big picture of all of the many things that are off limits so we can understand for ourselves
where our limits are and to keep our minds within the framework of the practice because once you
give up all of that, once you give up money, once you give up food in the afternoon, once you
give up working and buying and selling, once you give up playing games and entertainment and
beautification, all that you're left with is the practice. But then even more than that he goes on
to this simple teaching of just having your bowl and robes. What it means is living your life just
you and your robes and your bowl, which is the simplicity of the monks life, a monastic life,
the key to the Buddhist monastic discipline is to be content just as you are. So when we come here
to Poland or Rua even thought to ourselves tonight we could stay here because we don't have a
home that we have to guard, we don't have children, we don't have a place, we don't have
responsibilities at any sort. We're free like a bird and this is how the Buddha put it.
He said when someone takes the robes in their bowl this morning I went through Anradapura on
Amstrand, I didn't even need money, I didn't need to to to bring me food or to give me
Donna just went walking through the village and the people were so kind to give me some little
food that they had in the restaurants or in their stores or in their homes. And just like a bird
flying free with your two wings the Buddha said so this is one wing and the bowl is the other
wing. The rooms are there to cover the body that's enough it's just a cloth to cover your body
and the bowl is there to give you enough sustenance to continue on the path. This is the second
level of morality even higher than all of the rules this contentment this simple life where you don't
worry you don't concern yourselves about anything. Now of course when we have a monastery and we
have a meditation center there are many things that can be involved when we want to give a talk and
teach people over the internet and so on and we need supplies and equipment but in the end for
our own lives for our sustenance to keep us going and to keep us happy and content we don't need
luxuries we don't need entertainment all we need is our robes and our bowl. This is the simplicity
and this is the power of the monastic life and this is something in and of itself that makes people
real it makes people gain great faith in the Buddha even when they don't do it themselves and they
look at the monks they see the followers of the Buddha. This is the Buddha said that even
by looking at a follower of the Buddha and seeing how well-behaved they are whereas other
recklessness well for example I mean not to brag or look down on anyone but a good
an example that is a fair example I think is there was some strange sadu I guess I don't know
what you'd call him but he was wearing a similar robe and he had a beard and had long hair
and I met him on the robe and he said something to me and he had a little thing here I think
he was Hindu or something and while I was going on on around he was in the stores buying food
so the example here is the difference the point is people when they see the Buddha's disciples
doesn't know it doesn't mean they're enlightened or they've realized the truth for themselves
but that they're well disciplined and for this reason people will come to say these people are
enlightened I have had people come to me and become incredibly sure that I was on the right path
and that I was some pure and perfect monk and that they're and the therefore gained great faith
in the Buddha's teaching just be by seeing me on arms around or that I only wear these simple robes
and that I go for arms every day and that I don't touch money and so on but the Buddha said this
isn't a reason to think that either that person or their teacher for sure their teacher
is a perfectly enlightened Buddha and this is indeed the case these people who had great faith
a month later two months later they might for some silly reason decide that I'm useless or
that this monk is where they would get bored or so on and many things can happen
that their faith is not stable because it's just blind faith it's just irrational faith
there's no direct connection between this practice and it's something beautiful and it's
something wonderful and it's something powerful and it is something that leads people rightly
to decide to undertake this practice the story of Asaji who went walking through the village
on arms one of the first five disciples of the Buddha who was an arahant and when he walked on
arms you could anyone could see how pure and perfect he was and so this is what attracted
Upa Tissa who later became sorry put the reason why Sariputa joined the Buddha's order
is because of Asaji he saw Asaji going on arms run and he said that isn't enlightened
be and he was sure but of course Upa Tissa Sariputa was a very discerning person but it's
a good example for us that even just seeing this seeing this purity that these these monks
really need only this their robes and their bow so this is something that we strive for
and even lay people can strive for this sort of thing to live all of you you know you've come
here we all slept last night under the under the J idea with Yes and and Swameda who didn't
couldn't join us here and this is a great power that we have ordinary people can't do this
they wouldn't go and they would know we need a hotel we need to go into we can't possibly stay
on overnight so we have this power of morality of being able to of contentment
and being able to give up our attachments to things this is a kind of morality the third level
of morality then the Buddha talked about is guarding the senses he said then then the noble
disciple guards the eye and whatever and whatever they see with the eye they don't and the
Buddha uses this term they don't allow their minds to perceive or to to go into the characteristics
and the marks and the the aspects of what is seen this is a very important teaching this is
very very much the core of our practice this is how we begin our practice our practice begins with
let's seeing just be seen let hearing just be hearing as I said yesterday deep paid deep tama tama
mama sati sute sute tama tama mama sute when we see let it just be seeing when we hear that
it just be hearing and the Buddha said it's quite clear and the hadi chula hadi pado he doesn't
pay attention to he doesn't allow his mind to conceive the the characteristics of what is being
seen so if it's something that normally would seem beautiful still just see if it's something that
would be ugly still just see and eventually through our practice the mind comes to this that what
is seeing really is just seeing all of the aspects of good bad mean mind and so on just appear
with the ear whatever sounds or heard whatever with the nose whatever smells or heard it's just
smelling with tastes whatever tastes really taste or just tasting when we're eating our food we
become aware tasting we know that this is a taste when we feel something on the body whether it be
hot or cold or hard or soft or some pleasurable feeling or some painful feeling we know it
just as a fear whether it's painful or pleasant or so on and thoughts as well whatever thoughts we
have that are pleasurable or this pleasurable or unpleasant we know them just as thought
and this is where our practice begins this is the actually the the the moment that
mindful that concentration turns morality turns into concentration so this is the highest
form of morality it means guarding the sense so that and he says because if we didn't do that
there would arise liking there would arise disliking there would arise all sorts of
of unwholesome stage there would arise addiction when we see then we would be happy about it we
would be attracted to it when we hear then we would be attracted to the sound or repulsed by the
sound when we hear this noise then it would be angry or upset by when we have smells of good
smell or bad smell all of us none of us have showered in a day or more we have this bad smell
and we become repulsed by that when we have tastes we eat our food and it's good or bad
feelings and all of these will give rise to addiction and aversion this will create great
this is what creates all of the problems in the world we think that the problems in the world are
caused by conflict or caused by real problems that arise or or and so on but in the end they
only arise in the mind when we take our on object as being more than it is when seeing it's
no longer just seeing but it's me it's mine it's good it's bad this is my friend this is my
enemy this is my on my my food this is my my body you know when our body is too fat or too thin
or too tall or too short or too white or too dark then we think we feel very sad about our body
or when our body is beautiful when our body is the right shape or the right color we become
proud of it and we become attached to it and then we do this with everything we'll do this with
people this is my husband this is my wife this is my son this is my daughter and then we become
attached this is my father this is my mother and then when some change over comes them and we
suffer so this is what this is what it means morality morality is to keep your mind from falling
into unwholesome state once you do that concentration rise this concentration is the state of
mind that is free from those state and then we begin the practice this is how the practice begins
there's morality and contentment and then we have this guarding and focusing the mind and then
finally just knowing the mind the movements and the the activities of the body in the mind
and so then the Buddha gets into the actual practice it says for this reason even though
then their mind becomes focused they still don't think that they still don't know for themselves
that the Buddhism like because it's possible that in other other religions or religions that
there isn't an enlightenment that this can be taught or this can be achieved and then that
person undertakes sati and sampaganya then they undertake the practice so once they're guarding
their faculties guarding their senses then they become aware of everything they do when they're
walking they know they're walking when they're sitting they know they're sitting when they're
standing they know their standing when they're lying down they know they're lying down when
they're eating they know they're eating when they're drinking they know they're drinking when
they're talking they know they're talking when they're silent they know they're silent
when they're urinating and defecating this isn't a business people become shocked by hearing
this but the Buddha actually is a your meditation practice has to go through your whole life
meditating should meditate over urinating, urinating and defecating as well.
At the moment of urinating, they know they're urinating, at the moment of defecating.
This is coming to understand reality because this is a very, very ordinary part of reality.
When waking up, they know they're waking up, and falling asleep, they know they're falling
asleep.
Every aspect of our lives, when walking, walking, walking, when talking, talking, talking.
You can even feel your lips moving and know that you're talking.
When listening, hearing, urinating, urinating, urinating, and other feelings, and so on,
when showering, when eating, chewing, chewing, swallowing, and so on.
This is how our concentration develops.
This is really the basis of the meditation.
This is how we teach people when they come.
What we're doing when we teach you the mindful frustration, walking meditation, sitting
meditation is exactly this.
People giving you simple exercises that will serve as an example for you to then take
into your daily life and develop through your whole life, until your whole life becomes
mindful, so that you're aware and clearly alert, you know, of sati, you have mindfulness
or awareness or recognition of things, of seeing things as they are.
And then you have some pajanya, the knowledge of these things as being impermanent, unsatisfying
and so on, just simply seeing them as they are, the recognition and the awareness.
And what does this lead to?
This leads the mind to become, this is where our concentration is developed.
Now concentration has the effect of, as the Buddha said, removing the hindrances from
the mind.
And this is really the most important aspect of the practice that our practice should help
us to overcome the comets and the central desire, which I talked about in regards to morality.
What it means is by practicing the morality and guarding the senses and by practicing
in regards to the postures and movements of the body and the feelings and the thoughts.
Our central desire will be repressed, will be removed, there will be no opportunity to
arise. Of course, if we stop practicing and we'll just come back and we'll give rise to
more projections and judgments of objects. But during the time that we're practicing,
it has no opportunity because seeing is just seeing, hearing is just hearing, walking
is just walking.
And the same goes for our ill will.
We will have no aversion towards things and our drowsiness will go away.
We will not become lazy and depressed and drowsy in the mind.
Distraction will go away and our mind will become focused.
We will not have any distraction or worry about the past deeds or worry about the future.
And our doubt will go away. We will have no doubts in regards to the practice.
Every time we have doubts, we will say to ourselves, doubt thing and doubt.
And we can do this with all of these, actually. Part of our practice is the inclusion
of things. In the Sati Patanasu, the Buddha said, even include these in your practice.
So not only are we focusing on the senses, we're focusing on the movements of the body,
but this is the third one. The Buddha said, when you have central desire, you know, wanting,
wanting or liking, like when you have anger, you know, angry, angry or disliking, just
like when you feel drowsy and drowsy and drowsy and drowsy, when you have distracted or worried,
worried, if you have doubts, say doubt thing, doubt thing, confused, confused or so on.
And these are removed from the mind. This is the development of concentration. So this
is where the Buddha said, morality leads to concentration. So this is how our practice goes.
Now what does concentration lead lead to? Well, the Buddha goes on, but I'm going to cut
it short because the rest of the path is more or less optional. I mean, this is the fruits
of concentration that one can read people's minds, remember past lives, fly through the air,
that the mind can become quite focused. The most important aspect is that the mind becomes
focused and the hindrances are repressed. The hindrances are removed from the mind temporarily,
because it's these hindrances that the Buddha says in the trilada, the hindrances are
an obstacle to wisdom. What are these things? What is the problem with these things that
they are an obstacle for wisdom? So all of these other fruits of the practice are actually
incidental. The ability to read people's minds and remember past life is not really wisdom.
It's wisdom in a worldly sense, but it's not. If you go through the suity, you see that
even for this reason, when one remembers one's past lives, when one reads people's
minds, even for this reason, one would not say, oh, my teacher is enlightened. How could
one? This is not real knowledge. This is just an experience of something in the past or
something in the future or something external to yourself, some concept.
And so the most important thing is that one continues the practice, whether one gains
magical powers, whether one gains deep states of concentration or not. What's most important
is that one comes to realize the truth of reality. One comes to give rise to wisdom.
And so the Buddha said, the point at which the person is able to realize that this is
indeed, and my teacher is indeed enlightened and this is indeed a fully enlightened being,
is one one realizes for oneself the truth, the truth that the Buddha called the
foreign noble truths, the real truth of life, which are the truth of suffering. The reason
why something unpleasant occurs, the reason why a problem of any sort occurs.
These are the only truths that are really useful, if you think about it, and you can see
that the only really truly necessary truths for us to realize, no matter what other truths
we realize, is the truth of suffering, the truth of suffering in the sense that what is
the cause of suffering? Why is it that we suffer? Because if we had no suffering, nothing
else would matter. It wouldn't matter whether we couldn't or couldn't read people's
minds. It wouldn't matter whether we could not see far away or so. It wouldn't matter
if we had all the intellectual knowledge in the world. Once we understand the truth
of suffering, and we understand those things that cause suffering, as causing suffering.
Once we understand those things that are unsatisfying, as being unsatisfying, then we will
never cling and we will never attach, we will never follow or chase after those things
ever again, we will become free from suffering. And the Buddha said in this way, he comes
to be free from what the Buddha called the Asuva, that the reason why we understand,
the reason why we believe, the reason why we would even take the Buddha as our Lord
and Master, which is a very difficult thing for people to hear everything, is because
we come to realize the truth for ourselves. We come to see that indeed one, it is possible
to become perfectly enlightened. Two, our teacher teaches the way to become enlightened,
to become free from all the foundment. And three, we have for ourselves, we come free from,
we have gained some measure of enlightenment. This is when a person takes the Buddha as
their Lord and Master. So I am not trying to say that this is about any of us, but this
is how we should reserve our faith in the Buddha, that the faith that we have should
be limited to the results that we have gained, until we realize for ourselves the truth
of the Buddha taught and the truth of life, and that everything that for example, everything
that arises ceases, that there is nothing in the world that can bring us happiness and
therefore nothing in the world in any world, any frame of existence, that it is truly satisfying,
that should be clung to, nothing worth clinging to. No dhamma, sambhi, dhamma, namma, namma,
namma, namma, namma, namma, namma, no thing is worth clinging to, until we realize that for
ourselves and truly realize it, and therefore have no clinging inside, whether it be clinging
to sensuality, whether it be clinging to becoming, or whether it be clinging just due to ignorance.
Those are the Kama, Sava, Bawa, Sava, Avintasava.
Whether it be any of these types of defilements in the mind, whether it be the defilements
in regards to sensuality, all of these are gone, because all of the objects of the sense
we've come to see them just as arising and ceasing.
They can't satisfy us.
We've come to see them just for what they are.
We've come to see that what they are is something very, very simple.
Has nothing to do with good or bad or me or mine pleasant unpleasant, desirable, undesirable.
And they simply are what they are.
We've come to see this perfectly clearly.
We've come to remove all of the illusions and delusions, all of the habits that we have,
all of the partialities that we have that are based on the baseless, that have no basis
in reality.
Whether it be in favor of things or against things, we have no aversion towards things
as always, see those things that we thought were ugly or unpleasant or undesirable or fearsome
or so on, are as well simply experiences that arise and cease.
Bawa Sava is in regards to becoming, so in regards to becoming this or becoming that
we have no interest, we've come to see that anything that we become, anything that becomes,
will have to disappear, could never last forever.
No matter what high state of mind we might have, in the end it has to finish, it has to dissolve.
When we stop working for it, when we stop striving for it, it will disappear.
And so we give up all striving, we give up all clinging, all desire for any becoming,
or for any non-becoming, for getting rid of anything, for removing anything from our lives.
We come to accept what it is and be perfectly content with what it is.
Or Oizasa, any kind of taints or any kind of delusions that come simply from ignorance,
we remove all of these as well.
So all of our conceits and all of our arrogance, all of our wrong views, we remove them as well.
All of our views, any views that we might have or opinions that we might have, this is right,
that is right, this is wrong, that is wrong.
I believe this, I believe that we throw it all out of it.
Once we can do this, then we are said to have seen truth in it,
so as though then we have seen the bull elephant, in the sense that then we have come to know that the Buddha is the Buddha.
We've come to realize that anyone who can teach this teaching, that we have practiced and learned,
is truly indeed a perfectly enlightened being.
So this is the point where we come to truly understand and have faith in the Buddha.
So all of us are working towards that, on the path, working towards that.
So how we should react, how we should relate to this, is that our faith in the Buddha is exactly,
it should be perfectly in regards to our practice.
We all have faith in the Buddha not because other people have said that he's this or that,
or because of how we've seen other people act, or because we have faith in other people that we've seen.
But because we ourselves have to some extent realize the truth of his teachings,
and to the extent that we've realized the truth of his teachings, to that extent we've become faithful.
We've become confident in him, and we've taken him as our teacher, our guide, our Lord and our Master.
And of course the great thing about the Buddha's teaching, which should reassure all of us,
and certainly reassures everyone who everyone that I talk to, or teach Buddhism, is that the more you practice,
the more you want to practice.
So you might have decided when you first came to practice, just come for a few days to try it out.
But even when you come for just a few days, after a few days you want to stay for a week,
and after a week you want to stay for a month, and after a month you decide you want to give up your life and become a monk.
Anything you can happen, because this is the Buddhist teaching, it's open, it leads you on something that you can,
the more you practice, the more you want to practice.
The more faith you gain in the practice, or the more you practice, the more faith you gain.
The greater confidence you have.
So there never has to be, and the Buddha's teaching never has to be, never should be, and never has to be.
So some sort of blind faith, or some sort of clinging to faith, that any kind of fear that you might be wrong,
or fear that you might somehow lose your faith in the Buddha.
Often I will instruct people to give up their faith, say whatever doubts you may have.
Just give up any faith that you have.
Don't try to cling to it, or make yourself believe, give it up, because the truth will come by itself,
and the truth will give you an unshakable thing, an unshakable conviction in the Buddha.
Once you realize the truth for yourself, you'll never have to have any sort of attachment to faith, or any kind of clinging to faith.
The faith that you will have will be based perfectly on your understanding.
And so that is what it means, I think, for us to take refuge and to take the Buddha as our guide in Master.
Perfectly good reason for us to make such statues, because even from what little we've gained,
if you think of what little we've gained of the Buddha's teaching,
we can see that even that very little amount is incredibly profound.
That this little piece of the Buddha's teaching, and we know that we haven't made it maybe anywhere near the goal.
But even that that we've gained is something that has changed our lives in a very profound way.
We used to do evil things, and now we don't do evil things.
We used to avoid or even look down upon doing good deeds.
And now we undertake good deeds.
We used to think of meditation as a waste of time, and now we think of everything else as a waste of good meditation time.
And so, it's actually not such a remarkable thing to think that someone would carve a rock like this and make a Buddha statue,
even as impressive as it might seem.
It's actually, for someone who has practiced meditation, quite a small task.
Corcorping something into a rock is a small task compared to the task that we have ahead of us,
and the task that we are undertaking.
Even a weak in meditation seems to be much more difficult and much more worthwhile task,
and a much greater, greater respect and homage to the Buddha.
And this is how the Buddha put it as well.
And it's great to see this kind of homage being paid, but he said this isn't the true way to find homage to the Buddha.
If we think of the Buddha when he was about to pass away, and all the people from all of the country and all of the monks,
and even all the angels were bringing flowers and candles and incense to pay homage to the Buddha.
And the Buddha said, this isn't how you pay respect to an awful inline Buddha.
But he said, whatever person will be a monk, a nun, a layman or a laywoman,
if they practice the Buddha's teaching,
they will be able to pass away from the Buddha.
So, the Buddha said, this is a person who acts proper in regards to the Buddha,
someone whom Garu Garu, who acts with homage or respectfully towards the Buddha,
Mani Dipun Jainti, who pays homage and pays respect to the Buddha,
someone who practices correctly, who practices the teaching in order to realize the truth of life.
So, this is what we aim to do with our practice and as a Buddhist.
And this is where we should find faith and respect for the Buddha.
And this is kind of an explanation as to why we have such homage and such admiration for the Buddha
and why we don't spit on the Buddha images as they say.
We don't look down on the Buddha images because this is a picture of our teacher.
This is our someone who is our guide.
It doesn't mean that we take these things as a substitute for practice.
But we take these as a sign of honor and respect.
And we respect these things and we respect them in place of our teacher,
who has already passed away into the perfect peace, happiness and freedom of suffering.
So, I hope that has been of some years.
Thank you all for listening and wish you all and everyone listening in to be able to practice the Buddha's teaching
and to find true peace, happiness and freedom from suffering for yourselves.
Thank you all for tuning in and wish you all the best.
