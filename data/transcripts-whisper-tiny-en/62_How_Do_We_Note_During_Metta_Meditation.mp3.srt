1
00:00:00,000 --> 00:00:03,840
How do we note during metametitation?

2
00:00:03,840 --> 00:00:08,960
In the practice of metametitation, the mantra is used to evoke friendliness,

3
00:00:08,960 --> 00:00:11,520
so you would say to yourself something like,

4
00:00:11,520 --> 00:00:13,600
may all beings be happy.

5
00:00:13,600 --> 00:00:18,320
In Vipassana meditation, the mantra is used to evoke a strong recognition,

6
00:00:18,320 --> 00:00:21,600
Tirasana, of experiential reality.

7
00:00:21,600 --> 00:00:26,560
This process is sometimes referred to as noting the object of experience.

8
00:00:26,560 --> 00:00:31,520
Since metametitation focuses on sentient beings, which are conceptual,

9
00:00:31,520 --> 00:00:37,120
it is not really practical to combine noting concurrently with metametitation.

10
00:00:37,120 --> 00:00:41,440
Technically, it is possible that when you are practicing metametitation,

11
00:00:41,440 --> 00:00:46,960
to switch to Vipassana meditation and focus on the four foundations of mindfulness,

12
00:00:46,960 --> 00:00:50,160
but this is not the point of metametitation.

13
00:00:50,160 --> 00:00:54,240
Mahasisada talked about how when giving gifts,

14
00:00:54,240 --> 00:01:00,080
some people will insist on noting, moving, placing as they give something.

15
00:01:00,080 --> 00:01:03,680
He pointed out that such focus is not the purpose of giving.

16
00:01:03,680 --> 00:01:07,360
When someone gives a gift, they should make a determination.

17
00:01:07,360 --> 00:01:13,200
For example, may this gift be a support for my attainment of enlightenment.

18
00:01:13,200 --> 00:01:16,160
At the time, when you are making a determination,

19
00:01:16,160 --> 00:01:20,000
it should not be with mindfulness of ultimate reality.

20
00:01:20,000 --> 00:01:25,120
It should be with full intention that the gift is for the benefit of the recipient

21
00:01:25,120 --> 00:01:27,920
and for the benefit of one's own mind.

22
00:01:27,920 --> 00:01:31,200
That one may become free from suffering.

23
00:01:31,200 --> 00:01:35,360
Charity, like metapractus, is a conceptual practice.

24
00:01:35,360 --> 00:01:39,840
In theory, one should be always practicing Vipassana meditation,

25
00:01:39,840 --> 00:01:44,080
but in order to get to the level where such a state is possible.

26
00:01:44,080 --> 00:01:50,560
Practices like metapractus, as well as charity and morality are of great practical benefit

27
00:01:50,560 --> 00:02:20,400
and should be practiced independent of Vipassana meditation.

