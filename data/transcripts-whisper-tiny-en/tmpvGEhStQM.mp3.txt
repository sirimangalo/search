Welcome back to Ask Among.
Today's questions come from Papilon, BLN1, who asks some technical questions.
How many times a day could one meditate?
How long does one have to meditate for session for the first time?
Then does one increase the session time of meditation if yes, when?
Okay, so questions about a technical practice of meditation.
The first thing to realize is that meditation, ideally, in its developed form, should
be constant.
It should be something that is carried out for all of your waking hours.
So even sitting here, talking to you, I'm aware of the fact that I'm sitting, I meditate
on the sitting position, the awareness of the sitting, aware of the lips moving, aware
of the sounds of the nature around me, and the meditation is being aware of that and seeing
it clearly for what it is using this mantra, and even when you're talking, you can
say to yourself, feeling the lips moving or so on, you can carry this out.
Now whether you're able to do that 24 hours a day or for all of your waking hours is
another question.
So in the beginning, we do formal meditation to train ourselves in this, because obviously
to simply go out into life and attempt to be mindful, it's not really going to work in
the same way that a person who practices martial arts and they say, okay, we learn some
techniques, now I'm just going to go out and practice them in real fights.
So obviously not going to work out that way.
You have to train in a dojo in a gym and so on.
And then if someone comes and picks a fight with you, you can defend yourself.
But after the training.
So in the same way, with meditation we train in the gym or the lab, whatever you want
to call it, which is with a formal meditation practice.
And I recommend this, there's no question that this helps, because most of us are beginners
anyway.
We're talking, the advanced state is for someone who is enlightened.
If you're enlightened, you're always aware of everything that goes on and you're clearly
aware and you see it for what it is.
But along the path, we develop this, we train ourselves and this is what formal meditation
is all about.
So it really doesn't matter how long you do it, however long it's comfortable for you.
I think most people are not lazy in the sense of doing too little.
If you're intent upon it, I think more laziness comes with knowing that we've done a certain
amount of meditation.
People get good at doing a half an hour or an hour of meditation or even longer.
They start to think that they've attained something, they've realized something and many
times they haven't, many times all they've attained is the ability to sit perfectly still
and come and to fix and focus their mind in a concentrated state that's not really aware
and not really learning and not really meditating.
So we should never ever fixate on the amount of time that we do.
Even in a formal meditation course, I would never tell you that every day you should do
so many hours.
There are centers that do that.
I don't.
And my teacher was quite critical of that sort of thing because then you've turned meditation
into an entity, you know, my half hour of meditation, my hour of meditation.
If you say that you meditated for a certain amount of time, you're probably lying because
most of the time your mind was wandering and so on.
So meditation is not about how many minutes you do, it's about how many moments you're
aware, how many moments you clearly see things as they are and it's developing this
clear awareness, which is momentary, which is every experience that we have coming to learn
about experience and so even in a few steps, you can learn something about yourself and
if you're watching the breath, if you really are aware, just knowing this is the rising,
this is the falling and saying, you're rising and simply being with it as it is.
Even just a few times, it's much better than sitting for an hour and forcing the breath
and getting angry and getting frustrated and so on, actually that's beneficial as well
because it helps you learn about learning that you're an angry person and so on.
But at any moment that you're actually meditating and learning and opening up and coming
to accept and let go and simply be, you know, not just simply be but to learn, any moment
that you're learning, that's a moment of meditation, but I think this isn't quite satisfactory
for most people who want to set up a formal meditation during their life, so I recommend
generally people to do meditation at least morning and evening, so do twice a day.
If you have time during your normal daily routine, do another one.
If you have a day off, you can take a day to do practice meditation.
There's no limit, as I said, you should be mindful the whole day through, but I don't think
you'll be benefited by just filling your day up with meditation.
There's a limit, especially when you're not on a meditation course.
If you do an intensive meditation course, you can build up to many hours because it's
just natural.
Your mind comes down and you're able to focus and you don't have any desire to do
this or do that, and as a result, you can do many hours of meditation, but even in that
case, you have to take breaks and more important is that your mind will between the time
you're meditating, when you, the whole time, during the time you're doing sitting and
walking, your mindful, and when you get up off the mat, you're also mindful when you
walk to the washroom, when you walk to the kitchen, the dining room, and so on.
You're also mindful, it's much more important.
So there's no limit, but it's not really the point to set a time.
Another technical notice that we, you know, obviously from the videos we like to balance
walking and sitting, but that's simply a technical aspect of meditation.
That's one of the reasons why a person might time their meditation, your time it, not so
that you can be sure you meditate it so long, so that you know that you're balancing the
tool, because obviously walking meditation is a beneficial thing.
Another reason is because it is possible, you know, when you, if you don't time your
meditation, if you don't see, I'm going to sit for X number of minutes, then as soon
as something interesting arises, and by interesting, I mean difficult.
So a stress, a suffering, a pain in the body, we get up and quit, you know, because
we don't have any, there's nothing keeping us on the mat, but if you have got to set
time limit, and if it's a little bit longer than you meditated before, then when those interesting
experiences arise, you'll actually have a reason to stick around and learn about them.
And that's where meditation has its real benefit.
In a sense, forcing yourself to stick with the experience, but it's not forcing anything,
it's just not reacting or not allowing yourself to react, studying your reactions instead
of, you know, getting, when you're getting angry, to follow it and to run away from the
experience, you study the anger and you stick with it, and you come, you overcome the
anger, you come to realize that the problem is our disliking and so on.
When you want something and instead of going to get it, you study the one thing and you
see that that's stressful.
So there are reasons for putting a limit on the meditation and there are reasons for increasing
it.
The best way to do this is under a teacher because that's even more external, then you're
even less able to follow your preferences because the teacher's telling you, okay, today
do more meditation, so, you know, you can't just increase it when it's comfortable for
you.
And it challenges you.
I would recommend, you know, keeping it challenging it, but as long as you're learning,
unless you're in a formal meditation practice, it's going to change.
And if you're so absorbed in the time of meditation, you're always going to be disappointed
and stressed, you'll start to stress about your meditation, oh, I've, today, I only did
so much meditation or I didn't have time to do full meditation and which is really garbage,
it's not real.
I mean, when you're thinking like that, that's when you should be meditating.
If you're so upset them because you weren't able to meditate or stressed or worried about
are you going to have time to meditate, that's when you should be meditating.
When you're stressed, you know, instead of, you know, that's when real progress is going
to come about because obviously you're the sort of person who thinks about the future
and the past and worries about these things and is upset by them.
So don't fixate on the time and don't fixate on the necessity to do any formal meditation,
try to fixate on the reality in the present moment and that's much more important doing
formal meditation is good, but that should eventually come naturally where when the time
comes to meditate and there's nothing pressing, you sit still and then you watch your
breath or whatever.
So I hope that helps and you're able to set up a dedicated meditation practice and continue
to develop, to train yourself and be able to realize the truth of life and reality, okay,
thanks, all of that.
