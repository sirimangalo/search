1
00:00:00,000 --> 00:00:06,500
When we talk about nibana and no rebirths, what are we saying that happens,

2
00:00:06,500 --> 00:00:13,000
non-existence and of life?

3
00:00:13,000 --> 00:00:20,000
Well, it's a pretty simple answer. I mean, existence is momentary.

4
00:00:20,000 --> 00:00:28,000
One moment is one existence, and it arises in its seasons.

5
00:00:28,000 --> 00:00:31,000
That doesn't happen in nibana.

6
00:00:31,000 --> 00:00:38,000
It's the easiest way to understand it.

7
00:00:38,000 --> 00:00:47,000
And since life itself is only composed totally of those momentary experiences,

8
00:00:47,000 --> 00:00:56,000
then there really is no such thing as a life that could end.

9
00:00:56,000 --> 00:01:00,000
There's only experiences which end every moment,

10
00:01:00,000 --> 00:01:04,000
and that doesn't occur.

11
00:01:04,000 --> 00:01:10,000
There is no more arising of those momentary experiences

12
00:01:10,000 --> 00:01:14,000
of seeing, hearing, smelling, tasting, feeling, and thinking.

13
00:01:14,000 --> 00:01:21,000
Of any momentary experience, because nibana is eternal, that's what makes it eternal

14
00:01:21,000 --> 00:01:26,000
or undying, or it's not the end of life.

15
00:01:26,000 --> 00:01:29,000
It's eternal life.

16
00:01:29,000 --> 00:01:32,000
It's called amata. It means no dying.

17
00:01:32,000 --> 00:01:36,000
But the reason there's no dying is because there's no being born.

18
00:01:36,000 --> 00:01:38,000
There's no arising.

19
00:01:38,000 --> 00:01:41,000
Since there's no arising, there's therefore no ceasing.

20
00:01:41,000 --> 00:01:45,000
It's a pretty simple.

21
00:01:45,000 --> 00:01:53,000
The Buddha refused to answer questions of that kind, didn't he?

22
00:01:53,000 --> 00:01:56,000
No.

23
00:01:56,000 --> 00:02:01,000
I've heard people say that, but no, but nibana, it's quite clear.

24
00:02:01,000 --> 00:02:05,000
There's questions like, does the...

25
00:02:05,000 --> 00:02:08,000
Well, the question is, does the arrow hunt exist after death,

26
00:02:08,000 --> 00:02:10,000
or does he not exist after death?

27
00:02:10,000 --> 00:02:13,000
Yeah, that's what I was mentioning.

28
00:02:13,000 --> 00:02:17,000
The point being that there is no arrow hunt, right?

29
00:02:17,000 --> 00:02:19,000
That's a concept.

30
00:02:19,000 --> 00:02:22,000
The problem, the reason why the Buddha might not answer,

31
00:02:22,000 --> 00:02:26,000
in certain occasions, didn't answer, is because he knew that they listened

32
00:02:26,000 --> 00:02:29,000
or wouldn't be able to appreciate it, and wouldn't like that answer.

33
00:02:29,000 --> 00:02:30,000
Oh, I see.

34
00:02:30,000 --> 00:02:33,000
Not necessarily that it's unanswerable.

35
00:02:33,000 --> 00:02:36,000
The answer is that the arrow hunt never existed in the first place.

36
00:02:36,000 --> 00:02:38,000
There's only momentary experience.

37
00:02:38,000 --> 00:02:40,000
In fact, I was reading last night in the Majimanika.

38
00:02:40,000 --> 00:02:44,000
I have something very interesting how the Buddha didn't answer.

39
00:02:44,000 --> 00:02:45,000
He didn't want to answer.

40
00:02:45,000 --> 00:02:47,000
It was totally unrelated related.

41
00:02:47,000 --> 00:02:53,000
It was the question about whether brahmanas are superior to katikatias.

42
00:02:53,000 --> 00:02:57,000
The nobles are the brahmanas, which one is superior.

43
00:02:57,000 --> 00:02:58,000
And he just wouldn't...

44
00:02:58,000 --> 00:03:01,000
He kept not answering, because he knew that if he said,

45
00:03:01,000 --> 00:03:06,000
no, they're not superior, then the person wouldn't be butt, butt, butt and have arguments.

46
00:03:06,000 --> 00:03:14,000
So first he let them on this long explanation about how one could think

47
00:03:14,000 --> 00:03:17,000
that they are unequal and so on.

48
00:03:17,000 --> 00:03:20,000
Until finally, he cornered the guy, and he made the guy ask,

49
00:03:20,000 --> 00:03:25,000
well, in this regard, are they different?

50
00:03:25,000 --> 00:03:29,000
And he said, no, then the Buddha was able to say without worrying about,

51
00:03:29,000 --> 00:03:32,000
without concern about having this guy argue with him.

52
00:03:32,000 --> 00:03:36,000
No, then, well, in that case, in that way, they are not.

53
00:03:36,000 --> 00:03:41,000
In regards to meditation, it was something like, in regards to their purity of mind.

54
00:03:41,000 --> 00:03:44,000
Because just if any of the...

55
00:03:44,000 --> 00:03:49,000
If a kasad katya or brahmanas to take sticks and rub them together and make fire,

56
00:03:49,000 --> 00:03:52,000
the fire that comes is the same.

57
00:03:52,000 --> 00:03:59,000
So in the same way, if anyone is to practice meditation, it doesn't matter what cast them.

58
00:03:59,000 --> 00:04:06,000
But in other cases, the Buddha did say quite categorically that the different types of people are,

59
00:04:06,000 --> 00:04:09,000
there is no difference among them.

60
00:04:09,000 --> 00:04:13,000
But I think very much based on the audience, there were times where he...

61
00:04:13,000 --> 00:04:17,000
And then he said to Ananda afterwards, if I had told him one or the other,

62
00:04:17,000 --> 00:04:19,000
he would have been more confused.

63
00:04:19,000 --> 00:04:23,000
So that's why I didn't tell him, but it's not that there is no answer.

64
00:04:23,000 --> 00:04:27,000
And there are certainly, there is the case, I can't remember if it was the Buddha,

65
00:04:27,000 --> 00:04:33,000
sorry, Buddha, who said that there must have been the Buddha as well.

66
00:04:33,000 --> 00:04:40,000
What is it that happens at Parinibhana?

67
00:04:40,000 --> 00:04:51,000
And I think this monk had a pernicious view that the self is destroyed at Parinibhana.

68
00:04:51,000 --> 00:04:59,000
Some pernicious view like that, and the Buddha reprimands him,

69
00:04:59,000 --> 00:05:01,000
or sorry, Buddha, or I can't remember that.

70
00:05:01,000 --> 00:05:11,000
And then the answer to the question, what happens at Parinibhana?

71
00:05:11,000 --> 00:05:13,000
Is that the five...

72
00:05:13,000 --> 00:05:22,000
This is the orthodox answer, is that the five aggregates of clinging,

73
00:05:22,000 --> 00:05:27,000
which are impermanent unsatisfying in non-self,

74
00:05:27,000 --> 00:05:32,000
cease without arising again, cease without remainder.

75
00:05:32,000 --> 00:05:37,000
So nothing ceases, that wouldn't cease anyway,

76
00:05:37,000 --> 00:05:41,000
but they cease without remainder, meaning there is no more of these impermanence

77
00:05:41,000 --> 00:05:47,000
suffering and unsatisfying, and the non-self states arising.

78
00:05:47,000 --> 00:05:49,000
That's the orthodox answer, and it is in the typical.

79
00:05:49,000 --> 00:06:12,000
That's what happens at Parinibhana.

