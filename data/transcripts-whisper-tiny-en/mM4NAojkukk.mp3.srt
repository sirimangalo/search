1
00:00:00,000 --> 00:00:03,800
Okay, so today we continue our studio in the Lamocama.

2
00:00:03,800 --> 00:00:12,480
We want to verse number 24, which goes as well.

3
00:00:12,480 --> 00:00:35,540
You know the

4
00:00:35,540 --> 00:00:48,940
which means Uthana, Uthana, Uthana, who is effortful, puts forth effort. Satim, Uthana,

5
00:00:48,940 --> 00:01:00,140
who is mindful. Sujika Masa, with pure actions, with pure acts. Nisama, Arino,

6
00:01:00,140 --> 00:01:10,540
one who acts conscientiously, or considering. Sanyatasa, one who is subdued. Indamati,

7
00:01:10,540 --> 00:01:18,640
we know, who lives by the Dhamma. Apamatasa, one who is Apamat, Apamada,

8
00:01:18,640 --> 00:01:31,160
who has a heatful, verbiagellant. Yea, so Omi, Vadat, D. Perstetha, one, glory, ever increases,

9
00:01:31,160 --> 00:01:41,540
or greatly increases, constrainment increases. The glory of such a person who is all these things.

10
00:01:41,540 --> 00:01:47,740
Now this story was told in regards to a, well it's a fairly ordinary story. It actually doesn't have much

11
00:01:47,740 --> 00:01:55,440
to do with meditation, but it's another example of how the story shouldn't set the context,

12
00:01:55,440 --> 00:02:02,040
or shouldn't eclipse the meaning of the verse, shouldn't define the meaning of the verse.

13
00:02:02,040 --> 00:02:07,440
It does set the context, and it helps us to understand the reason why the Buddha gave the teaching.

14
00:02:07,440 --> 00:02:13,340
But as I've said before, the Buddha, giving the teaching, wasn't trying to give a teaching

15
00:02:13,340 --> 00:02:20,340
specifically about this instance. He was taking me the example and creating a rule out of it,

16
00:02:20,340 --> 00:02:28,340
which as you can tell from the verse actually does very much relate to the Buddha's teaching and the practice.

17
00:02:28,340 --> 00:02:33,340
So we have to, we have to be able to separate the story out. But anyway, we will tell the story.

18
00:02:33,340 --> 00:02:42,340
The story goes that there was the son of a very rich couple, and again it's a story of a fan about a plane,

19
00:02:42,340 --> 00:02:48,340
and the parents were, were afflicted with the plague, and so they told their son to, to run away.

20
00:02:48,340 --> 00:02:53,340
And they told them where to find all of their treasure that they had hidden, and said,

21
00:02:53,340 --> 00:02:57,340
when you're older, come back and retrieve your treasure, retrieve all of our wealth,

22
00:02:57,340 --> 00:03:03,340
because they hid it away and buried it or so on. And so greatly distressed and crying,

23
00:03:03,340 --> 00:03:11,840
he left us a boy and lived abroad for some time before coming back 12 years later, I think it's this.

24
00:03:11,840 --> 00:03:17,840
When he came back, he had grown up and had a beard and so on, and so nobody recognized him.

25
00:03:17,840 --> 00:03:23,840
And he went, he wears parents and said the treasure was buried and he found it and saw that it was there.

26
00:03:23,840 --> 00:03:33,840
But he thought to himself, no one recognizes me, and I'm, I have no station and have no, no other wealth.

27
00:03:33,840 --> 00:03:38,840
If I were to take this wealth and start to live by it, no one would believe it was mine,

28
00:03:38,840 --> 00:03:43,840
and they would, they would capture me and think I'd stolen and put me in jail.

29
00:03:43,840 --> 00:03:49,840
So he buried the treasure again, covered it up, and instead of taking it, he didn't take even a piece of it,

30
00:03:49,840 --> 00:03:52,840
because it would have been gold or so on, very valuable.

31
00:03:52,840 --> 00:03:57,840
Instead he went to live his life as an ordinary, as an ordinary worker.

32
00:03:57,840 --> 00:04:05,840
He found a job as a manager, getting people to work, telling people what to do in organizing labor.

33
00:04:05,840 --> 00:04:10,840
And I guess he figured that eventually he would slowly once he had got some of his own money,

34
00:04:10,840 --> 00:04:12,840
he would slowly bring that money in.

35
00:04:12,840 --> 00:04:16,840
But he didn't dare to take it, and so he lived quite conscientiously.

36
00:04:16,840 --> 00:04:20,840
And this was in Rajika, that he was living in Rajika, had a king of Rajika,

37
00:04:20,840 --> 00:04:29,840
who was a Sotapana, who was a follower of the Buddha, who had practiced the Buddha's teaching and become a Sotapana.

38
00:04:29,840 --> 00:04:38,840
And he also had some fairly special intuition of his own, he was able to tell the station of any man who's voice he heard.

39
00:04:38,840 --> 00:04:41,840
And it may have been because of his training as a king.

40
00:04:41,840 --> 00:04:54,840
But one day he heard this, this, this, what's his name, kumba, kumba, kumba, yas, kumba yu.

41
00:04:54,840 --> 00:05:08,840
One who has a voice of wealth, kumba, or voice of buried treasure, I think it means actually,

42
00:05:08,840 --> 00:05:13,840
the sound of the buried treasure, the sound of hidden treasure.

43
00:05:13,840 --> 00:05:21,840
And so he heard this, and he heard this sound of this man, and he said,

44
00:05:21,840 --> 00:05:24,840
now there's a man who has great wealth.

45
00:05:24,840 --> 00:05:31,840
And one of the servants who was standing there, she went and looked and saw that it was just an ordinary man who was actually quite poor.

46
00:05:31,840 --> 00:05:36,840
And she thought, well, kings don't just say ordinary things, so she sent someone to find out about him,

47
00:05:36,840 --> 00:05:43,840
and turns out that Yahi is living in a poor and a small hut alone, who absolutely know wealth.

48
00:05:43,840 --> 00:05:47,840
And so she came back and told the king, and the king said nothing.

49
00:05:47,840 --> 00:05:51,840
And yet the next time the king heard his voice, he said the same thing.

50
00:05:51,840 --> 00:05:57,840
He said, man, that's a man of great, great wealth, like more than ordinary.

51
00:05:57,840 --> 00:06:04,840
And so that this slave couldn't understand, and the servant sent a man to check.

52
00:06:04,840 --> 00:06:10,840
And again and again found out that there was nothing to, there was, he had no wealth in his own.

53
00:06:10,840 --> 00:06:13,840
And appeared that he had no wealth.

54
00:06:13,840 --> 00:06:18,840
And so the servant devised a trick, she took her daughter, and they went and she got her daughter to,

55
00:06:18,840 --> 00:06:28,840
they went and lived with him, pretending to be poor people, and got her daughter to become married to him, got him seduced by his, by her daughter,

56
00:06:28,840 --> 00:06:32,840
and ended up living together.

57
00:06:32,840 --> 00:06:39,840
And then she sent back to the king and told the king to order everyone to have a festival.

58
00:06:39,840 --> 00:06:47,840
But there was to be a festival, and everyone had to decorate, and they were part of their home, had to be decorated.

59
00:06:47,840 --> 00:06:50,840
And anyone who didn't decorate, it would be, it would be punished.

60
00:06:50,840 --> 00:06:54,840
This was the king's order, of course, kings can order just about anything.

61
00:06:54,840 --> 00:07:00,840
So this woman, she said, oh, my son, we need, we need money, and he said, I have no money, what are we going to do?

62
00:07:00,840 --> 00:07:05,840
And she said, well, we can go borrow some, find some way to get money, and she kept asking.

63
00:07:05,840 --> 00:07:10,840
So he went back to where the treasure was, and he was so pestered by her, that he didn't know what to do.

64
00:07:10,840 --> 00:07:16,840
He went back to the treasure and took one gold coin out, and he gave it, he brought it back and gave it.

65
00:07:16,840 --> 00:07:29,840
She took that gold coin, looked at it with some kind of older issue coin, and sent it off to the king, and used her own money as a royal servant to pay for the decorations.

66
00:07:29,840 --> 00:07:37,840
And again, she had to, later on, she had the king do the same thing again, and she pestered in the again to get money for the decorations, and he went and got through.

67
00:07:37,840 --> 00:07:42,840
Got, it says three more, but I don't think it's three more, because in total, there were only three.

68
00:07:42,840 --> 00:07:48,840
Anyway, in total, I think there were three gold coins that she sent off to the king.

69
00:07:48,840 --> 00:07:56,840
Then she went, then she went and told the king that he must have money, and there's something, the king can do what he wants.

70
00:07:56,840 --> 00:08:01,840
The king sent some men to bring him to the castle, and they tied him up, and they run to the castle, and the king said,

71
00:08:01,840 --> 00:08:06,840
what are you doing hiding all your wealth from the kingdom?

72
00:08:06,840 --> 00:08:09,840
Because normally you'd have to pay taxes, I guess.

73
00:08:09,840 --> 00:08:12,840
And he said, I have no wealth, where would I want to have wealth?

74
00:08:12,840 --> 00:08:19,840
I'm just an ordinary, I'm working, and as a day labor, and the king pulled out the three gold coins and then who are these?

75
00:08:19,840 --> 00:08:28,840
And he said, and he knew that the king was up, and he looked and he saw their servants, the royal servants had gone back, the mother and the daughter.

76
00:08:28,840 --> 00:08:32,840
And he knew that he was in trouble, and so he told the kingdom the king said, how much money do you have?

77
00:08:32,840 --> 00:08:33,840
And he said, there's much.

78
00:08:33,840 --> 00:08:45,840
The king sent a bunch of carriages, got the money, and see the thing was that they understood that it was, he told the whole story of how he had been a rich people's son,

79
00:08:45,840 --> 00:08:50,840
and that they had died and left the treasure to him, and he didn't want to take it.

80
00:08:50,840 --> 00:08:58,840
And in the umbrella of this gold, it was amazing at this man, because you've got to think that during that whole time, it's amazing that the greed didn't overcome,

81
00:08:58,840 --> 00:09:07,840
greed didn't overcome him, and he was able to live after being a rich, rich family son, and he was able to live as a poor man.

82
00:09:07,840 --> 00:09:20,840
And so the king was quite impressed, and he made him a royal treasure, a banker, whatever, and gave him his own daughter in marriage, or maybe this woman's daughter, it's not quite clear.

83
00:09:20,840 --> 00:09:24,840
And took him to see the Buddha, and said,

84
00:09:24,840 --> 00:09:43,840
one day, look at this man, the likes of him has never been seen before, even though he had so much money, he was never tempted to go and take it, and he was content to live and to work as an ordinary laborer for his life.

85
00:09:43,840 --> 00:10:03,840
So the Buddha's reply was these two verses, showing how a person who acts properly, a person who is conscientious, a person who is mindful, a person who knows the consequences of one's actions, for them glory increases.

86
00:10:03,840 --> 00:10:15,840
So by the sounds of it, it's actually quite a simple and mundane story, and it might actually seem to be encouraging us to live, to save up our wealth and so on.

87
00:10:15,840 --> 00:10:20,840
And for ordinary people, you can be seen in that way, and people living in the world can see the whom.

88
00:10:20,840 --> 00:10:30,840
When we work, we shouldn't become greedy, and become attached to our wealth, we should still work hard, even though we're wealthy, and we should be conscientious, and so many things here that are good in the world.

89
00:10:30,840 --> 00:10:41,840
Utano, if people should live in the world, should be hard working, set them up, of course, they have to be mindful, when they do things, they have to do it conscientiously.

90
00:10:41,840 --> 00:10:49,840
That's me, some God, you know, they have to be conscientious, and do things methodically, and do the right things.

91
00:10:49,840 --> 00:11:00,840
So they shouldn't be treacherous, and so on, and they should make their livelihood by pure means, not supposed to.

92
00:11:00,840 --> 00:11:18,840
Nowadays, you see people when they have mass lots of money, they tend to become very corrupted, and stingy, and so many things, and as a result, now you see so much discord in the world over these rich people who refuse to support, or who have basically cornered so much of the wealth.

93
00:11:18,840 --> 00:11:32,840
In Shady, and underhanded ways, by cheating, they're second by corrupting politicians, and driving politicians, and deregulation, and so on, and all of this.

94
00:11:32,840 --> 00:11:40,840
So this is what is called asu-ji-kamat, asu-ji-kamasa, impure deeds.

95
00:11:40,840 --> 00:11:49,840
So the Buddha said a person with fear deeds, this is how true glory comes about.

96
00:11:49,840 --> 00:12:07,840
This means a person who isn't arrogant, and isn't rash, so who acts in a mindful, really in a mindful way, but there isn't addicted to sensuality, always looking for new pleasures, and so on.

97
00:12:07,840 --> 00:12:19,840
And Damadhi, we know living by the Damma, so of course living by the Buddha's teaching, living by the truth, you could say, living truthfully, and living according to goodness and righteousness.

98
00:12:19,840 --> 00:12:43,840
Upamat-sasa, the person who overall, and really all of these things are a description of upamat-a-half, the person who is heedful and always unguired, for such a person, their glory increases. Yes, I guess I can just mean fame, or, like, glorious, I think a good translation, and it generally can mean simply worldly glory.

99
00:12:43,840 --> 00:12:50,840
But what we have to do here is think of who the Buddha was, and what he was bringing to the world.

100
00:12:50,840 --> 00:12:57,840
And obviously, he wasn't bringing people to teaching, yes, you should become bankers and treasures, and work hard to get the king's favor.

101
00:12:57,840 --> 00:13:00,840
So this obviously wasn't really the same.

102
00:13:00,840 --> 00:13:10,840
And in fact, if you look at it and you think about it, and you think about who has the true glory, of all the people in India, of all people who have come out of India,

103
00:13:10,840 --> 00:13:15,840
I can't think of one person in the history of India who has more glory than the Buddha.

104
00:13:15,840 --> 00:13:22,840
The second might be Gandhi who came much later, of course, but you'd have to think that of these two.

105
00:13:22,840 --> 00:13:34,840
There's no question that in all of India, of all the kings, or even the King Bimbisara, or King Ashoka who came later, none of them have even a part of the glory of the Buddha.

106
00:13:34,840 --> 00:13:40,840
So then you ask yourself, where does the true glory come from?

107
00:13:40,840 --> 00:13:55,840
And you can see that these virtues apply far more to someone like the Buddha, or to even to a Buddhist meditator than they do to this banker who acted in a very noble sort of way in a mundane sense.

108
00:13:55,840 --> 00:14:04,840
But it was actually quite a mundane sense. He was still very much, seems to be addicted to sensuality, falling for this woman's daughter.

109
00:14:04,840 --> 00:14:09,840
And if you read the story, it was, if you read the story, actually, he seems like a really nice guy at the time of the truth.

110
00:14:09,840 --> 00:14:19,840
When these women came to live in his home, and they started, first, what they did is they cut his bed so that when he sat on it, it fell apart.

111
00:14:19,840 --> 00:14:26,840
Because there was only one other bed, and that was where the daughter was sleeping. So you go and sleep with my daughter then. She called, she said,

112
00:14:26,840 --> 00:14:33,840
oh, son, you go and sleep with your sister. And of course, it didn't happen like that. It wasn't meant to happen like that.

113
00:14:33,840 --> 00:14:40,840
But what he said at the beginning, he said, you know, you guys were too much for me. When I was alone, I could come and go as I pleased.

114
00:14:40,840 --> 00:14:45,840
And now I've got all these troubles, now that you've come, which is really the truth.

115
00:14:45,840 --> 00:14:55,840
And as you take on a family and a mother-in-law and children and so on, so much headaches and problems come, which was what he was realizing.

116
00:14:55,840 --> 00:15:05,840
But he fell for it. But anyway, these qualities, you have to remember, the Buddha is not referring specifically to this example.

117
00:15:05,840 --> 00:15:10,840
He's using this example to show how this reality works.

118
00:15:10,840 --> 00:15:15,840
And it works for all people, most especially for us in meditation.

119
00:15:15,840 --> 00:15:20,840
So what we should do is look at this person in terms of its meditative applications.

120
00:15:20,840 --> 00:15:24,840
And we can use this guy. We can actually use this whole story as kind of an allegory.

121
00:15:24,840 --> 00:15:29,840
We can think of ourselves as working hard here in meditation.

122
00:15:29,840 --> 00:15:35,840
When you're working here, and actually you don't have, you don't have much salary.

123
00:15:35,840 --> 00:15:43,840
In the sense that you're practicing, and it seems like sometimes you're not getting much, and you're working really hard, and it's quite painful.

124
00:15:43,840 --> 00:15:44,840
Quite difficult.

125
00:15:44,840 --> 00:15:55,840
Well, at times, it seems like, oh, here, all I'm getting is bug bites, and not sleeping enough, and no pleasure, and no entertainment, and so on.

126
00:15:55,840 --> 00:15:59,840
None of that, and not even good food, and so on.

127
00:15:59,840 --> 00:16:05,840
So sometimes you think, oh, what's it all for? But what you're doing is you're developing all of these qualities.

128
00:16:05,840 --> 00:16:10,840
And as a result, you can look at what happens to someone with these qualities.

129
00:16:10,840 --> 00:16:16,840
This person eventually was recognized by a wise individual, a king, and a powerful individual.

130
00:16:16,840 --> 00:16:25,840
So you become recognized by the world, just as the Buddha became recognized by the world, and your glory.

131
00:16:25,840 --> 00:16:31,840
And we're going to put aside the idea of glory in the world, but here, in his case, glory in the world, came to him.

132
00:16:31,840 --> 00:16:33,840
But for you, glory in the Dhamma comes.

133
00:16:33,840 --> 00:16:35,840
So you become in the universe.

134
00:16:35,840 --> 00:16:37,840
And in fact, that's really what it is.

135
00:16:37,840 --> 00:16:43,840
The whole of the universe becomes, or reacts to your greatness.

136
00:16:43,840 --> 00:16:47,840
And so many people who practice meditation will go on to become angels.

137
00:16:47,840 --> 00:16:51,840
We're going to heaven, or that's according to the text, anyway.

138
00:16:51,840 --> 00:16:57,840
But even in this life, you become a great teacher, you become someone who people listen to, someone who people respect.

139
00:16:57,840 --> 00:17:03,840
The people who you find yourself surrounded by are good people who respect what you have to say, who listen to you,

140
00:17:03,840 --> 00:17:07,840
who care for you, who support you, and so on.

141
00:17:07,840 --> 00:17:09,840
Because you have these things.

142
00:17:09,840 --> 00:17:12,840
So what are we doing in meditation to develop this?

143
00:17:12,840 --> 00:17:17,840
Well, the first one that we're developing is the Thāṇa, which means effort.

144
00:17:17,840 --> 00:17:22,840
So the first quality of meditation, this is what you gain from things like walking meditation,

145
00:17:22,840 --> 00:17:23,840
or even just sitting.

146
00:17:23,840 --> 00:17:28,840
The effort to stay still, and the effort to stay with the object and not run away from them,

147
00:17:28,840 --> 00:17:32,840
and not avoid reality.

148
00:17:32,840 --> 00:17:37,840
Just the effort, just that effort to not fall into liking or disliking.

149
00:17:37,840 --> 00:17:39,840
Even the effort just to say to yourself,

150
00:17:39,840 --> 00:17:48,840
you're rising, falling, or say to yourself, sitting, or seeing, or hearing, or pain, or aching, or so on.

151
00:17:48,840 --> 00:17:55,840
And just simply that effort, it has a profound effect on your mind and on your whole being.

152
00:17:55,840 --> 00:17:59,840
Some people think that practicing here, you're not getting any exercise.

153
00:17:59,840 --> 00:18:01,840
But in fact, you're getting an incredible exercise.

154
00:18:01,840 --> 00:18:04,840
And your whole being is becoming stronger.

155
00:18:04,840 --> 00:18:11,840
You'll find that when you leave, even after doing the foundation course, that your mind is all of the wavering in your mind,

156
00:18:11,840 --> 00:18:13,840
or much of the wavering in your mind is gone.

157
00:18:13,840 --> 00:18:15,840
And you become a more powerful individual.

158
00:18:15,840 --> 00:18:19,840
And this is something that people respect, that the world respects,

159
00:18:19,840 --> 00:18:21,840
that the whole universe reacts to.

160
00:18:21,840 --> 00:18:26,840
And of course, internally, you become much more peaceful and much more at ease,

161
00:18:26,840 --> 00:18:31,840
at much more in tune with reality.

162
00:18:31,840 --> 00:18:35,840
So we have to be someone who develops mindfulness.

163
00:18:35,840 --> 00:18:39,840
So this is the practice that we're using the effort for.

164
00:18:39,840 --> 00:18:43,840
So the effort is to develop mindfulness, really.

165
00:18:43,840 --> 00:18:45,840
We have this effort and we're developing mindfulness.

166
00:18:45,840 --> 00:18:51,840
We put those two together, means having effort in being mindful.

167
00:18:51,840 --> 00:18:53,840
Then you really have the essence of the practice.

168
00:18:53,840 --> 00:18:57,840
Because concentration is something that comes by itself when you have these two.

169
00:18:57,840 --> 00:18:59,840
They surround concentration.

170
00:18:59,840 --> 00:19:01,840
So you want to develop right concentration.

171
00:19:01,840 --> 00:19:02,840
You have effort.

172
00:19:02,840 --> 00:19:04,840
You have directed effort.

173
00:19:04,840 --> 00:19:06,840
So it's not just working hard like this mandate.

174
00:19:06,840 --> 00:19:14,840
But in terms of meditation, it's working hard to work hard in a focused manner.

175
00:19:14,840 --> 00:19:18,840
Well, this man, of course, actually the truth is he was focused.

176
00:19:18,840 --> 00:19:22,840
He was working hard and he had a goal and he knew his way.

177
00:19:22,840 --> 00:19:25,840
And he knew the reasons for doing what he did.

178
00:19:25,840 --> 00:19:29,840
So in a sense, he was mindful.

179
00:19:29,840 --> 00:19:34,840
But what we're doing is even more so knowing what we're doing and understanding the reason

180
00:19:34,840 --> 00:19:41,840
and understanding the practice, understanding everything we do when you go to eat.

181
00:19:41,840 --> 00:19:46,840
For example, you understand wanting to eat and wanting to take, you understand the desires and so on.

182
00:19:46,840 --> 00:19:50,840
But that makes you eat too much, for example.

183
00:19:50,840 --> 00:19:54,840
So you understand cause and effect.

184
00:19:54,840 --> 00:20:00,840
So basically you have mindfulness and you're aware of things in an alternate sense.

185
00:20:00,840 --> 00:20:03,840
You're aware of every movement, every experience.

186
00:20:03,840 --> 00:20:07,840
And when you're walking, walking, walking, walking, you know it is walking.

187
00:20:07,840 --> 00:20:10,840
When rising, when rising, when pain, you know it is pain.

188
00:20:10,840 --> 00:20:13,840
This is mindfulness.

189
00:20:13,840 --> 00:20:18,840
So you have pure actions.

190
00:20:18,840 --> 00:20:24,840
And so this might sound like a person, like a thing, teaching for people living in the world.

191
00:20:24,840 --> 00:20:29,840
And because it means you don't cheat others and you don't lie and you don't steal.

192
00:20:29,840 --> 00:20:34,840
But it is on a much more profound level for someone who's practicing meditation.

193
00:20:34,840 --> 00:20:38,840
Because when you're practicing meditation, everything you do becomes pure.

194
00:20:38,840 --> 00:20:41,840
Or you're purifying everything you do, even eating.

195
00:20:41,840 --> 00:20:44,840
An ordinary person when they eat, they don't think about it.

196
00:20:44,840 --> 00:20:48,840
They eat is a great to eat. It's a silly question, of course.

197
00:20:48,840 --> 00:20:49,840
There's nothing wrong with eating.

198
00:20:49,840 --> 00:20:52,840
I bought this food, I made this food and it's mine.

199
00:20:52,840 --> 00:20:53,840
I can eat it.

200
00:20:53,840 --> 00:20:58,840
So they just eat it and they think that's suchikama, suchikama, that's a pure karma.

201
00:20:58,840 --> 00:21:01,840
But as a meditator, we know that that's actually not the case.

202
00:21:01,840 --> 00:21:04,840
Even though it's yours and you bought it and so on, it can still be an evil act.

203
00:21:04,840 --> 00:21:11,840
In the sense that you can still become addicted to it and evil, in the sense that it's going to create addiction and suffering for you.

204
00:21:11,840 --> 00:21:16,840
Even just eating food, we can see how much we become addicted and we lose our whole mindfulness.

205
00:21:16,840 --> 00:21:19,840
And forget what we're doing and our mind goes somewhere else.

206
00:21:19,840 --> 00:21:23,840
And maybe if it's bad food, we get upset and angry and don't like it.

207
00:21:23,840 --> 00:21:28,840
And we can see the evil for us in that, that it causes us suffering.

208
00:21:28,840 --> 00:21:32,840
And causes us to do all sorts of nasty things as well.

209
00:21:32,840 --> 00:21:35,840
So the purity that we're gaining is on a whole other level.

210
00:21:35,840 --> 00:21:40,840
The purity that you're gaining in your practice is something, it's exactly what the Buddha was referring to.

211
00:21:40,840 --> 00:21:43,840
And we have to be careful not to miss that.

212
00:21:43,840 --> 00:21:50,840
That the purity of karma doesn't come from intellectually thinking this is right, this is wrong, or even in terms of just the five precepts.

213
00:21:50,840 --> 00:21:52,840
It's on a much more profound level.

214
00:21:52,840 --> 00:21:56,840
Not doing anything with greed or anger or delusions.

215
00:21:56,840 --> 00:22:10,840
You need some person who acts conscientiously or aware of knowing the right way of acting.

216
00:22:10,840 --> 00:22:12,840
So this means knowing what you're doing when you act.

217
00:22:12,840 --> 00:22:16,840
It's in the same sense of having doing the right thing.

218
00:22:16,840 --> 00:22:20,840
Because when you're mindful, you're aware clearly what you're doing is just the right thing to do.

219
00:22:20,840 --> 00:22:22,840
Am I doing it for the right reason?

220
00:22:22,840 --> 00:22:24,840
You do it in the right way.

221
00:22:24,840 --> 00:22:32,840
So when you walk, normally when you walk, you're walking kind of mindlessly.

222
00:22:32,840 --> 00:22:40,840
And so as a result, you might trip them fall or find that your body becomes tense as a result of your actions.

223
00:22:40,840 --> 00:22:50,840
What you find in meditation is actually a lot of our tension and all of our stress is a result of the way we act and the way we move our bodies.

224
00:22:50,840 --> 00:22:56,840
So you come to refine your movement, you're going to refine your actions, even our speech.

225
00:22:56,840 --> 00:23:02,840
You'll find that we're speaking in a more clear way and we're speaking less.

226
00:23:02,840 --> 00:23:10,840
We find ourselves not going on and on the mindless things and things that are unrelated to mental development.

227
00:23:10,840 --> 00:23:13,840
And so on we find ourselves less interested in it.

228
00:23:13,840 --> 00:23:15,840
We see that it's causing us stress and suffering.

229
00:23:15,840 --> 00:23:22,840
So our actions and our speech and even our thoughts become much more clear and much more careful.

230
00:23:22,840 --> 00:23:29,840
That's what this means, acting carefully, which comes directly from meditation.

231
00:23:29,840 --> 00:23:33,840
Sunyatasa, we have Sunyatasa.

232
00:23:33,840 --> 00:23:44,840
We're developing this subdued nature or controlled in the sense that we're not letting ourselves go out of control.

233
00:23:44,840 --> 00:23:48,840
It doesn't mean you have to force yourself not to see things or hear things.

234
00:23:48,840 --> 00:24:00,840
It means when you see things, you're controlled in terms of just knowing it that it's seeing and not letting yourself get caught up in enjoying beautiful things or being angry at unpleasant things and so on.

235
00:24:00,840 --> 00:24:05,840
Falling into this partiality that brings you so much out of balance.

236
00:24:05,840 --> 00:24:16,840
So the wording is kind of in terms of having a livelihood that is righteous and so it does accord with the story.

237
00:24:16,840 --> 00:24:24,840
But you can never compare the life of a banker to the life of a Buddhist meditative, for example.

238
00:24:24,840 --> 00:24:32,840
Someone with a Buddha is really trying to talk about here and he's trying to explain to the king that it's not this person what he did that's so great.

239
00:24:32,840 --> 00:24:40,840
It's his qualities of mind which are which are to be found so much more in the meditative and the person who is practicing meditation.

240
00:24:40,840 --> 00:24:45,840
So one who lives by the dhamma, one who lives righteously.

241
00:24:45,840 --> 00:25:01,840
If you look at the way we're living here, living in a way that is cultivating goodness and developing peace and tranquility and is avoiding so much of the, we're building a society here in a sense, even in just the monastery like this.

242
00:25:01,840 --> 00:25:06,840
You're building a society that has so much, you know, there's no weapons of war, right?

243
00:25:06,840 --> 00:25:07,840
There's no drugs.

244
00:25:07,840 --> 00:25:09,840
There's no drugs.

245
00:25:09,840 --> 00:25:14,840
Sometimes the villagers bringing their red stuff, red juice stuff, trying to keep it out.

246
00:25:14,840 --> 00:25:27,840
There's no, there's so many, so much of society is no sensuality, no entertainment, none of this Hollywood addiction, addiction to sensuality or pleasure or entertainment.

247
00:25:27,840 --> 00:25:36,840
Beautification, all of this, so much of the things that bring, that are bringing the world down and are bringing chaos to the world.

248
00:25:36,840 --> 00:25:48,840
We're doing away with, we're developing a way of life that is pure, that is according to the truth and reality and harmony and peace.

249
00:25:48,840 --> 00:25:57,840
More, more over, we are developing this for the world and we're creating, you know, we're spreading this teaching.

250
00:25:57,840 --> 00:26:04,840
So when people come, they're able to see this and they're able to take the example back and they're able to use teachings like this.

251
00:26:04,840 --> 00:26:09,840
For example, when you record the teachings, you know, just our living by the dhamma and sitting, you're talking about it.

252
00:26:09,840 --> 00:26:16,840
It's something that helps the world because we're spreading because by talking about it, we let other people know we're talking.

253
00:26:16,840 --> 00:26:19,840
I think that other people know that I'm saying this and they're nothing.

254
00:26:19,840 --> 00:26:24,840
They'll think of this man who worked in a right way and they'll think about that.

255
00:26:24,840 --> 00:26:32,840
That's on a level that I can deal with that and they'll live their lives conscientiously and according to the truth, according to goodness.

256
00:26:32,840 --> 00:26:40,840
They won't become greedy and self-serving and when they do have wealth, they won't become obsessed by it.

257
00:26:40,840 --> 00:26:52,840
And in this way, in all of these ways, they will become a pamata, you know, become peaceful or vigilant, not negligent, not heedless.

258
00:26:52,840 --> 00:27:00,840
And as I said yesterday, really the meaning of heatfulness is mindfulness.

259
00:27:00,840 --> 00:27:12,840
Remember the said, there's at one point there's this, a quote, Satya, a repilasso, a pamata, a pamata, a woodshifting, a person who is never without mindfulness.

260
00:27:12,840 --> 00:27:17,840
This is the meaning of one who is heedful or not negligent.

261
00:27:17,840 --> 00:27:25,840
But when you put all of these things together, you get an idea of what it means to be heedful, a person who is conscientious, a person who is subdued.

262
00:27:25,840 --> 00:27:36,840
Most importantly, a person who is mindful and full of effort, never giving up, never becoming lazy, never slacking off and always trying and trying to better themselves.

263
00:27:36,840 --> 00:27:41,840
No matter how good they are as a person, always trying to make themselves a better person.

264
00:27:41,840 --> 00:27:45,840
This is a person who is a pamata, a pamata.

265
00:27:45,840 --> 00:27:49,840
And what happens for such a person, their glory ever increases.

266
00:27:49,840 --> 00:28:03,840
So he's not trying to say that the purpose, our purpose should be without glory. He's just observing that what this man has done by putting into practice these traits.

267
00:28:03,840 --> 00:28:09,840
The practice of these traits is what has brought this fruit.

268
00:28:09,840 --> 00:28:12,840
It is because of these things that this fruit has come.

269
00:28:12,840 --> 00:28:15,840
So he's explaining what is most important, which is these virtues.

270
00:28:15,840 --> 00:28:22,840
Actually, so many things, good things come from there. He's not trying to say the only good things that you become glorious.

271
00:28:22,840 --> 00:28:27,840
But glory is most reserved for those who have these traits.

272
00:28:27,840 --> 00:28:32,840
People who work hard and work in righteousness and do good things for the world.

273
00:28:32,840 --> 00:28:37,840
As we can see that the Buddha did and people like Gandhi or so on, we tried to help people.

274
00:28:37,840 --> 00:28:44,840
We're said to have tried to do good things and bring peace and happiness and harmony and prosperity.

275
00:28:44,840 --> 00:28:57,840
And glory in the sense of goodness and raising up people's consciousness and people's lives to happiness and peace.

276
00:28:57,840 --> 00:29:03,840
So this is what leads to the Buddha's teaching on what leads to Yasa, what leads to glory.

277
00:29:03,840 --> 00:29:08,840
That's verse number 24, and that's done upon the teaching for today.

278
00:29:08,840 --> 00:29:11,840
We had one for our meditation as well. Very good one.

279
00:29:11,840 --> 00:29:14,840
Then one from the upper mandavaga.

280
00:29:14,840 --> 00:29:41,840
So, thanks for tuning in and back to meditation.

