1
00:00:00,000 --> 00:00:29,200
One, one gets to live, May 25th, this quote is somewhat suspect.

2
00:00:29,200 --> 00:00:39,200
He looks like he's mostly translated something problematic.

3
00:00:39,200 --> 00:00:42,200
This is the word love here.

4
00:00:42,200 --> 00:00:52,200
As far as I can see, the only instance of this quote doesn't use the word love.

5
00:00:52,200 --> 00:01:02,200
This is the word patissantara, which some reason I think we've already had as a quote.

6
00:01:02,200 --> 00:01:06,200
I think at some point we had this list of six.

7
00:01:06,200 --> 00:01:11,200
Maybe I'm wrong.

8
00:01:11,200 --> 00:01:12,200
Pretty sure though.

9
00:01:12,200 --> 00:01:13,200
We got a lot.

10
00:01:13,200 --> 00:01:18,200
We talked about got a lot.

11
00:01:18,200 --> 00:01:21,200
Got a lot.

12
00:01:21,200 --> 00:01:25,200
It has the backstory of an angel coming to see the Buddha.

13
00:01:25,200 --> 00:01:29,200
Telling him about these six things.

14
00:01:29,200 --> 00:01:34,200
And the Buddha affirming that it's indeed true.

15
00:01:34,200 --> 00:01:38,200
Garawata.

16
00:01:38,200 --> 00:01:41,200
Six kinds of respect.

17
00:01:41,200 --> 00:01:45,200
Garawata comes from the root.

18
00:01:45,200 --> 00:01:48,200
The root would be gar, I guess.

19
00:01:48,200 --> 00:01:55,200
It's where the word guru comes from garu.

20
00:01:55,200 --> 00:01:58,200
And probably garu means heavy.

21
00:01:58,200 --> 00:02:04,200
Someone who is important.

22
00:02:04,200 --> 00:02:07,200
A teacher, garu is a teacher.

23
00:02:07,200 --> 00:02:10,200
The guru.

24
00:02:10,200 --> 00:02:13,200
But garu also means heavy.

25
00:02:13,200 --> 00:02:19,200
So it means taking something serious and garuata means taking something seriously.

26
00:02:19,200 --> 00:02:27,200
And there are six things that according to this angel and then according to the Buddha,

27
00:02:27,200 --> 00:02:37,200
are important for not falling away, for not declining.

28
00:02:37,200 --> 00:02:44,200
We want to progress in this life.

29
00:02:44,200 --> 00:02:45,200
We want to succeed.

30
00:02:45,200 --> 00:02:51,200
We're talking specifically about spirituality and Buddhism in the Buddha's teaching.

31
00:02:51,200 --> 00:02:53,200
What is it that leads to success?

32
00:02:53,200 --> 00:02:56,200
What is it that leads to progress?

33
00:02:56,200 --> 00:02:59,200
Now there are these six things.

34
00:02:59,200 --> 00:03:01,200
The number six I don't think is as translated.

35
00:03:01,200 --> 00:03:08,200
So let's go through them.

36
00:03:08,200 --> 00:03:10,200
So the Buddha.

37
00:03:10,200 --> 00:03:17,200
Having respect for and taking the Buddha seriously.

38
00:03:17,200 --> 00:03:24,200
So people traditionally will take the Buddha so seriously that they make images of the Buddha

39
00:03:24,200 --> 00:03:26,200
and they bow down to them and they give offerings.

40
00:03:26,200 --> 00:03:29,200
That's so seriously they take the Buddha.

41
00:03:29,200 --> 00:03:32,200
Some might argue a little too seriously.

42
00:03:32,200 --> 00:03:40,200
But the most important is appreciating the Buddha.

43
00:03:40,200 --> 00:03:47,200
And respecting his enlightenment and respecting his state, his perfection,

44
00:03:47,200 --> 00:03:51,200
his greatness.

45
00:03:51,200 --> 00:03:57,200
It's important because it keeps you in line with his teachings.

46
00:03:57,200 --> 00:04:02,200
It's easy for the mind to get lost, for the mind to lose its way.

47
00:04:02,200 --> 00:04:11,200
And if you don't take the Buddha seriously, your mind is very easily diverted and caught up by other things.

48
00:04:11,200 --> 00:04:14,200
Number two, of course, Dhamma Garawata.

49
00:04:14,200 --> 00:04:16,200
Number three, Sangha Garawata.

50
00:04:16,200 --> 00:04:19,200
So the Buddha, the Dhamman, the Sangha.

51
00:04:19,200 --> 00:04:21,200
We should take them seriously.

52
00:04:21,200 --> 00:04:25,200
The Dhamma of the Buddha refers to his teaching.

53
00:04:25,200 --> 00:04:27,200
We should take them seriously.

54
00:04:27,200 --> 00:04:35,200
We should not look lightly upon the meditation practice and the teachings.

55
00:04:35,200 --> 00:04:41,200
We should appreciate them.

56
00:04:41,200 --> 00:04:51,200
Some people will be so appreciative that they don't put books about the Buddha's teaching on the floor.

57
00:04:51,200 --> 00:04:56,200
So they'll keep them up high.

58
00:04:56,200 --> 00:05:00,200
And treat them books with respect.

59
00:05:00,200 --> 00:05:04,200
And so some might argue that that's not really necessary.

60
00:05:04,200 --> 00:05:08,200
But it's all about the sense of respect.

61
00:05:08,200 --> 00:05:14,200
So treating the Buddha in the Dhamman this way is useful for cultivating wholesome mind states,

62
00:05:14,200 --> 00:05:24,200
when states that are conducive to practice and deserve to progress.

63
00:05:24,200 --> 00:05:29,200
We...

64
00:05:29,200 --> 00:05:32,200
The most important is that we respect the teaching.

65
00:05:32,200 --> 00:05:35,200
We appreciate how important it is.

66
00:05:35,200 --> 00:05:41,200
Sometimes studying the Buddha's teaching is good because not just for the knowledge it gives you,

67
00:05:41,200 --> 00:05:44,200
but because it gives you a deeper speck.

68
00:05:44,200 --> 00:05:47,200
So that you don't have to doubt what little you know.

69
00:05:47,200 --> 00:05:54,200
If you know a lot more, it can help to solidify your practice and make it clear the bigger picture.

70
00:05:54,200 --> 00:05:55,200
Why are we doing this?

71
00:05:55,200 --> 00:05:57,200
Why is it important to let go?

72
00:05:57,200 --> 00:06:00,200
Why is it wrong to clean?

73
00:06:00,200 --> 00:06:04,200
Sometimes studying is useful.

74
00:06:04,200 --> 00:06:09,200
But we could also talk about appreciation of meditation practice.

75
00:06:09,200 --> 00:06:12,200
Taking that seriously.

76
00:06:12,200 --> 00:06:17,200
I think it just means the teachings of doctrine.

77
00:06:17,200 --> 00:06:19,200
Number three, the sangha.

78
00:06:19,200 --> 00:06:23,200
So appreciating teachers, appreciating enlightened beings even if they don't teach,

79
00:06:23,200 --> 00:06:26,200
just appreciating their enlightenment.

80
00:06:26,200 --> 00:06:31,200
They're appreciating looking up to someone who you'd like to emulate.

81
00:06:31,200 --> 00:06:38,200
When you take them seriously, it becomes easy to emulate them to follow their lead.

82
00:06:38,200 --> 00:06:41,200
To follow the way that they've gone.

83
00:06:41,200 --> 00:06:47,200
Follow in their footsteps as well.

84
00:06:47,200 --> 00:06:51,200
Number four, Sika Garawata, the training.

85
00:06:51,200 --> 00:06:53,200
Taking the training seriously.

86
00:06:53,200 --> 00:06:55,200
Training in morality.

87
00:06:55,200 --> 00:06:58,200
You're taking the precepts seriously.

88
00:06:58,200 --> 00:07:00,200
Training in concentration.

89
00:07:00,200 --> 00:07:03,200
So taking meditation seriously.

90
00:07:03,200 --> 00:07:08,200
And training in wisdom.

91
00:07:08,200 --> 00:07:09,200
So taking wisdom seriously.

92
00:07:09,200 --> 00:07:13,200
The practice of insight meditation.

93
00:07:13,200 --> 00:07:15,200
So it shouldn't be a hobby.

94
00:07:15,200 --> 00:07:20,200
There shouldn't be something that changes your life.

95
00:07:20,200 --> 00:07:22,200
It should be a part of your life.

96
00:07:22,200 --> 00:07:25,200
Every day meditating, every day being mindful,

97
00:07:25,200 --> 00:07:26,200
whatever you do.

98
00:07:26,200 --> 00:07:28,200
When you eat, eat mindfully.

99
00:07:28,200 --> 00:07:32,200
When you brush your teeth, brush your teeth mindfully.

100
00:07:32,200 --> 00:07:35,200
When you use the toilet, use the toilet mindfully.

101
00:07:35,200 --> 00:07:40,200
When you lie down to sleep, lie down to sleep mindfully.

102
00:07:40,200 --> 00:07:44,200
Take it seriously.

103
00:07:44,200 --> 00:07:47,200
Number five, upper manda and Garawata.

104
00:07:47,200 --> 00:07:50,200
So this is a heedfulness.

105
00:07:50,200 --> 00:07:55,200
Upper manda is a trademark teaching of the Buddha.

106
00:07:55,200 --> 00:07:59,200
It's very core teaching.

107
00:07:59,200 --> 00:08:02,200
It means being mindful, being vigilant,

108
00:08:02,200 --> 00:08:05,200
being constantly aware,

109
00:08:05,200 --> 00:08:09,200
consistent, continuously.

110
00:08:09,200 --> 00:08:12,200
If you don't have continuity in your practice,

111
00:08:12,200 --> 00:08:14,200
it's like two steps forward.

112
00:08:14,200 --> 00:08:15,200
One step back.

113
00:08:15,200 --> 00:08:16,200
It slows you down.

114
00:08:16,200 --> 00:08:20,200
It can even take you backwards if you're not consistent enough.

115
00:08:20,200 --> 00:08:23,200
You should try to be vigilant,

116
00:08:23,200 --> 00:08:29,200
heedful, lose sight of what's right, what's good, what's positive,

117
00:08:29,200 --> 00:08:32,200
don't get lost and negative.

118
00:08:36,200 --> 00:08:39,200
And number six, Batisantara.

119
00:08:39,200 --> 00:08:41,200
Batisantara, this problematic word,

120
00:08:41,200 --> 00:08:44,200
which doesn't mean love.

121
00:08:44,200 --> 00:08:48,200
Batisantara, this problematic word,

122
00:08:48,200 --> 00:08:51,200
which doesn't mean love.

123
00:08:51,200 --> 00:08:56,200
You don't really have meaning, but Tara,

124
00:08:56,200 --> 00:08:58,200
stir comes from,

125
00:08:58,200 --> 00:09:01,200
says literally spreading before.

126
00:09:01,200 --> 00:09:07,200
Batisantara is understood to refer to hospitality.

127
00:09:07,200 --> 00:09:09,200
There's two kinds of Batisantara,

128
00:09:09,200 --> 00:09:11,200
Aamisant Batisantara,

129
00:09:11,200 --> 00:09:13,200
and Tama Batisantara.

130
00:09:13,200 --> 00:09:15,200
So they don't have anything to do with love,

131
00:09:15,200 --> 00:09:16,200
not directly.

132
00:09:16,200 --> 00:09:23,200
And goodwill, perhaps, friendlyness, but hospitality is probably the way.

133
00:09:23,200 --> 00:09:26,200
So when you put things out for strangers,

134
00:09:26,200 --> 00:09:29,200
one is when someone,

135
00:09:29,200 --> 00:09:32,200
so when people come to visit you're being hospitable.

136
00:09:32,200 --> 00:09:34,200
It's curious that the Buddha uses this,

137
00:09:34,200 --> 00:09:38,200
but it is,

138
00:09:38,200 --> 00:09:43,200
it is what keeps the religion alive,

139
00:09:43,200 --> 00:09:45,200
keeps Buddhism alive.

140
00:09:45,200 --> 00:09:49,200
Good will, so goodwill to others,

141
00:09:49,200 --> 00:09:52,200
sharing with others,

142
00:09:52,200 --> 00:09:57,200
receiving others hospitable, hospitably.

143
00:09:57,200 --> 00:10:00,200
Aamisantara is with gifts,

144
00:10:00,200 --> 00:10:03,200
with physical things, with people that need food,

145
00:10:03,200 --> 00:10:06,200
giving them food when people need shelter,

146
00:10:06,200 --> 00:10:10,200
giving them shelter, this kind of thing.

147
00:10:10,200 --> 00:10:16,200
Dama Batisantara means offering the Dama, offering teachings,

148
00:10:16,200 --> 00:10:18,200
sharing the teachings with others,

149
00:10:18,200 --> 00:10:21,200
in order to be a teacher to share teachings.

150
00:10:21,200 --> 00:10:27,200
You can explain or demonstrate meditation practice,

151
00:10:27,200 --> 00:10:34,200
just as goodwill as a gift.

152
00:10:34,200 --> 00:10:40,200
As a help for others.

153
00:10:40,200 --> 00:10:42,200
So taking that seriously is,

154
00:10:42,200 --> 00:10:45,200
all of these are said to be important.

155
00:10:45,200 --> 00:10:47,200
And the Buddha says,

156
00:10:47,200 --> 00:11:06,200
they are not capable of fading away.

157
00:11:06,200 --> 00:11:21,200
So Buddhism sort of has a rap for being fairly dour and serious.

158
00:11:21,200 --> 00:11:25,200
And here's more about taking things seriously.

159
00:11:25,200 --> 00:11:27,200
Just because you take something seriously,

160
00:11:27,200 --> 00:11:30,200
it doesn't mean you have to be dour or serious.

161
00:11:30,200 --> 00:11:33,200
And you have to be conscientious.

162
00:11:33,200 --> 00:11:37,200
I think it's a very Buddhist word, conscientious.

163
00:11:37,200 --> 00:11:40,200
Don't be lazy, don't be negligent,

164
00:11:40,200 --> 00:11:45,200
don't be flippant or lax days ago.

165
00:11:45,200 --> 00:11:52,200
Be conscientious, mindful.

166
00:11:52,200 --> 00:11:55,200
These are good words.

167
00:11:55,200 --> 00:11:58,200
So again, I think we've already had this quote,

168
00:11:58,200 --> 00:12:00,200
so that's very familiar.

169
00:12:00,200 --> 00:12:03,200
I mean, I know this quote from my own studies,

170
00:12:03,200 --> 00:12:07,200
but it seems like we've gone through this recently.

171
00:12:07,200 --> 00:12:12,200
So that's the Dhamma for this evening.

172
00:12:12,200 --> 00:12:15,200
We have questions.

173
00:12:15,200 --> 00:12:18,200
People have questions about our site.

174
00:12:18,200 --> 00:12:20,200
The sites can undergo a big change,

175
00:12:20,200 --> 00:12:25,200
so let's not worry too much about it yet.

176
00:12:25,200 --> 00:12:28,200
Once we change over to the new format,

177
00:12:28,200 --> 00:12:35,200
we've got an IT team diligently working on this site.

178
00:12:35,200 --> 00:12:40,200
So hopefully that'll all come together this summer.

179
00:12:40,200 --> 00:13:04,200
No questions today?

180
00:13:04,200 --> 00:13:10,200
No, we don't have to have lots of questions every day.

181
00:13:10,200 --> 00:13:11,200
There will be questions.

182
00:13:11,200 --> 00:13:19,200
There will be more questions.

183
00:13:19,200 --> 00:13:20,200
When the Lord said,

184
00:13:20,200 --> 00:13:43,200
I don't know the mind of the Buddha.

185
00:13:43,200 --> 00:13:50,200
I would venture that he meant all of all some deeds,

186
00:13:50,200 --> 00:13:53,200
because Buddha's do teach that which is not directly related

187
00:13:53,200 --> 00:13:56,200
to the full number of paths, not directly.

188
00:13:56,200 --> 00:14:01,200
Of course, you can argue that all good deeds are supportive of the full number of paths.

189
00:14:01,200 --> 00:14:04,200
I would say it's more in that case,

190
00:14:04,200 --> 00:14:09,200
Buddha's teach things that are supportive,

191
00:14:09,200 --> 00:14:12,200
but not directly a part of the full number of paths.

192
00:14:12,200 --> 00:14:16,200
I mean, charity is not in the full mobile path,

193
00:14:16,200 --> 00:14:19,200
but it is supportive of the full mobile path.

194
00:14:19,200 --> 00:14:21,200
And Buddha's do teach it.

195
00:14:21,200 --> 00:14:29,200
So on the other hand, the word Upa sampada,

196
00:14:29,200 --> 00:14:35,200
you could argue means referring to the...

197
00:14:35,200 --> 00:14:37,200
It's not just saying,

198
00:14:37,200 --> 00:14:42,200
perform most of the deeds and say, get to the pinnacle,

199
00:14:42,200 --> 00:14:49,200
get to the highest, become complete, become full.

200
00:14:49,200 --> 00:14:56,200
And so there it's actually specifically referring to the full mobile path.

201
00:14:56,200 --> 00:15:01,200
I would say.

202
00:15:01,200 --> 00:15:07,200
The difficult questions you give me. Good, keep me on my toes.

203
00:15:07,200 --> 00:15:09,200
Can you talk about restraint?

204
00:15:09,200 --> 00:15:13,200
How much? And in what way should one restrain oneself?

205
00:15:13,200 --> 00:15:17,200
There's a good quote of the Buddha that I can't remember exactly where it is.

206
00:15:17,200 --> 00:15:20,200
Mahasi Sayyada talks about it.

207
00:15:20,200 --> 00:15:24,200
Restraint. Sunyada is restraint, or it's one word.

208
00:15:24,200 --> 00:15:33,200
Sunyada means restraint.

209
00:15:33,200 --> 00:15:35,200
Sunyada means restraint.

210
00:15:35,200 --> 00:15:40,200
Restrained.

211
00:15:40,200 --> 00:15:46,200
Restrained hands.

212
00:15:46,200 --> 00:15:49,200
Restrained of the feet.

213
00:15:49,200 --> 00:15:53,200
Restrained to the body of the self.

214
00:15:53,200 --> 00:16:00,200
All kinds of restraint are good.

215
00:16:00,200 --> 00:16:02,200
So there are different kinds.

216
00:16:02,200 --> 00:16:06,200
He talks about the five things, the five sorts of restraint.

217
00:16:06,200 --> 00:16:09,200
Restrained through morality.

218
00:16:09,200 --> 00:16:11,200
Restrained through effort.

219
00:16:11,200 --> 00:16:14,200
Restrained through knowledge.

220
00:16:14,200 --> 00:16:17,200
Restrained through mindfulness and restraint through wisdom.

221
00:16:17,200 --> 00:16:20,200
I think those are the five.

222
00:16:20,200 --> 00:16:23,200
So there are different kinds of restraint.

223
00:16:23,200 --> 00:16:27,200
The word restraint kind of implies forcing.

224
00:16:27,200 --> 00:16:29,200
But that's not really it.

225
00:16:29,200 --> 00:16:30,200
You think of these five.

226
00:16:30,200 --> 00:16:35,200
You restrain yourself in different ways with morality, with just by.

227
00:16:35,200 --> 00:16:37,200
And that's kind of forcing.

228
00:16:37,200 --> 00:16:39,200
That's kind of the courses way.

229
00:16:39,200 --> 00:16:42,200
Just saying no, no, no.

230
00:16:42,200 --> 00:16:45,200
I'm not going to do that, I'm not going to do that.

231
00:16:45,200 --> 00:16:50,200
But then so with effort.

232
00:16:50,200 --> 00:16:54,200
Maybe somehow related morality and effort.

233
00:16:54,200 --> 00:16:57,200
But effort can also refer to the effort in meditation.

234
00:16:57,200 --> 00:17:02,200
So if you work hard in like summit to meditation.

235
00:17:02,200 --> 00:17:03,200
I think it's effort.

236
00:17:03,200 --> 00:17:06,200
I'm actually, okay, right, remember.

237
00:17:06,200 --> 00:17:10,200
Media.

238
00:17:10,200 --> 00:17:13,200
Media somewhat I think.

239
00:17:13,200 --> 00:17:15,200
Yeah, with knowledge.

240
00:17:15,200 --> 00:17:16,200
No, it's not knowledge.

241
00:17:16,200 --> 00:17:17,200
Yeah, in the sun water.

242
00:17:17,200 --> 00:17:18,200
So it's not fun.

243
00:17:18,200 --> 00:17:19,200
Yeah, it's not the fifth one.

244
00:17:19,200 --> 00:17:20,200
There's five of them.

245
00:17:20,200 --> 00:17:22,200
Can't remember the more.

246
00:17:22,200 --> 00:17:26,200
Should know them.

247
00:17:26,200 --> 00:17:30,200
With mindfulness is probably the most important.

248
00:17:30,200 --> 00:17:34,200
So not exactly forcing yourself.

249
00:17:34,200 --> 00:17:37,200
But straightening your mind.

250
00:17:37,200 --> 00:17:41,200
So when the mind is, when the mind experiences something,

251
00:17:41,200 --> 00:17:43,200
things see it clearly.

252
00:17:43,200 --> 00:17:47,200
When you feel pain and you say to yourself, pain, pain.

253
00:17:47,200 --> 00:17:51,200
You are restraining yourself in the sense of you're diverting

254
00:17:51,200 --> 00:17:55,200
your attention not to the reactions.

255
00:17:55,200 --> 00:17:57,200
You know, they're with the mind things of it.

256
00:17:57,200 --> 00:18:00,200
But by fixing on the actual experience itself.

257
00:18:00,200 --> 00:18:04,200
So that restraints in the sense that it keeps the mind from

258
00:18:04,200 --> 00:18:08,200
getting upset about the pain or attached to things we enjoy

259
00:18:08,200 --> 00:18:15,200
with things as well.

260
00:18:15,200 --> 00:18:19,200
Sabe, who is Sabe, Sabe?

261
00:18:19,200 --> 00:18:28,200
Sabe, Sabe, Sabe, Sabe, Sabe is for the first line.

262
00:18:28,200 --> 00:18:31,200
I don't understand.

263
00:18:31,200 --> 00:18:47,200
Sabe, Sabe, Sabe, Sabe, Sabe is for the first time.

264
00:18:47,200 --> 00:18:57,200
You have to be able to overcome the pain and blocked your hand would

265
00:18:57,200 --> 00:18:59,200
continue.

266
00:18:59,200 --> 00:19:06,200
He is afraid of Punea, which is Kuzula.

267
00:19:06,200 --> 00:19:12,200
During walking meditation, sometimes we hear some joints making popping sounds out of nowhere.

268
00:19:12,200 --> 00:19:18,200
Why is this that hearing rise?

269
00:19:18,200 --> 00:19:19,200
I don't have any take on that.

270
00:19:19,200 --> 00:19:21,200
That's the body.

271
00:19:21,200 --> 00:19:24,200
I have to talk to someone who knows step above bones.

272
00:19:24,200 --> 00:19:28,200
I believe it's gas releasing in the body.

273
00:19:28,200 --> 00:19:32,200
What is the karma that leads to that?

274
00:19:32,200 --> 00:19:34,200
It's very complicated.

275
00:19:34,200 --> 00:19:36,200
It's the karma being a human.

276
00:19:36,200 --> 00:19:41,200
We've put together this very complex being that has lots of complex things happen to it,

277
00:19:41,200 --> 00:19:45,200
including our joints popping.

278
00:19:45,200 --> 00:19:50,200
It's just part of being human.

279
00:19:50,200 --> 00:19:53,200
Perhaps we're going to have to close that window and record.

280
00:19:53,200 --> 00:19:55,200
It's not okay to the next step.

281
00:19:55,200 --> 00:20:01,200
It's unfortunate because it's so hot up here.

282
00:20:01,200 --> 00:20:07,200
Why does the mind go to the ear?

283
00:20:07,200 --> 00:20:09,200
The mind is not focused.

284
00:20:09,200 --> 00:20:15,200
The mind is the nature of the mind when there's an experience.

285
00:20:15,200 --> 00:20:19,200
It is drawn to the experience.

286
00:20:19,200 --> 00:20:23,200
We're technically in Babidama.

287
00:20:23,200 --> 00:20:26,200
It's all cause and effect.

288
00:20:26,200 --> 00:20:31,200
There's resultant seeing hearing, smelling, tasting, feeling, thinking there.

289
00:20:31,200 --> 00:20:43,200
The results of karma are related to things that happened in the past.

290
00:20:43,200 --> 00:20:56,200
We're not so concerned about why it's happening.

291
00:20:56,200 --> 00:21:00,200
We're concerned about what is happening in the nature of what is happening,

292
00:21:00,200 --> 00:21:04,200
because it's not who cares why if it is happening,

293
00:21:04,200 --> 00:21:06,200
that's the issue.

294
00:21:06,200 --> 00:21:10,200
Of course, the popping isn't the problem.

295
00:21:10,200 --> 00:21:13,200
The issue is how you react to it.

296
00:21:13,200 --> 00:21:14,200
That's what's really important.

297
00:21:14,200 --> 00:21:16,200
It's important that we learn how we react to these things.

298
00:21:16,200 --> 00:21:18,200
Does it frustrate you?

299
00:21:18,200 --> 00:21:19,200
Do you feel curious?

300
00:21:19,200 --> 00:21:21,200
Do you wonder why you're the popping?

301
00:21:21,200 --> 00:21:23,200
Well, that wondering, that's what we want to learn about.

302
00:21:23,200 --> 00:21:24,200
Is that good?

303
00:21:24,200 --> 00:21:25,200
Is that bad?

304
00:21:25,200 --> 00:21:26,200
Is that problematic?

305
00:21:26,200 --> 00:21:31,200
Is that the best way to use my mental attention, my mental power?

306
00:21:31,200 --> 00:21:33,200
That's what we're focused on.

307
00:21:33,200 --> 00:21:35,200
That's what's important.

308
00:21:35,200 --> 00:21:50,200
So why isn't such a big deal?

309
00:21:50,200 --> 00:21:58,200
Okay, there's a little more questions than good night, everyone.

310
00:21:58,200 --> 00:22:13,200
See you next time.

