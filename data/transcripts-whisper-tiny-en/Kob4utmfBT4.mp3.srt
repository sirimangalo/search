1
00:00:00,000 --> 00:00:05,360
How do I deal with constantly seeing cause and effect in work when it becomes a distraction?

2
00:00:06,680 --> 00:00:08,680
We'll focus on the distraction

3
00:00:09,360 --> 00:00:13,560
when you feel distracted focus on that state of distraction

4
00:00:15,640 --> 00:00:21,560
This is called nana it can be a this is why we talk about what we call the upoculate

5
00:00:21,560 --> 00:00:26,760
We pass an upoculation. They're the defilements of insight, so there are good things

6
00:00:26,760 --> 00:00:32,600
But they distract you from the practice. When they distract you, you should focus on the distraction

7
00:00:32,600 --> 00:00:39,880
is distracted and distracted or overwhelmed or knowing, knowing, or thinking, thinking, or so

8
00:00:39,880 --> 00:00:47,960
and as best you can then come back again to your ordinary meditation practice

