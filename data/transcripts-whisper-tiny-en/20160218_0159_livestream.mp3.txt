Sorry, I'm a little bit late, just to reinstall my viewer.
Okay, I'm going to get the audio set up.
This one should come from now.
This one should come.
Okay, so now we should have voice on both channels.
Okay.
Okay.
Just going to set up my seat.
Okay.
Okay.
Okay, so good evening, everyone.
I'm broadcasting live.
This is actually the time when I normally broadcast on the internet anyway.
So there should be some people listening in at meditation.ceremango.org.
So today I understand is really Chinese New Year celebration.
Has anyone been taking part in the second life?
It's been happening around today, it's going on today.
Everybody's going on all day.
Is my audio okay?
Last time I saw that it was turning red.
I don't want to look too loud, I don't know if it's too loud.
I shouldn't ever see the red.
Okay.
Well, I don't celebrate Chinese New Year.
Put it down for something.
Honestly, I'm kind of confused by all the New Year's.
There's the one I grew up with.
It's the one we have on January 1st.
It's going to say, I grew up with a Christian calendar.
And so we have another Korean New Year.
Then there's some South Asia and Southeast Asia.
They have a New Year and people.
And now we have a third New Year that I have to think about.
So we, but Buddhist holidays are kind of, kind of like that.
We're just excuses for birthdays and excuses for birthdays and excuses for birthdays.
So our mortality, there are standard Buddhist holidays that actually have one million as a result.
But it's a nightmare that they have his first discourse.
So both monks and my peoples in Buddhist society has been when you've been exiting the room.
But New Year, well, not so much.
The idea of cycles is important.
It's important that we count in some way to be able to measure, to be able to compare.
So New Year is just going to measure, boy, a whole year is going to be.
And so it's traditionally become a tiny.
And Chinese tradition, it's a time for a member of your ancestors, which of course doesn't really have anyone to do.
Buddhism, it's much more interesting than just general folk religion.
But they didn't do it in Thailand.
People have Chinese background.
They didn't have Chinese background.
They didn't have Chinese background.
They burned stuff.
They weren't able to listen or burn.
They burned money, they burned houses, they burned food, they burned clothes.
They burned just about everything.
They burned effigy.
It's a big thing in India, at the time of the Buddha, it's a lot of instagram stuff.
It's a burning thing, burning things has always been symbolic for us.
It's a flame.
It's a little bit of talk about flames.
They said, you want to know the truth about fire?
I thought the cleansing of fire, we talked about the God of fire.
The Chinese tradition is somehow that if you burn it, it gets to the person who died because you've burnt the person who died.
So when you burn stuff, it gets to them.
It doesn't make any sense from the point of Buddhism.
Because you're not burning real stuff, you're not sacrificing anything, you're not giving anything.
The way Buddhism looks at things is much different.
So in that tradition, we should comment on this Chinese tradition the way Buddha would comment on the Indian tradition.
So the Buddha went after he became enlightened.
He walked from water in a seat to basically to Rajika.
But before he went to Rajika, 120 kilometers.
It's a long distance.
It was a long way to walk.
But before he went to Rajika, he had to find someone to give him legitimacy,
providing with the Judaism.
He would need to make his case for ordinary peoples.
And so at the time there was a group of ascetics, three groups of ascetics, three brothers,
which were following, living along the river.
He used to be a river going to Rajika in a surface seat.
2005, the years ago found a river.
And they all worshiped by us.
On the topic of fire, we have the spiral worshipers.
And so the Buddha sent to them all the same fire.
He spent some time convincing in the first of his magical powers,
and eventually he led them to a spot Gaiya Sisa,
which is, I guess, the top of the mountain.
You'll move on to that.
Oh, God, look at this.
And it was kind of close to Rajika in some moment.
And he taught them the adiitya periya sita.
And so he started by saying sabbang deep degree as it felt.
All sabbang deep degree, all sabbang deep degree,
all sabbang deep degree.
What do I mean by everything else?
So basically, the six senses, the internal base, the external base and consciousness,
giving the minds on fire.
The thought, um, mind, consciousness, a thought.
So this left is a common method of the Buddha. He asks a question in the answer again.
It's a very clear method of discourse.
In Aditam, it's what I do on the phone.
I agree, I agree.
I'll let you know something I'm not thinking.
Yes?
Well, I've been used while I was on Saturdays.
I shouldn't.
Last, those of you that have found an algorithm that showed you
may have had me now before the motion.
And you tell me job awards
So they're entire with two problems, but they're also in primary suffering.
So, Jazzy, I don't know if these are all the types of suffering.
So this is what they're on fire with.
Because they're caught up in some sense.
They're caught up in this perpetual cycle.
Decide the satisfaction.
So, this is the way they would have approached fire with two of them.
Probably an important or a good way.
And just responding or commenting on the rituals and things.
And it's not actually that related.
One other thing that we've seen is that these sorts of things.
Because often our rituals are considered good.
Rituals are considered to be positive.
And if it's a wholesome.
So you burn stuff for your ancestors and that's supposed to be wholesome.
Good.
And the idea here is that some of these things get to benefit.
And the people who've been living.
Those are going to be good.
So, your ancestors wherever they are will benefit from burning stuff.
It's curious.
I mean, this kind of thing happens all over the world.
You burn a sacrifice for God.
These were big things.
The angels wanted us to burn sacrifices, ancient times.
We read the Torah, the holy book of the Jewish people.
Apparently God likes them to burn stuff.
Something to burn stuff.
Somehow it got to God.
But it's the idea that somehow you could, somehow you could perform a sacrifice.
And that would benefit us.
We do it in Buddhism as well.
We do it towards the Buddha a lot.
Like we burn candles and flowers.
We burn incense light candles and put flowers in front of the Buddha.
I don't know that I can remember the point where the Buddha said that's a good thing.
I imagine you did it to the close that you could find where he said, oh yes, it's mandatory.
It's probably more impassive.
The most famous point is where he actually addressed this.
He said, this isn't how he is.
Rightly worshiped the Buddha.
But we do it anyway.
And we've got in many instances gone kind of crazy about it.
We're just talking about this today.
Funny enough, on Chinese New Year, that's what it is.
I have had this one for three.
The Chinese won't.
It's just something I do every day, but he just happened to come this one.
He went up to lunch.
And he was showing me pictures of how they...
They asked for 24 sets of food.
They asked him if one was good.
But in Sri Lankan tradition, they are after 28 sets of 28 liters.
So there's this list of 28 liters that's in one case.
That's over time.
It could be just made up.
But it's a list of 28 weren't come from them, really.
But they give these extra within all things.
And every time they know...
Well, the...
One of the most troubling aspects of it is that they throw out everything they give after me.
So I'm just watching them put together these sets of drinks.
So it's three different being glasses of just 28 sets.
And then at the end, they throw the juice away.
They say we can't drink it once it's given food.
And they do this with food as well as with food.
And they start looking to offer food to the good day.
Shall I show it up?
I'm not convinced that this is...
I mean, I've talked about it, but I don't want to go into too much detail.
But my overarching point is that this isn't really how goodness works in business work.
And goodness is doing a good deed and dedicate.
Whatever you like.
Using the power of the goodness.
There's power in goodness, but it has to be goodness.
Putting food in front of a statue.
I get it.
I get it.
There's faith in this confidence.
It's just...
You're not really benefiting a statue.
It's not goodness in the same way as giving food to a beggar.
Or giving food to a monk.
Or religious teacher.
If you feed someone who's teaching good things.
Sorry, I shouldn't.
I'm not hinting at anything.
But this is the thing, is people would give food
in the old days and even now in Buddhist countries.
It would be food to be food to be religious leaders.
And that's clearly good.
Because you're providing life to that person.
You know that you are directly supporting good things.
You're directly influenced in the future.
Giving to a statue.
You have to argue it's a different type of goodness.
But it borders on blind ritual.
Because there's some idea that by doing this,
and goodness comes in it.
It's not really how good it is in water.
Goodness comes from goodness.
There's actually a good thing that you're doing.
Putting juice in front of the statue.
Putting food in front of the statue.
I get that this is in goodness.
Anywhere.
That's how Buddhists do it.
And some people argue very seriously
that it is a good thing to do.
To each their own.
But it points to this.
And then this idea of it.
That we have no one things to do.
And we burn some.
And so a Buddhist.
An actual Buddhist ceremony.
Wouldn't be the burn stuff.
Hopefully that it gets to the people you love.
Though you could argue that you're good thoughts.
The fact that you are wishing them well.
Yeah, there's something for sure.
There's a way of clearing or having thoughts.
And thinking good thoughts about that person.
Respecting and reviewing.
It's a symbolic gesture.
That's probably how they're explained.
But there is a way that your goodness can reach the disease.
And that is by dedicating.
Actual good deans.
They can be compared burning fake money.
They're gold.
Too actually taking money and doing good things with it.
And then saying, this is our respect for that person.
And I do this on behalf of that person.
Those people.
That's something that actually has fruits.
There's fruit in many ways.
But most importantly, it says that the power of the goodness
gives you the power to determine in your mind.
There's all sort of the direction that you like that can take.
So it doesn't just work for disease value.
If you give charity and then you make a vow through this,
through this give me an eye, progress on the spirit of the pattern.
And that kind of thing.
Just because goodness has a powerful quality.
It takes away the guilt.
It takes away the stingy that you breathe.
I think the deans improved for venting capacity.
That's the idea.
So I mean, this hopefully is a no interesting quality.
It's not entirely what I had in mind to talk about today.
And I did want to talk about the idea.
It was just briefly to point out that with all these rituals
and all these sort of branches.
I shouldn't just make it a button.
We shouldn't just make the new be about the mirror.
So as I said, this is a way of counting.
And counting years.
But what is actually new?
This is the point.
What is that?
Are you the same old person with all the same old problems?
Because we're counting for a reason.
So we're just counting one year of our life gone.
That will never get back.
We're counting when we're closer to death.
And then we're just slaves to the stars and the sun.
We're shipping the sun.
We've gone around the sun again more time.
It is just for fun.
I think often times it's just to celebrate.
If you go to China, if you go to countries that celebrate these things.
For many people it's just an excuse to get done.
That's what we do here on New Year's Eve, right?
No, no, you should leave us anything.
You want to have a happy year.
And you yourself should...
And bother to leave us.
That's what we're supposed to do.
That's something we should do on our birthday.
Also something we should do on New Year's Eve.
That's what we're supposed to do on New Year's Eve.
That's why we make these resolutions.
But when you wish everyone a happy New Year's,
many of you have probably heard me.
Some of you have probably heard me with this talk so far.
There are four things that you should keep in mind that we should wish each other.
There's four things that we're supposed to bring as well to New Year's Eve.
I'm going to talk to you about New Year's Eve,
because this is what we wish for New Year's Eve.
This is what we want to change.
To find a point where we can see,
I'm going to be different this year.
I'm going to do something this year.
So it's just to actually find a new year.
So these four things, a wisdom, effort,
a self-perstraint, a composure, a kind of an internunciation.
So I've actually, for the actual new year,
I did the following year.
Just a few days before the New Year's Eve.
I did a guided meditation piece.
So I'd like to guide you guys through this one.
But if you want to look at the full one,
you can look at New Year's meditation to something.
It's a really good talk.
I've got it working well.
I like to do it very well.
But some talks I think they do kind of bland.
This one I think was pretty good.
It was much more guided meditation.
So that's what's really good.
The first is to close your eyes.
Start meditating.
Turn off Facebook.
Stop taking pictures.
Not that the pictures are unwanted.
At this point, let's switch to meditation.
Close your eyes.
And we start with wisdom.
Wisdom starts by seeing what you need.
So you close your eyes and say, what am I looking at?
What am I looking at?
When you close your eyes, you're really waiting to look at them.
And you can focus on love.
You can focus on death.
You can focus on the good there.
But for wisdom, we're going to focus on reality.
We're going to focus on the experiences.
Our experiences from science.
So we have to start by breaking reality up into its components.
If you start recognizing the things that the experience of your eyes,
you can't do that immediately.
Close your eyes.
It's not clear what am I looking at?
What is it?
I need direction.
The Buddha provided this direction.
I don't think everyone knows this practice of Buddhism.
He talked about mindfulness and people know that.
But he also talked about something called the four foundations of mindfulness.
These are important.
They're from the Buddha himself.
If you take them as a very useful tool,
you'll be making sense in your own experience.
The four foundations of mindfulness set the time of the body.
All of our movements and sensations in the body are feelings,
the pain, the pleasure, the calm, the thoughts,
the good thoughts, the bad thoughts, the good things.
And the dhammas.
So the dhammas are a whole bunch of stuff that really encapsulates the path
that we have to follow to become the Buddha.
But these four are a good guide.
The fourth one is sort of open-ended.
It's a lead to the goal.
So the bhavana.
So the bhavana.
It's just very similar.
So we start with the body.
Normally, I teach people to focus on the stomach because it's part of the body
that we're always going to experience because it's effective.
When it rises, we're going to look forward to seeing the body.
Not at large, it's just a new mind.
It gets you to start to recognize the body.
And you can just say to yourself, it's a good system.
So lots of different meditations as mentors, the mentor here,
who you're using is just based on it.
So we have a feeling such a good thing,
that we're going to have to look forward to.
We thought it's really safe thinking.
So we're kind of getting ahead of myself, but with wisdom.
Let's start with this way.
So wisdom is just learning how to see things.
Learning how to see what's going to start with the stomach.
The other ones we can use to talk about the other ones.
So for effort, the second one.
This is when you start to expand your awareness and become aware of everything else.
So wisdom would just understand that to be the basic idea,
seeing things instead of this is when we start to see the awareness.
But we need effort, which means we have to apply it to everything.
It's not enough to sit and meditate on a single object,
because so many other things are going to be moving away.
So this is when you feel pain, and you feel happiness.
Incorporating them into your practice means effort.
This is how we accomplish it.
We cannot get stuck on one thing.
We cannot get stuck on anything.
So with effort, we look at everything inside of it.
The third one, the composure, and it means then expanding it even beyond ourselves
to our experiences of the world around us.
And we feel the heat or the cold in the room,
and hear the noises in the room, and hear my speech.
And then the birds are the crickets, and the deer turn.
To meditate on them as well.
And here, see here.
Can you feel this feeling right there?
And this is called composure, because it prevents reaction to your experiences.
Whether it be attachment, or it will be a version,
it keeps you level head, it keeps you a piece.
It keeps you three years old.
It keeps you three years old.
It keeps you three years old.
And so we don't want to get caught up in them and become controlled by them.
And the final one is renunciation.
So renunciation has to do with our reaction.
So this is part of the dumber.
Beyond just our inner experience and the outer experience,
we have our reactions to all this.
And as I said, the idea is to be injected and not react.
But it's important to be able to understand our reaction,
to understand why we refer what it's like to react.
Because if you don't react to anything,
you're not saying right here.
But when we do react, this is where our practice begins in terms of renunciation.
Our practice is about giving up our attachments,
giving up our addictions, giving up our versions, giving up our partial addictions.
So we incorporate these things into our practice.
We're not trying to judge any of this.
We're not trying to judge any of this.
We're just trying to study it.
We're not not thinking to think.
We don't get upset about our upset.
We don't get angry about our anger.
We don't have to be afraid about fear.
We don't have to be afraid about fear.
We don't have to be afraid about fear.
We don't have to be afraid about fear.
We don't have to be afraid about fear.
But at least for things,
we can see how they very much fit into our insight meditation practice.
We're a good way of understanding.
We're a good way of looking at the framework of the first that people found there.
In order to get a sense of the quality of the practice,
and the fourth step of the time of the third time of the third time.
Okay, I think I've talked just about long enough.
Maybe we want to chat now.
Part of teaching the Dhamma and studying the Dhamma is conversation.
You never just be one star.
You never have any questions or comments or concerns about what I'm saying.
You just want to tell me I really can't teach birth beings face to face.
Always happy to talk to people about their experiences.
Thank you all for coming.
Happy meeting you.
I wish you could have brought that up earlier.
It's lower now.
Except that also turns down.
You know what I should do not?
What I did, I should do this way.
And the end.
Because I want voice to be high.
And so I think I can turn this back into a window.
Let me know if it's still to them, but I think it should be quieter now.
For me, on my fewer, I turn off all the sounds except for voice.
And so I don't hear anything but your voice.
So it's good on my end.
I like the ambient.
I thought the crickets were kind of a nice touch.
It's just their way to loud.
If I keep my standards set up, they're too loud on the audio stream.
So I just ruined that recording.
I forgot about the audience audio stream.
I did link it to everybody who couldn't come in today.
But your audio is not very good.
It's not like my audio.
My audio is direct from the source.
The audio you're getting is through the vbox or something else.
Not as good.
Because I upload it.
I watched that video that you made in the recording that you made in Simon and it was clipping as well.
Distorted.
If it ever goes to red, if you see the thing above my head ever turned red, that's bad.
That means it's clipping.
It means it's reached its limit and it just gets distorted.
More red is good.
It wasn't that bad.
It was more of a red than anything.
It's got a sound gate or something.
This is the way that is.
Oh, I'm too quiet.
That's interesting.
Because I wanted to be careful.
Simon was getting red bars last time.
Maybe I have to turn voice up in the volume.
There are so many variables.
So that's good.
Thank you.
I'm going to say good night.
I'm going to say good night.
