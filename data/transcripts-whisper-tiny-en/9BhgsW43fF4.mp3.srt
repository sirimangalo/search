1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:16,000
So today we continue on with verses 256 and 257, which read as follows.

3
00:00:16,000 --> 00:00:44,000
The Dhamapada is one of the most important things in this study of Dhamapada.

4
00:00:44,000 --> 00:01:11,000
Which means one is not a judge of the Dhamapada.

5
00:01:11,000 --> 00:01:22,000
Simply because one judge is forcefully, one is not a judge, a Dhamapada, a righteous judge,

6
00:01:22,000 --> 00:01:29,000
just because one judge is forcefully.

7
00:01:29,000 --> 00:01:48,000
Indeed one who is able to judge both cases and not cases.

8
00:01:48,000 --> 00:01:54,000
So if one judge is cases forcefully, one is not a Dhamapada.

9
00:01:54,000 --> 00:02:11,000
But if one judges both what are not, what are cases and what are not cases wisely,

10
00:02:11,000 --> 00:02:20,000
saying that without force, Dhammena, righteously, Samena,

11
00:02:20,000 --> 00:02:28,000
evenly, naivete, but a one judge is others.

12
00:02:28,000 --> 00:02:39,000
A judge without force, righteously evenly, the cases of others.

13
00:02:39,000 --> 00:02:45,000
And thus one guards the Dhamapada must a good domain to be a wise one.

14
00:02:45,000 --> 00:02:49,000
Gards the Dhamapada, T.

15
00:02:49,000 --> 00:02:57,000
Thus one is a Dhamapada judge.

16
00:02:57,000 --> 00:03:06,000
So this verse comes from a very simple story.

17
00:03:06,000 --> 00:03:11,000
So the one with quite a good lesson, I think.

18
00:03:11,000 --> 00:03:25,000
The monks were aware of judges in the society in which they lived.

19
00:03:25,000 --> 00:03:32,000
So when they would go on arms round, they would pass by the court.

20
00:03:32,000 --> 00:03:37,000
I don't know maybe it was an open-air court in ancient times.

21
00:03:37,000 --> 00:03:42,000
And so it was a place where people would come with their grievances

22
00:03:42,000 --> 00:03:52,000
to have them sorted out by the appointed judges, judges who would have been appointed by the king perhaps.

23
00:03:52,000 --> 00:03:55,000
Judges who had great power.

24
00:03:55,000 --> 00:04:05,000
And of course as normal, this institution would have been highly respected

25
00:04:05,000 --> 00:04:08,000
as a matter of course by the populace.

26
00:04:08,000 --> 00:04:18,000
It's given a reputation by teachers, by culture as being an institution of great importance,

27
00:04:18,000 --> 00:04:27,000
of great esteem.

28
00:04:27,000 --> 00:04:33,000
And so naturally the monks assumed that there were great things happening there.

29
00:04:33,000 --> 00:04:46,000
But one day they stopped and observed the proceedings and realized that in fact the judges were not judging impartially.

30
00:04:46,000 --> 00:04:59,000
In fact they were taking bribes from rich land holders in order to sway their decisions.

31
00:04:59,000 --> 00:05:15,000
And often would decide in ways that ruined the less fortunate or the less affluent of plaintiffs or defendants,

32
00:05:15,000 --> 00:05:22,000
causing ruin to a great number of people, unwarranted,

33
00:05:22,000 --> 00:05:31,000
stealing basically stealing their property and giving it to others based on bribes and faulty decisions.

34
00:05:31,000 --> 00:05:43,000
And they were shocked and very much disillusioned by the court in their area.

35
00:05:43,000 --> 00:05:49,000
And so they were discussing this and went to see the Buddha I think or the Buddha found out

36
00:05:49,000 --> 00:06:00,000
and as a result the Buddha remarked in this way.

37
00:06:00,000 --> 00:06:11,000
So the story, the topic today is about judging, which is a good Buddhist topic to talk about.

38
00:06:11,000 --> 00:06:23,000
First of all we can make a brief comment on society and remark that it seems to of course not gotten any better throughout the world.

39
00:06:23,000 --> 00:06:31,000
Corruption of course is rife in governments, in legal institutions.

40
00:06:31,000 --> 00:06:39,000
We hear stories of people using religious views to bias their decisions.

41
00:06:39,000 --> 00:06:46,000
But of course also greed, anger and delusion.

42
00:06:46,000 --> 00:06:55,000
But more importantly of course is how this relates to our own meditation practice.

43
00:06:55,000 --> 00:07:04,000
Judging is a good way to describe a large portion of our activity as human beings.

44
00:07:04,000 --> 00:07:17,000
We're of course constantly judging. Judgment describes much of our interaction with the world around us.

45
00:07:17,000 --> 00:07:25,000
And so the Buddha uses this word forcefully to describe it which I confess I have a hard time translating.

46
00:07:25,000 --> 00:07:33,000
I'm pretty confident in the translation but if you've ever read this verse in English you've probably noticed

47
00:07:33,000 --> 00:07:44,000
they use the word arbitrary which I don't think fits and I don't really understand how it is a translation of this word that I don't really understand that I confess.

48
00:07:44,000 --> 00:08:13,000
But the idea of being forceful is an interesting one simply because

49
00:08:13,000 --> 00:08:22,000
it often is something that lends a sort of credence.

50
00:08:22,000 --> 00:08:33,000
If someone is expressing an opinion forcefully it often is sufficient to convince your average person of the veracity of it.

51
00:08:33,000 --> 00:08:41,000
If someone says I believe this is right or this is wrong and they're great conviction it can often sway people.

52
00:08:41,000 --> 00:08:48,000
Often someone who is a great orator it often doesn't really matter what their stance is.

53
00:08:48,000 --> 00:09:04,000
People who train in debate, politics, politicians can be quite good at swaying people based on emotional appeal.

54
00:09:04,000 --> 00:09:21,000
But it goes even deeper than that and I think you could just use this word sahasa and it is used to describe any sort of bias, any sort of biased judgment where you force something.

55
00:09:21,000 --> 00:09:31,000
And so the idea that I get from this word is like a forcing square pegs to fit in round holes.

56
00:09:31,000 --> 00:09:46,000
You can pretend that it's going to fit or you can be convinced that it's going and you can use a lot of force to try to get in but it's not going to lead to a smooth or pleasant result.

57
00:09:46,000 --> 00:09:50,000
Or like trying to force two puzzle pieces that don't fit together.

58
00:09:50,000 --> 00:09:55,000
You have a puzzle and trying to fit a piece in where it doesn't belong.

59
00:09:55,000 --> 00:10:09,000
These sorts of analogies I think aptly describe the situation where you try to pass off improper judgment as proper judgment.

60
00:10:09,000 --> 00:10:24,000
And this example fits with a judge in the world who presents an opinion or a judgment that goes against reality.

61
00:10:24,000 --> 00:10:27,000
They force it through.

62
00:10:27,000 --> 00:10:32,000
But we do this as a way of describing what we do quite often.

63
00:10:32,000 --> 00:10:37,000
We do this out of greed, we do this out of anger, we do this out of delusion.

64
00:10:37,000 --> 00:10:52,000
It's an important point because for these judges and for anyone who makes these sorts of improper judgment, even in their own personal life, on a moment-to-moment basis, how we judge experience.

65
00:10:52,000 --> 00:11:01,000
It has often a sense of rightness about it.

66
00:11:01,000 --> 00:11:13,000
For a judge who is taking bribes, it's pretty clear they know they're doing something that is dangerous.

67
00:11:13,000 --> 00:11:23,000
But to say they know that it's wrong is I think an oversimplification because yes, they know it is seen as wrong by others.

68
00:11:23,000 --> 00:11:33,000
But a person who takes bribes like that or any sort of addict, someone who steals and this is basically what these judges were doing.

69
00:11:33,000 --> 00:11:46,000
Someone who is addicted to anything and that causes them to act and speak in manipulative ways or even just chase after objects of desire.

70
00:11:46,000 --> 00:11:49,000
It doesn't have a sense that it's wrong.

71
00:11:49,000 --> 00:11:56,000
People who are addicted to smoking, addicted to drugs, addicted to pornography.

72
00:11:56,000 --> 00:12:02,000
They might hate themselves for it, beat themselves up for it, but deep down they don't think it's wrong.

73
00:12:02,000 --> 00:12:09,000
They are forcing through this, they are glossing over.

74
00:12:09,000 --> 00:12:21,000
It's like a grinding of the gears that they miss because of the power of the desire.

75
00:12:21,000 --> 00:12:24,000
They only see the good of it.

76
00:12:24,000 --> 00:12:39,000
This is why it's quite difficult for people to give up sensual pleasure and give up the things that they are addicted to, the things they want, the things they crave.

77
00:12:39,000 --> 00:12:57,000
Because their lack of clarity, the imprecision of their judgment, the wrongness of their judgment, or they judge something to be satisfying a source of pleasure.

78
00:12:57,000 --> 00:13:03,000
It provides them with a conviction, a sense of the rightness of it.

79
00:13:03,000 --> 00:13:15,000
And this is, of course, why a very important point, why it's not really possible for someone who is mindful to get caught up in this.

80
00:13:15,000 --> 00:13:22,000
When you're mindful, your addictions seem to melt away at the moment when you're mindful.

81
00:13:22,000 --> 00:13:29,000
When you try to mindfully smoke cigarettes, you find it very hard to be attached to it.

82
00:13:29,000 --> 00:13:36,000
The desire seems to just melt away because there's clarity, because there's precision.

83
00:13:36,000 --> 00:13:52,000
And you can describe it as you're no longer just forcing through based on your belief, based on your imperfect perception, your perception of it as right.

84
00:13:52,000 --> 00:13:59,000
The greatness of mindfulness is that it gives you this clarity and the ability to perceive.

85
00:13:59,000 --> 00:14:05,000
Atang Anatang, as the Buddha said, what is a case, those are hard words.

86
00:14:05,000 --> 00:14:07,000
I had a hard time translating that.

87
00:14:07,000 --> 00:14:18,000
Atang, and Atang can mean case like you judge a case, like a matter, literally means something to matter, a subject, a topic or something.

88
00:14:18,000 --> 00:14:25,000
But it also means benefit. So Atang is that which has meaning, that which is meaningful.

89
00:14:25,000 --> 00:14:36,000
An Atang, that which is not meaningful, but it's extrapolated here to mean something like what is right and wrong.

90
00:14:36,000 --> 00:14:44,000
So if someone presents a case before a judge, the judge can see that it has merit.

91
00:14:44,000 --> 00:14:50,000
This case has merit. So that would be Atang. It has Atang. It has meaning. It's meaningful.

92
00:14:50,000 --> 00:14:58,000
If a case doesn't have merit, then they have to judge it against the person presenting the case. It's An Atang.

93
00:14:58,000 --> 00:15:06,000
So this kind of language, it's just the good or bad, right or wrong kind of language.

94
00:15:06,000 --> 00:15:17,000
So mindfulness provides you with that, the ability to see through, to see that taking bribes, there's no universe in which that is right.

95
00:15:17,000 --> 00:15:25,000
Again, it actually seems right to these judges who know that they're doing something quote unquote wrong, but it seems right why?

96
00:15:25,000 --> 00:15:33,000
Because money is right. Me getting money is right. That is the goal.

97
00:15:33,000 --> 00:15:42,000
This is why people lie and cheat and steal the imprecision. In their mind, they are grinding the gears forcing through.

98
00:15:42,000 --> 00:16:01,000
Their inability to see clearly is just providing them with a conviction that is able to just ignore all the stress and suffering and wretchedness that comes from addiction.

99
00:16:01,000 --> 00:16:20,000
An addict is so blind to the consequences that it feels like bliss to engage in their addiction, but any objective observer outside, even not someone practicing mindfulness, looks at them and says, what a wretch.

100
00:16:20,000 --> 00:16:31,000
And they're not wrong in saying that. It's quite objectively clear, this person is wretched, suffering terribly in their bliss.

101
00:16:31,000 --> 00:16:35,000
This is, I think, the meaning of sahasa forcing.

102
00:16:35,000 --> 00:16:44,000
And I think it's a really good word, a good way of describing what happens when you don't have the clarity of mindfulness.

103
00:16:44,000 --> 00:16:52,000
So mindfulness, again, is just the simple act of reminding yourself. It's just one moment.

104
00:16:52,000 --> 00:16:59,000
And in that moment, you adjust your perception, right?

105
00:16:59,000 --> 00:17:08,000
Because this state of mind that is forceful, that is just pushing through your based on the narrative that you provide.

106
00:17:08,000 --> 00:17:16,000
This is good for me, this is my goal, money, or pleasure, or whatever, or conversely anger, right?

107
00:17:16,000 --> 00:17:32,000
Some people can feel righteous in their anger. I read recently that there are Buddhist monks actually defending war and killing and saying that somewhere is justifiable.

108
00:17:32,000 --> 00:17:53,000
Based, I guess, on this attachment to society, right? If someone invades your country, the horror of losing your country, this thing that you cling to can lead you to great anger and the ability to kill in defense.

109
00:17:53,000 --> 00:18:12,000
I think defense can be warranted, but certainly not killing. I think why someone would force through that opinion is they're not able to see clearly enough the distinction between perhaps a defending yourself and murdering someone else.

110
00:18:12,000 --> 00:18:31,000
And so, as a result, I mean, personally, at least because of anger, the aversion to losing what you hold dear. Your country, your society, even Buddhist countries often go to war and are very passionate about protecting Buddhism, which is kind of horrific to think about.

111
00:18:31,000 --> 00:18:45,000
On the other hand, the Buddha didn't spend a lot of time trying to fix society. I think wisely he didn't take sides, didn't pick parties to follow or to support.

112
00:18:45,000 --> 00:19:08,000
He praised people who were praised worthy, but much more than people he praised qualities that were praised worthy to try to convince all sides to better themselves rather than taking sides and creating militaristic situations.

113
00:19:08,000 --> 00:19:22,000
But these states where we are caught up in greed and anger and delusion are made up of moments and they're perpetuated by moments.

114
00:19:22,000 --> 00:19:40,000
And so, it may seem insignificant to sit on your meditation, see it and repeat to yourself pain, pain, anger, liking, or any of these things.

115
00:19:40,000 --> 00:20:00,000
But you have to understand the, again, this nature of how our habits form and how they reform every moment that these are not masses or things that we carry around with us.

116
00:20:00,000 --> 00:20:19,000
Our processes, and our processes that only continue by the perpetuation, the feeding of them, we feed them through our continuation of the habit.

117
00:20:19,000 --> 00:20:32,000
Our anger perpetuates more anger, our greed cultivates the habit of greed, we like our liking, our anger makes us more angry and so on.

118
00:20:32,000 --> 00:21:00,000
And so every moment that we change this provides us with a alternative and it provides us with a clarity. So in that moment, it's like putting on the breaks and provides you with this self reflection that is so much a part of meditation that allows you to see how am I doing?

119
00:21:00,000 --> 00:21:13,000
How am I doing this? What is the state of my mind as I pass this judgment as I engage in this activity? It allows you to see.

120
00:21:13,000 --> 00:21:20,000
And that's the whole point, mindfulness leads to seeing, seeing clearly.

121
00:21:20,000 --> 00:21:35,000
It's quite, it should be quite apparent you should be able to see from moment to moment that you're less inclined to engage in hasty judgment, irrational judgment, reactivity.

122
00:21:35,000 --> 00:21:54,000
You start and said instead of reacting, going with it, to seeing your reactions, you start to be like you're watching yourself in horror as you get angry again and again as you cling again and again.

123
00:21:54,000 --> 00:22:22,000
And you see it, it wash away because you slowly, slowly stop feeding it. When you replace this forceful quality with a much more or a similar sort of forcefulness as I think it possibly, although I don't know about this word, Saha, you can use it in a way that is not unwholesome.

124
00:22:22,000 --> 00:22:41,000
If someone is clearly seeing the nature of their experience, then there is a forcefulness still to their actions, to their judgments, but the force is the force of knowledge, the force of understanding.

125
00:22:41,000 --> 00:23:01,000
They simply see clearly, they see how smoking, of smokers see is how smoking is disgusting and really unpleasant and not useful or beneficial in any way, seeing that it is a cause for addiction, it is a cause for the stress and the suffering of withdrawal and so on.

126
00:23:01,000 --> 00:23:14,000
And with anger, for example, with taking bribes apart from how it can lead to criminal prosecution and so on, vilification.

127
00:23:14,000 --> 00:23:34,000
Simply the insanity of the cruelty of hurting other people, taking away their property, for your own benefit, that sort of thing. Just seeing all this, affects a great change in a person.

128
00:23:34,000 --> 00:23:54,000
But again, there is a great strength and so the force, you gain a great force, a great inertia or what is the word, a great power in your mind that is based on a stronger conviction.

129
00:23:54,000 --> 00:24:11,000
It is no longer grinding and gears, it is knowing that the circular peg fits in the circular hole and going for it without any doubt. Seeing that, oh wait, this one is square, this goes in the square hole, putting things in the right place, simply because you see more clearly.

130
00:24:11,000 --> 00:24:25,000
That is the general principle of mindfulness and I will be passing that. So I think that is the lesson that we can take from these two verses. And that is the demo of that Padafar today. Thank you for listening.

