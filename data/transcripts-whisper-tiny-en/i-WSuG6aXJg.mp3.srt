1
00:00:00,000 --> 00:00:06,520
Hello and welcome back to our study of the Dhamapada.

2
00:00:06,520 --> 00:00:19,000
Today we continue with verses 2 19 and 2 20, which read as follows.

3
00:00:19,000 --> 00:00:29,000
The Dhamapada is a very important study of Dhamapada.

4
00:00:29,000 --> 00:00:37,000
The Dhamapada is a very important study of Dhamapada.

5
00:00:37,000 --> 00:01:05,000
When a person who has been gone long, gone away far away for a long time, returns well and in good health,

6
00:01:05,000 --> 00:01:11,000
which returns well from far away.

7
00:01:11,000 --> 00:01:22,000
Their relatives, their friends and even their relatives, friends and companions,

8
00:01:22,000 --> 00:01:28,000
they rejoice at their coming.

9
00:01:28,000 --> 00:01:43,000
Just so, the Teva katapunyampi, just so, for one who has done good deeds, gone from this world to the next.

10
00:01:43,000 --> 00:01:51,000
Punyani, patikandhanti, their good deeds received them.

11
00:01:51,000 --> 00:02:01,000
Just as relatives would, a dear one who has returned.

12
00:02:01,000 --> 00:02:06,000
It's a poignant and poetic set of verses.

13
00:02:06,000 --> 00:02:10,000
They were taught in regards to Nandya.

14
00:02:10,000 --> 00:02:16,000
Nandya was not a monk, he was a lay person.

15
00:02:16,000 --> 00:02:27,000
But he was very much devoted to Buddhism, devoted to the Buddha, the monks and the teachings, of course.

16
00:02:27,000 --> 00:02:45,000
And he was kind and generous to the monks and the Buddha and respectful and spent devoted a lot of his time to visiting and listening to the Buddha's teaching.

17
00:02:45,000 --> 00:03:02,000
You see this in Buddhist societies, people who are uncommonly interested in the monasteries and the sangha, the teaching and the practice.

18
00:03:02,000 --> 00:03:13,000
Even young people sometimes, and it's rare, but you'll see them sometimes very much interested in the religion, in the practice.

19
00:03:13,000 --> 00:03:16,000
He was one such person.

20
00:03:16,000 --> 00:03:25,000
So his parents, like all parents at the time, it seems, or most parents, ordinary parents at the time.

21
00:03:25,000 --> 00:03:26,000
What did they do?

22
00:03:26,000 --> 00:03:31,000
Well, yes, they sought out a wife for her, for him.

23
00:03:31,000 --> 00:03:38,000
And they found someone who lived next door.

24
00:03:38,000 --> 00:03:44,000
They might have been a cousin or something actually, was it?

25
00:03:44,000 --> 00:03:50,000
Yes, his uncle's daughter seems like marrying your cousins was a thing.

26
00:03:50,000 --> 00:03:57,000
Raywati, and they wanted the two of them to get married.

27
00:03:57,000 --> 00:04:00,000
I guess to keep it in the family.

28
00:04:00,000 --> 00:04:03,000
I keep the bloodline, I don't know.

29
00:04:03,000 --> 00:04:08,000
Well, there's some assurance, I guess, for the family, for the parents' perspective,

30
00:04:08,000 --> 00:04:16,000
that you're marrying someone who is of similar ideas and similar perspectives and similar backgrounds.

31
00:04:16,000 --> 00:04:24,000
And there are all sorts of other strange reasons people give, I suppose, like genes and blood and that sort of thing.

32
00:04:24,000 --> 00:04:31,000
Lineage, tradition, culture, culture is often a big part of it.

33
00:04:31,000 --> 00:04:41,000
But the thing about this was, Raywati was not at all compatible with Nandya.

34
00:04:41,000 --> 00:04:49,000
And though it's rather incidental to the story, the text does make a bit of a deal out of the fact that Nandya wouldn't marry her.

35
00:04:49,000 --> 00:04:57,000
Why? Because she had no interest in the monks or the Buddha, no interest in the teaching or the practice.

36
00:04:57,000 --> 00:05:05,000
She didn't seem all that generous or kind. And she said, I don't want to marry her.

37
00:05:05,000 --> 00:05:12,000
But the parents, I guess, were determined and so they went to Raywati.

38
00:05:12,000 --> 00:05:17,000
They didn't force them to get married, which seems to be the case, seems to be the way it was.

39
00:05:17,000 --> 00:05:21,000
We see a lot of these stories where they didn't force them to get married.

40
00:05:21,000 --> 00:05:29,000
Instead, they went to Raywati and said, look, we want you to start to be a good Buddhist.

41
00:05:29,000 --> 00:05:33,000
Start to be a respect to the Buddha. They wouldn't have said good Buddhist.

42
00:05:33,000 --> 00:05:41,000
Because back then it wasn't about religions, but they would have said, pay respect to the Gautama.

43
00:05:41,000 --> 00:05:47,000
He's the teacher of our son, and if you want to get into his good graces,

44
00:05:47,000 --> 00:05:59,000
you have to show that you are respectful and interested in keen on the Buddha and the teachings.

45
00:05:59,000 --> 00:06:07,000
Raywati was, I guess, keen on the marriage somehow, or else maybe she was coerced into it.

46
00:06:07,000 --> 00:06:11,000
For women at the time, I think it was not easy if they didn't get married.

47
00:06:11,000 --> 00:06:22,000
For her getting married, especially to someone who was wealthy like Nandya was, would be a very important task to accomplish in their life.

48
00:06:22,000 --> 00:06:28,000
Unfortunately, I'm not promoting this sort of culture, but it was the way of things at the time.

49
00:06:28,000 --> 00:06:36,000
So just to survive, they would have to secure a good marriage.

50
00:06:36,000 --> 00:06:43,000
And so she did as was requested and suddenly appeared by all appearances to be a good Buddhist, and so Nandya married her.

51
00:06:43,000 --> 00:06:49,000
It's not really actually important to the story, but it is an interesting point that we can come back to.

52
00:06:49,000 --> 00:07:02,000
The point is that Nandya was a very good sort of person, kind and generous, and very sensitive to the Dhamma.

53
00:07:02,000 --> 00:07:09,000
So he was interested in practicing. He was interested in listening to the Buddha's teaching.

54
00:07:09,000 --> 00:07:17,000
But he was also very generous, and so he would give gifts and hosts the monks in his house and that sort of thing.

55
00:07:17,000 --> 00:07:24,000
So Nandya did the same. He said to her, look, if you want to live in my house, you gotta take part.

56
00:07:24,000 --> 00:07:40,000
And so she did, she was very humble and respectful towards the monks and got involved, joined in with offering them food and that sort of thing.

57
00:07:40,000 --> 00:07:51,000
But Nandya, one day he had to stop to himself. He thought to himself, what would be something really good that I could do as a good deed.

58
00:07:51,000 --> 00:08:00,000
And he had heard or he had come to consider the idea of giving a dwelling.

59
00:08:00,000 --> 00:08:13,000
And this was his idea. And so he started making plans and he had this real strong ambition to offer a dwelling to the Sangha.

60
00:08:13,000 --> 00:08:24,000
And this was in Isipatana. So if those of you who were on the or have been on a pilgrimage to India, we can remember how Isipatana.

61
00:08:24,000 --> 00:08:32,000
This is where the Buddha became enlightened. So I'm not sure if this is the Buddha coming back to Isipatana or if it was when he was there in the first place.

62
00:08:32,000 --> 00:08:37,000
But you can remember that there are many ruins of monastery, so buildings.

63
00:08:37,000 --> 00:08:47,000
So it may be that we saw there the building that Nandya built, although most likely it was covered over by the ones we saw were only the later generations.

64
00:08:47,000 --> 00:08:57,000
But somewhere in there, supposedly Nandya built a monastery with four rooms. He built a dwelling.

65
00:08:57,000 --> 00:09:02,000
With four rooms and the text goes on about how splendid it was.

66
00:09:02,000 --> 00:09:12,000
I think more important is his great faith and his great conviction about his good deeds, about doing this.

67
00:09:12,000 --> 00:09:38,000
But doing something that he felt would be great for not just the Buddha's Asana, not just for the monks, but just good for him in general and good thing for him to do in general.

68
00:09:38,000 --> 00:09:52,000
And at that time, he put together this plan and he enacted it and he built this dwelling and he was very much caught up in the conviction and it just was such a wholesome, great deed.

69
00:09:52,000 --> 00:10:08,000
And then that day, the day that he offered, the monastery and they had his tradition of offering, so the Buddha would held out his hand and this was not just with the Buddha. This was a, I've tried to find out how it was.

70
00:10:08,000 --> 00:10:16,000
But this was apparently just a tradition in India. I don't know if Nandya you could ask someone and they could explain it to you even today.

71
00:10:16,000 --> 00:10:23,000
But they would take water and they would pour it over the hand of the person they were giving it to. It's not just a Buddhist thing.

72
00:10:23,000 --> 00:10:29,000
It was apparently something that people would do with each other as a means of washing their hands of it.

73
00:10:29,000 --> 00:10:36,000
So the person giving would take water, the person taking would hold out their hand, something like this, they pour water.

74
00:10:36,000 --> 00:10:41,000
At the moment when he poured water over the Buddha's hand to symbolize giving it.

75
00:10:41,000 --> 00:10:46,000
Mughalana was up in heaven, the heaven of the tawatingsa.

76
00:10:46,000 --> 00:10:57,000
Mughalana was one of the Buddha's chief disciples and he had very strong inclination towards mental powers.

77
00:10:57,000 --> 00:11:04,000
One of them which was traveling with his mind to the various realms.

78
00:11:04,000 --> 00:11:12,000
The heavens often, but also hell I think. He also saw many ghosts traveling around and related those.

79
00:11:12,000 --> 00:11:20,000
But one of his pastimes was to be in heaven and when he was up in heaven he saw this beautiful mansion,

80
00:11:20,000 --> 00:11:25,000
made of jewels and gold.

81
00:11:25,000 --> 00:11:36,000
And with lots of women it says, well lots of women there it did point out to story, I don't know.

82
00:11:36,000 --> 00:11:43,000
There were lots of women who attended the ideas that there were nymphs I guess in attendance.

83
00:11:43,000 --> 00:11:49,000
And he went in and he went up to and he said, oh I've never seen this great mansion before.

84
00:11:49,000 --> 00:11:59,000
It was the text says it was 12 leagues wide, something like that.

85
00:11:59,000 --> 00:12:03,000
Huge, just a real royal palace.

86
00:12:03,000 --> 00:12:07,000
Oh could only be built in heaven probably.

87
00:12:07,000 --> 00:12:14,000
And he said to some of the women the nymphs that were hanging out there he said, oh this I've never seen this before.

88
00:12:14,000 --> 00:12:19,000
Who's man to mention is this, who's palace is this?

89
00:12:19,000 --> 00:12:23,000
And they said, it's owner isn't here yet.

90
00:12:23,000 --> 00:12:26,000
I said, well where's it's owner?

91
00:12:26,000 --> 00:12:38,000
Well you see today, just this morning, Nandya, this man in Isipatana lives in Varanasi.

92
00:12:38,000 --> 00:12:49,000
Offered dwelling to the Buddha and his great conviction was so strong and his gift was so pure.

93
00:12:49,000 --> 00:12:55,000
But this is the result, he hasn't even died yet, he hasn't even come to heaven yet.

94
00:12:55,000 --> 00:12:58,000
And we're all kind of disappointed that he's not here.

95
00:12:58,000 --> 00:13:03,000
And we want to ask you venerable sir, please, when you talk to him please tell him to hurry up.

96
00:13:03,000 --> 00:13:08,000
We're waiting for him here.

97
00:13:08,000 --> 00:13:11,000
And Mogulano is quite surprised by this.

98
00:13:11,000 --> 00:13:14,000
So he went to see the Buddha.

99
00:13:14,000 --> 00:13:26,000
And he said to the Buddha, venerable sir, is it true that being who people who do good deeds,

100
00:13:26,000 --> 00:13:31,000
even though they haven't passed away yet, the results in heaven,

101
00:13:31,000 --> 00:13:37,000
palaces and whatever can spring up, even though they haven't left,

102
00:13:37,000 --> 00:13:42,000
they aren't there to experience them.

103
00:13:42,000 --> 00:13:46,000
And the Buddha said, you know for yourself why do you ask?

104
00:13:46,000 --> 00:13:54,000
And then he repeated this for these two verses as a lesson for that.

105
00:13:54,000 --> 00:14:00,000
So on the face of it it's a fairly worldly sort of tale.

106
00:14:00,000 --> 00:14:05,000
It's talking about things like rebirth and heaven, which are limited in Buddhism.

107
00:14:05,000 --> 00:14:07,000
They're not the goal of course.

108
00:14:07,000 --> 00:14:08,000
And they're not the main doctrine.

109
00:14:08,000 --> 00:14:13,000
And many people don't even believe in them, in this audience that we have in the West.

110
00:14:13,000 --> 00:14:18,000
There's a skepticism about the even existence of heaven and angels.

111
00:14:18,000 --> 00:14:21,000
Which is not a problem, not a large problem.

112
00:14:21,000 --> 00:14:23,000
It's not something we should be concerned with.

113
00:14:23,000 --> 00:14:27,000
So we don't have to talk about these things most of the time.

114
00:14:27,000 --> 00:14:31,000
But we come across a story like this, and there are some interesting points that we can make,

115
00:14:31,000 --> 00:14:36,000
even for people who are more focused on meditation.

116
00:14:36,000 --> 00:14:43,000
So for our meditation, in our practice, what's the significance of this story?

117
00:14:43,000 --> 00:14:47,000
Well the first lesson, before we get into the real core of it,

118
00:14:47,000 --> 00:14:51,000
is this side story of Nandya and Rehwati.

119
00:14:51,000 --> 00:15:01,000
We might know what to apparently, continues this story, and talks about how Nandya went on to do good deeds,

120
00:15:01,000 --> 00:15:08,000
pay respect and venerate the Buddha and his disciples and the teachings and so on.

121
00:15:08,000 --> 00:15:16,000
It didn't become enlightened I guess, but went to heaven and was able to live in his palace with all the nymphs.

122
00:15:16,000 --> 00:15:25,000
But Rehwati, on the other hand, was after Nandya died, it says that she stopped giving gifts, stopped respecting the sangha,

123
00:15:25,000 --> 00:15:34,000
started reviling the monks, and when she died she went to hell.

124
00:15:34,000 --> 00:15:49,000
So that whole story gives some reminder to us. First of all, the fact that Nandya would hold as a criteria for marriage,

125
00:15:49,000 --> 00:15:57,000
the sharing of similar religious or spiritual inclinations.

126
00:15:57,000 --> 00:16:02,000
Someone would have to be respectful or interested in the Buddha's teaching if I'm going to marry them.

127
00:16:02,000 --> 00:16:13,000
That sort of thing is often overlooked or trivialized by Buddhists and people in general.

128
00:16:13,000 --> 00:16:21,000
Sometimes we get lucky and that comes as a part of it, but quite often of course our romantic inclinations,

129
00:16:21,000 --> 00:16:31,000
and not only romantic inclinations, but our connections with others are determined by far more worldly things.

130
00:16:31,000 --> 00:16:44,000
It's obviously lust and desire, but friendships as well, often are based on sometimes quite strange premises.

131
00:16:44,000 --> 00:16:54,000
The amount of time we've known someone we often think is sufficient, known as person forever, we're just good friends.

132
00:16:54,000 --> 00:17:07,000
But the Buddha was quite particular about this and quite clear and specific and adamant about the importance of having good friends and the danger of having bad friends.

133
00:17:07,000 --> 00:17:11,000
We drag each other down, if bad friends will drag you down.

134
00:17:11,000 --> 00:17:20,000
So that's why solitude is often preferable, if you can't find a good friend, someone who can help you can support you.

135
00:17:20,000 --> 00:17:27,000
So in India I think was right and I can't imagine that their marriage was as perfect as it's kind of whitewashed to be in the story.

136
00:17:27,000 --> 00:17:45,000
The details are left out, but in India it seems that it seems that it was acting on coercion or perhaps force being pushed into her spiritual practice and her kindness and her generosity.

137
00:17:45,000 --> 00:17:54,000
It says that she appeared, who Texas, she seemed just like one who had faith.

138
00:17:54,000 --> 00:17:59,000
And we can see how that turned out for her being pushed into it, being forced into it.

139
00:17:59,000 --> 00:18:07,000
So with friendships and even with romantic engagements, I've often, as a teacher, seen students talk about this.

140
00:18:07,000 --> 00:18:23,000
And they ask me questions about how to deal with friends, how to deal with lovers, romantic interests, that sort of thing, partners, who are not interested in Buddhism.

141
00:18:23,000 --> 00:18:47,000
And I think the answer often is that you have to go your separate ways, you have to be determined and you have to be strong, decisive in deciding who your friends and companions are going to be.

142
00:18:47,000 --> 00:19:00,000
Sometimes, this is his cousin, but not a cousin that I think he should have spent a lot of time hanging out with or certainly getting married to, because it sounds like she had different interests, which of course is fine.

143
00:19:00,000 --> 00:19:05,000
It's not that you should look down on people, of course.

144
00:19:05,000 --> 00:19:22,000
You should just wisely and in a very objective sort of way understand that the interaction with this person and the purposeful engagement with them is probably not to anyone's benefit.

145
00:19:22,000 --> 00:19:25,000
You drive, they just drag you down.

146
00:19:25,000 --> 00:19:32,000
They lead you to problematic states, problematic results.

147
00:19:32,000 --> 00:19:38,000
So association with good people, it's very important to Buddha, set it among all of them.

148
00:19:38,000 --> 00:19:50,000
Let's say you are not a bala, not to associate with full, spend it and just say it with wise people.

149
00:19:50,000 --> 00:19:54,000
And again, it's not about looking down, it's not even about avoiding.

150
00:19:54,000 --> 00:20:07,000
But certainly, I think about not getting married to or considering to be a valuable friend or ally or compatriot companion.

151
00:20:07,000 --> 00:20:16,000
The second lesson is the sort of obvious lesson, and it's this lesson in the goodness of goodness.

152
00:20:16,000 --> 00:20:24,000
Good for goodness. Goodness for goodness is sake.

153
00:20:24,000 --> 00:20:27,000
That Buddhists are very much engaged in good deeds.

154
00:20:27,000 --> 00:20:44,000
Nanda was throughout his life, it sounds like interested in generosity and support and patronage for the Buddhist song at the Buddhist community.

155
00:20:44,000 --> 00:20:53,000
And it doesn't really say, but it also most likely dedicated to the teachings and the practice.

156
00:20:53,000 --> 00:21:00,000
But all of these things that what they have in common is what the verse talks about is Pune, their involved with goodness.

157
00:21:00,000 --> 00:21:04,000
And sometimes as meditators we miss this.

158
00:21:04,000 --> 00:21:06,000
So how does this relate to our meditation?

159
00:21:06,000 --> 00:21:16,000
Well, it's kind of a checking of ourselves. For those who are of the view that the most important part of Buddhism is meditation,

160
00:21:16,000 --> 00:21:19,000
therefore that's what I should focus on.

161
00:21:19,000 --> 00:21:31,000
It often can become the case that you miss some attachment because you're not challenging yourself with the challenge of giving,

162
00:21:31,000 --> 00:21:41,000
with the challenge of generosity, with the challenge of active good deeds, active kindness and humility and so on.

163
00:21:41,000 --> 00:21:49,000
And so as a result, you keep all the things that you own and any wealth that you accumulate and you can start to get very attached to it.

164
00:21:49,000 --> 00:22:00,000
And you don't notice it because it's easy to be at peace. There's no uncertainty, there's no instability as long as you have wealth and power and so on.

165
00:22:00,000 --> 00:22:07,000
But giving is very useful because of its quality of giving up.

166
00:22:07,000 --> 00:22:13,000
If it's truly giving, it often involves giving up something that you'd rather have for yourself.

167
00:22:13,000 --> 00:22:16,000
And so as a result, it challenges you.

168
00:22:16,000 --> 00:22:28,000
It forces you to confront your attachment to things and realize that there are many things that we don't need, but we just enjoy

169
00:22:28,000 --> 00:22:30,000
and become attached to.

170
00:22:30,000 --> 00:22:40,000
So giving challenges you, it forces you to see this, the greed, the attachment and the aversion and the suffering that come from not getting what you want,

171
00:22:40,000 --> 00:22:44,000
from withdrawal from your addictions.

172
00:22:44,000 --> 00:22:49,000
When you give you confront stinginess and you confront greed,

173
00:22:49,000 --> 00:22:52,000
so charity is a very good act.

174
00:22:52,000 --> 00:22:58,000
Jesse Sayada said something very pointed. He said, yes, it's true that you don't need to do good deeds.

175
00:22:58,000 --> 00:23:01,000
But if you don't do good deeds, what are you going to do?

176
00:23:01,000 --> 00:23:04,000
It's most likely that you'll just end up doing bad deeds.

177
00:23:04,000 --> 00:23:09,000
And that's certainly the case in the absence of good deeds.

178
00:23:09,000 --> 00:23:19,000
You know, all that's going to feel the void are many sorts of clinging and partiality and aversion and that sort of thing.

179
00:23:19,000 --> 00:23:27,000
So good deeds put us in a position, you know, supporting the Buddha.

180
00:23:27,000 --> 00:23:29,000
It didn't just end up supporting the Buddha.

181
00:23:29,000 --> 00:23:37,000
It put him in a position to hear the Buddha's teaching, to be in touch with the practice.

182
00:23:37,000 --> 00:23:44,000
Even if he didn't become enlightened, he's most certainly benefited from that association.

183
00:23:44,000 --> 00:23:46,000
And so it's something that we should remember.

184
00:23:46,000 --> 00:23:47,000
It is an important point.

185
00:23:47,000 --> 00:23:54,000
Of course, goodness of meditation and study and so on listening to Dhamma is also goodness.

186
00:23:54,000 --> 00:23:58,000
So when the Buddha talks about Purnya, he's not just talking about giving gifts.

187
00:23:58,000 --> 00:24:10,000
In fact, often he would use that the concept of doing the good deed of giving as a jumping off point to talk more generally about Purnya, which is what he does here.

188
00:24:10,000 --> 00:24:17,000
I think he's more generally referring not just to the generosity of giving a monastery, for example,

189
00:24:17,000 --> 00:24:26,000
but the goodness of any kind of practice, any kind of Purnya.

190
00:24:26,000 --> 00:24:34,000
And the third lesson that we should keep in mind is I think that's a very obvious part of the story,

191
00:24:34,000 --> 00:24:38,000
a glaring sort of questionable for many people, part of the story.

192
00:24:38,000 --> 00:24:44,000
It's more of a metaphysical aspect in the sense of it talks about the nature of reality.

193
00:24:44,000 --> 00:24:48,000
It's deeper and it has perhaps deeper implications for the practice.

194
00:24:48,000 --> 00:25:00,000
And that's the aspect of how hard deeds can affect seemingly in magical ways the world around us.

195
00:25:00,000 --> 00:25:07,920
So I'm certain there's a large amount of skepticism from some people in regards to the idea that there would even be a

196
00:25:07,920 --> 00:25:13,920
mansion in heaven for someone, let alone the fact that it would spring up spontaneously.

197
00:25:13,920 --> 00:25:21,920
The moment when someone does a good deed, like anytime we do a good deed, there's mansions popping up in heaven all over the place.

198
00:25:21,920 --> 00:25:25,920
I mean, first of all, it was a very sort of rare example.

199
00:25:25,920 --> 00:25:30,920
It's not every day that someone offers a dwelling place to the Buddha.

200
00:25:30,920 --> 00:25:35,920
But certainly there would have been a sense that this wasn't a one-time affair,

201
00:25:35,920 --> 00:25:50,920
which is the sort of thing that happened anytime people did those great deeds in the time of the Buddha with such worthy recipient.

202
00:25:50,920 --> 00:26:02,920
But there's in the Buddha's teaching a more general sense that not only do our good deeds affect our own minds,

203
00:26:02,920 --> 00:26:05,920
they by extension appear to affect the world around us.

204
00:26:05,920 --> 00:26:18,920
And so you have to have this idea that be open to the idea that the world around us is less independent of our experience than we think.

205
00:26:18,920 --> 00:26:36,920
And we always, you know, spiritual people are notorious for bringing up quantum physics, but there are qualities to reality that quantum physics seems to point out that shake up the idea that we're dealing with billiard balls,

206
00:26:36,920 --> 00:26:44,920
that we're dealing with entities that are there that exist in space and time independent of the observer, for example.

207
00:26:44,920 --> 00:26:50,920
The idea that the observing mind is a part of the reality.

208
00:26:50,920 --> 00:26:58,920
And so reality is dependent in some respects on the observer.

209
00:26:58,920 --> 00:27:03,920
Now scientists tend to confine this sort of thing to subatomic levels,

210
00:27:03,920 --> 00:27:17,920
but there are examples where it comes into the superatomic, the large scale, the macroscopic.

211
00:27:17,920 --> 00:27:25,920
And Buddhism has this sense that this is the case and is most especially the case in relation to good and bad deeds, of course,

212
00:27:25,920 --> 00:27:33,920
because those are the ones that are going to relate to suffering and happiness, because that's the whole definition of suffering of good and bad.

213
00:27:33,920 --> 00:27:38,920
Something is good because it relates to happiness and peace and freedom from suffering.

214
00:27:38,920 --> 00:27:50,920
Something is bad because it leads and encourages and involves suffering.

215
00:27:50,920 --> 00:27:59,920
And so it's something very important for us to keep in mind and to really have a sense of, in terms of our understanding of what reality is.

216
00:27:59,920 --> 00:28:15,920
It very much ties in with the nature of our perspective as meditators, that our perspective is not of a place where we are and the body in space and time and even the mind as an entity.

217
00:28:15,920 --> 00:28:42,920
But in terms of experiences that are very much dependent and tied up with, tied to our reactions, our intentions, our inclinations, so much so that it seems like the whole world is in some ways, not perhaps entirely, but in some ways,

218
00:28:42,920 --> 00:28:55,920
dependent, equally dependent on our states of mind, our collective states of mind, if you want, that perhaps if you were to pick apart, suddenly pick people off of the world, you start to see the world change.

219
00:28:55,920 --> 00:29:06,920
In ways that we don't really have instruments that can detect this, but it very well may be and seems that if you were to erase people, if you could somehow just, if you were God, right?

220
00:29:06,920 --> 00:29:20,920
Just wave a magic wand and start zotting people, start disappearing people so that they just didn't exist anymore.

221
00:29:20,920 --> 00:29:35,920
That you'd see the world start to unravel, and that's why the whole idea of God, of course, is not a part of this, because what it means or what it implies is that potentially,

222
00:29:35,920 --> 00:29:40,920
if all the beings were gone, there would be no reality.

223
00:29:40,920 --> 00:29:49,920
Which helps you to sort of come closer to a sense of a mindful perspective of the world.

224
00:29:49,920 --> 00:29:58,920
As meditators, you feel this, that reality is your experiences and experiences are extending out into the world.

225
00:29:58,920 --> 00:30:10,920
There's a relationship between the objects of experience and the experience.

226
00:30:10,920 --> 00:30:24,920
And it's a very different way of looking at the world from this sort of object entity-based, impersonal reality-based way of looking at the classical physics.

227
00:30:24,920 --> 00:30:34,920
We're all trained through popular science and popular culture to think of the world this way as being entities that exist.

228
00:30:34,920 --> 00:30:47,920
People exist, things exist, this house, this village, this country, to think of them as existing.

229
00:30:47,920 --> 00:30:58,920
It's a very different perspective that you start to gain from meditation, you instead of feeling your legs or your stomach, you feel pain or you feel tension.

230
00:30:58,920 --> 00:31:05,920
Instead of your body being hot, you just experience the heat and you start to see that it's actually quite different.

231
00:31:05,920 --> 00:31:12,920
That the body is just something that we conceive of, the body starts to disappear.

232
00:31:12,920 --> 00:31:25,920
Instead of seeing the stomach or seeing it as the stomach, you see it as attention, you experience it as attention.

233
00:31:25,920 --> 00:31:41,920
Why this is important might not seem evident right away, but the point of this and the point of the meditation practice is that our observation of reality is essential

234
00:31:41,920 --> 00:32:01,920
to becoming free from suffering because the mechanics of how suffering arises and how suffering ceases are exclusively in this process of reality.

235
00:32:01,920 --> 00:32:04,920
Suffering doesn't come from people, that person doesn't make you suffer.

236
00:32:04,920 --> 00:32:11,920
And if you think like that, that is why we can never become free from suffering because what is our way?

237
00:32:11,920 --> 00:32:15,920
Eliminate the person, eliminate our relationship with them, run away from them.

238
00:32:15,920 --> 00:32:19,920
It's too cold, turn up the heat, it's too hot, turn up the cold.

239
00:32:19,920 --> 00:32:22,920
You're hungry, eat, you're thirsty and drink.

240
00:32:22,920 --> 00:32:26,920
You want something, get it, you don't want something, throw it away.

241
00:32:26,920 --> 00:32:35,920
That's how we conceive of suffering, when there's a problem, solve it.

242
00:32:35,920 --> 00:32:43,920
It's very different, it's a very different perspective from their experiences and I don't understand them.

243
00:32:43,920 --> 00:32:53,920
Their experiences and now I do understand them and that understanding frees us from suffering.

244
00:32:53,920 --> 00:33:00,920
So the Buddhist idea is that our suffering comes from our lack of understanding.

245
00:33:00,920 --> 00:33:11,920
I mean it comes directly from our reactions and our engagements with the world that are improper and problematic.

246
00:33:11,920 --> 00:33:21,920
But the only reason that we engage in such ways is because we don't realize that we don't understand that those ways of engaging are causing us suffering.

247
00:33:21,920 --> 00:33:32,920
The practice of mindfulness gives us a better perspective, allows us to see more clearly what we're doing, how we're engaging, what are our emotions, what are our states of mind.

248
00:33:32,920 --> 00:33:47,920
And begin to change and begin to refine our habits to the point where we engage with the world, engage with experience in a way that doesn't lead to suffering simply by understanding,

249
00:33:47,920 --> 00:33:52,920
by being familiar with the mechanics of it. How does reality work?

250
00:33:52,920 --> 00:34:12,920
How does reality work? It involves very much a causal relationship or a causal framework by which our emotions, our intentions, our inclinations affect reality.

251
00:34:12,920 --> 00:34:18,920
Not just make us a bad person, but they actually change our universe.

252
00:34:18,920 --> 00:34:31,920
You know, their stories about our beings who first came in contact with the planet Earth and were enchanted by it.

253
00:34:31,920 --> 00:34:38,920
So much so that they begin to engage in it and receive sustenance from it.

254
00:34:38,920 --> 00:34:52,920
These were like celestial beings of some sort. But by engaging with it, by getting excited by this physical course object, they became more course themselves, and they became stuck to it.

255
00:34:52,920 --> 00:34:57,920
And because of the course list, the Earth became more course as well.

256
00:34:57,920 --> 00:35:06,920
It describes the sort of evolution of course list. Take it as you will, but it comes eventually down to things like sickness.

257
00:35:06,920 --> 00:35:11,920
Only arising because our minds became sick because we started to become more sick in you.

258
00:35:11,920 --> 00:35:24,920
To some extent, you can see this. You know, as the Buddha said, the beginning of eating meat was a real instigator or trigger for a lot of sickness.

259
00:35:24,920 --> 00:35:34,920
And of course, there's no more pointed example of that in today's society than in the present situation,

260
00:35:34,920 --> 00:35:54,920
where apparently it didn't come from the sale of wildlife and exotic animals, but there's so many sicknesses that have come from things like meat and so much bacteria that you only find in blood and animals and meat.

261
00:35:54,920 --> 00:36:11,920
So many sicknesses that come from coarseness, you know, from our need, you know, the whole industrial revolution and the intensity of desire that it involved.

262
00:36:11,920 --> 00:36:19,920
It wasn't just the products like bad food, processed foods. It wasn't just them that started making a sick.

263
00:36:19,920 --> 00:36:34,920
They were just concepts that are a part of this sickness of mind that was so intently fixated on pleasure as opposed to peace.

264
00:36:34,920 --> 00:36:43,920
And we went to war and the war, how much sickness comes from war, how much sickness comes from industry.

265
00:36:43,920 --> 00:36:57,920
From simply the living in cities, which are ultimately about maximizing our wealth and pleasure in a way that you can't find living in the forest.

266
00:36:57,920 --> 00:37:04,920
You can't find living in the country. The sickness, the sicknesses that come from all of this.

267
00:37:04,920 --> 00:37:14,920
And in an opposite sense, the way a person's world changes when they begin to do good deeds, when they begin to be kind.

268
00:37:14,920 --> 00:37:21,920
People who do these sorts of things start to see that their world changes, their friends change.

269
00:37:21,920 --> 00:37:29,920
Sometimes it seems in miraculous sorts of ways. So whether you believe in that extended sort of them, it's an interesting idea.

270
00:37:29,920 --> 00:37:52,920
It doesn't really matter what's important is in general and in its entirety for us to begin to see more clearly the causal relationship between our experiences and our reactions to our experiences.

271
00:37:52,920 --> 00:38:00,920
And our reactions and our intentions and the world around us, the things we experience.

272
00:38:00,920 --> 00:38:29,920
So that's the Dhamma Padafir tonight. Thank you all for listening.

