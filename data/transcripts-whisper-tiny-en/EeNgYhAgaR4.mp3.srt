1
00:00:30,000 --> 00:00:47,280
Good evening everyone, welcome back to the evening dumber, tonight we are ending the

2
00:00:47,280 --> 00:01:02,160
sabbasavasita and we've recently been over the topic of this section so I'm not

3
00:01:02,160 --> 00:01:06,960
going to go into great detail but it's the kind of thing that it's okay to go over

4
00:01:06,960 --> 00:01:13,080
tonight we're looking at the taints or defilements to be abandoned by

5
00:01:13,080 --> 00:01:26,480
developing or cultivating bawana, bawana is a very important word in Buddhism, bawat comes

6
00:01:26,480 --> 00:01:35,080
from bhu means existence or to be being, it's a simple word if you talk about

7
00:01:35,080 --> 00:01:45,320
something existing you say it, it exists bawati, bawat, bawa is something coming into

8
00:01:45,320 --> 00:02:01,720
being, it's existing or being, but bawana means causing something to be cut to be

9
00:02:01,720 --> 00:02:09,040
it's an important word because it describes Buddhist practice quite well

10
00:02:09,040 --> 00:02:22,720
Buddhist practice isn't about relaxing or running away Buddhism is very much

11
00:02:22,720 --> 00:02:33,360
about developing and cultivating some meditation this is why I have to repeat

12
00:02:33,360 --> 00:03:01,400
in the, let me get the door, why is someone marching into our house, this is new, so

13
00:03:01,400 --> 00:03:06,920
the idea is to become something, to not become something sorry, to cause things

14
00:03:06,920 --> 00:03:21,280
to come into being and specific things, we practice meditation, we are not just, what's the

15
00:03:21,280 --> 00:03:35,160
problem, I guess now that I've got the speakers on people can hear us outside, that's

16
00:03:35,160 --> 00:03:47,560
interesting, we're trying to cultivate certain qualities, so meditation isn't just

17
00:03:47,560 --> 00:03:52,520
about closing your eyes and blanking your mind, there's quite a bit more to

18
00:03:52,520 --> 00:03:57,120
meditation than people think, it's not even just a question about it being

19
00:03:57,120 --> 00:04:06,440
about relaxing, meditation isn't exactly about one single thing in their

20
00:04:06,440 --> 00:04:14,320
ways of summing it up, but there's quite a bit involved with meditation in many

21
00:04:14,320 --> 00:04:23,480
qualities of mind, many facets to the quality of mind that we're trying to

22
00:04:23,480 --> 00:04:45,480
cultivate meditation and Buddhist practice in general involves producing,

23
00:04:45,480 --> 00:04:52,880
coming, causing to come into being, into being, many qualities of mind, we're talking

24
00:04:52,880 --> 00:05:03,600
about qualities of mind that we're weak or non-existent before, we're not

25
00:05:03,600 --> 00:05:10,800
talking about sound physical, not talking about cultivating anything

26
00:05:10,800 --> 00:05:16,680
physical, Buddhism isn't about building monasteries or shaving off your

27
00:05:16,680 --> 00:05:24,320
head, your hair putting on special clothes, it's not about getting to the

28
00:05:24,320 --> 00:05:32,440
point where you can sit in full-load this posture, we're cultivating here's

29
00:05:32,440 --> 00:05:42,640
qualities of mind, that's something I think takes a little bit of patience, but

30
00:05:42,640 --> 00:05:49,640
should be a focus when we have the question of what are we getting out of this

31
00:05:49,640 --> 00:05:57,600
practice, where focus is too often on how happy is it making me, right, which is

32
00:05:57,600 --> 00:06:12,760
fine and good, but it smacks of potential impatience and addiction really to

33
00:06:12,760 --> 00:06:20,280
pleasure, right, if I say to you meditation is it's supposed to make you happy,

34
00:06:20,280 --> 00:06:26,040
well, we tend to avoid all the work, we tend to ignore all the work that has to

35
00:06:26,040 --> 00:06:33,040
be done and focus very much on that happiness, when in fact it's our

36
00:06:33,040 --> 00:06:42,760
obsession with happiness that makes us unhappy, if you want to talk about

37
00:06:42,760 --> 00:06:47,800
success and progress in the practice, you have to ask about your state of mind

38
00:06:47,800 --> 00:06:54,240
because happiness isn't something that's easy to come by, happiness requires a

39
00:06:54,240 --> 00:07:03,360
strong and healthy and pure mind, a well-developed mind, so bhavana, the Buddha

40
00:07:03,360 --> 00:07:11,440
said, a bhavitang zitang raghu samatiri jity, just like rain penetrates and

41
00:07:11,440 --> 00:07:21,880
undeveloped and poorly-thached roof, so to the undeveloped mind a bhavitang

42
00:07:21,880 --> 00:07:32,400
zitang, allows the defilements in, the defilements meaning those things that get us

43
00:07:32,400 --> 00:07:38,840
caught up in addiction and aversion and suffering, we suffer because our minds

44
00:07:38,840 --> 00:07:42,920
are undeveloped, not because we're working too hard, but because we're not

45
00:07:42,920 --> 00:07:51,760
really working hard enough, we're not working in the right way, if you want to

46
00:07:51,760 --> 00:07:54,920
be happy there are many qualities that you have to develop, most importantly

47
00:07:54,920 --> 00:08:03,960
mindfulness and this is the first one, mindfulness always is the first, it's

48
00:08:03,960 --> 00:08:11,760
very important that it be placed at the beginning, it's the one the Buddha said

49
00:08:11,760 --> 00:08:18,360
is always useful when you refer to the other, the bowjang is what we're supposed

50
00:08:18,360 --> 00:08:25,360
to be developing are the seven factors of enlightenment, bowjang, bowdianga,

51
00:08:25,360 --> 00:08:36,560
angas, just a word that means member, bowdi means enlightenment, bowdi is

52
00:08:36,560 --> 00:08:44,640
that moment when everything comes together, when one is perfect in all the

53
00:08:44,640 --> 00:08:53,320
many factors and qualities, faculties and qualities of mind that come

54
00:08:53,320 --> 00:09:02,320
together to create enlightenment, that's bowdi. So these seven factors are the

55
00:09:02,320 --> 00:09:09,280
seven things that cultivate and have been cultivated, realize bowdi,

56
00:09:09,280 --> 00:09:18,360
realize enlightenment, sati is the first, we'll talk a lot about sati and of course

57
00:09:18,360 --> 00:09:24,160
this is how we come to see, this is as we talked about last time, how we abandon

58
00:09:24,160 --> 00:09:31,400
the defilements temporarily and with the pure mind that comes from being

59
00:09:31,400 --> 00:09:42,520
mindful, we're able to abandon and free ourselves, see clearly and through

60
00:09:42,520 --> 00:09:57,280
seeing clearly, free ourselves. So once we've developed sati then there comes

61
00:09:57,280 --> 00:10:04,000
dhammavicaya which is means in regards to the dhammas, the understanding that

62
00:10:04,000 --> 00:10:10,240
comes about, the shedding the light, mindfulness is like a very powerful light

63
00:10:10,240 --> 00:10:17,080
and dhammavicaya is where you analyze, not intellectually but you start to

64
00:10:17,080 --> 00:10:27,120
react really or not react but taste, taste your reactions, dhammavicaya is

65
00:10:27,120 --> 00:10:32,520
where you start to discriminate, where you really start to be able to judge

66
00:10:32,520 --> 00:10:38,480
properly. We talk a lot about how judgment is a real problem but it's only

67
00:10:38,480 --> 00:10:44,520
wrong judgment that's a problem or ignorant judgment. Once you start to be

68
00:10:44,520 --> 00:10:47,880
able to see, once you see things clearly, you start to be able to judge things

69
00:10:47,880 --> 00:10:58,080
properly and undeniably discriminate between good and bad. This is undeniably

70
00:10:58,080 --> 00:11:06,400
bad, why? Well, I've seen it with perfect clarity, that's what we do in the

71
00:11:06,400 --> 00:11:10,080
practice, it's like picking up the bad stuff and throwing it away and sorting

72
00:11:10,080 --> 00:11:15,480
everything into good and bad, not because someone told you so or because you

73
00:11:15,480 --> 00:11:26,600
think so or because you feel so, because you know so and you see. Once you do

74
00:11:26,600 --> 00:11:38,560
that, well, to develop, you need to develop effort so you can have mindfulness

75
00:11:38,560 --> 00:11:44,920
and through mindfulness comes this discrimination but for it to succeed, you

76
00:11:44,920 --> 00:11:49,240
need effort, the third one is effort. This means the first two are really the

77
00:11:49,240 --> 00:11:56,280
practice but for it to be successful, you need to put effort in. You can't just

78
00:11:56,280 --> 00:12:03,440
do it once and give up, right? Sit down for ten minutes and that was that. Now

79
00:12:03,440 --> 00:12:08,800
I've meditated, I'm a meditator, something that you have to do with Buddha

80
00:12:08,800 --> 00:12:14,760
and Satatachakiri, I was saying now, I'm going to get into the habit or the activity

81
00:12:14,760 --> 00:12:24,520
of this continuous, Satatachanins, continuous mindfulness and investigation.

82
00:12:24,520 --> 00:12:30,240
It means when you're eating, eating should become a meditation. When you're walking

83
00:12:30,240 --> 00:12:35,680
around, sitting down, lying down to sleep. When you're in the shower, when you're on

84
00:12:35,680 --> 00:12:43,920
the toilet, everything you do, every moment, there's a moment when you can be

85
00:12:43,920 --> 00:12:52,600
cultivating these qualities, takes effort. But once you cultivate effort and the

86
00:12:52,600 --> 00:12:56,800
effort comes, another thing about it is once you're mindful and you start to see

87
00:12:56,800 --> 00:13:03,160
things clearly, you free up a lot of effort. When we talk about being tired, we

88
00:13:03,160 --> 00:13:07,960
think of being tired is because you've worked too hard, but it's oftentimes not

89
00:13:07,960 --> 00:13:14,800
even a physical tired. It's a mental stiffness. It's like a tension of the mind

90
00:13:14,800 --> 00:13:19,840
really. And once you're mindful, you suddenly feel all this energy come back.

91
00:13:19,840 --> 00:13:28,000
Gradually feel all this energy come back. As you start to let go and ease up,

92
00:13:28,000 --> 00:13:35,160
your mind becomes more focused and more alert. And you gain more energy and

93
00:13:35,160 --> 00:13:46,720
then the final of the energy side is rapture. And so once you become energetic,

94
00:13:46,720 --> 00:13:57,160
then you start to become, it becomes habitual. So rapture is this state of gaining

95
00:13:57,160 --> 00:14:04,280
momentum, really, becoming powerful. Through putting out effort, it becomes

96
00:14:04,280 --> 00:14:14,240
stronger and more, it's like inertia, really. When it starts to become, you get in

97
00:14:14,240 --> 00:14:25,520
the groove, it becomes habitual and easier, really, more efficient. Rapture

98
00:14:25,520 --> 00:14:29,600
refers to anything you get caught up in, really. That's why we use the word

99
00:14:29,600 --> 00:14:35,400
rapture. So it's easy to get caught up in many different things. Here you start

100
00:14:35,400 --> 00:14:40,360
to get caught up in mindfulness. And that's a really good sign. Once it becomes

101
00:14:40,360 --> 00:14:46,200
second nature, where you just mindful constantly throughout the day, or it

102
00:14:46,200 --> 00:14:51,240
becomes very much a part of who you are, that's rapture. You become in rapture

103
00:14:51,240 --> 00:14:59,400
by mindfulness, sort of, what a great thing. So those ones, well mindfulness is in

104
00:14:59,400 --> 00:15:05,240
the center, but the other three are on the effort side. And then we have, on the

105
00:15:05,240 --> 00:15:10,360
other side of things, your mind becomes calmer, so bussed in, trying it, your

106
00:15:10,360 --> 00:15:15,680
mind becomes more tranquil, samadhi, your mind becomes more focused. And to

107
00:15:15,680 --> 00:15:21,800
be a guy, your mind, this is the most important one, your mind becomes more

108
00:15:21,800 --> 00:15:29,520
equanimous. And once you've completely tranquilized the mind and the body, to the

109
00:15:29,520 --> 00:15:40,240
point where you're able to be still, and you feel very sorted out every

110
00:15:40,240 --> 00:15:47,560
experience, it comes orderly. It's still chaos, but it's a manageable chaos, and

111
00:15:47,560 --> 00:15:54,520
there's a quiet tune to it, the stillness, there's no more defilement really.

112
00:15:55,320 --> 00:16:01,360
Samadhi means you're focused, there's perfect clarity of focus on each object

113
00:16:01,360 --> 00:16:12,000
as it arises. And dupeika means you have no judging. These are the qualities that

114
00:16:12,000 --> 00:16:16,600
we try to develop. We put mindfulness in the center, the other ones are on the

115
00:16:16,600 --> 00:16:21,840
side of effort and concentration. So if you have a lot of effort, then you have

116
00:16:21,840 --> 00:16:26,480
to be clear that you're a lot of energy over energy, you're distracted and

117
00:16:26,480 --> 00:16:31,960
restless. Then you have to be clear about that and start to focus on the other

118
00:16:31,960 --> 00:16:40,200
three, calming your mind down, focusing a bit better, trying to be less jumpy and

119
00:16:40,200 --> 00:16:47,360
less judgmental or reactionary, more equanimous. I mean, just acknowledging those

120
00:16:47,360 --> 00:16:52,760
things helps you focus on, focus your mindfulness really. Mindfulness is the

121
00:16:52,760 --> 00:16:57,640
tool you don't have to get too distracted by this idea of balancing things. It's

122
00:16:57,640 --> 00:17:01,760
more of an intellectual thing for you to step back and say, uh-huh. Well, that's

123
00:17:01,760 --> 00:17:05,560
where I'm obviously not being mindful or it's harder for me to be mindful

124
00:17:05,560 --> 00:17:11,200
because I'm out of balance. And so knowing this helps you to focus your

125
00:17:11,200 --> 00:17:18,520
mindfulness and make focus your efforts to be more mindful of those things

126
00:17:18,520 --> 00:17:27,600
that are important. So this one, this one really pairs with the first one. Remember,

127
00:17:27,600 --> 00:17:34,320
we had the first one was seeing in this suit. So seeing is our practice. We're

128
00:17:34,320 --> 00:17:38,760
trying to see the truth. It's a very lofty sounding goal, but it's quite simple.

129
00:17:38,760 --> 00:17:45,560
It is lofty, but it's simple. We're trying to understand what's going on. Why are

130
00:17:45,560 --> 00:17:51,320
we suffering? We're trying to understand our minds, understand our experience,

131
00:17:51,320 --> 00:18:02,360
understand reality as a mundane thing, as a process of experience that causes us

132
00:18:02,360 --> 00:18:14,040
so much stress and suffering and to change that. To fix that, really. Fix that

133
00:18:14,040 --> 00:18:17,760
we're seeing through, through seeing the problem, understanding the problem,

134
00:18:17,760 --> 00:18:26,880
and naturally inclining or changing the way we incline our minds. And so the

135
00:18:26,880 --> 00:18:33,360
other five that we've been through up to today are more supportive practices.

136
00:18:33,360 --> 00:18:41,040
And indeed, each one of those supportive practices cuts off a part of the problem.

137
00:18:41,040 --> 00:18:50,840
And these are so much, which means these sort of threads that are getting us off

138
00:18:50,840 --> 00:19:02,760
balance that are leading to stress and suffering, really. But the main

139
00:19:02,760 --> 00:19:07,080
practice is still seeing. And Bhavan has just come bringing it back home that as

140
00:19:07,080 --> 00:19:11,400
you start to see and start practicing, there are qualities that are your

141
00:19:11,400 --> 00:19:16,080
cultivating. So Bhavan is the qualities. It's the same with the Satipatanas who

142
00:19:16,080 --> 00:19:20,320
are right. The Buddha goes through the Satipatanas who've done it at the very

143
00:19:20,320 --> 00:19:26,560
end. Well, before getting into the truth, he talks about the factors of

144
00:19:26,560 --> 00:19:32,920
enlightenment. We're developing through the practice. So this is this section,

145
00:19:32,920 --> 00:19:42,400
in these seven things, mindfulness, investigation, effort, rapture, tranquility,

146
00:19:42,400 --> 00:19:47,160
concentration, and equanimity. These are the seven factors of enlightenment.

147
00:19:52,200 --> 00:19:55,240
This is something interesting about each other that I didn't mention. It's

148
00:19:55,240 --> 00:20:18,480
one of these factors is based on, based on seclusion, dispassion, cessation, and

149
00:20:18,480 --> 00:20:38,160
as giving up relinquishment as its ending, as its result. So the point

150
00:20:38,160 --> 00:20:43,840
the stress here is on the fact that these are the qualities that lead to

151
00:20:43,840 --> 00:20:49,880
seclusion or are associated with seclusion, dispassion, and cessation.

152
00:20:49,880 --> 00:21:00,480
These are the qualities of mind. These are them. These are the way to

153
00:21:00,480 --> 00:21:06,040
true seclusion in the sense of being secluded from the defilements, where

154
00:21:06,040 --> 00:21:12,960
your mind is away from all that heat and stress and fever of defilement.

155
00:21:12,960 --> 00:21:21,480
Well, dispassion and cessation, the cessation of suffering. And have letting go

156
00:21:21,480 --> 00:21:24,920
is there ends. So we talk about letting go a lot in Buddhism. How does one

157
00:21:24,920 --> 00:21:36,160
let go? Letting go is the result, barinami. It's the ending. And it comes from

158
00:21:36,160 --> 00:21:41,360
all of these. It comes when you're mindful and when you develop all the

159
00:21:41,360 --> 00:21:53,520
qualities that lead to enlightenment. And so in conclusion the Buddha says when

160
00:21:53,520 --> 00:22:00,240
one has removed all of these defilements, the ones through seeing that should

161
00:22:00,240 --> 00:22:09,000
be abandoned by seeing the ones through using, through enduring, through

162
00:22:09,000 --> 00:22:17,000
avoiding, through removing, and through developing. Then one is called a

163
00:22:17,000 --> 00:22:23,600
bikhu, who dwells restrained with restraint of all the tanes. It's not quite the

164
00:22:23,600 --> 00:22:27,760
language I would use. It's a difficult language.

165
00:22:27,760 --> 00:22:40,600
It dwells restrained, dwells restrained, having restrained all of the ever

166
00:22:40,600 --> 00:22:57,480
strained is not the best word. Sabasa, as samma, or sambu-toh, having restrained them,

167
00:22:57,480 --> 00:23:06,240
I guess. So there's the ass of again, it's kind of this metaphor of

168
00:23:06,240 --> 00:23:17,160
streams or this, it's an imagery of these things that get us lost. It's like a

169
00:23:17,160 --> 00:23:25,320
leak. So it means you've plugged all the leaks. Our mind ordinarily is at

170
00:23:25,320 --> 00:23:32,040
peace. Why can't we just be here and now? Ordinary reality as we describe it

171
00:23:32,040 --> 00:23:40,520
and as we think of it is really pure, but it's the ass of that get us impure. They

172
00:23:40,520 --> 00:23:51,400
get us caught up in busyness and stress and friction. They're the poison. And so

173
00:23:51,400 --> 00:23:57,240
once one has done that, done this, one is plugged up on leaks.

174
00:23:57,240 --> 00:24:12,520
A ji-ji-tah-n-hong, one has cut off craving. We, what the yi-i-i-i-sang-yo-jah-n-hong, one is untied,

175
00:24:12,520 --> 00:24:29,520
we, we what they, I don't know, one is pickabodhi-se, flung off the fetters, samma-mana-bisamaya-antamakasi-dukasa,

176
00:24:29,520 --> 00:24:37,180
with the complete penetration of conceit is made the end of suffering, made an

177
00:24:37,180 --> 00:24:45,400
end of suffering. So again, what's great about this suit does, it gives a

178
00:24:45,400 --> 00:24:52,120
really comprehensive look at the practice. I mean, there's much more that could

179
00:24:52,120 --> 00:24:57,600
be said about each one of these and there's many details and many ways of

180
00:24:57,600 --> 00:25:02,360
implementing all the aspects of the practice, but this gives a good summary of

181
00:25:02,360 --> 00:25:08,840
Buddhist practice really, and it comes back, starts at seeing and comes back

182
00:25:08,840 --> 00:25:20,800
to cultivating, and really gives us some good pointers and good reminders on how

183
00:25:20,800 --> 00:25:26,840
to keep our practice going and keep it going in the right direction. So that's

184
00:25:26,840 --> 00:25:40,080
the samasa-visuta, and that's the demo for tonight. You can see if there are

185
00:25:40,080 --> 00:25:49,960
questions. Is it useful to expose ourselves the situations we are averse to in

186
00:25:49,960 --> 00:25:54,080
order to learn about them? For example, I have an aversion to reading, so it

187
00:25:54,080 --> 00:25:58,600
would be useful to try to read something and study my reactions using the

188
00:25:58,600 --> 00:26:04,440
noting technique. I think I just answered this one. I mean, it's not really a

189
00:26:04,440 --> 00:26:11,200
good idea to intentionally evoke defilements because then you're

190
00:26:11,200 --> 00:26:19,760
cultivating that triggering activity. I mean, part of what we're trying to learn

191
00:26:19,760 --> 00:26:29,560
is neutrality or objectivity, and if you're triggering it, there's the real

192
00:26:29,560 --> 00:26:37,560
danger of getting impatient or ambitious about the practice. If I do this, is it

193
00:26:37,560 --> 00:26:42,000
useful, like this question, or is it useful? It's potentially ambitious,

194
00:26:42,000 --> 00:26:50,200
where instead you should just live your life and then add mindfulness, right? Let

195
00:26:50,200 --> 00:26:55,840
the changes come naturally. I mean, instead of going out of your way to read a

196
00:26:55,840 --> 00:27:00,960
book, when you actually naturally are in a position where reading is the

197
00:27:00,960 --> 00:27:07,840
appropriate thing to do, then study it. It's more natural that way. There's a

198
00:27:07,840 --> 00:27:15,920
problem with sort of artificially evoking defilements, except in the most

199
00:27:15,920 --> 00:27:20,560
innocuous sorts of ways, like doing sitting meditation, walking meditation,

200
00:27:20,560 --> 00:27:27,760
potentially evokes quite a few problematic mind states, but it's quite benign,

201
00:27:27,760 --> 00:27:36,040
I think, in the sense of it's just walking and it's just sitting, right? But going

202
00:27:36,040 --> 00:27:43,600
out of your way, I think, is problematic. Who is Sonamahate or that Sayadha

203
00:27:43,600 --> 00:27:53,280
mentions on page 87, a manual to insert? There are two Sonas, but the main one is

204
00:27:53,280 --> 00:28:07,200
is about entered effort. He was a person who had too much push too hard. So I think

205
00:28:07,200 --> 00:28:11,360
this is where the seemingly of the three strings comes in, right? You have

206
00:28:11,360 --> 00:28:24,480
overzealousness with this, 87. We have rogue efforts made by Sonamahate,

207
00:28:24,480 --> 00:28:34,480
this is different, right? Thinking about teacher reflecting on the attributes

208
00:28:34,480 --> 00:28:41,320
considering the heroic efforts made by Sona. So Sona was so, if you want to know

209
00:28:41,320 --> 00:28:47,400
about any of these sorts of people, the dictionary of polypropernames is a

210
00:28:47,400 --> 00:28:51,560
really good resource. It can look up Sona there. I don't give you all the

211
00:28:51,560 --> 00:28:59,680
information, but he was so delicate that there were hairs apparently growing on

212
00:28:59,680 --> 00:29:04,880
the bottoms of his feet. And the king, I think, even asked to look at his feet

213
00:29:04,880 --> 00:29:09,520
because he was astonished that someone could possibly have hair there. But he had

214
00:29:09,520 --> 00:29:15,040
these fine hairs on the bottom of his feet. He was so delicate, apparently. And

215
00:29:15,040 --> 00:29:20,280
so when he became a monk, he did walking meditation and his feet started

216
00:29:20,280 --> 00:29:28,160
bleeding. I'm hoping getting the story right, but his feet started bleeding, but

217
00:29:28,160 --> 00:29:34,640
he persisted. And it was because of him, I think, the Buddha eventually allowed

218
00:29:34,640 --> 00:29:44,800
the monks to our sandals. Monks would go barefoot before that. But at one

219
00:29:44,800 --> 00:29:51,920
point it was, I think it was an abrama. I don't think it was the Buddha. The

220
00:29:51,920 --> 00:29:55,680
similarly of these three strings, if one is too, if a string is too tight, not

221
00:29:55,680 --> 00:30:01,240
may have been the Buddha. The string is too tight. It snaps. If a string is too

222
00:30:01,240 --> 00:30:06,880
loose, it doesn't play. The string has to be perfectly tuned. And that's how

223
00:30:06,880 --> 00:30:12,240
your effort should be. You shouldn't just push too hard. So he worked really

224
00:30:12,240 --> 00:30:20,280
hard, and that's a good thing, but in the end he worked too hard. But I

225
00:30:20,280 --> 00:30:24,920
recommend looking up the dictionary of polypropernames because I may have

226
00:30:24,920 --> 00:30:33,920
butchered some of that. Could you go over there not the luck and the suit next?

227
00:30:33,920 --> 00:30:49,160
Sure. I'll do that maybe Saturday. I want to become a monk. How can I go ahead?

228
00:30:49,160 --> 00:30:53,040
I'm not going to answer questions about how to become a monk because I think

229
00:30:53,040 --> 00:30:59,920
it's misguided and no misleading. It's problematic because people are

230
00:30:59,920 --> 00:31:06,560
doing people put there's too much. It's often the case that people put too

231
00:31:06,560 --> 00:31:19,160
much attention and attention to ordination. And that actually takes away from

232
00:31:19,160 --> 00:31:24,120
people's attention to meditation. It's seen as sort of a quick fix. And that

233
00:31:24,120 --> 00:31:30,920
causes problems from monastery. So I shy away from focusing on that aspect of

234
00:31:30,920 --> 00:31:39,600
Buddhism. Ordination is certainly possible, but it should be related to your

235
00:31:39,600 --> 00:31:44,920
meditation practice. And in that case, it'll come to that for them. You'll be

236
00:31:44,920 --> 00:31:54,560
at a center. And the opportunity will arise and you'll just take it. I mean, part

237
00:31:54,560 --> 00:31:58,160
of the problem is that it's hard to find a center where it's a good place to

238
00:31:58,160 --> 00:32:03,760
ordain. And it's hard for me to give a recommendation because there are many

239
00:32:03,760 --> 00:32:09,680
different, whether lots of obstacles to becoming a monk and for those who

240
00:32:09,680 --> 00:32:18,200
have become a monk in modern times. Come from a Jewish family. My family is

241
00:32:18,200 --> 00:32:23,640
worried that one day I might get the idea of becoming a monk. They say for years

242
00:32:23,640 --> 00:32:28,360
I slowly transform to a fundamentalist in my views through Buddhism. How could I

243
00:32:28,360 --> 00:32:32,840
deal with it with it with it's ignorance? It's very difficult to talk to people

244
00:32:32,840 --> 00:32:44,560
of no idea of them. My patients and practice on your own. And once you become

245
00:32:44,560 --> 00:32:50,920
very comfortable in the practice, people can't help but over time see that it's

246
00:32:50,920 --> 00:32:59,320
benefiting you. I mean, well, they can. But it becomes a matter of disassociating

247
00:32:59,320 --> 00:33:08,040
yourself with such people or bringing them over to your side. Where they accept

248
00:33:08,040 --> 00:33:11,640
the fact that it's a good thing.

249
00:33:17,600 --> 00:33:21,520
I think I'm sorry if that doesn't really answer your question, but it's not

250
00:33:21,520 --> 00:33:25,200
really an easily answered question. You can't just fix people's ignorance. You

251
00:33:25,200 --> 00:33:33,320
can't just solve their concerns. I could very well be sorry put his mother

252
00:33:33,320 --> 00:33:40,960
he was scolding him to the very end. Sorry put to who was great and the

253
00:33:40,960 --> 00:33:46,080
great wisdom. And it took until he was dying for his mother to see the

254
00:33:46,080 --> 00:34:10,200
goodness of what he was doing. There's another question about being a monk and

255
00:34:10,200 --> 00:34:19,360
just deleted it. Yeah, I'm not really going to answer that. Again, I don't want

256
00:34:19,360 --> 00:34:24,760
people to fixate on this idea of being a monk. I know it's very romantic and

257
00:34:24,760 --> 00:34:29,000
everyone wants there's many questions about it. It's quite an interest and

258
00:34:29,000 --> 00:34:38,520
that's a good thing. But maybe it's not such a good thing. I mean, I think I would

259
00:34:38,520 --> 00:34:44,760
discourage that and I would encourage people to focus more on meditation. And

260
00:34:44,760 --> 00:34:53,880
because becoming a monk, if you have to ask me, you're probably not there yet.

261
00:34:53,880 --> 00:34:58,840
The only time it should be asked is when you're in a meditation, in a

262
00:34:58,840 --> 00:35:09,600
monastery and you're talking to your monastic teacher and you're just looking

263
00:35:09,600 --> 00:35:15,160
to further your meditation practice that you're already undertaking. If you're

264
00:35:15,160 --> 00:35:23,320
asking me on the internet, I would say stick to your meditation for now or find

265
00:35:23,320 --> 00:35:30,360
a place to go and do a meditation course. It's much more practical,

266
00:35:30,360 --> 00:35:40,040
especially in modern times, to focus in that way. So that's all for tonight. Thank

267
00:35:40,040 --> 00:35:55,960
you all for tuning in. Have a good night.

