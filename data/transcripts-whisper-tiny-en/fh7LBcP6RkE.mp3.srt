1
00:00:00,000 --> 00:00:06,360
Hello, welcome back to as among next question comes from jazz guitar LMS

2
00:00:07,440 --> 00:00:18,320
Who asks why do I feel I need to get back at people when they haven't treated me the way I feel I deserve to be treated in order to regain my balance

3
00:00:19,920 --> 00:00:21,920
Good question

4
00:00:21,920 --> 00:00:24,360
I think

5
00:00:24,360 --> 00:00:29,960
We tend to focus on the anger element of revenge

6
00:00:32,280 --> 00:00:39,160
Thinking that when someone hurts us it makes us angry and that anger is a cause for us to want to get back at them

7
00:00:39,160 --> 00:00:43,720
And I don't think that is fair a fair assessment

8
00:00:43,720 --> 00:00:52,800
I think the key in in in seeking out revenge though anger plays an important part the key is the the delusion the ego

9
00:00:52,800 --> 00:00:59,920
That's involved and and the key it's highlighted in this question that people treat us the way we

10
00:01:01,120 --> 00:01:04,680
In a way that we feel we don't deserve to be treated so

11
00:01:05,880 --> 00:01:13,840
You know moreover we feel that they've gotten the better of us and they've shown us up they've done something that

12
00:01:14,840 --> 00:01:16,080
you know

13
00:01:16,080 --> 00:01:21,400
has somehow lessened our sense of self worth and so

14
00:01:21,400 --> 00:01:28,760
When it come if someone's attacking you now the reason you might fight back or if someone's doing something that annoys you

15
00:01:28,760 --> 00:01:35,200
The reason you might hurt them is out of anger. That's clear if it's immediate the anger is a disliking of

16
00:01:36,320 --> 00:01:41,040
of a certain stimulus and and and you know the

17
00:01:41,680 --> 00:01:44,480
The violence is is based on the anger

18
00:01:44,720 --> 00:01:49,960
But when you you're keeping it in your mind and thinking that person did me wrong that person hurt me

19
00:01:49,960 --> 00:01:57,240
It's all about the eye. It's all about our sense of of attachment to self, which is very much ego based

20
00:01:57,760 --> 00:02:02,160
So I think the reason why we want to get back at people

21
00:02:03,560 --> 00:02:08,200
It is is based on our sense of ego that that they have somehow

22
00:02:09,000 --> 00:02:16,200
You know, and this is this feeling of balance that they are now one up on us, which is ego. It's not anger

23
00:02:16,200 --> 00:02:20,800
It's it's it's an attachment to ours. Our status

24
00:02:21,400 --> 00:02:27,000
Our station in life that that we want to be on top and we don't want people to push us down and

25
00:02:27,240 --> 00:02:29,880
We want to be equal and so on and

26
00:02:30,600 --> 00:02:35,880
This is where the idea of the sense of balance comes in. So it's an interesting theory to think that

27
00:02:35,880 --> 00:02:44,920
If someone's hurt us, we have a right to hurt them back to regain balance. You know, it's a scientific sort of argument that

28
00:02:46,080 --> 00:02:50,000
You know something is out of balance and to put it back in the balance. You have to

29
00:02:50,760 --> 00:02:53,760
you have to to take revenge and

30
00:02:54,840 --> 00:03:02,360
Of course, that's not the truth because scientifically what we're talking about is a

31
00:03:02,360 --> 00:03:05,320
disruption of a

32
00:03:05,960 --> 00:03:07,960
peace a disruption of

33
00:03:11,320 --> 00:03:13,320
Stability or harmony or

34
00:03:14,680 --> 00:03:16,680
disruption of the

35
00:03:16,680 --> 00:03:18,680
Calm of the of

36
00:03:19,240 --> 00:03:21,160
Universe of the existence of the mind

37
00:03:21,480 --> 00:03:27,080
So this person hurting us has created a stress in their mind and a stress in our mind

38
00:03:27,080 --> 00:03:34,520
It's created a lot of things that didn't exist before it's created states of of anger states of ego states of

39
00:03:36,520 --> 00:03:38,520
Vengeance and so on so

40
00:03:39,160 --> 00:03:45,640
Hitting hurting someone back is only going to create more of those states. It's going to create more anger and

41
00:03:46,840 --> 00:03:51,800
If the other person has ego, which they probably do because that's why they've treated us in such a way

42
00:03:52,440 --> 00:03:53,640
then

43
00:03:53,640 --> 00:04:01,480
They're going to you know give rise to feelings of revenge and so on feelings of wanting to get the better of us and so on and

44
00:04:02,040 --> 00:04:04,920
The only way and this is born on reality

45
00:04:04,920 --> 00:04:10,280
I don't think anyone really believes that it's an eye for an eye if someone hurts you the best way to

46
00:04:11,080 --> 00:04:16,120
Regain the equilibrium is to hurt them back because we can see it's not born out in reality

47
00:04:16,840 --> 00:04:19,400
the only way to become free from the

48
00:04:19,400 --> 00:04:28,120
The stress that's been created the disharmony is for one side or both sides to finally give up and let go and

49
00:04:29,000 --> 00:04:31,000
To let to a to absorb

50
00:04:31,400 --> 00:04:33,400
the impact of the the

51
00:04:33,720 --> 00:04:38,920
Stress and the disharmony of someone hurts you to absorb it and to experience it and to let it

52
00:04:39,640 --> 00:04:43,960
reverberate in your mind so to speak until it slowly goes away

53
00:04:45,240 --> 00:04:45,800
and

54
00:04:45,800 --> 00:04:54,440
You know, it's it's kind of like giving that the taking on the disharmony of another person without reacting to it and creating more disharmony

55
00:04:54,920 --> 00:04:55,880
so

56
00:04:55,880 --> 00:04:58,360
and you know expanding and

57
00:05:00,520 --> 00:05:06,920
And multiplying it and I mean, I don't have there's obviously no scientific data to support this

58
00:05:06,920 --> 00:05:11,720
But you can get a feeling for it if you you know deal with these sorts of situations

59
00:05:11,720 --> 00:05:17,800
And you can see what's going on that there is ego involved and that's creating more stress and tension and so on

60
00:05:18,360 --> 00:05:22,760
And if you let go of the ego and you think you know that person hurt me

61
00:05:22,760 --> 00:05:29,000
But what is me what they've heard is simply you know a physical and mental flux of of experience

62
00:05:29,240 --> 00:05:30,440
There's no

63
00:05:30,440 --> 00:05:39,640
Self involved the Buddha said it's like the dirt the earth if someone urinates and defecates on the earth and and digs on the earth and and

64
00:05:39,640 --> 00:05:43,720
And and kicks at the dirt does the does the earth

65
00:05:44,920 --> 00:05:46,760
Get angry

66
00:05:46,760 --> 00:05:48,760
if someone you know is

67
00:05:49,000 --> 00:05:54,920
blowing at fire or water or kicking at water urinating and the water does the water get angry and so on and

68
00:05:55,240 --> 00:05:59,480
He said so this is really all we are as the four elements are were were physical

69
00:06:00,440 --> 00:06:05,080
You know when someone hits us they're only hitting the physical and the physical doesn't get angry about it

70
00:06:05,080 --> 00:06:12,120
It's only our attachment to some sort of self an ego that creates the suffering so

71
00:06:13,720 --> 00:06:19,720
You know that I think that there's your answer the reason why is this mainly mainly ego and

72
00:06:20,520 --> 00:06:22,520
The way out of it is to

73
00:06:23,720 --> 00:06:30,760
Let go of our sense of self if someone hurts you if someone attacks you examine that feeling examine your own ego and your attachment to

74
00:06:30,760 --> 00:06:35,960
Feelings of deserving this or deserving that because obviously there's no objective

75
00:06:36,920 --> 00:06:38,920
deserving

76
00:06:38,920 --> 00:06:47,360
In Buddhism you get what you deserve always which which simply means that what happens happened and it's up to you whether you're going to accept it or not

77
00:06:47,480 --> 00:06:53,400
Accepted and the more you're able to accept and and let go and let be

78
00:06:54,520 --> 00:06:57,160
The less tension there will be less stress

79
00:06:57,160 --> 00:07:03,320
There will be in your life and until finally people can attack you and it's something like it goes right through you

80
00:07:03,800 --> 00:07:05,800
Like like

81
00:07:05,800 --> 00:07:07,800
There's nothing there

82
00:07:09,080 --> 00:07:13,800
Because there's no ego there to react and to be stimulated by it

83
00:07:13,800 --> 00:07:31,320
Okay, so I hope that helps all the best

