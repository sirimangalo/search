Okay, good evening everyone. Welcome to our live broadcast November 24th
So this daily thing is working out well today is
Excuse me the shaving day in Thailand tomorrow's the full moon
I think
Which means I may be today it depends on which there's two traditions in Thailand one strict and one not so strict
One strict in one traditional, I think you could say because technically
Sometimes the moon is full after midnight sometimes it's full before midnight
but the traditional group goes more by
Calculations, then my exact hour
I don't know which one I'm saying tomorrow is but but many of the full months are still the same
Shaving today
I
Want some months
It's a big thing in Thailand some monks do shave more regularly and Sri Lanka they shave
Same every couple days
It's our kimon which one's better
If you shave like once a week then or once every few days you can shave everything shave your beard and
You head all at once and then it never gets so long and it's a bit of a drag because every month
Even once a month, and it's a lot of hair
Seems like less bust shave once a month
Probably get the inevitable question why do months shame I
Think it's cleaner
And it's less bust no ego attachment to the hair and hairstyle because monks were styling their hair
beginning sort of
And that was why they would have been stated to rule not to let it get longer
Longer we can get this long long to two fingers long
Or or two months if you haven't shaved into months you have to shave
And if it gets longer than the fingers then you have to
So that that also apparently goes for the beard or technically goes for the beard so that some monks were testing that there are monks who that their beard grow out
It's still two months. You have to shave your beard
That's why the beard down after two months
A lot of beard mostly we don't go by that if I go by that one in Thailand
If a real thing against facial hair in Thailand because most type people have very little facial hair
Still it's your rogue if you have facial hair big on appearances
Fortunately
So good evening
They I taught two people how to meditate or one person I guess this
Friend I'm talking about my my university friend got one of her friends
Come and learn how to meditate who also is struggling actually said suffers from many of the same issues
as she does
What was a very good minute
New person who's picked up like nothing some people most people
Most of us and I include myself and this aren't able to pick it up very quickly
But the other person gets it really quickly like
Her synchronicity, you know, it's a simple thing to can tell when they do the walking she's able to say step
Right
Yep, so she's in time with that's rare actually for someone who hasn't done it. It's actually a bit of a skill
But I was impressed by I just that simple
And then we did you're sat very still when I did my
Yeah, that was through the meditation after
You go one person at a time
I'm about this
Charity thing that we're looking
And some of the money got in touch with me and there's a kid. There's a children's home in Tampa that they've
involved
with
But Robin you sent me something about dogs actually
If there were something about cats or just pets in general because my stepmother fathers a real cat person
Yeah, I guess dogs have had more trouble, right bigger harder to take care of stuff for more
There may be but I can't take care of themselves like cats. Yeah, maybe I'll get
I'm not sure there and there may be something for cats. I did I just I didn't look that far
I was thinking about dogs today, and I just looked it up. I can check for cats too
I was really I was really thinking about helping humans. Okay. Yeah, I guess humans are important too
They have to look after the dogs after all yes
And the cats well
You have to praise the cats
I have to slave over the cats. They're doing their role in life
So what are you thinking to do for the children's home? Shall we do like an online campaign?
Or what were you thinking?
I don't know if I can say yes to that. I guess so. I guess I can say that's that sort of indeed I do because
But I just can't talk about money
Sure
Can you tell me the name of the organization so I can
So we can decide it on one, but I guess it's
Send the money. I guess we can just decide on this children's home
Send the money
Okay, she watches this did watch last
Children's home dot org
Children's home dot or they never visit us. They also have something over the holidays. It's a nice website
Oh, yes, it's 1892
We're in Tampa
We could just go for a tour. You know, I could give a gift and they could go for a tour
VIP tours, so what's a VIP tour you have to give a certain amount to become a VIP
We'll look like they're having a big event on Saturday December 12th, but you'll be there a little after that
Though we could give you know, if we
But that's when that's an open house
Make a donation from our list of most needed items, so they have like a
I think they have a wish list of some sort
There's a way to find out how to become a VIP like what's the criteria?
Then we'd have a goal, right? Yeah
How you can help
You're gonna make a gift and honor if someone you care about come part of a surrogate
The benefactor is $1,000. It's here for five years
I think it probably
They're most urgent needs, so we you know, we could ask people if they'd like to send these items
Although I don't think you probably want to have to carry them down there, so
We'd be better if we could send them to someone
in the area
But on their most urgent needs they're looking for
Women's and mentalties and personal care items like body wash and hair products and things
and batteries
Well, and they have a much longer list of ongoing needs just all sorts of personal care items and recreational items
So we could either
You know look for donations of items or do an online campaign or
Donate gift cards. We've got a lot of options here
Well
Come back to school supplies
I'm trying to think of something that could get my family involved
To be able to give us a gift to them
I'll get on there behalf
The best way to give on someone
If we had a bunch of supplies, then we could they could all together deliver it
Hmm, yeah, we would just need a place to send them to send them down there
So you wouldn't have to try to carry them all with you
There's urgent needs in November
You
Batteries
I
Actually serve children and adults
They suggest having a donation drive
Which kind of sounds like maybe what we're talking about maybe
I
When you're ready, Bantil there are a couple of questions
All right, back to the number. Okay. I know it was starting to feel like I'm on to your meeting there for a moment
At the moment someone dies. How long do they stay near their body? Are they still aware of their former selves?
How long before they are reborn?
I love no idea
It's not I'm not qualified to answer that question
Those questions
I have a good question to find someone who knows
Some of it's on the Buddha this past things down
But not really to that extent
You'll have stories that have been passed on
Spirits there was one spirit hanging out. I had a body and
And a monk came in the took the cloth off the body because they usually wrapped them up in a white cloth
Throw them in the rubbish in the carnal ground
We pulled the cloth off thinking it would make a nice robe cloth
And the spirit got really upset and went back into the body and stood up and chased
Chase them all the way back to his goodby
Where he closed the door and the body fell down it fell it dropped down
Against the door
He wanted his cloth back a real Buddhist zombie
Wanted the cloth back
So there was something the Buddha instated a rule as a result of that. I can't remember what the rule was
I think there was a rule and stated like
Making sure the body is really dead or something
It was a simple assistant. It wasn't about don't take don't take class from dead bodies
But then same to do
And it's not considered stealing because really your dead dude can move on and get over it
And there's something some sort of minor minor rule and then he instated to sort of make sure he didn't get chased by zombies
May be cover the body up with something else
Dear Bombay the more I give up doubts record the more I give up doubts
Regarding the practice the more I understand the practice the more I dedicate myself to the practice and the more I let go
The more they're develops an issue that could probably be seen as relating to what you describe as the second imperfection of insight
The imperfection of knowledge
What has been happening lately is a kind of circle or seesaw motion or something
I attain more clarity regarding financial
I start to see with greater and greater clarity the uselessness and worthlessness of aspects of myself
Of other people of several activities. I've been occupied with throughout my life
Of the goals. I've had in my life of the goals other people have etc
This makes me want to practice even more and dedicate myself even more and I do that
But soon afterwards when indulging in the many activities as before and dealing with the same people as before
Which tends to be bound to happen due to being a layperson living in the world
My experience with these activities and people is now different because of how the practice has affected the mind
Everything is now so much easier and smoother and sometimes it even feels like I could clearly accomplish anything
This tends to entice or alert very strongly to play the mundane game of life
To play this mundane game of life for at least a bit longer
As it feels like I'm now almost looking at these activities and interactions from the outside as some kind of super user
Of course this doesn't last for very long, but it makes things difficult
Even when it happens. I still always kind of remember the importance of the practice and the worthlessness of the things
I mentioned earlier
But at least this slows things down considerably. Can you talk about this?
Thank you
I think Robin just didn't talk about that. What's the question?
I'm sorry Robin that you had
That's not really fair is it?
I mean it's needed to hear about but
Yeah, I think we probably understand when he's talking about that that big change
That's the question
Yeah, you need to talk I mean Robin you're gonna but
I have pity on Robin. She has to talk. It has to say it
Oh, that's okay. I don't mind reading long questions. It's you know if it's helpful
But in the end was no question. What's the question? I mean good for you. It's great to hear about your practice, but
No, we don't it's not fair. You have to ask a question. It should be short and concise and simple. You see to understand
Is that person still around?
Yes
Well, then ask a question and make it simple. Keep it simple
Those are the rules
We don't need so much background really because you'll find that when you do come up with a question if there is one
And that you didn't really need to keep all the background details
You have to learn to be mindful you don't think so much
Sounds like there's maybe too much mental activity
Do you have a question ask it then things then
You don't be hit you with a stick
We'll have to I implement the Twitter
Limitation there. What's that 120 characters?
Yeah, it was originally at something like what 400 characters or something
200 and then people all know it's too short
Now how many how many posts is that person used just to ask to ask that not a question? It was just too
Oh Timo, this is the guy who's coming in December
So he's gonna put a little more information there and I believe
No, there's no no need for background
Just
Ask a question see and if you can't formulate a question then you got a problem. That's that's that's the reason it's a litmus test
That you still have it's still not clear in your mind
Which means you that's your problem. You have to start saying thinking thinking and
Looking at your mind and and maybe read my booklet again to figure out if you're actually meditating correctly
It's it's easy to get meditating on the wrong path
Maybe you have states and bliss or happiness look at the ten imperfections of insight
And see if you have any of the other ones you'll have be a calm
If you're powerful, well, that's a confidence at sunrise and the mocha. I think is the
Defound
So you have to say
Happy happy confident
Knowing
You know just on a calendar birthday that tomorrow is on a panasati day. Is that oh, I don't know
Is that a different tradition? I don't know
Which it's the full moon day
I guess it's what they say that when they say the Buddha
Taught in the Ana panasati sutta. So those people who are keen on Ana panasati
Have created a day
Okay
Okay, so kind of grumpy there. I didn't mean to send grumpy. It's a good thing. It's good to have that
It's a little bit grumpy. It's like wow really
It's not really a holiday like maga budge or
We sack a budge or a saddle hub, which are these are the three big ones
Is there a they're old and traditional and
Meaningful to have a day just for the sutta. Well, that's nice. It's not the wrong with it
Not on the level of a real Buddhist holiday
Is there anything special that they do in Thailand on that day
No, I mean, this is I don't know who which group this is I think it's tell you Sarah be good at
Popularized that. I don't know. Maybe it's his group. Maybe the dama you to Nikay
Okay, certainly not a big thing in Thailand as far as I know
Do you think it is important to keep attention on body in your daily life?
Well, not meditating rather than thoughts
One can understand on each adhukha anata through thoughts too
Is well not medit?
Question is while not meditating can one give importance to thoughts or should one combine oneself to the body?
Well, the body's easier when you're not meditating the body's easier to do mind
For thoughts can be a bit overwhelming, but certainly you should be mindful of all four
So deep at home during your life
We tend to recommend doing the body. It's easier
When you're standing walking sitting in mind. That's just easier to be mindful of
And that's for most people for some people the mind is easier
You know some people benefit more from
Focusing on one or another of the Sati Bhutan yourself. So as you know, no, there's no rules like that
Should this should that
Sati Bhutan are like and for weapons that you have in your guerilla
soldier
fighting a messy war
And so it's not it's not
It's not neat. It's messy. Sometimes like this. Sometimes like that. You have to change tactics depending on the enemy
and so on
Depending on the situation
You have to be clever like a boxer
To know when the duck and when to leave and when to punch and when to jab
When you have to know the long game you have to be patient
Okay
Expect to knock out in one punch
It's kind of like a war of a trishing
Bleed each other to bleed your enemies to death
Star of your enemies to death. That's really what it is. We're starving our defilements
I
Gonna note them to death
Hmm
What is this? Can it be that I've answered all the questions possible and no one has more question?
Our viewership is down right fewer people are watching. That's just fine
It's expected you know looks that time to
watch some
Funny monk for every day
Cut things they need to be doing
That's fine. This is just sort of a hello
Tomorrow we'll do another dumber panda, but again, that's not gonna be broadcast by it'll be audio live
And
Just tomorrow yet tomorrow is another dumber panda
That's enough. That's all good night. Thank you. There was one more question if you had time
Hi, Bante is an hour had to protect it from physical harm only during meditating or always
And they're not protected from physical harm even when meditating now
There's these stories about when you enter into jana or
or some apathy of some sort
You you're invincible
But that's nothing to do with being an arrow hunt first per se
It's to do with the state of attainment the state of meditation
Yeah, even non arrow hunts couldn't theoretically have that sort of
imperviousness. It's not based. It's not the
Exactly or directly related to their state of being an arrow hunt. It's the state of meditation that they go into
Didn't the Buddha hurt his foot or have some sort of an injury after he was enlightened. Yeah
Mogulana the most powerful meditative monk who had the most powerful attainment was beaten almost to death
Had all the bones in his body broke in the pyramid yet horrible karma. He tortured his parents
Beat in his parents because he didn't want to take care of him
Went to hell for that
This is a follow-up question from Timo. I guess I was looking for help regarding the seesaw motion between the practice and the lay life
I think questions seemed kind of conceited. Also, sorry Robin for the long questions. Don't be sorry
It wasn't even a question. Where's your question?
The question is
What sort of help can you give me for understanding?
We're dealing with the seesaw motion between the practice and the lay life
No, I don't know and I don't like general help questions either
As they're too complicated. It's like you want me to write about you know, it's not you're not alone many people ask these sorts of questions
It's like you want me to plan your life out
Yeah, general advice. It's too difficult. I don't know you. I don't know how to read the signal of others who tell give you some general advice
Digani kai at 31. I think
Signal of others and I'm really good at it, but there's a lot
Because it's not that simple. I can't just tell you something that's going to help you with that
You have to give specific examples like is this right is that right?
And I think maybe to some extent you already know the answer to some of the some of the questions that you might ask
So that you're avoiding them. You have to just look at the individual's
aspects of your life
Dealing with people feeling
Egotistical feeling ambitious feeling
That's we're not egotistical. I don't know you said that even ambitious was kind of like you were saying
And you know that that's ambition
How to deal with things is always just meditate. It's always just going to be learned to meditate. I don't have to see them clearly and then have to let go
That it details of living their life
Don't I mean the ultimate advice it just comes down to become a monk if you want real answers
I'm not an easy thing to do
Apologies probably not satisfied but
He's very grateful for the how to meditate books
So
Okay enough good night
Thank you Robin
Thank you, Dante good night. We're not another question. Do it
Are you not that you say no, okay, okay, good night Dante. Thank you
