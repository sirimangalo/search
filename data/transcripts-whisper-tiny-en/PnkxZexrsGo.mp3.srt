1
00:00:00,000 --> 00:00:08,000
I have the same question as Brenna. Is it necessary for me to sub-localize a description of the object I am aware of?

2
00:00:08,000 --> 00:00:12,000
I am able to perceive objects without necessarily naming them.

3
00:00:12,000 --> 00:00:18,000
Sometimes finding an English word to fit the thought is distracting.

4
00:00:18,000 --> 00:00:25,000
Again, this is only because you're untrained because it's new because it's something that you're probably not very good at,

5
00:00:25,000 --> 00:00:28,000
which is natural when you first start something.

6
00:00:28,000 --> 00:00:33,000
Eventually, you get better at recognizing things as they are.

7
00:00:33,000 --> 00:00:40,000
There's not that many experiences that after a good many hours of doing this.

8
00:00:40,000 --> 00:00:45,000
You get pretty good at finding names for just about everything.

9
00:00:45,000 --> 00:00:52,000
It's not like there's millions of different words that you could possibly have to use.

10
00:00:52,000 --> 00:00:57,000
But we don't call it sub-localizing.

11
00:00:57,000 --> 00:01:03,000
We call it reminding yourself, which is indeed how we understand the word set-p,

12
00:01:03,000 --> 00:01:12,000
which doesn't really mean to remember in the sense of re-reifying the object.

13
00:01:12,000 --> 00:01:16,000
Instead of going the next step of liking or disliking it, you say to yourself,

14
00:01:16,000 --> 00:01:22,000
you're saying, instead of saying, this is good, this is bad, you say this is this, that's the mindfulness.

15
00:01:22,000 --> 00:01:24,000
It's actually reminding yourself.

16
00:01:24,000 --> 00:01:28,000
Mindfulness of the Buddha is to remind yourself of the Buddha, to remember the Buddha.

17
00:01:28,000 --> 00:01:30,000
But you do that by saying to yourself,

18
00:01:30,000 --> 00:01:33,000
Buddha, Buddha, Bhagava, Bhagava, Arahang, Arahang.

19
00:01:33,000 --> 00:01:37,000
It's all the qualities of the Buddha that remind you of the Buddha,

20
00:01:37,000 --> 00:01:44,000
so keep you focused on the idea of the Buddha with mindfulness of the body into the same.

21
00:01:44,000 --> 00:01:48,000
But you remind yourself of the body into it.

22
00:01:48,000 --> 00:01:52,000
Otherwise, we don't consider it in this tradition to be meditation.

23
00:01:52,000 --> 00:01:57,000
Consider it just sitting there zoning out, which isn't kamatana,

24
00:01:57,000 --> 00:02:00,000
it isn't training, it isn't insight meditation.

25
00:02:00,000 --> 00:02:23,000
Not.

