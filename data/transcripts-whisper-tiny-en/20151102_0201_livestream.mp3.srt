1
00:00:00,000 --> 00:00:24,160
Good evening, everyone broadcasting live November 1st, 2015, and our non-dumb-apada nights

2
00:00:24,160 --> 00:00:33,320
have now taken on a bit of a, I think the word is a power, they're less colorful,

3
00:00:33,320 --> 00:00:38,160
less lively, not sure what we're going to do now that I've decided not to do dumb-apada

4
00:00:38,160 --> 00:00:46,960
every night. Maybe we won't do anything. Maybe I'll say hello. Thank you all for practicing.

5
00:00:46,960 --> 00:00:54,400
I guess I can take the time now to talk about our online courses. It's not like there's

6
00:00:54,400 --> 00:01:03,520
any good in advertising them, unless I start to get a little bit more vigilant, we'll see.

7
00:01:03,520 --> 00:01:13,920
Actually, it's not really about that. There's 28 slots, and 24 of them have been claimed so far

8
00:01:13,920 --> 00:01:22,400
right now, and that goes up and down, but 24 is about as high as we've gotten, and pretty much

9
00:01:22,400 --> 00:01:32,960
everyone comes out as expected, and is practicing diligently at least an hour or two a day.

10
00:01:34,800 --> 00:01:41,200
So that's really been, it's become sort of the highlight for me, you know, it's a real focus,

11
00:01:41,200 --> 00:01:50,640
because the biggest problem that I've had in sort of sharing this meditation technique is that

12
00:01:50,640 --> 00:01:57,520
I've always felt unable to share more than the first step, so everyone's been thinking,

13
00:01:57,520 --> 00:02:03,120
well, this is it, you know. This is the meditation that I'm supposed to do for the rest of my life,

14
00:02:03,120 --> 00:02:08,160
if I follow this tradition, it's just stepping right, stepping left, and rising, falling,

15
00:02:08,160 --> 00:02:13,200
it's a very simple technique, but there's many of the people who have started taking the course

16
00:02:13,200 --> 00:02:18,640
have found, it gets quite complex actually, they're quite involved, and there's quite a bit too,

17
00:02:18,640 --> 00:02:26,080
not just the adding of extra elements to the practice, but there's also the interview process,

18
00:02:26,080 --> 00:02:32,640
and there's even more, there's the next step which will be having people come to do

19
00:02:32,640 --> 00:02:39,760
intensive courses, which of course is the best, but also the most difficult to arrange for us

20
00:02:39,760 --> 00:02:45,520
to arrange and for the individuals to arrange to actually get out here, but at least what we're

21
00:02:45,520 --> 00:02:53,600
doing is, well, besides giving an introduction to the practice, we're also reducing the amount

22
00:02:53,600 --> 00:03:00,080
of time that you would have to spend practicing intensively to finish the course, so my idea was

23
00:03:00,080 --> 00:03:08,800
that if you do this for a while, we could finish the foundation course intensively in about a week,

24
00:03:11,680 --> 00:03:20,640
and to get through, to get that far would probably take maybe 12 to 14 weeks, maybe more,

25
00:03:23,200 --> 00:03:29,920
maybe more, so you'd finish most of the, you'd get all the technique down, and then you'd

26
00:03:29,920 --> 00:03:39,600
come here and put it into practice intensively for about a week, and you could pretty easily finish

27
00:03:39,600 --> 00:03:46,720
the course. So there's still four slots available, if someone wants to take Tuesday morning, or

28
00:03:48,000 --> 00:03:51,360
Thursday afternoon, or there's one on Saturday morning that's now available.

29
00:03:51,360 --> 00:03:57,600
Apart from that, we have 24 people doing it, which is great.

30
00:04:02,720 --> 00:04:08,160
I don't know. I just asked them every time. What did I give you last time? I don't keep track

31
00:04:08,160 --> 00:04:26,800
of anything. They could be lying. In this tradition, I think it's new, but our tradition isn't

32
00:04:26,800 --> 00:04:36,480
all that huge. I don't know another tradition. It's not something that I think is very well

33
00:04:36,480 --> 00:04:42,160
set up, very commonly set up, where you'd have this kind of online system. I do know that people

34
00:04:42,160 --> 00:04:46,720
have done right, so if you're just talking about the, having online courses, I do know people

35
00:04:46,720 --> 00:04:53,040
even in this tradition, another monk under my teacher was probably the one who gave me this idea,

36
00:04:53,040 --> 00:04:57,040
because he does this, or he's done this for a long time, where people would meet,

37
00:04:57,040 --> 00:04:59,840
we would talk through him once a week, and he'd lead them through the course,

38
00:04:59,840 --> 00:05:11,760
from Israel, as far as I know, he still does this. So he could, he'd benefit from this kind of

39
00:05:11,760 --> 00:05:16,800
set up. I could set him up a website, but there's a little bit of tension, you know, because the

40
00:05:16,800 --> 00:05:22,560
whole international group of people that people I started with, they're not comfortable working

41
00:05:22,560 --> 00:05:36,880
with me, because they had a falling out with their head guy many years ago.

42
00:05:36,880 --> 00:05:56,240
Are you, are you able to get on the site yet? A strange, maybe there's a, maybe there's

43
00:05:56,240 --> 00:06:07,520
a cable cut between Connecticut and Ontario. Well, we're going through Google, that one,

44
00:06:07,520 --> 00:06:13,120
oh no, this is in Pennsylvania, this server is in Pennsylvania. It's just a question of where

45
00:06:13,120 --> 00:06:29,600
the server is, there might be a problem or not. There it is, oh,

46
00:06:29,600 --> 00:06:42,560
there's a problem we have here is no one hears. If you're listening to the audio, you don't hear

47
00:06:42,560 --> 00:06:58,720
Robin, so she's talking to me. So there's a group practicing the karma pal meditation,

48
00:06:59,520 --> 00:07:07,760
they're not too enthusiastic about it, so worth going. I don't know anything about karma

49
00:07:07,760 --> 00:07:12,640
pal meditation, I mean, I can't actually, I don't think this was actually directed at me,

50
00:07:12,640 --> 00:07:17,040
it's kind of what do you guys think. So I'm just one of the guys, but

51
00:07:23,840 --> 00:07:31,600
well, I don't have an informed opinion about karma, I guess I can, I don't really have that much

52
00:07:31,600 --> 00:07:37,360
of informed opinion on going to other meditation groups. I know that when I have done it,

53
00:07:37,360 --> 00:07:41,200
I would just do my own meditation because it's not like they have thought police that are

54
00:07:42,400 --> 00:07:47,360
making sure you do their meditation, but it can also be a little bit awkward. I remember when I

55
00:07:47,360 --> 00:07:52,080
was first starting in this technique, I would only do this and I went to a dharma group

56
00:07:52,880 --> 00:07:57,760
and they did walking meditation in a circle and I was kind of rude about it. I just went off

57
00:07:57,760 --> 00:08:02,560
into, well, it wasn't rude, I went off into a corner and did my own walking meditation back and forth,

58
00:08:02,560 --> 00:08:09,600
but I think if that were to happen today, I certainly wouldn't go off and do my own walking

59
00:08:09,600 --> 00:08:14,800
meditation, I would walk with them in a circle because they walked together, they walked around

60
00:08:14,800 --> 00:08:26,480
the room in a line, you know. So I wouldn't get as stubborn as I was, but for the most part,

61
00:08:26,480 --> 00:08:34,560
you can do your own meditation, do a little bit of their meditation if you like. It's nice to

62
00:08:34,560 --> 00:08:39,920
go in a group to be in a group, but I think in the end, you might find it more trouble than it's worth

63
00:08:39,920 --> 00:09:01,200
unless you like their meditation technique. Not no one, most people don't though.

64
00:09:01,200 --> 00:09:11,200
You never know, you might find someone who can read your mind.

65
00:09:31,200 --> 00:09:45,760
Trying to know that I got caught in the train of thought and then I automatically go back to

66
00:09:45,760 --> 00:09:54,800
noting my walking, or whatever, I then recognized I was thinking, I see. Yeah, I mean, you shouldn't

67
00:09:54,800 --> 00:10:02,400
automatically go back to noting walking. You should try to catch the thought and say thinking,

68
00:10:02,400 --> 00:10:14,320
you should stop walking and say thinking, thinking, or you can just ignore it. And again, if you

69
00:10:14,320 --> 00:10:20,160
recognize it, I mean walking, you can, to some extent, just leave it alone. With all, keep it

70
00:10:20,160 --> 00:10:26,160
simpler than then worrying about this, just in general, if any one thing is taking you away

71
00:10:26,160 --> 00:10:31,680
significantly from the walking, then that's when you'd want to really definitely stop and start

72
00:10:31,680 --> 00:10:38,640
acknowledging it. But to some extent, and it's a judgment call on your part, you can do it either way,

73
00:10:38,640 --> 00:10:43,200
you don't have to stop to note every single thing, but normally we would stop and put our feet

74
00:10:43,200 --> 00:10:53,200
together and then say, thinking, thinking, or distracted, distracted.

75
00:11:13,440 --> 00:11:26,240
Slow night. All right. Well, originally I just intended this to be a sort of come online and say,

76
00:11:26,240 --> 00:11:32,080
hello, kind of thing. So we don't have to sit around here for a long periods of time.

77
00:11:33,520 --> 00:11:35,200
It's just a way to connect with people.

78
00:11:35,200 --> 00:11:46,160
Oh, here we have somebody. Please put a question tag at the beginning of your posts like Patrick

79
00:11:46,160 --> 00:12:09,120
O'Just did. Start your posts with a Q colon space. I don't know what awareness, release, and discernment

80
00:12:09,120 --> 00:12:17,280
release are. I think you're talking about Jetowi Muti and Banyawi Muti. Jetowi Muti is,

81
00:12:18,960 --> 00:12:25,120
it really depends exactly who you're talking to. I mean, Jetowi Muti involves

82
00:12:25,120 --> 00:12:30,480
samata meditation. Banyawi Muti involves sweet pass into meditation. Now, you might say that

83
00:12:30,480 --> 00:12:39,200
Jetowi Muti is only samata meditation, so it's entering into the mundane genres. And Banyawi Muti is a

84
00:12:39,200 --> 00:12:48,800
attainment of nibana, or you can say, Banyawi Muti is the attainment of nibana without the

85
00:12:48,800 --> 00:12:59,200
janas, without the mundane janas. And Jetowi Muti is attainment of nibana with also having attained the

86
00:12:59,200 --> 00:13:06,000
samata janas. So there was a group of monks in the time of the Buddha who, another monk

87
00:13:07,200 --> 00:13:11,440
became our hands, and the monk came up to them and asked them, so do you have all these magical

88
00:13:11,440 --> 00:13:17,200
powers and have you attained the formless janas and so on? And they said, no, and he said,

89
00:13:17,200 --> 00:13:23,920
they said, well, how could that be? And they said, well, we're Banyawi Muti. We Muti is is freedom,

90
00:13:23,920 --> 00:13:28,240
and Banyawi, I guess, is discernment. It's wisdom is more common translation.

91
00:13:30,800 --> 00:13:35,680
But they're just terms, and you have to see how they're used. So Banyawi Muti said to

92
00:13:35,680 --> 00:13:41,120
is thought, I think, by the commentaries to mean that they didn't practice samata meditation,

93
00:13:41,120 --> 00:13:47,360
they only practiced shipasana. And it's curious, it's curious that in that sutta,

94
00:13:47,360 --> 00:13:51,520
he doesn't ask, and they don't deny having attained the first four janas.

95
00:13:52,640 --> 00:13:57,120
And we argued about this, some years back, there was a debate about what this meant

96
00:13:57,120 --> 00:14:02,480
on one of the internet forums back when I was involved with them.

97
00:14:04,960 --> 00:14:09,120
And someone was saying, trying to explain that away and say, well, just because they didn't,

98
00:14:09,120 --> 00:14:13,520
doesn't mean they actually attain them. And I said, well, honestly, they have to obtain the first

99
00:14:13,520 --> 00:14:18,640
four janas because they attain them when they realize Nibana. So that's why, in my mind,

100
00:14:18,640 --> 00:14:22,800
that's the reason why they couldn't ask about that, because absolutely, if you become an

101
00:14:22,800 --> 00:14:28,320
arrowhead, you have to obtain janas. It's just a super mundane janas. But you don't,

102
00:14:28,320 --> 00:14:32,960
you haven't attained, or you don't have to attain the Arupa janas or magical powers because those

103
00:14:32,960 --> 00:14:39,360
are specific to samata janas. So it's a bit of a technical question.

104
00:14:49,680 --> 00:14:56,000
Monks can keep juice until dawn. As soon as dawn arises, the time when you would then be able

105
00:14:56,000 --> 00:15:04,320
to eat solid food, the, except for the opinion is that the juice has to be thrown out or used

106
00:15:04,320 --> 00:15:07,920
for other purposes, whatever those might be.

107
00:15:14,560 --> 00:15:20,640
Well, there's some debate, there's some talk about how if you accept it for food,

108
00:15:20,640 --> 00:15:25,120
then you have to eat it, but I don't think that applies to juice. So if your intention is to

109
00:15:25,120 --> 00:15:28,640
have it in the morning, when you receive it, and then you decide, oh, instead, I'll have it in

110
00:15:28,640 --> 00:15:33,040
the afternoon, there's something about that where it's a problem, but I'm not entirely sure how

111
00:15:33,040 --> 00:15:42,160
that, that applies. I don't really pay much attention to that. It's, but there is something like,

112
00:15:42,160 --> 00:15:50,800
if you keep, if you keep salt as a medicine, you can't then the next day sprinkle it on your food,

113
00:15:50,800 --> 00:15:56,400
right? But that doesn't have anything to do with your intention. That's just, you've kept it more

114
00:15:56,400 --> 00:16:01,760
than a day, so using it as food isn't allowed, but using salt as a medicine, you can keep salt

115
00:16:01,760 --> 00:16:09,440
for your whole life. So yeah, as long as it wouldn't work anyway, but yeah, juice is something that,

116
00:16:09,440 --> 00:16:24,000
even if you get it in the morning, you can keep it until the next morning. Yeah, yeah, juice is great.

117
00:16:26,480 --> 00:16:27,600
It's the answer to this.

118
00:16:27,600 --> 00:16:41,040
I mean, it's the happy medium because a lot of Buddhists who have, who keep the rule, not to

119
00:16:41,040 --> 00:16:46,240
eat in the evening, actually don't keep the rule because they have a lot of soil milk and milk,

120
00:16:47,440 --> 00:16:53,440
and even more, even heavier things, like they were trying to argue to have this soft tofu

121
00:16:53,440 --> 00:17:02,720
drinks, like it's a ginger, sweet ginger drink with soft soft tofu in it. No, it's chunks of

122
00:17:02,720 --> 00:17:10,240
very soft tofu, the chunks of them in the ginger drink, it's a Thai thing. It's too sweet, but

123
00:17:10,240 --> 00:17:15,760
otherwise it's pretty good. I mean, having ginger and tofu was great, but probably a bit too sweet,

124
00:17:15,760 --> 00:17:30,960
to be healthy.

125
00:17:39,680 --> 00:17:43,520
Yeah, I mean, if someone comes in the morning and offers a juice drink as well, then they can

126
00:17:43,520 --> 00:17:52,480
keep that for the afternoon. But I can also, because people have already given me,

127
00:17:54,080 --> 00:18:01,680
or they've given me the meals in advance through the gift cards, I can go to Tim Hortons

128
00:18:01,680 --> 00:18:08,560
potentially and get a juice there. Yeah, they have orange and apple juice.

129
00:18:08,560 --> 00:18:20,080
I think it's it's overpriced, but I'm sure it's overpriced, but

130
00:18:39,520 --> 00:18:57,760
Okay, well, thank you for working on the subtitles.

131
00:19:00,720 --> 00:19:05,280
Okay, so you can't, I guess you can't, that means you can't upload them yourself, you've looked

132
00:19:05,280 --> 00:19:15,280
at it. Let me see.

133
00:19:15,280 --> 00:19:37,680
Yeah, there's a YouTube subtitles add on with that's probably a bit, it cannot add a subtitle,

134
00:19:37,680 --> 00:19:50,080
to track to someone else's video. You just have to share it.

135
00:19:50,080 --> 00:19:52,560
So I have to be responsible to put this up,

136
00:19:52,560 --> 00:20:14,560
I don't know.

137
00:20:23,280 --> 00:20:31,680
Well, monastery fridges are, I mean, technically it has to be a little bit more organized than that.

138
00:20:31,680 --> 00:20:38,560
There has to be a room in the monastery. I mean, maybe even a building, I don't know how,

139
00:20:38,560 --> 00:20:45,200
if you want to get really technical, but at least a room that is designated as a, I think it's

140
00:20:45,200 --> 00:20:52,400
called a kapiagara or something, a room for the kapia, something like that. I don't know,

141
00:20:52,400 --> 00:20:59,280
I can't remember the exact poly term, but it's a place where the lay people, and it belongs to

142
00:20:59,280 --> 00:21:05,280
the lay people. So the stuff in there is not for the monks. So like, if I had juice in my room,

143
00:21:05,280 --> 00:21:10,720
if someone kept juice in my room, then I couldn't drink it even if they offered it to me.

144
00:21:10,720 --> 00:21:16,480
Like if there was a refrigerator in my room, but we consider the kitchen to be off limits to me.

145
00:21:16,480 --> 00:21:22,560
All the stuff in the kitchen is not mine. All the food and all the, any of the drinks and any of

146
00:21:22,560 --> 00:21:28,480
the stuff. Except the ice cubes. I had to make ice cubes a couple nights ago, because no one had,

147
00:21:28,480 --> 00:21:33,360
no one made ice cubes. It's an important first day to item, and we didn't have any.

148
00:21:33,360 --> 00:21:43,200
Are you listening? I don't know. I did. I injured myself. Yeah, I was putting up a screen,

149
00:21:43,200 --> 00:21:49,840
a couple of the guys came from the Buddhism Association, came by on Sunday, wait, Saturday,

150
00:21:49,840 --> 00:21:56,320
yesterday, Friday, maybe Friday, and they wanted to watch a Buddhist movie or a Buddhist

151
00:21:56,320 --> 00:22:01,120
theme movie. I think they ended up watching just in order. I don't know what they ended up watching.

152
00:22:01,120 --> 00:22:07,520
I came down, and it was in the middle of actually, I don't know what to say. It was in the middle

153
00:22:07,520 --> 00:22:14,480
of something that probably wasn't appropriate for a Buddhist monastery. But anyway, I think

154
00:22:14,480 --> 00:22:18,880
probably we're going to have to talk about if we want to do that again. But definitely if we have

155
00:22:18,880 --> 00:22:23,120
Buddhist movies, I'm happy that they come. I think they just didn't have a sense of what was a

156
00:22:23,120 --> 00:22:27,920
good Buddhist movie, don't worth watching. But they asked if they could come over, do some

157
00:22:27,920 --> 00:22:33,520
meditation and watch a Buddhist movie. That's great. So I put up the screen for them in the

158
00:22:33,520 --> 00:22:38,960
basement. But as I was putting up the screen, I pinched my finger. Pretty bad, actually. I'm surprised

159
00:22:38,960 --> 00:22:49,280
it didn't get all blood blister. But I immediately went and shoved it on. It's placed it against

160
00:22:49,280 --> 00:22:53,040
something really, really cold in the freezer, probably the thing that creates the ice.

161
00:22:53,040 --> 00:23:00,480
And just left it there for a bit. Well, I looked for, well, I looked for ice cubes and couldn't

162
00:23:00,480 --> 00:23:12,160
find any. Yeah, no, and I made ice cubes. There's ice cubes there. I mean, I filled up the

163
00:23:12,160 --> 00:23:28,080
ice cube trays. They just weren't full.

164
00:23:42,160 --> 00:23:49,920
It doesn't matter whether the layperson intends to offer it, it matters where it's kept.

165
00:23:52,640 --> 00:23:58,880
It has to be kept outside of the monastic. If food or anything is kept in the monastic quarters,

166
00:23:58,880 --> 00:24:13,040
that's not allowed. The monke's can't. It's called anti something kept inside. Yeah, I mean,

167
00:24:13,040 --> 00:24:20,800
we haven't officially designated the kitchen as being the special room, but that's sort of just

168
00:24:20,800 --> 00:24:27,360
a given. In fact, I guess I could say the only part of this, that this really, the monastic

169
00:24:27,360 --> 00:24:34,880
part is the upstairs. So I would suggest that that is off the mitts, keeping stuff upstairs is

170
00:24:34,880 --> 00:24:45,920
not allowed or it's allowed, but I couldn't partake of it.

171
00:24:45,920 --> 00:25:03,360
Yeah, I mean, the meditators as long as the food is not kept in my, not kept in my dwelling,

172
00:25:03,360 --> 00:25:19,280
not kept by me and not cooked by me. Those are the three aspects are not allowed.

173
00:25:19,280 --> 00:25:38,560
Okay, enough. No. I think we just sit here and people will start to try and think up questions

174
00:25:38,560 --> 00:25:44,880
to throw at me. If people had questions, they would have asked them, I know. If you had really,

175
00:25:44,880 --> 00:25:51,600
really had questions.

176
00:25:59,120 --> 00:25:59,440
Okay.

177
00:26:03,680 --> 00:26:07,520
Tomorrow we'll have Kundalakacy, the story of Kundalakacy, which is a

178
00:26:07,520 --> 00:26:13,440
Dhammapad, the next Dhammapad verse. See, when we go without it for a couple of days, then

179
00:26:13,440 --> 00:26:20,480
everybody's looking forward to it. So it's more special. Okay, thank you everyone. Thanks for

180
00:26:20,480 --> 00:26:50,320
having me for helping out. Have a good night.

