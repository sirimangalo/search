1
00:00:00,000 --> 00:00:06,600
How to meditate by you to Domobiku, read by Adirokes.

2
00:00:06,600 --> 00:00:10,400
Chapter 5 Mindful Prostration

3
00:00:10,400 --> 00:00:16,840
In this chapter, I will explain a third technique of meditation used as a preparatory exercise

4
00:00:16,840 --> 00:00:20,160
before walking and sitting meditation.

5
00:00:20,160 --> 00:00:23,960
This technique is called mindful prostration.

6
00:00:23,960 --> 00:00:29,240
It is an optional practice and may be omitted if desired.

7
00:00:29,240 --> 00:00:35,360
Meditation is a practice common to followers of various religious traditions around the world.

8
00:00:35,360 --> 00:00:40,760
In Buddhist countries, for example, prostration is used as a means of paying respect to

9
00:00:40,760 --> 00:00:46,200
one's parents, teachers, or figures of religious reverence.

10
00:00:46,200 --> 00:00:51,480
In other religious traditions, prostration may be used as a form of reverence towards an

11
00:00:51,480 --> 00:00:59,080
object of worship, a God, an angel, or some saintly figure, for example.

12
00:00:59,080 --> 00:01:04,880
Here the prostration is a means of paying respect to the meditation practice itself.

13
00:01:04,880 --> 00:01:12,800
It can be thought of as a means of creating humble and sincere appreciation for the practice.

14
00:01:12,800 --> 00:01:18,600
Reminding one that meditation is not just a hobby or pastime, but rather an important

15
00:01:18,600 --> 00:01:22,000
training worthy of respect.

16
00:01:22,000 --> 00:01:29,000
More importantly, though mindful prostration is a useful preparatory exercise, since it involves

17
00:01:29,000 --> 00:01:35,760
repetitive movements of various parts of the body, forcing one to focus on the activity

18
00:01:35,760 --> 00:01:37,800
at each moment.

19
00:01:37,800 --> 00:01:42,360
The technique of mindful prostration is performed as follows.

20
00:01:42,360 --> 00:01:48,040
You can find these illustrations in the appendix of the online or print version.

21
00:01:48,040 --> 00:01:54,040
Begin by sitting on the knees, either on the toes or on the tops of the feet.

22
00:01:54,040 --> 00:02:00,520
Place the hands palm down on the thighs, with back straight, and eyes open.

23
00:02:00,520 --> 00:02:06,080
Begin by turning the right hand 90 degrees on the thigh until it is perpendicular to the

24
00:02:06,080 --> 00:02:11,840
floor, keeping the mind focused on the movement of the hand.

25
00:02:11,840 --> 00:02:16,920
As the hand begins to turn, note turning.

26
00:02:16,920 --> 00:02:22,440
When the hand is halfway through the turning motion, again note turning.

27
00:02:22,440 --> 00:02:29,040
And when the hand completes the movement, note a third time turning.

28
00:02:29,040 --> 00:02:34,920
The word is repeated three times in order to create awareness of the motion throughout

29
00:02:34,920 --> 00:02:41,720
all three stages of motion, beginning, middle, and end.

30
00:02:41,720 --> 00:02:48,400
Next raise the right hand to the chest, stopping right before the thumb touches the chest,

31
00:02:48,400 --> 00:02:53,400
including raising, raising, raising.

32
00:02:53,400 --> 00:03:02,880
Then touch the edge of the thumb to the chest, noting, touching, touching, while the thumb

33
00:03:02,880 --> 00:03:05,680
touches the chest.

34
00:03:05,680 --> 00:03:17,040
Then repeat this sequence with the left hand, turning, turning, raising, raising, touching,

35
00:03:17,040 --> 00:03:19,920
touching.

36
00:03:19,920 --> 00:03:28,040
The left hand should touch not only the chest, but also the right hand, palm to palm.

37
00:03:28,040 --> 00:03:36,560
Next bring both hands up to the forehead, noting, raising, raising, raising.

38
00:03:36,560 --> 00:03:44,000
Then touching, touching, touching, when the thumb nails touch the forehead.

39
00:03:44,000 --> 00:03:53,480
Then bring the hands back down to the chest, noting, lowering, lowering, lowering, touching,

40
00:03:53,480 --> 00:03:56,360
touching, touching.

41
00:03:56,360 --> 00:03:59,480
Next comes the actual frustration.

42
00:03:59,480 --> 00:04:09,160
First bend the back down to a 45 degree angle, noting, bending, bending, bending.

43
00:04:09,160 --> 00:04:17,160
Then lower the right hand to the floor in front of the knees, saying, lowering, lowering,

44
00:04:17,160 --> 00:04:22,920
lowering, touching, touching, touching.

45
00:04:22,920 --> 00:04:28,480
Still keeping it perpendicular to the floor, this time with the edge of the little finger

46
00:04:28,480 --> 00:04:30,640
touching the floor.

47
00:04:30,640 --> 00:04:39,000
Finally, turn the hand, palm down to cover the floor, noting, covering, covering, covering,

48
00:04:39,000 --> 00:04:40,000
covering.

49
00:04:40,000 --> 00:04:50,400
Then repeat this sequence with the left hand, lowering, lowering, lowering, touching, touching,

50
00:04:50,400 --> 00:04:56,280
touching, covering, covering, covering.

51
00:04:56,280 --> 00:05:02,560
The hands should now be side by side, with the thumbs touching, and approximately 4 inches

52
00:05:02,560 --> 00:05:06,280
between index fingers.

53
00:05:06,280 --> 00:05:14,000
Next lower the head to touch the thumbs, saying, bending, bending, bending.

54
00:05:14,000 --> 00:05:21,040
As you bend the back and touching, touching, touching, when the forehead actually touches

55
00:05:21,040 --> 00:05:23,040
the thumbs.

56
00:05:23,040 --> 00:05:32,560
Then raise the head back again until the arms are straight, saying, raising, raising, raising.

57
00:05:32,560 --> 00:05:36,640
This is the first frustration.

58
00:05:36,640 --> 00:05:42,040
Once the arms are straight, start from the beginning to repeat the entire sequence a second

59
00:05:42,040 --> 00:05:50,600
time, except starting with the hands on the floor, noting, turning, turning, turning.

60
00:05:50,600 --> 00:06:00,280
As you turn the right hand, then raising, raising, raising, touching, touching, touching.

61
00:06:00,280 --> 00:06:13,480
When the left hand, turning, turning, raising, raising, raising, touching, touching, touching.

62
00:06:13,480 --> 00:06:19,440
As you raise the left hand this time, you should also raise the back from a 45 degree angle

63
00:06:19,440 --> 00:06:23,760
to a straight upright position.

64
00:06:23,760 --> 00:06:29,000
It is not necessary to acknowledge this movement separately, simply straighten the back

65
00:06:29,000 --> 00:06:33,320
as the left hand comes up to the chest.

66
00:06:33,320 --> 00:06:42,000
Then raise both hands up to the forehead again, noting, raising, raising, raising, touching,

67
00:06:42,000 --> 00:06:51,440
touching, touching, and down to the chest again, lowering, lowering, lowering, touching, touching,

68
00:06:51,440 --> 00:06:53,240
touching.

69
00:06:53,240 --> 00:06:59,240
Then bend the back again, bending, bending, bending.

70
00:06:59,240 --> 00:07:09,040
Finally lower the hands again one by one, lowering, lowering, lowering, touching, touching,

71
00:07:09,040 --> 00:07:27,140
covering, covering, covering, lowering, lowering, lowering, touching, touching, touching, covering,

72
00:07:27,140 --> 00:07:36,620
Push the thumbs with the forehead, bending, bending, bending, touching, touching, touching,

73
00:07:36,620 --> 00:07:42,420
and back up again, raising, raising, raising.

74
00:07:42,420 --> 00:07:48,900
This is the second frustration, after which a third frustration should be performed in the exact

75
00:07:48,900 --> 00:07:54,620
same manner, repeating the above one more time.

76
00:07:54,620 --> 00:08:00,780
Here the third frustration come up from the floor starting with the right hand as before,

77
00:08:00,780 --> 00:08:11,460
turning, turning, turning, raising, raising, touching, touching, touching, and the left

78
00:08:11,460 --> 00:08:24,500
hand, turning, turning, turning, raising, raising, touching, touching, touching,

79
00:08:24,500 --> 00:08:35,940
then bring the hands up to the forehead as before, raising, raising, touching, touching, touching,

80
00:08:35,940 --> 00:08:45,980
and back down to the chest, lowering, lowering, lowering, touching, touching, touching, touching.

81
00:08:45,980 --> 00:08:52,340
This time, however, instead of bending to do a fourth frustration, bring the hands down

82
00:08:52,340 --> 00:08:59,740
one at a time to rest on the thighs, returning them to their original position, starting

83
00:08:59,740 --> 00:09:08,420
with the right hand, note, lowering, lowering, lowering, touching, touching, touching,

84
00:09:08,420 --> 00:09:24,220
covering, covering, covering, and the left hand, lowering, lowering, lowering, touching, touching,

85
00:09:24,220 --> 00:09:26,260
covering, covering.

86
00:09:26,260 --> 00:09:31,420
Once one has completed the frustrations, one should continue on with the walking and

87
00:09:31,420 --> 00:09:34,900
sitting meditations in that order.

88
00:09:34,900 --> 00:09:41,380
It is important that as one changes position, one should maintain mindfulness, not standing

89
00:09:41,380 --> 00:09:46,340
up or sitting down hastily or mindfully.

90
00:09:46,340 --> 00:09:56,340
Before beginning to stand up, one should note, sitting, sitting, and then, standing, standing,

91
00:09:56,340 --> 00:10:00,460
as one lifts the body to a standing position.

92
00:10:00,460 --> 00:10:06,540
Without standing, continue immediately with the walking meditation, so that clear awareness

93
00:10:06,540 --> 00:10:10,100
of the present moment remains unbroken.

94
00:10:10,100 --> 00:10:16,860
In this way, the mindful frustration will act as a support for the walking meditation,

95
00:10:16,860 --> 00:10:23,300
just as the walking meditation will act as a support for the sitting meditation.

96
00:10:23,300 --> 00:10:29,140
During an intensive meditation course, students are instructed to practice all three techniques

97
00:10:29,140 --> 00:10:31,540
in this manner.

98
00:10:31,540 --> 00:10:37,340
Upon completion, they are instructed to rest for a short time and then start again from

99
00:10:37,340 --> 00:10:44,060
the beginning, practicing round after round for the duration of the lesson, normally

100
00:10:44,060 --> 00:10:47,260
one 24-hour period.

101
00:10:47,260 --> 00:10:52,340
Once this period is over, one would meet with the teacher to report and receive the

102
00:10:52,340 --> 00:10:59,100
next lesson, including more intricate walking and sitting techniques.

103
00:10:59,100 --> 00:11:04,060
Once this book is aimed towards giving the basics of meditation, advanced lessons will

104
00:11:04,060 --> 00:11:06,460
not be discussed here.

105
00:11:06,460 --> 00:11:11,260
Once one has mastered these basic techniques, one should seek guidance from a qualified

106
00:11:11,260 --> 00:11:15,980
instructor, if one wishes to pursue the practice further.

107
00:11:15,980 --> 00:11:21,740
If one is unable to enter a meditation course, one may begin by practicing these techniques

108
00:11:21,740 --> 00:11:28,540
once or twice a day, and contacting a teacher on a weekly or monthly basis, to obtain

109
00:11:28,540 --> 00:11:34,860
new lessons at a more gradual pace, according to a regimen agreed upon between teacher

110
00:11:34,860 --> 00:11:37,460
and student.

111
00:11:37,460 --> 00:11:41,980
This concludes the explanation of formal meditation practice.

112
00:11:41,980 --> 00:11:47,580
In the next and final chapter, I will discuss how to incorporate some of the concepts learned

113
00:11:47,580 --> 00:11:51,340
in this book into one's daily life.

114
00:11:51,340 --> 00:11:56,980
Thank you again for your interest, and again, I wish you peace, happiness, and freedom

115
00:11:56,980 --> 00:11:57,980
from suffering.

