1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamupanda.

2
00:00:04,000 --> 00:00:12,000
Today we continue on with verses number 102 and 103, which read as follows.

3
00:00:34,000 --> 00:00:48,000
The first one is actually similar.

4
00:00:48,000 --> 00:00:55,000
Almost the same as the ones we've looked at in the past couple of sessions.

5
00:00:55,000 --> 00:01:14,000
Whoever should speak, 100 Gattasatanga, 100 Gattas, 100 verses that are connected with the path of connected with the useless path.

6
00:01:14,000 --> 00:01:24,000
So that they do in the wrong, mislead you.

7
00:01:24,000 --> 00:01:28,000
Our useless basically.

8
00:01:28,000 --> 00:01:33,000
One word of Dhamma is greater.

9
00:01:33,000 --> 00:01:39,000
Very much the same as the past two verses, but 103 is different.

10
00:01:39,000 --> 00:01:48,000
A well-coated verse. Who should conquer thousands of thousands?

11
00:01:48,000 --> 00:01:53,000
A thousand times a thousand.

12
00:01:53,000 --> 00:02:03,000
Sajasang Sajasana, Sangame, Manusi, Jene, in war, a thousand men conquer.

13
00:02:03,000 --> 00:02:12,000
Who should a thousand thousand, a million men conquer in war?

14
00:02:12,000 --> 00:02:14,000
A thousand men in war.

15
00:02:14,000 --> 00:02:27,000
The one victory which is the victory over oneself.

16
00:02:27,000 --> 00:02:33,000
That over oneself is better.

17
00:02:33,000 --> 00:02:41,000
The victory over oneself, Sajasana, Manusi, that is the highest conquering in battle.

18
00:02:41,000 --> 00:02:43,000
The conquering of oneself.

19
00:02:43,000 --> 00:02:50,000
So who should conquer a million people, a million enemies in battle?

20
00:02:50,000 --> 00:02:54,000
It's still better to conquer oneself.

21
00:02:54,000 --> 00:03:01,000
This is the best conquering in war.

22
00:03:01,000 --> 00:03:10,000
So these two verses we are told were recited in regards to Kundalakacy.

23
00:03:10,000 --> 00:03:14,000
So Kundalakacy is Kaysai's hair.

24
00:03:14,000 --> 00:03:16,000
Kaysai is one who has hair.

25
00:03:16,000 --> 00:03:19,000
Kundalai is the spiral.

26
00:03:19,000 --> 00:03:32,000
And so the story goes that there was a girl of sixteen years old who was the daughter of a rich merchant in Rajagaha.

27
00:03:32,000 --> 00:03:44,000
And at the time there seemed to be, there was apparently a practice whereby women were kept in seclusion or kept locked up.

28
00:03:44,000 --> 00:03:51,000
Because there was a belief that I think even the commentary espouses that,

29
00:03:51,000 --> 00:03:55,000
quote unquote, when women reach this age they burn and long for men.

30
00:03:55,000 --> 00:04:03,000
So there was the idea that she would probably be rather promiscuous.

31
00:04:03,000 --> 00:04:05,000
I mean, it's actually quite fair.

32
00:04:05,000 --> 00:04:11,000
It's just that it doesn't apply equally to, it doesn't apply only to women.

33
00:04:11,000 --> 00:04:13,000
The same thing happens to men at that age.

34
00:04:13,000 --> 00:04:17,000
So to be fair, they should be locking all the men up.

35
00:04:17,000 --> 00:04:22,000
But society at that time being what it was, they locked the women up.

36
00:04:22,000 --> 00:04:28,000
Well, really to keep them safe, not just because they didn't trust them, but because to protect them.

37
00:04:28,000 --> 00:04:37,000
And to ensure that they married someone who was suitable rather than just going with whoever.

38
00:04:37,000 --> 00:04:41,000
So they kept her on the seventh floor of their house.

39
00:04:41,000 --> 00:04:45,000
They had a kind of a palatial residence with seven floors.

40
00:04:45,000 --> 00:04:48,000
Maybe it just had many floors, but on the top floor.

41
00:04:48,000 --> 00:04:54,000
And one day there was a parade, which apparently was another custom,

42
00:04:54,000 --> 00:04:58,000
where they would parade criminals through the city.

43
00:04:58,000 --> 00:05:02,000
And so they had caught this rather vicious criminal.

44
00:05:02,000 --> 00:05:10,000
And we're parading him through the town, taking him to the place of execution.

45
00:05:10,000 --> 00:05:15,000
It's a way of kind of boasting that they had caught bragging about it.

46
00:05:15,000 --> 00:05:24,000
And sort of promoting the system of government, yes, yes, look, we've caught this guy.

47
00:05:24,000 --> 00:05:25,000
Look how great we are.

48
00:05:25,000 --> 00:05:28,000
And then they would take them and cut their heads off.

49
00:05:28,000 --> 00:05:35,000
So they were taking this guy through the streets and whipping him and people throwing rotten vegetables at him and all.

50
00:05:35,000 --> 00:05:43,000
And then they passed by this house where this woman, this girl, is bored to tears up on the top floor of her house.

51
00:05:43,000 --> 00:05:48,000
And she looks down and she sees this man being dragged through the streets.

52
00:05:48,000 --> 00:05:52,000
And immediately she falls in love with him.

53
00:05:52,000 --> 00:05:58,000
If we were of that sort of bent, we might say that she had met her soul mate.

54
00:05:58,000 --> 00:06:01,000
This man was her soul mate.

55
00:06:01,000 --> 00:06:03,000
Of course in Buddhism we don't have such beliefs.

56
00:06:03,000 --> 00:06:08,000
And with a good reason as this story actually well illustrates.

57
00:06:08,000 --> 00:06:15,000
But her, this man was the man for her. This was, she'd never seen someone, never had someone have this effect on her.

58
00:06:15,000 --> 00:06:24,000
And we all, I think without proper instruction, tend to get this sort of idea where, wow, it must mean something.

59
00:06:24,000 --> 00:06:26,000
I fall in love with this person.

60
00:06:26,000 --> 00:06:28,000
It must mean that they are the one for me.

61
00:06:28,000 --> 00:06:32,000
We have all these songs to back that sort of idea up.

62
00:06:32,000 --> 00:06:34,000
We even apply it to food and so on.

63
00:06:34,000 --> 00:06:39,000
We think that it means something that I prefer this food, I prefer salty food, I prefer spicy food.

64
00:06:39,000 --> 00:06:41,000
And somehow therefore that's what I should eat.

65
00:06:41,000 --> 00:06:45,000
When in fact that could be the very food that kills you in the end.

66
00:06:45,000 --> 00:06:48,000
And the same goes with many different things.

67
00:06:48,000 --> 00:07:03,000
So our partiality, our preference for or against something, is often a very poor indicator of the benefits of it or the benefits of the object of our desires.

68
00:07:03,000 --> 00:07:14,000
Nonetheless, she became instantly obsessed with this man and so she told her servants to call her parents and said,

69
00:07:14,000 --> 00:07:18,000
and they came and she said, I must have that man as my husband.

70
00:07:18,000 --> 00:07:24,000
And they said, what are you talking about? How could we possibly, are you crazy?

71
00:07:24,000 --> 00:07:29,000
What do you want with such a person like that? Don't talk nonsense.

72
00:07:29,000 --> 00:07:35,000
And she said, if I don't get him as my husband, I'll stop eating.

73
00:07:35,000 --> 00:07:41,000
I will die here. I won't be able to live.

74
00:07:41,000 --> 00:07:45,000
It won't be possible for me to continue living if I don't get him as my husband.

75
00:07:45,000 --> 00:07:52,000
Only if I can have him as my soul mate, will I be able to live and so she refused to eat.

76
00:07:52,000 --> 00:07:58,000
And she just claimed that no matter what she was going to starve herself.

77
00:07:58,000 --> 00:08:08,000
So they, well, first the mother, so first the mother came and she said this to the mother and then the mother couldn't convince her.

78
00:08:08,000 --> 00:08:13,000
She said, well, we'll give you anyone. You can have, we'll find a suitable husband for you.

79
00:08:13,000 --> 00:08:18,000
Someone who is worthy of you. Not this terrible vicious thief that she wouldn't have it.

80
00:08:18,000 --> 00:08:24,000
So she called the mother called the father. The father came and tried to bully her into it and convince her into it.

81
00:08:24,000 --> 00:08:29,000
And then sort of let go of this man. And she wouldn't have any of it.

82
00:08:29,000 --> 00:08:34,000
Which just goes to show you how pernicious clinging can be.

83
00:08:34,000 --> 00:08:44,000
And so finally they, at their wits end, they called to some king's officer and gave him a thousand gold coins.

84
00:08:44,000 --> 00:08:53,000
And said, we must have that man. And so the man, this king's man, took the money.

85
00:08:53,000 --> 00:09:04,000
And replace, switch this, this thief, this terrible villain with some innocent person and had that innocent person executed.

86
00:09:04,000 --> 00:09:12,000
And then sent the thief off to the rich man and then sent word onto the king that the villain had been executed.

87
00:09:12,000 --> 00:09:17,000
And so this woman was ecstatic. This 16 year old girl really.

88
00:09:17,000 --> 00:09:26,000
And so she married this man and strove to really please him.

89
00:09:26,000 --> 00:09:34,000
Like she said, I've rescued you so that you can be my husband and now I will serve you as your faithful wife.

90
00:09:34,000 --> 00:09:39,000
And she tried her best to please him and do everything for him. She cooked for him. She cleaned for him.

91
00:09:39,000 --> 00:09:49,000
As she did everything, she was like the model sort of, well, I guess you could say that a slave, wife, no servant of sorts.

92
00:09:49,000 --> 00:09:55,000
Out of her dedication and devotion and her sincere belief that this guy was meant for her.

93
00:09:55,000 --> 00:10:01,000
And there was something to that love. Because she had been always taught that sort of thing.

94
00:10:01,000 --> 00:10:08,000
I think young women in traditional societies were often taught that, well, you know, someday my prince will come and all that.

95
00:10:08,000 --> 00:10:16,000
So this is where she was headed. That was how she had been taught.

96
00:10:16,000 --> 00:10:21,000
And it's funny how women are portrayed just as a little aside here.

97
00:10:21,000 --> 00:10:29,000
In the Buddhist texts, women are often portrayed a little bit with a little bit of sexism.

98
00:10:29,000 --> 00:10:37,000
You can't deny it. Like that quote that I gave you. It's typical of the commonaries to talk about how insatiable women are.

99
00:10:37,000 --> 00:10:44,000
But it's not exactly sexism. It's because this is the kind of thing that you have to remind monks of.

100
00:10:44,000 --> 00:10:48,000
Male monks because that would have been the main audience for these texts.

101
00:10:48,000 --> 00:11:00,000
For the most part, they would be men who were, you know, of a bent to want to incline towards attraction towards women.

102
00:11:00,000 --> 00:11:13,000
You know, thinking this sort of thing. And so you have this constant reminder, you know, constant, but this, every so often you'll come across a passage like this that just says women are, you can't trust them.

103
00:11:13,000 --> 00:11:19,000
They'll cheat on you. They'll, they're insatiable and they'll always want to have more children and so on and so on.

104
00:11:19,000 --> 00:11:29,000
Talking about how, if you read these out of context or if you just read them at face value, then they really sort of paint women in a bad light.

105
00:11:29,000 --> 00:11:41,000
But as to women themselves that we're going to see in this story, women are given, I think, a fairly fair treatment in, in the Dhamma in the Buddhist teaching.

106
00:11:41,000 --> 00:11:53,000
That especially in the, in the society where they really weren't given us, you can see much of a fair treatment. They're being locked up and all that and having to wait on their husbands and so on.

107
00:11:53,000 --> 00:11:59,000
So anyway, we'll see how this develops, but at the time this is what she thought was the right thing to do.

108
00:11:59,000 --> 00:12:19,000
And so the story starts to sound like this is the moral of the story, which it's not, but it sounds to this point that this is the thing she got her husband and now her duty for her life is to, to devote herself to him.

109
00:12:19,000 --> 00:12:30,000
And so really you would think that the average thief who was just about to be executed would be overjoyed with this, but what was going through the thief's mind.

110
00:12:30,000 --> 00:12:46,000
The only thing he could think of it says is when can I kill this woman, steal her jewels and go back to living a free life where I can eat in any tavern or restaurant that I want and where I can make money.

111
00:12:46,000 --> 00:12:58,000
However, I want usually by killing and raping and murdering people and no killing and raping and pillaging and robbing people.

112
00:12:58,000 --> 00:13:03,000
So indeed that's what he schemed to do.

113
00:13:03,000 --> 00:13:12,000
So for a few days he was thinking about how he could take advantage of the situation until finally he came across the plan.

114
00:13:12,000 --> 00:13:16,000
He figured he saw what was going to work.

115
00:13:16,000 --> 00:13:22,000
And so he laid down in bed and wouldn't get up and he said husband what's wrong.

116
00:13:22,000 --> 00:13:27,000
And he said, I feel really sick, I'm really bad.

117
00:13:27,000 --> 00:13:29,000
And he said why?

118
00:13:29,000 --> 00:13:36,000
Well, I see the thing is, and he starts to spin his yarn.

119
00:13:36,000 --> 00:13:51,000
The thing is, the reason I'm free, I mean thank you for freeing me, but the reason why I'm free is I made a vow to the spirit on the top of one of the mountains.

120
00:13:51,000 --> 00:13:59,000
That if I got free I would give some offering to the angel, to the deity.

121
00:13:59,000 --> 00:14:07,000
And that's who sent me you. You were sent by this angel, that's why I caught your eye that day.

122
00:14:07,000 --> 00:14:10,000
And that's why now you're my wife.

123
00:14:10,000 --> 00:14:15,000
And he said, I just feel really bad because I made that promise and I haven't fulfilled it.

124
00:14:15,000 --> 00:14:17,000
And he said, well then let's go fulfill it.

125
00:14:17,000 --> 00:14:21,000
And he said, oh really? You're willing to do that? Oh yes.

126
00:14:21,000 --> 00:14:30,000
And he said, what do we need? Well, he said, well we should take some rice porridge with honey and the five kinds of flowers.

127
00:14:30,000 --> 00:14:34,000
Whatever five kinds those are. And she agreed.

128
00:14:34,000 --> 00:14:39,000
And so they went up to the top of the place that's called Robber's Peak.

129
00:14:39,000 --> 00:14:46,000
Apparently there's a place called Robber's Peak which is where they would, in the past maybe they would throw people over the cliff.

130
00:14:46,000 --> 00:14:57,000
So it was a, you could go up one side and then there would be a cliff face and they would take Robber, take villains up and they would throw them over the cliff and they would fall down to their death.

131
00:14:57,000 --> 00:14:59,000
So he took her up there.

132
00:14:59,000 --> 00:15:08,000
And told her, he said, bring all your jewels, leave all your servants behind but bring all your finery all your valuables.

133
00:15:08,000 --> 00:15:16,000
No, he didn't say it like that. He said, put on all your finest jewels and clothes and so on and come with me.

134
00:15:16,000 --> 00:15:23,000
And we'll go together. And so they went up and they even told the servants not to follow them.

135
00:15:23,000 --> 00:15:30,000
And when they got to the top of the cliff, he just stood there.

136
00:15:30,000 --> 00:15:35,000
And she said, okay so what are we going to do? And he's, how are we going to do this offering?

137
00:15:35,000 --> 00:15:39,000
And he said, I don't really, I don't care about any offer, it was really just a lie.

138
00:15:39,000 --> 00:15:45,000
I'm here to steal your jewels and kill you. So, sorry.

139
00:15:45,000 --> 00:15:51,000
And it was just like a defining moment in this woman's life.

140
00:15:51,000 --> 00:15:57,000
Like, can you imagine having really your whole world?

141
00:15:57,000 --> 00:16:08,000
Every this man was her world, not for very long but it had just something that she had been preparing herself to dedicate her life to.

142
00:16:08,000 --> 00:16:18,000
And to suddenly have it pulled out from underneath her and more over her whole outlook on life to have it just shattered.

143
00:16:18,000 --> 00:16:29,000
This was my soul name. This is a joke. And it wasn't a joke. And he said, no, I'm ready to.

144
00:16:29,000 --> 00:16:35,000
Ready to kill you. There's nothing, no need to be if to cry and this is it.

145
00:16:35,000 --> 00:16:43,000
I'll take your jewels now and then throw you over the edge. That's all that's left now.

146
00:16:43,000 --> 00:16:49,000
And so she said, look, take my jewels. There's no need to kill me. I've been kind to you.

147
00:16:49,000 --> 00:16:53,000
I promise I won't tell anyone and he said, I can't trust you if I let you go.

148
00:16:53,000 --> 00:16:58,000
You'll turn me in and they'll come looking for me and they'll execute me for real this time.

149
00:16:58,000 --> 00:17:06,000
If I'm going to do this, I have to kill you. So, sorry. But that's the breaks.

150
00:17:06,000 --> 00:17:23,000
And so suddenly her mind cleared up with this. She was she's freed from her stupor and she just just mentally hit herself over the head.

151
00:17:23,000 --> 00:17:34,000
She was shaking her head at herself. How, how, how gullible she had been. How, how misled she had been. How intoxicated she had been.

152
00:17:34,000 --> 00:17:44,000
But her wisdom came back. And she said, she said to herself, this wisdom is not not just for cooking and cleaning.

153
00:17:44,000 --> 00:17:55,000
And so she thought of a plan and she said to him, okay, husband, if that's your wish, then that's your wife.

154
00:17:55,000 --> 00:18:04,000
All I ask is that you allow me to pay obedient to you to pay respect to you.

155
00:18:04,000 --> 00:18:14,000
And bow down to you and he said, fine, fine, whatever you must last last request and then you're gone.

156
00:18:14,000 --> 00:18:26,000
And so she held her hands up in supplication and went around him three times and then embraced him from the front and then embraced him from behind.

157
00:18:26,000 --> 00:18:37,000
And when she got behind him, she grabbed him in the back of his head and the lower back and pushed him over the edge.

158
00:18:37,000 --> 00:18:42,000
And he fell to his death.

159
00:18:42,000 --> 00:18:57,000
And so this woman now fully awakened in the mundane sense to her folly.

160
00:18:57,000 --> 00:19:15,000
Her whole life had been her whole outlook on life and been a sham and been a lie. The whole idea of true love, love at first sight, etc., etc. was just a lie.

161
00:19:15,000 --> 00:19:24,000
And so, and furthermore, she had just killed her husband and that wasn't likely to go over well if she went back home.

162
00:19:24,000 --> 00:19:37,000
Instead, she threw her through her valuables away and went and found some rags and decided to go forth and she became a wonder.

163
00:19:37,000 --> 00:19:49,000
As a wonder, she wandered through India until she came to a group of ascetics, female ascetics and she asked to join them.

164
00:19:49,000 --> 00:19:57,000
And I guess there were female ascetics at that time. Or maybe there were male ascetics.

165
00:19:57,000 --> 00:20:06,000
And asked them if she could join them. And this is I think where they actually pulled her hair out because that was the thing in India.

166
00:20:06,000 --> 00:20:14,000
They would put boards, put two boards on their shoulder and they had a big pit.

167
00:20:14,000 --> 00:20:20,000
And they put you in the pit and put boards on your shoulders and they'd stand on those boards.

168
00:20:20,000 --> 00:20:25,000
And then they'd take a special comb and they'd actually pull your hair out by the roots.

169
00:20:25,000 --> 00:20:30,000
And that's how they would ordain you. And I think that's how she got the name Kundalakacy.

170
00:20:30,000 --> 00:20:37,000
So her hair ended up being stunned by that or turned and it became earlier.

171
00:20:37,000 --> 00:20:42,000
Maybe she just had curly hair. I don't know.

172
00:20:42,000 --> 00:20:51,000
Anyway, she went forth and they taught her 1,000 views or beliefs, 1,000 articles of faith.

173
00:20:51,000 --> 00:20:59,000
So these were 1,000 teachings in this other religion.

174
00:20:59,000 --> 00:21:03,000
We don't know what those 1,000 things were, but these were the 1,000 things.

175
00:21:03,000 --> 00:21:16,000
And she became so skilled in them that she was able to use them and able to really be funnel any of her any person she debated with anyone who thought to question her.

176
00:21:16,000 --> 00:21:28,000
So she became quite an exceptional debater or religious leader, preacher.

177
00:21:28,000 --> 00:21:37,000
And finally, and so finally they said to her, well, now you can be a real teacher of our religion.

178
00:21:37,000 --> 00:21:40,000
And so they sent her out into the world, into India.

179
00:21:40,000 --> 00:21:46,000
And they gave her a branch of a rose apple tree, which was the tree of India.

180
00:21:46,000 --> 00:21:57,000
And they said, take this branch and put it down and use this branch to challenge other ascetics.

181
00:21:57,000 --> 00:22:00,000
And convert them to our doctrine.

182
00:22:00,000 --> 00:22:11,000
And they said, if you find someone, if someone is able, if you have a lay, if they're a lay person, is able to defeat you in debate,

183
00:22:11,000 --> 00:22:14,000
then you have to disrobe and become their servant.

184
00:22:14,000 --> 00:22:22,000
If a monk is able to defeat you in debate, then you have to become their student.

185
00:22:22,000 --> 00:22:25,000
That's your task.

186
00:22:25,000 --> 00:22:29,000
That's what you have to look for, find someone who is better than you.

187
00:22:29,000 --> 00:22:38,000
And so she went through it in India and she was so skilled in debate that no one could defeat her on any one of the 1,000 points.

188
00:22:38,000 --> 00:22:46,000
And so eventually she got a real reputation and all the other ascetics were afraid to challenge her.

189
00:22:46,000 --> 00:22:48,000
Everyone was afraid to challenge her.

190
00:22:48,000 --> 00:22:51,000
And so she eventually got a little bit.

191
00:22:51,000 --> 00:23:02,000
Well, anyway, she went wandering far and wide and eventually she came to Sawati, which would present a little bit more of a challenge to her.

192
00:23:02,000 --> 00:23:04,000
And so she went for arms in Sawati.

193
00:23:04,000 --> 00:23:10,000
But before she went into the city, she took her rose apple branch and she would change it.

194
00:23:10,000 --> 00:23:11,000
You know, it had leaves on it.

195
00:23:11,000 --> 00:23:16,000
So it was a fresh branch and she would cut down a new one when the old one withered out.

196
00:23:16,000 --> 00:23:21,000
But somehow a symbol.

197
00:23:21,000 --> 00:23:27,000
And so she stuck it in a pile of sand or stuck it in the earth and would leave it there.

198
00:23:27,000 --> 00:23:41,000
And she told this boy, she said, look, if anyone wants to challenge me, tell them that they should do a trample that.

199
00:23:41,000 --> 00:23:47,000
Or people just knew maybe that they should trample this branch if they wanted a challenge her.

200
00:23:47,000 --> 00:23:52,000
And so she went for arms and left this branch there and everyone was afraid to go near it.

201
00:23:52,000 --> 00:24:02,000
And these boys sort of gathered around it to see who would dare to challenge the famous, what they called her, the ascetic of the rose apple.

202
00:24:02,000 --> 00:24:10,000
For some reason she was carrying this branch around and it was her symbol by which people would know that she had come and she was ready to challenge her.

203
00:24:10,000 --> 00:24:15,000
And she was ready to challenge anyone.

204
00:24:15,000 --> 00:24:25,000
And that morning, who should happen along, but the well-known monk was in Sariputa.

205
00:24:25,000 --> 00:24:32,000
On that morning, Sariputa came by and saw the rose apple branch.

206
00:24:32,000 --> 00:24:44,000
And that's the boys, well, what does this mean? Where does this come from? And the boys told him the story that this was from the rose apple ascetic.

207
00:24:44,000 --> 00:24:53,000
She had come to challenge other ascetics to a debate and whoever would like to debate her should trample this.

208
00:24:53,000 --> 00:25:01,000
And he said to the boys, well, then trample it, pull it up and step on it, step all over it.

209
00:25:01,000 --> 00:25:06,000
And they said, we're afraid to, venerable sir.

210
00:25:06,000 --> 00:25:12,000
I said, oh, don't be afraid. I'll answer her questions. Just go ahead.

211
00:25:12,000 --> 00:25:17,000
And then he went and he sat down under a tree nearby.

212
00:25:17,000 --> 00:25:24,000
And sure enough, the boys trampled on this branch and they stood there jumping on it and stomping on it.

213
00:25:24,000 --> 00:25:31,000
And the ascetic came out and she saw this and she scolded them.

214
00:25:31,000 --> 00:25:38,000
He said, what's the meaning of this? Come on, I'm not going to debate with you little boys.

215
00:25:38,000 --> 00:25:43,000
And they said, he told me to do it. They pointed to the elder.

216
00:25:43,000 --> 00:25:50,000
Sariputa. And she turned and she said, is that true? And he said, yes.

217
00:25:50,000 --> 00:25:57,000
And he said, well, then will you debate with me? And she said, sure.

218
00:25:57,000 --> 00:26:03,000
And so she said, come by my residence and will debate.

219
00:26:03,000 --> 00:26:09,000
And so she went, she got ready, I guess, went and prepared herself in meanwhile.

220
00:26:09,000 --> 00:26:17,000
These boys or whoever was around spread word and all the people of the city spread the word and

221
00:26:17,000 --> 00:26:23,000
said, Sariputa is going to debate with the ascetic of the rose apple tree.

222
00:26:23,000 --> 00:26:28,000
And no, this would be something to see. And so everyone was excited.

223
00:26:28,000 --> 00:26:39,000
And so they all came along followed after the rose apple ascetic to Sariputa's residence.

224
00:26:39,000 --> 00:26:47,000
And they sat outside of his good day and everyone sat down and listened.

225
00:26:47,000 --> 00:26:53,000
And the woman said, the ascetic said to Sariputa, I'd like to ask you a question.

226
00:26:53,000 --> 00:26:55,000
And they said, well, then ask.

227
00:26:55,000 --> 00:27:02,000
And she asked him all, he asked some questions about each of the 1,000 beliefs.

228
00:27:02,000 --> 00:27:06,000
And he was able to refute each and every one of them.

229
00:27:06,000 --> 00:27:12,000
That no one had ever been able to refute even one of until she was out.

230
00:27:12,000 --> 00:27:17,000
And he said, he said, so he only has these few questions.

231
00:27:17,000 --> 00:27:19,000
Do you have any more?

232
00:27:19,000 --> 00:27:22,000
After she asked the 1,000, right?

233
00:27:22,000 --> 00:27:26,000
And he said, and she said, no, that's all.

234
00:27:26,000 --> 00:27:29,000
Realizing that she had now finally been vested.

235
00:27:29,000 --> 00:27:33,000
And he said, OK, well, then I'd like to ask you a question.

236
00:27:33,000 --> 00:27:38,000
Will you answer me? And she said, yes, for sure.

237
00:27:38,000 --> 00:27:45,000
And he said, what is one?

238
00:27:45,000 --> 00:27:48,000
Now, this may not have quite the significance.

239
00:27:48,000 --> 00:27:52,000
I think it actually sounds more profound than it actually is.

240
00:27:52,000 --> 00:27:58,000
But the truth of this question is it's the first question that you're supposed to ask of a novice monk,

241
00:27:58,000 --> 00:28:02,000
meaning it's the first thing that a novice Buddhist learns.

242
00:28:02,000 --> 00:28:07,000
And don't worry if you've never heard of these questions there.

243
00:28:07,000 --> 00:28:12,000
They're not so much used as far as I've seen, although this was the tradition.

244
00:28:12,000 --> 00:28:16,000
These are the 10 questions that a novice should learn.

245
00:28:16,000 --> 00:28:20,000
So it's what is one, what are two, what are three, what are four,

246
00:28:20,000 --> 00:28:26,000
what is four, what is five, six, seven, eight, nine, ten, and it's just one to ten.

247
00:28:26,000 --> 00:28:30,000
So he asked, economic, what is one?

248
00:28:30,000 --> 00:28:35,000
And she couldn't figure out, she couldn't answer even this one simple question.

249
00:28:35,000 --> 00:28:37,000
It was too simple.

250
00:28:37,000 --> 00:28:43,000
It was too, too ambiguous, really.

251
00:28:43,000 --> 00:28:45,000
Like, what could one be?

252
00:28:45,000 --> 00:28:47,000
So do you say it's that?

253
00:28:47,000 --> 00:28:53,000
You can always be attacked no matter which way you answer that.

254
00:28:53,000 --> 00:28:56,000
And she said, I can't answer it.

255
00:28:56,000 --> 00:29:01,000
And he said, please tell me, please tell me the answer.

256
00:29:01,000 --> 00:29:04,000
And he said, well, if you're a Dane, under me,

257
00:29:04,000 --> 00:29:06,000
then, or if you're a Dane in our religion youth,

258
00:29:06,000 --> 00:29:10,000
then we can give you the answer for short.

259
00:29:10,000 --> 00:29:14,000
And they said, she said, immediately, well, then, ordain me.

260
00:29:14,000 --> 00:29:17,000
And so he sent her to the bikunis, and the bikunis are a Dane.

261
00:29:17,000 --> 00:29:19,000
And she became known as kun-de-ce.

262
00:29:19,000 --> 00:29:23,000
This is the story of kun-de-ce.

263
00:29:23,000 --> 00:29:25,000
Within a few days, or a few weeks,

264
00:29:25,000 --> 00:29:28,000
there were very short times she became in our hunt.

265
00:29:28,000 --> 00:29:33,000
And she had all sorts of magical powers and supernatural faculties,

266
00:29:33,000 --> 00:29:37,000
and of course, free from suffering.

267
00:29:37,000 --> 00:29:40,000
And she became, I think, one of the great disciples of the Buddha.

268
00:29:40,000 --> 00:29:45,000
Doesn't mention it here, but she is one of the well-known.

269
00:29:45,000 --> 00:29:47,000
She's well-known for her wisdom.

270
00:29:47,000 --> 00:29:51,000
She's well-known for her quick thinking,

271
00:29:51,000 --> 00:29:55,000
but mostly known for the story,

272
00:29:55,000 --> 00:29:57,000
well, for having killed her husband.

273
00:29:57,000 --> 00:30:00,000
Of course, not the best thing to be known for,

274
00:30:00,000 --> 00:30:03,000
or to have been such a fool.

275
00:30:03,000 --> 00:30:10,000
But for waking up, and for quickly adapting to the new information,

276
00:30:10,000 --> 00:30:13,000
once she saw how foolish she had been,

277
00:30:13,000 --> 00:30:16,000
she was quickly able to adapt herself.

278
00:30:16,000 --> 00:30:18,000
And that's admirable.

279
00:30:18,000 --> 00:30:22,000
I mean, doing evil deeds is often just because of wrong view,

280
00:30:22,000 --> 00:30:24,000
because you've been taught the wrong things.

281
00:30:24,000 --> 00:30:27,000
But some people, once they realize the truth,

282
00:30:27,000 --> 00:30:30,000
and realize the error of their ways, they don't change.

283
00:30:30,000 --> 00:30:33,000
They're unable to change. They're not strong enough.

284
00:30:33,000 --> 00:30:36,000
And so it's admirable that she was able to change.

285
00:30:36,000 --> 00:30:41,000
And so profoundly changed that she actually became in our hunt.

286
00:30:41,000 --> 00:30:43,000
And so the monks were talking about this.

287
00:30:43,000 --> 00:30:47,000
They're talking about two things that were pretty impressive.

288
00:30:47,000 --> 00:30:52,000
First of all, the first impressive thing was the fact

289
00:30:52,000 --> 00:30:55,000
that all it took was one question.

290
00:30:55,000 --> 00:30:58,000
And so this relates to the first first.

291
00:30:58,000 --> 00:31:01,000
So they were talking about how incredible it was

292
00:31:01,000 --> 00:31:03,000
that with just such a short teaching,

293
00:31:03,000 --> 00:31:05,000
she was able to realize the truth.

294
00:31:05,000 --> 00:31:09,000
And then the second thing is that

295
00:31:09,000 --> 00:31:12,000
I'm actually not quite so impressive,

296
00:31:12,000 --> 00:31:14,000
but it seemed to be an impressive thing

297
00:31:14,000 --> 00:31:16,000
and the monks were talking about

298
00:31:16,000 --> 00:31:21,000
how shrewd she was, how clever she was

299
00:31:21,000 --> 00:31:25,000
at being able to outwit her husband and defeat him.

300
00:31:25,000 --> 00:31:28,000
He was ready to kill her.

301
00:31:28,000 --> 00:31:30,000
She ended up killing him.

302
00:31:30,000 --> 00:31:34,000
And it's amazing that she was able to think so quickly

303
00:31:34,000 --> 00:31:37,000
and defeat him.

304
00:31:37,000 --> 00:31:40,000
And so, of course, the Buddha came in

305
00:31:40,000 --> 00:31:42,000
and asked them what they were talking about

306
00:31:42,000 --> 00:31:45,000
when he found out what they were talking about.

307
00:31:45,000 --> 00:31:48,000
When they found out what he was talking about,

308
00:31:48,000 --> 00:31:53,000
they said he pointed out both in regards to both things.

309
00:31:53,000 --> 00:31:57,000
First he said, well, it's not a matter of quantity.

310
00:31:57,000 --> 00:31:58,000
It's a matter of quality.

311
00:31:58,000 --> 00:32:00,000
And we, of course, dealt with this before.

312
00:32:00,000 --> 00:32:02,000
But then he talks about the second verse,

313
00:32:02,000 --> 00:32:04,000
which is a new theme.

314
00:32:04,000 --> 00:32:06,000
And he said, well, you know, defeating her husband

315
00:32:06,000 --> 00:32:08,000
wasn't really that impressive.

316
00:32:08,000 --> 00:32:11,000
Defeating oneself, conquering oneself.

317
00:32:11,000 --> 00:32:13,000
That's impressive.

318
00:32:13,000 --> 00:32:16,000
And so this verse is actually quite important.

319
00:32:16,000 --> 00:32:19,000
And it's something that we always remind ourselves of.

320
00:32:19,000 --> 00:32:22,000
Whenever we want to get in fights or win arguments

321
00:32:22,000 --> 00:32:24,000
or when we feel good about ourselves

322
00:32:24,000 --> 00:32:27,000
because we're successful, because we're competitive

323
00:32:27,000 --> 00:32:31,000
for it, because we are able to defeat others in army

324
00:32:31,000 --> 00:32:35,000
in conflict or in debate or in war.

325
00:32:35,000 --> 00:32:40,000
And so this is contrary to the Buddha's teaching

326
00:32:40,000 --> 00:32:44,000
that this kind of defeating other people is actually quite

327
00:32:44,000 --> 00:32:46,000
meaningless.

328
00:32:46,000 --> 00:32:48,000
It doesn't have any real benefit.

329
00:32:48,000 --> 00:32:50,000
It's conquering ourselves.

330
00:32:50,000 --> 00:32:53,000
This is the greatest conquest.

331
00:32:53,000 --> 00:32:58,000
The greatest conquest is the teaching.

332
00:32:58,000 --> 00:33:01,000
And so that's the Dhammapada verse that we have,

333
00:33:01,000 --> 00:33:04,000
verses 102 and 103.

334
00:33:04,000 --> 00:33:06,000
How it relates to our practice,

335
00:33:06,000 --> 00:33:08,000
talking just about the second verse,

336
00:33:08,000 --> 00:33:12,000
it's really a good way to describe what we're talking about

337
00:33:12,000 --> 00:33:16,000
when we talk about meditation, self-conquest,

338
00:33:16,000 --> 00:33:18,000
working to better oneself,

339
00:33:18,000 --> 00:33:22,000
working to overcome the enemies inside.

340
00:33:22,000 --> 00:33:24,000
He talks about the villains.

341
00:33:24,000 --> 00:33:27,000
So they're talking about, oh,

342
00:33:27,000 --> 00:33:28,000
she conquered this villain.

343
00:33:28,000 --> 00:33:31,000
And the Buddha comes in and says, he actually says,

344
00:33:31,000 --> 00:33:33,000
well, it's the villains in the heart.

345
00:33:33,000 --> 00:33:35,000
It's the evil ones in the heart.

346
00:33:35,000 --> 00:33:38,000
These enemies that are inside of us.

347
00:33:38,000 --> 00:33:42,000
This is the real conquest that is praiseworthy,

348
00:33:42,000 --> 00:33:44,000
that you should praise.

349
00:33:44,000 --> 00:33:47,000
And on the fact that she was able to become an Arhan

350
00:33:47,000 --> 00:33:54,000
to give up all of her defilements.

351
00:33:54,000 --> 00:33:59,000
And so there's a difference between what we esteem in the world.

352
00:33:59,000 --> 00:34:02,000
Most people will esteem you by the accomplishments

353
00:34:02,000 --> 00:34:05,000
you've made because that's all we can see.

354
00:34:05,000 --> 00:34:08,000
That's what we normally, what we are accustomed to.

355
00:34:08,000 --> 00:34:12,000
We don't realize the profound benefit

356
00:34:12,000 --> 00:34:18,000
and importance of self-conquest,

357
00:34:18,000 --> 00:34:22,000
of bettering oneself and curing oneself

358
00:34:22,000 --> 00:34:25,000
of the things that cause us suffering,

359
00:34:25,000 --> 00:34:29,000
the bad habits and addictions and aversions

360
00:34:29,000 --> 00:34:34,000
just all the things that hurt us and hurt others,

361
00:34:34,000 --> 00:34:36,000
which is what we do in meditation.

362
00:34:36,000 --> 00:34:39,000
We change the mind.

363
00:34:39,000 --> 00:34:42,000
We tame the mind so that the mind just sees things

364
00:34:42,000 --> 00:34:45,000
objectively without reacting.

365
00:34:52,000 --> 00:34:55,000
And the Buddha said, the untrained mind is your worst enemy.

366
00:34:55,000 --> 00:34:57,000
And the trained mind is your best friend.

367
00:34:57,000 --> 00:34:59,000
That's what this verse is referring to.

368
00:34:59,000 --> 00:35:02,000
So quite simple, that powerful, meaningful,

369
00:35:02,000 --> 00:35:05,000
and something to remember whenever you feel like

370
00:35:05,000 --> 00:35:08,000
you should gloat or feel like you should conquer others,

371
00:35:08,000 --> 00:35:10,000
feel like you should fight,

372
00:35:10,000 --> 00:35:14,000
feel like it's somehow meaningful to win an argument.

373
00:35:14,000 --> 00:35:18,000
And remember, now I've just actually lost a more important battle.

374
00:35:18,000 --> 00:35:20,000
And that's the battle with myself

375
00:35:20,000 --> 00:35:27,000
because now I've given rise to ego and attachment and so on.

376
00:35:27,000 --> 00:35:30,000
So that's the dumb-apada for tonight.

377
00:35:30,000 --> 00:35:51,000
Thank you all for tuning in.

