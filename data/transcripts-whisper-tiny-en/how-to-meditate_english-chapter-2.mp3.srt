1
00:00:00,000 --> 00:00:06,000
How to meditate by you to Domo Beepu, read by Adirokes.

2
00:00:06,000 --> 00:00:09,000
Chapter 2 Sitting Meditation

3
00:00:09,000 --> 00:00:18,000
In this chapter, I will explain how to put into practice the principles learned in the first chapter during formal sitting meditation.

4
00:00:18,000 --> 00:00:29,000
Sitting meditation is a simple meditation exercise that can be performed sitting cross-legged on the floor, or even on a chair or bench.

5
00:00:29,000 --> 00:00:36,000
For people unable to sit up at all, a similar technique may be employed in the lying position.

6
00:00:36,000 --> 00:00:49,000
The purpose of formal meditation is to limit our experience to the fewest number of objects in order to allow for easy observation without becoming overwhelmed or distracted.

7
00:00:49,000 --> 00:00:57,000
When sitting still, the whole body is tranquil, and the only movement is when the breath enters and leaves the body.

8
00:00:57,000 --> 00:01:02,000
When the breath enters the body, there should be a rising motion in the abdomen.

9
00:01:02,000 --> 00:01:07,000
When the breath leaves the body, there should likewise be a falling motion.

10
00:01:07,000 --> 00:01:14,000
If the movement is not readily apparent, you can put your hand on your abdomen until it becomes clear.

11
00:01:14,000 --> 00:01:24,000
If it is difficult to perceive the motion of the abdomen even with your hand, you can try lying down on your back until you are able to perceive it.

12
00:01:24,000 --> 00:01:34,000
Difficulty in finding the rising and falling motion of the abdomen when sitting is generally due to mental tension and stress.

13
00:01:34,000 --> 00:01:47,000
If one is patient and persistent in the practice, one's mind and body will begin to relax until one is able to breathe as naturally sitting up as one lying down.

14
00:01:47,000 --> 00:01:58,000
The most important thing to remember is that we are trying to observe the breath in its natural state rather than forcing or controlling it in any way.

15
00:01:58,000 --> 00:02:16,000
In the beginning, the breath may be shallow or uncomfortable, but once the mind begins to let go and stop trying to control the breath, the rise and fall of the abdomen will become more clear and allow for comfortable observation.

16
00:02:16,000 --> 00:02:22,000
It is this rising and falling motion that we will use as our first object of meditation.

17
00:02:22,000 --> 00:02:33,000
Once we are able to observe the motion of the abdomen without difficulty, it will serve as a default object of meditation for us to return to at any time.

18
00:02:33,000 --> 00:02:38,000
The formal method for sitting meditation is as follows.

19
00:02:38,000 --> 00:02:48,000
1. Sit with the legs crossed if possible with one leg in front of the other, neither leg on top of the other.

20
00:02:48,000 --> 00:02:56,000
If this position is uncomfortable, you can sit in any position that is convenient for observation of the abdomen.

21
00:02:56,000 --> 00:03:03,000
2. Sit with one hand on top of the other, palms up on the lap.

22
00:03:03,000 --> 00:03:06,000
3. Sit with the back street.

23
00:03:06,000 --> 00:03:11,000
It is not necessary for the back to be perfectly straight if it is uncomfortable.

24
00:03:11,000 --> 00:03:17,000
As long as the movements of the abdomen are clearly discernible, any posture is okay.

25
00:03:17,000 --> 00:03:20,000
4. Close the eyes.

26
00:03:20,000 --> 00:03:28,000
Since the focus is on the stomach, having the eyes open will only distract the attention away from the object.

27
00:03:28,000 --> 00:03:40,000
5. Send the mind out to the abdomen. When the abdomen rises, give rise to the clear thought, silently in the mind, rising.

28
00:03:40,000 --> 00:03:51,000
And when the stomach falls, falling, repeat this practice until your attention is diverted to another object of awareness.

29
00:03:51,000 --> 00:04:03,000
Again, it is important to understand that the clear thought, rising, or falling, should be in the mind, which should be focused on the abdomen.

30
00:04:03,000 --> 00:04:07,000
It is as though one is speaking into the abdomen.

31
00:04:07,000 --> 00:04:14,000
This practice may be carried out for five or ten minutes, or longer if one is able.

32
00:04:14,000 --> 00:04:25,000
The next step is to incorporate all four foundations into the practice. The body, the feelings, the mind, and the dhammas.

33
00:04:25,000 --> 00:04:31,000
Regarding the body, watching the rising and falling is sufficient for a beginner meditative.

34
00:04:31,000 --> 00:04:41,000
At times, one might wish to also acknowledge the position of the body as sitting, sitting, or lying, lying.

35
00:04:41,000 --> 00:04:56,000
If it is bound to be more conducive for clear observation, in regards to feelings, when a sensation arises in the body, one should fix one's attention on it, discarding the abdomen and focusing on the sensation.

36
00:04:56,000 --> 00:05:04,000
If a feeling of pain should arise, for example, one should take the pain itself as a meditation object.

37
00:05:04,000 --> 00:05:12,000
Any one of the four foundations may serve as a meditation object, as all four are aspects of reality.

38
00:05:12,000 --> 00:05:18,000
It isn't necessary to stay with the rising and falling of the abdomen at all times.

39
00:05:18,000 --> 00:05:31,000
Instead, when pain arises, one should observe the new object, the pain, in order to clearly understand it for what it is, rather than judging or identifying with it.

40
00:05:31,000 --> 00:05:45,000
As explained earlier, the meditator should simply focus on the pain and create the clear thought, pain, pain, pain, pain, and till it goes away.

41
00:05:45,000 --> 00:05:52,000
Instead of getting upset about the pain, one will see it for what it is and let it go.

42
00:05:52,000 --> 00:06:04,000
When happiness arises, one should create the clear thought, happy.

43
00:06:04,000 --> 00:06:08,000
Until that feeling goes away.

44
00:06:08,000 --> 00:06:15,000
Here, the object is to avoid clinging to the feeling, which would create a dependency on it.

45
00:06:15,000 --> 00:06:22,000
When one clings to positive feelings, one will be inevitably dissatisfied when they are gone.

46
00:06:22,000 --> 00:06:34,000
Once the sensation disappears, one should return to the rising and falling of the abdomen and continue observing it as rising and falling.

47
00:06:34,000 --> 00:06:42,000
In regards to the mind, if thoughts arise during meditation, one should acknowledge them as thinking.

48
00:06:42,000 --> 00:06:49,000
It doesn't matter whether one is thinking about the past or future, or whether one's thoughts are good or bad.

49
00:06:49,000 --> 00:06:59,000
Instead of letting the mind wander and lose track of reality, bring the mind back to the reality with the thought, thinking.

50
00:06:59,000 --> 00:07:04,000
Then return to the rising and falling and continue to practice as normal.

51
00:07:04,000 --> 00:07:16,000
In regards to the dhammas, when the mind gives rise to liking, pleased by a certain experience, create the clear thought, liking, liking.

52
00:07:16,000 --> 00:07:33,000
When disliking arises, anger, boredom, frustration, etc., create the clear thought, disliking, disliking, angry, angry, bored, bored.

53
00:07:33,000 --> 00:07:38,000
Or frustrated, frustrated.

54
00:07:38,000 --> 00:07:48,000
When laziness or drowsiness comes up, create the clear thought, lazy, lazy, or drowsy, drowsy.

55
00:07:48,000 --> 00:07:57,000
When distraction or worry arise, distracted, distracted, worried, worried.

56
00:07:57,000 --> 00:08:07,000
When doubt or confusion arise, doubting, doubting, or confused, confused, and so on.

57
00:08:07,000 --> 00:08:18,000
Once the above hindrances subside, bring the mind back again to a clear awareness of the present moment by focusing on the rise and fall of the abdomen.

58
00:08:18,000 --> 00:08:28,000
Formal meditation practice has many benefits.

59
00:08:28,000 --> 00:08:35,000
The first being that the mind will become more happy and peaceful as a result.

60
00:08:35,000 --> 00:08:47,000
By cultivating the habit of clear awareness of reality, the mind will become happier, lighter, and more free from stress and suffering that come from judgment and clinging.

61
00:08:47,000 --> 00:08:59,000
Most meditators will experience states of bliss and happiness after a few days of meditating if they are diligent and systematic in their practice.

62
00:08:59,000 --> 00:09:10,000
It is important, of course, to recognize that such experiences are simply a fruit of the practice and not the substitute for proper practice itself.

63
00:09:10,000 --> 00:09:23,000
One must acknowledge them as one would any other experience as in happy, happy, or calm, calm.

64
00:09:23,000 --> 00:09:34,000
Nevertheless, such feelings are a true benefit of the practice that one can see for oneself, even after a short time practicing meditation.

65
00:09:34,000 --> 00:09:45,000
The second benefit is that one will begin to understand oneself and the world around in ways that are not possible without meditation practice.

66
00:09:45,000 --> 00:09:59,000
One will come to see clearly how one's own mental habits cause one to suffer, how external stimuli are not really a cause for suffering or happiness until one clings to them.

67
00:09:59,000 --> 00:10:18,000
One comes to see why there is suffering, even while one wishes only for happiness, how objects of desire and aversion are merely ephemeral experiences arising and ceasing incessantly, not worth clinging to or striving for in any way.

68
00:10:18,000 --> 00:10:23,000
Further, one will come to understand the minds of others in the same way.

69
00:10:23,000 --> 00:10:37,000
Without meditation, people tend to immediately judge others based on their actions and speech, giving rise to liking or disliking, attraction or hatred towards them.

70
00:10:37,000 --> 00:10:52,000
Through the practice of meditation, one comes to understand how others are a cause for their own suffering and happiness, and so one is more inclined to forgive and accept others as they are without judging them.

71
00:10:52,000 --> 00:10:59,000
The third benefit of the practice is that one becomes more aware and mindful of the world around.

72
00:10:59,000 --> 00:11:11,000
Without the support of meditation practice, one might go through most of one's day automatically, without being clearly aware of one's own actions, speech and thoughts.

73
00:11:11,000 --> 00:11:19,000
After cultivating meditative awareness, one will become more aware of one's day-to-day experience of reality.

74
00:11:19,000 --> 00:11:40,000
As a result, when difficult situations arise, one will be able to respond to situations with clarity of mind, accepting one's experiences for what they are, instead of falling prey to likes and dislikes, fear, anxiety, confusion, and so on.

75
00:11:40,000 --> 00:11:51,000
One will be able to bear conflict, difficulty, sickness, and even death much better than one would have without the practice of meditation.

76
00:11:51,000 --> 00:12:04,000
The fourth benefit, the true aim of the meditation practice, is that one will be able to rid oneself of the evils in one's own mind that cause suffering for oneself and others.

77
00:12:04,000 --> 00:12:16,000
Anger, greed, delusion, anxiety, worry, stress, fear, arrogance, conceit, and so on.

78
00:12:16,000 --> 00:12:27,000
One will see all mental states that create unhappiness and stress for oneself and others clearly as they are and not discard them as a result.

79
00:12:27,000 --> 00:12:34,000
This is an explanation of basic formal meditation practice and the benefits it brings.

80
00:12:34,000 --> 00:12:45,000
At this point, I would ask that you begin to practice according to this method, at least once, before going on to the next chapter, or back to your daily life.

81
00:12:45,000 --> 00:12:57,000
Practice for five or ten minutes, or however long is convenient, for the first time, right now, before you forget what you have read in this chapter.

82
00:12:57,000 --> 00:13:09,000
Rather than being like a person reading a menu, taste the fruit of the meditation practice for yourself, like one who actually uses the menu toward her meal.

83
00:13:09,000 --> 00:13:19,000
Thank you for your interest in meditation and I sincerely hope that this teaching will bring peace, happiness, and freedom from suffering to your life.

