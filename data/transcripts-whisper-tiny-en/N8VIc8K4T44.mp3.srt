1
00:00:00,000 --> 00:00:05,000
This might be a follow-up on from time when we talk about experiencing

2
00:00:05,000 --> 00:00:10,000
Nirvana directly. But how is this possible

3
00:00:10,000 --> 00:00:15,000
if consciousness ceases during Nirvana?

4
00:00:15,000 --> 00:00:20,000
Well that is an experience, isn't it?

5
00:00:20,000 --> 00:00:25,000
It's an experience to have consciousness suddenly cease on you.

6
00:00:25,000 --> 00:00:28,000
It's all just words.

7
00:00:28,000 --> 00:00:32,000
It doesn't mean much consciousness ceases.

8
00:00:32,000 --> 00:00:35,000
It's like,

9
00:00:35,000 --> 00:00:41,000
suppose you have a machine, think of the body

10
00:00:41,000 --> 00:00:45,000
or the being as a machine that's running 24-7.

11
00:00:45,000 --> 00:00:49,000
And so it's got a lot of problems because of that.

12
00:00:49,000 --> 00:00:54,000
It's heating up, it's melting, it's fraying, or it's wearing away or so on.

13
00:00:54,000 --> 00:00:57,000
That's not saying fraying and where we can say that.

14
00:00:57,000 --> 00:01:02,000
But let's say in general it's overheating.

15
00:01:02,000 --> 00:01:06,000
And it's much more, the human being is much more complicated.

16
00:01:06,000 --> 00:01:11,000
Nirvana is a little bit more subtle than that.

17
00:01:11,000 --> 00:01:18,000
But in general the idea of just turning the machine off for just a short time.

18
00:01:18,000 --> 00:01:22,000
And how that feels when you come back online again?

19
00:01:22,000 --> 00:01:25,000
How it feels to have just gone offline for a bit.

20
00:01:25,000 --> 00:01:32,000
That's how Nirvana is.

21
00:01:32,000 --> 00:01:35,000
During the time that the machine is off, there's no activity.

22
00:01:35,000 --> 00:01:39,000
But you certainly feel it after you come back.

23
00:01:39,000 --> 00:01:43,000
When the mind returns, there's something different.

24
00:01:43,000 --> 00:01:48,000
And you know that you've quote-unquote experienced something

25
00:01:48,000 --> 00:01:53,000
that is beyond explanation,

26
00:01:53,000 --> 00:01:55,000
unless you just want to explain consciousness ceases,

27
00:01:55,000 --> 00:01:57,000
which is kind of spoiling the fun.

28
00:01:57,000 --> 00:01:59,000
So the problem with saying consciousness ceases,

29
00:01:59,000 --> 00:02:05,000
Nirvana consciousness ceases, is that it's true,

30
00:02:05,000 --> 00:02:09,000
but it's so dull, right?

31
00:02:09,000 --> 00:02:13,000
You can't, you can explain Nirvana,

32
00:02:13,000 --> 00:02:17,000
but you can't describe it.

33
00:02:17,000 --> 00:02:22,000
There's no words that you could use to do justice to it.

34
00:02:22,000 --> 00:02:23,000
Let's put it that way.

35
00:02:23,000 --> 00:02:26,000
So no matter you might be able to technically say it's this

36
00:02:26,000 --> 00:02:29,000
or it's that, you can't do justice to it.

37
00:02:29,000 --> 00:02:31,000
And so that's the danger of putting,

38
00:02:31,000 --> 00:02:32,000
trying to put it into words,

39
00:02:32,000 --> 00:02:36,000
not that it's difficult to understand or so on.

40
00:02:36,000 --> 00:02:40,000
It's an experience.

41
00:02:40,000 --> 00:02:44,000
So whether that's accurate or not,

42
00:02:44,000 --> 00:02:47,000
because obviously there's no consciousness

43
00:02:47,000 --> 00:02:52,000
or the consciousness that is experiencing it has ceased,

44
00:02:52,000 --> 00:02:57,000
which is the experience itself, the cessation of consciousness.

45
00:02:57,000 --> 00:03:02,000
But you know, it's actually technically it's not described

46
00:03:02,000 --> 00:03:04,000
as the cessation of consciousness.

47
00:03:04,000 --> 00:03:09,000
There's the technical aspects of it are difficult.

48
00:03:09,000 --> 00:03:15,000
They say that consciousness is still there,

49
00:03:15,000 --> 00:03:19,000
but it's taking Nirvana as an object.

50
00:03:19,000 --> 00:03:22,000
And I don't want to go into those technical aspects.

51
00:03:22,000 --> 00:03:25,000
I like my explanation better, but it's not, it's wrong.

52
00:03:25,000 --> 00:03:31,000
No, the tech, the dogma, the canonical explanation

53
00:03:31,000 --> 00:03:34,000
is that consciousness is still there,

54
00:03:34,000 --> 00:03:38,000
but it's taking Nirvana as an object.

55
00:03:38,000 --> 00:03:40,000
And that just means it's...

56
00:03:40,000 --> 00:03:48,000
Would it be almost like the idea that true Nirvana

57
00:03:48,000 --> 00:03:51,000
is a lack of any...

58
00:03:51,000 --> 00:03:53,000
It's total presence.

59
00:03:53,000 --> 00:03:56,000
In other words, there's no past whatsoever

60
00:03:56,000 --> 00:04:00,000
and there's no future that you're dwelling exactly

61
00:04:00,000 --> 00:04:05,000
just as it were right in the middle of that spit in the middle of the second.

62
00:04:05,000 --> 00:04:11,000
In fact, there is no such thing as time in that respect.

63
00:04:11,000 --> 00:04:13,000
I'm sure.

64
00:04:13,000 --> 00:04:15,000
The future is just simply...

65
00:04:15,000 --> 00:04:20,000
The present is just the future going on its journey to the past.

66
00:04:20,000 --> 00:04:23,000
I mean, it's a split moment.

67
00:04:23,000 --> 00:04:28,000
I kind of like to think of Nirvana in that time that it is the present,

68
00:04:28,000 --> 00:04:31,000
but since it is true presence,

69
00:04:31,000 --> 00:04:36,000
there's no past, no future, there's almost like there is no consciousness.

70
00:04:36,000 --> 00:04:38,000
Well, maybe totally off track today.

71
00:04:38,000 --> 00:04:41,000
Well, the problem is that...

72
00:04:41,000 --> 00:04:47,000
Nirvana, the accurate definition of Nirvana is the cessation of suffering.

73
00:04:47,000 --> 00:04:50,000
And if you're totally in the present moment,

74
00:04:50,000 --> 00:04:56,000
contemplating an object, a conventional object,

75
00:04:56,000 --> 00:04:58,000
there's an experience where you're thinking,

76
00:04:58,000 --> 00:05:00,000
seeing, hearing, smiling, tasting, feeling, thinking,

77
00:05:00,000 --> 00:05:04,000
then they're suffering because those things are by definition dukka.

78
00:05:04,000 --> 00:05:06,000
So, Nirvana has no seat.

79
00:05:06,000 --> 00:05:09,000
There's no seeing, no hearing, no smelling, no tasting, no feeling, no thinking.

80
00:05:09,000 --> 00:05:12,000
And subs...

81
00:05:12,000 --> 00:05:16,000
Because of that, there's no memory of it.

82
00:05:16,000 --> 00:05:22,000
And there is no recognition in it.

83
00:05:22,000 --> 00:05:25,000
All of those things have...

84
00:05:25,000 --> 00:05:27,000
All of those things cease.

85
00:05:27,000 --> 00:05:33,000
So, it's, you know, poetically accurate to say it's true present moment.

86
00:05:33,000 --> 00:05:40,000
I think you have to be careful because there are ways of describing a truly present state,

87
00:05:40,000 --> 00:05:46,000
which you might in fact use to describe the moment before Nirvana,

88
00:05:46,000 --> 00:05:51,000
where one is truly present and truly sees the truth of suffering,

89
00:05:51,000 --> 00:05:54,000
really sees things as they are.

90
00:05:54,000 --> 00:06:00,000
Because the next moment is cessation.

91
00:06:00,000 --> 00:06:02,000
Here's our first question.

92
00:06:02,000 --> 00:06:04,000
Wait, wait, wait, let me stop.

93
00:06:04,000 --> 00:06:31,000
Thank you very much.

