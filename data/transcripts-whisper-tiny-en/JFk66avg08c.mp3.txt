Hi everyone. Another video log update here. A few things, mainly based on my last video update.
First, the idea of the bus tour this year, we've had quite a few people respond from all over the East Coast, which is really neat.
So, I've set up a preliminary sort of, it's a bit rough, but there's a Google map of all the destinations and possible destinations. We're putting in two categories.
So, if you've already expressed some intention to set up some kind of event, like a talk or even a meditation course, in your area that, for us, for me to run,
then I put you on the destination list. So, for sure, we'll try to get to those places that actually do set up such a thing.
For those places where there are people who have just mentioned that they'd like to have us, but are not sure if they could send anything up.
But, you know, if you're passing through kind of thing, I put on the possibilities.
And I may have missed some. It's a little bit disorganized here. There's no computer, so I'm just running this all through a telephone, through a smartphone, actually.
So, hopefully that works. You can check it out. The link will be in the description. You can also follow everything I do either on Facebook or Google Plus or on my web blog.
I'm pretty well connected. So, yeah, that's great. Thanks, everyone, for responding and showing that it actually is something that people would like to see happen.
So, let's see how it goes. It'll be an experiment anyway. And again, there's the website. You can go there and I've already put in some, just some bullet notes of the places that will potentially be going.
And we'll be ending up in Florida near Tampa. So, if you're in that area, there's already some people in that area who have expressed interest in setting something up, and I'll probably be there for a few days, maybe even a week.
Yeah, before heading back. So, yeah, if you're not on a list, and if you have a place that you'd like to see us come, head over to the website, jadica.ceremungalow.org. I'll put that in the description.
And you can let us know if people have already used the website to do that, which shows that it works.
So, that's the first thing. The second thing is the translation of my booklet. We've got ten translations, well, nine translations in the original English.
So, I've got ten languages that we're going to put together as a set and offer to my teacher.
Most of the people who have offered to translate are still underway and haven't been able to get it done in time, which is fine.
Actually, I'd rather have a good translation and a quick translation. I think we all would.
So, the Thai translation also, I'm going to have to go through it because it's probably because it's the only one I actually understand.
And so, I feel the need to correct a lot of things that's probably going to take some time for me to get the time to do that.
But we have a new Russian translation and a Dutch translation, apart from the original eight languages.
They're ten and hopefully I'll get a picture of that or maybe even a video to show you of the box set that we'll be offering to my teacher.
And, of course, you can download any of the ten languages from the website. I'll put that link in this video as well.
In case you're just hearing about it now. Again, if there's a language that isn't on that webpage as potentially under translation,
and you're ready to translate, ready to start translating into that language, please do let us know.
And send me a note and I'll add you to the list and then you can send us the doc file or ODT file or something that we can edit and turn into a PDF and an EPUB to put on the website.
So, that's great as well. It's been amazing to see how many languages, probably by the end we figure we'll get about 20 languages in total.
It looks pretty good to get another ten at least. So, that's really neat to think of all those different languages.
Another thing, the third thing that I wanted to talk about related to this is I'm planning to write a second book.
Some people were asking about some teachings that were a little more advanced because obviously this booklet is just for beginners.
So, the second, the sequel to the book would be sort of a map of the paths.
I try to make it fairly general because again I don't want to go into great detail about those things in a meditator experiences,
because without the active participation with a teacher, it can be not dangerous, at least, can lead you astray.
And I don't want to lead people to think that a book can somehow take the place of a teacher.
So, it'll be general, but I'll try to go through all the different stages and the concepts and principles that you have to keep in mind during the practice.
Once you've begun to undertake the practice either intensively or day to day over months or years for those people who haven't had a chance to undertake an intensive course.
So, look for that. Hopefully I'll have a couple of chapters up this year. Again, I'm studying this year, trying to get these poly exams that my teacher asked me to study for.
So, be a little bit busy, but hopefully all these different projects will, some of them will actually come to fruit. I guess I can't say they all will, but that's hopefully at least some of it comes to fruit.
Again, thanks everyone for the great support and response that you've given to all of these projects. Without that, none of this would happen.
So, please keep up the appreciation, at least, let us know that you're using and taking part and benefiting from all of this stuff, just so I do keep doing it.
And, show your support. If you want to support our organization, you're welcome to do that via our website or at least just let us know that you're out there and following along.
Anyway, you can see that you are. It's a great thing to see. We've got millions of people or millions of views on YouTube and thousands of people following these videos, so it's quite encouraging.
So, I'm wishing you all the best and please do keep it up, keep practicing, keep on the path and let's find peace, happiness and freedom from suffering together.
Thanks, peace for all the best.
