1
00:00:00,000 --> 00:00:02,160
You're starting.

2
00:00:02,160 --> 00:00:07,680
Peter asked, what is the formal way to greet a monk in Sri Lanka?

3
00:00:07,680 --> 00:00:10,080
And is it different from other countries?

4
00:00:10,080 --> 00:00:11,920
Is high-pantay acceptable?

5
00:00:18,400 --> 00:00:23,600
I think high-pantay you can say, when you're familiar,

6
00:00:23,600 --> 00:00:35,920
I would possibly call say, high-pantay if I meet Bantay utadamu somewhere here in the monastery.

7
00:00:38,480 --> 00:00:45,120
If it's an official occasion or a monk, you don't know, don't say,

8
00:00:45,120 --> 00:01:03,680
high-pantay, would say, rather, say, Namaskar kan, Bantay, or that's Thai, you know?

9
00:01:03,680 --> 00:01:05,680
No, isn't it?

10
00:01:07,360 --> 00:01:09,360
Okay, then don't say that.

11
00:01:09,360 --> 00:01:12,960
I corruption of you are Namaskar, Namaskar.

12
00:01:12,960 --> 00:01:17,200
It's going to be Namastain, Namastay, Bantay, that would be.

13
00:01:17,200 --> 00:01:20,240
But Namastay is also not going to be because that's Sanskrit.

14
00:01:23,440 --> 00:01:26,560
I mean, sorry to jump in, but yeah, that's fine.

15
00:01:26,560 --> 00:01:32,880
If would you say, high-father to a priest or high-brother to a Christian monk,

16
00:01:32,880 --> 00:01:38,000
I don't think people normally would if they have respect for the person they would

17
00:01:38,000 --> 00:01:43,120
I would think at least say, hello, father or something like that. Because, I mean, Bantay is not

18
00:01:43,120 --> 00:01:48,480
a nuclei, it's a title of respect. It's like, high-ventourable, I guess you could say.

19
00:01:50,960 --> 00:01:55,360
It's, you know, I mean, it gets ridiculous if you like, say,

20
00:01:55,360 --> 00:01:58,640
higher majesty or something like that. You don't really,

21
00:01:58,640 --> 00:02:05,040
if you're a king or something, but it's not on that level, but because of the fact that Bantay

22
00:02:05,040 --> 00:02:11,680
is a kind of respect, I think, high-bantay is a little bit too informal.

23
00:02:13,360 --> 00:02:18,800
It differs from country to country as we experienced just now.

24
00:02:20,480 --> 00:02:28,160
In Thailand, you would say, Namaskar, Arjan or Namaskar, Bradjan,

25
00:02:28,160 --> 00:02:42,960
and you would, in most countries, I think, put your hands together and don't look straight into

26
00:02:42,960 --> 00:02:49,760
the face in the beginning, later in the conversation, yes, but in the beginning, have the head a little

27
00:02:49,760 --> 00:03:00,400
bit lower or even a little bit bent or so, if it's an important monk, the more important the monk,

28
00:03:00,400 --> 00:03:05,120
the deeper the bow and things like that.

29
00:03:06,960 --> 00:03:12,480
Yeah, I mean, here in Sri Lanka, they touch their hands to your feet. If people would do that when

30
00:03:12,480 --> 00:03:18,080
they respect you, and we do that when we respect the senior monk, we touch our head, actually,

31
00:03:18,080 --> 00:03:25,200
to the feet of the elder. When people do that for their parents, or I learned here in Sri Lanka

32
00:03:25,200 --> 00:03:36,160
from the bikones who have stayed in Sri Lanka before me, that we do, like, people do to the

33
00:03:36,720 --> 00:03:48,000
Queen of England, I don't know how that is called, but probably, and with the palms to

34
00:03:48,000 --> 00:03:58,720
together, so that would the appropriate greeting for a bikone to greet a higher monk.

35
00:03:59,760 --> 00:04:04,880
We were talking about the word Bhante because I'm still pretty adamant that it's masculine,

36
00:04:05,440 --> 00:04:12,640
but that's really a grammatical question. They'll often say, I, I, I, or you could say,

37
00:04:12,640 --> 00:04:18,720
Bhadeh, I think, but no one would have, no one uses it, so, but I think you could, looking at,

38
00:04:18,720 --> 00:04:24,720
because it is used, there's two, two angels talking together, and once it calls the other Bhante,

39
00:04:24,720 --> 00:04:30,160
the female calls the male Bhante, and the male turns around and calls the female Bhante,

40
00:04:30,160 --> 00:04:35,200
which they're actually pretty much the same word, just once masculine, once feminine.

41
00:04:35,200 --> 00:04:42,240
It's not like that's so clear. They are not asking for.

42
00:04:42,240 --> 00:04:58,720
Hmm. Okay.

