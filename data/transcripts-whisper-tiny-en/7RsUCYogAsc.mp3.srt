1
00:00:00,000 --> 00:00:07,000
What is the difference between lucid dream and reality?

2
00:00:07,000 --> 00:00:10,000
Not much, I suppose.

3
00:00:10,000 --> 00:00:13,000
I've never been much into lucid dreaming,

4
00:00:13,000 --> 00:00:15,000
and I have heard a lot about it.

5
00:00:15,000 --> 00:00:18,000
So, I would understand it from a Buddhist point of view.

6
00:00:18,000 --> 00:00:27,000
It's the brain giving rise to stimulus without

7
00:00:27,000 --> 00:00:32,000
resorting to the organs of the body to provide the stimulus.

8
00:00:32,000 --> 00:00:36,000
So, it could still be an object of insight.

9
00:00:36,000 --> 00:00:43,000
It could still be the framework within to develop insight.

10
00:00:43,000 --> 00:00:47,000
Now, the problem that we might have is that the dream state

11
00:00:47,000 --> 00:00:54,000
is still a altered state.

12
00:00:54,000 --> 00:00:59,000
It's not a pure or a purely clear state.

13
00:00:59,000 --> 00:01:03,000
And, as I said, a lucid dreaming is not something that I've gotten into.

14
00:01:03,000 --> 00:01:06,000
So, I can't say exactly what's going on,

15
00:01:06,000 --> 00:01:10,000
but if it's simply the case where the brain is producing the stimulus,

16
00:01:10,000 --> 00:01:14,000
and the mind is in the A clear and lucid state,

17
00:01:14,000 --> 00:01:19,000
then it's still possible to cultivate meditation based on it.

18
00:01:19,000 --> 00:01:23,000
The direct answer to your question is that there is no difference.

19
00:01:23,000 --> 00:01:27,000
Reality is reality, dreams of any sort are a part of reality.

20
00:01:27,000 --> 00:01:28,000
The problem is not that.

21
00:01:28,000 --> 00:01:32,000
The problem with them are the difference with them is the state of mind.

22
00:01:32,000 --> 00:01:36,000
That is, that is reacting or interacting with them.

23
00:01:36,000 --> 00:01:40,000
Because an ordinary dream, the mind is in a befuddled state.

24
00:01:40,000 --> 00:01:44,000
It's in a state that is bereft of mindfulness or devoid of mindfulness.

25
00:01:44,000 --> 00:01:48,000
So, it's unable to grasp the object,

26
00:01:48,000 --> 00:01:50,000
unable to see clearly what's going on,

27
00:01:50,000 --> 00:01:56,000
and winds up getting caught up in concepts rather than experiencing ultimate reality.

