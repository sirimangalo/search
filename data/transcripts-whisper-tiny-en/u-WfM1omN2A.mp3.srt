1
00:00:00,000 --> 00:00:06,500
Would it be foolish for me to make a formal determination to become a Sotapana by the end of this lifetime?

2
00:00:06,500 --> 00:00:11,000
I heard there can be side effects to these kind of determinations.

3
00:00:13,000 --> 00:00:21,000
Yeah, there can be. There's the concern and the worry and even the guilt.

4
00:00:21,000 --> 00:00:27,000
Suppose then when you die, you know that you're not a Sotapana.

5
00:00:27,000 --> 00:00:30,000
How would that affect your dying moment?

6
00:00:30,000 --> 00:00:36,000
Feeling guilty that you didn't fulfill your vow.

7
00:00:36,000 --> 00:00:44,000
So putting placing time limits on any determination is quite risky.

8
00:00:44,000 --> 00:00:47,000
You don't see it. You don't see it.

9
00:00:47,000 --> 00:00:59,000
I can't think of one instance where it would be found in the Topitika.

10
00:00:59,000 --> 00:01:11,000
However, on the other hand, we do encourage in some cases for advanced meditators to train this sort of determination.

11
00:01:11,000 --> 00:01:19,000
In a simple sense, train themselves to enter into a certain stage of knowledge within one hour.

12
00:01:19,000 --> 00:01:24,000
So you're not demanding that it come. You're testing this ability.

13
00:01:24,000 --> 00:01:29,000
You're training this ability. You're saying within this hour, may I attain this stage of knowledge?

14
00:01:29,000 --> 00:01:38,000
And then you practice. And if it comes up, it encourages, it enforces this connection between the determination and the result.

15
00:01:38,000 --> 00:01:44,000
And it develops what is called the Additan upon me, the perfection of determination.

16
00:01:44,000 --> 00:01:56,000
This is a useful trait, the ability to make a determination and have it come true or to be able to follow through with it, to meet with that result.

17
00:01:56,000 --> 00:02:04,000
So that kind of thing, for example, it would help you to make such a vow to become a Sotapana.

18
00:02:04,000 --> 00:02:11,000
Once you get better at making determinations, determining this, determining that, may I.

19
00:02:11,000 --> 00:02:15,000
I mean, that might be a one suggestion is to make smaller determinations.

20
00:02:15,000 --> 00:02:32,000
Say within this week, may I practice so many hours, or for example, or with this year, may I go to five meditation courses, ten meditation, something like that.

21
00:02:32,000 --> 00:02:42,000
Training yourself and encouraging yourself because then it becomes something doable and not something risky.

22
00:02:42,000 --> 00:02:51,000
Because the risk in just making the formal vow now is that if you're not well trained in determination,

23
00:02:51,000 --> 00:02:58,000
what that really means is that if you don't have your life set up to get to the goal,

24
00:02:58,000 --> 00:03:02,000
it can have a deleterious effect.

25
00:03:02,000 --> 00:03:11,000
It can be problematic when you die because, as I said, you can feel guilty about it.

26
00:03:11,000 --> 00:03:19,000
But you can certainly cultivate or develop that yourself up to that level.

27
00:03:19,000 --> 00:03:26,000
But yeah, I think making such long-range determinations and putting a fine point on the end is dangerous,

28
00:03:26,000 --> 00:03:30,000
you know, how easy it's going to be for you to become a Sotabana.

29
00:03:30,000 --> 00:03:35,000
I would know such determinations as that I would put aside.

30
00:03:35,000 --> 00:03:39,000
The determination that you should make is to become an Arahat.

31
00:03:39,000 --> 00:03:41,000
May I become an Arahat.

32
00:03:41,000 --> 00:03:45,000
And if you keep that in mind, it puts you on the right track.

33
00:03:45,000 --> 00:03:50,000
It's not a time limit thing because the Sotabana has to become an Arahat in the end.

34
00:03:50,000 --> 00:03:52,000
They're on the way.

35
00:03:52,000 --> 00:03:57,000
And so focusing, and this is another thing that I would say,

36
00:03:57,000 --> 00:04:00,000
is that many people put far too much focus on Sotabana.

37
00:04:00,000 --> 00:04:03,000
You should focus on becoming an Arahat.

38
00:04:03,000 --> 00:04:10,000
And, you know, the kind of the idea being that when you set your site so high,

39
00:04:10,000 --> 00:04:15,000
you're no longer looking way, way up at becoming a Sotabana.

40
00:04:15,000 --> 00:04:21,000
And you might find that suddenly you're a Sotabana, you know, not in a week or a month or something,

41
00:04:21,000 --> 00:04:30,000
but you might find that you had one stage become a Sotabana without really expecting it.

42
00:04:30,000 --> 00:04:34,000
Because your sites were set so high, the Arahat is one without any defilement.

43
00:04:34,000 --> 00:04:39,000
So if you set yourself on that, at some point along the path, you have to become a Sotabana.

44
00:04:39,000 --> 00:04:45,000
At some point along the path, you'll become free from the first three fetters.

45
00:04:45,000 --> 00:04:56,000
So those kind of things we don't put, I mean, I'm sure you're aware that in general people don't put time limits on them.

46
00:04:56,000 --> 00:04:59,000
I wouldn't encourage it for simply for that reason.

47
00:04:59,000 --> 00:05:04,000
And I would encourage much more to make a general determination,

48
00:05:04,000 --> 00:05:08,000
repeated determination to become an Arahat, to become enlightened.

49
00:05:08,000 --> 00:05:12,000
If that's your goal now, if your goal is to become a Buddha, which obviously it's not,

50
00:05:12,000 --> 00:05:15,000
then you would make that determination.

51
00:05:15,000 --> 00:05:19,000
If your goal is to meet Maitaya, Buddha, the next Buddha who's coming,

52
00:05:19,000 --> 00:05:23,000
then make a determination for that and so on.

53
00:05:23,000 --> 00:05:31,000
But making determinations for certain attainments can be done, can be made,

54
00:05:31,000 --> 00:05:41,000
but much better to start small and to make determinations in terms of the practice

55
00:05:41,000 --> 00:05:45,000
that you're engaged in.

56
00:05:45,000 --> 00:05:51,000
Because in meditation practice, people will make determinations

57
00:05:51,000 --> 00:05:57,000
that seem to lead them to states, such as becoming a Sotabana,

58
00:05:57,000 --> 00:06:03,000
but they would do it during an intensive meditation course.

59
00:06:03,000 --> 00:06:06,000
So they make a determination within this hour,

60
00:06:06,000 --> 00:06:11,000
but my experience, Palasamapati, which is the attainment of cessation,

61
00:06:11,000 --> 00:06:14,000
which is the Sotabana.

62
00:06:14,000 --> 00:06:17,000
And they may not get it, and then they try again, and they try again,

63
00:06:17,000 --> 00:06:20,000
and they do it on an hourly basis, you see.

64
00:06:20,000 --> 00:06:23,000
So it no longer becomes such a big deal.

65
00:06:23,000 --> 00:06:27,000
It's just an attempt that they're making, and they get better at it,

66
00:06:27,000 --> 00:06:33,000
and eventually they do or seem to attain Palasamapati, which makes them a Sotabana.

67
00:06:33,000 --> 00:06:36,000
That's more how it goes.

68
00:06:36,000 --> 00:06:43,000
Just making a vague and general determination for the end of life thing is,

69
00:06:43,000 --> 00:06:48,000
I don't see it as all that beneficial and impotentially dangerous,

70
00:06:48,000 --> 00:07:12,000
but not immoral by any means.

