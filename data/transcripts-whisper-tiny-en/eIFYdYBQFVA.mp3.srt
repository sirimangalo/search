1
00:00:00,000 --> 00:00:05,000
Do meditators still push the limits of the body and mind to become enlightened?

2
00:00:05,000 --> 00:00:09,000
For example, the story you told about the monk who stuck his finger in a fire,

3
00:00:09,000 --> 00:00:14,000
or Zen monks who stand under cold waterfalls to strengthen the mind?

4
00:00:14,000 --> 00:00:23,000
Yes, many meditators still do that, and they push the limits

5
00:00:23,000 --> 00:00:30,000
and think that might help to become enlightened.

6
00:00:30,000 --> 00:00:37,000
I would say to push the limits a little bit might be very helpful,

7
00:00:37,000 --> 00:00:45,000
but it is for an unexperienced person very hard to see the limit

8
00:00:45,000 --> 00:00:51,000
and when to see the point when you push to the limit

9
00:00:51,000 --> 00:00:55,000
or when you push over it, and when you push over it,

10
00:00:55,000 --> 00:01:02,000
it can become very dangerous and stupid, even like stuck in the finger in a fire

11
00:01:02,000 --> 00:01:07,000
is not helpful to become enlightened.

12
00:01:07,000 --> 00:01:13,000
It might be to have good physical strength.

13
00:01:13,000 --> 00:01:18,000
It might be very helpful to stand under cold waterfalls,

14
00:01:18,000 --> 00:01:22,000
and that strengthens mind and body,

15
00:01:22,000 --> 00:01:28,000
but it is not helpful for becoming enlightened.

16
00:01:28,000 --> 00:01:31,000
It might help with your concentration,

17
00:01:31,000 --> 00:01:34,000
it might strengthen your health as I said,

18
00:01:34,000 --> 00:01:38,000
but that has nothing to do with enlightenment.

19
00:01:38,000 --> 00:01:45,000
That can very easily make your ego very strong,

20
00:01:45,000 --> 00:01:50,000
and I know some of the people who did such things.

21
00:01:50,000 --> 00:01:58,000
I met some Mahayana nuns who made such things.

22
00:01:58,000 --> 00:02:04,000
One who has been burning her arm,

23
00:02:04,000 --> 00:02:09,000
another had a cold, had put a cold on her head,

24
00:02:09,000 --> 00:02:11,000
and almost died.

25
00:02:11,000 --> 00:02:17,000
She passed out, passed out, and fell over,

26
00:02:17,000 --> 00:02:21,000
so that the cold fell off and that saved her life.

27
00:02:21,000 --> 00:02:27,000
Otherwise, that would have been the end of her life.

28
00:02:27,000 --> 00:02:30,000
The end of her striving for enlightenment,

29
00:02:30,000 --> 00:02:36,000
so that is what can happen when you push over the limits.

30
00:02:36,000 --> 00:02:40,000
But when you are on retreat,

31
00:02:40,000 --> 00:02:44,000
and you are not going to the limits,

32
00:02:44,000 --> 00:02:51,000
and when you not really push them with the help of an experienced teacher,

33
00:02:51,000 --> 00:02:57,000
then you will not have the results that you can have

34
00:02:57,000 --> 00:02:59,000
when you push them a little bit,

35
00:02:59,000 --> 00:03:06,000
but it is very important to have the guidance of a teacher,

36
00:03:06,000 --> 00:03:08,000
because as I mentioned before,

37
00:03:08,000 --> 00:03:11,000
you might not know where the limits are.

38
00:03:11,000 --> 00:03:20,000
You might think that a fasting starving might be a good thing

39
00:03:20,000 --> 00:03:23,000
to help you to become enlightened,

40
00:03:23,000 --> 00:03:28,000
or that to stay up all night before the teacher tells you

41
00:03:28,000 --> 00:03:30,000
might be a good thing,

42
00:03:30,000 --> 00:03:32,000
but it is not.

43
00:03:32,000 --> 00:03:36,000
It will ruin your meditation practice,

44
00:03:36,000 --> 00:03:39,000
because it has more to do with your ego

45
00:03:39,000 --> 00:03:42,000
and with your defilements

46
00:03:42,000 --> 00:03:54,000
and with actually the way of letting go and being

47
00:03:54,000 --> 00:03:58,000
in the present moment

48
00:03:58,000 --> 00:04:02,000
and doing what needs to be done in the present moment

49
00:04:02,000 --> 00:04:05,000
in order to become enlightened.

50
00:04:05,000 --> 00:04:10,000
So it is not easy to say yes and no to this question,

51
00:04:10,000 --> 00:04:16,000
but I said all I have to say.

52
00:04:16,000 --> 00:04:20,000
Yeah, it makes me think of Bloom Pajodok,

53
00:04:20,000 --> 00:04:24,000
what you are saying makes me think of Bloom Pajodok,

54
00:04:24,000 --> 00:04:26,000
said we don't have...

55
00:04:26,000 --> 00:04:29,000
We can't see our own eyes,

56
00:04:29,000 --> 00:04:32,000
and this is because why can't you see your own eyes

57
00:04:32,000 --> 00:04:34,000
because you don't have a mirror?

58
00:04:34,000 --> 00:04:38,000
And they said it in the same way we can't see our own defilements

59
00:04:38,000 --> 00:04:41,000
because we don't have the mirror of wisdom.

60
00:04:41,000 --> 00:04:43,000
So really it's a good point.

61
00:04:43,000 --> 00:04:46,000
It's not exactly what was being asked,

62
00:04:46,000 --> 00:04:53,000
but why pushing oneself should really best be done under a qualified teacher?

63
00:04:53,000 --> 00:04:58,000
It's a very good point as to how to deal with pushing yourself.

64
00:04:58,000 --> 00:05:02,000
I agree it should have to do with the teacher.

65
00:05:02,000 --> 00:05:12,000
But I think it's also because the state of enlightenment

66
00:05:12,000 --> 00:05:14,000
shouldn't be any kind of striving.

67
00:05:14,000 --> 00:05:18,000
The state of enlightenment is the perfect natural state.

68
00:05:18,000 --> 00:05:21,000
So an Arahant is in the natural state.

69
00:05:21,000 --> 00:05:24,000
The rest of us are all mixed up.

70
00:05:24,000 --> 00:05:26,000
Some of us push ourselves too hard in things.

71
00:05:26,000 --> 00:05:28,000
Some of us are too lazy in things.

72
00:05:28,000 --> 00:05:30,000
Some of us are on the wrong path.

73
00:05:30,000 --> 00:05:33,000
Some of us are on the right path or on a good path.

74
00:05:33,000 --> 00:05:35,000
Some of us are on a bad path.

75
00:05:35,000 --> 00:05:37,000
We have all different kinds.

76
00:05:37,000 --> 00:05:40,000
And most of us are none of the above and all of the above.

77
00:05:40,000 --> 00:05:41,000
We're all mixed up.

78
00:05:41,000 --> 00:05:43,000
Sometimes we push ourselves too hard.

79
00:05:43,000 --> 00:05:44,000
Sometimes we're too lazy.

80
00:05:44,000 --> 00:05:46,000
Sometimes we get on a good path.

81
00:05:46,000 --> 00:05:48,000
Sometimes we get on a bad path.

82
00:05:48,000 --> 00:05:51,000
Sometimes we go nowhere and just run around in circles.

83
00:05:51,000 --> 00:05:54,000
Sometimes we just stand there and look left and look right

84
00:05:54,000 --> 00:05:56,000
and don't know where to go.

85
00:05:56,000 --> 00:05:58,000
This is what the state that we're in.

86
00:05:58,000 --> 00:06:00,000
An Arahant doesn't have any of this.

87
00:06:00,000 --> 00:06:02,000
They're not pushing themselves in any way.

88
00:06:02,000 --> 00:06:06,000
When they practice, it's natural.

89
00:06:06,000 --> 00:06:10,000
It's effortless, I would say.

90
00:06:10,000 --> 00:06:13,000
This is how I think we should understand the state of the Arahant

91
00:06:13,000 --> 00:06:15,000
that it's a natural state.

92
00:06:15,000 --> 00:06:23,000
So pushing yourself means to affect some kind of a change.

93
00:06:23,000 --> 00:06:27,000
We're trying to get ourselves back to a state of equilibrium.

94
00:06:27,000 --> 00:06:30,000
Where we're simply aware of things, where we're aware of things

95
00:06:30,000 --> 00:06:37,000
as they arise without any kind of partiality.

96
00:06:37,000 --> 00:06:40,000
But the pushing yourself is the effort to do that.

97
00:06:40,000 --> 00:06:43,000
It's not the effort to go to this extreme or that extreme.

98
00:06:43,000 --> 00:06:49,000
And so the teacher's job is when you push yourself too hard to pull you back.

99
00:06:49,000 --> 00:06:52,000
So when a person is pushing themselves too hard,

100
00:06:52,000 --> 00:06:57,000
you either warn them in cases that they're torturing themselves.

101
00:06:57,000 --> 00:07:00,000
You have to tell them not to do that.

102
00:07:00,000 --> 00:07:04,000
In cases that they're getting excited about something,

103
00:07:04,000 --> 00:07:07,000
they're pushing themselves because they're really getting excited about something.

104
00:07:07,000 --> 00:07:11,000
It's the teacher's job to get bored and to be uninterested.

105
00:07:11,000 --> 00:07:14,000
To show them that that is not something interested.

106
00:07:14,000 --> 00:07:16,000
So the meditators come and say, I saw this, I saw that.

107
00:07:16,000 --> 00:07:19,000
And then, oh yeah, did you acknowledge and remind him

108
00:07:19,000 --> 00:07:22,000
that this is not something special, this is not the goal?

109
00:07:22,000 --> 00:07:25,000
When meditators are too lazy or not pushing hard enough,

110
00:07:25,000 --> 00:07:28,000
it's the teacher's job to say, today do more meditation

111
00:07:28,000 --> 00:07:32,000
or today I give you a new exercise and to push them on.

112
00:07:32,000 --> 00:07:37,000
When a meditator is struggling, it's the teacher's job to encourage them.

113
00:07:37,000 --> 00:07:42,000
All of these things are because we can't see our own defilements.

114
00:07:42,000 --> 00:07:46,000
We can't see what we're lacking.

115
00:07:46,000 --> 00:07:50,000
When you push yourself, you're pushing yourself based on your current condition.

116
00:07:50,000 --> 00:07:54,000
So if you have intentioned, if you have excitement about something,

117
00:07:54,000 --> 00:07:58,000
you'll decide that you have to push yourself to be more excited about it.

118
00:07:58,000 --> 00:08:03,000
Or if you're stressing about something, you'll think, yes, I have to push myself harder.

119
00:08:03,000 --> 00:08:05,000
Because that's the stress that you have.

120
00:08:05,000 --> 00:08:10,000
It's very difficult without wisdom, without the wisdom and the experience

121
00:08:10,000 --> 00:08:13,000
of having gone through the practice on your own.

122
00:08:13,000 --> 00:08:17,000
To catch yourself and say, no, no, I have to do the opposite of what I want to do.

123
00:08:17,000 --> 00:08:19,000
Because I'm out of balance.

124
00:08:19,000 --> 00:08:24,000
So people who push themselves is generally in that category.

125
00:08:24,000 --> 00:08:28,000
And it's as Panyani said, I agree it's often associated with ego.

126
00:08:28,000 --> 00:08:38,000
And the person might not be egotistical, but it's associated with an idea or a concept or a view

127
00:08:38,000 --> 00:08:41,000
that some segment of reality is better.

128
00:08:41,000 --> 00:08:43,000
No, I should torture myself.

129
00:08:43,000 --> 00:08:46,000
If I go to this extremely good and lose a sight of the balance,

130
00:08:46,000 --> 00:08:49,000
the state of balance, which is simply aware of things as they are.

131
00:08:49,000 --> 00:08:54,000
I mean, when you stand on a waterfall, you should be aware of standing under a waterfall.

132
00:08:54,000 --> 00:08:58,000
When you stick your finger in a fire, you should be aware of sticking your finger in the fire.

133
00:08:58,000 --> 00:09:03,000
The act of doing it has nothing to do with being enlightened.

134
00:09:03,000 --> 00:09:06,000
It has no reference, no relation.

135
00:09:06,000 --> 00:09:09,000
Even if it's something that makes your mind more powerful,

136
00:09:09,000 --> 00:09:12,000
if it's something that makes your body more healthy,

137
00:09:12,000 --> 00:09:24,000
if it's something that tests your mind or tests your endurance or your ability to bear,

138
00:09:24,000 --> 00:09:31,000
certain experiences, all of these can actually be hindrances to the practice.

139
00:09:31,000 --> 00:09:35,000
A person who has a strong mind can become attached to having a strong mind.

140
00:09:35,000 --> 00:09:38,000
A person who has a strong body can, of course, become attached to having a strong body.

141
00:09:38,000 --> 00:09:41,000
The story about the monk who stuck his finger in the fire is,

142
00:09:41,000 --> 00:09:43,000
I think, a little bit different.

143
00:09:43,000 --> 00:09:46,000
The reason why he did that was not to test himself.

144
00:09:46,000 --> 00:09:50,000
He wanted to offer his finger to the Buddha,

145
00:09:50,000 --> 00:09:59,000
or not exactly to the Buddha, to as an offering to the dharma kind of

146
00:09:59,000 --> 00:10:02,000
in his determination to become a Buddha.

147
00:10:02,000 --> 00:10:04,000
So he didn't stick his finger in the fire.

148
00:10:04,000 --> 00:10:08,000
He burnt his finger to the stump.

149
00:10:08,000 --> 00:10:10,000
I don't know which finger, maybe the baby finger,

150
00:10:10,000 --> 00:10:12,000
hopefully not one he uses all the time.

151
00:10:12,000 --> 00:10:15,000
But he burnt it to the stump, so he has no finger left.

152
00:10:15,000 --> 00:10:17,000
That's what they said.

153
00:10:17,000 --> 00:10:20,000
And it was a sacrifice that he made.

154
00:10:20,000 --> 00:10:23,000
So it wasn't exactly about pushing himself.

155
00:10:23,000 --> 00:10:29,000
It was some idea that some people might say was extreme or crazy,

156
00:10:29,000 --> 00:10:34,000
but was a part of his determination,

157
00:10:34,000 --> 00:10:38,000
making a sincere determination to become a Buddha.

158
00:10:38,000 --> 00:10:41,000
Honestly, my teacher, when he told this story,

159
00:10:41,000 --> 00:10:44,000
he had just gotten back from going around the world,

160
00:10:44,000 --> 00:10:47,000
and it was in front of all of the laypeople from the village

161
00:10:47,000 --> 00:10:50,000
that we were in in Jamthang.

162
00:10:50,000 --> 00:10:53,000
And he said, I don't think anybody here could do that.

163
00:10:53,000 --> 00:10:56,000
I think he'd all pass out before he got.

164
00:10:56,000 --> 00:10:59,000
He said, you people wouldn't.

165
00:10:59,000 --> 00:11:03,000
He said, nobody here would be able to do such a thing.

166
00:11:03,000 --> 00:11:09,000
He said, I think we wouldn't be able to stand such a thing.

167
00:11:09,000 --> 00:11:12,000
He was laughing about it.

168
00:11:12,000 --> 00:11:19,000
So it is somehow to be esteemed in one sense,

169
00:11:19,000 --> 00:11:25,000
or to be appreciated that this monk was able to have that much determination.

170
00:11:25,000 --> 00:11:29,000
Because the point being that you need that kind of determination

171
00:11:29,000 --> 00:11:32,000
to become a Buddha, you need more than that.

172
00:11:32,000 --> 00:11:36,000
You have to be willing to walk across an ocean of hot embers

173
00:11:36,000 --> 00:11:40,000
or swim through an ocean of razor blades from one end to the other.

174
00:11:40,000 --> 00:11:42,000
If that's what it takes to become a lane.

175
00:11:42,000 --> 00:11:44,000
So that's what that was about.

176
00:11:44,000 --> 00:11:52,000
It was kind of pushing the limits of one's intention to become a Buddha.

177
00:11:52,000 --> 00:11:55,000
There's one thing I want to add.

178
00:11:55,000 --> 00:11:57,000
You mentioned the determination.

179
00:11:57,000 --> 00:12:00,000
And that, of course, is a great determination.

180
00:12:00,000 --> 00:12:03,000
The monk did to become a Buddha.

181
00:12:03,000 --> 00:12:06,000
But when a meditator is on a retreat

182
00:12:06,000 --> 00:12:09,000
and has the goal to become enlightened,

183
00:12:09,000 --> 00:12:14,000
this determination is need kind of.

184
00:12:14,000 --> 00:12:18,000
This determination is needed.

185
00:12:18,000 --> 00:12:26,000
I would say you get out as a result

186
00:12:26,000 --> 00:12:31,000
what you give in when you are really able, willing

187
00:12:31,000 --> 00:12:39,000
to give up your life than you will have good results.

188
00:12:39,000 --> 00:12:43,000
But I don't think that's pushing to the limits.

189
00:12:43,000 --> 00:12:48,000
Although it might seem to some, it is.

190
00:12:48,000 --> 00:12:52,000
But this is more like, I would say,

191
00:12:52,000 --> 00:12:56,000
it's more like letting go of any limits

192
00:12:56,000 --> 00:13:03,000
and letting go of the wanting to live the life as you lived it before.

193
00:13:03,000 --> 00:13:08,000
That's a very, very different important point

194
00:13:08,000 --> 00:13:10,000
when you are on retreat, not so much

195
00:13:10,000 --> 00:13:15,000
when you are meditating alone at home.

196
00:13:15,000 --> 00:13:23,000
And it's kind of the difference between putting up with

197
00:13:23,000 --> 00:13:28,000
deadly pain and bringing it on.

198
00:13:28,000 --> 00:13:35,000
I mean, the giving up one's life should be

199
00:13:35,000 --> 00:13:38,000
for the purpose of meditating or saying,

200
00:13:38,000 --> 00:13:40,000
no matter what comes, I'm not going to react

201
00:13:40,000 --> 00:13:41,000
or I'm going to let go.

202
00:13:41,000 --> 00:13:44,000
I'm going to accept the experience.

203
00:13:44,000 --> 00:13:46,000
Whatever phenomena arise, seeing hearings,

204
00:13:46,000 --> 00:13:48,000
smelling, tasting, feeling, thinking,

205
00:13:48,000 --> 00:13:50,000
I'm going to see them just as seeing hearings.

206
00:13:50,000 --> 00:13:52,000
I'm not going to be partial to them in any way.

207
00:13:52,000 --> 00:13:55,000
And that includes death, that includes dying.

208
00:13:55,000 --> 00:13:58,000
So we were doing, that was the test that night

209
00:13:58,000 --> 00:14:02,000
to do walking meditation and sitting meditation in the rain.

210
00:14:02,000 --> 00:14:06,000
And we managed to do 45 minutes of walking in heavy rain.

211
00:14:06,000 --> 00:14:09,000
And even the lightning struck.

212
00:14:09,000 --> 00:14:11,000
So the lightning struck just three,

213
00:14:11,000 --> 00:14:12,000
you know, a few miles away.

214
00:14:12,000 --> 00:14:13,000
It was quite close.

215
00:14:13,000 --> 00:14:15,000
And so immediately this thought arose,

216
00:14:15,000 --> 00:14:17,000
wow, I could really die here.

217
00:14:17,000 --> 00:14:19,000
I mean, this is no longer an intellectual exercise.

218
00:14:19,000 --> 00:14:21,000
So we're doing walking back and forth.

219
00:14:21,000 --> 00:14:23,000
And thinking like this, like,

220
00:14:23,000 --> 00:14:25,000
this could be my last moment.

221
00:14:25,000 --> 00:14:29,000
I mean, obviously it's a little bit hyperbolic

222
00:14:29,000 --> 00:14:32,000
because, you know, but lightning has struck

223
00:14:32,000 --> 00:14:33,000
very close to our monastery.

224
00:14:33,000 --> 00:14:36,000
We felt the lightning in our feet.

225
00:14:36,000 --> 00:14:38,000
It's blown out the lights and so on.

226
00:14:38,000 --> 00:14:41,000
So it is hitting all around us and it could hit.

227
00:14:41,000 --> 00:14:44,000
But this feeling arose and suddenly,

228
00:14:44,000 --> 00:14:45,000
you know, this is really it.

229
00:14:45,000 --> 00:14:49,000
And so we just sat in the mind

230
00:14:49,000 --> 00:14:51,000
that we're doing this.

231
00:14:51,000 --> 00:14:53,000
We're going to take whatever comes.

232
00:14:53,000 --> 00:14:57,000
Even to that extent, we're not going to project

233
00:14:57,000 --> 00:15:00,000
anything on this experience of knowing,

234
00:15:00,000 --> 00:15:03,000
you know, there's thoughts that know that you could die.

235
00:15:03,000 --> 00:15:05,000
We're going to take those as just thoughts.

236
00:15:05,000 --> 00:15:07,000
There's worries or these fears or this.

237
00:15:07,000 --> 00:15:09,000
You got to get out of here.

238
00:15:09,000 --> 00:15:11,000
We're going to take that as a thought.

239
00:15:11,000 --> 00:15:13,000
And if and when we die,

240
00:15:13,000 --> 00:15:17,000
we're going to be mindful of that as it occurs.

241
00:15:17,000 --> 00:15:20,000
So reminding yourself that this could happen at any time.

242
00:15:20,000 --> 00:15:21,000
You better be ready for it.

243
00:15:21,000 --> 00:15:23,000
And being as mindful as you can,

244
00:15:23,000 --> 00:15:26,000
one step by one step by step by step.

245
00:15:26,000 --> 00:15:29,000
I think that you can consider within the limits.

246
00:15:29,000 --> 00:15:31,000
The rain thinking that rain was somehow going to help us.

247
00:15:31,000 --> 00:15:34,000
But we had made an determination to practice there.

248
00:15:34,000 --> 00:15:37,000
And so come what may we're going to do the practice.

249
00:15:37,000 --> 00:15:39,000
And I think that can be useful.

250
00:15:39,000 --> 00:15:42,000
There's a story of a monk who who who practiced meditation

251
00:15:42,000 --> 00:15:45,000
while it was snowing out in the open.

252
00:15:45,000 --> 00:15:48,000
And he just sat through it and persevered.

253
00:15:48,000 --> 00:15:50,000
Persevering in the face.

254
00:15:50,000 --> 00:15:53,000
There was a story of a monk who I think.

255
00:15:53,000 --> 00:15:56,000
There's a story of the monk in this monk in the Masudimaga

256
00:15:56,000 --> 00:15:58,000
who was determined to keep his morality.

257
00:15:58,000 --> 00:16:00,000
So these bandits,

258
00:16:00,000 --> 00:16:04,000
they tied him up with a vine that was still alive.

259
00:16:04,000 --> 00:16:08,000
And so this is how I understand this story.

260
00:16:08,000 --> 00:16:09,000
It was still alive.

261
00:16:09,000 --> 00:16:12,000
So he didn't want to break it by freeing himself.

262
00:16:12,000 --> 00:16:14,000
He knew that he could break out of it.

263
00:16:14,000 --> 00:16:16,000
But if he broke out of it, he'd be breaking the precept.

264
00:16:16,000 --> 00:16:19,000
This, you know, not even any moral precept,

265
00:16:19,000 --> 00:16:21,000
but it was one of the monks precepts,

266
00:16:21,000 --> 00:16:25,000
not to to kill vegetable life as well.

267
00:16:25,000 --> 00:16:27,000
And so he didn't do it.

268
00:16:27,000 --> 00:16:30,000
And he was sitting in sitting tied up in the forest

269
00:16:30,000 --> 00:16:33,000
and suddenly a forest where he came in the bandits ran away.

270
00:16:33,000 --> 00:16:35,000
And he was left sitting there.

271
00:16:35,000 --> 00:16:36,000
And he knew he could break out of it,

272
00:16:36,000 --> 00:16:37,000
but he didn't break out of it.

273
00:16:37,000 --> 00:16:40,000
And he sat there and was burnt to death.

274
00:16:40,000 --> 00:16:43,000
And the monks found him later and erected a jatia,

275
00:16:43,000 --> 00:16:45,000
a pagoda in his honor thinking,

276
00:16:45,000 --> 00:16:47,000
well, this guy was really serious.

277
00:16:47,000 --> 00:16:49,000
So I mean, even to that extent,

278
00:16:49,000 --> 00:16:54,000
I would say you're still within the limits of what is correct practice

279
00:16:54,000 --> 00:16:56,000
and what is useful practice theoretically.

280
00:16:56,000 --> 00:16:59,000
I mean, if a monk decides he wants to break out of the vines

281
00:16:59,000 --> 00:17:00,000
and break the precepts,

282
00:17:00,000 --> 00:17:02,000
I'm not going to scold him about it.

283
00:17:02,000 --> 00:17:06,000
But I don't think it's necessary to scold the monk

284
00:17:06,000 --> 00:17:10,000
who, in fact, the monks at the time seemed to think

285
00:17:10,000 --> 00:17:14,000
it was an incredible practice and an incredible sacrifice.

286
00:17:14,000 --> 00:17:16,000
That kind of pushing yourself,

287
00:17:16,000 --> 00:17:19,000
pushing yourself to stay with and to not react.

288
00:17:19,000 --> 00:17:23,000
But the action, the intention to do something

289
00:17:23,000 --> 00:17:25,000
has not just to do with ego, I think, also wrong view.

290
00:17:25,000 --> 00:17:49,000
And it has to do with what I was saying about

291
00:17:49,000 --> 00:17:51,000
that has an effect on the mind.

292
00:17:51,000 --> 00:17:56,000
It augments your wrong view and your attachment to wrong practice

293
00:17:56,000 --> 00:17:59,000
and also the ego in self as well.

294
00:17:59,000 --> 00:18:03,000
So I think that's where I would draw the line

295
00:18:03,000 --> 00:18:05,000
in terms of staying with it,

296
00:18:05,000 --> 00:18:07,000
even to the point of death,

297
00:18:07,000 --> 00:18:09,000
and actually encouraging it.

298
00:18:09,000 --> 00:18:11,000
So in the Sabasa, to the Buddha,

299
00:18:11,000 --> 00:18:14,000
says, even paying to the point of death,

300
00:18:14,000 --> 00:18:16,000
you should forbear.

301
00:18:16,000 --> 00:18:18,000
You should be patient with.

302
00:18:18,000 --> 00:18:21,000
This is Adiwasana, Baba.

303
00:18:21,000 --> 00:18:23,000
But then he says, I think we know the Baba,

304
00:18:23,000 --> 00:18:25,000
the one running away.

305
00:18:25,000 --> 00:18:27,000
He said, but if you're walking down a path

306
00:18:27,000 --> 00:18:29,000
and you come to a Bramble bush,

307
00:18:29,000 --> 00:18:31,000
you shouldn't just walk through the Bramble bush.

308
00:18:31,000 --> 00:18:33,000
This is where you should avoid certain things

309
00:18:33,000 --> 00:18:36,000
because there's no benefit from walking through the Bramble bush.

310
00:18:36,000 --> 00:18:40,000
If you're walking in the forest and an elephant

311
00:18:40,000 --> 00:18:43,000
starts charging it, you should go around the elephant

312
00:18:43,000 --> 00:18:46,000
or avoid the elephant or even run from the elephant

313
00:18:46,000 --> 00:18:48,000
and zigzag patterns, because apparently they can't run

314
00:18:48,000 --> 00:18:50,000
and zigzags.

315
00:18:50,000 --> 00:18:53,000
So the Buddha actually says there are cases

316
00:18:53,000 --> 00:18:56,000
where you might want to avoid certain things.

317
00:18:56,000 --> 00:19:00,000
But I think the point of,

318
00:19:00,000 --> 00:19:04,000
not creating suffering for that purpose

319
00:19:04,000 --> 00:19:07,000
because that's called see the Baba.

320
00:19:07,000 --> 00:19:10,000
I think it's important for us to understand

321
00:19:10,000 --> 00:19:12,000
that for an enlightened being,

322
00:19:12,000 --> 00:19:15,000
it might be different to sit in the forest

323
00:19:15,000 --> 00:19:24,000
bound up with a grass or with what was it with wine?

324
00:19:24,000 --> 00:19:30,000
And a dying fire.

325
00:19:30,000 --> 00:19:34,000
But for unenlightened beings,

326
00:19:34,000 --> 00:19:37,000
it's maybe better to run.

327
00:19:37,000 --> 00:19:38,000
This is true.

328
00:19:38,000 --> 00:19:40,000
The other thing is you don't always want to push yourself.

329
00:19:40,000 --> 00:19:43,000
The only thing I was saying is you can't fall the guy.

330
00:19:43,000 --> 00:19:48,000
But you shouldn't consider that that's necessary

331
00:19:48,000 --> 00:19:51,000
or that's even the best.

332
00:19:51,000 --> 00:19:53,000
Because it could be that by doing that,

333
00:19:53,000 --> 00:19:55,000
you're born in hell as a result.

334
00:19:55,000 --> 00:19:58,000
Because, oh my god, I'm burning up

335
00:19:58,000 --> 00:20:00,000
whereas you could have instead left

336
00:20:00,000 --> 00:20:01,000
and gone and practiced.

337
00:20:01,000 --> 00:20:03,000
We passed in and become enlightened

338
00:20:03,000 --> 00:20:06,000
and at least gone to heaven.

339
00:20:06,000 --> 00:20:13,000
Yeah.

