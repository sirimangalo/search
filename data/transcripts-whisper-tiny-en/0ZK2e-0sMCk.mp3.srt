1
00:00:00,000 --> 00:00:05,360
Since I teach in an inner-city school, I create karma with students that have social

2
00:00:05,360 --> 00:00:06,360
emotional problems.

3
00:00:06,360 --> 00:00:11,640
It seems like some students' negative karma are so severe that they are self-destructive.

4
00:00:11,640 --> 00:00:16,280
Is it better to create karma with wealthy, highly intelligent and nice people, so that in

5
00:00:16,280 --> 00:00:21,040
my next life I will be better associated with people that have these characteristics, rather

6
00:00:21,040 --> 00:00:36,960
than with people that have few virtues to have a better rebirth or a better karma?

7
00:00:36,960 --> 00:00:42,520
I think it's kind of a loaded question because I did answer something along these lines

8
00:00:42,520 --> 00:00:54,280
before about how it's better to help people who are of sound mind.

9
00:00:54,280 --> 00:01:01,160
So I assume that there's at least a part of this is an interest in that topic.

10
00:01:01,160 --> 00:01:13,320
But the point that I hope was made clear in that speech was that it has nothing to do

11
00:01:13,320 --> 00:01:16,560
with your own benefit.

12
00:01:16,560 --> 00:01:23,560
It has to do with the benefit that you're going to bring to others because you can bring

13
00:01:23,560 --> 00:01:32,800
benefit to people who have difficult minds.

14
00:01:32,800 --> 00:01:40,320
As I said, I taught in the jail, I taught meditation in the jail, and teaching in the

15
00:01:40,320 --> 00:01:48,400
jail gave me a lot of the same feeling as teaching in second life when I taught in this

16
00:01:48,400 --> 00:01:51,200
virtual reality world.

17
00:01:51,200 --> 00:01:55,640
In both places, I got the same feeling was that these people are not really serious about

18
00:01:55,640 --> 00:01:56,640
this.

19
00:01:56,640 --> 00:02:02,000
As I said, this one prisoner, he asked me, he wanted to learn how to fly, how to leave

20
00:02:02,000 --> 00:02:03,000
his body.

21
00:02:03,000 --> 00:02:09,240
He's in jail, what could be better to be able to leave his body and leave the prison

22
00:02:09,240 --> 00:02:15,560
and go flying around the city?

23
00:02:15,560 --> 00:02:17,200
But you can help those people.

24
00:02:17,200 --> 00:02:19,360
Theoretically, you can help anyone.

25
00:02:19,360 --> 00:02:25,040
And so part of the answer, I think, is to not be averse to helping the people who are

26
00:02:25,040 --> 00:02:26,720
you're confronted with.

27
00:02:26,720 --> 00:02:33,280
But the point was, if you're going to dedicate your life to helping, to bringing benefit

28
00:02:33,280 --> 00:02:39,200
to others, or if you're going to dedicate a portion of your life, and dedicating your life

29
00:02:39,200 --> 00:02:45,360
to helping others, you should try to find a way that so that your help will be best,

30
00:02:45,360 --> 00:02:47,480
will be most efficient.

31
00:02:47,480 --> 00:02:52,040
And for my point of view, the most efficient thing to do is to teach people how to deal

32
00:02:52,040 --> 00:02:58,800
with these people, or it's more efficient to teach people who are going to then be teachers.

33
00:02:58,800 --> 00:03:04,840
It's more efficient for me to teach people who come here, how to become leaders and spiritual

34
00:03:04,840 --> 00:03:15,760
guides than it would be for me to go around as they might do teaching people who are not

35
00:03:15,760 --> 00:03:22,440
spiritual, or dealing with interacting with people who are not, who are on a, maybe have

36
00:03:22,440 --> 00:03:25,400
a lesser level of interest.

37
00:03:25,400 --> 00:03:30,520
And it would be better to be that sort of person who is a teacher and a guide than to

38
00:03:30,520 --> 00:03:36,840
be someone who is further on down who is just helping people's material benefit.

39
00:03:36,840 --> 00:03:43,560
So for example, a person who is a spiritual guide for teaching people how to be good lawyers

40
00:03:43,560 --> 00:03:51,960
or good doctors or good secular or school teachers, better to be that person than to be

41
00:03:51,960 --> 00:03:57,560
the person who is the school teacher who is teaching secular subjects, subjects that have

42
00:03:57,560 --> 00:04:04,120
some benefit, but have far less benefit obviously than the spiritual side of being able

43
00:04:04,120 --> 00:04:05,520
to teach meditation.

44
00:04:05,520 --> 00:04:07,440
So I would answer in two ways.

45
00:04:07,440 --> 00:04:11,160
The first one is that if you're with these people, if this is your job and this is where

46
00:04:11,160 --> 00:04:15,600
you are in life, then certainly don't avoid them and don't think, oh boy, I wish I

47
00:04:15,600 --> 00:04:22,960
was with people who were rich and handsome and or whatever who had good karma and lots

48
00:04:22,960 --> 00:04:29,360
of who gave me a good rebirth or so on.

49
00:04:29,360 --> 00:04:35,480
But on the other hand, you sometimes have to question whether your life is the best use,

50
00:04:35,480 --> 00:04:45,560
whether your path at this time is the best use of your resources for both yourself and others.

51
00:04:45,560 --> 00:04:50,440
And that's always a judgment call because eventually of course the answer is no, the best

52
00:04:50,440 --> 00:04:54,720
life is to go off and become a monk and live in a cave.

53
00:04:54,720 --> 00:05:00,520
So it's totally up to you how far you want to go with that.

54
00:05:00,520 --> 00:05:05,680
There's the story of this Sotapana who then became the wife of a hunter, so she lived

55
00:05:05,680 --> 00:05:12,120
as a hunter's wife cleaning his, cleaning the guts off of his traps.

56
00:05:12,120 --> 00:05:16,880
So you certainly you can live in any position and you shouldn't have to worry about where

57
00:05:16,880 --> 00:05:19,240
you, where you find yourself.

58
00:05:19,240 --> 00:05:26,160
But the point of that was just in regards to bringing, having the most benefit and from

59
00:05:26,160 --> 00:05:32,320
my point of view it comes from teaching people how to teach meditation really or at

60
00:05:32,320 --> 00:05:36,640
least teaching meditation.

61
00:05:36,640 --> 00:05:44,560
I have two things to say to this.

62
00:05:44,560 --> 00:05:55,880
The first thing is you should, when you are with them, try not to become like them like

63
00:05:55,880 --> 00:06:10,720
with unwholesome karma and destructive, but you should try to make them more like you

64
00:06:10,720 --> 00:06:11,720
are.

65
00:06:11,720 --> 00:06:20,280
You should try to teach them the virtues when they have none and to be not destructive

66
00:06:20,280 --> 00:06:25,960
when they are destructive, that's the one thing I wanted to say.

67
00:06:25,960 --> 00:06:35,080
The other thing is that your karma brought you there maybe, there are always more reasons

68
00:06:35,080 --> 00:06:41,040
to get somewhere or to experience something than just only karma, but let's say your

69
00:06:41,040 --> 00:06:53,240
karma got you there than that's one thing, but what you are doing there, what you are

70
00:06:53,240 --> 00:06:59,320
doing with them is what creates your future karma.

71
00:06:59,320 --> 00:07:05,440
So if you can teach them in a wholesome way, when you can make them better people, when

72
00:07:05,440 --> 00:07:13,040
you don't get angry, when you don't do unwholesome things while you are teaching them,

73
00:07:13,040 --> 00:07:24,840
then it's absolutely not important if you teach intelligent or high class people or

74
00:07:24,840 --> 00:07:40,760
low class people, it's just important how wholesome your mind is at that moment.

75
00:07:40,760 --> 00:08:02,560
I could have explained better, but it's fine, it's getting late.

