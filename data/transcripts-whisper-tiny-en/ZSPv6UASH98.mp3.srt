1
00:00:00,000 --> 00:00:05,240
I have trouble letting go of relationships that have ended.

2
00:00:05,240 --> 00:00:10,000
I also was born with a vision problem, which I can't accept and struggle to deal with

3
00:00:10,000 --> 00:00:11,800
on a daily basis.

4
00:00:11,800 --> 00:00:18,000
How can I train myself to let go of these things that cause me pain?

5
00:00:18,000 --> 00:00:19,000
Right.

6
00:00:19,000 --> 00:00:25,440
I was literally here, these are issues that, well the first one is an issue that most

7
00:00:25,440 --> 00:00:30,680
people in the world have to deal with except hermits living in caves.

8
00:00:30,680 --> 00:00:38,160
And the second one is one of these difficult situations that are somewhat unique to the

9
00:00:38,160 --> 00:00:39,160
individual.

10
00:00:39,160 --> 00:00:44,960
So it's something that you have to deal with that many other people do not.

11
00:00:44,960 --> 00:00:54,760
Which leads to its own extra layer of issues, but that's how I would generally suggest

12
00:00:54,760 --> 00:01:04,320
to approach your issues, approach them layer by layer, focus on what is clearest in your

13
00:01:04,320 --> 00:01:09,720
mind, and get it clear in your mind, get a clear understanding of the different layers

14
00:01:09,720 --> 00:01:10,720
to it.

15
00:01:10,720 --> 00:01:18,480
So the first layer with the vision problems is maybe, well one layer will be maybe, maybe

16
00:01:18,480 --> 00:01:23,200
for some people feeling sorry for yourself because you have the problem and someone else

17
00:01:23,200 --> 00:01:26,160
doesn't have, and normal people don't have the problem.

18
00:01:26,160 --> 00:01:32,080
Part of it might be the anxiety of being around other people who don't have this problem,

19
00:01:32,080 --> 00:01:37,120
maybe having to wear special glasses that make you stand out or having to answer all

20
00:01:37,120 --> 00:01:46,600
sorts of questions or having to be led around by a dog or carry a cane or so on or whatever.

21
00:01:46,600 --> 00:01:51,120
So there will be these extra layers to the problem before you can actually get to your

22
00:01:51,120 --> 00:01:58,200
own acceptance of it, actually get to the problem, same with, would go with relationships.

23
00:01:58,200 --> 00:02:04,880
So when the relationship ends, you feel sad about the relationship, but that can lead

24
00:02:04,880 --> 00:02:12,880
to depression, it can lead to self-loathing, it can lead to even self-hatred when you

25
00:02:12,880 --> 00:02:19,760
can be angry at your self, your angry self for what you didn't do to maintain the relationship,

26
00:02:19,760 --> 00:02:23,760
you can start obsessing over what you could have done to save the relationship, you

27
00:02:23,760 --> 00:02:29,760
can start obsessing over how bad your life is now that you're not in the relationship.

28
00:02:29,760 --> 00:02:35,040
And once you start to see how this affects you, you can even start getting upset about

29
00:02:35,040 --> 00:02:39,560
how your level of upset is affecting your life.

30
00:02:39,560 --> 00:02:44,720
So you can see there's layer upon layer and it actually can become a feedback loop until

31
00:02:44,720 --> 00:02:49,960
it gets bigger and bigger and bigger and something that would simply a sadness can turn

32
00:02:49,960 --> 00:02:56,240
into a real neurosy, neurosis.

33
00:02:56,240 --> 00:03:03,640
So it's important first to take everything by layer by layer and second or maybe first thing

34
00:03:03,640 --> 00:03:10,440
is to separate it out into individual experiences.

35
00:03:10,440 --> 00:03:16,840
So instead of looking at anything as an issue, as a problem, try to see what's going on

36
00:03:16,840 --> 00:03:26,600
right now because we are not atomic and the word atom means something that is indivisible,

37
00:03:26,600 --> 00:03:30,560
so actually the atom isn't atomic, but if you don't understand the word atomic, atomic

38
00:03:30,560 --> 00:03:38,080
means something that is indivisible, but we are divisible and all of our experience,

39
00:03:38,080 --> 00:03:43,800
all of our situations, all of the issues in our life are also indivisible, they're not

40
00:03:43,800 --> 00:03:47,600
atomic, they can be broken up in the individual part.

41
00:03:47,600 --> 00:03:54,080
So if you have a problem, say with mourning or sadness, or if you have a problem with low

42
00:03:54,080 --> 00:04:00,360
self-esteem, right, so I have low self-esteem, you're talking about an atom, you're talking

43
00:04:00,360 --> 00:04:06,040
about an issue, something that is this, right, and once it's this, you can't do anything

44
00:04:06,040 --> 00:04:13,320
with that, unless you've got a cure for this, which we don't have, then you can't fix

45
00:04:13,320 --> 00:04:17,680
it, and why we don't have a cure is because it actually is not real, it's a concept in

46
00:04:17,680 --> 00:04:22,920
your mind that you create out of a series of things that do exist.

47
00:04:22,920 --> 00:04:30,520
So if you want to come to an actual solution in reality, you have to focus on that which

48
00:04:30,520 --> 00:04:37,280
exists, what exists are experiences.

49
00:04:37,280 --> 00:04:42,140
You might have an experience of sadness, right now you might have an experience of pain,

50
00:04:42,140 --> 00:04:47,880
you might have an experience of tension, you might have an experience of anger, you might

51
00:04:47,880 --> 00:04:51,100
have an experience of happiness, when you think, when you remember all the good things

52
00:04:51,100 --> 00:04:56,280
you did with the person, happiness, and then sadness when you realize you can't have

53
00:04:56,280 --> 00:05:06,280
you can't actually go any further with it, as far as indulging in the pleasure of being with the person who you love, and so on.

54
00:05:06,280 --> 00:05:13,280
You will also have simple experiences, like with a vision problem.

55
00:05:13,280 --> 00:05:18,280
It means you will have experiences where you realize that you can't see something.

56
00:05:18,280 --> 00:05:23,280
So you have to look at something, put something in front of you, you can't see it, for example.

57
00:05:23,280 --> 00:05:27,280
That's just an, that is actually just an objective experience.

58
00:05:27,280 --> 00:05:34,280
There's nothing wrong with that until you say to yourself, that's bad, until you get upset by it.

59
00:05:34,280 --> 00:05:41,280
So, there's individual sequences, there's individual experiences in sequence.

60
00:05:41,280 --> 00:05:51,280
And once you can understand these, then you can put them into layers, and you can see what is in this layer, what is in this layer, and then you can slowly break up the layers, start with the top layer,

61
00:05:51,280 --> 00:05:57,280
you know, your guilt about whatever the situation is, or whatever, how you feel about the situation.

62
00:05:57,280 --> 00:06:11,280
And once you can break that apart, then actually look at the situation, like the sadness, or the frustration at having a business with the guilt, or the low self-esteem.

63
00:06:11,280 --> 00:06:25,280
That's really the essence of the Buddha's teaching. You know, when they had this big debate about, I want to say 500 years after the Buddha passed away, I can't.

64
00:06:25,280 --> 00:06:30,280
I don't know my history is not that good, but it's kind of shameful actually. I should know all this.

65
00:06:30,280 --> 00:06:48,280
But whenever they made the Katauatu, this book in the Abidhamma would have been the third Buddhist council, which would have been maybe 300 years after the Buddha passed away, I can't remember now.

66
00:06:48,280 --> 00:06:55,280
Anyway, the thing was, they had all these bogus monks who were saying the Buddha taught this, the Buddha taught that.

67
00:06:55,280 --> 00:07:03,280
And finally, there was one monk who was able to say, what the Buddha taught, and he said, who was the Buddha?

68
00:07:03,280 --> 00:07:15,280
What sort of monk was the Buddha? The Buddha was a wee budge of wadi. One who claims, or who teaches, who exhorts people to break things up.

69
00:07:15,280 --> 00:07:26,280
We budge of means to dissect and to break into constituent parts. It was basically the five aggregates, or the six senses, or the elements.

70
00:07:26,280 --> 00:07:39,280
So those pieces, the building blocks, that make up reality, because if you can do that, then you can solve all of your problems, because you come to see actually that they're not problems.

71
00:07:39,280 --> 00:07:47,280
They're just experiences, and you come to see how this way of reacting is useless, that way of reacting is useless.

72
00:07:47,280 --> 00:07:58,280
So eventually you stop reacting in ways that bring suffering to you or to other people, because you see that they're unbeneficial, they're harmful.

73
00:07:58,280 --> 00:08:11,280
That's general, and it's sort of relating to what you experience. So hopefully that helps, wishing you all the best. Obviously, practicing meditation helps.

74
00:08:11,280 --> 00:08:17,280
So one thing you might try to look at is the attachment to self, and the idea that this is me, this is mine.

75
00:08:17,280 --> 00:08:30,280
So you might say in the West Eagle, the idea of something that's causing me pain, and when you identify with the pain, instead of just experiencing it, now this is pain, seeing it just as an experience.

76
00:08:30,280 --> 00:08:43,280
When you have that ego attached, it makes all your problems much worse. So instead of pain, now it's my pain, and it's something that's on me, something that I have to bear.

77
00:08:43,280 --> 00:08:50,280
Which makes it much, much worse, instead of just saying, oh, this is pain. It might be painful, but it will come, and it will go.

78
00:08:50,280 --> 00:08:59,280
Once it's your pain, then it's a problem, because then you get angry about it, and I'm sat and frustrated as to why does my life have to be this way, and so on, and so on.

79
00:08:59,280 --> 00:09:08,280
The whole ego, the eye, if you can let go, that's great. Obviously, practicing meditation best way to do it, and try reading my booklet if you haven't already.

80
00:09:08,280 --> 00:09:14,280
I recommend that as a resource, how to meditate by you to demo.

