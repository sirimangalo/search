1
00:00:00,000 --> 00:00:03,000
Can meditation help cure agrophobia?

2
00:00:05,000 --> 00:00:08,000
Agro, I grow.

3
00:00:08,000 --> 00:00:11,000
I think it's agrophobia, she's here at that side.

4
00:00:12,000 --> 00:00:17,000
Agrophobia, I know Agrophobia is fear of heights,

5
00:00:17,000 --> 00:00:19,000
is she spelling it correctly or am I just wrong?

6
00:00:19,000 --> 00:00:21,000
What is agrophobia?

7
00:00:23,000 --> 00:00:26,000
No, Agrophobia, that might be that you are,

8
00:00:26,000 --> 00:00:29,000
or could you marry on specify?

9
00:00:30,000 --> 00:00:34,000
Agrophobia, I know Agrophobia, I thought it was fear of heights

10
00:00:34,000 --> 00:00:37,000
because it's the same as the Polyward Agrophobia,

11
00:00:37,000 --> 00:00:39,000
which means height or pinnacle.

12
00:00:40,000 --> 00:00:42,000
And maybe I'm wrong.

13
00:00:42,000 --> 00:00:43,000
Agro.

14
00:00:46,000 --> 00:00:49,000
Agrophobia, I know.

15
00:00:51,000 --> 00:00:54,000
Maybe we can get back to agrophobia.

16
00:00:54,000 --> 00:01:03,000
Okay, I think it can be taken more general even if we don't know

17
00:01:03,000 --> 00:01:07,000
what the word Agro, Agro.

18
00:01:08,000 --> 00:01:10,000
For sure.

19
00:01:11,000 --> 00:01:21,000
If practice properly, it can certainly

20
00:01:21,000 --> 00:01:32,000
not all kinds of meditation can probably cure phobias.

21
00:01:33,000 --> 00:01:37,000
And it depends, of course, of the intensity,

22
00:01:38,000 --> 00:01:42,000
the time that you spend meditating.

23
00:01:42,000 --> 00:01:56,000
And the most important of the willingness and the capability to let go.

24
00:01:57,000 --> 00:02:08,000
It's probably so that a phobia could be cured rather quickly with meditation,

25
00:02:08,000 --> 00:02:13,000
but when the mind is still attached and when the mind

26
00:02:14,000 --> 00:02:21,000
clings to this attachment or clings to being attached,

27
00:02:22,000 --> 00:02:27,000
then a phobia cannot be let go of.

28
00:02:27,000 --> 00:02:32,000
And so you don't have to work only on the phobias,

29
00:02:32,000 --> 00:02:39,000
but more even on the letting go part of it.

30
00:02:42,000 --> 00:02:48,000
Because a phobia is nothing as far as I understand the term,

31
00:02:48,000 --> 00:02:54,000
but I'm not a specialist, it's nothing that is chemical,

32
00:02:54,000 --> 00:03:01,000
or so it is quite strong neurosis.

33
00:03:02,000 --> 00:03:13,000
And like a really deep sitting intense fear in this case of going out in public places.

34
00:03:13,000 --> 00:03:26,000
So, yes, when you meditate, we pass on a meditation and note every single moment,

35
00:03:27,000 --> 00:03:35,000
and you note every fear that arises,

36
00:03:35,000 --> 00:03:45,000
and you know it arising and seizing again, then you might be able to understand it,

37
00:03:46,000 --> 00:03:49,000
not understand in terms to question,

38
00:03:49,000 --> 00:03:53,000
where does it come from, why do I have it and all that?

39
00:03:53,000 --> 00:03:59,000
Because this is off topic, this is not important for that question.

40
00:03:59,000 --> 00:04:12,000
Important is to understand when it is there and when it is not there,

41
00:04:12,000 --> 00:04:21,000
and even in five seconds time, let's say, or ten seconds time,

42
00:04:21,000 --> 00:04:30,000
there will probably, if you say now with your knowledge of now, you would say,

43
00:04:30,000 --> 00:04:34,000
well, all that five seconds, all that ten seconds,

44
00:04:34,000 --> 00:04:40,000
there was the agarophobia, but in reality,

45
00:04:40,000 --> 00:04:47,000
there was probably one second or even less of agarophobia,

46
00:04:47,000 --> 00:04:53,000
and another second of the thought, oh, I have fear,

47
00:04:53,000 --> 00:04:58,000
and then even something different, and another thought came up.

48
00:04:58,000 --> 00:05:10,000
So, when you are honest with this, and try to really let go of the idea that I have this,

49
00:05:10,000 --> 00:05:16,000
then I would say, yes, possible.

50
00:05:16,000 --> 00:05:22,000
Yeah, just to expand on that, the real problem as you said,

51
00:05:22,000 --> 00:05:26,000
what you are saying is that it is the identification.

52
00:05:26,000 --> 00:05:30,000
Fear is one thing, fear is anger based, but fear,

53
00:05:30,000 --> 00:05:37,000
as with all of our defilements, are rooted in delusion, in ignorance,

54
00:05:37,000 --> 00:05:43,000
in the idea that they are an entity, one that they are ours,

55
00:05:43,000 --> 00:05:46,000
two that they have a core.

56
00:05:46,000 --> 00:05:51,000
For example, in this case, you will say, I have agarophobia,

57
00:05:51,000 --> 00:05:57,000
which we now learn is an anxiety disorder characterized by anxiety and situations

58
00:05:57,000 --> 00:06:03,000
where it is perceived to be difficult or embarrassing to escape.

59
00:06:03,000 --> 00:06:09,000
So, someone else is just fear of being in public places, being around people.

60
00:06:09,000 --> 00:06:12,000
So, you think that you have this condition.

61
00:06:12,000 --> 00:06:16,000
So, first of all, you say it is mine, second of all, you say it is a condition.

62
00:06:16,000 --> 00:06:20,000
This goes with really anything, whether it be physical diseases,

63
00:06:20,000 --> 00:06:25,000
or more critically with mental sicknesses.

64
00:06:25,000 --> 00:06:29,000
People who are depressed will say, I have clinical depression,

65
00:06:29,000 --> 00:06:35,000
I have this kind of, I am bipolar, or I am autistic, or XYZ.

66
00:06:35,000 --> 00:06:39,000
So, we give it a self of its own, we give it a life of its own.

67
00:06:39,000 --> 00:06:45,000
What in fact, as Paani was saying, it is just moment to moment experiences.

68
00:06:45,000 --> 00:06:52,000
So, really, the fear is a very small thing and not of any consequence,

69
00:06:52,000 --> 00:06:54,000
not of much consequence.

70
00:06:54,000 --> 00:06:57,000
What is a much more consequence is the clinging to it.

71
00:06:57,000 --> 00:07:00,000
So, if you are just saying to yourself, afraid, afraid,

72
00:07:00,000 --> 00:07:05,000
you will come to see that it is just fear.

73
00:07:05,000 --> 00:07:07,000
And it doesn't have any significance.

74
00:07:07,000 --> 00:07:09,000
It doesn't mean that you should or shouldn't go outside.

75
00:07:09,000 --> 00:07:12,000
I would say one thing that probably if you are more you meditate,

76
00:07:12,000 --> 00:07:15,000
the less interested you are going out in public places.

77
00:07:15,000 --> 00:07:19,000
It is not like, yes, then I will be able to go out and be a party animal

78
00:07:19,000 --> 00:07:21,000
or a social butterfly.

79
00:07:21,000 --> 00:07:23,000
So, no, not likely.

80
00:07:23,000 --> 00:07:28,000
You may have less fear of going outside, but you will have much more distaste

81
00:07:28,000 --> 00:07:31,000
for going outside or disinterest and going outside.

82
00:07:31,000 --> 00:07:36,000
So, people might not even notice the difference.

83
00:07:36,000 --> 00:07:39,000
But you will notice the difference yourself because it is not out of fear.

84
00:07:39,000 --> 00:07:42,000
It will be out of wisdom and the realization that there is a reason

85
00:07:42,000 --> 00:07:51,000
that these fears arise because there are so many defilements involved in public appearances

86
00:07:51,000 --> 00:07:56,000
and situations, society and so on.

87
00:07:56,000 --> 00:08:01,000
All of our defilements become multiplied when we are around other people.

88
00:08:01,000 --> 00:08:07,000
And so, this is why we develop fear of them because of the feeling of being inadequate

89
00:08:07,000 --> 00:08:14,000
or memory that we did stupid things, said stupid things that we were socially inept

90
00:08:14,000 --> 00:08:15,000
and so on.

91
00:08:15,000 --> 00:08:20,000
And of course, many other reasons, but it all has to do with the complexity of social interaction

92
00:08:20,000 --> 00:08:23,000
which, to some extent, can be mitigated.

93
00:08:23,000 --> 00:08:29,000
But in the end, it is generally just to be avoided because you can't stop people

94
00:08:29,000 --> 00:08:31,000
from being silly.

95
00:08:31,000 --> 00:08:34,000
You know, you go to a social situation, so I will just be mindful

96
00:08:34,000 --> 00:08:39,000
but everyone is drunk and hitting on each other and fighting with each other

97
00:08:39,000 --> 00:08:43,000
and saying silly things, doing silly things.

98
00:08:43,000 --> 00:08:50,000
In the end, you just decide that you are not to leave out the house.

