1
00:00:00,000 --> 00:00:23,760
Okay, good evening, everyone.

2
00:00:23,760 --> 00:00:51,760
Welcome to tonight's demonstration.

3
00:00:51,760 --> 00:00:58,760
This night we talked about happiness, right, last night two kinds of happiness.

4
00:00:58,760 --> 00:01:17,360
It's the idea that people seeking happiness tend to not find actual happiness.

5
00:01:17,360 --> 00:01:32,160
And it goes perhaps without saying that the goal is to find happiness, just a different

6
00:01:32,160 --> 00:01:34,760
type of happiness.

7
00:01:34,760 --> 00:01:43,760
It still bears mentioning and discussing the fact that we are trying to find happiness.

8
00:01:43,760 --> 00:01:49,960
This is our goal.

9
00:01:49,960 --> 00:02:01,360
This is the ultimate quest.

10
00:02:01,360 --> 00:02:10,680
It's easy to miss that in our practice and to get hung up on unpleasantness with the

11
00:02:10,680 --> 00:02:17,800
wrong idea that somehow meditation should be unpleasant.

12
00:02:17,800 --> 00:02:26,600
Just as easy it is to get hung up on the idea that meditation should be pleasant.

13
00:02:26,600 --> 00:02:34,280
So we get hung up, so it's clear when you get hung up on meditation it's having to be pleasant

14
00:02:34,280 --> 00:02:42,960
and even climb in such a way to avoid unpleasant experiences or those experiences that cause

15
00:02:42,960 --> 00:02:47,000
disliking to arise.

16
00:02:47,000 --> 00:03:02,120
The problem of course with that is that you can't create a state where you're never in danger

17
00:03:02,120 --> 00:03:11,960
of the arising of states that writing of things, encountering of things that might trigger

18
00:03:11,960 --> 00:03:23,640
dislike in you.

19
00:03:23,640 --> 00:03:32,000
But it also happens that a meditator can get caught up in the idea that as I said, meditation

20
00:03:32,000 --> 00:03:38,720
should be unpleasant and so they become to be averse to meditation.

21
00:03:38,720 --> 00:03:43,920
They associate meditation with discomfort, with unpleasantness.

22
00:03:43,920 --> 00:03:53,520
So meditation can be quite unattractive.

23
00:03:53,520 --> 00:04:03,320
There's an automatic repulsion, oh, I have to go and meditate again how would it

24
00:04:03,320 --> 00:04:08,240
show or would it bother?

25
00:04:08,240 --> 00:04:11,600
That's not good either.

26
00:04:11,600 --> 00:04:15,800
Meditators will often come with this as a problem, especially long-term meditators practicing

27
00:04:15,800 --> 00:04:23,480
at home because they're not privy to the sort of common tranquility that comes

28
00:04:23,480 --> 00:04:26,440
from being in a meditation center.

29
00:04:26,440 --> 00:04:31,960
So there's lots of states that have the potential to create dislike.

30
00:04:31,960 --> 00:04:39,400
But this is the key distinction, is that the suffering doesn't come from the experiences

31
00:04:39,400 --> 00:04:45,640
it comes from our reactions to experiences.

32
00:04:45,640 --> 00:05:02,680
And so meditation isn't about, it isn't about constantly receiving only those experiences

33
00:05:02,680 --> 00:05:13,160
that please us and never experiencing anything that displeases us.

34
00:05:13,160 --> 00:05:22,280
It's about freeing ourselves from any kind of displeasure.

35
00:05:22,280 --> 00:05:31,160
Well, that's really the only factor in the equation that we have to work out, how to remove

36
00:05:31,160 --> 00:05:33,760
displeasure.

37
00:05:33,760 --> 00:05:39,440
If you look at the Abhidhamma, it's quite interesting and revealing.

38
00:05:39,440 --> 00:05:45,800
We have two states, Somanasa, Domanasa.

39
00:05:45,800 --> 00:05:55,160
So comes from Su, which means good, Doh comes from Doh, which means bad, Domanasa and

40
00:05:55,160 --> 00:06:10,640
Somanasa.

41
00:06:10,640 --> 00:06:20,280
Somanasa means mental pleasure, a mental good feeling, a happy mind, and Doh Manasa is

42
00:06:20,280 --> 00:06:23,160
the opposite of displeased mind.

43
00:06:23,160 --> 00:06:32,160
But looking at where these two fall is quite revealing, sorry, there's the third one as

44
00:06:32,160 --> 00:06:33,160
well.

45
00:06:33,160 --> 00:06:44,960
The third one is Upeka, Upeka means equanimity or neutrality, a neutral feeling.

46
00:06:44,960 --> 00:06:50,480
And so if you look through the types of minds that can arise according to the Abhidhamma,

47
00:06:50,480 --> 00:06:55,920
we have the Akusalamula Chita and the Sobanajita.

48
00:06:55,920 --> 00:07:03,120
So Akusalamula means those which are rooted in unwholesomeness, and the Sobanajita are

49
00:07:03,120 --> 00:07:11,680
those rooted in wholesomeness or those that are beautiful, including wholesome and wholesome

50
00:07:11,680 --> 00:07:17,960
result in and so on.

51
00:07:17,960 --> 00:07:22,200
And somanasa occurs in both.

52
00:07:22,200 --> 00:07:24,680
The first eight minds are minds of greed.

53
00:07:24,680 --> 00:07:35,520
We have somanasa, Sahagatang, Ditigata, Sampa Yutang, Asangarikang, Lobamulajitang, Akang.

54
00:07:35,520 --> 00:07:48,840
The first mind is a mind with pleasure, with a happy mind, with a happy state of mind.

55
00:07:48,840 --> 00:07:53,080
But it's accompanied by views, it's a greed mind.

56
00:07:53,080 --> 00:07:59,960
So greed can be accompanied by pleasure, it says my greed is such a tough one, desire.

57
00:07:59,960 --> 00:08:07,160
And you want something, it can be quite pleasant to want, is by the Buddha said, desire

58
00:08:07,160 --> 00:08:22,120
is like a sweet poison, it's like bait on a hook, yummy, we don't see the hook.

59
00:08:22,120 --> 00:08:28,920
Desire is pleasant, and this is why the problem, this is why we can't trust the pleasantness

60
00:08:28,920 --> 00:08:34,600
of an experience, as an indicator of the goodness of an experience.

61
00:08:34,600 --> 00:08:39,360
This is what leads some people to say you have to suffer, to build character, to think

62
00:08:39,360 --> 00:08:49,800
that goodness has to come from unpleasantness, doesn't have to, in fact it can't.

63
00:08:49,800 --> 00:08:58,840
Because Domanasa only occurs in Akuslamulaj, a displeased mind is only associated with anger,

64
00:08:58,840 --> 00:09:10,240
Domanasa, Sahagatang, Asankarican, Akuslamulajitang, Akuslamulajitang, Akuslamulajitang, Akuslamulajitang.

65
00:09:10,240 --> 00:09:20,640
The mind that is prompted or un-prompted, that's associated with Apatigaha, Domanasa, Sahagatang,

66
00:09:20,640 --> 00:09:32,040
Akuslamulajitang, Aasangarican, aversion, it's only the aversion to the anger-mind that

67
00:09:32,040 --> 00:09:34,840
have displeasure in them.

68
00:09:34,840 --> 00:09:40,600
You can't possibly have a wholesome mind that is accompanied with displeasure.

69
00:09:40,600 --> 00:09:49,720
Now you can have a neutral mind, your mind can be equonamous, but it can be equoniveness

70
00:09:49,720 --> 00:10:01,720
When you desire something, so the lobamulajita can also have upekasagatam, can also be accompanied

71
00:10:01,720 --> 00:10:05,720
with equanimity.

72
00:10:05,720 --> 00:10:12,640
So to sum up, or then you have in the sopenajita, so just to round it out, but the sopenajita,

73
00:10:12,640 --> 00:10:19,640
so you have sopenajasagatam and upekasagatam.

74
00:10:19,640 --> 00:10:34,640
So either with wisdom or without wisdom, but either way wholesome.

75
00:10:34,640 --> 00:10:45,640
And so to sum up, sopenajasag, a happy mind, and upeka can occur in both wholesome and unwholesome minds.

76
00:10:45,640 --> 00:10:52,320
So neither one is a good sign, is a good indication of the goodness or the wholesomeness

77
00:10:52,320 --> 00:10:57,760
of the unwholesomeness of the mind, but don't mean a second only occur with an anger

78
00:10:57,760 --> 00:11:03,760
of the mind. You can't be displeased when performing good deeds.

79
00:11:03,760 --> 00:11:13,760
When performing good deeds in the moment of a wholesome state.

80
00:11:13,760 --> 00:11:18,760
So a bit technical bit, it gets the point across.

81
00:11:18,760 --> 00:11:28,760
It makes it sort of clear that too often we get hung up on the pleasure and displeasure,

82
00:11:28,760 --> 00:11:30,760
and we're seeing them rightly.

83
00:11:30,760 --> 00:11:38,760
So we get hung up on the idea that if something is pleasant or calming, then it must be good.

84
00:11:38,760 --> 00:11:44,760
But there's a lot of unwholesomeness that associated with calm and happiness.

85
00:11:44,760 --> 00:11:51,760
It's addictive, any sensation that we like, be it calm or be it a happy sensation.

86
00:11:51,760 --> 00:11:59,760
Anything associated that brings about this sensation, be it a calm sensation or a happy sensation.

87
00:11:59,760 --> 00:12:06,760
When you like it, when you desire for it or you desire for that which brings it about,

88
00:12:06,760 --> 00:12:10,760
that's unwholesomeness.

89
00:12:10,760 --> 00:12:19,760
And as a result, it has craving, clinging, addiction, eventual disappointment and suffering.

90
00:12:19,760 --> 00:12:22,760
So even meditation can be a source of this.

91
00:12:22,760 --> 00:12:38,760
If you like something, you like the feelings that you get, that liking is a cause for stress and suffering.

92
00:12:38,760 --> 00:12:50,760
But goodness, goodness also has to be pleasant or calm when you're practicing meditation

93
00:12:50,760 --> 00:12:58,760
or when you're doing other wholesome deeds like giving gifts or keeping moral precepts.

94
00:12:58,760 --> 00:13:08,760
This all has to be accompanied and it very often is accompanied by joy, happiness or calm.

95
00:13:08,760 --> 00:13:11,760
It can never be as accompanied by displeasure.

96
00:13:11,760 --> 00:13:16,760
If you're displeased, especially when you're meditating, that's a problem.

97
00:13:16,760 --> 00:13:17,760
That's not good.

98
00:13:17,760 --> 00:13:19,760
It's not wholesome.

99
00:13:19,760 --> 00:13:23,760
So even disliking meditation is a good example.

100
00:13:23,760 --> 00:13:29,760
The problem isn't the meditation, the problem is the disliking.

101
00:13:29,760 --> 00:13:37,760
The other thing it doesn't mean is that there's something wrong with the things that you like or dislike.

102
00:13:37,760 --> 00:13:41,760
So it can happen that a meditator will be afraid of pleasure.

103
00:13:41,760 --> 00:13:46,760
When they feel happy, they'll think, oh, there's something wrong here.

104
00:13:46,760 --> 00:13:49,760
This is, I must be attached to something.

105
00:13:49,760 --> 00:14:04,760
Or if a meditator feels displeased or feels displeased by something, there's pain or there's restlessness or some kind of agitation in the mind that makes them upset.

106
00:14:04,760 --> 00:14:07,760
I think there must be something wrong with their meditation.

107
00:14:07,760 --> 00:14:09,760
They must be doing something wrong.

108
00:14:09,760 --> 00:14:18,760
But the only thing they're doing wrong is reacting wrongly.

109
00:14:18,760 --> 00:14:22,760
Goodness and evil.

110
00:14:22,760 --> 00:14:29,760
Neither one exists inherently in our actions or speech.

111
00:14:29,760 --> 00:14:34,760
Even in our thoughts, they're called a kantuka kilesa.

112
00:14:34,760 --> 00:14:37,760
A kantuka means a visitor.

113
00:14:37,760 --> 00:14:39,760
They visit upon our minds.

114
00:14:39,760 --> 00:14:42,760
They invade our minds.

115
00:14:42,760 --> 00:14:51,760
They say, pabhasarang, bikowai, pabhasarang, idang, bikowai, jitang.

116
00:14:51,760 --> 00:14:59,760
This mind is radiant, brilliant, pure.

117
00:14:59,760 --> 00:15:08,760
But it's defiled by invading defilements.

118
00:15:08,760 --> 00:15:16,760
By these visiting defilements that come from time to time and end.

119
00:15:16,760 --> 00:15:25,760
Arise, not based, not inherent in the experiences or even in the mind, but they arise as a result.

120
00:15:25,760 --> 00:15:32,760
As a result of ignorance, as a result of delusion, as a result of improper attention.

121
00:15:32,760 --> 00:15:42,760
An improper grasp on our experience.

122
00:15:42,760 --> 00:15:48,760
So to be clear, it's actually not so much about it happiness or suffering.

123
00:15:48,760 --> 00:15:52,760
As I said at the beginning of the talk, our goal is happiness.

124
00:15:52,760 --> 00:15:58,760
But the practice has nothing to do with happiness or suffering.

125
00:15:58,760 --> 00:16:07,760
And as everything to do with our reactions, as everything to do with changing the way we relate to experience.

126
00:16:07,760 --> 00:16:10,760
So it has everything to do with goodness.

127
00:16:10,760 --> 00:16:13,760
That's why I always tell meditators, don't worry about happiness.

128
00:16:13,760 --> 00:16:17,760
Don't worry about being happy.

129
00:16:17,760 --> 00:16:19,760
Don't worry about am I happy?

130
00:16:19,760 --> 00:16:21,760
Am I unhappy?

131
00:16:21,760 --> 00:16:27,760
Put your nose to the grindstone and worry about having a good mind.

132
00:16:27,760 --> 00:16:38,760
And the good mind always brings happiness.

133
00:16:38,760 --> 00:16:41,760
Chitang dantang sukha vahang.

134
00:16:41,760 --> 00:16:43,760
The trained mind brings happiness.

135
00:16:43,760 --> 00:16:47,760
But to be clear what that means and a good mind,

136
00:16:47,760 --> 00:16:49,760
a good mind cannot be displeased.

137
00:16:49,760 --> 00:16:50,760
They can suffer.

138
00:16:50,760 --> 00:16:54,760
They can be physical pain.

139
00:16:54,760 --> 00:16:58,760
But the good mind, you'll know it.

140
00:16:58,760 --> 00:17:00,760
You'll know it by the purity of it.

141
00:17:00,760 --> 00:17:04,760
There will be no displeasure in that mind.

142
00:17:04,760 --> 00:17:10,760
There will either be pleasure or there will be calm.

143
00:17:10,760 --> 00:17:12,760
But there will also be purity.

144
00:17:12,760 --> 00:17:14,760
There will be no desire.

145
00:17:14,760 --> 00:17:17,760
Not even any liking of itself.

146
00:17:17,760 --> 00:17:21,760
A liking of the experience.

147
00:17:21,760 --> 00:17:25,760
The key is not the pleasure or the calm.

148
00:17:25,760 --> 00:17:36,760
The key is the purity.

149
00:17:36,760 --> 00:17:39,760
When we practice moment after moment,

150
00:17:39,760 --> 00:17:41,760
this is what we're trying to create.

151
00:17:41,760 --> 00:17:44,760
We're trying to create the pure state of mind

152
00:17:44,760 --> 00:17:49,760
that sees the experience as it is that is here is present.

153
00:17:49,760 --> 00:17:53,760
It's not in the past or the future.

154
00:17:53,760 --> 00:17:58,760
Caught up in concepts or views or beliefs.

155
00:17:58,760 --> 00:18:05,760
Identification or judgment.

156
00:18:05,760 --> 00:18:10,760
The mind that is not ensnared by the hindrances of the defilements,

157
00:18:10,760 --> 00:18:13,760
liking or disliking,

158
00:18:13,760 --> 00:18:20,760
worrying, agitation, restlessness, doubt, confusion, arrogance, conceit.

159
00:18:20,760 --> 00:18:24,760
It's a mind that is free from all of the above.

160
00:18:24,760 --> 00:18:38,760
It's a mind that is clear, calm, quiet, content.

161
00:18:38,760 --> 00:18:41,760
It's a mind that is mindful.

162
00:18:41,760 --> 00:18:43,760
It is the full mind.

163
00:18:43,760 --> 00:18:46,760
Mind, full meaning, full mind.

164
00:18:46,760 --> 00:18:49,760
Like the full moon in all its radiance.

165
00:18:49,760 --> 00:18:52,760
There's no half awareness.

166
00:18:52,760 --> 00:18:54,760
The mind is not flitting here and there.

167
00:18:54,760 --> 00:19:00,760
The mind is fixed and focused on the object like a full moon.

168
00:19:00,760 --> 00:19:05,760
No halfway.

169
00:19:05,760 --> 00:19:07,760
Worry very much about that mind.

170
00:19:07,760 --> 00:19:12,760
Not worry, but focus your attention very much on that mind,

171
00:19:12,760 --> 00:19:18,760
on attaining that mind, that state of mind, that moment,

172
00:19:18,760 --> 00:19:20,760
again and again.

173
00:19:20,760 --> 00:19:23,760
The moment where you experience something just as it is

174
00:19:23,760 --> 00:19:26,760
with no liking or dislike or none of the above.

175
00:19:26,760 --> 00:19:31,760
No defilement.

176
00:19:31,760 --> 00:19:37,760
And that's what brings true happiness.

177
00:19:37,760 --> 00:19:38,760
So there you go.

178
00:19:38,760 --> 00:19:43,760
There's the demo for tonight.

179
00:19:43,760 --> 00:19:56,760
Some thoughts on different types of mind.

180
00:19:56,760 --> 00:20:02,760
I have one question on this website today.

181
00:20:02,760 --> 00:20:05,760
Is it okay to allow your eyes to open a little bit

182
00:20:05,760 --> 00:20:09,760
when meditating as long as I am in a dark room?

183
00:20:09,760 --> 00:20:10,760
I mean, there's no law.

184
00:20:10,760 --> 00:20:13,760
There's no rule against meditating with your eyes opened.

185
00:20:13,760 --> 00:20:19,760
We tend to prefer having them closed.

186
00:20:19,760 --> 00:20:24,760
It's a little too distracting to have your eyes open.

187
00:20:24,760 --> 00:20:29,760
So as a means of training, closing your eyes is quite useful.

188
00:20:29,760 --> 00:20:30,760
But it's not a rule.

189
00:20:30,760 --> 00:20:59,760
I wouldn't worry about it in that way.

190
00:20:59,760 --> 00:21:07,760
Do we have any local questions?

191
00:21:07,760 --> 00:21:09,760
Local second-life questions.

192
00:21:09,760 --> 00:21:22,760
Live questions, I guess is the term.

193
00:21:22,760 --> 00:21:23,760
All right.

194
00:21:23,760 --> 00:21:32,760
Well, thank you all for coming out.

195
00:21:32,760 --> 00:21:48,760
We're seeing you all a good night and good practice.

