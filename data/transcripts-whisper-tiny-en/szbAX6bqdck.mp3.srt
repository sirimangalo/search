1
00:00:00,000 --> 00:00:09,480
Hello, today we will be starting a series of videos discussing core concepts in Buddhism.

2
00:00:09,480 --> 00:00:14,080
So trying to fill in some of the gaps that have been left by my other videos which have

3
00:00:14,080 --> 00:00:19,640
mostly been centered around meditation and core concepts dealing with meditation.

4
00:00:19,640 --> 00:00:25,160
So we're going to back off a little bit and deal with those concepts that I may have

5
00:00:25,160 --> 00:00:27,840
taken for granted in other videos.

6
00:00:27,840 --> 00:00:36,000
And some of the concepts, at least in the beginning, may seem a little bit peripheral

7
00:00:36,000 --> 00:00:37,000
to the main goal.

8
00:00:37,000 --> 00:00:44,440
We're going to start at some very fundamental concepts that may not have much to do intrinsically

9
00:00:44,440 --> 00:00:50,000
with meditation, but kind of set the stage and are the way that the Buddha would set

10
00:00:50,000 --> 00:00:55,520
the stage himself to begin to discuss the practice of meditation.

11
00:00:55,520 --> 00:01:00,080
So before getting into deep concepts like the Four Noble Truths, which we will get into

12
00:01:00,080 --> 00:01:08,720
later on in this series, we're going to begin to discuss how one who has maybe no knowledge

13
00:01:08,720 --> 00:01:15,920
of Buddhism or even no prior inclination towards the spiritual life might begin to cultivate

14
00:01:15,920 --> 00:01:21,800
spirituality and cultivate the path leading to enlightenment.

15
00:01:21,800 --> 00:01:27,280
So the first concept that we want to discuss today is called Dana, or Giving in English.

16
00:01:27,280 --> 00:01:32,680
And it may seem a little bit, as I said, peripheral or even extraneous, to Buddhism, to

17
00:01:32,680 --> 00:01:37,160
have to practice charity or generosity.

18
00:01:37,160 --> 00:01:47,920
But given the core goal in Buddhism is that of letting go and giving up, it's actually

19
00:01:47,920 --> 00:01:56,080
used by the Buddha as an introduction or an entry point by which to begin to cultivate this

20
00:01:56,080 --> 00:02:00,400
mine state of letting go, that is the practice of giving.

21
00:02:00,400 --> 00:02:05,920
So for someone who is new to meditation practice, the practice of giving can be incredibly

22
00:02:05,920 --> 00:02:14,440
supportive to create the mine states or the inclination towards giving up.

23
00:02:14,440 --> 00:02:22,280
So giving in Buddhism in that sense is as important as it may be, is not designed with

24
00:02:22,280 --> 00:02:26,040
the recipient in mind or for the benefit of the recipient.

25
00:02:26,040 --> 00:02:29,120
The idea is to benefit the giver, benefit the donor.

26
00:02:29,120 --> 00:02:34,080
And therefore, it's something to be done as a matter of course, rather than something

27
00:02:34,080 --> 00:02:38,560
that one goes out of one's way to practice intentionally.

28
00:02:38,560 --> 00:02:43,480
But the point being that a person who is giving, a person who is inclined to give, is

29
00:02:43,480 --> 00:02:45,240
more likely to give up.

30
00:02:45,240 --> 00:02:53,280
And a person who is looking to give up will, by nature, be generous and giving.

31
00:02:53,280 --> 00:02:59,520
And so we're trying to cultivate this quality of mind which is a giving, a generous state

32
00:02:59,520 --> 00:03:08,400
of mind which is in very much a beginning of giving up.

33
00:03:08,400 --> 00:03:18,680
And this is actually something that we find a lack of in places where Buddhism has come

34
00:03:18,680 --> 00:03:23,480
for the first time and people have begun to examine the text because often they will jump

35
00:03:23,480 --> 00:03:29,280
right into the core concepts like practice of insight meditation or the deep philosophical

36
00:03:29,280 --> 00:03:35,160
teachings like the four noble truths or dependent origination, the eight full noble path

37
00:03:35,160 --> 00:03:42,800
without stepping back and actually understanding the qualities of mind and the virtues

38
00:03:42,800 --> 00:03:49,320
that we're trying to cultivate and without imbibing or internalizing these qualities.

39
00:03:49,320 --> 00:03:52,400
So the first one here is generosity.

40
00:03:52,400 --> 00:03:53,560
This is what we're focusing on.

41
00:03:53,560 --> 00:04:00,440
Now, the means of practicing generosity, we have to take into account three factors.

42
00:04:00,440 --> 00:04:06,080
The first factor is the intention of the giver.

43
00:04:06,080 --> 00:04:08,600
The second factor is the object being given.

44
00:04:08,600 --> 00:04:14,360
And the third factor is the recipient of the gift.

45
00:04:14,360 --> 00:04:18,360
And all three of these play an important role in determining what we mean by giving

46
00:04:18,360 --> 00:04:19,360
in Buddhism.

47
00:04:19,360 --> 00:04:23,640
So we understand that giving is important, it's a beginning to giving up.

48
00:04:23,640 --> 00:04:24,720
How are we going to practice it?

49
00:04:24,720 --> 00:04:29,040
Well, first we have to focus very much on our intentions for giving.

50
00:04:29,040 --> 00:04:36,400
We can't give out a fear or out of spite or out of desire to get something back from

51
00:04:36,400 --> 00:04:38,280
the recipient.

52
00:04:38,280 --> 00:04:44,160
We have to give if we want it to benefit us and to be a support for our spiritual practice.

53
00:04:44,160 --> 00:04:48,600
We have to give out of an intention to better ourselves, to become better people.

54
00:04:48,600 --> 00:04:53,640
We have to want to bring happiness to ourselves and we want to create harmony between ourselves

55
00:04:53,640 --> 00:04:55,040
and other people.

56
00:04:55,040 --> 00:04:59,600
We want to cultivate this wholesome state of mind.

57
00:04:59,600 --> 00:05:01,600
And so the intentions in giving are very important.

58
00:05:01,600 --> 00:05:05,800
At the moment of giving, before you're giving, when you're thinking, even thinking about

59
00:05:05,800 --> 00:05:13,200
giving, and even after you've given, not to feel regret, remorse and having lost something,

60
00:05:13,200 --> 00:05:16,120
for example, having had to renounce something.

61
00:05:16,120 --> 00:05:19,680
So we have to cultivate these states and get into the habit of it.

62
00:05:19,680 --> 00:05:26,200
So generosity is something that is actually going to be important throughout our Buddhist practice.

63
00:05:26,200 --> 00:05:30,480
The second aspect is the aspect of the object itself.

64
00:05:30,480 --> 00:05:34,960
And this can be broken up into two types.

65
00:05:34,960 --> 00:05:42,400
There's the Amisadana, which is a physical or a material object, material gift.

66
00:05:42,400 --> 00:05:43,400
And the other one is a spirit.

67
00:05:43,400 --> 00:05:50,720
So you have Dhamma Dhamma, or some kind of spiritual or immaterial mental support.

68
00:05:50,720 --> 00:06:00,280
So this could be giving your time to someone or your energy or effort or your knowledge.

69
00:06:00,280 --> 00:06:05,280
And most importantly, of course, your wisdom, so this would be teaching the Dhamma or the

70
00:06:05,280 --> 00:06:08,520
meditation practice to others.

71
00:06:08,520 --> 00:06:16,240
And as I said, we often see that a lack of this in Buddhist practitioners, even those who

72
00:06:16,240 --> 00:06:20,880
are dedicated very much to the meditation practice, they can often become quite selfish

73
00:06:20,880 --> 00:06:26,280
in terms of trying to support their own practice and better their own practice, and shunning

74
00:06:26,280 --> 00:06:34,640
the idea of sharing the meditation practice with others, or even shunning the idea of being

75
00:06:34,640 --> 00:06:43,840
generous or supporting others or working to help others, even in a material form.

76
00:06:43,840 --> 00:06:49,000
And so again, we have to understand that this is an important aspect of our practice, the

77
00:06:49,000 --> 00:06:51,080
willingness to give to others.

78
00:06:51,080 --> 00:06:54,240
It's something that we should do as a matter of course, when people ask for our help,

79
00:06:54,240 --> 00:06:58,680
when people want our support, they say, hey, I heard your practicing meditation, could

80
00:06:58,680 --> 00:07:00,440
you show it to me?

81
00:07:00,440 --> 00:07:04,960
When we say no, when we say, I'm sorry, I'm not a teacher, and we try to brush them

82
00:07:04,960 --> 00:07:09,520
off, or we try to run away from these sorts of responsibilities, this is a sort of clinging.

83
00:07:09,520 --> 00:07:13,480
It's something that is not going to support our practice, but we'll inhibit our practice,

84
00:07:13,480 --> 00:07:16,120
because it's stopping us from letting go.

85
00:07:16,120 --> 00:07:19,160
So giving is important on both sides.

86
00:07:19,160 --> 00:07:22,160
On the material, in terms of the object on the material side, we have to be willing to

87
00:07:22,160 --> 00:07:26,360
give materials, share the things that we have, and on the spiritual side, to share the

88
00:07:26,360 --> 00:07:28,800
knowledge and wisdom that we have.

89
00:07:28,800 --> 00:07:35,960
Now the recipient, the third aspect, is important in so far as first their need.

90
00:07:35,960 --> 00:07:40,680
So if it's someone who is in need of your help, and their worthiness, if it's someone

91
00:07:40,680 --> 00:07:42,600
who is worthy of your help.

92
00:07:42,600 --> 00:07:46,680
So for need, we're talking about people who are maybe hungry, giving them food, or

93
00:07:46,680 --> 00:07:51,200
people who are need of shelter, giving them shelter, people who need your help.

94
00:07:51,200 --> 00:07:56,240
And it's not so much them asking you for help, but it's actually their need, and this

95
00:07:56,240 --> 00:08:00,280
takes wisdom, this takes discrimination, you have to be clear whether they need it.

96
00:08:00,280 --> 00:08:01,760
Why is this important?

97
00:08:01,760 --> 00:08:07,720
Because it's important to create this positive state of mind in your own mind.

98
00:08:07,720 --> 00:08:12,400
If you give something to someone thinking they didn't really need it, but just because

99
00:08:12,400 --> 00:08:15,440
they were bugging you, then it doesn't really help your state of mind except to get

100
00:08:15,440 --> 00:08:17,480
rid of the annoying person or something.

101
00:08:17,480 --> 00:08:21,160
It doesn't help you to let go so much.

102
00:08:21,160 --> 00:08:25,440
We give to those recipients who are in need, it makes us feel confident, it makes us feel

103
00:08:25,440 --> 00:08:26,440
good about ourselves.

104
00:08:26,440 --> 00:08:28,960
These are the kinds of things that support our practice.

105
00:08:28,960 --> 00:08:34,640
It's like putting oil in your changing the oil in your engine, and it makes everything

106
00:08:34,640 --> 00:08:37,920
run smoother, or filling up your gas tank.

107
00:08:37,920 --> 00:08:43,280
All of these deep concepts, like the form of a truth or something, it's like this great

108
00:08:43,280 --> 00:08:49,000
sports car, and the ideas of giving, things like giving and morality, which we'll talk

109
00:08:49,000 --> 00:08:54,440
about next, are sort of like filling up your gas tank because they give you the power to

110
00:08:54,440 --> 00:08:59,240
take the car, to take the vehicle, or to follow the path.

111
00:08:59,240 --> 00:09:03,080
In order to get that, we have to give to those who are going to inspire us.

112
00:09:03,080 --> 00:09:04,520
We're feel happy having given to them.

113
00:09:04,520 --> 00:09:09,480
We won't feel skeptical as to whether they're going to use it for bad purposes, like

114
00:09:09,480 --> 00:09:15,080
giving money to a drug addict or so on.

115
00:09:15,080 --> 00:09:23,760
As far as need and as far as worth, a person who is immoral, the benefit of giving to them,

116
00:09:23,760 --> 00:09:30,320
of course, is far lower because you're never going to give rise to the states of confidence

117
00:09:30,320 --> 00:09:37,640
in the states of joy and happiness and peace of mind that comes from giving to spiritual

118
00:09:37,640 --> 00:09:44,040
people, from giving, from supporting causes that are of benefit to the world and so on.

119
00:09:44,040 --> 00:09:45,840
This is just a brief overview.

120
00:09:45,840 --> 00:09:50,640
We could talk a lot more about giving, but basically what I want to impress is the importance

121
00:09:50,640 --> 00:09:52,640
and the connection in bitterness.

122
00:09:52,640 --> 00:09:57,040
This is the first thing that the Buddha would talk about when he gave a sequence of teachings.

123
00:09:57,040 --> 00:09:58,280
It's called the Anupubhikata.

124
00:09:58,280 --> 00:10:03,240
The first thing he would talk about in order, the first thing he would talk about is the practice

125
00:10:03,240 --> 00:10:04,240
of giving.

126
00:10:04,240 --> 00:10:08,840
So it's an important first step and it's something to keep in mind throughout our practice

127
00:10:08,840 --> 00:10:16,120
that if we're not giving a charitable, if we're not able to sacrifice our own well-being,

128
00:10:16,120 --> 00:10:21,600
our own desires and wants, then very difficult for us to come to let go.

129
00:10:21,600 --> 00:10:27,360
It's part and parcel with the ability to free ourselves from clinging and therefore

130
00:10:27,360 --> 00:10:28,360
suffering.

131
00:10:28,360 --> 00:10:29,520
So thank you for tuning in.

132
00:10:29,520 --> 00:10:51,960
This is the first in our video.

