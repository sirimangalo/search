1
00:00:00,000 --> 00:00:06,280
Kevin has a question, what are the major differences in the rules that must be

2
00:00:06,280 --> 00:00:11,880
followed by Samaneera and a fully ordained be true? Is there a different

3
00:00:11,880 --> 00:00:35,320
good idea? Hardly, really, but I'm currently trying to ordained as a

4
00:00:35,320 --> 00:00:43,080
seminar, and I believe Yen's would also like to as well at some point. It's a bit

5
00:00:43,080 --> 00:00:50,440
trickier being a female, but from what I understand, you're asking about

6
00:00:50,440 --> 00:01:01,120
bikus, so that's the male, 227 rules, females have 311, but as a seminar you take on

7
00:01:01,120 --> 00:01:12,280
two more precepts, formally. The big one is that you're no longer handling money.

8
00:01:12,280 --> 00:01:20,240
Another one is kind of an extension of no more sort of entertainments, and

9
00:01:20,240 --> 00:01:26,840
actually it's only had one really. The seventh one becomes two. All they do is

10
00:01:26,840 --> 00:01:31,480
split it in half. If you're talking about it from the eight precepts, you're

11
00:01:31,480 --> 00:01:36,400
really only had one. That's a very important step, and it's an important point

12
00:01:36,400 --> 00:01:40,680
that for any monk who claims that there's nothing wrong with using money, it's

13
00:01:40,680 --> 00:01:48,640
the only thing that defines a difference between a yogi and one who's gone

14
00:01:48,640 --> 00:01:57,100
for it. But I think the important thing if you're doing as a

15
00:01:57,100 --> 00:02:02,800
seminary, maybe with the exception of some cases is Pontius and Sri Lanka,

16
00:02:02,800 --> 00:02:11,360
people stay as seminary so they can live the life of a recluse and still cook

17
00:02:11,360 --> 00:02:19,600
food. But I think in general if your or Dana's a seminary, you're becoming a

18
00:02:19,600 --> 00:02:27,800
novice to become a biku and therefore you're somewhat or you're expected to

19
00:02:27,800 --> 00:02:34,240
be training as a biku while you're novice so that the day you do or Dana's a

20
00:02:34,240 --> 00:02:40,320
biku, it's not a huge jump, a huge shift and actually I was just talking to

21
00:02:40,320 --> 00:02:50,040
Jens today, Jassa. Sorry, Jassa. I remember it again, you had more heading.

22
00:02:50,040 --> 00:02:56,560
It's just talking about ordaining just a little bit today with Jens and Jassa.

23
00:02:56,560 --> 00:03:14,560
Yes, sorry. And you're saying the training actually starts now for us being here

24
00:03:14,560 --> 00:03:22,840
even as yogis with the intention, the volition to ordain should start now

25
00:03:22,840 --> 00:03:36,000
gearing the mind towards kind of relinquishing a lot of central attachments and

26
00:03:36,000 --> 00:03:44,160
for me most recently it's been tea, just even though I can make myself a cup of tea I

27
00:03:44,160 --> 00:03:52,560
have now officially, hopefully this will be, you know, a very committed, I've really committed

28
00:03:52,560 --> 00:03:59,000
myself to know more making myself tea. If I'm offered a cup of tea, that's one thing,

29
00:03:59,000 --> 00:04:06,560
but Pontis would really like people to not be taking caffeine and I really, really

30
00:04:06,560 --> 00:04:15,680
enjoy tea and Sri Lanka is famous for its tea, it's probably the best tea in the world.

31
00:04:15,680 --> 00:04:22,560
So it's one step at a time. We get so caught up in the little things when I was in

32
00:04:22,560 --> 00:04:26,560
Thailand it was the same sort of things, but it was much later because of course the monks

33
00:04:26,560 --> 00:04:32,160
in Thailand are mostly not keeping the rules and so I had just pulled myself up by the

34
00:04:32,160 --> 00:04:39,080
bootstraps, one rule at a time, I was like, just okay, so today no tea, oh I'm

35
00:04:39,080 --> 00:04:43,480
going to do it, no coffee today, you know, it was that kind of thing. We're going to

36
00:04:43,480 --> 00:04:51,600
stop having coffee, stop having coffee, no coffee made, no coffee, no sugar, no more and

37
00:04:51,600 --> 00:04:57,760
then the tetrapags, no milk, sorry milk, tetrapags, no more soy milk in the evening or

38
00:04:57,760 --> 00:05:04,880
maybe what are we going to do and then it was just surviving off of cold alms food.

39
00:05:04,880 --> 00:05:12,320
That also plays at some monks, mum was really funny because they were so cold and very

40
00:05:12,320 --> 00:05:17,680
poor alms around the people that were not really interested in what we did, so we were

41
00:05:17,680 --> 00:05:28,840
just ready to support and sometimes all we would get is these packages of mama noodles, mama,

42
00:05:28,840 --> 00:05:33,920
just like Mr. noodles in America and you know I can go back to the monastery and collect

43
00:05:33,920 --> 00:05:40,480
together this rotten firewood because it's so wet and cold and it's been like an hour

44
00:05:40,480 --> 00:05:47,480
late in a fire to cook these, not cook, but to reconstitute, to get boiling water and reconstitute,

45
00:05:47,480 --> 00:05:56,400
these already cooked noodles and until these supporters of my intramcy and my came up and

46
00:05:56,400 --> 00:06:04,280
that's Ann, what are you doing? I'm cooking mama, mama doodle, but it was really hard

47
00:06:04,280 --> 00:06:08,960
going, so that was all that we had and then having to give up on top of that, all of

48
00:06:08,960 --> 00:06:16,720
this other stuff and Brian was very upset, Brian, you heard about Brian this guy from

49
00:06:16,720 --> 00:06:23,760
the Dutch guy, he's actually contacted us again to understand, he was so I upset at me,

50
00:06:23,760 --> 00:06:27,560
come on, we can have something in the evening, what can we have, the soy milk, we can have

51
00:06:27,560 --> 00:06:33,800
the soy milk, no, no, no, no, no, no, no, oh and it was so, because it was very difficult,

52
00:06:33,800 --> 00:06:38,040
we didn't have any, it was just the two of us and no one was offering us anything, there

53
00:06:38,040 --> 00:06:42,640
was no one, there was certainly no rice school in the morning, off on on the ground, you

54
00:06:42,640 --> 00:06:48,560
know, basic, quite a bit of honestly, it's true, but most of it was just like pork rind

55
00:06:48,560 --> 00:06:57,860
or sweets and mama doodles, so it wasn't, it certainly wasn't a good protein count or

56
00:06:57,860 --> 00:07:05,480
vitamin count and then we were there in the evening alone, so I made some concessions,

57
00:07:05,480 --> 00:07:10,200
we had these those belly fruits, you know, those little round fruits that you were going

58
00:07:10,200 --> 00:07:19,040
to buy at Anarada Pura, kind of you were going to buy, little round slices in the

59
00:07:19,040 --> 00:07:24,240
dried from making tea, they're big in Thailand, and we had some of those and I said okay

60
00:07:24,240 --> 00:07:31,120
so we'll boil up some of these, and we sat there, the two of us boiling up these bell

61
00:07:31,120 --> 00:07:36,920
fruits and making tea out of that and drinking it, because it's actually quite good for

62
00:07:36,920 --> 00:07:41,240
you, the problem is that you really shouldn't be drinking it daily unless you're sick,

63
00:07:41,240 --> 00:07:48,880
but we were bordering on sick with that sort of lifestyle, but it was fun, but I remember

64
00:07:48,880 --> 00:07:52,560
that period because it was a real challenge, the hardest part was not that, the hardest

65
00:07:52,560 --> 00:07:59,120
part was that we were so used to having soy amel, having tea and having these luxuries,

66
00:07:59,120 --> 00:08:06,240
getting what you want, and having some love, until I told the matrices that we were leaving

67
00:08:06,240 --> 00:08:10,640
and then they made me bake in eggs and just luxurate food because they were so happy

68
00:08:10,640 --> 00:08:20,840
that we were leaving, that was horrible, this is stories of my monastic life, new

69
00:08:20,840 --> 00:08:33,640
eggs, amazing how the mind can cling to it's comforts, yeah actually it's a bit ridiculous

70
00:08:33,640 --> 00:08:41,640
but like be walking in meditation, I'll think okay no more tea or what am I going to give

71
00:08:41,640 --> 00:08:46,160
up today, something, it's like lent, I used to be Catholic and you have to give up something

72
00:08:46,160 --> 00:08:53,020
for Lent, but here it's like okay, it's my Lent for today, Lent on top of Lent, it's

73
00:08:53,020 --> 00:09:00,880
just adding all the things, but I do watch this sort of inner panic, inner fear arise

74
00:09:00,880 --> 00:09:06,840
when I say to myself oh I'm going to give up and it's funny because before I decided

75
00:09:06,840 --> 00:09:16,180
okay no more tea, like making tea for myself, I had this mindset, I go God that's really

76
00:09:16,180 --> 00:09:22,260
going to suck. I'm going to be watching Kathy Maker T. She's going to be like,

77
00:09:22,260 --> 00:09:35,540
oh it's so good and tah tah tah. And I'm talking about her T. And you really tell her

78
00:09:35,540 --> 00:09:51,740
that. I went the restaurant there the morning. My god this is so good. I did sit in the

79
00:09:51,740 --> 00:09:55,700
restaurant the other day saying how great my cup of tea was but I didn't actually

80
00:09:55,700 --> 00:09:59,820
know at that point that you'd given up tea else I wouldn't have said anything but it

81
00:09:59,820 --> 00:10:06,620
was a very very good cup of tea actually exceptionally good. These are attachments.

82
00:10:06,620 --> 00:10:12,340
These are attachments. Well I was going to say that I tend to find when you give

83
00:10:12,340 --> 00:10:16,860
something up that it's better not to think of it as oh now I'm going to give this up

84
00:10:16,860 --> 00:10:22,980
like but to think of it as okay now if I decide I'm not having this anymore then I

85
00:10:22,980 --> 00:10:30,060
gain the sense of strength of not needing to worry about this thing and just to

86
00:10:30,060 --> 00:10:34,860
say like okay I'm no longer going to feel like lust for this thing or I'm

87
00:10:34,860 --> 00:10:37,860
going to even think about it I'm just not going to have it anymore. That's an

88
00:10:37,860 --> 00:10:41,340
excellent point. What are the benefits? I mean that's the whole point. It's a

89
00:10:41,340 --> 00:10:45,860
wonderful benefit. Yeah you have to look at it more like that than when you do

90
00:10:45,860 --> 00:10:53,180
look at it like that it is more of a benefit. It takes away so much stress like for

91
00:10:53,180 --> 00:10:59,380
example here we don't have a previous meditation sense of state that you

92
00:10:59,380 --> 00:11:05,380
were allowed to have over teen in the evening or milk or soil milk or the

93
00:11:05,380 --> 00:11:10,660
lots of other things and actually you would just get a lot more cravings for

94
00:11:10,660 --> 00:11:14,260
these things because you were allowed them and here when you just say to

95
00:11:14,260 --> 00:11:21,020
yourself okay I'm not going to have them then actually you don't feel so much

96
00:11:21,020 --> 00:11:26,580
craving for them and you don't yeah it's something another thing you don't

97
00:11:26,580 --> 00:11:30,740
have to worry about. It's another great happiness that was what I wanted to say

98
00:11:30,740 --> 00:11:35,420
is that the power that you feel you know the stories that I was telling you

99
00:11:35,420 --> 00:11:39,620
about this guy who I was telling you we were telling the Sri Lanka woman but

100
00:11:39,620 --> 00:11:45,900
this man who threw his pint pot of beans into the river and said I've conquered

101
00:11:45,900 --> 00:11:52,380
I have conquered because he conquered the power at the very least I mean the

102
00:11:52,380 --> 00:12:00,460
power that you feel at not giving in to your desires to be free from your

103
00:12:00,460 --> 00:12:04,580
desires but that's really an important point is that freedom from desires brings

104
00:12:04,580 --> 00:12:13,460
happiness it brings clarity of mind and it brings joy it brings true

105
00:12:13,460 --> 00:12:17,620
unadulterated bliss that we don't realize we think it's the clinging and it's

106
00:12:17,620 --> 00:12:21,700
the craving it's the getting that leads to happiness but as the Buddha said

107
00:12:21,700 --> 00:12:27,100
for every desire that is let go of happiness is one he who would all happiness

108
00:12:27,100 --> 00:12:38,940
have must with all lust be done but the interesting thing is I'm sorry the mind is

109
00:12:38,940 --> 00:12:45,420
very very tricky it doesn't matter if it's tea coffee or whatever like when I

110
00:12:45,420 --> 00:12:52,860
did the 21 day course here we knew five o'clockish I'd have fruit juice and

111
00:12:52,860 --> 00:12:58,060
the mind would start at four thirty it's that thinking of fruit juice instead

112
00:12:58,060 --> 00:13:04,300
visualizing and can really see what's happening in the mind and it wouldn't

113
00:13:04,300 --> 00:13:08,900
let go like okay now you're saying anything that goes okay I'm gonna not have

114
00:13:08,900 --> 00:13:14,580
coffee I'm gonna have tea and you start craving for fruit juice it's just simple

115
00:13:14,580 --> 00:13:20,220
little things and it's so interesting to watch and it's so difficult to let

116
00:13:20,220 --> 00:13:26,340
it off that's a big reason I mean the monastic life really does hope that's one

117
00:13:26,340 --> 00:13:31,100
really good way that it helps is that no you may not have fruit juice you may

118
00:13:31,100 --> 00:13:36,100
not get anything you may not even get honest but I mean to have that fear in

119
00:13:36,100 --> 00:13:41,300
your mind every year it's really a challenge and there are times where you go

120
00:13:41,300 --> 00:13:46,140
without food I mean you it's kind of you feel ashamed as well because you're

121
00:13:46,140 --> 00:13:51,660
so weak inside but but but the seeing I mean and this is another point is

122
00:13:51,660 --> 00:13:55,620
that because you're giving up this and nothing you see it more clearly

123
00:13:55,620 --> 00:14:01,380
never said but for sure this that would be a good example of the importance

124
00:14:01,380 --> 00:14:06,740
of the benefit of ordaining because and you got no choice there's no there's

125
00:14:06,740 --> 00:14:10,420
no wishing for anything because if you don't get it you know I mean and that's

126
00:14:10,420 --> 00:14:15,240
really why the rules are so comprehensive that tea should only be as a monk

127
00:14:15,240 --> 00:14:19,420
tea should only be for medicine because otherwise that's how it goes you know

128
00:14:19,420 --> 00:14:22,220
you're not allowed you don't get fruit juice well I'll have some tea instead

129
00:14:22,220 --> 00:14:26,300
because tea I can keep forever because it's Buddha said tea can be kept forever

130
00:14:26,300 --> 00:14:32,100
but it can only be kept forever if it's a medicine so it's it's blocking up

131
00:14:32,100 --> 00:14:36,660
all the holes I suppose but in the end there's the other point of what you're

132
00:14:36,660 --> 00:14:40,540
saying is that it's really guard the mind though it does nothing to do with

133
00:14:40,540 --> 00:14:45,260
what rules you can't but what you abstain from and that's a good point of

134
00:14:45,260 --> 00:14:49,900
Kathy brought up as well you have to teach your mind what is truly a benefit

135
00:14:49,900 --> 00:14:55,540
you can't just say no no no no no no forbidden forbidden forbidden forbidden

136
00:14:55,540 --> 00:15:01,660
the mind will not accept that the mind can't accept that what it likes is is a

137
00:15:01,660 --> 00:15:06,940
cause for suffering you have to find it true happiness and bring it true

138
00:15:06,940 --> 00:15:12,060
happiness so you have to see these things there's a cause for happiness

139
00:15:12,060 --> 00:15:17,740
I think we've moved quite away from that whatever

140
00:15:17,740 --> 00:15:24,060
the question was the question was the difference she knows samanir and

141
00:15:24,060 --> 00:15:28,300
there's a good example there's a good difference

142
00:15:28,300 --> 00:15:32,320
samanirs are are still in it whatever they have a different

143
00:15:32,320 --> 00:15:37,580
than yes samanir are still can keep food for example still can cook food

144
00:15:37,580 --> 00:15:42,420
still can have fruit juice of their own at five I mean nagasena was having

145
00:15:42,420 --> 00:15:46,580
some it seemed like some trouble with that like he was able to take what he

146
00:15:46,580 --> 00:15:53,620
wants and so on and that seemed to be a distraction for him it's a real

147
00:15:53,620 --> 00:15:57,860
encouraging thing to to not have that ability it's a test and you're

148
00:15:57,860 --> 00:16:03,380
actually able to test yourself it's just too much convenience to do otherwise

149
00:16:03,380 --> 00:16:06,980
and that's why what you're saying I think is as well yes they only have one

150
00:16:06,980 --> 00:16:12,540
extra rule so they only have a total of 10 rules that they have to keep but for

151
00:16:12,540 --> 00:16:17,780
sure they should be training in all the rest of the rules to to some extent and

152
00:16:17,780 --> 00:16:22,580
so we start novices off by keeping the 75 safeties and we'll have them so I had

153
00:16:22,580 --> 00:16:26,860
nagasena start to memorize them but the most important is that you're

154
00:16:26,860 --> 00:16:31,300
keeping keeping the safeties which are considered to be appropriate for

155
00:16:31,300 --> 00:16:41,940
novices as well and I must say that if I end up ordaining as a michi one of the

156
00:16:41,940 --> 00:16:49,780
real pitfalls of that is that I still have that option technically to you know make

157
00:16:49,780 --> 00:16:57,820
food or drink or buy food you know the restaurant is amazing yeah even handle

158
00:16:57,820 --> 00:17:05,140
money I could you know I could I could give up all my money I could say oh I'm

159
00:17:05,140 --> 00:17:12,140
not gonna make myself food but what I find is that's much harder say at a

160
00:17:12,140 --> 00:17:16,500
monastery like this where you don't even have the option of soy milk in the

161
00:17:16,500 --> 00:17:24,740
afternoon cheese and chocolate I mean that's totally the question when it's not

162
00:17:24,740 --> 00:17:30,540
even an option it's much easier to just let go of it but when the stuff is

163
00:17:30,540 --> 00:17:36,820
sitting there and here it's not cheese and chocolate it's kind of liquefied

164
00:17:36,820 --> 00:17:41,380
cheese and chocolate in a way they've got this Viva stuff it's popular and the

165
00:17:41,380 --> 00:17:49,420
Nilo chocolate drink and I don't know what they put in it but maybe it's somewhat

166
00:17:49,420 --> 00:17:55,380
addictive or something it tastes good and then there's sugar and sweets of

167
00:17:55,380 --> 00:18:03,260
course even on all the sweet there's lots of sweets to tempt but I think the

168
00:18:03,260 --> 00:18:11,900
Vineas like such a blessing to be you know hopefully I will actually be able to

169
00:18:11,900 --> 00:18:19,340
fully redeem and have such a blessing that it's like no it's not even an

170
00:18:19,340 --> 00:18:26,540
option anymore like you it's like a protection in a way it's like not because

171
00:18:26,540 --> 00:18:34,820
much easier to let go whereas I know maybe it is it's good to exert the extra

172
00:18:34,820 --> 00:18:39,420
effort and say okay well it is an option right now and I'm choosing not to but

173
00:18:39,420 --> 00:18:43,860
it's much harder I think I mean and then there's the cynical other side of it

174
00:18:43,860 --> 00:18:48,820
is that it's easy to get egotistical about your rules look at me I have all

175
00:18:48,820 --> 00:18:53,340
these I look at how good I am at keeping these rules and so I'm going to caution

176
00:18:53,340 --> 00:18:59,660
against that so the only final word is that it's important to keep them in

177
00:18:59,660 --> 00:19:03,180
perspective that the Vineas not the most important and the most important

178
00:19:03,180 --> 00:19:06,700
thing is that the person has decided to go forth so whether they're a

179
00:19:06,700 --> 00:19:11,340
salmoneur or a be cool most important is that they train their minds or even

180
00:19:11,340 --> 00:19:16,140
whether it's a lay person the most important is the guarding of the mind

181
00:19:16,140 --> 00:19:20,420
there was this monk who had we had this one already this monk who had an

182
00:19:20,420 --> 00:19:23,540
abidama teacher who taught him all the abidama and the monk who had the wind and

183
00:19:23,540 --> 00:19:27,300
and he also had a windy teacher who taught him all the windy rules they thought

184
00:19:27,300 --> 00:19:31,540
it's just hundreds and hundreds of things I have to remember I have to keep

185
00:19:31,540 --> 00:19:37,060
these that I can't even move my hand or I can't even move my hand without

186
00:19:37,060 --> 00:19:43,180
breaking a row and I'm going to say well can you guard one thing and he said

187
00:19:43,180 --> 00:19:49,780
guard your mind if you guard your mind none of the none of the rest of this is of any

188
00:19:49,780 --> 00:19:56,340
important is once you can guard the mind and understand experience understand

189
00:19:56,340 --> 00:20:03,020
things from an experiential and let go and see things clearly as what they are

190
00:20:03,020 --> 00:20:07,780
and then there's no difference so the essence is the same person has gone

191
00:20:07,780 --> 00:20:13,060
forth but the difference with the beaucoup is that they've been accepted into

192
00:20:13,060 --> 00:20:20,500
the community and this is important another level as for communal harmony

193
00:20:20,500 --> 00:20:25,380
there's a person may use money without being defiled but when a community uses

194
00:20:25,380 --> 00:20:30,700
money you're just asking for trouble and you get it there's a lot of corruption

195
00:20:30,700 --> 00:20:37,140
and new monks become powerful and corrupt and to cause problems and destroy

196
00:20:37,140 --> 00:21:06,220
monsters and so yeah different different really

