1
00:00:00,000 --> 00:00:11,000
When I meditate, words that I say in my mind kind of drop off, so to speak, and it's just like me basically visualizing what my body is doing.

2
00:00:11,000 --> 00:00:18,000
I don't know if this is the right way, and that must I keep saying words in my mind like rising and falling?

3
00:00:18,000 --> 00:00:35,000
It would certainly be better in the beginning if you really say the words in your mind, because that makes sure that you know what is going on in the present moment.

4
00:00:35,000 --> 00:00:55,000
On the larger scale, it is the most important that you know what is going on, even if you don't find the correct word in the same moment.

5
00:00:55,000 --> 00:01:19,000
I'm not 100% sure what you are meaning with kind of drop off, so I'm not really sure what's going on, but as long as you are as you know that there is the rising and falling, that's the most important.

6
00:01:19,000 --> 00:01:34,000
If you are able to say rising and falling in your mind, that's much better.

7
00:01:34,000 --> 00:01:51,000
You have to admit the difficulty that you're faced with when it's just me basically visualizing what my body is doing, because this kind of thought becomes reinforced in the mind that it's me visualizing.

8
00:01:51,000 --> 00:02:09,000
No, it doesn't even need to become reinforced. This kind of mind is our default. We naturally assume things to be self, to be me, to be mind, to be a possession.

9
00:02:09,000 --> 00:02:22,000
You know, we cling to things. Lumpa Jodog is quite clear about this. If you're not mindful, even though you know something, he said, delusion knows things is the best at knowing things.

10
00:02:22,000 --> 00:02:39,000
Delusion knows things incredibly well, but it knows things based on views, so it has views about them. It knows things based on conceit, so it becomes conceited about them, and it knows things based on craving.

11
00:02:39,000 --> 00:02:55,000
It has craving about them. The view is the idea that this is the self. This is mine, or this is a part of who I am. This experience has a eye to it.

12
00:02:55,000 --> 00:03:22,000
Concede is taking it to be, or taking it to imply, I am this. Putting yourself in the object, in terms of it being a sign that you're a good person, or that you're a good part of you, or it's a bad part of you.

13
00:03:22,000 --> 00:03:35,000
You're having conceit about the object as being a good thing or a bad thing. And then craving, saying it's mine. So clinging to it as being a good thing or a bad thing, as liking it or disliking it.

14
00:03:35,000 --> 00:03:54,000
And this goes on naturally. The commentary says that dogs know, when they walk, dogs are able to give rise to knowledge of the action, but they know it based on greed, anger, and delusion.

15
00:03:54,000 --> 00:04:10,000
And did team mana, doesn't have these three departments. When a dog feels pain, the dog knows it feels pain. There's no way to avoid that, but it identifies with the pain.

16
00:04:10,000 --> 00:04:26,000
So it sees the pain as my pain is me, and so on. This is what we're trying to get away from, to create with the Buddha called Patisati Mata. This simple, only knowing things as they are, specifically as they are.

17
00:04:26,000 --> 00:04:55,000
So it's not just set detent to recognize something, it's recognizing something just for what it is. And that's what is the important quality of mind that we're trying to create, and that's created quite nicely by using the, not the label, but this simple thought, to establish and remind ourselves with a simple thought,

18
00:04:55,000 --> 00:05:02,000
that removes all the complications of now my stomach is rising or so on.

19
00:05:02,000 --> 00:05:11,000
Now there is the experience of rising, where this experience is rising.

20
00:05:11,000 --> 00:05:35,000
Yeah, I think I didn't understand very clearly what the question was about.

