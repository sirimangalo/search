1
00:00:00,000 --> 00:00:10,000
Enjoying, liking, and work.

2
00:00:10,000 --> 00:00:14,640
So when you're meditating you have to be careful not to work too much.

3
00:00:14,640 --> 00:00:22,080
Some people will fill up their days with work, their stories in the, in the, there's one

4
00:00:22,080 --> 00:00:28,000
good story in the Damapanda about a monk who swept all day, and the other monks were

5
00:00:28,000 --> 00:00:29,000
quite impressed.

6
00:00:29,000 --> 00:00:42,000
And they would say, wow, yeah, this guy's a real energetic person with a good work ethic.

7
00:00:42,000 --> 00:00:45,000
It was an energetic monk, not lazy.

8
00:00:45,000 --> 00:00:46,000
It's a good quality.

9
00:00:46,000 --> 00:00:49,000
The Buddha said, you know, you need to enter and see.

10
00:00:49,000 --> 00:00:52,800
And so they told the Buddha, and the Buddha said, a person who sweeps all the time isn't

11
00:00:52,800 --> 00:00:57,800
called the monk, we're called a janitor.

12
00:00:57,800 --> 00:01:03,600
I have this story.

13
00:01:03,600 --> 00:01:06,200
What happens is, of course, meditation is difficult.

14
00:01:06,200 --> 00:01:12,200
It's difficult to be patient and to be still and to stay with your experiences.

15
00:01:12,200 --> 00:01:16,400
So we find ways of mitigating that.

16
00:01:16,400 --> 00:01:19,680
And to some extent, it's good to take a break, right?

17
00:01:19,680 --> 00:01:24,280
You want to have a cup of tea, not because tea is important, but because it's a way

18
00:01:24,280 --> 00:01:25,280
of relaxing.

19
00:01:25,280 --> 00:01:26,280
It makes it easier.

20
00:01:26,280 --> 00:01:35,080
It's stepping back and saying, oh, okay, I need to relax and try again.

21
00:01:35,080 --> 00:01:45,480
It's admitting, admitting your failing and your imperfection, yes, I can't.

22
00:01:45,480 --> 00:01:52,080
I should just be meditating all the time, but I just need a break.

23
00:01:52,080 --> 00:01:58,280
I'm not perfect, so I need to take some time off.

24
00:01:58,280 --> 00:02:06,280
But then you start to take longer breaks and then you do some cleaning up, right?

25
00:02:06,280 --> 00:02:13,480
You take time out of every day, I remember having a daily ritual of, okay, this is

26
00:02:13,480 --> 00:02:15,480
the time of day when I'll do my sweeping.

27
00:02:15,480 --> 00:02:21,480
This is when I'll do my cleaning, and it's good to have that kind of a ritual.

28
00:02:21,480 --> 00:02:28,280
It gives you some stability and as I said, it's just a step back.

29
00:02:28,280 --> 00:02:34,680
But in certain cases, it becomes a crutch and then you find more things to do and you

30
00:02:34,680 --> 00:02:37,280
start looking for work.

31
00:02:37,280 --> 00:02:38,280
It's easier.

32
00:02:38,280 --> 00:02:43,280
It's easier to sweep all day than in this to meditate all day, certainly.

33
00:02:43,280 --> 00:02:50,480
And so some monks will sweep, the whole monastery, sweep in the morning, sweep in the afternoon.

34
00:02:50,480 --> 00:02:58,680
There was one nun in our monastery who did this and she was a little bit off, so she'd

35
00:02:58,680 --> 00:03:04,080
come around and I remember, I didn't know idea, she'd come around and she'd mutter things.

36
00:03:04,080 --> 00:03:08,280
I had no idea what she was saying, but I remember sitting with a monk and found that

37
00:03:08,280 --> 00:03:13,280
she was actually quite, she had quite a foul mouth.

38
00:03:13,280 --> 00:03:19,680
But for the longest time, I thought she was maybe quite an impressive person, sweeping

39
00:03:19,680 --> 00:03:24,280
the whole monastery every day, every day working, working very hard and muttering to her

40
00:03:24,280 --> 00:03:39,080
son, but she was crazy and she was actually quite foul and most the way that I didn't understand.

41
00:03:39,080 --> 00:03:40,080
It's a simple point.

42
00:03:40,080 --> 00:03:45,480
Don't get too caught up in work.

43
00:03:45,480 --> 00:03:51,480
Don't get caught up in extracurricular activities.

44
00:03:51,480 --> 00:03:58,680
Of course, in truth, if you're sweeping, you can do it mindfully and to some extent

45
00:03:58,680 --> 00:04:03,680
that's a good part and a proper part of the practice.

46
00:04:03,680 --> 00:04:08,080
It is necessary and even in our center, I'm telling all the meditators to do cleaning,

47
00:04:08,080 --> 00:04:13,320
to spend some little time every day because we don't have a janitor, we don't have anyone

48
00:04:13,320 --> 00:04:18,600
looking after this place, this is our place to look after.

49
00:04:18,600 --> 00:04:22,720
And so mindful cleaning is an important part of your day.

50
00:04:22,720 --> 00:04:29,760
It should be a part of your schedule, but just don't get caught up because it can happen

51
00:04:29,760 --> 00:04:34,800
that this is easy, I can just vacuuming and I can be mindful.

52
00:04:34,800 --> 00:04:40,640
Not as mindful, but you think, well, it's more comfortable, so a little bunch of other

53
00:04:40,640 --> 00:04:41,640
work.

54
00:04:41,640 --> 00:04:48,480
Later on in the course, in a standard meditation center, we've never have

55
00:04:48,480 --> 00:04:52,880
people cooking, even reheating their own food.

56
00:04:52,880 --> 00:04:58,080
Near the end of the course, we'd actually bring this, the meditators, food in a container,

57
00:04:58,080 --> 00:05:03,240
we'd have these stacked containers and we'd bring them, bring the food to their room

58
00:05:03,240 --> 00:05:07,360
and we wouldn't even let them leave their room.

59
00:05:07,360 --> 00:05:13,400
In fact, even times before my time, apparently, anyone who came to the monastery, they

60
00:05:13,400 --> 00:05:17,120
wouldn't even let them leave the room from day one.

61
00:05:17,120 --> 00:05:22,000
So they bring them food every day and the teacher would come and see them at their room,

62
00:05:22,000 --> 00:05:25,960
come to the door, knock on the door, ask them about their practice, and then close the

63
00:05:25,960 --> 00:05:28,200
door and move on.

64
00:05:28,200 --> 00:05:39,240
So for 21 days, for 30 days, you'd just be in your room the whole time, and we don't

65
00:05:39,240 --> 00:05:45,120
expect meditators to be able to handle that, but it's a good example to think of.

66
00:05:45,120 --> 00:06:00,640
First thing I am saying, how do you feel for 24, 30 days?

