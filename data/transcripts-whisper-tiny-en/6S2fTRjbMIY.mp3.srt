1
00:00:00,000 --> 00:00:08,360
Okay, so what should one do about those that try to convert us?

2
00:00:08,360 --> 00:00:13,160
This seems to happen to me when I'm waiting for the bus for anything.

3
00:00:13,160 --> 00:00:18,120
Some of them won't leave me alone until my bus is there.

4
00:00:18,120 --> 00:00:26,000
What to do with the crazy zealots?

5
00:00:26,000 --> 00:00:34,000
I've, you know, I've, of course come up with it, come up against this myself.

6
00:00:34,000 --> 00:00:41,000
The best way, if you can, the best way that I've found is to ask them lots of questions.

7
00:00:41,000 --> 00:00:50,000
Because it helps you to be open-minded and it helps to change your, the angst that we have, the aversion that we have.

8
00:00:50,000 --> 00:00:56,000
This is what we call Batiga. Batiga is this, this kind of annoyance that comes up.

9
00:00:56,000 --> 00:01:02,000
It becomes annoying or it, it chafes on us to have to put up with people.

10
00:01:02,000 --> 00:01:06,000
But clearly, these sort of people have some problem inside.

11
00:01:06,000 --> 00:01:10,000
They have some very, very, very strong clinging.

12
00:01:10,000 --> 00:01:16,000
It's clinging to views and it's clinging to their idea of pleasure and happiness.

13
00:01:16,000 --> 00:01:25,000
And when it's right, they're clinging, you know, clinging out of fear as well, clinging out of ignorance as well.

14
00:01:25,000 --> 00:01:31,000
And the best way that, you know, you consider them as someone without big knot inside.

15
00:01:31,000 --> 00:01:33,000
They're all twisted up inside.

16
00:01:33,000 --> 00:01:37,000
And you have to look at this and you have to help them to untie it.

17
00:01:37,000 --> 00:01:43,000
And it takes skill, it takes clarity of mind.

18
00:01:43,000 --> 00:01:46,000
So what I find is the best is to explore it with them.

19
00:01:46,000 --> 00:01:51,000
They're telling you that X is right or X is the right path.

20
00:01:51,000 --> 00:01:56,000
Or if you don't X, then you're going to Y or you're going to help.

21
00:01:56,000 --> 00:01:59,000
You're going to suffer.

22
00:01:59,000 --> 00:02:01,000
So you can ask them about it.

23
00:02:01,000 --> 00:02:04,000
That's what I found is the best thing to do.

24
00:02:04,000 --> 00:02:11,000
Because it helps to ease their pressure and you can sort of help carry their burden around for them.

25
00:02:11,000 --> 00:02:14,000
Say, okay, well, let's look at this burden.

26
00:02:14,000 --> 00:02:18,000
And hopefully when you look at it, you'll see that it's less useful than you thought.

27
00:02:18,000 --> 00:02:24,000
I mean, it helps to ease up the situation.

28
00:02:24,000 --> 00:02:31,000
If I keep denying God, yeah, I guess you get that.

29
00:02:31,000 --> 00:02:35,000
I don't think I've ever gotten it quite like that.

30
00:02:35,000 --> 00:02:40,000
You know, no one, I've never had anyone tell me that I'm going to help.

31
00:02:40,000 --> 00:02:43,000
But, you know, that's what they're thinking, right?

32
00:02:43,000 --> 00:02:47,000
But, you know, if you talk to them about it and ask them,

33
00:02:47,000 --> 00:02:49,000
I was asking, it was funny.

34
00:02:49,000 --> 00:02:51,000
I have a funny story that I'll tell you.

35
00:02:51,000 --> 00:02:55,000
And this is hopefully a little bit of support for the idea of asking questions.

36
00:02:55,000 --> 00:03:02,000
Because I went to this evangelical church that was,

37
00:03:02,000 --> 00:03:05,000
I've told this story before, but some of you may have heard it.

38
00:03:05,000 --> 00:03:06,000
But let's start at the beginning.

39
00:03:06,000 --> 00:03:11,000
I was teaching meditation in Los Angeles at a time monastery.

40
00:03:11,000 --> 00:03:17,000
There were some English teachers that were teaching the monks English in the monastery.

41
00:03:17,000 --> 00:03:19,000
And I said, well, bring them down, some Westerners.

42
00:03:19,000 --> 00:03:21,000
Let's teach them meditation.

43
00:03:21,000 --> 00:03:23,000
They're like, I don't think they'll be interested.

44
00:03:23,000 --> 00:03:25,000
Why not? Bring them down.

45
00:03:25,000 --> 00:03:29,000
Well, they're actually evangelical Christians.

46
00:03:29,000 --> 00:03:31,000
Like, oh, well, bring them down.

47
00:03:31,000 --> 00:03:33,000
Christians can meditate by not.

48
00:03:33,000 --> 00:03:36,000
Turns out they're, by the looks of it,

49
00:03:36,000 --> 00:03:39,000
their only reason for going there was to convert everyone.

50
00:03:39,000 --> 00:03:44,000
Because they were totally,

51
00:03:44,000 --> 00:03:46,000
Battiga is the worst.

52
00:03:46,000 --> 00:03:51,000
They were averse to me.

53
00:03:51,000 --> 00:03:54,000
And, you know, cold.

54
00:03:54,000 --> 00:03:56,000
A couple of them were nice, but they're leader.

55
00:03:56,000 --> 00:03:58,000
There was the leader of them.

56
00:03:58,000 --> 00:04:01,000
And she said, well, why don't you take my card and they're like,

57
00:04:01,000 --> 00:04:03,000
no, thanks, we don't want your card.

58
00:04:03,000 --> 00:04:07,000
Like, whoa, you know, it was, it was that severe.

59
00:04:07,000 --> 00:04:10,000
And so I talked them a little bit and tried to, you know,

60
00:04:10,000 --> 00:04:13,000
open them up and just explain a little bit about meditation.

61
00:04:13,000 --> 00:04:16,000
And I think in the end a couple of them took my card.

62
00:04:16,000 --> 00:04:21,000
Next night, the woman, the leader comes back with her husband

63
00:04:21,000 --> 00:04:25,000
who works at the evangelical parish or whatever you call it.

64
00:04:25,000 --> 00:04:29,000
And he had a Bible with him and their kids came as well.

65
00:04:29,000 --> 00:04:32,000
And their kids were a real trip because they had trained their kids

66
00:04:32,000 --> 00:04:34,000
to say all these crazy things.

67
00:04:34,000 --> 00:04:37,000
And so I was asking him and then I was asking his kids

68
00:04:37,000 --> 00:04:40,000
and they said, go ahead, you can tell him and the kid would say,

69
00:04:40,000 --> 00:04:43,000
if you don't believe in God, you're going to hell or something.

70
00:04:43,000 --> 00:04:45,000
Like, or whatever they were saying, all of these,

71
00:04:45,000 --> 00:04:48,000
these crazy things that they say.

72
00:04:48,000 --> 00:04:51,000
And, you know, we really nailed them.

73
00:04:51,000 --> 00:04:55,000
I was with a Jewish friend who, a Jewish Hindu friend,

74
00:04:55,000 --> 00:04:59,000
who she's really a trip and she had some good arguments as well.

75
00:04:59,000 --> 00:05:01,000
And we were just arguing with them.

76
00:05:01,000 --> 00:05:05,000
But the argument didn't, you know, it didn't help them at all.

77
00:05:05,000 --> 00:05:08,000
It created more aversion in their mind.

78
00:05:08,000 --> 00:05:12,000
And it bridged, it widened the gap between us, really.

79
00:05:12,000 --> 00:05:16,000
It didn't end the night on, we felt good because we had won the argument.

80
00:05:16,000 --> 00:05:20,000
But, you know, what is that except conceit and attachment?

81
00:05:20,000 --> 00:05:23,000
So the next morning I was thinking about this.

82
00:05:23,000 --> 00:05:26,000
And I said, well, if they're going to come and visit me,

83
00:05:26,000 --> 00:05:28,000
then I have a right to go visit them.

84
00:05:28,000 --> 00:05:29,000
And so I did.

85
00:05:29,000 --> 00:05:32,000
I walked the next day after my arms round.

86
00:05:32,000 --> 00:05:35,000
On the way back from arms or after I ate her, I can't remember.

87
00:05:35,000 --> 00:05:37,000
I don't know because I ate on arms round.

88
00:05:37,000 --> 00:05:44,000
And then when I came back, I said, okay, I'm going to go down to the parish and walk down there.

89
00:05:44,000 --> 00:05:54,000
And went in and there was a guy up on the dais or whatever, talking.

90
00:05:54,000 --> 00:05:58,000
And the hall was full of people.

91
00:05:58,000 --> 00:06:04,000
I stood in the back and this, this, this, this couple of people were standing in the back.

92
00:06:04,000 --> 00:06:07,000
And I was, I think I talked with him or I can't remember how it came up.

93
00:06:07,000 --> 00:06:13,000
But it turns out it was a, it was a teaching of pastors

94
00:06:13,000 --> 00:06:16,000
that this guy was a teacher of pastors.

95
00:06:16,000 --> 00:06:18,000
I don't know what they call him.

96
00:06:18,000 --> 00:06:23,000
He was teaching the teachers or he was a pastor to the pastors or something like that.

97
00:06:23,000 --> 00:06:26,000
Came down from San Francisco.

98
00:06:26,000 --> 00:06:34,000
And he, so he went on and on and I was just kind of listening.

99
00:06:34,000 --> 00:06:36,000
And then this guy beside me, hands me a Bible.

100
00:06:36,000 --> 00:06:38,000
And he said, you can keep that.

101
00:06:38,000 --> 00:06:40,000
And it was a nice little Bible with gold trim.

102
00:06:40,000 --> 00:06:42,000
It was not a cheap item.

103
00:06:42,000 --> 00:06:44,000
It was his Bible that he was giving me.

104
00:06:44,000 --> 00:06:46,000
So they're nice people, right?

105
00:06:46,000 --> 00:06:50,000
They have an agenda, but, you know, they, they can be,

106
00:06:50,000 --> 00:06:54,000
they have a big heart in many ways.

107
00:06:54,000 --> 00:06:57,000
So I was reading through it and then I started reading through other parts

108
00:06:57,000 --> 00:07:01,000
and then it kind of wound down and I went outside and sat on a bench

109
00:07:01,000 --> 00:07:05,000
and started reading and I was reading some passages and looking up

110
00:07:05,000 --> 00:07:09,000
because I had heard, I had heard that God is love.

111
00:07:09,000 --> 00:07:16,000
So I was looking it up and I can't remember the whole, the whole thing.

112
00:07:16,000 --> 00:07:22,000
But then the guy comes out, the guy bursts out the doors and,

113
00:07:22,000 --> 00:07:29,000
you know, bears down on me with both barrels loaded.

114
00:07:29,000 --> 00:07:33,000
And he said, what an opportunity.

115
00:07:33,000 --> 00:07:37,000
He said, I saw you in the back and and so on and so on.

116
00:07:37,000 --> 00:07:41,000
The Bible that was this, you can see, was this big.

117
00:07:41,000 --> 00:07:43,000
It was, it was huge.

118
00:07:43,000 --> 00:07:50,000
He had a, you know, the, the Bible's Bible and he drops it on the bench beside me

119
00:07:50,000 --> 00:07:54,000
and, and puts his foot up on the bench or something like that.

120
00:07:54,000 --> 00:07:58,000
And it was just like, what an opportunity to convert me or something.

121
00:07:58,000 --> 00:08:01,000
And so this is where I, I,

122
00:08:01,000 --> 00:08:04,000
this is, this was the example of, of how to deal with these people.

123
00:08:04,000 --> 00:08:08,000
As I started asking him questions, because here I had a Bible and I was like,

124
00:08:08,000 --> 00:08:11,000
great, here I have the teacher of the pastors right in front of me

125
00:08:11,000 --> 00:08:13,000
and I can ask him questions.

126
00:08:13,000 --> 00:08:18,000
So I asked him some fairly, no, not, not terribly pointed

127
00:08:18,000 --> 00:08:21,000
because you don't want to sound threatening to them.

128
00:08:21,000 --> 00:08:26,000
You don't want to seem to be just asking questions for the sake of asking questions.

129
00:08:26,000 --> 00:08:29,000
You know, I was really interested and he told me some really weird things

130
00:08:29,000 --> 00:08:32,000
like how we die twice.

131
00:08:32,000 --> 00:08:36,000
There was something about how God is love and I said like,

132
00:08:36,000 --> 00:08:40,000
if God is love, then why does he send you to hell or something like that?

133
00:08:40,000 --> 00:08:45,000
And then he went on about how God is also truth or something or I don't know

134
00:08:45,000 --> 00:08:50,000
and how God, God loves us and therefore he has to send us to hell, something like that.

135
00:08:50,000 --> 00:08:54,000
I mean, what's going on here is you're starting to point out to them

136
00:08:54,000 --> 00:08:57,000
or let them, let them show themselves the inconsistencies

137
00:08:57,000 --> 00:09:00,000
because in trying to explain such a thing,

138
00:09:00,000 --> 00:09:04,000
you have to run so many somersaults that it gets tiring

139
00:09:04,000 --> 00:09:07,000
and you go through the inconsistencies.

140
00:09:07,000 --> 00:09:09,000
You're forced through them.

141
00:09:09,000 --> 00:09:12,000
You're forced to feel the inconsistency of your argument

142
00:09:12,000 --> 00:09:14,000
and that's a powerful thing.

143
00:09:14,000 --> 00:09:17,000
So I did this with them a few times in a few ways.

144
00:09:17,000 --> 00:09:20,000
I asked him about children.

145
00:09:20,000 --> 00:09:23,000
I said, what about children?

146
00:09:23,000 --> 00:09:26,000
Did they go to hell? No.

147
00:09:26,000 --> 00:09:27,000
No, I didn't ask him.

148
00:09:27,000 --> 00:09:29,000
I said, well, what about a person in a country

149
00:09:29,000 --> 00:09:32,000
where they've never heard of Jesus?

150
00:09:32,000 --> 00:09:36,000
What if they die without having taken Jesus into their heart

151
00:09:36,000 --> 00:09:38,000
or something about a child?

152
00:09:38,000 --> 00:09:42,000
It came up and he said, well, yeah, they go according to the Bible,

153
00:09:42,000 --> 00:09:43,000
they go to hell as well.

154
00:09:43,000 --> 00:09:46,000
And I said, well, that doesn't sound really fair, does it?

155
00:09:46,000 --> 00:09:50,000
And he said something like, no, no, it doesn't.

156
00:09:50,000 --> 00:09:53,000
I can't remember exactly how it went, but it was so absurd.

157
00:09:53,000 --> 00:09:56,000
It was like, this is the guy who's supposed to be teaching this stuff

158
00:09:56,000 --> 00:09:59,000
and he doesn't even think it's fair.

159
00:09:59,000 --> 00:10:01,000
And then we were talking about faith.

160
00:10:01,000 --> 00:10:06,000
In the end, it finally came down to this where I really got him.

161
00:10:06,000 --> 00:10:09,000
So I was asking about faith and he was saying,

162
00:10:09,000 --> 00:10:13,000
you know, the point in Christianity or in his Christianity

163
00:10:13,000 --> 00:10:15,000
is that it's faith.

164
00:10:15,000 --> 00:10:20,000
The faith is what determines whether you're Christian or not

165
00:10:20,000 --> 00:10:23,000
and it's because it's because of faith that works,

166
00:10:23,000 --> 00:10:27,000
and so he has this argument of faith over works.

167
00:10:27,000 --> 00:10:34,000
And so he said, when you really, really become Christian

168
00:10:34,000 --> 00:10:40,000
is when you have this unwavering faith or a person who is truly accepted Christ.

169
00:10:40,000 --> 00:10:43,000
He said, it's something like those people in Africa or in countries

170
00:10:43,000 --> 00:10:45,000
where they've never heard of Jesus Christ.

171
00:10:45,000 --> 00:10:47,000
They've already hardened their hearts to Jesus.

172
00:10:47,000 --> 00:10:51,000
And he said, for a person who is truly accepted Jesus into their hearts,

173
00:10:51,000 --> 00:10:54,000
they have unwavering faith.

174
00:10:54,000 --> 00:10:56,000
And so I asked him.

175
00:10:56,000 --> 00:10:58,000
I said, so what about your faith?

176
00:10:58,000 --> 00:11:00,000
Does your faith ever waver?

177
00:11:00,000 --> 00:11:03,000
And you could tell I could see it in him.

178
00:11:03,000 --> 00:11:05,000
He just went like this.

179
00:11:05,000 --> 00:11:10,000
He started his mind started wavering in just a split second

180
00:11:10,000 --> 00:11:14,000
and then he said, well, for myself, no, I'd have to say it doesn't waver.

181
00:11:14,000 --> 00:11:18,000
But he was wavering all over the place.

182
00:11:18,000 --> 00:11:22,000
And so I just left him at that and thinking to myself, wow,

183
00:11:22,000 --> 00:11:24,000
this is the teacher's teacher.

184
00:11:24,000 --> 00:11:27,000
They don't really have that much.

185
00:11:27,000 --> 00:11:31,000
And if you just go through it, it's kind of fun actually.

186
00:11:31,000 --> 00:11:34,000
I went through with some Mormon kids through this.

187
00:11:34,000 --> 00:11:37,000
Later on, I realized there was something to this.

188
00:11:37,000 --> 00:11:40,000
And so I talked with the Mormons and actually it was really interesting.

189
00:11:40,000 --> 00:11:45,000
Mormons are like, they don't really have many beliefs at all.

190
00:11:45,000 --> 00:11:49,000
They were like talking about how God went through all that we have to go through.

191
00:11:49,000 --> 00:11:51,000
And that's how he became God.

192
00:11:51,000 --> 00:11:53,000
And I was like, that's kind of interesting.

193
00:11:53,000 --> 00:11:55,000
And a lot of what they were saying was like,

194
00:11:55,000 --> 00:12:01,000
actually compatible with a never-ending universe or with some sorrow.

195
00:12:04,000 --> 00:12:07,000
So I think, well, that's my answer to that question.

196
00:12:07,000 --> 00:12:16,000
Actually, that's helpful in some way.

