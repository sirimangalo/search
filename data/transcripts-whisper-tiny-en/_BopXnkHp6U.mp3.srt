1
00:00:00,000 --> 00:00:06,360
Hey, everyone. This is Adder again. I'm here with an update on our Meditation Center

2
00:00:06,360 --> 00:00:13,240
project and a little bit of other news from Sarah Mangalow. Our fundraiser continues to

3
00:00:13,240 --> 00:00:19,960
move along steadily. We've raised over $26,000 now and I and the rest of the Sarah Mangalow

4
00:00:19,960 --> 00:00:29,800
community are very appreciative of all of the support that has shown up in this project.

5
00:00:29,800 --> 00:00:35,320
So we continue to keep this going. We're continuing to try to gather some videos for

6
00:00:35,320 --> 00:00:41,720
our testimonial series. If there's any way that you all want to help, please reach out

7
00:00:41,720 --> 00:00:48,040
to us and we will make room for whatever is exciting for you.

8
00:00:48,040 --> 00:00:58,120
I wanted to mention one thing about donating through our platform, GoFundMe. GoFundMe is

9
00:00:58,120 --> 00:01:04,880
a for-profit company that is providing a really fantastic, easy-to-use service for us.

10
00:01:04,880 --> 00:01:11,080
Part of their business model is that they leave an option for giving a tip to GoFundMe

11
00:01:11,080 --> 00:01:18,520
to that organization. I just want to make sure everyone knows that that tip is optional.

12
00:01:18,520 --> 00:01:24,400
When donating, it's not clear how to avoid doing it, but you can always select to choose

13
00:01:24,400 --> 00:01:32,280
an other amount and manually enter zero. So I know there's some people that got stuck

14
00:01:32,280 --> 00:01:40,000
giving a larger tip than they wanted or were confused about that process. GoFundMe does

15
00:01:40,000 --> 00:01:48,640
have an offer refunds for people who have given that tip to them in error. So feel free

16
00:01:48,640 --> 00:01:54,320
if you're having any trouble donating money and want to make sure that the most money is

17
00:01:54,320 --> 00:02:00,320
getting to the organization and support of our projects. Feel free to reach out to us and

18
00:02:00,320 --> 00:02:06,320
we can always help you. So I wanted to share a little bit about some of the other stuff going

19
00:02:06,320 --> 00:02:15,840
on at Siri Mangalo. Our community is multifaceted and reaches people in different places. Some

20
00:02:15,840 --> 00:02:22,320
are very involved in our YouTube live streams. Other people are very involved on our Discord

21
00:02:22,320 --> 00:02:30,520
server. Others spend their time on meditation plus our community meditation app. And I wanted

22
00:02:30,520 --> 00:02:37,880
to share a little bit about the book project that's been happening. We have and will continue

23
00:02:37,880 --> 00:02:48,960
to be putting out some information about this project of transcribing talks from Bantayu

24
00:02:48,960 --> 00:02:56,280
to Damo and putting them in written text form. We're doing a series of booklets. The

25
00:02:56,280 --> 00:03:02,480
booklet on Enlightenment has been out for a while. And we've just released a booklet

26
00:03:02,480 --> 00:03:11,680
on pilgrimage talks. These come from about one year ago exactly about a dozen of us had

27
00:03:11,680 --> 00:03:20,040
gone to India and Nepal to visit the holy sites of Buddhist pilgrimage. And their Bantayu

28
00:03:20,040 --> 00:03:26,960
gave a series of talks at each of the major pilgrimage sites. And this is a really great

29
00:03:26,960 --> 00:03:34,280
series of talks that are all available on YouTube. But we've also now got them written out

30
00:03:34,280 --> 00:03:41,920
in booklet form. So you can get that on our website. We'll have links in the video description

31
00:03:41,920 --> 00:03:48,720
here. And this, the booklet project, this book project is an ongoing one. And we can always

32
00:03:48,720 --> 00:03:54,600
use more people in, we have, if you want to contribute a little bit, it's pretty easy

33
00:03:54,600 --> 00:04:03,040
to jump into the project and do a little bit of transcribing for it. But what we really

34
00:04:03,040 --> 00:04:07,240
need right now is a lot of editing. So if there are people that want to get on and just

35
00:04:07,240 --> 00:04:15,320
edit the text that people have transcribed or that we've gotten some automated transcription

36
00:04:15,320 --> 00:04:22,640
for, we could use some more hands editing. So we'll have information in the video description

37
00:04:22,640 --> 00:04:28,800
about that. And you can feel free to hop on our Discord server and help with that. And

38
00:04:28,800 --> 00:04:34,720
if not, I do encourage you to check out that series of talks. It's really, really amazing

39
00:04:34,720 --> 00:04:44,960
and valuable. A lot of wise words put together at some of the most important sites in Buddhist

40
00:04:44,960 --> 00:04:55,120
history. We also have a team that's sort of following the, the production of these English

41
00:04:55,120 --> 00:04:59,840
language booklets that is translating them into Spanish. So we'll make sure to keep you

42
00:04:59,840 --> 00:05:04,560
updated with info on that, that they've got the Enlightenment booklet translated and will

43
00:05:04,560 --> 00:05:14,200
be right behind with this, this pure pilgrimage booklet. So if you are native Spanish speaker,

44
00:05:14,200 --> 00:05:20,400
I encourage you to take a look at that. And we can always use more translators to help

45
00:05:20,400 --> 00:05:24,520
translate these teachings into, into various languages.

46
00:05:24,520 --> 00:05:31,800
Well, I think that's about all I have for today. Like I said, please stay in touch with us

47
00:05:31,800 --> 00:05:40,000
in the community. The Discord server is a great place to, to get involved. And if you

48
00:05:40,000 --> 00:05:44,160
have any questions about the fundraiser or any of the things that we're doing, you

49
00:05:44,160 --> 00:05:52,040
can shoot a message, you know, on, go fund me on the contact page from our website or

50
00:05:52,040 --> 00:05:58,440
hop on the Discord server and reach out to us. I wish you all the best. I wish you health

51
00:05:58,440 --> 00:06:28,280
and happiness. Thank you all. Take care.

