1
00:00:00,000 --> 00:00:05,520
Hello and welcome back to our study of the Dhamapada. Today we continue on with

2
00:00:05,520 --> 00:00:10,800
verse 1-23 which we just follow.

3
00:00:10,800 --> 00:00:25,800
We are going to go to the Dhamapada. We are going to go to the Dhamapada and the Dhamapada.

4
00:00:25,800 --> 00:00:40,440
And just as a merchant relates to dangers on the path or a dangerous path when he has

5
00:00:40,440 --> 00:00:56,080
upset to few weapons or few defenses, Mahanda nova, great wealth. Or just as a person,

6
00:00:56,080 --> 00:01:08,000
Jiwitakama, one who desires to live relates to poison.

7
00:01:08,000 --> 00:01:18,360
Apanipari wa ji, one should avoid evil.

8
00:01:18,360 --> 00:01:24,400
So we have two analogies here. The first is a merchant.

9
00:01:24,400 --> 00:01:32,400
How a merchant avoids a dangerous path, a merchant who has little defenses but great wealth.

10
00:01:32,400 --> 00:01:43,160
And the second is a person desiring to live, avoids poison, doesn't eat or drink poison.

11
00:01:43,160 --> 00:01:52,840
In the same way we should avoid evil. Simple verse. It's also a fairly simple story.

12
00:01:52,840 --> 00:01:58,040
It's actually not so much related. This is another example of how the Buddha used a story

13
00:01:58,040 --> 00:02:09,560
to, as it seems as an excuse to give a teaching. Relating some fairly unmeaningful discussion

14
00:02:09,560 --> 00:02:13,440
turning it into meaningful teaching.

15
00:02:13,440 --> 00:02:23,200
So the story goes that there were these thieves who were trying to rob this very rich merchant.

16
00:02:23,200 --> 00:02:29,080
He went out on travels but he took monks with him. So he had lots of supplies and he

17
00:02:29,080 --> 00:02:36,080
invited monks to travel with him, which was convenient for the monks because he would have

18
00:02:36,080 --> 00:02:41,400
supplies for them. When traveling it's hard to find a place and to know where to go

19
00:02:41,400 --> 00:02:49,560
for arms. So when a monk is staying in a village they acquire people who support them,

20
00:02:49,560 --> 00:02:56,120
who treat them as a part of their society and give them food.

21
00:02:56,120 --> 00:02:59,800
But when a monk goes to a new place, I mean even in a Buddhist country this is still

22
00:02:59,800 --> 00:03:05,840
the case when you go to a new place. It's hard to expect that you're going to receive

23
00:03:05,840 --> 00:03:13,720
arms or expect more especially when you're traveling.

24
00:03:13,720 --> 00:03:19,200
So they went with his merchant who said that he would care for them and feed them along

25
00:03:19,200 --> 00:03:24,080
the trail along the way. So they went together. There was a whole bunch of monks. The

26
00:03:24,080 --> 00:03:33,920
commentary is just 500 but I think 500 is just code for a large group.

27
00:03:33,920 --> 00:03:43,560
And he got to a certain village and was resting there when these thieves caught up with

28
00:03:43,560 --> 00:03:52,080
him and lay in weight so that when he left the village they would be able to rob him.

29
00:03:52,080 --> 00:03:55,440
And they waited and they didn't see him coming through the village so they assumed he

30
00:03:55,440 --> 00:03:59,840
was there. They wanted to find out when he was going to leave so they could prepare

31
00:03:59,840 --> 00:04:07,240
an ambush and rob him. And so one of the thieves went to a friend he had in the city

32
00:04:07,240 --> 00:04:17,560
and asked him about it and the man found out for him. I went to the merchant and asked

33
00:04:17,560 --> 00:04:27,600
him and found out and told him that in two days the merchant was going to set out and

34
00:04:27,600 --> 00:04:46,560
so this thief went back to his friends and got them already. And then this friend of

35
00:04:46,560 --> 00:04:51,760
his somehow it's kind of strange actually and maybe he should read the poly but the English

36
00:04:51,760 --> 00:04:59,880
as it's written he goes to the merchant and lets the merchant know which is reasonable.

37
00:04:59,880 --> 00:05:05,240
To that extent it's understandable because it's like he feels guilty especially he mentions

38
00:05:05,240 --> 00:05:10,040
that there's 500 monks with this merchant. So there's a large group of monks with this

39
00:05:10,040 --> 00:05:14,160
merchant so if he doesn't mourn the merchant it's not just going to hurt the merchant

40
00:05:14,160 --> 00:05:22,000
but it's also going to impact hurt and maybe kill some of these monks. So he goes and tells

41
00:05:22,000 --> 00:05:26,640
the merchant that there's a band of thieves laying in wait for him. So all well and good

42
00:05:26,640 --> 00:05:34,160
but then the thieves come back to the guy and he tells them basically that the merchant

43
00:05:34,160 --> 00:05:40,000
has changed his mind. The merchant decides to head home so he turns around and prepares

44
00:05:40,000 --> 00:05:45,160
to head out of the other gate and he tells the thieves this which is kind of strange how

45
00:05:45,160 --> 00:05:51,840
he's talking to both sides. So the thieves turn around go to the other side of the village

46
00:05:51,840 --> 00:05:56,880
and they prepare an ambush on the road back and the man goes back to the merchant and

47
00:05:56,880 --> 00:06:03,240
lets him know that. Maybe something's lost in the translation it seems like odd behavior

48
00:06:03,240 --> 00:06:13,720
to me but anyway it's just a story. Finally the merchant not being able to go forward

49
00:06:13,720 --> 00:06:17,200
and now knowing that if he goes back same thing's going to happen and that he really doesn't

50
00:06:17,200 --> 00:06:23,920
have the defenses says to the monks look I think there's no reason for me to move on I'm

51
00:06:23,920 --> 00:06:30,120
just going to stay here in this village, set up and sell my wares here see how I can do

52
00:06:30,120 --> 00:06:38,160
because clearly there's danger waiting for me if I leave. And so the monks hearing this

53
00:06:38,160 --> 00:06:43,320
discussed among themselves and decided there was no way they could continue along the path

54
00:06:43,320 --> 00:06:49,520
and the best thing for them was to return back to the Buddha which they did. When they came

55
00:06:49,520 --> 00:06:57,480
to the Buddha they told him the story and the Buddha used this as an excuse or as an example

56
00:06:57,480 --> 00:07:08,360
a comparison to say look here's a guy who knows danger when he sees it and so he doesn't

57
00:07:08,360 --> 00:07:17,160
set out on the path when there's danger on the path and likewise you all who desire good

58
00:07:17,160 --> 00:07:27,840
and who understand the danger of evil should refrain from evil just as and then he gives

59
00:07:27,840 --> 00:07:37,920
the second example a person who desires to live would avoid poison. So that's the backstory

60
00:07:37,920 --> 00:07:45,600
small backstory and that's the verse. So how does this mean how does it relate to our practice?

61
00:07:45,600 --> 00:07:55,320
Well the word by is interesting because we talk about the dangers and we're always talking

62
00:07:55,320 --> 00:08:02,560
about how bad evil is right it's doing evil is a bad thing doing good is a good thing.

63
00:08:02,560 --> 00:08:09,160
But if you it may not be clear exactly what is wrong with doing bad deeds what is wrong

64
00:08:09,160 --> 00:08:16,960
with being an evil person but it's wrong with taking advantage of people and maybe even more

65
00:08:16,960 --> 00:08:24,880
difficult what is wrong with indulging in pleasure central pleasure in general what's wrong

66
00:08:24,880 --> 00:08:36,040
with it. Why do we consider that to be quote unquote evil? Because of the dangers there are

67
00:08:36,040 --> 00:08:41,280
four dangers in the performance of evil deeds or if you like if you don't like the word evil

68
00:08:41,280 --> 00:08:46,120
though it is the word they use you could understand by the definition of evil and Buddhism it

69
00:08:46,120 --> 00:08:55,120
just means unholsomeness or that which leads to suffering that which has a danger that which is

70
00:08:55,120 --> 00:09:01,880
not worth doing. So there are four dangers that we talk about there's actually 12 dangers but

71
00:09:01,880 --> 00:09:07,360
four of them are specifically related to people doing evil deeds there's four dangers that all

72
00:09:07,360 --> 00:09:17,000
beings have to face. These are births all the age sickness and death. Birth means if you have to

73
00:09:17,000 --> 00:09:24,960
if you die you have to be born again and there's four dangers that people performing good deeds

74
00:09:24,960 --> 00:09:35,520
have to deal with. So people who have engaged in spiritual practice these are the evils of anger

75
00:09:35,520 --> 00:09:44,520
the evils of sensual pleasure the evil of sexual desire and the evil of laziness I believe.

76
00:09:44,520 --> 00:09:52,680
But the four that we're concerned with here are the evil the dangers that are that present

77
00:09:52,680 --> 00:10:00,040
themselves to people who perform evil. What are the parts of the problem with it? The big

78
00:10:00,040 --> 00:10:05,240
one that we always point to is when you pass away you know if you kill someone in this life

79
00:10:05,240 --> 00:10:10,960
people understand in Buddhism that it means someone's going to come back and kill you in the next

80
00:10:10,960 --> 00:10:17,880
life. It's not always like that but we understand there's some future births consequences

81
00:10:17,880 --> 00:10:24,120
or there's some ultimate consequences. Without that it's true that there's a potential problem

82
00:10:24,120 --> 00:10:32,600
with Buddhist theory because you could outrun the consequences if you do all sorts of unwholesome

83
00:10:32,600 --> 00:10:39,640
deeds or cultivate all sorts of addictions to sensuality and pleasure then just when you turn like

84
00:10:39,640 --> 00:10:47,960
65 or as you start taking to age you can just say enough of that and kill yourself or whatever

85
00:10:47,960 --> 00:10:57,960
you you can live out your life and not have to face the consequences if there were no future

86
00:10:57,960 --> 00:11:07,800
existence if desire didn't have the potential to relink to create more if desire didn't have the

87
00:11:07,800 --> 00:11:15,880
potential to create really but specifically the potential to create more life then you'll be able

88
00:11:15,880 --> 00:11:22,680
to get off scot-free there wouldn't be too much danger but even still before we look at that there

89
00:11:22,680 --> 00:11:29,880
are three other types of dangers that only relate to this life. The first is self-sensure

90
00:11:29,880 --> 00:11:45,000
though at anawanda the blaming yourself so the Buddhists and just like a blanket are deans

91
00:11:46,040 --> 00:11:53,480
cover us when we're alone if we do good deans there's a warm comfortable blanket if we do bad

92
00:11:53,480 --> 00:12:04,920
deans it's a heavy uncomfortable shroud covering us and this is certainly true something that you

93
00:12:04,920 --> 00:12:09,320
can see for yourself based on the deeds that you do it something that we see very clearly in

94
00:12:09,320 --> 00:12:14,920
meditation that the good deeds that we do in the bad deeds that we do do cover us and go along with

95
00:12:14,920 --> 00:12:24,280
us they follow us the bad deeds follow us like a ball and chain good deeds lift us up light in

96
00:12:24,280 --> 00:12:32,760
our load light in our step they change us there are change in direction every ethical act that

97
00:12:32,760 --> 00:12:39,800
we perform changes the direction that we're going it affects our life it sends out ripples into

98
00:12:39,800 --> 00:12:45,000
the universe changes how people look at us well first of all it chains how we look at ourselves the

99
00:12:45,000 --> 00:12:51,720
second one is how how other people look at us so the other thing it changes and evil deeds are

100
00:12:52,680 --> 00:13:00,680
for us so what they they lead other people to scold us and blame us and the censure us to look

101
00:13:00,680 --> 00:13:09,400
down upon us it leads to tension and friction even just sensuality and greed when we're greedy

102
00:13:09,400 --> 00:13:15,400
it means we need a portion of something we need something for ourselves someone else wants it

103
00:13:15,400 --> 00:13:19,880
then we have a conflict and we have to resolve it either by fighting over it or by

104
00:13:21,080 --> 00:13:27,080
suppressing our desires and relinquishing it but either way it leads to suffering even to give

105
00:13:27,080 --> 00:13:32,760
other people something that you want is a cause for suffering I mean torments you because you

106
00:13:32,760 --> 00:13:43,000
wanted and you can't have it and the third one is punishment that there is punishment that comes

107
00:13:43,000 --> 00:13:51,400
so that it when we break laws or when we kill when we when we break ethical rules and maxims

108
00:13:52,200 --> 00:13:59,480
not only do is their censure but there's also punishment so we might lose friendships or it might lead

109
00:13:59,480 --> 00:14:09,000
to fighting it might lead even to criminal punishment there are many dangers to doing evil

110
00:14:09,000 --> 00:14:16,680
deeds that you can see in the here now now about the fourth one the fourth danger which is in the

111
00:14:16,680 --> 00:14:24,760
future life it really bears examining and it helps us and it gives us a chance to talk about

112
00:14:24,760 --> 00:14:33,880
how this all relates to our meditation practice because the big problem with unwholesome deeds is

113
00:14:33,880 --> 00:14:40,920
that they're productive and that's how we look at our life life isn't a thing it's not atomic

114
00:14:40,920 --> 00:14:49,160
like hey I'm a being and I'm alive life is caused by it's it's continued it's a it's a conservation

115
00:14:49,160 --> 00:14:58,760
as they say not only are we do we are we alive but we're we're being born every moment right because

116
00:14:59,640 --> 00:15:09,240
there really is no no logically inherent reason to think a priori that that just because we are

117
00:15:09,240 --> 00:15:13,880
alive now we're going to be alive in the next one so there's something that's keeping us alive

118
00:15:13,880 --> 00:15:18,280
and not only alive but there's something that there are many things that are determining which

119
00:15:18,280 --> 00:15:23,880
direction we take in our lives is some of the choice some are choices that we make some are

120
00:15:24,440 --> 00:15:32,280
that which is imposed upon us and one of the things that influences that is our desire now

121
00:15:32,280 --> 00:15:37,720
without desire you would still be alive without greed anger and delusion one continues to live on

122
00:15:38,520 --> 00:15:46,760
but many of the aspects of one life one's life that would be created with greed and based on

123
00:15:46,760 --> 00:15:53,320
greed anger delusion will not be created so enlightened being lives by necessity a very simple life

124
00:15:53,320 --> 00:16:01,320
they aren't able to go out and make money and and work and so on because that part of life

125
00:16:01,320 --> 00:16:11,240
that that the impetus that creates that part of life isn't there and so that's how we look at

126
00:16:11,240 --> 00:16:18,120
at in general at unholesum and wholesome deeds when you when you want something

127
00:16:19,880 --> 00:16:27,640
you create a whole new aspect of your life involved with getting that and it can often be quite

128
00:16:27,640 --> 00:16:33,480
complex the drive to attain and all the work that you have to do and the planning and

129
00:16:33,480 --> 00:16:41,320
manipulation of others even and all of the many things you have to do you end up committing yourself

130
00:16:41,320 --> 00:16:48,520
like when you want a big house then you commit yourself to alone if you want a car when you

131
00:16:48,520 --> 00:16:56,360
fall in love with another person you get married and you start a whole new there's a whole new

132
00:16:56,360 --> 00:17:06,680
thing that is born and so the idea of rebirth is really really that's I don't think it's it would

133
00:17:06,680 --> 00:17:12,840
be acceptable evidence but it is the evidence if you understand reality it's the evidence for

134
00:17:12,840 --> 00:17:24,440
rebirth is this productive potential for karma when you when you have desire it creates it

135
00:17:24,440 --> 00:17:33,640
manifests something in your life and that's all that's left on the other side the brain and the body

136
00:17:34,680 --> 00:17:44,360
and the and external stimulus altogether is also capable of creating life or thought or experience

137
00:17:44,360 --> 00:17:51,480
you can when you hear something the sound triggers the experience of hearing in the mind

138
00:17:51,480 --> 00:18:03,320
the consciousness to arise in at the ear now when you die all of that becomes unproductive

139
00:18:03,320 --> 00:18:12,840
unable to produce experience but the desire is still capable still perfectly capable of creating

140
00:18:12,840 --> 00:18:22,520
new experience it's all that's left and so that is what the fact is that that is what creates

141
00:18:22,520 --> 00:18:27,960
the next life I mean there's no traveling to be born in a womb or something there's a restarting

142
00:18:28,920 --> 00:18:36,920
with just a single two cells starting to create something so the mind and the body working

143
00:18:36,920 --> 00:18:44,200
that's for humans now for angels for other types of beings in hell and so on it's spontaneous

144
00:18:44,200 --> 00:18:54,600
but it's spontaneous based on our desires our attachments personal desires when they pass away

145
00:18:54,600 --> 00:19:00,520
all the productive capability of the body ceases productive capability of the mind has already

146
00:19:00,520 --> 00:19:12,600
ceased in terms of karma and so they pass away and there's no rebirth so that's a little

147
00:19:12,600 --> 00:19:19,800
bit of off track but that's the danger the dangers that will have to be reborn again now for

148
00:19:19,800 --> 00:19:23,880
many people that sounds quite exciting they think wow great I can be reborn again and when we

149
00:19:23,880 --> 00:19:32,200
talk about heaven and so on that's great as well that's that's a good reason to be reborn

150
00:19:32,200 --> 00:19:38,760
because hey we can be reborn in heaven but it ignores the danger or we must never forget

151
00:19:38,760 --> 00:19:48,120
about the danger of evil and so that is what this verse reminds us of that there are consequences

152
00:19:48,120 --> 00:19:56,200
and just as we would avoid other things that cause that have danger and have great potential to

153
00:19:56,200 --> 00:20:05,400
cause harm like poison nor a dangerous road for a rich merchant then we should guard ourselves

154
00:20:05,400 --> 00:20:16,040
and we should avoid the path of evil how we do this is how it relates to our meditation it's

155
00:20:16,040 --> 00:20:20,840
because the way that we do this is through the practice I mean this is very much what would

156
00:20:20,840 --> 00:20:27,720
this practice is based on avoiding evil what is a mism to philosophy where you're just trying to

157
00:20:27,720 --> 00:20:38,200
explore the universe or become some spiritual being with all sorts of high-minded thoughts it's

158
00:20:38,200 --> 00:20:48,200
specifically to be free from evil be free from suffering and to cultivate good so this is very

159
00:20:48,200 --> 00:20:57,080
this is completely the basis of Buddhist practice we give charity it's to help us overcome greed

160
00:20:57,080 --> 00:21:06,520
we practice morality it's to help us overcome anger mostly also greed and we practice meditation

161
00:21:06,520 --> 00:21:11,320
insight meditation to overcome delusion the clarity of mind

162
00:21:13,560 --> 00:21:21,480
it frees us from our ego and conceit and arrogance and wrong views and beliefs of all sorts

163
00:21:23,880 --> 00:21:29,640
all of this is the practice of the Buddhist teachings how we avoid evil

164
00:21:29,640 --> 00:21:41,080
when so when you give gifts you're training yourself in generosity and moreover you're seeing

165
00:21:41,080 --> 00:21:46,520
the benefits of generosity you're seeing able to see your attachments and how much suffering comes

166
00:21:46,520 --> 00:21:51,640
from it when you give something often you don't want to and you cringe and think oh I want that for

167
00:21:51,640 --> 00:21:57,800
myself or you feel like you want but you overcome that and by overcoming it you see with the real

168
00:21:57,800 --> 00:22:03,720
problem is the problem is not losing what you want the problem is this one thing that is really an

169
00:22:03,720 --> 00:22:13,160
evil unpleasant unwholesome state and when you practice morality you start to see how frustrated we

170
00:22:13,160 --> 00:22:19,720
get when when something doesn't go our way and now we want to lash out or hurt others

171
00:22:19,720 --> 00:22:28,680
and so it helps us cultivate patience and it helps us overcome anger we get to see the problem

172
00:22:28,680 --> 00:22:37,560
with anger because we don't follow it and we get to change and become more patient rather than

173
00:22:37,560 --> 00:22:44,040
attacking or hurting others or hurting ourselves and when we practice meditation we see all

174
00:22:44,040 --> 00:22:51,000
of this in much more clarity we see moment by moment how things arise we start to see patterns

175
00:22:51,000 --> 00:22:57,240
we see how our behavior is hurting ourselves very clearly because meditation is actually

176
00:22:57,240 --> 00:23:04,280
quite a simple thing but it can be the most traumatizing unpleasant experience because our minds

177
00:23:04,280 --> 00:23:14,920
are quite messed up for the moment if we can ever train them our minds become lazy and chaotic

178
00:23:16,440 --> 00:23:25,240
messy cluttered and so through the meditation practice we sort these out reminding ourselves

179
00:23:25,240 --> 00:23:30,520
this is this this is this this is this and it's about sorting things into what they really are

180
00:23:30,520 --> 00:23:40,440
and seeing the chaos of the mind and the automatic adjustment that comes from seeing the danger

181
00:23:40,440 --> 00:23:48,520
and the problems and things so this is our teaching tonight from the Dhammapada

182
00:23:49,480 --> 00:23:58,280
I think we'll stop it there basic teaching avoid evil avoid it like the plague avoid it like poison

183
00:23:58,280 --> 00:24:07,240
avoid it like an evil and it's a neat the um just one more little thing the the how it does relate

184
00:24:07,240 --> 00:24:12,440
to the story of the merchant it's about staying still about being centered this village it's kind

185
00:24:12,440 --> 00:24:18,760
of a nice analogy of staying in the village where peace and happiness reign instead of being

186
00:24:18,760 --> 00:24:26,280
greedy and going off and looking for more so that's about living our lives existentially in a sense

187
00:24:26,280 --> 00:24:37,000
instead of seeking out a pleasure or or chasing it or following after our version of following

188
00:24:37,000 --> 00:24:45,000
after desire or aversion stay stay present don't follow an evil path don't go looking for trouble

189
00:24:45,000 --> 00:24:58,280
so that's all it's a Dhammapada for tonight thank you all for tuning

