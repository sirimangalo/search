1
00:00:00,000 --> 00:00:27,400
Okay, we go on to finish the greatest person among the list with that, the last two parts

2
00:00:27,400 --> 00:00:50,160
of this person, Kaitanu, Kaitanu, Kaitanu, Kaitanu, Kaitanu, Kaitanu, Kaitanu,

3
00:00:50,160 --> 00:01:09,280
Katan Yutam means gratitude, khalena, tamma salvana means to hear the dhamma at the

4
00:01:09,280 --> 00:01:24,640
right time. Katan Yutam is something very important in Buddhism, which is maybe not

5
00:01:24,640 --> 00:01:46,200
talked about quite enough. But it is certainly a part of the Lord Buddha's teaching that

6
00:01:46,200 --> 00:01:52,560
we should be grateful for those people who have helped us. We should never forget the

7
00:01:52,560 --> 00:02:00,000
things, the good things that they have done to us, done for us. If you try our best to put

8
00:02:00,000 --> 00:02:06,440
aside the bad things which they have done and whatever good things someone else has done

9
00:02:06,440 --> 00:02:21,440
for us always try to remember those things. It doesn't mean that we have to be with

10
00:02:21,440 --> 00:02:39,560
the people who have done good things for us or even necessarily associate with them. Sometimes

11
00:02:39,560 --> 00:02:48,720
people have done good things for us, but also the sort of people who we shouldn't associate

12
00:02:48,720 --> 00:03:03,880
closely with. For instance with monks it is always difficult to associate with lay

13
00:03:03,880 --> 00:03:17,640
people. Sometimes it seems like the monks are very distant from the world and of course

14
00:03:17,640 --> 00:03:26,120
they have your for monks who have removed themselves from the world. Sometimes it is

15
00:03:26,120 --> 00:03:43,160
quite difficult because we depend on lay people as our support and yet it is prohibited

16
00:03:43,160 --> 00:03:52,560
for monks to repay the gratitude given by lay people, or repay the acts of kindness shown

17
00:03:52,560 --> 00:04:01,400
by lay people with similar acts of help. It is considered wrong livelihood for monks

18
00:04:01,400 --> 00:04:12,160
to barter with lay people with the supporters to one thing and then the monks turn around

19
00:04:12,160 --> 00:04:23,480
and support with labor or with gifts or so on in the same way. It is considered to be flattering

20
00:04:23,480 --> 00:04:41,160
or ingratiating oneself with lay people. As a monk we are content with whatever they get.

21
00:04:41,160 --> 00:04:55,040
Very happy to receive nothing and have to be content with nothing and so we are not allowed

22
00:04:55,040 --> 00:05:08,160
to ask for things or to return the favor in the way of a misap objects, external objects.

23
00:05:08,160 --> 00:05:15,160
Of course it is the job of the monks to turn around and teach to study intensively and

24
00:05:15,160 --> 00:05:22,960
to memorize. Sometimes for lay people the only duty is to come to practice and for monks

25
00:05:22,960 --> 00:05:30,840
it is necessary to study and to memorize them or do the teaching to pass it on. So they

26
00:05:30,840 --> 00:05:40,120
often are in a better position to teach as a result so they can go ahead and teach as a

27
00:05:40,120 --> 00:05:52,640
way of returning the favor. Sometimes we can't always show our gratitude due to the circumstances.

28
00:05:52,640 --> 00:05:57,520
We have to keep in our hearts a sense of gratitude for those people who helped us. First

29
00:05:57,520 --> 00:06:04,040
of all gratitude for our inner father. Even though sometimes we can't be with them sometimes

30
00:06:04,040 --> 00:06:17,240
they, even our mother and father might have wronged you. It is still somehow our duty not

31
00:06:17,240 --> 00:06:24,800
to be with them or even to necessarily to support them. It is somehow our duty to try our best

32
00:06:24,800 --> 00:06:32,040
to show them the way if possible. Sometimes it is not possible. Many times it does not

33
00:06:32,040 --> 00:06:41,880
come about. But sometimes if we are lucky it is possible. It is possible to bring about

34
00:06:41,880 --> 00:06:50,080
a change in our parents. Sorry I put this mother even to the last moment before Sarah

35
00:06:50,080 --> 00:06:59,400
Puta was going to pass away. She still held firmly and was very adamant that Sarah Puta had

36
00:06:59,400 --> 00:07:07,640
followed the wrong path by leaving behind the religion of the heat of the brahmins. And

37
00:07:07,640 --> 00:07:15,360
before he passed away Sarah Puta was very sick and went to see his mother. He managed to

38
00:07:15,360 --> 00:07:23,920
show her that the path that she had followed was truly the right path. In fact it was the

39
00:07:23,920 --> 00:07:32,800
path that even brahmins himself made the greatest perspective. And brahmins himself came

40
00:07:32,800 --> 00:07:49,200
to pay respect to Sarah Puta before he passed away. Sarah Puta was a very good example of gratitude.

41
00:07:49,200 --> 00:07:53,440
Not just in regards to his mother but in regards to anyone who had done something good

42
00:07:53,440 --> 00:07:59,600
for him. The way he showed it was very clearly in line with the Buddha's teaching. There

43
00:07:59,600 --> 00:08:08,280
was one man who came to be ordained. It was the man who had believed had disrobed six

44
00:08:08,280 --> 00:08:12,560
times. He had become a monk and disrobed and become a monk and disrobed in the seventh

45
00:08:12,560 --> 00:08:19,440
time. No one wanted to ordain him. Nobody would ordain him. And he came and he asked

46
00:08:19,440 --> 00:08:26,680
permission to ordain again. And the Lord Buddha knew that if he ordained the seventh

47
00:08:26,680 --> 00:08:41,360
time he would become an arahan. So he asked the monks, is there anyone who could think

48
00:08:41,360 --> 00:08:52,360
of something that this old man had done for them. And Sarah Puta spoke up and said,

49
00:08:52,360 --> 00:08:59,480
yes, one time when I was going by his house he put a spoonful of rice in my bowl.

50
00:08:59,480 --> 00:09:07,440
I remember something good that he had did for me. And the Lord Buddha said, well Sarah

51
00:09:07,440 --> 00:09:27,680
Puta, the new ordain him. And he said, sad to Sarah Puta, very good Sarah Puta. Because

52
00:09:27,680 --> 00:09:37,400
this is a sign of a good person who remembers even a small gift given to them. Even

53
00:09:37,400 --> 00:09:42,080
a small good deed someone does for them they remember. They don't ever forget the good

54
00:09:42,080 --> 00:09:52,560
deeds which one is done. Even a small good deed. And so he ended up ordaining and becoming

55
00:09:52,560 --> 00:10:01,720
an underserved puta and becoming an arahan. As a result of Sarah Puta's gratitude. She

56
00:10:01,720 --> 00:10:11,080
has one spoonful of rice. A gratitude isn't exactly what we're practicing here. We're

57
00:10:11,080 --> 00:10:18,560
not directly practicing gratitude but we can explain how this gratitude fit in with the

58
00:10:18,560 --> 00:10:24,960
meditation practice. It's important that every time we practice when we finish practicing

59
00:10:24,960 --> 00:10:34,080
when we spend a short period of time dedicating the merit which comes from the practice

60
00:10:34,080 --> 00:10:42,320
to our parents and our teachers. Teachers since we were young, to our relatives and our

61
00:10:42,320 --> 00:10:51,720
friends and our enemies and to all beings and all beings being free from suffering.

62
00:10:51,720 --> 00:10:58,760
If we do like this whenever we meet with danger anywhere we go. We have to try to remember

63
00:10:58,760 --> 00:11:08,800
every time to distribute the goodness which comes from the meditation practice. It's

64
00:11:08,800 --> 00:11:15,400
a way of developing gratitude in the heart. Developing a sense of gratitude for those

65
00:11:15,400 --> 00:11:27,200
people who have helped us in this way. And the last thing to talk about is Gala, indedhamma,

66
00:11:27,200 --> 00:11:37,120
samana. Do you hear the dhamma at the right time? Hearing the dhamma is something which

67
00:11:37,120 --> 00:11:46,560
is very difficult. Those of us who come from other countries we can appreciate this. Sometimes

68
00:11:46,560 --> 00:11:51,760
the Thai people don't appreciate it so much. How difficult it is. Which is natural because

69
00:11:51,760 --> 00:12:01,800
it's not so difficult in Thailand. But the difficulty doesn't just come from not living

70
00:12:01,800 --> 00:12:09,840
in a place where we can hear the dhamma. It also comes from the difficulty in finding

71
00:12:09,840 --> 00:12:18,280
a time to hear the dhamma. You will go to say, kalina. At a time when there is the dhamma

72
00:12:18,280 --> 00:12:23,760
or at the right at the time which is suitable it can also meet the fact that it's hard

73
00:12:23,760 --> 00:12:30,880
to find the time to hear the dhamma. Even people live in Thailand in the time of the

74
00:12:30,880 --> 00:12:36,000
Buddhist asana is very strong and they don't take the time. Even in the Buddhist time

75
00:12:36,000 --> 00:12:45,560
many people couldn't find the time to hear the dhamma. Or it can also mean in the time

76
00:12:45,560 --> 00:12:52,840
when there is the dhamma in the world. It's not that there's going to be a Buddha or a

77
00:12:52,840 --> 00:12:59,200
rise in the world every day. It's obvious that it's something very rare to find someone

78
00:12:59,200 --> 00:13:11,040
in a rise in the world who we could call a Buddha. Who is capable of teaching. It's

79
00:13:11,040 --> 00:13:20,560
very difficult. It is dhamma which is very difficult to see. It is very profound and deep

80
00:13:20,560 --> 00:13:33,480
and hard to understand. Take someone and say at least at the very minimum it would take

81
00:13:33,480 --> 00:13:44,240
four uncountable eons and a hundred thousand great eons on top of that. Just to become

82
00:13:44,240 --> 00:13:52,160
a Buddha which I talked about before when I talked about the Buddha, the dhamma, the sun.

83
00:13:52,160 --> 00:14:04,960
Enjoying that time. So other Buddha's a rise in pass away but so much of that time is

84
00:14:04,960 --> 00:14:14,480
full of this empty of the teaching of the Lord Buddha. So in this life we've had a chance

85
00:14:14,480 --> 00:14:20,720
to come to see the Lord Buddha, to see the teachings, to come to hear the teachings of the

86
00:14:20,720 --> 00:14:29,120
Lord Buddha. We shouldn't take it for granted. It's not something it's easy to find. Not

87
00:14:29,120 --> 00:14:37,040
just difficult to find in the world now but difficult to find this birth as a human being

88
00:14:37,040 --> 00:14:43,520
in the time of the Buddha's teaching. Buddha's teaching is something which doesn't arise in the

89
00:14:43,520 --> 00:14:51,920
world every day. It shouldn't be negligent. We've missed time for ourselves. We've wasted

90
00:14:51,920 --> 00:14:56,880
twenty five hundred years not coming to see the truth. Even though the Buddha's teaching has been

91
00:14:56,880 --> 00:15:03,600
around for twenty five hundred years we still haven't reached the goal which means all of us have

92
00:15:03,600 --> 00:15:10,800
been quite negligent. It's not that we have found a chance we should not waste our time being

93
00:15:10,800 --> 00:15:33,760
negligent anymore. So what does it mean to hear the Dhamma? The Dhamma of the Lord Buddha,

94
00:15:33,760 --> 00:15:38,560
there are eighty four thousand teachings according to Prananda who kept it in his mind with

95
00:15:38,560 --> 00:15:47,680
everything the Lord Buddha taught. But all together it comes down to three things.

96
00:15:50,000 --> 00:15:56,560
eighty four thousand teachings come down to the teaching on morality which means to refrain from bad

97
00:15:56,560 --> 00:16:04,720
deeds. Teaching on concentration which means to be full of goodness in the heart.

98
00:16:04,720 --> 00:16:13,440
And the teaching on wisdom which means to seek clearly the truth of suffering and the cause of

99
00:16:13,440 --> 00:16:21,840
suffering. The cessation of suffering and the path which leads out to the cessation of suffering.

100
00:16:21,840 --> 00:16:32,960
eighty four thousand teachings that all come down to only three things.

101
00:16:32,960 --> 00:16:40,800
A tambhukana sasa nam. Buddha's teaching all comes down to the three things.

102
00:16:44,800 --> 00:16:50,320
The morality morality doesn't necessarily mean keeping precepts. Although the rules which we keep

103
00:16:50,320 --> 00:16:56,400
are quite important. For the people it's important to keep it five precepts at least.

104
00:16:57,440 --> 00:16:59,680
If you're practicing meditation eight precepts.

105
00:17:01,760 --> 00:17:06,720
Novices have to keep ten monks have to keep 227.

106
00:17:10,080 --> 00:17:16,160
But these are simply worldly precepts. These are things which create a communal harmony.

107
00:17:16,160 --> 00:17:23,040
We can keep these precepts. They will create harmony in the monastery, harmony in the country,

108
00:17:23,040 --> 00:17:28,720
harmony in the world. They create a sort of establishment.

109
00:17:34,560 --> 00:17:42,720
But the morality which the Lord Buddha talked about was a sort of morality of mind or virtue of

110
00:17:42,720 --> 00:17:49,120
mind. Or the mind doesn't fall into evil states.

111
00:17:53,040 --> 00:17:59,360
When we watch right, when we watch the foot right goes, that goes, that's a lifting,

112
00:17:59,360 --> 00:18:03,360
putting, lifting, lifting, moving, putting and so on.

113
00:18:05,280 --> 00:18:09,920
When we guard the mind and don't let it stray from the object and keep bringing it back again and

114
00:18:09,920 --> 00:18:19,360
again to the object, this is morality. It's called the morality makanga.

115
00:18:22,160 --> 00:18:28,240
This is the factor of the path. Morality is a factor of the part of the path which we have to follow.

116
00:18:29,120 --> 00:18:32,880
Altogether there are eight parts or we can see three parts to the path.

117
00:18:34,000 --> 00:18:36,720
This is the morality part of the path which we have to follow.

118
00:18:36,720 --> 00:18:44,480
When we guard our minds, keep it from wandering away from the object, this is morality.

119
00:18:46,560 --> 00:18:50,640
And once we guard our minds in this way, there arises concentration.

120
00:18:52,160 --> 00:18:55,520
So most people think of concentration as fixing on one thing.

121
00:18:57,120 --> 00:19:02,240
This is a kind of concentration but it's a concentration in the world when our minds are fixed

122
00:19:02,240 --> 00:19:09,680
and focused on a single object. But this is impossible. It's impossible for this kind of

123
00:19:09,680 --> 00:19:16,880
concentration to lead to wisdom and the wisdom which the Lord would have needed us to see,

124
00:19:16,880 --> 00:19:23,280
which is impermanence. Until we can see impermanence, it's clear that we're not meditating

125
00:19:24,880 --> 00:19:29,680
correctly according to the Lord Buddhist teaching. We're still only practicing with the Lord

126
00:19:29,680 --> 00:19:36,480
Buddha called Samatat, which is a good thing but it's not correct as far as allowing us to see

127
00:19:36,480 --> 00:19:45,680
the truth of suffering as far. We have to come to have fixed concentration on everything

128
00:19:45,680 --> 00:19:51,840
which arises in sieces, when those things which are impermanent, those things which are real inside

129
00:19:51,840 --> 00:20:01,280
of ourselves. When we watch the foot moving or we watch the belly rising, we see rise,

130
00:20:02,960 --> 00:20:09,440
the mind fixes on that object. Why do we use the word to acknowledge? The mind fixes when we

131
00:20:09,440 --> 00:20:15,840
use the word as an aid and it fixes on them. Just like when you practice Samatat, you need

132
00:20:15,840 --> 00:20:22,640
to say Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha,

133
00:20:23,840 --> 00:20:29,440
you practice with us and you also have to fix but you have to fix on reality that's

134
00:20:29,440 --> 00:20:41,840
still in a different way. Concentration means you have to fix the mind, you use the word like

135
00:20:41,840 --> 00:20:48,480
a mantra almost. You fix on the ultimate reality to keep the mind from wandering because normally

136
00:20:48,480 --> 00:20:58,240
the mind is watching the phenomena arise and sees what it's watching with wrong view, with

137
00:20:58,240 --> 00:21:15,040
conceit and with that craving. Thinking that this is me, this is mine, this is under my control and so on.

138
00:21:15,040 --> 00:21:33,840
We'll see to ourselves rising from the mind is

