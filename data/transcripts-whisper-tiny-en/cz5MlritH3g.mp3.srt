1
00:00:00,000 --> 00:00:14,320
I'm a good evening, so today I'm going to be answering the question, my normal

2
00:00:14,320 --> 00:00:17,840
schedule is going to be.

3
00:00:17,840 --> 00:00:23,960
But on hold while I'm traveling, that I thought I'd take this opportunity to go over

4
00:00:23,960 --> 00:00:34,280
some of the questions that are languishing on our website without answers.

5
00:00:34,280 --> 00:00:39,560
So the top question for today is about anger.

6
00:00:39,560 --> 00:00:50,360
In some ways it's similar, it's a part of this category of questions that involve a frustration

7
00:00:50,360 --> 00:00:58,760
or a disappointment, a concern, even a doubt perhaps, as to why the meditation isn't bringing

8
00:00:58,760 --> 00:01:07,360
results, or even worse, why isn't bringing bad results.

9
00:01:07,360 --> 00:01:15,320
So the person describes how they practice a different way and something very strange

10
00:01:15,320 --> 00:01:20,920
happened, but they weren't afraid because they believed that meditation can't bring bad

11
00:01:20,920 --> 00:01:21,920
things.

12
00:01:21,920 --> 00:01:30,040
Well, but then they started practicing our way, according to my videos and book, I think,

13
00:01:30,040 --> 00:01:31,480
and bad things started happening.

14
00:01:31,480 --> 00:01:38,320
Well, I'm not sure that's exactly what they're getting at, but they find themselves angry,

15
00:01:38,320 --> 00:01:45,320
perhaps even more angry than before, angry at their friends, angry during meditation,

16
00:01:45,320 --> 00:01:46,320
perhaps.

17
00:01:46,320 --> 00:01:51,840
The very least anger isn't going away, it's not, when they practice meditation, they don't

18
00:01:51,840 --> 00:01:53,600
feel less angry.

19
00:01:53,600 --> 00:01:59,440
I've talked about why this can be, why meditation seems to bring bad things, can meditation

20
00:01:59,440 --> 00:02:01,320
bring bad things, right?

21
00:02:01,320 --> 00:02:07,120
So I'm going to try to not have too much overlap with that video, I won't delve too much

22
00:02:07,120 --> 00:02:14,000
into why, except maybe at the end, but I'd like to talk about a misconception about

23
00:02:14,000 --> 00:02:17,840
meditation, a couple of misconceptions.

24
00:02:17,840 --> 00:02:24,280
The first I think is that meditation can't bring bad things, and I think I may have

25
00:02:24,280 --> 00:02:30,760
mentioned this in my other video, but let's get a little deeper into this.

26
00:02:30,760 --> 00:02:40,920
One certainly can bring bad things, and I want to be very careful to qualify that statement,

27
00:02:40,920 --> 00:02:47,080
because we use the word meditation loosely, and that's dangerous.

28
00:02:47,080 --> 00:02:53,280
Well, that's problematic, I don't want to freak people out, they say meditation can cause

29
00:02:53,280 --> 00:02:57,920
problems, people won't want to meditate, but the point being that the word meditation

30
00:02:57,920 --> 00:03:04,280
could mean anything, it's very ill-defined, it's to find differently in different contexts,

31
00:03:04,280 --> 00:03:09,840
they cart, wrote a book called Meditations, I don't know what the French, I guess the French

32
00:03:09,840 --> 00:03:20,520
was, did it, that's, you know, I don't know, or it was in Latin, I think originally.

33
00:03:20,520 --> 00:03:26,680
And then there are many Christian talks, the word meditation is an English word, of course,

34
00:03:26,680 --> 00:03:35,520
and it comes from the Latin, probably meditary, I suppose, something like that.

35
00:03:35,520 --> 00:03:41,080
And so we have to be clear when we use the word meditation, what we mean.

36
00:03:41,080 --> 00:03:44,960
Now when I use it, I generally mean the meditation that I teach, and that's a very specific

37
00:03:44,960 --> 00:03:46,280
usage of the word, right?

38
00:03:46,280 --> 00:03:50,160
If I were to say it that way, then yes, in my other video, I said meditation can't

39
00:03:50,160 --> 00:03:55,800
lean to bad things, because when I say meditation, I mean my meditation, but that's a dangerous

40
00:03:55,800 --> 00:04:02,640
statement without qualifying it, because for most people meditation is all the same, for

41
00:04:02,640 --> 00:04:07,000
most people there's this conception that when you sit down and close your eyes, one thing

42
00:04:07,000 --> 00:04:11,440
and one thing only is gonna happen, it may not know exactly what that is, but it's all

43
00:04:11,440 --> 00:04:12,440
the same, right?

44
00:04:12,440 --> 00:04:16,080
Maybe different techniques, but the same thing is gonna happen, and that's just patently

45
00:04:16,080 --> 00:04:18,720
false.

46
00:04:18,720 --> 00:04:23,720
Many different things could happen, based on the meditation, based on your state of mind.

47
00:04:23,720 --> 00:04:31,700
Now I've outlined why I believe, and I'm quite sure that mindfulness meditation as the

48
00:04:31,700 --> 00:04:40,400
actual practice of cultivating mindfulness can't lead to bad things, but first of all, you

49
00:04:40,400 --> 00:04:44,600
have to be practicing mindfulness, and there are many other types of meditation that aren't

50
00:04:44,600 --> 00:04:51,800
mindfulness, according to our tradition, and then you have to be doing it properly, right?

51
00:04:51,800 --> 00:04:57,040
So bad things certainly can come by closing your eyes and focusing your mind in some way,

52
00:04:57,040 --> 00:05:02,440
which is really, I think, all that meditation means, you do something with your mind,

53
00:05:02,440 --> 00:05:08,680
you do something with your body, and some results come from it, but you could be doing

54
00:05:08,680 --> 00:05:09,680
anything, right?

55
00:05:09,680 --> 00:05:13,760
There's premeditative murder, which is a good example, it may not seem like it, but it

56
00:05:13,760 --> 00:05:21,840
actually is, because you can sit, close your eyes, and plot someone's demise, plot a murder.

57
00:05:21,840 --> 00:05:29,360
I think that's probably quite rare use of the word meditation, but there's many less extreme

58
00:05:29,360 --> 00:05:34,320
things you can do that are still bad, where you sit and you plot, you think evil thoughts,

59
00:05:34,320 --> 00:05:38,600
or you become conceited, sometimes you might close your eyes, even Buddhist meditators fall

60
00:05:38,600 --> 00:05:42,840
into this, you close your eyes and you think of how great you are, or how great Buddhism

61
00:05:42,840 --> 00:05:52,760
is, or how great meditation is, and you feel quite proud of it.

62
00:05:52,760 --> 00:06:00,680
So it's important to understand that meditation may not always be positive.

63
00:06:00,680 --> 00:06:06,480
I think it's maybe a little disingenuous to talk about negative meditations, but there's

64
00:06:06,480 --> 00:06:13,080
another category, and that's the category of meditations that are based on a conceptual

65
00:06:13,080 --> 00:06:18,400
object, and it might not be familiar, it should be familiar to most of my students, but

66
00:06:18,400 --> 00:06:24,880
this idea that you would focus on a person, a place, a thing, even an element, like some

67
00:06:24,880 --> 00:06:32,040
kind of concept, a really common one is something like a flame, you might focus on a flame.

68
00:06:32,040 --> 00:06:39,040
Because a flame is a thing, it's actually, you don't ever see a flame, you see a light,

69
00:06:39,040 --> 00:06:43,720
there's an experience of seeing, but in your mind you say that's a flame, as opposed

70
00:06:43,720 --> 00:06:49,760
to being anything else, so it is conceptual, and because it's conceptual, it can connect

71
00:06:49,760 --> 00:06:53,840
with other concepts, it exists in your mind.

72
00:06:53,840 --> 00:06:57,960
Why we stick to very simple things like fires like flame is because it's actually a little

73
00:06:57,960 --> 00:07:02,760
bit difficult for it to connect with other concepts if you're strict, and if you have

74
00:07:02,760 --> 00:07:10,960
a teacher, most especially, you're likely able to just stick with fire, but do it over

75
00:07:10,960 --> 00:07:15,640
time, especially without a teacher, and you find it morphs into something else, because

76
00:07:15,640 --> 00:07:22,520
it connects with your other concepts in the mind, hell, perhaps, magic, perhaps, and

77
00:07:22,520 --> 00:07:27,960
strain things start to happen, you'll find it morphs, and it evolves, and because you're

78
00:07:27,960 --> 00:07:34,040
not mindful, you follow the evolution, you think this is the path, maybe it's going somewhere,

79
00:07:34,040 --> 00:07:39,320
and it can lead you to good things, it can lead you apparently to magical powers, but it

80
00:07:39,320 --> 00:07:48,760
can also lead you to bad things, if you have problematic imagination and emotions, you might

81
00:07:48,760 --> 00:07:53,880
start to get afraid, and you caught up in the fear, and I think this person who practiced

82
00:07:53,880 --> 00:07:57,520
his mindfulness of breathing and something happens, and they're not afraid, I think that

83
00:07:57,520 --> 00:08:04,160
is quite helpful, because fear can lead you in very bad direction, but the opposite

84
00:08:04,160 --> 00:08:10,600
is also true, without caution, without fear, you can find yourself developing a specific

85
00:08:10,600 --> 00:08:17,600
habit that may be quite particular to your mind, and can lead you in all sorts of directions,

86
00:08:17,600 --> 00:08:24,240
I mean, I've seen people with great confidence go crazy temporarily, because they just

87
00:08:24,240 --> 00:08:29,200
didn't have any real mindfulness, they didn't have a clear understanding of what they're

88
00:08:29,200 --> 00:08:35,960
doing, their meditation wasn't based on experiences, it was based on concepts, but because

89
00:08:35,960 --> 00:08:41,360
they were so confident, they developed this, and they developed it to such a point that

90
00:08:41,360 --> 00:08:46,920
they just kind of got lost in it, got stuck in it, because meditation does cultivate

91
00:08:46,920 --> 00:08:51,760
habits, and once you've cultivated a habit, it's very difficult to change it, it's not

92
00:08:51,760 --> 00:09:03,840
impossible of course, you can't go crazy irrevocably, but certainly quite powerfully,

93
00:09:03,840 --> 00:09:11,160
and it can be quite difficult to get out of the insanity, so not exactly related to the

94
00:09:11,160 --> 00:09:19,200
question, but an important preface. The more important issue here about this question

95
00:09:19,200 --> 00:09:25,840
that this person is asking is this sense that is quite common for meditators, and it's

96
00:09:25,840 --> 00:09:34,840
another, I would say, misunderstanding, misconception about meditation, is that meditation

97
00:09:34,840 --> 00:09:46,600
is a direct path to happiness and purity, let's say, so I think there's two things here,

98
00:09:46,600 --> 00:09:55,760
happiness, meaning states that are comfortable, peaceful, pleasant, enjoyable, perhaps,

99
00:09:55,760 --> 00:10:04,200
and purity being states that are free from anger, for example, also lust or greed, confusion

100
00:10:04,200 --> 00:10:10,040
doubt, worry, fear, all of these things. That meditation, when you sit down and close your

101
00:10:10,040 --> 00:10:15,960
eyes and you practice correctly, it should be like clicking a switch, or maybe not

102
00:10:15,960 --> 00:10:24,280
picking the switch, but it should be a clear tasting of these two qualities, these two states

103
00:10:24,280 --> 00:10:31,680
that have these two qualities, purity and happiness. So the analogy I was thinking of

104
00:10:31,680 --> 00:10:38,680
is it's similar to eating a bowl of rice, so we think of meditation as like eating a

105
00:10:38,680 --> 00:10:45,160
bowl of rice, you have rice, you cook the rice and you eat it. Quite simple, meditation

106
00:10:45,160 --> 00:10:51,880
is not, and by meditation I mean our meditation, if I haven't confused you, let's be

107
00:10:51,880 --> 00:10:57,280
clear here, now I'm switching, I'm no longer saying meditation could be anything, I'm

108
00:10:57,280 --> 00:11:02,760
thinking about mindfulness meditation, mindfulness meditation is not like that, it's not like

109
00:11:02,760 --> 00:11:07,720
eating a bowl of rice. There are a lot of things that are like eating a bowl of rice,

110
00:11:07,720 --> 00:11:19,600
not just meditations, and I think the perception or the search, our inclination to find

111
00:11:19,600 --> 00:11:25,000
things that are like eating a bowl of rice leads us to taking drugs, medication, I think

112
00:11:25,000 --> 00:11:32,560
there's a real problem with our reliance upon drugs, which are very much a bowl of rice.

113
00:11:32,560 --> 00:11:42,760
You take the drugs, yes, they do have a mean intermediate and direct result. Even just

114
00:11:42,760 --> 00:11:50,800
enjoyment of sensual pleasures, which means any kind of sight or sound or taste or smell

115
00:11:50,800 --> 00:11:58,920
or feeling, even a thought, you know, if you read fiction or fantasize about things, all

116
00:11:58,920 --> 00:12:05,040
of those are like eating a bowl of rice, you have the rice and you feel good, and goodness

117
00:12:05,040 --> 00:12:12,400
comes from it. The problem, and this analogy is not going to be perfect, but let's take

118
00:12:12,400 --> 00:12:19,840
it somewhere, the problem is that eventually you run out of rice, and suppose you have

119
00:12:19,840 --> 00:12:24,120
a family, and you say, okay, well this rice will just eat it, and you spend all your

120
00:12:24,120 --> 00:12:29,320
time cooking and eating the rice and just enjoying life because you got this rice, but then

121
00:12:29,320 --> 00:12:37,080
you run out of rice, and then you have to suffer. You have no way of getting more rice.

122
00:12:37,080 --> 00:12:43,160
That's assuming, we're putting aside money in a very simple sense, in a very simple setting.

123
00:12:43,160 --> 00:12:47,200
You ran out of rice, and now you're in trouble. You're going to suffer because you have

124
00:12:47,200 --> 00:12:54,000
no rice, so there's something better you could have done, and maybe you can see where this

125
00:12:54,000 --> 00:13:01,440
analogy is coming. To get more rice without money, we're putting aside the whole money

126
00:13:01,440 --> 00:13:09,680
economy. You have to go and harvest rice, and so you say, oh, well, there we go, and

127
00:13:09,680 --> 00:13:14,360
I'm out of rice, so I'll go and harvest more rice. Now harvesting rice is work that you

128
00:13:14,360 --> 00:13:26,880
have to do, and so I think you could stretch this analogy to talk about tranquility meditation.

129
00:13:26,880 --> 00:13:33,880
Tranquility meditation might be like harvesting rice. I don't know, maybe not. Let's just

130
00:13:33,880 --> 00:13:37,720
put it this way, that harvesting rice isn't still enough because the person then they say,

131
00:13:37,720 --> 00:13:41,560
well, we run out of rice, let's go and harvest rice. Okay, we go out and, oh, our field is full

132
00:13:41,560 --> 00:13:49,520
of rice. Well, if they're lucky, that's a good thing. And so we might think of meditation

133
00:13:49,520 --> 00:13:56,080
that way, that it's like harvesting rice, and if you harvest rice, you get more rice, and

134
00:13:56,080 --> 00:14:00,520
then you can eat it, and you can enjoy it. Meditation still isn't like that, and there's

135
00:14:00,520 --> 00:14:05,960
still a problem, and it does hold with mindfulness meditation. The problem with harvesting

136
00:14:05,960 --> 00:14:13,640
rice is you run out of rice to harvest as well. Meditation is mindfulness meditation. It's

137
00:14:13,640 --> 00:14:22,760
much more like planting rice. I think it's a fairly good analogy, and I'll explain why you

138
00:14:22,760 --> 00:14:28,520
plant rice, and then you have rice to harvest. But one thing about planting rice, if you'll

139
00:14:28,520 --> 00:14:36,040
notice, you actually need rice to plant rice. So I don't know how that fits in, but into

140
00:14:36,040 --> 00:14:41,400
our analogy. But what does fit is the fact that you have to sacrifice happiness to practice

141
00:14:41,400 --> 00:14:50,240
mindfulness meditation quite often. And I think perhaps always, let's just say practically

142
00:14:50,240 --> 00:14:55,720
speaking, everyone has to sacrifice happiness in order to practice meditation. And there's

143
00:14:55,720 --> 00:14:59,640
a reason for that, and I've talked about that in other videos. The reason is because you're

144
00:14:59,640 --> 00:15:05,840
trying to understand why you're suffering. You're trying to understand what you're doing

145
00:15:05,840 --> 00:15:13,160
wrong. You're trying to change your perspective on the things that cause you're suffering.

146
00:15:13,160 --> 00:15:21,600
So mindfulness is not a practice where you can just sit and enjoy it. It's something

147
00:15:21,600 --> 00:15:27,840
you have to do in order to grow rice, and then in order to eat it. And the growing rice,

148
00:15:27,840 --> 00:15:35,280
in meditation practice, once you practice mindfulness, many bad things come. Bad things come

149
00:15:35,280 --> 00:15:44,640
because you're physically changing your behavior. Meditation, mindfulness meditation

150
00:15:44,640 --> 00:15:51,560
involves physical postures, sitting still, walking back and forth. And that's quite uncomfortable

151
00:15:51,560 --> 00:15:58,320
both physically and mentally. It can be. Not always, but it can be. And when it is, well,

152
00:15:58,320 --> 00:16:05,520
that's a bad thing that comes. It's not like just enjoying, and you can't just enjoy meditation

153
00:16:05,520 --> 00:16:12,920
because you're going to feel pain. Mentally, you're going to observe the pain. You're not

154
00:16:12,920 --> 00:16:21,280
going to try to distract yourself with something pleasant. But as you practice meditation,

155
00:16:21,280 --> 00:16:27,360
just like planting rice, something changes. You start to grow something that has a great

156
00:16:27,360 --> 00:16:35,720
potential to feed you, a great potential to bring you happiness and bring you purity.

157
00:16:35,720 --> 00:16:41,680
So the practice of mindfulness, just like planting rice, it doesn't lead even directly

158
00:16:41,680 --> 00:16:48,560
to happiness. Sometimes it does. It can't. But let's say it doesn't lead ever directly

159
00:16:48,560 --> 00:16:54,840
because there's something in between. And what is in between is the understanding, the

160
00:16:54,840 --> 00:17:05,320
insight, that the result of mindfulness, which is actually, it's quite immediate. And

161
00:17:05,320 --> 00:17:11,760
that's why I say sometimes it can be quite pleasant. It actually isn't, but it is through

162
00:17:11,760 --> 00:17:20,360
the intermediary. Once you are mindful, like even just a moment, sometimes you'll find

163
00:17:20,360 --> 00:17:25,160
yourself very mindful of pain, feel the pain in your very mindful of it. And then you

164
00:17:25,160 --> 00:17:32,600
find yourself free from it. Find yourself free from anger over it. But the intermediary

165
00:17:32,600 --> 00:17:40,080
was inside. The intermediary was seen clearly. It was Vipassana. Inside again, I think

166
00:17:40,080 --> 00:17:48,320
is a bit misleading. But it's the clear seeing that comes from observing. And clear

167
00:17:48,320 --> 00:17:54,040
seeing, I think, is more involved in just a simple moment of seeing clearly. It also can

168
00:17:54,040 --> 00:18:02,240
involve sort of complex states of realizing how wrong you are seeing things. And so it can

169
00:18:02,240 --> 00:18:10,760
be an involved process. And that's why often, over time, only, are you able to see results?

170
00:18:10,760 --> 00:18:20,120
But the important point, I think, is that we don't see meditation as a direct link to

171
00:18:20,120 --> 00:18:30,560
happiness and purity. Meditation is about unlearning, observing and learning about our bad habits

172
00:18:30,560 --> 00:18:41,800
and learning new habits, better habits. Those habits will grow in our mind, like rice

173
00:18:41,800 --> 00:18:48,920
plants, but we have lots of weeds, you might say. They can't go out to a field of weeds

174
00:18:48,920 --> 00:18:55,040
looking for rice. Can't even go out into a field of weeds with a sickle or a knife or whatever

175
00:18:55,040 --> 00:19:10,920
it is. And cut down the weeds and try to get rice to cook. You need to plant. So it's

176
00:19:10,920 --> 00:19:17,680
a three-step process, really. Meditation is the mindfulness meditation. It's not something

177
00:19:17,680 --> 00:19:24,160
that we should ever think of as bringing us directly happiness or purity. Meaning, when

178
00:19:24,160 --> 00:19:27,960
you're mindful, you might feel very angry. You might find that mindfulness makes you more

179
00:19:27,960 --> 00:19:32,760
angry. Why? Again, you're sitting still, very still. And we don't like that. That makes

180
00:19:32,760 --> 00:19:40,280
us angry. You're observing the experiences, you're facing them, head on, and we don't like

181
00:19:40,280 --> 00:19:50,080
that. We want to avoid them. We want to run away from them. So that makes us angry.

182
00:19:50,080 --> 00:19:59,080
And that can translate into angry states during your life. You normally, when someone

183
00:19:59,080 --> 00:20:02,680
would interact with you, you would engage with them. And you would do something in a way

184
00:20:02,680 --> 00:20:09,760
that makes you happy, like eating your rice. When you're mindful, you might find it frustrating.

185
00:20:09,760 --> 00:20:14,480
You might find that, hey, this person is interrupting my meditation or making it difficult

186
00:20:14,480 --> 00:20:17,960
for me to meditate. You might feel stressed because it's just very hard to be mindful

187
00:20:17,960 --> 00:20:27,240
when you're in a social situation. You might find your lack of entertainment, your lack

188
00:20:27,240 --> 00:20:32,080
of enjoyment that comes from being mindful, frustrates you when you're with other people.

189
00:20:32,080 --> 00:20:37,200
And so you find yourself just frustrated all the time and displeased and depressed, even.

190
00:20:37,200 --> 00:20:42,240
You might feel like meditation makes you more depressed. It's not the fault of meditation.

191
00:20:42,240 --> 00:20:54,320
I mean, it's the fault of our weedy, barren field, our field is barren of clarity, of purity.

192
00:20:54,320 --> 00:20:59,120
And we have to cultivate that. But make no mistake, and there should be no doubt, there

193
00:20:59,120 --> 00:21:05,840
can be no doubt that mindfulness does lead to clarity, which does lead to purity, which

194
00:21:05,840 --> 00:21:10,800
does lead to happiness. Why can there be no doubt is because of the nature of the exercise,

195
00:21:10,800 --> 00:21:19,320
the nature of the practice. Mindfulness is reminding yourself of how things are. It's

196
00:21:19,320 --> 00:21:25,320
about creating a perspective. It is what it is. It's quite simple, and it's quite direct.

197
00:21:25,320 --> 00:21:30,760
It's quite impossible. I mean, most people, it's very hard for us because we're not used

198
00:21:30,760 --> 00:21:38,560
to thinking in this way, but for someone who's practiced, it's clear. It's perfectly clear

199
00:21:38,560 --> 00:21:44,040
that there is no doubt. There can be no doubt about the practice. It's a very simple and

200
00:21:44,040 --> 00:21:53,520
clear and obvious cause and effect practice. You remind yourself, and you cultivate a perspective

201
00:21:53,520 --> 00:21:57,840
of it is what it is, as opposed to it's bad, it's good, it's me, it's mine, anything

202
00:21:57,840 --> 00:22:03,600
that might cause reactions. And as a result, you see things clearly. The only result could

203
00:22:03,600 --> 00:22:13,000
be a way of seeing that is clear, that is pure, and that transforms so much. It pulls up

204
00:22:13,000 --> 00:22:23,360
all the weeds in the field. It plants so many productive, fruitful rice plants, and the

205
00:22:23,360 --> 00:22:33,160
first out can only be rice. Of course, there can be pests that come in. In Thailand, they

206
00:22:33,160 --> 00:22:39,120
have to worry about crabs. The crabs will cut the rice plants, mice as well. Of course,

207
00:22:39,120 --> 00:22:44,120
they kill the mice and the crabs. But the analogy holds, we have to sometimes kill mice

208
00:22:44,120 --> 00:22:52,320
and crabs in our minds or in our lives. Meaning we have to, and it, no, you don't have

209
00:22:52,320 --> 00:22:58,360
to actually kill mice and crabs. You shouldn't, that's bad. But things in your life

210
00:22:58,360 --> 00:23:01,920
that you have to eliminate in order to progress in the practice, things that you have

211
00:23:01,920 --> 00:23:10,160
to cultivate, sometimes you need fences to keep things out. And fences are things like

212
00:23:10,160 --> 00:23:15,160
their other meditation practices. Morality ethics is a very good thing, keeping the five

213
00:23:15,160 --> 00:23:22,400
or eight presets. Even just things like our techniques, like walking and sitting meditation

214
00:23:22,400 --> 00:23:26,040
are very good waves. Because someone might say, oh, I can just be mindful, I don't need

215
00:23:26,040 --> 00:23:34,280
to do actual walking or sitting meditation. I'll watch out for the crabs and the fungus

216
00:23:34,280 --> 00:23:43,920
that might come and blight your field. But this idea that meditation, we should be discouraged

217
00:23:43,920 --> 00:23:49,960
because meditation is bringing bad things. I think a product of this sort of misunderstanding

218
00:23:49,960 --> 00:23:59,200
of what meditation is. A good idea of why, why I shouldn't, why isn't mindfulness?

219
00:23:59,200 --> 00:24:03,640
Why wouldn't it be just be easier to have a practice that does just lead to directly

220
00:24:03,640 --> 00:24:10,600
to happiness? Quite simply because we're dealing with the problems. It's like a physician,

221
00:24:10,600 --> 00:24:17,120
a physician doesn't just give you medication to make you feel good. They find somewhere

222
00:24:17,120 --> 00:24:23,680
like a person has cancer. Cancer treatments can be quite painful. And it's quite simply

223
00:24:23,680 --> 00:24:27,560
because you're dealing with the problem. You're getting involved in the problem. And

224
00:24:27,560 --> 00:24:33,440
the problem is painful. The problem is unpleasant until you can cure and fix the problem

225
00:24:33,440 --> 00:24:46,520
you won't have happiness. It's quite a simple equation or a simple framework of not seeking

226
00:24:46,520 --> 00:24:55,280
out the desired results. But focusing on what it is that's stopping you from achieving

227
00:24:55,280 --> 00:25:03,160
the desired result, which physicians do. Some oncologists would do for a cancer patient.

228
00:25:03,160 --> 00:25:08,800
The meditation teacher does for a meditator. They help them to focus on what's causing

229
00:25:08,800 --> 00:25:17,520
them stress and suffering, what's causing the anger. And so that can be quite unpleasant.

230
00:25:17,520 --> 00:25:26,160
It's the nature of things. And so, anyway, I hope that helped. That's the answer for that

231
00:25:26,160 --> 00:25:30,560
question. I'll try and make these regularly now, at least during this month. Next month,

232
00:25:30,560 --> 00:25:36,640
I'm going to be away probably. Well, no promises on videos, but thank you all for tuning

233
00:25:36,640 --> 00:25:39,920
in. I'll show you the best.

