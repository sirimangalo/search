1
00:00:00,000 --> 00:00:11,000
Okay, trying again.

2
00:00:11,000 --> 00:00:17,500
Looks good.

3
00:00:17,500 --> 00:00:28,660
So tonight I was thinking about a verse, a dumb upon the verse, I think, that a rogue

4
00:00:28,660 --> 00:00:41,660
say a paramalabha, freedom from sickness, freedom from illness is the greatest gain.

5
00:00:41,660 --> 00:00:51,080
Sun to tea, paramang dhanang, contentment is the greatest wealth.

6
00:00:51,080 --> 00:01:03,780
Every sasa paramang dhanang dhanang, paramang dhanang, paramang dhanang, the familiarity is the greatest

7
00:01:03,780 --> 00:01:20,780
relation, mibhanang paramang sukhan, mibhan is the highest happiness.

8
00:01:20,780 --> 00:01:28,740
Thinking about this verse, you can really get a sense of the Buddha saying this, looking

9
00:01:28,740 --> 00:01:36,760
out on the community of monks living in the forest, reflecting on all that he'd been

10
00:01:36,760 --> 00:01:53,560
through to become a Buddha, and expressing his, expressing his perception, his outlook

11
00:01:53,560 --> 00:01:59,560
on the world in this way.

12
00:01:59,560 --> 00:02:05,760
A roguea means not having roguea, roguea means that would root chate that which disturbs

13
00:02:05,760 --> 00:02:25,540
you afflict you, affliction, the greatest gain, the greatest possession, the greatest gain

14
00:02:25,540 --> 00:02:37,120
actually, lava means gain, ask anyone who's physically sick, physically ill, that they

15
00:02:37,120 --> 00:02:52,600
want money, fame, glory, mansion, power, all the riches in all the world that won't save

16
00:02:52,600 --> 00:02:54,280
you from sickness.

17
00:02:54,280 --> 00:03:02,360
The four kinds of sickness in Buddhism, there's sickness that comes from food, sickness

18
00:03:02,360 --> 00:03:11,320
that comes from the environment, sickness that comes from karma, and sickness that comes

19
00:03:11,320 --> 00:03:17,960
from the mind.

20
00:03:17,960 --> 00:03:24,960
Actually, it's quite likely that the Buddha wasn't thinking of physical illness when he said

21
00:03:24,960 --> 00:03:25,960
this.

22
00:03:25,960 --> 00:03:32,000
I think we can all agree that if you're physically unwell, all the material possessions

23
00:03:32,000 --> 00:03:46,840
in the world are not going to compare to the gain of becoming free from that sickness,

24
00:03:46,840 --> 00:03:53,920
spend a lot of money and a lot of effort just to free ourselves from illness, everything

25
00:03:53,920 --> 00:03:59,480
else becomes meaningless because you can't of course enjoy any of the pleasures or riches

26
00:03:59,480 --> 00:04:04,240
or gains, accomplishments in life if you're physically ill.

27
00:04:04,240 --> 00:04:11,400
But I don't think that was exactly what the Buddha was talking about, I mean, it actually

28
00:04:11,400 --> 00:04:19,640
isn't the greatest gain, not in the Buddhist sense, because you have to understand sickness

29
00:04:19,640 --> 00:04:27,000
of the mind, you have to understand the full range of sickness, and person can be perfectly

30
00:04:27,000 --> 00:04:35,920
healthy in the body, but if they're mind is ill, physical body, physical health does them

31
00:04:35,920 --> 00:04:42,160
no good either.

32
00:04:42,160 --> 00:04:50,480
Sickness from food, sickness from the environment, these are physical.

33
00:04:50,480 --> 00:04:55,800
Sickness from karma, well that can be physical, can be mental.

34
00:04:55,800 --> 00:05:01,360
But sometimes we're born with diseases, with illnesses, physical illnesses, some people

35
00:05:01,360 --> 00:05:15,080
are born deficient in some way in the body, some people are born with all kinds of illness,

36
00:05:15,080 --> 00:05:23,760
some people die, some humans die shortly after childbirth because they're so ill.

37
00:05:23,760 --> 00:05:29,840
This can be because of karma, I mean sickness because of food is quite obvious, you

38
00:05:29,840 --> 00:05:34,840
eat too much, you eat the wrong kinds of food, this is quite clear, and the environment

39
00:05:34,840 --> 00:05:47,160
could be anything from bacteria, viruses, infections, radiation, perhaps, chemicals, now

40
00:05:47,160 --> 00:05:55,440
everything's got these terrible chemicals in it, making us all sick.

41
00:05:55,440 --> 00:06:00,920
The karma, that one's not so clear, karma is the Buddhist answer to why we're born

42
00:06:00,920 --> 00:06:07,480
certain ways, and in fact not even karma, not just karma, right, because while the environment

43
00:06:07,480 --> 00:06:15,280
can have a factor, play a part, karma certainly plays a part, but it's much more complicated

44
00:06:15,280 --> 00:06:23,520
than that, sometimes we perform certain karma, and as a byproduct, bad or good things

45
00:06:23,520 --> 00:06:28,400
happen, that are totally, I mean things that we like, but more totally had nothing to do

46
00:06:28,400 --> 00:06:39,520
with the karma, right, like perhaps you did some very bad things, and as a result you

47
00:06:39,520 --> 00:06:48,760
have this bad karma in your life, but as a result, let's say a person who is very poor

48
00:06:48,760 --> 00:06:56,160
and so they have to go and live off in the countryside, and then there's a fire that

49
00:06:56,160 --> 00:07:02,600
burns down the city, right, let me just say, the point being that there are circumstances

50
00:07:02,600 --> 00:07:07,400
or results that don't always have to do with karma, I'm just trying to say not everything

51
00:07:07,400 --> 00:07:12,520
is karma, this person who was very poor because they were maybe very stingy, so they

52
00:07:12,520 --> 00:07:20,600
all look, it's a very bad thing, but actually they got quite lucky because they meant that

53
00:07:20,600 --> 00:07:27,000
they didn't meet with the same fate, or rich people who as a result of being rich, someone

54
00:07:27,000 --> 00:07:35,880
comes and steals stuff from them, doesn't have to be past karma, I'm going to do all sorts

55
00:07:35,880 --> 00:07:41,120
of good karma to become rich, and then the vicissitudes of life that may have nothing

56
00:07:41,120 --> 00:07:47,440
to do with karma, just take it all away, it can happen, it's quite complicated as the

57
00:07:47,440 --> 00:07:59,200
point, but karma certainly plays a part in this life and from life to life as an extension,

58
00:07:59,200 --> 00:08:05,200
but it's, I think you think we all get a sense that it's the mental illness that the

59
00:08:05,200 --> 00:08:13,840
Buddha was really thinking of or that it's most important here, the greatest gain, and

60
00:08:13,840 --> 00:08:22,720
it's a gain, you see, because we're all sick in the mind until we become free from that

61
00:08:22,720 --> 00:08:36,080
sickness, till we gain that greatest gain, becoming free from greed, free from anger, free

62
00:08:36,080 --> 00:08:47,120
from arrogance, conceit, free from attachment and addiction, free from views and opinions

63
00:08:47,120 --> 00:08:55,920
and beliefs and free from our, free from the things that keep us stressed and suffering

64
00:08:55,920 --> 00:09:04,120
in our minds, you know, like depression is a mental illness or anxiety, but arrogance

65
00:09:04,120 --> 00:09:14,160
and conceit, they are also mental illnesses, addiction is a mental illness, anger, anger

66
00:09:14,160 --> 00:09:25,800
is a mental illness, so that's the first part of the verse, freedom from illness, what's

67
00:09:25,800 --> 00:09:31,560
the greatest gain, becoming free from this sickness, someone was telling me this is like

68
00:09:31,560 --> 00:09:42,320
a hospital, we're all like sick patients here, it's quite true, the real problem is all those

69
00:09:42,320 --> 00:09:47,640
people out there who shouldn't be in the hospital and are running free, contaminating each

70
00:09:47,640 --> 00:09:53,720
other, spreading their sickness to each other, really, right, it's no exaggeration, there

71
00:09:53,720 --> 00:10:00,080
are sick people out there doing sick things and spreading sickness, they really should

72
00:10:00,080 --> 00:10:11,760
find the hospital, find a doctor like the Buddha,

73
00:10:11,760 --> 00:10:19,960
one to deep, but among them, imagine the Buddha living off in the forest, content with

74
00:10:19,960 --> 00:10:28,000
no, with no luxuries, no possessions, the monks aren't even allowed to touch money, the

75
00:10:28,000 --> 00:10:35,120
greatest possession is contentment, looking out at the people, these people content, I mean

76
00:10:35,120 --> 00:10:41,480
how content are you, it might feel quite discontent in the beginning, but how great

77
00:10:41,480 --> 00:10:48,760
is it to be able to be here, how powerful is it to not have to go and seek out entertainment

78
00:10:48,760 --> 00:11:00,960
or diversion, how great it is that you can come and be at peace, and you can see how

79
00:11:00,960 --> 00:11:09,240
challenging it is, right, I mean, we're quite poor in our desires for things, people in

80
00:11:09,240 --> 00:11:17,360
society are very poor because they're always wanting, they never have all that they want,

81
00:11:17,360 --> 00:11:24,920
and you come here and you realize how poor we are, because we don't have this contentment,

82
00:11:24,920 --> 00:11:35,320
when we sit and we're displeased, we're unsatisfied, we don't want something, anything,

83
00:11:35,320 --> 00:11:45,640
we're lacking, we don't have all that we want, and so the Buddha looked out on all

84
00:11:45,640 --> 00:11:52,200
the hour hunts sitting around him and he thought about his own trials, and now he had become

85
00:11:52,200 --> 00:11:59,200
content, and he said this is the greatest, the greatest possession, if you're content

86
00:11:59,200 --> 00:12:05,680
you don't need anything, right, you never want for anything, there's no question that

87
00:12:05,680 --> 00:12:10,760
that is the greatest possession, there's no other possession that can make you satisfied

88
00:12:10,760 --> 00:12:20,320
like contentment, it's the second part, the third part of the verse, we saw us up in

89
00:12:20,320 --> 00:12:26,680
Paramahnyati, I thought about this one, and it was kind of strange, I think, well, what's

90
00:12:26,680 --> 00:12:34,960
so important about that, but then you think about where the Buddha was when he said this,

91
00:12:34,960 --> 00:12:42,040
far, far away from his father and his mother, then his family, but surrounded by people

92
00:12:42,040 --> 00:12:56,640
like us, meditators, monastics, spiritual beings, we saw some means, sorry I didn't translate

93
00:12:56,640 --> 00:13:03,920
it, we saw some means familiarity, but it has a sense of the idea, we saw some relates

94
00:13:03,920 --> 00:13:13,560
to, if I have certain possessions, and I say to you, hey, you're welcome to take some

95
00:13:13,560 --> 00:13:18,880
of, you're welcome to take anything you want, don't have to ask me, I don't remember

96
00:13:18,880 --> 00:13:24,040
what the word is, if there is a better word than familiarity, but it means this sense

97
00:13:24,040 --> 00:13:32,560
of taking each other for granted, or being able to take each other for granted, and taking

98
00:13:32,560 --> 00:13:43,240
it for granted, what's mine is yours, what's yours is mine, that kind of thing.

99
00:13:43,240 --> 00:13:49,280
So it's like when we talk about Dhamma brothers and sisters, our Dhamma relatives,

100
00:13:49,280 --> 00:14:02,320
they say in Dhamma, Dhamma, so Dhamma relatives, Dhamma family, and so all of you, even

101
00:14:02,320 --> 00:14:09,800
here in second life, all of you on YouTube were part of an extended family, all the

102
00:14:09,800 --> 00:14:20,160
some of us are, you know, like any family at different stages of maturity, but we're all

103
00:14:20,160 --> 00:14:27,560
a family, and the greatest, the greatest families is this when we, when we're in harmony,

104
00:14:27,560 --> 00:14:34,960
you know, I can depend upon all of you to not steal my things, or break my things, or

105
00:14:34,960 --> 00:14:41,280
to hurt me, or to lie to me, or to cheat me, the most wonderful thing, or a wonderful

106
00:14:41,280 --> 00:14:48,040
thing about being a Buddhist monk is that for the most part you're surrounded by good people.

107
00:14:48,040 --> 00:14:51,360
I'm always shocked when I find out someone was lying to me, because people don't lie

108
00:14:51,360 --> 00:14:57,440
to me that often, not that I know, but I think generally lie to me a lot less than they

109
00:14:57,440 --> 00:15:03,520
do to others, and I can depend upon that, and that's so great, you know, that's the most

110
00:15:03,520 --> 00:15:11,040
wonderful, I can't really depend on my family, I don't know if they'll lie to me or not,

111
00:15:11,040 --> 00:15:16,360
but in being we, we are born into certain families, or a certain, we're born into a certain

112
00:15:16,360 --> 00:15:25,840
family with certain relatives, but those aren't, those aren't the greatest relatives.

113
00:15:25,840 --> 00:15:33,200
The greatest of all relations doesn't have to do with blood, it has to do with familiarity

114
00:15:33,200 --> 00:15:39,440
and compatibility.

115
00:15:39,440 --> 00:15:46,080
So he was expressing his appreciation of his community, so we take a moment to also appreciate

116
00:15:46,080 --> 00:15:54,400
our community here, how great it is to be surrounded by such good people.

117
00:15:54,400 --> 00:16:04,400
The fourth part we get to the core of the issue, nibbanang baramang sukang, nibana, freedom from suffering,

118
00:16:04,400 --> 00:16:10,880
is the highest happiness.

119
00:16:10,880 --> 00:16:20,320
Nibana is a scary thing, I think, I mean anything strange and unfamiliar is scary, but

120
00:16:20,320 --> 00:16:25,640
it's, there's a special dread I think associated with nibana because it threatens everything

121
00:16:25,640 --> 00:16:29,960
we hold dear, purposefully, right?

122
00:16:29,960 --> 00:16:37,840
Nibana, if you learn anything about it, nibana means you let go of everything, nibana

123
00:16:37,840 --> 00:16:46,840
is no attachment, none of the things you love, none of the things you want, it's none

124
00:16:46,840 --> 00:16:58,840
of them, none of the things you love, and so it's actually quite stressful, nibana is

125
00:16:58,840 --> 00:17:04,680
a very stressful thing for us, until you realize it, of course, while until you start to

126
00:17:04,680 --> 00:17:07,960
see that the things that you're holding on to and saying, hey wait a minute, I don't

127
00:17:07,960 --> 00:17:13,880
want to let go of this, you realize that all those things are not worth clinging to, all

128
00:17:13,880 --> 00:17:23,080
of those things are impermanent, unsatisfying, uncontrollable, meaningless, purposeless,

129
00:17:23,080 --> 00:17:28,080
useless, and you see this, this is what you're starting to see or have started to have

130
00:17:28,080 --> 00:17:37,720
come to see, yes, there's no real meaning or purpose or benefit at these things.

131
00:17:37,720 --> 00:17:45,560
Nibana's can't be found in a risen phenomena, it can be found in nibana, which is true

132
00:17:45,560 --> 00:17:51,800
peace, so it was a question asked that I repeated, some of you have sure heard me say,

133
00:17:51,800 --> 00:17:57,880
how can nibana be happiness when there is no feeling in nibana?

134
00:17:57,880 --> 00:18:01,600
And the answer, sorry, put the given answer and said it's precisely because there is no

135
00:18:01,600 --> 00:18:10,840
feeling that nibana is happiness, it's a very difficult truth to understand, because

136
00:18:10,840 --> 00:18:20,280
we're very much addicted to feeling, happy feeling is a good, calm feeling is good, painful

137
00:18:20,280 --> 00:18:27,640
feeling is bad, this is how we think, until we start to see that even happy feeling, calm

138
00:18:27,640 --> 00:18:35,840
feeling, they're impermanent, unsatisfying, uncontrollable.

139
00:18:35,840 --> 00:18:42,720
Only then we start to let them go, sorry to see, this isn't real happiness, this is stress,

140
00:18:42,720 --> 00:18:49,080
remind us no more at peace than it was before I got this feeling, I have no more satisfied

141
00:18:49,080 --> 00:19:01,720
than before, if anything I just wanted more, if anything I'm just attached to it more.

142
00:19:01,720 --> 00:19:05,600
And that's when we start to turn and incline and say maybe there's something better than

143
00:19:05,600 --> 00:19:14,760
feeling, maybe there's something better than a risen phenomena, there's a natural inclination,

144
00:19:14,760 --> 00:19:19,560
I mean there's no brainwashing going on here, you'll see it for yourself, I don't have

145
00:19:19,560 --> 00:19:31,720
to tell you, it's irrefutable, once you realize that then when comes to nibana, and once

146
00:19:31,720 --> 00:19:40,640
you've seen it there's no question, nibana and paramong, so can nibana is the highest happiness.

147
00:19:40,640 --> 00:19:45,680
So that was what I was thinking about tonight, we'll take that as our demo for tonight, thank

148
00:19:45,680 --> 00:20:11,600
you all for coming out, have a good night.

149
00:20:45,680 --> 00:21:04,080
You do chat, it's toxic as ever, okay, let's go to questions.

150
00:21:15,680 --> 00:21:39,800
Okay, I'm going to say no questions tonight, thank you all for tuning in.

