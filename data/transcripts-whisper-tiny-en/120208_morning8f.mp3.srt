1
00:00:00,000 --> 00:00:27,360
Okay, so, can I move on to getting to include in our meditation sessions a theoretical guidance?

2
00:00:27,360 --> 00:00:36,360
Some kind of practical theory that we can apply to our meditation practice, and I'll try to find something every evening

3
00:00:36,360 --> 00:00:48,360
from the Buddhist teaching that's applicable to our practice, something that we can use.

4
00:00:48,360 --> 00:00:56,360
So, as this is the first talk, I thought it would be important to start the beginning.

5
00:00:56,360 --> 00:01:00,360
It's a setipatana.

6
00:01:00,360 --> 00:01:15,360
Of course, we're all familiar with what are the four setipatana, and we have our understanding of how we put into practice the four setipatana.

7
00:01:15,360 --> 00:01:22,360
And actually, I think you all have heard something about how one of the benefits of setipatana,

8
00:01:22,360 --> 00:01:26,360
but I think it's important for us to go over this again.

9
00:01:26,360 --> 00:01:31,360
It helps us to understand why we're practicing.

10
00:01:31,360 --> 00:01:36,360
And it helps us to focus our practice in the right direction.

11
00:01:36,360 --> 00:01:41,360
So, I'll try to go through these in a practical way.

12
00:01:41,360 --> 00:01:45,360
The first reason why we practice we pass in is to purify our mind.

13
00:01:45,360 --> 00:01:56,360
So, this should be our main focus in our practice.

14
00:01:56,360 --> 00:02:04,360
It's not to see special things or to experience special states.

15
00:02:04,360 --> 00:02:12,360
The practice is for cleaning our mind, straightening our mind,

16
00:02:12,360 --> 00:02:17,360
straightening out what's crooked about our mind, cleaning what's dirty about our mind,

17
00:02:17,360 --> 00:02:21,360
fixing what's broken about our mind.

18
00:02:21,360 --> 00:02:38,360
And not even trying to teach ourselves this is wrong or that is wrong or find things that are wrong about our mind.

19
00:02:38,360 --> 00:02:45,360
But just fix that which we know is broken, clean that which we know is dirty that we know for ourselves.

20
00:02:45,360 --> 00:02:55,360
We don't have to read books to find out what is bad and what is unwholesome.

21
00:02:55,360 --> 00:03:01,360
But when we look inside, we'll see very clearly certain things that are broken.

22
00:03:01,360 --> 00:03:05,360
In the beginning, we'll have all sorts of ideas about what's wrong with us.

23
00:03:05,360 --> 00:03:14,360
Maybe we're not smart enough or maybe we're not good enough or so on.

24
00:03:14,360 --> 00:03:19,360
Not focused enough, many meditators come and get angry at themselves because they're not focused.

25
00:03:19,360 --> 00:03:28,360
And so we begin to feel guilty and we think that something's wrong with us.

26
00:03:28,360 --> 00:03:37,360
But would we begin to see when we apply the practice and when we apply it day after day after day?

27
00:03:37,360 --> 00:03:45,360
Is that really what's the real problem and the most pressing problem is this guilt and this self-hate kick?

28
00:03:45,360 --> 00:03:50,360
It's feeling that something's wrong with us.

29
00:03:50,360 --> 00:03:56,360
So the meaning is that we come to see for ourselves which are wrong.

30
00:03:56,360 --> 00:04:01,360
We don't need anyone to tell us and we don't need to have any belief and we have to give up all our beliefs.

31
00:04:01,360 --> 00:04:06,360
Any belief that we have that this is wrong and that is wrong and this is bad and that is bad.

32
00:04:06,360 --> 00:04:15,360
We have to give it up because all of that is a cause for us to really hurt ourselves.

33
00:04:15,360 --> 00:04:22,360
It's the reason why we suffer so horribly at time.

34
00:04:22,360 --> 00:04:35,360
I'm getting angry as bad but when you get angry that you're angry it's far worse and that leads to true and extreme suffering.

35
00:04:35,360 --> 00:04:41,360
When you want things this is bad but when you hate yourself because of your addictions and because of your desires

36
00:04:41,360 --> 00:04:45,360
when you feel guilty about them this is worse.

37
00:04:45,360 --> 00:04:52,360
This is real and extreme suffering.

38
00:04:52,360 --> 00:05:03,360
So when we talk about cleaning the mind, purifying the mind we're talking about something empirical, something you can see for yourself.

39
00:05:03,360 --> 00:05:07,360
It's not based on commandments or requirements.

40
00:05:07,360 --> 00:05:12,360
It's based on your experience of what hurts you and what helps you.

41
00:05:12,360 --> 00:05:19,360
What brings your peace and what makes you suffer?

42
00:05:19,360 --> 00:05:26,360
This is the first reason why it hurts.

43
00:05:26,360 --> 00:05:41,360
The way this comes about in our practice is through the direct experience and reaffirmation.

44
00:05:41,360 --> 00:05:47,360
That we use that basically means it is what it is.

45
00:05:47,360 --> 00:05:51,360
Seeing is only seeing, hearing is only hearing.

46
00:05:51,360 --> 00:05:56,360
Smelling is only smelling, tasting is only tasting, feeling and feeling.

47
00:05:56,360 --> 00:05:59,360
Thinking is only feeling.

48
00:05:59,360 --> 00:06:03,360
It is bad and it's only bad.

49
00:06:03,360 --> 00:06:09,360
When we remind ourselves of this again and again,

50
00:06:09,360 --> 00:06:14,360
we straighten out all that is wrong with the mind.

51
00:06:14,360 --> 00:06:17,360
Even when you're angry knowing that it's just this is anger.

52
00:06:17,360 --> 00:06:24,360
It is and it's only that.

53
00:06:24,360 --> 00:06:35,360
So there's no room for feeling bad about it or guilty about it or getting frustrated about your anger.

54
00:06:35,360 --> 00:06:41,360
And then when you have pain before you're giving it angry about it, you know that it's pain.

55
00:06:41,360 --> 00:06:45,360
And so there's no room for you to.

56
00:06:45,360 --> 00:06:54,360
There's no reason for you to get angry about it.

57
00:06:54,360 --> 00:07:04,360
And so you're not truly purified of mind.

58
00:07:04,360 --> 00:07:11,360
It's kind of amazing actually how like some liquid detergent,

59
00:07:11,360 --> 00:07:16,360
like bleach for example, bleach has this magical property that

60
00:07:16,360 --> 00:07:22,360
kills all bacteria and germs and cleans away all stains and so on.

61
00:07:22,360 --> 00:07:26,360
Mindfulness is the same. It has this magical property that everything.

62
00:07:26,360 --> 00:07:32,360
You don't even have to go around looking for a cure to for this problem or cure for that problem.

63
00:07:32,360 --> 00:07:38,360
It's amazing how all of our problems can be solved by this mind.

64
00:07:38,360 --> 00:07:49,360
And this is because of it, the Buddha said that the true cause of suffering and all suffering is ignorance.

65
00:07:49,360 --> 00:07:55,360
When we look at deeply into the Buddha's teaching, we see that the real problem isn't craving actually,

66
00:07:55,360 --> 00:07:59,360
even though the Buddha said craving is the cause of suffering.

67
00:07:59,360 --> 00:08:06,360
He explained it by saying that the cure for suffering is wisdom.

68
00:08:06,360 --> 00:08:08,360
So the real problem is the ignorance.

69
00:08:08,360 --> 00:08:13,360
We're ignorant as to these things that we crave, these things that we cling to.

70
00:08:13,360 --> 00:08:16,360
We think somehow they're going to bring us happiness.

71
00:08:16,360 --> 00:08:21,360
Once we see clearly that they don't, everything clears up.

72
00:08:21,360 --> 00:08:27,360
It's like having a big knot.

73
00:08:27,360 --> 00:08:30,360
And you look at the knot and you think, oh, that's real problem.

74
00:08:30,360 --> 00:08:34,360
But once you solve the knot, it's just a string with no knot.

75
00:08:34,360 --> 00:08:37,360
Like the knot was never there.

76
00:08:37,360 --> 00:08:42,360
You think, how do I solve this? But all you do is untie it and there is no problem.

77
00:08:42,360 --> 00:08:45,360
It's like there was never a problem. It was just the string.

78
00:08:45,360 --> 00:08:49,360
In the beginning it was the string and the end it was the string.

79
00:08:49,360 --> 00:08:55,360
The only difference is the formation that was in the wrong, it wasn't string.

80
00:08:55,360 --> 00:08:58,360
And so we do this with the mind.

81
00:08:58,360 --> 00:09:04,360
This is really the focus of our practice.

82
00:09:04,360 --> 00:09:08,360
The second reason why we practice and really getting into the benefits of

83
00:09:08,360 --> 00:09:15,360
straightening your mind and purifying your mind

84
00:09:15,360 --> 00:09:20,360
is that once your mind is pure, you do away with all kinds of mental,

85
00:09:20,360 --> 00:09:28,360
mental illness, mental disease, all kinds of sorrows, and despair,

86
00:09:28,360 --> 00:09:34,360
and stresses, and worries, and obsessions, and dictions, and so on.

87
00:09:34,360 --> 00:09:37,360
All of these things that they talk about in the world is being

88
00:09:37,360 --> 00:09:44,360
the problems that you need to solve with drugs or so on.

89
00:09:44,360 --> 00:09:50,360
But it's not true that all of them can be cured for people who have

90
00:09:50,360 --> 00:09:55,360
chemical imbalances in the brain. It's possible that meditation will never

91
00:09:55,360 --> 00:10:01,360
in this life solve their problem, but the actual practice of the meditation

92
00:10:01,360 --> 00:10:05,360
for people who are able to understand it.

93
00:10:05,360 --> 00:10:11,360
Every moment of practicing changes the mind and as a result changes the brain.

94
00:10:11,360 --> 00:10:15,360
There's this phenomenon they're learning about now called neuroplasticity

95
00:10:15,360 --> 00:10:19,360
that the brain can actually change the way it works.

96
00:10:19,360 --> 00:10:28,360
An example is people who go blind find that,

97
00:10:28,360 --> 00:10:33,360
they find in the brains of people who were born blind or have gone blind,

98
00:10:33,360 --> 00:10:38,360
that the part of the brain that used to see is now used for hearing for example.

99
00:10:38,360 --> 00:10:45,360
The brain starts to use this unused hard drive space or CPU space

100
00:10:45,360 --> 00:10:50,360
for something for another task.

101
00:10:50,360 --> 00:10:55,360
But the point is that our brains even our brains can change.

102
00:10:55,360 --> 00:11:02,360
It's unlikely that people with serious mental problems will get far in the meditation,

103
00:11:02,360 --> 00:11:07,360
but even getting a short way in the meditation.

104
00:11:07,360 --> 00:11:12,360
They can set them on the right path and can change their mind.

105
00:11:12,360 --> 00:11:15,360
So all kinds of mental sickness is mental illnesses.

106
00:11:15,360 --> 00:11:22,360
Come by by untying the knots in our behavior, the knots in our habit.

107
00:11:22,360 --> 00:11:26,360
We see that actually all of these problems we thought that we had brain.

108
00:11:26,360 --> 00:11:33,360
We had an imbalant chemical imbalances when we came to see that actually it was just not.

109
00:11:33,360 --> 00:11:38,360
It's just strings, strings, and the kind of a string theory.

110
00:11:38,360 --> 00:11:45,360
Well, this means a string is tied in knots and all we have to do is untie the knot.

111
00:11:45,360 --> 00:11:48,360
When you untie the knots, it's like it was never there.

112
00:11:48,360 --> 00:11:53,360
It's gone. It's just a illusion.

113
00:11:53,360 --> 00:11:58,360
The third reason why we practice is to overcome suffering.

114
00:11:58,360 --> 00:12:03,360
Pain in the body and pain in the mind.

115
00:12:03,360 --> 00:12:06,360
To be free from it really.

116
00:12:06,360 --> 00:12:09,360
You know, we start with pain in the mind.

117
00:12:09,360 --> 00:12:14,360
When you're sitting in meditation there's not much you can do about physical pain.

118
00:12:14,360 --> 00:12:17,360
Sometimes you have no choice but to sit through it.

119
00:12:17,360 --> 00:12:21,360
You adjust your stretch and move and it doesn't go away.

120
00:12:21,360 --> 00:12:26,360
And it can get so bad until finally you realize that there's no way out.

121
00:12:26,360 --> 00:12:29,360
You realize that you have no way out.

122
00:12:29,360 --> 00:12:37,360
And so you make this profound change by just saying let it be.

123
00:12:37,360 --> 00:12:41,360
And when the pain comes up you stick with it.

124
00:12:41,360 --> 00:12:43,360
In our daily life we're not able to do this.

125
00:12:43,360 --> 00:12:45,360
It's something we don't ever know this.

126
00:12:45,360 --> 00:12:49,360
We think why do I have all this pain when I'm sitting here in meditation?

127
00:12:49,360 --> 00:12:51,360
When I'm at home I don't have this pain.

128
00:12:51,360 --> 00:12:56,360
And when you're at home you're shifting every few seconds.

129
00:12:56,360 --> 00:12:59,360
Every minute you change position many times.

130
00:12:59,360 --> 00:13:04,360
When you're in meditation you do each shift position much less.

131
00:13:04,360 --> 00:13:08,360
And so it's not something caused by meditation.

132
00:13:08,360 --> 00:13:12,360
It's actually now you're finally seeing the truth of the body.

133
00:13:12,360 --> 00:13:15,360
The truth of suffering.

134
00:13:15,360 --> 00:13:27,360
And so finally after trying again and again through the deep pain you give in and you let it let the pain be.

135
00:13:27,360 --> 00:13:34,360
And you watch it and you say pain.

136
00:13:34,360 --> 00:13:37,360
And you realize that it's not really pain at all.

137
00:13:37,360 --> 00:13:40,360
Physical pain isn't really painful.

138
00:13:40,360 --> 00:13:45,360
Until the mind says it's no good until the mind dislikes it.

139
00:13:45,360 --> 00:13:53,360
So the Buddha said this is like you have one thorn in your body and you take another thorn to try to get it out.

140
00:13:53,360 --> 00:13:57,360
Take a thorn off the tree and you break it and try to get the first thorn out.

141
00:13:57,360 --> 00:14:01,360
Now with that kind of method are you going to get less pain or more pain?

142
00:14:01,360 --> 00:14:04,360
It's most likely you're going to get twice as much pain.

143
00:14:04,360 --> 00:14:11,360
It's when they had glass in my foot and they was trying to get it out with a needle and it turned out there was no glass in my foot after all.

144
00:14:11,360 --> 00:14:15,360
But I got a lot of pain for it.

145
00:14:15,360 --> 00:14:18,360
This is what we do with the mind pain.

146
00:14:18,360 --> 00:14:25,360
This is a good example actually because in my mind I thought there was glass there so I was always trying to fix the problem.

147
00:14:25,360 --> 00:14:35,360
And it must have been something common because I tried many times and then the doctor tried and so much pain.

148
00:14:35,360 --> 00:14:37,360
This is what we do.

149
00:14:37,360 --> 00:14:42,360
We think we're going to fix it and we try to fix it and end up just causing more pain for ourselves.

150
00:14:42,360 --> 00:14:43,360
You can believe it alone.

151
00:14:43,360 --> 00:14:46,360
Of course if there is glass in your foot you should get it out.

152
00:14:46,360 --> 00:14:53,360
It's not something simple but it's just an analogy.

153
00:14:53,360 --> 00:14:59,360
Truth is for like pain when we're sitting in meditation.

154
00:14:59,360 --> 00:15:05,360
Nothing is going to happen to our legs if we sit still if we let it be.

155
00:15:05,360 --> 00:15:11,360
Except that our legs might come down.

156
00:15:11,360 --> 00:15:14,360
And so you can see that actually you get rid of physical pain as well.

157
00:15:14,360 --> 00:15:20,360
You say you're still physical pain but you don't feel it as pain.

158
00:15:20,360 --> 00:15:29,360
The fourth reason why we practice is to set ourselves on the right path.

159
00:15:29,360 --> 00:15:33,360
This is something that people are always asking what is the right thing to do.

160
00:15:33,360 --> 00:15:35,360
What is the right way to live my life?

161
00:15:35,360 --> 00:15:37,360
How can I live my life?

162
00:15:37,360 --> 00:15:40,360
And the truth is it's not important what you do or how you live your life.

163
00:15:40,360 --> 00:15:43,360
Everyone lives their life anyway.

164
00:15:43,360 --> 00:15:49,360
Your life is going to go up and down and all around and change in many ways.

165
00:15:49,360 --> 00:15:56,360
The Buddha said it's like when you take a cloth, if you have a white cloth.

166
00:15:56,360 --> 00:16:02,360
And it's covered in dirt and grime and oil and stains.

167
00:16:02,360 --> 00:16:05,360
And then it doesn't matter what color you dip it in.

168
00:16:05,360 --> 00:16:08,360
You put it in red color or blue color or green color.

169
00:16:08,360 --> 00:16:10,360
It's not going to take the color.

170
00:16:10,360 --> 00:16:16,360
It's not going to come out in the beautiful green or beautiful red or beautiful yellow color.

171
00:16:16,360 --> 00:16:24,360
But if you take a clean cloth and you dip it in whatever color you dip it in, it will become clean.

172
00:16:24,360 --> 00:16:29,360
Whatever you do in your life, the right path is not what you're doing.

173
00:16:29,360 --> 00:16:35,360
The right path is how you're doing it and your approach to life.

174
00:16:35,360 --> 00:16:42,360
Because life is life, I'm not a monk, you're not a carpenter, an engineer or this or that.

175
00:16:42,360 --> 00:16:48,360
We are experienced, we're moment to moment experience, that's who we are.

176
00:16:48,360 --> 00:16:52,360
And every experience affects the next experience.

177
00:16:52,360 --> 00:16:59,360
So if we act and speak and if we think in with it, we have a pure mind,

178
00:16:59,360 --> 00:17:04,360
the next experience will be based on that purity and happiness will follow us.

179
00:17:04,360 --> 00:17:12,360
If we develop unimpure minds and unhosen mind state, then so also actions and speech.

180
00:17:12,360 --> 00:17:21,360
Then we will receive the detriment of that and as a result suffering of all.

181
00:17:21,360 --> 00:17:26,360
So this is how we understand people wonder how can meditation help me in my life?

182
00:17:26,360 --> 00:17:27,360
What can it do for me?

183
00:17:27,360 --> 00:17:29,360
It does the most important thing for you.

184
00:17:29,360 --> 00:17:32,360
It won't tell you what career you should take.

185
00:17:32,360 --> 00:17:39,360
But it will help you understand that which is much far more important than your career.

186
00:17:39,360 --> 00:17:49,360
And that is how to live, how to interact with reality rather than react to it.

187
00:17:49,360 --> 00:17:57,360
And the final reason for practicing meditation is that it leads us to Nirvana or freedom from suffering.

188
00:17:57,360 --> 00:18:03,360
And this goes in stages, so when we first start to practice or when we practice this a little bit,

189
00:18:03,360 --> 00:18:09,360
all it does is make us happier and more peaceful and more clear about the world with more wisdom.

190
00:18:09,360 --> 00:18:16,360
So we still have to maybe come back and be reborn and we can even change and do become a bad person later on.

191
00:18:16,360 --> 00:18:19,360
But it's helped us in some level.

192
00:18:19,360 --> 00:18:24,360
If we practice more then it will purify us to such an extent that we don't have to come back as a human.

193
00:18:24,360 --> 00:18:33,360
We can be born in a pure state where we are able to be free from suffering for a long time and maybe still fall back.

194
00:18:33,360 --> 00:18:39,360
If we practice on and on until we realize nibana then we'll be set in the practice.

195
00:18:39,360 --> 00:18:45,360
And it will only be a matter of time and eventually we'll become free and not have to come back at all.

196
00:18:45,360 --> 00:18:56,360
So we can go to pure and pure and pure existence until finally we're free because the mind has come to incline towards nibana.

197
00:18:56,360 --> 00:19:01,360
Just like a tree that is inclining in one direction is going to fall in that direction.

198
00:19:01,360 --> 00:19:05,360
But if you change the direction then it's going to fall.

199
00:19:05,360 --> 00:19:08,360
Then you can look and say now it's going to fall in this direction.

200
00:19:08,360 --> 00:19:15,360
So whereas our mind is inclined towards some sara we change it to incline towards nibana.

201
00:19:15,360 --> 00:19:18,360
And all you have to do is incline towards nibana.

202
00:19:18,360 --> 00:19:23,360
It will fall eventually in that direction.

203
00:19:23,360 --> 00:19:30,360
So this is a teaching on the five benefits or five reasons why we practice we pass in the minute.

204
00:19:30,360 --> 00:19:33,360
In fact practice the four foundations of mindfulness.

205
00:19:33,360 --> 00:19:36,360
Just a short step talk before we do our actual practice.

206
00:19:36,360 --> 00:19:40,360
So now we'll continue on walking and sitting.

