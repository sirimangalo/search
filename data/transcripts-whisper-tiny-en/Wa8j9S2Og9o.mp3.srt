1
00:00:00,000 --> 00:00:03,880
I constantly try to identify with everything around

2
00:00:05,240 --> 00:00:12,500
Maybe because I don't accept myself fully. I really would like to be at peace with who I am and my experiences

3
00:00:14,000 --> 00:00:16,000
But at the same time

4
00:00:16,680 --> 00:00:21,240
Not be afraid with any other situation. I'm always thinking about falling into the negative

5
00:00:21,240 --> 00:00:32,040
Well, first of all, I wouldn't second guess yourself too much like why is why am I doing this? Why am I doing that?

6
00:00:32,640 --> 00:00:34,640
It's not really useful

7
00:00:34,640 --> 00:00:40,120
Don't I don't subscribe and I don't recommend subscribing to this sort of

8
00:00:40,680 --> 00:00:42,120
vane of

9
00:00:43,120 --> 00:00:47,720
Psychology where you try to find the source of your problems

10
00:00:47,720 --> 00:00:51,640
Where did it come from? Why am I like this and why am I doing this?

11
00:00:52,200 --> 00:00:54,760
Don't try to find answers

12
00:00:55,640 --> 00:00:57,640
Try to learn about

13
00:00:57,960 --> 00:00:59,160
experiences

14
00:00:59,160 --> 00:01:03,360
So I constantly try to identify with everything around. Well good learn about that

15
00:01:03,960 --> 00:01:05,960
Study that study your

16
00:01:05,960 --> 00:01:11,480
habit of constantly trying to identify with with things like just take that

17
00:01:12,960 --> 00:01:15,760
process and study it because

18
00:01:15,760 --> 00:01:21,600
Because that's the practice of Yipassana in its entirety

19
00:01:22,040 --> 00:01:26,600
Once you study your experiences learn about them see them for what they are

20
00:01:27,840 --> 00:01:29,840
you will naturally

21
00:01:31,240 --> 00:01:38,160
Separate them into their proper categories. This is useful. This is useless. This is beneficial. This is unbeneficial

22
00:01:38,160 --> 00:01:40,160
You don't have to

23
00:01:40,160 --> 00:01:47,640
criticize them. You don't have to judge them. And in fact judging them

24
00:01:47,640 --> 00:01:51,480
prevents you from examining them prevents you from seeing them as they are so

25
00:01:52,560 --> 00:01:58,000
Don't worry if you don't accept yourself fully. That's not important. We don't need to we don't need to accept ourselves

26
00:01:58,000 --> 00:02:01,520
Because the idea of a self is just a concept

27
00:02:02,160 --> 00:02:06,800
Just learn about yourself learn about your experiences learn about who you are

28
00:02:06,800 --> 00:02:14,240
And then we can go on talking about wanting to be at peace with who I am. Don't worry about that

29
00:02:14,240 --> 00:02:20,480
Stop wanting to stop wanting to be at peace with who you are and learn about what it's like to be not at peace with yourself

30
00:02:21,200 --> 00:02:23,400
That means you're not at peace with yourself great

31
00:02:23,400 --> 00:02:29,040
So look at this lack of peace with yourself this suffering inside because we all have it

32
00:02:29,040 --> 00:02:36,800
But look at it stop judging and stop being upset about it and just learn about what it's like if you judge it

33
00:02:36,800 --> 00:02:38,800
If you repress it if you

34
00:02:39,160 --> 00:02:44,720
Don't want if you feel upset about the fact that you are not at peace with yourself then look at that as well

35
00:02:44,800 --> 00:02:50,800
Look at how you're upset by your your negative qualities and so on

36
00:02:50,800 --> 00:02:59,280
I'm always thinking about falling into the negative. So look at that. Look at how you fall into the negative

37
00:03:00,560 --> 00:03:04,560
Think of it as a laboratory if you think about life as a laboratory or as a school

38
00:03:05,280 --> 00:03:11,160
Then you're not really making mistakes. You're learning lessons. You're studying you don't make mistakes in laboratories

39
00:03:11,160 --> 00:03:16,240
You I guess you do but in schools you in studying you don't make mistakes you learn

40
00:03:16,240 --> 00:03:20,360
Yeah, let's say in a gym in gymnastics or so on

41
00:03:20,960 --> 00:03:25,120
Yeah, you trip a lot you fail a lot, but that's the process of learning

42
00:03:26,520 --> 00:03:29,000
So you think of this as some kind of mental

43
00:03:29,800 --> 00:03:34,080
Gymnastics or mental it's mental training and in mental training you fail a lot

44
00:03:35,120 --> 00:03:39,120
If you think of life as a school, then you don't have to worry about failure

45
00:03:39,120 --> 00:03:42,600
You don't have to worry about your problems because they're not problems there

46
00:03:43,640 --> 00:03:45,160
lessons

47
00:03:45,160 --> 00:03:47,160
And they're giving you an opportunity to learn

48
00:03:48,960 --> 00:03:51,560
There is back anything to comment

49
00:03:51,560 --> 00:04:15,560
Okay

