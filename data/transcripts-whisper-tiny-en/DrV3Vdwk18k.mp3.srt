1
00:00:00,000 --> 00:00:02,000
Okay, go ahead

2
00:00:02,000 --> 00:00:08,120
If the desires of the root of suffering, is there anything inherently wrong with them?

3
00:00:08,920 --> 00:00:18,240
But he best acknowledged and put to good use our desire without getting carried away by their passions. Yeah

4
00:00:18,920 --> 00:00:20,120
well

5
00:00:20,120 --> 00:00:23,640
That's interesting question. I don't I don't quite get the logical

6
00:00:23,640 --> 00:00:31,360
movement so the premise is if desires are the that the premises that desires are the root of suffering and

7
00:00:33,400 --> 00:00:38,520
Then somehow that leads you to the conclusion that there may not be anything wrong with them

8
00:00:38,520 --> 00:00:43,840
I don't see the fault the logic there if they are the root of suffering. How could there be anything good about them?

9
00:00:44,720 --> 00:00:47,200
I mean it doesn't it doesn't it isn't

10
00:00:47,200 --> 00:00:56,360
I don't I don't I don't get it if some if something is the desire if something if anything were the root of suffering

11
00:00:58,320 --> 00:01:01,480
Isn't that a sign that there's something inherently wrong? I

12
00:01:02,560 --> 00:01:07,920
Don't know so okay, so so something is a little bit unclear there to me

13
00:01:09,360 --> 00:01:11,640
But maybe let me a little bit

14
00:01:11,640 --> 00:01:16,920
But I want to just add something here because he's he's got an there's another point involved with this and that's

15
00:01:19,320 --> 00:01:21,320
Using desires

16
00:01:21,960 --> 00:01:25,640
For good use without getting carried away from them now

17
00:01:25,640 --> 00:01:33,160
Maybe the idea that you're trying to be trying to present is that desires can cause suffering desire to the root of suffering in the

18
00:01:33,160 --> 00:01:36,280
Sense that they can cause suffering. That's not Buddhist theory

19
00:01:36,280 --> 00:01:45,400
desires do cause suffering if you if you're referring to the mental state of wanting something or

20
00:01:46,360 --> 00:01:52,600
instead of just the intention that we call desire, but if it's an actual desire, it

21
00:01:53,160 --> 00:01:58,920
can't help but bring if it brings a result it can't help but bring

22
00:02:00,200 --> 00:02:03,120
Suffering as a result now. It's possible that they can be nullified

23
00:02:03,120 --> 00:02:06,800
So not all desires do bring results

24
00:02:08,400 --> 00:02:13,200
Sketchy may not bring results depending on what theory you subscribe to but

25
00:02:14,480 --> 00:02:20,640
In general, let's say they do bring results and there's no way that a desire could bring a positive result

26
00:02:20,640 --> 00:02:23,680
The Buddha is very very clear about this specifically clear

27
00:02:23,680 --> 00:02:33,760
Desire can only bring it's just lost it now, but in the in the and with there any guy a book of ones

28
00:02:35,360 --> 00:02:37,360
Desire can only bring

29
00:02:38,400 --> 00:02:40,400
suffering

30
00:02:42,000 --> 00:02:43,200
Anyway

31
00:02:43,440 --> 00:02:48,640
So no no way no way to use desires for but I mean

32
00:02:48,640 --> 00:02:54,640
If you if you ask someone else they might say some some people I think might say

33
00:02:55,840 --> 00:03:00,720
desires are not so bad just don't as you say let them become passions or

34
00:03:01,760 --> 00:03:05,760
let yourself get carried away by them, but that's not technically

35
00:03:08,320 --> 00:03:10,000
Honest

36
00:03:10,000 --> 00:03:14,160
You can you know I can couch what I said in those kind of words and say you know

37
00:03:14,160 --> 00:03:18,640
Don't worry too much and that's a good advice. Don't worry too much about your desires

38
00:03:20,400 --> 00:03:24,480
You know try to learn about them and learn about the bad desires and passions and

39
00:03:24,960 --> 00:03:28,240
That's a good advice because once you do that you'll start because

40
00:03:29,040 --> 00:03:34,400
You can't hope as a person is never practiced, but it's some can't hope to get close to their

41
00:03:36,240 --> 00:03:38,720
You might say innocent desires

42
00:03:39,440 --> 00:03:42,400
Yeah, you would say innocent desires so desires that are not

43
00:03:42,400 --> 00:03:48,480
Evil evil evil evil evil in a in a in a conventional worldly sense

44
00:03:49,120 --> 00:03:51,840
So desires that don't need to kill steel light sheet

45
00:03:53,440 --> 00:03:57,200
Kill steel light sheet. Yeah, and so on

46
00:04:04,000 --> 00:04:07,680
Lost it totally. Yes, the innocent desires, but you can get rid of them

47
00:04:08,880 --> 00:04:10,880
because you're

48
00:04:10,880 --> 00:04:14,160
Well because you're very much attached to them

49
00:04:14,400 --> 00:04:20,000
But because there's all these course and and crazy defilements because you still want to kill steel light sheet

50
00:04:20,720 --> 00:04:25,600
And so until you get rid of those you're not going to be able to see clearly the more subtle ones

51
00:04:26,400 --> 00:04:28,560
You're not going to have a chance to get rid of them

52
00:04:30,400 --> 00:04:36,080
And so so taking a step by step. It's a very good advice to tell you just to

53
00:04:36,080 --> 00:04:38,080
um

54
00:04:39,760 --> 00:04:43,600
To deal to try to deal with the passions and so on and to not worry about them

55
00:04:43,840 --> 00:04:47,680
So in that sense I could answer this. How do we

56
00:04:48,960 --> 00:04:55,440
We can't put a good use your desires because that what I mean might not being honest is that all desires are negative

57
00:04:55,920 --> 00:05:00,240
And even though we might put up with them. We certainly can't put them to good use

58
00:05:00,240 --> 00:05:04,240
um, but

59
00:05:04,240 --> 00:05:09,040
How do we how would we if your question was put up with them without getting carried away by them?

60
00:05:12,880 --> 00:05:17,360
I think maybe that's just a good way of something and I put up with them without getting carried away with them

61
00:05:18,320 --> 00:05:19,200
uh

62
00:05:19,200 --> 00:05:20,480
Deal with them

63
00:05:20,480 --> 00:05:26,000
They're getting carried away part first because that's much more important than deal with the views that

64
00:05:26,000 --> 00:05:31,600
We need you to get carried away first of all and they'll deal with your ideas and

65
00:05:33,200 --> 00:05:35,200
And so on

66
00:05:35,200 --> 00:05:40,080
a lot of these how do I deal with questions are really dishonest not dishonest, but

67
00:05:40,960 --> 00:05:42,400
um

68
00:05:42,400 --> 00:05:45,040
It's dishonest of me to give you a direct answer

69
00:05:46,480 --> 00:05:48,080
Because

70
00:05:48,080 --> 00:05:54,160
That's not the point of Buddhism the point of Buddhism is not to deal with the point of Buddhism is to get rid of to be free from

71
00:05:54,160 --> 00:06:00,160
So the answer in all cases is don't put up with it if you can't become among at least

72
00:06:00,880 --> 00:06:02,880
Start meditating do some

73
00:06:03,120 --> 00:06:08,960
Serious meditation take time to do a meditation course if you can if you really really want to

74
00:06:09,600 --> 00:06:13,600
Have answers to all your problems take at least a meditation course

75
00:06:14,320 --> 00:06:15,280
um

76
00:06:15,280 --> 00:06:19,120
You know, hey, I'll be in Canada. I think a lot of people here in America. So

77
00:06:19,840 --> 00:06:21,840
It's not that far

78
00:06:21,840 --> 00:06:26,240
See if we can actually run wouldn't it be great if we can actually run courses in Canada

79
00:06:27,600 --> 00:06:29,600
One day we'll have a place where we can do that

80
00:06:30,560 --> 00:06:32,880
We'll see maybe I can have people stay in my apartment

81
00:06:34,000 --> 00:06:39,280
Something like that you can come and pitch a tent. Maybe it looks like a really nice natural area

82
00:06:40,720 --> 00:06:45,520
There's one Laurie said he would come and pitch a tent and it looks like you'll be able to have to talk to the

83
00:06:46,480 --> 00:06:50,400
To this the people supporting me, but they're very they seem very kind

84
00:06:50,400 --> 00:06:55,440
And they're very accommodating. So we'll see

