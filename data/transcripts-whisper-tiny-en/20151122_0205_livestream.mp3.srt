1
00:00:00,000 --> 00:00:15,440
That's the one that we're broadcasting live.

2
00:00:15,440 --> 00:00:17,680
So good evening everyone.

3
00:00:17,680 --> 00:00:25,920
Welcome to our live broadcast November 21st, 2015.

4
00:00:25,920 --> 00:00:34,960
Apparently, last night's Damapada video was too long.

5
00:00:34,960 --> 00:00:40,720
I told that no one has time to sit through a 47-minute video.

6
00:00:40,720 --> 00:00:47,800
And normally, comments like this will go unanswered and discuss, and I'm not reading all

7
00:00:47,800 --> 00:00:51,600
comments, so please don't expect me to reply to your comments.

8
00:00:51,600 --> 00:00:55,680
But I do read the first couple on each video just to make sure the video got uploaded

9
00:00:55,680 --> 00:01:00,360
correctly and no one's having problems, and sometimes the first couple of comments tell

10
00:01:00,360 --> 00:01:05,640
me that the audio was no good or was missing or something like this or that.

11
00:01:05,640 --> 00:01:07,200
But no, it was too long.

12
00:01:07,200 --> 00:01:12,840
And I just thought it interesting to comment because a 47-minute Damapada is usually

13
00:01:12,840 --> 00:01:15,440
considered to be too short.

14
00:01:15,440 --> 00:01:19,840
So if people are having trouble, and I know this is a thing really, it's true, YouTube

15
00:01:19,840 --> 00:01:24,040
actually tells you, no one's going to watch a 47-minute video.

16
00:01:24,040 --> 00:01:30,680
You shouldn't upload it, you should upload short videos when you learn about what YouTube

17
00:01:30,680 --> 00:01:31,680
has to say.

18
00:01:31,680 --> 00:01:34,040
I mean, they know from experience.

19
00:01:34,040 --> 00:01:39,200
I think I've even got a tool that lets me see how many minutes of my video people watch

20
00:01:39,200 --> 00:01:40,200
on average.

21
00:01:40,200 --> 00:01:45,760
It says, out of a 47-minute video average number of minutes watched, it's probably

22
00:01:45,760 --> 00:01:48,400
about three.

23
00:01:48,400 --> 00:01:52,520
I don't know, I don't use the tool, but I can check.

24
00:01:52,520 --> 00:02:00,800
But it's a bit of a shame really because, as I said, and for good reason, Damapada's 47 minutes

25
00:02:00,800 --> 00:02:01,800
that's short.

26
00:02:01,800 --> 00:02:09,160
I've given a couple of hours, Damapada one's I think.

27
00:02:09,160 --> 00:02:12,560
People have to two hours, people start to complain a bit.

28
00:02:12,560 --> 00:02:18,000
About two or three hours is not unheard of.

29
00:02:18,000 --> 00:02:20,120
One hour is a pretty good length for a talk.

30
00:02:20,120 --> 00:02:23,800
It's usually what we aim for.

31
00:02:23,800 --> 00:02:27,720
Now I know that Damapada videos have been shorter, and some of the videos don't require

32
00:02:27,720 --> 00:02:32,480
it, and I'm probably going more in depth and more off track.

33
00:02:32,480 --> 00:02:33,480
And so I appreciate that.

34
00:02:33,480 --> 00:02:45,480
I will try to stay on track, maybe a little more on track than before, but being said, shouldn't

35
00:02:45,480 --> 00:02:51,360
be the length, it should be the content, if the content is objectionable then that's a

36
00:02:51,360 --> 00:02:52,360
problem.

37
00:02:52,360 --> 00:03:01,800
Anyway, not a big deal, I'll just thought it was interesting.

38
00:03:01,800 --> 00:03:06,320
We have a quote from the Buddha's last moments, that's actually a really good quote.

39
00:03:06,320 --> 00:03:16,480
It's one that I juntong brings up a lot.

40
00:03:16,480 --> 00:03:44,400
That's a part of it anyway.

41
00:03:44,400 --> 00:03:47,020
Thank you.

42
00:03:47,020 --> 00:04:00,800
Yoho en daa bikhu bikhu niva bikhu bikhu niva Satan de mantee purno

43
00:04:00,800 --> 00:04:07,780
weharati sam synaptic patipano anothama johnri

44
00:04:07,780 --> 00:03:50,340
so d

45
00:03:50,340 --> 00:04:11,680
sirakarthi karaun kar gy many

46
00:04:11,680 --> 00:04:21,300
Manet, Maneti, Pujeti, and there's a word that's not normally there.

47
00:04:21,300 --> 00:04:28,180
He doesn't put down with his photo caves through Nandisati.

48
00:04:28,180 --> 00:04:31,880
His word isn't in all of the person.

49
00:04:31,880 --> 00:04:59,180
It means any monk, female or male, or lead disciple, male or female, who practices the dhamma, in line with the dhamma, leading to the dhamma, who dwells practicing the dhamma,

50
00:04:59,180 --> 00:05:01,680
in order to attain the dhamma.

51
00:05:01,680 --> 00:05:09,180
Sami, Zhi, Pujeti, Pandavo, practices properly.

52
00:05:09,180 --> 00:05:22,180
Anudamata, the one who fares according to the dhamma, so that hag dhamma sat karuti, such a person does right by the Buddha, sat karuti.

53
00:05:22,180 --> 00:05:29,180
Gharuti does, makes reverence to the Buddha, makes homage.

54
00:05:29,180 --> 00:05:37,180
Maneti holds up high, holds in their mind, honors things highly of.

55
00:05:37,180 --> 00:05:49,180
Pujeti, reverence is apatiati, no idea what apatiati is, not in all of the other versions.

56
00:05:49,180 --> 00:05:56,180
Paramaya, Pujaya, with the highest form of reverence.

57
00:05:56,180 --> 00:06:06,180
Tasma, Thihan, and the dhamma, Nudhamma, Patipanga, Vihan is Sami, Zhi, Parthipanga, Anudhamma, Chandino.

58
00:06:06,180 --> 00:06:18,180
Therefore, ananda, you all should dwell, practicing the dhamma in order to attain the dhamma.

59
00:06:18,180 --> 00:06:33,180
Should practice properly, should fare according to the dhamma, unless you should train yourselves.

60
00:06:33,180 --> 00:06:57,180
Ah, yes, you have that actually, in English.

61
00:06:57,180 --> 00:07:03,180
So, do you have any questions for a domain?

62
00:07:03,180 --> 00:07:05,180
We do.

63
00:07:05,180 --> 00:07:13,180
Thinking about self and non-self, or what is, or what is not, I came to the idea that there is just happening.

64
00:07:13,180 --> 00:07:22,180
Is this idea of just happening in line with Buddhist teaching moments of happening, actions, acting upon actions?

65
00:07:22,180 --> 00:07:31,180
Yeah, I mean, that sort of insight is kind of the objectivity poking through,

66
00:07:31,180 --> 00:07:36,180
since you're starting to become more objective, that sounds what it sounds like.

67
00:07:36,180 --> 00:07:38,180
Don't hold on to it.

68
00:07:38,180 --> 00:07:39,180
That's a raft.

69
00:07:39,180 --> 00:07:43,180
That raft changed your mind, so it got you across something.

70
00:07:43,180 --> 00:07:51,180
That's through the raft away and keep going.

71
00:07:51,180 --> 00:07:52,180
Hello, Bhante.

72
00:07:52,180 --> 00:07:54,180
Can you talk about body scanning?

73
00:07:54,180 --> 00:07:57,180
It should always be categorized as solitary, right?

74
00:07:57,180 --> 00:08:04,180
Many people seem to associate practicing mindfulness with doing body scanning, or even associate

75
00:08:04,180 --> 00:08:09,180
the term, we possibly know body scanning, maybe due to goingka.

76
00:08:09,180 --> 00:08:14,180
The more I have practiced in sight meditation, the less I have done body scanning.

77
00:08:14,180 --> 00:08:16,180
Should I stop it completely?

78
00:08:16,180 --> 00:08:19,180
I've noticed it's easy to get attached to the tranquility.

79
00:08:19,180 --> 00:08:24,180
How to think about this and how to talk about it to the people that have the practicing

80
00:08:24,180 --> 00:08:27,180
mindfulness equals body scanning view.

81
00:08:27,180 --> 00:08:34,180
If one wants to practice repassana intensively, and advance once understanding of reality,

82
00:08:34,180 --> 00:08:39,180
is it counterproductive to do body scanning once in a while, or can it be useful somehow?

83
00:08:39,180 --> 00:08:41,180
Sorry for the articulation.

84
00:08:41,180 --> 00:08:43,180
I'm tired or something.

85
00:08:43,180 --> 00:08:46,180
Thank you.

86
00:08:46,180 --> 00:08:49,180
Yeah, the word body scanning.

87
00:08:49,180 --> 00:08:57,180
I mean, words are really problematic when people have catchphrases or labels for things.

88
00:08:57,180 --> 00:09:04,180
Like our, for example, our technique is often talked about as labeling, or noting.

89
00:09:04,180 --> 00:09:07,180
Acknowledging was a big one.

90
00:09:07,180 --> 00:09:11,180
Acknowledging, but acknowledging is a problematic term.

91
00:09:11,180 --> 00:09:18,180
Anyway, a little bit off track, the point being body scanning, in order to understand it,

92
00:09:18,180 --> 00:09:21,180
we really have to break it down and see what you're doing.

93
00:09:21,180 --> 00:09:28,180
Because body scanning is not, when you say body scanning, it is not immediately mean that it's some of that.

94
00:09:28,180 --> 00:09:31,180
No, that's, I don't think that's fair to say.

95
00:09:31,180 --> 00:09:36,180
As to whether it is proper meditation and whether it is, is we pass in that?

96
00:09:36,180 --> 00:09:42,180
Right, the question would be whether it's proper meditation, because we pass in that depends on the object.

97
00:09:42,180 --> 00:10:02,180
Now body scanning, body scanning could be problematic if it deals with a body, like moving through the body.

98
00:10:02,180 --> 00:10:07,180
The, first of all, I'll say there's no reason to suggest that one should practice in this way.

99
00:10:07,180 --> 00:10:19,180
I can't think of, I can't think of a single, even say that we see the maga, the passage, which encourages body scanning.

100
00:10:19,180 --> 00:10:24,180
I may, there may be, but I don't, I can't think of anything that hints at it.

101
00:10:24,180 --> 00:10:31,180
I can't even, I don't even, now maybe lady cyanosis something about this.

102
00:10:31,180 --> 00:10:34,180
And maybe it is in the commentary somewhere.

103
00:10:34,180 --> 00:10:38,180
But I, I would imagine that even lady cyanide didn't.

104
00:10:38,180 --> 00:10:39,180
I mean that would be a good question.

105
00:10:39,180 --> 00:10:45,180
Did lady cyanide, who was supposed to be the teacher of the teacher of Goenka?

106
00:10:45,180 --> 00:10:49,180
And lady cyanide is usually who they, they go back to.

107
00:10:49,180 --> 00:10:55,180
So the question would be whether lady cyanide or Wabu cyanide, who I think is another one, a student of lady cyanide.

108
00:10:55,180 --> 00:11:00,180
Whether these guys talked about body scanning and where they got it from.

109
00:11:00,180 --> 00:11:02,180
So I mean that is something.

110
00:11:02,180 --> 00:11:09,180
When there's no, when there's no longstanding tradition, or you can't bring it back to the Buddha,

111
00:11:09,180 --> 00:11:13,180
you can't bring it back to the sort of things that the Buddha taught.

112
00:11:13,180 --> 00:11:19,180
If you contrast that with what we do, as I've been pointing out, especially in our study of the Wisudimanga,

113
00:11:19,180 --> 00:11:23,180
there is very strong precedent for this kind of practice.

114
00:11:23,180 --> 00:11:31,180
In the Wisudimanga, it literally, or it, it directly explains to practice in this way.

115
00:11:31,180 --> 00:11:33,180
This is how you practice meditation.

116
00:11:33,180 --> 00:11:40,180
In the Satipatanasuta, the Buddha himself appears to be at least, it's a subject to interpretation,

117
00:11:40,180 --> 00:11:51,180
but the grammar itself supports directly, saying to yourself things like walking, walking, or angry, angry, or pain, pain, or so.

118
00:11:51,180 --> 00:11:58,180
It's pretty easy to read that into the Bible.

119
00:11:58,180 --> 00:12:03,180
What I'm saying, some people deny this, some people say, no, that's not what it says, because it depends what you mean by the grammar,

120
00:12:03,180 --> 00:12:10,180
but it literally says, cachanto wagachami di pajanati, cachami di means I am walking, and di is a quote.

121
00:12:10,180 --> 00:12:12,180
So it is a quote.

122
00:12:12,180 --> 00:12:16,180
You know, but the word the verb is pajanati, which means no is clearly.

123
00:12:16,180 --> 00:12:21,180
So one knows clearly, quote unquote, I am walking.

124
00:12:21,180 --> 00:12:34,180
So that coupled with the tradition of how meditation was practiced is a clear indication that what we're doing is not something new or off the wall or far-fetched.

125
00:12:34,180 --> 00:12:43,180
Now body scanning, just as a precursor, I want to say that, that I don't think it's well supported by the text.

126
00:12:43,180 --> 00:12:48,180
It could be wrong, and it would be interesting to talk to them about that.

127
00:12:48,180 --> 00:12:55,180
But that having been said, that's not that we shouldn't be dogmatic, so dogmatic as to discard it as a result of that.

128
00:12:55,180 --> 00:13:01,180
But as I said, we have a bit of a problem because it may encourage the concept of a body.

129
00:13:01,180 --> 00:13:13,180
The other thing it may encourage is self, in the sense of actively seeking out.

130
00:13:13,180 --> 00:13:18,180
Now, there's a little bit of that in what we do, but we're careful to, this is a sensitive subject.

131
00:13:18,180 --> 00:13:24,180
It's something that we have to be sensitive to, that we're not actually forcing or seeking.

132
00:13:24,180 --> 00:13:40,180
And so we tell you to focus on the stomach rising and falling, and you could argue that that pushing yourself to stay with the stomach could be, and it can become somewhat forceful and based on concepts of control.

133
00:13:40,180 --> 00:13:51,180
But we're fairly careful not to let it become that we say that's where we start, but we're completely open to letting the mind go into what is often called choiceless awareness.

134
00:13:51,180 --> 00:14:03,180
So it is pretty choiceless. We're not exactly choosing, even though we do choose the stomach, we do it as a base more than anything.

135
00:14:03,180 --> 00:14:06,180
I mean, it's still that potential criticism.

136
00:14:06,180 --> 00:14:14,180
And so I think some people would say, well, if you're going to be totally, we're going to be sincere about this.

137
00:14:14,180 --> 00:14:16,180
It should be completely choiceless.

138
00:14:16,180 --> 00:14:22,180
Of course, that's a problem, and that doesn't really work. You can't do that. And so as a result, we do use some control.

139
00:14:22,180 --> 00:14:39,180
And I think the biggest argument for either practice, whether it's the body scan or whether it's staying with the stomach rising and falling, is that you need to start from a point of control in order to just, you know, get your feet.

140
00:14:39,180 --> 00:14:46,180
In order to just get balanced enough to start to see and to see how control breaks down.

141
00:14:46,180 --> 00:14:51,180
So it's not exactly control, but there is the potential.

142
00:14:51,180 --> 00:14:58,180
And I think a good thing about the body scan is that it's not partial.

143
00:14:58,180 --> 00:15:06,180
So the idea is if you scan from head to feet, you're not choosing this part or that part based on your preference.

144
00:15:06,180 --> 00:15:11,180
Going to certain things and avoiding other things, you're forced to go through the entire body, I think.

145
00:15:11,180 --> 00:15:15,180
I've never practiced it. I think they do from head to feet.

146
00:15:15,180 --> 00:15:22,180
But that being said, apart from those minor thoughts about it, I don't think about it too much.

147
00:15:22,180 --> 00:15:27,180
And probably this is probably a shouldn't have said even that much because it's not our technique.

148
00:15:27,180 --> 00:15:32,180
And I don't like to talk about things that don't concern us.

149
00:15:32,180 --> 00:15:38,180
But it allowed me to talk about our technique and I think rather than attack other people's techniques,

150
00:15:38,180 --> 00:15:42,180
I would say that about our technique that it's well represented in the texts.

151
00:15:42,180 --> 00:15:49,180
And so that's why we practice this way.

152
00:15:49,180 --> 00:15:55,180
Is that everything there was a lot of question as to what you should do?

153
00:15:55,180 --> 00:15:57,180
Should you stop it completely?

154
00:15:57,180 --> 00:16:02,180
You should practice one way or the other if you're practicing our technique, practice our technique.

155
00:16:02,180 --> 00:16:06,180
If you're practicing that technique, practice that technique. You shouldn't mix.

156
00:16:06,180 --> 00:16:10,180
You shouldn't, because that becomes based based on your partiality, mixing.

157
00:16:10,180 --> 00:16:14,180
Apart from being confusing, it's also usually based on preference.

158
00:16:14,180 --> 00:16:20,180
I like to do this sometimes and that sometimes so when I feel like doing this on the disc, when I feel like doing that, I'll do that.

159
00:16:20,180 --> 00:16:28,180
And that's hugely problematic.

160
00:16:28,180 --> 00:16:35,180
How about the idea of talking about this two people who have the body scanning view?

161
00:16:35,180 --> 00:16:39,180
We're not about changing people's views. If they have that view, then power to them.

162
00:16:39,180 --> 00:16:42,180
If they're looking to change, then let them change.

163
00:16:42,180 --> 00:16:45,180
We're not trying to convince the world of our practice.

164
00:16:45,180 --> 00:16:49,180
Usually if people even want us to convince them we say,

165
00:16:49,180 --> 00:16:51,180
you know, it's too much trouble for me.

166
00:16:51,180 --> 00:16:57,180
I mean, that's what sort of the example we get from the arrow hunts in the texts.

167
00:16:57,180 --> 00:17:04,180
For the most part, they would be, you know, teaching you would be too much trouble as the kind of thing they would say.

168
00:17:04,180 --> 00:17:07,180
It's important. It's important not to pander, you know?

169
00:17:07,180 --> 00:17:17,180
We're not too, not too obsessed over changing other people.

170
00:17:17,180 --> 00:17:21,180
So look at yourself. At that point, you should look at your own thoughts.

171
00:17:21,180 --> 00:17:25,180
Do you understand this? Okay. Do you ever do it the way you understand it?

172
00:17:25,180 --> 00:17:28,180
And then you ask yourself, maybe I'm doing it wrong.

173
00:17:28,180 --> 00:17:30,180
That's when you have to figure out.

174
00:17:30,180 --> 00:17:33,180
Everyone should look at their own feet when they walk.

175
00:17:33,180 --> 00:17:37,180
Not literally, when we do walking meditation, don't look at your feet.

176
00:17:37,180 --> 00:17:49,180
But as the Buddha said, we watch our own paths. We don't worry about the footsteps of others.

177
00:17:49,180 --> 00:17:53,180
Question about the meditator list. Excuse me.

178
00:17:53,180 --> 00:17:57,180
What does the plus one and the number shown between the two hands into K?

179
00:17:57,180 --> 00:17:59,180
When has changed from 5 to 2?

180
00:17:59,180 --> 00:18:01,180
And fairly equal to the layer?

181
00:18:01,180 --> 00:18:03,180
I don't think that's possible.

182
00:18:03,180 --> 00:18:06,180
Unless you're using the Android app, it does weird things.

183
00:18:06,180 --> 00:18:08,180
I don't know how to fix it.

184
00:18:08,180 --> 00:18:12,180
Yeah, the Android app sometimes shows that shows it in green,

185
00:18:12,180 --> 00:18:16,180
meaning that you've already clicked on it when you have it.

186
00:18:16,180 --> 00:18:20,180
If you don't move the list, it fixes itself on the next update.

187
00:18:20,180 --> 00:18:24,180
But if you scroll up and down, something was kind of funny.

188
00:18:24,180 --> 00:18:28,180
Because there's something wrong with it. It's going to be fixed.

189
00:18:28,180 --> 00:18:32,180
We need someone who's smart.

190
00:18:32,180 --> 00:18:36,180
The plus one means plus one.

191
00:18:36,180 --> 00:18:38,180
That means you've done something good.

192
00:18:38,180 --> 00:18:40,180
How do I wonder if it's likes?

193
00:18:40,180 --> 00:18:42,180
It's likes.

194
00:18:42,180 --> 00:18:44,180
It's like a Facebook like.

195
00:18:44,180 --> 00:18:50,180
So I don't think they can go down, but...

196
00:18:50,180 --> 00:18:54,180
I don't know what it's like.

197
00:18:54,180 --> 00:18:57,180
Yeah, technical difficulties.

198
00:18:57,180 --> 00:19:02,180
And weekends I try to practice the 8 precepts. I've noticed strong craving,

199
00:19:02,180 --> 00:19:05,180
but there doesn't seem to be any object for the craving.

200
00:19:05,180 --> 00:19:07,180
It seems like the mind is just hungry.

201
00:19:07,180 --> 00:19:10,180
I can watch this hungry mind for only so long,

202
00:19:10,180 --> 00:19:12,180
then I end up feeding it.

203
00:19:12,180 --> 00:19:17,180
Is the idea to feed it, but in the most wholesome way possible?

204
00:19:17,180 --> 00:19:23,180
I think my feed it you're talking sort of a figurative feeding.

205
00:19:23,180 --> 00:19:26,180
I hope.

206
00:19:26,180 --> 00:19:32,180
I'm talking about feeding the mind, right?

207
00:19:32,180 --> 00:19:37,180
I mean, the best is if you can learn to overcome and let go of it.

208
00:19:37,180 --> 00:19:42,180
But yeah, if you're right, so you're trying to keep the precepts

209
00:19:42,180 --> 00:19:47,180
and then you end up breaking the 8 precepts maybe by listening music or something

210
00:19:47,180 --> 00:19:50,180
or watching a movie or something.

211
00:19:50,180 --> 00:19:53,180
I mean, there are ways to do it without breaking the precepts.

212
00:19:53,180 --> 00:19:57,180
It was just sort of distracting, but it's kind of beside the point.

213
00:19:57,180 --> 00:20:00,180
In the end, you have to learn to overcome it.

214
00:20:00,180 --> 00:20:04,180
If you're keeping the 8 precepts, you should keep the 8 precepts.

215
00:20:04,180 --> 00:20:06,180
It's only temporary.

216
00:20:06,180 --> 00:20:10,180
So you learn to start teaching yourself control if nothing else.

217
00:20:10,180 --> 00:20:14,180
But it's, I mean, that's part of the reason for keeping the 8 precepts is to learn about

218
00:20:14,180 --> 00:20:16,180
these, learn about your desires and to have a chance.

219
00:20:16,180 --> 00:20:26,180
Because if you constantly have the ability to indulge, to satisfy your desires,

220
00:20:26,180 --> 00:20:29,180
you'll never get to see what this desire is like.

221
00:20:29,180 --> 00:20:32,180
You'll never really get to understand what desire is.

222
00:20:32,180 --> 00:20:37,180
You'll never have a chance to challenge this idea that desire is worth having

223
00:20:37,180 --> 00:20:42,180
and that desire is a sign that you should chase after what you're looking for.

224
00:20:42,180 --> 00:20:47,180
So as you watch the desire without indulging in it,

225
00:20:47,180 --> 00:20:49,180
you're able to see a middle way,

226
00:20:49,180 --> 00:20:52,180
a way of just being with desire without acting on it

227
00:20:52,180 --> 00:20:54,180
or without repressing it.

228
00:20:54,180 --> 00:20:56,180
That's what we're looking for.

229
00:20:56,180 --> 00:20:59,180
That's, and so what you're talking about is not the idea.

230
00:20:59,180 --> 00:21:00,180
The idea is not defeated.

231
00:21:00,180 --> 00:21:03,180
But if you feed it, then just be as mindful as you can.

232
00:21:03,180 --> 00:21:13,180
Or if you're keeping any precepts, you shouldn't feed it.

233
00:21:13,180 --> 00:21:15,180
Hello, Bhante.

234
00:21:15,180 --> 00:21:19,180
Could you please tell whether it is true that one has to cycle through the

235
00:21:19,180 --> 00:21:23,180
insight knowledge is many, many times after third path before actually arriving at

236
00:21:23,180 --> 00:21:28,180
work path or does one only have to get through one more progress of insight cycle after

237
00:21:28,180 --> 00:21:30,180
third path to attain fourth path?

238
00:21:30,180 --> 00:21:32,180
Thanks.

239
00:21:32,180 --> 00:21:37,180
It only requires one cycle, but that's technically,

240
00:21:37,180 --> 00:21:41,180
realistically it usually requires many.

241
00:21:41,180 --> 00:21:43,180
But there's two different cycles.

242
00:21:43,180 --> 00:21:47,180
One cycle is only going to be a review,

243
00:21:47,180 --> 00:21:53,180
so it's not going to be all 16 stages of knowledge.

244
00:21:53,180 --> 00:21:59,180
You can actually only go through the stages of knowledge four times.

245
00:21:59,180 --> 00:22:04,180
So you're not actually going through all the stages of knowledge, not technically.

246
00:22:04,180 --> 00:22:05,180
Because it's missing.

247
00:22:05,180 --> 00:22:07,180
It's missing one until you reach the next path.

248
00:22:07,180 --> 00:22:11,180
But I think that's really a technicality.

249
00:22:11,180 --> 00:22:15,180
Every time you go through the knowledge, you reduce the number of,

250
00:22:15,180 --> 00:22:17,180
you reduce it, you're defined.

251
00:22:17,180 --> 00:22:20,180
So they get cut off piece by piece by piece.

252
00:22:20,180 --> 00:22:22,180
When you reach a certain point, that's called Ananga.

253
00:22:22,180 --> 00:22:23,180
And you reach another.

254
00:22:23,180 --> 00:22:28,180
When you reach the final point where there's none left, that's R.

255
00:22:28,180 --> 00:22:31,180
There's one more.

256
00:22:31,180 --> 00:22:49,180
Could you please talk about how Ananga progresses to our

257
00:22:49,180 --> 00:22:50,180
hardship?

258
00:22:50,180 --> 00:22:52,180
I am.

259
00:22:52,180 --> 00:22:56,180
At high equanimity after third path, any advice?

260
00:22:56,180 --> 00:23:00,180
That frustrated after practicing a long time?

261
00:23:00,180 --> 00:23:01,180
Advice.

262
00:23:01,180 --> 00:23:04,180
Here's a person with yellow.

263
00:23:04,180 --> 00:23:05,180
Orange.

264
00:23:05,180 --> 00:23:08,180
Who hasn't done any meditation with us?

265
00:23:08,180 --> 00:23:10,180
Ananga means don't get frustrated.

266
00:23:10,180 --> 00:23:12,180
I'm not going to answer your question.

267
00:23:12,180 --> 00:23:16,180
If you start meditating with us, I think I'm not going to answer these sorts of questions.

268
00:23:16,180 --> 00:23:17,180
It's either.

269
00:23:17,180 --> 00:23:21,180
If you want to meditate with us, you should start logging your meditation.

270
00:23:21,180 --> 00:23:22,180
Read my booklet.

271
00:23:22,180 --> 00:23:25,180
Start practicing according to our technique.

272
00:23:25,180 --> 00:23:28,180
Start logging your meditation, and if you have questions about your meditation,

273
00:23:28,180 --> 00:23:30,180
I'm happy to answer them.

274
00:23:30,180 --> 00:23:33,180
But I'm not going to talk about these.

275
00:23:33,180 --> 00:23:36,180
Because it's not something I can verify, and from the sounds of it,

276
00:23:36,180 --> 00:23:45,180
there's a misunderstanding of what the Ananga means.

277
00:23:45,180 --> 00:23:49,180
Sorry.

278
00:23:49,180 --> 00:23:51,180
And with that, we are caught up.

279
00:23:51,180 --> 00:23:54,180
Nobody clicks on people's hands anymore, do they?

280
00:23:54,180 --> 00:23:56,180
I used to.

281
00:23:56,180 --> 00:23:57,180
I do sometimes.

282
00:23:57,180 --> 00:23:58,180
There's a lot of them.

283
00:23:58,180 --> 00:24:03,180
Click, click, click.

284
00:24:03,180 --> 00:24:06,180
Well, we don't do very often.

285
00:24:06,180 --> 00:24:09,180
It is up on the top part.

286
00:24:09,180 --> 00:24:14,180
Click to like questions and comments and things.

287
00:24:14,180 --> 00:24:17,180
I forget to do that.

288
00:24:17,180 --> 00:24:38,180
Okay, that's all then for tonight.

289
00:24:38,180 --> 00:24:40,180
Thank you all for tuning in.

290
00:24:40,180 --> 00:24:41,180
Good night.

291
00:24:41,180 --> 00:24:42,180
Thanks for having me.

292
00:24:42,180 --> 00:24:43,180
Thank you.

293
00:24:43,180 --> 00:24:47,180
Good night.

