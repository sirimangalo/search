1
00:00:00,000 --> 00:00:26,000
How do we balance our faculties with wisdom balancing confidence and what should we do to stay in the middle path?

2
00:00:26,000 --> 00:00:31,000
Well, I have talked about the balancing the faculties.

3
00:00:31,000 --> 00:00:36,000
It's in the booklet, if you've read my booklet on how to meditate.

4
00:00:36,000 --> 00:00:42,000
It gives, I mean a lot of the stuff in that booklet is just straight what my teacher taught.

5
00:00:42,000 --> 00:00:49,000
Which is why I made the videos and why I made the booklet because he taught the same thing almost every day and he didn't change it.

6
00:00:49,000 --> 00:00:52,000
It wasn't based on any idea of his.

7
00:00:52,000 --> 00:00:56,000
It's like this is what the Buddha taught and giving it to people.

8
00:00:56,000 --> 00:01:00,000
So it seemed like the kind of thing that you could really systematize.

9
00:01:00,000 --> 00:01:11,000
Not overly systematize, but just get the information out there because once people know they have the instruction manual then they can go with it.

10
00:01:11,000 --> 00:01:21,000
So there is a standard explanation of how to balance the faculties and hopefully that's what's conveyed in the book.

11
00:01:21,000 --> 00:01:35,000
But something that isn't conveyed in the book is this famous simile that is also given that the faculties are like horses and the rider.

12
00:01:35,000 --> 00:01:45,000
Confidence, wisdom, concentration and confidence, concentration, wisdom and effort.

13
00:01:45,000 --> 00:01:56,000
In the total wrong order, confidence, effort, concentration and wisdom are like four horses and they pair up together.

14
00:01:56,000 --> 00:02:04,000
So you've got confidence and wisdom as a pair and then you've got behind them you've got effort and concentration and they're a pair.

15
00:02:04,000 --> 00:02:12,000
And these four horses have to be tied together and have to be going in unison in order for the carriage to go properly.

16
00:02:12,000 --> 00:02:17,000
So what do you need to make the carriage go properly and to keep the horses in check when you need a driver.

17
00:02:17,000 --> 00:02:24,000
The driver of the four horses and this driver is the fifth faculty and that's mindfulness.

18
00:02:24,000 --> 00:02:36,000
And this is how we should understand how to, the teaching on the five faculties is actually not so much a teaching on how to.

19
00:02:36,000 --> 00:02:47,000
It's a teaching on when to or why to or how to apply mindfulness.

20
00:02:47,000 --> 00:02:54,000
Because if you're mindful then the faculties will naturally be balanced.

21
00:02:54,000 --> 00:03:02,000
There is no imbalance of the faculties for someone who is in a mindful and clear clearly aware state.

22
00:03:02,000 --> 00:03:13,000
It's when we're unmindful that the imbalance occurs for whatever reason.

23
00:03:13,000 --> 00:03:27,000
So the importance of this teaching know that if you have too much confidence it leads to greed or depending on who you ask.

24
00:03:27,000 --> 00:03:32,000
It leads to, you know, obviously it leads to following after your own intentions.

25
00:03:32,000 --> 00:03:36,000
Maybe it should always say it leads to greed I think but I've read elsewhere.

26
00:03:36,000 --> 00:03:40,000
There's a different explanation I can't remember.

27
00:03:40,000 --> 00:03:46,000
But as I always as I generally explain it that you tend to believe and follow your own intentions.

28
00:03:46,000 --> 00:03:49,000
So you want something you think yeah that's right.

29
00:03:49,000 --> 00:03:57,000
You did this sort of person who thinks things are right because they're right because I think it's right.

30
00:03:57,000 --> 00:03:59,000
I think it's right because I think it's right.

31
00:03:59,000 --> 00:04:04,000
And so whatever hits them they whatever it says hey that's a good idea let's do it.

32
00:04:04,000 --> 00:04:11,000
Without even thinking or examining or looking or reflecting as the Buddha said on what are the results of that action.

33
00:04:11,000 --> 00:04:15,000
It's a good talk the Buddha gave to Rahul that's very famous.

34
00:04:15,000 --> 00:04:21,000
He said what are mirrors for and Rahul is the Buddha's son.

35
00:04:21,000 --> 00:04:25,000
The Buddha had a son before he went off to become monk.

36
00:04:25,000 --> 00:04:30,000
He said what are mirrors for and Rahul is that mirrors are for reflecting.

37
00:04:30,000 --> 00:04:35,000
And he said in the same way all you should reflect on all of your acts.

38
00:04:35,000 --> 00:04:40,000
You should use mindfulness to reflect upon all of your acts.

39
00:04:40,000 --> 00:04:44,000
When you have done something when you have said something when you even thought something.

40
00:04:44,000 --> 00:04:47,000
You should mindfully reflect on the act.

41
00:04:47,000 --> 00:04:51,000
Was it good to do that? Was it good to say that? Was it good to think that?

42
00:04:51,000 --> 00:04:53,000
What was the result?

43
00:04:53,000 --> 00:05:00,000
And if we all did this we'd be able to see what was causing us happiness and causing us suffering.

44
00:05:00,000 --> 00:05:04,000
Because people have too much confidence they don't do this.

45
00:05:04,000 --> 00:05:11,000
It's very hard to deal with such people because they really believe in what they're doing and what they're saying and what they're thinking.

46
00:05:11,000 --> 00:05:15,000
Without even looking and seeing what harm it's causing to themselves and others.

47
00:05:15,000 --> 00:05:19,000
That explains a lot of the problems that go on in this world.

48
00:05:19,000 --> 00:05:27,000
If you have too much wisdom and hear wisdom in the sense of not having seen it for yourself.

49
00:05:27,000 --> 00:05:32,000
But having read about it or having thought about it or having intellectually understood it.

50
00:05:32,000 --> 00:05:38,000
Because it really, it seems so much like direct realization.

51
00:05:38,000 --> 00:05:44,000
People who have read all of the Buddha's teaching can sound exactly like an enlightened being.

52
00:05:44,000 --> 00:05:53,000
It seems wow yeah they know everything and they can teach and people who practice can become enlightened even.

53
00:05:53,000 --> 00:05:58,000
But they don't have the verification and therefore they don't have the confidence.

54
00:05:58,000 --> 00:06:02,000
And so as a result it leads to doubt.

55
00:06:02,000 --> 00:06:09,000
It leads them to be always thinking and considering and wondering what is the way and what is this?

56
00:06:09,000 --> 00:06:12,000
And so they have a lot of doubt.

57
00:06:12,000 --> 00:06:20,000
The person has too much effort but not enough concentration and they will become distracted.

58
00:06:20,000 --> 00:06:22,000
This kind of goes with what I just said.

59
00:06:22,000 --> 00:06:25,000
Sometimes it's not doubt but it's just thinking a lot.

60
00:06:25,000 --> 00:06:30,000
If you're thinking a lot you find that you have probably too much energy in so you can do lying meditation.

61
00:06:30,000 --> 00:06:33,000
Yeah there are tricks for some of them actually.

62
00:06:33,000 --> 00:06:35,000
In fact there are tricks for all of them.

63
00:06:35,000 --> 00:06:40,000
If we do the Sati Patanasu that I can go through it and I did in second life actually go through the...

64
00:06:40,000 --> 00:06:44,000
That's right I had a Sutta study course at one point.

65
00:06:44,000 --> 00:06:50,000
And we went through the Sati Patanasu and I talked about what the commentary had to say on the faculties.

66
00:06:50,000 --> 00:06:54,000
I would recommend looking at the commentary this to the Sati Patanasu dates.

67
00:06:54,000 --> 00:07:02,000
In the way of mindfulness, this book called The Way of Mindfulness which is an excellent book.

68
00:07:02,000 --> 00:07:04,000
Worth everyone reading.

69
00:07:04,000 --> 00:07:06,000
It's not written by the author.

70
00:07:06,000 --> 00:07:12,000
It's a translation and compilation of the discourse and its commentary and sub-commentary.

71
00:07:12,000 --> 00:07:17,000
Excellent resource by Soma Taylor.

72
00:07:17,000 --> 00:07:22,000
So if you read the part on the five faculties what the commentary is to say.

73
00:07:22,000 --> 00:07:26,000
It'll talk about how to improve each of these in different ways that you can improve them.

74
00:07:26,000 --> 00:07:32,000
Some of them are a little bit odd but in general pretty good advice.

75
00:07:32,000 --> 00:07:34,000
And it's certainly not all inclusive I don't think.

76
00:07:34,000 --> 00:07:39,000
I think there are some ways that it doesn't mention.

77
00:07:39,000 --> 00:07:46,000
Yeah and if a person has a lot of concentration but not enough effort then they'll feel tired and sleepy

78
00:07:46,000 --> 00:07:49,000
and as a result they should get up and do walking meditation.

79
00:07:49,000 --> 00:07:53,000
There's kind of things you can balance them in this way.

80
00:07:53,000 --> 00:07:55,000
But ultimately none of them really work.

81
00:07:55,000 --> 00:08:02,000
None of them really have the ultimate result of balancing the mind in the way that mindfulness does.

82
00:08:02,000 --> 00:08:07,000
So talking about them is identifying in yourself what's going wrong

83
00:08:07,000 --> 00:08:12,000
and making people realize that ah confidence isn't the answer.

84
00:08:12,000 --> 00:08:16,000
Because look at me I'm confident and you're being an idiot and doing all these stupid things.

85
00:08:16,000 --> 00:08:19,000
Drunk people are very confident.

86
00:08:19,000 --> 00:08:23,000
It doesn't mean that drunk people are always doing the right thing.

87
00:08:23,000 --> 00:08:24,000
Right?

88
00:08:24,000 --> 00:08:27,000
That was a good example.

89
00:08:27,000 --> 00:08:30,000
And to people who think a lot.

90
00:08:30,000 --> 00:08:32,000
People who study a lot.

91
00:08:32,000 --> 00:08:35,000
To help them to see them know you're not enlightened.

92
00:08:35,000 --> 00:08:42,000
I know it's really not doing a lot of good to be able to recite all of these teachings and so on.

93
00:08:42,000 --> 00:08:44,000
Remember all of these things.

94
00:08:44,000 --> 00:08:46,000
It's always funny.

95
00:08:46,000 --> 00:08:56,000
I find it funny and you're talking to people who are able to point out the smallest of errors.

96
00:08:56,000 --> 00:09:00,000
I give a talk once in Thailand I was learning how to give talks in Thai.

97
00:09:00,000 --> 00:09:01,000
So I give this talk in Thai.

98
00:09:01,000 --> 00:09:04,000
I mean that's an accomplishment.

99
00:09:04,000 --> 00:09:09,000
Just to be able to give a talk in Thai is an accomplishment.

100
00:09:09,000 --> 00:09:14,000
I give a talk on the Buddha, the Dhamma and the Sangha, an easy talk.

101
00:09:14,000 --> 00:09:15,000
Based on what are the three refuges?

102
00:09:15,000 --> 00:09:16,000
What is the Buddha?

103
00:09:16,000 --> 00:09:17,000
What is the Dhamma?

104
00:09:17,000 --> 00:09:18,000
What is the Sangha?

105
00:09:18,000 --> 00:09:20,000
According to a meditator.

106
00:09:20,000 --> 00:09:22,000
I gave it totally in Thai.

107
00:09:22,000 --> 00:09:25,000
This non came up and she said oh you're just like a Thai person.

108
00:09:25,000 --> 00:09:26,000
They felt so good.

109
00:09:26,000 --> 00:09:27,000
I'm happy.

110
00:09:27,000 --> 00:09:28,000
Proud of myself.

111
00:09:28,000 --> 00:09:29,000
No.

112
00:09:29,000 --> 00:09:35,000
And then this man, this guy who you know wasn't really interested.

113
00:09:35,000 --> 00:09:38,000
He had come to really cause trouble in the monastery.

114
00:09:38,000 --> 00:09:45,000
He was looking, criticizing even our head teacher who's the best monk or the best,

115
00:09:45,000 --> 00:09:49,000
the most noble being I've ever met.

116
00:09:49,000 --> 00:09:56,000
And he came up to me the next day and he said he said, because I had said in the talk.

117
00:09:56,000 --> 00:10:00,000
I had said when we talk about the Sangha, there are different kinds of Sangha.

118
00:10:00,000 --> 00:10:07,000
When we talk about Samuti Sangha, which is the conceptual Sangha, then I said it.

119
00:10:07,000 --> 00:10:12,000
Any monk is the Sangha.

120
00:10:12,000 --> 00:10:20,000
One or any monk is Samuti Sangha?

121
00:10:20,000 --> 00:10:24,000
I was just basically saying that the group of monks are just conceptual.

122
00:10:24,000 --> 00:10:27,000
So a monk can say I'm Sangha.

123
00:10:27,000 --> 00:10:33,000
I'm part of the Sangha because he's wearing a robe, because he's had the ordination.

124
00:10:33,000 --> 00:10:39,000
And he came up to me the next day and said, you know, what is the Samuti Sangha?

125
00:10:39,000 --> 00:10:42,000
He said, well, as I said, any monk is a Samuti Sangha.

126
00:10:42,000 --> 00:10:44,000
He said, no, you're wrong.

127
00:10:44,000 --> 00:10:48,000
And he said, my chai, he said, no.

128
00:10:48,000 --> 00:10:50,000
I said, well, then what's the Sangha?

129
00:10:50,000 --> 00:10:53,000
He said, four monks, that's a Samuti Sangha.

130
00:10:53,000 --> 00:10:59,000
And you know, he's right because the Sangha, in a technical sense in the Vinaya,

131
00:10:59,000 --> 00:11:04,000
four monks together constitute what is called a Sangha.

132
00:11:04,000 --> 00:11:05,000
So he was correct.

133
00:11:05,000 --> 00:11:06,000
But he was also wrong.

134
00:11:06,000 --> 00:11:09,000
And he was wrong to criticize what I was saying.

135
00:11:09,000 --> 00:11:14,000
Because I wasn't saying that one monk alone constitutes a valid Sangha.

136
00:11:14,000 --> 00:11:19,000
It's not true, but a monk is Sangha.

137
00:11:19,000 --> 00:11:22,000
And that's generally understood to be that way.

138
00:11:22,000 --> 00:11:25,000
Anyone who talks about it, even in Thailand, they talk about monks.

139
00:11:25,000 --> 00:11:29,000
They say, oh, that's Prasong, Prasong, which means the Sangha.

140
00:11:29,000 --> 00:11:32,000
You know, oh, he's Prasong, he's Sangha.

141
00:11:32,000 --> 00:11:35,000
And we talk like this.

142
00:11:35,000 --> 00:11:38,000
A way of track, way of topic here.

143
00:11:38,000 --> 00:11:42,000
But the point being that this is the kind of,

144
00:11:42,000 --> 00:11:46,000
this is a perfect example of how someone takes it too far.

145
00:11:46,000 --> 00:11:48,000
And then they go around trying to point out the thoughts.

146
00:11:48,000 --> 00:11:53,000
I mean, I was thinking, man, you just heard me give my first real,

147
00:11:53,000 --> 00:11:55,000
Thai talk.

148
00:11:55,000 --> 00:12:01,000
And all you can think about is how I flubbed the word Sangha.

149
00:12:01,000 --> 00:12:09,000
So it was a good test for me as far as the ego goes.

150
00:12:09,000 --> 00:12:14,000
But also a very good example here of this sort of thing.

151
00:12:14,000 --> 00:12:22,000
And the recognition here, I mean, this is the kind of talk you would give to that sort of person.

152
00:12:22,000 --> 00:12:25,000
There's a better talk, actually.

153
00:12:25,000 --> 00:12:28,000
The talk I give on the five types of people.

154
00:12:28,000 --> 00:12:33,000
It's just a talk that my teacher always just to give on the five types of dhamma wihari,

155
00:12:33,000 --> 00:12:36,000
if you've ever heard me talk about what is a dhamma wihari,

156
00:12:36,000 --> 00:12:39,000
the five types of people that exist in the world.

157
00:12:39,000 --> 00:12:41,000
It's a good one to give to that person.

158
00:12:41,000 --> 00:12:44,000
But giving the talk on the balancing the faculties is useful

159
00:12:44,000 --> 00:12:48,000
because it suddenly reminds us, oh, yes, in my, I've got these as well.

160
00:12:48,000 --> 00:12:56,000
I'm very active or I'm very lazy or I'm very skeptical of everything,

161
00:12:56,000 --> 00:13:02,000
or I have so much overconfidence and so on.

162
00:13:02,000 --> 00:13:06,000
So people always ask, how should we balance the faculties?

163
00:13:06,000 --> 00:13:08,000
And we really shouldn't look at it that way.

164
00:13:08,000 --> 00:13:10,000
You should practice mindfulness.

165
00:13:10,000 --> 00:13:12,000
And the answer is very simple that through mindfulness,

166
00:13:12,000 --> 00:13:14,000
you balance the faculties.

167
00:13:14,000 --> 00:13:17,000
What you should do from time to time and look and see is look and see

168
00:13:17,000 --> 00:13:21,000
how your faculties are doing because what happens is we miss one.

169
00:13:21,000 --> 00:13:24,000
We think we're being mindful and we're mindful of some things,

170
00:13:24,000 --> 00:13:25,000
but we're missing.

171
00:13:25,000 --> 00:13:28,000
We fail to catch something and we do it over and over again

172
00:13:28,000 --> 00:13:30,000
and then we can't understand why we're suffering.

173
00:13:30,000 --> 00:13:32,000
Here I am practicing meditation.

174
00:13:32,000 --> 00:13:33,000
Why am I still suffering?

175
00:13:33,000 --> 00:13:36,000
You're probably missing something.

176
00:13:36,000 --> 00:13:40,000
Okay, that's the first part of your question.

177
00:13:40,000 --> 00:13:41,000
Let me stop there.

178
00:13:41,000 --> 00:13:45,000
I'm just going to save this and then I'll answer the second part.

