Hello, good evening, welcome back again Wednesday, and here with Shradha today, Chris
has some problem with his throat, I think, Chris is not well.
So he might be here, but he's not able to talk, so he might help out with the moderating.
So welcome, we're here to investigate the dhamma, to learn about the dhamma, the study
the dhamma together. The dhamma means the truth or reality. So we come here to practice and
to think about the dhamma as well, because people will be asking questions, I'll be answering
them, so unavoidably there will be some thinking involved. We just can't confuse the thinking
from the practicing, don't ever think that study and intellect and mental consideration
is ever going to be a replacement for seeing clearly, seeing clearly can only come from
looking investigating. So although everyone, please feel free to chat and talk for the
first bit, this is a time for us to express our appreciation of each other by expressing
greetings, some old niyakata, pleasant speech, pleasant trees. They will come a point where
I ask you to cease, no more chatting, not yet, but I'll tell you when. And then in the chat
box we ask there will only be questions, no answers to questions, no comments, no, no nothing.
Because at that point you should close your eyes and practice, bring your attention to
your experience, try to learn more about yourself, try to find the dhamma within yourself.
And try to be copying questions, you can post them now, you can post them once we start,
try to copy them down and post them, ask them, and I will answer them.
Questions were most prioritizing those that relate to practice, especially those that require
an answer. There are questions where the person asking them could benefit in their practice
from getting an answer, those are the questions we're going to prioritize. Be on that if
there are other questions about practice or questions about Buddhism, well we'll prioritize
them lower. Some questions we just don't answer, questions of a personal nature about
me or about other people or other traditions or that sort of thing, just not interested
and I'm not at the birdie to talk about my own personal practice.
Questions about the monk's life and that sort of thing or lower priority.
If you can try your best to use proper grammar and spelling, put some care into your questions.
Absolutely, there are many people who come here who's English is not very good and that's
perfectly valid but there's no excuse if your English is good for not using proper punctuation
and grammar out of laziness, sloppiness, carelessness. Try and be mindful and ask the
best question you can, clearest, most concise, you're more likely to get a positive response.
I'm ready, I think at this point we'll ask to close the chat, so from here on please refrain
from posting anything in chat that's not a question everyone. Close your eyes, put your
mind on the body. The body is a great object of meditation because it's the most
prominent aspect of experience. It's the pasture of the mind. The mind spends its time
in the body and the mind is best observed when focused on the body. So by focusing on the
body you learn about how the mind works because the mind is kept present and the mind
is easily observed as it arises with the body and ceases with the body. You can focus on
the stomach, rising and falling. You can focus on the posture of the body sitting, standing,
lying, walking. You can focus on the feelings of pleasure, pain, calm and just find a
word to remind yourself of the experience. They'll be sitting or rising, falling, pain,
happy, calm, thinking, liking, disliking. Find words that are mantras that bring the mind
to focus on the object, to focus on the experience. Allow you to see the experience just
as it is without judgment, without reaction, without interpretation, without identification.
And we'll get started now on questions. If you don't have questions, don't post, no more
posts in the chat, please. Except only questions from here on. And I'll ask our moderators
to delete anything that's not a question, just to keep it clean. And to remind people
that you're supposed to be focused, close your eyes, practice with us. Don't worry, if your
question's been posted, somebody will get to it in Duke time.
During meditation, how should we handle persistent sounds or persistent body-to-year violence, or anything
else that keeps grabbing your attention away from this moment? It sometimes feels like my mind
is taking every chance that can to keep him from having an effective, streamlined meditation
session. So part of the problem is this concept of an effective streamlined meditation
session, as though a mind that was with the stomach the whole time would somehow be preferable
to what you're experiencing. Now, there really is no question that would be the case.
It would be great if you could. But what's stopping you is not your inability to practice
properly. It's the imperfections of your mind. And the mind is not perfect. And that is
the object of meditation practice, to see the imperfections of the mind, to see the
nature of ourselves, of the body and the mind. So there's no good that comes from wishing
or wanting to be focused on the stomach, for example. Proper practice is to note and observe
the way the mind works, the habits of the mind be good or bad. In this case, it's a habit
of distraction, I guess, then you would say distracted, distracted. But the practice isn't
to control. It isn't to find a way to be with the stomach. There would be no effect. There
would be no positive effect to that. In fact, there would be a negative effect if you were
to try to force your mind to stay with the stomach. The nature of experience is unpredictable.
It's unfixable. It's unsatisfying. You can't gain satisfaction. So the desire to make
it some other way is really the problem. That's what causes you stress and suffering, not
the distraction. How you should handle persistent sounds, et cetera, as the same as how you
should handle occasional experiences or any experiences. Persistent is just a judgment,
it's just an interpretation. Meditation is focused on the present moment. What's happening
now? And what's happening now has no reference to what's happened in the past or what will
happen in the future. So there's no persistent. When an arise is noted, you'll come to see
more clearly about how the mind works and you'll change the way you approach experience
from trying to change or fix or perfect to just try to understand. And perfection comes
from understanding, comes from seeing clearly. Any suggestions for maintaining mindfulness
during working in a capitalistic structure? Like signed, I built tension throughout the work
day, even when I meditate beforehand and go in with mindful intention.
Well, don't be concerned about the built tension. If there's tension, may take that as your
object of meditation. You know, it's a misunderstanding to think that meditation should
make everything smooth. It's not your experience as that will change and certainly not
in the beginning. It's your reactions to them. Even enlightened beings have stress and
suffering, stress in the sense of physical stress and stress from having to even do mental
work. What they don't have is the disliking, the reaction to it, the worry, the upset,
any of that. So even tension can build in enlightened being because they have to work sometimes.
They have to do things they have to think. But that tension is something they're very mindful
of. And so they don't suffer even when there's tension. It's easy to mistake
Yeah, innocent experiences from problem as problems. We mistake our experiences as problems.
The problem is not with the experiences. And so we think there's something wrong with
our practice when in fact it's just something wrong with the universe. The universe is unpredictable.
It's unsatisfying. It's uncontrollable. And so you're going to be able to control. You're
going to go through experiences that are unpleasant. It's up to you whether you're affected
by them. When focusing on my stomach, I feel strong energy or feeling in my chest neck
or head and top of my head. Not maintaining awareness of the youth.
Yes. Just say feeling, feeling. Energy is just an interpretation really. It's just a feeling.
If you like it or dislike it, try and note that as well.
One day when we realize we've encountered one of the hindrances. Do we note the hindrances
itself or knowing knowing? Have you read the booklet? If you haven't read the booklet,
I recommend you do that. It does talk about the hindrances. It may not be as clear as I would
like, but it does talk about them. The hindrances are liking, disliking, drowsiness, distraction
and doubt. Those are simple names for them. You don't need to use the poly or the academic
English literal translations. You would just say liking, disliking, drowsiness, distraction.
Yes. Note the hindrances itself. I guess I see what you're saying. Maybe when you know
that you've encountered one. Like, oh, I was just angry there or something. If it's afterwards
and you realize it too late, you can just say knowing knowing. But you could also just say
angry because you're just trying to remind yourself that was anger. Should we vocalize
mantras in our head or if we inform vocalization? You should vocalize them in the object.
All I mean by that is it's misleading to think that somehow the mind is in the head. There's
a lot of mental activity that goes on in the head, of course, a lot of our senses are there,
but not all mental activity takes place there. Vocalization is a mantra. It's a meditation
tool. Yes, you should vocalize, but it's not in the head. It's in the object.
All right. It's what leans your, it's related to the object, really being in the mind.
It has no place.
On date, my astrophobia is getting worse and worse. Now I'm having panic attacks, even
seeing it as an attachment to being safe and a version to pee or to proceed. I don't
know what astrophobia is, but phobia, you know what that means. So if you're afraid, you
just say afraid, panic attacks, just say panicking or see the thing about panicking is
it's fear and then it's physical. So try and be, try and separate those two. The physical
is not the fear, the fear is not the physical. Whatever the physical is, it reinforces because
you react to the physical thinking, oh, I'm panicking and it creates a feedback loop.
So try and break that chain when you're afraid, say afraid, but when you feel the physical
results, try and note those as just physical experiences, feeling or tense or so on. If you
dislike pain, as you say, say disliking, if you crave to be safe, say wanting. Try not to judge
it as getting worse or worse either. Don't make reference to that. Just take it as it is
at any moment. Because getting worse and worse is a part of that feedback loop of reinforcing
your fear, reinforcing your dislike, your aversion. It's a narrative you're telling yourself
and that's part of the problem. And since the noting is changing the narrative, it's creating
a new narrative. A narrative that no one would really want to listen to, but that's the
whole point. It helps you get bored of it all because it's what's really happening. It's
like a sports broadcaster. Just play, play, play. No, I'm seeing. No, I'm hearing. See, we
have worked ourselves up. Our narratives are exciting. I'm clinically depressed. I have
Astrophobia. It's getting worse and worse. That's a narrative. Don't fall into that narrative.
Create a new narrative. Now I'm seeing. No, I'm hearing. No, I'm feeling. No, I'm afraid.
You get bored of it pretty quick. When you get bored, you'll let go and say, oh, it's just
this boring, uninteresting series of experiences.
You freshly have students. How does one become your student? I put the things to their
course in the book. Yep, there's two ways. One, you can read the booklet and then it's
a good way to follow this tradition. The second way, well, if you really want to take
me as your teacher, you can, it's not really taking me as your teacher. It's just a temporary
thing. You don't have to officially say, it's not a, it's not a for life thing. But
you would take me as your teacher for the duration of the course. Then you can go to our
meditation hub and there's a way to sign up for a slot. And then we meet once a week
and you practice at home till we can find a way to have meditators come to stay with
us again. One to these are two different questions by two people, but they're kind of related
so I put them together. The first one is, I often find that I reach a point in meditation
where I struggle to continue. Is it okay to force myself into second question is, how to encourage
self to meditate even when you're not wanting to? Yeah, that's the same sort of problem.
The focus should be on the struggle. What does it mean to struggle? When not wanting
to, it's the same sort of idea. Usually it's an aversion, but it can be a desire to do
something else. And those should be the object of your mindfulness. If you learn to focus
on those, you won't need to force yourself or encourage yourself. You shouldn't rely
on encouragement. You shouldn't rely on forcing or anything like that. It's only a temporary
solution. Not really effective. Try and focus on what you are experiencing, which is the
lack of desire, the aversion, the desire to do other things, boredom, all of that. Meditation
will evoke many responses, many reactions. And it's those reactions we want to learn
about to help us to understand that it's those reactions that are the problem, not the activity
that we're undertaking. Could use pacify how wanting feeds itself and how to break the
feedback. There is seeing the object, then the liking, then the intermission to amplify it. Want to
indeed then a hell of stress. A hell of stress. I don't know what you want as an answer. I mean,
it sounds like you see that. How to break the feedback. Again, it's a practice. I think asking
a question like this is motivated by a sense of despair perhaps. And it's a common sense because
craving is not a simple thing to overcome. It's a difficult thing. It's a monumental task.
Craving is what causes us to be born. That's to be born again and again,
at infinitum. It's not an easy thing to overcome. But I said it like this. He said,
don't get angry. Don't get angry. Be always mindful. Calm your mind. Become,
mild, not calm. Become internally composed. And train yourself to leave behind craving.
And the phrasing is interesting. Adjantong, my teacher pointed this out. He said, the phrasing is
interesting because he just says, don't be angry. A cold, I was it. I'll be up and no. No,
don't have any ill will. Just be someone who doesn't have ill will. But then he doesn't say,
don't be someone who doesn't have craving. A version is something you can give up.
Maybe not entirely or not easily, of course. But craving is different. Craving is something you have
to slowly slowly break. Craving is pernicious. A bita vinengi, cicam. Cicam, train yourself
to become free from a bita, which is well desired. So not really an answer. Just a reassurance
that it takes time. Don't be impatient. Don't be discouraged. Slowly, slowly, you cultivate
understanding. Clarity of mind is that clarity makes it impossible to desire things which are
undesirable or not worthy of desire, which is everything that arises. When you start to see that
things arise and cease, you find that there is no reason to desire them. There's no cause
for it. I was roller skating the other day and noticed bugs all over the pavement. I must at
some point be rolling over these bugs. I must also be doing so when I'm biking or driving with this
and more. No. No, a morality is never the action anyway. It's the mine state. So if you're worried
about it, that's actually immoral. If you're sad about being dying, that's also immoral.
The immorality and Buddhism is just what is a bad habit in the mind.
If you're roller skating for fun while the fun part is unfortunately immoral, immoral is just,
I mean, it's a really not a great word because it doesn't actually
in English give the sense that we need, but it is unethical, technically, technically anything
that cultivates or encourages bad habits is unethical. So any craving, any activity which is
induced by craving and encouraging and reinforcing craving is unethical. Even walking
can't be unethical. Roller skating certainly, driving, but it's not the action. It's the quality
of mind, quality of mind that it reinforces. Now, if when you do these activities, you're constantly
sad or worried about beings being killed, then maybe you should stop doing those activities because
that's immoral. And of course, I mean, what you're asking is it would be immoral if you knew
there were beings there and intentionally roller skated over them. Like you see some bugs in you,
roller skate over them in order to kill them. That would be unethical.
Deeply unethical, like on another level, that would be breaking the five precepts.
It's very hard to be reborn as a human being if you break the five precepts. If you're
casually and repeatedly and habitually break the five precepts, it's very difficult to be born
to human. Instead, okay, that I only get through maybe 12 to 14 touching points in an hour
sitting fashion. This is a follow up to the first question one day answer. Yeah, if you can't get
through them all, then you might want to just do fewer. You don't have to do them all. You should
do them if you have the focus and the concentration. You might want to work up to them
like we did in the course.
The idea is to do the points as you're able to do them. So, that's why we give them to you in order
as you work up to it.
I feel like I get tunnel visions when noting the rising falling,
like experiencing little sensations. Sometimes the noticing is in the back of my mind that I can
feel clearly, which is mindful, which is mindful, which is mindful of all. Okay.
Mindfulness when you note either of those experiences, what you're seeing is impermanence,
you can't predict what's going to be this way or it's going to be that way. Sometimes it's one
way. Sometimes it's another mindfulness is when you note that if you feel like you have tunnel
vision, you would not feeling feeling. If you hear and feel things, you would not hearing
or feeling.
Basically, if you notice anything, then it's already something you should take as an object.
When you're noting and you don't notice how you're noting, then you don't have to note that
that way, the nature of it, because you're just doing it.
Should we involve invasions or should invasions be noted as wanting something to enter
they can, or something else?
You should note it as it is. If it strikes you as being in patience, you can just note in
patience. But yeah, impatience is often aversion or desire. What we call impatience is
often one or the other, and noting, if I have a feeling of relaxation that I enjoy,
is it better to note the relaxed feeling of the body, liking of the mind or alternate between
the two different notes? Whichever is clear is just pick one. It doesn't really matter.
Whichever you can see clearly, note that one. Once it's gone, then just go back to the rising
falling or whatever.
Yes, but you shouldn't intellectualize it. When you watch the stomach, you say rise, saying that's
from the beginning of the movement to the end of the movement, and you shouldn't force yourself
to try and artificially see the beginning of the end. It's your intention is to start saying it at
the beginning and to stop saying it at the end, and that allows you to observe the whole thing.
But it's impermanent. Your mind is sometimes going to focus on the ending, sometimes going to focus
on the beginning, sometimes see the beginning ending, sometimes see beginning and ending within
the breath. So each little piece of movement, sometimes is a beginning and an end. It's unpredictable.
If you don't try to force it one way or the other, just take it as it comes and come to be flexible.
It's the practice designed to correct itself. Maybe some wrong-loading or
include becoming more refined and precise over time. Again, to be perfectionist, so I worry a lot
about trying now to just do. Yes, the practice correct itself or more appropriately wisdom
corrects your behavior, and the practice is a part of that. So the practice is something very
simple, but our minds are complex and overcomplicated. So our minds add a lot to what we call
the practice, and that changes as we gain more wisdom. You can only hurt yourself so much until
you realize that I'm hurting myself, and hopefully if you're very mindful, it takes less time
because you're quicker to notice and less stubborn about hurting yourself over and over again,
because mindfulness is not painful. Mindfulness is not a cause of pain or suffering.
If you worry a lot, don't forget to note that as well. Say worry, worry, and you're not
up to try to just do it. That's a misunderstanding. It's a misunderstanding that's quite common
where people think I'm forcing it. I've got to stop myself, but stopping yourself is also forcing.
If you want to learn how not to force, you have to learn to just observe even observing the forcing.
So this is a part of letting yourself cause you suffering until you realize that you're causing
yourself suffering. When you realize how stressful it is to try and force, it's just going to naturally
correct itself. When I develop a graduate on someone due to a perceived form, it tends to
linger for a long time in my mind. What should I focus on during meditation to eliminate the scratch
for good? Wanting to eliminate the grudge is probably part of the problem. It's not how we look at it.
sorts of questions are understandable, but misguided. Just focus on the experience as it happens.
Rather than thinking about it as a grudge, focus on the experiences, grud just don't exist.
It's just your interpretation over time when you consider the past or the future.
Nothing lingers for a long time. Things come and they go, they arise and they see
very quickly actually, but it's hard to see that. That's what you want to see. You want to see
moments of experience when you're angry at someone that's just a moment, say angry. That's it.
Sometimes I find myself not in frantically. Should I not notice anything and then slow down my
mental motive? Yeah, I don't know about noticing, but you're on the right track. I mean,
I wouldn't slow it down. I would just stop noting frantically. There's some reason why you're
doing that. Maybe you're ambitious about it, craving something, and just noting how fast it's
going will slow it down. You can say something like knowing, noticing might be okay, too, because
you've noticed that, to say noticing, it's not wrong.
I'm coming back to some of the questions from before. Does meditation help you love yourself
or have no self at all? Well, the self is just a concept. It's not what it's real. It's not what's
part of experience. So I guess neither any thought about the self is just an illusion.
Just try and note experiences. That's what meditation helps. It helps you see clearly.
Understand experience for what it is.
But there was one about, one about this one about PTSD.
Yeah, that's next. I've been diagnosed with PTSD with association. I feel like the world is a
simulation or a movie. Sometimes it seems like people around me repeat what I was just thinking,
kind of like reading my mind, but I don't, but I know they're not listening to my thoughts.
Can you explain this?
Well, the mind does strange things. It is possible to
read people's mind. It's probably not likely that everyone is.
It's common. I mean, as you're experiencing, this is a thing where we become,
you might say paranoid, but interpretative, over interpretative, where we interpret everything,
where we make connections. That's what it literally is at the basic, most basic level.
We make connections with things. No meditation and mindfulness.
That's one of the things it changes, rather than making connections with things, because that's
a big part of where reactions come from, rather than making connections. We try to see things just
as they are. So if you think something, you say thinking. If you hear someone say something,
it's hearing, and you don't make the connection between what you were thinking and what they were,
what they were saying. If you do make that connection, you don't make any connection with it.
So that connection, and you're mind, hey, they just said what I was thinking. Note that,
note that thought. If there's any emotion, because you've now connected it to something,
to what is bad, to harm that might come to your fear, that sort of thing. If there's fear
that arises, if you're angry about it, if you're worried about it, if you're confused about it,
all of that, don't make a connection there as well. The mind is always making connections,
and we try to change that so that things just are what they are.
That, I think, would help immensely with PTSD in its many forms.
If you're interested, I'd recommend maybe trying to do an at-home meditation course that might help.
I get paranoid when men are around me, kind of weird, but should I know paranoid?
It's not very weird, men are scary. When you're afraid, say afraid, afraid,
you do have to take precautions. It's not to say that when you're afraid of something,
there's not a reason for being cautious, but fear doesn't help. Fear paralyzes you, fear excites
you, fear keeps you from being level-headed and focused at peace, to try and know the afraid,
afraid is better than paranoid. Although paranoid can be worried as well, and worry might be a
little more clear sometimes. Try and know them both. And paranoid has to do with thoughts as well.
Again, this connection, making connections, is that person following me?
Why is that person looking at me? Thinking, thinking, it doesn't mean you're discarding them.
See, meditating isn't discarding things. Meditating is seeing things clearly,
so it can actually help you to be more alert, to more aware of your service around things,
more aware of people's intentions, more clearly aware of your circumstances that allows you to be
safer. I'm doing sitting meditation. I start breathing fast, and then I see instead of rising
falling, knowing like you said, I feel more uncomfortable because it lasts the whole meditation,
what to do. Do you feel uncomfortable? Just note that.
But if you're breathing fast, you can still note rising, falling. You can also note rise, fall.
So follow up to just do it. How to note when observing, where forcing the practice used to
be, I probably wouldn't because you're not actually forcing. That's how it appears to you,
but there's just a feeling, a feeling of tension. It's usually tension because you feel like,
oh, this is me forcing it, and that's why it's so tense, but it's actually just tension in the body.
When you watch the stomach, it's a common thing for it to become very tense, and you think
I'm forcing it, it's just tension, just note tense, tense. And then if there is in the mind,
the desire for things to be a certain way, note that as well, wanting or craving. If you're worried
about it, just stress about it, note that as well.
My legs fall asleep completely. My legs fall completely asleep after about 30 minutes of sitting
meditation. I'm still working my sitting posture any apply.
Yeah, there's nothing wrong with legs falling asleep. And if you experience, if you feel something,
say feeling, but if you don't feel anything, just continue on with your practice, that'll work
itself out. It's not a problem. It is a problem. You have to be careful when you stand up. Don't
stand up too quickly. You can apparently break a leg. If you just try to stand on a leg that's
asleep, it apparently can be dangerous. So wait for them to wake up again before you try to walk.
I've never actually seen heard of someone specifically, but apparently it can happen.
Okay, dreadful thing to think. Is it ever proper to note, waiting, waiting, or is that
so conceptual? No, that's fine.
Easier to notice when an object arises by challenging when it ceases to be. It's usually
an object takes its place. Using an object is harder than me to notice. It lies.
So again, it's not something we should try to do. You shouldn't try to note the ending.
Session is by its very definition, not very noticeable. It's gone. What are you going to notice?
Just try and be mindful of things. Be mindful of experiences. You'll see the beginning,
you'll see the end. You'll see it without even helping, without even trying
to see the beginning or the end. Just try and note the experience.
When you first notice it, note that, note it then. See the beginning and end? That's the result.
That's what you're going to see as a result. That's not your practice. There's no trying.
So it can't be hard or easy. It just comes when it comes. If it doesn't come, don't look
for it. Otherwise it becomes intellectual, this idea of some, like there should be a signpost.
Hello, I'm ceasing now. It's very obvious. It's not something like that.
In regards to the question about legs, all in its teeth, is it best to not feel like
what would be more specific? For example, tingling or buzzing?
It doesn't really matter. Feelings are enough, but tingling is fine.
I'm going to do a follow-up question from the person who asked this question earlier,
and I'm driving biking or roller skating. I know that the consequences box time because of this.
But the further advice against these activities because of the five precepts.
I mean, I'm not the Buddha, so I can't tell you, but I think you might advise against them
because of that. These are kind of things that are kind of violent and it's not very nice to
do things that you know are going to kill lots of beings, but it's not breaking the five precepts.
Because there's no intention. You aren't intentionally trying to kill a specific being.
You just know that if you do this, beings are likely going to die. You don't know that for sure,
but it's likely.
And it's not breaking the five precepts. It's just something that's something to be mindful of,
and there's a lot of things that don't break the five precepts that are
best in an ideal situation, an ideal world, better to do without not eating meat, for example.
Go ahead, eat meat, but it's probably a better world if we stopped eating meat.
Not like that world is likely to ever come about for a long time.
Is the at-home meditation course free?
Everything we do is free. We never charge or even ask for a donation for anything.
I like to be able to mention that and I'm very proud, I guess. I'm very appreciative
of our community, our volunteers, our board of directors, and all of our supporters
who allow us to do this completely for free. We have people who donate and give us support.
And of course you're welcome to do that, but it has no bearing on what you've gained
as in terms of an obligation to return some remuneration. So there's no suggested donation
for anything. There's no bidonation for anything. Everything is and always will be free.
If at some point we are unable to continue offering these things, then we'll just stop.
If we should never have it, not enough resources, we'll just stop.
So if you want it to keep going, you can always support. And if you want it to increase,
then you're always welcome to get involved and learn about ways that you can support us to
expand our activities, like maybe someday building a monastery or something like that.
Everything's free. If it weren't free, there would be some kind of attachment we would have.
That's how I understand it. Anytime you start charging, there's a sense. The only reason
could be is because you want it to happen, because you have some attachment to it.
We do what we do because people are asking us to do it, not because we want it to continue.
So if there's ever as long as there's enough resources, we'll keep doing it.
And there's people who are asking us to do it. And it's a part of our own path.
Some people decide to become Buddhas, and so they help so many people.
Other people don't, and decide to help very few people. It's maybe not even so much a decision as
it is a difference of character. And so you can't expect people to do more or less than they do
either. But I do these at home meditation courses, not entirely sure why, but that's what I do.
One of the things I do right now, so as long as I do it, it will be free. If I decide to stop doing it,
then it won't happen.
I wonder if there was another question about that. The at home meditation courses still confusing
for me. It's like you won't forget in the time difference. Could I, you know, need to set
this specific time? I just wanted to add a note that there is the, it has now the page has been
changed where you can see both your time and one day's time, so that should not be confusing anymore.
Yeah, no, I don't, I can't honor any custom requests like that. You have to pick a slot.
The onus is on you to do a little bit of the work. It's the thing about things being free is
you have to do some of the work yourself. So it being confusing is a challenge.
Something you'll have to overcome. Forgetfulness, yeah, you'll have to work on that. I forget
sometimes. I've forgotten appointment. Forget meetings. Forgetting the mind is unpredictable sometimes.
It's a challenge, but no, there's no harm if you do forget an appointment, just make another
booking. It's not the end of the world. We have no policy of of blacklisting people for
missed meetings. Unless we ever were to find out there was something malicious like someone was
blocking, blocking slots without any intention of of keeping them of course, but if you forget
or something comes up and you can't make an appointment, I just delete it, but you're welcome too.
It's, there's no prejudice. So you're welcome to book again. I delete it because I'm not sure
what happened. It may be that you've decided not to do the course. It may be that you finish the course.
I don't know. I just delete it without prejudice. You're welcome to book again.
When pain arises with a virgin in the UK office, which is the sensation that is worse,
most care be noted, or should the version be selected as it is one of the five infants.
Whichever is clearest.
You see, the noting helps you to see clearly and sorted all out. It will become untangled.
Eventually, you'll be able to know everything individually.
But you don't have to know, you don't have to know every single thing because it's very quick.
Just try and note something, whichever is clearest.
There's a question about you that they've asked many times. I don't know if you want to talk to me.
I don't know. I don't even know longer where glasses is. It's because it's sticky.
I don't know what cities are. I no longer use glasses because some kind of people paid for me to
have laser eye surgery. Not a question, I guess. Anyway, we're done. It's time-up.
So from now on, last period, only questions that are important. Only top-tier questions,
okay? If there are no top-tier questions, we stop. Go ahead.
I don't think there's any more specific education-related questions we've already gotten.
Go on to all of these. Is this question I'm not sure if you'd like to answer?
Okay, last one.
A realized that I have a strong sense of attachment to my sense of self. A sense of identity is
there any advice you could give me to let go of this attachment.
I'm not going to answer that. It's not that important. I'd recommend to read the booklet.
Sorry, I don't mean to dismiss your question out of hand, but
it's sort of a theoretical problem that you have. What's real is going to be experiences,
maybe there's conceit, maybe there's views, but all of that comes clear through your practice.
Read the booklet. If you're interested, take a net home meditation course.
That'll all clear itself up.
Practice is not letting go. Practice is seeing clearly. Letting go comes from practice, from seeing clearly.
Okay, we're done.
Sad. You can talk again. Thank you all for coming.
A good group. Good session.
We have had a good one hour today.
That was an hour where none of us wasted our time, where we can say for that hour we were making
use, good use of our birth as human beings. We were not wasting our time in Samsara. We were not
wandering anymore. We had purpose for that hour. We were inclined towards the good. So sad.
Thank you for your time. Thank you. Thank you for your time.
