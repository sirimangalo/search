1
00:00:00,000 --> 00:00:16,600
Good evening everyone, broadcasting live April 7th.

2
00:00:16,600 --> 00:00:27,400
Today's quote, it's actually got quite a bit in it, you know, it's a simple message.

3
00:00:27,400 --> 00:00:38,600
This is from the Intivutaka.

4
00:00:38,600 --> 00:00:51,400
The Buddha says, even should one, Sankar, Tikkarnay, J.P.B.Kare, even if one should

5
00:00:51,400 --> 00:00:58,400
the corner of my robe.

6
00:00:58,400 --> 00:01:04,400
Take hold of it, even if one having taken hold of the corner of my robe.

7
00:01:04,400 --> 00:01:20,400
It should follow after me, it should follow behind my back, one after the other.

8
00:01:20,400 --> 00:01:38,400
One day, Paday, Paday, Padam, Paday, Padam, Kippantum, stepping in my footsteps.

9
00:01:38,400 --> 00:01:42,400
Putting down his feet in my footsteps.

10
00:01:42,400 --> 00:01:49,400
So literally getting about as close as he could to the Buddha, following him everywhere.

11
00:01:49,400 --> 00:02:07,400
So Jajodhi, and if he is Anvijna, Anvijana, with craving, with great desires, follow desire.

12
00:02:07,400 --> 00:02:25,400
Thi, Thi, Bhasa, Raghu, Kameh Suhthi, Bhasa, Raghu, with sharp, extreme lust for sensuality.

13
00:02:25,400 --> 00:02:37,400
Praya, Pan-najito, with a mind full of ill will, a mind of ill will, angry at other people,

14
00:02:37,400 --> 00:02:44,400
hating other people with only bitter thoughts for others.

15
00:02:44,400 --> 00:02:56,400
Padutam, Manasanko, corrupted with corrupted mental formations of thoughts that are corrupted,

16
00:02:56,400 --> 00:03:09,400
thinking to harm others and to manipulate others, to oppress and abuse others.

17
00:03:09,400 --> 00:03:23,400
Muktatsati, with confused mindfulness, confused recognition, the reaction, reacting to things in a confused way,

18
00:03:23,400 --> 00:03:25,400
without wisdom, without clarity.

19
00:03:25,400 --> 00:03:36,400
And things arise, reacting based on, like being in the dark, based on habit, based on ignorance.

20
00:03:36,400 --> 00:03:40,400
So it would give rise to greed and anger.

21
00:03:40,400 --> 00:03:50,400
Asapadhan, or without full comprehension, without some full,

22
00:03:50,400 --> 00:04:14,400
or some is, like, comprehensive, but is full, jazz, knowledge.

23
00:04:14,400 --> 00:04:25,400
Oops, their mind is not impartial, objective, able to see things as they are.

24
00:04:25,400 --> 00:04:33,400
As I said, there's a lot in here because it looks like a simple quote, but each of these words has very important meaning to them.

25
00:04:33,400 --> 00:04:53,400
We bunted Jitou, is that me? It's just more desirable. We bunted the wandering mind, confused mind, we bunted the room, the stray.

26
00:04:53,400 --> 00:05:05,400
We bunted the wandering mind, the mind that doesn't stay focused, the mind that goes hither and thither, thinking about this and that.

27
00:05:05,400 --> 00:05:28,400
So Bach, a team real, with unguarded, untrained, sort of run of the meal, maybe it doesn't mean that, but Bach, a team can be ordinary, but here it means unguarded or untrained senses.

28
00:05:28,400 --> 00:05:39,400
So the senses are our doors, by which we experience things, but the problem is we don't just experience things, we react to them.

29
00:05:39,400 --> 00:05:54,400
So this is untrained, we're based on habit and memory and experience, we react to a little thing in a very specific way that often is a cause for great suffering for us.

30
00:05:54,400 --> 00:06:01,400
Then others will cause for bad karma.

31
00:06:01,400 --> 00:06:13,400
So all of these things, so this, imagine this person holding on to the Buddha's coattails, so to speak,

32
00:06:13,400 --> 00:06:28,400
walking in his footsteps, when he's got all these things, he's full of greed, as many desires, as craving or lust for sensuality, as ill will,

33
00:06:28,400 --> 00:06:54,400
as a corrupt and thoughts, mental activity, is confused, is the muddled awareness, without comprehension and understanding, without focus, or somehow, without this kind of balanced, calm mind.

34
00:06:54,400 --> 00:07:01,400
We ban that people who are wandering in mind and with untrained faculties.

35
00:07:01,400 --> 00:07:05,400
So what about him? What about her?

36
00:07:05,400 --> 00:07:22,400
Atakoso Arakawa, such a person, well, such a person, Arakawa Maham is very far from me, Maham to me.

37
00:07:22,400 --> 00:07:26,400
He's a far away from me.

38
00:07:26,400 --> 00:07:30,400
He's as low, very far from me.

39
00:07:30,400 --> 00:07:37,400
Ahanchatasa, and me from him, for her.

40
00:07:37,400 --> 00:07:41,400
Dangisahi to what's the cause of that?

41
00:07:41,400 --> 00:07:52,400
Dhamani is so big away, big kunapasati. For the dhamma, because that bigu that monk doesn't say,

42
00:07:52,400 --> 00:08:00,400
I don't say the dhamma. Dhamma and apasanto namma and apasati.

43
00:08:00,400 --> 00:08:11,400
Not seeing the dhamma, they don't see me.

44
00:08:11,400 --> 00:08:21,400
So the meaning here is that, in two ways, having all those things is a sign, of course, of not seeing the dhamma.

45
00:08:21,400 --> 00:08:29,400
It's sort of the definition of not seeing the dhamma, because seeing the dhamma is to see things as they are.

46
00:08:29,400 --> 00:08:42,400
And to live life impartially, objectively wisely, and all of these things are not that.

47
00:08:42,400 --> 00:08:49,400
But also, you may have explained it as that these things stop us from seeing truth.

48
00:08:49,400 --> 00:08:56,400
You can't see the dhamma if you have all of these things.

49
00:08:56,400 --> 00:08:59,400
Maybe you'll live with the Buddha.

50
00:08:59,400 --> 00:09:09,400
That's why I had Jantong said my teacher said, because he's very famous in Thailand or the fairly famous in Thailand.

51
00:09:09,400 --> 00:09:24,400
And I guess now he's very, very famous, but I'm coming to visit him thousands of people on his birthday, just pouring in.

52
00:09:24,400 --> 00:09:45,400
And so he said, well, going to see noble people, going to see, going to live with them, or even wait on them, or care for them, or attend to them as their student, and as their attendant.

53
00:09:45,400 --> 00:10:00,400
It's not the same as becoming a noble one yourself. It's a simple thing to say, but it's one of those quotes that I've got published in the past year.

54
00:10:00,400 --> 00:10:10,400
So this is an important quote and an important sort of teaching in the context of religion, because a lot of people end up doing that.

55
00:10:10,400 --> 00:10:15,400
They think that by associating with good people, that's nothing.

56
00:10:15,400 --> 00:10:20,400
The Buddha did say that associating with good people is the whole of the whole of life.

57
00:10:20,400 --> 00:10:25,400
But the thing about it is that wise people tend to say things like this.

58
00:10:25,400 --> 00:10:32,400
They say, you know, look, it's not about following me around, it's about changing yourself.

59
00:10:32,400 --> 00:10:42,400
Of course, the great thing about associating with wise people is that they teach these things. They push you to become a better person.

60
00:10:42,400 --> 00:10:56,400
They don't let you walk around, holding on to their robes.

61
00:10:56,400 --> 00:11:15,400
And then there's the idea about being far and being near to the Buddha, which kind of has greater implications in terms of the idea of space,

62
00:11:15,400 --> 00:11:41,400
and the concept of reality. What is real? We think of space as being important, location being important, when in fact it's really just a part of the impression that we have of things.

63
00:11:41,400 --> 00:11:51,400
Someone on Facebook today, one an old friend from high school. He posted, he said, what was it?

64
00:11:51,400 --> 00:12:03,400
There's nothing wrong with my brain. The anxiety I'm experiencing is just a reflection of all the things that are messed up in the world.

65
00:12:03,400 --> 00:12:10,400
And I replied and I said, the problem is that the world is all in your brain, because it's really true.

66
00:12:10,400 --> 00:12:21,400
I mean, it's not even in the mind. The mind gets everything from the brain, which projects everything and creates things.

67
00:12:21,400 --> 00:12:26,400
They did a study that said, when you look at something, you don't look at the whole object.

68
00:12:26,400 --> 00:12:33,400
So we think in the room around us there's all these objects that we're seeing, but in fact, we only see very small bits of it.

69
00:12:33,400 --> 00:12:47,400
We only see enough to be able to create the picture in our minds. And they did studies, they did tests on the people that they were able to trick them into not being able to see something on the pictures.

70
00:12:47,400 --> 00:13:01,400
They changed something, but you couldn't see it. Because you weren't actually seeing the picture. You weren't actually seeing the light was only the impetus.

71
00:13:01,400 --> 00:13:13,400
Or the spark that then the instigation that leads the mind to create image.

72
00:13:13,400 --> 00:13:21,400
Anyway, the point of how that relates is the world is really all in our heads.

73
00:13:21,400 --> 00:13:25,400
All in our minds is to have to say.

74
00:13:25,400 --> 00:13:35,400
And so this is how, I mean, this kind of a theory, right? Some people would challenge it and say, that's not true.

75
00:13:35,400 --> 00:13:39,400
The world does exist out there, whether we experience it or not.

76
00:13:39,400 --> 00:13:49,400
But under this theory, the idea of some mind being primary.

77
00:13:49,400 --> 00:13:56,400
And this is how the whole idea of rebirth and karma works.

78
00:13:56,400 --> 00:14:01,400
Because we create these experiences, we create these existences.

79
00:14:01,400 --> 00:14:16,400
And so it's how we understand our relationships with others, why we're brought into contact with the people around us,

80
00:14:16,400 --> 00:14:24,400
people who we meet in life.

81
00:14:24,400 --> 00:14:33,400
Because we see with them, we see them, we see in the same way as them.

82
00:14:33,400 --> 00:14:39,400
And so to be close to the Buddha, the only way to be close to the Buddha is to see in the way he sees.

83
00:14:39,400 --> 00:14:42,400
To see things the way he sees them.

84
00:14:42,400 --> 00:14:48,400
Because that the way you see things, the way you look at things, the way you react to things.

85
00:14:48,400 --> 00:14:51,400
That's the whole of who you are. That's what makes you who you are.

86
00:14:51,400 --> 00:14:57,400
It's what determines your life, your future.

87
00:14:57,400 --> 00:15:04,400
So really that's all we're doing in meditation, is learning to see things the way the Buddha saw things.

88
00:15:04,400 --> 00:15:15,400
Which is as they are, why we call them the Buddha is because he awoke to things as they are, or he came to understand things as they are.

89
00:15:15,400 --> 00:15:20,400
We claim that we don't see things as they are, we react to them.

90
00:15:20,400 --> 00:15:24,400
I mean we see things as they are of course, but we do much more than that.

91
00:15:24,400 --> 00:15:33,400
As soon as you see something as it is, you get lost in how you want it to be, or how you want it not to be.

92
00:15:33,400 --> 00:15:43,400
We can think of it, what it makes you think of, cut in the past, future.

93
00:15:43,400 --> 00:15:46,400
I'm not seeing things as they are.

94
00:15:46,400 --> 00:15:51,400
Because we don't see that, we can see the Buddha.

95
00:15:51,400 --> 00:15:54,400
Very far from the Buddha, whether we're right next to him, and then he says,

96
00:15:54,400 --> 00:16:17,400
I suppose there was a monk living a hundred yogiṇa, just like a league, hundred yogiṇas, like 16 kilometers or something, I don't know.

97
00:16:17,400 --> 00:16:24,400
A hundred yogiṇa, a hundred far distances.

98
00:16:24,400 --> 00:16:28,400
And then he is not any of those things.

99
00:16:28,400 --> 00:16:49,400
He is not with many desires.

100
00:16:49,400 --> 00:16:52,400
Not with many desires.

101
00:16:52,400 --> 00:17:02,400
So Nati Bhasara, not with sharp lust, with strong lust in regards to sensuality.

102
00:17:02,400 --> 00:17:15,400
Ambhyapana, abhyaya, panachito, panachito, bhyaya, panachito, without ill will.

103
00:17:15,400 --> 00:17:22,400
Abhadoṇa, samkapo, is uncorrupted mental formations.

104
00:17:22,400 --> 00:17:35,400
Upati tasati, who is established mindfulness, with a mind that is able to grasp things.

105
00:17:35,400 --> 00:17:40,400
The way of grasping, we say in English, we use the word grasp in two ways.

106
00:17:40,400 --> 00:17:46,400
Because if you grasp something, it means you understand it, not like grasping something, like grasping

107
00:17:46,400 --> 00:17:50,400
onto something, or grasping in general.

108
00:17:50,400 --> 00:17:53,400
When you grasp something, it means you understand it.

109
00:17:53,400 --> 00:17:55,400
And that's what we do in the meditation.

110
00:17:55,400 --> 00:18:00,400
Every moment trying to grasp it, just for a moment, just grasp that seeing.

111
00:18:00,400 --> 00:18:03,400
And you grasp it, and you say, ah, yes, that's seeing.

112
00:18:03,400 --> 00:18:08,400
Instead of saying, that's good, that's bad, that's me, that's mine.

113
00:18:08,400 --> 00:18:12,400
So on.

114
00:18:12,400 --> 00:18:22,400
Sampajana, fully comprehending or full awareness, full knowledge, full and right knowledge.

115
00:18:22,400 --> 00:18:27,400
Sammajito, well balanced or well focused.

116
00:18:27,400 --> 00:18:38,400
A kangajito, with one point, with a one pointed mind.

117
00:18:38,400 --> 00:18:49,400
Samu tindriyos, trained and restrained senses.

118
00:18:49,400 --> 00:18:58,400
A takoso santikeva, my person is as though, in my presence, a hunchatasa.

119
00:18:58,400 --> 00:19:01,400
And me and his tang gives a heath, which is the cause.

120
00:19:01,400 --> 00:19:06,400
The manh is so big away, big capacity.

121
00:19:06,400 --> 00:19:12,400
For that manh sees the manh, the manh, the manh, the manh, the manh, the manh, the manh,

122
00:19:12,400 --> 00:19:18,400
seeing the manh, he sees me.

123
00:19:18,400 --> 00:19:26,400
There you go.

124
00:19:26,400 --> 00:19:34,400
That's the number for tonight.

125
00:19:34,400 --> 00:19:37,400
Does anyone have any questions?

126
00:19:37,400 --> 00:19:40,400
I think I won't make you come on and hang out.

127
00:19:40,400 --> 00:19:43,400
I'll just go back to text questions.

128
00:19:43,400 --> 00:19:46,400
You know what I was thinking?

129
00:19:46,400 --> 00:19:52,400
What we could do is instead of like doing questions and videos about their answers,

130
00:19:52,400 --> 00:19:58,400
is we could set up some kind of page where people could vote on topics.

131
00:19:58,400 --> 00:20:01,400
And just like one word topics.

132
00:20:01,400 --> 00:20:06,400
And do a video on this, and people could vote on it.

133
00:20:06,400 --> 00:20:10,400
But I can do a video on certain topics.

134
00:20:10,400 --> 00:20:14,400
I think that would work well.

135
00:20:14,400 --> 00:20:25,400
Someone has to make such a page.

136
00:20:25,400 --> 00:20:35,400
Does any questions or take text questions tonight?

137
00:20:35,400 --> 00:20:45,400
I have to meditation.serimongalo.org, those of you over at YouTube.

138
00:20:45,400 --> 00:20:48,400
You have to find our meditation page.

139
00:20:48,400 --> 00:20:50,400
And they have to be live, unfortunately.

140
00:20:50,400 --> 00:20:58,400
I'm not going to go digging back through the past chat and find dead comments.

141
00:20:58,400 --> 00:21:27,400
I don't know.

142
00:21:27,400 --> 00:21:28,400
New York.

143
00:21:28,400 --> 00:21:33,400
Well, I posted some events on Facebook.

144
00:21:33,400 --> 00:21:39,400
So, I shared some events that were shared with me.

145
00:21:39,400 --> 00:21:45,400
So, I think that's what you got to do is go to those events.

146
00:21:45,400 --> 00:21:48,400
Let me see.

147
00:21:48,400 --> 00:21:58,400
There's a April 16th event at Rockaway Beach.

148
00:21:58,400 --> 00:22:02,400
90-16 Rockaway Beach Boulevard.

149
00:22:02,400 --> 00:22:07,400
Wait, 1-1-6-9-3 Rockaway Beach Queens.

150
00:22:07,400 --> 00:22:10,400
Is that 3 p.m. on April 16th?

151
00:22:10,400 --> 00:22:11,400
I don't know.

152
00:22:11,400 --> 00:22:15,400
I mean, I'm just getting these as you guys are.

153
00:22:15,400 --> 00:22:17,400
I didn't plan any of this.

154
00:22:17,400 --> 00:22:21,400
And then I'm doing a neon meditation.

155
00:22:21,400 --> 00:22:27,400
7 p.m. at light bright neon 2-32-3rd street.

156
00:22:27,400 --> 00:22:30,400
Numbers seen 102 Brooklyn.

157
00:22:30,400 --> 00:22:38,400
That's April 19th, 7 p.m.

158
00:22:38,400 --> 00:22:45,400
I think like 16th and 17th and 18th, there's other stuff that I'm not actually.

159
00:22:45,400 --> 00:22:47,400
I'm not actually the teacher all these days.

160
00:22:47,400 --> 00:22:48,400
I don't know.

161
00:22:48,400 --> 00:22:51,400
Because these are the only ones I got.

162
00:22:51,400 --> 00:22:53,400
16th, 19th and 20th.

163
00:22:53,400 --> 00:22:54,400
I don't know.

164
00:22:54,400 --> 00:22:59,400
But the 21st, that's Dharma Punks.

165
00:22:59,400 --> 00:23:00,400
I think.

166
00:23:00,400 --> 00:23:02,400
No?

167
00:23:02,400 --> 00:23:07,400
Maybe.

168
00:23:07,400 --> 00:23:10,400
Yeah, Dharma Punks in Manhattan.

169
00:23:10,400 --> 00:23:11,400
Don't know.

170
00:23:11,400 --> 00:23:16,400
You have to find the Dharma Punks at that place.

171
00:23:16,400 --> 00:23:22,400
A little yoga, Dharma and wellness, that's a place I think.

172
00:23:22,400 --> 00:23:32,400
And then April 22nd, 4th green Brooklyn.

173
00:23:32,400 --> 00:23:43,400
The lucky lotus, yoga and vegan cafe.

174
00:23:43,400 --> 00:23:45,400
These are all on my page though.

175
00:23:45,400 --> 00:24:03,400
Let's see if we can go look at them.

176
00:24:03,400 --> 00:24:12,400
How is the mind so powerful and yet so I think you're trying to say naive?

177
00:24:12,400 --> 00:24:15,400
naive is not spelled that way, but that's okay.

178
00:24:15,400 --> 00:24:20,400
I think you're saying naive.

179
00:24:20,400 --> 00:24:28,400
Powerful doesn't mean that it's in control of the power, right?

180
00:24:28,400 --> 00:24:33,400
The mind is not in control of its own power and that's really the problem.

181
00:24:33,400 --> 00:24:39,400
Empowerful doesn't mean good.

182
00:24:39,400 --> 00:24:44,400
So the mind ends up, I mean the mind can be very wise.

183
00:24:44,400 --> 00:24:52,400
You know, the mind can be very smart, clever and so on.

184
00:24:52,400 --> 00:24:54,400
But it can also be very naive.

185
00:24:54,400 --> 00:25:02,400
So it twists itself up and ends up creating its own creating problems for itself.

186
00:25:02,400 --> 00:25:10,400
Because it has no direction.

187
00:25:10,400 --> 00:25:18,400
The mind lacks is training.

188
00:25:18,400 --> 00:25:23,400
Training is a very specific thing.

189
00:25:23,400 --> 00:25:31,400
See, the mind that is just going randomly is very unlikely to even think about training itself.

190
00:25:31,400 --> 00:25:37,400
So as a result, you see that most of us don't think of training ourselves.

191
00:25:37,400 --> 00:25:39,400
I really didn't.

192
00:25:39,400 --> 00:25:45,400
I was looking, I went to Asia looking for wisdom looking for insight, but didn't think I'd have to work for it.

193
00:25:45,400 --> 00:25:46,400
Most people don't.

194
00:25:46,400 --> 00:25:49,400
Most people who come to meditate don't even think of it as work.

195
00:25:49,400 --> 00:25:55,400
They think, yeah, meditation, that'll be like a pill I can swallow and I'll just be.

196
00:25:55,400 --> 00:25:59,400
I'll sit down on my mat and poof on the hat there, right?

197
00:25:59,400 --> 00:26:03,400
And they're like, oh crap, this is tough.

198
00:26:03,400 --> 00:26:07,400
A lot of people don't like the meditation that I teach when I first teach it.

199
00:26:07,400 --> 00:26:09,400
You can't teach this to everyone.

200
00:26:09,400 --> 00:26:11,400
Not everyone's going to appreciate it.

201
00:26:11,400 --> 00:26:13,400
I'm sure that'll be the case in New York.

202
00:26:13,400 --> 00:26:17,400
There will be many people who are not very happy about what I'm teaching.

203
00:26:17,400 --> 00:26:22,400
I mean, they won't say it, but it won't stick with most people.

204
00:26:22,400 --> 00:26:24,400
So people want comfort.

205
00:26:24,400 --> 00:26:27,400
They want an escape.

206
00:26:27,400 --> 00:26:33,400
It's hard to get people to grow up and learn to look, learn to look at their problems.

207
00:26:33,400 --> 00:26:40,400
That's maybe harsh language, but I don't mean I try and don't take that too hard.

208
00:26:40,400 --> 00:26:45,400
It's not too critical, but it really is the case.

209
00:26:45,400 --> 00:26:48,400
I'm not condescending or looking down on such people.

210
00:26:48,400 --> 00:26:50,400
I was the same.

211
00:26:50,400 --> 00:26:54,400
We're all in this position where we just have to grow up.

212
00:26:54,400 --> 00:27:02,400
We have to start really taking the bull by the horns and stop running away and stop fooling ourselves.

213
00:27:02,400 --> 00:27:08,400
We're going to be able to just ignore the problems and we're going to escape our problems.

214
00:27:08,400 --> 00:27:14,400
We have to really go facing your problems as the hardest thing.

215
00:27:14,400 --> 00:27:23,400
Anyway, kind of went off on tangent there, but the mind is just complex.

216
00:27:23,400 --> 00:27:31,400
I suppose there's a result that kind of does do itself.

217
00:27:31,400 --> 00:27:41,400
It's like the ocean is powerful, I suppose you could say, but most of the time the ocean doesn't do anything.

218
00:27:41,400 --> 00:27:44,400
You say the ocean is powerful, what the heck does that mean?

219
00:27:44,400 --> 00:27:46,400
You go to the ocean.

220
00:27:46,400 --> 00:27:49,400
Ooh, it's not doing anything powerful, right?

221
00:27:49,400 --> 00:27:59,400
But then you have a tsunami, right? Or you have a hurricane, and then you see the power of the ocean.

222
00:27:59,400 --> 00:28:04,400
When the ocean is in its natural state, the mind of its natural state is diffuse.

223
00:28:04,400 --> 00:28:11,400
It's full of contradictions that use up all its energy.

224
00:28:11,400 --> 00:28:17,400
So if you train your mind, you have all this energy that's very powerful,

225
00:28:17,400 --> 00:28:24,400
then you have to concentrate it.

226
00:28:24,400 --> 00:28:45,400
So I'm not sure if you're an English native speaker, but the words grow up in English.

227
00:28:45,400 --> 00:28:48,400
It's an idiom that doesn't mean get older.

228
00:28:48,400 --> 00:28:55,400
It means stop being chosen now, stop being immature.

229
00:28:55,400 --> 00:28:58,400
There's nothing to do with age.

230
00:28:58,400 --> 00:29:14,400
Grow up means stop being immature.

231
00:29:14,400 --> 00:29:21,400
Anyway, if that's all, I'll say goodnight, I've still got stuff.

232
00:29:21,400 --> 00:29:25,400
This Sunday we're having a storytelling,

233
00:29:25,400 --> 00:29:27,400
if some of you know about the storytelling thing,

234
00:29:27,400 --> 00:29:30,400
we're having on Sunday at the spectator auditorium,

235
00:29:30,400 --> 00:29:33,400
if any of you are local, you're welcome to come on over.

236
00:29:33,400 --> 00:29:37,400
I'll be telling a story from the Jataka about a bird.

237
00:29:37,400 --> 00:29:40,400
It's the one I told in second life recently, I think.

238
00:29:40,400 --> 00:29:46,400
It's about a bird that stays by its tree,

239
00:29:46,400 --> 00:29:52,400
stays in its tree even after the tree kind of dries up, dies,

240
00:29:52,400 --> 00:29:57,400
and it stays there out of gratitude to the tree.

241
00:29:57,400 --> 00:30:04,400
The story, the event is about relating religion to the environment,

242
00:30:04,400 --> 00:30:07,400
because there's the issues of climate change.

243
00:30:07,400 --> 00:30:11,400
How do our religions relate and deal with climate change,

244
00:30:11,400 --> 00:30:15,400
relate to the environment and protection of the environment?

245
00:30:15,400 --> 00:30:19,400
The moral of this story is about contentment, really,

246
00:30:19,400 --> 00:30:24,400
which is Buddhism's probably the best way that Buddhism relates

247
00:30:24,400 --> 00:30:31,400
to environmental protection and so on is not being greedy,

248
00:30:31,400 --> 00:30:37,400
not taking more than is appropriate, that kind of thing.

249
00:30:37,400 --> 00:30:45,400
Not going to seek out and also to be appreciative,

250
00:30:45,400 --> 00:30:50,400
so the idea of appreciating the environment in this bird

251
00:30:50,400 --> 00:30:53,400
stays with the tree and appreciates it.

252
00:30:53,400 --> 00:30:57,400
It's content with it, so it's about the Buddha tells the story

253
00:30:57,400 --> 00:31:01,400
to a monk who runs, who gets kind of upset,

254
00:31:01,400 --> 00:31:04,400
because a village burns down.

255
00:31:04,400 --> 00:31:06,400
So you're staying and depending on this village,

256
00:31:06,400 --> 00:31:09,400
for the rains retreat, and the village burned down,

257
00:31:09,400 --> 00:31:11,400
I think.

258
00:31:11,400 --> 00:31:15,400
So it was very hard for him to get food.

259
00:31:15,400 --> 00:31:17,400
And so I told this to the Buddha and the Muslim,

260
00:31:17,400 --> 00:31:19,400
don't just go by food.

261
00:31:19,400 --> 00:31:22,400
Food isn't a good indicator.

262
00:31:22,400 --> 00:31:26,400
When you get the requisite, so it's not a good indicator.

263
00:31:26,400 --> 00:31:29,400
What a good place is.

264
00:31:29,400 --> 00:31:35,400
If you can meditate, well, there you should stay there.

265
00:31:35,400 --> 00:31:38,400
And then he told the story of this bird that stayed with this tree

266
00:31:38,400 --> 00:31:45,400
because it was content even when the tree died.

267
00:31:45,400 --> 00:31:48,400
It's a little bit longer than you have to hear me tell the story.

268
00:31:48,400 --> 00:31:51,400
But it's just the simple thing.

269
00:31:51,400 --> 00:31:53,400
So I got to prepare for that kind of.

270
00:31:53,400 --> 00:31:56,400
And then I got two exams next week.

271
00:31:56,400 --> 00:31:57,400
And then I'm done.

272
00:31:57,400 --> 00:32:00,400
And then off to New York.

273
00:32:00,400 --> 00:32:02,400
And then it's already almost May.

274
00:32:02,400 --> 00:32:04,400
And then in May, we've got one event.

275
00:32:04,400 --> 00:32:07,400
The Buddha's birthday in Mississauga.

276
00:32:07,400 --> 00:32:12,400
And then in June, I'm off to Asia for months.

277
00:32:12,400 --> 00:32:17,400
And then back here for the rains.

278
00:32:17,400 --> 00:32:20,400
Lots of people interested in coming to meditate all the way into,

279
00:32:20,400 --> 00:32:30,400
like, November, which is a bit awkward because I can't think that far ahead.

280
00:32:30,400 --> 00:32:35,400
I mean, because in the past, I've had trouble thinking far ahead.

281
00:32:35,400 --> 00:32:37,400
Now it seems pretty stable.

282
00:32:37,400 --> 00:32:42,400
So maybe we can start tentatively accepting long-term reservations.

283
00:32:42,400 --> 00:32:49,400
I just hesitate to do so because I can change the future's uncertainty.

284
00:32:49,400 --> 00:32:57,400
I could end up moving back to Asia or something, not likely.

285
00:32:57,400 --> 00:33:05,400
I think we can start to be a little more accepting of future plans for this sort.

286
00:33:05,400 --> 00:33:08,400
These days.

287
00:33:08,400 --> 00:33:10,400
But we maybe need a bigger place.

288
00:33:10,400 --> 00:33:15,400
We have to start thinking about finding a bigger house in the area.

289
00:33:15,400 --> 00:33:21,400
I know it's not nice to have to move, but this house really isn't.

290
00:33:21,400 --> 00:33:22,400
I mean, it's fairly big.

291
00:33:22,400 --> 00:33:25,400
It's just not well laid out for a meditation center.

292
00:33:25,400 --> 00:33:29,400
Their houses and close to the university that have like six bedrooms,

293
00:33:29,400 --> 00:33:32,400
because they were built and they were renovated to house students.

294
00:33:32,400 --> 00:33:36,400
So they're trying to fit as many students in houses as you can,

295
00:33:36,400 --> 00:33:39,400
which kind of fits our situation.

296
00:33:39,400 --> 00:33:45,400
We've got six bedroom houses that are fairly reasonable.

297
00:33:45,400 --> 00:33:49,400
We should maybe talk about that anyway.

298
00:33:49,400 --> 00:33:54,400
Anyway, that's all for tonight.

299
00:33:54,400 --> 00:33:56,400
Thank you guys.

300
00:33:56,400 --> 00:33:57,400
Thanks for coming out.

301
00:33:57,400 --> 00:34:17,400
Have a good night.

