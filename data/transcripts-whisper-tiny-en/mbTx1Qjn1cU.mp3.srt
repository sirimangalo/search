1
00:00:00,000 --> 00:00:23,840
Good evening everyone, broadcasting live March 17th, hopefully the audio is working fine.

2
00:00:23,840 --> 00:00:37,480
It makes quote actually seems a lot like a repeat and on the face of it is pretty simple quote.

3
00:00:37,480 --> 00:00:53,200
Buddhist Nandya asks, it's an interesting question, if we only look at the first part of the quote,

4
00:00:53,200 --> 00:01:00,400
we might not jump to the answer, we may not immediately think of the answer.

5
00:01:00,400 --> 00:01:06,920
I think I did, I think when I read it I was like, oh I know what he's going to say, of course.

6
00:01:06,920 --> 00:01:11,080
Oh no, actually once he said the eight things, he said there are eight things, I said,

7
00:01:11,080 --> 00:01:16,640
what conditions when developed and practiced lead to Nirvana, have Nirvana as their goal,

8
00:01:16,640 --> 00:01:29,320
culminated in Nirvana. Then he says there are Nandya eight things, and then of course we know what it's going to say.

9
00:01:29,320 --> 00:01:36,240
Eight things when you develop eight things, then you practice eight things and leads to Nirvana,

10
00:01:36,240 --> 00:01:43,840
they have Nirvana as their goal, they culminated in Nirvana.

11
00:01:43,840 --> 00:01:47,640
And of course then he says the eight four noble paths.

12
00:01:47,640 --> 00:01:57,080
Right view, right start, right speech, right action, right livelihood, right effort, right mindfulness,

13
00:01:57,080 --> 00:02:08,160
and right concentration.

14
00:02:08,160 --> 00:02:15,800
It's a simple quote, we might not have much to say about it except.

15
00:02:15,800 --> 00:02:31,600
It interests me particularly having just written an article on what I would consider non-mainstream Buddhist teaching.

16
00:02:31,600 --> 00:02:46,760
And so it gives opportunity to sort of highlight this quote as an example of really right Buddhism.

17
00:02:46,760 --> 00:03:01,400
It's easy to lose sight of the core practices that we're aiming for.

18
00:03:01,400 --> 00:03:20,720
We get caught up in theory and and and my argumentation really. There's so many new doctrines, new practices, new ideas.

19
00:03:20,720 --> 00:03:50,560
The idea that things have to be that there is a degradation or more it seems that the old, while the old stops working and so rather than try to figure out what we're doing wrong, we change the parameters.

20
00:03:50,560 --> 00:04:20,160
And so on on a sort of a the microscopic or the personal level, an example of this is where we lose sight of mindfulness and something happens and we.

21
00:04:20,160 --> 00:04:44,760
We're unable to resolve the situation, for example, we might have bright lights or colors arise, we might have rapturous experiences of excitement in the body or in the mind.

22
00:04:44,760 --> 00:05:00,400
We might have sounds or smells or tastes and feelings, we might have thoughts that arise that we don't know how to deal with.

23
00:05:00,400 --> 00:05:23,720
And we lose sight of the very basic practice, we can get lost in exceptional experiences, attend opaquilates as a good example of some of them I mentioned actually.

24
00:05:23,720 --> 00:05:36,320
Or we can get caught up in trying to find causes for our problems and creating stories for while I'm this way because of this and this and this.

25
00:05:36,320 --> 00:05:54,800
A lot of people come with the idea that they're stuck in their practice, you know, hear that a lot, just gotten stuck in my practice which is probably in.

26
00:05:54,800 --> 00:06:19,800
Nine 90% of the cases it's just a miss and the wrong focus lost having lost focus of the core practice and there's no way to get stuck there's no such thing as being I mean we're all stuck or stuck in these bodies or stuck with these mind.

27
00:06:19,800 --> 00:06:27,480
That are imperfect malfunctioning.

28
00:06:27,480 --> 00:06:40,440
And so it's not a really a matter even of getting unstuck in the situation it's about getting unstuck in our reactions to the situation.

29
00:06:40,440 --> 00:07:03,200
So if you're stuck, what you should really be asking is does that bother you, it's in body that you're stuck, how do you feel about that, do identify with that experience and moreover I guess what do you mean by being stuck because that's just a concept and gets back to this idea of losing sight of what's really happening.

30
00:07:03,200 --> 00:07:23,040
There's no, you know, what does it mean you have something sticky on your fingers, no, when you say I'm stuck it doesn't mean anything really it's a conclusion that you come to it's not an observation observations are momentary they arise and they see there's nothing to stick to.

31
00:07:23,040 --> 00:07:47,680
You know they think sticky is our reactions to things, follow this, I mean it relates to this quote because of how simple the quote is, you know if you have these eight things that's all you need, you're reading that question and you're wondering yeah what is it that leads to it around and what do you need to get, you know like alright it's just the full normal path and.

32
00:07:47,680 --> 00:08:02,320
It's kind of almost anti-climatic because climactic because you think well there must be more than that that's it that's all news.

33
00:08:02,320 --> 00:08:07,920
This is why people create new teachings, new ideas because.

34
00:08:07,920 --> 00:08:30,720
Well yeah I know the eight full normal path but there must be something more than that, right, besides the full normal path. It's like I remember I went to a teacher once and I was really sort of depressed about my situation and I've gotten real trouble with my, with a layman who was my teacher at the time.

35
00:08:30,720 --> 00:08:42,920
They were saying all sorts of nasty things about me and I felt kind of like Jamie and we had an evil person so I went to this one of the monks and I said, how do you know if you're an evil person.

36
00:08:42,920 --> 00:09:04,720
I was really at my wits and and he said, and he he of this very stark answer he said, well there these ten ten types of evil that the acoustic law come apart, which are. And they relate actually to the eight full normal path in some ways, basically wrong action, wrong speech and wrong thought.

37
00:09:04,720 --> 00:09:19,320
And he said, if you're if you're breaking any of those that seem nice and I thought about it and I said, well what if you're okay with all of those but you still feel like you're an evil person.

38
00:09:19,320 --> 00:09:30,320
It's like that kind of it was a silly silly question really, you know, we think too much about things.

39
00:09:30,320 --> 00:09:35,520
I think there's a problem. I think something's wrong.

40
00:09:35,520 --> 00:09:43,320
When in fact it comes down to reality, is it good or is it bad, is it right, is it wrong, it is or it isn't.

41
00:09:43,320 --> 00:09:59,520
Very simple, the dhamma is very simple. And so I made a statement about one text recently as being sofistic.

42
00:09:59,520 --> 00:10:15,920
Sofism and you know that's disagreeable to people who like that text and I understand that probably problematic to be bringing these things up on the internet.

43
00:10:15,920 --> 00:10:30,720
The point is, is this, you know, this is not sofistic. This is simple. There's no, there's no high or exalted teachings here. There's a perfect teaching.

44
00:10:30,720 --> 00:10:41,520
And it's a very simple teaching right view. Do you understand the foreignable interests? Do you know what is suffering? Do you know those things that are suffering? Do you know them as suffering?

45
00:10:41,520 --> 00:10:55,120
Do you understand the cause of suffering? Do you understand the cessation of suffering? Do you understand the path that leads to the cessation of suffering?

46
00:10:55,120 --> 00:11:04,920
Which is the age for the mobile path? That's right view, if you know those things. That's really, it's quite simple, you know.

47
00:11:04,920 --> 00:11:19,520
There's nothing complicated about it. You know, the taste of the dhamma, the taste of this 8-fold normal path, or this small quote, the flavor of it is unique.

48
00:11:19,520 --> 00:11:27,320
You can, this is why when you read these texts, you can smell something right away when it's fishy.

49
00:11:27,320 --> 00:11:37,320
You know something's, when something is not like this, you can catch it. The right side.

50
00:11:43,320 --> 00:11:57,120
It's trying to, how strange. You see, I get called day and night, right side, so you don't have thoughts

51
00:11:57,120 --> 00:12:04,920
without greed, we went over this a few days ago, thoughts without greed, thoughts without anger, thoughts without delusion.

52
00:12:04,920 --> 00:12:15,920
Right speech, not lying, not backbiting, not for gossiping, not referring from harsh speech,

53
00:12:15,920 --> 00:12:34,920
for framing from useless speech. Right action, not killing, not stealing, not cheating. Right livelihood, not engaging in wrong speech or wrong action for your livelihood.

54
00:12:34,920 --> 00:12:55,720
Right effort, the effort to, the effort to prevent and eradicate, to find un also states, and to cultivate and maintain or protect wholesome states.

55
00:12:55,720 --> 00:13:06,320
Right mindfulness means mindfulness of the body, mindfulness of feelings, mindfulness of the mind, mindfulness of the mind, mindfulness of the numbers.

56
00:13:06,320 --> 00:13:16,720
And right concentration means concentration that frees you from the five hindrances, usually so that are equated to the four genres,

57
00:13:16,720 --> 00:13:25,120
or concentration that is associated with the other seven path factors.

58
00:13:25,120 --> 00:13:32,920
However, you look at it, it's quite simple, quite straightforward, pure in that sense.

59
00:13:32,920 --> 00:13:41,520
There's nothing extra, there's nothing quirky or controversial even about it.

60
00:13:41,520 --> 00:13:50,720
This is why the Buddha said, any religion which has this eight full normal path in it that's considered that to be the religion where you'll find enlightened beings.

61
00:13:50,720 --> 00:14:00,320
A religion is without these eight full normal path factors, and consider that that religion is not going to have enlightened beings.

62
00:14:00,320 --> 00:14:04,520
These are the things that lead to Nirvana.

63
00:14:04,520 --> 00:14:34,320
My nirana means freedom from suffering, that's explicitly literally what nirana means, something we should not something hard to understand either, which I guess that's one thing you could look at this and say, oh, yeah, those Buddhists, they believe in this state called nirana, it's just their belief, it's not a belief, it's a term that refers to freedom from suffering.

64
00:14:34,320 --> 00:14:50,320
The cessation of suffering, and that's not a belief, I mean we have a belief that that's attainable, most people would argue that it's not attainable, okay, we have a belief that it's attainable that's our belief.

65
00:14:50,320 --> 00:15:03,320
It's a claim, you want to find out, well here's eight things that you should do, cultivate these eight things.

66
00:15:03,320 --> 00:15:12,320
You can see for yourself another is such a thing, freedom from suffering, cessation of suffering.

67
00:15:12,320 --> 00:15:41,320
It's a number for tonight, Vanessa's back from Austria, Vanessa's back, Robin left, more people coming, I don't know.

68
00:15:41,320 --> 00:15:46,320
We have room if anyone wants to come and do a course here.

69
00:15:46,320 --> 00:16:08,320
We also have room on the meeting pages, if anyone wants to do an online course, which is really a great way to start.

70
00:16:08,320 --> 00:16:25,320
If you want to learn how to meditate, if you want to start, but coming to do an intensive course seems to daunting, or you don't have the ability to come out here to an online course, it's a good way to start.

71
00:16:25,320 --> 00:16:42,320
Good way to get a good way to get a basic understanding of a teaching.

72
00:16:42,320 --> 00:16:57,320
Anyway, so that's all for tonight. If anyone has questions, you're welcome to join the Hangout.

73
00:16:57,320 --> 00:17:17,320
It's my name, I have a fixed hand. Hello, do you have a question? Your mic is muted.

74
00:17:17,320 --> 00:17:26,320
You have to unmute yourself.

75
00:17:26,320 --> 00:17:33,320
I can't hear you or see.

76
00:17:33,320 --> 00:17:36,320
You can go.

77
00:17:36,320 --> 00:17:42,320
We'll meet tomorrow. What's tomorrow Friday? We'll meet tomorrow at five.

78
00:17:42,320 --> 00:17:53,320
I should give you the first paper you have 10 days.

79
00:17:53,320 --> 00:17:57,320
Good.

80
00:17:57,320 --> 00:18:03,320
You've done this before, no? Did this last time you were here, right?

81
00:18:03,320 --> 00:18:24,320
Everything you need would happen to your foot.

82
00:18:24,320 --> 00:18:34,320
How about walking, walking is okay?

83
00:18:34,320 --> 00:18:42,320
Anyone don't strain it? Do it again?

84
00:18:42,320 --> 00:19:00,320
It's okay. This kind of feels so like, over how many you come back here?

85
00:19:00,320 --> 00:19:18,320
I don't know where things back on.

86
00:19:18,320 --> 00:19:39,320
My family was the same.

87
00:19:39,320 --> 00:19:49,320
I've come here zombie.

88
00:19:49,320 --> 00:20:00,320
But did you lose your job? Is that cool? No? You're okay?

89
00:20:00,320 --> 00:20:15,320
I feel like I feel like strong, you know, my decision is waking up in a thunderstorm.

90
00:20:15,320 --> 00:20:31,320
I think if it has a duty, if I don't do it, I don't need it.

91
00:20:31,320 --> 00:20:46,320
Remember, it's all about our reactions. I'm not so concerned about our situation.

92
00:20:46,320 --> 00:21:09,320
If you want to, if you want to, if you want to,

93
00:21:09,320 --> 00:21:23,320
you're okay.

94
00:21:23,320 --> 00:21:27,320
I'm over the reunion there, so it's strange.

95
00:21:27,320 --> 00:21:36,320
Take care of time. You don't come by it, so mindfulness is going to stay on the path.

96
00:21:36,320 --> 00:21:37,320
It's just detailed.

97
00:21:37,320 --> 00:21:38,320
Yeah.

98
00:21:38,320 --> 00:21:39,320
It takes me here actually.

99
00:21:39,320 --> 00:21:40,320
Yeah.

100
00:21:40,320 --> 00:21:42,320
I really like it.

101
00:21:42,320 --> 00:21:43,320
I'm glad to have you.

102
00:21:43,320 --> 00:21:44,320
Yeah.

103
00:21:44,320 --> 00:21:46,320
It's so entitled sometimes.

104
00:21:46,320 --> 00:21:57,320
It's like, it's like, it's like, it's like, it's like, it's like, it's like, it's like,

105
00:21:57,320 --> 00:22:09,320
it's like, it's like, it's like, it's like, it's like you're making up

106
00:22:09,320 --> 00:22:11,320
pressure so that all people keep working more and more.

107
00:22:11,320 --> 00:22:12,320
That's like, definitely.

108
00:22:12,320 --> 00:22:13,320
And it's like, I have to lie to them all because I see the

109
00:22:13,320 --> 00:22:16,320
model.

110
00:22:16,320 --> 00:22:24,320
I see the devil suffering in their things.

111
00:22:24,320 --> 00:22:26,320
I noticed that still, I can't really state dead conscious.

112
00:22:26,320 --> 00:22:44,400
Do what you can, do what you can with what you've got, but that's the thing, you know,

113
00:22:44,400 --> 00:22:51,280
if you stay in the world, you got to deal with that, you're lucky some people have really

114
00:22:51,280 --> 00:22:58,160
hired, right? Some countries, some places, it's quite scary actually, very little opportunity

115
00:22:58,160 --> 00:23:18,480
for reflection and mindfulness, right? No, let's see the, where they call the, you have to,

116
00:23:18,480 --> 00:23:26,960
do the kind of word, it's not perfect, no, you live in the world, so you can't be mindful

117
00:23:26,960 --> 00:23:35,440
all the time, you have to work, you have to interact, I'm glad you're here, you can get started,

118
00:23:35,440 --> 00:23:42,640
we'll talk again tomorrow at five, okay, get settled in, you ready to start or you want to start

119
00:23:42,640 --> 00:23:47,440
tomorrow, are you okay to start now? You've got time, you don't have to start tonight if you want

120
00:23:47,440 --> 00:23:55,600
to take it easy in the beginning, see how it goes, you're still jet-like I guess you flew today

121
00:23:55,600 --> 00:23:59,920
and you've got, you have lots of time, if you don't want to start today you can start tomorrow,

122
00:23:59,920 --> 00:24:07,200
we can meet the next day, we can meet tomorrow, but if you don't want to switch we can,

123
00:24:07,200 --> 00:24:11,840
you don't do enough practice on this one, we'll meet tomorrow at five by the way,

124
00:24:11,840 --> 00:24:22,240
I will, you see how it goes, yeah, okay, you're ready, yeah, I made good sleep soon, but

125
00:24:22,240 --> 00:24:36,000
yeah, you know where food is, everything, it's all, thank you, it's not so food, if you need

126
00:24:36,000 --> 00:24:40,240
anything special, there's anything missing in the food or something,

127
00:24:40,240 --> 00:24:45,200
you know, it's just, they very can go like, well, they just move in there something to do,

128
00:24:45,200 --> 00:24:49,360
like, I don't know what kind of work you would have wanted to see.

129
00:24:49,360 --> 00:24:55,840
Yeah, yeah, there's really not, I don't, I don't really know, there's not much, just yeah,

130
00:24:55,840 --> 00:25:00,400
it's good if you make sure like if something needs, you see something's dirty or need sweeping

131
00:25:00,400 --> 00:25:05,120
or so on, great, you have time, you have time every day to do a little bit of sweeping or a little

132
00:25:05,120 --> 00:25:19,440
bit of something, it's good. We're getting a blender, so you can make, there's frozen fruit,

133
00:25:19,440 --> 00:25:27,600
you can make smoothies if you want. Yeah, frozen fruits apparently easiest because it doesn't

134
00:25:27,600 --> 00:25:33,600
go bad, so there's Robin brought frozen fruit, when we get this thing, you can, I don't know how

135
00:25:33,600 --> 00:25:47,440
hard it's been, this makes me. Yeah. Okay, good. All right, have a good night. See you tomorrow.

136
00:25:50,560 --> 00:25:50,880
All right.

137
00:25:55,680 --> 00:25:59,440
Try to figure out what's going on here, I've had like three people trying to calm me.

138
00:25:59,440 --> 00:26:06,400
Is this people actually trying to get there? Is this just a malfunction of my phone?

139
00:26:07,360 --> 00:26:11,360
People actually trying to get into the, if you're trying to get into the hangout, if you're watching

140
00:26:11,360 --> 00:26:15,680
this and thinking, how can I join? How can I join? How can I join? You have to click on the link,

141
00:26:15,680 --> 00:26:16,480
there's a link.

142
00:26:19,120 --> 00:26:25,840
No, I'm talking to people actually on the internet right now, there's people watching this.

143
00:26:25,840 --> 00:26:31,120
I'm sorry. It's okay. And now you don't, you don't have to pay attention to this, but there's

144
00:26:31,120 --> 00:26:36,400
people I think trying to get on. If you want to get on, you have to go to meditation.serimongo.org

145
00:26:37,440 --> 00:26:42,640
and click on the link. It's the only way. Good night.

146
00:26:42,640 --> 00:27:02,400
And since no one's joining except for this mute and mysterious pictureless person,

147
00:27:02,400 --> 00:27:18,320
I think we're going to say good night. If you were trying to get on, try again tomorrow. Good night, everyone.

