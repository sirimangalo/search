1
00:00:00,000 --> 00:00:06,560
Okay, I understand calling myself a Buddhist isn't important, but I was wondering if you

2
00:00:06,560 --> 00:00:12,100
could suggest a more Buddhist way of living my life daily practice.

3
00:00:12,100 --> 00:00:17,960
More specifically, in your tradition is there a structure to daily life I should be following.

4
00:00:17,960 --> 00:00:27,280
I, E meditation twice a day, have designated reflection time, designated study time, etc.

5
00:00:27,280 --> 00:00:31,280
I think so. You understood? Go ahead.

6
00:00:31,280 --> 00:00:36,280
I just had the thought, try to be mindful all the time.

7
00:00:36,280 --> 00:00:44,280
That seems to be the most important for me, but now please continue with what you wanted to say.

8
00:00:44,280 --> 00:00:51,280
I find the schedule helps. No, it hesitating because it's...

9
00:00:51,280 --> 00:00:55,280
You don't want to be beating yourself up because you don't have a schedule.

10
00:00:55,280 --> 00:01:00,280
Oh, I didn't meditate for my two hours today. Oh, what a bad meditator I am.

11
00:01:00,280 --> 00:01:04,280
How am I ever going to become a nightmare? Wait a minute.

12
00:01:04,280 --> 00:01:08,280
Why am I not meditating on this, right? Why am I not mindful of this?

13
00:01:08,280 --> 00:01:10,280
Because meditation can come at any time.

14
00:01:10,280 --> 00:01:17,280
But I think without a schedule in the world, it's easy to get caught up.

15
00:01:17,280 --> 00:01:23,280
You say, man, I'll just be mindful during my daily life, and then you become less and less mindful during your daily life.

16
00:01:23,280 --> 00:01:29,280
You can get lost because you really have no...

17
00:01:29,280 --> 00:01:32,280
Nothing to lean on or nothing to cling to.

18
00:01:32,280 --> 00:01:37,280
Nothing to keep you strong when you're training.

19
00:01:37,280 --> 00:01:41,280
If you're not already strong in and of yourself in the world,

20
00:01:41,280 --> 00:01:45,280
you'll just get washed away because you can say, be mindful.

21
00:01:45,280 --> 00:01:50,280
But if you don't even know what mindfulness is, how can you...

22
00:01:50,280 --> 00:01:53,280
Like pulling yourself up by your bootstraps, right?

23
00:01:53,280 --> 00:01:59,280
So in that sense, especially in the beginning, a schedule I think can be quite helpful.

24
00:01:59,280 --> 00:02:06,280
And it forces you to not get involved. It gives you an excuse.

25
00:02:06,280 --> 00:02:12,280
If you can manage to have a schedule, you can say, well, now's my meditation time or today's my meditation day or so on.

26
00:02:12,280 --> 00:02:18,280
And if you have that schedule for yourself, you can make excuses for yourself and for other people.

27
00:02:18,280 --> 00:02:27,280
You shouldn't be doing other things. Otherwise, it's easy to say, oh, just let it go and go with the flow and they want me to help them with this or that.

28
00:02:27,280 --> 00:02:32,280
Well, you know, I'll make merit doing helping them with this or that.

29
00:02:32,280 --> 00:02:38,280
But when you have a schedule, you can say, I'm sorry at this time, I'm not free, I'm doing my meditation and so on.

30
00:02:38,280 --> 00:02:44,280
So it helps you to actually do the practice.

31
00:02:44,280 --> 00:02:53,280
I found that that helpful for me when I was in university, for example, that I would make sure in the morning I did some meditation and in the evening I did some meditation.

32
00:02:53,280 --> 00:03:02,280
I guess probably what would be best to suggest then is that to do some, but not set time limits.

33
00:03:02,280 --> 00:03:08,280
So some days it might only be half an hour, some days it might be an hour, some days it might only be five minutes.

34
00:03:08,280 --> 00:03:16,280
Meditators, when they leave, generally one of the most common questions we get is, how much meditation should I do every day?

35
00:03:16,280 --> 00:03:21,280
You know, obviously they won't be able to every day do eight hours, ten hours, twelve hours a day.

36
00:03:21,280 --> 00:03:24,280
How much is the minimum that I should do?

37
00:03:24,280 --> 00:03:33,280
And so, you know, after getting this question often I started, my answer started being, if you do zero minutes, that's not enough.

38
00:03:33,280 --> 00:03:39,280
As long as you do more than that, you can consider that you've done your duty for the day. Do some meditation every day.

39
00:03:39,280 --> 00:03:46,280
I think that's useful, it may seem silly because well, then one minute is enough and then I should feel content, but no.

40
00:03:46,280 --> 00:03:48,280
The point then is that you're meditating.

41
00:03:48,280 --> 00:03:50,280
The point then is that you're intent on meditating.

42
00:03:50,280 --> 00:03:58,280
There are people who, who have to they leave, they will go years without meditating, which is unfathomable, really.

43
00:03:58,280 --> 00:04:05,280
If you set yourself on doing some every day and you don't set how much, say how many it's going to be, I think you'll never come to that.

44
00:04:05,280 --> 00:04:11,280
I think you'll always come back to, oh, you know, today maybe I'll do a little more or push myself.

45
00:04:11,280 --> 00:04:18,280
Because if you do five minutes, you say, wow, that was, you know, my mind's still not even settled.

46
00:04:18,280 --> 00:04:20,280
And you'll say, oh, that was a horrible meditation.

47
00:04:20,280 --> 00:04:24,280
And you'll say, that's because I haven't been mindful for the rest of the day.

48
00:04:24,280 --> 00:04:33,280
It gives you that wake-up call and says, hey, remember, you're not on this earth just to die.

49
00:04:33,280 --> 00:04:42,280
You're not on this earth just to eat, drink, and be merry, and then pass away having lived a useless life.

50
00:04:42,280 --> 00:04:45,280
That there's more to life than this, it wakes you up.

51
00:04:45,280 --> 00:04:51,280
Hey, your mind, you know, the mind is, is another organ in a sense.

52
00:04:51,280 --> 00:04:57,280
You know, you have the muscles in your body, and if you don't take care of them, they become like ours.

53
00:04:57,280 --> 00:04:59,280
Well, the mind is that that way as well.

54
00:04:59,280 --> 00:05:04,280
Hey, if you don't take care of your mind, it starts to atrophy.

55
00:05:04,280 --> 00:05:14,280
Your mind becomes weak, your mind becomes useless, your mind becomes your worst enemy.

56
00:05:14,280 --> 00:05:22,280
And so the seeing, the seeing that when you practice even, say, five minutes a day,

57
00:05:22,280 --> 00:05:26,280
you say, I'm going to at least do some every day, and you do five minutes, you'll see that.

58
00:05:26,280 --> 00:05:30,280
You'll say, oh, my mind has got some bad stuff in it.

59
00:05:30,280 --> 00:05:33,280
I've been neglecting my mind.

60
00:05:33,280 --> 00:05:36,280
And that, I think, helps to keep you on the path.

61
00:05:36,280 --> 00:05:41,280
So in regards to practice, that's what I would say, do at least, at least,

62
00:05:41,280 --> 00:05:46,280
more than, whatever is more than zero, do that much per day, at least that much per day.

63
00:05:46,280 --> 00:05:49,280
And you don't have to specify, I wouldn't specify.

64
00:05:49,280 --> 00:05:54,280
If you want to say, I'm going to do more than zero twice a day, which is even better.

65
00:05:54,280 --> 00:05:59,280
And then you don't have to specify at least before you sleep, do five minutes.

66
00:05:59,280 --> 00:06:06,280
And, well, of course, the more the better, but yeah, you can add that in there, and the more the better.

67
00:06:06,280 --> 00:06:10,280
So then you have your schedule set up.

68
00:06:10,280 --> 00:06:19,280
As for reflection time and study time, you know, it so depends on your lifestyle, right?

69
00:06:19,280 --> 00:06:20,280
Great.

70
00:06:20,280 --> 00:06:26,280
If you can do study every day when, as a monk, having been a monk for most of the time that I was Buddhist,

71
00:06:26,280 --> 00:06:30,280
I don't really have much to compare to.

72
00:06:30,280 --> 00:06:37,280
I've been studying quite a bit, but even as a lay person, I would always dedicate time to reading the Buddha's teaching.

73
00:06:37,280 --> 00:06:42,280
You know, you have this book, so the Buddha's teaching, try to do one suta a day.

74
00:06:42,280 --> 00:06:49,280
I don't think designated times is generally helpful in that sense, but it depends on your schedule.

75
00:06:49,280 --> 00:06:51,280
It depends on your lifestyle.

76
00:06:51,280 --> 00:06:55,280
I would say, you know, do some every day, make sure that you're doing some every day.

77
00:06:55,280 --> 00:07:04,280
If you have the time, and more if you have the community, then you can set up a schedule and do it together as a group,

78
00:07:04,280 --> 00:07:08,280
get everyone together and have each other support.

79
00:07:08,280 --> 00:07:16,280
If you're on your own, I think it's difficult to expect that you're going to every day do some at this time or that time.

80
00:07:16,280 --> 00:07:22,280
But if you set a schedule that you're going to try every day every day to do to read at least one suta or something like that,

81
00:07:22,280 --> 00:07:24,280
I mean, no.

82
00:07:24,280 --> 00:07:25,280
I guess the answer is no.

83
00:07:25,280 --> 00:07:31,280
There isn't an art tradition any structure in daily life, except the more the better.

84
00:07:31,280 --> 00:07:39,280
And if you're really concerned, well, the answer is to, I'm not supposed to say that.

85
00:07:39,280 --> 00:07:42,280
The answer is to become a monk.

86
00:07:42,280 --> 00:07:51,280
If you really want a more Buddhist way of living your life, not everyone can become a monk, of course.

87
00:07:51,280 --> 00:07:56,280
There are people who, you know, have given this talk before about this man who couldn't become a monk,

88
00:07:56,280 --> 00:08:05,280
I think, because he was looking after his aged parents, got the karika, got the kara,

89
00:08:05,280 --> 00:08:06,280
who made pots.

90
00:08:06,280 --> 00:08:14,280
So what he'd do, and I've told this story many times, go down to the river, get this earth that had been dug up or had been washed out,

91
00:08:14,280 --> 00:08:24,280
find wood that had been knocked down and was just useless wood and fire the make pots and fire them and sell them by the side of the road.

92
00:08:24,280 --> 00:08:29,280
And that's how he lived his life, just sitting by the side of the road, selling these handmade pots.

93
00:08:29,280 --> 00:08:31,280
And when people ask, how much do you want for that pot?

94
00:08:31,280 --> 00:08:33,280
You could say, just leave what you like.

95
00:08:33,280 --> 00:08:37,280
If you want to leave some rice or leave some beans, that's enough.

96
00:08:37,280 --> 00:08:39,280
Whatever you think it's worth.

97
00:08:39,280 --> 00:08:43,280
And that's how he lived his life and took care of his mother and father.

98
00:08:43,280 --> 00:08:45,280
He was an anagami, though.

99
00:08:45,280 --> 00:08:53,280
So the only reason I bring it up is kind of the idea of simplifying your life to become more and more like.

100
00:08:53,280 --> 00:09:07,280
The ideal solitude or the state of being in a meditation retreat or the life of a meditator.

101
00:09:07,280 --> 00:09:15,280
I would only add that it might be helpful to find a group.

102
00:09:15,280 --> 00:09:27,280
So if you can't go on retreat regularly, which would be the best next to becoming a monk.

103
00:09:27,280 --> 00:09:34,280
And if you can't go on retreat very often, then it would be good to have a group.

104
00:09:34,280 --> 00:09:46,280
Maybe we can study the Dhamma with or discuss the Dhamma with and can meditate together also.

