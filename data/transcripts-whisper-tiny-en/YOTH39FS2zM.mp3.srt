1
00:00:00,000 --> 00:00:04,120
Hello and welcome back to our study with Dampanda.

2
00:00:04,120 --> 00:00:32,120
Today we look at 1st, 175, which reads as follows.

3
00:00:32,120 --> 00:00:44,120
And by the path of the sun, the powerful go through the air.

4
00:00:44,120 --> 00:00:50,400
The wise go out from this world, Nianti, Diranookama.

5
00:00:50,400 --> 00:01:00,640
The wise go out from this world, having defeated Mara and his arm, together with his arm.

6
00:01:00,640 --> 00:01:07,440
The three different paths or ways of going.

7
00:01:07,440 --> 00:01:12,640
The story behind this is one of the very short stories in the Dhammapadha.

8
00:01:12,640 --> 00:01:14,480
It's not much to it.

9
00:01:14,480 --> 00:01:19,640
The story goes that there were 30 monks who came to see the Buddha.

10
00:01:19,640 --> 00:01:23,840
And they came just as Ananda who was going to see the Buddha.

11
00:01:23,840 --> 00:01:27,320
So Ananda saw that these monks were coming and he thought,

12
00:01:27,320 --> 00:01:33,320
Okay, I'll wait and let them have a private audience with the Buddha and then I'll go.

13
00:01:33,320 --> 00:01:42,320
And I think Ananda was going to go wait on the Buddha, helping bathe or provide him with whatever he needed.

14
00:01:42,320 --> 00:01:46,320
So, well, I'll just wait.

15
00:01:46,320 --> 00:01:50,320
And he waited and waited.

16
00:01:50,320 --> 00:01:56,320
They never came out and so he went in.

17
00:01:56,320 --> 00:02:01,320
After quite some time and the monks were gone.

18
00:02:01,320 --> 00:02:04,320
They just disappeared.

19
00:02:04,320 --> 00:02:06,320
And the text explains what happened.

20
00:02:06,320 --> 00:02:07,320
Well, they were there.

21
00:02:07,320 --> 00:02:09,320
Well, Ananda was waiting.

22
00:02:09,320 --> 00:02:11,320
They listened to the Buddha teach, what he called.

23
00:02:11,320 --> 00:02:17,320
They called Sārini and Hama, which is actually a specific Dhammapadha.

24
00:02:17,320 --> 00:02:20,320
Something memorable, Sārini is memorable.

25
00:02:20,320 --> 00:02:25,320
So I think it just means he taught them some memorable Dhammapadha.

26
00:02:25,320 --> 00:02:28,320
Having listened to this, they became our hunts.

27
00:02:28,320 --> 00:02:31,320
Having become our hunts become enlightened.

28
00:02:31,320 --> 00:02:37,320
They flew through the air out of the forest, I guess, where was the Buddha?

29
00:02:37,320 --> 00:02:42,320
It was in the Jita one, so he was in the forest.

30
00:02:42,320 --> 00:02:48,320
And these 30 monks were after hearing the Buddha's teaching and becoming enlightened.

31
00:02:48,320 --> 00:02:50,320
They flew out.

32
00:02:50,320 --> 00:02:55,320
So Ananda came in and didn't see them in Sārini, where did those monks go?

33
00:02:55,320 --> 00:02:57,320
What happened to those monks?

34
00:02:57,320 --> 00:02:58,320
Where are they?

35
00:02:58,320 --> 00:02:59,320
Kata.

36
00:02:59,320 --> 00:03:02,320
They were gone.

37
00:03:02,320 --> 00:03:09,320
He asked, Kata, Nina, Monkey, and by which path did they go one day?

38
00:03:09,320 --> 00:03:14,320
Ananda, they went by the sky.

39
00:03:14,320 --> 00:03:16,320
And Ananda said, what?

40
00:03:16,320 --> 00:03:19,320
They became our hunts already, that quickly.

41
00:03:19,320 --> 00:03:23,320
You know, as I mean, they keen as of what he says.

42
00:03:23,320 --> 00:03:29,320
So having destroyed the taints and he says, in my presence,

43
00:03:29,320 --> 00:03:35,320
having heard the Dhamma, they became our hunts.

44
00:03:35,320 --> 00:03:40,320
And at that moment it said that there were some swans flying through the air,

45
00:03:40,320 --> 00:03:46,320
and the Buddha said, Ananda, those who have well developed

46
00:03:46,320 --> 00:03:49,320
before Idipada.

47
00:03:49,320 --> 00:03:56,320
Idipada is the roads to power, the path to power.

48
00:03:56,320 --> 00:04:00,320
Fly through the air, go through the air like a swan.

49
00:04:00,320 --> 00:04:06,320
And then he taught this verse.

50
00:04:06,320 --> 00:04:10,320
So I guess there's a lot we could say about magical powers,

51
00:04:10,320 --> 00:04:12,320
like flying through the air.

52
00:04:12,320 --> 00:04:14,320
I'd rather not talk too much about it.

53
00:04:14,320 --> 00:04:25,320
I've sort of festigously or dedicatedly avoided talking about the more magical parts.

54
00:04:25,320 --> 00:04:32,320
And I've have explained earlier in this series about how it's not really the point.

55
00:04:32,320 --> 00:04:36,320
And it doesn't really matter if you don't believe in such things.

56
00:04:36,320 --> 00:04:40,320
I think that sort of a cop out we have is that, oh, nowadays,

57
00:04:40,320 --> 00:04:48,320
it's much less common near, near, unheard of to have someone gain magical powers

58
00:04:48,320 --> 00:04:50,320
being able to fly through the air.

59
00:04:50,320 --> 00:04:55,320
But of course it's more important to note that it's not necessary.

60
00:04:55,320 --> 00:05:04,320
And there too are related in the sense that they both deal with the mind.

61
00:05:04,320 --> 00:05:12,320
But the path to become an horror hunt and the path to gain magical powers are quite divergent.

62
00:05:12,320 --> 00:05:15,320
And one is not necessary for the other.

63
00:05:15,320 --> 00:05:20,320
Now Ananda makes an interesting, his question is interesting

64
00:05:20,320 --> 00:05:26,320
because it almost sounds like he assumes that if they gain magical powers

65
00:05:26,320 --> 00:05:28,320
they must become a horror hunt.

66
00:05:28,320 --> 00:05:33,320
Which I think is a fair assumption because that is what the Buddha would have led them towards.

67
00:05:33,320 --> 00:05:38,320
I mean, I think what's more fantastical about this story is how quickly people can become enlightened

68
00:05:38,320 --> 00:05:40,320
in the presence of the Buddha.

69
00:05:40,320 --> 00:05:43,320
That's really an amazing thing.

70
00:05:43,320 --> 00:05:48,320
And I think open to some skepticism people would think, how is this possible?

71
00:05:48,320 --> 00:05:52,320
It takes me so long to even gain a little bit of insight.

72
00:05:52,320 --> 00:05:55,320
And yet these people, they just heard the Buddha speak.

73
00:05:55,320 --> 00:05:58,320
We don't know exactly what the Buddha taught these monks,

74
00:05:58,320 --> 00:06:04,320
but we have this idea that the Buddha was very good at giving people

75
00:06:04,320 --> 00:06:06,320
exactly what they needed.

76
00:06:06,320 --> 00:06:12,320
And people who had the great fortune to meet with the Buddha were fortunate

77
00:06:12,320 --> 00:06:16,320
in such a way that they already had quite a good deal of development

78
00:06:16,320 --> 00:06:23,320
that those of us who are still stuck in samsara thousands of years later probably didn't have.

79
00:06:23,320 --> 00:06:26,320
So there's that as well.

80
00:06:26,320 --> 00:06:31,320
But there's a lot of interesting discussion about how it could be possible to,

81
00:06:31,320 --> 00:06:39,320
for example, fly through the air when physics appears to show that that's not possible.

82
00:06:39,320 --> 00:06:48,320
So I guess there's some sense that the laws of physics are circumscribed

83
00:06:48,320 --> 00:06:58,320
or are limited and are separate from the realm of the mind

84
00:06:58,320 --> 00:07:04,320
and in fact manipulate a manipulatable by the mind.

85
00:07:04,320 --> 00:07:10,320
In ways that are not really possible to detect within with physical instruments,

86
00:07:10,320 --> 00:07:17,320
the mind being outside, I take for example how quantum physics works.

87
00:07:17,320 --> 00:07:22,320
I mean first of all the example that quantum physics really threw a monkey wrench

88
00:07:22,320 --> 00:07:30,320
in the whole deterministic concept of reality or at least the physical concept of reality

89
00:07:30,320 --> 00:07:36,320
that we're not really sure anymore what reality is made of or there's debate.

90
00:07:36,320 --> 00:07:40,320
There's more debate because clearly it's not just billiard balls,

91
00:07:40,320 --> 00:07:45,320
any more of a billiard balls of matter bouncing off of each other.

92
00:07:45,320 --> 00:07:51,320
But more than that it showed that apparently you can,

93
00:07:51,320 --> 00:07:55,320
I mean one interpretation at any rate,

94
00:07:55,320 --> 00:08:02,320
is that there is this dependency of physical reality on the mind or an interaction.

95
00:08:02,320 --> 00:08:06,320
Of course it's hotly debated and a lot of people would deny that,

96
00:08:06,320 --> 00:08:14,320
but there is a way of understanding anyway it's quite.

97
00:08:14,320 --> 00:08:23,320
The point being that reality is a lot less fixed or certain than we might think it is

98
00:08:23,320 --> 00:08:28,320
and I don't want to dwell too much on this because I think there's of course much more important things.

99
00:08:28,320 --> 00:08:33,320
Regardless of your appreciation for the ability to fly through the air,

100
00:08:33,320 --> 00:08:41,320
I think the most important point of this story is that the Buddha appears to in a way put on a remind

101
00:08:41,320 --> 00:08:48,320
of something or make clear to redirect the conversation.

102
00:08:48,320 --> 00:08:54,320
So Ananda associates flying through the air with enlightenment

103
00:08:54,320 --> 00:09:03,320
and the Buddha is fairly quick it seems to remind him or to make clear the point

104
00:09:03,320 --> 00:09:11,320
that it's those who have gained the mental power sort of this mental fortitude,

105
00:09:11,320 --> 00:09:18,320
which is useful for becoming enlightened, but not sufficient.

106
00:09:18,320 --> 00:09:21,320
You might say it's necessary to become enlightened as well,

107
00:09:21,320 --> 00:09:24,320
but when directed in a certain way,

108
00:09:24,320 --> 00:09:33,320
before you depart which are your appreciation or your interest in something,

109
00:09:33,320 --> 00:09:37,320
the effort you put out for the task,

110
00:09:37,320 --> 00:09:43,320
the focus of mind that you put on the task and your reflection,

111
00:09:43,320 --> 00:09:53,320
your ability to adjust and adapt and keep a wise look on a wise view of what you're doing.

112
00:09:53,320 --> 00:09:59,320
These four things are the roads to success, the roads to any power and any kind of activity,

113
00:09:59,320 --> 00:10:07,320
but particularly they relate to magical powers that gain the help you gain fortitude of mind.

114
00:10:07,320 --> 00:10:18,320
And the Buddha says those who fly through the air have gained that power of mind,

115
00:10:18,320 --> 00:10:23,320
and in the power.

116
00:10:23,320 --> 00:10:27,320
But it reminds Ananda that the real path, the real going,

117
00:10:27,320 --> 00:10:30,320
is not when you go through the air or go on land.

118
00:10:30,320 --> 00:10:34,320
The most important is going out knee-yant,

119
00:10:34,320 --> 00:10:39,320
the young team means they go knee-yant, it's like knee-bana,

120
00:10:39,320 --> 00:10:45,320
knee-yant, they go out from this world, Lokama.

121
00:10:45,320 --> 00:10:51,320
So the real lesson here is about the path, it's about the way.

122
00:10:51,320 --> 00:10:55,320
And it relates to things like gaining magical powers

123
00:10:55,320 --> 00:11:02,320
because magical powers are fortitude of mind as one spiritual goal.

124
00:11:02,320 --> 00:11:10,320
Even the great calm and tranquil states are desired and pursued

125
00:11:10,320 --> 00:11:15,320
by a great number of spiritual people in the world.

126
00:11:15,320 --> 00:11:22,320
But the Buddha makes clear that this path following whatever it may be,

127
00:11:22,320 --> 00:11:26,320
whether it might be the ordinary way of going here or going there

128
00:11:26,320 --> 00:11:30,320
or becoming this, becoming that, or even the spiritual way,

129
00:11:30,320 --> 00:11:33,320
of being able to fly through the air or read people's minds

130
00:11:33,320 --> 00:11:37,320
or remember your past life.

131
00:11:37,320 --> 00:11:44,320
So many different magical powers that are claimed to be possible.

132
00:11:44,320 --> 00:11:47,320
But the real path is internal.

133
00:11:47,320 --> 00:11:53,320
It's about leaving the world, which can never be done no matter how fast you fly

134
00:11:53,320 --> 00:12:02,320
or matter how fast you run.

135
00:12:02,320 --> 00:12:08,320
But it's done by inner, an inner movement, an inner travel,

136
00:12:08,320 --> 00:12:15,320
traveling from the defiled state to an undefiled state,

137
00:12:15,320 --> 00:12:25,320
traveling from impurity to purity, from darkness to light.

138
00:12:25,320 --> 00:12:28,320
Because it feels very much like we're traveling.

139
00:12:28,320 --> 00:12:33,320
So going a long way, you practice a lot of meditation days, weeks,

140
00:12:33,320 --> 00:12:35,320
months, years, lifetimes.

141
00:12:35,320 --> 00:12:37,320
It really feels like you've taken a trip.

142
00:12:37,320 --> 00:12:41,320
If you think about who you were before you started practicing,

143
00:12:41,320 --> 00:12:45,320
you've come a long way, we would say.

144
00:12:45,320 --> 00:12:48,320
It is a real path about a described it as such.

145
00:12:48,320 --> 00:12:51,320
And you would chastise the monks if they were talking too much

146
00:12:51,320 --> 00:12:53,320
about external paths.

147
00:12:53,320 --> 00:12:56,320
It's an a-sable mongol, not done yet.

148
00:12:56,320 --> 00:13:00,320
This is the path, the only path, not those other ones.

149
00:13:00,320 --> 00:13:07,320
There are no others.

150
00:13:07,320 --> 00:13:09,320
When you begin to practice mindfulness,

151
00:13:09,320 --> 00:13:13,320
your perception of the world changes rather than thinking about,

152
00:13:13,320 --> 00:13:15,320
oh, now I'm here, now I'm there.

153
00:13:15,320 --> 00:13:18,320
The real has that actually are always here.

154
00:13:18,320 --> 00:13:20,320
Wherever you go, it's here.

155
00:13:20,320 --> 00:13:23,320
And this is because our ordinary activity

156
00:13:23,320 --> 00:13:27,320
in a conceptual sense is never here.

157
00:13:27,320 --> 00:13:29,320
It's always somewhere else.

158
00:13:29,320 --> 00:13:35,320
Or thinking about conceptual places and things.

159
00:13:35,320 --> 00:13:39,320
But when you're mindful, you're truly here and now.

160
00:13:39,320 --> 00:13:43,320
You realize that before you only thought you were here,

161
00:13:43,320 --> 00:13:47,320
here or there, but now you can see where you really are.

162
00:13:47,320 --> 00:13:51,320
Because you're in tune with what's really happening

163
00:13:51,320 --> 00:13:58,320
in the present moment.

164
00:13:58,320 --> 00:14:01,320
So the question then is, well, what is this path?

165
00:14:01,320 --> 00:14:04,320
What is this path that we talk about being mindful

166
00:14:04,320 --> 00:14:07,320
and what is the way?

167
00:14:07,320 --> 00:14:12,320
And the Buddha provides, you know, we obviously give many answers.

168
00:14:12,320 --> 00:14:18,320
And this is a topic forms the core of a lot of what I teach

169
00:14:18,320 --> 00:14:22,320
and what the Buddha taught, what we teach and Buddhism.

170
00:14:22,320 --> 00:14:29,320
But in this verse, the Buddha offers an interesting example

171
00:14:29,320 --> 00:14:32,320
or interesting description.

172
00:14:32,320 --> 00:14:36,320
Marang, Taitwa, Marang, Sawaheenin,

173
00:14:36,320 --> 00:14:39,320
having conquered Mara.

174
00:14:39,320 --> 00:14:42,320
This is a way that we often talk about the path.

175
00:14:42,320 --> 00:14:45,320
Mara is the evil one.

176
00:14:45,320 --> 00:14:50,320
Mara means evil, having conquered evil,

177
00:14:50,320 --> 00:14:58,320
together with its armies.

178
00:14:58,320 --> 00:15:01,320
And so Mara, when we talk about evil,

179
00:15:01,320 --> 00:15:04,320
I mean, it's being impersonified.

180
00:15:04,320 --> 00:15:09,320
And there is a sense that one type of Mara is actually a heavenly

181
00:15:09,320 --> 00:15:19,320
or a supernatural being, supernatural being, living in some high state of existence

182
00:15:19,320 --> 00:15:25,320
that has what they call the delight over the creation of others.

183
00:15:25,320 --> 00:15:30,320
So there are beings out there that delight in our creations.

184
00:15:30,320 --> 00:15:34,320
They're probably having a field day with all the things that humans have created.

185
00:15:34,320 --> 00:15:38,320
And so they are supposed to try and manipulate.

186
00:15:38,320 --> 00:15:42,320
They spend time manipulating humans to make them create more.

187
00:15:42,320 --> 00:15:48,320
And they're very much displeased by people like the Buddha

188
00:15:48,320 --> 00:15:56,320
who encourage people to stop being creative and to stop chasing after things.

189
00:15:56,320 --> 00:16:01,320
Stop building up really in a sense tearing down

190
00:16:01,320 --> 00:16:04,320
and building up internally.

191
00:16:04,320 --> 00:16:07,320
Something that they don't really delight in.

192
00:16:07,320 --> 00:16:11,320
So the Buddha called the Mara, whether that's actually what they call themselves.

193
00:16:11,320 --> 00:16:13,320
They labeled these angels.

194
00:16:13,320 --> 00:16:21,320
These heavenly creatures or ethereal beings, Mara.

195
00:16:21,320 --> 00:16:25,320
But the evil that we really talk about in the Buddha's really,

196
00:16:25,320 --> 00:16:29,320
I think, focusing on here is the evil of defilement.

197
00:16:29,320 --> 00:16:32,320
So the Buddha talked about ten armies of Mara.

198
00:16:32,320 --> 00:16:38,320
And so here he says Mara and his army, Samara, and its armies.

199
00:16:38,320 --> 00:16:45,320
So they're talking about the evil of defilement and also the evil of suffering.

200
00:16:45,320 --> 00:16:51,320
There are many things in the world that create for us problems

201
00:16:51,320 --> 00:17:01,320
and cause us to be distracted and diverted and trapped in the world.

202
00:17:01,320 --> 00:17:04,320
So the first, of course, central pleasures.

203
00:17:04,320 --> 00:17:09,320
I'll go through the ten armies of Mara because I think that's quite useful.

204
00:17:09,320 --> 00:17:11,320
Central pleasure, this is an army of Mara.

205
00:17:11,320 --> 00:17:15,320
It gets us so trapped and caught up.

206
00:17:15,320 --> 00:17:18,320
Think about how much of our life is spent.

207
00:17:18,320 --> 00:17:24,320
How much of our energy is spent pursuing sensuality.

208
00:17:24,320 --> 00:17:29,320
How much of our economy and our activity as a human species is caught up

209
00:17:29,320 --> 00:17:35,320
and just trying to see beautiful things here, beautiful sounds.

210
00:17:35,320 --> 00:17:39,320
Take what we call the music industry.

211
00:17:39,320 --> 00:17:45,320
So it's a huge industry and how much money goes into it and time and energy.

212
00:17:45,320 --> 00:17:51,320
I walk around university and at least half of the students

213
00:17:51,320 --> 00:17:56,320
walking around have these earbuds in their ear now.

214
00:17:56,320 --> 00:18:01,320
That's such a thing where we're constantly trying to hear a beautiful sound

215
00:18:01,320 --> 00:18:06,320
to really define it very simply.

216
00:18:06,320 --> 00:18:08,320
It's just trying to hear a beautiful sound.

217
00:18:08,320 --> 00:18:12,320
You put so much more into it and turn it into such a thing.

218
00:18:12,320 --> 00:18:17,320
What type of music you listen to and how the music calls to you

219
00:18:17,320 --> 00:18:20,320
and all these descriptions of it.

220
00:18:20,320 --> 00:18:23,320
Ultimately, the core of it.

221
00:18:23,320 --> 00:18:27,320
There could be a message in certain singing and so on,

222
00:18:27,320 --> 00:18:33,320
but the core of it is our desire to hear something pleasant.

223
00:18:33,320 --> 00:18:35,320
Smells, tastes.

224
00:18:35,320 --> 00:18:38,320
How much energy goes into food.

225
00:18:38,320 --> 00:18:44,320
How obsessed we are or good to food.

226
00:18:44,320 --> 00:18:52,320
How much of our lives has caught up in flavors and feelings.

227
00:18:52,320 --> 00:18:57,320
So pleasant feelings, warm, cool.

228
00:18:57,320 --> 00:19:00,320
How obsessed we are with the cold when it's cold,

229
00:19:00,320 --> 00:19:04,320
we complain when it's hot, we complain.

230
00:19:04,320 --> 00:19:12,320
Nice, pleasant touch of soft and hard.

231
00:19:12,320 --> 00:19:21,320
The feelings that come from sexual and sensual touch and that sort of thing.

232
00:19:21,320 --> 00:19:22,320
It's the first one.

233
00:19:22,320 --> 00:19:24,320
The second one is discontent.

234
00:19:24,320 --> 00:19:31,320
So there are armies and there are sort of things that contribute to our

235
00:19:31,320 --> 00:19:35,320
defilement, to our impure state, to the state of mind,

236
00:19:35,320 --> 00:19:38,320
states of mind that cause us suffering.

237
00:19:38,320 --> 00:19:44,320
We're not trying to be, it's not about sin or evil or so on.

238
00:19:44,320 --> 00:19:48,320
It's particularly about things that hurt us.

239
00:19:48,320 --> 00:19:52,320
Discontent is a big one.

240
00:19:52,320 --> 00:19:54,320
Contentment, the Buddha said, is the greatest game,

241
00:19:54,320 --> 00:19:58,320
and it's discontent that drives us.

242
00:19:58,320 --> 00:20:01,320
We can't sit still, we can't be alone, we can't be,

243
00:20:01,320 --> 00:20:07,320
we can't just be and caught up in so much discontent.

244
00:20:07,320 --> 00:20:10,320
Number three is hunger and thirst.

245
00:20:10,320 --> 00:20:15,320
Hunger and thirst are not usual, not impure.

246
00:20:15,320 --> 00:20:21,320
It's not an impure thing that you need water and food, but it's evil.

247
00:20:21,320 --> 00:20:30,320
It's an ancillary evil in the sense that it drives us to so much stress and diversion.

248
00:20:30,320 --> 00:20:40,320
If we weren't hungry and didn't get thirsty, how much we would save on our

249
00:20:40,320 --> 00:20:46,320
ambition and our drive to attain and obtain.

250
00:20:46,320 --> 00:20:49,320
Number four is craving.

251
00:20:49,320 --> 00:20:51,320
Of course this is a big one.

252
00:20:51,320 --> 00:20:53,320
Really the root of it all.

253
00:20:53,320 --> 00:20:59,320
We want things and it turns out that nothing we want can bring us happiness.

254
00:20:59,320 --> 00:21:05,320
It just brings us more and more wanting.

255
00:21:05,320 --> 00:21:15,320
Now of course the overcoming of all these things is conquering all of these.

256
00:21:15,320 --> 00:21:22,320
There's what we go through in meditation, especially craving.

257
00:21:22,320 --> 00:21:28,320
So we overcome craving by realizing that the things that we crave are not worth craving.

258
00:21:28,320 --> 00:21:31,320
There's no good in them.

259
00:21:31,320 --> 00:21:35,320
Number five is loss and torpor.

260
00:21:35,320 --> 00:21:40,320
We're caught up by this unwieldiness of mind that comes from sleeping a lot.

261
00:21:40,320 --> 00:21:48,320
It comes from living our life in laziness.

262
00:21:48,320 --> 00:21:52,320
Keeps us from being awake, being alert, being alive.

263
00:21:52,320 --> 00:22:00,320
Very much caught up and enslaved by laziness.

264
00:22:00,320 --> 00:22:02,320
Number six is fear.

265
00:22:02,320 --> 00:22:06,320
Of course fear drives us to so much what we call it, what we might call evil.

266
00:22:06,320 --> 00:22:10,320
We might kill or steal or lie or cheat.

267
00:22:10,320 --> 00:22:17,320
Simply out of fear that it prevents you from being at peace.

268
00:22:17,320 --> 00:22:21,320
Fear causes us to do irrational things.

269
00:22:21,320 --> 00:22:23,320
Doubt as well.

270
00:22:23,320 --> 00:22:24,320
Number seven.

271
00:22:24,320 --> 00:22:26,320
Doubt as a big one you might say.

272
00:22:26,320 --> 00:22:32,320
What's wrong without doubt stops you from doing what is right if you doubt something.

273
00:22:32,320 --> 00:22:35,320
It doesn't mean that just because you doubted it's wrong.

274
00:22:35,320 --> 00:22:38,320
Very easy to doubt what is the right thing to do.

275
00:22:38,320 --> 00:22:42,320
What is good for you.

276
00:22:42,320 --> 00:22:46,320
And doubt can also cause us to do evil.

277
00:22:46,320 --> 00:22:54,320
We are unable to find the right answer, crippled by our doubts.

278
00:22:54,320 --> 00:22:57,320
So we do the wrong thing.

279
00:22:57,320 --> 00:23:01,320
Number eight is conceit and ingratitude.

280
00:23:01,320 --> 00:23:13,320
A bunch of things together which is curious conceit is, I guess in this sense,

281
00:23:13,320 --> 00:23:17,320
not being able to appreciate others.

282
00:23:17,320 --> 00:23:20,320
So conceit would be an overappreciation.

283
00:23:20,320 --> 00:23:26,320
Not raising yourself up, but in relation to ingratitude,

284
00:23:26,320 --> 00:23:29,320
I think it means not being able to appreciate others.

285
00:23:29,320 --> 00:23:34,320
So ingratitude is not being able to appreciate what others do for you.

286
00:23:34,320 --> 00:23:38,320
Appreciate the goodness of others.

287
00:23:38,320 --> 00:23:43,320
These are not the core evil, but they're a cause for great evil.

288
00:23:43,320 --> 00:23:47,320
Because if you don't think about the good other people have done to you,

289
00:23:47,320 --> 00:23:53,320
it's easy to then become greedy and well-conceded.

290
00:23:53,320 --> 00:23:57,320
You only think about yourself and you become very much,

291
00:23:57,320 --> 00:24:03,320
it's very easy to then become attached to sensuality

292
00:24:03,320 --> 00:24:11,320
without realizing that the cause and the benefit that other people have given to you.

293
00:24:11,320 --> 00:24:22,320
Number nine is gain, renown, and honor that are falsely received.

294
00:24:22,320 --> 00:24:26,320
So again, it's not wrong to have gain, renown, or honor,

295
00:24:26,320 --> 00:24:30,320
but often we receive them without being worthy of them.

296
00:24:30,320 --> 00:24:36,320
So someone might be very much praised as a teacher,

297
00:24:36,320 --> 00:24:41,320
but it turns out that they're corrupt.

298
00:24:41,320 --> 00:24:44,320
Someone might be praised as a meditator,

299
00:24:44,320 --> 00:24:51,320
but it turns out they're just sleeping all the time.

300
00:24:51,320 --> 00:24:53,320
I'm about to have a gain.

301
00:24:53,320 --> 00:24:56,320
So someone gain renown and honor.

302
00:24:56,320 --> 00:24:59,320
They can all be gained falsely.

303
00:24:59,320 --> 00:25:04,320
You can cheat and manipulate others, steal, and so on.

304
00:25:04,320 --> 00:25:08,320
But gain when it's not, when you didn't work for it,

305
00:25:08,320 --> 00:25:11,320
if you work for it, there's an appreciation of it,

306
00:25:11,320 --> 00:25:17,320
when you meditate on your work for the peace and the happiness that you get from it,

307
00:25:17,320 --> 00:25:19,320
and then people appreciate you.

308
00:25:19,320 --> 00:25:21,320
Well, there's not such a big problem there.

309
00:25:21,320 --> 00:25:28,320
It's not as likely to lead you to become conceited about it.

310
00:25:28,320 --> 00:25:32,320
But when you gain things unwillingly when people appreciate you,

311
00:25:32,320 --> 00:25:35,320
there's a problem for new monks nowadays,

312
00:25:35,320 --> 00:25:38,320
because monks in Asia anyway,

313
00:25:38,320 --> 00:25:45,320
because monks have been, for a long time, treated quite well by lay people.

314
00:25:45,320 --> 00:25:51,320
In some countries, in Thailand, it was like that, especially.

315
00:25:51,320 --> 00:25:56,320
So new monks would often become quite egotistical,

316
00:25:56,320 --> 00:25:59,320
especially westerners who aren't used to it.

317
00:25:59,320 --> 00:26:03,320
And when everyone's esteeming them,

318
00:26:03,320 --> 00:26:07,320
they get their sense, oh, it must be something special.

319
00:26:07,320 --> 00:26:10,320
Without, it's kind of subconscious, really.

320
00:26:10,320 --> 00:26:16,320
There's a story we've been through the story of Badia.

321
00:26:16,320 --> 00:26:19,320
Badia was a man who thought he was enlightened,

322
00:26:19,320 --> 00:26:23,320
because he was shipwrecked, and he lost his clothes,

323
00:26:23,320 --> 00:26:25,320
and so he was naked.

324
00:26:25,320 --> 00:26:27,320
Because he was naked, he found trying to find something,

325
00:26:27,320 --> 00:26:30,320
and I think he found, I don't know, a piece of bark,

326
00:26:30,320 --> 00:26:35,320
or a piece of wood to cover up his private parts.

327
00:26:35,320 --> 00:26:38,320
And people saw him and thought, oh, this must be some kind of saint,

328
00:26:38,320 --> 00:26:40,320
because he goes without clothes.

329
00:26:40,320 --> 00:26:43,320
And so at first he pretended, and he was like, yes, yes.

330
00:26:43,320 --> 00:26:45,320
And they brought him food, and he was like, oh, good.

331
00:26:45,320 --> 00:26:47,320
But eventually he started believing it.

332
00:26:47,320 --> 00:26:51,320
And he had the sense, because of the gain that people were getting it.

333
00:26:51,320 --> 00:27:04,320
So it's a cause for great distraction and delusion.

334
00:27:04,320 --> 00:27:08,320
It can mislead you just by the fact,

335
00:27:08,320 --> 00:27:13,320
you know, when we often look to other people for confirmation

336
00:27:13,320 --> 00:27:16,320
that we're doing the right thing, and if everyone's praising you,

337
00:27:16,320 --> 00:27:19,320
it's easy to start to think that you're on the right track,

338
00:27:19,320 --> 00:27:24,320
but it's quite possible that you're being wrongfully placed.

339
00:27:24,320 --> 00:27:30,320
And number 10 is self-exaltation and disparaging others.

340
00:27:30,320 --> 00:27:34,320
So again, this is really related to conceit,

341
00:27:34,320 --> 00:27:39,320
but it's in the topic of how we relate to others

342
00:27:39,320 --> 00:27:42,320
when we disparage others and hold ourselves up.

343
00:27:42,320 --> 00:27:45,320
It's a very bad habit, obviously.

344
00:27:45,320 --> 00:27:53,320
I mean, this is a evil in itself, but it's a bad habit

345
00:27:53,320 --> 00:27:56,320
that means to other defilement or it becomes pernicious,

346
00:27:56,320 --> 00:28:00,320
because even meditators can have this.

347
00:28:00,320 --> 00:28:03,320
You might say that all 10 of these are things that meditators

348
00:28:03,320 --> 00:28:05,320
particularly have to deal with,

349
00:28:05,320 --> 00:28:08,320
or meditators still have to deal with,

350
00:28:08,320 --> 00:28:11,320
even though they might do away with some of the course ones.

351
00:28:11,320 --> 00:28:16,320
These are things that Mara tries to throw at meditators.

352
00:28:16,320 --> 00:28:23,320
So you begin to gain insight and clarity and peace of mind,

353
00:28:23,320 --> 00:28:25,320
and then you look at the people who don't meditate

354
00:28:25,320 --> 00:28:29,320
and you think, I'm better than all those people, right?

355
00:28:29,320 --> 00:28:32,320
Quite pernicious, and it can really distract you

356
00:28:32,320 --> 00:28:34,320
and get you off track.

357
00:28:34,320 --> 00:28:36,320
It can be a real problem,

358
00:28:36,320 --> 00:28:40,320
especially for monks who are often held in a higher regard,

359
00:28:40,320 --> 00:28:42,320
because they're held in a higher regard.

360
00:28:42,320 --> 00:28:46,320
They find it hard often to receive criticism.

361
00:28:46,320 --> 00:28:49,320
Maybe for you, I'm a monk.

362
00:28:49,320 --> 00:28:52,320
Meditators, you can have that as well, right?

363
00:28:52,320 --> 00:28:56,320
As a meditator, someone who doesn't meditate criticizes you.

364
00:28:56,320 --> 00:29:01,320
It's a very hard pill or bitter pill to swallow.

365
00:29:01,320 --> 00:29:11,320
Someone who's not meditating points out how unmindfully you are.

366
00:29:11,320 --> 00:29:13,320
These are the 10 armies of Mara there.

367
00:29:13,320 --> 00:29:16,320
You could say they're just part of the larger picture

368
00:29:16,320 --> 00:29:17,320
and to follow them.

369
00:29:17,320 --> 00:29:20,320
That's a good list.

370
00:29:20,320 --> 00:29:24,320
So Mara himself really just refers to all of the stuff inside

371
00:29:24,320 --> 00:29:26,320
and he's even pure.

372
00:29:26,320 --> 00:29:28,320
And it's even pure when we consider it impure

373
00:29:28,320 --> 00:29:31,320
because it leads us to suffering.

374
00:29:31,320 --> 00:29:34,320
It's not so much what it does to other people

375
00:29:34,320 --> 00:29:36,320
because they should be able to take care of themselves.

376
00:29:36,320 --> 00:29:40,320
They are responsible for their own happiness and suffering.

377
00:29:40,320 --> 00:29:45,320
But if you've heard other people, if you relate to other people

378
00:29:45,320 --> 00:29:49,320
and clingy or a stressful way,

379
00:29:49,320 --> 00:29:51,320
you're just going to create bad habits.

380
00:29:51,320 --> 00:29:55,320
And you're going to create a sadness,

381
00:29:55,320 --> 00:30:00,320
a sense of low self esteem or guilt.

382
00:30:00,320 --> 00:30:01,320
Right?

383
00:30:01,320 --> 00:30:03,320
Guilt is the word.

384
00:30:03,320 --> 00:30:07,320
If you'll guilty afterwards.

385
00:30:07,320 --> 00:30:12,320
If you'll bad about it, then so it will cause your stress and suffering.

386
00:30:12,320 --> 00:30:15,320
And as well, if it has nothing, even if it has nothing to do with other people,

387
00:30:15,320 --> 00:30:18,320
if you just sit around desiring this or desiring that

388
00:30:18,320 --> 00:30:22,320
or if you live your life craving this, craving that,

389
00:30:22,320 --> 00:30:26,320
chasing this, chasing that,

390
00:30:26,320 --> 00:30:27,320
you'll never be at peace.

391
00:30:27,320 --> 00:30:32,320
You'll always be wanting and never be free from suffering.

392
00:30:32,320 --> 00:30:36,320
And so the practice of meditation is this is the path.

393
00:30:36,320 --> 00:30:40,320
This is the way one gets out of the world.

394
00:30:40,320 --> 00:30:44,320
The world is all of the things that we create

395
00:30:44,320 --> 00:30:50,320
and get caught up in when you stop being caught up,

396
00:30:50,320 --> 00:30:53,320
chasing after you free yourself from the world,

397
00:30:53,320 --> 00:30:59,320
you go out of the world and leave behind the world.

398
00:30:59,320 --> 00:31:02,320
That's the real teaching, I think, in this first.

399
00:31:02,320 --> 00:31:05,320
So that's the demo path I've heard tonight.

400
00:31:05,320 --> 00:31:07,320
Thank you all for tuning in.

401
00:31:07,320 --> 00:31:24,320
I wish you all the best.

