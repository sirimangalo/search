Hello, good evening.
So today I'll be answering a question about the Satipatana Sutta.
It's the discourse on mindfulness.
In the Satipatana Sutta it's broken up into sections and at the end of each section there's
a repeated phrase and the Buddha says several very important things.
It's important that we don't gloss over or we don't skip over what the Buddha says
about the practice of mindfulness.
At the end of each section so we have the four foundations of mindfulness, body, feelings,
mind, dhamma, kaya, vidana, jita, dhamma.
And then kaya and dhamma are split up into sections.
And one part of the ending of each section says that, yeah, at the beginning actually
words says itya jita dhamma, kaya, kaya, nupasi, and so on.
Adjita dhamma means internally.
Thus one is mindful internally and then he says bhiddhava or externally and then he says
internally or externally or internally and externally.
And so the question came when it's been asked before, what is meant by internally and
externally?
How do you be mindful of something internally and how do you be mindful of something externally?
What's the difference?
Most puzzling is how you would be mindful of something externally.
Based on our practice, it's very much internal.
Based on most people's understanding of meditation, it's the inner way, the inward path.
So the idea that you could somehow meditate on something outside of yourself or be mindful
at least of something outside of yourself is a bit confusing.
So I can give three possible answers to how one can practice external mindfulness.
And this is based a little bit on the commentaries, a little bit on different sutas and
a little bit on my own understanding of the Buddha's teaching.
The first way that one can practice mindfulness externally is in the practice of Yipasana.
The second way someone can practice mindfulness of something externally is in the practice
of samata and the third way that one can practice mindfulness of something that is externally
is very in the practice of Yipasana.
The first one in preparation for Vipasana is what I meant.
So in preparation for Vipasana, in the practice of samata, or you could even say in preliminary
practice of samata and in the actual practice of Yipasana.
So let's play now this work.
In preparation for the practice of Yipasana or in conjunction, what I meant by in the practice
of samata is in the practice of Yipasana is as a support for one's practice of Yipasana.
And this is what the sub-commentary actually says about this.
It makes a note that actually this external idea isn't actually meditation at all.
Or it says it can't lead to tranquility of mind, it can't lead to the focus and concentration
that's needed to develop insight.
But it's something that's useful in insight in a way that a lot of things are in the
sutas and just in the practical reality, along our paths to enlightenment.
So we have to differentiate between what is meditation, what is not meditation in regards
in this regards.
And by the sub-commentaries explanation, there's this idea that our reflections on external
realities can often be quite valuable in our own practice of mindfulness.
Even though it's not mindfulness in the same way, it's mindfulness in a way that allows
us to understand and appreciate reality better and more clearly.
So when you focus on someone else's body, you can focus on and each section has this.
So you can focus on their breath.
You can focus on their postures when they're walking, so watch someone else walk as
they're walking walking.
And you can focus on, of course, a dead body or someone else's parts of someone else's
body, their hair and their nails and their teeth and their skin and so on.
And some of this might sound quite weird, like why would you focus on someone else's breathing,
why would you focus on someone else's walking?
My idea in regards to this is that given that each, the end of each section is the same
for each section and yet each section is a different sort of practice that in some ways
one wants to say that the words are only meant to describe, and they do this quite well,
to describe the general idea of how one is mindful.
And so at the end of each section, he says, one is mindful externally, internally.
Once he's the beginning, the arising and the ceasing of things, one sees that there is
just the body, there is just the feelings, they are what they are.
And so it describes how one is mindful, but it may very well be that some sections are much
more useful when practiced externally, and that's granted that.
It's a good point that some of the objects of meditation are internal, which is mostly
what we do, of course, walking, walking, it really only makes sense if you're paying attention
to your own movement because you can't feel someone else's walking.
With the breath, when there are different kinds of breath meditation, with our breath meditation,
it doesn't make sense that you should focus on someone else's rising and falling because
you won't actually experience rising and falling.
You won't experience the tension, you won't experience the fluidity.
But in regarding a corpse, which is a part of the suta, it can be quite useful.
Useful, let's say, for Vipasana, because if you're looking at someone else's dead body
and bloated body and then flesh being rotted, maggots coming out of it, don't say that
that's just a concept, that's quite powerful as a reflection, it sets a very good tone
for your own practice, it leads to a revolution, makes you realize that actually this body
is just like that, hey, there's bones in this body as well and so on.
And even mindfulness of the parts of the body, if you look at someone else's, suppose
there's someone who you're very much attracted to, you can focus on their hair and we
think, oh, their hair is quite beautiful, we're going to hand some, but then you study
their hair in a Buddhist way and you think, oh, yes, this hair of theirs is planted like
rice in their scalp, which is not just mud and water, knowing the scalp there's blood
and there's oil and there's flakes of skin and so on.
And the hair doesn't smell like rice does, no, the hair smells like body, it smells
putrid after some time.
The nails, they turn yellow, they get dirt and skin and particles and all sorts of stuff
underneath them, they get chipped and broken and so on, so you look at someone's beautiful
nails and then you think about their nails in a different way, it can be very useful,
not just for some of the meditation but also for repass in the meditation, it's not
rebass in it, it's not mindfulness in the way that we narrowly understand it, but there's
a mindfulness there, you're contemplating, you're reflecting in a way that's going to be
quite useful and this is important to understand because our practice cannot be my
opic in the way of just, you know, just dusting up a hot tub, just focusing on we pass
and not seeing clearly, we have to also be aware of things in our life and relate to things
in our lives and reflect, we have to make a clear delineation between actual mindfulness
practice and mindful reflection, you know, where you say, Ajiram, what the yankakayo,
this before long this body will lie on the earth just like that body there, that's
a reflection, it's not actually being mindful of, you know, arising and ceasing phenomena,
but it's an external reflection, focusing on the body and seeing the body dead and focusing
on the body and seeing it ugly and so on.
These things can be a great support for repass in a meditation and can actually be a real
catalyst for true insight, there are cases in the sutas, there's a case of a monk who
saw a woman walking down the street, but he had been so fixed on the contemplation of
bones that all he saw was her teeth when she smiled when she laughed and when he saw
her teeth, he just saw her, he reflected and he absorbed into that and saw this woman
as just a second bones walking down the street and she walked past him and he kept walking
on arms around and a man came and he said, hey have you seen my wife, it's chasing after
some reason and he says, oh, I just saw a bag of bones, or I just saw a pile of bones,
and he said, because that's all he actually saw and this monk when that happened, he
became enlightened just by seeing the woman's teeth.
Some people might say, how is it possible? I've been asked this several times, how is
it possible that this woman's teeth he became enlightened? No, it's not directly possible
that that should be enlightened and enlightening experience, but it's the reflection. It sets
you in the mood, potentially perfect mood of seeing things just as they are, just the
last straw that you needed to see clearly. So we have to separate between meditation
and not meditation, but we also have to see that many times there's going to be some
interaction with our non-meditative qualities. The sub-commentary seems to be saying that
it's for the purpose of supporting insight.
But external can very much be a support or a preliminary for summit to meditation. It often
quite often is. In classical summit to meditation, you would focus on an external object
like a disc or something. In the Saty bitanasu, you would be mindful of a dead body for
the purpose of gaining this fixed concentration on the idea of the dead body. You would
focus on one of the parts of the body and you would come to be absorbed in that hair
or teeth or so on. And it could be an object of meditation that would lead to tranquility.
But the third way that this external idea of external practice could be thought of as
could be understood is in the actual practice of insight meditation. What I mean by this
is externally and internally, if you're going to look at the arising and ceasing of things,
then it has to be based on experience. And so this is the clue because externally and
internally, as we normally think of them, have nothing to do with experience. They have
to do with entities. And so understanding the difference between entities and experiences
is important. So it's important to understand how this can be a practice of true mindfulness.
So external entities, you have people and places and things and you see them. You see the
person walking. Even if you think of it just as walking, it's a thing. And it's not actually
an experience of walking. It's an experience of seeing. So if you talk about something
external, it's an experience of seeing. It's an experience of conceiving. But in every
experience, there is, in one way, there is an external and an internal. And that is in
regards to the senses. So our practice of mindfulness involves external stimuli and internal
sense basis and the contact between those two. No, you don't see anything unless there's
light touching the eye. You don't hear anything unless you're sound touching the ear.
And so the sound is external. The light is external. And these are, you know, it seems
quite simple. But it describes a framework that is unlike how we normally perceive reality.
Our normal perception of reality is people places and things. So understanding and looking
at reality in terms of sights and the eye, sounds and the ear, smells and the nose, tastes
and the tongue, feelings and the body and thoughts and the mind. It's something that the
Buddha taught again and again and again, quite often. It's one of the most often taught
teachings on the six senses, internal and external. And they're actually called that.
There's the internal sense basis and the external sense objects. And so there's a pair
of them. And so sometimes, and this is really very close to what the language of the Satyabhata
understood. Sometimes, when sees the object and the light touching the eye, as it experiences
that, sometimes someone is aware of it from internally of the eye seeing. It's just a perspective
same with the, so sometimes someone is aware of the fact that the ear is receiving the sound.
Sometimes sees it more as the sound that touches the ear. But understanding and seeing
reality in terms of this framework gets you so much closer to what's really happening.
It gets you to a state of existence, a state of being, a state of interaction with the
world, that is perfect, that is clear, that is free from any kind of judgment, partiality
or ignorance. It brings us closer to reality, brings us closer to what's really going on.
Because our idea of seeing someone, seeing something is an abstraction. It's based first
on the light touching the eye and second on the processing of it. And it leads third to
the judgment and so on. Because experience is momentary, our sights and sounds and smells,
they come and they go. And because they are simple, they are basic, they are fundamental
building blocks. There's no possibility, there's no potential for clinging to them.
When you're aware, when you're observing from the point of view of light touching the
eye, there's no room for any attachment. And so this is, I think, the best way, it may
not be what the Buddha actually meant when he said or what the words in each of these
sections actually means this specific teaching. But it is a good opportunity for us to look
at an in-repassing a meditation based on the four foundations of mindfulness. To look
at how we perceive reality, how reality presents itself to us. And that is through these
six senses. So when we say to ourselves, seeing, seeing, there's actually three things.
There's the light, there's the eye, and there's the consciousness. Because even if there's
light touching the eye, if your mind is somewhere else, you can't see. Even when they're
sound touching the ear, when there's sound waves touching the ear, sometimes you're so
focused, you don't even hear someone when they call you. Quite often we don't notice
smells and tastes and feelings until they become unpleasant and then we realize when they
become acute. So in reality, there are these three things. And together this is contact.
And the Buddha talked about these again and again. And quite clear that all he was trying
to do was describe reality so that we could know it when we saw it. And so that we could
change the way we look at things. Like if someone looking at your car, for example, and
they want to show you, they want to point out what's wrong with it so you can fix it.
To show you a new way of looking at things. The way we look at things is problematic.
It's imprecise. So it's not that seeing people in places and things is wrong. It's just
that it's unable to provide the clarity that we need to change. If and when we become
free from all wrongness, all imperfections in our mind, all bad habits, then we would
be fine to focus on abstract entities. But because they're abstract, they leave room for
misunderstanding. And when we have misunderstanding, this is where it gives rise to bad habits,
bad ideas. So describing and illustrating and laying out all of the building blocks of
reality and how the mind interacts with them in terms of the six senses internally and externally
is a very good example. It's allows us to become closer to reality. So there you go. There's
three ways you could understand it. One, it's useful to reflect on external things. Two, you
take the external object as a basis for sound for tranquility. And three, it's nothing to do
with external objects at all. It has to do with external aspects of experience. And that
may very well be what the put is referring to here. Either way, all three are valid. And that's
an answer to the question. So thank you for listening. I wish you all the best.
