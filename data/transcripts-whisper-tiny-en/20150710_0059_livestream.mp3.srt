1
00:00:00,000 --> 00:00:06,500
Okay, good evening.

2
00:00:06,500 --> 00:00:18,600
We're encusting mine from Stonyfrick Ontario July 9th, 2015, so we're following along with

3
00:00:18,600 --> 00:00:30,900
this book, Bunderwech, or words of Buddha, and by Bender and Bodhamika, 365 days of

4
00:00:30,900 --> 00:00:31,900
Dublin.

5
00:00:31,900 --> 00:00:42,600
And so we're on number 190, which is July 9th, and it has to do with sickness, mental

6
00:00:42,600 --> 00:00:44,600
sickness and physical sickness.

7
00:00:44,600 --> 00:00:50,600
There's lots of people in the world who are without physical sickness.

8
00:00:50,600 --> 00:00:56,600
There are lots of people who think that they are free from mental sickness.

9
00:00:56,600 --> 00:01:04,600
According to the Buddha, it's rare to find someone who actually is free from mental sickness.

10
00:01:04,600 --> 00:01:13,600
Mental sickness means that it means the point is that it means different things to different

11
00:01:13,600 --> 00:01:14,600
people.

12
00:01:14,600 --> 00:01:33,600
Most people think of mental illness as extreme states of abnormality that prevent one from functioning

13
00:01:33,600 --> 00:01:40,600
anxiety that seriously hamper ones who go to the function.

14
00:01:40,600 --> 00:01:59,600
This isn't the Buddha's scope on what is, and there's a mental illness to the Buddha.

15
00:01:59,600 --> 00:02:09,600
This is anything that any wrongness of mind.

16
00:02:09,600 --> 00:02:16,600
So a person who is selfishness or mental illness, a person who is arrogant, that arrogance

17
00:02:16,600 --> 00:02:23,600
is a mental illness.

18
00:02:23,600 --> 00:02:28,600
A person who is addicted to something that addiction is a mental illness.

19
00:02:28,600 --> 00:02:38,600
I've had people take issue to this kind of outlook.

20
00:02:38,600 --> 00:02:47,600
There's the idea that calling something an illness is putting a stigma or negative

21
00:02:47,600 --> 00:02:59,600
connotation, but I don't think that's really a valid argument because it's true.

22
00:02:59,600 --> 00:03:04,600
It's a fact that these things are problems.

23
00:03:04,600 --> 00:03:09,600
Calling the illnesses just calls them what they are.

24
00:03:09,600 --> 00:03:19,600
The whole is an illness.

25
00:03:19,600 --> 00:03:28,600
It's not terminal, but anything that affects the body.

26
00:03:28,600 --> 00:03:29,600
It's kind of in this.

27
00:03:29,600 --> 00:03:35,600
So we say the same thing about the mind, the mind, anything that affects the mind negatively.

28
00:03:35,600 --> 00:03:38,600
It's an illness.

29
00:03:38,600 --> 00:03:40,600
Some people think that they're afraid for mental illness.

30
00:03:40,600 --> 00:03:45,600
Most people think they're good people.

31
00:03:45,600 --> 00:03:54,600
This is mainly due to our low level of mindfulness.

32
00:03:54,600 --> 00:04:10,600
We aren't able to see the finer defilements in our minds.

33
00:04:10,600 --> 00:04:15,600
We aren't able to notice.

34
00:04:15,600 --> 00:04:28,600
We don't know our own minds.

35
00:04:28,600 --> 00:04:31,600
And we're quick to forget.

36
00:04:31,600 --> 00:04:56,600
So we are all that cognizant of our faults in our state of our mind.

37
00:04:56,600 --> 00:05:03,600
The Buddha actually decides this passage, which is actually the words of the Buddha.

38
00:05:03,600 --> 00:05:18,600
The Buddha further delimited illness as coming from four root causes.

39
00:05:18,600 --> 00:05:28,600
So we have four causes of illness.

40
00:05:28,600 --> 00:05:31,600
Illness can be caused by food.

41
00:05:31,600 --> 00:05:34,600
Illness can be caused by weather.

42
00:05:34,600 --> 00:05:41,600
Illness can be caused by karma.

43
00:05:41,600 --> 00:05:50,600
These more are considered to be the root causes of illness.

44
00:05:50,600 --> 00:05:53,600
So, and they should be taken seriously.

45
00:05:53,600 --> 00:06:02,600
The Buddha also said quite famously, among those who know,

46
00:06:02,600 --> 00:06:12,600
the Buddha said, Rokaya, Paramala, but freedom from illness is the greatest.

47
00:06:12,600 --> 00:06:24,600
Laba, gain, greatest gain, greatest possession.

48
00:06:24,600 --> 00:06:45,600
So, it's a serious subject, and even physically speaking, we shouldn't discount the benefits of being free from illness, from physical illness.

49
00:06:45,600 --> 00:07:01,600
If our body is not in tip-top shape, it's very difficult for us to do good for ourselves that alone ever.

50
00:07:01,600 --> 00:07:14,600
But it happens that people ignore the mental illness entirely and focus solely on physical wellbeing.

51
00:07:14,600 --> 00:07:25,600
So, there are people who are obsessed with the diets and physical regimen, physical exercise.

52
00:07:25,600 --> 00:07:43,600
And those minds are a mess who are arrogant and conceited and proud for who are filled with self-hatred that they're image, trying to constantly fix their image.

53
00:07:43,600 --> 00:07:49,600
They're image, work on their image, out of low self-esteem.

54
00:07:49,600 --> 00:07:53,600
They're two kinds of people, people with high self-esteem, people with low self-esteem.

55
00:07:53,600 --> 00:07:56,600
Both are very dangerous.

56
00:07:56,600 --> 00:08:18,600
They're very destructive, both through self endeavors.

57
00:08:18,600 --> 00:08:25,600
And so, we become proud of our physical health, people can think that life is good.

58
00:08:25,600 --> 00:08:30,600
There's no need to practice meditation because nothing's wrong.

59
00:08:30,600 --> 00:08:36,600
They have everything I could ever want, and my life is good, it's kind of thing.

60
00:08:36,600 --> 00:09:02,600
So, they're just inclined to practice meditation in this world.

61
00:09:02,600 --> 00:09:06,600
So, we have to look at physical and mental illness.

62
00:09:06,600 --> 00:09:16,600
When we look at these four types, four causes of illness, we can get a clear sense of what is meant by illness.

63
00:09:16,600 --> 00:09:22,600
So, we have, from food, illness can come from our diet, improper diet.

64
00:09:22,600 --> 00:09:24,600
So, diet is important.

65
00:09:24,600 --> 00:09:38,600
It seems to have accepted that the praised one, praised the monk for actually separating out his arms food into what is healthy and unhealthy.

66
00:09:38,600 --> 00:09:41,600
Just sort of a backhanded compliment.

67
00:09:41,600 --> 00:09:53,600
He actually used it as a chance to talk about this monk's past lives when he knew the difference between what was good and what was bad for his mind.

68
00:09:53,600 --> 00:10:03,600
So, again, we have the comparison, the contrast between physical and mental illness.

69
00:10:03,600 --> 00:10:10,600
The monks were complaining about this monk who was concerned about his physical health.

70
00:10:10,600 --> 00:10:12,600
They said, hey, what's he doing?

71
00:10:12,600 --> 00:10:17,600
But he said, no, this is actually, this monk is just like that.

72
00:10:17,600 --> 00:10:27,600
It's good for him, good for him that he knows what is.

73
00:10:27,600 --> 00:10:55,600
He also knew, and he knew it was good for his mind and it was good for his body.

74
00:10:55,600 --> 00:10:58,600
So, yeah, we have to be careful if we eat.

75
00:10:58,600 --> 00:11:01,600
We have been for meditation, for spiritual development.

76
00:11:01,600 --> 00:11:06,600
Food can affect your practice if you eat too much, if you eat too little.

77
00:11:06,600 --> 00:11:12,600
Once had a man who came to do a course with us, he wanted to live off fruit for his whole course.

78
00:11:12,600 --> 00:11:20,600
They couldn't, probably not a good idea, but not really against the rules, so long as you're eating.

79
00:11:20,600 --> 00:11:27,600
He tried to discourage him, but he would be discouraged.

80
00:11:27,600 --> 00:11:34,600
Near the end of the course, he came to me and he had sprained something.

81
00:11:34,600 --> 00:11:41,600
He had sprained his name, and he realized that he had gone two weeks or something with only eating fruit.

82
00:11:41,600 --> 00:11:46,600
Because of the walking meditation that we do, he wasn't able to continue.

83
00:11:46,600 --> 00:11:54,600
He decided, well, maybe it's a good idea if I actually eat something.

84
00:11:54,600 --> 00:12:01,600
So, meditation, you have to be, you have to take care of your body, you won't let you continue,

85
00:12:01,600 --> 00:12:17,600
you're not a tip-top shape, a good shape.

86
00:12:17,600 --> 00:12:22,600
Also, if you eat too much, of course, let me see tired and make the tip of the practice.

87
00:12:22,600 --> 00:12:30,600
Of course, this is only physical, and it's conducive to smooth practice, but not necessarily.

88
00:12:30,600 --> 00:12:36,600
People who are sick, people who are crippled, people who have any kind of disability can still practice,

89
00:12:36,600 --> 00:12:39,600
not to say that they can.

90
00:12:39,600 --> 00:12:50,600
But the ideal situation is to be able to walk and to have a comfortable, good digestion and all this.

91
00:12:50,600 --> 00:13:05,600
But these are the types of sickness that come to include sicknesses like heart disease and diabetes.

92
00:13:05,600 --> 00:13:32,600
This is the main meditation, it's a spiritual practice.

93
00:13:32,600 --> 00:13:48,600
Let me have sickness that comes from...

94
00:13:48,600 --> 00:14:04,600
This is the environment, this is viruses, radiation, poisoning, food poisoning, etc.

95
00:14:04,600 --> 00:14:11,600
Not food poisoning, yeah, food poisoning, yes.

96
00:14:11,600 --> 00:14:27,600
Certain types of cancer, sunburn is kind of thing, poison ivy, all that is environmental.

97
00:14:27,600 --> 00:14:33,600
You also have to be careful, the mood is clear that you don't just ignore your environment and let yourself suffering.

98
00:14:33,600 --> 00:14:54,600
You have to take care of your body and part of this is avoiding dangerous situations, situations dangerous to the body, problematic for the body.

99
00:14:54,600 --> 00:15:05,600
So these two are physical, food and food and environment.

100
00:15:05,600 --> 00:15:11,600
Now the other two, these are the ones that are more mental, karma.

101
00:15:11,600 --> 00:15:16,600
Sickness that comes from karma, karma of course is mental.

102
00:15:16,600 --> 00:15:30,600
We are good in the bad deeds, in this case bad deeds that we've done, bad deeds that we've done will lead us to get sick in the cause of sickness in the body.

103
00:15:30,600 --> 00:15:54,600
And we just to be negligent.

104
00:15:54,600 --> 00:16:03,600
So that's the answer. It's often met with a great amount of disgust and opposition.

105
00:16:03,600 --> 00:16:15,600
Thinking this is a horrible thing to say this, no karma was never promoted as being a positive thing or something that we...

106
00:16:15,600 --> 00:16:21,600
People would leap at thinking, oh, what a great system? No, karma is scary and awful.

107
00:16:21,600 --> 00:16:32,600
It's awful that people have to suffer further. I'm good in bad deeds, suffer further in bad deeds, it's not a good thing.

108
00:16:32,600 --> 00:16:47,600
But whatever else you say about it, it is just and provides meaning, provides answers to why.

109
00:16:47,600 --> 00:17:04,600
People who wonder why, well, if there's a logical reason, like we don't ask why a convict goes to prison.

110
00:17:04,600 --> 00:17:24,600
We don't ask why people who hurt others, wind up cultivating enemies and peace and retribution.

111
00:17:24,600 --> 00:17:28,600
We understand that there's cause and effect.

112
00:17:28,600 --> 00:17:36,600
So karma is just part of that and if we can accept that our sicknesses are often caused by karma, cancer.

113
00:17:36,600 --> 00:17:43,600
A lot of unexplainable cancer seems to be likely, carnically based.

114
00:17:43,600 --> 00:17:55,600
People who are born with disabilities, think it's safe to assume that a lot of this is karma if you're at all.

115
00:17:55,600 --> 00:18:18,600
This art gives us some good background why we are the way we are and why others are the way they are.

116
00:18:18,600 --> 00:18:25,600
And the fourth one is a sickness that comes from the mind, so the mind can cause sickness.

117
00:18:25,600 --> 00:18:33,600
This is the one that we're focusing most on in meditation, although the karma one you could say is a big part of our focus.

118
00:18:33,600 --> 00:18:37,600
We're more concerned with the state of mind.

119
00:18:37,600 --> 00:18:48,600
The mind makes us sick, simply having certain states of mind makes the body sick when we're angry our bodies become inefficient, greedy.

120
00:18:48,600 --> 00:18:58,600
We neglect our bodies, deluded our bodies stiffen up and have a hard time functioning.

121
00:18:58,600 --> 00:19:08,600
The state of mind is a cause for direct cause for illness.

122
00:19:08,600 --> 00:19:16,600
Karma is more focused on acts that we perform with body and speech or even thought.

123
00:19:16,600 --> 00:19:26,600
The actual mental state, the anger and the greed and the delusion in our mind is cause physical sickness.

124
00:19:26,600 --> 00:19:30,600
And they are of themselves sickness.

125
00:19:30,600 --> 00:19:39,600
These are what the Buddha is talking about in this verse from the book that we're going through, mental illness.

126
00:19:39,600 --> 00:19:52,600
I'm trying to find people who are afraid from this because when we come right down to it, mental illness is any state of upset in the mind.

127
00:19:52,600 --> 00:19:58,600
It's only a matter of degrees.

128
00:19:58,600 --> 00:20:01,600
Some of it is such a severe degree, of course.

129
00:20:01,600 --> 00:20:09,600
It is organic in this sense that the brain is damaged and we aren't able to fix it in this life.

130
00:20:09,600 --> 00:20:12,600
But that's not categorically different.

131
00:20:12,600 --> 00:20:19,600
It doesn't have to be considered categorically different from milder forms of illness.

132
00:20:19,600 --> 00:20:26,600
It's just so extreme that it's manifested itself in the physical realm.

133
00:20:26,600 --> 00:20:39,600
All illness can be cured of a time just often takes more than one lifetime.

134
00:20:39,600 --> 00:20:46,600
Only those who are afraid from the violence, that's the Buddha's end, those are the people who are able to free themselves.

135
00:20:46,600 --> 00:20:50,600
They've been able to be free from mental illness.

136
00:20:50,600 --> 00:20:55,600
That's what we're in for.

137
00:20:55,600 --> 00:20:58,600
There's some food for that.

138
00:20:58,600 --> 00:21:03,600
Mental illness.

139
00:21:03,600 --> 00:21:05,600
Don't physical illness.

140
00:21:05,600 --> 00:21:11,600
Thank you for tuning in, which will be a long bit of practice.

141
00:21:11,600 --> 00:21:18,600
Thank you very much.

