1
00:00:00,000 --> 00:00:07,520
I have a lot of fear inside me. I feel inferior and superior to others at the same time and I cannot make

2
00:00:07,520 --> 00:00:14,080
friends easily because I feel everyone criticizes me. I cannot feel intimacy with others. Does meditating help to

3
00:00:14,080 --> 00:00:22,720
diminish my ego? Thank you. Yes, meditation does help to diminish your ego. You don't have fear, you don't feel

4
00:00:22,720 --> 00:00:28,960
inferior and superior. It's not that you can't make friends easily and you don't feel that everybody

5
00:00:28,960 --> 00:00:35,360
criticizes me. It's not that you cannot feel intimacy with others. What's happening is fear arises.

6
00:00:36,480 --> 00:00:41,760
Fear arises, you identify with it, you think I am afraid and therefore, oh my gosh, I've got a

7
00:00:41,760 --> 00:00:46,720
problem. Now, if you just saw it as fear, you'd be able to watch it arise and cease and you'd see,

8
00:00:46,720 --> 00:00:52,960
oh, it came by itself and it disappeared by itself. It wasn't really mine at all and you feel

9
00:00:52,960 --> 00:01:03,840
quite free because it's like looking at a blazing raging fire from afar. It's hot and that's bad but

10
00:01:03,840 --> 00:01:12,320
I'm not standing in it. You can't feel inferior and superior to someone at the same time that's

11
00:01:12,320 --> 00:01:17,360
impossible but at certain times feelings of superiority arise, at certain time feelings of

12
00:01:17,360 --> 00:01:26,160
inferior or derives. Now, these are a product of a sense of attachment to self. It's called

13
00:01:26,160 --> 00:01:33,200
conceit and it's unwholesome but it's not something that you should get upset about. It's

14
00:01:33,200 --> 00:01:40,080
something that you should see clearly and see the uselessness of. See, that is to your detriment

15
00:01:40,080 --> 00:01:46,880
to think such thing. One thing to feel intimacy is also a problem so the desire to feel

16
00:01:46,880 --> 00:01:55,440
intimacy, you should watch when that arises and really it's misleading to think that we can

17
00:01:55,440 --> 00:02:02,320
never be intimate with anyone. We're always an island. You never share other people's thoughts,

18
00:02:02,320 --> 00:02:08,160
really, you never share their space. You just get really really close. That's the best you can hope

19
00:02:08,160 --> 00:02:15,120
but you're never in someone else's experience. We're not halves of a whole or complete beings.

20
00:02:15,120 --> 00:02:27,440
This we can do is knock. Don't worry about intimacy. Meditation will help. The whole question

21
00:02:27,440 --> 00:02:32,640
is wrong and it's right. I'm not really picking on you but I want to help you to see that it's wrong

22
00:02:32,640 --> 00:02:35,840
because this is the kind of question anyone would ask and other people have asked.

23
00:02:36,960 --> 00:02:41,760
But it's not my ego. You've already made a mistake there. You can't ask someone,

24
00:02:41,760 --> 00:02:49,600
how do I diminish my ego? Does this help diminish my ego? The fact that you say my ego is

25
00:02:49,600 --> 00:02:53,920
our is egotistical. It's a funny way of phrasing.

26
00:02:57,040 --> 00:03:01,360
And semantically it's not a problem but it hints to me that you do have

27
00:03:01,360 --> 00:03:06,880
ego problems like most people do in the sense. Not that you're egotistical but in the sense

28
00:03:06,880 --> 00:03:13,680
that you identify with things. You think of things as me as mine. And it's not your ego. In fact,

29
00:03:13,680 --> 00:03:20,240
there is no ego whatsoever. You go as just a concept that we use to talk about

30
00:03:21,520 --> 00:03:29,920
the set of experiences that arise, that are associated with identification and comparison and

31
00:03:29,920 --> 00:03:39,920
and view of self.

