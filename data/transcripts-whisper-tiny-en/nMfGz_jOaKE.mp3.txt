I have spent some time in a Vashnava monastery, better known as the Hare Krsna's, and their
scriptures say that the Buddha is an incarnation of God or Krsna.
Do you think that this was a tactic to reduce people converting from Hinduism to Buddhism?
Hmm, I don't think so.
Because actually this is more a Hindu belief than a Buddhist belief.
So how could they have gone people to become Buddhists or Hindus to become Buddhists?
No, it's just to reduce people, to stop people from converting.
Oh, well, yeah, then I do think.
Yeah, yeah, probably that was the case, then I understood it wrong, so I don't know.
I was going to say in Islam, Jesus Christ is one of their prophets.
So I don't think it's unusual in cultures that grew up together to have to share figures.
Well, in a religion that, and most religions claim to be universal truths, Christianity
claims to be the whole truth.
Islam claims to be the whole truth.
Hinduism also claims to be a philosophy that encompasses the whole of reality.
So to allow for something outside of that doctrine is not possible.
So regardless of whether it's a jealousy thing or a worry about people leaving it, it's
a way of blocking out the truth, really, that their doings is that their truths or other
religious paths, because it's a threat to your belief to think that there could be something
else, like an enlightenment that isn't your enlightenment.
So they therefore find an explanation, so people will come to the teach, well, what about
the Buddha?
Well, Buddha was just another, actually, Vishnu or Krishna has many incarnations, and Buddha
is just another.
So this is how these theories come up, and it can become so ingrained in people that I've
talked to these people, the very Christians, and they'll say it matter of factly, because
it's their accepted belief, and they really do believe that that's the case.
They believe that we're deluded to think that Buddhism teaches something other than the
worship of Krishna or the attainment of Godhood.
I think there's a real problem with this, because this theory makes Buddha a God, which
is not the case, which was not the case, and they are really tense, everything, and
puts it in the wrong direction, and it makes all the entire teaching, and the entire
story of the Buddha kind of silly.
And there's one other thing that I thought of as when I was talking about how these religious
traditions are claimed to have the universe of the whole truth.
I think you'd almost have room to say that Buddhism doesn't claim such a thing, that
we might even boast of ourselves that we aren't falling into that folly, because in
one sense, anyway, because Buddhism can accept many of the doctrines of these religions.
You could even say, well, yeah, maybe Jesus Christ was the Son of God, but who's this
God guy anyway?
What's so special about him, or in Hinduism, we would accept, you know, Krishna may
be Krishna exists. I mean, I think it's a little bit silly, because Krishna is actually
a part of a Hindu, I guess you shouldn't talk about.
These are stories that they composed much later on.
The God Krishna doesn't even come into Hindu religion until after the Buddha, the Upanishads
don't talk about Krishna, I don't think.
One of the early gods of Indra, the Vedic God is Indra, and then you have Pajapati and
I think even Vishnu, I can't remember, but then it was much, much later when Krishna
came in the Mahabharata and Ramayana, which were really just folk tales like the Odyssey
or the Iliad, very similar, long oral stories that people told, very much like the stories
of Homer, but then the problem is that they became religious texts and they incorporated
religious teachings of the Upanishads into them, and they took this religious tradition
and put them in the context of these stories, and so you have the Bhagavad Gita, which
is very much a religious teaching, but it's placed in the mouths of the mouth of this new
God, it was really a new God, like Shiva, for example, who came up later as well.
But we could accept that there are such gods, we might not accept Krishna because we might
say, well, it looks a little bit more like he was made up as they went along, but we
might accept that Indra exists, and in fact in Buddhism we do accept Indra because at the
time of the Buddha it was well-known in people who were talking about him, but the Buddha said,
well, you know, actually his name is not Indra, his name is Saka, so this kind of thing,
but we can accept that these people have this path and it can lead you to become one with
God.
We could even go so far as to say, you can become one with God, always says it's not enough.
So I think that is a valid point that it's simply because these religions are trying
to fit the whole universe, the complex and manifold infinitely diverse universe with all
of its infinite paths and potentials into a very, very, very small framework of reality
that their own minds are able to encompass.
So I think in one sense we are free from that in the Buddha's teaching that we don't
really try to figure out who is God and what does He want and what does He want us to do.
We're not really interested actually.
We find a universal truth in another sense and that is that even God, even heaven,
even God who and ourselves are meditation practice, all of these things are made up of building
blocks, just as a physicist will tell you, the mind is also an experience in general
is made up entirely of building blocks, of physical and mental components that are impermanence
suffering and on self.
The meaning is that there is no God, no matter whether you see God and He comes and talks
to you, there is no God.
When I'm sitting here talking to you, there is no me.
And none of us really exists, this place, as I was telling you, I said, this monastery
has no people in it and you have, if you want the best way to get along in the monasteries
to remind yourself that there's no people here, because if there's no people working
the problem come from and there is no people, there are only physical and mental realities
that arise and cease.
Thank you.
