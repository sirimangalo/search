1
00:00:00,000 --> 00:00:29,200
Good evening everyone, broadcasting live, February 18th, 2016.

2
00:00:29,200 --> 00:00:52,000
It quote is about desire, desire as a fetter, so I'm probably the least

3
00:00:52,000 --> 00:01:13,040
listening aspect of Buddhism, that it emphasizes very much the disadvantages of liking,

4
00:01:13,040 --> 00:01:29,840
that it negates our likes, and it negates the significance or the validity of our desires.

5
00:01:29,840 --> 00:01:46,040
Desire in general, and anyone who gets in the way between us and our objects of our desire,

6
00:01:46,040 --> 00:02:04,040
is generally not our most favorite person, generally not met with a positive response.

7
00:02:04,040 --> 00:02:12,480
So this subject is one that we have to approach with mindfulness.

8
00:02:12,480 --> 00:02:29,080
And listening when contemplating desire, we have to listen with care, because it's very easy

9
00:02:29,080 --> 00:02:39,080
to fall into prejudice and dissatisfaction.

10
00:02:39,080 --> 00:02:47,480
It's not nice to hear people, it's not nice to hear, even if you intellectually accept it,

11
00:02:47,480 --> 00:03:09,880
that it's not nice emotionally to have your desires discarded or made nothing made out to be dangerous.

12
00:03:09,880 --> 00:03:19,880
So how we look at desires in the world is that which brings us happiness, it's our desires that lead to fulfillment,

13
00:03:19,880 --> 00:03:26,680
that lead to satisfaction, this is how we look at desires.

14
00:03:26,680 --> 00:03:37,080
We see the pleasure that they bring us, and we come to the conclusion therefore that satisfaction lies in that direction.

15
00:03:37,080 --> 00:03:50,080
It stands to reason, if you go mining for gold and you see flakes of gold stands to reason that there's gold.

16
00:03:50,080 --> 00:03:58,680
And if you keep digging, if you keep blasting into the earth, eventually you'll strike it rich.

17
00:03:58,680 --> 00:04:09,480
This is the idea behind, this is how we make the jump from pleasure to happiness or content in satisfaction.

18
00:04:09,480 --> 00:04:18,080
So we get moments of pleasure and we think, oh well that means that this is the direction that I need to be headed.

19
00:04:18,080 --> 00:04:29,480
And so we cultivate that. So that's a part of it, I think.

20
00:04:29,480 --> 00:04:38,880
But Buddhism, of course, looks at the whole subject a little bit differently and points out some uncomfortable truths about this.

21
00:04:38,880 --> 00:04:56,080
Path and about the presuppositions of the suppositions that we make in regards to desire.

22
00:04:56,080 --> 00:05:06,680
First of all that a person who has a mindful desire or lust doesn't see clearly.

23
00:05:06,680 --> 00:05:18,480
So that it isn't really even entirely about our prejudice or about logic or reason or about misunderstanding.

24
00:05:18,480 --> 00:05:34,280
It's just about a mind intoxicated. When you're drunk, you don't do things out of based on logic or even faulty logic.

25
00:05:34,280 --> 00:05:40,480
You don't even think logically.

26
00:05:40,480 --> 00:05:51,080
And so the argument for the chasing after our desires isn't even much of an argument.

27
00:05:51,080 --> 00:06:04,080
There isn't even much of an argument and this is why addicts know that and will tell you that they are on the wrong path.

28
00:06:04,080 --> 00:06:15,880
But they can't stop themselves. They can't help themselves.

29
00:06:15,880 --> 00:06:40,680
And so Buddhism doesn't have to be presented and if you present it just theoretically it comes off as somewhat harsh and unappealing. Of course unappealing to those of us who are to the extent that we're still addicted to things.

30
00:06:40,680 --> 00:06:49,680
But if you approach it in this way of helping people to admit what they already know and that they're not being satisfied.

31
00:06:49,680 --> 00:07:09,280
They are addicted to see clearly without judgment, without prejudice, without even relying on any theory, just to see clearly what's happening.

32
00:07:09,280 --> 00:07:14,880
What is this path of chasing after our desire?

33
00:07:14,880 --> 00:07:32,280
And so that's how Buddhism approaches addiction, desire in general, ambition, anything to do with wanting, liking.

34
00:07:32,280 --> 00:07:47,280
To just study. If you have an addiction as you're engaging in it rather than feeling guilty or hating yourself or getting angry of all of which to very little to solve the problem, study it.

35
00:07:47,280 --> 00:07:56,280
Look at it objectively, look at your reactions, look at your emotions, look at the pleasure that you get.

36
00:07:56,280 --> 00:08:16,280
And not only will you see that you're not being satisfied, but that the pleasure that you're getting is not permanent, but also that the pleasure itself is inconsequential, it's meaningless, it's not even all that attractive.

37
00:08:16,280 --> 00:08:27,280
You lose your attraction to the pleasure as you look at it objectively, of course, because you're no longer judging it.

38
00:08:27,280 --> 00:08:51,280
I mean, you can explain it this way that it's okay if you want to want things that's fine, but study them and be sure that it's right to want those things.

39
00:08:51,280 --> 00:09:02,280
Basically, you can never argue that you could objectively want something, because if you're objective you don't want it, right? You're neutral.

40
00:09:02,280 --> 00:09:21,280
So, this idea of studying things, this is sort of where this quote comes from. This quote is one aspect of how we study desire. And that's in terms of the three times. This is from the English Dernicaya Book of Three is the Deacon Park.

41
00:09:21,280 --> 00:09:34,280
And we turn to the actual suit as I think a little bit. This isn't abbreviation this quote, but basically it says you can desire and lust.

42
00:09:34,280 --> 00:09:45,280
Desire arises in regards to things, three things in the path from the past, from the future and from the present.

43
00:09:45,280 --> 00:10:06,280
So, things that have already passed, you can have desire for them. And so, acknowledging this and differentiating the three times is one way of allowing you to be objective about it, to see exactly what's going to be precise.

44
00:10:06,280 --> 00:10:17,280
Now, this is coming about because of a memory. This is coming about because of a plan in the future ambition. This is coming about because of a present experience.

45
00:10:17,280 --> 00:10:25,280
That's where it's one way of explaining where desire comes from. You have desire based on things in the past wanting them to happen again.

46
00:10:25,280 --> 00:10:39,280
And boy, we had such a good time or I had such a good meal or I really like that person. So, that beautiful thing or heard that beautiful sound.

47
00:10:39,280 --> 00:10:44,280
We have desire for it.

48
00:10:44,280 --> 00:11:00,280
Desire arises and desire might not even be the best. Maybe lust or infatuation, although those are somewhat pejorative, although it is a negative sort of thing, but the point is just an attachment.

49
00:11:00,280 --> 00:11:09,280
Maybe attachment is the best because desire, if you have something, you can't really say that you desire it.

50
00:11:09,280 --> 00:11:18,280
You can say you're attached to it. So, there's two different aspects in English, we differentiate. If you desire something, it means something you don't have.

51
00:11:18,280 --> 00:11:26,280
But if you have something, you don't desire it, you like it or you enjoy it or something. But it's the same mind state.

52
00:11:26,280 --> 00:11:36,280
So, this mind state of attachment or clinging or stickiness can be about things in the past. It can be about things that haven't even come yet.

53
00:11:36,280 --> 00:11:43,280
Ambitions we have desires for the future. People who don't have something that they want.

54
00:11:43,280 --> 00:11:48,280
Well, it constantly, right? The sort of thing is incessant.

55
00:11:48,280 --> 00:11:57,280
Or it can be in the present to experience something and you like it. This is where it changes to what we call liking. Same mind state.

56
00:11:57,280 --> 00:12:02,280
But if it's about something in the present, it's what we call it liking.

57
00:12:02,280 --> 00:12:11,280
Now, the interesting thing is how we understand them to be the same is that you don't actually desire something.

58
00:12:11,280 --> 00:12:22,280
You, something arises, whether it be a memory or a plan or an experience in the present. And right after that, there arises a clinging to that experience.

59
00:12:22,280 --> 00:12:36,280
When the clinging to that experience is what gives rise to the seeking to be turning yourself in the direction so that that experience can be repeated.

60
00:12:36,280 --> 00:12:43,280
But it's just a, it's actually a reaction to us to something that you experience.

61
00:12:43,280 --> 00:12:48,280
You don't desire the thing that you are lacking.

62
00:12:48,280 --> 00:12:58,280
So, suppose I, suppose I had good food this morning and I'm hungry again.

63
00:12:58,280 --> 00:13:01,280
So I want to go and get it.

64
00:13:01,280 --> 00:13:10,280
It's not the food that I'm going to get that I want. It's the, the, the picture in my mind that I cling to that I like.

65
00:13:10,280 --> 00:13:18,280
I see a picture of a hamburger in my head and I really like that picture. I like the, I remember the taste and I really like that taste.

66
00:13:18,280 --> 00:13:24,280
I really like the thought there's a liking that happens.

67
00:13:24,280 --> 00:13:31,280
And this is, it may seem pedantic, but it's actually quite important because it points to what's really going on.

68
00:13:31,280 --> 00:13:37,280
You can't be mindful of that hamburger that's in the restaurant, but you can be mindful of the thought.

69
00:13:37,280 --> 00:13:53,280
You can be mindful of the attachment to this image or this thought or this taste that comes about.

70
00:13:53,280 --> 00:14:02,280
So, in regards to whether it's the past or the present or the future, it's still just in the present moment.

71
00:14:02,280 --> 00:14:08,280
And in this moment is when, this moment is when we be mindful of our desires,

72
00:14:08,280 --> 00:14:24,280
for wanting, wanting, or liking, liking, or happy, happy or pleasure, pleasure, and feeling.

73
00:14:24,280 --> 00:14:34,280
One thinks about and mentally examines things in the past that are the basis for desire and lust.

74
00:14:34,280 --> 00:14:38,280
As one does so, desire arises.

75
00:14:38,280 --> 00:14:45,280
So, the rise is based on that thing that will be in the past or the future or the present.

76
00:14:45,280 --> 00:14:49,280
When desire springs up, one is feathered by those things. Yes.

77
00:14:49,280 --> 00:14:58,280
So, then it needs one to focus one's attention on those things and on the attainment of similar experiences.

78
00:14:58,280 --> 00:15:04,280
So, we have the picture of the experience of this thought about the hamburger.

79
00:15:04,280 --> 00:15:08,280
I want those sorts of experiences and experiences like that.

80
00:15:08,280 --> 00:15:13,280
We actually taste the hamburger.

81
00:15:13,280 --> 00:15:14,280
And that's a fetter.

82
00:15:14,280 --> 00:15:17,280
We get bound to that and we can't be free.

83
00:15:17,280 --> 00:15:24,280
And because there's so many of these, we're constantly reaching out, constantly seeking out.

84
00:15:24,280 --> 00:15:45,280
It's only through meditation, through being objective that we can free ourselves from the rest that we can break free and release ourselves from these bonds.

85
00:15:45,280 --> 00:15:51,280
And then he goes on to talk about how desire does not arise.

86
00:15:51,280 --> 00:15:57,280
How does it not arise?

87
00:15:57,280 --> 00:16:04,280
One understands the future result of things in the past that are the basis for desire and lust.

88
00:16:04,280 --> 00:16:08,280
I mean, understood the future result, one avoids it.

89
00:16:08,280 --> 00:16:20,280
So, by understanding it and by seeing cause and effect, by seeing the nature of these things, one of those is one's attached to it.

90
00:16:20,280 --> 00:16:26,280
Again, through meditation practice.

91
00:16:26,280 --> 00:16:30,280
Having avoided it, one becomes dispassionate in mind.

92
00:16:30,280 --> 00:16:32,280
Having pierced through with wisdom.

93
00:16:32,280 --> 00:16:34,280
One seems.

94
00:16:34,280 --> 00:16:35,280
Banyaya.

95
00:16:35,280 --> 00:16:36,280
Athiridha.

96
00:16:36,280 --> 00:16:37,280
Athiridha.

97
00:16:37,280 --> 00:16:45,280
Athiridha.

98
00:16:45,280 --> 00:16:48,280
See these little gems that you find in the end good for any kaya.

99
00:16:48,280 --> 00:17:01,280
I mean, good for any kaya is full of lists, but it's not just lists, it's nuances and pithi sayings of the Buddha.

100
00:17:01,280 --> 00:17:05,280
Anyway, the important point, desire is to be studied.

101
00:17:05,280 --> 00:17:08,280
Anger is to be studied, delusion is to be studied.

102
00:17:08,280 --> 00:17:11,280
The filaments are to be studied.

103
00:17:11,280 --> 00:17:18,280
You study the things that you desire, you study how they work together, how desire works, and so on.

104
00:17:18,280 --> 00:17:20,280
You'll lose all interest in it.

105
00:17:20,280 --> 00:17:25,280
You don't have to feel bad or guilty about it.

106
00:17:25,280 --> 00:17:36,280
Then just give it up.

107
00:17:36,280 --> 00:17:41,280
Does anyone not hear that a lot of people don't hear the live stream?

108
00:17:41,280 --> 00:17:43,280
What happened to the live stream?

109
00:17:43,280 --> 00:17:44,280
No, no.

110
00:17:44,280 --> 00:17:49,280
Oh, it's on the wrong stream.

111
00:17:49,280 --> 00:17:51,280
Hello?

112
00:17:51,280 --> 00:17:54,280
Sorry, I'm having terrible problems with my audio.

113
00:17:54,280 --> 00:17:57,280
It was just a mistake.

114
00:17:57,280 --> 00:18:01,280
I forgot to switch it over.

115
00:18:01,280 --> 00:18:05,280
Live streams no.

116
00:18:05,280 --> 00:18:08,280
I recorded 16 minutes of silence.

117
00:18:08,280 --> 00:18:14,280
That would be on the website.

118
00:18:14,280 --> 00:18:23,280
The hangouts up, if anyone wants to come on and ask a question or I guess even just say hi.

119
00:18:23,280 --> 00:18:32,280
You can go ahead.

120
00:18:32,280 --> 00:18:42,280
Anyways, I'm going to hand it.

121
00:18:42,280 --> 00:19:05,280
Hello, Bonta.

122
00:19:05,280 --> 00:19:15,280
I had you on YouTube, so I got it all.

123
00:19:15,280 --> 00:19:21,280
I know I went to the website first and it was not there.

124
00:19:21,280 --> 00:19:29,280
I was just fiddling with my audio and then it was late and I didn't have enough time to

125
00:19:29,280 --> 00:19:38,280
do it.

126
00:19:38,280 --> 00:19:50,280
You have to turn YouTube off if you're joining the hangout, Tom.

127
00:19:50,280 --> 00:19:59,280
Is that better?

128
00:19:59,280 --> 00:20:02,280
You've got it playing in the background somewhere.

129
00:20:02,280 --> 00:20:06,280
I think, I'll get rid of that.

130
00:20:06,280 --> 00:20:07,280
Sorry.

131
00:20:07,280 --> 00:20:09,280
There we are.

132
00:20:09,280 --> 00:20:11,280
Yep, that sounds better.

133
00:20:11,280 --> 00:20:13,280
Sorry.

134
00:20:13,280 --> 00:20:14,280
Technofo up here.

135
00:20:14,280 --> 00:20:23,280
Yeah, well, let's see.

136
00:20:23,280 --> 00:20:26,280
I don't suppose any of you have questions.

137
00:20:26,280 --> 00:20:27,280
I do.

138
00:20:27,280 --> 00:20:28,280
Okay.

139
00:20:28,280 --> 00:20:29,280
Give me a question.

140
00:20:29,280 --> 00:20:35,280
My question is, this is a long standing issue for me.

141
00:20:35,280 --> 00:20:40,280
I like to follow current events, let's say.

142
00:20:40,280 --> 00:20:45,280
And it still has a fair amount of appeal to me.

143
00:20:45,280 --> 00:20:51,280
So what happens is it ends up being something I'm mulling over throughout the day.

144
00:20:51,280 --> 00:20:57,280
So I think I know the answer to this, but should I just like any other form of

145
00:20:57,280 --> 00:21:02,280
entertainment, I guess, should I just decrease it or eliminate it?

146
00:21:02,280 --> 00:21:06,280
What would your observation be on that?

147
00:21:06,280 --> 00:21:10,280
I'm moderated.

148
00:21:10,280 --> 00:21:11,280
What your level of commitment is.

149
00:21:11,280 --> 00:21:16,280
Obviously during a meditation course, you would not have any

150
00:21:16,280 --> 00:21:27,280
connection, any curiosity, any engagement in current event.

151
00:21:27,280 --> 00:21:31,280
So, you know, it's a distraction.

152
00:21:31,280 --> 00:21:39,280
It's probably not going to hurt your practice, but it's going to,

153
00:21:39,280 --> 00:21:44,280
you know, it'll take time out of your practice.

154
00:21:44,280 --> 00:21:50,280
Because you can't really easily be mindful when you're engaging in whatever,

155
00:21:50,280 --> 00:21:59,280
when you're studying when you're watching the news.

156
00:21:59,280 --> 00:22:06,280
Well, mostly it's reading or going on the internet for the most part.

157
00:22:06,280 --> 00:22:11,280
So it's the one thing in particular that hasn't fallen away.

158
00:22:11,280 --> 00:22:15,280
So I'll just leave it at that, and I'll be mindful of,

159
00:22:15,280 --> 00:22:19,280
I've been kind of grappling with it.

160
00:22:19,280 --> 00:22:26,280
It just has a certain allure for me, so I'll leave it at that.

161
00:22:26,280 --> 00:22:29,280
But, you know, it wouldn't be too hard on yourself.

162
00:22:29,280 --> 00:22:32,280
You know, you start, you take on layers.

163
00:22:32,280 --> 00:22:34,280
Are you keeping the five precepts?

164
00:22:34,280 --> 00:22:36,280
That's the first thing you have to ask yourself.

165
00:22:36,280 --> 00:22:40,280
Once you're keeping the five precepts, you're pretty good to go.

166
00:22:40,280 --> 00:22:43,280
I mean, then, yeah, how much do you want to keep the eight precepts?

167
00:22:43,280 --> 00:22:46,280
Do you want to keep the months away?

168
00:22:46,280 --> 00:22:53,280
Or do you just want to start reducing things like food and entertainment and sleep?

169
00:22:53,280 --> 00:22:56,280
Entertainment is pretty much fallen away.

170
00:22:56,280 --> 00:23:00,280
So I'll start talking about myself and let somebody else jump in.

171
00:23:00,280 --> 00:23:03,280
No, it's a good question.

172
00:23:03,280 --> 00:23:06,280
Because it is easy to get caught up in current events.

173
00:23:06,280 --> 00:23:07,280
It's news people.

174
00:23:07,280 --> 00:23:08,280
Yeah.

175
00:23:08,280 --> 00:23:11,280
My mom's a lot of news.

176
00:23:11,280 --> 00:23:12,280
Not like it's an obsession.

177
00:23:12,280 --> 00:23:14,280
I mean, it's not hurting her, right?

178
00:23:14,280 --> 00:23:15,280
It's a person.

179
00:23:15,280 --> 00:23:19,280
It just kind of gets a new way.

180
00:23:19,280 --> 00:23:22,280
Thank you.

181
00:23:22,280 --> 00:23:29,280
Anybody else got questions?

182
00:23:29,280 --> 00:23:30,280
All right.

183
00:23:30,280 --> 00:23:51,280
Anybody got comments?

184
00:23:51,280 --> 00:24:01,280
No comments, no questions.

185
00:24:01,280 --> 00:24:06,280
Well, that was an excellent talk.

186
00:24:06,280 --> 00:24:08,280
Oh, thank you.

187
00:24:08,280 --> 00:24:11,280
And it was good for me anyway.

188
00:24:11,280 --> 00:24:12,280
Yep.

189
00:24:12,280 --> 00:24:13,280
Thank you, Bante.

190
00:24:13,280 --> 00:24:14,280
That was a good talk.

191
00:24:14,280 --> 00:24:15,280
Thanks.

192
00:24:15,280 --> 00:24:20,280
You made me think about food.

193
00:24:20,280 --> 00:24:25,280
Were you salivating over my cheeseburger?

194
00:24:25,280 --> 00:24:27,280
That was a good image.

195
00:24:27,280 --> 00:24:31,280
That's definitely interesting to hear about.

196
00:24:31,280 --> 00:24:38,280
Made it easy to visualize for sure.

197
00:24:38,280 --> 00:24:39,280
It's funny.

198
00:24:39,280 --> 00:24:46,280
Once you become a Buddhist, it's really funny watching your how your mind works.

199
00:24:46,280 --> 00:24:51,280
Yeah.

200
00:24:51,280 --> 00:25:02,280
All the other stuff that we used to think was me was I now becomes like your lab study.

201
00:25:02,280 --> 00:25:03,280
Yeah.

202
00:25:03,280 --> 00:25:10,280
So it's definitely interesting how the practice has layers too.

203
00:25:10,280 --> 00:25:17,280
Is it good on?

204
00:25:17,280 --> 00:25:22,280
Yeah.

205
00:25:22,280 --> 00:25:24,280
It's very really does.

206
00:25:24,280 --> 00:25:25,280
Okay.

207
00:25:25,280 --> 00:25:26,280
Well, have a good night everyone.

208
00:25:26,280 --> 00:25:27,280
Good night.

209
00:25:27,280 --> 00:25:28,280
Thank you, Bante.

210
00:25:28,280 --> 00:25:29,280
Thank you.

211
00:25:29,280 --> 00:25:48,280
Thank you.

