1
00:00:00,000 --> 00:00:06,360
Good evening. Tonight I'd like to talk about the existence of the mind and the

2
00:00:06,360 --> 00:00:16,480
non-existence of God. Now in science it is generally believed by many scientists

3
00:00:16,480 --> 00:00:26,960
that the mind is simply a extension of the brain or a product or a function of

4
00:00:26,960 --> 00:00:33,480
the brain and that there's nothing outside of the physical and I think this

5
00:00:33,480 --> 00:00:39,600
says something about the bias of certain scientists and of the scientific

6
00:00:39,600 --> 00:00:47,920
tradition in general that it has always focused on the physical and with good

7
00:00:47,920 --> 00:00:53,560
reason I mean for the most part our understanding of the mental especially in

8
00:00:53,560 --> 00:00:58,800
the West has been very supernatural. It's been associated with a creator God,

9
00:00:58,800 --> 00:01:06,440
with a soul and so on. In fact even in India there is the belief in a soul in

10
00:01:06,440 --> 00:01:12,360
a self, an abdomen and brahma and so on and so it's generally been discarded

11
00:01:12,360 --> 00:01:16,840
that anything outside of the physical because it can't be experimented on it is

12
00:01:16,840 --> 00:01:20,520
outside of the realm of science outside of the realm of reality into something

13
00:01:20,520 --> 00:01:26,960
to be always regarded with suspicion. But suppose that there was something that

14
00:01:26,960 --> 00:01:32,840
existed outside of the physical but could be experimented on, could was

15
00:01:32,840 --> 00:01:38,600
empirically realizable, empirically verifiable and had nothing to do with

16
00:01:38,600 --> 00:01:43,640
the supernatural was completely and perfectly subject to natural laws of its

17
00:01:43,640 --> 00:01:48,480
own kind which would of course be somewhat different though related to the

18
00:01:48,480 --> 00:01:57,100
physical laws. And I submit that well God and the soul and all of these

19
00:01:57,100 --> 00:02:03,280
supernatural entities that are generally referred to in religious

20
00:02:03,280 --> 00:02:08,440
traditions are not empirically realized but are not subject to experiment and

21
00:02:08,440 --> 00:02:13,240
so on that there is something which though not being physical is subject to

22
00:02:13,240 --> 00:02:19,320
experiment and is empirically verifiable and that is the mind and how we do this

23
00:02:19,320 --> 00:02:22,240
of course is through the practice of meditation at the time when we're

24
00:02:22,240 --> 00:02:29,480
practicing meditation you'd have to be intentionally blind what I think most

25
00:02:29,480 --> 00:02:35,280
of us in the West are to deny the existence of something which was so clearly

26
00:02:35,280 --> 00:02:41,000
evident and so clearly not physical that at the time when you close your eyes

27
00:02:41,000 --> 00:02:44,520
it's so obvious that there is something watching for instance when you're

28
00:02:44,520 --> 00:02:48,280
watching the breath when you watch the breath go in and out it's clear and it's

29
00:02:48,280 --> 00:02:52,560
something that you can do an experiment on you close your eyes and you're

30
00:02:52,560 --> 00:03:00,600
aware there is an awareness whether it's you or not is is a subjective sort of

31
00:03:00,600 --> 00:03:05,360
thing but at any rate there is something that knows and that which knows while

32
00:03:05,360 --> 00:03:08,320
we call it the mind whatever you want to call it there is something that is

33
00:03:08,320 --> 00:03:14,840
aware now scientists of course say well like everything else this is

34
00:03:14,840 --> 00:03:22,640
simply a product of the hallucinations or the the delusions that arise from

35
00:03:22,640 --> 00:03:27,760
having an imperfect brain and so on and in the end it's all simply a physical

36
00:03:27,760 --> 00:03:32,480
natural process I agree that it's natural I don't agree that it's physical and

37
00:03:32,480 --> 00:03:37,800
to put this in perspective I want to make it understood that I give no more

38
00:03:37,800 --> 00:03:45,800
credit to the scientific view of materialism than I do to say a Hindu view of

39
00:03:45,800 --> 00:03:50,640
mind only or the existence of the mind only and the existence of mind only

40
00:03:50,640 --> 00:03:57,200
works in this way and it might be easy to see a parallel here the Hindu or

41
00:03:57,200 --> 00:04:03,640
many of the Indian traditions Buddhist some Buddhists as well will say that

42
00:04:03,640 --> 00:04:08,160
there's only the mind everything that is physical is merely a product of mental

43
00:04:08,160 --> 00:04:14,160
delusion our mental imagination it's a dream everything that we see around us

44
00:04:14,160 --> 00:04:19,920
just like in them that movie the matrix is simply a product of mental

45
00:04:19,920 --> 00:04:24,720
hallucination or imagination in reality there's only the mind when we feel

46
00:04:24,720 --> 00:04:29,560
something hard it's actually our mind projecting out and creating this

47
00:04:29,560 --> 00:04:35,720
sense of heart when we feel heat when we feel pressure or so on this is only

48
00:04:35,720 --> 00:04:40,960
something that's happening in the mind and I don't think that there's any reason

49
00:04:40,960 --> 00:04:45,120
to believe one hypothesis over the other one theory over the other all of the

50
00:04:45,120 --> 00:04:49,920
physical experiments that are done in science could easily and very very

51
00:04:49,920 --> 00:04:56,440
quickly be discarded through if we accept a belief in mind only we can say

52
00:04:56,440 --> 00:05:01,480
that the physical is only something that arises based on the mind I don't

53
00:05:01,480 --> 00:05:06,520
believe that and I don't believe the materialist view that there is no there is

54
00:05:06,520 --> 00:05:13,080
no mind how I how I approach this is in in this way the belief in only the

55
00:05:13,080 --> 00:05:18,000
physical and the belief in only the mental either one of them has to discard

56
00:05:18,000 --> 00:05:23,960
the other you have to have some explanation as to why that which appears to

57
00:05:23,960 --> 00:05:28,040
exist does not exist if you choose the materialist side you have to have an

58
00:05:28,040 --> 00:05:35,080
explanation of why what appears to be a very empirical reality that of the

59
00:05:35,080 --> 00:05:40,520
mind why doesn't it exist why is it that we think it exists or we we experience it

60
00:05:40,520 --> 00:05:44,440
to exist and it actually doesn't of course scientists have all of this they

61
00:05:44,440 --> 00:05:49,440
have their explanations the on the other side the mind only people they have

62
00:05:49,440 --> 00:05:55,680
their own explanations but they are required to provide those explanations as

63
00:05:55,680 --> 00:06:01,320
to why the physical what appears to be physical why is it that in what way can

64
00:06:01,320 --> 00:06:05,200
you say that it doesn't exist and of course existence here is a tricky thing

65
00:06:05,200 --> 00:06:09,560
how can you say that this exists and this does not exist you avoid both of

66
00:06:09,560 --> 00:06:17,160
these extremes both of these dilemmas and you you clearly have the

67
00:06:17,160 --> 00:06:22,120
advantage by having a simpler explanation when you accept the existence of

68
00:06:22,120 --> 00:06:26,760
both we accept the existence of the physical because we can feel it we can

69
00:06:26,760 --> 00:06:29,800
see it we can hear it we can smell it we can taste it we can run all sorts of

70
00:06:29,800 --> 00:06:35,160
experiments on it which are clearly one realm of reality and that is the

71
00:06:35,160 --> 00:06:38,680
physical realm we can do the same with the mind and this is unknown to most

72
00:06:38,680 --> 00:06:43,160
scientists I understand but it is very well known to the eastern quote-unquote

73
00:06:43,160 --> 00:06:46,960
religious or spiritual traditions which in the end can be very scientific

74
00:06:46,960 --> 00:06:53,280
early Buddhism was incredibly scientific and has the mind reduced it's a

75
00:06:53,280 --> 00:06:58,920
reductionist view of the mind reduced into various building blocks just as the

76
00:06:58,920 --> 00:07:04,000
physical world is so that every experience is considered to be a building block

77
00:07:04,000 --> 00:07:10,880
of what we then call the mind and it is something which clearly exists which

78
00:07:10,880 --> 00:07:14,280
can be experimented on which can be verified and it has a whole

79
00:07:14,280 --> 00:07:22,640
magisterium of its own so in a sense these are two magisterium two mutually

80
00:07:22,640 --> 00:07:28,000
exclusive realms of dominance that the mind has its own realm and the body has its

81
00:07:28,000 --> 00:07:31,080
own realm now it's clear that they work together it's clear in fact that

82
00:07:31,080 --> 00:07:40,240
they're probably part of a hole which we would then call a being or so on but

83
00:07:40,240 --> 00:07:46,000
of course that hole is only the sum of its parts this is an explanation of why I

84
00:07:46,000 --> 00:07:50,240
would say it's most rational to believe in both the mind and the body I don't

85
00:07:50,240 --> 00:07:53,840
think there's any reason for us to have to do mental somersaults to try to

86
00:07:53,840 --> 00:08:18,840
explain away one to the other side

