1
00:00:00,000 --> 00:00:11,080
Good evening, continuing on with our study of the Dhamapada today, verses 206 to 208, which

2
00:00:11,080 --> 00:00:14,520
read as follows.

3
00:00:14,520 --> 00:00:31,840
Saju das anamaryana, sanny waso sada sukho, a das anena bala anam, nityamewa suki siya, bala sanga taturi

4
00:00:31,840 --> 00:00:49,800
digamadana sujiti, dukobali sanga sukha, amita aneva sambada, diroja sukha sanga sukha sanga sukha

5
00:00:49,800 --> 00:01:01,120
nitya nanga samagamu, tasmahi diranja panyanja bhausutantja

6
00:01:01,120 --> 00:01:20,080
dor raiha cilang, vatavantanga, vatavantamaryang, tanta disang sampuri samsumetang, bhajita nakata pattang

7
00:01:20,080 --> 00:01:39,640
vatandima, which means it is good to see the noble ones, being in their company is always

8
00:01:39,640 --> 00:01:54,040
happiness, not seeing, by never seeing fools, foolish people, never seeing foolish people

9
00:01:54,040 --> 00:02:05,360
would be, would be, ever happiness, it would be happiness to never see foolish people,

10
00:02:05,360 --> 00:02:22,280
one who fares together with fools, indeed, sorrows for a long time, painful, stressful is

11
00:02:22,280 --> 00:02:36,600
the association with fools, who are always like enemies, and association with the

12
00:02:36,600 --> 00:02:49,320
steadfast is happiness, just like associating or living together with relatives, therefore

13
00:02:49,320 --> 00:03:14,520
indeed, one who is steadfast, wise, well-learning bhausutta with good morality or ethics,

14
00:03:14,520 --> 00:03:28,200
who is dutiful and noble, such a person, a person of such a sort, who is gentle, a good

15
00:03:28,200 --> 00:03:45,560
fellow, a good person, sapurisa, who is wise, sumeda, bhajita, one should, you should follow such a person,

16
00:03:45,560 --> 00:03:55,400
like the stars follow the moon, to the stars follow the moon, those are the moon follows

17
00:03:55,400 --> 00:04:07,320
the stars, I don't know astrology, astronomy enough, no one is that's true, I don't think

18
00:04:07,320 --> 00:04:17,680
the moon actually follows the stars, but it may look like that.

19
00:04:17,680 --> 00:04:31,040
So these three verses were taught in relation to as a result of saka, saka was the king

20
00:04:31,040 --> 00:04:39,560
or is the king of the gods of the thirty-three, so there's this group of angels or gods

21
00:04:39,560 --> 00:04:53,120
and deva we would call them, and the devas are not enlightened necessarily, they can be,

22
00:04:53,120 --> 00:04:57,320
but it doesn't take enlightenment to be king of the gods, so they're not considered very

23
00:04:57,320 --> 00:05:04,200
high up in the scheme of things and Buddhism, but there's something quite impressive and

24
00:05:04,200 --> 00:05:13,600
special in a sense of the goodness that's required to become a deva, and especially to

25
00:05:13,600 --> 00:05:18,160
become the king while that's an impressive feat, and take some great greatness of mind

26
00:05:18,160 --> 00:05:24,120
if you learn anything about saka, if you ever read his story, he did a lot of good things

27
00:05:24,120 --> 00:05:31,440
for his society and his community and his family when he was on earth, and as a result

28
00:05:31,440 --> 00:05:37,640
of that being sort of a leader, but a leader not in the sense of ordering people around,

29
00:05:37,640 --> 00:05:42,360
but in the sense of giving a good example and helping and supporting his community was

30
00:05:42,360 --> 00:05:50,520
really a good story in the jataka about it, as a result he became king.

31
00:05:50,520 --> 00:06:01,360
Humility is often a cause for people to become a high rank or a high society.

32
00:06:01,360 --> 00:06:04,880
But maybe not always, it doesn't always appear that way when you look at people who make

33
00:06:04,880 --> 00:06:12,640
it to a high society, to a high rank, a high status, to positions of leadership, but of

34
00:06:12,640 --> 00:06:18,640
course we don't know people's past lives.

35
00:06:18,640 --> 00:06:25,120
Anyway, saka was quite special in this way, he was very good and very noble being, even

36
00:06:25,120 --> 00:06:32,040
before he became a sota panda, which apparently he now is, but when he heard that the Buddha

37
00:06:32,040 --> 00:06:40,560
was going to pass away, remember we heard earlier the story of the Buddha telling his

38
00:06:40,560 --> 00:06:46,360
monks that he was going to pass away four months, well he traveled for some time and then

39
00:06:46,360 --> 00:06:55,480
four months later he had a meal that disagreed with him, or it may or may not have been

40
00:06:55,480 --> 00:06:57,640
the meal, depends I think on who you ask.

41
00:06:57,640 --> 00:07:08,680
The commentary seems to think it was unrelated to the meal, I'm not sure.

42
00:07:08,680 --> 00:07:20,720
But at any rate the Buddha became quite ill and became sick with dysentery and saka

43
00:07:20,720 --> 00:07:27,080
heard about this and came all the way from his wonderful place in heaven and came to where

44
00:07:27,080 --> 00:07:35,480
the Buddha was lying there and he started massaging the Buddha's feet and it was dark

45
00:07:35,480 --> 00:07:43,240
I guess maybe the Buddha couldn't see who it was and he said who is that and but the Buddha

46
00:07:43,240 --> 00:07:46,840
also asked these questions, they say the Buddha knew these sorts of things and he wouldn't

47
00:07:46,840 --> 00:07:51,960
have been unknowing of who it was but it's a good way to start a conversation so he

48
00:07:51,960 --> 00:08:04,360
asked who is that and saka told him it's me, saka and the Buddha said saka why are

49
00:08:04,360 --> 00:08:15,480
you here, your home is so far more pleasant than this place and the smell he said of

50
00:08:15,480 --> 00:08:27,480
human, the smell of human beings, angel, Devas can smell human beings from 1,000 leagues

51
00:08:27,480 --> 00:08:33,800
which is about 16,000 miles and they get anywhere close to human beings and they can

52
00:08:33,800 --> 00:08:44,120
already start to smell the disgusting smell of the filth and the vial coarseness of humanity

53
00:08:45,640 --> 00:08:49,880
and he said you can smell that from so far away and you'd hear you are, what are you doing here?

54
00:08:52,360 --> 00:08:59,720
And saka said, oh, venerable sir, from even further away than that I could smell the greatness

55
00:08:59,720 --> 00:09:05,960
and the goodness of who you are, the nobility of who you are and so being in your presence

56
00:09:05,960 --> 00:09:14,200
is always like a breath of fresh air and he spent the time looking after the Buddha while

57
00:09:14,200 --> 00:09:20,600
the Buddha was sick and every day he would take the Buddha's chamber pot out, the chamber pot

58
00:09:20,600 --> 00:09:26,120
is a pot that you urinate and defecate in and it has to be in the olden days they didn't of

59
00:09:26,120 --> 00:09:31,640
course have flush toilet so it has to be carried outside and washed out and every day he would

60
00:09:31,640 --> 00:09:37,560
pick up this pot and he would put place it on top of his head and walk outside with it and never

61
00:09:37,560 --> 00:09:46,360
once the commentary says did he even wrinkle his face, not one muscle in his face moved he was

62
00:09:46,360 --> 00:09:52,280
perfectly smiling even with a stench of the Buddha's feces and urine

63
00:09:52,280 --> 00:09:59,720
and he wouldn't let anybody else do it and he took it outside and washed it out and cared for

64
00:09:59,720 --> 00:10:07,800
the Buddha while he was sick and the monks got together afterwards and they send amongst themselves

65
00:10:07,800 --> 00:10:15,160
it's quite amazing that saka would do such a thing after all it's so hard for angels to bear

66
00:10:15,160 --> 00:10:21,400
for they was to bear with the stench of humans and the Buddha heard them talking as usual and

67
00:10:21,400 --> 00:10:28,360
he asked them what are you talking about they said told him and he said oh monks it's not unusual

68
00:10:28,360 --> 00:10:36,600
it's not surprising because and then he told the story of how he taught saka and gave him

69
00:10:37,560 --> 00:10:43,480
the dumb mind and as a result saka became a sort of thunder which is an interesting story I won't

70
00:10:43,480 --> 00:10:48,440
go into the details of what he taught him but it's something probably I'll try to give a talk in the

71
00:10:48,440 --> 00:10:54,280
future about the questions of saka what he asked the Buddha is quite enlightening

72
00:10:55,880 --> 00:11:03,480
it shows the sort of heightened state of of mind is a very high-minded individual

73
00:11:07,880 --> 00:11:14,760
but because of that saka is very happy to be around me the Buddha said because of the

74
00:11:14,760 --> 00:11:20,760
the greatness of being around noble ones and then of course he taught this verse it's great

75
00:11:21,400 --> 00:11:26,600
it's always happiness it's always pleasant is the point right because of our unpleasant it is

76
00:11:26,600 --> 00:11:31,960
very angels to be around humans but it's always pleasant being around noble ones

77
00:11:33,480 --> 00:11:38,760
and then he said it conversely being around foolish people that you might even find foolish

78
00:11:38,760 --> 00:11:45,560
people foolish beings in heaven would be very unpleasant and then he taught these verses

79
00:11:49,080 --> 00:11:55,720
so there's not a lot in here that is very you know related to the core specifics of practice

80
00:11:55,720 --> 00:12:00,920
there's some then I'll go into that but it's a very important verse for practitioners

81
00:12:00,920 --> 00:12:10,680
and in the context of a meditation center and the practice of meditation because it works on

82
00:12:10,680 --> 00:12:16,680
two levels of course it's very practical advice and so on the first level we should take the

83
00:12:16,680 --> 00:12:22,360
advice of the Buddha to consider all the people we surround ourselves within life in the world

84
00:12:23,000 --> 00:12:30,440
the people you meet the people you work with the friends you associate with and engage in

85
00:12:30,440 --> 00:12:38,200
recreational activities with even the people you talk philosophy and religion and dhamma with

86
00:12:39,480 --> 00:12:49,320
you should consider are they foolish or are they wise but on a deeper level and it really has

87
00:12:49,320 --> 00:12:58,600
we have to acknowledge that to some extent even people who are not vile and villainous and

88
00:12:58,600 --> 00:13:05,880
manipulative and so on even us we all come into this world and and even those of us who are

89
00:13:05,880 --> 00:13:14,200
interested in meditation we can still be quite foolish and so the organization of

90
00:13:15,560 --> 00:13:24,440
meditation center is a big part of I think the practice of Buddhism we take the example of

91
00:13:24,440 --> 00:13:34,360
Buddhist monastery right Buddhism is I think the most monastic centric religion of all the major

92
00:13:34,360 --> 00:13:45,480
religions and we don't have priests in early Buddhism or in teravada Buddhism monks are not priests

93
00:13:45,480 --> 00:13:50,360
we are monks and it's a little bit different than a person who might call themselves self-priest

94
00:13:50,360 --> 00:13:57,480
and so we organize ourselves in monastic communities and lay people organize themselves around

95
00:13:57,480 --> 00:14:02,680
monastic communities get involved with monastic communities but also organize their own communities

96
00:14:02,680 --> 00:14:09,400
so we have lay teachers throughout the history of Buddhism who created organizations and brought

97
00:14:09,400 --> 00:14:17,880
people together as in ashrams and that sort of thing and this kind of community this is the

98
00:14:17,880 --> 00:14:26,280
reason why we create a monastery and meditation center in the beginning of course it was a chance

99
00:14:26,280 --> 00:14:31,400
to be close to the Buddha we could have this great friend this wise person who we should

100
00:14:31,400 --> 00:14:40,280
and always should associate with but it of course evolved to still be quite useful while we don't

101
00:14:40,280 --> 00:14:46,520
have the Buddha but then we have his enlightened disciples or at the very least we have all of us

102
00:14:46,520 --> 00:14:53,640
who are part of the process of enlightenment you could call a person who is not yet even a sotapan

103
00:14:53,640 --> 00:14:59,080
you could call them an enlightened follower because they are on the path to become enlightened

104
00:15:02,200 --> 00:15:07,880
you might you might not call them an enlightened being but they are a part of the process

105
00:15:07,880 --> 00:15:14,840
and so surrounding yourselves with even foolish people who are interested in not being foolish anymore

106
00:15:14,840 --> 00:15:19,960
right it's an important part of the practice surrounding ourselves with each other who are

107
00:15:20,680 --> 00:15:28,920
while stilling have foolish tendencies but are inclined and dedicated really to overcoming

108
00:15:28,920 --> 00:15:37,480
our own foolishness it's a very important quality so regardless of or above and beyond the importance

109
00:15:37,480 --> 00:15:46,600
of sort of reflecting on the friends that you keep the company that you keep in your worldly

110
00:15:46,600 --> 00:15:57,320
activities we also reflect upon the fact that it's good to organize your time around your meditation

111
00:15:57,320 --> 00:16:05,320
community and try your best to find time to leave behind even your good friends even people you

112
00:16:05,320 --> 00:16:11,320
say well they're good people they're keeping ordinary morality and ethics but you leave that all

113
00:16:11,320 --> 00:16:16,920
behind as a group or as an individual you come as friends even right so here's a good example we

114
00:16:16,920 --> 00:16:22,760
have two friends here and they are good friends in the world probably they keep good ethics

115
00:16:22,760 --> 00:16:27,320
and morality but even that's not enough because they can still be foolish together as they know

116
00:16:27,320 --> 00:16:33,480
we have foolish tendencies so you bring your friends to a special state where you are really

117
00:16:33,480 --> 00:16:40,200
close to the Buddha right are you you're close to a teacher who you can say well he's or she is

118
00:16:40,200 --> 00:16:46,600
less less foolish than we are perhaps or at the very least they know teachings they can direct us

119
00:16:46,600 --> 00:16:52,360
in the teachings of the one who was in the least foolish of all or they're completely non foolish

120
00:16:52,360 --> 00:17:05,640
which is the Buddha so part of part of our seeking for a life surrounded by wise people or

121
00:17:05,640 --> 00:17:12,520
in association with wise people I think is this creation of meditation centers, monasteries,

122
00:17:12,520 --> 00:17:20,120
places of practice and so I just wanted to stress that because I think it points to the greatness

123
00:17:20,120 --> 00:17:29,240
of the association and the organization that we've created that we have all the volunteers

124
00:17:29,240 --> 00:17:38,520
working hard at and we have people coming to stay and help out and contribute to the creation

125
00:17:38,520 --> 00:17:46,280
of this place I think it can't be understated and the other thing of course is that it's worth

126
00:17:46,280 --> 00:17:52,440
noting that in our case it's quite limited we're only only have a few rooms for people to stay

127
00:17:52,440 --> 00:18:00,680
and so it's something for us to consider I think that given this importance of creating this

128
00:18:00,680 --> 00:18:06,440
creating community that we should all consider perhaps as an organization how we can move forward

129
00:18:06,440 --> 00:18:17,160
to expand I say this because on the one hand I'm not very interested in expansion and they sort

130
00:18:17,160 --> 00:18:23,640
of the ungainliness involved with seeking out bigger and bigger and being ambitious and so on

131
00:18:24,200 --> 00:18:28,920
but on the other hand when you reflect on teachings like this you think well we can talk about

132
00:18:28,920 --> 00:18:33,080
community and the importance of allowing people and providing the opportunity for people

133
00:18:33,080 --> 00:18:37,960
to come together in this way or the importance for all of us to engage in this way

134
00:18:38,840 --> 00:18:45,960
but it's very difficult when we have to turn people away and don't really have the capacity

135
00:18:45,960 --> 00:18:50,280
to take on long-term meditators or monks you know there are people who want to come and

136
00:18:51,240 --> 00:18:59,320
stay in ordain as monastics male or female but we have no room for anyone so something to consider

137
00:18:59,320 --> 00:19:04,200
I think that that's the first part of the lesson that we get from this verse is

138
00:19:04,200 --> 00:19:08,280
importance of I mean it's the obvious lesson that the Buddha is trying to teach the importance

139
00:19:08,280 --> 00:19:14,040
of associating with good people or having a positive association with each other

140
00:19:14,840 --> 00:19:22,040
not leading each other in foolish direction because of course association with fools is painful

141
00:19:22,040 --> 00:19:27,800
it's painful because they lead us in the wrong direction they encourage us in their bad ways

142
00:19:27,800 --> 00:19:35,320
they encourage us in our bad ways they're manipulative they're unreliable etc etc

143
00:19:35,960 --> 00:19:42,360
just all around I mean that's that's the reason why we call them the point is be able to discern

144
00:19:42,360 --> 00:19:49,800
which people do that which people lead you in a bad direction and waste your time and waste their

145
00:19:49,800 --> 00:20:01,800
time and cultivate immorality together and more importantly finding ways to create an environment

146
00:20:03,000 --> 00:20:07,400
that is conducive towards all of us giving up our foolishness so you might have friends who are

147
00:20:07,400 --> 00:20:13,400
foolish but you and they both want to be less foolish so you and they create a new

148
00:20:13,400 --> 00:20:22,520
sort of situation whereby you both can become less foolish together of course relying very much

149
00:20:22,520 --> 00:20:30,120
often on a teacher or the Buddha's teaching the other part of the lesson is the quality is

150
00:20:30,760 --> 00:20:35,320
that the Buddha mentions here we would I would be negligent if I didn't include

151
00:20:36,120 --> 00:20:40,600
a little bit of discussion about the actual qualities of what makes a good person the Buddha

152
00:20:40,600 --> 00:20:47,720
lays them out precisely it's just one list that talks about the qualities of a enlightened

153
00:20:47,720 --> 00:20:55,320
being but it's in regards I think it's important in regards to their status or their capacity

154
00:20:55,320 --> 00:21:01,800
as a good friend or as someone you'd want to associate with so the first one is that they're steadfast

155
00:21:01,800 --> 00:21:10,920
dira dira can mean wise but here we have bhanya wa which also means wise so dira here might better

156
00:21:10,920 --> 00:21:18,200
mean steadfast as to to different meaning steadfast can refer to wisdom but it also has the element

157
00:21:18,200 --> 00:21:26,200
of confidence but it's the certainty that one gets from wisdom some people are very confident

158
00:21:26,200 --> 00:21:32,600
but unwise and so there's not a great stability there that be confident in one thing one day

159
00:21:32,600 --> 00:21:37,080
and then confident in another thing another day they're confident whatever they decide I believe

160
00:21:37,080 --> 00:21:41,640
this go this way oh no I believe this absolutely this is right oh wait it's wrong

161
00:21:43,000 --> 00:21:49,000
without wisdom confidence is very unstable so someone who is steadfast means they have a confidence

162
00:21:49,000 --> 00:21:55,880
that is unshakeable and it never changes they're reliable right a person who has wisdom who has

163
00:21:56,840 --> 00:22:02,440
knowledge of the truth because of course the truth doesn't change their knowledge as a result their

164
00:22:02,440 --> 00:22:10,600
beliefs their philosophy never changes and so they're reliable there's someone you can go to for

165
00:22:10,600 --> 00:22:14,280
answers and you'll always get the same answers you won't like you may even don't like them

166
00:22:14,280 --> 00:22:22,360
but they won't they won't be unreliable and moreover it's much more reliable because of the wisdom

167
00:22:23,080 --> 00:22:29,080
so the second quality is wisdom and a person who you should take as a friend is someone who is

168
00:22:29,080 --> 00:22:34,360
wise someone who has seen the truth for themselves because of course then they are the most reliable

169
00:22:34,360 --> 00:22:46,200
the third quality is Bahu Sutta now Bahu Sutta means they've heard a lot literally it means that

170
00:22:46,200 --> 00:22:49,880
they've learned a lot of the Buddha's teaching and of course the Buddha's time it would have

171
00:22:49,880 --> 00:22:55,960
been all through listening you heard the Buddha say a lot you heard his followers his chief disciples

172
00:22:56,680 --> 00:23:02,920
teach a lot and nowadays it means you've read a lot probably but also of course you've heard

173
00:23:02,920 --> 00:23:09,640
talks on the Dhamma many there many scholar monks even today who give very deep and detailed

174
00:23:09,640 --> 00:23:16,200
teaching on Buddhist theory and so you heard a lot of that and this is important

175
00:23:20,440 --> 00:23:28,280
separate from wisdom because sometimes you might associate with a wise person but they don't give

176
00:23:28,280 --> 00:23:35,160
you much instruction right it is truly possible for someone to be an Arahan not a Buddha but an

177
00:23:35,160 --> 00:23:44,040
Arahan and never teach anyone maybe never even help anyone because an Arahan just means they've

178
00:23:44,040 --> 00:23:51,160
given up there any defilements it doesn't mean that they are automatically a teacher or inclined

179
00:23:51,160 --> 00:23:58,120
to teaching moreover they they might not be able to help other people with their problems they'll

180
00:23:58,120 --> 00:24:03,400
be completely clear in their own minds but their capacity to share that knowledge with others

181
00:24:03,400 --> 00:24:11,480
might be limited as often limited by their learning and so a teacher who has learned a lot is

182
00:24:12,200 --> 00:24:17,640
quite valuable of course a teacher has learned a lot who doesn't have wisdom maybe less value

183
00:24:17,640 --> 00:24:22,280
and the Risudimaga goes into detail of this best is the Buddha he should be your best teacher

184
00:24:22,920 --> 00:24:27,320
then one of his chief disciples and then an Arahan and so on and so on but if you don't have a

185
00:24:27,320 --> 00:24:32,760
Sotapana even a Sotapana who can help you you should find someone who has learned a lot

186
00:24:33,640 --> 00:24:39,320
I think or he talks about the differences because a Sotapana or even an Arahan who hasn't learned a

187
00:24:39,320 --> 00:24:47,640
lot they know a path absolutely they know a path but it's their path my person who has learned a lot

188
00:24:48,520 --> 00:24:55,000
knows a wider path they know more they have a wider path that they can help people follow maybe

189
00:24:55,000 --> 00:25:00,760
your path is not the same as my path so they can't help you as much but if you've learned a lot

190
00:25:00,760 --> 00:25:07,000
you can help a lot better so it's an important quality and friends specifically not necessarily

191
00:25:07,000 --> 00:25:17,400
as meditators but for someone who's going to teach it's invaluable and the fourth quality is

192
00:25:17,400 --> 00:25:24,280
they're ethical so in the context of monks of course this means they keep all the monastic

193
00:25:24,280 --> 00:25:30,120
precepts but more deeply it means they are their body and their speech the things they do

194
00:25:30,120 --> 00:25:39,960
and the things they say are not manipulative or deceptive or harsh painful or useless

195
00:25:39,960 --> 00:25:48,120
you need a friend who is going to be a good example for you because all of us when we come to

196
00:25:48,120 --> 00:25:53,480
practice we have these tendencies if you came here and you saw me dancing and singing well

197
00:25:54,120 --> 00:25:58,760
it might encourage you to dance and sing and then you wouldn't get anywhere because you'd be

198
00:25:58,760 --> 00:26:06,200
caught up in sensuality so you need people who are a good example and who give us a good

199
00:26:06,200 --> 00:26:12,840
example to follow like an alcoholic shouldn't surround them by cells with alcoholics

200
00:26:13,960 --> 00:26:24,440
that one's pretty obvious and the one to be on one two three four fifth one is the fifth one

201
00:26:24,440 --> 00:26:31,160
is interesting and I think it relates to the story it's at the poly is what the one that I think

202
00:26:31,160 --> 00:26:39,400
it means someone who is dutiful but or that's how they trend that's how they translate it but

203
00:26:40,440 --> 00:26:45,560
what that is a duty and it's an interesting word that we don't talk about perhaps enough

204
00:26:45,560 --> 00:26:51,560
and it's something I do talk about with people on on a regular basis about worldly affairs

205
00:26:51,560 --> 00:27:01,160
so a little background we basically have two parts to morality and Buddhism in one sense

206
00:27:01,160 --> 00:27:08,440
and one is cila cila is those things you shouldn't do on whatever level that is so there are

207
00:27:08,440 --> 00:27:12,600
things you should never do like killing and stealing there are things you shouldn't do if you want

208
00:27:12,600 --> 00:27:19,960
to progress in meditation like eating in the afternoon they're using luxurious beds and seats

209
00:27:19,960 --> 00:27:25,720
that sort of thing and then there are those things that you shouldn't do if you're a monk or

210
00:27:25,720 --> 00:27:32,120
involved in a community like rules of the community not using money which you know it's just a

211
00:27:32,120 --> 00:27:39,960
monastic rule not lighting fires or cutting plants or that sort of thing those are things you

212
00:27:39,960 --> 00:27:44,120
shouldn't do the other side of the morality is called what which means things you should do

213
00:27:44,120 --> 00:27:49,480
and of course meditation is included in one of the things you should do but these are duties

214
00:27:50,360 --> 00:27:56,360
related often to the community or just related to good behavior saka caring for his teacher when

215
00:27:56,360 --> 00:28:04,280
his teacher was sick is one of his duties as a student it's a student's job to it's it's a

216
00:28:05,240 --> 00:28:11,160
a student who is living under a teacher it's their job to take care of their sick teacher

217
00:28:11,160 --> 00:28:17,640
it's a duty that's an example in Buddhism there are many duties them this is for monks that monks

218
00:28:17,640 --> 00:28:23,800
have their duties when you go on arms around their duties when you come as a visitor to a monastery

219
00:28:23,800 --> 00:28:30,840
their duties when you are living in a monastery in a visitor comes many many duties that you have

220
00:28:30,840 --> 00:28:38,120
their duties when you eat food when you go into the dining hall many many duties duties of the

221
00:28:38,120 --> 00:28:45,880
teacher the student duties the teacher the student has to the teacher and so on and this of course

222
00:28:45,880 --> 00:28:51,640
expands why I get this why I answer this in regards to worldly things is because there are many

223
00:28:51,640 --> 00:28:58,040
duties that you could say in the world in the sense that it's important to separate certain

224
00:28:58,040 --> 00:29:04,680
activities oh out from from what is really important like some people would say well all of this

225
00:29:04,680 --> 00:29:09,400
teaching about letting go and giving up is great but what do I do about my kids and my my parents

226
00:29:09,400 --> 00:29:15,400
and my family and and so on what do I do about the society that I live in all of those things are

227
00:29:15,400 --> 00:29:20,680
in the realm of duties so you might say if a student doesn't take care of their teacher when

228
00:29:20,680 --> 00:29:27,880
they're sick like sake and did well he's not necessarily an evil person but if he doesn't do it

229
00:29:27,880 --> 00:29:34,760
it you know the teacher stays sick and maybe dies or so on and it's just not it's not conducive

230
00:29:34,760 --> 00:29:42,360
to a proper it's not conducive to a whole healthy and wholesome society monastic society

231
00:29:43,400 --> 00:29:46,600
if you don't take care of your kids if you don't take care of your parents if you don't

232
00:29:47,400 --> 00:29:53,320
if you don't relate to your friends well to your employer to the king the government that's

233
00:29:53,320 --> 00:29:58,840
sort of thing if you don't relate properly to them it's very hard to progress in meditation

234
00:29:58,840 --> 00:30:08,040
practice and it's not very good for society or good for the community and so on so having a

235
00:30:08,040 --> 00:30:13,480
teacher or people let's put a side teacher because it's not the only thing that this first

236
00:30:13,480 --> 00:30:20,840
refers to if you're associated with the community that is following these duties that has a sense

237
00:30:20,840 --> 00:30:26,360
of duty to each other in a sense of obligation to the community cleaning the cleaning that we

238
00:30:26,360 --> 00:30:34,760
ask meditators to do and just caring for the center and caring for each other so being

239
00:30:34,760 --> 00:30:41,160
considerate to each other not making loud noises necessarily not chatting with each other distracting

240
00:30:41,160 --> 00:30:50,440
each other from the practice all of these things contribute to the harmony and are very important

241
00:30:50,440 --> 00:30:55,720
at the kind of thing that the Buddha said leads to happiness this is the end of the sukawaga the

242
00:30:55,720 --> 00:31:02,280
happiness chapter so it's an important part of happiness find people who are that way if

243
00:31:02,280 --> 00:31:07,560
if on the other hand as i've seen if you go to Buddhist monasteries they're not always

244
00:31:08,440 --> 00:31:13,960
full of people who are harmonious some of the things that you see are not our indicative of

245
00:31:13,960 --> 00:31:21,240
a lapse in this sort of teaching about duties to each other in a sense of commitment and

246
00:31:21,240 --> 00:31:31,400
obligation to the community and the final quality is area is nobility and this is of course you

247
00:31:31,400 --> 00:31:36,440
couldn't you couldn't leave out this quality you should surround yourselves with people who are

248
00:31:36,440 --> 00:31:44,600
noble now nobility is of two kinds one is someone who has freed themselves from defilements

249
00:31:44,600 --> 00:31:49,160
and that is they said is a noble and enlightened being they freed themselves from greed

250
00:31:49,960 --> 00:31:55,000
they freed themselves from anger they freed themselves from delusion but on the other side

251
00:31:55,720 --> 00:31:59,560
it's a concession and i think it's an important concession to say you can call people

252
00:32:00,200 --> 00:32:05,240
in some way noble because they're on the path and they're part of the process of becoming noble

253
00:32:05,240 --> 00:32:13,160
if they are inclined and engaged in the practice of becoming noble if they're engaged in the

254
00:32:13,160 --> 00:32:19,560
practice of mindfulness they are working to overcome their greed i mean even a sota panda can

255
00:32:19,560 --> 00:32:28,920
still have greed and anger and some delusion but even putting that aside people who are what we

256
00:32:28,920 --> 00:32:37,160
call caliana poutu jana a poutu jana someone who still has defilements but they're caliana they're

257
00:32:37,160 --> 00:32:47,160
beautiful because they they are inclined to to work away from to to free themselves and they're

258
00:32:47,160 --> 00:32:53,240
working to free themselves from those defilements you could say that person is also in that sense

259
00:32:53,240 --> 00:33:00,440
noble and these are the type of people we should associate with this is the sort of association

260
00:33:00,440 --> 00:33:07,720
we should want it's a sort of association that the buddha said is the entirety of the holy

261
00:33:07,720 --> 00:33:17,880
life the ascetic life the brahmacharya the religious life what is the religious life well it's

262
00:33:17,880 --> 00:33:24,200
association with good people the buddha said you associate with enlightened people good qualities will

263
00:33:24,200 --> 00:33:34,920
increase associate with good people in a good way good things will come so that's the that's the

264
00:33:34,920 --> 00:33:43,800
reason why saka decided to and didn't hesitate at all to take care of the buddha because he was

265
00:33:43,800 --> 00:33:50,680
very happy to be close and have an opportunity to associate with the buddha but it's a broader

266
00:33:51,320 --> 00:33:57,640
lesson for all of us reminding us to associate with good people in a good way and that's the

267
00:33:57,640 --> 00:34:17,880
number part of her tonight thank you all for listening

