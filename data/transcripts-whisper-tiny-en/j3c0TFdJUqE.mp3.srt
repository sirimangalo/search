1
00:00:00,000 --> 00:00:14,000
Hi, just a update here on some of the things that I've been involved in recently.

2
00:00:14,000 --> 00:00:28,000
I've recently moved to a place called Moore Park, CA, Moore Park, California, with the hopes of starting a meditation center here.

3
00:00:28,000 --> 00:00:35,000
I've moved around quite a bit lately, trying to find a suitable place.

4
00:00:35,000 --> 00:00:40,000
But I think we've got some good chance of doing it here.

5
00:00:40,000 --> 00:00:51,000
Not to get anybody's hopes up, but hopefully with some luck in the future we'll have a meditation center here.

6
00:00:51,000 --> 00:01:06,000
Now I'm staying in a condominium or rental apartment complex and we have a few apartments that we've been given the use of for the time being.

7
00:01:06,000 --> 00:01:16,000
So if anyone would like to come to meditate, we're welcome to contact us via the website and just let us know when you'd like to come

8
00:01:16,000 --> 00:01:20,000
and we can confirm whether we have a space.

9
00:01:20,000 --> 00:01:31,000
The second thing is that I've started to teach or broadcast my teachings over the internet again.

10
00:01:31,000 --> 00:01:41,000
So I'm teaching here every evening at 7pm to the local meditators.

11
00:01:41,000 --> 00:01:50,000
But I'm also broadcasting it over the internet with this neat service called ustream.tv.

12
00:01:50,000 --> 00:02:03,000
And you can click on the link or not click on the link in the description there or type the link in.

13
00:02:03,000 --> 00:02:16,000
And if you tune in at 7pm on the day is given right now in Tuesday, Thursday and Saturday, I'll be giving an English talk.

14
00:02:16,000 --> 00:02:24,000
And every day we'll be doing meditation from 730 until 9pm.

15
00:02:24,000 --> 00:02:28,000
So you're welcome to take that time to meditate with us.

16
00:02:28,000 --> 00:02:33,000
And hopefully this will be something we can continue on.

17
00:02:33,000 --> 00:02:42,000
Sort of as a supplement for the meditation videos that I've been producing.

18
00:02:42,000 --> 00:02:51,000
The third thing is that the meditation videos, the how to meditate and why meditate videos have now all been given subtitle transcription.

19
00:02:51,000 --> 00:02:57,000
So if you look at those videos now you should be able to see the English subtitles.

20
00:02:57,000 --> 00:03:07,000
You'll also see for some of them that other languages have been produced some translations of the subtitles.

21
00:03:07,000 --> 00:03:14,000
Right now we have Swedish and a couple of Indonesian translations and other people have promised to do other languages.

22
00:03:14,000 --> 00:03:25,000
If anyone would like to help with this you're welcome to download the English subtitle files from the website via the link given.

23
00:03:25,000 --> 00:03:32,000
And just change the English text into your language.

24
00:03:32,000 --> 00:03:35,000
And once you've done that just send it back to me.

25
00:03:35,000 --> 00:03:38,000
You can send it directly to my email, youtube.mo.gmail.com.

26
00:03:38,000 --> 00:03:41,000
I don't answer email that much.

27
00:03:41,000 --> 00:03:43,000
I tend not to have the time.

28
00:03:43,000 --> 00:03:51,000
It seems to be something that I need to shy away from.

29
00:03:51,000 --> 00:04:01,000
But in a case like this something that's useful I do check in and I will be sure to put them up on the internet when I get a chance.

30
00:04:01,000 --> 00:04:10,000
So again thanks for all the interest and I'm happy to see that so many people are keen on the practice of meditation.

31
00:04:10,000 --> 00:04:20,000
I guess one final thing that I'd like to emphasize if I haven't before is the need for a meditation environment.

32
00:04:20,000 --> 00:04:28,000
And I'd like to encourage people that if you have the time come and stay with us for a week or a couple of weeks.

33
00:04:28,000 --> 00:04:39,000
And see what you can learn from the direct interaction with a teacher and with other meditators and the environment that's conducive to meditation.

34
00:04:39,000 --> 00:04:46,000
If you'd like to do that just contact us again in the website and let us know when you like to come.

35
00:04:46,000 --> 00:04:54,000
So that's all for today. This is my video web blog for today. Thanks for tuning in.

