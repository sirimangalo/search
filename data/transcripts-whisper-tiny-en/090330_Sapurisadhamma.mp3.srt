1
00:00:00,000 --> 00:00:13,280
But today I'm going to talk to you about the seven sampuris atamma, the sampuris atamma,

2
00:00:13,280 --> 00:00:24,520
where the dhamma is, the qualities, the virtues of a culture, the civilized, a good

3
00:00:24,520 --> 00:00:38,960
natured person. My translator is good fellow. Samaritan means good, well-behaved,

4
00:00:38,960 --> 00:00:53,560
a cultured, easy, not here, doesn't mean easy going here. Well-behaved, well-civilized,

5
00:00:53,560 --> 00:00:59,000
I guess, is the best word here. Civilized individual. A gentleman would be the English transition.

6
00:00:59,000 --> 00:01:05,000
Puris, Puris, I mean man. But here, of course, it's the same as we used to use in English,

7
00:01:05,000 --> 00:01:10,120
we used to say man, man for everyone. So in the time of the Buddha it was the same,

8
00:01:10,120 --> 00:01:15,000
they used the word man for just about everyone. It could mean women as well, of course.

9
00:01:15,000 --> 00:01:26,280
So the word gentleman means a culture, a civilized individual. So this is sort of the Lord Buddha's

10
00:01:26,280 --> 00:01:40,920
teaching on, let me say it's on social science, or on etiquette, modern manners, or on how to

11
00:01:40,920 --> 00:01:52,200
behave well, or how to live one's life well in society. It's sort of a way to determine whether

12
00:01:52,200 --> 00:01:58,200
someone is a well-behaved, a civilized, a cultured individual, or whether they're just an ordinary

13
00:02:00,200 --> 00:02:07,800
poor, mannered, bore, I'd say, someone without manners, someone without education, without

14
00:02:07,800 --> 00:02:19,000
etiquette, without culture, uncultured individual. And so the Lord Buddha's measure was these seven

15
00:02:19,000 --> 00:02:26,760
things, which we call the Sepulis atam. And these are in Pali, within this translation the Pali is

16
00:02:26,760 --> 00:02:39,880
the Pamanuta, one knows the truth, or the Dhamma reality. Number two, Atanuta, one knows the meaning,

17
00:02:40,840 --> 00:02:50,280
one knows what is the meaning of the truth. Three, Atanuta, one knows oneself.

18
00:02:50,280 --> 00:03:06,280
Four, Atanuta, one knows moderation. Number five, Dalanuta, one knows in regards to time,

19
00:03:06,280 --> 00:03:22,280
knows the right time, has understanding in regards to time. Number six, Bookalaparok or Anuta,

20
00:03:22,280 --> 00:03:28,600
one is able to discriminate between individuals, and Pari, and seven Paris Anuta, one understands

21
00:03:28,600 --> 00:03:38,520
in regards to companies, in regards to groups, in regards to different social groups.

22
00:03:41,560 --> 00:03:48,440
These seven things are sort of what makes up our understanding of how to interact with society,

23
00:03:48,440 --> 00:03:57,000
and how to live our lives as a cultured or as a well-behaved individual from a Buddhist point of view.

24
00:03:57,000 --> 00:04:03,720
And it also directly relates to the meditation, so this isn't simply about how to interact with

25
00:04:03,720 --> 00:04:09,720
society, it's about how to live our lives and how to approach reality, how to approach the

26
00:04:09,720 --> 00:04:17,720
experience of the world, which we have at every moment. So it can be interpreted on different levels.

27
00:04:19,240 --> 00:04:23,960
For instance, the first one, Dalanuta means understanding the truth of reality.

28
00:04:23,960 --> 00:04:31,320
This is most important. The truth means, here Dhamma means anything that is real,

29
00:04:31,880 --> 00:04:37,960
it means reality. Reality is, you could say, all various types of reality. So it means being

30
00:04:37,960 --> 00:04:45,720
able to understand what are good things and what are bad things. You understand that anger is

31
00:04:45,720 --> 00:04:55,080
unwholesome, you understand, or even just understanding about all the different kinds of reality.

32
00:04:56,360 --> 00:05:02,760
So it means understanding about the physical reality and understanding about mental reality.

33
00:05:02,760 --> 00:05:07,800
This is most important because this is where all of our emotions, all of our mental states are.

34
00:05:07,800 --> 00:05:16,920
When we get angry, it's a reality. It's a part of reality and part of being a cultured individual

35
00:05:16,920 --> 00:05:23,560
is understanding things like anger. It means not just following after our emotions,

36
00:05:23,560 --> 00:05:30,120
it means understanding things, knowing what is truth and what is not truth. For instance,

37
00:05:30,120 --> 00:05:36,600
understanding that anger is real, but I am angry, this is only a concept.

38
00:05:36,600 --> 00:05:43,080
When we say, I am an angry individual, this is only a view, this is only our opinion, our understanding

39
00:05:43,080 --> 00:05:52,600
is that this is a part of me, or we can say, you made me angry, or you deserve to

40
00:05:54,360 --> 00:05:59,560
for me to get angry at you. You deserve to be hurt, you deserve punishment.

41
00:05:59,560 --> 00:06:07,080
I am rightful to get angry, I am right to be angry, I am right to be angry.

42
00:06:09,080 --> 00:06:14,600
We have all these ideas. When we, on ordinary individual, just take things in this way,

43
00:06:14,600 --> 00:06:19,400
so when they get angry, they feel like, yeah, I'm really angry at you, and they don't think about

44
00:06:19,400 --> 00:06:24,680
trying to get rid of the anger. They think of trying to get back at the person or the thing

45
00:06:24,680 --> 00:06:32,920
that made them angry. A cultured individual is someone who tries to understand the reality and

46
00:06:32,920 --> 00:06:40,040
tries to make themselves a better person, not trying to change the world around them. We don't yell

47
00:06:40,040 --> 00:06:45,880
at the reason why people don't yell at and scream, do this and do that, all sorts of unholesome

48
00:06:45,880 --> 00:06:49,960
deeds, is because they understand these things and they understand the consequences.

49
00:06:49,960 --> 00:06:56,120
When you do this, when you yell at someone, it makes you look bad, and yell at someone,

50
00:06:56,120 --> 00:07:01,000
it makes them suffer. When you yell at someone, it makes them angry, even return. When you

51
00:07:01,000 --> 00:07:06,600
yell at someone, it makes you feel bad, it makes you feel guilty, and so on. When you yell at

52
00:07:06,600 --> 00:07:14,280
someone, it creates more angry, it makes you an angry person, and so on. All of these understandings

53
00:07:14,280 --> 00:07:20,360
are, it means understanding the dhamma. The second one is understanding the meaning of the dhamma,

54
00:07:20,360 --> 00:07:25,960
so these two go hand in hand. The difference is, for instance, you understand what are good things,

55
00:07:27,080 --> 00:07:33,880
and then you understand what are bad things. When someone tells you love and kindness is a good

56
00:07:33,880 --> 00:07:40,120
thing, anger is a bad thing, this is understanding the dhamma, understanding the reality.

57
00:07:40,120 --> 00:07:45,800
But then the ata is the meaning, it's understanding, what is it? Why is it that love is a good

58
00:07:45,800 --> 00:07:53,880
that kindness is a good thing, loving kindness, or compassion, or the wise mindfulness, a good

59
00:07:53,880 --> 00:07:57,720
thing. So we hear about mindfulness, and we hear it said that it's a good thing, knowing that is

60
00:07:57,720 --> 00:08:03,800
good for us. But atanuta means understanding, why is it good for us? Understanding the meaning

61
00:08:03,800 --> 00:08:12,200
behind it, or it means understanding the meaning of all of these different teachings of the

62
00:08:12,200 --> 00:08:19,240
Lord Buddha. But here specifically, it means understanding why things are wholesome or unwholesome,

63
00:08:19,240 --> 00:08:27,080
why things are good and why things are bad. So for instance, we understand that anger is a bad

64
00:08:27,080 --> 00:08:32,120
thing for all of the reasons I explain, because it makes us look bad, whereas a very shallow reason,

65
00:08:32,120 --> 00:08:36,840
because it makes people upset as a better reason, because it makes them angry at us and want to

66
00:08:36,840 --> 00:08:42,120
get revenge as an even better reason. And simply because it makes us feel bad, makes us feel guilty,

67
00:08:42,120 --> 00:08:48,200
makes us feel upset as a really good reason. The best reason is that it makes us more angry,

68
00:08:48,760 --> 00:08:54,520
and it leaves a scar on who we are, and it changes who we are, as we do bad things and say bad things.

69
00:08:54,520 --> 00:09:01,800
And when we extend love to other people, we feel good, they feel good, they remember us,

70
00:09:01,800 --> 00:09:07,720
they want to help us, they want to do good things for us, and our minds become pure, our minds become

71
00:09:07,720 --> 00:09:12,920
clear and so on. When we have mindfulness, it's even better because it helps us to understand all

72
00:09:12,920 --> 00:09:17,560
of the good things and bad things inside, and understand why they are good and why they are bad.

73
00:09:17,560 --> 00:09:23,720
It's easy for us to explain about these things and sort of get appearing, oh yeah, yeah, so anger is not good,

74
00:09:24,840 --> 00:09:28,760
but simply because we've heard that it's not good, it doesn't mean that we're not getting

75
00:09:28,760 --> 00:09:34,600
an angry anymore, because we think we understand it, but what happens is we just have this thought

76
00:09:34,600 --> 00:09:40,680
that sounds good. We haven't really experienced for ourselves the disadvantages of things like anger,

77
00:09:40,680 --> 00:09:48,360
but when you have mindfulness and you focus and you say angry, angry or so on, you understand

78
00:09:48,360 --> 00:09:52,200
really that anger, and truly that anger is not a good thing, and no one has to tell you,

79
00:09:52,200 --> 00:09:59,160
and no one has to explain to you, and it's very clear to you the disadvantages of getting angry,

80
00:09:59,880 --> 00:10:05,480
so this is understanding the meaning or understanding the higher up-tap, the higher

81
00:10:05,480 --> 00:10:13,640
core essence of the Buddhist teaching. These are the first two, the third one is understanding

82
00:10:13,640 --> 00:10:21,080
yourself. So on the level of society, it means understanding who we are in our place in society,

83
00:10:21,800 --> 00:10:26,600
in regards to our parents, understanding responsibilities, we have towards our parents,

84
00:10:26,600 --> 00:10:32,440
and I am the sander, the daughter, and I have a responsibility towards my parents,

85
00:10:32,440 --> 00:10:36,840
and we have some sort of responsibility generally to our parents because of all the good that

86
00:10:36,840 --> 00:10:43,800
they've done to us, and this is something we generally forget, and they've had their wishes

87
00:10:43,800 --> 00:10:50,200
placed in us for all of our lives, hoping that we'd be happy and we'd be well off and we'd do

88
00:10:51,000 --> 00:10:56,440
good things, and they've given us so much, and so we have some kind of responsibility to

89
00:10:56,440 --> 00:11:06,520
live our lives and to make our lives worthwhile and to do things to further ourselves and develop

90
00:11:06,520 --> 00:11:12,840
ourselves. The thing that makes our parents most happy is not that we give them things, not that

91
00:11:12,840 --> 00:11:19,080
we somehow try to pay them back monetarily, that rarely, if ever, makes parents truly happy.

92
00:11:19,080 --> 00:11:28,280
I was once a woman who she came to practice meditation with me and first she didn't want to say

93
00:11:28,280 --> 00:11:35,320
it, but after a while she told me she was a prostitute and she was doing it for her mother,

94
00:11:36,840 --> 00:11:40,040
and I said to her, I said, do you really think it's making it would make your mother happy if

95
00:11:40,040 --> 00:11:45,160
she knew that you were a prostitute? And I said, and she said, no, and I said, what do you think,

96
00:11:45,160 --> 00:11:49,560
money is really going to make her happy and she said, no, I said, do you think if you were happy

97
00:11:49,560 --> 00:11:55,720
she wouldn't be happy and she said, yes, and so she really thought about this and she decided to

98
00:11:55,720 --> 00:12:03,960
change her life that she wouldn't go on and do this anymore. Of course our parents want us to develop

99
00:12:03,960 --> 00:12:09,800
ourselves, want to see that we are on the right path, and sometimes we can even show them what

100
00:12:09,800 --> 00:12:15,480
is the right path. When we get on a path or get in gain understanding that they don't have,

101
00:12:15,480 --> 00:12:21,960
we can actually become teachers of our parents that we go out and try to lecture them, but when

102
00:12:21,960 --> 00:12:26,440
they see us as an example, they want to follow something they've never realized before.

103
00:12:27,400 --> 00:12:31,560
We can actually, then we can truly pair our parents back if we help them to get on the right path.

104
00:12:31,560 --> 00:12:38,040
This is an example, but it means understanding our role in life, understanding who we are.

105
00:12:39,880 --> 00:12:44,760
For example, with monks, it's a clear example of monks we have to sit according to seniority.

106
00:12:45,400 --> 00:12:50,520
So the example they give is knowing yourself is knowing not to sit in front of the senior

107
00:12:50,520 --> 00:12:56,120
monks, not to sit behind junior monks, but this sort of applies in society as well,

108
00:12:56,120 --> 00:13:04,040
understanding that you're the employee and not yelling or getting all hotty towards your boss.

109
00:13:05,080 --> 00:13:10,120
This is useful because it allows us to keep our job and so on. When we're angry at our boss

110
00:13:10,120 --> 00:13:16,920
knowing how to keep it, keep a lid on it and so on. Understanding our place when we have teachers

111
00:13:16,920 --> 00:13:26,600
and not getting angry or distaining our teachers having respect and gratitude for our teachers,

112
00:13:26,600 --> 00:13:30,280
even they might be getting paid if we're in school or they might be getting money for it,

113
00:13:30,280 --> 00:13:34,040
but we simply think that these people are giving us something and giving us knowledge.

114
00:13:37,080 --> 00:13:40,280
We don't have to think about the fact that they're getting paid. This is a problem in the West,

115
00:13:40,280 --> 00:13:44,520
you should be because most of the teachers in public school, high school, university,

116
00:13:44,520 --> 00:13:50,360
they get very well and they're actually generally quite well paid. So the students often have

117
00:13:50,360 --> 00:13:55,880
very little respect for them, but this shouldn't be the way we take things if we're the student,

118
00:13:57,400 --> 00:14:01,000
where they're to learn this person is giving us the highest gift, which is knowledge.

119
00:14:02,600 --> 00:14:06,440
So we take it like that and we understand our role as a student and we try to make their job

120
00:14:06,440 --> 00:14:11,480
easier for them because then they want to teach us and they'll even give us extra teaching.

121
00:14:11,480 --> 00:14:14,920
They'll give us the best teaching, they can, they'll feel comfortable teaching us.

122
00:14:15,720 --> 00:14:18,360
Otherwise it's unlikely that we're going to learn anything,

123
00:14:18,360 --> 00:14:24,280
that we're always rude and considerate towards our teachers. As an example,

124
00:14:24,920 --> 00:14:32,200
understanding our place in every situation, knowing what we're supposed to do in every situation,

125
00:14:32,200 --> 00:14:36,920
this is on a very shallow level, but as we get deeper, we understand that actually the

126
00:14:36,920 --> 00:14:42,280
meditation comes into play here. Understanding oneself is understanding what one is doing

127
00:14:42,280 --> 00:14:45,480
and what is one is receiving from the outside world at every moment.

128
00:14:46,520 --> 00:14:50,280
So when you're in a situation where someone is yelling or is angry or so on,

129
00:14:50,280 --> 00:14:55,240
being able to stay calm, understanding your own emotions, feeling your own anger come up,

130
00:14:55,720 --> 00:14:58,440
when you see something you want, feeling your own desire come up,

131
00:14:59,000 --> 00:15:03,800
and being able to stay calm, being able to keep your cool at all times.

132
00:15:03,800 --> 00:15:08,440
This means understanding yourself, understanding what you're doing and what's happening.

133
00:15:09,000 --> 00:15:14,840
So normally when we get angry, we write away yell and say about things and do bad things

134
00:15:14,840 --> 00:15:20,920
or want to hurt other people. When we are cultured individuals, when we develop ourselves,

135
00:15:21,640 --> 00:15:28,840
civilization is that we're able to, we're able to keep a lid on it and we're able to

136
00:15:28,840 --> 00:15:35,880
know and be in time to stop ourselves from doing bad things or saying bad things and so on.

137
00:15:36,440 --> 00:15:41,560
And even understanding our own minds, keeping our minds from giving rise to these bad things,

138
00:15:41,560 --> 00:15:46,520
these bad emotions. So when we hear things, we can just say hearing, hearing, even if it's

139
00:15:46,520 --> 00:15:51,640
angry or bitter words, we can just say to ourselves hearing, hearing if someone's yelling at us

140
00:15:51,640 --> 00:15:58,360
for instance, or if someone's doing something we don't like, we can say angry, anger if we see,

141
00:15:58,360 --> 00:16:03,960
just a seeing, but when the emotions arise, also catching them and being aware of them

142
00:16:05,240 --> 00:16:08,440
and not letting them lead us to do bad things or say bad things and so on.

143
00:16:10,760 --> 00:16:16,440
This is number three, knowing ourselves, knowing all about ourselves,

144
00:16:16,440 --> 00:16:21,160
you know what we're doing, what's happening inside of ourselves and also understanding our place

145
00:16:21,160 --> 00:16:26,040
in life. Number four is muta, muta, understanding moderation,

146
00:16:27,480 --> 00:16:34,280
knowing moderation and all things. The general example is knowing moderation and food and it's

147
00:16:34,280 --> 00:16:40,520
amazing how how important food actually is in a spiritual life. You can see this is

148
00:16:41,240 --> 00:16:45,400
understood in many spiritual practices and in Buddhism it's very much understood as well.

149
00:16:45,400 --> 00:16:53,240
We take food to be a very important part of spiritual life or religious life and it doesn't

150
00:16:53,240 --> 00:16:58,760
mean not eating this or not eating that. It means eating enough. Buddhism, the religious aspect

151
00:16:58,760 --> 00:17:04,760
of food and Buddhism is knowing enough doesn't matter that you have to eat this or this or not

152
00:17:04,760 --> 00:17:10,760
eat that or that. It means if you eat too much, this is where the problems start to come in.

153
00:17:11,560 --> 00:17:15,160
See if you get picky about what you're eating and so on and so on, then you start to have to

154
00:17:15,160 --> 00:17:23,000
have to have to worry and have to think a lot about what's what you're going to eat and what

155
00:17:23,000 --> 00:17:27,240
you're going to get and you have to try to get the right foods and so on. But if you just eat

156
00:17:27,240 --> 00:17:31,800
enough to survive and it's actually amazing how little you have to worry about food. You can take

157
00:17:31,800 --> 00:17:37,320
whatever. You can eat a lot of food that other people can't eat. If you eat say one meal a day

158
00:17:37,320 --> 00:17:41,720
in the morning, it's amazing the things you can eat that other people can't. You can eat ice

159
00:17:41,720 --> 00:17:46,280
cream. You can eat donuts. You can eat really whatever you want because in the end, none of it's

160
00:17:46,280 --> 00:17:53,080
going to be there by the end of the day. You're only eating ones and it doesn't mean you can go

161
00:17:53,080 --> 00:18:01,720
overboard but it means if you eat in moderation, this is a very important part of your freedom and

162
00:18:01,720 --> 00:18:11,320
your ability to stay at peace and unconcerned with such trivialities like finding food or finding

163
00:18:11,320 --> 00:18:17,400
the right food or avoiding this food, avoiding that food. And you find that your digestive system

164
00:18:17,400 --> 00:18:22,680
works much better and you're so on free from so many illnesses. So food is a very important part

165
00:18:22,680 --> 00:18:29,480
knowing moderation and food. This is something we often miss when we approach Buddhism is the importance

166
00:18:29,480 --> 00:18:36,120
of of knowing moderation and food not eating too little, not eating too much. But it can be

167
00:18:36,120 --> 00:18:43,080
moderation and all things, understanding moderation and sensual pleasures for instance. Now when

168
00:18:43,080 --> 00:18:47,400
it comes right down to it, when we're in meditation, we have to give up all sensual pleasures

169
00:18:47,400 --> 00:18:54,440
but for ordinary people living their lives, at the very least, we have to moderate these things.

170
00:18:54,440 --> 00:19:02,840
We also have to moderate music or entertainment or even all sorts of

171
00:19:02,840 --> 00:19:11,480
all sorts of diversions that we have, I guess things like movies or going to see shows or even

172
00:19:11,480 --> 00:19:17,080
talking with other people or of course sexual activity, these things we have to moderate because

173
00:19:17,080 --> 00:19:23,640
they're incredibly addictive and as we indulge again and again and again these things become

174
00:19:24,520 --> 00:19:28,920
something that we need to become an addiction for us.

175
00:19:28,920 --> 00:19:37,560
You can also mean moderation in meditation practice. Sometimes people get a little bit off track

176
00:19:37,560 --> 00:19:42,120
thinking that they have to walk and sit walk and sit walk and sit walk and sit for many,

177
00:19:42,120 --> 00:19:49,080
many hours a day and this can be useful at times but it actually can be detrimental if it goes

178
00:19:49,080 --> 00:19:54,680
overboard if we push ourselves beyond our limits and so we have to know moderation here as well.

179
00:19:54,680 --> 00:19:59,320
We have to understand that it's not necessary to do formal meditation practice all day.

180
00:20:00,120 --> 00:20:04,440
It's necessary to be mindful all the time but we can do this when we're taking a break

181
00:20:04,440 --> 00:20:10,440
and we shouldn't try to take a break. We shouldn't try to slowly increase the amount of time

182
00:20:10,440 --> 00:20:16,760
we do meditation practice and when we start to get stressed when we start to get over exerted we have

183
00:20:16,760 --> 00:20:22,680
to step back and take a break, sit comfortably, go for a walk or the washroom or so and have a

184
00:20:22,680 --> 00:20:28,120
cup of tea or so. This can be quite useful in bringing the mind back to its natural state if we've

185
00:20:28,120 --> 00:20:35,880
gotten off track for instance. So this is number four understanding moderation. Number five is

186
00:20:35,880 --> 00:20:44,520
understanding time. Understanding time on a superficial level in everyday life situation it means

187
00:20:44,520 --> 00:20:53,240
understanding understanding the right time to do things means being on time for things.

188
00:20:54,200 --> 00:21:00,280
It means understanding good lengths of time when you have to give talks not talking too much.

189
00:21:01,240 --> 00:21:06,120
It means things like practicing meditation when we practice meditation not meditating for

190
00:21:06,120 --> 00:21:11,480
too long of a time not meditating to short of a time not getting involved in things that waste our

191
00:21:11,480 --> 00:21:18,120
time. Understanding the importance of time and the importance of the opportunity that we have

192
00:21:18,120 --> 00:21:23,720
to come in practice meditation not wasting this time that we have. We have a very short time

193
00:21:23,720 --> 00:21:28,200
and understanding that it's a very short time. Even the time we have left on this earth

194
00:21:29,400 --> 00:21:34,360
could be very short. We don't any of us know when how much longer we have.

195
00:21:34,360 --> 00:21:41,480
And understanding this is very important to help us to continue to practice. Understanding when

196
00:21:41,480 --> 00:21:48,280
is the right time to do this to do that. Understanding being able to organize our time.

197
00:21:49,640 --> 00:21:55,320
This is an important sign of a civilized individual. You can see this when people are late. Everyone

198
00:21:55,320 --> 00:22:03,720
gets angry. When people take too long for something people get upset. They think that this person

199
00:22:03,720 --> 00:22:13,880
doesn't understand in regards to how long or how much time to take. On a meditative level I think

200
00:22:13,880 --> 00:22:19,480
there's another important aspect of time which plays a very integral part of Buddhist meditation

201
00:22:19,480 --> 00:22:26,520
practice. Understanding time means understanding the difference between the different modes of time

202
00:22:26,520 --> 00:22:34,200
and the different time frames or the different times. We have three times in Buddhism this

203
00:22:34,200 --> 00:22:40,680
is the past, the present and the future. Understanding the difference between the past, the

204
00:22:40,680 --> 00:22:46,920
present and the future in regards to the present moment. Understanding the present moment.

205
00:22:46,920 --> 00:22:53,960
Understanding that all there is in reality is the present moment. Understanding when

206
00:22:53,960 --> 00:22:59,480
our mind is going off into the past and seeing it simply as a thought that arises in the present

207
00:22:59,480 --> 00:23:06,120
moment. I'm not getting caught up in the past. The most important understanding of time that we can

208
00:23:06,120 --> 00:23:14,680
have is keeping the ability to keep our minds in the present moment. Understanding of reality is

209
00:23:14,680 --> 00:23:20,120
being the present moment and this practice of not letting our minds wander off into the past.

210
00:23:20,120 --> 00:23:25,240
Understanding about the past that it's already gone. That's the word Buddha said,

211
00:23:25,240 --> 00:23:28,920
you know, you're that deep down for human time. What is in the past is gone already.

212
00:23:29,880 --> 00:23:32,920
Apatantya and Akatam, what's in the future, hasn't come yet.

213
00:23:34,600 --> 00:23:41,000
Understanding in this way is so that when we focus on meditation that we're actually in the present

214
00:23:41,000 --> 00:23:47,480
moment. So when we walk in meditation step being right, step being left, we're actually saying

215
00:23:47,480 --> 00:23:52,360
to ourselves step being right as the foot is moving. So we're not thinking about something in the

216
00:23:52,360 --> 00:23:57,560
past. We're thinking about something in the future. We're aware of the thing as it happens and

217
00:23:57,560 --> 00:24:03,320
our mind is with emotion as it happens. When we say it, there may say rise in falling all

218
00:24:03,320 --> 00:24:08,360
exactly the same. Being able to see things in the present moment not letting our minds go back to

219
00:24:08,360 --> 00:24:13,400
the past or worry about the future. This is very important in our lives as well. You can see

220
00:24:13,400 --> 00:24:19,240
people destroy themselves by worrying about the future. Create so much stress and suffering

221
00:24:19,240 --> 00:24:24,760
simply by worrying or mourning about the past, about stressing over the future.

222
00:24:26,120 --> 00:24:31,800
And so this is a sign that these people have more work to do. These people are lacking something.

223
00:24:31,800 --> 00:24:39,720
And this is Galan, you understand about these frames of time that the past has gone and the

224
00:24:39,720 --> 00:24:48,360
future has become and so on. So this is number five. Number six is understanding in regards to

225
00:24:48,360 --> 00:24:57,560
various individuals. This is an important one in regards to our understanding about who we should

226
00:24:58,760 --> 00:25:03,960
associate with. Being able to see the difference between people who are on the right path and who

227
00:25:03,960 --> 00:25:08,920
are on the wrong path. Being able to see the difference between people who are going to be a support

228
00:25:08,920 --> 00:25:15,080
for our own development and people who are going to be an detriment. People who are going to lead us

229
00:25:15,080 --> 00:25:22,120
in the wrong path a waste our time, people who are going to give us a bad name as a one sort of

230
00:25:22,120 --> 00:25:31,320
shallow example. People who are going to maybe make it difficult. Sometimes just by associating

231
00:25:31,320 --> 00:25:35,800
with the wrong people that makes it difficult for the right people to come and associate with

232
00:25:35,800 --> 00:25:43,720
you. If your friends are all on the wrong path it's very difficult for you to find good people and

233
00:25:43,720 --> 00:25:51,720
to meet and to share with good people because of your association. So this is something we sometimes

234
00:25:51,720 --> 00:25:56,840
don't understand that it's simply because we call someone a friend. It doesn't mean it's actually

235
00:25:56,840 --> 00:26:03,480
beneficial for either of us a relationship. Sometimes even we have to back off and we have to say

236
00:26:03,480 --> 00:26:08,600
that say to ourselves that this person our association with them in this way is not beneficial for

237
00:26:08,600 --> 00:26:17,080
either of us. We're just leading each other down the wrong path. Often really the most useful

238
00:26:17,080 --> 00:26:23,000
thing for us when we're training in meditation is to not get involved with anybody. But here it

239
00:26:23,000 --> 00:26:29,480
specifically means understanding differences in regards to individuals. So when someone's on a

240
00:26:29,480 --> 00:26:34,360
level where they can't really understand things like meditation or mental development,

241
00:26:34,360 --> 00:26:40,120
we have to be able to deal with them in a certain way. We can't go in and talk with them about all

242
00:26:40,120 --> 00:26:44,920
of these higher things. We have to understand the level that they're on. This is when it's

243
00:26:44,920 --> 00:26:50,920
meant by Pukalaparopar and understanding how to deal with different types of people.

244
00:26:51,720 --> 00:26:58,840
And when people are well-developed meditators understanding to go to talk with them and to ask

245
00:26:58,840 --> 00:27:06,120
questions and to understand which people we should ask advice from. Sometimes people

246
00:27:06,120 --> 00:27:10,200
they just look at a monk and they see that he's wearing robes and they think immediately he must

247
00:27:10,200 --> 00:27:15,480
be wise. He must understand everything. We have to be more careful than that and we have to actually

248
00:27:15,480 --> 00:27:21,720
look at the person behind the robes for instance. In all cases when we approach people and we hear

249
00:27:21,720 --> 00:27:27,560
that this person's a meditation teacher, this person's a monk and so on, we shouldn't just immediately

250
00:27:27,560 --> 00:27:32,760
assume that that means that anything they tell us is going to be enlightening, it's going to be

251
00:27:32,760 --> 00:27:41,640
true and we have to be able to see and to understand the difference between what makes a person

252
00:27:43,560 --> 00:27:50,840
a good teacher for instance. And the final one is understanding companies, different companies

253
00:27:50,840 --> 00:27:58,680
and this means understanding how to act in various societies. So when we are dealing with

254
00:28:00,840 --> 00:28:07,480
this American people for instance versus Thai people understanding how to deal with older people,

255
00:28:07,480 --> 00:28:15,720
understanding how to deal with younger people, understanding when we go into a social circumstance,

256
00:28:15,720 --> 00:28:25,400
understanding how to carry out in various social circles. This is something that we can

257
00:28:26,520 --> 00:28:32,520
really see the benefit of meditation practice because often they become quite overwhelming when

258
00:28:32,520 --> 00:28:39,000
we have to deal with people and of high status or people and when we have to meet famous people

259
00:28:39,000 --> 00:28:45,800
and we have to meet people who are very important when we have to go to meetings, when we have to

260
00:28:47,320 --> 00:28:56,920
go to talk with people of various stature. When we use the meditation practice we can really

261
00:28:56,920 --> 00:29:01,960
watch ourselves and can really keep our own balance. Another thing is sometimes you go into

262
00:29:01,960 --> 00:29:08,440
groups of people and they are so unfocused. Their minds are so off-balance, sometimes you go into

263
00:29:09,800 --> 00:29:18,200
parties or you stumble upon groups of people whose minds are so disturbed and unfocused,

264
00:29:18,200 --> 00:29:23,880
distracted, that it can be very disconcerting and you are not sure exactly how to act and you find

265
00:29:23,880 --> 00:29:29,800
yourself following after them and losing your focus as well. Meditation practice is very important

266
00:29:29,800 --> 00:29:34,760
when we go out into society, when we are at work for instance, we often realize that all the

267
00:29:34,760 --> 00:29:40,360
people around us are incredibly stressed and the meditation practice is something which helps us

268
00:29:40,360 --> 00:29:46,600
to balance ourselves and pull ourselves away from them and not have to follow after these people.

269
00:29:47,400 --> 00:29:53,880
We often find ourselves, it is very easy to we want to ingratiate ourselves and we want to

270
00:29:53,880 --> 00:30:01,800
create a sense of harmony with all the people around us. This can be very dangerous for our

271
00:30:01,800 --> 00:30:07,320
own well-being when we try to harmonize with the people around us and say things that makes them

272
00:30:07,320 --> 00:30:13,800
happy and so on. Sometimes the best thing is to cut ourselves off from them and to be able to

273
00:30:13,800 --> 00:30:20,040
sit back and approach them mindfully, not approach them on their terms but approach them on our

274
00:30:20,040 --> 00:30:26,360
terms and this is often the best thing for ourselves and the best thing for the people around us.

275
00:30:26,360 --> 00:30:31,640
They are able to use us as a mirror because everything they say it simply bounces off and we don't

276
00:30:31,640 --> 00:30:36,600
take it in when they say something or when they do something when they start to act all flustered

277
00:30:37,400 --> 00:30:43,400
that we are able to be mindful and not let it make us bother or flustered or upset

278
00:30:44,200 --> 00:30:47,880
and so they have to deal with it themselves and they are able to see the difference between them

279
00:30:47,880 --> 00:30:54,440
and us and they are able to see the disadvantages of their behavior and so on and we can see through

280
00:30:54,440 --> 00:30:59,240
the meditation practice that we can actually change the people around us, we can actually change

281
00:30:59,240 --> 00:31:07,320
many situations. This is a very important part of our understanding of how to act in certain

282
00:31:07,320 --> 00:31:15,720
situations in society and this is a part of what it means to understand the various companies,

283
00:31:15,720 --> 00:31:20,600
the various groups of people and various societies and understand understanding the differences

284
00:31:20,600 --> 00:31:31,720
and being able to adapt ourselves to the circumstances. All together these are the things that

285
00:31:31,720 --> 00:31:37,240
Lord Buddha said which make a person culture and which make a person a gentleman or a gentleman

286
00:31:37,240 --> 00:31:44,760
or a lady you can say make someone culture or civilized and you can see it has a lot to do with

287
00:31:44,760 --> 00:31:51,560
the meditation practice as we start to practice meditation our minds become more pure and more adaptable

288
00:31:52,440 --> 00:31:57,800
and more in tune with reality so we are able to understand what is the right thing to do

289
00:31:57,800 --> 00:32:03,640
and the right thing to say at every moment. We are able to catch our mistakes so when we make a

290
00:32:03,640 --> 00:32:08,760
mistake we can see very clearly that that was the wrong thing to do. Why because we are here to

291
00:32:08,760 --> 00:32:15,880
feel the vibrations, we are here to feel the consequences of our actions. So it is very easy for us

292
00:32:15,880 --> 00:32:23,400
to adapt ourselves and to change ourselves and to develop ourselves and to become someone who

293
00:32:23,400 --> 00:32:28,360
is civilized and cultured and who understands all these things, understands how to react to various

294
00:32:28,360 --> 00:32:34,920
people, various individuals, various groups, who understands about moderation and time, who understands

295
00:32:34,920 --> 00:32:41,400
about themselves and understands about reality. So these are the all together the seven things

296
00:32:41,400 --> 00:32:47,800
which the Lord Buddha said, the sub-puri satamma and this is the dhamma that I wanted to impart

297
00:32:47,800 --> 00:32:54,600
on this occasion and that is all for today. So everyone please continue meditating and we will see

298
00:32:54,600 --> 00:33:10,520
you from reporting at 3 o'clock.

