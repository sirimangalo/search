1
00:00:00,000 --> 00:00:13,000
If mind is just a concept, what is that gets reborn reincarnated, that carries our karma, a little confused?

2
00:00:13,000 --> 00:00:18,000
I guess I have to start now.

3
00:00:18,000 --> 00:00:23,000
Please speak up when you have something to say.

4
00:00:23,000 --> 00:00:28,000
Nothing gets reborn.

5
00:00:28,000 --> 00:00:29,000
Nothing gets reborn.

6
00:00:29,000 --> 00:00:31,000
There is a first statement.

7
00:00:31,000 --> 00:00:34,000
Nothing gets reincarnated.

8
00:00:34,000 --> 00:00:36,000
Nothing carries karma.

9
00:00:36,000 --> 00:00:40,000
Karma itself doesn't exist.

10
00:00:40,000 --> 00:00:44,000
And those are all the statements that I would make to start off my answer.

11
00:00:44,000 --> 00:00:47,000
They are verifiable or deniable statements.

12
00:00:47,000 --> 00:00:51,000
And I would add another one in there is confusion exists.

13
00:00:51,000 --> 00:00:54,000
Obviously what you have in your mind now is confusion.

14
00:00:54,000 --> 00:01:01,000
And that little later on we can explain how that is much more important than understanding

15
00:01:01,000 --> 00:01:04,000
the answer to this question.

16
00:01:04,000 --> 00:01:14,000
In fact, something that isn't often fully appreciated is that you don't really need answers

17
00:01:14,000 --> 00:01:16,000
to these sorts of questions.

18
00:01:16,000 --> 00:01:20,000
Often answering these questions does assuage your doubt.

19
00:01:20,000 --> 00:01:27,000
And it's another one of these cases of just avoiding the real problem is that you have this sort of doubt

20
00:01:27,000 --> 00:01:28,000
in the first place.

21
00:01:28,000 --> 00:01:29,000
Which we all do.

22
00:01:29,000 --> 00:01:31,000
I'm not trying to pick on you.

23
00:01:31,000 --> 00:01:39,000
But it's very hard for Western people to get their minds around the concept that you don't

24
00:01:39,000 --> 00:01:41,000
need an answer to this question.

25
00:01:41,000 --> 00:01:43,000
Because I think, well, I have doubt.

26
00:01:43,000 --> 00:01:46,000
So you have to solve that to get rid of your doubt.

27
00:01:46,000 --> 00:01:47,000
And actually you don't.

28
00:01:47,000 --> 00:01:51,000
If you look at the doubt, you'll see that it's useless.

29
00:01:51,000 --> 00:01:55,000
And you'll see that all doubt is useless.

30
00:01:55,000 --> 00:01:56,000
And guess what?

31
00:01:56,000 --> 00:02:00,000
Once you see that all doubt is useless, you're enlightened.

32
00:02:00,000 --> 00:02:02,000
No, it's not quite that simple.

33
00:02:02,000 --> 00:02:04,000
It's not just a reflective thing.

34
00:02:04,000 --> 00:02:08,000
But the experience of enlightenment does away with doubt.

35
00:02:08,000 --> 00:02:09,000
The sort of pattern.

36
00:02:09,000 --> 00:02:16,000
A sort of pattern is free from the mental state of doubt.

37
00:02:16,000 --> 00:02:18,000
Do it to a certain extent.

38
00:02:18,000 --> 00:02:20,000
At the very least, doubt is a hindrance.

39
00:02:20,000 --> 00:02:28,000
It's something that distracts and crushes the mind's potential.

40
00:02:28,000 --> 00:02:30,000
It halts the mind in its tracks.

41
00:02:30,000 --> 00:02:38,000
So it stops you from seeing clearly, it stops you from finding peace and so on.

42
00:02:38,000 --> 00:02:43,000
And so one way of understanding the practice is just the overcoming of doubt.

43
00:02:43,000 --> 00:02:46,000
Or the giving up of things like doubt.

44
00:02:46,000 --> 00:02:52,000
And if all you were to do is focus on this doubt, all kinds of doubt.

45
00:02:52,000 --> 00:02:55,000
And overcome that.

46
00:02:55,000 --> 00:02:58,000
I think you would be doing enough.

47
00:02:58,000 --> 00:03:02,000
And so in that sense, you don't need answers to any question.

48
00:03:02,000 --> 00:03:06,000
You don't need even the ones that seem so important.

49
00:03:06,000 --> 00:03:09,000
And this is an important question that people have.

50
00:03:09,000 --> 00:03:11,000
Is there such a thing as a rebirth?

51
00:03:11,000 --> 00:03:14,000
I've even said to me, I can't practice unless I believe,

52
00:03:14,000 --> 00:03:20,000
unless I have something to convince me in the truth of reincarnation of rebirth.

53
00:03:20,000 --> 00:03:25,000
Buddhism seems to make no sense to me unless I can solve this question.

54
00:03:25,000 --> 00:03:27,000
Answer this question.

55
00:03:27,000 --> 00:03:30,000
Which is a very pitiful state to me, right?

56
00:03:30,000 --> 00:03:33,000
Because you may never have, you probably will.

57
00:03:33,000 --> 00:03:36,000
Probably will never have the proof that you're looking for.

58
00:03:36,000 --> 00:03:39,000
Science doesn't even give proofs, right?

59
00:03:39,000 --> 00:03:48,000
No, the only way to prove it would be to experience it for yourself, right?

60
00:03:48,000 --> 00:03:53,000
Which puts you in a precarious position of having to wait until you pass away.

61
00:03:53,000 --> 00:03:58,000
Either that, or what normally people are hinting at, in this case,

62
00:03:58,000 --> 00:04:02,000
is that they really, really, really want to remember their past lives.

63
00:04:02,000 --> 00:04:04,000
Because it's so cool.

64
00:04:04,000 --> 00:04:08,000
And so they cover it up with this idea that they have to, in order to appreciate Buddhism,

65
00:04:08,000 --> 00:04:15,000
which is, which is, in that sense, kind of deceptive.

66
00:04:15,000 --> 00:04:17,000
Because it's just greed.

67
00:04:17,000 --> 00:04:18,000
Okay.

68
00:04:18,000 --> 00:04:19,000
Way beside the point.

69
00:04:19,000 --> 00:04:21,000
But very much to the point.

70
00:04:21,000 --> 00:04:27,000
And that's how we should, we should, the conclusion we should come to.

71
00:04:27,000 --> 00:04:37,000
But to directly answer your question, because for sure that will help to make the doubt easier to deal with.

72
00:04:37,000 --> 00:04:43,000
I can't do better, I think, than to point to the video I made on the nature of reality.

73
00:04:43,000 --> 00:04:48,000
And I, actually, I would think you've probably seen that video,

74
00:04:48,000 --> 00:04:53,000
which makes me wonder if I did cover it in enough detail.

75
00:04:53,000 --> 00:05:00,000
But it's not really what the Buddha would say, but I've said it several times already,

76
00:05:00,000 --> 00:05:05,000
is that Buddhist, it's not that Buddhist believe in rebirth.

77
00:05:05,000 --> 00:05:08,000
It's that Buddhist don't believe in death.

78
00:05:08,000 --> 00:05:13,000
And, honestly, I have to think that this is something, not something the words the Buddha may have used,

79
00:05:13,000 --> 00:05:18,000
but it does accurately reflect the Buddha's teaching as I understand it.

80
00:05:18,000 --> 00:05:23,000
Because death in Buddhism, the Buddha often talked about when a person dies,

81
00:05:23,000 --> 00:05:24,000
and he used these words.

82
00:05:24,000 --> 00:05:26,000
And I used these, we all used this word.

83
00:05:26,000 --> 00:05:36,000
But from a doctrinal point of view and from a core doctrine,

84
00:05:36,000 --> 00:05:39,000
in terms of core doctrine, death occurs at every moment.

85
00:05:39,000 --> 00:05:42,000
That's what we're talking about when we say the mind doesn't exist.

86
00:05:42,000 --> 00:05:49,000
Mind is just a designation for these momentary experiences.

87
00:05:49,000 --> 00:05:54,000
The mental side of the momentary experiences, the awareness of the object,

88
00:05:54,000 --> 00:06:00,000
the awareness of an object that is part of an experience.

89
00:06:00,000 --> 00:06:07,000
So, when the body dies, we're totally dealing with concepts at that point,

90
00:06:07,000 --> 00:06:09,000
because nothing died.

91
00:06:09,000 --> 00:06:12,000
Mind and body are arising and ceasing and rising.

92
00:06:12,000 --> 00:06:16,000
Physical and mental are arising and ceasing and ceasing.

93
00:06:16,000 --> 00:06:18,000
And that doesn't stop.

94
00:06:18,000 --> 00:06:21,000
I think the video is really good.

95
00:06:21,000 --> 00:06:24,000
It arose out of a conversation I had with my father,

96
00:06:24,000 --> 00:06:26,000
and eventually an article I wrote about it,

97
00:06:26,000 --> 00:06:29,000
and then continuously getting this question I thought,

98
00:06:29,000 --> 00:06:30,000
this might help.

99
00:06:30,000 --> 00:06:33,000
I don't think it helps as much as I would have liked,

100
00:06:33,000 --> 00:06:37,000
because of the nature of the human mind, right?

101
00:06:37,000 --> 00:06:40,000
Doubt is insidious.

102
00:06:40,000 --> 00:06:48,000
It's a doubt is not a sign that you need an answer.

103
00:06:48,000 --> 00:06:50,000
It's a sign that you've got a problem.

104
00:06:50,000 --> 00:06:52,000
The problem is the doubt.

105
00:06:52,000 --> 00:06:54,000
And so you can see this in people.

106
00:06:54,000 --> 00:06:57,000
They get an answer to one question, and they have ten more.

107
00:06:57,000 --> 00:06:59,000
Why?

108
00:06:59,000 --> 00:07:04,000
Though the why is not always because they didn't get a sufficient answer.

109
00:07:04,000 --> 00:07:06,000
It's because they're addicted to doubting.

110
00:07:06,000 --> 00:07:09,000
And there's a pleasure that comes from it, right?

111
00:07:09,000 --> 00:07:10,000
It can be a desire.

112
00:07:10,000 --> 00:07:13,000
It can be caught up in desire.

113
00:07:13,000 --> 00:07:15,000
They don't want to go meditate.

114
00:07:15,000 --> 00:07:17,000
We didn't even meditate tonight.

115
00:07:17,000 --> 00:07:19,000
They don't want to go meditate.

116
00:07:19,000 --> 00:07:24,000
They want to ask questions, and philosophize,

117
00:07:24,000 --> 00:07:29,000
and give rise to this rush of getting some conclusion

118
00:07:29,000 --> 00:07:33,000
or coming to some kind of a resolution.

119
00:07:33,000 --> 00:07:38,000
This high-get-hit moment become addicted to it, right?

120
00:07:38,000 --> 00:07:40,000
So we want that, and when we don't get it,

121
00:07:40,000 --> 00:07:42,000
we're not satisfied with the answer, but when we get it,

122
00:07:42,000 --> 00:07:44,000
oh, we get another rush.

123
00:07:44,000 --> 00:07:46,000
It's totally chemical.

124
00:07:46,000 --> 00:07:50,000
So that's why I think this video,

125
00:07:50,000 --> 00:07:54,000
I think there may be many things wrong with the video,

126
00:07:54,000 --> 00:07:57,000
but it seems to me because I've explained this several times

127
00:07:57,000 --> 00:07:59,000
that the problem isn't in the explanations,

128
00:07:59,000 --> 00:08:01,000
because I'm sure the explanation is sound.

129
00:08:01,000 --> 00:08:04,000
The problem is that that explanation isn't the answer.

130
00:08:04,000 --> 00:08:08,000
The answer is to focus on the doubt.

131
00:08:08,000 --> 00:08:10,000
I always say you're doubting.

132
00:08:10,000 --> 00:08:13,000
Well, forget about what you're doubting about.

133
00:08:13,000 --> 00:08:15,000
Focus on the doubt, because once you can get rid of the doubt,

134
00:08:15,000 --> 00:08:17,000
you've done all that you need to do.

135
00:08:17,000 --> 00:08:19,000
People say, is this meditation good for me?

136
00:08:19,000 --> 00:08:20,000
I'm not sure.

137
00:08:20,000 --> 00:08:21,000
I have lots of doubts.

138
00:08:21,000 --> 00:08:25,000
I say, well, this meditation is for the purpose of giving up doubt

139
00:08:25,000 --> 00:08:28,000
and overcoming doubt, so all you have to do is focus on the doubt

140
00:08:28,000 --> 00:08:33,000
and when it's gone, you've accomplished your mission.

141
00:08:33,000 --> 00:08:39,000
But the explanation for what it's worth is that

142
00:08:39,000 --> 00:08:44,000
in order to understand reality, we have to give up

143
00:08:44,000 --> 00:08:47,000
all of our preconceived notions.

144
00:08:47,000 --> 00:08:49,000
So at the beginning, I think, I see something like,

145
00:08:49,000 --> 00:08:53,000
forget about everything you've been taught.

146
00:08:53,000 --> 00:08:55,000
Forget everything.

147
00:08:55,000 --> 00:08:57,000
Forget what your parents taught you.

148
00:08:57,000 --> 00:08:58,000
Forget what society.

149
00:08:58,000 --> 00:09:00,000
Forget that you are a human being.

150
00:09:00,000 --> 00:09:05,000
Forget absolutely every concept you have

151
00:09:05,000 --> 00:09:09,000
that puts anything in any context.

152
00:09:09,000 --> 00:09:14,000
That's really what is necessary to understand reality

153
00:09:14,000 --> 00:09:18,000
because context limits understanding.

154
00:09:18,000 --> 00:09:23,000
When you can see this by how people respond to the idea

155
00:09:23,000 --> 00:09:27,000
that sex is for not sexual intercourse is for nothing.

156
00:09:27,000 --> 00:09:30,000
That is a funny discussion.

157
00:09:30,000 --> 00:09:32,000
Sex is for nothing.

158
00:09:32,000 --> 00:09:34,000
But sex is for procreation.

159
00:09:34,000 --> 00:09:37,000
How do you know if there weren't procreation?

160
00:09:37,000 --> 00:09:40,000
There would continue the human race.

161
00:09:40,000 --> 00:09:44,000
Well, what's the continuation of the human race for?

162
00:09:44,000 --> 00:09:46,000
And then what's the continuation of the human race for?

163
00:09:46,000 --> 00:09:48,000
Well, I wouldn't have been born if I hadn't been.

164
00:09:48,000 --> 00:09:52,000
Well, what's the purpose of you being born?

165
00:09:52,000 --> 00:09:57,000
The reason for this kind of questions

166
00:09:57,000 --> 00:10:01,000
is because we're thinking in a context.

167
00:10:01,000 --> 00:10:04,000
In order to understand reality, you have to come out of that.

168
00:10:04,000 --> 00:10:09,000
When you do that, you start to see exactly what I said.

169
00:10:09,000 --> 00:10:12,000
It really is just experience.

170
00:10:12,000 --> 00:10:14,000
All you can say about reality,

171
00:10:14,000 --> 00:10:16,000
without getting into some kind of context

172
00:10:16,000 --> 00:10:21,000
or some kind of concept or projection,

173
00:10:21,000 --> 00:10:24,000
is that experience occurs.

174
00:10:24,000 --> 00:10:25,000
There is seeing, there is hearing,

175
00:10:25,000 --> 00:10:26,000
there is smelling, there is tasting,

176
00:10:26,000 --> 00:10:28,000
there is feeling, and there is thinking.

177
00:10:28,000 --> 00:10:32,000
You can say that pretty reassuringly.

178
00:10:32,000 --> 00:10:35,000
The question of whether it's real or not real.

179
00:10:35,000 --> 00:10:39,000
I might just seeing things in my hallucinating is meaningless.

180
00:10:39,000 --> 00:10:44,000
The experience occurs as it occurs or doesn't occur.

181
00:10:44,000 --> 00:10:49,000
One or the other.

182
00:10:49,000 --> 00:10:55,000
Then you can go in several directions.

183
00:10:55,000 --> 00:11:03,000
The first is to, I think, what logically comes next

184
00:11:03,000 --> 00:11:09,000
is that it's less likely to think of a point where this stops,

185
00:11:09,000 --> 00:11:13,000
where this ceases, based on chance,

186
00:11:13,000 --> 00:11:16,000
or based on just time that eventually this ceases.

187
00:11:16,000 --> 00:11:20,000
Especially if you go into meditation as you go deeper,

188
00:11:20,000 --> 00:11:25,000
you see that mind states give rise to future mind states,

189
00:11:25,000 --> 00:11:27,000
which is really the core of the Buddha's teaching,

190
00:11:27,000 --> 00:11:30,000
the craving leads to further becoming.

191
00:11:30,000 --> 00:11:34,000
So it's true that it's possible for this to cease,

192
00:11:34,000 --> 00:11:38,000
which would be a kind of a death, in a sense.

193
00:11:38,000 --> 00:11:45,000
But that cessation can only come at the end of desire.

194
00:11:45,000 --> 00:11:47,000
As long as there is more desire,

195
00:11:47,000 --> 00:11:48,000
there will be more becoming,

196
00:11:48,000 --> 00:11:53,000
more arising, more of this stuff coming up, more experiences.

197
00:11:53,000 --> 00:11:58,000
And so it becomes illogical.

198
00:11:58,000 --> 00:12:01,000
It becomes not logical.

199
00:12:01,000 --> 00:12:03,000
It goes against your experience at that point

200
00:12:03,000 --> 00:12:07,000
to think that suddenly it will stop.

201
00:12:07,000 --> 00:12:09,000
Because you're watching the cause of effect,

202
00:12:09,000 --> 00:12:11,000
and you're saying, these causes,

203
00:12:11,000 --> 00:12:15,000
well, as far as I can see, they're having these effects.

204
00:12:15,000 --> 00:12:19,000
This desire leads to this result.

205
00:12:19,000 --> 00:12:22,000
You want some things, so there leads to thinking about it,

206
00:12:22,000 --> 00:12:26,000
developing something that wasn't there before, suddenly born.

207
00:12:26,000 --> 00:12:30,000
And this is how it works.

208
00:12:30,000 --> 00:12:33,000
The idea of a human dying and being born

209
00:12:33,000 --> 00:12:36,000
is actually just like waves in an ocean,

210
00:12:36,000 --> 00:12:40,000
because we've contrived this reality

211
00:12:40,000 --> 00:12:42,000
of being a human being, or this reality,

212
00:12:42,000 --> 00:12:44,000
this state of being a human being.

213
00:12:44,000 --> 00:12:46,000
And it occurs in waves.

214
00:12:46,000 --> 00:12:47,000
It's not always like that.

215
00:12:47,000 --> 00:12:52,000
There are beings that don't arise in this way,

216
00:12:52,000 --> 00:12:57,000
beings that go from one state to another,

217
00:12:57,000 --> 00:13:01,000
but not in terms of birth and death.

218
00:13:01,000 --> 00:13:04,000
That didn't even finish answering your question, though.

219
00:13:04,000 --> 00:13:09,000
That has to do with the whole idea of getting reborn,

220
00:13:09,000 --> 00:13:13,000
because we don't believe in these things.

221
00:13:13,000 --> 00:13:16,000
Sorry, the only part of that question

222
00:13:16,000 --> 00:13:21,000
that doesn't answer is in terms of karma.

223
00:13:21,000 --> 00:13:23,000
No, but it does actually, no?

224
00:13:23,000 --> 00:13:26,000
Because just to point out that what is karma?

225
00:13:26,000 --> 00:13:28,000
Karma is that relationship,

226
00:13:28,000 --> 00:13:32,000
where you see this craving leads to this suffering.

227
00:13:32,000 --> 00:13:35,000
This wisdom, for example, leads to this happiness,

228
00:13:35,000 --> 00:13:37,000
or this peace.

229
00:13:37,000 --> 00:13:39,000
Seeing how mine states,

230
00:13:39,000 --> 00:13:42,000
momentary mine states lead one to the next.

231
00:13:42,000 --> 00:13:44,000
That's how karma works.

232
00:13:44,000 --> 00:13:49,000
Karma is a relationship from one experience to the next.

233
00:13:49,000 --> 00:13:51,000
And as such, it doesn't even exist.

234
00:13:51,000 --> 00:13:54,000
It's just an observation that occurs.

235
00:13:54,000 --> 00:13:57,000
You can see that when this arises,

236
00:13:57,000 --> 00:13:59,000
that arises with the arising.

237
00:13:59,000 --> 00:14:01,000
This there is the arising of that.

238
00:14:01,000 --> 00:14:05,000
When this doesn't arise, that doesn't arise.

239
00:14:05,000 --> 00:14:09,000
And in the end,

240
00:14:09,000 --> 00:14:19,000
I would say even that realization is discarded.

241
00:14:19,000 --> 00:14:21,000
In a sense, because in Arahant,

242
00:14:21,000 --> 00:14:23,000
it doesn't create karma.

243
00:14:23,000 --> 00:14:27,000
But that's because they also don't think in terms of correlation.

244
00:14:27,000 --> 00:14:30,000
They just see things as they are a moment to moment.

245
00:14:30,000 --> 00:14:32,000
They understand intellectually,

246
00:14:32,000 --> 00:14:33,000
as the Buddha taught,

247
00:14:33,000 --> 00:14:39,000
but it's just a mupada or dependent origination.

248
00:14:39,000 --> 00:14:42,000
But as far as I can see,

249
00:14:42,000 --> 00:14:45,000
their experience is not in that way.

250
00:14:45,000 --> 00:14:48,000
The experience is one to one.

251
00:14:48,000 --> 00:14:53,000
So there's no thought about karma on an experiential level.

252
00:14:53,000 --> 00:14:59,000
There's only a moment to moment arising and ceasing.

253
00:14:59,000 --> 00:15:01,000
I hope that helps.

254
00:15:01,000 --> 00:15:04,000
I think we're looking at the karma,

255
00:15:04,000 --> 00:15:07,000
like from our materialistic point of view,

256
00:15:07,000 --> 00:15:10,000
and want to give it some form,

257
00:15:10,000 --> 00:15:14,000
and to make it something that we can understand.

258
00:15:14,000 --> 00:15:22,000
And that's just not appropriate for that kind of wanted to say thing.

259
00:15:22,000 --> 00:15:25,000
But it's exactly not a thing.

260
00:15:25,000 --> 00:15:34,000
And in a way like, there is not a fruit in the seed.

261
00:15:34,000 --> 00:15:36,000
It's just not yet there.

262
00:15:36,000 --> 00:15:39,000
It becomes maybe later a fruit.

263
00:15:39,000 --> 00:15:41,000
But when there is a seed,

264
00:15:41,000 --> 00:15:43,000
there's not a fruit yet.

265
00:15:43,000 --> 00:15:47,000
And the fruit is not stored in the seed.

266
00:15:47,000 --> 00:15:49,000
It's not stored in the tree.

267
00:15:49,000 --> 00:15:54,000
It's just grows when the conditions are there.

268
00:15:54,000 --> 00:16:01,000
So there's just nothing that carries our karma.

269
00:16:01,000 --> 00:16:05,000
This is just to be accepted.

270
00:16:05,000 --> 00:16:07,000
The whole thing about it is very clear.

271
00:16:07,000 --> 00:16:09,000
The whole thing about DNA, right?

272
00:16:09,000 --> 00:16:12,000
The information is encoded there.

273
00:16:12,000 --> 00:16:15,000
To replicate it, the fruit comes from the tree.

274
00:16:15,000 --> 00:16:18,000
And we have scientific explanations as to why that is.

275
00:16:18,000 --> 00:16:22,000
But no, you won't find the fruit there.

276
00:16:22,000 --> 00:16:26,000
And you won't find a thing that is the relationship.

277
00:16:26,000 --> 00:16:30,000
But you'll see how the DNA leads to this or whatever,

278
00:16:30,000 --> 00:16:34,000
and how the replication can occur under these conditions.

279
00:16:34,000 --> 00:16:38,000
But the seed karma is a relation.

280
00:16:38,000 --> 00:16:39,000
It's like gravity.

281
00:16:39,000 --> 00:16:43,000
Gravity, we think generally we think it's something that exists, right?

282
00:16:43,000 --> 00:16:45,000
But when you think about it, when you study physics,

283
00:16:45,000 --> 00:16:47,000
it actually doesn't exist.

284
00:16:47,000 --> 00:16:51,000
This whole idea of curved space is just mind-blowing, right?

285
00:16:51,000 --> 00:16:55,000
Because it actually turns out that it's not coming back down, right?

286
00:16:55,000 --> 00:16:58,000
What goes up doesn't come down or so on.

287
00:16:58,000 --> 00:17:03,000
It's only the fact that space is curved, for example,

288
00:17:03,000 --> 00:17:10,000
or if that's really the tree.

289
00:17:10,000 --> 00:17:14,000
So that was to be the last question.

290
00:17:14,000 --> 00:17:22,000
And I think we have to give the haul back up, because we've kind of chased everyone out of the haul.

291
00:17:22,000 --> 00:17:26,000
This should be a meditation haul, but at this moment it's still not.

292
00:17:26,000 --> 00:17:48,000
It's now a bedroom, and I'm still recording.

