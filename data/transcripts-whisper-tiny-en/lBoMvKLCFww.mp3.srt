1
00:00:00,000 --> 00:00:22,280
Okay welcome everyone to the morning session in second life, today I'd like to go back

2
00:00:22,280 --> 00:00:36,720
to something a little more basic than perhaps usual. And often I'm talking about the

3
00:00:36,720 --> 00:00:48,000
results of meditation or the place of meditation in our lives and so on. My meditation

4
00:00:48,000 --> 00:00:57,840
is so important. But I get sometimes people saying that these ideas are nice and

5
00:00:57,840 --> 00:01:06,280
interesting but very difficult to put into practice. And I think part of the

6
00:01:06,280 --> 00:01:17,680
reason for that is that if you rely on just talks and people giving a talk on

7
00:01:17,680 --> 00:01:35,720
Buddhism or meditation, it's necessarily very general, broad, vague and

8
00:01:35,720 --> 00:01:45,040
impractical as a complete method of practice. The purpose of a dhamma talk is

9
00:01:45,040 --> 00:01:58,760
to encourage and elucidate the meaning of the meditation practice which people

10
00:01:58,760 --> 00:02:04,120
are already engaged in. For those people who have already received instruction

11
00:02:04,120 --> 00:02:09,840
in meditation. So the best role of a of a talk is for those people who are

12
00:02:09,840 --> 00:02:17,640
already meditating. Normally when I give talks it would be to those who are

13
00:02:17,640 --> 00:02:24,360
engaged in a intensive meditation course. I give the talk with my eyes

14
00:02:24,360 --> 00:02:37,840
closed and ask everyone to close their eyes and practice meditation as they

15
00:02:37,840 --> 00:02:47,440
listen and the talk is a way to encourage and I just want to practice based on

16
00:02:47,440 --> 00:02:53,240
the principles. But it's anyway today I thought I'd go back and sort of talk

17
00:02:53,240 --> 00:03:00,200
about exactly what is it that I propose for Buddhists or people interested in

18
00:03:00,200 --> 00:03:16,640
Buddhist meditation to practice. One, one quality or characteristic and the

19
00:03:16,640 --> 00:03:27,560
virtue that is often talked about in Buddhism is this idea of mindfulness. I

20
00:03:27,560 --> 00:03:33,960
think we hear about mindfulness quite often in all of the major schools of

21
00:03:33,960 --> 00:03:42,400
Buddhism. It's considered to be a very integral part of Buddhist meditation

22
00:03:42,400 --> 00:03:54,840
practice and mental development. And I think this is with good cause because

23
00:03:54,840 --> 00:04:00,360
it's something that the Buddha himself taught probably more often than any

24
00:04:00,360 --> 00:04:11,280
other type of meditation practice. And spoke of it in much higher praise than

25
00:04:11,280 --> 00:04:20,480
any other practice. The Buddha said that the practice of mindfulness or this

26
00:04:20,480 --> 00:04:27,080
word that we translate into English as mindfulness. He said it is the direct

27
00:04:27,080 --> 00:04:33,080
way or the straight path that leads only in one direction and that is to

28
00:04:33,080 --> 00:04:40,360
the purification of beings for the overcoming of sorrow, lamentation, and despair

29
00:04:40,360 --> 00:04:46,480
for being free from mental and physical suffering,

30
00:04:46,480 --> 00:04:52,960
for attaining the right path and for realizing freedom from suffering. He never

31
00:04:52,960 --> 00:05:02,720
said this about any other practice. He was never this clear in his discussion

32
00:05:02,720 --> 00:05:09,160
of other meditation practices. And so the question might come up as to why there

33
00:05:09,160 --> 00:05:13,920
are many different types of meditation practice. In that case, if this practice is

34
00:05:13,920 --> 00:05:24,160
the best way, and the thing I think about the Buddha is that he was able to see

35
00:05:24,160 --> 00:05:30,920
for individual people what they required in a way that I would say most if not

36
00:05:30,920 --> 00:05:37,280
all teachers of today are not able, even the best teachers. And I've seen

37
00:05:37,280 --> 00:05:49,440
and been in contact with several fairly experienced teachers. And as great

38
00:05:49,440 --> 00:05:59,320
and wonderful as they are, they still have problems judging and assessing

39
00:05:59,320 --> 00:06:05,240
every meditator's practice. In the way that the Buddha was able to do, we

40
00:06:05,240 --> 00:06:12,200
have a story that apparently even the Buddha's chief disciples, Sariputa, would

41
00:06:12,200 --> 00:06:19,320
get it wrong at times, where he would recommend a certain meditation practice,

42
00:06:19,320 --> 00:06:25,200
and it turned out to be the wrong one for that individual person. It's very

43
00:06:25,200 --> 00:06:30,240
difficult to know where a person's coming from, what's going to come up in

44
00:06:30,240 --> 00:06:34,000
meditation, because we're not dealing just with this life. We're dealing with

45
00:06:34,000 --> 00:06:38,600
all of the complexities that make up who we are. When we come to a meditation

46
00:06:38,600 --> 00:06:46,000
course, we can have a certain mode of behavior, and that mode of behavior can

47
00:06:46,000 --> 00:06:50,920
completely change through the course of the practice due to other factors

48
00:06:50,920 --> 00:06:58,480
from another part of our history coming up, coming to the forefront as the

49
00:06:58,480 --> 00:07:05,040
old modes of behavior disappear, new modes of behavior appear. And it's

50
00:07:05,040 --> 00:07:09,880
kind of like layers of an onion in that sense. So it's quite difficult. And I

51
00:07:09,880 --> 00:07:16,640
think for this reason, the safest bet is to stick with this very general

52
00:07:16,640 --> 00:07:24,600
broad-based and seemingly universal, universally applicable meditation. In

53
00:07:24,600 --> 00:07:32,360
Buddhism, we often talk about the different character types, and there are many

54
00:07:32,360 --> 00:07:37,040
different meditation practices that are suitable only for specific character

55
00:07:37,040 --> 00:07:48,040
types. So for some people, if a person is a lustful temperament, then they

56
00:07:48,040 --> 00:07:55,680
should focus on the objective impurities of the body. As an example, as a way

57
00:07:55,680 --> 00:08:00,280
of sort of countering that, if there are person who is very much addicted to

58
00:08:00,280 --> 00:08:05,920
sensuality, then by focusing on the body, you come to see that actually

59
00:08:05,920 --> 00:08:11,960
there's nothing particularly attractive about it. If a person is an

60
00:08:11,960 --> 00:08:17,600
angrier hateful temperament, then they should practice loving kindness. But not

61
00:08:17,600 --> 00:08:22,640
vice versa, if a person is a hating temperament full of anger, they shouldn't

62
00:08:22,640 --> 00:08:27,560
focus on the impurities of the body. If a person is a lustful temperament, then

63
00:08:27,560 --> 00:08:33,880
they also won't do very well, necessarily, focusing on loving kindness. I'm

64
00:08:33,880 --> 00:08:40,480
not sure about that one, but the point is, in the case of loving kindness,

65
00:08:40,480 --> 00:08:45,440
purpose of a lustful temperament, it can easily become lust and attraction

66
00:08:45,440 --> 00:08:52,320
towards the object, the person who is the object. But there are many examples

67
00:08:52,320 --> 00:08:57,920
like that of meditation practices that aren't suitable for an individual

68
00:08:57,920 --> 00:09:05,000
person. But when it comes to practicing mindfulness, we don't have this

69
00:09:05,000 --> 00:09:09,920
characterization. And I think this is a problem that I've encountered often

70
00:09:09,920 --> 00:09:13,840
with people who have been Buddhist for a long time and say that mindfulness

71
00:09:13,840 --> 00:09:19,480
meditation is just not suitable to their character. But according to the

72
00:09:19,480 --> 00:09:29,960
Buddhist texts, the ones I follow anyway, the practice of mindfulness is outside

73
00:09:29,960 --> 00:09:37,360
of classification. Because mindfulness is dealing with ultimate realities. It's

74
00:09:37,360 --> 00:09:45,440
not a meditation that is for calming the mind. It's not a creation-based

75
00:09:45,440 --> 00:09:52,720
meditation where you focus on the concept or the beings or something that you

76
00:09:52,720 --> 00:09:57,480
imagine or think of in your mind, you're focusing on what's in front of you.

77
00:09:57,480 --> 00:10:01,720
And if you look at all of the Buddhist teaching, you can see that this is

78
00:10:01,720 --> 00:10:05,160
really what the Buddha wanted us to focus on, even when he was not saying

79
00:10:05,160 --> 00:10:14,000
meditating this way in that way. What he was talking about was always one

80
00:10:14,000 --> 00:10:18,600
former another of the objects of mindfulness. The four, what we call the four

81
00:10:18,600 --> 00:10:27,200
foundations of the four, establishments of mindfulness. So I think this is

82
00:10:27,200 --> 00:10:37,680
without a doubt the most widely used and widely applicable form of

83
00:10:37,680 --> 00:10:41,240
meditation practice. And it's the safest for a meditation teacher. That's why

84
00:10:41,240 --> 00:10:48,720
so many people use it. Because it clearly has objective benefits for pretty

85
00:10:48,720 --> 00:10:56,640
much everyone who will undertake the practice. So a word about mindfulness, that's

86
00:10:56,640 --> 00:11:01,600
the first problem. The next problem, once we've established that mindfulness is a

87
00:11:01,600 --> 00:11:06,320
useful meditation, is to understand what is mindfulness. And the problem, of

88
00:11:06,320 --> 00:11:11,720
course, is that the word mindfulness is a fairly poor translation. And most

89
00:11:11,720 --> 00:11:16,440
Buddhist teachers who are familiar with the old languages of India, Sanskrit

90
00:11:16,440 --> 00:11:22,560
poly, will tell you that it's not really a proper translation. Actually, I

91
00:11:22,560 --> 00:11:27,720
like the word and I think I think it is a good word. Mindfulness is this sense of

92
00:11:27,720 --> 00:11:34,120
being alert and knowing what you're doing and being present. And I think

93
00:11:34,120 --> 00:11:37,720
that's important in the meditation, but it doesn't quite capture what we're

94
00:11:37,720 --> 00:11:43,880
doing when we meditate. Meditation is a work. It's something that you have to

95
00:11:43,880 --> 00:11:50,080
do to do. It's a practice that you have to undertake. Like in transcendental

96
00:11:50,080 --> 00:11:56,120
meditation, you have an object and you have to develop that. Meditation is

97
00:11:56,120 --> 00:12:00,360
considered a form of development. So I think the problem with mindfulness is

98
00:12:00,360 --> 00:12:07,240
people generally assume that if you're watching reality, if you're looking at

99
00:12:07,240 --> 00:12:11,160
it, that you're going to see it clearly. That's simply by being present. When

100
00:12:11,160 --> 00:12:15,000
you walk, knowing that you walk, when you sit, knowing that you sit, in the

101
00:12:15,000 --> 00:12:22,760
sense of having a consciousness of it, that that's enough. And this is an

102
00:12:22,760 --> 00:12:27,520
argument in Buddhism. Some people do believe there are meditation centers

103
00:12:27,520 --> 00:12:32,040
that do believe that that's the case. So I'm not going to say it's not true that

104
00:12:32,040 --> 00:12:38,920
you have some level of mindfulness. But what I'd like to suggest is that by

105
00:12:38,920 --> 00:12:46,120
working, by actually trying to develop a clearer and more perfect understanding

106
00:12:46,120 --> 00:13:04,440
of the present moment, you get a far more far more deeper understanding and

107
00:13:04,440 --> 00:13:13,200
realization of the truth in a shorter time, that by simply watching, you don't

108
00:13:13,200 --> 00:13:22,600
have the necessary focus and clarity of mind. This is what I've seen through

109
00:13:22,600 --> 00:13:26,040
the practice of meditation, of the meditation that I teach, and that's why I

110
00:13:26,040 --> 00:13:31,640
teach it. It often seems a little bit counterintuitive for those people who

111
00:13:31,640 --> 00:13:39,400
kind of have this idea that meditation should be a relaxing or a

112
00:13:39,400 --> 00:13:45,920
comfortable state where you're just taking it easy, kind of settling into the

113
00:13:45,920 --> 00:13:52,400
moment as it were, as a way of calming the mind. And where as calming the

114
00:13:52,400 --> 00:13:57,080
mind is a part of meditation, what I want to express is that from what I've

115
00:13:57,080 --> 00:14:03,360
seen, in order to really gain true and lasting fruit from the practice, there's

116
00:14:03,360 --> 00:14:08,520
a certain amount of work that is required and that we shouldn't be complacent

117
00:14:08,520 --> 00:14:13,400
in thinking that we can just sit, watch, and kind of let our minds float around

118
00:14:13,400 --> 00:14:19,880
and expect again real benefits. I mean you can try both of them from what I've

119
00:14:19,880 --> 00:14:24,320
seen. There's a great benefit that comes from working and if you actually put

120
00:14:24,320 --> 00:14:29,760
out the effort, you'll find that your mind is much more stable, strong. It's kind

121
00:14:29,760 --> 00:14:36,720
of like working out the body. When you put out the right amount of effort, when

122
00:14:36,720 --> 00:14:42,320
you work, when you push yourself again and again and again in a repetitious

123
00:14:42,320 --> 00:14:50,360
manner, what you gain out of it is not any visible benefit. Like when you lift

124
00:14:50,360 --> 00:14:55,840
weights, you aren't actually lifting things and when you finish, you don't have

125
00:14:55,840 --> 00:14:59,040
a bunch of things up in a high place. You're lifting up and down, up and down,

126
00:14:59,040 --> 00:15:04,920
and there's no outcome. But the weights return to their original location, but

127
00:15:04,920 --> 00:15:09,720
what you gain is a physical strength. So the work and meditation, you do

128
00:15:09,720 --> 00:15:14,400
something repetitious again and again and again, and you know, we do this

129
00:15:14,400 --> 00:15:18,320
walking back and forth and people think, well that's boring, it's stupid, you're

130
00:15:18,320 --> 00:15:22,200
not going anywhere. It's just pacing back and forth. But in the same way as when

131
00:15:22,200 --> 00:15:26,480
you're lifting weights, at the end of the day, you gain something. You gain a

132
00:15:26,480 --> 00:15:33,520
strength of mind, a fortitude of mind. And this is something that's verifiable.

133
00:15:33,520 --> 00:15:43,280
So what I'm talking about here is a application of the mind. Mindfulness in a

134
00:15:43,280 --> 00:15:50,640
meditative sense is best understood as the work that we put in to remind ourselves

135
00:15:50,640 --> 00:15:56,760
of the reality. The word septi, which we translate as mindfulness, actually has

136
00:15:56,760 --> 00:16:03,840
something to do with the word, remember, or to remember. When you remember your past

137
00:16:03,840 --> 00:16:08,280
lives, they call that septi, what we translate as mindfulness. When you think of

138
00:16:08,280 --> 00:16:20,880
the Buddha, they call that septi. And when you, when you think of the, when you

139
00:16:20,880 --> 00:16:26,000
think of any object, when you remember something, or recollect, or send your

140
00:16:26,000 --> 00:16:30,400
mind out to the object, thinking of it, they call that septi. This isn't

141
00:16:30,400 --> 00:16:35,920
exactly what I'm talking about here in terms of the objects of mindfulness or

142
00:16:35,920 --> 00:16:40,640
the foundations of mindfulness. But it gets you an idea of where the word comes

143
00:16:40,640 --> 00:16:47,320
from. It has to do with remembering or recognizing or establishing in the

144
00:16:47,320 --> 00:16:57,640
mind, the object. So this is incredibly important in our relationship with

145
00:16:57,640 --> 00:17:02,560
ultimate reality, with the reality in front of us, because we don't do this. When we

146
00:17:02,560 --> 00:17:06,840
see something, we like it, or we dislike it. When we hear something, we like it,

147
00:17:06,840 --> 00:17:10,140
or we just, we're always judging. Even when I'm talking, now there's

148
00:17:10,140 --> 00:17:13,960
judgments going through everyone's mind, unless you're enlightened. You're

149
00:17:13,960 --> 00:17:18,880
thinking, boy, good talk, boy, bad talk, boring, and so on. And it'll change for

150
00:17:18,880 --> 00:17:22,600
moment to moment. So you might enjoy the talk. I assume that you're here because

151
00:17:22,600 --> 00:17:29,440
you think I have something useful to say. But that be that as it may, there's

152
00:17:29,440 --> 00:17:33,720
going to be moments where suddenly your mind gets bored. Your mind starts to

153
00:17:33,720 --> 00:17:38,080
lose interest and starts to think about checking something out, else I would

154
00:17:38,080 --> 00:17:45,880
spend back to Facebook or YouTube or whatever. It's it's not something that you

155
00:17:45,880 --> 00:17:50,720
can blame on yourself. It's the way of the mind. The mind judges liking,

156
00:17:50,720 --> 00:18:00,320
disliking, and reacting continuously. And I've been studying a bit about quantum

157
00:18:00,320 --> 00:18:07,120
physics. If you've been following me, you've been probably hearing about how

158
00:18:07,120 --> 00:18:10,840
I've become interested in this. And the reason I'm so interested in it is

159
00:18:10,840 --> 00:18:18,240
because that's exactly what quantum physics allows for or describes. And I've

160
00:18:18,240 --> 00:18:22,140
been told by several people that that the mind has nothing to do with

161
00:18:22,140 --> 00:18:27,680
conscious with quantum physics. But there's this really interesting author

162
00:18:27,680 --> 00:18:32,160
that I've been reading that one of the experts in the field of quantum physics.

163
00:18:32,160 --> 00:18:39,840
And he explains how quantum physics really leaves open or leaves a perfect

164
00:18:39,840 --> 00:18:44,840
space for the mind, for consciousness, for what we already experience. And that

165
00:18:44,840 --> 00:18:56,040
is to interpret and to make decisions that with everything that is experienced,

166
00:18:56,040 --> 00:19:03,960
there's the moment where the mind intervenes and collapses the quantum state

167
00:19:03,960 --> 00:19:11,440
from from a smeared out series of possibilities to a to a specific state, a

168
00:19:11,440 --> 00:19:16,800
decision that's going to be like this. And this is this is very close to the

169
00:19:16,800 --> 00:19:21,680
description of karma in Buddhism. And it's very close to what is

170
00:19:21,680 --> 00:19:29,520
experienced in meditation. I was quite impressed to see that, you know, what

171
00:19:29,520 --> 00:19:34,560
we've been doing in meditation, realizing in meditation is being explained

172
00:19:34,560 --> 00:19:39,040
by by by quantum quantum in terms of quantum physics, people who are testing this

173
00:19:39,040 --> 00:19:41,720
and saying, oh, this is how it looks like it's happening. And we're like, yeah,

174
00:19:41,720 --> 00:19:50,760
that is how it happens, which is quite exciting. So all we're trying to do here,

175
00:19:50,760 --> 00:19:54,520
this intervention goes on, the mental intervention goes on in every moment,

176
00:19:54,520 --> 00:19:57,760
every time we experience something, it's a mental intervention into the

177
00:19:58,080 --> 00:20:05,720
otherwise closed physical reality, this physical realm that goes in terms

178
00:20:05,720 --> 00:20:13,000
of cause and effect and and is otherwise untouched. X causes, why causes

179
00:20:13,000 --> 00:20:19,280
Z and so on. When the mind gets in there, it's able to change things or it

180
00:20:19,280 --> 00:20:26,560
had it seems to to play a part in this. And this is quite obvious in meditation

181
00:20:26,560 --> 00:20:31,600
that we are able to decide to do this and decide to do that, judge things in

182
00:20:31,600 --> 00:20:39,160
this way or that way. So what we're doing in the practice of mindfulness is

183
00:20:39,160 --> 00:20:44,000
trying to purify that intervention or purify that mental state. When we see

184
00:20:44,000 --> 00:20:49,840
something, we see it purely, we have a pure response, a response that is creating

185
00:20:49,840 --> 00:20:58,920
peace, happiness, harmony, big bears with boxes on their feet, and freedom

186
00:20:58,920 --> 00:21:16,280
from suffering. And we do this at every moment in the meditation. Our meditation

187
00:21:16,280 --> 00:21:21,000
has to be a moment by moment practice. This is why you always share in

188
00:21:21,000 --> 00:21:24,360
Buddhism, you know, stay in the present moment. Don't think about the future,

189
00:21:24,360 --> 00:21:29,200
don't think about the past. It's not just a nice thing to think or to

190
00:21:29,200 --> 00:21:34,040
appreciate. It's actually a meditation instruction that right now is when

191
00:21:34,040 --> 00:21:38,360
you're meditating. You can't say, I've been sitting for an hour, I'm going to

192
00:21:38,360 --> 00:21:44,120
sit for an hour, practice in the news AI, sat for an hour. That's really the

193
00:21:44,120 --> 00:21:48,360
wrong way to look at meditation. Right now, if you're meditating right now,

194
00:21:48,360 --> 00:21:52,160
then right now is meditative and that has no bearing on the next moment

195
00:21:52,160 --> 00:21:58,320
because the decisions that we make, the judgments that we make occur at every

196
00:21:58,320 --> 00:22:03,360
moment of seeing, hearing, smelling, chasing, feeling. And this is

197
00:22:03,360 --> 00:22:08,160
incessant. So our meditation has to be incessant, it has to be moment to

198
00:22:08,160 --> 00:22:13,240
moment. And this is sort of the basis of the practice that I teach. If you've

199
00:22:13,240 --> 00:22:20,480
been already following, indeed, if you've already been following the practice

200
00:22:20,480 --> 00:22:29,600
that I've been teaching, you can see that this is, this is what I'm talking

201
00:22:29,600 --> 00:22:33,560
about. You know, when, when the belly, when the stomach rises and you focus on

202
00:22:33,560 --> 00:22:37,080
the rising and then you forget about it and the stomach follows you focus on

203
00:22:37,080 --> 00:22:41,600
the falling. When you feel pain in the body, you focus on the pain and you

204
00:22:41,600 --> 00:22:48,000
simply see it for what it is. You, you strengthen and fortify the mind at that

205
00:22:48,000 --> 00:22:53,120
moment so that when it makes a judgment, it makes one that's clear and pure

206
00:22:53,120 --> 00:22:57,640
and impartial. Seeing is seeing, hearing is hearing, smelling is swelling,

207
00:22:57,640 --> 00:23:07,520
tasting is tasting, feeling is feeling, thinking, thinking. So this is the

208
00:23:07,520 --> 00:23:11,640
sort of the understanding that I have of what is meant by mind from this and

209
00:23:11,640 --> 00:23:17,240
this is how I practice and how I teach. The method, if you're not familiar

210
00:23:17,240 --> 00:23:24,280
with it, is to use a label or a word or the way I like to talk about it now is

211
00:23:24,280 --> 00:23:29,000
a mantra because everybody's familiar, most people are familiar with mantras.

212
00:23:29,000 --> 00:23:36,280
It's something that is said to focus your mind. So it's kind of coincidental

213
00:23:36,280 --> 00:23:42,680
that these two practices use the same technique because a mantra meditation

214
00:23:42,680 --> 00:23:46,400
generally is nothing to do with the reality in front of us. You don't hear

215
00:23:46,400 --> 00:23:52,720
people saying mantras like pain, pain, pain, or so on. You hear mantras like God

216
00:23:52,720 --> 00:23:59,440
or Jesus or Buddha or or or Om or so on. You have mantras which are have some

217
00:23:59,440 --> 00:24:04,160
spiritual meaning for people and they're somehow special. So the idea of

218
00:24:04,160 --> 00:24:11,200
watching your stomach and saying rising, falling, rising, falling. It seems

219
00:24:11,200 --> 00:24:19,480
rather counterintuitive. It's something mundane. But the incredible thing is

220
00:24:19,480 --> 00:24:28,360
that it in fact is the more profound of the two because who we are is the

221
00:24:28,360 --> 00:24:34,560
most profound subject of all. And by focusing on ourselves and that is our very

222
00:24:34,560 --> 00:24:43,080
mundane selves, the reality of who we are. This is the body and the mind.

223
00:24:43,080 --> 00:24:47,360
We come to understand the whole of the universe because we're dealing in terms

224
00:24:47,360 --> 00:24:53,040
not of something that is conceptual like our concept of God or our

225
00:24:53,040 --> 00:25:00,720
concept of of the soul, our concept of heaven or enlightenment or whatever.

226
00:25:00,720 --> 00:25:07,400
We're focusing on something that is perfectly real that is is real in some

227
00:25:07,400 --> 00:25:13,520
ultimate sense, meaning verifiably. Everyone can verify that our stomach

228
00:25:13,520 --> 00:25:20,240
does rise and does fall. Everyone can verify that we do have pain in the body.

229
00:25:21,080 --> 00:25:25,920
Everyone has to verify that there is thought to

230
00:25:25,920 --> 00:25:31,600
can verify that there are emotions. And the problem is that most of the time

231
00:25:31,600 --> 00:25:38,720
we we miss this and we think of these things as mundane, as boring, as

232
00:25:38,720 --> 00:25:45,720
uninteresting. We often have this sense of self. I don't want to say hatred,

233
00:25:45,720 --> 00:25:51,600
but it's it's a low self esteem or or we we dismiss the very nature of who we

234
00:25:51,600 --> 00:25:57,320
are as being mundane, boring, uninteresting, and so on. One of the reasons why we

235
00:25:57,320 --> 00:26:02,120
come on second life and deck ourselves out to be something we're not because

236
00:26:02,120 --> 00:26:11,720
who we are is is generally fairly disappointing. So I would submit that the

237
00:26:11,720 --> 00:26:17,920
teachings of the Buddha are not to find something new and exciting, but to

238
00:26:17,920 --> 00:26:25,360
make that which is uninteresting, boring, dull, and and seemingly useless to

239
00:26:25,360 --> 00:26:32,400
find some meaning and use and benefit in it to to come back to who we really

240
00:26:32,400 --> 00:26:41,080
are, what we really are and to transform that into a pure and meaningful

241
00:26:41,080 --> 00:26:46,840
reality. And we do this by watching it. And as I said by purifying the whole

242
00:26:46,840 --> 00:26:51,320
system of who we are because that's really what we are we're a system. We're a

243
00:26:51,320 --> 00:26:55,920
physical we have a physical component and that system is going in terms of

244
00:26:55,920 --> 00:26:59,800
cause and effect, but then we also have a mental component that is able to

245
00:26:59,800 --> 00:27:04,720
adjust and to alter this. The practice of the Buddha is to adjust and alter it

246
00:27:04,720 --> 00:27:12,000
in such a way so that it becomes harmonious and pure that it becomes free

247
00:27:12,000 --> 00:27:24,280
from defilement, free from evil, free from suffering. So that we're able to work as a

248
00:27:24,280 --> 00:27:29,960
system in a way that is perfectly pure and beneficial most to ourselves and to

249
00:27:29,960 --> 00:27:37,200
other people. I would say the practice of meditation allows this when you remind

250
00:27:37,200 --> 00:27:41,480
yourself of something and whether it be something in the body, say focusing on

251
00:27:41,480 --> 00:27:45,600
the movements of some part of the body. In this case, I'm always recommend

252
00:27:45,600 --> 00:27:50,800
the stomach. This is the one movement that we have when we're sitting still. If

253
00:27:50,800 --> 00:27:54,320
you put your hand on your stomach, you'll feel the rising and the following

254
00:27:54,320 --> 00:27:58,880
motion. So when it rises, you simply see it for what it is.

255
00:27:58,880 --> 00:28:06,320
Rising, just remind yourself, stopping your this or changing this mind that

256
00:28:06,320 --> 00:28:12,920
wants to judge, wants to like and dislike and get bored and upset and disappointed

257
00:28:12,920 --> 00:28:21,840
and so on. When it falls, you don't say it out loud. You create this idea in

258
00:28:21,840 --> 00:28:28,960
your mind that that's what that is, which is true. That's a concise

259
00:28:28,960 --> 00:28:33,240
explanation of what's going on. Is it the rising? It doesn't really matter what

260
00:28:33,240 --> 00:28:38,520
the word is, as long as it is as concise as possible, understanding of what's

261
00:28:38,520 --> 00:28:44,240
happening. When you feel pain in the body, focusing on the pain, just remind

262
00:28:44,240 --> 00:28:48,400
yourself, it's pain. Normally, pain is something we don't want to focus on. We're

263
00:28:48,400 --> 00:28:55,440
very quick to run away from to try to escape. So when we say to ourselves,

264
00:28:55,440 --> 00:29:05,760
pain, pain, pain, pain, we change that. We change our reaction. How do you say

265
00:29:05,760 --> 00:29:15,240
a reactionary behavior to one that is accepting, understanding, and able to live

266
00:29:15,240 --> 00:29:19,520
with the reality that's in front of us. When we're thinking something, just

267
00:29:19,520 --> 00:29:23,800
knowing that we're thinking, reminding ourselves, this is a thought, you think

268
00:29:23,800 --> 00:29:28,960
about, this might seem fairly banal and uninteresting, but think about how often

269
00:29:28,960 --> 00:29:33,040
our thoughts destroy us. Thoughts about what we've done in the past, stupid things

270
00:29:33,040 --> 00:29:37,480
we did, or bad things other people didn't do us, bad things that happened. We

271
00:29:37,480 --> 00:29:41,160
worry about the future, what's going to happen, and so on. We wind up

272
00:29:41,160 --> 00:29:47,760
destroying ourselves. We wind up creating great amounts of suffering for ourselves.

273
00:29:47,760 --> 00:29:55,480
So when we know that it's just a thought, you can verify for yourself. You say

274
00:29:55,480 --> 00:30:01,400
to yourself, thinking, thinking, you'll see it disappears. It's so, I mean, it's one of

275
00:30:01,400 --> 00:30:06,440
the reasons why I've become more engaged, quote unquote. I never thought of myself as

276
00:30:06,440 --> 00:30:11,360
an engaged Buddhist, but I guess that's in a sense what it is to teach other

277
00:30:11,360 --> 00:30:21,000
people, but it's just so, it creates such an imperative in the mind when you see

278
00:30:21,000 --> 00:30:26,720
people suffering from things that could be solved in about five minutes of

279
00:30:26,720 --> 00:30:31,240
explanation as to how to meditate. There may be more, but you know, there are

280
00:30:31,240 --> 00:30:35,480
times where I've sent an email to someone explaining, you know, they come to me

281
00:30:35,480 --> 00:30:42,680
with what seems to be a life-threatening problem of anxiety or depression or

282
00:30:42,680 --> 00:30:48,440
so on. And just one email from me, you know, not for me, but from someone who

283
00:30:48,440 --> 00:30:53,440
who understands or is able to explain these things, and it's not even being

284
00:30:53,440 --> 00:30:57,000
special. It's just having a basic understanding and being able to share that

285
00:30:57,000 --> 00:31:02,760
understanding with others. It can really just end the problem then and there,

286
00:31:02,760 --> 00:31:06,360
and they've been taking drugs and medication and so on, and the doctors say

287
00:31:06,360 --> 00:31:12,280
there's something wrong with them. And suddenly, poof, they're not healed, but

288
00:31:12,280 --> 00:31:19,880
they have a new way of looking at it and a way out. And if, if they stick with it,

289
00:31:19,880 --> 00:31:25,480
and if they have, you know, reminders from a teacher, if they keep in contact,

290
00:31:25,480 --> 00:31:31,000
and there's no question that they'll be able to overcome even the most,

291
00:31:31,000 --> 00:31:34,920
even some of the most severe mental sicknesses. I would say there are

292
00:31:34,920 --> 00:31:40,480
probably cases that are difficult, and I'm not even sure if they can be

293
00:31:40,480 --> 00:31:45,120
solved through meditation of true chemical imbalances in the brain by

294
00:31:45,120 --> 00:31:51,800
polar schizophrenia and so on. Those I'm not sure, and I have no, no length

295
00:31:51,800 --> 00:31:59,200
the experience as to how meditation deals with those. But, you're willing to

296
00:31:59,200 --> 00:32:09,960
try and I'm sure there's a chance, at least, if you're interested in getting

297
00:32:09,960 --> 00:32:19,480
touch, that's very good to hear. There you go. So even some of the quote-unquote

298
00:32:19,480 --> 00:32:28,840
most, anything, most extreme or most, you know, the ones that that are said to

299
00:32:28,840 --> 00:32:37,840
have the most, in terms of a physical cause, can actually be altered and

300
00:32:37,840 --> 00:32:44,960
affected and benefited through the practice of meditation. And the final one is

301
00:32:44,960 --> 00:32:49,440
our emotions. So there are four Satipatana, which is the, we translate it

302
00:32:49,440 --> 00:32:52,360
generally as the four foundations of mindfulness, which isn't a very good

303
00:32:52,360 --> 00:32:56,400
translation, but that's what they're known as. It's the body, which is, say,

304
00:32:56,400 --> 00:33:00,480
the rising and the falling or the movements of the body. The feelings, which is

305
00:33:00,480 --> 00:33:05,320
pain or happiness or calm, the thoughts, which is just thinking about anything.

306
00:33:05,320 --> 00:33:12,600
And the, the dumb ones, or the teachings of the Buddha, which, you know, it's kind

307
00:33:12,600 --> 00:33:17,440
of just attack and add on category for, for about everything else that you're

308
00:33:17,440 --> 00:33:22,640
going to have to focus on. And it starts with the emotions, those things that

309
00:33:22,640 --> 00:33:28,880
get away, get in the way of our ability to see things impartially, liking

310
00:33:28,880 --> 00:33:37,000
things, disliking things, distractions, worry, fear, depression, boredom,

311
00:33:37,000 --> 00:33:47,160
laziness, doubt. And so focusing on these is a great importance, especially for

312
00:33:47,160 --> 00:33:52,080
someone who's just beginning, because these tend to come to the four for most

313
00:33:52,080 --> 00:33:56,000
people, if you're not familiar with meditation. We're, we're sitting in

314
00:33:56,000 --> 00:34:01,240
meditation and thinking about what we want to do, what we'd rather be doing. We

315
00:34:01,240 --> 00:34:07,640
don't like what's going on. We're judging our practice, our minds, our minds are

316
00:34:07,640 --> 00:34:11,120
not still enough, and we don't like it. Sitting here is painful and uncomfortable,

317
00:34:11,120 --> 00:34:14,720
we don't like it. These emotions that come up, where we start thinking about

318
00:34:14,720 --> 00:34:20,400
something from the past, that makes us absolutely feel happy, or we want

319
00:34:20,400 --> 00:34:30,240
something for the future or so on. So again, we have, we have this, this habitual

320
00:34:30,240 --> 00:34:35,840
state, which in many cases is chemically wired into the brain. We've got these,

321
00:34:35,840 --> 00:34:44,400
these cycles of addiction and these brain patterns that are happening anyway. All

322
00:34:44,400 --> 00:34:49,080
we're doing here is, we're using the other side of a reality, which is the

323
00:34:49,080 --> 00:34:56,360
mind to, to adjust and to slowly alter. In a many cases, not that, that slow at

324
00:34:56,360 --> 00:35:00,760
all. It's something that has immediate benefits and immediate effects. When

325
00:35:00,760 --> 00:35:06,720
you simply remind yourself that it is what it is, and stop judging, you stop the

326
00:35:06,720 --> 00:35:12,360
cycle. You, you, you change the old in way of intervening, the old way of

327
00:35:12,360 --> 00:35:16,920
approaching reality in terms of liking and disliking, which is a very

328
00:35:16,920 --> 00:35:23,840
mental thing, into a new way of, of affecting reality. And that is based on

329
00:35:23,840 --> 00:35:29,120
impartiality, based on acceptance. Just by accepting things, they are what they

330
00:35:29,120 --> 00:35:33,120
are. When you're angry, say to yourself, I agree, I agree. When you want something

331
00:35:33,120 --> 00:35:38,920
wanting, wanting, when you feel depressed, depressed, stressed, stressed, worried,

332
00:35:38,920 --> 00:35:46,800
worried, confused, confused, unsure, bored, whatever it is. This, this

333
00:35:46,800 --> 00:35:52,720
moment, a moment, altering of the mind state, actually has a moment to moment

334
00:35:52,720 --> 00:36:01,240
effect on the, the cycles that occur in the body. So this is a sort of an

335
00:36:01,240 --> 00:36:06,760
overview of the practice of meditation, as I understand it, and as I teach it.

336
00:36:06,760 --> 00:36:10,480
And I thought that would be something, especially for people who are new

337
00:36:10,480 --> 00:36:16,680
that's of, of much more benefit than giving a talk for, that would be designed for

338
00:36:16,680 --> 00:36:22,680
people who are engaged in intensive meditation practice. So both ways are

339
00:36:22,680 --> 00:36:30,000
good, but I thought this would be a good one to, to give a talk on and also

340
00:36:30,000 --> 00:36:34,360
record, which I'm doing and place on the internet for people who are new to

341
00:36:34,360 --> 00:36:39,560
meditation. So that's all I have to say for today, and I'd like to thank you all

342
00:36:39,560 --> 00:36:45,080
for coming. I hope that this has been of some benefit for all of you to progress on

343
00:36:45,080 --> 00:36:51,600
in your practice of meditation and for the realization of the truth of the

344
00:36:51,600 --> 00:36:56,760
Buddhist teaching through the benefits of the practice, I wish all of you and

345
00:36:56,760 --> 00:37:01,560
for all of the people who are watching and listening to this teaching, may you

346
00:37:01,560 --> 00:37:05,960
all find peace, happiness and freedom from suffering for yourselves and be

347
00:37:05,960 --> 00:37:10,560
able to bring this teaching to help other people find peace, happiness and freedom

348
00:37:10,560 --> 00:37:13,480
from suffering for themselves. Thank you all for coming. If you have any

349
00:37:13,480 --> 00:37:43,320
questions, I'm happy to answer and otherwise have a good day.

