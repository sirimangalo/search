1
00:00:00,000 --> 00:00:07,000
Are you saying that you analyze the act of sexual activities including masturbation in order to stop the act entirely?

2
00:00:07,000 --> 00:00:10,000
Yes, yes exactly, that's what I'm saying.

3
00:00:10,000 --> 00:00:15,000
But the point is that you need not engage in masturbation.

4
00:00:15,000 --> 00:00:17,000
That's not part of it.

5
00:00:17,000 --> 00:00:19,000
And it's a delicate line.

6
00:00:19,000 --> 00:00:25,000
Why I say that and why I emphasize that is because masturbation will actually decrease your awareness.

7
00:00:25,000 --> 00:00:29,000
It's not something you should feel guilty about.

8
00:00:29,000 --> 00:00:34,000
Guilt is a negative mindset and it's not going to help.

9
00:00:34,000 --> 00:00:40,000
But you should never get the idea that if I don't masturbate I'm going to start repressing it.

10
00:00:40,000 --> 00:00:45,000
This is I think a pervasive misunderstanding.

11
00:00:45,000 --> 00:00:54,000
When the desire comes up and you start engaging in bodily activities that are the precursor to masturbation,

12
00:00:54,000 --> 00:00:56,000
start to watch those.

13
00:00:56,000 --> 00:01:01,000
If it happens that you start to engage in masturbation then watch yourself.

14
00:01:01,000 --> 00:01:07,000
And you'll see and it may be that in the beginning you can't stop yourself.

15
00:01:07,000 --> 00:01:14,000
But you will begin to see, no, you will not begin to see it right away you will see the misunderstanding.

16
00:01:14,000 --> 00:01:20,000
That there's actually no pleasure or no happiness to be found here.

17
00:01:20,000 --> 00:01:26,000
Once you look and you accept this pleasure because the funny thing about engaging in sensuality is

18
00:01:26,000 --> 00:01:31,000
we're not engaging in the happiness in the happiness at all.

19
00:01:31,000 --> 00:01:34,000
We're engaging in the desire for the happiness.

20
00:01:34,000 --> 00:01:37,000
We're focusing much more on the desire than on the happiness.

21
00:01:37,000 --> 00:01:41,000
And when you focus on the happiness suddenly you reassure yourself that,

22
00:01:41,000 --> 00:01:45,000
I don't need to do this because the happiness is right here.

23
00:01:45,000 --> 00:01:49,000
And you look at it and then you say it's nothing.

24
00:01:49,000 --> 00:01:53,000
You don't reject it and say this is horrible, this is actually suffering.

25
00:01:53,000 --> 00:01:55,000
You'll see it's useless.

26
00:01:55,000 --> 00:01:59,000
You don't even really see this useless, but you experience it.

27
00:01:59,000 --> 00:02:01,000
There's happiness and that's it.

28
00:02:01,000 --> 00:02:02,000
There's no reaction.

29
00:02:02,000 --> 00:02:06,000
There's no thought this is good, this is bad, this is me, this is mine, this is this, this is that.

30
00:02:06,000 --> 00:02:09,000
It's happiness and it's nothing.

31
00:02:09,000 --> 00:02:12,000
It is what it is, it's there and then it goes.

32
00:02:12,000 --> 00:02:18,000
You don't become angry or frustrated or disgusted by it.

33
00:02:18,000 --> 00:02:20,000
You just become disenchanted with it.

34
00:02:20,000 --> 00:02:23,000
You lose this enchantment, the desire disappears.

35
00:02:23,000 --> 00:02:29,000
The happiness doesn't disappear or the happiness is not the issue.

36
00:02:29,000 --> 00:02:34,000
But the important thing is that the desire disappears because the happiness will disappear.

37
00:02:34,000 --> 00:02:35,000
It will be gone.

38
00:02:35,000 --> 00:02:42,000
But as you watch the happiness, sorry, I'm focusing on the, I'm referring to the pleasure here.

39
00:02:42,000 --> 00:02:51,000
Focusing on the pleasure will, first of all, allow you to stop the whole seeking process.

40
00:02:51,000 --> 00:02:58,000
And in second of all, I'd like you to give up the desire for the happiness entirely.

41
00:02:58,000 --> 00:03:04,000
It will allow you to become free from, you know, free from the desire.

42
00:03:04,000 --> 00:03:12,000
It will allow you to become disenchanted, it will allow you to come to terms with the pleasure

43
00:03:12,000 --> 00:03:35,000
and let you see that it's actually just an experience.

