Okay, good evening, everyone. Welcome to our live broadcast, broadcasting live in second
life, live on our website via audio and recording for upload to YouTube. And of course
live here in Hamilton to the live studio audience. We're overfull. We now have seven
people staying in this house. No, do we? We have six people right now and tonight we're
going to have a seventh, supposed to be coming. We're overfull. We just can't turn people
away. Just can't keep people. They're breaking down the doors to get in. That's how passionate
people become. Religion, religion is such a powerful thing. Know the word religion has
such a bad reputation or such a specific reputation that makes a lot of people uneasy
when they hear the word. So I think it's important to reform the word. Religion means taking
things seriously. So tonight's talk is about someone who, well not actually so much about
him hopefully, but it involves the discussion with someone who took Buddhism quite seriously,
took the Buddha's teachings quite seriously. And he stands out as someone who is remarkable
in terms of taking the Buddha's teaching seriously. He was designated by the Buddha as foremost
of the Buddha's disciples who went forth, left the home life out of faith, out of confidence.
So I had a religious sense of religiosity. His name was Raktapala. You know the story of
Raktapala. It comes in the Gmanikaya, so it's 82. It's called Raktapala. So the story
goes, Raktapala, heard the Buddha's teaching and realized that it was quite difficult
for him to practice it while living at home. He thought to himself, wow, the sort of
teaching isn't the kind of thing you can do or surrounded by sensuality and caught up
by daily affairs of the quote unquote real world. It's not the real world, but what people
would call the real world. The ordinary mundane contrived artificial world of society that
we've put together to help us achieve our sensual, our goals of sensual pleasure. So useful
for becoming enlightened, too busy, to caught up in the fountain. So you decided you wanted
to go forth. He wanted to become a monk, but he asked the Buddha to ordain him and the
Buddha asked, do you have your parents permission? And he said, no, I don't have my parents
permission. But I said, well, I don't ordain people who don't have their parents permission.
And so I had to go back to get his parents permission to make a long story short. His
parents didn't give permission. And he laid down on the floor. He pleaded in bed with them,
but eventually he laid down on the floor and said, I'm not going to eat or drink or do
anything. I'm not going to get up off this floor until you allow me to ordain. And
so they waited him out for a while, but then he made good on this threat and he just
lay there and started starving to death. And they called his friends to try and convince
him. And his friends came over, talked to him and then went and talked to his parents
and said, look, he's pretty serious about this. How about this? If you let him ordain,
at least you'll get to see him alive. You can go and visit him. But if you don't let him
ordain, he's going to die. Then you won't see him. Then he'll be gone. So let him
eat. And the story goes on and on. But tonight I didn't want to talk so much about his
story as interesting as inspiring as it is. Later on in his life, he was living, I can't
remember where he was living, but he went somewhere and met with a king. And King asks
him, I can see if I can find a corabi. Yeah, okay. I was going to say corabi. Yeah. What
was the name of this king on the name of the place where the king lived? I think he was
actually the name of the place. And the king came to see him and he said, they're at
Apollo. He said, you know, I know people who leave the home life and go to the forest to
do their religious thing. And they do it for one of four reasons. They do it because they've
got an old. They do it because they've gotten sick. They do it because they've lost wealth or
they do it because they've lost their relatives. So someone who was old can no longer work
and is feeble and can't find any sort of solace or status or work or activity in the world
of young people and so they go off into the forest and become a religious person. This
actually happens even in Buddhism. Old people become monks and it's somewhat troublesome
because they have to get sick. And that's the second one is people get sick and then they
want to go off to the forest while there's not much they can do in India at the time. Nowadays
we just put them in old age homes and forget about them. That same sort of thing. They didn't
have old age soldiers sent them off to the forest to live or to die. Or someone who loses
wealth and become impoverished and have to become a beggar and have to live under a tree
or in a ditch or something. Also come and then also happens now. Or throughout loss of relatives
relatives who maybe took care of the person or maybe just out of grief they would realize
the suffering of life and leave the world and he said, but you are young, you're healthy.
You come from a good family and they're all still alive. What was it that caused you
to leave home? Why did you leave behind the world? It's a good question with all the wonderful
things that we have in the world. Why do we leave it? Why do we come here? Why do you come
here to torture yourself? Even just to meditate people who go to the monasteries, temples,
churches, when something bad happens? So it's remarkable to see someone who has their
whole life ahead of them. Couldn't be capable of so much in the world and says, why
we're at the palace, parents wouldn't allow him. They were quite upset that he wanted
to throw away his wonderful future. All the wonderful and good things he can do, right?
I wanted to do this useless thing on becoming a reckless, becoming a Buddhist monk. How
useless they thought. And right to the palace, the right to the palace answers is well
known in the Buddhist dispensation. He claims that it comes from the Buddha. I'm not sure
if we actually have somewhere where the Buddha said this, we very well may have, but
right to the palace the one is known for giving it. He says it comes from the Buddha. He said
there are these four dhammodesa. And today it comes from this, this means to to indicate
dikka and desa, it's the same. So the things that Buddha pointed out, four dhammodesa, four
indicators, dhamma indicators. And when these were pointed out to me, I realized that I
had to leave. I had to do something. I think these are quite useful, not just for encouraging
people to take up meditation or take up Buddhism, but also to encourage us in our practice
and to remind us why we're here when we get discouraged. And to sort of focus our attention
on what's really important, clarify and keep us on track. So the first is Upaneeti
Loco Adhu. The world is uncertain, the world is unstable. So all these good things, I mean
just the fact that those losses exist, you don't need to experience the loss to know that
that's a part of life and to know that it's a danger and to start to question, what am
I doing here? Wasting my time when I certainly won't be prepared for for old age, for sickness,
for loss, for death, and the uncertainty of life, all the things that we hold dear, even
our own selves, even our own families, our own situation, everything about us. It's unpredictable.
It's chaotic. It's the first reason why one would go off and become a monk or become
or go off in practice meditation. The second one is Atana Ologo, Anabhi Saru.
This world has no refuge, no master or guardian protector. And the King questions him and
all these. He says, what do you mean? It seems very constant. Here I'm the King and life
is very constant for me. They say, well, do you remember when you were young and you could
shoot an arrow or a rhino horse? Can you do that now? In permanent, you see? Old age comes
to us all seeing this and knowing that this is the case.
And then he says, well, what about this? No protector. What do you mean? I've got elephants,
I've got warriors, I've got lots and lots of refuge in protectors. He said, well, what
if you get sick, suppose you get really sick, your elephant's going to protect you from
the sickness? Is your family, your warriors are going to stand around you and keep the sickness
away? No, there's nobody who can protect you. When we're young, this is one of the big
shadowing of youth, the realization that our parents can't protect us from suffering is
a shock of growing up. Realization that suffering is, that we are vulnerable to suffering
and that we're protected from it. We're not sheltered from it. There's nothing that
I can shelter for, that's from it. Number three, a sakoloko, sambangpahaya, gaman yandhi. This world
is not an owner. There's no ownership. There's nothing of its own. It goes, having abandoned
everything, the kings and what do you mean? I can go with everything, full jewels and
we have so much that is ours, right? We've got our cars and our houses and our families
and our friends and our iPhones and our computers and everything. That's so much, it's
ours. We go with it. No, you don't go with any of it. He says, you know whether you're
going to be king in the next life, whether you're going to be human in the next life
to be even enjoy the riches, maybe you'll be born a hungry ghost guarding your own treasure
or maybe you'll be born an ant or a termite living in the wood wall of the palace. Not
just in the next life. All of our possessions, right? We can't hold on to them. We can't
hold on to them. Let this be mine forever. Let this car, this house, or this iPhone. Let
it be mine. Even our friends and family and even our own bodies don't really belong to
us. We're borrowing them. We're borrowing them because we don't know when they're going
to be taken back. We're borrowing them. It's like we've stolen them and we're just waiting
for the police to come and arrest us. Wait until we get caught. It's like we're living
on stolen time. So we've got to get as much pleasure and use out of it. Joy ride in this
body that we have and then death comes and catches us and wham. Takes it away. We don't know
when that's going to be. We're just trying to run away for on the lamb. We're on the
run. Death is on our heels. It's a good analogy. Number four, uno, loco, atito, tanhara dasu. Una.
Una is the same. That's where the word want, want and own from the same place. Una means
wanting. This world is wanting is lacking. Atita. Unsatisfied. Tanhara dasu. The slave dasu
has a slave of them of craving. Una means, what do you mean? I can satisfy myself any time.
I get whatever I want. I'm always satisfied. I eat and that I'm satisfied. Una says,
well, what if you heard about another kingdom that was weak but had lots of resources
and they told you that it was ready for the taking. What would you do? I'd conquer it.
What about another one and another one? I never have enough. People just said, it could
rain gold, it could rain gold or precious jewels and the shower of them would never be enough
for a human, for one being. It's a nature of craving. The very nature of craving
aside its habitual, its ever-increasing. It's self-perpetuating or self-enforcing or it feeds itself.
It becomes stronger the more you engage in it. It's a very scary thing. We're always unsatisfied.
We can never be satisfied. Seeing this many people leave home when they realize how
unsatisfied they are. They can eat and eat and all their gaining is more and more craving.
It's one of the big things you realize here is how attached we are to diversion,
how unable we are just to be with ourselves, just to have the patience to be such a simple thing.
They go that easy, but isn't it ridiculous how we can't just be,
we can be ourselves. We have to do, make, create, get. It's quite surprising because I think
we generally have this idea that we can just be that we always are. We're not good at
arming. We're not good at being. We have to do. We have to get so much craving and aversion
of so many in-built reactions to everything. So realizing this, realizing all four of these things,
this is what needs one to go forth, to become a monk, to practice meditation. This is the
encouragement, the reminder to all of us that we're here for a reason. We're here to figure this
out. We're going to solve this problem. The problem of impermanence and that the world, any goal,
we might attain any object we might acquire. Anything we might become will always be
unstable, uncertain impermanence. There's nothing that can keep us from the vicissitudes in life,
there's no protection, there's no refuge, there's nowhere we can go, nothing we can do
to avoid the vicissitudes in life. There's all of our possessions can protect us, all of our
friends and family. We leave them all behind. And all the things we hold dear and all the things
we crave and love and cling to are not ours. They'll be gone. We'll be gone from them soon enough.
And finally, that craving, we're slaves of craving, slaves of desire.
We are not masters of our desire. Our desires are not our possession that we can control.
They are the masters where we are the slave. Very scary reality. And the scariest, these scary realities
encourage us because there is something that can protect us. There is a refuge and that's the
number that's the truth, reality, seeing things as they are. Once we learn how to just be
when we become content, we become stable, we become invincible. There's no coming or going,
and we fear ourselves from suffering.
So there you go. There's the dhamma for tonight. Fine teaching,
and then on by Radhapana, Buddhist monk with the greatest faith and the greatest confidence
in going forth. No doubt in his mind. He would have done anything, he would have died
just to do what we're doing. It's a good example of
a religiosity and how wonderful and powerful it can be.
So there you go. Thank you for tuning in. Have a good night.
