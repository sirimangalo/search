1
00:00:00,000 --> 00:00:02,400
Hi, good evening.

2
00:00:02,400 --> 00:00:11,040
Tonight I thought I would continue my talks on the nature of reality, continuing where I

3
00:00:11,040 --> 00:00:22,160
last left off, explaining the existence of the mind as opposed to the existence of material

4
00:00:22,160 --> 00:00:24,120
only.

5
00:00:24,120 --> 00:00:28,040
And so just to recap and sort of summarize what I had said last time, maybe make any

6
00:00:28,040 --> 00:00:39,280
little bit more clear, basically the idea that there is only the material is about as reasonable

7
00:00:39,280 --> 00:00:43,720
as the idea that there is only mental.

8
00:00:43,720 --> 00:00:51,400
And so whereas we can make arguments to explain the mind away as simply a function of

9
00:00:51,400 --> 00:00:56,920
the body, what we could also explain the body away is simply a delusion or a function

10
00:00:56,920 --> 00:01:05,200
of the mind and how that works is, well from the body point of view we say that we can

11
00:01:05,200 --> 00:01:12,840
look at the body and we can look at the brain specifically and take parts of this and come

12
00:01:12,840 --> 00:01:16,960
to say that this part represents this mental function and so on and then we can say well

13
00:01:16,960 --> 00:01:20,320
that's where the mind comes from.

14
00:01:20,320 --> 00:01:26,160
We can on the other side we can look at the mind and we can say okay when I wish for seeing

15
00:01:26,160 --> 00:01:31,720
to arise and when I wish for moving to arise and there is moving and so we can say

16
00:01:31,720 --> 00:01:37,760
well these are just functions of the mind, the experience of hot, the experience of cold

17
00:01:37,760 --> 00:01:41,760
and so on, this is simply a result of the mind.

18
00:01:41,760 --> 00:01:50,800
Now both of these sort of miss the very obvious point that both body and mind very clearly

19
00:01:50,800 --> 00:01:59,840
do exist and so it's taking a leap of faith to say that one or the other just not exist.

20
00:01:59,840 --> 00:02:01,640
It's making an unreasonable claim.

21
00:02:01,640 --> 00:02:05,280
Now I think the problem that most materials would have with the idea that the mind

22
00:02:05,280 --> 00:02:12,560
exists is this idea that well once you allow the mind to exist then your or once you

23
00:02:12,560 --> 00:02:20,160
create a belief that the mind exists it's associated with the belief that there is a

24
00:02:20,160 --> 00:02:26,880
self where there is a controlling, there is a autonomous mind or something which is able

25
00:02:26,880 --> 00:02:33,480
to experience, is able to have emotions without relying or without being influenced by

26
00:02:33,480 --> 00:02:36,600
the body and of course this is not necessary.

27
00:02:36,600 --> 00:02:41,800
It's very possible to say the mind exists but it's very much interdependent with the body

28
00:02:41,800 --> 00:02:48,600
and it goes both ways because obviously the mind if you give it an existence or if you

29
00:02:48,600 --> 00:02:53,760
allow for it its existence then you can very clearly see how the mind affects the body,

30
00:02:53,760 --> 00:02:59,480
our experience can very much have an effect on the body and this has been carried out

31
00:02:59,480 --> 00:03:03,240
in neuroscience and so on.

32
00:03:03,240 --> 00:03:09,200
Now you can believe either way and say that it's all just a physical reality or you

33
00:03:09,200 --> 00:03:13,760
can say it's all just a mental reality but the truth of it is both of these realities

34
00:03:13,760 --> 00:03:17,680
are very much appear to exist, there's no reason to say that one or the other doesn't

35
00:03:17,680 --> 00:03:23,360
exist and they're very much different in their nature.

36
00:03:23,360 --> 00:03:30,520
So I'd like to then extrapolate on this and say well once you admit the existence of

37
00:03:30,520 --> 00:03:36,840
the mind without admitting that it is some autonomous self that is able to control or

38
00:03:36,840 --> 00:03:45,400
is able to determine one's future without the interaction or the interdependence on the

39
00:03:45,400 --> 00:03:46,720
body.

40
00:03:46,720 --> 00:03:50,840
Once you admit the existence of the mind then you have certain interesting corollaries

41
00:03:50,840 --> 00:03:57,080
and so this is what I like to talk about in my next videos so in order this video

42
00:03:57,080 --> 00:04:03,280
just be sort of an introduction and I'm going to talk about first of all the existence

43
00:04:03,280 --> 00:04:12,040
of real ethics which we can call absolute ethics as opposed to relative morality.

44
00:04:12,040 --> 00:04:19,880
The non-existence of self and the non-existence of God.

45
00:04:19,880 --> 00:04:24,000
I think these three things there are many other things that come from the realization

46
00:04:24,000 --> 00:04:28,480
of the existence of the mind and of course meditation is a very important one of those

47
00:04:28,480 --> 00:04:35,920
but in these ones I'm going to talk about very specifically ideas which I think people

48
00:04:35,920 --> 00:04:43,280
generally have problems with this is the absolute morality, the non-existence of self

49
00:04:43,280 --> 00:04:49,680
and the non-existence of God so please tune in and I'll try to address these in the next

50
00:04:49,680 --> 00:05:10,520
video.

