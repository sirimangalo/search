So, good evening everyone from broadcasting live February 4th, 2016.
Today's quote is interesting.
I had to read it a few times before I actually got a sense of what it means.
Still, I don't have it completely.
Translations a little bit weird.
So, let's break it up concerning those internal things.
I'm not convinced that's a very good translation.
I don't really understand it.
I don't think it's a part of the actual text.
It's much simpler than it's actually written.
So, say kassa beke beke uno, up, but dema de sassa, ma de sassa.
Say kassa, it's talking about a say kind of a learner.
One who is still in training.
So, there are two kinds of monks.
There are two kinds of Buddhists.
Say kassa and a say kassa.
Actually, I guess it's two kinds of enlightened beings, really.
Say kassa is someone who still is in training.
Say kate, one who trains, say kassa, one who is in training.
Say kassa is one who trains.
So, it's just a standard description of someone who's still striving.
And then we have a put dema de sassa.
One whose mind has not attained supreme freedom from yoga.
One who doesn't practice yoga.
No, supreme freedom from yoga.
Yoga is a bind, a bond.
Something that keeps you tired.
Something that keeps you stuck.
So, to attain supreme freedom from bondage.
Then there's this funny thing about internal things.
I read it as one who dwells.
And then something about having made who dwells, having made internal that which is inside.
I don't get it, but probably it's not good enough.
I think it means focusing on internal things.
One who dwells, focusing on internal things.
And then it just says there's one thing.
There's no other thing that is more useful than.
You only saw Manasika again, a very common Buddhist term that you almost don't want to translate,
but he translates as giving close attention.
Giving close attention to the mind.
Not quite how I would put it.
You only saw, you only saw Manas, you only actually means womb, the source or the womb.
So what it means is keeping things in mind, keeping the sources of things in mind or getting to the source of things.
Analyzing things from a point of view of their source.
So it can be used intellectually.
It's usually not.
It's usually used in terms of describing meditation.
It's a really good description.
Because insight meditation is all about getting to the source of things.
This is why we have this word that we use to remind ourselves.
To get to the your need, to get to the source.
Because normally when we experience things, we create so much more out of them.
That's all on a conceptual and abstract plane.
And we get stuck on this abstract plane that ends up being quite removed from reality.
It's removed from the source.
So you only saw Manasik Gharas about returning to the source of things.
Now, the Nani Mitah Gharinan will be handed in a Gharin.
Not grasping at the particulars, not grasping at the signs.
Nimitah is the sign.
So when you see a flower.
A sign of the flower is what makes you think it's a flower.
When you hear a car, the sign of the car is what makes you think of as a car.
The sign, and on the engine are the different characteristics, is it good or bad or beautiful
or ugly or sweet or sour or so.
So the characteristics that we don't cling to, we get to the source.
When you hear a car, the truth of it is the sound.
The source of it is the sound.
So the Buddha says this is the one thing that allows us, if there's nothing, if you take
nothing else, just have yoni soma nasi kara, and there's a nice verse that comes after yoni
so yoni soma nasi kara, damo se casa bikkuno.
Yoni soma nasi kara is the dama for bikkun in training.
The tanyo a vang bhukarum, kutamatasa patiya.
Not tanyo, there is no other dama that is of such great benefit that does so much as this
or the attainment of the highest benefit of the highest goal.
Yoni soma nasi kara bikkuno is for the source, very clear instruction, get to the source
of things.
It doesn't mean go back to their causes in the pastor's son, get to the source of the
present, like when you say to yourself, pain, pain, be with the source of the pain, hearing,
you're at the real, true source of the experience.
Yang dukasa papuni, the destruction or the end of duka, the end of suffering, papuni, the
reach, the reaches, papuni, not sure about them, papuni means it means attain, I just
don't know the tense, I think it means he reach, yeah, here we are, pot, oh it's will should
reach, would reach, papuni is potential, so it's simple but very powerful teaching, good
dad do, good to explain it, to be clear about what it means, as they quote the transitions
it's not ideal, it's not close attention to the mind, it's not the mind, it's the objects
that the mind keeps close attention to, the mind not just close attention but attention
to the source, yoni, so, yoni, so, but the hung be cool, a be cool who strives for the
source, or sticks to the source, doesn't get caught up in extrapolation or judgment
or reaction, it's amazing how it changes your outlook, just teaching students at the
university tomorrow again, they'll be teaching me, just give people a five minute meditation
lesson, you can show them something that they've never seen before, open a door that they've
never even knew was there, I've been teaching hour long sessions and people say they never
realized their mind was so chaotic or so messed up through not meditating, teach a course
on the internet, I've been teaching courses on the internet and people who I don't
know say that people who I've never met in person say they've changed their lives
or meditation, and now we have three meditators here who are really on the sake of path,
here at our house who are doing real intensive meditation, practicing many hours a day,
all of this is the good work, the best part of Buddhism, best part of the Buddhist teaching,
it's called doing the Buddhist work, like another religion, they have the Lord's work
or so on, they have the Buddhist work, so that's our dumb drop for today, thank you all
for tuning in, wishing you all the best practice, thank you for tuning in, wishing you
