1
00:00:00,000 --> 00:00:15,040
Today, I will finish up the set of talks on the Asava, the

2
00:00:15,040 --> 00:00:18,760
performance which make the mind go bad.

3
00:00:18,760 --> 00:00:28,520
It's important to understand not trying to make things seem

4
00:00:28,520 --> 00:00:33,520
very negative or very unpleasant.

5
00:00:33,520 --> 00:00:36,520
We're only trying to do away with that part of our

6
00:00:36,520 --> 00:00:40,520
existence which is undesirable.

7
00:00:40,520 --> 00:00:44,520
That part of our existence which is uncomfortable, which is

8
00:00:44,520 --> 00:00:46,520
unpleasant.

9
00:00:46,520 --> 00:00:49,520
All the good things, all of the wonderful things are the things

10
00:00:49,520 --> 00:00:51,520
which we should keep.

11
00:00:51,520 --> 00:00:54,520
Things like love, things like mindfulness and

12
00:00:54,520 --> 00:01:00,520
wisdom, consideration, gratitude, charity and so on.

13
00:01:00,520 --> 00:01:06,520
Compassion, peace, all of these good things are the things

14
00:01:06,520 --> 00:01:08,520
which we are striving for.

15
00:01:08,520 --> 00:01:11,520
But the reason why we don't achieve them all of the time is because

16
00:01:11,520 --> 00:01:15,520
of certain things which exist in our own hearts and in the hearts

17
00:01:15,520 --> 00:01:18,520
of the people around us.

18
00:01:18,520 --> 00:01:24,520
So when our unwholesome tendencies collide with other people's

19
00:01:24,520 --> 00:01:29,520
unwholesome mind states, this is where the cause of

20
00:01:29,520 --> 00:01:30,520
suffering.

21
00:01:30,520 --> 00:01:36,520
This is what leads to struggle, what leads to conflict which

22
00:01:36,520 --> 00:01:39,520
leads to suffering in the end.

23
00:01:39,520 --> 00:01:41,520
So we're trying to do away with these things.

24
00:01:41,520 --> 00:01:43,520
It's important to understand that we want to be happy.

25
00:01:43,520 --> 00:01:46,520
We're more practicing for the purpose of being at peace and

26
00:01:46,520 --> 00:01:49,520
being happy.

27
00:01:49,520 --> 00:01:52,520
What we're looking at like the doctor will look at the sickness

28
00:01:52,520 --> 00:01:53,520
in the body.

29
00:01:53,520 --> 00:01:56,520
We're looking at the sicknesses in the mind.

30
00:01:56,520 --> 00:01:58,520
We're strong enough.

31
00:01:58,520 --> 00:02:00,520
We are competent people.

32
00:02:00,520 --> 00:02:04,520
We are people who have strong minds.

33
00:02:04,520 --> 00:02:07,520
People who have strong goodness inside.

34
00:02:07,520 --> 00:02:10,520
We're not people who are set on doing evil deeds or set

35
00:02:10,520 --> 00:02:12,520
on doing bad deeds.

36
00:02:12,520 --> 00:02:15,520
So this is why we can talk about and we can teach and we

37
00:02:15,520 --> 00:02:17,520
can practice together to try and do away with these things

38
00:02:17,520 --> 00:02:21,520
because they are a relatively small part of our minds.

39
00:02:21,520 --> 00:02:24,520
All of the good things are very strong in our minds.

40
00:02:24,520 --> 00:02:29,520
This is why we're able to deal with these unwholesome things.

41
00:02:29,520 --> 00:02:33,520
And the reason why we come to do this is because of the

42
00:02:33,520 --> 00:02:35,520
wholesomeness in our mind.

43
00:02:35,520 --> 00:02:37,520
If we didn't have goodness in our minds, we wouldn't have

44
00:02:37,520 --> 00:02:40,520
any interest in doing away with the bad things.

45
00:02:40,520 --> 00:02:42,520
The things which are unpleasant, which are unwholesome,

46
00:02:42,520 --> 00:02:45,520
which are cause for suffering for ourselves and others.

47
00:02:48,520 --> 00:02:53,520
But since we are, and this is not a boasting or bragging,

48
00:02:53,520 --> 00:02:56,520
it's an offering encouragement to everyone who has

49
00:02:56,520 --> 00:02:59,520
come here and appreciation to everyone who comes to practice

50
00:02:59,520 --> 00:03:01,520
meditation.

51
00:03:01,520 --> 00:03:05,520
It's a very obvious stating an obvious fact that a person

52
00:03:05,520 --> 00:03:08,520
who comes to practice to develop themselves,

53
00:03:08,520 --> 00:03:11,520
to cultivate good deeds and good states of mind,

54
00:03:11,520 --> 00:03:15,520
wholesomeness states of mind, which lead to happiness and lead to peace.

55
00:03:15,520 --> 00:03:19,520
It takes quite a special person, a person who is really keen

56
00:03:19,520 --> 00:03:23,520
on bringing happiness and peace to this world.

57
00:03:23,520 --> 00:03:27,520
So it's worth appreciating and worth understanding in this way.

58
00:03:27,520 --> 00:03:31,520
And since we all, since the people who come here have these good

59
00:03:31,520 --> 00:03:36,520
states of mind, this is why they're able to address the

60
00:03:36,520 --> 00:03:40,520
unwholesome side because of the goodness which they have inside.

61
00:03:40,520 --> 00:03:48,520
And so today's talk is about the Palwanapahatapah,

62
00:03:48,520 --> 00:03:52,520
the azawat, the unwholesome states inside,

63
00:03:52,520 --> 00:03:58,520
which can be removed through developing or through cultivating,

64
00:03:58,520 --> 00:04:01,520
through bringing about a higher state of being,

65
00:04:01,520 --> 00:04:05,520
through raising up one's mind.

66
00:04:05,520 --> 00:04:15,520
Palwanah, and this is the peak of the talk.

67
00:04:15,520 --> 00:04:20,520
This is the culmination of all of the other ways and methods

68
00:04:20,520 --> 00:04:24,520
for overcoming wholesomeness and doing away with wholesomeness.

69
00:04:24,520 --> 00:04:28,520
In the end, they all meet,

70
00:04:28,520 --> 00:04:32,520
or they all come together and are for the purpose of this last one,

71
00:04:32,520 --> 00:04:36,520
which is mental cultivation for mental development.

72
00:04:36,520 --> 00:04:42,520
Palwanah, in classic Buddhist thought is of two kinds.

73
00:04:42,520 --> 00:04:46,520
So there's samata, palwanah, and wipasana, palwanah.

74
00:04:46,520 --> 00:04:50,520
But when it comes to ordinary practice for the purpose of doing

75
00:04:50,520 --> 00:04:54,520
away with defilements, it's really not necessary to divide

76
00:04:54,520 --> 00:04:58,520
meditation, practice, or mental development into two types.

77
00:04:58,520 --> 00:05:03,520
It's only a technical classification,

78
00:05:03,520 --> 00:05:06,520
and it's very useful for pointing out those types of meditation,

79
00:05:06,520 --> 00:05:11,520
which will indeed not lead to become freedom from defilement.

80
00:05:11,520 --> 00:05:13,520
Those states of meditation,

81
00:05:13,520 --> 00:05:18,520
which are for the purpose of simply dwelling in bliss,

82
00:05:18,520 --> 00:05:23,520
or dwelling in peace, in a state of calm,

83
00:05:23,520 --> 00:05:26,520
an equanimity, which has nothing to do with

84
00:05:26,520 --> 00:05:32,520
an understanding about ourselves and our minds.

85
00:05:32,520 --> 00:05:36,520
For instance, practicing meditation based on a concept,

86
00:05:36,520 --> 00:05:42,520
when we meditate based on a word mantra,

87
00:05:42,520 --> 00:05:46,520
a mantra in this case, which has nothing to do with the present moment,

88
00:05:46,520 --> 00:05:48,520
maybe on the past or the future,

89
00:05:48,520 --> 00:05:50,520
or maybe some conceptual reality,

90
00:05:50,520 --> 00:05:52,520
which we create in our minds.

91
00:05:52,520 --> 00:05:55,520
For focusing on the light, which doesn't exist,

92
00:05:55,520 --> 00:05:57,520
but which we create in our minds.

93
00:05:57,520 --> 00:05:59,520
So an imaginary concept.

94
00:05:59,520 --> 00:06:03,520
These kind of meditations will not lead ultimately to freedom

95
00:06:03,520 --> 00:06:07,520
from defilement, freedom from suffering,

96
00:06:07,520 --> 00:06:10,520
because they will not lead to wisdom and understanding,

97
00:06:10,520 --> 00:06:13,520
and they're not considered mental development.

98
00:06:13,520 --> 00:06:16,520
They don't lead us to understand the reason why we get angry,

99
00:06:16,520 --> 00:06:18,520
the reason why we get greedy.

100
00:06:18,520 --> 00:06:22,520
They only have the ability to cover up and replace greed and anger.

101
00:06:22,520 --> 00:06:26,520
They don't have the ability to replace delusion with wisdom,

102
00:06:26,520 --> 00:06:30,520
and so they can't be considered mental development,

103
00:06:30,520 --> 00:06:33,520
or bhavana in the highest sense.

104
00:06:33,520 --> 00:06:36,520
They could be considered samata bhavana in the sense

105
00:06:36,520 --> 00:06:39,520
that they only lead to states of calm,

106
00:06:39,520 --> 00:06:42,520
samata meaning calm or tranquility.

107
00:06:42,520 --> 00:06:45,520
But in the practice of the Lord Buddha,

108
00:06:45,520 --> 00:06:48,520
it is composed of both samata and vipas,

109
00:06:48,520 --> 00:06:51,520
and that means the mind becomes peaceful and calm,

110
00:06:51,520 --> 00:06:55,520
and also sees clearly at the same time.

111
00:06:55,520 --> 00:06:57,520
So when we practice rising, falling,

112
00:06:57,520 --> 00:07:01,520
or walking, stepping right, stepping like left,

113
00:07:01,520 --> 00:07:03,520
we can feel our minds calming down,

114
00:07:03,520 --> 00:07:06,520
becoming more relaxed and more at peace

115
00:07:06,520 --> 00:07:08,520
as we become better and more proficient.

116
00:07:08,520 --> 00:07:12,520
And if we're really sincere about our mind, our acknowledgement.

117
00:07:14,520 --> 00:07:17,520
And at the same time, we come to see the truth

118
00:07:17,520 --> 00:07:19,520
about the things which we hold on to,

119
00:07:19,520 --> 00:07:23,520
which we grasp after, which we are addicted to,

120
00:07:23,520 --> 00:07:25,520
and desire for.

121
00:07:25,520 --> 00:07:27,520
We can see that they are impermanent,

122
00:07:27,520 --> 00:07:29,520
we can see that they are unsatisfying,

123
00:07:29,520 --> 00:07:33,520
and we can see that they are not under our control.

124
00:07:33,520 --> 00:07:36,520
When we see in this way, we come to let go,

125
00:07:36,520 --> 00:07:38,520
and we come to truly be free from the asua.

126
00:07:38,520 --> 00:07:42,520
And so, and this is called insight through seeing clearly,

127
00:07:42,520 --> 00:07:44,520
vipasana.

128
00:07:44,520 --> 00:07:48,520
So the practice which we follow is both samata and vipasana,

129
00:07:48,520 --> 00:07:51,520
because it makes the mind at peace and at rest,

130
00:07:51,520 --> 00:07:54,520
but ultimately through insight,

131
00:07:54,520 --> 00:07:58,520
not through repressing the defilements

132
00:07:58,520 --> 00:08:01,520
or avoiding bad mind states,

133
00:08:01,520 --> 00:08:05,520
but rather examining them and coming to see the cause of suffering

134
00:08:05,520 --> 00:08:07,520
and those states which lead the suffering,

135
00:08:07,520 --> 00:08:10,520
those states which lead to peace and happiness

136
00:08:10,520 --> 00:08:13,520
and coming to see the difference and see the truth

137
00:08:13,520 --> 00:08:20,520
about the things which we grasp after.

138
00:08:20,520 --> 00:08:22,520
And according to the Lord Buddha,

139
00:08:22,520 --> 00:08:26,520
this type of power now or mental development

140
00:08:26,520 --> 00:08:28,520
is composed of seven factors,

141
00:08:28,520 --> 00:08:30,520
and this is the seven factors,

142
00:08:30,520 --> 00:08:32,520
the power na pahatapah,

143
00:08:32,520 --> 00:08:37,520
the things which lead one to destroy or do away with,

144
00:08:37,520 --> 00:08:42,520
or clean up, clean out the asua.

145
00:08:42,520 --> 00:08:46,520
And these do it in the highest sense.

146
00:08:46,520 --> 00:08:47,520
And these all together,

147
00:08:47,520 --> 00:08:49,520
they start with mindfulness.

148
00:08:49,520 --> 00:08:54,520
The first part of mental development is mindfulness, of course.

149
00:08:54,520 --> 00:08:57,520
It's called sati sampochon.

150
00:08:57,520 --> 00:08:59,520
Sati sampochon kat.

151
00:08:59,520 --> 00:09:03,520
And bhod kat means wisdom or enlightenment.

152
00:09:03,520 --> 00:09:07,520
An kat means a member or a factor.

153
00:09:07,520 --> 00:09:10,520
So this is the factor of enlightenment

154
00:09:10,520 --> 00:09:14,520
or the factor of seeing clearly or awakening,

155
00:09:14,520 --> 00:09:18,520
waking up to the truth, which is called mindfulness.

156
00:09:18,520 --> 00:09:21,520
And this is very clear, I think, to meditators

157
00:09:21,520 --> 00:09:23,520
even when they first come to practice.

158
00:09:23,520 --> 00:09:26,520
Mindfulness is something which really does wake you up

159
00:09:26,520 --> 00:09:29,520
to the state of mind which you live with.

160
00:09:29,520 --> 00:09:32,520
Many times we don't see how distracted,

161
00:09:32,520 --> 00:09:35,520
how corrupted our mind has become.

162
00:09:35,520 --> 00:09:39,520
Our mind has become like a wild animal.

163
00:09:39,520 --> 00:09:42,520
Through lack of training.

164
00:09:42,520 --> 00:09:45,520
And it's going to take quite a bit of work to change.

165
00:09:45,520 --> 00:09:47,520
This is waking up.

166
00:09:47,520 --> 00:09:50,520
This is the first part of mental development.

167
00:09:50,520 --> 00:09:53,520
So it's, of course, based on the four foundations of mindfulness.

168
00:09:53,520 --> 00:09:55,520
When we're mindful of the body,

169
00:09:55,520 --> 00:09:57,520
stepping right, stepping left,

170
00:09:57,520 --> 00:09:59,520
or sitting rising, falling.

171
00:09:59,520 --> 00:10:01,520
This is mindfulness of the body.

172
00:10:01,520 --> 00:10:06,520
This is the first part of sati mindfulness.

173
00:10:06,520 --> 00:10:10,520
I'm creating the clear thought based on the body

174
00:10:10,520 --> 00:10:11,520
as the Lord Buddha said,

175
00:10:11,520 --> 00:10:13,520
katsantu wagatamiti percentiti.

176
00:10:13,520 --> 00:10:17,520
When walking, they know, I'm walking, or walking.

177
00:10:17,520 --> 00:10:19,520
In English, it's harder to say,

178
00:10:19,520 --> 00:10:22,520
I am walking, it's a longer than in the valley.

179
00:10:22,520 --> 00:10:24,520
So we simply say walking, walking,

180
00:10:24,520 --> 00:10:26,520
walking, or we break it up into pieces.

181
00:10:26,520 --> 00:10:28,520
Stepping, right, stepping left,

182
00:10:28,520 --> 00:10:31,520
or lifting, placing, lifting, placing,

183
00:10:31,520 --> 00:10:33,520
or lifting, moving, placing.

184
00:10:33,520 --> 00:10:37,520
And so on.

185
00:10:37,520 --> 00:10:41,520
But the most important thing is creating the clear thought

186
00:10:41,520 --> 00:10:42,520
in the present moment.

187
00:10:42,520 --> 00:10:43,520
This is mindfulness.

188
00:10:43,520 --> 00:10:46,520
The clear thought which replaces the distracted

189
00:10:46,520 --> 00:10:49,520
or the deluded or the judgmental thought.

190
00:10:49,520 --> 00:10:55,520
Instead of clear thought based on present the present reality.

191
00:10:55,520 --> 00:10:57,520
Body, and then we have feelings.

192
00:10:57,520 --> 00:10:59,520
It's the same when we have painful feelings

193
00:10:59,520 --> 00:11:01,520
or calm feelings or happy feelings.

194
00:11:01,520 --> 00:11:04,520
We simply say pain.

195
00:11:04,520 --> 00:11:06,520
Creating a clear thought, reminding ourselves

196
00:11:06,520 --> 00:11:08,520
that it's only pain.

197
00:11:08,520 --> 00:11:11,520
We feel like reminding ourselves that it's only happiness.

198
00:11:11,520 --> 00:11:14,520
Calm, reminding ourselves that it's only calm

199
00:11:14,520 --> 00:11:17,520
and not holding on to any of these states of mind.

200
00:11:17,520 --> 00:11:20,520
When we have thoughts,

201
00:11:20,520 --> 00:11:22,520
this is mindfulness of the mind.

202
00:11:22,520 --> 00:11:24,520
We're thinking about the past or future.

203
00:11:24,520 --> 00:11:26,520
Thinking good thoughts, thinking bad thoughts.

204
00:11:26,520 --> 00:11:28,520
We simply say to ourselves,

205
00:11:28,520 --> 00:11:31,520
thinking, thinking, thinking, thinking,

206
00:11:31,520 --> 00:11:33,520
not judging, replacing the thinking

207
00:11:33,520 --> 00:11:36,520
with a new kind of thinking which is clearly aware.

208
00:11:36,520 --> 00:11:38,520
Like the mind, it's called the mind,

209
00:11:38,520 --> 00:11:40,520
knows the mind.

210
00:11:40,520 --> 00:11:45,520
It's the mind in the mind.

211
00:11:45,520 --> 00:11:47,520
Seeing the mind in the mind.

212
00:11:47,520 --> 00:11:50,520
Seeing a part of the mind which is present

213
00:11:50,520 --> 00:11:53,520
in this case, the thoughts.

214
00:11:53,520 --> 00:11:56,520
And then mindfulness of the mind objects

215
00:11:56,520 --> 00:11:58,520
of the dhamma in the states of mind.

216
00:11:58,520 --> 00:12:01,520
So in this case, the five hindrance is liking,

217
00:12:01,520 --> 00:12:04,520
disliking, drowsiness, distraction, doubt.

218
00:12:04,520 --> 00:12:08,520
And these five things are very important for meditators.

219
00:12:08,520 --> 00:12:12,520
They can, they're a cause for the meditation to go bad

220
00:12:12,520 --> 00:12:13,520
to go wrong.

221
00:12:13,520 --> 00:12:15,520
If these five things exist in the mind

222
00:12:15,520 --> 00:12:19,520
and are not removed or not rooted out,

223
00:12:19,520 --> 00:12:21,520
they will ever and again cause stress

224
00:12:21,520 --> 00:12:24,520
and suffering and a general inability

225
00:12:24,520 --> 00:12:26,520
to continue in the practice.

226
00:12:26,520 --> 00:12:29,520
So it's important that we are very vigilant

227
00:12:29,520 --> 00:12:31,520
in doing away with this when they arise,

228
00:12:31,520 --> 00:12:34,520
liking, when we like something we say to ourselves,

229
00:12:34,520 --> 00:12:38,520
liking, liking, when we don't like something disliking, disliking.

230
00:12:38,520 --> 00:12:41,520
We feel drows, you we say drowsy, drowsy.

231
00:12:41,520 --> 00:12:45,520
Distract, could we say distracting, distracting.

232
00:12:45,520 --> 00:12:48,520
And we doubt we say doubting, doubting, doubting.

233
00:12:48,520 --> 00:12:50,520
And this is the five hindrance

234
00:12:50,520 --> 00:12:55,520
is this is under the heading of dhamma or mind states.

235
00:12:55,520 --> 00:12:57,520
Now two more things that we have to keep in mind

236
00:12:57,520 --> 00:12:59,520
in terms of mindfulness.

237
00:12:59,520 --> 00:13:01,520
These are the things which I've explained

238
00:13:01,520 --> 00:13:03,520
for the during the time when we're not practicing,

239
00:13:03,520 --> 00:13:06,520
when we're not doing walking or when we're not doing sitting.

240
00:13:06,520 --> 00:13:08,520
When we're not doing walking or sitting,

241
00:13:08,520 --> 00:13:10,520
we have to be aware of the four pictures,

242
00:13:10,520 --> 00:13:12,520
standing, walking, sitting in the mind,

243
00:13:12,520 --> 00:13:14,520
understanding, standing, standing.

244
00:13:14,520 --> 00:13:17,520
When we walk, walking, walking,

245
00:13:17,520 --> 00:13:21,520
when we sit, sitting, sitting, when we lie down, lying, lying,

246
00:13:21,520 --> 00:13:23,520
and so on.

247
00:13:23,520 --> 00:13:26,520
And then the sixth sense is seeing, hearing, smelling,

248
00:13:26,520 --> 00:13:28,520
tasting, feeling, thinking, just saying to ourselves,

249
00:13:28,520 --> 00:13:31,520
seeing, seeing, hearing, hearing, and so on.

250
00:13:31,520 --> 00:13:36,520
When we see, hear, smell, taste, feel, I think, anything.

251
00:13:36,520 --> 00:13:41,520
Acknowledging in this way is that it creates a state of

252
00:13:41,520 --> 00:13:43,520
meant of developed mind.

253
00:13:43,520 --> 00:13:46,520
It's a way of cultivating the other factors

254
00:13:46,520 --> 00:13:47,520
of enlightenment.

255
00:13:47,520 --> 00:13:49,520
When we start off with mindfulness,

256
00:13:49,520 --> 00:13:52,520
all of the other factors of enlightenment arise in order.

257
00:13:52,520 --> 00:13:54,520
So this is the first one.

258
00:13:54,520 --> 00:13:58,520
And it is also one which we have to keep coming back to.

259
00:13:58,520 --> 00:14:00,520
We can't say, once we have mindfulness,

260
00:14:00,520 --> 00:14:03,520
then we can go on to the next one and forget about mindfulness.

261
00:14:03,520 --> 00:14:06,520
Until we have, for as long as we have mindfulness,

262
00:14:06,520 --> 00:14:09,520
the other factors will continue to arise,

263
00:14:09,520 --> 00:14:12,520
but when we give upfulness, the other factors will not be able

264
00:14:12,520 --> 00:14:17,520
to send us on to become awake, to wake up.

265
00:14:17,520 --> 00:14:21,520
It has to come from mindfulness to create the other six factors.

266
00:14:21,520 --> 00:14:24,520
At all times, mindfulness is not what I said,

267
00:14:24,520 --> 00:14:28,520
it's a team to go away, it's a fatigue come with harmony.

268
00:14:28,520 --> 00:14:31,520
Mindfulness so monks is always useful.

269
00:14:31,520 --> 00:14:33,520
There's ever of use.

270
00:14:33,520 --> 00:14:36,520
There's always beneficial.

271
00:14:36,520 --> 00:14:41,520
It's something which we have to always continue back.

272
00:14:41,520 --> 00:14:43,520
Continue developing.

273
00:14:43,520 --> 00:14:46,520
Until the moment when we become free from suffering.

274
00:14:46,520 --> 00:14:49,520
Until the moment when we wake up and realize the truth

275
00:14:49,520 --> 00:14:52,520
and let go and become free.

276
00:14:52,520 --> 00:14:54,520
This is the first one.

277
00:14:54,520 --> 00:14:56,520
The second one, once we have mindfulness,

278
00:14:56,520 --> 00:15:00,520
then there will arise a certain understanding.

279
00:15:00,520 --> 00:15:02,520
It's called tama witsaya.

280
00:15:02,520 --> 00:15:06,520
And this is, it's called considering,

281
00:15:06,520 --> 00:15:10,520
or discriminating between mind states.

282
00:15:10,520 --> 00:15:14,520
So we'll create, there will arise a discrimination

283
00:15:14,520 --> 00:15:16,520
which is non-judgmental,

284
00:15:16,520 --> 00:15:19,520
which simply sees things for what they are.

285
00:15:19,520 --> 00:15:22,520
It sees those things which are impermanent as impermanence,

286
00:15:22,520 --> 00:15:25,520
sees those things which are unsatisfying as unsatisfying,

287
00:15:25,520 --> 00:15:28,520
sees those things which are not under our control,

288
00:15:28,520 --> 00:15:29,520
as not under our control.

289
00:15:29,520 --> 00:15:32,520
And it will start to see more and more clearly

290
00:15:32,520 --> 00:15:34,520
that actually there's no difference.

291
00:15:34,520 --> 00:15:38,520
And any kind of judgment is simply a delusion in the mind.

292
00:15:38,520 --> 00:15:40,520
That everything inside of us in the world around us

293
00:15:40,520 --> 00:15:42,520
has the same characteristics.

294
00:15:42,520 --> 00:15:46,520
And this is impermanence, suffering, impermanence,

295
00:15:46,520 --> 00:15:50,520
unsatisfying, and not under our control.

296
00:15:50,520 --> 00:15:53,520
And impermanence, suffering, and non-self.

297
00:15:53,520 --> 00:15:56,520
These three characteristics are called the samañilakana.

298
00:15:56,520 --> 00:16:01,520
They exist in everything around us and inside of us

299
00:16:01,520 --> 00:16:04,520
in the world around us.

300
00:16:04,520 --> 00:16:06,520
When we see in this way, this is tama witsaya.

301
00:16:06,520 --> 00:16:09,520
This is the process of letting go.

302
00:16:09,520 --> 00:16:11,520
Once we have tama witsaya,

303
00:16:11,520 --> 00:16:18,520
we will start to develop this very set state of effort

304
00:16:18,520 --> 00:16:19,520
or exertion.

305
00:16:19,520 --> 00:16:21,520
It's called vitya, sambhui sambhui sambhui.

306
00:16:21,520 --> 00:16:24,520
The factor of enlightenment, which is effort.

307
00:16:24,520 --> 00:16:26,520
In the beginning, it's very difficult to put out effort.

308
00:16:26,520 --> 00:16:28,520
We put out effort often in the wrong way.

309
00:16:28,520 --> 00:16:30,520
We find ourselves walking and sitting,

310
00:16:30,520 --> 00:16:32,520
but not really being mindful.

311
00:16:32,520 --> 00:16:34,520
But once we start to see impermanence,

312
00:16:34,520 --> 00:16:37,520
suffering and non-self, we don't hold on to anything.

313
00:16:37,520 --> 00:16:39,520
When it comes up, we let it go.

314
00:16:39,520 --> 00:16:40,520
We acknowledge, let it go.

315
00:16:40,520 --> 00:16:42,520
We say, when we use mindfulness

316
00:16:42,520 --> 00:16:44,520
and continue to let it go, let it go,

317
00:16:44,520 --> 00:16:46,520
not holding on to anything.

318
00:16:46,520 --> 00:16:48,520
And we'll start to develop like a train

319
00:16:48,520 --> 00:16:50,520
when it first starts up as very slow

320
00:16:50,520 --> 00:16:53,520
and slowly gains momentum.

321
00:16:53,520 --> 00:16:56,520
As we start to practice, we'll find ourselves gaining momentum.

322
00:16:56,520 --> 00:16:58,520
This is vitya.

323
00:16:58,520 --> 00:17:00,520
And then vitya, it will arise.

324
00:17:00,520 --> 00:17:04,520
Vitya sambhui sambhui sambhui sambhui sambhui sambhui sambhui sambhui sambhui sambhui

325
00:17:04,520 --> 00:17:07,520
means our minds will like the locomotive.

326
00:17:07,520 --> 00:17:09,520
Once it's going, it starts going and going.

327
00:17:09,520 --> 00:17:11,520
It starts to go of its own power.

328
00:17:11,520 --> 00:17:14,520
The rapture means the mind starts to work on its own.

329
00:17:14,520 --> 00:17:19,520
It starts to fall into the track, fall into the groove,

330
00:17:19,520 --> 00:17:21,520
finding mindfulness of very natural

331
00:17:21,520 --> 00:17:25,520
and a very peaceful and a very harmonious way to live.

332
00:17:25,520 --> 00:17:28,520
Simply replacing, creating a clear thought

333
00:17:28,520 --> 00:17:35,480
thought, instead of all of the unclear, diluted, fuzzy thoughts, which continue on ever.

334
00:17:35,480 --> 00:17:39,880
And again, we replace these with clear thought, simply knowing, simply being aware of

335
00:17:39,880 --> 00:17:47,320
everything we do, according to the Satipatanasupta, the discourse of mindfulness.

336
00:17:47,320 --> 00:17:51,400
This is BT, our mind will become, at this time, many meditators will have such a

337
00:17:51,400 --> 00:17:58,120
charged state that they might find themselves getting goosebumps, that they might find themselves

338
00:17:58,120 --> 00:18:04,200
crying for no apparent reason, they might find themselves smiling, they might find themselves

339
00:18:04,200 --> 00:18:09,400
feeling very light, find themselves rocking back and forth or so on.

340
00:18:09,400 --> 00:18:13,080
All of these things we have to be careful that we don't get stuck on as well.

341
00:18:13,080 --> 00:18:16,360
They're very good things, they're a sign of good progress.

342
00:18:16,360 --> 00:18:20,760
It's a factor of enlightenment, but we have to be careful that we don't get stuck on

343
00:18:20,760 --> 00:18:25,480
when we cry, we have to say crying, crying, we feel happy, we have to say happy,

344
00:18:25,480 --> 00:18:31,720
we feel light, we have to say light, we see anything, we have to say seeing and so on.

345
00:18:35,160 --> 00:18:42,200
Once we have rapture, then there will arise, busity, busity is tranquility, then the mind will

346
00:18:42,200 --> 00:18:48,600
really start to calm down. All this time, the mind is still very active, and the meditators

347
00:18:48,600 --> 00:18:54,280
sometimes feel like they're hopeless, like they're not able to carry out the job,

348
00:18:54,280 --> 00:19:01,560
which they've undertaken. They feel like they're not able to get the results, which they

349
00:19:01,560 --> 00:19:02,440
had hoped for.

350
00:19:06,440 --> 00:19:13,320
Do you know a manang, manang kon, kam manang kon, you know, good to sell out, a vanity.

351
00:19:17,160 --> 00:19:23,720
And this is because in our daily lives, we had no meditation practice.

352
00:19:23,720 --> 00:19:30,760
We had not developed our minds in any way. Only when we do come to practice, then we begin to train

353
00:19:30,760 --> 00:19:36,440
our minds. And we have to deal with so many years of a mind which is untrained, which we just

354
00:19:36,440 --> 00:19:44,600
let go in any way it wanted. And we see now the disadvantages are the fault in such a way of

355
00:19:44,600 --> 00:19:51,320
existence, just letting our minds wander and develop bad habits and encouraging bad habits.

356
00:19:51,320 --> 00:19:55,960
And now when we come to train ourselves, yes, in the beginning it's very difficult,

357
00:19:55,960 --> 00:20:01,960
but this is not a reason for meditators to become discouraged. It's only a reason to understand

358
00:20:03,800 --> 00:20:11,160
that there is indeed a correct and an incorrect way to keep the mind to use the mind.

359
00:20:13,480 --> 00:20:17,560
And when we come to practice, we can see what is the correct way and the correct way is to have

360
00:20:17,560 --> 00:20:22,360
clear thought at all times, simply be aware of whatever comes up, whether it's a good thing or a bad

361
00:20:22,360 --> 00:20:27,640
thing, not be judging, not be judgmental about it, even our judgments, not to be judgmental about

362
00:20:27,640 --> 00:20:33,560
them. To see that these are only an old way of thinking away, which is not conducive to happiness,

363
00:20:34,280 --> 00:20:41,240
but can only lead to suffering if we let it. And learning to let go of it. This is then the mind

364
00:20:41,240 --> 00:20:47,480
will calm down. Once we practice on and practice on eventually, we won't have to put out any

365
00:20:47,480 --> 00:20:54,520
effort. Our mind will calm down of its own, of its own, innate, of its own volition.

366
00:20:56,360 --> 00:21:01,960
Once the mind calms down, plus the tea arises, then the arise is cemetery, which is concentration.

367
00:21:01,960 --> 00:21:08,760
The mind will be fixed very strongly, like playing a piano. The hands will be very sure

368
00:21:10,120 --> 00:21:15,480
once we do away with all of our old habits, using our hands, and we can learn to play the

369
00:21:15,480 --> 00:21:22,200
piano. Our fingers will be very sure on the keys, and we'll find we're able to play the notes

370
00:21:22,200 --> 00:21:28,680
very accurately. In meditation, it's the same as for when we practice, when we say rising, our

371
00:21:28,680 --> 00:21:33,400
mind will be very clearly aware of the rising. We say falling, the mind will be very clearly

372
00:21:33,400 --> 00:21:39,080
aware of the falling. Rising, falling, rising, falling, and as the same with everything.

373
00:21:39,800 --> 00:21:44,520
This is what we have to develop. All of these things we have to develop, and these are

374
00:21:44,520 --> 00:21:49,480
mental development. When we develop them, starting with mindfulness up to all the way up to

375
00:21:49,480 --> 00:21:55,080
concentration, there will arise the seventh one, which is equanimity, and this is an equanimity,

376
00:21:55,080 --> 00:22:01,880
which is at complete peace with the world around it. It sees everything as simply for what it is.

377
00:22:02,840 --> 00:22:08,760
Just like a tree lives in nature without judging, without getting upset, or just as a rock,

378
00:22:08,760 --> 00:22:17,240
or a mountain, just as a very true part of nature. We become a very strange sort of being,

379
00:22:17,240 --> 00:22:22,840
a being which is very difficult to find. When it doesn't fight, that doesn't get in conflict,

380
00:22:22,840 --> 00:22:29,720
that doesn't cry, that doesn't get sad, doesn't grieve, doesn't get upset,

381
00:22:29,720 --> 00:22:41,800
doesn't feel any sense of sorrow or loss at anything whatsoever. A person who is completely

382
00:22:41,800 --> 00:22:47,000
at peace with the world around them, because they have found the truth. They have seen the truth,

383
00:22:47,000 --> 00:22:53,080
and their mind has become set in the truth through the other six factors. So all together,

384
00:22:53,080 --> 00:22:57,080
these are the seven factors of enlightenment, and when they come together in this way,

385
00:22:57,080 --> 00:23:03,800
the equanimity which arises will send one into freedom. Just like an airplane when the airplane

386
00:23:03,800 --> 00:23:10,840
goes and goes through seven stages, and at the seventh stage, the airplane is fast enough,

387
00:23:10,840 --> 00:23:17,400
it can fly. The moment when we have equanimity, the equanimity will be the last stage to allow us

388
00:23:17,400 --> 00:23:25,000
to fly and be free from any sort of attachment or any sort of bond, just like a bird,

389
00:23:25,000 --> 00:23:30,120
or an airplane flying in the sky without any friction or any bond whatsoever.

390
00:23:30,120 --> 00:23:35,160
These seven altogether are the things which we have to develop. Most importantly, mindfulness,

391
00:23:35,160 --> 00:23:39,640
it's not important to remember everything. If you get the sort of gist of it,

392
00:23:39,640 --> 00:23:44,120
you can see where mindfulness is leading and eventually will become a complete peace with everything.

393
00:23:44,760 --> 00:23:48,280
And we can see this if we're careful and if we're honest with ourselves,

394
00:23:48,280 --> 00:23:54,200
we can see that the correct practice does lead indeed to true peace and harmony.

395
00:23:54,840 --> 00:23:58,520
It's only up to us to continue practicing in the correct manner.

396
00:24:00,120 --> 00:24:06,680
And that's all now for the talk on the Sadhatsawa. If you're interested in learning more about it,

397
00:24:06,680 --> 00:24:10,920
it's in the second discourse of the Matimani Kaya in the middle-length discourses.

398
00:24:10,920 --> 00:24:19,000
And otherwise, we just continue practicing and they sincerely wish for everyone listening.

399
00:24:20,840 --> 00:24:25,320
They are able to find freedom from the things which make the mind go bad,

400
00:24:25,320 --> 00:24:29,720
which cause the mind to go sour. They are able to do away with mental environments

401
00:24:29,720 --> 00:24:34,120
and come to true peace and true freedom on their own time.

402
00:24:34,120 --> 00:24:44,120
And that's all for today. We'll continue with meditation practice at your own pace.

