1
00:00:00,000 --> 00:00:08,240
Good evening everyone, welcome to our live broadcast.

2
00:00:08,240 --> 00:00:16,520
Tonight we are going to go straight into questions and answers, we just had our volunteer

3
00:00:16,520 --> 00:00:23,920
meeting so I didn't actually research something to talk about tonight, I could come up

4
00:00:23,920 --> 00:00:30,920
with something off the top of my head but I think we'll just keep it short tonight,

5
00:00:30,920 --> 00:00:37,520
answer people's questions and really look at this every night so it's not like you're

6
00:00:37,520 --> 00:00:45,720
missing out, we'll be back again tomorrow.

7
00:00:45,720 --> 00:00:56,200
So Robin's having sound issues I think unfortunately, so if and when you're able to

8
00:00:56,200 --> 00:01:10,720
talk, go ahead but until I hear you, I will start off the questions.

9
00:01:10,720 --> 00:01:24,400
Does acceptance arise from dispassion, acceptance arise from dispassion, acceptance isn't

10
00:01:24,400 --> 00:01:27,920
exactly what we want, you see.

11
00:01:27,920 --> 00:01:35,480
We don't use the word acceptance that much because it's not exactly, it's not at all

12
00:01:35,480 --> 00:01:43,200
really what we are, it's improper, it's incorrect, it's an improper description of the

13
00:01:43,200 --> 00:01:48,840
goal, we're not accepting anything.

14
00:01:48,840 --> 00:01:54,640
You might, I mean some two-team is probably the closest song means with and two-team means

15
00:01:54,640 --> 00:02:02,160
happiness, so it means being happy with what you have, but that doesn't refer to

16
00:02:02,160 --> 00:02:12,720
you might argue that that's acceptance, but that's just one quality that we have describing

17
00:02:12,720 --> 00:02:18,280
the Buddha's teaching, acceptance is not the core value, dispassion leads to turning

18
00:02:18,280 --> 00:02:22,920
away actually, discarding.

19
00:02:22,920 --> 00:02:29,320
We don't accept anything about the universe, we discard everything about the universe,

20
00:02:29,320 --> 00:02:41,320
we dismiss it as meaningless, pointless of no consequence of no value, the only thing

21
00:02:41,320 --> 00:02:56,760
we accept, we accept the truth about reality, and maybe it's just nitpicking because

22
00:02:56,760 --> 00:03:05,680
it is to some extent an acceptance in terms of not getting upset, but it's not an acceptance

23
00:03:05,680 --> 00:03:14,840
exactly because you're still clear in your mind that there's no benefit to the universe,

24
00:03:14,840 --> 00:03:21,320
to any aspect of some sorrow, so it's really a rejection of some sorrow, and it's that rejection

25
00:03:21,320 --> 00:03:29,320
which brings about or creates the conditions for the realization of nibana, it only comes

26
00:03:29,320 --> 00:03:33,760
about not by acceptance but by rejection.

27
00:03:33,760 --> 00:03:41,000
The moment before realizing nibana, nibana is a moment of realizing that reality is

28
00:03:41,000 --> 00:03:49,920
superman and unsatisfying and controllable and rejecting it, so I guess it just means

29
00:03:49,920 --> 00:03:55,040
it would be a little bit careful with the word acceptance, but some form of acceptance

30
00:03:55,040 --> 00:04:06,320
could be seen as coming about from that process in the sense of giving up your ambitions,

31
00:04:06,320 --> 00:04:13,640
accepting that you're not ever going to get stability, satisfaction, or control from

32
00:04:13,640 --> 00:04:23,840
some sorrow, and thereby giving up, but it's more about giving up than accepting.

33
00:04:23,840 --> 00:04:33,280
Acceptance, the preceding cause of loving kindness, kind of accepting people as they are.

34
00:04:33,280 --> 00:04:42,560
Equanimity is caused by acceptance, in the sense of accepting that people are the way

35
00:04:42,560 --> 00:04:47,160
they are and that you're not going to change them.

36
00:04:47,160 --> 00:04:53,880
Equanimity is the big one, meta, no meta, friendliness or loving kindness, the cause

37
00:04:53,880 --> 00:05:06,440
of that is seeing a potential for people to be happy and thinking about a thought of someone

38
00:05:06,440 --> 00:05:15,360
obtaining happiness, and as a result you feel happy or you feel a desire for them to be

39
00:05:15,360 --> 00:05:23,640
happy in a sense, for the inclination for their happiness, for them to obtain the happiness.

40
00:05:23,640 --> 00:05:29,760
But acceptance is what leads to unanimity, if someone's just a terrible, horrible person,

41
00:05:29,760 --> 00:05:37,280
you might try to help them or alleviate the problem, but if you can't, then you accept

42
00:05:37,280 --> 00:05:45,200
it, in a sense, and have unanimity, except that you can't change them.

43
00:05:45,200 --> 00:05:52,360
No, we had this one, no, could I replace formal practice with just sitting down, doing

44
00:05:52,360 --> 00:05:58,160
nothing, no, we had that last night, are we going to use this site anymore, no this site

45
00:05:58,160 --> 00:06:03,400
is closing down, unfortunately, I'm actually kind of fond of the layout of this site, having

46
00:06:03,400 --> 00:06:11,680
it all on one page as opposed to the tabs, but the new site is going to supersede this.

47
00:06:11,680 --> 00:06:15,520
So if you have complaints about the UI, if you'd rather see it, look some other way,

48
00:06:15,520 --> 00:06:24,200
you'll talk to us on GitHub, I don't think there's even a link, but I'll post the link

49
00:06:24,200 --> 00:06:29,080
here, and then you guys can hang on to this link, because I'm posting issues, you have

50
00:06:29,080 --> 00:06:34,480
to learn a little bit about GitHub, here's the issues paint.

51
00:06:34,480 --> 00:06:40,280
So you go there if you have an issue with the new site, not this site, the new site,

52
00:06:40,280 --> 00:06:43,080
that we're working on.

53
00:06:43,080 --> 00:06:48,760
Rob and you're still mute.

54
00:06:48,760 --> 00:06:55,760
Yeah, don't hear you, I see your lips moving, more sound coming out.

55
00:07:18,760 --> 00:07:27,640
Monday post the really long joke, fortunately, I don't know time to read it.

56
00:07:27,640 --> 00:07:31,560
Regarding your talk yesterday, I was wondering what makes a person greater equal less

57
00:07:31,560 --> 00:07:42,880
to someone, right, well, you could say even worldly matters, worldly affairs would count

58
00:07:42,880 --> 00:07:50,120
like if someone is richer, no, that wouldn't work with it.

59
00:07:50,120 --> 00:07:56,160
Well, yeah, suppose someone has a lot of money, and thereby thinks of themselves as

60
00:07:56,160 --> 00:08:00,720
greater than another person, that's conceit, right, if I've got a lot of money and I look

61
00:08:00,720 --> 00:08:08,440
at a poor person and say, I'm greater than them, that conceiving of being greater, even

62
00:08:08,440 --> 00:08:13,560
though they're not greater, we could argue that money from a Buddhist point of view doesn't

63
00:08:13,560 --> 00:08:18,280
make someone greater, and therefore, here's a case of someone thinking they were greater

64
00:08:18,280 --> 00:08:20,960
when they're not actually greater.

65
00:08:20,960 --> 00:08:29,520
On the other hand, you could have someone who is pure of mind, right, they're kind and

66
00:08:29,520 --> 00:08:38,520
generous and compassionate, and they're wise, but they might have low self esteem, and

67
00:08:38,520 --> 00:08:43,000
they might be always worried about, they might have the fault of being worried about

68
00:08:43,000 --> 00:08:48,640
bad things they do, and so they look at someone else who's very confident, say, and

69
00:08:48,640 --> 00:09:02,320
who appears to be, who does bold acts of goodness or something, and they think, wow,

70
00:09:02,320 --> 00:09:14,800
that person is better than me, that person is better than me because of how confident they

71
00:09:14,800 --> 00:09:16,200
are or something like that.

72
00:09:16,200 --> 00:09:20,480
In fact, you might argue that while the confidence is actually potentially a detriment to

73
00:09:20,480 --> 00:09:25,920
that person, and maybe it's associated with ego, so they might actually not be better.

74
00:09:25,920 --> 00:09:32,920
On the other hand, a person can be a very good person and know it and can become conceited

75
00:09:32,920 --> 00:09:39,440
about it, and they look at people who are evil and mean and nasty and think, huh, I'm

76
00:09:39,440 --> 00:09:45,400
better than that person, they are better than them, it's still conceited.

77
00:09:45,400 --> 00:09:49,640
So what really makes a person greater lesser and equal is, in regards to their goodness,

78
00:09:49,640 --> 00:09:56,000
their purity, their good qualities of Buddhism, to someone regularly give rise to greater

79
00:09:56,000 --> 00:10:00,280
ones, you know, someone is so dependent, that's not going to go on again, even a so dependent

80
00:10:00,280 --> 00:10:03,280
can be conceited about it.

81
00:10:03,280 --> 00:10:16,960
Is it breaking up or anything?

82
00:10:16,960 --> 00:10:18,960
No, it's great.

83
00:10:18,960 --> 00:10:19,960
Okay, thank you.

84
00:10:19,960 --> 00:10:19,720
Okay, nine plus years ago, when I started meditating, my first teacher taught, standing,

85
00:10:19,720 --> 00:10:26,600
standing, but also, rupa stands, rupa sits, and so on, I never saw any fault in that.

86
00:10:26,600 --> 00:10:30,480
Now when the mind falls back into this old way of noting, it feels or seems like the mind

87
00:10:30,480 --> 00:10:34,600
is creating a separate idea or entity for the label rupa.

88
00:10:34,600 --> 00:10:38,800
What are your thoughts on noting, standing, standing versus rupa stands?

89
00:10:38,800 --> 00:10:45,280
Yes, of course, I've read your booklet, I'm puzzled because you're both Mahasti teachers.

90
00:10:45,280 --> 00:10:53,400
Mahasti Sayada seemed somewhat critical of that sort of, not exactly critical, but dismissed

91
00:10:53,400 --> 00:10:56,000
the idea that that was necessary.

92
00:10:56,000 --> 00:11:03,080
One of his books, one of the sort of well-known, you can read through it, I can find

93
00:11:03,080 --> 00:11:08,080
it for you if you like, discusses this and says, you know, the Buddha said, I go, in the

94
00:11:08,080 --> 00:11:17,960
fact, kachami, I stand, deep-dome Hee, so, and he talks about whether you need to actually

95
00:11:17,960 --> 00:11:25,480
talk about rupa or, say, air element, or rupa or that kind of thing.

96
00:11:25,480 --> 00:11:30,120
And I would agree, you know, even if he wasn't the Mahasti Sayada, I would still agree

97
00:11:30,120 --> 00:11:38,400
because it's somehow intellectualizing, you know, I mean, it's not something you have

98
00:11:38,400 --> 00:11:40,440
to remind yourself about.

99
00:11:40,440 --> 00:11:46,360
The rupa is the experience, the experiences of rupa, you know, when you say standing, you

100
00:11:46,360 --> 00:11:52,360
are aware of the rupa standing, but as soon as you say rupa, you're introducing some sort

101
00:11:52,360 --> 00:12:00,440
of theory in the sense of what is rupa or this being rupa or that kind of thing, it's

102
00:12:00,440 --> 00:12:02,120
not necessarily wrong.

103
00:12:02,120 --> 00:12:09,520
But I would bet that anyone who does that could have the potential for, especially if

104
00:12:09,520 --> 00:12:16,280
they know what rupa is, could have the potential for over intellectualizing, if you say

105
00:12:16,280 --> 00:12:20,920
you've done it and it works, and it worked for you, I mean, yeah, it probably does work

106
00:12:20,920 --> 00:12:21,920
fine.

107
00:12:21,920 --> 00:12:27,400
But I would recommend for that reason because you're adding, as you see, as you say, you're

108
00:12:27,400 --> 00:12:36,920
adding an extra concept and there's a potential for distraction over analyzing and intellectual

109
00:12:36,920 --> 00:12:37,920
izing that kind.

110
00:12:37,920 --> 00:12:44,120
And you want to keep it as simple as possible, standing as well, standing as I would

111
00:12:44,120 --> 00:12:52,480
rather do better, it seems that the Buddha is, you know, if you read this, it's Ditomhi, I'm

112
00:12:52,480 --> 00:12:55,520
standing, which, you know, people would say, oh, you can't say, I'm standing, but the

113
00:12:55,520 --> 00:12:56,520
Buddha didn't.

114
00:12:56,520 --> 00:13:01,520
So, it's just the simplest way to say standing as standing.

115
00:13:01,520 --> 00:13:09,440
Is it dangerous to practice other types of meditation while also practicing mindful meditation

116
00:13:09,440 --> 00:13:12,120
now that the same time?

117
00:13:12,120 --> 00:13:20,480
Well, you can look up my video on other meditation types, I think that's what it's called

118
00:13:20,480 --> 00:13:23,160
other meditation techniques or something.

119
00:13:23,160 --> 00:13:28,160
There are four protective meditations, I hopefully there's actually one that's labeled

120
00:13:28,160 --> 00:13:29,160
protective meditations.

121
00:13:29,160 --> 00:13:33,840
There's four types of meditation that actually support your insight practice, other types

122
00:13:33,840 --> 00:13:36,760
of meditation, probably not so much.

123
00:13:36,760 --> 00:13:42,680
So I would recommend sticking to those four if you wanted something that actually was

124
00:13:42,680 --> 00:13:48,360
going to be useful to augment your meditation practice, practice from time to time these

125
00:13:48,360 --> 00:13:50,880
other four meditations.

126
00:13:50,880 --> 00:13:56,360
They are, you know, look up the video, but they're quickly mindfulness of the Buddha,

127
00:13:56,360 --> 00:14:04,320
mindfulness of not in any particular mindfulness of load, someness of the body or the aspects

128
00:14:04,320 --> 00:14:09,640
of the body, loving kindness and mindfulness and death.

129
00:14:09,640 --> 00:14:17,280
So we could argue that like compassion goes along with loving kindness and moody time

130
00:14:17,280 --> 00:14:27,040
and good unity as well, but those four are the big ones.

131
00:14:27,040 --> 00:14:34,040
And I've read chapters one and two of your booklet and I'd like to understand what you think

132
00:14:34,040 --> 00:14:38,280
is the larger goal of why we practice the four foundations of mindfulness.

133
00:14:38,280 --> 00:14:43,200
Is it to develop a clearer awareness continuously with what we can use to investigate

134
00:14:43,200 --> 00:14:47,840
on phenomena and see the characteristics in meditation?

135
00:14:47,840 --> 00:14:54,200
You know, that sounds good and it's probably okay, but I wouldn't overthink it.

136
00:14:54,200 --> 00:15:01,040
The goal is just to, well, the goal state, you know, that that's the practice state

137
00:15:01,040 --> 00:15:09,080
is to just see things as they are, plain and simple, and thereby see impermanence on

138
00:15:09,080 --> 00:15:19,480
satisfactoryness and uncontrollability, see that things are unsaid, unstable, unsatisfying

139
00:15:19,480 --> 00:15:23,520
and uncontrollable, which you're going to see, you're going to be challenged by the

140
00:15:23,520 --> 00:15:24,800
practice.

141
00:15:24,800 --> 00:15:29,840
If you're doing it properly, it will be challenging and it will force you to reevaluate

142
00:15:29,840 --> 00:15:38,440
your attachments and it will force you to become more flexible as you see that your expectations

143
00:15:38,440 --> 00:15:46,000
and your rigidity of mind, your clinging to things is causing you suffering.

144
00:15:46,000 --> 00:15:52,160
I wouldn't over-intellectualize the goal because practically you're just going to experience

145
00:15:52,160 --> 00:15:58,600
so much that what you should be focusing on is dealing with and learning about what you

146
00:15:58,600 --> 00:16:01,920
experience.

147
00:16:01,920 --> 00:16:07,880
But yeah, I mean, that's basically what you're saying.

148
00:16:07,880 --> 00:16:08,880
So you're fine.

149
00:16:08,880 --> 00:16:14,120
I just, I guess I'm always wary of putting it out in words like that, but it sounds

150
00:16:14,120 --> 00:16:17,440
like actually you've got a good grasp on it.

151
00:16:17,440 --> 00:16:23,920
Sorry, this was I think another part of the original question, I find it's hard to keep

152
00:16:23,920 --> 00:16:27,960
up with the effort to pick up the four foundations of mindfulness throughout the day.

153
00:16:27,960 --> 00:16:31,760
I was hoping keeping the larger purpose, motivation in mind would help me.

154
00:16:31,760 --> 00:16:34,120
You see, that's what I was afraid of.

155
00:16:34,120 --> 00:16:36,800
You're trying to find a trick to make it easier.

156
00:16:36,800 --> 00:16:40,800
Anytime anyone says I find it hard, you've got to say good, it's supposed to be hard.

157
00:16:40,800 --> 00:16:41,800
It's a challenge.

158
00:16:41,800 --> 00:16:43,440
It's meant to challenge you.

159
00:16:43,440 --> 00:16:45,320
Don't look for a trick.

160
00:16:45,320 --> 00:16:47,520
Right with it, work on it.

161
00:16:47,520 --> 00:16:53,720
When it's hard, that's when the work is happening because you're seeing it's unpredictable.

162
00:16:53,720 --> 00:16:56,440
It's unsatisfying and it's uncontrollable.

163
00:16:56,440 --> 00:16:57,760
The practice is that way.

164
00:16:57,760 --> 00:17:00,960
That's what you're supposed to see.

165
00:17:00,960 --> 00:17:03,280
So yeah, work on it.

166
00:17:03,280 --> 00:17:04,920
There's no shortcut.

167
00:17:04,920 --> 00:17:10,240
Trying to find a trick that makes it, that helps you, it's not going to help you.

168
00:17:10,240 --> 00:17:14,640
Don't look for something to help you work on it.

169
00:17:14,640 --> 00:17:20,360
Be more patient, be more methodical.

170
00:17:20,360 --> 00:17:21,360
There's no trick.

171
00:17:21,360 --> 00:17:22,880
It's quite clear what you have to do.

172
00:17:22,880 --> 00:17:28,440
It's just, we don't like the fact that it's so difficult, so we try to find a way around

173
00:17:28,440 --> 00:17:30,160
it, a shortcut.

174
00:17:30,160 --> 00:17:31,160
Don't look for shortcuts.

175
00:17:31,160 --> 00:17:35,680
That in and of itself is, or don't look for crutches or supports, because that in and

176
00:17:35,680 --> 00:17:37,760
of itself is an unwholesome mind state.

177
00:17:37,760 --> 00:17:38,760
It's laziness.

178
00:17:38,760 --> 00:17:39,760
It's common.

179
00:17:39,760 --> 00:17:46,720
It's not a criticism, but something you have to be wary of, there's no shortcut.

180
00:17:46,720 --> 00:17:53,280
One day I have a question and I don't believe this isn't asking about a shortcut, but maybe

181
00:17:53,280 --> 00:17:56,880
it is phones with all their notifications.

182
00:17:56,880 --> 00:17:59,080
So distracting.

183
00:17:59,080 --> 00:18:07,600
When practicing daily meditation, you know, if I hear notifications, it's not simple like

184
00:18:07,600 --> 00:18:13,320
hearing hearing for other ordinary sounds, I start to get worried, anxious, somebody

185
00:18:13,320 --> 00:18:17,320
need me, do I need to answer that.

186
00:18:17,320 --> 00:18:19,440
So would it be a better practice too?

187
00:18:19,440 --> 00:18:25,440
No, all of the myriad of curiosity worried, anxious, or just turn off the phone before

188
00:18:25,440 --> 00:18:34,560
daily meditation.

189
00:18:34,560 --> 00:18:40,560
You'd want to more look at your level and practice for a beginner, much better to turn

190
00:18:40,560 --> 00:18:42,680
off the phone.

191
00:18:42,680 --> 00:18:46,640
When you're doing an intensive course, you don't need that distraction.

192
00:18:46,640 --> 00:18:48,960
You're going to find all the deep stuff anyway.

193
00:18:48,960 --> 00:18:55,080
But during daily life for a continuing practitioner, I would say, you're going to have to

194
00:18:55,080 --> 00:18:59,800
learn to deal with these things, and you're strong enough through your practice to recognize

195
00:18:59,800 --> 00:19:03,720
them and deal with them and to not get lost in them.

196
00:19:03,720 --> 00:19:11,240
So I would argue, to some extent, you would want to open yourself up to those.

197
00:19:11,240 --> 00:19:17,560
Now when you're really trying to be more intensive about your practice, I would just

198
00:19:17,560 --> 00:19:19,680
turn off the notifications.

199
00:19:19,680 --> 00:19:23,640
Because the other thing is, even without the worry and so on, there's an obligation,

200
00:19:23,640 --> 00:19:25,480
there's a duty.

201
00:19:25,480 --> 00:19:30,440
And if you make a determination to focus on the meditation, you're basically saying I'm

202
00:19:30,440 --> 00:19:37,920
not going to be in contact with people because that it is a, does diffuse my meditation

203
00:19:37,920 --> 00:19:38,920
practice.

204
00:19:38,920 --> 00:19:46,120
So I guess I would say when it's when you're doing intensive practice, better to turn

205
00:19:46,120 --> 00:19:47,120
it off.

206
00:19:47,120 --> 00:19:53,560
But unless you're a beginner, for someone who's new, then I would argue probably turning

207
00:19:53,560 --> 00:19:55,680
it off, turning them off as the best.

208
00:19:55,680 --> 00:20:01,080
But if it's not intensive practice, you might want to turn them on and learn to deal with

209
00:20:01,080 --> 00:20:02,080
the worry.

210
00:20:02,080 --> 00:20:11,280
That's pretty much what I do now, but it is challenging, which is good, it's supposed

211
00:20:11,280 --> 00:20:12,280
to be.

212
00:20:12,280 --> 00:20:13,280
It is.

213
00:20:13,280 --> 00:20:15,520
I recently had a major setback.

214
00:20:15,520 --> 00:20:20,640
I just recently got back from New Mexico, and on the way back I lost my temper and quite

215
00:20:20,640 --> 00:20:22,760
verbally, even yelling a few times.

216
00:20:22,760 --> 00:20:26,600
I felt all my progress and mindfulness was in vain.

217
00:20:26,600 --> 00:20:32,640
He said normal to have to start over or a feeling of starting over, I feel lousy about

218
00:20:32,640 --> 00:20:33,640
it.

219
00:20:33,640 --> 00:20:36,840
Yeah, I mean, you can't lose mindfulness.

220
00:20:36,840 --> 00:20:42,560
Mindfulness is not really an aggregate quality, so that's something that gets bigger over

221
00:20:42,560 --> 00:20:43,560
time.

222
00:20:43,560 --> 00:20:44,880
That's concentration.

223
00:20:44,880 --> 00:20:49,640
So, and that's more what meditators build up, and they build up concentration, the extent

224
00:20:49,640 --> 00:20:52,200
that they think they gain something.

225
00:20:52,200 --> 00:20:59,320
And then to see the mindfulness pop like a bubble, it's somewhat disappointing, because

226
00:20:59,320 --> 00:21:04,320
we have expectations and we think we've gained something real when we actually have it.

227
00:21:04,320 --> 00:21:11,440
But mindfulness will allow you to know that you're angry, and the fact that you're aware

228
00:21:11,440 --> 00:21:16,800
that you're angry, and that you're aware now that you feel lousy about it, all of that

229
00:21:16,800 --> 00:21:26,080
is it's a start, you know, being aware of these things is a sign of a mindful awareness,

230
00:21:26,080 --> 00:21:29,200
or a mindful quality to your mind.

231
00:21:29,200 --> 00:21:38,800
So, you know, progress will be, yeah, getting less angry about things.

232
00:21:38,800 --> 00:21:43,360
But in the beginning it's just more accepting that they're there, you know, and learning

233
00:21:43,360 --> 00:21:50,640
to identify them, rather than, and this is maybe where acceptance could come in, is that

234
00:21:50,640 --> 00:21:56,360
rather than pretending they're not there, or trying to wish them away, accepting that

235
00:21:56,360 --> 00:21:59,840
you've got a problem and that you've got to deal with it, it's sort of a temporary acceptance

236
00:21:59,840 --> 00:22:03,800
in the sense of accepting that you're going to have to deal with these things rather

237
00:22:03,800 --> 00:22:09,480
than just run away from them, you know, that they're still there, accepting the fact

238
00:22:09,480 --> 00:22:16,400
that you're not enlightening it, and you've got work to do.

239
00:22:16,400 --> 00:22:19,800
So I wouldn't feel discouraged about that at all.

240
00:22:19,800 --> 00:22:23,280
I would take it as an opportunity to deal with these emotions.

241
00:22:23,280 --> 00:22:33,160
When you feel bad about it, say, upset, upset, or disliking, disliking, sad, sad, angry, frustrated,

242
00:22:33,160 --> 00:22:34,160
whatever.

243
00:22:34,160 --> 00:22:37,800
Don't think of it as a setback, it was a mistake.

244
00:22:37,800 --> 00:22:43,640
It was a part of the problem, it was a product of the problem that you have inside, okay,

245
00:22:43,640 --> 00:22:52,040
well, we know what the problem is, it's like when you, when a plumber tries to find a

246
00:22:52,040 --> 00:22:55,920
drain, a clog in the drain, it's great when you find it, nah, now you've found the

247
00:22:55,920 --> 00:23:01,760
clog, or when a computer programmer finds a bug, they found the bug in the program, now

248
00:23:01,760 --> 00:23:04,160
they just have to fix it.

249
00:23:04,160 --> 00:23:20,240
So you've found the bug, and just have to fix it, and you're all caught up on questions

250
00:23:20,240 --> 00:23:21,240
Monday.

251
00:23:21,240 --> 00:23:30,920
All right, I'm all sorry for a brief, the brevity of tonight's show, but I'll be back tomorrow.

252
00:23:30,920 --> 00:23:37,880
Today I also went to Mississauga, gave a short talk there to a bunch of Christians, actually.

253
00:23:37,880 --> 00:23:43,880
He said, he took me aside before the talk, and he's got all serious and said, I want

254
00:23:43,880 --> 00:23:48,360
to tell you, these are mostly Christians, and I thought he was going to say, and I know

255
00:23:48,360 --> 00:23:55,000
you've been bashing Christians lately, but he doesn't know what he was going to tell

256
00:23:55,000 --> 00:24:01,440
you, you know, I would don't eat a bash, because I'm not very friendly towards Christianity,

257
00:24:01,440 --> 00:24:07,000
you know, I can because it was good, I think they were happy with it, I know.

258
00:24:07,000 --> 00:24:11,440
It was interesting because they're Christian, but the woman who sort of organized it,

259
00:24:11,440 --> 00:24:21,080
her husband and her daughter died in a car crash, and she was just, of course, devastated,

260
00:24:21,080 --> 00:24:26,160
and she came to the monastery there, and it was the monks there who helped her deal

261
00:24:26,160 --> 00:24:34,520
with her sadness, her loss three years ago, and so she actually is very much impressed

262
00:24:34,520 --> 00:24:42,600
by Buddhism, and I think she was actually somewhat critical of, she sort of left her Christian

263
00:24:42,600 --> 00:24:48,920
faith, because it didn't really help her, and it certainly couldn't explain why they

264
00:24:48,920 --> 00:25:03,600
had to die, besides it's all part of some mythical beings planned for us, it'd be a shame

265
00:25:03,600 --> 00:25:17,240
to have to be stuck on that kind of meaningless belief, all right, anyway, and on that note,

266
00:25:17,240 --> 00:25:21,520
have a good night, everyone, thank you, Robin, for joining me, as usual, thank you,

267
00:25:21,520 --> 00:25:49,040
and I'll see you next time, bye.

