1
00:00:00,000 --> 00:00:04,240
In a little confused about how much should I donate after a retreat?

2
00:00:04,240 --> 00:00:06,880
Nothing. You shouldn't donate anything.

3
00:00:06,880 --> 00:00:11,440
Your donation is the retreat. That is your donation.

4
00:00:11,440 --> 00:00:15,520
You shouldn't donate after a retreat. Now in certain cases,

5
00:00:15,520 --> 00:00:18,480
you have to feel pity on the organization

6
00:00:18,480 --> 00:00:22,400
and kind of feel

7
00:00:22,400 --> 00:00:27,040
concerned for the organization that

8
00:00:27,040 --> 00:00:32,720
all of us have come and used up a lot of the resources of the organization.

9
00:00:32,720 --> 00:00:39,200
So if we don't support it, first of all, it's kind of

10
00:00:42,560 --> 00:00:49,760
that's kind of crass, I suppose, but it's a problem for the organization.

11
00:00:49,760 --> 00:00:53,760
But I don't think that should be a hard and fast rule and personally I don't

12
00:00:53,760 --> 00:00:58,480
subscribe to such theory

13
00:00:58,480 --> 00:01:02,480
that of reimbursing or of

14
00:01:02,480 --> 00:01:06,240
even paying for any expenses in the course.

15
00:01:06,240 --> 00:01:09,200
Now in certain cases, that's the only way possible.

16
00:01:09,200 --> 00:01:13,040
Now for example, you might get a group of people together and say okay,

17
00:01:13,040 --> 00:01:20,160
let's pitch in to rent this place and so on and so on and so on.

18
00:01:20,160 --> 00:01:26,320
But this is just to cover the need.

19
00:01:26,320 --> 00:01:29,600
I wrote a long blog post and

20
00:01:29,600 --> 00:01:32,160
I didn't get many comments so I get the feeling that it was just too

21
00:01:32,160 --> 00:01:34,400
wordy but

22
00:01:34,400 --> 00:01:38,240
I still would recommend you to read that blog post if you want my thoughts on it.

23
00:01:38,240 --> 00:01:40,400
It was on, it was called

24
00:01:40,400 --> 00:01:43,600
there is too such thing as a free lunch

25
00:01:43,600 --> 00:01:47,680
and the gist of it is in case it really is too wordy

26
00:01:47,680 --> 00:01:52,800
is that we shouldn't be trying to reimburse

27
00:01:52,800 --> 00:01:55,760
and we shouldn't be trying to pay it forward. We shouldn't be trying to pay it

28
00:01:55,760 --> 00:01:58,480
back, we shouldn't be trying to pay it forward.

29
00:01:58,480 --> 00:02:04,080
We should be trying to pay for it.

30
00:02:04,080 --> 00:02:09,280
When there is a need we should provide resources

31
00:02:09,280 --> 00:02:13,440
as we're able to fulfill that need. When there's no need

32
00:02:13,440 --> 00:02:17,360
we should be willing to take the free lunch.

33
00:02:17,360 --> 00:02:23,440
We should feel no remorse for taking the free lunch.

34
00:02:23,440 --> 00:02:31,120
If everyone were to believe in the philosophy of free lunches

35
00:02:31,120 --> 00:02:34,160
then everyone would have free lunches because

36
00:02:34,160 --> 00:02:37,600
it's not taking, it's not the philosophy that the

37
00:02:37,600 --> 00:02:40,800
philosophy that you can take whatever you want. It's the philosophy

38
00:02:40,800 --> 00:02:46,160
of providing free lunches. When someone needs something

39
00:02:46,160 --> 00:02:50,480
you give it to them and when you need something

40
00:02:50,480 --> 00:02:57,680
you take it. When you come to the meditation course

41
00:02:57,680 --> 00:03:01,920
you're not looking to support the organization,

42
00:03:01,920 --> 00:03:06,240
you're looking for something, you need something, you need meditation.

43
00:03:06,240 --> 00:03:13,600
So the interaction, the transaction had nothing to do with

44
00:03:13,600 --> 00:03:18,560
the organization's need. Now when the organization

45
00:03:18,560 --> 00:03:21,680
needs things, when you see that there's a retreat going on,

46
00:03:21,680 --> 00:03:24,880
regardless of whether you're going to join the retreat

47
00:03:24,880 --> 00:03:28,560
you should consider does the organization need resources to

48
00:03:28,560 --> 00:03:31,920
to do this. So even if you're not participating in the retreat

49
00:03:31,920 --> 00:03:35,680
before you even think of participating you should consider

50
00:03:35,680 --> 00:03:39,600
what resources are needed for this organization

51
00:03:39,600 --> 00:03:43,200
if you believe in the organization. It should have nothing to do with what

52
00:03:43,200 --> 00:03:45,840
you take. What you take is because you need

53
00:03:45,840 --> 00:03:49,520
what you give is because they need. If they don't need you don't have to

54
00:03:49,520 --> 00:03:53,200
give. Now this doesn't apply for most organizations, most

55
00:03:53,200 --> 00:03:58,400
organizations don't subscribe to such a philosophy and therefore

56
00:03:58,400 --> 00:04:03,840
quite insistent or hinting anyway at the need to donate

57
00:04:03,840 --> 00:04:08,960
and for every participant to donate. And there are even many

58
00:04:08,960 --> 00:04:14,080
monastic organizations that talk about this

59
00:04:14,080 --> 00:04:20,320
repaying your debt to the Sangha because if you take something from

60
00:04:20,320 --> 00:04:23,520
the monastery, if you use something in the monastery

61
00:04:23,520 --> 00:04:27,360
you're in debt to the monastery. This is the concept. Now

62
00:04:27,360 --> 00:04:30,720
this is totally, goes against with the Buddha,

63
00:04:30,720 --> 00:04:35,760
how the Buddha explained the allocation of resources. The Buddha explained

64
00:04:35,760 --> 00:04:42,480
four types of people who use resources. The first person is a person who

65
00:04:42,480 --> 00:04:46,320
steals them. The second person is a person who

66
00:04:46,320 --> 00:04:52,480
loans them or borrows them. The third person is a person who

67
00:04:52,480 --> 00:04:59,920
inherits them and the fourth person is the person who deserves them.

68
00:04:59,920 --> 00:05:06,320
Excuse me. A person who steals resources is a person who is

69
00:05:06,320 --> 00:05:13,600
their under false pretenses or false

70
00:05:13,600 --> 00:05:19,440
medical. It is lying about it. So someone who goes to practice but is just

71
00:05:19,440 --> 00:05:24,960
looking to pick up women or something. Pick up a

72
00:05:24,960 --> 00:05:32,080
sort of romantic engagement, for example. No, that's not true.

73
00:05:32,080 --> 00:05:37,520
It has to be worse than that. It has to be specifically

74
00:05:37,520 --> 00:05:46,880
deceptive. So suppose, I don't know what an example in a meditation course would be.

75
00:05:46,880 --> 00:05:52,800
Like an undercover, suppose a person from another religion

76
00:05:52,800 --> 00:05:59,120
were to come and just to spy. So they pretend to take the meditation course but

77
00:05:59,120 --> 00:06:01,440
they're really there just to spy and to

78
00:06:01,440 --> 00:06:06,400
to cause problems. This is a person who steals the resources.

79
00:06:06,400 --> 00:06:10,000
So the Buddha example, the Buddha gave us as a monk, a person who

80
00:06:10,000 --> 00:06:13,040
pretends to be a monk and goes on arms round and people say, oh there's a

81
00:06:13,040 --> 00:06:16,480
Buddhist monk coming and let's give them food but they're not

82
00:06:16,480 --> 00:06:20,400
really a Buddhist monk. That's the person who steals.

83
00:06:20,400 --> 00:06:23,760
A person who borrows is a person who is really there. They sign up for a

84
00:06:23,760 --> 00:06:27,760
meditation course or they really ordain as a monk but they don't practice.

85
00:06:27,760 --> 00:06:30,800
So they sleep all day in the meditation course or they

86
00:06:30,800 --> 00:06:40,240
goof off or they master baders and I'll have to say that. I don't know.

87
00:06:40,240 --> 00:06:44,160
That's true. The reason I think of it is because I had a student once who

88
00:06:44,160 --> 00:06:47,840
admitted it to me. He came and admitted to having

89
00:06:47,840 --> 00:06:51,280
masturbated during the course but sorry that's a bit crude.

90
00:06:51,280 --> 00:06:55,440
I'll try to be a little bit more a little bit less crude.

91
00:06:55,440 --> 00:06:58,800
But somehow you know it's it's nice to bring up things that

92
00:06:58,800 --> 00:07:02,640
people are are not willing to talk about especially that one because it seems

93
00:07:02,640 --> 00:07:07,280
to be such a pervasive pervasive

94
00:07:07,280 --> 00:07:11,200
such a big problem for people anyway.

95
00:07:11,200 --> 00:07:16,640
So they do things that are not appropriate. This is a person who is

96
00:07:16,640 --> 00:07:19,600
let's not let's not say do things that are inappropriate but who doesn't do

97
00:07:19,600 --> 00:07:23,280
things that are necessary. This is a person who's borrowing

98
00:07:23,280 --> 00:07:27,280
their resources and they are truly in debt when they leave.

99
00:07:27,280 --> 00:07:31,520
They because they've taken all these things without without any

100
00:07:31,520 --> 00:07:38,800
reason for deserving them. The third type of person who inherits them

101
00:07:38,800 --> 00:07:42,080
is a person who actually tries to practice.

102
00:07:42,080 --> 00:07:47,040
Who may not be enlightened or may not have achieved enlightened but

103
00:07:47,040 --> 00:07:50,240
who undertakes the meditation course

104
00:07:50,240 --> 00:07:55,520
to practice to develop themselves to practice the Buddhist teacher.

105
00:07:55,520 --> 00:07:59,040
This the Buddha said is someone who inherits the resources.

106
00:07:59,040 --> 00:08:06,320
Why? Because those resources were given to the Buddha or given to the

107
00:08:06,320 --> 00:08:10,880
organization because of by people who believe in what the organization is

108
00:08:10,880 --> 00:08:14,560
doing. Believe in the practice. Now these people are undertaking the

109
00:08:14,560 --> 00:08:20,000
practice and are in line with the organizations

110
00:08:20,000 --> 00:08:31,600
goals and goals and ideals.

111
00:08:31,600 --> 00:08:36,880
So these people are inheriting it based on the worthiness

112
00:08:36,880 --> 00:08:40,560
or in people's minds of the organization. So not just talking about Buddhism

113
00:08:40,560 --> 00:08:46,560
but the organization in general. The fourth type of person

114
00:08:46,560 --> 00:08:50,240
are those who deserve them. Are those who have practiced

115
00:08:50,240 --> 00:08:53,920
and are enlightened in the case of Buddhism or who have

116
00:08:53,920 --> 00:08:58,560
developed, realized the goals of the organization. So they have practiced

117
00:08:58,560 --> 00:09:03,360
the meditation and become enlightened and are therefore deserving of whatever

118
00:09:03,360 --> 00:09:06,240
people give them because their mind is pure because

119
00:09:06,240 --> 00:09:11,920
they have attained the goals of the organization. They are therefore

120
00:09:11,920 --> 00:09:16,160
equivalent to the organization. In this case equivalent to the Buddha

121
00:09:16,160 --> 00:09:18,560
and therefore people who have faith in the Buddha

122
00:09:18,560 --> 00:09:21,440
and give to the monks without a faith in the Buddha

123
00:09:21,440 --> 00:09:28,160
are basically giving to the Buddha because they're giving to someone who is

124
00:09:28,160 --> 00:09:31,120
in that sense, in one sense, equivalent to the Buddha

125
00:09:31,120 --> 00:09:36,240
in the sense of being free from suffering. So that's the Buddha's teaching

126
00:09:36,240 --> 00:09:41,040
which sort of puts to rest this idea of being

127
00:09:41,040 --> 00:09:44,640
in debt. If you're actually practicing meditation during the course you're not in

128
00:09:44,640 --> 00:09:47,600
debt to the organization. If you're putting in

129
00:09:47,600 --> 00:09:54,400
to practice their teachings then and honestly

130
00:09:54,400 --> 00:10:04,400
and wholeheartedly then you're not in debt to the organization.

