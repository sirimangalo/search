1
00:00:00,000 --> 00:00:02,560
And I will come back to ask a monk.

2
00:00:02,560 --> 00:00:06,520
Next question comes from Scott, who asks,

3
00:00:06,520 --> 00:00:10,080
would you mind telling me what the precise holidays

4
00:00:10,080 --> 00:00:12,480
in Theravada tradition are?

5
00:00:12,480 --> 00:00:18,040
I know Uposita, Makabuja, Weisak, Damadeh,

6
00:00:18,040 --> 00:00:20,640
Pavadhanadeh, and Anapana Satideh.

7
00:00:20,640 --> 00:00:21,880
Am I missing others?

8
00:00:25,920 --> 00:00:29,080
But first of all, I wouldn't worry too much about it,

9
00:00:29,080 --> 00:00:32,320
because obviously every day that you're mindful

10
00:00:32,320 --> 00:00:36,520
that you're practicing the dhamma is a holiday, a holy day.

11
00:00:36,520 --> 00:00:43,280
It's only, the Buddhist holidays are only a way of marking time

12
00:00:43,280 --> 00:00:50,600
so that every so often we can get together and remember

13
00:00:50,600 --> 00:00:56,440
and be mindful of the Buddhist teaching and practice intensively.

14
00:00:56,440 --> 00:01:00,960
So the first Buddhist holidays, the Uposita day.

15
00:01:00,960 --> 00:01:03,520
And in regards to the Uposita day,

16
00:01:03,520 --> 00:01:07,280
I think this is a clear example of how we shouldn't be

17
00:01:07,280 --> 00:01:11,240
to caught up on the idea of specific holidays.

18
00:01:11,240 --> 00:01:16,480
Because the Uposita days are the lunar cycle,

19
00:01:16,480 --> 00:01:20,360
the full moon, the empty moon, the half moon.

20
00:01:20,360 --> 00:01:25,000
These were in India considered special days.

21
00:01:25,000 --> 00:01:27,440
So this is how they mark time.

22
00:01:27,440 --> 00:01:31,560
Nowadays, most people in the world have a different system.

23
00:01:31,560 --> 00:01:37,360
Our system is the seven day week, the 12 month year,

24
00:01:37,360 --> 00:01:41,960
which has nothing to do with the Buddhist teaching.

25
00:01:41,960 --> 00:01:45,080
Nothing to do with, there's nothing in the Buddhist teaching

26
00:01:45,080 --> 00:01:46,240
about it.

27
00:01:46,240 --> 00:01:49,960
There's no mention of how we should deal with this

28
00:01:49,960 --> 00:01:51,640
because it's something that developed much later.

29
00:01:51,640 --> 00:01:54,240
It's a very artificial system as well.

30
00:01:54,240 --> 00:01:56,560
It's even more artificial because these months just sort

31
00:01:56,560 --> 00:01:58,200
of sprung up and adding months,

32
00:01:58,200 --> 00:02:01,680
subtracting months and fitting the weeks in there

33
00:02:01,680 --> 00:02:04,280
sort of haphazardly.

34
00:02:04,280 --> 00:02:06,640
It's really a fairly chaotic and useless system,

35
00:02:06,640 --> 00:02:09,360
but it happens to be the system in use

36
00:02:09,360 --> 00:02:12,640
and it happens to have a great pull on our lives.

37
00:02:12,640 --> 00:02:16,200
So what's the answer?

38
00:02:16,200 --> 00:02:20,320
Because we have a problem here.

39
00:02:20,320 --> 00:02:27,200
These holidays sometimes often times arrive on weekdays

40
00:02:27,200 --> 00:02:28,520
when most people are working.

41
00:02:28,520 --> 00:02:31,440
So even here in Buddhist countries, it's very difficult

42
00:02:31,440 --> 00:02:33,440
for people to come for the Buddhist holidays,

43
00:02:33,440 --> 00:02:36,560
the full moon day, which is very important here in Sri Lanka.

44
00:02:36,560 --> 00:02:38,840
Many people can't observe it.

45
00:02:38,840 --> 00:02:43,280
And so what's to be done, I think the key is what's

46
00:02:43,280 --> 00:02:46,400
been done in non-Buddhist countries

47
00:02:46,400 --> 00:02:51,040
where they'll have the holiday on a weekend, of course,

48
00:02:51,040 --> 00:02:52,240
the Saturday or Sunday.

49
00:02:52,240 --> 00:02:57,720
And even in non-sectarian or non-religious Buddhist circles,

50
00:02:57,720 --> 00:02:59,240
they'll have days of mindfulness.

51
00:02:59,240 --> 00:03:02,120
And of course, they do it on a Saturday or a Sunday.

52
00:03:02,120 --> 00:03:03,640
They might even have a weekend course

53
00:03:03,640 --> 00:03:06,240
where they do Friday, Saturday, Sunday.

54
00:03:06,240 --> 00:03:10,480
And I think that is crucial in continuing

55
00:03:10,480 --> 00:03:11,160
of Buddhist teaching.

56
00:03:11,160 --> 00:03:15,520
It's never going to survive this tradition of lay people

57
00:03:15,520 --> 00:03:19,920
coming to the temple, the monastery, excuse me,

58
00:03:19,920 --> 00:03:23,760
on a full moon day.

59
00:03:23,760 --> 00:03:28,400
Now, that being said, these are important from a monastic point.

60
00:03:28,400 --> 00:03:30,720
Obviously, we have no such restrictions.

61
00:03:30,720 --> 00:03:34,680
So we do observe the full moon, the empty moon,

62
00:03:34,680 --> 00:03:38,120
as our religious days where we're to get together

63
00:03:38,120 --> 00:03:44,840
and to recite the rules of the monastic community.

64
00:03:44,840 --> 00:03:51,440
And maybe listen to Dhamma teaching and discussion and so on.

65
00:03:51,440 --> 00:03:54,320
But for lay people, I think it's much more profitable

66
00:03:54,320 --> 00:03:57,920
to fit it in with their work cycle.

67
00:03:57,920 --> 00:04:01,280
Every Sunday people should come to the monastery

68
00:04:01,280 --> 00:04:03,960
or Saturday or whatever you want to choose.

69
00:04:03,960 --> 00:04:09,240
Both Saturday and Sunday, I know in Western countries

70
00:04:09,240 --> 00:04:10,560
they'll do this.

71
00:04:10,560 --> 00:04:14,880
And this Sri Lankan monasteries will have Sunday school,

72
00:04:14,880 --> 00:04:17,160
Buddhist Sunday school, which is great.

73
00:04:17,160 --> 00:04:19,440
That's crucial.

74
00:04:19,440 --> 00:04:21,800
Here in Sri Lanka, they do have a Saturday Dhamma school.

75
00:04:21,800 --> 00:04:24,600
I go to teach at it sometimes.

76
00:04:24,600 --> 00:04:30,880
But they still are doing the poia day, which is fine.

77
00:04:30,880 --> 00:04:33,400
But I think it's not the most important.

78
00:04:33,400 --> 00:04:35,440
It was not a Buddhist concept.

79
00:04:35,440 --> 00:04:38,120
It was something that the Buddha used.

80
00:04:38,120 --> 00:04:43,160
Because that was the tradition.

81
00:04:43,160 --> 00:04:51,040
So that's the first and foremost is this weekly uposita,

82
00:04:51,040 --> 00:04:53,160
which we should adopt.

83
00:04:53,160 --> 00:04:57,240
But I think we should adopt it in a way that's suitable for us.

84
00:04:57,240 --> 00:05:00,080
Every Saturday you can pick every Sunday

85
00:05:00,080 --> 00:05:02,840
where you are going to keep the uposita seala, the eight

86
00:05:02,840 --> 00:05:08,080
precepts, you know, celibacy, not eating in the afternoon.

87
00:05:08,080 --> 00:05:15,440
Eating only in the morning, not listening to entertainment,

88
00:05:15,440 --> 00:05:20,480
not indulging in entertainment or beautification

89
00:05:20,480 --> 00:05:25,680
and sleeping on the floor, not sleeping in a bed.

90
00:05:25,680 --> 00:05:26,960
Do that on a Saturday.

91
00:05:26,960 --> 00:05:29,080
Do that for 24 hours, start Friday night.

92
00:05:29,080 --> 00:05:35,400
And Saturday night or start Saturday morning before dawn

93
00:05:35,400 --> 00:05:40,160
and finish Sunday morning after dawn.

94
00:05:40,160 --> 00:05:43,320
So you can be sure that you have 24 hours.

95
00:05:43,320 --> 00:05:46,800
You have a full day.

96
00:05:46,800 --> 00:05:49,400
But as far as the tradition goes,

97
00:05:49,400 --> 00:05:52,560
if you want to know about the holidays,

98
00:05:52,560 --> 00:05:55,400
the ones that are generally agreed upon are three.

99
00:05:55,400 --> 00:05:58,280
The Visaka Pujada, the Fumuna Visaka,

100
00:05:58,280 --> 00:05:59,560
which is called Visaka.

101
00:05:59,560 --> 00:06:06,600
But in Pali, is Visaka, the Fumuna of Asalahau,

102
00:06:06,600 --> 00:06:08,720
which you might be referring to by Damade,

103
00:06:08,720 --> 00:06:11,800
it's when the Buddha first taught the Fumuna of Asalahah

104
00:06:11,800 --> 00:06:15,760
and the Fumuna of Magha, which is the day

105
00:06:15,760 --> 00:06:18,760
when the Buddha apparently gave the Awadapati Moka.

106
00:06:18,760 --> 00:06:22,680
So these three days, Visaka Buddha is the day

107
00:06:22,680 --> 00:06:26,000
when the Buddha was born.

108
00:06:26,000 --> 00:06:27,080
The Bodhisattva was born.

109
00:06:27,080 --> 00:06:28,600
The Buddha became enlightened.

110
00:06:28,600 --> 00:06:31,280
Bodhisattva became the Buddha and became enlightened.

111
00:06:31,280 --> 00:06:36,920
And the day that he passed away into Parinibhana,

112
00:06:36,920 --> 00:06:40,840
Asalah has when he first taught the Dhamma Chakapavatana,

113
00:06:40,840 --> 00:06:43,560
so when he gave his first discourse

114
00:06:43,560 --> 00:06:49,480
and someone who first became a Sotapana and enlightened

115
00:06:49,480 --> 00:06:51,880
being or came to understand the Dhamma

116
00:06:51,880 --> 00:06:55,360
even on a basic level.

117
00:06:55,360 --> 00:06:59,480
And Asalahabuja, the Awadapati Moka, which is the core

118
00:06:59,480 --> 00:07:04,120
of the Buddha's teaching that the refraining from bad deans

119
00:07:04,120 --> 00:07:07,280
in the development of good deeds and the purification of mind.

120
00:07:07,280 --> 00:07:10,120
This is the teaching of all the Buddhas.

121
00:07:10,120 --> 00:07:17,120
This was with the Buddha taught as the basic exhortation,

122
00:07:17,120 --> 00:07:21,320
the teaching that we should keep in mind as sort of fundamental.

123
00:07:21,320 --> 00:07:22,840
And it's a little bit longer than that,

124
00:07:22,840 --> 00:07:25,880
but it's worth looking up.

125
00:07:25,880 --> 00:07:29,520
Other holidays, like Pawarana Day and Anapana Sati Day,

126
00:07:29,520 --> 00:07:34,840
I think they're more local and changes how they're observed,

127
00:07:34,840 --> 00:07:36,360
whether they're observed.

128
00:07:36,360 --> 00:07:40,760
There's Intai, there's a Devo Rohana, which is the day

129
00:07:40,760 --> 00:07:45,960
when the Buddha came down from heaven here in Sri Lanka.

130
00:07:45,960 --> 00:07:50,120
There's the day when Buddhism first came to Sri Lanka,

131
00:07:50,120 --> 00:07:52,800
which is of course only observed here in Sri Lanka.

132
00:07:52,800 --> 00:07:57,400
But I think these are cultural, and it's often quite disappointing

133
00:07:57,400 --> 00:07:58,840
to see how they're carried out.

134
00:07:58,840 --> 00:08:03,040
That often there's great pomp and celebration and music

135
00:08:03,040 --> 00:08:06,080
and many things that are actually contrary

136
00:08:06,080 --> 00:08:13,280
to the Buddhist teaching of introspection and sobriety

137
00:08:13,280 --> 00:08:18,800
and the scientific investigation of reality.

138
00:08:18,800 --> 00:08:23,760
So I wouldn't concern too much about the holidays.

139
00:08:23,760 --> 00:08:29,560
It's good to know the three so that you can use them

140
00:08:29,560 --> 00:08:36,080
as a yearly opportunity to think about the Buddha.

141
00:08:36,080 --> 00:08:38,480
But I would say it's much more important

142
00:08:38,480 --> 00:08:40,960
that we practice daily, the Buddha's teaching,

143
00:08:40,960 --> 00:08:47,240
and at least weekly that we have some sort of a special day

144
00:08:47,240 --> 00:08:53,280
where we undertake the precepts on a more fundamental level.

145
00:08:53,280 --> 00:08:54,040
So I hope that helps.

146
00:08:54,040 --> 00:08:55,920
I'm not fully able to answer your question.

147
00:08:55,920 --> 00:08:57,240
I'm not able to give a list.

148
00:08:57,240 --> 00:08:58,840
I don't think such a list exists.

149
00:08:58,840 --> 00:09:01,760
But for each country, they do have their list.

150
00:09:01,760 --> 00:09:05,640
It's just not high on my, you know,

151
00:09:05,640 --> 00:09:08,720
it's not something that I keep in my memory,

152
00:09:08,720 --> 00:09:11,960
except for the major ones, which I think you've covered.

153
00:09:11,960 --> 00:09:18,960
So I hope that helps for the best.

