1
00:00:00,000 --> 00:00:04,500
There's this person who really annoys or even anchors me sometimes.

2
00:00:04,500 --> 00:00:11,000
What bothers me about this is to know that it's not him, but I who is causing myself to suffer.

3
00:00:11,000 --> 00:00:12,500
But why can't I let it go?

4
00:00:12,500 --> 00:00:16,500
Why am I attached to suffering?

5
00:00:16,500 --> 00:00:20,000
It's the question, right?

6
00:00:20,000 --> 00:00:24,800
That's the curious truth.

7
00:00:24,800 --> 00:00:26,200
It's a profound question.

8
00:00:26,200 --> 00:00:30,200
It's not something that normally people ask, why do I suffer?

9
00:00:30,200 --> 00:00:35,200
And they say, well, you're making me suffer, and that's the problem.

10
00:00:35,200 --> 00:00:43,200
The curious thing about suffering is there are different levels to our understanding of it,

11
00:00:43,200 --> 00:00:44,200
which I've talked about before.

12
00:00:44,200 --> 00:00:46,200
I'm going to go into it here.

13
00:00:46,200 --> 00:00:50,200
I think talking about suffering would probably give you some background,

14
00:00:50,200 --> 00:00:55,200
better background to answer this question.

15
00:00:55,200 --> 00:01:06,200
I think briefly I can say before I explain the means, the simple answer to your question is because that's not the practice.

16
00:01:06,200 --> 00:01:10,200
The practice is not to let go of suffering.

17
00:01:10,200 --> 00:01:17,200
The practice is to observe reality.

18
00:01:17,200 --> 00:01:27,200
And the result of observing reality closely, objectively, clearly is to understand reality for what it is.

19
00:01:27,200 --> 00:01:31,200
The result of understanding reality for what it is is letting go.

20
00:01:31,200 --> 00:01:34,200
So it's a program.

21
00:01:34,200 --> 00:01:41,200
So most people understand suffering to be a feeling that you get from time to time.

22
00:01:41,200 --> 00:01:47,200
And so they try to find a way to avoid the feeling.

23
00:01:47,200 --> 00:01:50,200
The understanding of suffering is that it's specific.

24
00:01:50,200 --> 00:01:54,200
This or that experience causes suffering.

25
00:01:54,200 --> 00:02:00,200
And so their means of escaping suffering is to run away from the experience, to chase it away,

26
00:02:00,200 --> 00:02:10,200
to blame other people, blame external events, and secure circumstances, entities outside of themselves.

27
00:02:10,200 --> 00:02:19,200
Or in, of course, in their own bodies, even in their own minds, blame those things and try their best to avoid them.

28
00:02:19,200 --> 00:02:22,200
This is how ordinary people deal with suffering.

29
00:02:22,200 --> 00:02:26,200
You have a headache, take a pill, you have a backache, get a massage,

30
00:02:26,200 --> 00:02:34,200
you have a stomachache, take some, and add acid.

31
00:02:34,200 --> 00:02:44,200
Eventually, a spiritual person with a little bit of wisdom, at least, will see that this isn't effective.

32
00:02:44,200 --> 00:02:51,200
This isn't via a viable solution to the problem.

33
00:02:51,200 --> 00:02:55,200
Eventually, there are certain sufferings that you can't run away from.

34
00:02:55,200 --> 00:03:01,200
You can't avoid, you can't be free from simply by chasing away.

35
00:03:01,200 --> 00:03:09,200
It's old age sickness, death, and even extreme trauma.

36
00:03:09,200 --> 00:03:29,200
The loss of a loved one, loss of wealth, loss of health, loss, or sadness, or any kind of great suffering,

37
00:03:29,200 --> 00:03:37,200
will make a person realize that this is an viable solution that these states are suffering,

38
00:03:37,200 --> 00:03:39,200
you can't avoid.

39
00:03:39,200 --> 00:03:43,200
There has to be a better way to be able to actually deal with the suffering.

40
00:03:43,200 --> 00:03:46,200
This is what often leads people to the spiritual life.

41
00:03:46,200 --> 00:03:51,200
For us, this leads us to come and meditate.

42
00:03:51,200 --> 00:04:05,200
The third understanding of suffering is a quality inherent in reality.

43
00:04:05,200 --> 00:04:19,200
In the same way that fire is hot, experience is a cause for suffering.

44
00:04:19,200 --> 00:04:25,200
We don't really have a, I can't think of a word for it, but it's like static electricity.

45
00:04:25,200 --> 00:04:27,200
It has, or potential energy.

46
00:04:27,200 --> 00:04:30,200
There's no energy in it, but it has the potential.

47
00:04:30,200 --> 00:04:34,200
A rock that's up high doesn't seem to have any energy.

48
00:04:34,200 --> 00:04:36,200
It's not moving, but it has potential energy.

49
00:04:36,200 --> 00:04:45,200
Because if you push it, it exhibits extreme amounts of energy as it falls to the earth below.

50
00:04:45,200 --> 00:04:51,200
So reality has this kind of static electricity or this potential for causing suffering.

51
00:04:51,200 --> 00:04:56,200
So we say fire is hot, but it's not burning you.

52
00:04:56,200 --> 00:05:04,200
When we say that experience is suffering, we don't mean that it hurts you.

53
00:05:04,200 --> 00:05:06,200
It has the potential to hurt you.

54
00:05:06,200 --> 00:05:16,200
Not which causes that suffering is craving, so the cause of all of our suffering is craving.

55
00:05:16,200 --> 00:05:22,200
So the third is this realization, this observation that leads you to see that the reason

56
00:05:22,200 --> 00:05:32,200
that you suffer is not really because you experience that this or that, but it's because,

57
00:05:32,200 --> 00:05:37,200
well, the third stage is it's because nothing can satisfy you.

58
00:05:37,200 --> 00:05:45,200
It's because, or it's the realization that all of reality is contributing to your suffering,

59
00:05:45,200 --> 00:05:50,200
because your partial to certain things and partial against certain things.

60
00:05:50,200 --> 00:05:58,200
It's the realization that that's not correct, that's not, that's to do simply to ignorance.

61
00:05:58,200 --> 00:06:06,200
And it's the realization that all, really all experience is the same, that pain is really the same as pleasure.

62
00:06:06,200 --> 00:06:10,200
Good memories are the same as bad memories, they're still just memories.

63
00:06:10,200 --> 00:06:17,200
It's really only our judgment and our incorrect judgment that points to certain things as being stable,

64
00:06:17,200 --> 00:06:24,200
as being pleasant, as being controllable, and therefore leading to expectations and ultimately disappointment,

65
00:06:24,200 --> 00:06:29,200
we don't get what we want.

66
00:06:29,200 --> 00:06:38,200
That's the third, which leads ultimately to a realization that the truth of suffering,

67
00:06:38,200 --> 00:06:40,200
and that's what leads you to let go.

68
00:06:40,200 --> 00:06:46,200
So the third and the fourth are kind of like one and the same, it's the path and then the goal.

69
00:06:46,200 --> 00:06:52,200
When you finally get it, the fourth one is this moment, this sort of gestalt, I guess.

70
00:06:52,200 --> 00:06:59,200
So there's epiphany that you have when you realize that nothing is worth clinging to.

71
00:06:59,200 --> 00:07:02,200
When you realize that everything is like this.

72
00:07:02,200 --> 00:07:07,200
So the third is the practice of observing and investigating,

73
00:07:07,200 --> 00:07:16,200
and seeing that the things that you thought were pleasant and the cause for happiness and contentment are actually making you more discontent,

74
00:07:16,200 --> 00:07:22,200
more unhappy, more unpleasant, more bitter, and mean as a person.

75
00:07:22,200 --> 00:07:29,200
And the fourth one is this final realization that absolutely everything is not worth clinging to.

76
00:07:29,200 --> 00:07:36,200
And so that's the moment where the mind lets go and that's the realization of the freedom from suffering,

77
00:07:36,200 --> 00:07:39,200
which pick on their banner.

78
00:07:39,200 --> 00:07:45,200
So why you can't let go, I would say, if I was going to be kind of smart me about it,

79
00:07:45,200 --> 00:07:48,200
I would say because you haven't done the word.

80
00:07:48,200 --> 00:07:51,200
You haven't done what is required to let go.

81
00:07:51,200 --> 00:07:54,200
Letting go is not something you do.

82
00:07:54,200 --> 00:08:01,200
In fact, the irony there is that you're talking about forcing yourself to let go, which are opposites.

83
00:08:01,200 --> 00:08:04,200
Letting go is, it means letting come actually.

84
00:08:04,200 --> 00:08:09,200
Letting go means really stopping to force things.

85
00:08:09,200 --> 00:08:13,200
Once you can do that, you'll be free from suffering.

86
00:08:13,200 --> 00:08:16,200
So the question is really a paradox.

87
00:08:16,200 --> 00:08:22,200
There's something like a paradox where you're asking, why can't I force myself to let go?

88
00:08:22,200 --> 00:08:26,200
When you say, why can't I let go?

89
00:08:26,200 --> 00:08:27,200
You don't let go.

90
00:08:27,200 --> 00:08:30,200
The letting go occurs because you see things clearly.

91
00:08:30,200 --> 00:08:37,200
There's just no clinging because you would have no reason to cling.

92
00:08:37,200 --> 00:08:42,200
The question as to why where it has to suffering is more interesting.

93
00:08:42,200 --> 00:08:44,200
And this is what's curious about this.

94
00:08:44,200 --> 00:08:48,200
Why the heck are we attached to that, which is suffering?

95
00:08:48,200 --> 00:08:52,200
And this is the key to the essence of Buddhism is that it's ignorance.

96
00:08:52,200 --> 00:08:55,200
We aren't always attached to suffering.

97
00:08:55,200 --> 00:08:59,200
It's more like we are being tossed about in the ocean.

98
00:08:59,200 --> 00:09:02,200
And so sometimes we go this way, sometimes we go that way.

99
00:09:02,200 --> 00:09:03,200
Sometimes we can be really good.

100
00:09:03,200 --> 00:09:06,200
Sometimes people are just naturally good.

101
00:09:06,200 --> 00:09:08,200
They're going in that direction.

102
00:09:08,200 --> 00:09:11,200
Sometimes people are naturally horrible, evil, mean creatures.

103
00:09:11,200 --> 00:09:15,200
They go up and down back and forth, but randomly.

104
00:09:15,200 --> 00:09:18,200
But it's all based on ignorance.

105
00:09:18,200 --> 00:09:23,200
Once you know, then you become steered in a specific direction.

106
00:09:23,200 --> 00:09:26,200
And that's towards peace, happiness, and freedom from suffering.

107
00:09:26,200 --> 00:09:29,200
So knowledge is something that directs the mind,

108
00:09:29,200 --> 00:09:33,200
just like the captain directs the ship.

109
00:09:33,200 --> 00:09:40,200
Without a captain, the mindfulness, the boat just is tossed to the waves.

110
00:09:40,200 --> 00:09:47,200
Gosh, by the waves, the current.

111
00:09:47,200 --> 00:09:58,200
So it's just, you could say, kind of by chance.

112
00:09:58,200 --> 00:10:00,200
It's where you find yourself.

113
00:10:00,200 --> 00:10:07,200
Or it's, one of the currents is attachment to suffering.

114
00:10:07,200 --> 00:10:10,200
And it's not being created.

