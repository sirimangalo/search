1
00:00:00,000 --> 00:00:11,000
So, okay, this is my second in my series of how-to's on meditation.

2
00:00:11,000 --> 00:00:22,000
So, the first clip was on basic sitting meditation, and I apologize, I'm restricted to ten

3
00:00:22,000 --> 00:00:28,000
megabytes, no ten minutes on YouTube, so we weren't able to do a full three minutes

4
00:00:28,000 --> 00:00:30,000
or a few minutes of meditation.

5
00:00:30,000 --> 00:00:32,000
So, in this one, I'm going to have you pause.

6
00:00:32,000 --> 00:00:35,000
Let me do guided meditation.

7
00:00:35,000 --> 00:00:42,000
If you're interested, you can sit, push pause, and I'll just sit for maybe 30 seconds

8
00:00:42,000 --> 00:00:44,000
or a minute or so.

9
00:00:44,000 --> 00:00:50,000
So, today I'm going to go over advanced techniques in sitting meditation, and they're not

10
00:00:50,000 --> 00:00:54,000
so advanced so that you should be scared away from listening to the sit.

11
00:00:54,000 --> 00:01:02,000
And that once you understand the basic sitting meditation already, then what do you do

12
00:01:02,000 --> 00:01:11,000
to deal with the problems and the phenomena which arise during meditation?

13
00:01:11,000 --> 00:01:16,000
So, last time we talked about the rising and the falling of the abdomen, and we use that

14
00:01:16,000 --> 00:01:22,000
as our meditation object, and we're saying rising, falling as it rises and falls.

15
00:01:22,000 --> 00:01:27,000
But the problem with that is, at the time when we're saying rising, falling,

16
00:01:27,000 --> 00:01:32,000
or we're trying to watch the abdomen, our mind starts to wander.

17
00:01:32,000 --> 00:01:36,000
Sometimes we have pain or aching in our bodies.

18
00:01:36,000 --> 00:01:41,000
Sometimes we have thoughts thinking about the past or thinking about the future,

19
00:01:41,000 --> 00:01:44,000
with thoughts, bad thoughts.

20
00:01:44,000 --> 00:01:47,000
And sometimes we have emotions arise like,

21
00:01:47,000 --> 00:01:52,000
seeing this, like not disliking this, disliking that, drowsiness,

22
00:01:52,000 --> 00:01:56,000
laziness, distraction, or doubt.

23
00:01:56,000 --> 00:02:00,000
These sort of things will arise during the time, and many other things,

24
00:02:00,000 --> 00:02:02,000
but this is sort of an example.

25
00:02:02,000 --> 00:02:05,000
So, all of these things I mentioned, they come under a heading,

26
00:02:05,000 --> 00:02:10,000
which is called the Four Foundations of Mindfulness, and this is a Buddhist term,

27
00:02:10,000 --> 00:02:12,000
but it's a meditation term.

28
00:02:12,000 --> 00:02:15,000
It's a very important thing in the practice of meditation.

29
00:02:15,000 --> 00:02:19,000
There are four foundations, meaning is, during the time you're practicing,

30
00:02:19,000 --> 00:02:25,000
you have to guard these four stations, or these four foundations.

31
00:02:25,000 --> 00:02:28,000
What it means, the first one is the body,

32
00:02:28,000 --> 00:02:31,000
and this is what we're starting when we're rising, falling.

33
00:02:31,000 --> 00:02:35,000
The point is, we can use this as our meditation object.

34
00:02:35,000 --> 00:02:39,000
It's called the body, and this is very simple, this one we already understand.

35
00:02:39,000 --> 00:02:41,000
But what happens when we have feelings?

36
00:02:41,000 --> 00:02:44,000
Well, the second foundation of mindfulness is called feelings.

37
00:02:44,000 --> 00:02:52,000
And this means that we need to have an answer to pain or aching

38
00:02:52,000 --> 00:02:54,000
or soreness that arises when we're sitting.

39
00:02:54,000 --> 00:02:59,000
And so this is our answer, is to use the pain as the meditation object.

40
00:02:59,000 --> 00:03:02,000
At the time when we feel pain in the legs or in the back,

41
00:03:02,000 --> 00:03:05,000
or so on, instead of staying rising, falling, and focusing on the stomach,

42
00:03:05,000 --> 00:03:09,000
instead shift your attention to the pain or the aching,

43
00:03:09,000 --> 00:03:12,000
to the part of the body which is causing suffering.

44
00:03:12,000 --> 00:03:17,000
And say to yourself, pain, pain,

45
00:03:17,000 --> 00:03:21,000
and say it calmly, say it quietly in your mind,

46
00:03:21,000 --> 00:03:23,000
you don't say it out loud.

47
00:03:23,000 --> 00:03:29,000
Only to keep the mind fixed on the reality of the object.

48
00:03:29,000 --> 00:03:32,000
The pain itself is not a bad thing.

49
00:03:32,000 --> 00:03:36,000
What we create suffering is when we start to say this pain is bad,

50
00:03:36,000 --> 00:03:39,000
or this pain is near, or this pain is caused by sitting meditation,

51
00:03:39,000 --> 00:03:42,000
or sitting meditation is bad, or so on.

52
00:03:42,000 --> 00:03:46,000
The pain itself is not bad, and if we learn to become friends with it,

53
00:03:46,000 --> 00:03:51,000
and learn to retrain our mind,

54
00:03:51,000 --> 00:03:54,000
realizing that it's only pain and nothing else,

55
00:03:54,000 --> 00:03:57,000
then we can be happy, and we can be at peace.

56
00:03:57,000 --> 00:03:58,000
This is what's called feelings.

57
00:03:58,000 --> 00:04:02,000
When we feel happy, we can also say happy, happy when we feel calm.

58
00:04:02,000 --> 00:04:04,000
We should also say calm.

59
00:04:04,000 --> 00:04:07,000
I'm being aware that these also are transitory.

60
00:04:07,000 --> 00:04:10,000
They're imperman, they come and they go.

61
00:04:10,000 --> 00:04:11,000
This is number two.

62
00:04:11,000 --> 00:04:14,000
Number three, the third foundation of mindfulness,

63
00:04:14,000 --> 00:04:16,000
so we have body, we have feelings.

64
00:04:16,000 --> 00:04:19,000
The third one is the mind, so we have to have an answer

65
00:04:19,000 --> 00:04:22,000
for what to do when our mind starts to wander.

66
00:04:22,000 --> 00:04:25,000
When we're sitting, but our mind is not with our body,

67
00:04:25,000 --> 00:04:27,000
our mind is not with the rising and falling,

68
00:04:27,000 --> 00:04:30,000
it's thinking this, thinking that.

69
00:04:30,000 --> 00:04:32,000
The answer is to say to ourselves,

70
00:04:32,000 --> 00:04:37,000
thinking, thinking, thinking, thinking.

71
00:04:37,000 --> 00:04:39,000
When we say thinking, thinking,

72
00:04:39,000 --> 00:04:43,000
our mind will come back to reality.

73
00:04:43,000 --> 00:04:45,000
Thinking will disappear and we'll come back.

74
00:04:45,000 --> 00:04:47,000
We can come back to rising and falling,

75
00:04:47,000 --> 00:04:50,000
but it's also to see clearly about the mind.

76
00:04:50,000 --> 00:04:52,000
This is clearly the nature of our mind,

77
00:04:52,000 --> 00:04:56,000
and we can start to see what is it that makes our mind

78
00:04:56,000 --> 00:05:00,000
what sort of activities make our mind distracted,

79
00:05:00,000 --> 00:05:04,000
and what sort of activities are going to bring focus

80
00:05:04,000 --> 00:05:07,000
and tranquility to them.

81
00:05:07,000 --> 00:05:09,000
This is the third foundation.

82
00:05:09,000 --> 00:05:13,000
The fourth foundation is called Dhamma,

83
00:05:13,000 --> 00:05:16,000
and it's specifically a Buddhist thing,

84
00:05:16,000 --> 00:05:20,000
but it means anything the mind takes is an object.

85
00:05:20,000 --> 00:05:22,000
It can mean anything.

86
00:05:22,000 --> 00:05:24,000
But in the beginning, we're focusing on those things

87
00:05:24,000 --> 00:05:25,000
which the mind will take as an object

88
00:05:25,000 --> 00:05:28,000
which are important in meditation.

89
00:05:28,000 --> 00:05:30,000
We're only teaching meditation here.

90
00:05:30,000 --> 00:05:33,000
We're not trying to get into the Buddhist doctrine

91
00:05:33,000 --> 00:05:37,000
or philosophy or something, not in this society.

92
00:05:37,000 --> 00:05:41,000
So there are five things which meditators have to keep in mind

93
00:05:41,000 --> 00:05:43,000
in the very beginning when they start to practice

94
00:05:43,000 --> 00:05:46,000
and these are called in simple terms,

95
00:05:46,000 --> 00:05:50,000
liking, disliking, drowsiness,

96
00:05:50,000 --> 00:05:53,000
distraction, and doubt.

97
00:05:53,000 --> 00:05:56,000
There's the liking, disliking, drowsiness, distraction.

98
00:05:56,000 --> 00:05:59,000
These things are important because when these come up

99
00:05:59,000 --> 00:06:01,000
and the mind will not be possible

100
00:06:01,000 --> 00:06:05,000
or it will be extremely difficult to keep the mind focused

101
00:06:05,000 --> 00:06:07,000
to keep the mind with the present moment

102
00:06:07,000 --> 00:06:10,000
with the objects that we're trying to watch.

103
00:06:10,000 --> 00:06:14,000
So these things we have to be aware of,

104
00:06:14,000 --> 00:06:17,000
and this is the fourth foundation called Dhamma.

105
00:06:17,000 --> 00:06:19,000
These five things are called the five hindrances.

106
00:06:19,000 --> 00:06:21,000
They're part of the Buddhist teaching,

107
00:06:21,000 --> 00:06:24,000
part of the Dhamma, part of reality.

108
00:06:24,000 --> 00:06:27,000
So they're part of reality that's important for meditators.

109
00:06:27,000 --> 00:06:30,000
So liking, if you like something, you want something

110
00:06:30,000 --> 00:06:32,000
so we can say to ourselves when we're sitting,

111
00:06:32,000 --> 00:06:34,000
rising, falling, we start to like,

112
00:06:34,000 --> 00:06:36,000
or we start to want state ourselves

113
00:06:36,000 --> 00:06:38,000
instead of saying rising, falling,

114
00:06:38,000 --> 00:06:40,000
switch to say liking, liking.

115
00:06:40,000 --> 00:06:42,000
I think a one thing, one thing.

116
00:06:42,000 --> 00:06:45,000
Learn, teach yourselves to let go.

117
00:06:45,000 --> 00:06:48,000
Retrain our minds not to get fixed

118
00:06:48,000 --> 00:06:51,000
and get obsessed with things.

119
00:06:51,000 --> 00:06:54,000
Number one, number two, disliking.

120
00:06:54,000 --> 00:06:56,000
We have disliking our anger upset.

121
00:06:56,000 --> 00:06:58,000
So we say to ourselves,

122
00:06:58,000 --> 00:07:01,000
disliking, disliking upset.

123
00:07:01,000 --> 00:07:03,000
Reminding ourselves is the only an emotion

124
00:07:03,000 --> 00:07:07,000
there's no need to get overworked.

125
00:07:07,000 --> 00:07:10,000
There's no need to get stressed out about this

126
00:07:10,000 --> 00:07:13,000
or freaked out or upset or so on.

127
00:07:13,000 --> 00:07:14,000
Only an emotion.

128
00:07:14,000 --> 00:07:16,000
So we say disliking, disliking,

129
00:07:16,000 --> 00:07:19,000
until it goes away.

130
00:07:19,000 --> 00:07:23,000
And the third one is called this grouseiness.

131
00:07:23,000 --> 00:07:25,000
So this is some people when they meditate

132
00:07:25,000 --> 00:07:27,000
to find themselves falling asleep.

133
00:07:27,000 --> 00:07:29,000
It's quite difficult and in the beginning

134
00:07:29,000 --> 00:07:31,000
you normally fall asleep when you meditate.

135
00:07:31,000 --> 00:07:32,000
It's just normal.

136
00:07:32,000 --> 00:07:33,000
It's just nothing wrong.

137
00:07:33,000 --> 00:07:37,000
It's the beginner mind or it's the beginning.

138
00:07:37,000 --> 00:07:39,000
It's the start to meditation.

139
00:07:39,000 --> 00:07:41,000
So when you feel drowsy,

140
00:07:41,000 --> 00:07:44,000
only say to yourself drowsy,

141
00:07:44,000 --> 00:07:50,000
and try to catch the nature of the grouseiness.

142
00:07:50,000 --> 00:07:52,000
And you find if you work on it after some time

143
00:07:52,000 --> 00:07:55,000
you can totally overcome the grouseiness.

144
00:07:55,000 --> 00:07:56,000
Of course, walking meditation helps,

145
00:07:56,000 --> 00:07:59,000
and that'll be my next episode.

146
00:07:59,000 --> 00:08:02,000
The fourth is called distraction.

147
00:08:02,000 --> 00:08:05,000
Distraction here is we put a specific meaning on it.

148
00:08:05,000 --> 00:08:08,000
It means specifically when you're thinking too much.

149
00:08:08,000 --> 00:08:10,000
When you're thinking beyond what is normal.

150
00:08:10,000 --> 00:08:12,000
When the mind is not focused,

151
00:08:12,000 --> 00:08:14,000
you cannot stay with anyone object is thinking here,

152
00:08:14,000 --> 00:08:17,000
thinking they're thinking all over the place all the time.

153
00:08:17,000 --> 00:08:20,000
It's called distracted.

154
00:08:20,000 --> 00:08:22,000
It's also a hindrance in meditation.

155
00:08:22,000 --> 00:08:23,000
So we say to ourselves,

156
00:08:23,000 --> 00:08:24,000
when we feel distracted,

157
00:08:24,000 --> 00:08:27,000
we say distracted, distracted, distracted.

158
00:08:27,000 --> 00:08:29,000
And the fifth one is doubt.

159
00:08:29,000 --> 00:08:30,000
And doubt about ourselves,

160
00:08:30,000 --> 00:08:32,000
thought about our practice, thought about this,

161
00:08:32,000 --> 00:08:34,000
thought about that.

162
00:08:34,000 --> 00:08:37,000
In meditation practice doubt is a hindrance.

163
00:08:37,000 --> 00:08:40,000
We're not talking about theology or philosophy

164
00:08:40,000 --> 00:08:43,000
or Buddhist doctrine or so on.

165
00:08:43,000 --> 00:08:45,000
We're talking about meditation.

166
00:08:45,000 --> 00:08:49,000
In meditation doubt is a bad thing.

167
00:08:49,000 --> 00:08:50,000
And why?

168
00:08:50,000 --> 00:08:54,000
Because it disturbs the concentration and the focus of the mind.

169
00:08:54,000 --> 00:08:56,000
So when we feel doubt,

170
00:08:56,000 --> 00:08:57,000
we have to say to ourselves,

171
00:08:57,000 --> 00:08:58,000
doubt thing,

172
00:08:58,000 --> 00:09:01,000
doubt thing, doubt thing, doubt thing,

173
00:09:01,000 --> 00:09:02,000
doubt thing.

174
00:09:02,000 --> 00:09:05,000
These together are the five hindrance.

175
00:09:05,000 --> 00:09:07,000
You know, the fourth foundation of mindfulness.

176
00:09:07,000 --> 00:09:08,000
So altogether we have body,

177
00:09:08,000 --> 00:09:10,000
feeling mind and mind objects.

178
00:09:10,000 --> 00:09:14,000
What it is is the basics of meditation practice

179
00:09:14,000 --> 00:09:38,000
in the Buddhist tradition.

