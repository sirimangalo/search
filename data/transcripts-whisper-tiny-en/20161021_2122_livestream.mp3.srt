1
00:00:00,000 --> 00:00:14,920
require something internal, an eye, but that eyes is considered to be the physical aspect

2
00:00:14,920 --> 00:00:22,720
of the experience, which arises in seizes.

3
00:00:22,720 --> 00:00:27,920
So what it means is the experience, since the experience arises in seizes in a moment,

4
00:00:27,920 --> 00:00:31,800
every part of it, whether the physical part and the mental part, all of them arise

5
00:00:31,800 --> 00:00:33,360
in seizes in a moment.

6
00:00:33,360 --> 00:00:37,360
So it's just quite technical, and I mean, you think it's actually kind of pedantic, I think,

7
00:00:37,360 --> 00:00:39,680
well, why don't we just say the eye exists?

8
00:00:39,680 --> 00:00:47,040
Because we're only talking about reality from the point of view of experience, from the

9
00:00:47,040 --> 00:00:54,480
point of experience, even none of these things really practically speaking exist.

10
00:00:54,480 --> 00:00:58,800
There are just aspects of this one thing that we call an experience of seeing.

11
00:00:58,800 --> 00:01:03,520
So in the experience of seeing there, which is momentary, there's the physical aspect,

12
00:01:03,520 --> 00:01:10,360
there is the physical internal aspect, there's the physical external aspect, and there's

13
00:01:10,360 --> 00:01:11,960
the mental aspect.

14
00:01:11,960 --> 00:01:19,440
And those three coming together are those three arising together, or the experience of

15
00:01:19,440 --> 00:01:31,720
seeing, but in a way you could think of it as just convention, because you can also see

16
00:01:31,720 --> 00:01:39,320
things, you can also see things in the mind without using the eye, without relying

17
00:01:39,320 --> 00:01:41,200
on light.

18
00:01:41,200 --> 00:01:49,360
So we're just describing, hey, it appears that certain things are physical.

19
00:01:49,360 --> 00:01:57,080
So you see actual light, and it's touching an actual eye.

20
00:01:57,080 --> 00:02:03,000
But in reality, when you see, there's only the experience of kind of extrapolating the

21
00:02:03,000 --> 00:02:10,720
fact that there is the physical base, physical eye base.

22
00:02:10,720 --> 00:02:13,200
So as I said, I wouldn't really overthink this.

23
00:02:13,200 --> 00:02:19,040
You're not trying to see that practically speaking, trying to see that each and every one

24
00:02:19,040 --> 00:02:22,480
of them individually is impermanent, and we're certainly not talking about this physical

25
00:02:22,480 --> 00:02:23,480
eye.

26
00:02:23,480 --> 00:02:30,360
The physical eye, as you say, is lasting and stable.

27
00:02:30,360 --> 00:02:33,400
So that's because it doesn't exist.

28
00:02:33,400 --> 00:02:35,600
It's not real.

29
00:02:35,600 --> 00:02:40,520
Real is the experience that arises and it's safe to focus more on that.

30
00:02:40,520 --> 00:02:45,520
So it's just that the texts often try to break everything down, and they say, they take

31
00:02:45,520 --> 00:02:49,520
each aspect and say, well, each one of them is impermanent, but it's not a you experience.

32
00:02:49,520 --> 00:02:51,520
The experience that it's seeing is impermanent.

33
00:02:51,520 --> 00:02:57,280
It's seeing arises and states that it's how we practically speaking, see it.

34
00:02:57,280 --> 00:03:06,800
It's my understanding of it.

35
00:03:06,800 --> 00:03:10,920
And when you talk about physical forms being around for many thousands of years, no,

36
00:03:10,920 --> 00:03:15,000
we're talking about the physical form that is part of that experience.

37
00:03:15,000 --> 00:03:20,840
The rock itself doesn't exist, not from the point of view of experience, not which

38
00:03:20,840 --> 00:03:25,000
is the paradigm that we use in Buddhism as you can see.

39
00:03:25,000 --> 00:03:32,840
Well, that was all the questions for tonight.

40
00:03:32,840 --> 00:03:33,840
That was it.

41
00:03:33,840 --> 00:03:34,840
All right.

42
00:03:34,840 --> 00:03:40,280
Well, then have a good night, everyone.

43
00:03:40,280 --> 00:03:43,280
Thanks Robin for your help.

44
00:03:43,280 --> 00:03:44,280
Thank you, Bante.

45
00:03:44,280 --> 00:04:04,280
Good night.

