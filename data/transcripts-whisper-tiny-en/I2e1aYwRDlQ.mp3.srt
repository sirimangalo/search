1
00:00:00,000 --> 00:00:24,680
Good evening everyone broadcasting live July 6th today's quote is about suffering it's all

2
00:00:24,680 --> 00:00:35,440
about suffering isn't it it's one of the first things you hear about Buddhism is

3
00:00:35,440 --> 00:00:50,600
that I'm going to focus on suffering right about talk about or that explain

4
00:00:50,600 --> 00:01:06,880
Buddhism will try and defend it couch it in fine language and nuance which is

5
00:01:06,880 --> 00:01:16,920
kind of funny because it's the most important it's the most important aspect

6
00:01:16,920 --> 00:01:23,960
of suffering it's the most important aspect of spiritual practice it's the most

7
00:01:23,960 --> 00:01:35,520
important concept or quality of the idea in the world and the universe for being

8
00:01:35,520 --> 00:01:44,160
the Buddha was incredible that he was able to so clearly identify the problem

9
00:01:44,160 --> 00:01:59,520
it's almost two obvious almost doesn't even need to be said except that of course

10
00:01:59,520 --> 00:02:06,880
what is the problem that's suffering it turns out that we're very good at avoiding

11
00:02:06,880 --> 00:02:19,240
and twisting the twisting the truth to find some other problem to not have to

12
00:02:19,240 --> 00:02:25,400
deal with suffering if I can solve something else there would be no problem

13
00:02:25,400 --> 00:02:42,840
only I had money only I had a girlfriend or a boyfriend the problem is inequality

14
00:02:42,840 --> 00:02:58,920
or the problem is lack of respect to the problem is the problem is whatever

15
00:02:58,920 --> 00:03:06,040
problem I'm facing whether it be external or internal

16
00:03:06,040 --> 00:03:15,880
when the back it's quite simple the problem is suffering that's the problem

17
00:03:15,880 --> 00:03:21,720
that's what we should be focusing on I think it's so depressing to talk about

18
00:03:21,720 --> 00:03:34,200
suffering right until brainwashed we've become into avoiding problem and you

19
00:03:34,200 --> 00:03:40,800
can't fix the problem because from early on in life we we get we suffer

20
00:03:42,160 --> 00:03:51,480
we can't we have no way of fixing it and so we learn to avoid ignore

21
00:03:55,520 --> 00:04:01,120
become willfully blind towards suffering there's another way we don't have an

22
00:04:01,120 --> 00:04:10,720
answer so we we learn to put up with it and so you often get the the view

23
00:04:10,720 --> 00:04:16,320
that suffering is a part of life suffering is something you can't avoid I

24
00:04:16,320 --> 00:04:20,520
mean that's actually kind of wise you know in Buddhism we talk about that we

25
00:04:20,520 --> 00:04:27,240
can't yes there are aspects of suffering you can't avoid in life you can't

26
00:04:27,240 --> 00:04:36,320
avoid getting old you can't avoid getting sick you can't avoid dying you're

27
00:04:36,320 --> 00:04:41,080
donating defecating eating hunger thirst heat cold you can't avoid all these

28
00:04:41,080 --> 00:04:43,440
things

29
00:04:47,120 --> 00:04:51,480
and then you can be free from suffering

30
00:04:51,480 --> 00:05:01,680
the Buddha asks this the the court is actually more specific it's about these

31
00:05:01,680 --> 00:05:09,200
boys were tormenting fish but someone noted this quite apt for national

32
00:05:09,200 --> 00:05:14,080
fishing day I don't know if it's in America but in Canada it's hard people are

33
00:05:14,080 --> 00:05:18,160
going around celebrating celebrating the catching of fish it's like a

34
00:05:18,160 --> 00:05:26,400
or ice hockey or something they call it a sport one thing would be a sport if

35
00:05:26,400 --> 00:05:32,120
we gave weapons to the other team right then it would be a sport bloodthirsty

36
00:05:32,120 --> 00:05:42,600
sport that would that would so over people up if the fish had fishing

37
00:05:42,600 --> 00:05:51,200
rock I could hook the humans became a test of who could hook the other team who

38
00:05:51,200 --> 00:05:58,160
could bait the other team see whatnot we're gonna eat this plate of food whether

39
00:05:58,160 --> 00:06:09,960
it was it was bait and that'd be awful no way of knowing just having to take

40
00:06:09,960 --> 00:06:13,520
you've got feeling whether you should eat this plate of food or not because

41
00:06:13,520 --> 00:06:21,840
there might be some invisible hook in it suddenly yanked into the water and

42
00:06:21,840 --> 00:06:27,400
drowned it's basically it

43
00:06:33,520 --> 00:06:38,040
so they were tormenting fish it looks like they weren't even killing them

44
00:06:38,040 --> 00:06:43,000
they were they weren't going to eat them it was just for fun because kids are

45
00:06:43,000 --> 00:06:52,000
like that and says kids are innocent kids are not innocent kids are pure kids

46
00:06:52,000 --> 00:06:58,120
are not pure we should just try and be a kid I can't think of anything more

47
00:06:58,120 --> 00:07:05,040
horrific kids are horrible it can be so cruel but the interesting thing is it's

48
00:07:05,040 --> 00:07:10,160
not it's not because they're in intentionally evil it's because they don't

49
00:07:10,160 --> 00:07:15,120
know any better they don't understand the consequences kids have forgotten all

50
00:07:15,120 --> 00:07:26,200
about consequences they're born fresh and let go completely of past memories

51
00:07:26,200 --> 00:07:33,400
it's all they can think about is the pleasure that they can get wherever they

52
00:07:33,400 --> 00:07:43,640
can get it like kids are so happy so simple they just want to be happy

53
00:07:43,640 --> 00:07:50,440
you don't realize the consequences of their their quest for happiness and

54
00:07:50,440 --> 00:07:54,640
so they do terrible things oh let's torment some fish that'll make us happy

55
00:07:54,640 --> 00:08:00,760
doesn't matter what you're about to fish as long as we get happy the Buddha

56
00:08:00,760 --> 00:08:07,520
comes out and asks them to try to point out the consequences

57
00:08:10,120 --> 00:08:18,960
until there's more of this helping us to realize we're not so separate from

58
00:08:18,960 --> 00:08:30,180
our victims so separate from each other and so he asks them it's the

59
00:08:30,180 --> 00:08:48,300
valley by at the world to make America to cuss up a young woman to come are you afraid boys of suffering is

60
00:08:48,300 --> 00:08:58,340
suffering up here means not dear and dear and unpleasant to you and pleasing

61
00:08:58,340 --> 00:09:09,620
displeasing to you and they said even when they buy him a man even when they

62
00:09:09,620 --> 00:09:17,260
buy him a man when they do cuss up young no to come yes

63
00:09:17,260 --> 00:09:22,260
it is so vulnerable sir we are afraid of we're afraid when

64
00:09:22,260 --> 00:09:33,340
normal sir of suffering suffering is displeasing to us this from new

65
00:09:33,340 --> 00:09:41,380
Donna who Donna is where the Buddha gave inspired who Donna is where you

66
00:09:41,380 --> 00:09:47,620
proclaim something it's a new Donna so these are these are short generally

67
00:09:47,620 --> 00:09:53,220
short suit does where the Buddha would proclaim something something sort of

68
00:09:53,220 --> 00:10:00,060
quite profound or meaningful anyway and so then he gave you Donna at the

69
00:10:00,060 --> 00:10:17,180
Kolbago I ate them we need to die young with I am I'm a good darling at that time the

70
00:10:17,180 --> 00:10:18,380
blessing one gave this exclamation this utterance

71
00:10:18,380 --> 00:10:24,580
Satyay by at the dukas if you are afraid of suffering Satyay will do come up

72
00:10:24,580 --> 00:10:31,580
yeah if suffering is displeasing to you mark at that papa kung kung kung kung kung

73
00:10:31,580 --> 00:10:42,260
don't do evil deeds how we were how we were young diva rahou in public or in

74
00:10:42,260 --> 00:10:56,180
private Satyay it's a papa kung kung kung kung kari sata karo tova if and if evil

75
00:10:56,180 --> 00:11:09,820
deeds you will do or do do or will do right if you are doing or or will do now

76
00:11:09,820 --> 00:11:21,140
or do kamu jati who fades up be alive at our verses are sometimes difficult

77
00:11:21,140 --> 00:11:33,580
because it's mangled basically you cannot be you cannot find an escape from death

78
00:11:33,580 --> 00:11:46,900
if I'm suffering you will paint something you will not be free from suffering by

79
00:11:46,900 --> 00:11:51,260
running away from it by escaping something

80
00:11:51,260 --> 00:12:08,500
don't do evil no one to suffer quite simple evil leads to suffering suffering

81
00:12:08,500 --> 00:12:16,820
is the problem don't do you well there's not a complicated place what is the all

82
00:12:16,820 --> 00:12:25,460
the problems of the world can boil down to to this suffering well

83
00:12:25,460 --> 00:12:31,300
inequality isn't the problem it's suffering the climate isn't problem the

84
00:12:31,300 --> 00:12:36,340
environment isn't a problem it's a problem it isn't even a problem that you don't

85
00:12:36,340 --> 00:12:43,180
have any money or even any food the problem is suffering sometimes you lose your

86
00:12:43,180 --> 00:12:48,220
job and well you still have food and you still have health and you still have a

87
00:12:48,220 --> 00:12:54,500
place to stay but it's so much suffering you don't want to have lost your jobs

88
00:12:54,500 --> 00:13:05,060
and suffering and then the cause of suffering is even quite simple cause of

89
00:13:05,060 --> 00:13:15,980
suffering in essence is evil that's kind of a totality that that it's evil

90
00:13:15,980 --> 00:13:21,300
because it causes suffering it causes suffering because it's more like it's

91
00:13:21,300 --> 00:13:27,020
evil because it causes suffering so the question is what is evil what is it that

92
00:13:27,020 --> 00:13:36,500
causes suffering this is the key then the Buddha was equally awesome to be able

93
00:13:36,500 --> 00:13:50,140
to identify the simple cause the cause of suffering isn't other people or isn't

94
00:13:50,140 --> 00:13:56,660
the situation you're in cause of suffering is it's not any situation on any

95
00:13:56,660 --> 00:14:03,100
experience cause of suffering is our reactions to experience specifically more

96
00:14:03,100 --> 00:14:13,900
directly our attachment our desire for certain experiences it's our desires

97
00:14:13,900 --> 00:14:28,180
that propel us to cultivate expectations hopes one ambitions when they set us up

98
00:14:28,180 --> 00:14:31,380
for disappointment you want things to be a certain way and then they're in

99
00:14:31,380 --> 00:14:47,460
other way when you like something not having it change when your partial

100
00:14:47,460 --> 00:14:54,820
to certain experiences other experiences become unpleasant displeasing

101
00:14:54,820 --> 00:15:04,100
and then you cultivate diversion towards them and not what you want

102
00:15:10,300 --> 00:15:15,260
craving is called the cause of suffering but it still doesn't actually get to

103
00:15:15,260 --> 00:15:18,980
the point the interesting thing is we say craving is the cause of suffering so we

104
00:15:18,980 --> 00:15:24,940
talk a lot about letting go if you can let go then you'll be free from suffering

105
00:15:24,940 --> 00:15:30,300
but it still doesn't tell the whole picture because how do you let go I mean

106
00:15:30,300 --> 00:15:37,300
it's actually quite reasonable and fairly well understood and in general

107
00:15:37,300 --> 00:15:41,420
sense that yeah if you can learn to let go you suffer less stressing and

108
00:15:41,420 --> 00:15:46,380
obsessing questions how do you let go we haven't completed the picture

109
00:15:46,380 --> 00:15:52,460
because the the cause of craving there's really a vast what is the cause of

110
00:15:52,460 --> 00:15:56,620
craving what is the cause of desire there's the cause of clinging to things

111
00:15:56,620 --> 00:16:02,460
expecting things wanting things needing things to be a certain way it's

112
00:16:02,460 --> 00:16:10,860
actually it's not some inherent evil or maliciousness on our part it's simple

113
00:16:10,860 --> 00:16:18,300
delusion simple ignorance you know understand why there is suffering

114
00:16:18,300 --> 00:16:23,660
they're suffering because of nature because of the way nature works it's

115
00:16:23,660 --> 00:16:35,340
it's somewhat random or happen happen stance chance things have turned out

116
00:16:35,340 --> 00:16:44,940
the way they have on women there's no rhyme or reason to the universe there's

117
00:16:44,940 --> 00:16:54,660
no meaning behind things there's a history a long and terrible history but in

118
00:16:54,660 --> 00:17:02,940
all aspects the universe is put together by put together haphazardly by

119
00:17:02,940 --> 00:17:18,260
chance by by blindness and through blindness we have created this this

120
00:17:18,260 --> 00:17:24,900
prison for ourselves created this existence that sometimes good sometimes bad

121
00:17:24,900 --> 00:17:28,100
sometimes we're really great people we can be really good to each other

122
00:17:28,100 --> 00:17:34,660
usually based on knowledge of cause of the fact but then we forget in

123
00:17:34,660 --> 00:17:39,860
the other side of the good and we become terrible people again we become

124
00:17:39,860 --> 00:17:45,540
addicted to things we become obsessed we don't realize we're doing it we

125
00:17:45,540 --> 00:17:55,220
cultivate all sorts of bad habits and we suffer this is what you realize in

126
00:17:55,220 --> 00:18:01,700
meditation what am I doing to myself what have I been doing to myself how

127
00:18:01,700 --> 00:18:05,460
could I have been so blind this is what you realize when you start to

128
00:18:05,460 --> 00:18:15,940
meditate you see all your bad habits it's didn't even totally

129
00:18:19,300 --> 00:18:27,460
inept this life thing incompetent incredible incompetence

130
00:18:27,460 --> 00:18:36,180
inability to to find the truth to find the right to find the good to find

131
00:18:36,180 --> 00:18:40,900
that which makes us happy why can't we just be happy why can't we find what makes

132
00:18:40,900 --> 00:18:48,660
us happy why do we suffer blindness ignorance when you practice

133
00:18:48,660 --> 00:18:52,580
insight meditation that's what happens you don't let go it's not what you're

134
00:18:52,580 --> 00:18:58,900
doing letting go comes from the practice

135
00:18:58,900 --> 00:19:03,860
why does it come because you're you see clearly what you're doing is

136
00:19:03,860 --> 00:19:07,860
trying to see things just see things as they are

137
00:19:07,860 --> 00:19:12,340
open your eyes understand what you're doing to yourself

138
00:19:12,340 --> 00:19:17,300
you start to see you start to see what you're doing

139
00:19:17,300 --> 00:19:25,140
what the mind is doing you see greed you see anger you see delusion

140
00:19:25,140 --> 00:19:31,140
you see conceit you see arrogance all sorts of evil

141
00:19:31,140 --> 00:19:36,100
why is it evil you can see it's causing you suffering

142
00:19:37,860 --> 00:19:44,020
so anyway that's not a bit about suffering tonight

143
00:19:44,020 --> 00:19:47,780
it's so much so great to talk such a great subject it's something we should

144
00:19:47,780 --> 00:19:52,740
talk about all the time it should all concentrate our efforts and our

145
00:19:52,740 --> 00:19:56,740
focus our attention on suffering we should

146
00:19:56,740 --> 00:20:01,860
we should stop being such a taboo subject it's the problem it's what we

147
00:20:01,860 --> 00:20:05,300
should all be working on it's what the government should be working on

148
00:20:05,300 --> 00:20:09,540
it's what everyone religious should be working on freaking

149
00:20:09,540 --> 00:20:14,420
them on god god it's not going to help you

150
00:20:14,420 --> 00:20:22,420
god it's not the problem praying is nothing problem

151
00:20:22,420 --> 00:20:30,100
suffering that's the problem all right so let's look at some questions here

152
00:20:30,100 --> 00:20:33,460
it'll really be nice if you could put the question mark before the question

153
00:20:33,460 --> 00:20:38,340
it's pretty easy if you figured out that'd make it easier for me to tell

154
00:20:38,340 --> 00:20:44,740
what your question do you always associate the feeling with the thought

155
00:20:44,740 --> 00:20:51,220
saying that this is the feeling this is the that's really

156
00:20:51,220 --> 00:20:55,380
you have to be careful to be dramatic on your questions

157
00:20:55,380 --> 00:21:00,500
i think it gives me a free pass to skip it because i don't know what you're saying

158
00:21:02,580 --> 00:21:07,060
uh this feeling is from this thought or should they be a separate things you

159
00:21:07,060 --> 00:21:10,740
only have to worry about what things are don't don't focus too much

160
00:21:10,740 --> 00:21:15,060
in terms of your practice on cause and effect you'll see cause and

161
00:21:15,060 --> 00:21:18,340
effect you'll see what leads to what what's fine

162
00:21:18,340 --> 00:21:23,220
your practice should only be this is this

163
00:21:23,220 --> 00:21:27,700
remember those words this is this that's your your whole practice

164
00:21:27,700 --> 00:21:45,220
reminding yourself this is this

165
00:21:51,220 --> 00:21:55,860
yeah those kids videos if you haven't you haven't if you know of any

166
00:21:55,860 --> 00:22:02,340
these kids who uh you think might be interested in meditation

167
00:22:02,340 --> 00:22:08,180
there's videos on how to meditate for kids it seemed to work seemed to be

168
00:22:08,180 --> 00:22:18,740
beneficial how do we observe understand mind and mental objects

169
00:22:18,740 --> 00:22:23,700
well mental objects is just a translation of dhamma dhamma doesn't really

170
00:22:23,700 --> 00:22:30,820
translate to mental objects but that's an explanatory translation

171
00:22:32,180 --> 00:22:36,180
well there's a six senses this is in the context of six senses so seeing

172
00:22:36,180 --> 00:22:41,460
what the object of seeing is is lighter or formed

173
00:22:41,460 --> 00:22:46,100
which the object of hearing is sound smells tastes feeling the object of the

174
00:22:46,100 --> 00:22:52,420
mind is thoughts or dhamma just means something mental

175
00:22:52,420 --> 00:22:58,500
something something in the mind

176
00:22:58,500 --> 00:23:02,900
I think that can be also if you see have visions in the in the mind or if you

177
00:23:02,900 --> 00:23:07,700
have uh if a song is going through your head then it's still I think

178
00:23:07,700 --> 00:23:12,420
called dhamma even though it was a sight or a sound because it's not coming

179
00:23:12,420 --> 00:23:15,460
from the eyes it's considered to be dhamma

180
00:23:15,460 --> 00:23:27,300
the suffering and the direct effect of

181
00:23:27,300 --> 00:23:32,980
latching on the artificial nature of things rather than their true nature

182
00:23:36,340 --> 00:23:42,580
I mean it's not all artificial nature causes suffering

183
00:23:42,580 --> 00:23:48,900
but it's certainly necessary if you see things truly as they are you won't

184
00:23:48,900 --> 00:23:54,580
latch onto the manual software so it's necessary but not sufficient

185
00:23:56,820 --> 00:24:02,340
I think I would say if you see the artificial nature things I mean even

186
00:24:02,340 --> 00:24:07,300
enlightened people see enlightened being see the artificial nature they know

187
00:24:07,300 --> 00:24:12,180
that this is a camera or computer this is a monk sitting here

188
00:24:12,180 --> 00:24:19,300
that's artificial but they know it they're not even capable of seeing it

189
00:24:19,300 --> 00:24:24,420
but the latching on I guess is the key here

190
00:24:25,300 --> 00:24:30,260
the latching on to anything is the problem it's the latching not the artificial

191
00:24:30,260 --> 00:24:34,500
but it will only latch because of the artificial you can't latch on when you see

192
00:24:34,500 --> 00:24:39,380
something as it is because it arises and ceases it's

193
00:24:39,380 --> 00:24:45,140
it's absurd to think about latching on to it

194
00:24:47,940 --> 00:24:53,140
what are some good words for noting the transition from standing to sitting

195
00:24:53,140 --> 00:24:56,100
and vice versa

196
00:24:56,100 --> 00:25:02,420
I mean whatever bending lifting touching

197
00:25:02,420 --> 00:25:08,020
you should note intention if you're if you can try to catch the intention

198
00:25:08,020 --> 00:25:11,780
wanting to stand wanting to stand and bending

199
00:25:11,780 --> 00:25:15,460
looking and stretching and so on

200
00:25:15,460 --> 00:25:20,660
wanting to sit wanting to bending lowering touching

201
00:25:27,860 --> 00:25:33,380
52 viewers becoming things are really picking up

202
00:25:33,380 --> 00:25:37,060
they got more and more people coming to do meditation courses

203
00:25:37,060 --> 00:25:40,820
we really should think seriously about

204
00:25:40,820 --> 00:25:47,860
about someday setting up our own center not renting a house

205
00:25:47,860 --> 00:25:52,420
we should think about this we have lots of people interested in

206
00:25:52,420 --> 00:25:59,220
we should talk about it see if there's enough support

207
00:25:59,220 --> 00:26:06,660
I think because this isn't really you know stable

208
00:26:06,660 --> 00:26:09,620
I don't know what the situation and things is going to be whether we're always

209
00:26:09,620 --> 00:26:12,580
going to be able to rent like this

210
00:26:12,580 --> 00:26:18,660
and moreover it's we're gonna be full we're gonna have four meditators

211
00:26:18,660 --> 00:26:25,940
throughout August and into September

212
00:26:25,940 --> 00:26:29,940
it doesn't seem like a lot but it's a full house

213
00:26:32,900 --> 00:26:37,380
so anyway great to see such

214
00:26:37,380 --> 00:26:45,140
such interest in these things it's great to think that this is somehow useful

215
00:26:46,740 --> 00:26:53,380
which reminds me I'm probably going to try to set up a testimonials page

216
00:26:53,380 --> 00:26:59,140
if you've been practicing meditation I'll probably make an announcement

217
00:26:59,140 --> 00:27:04,100
once we get it set up again but

218
00:27:04,100 --> 00:27:09,220
practicing this meditation and feel like it's been beneficial to you

219
00:27:09,220 --> 00:27:15,140
if you could send us a testimonial this means some short words about how

220
00:27:15,140 --> 00:27:19,140
the meditation benefited you send it to info at

221
00:27:19,140 --> 00:27:26,740
seremungalow.org for now I'll try to set up a new email address or something

222
00:27:26,740 --> 00:27:31,060
but info works info at seremungalow.org

223
00:27:31,060 --> 00:27:33,700
and we'll try to collect them start collecting them and

224
00:27:33,700 --> 00:27:39,540
put them up on the website I also get from Facebook from time to time and

225
00:27:39,540 --> 00:27:44,500
even YouTube from time to time so we'll try to collect them all together

226
00:27:44,500 --> 00:27:50,820
I think that's useful for most people to decide whether we're legit

227
00:27:50,820 --> 00:28:05,460
or that we're not some fly-by-night operation or some cult or

228
00:28:05,460 --> 00:28:11,300
are some people more like more attached to a version than to read

229
00:28:11,300 --> 00:28:16,260
yes some people are more inclined towards anger

230
00:28:19,940 --> 00:28:22,420
how can we be sure of the accuracy of our

231
00:28:22,420 --> 00:28:25,780
notings with noting falsely not consolidate

232
00:28:25,780 --> 00:28:32,740
delusion no it would come from delusion I think

233
00:28:32,740 --> 00:28:36,740
it's not that difficult because if you know if you know it's

234
00:28:36,740 --> 00:28:40,420
something like cat cat it's not really clear and it's not

235
00:28:40,420 --> 00:28:48,180
cutting through the delusion not as well

236
00:28:48,180 --> 00:28:52,980
so you have to note realities if you hear something like you hear

237
00:28:52,980 --> 00:28:56,180
something and say seeing that's problematic as

238
00:28:56,180 --> 00:29:11,380
close because without twisted that makes your mind

239
00:29:11,380 --> 00:29:15,220
all conditioned phenomena are dukka is still true

240
00:29:15,220 --> 00:29:21,060
even if you don't latch on to things

241
00:29:21,060 --> 00:29:29,140
well there dukka doesn't mean they're suffering dukka means they're

242
00:29:30,260 --> 00:29:32,900
basically not worth clinging to and I know that's not a direct

243
00:29:32,900 --> 00:29:36,740
translation but it still relates to clinging if you don't cling to

244
00:29:36,740 --> 00:29:40,500
something can't cause yourself

245
00:29:42,740 --> 00:29:46,340
dukka means basically not worth clinging to

246
00:29:46,340 --> 00:29:51,860
that's the explanatory translation

247
00:29:51,860 --> 00:29:55,780
so in our hand you could say it still has dukka no not really

248
00:29:55,780 --> 00:29:59,460
yes in a sense and they would admit that nabuddha

249
00:29:59,460 --> 00:30:05,460
but not in a sense it's not really suffering they're not suffering

250
00:30:07,140 --> 00:30:10,580
dukka means when you cling to it it causes your suffering why because it's

251
00:30:10,580 --> 00:30:16,100
impermanent you won't meet your expectations

252
00:30:16,100 --> 00:30:19,540
it's unpredictable you don't like it while it might come

253
00:30:19,540 --> 00:30:22,980
you like it while it might go

254
00:30:22,980 --> 00:30:27,540
it's not under your control it's a nitya dukka ananta

255
00:30:27,540 --> 00:30:31,060
means these things are not worth clinging to basically somebody dhamma

256
00:30:31,060 --> 00:30:35,380
nalana be new wayside that's all you need to know

257
00:30:35,380 --> 00:30:43,140
no namma is worth clinging to

258
00:30:43,940 --> 00:30:48,260
all right enough for tonight thank you all for tuning in

259
00:30:48,260 --> 00:30:51,140
machine all good practice

260
00:30:51,140 --> 00:31:07,620
dee he well

