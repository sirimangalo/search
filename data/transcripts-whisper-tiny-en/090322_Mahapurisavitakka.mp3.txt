So today I'd like to talk about this one of my favorite aspects of the Dhamma, the teaching
of the Lord Buddha.
This is the Maham police that we talk about, the eight reflections of thoughts that arise
in a great being, arise in someone who is practicing to better themselves, to develop
themselves.
So as we practice the Dhamma, this is something that we realize and something that we realize
about the teaching of the Buddha.
Specifically we realize that this teaching in one sense, it's not for everybody.
So we can say that everyone will benefit from the practice of the Buddha's teaching.
There are always more people who will not accept and not appreciate the meditation practice.
And this is with the course with everything.
Everything in the world, there are always some people who like it and some people who don't like it.
Some people like swimming, some people don't.
Some people like golf, some people don't.
Some people like this food or that food and people don't.
That's with everything.
But the thing about the Buddha's teaching that one realizes as one practices.
One realizes that there are eight, altogether eight, realizations, which they sort of came up with.
How it happened was one of the Lord Buddha's students, Anurutta, when he was practicing alone,
he had these seven considerations come up in his mind.
And the first one is that this teaching, the realization that this teaching is not for everybody.
But what is sort of amazing about it is, or how wonderful it is.
One of the wonderful things about it is that it's only those people who are content with little.
I'm sorry, who have a few wants can really and really appreciate the teaching.
So it's sort of an appreciation of the Buddha's teaching that it's only people who are in a good way,
can appreciate it, can come to understand and practice it.
Those people who want a lot of things, and yeah, truly you can say that these people will not appreciate
Buddha's teaching, because it's about letting go and giving up.
These people will not be able to keep even the simplest moral precepts,
let alone be able to develop concentration and wisdom.
More we talk about letting go and giving up and sitting still,
the more turned off they become.
They talk about suffering, how these things are not going to make you happy and so on.
This is something which is very unpleasant to them, undesirable to these.
To most people really, because we have many wants and many addictions in this sense.
So our addictions lead us to disadain the idea that giving these things out makes them a good thing.
So I know Buddha realized this, and this is one of his sort of a deep realization that came to him,
something that helped him to realize what was going to get him through this practice.
This was before I know Buddha was enlightened.
He was still practicing to become enlightened.
So as he was practicing the teaching, just as we were all practicing here,
he realized that he was going to have to let go of a lot of his wants.
No wanting for beautiful things, or as the texts say wanting to appreciate him,
wanting other people to admire him for his special attainment.
This is the first realization that came to him.
The second realization is this is a very similar one instead.
Not only do we need to have few wants, we also have to be content,
and this means content with the things that we have.
So in the text say about one who appreciates Buddha's teaching,
if they get whatever clothing they get, as monks, whatever robes they get,
and when they get ragrobes, they wear the ragrobes feeling just as comfortable
as any ordinary person with all of these wardrobe full of clothes.
Whatever arms food they get when they mix it up in the bowl,
and they don't know normally it's some good food,
some is very coarse food, but they eat it feeling just as comfortable
as someone who had the best selection of the most nutritious,
healthiest, delicious food.
Whenever dwelling they have, when they sit at the root of the tree,
monks are supposed to ferment a strive to live at the foot of a tree
for as long as life shall last, or always forever,
or to always strive to go out into the forest, into the root of the tree.
When we live at the root of the tree, when we reflect in this way,
when we're content, when we don't have wishes and wants and desires and need,
we're just as comfortable.
In fact, of course, much more comfortable than most people living in these wonderful, luxurious houses,
you know, where the wind can't get at them, the flies can't get at them.
The heat, the cold, and so on, but here living under the tree
where all of these things can get at the monk,
because of contentment, because of their ability to withstand good things
and bad things without becoming attached or repulsed by these things.
They feel much more comfortable in those people who are oppressed by greed and anger and delusion,
even living amongst the greatest of luxury.
And of course, whatever bedding, when the monks are sleeping on grass or so on.
And for all of us, of course, for the meditators, this is the same.
When we come here, many things are obviously not as wonderful or perfect as we would have otherwise expected in the outside world,
but we find that we're actually much more comfortable.
Whereas before we have thought we could never sleep on the floor,
or we could never do with just eating once a day or twice a day
or just eating, not being able to choose which food we get and so on.
This is the second thing he thought of.
The third thing he realized was that this teaching is only for those people who are content with seclusion.
It's not for those people who are fond of society.
This is one very important aspect of the Buddhist teaching.
It's that the only way you can really expect to progress in the meditation practice is if you're able to seclude yourself externally,
at least in the beginning, exclude yourself externally.
Of course, the higher levels, it's much more important to exclude yourself internally,
where no matter what comes, you're able to stay secluded from the filaments, from liking and disliking.
But in the very beginning, before we're able to do this, it's of the greatest importance.
That we don't talk with other people, chat with other people, sit around with other people,
and go here and go there with other people.
Some people are unable to be alone.
Even when they meditate, they have to meditate together.
This is an endemic of many meditation groups or practitioners.
When they're in a group, they can practice wonderfully, but when you put them alone,
they feel completely unconcentrated, unable to focus unhappy and so on.
They like this connection with other people.
This leads to interaction and the receiving of other people's conditions.
When they're upset, you get upset.
When they're distracted, you become distracted and so on.
At least in the beginning, when we're all beginner practitioners,
and we have all these bad and wholesome minds states,
we have to separate ourselves.
Just like a patient who is contagious, as a contagious disease,
they shouldn't go around other patients with other contagious diseases.
They use to share your diseases.
Once we've practiced and we've gone on to higher stages, then it becomes less of an issue.
But in the very beginning, it's something which is most important.
It's the reason why many people are forced to leave,
or they come out of it saying they didn't get anything.
Some people even say meditation is useless.
They don't see any benefit, but it's really because they're not meditating.
Their minds are not fixed or focused.
They're not paying attention to themselves.
They're constantly paying attention to the other people in the room,
either through conversation, through watching, through whatever connection.
Some people even use telephone, some contact in the outside room.
This is very dangerous.
If we can't cut these things off, it's difficult to see that we're going to actually progress in the Buddhist teaching.
So here is this monk.
He realized this.
This was the third thing that he realized.
The fourth thing he realized was that this teaching is for people who are energetic,
not people who are lazy.
So this is very important for meditators as well.
We can see fairly clearly even in the beginning that most of us have lazy tendencies.
We tend to have difficulty when we can't sleep long hours.
We have difficulty bringing ourselves again and again to have to walk and say to the monk and say,
we want to take breaks and so on.
We want to lie down during the day, we want to sleep many hours and so on.
But if we're not energetic, we can see that also that the meditation practice is very difficult for it to have any effect over us.
If we sleep a lot, sleep during the day, we find ourselves sluggish and we find ourselves unable to make this continuous mindfulness
and to keep going, uphill, keep progressing, keep developing in the meditation practice.
We find we're unable to really practice when we walk.
When we sit, we find our minds becoming distracted because we don't set ourselves to the task.
We don't have this level of energy which is required to keep the mind with the object.
We find ourselves looking here, looking there when we walk.
When we sit, we find ourselves moving and so on.
We're nodding and falling asleep and not really paying attention.
So this is one thing that we have to really work on in the practice.
We have to have the energy, the effort to keep ourselves with the object again and again because it's really,
what you can see is it's really not something that accumulates.
It's not like concentration whereas you practice more and more, you become more focused.
Mindfulness is something you have to create every moment.
It's not like you're mindful for a few moments and then that's going to carry on into the next moments.
It really doesn't work that way until you can change the way you look at things.
So that you're actually seeing things simply for as they are.
You can always go back to this state of unmindfulness of following after things.
And it's not this feeling some people get caught up in this feeling of increased concentration.
But mindfulness doesn't increase. It just is or it isn't.
Either you're mindful or you're not. There's no more mindful, less mindful.
It's not like concentration.
So this is why the necessity of a very different type of effort where you're always alert and reminding yourself
and bringing yourself back again and again.
It's very difficult actually.
You can find yourself mindful for a short time and then easily get off track.
So this is one of the very important things that this underwent.
I realized that you were sitting in meditation.
The fifth thing that he realized was that this practice, this teaching is for those people who are mindful,
not for those people who are unmindful with this goes hand in hand with the last one.
The energy that we're the effort that we're looking for is the effort to be mindful.
So this obviously means the same thing that you have to be mindful when we're practicing.
Your mind has to be very clear, very alert, very established.
If your mind is following after conditions or floating here and there, this is no good.
At the moment when your belly is rising, your mind has to know very clearly that this is rising.
When the belly is falling, if your nose is falling.
A little bit on what is mindfulness is also very helpful because the word mindfulness, of course, is not a direct translation.
A more direct translation is recollection or remembrance, something to do with memory.
The ability to remember, this is mindfulness.
If you want to set if you want a good translation, the translation is the ability to remember or remembrance or recollection.
This is borne out by the text, which say very clearly that good mindfulness is marked by the ability to remember things that happened in the past.
No matter how long and long it was, you can still remember.
People will say, oh, do you remember this and you'll be able to tell them exactly what happened.
If people start talking about the past, you'll be able to say no, no, it was like this, like this.
And they're amazed at how clearly clear it is in your mind and this is the ability to recall, the ability to send the mind to something and catch the truth about it.
But this is one type of mindfulness and this is really not very useful in the meditation practice because that doesn't tell you anything about the reality, which we're experiencing.
Our awareness of the past, of course, is a very different type of awareness from our awareness of the present because the past is all conceptual.
We could make it up, we could start thinking of, yeah, it was like this, but then we could say no, no, let's change that and let's say it was like this and suddenly we've got a new memory.
So we can alter it, in this sense, it's impossible to see it for what it is.
It's impossible to have it tell us something very meaningful.
We can't look at the past and say that's, well, that was impermanent, yeah, that was rising and ceasing.
It depends on our awareness in the present moment at that time.
So what we're talking about in meditation practice is remembrance of the present moment, remembering it for what it is.
So when the belly rises, the ability to remember it as rising, not to think of it as good or as bad,
or let our minds wander, letting our minds drift, follow after it.
And this comes up later at the end of this discussion.
I'll go into this a little bit later, but having the mind stay with the object and know it simply for what it is, not making more of it than it is.
This is the meaning of the word mindfulness.
So when we're unable to do this, when the mind wanders, when the mind follows after conditions and enjoys them,
or is disturbed by them, this is a sign of lack of mindfulness.
And this is not, this is dangerous.
It's not conducive to the meditation practice.
So it's very important that we are able to be mindful.
And if we expect to progress in the practice, it's necessary for us to be mindful.
This is what I know we're going to realize as the fifth Mahapurisa Ritaka.
The sixth was what he realized was that, yes indeed, you need concentration.
That one has to be focused.
We can't let our minds wander.
We can't be someone who is unable to stay fixed and focused, is unable to walk and keep the mind with the foot.
It's unable to sit and keep the mind with the stomach, for instance, or with any particular object.
If we're not focused, if we're distracted, there are many people coming to practice meditation,
and their minds are so distracted that they can understand why meditation is a good thing,
because they're sitting there, and their minds are just flying all over the place.
I think it's a product of the meditation.
The truth is, they've just never looked at their own mind.
If you watch them in daily life, they're actually much worse when they're not walking their city.
But these people have a difficulty in the meditation practice, and it doesn't mean that they can't practice.
It just means that they have to train themselves.
We have to adjust ourselves in all of these things.
If we're lazy, we have to become effortful.
If we have many wants, we have to give up our wants.
This is one of the most important first steps in meditation practices to give up all of our attachments,
at least in so far as we're able to.
To start to say to ourselves that this is not really as important as I think it is,
and say to myself, I sacrifice this, I let it go.
When we're able to do this, this is an incredible benefit for our practice, and so on.
All of these things that I've been discussing up into this number six, which is concentration.
The person who expects, again, benefit from the meditation practice has to be focused.
They have to have the mind with the present moment at all times.
They have to be focused on the present moment.
It doesn't mean we have to be focused on a certain object at all times.
This is a different type of concentration.
Because here in the present moment, what we realize is there's no object that lasts longer than a moment.
So if we're going to be focused, we have, in the present moment, we have to be focused on each individual object,
simply knowing it for what it is, not seeing it as something else or not, not letting ourselves follow after it.
This is number six.
The seventh and final thing which he thought about was that this practice or this teaching is for those people who are wise,
not those people who are unwise.
And there's something here to the idea that even before you start practicing, you need an element of wisdom.
You need some sort of wisdom before you can even start practicing.
I think this goes without saying that people who don't have this rudimentary level of wisdom find it very hard to even appreciate the meditation teaching.
These are the type of people who when they hear about meditation, they say, what's sitting still for an hour, what a useless thing to do.
Walking and sitting for days on end, I mean, what a waste of your life.
These are sort of people who don't yet get it. We don't yet see the suffering which they're creating for themselves, which they're building up in their daily lives and they're pursued after essential pleasures and so on.
The pursuit after whatever wanting to be rich, wanting to be famous, wanting to be this or that, whatever people have in their mind wanting to be productive or often just wanting to follow after the footsteps of their ancestors.
Doing what everybody else did, going to school, getting a job, getting old sick and dying.
And they think this is so important that nothing should get in the way and you can't imagine wasting your time doing something else, like just sitting there.
You have to see clearly that this sort of life or this sort of way of looking at life is really dangerous.
It's actually a cause for suffering and is a cause for us to get lost is actually a sort of useless way of living.
You get you go to school to get a job, you get a job to get money, but at the time you got money or old and then you get old sick and die.
It really has no meaning. That's what's useless. That's what's always to time.
And we start to think about things like suffering. We start to think about things like old age, sickness and death.
But even just this sort of level of suffering which exists, which is so pervasive in our everyday lives that we have to meet with so many unpleasant things.
You have to be able to see that this is the most important aspect of reality in the most important subject for our attention in order to appreciate the meditation.
Because the meditation is looking deeply at the present experience. It's not trying to gain something for the future.
So if you don't have this understanding of the importance in looking at the present moment or in changing yourself, in making yourself a better person in training the mind.
And if you're not able to see the problems that exist in your mind are the problem with clinging or the problem with sensual indulgence or over indulgence, then it's very difficult to come to practice meditation.
But another way of looking at this is simply that this meditation requires one to develop wisdom.
And we don't just sit here just to feel happy, just to feel comfortable. Some people are able to do that. They're able to sit for long periods of time, simply feeling peaceful and calm.
And we have many meditation techniques all over the world that teach simply this.
We have the called transcendental meditation or however, meditations that make you feel peaceful and calm or lead you to some nice state of being. But in the teaching of the Lord Buddha, this is not conducive to the fruit of the practice.
It's not the way which we're trying to proceed. If we simply just sit there peaceful and calm all the time, we never learn the things that are necessary for us to learn to attain the goal of Buddha's teaching, which is to let go, to become free, to never have anger, greed or delusion arise again.
And the way that we have these things never arise again, it's not simply through focusing the mind, it's through realization, realization that all of these things are dangerous and cause of suffering and so on.
And this realization, this is what we call wisdom. So it's the most important part of the Buddha's teaching really is wisdom.
And if we practice to the extent of getting concentration, getting focused, but we don't yet understand, we don't yet see the things in front of us as impermanence, suffering and non-self, then we can never lead to the consummation of the Buddha's teaching, which is remotely, which is freedom or release.
And so this is the realization which I don't want to come up with.
Once he thought they're thought about thinking about these seven things, they say the Lord Buddha was able to read his mind and knew what he was thinking about.
This is a common sort of ability of those people who practice meditation all of their lives and never leave the forest and so on.
And when you practice for a long time, if you were to live in a cave or years and years and years, you could eventually, your mind becomes so subtle that you're able to appreciate the vibrations around you to the extent that you can read people's minds.
And this is what they say about many of, even the Buddha and many of his disciples, and you can even find these people nowadays, I've heard about monks who they're sitting giving a talk and one of the students.
There was a story about a monk, one of the students looked at the teacher and said, boy, he looked or said to themselves, thought to themselves, boy, our teacher looks just like a fish.
And the teacher was sitting there and he's giving the talk and he says, you know, suppose there was someone who was sitting there and thought their teacher looked just like a fish.
He used it as an example. The student was like, oh, why?
They realized the teacher could tell what they were thinking. This you get sometimes.
But it isn't like it's necessary to read people's minds to be a teacher.
And it's not really difficult to understand how it happens because most of us have this ability to some extent anyway.
You know, even when we look at someone's face, we can tell what they're thinking to some extent.
When we look at their body language, we might be sitting there and suddenly they fidget, you can read them.
You can read things from people's body language. This is sort of the same sort of idea, except it's on a much deeper level to the point where you can catch even even thought process.
But anyway, the Buddha, of course, was perfectly developed in this and so he came to Anurudhana and he wasn't going to let him get off the hook just with these seven and he added an eight.
And it's quite interesting that he did so. He sort of wonder whether it was just kind of saying, you know, hey, stop thinking.
It's not, stop sitting there thinking. This isn't the way to practice because Anurudhana was obviously sitting there ruminating over this for some time.
And the Lord Buddha gave him the eighth one and he said, great, good for you. These seven things are correct. They're very well said.
But I got an eighth one for you. And he said number eight is that this dhamma is for people who delight in non-diversification, not those people who delight in diversification.
So diversification here is just a translation that's the only one that seems appropriate. Bapancha is the word.
It refers to those states of mind which make more of something than it actually is. And this, of course, was back to what I was saying earlier.
That we often make more of some, or we ever and again make more of something than it actually is. I mean, simply saying that something is good or bad is making much more of it than it actually is.
Now, in this case Anurudhana, you know, as he was thinking, you know, there are obviously arises many mind states because he's not really being mindful. He's sitting there, you know, formulating this sort of system or this set of ideas.
And the Buddha gave him an eighth one. He said, you know, basically getting back to practice. But it's, of course, the most important of all of these qualities. And, you know, all eight of these are incredible, are excellent guidelines. This isn't, it isn't to say that Anurudhana was in anywhere wrong.
The Buddha just wanted to give him the, so it seems he just wanted to give him the tool that was necessary to get him back on track or to get him to the next level.
He said, above and beyond all of these things, the most important thing is that we don't make more of things than they actually are.
So, when we sit in meditation, whatever comes up, we simply realize it for what it is.
And this is where people always get into trouble, especially near the end of the course. As they start to practice, they start to interpret things.
I was terrible about this when I first started. I had this feeling of a, of a line in the middle of my body and I started to think, yeah, that's the middle way that they're talking about.
So, I was pushing myself to try to get to this very center of my body. It's kind of a ridiculous to think about, but you get crazy as you'd want to practice.
You know, he's just starting, you don't have, nobody's giving you much information and you can attach and make so much more of things than they actually are.
This is very common, especially as we put you further and further into the practice and have you practice more and more.
So, this is the most important is that we, we remember, we realized that we remember things as we've instructed the meditators, that when you see something, it's only seeing.
When you hear something, it's only hearing, when you smell, it's only smelling, when you taste, it's only tasting. And there's no exception.
Whatever you see, it's just simply seeing. Now, it's funny how, no matter how many times we impress this on people, there's always somebody, or most people will always come back and say, oh, I think I realized it.
Now, what was it? I saw this, or I saw that, and you just think, you know, whatever I've been telling you this whole time, it's just a sight, it's not anything special whatsoever.
Or they sit there and they feel some new feeling. And, you know, whatever you feel, it's just feeling. There's nothing special.
This is what we're trying to realize. There's nothing special. There's nothing in this whole universe that's special.
If you can't realize this, you've got a, you've got a block in your practice and you will never be able to get to the core of the Buddhist teaching.
So, this is what is meant by Papancha, Nipapancha, and Papancha, Nipapancha means not making more of things than they actually are. If it's a feeling, it's a feeling, it's a sight, it's a sight.
It's a thought, it's a thought, whatever you think. When you think to yourself, you know, this practice is terrible or sucks or something, this is just a thought.
If you think to yourself, you know, here I am sitting at one person say, here I am practicing meditation, and it's supposed to be for the end of suffering,
and all I do is suffer more and more. This is just, you know, these thoughts that arise and we get caught up in this idea that, yeah, meditation must be bad because I'm suffering when I sit in meditation, and I'm supposed to find that we get all these logic circuits working us on.
And if we're not careful, we can really trick ourselves and get into this.
Sometimes I say something, and people take it to me more than it actually means, and suddenly they're going, you know, like I say, there's teachers who can read people's minds and suddenly everything.
So is he reading my mind? Oh, is he saying that because he can read my mind? This is a very clear example.
In fact, it's often the case from what I've seen, even back in Thailand, where one of our great teachers, he says something, and everybody in the whole room thinks he's talking to them.
He thinks he's read their mind and he's giving them some instruction, every single person in the room.
And it's really kind of humorous to see a room full of people with everybody thinking, yeah, he's talking to me.
It actually does say something about his teaching ability that's true, but I don't really think he's just going on reading everybody's minds and giving them piece by piece information.
You know, it's possible, but I think more often than not, we interpret things and make more of things than they actually are.
And this is most important in the meditation practice. When you feel pain, it doesn't mean your leg is going to fall off.
It doesn't mean that meditation practice is bad. It means something has arisen and it has the name pain, and if you don't get that, you'll never get through the Buddha's teaching.
You'll never get to the core of the Buddha's teaching. If you can't get this simple truth, it is what it is.
And so this is what we're practicing. And as we practice on and on, we slowly realize this. That's just what it is. It's pain.
End of story. That's it.
And this was the eighth item which the Lord would have gave to Anaruda, and as a result, the end of the story is that Anaruda as a result became an arrow hunt through this practice, through the practice of non-diversifying, not making more of things than they actually are.
There's a little bit more in there. First, the Buddha goes back and tells all the other monks and explains it to that, explains the same things to them, and goes into a little more detail.
I've covered it more mostly. I don't think there's any to do any further, and we've gone long enough now.
So, invite everyone to continue practicing. First, we do mindful frustration, then walking and then sitting.
And then we do have reporting and starting at three o'clock.
