1
00:00:00,000 --> 00:00:06,000
Okay, sometimes during the meditation there are very short thoughts which when noticed disappear

2
00:00:06,000 --> 00:00:11,520
very quickly, should one still stop and pronounce to one's staff thinking or just move

3
00:00:11,520 --> 00:00:17,160
on with the breath, absolutely stop and say to yourself thinking, we never are mindful

4
00:00:17,160 --> 00:00:21,120
we cannot possibly be mindful of something as it occurs.

5
00:00:21,120 --> 00:00:26,880
The moment after it occurs is when we are mindful of it, when we are aware of it.

6
00:00:26,880 --> 00:00:30,760
So in that moment we say to ourselves thinking and it doesn't even have to be quite

7
00:00:30,760 --> 00:00:33,240
so technical as that.

8
00:00:33,240 --> 00:00:38,520
Even if even once you know that it's disappeared, you can say you can acknowledge something

9
00:00:38,520 --> 00:00:42,880
to yourself, you can say to yourself knowing that it's disappeared, you can say to yourself

10
00:00:42,880 --> 00:00:46,440
disappearing, knowing that it's disappeared, you can say to yourself thinking, knowing

11
00:00:46,440 --> 00:00:48,400
that there was thinking that it's disappeared.

12
00:00:48,400 --> 00:00:55,600
The Buddha said, Samudya Dammanupasi wyadammanupasi wyadammanupasi wyadati.

13
00:00:55,600 --> 00:01:01,680
He is aware of the arising, of the object or he is aware of the ceasing of the object.

14
00:01:01,680 --> 00:01:06,800
Either one is considered mindfulness and why you would even bother is because if you don't

15
00:01:06,800 --> 00:01:14,560
bother they are going to arise, or there are the potential to arise, views, conceits,

16
00:01:14,560 --> 00:01:19,600
cravings these three things that will arise at the time when you're not clearly aware of the object

17
00:01:19,600 --> 00:01:26,400
So you'll have the idea that this is my thought or this is I that is thinking you'll have the the conceit

18
00:01:26,400 --> 00:01:28,480
That was a good thought or a bad thought or I

19
00:01:29,200 --> 00:01:36,320
Yeah, I am this thought or so on and you'll have craving the idea that that was a that you you like liking the thought or

20
00:01:36,320 --> 00:01:39,520
Just liking the thought you'll be happy about it or upset about it

21
00:01:39,520 --> 00:01:45,600
so for sure rather than and and at the very least you have this doubt or eyes should I shouldn't I

22
00:01:45,600 --> 00:01:47,600
You know, did I do it right or so on?

23
00:01:47,680 --> 00:01:51,920
Just take it as a rule that if you know that you were thinking at least say to yourself knowing knowing

24
00:01:52,480 --> 00:01:54,480
Becoming aware of the knowing

25
00:01:54,800 --> 00:01:58,880
You'll find that that helps very much in the beginning and eventually

26
00:02:01,120 --> 00:02:06,080
You're you're able to be more more precise so that when you know the thought or eyes as you catch it right away

27
00:02:06,080 --> 00:02:10,080
It's thinking thinking and then it disappears and that said you only have to say thinking once

28
00:02:10,640 --> 00:02:14,720
Just to know that you're thinking or say it a couple of times to know that it's common gone and so on

29
00:02:15,200 --> 00:02:19,680
The idea is as I said that it fixes the mind and keeps the mind

30
00:02:20,720 --> 00:02:22,720
with a clear thought

31
00:02:22,880 --> 00:02:24,880
the the

32
00:02:25,760 --> 00:02:29,600
The acknowledgement is only a replacement for thought. It's not an addition

33
00:02:30,080 --> 00:02:31,920
Because if you don't as I said if you don't

34
00:02:31,920 --> 00:02:36,960
Then they're going to rise other thoughts. This is me. This is mine. This is good. This is bad. And so

35
00:02:40,480 --> 00:02:42,480
Yes

36
00:02:42,480 --> 00:02:46,960
But the more you meditate the the more the mind comes down and

37
00:02:48,480 --> 00:02:50,000
the these

38
00:02:50,000 --> 00:02:59,360
Short millisecond thoughts are not coming up so often anymore, so it will get much easier by the time and

39
00:02:59,360 --> 00:03:07,600
You will have more time to note thoughts and so on because there's just not so much coming up anymore

