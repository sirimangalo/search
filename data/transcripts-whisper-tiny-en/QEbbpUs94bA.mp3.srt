1
00:00:00,000 --> 00:00:14,080
Good evening, everyone, broadcasting live May 27th.

2
00:00:14,080 --> 00:00:27,760
Today's quote is about the Buddha, about how radiant and clear is his complexion.

3
00:00:27,760 --> 00:00:33,760
Just as a trink in a bread-gold, shines and glitters.

4
00:00:33,760 --> 00:00:37,760
So too the good-go-go senses are calmed.

5
00:00:37,760 --> 00:00:46,560
And his complexion is clear and radiant.

6
00:00:46,560 --> 00:00:58,240
So in and of itself it's a praise of the Buddha, and the most obvious benefit of this

7
00:00:58,240 --> 00:01:06,160
quote is to encourage reverence for the Buddha, which is good in and of itself.

8
00:01:06,160 --> 00:01:15,880
It's good to revere great people, it's good to respect them, it's good even to potentially

9
00:01:15,880 --> 00:01:19,800
bow down for the Buddha, which is a common thing for Buddhists to do.

10
00:01:19,800 --> 00:01:29,560
These are good things, they cultivate humility, respect, reverence, appreciation, they help

11
00:01:29,560 --> 00:01:39,520
you take the practice, the teaching seriously, when you think about the Buddha and how

12
00:01:39,520 --> 00:01:47,440
wonderfully it was, it's a good thing for your practice.

13
00:01:47,440 --> 00:01:55,600
I think what's more interesting is why, why is the Buddha so radiant?

14
00:01:55,600 --> 00:02:02,360
What is it about a Buddha, as opposed to another religious teacher that we would argue

15
00:02:02,360 --> 00:02:10,440
or say who claim makes him more radiant, or what makes him special, what makes him radiant

16
00:02:10,440 --> 00:02:13,680
more than an ordinary person?

17
00:02:13,680 --> 00:02:17,600
And that we find in the Buddha's teaching, and that's more interesting because then it

18
00:02:17,600 --> 00:02:20,800
is something that we can actually emulate.

19
00:02:20,800 --> 00:02:36,880
We emulate the practices and behaviors, minds, states that need one to be radiant.

20
00:02:36,880 --> 00:02:43,200
That's more of a deeper benefit, and simply praising the Buddha, because of course that's

21
00:02:43,200 --> 00:02:50,480
quite limited, some people that's all they do is, rather than working to better themselves,

22
00:02:50,480 --> 00:02:53,080
they praise people who are better than themselves.

23
00:02:53,080 --> 00:02:57,760
I mean, not even in religion, this is a common thing in the world.

24
00:02:57,760 --> 00:03:03,040
People use it as a defense mechanism when someone is great, when someone is exceptional

25
00:03:03,040 --> 00:03:10,240
at something, they use it as a defense mechanism to praise them.

26
00:03:10,240 --> 00:03:17,560
It is a way of avoiding actually emulating them or keeping up with them, or you're such

27
00:03:17,560 --> 00:03:27,720
a good person, while that means you're not going to become a good person, and that's

28
00:03:27,720 --> 00:03:33,480
actually the thing, oh, you're so nice, so wonderful.

29
00:03:33,480 --> 00:03:44,160
Basically saying, I have no intention of becoming such a nice person, if I never marketable.

30
00:03:44,160 --> 00:03:50,880
As we may be wish, we could be like that person, but just the idea of praise is limited

31
00:03:50,880 --> 00:03:57,280
and limiting and can be potentially limiting if we rely upon praise of the Buddha, we worship

32
00:03:57,280 --> 00:04:02,480
the Buddha, he's so great, and then people say things like, you can't possibly see

33
00:04:02,480 --> 00:04:08,160
me banning yourself, Iran is not something a human being, or I had one man tell me, human

34
00:04:08,160 --> 00:04:17,440
beings can't attain me banning, he said it's like, no, I wouldn't say he said, oh,

35
00:04:17,440 --> 00:04:29,280
you Westerners, you look at the moon, and you think to go to the moon, and I said, well,

36
00:04:29,280 --> 00:04:50,440
we did actually go to the moon, the idea is to emulate, not simply review, emulate

37
00:04:50,440 --> 00:04:55,280
the enlightened ones, and the Buddha said, if you want to respect the Buddha, what's the

38
00:04:55,280 --> 00:04:59,480
greatest respect is to practices teaching?

39
00:04:59,480 --> 00:05:04,200
What are these great teachings that lead one to be radiant?

40
00:05:04,200 --> 00:05:11,240
The one that's most directly related to radiance and to this clear complexion is staying

41
00:05:11,240 --> 00:05:17,360
in the present moment, of course, that describes very much the core practice of insight

42
00:05:17,360 --> 00:05:25,200
and meditation, but this description of being rooted in the present moment.

43
00:05:25,200 --> 00:05:38,200
And he uses language relating to plants and how they're rooted in the ground, so he talks

44
00:05:38,200 --> 00:05:45,080
about grass, and when you cut grass, it gets cut off from its source of energy and

45
00:05:45,080 --> 00:05:56,800
whithers up and dies, and likewise, this is, I'm very apt analogy because likewise, the

46
00:05:56,800 --> 00:06:04,000
mind that's caught up in the past or the future gets cut off from reality and loses its

47
00:06:04,000 --> 00:06:20,880
energy and it whithers up, dries up, so people who are poor complexion, the sense of

48
00:06:20,880 --> 00:06:33,560
aged and wrinkled, and so beyond normal, people whose bodies become sick and chronically

49
00:06:33,560 --> 00:06:45,400
unhealthy, it's for many causes, but one cause is not being present, but it was asked

50
00:06:45,400 --> 00:06:51,240
why, how is it that these monks who only eat one meal a day, how can they be radiant?

51
00:06:51,240 --> 00:06:55,120
How can they look so healthy?

52
00:06:55,120 --> 00:07:01,920
So young, if they're not eating full meals, sometimes they don't get much to eat, how can

53
00:07:01,920 --> 00:07:10,440
they still look so radiant, young is what the Buddha said, it's because they don't, for

54
00:07:10,440 --> 00:07:16,080
the past they do not mourn or for the future wheat, they take the present as it comes

55
00:07:16,080 --> 00:07:29,560
and thus their color keeps, it's from the Jataka.

56
00:07:29,560 --> 00:07:33,280
So everything that we experience occurs in the present moment, you don't have to go looking

57
00:07:33,280 --> 00:07:40,920
for it, you just have to remind yourself that you're here now, when you think about the

58
00:07:40,920 --> 00:07:45,800
past, remind yourself it's just a thought and it's present, you worry about the future

59
00:07:45,800 --> 00:07:52,360
remind yourself, this is worry and this is thought, this is planning, this is remembering,

60
00:07:52,360 --> 00:07:56,880
even remembering and planning, they all happen here and now, to remind yourself otherwise

61
00:07:56,880 --> 00:08:02,640
you create the concept of future and past and you're lost, you're in the realm of

62
00:08:02,640 --> 00:08:09,040
concepts, no longer in the realm of reality, you create a new universe, it's not here

63
00:08:09,040 --> 00:08:19,120
and now, it's completely illusory and you can feel the difference, you might argue

64
00:08:19,120 --> 00:08:28,160
well, that's useful and so on, argue as you may, we suffer from living in the past and

65
00:08:28,160 --> 00:08:35,160
the future, you can feel it viscerally, you lose energy, you're weighted down, you're tired,

66
00:08:35,160 --> 00:08:45,120
you get sick, all of our desires and the versions, they come from this realm of concepts,

67
00:08:45,120 --> 00:08:55,520
the reality is not cleanable, if you're living in reality, there's no quality to it that

68
00:08:55,520 --> 00:09:23,520
is appealing or displeasing, so we live in the present moment, this is what we try to do

69
00:09:23,520 --> 00:09:29,400
when we meditate, we try to just be here now, plain and simple, not judge it, not reacts

70
00:09:29,400 --> 00:09:38,280
with, it's not easy, so it's becoming a new habit, so it takes training, you don't

71
00:09:38,280 --> 00:09:49,680
take practice, but that's what we do and then we shine as well, we become more radiant,

72
00:09:49,680 --> 00:10:02,760
still, we've been done enough for today, everybody's waving at me, we've got a couple of

73
00:10:02,760 --> 00:10:09,680
questions here, do you have to see impermanence suffering in non-self, moment to moment or

74
00:10:09,680 --> 00:10:21,200
just simply knowing and understanding impermanence suffering in non-self, moment to moment satisfy the practice.

75
00:10:21,200 --> 00:10:31,280
Knowing and seeing are the same, sounds like you're maybe referring to intellectual knowledge,

76
00:10:31,280 --> 00:10:38,760
theoretical knowledge, like, hey, I was happy yesterday and I'm not happy today, that's

77
00:10:38,760 --> 00:10:45,480
impermanence or something like that, knowing and seeing, or knowing comes from seeing, when

78
00:10:45,480 --> 00:10:53,320
you see you know, you know because you see and it's not something you have to do, it's

79
00:10:53,320 --> 00:11:00,720
something that happens, what you have to do is be present and be only present and you'll

80
00:11:00,720 --> 00:11:07,320
see things arising and ceasing, they're impermanent, means they're not stable, they're

81
00:11:07,320 --> 00:11:13,800
stuck, which means they're not satisfying, because they're unstable, they can't claim that

82
00:11:13,800 --> 00:11:21,480
that's a cause of happiness, like that's going to satisfy and you get control them, they don't

83
00:11:21,480 --> 00:11:26,720
belong to you, they don't have any entity of their own, their ephemeral, their lives and

84
00:11:26,720 --> 00:11:31,440
next things.

85
00:11:31,440 --> 00:11:35,640
Once you understand and know suffering impermanence and non-self, what is the next part

86
00:11:35,640 --> 00:11:43,680
of the practice, meditation, it only takes a moment, if you have one moment of pure understanding

87
00:11:43,680 --> 00:11:49,680
of impermanence or suffering or non-self, one or the other you'll see it clearly, so clearly

88
00:11:49,680 --> 00:11:58,920
the next moment is the cessation, nibana, so there isn't really a next step, the next step

89
00:11:58,920 --> 00:12:04,360
happens by itself.

90
00:12:04,360 --> 00:12:10,360
Is it natural for fear to arise at times when sees non-control?

91
00:12:10,360 --> 00:12:22,680
Ah, absolutely, yes, it's called bionyana, not exactly, bionyana is you see the fearsomeness,

92
00:12:22,680 --> 00:12:29,480
the fear that comes from it is problem, fear is based on anger, dosa, so you have to acknowledge

93
00:12:29,480 --> 00:12:36,320
it, afraid, afraid, but it's called, of course, common once you see how terrifying

94
00:12:36,320 --> 00:12:40,920
it is, how unstable everything is, it's like having the rug pulled out from under

95
00:12:40,920 --> 00:12:49,600
you're having your whole foundation shook, because we think of our lives as, I can control

96
00:12:49,600 --> 00:12:55,280
this and then we realize, ah, I can't control this, that's scary, but the knowledge is

97
00:12:55,280 --> 00:13:00,360
not afraid, it's not, it's not important to be afraid, it's not good to be afraid, it's

98
00:13:00,360 --> 00:13:09,640
just a reaction that we have to work on, and overcome.

99
00:13:09,640 --> 00:13:14,360
When a non-enlightened person not seeing as seeing hearing is hearing, is it still karma?

100
00:13:14,360 --> 00:13:23,360
Yes, it's kusala karma, kusala kamba, it's nyanasampa uta as well, so it's associated

101
00:13:23,360 --> 00:13:37,640
with knowledge, that's karma and the best, best, of the best sort, karma that gets

102
00:13:37,640 --> 00:13:45,720
you closer to seeing the truths and letting go, realizing the environment, what is the role

103
00:13:45,720 --> 00:13:51,680
of prayer and spiritual practice, if you're praying to another being that's delusion, that's

104
00:13:51,680 --> 00:13:57,360
what role it has, but if you're making a determination for something, may this happen, may

105
00:13:57,360 --> 00:14:05,080
that happen, that's a good way of setting yourself, and it's also, it actually changes

106
00:14:05,080 --> 00:14:10,800
the universe, it changes your mind, it changes your surroundings, if you have very strong,

107
00:14:10,800 --> 00:14:16,880
a very strong mind, you can actually change things with your determination, may it be

108
00:14:16,880 --> 00:14:23,720
thus, may it be thus, may it not be, and you can actually affect the universe, this is how

109
00:14:23,720 --> 00:14:30,640
some meditators, some meditation practices, they actually practice to manifest things,

110
00:14:30,640 --> 00:14:36,560
like may I be rich, may I be this or that, because we don't do that, but they seem to have

111
00:14:36,560 --> 00:14:43,640
some results in getting what they want through the power of mind, the power of determination.

112
00:14:43,640 --> 00:14:48,200
I'm making a wish, may I be happy, may other means be happy, my parents be happy, can

113
00:14:48,200 --> 00:14:53,800
really affect them, it can change reality, we have a strong enough determination, so

114
00:14:53,800 --> 00:15:02,160
we call it determination at ithana.

115
00:15:02,160 --> 00:15:06,560
If you hear a voice clearly, is it important to note all the change and the voice happening

116
00:15:06,560 --> 00:15:14,760
in that particular moment? No, just hearing hearing, you don't have to worry about the

117
00:15:14,760 --> 00:15:24,480
particulars, you'll see all the particulars anyway, but what you have to remind yourself

118
00:15:24,480 --> 00:15:29,360
is that that's just hearing, it's nothing else, it's not good, it's not bad, it's not

119
00:15:29,360 --> 00:15:54,720
bad, it's not bad, good number of people tonight, most of you are a green, which is good,

120
00:15:54,720 --> 00:16:01,720
green means you've meditated recently, appreciate that.

121
00:16:25,720 --> 00:16:35,720
Okay, no other questions, we will stop there, tomorrow I'm not, I don't think I'll

122
00:16:35,720 --> 00:16:41,120
be broadcasting tomorrow, it's a long day tomorrow, with this big thing in Mississauga,

123
00:16:41,120 --> 00:16:56,920
so I'm just taking a break, we'll see, I don't think so, anyway, have a good night everyone.

