1
00:00:00,000 --> 00:00:05,060
My understanding of the Buddhist conception of love is that it is a kind of

2
00:00:05,060 --> 00:00:10,020
delusion and attachment with attendant negative connotations. If this is the case

3
00:00:10,020 --> 00:00:14,080
how could we characterize the emotion motivation which impels a bodhisattva to

4
00:00:14,080 --> 00:00:23,680
be reborn? Love is not delusion and again back to the Abidhamma love is a word

5
00:00:23,680 --> 00:00:32,760
and is ambiguous because most often in the West we're referring to Eros,

6
00:00:32,760 --> 00:00:40,440
Eros which Eros which is lustful love or not even lustful but just desire for

7
00:00:40,440 --> 00:00:46,840
something. I love apples or I love my wife or my girlfriend or my boyfriend

8
00:00:46,840 --> 00:00:55,280
or I love my children, adoration and so on, is often unwholesome not

9
00:00:55,280 --> 00:01:05,080
delusion based on delusion but is craving or greed based, desire based and so

10
00:01:05,080 --> 00:01:11,600
that's unwholesome but love that is desire for beings to be happy, wishing for

11
00:01:11,600 --> 00:01:20,440
them to be happy or acting. The impulsion that impulse push moves us to act for

12
00:01:20,440 --> 00:01:25,920
the benefit of others. This is Mehta and this is a positive state, it's wholesome.

13
00:01:25,920 --> 00:01:29,880
It's worldly, it won't get you to Nibana but it's wholesome and if

14
00:01:29,880 --> 00:01:35,360
cultivated can eventually lead to limitless love for the whole universe and

15
00:01:35,360 --> 00:01:40,640
which can lead you to be born as Abramla and can give great states of

16
00:01:40,640 --> 00:01:44,840
concentration which are also beneficial to the practice of insight

17
00:01:44,840 --> 00:01:51,960
meditation. So it depends what you mean by love but the emotion motivation

18
00:01:51,960 --> 00:01:59,000
that impels the bodhisattva to be reborn. The reason a bodhisattva in the

19
00:01:59,000 --> 00:02:04,000
terror of attradition is reborn and I'm assuming you're talking about the

20
00:02:04,000 --> 00:02:07,880
terror of attabodhisattva without the V because with the V in there it gets

21
00:02:07,880 --> 00:02:16,400
kind of weird and Mahayana conception gets pretty radical but what impels a

22
00:02:16,400 --> 00:02:20,920
bodhisattva to be reborn again is delusion and defilement. The only reason

23
00:02:20,920 --> 00:02:25,560
we're reborn is because there's some kind of attachment. Now although the

24
00:02:25,560 --> 00:02:35,040
bodhisattva has done is refused to listen or to accept an enlightenment that

25
00:02:35,040 --> 00:02:43,360
is that is void of omniscience. So given the opportunity to become enlightened

26
00:02:43,360 --> 00:02:50,800
in this life without being omniscient they discard it. There's that. Now

27
00:02:50,800 --> 00:02:58,580
what moves them to do it? Again so now we have to distinguish between

28
00:02:58,580 --> 00:03:03,360
bodhisattas. Some bodhisattas are not going to become Buddhas and they make a

29
00:03:03,360 --> 00:03:08,480
determination to become a Buddha and are unsuccessful and often these

30
00:03:08,480 --> 00:03:15,280
determinations that they make are based on ego or desire or whatever. In the

31
00:03:15,280 --> 00:03:21,120
case of Arbuddha he made a vow out of I guess you could say wisdom and

32
00:03:21,120 --> 00:03:27,960
confidence and faith because he was a highly developed aesthetic who had

33
00:03:27,960 --> 00:03:34,680
magical powers and insight and he knew with certainty with confidence that if

34
00:03:34,680 --> 00:03:37,720
he listened to the Buddha's teaching he would become an Arhan very

35
00:03:37,720 --> 00:03:45,760
quickly right away and so he made the determination to go one step further to

36
00:03:45,760 --> 00:03:52,880
go the next step which was a huge step out of wisdom and understanding. It's

37
00:03:52,880 --> 00:03:56,240
interesting curious to note that as soon as he became a Buddha one of the first

38
00:03:56,240 --> 00:04:02,040
things he thought of was not taking advantage of his omniscience. It was to

39
00:04:02,040 --> 00:04:06,760
pass away into Parinibhana which you know he would have saved himself a lot of

40
00:04:06,760 --> 00:04:15,040
time and he just done that back in the time of Department of Buddha. But the

41
00:04:15,040 --> 00:04:19,600
result but the result of his omniscience was such that he wasn't able to do it

42
00:04:19,600 --> 00:04:24,040
because immediately he was asked to teach Brahma came down and asked him to

43
00:04:24,040 --> 00:04:28,360
teach because he was omniscient no one would come down and ask and Brahma would

44
00:04:28,360 --> 00:04:32,520
not come down and ask me to teach for example because I'm not omniscient but

45
00:04:32,520 --> 00:04:36,520
he came down and asked the Buddha to teach and so it was based on this quality.

46
00:04:36,520 --> 00:04:42,640
What motivated it and so again this is it differs from from person to person but

47
00:04:42,640 --> 00:04:47,960
the real bodhisattas it would seem they do it out of confidence and wisdom on

48
00:04:47,960 --> 00:04:53,680
that occasion where they are recognized and where the another Buddha says see

49
00:04:53,680 --> 00:04:56,640
that guy is lying in the mud and he's going to become a Buddha in the future

50
00:04:56,640 --> 00:05:18,080
life and so.

