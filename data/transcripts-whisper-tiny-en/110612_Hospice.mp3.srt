1
00:00:00,000 --> 00:00:04,800
So welcome back to Ask a Monk.

2
00:00:04,800 --> 00:00:14,080
Today I will be talking about dealing with terminally ill patients, hospice care and some

3
00:00:14,080 --> 00:00:24,880
of the ways that I would think one can approach people in this condition and help them

4
00:00:24,880 --> 00:00:28,720
through the practice of the meditation.

5
00:00:28,720 --> 00:00:40,480
The first thing to note or to discuss is in regards to general practices in terms

6
00:00:40,480 --> 00:00:47,240
of teaching meditation, whether it's terminally ill, it doesn't just relate to this specific

7
00:00:47,240 --> 00:00:52,440
group of people, but in any case when you're teaching meditation there are certain

8
00:00:52,440 --> 00:00:57,000
principles that you have to keep in mind.

9
00:00:57,000 --> 00:01:03,760
The first one is that anytime you're teaching meditation you should also be mindful yourself

10
00:01:03,760 --> 00:01:13,480
and that's probably the most important principle to keep in mind that it's not simply

11
00:01:13,480 --> 00:01:23,400
or it's not good enough to simply know the principles and to have book theory in order

12
00:01:23,400 --> 00:01:29,480
to teach in order to be effective, in order to really be able to respond and interact

13
00:01:29,480 --> 00:01:36,360
with the patient, the student, one has to be practicing.

14
00:01:36,360 --> 00:01:41,680
So at the time when you're teaching, at the time when you're speaking even, you should

15
00:01:41,680 --> 00:01:45,640
try to remind yourself of the things that you're saying.

16
00:01:45,640 --> 00:01:55,800
Otherwise you fall into all sorts of biases and emotions and you become stressed yourself

17
00:01:55,800 --> 00:02:04,760
when the person doesn't respond favorably or you can get caught up in your own emotions,

18
00:02:04,760 --> 00:02:15,520
go overboard, you can become overconfident and overbearing and it becomes difficult to remain

19
00:02:15,520 --> 00:02:19,880
objective and impartial and in part the truth.

20
00:02:19,880 --> 00:02:25,680
I mean the truth is something that is very difficult to understand and very difficult

21
00:02:25,680 --> 00:02:33,840
to keep in mind, it's easy for us to slip off into illusion and fantasy and give

22
00:02:33,840 --> 00:02:39,440
the wrong advice when we're teaching we want to give something useful, we want to be

23
00:02:39,440 --> 00:02:47,600
appreciated and so if we're not careful, we can easily slip into bias and prejudice

24
00:02:47,600 --> 00:02:55,480
and find ourselves just saying something that sounds good or that it's going to give

25
00:02:55,480 --> 00:03:02,080
a favorable response from our listeners which you may not always be in line with the

26
00:03:02,080 --> 00:03:03,080
truth.

27
00:03:03,080 --> 00:03:10,120
Something that sounds good, a field good sort of philosophy, it's not good enough and that

28
00:03:10,120 --> 00:03:17,880
leads to the second point that in general I would recommend for anyone who's going to teach

29
00:03:17,880 --> 00:03:21,360
meditation besides practicing by yourself.

30
00:03:21,360 --> 00:03:30,000
Have it clearly in mind that your approach to it should be a meditation, it should be

31
00:03:30,000 --> 00:03:36,120
a meditative interaction with the person and the only thing that you're giving to the

32
00:03:36,120 --> 00:03:39,120
person should be information.

33
00:03:39,120 --> 00:03:45,200
You shouldn't be giving them yourself, it shouldn't be about you or your guru's status,

34
00:03:45,200 --> 00:03:54,920
you shouldn't be giving them a image or a presentation of some charismatic or...

35
00:03:54,920 --> 00:04:09,480
...impressive personality, the whole idea of a guru ship or impressing something

36
00:04:09,480 --> 00:04:18,920
on your students is quite dangerous and it doesn't have the desired effect, you can impress

37
00:04:18,920 --> 00:04:27,920
someone emotionally and I've tried this or if you try it you can see for yourself the

38
00:04:27,920 --> 00:04:32,000
results, the result is people have great faith in you but they don't really understand

39
00:04:32,000 --> 00:04:33,160
what you're saying.

40
00:04:33,160 --> 00:04:41,560
They put more emphasis on the messenger rather than the message and so it's easy to get

41
00:04:41,560 --> 00:04:48,480
caught up in this as well, giving yourself as the message and pumping people up and giving

42
00:04:48,480 --> 00:04:53,720
them confidence and encouragement but not really giving them any skills.

43
00:04:53,720 --> 00:05:01,280
Much more useful than giving an image or confidence or encouragement and being a support

44
00:05:01,280 --> 00:05:05,840
for people to lean on is helping them be a support for yourself, that's I guess the third

45
00:05:05,840 --> 00:05:11,560
thing I would say is that you're not there to be a support for them, you're there to

46
00:05:11,560 --> 00:05:19,160
teach them how to support themselves which is very often overlooked, we're much better

47
00:05:19,160 --> 00:05:24,360
at or much more inclined to be a support for others and have them lean on us and depend

48
00:05:24,360 --> 00:05:31,440
on us which as I'm going to explain is really a part of the problem, the dependency.

49
00:05:31,440 --> 00:05:37,440
Once they can be strong which is really the problem that we face because we've gone

50
00:05:37,440 --> 00:05:44,120
our whole lives without building ourselves up without strengthening our minds and giving

51
00:05:44,120 --> 00:05:50,080
ourselves the ability to deal with difficulty and so when it comes we're still children,

52
00:05:50,080 --> 00:05:56,280
we act like young children, we can cry at grown adults, we'll cry and moan and just

53
00:05:56,280 --> 00:06:03,080
want to take drugs and medication and so on.

54
00:06:03,080 --> 00:06:10,520
What you should be there to give is information, now the point about meditating is that

55
00:06:10,520 --> 00:06:15,320
you'll give the right information and the information you give will be unbiased, impartial

56
00:06:15,320 --> 00:06:23,800
and will be a service to the person like you're there as a book or you're there as a kind

57
00:06:23,800 --> 00:06:30,720
of like a coach, someone to stop you and you're going in the wrong direction and push

58
00:06:30,720 --> 00:06:33,640
you in the right direction and encourage you.

59
00:06:33,640 --> 00:06:39,840
But because you're mindful, because you're there, you're present, you're in the present

60
00:06:39,840 --> 00:06:47,280
moment, you're able to respond and you're able to catch the emotions of the other person

61
00:06:47,280 --> 00:06:52,440
and you're able to see the state of their mind feel and experience it and react appropriately

62
00:06:52,440 --> 00:06:58,160
and interact with the situation without tangling your own emotions up in it.

63
00:06:58,160 --> 00:07:06,440
To stay mindful but don't give impressions, give information and continue to give information

64
00:07:06,440 --> 00:07:13,800
and constantly give unbiased information telling people the results of this act, not

65
00:07:13,800 --> 00:07:18,400
saying them don't do this, don't do that but you know explaining to them why it's better

66
00:07:18,400 --> 00:07:20,320
to do this and better to do that and so on.

67
00:07:20,320 --> 00:07:25,440
So these are general principles that I would follow in terms of the meditation.

68
00:07:25,440 --> 00:07:33,680
There's be mindful, second, try to give information rather than some kind of feeling because

69
00:07:33,680 --> 00:07:37,040
it's the information that they will use and it's the technique that they will use.

70
00:07:37,040 --> 00:07:42,880
It has to come from their feeling, it has to be their emotion, it has to be their

71
00:07:42,880 --> 00:07:48,120
volition and their courage in themselves, they have to be looking for it.

72
00:07:48,120 --> 00:07:54,000
If you are pushing the meditation on them and they're not there, it's not coming from

73
00:07:54,000 --> 00:08:04,240
their heart, they'll do it to keep you there, to impress you and so on and when you're

74
00:08:04,240 --> 00:08:08,920
gone and even when you're there it won't be from the heart and it won't have the desired

75
00:08:08,920 --> 00:08:09,920
results.

76
00:08:09,920 --> 00:08:18,200
So you have to be step back and let them let their heart come forth, let their intention

77
00:08:18,200 --> 00:08:30,000
come forth and thirdly that is the point is to make themselves rely on.

78
00:08:30,000 --> 00:08:36,200
So by stepping back, by just giving them information and not bringing your own ego into

79
00:08:36,200 --> 00:08:42,320
the equation, you allow them to step up to the plate and take their own future into

80
00:08:42,320 --> 00:08:44,280
their hands.

81
00:08:44,280 --> 00:08:51,000
But this is more pronounced, I think, with people who are in great suffering.

82
00:08:51,000 --> 00:08:56,320
First of all, the one thing I'd say about people who are in great suffering, there's an

83
00:08:56,320 --> 00:08:59,080
advantage and a disadvantage that they have.

84
00:08:59,080 --> 00:09:02,600
The advantage is they see things that many of us don't see.

85
00:09:02,600 --> 00:09:05,960
They are experiencing the reason for practicing meditation.

86
00:09:05,960 --> 00:09:17,800
We practice, one of the great reasons anyway is because of the eventuality or the danger

87
00:09:17,800 --> 00:09:21,680
that we all face, that we might be in this situation at some point.

88
00:09:21,680 --> 00:09:26,240
Well, they're in it and they can see the need to meditate or the need to do something.

89
00:09:26,240 --> 00:09:28,600
They're looking for a way out of suffering.

90
00:09:28,600 --> 00:09:33,840
They have the suffering that many of us are blind to, we forget exists and so as a result

91
00:09:33,840 --> 00:09:41,760
we have no strength or fortitude of mind and when it comes, we like them, like people

92
00:09:41,760 --> 00:09:45,480
who are in it already, we're unable to deal with it.

93
00:09:45,480 --> 00:09:47,480
So they're looking for it.

94
00:09:47,480 --> 00:09:51,560
This is the advantage is that the people who are suffering greatly at the end of their

95
00:09:51,560 --> 00:10:01,280
lives, people who are terminally ill, they're not looking to pass the time or to seek entertainment

96
00:10:01,280 --> 00:10:04,800
and so on, they have a problem and they're looking to fix it, they're looking to find

97
00:10:04,800 --> 00:10:06,800
the solution.

98
00:10:06,800 --> 00:10:12,360
So they can be a really good audience in this sense and this is why often in this case

99
00:10:12,360 --> 00:10:14,600
giving information is often enough.

100
00:10:14,600 --> 00:10:19,800
But it has to be confident information and you have to be able to give it in a way that

101
00:10:19,800 --> 00:10:23,160
they understand, you're not just giving them a book to read or you're not just reading

102
00:10:23,160 --> 00:10:26,400
a book to them, you're explaining to them and you're going through it.

103
00:10:26,400 --> 00:10:31,360
I try to explain basically what the sort of things that I would be.

104
00:10:31,360 --> 00:10:35,160
Now the disadvantage with teaching people who are terminally ill and so on is because

105
00:10:35,160 --> 00:10:44,840
you're often fighting with an alternative or a different way of treatment.

106
00:10:44,840 --> 00:10:56,160
So the treatment in hospitals which quite often has to do with medication will very, quite

107
00:10:56,160 --> 00:11:03,520
often get in the way and it's going to be something that you'll have to work around and

108
00:11:03,520 --> 00:11:07,520
it's always going to lessen the effects of the meditation.

109
00:11:07,520 --> 00:11:15,520
If people who are on medication who have a very difficult time, medication based on or

110
00:11:15,520 --> 00:11:23,680
a medication of the sort that is meant to relieve pain or dull the pain is a real hindrance

111
00:11:23,680 --> 00:11:30,280
to meditation practice because not only does it dull the mind and muddle the mind but

112
00:11:30,280 --> 00:11:39,720
it also reinforces the avoidance of the difficulty just like alcohol or recreational drugs.

113
00:11:39,720 --> 00:11:47,640
It's a form of escape and so not only does it hurt the body and affect our body and

114
00:11:47,640 --> 00:11:52,440
our brain's ability to process information but it also affects the mind's willingness

115
00:11:52,440 --> 00:11:55,400
and ability to deal with pain and suffering.

116
00:11:55,400 --> 00:11:58,040
It sends you on the wrong direction.

117
00:11:58,040 --> 00:12:04,800
So I would say one of the first things that you should explain to people who are on medication

118
00:12:04,800 --> 00:12:09,080
or who are in this position, hopefully people who haven't yet decided what form of treatment

119
00:12:09,080 --> 00:12:14,520
they're going to take is explaining the differences in the treatment that yes, medication

120
00:12:14,520 --> 00:12:22,400
is something that is going to solve the problem in the short term but in the long term

121
00:12:22,400 --> 00:12:31,840
it's going to lead to a dependence and a addiction and the only way to have it effectively

122
00:12:31,840 --> 00:12:36,920
work is to drug you up to the point where you're unconscious because it's going to have

123
00:12:36,920 --> 00:12:41,360
less and less of effect your body is going to become more tolerant to the drugs and so

124
00:12:41,360 --> 00:12:44,400
you'll need more and more and so on.

125
00:12:44,400 --> 00:12:48,320
The pain is going to become more intense and your aversion to the pain will become more

126
00:12:48,320 --> 00:12:49,320
intense.

127
00:12:49,320 --> 00:12:55,360
I had direct experience, my grandmother was quite ill and they'd drug her up in a nursing

128
00:12:55,360 --> 00:13:00,840
home to the point where she couldn't even recognize people because this was their way

129
00:13:00,840 --> 00:13:06,680
of dealing with that it was laziness and it was sloppiness and it was negligence.

130
00:13:06,680 --> 00:13:10,440
When she had pain and she complained about it they just increased her medication rather

131
00:13:10,440 --> 00:13:14,840
than going to see a doctor so they just increased it to the point where she'd still

132
00:13:14,840 --> 00:13:20,440
had no pain but she was more or less unconscious to the world around her and when this

133
00:13:20,440 --> 00:13:26,680
was realized they took her off the medication but then she was an incredible suffering.

134
00:13:26,680 --> 00:13:33,920
She wasn't able to deal with this pain that she had been teaching herself to avoid

135
00:13:33,920 --> 00:13:42,840
to run away from to stop to escape through the medication and as a result she was in great

136
00:13:42,840 --> 00:13:49,240
and terrible suffering that was very, you know, most difficult for her to deal with.

137
00:13:49,240 --> 00:13:58,880
So we should explain this and try to make it clear that there are alternatives that it's

138
00:13:58,880 --> 00:14:05,440
actually in our better interest to come to terms with the pain and to die with the clear

139
00:14:05,440 --> 00:14:08,960
mind if we're going to die that our minds should be clear we should know what our do

140
00:14:08,960 --> 00:14:15,480
we're doing we should have the ability to find closure with our relatives and so on with

141
00:14:15,480 --> 00:14:25,040
a clear and alert mind and give confidence that there is another way and explain the

142
00:14:25,040 --> 00:14:29,960
basically the theory of the meditation and so here goes exactly the sorts of things

143
00:14:29,960 --> 00:14:34,720
that I would say to them I would talk to them you know get right to the point about pain

144
00:14:34,720 --> 00:14:43,280
and say that well this is going to be our our training in this in the meditation practice

145
00:14:43,280 --> 00:14:50,640
to deal with the pain or to approach the pain and to change the way we look at the pain.

146
00:14:50,640 --> 00:15:02,000
So you explain first about the nature of feelings that the physical feeling is actually

147
00:15:02,000 --> 00:15:09,120
not really the problem the problem is our inability to accept it the fact that it bothers

148
00:15:09,120 --> 00:15:14,160
us and to explain to people how when you when you're actually look at the pain and when

149
00:15:14,160 --> 00:15:18,360
you're actually there with it you can see that there's two things there's the pain and

150
00:15:18,360 --> 00:15:23,440
then there's your aversion to to the pain and these are quite separate and encourage

151
00:15:23,440 --> 00:15:30,320
people to to look at it to examine it and to become comfortable with it explaining that

152
00:15:30,320 --> 00:15:36,080
as you look at the pain and as you examine it you're able to see that it's it's something

153
00:15:36,080 --> 00:15:42,560
that comes and goes it actually has no effect on the mind the problem is that we've

154
00:15:42,560 --> 00:15:48,440
developed this wrong idea that there's something bad about the pain being based on our worries

155
00:15:48,440 --> 00:15:57,760
and our are feeling that somehow it's going to lead to injury or or it means something

156
00:15:57,760 --> 00:16:04,320
more more significant in terms of an illness or causing that so eventually to the point

157
00:16:04,320 --> 00:16:11,440
where any little pain causes suffering in our mind we could explain to people that we're

158
00:16:11,440 --> 00:16:18,080
actually causing the suffering ourselves by our aversion to the pain so explain to people the

159
00:16:20,720 --> 00:16:25,920
relationship between the experience and the suffering how in the beginning there is the

160
00:16:25,920 --> 00:16:33,520
tension and the pressure in the body the body being in a in a specific position and the pressure

161
00:16:33,520 --> 00:16:39,680
build up and the the stiffness build up and so on and then there arises this painful feeling

162
00:16:39,680 --> 00:16:44,320
now once there arises the painful feeling the mind picks it up and starts to run away with it

163
00:16:46,880 --> 00:16:52,480
now it depends how the the state of of mind of the person how much you want to go into detail

164
00:16:52,480 --> 00:16:57,280
but at the very least you should be able to explain to them that the first of all that the feelings

165
00:16:57,280 --> 00:17:03,200
are going to be our focus that what you know make clear to them what we're going to be dealing with

166
00:17:03,200 --> 00:17:08,480
what the meditation is designed to do is designed to help us to work through the feelings and to

167
00:17:08,480 --> 00:17:12,880
come to let go of them and actually overcome them to the point that they don't bother us

168
00:17:15,680 --> 00:17:21,120
and explaining how we're going to do that that we're going to actually focus on the pain and try

169
00:17:21,120 --> 00:17:27,760
to remind ourselves the nature of the pain you know it's just a pain or a feeling and so we use

170
00:17:27,760 --> 00:17:34,000
this this word that we have explained to them the technique of meditation you know the word the

171
00:17:34,000 --> 00:17:38,400
word mantra if they've ever heard of this or if not explained to them what a mantra is it's

172
00:17:38,400 --> 00:17:44,800
something that helps you focus on an object something that helps you to come to see only the object

173
00:17:44,800 --> 00:17:54,720
and to to keep other things from interfering from distracting you so to keep yourself focused on

174
00:17:54,720 --> 00:18:03,120
a single object now what that does is keeps you focused on simply on the pain now reminding

175
00:18:03,120 --> 00:18:08,000
people that the pain is not the problem once you're focused on just the sensation for itself

176
00:18:08,000 --> 00:18:13,200
what do you come to realize is that it's actually not a negative experience it's not something

177
00:18:13,200 --> 00:18:19,440
unpleasant and what doesn't have a chance to come in is this judgment this disliking this upset

178
00:18:19,440 --> 00:18:25,920
about the pain we're going to be able to separate the feelings of upset and stress and anger

179
00:18:25,920 --> 00:18:32,720
and frustration about the sadness from the actual pain itself and just experience the pain

180
00:18:32,720 --> 00:18:36,800
for what it is and you can reassure them that it works tell them that you know you've done this

181
00:18:36,800 --> 00:18:43,280
and so on and they should try it for themselves if it doesn't work they can they're welcome to stop

182
00:18:43,280 --> 00:18:48,480
and try the medication so basically saying to people that you know we want to try this as an alternative

183
00:18:48,480 --> 00:18:53,040
and see what you think and and if it doesn't work you've always got the medication to go

184
00:18:53,040 --> 00:19:00,880
back up on as a backup now once you've explained this we can you know this is the theory

185
00:19:00,880 --> 00:19:04,720
this is something that they have to keep in mind then you can start them on something simpler say

186
00:19:04,720 --> 00:19:09,760
you know this mantra this word that we use the idea is to allow us to see things clearly so

187
00:19:11,120 --> 00:19:16,880
we have to we teach people to start practicing it and find a simple object to practice it on

188
00:19:17,600 --> 00:19:22,000
the basic object of our contemplation as many of you are aware is the stomach

189
00:19:22,720 --> 00:19:28,320
and this is used very useful for people who are in bed lying down when you're lying down in

190
00:19:28,320 --> 00:19:35,280
your relax you'll find the stomach is quite evident and easy to follow and so we watch it rising

191
00:19:35,280 --> 00:19:41,520
falling rising falling and keep our mind on it and we have this word that allows our mind to focus

192
00:19:41,520 --> 00:19:49,920
only on the stomach when it rises say to yourself rise when it falls say to yourself and your mind

193
00:19:49,920 --> 00:19:56,160
will focus on that object and that object only if won't nothing else will come into distracted

194
00:19:56,160 --> 00:20:01,840
or your mind won't be flitting here and there your mind will become focused and concentrated

195
00:20:01,840 --> 00:20:09,120
and then for a time you'll find that you're able to find peace and and clarity of mind now as

196
00:20:09,120 --> 00:20:15,280
you do that you'll find that from time to time pain arises and especially if you turn

197
00:20:15,280 --> 00:20:20,560
when you're familiar if you have a sickness and so on you'll find that that these sharp and

198
00:20:20,560 --> 00:20:26,080
unpleasant sensations arise now it's at that point and you can lead people through that so you can

199
00:20:26,080 --> 00:20:30,000
have them start to watch the stomach and then it start to explain as they're practicing

200
00:20:30,720 --> 00:20:34,480
as they're watching the stomach explained them about the sorts of things that might arise

201
00:20:35,360 --> 00:20:42,080
the most prominent being the feelings the pain and so on and say when the pain arises try you know

202
00:20:42,080 --> 00:20:46,400
we can always go back to the medication reassuring them that they don't and this isn't going to be

203
00:20:46,400 --> 00:20:53,760
we're not trying to torture them but reassure them to try first to acknowledge pain and just

204
00:20:53,760 --> 00:20:59,840
remind themselves of the pain and keep their minds on the pain as the truth is it's only when

205
00:20:59,840 --> 00:21:09,680
their mind slips away from the pain into judgment into aversion into dislike or of the pain

206
00:21:10,240 --> 00:21:14,640
that it begins to give rise to thoughts of how to get away and how to change it how to find

207
00:21:14,640 --> 00:21:21,520
some way anyway to escape the situation as long as you're focusing on the situation seeing it

208
00:21:21,520 --> 00:21:25,920
for what it is it doesn't even enter your mind that you have to change the situation it doesn't

209
00:21:25,920 --> 00:21:32,880
enter your mind that this is a bad situation there's no room for that your mind is fully aware

210
00:21:32,880 --> 00:21:37,760
it's fully focused on the object itself you don't have to explain all this to them but you know

211
00:21:37,760 --> 00:21:44,640
have them try it have them see for themselves whether it's this is true or not what the result is

212
00:21:45,600 --> 00:21:50,480
I tried this with a woman who had stomach cancer and she had had it for about seven years she

213
00:21:50,480 --> 00:21:55,680
was in her final stages and so she was on some pretty heavy pain medication and right before

214
00:21:55,680 --> 00:21:59,200
she was going to take it I said well let's try this and this is exactly what I did with her I

215
00:21:59,200 --> 00:22:05,360
led her through that watching the stomach rising falling and then she was suddenly this pain arose

216
00:22:05,360 --> 00:22:10,160
in her stomach and she wanted to take her medication they said well try this first you know see

217
00:22:10,160 --> 00:22:18,320
see what what it does say to yourself pain and so she closed her eyes and she tried and I guess

218
00:22:18,320 --> 00:22:24,160
she she she did she tried it she said to herself pain and then she fell asleep she fell

219
00:22:24,160 --> 00:22:32,000
she you know she passed out into into unconscious is probably because she you know not been

220
00:22:32,000 --> 00:22:37,200
sleeping very well due to the pain and the suffering but as a result she didn't have to take

221
00:22:37,200 --> 00:22:42,320
the medication at that time now I didn't have time to stay with her and she obviously would have

222
00:22:42,320 --> 00:22:47,920
stayed on the medication but if you can keep up with this if you can help people through it

223
00:22:50,400 --> 00:22:56,640
and and explain to people again and again and keep encouraging them through it and leading them

224
00:22:56,640 --> 00:23:02,960
through it and meditating with them and saying let's try it again eventually you can if not

225
00:23:02,960 --> 00:23:08,000
wean them off the medication you can at least help them to keep the medication at a minimum or

226
00:23:08,000 --> 00:23:16,480
only when it's terribly incredibly severe and in fact as I said these sorts of people are

227
00:23:17,600 --> 00:23:23,600
prime can be prime material for the meditation practice and can gain great insights and and wisdom

228
00:23:23,600 --> 00:23:28,800
yeah they can actually mitigate some of the effects of illness and some people are able to overcome

229
00:23:28,800 --> 00:23:34,240
the illness as a result of the easing up of the tension and the body's better ability to heal

230
00:23:34,240 --> 00:23:42,640
itself but at the very least they'll be able to deal with the end of life and and they should

231
00:23:42,640 --> 00:23:48,000
find that especially with the help of someone who's done it before and who's encouraging them

232
00:23:48,000 --> 00:23:57,440
through it and reminding them about these things that they you know they're whole attitude will

233
00:23:57,440 --> 00:24:04,720
change and they will they will become much you know in many ways a new person much better

234
00:24:04,720 --> 00:24:10,000
able to deal with the difficulties of the problem so I think that's my cue to finish

235
00:24:10,000 --> 00:24:18,640
I'd like to thank you all for listening for tuning in and I hope this helps too and I hope

236
00:24:18,640 --> 00:24:23,040
that people are able to use this for helping people in these sorts of situations so thank

237
00:24:23,040 --> 00:24:42,800
for tuning in all the best

