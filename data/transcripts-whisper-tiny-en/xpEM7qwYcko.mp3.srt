1
00:00:00,000 --> 00:00:05,000
What should one do when one's family member or close friends

2
00:00:05,000 --> 00:00:10,000
are clearly suffering from greed, anger and delusion?

3
00:00:15,000 --> 00:00:21,000
Work even harder on yourself. I think it's the best answer.

4
00:00:23,000 --> 00:00:26,000
But it's really the answer for everything.

5
00:00:26,000 --> 00:00:31,000
The harder you work on yourself and the more effort you put into making yourself a better person.

6
00:00:34,000 --> 00:00:39,000
The more benefit the people who are close to you

7
00:00:40,000 --> 00:00:43,000
or the better it is for the people who are close to you.

8
00:00:43,000 --> 00:00:47,000
The first people the benefit from your meditation practice are

9
00:00:47,000 --> 00:00:50,000
I think obviously the people who are closest to you.

10
00:00:50,000 --> 00:00:56,000
We are like a river.

11
00:00:57,000 --> 00:01:03,000
All of our actions by body, speech and mind

12
00:01:04,000 --> 00:01:09,000
are the outpouring of our source, which is the mind.

13
00:01:10,000 --> 00:01:13,000
If the source is tainted,

14
00:01:14,000 --> 00:01:16,000
then guess what?

15
00:01:16,000 --> 00:01:20,000
The river is tainted as well, and all the way down anyone,

16
00:01:20,000 --> 00:01:24,000
any being, any animal, any creature that drinks from the river

17
00:01:25,000 --> 00:01:28,000
will drink, be drinking poisoned water.

18
00:01:31,000 --> 00:01:33,000
That's what we're giving to people.

19
00:01:33,000 --> 00:01:37,000
This is what we're doing already for our family members.

20
00:01:37,000 --> 00:01:40,000
Without having practice meditation, we're already doing a lot for them.

21
00:01:40,000 --> 00:01:44,000
The problem is it's tainted and a lot of it's negative.

22
00:01:44,000 --> 00:01:52,000
So the things we do to and for and in regards to our closest friends and relatives

23
00:01:52,000 --> 00:01:55,000
is often for their detriment.

24
00:01:56,000 --> 00:02:04,000
Once we clear and purify our minds,

25
00:02:04,000 --> 00:02:13,000
on the source, then our actions and interactions with the people around us becomes

26
00:02:13,000 --> 00:02:30,000
incredibly different and really is transformative in very good, in very well, incredible way.

27
00:02:31,000 --> 00:02:37,000
You'll often find that simply being mindful the people around you

28
00:02:37,000 --> 00:02:43,000
are able to give up as well.

29
00:02:43,000 --> 00:02:48,000
They're bad deeds and they're bad habits and so on.

30
00:02:49,000 --> 00:02:54,000
This really goes back to how Buddhism has spread.

31
00:02:54,000 --> 00:02:57,000
Buddhism is always spread by example.

32
00:02:57,000 --> 00:03:01,000
It hasn't spread by preaching or proselytizing.

33
00:03:01,000 --> 00:03:05,000
It is spread by showing, by doing.

34
00:03:05,000 --> 00:03:10,000
We have the example of Asaji, one of my Dhamma Bhata videos,

35
00:03:10,000 --> 00:03:15,000
how, sorry, put to simply saw Asaji walking.

36
00:03:15,000 --> 00:03:18,000
And he said, this must be a special person.

37
00:03:18,000 --> 00:03:26,000
He's got something about him that is beyond an ordinary human being.

38
00:03:27,000 --> 00:03:30,000
And he was so he was attracted to it as well.

39
00:03:30,000 --> 00:03:36,000
The whole manner of acting, speaking,

40
00:03:36,000 --> 00:03:42,000
acting and speaking, even just deporting oneself changes

41
00:03:42,000 --> 00:03:46,000
and it really affects the people around us.

42
00:03:46,000 --> 00:03:50,000
The more clear and pure your mind, your mind becomes,

43
00:03:50,000 --> 00:03:58,000
the more, more pure your actions are.

44
00:03:58,000 --> 00:04:02,000
And this has real consequences.

45
00:04:02,000 --> 00:04:03,000
This is how Buddhism spreads.

46
00:04:03,000 --> 00:04:05,000
This is how the practice spreads.

47
00:04:05,000 --> 00:04:08,000
This is how purity spreads.

48
00:04:08,000 --> 00:04:16,000
So the idea of changing people, it really never works anyway.

49
00:04:16,000 --> 00:04:19,000
It doesn't work with your closest friends and relatives.

50
00:04:19,000 --> 00:04:22,000
It doesn't work with people who you're not close to.

51
00:04:22,000 --> 00:04:27,000
One of the first things you learn as a meditation teacher is you can't convince someone

52
00:04:27,000 --> 00:04:31,000
to see the truth.

53
00:04:31,000 --> 00:04:34,000
You can't convince someone to practice correctly.

54
00:04:34,000 --> 00:04:37,000
You can't convince someone to give up wrong views.

55
00:04:37,000 --> 00:04:44,000
You have to give them the opportunity and show them the way

56
00:04:44,000 --> 00:04:48,000
to see their own reality objectively.

57
00:04:48,000 --> 00:04:56,000
You have to step back and give them the space that other people don't give them.

58
00:04:56,000 --> 00:05:02,000
And because our ordinary reactions with other people are always back and forth.

59
00:05:02,000 --> 00:05:07,000
Someone says or does something and right away we react to it.

60
00:05:07,000 --> 00:05:11,000
And if someone says I have a lot of suffering, I've got a lot of problems.

61
00:05:11,000 --> 00:05:15,000
They'll say, I'll go take a bath or go watch a movie or something.

62
00:05:15,000 --> 00:05:21,000
Our defilements come right back and bounce off of their defilements.

63
00:05:21,000 --> 00:05:25,000
When your mind is pure, you don't do that.

64
00:05:25,000 --> 00:05:28,000
You sit there and you listen to them.

65
00:05:28,000 --> 00:05:34,000
And the key to being a meditation teacher, this is an example,

66
00:05:34,000 --> 00:05:39,000
is to listen and to allow the person to experience it

67
00:05:39,000 --> 00:05:42,000
and help them to experience it for what it is.

68
00:05:42,000 --> 00:05:46,000
It's really how we practice.

69
00:05:46,000 --> 00:05:51,000
So simply listening and not reacting.

70
00:05:51,000 --> 00:05:54,000
It gives the person the closure.

71
00:05:54,000 --> 00:05:59,000
They've come out with this emotion or this clinging, this attachment

72
00:05:59,000 --> 00:06:02,000
and you say, greed, anger, delusion, they've come out with it.

73
00:06:02,000 --> 00:06:06,000
It allows them to come back full circle and experience it.

74
00:06:06,000 --> 00:06:10,000
And if someone gets angry at you and you attack them,

75
00:06:10,000 --> 00:06:12,000
then they don't have closure.

76
00:06:12,000 --> 00:06:16,000
They now have an opening for more anger.

77
00:06:16,000 --> 00:06:19,000
But if someone gets angry at you and you don't respond,

78
00:06:19,000 --> 00:06:24,000
then all that they're left with is the anger and the result of it, the suffering.

79
00:06:24,000 --> 00:06:28,000
So they have closure and they don't have any continuation.

80
00:06:28,000 --> 00:06:30,000
You haven't continued it.

81
00:06:30,000 --> 00:06:32,000
You haven't created a chain.

82
00:06:32,000 --> 00:06:36,000
All they've got is a circle and you haven't put the next linking.

83
00:06:36,000 --> 00:06:42,000
If you can understand that, that's sort of idea that the non-reactivity

84
00:06:42,000 --> 00:06:48,000
where you simply let the person dwell in their experience.

85
00:06:48,000 --> 00:06:51,000
It's the very beginning of meditation.

86
00:06:51,000 --> 00:06:57,000
And then you're in a position having given them that moment

87
00:06:57,000 --> 00:07:01,000
you're in a position to explain to them what it is that they're experiencing.

88
00:07:01,000 --> 00:07:04,000
But you can't do that at the moment when they're greedy, angry, deluded.

89
00:07:04,000 --> 00:07:09,000
When they have all these emotions, the first thing to do is to let them experience it.

90
00:07:09,000 --> 00:07:13,000
Anyway, and of course the most important is to work on yourself

91
00:07:13,000 --> 00:07:15,000
because that's how you do this.

92
00:07:15,000 --> 00:07:21,000
Once you've worked on yourself, you naturally don't react.

93
00:07:21,000 --> 00:07:26,000
You're naturally non-reactive and you're naturally giving people the opportunity.

94
00:07:26,000 --> 00:07:29,000
And when they're angry, you're just seeing the anger.

95
00:07:29,000 --> 00:07:32,000
And when you see the anger, let's them see the anger.

96
00:07:32,000 --> 00:07:37,000
You're looking at it, they're looking at it, and they're learning about it.

97
00:07:37,000 --> 00:07:42,000
It gives them the opportunity to do the learning that they need to do.

98
00:07:42,000 --> 00:08:06,000
So I hope that helps.

