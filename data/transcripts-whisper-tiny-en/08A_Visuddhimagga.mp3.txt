Today, we come to the temperaments, for a graph, 74.
So there are six kinds of temperaments, 3D
temperaments, heating temperaments,
diluted temperaments, faithful temperaments,
intelligent temperaments, and speculative temperaments.
In this sheet, the second column represents the six temperaments.
And the first line, charita means temperaments.
And there are dosa, raga, sadha, booty, moha, vita pan, so on.
So the greedy temperaments is raga,
and heating temperaments dosa.
We do the temperaments moha, faithful temperaments, sadha.
Intelligent temperaments, booty.
Booty is a synonym of mehana, of panya, knowledge,
wisdom.
And speculative temperaments is vita ka.
And some teachers say there are more than six kinds of temperaments,
and the mixture of them, and so on.
So they are not so much interesting, because they are confusing.
So we take only following the order.
We take six temperaments, and three are good temperaments,
and three are bad temperaments.
And each one of them has terror in another group.
So faithful temperaments is terror to greedy temperaments,
because faith is strong when profitable karma occurs
in one of greedy temperaments, going to its special qualities
being near to those of greed.
That means greed is similar to faith or sadha,
because in an unprofitable way, greed is affectionate
and not over austere.
And so in a profitable way is faith.
In fact, instead of saying in an unprofitable way,
we should say, on the side of unprofitable states,
so on the side of Akusala.
So it means among Akusala, greed is affectionate
and not over austere.
And among Akusala states, faith has the same quality.
Faith is affectionate and not over austere.
So they are similar.
So with intelligent temperaments and hating temperaments,
they are also similar.
So it wasn't who habitually gets angry,
maybe of intelligent temperaments.
So there are some people who are short-tempered
and get angry for a very small reason.
And those people may be sharp in their intelligence.
So they are similar or they are parallel.
And in paragraph 76, the last sentence,
and hate Akus and the motive of condemning living beings,
the only word here is avoid.
Does condemn mean avoid?
No, no.
Confidence, church, I see.
But here, avoiding.
So hate Akus and the motive of avoiding living beings,
while understanding Akus and the motive of avoiding formations.
Formation means, mind and matter, sankara.
And then speculative temperament has its parallel
and diluted or moja temperament.
They are similar.
And then the source of cause of these temperaments,
a discussion on the source of temperaments.
He also first the other gives the reason
given by other teachers.
And then he said, they are all inconclusive or indecisive.
Now, on page, page 104, paragraph 80, s, some say.
So there is a footnote.
Some is said with reference to the elder poopertaser.
For it is put in this way, in this way, by him, in the multi-mugga.
Now, there was a book called We Multi-Mugga.
And it was written before, before we saw the mugga.
And it was written by an elder named Upertaser.
And it was written in Harley-Lambits.
There is a book by Professor Baba, the comparative study
of We So The Mugga and We Multi-Mugga.
And in the sub-commentary to We So The Mugga,
we call We So The Mugga a commentary.
Although it is not a commentary of one particular
collection.
It is a common commentary of all So The Mugga.
So we call We So The Mugga a commentary.
And so there is a sub-commentary on We So The Mugga.
And the name of the elder, as well as the name of the book,
was mentioned in the sub-commentary.
So in the New We So The Mugga itself, nothing is mentioned.
Just say, some say.
So some refers to the elder Upertaser,
who wrote the book We Multi-Mugga.
So in that book, it is said that there are three kinds of temperaments
to begin with, have their source in previous habit.
And they have their source in the elements and humans.
Elements mean the four great elements,
earth, element, water, element, and so on.
And humans, what's humans?
Yeah, humans mean something in the body, wind,
flame, and what is the other thing?
What?
Something like that.
The Upertaser.
Are they called humans?
Yeah, in medieval England, they had that kind of.
Yes.
So they are the sources for the kinds of temperaments.
So in previous habit, and also the elements and humans.
And apparently, one of really temperaments
has formally had plenty of desirable task
and gratifying work to do or has reappeared here.
That means reborn here after dying in a heaven.
So it wasn't who dies as a dam and then reborn as a human being.
He tends to have the greedy temperament.
And one of hating temperament has formally
had plenty of stabbing and torturing and brutal work
to do or has appeared after dying in one of the hells
or the naga existence.
So one who is reborn after dying from hell
or after dying from the existence of servants
tends to have hating temperaments.
And one of the looted temperaments has
probably drunk a lot of toxic and neglected learning
and questioning or has reappeared here
after dying in that animal existence.
So this is according to the previous habit.
And then they are explained according to the elements.
The person is of deluded temperament
because two elements are prominent.
That is to say the earth, element, fire, element, and so on.
So one who has these elements prominent in his body.
So the dead person tends to become of deluded temperament
and so on.
But not all those who have plenty of desirable tasks
and gratifying work to do and who have reappeared here
after dying in a heaven are of greedy temperaments
and so on.
So they are not conclusive.
But the other gives the exposition given
by the teachers of the commentary.
That means given by the Theta-water tradition.
And they said that the fact that these beings
have prominence of greed, prominence of hate, prominence
of delusion, prominence of non-grid, non-hate,
the non-delusion, and that is governed by previous root
cause.
For one in one, men at the moment of his accumulating,
rebirth-reducing comma, greed is strong and non-grid is weak.
Non-hate and non-delusion are strong and hate
and delusion are weak.
Then his weak non-reduce unable to prevail over his greed.
But his non-hate and non-delusion being strong
are able to prevail over his hate and delusion.
That is why on being reborn through rebutting,
given by that comma, he has greed.
He's coordinated and untangry and possesses
understanding with knowledge like a lightning flesh.
Now, at the moment of his accumulating,
rebirth-reducing comma, greed is strong and non-grid is weak.
Here, it does not mean that the comma
and greed are non-grid arise at the same time or at the same moment.
Because greed is unwholesome mental state
and non-reduce wholesome mental state.
So they cannot arise together.
And the comma, which gives results as relinking
in human existence and so on, is good comma.
Kuzala, I mean wholesome comma, and it cannot be.
And wholesome comma.
So when Kuzala comma arises in our minds,
there can be no greed arising at the same time.
But he has greed before and after that comma.
So that is what is meant here at the moment
of his accumulating comma.
Not the arise at the same time, at the same moment.
But the comma is influenced by, say, strong greed
and non-grid and so on.
So when at the moment of accumulating comma,
greed is strong and non-grid is weak and so on,
then there is this difference in doing
of different temperament.
So these are explained with reference to which root causes
of which he too is predominant or strong
and weak at the moment of accumulating
of good or bad comma, especially good comma.
And then we go to how it is to be known that the person
is this person is of greedy temperament and so on.
So this is something like psychology.
So we guess the temperament of a person
by the posture he takes, by the action,
by his manner of eating, by his seeing,
and by the kind of state occurring in his mind.
So they are explained in detail.
Now when one of greedy temperament is walking
in his usual manner, he walks carefully,
and that means he walks gracefully, puts his foot down slowly,
puts it down evenly, lifts it up evenly,
and his step is springy.
What is springy?
That's nice.
That's nice, isn't it?
Oh, I think.
The explanation given in the footnote is that
the footprint, you know, one go there, let the foot print,
then therefore his footprint is even.
There is no, what do you call it?
There is a question.
No, no, no, no, no, no, no, no, no, no, no.
But the footprint of ordinary bugle
has something, not touching, equally, evenly.
So springy means not touching in the middle,
that is springy.
So by...
That's a good rule, it's bad for you.
That's a good rule, yes.
Yes, it's a sad rule, that's a good rule.
And one of the heading temperament works
as though he was digging with the point of his feet,
puts his foot down quickly,
lifts it up quickly, and his step is dragged along.
And the leader temperament works with a perplexed guide,
puts his foot down, has it hesitantly,
lifts it up hesitantly,
and his step is pressed down suddenly,
and then it was.
And this is said in the account of the origin
of the Margandilla Suta.
In that Suta, the name of the man was Margandilla,
and he had a beautiful daughter.
So he wanted to give his beautiful daughter
to a deserving man, but he had not found one yet.
So one day he saw the footprints of the Buddha.
And so he knew that this footprint
belongs to an extraordinary man.
So he followed the footprints and he came to the Buddha
and offered his daughter to the Buddha.
So this verse was uttered by him
when he saw the footprint of the Buddha.
It was not of a greedy person, it was not of a hating person,
it was not of a deluded person,
but it must belong to a very extraordinary person.
And then in paragraph 89,
about the middle of the paragraph,
and he slipped in a confident manner.
Actually, he slipped in a pleasant manner,
a pleasant look at something like that.
Not confident.
And a little further down,
with his body flung down, he sleeps with a skull.
What is skull?
Skull?
Yeah.
From?
Yes.
With a frog.
That's right.
And then to two lines down,
one of the deluded temperaments,
spread his breath all.
How do you pronounce that?
All right.
All right.
That means, not symmetrical.
All right, right, yes.
That's right.
And sleep moves, we face down what with this body's crawling.
But when he works, he gets up,
and he says, let's say in whom?
So this is by his posture or department.
Now by action,
also in the acts of sitting, et cetera,
one of greedy temperament girls,
the broom world, and he sits cleanly and evenly
without hurling or scattering the sand,
as if he were strewn into water clouds,
as a kind of powers.
So we can guess his temperament from looking
how he's swept.
But it is not possible in this country,
because we use
in machines.
Exactly.
Exactly.
That came clean as.
And then by eating,
one of greedy temperament likes eating rich sweet food.
So those who like sweet food
are said to be of greedy temperament.
And one of hating temperament likes eating rough sour food.
So if you like sour food,
you want to hate in temperament.
And if you are not decided,
you have no settled choice
then you're of a diluted temperament.
So what do you like?
I like sour food.
And then by seeing and so on,
when one of greedy temperament sees,
even a slightly pleasing visible object,
he looks long as if surprised.
He sees his own trivial virtues,
discounts genuine fall.
And when debutting he does so with regret
as if unwilling to leave with regret,
the actual word use is with attachment.
He has a attachment or he has something like a concern
for that object.
Attachment.
Yes.
And one of hating temperament
sees even a slightly unpleasing visible object.
He avoids looking long as if he were tired.
He picks up trivial for his counts genuine virtues.
And when debutting he does so without attachment
or regret as if anxious to leave and so on.
Or then for the diluted,
if he hears others criticizing, he criticizes it.
If he hears others praising he praises,
but actually he feels
like the calamity in himself.
I want to use indifference instead of equanimity.
So indifference in himself,
the indifference of unknowing.
So here indifference means not knowing.
Moha.
So too with sounds and so on.
And then by the kind of states occurring,
one of greedy temperament there is frequent occurrence
of such states as deceit, fraud,
fraud, evilness of wishes, greatness of wishes,
discontent, property and personal vanity and so on.
And then for the faithful temperament,
there is frequent occurrence of such states
as free generosity, desire to see noble ones, desire to hear
the good dhamma and great gladness.
In the original,
it means of frequent gladness.
So he gets gladness frequently,
not necessarily great. And then on page 109 bottom line,
things suitable for those of different temperaments.
So for one of greedy temperament,
a suitable lodging is unwashed,
seal and stance level with the ground
and it can be either an overhanging rock with an unprepared
privilege, a grass hard or a leaf house, et cetera.
It ought to be splatter with that full of beds
which is telepeated too high or too low.
It is opposite of his temperament,
he is of greedy temperament.
So it is suitable
bed lodging, a suitable for him.
And then with regard to the bull,
because he's talking about months
and the right kind of bull for him is an ugly, clear bull
distributed by stoppings and joints,
or a heavy and misshapen iron bull
as un-appetizing as his car.
Now, some words are misplaced here.
And the right kind of bull for him is an ugly, clear bull.
Or a heavy and misshapen iron bull
just figured by stoppings and joints.
That's it.
Because if an ugly, clear bull has defecks,
then he has to throw it away
because it may have cracks and it's difficult to make amends.
But an iron bull, if it becomes,
if it has a holes or something,
and you can put pivots or stoppings and joints
to keep the bull together.
So the heavy and misshapen,
I mean, is figured by stoppings and joints
should be, should qualify the heavy and misshapen iron bull.
And on page 111, photograph 101,
the right lodging for one of diluted temperament has a view
and is not shut in, where the forequarters are visible
to him as he sits there.
He is of diluted temperament.
So he needs space, a large space.
As to the postures, walking is right.
The right kind of object for his contemplation is not small.
That is to say, the size of a windowing basket
or the size of a saucer.
That means not the size of a windowing basket
or not the size of a saucer.
Those are small objects.
But a person of diluted temperament needs large objects.
Suppose he wants to practice earth, casino meditation,
looking at the earth disk or earth.
So if he is of a diluted temperament,
he needs to have a big plot of land or something
to look at.
Not just a small earth disk.
Because his mind needs to look at a larger object.
So the object for his contemplation is not small.
That is to say, not the size of a windowing basket
or not the size of a saucer.
For his mind, he becomes more confused in a confined space.
So the right kind is an empty, large casino.
Maybe about the size of a tennis court or full ball.
Maybe full full.
The rest is as stated for one of his 18th and 10th
temperament.
That's correct.
And for the speculative temperament,
the small one is right.
So for a percent of winter temperament,
because he is peculiar if he wants to think of many things.
So for him, a small object is suitable
about the size of a windowing basket or the size of a saucer.
The rest is as stated for one greedy temperament.
And these are the details with the definition
of a kind source recognition and what is suitable and so on.
Now, he comes with the 40 subjects of meditation now.
So the 40 subjects are given in paragraph 104 and the following.
So 10 casinos, the meaning of the word casino means all or to tell.
Look, when you look at the disk, you look at the whole disk.
So whole or total.
10 casinos and 10 kinds of foulness, 10 recollections,
four divine abidings, four immaterial states,
one perception, one defining.
So altogether, 40 subjects.
These are the 40 subjects of some mathematician.
Here, the 10 casinos are this earth casino, water casino,
fire casino, air casino, blue casino,
yellow casino, red casino, white casino,
light casino, and limited space casino.
So there are 10 casinos.
And how to prepare for this casino
and how to practice will be explained in the next chapter.
The 10 kinds of recollections are this.
We collection of the Buddha.
That means the recollection of the qualities of the Buddha,
recollection of the dharma, recollection of the sangha,
recollection of virtue.
That means one's own sealant, recollection of generosity,
recollection of deities, recollections of mindfulness of death.
And then mindfulness occupied with the body,
mindfulness of breathing, and recollection of peace.
Peace here means nibana.
These are the 10 recollections.
And then the four divine abidings
are these loving-kindness, compassion, gladness,
or sympathetic joy and equality.
The four immaterial states are these.
The base consisting of boundless space.
The four immaterial states are just the four R. R.
R. R. R. The formless formless steps of consciousness
there.
The base consisting of boundless consciousness,
the base consisting of nothingness,
and the base consisting of neither perception or non-perception.
The one perception is a perception of reposiveness
in nutriman or in food.
The one defining is the defining of the four elements.
That means trying to see the four elements clearly
one different from the other.
So this is earth element, this is water element, and so on.
This is how the exposition you should
be understood as to enumeration.
So these are the list of 40 subjects
of some mathematician, as to which bring access only
and which absorption.
Now, the samadhi, if you remember,
is divided into different kinds.
And there is a division into neighborhood summary
or neighborhood concentration, and our access concentration
and absorption concentration.
Some subjects of meditation can lead to access concentration only
and not to absorption.
And the others can lead to absorption also,
so as to which bring access only and which absorption.
The egg reproduction, accepting mindfulness
occupied with the body and mindfulness of breathing,
the perception of reposiveness in nutriman
and the defining of the food elements,
are 10 meditation subjects that bring access only.
You may look at the chart here.
And you look at the Jana Kollant, the last Kollant.
If we say no Jana, that means they can lead only
to access concentration.
They cannot lead to Jana concentration.
So if you practice the recollection of the Buddha,
you may get just the concentration of mine,
but you will not get Jana through that type of meditation.
Because the qualities of the Buddha are profound,
and there are many qualities to keep your mind on.
And so it cannot help you to get real strong concentration
or to get Jana.
So the egg recollections, the perception of reposiveness
in nutriman and the defining of food elements
can lead you to the gaining of access concentration only.
You will not get Jana if you practice those meditations.
Then one perception and one defining.
So you look at no Jana.
Or if you look at here, AP means absorption, absorption
development.
P means preliminary.
And UP means access.
And AB means absorption.
The others bring absorption.
So the others, if you practice the other subjects of meditation,
you can gain Jana, so absorption.
Then as to the kind of Jana, again,
please look at the Jana column.
Among those that bring absorption, the 10 casinos,
together with mindfulness of breathing,
bring all four Janas, four means here, five,
because there are two ways of describing Jana.
According to full-fold method and five-fold method,
we are familiar with five-fold methods, right?
First Jana, second Jana, third Jana, fourth Jana, fifth Jana.
But in Visori Maga and in many suitors, only full-fold
method is mentioned.
So we have to adapt this.
So bring all four Janas, bring all five Janas.
So the 10 casinos can bring, if you practice one of the 10 casinos,
you can get first Janas, again, but fourth-fold,
so you can get all five Janas.
And then breathing.
Really always use the first Jana.
No, breathing is there.
Oh, I see.
So first to first, that that is, this kayaga does it,
it means contemplating on different parts of the body.
I've had hair, body hair, new teeth, skin, and so on.
And what's this last one?
Yeah, that is breathing.
I want to say that.
Anna, Bana.
This one, so on.
Oh, P, P, P is this.
Who pass a mano, I'm sorry.
This is P's.
I see.
This one.
The order is a little different.
So this is P's.
And Marana is on death.
Yeah, good, okay.
That is on death.
Okay, now I'm going to try.
10 kinds of fallness together with mindfulness,
but you bite with the body, bring the first Jana.
So 10 as well, first, 10 and this one, kayaga does it.
And we do first Jana only, because the object is gross.
And so you need Vitaka to keep your mind on the object,
but our Vitaka, mind cannot dwell on these objects.
It is explained as when you are going against a strong current,
as you need, you need to, you need some full,
say to keep the boat going.
And in the same way, since the objects are gross,
you need Vitaka for your consciousness,
or mind to be on the object.
Then, fallness, imagination, and you look at the corpse.
And so these objects are gross.
So they lead to first Jana only.
The first three divine abidings bring three Jana's,
that means you have four Jana's, first again, that fourth.
Meta, Karuna, and Modita.
And then the last one, the fourth divine abidings, Upika.
And the four in material stage, Arubavajara,
bring the fourth Jana, that means fifth Jana.
So Upika can lead to fifth Jana,
and these four also to fifth Jana.
So as to the kind of Jana, here to be understood that way.
As to surmounting, surmounting means overcoming.
There are two kinds of surmounting,
surmounting the factors of Jana
and surmounting the object.
That means you have got first Jana.
Now, if you want to get second Jana,
you have to overcome, or you have to eliminate
the first factor, Upika.
So surmounting really means eliminating.
By eliminating factors, you get higher Jana's.
So first you get the first Jana, and it has five factors.
Then if you want to get the second Jana,
you have to eliminate one factor.
Then if you want to get the third Jana,
you have to eliminate one more, one more, and so on.
So the Rubavajara Jana's are those surmounting factors.
And Arubavajara Jana's are those surmounting objects.
Because in all the four Arubavajara Jana,
there are only two factors.
So there is no difference of factors
in the four Arubavajara Jana.
But what makes them different is the object they take.
So if you want to get the first Arubavajara Jana,
you have to overcome, or you have to surmount
the Kacina sign.
Because Kacina sign is the object of Rubavajara Jana's.
Then if you want to get the second Arubavajara Jana,
you have to overcome the first Arubavajara Jana and so on.
So here by surmounting or by eliminating objects,
you get higher Jana's.
So there are two kinds of surmounting.
Some aren't in fact surmounting of factors and surmounting of objects.
Let's put to the side this question.
So basically, these chapters on concentration
are Shamata.
Yes.
And when these chapters on knowledge
are we'll be reposting it and they're both meditations.
But because I guess the problem is,
I think of the one you think of Samadhi.
Samadhi also concludes the posthumment.
Doesn't the exact same thing?
And this concentration is the translation of Samadhi.
So I get, that's where I get confused
when I think this chapter is on meditation.
But actually the third chapter is also on meditation.
Yeah, actually the third chapter is on meditation.
I mean, the third, the third, the third, the third block.
Yes.
No.
Beginning with this chapter, the third chapter,
we come to meditation.
But the third chapter is just the preparation.
You have not gone into meditation,
but you have to purify your virtue.
And then you have to cut the impediments.
And then you find it easier, get the meditation subject,
and so on.
So you're preparing for the practice of meditation in this chapter.
You have not come to the real practice yet.
And this meditation is the Samatam meditation.
So until maybe chapter 13, at the end of chapter 13,
you know, is chapter 14 begins on the third section.
That's where the third section will do.
So from the, actually chapter 14 does not deal with Vipasana yet.
But the basis for Vipasana.
So we, Vipasana actually begins with 15.
Perfectation of you.
That's right.
Chapter 8.
Let me see.
Yes.
It is a case of view, chapter 18.
That's what we begin to get for possible.
From there we get Vipasana.
So this is a point of confusion that I always
come up against, because when I think of meditation or samadhi,
I think of both Shamata and Vipasana.
But in this, this, in the Vasulimago,
when you talk about samadhi, they're actually just talking about
Shamata.
That's right.
Yes.
The three steps.
Sila, samadhi, and banya.
So samadhi is, samadhi is here, synonymous with samadha meditation.
But we need samadhi and Vipasana meditation too.
But that's a technical term.
Then samadhi means we, I mean, samadha meditation.
Because, you know, it's where Zen, the meditate,
which, and we think ourselves in a meditation school.
But in a sense, we're actually a wisdom school in this
ranging because we do the pastana.
We're cross-drilled for the past and Samadhi in that.
Yeah.
And as to extension and non-extension,
that means, whether you can extend the sign you have got into
your mind or whether it is not possible or whether it is not
beneficial to extend.
So, when you, when you have got the image of the, for example,
when you have got the image of the casina in your mind,
then you can expand it.
First, the casina, casina just may be about, say,
eight inches, a diameter or ten inches diameter.
You look at it and you try to get it in your mind.
And when you have really got the image in your mind,
and you can see it with your eyes closed,
then you develop on it again and again.
And when it becomes clearer, refined,
then you can expand it for as large as you like.
So, some subjects of meditation can be expanded and some cannot.
So, there are ten casinas among those body subjects need be
extended.
That means, you can extend, you need to extend them because
it is by way of extending the sign of the counterpart sign,
that you become able to hear a sound or see things in that area.
So, suppose you extend your...
