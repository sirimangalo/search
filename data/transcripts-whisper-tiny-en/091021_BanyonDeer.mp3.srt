1
00:00:00,000 --> 00:00:18,120
Today, I thought I would give a retelling of either two or three of the jot to go stories.

2
00:00:18,120 --> 00:00:28,200
Because I think these stories are quite beneficial as allegories, you could say, or as

3
00:00:28,200 --> 00:00:33,360
good examples or things to keep in mind, they're like the Asop's famous, which we can

4
00:00:33,360 --> 00:00:40,920
always call to mind and use as a reminder of some principle or some moral.

5
00:00:40,920 --> 00:00:44,880
But we find about the jot to get tails is actually there.

6
00:00:44,880 --> 00:00:53,000
They can be quite profound and often go far beyond the stories of Asop, which were often

7
00:00:53,000 --> 00:00:59,720
cute or practical, like how to get by in the world.

8
00:00:59,720 --> 00:01:07,320
These ones are much more on the level of virtue and spiritual advancement.

9
00:01:07,320 --> 00:01:13,720
So while they have the same sort of flavors, Asop, they go a lot deeper in their treatment

10
00:01:13,720 --> 00:01:27,000
of morality and virtue and development of the mind and development of the spiritual path.

11
00:01:27,000 --> 00:01:31,400
So I have three here, and I'm not sure if I'll get through them all in the time that I've

12
00:01:31,400 --> 00:01:36,400
allotted myself, but I'll just go until times opened.

13
00:01:36,400 --> 00:01:39,000
Then I'll stop.

14
00:01:39,000 --> 00:01:44,320
So the first one is one that is quite well-known among Buddhist circles.

15
00:01:44,320 --> 00:01:51,600
It's the Negrodemiga-Chatika, which is the Banyan deer, I believe it's translated as

16
00:01:51,600 --> 00:01:59,240
Miga means deer, Chatika is the path that means the past life or the life or the birth,

17
00:01:59,240 --> 00:02:06,200
a certain birth when the Buddha was born as a, the Bodhisattva was born as a deer of

18
00:02:06,200 --> 00:02:11,520
the Negrodem type, and I guess this is a Banyan deer.

19
00:02:11,520 --> 00:02:18,960
So the story starts, keep only with the Banyan deer, and this is the moral, which is going

20
00:02:18,960 --> 00:02:20,040
to come at the end.

21
00:02:20,040 --> 00:02:25,880
So the first, the Jatika's were made up of both prose and verse, and the verse is what

22
00:02:25,880 --> 00:02:34,360
was kept, strictly recited and remembered, and the prose was perhaps modified or even added

23
00:02:34,360 --> 00:02:42,400
to later on, but who knows some of it could be exactly word-for-word, but it seems likely

24
00:02:42,400 --> 00:02:53,360
that it was not recited as strictly as the verses, and so it was just made up based

25
00:02:53,360 --> 00:02:59,120
on the verses, or reconstructed based on the verses, because the verses hold the core of

26
00:02:59,120 --> 00:03:00,800
the story.

27
00:03:00,800 --> 00:03:07,800
So this story has one verse, and the verses keep only with the Banyan deer, and so on.

28
00:03:07,800 --> 00:03:12,280
This story was told by the master, the Buddha, well in Jetawana, about the mother of the

29
00:03:12,280 --> 00:03:15,360
elder named Prince Kasapah.

30
00:03:15,360 --> 00:03:20,680
The daughter we learn of a wealthy merchant of Rajika-ha was deeply rooted in goodness,

31
00:03:20,680 --> 00:03:23,280
and scorned Al-Temporal things.

32
00:03:23,280 --> 00:03:28,200
She had reached her final existence, and within her breast, like a lamp in a picture,

33
00:03:28,200 --> 00:03:34,200
flowed her sure hope of winning Arahant ship, which was in full enlightenment.

34
00:03:34,200 --> 00:03:38,520
As soon as she reached knowledge of herself, as soon as she reached came of age, she took

35
00:03:38,520 --> 00:03:43,600
no joy in a worldly life, but yearned to renounce the world.

36
00:03:43,600 --> 00:03:48,520
With this aim she said to her mother and father, my dear parents, my heart takes no joy

37
00:03:48,520 --> 00:03:54,160
in a worldly life, feign what I embrace the saving doctrine of the Buddha, suffer me to

38
00:03:54,160 --> 00:04:01,760
take the vows, what my dear, ours is a very wealthy family, and you are our only daughter.

39
00:04:01,760 --> 00:04:04,640
You cannot take the vows.

40
00:04:04,640 --> 00:04:06,560
This means becoming then.

41
00:04:06,560 --> 00:04:10,640
Having failed to win her parents' consent, though she asked them again and again, she

42
00:04:10,640 --> 00:04:14,080
thought to herself, be it so, then.

43
00:04:14,080 --> 00:04:19,200
When I am married into another family, I will gain my husband's consent and take the vows.

44
00:04:19,200 --> 00:04:22,240
And when being grown up, she entered another family.

45
00:04:22,240 --> 00:04:27,320
She proved a devoted wife and lived a life of goodness and virtue in her new home.

46
00:04:27,320 --> 00:04:33,520
Now it came to pass that she conceived of a child, though she knew it not.

47
00:04:33,520 --> 00:04:37,680
There was a festival proclaimed in that city and everybody kept holiday, the city being

48
00:04:37,680 --> 00:04:43,120
decked like a city of the gods, but she, even at the height of the festival, neither

49
00:04:43,120 --> 00:04:49,040
anointed herself nor put on any finery, going about in her everyday attire.

50
00:04:49,040 --> 00:04:53,600
So her husband said to her, my dear wife, everybody is in holiday-making, but you do not

51
00:04:53,600 --> 00:04:56,360
put on your bravery.

52
00:04:56,360 --> 00:05:02,240
My lord and master, she replied, the body is filled with two and thirty component parts.

53
00:05:02,240 --> 00:05:04,600
Wherefore should it be adorned?

54
00:05:04,600 --> 00:05:08,560
This bodily frame is not of angelic or archangelic mold.

55
00:05:08,560 --> 00:05:12,400
It is not made of gold, jewels, or yellow sandalwood.

56
00:05:12,400 --> 00:05:17,800
It takes not its birth from the womb of a lotus flower, white or red or blue.

57
00:05:17,800 --> 00:05:24,040
It is not filled with any immortal balsam, nay, it is a bread of corruption, and born

58
00:05:24,040 --> 00:05:25,920
of mortal parents.

59
00:05:25,920 --> 00:05:29,960
The qualities that market are the wearing and wasting away, the decay and destruction

60
00:05:29,960 --> 00:05:32,720
of them merely transient.

61
00:05:32,720 --> 00:05:37,240
It is faded to swell a graveyard and it is devoted to lusts.

62
00:05:37,240 --> 00:05:41,080
It is the source of sorrow and the occasion of lamentation.

63
00:05:41,080 --> 00:05:46,560
It is the abode of all diseases and the repository of the workings of karma.

64
00:05:46,560 --> 00:05:49,320
All within it is always excreting.

65
00:05:49,320 --> 00:05:56,040
Yay, as all the world can see, it's end is death, passing to the chinal house, there to

66
00:05:56,040 --> 00:05:58,920
be the dwelling place of worms.

67
00:05:58,920 --> 00:06:02,960
What should I achieve my bridegroom by tricking out this body?

68
00:06:02,960 --> 00:06:10,680
What not its adornment be like decorating the outside of a closed stool of a toilet?

69
00:06:10,680 --> 00:06:13,200
My dear wife rejoined the young merchant.

70
00:06:13,200 --> 00:06:19,440
If you regard this body as so sinful, why don't you become a nun?

71
00:06:19,440 --> 00:06:24,120
If I am accepted by husband, I will take the vows this very day.

72
00:06:24,120 --> 00:06:28,720
Very good, said he, I will get you admitted into the order.

73
00:06:28,720 --> 00:06:33,800
And after he had shown lavish bounty and hospitality to the order of monks, he escorted her

74
00:06:33,800 --> 00:06:39,080
with a large following to the nunnery and had her admitted as a sister.

75
00:06:39,080 --> 00:06:43,920
Instead of the following of David Datta, who was the archandomy of the Buddha and bent on

76
00:06:43,920 --> 00:06:48,840
creating a schism in the Sangha, great was her joy at the fulfillment of her desire

77
00:06:48,840 --> 00:06:51,280
to become a sister.

78
00:06:51,280 --> 00:06:57,320
As her time for giving birth grew near, the sisters, noticing the change in her person,

79
00:06:57,320 --> 00:07:03,320
the swelling in her hands and feet and her increased size said, lady, you seem about to become

80
00:07:03,320 --> 00:07:04,320
a mother.

81
00:07:04,320 --> 00:07:06,480
What does it mean?

82
00:07:06,480 --> 00:07:10,880
I cannot tell ladies, I only know I have led a virtuous life.

83
00:07:10,880 --> 00:07:15,480
So the sisters brought her before David Datta saying, Lord, this young gentleman who was

84
00:07:15,480 --> 00:07:21,000
admitted a sister with the reluctant consent, reluctant consent of her husband has now proved

85
00:07:21,000 --> 00:07:25,800
to be with child, but whether this dates from before her admission to the order or not,

86
00:07:25,800 --> 00:07:30,960
we cannot say, what are we to do now?

87
00:07:30,960 --> 00:07:37,040
Not being a Buddha and not having any charity love or pity, David Datta thought this.

88
00:07:37,040 --> 00:07:42,000
It will be a damaging report to get a broad that one of my sisters is with child, and

89
00:07:42,000 --> 00:07:53,080
that I condone the offense for the sexual act was a fence entailing expulsion from the order.

90
00:07:53,080 --> 00:07:56,760
My course is clear, I must expel this woman from the order.

91
00:07:56,760 --> 00:08:03,640
Many inquiry, starting forward as if to thrust aside a mast of stone, he said away and

92
00:08:03,640 --> 00:08:07,800
expel this woman.

93
00:08:07,800 --> 00:08:12,120
Receiving this answer, they arose and with reference, salutation, withdrew to their

94
00:08:12,120 --> 00:08:14,120
own nunnery.

95
00:08:14,120 --> 00:08:18,880
But the girl said to those sisters, ladies, David Datta, the elder is not the Buddha.

96
00:08:18,880 --> 00:08:23,760
My vows were not taken under David Datta, but under the Buddha, the foremost of the world.

97
00:08:23,760 --> 00:08:29,160
Love me not of the vocation I won so hardily, but take me before the master at Jaita

98
00:08:29,160 --> 00:08:31,320
Wanda.

99
00:08:31,320 --> 00:08:35,640
So they set out with her for Jaita Wanda and journeying over the 45 leagues thither from

100
00:08:35,640 --> 00:08:41,320
Rajakaha, came and do course to their destination, where with reverence and salutation

101
00:08:41,320 --> 00:08:45,200
to the master they laid the matter before him.

102
00:08:45,200 --> 00:08:49,800
Thought the master, the Buddha, albeit the child was conceived while she was still of the

103
00:08:49,800 --> 00:08:54,520
laity, yet it will give the heredics an occasion to say that the ascetic goetama has taken

104
00:08:54,520 --> 00:08:57,880
a sister, expelled by David Datta.

105
00:08:57,880 --> 00:09:02,800
Therefore, to cut shorts at tauch, such tauch, this case must be heard in the presence of

106
00:09:02,800 --> 00:09:05,480
the king and his court.

107
00:09:05,480 --> 00:09:11,920
So on the morrow, he sent for Passainadi, king of Kosala, the elder and the younger Anatapindika,

108
00:09:11,920 --> 00:09:17,200
the lady Visaka, the great lady disciple, and other well-known personages, and in the evening

109
00:09:17,200 --> 00:09:23,400
when the four classes of the faithful followers were all assembled, the monks, the nuns,

110
00:09:23,400 --> 00:09:25,560
the laymen, and the laywomen.

111
00:09:25,560 --> 00:09:31,960
He said to the elder Upali, who was the expert in the discipline of the monks, go and

112
00:09:31,960 --> 00:09:37,320
clear up this matter of the young sister in the presence of the four groups of my disciples.

113
00:09:37,320 --> 00:09:43,480
It shall be done, reference sirs of the elder, and forth to the assembly he went and he

114
00:09:43,480 --> 00:09:48,760
went, and there sitting himself in his place he called up Visaka, the lay disciple, inside

115
00:09:48,760 --> 00:09:54,720
of the king, and placed the conduct of the inquiry in her hands, saying, first ascertain

116
00:09:54,720 --> 00:10:01,760
the precise day of the precise month on which this girl joined the order, Visaka, and

117
00:10:01,760 --> 00:10:06,120
then's compute whether she conceived before or since that date.

118
00:10:06,120 --> 00:10:11,480
Accordingly, the lady had a curtain put up as a screen behind what she retired with

119
00:10:11,480 --> 00:10:15,200
the girl.

120
00:10:15,200 --> 00:10:23,800
Spectatus Manimus, Piedeba's umbilical, Ypsoventra, Puyle, whatever that means.

121
00:10:23,800 --> 00:10:28,120
The lady found, on comparing the days and the months, that the conception had taken place

122
00:10:28,120 --> 00:10:31,280
before the girl had become a sister.

123
00:10:31,280 --> 00:10:37,080
This she reported to the elder, who proclaimed the sister innocent before all the assembly.

124
00:10:37,080 --> 00:10:42,080
And she, now that her innocence was established, reverently saluted the order and the

125
00:10:42,080 --> 00:10:48,080
matter, and with the sisters returned to her own nunnery.

126
00:10:48,080 --> 00:10:52,280
When her time was come, she bore the sun, strong in spirit, for whom she had prayed at the

127
00:10:52,280 --> 00:10:56,720
feet of the Buddha, Buddha, Padumudra, ages ago.

128
00:10:56,720 --> 00:11:00,560
One day when the king was passing by the nunnery, he heard the cry of an infant and asked

129
00:11:00,560 --> 00:11:03,280
his courtiers what it meant.

130
00:11:03,280 --> 00:11:07,120
Today, knowing the facts, told his majesty that the cry came from the child to which the

131
00:11:07,120 --> 00:11:09,680
young sister had given birth.

132
00:11:09,680 --> 00:11:15,360
Surs at the king, the care of children as a clog on sisters in their religious life.

133
00:11:15,360 --> 00:11:18,160
Let us take charge of him.

134
00:11:18,160 --> 00:11:22,200
So the infant was handed over by the king's command to the ladies of his family and brought

135
00:11:22,200 --> 00:11:24,000
up as a prince.

136
00:11:24,000 --> 00:11:28,520
When the day came for him to be named, he was called Kasypa, but was known as Prince Kasypa

137
00:11:28,520 --> 00:11:32,200
because he was brought up like a prince.

138
00:11:32,200 --> 00:11:36,120
At the age of seven, he was admitted a novice under the master and a full brother when

139
00:11:36,120 --> 00:11:38,120
he was old enough.

140
00:11:38,120 --> 00:11:42,840
As time went on, he waxed famous among the expounders of the truth, so the master gave

141
00:11:42,840 --> 00:11:45,160
him precedent saying brethren.

142
00:11:45,160 --> 00:11:50,440
The first in eloquence among my disciples is Prince Kasypa.

143
00:11:50,440 --> 00:11:55,000
Afterwards by virtue of the vamikasuta, he won Arahanship.

144
00:11:55,000 --> 00:12:01,400
So to his mother, the sister, grew to clear vision and won the supreme fruit.

145
00:12:01,400 --> 00:12:05,240
This cussip of the elder shone in the faith of the Buddha, even as the full moon in the

146
00:12:05,240 --> 00:12:07,680
mid-heaven.

147
00:12:07,680 --> 00:12:12,720
Now one day in the afternoon when the Duttagata, on return from his arms round, had addressed

148
00:12:12,720 --> 00:12:17,200
the brethren, he passed into his perfume chamber.

149
00:12:17,200 --> 00:12:21,280
At the close of his address, the brethren spent the daytime either in their night-quarters

150
00:12:21,280 --> 00:12:24,160
or in their day-quarters till it was evening.

151
00:12:24,160 --> 00:12:28,280
When they assembled in the hall of truth and spoke his follows.

152
00:12:28,280 --> 00:12:32,800
Brethren, David Duttagata, because he was not a Buddha, and because he had no charity,

153
00:12:32,800 --> 00:12:39,640
love or pity, was nigh being the ruin of the elder Prince Kasypa and his reverend mother.

154
00:12:39,640 --> 00:12:43,840
But the all enlightened Buddha being the Lord of truth and being perfect in charity, love

155
00:12:43,840 --> 00:12:48,680
and pity has proved their salvation.

156
00:12:48,680 --> 00:12:53,120
And as they sat there telling the praises of the Buddha, he entered the hall with all

157
00:12:53,120 --> 00:12:56,920
the grace of a Buddha, and asked as he took his seed.

158
00:12:56,920 --> 00:13:01,480
What was it they were talking of is they sat together.

159
00:13:01,480 --> 00:13:06,880
Of your own virtues, venerable Sir, said they, and told him all.

160
00:13:06,880 --> 00:13:10,720
This is not the first time Brethren said he that the Duttagata has proved the salvation

161
00:13:10,720 --> 00:13:12,760
and refuge of these two.

162
00:13:12,760 --> 00:13:17,040
He was the same to them in the past also.

163
00:13:17,040 --> 00:13:20,960
Then on the Brethren asked skiing him to explain this to them, he revealed what rebirth

164
00:13:20,960 --> 00:13:24,640
had hidden from them.

165
00:13:24,640 --> 00:13:29,200
Once upon a time when Brethren Duttagata was reigning in Baneras, the Bodhisattva was

166
00:13:29,200 --> 00:13:31,760
born a deer.

167
00:13:31,760 --> 00:13:36,000
At his birth he was golden of hue, his eyes were like round jewels.

168
00:13:36,000 --> 00:13:39,400
The sheen of his horns were as of silver.

169
00:13:39,400 --> 00:13:42,440
His mouth was red as a bunch of scarlet cloth.

170
00:13:42,440 --> 00:13:45,320
His forehooves were as though lacquered.

171
00:13:45,320 --> 00:13:50,440
His tail was like the yaks, and he was as big as a young fool.

172
00:13:50,440 --> 00:13:56,240
And by five hundred deer he dwelt in the forest under the name of King Banyan deer.

173
00:13:56,240 --> 00:14:00,200
And hard by him dwelt another deer, also with an attendant herd of five hundred deer

174
00:14:00,200 --> 00:14:07,640
who was named Branch deer, and was as golden of hue as the Bodhisattva.

175
00:14:07,640 --> 00:14:12,120
In those days the king of Baneras was passionately fond of hunting, and always had meet

176
00:14:12,120 --> 00:14:14,560
at every meal.

177
00:14:14,560 --> 00:14:19,200
Every day he mustered the whole of his subjects, townsfolk and country folk alike, to

178
00:14:19,200 --> 00:14:23,760
the detriment of their business, and when hunting.

179
00:14:23,760 --> 00:14:28,840
Thought as people this king of ours stops at all our work, suppose we were to sow food

180
00:14:28,840 --> 00:14:34,080
and supply water for the deer in his own pleasant, and having driven in a number of deer

181
00:14:34,080 --> 00:14:38,200
to bar them in and deliver them over to the king.

182
00:14:38,200 --> 00:14:42,360
So they sowed in the pleasant grass for the deer to eat, and supplied water for them

183
00:14:42,360 --> 00:14:46,440
to drink, and opened the gate wide.

184
00:14:46,440 --> 00:14:50,760
When they called out the townsfolk and set out into the forest, armed with sticks

185
00:14:50,760 --> 00:14:54,520
and all manners of weapons to find the deer.

186
00:14:54,520 --> 00:14:59,280
They surrounded about a league of forest in order to catch the deer within their circle,

187
00:14:59,280 --> 00:15:04,040
and in so doing surrounded the hunt of the Banyan and Branch deer.

188
00:15:04,040 --> 00:15:08,000
As soon as they perceive the deer they proceeded to beat the trees, bushes, and ground

189
00:15:08,000 --> 00:15:12,600
with their sticks till they drove the herds out of their layers.

190
00:15:12,600 --> 00:15:16,560
Then they rattled their swords and spears and bows with so great a din that they drove

191
00:15:16,560 --> 00:15:22,040
all the deer into the pleasant, and shot the gate.

192
00:15:22,040 --> 00:15:27,680
Then they went to the king and said, sire, you put a stop to our work by always going

193
00:15:27,680 --> 00:15:33,320
a hunting, so we have driven deer enough from the forest to fill your pleasant, henceforth

194
00:15:33,320 --> 00:15:37,600
feed on them.

195
00:15:37,600 --> 00:15:43,080
Hereupon the king we took himself to the pleasant, and in looking over the herd saw among

196
00:15:43,080 --> 00:15:50,480
them two golden deer, to whom he granted immunity.

197
00:15:50,480 --> 00:15:54,160
Sometimes he would go of his own accord and shoot a deer to bring home, sometimes the

198
00:15:54,160 --> 00:15:56,760
cook would go and shoot one.

199
00:15:56,760 --> 00:16:01,040
At first sight of the bow the deer would dash off trembling for their lives, but after

200
00:16:01,040 --> 00:16:05,960
receiving two or three wounds they grew weary and faint and were slain.

201
00:16:05,960 --> 00:16:10,640
The herd of deer told this to the Bodhisattva who sent for branches at friend.

202
00:16:10,640 --> 00:16:15,080
The deer are being destroyed in great numbers, and though they cannot escape death at

203
00:16:15,080 --> 00:16:19,240
least let them not be heatlessly wounded.

204
00:16:19,240 --> 00:16:25,640
Let the deer go to the block by turns, one day one from my herd, and next day one from yours.

205
00:16:25,640 --> 00:16:30,200
The deer on whom the lot falls to go, to the place of execution and lie down with its head

206
00:16:30,200 --> 00:16:37,400
on your block, must do so.

207
00:16:37,400 --> 00:16:40,840
In this wise the deer will escape wounding.

208
00:16:40,840 --> 00:16:45,160
The other agreed and then forth the deer whose turn it was, used to go and lie down with

209
00:16:45,160 --> 00:16:49,120
its neck, ready on the block.

210
00:16:49,120 --> 00:16:55,240
The cook used to go and carry off only the victim which awaited them.

211
00:16:55,240 --> 00:17:00,000
Now one day the lot fell on a pregnant dove of the herd of branch, and she went to branch

212
00:17:00,000 --> 00:17:03,960
and said, Lord, I am with young.

213
00:17:03,960 --> 00:17:09,920
When I have brought forth my little one there will be two of us to take our turn, or

214
00:17:09,920 --> 00:17:12,480
to me to be passed over this turn.

215
00:17:12,480 --> 00:17:16,600
No, I cannot make your turn another," said he.

216
00:17:16,600 --> 00:17:21,560
You must bear the consequences of your own fortune, be gone.

217
00:17:21,560 --> 00:17:26,920
Having no favor with him, the doe went on to the Bodhisatta and told him her story, and

218
00:17:26,920 --> 00:17:36,680
he answered, very well, you go away, and I will see that the turn passes over you.

219
00:17:36,680 --> 00:17:41,120
And therewith all he went himself to the place of execution and lay down with his own

220
00:17:41,120 --> 00:17:43,760
head on the block.

221
00:17:43,760 --> 00:17:46,080
Crying the cook on seeing him, why?

222
00:17:46,080 --> 00:17:50,760
Here's the king of the deer who was granted immunity, what does this mean?

223
00:17:50,760 --> 00:17:54,200
And off he ran to tell the king.

224
00:17:54,200 --> 00:17:59,520
The moment he heard of it, the king mounted his chariot and arrived with a large following.

225
00:17:59,520 --> 00:18:04,520
My friend, the king of the deer, he said, on beholding the Bodhisatta, did I not promise

226
00:18:04,520 --> 00:18:09,680
you your life how comes it that you are lying here?

227
00:18:09,680 --> 00:18:15,120
Sire the Bodhisatta replied, there came to me a doe, big with young who prayed me to let

228
00:18:15,120 --> 00:18:20,680
her turn fall on another, and as I could not pass the doom of one on to another, I

229
00:18:20,680 --> 00:18:28,120
lay down my own life for her and taking her doom on myself have laid me down here.

230
00:18:28,120 --> 00:18:33,560
Think not that there is anything behind this, your majesty.

231
00:18:33,560 --> 00:18:39,480
My lord, the golden king of the deer is that the king.

232
00:18:39,480 --> 00:18:48,000
Never yet saw I, even among men, one so abounding in charity love and pity is you.

233
00:18:48,000 --> 00:18:55,560
Before I am pleased with you, arise I spare the lives above you end of her.

234
00:18:55,560 --> 00:19:02,000
Though to be spared which other rest do, O king of men, I spare them their lives too,

235
00:19:02,000 --> 00:19:04,080
my lord.

236
00:19:04,080 --> 00:19:07,680
Sire only the deer in your pleasant will thus have gained immunity which shall all the rest

237
00:19:07,680 --> 00:19:09,320
do.

238
00:19:09,320 --> 00:19:12,680
Their lives too I spare, my lord.

239
00:19:12,680 --> 00:19:16,320
Sire, dear will thus be safe, what would have but the rest of the four could it footed

240
00:19:16,320 --> 00:19:18,640
creatures, what would they do?

241
00:19:18,640 --> 00:19:21,760
I spare them their lives too.

242
00:19:21,760 --> 00:19:26,320
Sire, four would have creatures will thus be safe, what would the flocks and birds do?

243
00:19:26,320 --> 00:19:28,720
They too shall be spared.

244
00:19:28,720 --> 00:19:34,520
Sire, birds will thus be safe, but what would the fishes do who live in the water?

245
00:19:34,520 --> 00:19:39,520
I spare their lives also.

246
00:19:39,520 --> 00:19:42,760
After that's interceding with the king for the lives of all creatures, the great being

247
00:19:42,760 --> 00:19:47,880
arose, establishing the king in the five precepts saying, walk in righteousness, great

248
00:19:47,880 --> 00:19:54,960
king, walk in righteousness, and justice towards parents, children, townsmen, and country folk,

249
00:19:54,960 --> 00:20:00,240
so that when this earthly body is dissolved you may enter the bliss of heaven.

250
00:20:00,240 --> 00:20:05,120
Thus with the grace and charm that marks a Buddha did he teach the truth to the king.

251
00:20:05,120 --> 00:20:09,160
A few days he tarried in the pleasant for the king's instruction and then with his attendant

252
00:20:09,160 --> 00:20:15,320
heard he passed to the forest again, and that no brought forth a fawn fair as the opening

253
00:20:15,320 --> 00:20:20,240
bud of the lotus who used to play about with the branched ear.

254
00:20:20,240 --> 00:20:24,280
Seeing this his mother said to him, my child don't go about with him, only go about with

255
00:20:24,280 --> 00:20:30,680
the herd of the banyan deer, and by way of exhortation she repeated the stanza which falls

256
00:20:30,680 --> 00:20:32,720
in this birth.

257
00:20:32,720 --> 00:20:37,920
Keep only to the banyan deer and shunned the branched ears heard, more welcome far as death

258
00:20:37,920 --> 00:20:46,680
my child in banyan's company than in the amplest term of life with branched.

259
00:20:46,680 --> 00:20:51,080
Then forth the deer now in the enjoyment of immunity used to eat men's crops and the men

260
00:20:51,080 --> 00:20:57,000
remembering the immunity granted to them did not dare to hit the deer or drive them away.

261
00:20:57,000 --> 00:21:01,480
So they assembled in the king's courtyard and laid the matter before the king.

262
00:21:01,480 --> 00:21:06,640
Said he, when the banyan deer won my favor I promised him a boon, I will forego my kingdom

263
00:21:06,640 --> 00:21:13,120
rather than my promise, be gone, not a man in my kingdom may harm the deer.

264
00:21:13,120 --> 00:21:16,240
But when this came to the ears of the banyan deer he called his her together and said

265
00:21:16,240 --> 00:21:19,800
hence forth you shall not eat the crops of others.

266
00:21:19,800 --> 00:21:25,200
At having thus forbidden them he sent a message to the men saying from this day forward,

267
00:21:25,200 --> 00:21:31,680
let no husband men fence his field, but merely indicated with leaves tied up around it.

268
00:21:31,680 --> 00:21:36,280
And so we here began a plan of tying up leaves to indicate the fields, and never was

269
00:21:36,280 --> 00:21:39,360
a deer known to trespass on a field so marked.

270
00:21:39,360 --> 00:21:43,960
For thus they had been instructed by the Bodhisattva.

271
00:21:43,960 --> 00:21:48,680
Lasted the Bodhisattva exhort the deer of his herd and thus did he act all his life long

272
00:21:48,680 --> 00:21:56,720
and at the close of life long passed away with them to fare according to his desserts.

273
00:21:56,720 --> 00:22:01,160
The king who abode by the Bodhisattva's teaching and after a life spent in good works passed

274
00:22:01,160 --> 00:22:05,120
away to fare according to his desserts.

275
00:22:05,120 --> 00:22:09,840
At the close of this lesson when the master had repeated that, as now in bygone days

276
00:22:09,840 --> 00:22:14,960
also he had been the salvation of the pair he preached the four noble truths.

277
00:22:14,960 --> 00:22:19,040
He then showed the connection linking together the two stories he had told and identified

278
00:22:19,040 --> 00:22:24,320
the birth by saying, David Atta was the branch deer of those days and his followers

279
00:22:24,320 --> 00:22:27,160
now were the deer's herd.

280
00:22:27,160 --> 00:22:33,680
The nun was the doe and Prince Kasabah was her offspring and Ananda was the king and

281
00:22:33,680 --> 00:22:35,200
I myself was the banyan deer.

