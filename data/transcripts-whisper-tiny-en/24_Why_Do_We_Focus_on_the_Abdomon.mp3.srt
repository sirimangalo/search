1
00:00:00,000 --> 00:00:02,960
Why do we focus on the abdomen?

2
00:00:02,960 --> 00:00:05,760
The abdomen is a physical phenomenon.

3
00:00:05,760 --> 00:00:08,040
It is the elements of motion,

4
00:00:08,040 --> 00:00:11,440
while you are the two as described in Buddhist texts.

5
00:00:11,440 --> 00:00:13,480
Mindfulness of breathing,

6
00:00:13,480 --> 00:00:17,160
anapana sati is technically considered

7
00:00:17,160 --> 00:00:20,640
tranquility, samatha, meditation,

8
00:00:20,640 --> 00:00:23,240
while analysis of the elements,

9
00:00:23,240 --> 00:00:25,560
chatura daktu wa wa tana,

10
00:00:25,560 --> 00:00:29,800
is considered the basis of insight meditation.

11
00:00:29,800 --> 00:00:33,360
All the elements are experienced directly.

12
00:00:33,360 --> 00:00:36,280
The breath itself is a concept.

13
00:00:36,280 --> 00:00:41,520
Although watching the abdomen can be thought of as mindfulness of breathing,

14
00:00:41,520 --> 00:00:47,320
it differs from watching in and out breathing in long, important way.

15
00:00:47,320 --> 00:00:50,880
The dakta often leads to tranquility,

16
00:00:50,880 --> 00:00:54,920
rather than directly to seeing the nature of reality.

17
00:00:54,920 --> 00:00:59,120
While the former is sat family in ultimate reality

18
00:00:59,120 --> 00:01:00,640
and thus, when you see it,

19
00:01:00,640 --> 00:01:07,640
to seeing it clearly for what it is.

