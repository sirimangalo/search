1
00:00:00,000 --> 00:00:07,320
There are fun things. I am seeking to understand the state of mind of an R-HOT. Is it a

2
00:00:07,320 --> 00:00:12,640
state of knowing everything about reality or can a subset about reality that could

3
00:00:12,640 --> 00:00:18,120
vary from person to person? What would be the lowest common denominator?

4
00:00:18,120 --> 00:00:22,160
An R-HOT is not someone who knows everything. A fully enlightened Buddha is someone

5
00:00:22,160 --> 00:00:28,760
who has unobstructed knowledge. So it's kind of like knowing everything except it's

6
00:00:28,760 --> 00:00:33,280
knowing whatever you want to know. So apparently the Buddha was able to find an answer

7
00:00:33,280 --> 00:00:40,200
to every question that he posed. Every question that was posed to him. Any knowledge

8
00:00:40,200 --> 00:00:48,000
that was required was an unobstructed access to it. But for an R-HOT, that's not the

9
00:00:48,000 --> 00:00:54,920
case. In fact, the point is that the better point is that no one can know everything

10
00:00:54,920 --> 00:00:59,120
because knowing everything implies knowing two things at once, you can never know more

11
00:00:59,120 --> 00:01:08,800
than one thing. But the Buddha had unobstructed knowledge. An R-HOT only knows four things

12
00:01:08,800 --> 00:01:15,640
and that's the four noble truths. They have the realization of the four noble truths.

13
00:01:15,640 --> 00:01:22,840
More importantly, they know enough to free themselves from suffering. An R-HOT has to let

14
00:01:22,840 --> 00:01:29,840
it go of everything. Only a Buddha has to know everything.

