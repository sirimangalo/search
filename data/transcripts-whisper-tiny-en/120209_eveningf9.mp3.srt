1
00:00:00,000 --> 00:00:19,400
Okay, so before we practice, we have a short pep talk, listening to the dumb eyes.

2
00:00:19,400 --> 00:00:25,440
We learn quite quickly listening to the dumb eyes when you benefit.

3
00:00:25,440 --> 00:00:35,840
Easy to miss and to lose sight of it until you actually hear the dumb when you read the Buddha's teaching

4
00:00:35,840 --> 00:00:43,440
or hear someone give a talk and you realize the benefit, realize it's easy to forget.

5
00:00:43,440 --> 00:01:00,440
So before we practice, we're going to jump, start our brains, get ourselves into the right

6
00:01:00,440 --> 00:01:10,440
ear and then listening to the dumb.

7
00:01:10,440 --> 00:01:17,440
So we've talked about what it means to progress and what we're aiming for and so on.

8
00:01:17,440 --> 00:01:25,440
Another very important aspect of our practice is what to watch out for.

9
00:01:25,440 --> 00:01:28,440
What's going to get in the way of our practice?

10
00:01:28,440 --> 00:01:42,440
It's going to stop us from progressing.

11
00:01:42,440 --> 00:01:53,440
It's something most people don't realize unless you spend lots of time in a meditation center watching meditators come and go.

12
00:01:53,440 --> 00:02:03,440
You're in a monastery watching people come in ordained and try to live their lives in this way.

13
00:02:03,440 --> 00:02:05,440
And everyone has their obstacles.

14
00:02:05,440 --> 00:02:10,440
Everyone has things that come to get in their way.

15
00:02:10,440 --> 00:02:17,440
As a teacher, you have to realize this, you have to.

16
00:02:17,440 --> 00:02:32,440
You're ready to see and to understand and so that you don't get caught up in the meditators' problems and try to fix them and so on.

17
00:02:32,440 --> 00:02:46,440
Do you help the meditators to understand what it is that this is one of these obstructions?

18
00:02:46,440 --> 00:03:03,440
We have to understand that this is part of the course, part of our, part of the challenge.

19
00:03:03,440 --> 00:03:08,440
Something that we have to accept.

20
00:03:08,440 --> 00:03:18,440
These things we call mara.

21
00:03:18,440 --> 00:03:23,440
We call mara means that which causes death.

22
00:03:23,440 --> 00:03:25,440
That's what mara means.

23
00:03:25,440 --> 00:03:27,440
What does it cause death of?

24
00:03:27,440 --> 00:03:30,440
It causes the death of goodness.

25
00:03:30,440 --> 00:03:41,440
I remember correctly the etymology.

26
00:03:41,440 --> 00:03:49,440
That which causes goodness to die.

27
00:03:49,440 --> 00:04:00,440
The mara, that which stops us from doing good deeds.

28
00:04:00,440 --> 00:04:06,440
The things that get in our way and otherwise you might be confused and think,

29
00:04:06,440 --> 00:04:07,440
what do I do about this?

30
00:04:07,440 --> 00:04:08,440
This is the big problem.

31
00:04:08,440 --> 00:04:15,440
The problem is when we take it to be a problem because that's what mara wants.

32
00:04:15,440 --> 00:04:19,440
Like the Buddhist Satan, it's a trickster.

33
00:04:19,440 --> 00:04:22,440
It comes in tricks you into thinking there's a problem.

34
00:04:22,440 --> 00:04:24,440
I think what do I do?

35
00:04:24,440 --> 00:04:31,440
I can't go forward and then you're stuck.

36
00:04:31,440 --> 00:04:39,440
Once you understand that this is mara, this is the illusion.

37
00:04:39,440 --> 00:04:46,440
That's the goal of this thing.

38
00:04:46,440 --> 00:04:53,440
The reason why we're stuck in some sorrow, we keep coming back is because we follow mara.

39
00:04:53,440 --> 00:04:58,440
When some problem comes up we give up.

40
00:04:58,440 --> 00:05:02,440
We feel like we're useless or we can't do it.

41
00:05:02,440 --> 00:05:09,440
Let me give in to mara.

42
00:05:09,440 --> 00:05:12,440
Even when for mara we can all become enlightened very easily.

43
00:05:12,440 --> 00:05:17,440
But every meditator who comes has their own mara.

44
00:05:17,440 --> 00:05:21,440
These things that are blocked when they're trying to get in their way.

45
00:05:21,440 --> 00:05:26,440
Some people have so much that they stop.

46
00:05:26,440 --> 00:05:34,440
They can't break through and see that it's just an illusion.

47
00:05:34,440 --> 00:05:36,440
The problem is just an illusion.

48
00:05:36,440 --> 00:05:39,440
The idea of a problem is all in the mind.

49
00:05:39,440 --> 00:05:42,440
That these things are just some power done by there.

50
00:05:42,440 --> 00:05:46,440
Just existential phenomena.

51
00:05:46,440 --> 00:05:49,440
They exist.

52
00:05:49,440 --> 00:05:52,440
They are what they are.

53
00:05:52,440 --> 00:05:54,440
These special things.

54
00:05:54,440 --> 00:05:56,440
Special good things and special bad things.

55
00:05:56,440 --> 00:05:58,440
These are the special bad things that we think.

56
00:05:58,440 --> 00:05:59,440
This is special.

57
00:05:59,440 --> 00:06:04,440
I can acknowledge everything else but I can't acknowledge that it's special.

58
00:06:04,440 --> 00:06:06,440
Those meditators we have to remember.

59
00:06:06,440 --> 00:06:13,440
We have to learn and what it takes time to learn is there's nothing special.

60
00:06:13,440 --> 00:06:16,440
Good things are not special bad things are not special.

61
00:06:16,440 --> 00:06:18,440
They are what they are.

62
00:06:18,440 --> 00:06:21,440
You can acknowledge everything and you should.

63
00:06:21,440 --> 00:06:27,440
When you have a good experience you feel like you're floating or so and you have to acknowledge that.

64
00:06:27,440 --> 00:06:31,440
When you have bad experiences you feel like you're dying.

65
00:06:31,440 --> 00:06:34,440
You have to acknowledge that.

66
00:06:34,440 --> 00:06:36,440
If you want to progress.

67
00:06:36,440 --> 00:06:43,440
If you don't acknowledge it, mara will take you off course.

68
00:06:43,440 --> 00:06:45,440
Good things, mara will take you off course.

69
00:06:45,440 --> 00:06:48,440
Good things as well.

70
00:06:48,440 --> 00:06:51,440
It's like a carrot.

71
00:06:51,440 --> 00:06:52,440
A carrot and a stick.

72
00:06:52,440 --> 00:06:57,440
He uses a carrot to lean you away.

73
00:06:57,440 --> 00:07:01,440
And he uses a stick to chase you away.

74
00:07:01,440 --> 00:07:04,440
This is mara.

75
00:07:04,440 --> 00:07:07,440
So here we talk about what is mara.

76
00:07:07,440 --> 00:07:09,440
There are five kinds of mara.

77
00:07:09,440 --> 00:07:11,440
The first one.

78
00:07:11,440 --> 00:07:13,440
And you know the particular order.

79
00:07:13,440 --> 00:07:15,440
The first one, kandamara.

80
00:07:15,440 --> 00:07:20,440
Kandamara is the one that we are familiar with.

81
00:07:20,440 --> 00:07:23,440
Kandam means aggregates.

82
00:07:23,440 --> 00:07:29,440
So it's the body feelings, memories, thoughts and consciousness.

83
00:07:29,440 --> 00:07:36,440
He is only a kandam.

84
00:07:36,440 --> 00:07:37,440
And these are mara.

85
00:07:37,440 --> 00:07:39,440
These get in our way.

86
00:07:39,440 --> 00:07:46,440
They are very quickly and frequently.

87
00:07:46,440 --> 00:07:48,440
The body gets in our way.

88
00:07:48,440 --> 00:07:53,440
The body sometimes can't walk.

89
00:07:53,440 --> 00:07:55,440
The body gets sick.

90
00:07:55,440 --> 00:08:00,440
They have flu or cold or some disease or so on.

91
00:08:00,440 --> 00:08:03,440
It gets in our way.

92
00:08:03,440 --> 00:08:05,440
Mara.

93
00:08:05,440 --> 00:08:09,440
So people think they can do it because they are sick or so on.

94
00:08:09,440 --> 00:08:13,440
And it means they are following mara.

95
00:08:13,440 --> 00:08:22,440
Giving in tomorrow.

96
00:08:22,440 --> 00:08:25,440
The feelings can be mara when we have pain.

97
00:08:25,440 --> 00:08:27,440
They are very common.

98
00:08:27,440 --> 00:08:32,440
They have a strong pain in you.

99
00:08:32,440 --> 00:08:36,440
But it tastes your way.

100
00:08:36,440 --> 00:08:39,440
This is mara's thick.

101
00:08:39,440 --> 00:08:44,440
This is by extreme pain.

102
00:08:44,440 --> 00:08:59,440
The Buddha made a very bold statement for harsh instruction or hard to follow instruction.

103
00:08:59,440 --> 00:09:05,440
Extreme mandate for us.

104
00:09:05,440 --> 00:09:10,440
And that is that even if we really want to give up our defilements,

105
00:09:10,440 --> 00:09:15,440
even pain that feels like you are going to die, you should be mindful.

106
00:09:15,440 --> 00:09:19,440
Even if it feels like you are going to die.

107
00:09:19,440 --> 00:09:20,440
I've had this before.

108
00:09:20,440 --> 00:09:27,440
If you have been to India, if you are a foreigner going to India, this is something you can often

109
00:09:27,440 --> 00:09:30,440
relate to.

110
00:09:30,440 --> 00:09:36,440
It's also in Thailand where you get this back to your amoebic dysentery.

111
00:09:36,440 --> 00:09:41,440
It's like food poisoning to the extreme.

112
00:09:41,440 --> 00:09:43,440
And some people do die, I think.

113
00:09:43,440 --> 00:09:46,440
But no matter what, you feel like you have been dying.

114
00:09:46,440 --> 00:09:56,440
Pain through your whole body.

115
00:09:56,440 --> 00:09:57,440
This is mara.

116
00:09:57,440 --> 00:09:59,440
It gets in a way.

117
00:09:59,440 --> 00:10:03,440
But in most cases, it's experienced.

118
00:10:03,440 --> 00:10:06,440
You are lying by the problem.

119
00:10:06,440 --> 00:10:09,440
Your back is a very common one.

120
00:10:09,440 --> 00:10:12,440
People who have back problems will find it get in their way.

121
00:10:12,440 --> 00:10:14,440
And they really feel like they can't continue.

122
00:10:14,440 --> 00:10:17,440
They become quite discouraged in the practice.

123
00:10:17,440 --> 00:10:19,440
Because they get the idea.

124
00:10:19,440 --> 00:10:20,440
It's a problem.

125
00:10:20,440 --> 00:10:21,440
It's a problem.

126
00:10:21,440 --> 00:10:22,440
It's a problem.

127
00:10:22,440 --> 00:10:23,440
It's a problem.

128
00:10:23,440 --> 00:10:26,440
It's very difficult.

129
00:10:26,440 --> 00:10:28,440
It's something so foreign to it.

130
00:10:28,440 --> 00:10:31,440
It's quite an extreme.

131
00:10:31,440 --> 00:10:33,440
Mara is not something late.

132
00:10:33,440 --> 00:10:37,440
It's not really just an illusion.

133
00:10:37,440 --> 00:10:40,440
These are real things that can get in your way.

134
00:10:40,440 --> 00:10:43,440
But the illusion is that it's a problem.

135
00:10:43,440 --> 00:10:46,440
It depends how you look at it.

136
00:10:46,440 --> 00:10:52,440
If you see it from the right angle, you realize it's like a knot.

137
00:10:52,440 --> 00:10:55,440
A knot is not something that exists.

138
00:10:55,440 --> 00:10:56,440
It's just rope.

139
00:10:56,440 --> 00:11:04,440
If you untie it, the knot is like it was never there.

140
00:11:04,440 --> 00:11:14,440
Totally disappears.

141
00:11:14,440 --> 00:11:18,440
Then you have memory.

142
00:11:18,440 --> 00:11:21,440
Memory is one of the angry memories can get in the way.

143
00:11:21,440 --> 00:11:25,440
The memories of the past, memories of the future.

144
00:11:25,440 --> 00:11:27,440
The future is a...

145
00:11:27,440 --> 00:11:30,440
It's a memories of future means thinking of the future.

146
00:11:30,440 --> 00:11:33,440
Remembering what you're going to do in the future.

147
00:11:33,440 --> 00:11:36,440
If you remember the bad things you've done,

148
00:11:36,440 --> 00:11:39,440
this comes up for meditators quite frequently.

149
00:11:39,440 --> 00:11:43,440
They feel so discouraged because of the bad things they've done.

150
00:11:43,440 --> 00:11:45,440
If they've done bad things to their parents,

151
00:11:45,440 --> 00:11:47,440
they'll often think that they have to go home

152
00:11:47,440 --> 00:11:51,440
and apologize for it to make things right with their parents.

153
00:11:51,440 --> 00:11:53,440
They remember bad relationships they've had

154
00:11:53,440 --> 00:11:55,440
or even recent relationships.

155
00:11:55,440 --> 00:11:57,440
This is very common.

156
00:11:57,440 --> 00:12:00,440
Meditators will run away thinking they have to go fix the problem

157
00:12:00,440 --> 00:12:04,440
now that they understand.

158
00:12:04,440 --> 00:12:07,440
And then they get home and realize that it's not so easy to fix

159
00:12:07,440 --> 00:12:11,440
and they would have been far better off working,

160
00:12:11,440 --> 00:12:14,440
working to change themselves from within,

161
00:12:14,440 --> 00:12:19,440
and then trying to change other people from without.

162
00:12:19,440 --> 00:12:21,440
This is something that people think it's a problem

163
00:12:21,440 --> 00:12:23,440
that's stopping them from practicing.

164
00:12:23,440 --> 00:12:28,440
They have to go home and fix things.

165
00:12:28,440 --> 00:12:36,440
The memories of people needs them to think about what they have to do.

166
00:12:36,440 --> 00:12:48,440
When we have the Sankala, I'm going to get the thoughts on judgments.

167
00:12:48,440 --> 00:12:52,440
This is really the big one because when we start to think

168
00:12:52,440 --> 00:12:56,440
it's a problem, this is what we think of things.

169
00:12:56,440 --> 00:12:58,440
We don't just accept it for what it is.

170
00:12:58,440 --> 00:13:02,440
We have an opinion about it.

171
00:13:02,440 --> 00:13:04,440
We've become partial towards it.

172
00:13:04,440 --> 00:13:07,440
We interpret it as something more.

173
00:13:07,440 --> 00:13:12,440
We experience something good and you interpret it as being special.

174
00:13:12,440 --> 00:13:18,440
We experience something bad and you interpret it as being special.

175
00:13:18,440 --> 00:13:24,440
Always interpreting instead of experiencing.

176
00:13:24,440 --> 00:13:30,440
But this is the nature you can't just explain it away.

177
00:13:30,440 --> 00:13:35,440
You need to work if you want to be free from this kind of interpretation.

178
00:13:35,440 --> 00:13:37,440
You have to work hard.

179
00:13:37,440 --> 00:13:40,440
Keep your mind clear when something comes up, acknowledge it.

180
00:13:40,440 --> 00:13:42,440
Good things, bad things.

181
00:13:42,440 --> 00:13:47,440
Be clear in your mind what it is and see it only as that.

182
00:13:47,440 --> 00:13:49,440
Some kind.

183
00:13:49,440 --> 00:13:52,440
Vinyana, vinyana.

184
00:13:52,440 --> 00:13:54,440
Vinyana is a conductor.

185
00:13:54,440 --> 00:13:56,440
It can be Mars as well.

186
00:13:56,440 --> 00:14:00,440
Seeing, hearing, smiling, tasting, feeling, thinking.

187
00:14:00,440 --> 00:14:02,440
Sounds can be a problem.

188
00:14:02,440 --> 00:14:08,440
Sometimes in meditation centers there's construction going on and meditators become so frustrated.

189
00:14:08,440 --> 00:14:11,440
They have to hear this sound and they try to turn it off.

190
00:14:11,440 --> 00:14:15,440
When I first started meditating, I thought if you meditate enough,

191
00:14:15,440 --> 00:14:18,440
you can turn off the sound and say hearing, hearing.

192
00:14:18,440 --> 00:14:20,440
Eventually, I'll just stop hearing it.

193
00:14:20,440 --> 00:14:21,440
This is what I thought.

194
00:14:21,440 --> 00:14:26,440
And I worked so hard and got such a headache.

195
00:14:26,440 --> 00:14:28,440
That's what I got out of it as a headache.

196
00:14:28,440 --> 00:14:31,440
But many meditators actually run away.

197
00:14:31,440 --> 00:14:35,440
Thinking that they can think of something's wrong.

198
00:14:35,440 --> 00:14:39,440
This isn't a real meditation center where it's a piece of quiet.

199
00:14:39,440 --> 00:14:42,440
There's too much voice.

200
00:14:42,440 --> 00:14:45,440
Maybe they see something they don't like.

201
00:14:45,440 --> 00:14:49,440
Sometimes they see something in the monastery and it sets them off.

202
00:14:49,440 --> 00:14:53,440
Sometimes they're thinking it's bad.

203
00:14:53,440 --> 00:14:58,440
Maybe they see something scary and staying in the cave and you see it's big spider.

204
00:14:58,440 --> 00:15:03,440
Some people actually get very afraid.

205
00:15:03,440 --> 00:15:08,440
And so on.

206
00:15:08,440 --> 00:15:10,440
So this is kandama.

207
00:15:10,440 --> 00:15:15,440
These are just us, our body and our mind can get in our way.

208
00:15:15,440 --> 00:15:20,440
If you break your language on, hurt yourself,

209
00:15:20,440 --> 00:15:22,440
you get sick.

210
00:15:22,440 --> 00:15:28,440
These are things that get in the way and you have to be willing to work with them.

211
00:15:28,440 --> 00:15:31,440
And not give up because of them.

212
00:15:31,440 --> 00:15:46,440
Your second mind's papizan kandama.

213
00:15:46,440 --> 00:15:57,440
These are the actions that we've done in the past.

214
00:15:57,440 --> 00:16:05,440
If we've done bad things to hurt other people.

215
00:16:05,440 --> 00:16:08,440
And we're supposed to give it some harm.

216
00:16:08,440 --> 00:16:10,440
And then kandama and kandama.

217
00:16:10,440 --> 00:16:14,440
Give it some harm means the defilements that we have inside.

218
00:16:14,440 --> 00:16:18,440
These can stop people from practicing.

219
00:16:18,440 --> 00:16:23,440
If you're so angry in the practice that you think something's wrong.

220
00:16:23,440 --> 00:16:28,440
Or you have so much greed and desire for something you think something's wrong.

221
00:16:28,440 --> 00:16:32,440
Some people come and practice and feel so much desire or lust come up.

222
00:16:32,440 --> 00:16:36,440
But they feel like they just can't do it.

223
00:16:36,440 --> 00:16:38,440
But I think more common is anger.

224
00:16:38,440 --> 00:16:41,440
Many people come and they have so much frustration.

225
00:16:41,440 --> 00:16:44,440
This is just making me a mean person.

226
00:16:44,440 --> 00:16:46,440
They think I'm coming here to be a better person.

227
00:16:46,440 --> 00:16:49,440
They're talking about how trying to be a better person.

228
00:16:49,440 --> 00:16:52,440
This is ridiculous because all it's doing is making me such an evil horrible person.

229
00:16:52,440 --> 00:16:55,440
But this is coming from inside of us.

230
00:16:55,440 --> 00:16:58,440
It's not coming from the practice.

231
00:16:58,440 --> 00:17:00,440
The practice is helping us to let go.

232
00:17:00,440 --> 00:17:03,440
And when you let go, it all comes out.

233
00:17:03,440 --> 00:17:07,440
Not forcing it down or forcing it away.

234
00:17:07,440 --> 00:17:09,440
Letting it come up.

235
00:17:09,440 --> 00:17:17,440
And then you see how much is in there.

236
00:17:17,440 --> 00:17:22,440
The key name for more.

237
00:17:22,440 --> 00:17:26,440
Also desire can get in the way you want to do this, want to do that.

238
00:17:26,440 --> 00:17:34,440
And if I go home, I better food or a nice bed, hot shower.

239
00:17:34,440 --> 00:17:37,440
Do this and do that.

240
00:17:37,440 --> 00:17:38,440
Give me some more.

241
00:17:38,440 --> 00:17:43,440
It gets in the way for many meditators.

242
00:17:43,440 --> 00:17:48,480
someone is not truly dedicated to developing themselves to becoming a bad person.

243
00:17:48,480 --> 00:17:52,800
If they don't have the kusa by the inside, sometimes they'll annoy.

244
00:17:52,800 --> 00:18:01,720
Too many kisra, not enough kusa, not enough good things inside, that's quite common.

245
00:18:01,720 --> 00:18:06,320
This is why we try to tell people how horrible it is here, because they may know only

246
00:18:06,320 --> 00:18:13,400
the really people who are really dedicated will come, because really the place isn't

247
00:18:13,400 --> 00:18:19,840
the big problem, the bigger problems inside of us, and they don't have enough confidence

248
00:18:19,840 --> 00:18:25,120
in themselves to come to the forest with leeches and spiders and snakes and scorpions,

249
00:18:25,120 --> 00:18:28,640
and how can they deal with the real problems inside?

250
00:18:28,640 --> 00:18:32,880
How can they ever hope to deal with what is really difficult?

251
00:18:32,880 --> 00:18:40,640
We'll talk about pain, and they'll just say, oh wait, we're not going to deal with that.

252
00:18:40,640 --> 00:18:49,000
We'll think we're crazy, so we need people who can at least deal with spiders and snakes

253
00:18:49,000 --> 00:18:54,560
and so on, and we can help them deal with a really harder thing.

254
00:18:54,560 --> 00:19:00,400
Then I'll be some kara mara, I'll be some kara mara is what we do based on our defilements.

255
00:19:00,400 --> 00:19:06,560
We have defilements, and they've caused us to do bad things in the past, and this gets

256
00:19:06,560 --> 00:19:13,040
in our way as well.

257
00:19:13,040 --> 00:19:18,720
We've heard other people, we've caused problems around others, or we know some people

258
00:19:18,720 --> 00:19:22,720
there was one Sri Lankan man who came all the way from Canada.

259
00:19:22,720 --> 00:19:30,280
I went to Canada to teach, just as I was invited, and I was going any way out of it.

260
00:19:30,280 --> 00:19:34,080
And this one man who came to me said, oh, I want to go to Thailand to practice, if there's

261
00:19:34,080 --> 00:19:39,400
no no come to Sri Lanka, or as I said, you can come to Sri Lanka and he took me up on

262
00:19:39,400 --> 00:19:40,400
it.

263
00:19:40,400 --> 00:19:43,760
He came all the way from Canada just to practice meditation.

264
00:19:43,760 --> 00:19:48,880
But before he came to see me on this way, he stopped to see his family and met a woman

265
00:19:48,880 --> 00:19:57,240
and promised to marry her, and then he came to meditate, and after about a week he started

266
00:19:57,240 --> 00:20:02,160
thinking about her, or someone called, even after a few days, some people, that's why

267
00:20:02,160 --> 00:20:07,040
some people, his family came to tell him that he had to go and sort this out first,

268
00:20:07,040 --> 00:20:12,600
people were all going crazy because he promised to marry this girl.

269
00:20:12,600 --> 00:20:18,800
This was karma, this was his karma that he had performed, gets in the way.

270
00:20:18,800 --> 00:20:23,640
So that's why we tell people, always sort out your issues before you come, sort everything

271
00:20:23,640 --> 00:20:26,240
out before you come.

272
00:20:26,240 --> 00:20:30,760
Because it actually isn't the problem during meditation, but you'll start to think it's

273
00:20:30,760 --> 00:20:31,760
a problem.

274
00:20:31,760 --> 00:20:36,600
Something comes up, oh, I have to go deal with this and you don't really.

275
00:20:36,600 --> 00:20:43,240
But sometimes this man, I couldn't explain to him, I have to, I have to, I have to,

276
00:20:43,240 --> 00:20:46,600
I have to, I know you don't have to.

277
00:20:46,600 --> 00:20:50,480
And then finally, okay, he's going to go and say, okay, then you promise me, he said,

278
00:20:50,480 --> 00:20:51,480
what do I do when I leave?

279
00:20:51,480 --> 00:20:52,480
I said, you come back.

280
00:20:52,480 --> 00:21:02,560
When you come back in two days, and he did come back and he finished the course, that

281
00:21:02,560 --> 00:21:04,960
I made him promise.

282
00:21:04,960 --> 00:21:09,440
So it wasn't his kilesa actually that was stopping him, it was a karma that was stopping

283
00:21:09,440 --> 00:21:10,440
him.

284
00:21:10,440 --> 00:21:16,480
Then he did have to go and fix the karma, and he did come back and finish the course.

285
00:21:16,480 --> 00:21:26,280
But not necessarily, this is, it can be a dangerous, it can be so much that you're starting

286
00:21:26,280 --> 00:21:32,080
to meditate and then like people come and harass you because of bad things you've done.

287
00:21:32,080 --> 00:21:39,720
Like this monk we're talking about who is a younger brother, his wife, younger brother's

288
00:21:39,720 --> 00:21:45,680
wife was afraid they was going to come back and take his fortune back.

289
00:21:45,680 --> 00:21:51,920
So she sent some mercenaries to kill him.

290
00:21:51,920 --> 00:21:55,240
This weekend imagine, I mean from a Buddhist point of view, we imagine this is from his

291
00:21:55,240 --> 00:22:01,240
past lives when he had done bad things, so he had, and now had bad things come to him.

292
00:22:01,240 --> 00:22:09,440
This happens when you practice, sometimes just crazy things happen, like when I got bitten

293
00:22:09,440 --> 00:22:13,320
by a snake, they said right away karma karma, he's got to be something he did in the

294
00:22:13,320 --> 00:22:19,840
past life, because other people go and nothing happens to them but you go, something happens.

295
00:22:19,840 --> 00:22:25,120
That's what they say, but you can see many strange things happen, to meditators, if you

296
00:22:25,120 --> 00:22:31,000
stay in a meditation center, you can see, trying to help people and sometimes you can't

297
00:22:31,000 --> 00:22:37,480
help them, they have karma to do.

298
00:22:37,480 --> 00:22:47,800
Then muchumara, number four, death, I think this one goes, doesn't have to, doesn't

299
00:22:47,800 --> 00:22:55,040
take much explanation, death is something that stops you from practicing.

300
00:22:55,040 --> 00:23:00,680
But it's, so you think, well, it's the point of talking about this, though, if I die

301
00:23:00,680 --> 00:23:04,920
or die, there's not much I can do about it, but there is one thing we can do and that's

302
00:23:04,920 --> 00:23:10,000
work hard now, but they said we should think often about death because it's something

303
00:23:10,000 --> 00:23:17,880
that could come tomorrow, it is something that we have to deal with, something that we

304
00:23:17,880 --> 00:23:32,840
have to be ready for and prepare ourselves constantly, or if you don't want to think

305
00:23:32,840 --> 00:23:37,440
like that, it's kind of more of a bit, the point is that we have to work while we have

306
00:23:37,440 --> 00:23:45,960
time, the problem is you think you have time, we think we have so much time, we would

307
00:23:45,960 --> 00:23:54,360
have said people become negligent because of youth, I think I'm still young now, I'll

308
00:23:54,360 --> 00:24:02,160
do it when I'm older, they become negligent because of health, so you think I'm, I'm

309
00:24:02,160 --> 00:24:13,920
still healthy, they become, they become legal on practice, there's still a lot of time

310
00:24:13,920 --> 00:24:21,360
I'm still quite healthy, I can work, maybe become negligent because of life, thinking

311
00:24:21,360 --> 00:24:29,280
I'm still alive, or not even thinking but like feeling, thinking that they'll be alive

312
00:24:29,280 --> 00:24:42,240
forever, not thinking of that thinking of life, realizing that any time we can die, this

313
00:24:42,240 --> 00:24:45,840
is why the forest can be quite helpful because when you talk about snakes and scorpions

314
00:24:45,840 --> 00:24:52,800
you can be bitten by a scorpion, or bitten by a snake, or stung by a scorpion, and

315
00:24:52,800 --> 00:24:58,240
you usually don't die but who knows, that's why it's useful to practice up on the slab

316
00:24:58,240 --> 00:25:08,880
because you know if you trip and fall you might not get up again, in remote places, the

317
00:25:08,880 --> 00:25:14,720
really extreme monks, they want to practice all day and all night, but at night they're

318
00:25:14,720 --> 00:25:20,080
so tired, so what they'll do is they'll sit on the edge of the cliff, when they're

319
00:25:20,080 --> 00:25:22,760
just sitting meditation, they'll sit on the edge of the cliff and then they know they

320
00:25:22,760 --> 00:25:29,920
won't fall asleep, it's dangerous though because sometimes I'm not tired, I know you can

321
00:25:29,920 --> 00:25:37,960
control, sometimes you might just fall asleep, but apparently this is a useful way of keeping

322
00:25:37,960 --> 00:25:46,640
awake, sit on the edge of the cliff, I don't know if I would try that, seems to need it,

323
00:25:46,640 --> 00:25:56,840
it's not quite under our control, but what we shouldn't do is always be thinking and

324
00:25:56,840 --> 00:26:03,320
always be clear in our minds that death is going to be the ultimate end, death is always

325
00:26:03,320 --> 00:26:07,800
going to get in our way and it does get in people's way and their stories, our teacher

326
00:26:07,800 --> 00:26:16,960
always says, before you ordain no climbing trees, no swimming, no driving, he has these

327
00:26:16,960 --> 00:26:22,800
rules that you can't do a week before you ordain something like that, once you intend

328
00:26:22,800 --> 00:26:33,640
to ordain none of these things, because it happens with some frequency, someone decides

329
00:26:33,640 --> 00:26:38,640
to become a monk and then doing goofing off or something, climb a tree, die, fall and

330
00:26:38,640 --> 00:26:47,320
break their neck, like the day before, go swimming, drown, get in, drive a car and get

331
00:26:47,320 --> 00:26:55,600
an accident, right before they're going to ordain, this is more much more, gets in our

332
00:26:55,600 --> 00:27:00,160
way, so you have to be careful as well and you have to not think that I'll do it tomorrow

333
00:27:00,160 --> 00:27:05,240
or next week or next month, what is important to do, you should do it now, I'll table

334
00:27:05,240 --> 00:27:13,040
a teacher mata, go janya mara namsui, who knows whether death will be tomorrow, the task

335
00:27:13,040 --> 00:27:22,560
should be done today in earnest, who knows whether death will come tomorrow, what you

336
00:27:22,560 --> 00:27:23,560
are.

337
00:27:23,560 --> 00:27:28,240
Number five is called Deva Puta Mara, this is what we know is the mara that we hear about

338
00:27:28,240 --> 00:27:34,040
or Satan, you've ever seen a little Buddha or these movies where they show mara, this is

339
00:27:34,040 --> 00:27:40,840
Deva Puta Mara, it's an angel, mara isn't formed of angel, that would be think normally

340
00:27:40,840 --> 00:27:46,200
of angels, but you know in Christianity they talk about the fallen angel, it's actually

341
00:27:46,200 --> 00:27:55,160
I think borrowed from Buddhism because it's a Buddhist story or Buddhist teaching,

342
00:27:55,160 --> 00:28:02,040
well, it's mixed anyway, but mara is considered to be an angel or a celestial being

343
00:28:02,040 --> 00:28:10,400
a powerful being a man, and so many meditators will have angels come to them or voices come

344
00:28:10,400 --> 00:28:15,040
to them and they think, oh, an angel told me this or told me that and right away they'll

345
00:28:15,040 --> 00:28:21,400
think they have to follow it, many, I've heard many stories from Asian Buddhists who

346
00:28:21,400 --> 00:28:29,960
say they have to do something because an angel told them, so we have to be quick to explain

347
00:28:29,960 --> 00:28:36,440
to them, we have to ask them, how do you know the angel was a good angel, do you not know

348
00:28:36,440 --> 00:28:43,440
that mara is also an angel, the Buddha has the teaching on this, but this we have to be careful

349
00:28:43,440 --> 00:28:52,840
about, this comes to meditators, they will be meditating, suddenly they hear things and

350
00:28:52,840 --> 00:28:59,520
they want to do, we had a monk once who had a voice telling to kill himself, to slit his

351
00:28:59,520 --> 00:29:05,120
wrists, and so he did it, he actually slit his wrists and he didn't die and then it told

352
00:29:05,120 --> 00:29:16,120
him to light himself on fire and so he did it, he lit himself on fire, so people start to

353
00:29:16,120 --> 00:29:22,240
get scared, oh, maybe I'll hear voices, but if you hear voices it's just voices, you hear

354
00:29:22,240 --> 00:29:29,760
my voice or you scared of it, why would you follow it, this is the question, the problem

355
00:29:29,760 --> 00:29:35,800
is not the voice, the problem is when we interpret it, oh, maybe it means something,

356
00:29:35,800 --> 00:29:46,880
yeah, it means you're heard of voice, it means hearing, has a reason, so these are the five

357
00:29:46,880 --> 00:29:52,360
maras, this is something for us to consider in our meditation, we should be clear about

358
00:29:52,360 --> 00:29:56,440
because this will get in our way, it's a special thing that will get in our way, as

359
00:29:56,440 --> 00:30:09,240
soon as we think that it's special, it will stop us in our practice, once we examine

360
00:30:09,240 --> 00:30:16,960
and examine again and verify and reaffirm in our minds that it's only sambala, dhamma,

361
00:30:16,960 --> 00:30:26,360
existential phenomena, they arise and they exist, that's it, it is like that.

362
00:30:26,360 --> 00:30:33,720
Once we can see that, funny things happen, problems clear up, there was someone who was

363
00:30:33,720 --> 00:30:43,680
a funny thing in Thailand, this one meditator came to practice, then her boyfriend contacted

364
00:30:43,680 --> 00:30:47,520
us and said he needed to talk to her, he said meditators aren't allowed to talk to outsiders,

365
00:30:47,520 --> 00:30:54,200
oh, and he was so angry and he said it's a very important thing, he said, if you don't

366
00:30:54,200 --> 00:30:59,200
let me talk to her, I'm going to come out there and I'm going to find her, and he said,

367
00:30:59,200 --> 00:31:04,080
so it's your choice, and I said, well you can do it or you want, I'm not claiming it involved,

368
00:31:04,080 --> 00:31:14,600
and then he contacted us and said, it's been sorted out, sorry about that, when you see

369
00:31:14,600 --> 00:31:19,640
the same thing comes from pain, you think, no, there's nothing you can do, finally you

370
00:31:19,640 --> 00:31:26,640
set your mind and you say, die and die, if I die I die, and you focus on it, boom, disappear,

371
00:31:26,640 --> 00:31:33,320
like it was never there, and so on, many things like this, once you give up, once you let

372
00:31:33,320 --> 00:31:40,400
go, it's this, similarly that really is vivid from the clinging words, like birds clinging

373
00:31:40,400 --> 00:31:46,600
to the side of this cliff, we're so afraid we're going to fall, like birds in a nest,

374
00:31:46,600 --> 00:31:52,240
no, and a mother kicks them in a version, like no, no, don't kick me under the nest,

375
00:31:52,240 --> 00:31:59,360
but the mother knows best, kicks them out and they fly, of course with baby birds it doesn't

376
00:31:59,360 --> 00:32:06,280
always work, sometimes they fall and die, natural selection, but with us we're like this,

377
00:32:06,280 --> 00:32:12,680
we have the wings, we just don't know it, so we're clinging to the side of it, oh no

378
00:32:12,680 --> 00:32:18,360
I'm going to fall, but because you think like that you can't fly, yes you don't fall,

379
00:32:18,360 --> 00:32:26,440
but because of it you can't fly, until we let go completely and accept and really take

380
00:32:26,440 --> 00:32:32,000
to heart this, as our true refuge, this is the point we're seeking refuge in something

381
00:32:32,000 --> 00:32:40,360
that can't be our refuge, once we take the practices our refuge, you say let things

382
00:32:40,360 --> 00:32:46,200
outside and be as they're going to be, let my body be as it's going to be, but my mind

383
00:32:46,200 --> 00:32:54,080
will be clear, take the practices around it, anything that comes mindful of, see it clearly

384
00:32:54,080 --> 00:33:09,680
for what it is, once you do that then mark and kick, no more, no more problems, so this is important

385
00:33:09,680 --> 00:33:14,760
for meditators, important for us all to realize to be aware of when we come to practice

386
00:33:14,760 --> 00:33:23,120
meditation, so there we go, there's the pep talk, no time for the work, we go on to practice mindful

387
00:33:23,120 --> 00:33:52,200
frustration walking and sitting.

