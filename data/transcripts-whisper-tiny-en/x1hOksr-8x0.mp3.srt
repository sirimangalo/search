1
00:00:00,000 --> 00:00:05,440
Good evening and welcome back to our study of the Dhamupada.

2
00:00:05,440 --> 00:00:18,920
Today we continue on with verse number 81, which reads as follows.

3
00:00:18,920 --> 00:00:44,840
Just as a rock that is a single hunk in the sense of having no cracks or a solid piece of granite,

4
00:00:44,840 --> 00:00:56,480
just as the winds don't stir the rock, don't disturb the rock, wind can never make it waver.

5
00:00:56,480 --> 00:01:10,880
Even so a bong in regards to nindha which is blame or criticism insult, the personal

6
00:01:10,880 --> 00:01:20,320
sat which is praise in the face of praise and blame, natsiming and deepandita, the wise to not

7
00:01:20,320 --> 00:01:28,920
quiver, the waver, or not stirred or not moved.

8
00:01:28,920 --> 00:01:39,040
This was given in regards to Lakuntaka Badia, a monk who was short, his name was Badia,

9
00:01:39,040 --> 00:01:46,040
or whether that was his name or not, Badia just means a royal or a high class person

10
00:01:46,040 --> 00:01:55,640
but it could be a name. Yesterday I think we had was also, no, the story that we had yesterday

11
00:01:55,640 --> 00:02:01,120
was similar to the one of Badia Raja, who was a king named Badia.

12
00:02:01,120 --> 00:02:07,480
Maybe a common name, maybe it's an Appalachian, we'll take it as a name I guess.

13
00:02:07,480 --> 00:02:16,880
But they called him Lakuntaka because he was a dwarf, he was short to say the least, he was a dwarf.

14
00:02:16,880 --> 00:02:24,720
And like many dwarves have to go through, he suffered at the hands of the novice monks

15
00:02:24,720 --> 00:02:34,080
and the unconverted, they say, so those who hadn't yet realized the Buddha's teaching

16
00:02:34,080 --> 00:02:40,440
unconverted I think is an old form, the word would be the ordinary people, people who

17
00:02:40,440 --> 00:02:51,720
haven't come to see it on a deeper level that things like taunting and poking fun at people

18
00:02:51,720 --> 00:02:57,640
because of their height, it's not a cool thing to do to those sorts of people.

19
00:02:57,640 --> 00:03:02,160
Back then this sort of thing happened in Thailand when I was there. It's funny the sort

20
00:03:02,160 --> 00:03:07,200
of things that happened, it's a different culture I guess. Like if someone had dark skin

21
00:03:07,200 --> 00:03:11,880
they'd make jokes about them being very black and we were up on a mountain once and one

22
00:03:11,880 --> 00:03:18,840
of the monks said, oh if we leave you here, when we come back the clouds will be all dark.

23
00:03:18,840 --> 00:03:24,800
That sort of thing wouldn't fly in the West and things like calling people fat, they don't

24
00:03:24,800 --> 00:03:29,920
really have a problem with talking about people being fat or that kind of thing.

25
00:03:29,920 --> 00:03:35,400
But so there was one in Thailand, there was one monk who was very short and he was constantly

26
00:03:35,400 --> 00:03:40,080
being harassed by the other monks, just joking and picking on him.

27
00:03:40,080 --> 00:03:46,760
But I think another thing is in Thailand they have thinner thicker skin, which means

28
00:03:46,760 --> 00:03:53,760
they don't respond so strongly to insults or decisions. I don't know. On the other hand

29
00:03:53,760 --> 00:04:03,800
they've also seen monks get up and start fist fights. So yeah, different culture.

30
00:04:03,800 --> 00:04:11,000
But in the time of the Buddha this was also a thing. But the story is it's just a simple

31
00:04:11,000 --> 00:04:17,800
expression of the fact that he didn't respond. He didn't get upset, he didn't lash out

32
00:04:17,800 --> 00:04:28,280
at them, probably because he was an Aran. But it's one of those stories that shows the

33
00:04:28,280 --> 00:04:36,480
expression. It's an example of the expression of an Aran. How an Aran deals with things.

34
00:04:36,480 --> 00:04:39,920
And it's good for us to compare with how we deal with things. If someone picked on your

35
00:04:39,920 --> 00:04:46,720
height or your weight or your color of your skin, that kind of thing, how would you feel?

36
00:04:46,720 --> 00:04:51,960
How do you handle it when someone points out flaws like your teeth are not straight or

37
00:04:51,960 --> 00:05:00,000
your head's going bald? Someone actually said something really funny today. I don't know.

38
00:05:00,000 --> 00:05:07,560
I don't want to poke fun. It was really a nice someone. Everyone's so young here. It's funny.

39
00:05:07,560 --> 00:05:12,160
It kind of surprises me that these people are actually third and fourth year universities

40
00:05:12,160 --> 00:05:17,440
students because they seem very, very young, all of a sudden. And it's like I'm in a time

41
00:05:17,440 --> 00:05:22,720
warp coming back so many years later. But this woman came up to me and a student came

42
00:05:22,720 --> 00:05:30,120
up to me and said, I wanted to know about Buddhism more than to know why I was wearing

43
00:05:30,120 --> 00:05:33,720
robes actually. That's how people start the conversation. But she was interested and she's

44
00:05:33,720 --> 00:05:39,040
probably coming to the piecewalk to her. But she said, how old are you? And I said 36

45
00:05:39,040 --> 00:05:49,160
and she said, oh, you look really good for 36. And I thought 36 is really is old. Kind

46
00:05:49,160 --> 00:05:53,480
of funny. I mean, that was actually a compliment. You know, you could easily go the other

47
00:05:53,480 --> 00:06:00,320
way. I've heard that and people say, oh, you're only 36. And Thailand was like that.

48
00:06:00,320 --> 00:06:05,320
Many people were surprised that I wasn't much older. And actually when I was like 25,

49
00:06:05,320 --> 00:06:11,520
I was already going bald, I think. Like you're only 25, which is of course wouldn't

50
00:06:11,520 --> 00:06:18,720
be good for the ego. But we're very much against holding onto the ego. So it's good to

51
00:06:18,720 --> 00:06:24,400
have these kind of things happen. It could be tested. But it's also good for us to see an

52
00:06:24,400 --> 00:06:37,080
example like this, who wasn't affected by praise or blame. Praise and blame are two of

53
00:06:37,080 --> 00:06:45,880
the eight, what we call Loki and the Marlokadama, the damas of the world, their truths in

54
00:06:45,880 --> 00:06:52,720
the world. And they say, you can't be free from praise and blame. You can't say you're

55
00:06:52,720 --> 00:06:59,920
always going to be praised. They're also going to be translated as the vicissitudes of life.

56
00:06:59,920 --> 00:07:04,480
As in the mongolists with the words, as putasaloka, dami, jitangyasana, kampati. It's

57
00:07:04,480 --> 00:07:10,000
basically the same thing as this verse. The greatest blessing, eight mongolamutamang

58
00:07:10,000 --> 00:07:20,480
is when someone who is touched by the damas of the world, the worldly damas, worldly truths,

59
00:07:20,480 --> 00:07:27,240
the characteristics of the world, the aspects of existence, jitangyasana kampati, whoever's

60
00:07:27,240 --> 00:07:35,560
mind doesn't waver. It's basically the same thing. This is the greatest blessing. And

61
00:07:35,560 --> 00:07:40,280
it's really a good expression of our practice. So to jump right into what this verse means

62
00:07:40,280 --> 00:07:48,120
to our practice, this is really what it's all about. Because if you've started practicing

63
00:07:48,120 --> 00:07:58,360
in this tradition, you can see that the mind is very apt or very apt, very quick to

64
00:07:58,360 --> 00:08:04,840
waver, quick to respond, quick to react. And this is what we mean by wavering. Forget

65
00:08:04,840 --> 00:08:12,080
about praise and blame. Just sitting still, we react to pain, we react to itching, we react

66
00:08:12,080 --> 00:08:17,480
to heat and cold and noise if we're trying to meditate and there's noise, we react to

67
00:08:17,480 --> 00:08:24,120
our thoughts, we react to our emotions, we react to just about everything. So our mind

68
00:08:24,120 --> 00:08:31,200
is constantly wavering. And then it extends into our life as well. Throughout our life,

69
00:08:31,200 --> 00:08:35,600
throughout our daily life, we're worried about this, afraid of that, angry about this,

70
00:08:35,600 --> 00:08:44,160
wanting that tossed and turned by the vicissitudes of life. And so the answer to a lot

71
00:08:44,160 --> 00:08:50,920
of life's problems are simply stop wavering, stop reacting, stand firm like a mountain,

72
00:08:50,920 --> 00:09:06,240
like a stone, stand firm like a rock, unwavering, unmoved, unshaken. I guess it's a little

73
00:09:06,240 --> 00:09:13,280
bit, we have a little bit of a wrong idea, usually, of what it means to not waver. We

74
00:09:13,280 --> 00:09:20,760
think that somehow you have to force yourself not to react or so on. And so we, in ordinary

75
00:09:20,760 --> 00:09:27,160
usage, we talk about people who talk about being strong, stay strong, you know, like

76
00:09:27,160 --> 00:09:35,800
as though you have to repress your feelings and grit and merit or grin and merit or something,

77
00:09:35,800 --> 00:09:43,800
get your teeth, don't react, keep a stiff upper lip, that kind of thing. But that's

78
00:09:43,800 --> 00:09:49,320
not what it means at all. And that's what's so great about insight meditation is it's

79
00:09:49,320 --> 00:09:59,280
a total different category from any kind of forced repression of reactions. And that's

80
00:09:59,280 --> 00:10:03,680
important because what you realize after some time is repression doesn't really work, forcing

81
00:10:03,680 --> 00:10:11,680
yourself to be a good person, forcing yourself to be strong, forcing yourself not to react,

82
00:10:11,680 --> 00:10:18,400
forcing yourself in general, is unsustainable. So we start to get the idea well, just forget

83
00:10:18,400 --> 00:10:23,320
that. It's just drink and be merry because there's no way to repression is not good.

84
00:10:23,320 --> 00:10:30,880
So unknowingly, in modern times we've come to see what the profound thinkers in the Buddhist

85
00:10:30,880 --> 00:10:34,560
time we're also seeing that there's only two extremes and you have to choose between

86
00:10:34,560 --> 00:10:41,880
them. And nobody, very few people are able to see the middle way between this taught

87
00:10:41,880 --> 00:10:48,400
self torture and indulgence. Usually it's one didn't work, so we tried the other one.

88
00:10:48,400 --> 00:10:53,320
The other one didn't work, so we tried the first one. But the Buddha taught the middle

89
00:10:53,320 --> 00:10:57,520
way and that's why it's, that's why this middle way idea is so important. It's the middle

90
00:10:57,520 --> 00:11:04,760
way between these two extremes. It's this not wavering. It's not repressing at all. It's

91
00:11:04,760 --> 00:11:10,960
seeing so clearly that you have nothing, nothing to react about. It's like you already

92
00:11:10,960 --> 00:11:16,080
knew you already expecting it. It doesn't surprise you. It doesn't impact on your mind

93
00:11:16,080 --> 00:11:22,480
at all. It comes totally known and understood into your mind when it arises. You understand

94
00:11:22,480 --> 00:11:27,680
it when it ceases. You understand it and that's all that happens. You were at peace before

95
00:11:27,680 --> 00:11:34,920
it arose. You're at peace after it leaves. This is what we strive for. Not repression,

96
00:11:34,920 --> 00:11:41,160
not forcing, not going somewhere, becoming something, but giving up and letting go and seeing

97
00:11:41,160 --> 00:11:48,960
clearly and understanding the nature of our experience, the nature of reality. And then

98
00:11:48,960 --> 00:11:56,560
when someone calls you a buffalo, you know, I don't have a tail. How can I be a buffalo?

99
00:11:56,560 --> 00:12:01,200
Someone calls you a short? Well, I'm short. I knew that already. So if you're fat and

100
00:12:01,200 --> 00:12:06,720
someone calls you fat, well, yeah, I'm fat. There's no reason to get upset. We're getting

101
00:12:06,720 --> 00:12:18,640
upset doesn't help. It's a flawed reaction. It hurts you. It causes friction between

102
00:12:18,640 --> 00:12:32,640
you and the other party. It doesn't make you any taller. It doesn't change reality.

103
00:12:32,640 --> 00:12:43,320
We get so incensed by these things. Sometimes we're reasonably so. For example, judging

104
00:12:43,320 --> 00:12:47,400
people by the color of their skin is no small thing. It's obviously an incredible amount

105
00:12:47,400 --> 00:12:55,640
of suffering for large numbers of people. And so the same goes for prejudice against

106
00:12:55,640 --> 00:13:01,320
shorter, tall people or fat people, that kind of thing. There's no question that these

107
00:13:01,320 --> 00:13:08,040
novices were wrong. What's just amazing is that in the face of such wrong, there was no anger,

108
00:13:08,040 --> 00:13:18,480
there was no, there was no reaction. There was no backlash. There was no enmity from the

109
00:13:18,480 --> 00:13:23,920
part of la kum taka bandia. And that's what's great about anara hunt is they've let

110
00:13:23,920 --> 00:13:29,480
go. They've risen above this. They have no quarrel with anyone, unless they're able to

111
00:13:29,480 --> 00:13:37,320
live with and be at peace with everyone. And everything, no matter what dhamma's arise,

112
00:13:37,320 --> 00:13:48,960
whether it be praise or blame or fame or infamy or wealth or poverty, happiness or suffering,

113
00:13:48,960 --> 00:13:53,320
put us a little kandami. He did dhamma's in the company. Their mind doesn't wafer. A

114
00:13:53,320 --> 00:14:04,000
soul kang, they are unsaddened. They are free from sorrow. Sorrowless. We're a gentleman

115
00:14:04,000 --> 00:14:12,600
without any stains or stainless, means without any evil in their hearts, came among the safe.

116
00:14:12,600 --> 00:14:17,840
It's true safety. In fact, invincibility, because if nothing can affect you, then no one

117
00:14:17,840 --> 00:14:23,920
can ever hurt you and no circumstance can ever cause you suffering because you don't react.

118
00:14:23,920 --> 00:14:35,080
You don't, you don't waver. You weren't shaken. Unshaken. Eight among alamut among. This

119
00:14:35,080 --> 00:14:46,320
is the highest blessing. And this is why ninda pasan sasu nna samin janti bandita. In regards

120
00:14:46,320 --> 00:14:57,480
to praise and blame, the wise do not wafer are unshaken just like a rock. Like a solid rock

121
00:14:57,480 --> 00:15:06,160
is not shaken by the wind. It's not moved by the wind. Samirati. It's not tossed about

122
00:15:06,160 --> 00:15:14,640
by the wind. So that's dhamma panda for tonight. Thank you all for tuning in. Keep practicing

123
00:15:14,640 --> 00:15:19,160
and…

