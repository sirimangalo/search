1
00:00:00,000 --> 00:00:07,000
While not meditating, should life itself be a continuous meditation, like driving to work,

2
00:00:07,000 --> 00:00:14,000
saying driving, or getting fired, saying angry, angry, etc.?

3
00:00:14,000 --> 00:00:16,000
Yeah, absolutely.

4
00:00:16,000 --> 00:00:25,000
The reason why we can't is the same reason why we're still in a life where we drive cars and get fired from jobs.

5
00:00:25,000 --> 00:00:40,000
A person who is able to do that, where they're mindful all the time, wouldn't have a job, wouldn't be driving a car most likely.

6
00:00:40,000 --> 00:00:50,000
So, in a sense, the question is another one of these conflating two issues, you know, trying to apply one thing that should be applied another thing.

7
00:00:50,000 --> 00:00:57,000
So, you think, wow, I could never imagine being mindful all the time during my life.

8
00:00:57,000 --> 00:01:07,000
And you literally couldn't, because the reason why you're not able to be mindful all the time is you're the greed, anger, and delusion.

9
00:01:07,000 --> 00:01:14,000
That same greed, anger, and delusion is also the reason why you're still in a life that is caught up in the world.

10
00:01:14,000 --> 00:01:22,000
Once you get free from your attachments and your confusions and delusions and views and all that, you'll give up that life.

11
00:01:22,000 --> 00:01:26,000
And so, life becomes quite simple, life becomes about right here and now, now I'm sitting.

12
00:01:26,000 --> 00:01:32,000
I'm sitting, it's easy, now I'm walking, maybe now I'm talking, eating.

13
00:01:32,000 --> 00:01:41,000
And so, it's actually not that complicated, certainly doable.

14
00:01:41,000 --> 00:01:50,000
For, in the meantime, you're just trying to get yourself in that state, you know, bring the meditative state to your life.

15
00:01:50,000 --> 00:01:54,000
Your life is over here with all the craziness, the meditative state is over here.

16
00:01:54,000 --> 00:02:00,000
You're trying to bring them together, and sometimes you're able to bring them together and then they go apart.

17
00:02:00,000 --> 00:02:13,000
It's a matter of training, but eventually, your life is a meditation.

