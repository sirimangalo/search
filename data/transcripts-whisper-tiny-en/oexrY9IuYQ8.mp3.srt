1
00:00:00,000 --> 00:00:03,480
Okay, good evening.

2
00:00:03,480 --> 00:00:09,600
So tonight I'll be answering a question about meditation practice.

3
00:00:09,600 --> 00:00:22,920
But first I wanted to revive an old activity that I used to do and didn't sort of

4
00:00:22,920 --> 00:00:33,640
about the example of my teacher who used to do it, and that is to request, he didn't

5
00:00:33,640 --> 00:00:41,240
really request him or like told us to do it, but request meditators to talk about their

6
00:00:41,240 --> 00:00:50,120
the benefits of the practice, share them with others, something that can be quite inspiring

7
00:00:50,120 --> 00:00:57,720
if you're unsure or even unaware of the benefits of meditation practice, it helps to make

8
00:00:57,720 --> 00:01:08,800
it a little more real to see other people like you going through the practice.

9
00:01:08,800 --> 00:01:19,040
So I'd like to invite Galen to come and just say a few words.

10
00:01:19,040 --> 00:01:29,080
The basic idea is before you started meditating, you know what you were like and how

11
00:01:29,080 --> 00:01:39,880
you were what you're feeling was, and after you've done quite a bit of meditation, especially

12
00:01:39,880 --> 00:01:46,880
the intensive or mostly interested in the intensive courses that you've done, how that

13
00:01:46,880 --> 00:01:56,640
changed the way you were and how you felt.

14
00:01:56,640 --> 00:02:10,160
Well, I think that, you know, it hasn't removed my practice, hasn't removed any of the,

15
00:02:10,160 --> 00:02:16,160
you know, sort of, I guess, issues I would say that I've had over the years or things

16
00:02:16,160 --> 00:02:22,160
about myself that I haven't been able to accept very well or don't want to be part

17
00:02:22,160 --> 00:02:29,440
of my experience, you know, it hasn't gotten rid of any of that, of course.

18
00:02:29,440 --> 00:02:36,640
Even though at times I may wish that it did, but I think that it's definitely helped

19
00:02:36,640 --> 00:02:50,680
to kind of get less rudded when I'm going through things that I don't want to experience.

20
00:02:50,680 --> 00:03:00,080
I know it's a specifically about the courses, especially after my first course I remember

21
00:03:00,080 --> 00:03:10,760
doing a lot of liberation in seeing things come and go, which, you know, it's obviously

22
00:03:10,760 --> 00:03:17,120
a very basic sort of thing, you know, in regular life things come and go as well.

23
00:03:17,120 --> 00:03:24,680
But I think really sitting, you know, sitting there and having absolutely nothing to distract

24
00:03:24,680 --> 00:03:32,760
yourself with, and I mean, you can, you see, like the moment when things come and go, you

25
00:03:32,760 --> 00:03:39,560
know, more or less, you might not be paying attention at that exact moment, but I think

26
00:03:39,560 --> 00:03:45,600
that really changes things and I think especially the fact that you don't have other

27
00:03:45,600 --> 00:03:55,480
things to go to to distract yourself with and to move on with, the helping move on with.

28
00:03:55,480 --> 00:04:02,360
I think that it's a really powerful thing to know that you can, you know, really just

29
00:04:02,360 --> 00:04:09,560
do nothing and in some sense, I think meditation is more doing nothing than, you know,

30
00:04:09,560 --> 00:04:16,640
just sitting there and in another sense, maybe it's not, but of course you're kind of doing

31
00:04:16,640 --> 00:04:17,640
something.

32
00:04:17,640 --> 00:04:27,080
But so yeah, I think that, I think that for me seeing things come and go without distracting

33
00:04:27,080 --> 00:04:34,400
myself or doing anything really about it is pretty powerful and I definitely left that

34
00:04:34,400 --> 00:04:41,200
first retreat feeling that and wanting to talk to, you know, everybody that I knew, I

35
00:04:41,200 --> 00:04:47,560
tried not to get too preachy, but yeah, really wanting to tell people about my experience

36
00:04:47,560 --> 00:04:54,400
and try to influence others into giving it a try.

37
00:04:54,400 --> 00:05:24,360
So the question for tonight is in the general realm of the

38
00:05:24,360 --> 00:05:36,120
topic of bad things that happen due to meditation practice.

39
00:05:36,120 --> 00:05:42,840
So someone asks or comments about how meditation appears to make them, well, they didn't

40
00:05:42,840 --> 00:05:43,840
phrase it quite like that.

41
00:05:43,840 --> 00:05:50,720
It was as a result, something like as a result of meditation, I find myself more selfish

42
00:05:50,720 --> 00:05:56,040
and annoyed by other people and they're wondering if this is something, do they have

43
00:05:56,040 --> 00:06:01,840
to practice other types of meditation as well or is this something that will go away in

44
00:06:01,840 --> 00:06:02,840
time?

45
00:06:02,840 --> 00:06:09,760
So the first quickest answer and something we have to be quite clear about is that it's

46
00:06:09,760 --> 00:06:16,320
not possible for bad things to come from meditation practice.

47
00:06:16,320 --> 00:06:22,840
But it's important, it's important to note that to qualify that by meditation practice

48
00:06:22,840 --> 00:06:25,440
we mean mindfulness.

49
00:06:25,440 --> 00:06:31,080
Certainly it's possible for bad things that happen when you're sitting still with your

50
00:06:31,080 --> 00:06:36,400
eyes closed or walking back and forth, certainly because it hasn't said anything about

51
00:06:36,400 --> 00:06:44,920
how your mind is inclined, but through the practice of mindfulness it's not possible

52
00:06:44,920 --> 00:06:46,280
for bad things to arise.

53
00:06:46,280 --> 00:06:55,200
So it's an interesting and important topic to discuss why it is that it appears to be that

54
00:06:55,200 --> 00:06:56,200
way.

55
00:06:56,200 --> 00:07:02,840
It appears that bad things are happening because your mind is making things worse.

56
00:07:02,840 --> 00:07:09,800
And so we've eliminated one option that bad things happen that are caused by meditation

57
00:07:09,800 --> 00:07:10,800
practice.

58
00:07:10,800 --> 00:07:17,800
Mindfulness, there are three other options of regards to things that you think are

59
00:07:17,800 --> 00:07:22,240
bad and you think are caused by meditation practice.

60
00:07:22,240 --> 00:07:27,040
The first option is those things that you think that are bad that are caused by meditation

61
00:07:27,040 --> 00:07:34,000
practice, but they aren't bad and they aren't caused by meditation practice.

62
00:07:34,000 --> 00:07:36,240
Second option is things that you think that are bad.

63
00:07:36,240 --> 00:07:43,480
So it's a very Buddhist sort of classification if you've ever read any of the suitors.

64
00:07:43,480 --> 00:07:48,560
Things that you think that are bad that are caused, you think that are bad, you think

65
00:07:48,560 --> 00:07:53,960
they're caused by meditation practice and they are bad but they're not caused by meditation

66
00:07:53,960 --> 00:07:55,120
practice.

67
00:07:55,120 --> 00:08:00,160
And the third option, of course, things that you think that are bad and you think they're

68
00:08:00,160 --> 00:08:06,280
caused by the meditation practice and they're caused by them, where do we get, they're

69
00:08:06,280 --> 00:08:12,160
caused by the meditation practice but they're not bad that I repeat myself.

70
00:08:12,160 --> 00:08:14,400
They're not bad.

71
00:08:14,400 --> 00:08:16,640
They're not caused by meditation practice.

72
00:08:16,640 --> 00:08:17,800
They're bad.

73
00:08:19,800 --> 00:08:20,760
They're not caused by meditation practice.

74
00:08:20,760 --> 00:08:22,000
They're not bad.

75
00:08:22,000 --> 00:08:23,840
They're caused by meditation practice.

76
00:08:23,840 --> 00:08:32,720
There's no such thing that things that are both bad and caused by meditation practice.

77
00:08:32,720 --> 00:08:37,680
And regards to the quote question, specifically, there's the third, it's the third one.

78
00:08:37,680 --> 00:08:38,880
Well, it's one of them.

79
00:08:38,880 --> 00:08:40,280
Get myself confused.

80
00:08:40,280 --> 00:08:43,960
One of them is touched upon by the actual specific question,

81
00:08:43,960 --> 00:08:47,240
but it's important to talk about all three.

82
00:08:47,240 --> 00:08:49,480
It's in relation to things that we think that are bad

83
00:08:49,480 --> 00:08:51,840
and we think are caused by meditation practice.

84
00:08:51,840 --> 00:09:00,400
Because it's a part of the struggle that goes on when we practice meditation.

85
00:09:00,400 --> 00:09:04,120
So those things that we think are bad, we think caused by meditation

86
00:09:04,120 --> 00:09:09,280
that are neither bad nor caused by meditation practice.

87
00:09:09,280 --> 00:09:16,320
This is a common and diverse spread of phenomena.

88
00:09:16,320 --> 00:09:21,040
This refers to a lot of the bad things that we have in life

89
00:09:21,040 --> 00:09:22,360
that turn out to not be bad.

90
00:09:22,360 --> 00:09:25,920
And this is a big, basic part of mindfulness practice

91
00:09:25,920 --> 00:09:28,720
is learning to change our perception about things.

92
00:09:31,960 --> 00:09:35,640
Most specifically, and really the only ones that are pertinent

93
00:09:35,640 --> 00:09:40,560
for meditation practice is impermanent suffering in non-self.

94
00:09:40,560 --> 00:09:42,200
And I've talked about this many times.

95
00:09:42,200 --> 00:09:45,720
I think a very important video I did.

96
00:09:45,720 --> 00:09:48,600
I mean, I did it because I thought it was an important topic

97
00:09:48,600 --> 00:09:52,880
to make clear and I encourage people to watch it

98
00:09:52,880 --> 00:09:57,520
on the nature of reality, I think, is the title.

99
00:10:01,120 --> 00:10:04,840
But it relates to those experiences we have.

100
00:10:04,840 --> 00:10:07,960
We think something's wrong, but nothing's wrong.

101
00:10:07,960 --> 00:10:10,200
And it's certainly not caused by meditation practice.

102
00:10:10,200 --> 00:10:13,880
Impermanence is not a rising because of meditation practice,

103
00:10:13,880 --> 00:10:16,560
but it feels like that because our efforts are always

104
00:10:16,560 --> 00:10:21,680
constantly directed in life towards finding stability

105
00:10:21,680 --> 00:10:23,600
impermanence.

106
00:10:23,600 --> 00:10:26,960
And so we're disturbed and we think something's wrong

107
00:10:26,960 --> 00:10:29,560
as we normally do when things are impermanent.

108
00:10:29,560 --> 00:10:31,400
But meditation practice, mindfulness,

109
00:10:31,400 --> 00:10:33,520
is not letting us avoid it.

110
00:10:33,520 --> 00:10:36,320
It's not taking us out of the impermanence

111
00:10:36,320 --> 00:10:38,640
into a state of stability.

112
00:10:38,640 --> 00:10:42,920
It's forcing us to learn to cope with instability

113
00:10:42,920 --> 00:10:47,760
and become accustomed or flexible in the face of instability

114
00:10:47,760 --> 00:10:53,240
and the face of suffering, pain, stress,

115
00:10:53,240 --> 00:10:58,200
in the face of a non-self, which means the uncontrollability

116
00:10:58,200 --> 00:11:07,080
or the instability in substantial insubstantiality of things.

117
00:11:07,080 --> 00:11:13,560
When we're faced with this, we think something's wrong.

118
00:11:13,560 --> 00:11:15,280
Meditation can be quite painful.

119
00:11:15,280 --> 00:11:18,240
I think that's caused by meditation practice.

120
00:11:18,240 --> 00:11:19,320
It can be quite stressful.

121
00:11:19,320 --> 00:11:20,560
The mind feels chaotic.

122
00:11:20,560 --> 00:11:25,160
And we think this meditation, something's wrong.

123
00:11:25,160 --> 00:11:27,880
Even just watching the stomach, we feel like we can't do it

124
00:11:27,880 --> 00:11:32,560
because it's unpleasant sometimes.

125
00:11:32,560 --> 00:11:37,360
It's stuck sometimes, not moving.

126
00:11:37,360 --> 00:11:45,760
But all in its entirety, all of experience

127
00:11:45,760 --> 00:11:46,960
is not a problem.

128
00:11:46,960 --> 00:11:50,960
There's no experience that we could ever have.

129
00:11:50,960 --> 00:11:52,880
That is a problem.

130
00:11:52,880 --> 00:11:54,120
It's not our experiences.

131
00:11:54,120 --> 00:11:55,560
It's our reactions.

132
00:11:55,560 --> 00:11:59,000
Problems come when we sequentialize.

133
00:11:59,000 --> 00:12:01,200
When we create a chain of causation,

134
00:12:01,200 --> 00:12:03,640
when we react, and we react to react,

135
00:12:03,640 --> 00:12:06,280
and we build up a reaction until it explodes

136
00:12:06,280 --> 00:12:09,240
and into action and into doing something about a karma.

137
00:12:12,560 --> 00:12:14,240
That's not what this person is referring to,

138
00:12:14,240 --> 00:12:17,760
but it is an important struggle

139
00:12:17,760 --> 00:12:19,480
that we have in meditation practice.

140
00:12:19,480 --> 00:12:21,400
It's learning to change the way we look at things

141
00:12:21,400 --> 00:12:24,720
from seeing pain as a problem, seeing

142
00:12:24,720 --> 00:12:30,920
impermanence as a problem, to learn to be flexible and adaptable.

143
00:12:30,920 --> 00:12:35,520
And familiar with the vicissitudes of life,

144
00:12:35,520 --> 00:12:37,640
the potential for suffering, and the fact

145
00:12:37,640 --> 00:12:39,440
that we can't control things so that instead

146
00:12:39,440 --> 00:12:42,960
of trying to control, we learn to see them just as they are.

147
00:12:45,960 --> 00:12:48,960
The second category, things that get these straight,

148
00:12:48,960 --> 00:12:54,000
things that are bad, but they're not caused by meditation

149
00:12:54,000 --> 00:12:56,640
practice, that's to the other one first.

150
00:12:56,640 --> 00:13:00,520
Things that are not bad, but are caused by the meditation practice.

151
00:13:00,520 --> 00:13:01,840
We think they're bad, we think they're caused

152
00:13:01,840 --> 00:13:03,560
by the meditation practice.

153
00:13:03,560 --> 00:13:05,360
They are caused by the meditation practice,

154
00:13:05,360 --> 00:13:06,600
but they're not bad.

155
00:13:08,640 --> 00:13:11,520
This, I think, is a smaller and actually smaller subset

156
00:13:11,520 --> 00:13:13,200
of phenomena.

157
00:13:15,160 --> 00:13:19,880
I think a common one for meditators is the fear of letting go.

158
00:13:21,400 --> 00:13:23,920
And it really only, these things really only come up

159
00:13:23,920 --> 00:13:26,440
in the beginning, because it's hard to imagine

160
00:13:26,440 --> 00:13:29,520
from our perspective something that you can perceive

161
00:13:29,520 --> 00:13:31,720
as bad, that comes from the meditation practice,

162
00:13:31,720 --> 00:13:35,160
since the things that come actually come from meditation,

163
00:13:35,160 --> 00:13:39,360
mindfulness, practice are so good, but it does happen

164
00:13:39,360 --> 00:13:41,520
in the beginning, it can happen in the beginning,

165
00:13:41,520 --> 00:13:44,160
that as you begin to let go, it can be quite scary.

166
00:13:45,680 --> 00:13:47,840
You start to get afraid and concerned,

167
00:13:47,840 --> 00:13:51,640
and I feel like you don't know yourself,

168
00:13:51,640 --> 00:13:55,040
even, where is this leading?

169
00:13:55,040 --> 00:13:59,080
You read about, often read about in Buddhist texts,

170
00:13:59,080 --> 00:14:01,840
or you hear a talk about letting go,

171
00:14:01,840 --> 00:14:03,960
not clinging to things, and you think all these things

172
00:14:03,960 --> 00:14:06,360
that I like, all these things that I hold on to.

173
00:14:07,480 --> 00:14:09,520
I have to let them go.

174
00:14:09,520 --> 00:14:10,960
And of course, that's not how it works.

175
00:14:10,960 --> 00:14:14,680
You can't be forced, you're never gonna be forced

176
00:14:14,680 --> 00:14:17,680
artificially to let go, because meditation isn't artificial,

177
00:14:17,680 --> 00:14:21,640
it's natural, it's about seeing the truth.

178
00:14:21,640 --> 00:14:25,200
And the truth unfortunately happens to be that

179
00:14:25,200 --> 00:14:26,960
the things you like, the things you want,

180
00:14:26,960 --> 00:14:28,480
are not actually gonna make you happy,

181
00:14:28,480 --> 00:14:30,520
and wanting and liking them,

182
00:14:30,520 --> 00:14:33,400
it's only a cause for more stress and suffering.

183
00:14:36,400 --> 00:14:41,400
A much more common is when others perceive

184
00:14:43,680 --> 00:14:48,680
in the meditator, the changes, because of course,

185
00:14:49,560 --> 00:14:53,880
it's very hard for non-meditators to cope with change.

186
00:14:53,880 --> 00:14:56,160
And so even though the change might be neutral,

187
00:14:56,160 --> 00:15:01,160
any change of an individual is stressful, scary,

188
00:15:03,400 --> 00:15:05,920
but more importantly, a lot of the changes

189
00:15:05,920 --> 00:15:08,000
that meditators undergo are horrific,

190
00:15:08,000 --> 00:15:12,000
and even when they do understand and do perceive

191
00:15:12,000 --> 00:15:16,040
what's happened to the meditator correctly, they're upset

192
00:15:16,040 --> 00:15:20,720
and displeased by the fact that the meditator

193
00:15:20,720 --> 00:15:25,720
is no longer exciting or excited or passionate,

194
00:15:27,880 --> 00:15:32,560
no longer caught up in all the world-y pleasures that they are.

195
00:15:34,560 --> 00:15:37,440
And when this is family members or close friends,

196
00:15:37,440 --> 00:15:39,880
it can make meditators that what they're doing

197
00:15:39,880 --> 00:15:44,120
and be unsure, at least in the beginning.

198
00:15:44,120 --> 00:15:46,560
I don't think it's a long-term concern,

199
00:15:46,560 --> 00:15:48,880
but it certainly can happen for meditators,

200
00:15:48,880 --> 00:15:51,400
people who do a little bit of meditation

201
00:15:51,400 --> 00:15:56,400
and find that there's a conflict with their friends

202
00:15:56,840 --> 00:16:00,920
and family and others who are displeased

203
00:16:00,920 --> 00:16:05,920
and unhappy about the changes that they're going through.

204
00:16:08,960 --> 00:16:10,600
But really of the things, as I said,

205
00:16:10,600 --> 00:16:12,400
that happen in meditation more often,

206
00:16:12,400 --> 00:16:19,400
it's quite liberating and it should feel good

207
00:16:20,360 --> 00:16:23,040
what happens through the meditation practice.

208
00:16:24,520 --> 00:16:26,440
But the third category is the one that I think

209
00:16:26,440 --> 00:16:28,960
is of concern in this question.

210
00:16:28,960 --> 00:16:31,640
That is bad things that we think they're bad,

211
00:16:31,640 --> 00:16:33,440
we think they come through the meditation practice,

212
00:16:33,440 --> 00:16:35,880
they're bad, but they don't actually happen

213
00:16:35,880 --> 00:16:37,080
because we're meditating.

214
00:16:38,920 --> 00:16:41,600
So to understand this, it's a little bit difficult

215
00:16:41,600 --> 00:16:45,720
to understand, we have to understand

216
00:16:45,720 --> 00:16:47,680
the state of someone who doesn't meditate

217
00:16:47,680 --> 00:16:52,000
is always avoiding, there's a sense

218
00:16:52,000 --> 00:16:55,680
in which it's always avoiding bad things.

219
00:16:57,600 --> 00:16:59,640
It may not always appear like that

220
00:16:59,640 --> 00:17:02,560
or it's an odd sort of phrasing,

221
00:17:02,560 --> 00:17:03,760
but from a Buddhist perspective,

222
00:17:03,760 --> 00:17:05,440
that's the key difference.

223
00:17:06,760 --> 00:17:11,160
We have this phrase flight or flight

224
00:17:11,160 --> 00:17:14,200
and whether you're fighting something or fleeing it,

225
00:17:14,200 --> 00:17:17,920
you're avoiding it, you're avoiding from a perspective

226
00:17:17,920 --> 00:17:20,680
of someone who, from a perspective,

227
00:17:20,680 --> 00:17:24,320
in opposition of the perspective of observing

228
00:17:24,320 --> 00:17:28,040
the phenomenon and being mindful of it,

229
00:17:28,040 --> 00:17:29,640
it's a sort of avoidance.

230
00:17:29,640 --> 00:17:34,160
And so building up those habits doesn't allow us

231
00:17:34,160 --> 00:17:36,320
often to appreciate,

232
00:17:38,000 --> 00:17:40,640
first of all, appreciate the nature of our minds

233
00:17:40,640 --> 00:17:43,080
and understand them and be aware

234
00:17:43,080 --> 00:17:44,560
of the problems that are there.

235
00:17:45,520 --> 00:17:47,880
But second of all, it often doesn't allow them

236
00:17:49,040 --> 00:17:53,840
to develop uninhibited, right?

237
00:17:53,840 --> 00:17:57,600
So what I mean by this is sometimes you get angry

238
00:17:57,600 --> 00:17:59,360
and you find a way to avoid the anger.

239
00:17:59,360 --> 00:18:02,920
Now, it's not a very healthy way of dealing with things,

240
00:18:02,920 --> 00:18:10,760
but it's easy and it is temporarily feasible.

241
00:18:10,760 --> 00:18:13,960
You get angry and so you try to make yourself happy

242
00:18:13,960 --> 00:18:16,600
or you go away from the person that's making you unhappy.

243
00:18:16,600 --> 00:18:20,040
That's a good example because it actually is quite wholesome.

244
00:18:20,040 --> 00:18:21,320
Suppose you're angry with someone.

245
00:18:21,320 --> 00:18:23,480
Well, the good idea is to go off into your room

246
00:18:23,480 --> 00:18:26,800
and meditate, but just going off, going for a walk

247
00:18:26,800 --> 00:18:28,840
can be quite beneficial.

248
00:18:28,840 --> 00:18:30,440
Meditation doesn't let you do that.

249
00:18:30,440 --> 00:18:32,600
So there's two sides to this.

250
00:18:32,600 --> 00:18:36,080
One, when you meditate, first of all,

251
00:18:36,080 --> 00:18:38,120
you're going to see lots of things about yourself

252
00:18:38,120 --> 00:18:39,480
that you didn't see before.

253
00:18:40,520 --> 00:18:43,840
And second of all, some of the bad things that are there

254
00:18:43,840 --> 00:18:46,600
are going to have a chance, an opportunity

255
00:18:46,600 --> 00:18:49,600
to present themselves in grow uninhibited

256
00:18:49,600 --> 00:18:51,120
because you're no longer avoiding them.

257
00:18:51,120 --> 00:18:53,360
So just like a person who makes you angry,

258
00:18:53,360 --> 00:18:55,360
when an experience makes you angry,

259
00:18:55,360 --> 00:18:57,640
sticking with it gives you an opportunity to get angry.

260
00:18:57,640 --> 00:18:59,600
That's not a good thing,

261
00:18:59,600 --> 00:19:02,800
but it's not caused by mindfulness.

262
00:19:02,800 --> 00:19:09,800
It grows out of an in-between state

263
00:19:09,800 --> 00:19:12,920
where you're not yet being mindful of it,

264
00:19:12,920 --> 00:19:14,480
but you're no longer trying to escape it.

265
00:19:14,480 --> 00:19:17,760
So just sitting still in getting very angry,

266
00:19:17,760 --> 00:19:21,880
or let's say bored of meditation or frustrated about meditation,

267
00:19:21,880 --> 00:19:25,280
is really going to become a problematic thing.

268
00:19:25,280 --> 00:19:30,520
Once you've this person mentions the interactions

269
00:19:30,520 --> 00:19:34,040
they have with others, and once you stop wanting

270
00:19:34,040 --> 00:19:37,240
to interact with others because of some preliminary insight

271
00:19:37,240 --> 00:19:44,240
into mindfulness, you can give space to the annoyance,

272
00:19:44,240 --> 00:19:48,200
any new sort of annoyance for people.

273
00:19:48,200 --> 00:19:50,040
And that's not mindfulness by any means.

274
00:19:50,040 --> 00:19:51,520
It's the opposite.

275
00:19:51,520 --> 00:19:53,320
It's a reaction.

276
00:19:53,320 --> 00:19:55,760
But you've taken away, you've let your guard down,

277
00:19:55,760 --> 00:20:02,760
or no, in this case you've let go of the sort of the appeasing

278
00:20:02,760 --> 00:20:09,440
of your defilements, which would be engaging

279
00:20:09,440 --> 00:20:14,320
in pleasant conversation or pleasant interactions with others.

280
00:20:14,320 --> 00:20:16,200
But you don't have the level of mindfulness

281
00:20:16,200 --> 00:20:19,480
that allows you to just be with the experience.

282
00:20:19,480 --> 00:20:22,040
So when you're around people, normally,

283
00:20:22,040 --> 00:20:26,280
you could find a way to be pleased by that, engage with them.

284
00:20:26,280 --> 00:20:28,120
And when you don't do that, you know,

285
00:20:28,120 --> 00:20:31,120
say go to the opposite way and get annoyed by them,

286
00:20:31,120 --> 00:20:32,560
you see.

287
00:20:32,560 --> 00:20:34,280
But it's not because of mindfulness.

288
00:20:34,280 --> 00:20:43,280
It's because you're doing some kind of shell,

289
00:20:43,280 --> 00:20:45,480
this shell of the meditation practice.

290
00:20:48,280 --> 00:20:51,920
And so it gives the opportunity for the unpleasant emotions

291
00:20:51,920 --> 00:20:53,880
or all of these unpleasant things,

292
00:20:53,880 --> 00:20:55,120
like the annoyance towards others.

293
00:20:55,120 --> 00:20:55,840
They're a part of you.

294
00:20:55,840 --> 00:20:57,360
They have nothing to do with mindfulness.

295
00:20:57,360 --> 00:21:00,080
They're not a product of being mindful.

296
00:21:00,080 --> 00:21:01,920
But they have the opportunity to arise.

297
00:21:01,920 --> 00:21:05,160
So first, bad things that you didn't see

298
00:21:05,160 --> 00:21:06,680
will come up in meditation.

299
00:21:06,680 --> 00:21:07,320
Absolutely.

300
00:21:07,320 --> 00:21:08,760
That's part of the process.

301
00:21:08,760 --> 00:21:10,600
That's the Buddha acknowledges.

302
00:21:10,600 --> 00:21:14,960
It's part of the realm of the objects to be mindful.

303
00:21:14,960 --> 00:21:16,000
It's under dhammas.

304
00:21:16,000 --> 00:21:21,880
So liking and disliking, things like doubt

305
00:21:21,880 --> 00:21:25,960
confusion, worry, fear, worry is another big one.

306
00:21:25,960 --> 00:21:28,080
You might have never worried before you meditated

307
00:21:28,080 --> 00:21:31,920
about getting angry or clinging to things, right?

308
00:21:31,920 --> 00:21:33,000
When you're clinging, just go for it.

309
00:21:33,000 --> 00:21:34,600
Now you find you're worrying about everything.

310
00:21:34,600 --> 00:21:36,640
You're afraid of everything.

311
00:21:36,640 --> 00:21:37,800
You have worries that you, you know,

312
00:21:37,800 --> 00:21:39,440
before I was a very confident person.

313
00:21:39,440 --> 00:21:41,600
Now I'm worried about everything.

314
00:21:41,600 --> 00:21:43,800
It's not that worry is a good thing.

315
00:21:43,800 --> 00:21:46,040
But you've become aware of the badness of things.

316
00:21:46,040 --> 00:21:51,760
And you're not yet able to cope with or overcome them

317
00:21:51,760 --> 00:21:53,240
or change the way you look at them

318
00:21:53,240 --> 00:21:54,160
so that you let go of them.

319
00:21:54,160 --> 00:21:56,480
And so you're reacting to the bad things

320
00:21:56,480 --> 00:21:58,920
in ways that you wouldn't have reacted before.

321
00:21:58,920 --> 00:22:00,440
Greed, okay, let's go get it.

322
00:22:00,440 --> 00:22:02,040
Anger, okay, let's fight.

323
00:22:03,720 --> 00:22:04,960
So you add worry in there.

324
00:22:04,960 --> 00:22:06,600
You add doubt.

325
00:22:06,600 --> 00:22:07,680
Doubt about what you're doing

326
00:22:07,680 --> 00:22:10,400
because you're doing something new.

327
00:22:10,400 --> 00:22:11,760
It's not because you're being mindful

328
00:22:11,760 --> 00:22:13,480
that you start to have doubt everything.

329
00:22:14,480 --> 00:22:17,040
But it's because you're doing something

330
00:22:17,040 --> 00:22:19,200
that you don't know the truth about.

331
00:22:19,200 --> 00:22:20,840
When you start to meditate, you're not sure.

332
00:22:20,840 --> 00:22:22,120
Is it good, is it right?

333
00:22:25,160 --> 00:22:27,360
And so you give rise to doubt.

334
00:22:27,360 --> 00:22:31,320
So it's important to make the mistake

335
00:22:31,320 --> 00:22:33,080
of thinking that things like annoyance

336
00:22:33,080 --> 00:22:35,760
with others, that that, yeah, that's mean being mindful

337
00:22:35,760 --> 00:22:38,040
and now I'm scoffed at other people

338
00:22:38,040 --> 00:22:39,680
because of how worldly they are.

339
00:22:39,680 --> 00:22:42,080
So absolutely not.

340
00:22:42,080 --> 00:22:44,720
A person who is mindful of it, who is truly mindful,

341
00:22:45,800 --> 00:22:50,360
should never be disturbed by any evil in others,

342
00:22:50,360 --> 00:22:53,680
any, I mean, I include everything

343
00:22:53,680 --> 00:22:56,320
like just being annoying, for example,

344
00:22:56,320 --> 00:22:58,960
or being worldly, talking about things

345
00:22:58,960 --> 00:23:01,240
that are useless, even holding wrong views

346
00:23:01,240 --> 00:23:04,560
if someone starts to say bad things

347
00:23:04,560 --> 00:23:07,720
about Buddhists or say evil things

348
00:23:07,720 --> 00:23:10,600
about other people and be mean and nasty.

349
00:23:10,600 --> 00:23:12,800
It should never bother one.

350
00:23:12,800 --> 00:23:15,680
The Buddha said when people even say bad things

351
00:23:15,680 --> 00:23:20,000
about Buddhism, you get annoyed and upset about that.

352
00:23:20,000 --> 00:23:21,360
You're not following his teachings.

353
00:23:21,360 --> 00:23:23,200
You could never truly understand

354
00:23:23,200 --> 00:23:26,240
and know the truth of what they were saying.

355
00:23:30,920 --> 00:23:35,240
And much more important when you realize this

356
00:23:35,240 --> 00:23:40,080
or having that in mind, when you are annoyed

357
00:23:40,080 --> 00:23:43,720
by annoyed by things or when you find yourself clinging

358
00:23:43,720 --> 00:23:48,000
to things or whatever arises when you meditate,

359
00:23:48,000 --> 00:23:51,760
it of course has to be an object of your meditation practice.

360
00:23:51,760 --> 00:23:54,200
So you find meditation, you say it's making you selfish

361
00:23:54,200 --> 00:23:56,240
and annoyed by others.

362
00:23:56,240 --> 00:23:58,320
That should be the object of mindfulness.

363
00:23:58,320 --> 00:24:02,000
It's not because you're mindful that that's happening.

364
00:24:02,000 --> 00:24:04,800
It's because you're not reacting to it

365
00:24:04,800 --> 00:24:08,440
or finding ways to avoid it.

366
00:24:08,440 --> 00:24:10,600
Normally, if we were ever to feel annoyed,

367
00:24:10,600 --> 00:24:11,920
we would reprimand ourselves and say,

368
00:24:11,920 --> 00:24:16,800
okay, I should enjoy myself and appreciate these people

369
00:24:16,800 --> 00:24:20,360
and get involved in their conversations.

370
00:24:20,360 --> 00:24:22,560
Not saying you should never do that.

371
00:24:22,560 --> 00:24:26,760
In fact, another problem, another problem that's related

372
00:24:26,760 --> 00:24:29,160
is getting wrong ideas about meditation practice.

373
00:24:29,160 --> 00:24:31,640
Like suddenly, you can't engage with other people.

374
00:24:31,640 --> 00:24:34,680
You have to act all stiff and rigid

375
00:24:34,680 --> 00:24:38,320
and that's not true or proper either, I don't think.

376
00:24:38,320 --> 00:24:41,040
So meditators get the wrong idea about meditation.

377
00:24:41,040 --> 00:24:44,960
They become too, they become a meditator, you could say,

378
00:24:44,960 --> 00:24:47,120
and becoming anything, of course, is problematic

379
00:24:47,120 --> 00:24:50,120
because it's artificial and you get this idea

380
00:24:50,120 --> 00:24:53,000
in your mind that you create a personality about.

381
00:24:54,240 --> 00:24:56,480
And then there's acceptable and unacceptable

382
00:24:56,480 --> 00:24:58,120
and when the unacceptable happens,

383
00:24:58,120 --> 00:25:01,080
you react to get upset, you get annoyed.

384
00:25:02,040 --> 00:25:04,080
These people are talking about worldly things

385
00:25:04,080 --> 00:25:06,200
that you shouldn't be doing that.

386
00:25:06,200 --> 00:25:08,480
I don't want to hear this and you get upset about it.

387
00:25:11,600 --> 00:25:12,880
No, rather, when that happens,

388
00:25:12,880 --> 00:25:14,960
you should say to yourself, annoyed, annoyed.

389
00:25:16,160 --> 00:25:17,840
And it's just a part of who you are.

390
00:25:17,840 --> 00:25:20,080
For some people, that would never happen.

391
00:25:20,080 --> 00:25:22,680
Maybe for others, it would be something else.

392
00:25:22,680 --> 00:25:27,080
They would find it hard to not get caught up often.

393
00:25:27,080 --> 00:25:29,600
We find it hard not to get up and get caught up

394
00:25:29,600 --> 00:25:32,920
in useless talk because of our greed.

395
00:25:32,920 --> 00:25:34,360
Some people are full of greed,

396
00:25:34,360 --> 00:25:36,560
some people are more anger.

397
00:25:36,560 --> 00:25:37,840
Some people have more delusion

398
00:25:37,840 --> 00:25:39,240
so they just want to fight with people

399
00:25:39,240 --> 00:25:43,200
and argue with them and so on, that sort of thing.

400
00:25:43,200 --> 00:25:46,800
But either way, there's no such thing as bad things

401
00:25:46,800 --> 00:25:49,400
that happen because of mindfulness, not directly.

402
00:25:51,200 --> 00:25:52,680
So that's the answer to that question.

403
00:25:52,680 --> 00:25:54,720
I think it's an important topic.

404
00:25:54,720 --> 00:25:55,840
Thank you for asking.

405
00:25:57,080 --> 00:25:58,200
We'll show all the best.

406
00:25:58,200 --> 00:26:08,200
And thank you, Gailin, for talking.

