1
00:00:00,000 --> 00:00:10,080
So, recently I did a video about the, I don't even remember what the video was about,

2
00:00:10,080 --> 00:00:16,680
but at one point I explained that, oh, it was about the self, I think.

3
00:00:16,680 --> 00:00:22,400
And I said that the, I was, I was ranting, I was off on a tangent that somehow related

4
00:00:22,400 --> 00:00:27,960
to the question and I said that the body, for example, is unnatural.

5
00:00:27,960 --> 00:00:31,960
And since then I think three or four people at least have called me out on that and said,

6
00:00:31,960 --> 00:00:34,800
how can you say that the body is unnatural?

7
00:00:34,800 --> 00:00:37,400
It was a wrong choice of words.

8
00:00:37,400 --> 00:00:46,840
And I guess it really struck a chord with people who didn't agree with that.

9
00:00:46,840 --> 00:00:55,960
But I stand by it and I'd like to explain myself exactly what I meant by the word unnatural.

10
00:00:55,960 --> 00:01:01,320
One of all the word unnatural is unnatural, which I've just found out makes it

11
00:01:01,320 --> 00:01:03,280
ontological.

12
00:01:03,280 --> 00:01:06,120
It's an example of itself.

13
00:01:06,120 --> 00:01:14,240
In nature, or from, from outside of the realm of, of humanity, everything is natural.

14
00:01:14,240 --> 00:01:20,200
So you might say that Buddhism shares that opinion, though I'm, I'm not convinced that

15
00:01:20,200 --> 00:01:27,360
it does, maybe that the Buddha would have maybe said everything is natural, maybe, but

16
00:01:27,360 --> 00:01:28,360
maybe not.

17
00:01:28,360 --> 00:01:30,120
I don't think that there's any proof, by the way.

18
00:01:30,120 --> 00:01:35,040
But either way, the word unnatural itself is a human construct.

19
00:01:35,040 --> 00:01:41,440
So we delineate what is natural and what is unnatural.

20
00:01:41,440 --> 00:01:44,960
And I think that there are two senses that we use it in.

21
00:01:44,960 --> 00:01:48,040
The first is something that is perverse.

22
00:01:48,040 --> 00:01:52,720
So I obviously wasn't using this definition of the word unnatural.

23
00:01:52,720 --> 00:02:02,520
So if, if someone says that pedophilia is unnatural, or no, I don't know if that's even

24
00:02:02,520 --> 00:02:10,840
a good example, maybe eating, eating feces is unnatural.

25
00:02:10,840 --> 00:02:16,080
Something that, that, that, that is perverse, what's a good example, Robin of something

26
00:02:16,080 --> 00:02:17,080
that's perverse.

27
00:02:17,080 --> 00:02:19,640
You've got that sense, something that's unnatural.

28
00:02:19,640 --> 00:02:26,280
Both of those were very perverse, so yeah, but, but, so we'd call that unnatural in, in a sense

29
00:02:26,280 --> 00:02:33,800
of, it's, it's kind of a supposed Judeo-Christian, in a sense of going against some godly,

30
00:02:33,800 --> 00:02:37,080
divine intention.

31
00:02:37,080 --> 00:02:40,600
But I think Buddhism, in Buddhism we could at least accept that there are certain things

32
00:02:40,600 --> 00:02:46,960
that are perverse, and by that we mean unnatural.

33
00:02:46,960 --> 00:02:52,320
The second sense is simply the human, the first one's a human construction as well,

34
00:02:52,320 --> 00:02:59,280
but the second is the idea that the difference between something that is made by humans

35
00:02:59,280 --> 00:03:04,560
and something that exists in the natural realm, or as Buddhists we might expand that

36
00:03:04,560 --> 00:03:11,160
to say something that was created by a sentient being explicitly created or intentionally,

37
00:03:11,160 --> 00:03:20,520
I don't know, maybe not intentionally, but explicitly created by a sentient being.

38
00:03:20,520 --> 00:03:25,560
And as we know, the reason we limit it normally to humans is that most sentient beings

39
00:03:25,560 --> 00:03:35,000
in this world, in this realm, don't create things, but if we expanded it we might say

40
00:03:35,000 --> 00:03:44,080
that an aunt or a termite mound is unnatural, but a scientist would say no, it's natural

41
00:03:44,080 --> 00:03:46,320
because it's not human-made.

42
00:03:46,320 --> 00:03:50,640
So we consider that something artificial is something that is created by human beings.

43
00:03:50,640 --> 00:04:00,840
It is a arbitrary distinction, but I think it's a useful one, because there are clearly

44
00:04:00,840 --> 00:04:10,080
those things that exist outside of active, maybe in, you could say, intentional human,

45
00:04:10,080 --> 00:04:13,880
I guess you'd say, intentional human construction.

46
00:04:13,880 --> 00:04:21,080
The body is not one such thing, and that's a key point in, it's a key belief or theory

47
00:04:21,080 --> 00:04:28,760
or claim in Buddhism that distinguishes it from modern western science, which will say

48
00:04:28,760 --> 00:04:33,120
that the body evolved naturally, based on natural selection.

49
00:04:33,120 --> 00:04:39,120
Now we consider that to be a limited partial explanation of how things happened.

50
00:04:39,120 --> 00:04:47,080
Now, indeed, the environment has dictated to some extent how the human form has evolved,

51
00:04:47,080 --> 00:04:51,960
but the point I wanted to make is that we are, in the end, essentially responsible for

52
00:04:51,960 --> 00:04:59,840
creating, through our decisions, from life to life to life, we're responsible for creating

53
00:04:59,840 --> 00:05:01,160
the form that we find.

54
00:05:01,160 --> 00:05:08,840
The reason, and so the point, the importance of this is that without understanding it,

55
00:05:08,840 --> 00:05:14,400
we tend to think of the human form as being somehow the default.

56
00:05:14,400 --> 00:05:21,920
The human form is somehow a part of what it means to exist in the universe.

57
00:05:21,920 --> 00:05:26,440
And therefore, as a result, we get questions about sexuality.

58
00:05:26,440 --> 00:05:32,160
We say, well, giving up sexuality, how can you do that when it's a part of nature?

59
00:05:32,160 --> 00:05:34,280
It's a part of being human.

60
00:05:34,280 --> 00:05:41,040
And the implication there is that somehow that means something.

61
00:05:41,040 --> 00:05:46,920
Because something is a part of what it means to be human, then therefore it's natural.

62
00:05:46,920 --> 00:05:53,520
And that I disagree with fervently, and that's the sort of thing I was trying to get

63
00:05:53,520 --> 00:05:54,520
at.

64
00:05:54,520 --> 00:06:00,800
Anything that you can pass off as being natural simply because it's the way humans do eating

65
00:06:00,800 --> 00:06:09,240
food, breathing, having sexual intercourse, any of these things, there's no reason to think

66
00:06:09,240 --> 00:06:17,200
that they are in any way, I guess, right or in any way the default.

67
00:06:17,200 --> 00:06:27,240
They're a part of this artificial construct or artificial set of experiences that has been

68
00:06:27,240 --> 00:06:38,440
created through our specific, repeated behavior patterns of patterned behavior.

69
00:06:38,440 --> 00:06:46,040
So by acting in a certain way, we've evolved to become human beings.

70
00:06:46,040 --> 00:06:51,640
And so basically, the point being that the mind was partially, at least partially, if not

71
00:06:51,640 --> 00:06:57,240
mostly, involved with the creation of the human form.

72
00:06:57,240 --> 00:07:05,200
It's an artificial distinction, but it means something in the sense that there's nothing

73
00:07:05,200 --> 00:07:09,880
to the human form that is in any way special.

74
00:07:09,880 --> 00:07:16,440
Any set of experiences that are specific to humanity, for example, those involving sexuality,

75
00:07:16,440 --> 00:07:25,640
those involving digestion or ingestion of food are by no means special.

76
00:07:25,640 --> 00:07:27,840
They're still just experiences.

77
00:07:27,840 --> 00:07:32,400
The truth about nature is it's experience in some way.

78
00:07:32,400 --> 00:07:37,840
The question of whether the six senses are intrinsic to nature is an interesting one,

79
00:07:37,840 --> 00:07:42,280
like whether there could be a seventh sense or any number more senses.

80
00:07:42,280 --> 00:07:48,280
I imagine there could be, and I imagine the difference between, say, smelling and tasting

81
00:07:48,280 --> 00:07:53,720
is not really chemically speaking, I don't think it's that different.

82
00:07:53,720 --> 00:07:56,000
It's just different organs that do it.

83
00:07:56,000 --> 00:08:00,840
I don't know that you could actually claim that there's anything different between the

84
00:08:00,840 --> 00:08:01,840
six senses.

85
00:08:01,840 --> 00:08:09,760
The six senses themselves may simply be a product of the artificial state of detecting

86
00:08:09,760 --> 00:08:17,320
the environment, but in any case, experience is the ultimate building block of nature.

87
00:08:17,320 --> 00:08:24,480
Humanity is not a natural thing, it's an artificial construct, like a sand castle.

88
00:08:24,480 --> 00:08:34,360
Human being is as natural as a sand castle, by some definition, it's unnatural.

89
00:08:34,360 --> 00:08:38,160
You go to the beach, and you don't say, oh, look what the waves did, or look what formed

90
00:08:38,160 --> 00:08:39,160
by the wind.

91
00:08:39,160 --> 00:08:46,560
It's clear that the sand castle was built intentionally, and the human form, to some extent,

92
00:08:46,560 --> 00:08:53,400
is similar, which is obviously not nearly as obvious as the sand castle, especially based

93
00:08:53,400 --> 00:08:58,280
on modern western science, which tries to deny that and saying that it just arose based

94
00:08:58,280 --> 00:09:04,360
on a series of non-unconscious.

95
00:09:04,360 --> 00:09:11,160
I guess the point is that modern science still hasn't, to my knowledge, adequately incorporated

96
00:09:11,160 --> 00:09:14,760
consciousness into the mix.

97
00:09:14,760 --> 00:09:20,680
Although if you did, then you would say that consciousness is natural, and nothing would

98
00:09:20,680 --> 00:09:22,240
be unnatural.

99
00:09:22,240 --> 00:09:30,680
So to one sense, as I said, everything is natural, but the only reason we use the word

100
00:09:30,680 --> 00:09:40,360
unnatural is to describe something that is specifically intentionally constructed by humanity.

101
00:09:40,360 --> 00:09:47,440
Now, we weren't aware, it was unconsciously, we weren't saying, hey, let's build a human

102
00:09:47,440 --> 00:09:52,120
body, obviously, but through our choices, we have altered ourselves as all

103
00:09:52,120 --> 00:09:53,720
I was trying to say.

104
00:09:53,720 --> 00:10:00,800
It's maybe, because it's unconscious, it may not be exactly, but it's still artificial

105
00:10:00,800 --> 00:10:01,800
and it's still unnatural.

106
00:10:01,800 --> 00:10:08,680
An unnatural thing doesn't have to be consciously created, but created through one's

107
00:10:08,680 --> 00:10:19,160
activities, like a garbage heap, is created through one's activities, or an ant hill,

108
00:10:19,160 --> 00:10:23,760
and though they may not have wanted to make a hill, it's created because of the ant digging

109
00:10:23,760 --> 00:10:26,200
and the sand accumulated.

110
00:10:26,200 --> 00:10:32,800
So because it was consciously created by sentient beings, we therefore call it unnatural.

111
00:10:32,800 --> 00:10:40,840
So there's my defense, and it's an important one because it helps us to, it reminds us

112
00:10:40,840 --> 00:10:58,200
not to take the human form too seriously.

