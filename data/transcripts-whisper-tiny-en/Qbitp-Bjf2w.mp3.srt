1
00:00:00,000 --> 00:00:16,080
Okay, good evening everyone, welcome back to our live broadcast out of the time for

2
00:00:16,080 --> 00:00:26,220
all of us to act together as a community, to come together as a community, take the

3
00:00:26,220 --> 00:00:39,320
time to reflect on our practice, as my teachers used to say as you often hear monks in

4
00:00:39,320 --> 00:00:54,300
Thailand, keep Banchi, keep Banchi which is a colloquialism for looking at

5
00:00:54,300 --> 00:01:16,580
our bank account or calculating our profit, looking at what we've gotten out of

6
00:01:16,580 --> 00:01:33,060
the practice, let's come of our practice, what we've gained from the practice, we come together

7
00:01:33,060 --> 00:01:54,420
to get direction on our practice, the right direction to adjust our focus, but we also come together

8
00:01:54,420 --> 00:02:07,560
to talk about what we've gained from the practice, subjective constant concern, right, when

9
00:02:07,560 --> 00:02:14,760
am I getting out of this, you have to look at the bottom line, you're putting a lot of effort,

10
00:02:14,760 --> 00:02:36,760
look at it, look at this at you, the Buddhism is all about cause and effect results, actions

11
00:02:36,760 --> 00:02:45,800
and their results, come as actions we pack, means results, so we have results of many different

12
00:02:45,800 --> 00:02:58,120
things, first of all there's the results of our ordinary efforts to try and solve our problems,

13
00:02:58,120 --> 00:03:14,400
and something comes to mind that is undesirable, we work very hard to avoid it, to quench it

14
00:03:14,400 --> 00:03:30,120
or quench it, to be free from it, and something good comes to mind, the actions are to

15
00:03:30,120 --> 00:03:44,160
obtain it, to sustain it, to maintain it, to get what we want, to always get what we want,

16
00:03:44,160 --> 00:03:51,440
and we have strategies for this, so I was talking about yesterday we have ways of thinking

17
00:03:51,440 --> 00:04:00,240
positive or setting up our lives in such a way that we are protected from the things

18
00:04:00,240 --> 00:04:09,040
that we don't like, and that we're able to protect the things that we do like so that we

19
00:04:09,040 --> 00:04:36,160
can obtain and experience them frequently, repeatedly at our pleasure.

20
00:04:36,160 --> 00:04:39,960
And this is the worst thing to do of course, this is what we're starting to realize through

21
00:04:39,960 --> 00:04:52,120
the practice, this is the word disaster comes, if you spend all your time cultivating

22
00:04:52,120 --> 00:05:00,800
this attachment and aversion, these habits of avoiding the unpleasant and obtaining the

23
00:05:00,800 --> 00:05:10,440
unpleasant, highly unstable, the sort of thing that can come crashing down on you and

24
00:05:10,440 --> 00:05:24,280
when your defenses are down, then they do great stress and suffering, when you don't

25
00:05:24,280 --> 00:05:33,640
get what you want, when you get what you don't want, and we're learning about these

26
00:05:33,640 --> 00:05:39,720
habits when we practice as you begin to meditate, you start to see these habits, these

27
00:05:39,720 --> 00:05:44,880
strategies, these practices and you realize it's not really the best way, the results are

28
00:05:44,880 --> 00:06:12,600
not so good, another strategy of course is to cultivate tranquility meditation, so we

29
00:06:12,600 --> 00:06:28,920
cultivate states and habits that replace our reactions, sort of avoid the problem entirely

30
00:06:28,920 --> 00:06:36,480
right, so if you're angry at someone, immerse yourself in universal love and the problem

31
00:06:36,480 --> 00:06:45,560
never comes up, spend time sitting, cultivating love for all being love for those that

32
00:06:45,560 --> 00:06:56,600
you hate, it's a great method, works wonders, if you have lust, passion for the body for

33
00:06:56,600 --> 00:07:04,360
bodily, carnal pleasure, cultivate mindfulness of the body, look at the different body parts

34
00:07:04,360 --> 00:07:13,160
and see the most unpleasant as disgusting as not worth clinging to and it suppresses

35
00:07:13,160 --> 00:07:23,600
the desire, replaces it with a sobering awareness of the true nature of the body as not

36
00:07:23,600 --> 00:07:34,640
being in any way desirable, worthy of clinging to and so on, of course the more universal

37
00:07:34,640 --> 00:07:43,560
meditations like fixing your mind on a colour, for example, or any sort of concept meditating

38
00:07:43,560 --> 00:08:00,160
on the Buddha, for example, and these are much better results, the results here are lasting

39
00:08:00,160 --> 00:08:16,040
stable and unadulterated, they're not tainted by defoundment, so when you stop practicing

40
00:08:16,040 --> 00:08:20,600
you're not any worse often, you were before, you're in fact better because you now have

41
00:08:20,600 --> 00:08:28,080
this skill, this talent of entering into these states and so it's a great thing to do,

42
00:08:28,080 --> 00:08:33,320
people often wonder about this sort of meditation whether it's actually any good or

43
00:08:33,320 --> 00:08:40,560
any use, well this is the use, it provides you with a way out, provides you with sort

44
00:08:40,560 --> 00:08:55,720
of a freedom from suffering and yet, and yet it too is not permanent and so has great

45
00:08:55,720 --> 00:09:02,360
of a temporary solution as it might be, it's not by any means permanent solution, it can't

46
00:09:02,360 --> 00:09:15,520
actually free you from suffering because when you, when and if you ever stop or lose and

47
00:09:15,520 --> 00:09:24,120
lose the interest or the initiative or the impulsion to continue, you just wind up where

48
00:09:24,120 --> 00:09:35,040
you started, you know you begin to cultivate bad habits again or your bad habits crop up again,

49
00:09:35,040 --> 00:09:44,480
so even that's not the solution, good results but not the results we're looking for,

50
00:09:44,480 --> 00:09:51,480
so we go further, this is really why we we bother to torture ourselves with insight

51
00:09:51,480 --> 00:10:05,360
meditation, because we see there's got to be a more permanent solution, more stable

52
00:10:05,360 --> 00:10:11,960
solution, something that can allow us to truly change and truly become free and so cultivate

53
00:10:11,960 --> 00:10:23,040
insight meditation now, insight meditation has this great power of universal applicability,

54
00:10:23,040 --> 00:10:27,920
it's not just when you're angry you have, you cultivate love or when you're passionate

55
00:10:27,920 --> 00:10:37,080
or lustful, you cultivate mindfulness of the body or something, insight meditation cultivates

56
00:10:37,080 --> 00:10:45,640
wisdom and wisdom gives you an understanding of all aspects of reality that frees you

57
00:10:45,640 --> 00:11:01,040
from clinging, it's an awareness, a presence, it's like a solution, a solvent that dissolves

58
00:11:01,040 --> 00:11:17,440
all of the stickiness in the mind, it's far more lasting and stable and requires so much

59
00:11:17,440 --> 00:11:28,320
less effort to upkeep because it's innate, it's pure, it's not based on effort, per se,

60
00:11:28,320 --> 00:11:32,520
it's based on wisdom, meaning it's not something you have to force, it's not artificial,

61
00:11:32,520 --> 00:11:41,440
it's natural, that being said something that is not acknowledged nearly enough is that

62
00:11:41,440 --> 00:11:53,320
even insight is not permanent, even insight having been gained can be lost, the person

63
00:11:53,320 --> 00:12:01,600
on the path of insight, can gain profound insights into reality and think maybe they've

64
00:12:01,600 --> 00:12:11,880
really succeeded in the practice only to fall away when they give up the practice and

65
00:12:11,880 --> 00:12:22,120
lose everything they've gained.

66
00:12:22,120 --> 00:12:25,880
That's one kind of thing, why go through all the effort, maybe I'll go back to summit

67
00:12:25,880 --> 00:12:30,160
meditation if that's the case, I thought this was going to be a one time thing where

68
00:12:30,160 --> 00:12:35,240
I don't have to go through all the torture of actually dealing with my problems and figuring

69
00:12:35,240 --> 00:12:43,440
them out, I'm just going to get them all back again, but it's to be honest, it probably

70
00:12:43,440 --> 00:12:47,880
lasts a lot longer and it's a lot more stable, it's the most stable of all of them all

71
00:12:47,880 --> 00:12:56,600
so far, that's not permanent, this life maybe you keep it, if you get really strong insight,

72
00:12:56,600 --> 00:13:02,000
there are certain things that you won't lose throughout your life, you can lose them in

73
00:13:02,000 --> 00:13:13,120
the next life or the next life or some life in the future, and I'm permanent, but not

74
00:13:13,120 --> 00:13:18,080
to despair, there is light at the end of the tunnel, and in fact, insight meditation

75
00:13:18,080 --> 00:13:23,360
leads to something else, it leads to freedom, it leads to nibana, it leads to the

76
00:13:23,360 --> 00:13:37,400
experience of what we call the deathless or the under reason perhaps leads to an experience

77
00:13:37,400 --> 00:13:46,700
that is outside of samsara, and so we have to distinguish between these two being on the

78
00:13:46,700 --> 00:13:53,440
path of insight, it's not enough, it's not something you accumulate and never lose, something

79
00:13:53,440 --> 00:13:59,400
you accumulate, and if you accumulate enough of it, you can break through to the point

80
00:13:59,400 --> 00:14:20,840
where you reach another level of action with another level of result, so the results

81
00:14:20,840 --> 00:14:27,440
of insight are stable and lasting, but not permanent, but the results of nibana, the results

82
00:14:27,440 --> 00:14:37,600
of entering into the manganjana, pollenjana, of experiencing nibana, even just a moment,

83
00:14:37,600 --> 00:14:44,160
even for just three moments of time, that's apparently how shortest amount of time you

84
00:14:44,160 --> 00:14:50,080
can experience nibana or something or maybe two moments, but for the first time I think

85
00:14:50,080 --> 00:15:01,960
it's three, of course it can be longer, it can be hours or days, but even for just that

86
00:15:01,960 --> 00:15:10,160
moment, this is, and again it requires perhaps little faith, but something you can verify

87
00:15:10,160 --> 00:15:21,680
for yourself, that this state is the point of no return, you can't go back, person who

88
00:15:21,680 --> 00:15:33,120
has seen nibana, even for just a moment, will be changed permanently, they've set in

89
00:15:33,120 --> 00:15:43,080
motion the cessation of suffering, the escape from samsara, they're permanently altered

90
00:15:43,080 --> 00:15:51,200
and removed from them from their very being, sort of the lynch pinned to this whole mass

91
00:15:51,200 --> 00:16:02,960
of suffering, ignorance, they come to see outside samsara, and this was this happiness,

92
00:16:02,960 --> 00:16:16,040
this peace, this great freedom is lasting, there's no return from it, so to be clear

93
00:16:16,040 --> 00:16:22,600
our daily practice of meditation is great, it's lasting, it's great quite beneficial

94
00:16:22,600 --> 00:16:27,920
and it is what eventually leans to true freedom, but it's not true freedom, to truly

95
00:16:27,920 --> 00:16:35,960
be free and to have a perfect release, you need to truly understand samsara, you need

96
00:16:35,960 --> 00:16:42,440
to get to the point where you really get it, where you have the sapiphany that tells

97
00:16:42,440 --> 00:16:53,040
you that nothing is worth clinging to, that ceases where the mind drops out, unplugs, ceases

98
00:16:53,040 --> 00:17:08,040
to reach out, ceases to advert to samsara, and there's an experience that is beyond

99
00:17:08,040 --> 00:17:14,600
samsara, that is outside of it, and that moment on one is called a soda panda which

100
00:17:14,600 --> 00:17:18,520
means they have attained the stream, they have entered into the stream meaning they're

101
00:17:18,520 --> 00:17:29,480
on the current, leading them to complete freedom from suffering, so it doesn't mean to

102
00:17:29,480 --> 00:17:39,720
say that we should not all be praised and appreciated for all the good merit that we're gaining

103
00:17:39,720 --> 00:17:46,640
in our practices of all sorts, we practice the Buddhist teaching on all levels, but it's

104
00:17:46,640 --> 00:17:53,640
a reminder for us not to be complacent, that there is a goal, that this isn't just a hobby

105
00:17:53,640 --> 00:18:02,080
or something that you take up to do on a daily basis, it's something that you use to achieve

106
00:18:02,080 --> 00:18:09,760
true understanding and realization of the ultimate state and the ultimate freedom, it's

107
00:18:09,760 --> 00:18:20,880
what we're aiming for, it has the greatest result, the greatest effect, it has a true benefit,

108
00:18:20,880 --> 00:18:33,760
true purpose, true meaning, there you know there's our dhamma for today, the results getting

109
00:18:33,760 --> 00:18:39,760
results, thank you all for tuning in, for coming out, have a good day.

