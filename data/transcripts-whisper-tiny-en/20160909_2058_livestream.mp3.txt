Good morning everyone, welcome to our live broadcast.
Tonight we're looking at... I'm going to turn the guy a book of fours, 93 to 95, 92 to 94.
Actually mainly 93.
I'm looking at it's a good example of the teachings of the Buddha on the two aspects
of meditation, the two main aspects of meditation practice, and there's the aspect of
tranquility in the aspect of insight, clear sight, clear vision, vipasana.
So these we often refer to as samata and vipasana.
Here the Buddha specifies Ajitang Jetosamata, I mean samata means tranquility, Ajitang Jetosamata
means inner tranquility of mind, or inner mental tranquility, being inner internally composed.
And the other one is Adipanya Damoipasana, insight into reality with higher wisdom.
Some people gain tranquility and insight.
It's important to understand these as the two aspects of meditation.
The Buddha says some people gain tranquility but don't gain insight.
Some people gain insight but don't gain tranquility.
Some people don't gain either, but don't have either, in some how both.
So there are certain types of meditation, practice, and there are certain meditators who
only practice leading to tranquility, for example.
There's many meditations of this sort, any meditation that doesn't focus on reality.
You could not have any beneficial meditation, not a meditation outside of the Buddha's
teaching.
It's only for the purpose of gaining tranquility.
There are meditations which are meant to lead to other things, but for the most part
they are based themselves around tranquility, any meditation that fixes on a single imaginary
object.
It's so conjured up in the mind or even on a physically apparent object, maybe a site
or a sound, usually a site, but it couldn't very well be a sound.
There are people who have meditations based on sounds.
But the idea is to focus on the concept.
So when you look at a candle, you're focusing on it as being something, being a flame.
You're focusing on a colour, you're focusing it on being that colour, and not focusing
on the experience of seeing it, you're not meditating on the experience, so you're not
meditating on reality exactly.
Any meditation that's focused on beings, so loving-kindness meditation, friendliness
what we call maita or compassion karuna.
All these types of meditation only have is the goal of calming the mind, or they only
have, they only exist in this classification, they're only on the tranquility side.
They can lead to other things, magical powers.
They can lead to insight into things like past lives or the thoughts of others seeing or
hearing things far away, that kind of thing, a lot of magical things.
They can't teach you about reality, so personal practices, these might kind of very powerful
mind.
Of course, they're yogis and mystics and other traditions that have very powerful minds.
It has no bearing on reality.
So when you come back down to earth, when you stop meditating, there can be great defilement,
lying dormant underneath your tranquility, as you haven't come to understand, you've
just come to avoid.
You're able to transcend the transcendings of temporary affair.
It doesn't last when you come back down to earth.
The other type of meditation is where you muck around in the mundane, which is actually
much more uncommon.
You don't want to get the impression that both of these are equal, they're actually
quite different, and insight meditation is special, tranquility meditation is not so special.
In the sense that it's not something that is only found in Buddhism, it's quite special.
It's not as special, it's not special in the same way as insight meditation, only the
Buddha taught, insight meditation, or maybe that's not even fair, because it's not only
the Buddha taught, insight meditation leading to enlightenment, so you could argue that
there are certainly mystics and yogis who learn something about reality, and I'm getting
very far, but potentially there's an ability to practice both.
Regardless, it's an important argument, it's important to understand the difference,
so insight into reality, dhamma vipasana.
It's still more special, because it's lasting, it's something that's more real, that
ethics is, the solution is more natural, but there are some people who practice it without
gaining any sort of tranquility, and that can be a problem, because they find themselves
overwhelmed by the incredibly unpleasant nature of reality, it's unstable, chaotic.
If your mind is not stable, it's not focused, if you were to just go off and try to note
things, right, to take this tradition, this is why we tell people not to just note things
one after the other, you can easily work yourself up and you don't ever gain the tranquility,
necessary to see things deeply and to really understand things, you know, gain the clarity
of mind, the richness, the fullness of the experience, the superficial insight as your
mind is not steady, it's not composed, so that's why we give people a meditation technique,
we have them always come back again to the stomach, and then we watch various points on
the body, that kind of thing, but it's something to keep you stable and to cultivate a stability
here inside, to not go back into the idea of focusing on a concept, you don't need to
practice focus on a concept to tranquilize the mind, but you need some structure to your
meditation, so this is why we give these exercises, we put people through a course, they're
going through, this is why we do walking meditations, one step being right, one foot at a
time and then increasing through the course, we keep it rather formalized, because it helps
compose the mind, tranquilize the mind, and so the Buddha rightly says that someone is just
practicing some of the meditation should use that as the base and then apply their mind
to gain insight, someone who's just gained insight should use that as a base and focus
on tranquilizing the mind, someone who's got neither, the Buddha says they should think of
themselves as a person, they should think like a person with their head on fire, with
a close or their head on fire, they should put forth and let's look at the Pali,
where like the Pali here is because there's lots of qualities that one should cultivate,
but without either if one is in real dire straits, without any tranquility or insight,
someone needs to do so more, and just as if their head is on fire, they should put forth
great zeal, the chanda, why am I a great effort, oh sa, oh sa is in a word for effort,
it means like fortitude resolve, apatiwā nītā, zeal, another word for zeal, wonder
if that's actually negative, it looks like a negative to me, alright, patiwā nā mean shrinking,
so they should put forth the quality of not shying away, they should be gung ho, they
might say satitā, they should have mindfulness or remembrance, they should remember themselves,
remember the present moment, some pajanya, they should have clear comprehension or clear
knowledge, gharanya, they should be done by them, sa yatāpi bhikouy a dītā jī aloha,
a dītā sī sī sī, just as if monks there were someone with their heads, their clothes
on fire or their head on fire, tā sī sī sī sī sī sī sī, nī bhā nāya a dī
endātā nī sī sī sī sī sī sī sī sī sī sī sī sī sī sī sī sī sī tī
sī sī sī sī sī sī sī kī sī zī sī sī sī sī sī sī sī sī sī sī sī sī
a sign that things are done, it's just a sign that you're on the right path. Even someone
who has both tranquility and insight, that's not in and of itself a certain, something
that was up, a description or a certainty that the person is enlightened. But you need
to use both of these. Once you have both of them and you don't have just tranquility
or you don't have just insight without keeping your mind focused through the practice.
And you have to use them both and increase them both.
Because they heard tonight, what happens when
you're an AK evan, you'll grow a voice by one,
one, one could cultivate the endeavor for further destruction of deviant.
So, there's a acknowledgement that having these is already destroying the defilements and
with these, you've already come a long way in reducing your defilements through insight,
tranquility, through seeing things as they are, but the work for one, for such a one,
is to continue to further their practice and continue on the way.
So, there's this, when there's a big debate over this, is people have different ideas
of what this means and they talk about, the Buddha talks about these things called
and John as John a meaning meditation, but there are four distinct levels of meditation
that the Buddha talks about and they can be interpreted either through the lens of insight
or through the lens of tranquility and there's a big debate over which is which and which
the commentary means and people try to put the commentaries and the teravada tradition
and anyway there's this whole big debate that some of you might be familiar with.
I like this sort of teaching here and it's clear that Buddha didn't debate this sort
of thing too much, but he didn't try to point out that there are these two qualities
and clearly the best is when they're both together.
You wouldn't want to have just one or the other, but I think a lot of people don't realize
that much meditation is just in one category or the other, usually in the tranquility
side because people can gain magical powers and not being Buddhist, but you can't
gain wisdom, true wisdom, unless you're practicing in line with the Buddha's teachings.
So it's important not to cling to your tranquility, when you practice and your mind
becomes tranquil, becomes calm, it's important not to cling to that, but at the same time
it's important not to get too caught up in insight and investigation and without
tranquility in the mind otherwise you just become distracted and superficial and it can
lead to distraction and mental fermentation anyway, not a bit of dhamma today, there's
our dhamma for this evening, and we'll go on to questions.
Good, I'm just unmute you there, just give me one second here, let's make sure this
is all right, no it's not okay so I'm going to put ganks for you, okay go ahead and say something,
okay can you hear me now Dante? Yes, why is it quieter? Oh because these times I think we're
good, okay, in the past I've done things that I regret and a lot of people know about it,
what can I do to stop caring about what people think of me and move on in my life, stop
caring what people think about you, my insight meditation, I mean this practice is just
incredible for that really, but sometimes it takes a affirmation on your part, you have
to remind yourself and say look, this is all just, this isn't real, there's no, the
worst thing that could happen is I'll die or the worst thing that, you know, thinking about
all these things that could happen and they may just happen and they're just things,
you know there's this book that my mom gave me once, don't sweat the small stuff and
it's all small stuff and then these modern Buddhist books, you know, I'm pretty good
book, even Buddhism puts everything in perspective, it is all small stuff, what people
think about you is not life threatening and even if it was, then it would just be your
life, probably the best, the best view to take is that what other people think of you
doesn't make you a good or a bad person, so because good or bad being good or bad, being
wholesome or unwholesome, that's really kind of important, so if you ask yourself what's
most important, the most important is wholesomeness and unwholesome, that's good and evil,
and other people think about you doesn't affect that, so what I'm saying is sometimes
it's good to remind yourself what's important and this is the stepping back and reflecting,
it's not meditation but it's sometimes useful but ultimately it's the meditation that helps
you realize, it helps you realize that none of that's significant when you start to see
people as just experiences and certainly it doesn't matter what they think of you because
there's no day, the thought pops into their hand is just an experience, I mean there's
no quick fix to any of these problems that so many problems that people come and ask, it's
a matter of learning and understanding and letting go and insight meditation is definitely
for that, your mind starts to relax, you stop freaking out, the thoughts that come up when
you regret are just thoughts and so most important is that you recognize them as just
thoughts, memories, if you feel upset about them then you have to note that you're upset,
you can't stop them from coming up and for the most part in the beginning you can't
stop getting upset about it, but as you watch and as you look your mind starts to see
and that's sort of what you're seeing now is how unpleasant it is and the more you see
how unpleasant it is, the more clearly you see how unpleasant it is, the more you quickly
you learn to not do that, to not stress about what people think about you, the meditations
just kind of an acceleration of the learning process because you learn things in life,
we learn lessons based on the stress that things cause us so we, this is why older people
tend to be more, tend to be more moderate, less prone to outburst because they've come
to see that it doesn't bring any benefit but they've seen slowly and their vision is
just a lot, less clear than through meditation practice.
But it is possible to use poly as an everyday communication language, as a monk who travels
abroad have you ever faced a situation where you had to communicate in poly with another
monk who didn't speak English or Thai?
I have and my poly is not nearly good enough to do so but I've used it to say a few words
to Burmese monks, Sri Lankan monks.
I think there was even one case where I didn't try to talk to a monk who knew poly.
Ajahn Tanga is this, he did this with a Burmese monk who came to Sri Lankan monk who came
with us to see him and Sri Lankan monks spoke good English but no Thai of course Ajahn
Tanga isn't speaking with, so he started speaking to him in poly and this Sri Lankan
monk was like I don't speak poly, he just assumed because a lot of Sri Lankan monks
are quite clever in poly but Ajahn Tanga can say a few sentences speak a little bit, he's
actually cultivated it.
We put together a, some monks in Thailand put together a textbook for learning conversational
poly and I have that it would be really neat too, I've never had a chance to actually
use it but it would be something to go through and actually learn how to talk.
You need a group of people, one time we tried the long time ago I tried this with some
people on the internet posting chat messages so someone would post a sentence on like
a wiki and then the next person would post a response so you could do it on your own time
on your own leisure and it really was helpful for me to take the time to compose conversational
sentences because the only other person that really got into it wasn't very good at it
and I mean I wasn't that good but he was substantially worse and so it then he gave up
but that would be something to have some sort of group where we actually tried.
I've talked about it on our poly, we have this high level poly study list but they're
all PhDs and professors and a few monks, mostly very busy and I never was able to get
anyone really interested in it. I have noticed an immediate effect in how I am less reactive
to my teacher's laughter of me to the point where it no longer has a lasting effect. In
class during their laughter I mindfully note the subsequent impurities that arise, the anxiety,
the sadness, the anger, as well as the desire to change or control their laughter.
I believe it would be best for me to, sorry, I believe it would be best for me to continue
to meditate on the impurities that arise from their laughter rather than try to control
or change it. Wouldn't you think it would be why it's just for me to meditate on the
desire to change and control their laughter and on all the subsequent impurities rather
than act upon the impure on wholesome desire? Wouldn't that give the best result for
we pass in a practice in order to become content with reality rather than try to control
it? I don't understand the wouldn't, I mean of course, yes the answer is yes but
have I ever thought it sounds like you're saying well no probably not but if that's just
a straight-up question then absolutely that sounds very much like you're understanding
what we're trying to do. Sometimes in lying meditation my breath elongates such, my
breaths elongates such that they become long. I know the principle of uncontrollability
impermanence and unsatisfactoriness and so would long breaths as in naturally occurring
long breaths not be an issue. In addition sometimes I do not feel like doing sitting meditation
so would it be fine to make sitting meditation up with lying meditation? I don't see
why it would be a problem if I'm still carrying out the practice of mindfulness though
in different postures. You notice that you know that you're practicing, actually practicing
meditation when you don't really have any questions when you're able to answer all your
questions. It sounds to me that you know what you're doing and those are not issues.
Go for it. My mind keeps wandering into past regrets. What can I do about it? I tell
myself to stop thinking about it but I can't help it. Mmm you're starting to understand
this idea of non-self telling yourself to stop thinking doesn't make this thinking
stop. That's not how thoughts work. Thoughts are not self. That's what that means.
What you're experiencing is exactly what to put a meant by a non-self or not self.
So it's not about doing anything about it. It's about learning not to try to do things
about it. Now your mind is wandering. Let's be clear about what's happening. Thoughts of
the past are arising. That's a much clearer way of saying it. Based on those thoughts
of the past there are arises subsequent regret which may be even clearer than that would
be disliking. You don't think of it as disliking but that's what it is. There's a disliking
of the thought or disliking of the concept behind the thought. But I disliking nonetheless.
And if you look at it that way it's actually quite easy to deal with. The thoughts are
not a problem. It's the disliking of them. So if you say to yourself thinking thinking
it's not a problem for those thoughts to come up again and again and again. But you're
able to free yourself from the disliking them because you don't see them in that way.
You don't see them as regrets. You see them just as thoughts. Whereas it takes training
and it's actually to be quite quick to catch it. But you can also catch the regrets.
So if you feel disliking of the thought, say to yourself disliking or sad, I agree however
it appears to you. But absolutely this is what you're starting. If you're practicing
the meditation as we teach, as we practice here, this is exactly what you should see that
you can't stop your mind from thinking about whatever it wants to think about. That's the
habit. It's this habitual nature of the mind. All you can do is start to change your
habits, specifically the habit of regretting things, getting upset about things. And how
you do that is by seeing it clearly, by watching and seeing how much stress you're causing
yourself and slowly your mind starts to change as it sees how much stress it's causing
yourself. But there's there's another problem here is the stress that you're causing yourself
by trying to control, trying to stop yourself and think that affirms the disliking of the
thoughts, this trying to get rid of them. It's just reaffirming the dislike of them, the
regret, as you say. It's also creating stress because you can't get rid of them. And so you
should just stop that. See that. In fact, that in and of itself is a habit. And it's a very
strong habit in a beginner meditator. So that's one of the first things that you start to see
you have to let go of is this need to control everything. Because it's causing a lot of stress
and suffering. How do you do that? Well, you can't force yourself to stop forcing. You just
have to, as already stated, you have to see it as as how much for how much stress it's
causing you. And once you see how much stress and suffering it's causing you, you'll start
to let go of that. What can one to about the fear of having a bad reputation or failure?
I'm not sure now. Have you actually started meditating in this tradition? If you haven't,
I've other than 19 hours of this person and I don't even be there. If you haven't, I would
recommend start by reading my booklet and you would just note to yourself a phrase, a phrase.
But what is the world of Sankara that is mentioned as one of the three worlds,
others being the world of space and the world of beings? Sankara means formation. So this refers to
the world of experiences. Experiences is one way of looking at the world. The three worlds are
ways of looking at the world. Akasa Loka is looking at the world from a physics point of view.
Set the Loka is looking at the world from a point of view of beings or realms of beings.
Sankara Loka is looking at the world from a point of view of experience. So Sankara is
something that is formed or is real, you could say, that arises. They're called formations,
but really what they are is sabhava dhamma, dhammas that have existential phenomena. I translate
that as things that exist. They arise and they cease like beings and space doesn't really exist.
It's just a concept, but it's based on experiences. So each experience is formed from other
experiences or it's formed from causes and conditions. So it's called a Sankara.
But what that is referring to is experientiality.
Regarding your Askamung video on addiction, attachment to entertainment,
how should I then spend my time? I have decided to terminate a lot of my stimulation.
We're removing the games from my phone, including Sudoku and word search.
As I understand this to create states of attachment and addiction and thus discontent.
With the stimulation from casual mobile puzzle games interfere with my progress and we pass
in a meditation in becoming content with the ultimate reality or will I still be
able to eventually and naturally give these things up without the first doing so and continue
to progress and we pass in. Those things will slow your progress. So yeah, you can, I mean,
doesn't make you unable to cultivate insight. It will get in your way so how much is dependent
on how obsessed you are with them, but they're not evil. See, the eight precepts, the Buddha
recommended for people who are undertaking intensive meditation courses. So the question,
how should you spend your time is why you spend in meditation and meditation?
Now, it's up to you how comfortable you feel meditating all the time.
Now, especially if you don't have direct contact with a teacher,
meditating a lot becomes difficult. So if you're not in the meditation course,
there's really no hard and fast answer to this. I mean, obviously the less entertainment you
can engage in the better, but the only thing I would say is it's not that significant.
It's not a huge problem. Playing Sudoku or word search is not going to prevent you from
becoming enlightened. It just makes it more difficult and slows you down. But the idea
that you can just give it up, I guess the point is the idea that you can just give it up without
doing intensive meditation is generally, well, it's a difficult thing to do. So I wouldn't
worry too much about it. You have the knowledge that it's detriment, because I'm extent,
and so try to moderate it. And if you can do without it, fine. If you can't, you wouldn't lose
sleep over it. You know, you know what you have to do. And if you want to increase your progress,
you know, better, you can the more time and effort you can put into actually meditating the
better. Because you can't be mindful of course when you're playing these games. That's the
problem. That's one of the big problems. The addiction to them is another problem.
Part of me feeling good about myself is through shopping rather than contentment. Is there
anything I can do to stop this emotional spending? Meditate more. Again, these are only
only things that you can for yourself through insight. It's not something you can just turn off.
When I try to feel the physical sensations in the abdomen, lots of mental images of
abdomen of my body and face occur between those physical sensations. It also happens when I meditate
with open eyes and out seeing images of my face flash between seeing. Sometimes it even feels that
there are more mental images than physical sensations. When I try to focus harder on just
abdomen, the number of images and their persistence increases. What to do with those mental images?
Should they be noted? It would be really difficult because it seems that just a couple of images
can appear between physical sensations in one second. Thank you for your help. Yes, they should
be noted. You should note them as seeing as long as they last. They only last a moment just say
once seeing and then go back to your exercise. You'll find that this is a sign, you know, lack of
stability of mind which is natural in the beginning. But eventually your mind will become more
sorted out and they won't mix like this. Or maybe they will, they'll come back. But when they do,
you just have to note them. Noting in between is not a problem if you're saying rising and then
an image comes, say seeing, seeing in a film in the last moment and let's go back to the stomach
again. This meditation is very much about being flexible, so it doesn't matter. It causes you to go
back and forth. As long as you keep trying to come back, you don't have to force the mind to stay
with the stomach. Absolutely not. When you see anything, you'd say to yourself, see if it stays,
you'd say seeing, see it doesn't. You'll go back to the rising volume.
Our monks allow to accumulate wealth and to live within their own shelter residents.
If not, does a monk have no self-reliance or self-independence? Wouldn't a monk be entirely
dependent upon a monastery? Would this not be a scary proposition? As well, it is a requirement for
monks to live in monasteries and how bound our monks to the monastery. What are the rules
governing their freedom in terms of traveling from the monastery and whatnot? Well, there are a lot
of rules. So to some extent, they're allowed to accumulate wealth, but very, very limited extent.
And by wealth, it's just been possessions. So there are some possessions that monks can have.
Monks can't have more than one set of robes. They can't have more than one bowl,
but they can have various other cloths and belt and possessions. You can have a needle and
thread in it. But it's not about then relying on others. It's about not relying on anything.
It's about living in poverty without those things. So without wealth, money doesn't make
you dependent. Money takes you out of the realm of those activities that require you to spend
money. You can't go to a concert or something. You can't buy things. You live without
a monk is a fairly strict and austere life monastic life. But they're completely independent.
It doesn't create dependence. The only dependence monks have is on their base
requisites and they would understand this and it was a concern and that's why we try to address it.
That's a big reason for the poverty thing because you don't want to be dependent.
So for robes, you're to collect scraps of rags that have been thrown out.
For food, you're to go on arms around and just take whatever people are giving out to poor
or you're to the religious in India and in any Buddhist country, people give out food to
the religious. But even in Canada, I've been thinking about it, thinking about going on
arms around. The problem is I'll get started and then it'll get really, really cold in the snow.
But some monks even go on arms around in the snow. It's a little harder in a city.
A city like this. There's no excuse really. I could go on arms around. I think maybe I've just
gotten complacent. The other thing is once you start teaching, you get so busy and you start to give
up certain things that you did before you were a teacher. I used to go on arms around everywhere
in North Hollywood. I went on arms around. I could do it here. Anyway, that's not good to
the point is to go with whatever is available. People bring me food so I don't need to go on
arms around. I'm not dependent on them. They desire to bring me the food. They didn't. I'd go on
arms around or I'd go find other ways to get food. But that's the point is to try to
live with just the bare necessities. It's also why we only need one meal in the morning.
For shelter, you should be content even with staying at the foot of a tree. For medicines,
you should be content with drinking your own urine, which is apparently a very good medicine.
Munks don't have to live in monasteries wherever a monk lives at some monastery,
but they do have to stay in one place for three months. Right now, it's the three month
rains. You have to stay in a... You have to actually stay in a building. You can't stay under a
tree during the rains. It's not proper because it rains a lot. Of course, it doesn't rain a lot
here, but that's the tradition three months. I have been practicing for a few months.
Could I advance to noting three parts of a step instead of one part, like stepping right,
stepping left? And what are the three words that one could use to know with? Thanks.
It's a free world. You're welcome to do whatever you want. I wouldn't give you the three steps
personally until I've gone through the stages with you. So you'd have to... If you wanted me to
give it to you, you'd have to sign up for an online course and we'd meet every week and
after a few weeks, I might give you the second step and the third step, even the fourth step,
fifth step, sixth step, and I'll only give them to you once we've gone through some interviews.
Either you've come here and you do an intensive course or we do an online course.
For those interested in the online course, it's here on this site. You just go into the menu
and you go to the schedule, pick a slot during the week, and then you have to actually show up
and we do video conferencing and talk about your practice. Right now, I've got several people
doing that. It's difficult for me to meditate at home. Somebody is always there and they have
poor boundary sense. They often come in disruptively and don't really respect the meditation
practice. This leads me to be on edge when meditating that they may come in at any moment.
I try to think worry, worry, as that is the experience, but oftentimes the significantly
detracts from the meditation and hurts my focus. Do you have any advice for dealing with this
situation better? Worry, worry is the proper thing to do. There might also be dislike
ing or frustration, but the statement that this significantly detracts from the meditation
is a judgment. The idea that it hurts your focus is a judgment. This is not a problem.
The problem is that you get upset about it and that you wish it were otherwise.
So don't worry about your focus. That's not where we're focusing on.
Don't worry about quality of meditation. There's no such thing. Quality of meditation is in the moment
when you have a moment of clarity. Every moment that you're clearly aware when you say there's
a worrying, that's good meditation. If you don't like it and you say there's a disliking,
you think about the person coming in thinking or the person comes in and you hear them and you
say hearing hearing or you have to open your eyes and talk to them and it's frustrating. It
angers you and annoys you and you say angry, angry or annoyed. Every time you do that,
that's good meditation. Don't worry about anything else. Focus, concentration, calm,
don't know that matters. I mean all of that will come through the practice.
Does a meditator ever take Nivana as object instead of the stomach as a point of return for the mind?
Thank you, Bhante.
It means to take Nivana as the object. The only time you can actually take Nivana as an object.
Well, there's two ways. One, you can conceive of it and that's an actual
tranquility meditation, but the only real way to take Nivana as an object is to enter
Nivana, which is, of course, that's the attainment of enlightenment.
That's the goal.
You're all caught up on questions, Bhante.
All right. Well, thanks everyone for your questions tonight. Thanks Raman for your help.
Thank you, Bhante.
Thank you, Bhante. Good night.
