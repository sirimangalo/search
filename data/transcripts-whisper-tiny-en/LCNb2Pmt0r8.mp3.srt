1
00:00:00,000 --> 00:00:05,720
If a person barged into your house surroundings clearly with the intention to kill

2
00:00:05,720 --> 00:00:10,320
either you or someone near you, would you have let him do it and just console

3
00:00:10,320 --> 00:00:17,880
yourself with the thought that karma will deal with him. Let him do it. Well

4
00:00:17,880 --> 00:00:21,360
you know there are some there's some leeway there but okay let's assuming that

5
00:00:21,360 --> 00:00:28,280
the only options were kill this person or be killed or let him kill someone

6
00:00:28,280 --> 00:00:33,520
near you. I mean if it's going to be someone else then you might try to

7
00:00:33,520 --> 00:00:39,480
sacrifice yourself instead. But again how do you know what the result is

8
00:00:39,480 --> 00:00:44,280
going to be? The funny thing about these situations is they are karmic. Most of the

9
00:00:44,280 --> 00:00:48,720
time they're based on very strong past karma. So you have cases where the

10
00:00:48,720 --> 00:00:55,160
perfectly plotted murder is unsuccessful or you have cases where the perfectly

11
00:00:55,160 --> 00:01:01,520
plotted rescue is botched and people die the person dies no matter what

12
00:01:01,520 --> 00:01:06,440
people try to do. So because you can't know the outcome it's quite dangerous

13
00:01:06,440 --> 00:01:10,000
to play God and to think that you're going to rule the situation and try to

14
00:01:10,000 --> 00:01:16,360
fix things in any situation. So it's important this is why as Buddhists we're

15
00:01:16,360 --> 00:01:23,080
very we're very careful to go to the root and to stick to our guns in regards

16
00:01:23,080 --> 00:01:27,320
to the idea that wholesomeness leads to goodness it leads to happiness and

17
00:01:27,320 --> 00:01:31,920
unwholesomeness leads to suffering and so we're very careful that our actions

18
00:01:31,920 --> 00:01:38,560
are speech and our thoughts are wholesome and we take that as a refuge that we are

19
00:01:38,560 --> 00:01:44,800
not to blame for bad things if our minds are pure and so we let the world

20
00:01:44,800 --> 00:01:49,240
sometimes the world can barge in on us like this. We take it at to be other

21
00:01:49,240 --> 00:01:54,440
people's business or our past business not our present business nothing that

22
00:01:54,440 --> 00:01:59,600
we want to be involved in now. So that's what I would console myself in not

23
00:01:59,600 --> 00:02:03,200
the thought that karma will deal with him. The fact that karma will deal with

24
00:02:03,200 --> 00:02:09,720
the person who who does harm to others it should be a cause for remorse if

25
00:02:09,720 --> 00:02:17,480
anything although remorse is unwholesome but compassion and the belief that no

26
00:02:17,480 --> 00:02:22,840
one deserves to suffer even those people who have done bad you know not

27
00:02:22,840 --> 00:02:26,960
that no one deserves this ever suffering is never a good thing no matter who it

28
00:02:26,960 --> 00:02:31,040
is there's no just there's no there's no gloating over people who have done

29
00:02:31,040 --> 00:02:38,680
bad things that would be unwholesome it would be unwholesome to gloat and so we

30
00:02:38,680 --> 00:02:45,200
would feel compassion and wish for that person to find way a way to become

31
00:02:45,200 --> 00:02:51,160
free from their suffering as well. We don't take so we don't ever take take

32
00:02:51,160 --> 00:02:55,360
consolation in other people's suffering he'll get his karma will get him

33
00:02:55,360 --> 00:02:59,120
back that kind of thing. It's not Buddhist thing to do nor is it a Buddhist

34
00:02:59,120 --> 00:03:06,920
thing to to pass to pass judgment on people because bad things have happened

35
00:03:06,920 --> 00:03:10,920
so if someone's born with a disability for example it's not a Buddhist thing

36
00:03:10,920 --> 00:03:16,040
to dismiss them thinking that they've done bad things in their past life there's

37
00:03:16,040 --> 00:03:21,240
there's a there's a we have this kind of general belief and understanding

38
00:03:21,240 --> 00:03:25,120
that that most likely there's some reason for why they're that way but it

39
00:03:25,120 --> 00:03:29,680
certainly doesn't turn us to think of them as a bad person so some people

40
00:03:29,680 --> 00:03:35,520
criticize Buddhism as being having this potential having the potential to for

41
00:03:35,520 --> 00:03:44,520
abuse whereby people where we judge people who have disabilities and so on

42
00:03:44,520 --> 00:03:49,920
and who have a hard life we say well it's just their bad karma and so we we

43
00:03:49,920 --> 00:03:53,320
look down upon them which is which is not a Buddhist thing to do it's just

44
00:03:53,320 --> 00:04:08,920
not a Buddhist thing to look down on people right anyway so that's that

