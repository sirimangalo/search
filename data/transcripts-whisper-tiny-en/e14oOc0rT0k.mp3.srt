1
00:00:00,000 --> 00:00:05,000
Hello, another question today from C.C. Pathless.

2
00:00:05,000 --> 00:00:08,000
How you to demo I have been wondering about purpose

3
00:00:08,000 --> 00:00:12,000
and why some strive to become the center of their one lives through purpose.

4
00:00:12,000 --> 00:00:15,000
As if a genuine belief in having a purpose,

5
00:00:15,000 --> 00:00:17,000
say through work or education,

6
00:00:17,000 --> 00:00:21,000
is sort of a qualifier for being.

7
00:00:21,000 --> 00:00:24,000
I'm not 100% sure I understand the question

8
00:00:24,000 --> 00:00:26,000
but I think I get the gist of it

9
00:00:26,000 --> 00:00:30,000
and why do people try to find purpose in this life

10
00:00:30,000 --> 00:00:36,000
in terms of becoming successful in education or work.

11
00:00:36,000 --> 00:00:42,000
Why do people try to find these mundane purposes?

12
00:00:42,000 --> 00:00:44,000
I would say purpose in general,

13
00:00:44,000 --> 00:00:49,000
even though people might have altruistic feelings

14
00:00:49,000 --> 00:00:53,000
or might claim to be working for the better goal

15
00:00:53,000 --> 00:00:55,000
or have some higher purpose,

16
00:00:55,000 --> 00:01:00,000
in general it comes down to the happiness

17
00:01:00,000 --> 00:01:02,000
that comes when you succeed.

18
00:01:02,000 --> 00:01:05,000
The happiness comes from getting what you want really.

19
00:01:05,000 --> 00:01:08,000
When people praise you,

20
00:01:08,000 --> 00:01:11,000
when you succeed in something,

21
00:01:11,000 --> 00:01:14,000
when you have something difficult to do,

22
00:01:14,000 --> 00:01:17,000
and you create something that's beautiful,

23
00:01:17,000 --> 00:01:22,000
that's pleasant.

24
00:01:22,000 --> 00:01:25,000
When you get that, it makes you feel happy

25
00:01:25,000 --> 00:01:27,000
and so you're encouraged in your work.

26
00:01:27,000 --> 00:01:30,000
When people do things, people even helping

27
00:01:30,000 --> 00:01:33,000
others, helping poor people,

28
00:01:33,000 --> 00:01:35,000
helping sick people,

29
00:01:35,000 --> 00:01:36,000
so on.

30
00:01:36,000 --> 00:01:38,000
When they see people becoming healthy,

31
00:01:38,000 --> 00:01:42,000
when they see people smiling and having enough food to eat

32
00:01:42,000 --> 00:01:45,000
and so on, it makes you happy and so you want to do it again.

33
00:01:45,000 --> 00:01:47,000
This isn't a bad thing really

34
00:01:47,000 --> 00:01:52,000
but as far as purpose goes,

35
00:01:52,000 --> 00:01:56,000
I think it's more just the feeling

36
00:01:56,000 --> 00:01:59,000
that doing good deeds leads to happiness.

37
00:01:59,000 --> 00:02:03,000
That encourages people to find a purpose

38
00:02:03,000 --> 00:02:07,000
to try to succeed even in terms of education

39
00:02:07,000 --> 00:02:11,000
when you solve a problem or when you write an essay

40
00:02:11,000 --> 00:02:14,000
and it gets good reviews and so on.

41
00:02:14,000 --> 00:02:18,000
When people read it and they appreciate it

42
00:02:18,000 --> 00:02:21,000
and when it has a benefit to other people,

43
00:02:21,000 --> 00:02:24,000
there's a feeling of happiness that comes from that.

44
00:02:24,000 --> 00:02:27,000
This is in a worldly sense.

45
00:02:27,000 --> 00:02:32,000
The reason why there is progress in the world,

46
00:02:32,000 --> 00:02:35,000
the reason why we have scientific investigation

47
00:02:35,000 --> 00:02:37,000
and discovery and so on,

48
00:02:37,000 --> 00:02:39,000
because there is kind of, in a sense,

49
00:02:39,000 --> 00:02:41,000
you can say an addiction.

50
00:02:41,000 --> 00:02:44,000
Even in a Buddhist sense,

51
00:02:44,000 --> 00:02:48,000
this is considered a good thing.

52
00:02:48,000 --> 00:02:51,000
It's good to help people.

53
00:02:51,000 --> 00:02:54,000
It's good to do things that are purposeful

54
00:02:54,000 --> 00:02:56,000
for the purpose of benefiting others

55
00:02:56,000 --> 00:02:58,000
but it's only good in a mundane sense

56
00:02:58,000 --> 00:03:04,000
and it really isn't a purpose in the ultimate sense.

57
00:03:04,000 --> 00:03:07,000
In the end, you're just perpetuating

58
00:03:07,000 --> 00:03:12,000
this cycle of rebirth, the cycle of existence.

59
00:03:12,000 --> 00:03:14,000
You can't say that by helping society,

60
00:03:14,000 --> 00:03:17,000
helping sick people and poor people,

61
00:03:17,000 --> 00:03:19,000
somehow you're going to end poverty

62
00:03:19,000 --> 00:03:21,000
and you're going to end sickness.

63
00:03:21,000 --> 00:03:23,000
The causes go much deeper than that

64
00:03:23,000 --> 00:03:25,000
and in the end, of course,

65
00:03:25,000 --> 00:03:27,000
there's nothing you can do to help this earth

66
00:03:27,000 --> 00:03:30,000
and we're going to go crashing into the sun

67
00:03:30,000 --> 00:03:33,000
but will have burned up to a crisp long before that.

68
00:03:33,000 --> 00:03:39,000
It's kind of like an impossible task

69
00:03:39,000 --> 00:03:41,000
unless you subscribe to the idea

70
00:03:41,000 --> 00:03:44,000
that we're somehow going to colonize the stars,

71
00:03:44,000 --> 00:03:46,000
but even then, it just goes on and on.

72
00:03:46,000 --> 00:03:49,000
None of this is a final solution.

73
00:03:49,000 --> 00:03:51,000
There is no such thing as a final solution

74
00:03:51,000 --> 00:03:53,000
because everything changes

75
00:03:53,000 --> 00:03:57,000
and with change comes new problems

76
00:03:57,000 --> 00:04:00,000
and new difficulties.

77
00:04:00,000 --> 00:04:08,000
In the end, there will be no intrinsic benefit

78
00:04:08,000 --> 00:04:10,000
to any of these things.

79
00:04:10,000 --> 00:04:14,000
The benefit that comes from doing purposeful acts,

80
00:04:14,000 --> 00:04:17,000
even educating yourself or working hard

81
00:04:17,000 --> 00:04:20,000
is how it changes your mind, how it develops,

82
00:04:20,000 --> 00:04:23,000
how it brings you closer to an understanding of this

83
00:04:23,000 --> 00:04:28,000
and understanding of the reality of the universe

84
00:04:28,000 --> 00:04:31,000
that nothing is worth clinging to,

85
00:04:31,000 --> 00:04:36,000
that we should be able to let go, be able to give up

86
00:04:36,000 --> 00:04:43,000
not to need, but to be able to be able to let go

87
00:04:43,000 --> 00:04:45,000
when you give something to someone else

88
00:04:45,000 --> 00:04:48,000
when you help someone else, it's an act of letting go

89
00:04:48,000 --> 00:04:51,000
and so it helps you to come back to your center

90
00:04:51,000 --> 00:04:54,000
and define the permanent which is the knot moving,

91
00:04:54,000 --> 00:04:58,000
the knot wandering on, the knot continuing

92
00:04:58,000 --> 00:05:02,000
of creation and of existence.

93
00:05:02,000 --> 00:05:04,000
I hope that helps.

94
00:05:04,000 --> 00:05:07,000
If you clarify the question, if I haven't answered it,

95
00:05:07,000 --> 00:05:12,000
then please do and I'll be happy to continue the discussion.

96
00:05:12,000 --> 00:05:28,000
Thanks for the question.

