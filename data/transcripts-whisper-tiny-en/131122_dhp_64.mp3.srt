1
00:00:00,000 --> 00:00:06,320
Okay, welcome back to our study of the Damabanda.

2
00:00:06,320 --> 00:00:14,440
Today we continue on with verse number 64, which reads as follows.

3
00:00:14,440 --> 00:00:34,360
Thank you very much, Baba.

4
00:00:34,360 --> 00:00:44,440
Yeah, what do you want, B.J. Malo, a fool who, for his whole life, burned it down by

5
00:00:44,440 --> 00:00:52,040
your capacity, attends upon or associates with a wise person?

6
00:00:52,040 --> 00:01:01,640
Not so, that among which I did, such a person, such a fool, even though they spend their

7
00:01:01,640 --> 00:01:08,680
whole life with the wise person, doesn't realize the dogma for themselves.

8
00:01:08,680 --> 00:01:25,160
Dambi suprasangita, just as the spoon in the soup, doesn't taste the soup.

9
00:01:25,160 --> 00:01:36,040
Ah, so this is a story, a verse based on a story, another very short story, of the

10
00:01:36,040 --> 00:01:43,120
venerable Dahi, who gets a bit of a fool, not sure if this is Lalo Dahi or just another

11
00:01:43,120 --> 00:01:51,800
vu Dahi, but it's one of those people who never really caught on to the actual teaching.

12
00:01:51,800 --> 00:01:58,840
It may not have been a bad monk, but he just kind of stuck around and didn't really

13
00:01:58,840 --> 00:02:03,760
have a clue as to what the Buddha taught and certainly hadn't realized it for himself.

14
00:02:03,760 --> 00:02:11,560
But when the elders left the Dhamma Hall and there was nobody around, he liked to go and

15
00:02:11,560 --> 00:02:20,560
sit up in the Dhammas and the Dhammas Dhamma chair, that they have a special chair where

16
00:02:20,560 --> 00:02:25,680
someone when they're giving a Dhamma talk is sits, so that even a younger monk would

17
00:02:25,680 --> 00:02:30,640
be sitting higher than the senior monks, for the time that they were giving the Dhamma

18
00:02:30,640 --> 00:02:31,640
talk.

19
00:02:31,640 --> 00:02:44,400
So whoever's teaching the Dhamma, they would have them sit higher as a show of respect.

20
00:02:44,400 --> 00:02:49,280
And so he used to go and sit on it and pretend like he was some great out-learning

21
00:02:49,280 --> 00:02:50,960
and what he was doing.

22
00:02:50,960 --> 00:02:55,440
So one day he's doing this and a bunch of visiting monks come in and they see him up on

23
00:02:55,440 --> 00:02:58,640
the Dhamma chair and they go, this must be some great elder.

24
00:02:58,640 --> 00:03:10,000
And so they all go up and they sit around him and ask him all sorts of questions about

25
00:03:10,000 --> 00:03:14,960
the aggregates and very deep subjects, but the five aggregates and the twelve.

26
00:03:14,960 --> 00:03:22,520
I had done a twelve basis and the eighteen, that was the eighteen element.

27
00:03:22,520 --> 00:03:28,320
I asked him all sorts of questions and he's like, I don't know if he has to, he turns

28
00:03:28,320 --> 00:03:38,360
out to be a fool and not have any answers and the other monks are appalled because the

29
00:03:38,360 --> 00:03:41,040
monastery was staying with it was the monastery of the Buddha.

30
00:03:41,040 --> 00:03:44,040
So he said to themselves, how can it be?

31
00:03:44,040 --> 00:03:46,720
Here you are, how long have you been a monk and I guess he was an elder so he had been

32
00:03:46,720 --> 00:03:50,520
a monk for at least ten years or at least he had been there for some time and they

33
00:03:50,520 --> 00:03:58,400
were appalled that he had been with the Buddha all that time and hadn't learned anything.

34
00:03:58,400 --> 00:04:07,120
And so criticizing him, they went over to pay respect to the Buddha, they waited until

35
00:04:07,120 --> 00:04:10,600
they appropriate time and made an appointment to see the Buddha and went in and paid respect

36
00:04:10,600 --> 00:04:14,520
to the Buddha and said, when the Buddha was there, there's this monk and he's sitting

37
00:04:14,520 --> 00:04:19,440
up in the dhamma chair and we asked him all these questions and he had in the clue and

38
00:04:19,440 --> 00:04:24,160
we're thinking how can it be that someone who'd been with you so long, looking in the

39
00:04:24,160 --> 00:04:32,200
same monastery as the blessed one himself still wasn't able to realize the dhamma firm

40
00:04:32,200 --> 00:04:40,120
there was such a, had no clue about the dhamma and the Buddha taught them, gave me teaching and spoke

41
00:04:40,120 --> 00:04:45,520
this verse, he said, even if a fool, even if they lived together with a wife, with wise people

42
00:04:45,520 --> 00:04:53,960
for a hundred years, because they're a fool, they never realized the dhamma no matter how long

43
00:04:53,960 --> 00:05:02,520
they, how much time they spend and then he said, it's just like this, just like the spoon, when you put a

44
00:05:02,520 --> 00:05:07,600
spoon in the soup, it never has an opportunity to taste the soup no matter if you keep it in the

45
00:05:07,600 --> 00:05:16,240
soup for a hundred years. So that's the verse, that's the story, that's the verse, the actual

46
00:05:16,240 --> 00:05:23,880
teaching behind it is obviously more generally applicable than just the story, it's a caution to

47
00:05:23,880 --> 00:05:30,040
us, and some people might even be surprised that this is possible, how could it be that you could

48
00:05:30,040 --> 00:05:40,400
spend so much time even your whole life with wise people and never become wise and the analogy

49
00:05:40,400 --> 00:05:48,360
of the spoon in the soup is quite apt, it's because of the nature of the people, if a person is

50
00:05:48,360 --> 00:05:57,600
of a nature to be cruel and lustful and arrogant and deluded and all these bad things, so performs

51
00:05:57,600 --> 00:06:13,400
unwholesome acts and it's unwholesome in their speech and unguarded in their behavior and unwholesome

52
00:06:13,400 --> 00:06:20,160
in the mind, if they're inclined towards unwholesomeness, then physical presence doesn't help them.

53
00:06:20,160 --> 00:06:29,080
You see this acutely with even people who can answer questions and do have knowledge of the

54
00:06:29,080 --> 00:06:45,960
dhamma, people who have studied a lot but have never practiced can still inside be incredibly

55
00:06:45,960 --> 00:06:56,000
clueless to the truth, unenlightened inside, so they have trouble answering questions in a

56
00:06:56,000 --> 00:07:02,120
cogent fashion, they aren't able to put together the Buddha's teaching into a way that's

57
00:07:02,120 --> 00:07:06,640
understandable because they themselves don't understand it, so when asked deep questions even

58
00:07:06,640 --> 00:07:14,160
people who have studied a lot, it's easy to tell whether they have or not practiced based on

59
00:07:14,160 --> 00:07:20,440
their ability to explain the teaching and the coaching man. Meditation is really the key, I always

60
00:07:20,440 --> 00:07:25,040
try to bring these videos back to our meditation practice and it's the meditation that allows

61
00:07:25,040 --> 00:07:34,960
you to taste the soup so to speak. If a person isn't practicing the teaching, it's like reading a

62
00:07:34,960 --> 00:07:39,240
map but never driving or reading the driver's manual and never actually getting behind the wheel,

63
00:07:39,240 --> 00:07:47,920
our knowledge of the Buddha's teaching and even our irrational understanding and agreement of

64
00:07:47,920 --> 00:07:57,520
the Buddha's teaching, it's similar to any philosophy where you find philosophers passionately

65
00:07:57,520 --> 00:08:03,200
arguing for a certain philosophy but not following it themselves, you find doctors who smoke

66
00:08:03,200 --> 00:08:16,560
cigarettes or themselves have poor health habits. This kind of hypocrisy is one way of putting

67
00:08:16,560 --> 00:08:22,160
a Buddha, it can just be a superficial understanding or a lack of understanding. There are many

68
00:08:22,160 --> 00:08:31,680
reasons for this, there are monks who become monks for the wrong reason. They ordain because

69
00:08:31,680 --> 00:08:38,720
they have nowhere else to go, there are social outcasts and so on in Asian countries, it's often

70
00:08:38,720 --> 00:08:46,160
poor people who ordain as novices, novices will become novices not because they have this keen

71
00:08:46,160 --> 00:08:50,880
interest at seven years old to become enlightened but because at seven years old they don't have

72
00:08:50,880 --> 00:08:55,040
enough food to eat and their parents can't feed them so they go to the monastery and they're

73
00:08:55,040 --> 00:09:00,960
promised a meal and shelter and getting off on the wrong foot like this, they grow up in the wrong

74
00:09:00,960 --> 00:09:07,600
way and by the time they become monks they're in their trained in their mind, they're trained in

75
00:09:07,600 --> 00:09:14,320
the wrong way, they're set in being a career monk and as a result they don't have the inclination

76
00:09:14,320 --> 00:09:23,120
or the they're in your and you can say against the realization in your, I don't know if that's

77
00:09:23,120 --> 00:09:28,480
right, I'm looking for like insulated against it, they're not able to realize it because of the

78
00:09:30,560 --> 00:09:36,720
now they're in your and I think it's the word again but they're far from it, they're not able to

79
00:09:36,720 --> 00:09:43,840
connect with the Dhamma because it's a they're they're way of thinking about it as as a livelihood

80
00:09:45,040 --> 00:09:53,840
and as an intellectual pursuit or as a culture there's so much ingrained in them that is is wrong

81
00:09:54,400 --> 00:10:00,320
whether the Buddha said it's I think the Buddha said or also was on the commentary is that

82
00:10:00,320 --> 00:10:09,520
it's harder to remove right view than it is to remove wrong view, they're paraphrasing

83
00:10:09,520 --> 00:10:14,640
but the point is the person with wrong view you can teach them meditation quite easily because

84
00:10:14,640 --> 00:10:18,000
they're wrong views it's very easy to change and they when they realize how wrong they are

85
00:10:18,000 --> 00:10:21,920
they give up and they follow the teaching but a person who has right view and who knows all the

86
00:10:21,920 --> 00:10:25,920
truth intellectually it's very hard to teach because no matter what you say they say yes I know

87
00:10:25,920 --> 00:10:30,240
they yes yes yes I know and they don't realize that actually they don't really know what they're

88
00:10:30,240 --> 00:10:40,560
talking about they know everything but they know nothing and then there are monks who are

89
00:10:40,560 --> 00:10:46,000
corrupt who are just using it as a livelihood or even engaging in evil practices

90
00:10:51,520 --> 00:10:55,520
and then there are just people who never get it it's I mean for most of us it's very difficult

91
00:10:55,520 --> 00:10:59,760
we're talking last night about the four types of lotus there is just some people who will never

92
00:10:59,760 --> 00:11:07,520
get the demon who realize it maybe their minds are in the wrong place but we notice that that's

93
00:11:07,520 --> 00:11:11,040
too discouraging I think it's not really what we're talking about here we're talking about people

94
00:11:11,040 --> 00:11:15,600
who don't want to learn people who are foolish who are engaged in wasting their time and

95
00:11:18,960 --> 00:11:24,720
are too lazy to actually put the teachings into practice again it's not a criticism or it's not

96
00:11:24,720 --> 00:11:33,200
really not a want to go around pointing fingers but it's an admonishment to all of us to take

97
00:11:33,200 --> 00:11:39,600
this seriously that we should never be complacent just because we're Buddhists are obviously just

98
00:11:39,600 --> 00:11:43,840
because we're monks I think that's often the case there are people who think that just because

99
00:11:43,840 --> 00:11:49,440
we're monks who are perfect Christian there's the opposite case people who don't never give monks

100
00:11:49,440 --> 00:11:56,880
a break and are always criticizing the smallest faults in monks I know there's people like that

101
00:11:58,400 --> 00:12:04,480
we don't realize how difficult it is to be a monk but it certainly isn't enough it's not enough

102
00:12:04,480 --> 00:12:10,880
to call ourselves Buddhist they people are the same it's very easy to become complacent thinking

103
00:12:10,880 --> 00:12:16,320
that I'm Buddhist now and that's enough it's certainly not enough a person the Buddhist at a person

104
00:12:16,320 --> 00:12:20,800
can live their whole life with a wise person and never become wise it has all to do with our own

105
00:12:20,800 --> 00:12:28,560
practice good companions are only useful if we emulate them if we follow them if we learn from them

106
00:12:28,560 --> 00:12:36,960
otherwise we become a burden on them and they're better off without us so our practice of

107
00:12:36,960 --> 00:12:45,120
cultivating wholesome actions wholesome speech wholesome thoughts and cultivating wisdom

108
00:12:45,120 --> 00:12:52,400
it's all it's all the burden is all placed on us it's at every moment how we act how we speak

109
00:12:52,400 --> 00:13:01,040
how we think whether we have mindfulness whether we have awareness whether we're cultivating insight

110
00:13:01,040 --> 00:13:08,000
and wisdom for ourselves that's the determination of whether we taste the soup otherwise we're

111
00:13:08,000 --> 00:13:15,760
just like a spoon it can be surrounded by the teachings but never realize the teachings for yourself

112
00:13:17,120 --> 00:13:22,240
still not a bad thing there's lots of good that can come from being surrounded by the Buddhist

113
00:13:22,240 --> 00:13:27,840
teaching and being being a monk and so on but to realize the Buddhist teaching you have to practice

114
00:13:27,840 --> 00:13:32,240
for yourself and being said the Buddha said even the Buddha is just one who shows the way

115
00:13:32,240 --> 00:13:39,040
it's us who have to walk the path so simple teaching something is a reminder for us

116
00:13:39,680 --> 00:13:44,960
and it's a nice analogy for us to remember the simile of the spoon in the soup

117
00:13:45,680 --> 00:13:50,080
tomorrow we have the other side of the coin those who can taste the soup so tune in not to more

118
00:13:50,080 --> 00:13:55,360
next time so tune in next time thank you all for keeping up and for the positive feedback

119
00:13:55,360 --> 00:14:02,160
I appreciate it and I'm glad to know that these videos are a benefit which for everyone out there to

120
00:14:02,160 --> 00:14:06,640
be able to put the teachings of the Buddha into practice and find true peace happiness and freedom

121
00:14:06,640 --> 00:14:30,880
from suffering I think

