Okay, good evening, everyone.
Welcome to our evening, Dhamma.
Today, we are studying the Mahangupala Sutta.
And it's about a gopala.
Gomin's cow in Bali.
Bala means one who...
Gopala cow.
Bala cow means one who...
who cares for who tends to guard.
So a cow garder.
A cow herd.
Like someone who looks after cows.
And why we're talking about cowards is because the Buddha compares a practitioner with a cow herd.
There's a cow herd, some cowards.
Some cowards are good at what they do.
They're able to keep and raise a whole herd of cattle.
But some other cowards are not able to.
They're not very good at their job.
In the same way, meditators, practitioners, Buddhists, let's say.
Some Buddhists just aren't very good at their job.
They just aren't very good.
And so this really serves as a guide.
This one is quite deep about meditation.
There's not a lot of fluff in this Sutta, but I think it's really useful for providing direction specifically for meditators.
So there are eleven qualities of a coward.
A coward has to, first of all, to be root by new means.
It has to be knowledgeable about physique, about the physical.
So a cow herd has to know something about the cows, about the body of the cows.
If a person is going to buy cows, you have to buy the right cows.
Know something about the body, about anatomy.
You'll be able to keep the cows healthy, so you need to know a lot about their physical characteristics.
The same way of a practitioner, meditator, is where it all starts, and all starts with the body.
Meditator should be aware of the physical realm.
It's so important because it compromises most of what we think of as reality.
When you think of the world, the universe, existence, we don't think all of the existence is my mind.
I think of existence as the physical realm.
When I ask about you, who are you?
When I ask someone to describe a person, the first thing that is going to be physical.
Are they male?
Are they female?
That's always been a big part.
They hold their young, but all has to do with the physical body.
We get into qualities of the mind as well, but much of who we are and how we relate to the world is physical.
If we're going to be good at this, good at understanding ourselves, we have to understand the physical.
When we begin to meditate, of course, we're using the body as our primary object.
Throughout the meditation course, the body is the primary object.
The Buddhist has the way to discriminate someone who understands the body and somebody who doesn't.
Someone who understands that the body is the physical reality is the four elements.
This is called wisdom.
It's regarding the body.
When I learned about them in high school, one of the first things our teachers taught us was, this is how the simple people, the primitive people, the Greek, Greece, we usually hear about it.
This is how primitive people thought of science, the thought of the world, as far as they could get.
Of course, we've got much further now and so on and so on.
I was thought of the four elements as a primitive description of the physical realm.
I'm not very advanced, not very interesting, in fact.
Quite a bit more special than any physical theory about the micro subatomic physics and astrophysics and all that.
Because they relate to experience.
So a person who understands the Buddha says a person who understands the physical.
Someone who understands that it's experience.
It's something that is experienced.
And talk about physical, we're not thinking about concepts.
Concepts are not physical, they're mental.
What is truly physical is what is experienced.
Feeling of earth, air, water and fire.
These aren't the earth, air, water and fire that we normally think of.
Earth is just a name for the aspect of experience that is hardness.
So air is that aspect of experience that is tension, water or fire, that aspect of experience that is heat.
And water is that aspect of not quite experience, but kind of, that is the cohesion or stickiness.
Attraction again.
The idea is that our outlook on the world, our perspective, our relationship with the world, has to be experiential.
That our body, in by extension, the rest of the universe, because of course, we don't experience the universe as human beings, except through the body.
But whatever we do experience, that physical aspect of it is.
It's just these experiences.
It's very important for meditators. It's important to be primarily.
It's the first step to becoming a meditator, just to be mindful of your experience, how you feel against the floor, how the temperature feels.
And stiffness in your limbs and so on.
Begin to, it's the beginning of approaching the realm of experience, where you're aware of the experiential world.
So that's number one.
Number two, a cow herd has to be skilled in lakana, lakana kusul, has to be skilled in the characteristics.
Is this cow, you know, the nature of the cow?
What characteristics of the cow? Make it a good one. Not just is it a big body or an introvert's body, but also maybe it's hooves or it's ears or it's horns or maybe it's tongue or it's teeth or something.
All the different characteristics.
Likewise, I guess the analogy here is to ourselves to think of ourselves as either a herd of cows or a cow.
Maybe we're a cow and we're looking after ourselves or experience of the cow.
And so if you think about yourself, if I'm a cow, what makes me a good cow?
It says a practitioner has to be skilled in characteristics, knowing what are good qualities and what are bad qualities.
What makes one a good person and what makes one a bad person?
How do you know the cow is worth buying? Should I buy this cow or that cow?
What makes a good cow?
So he says for a meditator, being skilled in qualities or characteristics is to know what makes one a wise person and what makes one a fool.
The fool is likewise distinguished by their actions.
So the meaning here that I get out of this is that it's not because you're Buddhist or because you're a meditator.
If you just say, I'm Buddhist and I agree with everything the Buddha said and my teacher is the Buddha and so on.
It's not because you come here and you're a meditator.
You know, this would be a big thing for monks. When you become a monk, it could be quite a prestigious thing.
Look at me and doing this great thing, living off in the forest or just even just wearing robes.
Maybe some of you put on white clothes and that's going to be a great encouragement because then you have a uniform.
But it can also be for some people who wear white clothes for years. It can be an attachment.
We're not wise just because we follow ourselves Buddhist or monks or meditators, not even because we meditate.
Well, that's not true. Not just because we sit and close our eyes, but because we meditate.
Because of our actions, not just because we meditate because of all the many things as Buddhists that we do.
Primarily practicing mindfulness, but in total based on our actions, a person is not a good person because of their name or their background or their wealth or their fame.
A good person because of their actions, because of the mental action really, the mental quality of their mind, the things that they do, the inclinations of the mind.
So that's number two. Number three, the cow herd passed in on how to take care of their cow's health.
And that includes being aware or being unguired against flies.
The last thing you want on your cows is for the flies to lay eggs. Apparently, it can be a very nasty thing if you're supposed.
Your cow has some kind of a wound and the flies lay eggs in the wound. It can actually make the cow very sick and perhaps even die.
So a cow herd has to be vigilant and take care of their cows. I mean nowadays, I suppose they have all sorts of medicines and things that I don't know how they do it nowadays, but in ancient India, in India, the flies where you have to watch out for them fly eggs.
That's a good analogy. You know, it's quite vivid meditators. There's this song, there ain't no flies on us. It's a chant that you do when you're competing with other people.
When we were kids, we used to chant, there ain't no flies on us.
This is a kind of a festering, right? You don't want to let things fester. There's meditators. It's a very important aspect of our practice that we are vigilant.
It's not just about closing your eyes or it's not even just about trying to watch your stomach or go through an exercise.
If you do that, but you're not meticulous about all the little aspects of your mental activity, it's very easy for something to fester.
So the Buddha says when desire arises. One is not mindful of it. Suppose your mind full of other things, but the desire you let it go, you follow it, you cultivate it.
One thing this, one thing that, maybe it's just about food. Maybe you have some special food that you like to eat and you really sit and enjoy it.
So it's your break from the practice and then you have the food and it's just so enjoyable.
You let it fester, let the eggs grow. Or maybe it's just like something you don't like and rather than being mindful of it. You let it grow.
So this is a common thing in the beginning. As a meditation teacher, I'm responsible for your flies as well.
I'm going to have to catch a meditator when they let it do very caring for the meditators.
You have to care for their own minds and the teacher has to be very careful and cautious in supporting the meditators not to let something fester, something dislike.
It could be something at the center. Maybe they don't like the food and they let it get to them.
Maybe it's too cold, too hot.
Here it's not too cold, even though it's very cold in Canada. We have quite the luxury of the heat here.
In Thailand, why? Sometimes. It's a much hotter climate, but there's no heat.
So in the winter it could get very cold at night.
Anything you have to be very careful not to let it fester.
The third one, some kind of delusion. They say cruelty and I said trouble with this third one, but I like to think of it more as arrogance.
It's not really actually what it means. I don't know.
I'm going to scoot a fudge that one because we have ill-will and cruelty and I don't really see the difference there.
Anyway, I think I looked at that into that, but let's just skip over it.
Because in general the point is when evil and wholesome states have arisen, some cowards just tolerate them.
Don't abandon them, don't remove them, don't obey them.
If any un-wholesome states, and again by un-wholesome we're not,
if that's something we feel guilty about, we shouldn't feel guilty about it.
We should just understand that these things are for our own detriment.
We're going to ruin us. We'll let the cow eggs fester, nobody else is going to suffer, but you.
It doesn't make you a bad person that you have to feel ashamed.
It makes you a bad person because you have to suffer.
The difference between a bad person and a good person is only that they suffer more.
A bad person suffers more than a good person. That's all that means.
That's an important part of the practice too, is that don't be discouraged that you have flies egg.
Just learn how to deal with them, and until you do you're going to suffer terribly.
The next one, I don't know how many numbers which one were number at.
A cow herd has to be able to dress the wounds, so not just keep them from getting infected.
Well, big part of keeping them from getting infected is caring for them.
When you're wounded, now we're all wounded.
This is again an apt analogy because our minds are wounded.
We're talking about the wounds of the mind.
This cow of our mind has all sorts of scar, and that scar has been open wounds.
That are just ripe to be a professor to get infected.
In fact, I think the wounds here that he's talking about are just the senses.
The senses are wounds.
We're talking in a very deep philosophical sense here.
I mean, this deep claim that's very difficult to stomach, let alone understand.
It's that sensual experience of seeing, hearing, smelling, tasting, and feeling, thinking.
It's like, well, let's not be too negative and say they're like wounds, but the point is
they have the potential for causing us suffering.
This is where the, this is where the defilements arise.
Is that the senses?
And so a meditator tending to their senses is like a cow herd tending to the wounds of the cow.
Keeping them clean, keeping them clean basically, keeping them covered.
Two things you have to do, keep them clean and keep them covered.
So how does a meditator keep their wounds clean and covered?
First of all, they clean them using mindfulness.
So we clean up, we keep our senses clean when we see something with the eye.
But the Buddha uses a very useful phrase that helps us understand what meditation is meant to be like.
He says, not nimitagahinana nimjana gahi.
Sometimes meditators complain that when we meditate, when we use the mantra,
it doesn't allow us to have a deep understanding of the details and the characteristics of the experience.
But that's actually the point. The Buddha says, nimitagahinana nimjana nimjana gahi.
Namitagahavi, the characteristics and namjana nimjana gahi, the details, the specifics, the particulars.
One doesn't grasp nagahin.
Because if one grasp with the particulars, that what you see, if you're focused on seeing flowers in the carpet, for example,
the flowers are what give rise to potential, desire or attachment.
Like this story, there was a monk who was walking for alms and this beautiful woman came out.
She was running away from her husband and she saw him and she laughed.
He looked up and he was so focused on his practice that all they saw were her teeth.
It made him think not of something beautiful, but of a skeleton.
When she ran by and the husband came and said, have you seen this woman?
Well, they saw some bones.
I think he was very much practicing this type of meditation where you're aware of the parts of the body.
It's a good way of helping to overcome desire for the body.
You see that the body is really just made up of mucky stuff.
There's nothing beautiful about it.
But when we're mindful, we keep this sort of purity.
You see, it's just seeing, it's eight years after seeing.
And that's the end of the story.
There's no room for it.
There's nothing desirable about seeing.
It's only the particulars, the concepts, and then the recognition, sunya, which allows us, reminds us,
hey, that's like that thing that made me happy before.
Maybe I should chase after that.
Not maybe, but then, right away, there's the desire for it.
How you keep them covered, how you keep your wounds covered is by caring for the senses in terms of not running around,
chasing after experiences that are going to give rise to desire and aversion,
mainly coming to a meditation center.
I'm going to talk a lot about seclusion, finding a secluded place,
coming to a meditation center is a great way to do that,
even just closing your bedroom door, finding a quiet spot,
going off into the park or into the forest for the day,
spending some time alone.
It's a great way to keep your wounds covered.
It's not going to keep them clean,
but it helps.
It helps keep them clean, helps keep them from getting infected again.
If you do these two things, basically live a simple life,
or live a secluded life.
Simple life is maybe the best way to explain it.
And then in that simple life, you undertake a sort of a diligent practice of objectivity,
and this mindful state where you're aware of things as they are,
not being deluded and confused and prejudiced.
That's how one dresses one's wounds.
And then I guess this is something that cowards up.
Do they have to smoke out the sheds?
I don't know what that refers to,
but I guess keeping the flies out of the barn,
keeping the bugs out of the barn, something you smoke.
Smoke is a good way to keep bugs away.
That's the only thing I can think of here.
Maybe smoke out means to get rid of the critters that are in them.
You don't want critters in the barn, so it may be mice or something.
So if you smoke them out, they all go away.
And this is, I think, the equivalent of the English phrase of getting the cobwebs,
sweeping away the cobwebs.
We say that about the mind, right?
Cobwebs in your mind are laziness, are they?
Right, keeping house.
Keeping house.
This actually refers to teaching the dhamma.
So a meditator doesn't teach other people the dhamma.
It doesn't surround themselves with other people who are also practicing.
But it did encourage spreading the dhamma, sharing it with others.
And the idea here is that it cleans out the barn.
Those are the cobwebs of the mind, but it's your environment.
The most important thing about your environment is that you're surrounded by other people who are meditating.
And so a big part of that, specifically since you're the way to do that,
is to share the dhamma with each other, to teach each other new people who come,
teach them how to meditate.
We should have the old meditators teach the new meditators.
They show them how to do walking meditation, sitting meditation, mindful frustration.
When I first went to Thailand, it was one of my duties for a long time,
was to show people how to do the mindful frustration in the walking city.
Even just that much, you did a great service to people.
Just giving them the technique.
That's sometimes useful. The next one is a cow herd has to know the forward, the crossing,
where to cross the river. If you've got cows, you can't just say,
get on across that river and have them all drown.
You have to know where to cross.
How do you find out where to cross while you ask people?
He says, there are some meditators who never go and ask about the teaching.
Never go and find a teacher. Never go and ask questions.
Never go and study.
Then go and try to find wisdom knowledge, to find the way across the river.
This is important. It's important to ask questions.
It's important to have it to find a teacher.
Seek them out and ask them your questions and ask for help with your practice.
If you don't go to them, they can't teach you.
The next one is someone who doesn't understand something about which cows have drank water.
They've gone down to the river.
How do you know which ones have drank their fill?
Some meditators, some practitioners don't know what it means to have drunk your fill,
to have already drank the water.
Some people don't, don't aren't inspired by the teachings.
For some people, it's quite uninspiring to think about being objective and to have a pure mind,
to have clarity of mind, to be present here and now for some people who's not inspiring.
You should be inspired to be pure and to be cleansing and constantly,
bettering yourself, thinking your mind better.
The greatest thing we can do for ourselves is being present
and cultivating all the clarity of mind and wisdom that comes from just being here.
Just being, that's really the most amazing thing about mindfulness is how much
the depth of wisdom and the challenge to the very core of our being
that comes from just being present.
You think, well, there are other things that are more challenging than that,
maybe it's challenging, but it's not the ultimate challenge.
It in fact turns out that, yes, it is the ultimate challenge to just be here now
without judging, without reacting, without getting lost in the past and the future
just desires and imagination and so on.
So some people are inspired by this.
When they hear the dhamma, when they think about practicing the dhamma, it's inspiring for the brothers,
they're not inspired.
I don't make a very good callard.
Because they don't know what it means to be full of the dhamma, to be inspired by the dhamma.
And then some cowards don't know the path.
Don't know the road.
I had a funny experience with this.
I can attest, if you don't know the road, it's quite in Thailand, they send the cows up into the hills.
People live in the hills of Thailand, and it's really hills rolling hills, almost mountains,
but just hills as far as the eye can see, and pads everywhere.
So I was following a cow path, off into the wilderness, and then I wanted to go back
and suddenly it turned around and realized that there were more than one path.
In fact, there were pads up and down every hill.
I got lost. It was my big story of how I got lost in the forest.
For hours and hours, I was lost.
Tell that story a lot for various reasons.
But here are the paths that we need to know is the eightfold normal path.
The eightfold normal path, right view, right inclination, right speech, right action,
right livelihood, right effort, right mindfulness, right concentration.
Basically, morality, concentration, and wisdom, are you ethical?
Do you know what it means to be ethical?
Are you focused? Do you know what it means to be focused?
Are you wise? Do you know what it means to be wise?
That's knowing the path.
The next one is being skilled in pastures.
How far does the pasture go?
How do I know when the cows are out of the pasture?
I know they've gone outside of the pasture.
I have to know how far your pasture goes.
If they stray from that, there's all sorts of problems.
Maybe there are holes in the field, or maybe there are wild animals.
Maybe someone will steal the cows if they go outside the pasture.
There's lots of dangers.
The meditator has their pasture.
We would have talked often about this the pasture.
He said, our pasture is what?
Our pasture is the four foundations of mindfulness.
A meditator should always, always, always be within the boundaries of the four foundations of mindfulness.
If you read the four foundations of mindfulness, it's a bit complicated.
Overcomplicated, perhaps.
That's because it's a practical teaching.
But the pastures are just reality.
But the problem is that reality is a heavily latent term that means so many different things to different people,
depending on how you look at it.
Experience a reality, experience.
Our boundaries are the boundaries of experience.
When you see something, your boundary is the same.
So mindfulness is the practice of, is how you behave.
Your boundary should be mindfulness.
You don't go any further than seeing is seeing.
Hearing is hearing.
Seeing comes up.
What is that?
That's hearing.
And that's all.
That's your boundary.
That's your pasture.
As soon as you get beyond that, now the danger.
That's where the danger comes.
So knowing the, using the four foundations of mindfulness, this is how we keep our minds within the confines of reality.
Seeing is seeing, hearing is hearing.
The body is the body, right?
Feeling hard, soft, moving the body.
Move the body knowing that it's moving.
When you sit knowing that you're sitting when you stand or that you're standing.
Feelings when you have a, a, a, a pleasant feeling when you feel painful feeling knowing that you're standing.
It's a painful feeling knowing that it's a painful feeling.
Not letting your mind go beyond that.
When you have thoughts about the pasture, the future knowing that they're just thoughts or thoughts.
Doesn't matter past thoughts, future thoughts, good thoughts, bad thoughts.
Sometimes thoughts can make us very, very, very upset, overwhelmed.
If they're very bad thoughts, right?
Something bad happened to us in the past, something very bad.
That thought can consume us for years, for a throughout our lives.
Which when you think about it is, I mean, it's understandable, but it's kind of silly because it's just a thought.
It's one of the big things you learn as a meditator.
A lot of our problems are pretty silly.
And if only we had known these simple, only we had really had some guidance to say,
look, it really is just a thought.
It really is just a feeling.
It really is just the body.
And emotions, even emotions.
When you're angry, what am I going to do about this?
And everyone will have a different opinion and advice about what you should do in your angry.
Or when you want something.
Well, go get it.
You want it.
Go get it.
Follow your heart.
Lots of different advice for when we want or we like or dislike things.
How to behave when we're angry, when we're sad, when we're afraid.
All sorts of ways of dealing with it.
That are outside of the realm of mindfulness.
And so even our emotions, even them,
those things that really can cause such stress and suffering.
Well, they can't if we see them as they are.
If you see fear as fear, there's nothing to fear.
If you see worry as worry, and that's all it is.
People kill themselves over depression.
And yet it's just depression.
If they can only learn to see it just as being depressed.
That's why we use mindfulness.
That's our posture.
Another one is some cowards try to milk the cows when they're dry.
I know they try to milk the cows all the way until they're dry.
Milk them for all their worth, which I don't know whether that's a bad thing for cowards to do.
I don't know whether it actually is harmful to the cow, but there's something that's harmful for monks.
This is particularly for monks, so it doesn't really apply to all Buddhists, but you could generalize.
Because some monks are greedy, and so they milk their followers for all their worth.
When they go in for alms and they're offered food, they take what you want while they take it off.
It's sort of a very specific thing, but I think it should be generalized.
I think the Buddha would want to generalize, generalize to being greedy.
Maybe being manipulative, let's say.
Not knowing your own, not knowing which nest, the difference between what you want and what you need.
Let's put it that way, monks need food to live, just like everybody else.
But there's a difference between what you need and what you want.
So, applying it to meditators, you know what you need.
That's quite different from what you want.
Sometimes you want good food or delicious food.
Sometimes you want very comfortable beds.
You want to go home and sleep on your nice bed.
The things we want in terms of living our life, the comforts and enjoyment.
That's the things that we need.
That's not really, I think, what he's getting here.
He's getting dry, milking dry refers to specifically refers to taking advantage of people who support you.
Because it's a problem for religion, if a monk becomes unsustainable.
The thing about a monk's life, a monk is someone who is cared for by a community.
The only way a monk can survive is because they're surrounded by people who want them to do this.
You think, yes, this is a good thing.
When in fact you think, well, why in the world would other people want to let this person get away with not having to work for their food,
not having to take care of themselves in so many ways?
It's a good question because the only way it could happen if that person was somehow useful.
You could argue that some monks are just useful to themselves, which I think is fair.
I'm willing, I'm happy to support a monk who's truly useful to themselves.
I'm happy to support all of you and you're not helping me in any way.
You're supporting all of you and monks are like, you support the monks.
I would support anyone to become a monk, provided, provided they were doing good for themselves.
That means all of you coming to meditate.
You're doing good for yourself. You're not doing good for me.
Good for you and we support that.
But of course, over the long-term monks are far more useful in other ways because they teach.
They help other people to practice meditation.
Even if they just memorize the Buddhist teaching, they're great resource.
They're able to teach others.
It's an important relationship and to take advantage of that.
To let it become an obsession where you're always taking and taking.
There's not something you meditators have to worry about.
There was a time where we were trying to fulfill all the meditators' desires.
If you need anything right down here, you need that, oh boy, the list started getting longer and longer and longer.
I think that's a good example.
It's problematic because it makes the system break down.
It got hard for us.
The food bills were very high and so on.
That's an example of extending this.
It's not exact.
It's not that problematic.
What's really problematic is when a monk does this sort of thing because people respect them
and the religious leaders, you know, you want to support them.
But wow, if they need all these things, I'm very well taken care of.
Everyone gets me whatever I want.
So I have to be so quite careful to remind myself of,
once in needs, do I need that or do I just want that?
That's I guess number 10 because now we're at the last one.
The last one is, some cow herds don't respect the old cows.
The bulls, the ones who are the head of the herd.
I think that's important probably because they could gore you
and if you don't know which one's the head bull,
you better watch out because they might kill you.
That's the one that you got to watch out for.
And in the same way practitioners, particularly monks, are not respectful
for to their elders.
It's a big thing in Buddhism.
It's not, this isn't a very deep teaching, but it's an important
teaching for practical purposes.
Because it's too easy to get caught up in your own idea of
worth.
This person is a good person.
This person is not a good person and become very critical.
This is how the newcomers to Buddhism as monks, it's a big deal.
Where a new monk will almost always think they're better than the old monks.
So one thing I noticed in Thailand as he watched these monks come through
and most of them don't make it past a few months.
Most of them never intended to make it past a few months.
But some of them do.
And you notice that the new monks are just trying to figure out how to be a monk.
But after about a year, you notice that there's a sense that,
wow, I wasn't a very good monk before.
But now after a year, I'm a really good monk.
So I really got this down and I'm really good at it.
And then as time goes by maybe after two years or so,
they start to realize, boy, I wasn't a very good monk before,
but now I'm a pretty good monk after a couple years or a few years.
And then as time goes on, they start to realize,
boy, I wasn't a very good monk back then, but now I'm an okay monk.
And as time goes on, you have a steam of yourself.
You start, you realize again and again that you're better than you were before,
except that state of better is less good than it was.
It means you have less of a high opinion of yourself.
And a parallel with meditators is after a meditation course.
Sometimes you have your own such a high that you feel, boy,
I am, and now I'm enlightened, that was it.
I'm just, I did it.
When you go home and you try to teach everybody about everybody else
and then you realize that you're really not all you thought you were.
The benefits you got were not nearly as profound or,
no, we're significant, but they didn't mean that you have no more defilements than.
So how that relates to this is, we have this social order of paying respect to our elders.
It's a monastic thing, really.
Because we don't want, and it's important,
we shouldn't just take this as paying lip, something we should pay lip service to.
We respect people who have been there a long time because of their knowledge,
because of their experience.
And because I might be wrong when I think this person is good, that person is good.
If we just esteem people based on our own understanding of who is a good meditator,
a good monk, then everyone does that, and then it's chaos.
The only way you can have order and really a potential for harmony is to everybody pay respect in the same order.
The sense of trying to promote people who've been there a longer period of time also to prop them up and to be clear that,
if you've been here for a long time, you better shape up, you better keep up with the new people.
Anyway, that's just probably more of a monastic thing.
But there you go, that's this suit.
I think there's a lot of useful stuff in there.
It's another good way of describing the monastic life in particular,
but in general, the meditative life.
That's what I hope that was helpful.
That's the demo for tonight.
Thank you all for tuning in.
