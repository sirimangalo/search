1
00:00:00,000 --> 00:00:29,940
Good evening, everyone, broadcasting my merch vivs, today's quote, actually quite an interesting

2
00:00:29,940 --> 00:00:39,240
little passage, when the Buddha was staying in a cup of water in the park of the

3
00:00:39,240 --> 00:00:54,320
banyan tree, so I think that's being grown around me, and grown around me, which was

4
00:00:54,320 --> 00:01:04,620
built by his family, his mother and his father, my family's got together and built, and

5
00:01:04,620 --> 00:01:12,820
grown down.

6
00:01:12,820 --> 00:01:23,980
Mahanama, Mahanama, there are two Mahanamas that people, one of them was actually the

7
00:01:23,980 --> 00:01:33,920
first find disciples of the Buddha, but that's not this one.

8
00:01:33,920 --> 00:01:42,660
Mahanama is actually interesting, interesting word, it means great name, kind of like a

9
00:01:42,660 --> 00:01:59,700
well-known person, so Mahanama was the name they gave to the princess, but this Mahanama

10
00:01:59,700 --> 00:02:16,260
was like he was Anurudha's brother, because it happened that when Anurudha became a monk,

11
00:02:16,260 --> 00:02:23,700
he did so after a discussion with his brother Mahanama, Mahanama said to Anurudha, you know,

12
00:02:23,700 --> 00:02:30,460
all these other sakins are going forth and becoming monks, so one of us, but no one from

13
00:02:30,460 --> 00:02:37,900
our family has become a monk yet, so either you become a monk or I become a monk, one

14
00:02:37,900 --> 00:02:44,340
of us has too, because our family is unrepresented, and everyone is going to say, oh, look

15
00:02:44,340 --> 00:02:51,500
at these, this family is too lazy to become monks, and then following after the tradition

16
00:02:51,500 --> 00:03:05,820
of Siddhartha, so Anurudha said, well, gee, becoming a monk, that sounds quite tough,

17
00:03:05,820 --> 00:03:13,460
you know, I've heard about it, I've heard what that's like, and I live under trees and

18
00:03:13,460 --> 00:03:21,020
stuff, that sounds difficult to me, you become a monk and I'll stay and I'll become

19
00:03:21,020 --> 00:03:26,140
a lay, I'll be a lay person, I'll stay as a lay person with the lay life, it's got to be

20
00:03:26,140 --> 00:03:28,180
easier, right?

21
00:03:28,180 --> 00:03:36,420
Mahanama says very well, and he starts to explain to Anurudha how all the things that

22
00:03:36,420 --> 00:03:39,740
he'd have to do, so without have to show you how to be a lay person, and tell you to

23
00:03:39,740 --> 00:03:46,260
be a lay person, how to run the affairs of our household, tells them all about the farms

24
00:03:46,260 --> 00:03:50,500
that they own, and that they have to care for, and the workers that have to be paid,

25
00:03:50,500 --> 00:03:59,020
and crops that have to come in, or the merchants that have to be dealt with, or the work

26
00:03:59,020 --> 00:04:12,380
that has to be done to cultivate and reap harvest and prepare, and care for the crops

27
00:04:12,380 --> 00:04:20,420
and animals and people, and he goes on and on, and Anurudha says, stop, stop, stop.

28
00:04:20,420 --> 00:04:26,740
This is what it takes to be, this is what it takes to become, to be a lay person, so it

29
00:04:26,740 --> 00:04:31,820
takes to live in the world, there's no way being a monk could be harder than that, he said,

30
00:04:31,820 --> 00:04:37,460
I'll become a monk, I've changed my life, I'll become a monk and you stay as a lay person,

31
00:04:37,460 --> 00:04:49,420
that sounds like hell to me, and some Mahanama almost became a monk, but because he was

32
00:04:49,420 --> 00:04:56,740
kind, he let his younger brother become a monk and stand, and he stayed to run their

33
00:04:56,740 --> 00:05:09,980
household, run their family business, but we get a lot of these talks between him and

34
00:05:09,980 --> 00:05:18,220
the Buddha, seems the Buddha had a special place in his heart, or he even kept a special

35
00:05:18,220 --> 00:05:26,060
place for Mahanama, or Mahanama had a special place for the Buddha, and in the sense of

36
00:05:26,060 --> 00:05:31,740
having a special interest, and coming to see the Buddha, because he wasn't able to

37
00:05:31,740 --> 00:05:38,780
ordain, so he wanted to get teachings whenever he could, and so this is one such

38
00:05:38,780 --> 00:05:55,860
teaching, and he's sort of the iconic, or the exemplary lay person, and as a result

39
00:05:55,860 --> 00:06:02,300
of his backstory, he makes the perfect foil for the Buddha to talk about how lay people

40
00:06:02,300 --> 00:06:07,740
should live, so his talks are always quite interesting for lay people, I think, and

41
00:06:07,740 --> 00:06:11,980
so whereas many of the talks we say, well this is more directed towards monks, and so

42
00:06:11,980 --> 00:06:16,260
you have to adapt it if you want to practice it as a lay person, this one is the other

43
00:06:16,260 --> 00:06:22,020
way, it's much more relatable to lay people, so his first question is how does one become

44
00:06:22,020 --> 00:06:28,580
a lay disciple, and so here we have the Buddha's words, if you were wondering whether

45
00:06:28,580 --> 00:06:39,060
there was any text-based method of becoming a Buddhist, or a lay Buddhist, but this is

46
00:06:39,060 --> 00:06:47,700
when one has taken refuge in the Buddha, the Dhamma and the Sangha, that's all it takes,

47
00:06:47,700 --> 00:06:55,860
then what is a lay disciple, one has made a determination that the Buddha is my refuge,

48
00:06:55,860 --> 00:07:04,060
this teaching is my refuge, and the teachers who pass on his teaching there, my refuge makes

49
00:07:04,060 --> 00:07:11,020
that determination, one is then a lay Buddhist, and it's all it takes, and so then he goes

50
00:07:11,020 --> 00:07:19,020
on to say, well how is one, how is it lay disciple of virtuous, just because your lay

51
00:07:19,020 --> 00:07:25,780
person doesn't actually mean you're any good at anything, and so after taking refuge

52
00:07:25,780 --> 00:07:32,380
the next step is how to be a virtuous, how to actually be a good person, a good Buddhist,

53
00:07:32,380 --> 00:07:37,860
which is the Buddhist, but a good Buddhist, and so he says it gives the five precepts,

54
00:07:37,860 --> 00:07:43,060
and these two together really make up what it means to be a good Buddhist, if you're

55
00:07:43,060 --> 00:07:50,420
breaking the five precepts, could say you're still Buddhist, it's not a very good one,

56
00:07:50,420 --> 00:07:56,180
you don't take refuge in the three refuges, Buddha, the Dhamma and Sangha, and you're

57
00:07:56,180 --> 00:08:02,780
not a Buddhist at all, but these together make you not only a Buddhist, but a good Buddhist,

58
00:08:02,780 --> 00:08:07,940
and you take the three refuges, your Buddhist, when you keep the five precepts, you're

59
00:08:07,940 --> 00:08:14,540
a good Buddhist, you're virtuous, and then two more questions which are probably more interesting

60
00:08:14,540 --> 00:08:23,940
is how do you help yourself and not help others, and how do you help yourself and others?

61
00:08:23,940 --> 00:08:30,260
They're interesting, especially because they talk about lay people teaching, lay people

62
00:08:30,260 --> 00:08:38,580
encouraging others, passing the teaching on to others, so not only having faith, virtue

63
00:08:38,580 --> 00:08:59,420
or enunciation, and desire to hear the Dhamma, desire to practice the Dhamma, and these cultivating

64
00:08:59,420 --> 00:09:09,180
these things in oneself, that's helping oneself.

65
00:09:09,180 --> 00:09:18,900
But the idea behind this, the last, the last paragraph, the last question, how one helps

66
00:09:18,900 --> 00:09:28,340
others also, is that there's more than just helping yourself, you should strive to

67
00:09:28,340 --> 00:09:31,700
establish such things in others.

68
00:09:31,700 --> 00:09:39,420
I mean, shouldn't, shouldn't, it's one, you see, it's curious how it avoids such words

69
00:09:39,420 --> 00:09:46,260
as shouldn't, don't just help yourself and help others as well, but that's important

70
00:09:46,260 --> 00:09:52,460
because not everyone is a teacher, and not everyone is in a position to teach brothers,

71
00:09:52,460 --> 00:10:00,220
it's not about going out and finding students and pushing to be a teacher or wanting

72
00:10:00,220 --> 00:10:06,140
to be a teacher and wanting to, wanting other people to understand the Dhamma, it's about

73
00:10:06,140 --> 00:10:16,420
the kindness of it, it's about the compassion, and to just cut people off and to not

74
00:10:16,420 --> 00:10:22,780
share good things, it's like not sharing your possessions, not sharing your treasure with

75
00:10:22,780 --> 00:10:32,260
your family, with your friend, with your children, to keep it all to yourself, so keeping

76
00:10:32,260 --> 00:10:43,820
in great treasure to yourself, you have these things for yourself, it's a question of whether

77
00:10:43,820 --> 00:10:49,780
you share them with others, now there may not be the opportunity in it, so you may have

78
00:10:49,780 --> 00:10:53,900
to keep the treasures to yourself, and the people might not want your treasures, they might

79
00:10:53,900 --> 00:11:07,100
not see the most treasure, or there may be no one who is able to share the treasure with

80
00:11:07,100 --> 00:11:20,740
them, but we try and help in it, you see other people suffering, often we can't do

81
00:11:20,740 --> 00:11:27,780
them, so we need to be afraid, and this is a treasure, this is a good thing, every moment

82
00:11:27,780 --> 00:11:34,500
that we're mindful, every moment that a person's mindful, it's a clear thought, a clear

83
00:11:34,500 --> 00:11:41,300
moment, that's a good thing, if you can explain that to someone, if you can bring someone

84
00:11:41,300 --> 00:11:50,180
else to have a moment of clear thought, that's a good thing, you help them, not only help

85
00:11:50,180 --> 00:11:57,740
yourself, but you help others, there should never be afraid to help, it would be afraid

86
00:11:57,740 --> 00:12:04,780
to share the teaching, it doesn't take a lot, sometimes all it takes is showing people

87
00:12:04,780 --> 00:12:14,900
how to do, sending meditation, how to say to themselves, rise and focus their mind, sometimes

88
00:12:14,900 --> 00:12:22,380
it doesn't even take that, you can just pass a book along, and pass a link along, send

89
00:12:22,380 --> 00:12:35,300
people in the right direction, and you help others, so that's what this is all about, helping

90
00:12:35,300 --> 00:12:44,340
ourselves, helping others, so this group is all about, really appreciate all of you coming

91
00:12:44,340 --> 00:12:54,900
together, all of us, coming together as a community, it's great to have this online group,

92
00:12:54,900 --> 00:13:00,060
it gives me encouragement that I can see, it gives other people encouragement as well,

93
00:13:00,060 --> 00:13:07,700
I shouldn't take these things lightly, I should be proud of ourselves, not proud like that,

94
00:13:07,700 --> 00:13:16,380
but I should feel happy, I should rejoice in the goodness of coming together in this way,

95
00:13:16,380 --> 00:13:25,780
the goodness that we develop as a community and through just being here for each other,

96
00:13:25,780 --> 00:13:35,220
so may this community live long, may we help or help, maybe find help for ourselves,

97
00:13:35,220 --> 00:13:43,700
may we bring help to each other, through this community, and through our practice, so

98
00:13:43,700 --> 00:14:13,540
and thank you, have a good night.

