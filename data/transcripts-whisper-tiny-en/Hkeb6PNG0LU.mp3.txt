So, tonight's question is about fun.
I don't think it's really about fun, but my answer is going to be about fun.
So, the person asks whether they're practicing correctly, they say they've been doing this
for some time, noting, using the technique, but they're not sure how to know whether
they're doing it right because it's not fun.
So, I thought I would expand this answer to discuss the topic of fun, it won't take
a long time hopefully, but just go over some of the issues around meditation and fun.
So, the reason why your meditation might not be fun, I can think of three reasons.
That's what I'll talk about.
First reason why your meditation might not be fun is because you're doing it wrong.
It's quite easy to do, to practice incorrectly, especially if you don't have a teacher.
If you're just going by my booklet on how to meditate, it's quite possible that you're
practicing wrong.
And by wrong, I mean getting something wrong, you're, you haven't quite understood perhaps
how to do the noting and so you're trying to make the things go away, sometimes you have
extreme problems to the extent that someone rather than when they have pain saying to
themselves pain, pain, that repeat to themselves, no pain, no pain, or if they're afraid
they'll say calm, calm, because the point, their misunderstanding is that they should repeat
what they want to happen, a less extreme, but still wrong perspective is to say to
yourself, for example, pain and want the pain to go away, or to say thinking, thinking
and be frustrated when it comes back, and without any input from a teacher, a feedback
from a teacher, it can be quite discouraging because you have a perspective, an attitude
of trying to make, control things and make things change based on your whim, which really
is the whole point of the meditation to see that that's not possible.
And practicing wrongly can make the meditation painful, unpleasant, especially if you have
a wrong perspective, if you have a wrong attitude, a wrong outlook.
I think I could suggest that wanting your meditation to be fun, I don't really think what
this person was saying is they wanted it to be fun, but it's, fun is not really inherently
different from any other kind of pleasure seeking.
We call things fun, we seek out fun as a part of our pleasure seeking, it's really just
another word for a type of pleasure seeking, similar to eating or having sex or listening
to music, dancing we say is fun, but deep down what we really mean, what we're really
responding to is the pleasure involved.
So the idea that your meditation should be pleasurable is wrong and has problematic consequences
because you'll be frustrated when the things that you find pleasurable are mixed with the
things you find un-displeasurable or displeasing.
The second reason why your practice might not be fun is because you're not good at it.
And really being not good at meditation is essentially doing it wrong, but it's a different
kind of doing it wrong, I think, because you say someone's doing it wrong when there's
something fundamental about their technique, about their perspective, about their approach,
that is problematic, that can be easily corrected through explanation, through advice,
not even exactly advice, but just pointing it out when you're doing that wrong, like
take weightlifting, for example, you might try to lift too much, or you might try to lift
the wrong way, and you might hurt yourself as a result if you're training in sports and
on golfing, it's one thing to not be good at golfing, but it's another to maybe hold
to stick the wrong way, the club the wrong way, or use the wrong club, right?
Just doing it wrong, if you're trying to hit it very far and you use the putter, you're
doing it wrong, and meditation is similar, similar problems, of course.
But not being good at something means you have an understanding of the theory, which hopefully
you do if you've read the booklet and we've gone over it and reassured ourselves together
that you're doing it, you understand the theory, that shouldn't be something you should
worry about, because there's no deep theory that you have to understand, and I think
you have to make a break as I'll make a line, because you'll drive yourself crazy if
you think, oh, is there some theory that I don't know, am I doing it wrong, it's a common
common question, and you have to separate, no, you're not doing it wrong per se, you're
doing it wrong in the sense that you're not good at it, and it's important to understand
that that's a part, that's true about meditation, that you have to get good at it, it
is a training, it's not a switch that you turn on, it's not just a time out where, okay,
all that craziness in my life, I'm going to put aside and do nothing, and poofing
and be peaceful.
It is in fact, like that, meditation is in the end to let go of all the things that
cause the suffering, stop doing the things that cause the suffering, but your mind's not
going to let you do that, because we habitually seek out suffering, not intentionally,
we think it's going to make us happy, but it doesn't, and so because of those habits
having been built up, we're in a real bind, we try to sit still, and we're not going
to sit still, we try to focus and we're not going to focus, we try to just observe something
and our mind's got a whole other idea.
So not being good at it, when you say to yourself, pain, pain, but your mind is maybe angry
about the pain, or you try to say it, and then you're distracted by something else.
You try to focus on the stomach, but your mind is distracted, you try to sit still, but
you rest this or agitated, or you're falling asleep.
There's much about the imbalance of the faculties, right, just effort and concentration
or very common, there's not much you can do about that, except get good, as you practice
more, you're going to get better at it, and part of that, it's not something you can push
or pull, it's just through repetition and application and diligence, and become more energetic
and less distracted, so concentration and energy will start to work together rather than
fight each other, or sometimes you're restless, sometimes you're falling asleep.
So it's important to understand that, that you can be doing everything right, and it's
just can still be a horrible experience, because you're not very good at it, and that's like
any training, you're playing golf, it's a horrible experience in the beginning, but
meditation is just like that, when you finally hit the ball and goes up to that grass,
when you finally, just for a moment, you know, it was really mindful there, and it totally
didn't bother me, as you realize you're starting to get good at it.
And the third reason why meditation might not be fun is because the goal of meditation
is not to have fun, fun is a, as I said already, an inferior state, fun is not the goal
nor should it be the goal of meditation or one's life, having fun is just another means
of seeking pleasure.
Consider anything we call fun or we think of as fun.
How long can you engage in that activity before it becomes boring, right?
Because of course the flip side of something being fun is something being boring, meditation
is very boring, but it's only boring because you see it that way, and the only reason
you see it that way is because you stuck to certain things as being fun, and you're addicted
to those things.
This is the addiction cycle, fun does not escape and cannot escape the addiction cycle,
and the addiction cycle cannot escape suffering, it's possible to engage in something without
suffering for a time, but the build up, the lead up, the result is a susceptibility to
suffering.
There's no question, you might say, I don't suffer, I have fun and I don't suffer.
First of all, you're deluding yourself most likely because after time we don't realize
how stressful and unpleasant our lives are, we try to forget, we've cultivated the ability
to ignore and forget how much suffering we have.
The second of all, it's not even so much that you do suffer, it's that you're caught
up in suffering, you're vulnerable to it.
What happens if you get sick, you're a sports, you're a sports person, an athlete, and
then you get sick, and you get an illness, or you do rock climbing, and I was really quite,
I was good at it.
But then I got tendonitis in my elbow, tendonitis in my fingers, and I can no longer do it.
And that wasn't even the big deal, but you can, people can like tiger woods, they hurt
himself very bad, I think.
I only know that because his mother's Buddhist, so here's things about him.
You can't escape the potential for suffering, man, what about getting old and sick and dying,
how are you going to be when you die?
Your state of mind predicts your rebirth, predicts your future, predicts your peace of
mind that when you die, and because of the addiction cycle we're constantly seeking.
When you die, you'll see these bright lights, and chase after them, and oh, you'll be
reborn.
It's funny how other religions talk about the bright light, and I saw the light, and that
was a sign of heaven, well, maybe.
We have other stories where people who chase after those lights end up, end up in hell
and suffering.
Meditation isn't supposed to be for the purpose of fun, and it isn't even for the purpose
of pleasure.
We have to understand that it's kind of a paradox, where the end goal, of course, is peace,
but it's not the peace that you enjoy, and really the whole idea of enjoying things has
to be done away with, and that's the profound and radical aspect of Buddhism, because it seems
horrific that you should even suggest to get rid of enjoying things, but the whole concept
and system of enjoyment is wrong-minded, wrong-headed, wrong-intentioned.
Happiness doesn't work that way.
Enjoying involves liking, it involves wanting, it involves needing, it involves addiction,
and so it never frees you from the vulnerability of suffering.
Happiness, peace, have to be independent of things, events, people, places, things, and
events, experiences.
If your happiness is dependent on sitting there peaceful and calm, you'll never be peaceful
and calm.
It's a paradox, sort of, or whatever it is, it's ironic, no, it's something.
It's unfortunate, it would be nice that I can have want to be peaceful and just be peaceful,
but one thing of course disturbs your peace, catch 22, maybe I don't know, it's a problem,
and so our focus can never be on happiness, I've talked about this before.
You can never focus on peace and happiness.
Your focus has to be on goodness, and really by goodness in this deep meditative sense,
it means a good state of mind, purity.
So when we say to ourselves, pain, pain, or even just rising, falling, thinking, thinking,
we're trying to cultivate what we consider to be the purest state of mind, the purest
state of mind, the purest state of mind, a risen state of mind is the one that knows an
object just as it is without judgment, without reaction.
It is what it is, pain is what?
Not good, not bad, not mean, not mind, not a problem, not something to fix.
Pain is pain, rising is rising, it's a training that we're doing, why are we focusing
on the stomach and the feet, there's nothing special about them.
But there's nothing, there shouldn't be dismissed either, there's nothing inferior about
them, this is an experience moving the foot, this is an experience.
Our focus is not on having fun, absolutely, but it's also not even on peace and happiness,
and it never can be, our focus always has to be on goodness and purity, and that shouldn't
be so bad, I mean, that shouldn't sound so bad, imagine having a perfectly pure mind,
the only result could ever be peace, happiness, that's an important distinction because
we'd go the other way, right, who in this world thinks of, oh, well, I shouldn't put
us down too much, but we're in society, we're in modern society, are we taught about
goodness, I mean, I think in religion we often are, and it's not the same, many people
do have ethics and ideas of helping others and so on, but what are we told, what is much
more prevalent and in front of us, seek out pleasure, seek out happiness, do what makes
you happy and so on, and so our focus is on the result, but the only thing that brings
that result truly and sustainably brings it is goodness, is the purity of life, so I don't
think this person was, it wasn't literally saying it should be having fun like playing
a game, but they certainly did seem to be discouraged, and many people are discouraged,
I don't think people think of meditation as a game very much, if you got this far you're
not here to play a game and have fun, but we say something is not fun, and by that we mean
it's not pleasurable, which really is the same thing, again, it's just another fun, it's
just another type of pleasure, so don't ever try and find peace and happiness and meditation,
try and find purity and goodness and clarity of mind, strength as well of mind, because
happiness is a result that's not a practice, it's not the cause, happiness is not the cause
of happiness, right, goodness is, so some thoughts on fun and meditation, leave it to the
buddhist to take the fun out of everything, that should be our motto, we take the fun out
of everything, because it's not good for you, it's in fear here, so that's the answer
to that, thank you for listening.
