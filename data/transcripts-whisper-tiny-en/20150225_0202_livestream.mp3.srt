1
00:00:00,000 --> 00:00:19,000
Okay, back broadcasting this evening from Stony Creek, Ontario, Canada, back in the cold,

2
00:00:19,000 --> 00:00:41,000
right whiteboard.

3
00:00:41,000 --> 00:00:45,000
It's different here.

4
00:00:45,000 --> 00:00:52,000
There are many things different in the West.

5
00:00:52,000 --> 00:01:00,000
Sometimes it feels like fitting, trying to fit around pangs and square holes,

6
00:01:00,000 --> 00:01:14,000
the square pangs and round holes, with the monastic life, with the meditation practice.

7
00:01:14,000 --> 00:01:18,000
With the philosophy of the Buddha.

8
00:01:18,000 --> 00:01:32,000
So of course, constantly much easier in the dedicated Buddhist culture.

9
00:01:32,000 --> 00:01:45,000
But the key thing is to pair the Buddha's teaching down to the essentials.

10
00:01:45,000 --> 00:01:49,000
And then we build it from there.

11
00:01:49,000 --> 00:02:05,000
So I never remove anything from Buddha's teaching, but it's not paired down, but to begin with the essentials.

12
00:02:05,000 --> 00:02:11,000
As long as you're not altering them the rest.

13
00:02:11,000 --> 00:02:25,000
Some extent can be adapted, or it's not important as essential or concerned.

14
00:02:25,000 --> 00:02:36,000
So when you look at the Buddha's teaching in its textual form, it seems like it's quite stuck in very specific society.

15
00:02:36,000 --> 00:02:40,000
And of course, many of the Buddha's teachings were given in context.

16
00:02:40,000 --> 00:02:49,000
So if you try to apply everything all at once and try to just import Buddhism into your life.

17
00:02:49,000 --> 00:02:55,000
You have not been a very weird situation or awkward situation,

18
00:02:55,000 --> 00:03:02,000
but much of it doesn't immediately fit with your life.

19
00:03:02,000 --> 00:03:12,000
On the other hand, you figure out what is the essence that you can start there and can slowly apply the rest of the day.

20
00:03:12,000 --> 00:03:17,000
Add on as you go the rest of the teachings.

21
00:03:17,000 --> 00:03:27,000
So to do that, of course, it's necessary that we find the essence of the Buddha's teaching.

22
00:03:27,000 --> 00:03:38,000
The teravada that is fairly simple, and by teravada, in the commentary of tradition,

23
00:03:38,000 --> 00:03:49,000
that grew up surrounding all the canon that we found it to be together.

24
00:03:49,000 --> 00:03:55,000
It's simple because the commentaries are quite clear about what is the essence of the Buddha himself

25
00:03:55,000 --> 00:04:05,000
is quite clear, but the commentaries sort of spell it out for us in one term,

26
00:04:05,000 --> 00:04:19,000
pointing out what the Buddha emphasized or showed to be the core.

27
00:04:19,000 --> 00:04:25,000
So we have this quote in the commentary somewhere.

28
00:04:25,000 --> 00:04:53,000
When you pare down the Buddha's teaching, the words of the Buddha has found in the three baskets for the three sections,

29
00:04:53,000 --> 00:05:16,000
and when you break it down, it boils down to the path of a Bhamada.

30
00:05:16,000 --> 00:05:31,000
This is how the commentaries phrase it and they must be thinking specifically of the Buddha's last words.

31
00:05:31,000 --> 00:05:53,000
When the Buddha send a Bhamada in a sampadita, which is an injunction at the end means it's an order, a command, or an injunction,

32
00:05:53,000 --> 00:06:18,000
bring to fulfilment or come to fulfilment with a Bhamada, sampadita means come to fulfilment or reach the goal.

33
00:06:18,000 --> 00:06:31,000
How do you reach the goal, how do you come to fulfilment of Bhamada with a Bhamada?

34
00:06:31,000 --> 00:06:44,000
It's a difficult word to translate, not to understand, but to translate, we don't really have a good example.

35
00:06:44,000 --> 00:06:55,000
Equivalent in English, I think it's quite satisfying because it's a negative, a mada, a means, it's a negative.

36
00:06:55,000 --> 00:07:22,000
Bhamada means negligence or heatlessness, so maybe in the opposite would be vigilance or non-negligence if you want to stay literal.

37
00:07:22,000 --> 00:07:43,000
So the meaning is to not get lost, to not be lazy or heatless and that our minds wonder from reality or from truth.

38
00:07:43,000 --> 00:07:58,000
It means to keep our minds focused on reality or the present moment.

39
00:07:58,000 --> 00:08:03,000
Don't understand the word and it's meaning if you look in the context of the five precepts.

40
00:08:03,000 --> 00:08:32,000
Bhamada says about the fifth precept, he gives an injunction that we should encourage us to take the rule not to take alcohol or strong drink, any kind of spirits or liquor or any kind of

41
00:08:32,000 --> 00:08:35,000
intoxicating beverages.

42
00:08:35,000 --> 00:08:50,000
And the word for intoxicating is the mada time that our base or a cause for Bhamada.

43
00:08:50,000 --> 00:09:07,000
So what alcohol does to you is a kind of Bhamada, it makes you negligent, it makes you lose your, it makes you less alert.

44
00:09:07,000 --> 00:09:14,000
It makes you less mindful.

45
00:09:14,000 --> 00:09:21,000
And that's really the key of Bhamada is the Buddha sending, mindfulness is really the key.

46
00:09:21,000 --> 00:09:33,000
So in brief, we send Satyaya, we pull us, we pull us up a mountain to do to do.

47
00:09:33,000 --> 00:09:41,000
One one is never without mindfulness, never lacking in mindfulness.

48
00:09:41,000 --> 00:09:45,000
So it is meant by a Bhamada.

49
00:09:45,000 --> 00:09:47,000
So what does it mean to not be negligent?

50
00:09:47,000 --> 00:09:50,000
It means to be always mindful.

51
00:09:50,000 --> 00:10:01,000
So to always have Satyaya, mindfulness is again exactly a good transfer, perfect translation.

52
00:10:01,000 --> 00:10:06,000
Satyaya, we pull us off the mountain to do it.

53
00:10:06,000 --> 00:10:14,000
We try to have mindfulness all the time, maybe a mindfulness you consider to be a Bhamada.

54
00:10:14,000 --> 00:10:19,000
Now this is all just speech if you want to really understand a Bhamada.

55
00:10:19,000 --> 00:10:22,000
You don't have to understand it intellectually.

56
00:10:22,000 --> 00:10:28,000
If you try practicing Satyaya, you can see this, see what it is with meaning.

57
00:10:28,000 --> 00:10:33,000
Because when you practice Satyaya, you can see that you have some different quality of mind.

58
00:10:33,000 --> 00:10:47,000
There is an alert quality, a clarity of mind, an objectivity, a freshness to the mind.

59
00:10:47,000 --> 00:10:54,000
It is missing from an ordinary, delightful state.

60
00:10:54,000 --> 00:10:59,000
So it is this state that the Buddha is referring to, this is really it.

61
00:10:59,000 --> 00:11:12,000
You can cultivate that state and develop the quality of mind that support that state.

62
00:11:12,000 --> 00:11:22,000
If you can keep with that, this path is the essence of Buddhism.

63
00:11:22,000 --> 00:11:29,000
Now there is one more teaching that is interesting and it is where the Buddha said it,

64
00:11:29,000 --> 00:11:32,000
explains a Bhamada in detail.

65
00:11:32,000 --> 00:11:38,000
Or he did not explain it, but he explains how to practice a Bhamada.

66
00:11:38,000 --> 00:11:45,000
What it means to be a Bhamada, to be vigilant, to be not negligent.

67
00:11:45,000 --> 00:12:06,000
And so he gives four qualities, four qualities that we do to be not, to be a Bhamada.

68
00:12:06,000 --> 00:12:16,000
They are kind of interesting, they are quite simple.

69
00:12:16,000 --> 00:12:25,000
I may have been known to never be, to have no ill will, or anger, you can say.

70
00:12:25,000 --> 00:12:38,000
I may have been known to be always mindful, so I know that one.

71
00:12:38,000 --> 00:12:46,000
Anja tang sussumahito, to be internally well composed.

72
00:12:46,000 --> 00:12:54,000
Anja tang sussumahito, sussumahito is related to the word samadhi.

73
00:12:54,000 --> 00:13:05,000
But samadhi don't mean level head-end, or equonomiscence, but it is balanced, so well balanced.

74
00:13:05,000 --> 00:13:12,000
Anja tang sussumahito, internally well composed.

75
00:13:12,000 --> 00:13:26,000
Number four, a bhinja vinayi sika, sika means training, vinayi, to be free from, or to lead

76
00:13:26,000 --> 00:13:37,000
oneself out, to undertake the training for leading oneself out.

77
00:13:37,000 --> 00:13:48,000
A bhinja, from greed, or desire, or craving.

78
00:13:48,000 --> 00:13:56,000
And this fourth one is kind of curious, so anyway, in order, a bhinomiscence is quite simple,

79
00:13:56,000 --> 00:14:01,000
the anger, the anger, and greed are here.

80
00:14:01,000 --> 00:14:09,000
When we start with anger, anger is sort of a, it's different from desire, because anger is painful,

81
00:14:09,000 --> 00:14:12,000
so it is a good one to start with.

82
00:14:12,000 --> 00:14:16,000
That's quite easy for people to see how anger is a bad thing.

83
00:14:16,000 --> 00:14:24,000
So it seems to have to agree for the most part that it is something we should do away with.

84
00:14:24,000 --> 00:14:32,000
It's easier anyway, because it's unpleasant when it likes being angry.

85
00:14:32,000 --> 00:14:37,000
You might for a second feel good about the fact that you're upset at someone,

86
00:14:37,000 --> 00:14:41,000
but it obviously is a foul, it's tasty, you know.

87
00:14:41,000 --> 00:14:46,000
It's painful to actually be angry.

88
00:14:46,000 --> 00:14:55,000
It should be upset, it's stressful, harmful.

89
00:14:55,000 --> 00:15:01,000
It's harmful up in the long term as well, something that is all around not good for us.

90
00:15:01,000 --> 00:15:06,000
It makes us bitter people, it loses friends and respect.

91
00:15:06,000 --> 00:15:14,000
It gains us friends with bad intentions, it gains us enemies.

92
00:15:14,000 --> 00:15:22,000
It hurts our health, and when we die, it makes us die with fear and anger and resentment.

93
00:15:22,000 --> 00:15:24,000
We need a bad way.

94
00:15:24,000 --> 00:15:31,000
So anywhere we go in the future, anything that might happen in the future life is tainted.

95
00:15:31,000 --> 00:15:33,000
It's an easy one to give up, they have to know.

96
00:15:33,000 --> 00:15:36,000
Sadasa-to means to always have sucked in.

97
00:15:36,000 --> 00:15:41,000
So it means to always be clear about the nature of our experience.

98
00:15:41,000 --> 00:15:44,000
There will be a little bit of that.

99
00:15:44,000 --> 00:15:47,000
If you have any misunderstanding about it,

100
00:15:47,000 --> 00:15:51,000
to never be at odds with it.

101
00:15:51,000 --> 00:15:57,000
You know, where when we stand and walk, when we sit, we lie down.

102
00:15:57,000 --> 00:16:01,000
When we see you when we hear, when we smell, taste, feel the thing.

103
00:16:01,000 --> 00:16:03,000
You know, where of it.

104
00:16:03,000 --> 00:16:08,000
And it is, not just the where of it, but being aware that it is what it is.

105
00:16:08,000 --> 00:16:12,000
You remind ourselves of seeing, hearing.

106
00:16:12,000 --> 00:16:19,000
If we use this word to remind ourselves of, creates this clarity in mind.

107
00:16:19,000 --> 00:16:25,000
It is a setting.

108
00:16:25,000 --> 00:16:33,000
A jadang susa-mahi-doh means to focus our minds and to compose our minds.

109
00:16:33,000 --> 00:16:40,000
When this comes through mindfulness, it is your mindful, your mind is going to sort itself out now.

110
00:16:40,000 --> 00:16:50,000
This may take days, weeks, months, years, depending on how intensive you practice.

111
00:16:50,000 --> 00:16:53,000
It will come.

112
00:16:53,000 --> 00:17:00,000
If you dedicate yourself to practice.

113
00:17:00,000 --> 00:17:05,000
Your mind becomes more orderly.

114
00:17:05,000 --> 00:17:15,000
You may still have likes and dislikes, or upset in the mind.

115
00:17:15,000 --> 00:17:17,000
Your mind becomes orderly.

116
00:17:17,000 --> 00:17:22,000
There is not so much likes and dislikes as your mind might still not be,

117
00:17:22,000 --> 00:17:31,000
might still be loud in the sense that thoughts come.

118
00:17:31,000 --> 00:17:36,000
Things still interfere, you still interact with the world around you.

119
00:17:36,000 --> 00:17:44,000
You still experience pain and pleasure, you still experience the world.

120
00:17:44,000 --> 00:17:53,000
But once you are well composed, become more objective and less effective.

121
00:17:53,000 --> 00:17:57,000
Jadang susa-mahi-doh internally, well composed.

122
00:17:57,000 --> 00:18:06,000
And then before this interesting, fourth one is interesting in that it actually points out with you.

123
00:18:06,000 --> 00:18:09,000
Craving is something that you have to train to overcome.

124
00:18:09,000 --> 00:18:12,000
It is not like anger where you can just decide to be a free permit.

125
00:18:12,000 --> 00:18:15,000
For the most part, you can set yourself on that path.

126
00:18:15,000 --> 00:18:21,000
Craving is something that takes a little more care because we don't really want to be free from it.

127
00:18:21,000 --> 00:18:32,000
It is scary for us to think of letting go and think that to some extent that craving is all that allows us to keep our happiness.

128
00:18:32,000 --> 00:18:35,000
The only way you can be happy is to want things.

129
00:18:35,000 --> 00:18:43,000
If I didn't like things anymore, how could I possibly have to lose my happiness?

130
00:18:43,000 --> 00:18:46,000
Actually, this liking thing is great.

131
00:18:46,000 --> 00:18:49,000
It allows me to experience pleasure.

132
00:18:49,000 --> 00:18:55,000
Liking can actually be quite pleasurable.

133
00:18:55,000 --> 00:19:00,000
But the curious thing is, is we find no matter how much liking or craving we have,

134
00:19:00,000 --> 00:19:05,000
we're never really happy, we're never really satisfied.

135
00:19:05,000 --> 00:19:07,000
We're never really at peace with ourselves.

136
00:19:07,000 --> 00:19:15,000
And in fact, we've become less and less at peace with life, with experience, with reality,

137
00:19:15,000 --> 00:19:18,000
and more liking and wanting we have.

138
00:19:18,000 --> 00:19:26,000
So, eventually we come to a crisis where we have to choose,

139
00:19:26,000 --> 00:19:32,000
well, although we have to do something about it, we have to fix the problem.

140
00:19:32,000 --> 00:19:35,000
This is where most people find themselves when they turn to meditation,

141
00:19:35,000 --> 00:19:47,000
as they can no longer satisfy their addictions, habits, and so they make a desperate attempt to change the world that they're all.

142
00:19:47,000 --> 00:19:52,000
And they begin to realize that the problem is the craving itself.

143
00:19:52,000 --> 00:19:56,000
Not that they're not the fact that the problem isn't that they're not getting what they want.

144
00:19:56,000 --> 00:19:58,000
That's unavoidable.

145
00:19:58,000 --> 00:20:08,000
You can't always get the problem is the one thing itself.

146
00:20:08,000 --> 00:20:12,000
That's where we find ourselves in the practice.

147
00:20:12,000 --> 00:20:15,000
Looking to train ourselves to be free from craving.

148
00:20:15,000 --> 00:20:22,000
It's a long potential, a long path.

149
00:20:22,000 --> 00:20:26,000
Something you have to be patient with, but persistent.

150
00:20:26,000 --> 00:20:31,000
Just because it's difficult and lengthy.

151
00:20:31,000 --> 00:20:34,000
I mean, you can slack off.

152
00:20:34,000 --> 00:20:43,000
So it should always try to forge on.

153
00:20:43,000 --> 00:20:46,000
This is why communities are so helpful.

154
00:20:46,000 --> 00:20:50,000
Friends are so useful to keep in touch with other meditators.

155
00:20:50,000 --> 00:20:51,000
It's a great thing.

156
00:20:51,000 --> 00:20:54,000
We encourage ourselves in the practice.

157
00:20:54,000 --> 00:20:59,000
So it's always nice to see people coming together here on the internet to practice,

158
00:20:59,000 --> 00:21:02,000
and I'm happy to encourage.

159
00:21:02,000 --> 00:21:12,000
I will be trying to give more online teachings now that I'm back.

160
00:21:12,000 --> 00:21:16,000
There's always I'm back here in Canada.

161
00:21:16,000 --> 00:21:20,000
Maybe not every night, but I'm trying to see.

162
00:21:20,000 --> 00:21:24,000
I don't know how vocal meditators think about as well.

163
00:21:24,000 --> 00:21:27,000
So maybe you can combine with you.

164
00:21:27,000 --> 00:21:32,000
Probably I should have been downstairs today giving this talk to you about my one meditator,

165
00:21:32,000 --> 00:21:37,000
but there's a try again tomorrow maybe.

166
00:21:37,000 --> 00:21:42,000
Anyway, that's the demo for today, the path of pamanda.

167
00:21:42,000 --> 00:21:45,000
It's four demos.

168
00:21:45,000 --> 00:21:50,000
Just something for us to think about reminding ourselves if it's important.

169
00:21:50,000 --> 00:22:14,000
So thank you all for tuning in and keep practicing.

