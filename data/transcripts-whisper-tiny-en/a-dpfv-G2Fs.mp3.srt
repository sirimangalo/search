1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone. Welcome to our daily dumber.

2
00:00:07,000 --> 00:00:33,000
When we try to define or explain Buddhism,

3
00:00:38,000 --> 00:00:44,000
There are lots of ways we can try to sum up Buddhism.

4
00:00:52,000 --> 00:01:01,000
We can say that Buddhism is all about suffering. That's a common one, right?

5
00:01:01,000 --> 00:01:08,000
We're here about the four noble truths.

6
00:01:08,000 --> 00:01:11,000
We think.

7
00:01:11,000 --> 00:01:17,000
We want to defining future Buddhism at suffering.

8
00:01:17,000 --> 00:01:21,000
Or maybe we think of the goal of Buddhism.

9
00:01:21,000 --> 00:01:25,000
We say Buddhism is all about freedom,

10
00:01:25,000 --> 00:01:29,000
liberation, or more specifically Nirvana.

11
00:01:29,000 --> 00:01:38,000
What is Buddhism about? It's about escaping some sorrow.

12
00:01:38,000 --> 00:01:44,000
Or we might look at the quality of Buddhism.

13
00:01:44,000 --> 00:01:51,000
See how Buddhism is different from faith-based religions and say Buddhism is all about wisdom.

14
00:01:51,000 --> 00:01:59,000
Or understanding or insight.

15
00:01:59,000 --> 00:02:05,000
You might even say Buddhism is all about moderation or the middle way.

16
00:02:05,000 --> 00:02:12,000
People who think of the first teaching of the Buddha where he taught what he called the middle way.

17
00:02:12,000 --> 00:02:26,000
And they think, well, that's the essence of Buddhism.

18
00:02:26,000 --> 00:02:28,000
The different ways to explain it.

19
00:02:28,000 --> 00:02:35,000
We might say, think about the sati patanasut and think of mindfulness.

20
00:02:35,000 --> 00:02:38,000
The Buddhism is all about mindfulness.

21
00:02:38,000 --> 00:02:43,000
It's the Buddha called mindfulness, the aikai and the manga.

22
00:02:43,000 --> 00:02:49,000
We need to go to a lot of his attention to a lot of his time to practice teaching

23
00:02:49,000 --> 00:03:00,000
the four sati patanas, the four foundations of mindfulness.

24
00:03:00,000 --> 00:03:15,000
But there's one concept, one idea that I think perhaps best sums up the Buddha's teaching.

25
00:03:15,000 --> 00:03:32,000
It's most true to the Buddha's intention for what his legacy was to be.

26
00:03:32,000 --> 00:03:40,000
In fact, it can be detrimental to try and explain Buddhism as being one thing.

27
00:03:40,000 --> 00:03:43,000
There is being one simple concept.

28
00:03:43,000 --> 00:03:54,000
So as a gross oversimplification, if we're going to grossly oversimplify the Buddha's teaching,

29
00:03:54,000 --> 00:04:10,000
we should sum it up by really we should sum it up by the concept of a pamanda.

30
00:04:10,000 --> 00:04:18,000
Simply because this is how the Buddha would have it, it seems would have it summed up.

31
00:04:18,000 --> 00:04:30,000
And how those who carried on the Buddha's teaching and compiled it to Pitaga, how they would have it summed up.

32
00:04:30,000 --> 00:04:46,000
How we know about the Buddha is because his last words were a pamanda and a sampadita.

33
00:04:46,000 --> 00:04:54,000
And everything that arises ceases, all formations are subject to cessation.

34
00:04:54,000 --> 00:05:07,000
It means anything that we build up will fall down, anything we cling to will eventually fade away.

35
00:05:07,000 --> 00:05:14,000
We'll lose everything we hold dear.

36
00:05:14,000 --> 00:05:39,000
And so we should, a pamanda in a sampadita, we've become full or bring to fulfillment or be ever engaged in a pamanda.

37
00:05:39,000 --> 00:06:00,000
We have in the Dhamapada we have verses like, a pamanda, a pamanda, a pamanda, a pamanda, a pamanda, a pamanda.

38
00:06:00,000 --> 00:06:16,000
Someone who is a pamanda, someone who is a pamanda, a pamanda is the path to immortality.

39
00:06:16,000 --> 00:06:27,000
Pamanda, which is the opposite, is the path to death.

40
00:06:27,000 --> 00:06:33,000
Those who are a pamanda never die.

41
00:06:33,000 --> 00:06:44,000
Those who are pamanda are as though already dead.

42
00:06:44,000 --> 00:07:10,000
Or we have, in regards to Angulimala, Yobubhi, Pamajituaya, Batcha, Nosa, Patcha, Sona, Pamajithi.

43
00:07:10,000 --> 00:07:17,000
Pamsapamanda, they illuminate, like Angulimala used to be pamanda.

44
00:07:17,000 --> 00:07:25,000
Now he's, once he met the Buddha, he became a pamanda.

45
00:07:25,000 --> 00:07:31,000
They light up the earth, they light up the world, like the moon coming out from behind the cloud.

46
00:07:31,000 --> 00:07:41,000
It's our mind, our being is capable of so much brightness, so much greatness.

47
00:07:41,000 --> 00:07:52,000
But when more pamanda is shrouded, it's covered, it's obscured.

48
00:07:52,000 --> 00:07:54,000
So how do we understand pamanda?

49
00:07:54,000 --> 00:07:58,000
It's an interesting question I like to ask this of my Sri Lankan students, because they tell me,

50
00:07:58,000 --> 00:08:01,000
what pamanda means to not be late.

51
00:08:01,000 --> 00:08:07,000
It's this curious thing, how it's changed and singly.

52
00:08:07,000 --> 00:08:14,000
But I think it comes from the idea that you lose track of time if you're pamanda.

53
00:08:14,000 --> 00:08:16,000
You lose track of yourself, right?

54
00:08:16,000 --> 00:08:19,000
It sounds quite Buddhist, doesn't it?

55
00:08:19,000 --> 00:08:27,000
When you're not aware, it's time to go, it's time to do this, it's time to do that, you're not present.

56
00:08:27,000 --> 00:08:36,000
So you lose track of time and as a result, you're late, something like that.

57
00:08:36,000 --> 00:08:42,000
But the best way, the easiest way to understand pamanda and up pamanda,

58
00:08:42,000 --> 00:08:51,000
pamanda being the bad one, up pamanda being the good one is when we look at how the Buddha discussed

59
00:08:51,000 --> 00:08:59,000
in toxicants, alcohol, the fifth precept, Sura, Maryamanda, pamanda,

60
00:08:59,000 --> 00:09:25,000
pamanda, alcohol, liquor and booze, liquor and beer is a pamanda, is a basis for pamanda.

61
00:09:25,000 --> 00:09:33,000
So the ordinary way of understanding pamanda is that it's intoxicating, intoxication,

62
00:09:33,000 --> 00:09:35,000
when we're drunk.

63
00:09:35,000 --> 00:09:37,000
That's what it means to be pamanda.

64
00:09:37,000 --> 00:09:39,000
It comes from the root munch.

65
00:09:39,000 --> 00:10:05,000
It means to be drunk, it means to be confused or deluded, clouded in the mind, unclear.

66
00:10:05,000 --> 00:10:15,000
But just turns it into something a little more special, a little more extreme.

67
00:10:15,000 --> 00:10:21,000
So pamanda is something like when you're actually mentally incapacitated,

68
00:10:21,000 --> 00:10:26,000
and munch is just the idea of being confused, I think.

69
00:10:26,000 --> 00:10:33,000
But pamans or pamanda, when it becomes...

70
00:10:33,000 --> 00:10:35,000
This is when you're truly intoxicated.

71
00:10:35,000 --> 00:10:41,000
So either physically from taking alcohol or intoxicating or mentally,

72
00:10:41,000 --> 00:10:51,000
from engaging in or being overwhelmed by desire, aversion, delusion,

73
00:10:51,000 --> 00:10:57,000
which clouded the mind, confused the mind,

74
00:10:57,000 --> 00:11:06,000
destroyed the clarity of the mind, and the purity of the mind.

75
00:11:06,000 --> 00:11:16,000
Of course, pamanda has everything to do with sepamanda,

76
00:11:16,000 --> 00:11:18,000
with mindfulness.

77
00:11:18,000 --> 00:11:24,000
But it's saditya, vipavas, so apamato dhiyudjati.

78
00:11:24,000 --> 00:11:33,000
To never be without septi, this is what it means to be apamanda.

79
00:11:33,000 --> 00:11:39,000
So we understand pretty clearly, it's not like this is hard to understand

80
00:11:39,000 --> 00:11:45,000
or poorly understood concept for those who have studied it.

81
00:11:45,000 --> 00:11:50,000
It's not like the texts are unclear.

82
00:11:50,000 --> 00:11:56,000
It's important that we understand exactly what this is an important concept to understand.

83
00:11:56,000 --> 00:12:01,000
Clarity of mind is what we're talking, so apamanda means not being clouded,

84
00:12:01,000 --> 00:12:05,000
not being confused, not being intoxicated, really.

85
00:12:05,000 --> 00:12:09,000
The idea is that the defilements of the mind intoxicate us.

86
00:12:09,000 --> 00:12:12,000
They take away our clarity.

87
00:12:12,000 --> 00:12:17,000
They keep us from seeing clearly.

88
00:12:17,000 --> 00:12:21,000
So our practice and meditation, really, this is what it's all about.

89
00:12:21,000 --> 00:12:23,000
We can talk about mindfulness.

90
00:12:23,000 --> 00:12:25,000
This is a good thing to talk about.

91
00:12:25,000 --> 00:12:27,000
We can talk about wisdom or insight.

92
00:12:27,000 --> 00:12:38,000
These are good aspects of it, but the real active component is cultivating clarity of mind.

93
00:12:38,000 --> 00:12:44,000
We use mindfulness to do that when you remind yourself, seeing, seeing,

94
00:12:44,000 --> 00:12:49,000
pain, pain, thinking, thinking, or whatever.

95
00:12:49,000 --> 00:12:52,000
You're actively cultivating clarity of mind.

96
00:12:52,000 --> 00:12:57,000
You're trying to remind yourself, hey, this is what's happening.

97
00:12:57,000 --> 00:13:00,000
This is what's present.

98
00:13:00,000 --> 00:13:02,000
This is reality.

99
00:13:02,000 --> 00:13:07,000
Bringing your mind back, dragging your mind out of the clouds,

100
00:13:07,000 --> 00:13:16,000
back down to the mundane, objective reality of our experience.

101
00:13:16,000 --> 00:13:19,000
What we're seeing, what we're hearing in every moment,

102
00:13:19,000 --> 00:13:31,000
smelling, tasting, thinking, the body of the mind.

103
00:13:31,000 --> 00:13:34,000
So there's this idea that we're intoxicated.

104
00:13:34,000 --> 00:13:37,000
We're intoxicated to ignorance.

105
00:13:37,000 --> 00:13:40,000
We're intoxicated due to our attachments in our version.

106
00:13:40,000 --> 00:13:43,000
We're like drunk people.

107
00:13:43,000 --> 00:13:52,000
They're really most of the time drunk on life, drunk on youth, drunk on sensual enjoyment.

108
00:13:52,000 --> 00:13:55,000
We don't see clearly.

109
00:13:55,000 --> 00:14:03,000
We have very strong delusions that our habits are based on delusion.

110
00:14:03,000 --> 00:14:12,000
And so our reactions to our experiences are based on delusion,

111
00:14:12,000 --> 00:14:15,000
why we like certain things, why we dislike certain things,

112
00:14:15,000 --> 00:14:24,000
why we react violently either positive or negative to our experiences.

113
00:14:24,000 --> 00:14:29,000
Because we are intoxicated, we're drunk.

114
00:14:29,000 --> 00:14:33,000
And so our meditation practice is about sobering up.

115
00:14:33,000 --> 00:14:39,000
It's about having this pure state of mind, this clear state of mind.

116
00:14:39,000 --> 00:14:41,000
That's the active component.

117
00:14:41,000 --> 00:14:43,000
That's what changes us.

118
00:14:43,000 --> 00:14:52,000
That's what lifts us out of the quagmire,

119
00:14:52,000 --> 00:15:02,000
and lifts us out of the morass of delusion that we're caught in.

120
00:15:02,000 --> 00:15:06,000
So if you want to, I mean, the use of this is to help us understand

121
00:15:06,000 --> 00:15:11,000
or help us get a sense of how meditation should be experienced.

122
00:15:11,000 --> 00:15:13,000
What should it feel like to meditate?

123
00:15:13,000 --> 00:15:18,000
How do you know if you're truly meditating?

124
00:15:18,000 --> 00:15:23,000
And it's the difference between following your delusions

125
00:15:23,000 --> 00:15:26,000
and seeing them clearly.

126
00:15:26,000 --> 00:15:31,000
Once you start to see your desires, your aversion, your confusion,

127
00:15:31,000 --> 00:15:34,000
your delusion, your doubt, your worry, your stress.

128
00:15:34,000 --> 00:15:37,000
Once you start to see it clearly,

129
00:15:37,000 --> 00:15:41,000
and you start to be present.

130
00:15:41,000 --> 00:15:43,000
It's like waking up.

131
00:15:43,000 --> 00:15:45,000
You feel like you lived your life asleep

132
00:15:45,000 --> 00:15:49,000
and you're only just waking up.

133
00:15:49,000 --> 00:15:51,000
It's like you lived your life drunk

134
00:15:51,000 --> 00:15:54,000
and you've only just somered up.

135
00:15:54,000 --> 00:15:57,000
And that's really, I mean, again,

136
00:15:57,000 --> 00:15:59,000
there are many ways and many important aspects

137
00:15:59,000 --> 00:16:00,000
of the Buddha's teaching,

138
00:16:00,000 --> 00:16:03,000
but the last words of the Buddha.

139
00:16:03,000 --> 00:16:06,000
What the commentary says is the whole of the tibitika,

140
00:16:06,000 --> 00:16:09,000
all these thousands of teachings of the Buddha.

141
00:16:09,000 --> 00:16:12,000
When you sum it up, it's the path

142
00:16:12,000 --> 00:16:16,000
that the weight and the means

143
00:16:16,000 --> 00:16:20,000
to become a pamada, to become sober,

144
00:16:20,000 --> 00:16:25,000
un-endtoxicated.

145
00:16:25,000 --> 00:16:29,000
And so along with all the other aspects of the teaching,

146
00:16:29,000 --> 00:16:36,000
this is what we see as the essence of Buddhism,

147
00:16:36,000 --> 00:16:40,000
because it leads to all of the good things.

148
00:16:40,000 --> 00:16:44,000
And it's the use of mindfulness to create clarity of mind,

149
00:16:44,000 --> 00:16:48,000
which brings about morality, concentration,

150
00:16:48,000 --> 00:16:52,000
and wisdom, and understanding, and freedom,

151
00:16:52,000 --> 00:16:57,000
and release liberation from suffering.

152
00:17:01,000 --> 00:17:04,000
So to be clear, this is what our meditation should bring.

153
00:17:04,000 --> 00:17:06,000
It shouldn't just make you calm or make you feel good,

154
00:17:06,000 --> 00:17:11,000
or reinforce your beliefs or your ideas.

155
00:17:11,000 --> 00:17:13,000
Meditation should help you.

156
00:17:13,000 --> 00:17:16,000
To make this shift where you start to see things clearly,

157
00:17:16,000 --> 00:17:19,000
you start to understand yourself

158
00:17:19,000 --> 00:17:22,000
and see what you've overlooked.

159
00:17:22,000 --> 00:17:26,000
You start to see what's in front of you.

160
00:17:26,000 --> 00:17:29,000
You can see the nature of your own mind,

161
00:17:29,000 --> 00:17:31,000
and the nature of the world around you,

162
00:17:31,000 --> 00:17:35,000
and both as imperman and suffering in non-self.

163
00:17:35,000 --> 00:17:37,000
And you begin to let go.

164
00:17:37,000 --> 00:17:43,000
You've been to give up all these bad habits

165
00:17:43,000 --> 00:17:45,000
that are like...

166
00:17:45,000 --> 00:17:47,000
It's like darkness.

167
00:17:47,000 --> 00:17:50,000
And in darkness, grow many unpleasant things,

168
00:17:50,000 --> 00:17:55,000
mushrooms, bacteria, mold.

169
00:17:55,000 --> 00:17:57,000
But when you shine the light on,

170
00:17:57,000 --> 00:18:00,000
when the sun shines down, it all dries up,

171
00:18:00,000 --> 00:18:06,000
and cleans up and becomes purified.

172
00:18:06,000 --> 00:18:08,000
It's purified by the light.

173
00:18:08,000 --> 00:18:12,000
All these bad habits, all these unwholesome qualities of mind,

174
00:18:12,000 --> 00:18:15,000
are dried up through the power of mindfulness,

175
00:18:15,000 --> 00:18:18,000
through the power of clarity.

176
00:18:22,000 --> 00:18:27,000
So something for us all to remember to think about,

177
00:18:27,000 --> 00:18:30,000
and to keep in mind when we practice,

178
00:18:30,000 --> 00:18:33,000
that our goal is clarity of mind.

179
00:18:33,000 --> 00:18:34,000
So there you go.

180
00:18:34,000 --> 00:18:36,000
That's the demo for tonight.

181
00:18:36,000 --> 00:18:38,000
Thank you all for tuning in.

182
00:18:38,000 --> 00:19:05,000
I wish you all the best.

