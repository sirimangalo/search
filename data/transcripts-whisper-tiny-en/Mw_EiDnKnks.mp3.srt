1
00:00:00,000 --> 00:00:02,640
OK, go ahead.

2
00:00:02,640 --> 00:00:06,480
I find that my meditation practice has progressed

3
00:00:06,480 --> 00:00:09,600
where I can see no need to think about the stresses

4
00:00:09,600 --> 00:00:11,120
I've had in the past.

5
00:00:11,120 --> 00:00:13,720
But now I'm seeing them pop up in my dreams that night.

6
00:00:20,880 --> 00:00:22,920
Yeah.

7
00:00:22,920 --> 00:00:25,920
Well, it couldn't be that you're still suppressing them.

8
00:00:25,920 --> 00:00:28,960
But that's not necessarily the case.

9
00:00:28,960 --> 00:00:32,720
They can't just be, you know, the brain keeps echoes of stuff.

10
00:00:32,720 --> 00:00:37,760
So you've got back, lots of backup copies of your emotions.

11
00:00:37,760 --> 00:00:40,960
Once you give them up, your brain still holds onto them

12
00:00:40,960 --> 00:00:45,000
and pops up with them.

13
00:00:45,000 --> 00:00:49,640
If you find that in your dreams, you're stressed about them.

14
00:00:49,640 --> 00:00:49,840
Right?

15
00:00:49,840 --> 00:00:51,640
It's how you're saying, I know anything about the stress.

16
00:00:51,640 --> 00:00:54,200
But it's OK, the question is, are you stressed in your dreams?

17
00:00:54,200 --> 00:00:55,600
Because if you're stressed in your dreams,

18
00:00:55,600 --> 00:00:57,600
then you're still missing.

19
00:00:57,600 --> 00:01:00,200
You still haven't dealt with them.

20
00:01:00,200 --> 00:01:02,680
I mean, do you still have problems?

21
00:01:02,680 --> 00:01:07,440
In fact, you should find that you dream less

22
00:01:07,440 --> 00:01:09,000
the further you get in the practice.

23
00:01:13,000 --> 00:01:16,760
But I can say it can still come up.

24
00:01:16,760 --> 00:01:18,840
Question is how you relate to it in your dream.

25
00:01:18,840 --> 00:01:21,600
If you still have fear and worry and stress and anger

26
00:01:21,600 --> 00:01:24,480
and lust and so on, even in the dreams

27
00:01:24,480 --> 00:01:28,480
of that design, those things are still in your mind.

