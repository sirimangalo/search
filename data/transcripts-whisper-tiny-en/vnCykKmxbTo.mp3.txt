Okay, hi and welcome back to Ask a Monk.
Today's question comes from German 1184.
You said in your Ask a Monk series, for people caught up in lust, it is good for them
to contemplate the unpleasant aspects of the body.
What is the proper way to do that?
I did mention this and I think I mentioned as well that for people caught up in anger,
it is good to practice loving kindness.
These two meditations are a part of a set of four meditations called the Arrakakamatana.
Arrakam means to guard.
So the meaning can either be these four meditations are meant to guard the person's mind
or there is something that protects you during the time of the practicing insight meditation
or practicing real meditation to seek clearly.
These are sort of like protection that will help to keep you from getting to the point
where you can't get your mind to the point where you can't practice meditation, so stopping
you from falling into great states of lust, great states of anger and so on.
The four kamatana or meditations are mindfulness of the Buddha, mindfulness of the
loteness or the ugliness of the body, the unpleasant aspects of the body, loving kindness
and death.
So these four meditations they have different qualities and of course these aren't necessary
and I haven't included them in my meditation series but they can be quite helpful, they
are quite helpful and so they are recommended if you're interested in practicing them.
Mindfulness of the Buddha is practical for Buddhist obviously for people who consider the
teachings of the Buddha to be correct, to be useful, to be beneficial.
So thinking about the Buddha as an example, thinking about the Buddha as someone who was
a really good teacher, who was the perfect teacher, someone who was free from defilement.
So we often think of the characteristics of the Buddha, for instance that he had great wisdom.
We think of the incredible wisdom and we read through his teachings and we gain great
faith that yes indeed this is a person who has great wisdom and so we should follow his
teachings because probably they are going to lead to the result that he says.
Because he had great purity, so we read and we learn about the Buddha's purity and also
through practicing his teachings we come to see that his teachings are incredibly pure.
There is nothing dogmatic or there is nothing negative, nothing that is going to lead
us to become brainwashed or whatever, that the Buddha was very open-minded and he was
simply giving something that was of use to people as opposed to looking for students or
trying to become famous or so on.
And the third quality is that he had great compassion because he didn't obviously have
to teach, he could have sat in the forest and meditated and passed away but when he was
asked to teach he decided that he would give up his own peace and quiet to give something
to other people.
There are many qualities of the Buddha and this is obviously something that is more useful
for Buddhists and I would encourage you to read about the Buddha.
One of the easiest ways that Buddhists will do it is to go over the polyverses and think
of consider each word for instance when we say it'd be so Bhakawa means the blessed one
so he's blessed and you learn about why he's blessed and so on.
But I won't go into detail, maybe in the future video I'll go through each of these
individually otherwise I won't have time.
The second meditation is mindfulness of the unpleasant aspects of the body and this is
quite useful to do away with the irrational view that the body is something beautiful.
There's really nothing beautiful about the body at all and it can be objected that there's
nothing ugly about the body either but if you put it on a set of balances our idea that
the body is beautiful is simply due to not paying enough attention to it whereas our idea
that it's unpleasant is actually I would say more based in fact and I'm only saying
that as a comparison if you compare our body to something like gold or diamonds or a flower
say our body actually isn't on a scale of beauty isn't very high and the way you understand
this is by going through the parts of the body so the unpleasant aspects of the body is
actually everything start at the top you start with the hair on your head and ask yourself
you know what is the hair like well it's like grass that's been planted in this skull
and you know what color is it and how does it smell you know if you don't wash it obviously
it's going to be unpleasant so we simply look at the hair and we're going to see that
actually you know the hair is not very beautiful we think oh look at how beautiful that
hair is but if you ever cut it off and put it on a plate you know would it look appetizing
or would it look beautiful you know what we do when we cut our hair off it looks disgusting
and people who keep their hair after they've cut it are often you know with revulsion
and we go through with the hair on our head the hair on our body our nails our teeth
our skin our our flesh our blood our bones our bone marrow our feces our urine our liver
our spleen our heart and so on and we go through all of the parts of the body no normally
monks will do this in Pali so we'll start with ques a loma na karata da jou monks
a naha loo at the team junk and then goes on and we go through 32 parts of the body and
then we'll break them up so we'll start with ques a ques a ques a ques a ques a which means
hair of the hand and we'll just repeat that over and over to ourselves contemplating the
hair on the head and much the same way as we practice insight meditation except now we're
focusing on a conceptual piece of reality you know the hair or the bones or the skin or
whatever and that does help you see it clearly you don't have to say this is disgusting
you just look at it and you'll see well actually you know it's not very beautiful you
don't have to you know agree that it's disgusting but it's certainly not the wonderful
incredible attractive thing that we think it is and that helps to bring us back to our
more rational state of mind in this regard the third meditation is loving kindness and
this is very useful after we meditate you know when once you finish meditation you should
use the power and the strength that you gain and the clarity of mind that you gain from
meditation use it to express your appreciation for all their beings and to clear up a lot
of the problems that we might have with other people this is something that really helps
with your insight meditation helps with helps us to meditate it helps us to straighten
out a lot of the the crookedness that we often have in our minds wishing first for our
parents to be happy then for our relatives for our family for the people nearby for the
people in this area in this city in this world and go from from you know my house to my
city to my country to the whole world to the whole universe start with humans and animals
angels God whatever to all beings extending it out one by one by one you can do that for
like five minutes after you finish meditating or even just a minute or two is fine and
that's useful according you know technically useful in terms of getting rid of anger
it's in a lot in a broader context it's useful for clearing up our relationships and
making it straightening our mind out the fourth one is mindfulness of death and this is quite
useful to help us to give it get a sense of urgency you know to help us wake up to the
fact that we can't just you know go on with our lives and live our lives as though we're
dead or as though there were no tomorrow or so on because eventually there's the
right day of reckoning and that's our death and then our whole life flashes before our
eyes and we have to you know face up and whatever is strong in our minds we're going to
cling to it and we're going to we're going to follow after that and we'll have to be
born again if it's a bad thing we'll be born in a bad rebirth the way we would be mindful
of death you know there's there's many ways that the the technical method is to simply
save yourself that life is uncertain death is certain life is not sure death is sure life
has death as its end all beings have to die I too will have to die one day but you can
adapt this and you can you know think of I always used to when I was in a plane I would
think about the plane crashing and I would examine my own emotions because obviously
that would be something very traumatic for me and so on so you you can use these and you
should use all of these these are all beneficial they're all meditations which can be
used in in a accompaniment to the inside meditation okay so thanks for the question
hope that helps
