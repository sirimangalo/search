1
00:00:00,000 --> 00:00:23,000
Good evening everyone, I'm broadcasting live April 26th, 6th.

2
00:00:23,000 --> 00:00:42,000
Today's quote again about anger.

3
00:00:42,000 --> 00:00:52,000
It's time from the Masudimaga.

4
00:00:52,000 --> 00:01:02,000
It's under the teaching about Mehta, so in order to cultivate Mehta you have to be skillful and overcoming anger.

5
00:01:02,000 --> 00:01:13,000
And then there's a set of verses that forms tonight's quote.

6
00:01:13,000 --> 00:01:26,000
So the first thing is, if someone hurts you, it hurts you insofar as they can.

7
00:01:26,000 --> 00:01:31,000
They've done whatever they can to hurt you.

8
00:01:31,000 --> 00:01:36,000
That makes you hurt once.

9
00:01:36,000 --> 00:01:44,000
You've heard because of the things that they've done.

10
00:01:44,000 --> 00:01:52,000
And then the question is, the question you should ask yourself, so this quote is actually,

11
00:01:52,000 --> 00:01:55,000
it's supposed to be directed to yourself.

12
00:01:55,000 --> 00:02:00,000
When you can angry about something someone has done to you,

13
00:02:00,000 --> 00:02:05,000
you should ask yourself, why do I want to hurt myself again?

14
00:02:05,000 --> 00:02:10,000
Why am I sitting here hurting myself?

15
00:02:10,000 --> 00:02:12,000
Because someone has hurt me.

16
00:02:12,000 --> 00:02:14,000
Why am I doing it again?

17
00:02:14,000 --> 00:02:17,000
Why am I making it twice as bad?

18
00:02:17,000 --> 00:02:23,000
Why am I helping my enemies?

19
00:02:23,000 --> 00:02:25,000
Who people who want to bring me suffering?

20
00:02:25,000 --> 00:02:30,000
Why do I suffer?

21
00:02:30,000 --> 00:02:35,000
Do you think that anger is a valid response or proper response?

22
00:02:35,000 --> 00:02:37,000
Do anything really?

23
00:02:37,000 --> 00:02:40,000
I'm going to set if you're in pain and then you get upset about it.

24
00:02:40,000 --> 00:02:48,000
It's like you have a thorn or a splinter in your skin,

25
00:02:48,000 --> 00:02:53,000
and you take another thorn and you try to gouge it out.

26
00:02:53,000 --> 00:02:59,000
You make it twice as bad.

27
00:02:59,000 --> 00:03:03,000
The second stand size,

28
00:03:03,000 --> 00:03:05,000
in tears you left your family.

29
00:03:05,000 --> 00:03:09,000
They had been kind and helpful to,

30
00:03:09,000 --> 00:03:15,000
so why not leave your enemy the anger that brings harm to you?

31
00:03:15,000 --> 00:03:21,000
So we left behind all that was dear to us to come to meditate to

32
00:03:21,000 --> 00:03:23,000
cultivate spirituality.

33
00:03:23,000 --> 00:03:29,000
We give up all the good things and all the good things or

34
00:03:29,000 --> 00:03:35,000
a pleasurable things in the world.

35
00:03:35,000 --> 00:03:39,000
And yet we bring our enemies with us.

36
00:03:39,000 --> 00:03:43,000
Now when we get angry, why are we holding on to bad things?

37
00:03:43,000 --> 00:03:55,000
This is an admonishment to self admonishment for someone who is angry.

38
00:03:55,000 --> 00:04:01,000
Actually it's generally easier to give up anger than it is to give up

39
00:04:01,000 --> 00:04:05,000
greed or craving.

40
00:04:05,000 --> 00:04:11,000
And this kind of admonishment is actually quite fairly simple.

41
00:04:11,000 --> 00:04:14,000
Just remind yourself that this is hurting you.

42
00:04:14,000 --> 00:04:16,000
This is harmful.

43
00:04:16,000 --> 00:04:27,000
The anger, the reaction, the judging.

44
00:04:27,000 --> 00:04:31,000
This anger that you entertain, the third stands of this anger that you

45
00:04:31,000 --> 00:04:32,000
entertain.

46
00:04:32,000 --> 00:04:36,000
And again I'm reading actually a neonomoly translation,

47
00:04:36,000 --> 00:04:40,000
so it's a bit different from the one on our website.

48
00:04:40,000 --> 00:04:44,000
The anger that you entertain is gnawing at the very roots of all the

49
00:04:44,000 --> 00:04:50,000
virtue that you guard, who is there such a fool as you?

50
00:04:50,000 --> 00:04:56,000
Anger, greed, they gnaw away at our goodness.

51
00:04:56,000 --> 00:05:00,000
No, they chew at it.

52
00:05:00,000 --> 00:05:04,000
They eat away at all the good that we've done.

53
00:05:04,000 --> 00:05:10,000
It gets spoiled. You can be helpful to people and

54
00:05:10,000 --> 00:05:13,000
and charitable and kind.

55
00:05:13,000 --> 00:05:18,000
Then you're angry one time and you lose your reputation.

56
00:05:18,000 --> 00:05:20,000
That's a good person.

57
00:05:20,000 --> 00:05:25,000
And it inflames your mind and it prevents you from enjoying

58
00:05:25,000 --> 00:05:29,000
the peace and happiness that come from goodness.

59
00:05:29,000 --> 00:05:37,000
Remind ourselves of how it's eating away at our virtue.

60
00:05:37,000 --> 00:05:41,000
It's making us, it's making us more coarse.

61
00:05:41,000 --> 00:05:49,000
It helps us to give up the inclination to being, to get

62
00:05:49,000 --> 00:05:52,000
angry.

63
00:05:52,000 --> 00:05:56,000
The next stand, another does ignobled deeds.

64
00:05:56,000 --> 00:05:59,000
So you are angry. How is this?

65
00:05:59,000 --> 00:06:06,000
Do you then want to copy to the sort of acts that he commits?

66
00:06:06,000 --> 00:06:11,000
So this person has done something evil and it is evil.

67
00:06:11,000 --> 00:06:15,000
And so why in the world are you now?

68
00:06:15,000 --> 00:06:18,000
Who believes that it was wrong?

69
00:06:18,000 --> 00:06:22,000
Why are you now doing the same thing?

70
00:06:22,000 --> 00:06:27,000
That's a curious point because when you get angry at someone

71
00:06:27,000 --> 00:06:30,000
who's evil, you're joining them in their evil.

72
00:06:30,000 --> 00:06:35,000
In fact, the Buddha said and it mentions in this up above that

73
00:06:35,000 --> 00:06:38,000
it's worse to get angry back at someone.

74
00:06:38,000 --> 00:06:40,000
It's a worse evil.

75
00:06:40,000 --> 00:06:44,000
Someone's angry at you and you return the anger.

76
00:06:44,000 --> 00:06:49,000
That's worse because we all get angry when anger comes

77
00:06:49,000 --> 00:06:57,000
and if we love each other then we let each other vent

78
00:06:57,000 --> 00:06:59,000
when someone gets angry at us.

79
00:06:59,000 --> 00:07:02,000
We appreciate what they're saying when they're sitting

80
00:07:02,000 --> 00:07:07,000
and we try to calm them down and appease them in some way.

81
00:07:07,000 --> 00:07:12,000
But when you get angry back then you start conflict.

82
00:07:12,000 --> 00:07:15,000
That's when the real problem is.

83
00:07:15,000 --> 00:07:17,000
So this is a good lesson.

84
00:07:17,000 --> 00:07:20,000
We judge each other very much and if someone gets angry

85
00:07:20,000 --> 00:07:22,000
we get angry back.

86
00:07:22,000 --> 00:07:25,000
If someone is greedy we get upset at them.

87
00:07:25,000 --> 00:07:28,000
If someone is arrogant and conceited we get angry and upset

88
00:07:28,000 --> 00:07:32,000
about the bad things in others.

89
00:07:32,000 --> 00:07:36,000
And then that makes us just as bad.

90
00:07:36,000 --> 00:07:39,000
Moreover we're too critical.

91
00:07:39,000 --> 00:07:43,000
Critical of ourselves and critical of each other.

92
00:07:43,000 --> 00:07:47,000
And in monasteries it's quite common for new monks

93
00:07:47,000 --> 00:07:49,000
to come and just criticize around.

94
00:07:49,000 --> 00:07:52,000
They criticize every other monk called the old monks

95
00:07:52,000 --> 00:07:55,000
or the monks who have been there for a long time

96
00:07:55,000 --> 00:07:59,000
because they're not perfect.

97
00:07:59,000 --> 00:08:02,000
With this kind of conception that you're not perfect

98
00:08:02,000 --> 00:08:05,000
then you deserve to be criticized.

99
00:08:05,000 --> 00:08:08,000
Really if you're not perfect you deserve to be helped

100
00:08:08,000 --> 00:08:12,000
to be supported and directed and guided.

101
00:08:12,000 --> 00:08:16,000
And pushed and prodded in the right direction

102
00:08:16,000 --> 00:08:21,000
but to denounce someone because they're imperfect

103
00:08:21,000 --> 00:08:23,000
that's not the Buddhist way.

104
00:08:23,000 --> 00:08:26,000
For more reasons than one.

105
00:08:26,000 --> 00:08:29,000
Because it's useless, it's unhelpful

106
00:08:29,000 --> 00:08:35,000
and because it's harmful to yourself as well.

107
00:08:35,000 --> 00:08:39,000
The next stand, the next stand

108
00:08:39,000 --> 00:08:43,000
is that when you get angry at something someone has done

109
00:08:43,000 --> 00:08:45,000
you do what they would have you do.

110
00:08:45,000 --> 00:08:49,000
They want you to suffer and you can go to your suffering

111
00:08:49,000 --> 00:08:54,000
and that's key because really other people can't make us suffer.

112
00:08:54,000 --> 00:08:57,000
They can hit you, they can denounce you,

113
00:08:57,000 --> 00:09:00,000
they can scold you, they can insult you

114
00:09:00,000 --> 00:09:02,000
but they can't make you suffer.

115
00:09:02,000 --> 00:09:05,000
They can't make your mind suffer.

116
00:09:05,000 --> 00:09:08,000
They can't make you get angry.

117
00:09:08,000 --> 00:09:15,000
I mean, it depends how you look at it

118
00:09:15,000 --> 00:09:20,000
but it's possible to be free from anger

119
00:09:20,000 --> 00:09:22,000
no matter what someone else does.

120
00:09:22,000 --> 00:09:27,000
But once you get angry that's when the suffering comes.

121
00:09:27,000 --> 00:09:30,000
There's no question you suffer when you're angry

122
00:09:30,000 --> 00:09:32,000
you suffer when you dislike something.

123
00:09:32,000 --> 00:09:35,000
That's where suffering comes from.

124
00:09:35,000 --> 00:09:42,000
The next stand is out.

125
00:09:42,000 --> 00:09:45,000
If you get angry then maybe you make him suffer.

126
00:09:45,000 --> 00:09:47,000
Maybe not.

127
00:09:47,000 --> 00:09:50,000
The width, the hurt that anger brings you certainly

128
00:09:50,000 --> 00:09:52,000
are punished now.

129
00:09:52,000 --> 00:09:55,000
So when you get angry back usually it's because you are thinking

130
00:09:55,000 --> 00:10:01,000
of ways to retaliate, to seek revenge.

131
00:10:01,000 --> 00:10:05,000
So there's like, well maybe you can find revenge.

132
00:10:05,000 --> 00:10:07,000
Maybe you'll actually end up harming the other person

133
00:10:07,000 --> 00:10:09,000
and getting revenge.

134
00:10:09,000 --> 00:10:11,000
We don't know, it's possible.

135
00:10:11,000 --> 00:10:14,000
But what's certain is that you're punishing yourself

136
00:10:14,000 --> 00:10:17,000
with your anger and you punish yourself more

137
00:10:17,000 --> 00:10:21,000
if you seek revenge.

138
00:10:21,000 --> 00:10:24,000
If anger blinded enemies set out,

139
00:10:24,000 --> 00:10:28,000
tread the path of woe, do you by getting angry

140
00:10:28,000 --> 00:10:32,000
too, intend to follow heel to toe?

141
00:10:32,000 --> 00:10:37,000
You intend to follow these anger blinded enemies.

142
00:10:37,000 --> 00:10:42,000
If hurt is done you by a foe because of anger on your part,

143
00:10:42,000 --> 00:10:46,000
then put your anger down for why?

144
00:10:46,000 --> 00:10:57,000
Should you be harassed groundlessly?

145
00:10:57,000 --> 00:11:02,000
That's why you weren't the one that did the evil

146
00:11:02,000 --> 00:11:04,000
why are you suffering?

147
00:11:04,000 --> 00:11:09,000
Someone hurts you or attacks you or criticizes you or so.

148
00:11:09,000 --> 00:11:12,000
That's their problem.

149
00:11:12,000 --> 00:11:16,000
Why are you suffering?

150
00:11:16,000 --> 00:11:20,000
Since states last but a moment's time those aggregates

151
00:11:20,000 --> 00:11:23,000
by which was done the odious act have ceased.

152
00:11:23,000 --> 00:11:26,000
So now what is it you are angry with?

153
00:11:26,000 --> 00:11:30,000
This is very much in meditation.

154
00:11:30,000 --> 00:11:33,000
The person who were angry, it doesn't exist.

155
00:11:33,000 --> 00:11:37,000
The states that arose that gave rise to the event

156
00:11:37,000 --> 00:11:41,000
that were angry at, that those have ceased.

157
00:11:41,000 --> 00:11:44,000
And were angry at something worried about something

158
00:11:44,000 --> 00:11:47,000
or when we're attached to something we want something?

159
00:11:47,000 --> 00:11:50,000
That thing is only a concept.

160
00:11:50,000 --> 00:11:52,000
The experience is a rise and cease.

161
00:11:52,000 --> 00:11:55,000
And if it's a good thing, well that good thing is

162
00:11:55,000 --> 00:11:59,000
not really good in any way, it just arises and ceases,

163
00:11:59,000 --> 00:12:02,000
comes and goes in the same with bad things.

164
00:12:02,000 --> 00:12:05,000
There's no person who is our enemy.

165
00:12:05,000 --> 00:12:07,000
What are we angry at?

166
00:12:07,000 --> 00:12:09,000
What really are we angry at?

167
00:12:09,000 --> 00:12:12,000
What we're angry at is a memory, a thought

168
00:12:12,000 --> 00:12:18,000
that arises in our mind that is an echo of some past deed.

169
00:12:18,000 --> 00:12:24,000
And we get angry at that thought.

170
00:12:24,000 --> 00:12:27,000
Who shall he hurt, who seeks to hurt another

171
00:12:27,000 --> 00:12:29,000
in the others absence?

172
00:12:29,000 --> 00:12:31,000
Your presence is the cause of hurt.

173
00:12:31,000 --> 00:12:34,000
Why are you angry then with him?

174
00:12:34,000 --> 00:12:36,000
Yes, we're angry at ourselves actually.

175
00:12:36,000 --> 00:12:39,000
It's our own mind states, the thoughts that arise

176
00:12:39,000 --> 00:12:40,000
in our own mind.

177
00:12:40,000 --> 00:12:43,000
The person who did this deed,

178
00:12:43,000 --> 00:12:46,000
if someone sits in his very angry at another person,

179
00:12:46,000 --> 00:12:48,000
it's nothing to do with the other person.

180
00:12:48,000 --> 00:12:52,000
It's you yourself or hurting yourself.

181
00:12:52,000 --> 00:12:55,000
You're angry at thoughts that arise in your own mind,

182
00:12:55,000 --> 00:12:59,000
nothing to do with the other person.

183
00:12:59,000 --> 00:13:02,000
So this is to do with anger,

184
00:13:02,000 --> 00:13:07,000
but more generally it's the idea

185
00:13:07,000 --> 00:13:10,000
that only we can hurt ourselves.

186
00:13:10,000 --> 00:13:15,000
And happiness as well has to come from within.

187
00:13:15,000 --> 00:13:21,000
Happiness can't be based on a thing.

188
00:13:21,000 --> 00:13:23,000
And suffering doesn't come from things.

189
00:13:23,000 --> 00:13:26,000
Happiness and suffering don't come from the outside.

190
00:13:26,000 --> 00:13:28,000
They come from within.

191
00:13:28,000 --> 00:13:29,000
It's quite simple.

192
00:13:29,000 --> 00:13:35,000
Our state of mind determines our state of happiness.

193
00:13:35,000 --> 00:13:38,000
Not the world around us.

194
00:13:38,000 --> 00:13:39,000
This is crucial.

195
00:13:39,000 --> 00:13:42,000
It's a specific Buddhist doctrine,

196
00:13:42,000 --> 00:13:46,000
that happiness and suffering come from the mind.

197
00:13:46,000 --> 00:13:49,000
This is clear.

198
00:13:49,000 --> 00:13:56,000
So a big part of our practice is learning to be objective

199
00:13:56,000 --> 00:14:01,000
and to see the experiences that come from external.

200
00:14:01,000 --> 00:14:09,000
Or that come from the body and the mind.

201
00:14:09,000 --> 00:14:17,000
To see them with wisdom.

202
00:14:17,000 --> 00:14:22,000
To cultivate a relationship with experience

203
00:14:22,000 --> 00:14:26,000
that is free from likes and dislike.

204
00:14:28,000 --> 00:14:31,000
To be objective and to see things as they are.

205
00:14:31,000 --> 00:14:34,000
Because when you do,

206
00:14:34,000 --> 00:14:37,000
it's not there's not intrinsic likability

207
00:14:37,000 --> 00:14:41,000
or dislikeability.

208
00:14:41,000 --> 00:14:46,000
Attractiveness or

209
00:14:46,000 --> 00:14:48,000
unattractiveness.

210
00:14:48,000 --> 00:14:52,000
It's not a quality of things.

211
00:14:59,000 --> 00:15:02,000
And so when we look at things as they are,

212
00:15:02,000 --> 00:15:05,000
we find that they're not attractive.

213
00:15:05,000 --> 00:15:08,000
We don't give rise to the attraction.

214
00:15:08,000 --> 00:15:10,000
Attraction and aversion,

215
00:15:10,000 --> 00:15:13,000
they come from not seeing things as they are.

216
00:15:13,000 --> 00:15:15,000
That's key as well.

217
00:15:15,000 --> 00:15:17,000
Because it could be otherwise.

218
00:15:17,000 --> 00:15:19,000
Mostly we think it's otherwise.

219
00:15:19,000 --> 00:15:22,000
We think that some things are intrinsically beautiful

220
00:15:22,000 --> 00:15:23,000
or desirable.

221
00:15:23,000 --> 00:15:25,000
Some of the things are not.

222
00:15:25,000 --> 00:15:27,000
Or intrinsically

223
00:15:27,000 --> 00:15:30,000
dislikeable.

224
00:15:30,000 --> 00:15:33,000
Or cause direct cause for disliking.

225
00:15:33,000 --> 00:15:34,000
It's not true.

226
00:15:34,000 --> 00:15:38,000
When you see things as they are,

227
00:15:38,000 --> 00:15:40,000
it's only because delusion.

228
00:15:40,000 --> 00:15:42,000
Because of delusion that we

229
00:15:42,000 --> 00:15:45,000
have desire and aversion.

230
00:15:45,000 --> 00:15:48,000
So that's our number for tonight.

231
00:15:48,000 --> 00:15:50,000
If you're interested in the wisudimanga,

232
00:15:50,000 --> 00:15:53,000
we're actually studying it on Sundays.

233
00:15:53,000 --> 00:15:57,000
But we're most of the way through it already.

234
00:15:57,000 --> 00:16:01,000
So, that's our number for tonight.

235
00:16:01,000 --> 00:16:04,000
If you're interested in the wisudimanga,

236
00:16:04,000 --> 00:16:07,000
we're actually studying it on Sundays.

237
00:16:07,000 --> 00:16:11,000
But we're most of the way through it already.

238
00:16:11,000 --> 00:16:16,000
We see the wisudimanga.

239
00:16:16,000 --> 00:16:19,000
We see the wisudimanga.

240
00:16:19,000 --> 00:16:27,000
It's more like a manual or a reference guide.

241
00:16:27,000 --> 00:16:30,000
It's good for teachers.

242
00:16:30,000 --> 00:16:33,000
It's also good for meditators.

243
00:16:33,000 --> 00:16:35,000
Experience meditators, I think.

244
00:16:35,000 --> 00:16:38,000
There's just so much in it, so many different.

245
00:16:38,000 --> 00:16:42,000
Like this set of verses is quite useful.

246
00:16:42,000 --> 00:16:47,000
That's from the wisudimanga.

247
00:16:47,000 --> 00:16:49,000
You too can go.

248
00:16:49,000 --> 00:16:50,000
Don't have to stay.

249
00:16:50,000 --> 00:16:53,000
I'm just going to answer questions if people have.

250
00:16:53,000 --> 00:17:13,000
So, I was in New York.

251
00:17:13,000 --> 00:17:21,000
The monk I was staying with in New York doesn't like the wisudimanga.

252
00:17:21,000 --> 00:17:23,000
Or the commentaries.

253
00:17:23,000 --> 00:17:27,000
Or the abbedama, which is very common with western monks.

254
00:17:27,000 --> 00:17:30,000
Especially in the agen chag group.

255
00:17:30,000 --> 00:17:34,000
Although he's not exactly with the agen chag group.

256
00:17:34,000 --> 00:17:38,000
So, keen on the sutives, which is great.

257
00:17:38,000 --> 00:17:43,000
The sutives are just the most pure.

258
00:17:43,000 --> 00:17:49,000
But, you know, the sutives are not the only source of Buddhism.

259
00:17:49,000 --> 00:17:59,000
The sutives are talks that were given mostly that were given to the monks, given to certain

260
00:17:59,000 --> 00:18:01,000
people at a certain time.

261
00:18:01,000 --> 00:18:07,000
So, they have to be interpreted, especially because we're so far removed from that time

262
00:18:07,000 --> 00:18:09,000
and that place.

263
00:18:09,000 --> 00:18:13,000
So, no matter what, you have to explain the sutives.

264
00:18:13,000 --> 00:18:18,000
So, whether you follow the commentary explanation, the sutimanga explanation.

265
00:18:18,000 --> 00:18:23,000
Or you follow some modern commentators explanation and it turns out to be no better.

266
00:18:23,000 --> 00:18:26,000
It turns out to be actually usually worse, I would say.

267
00:18:26,000 --> 00:18:27,000
I would argue.

268
00:18:27,000 --> 00:18:37,000
Worse in the sense that whether you agree with it or not, far less rigorous, far less objective,

269
00:18:37,000 --> 00:18:41,000
far less comprehensive, far less in depth.

270
00:18:41,000 --> 00:18:45,000
The wisudimanga just gets incredible work.

271
00:18:45,000 --> 00:18:50,000
If you disagree with it or agree with it or disagree with it, it's amazing that someone actually

272
00:18:50,000 --> 00:18:57,000
wrote it because it's huge and it's so in depth and it's got so much in it.

273
00:18:57,000 --> 00:19:05,000
Remember, I was giving a talk when I was in Sri Lanka last time and the man who was translating

274
00:19:05,000 --> 00:19:09,000
for me refused to translate because I was quoting the wisudimanga and he said,

275
00:19:09,000 --> 00:19:16,000
oh, we don't follow the wisudimanga here.

276
00:19:16,000 --> 00:19:22,000
I think I was saying something mean or evil.

277
00:19:22,000 --> 00:19:25,000
So, it's a contentious.

278
00:19:25,000 --> 00:19:30,000
The wisudimanga tradition is fairly contentious to some people.

279
00:19:30,000 --> 00:19:38,000
Most of the orthodox, most terabyte of society is for the most part they follow the wisudimanga.

280
00:19:38,000 --> 00:19:40,000
The wisudimanga.

281
00:19:40,000 --> 00:19:44,000
In Thailand, it forms the latter part of one's poly studies.

282
00:19:44,000 --> 00:19:50,000
So, any monk who studies higher level poly will have to translate the wisudimanga into

283
00:19:50,000 --> 00:19:56,000
Thai and then translate Thai passages, Thai translations of the wisudimanga back into

284
00:19:56,000 --> 00:19:57,000
poly.

285
00:19:57,000 --> 00:19:59,000
So, they have to really memorize.

286
00:19:59,000 --> 00:20:05,000
Many of them basically I think will memorize the wisudimanga in poly and English,

287
00:20:05,000 --> 00:20:08,000
which is impressive in itself.

288
00:20:08,000 --> 00:20:18,000
Maybe not memorize, but come close.

289
00:20:18,000 --> 00:20:23,000
Any requirements for taking a meditation course at our center?

290
00:20:23,000 --> 00:20:36,000
Yeah, probably.

291
00:20:36,000 --> 00:20:38,000
If you're coming alone in your under 16, I think we'd have to have a talk about that.

292
00:20:38,000 --> 00:20:39,000
I don't know.

293
00:20:39,000 --> 00:20:47,000
I would imagine having people under 16 coming and doing courses is problematic unless we talk to their parents.

294
00:20:47,000 --> 00:20:56,000
I think 16 is probably, and as for upper age limit, it's more to do with your health.

295
00:20:56,000 --> 00:21:01,000
If you're able to do walking and sitting meditation for hours a day, it's fine.

296
00:21:01,000 --> 00:21:11,000
You can sit on a chair if you have to.

297
00:21:11,000 --> 00:21:18,000
No, I don't know.

298
00:21:18,000 --> 00:21:22,000
There are some other things I'm looking at actually.

299
00:21:22,000 --> 00:21:25,000
Actually, the Angutranikaya has a bit.

300
00:21:25,000 --> 00:21:39,000
Volume and I've started maybe collecting some good sutas from it.

301
00:21:39,000 --> 00:21:42,000
It's a new set of robes for my trip to Asia.

302
00:21:42,000 --> 00:21:48,000
The funny thing about these robes, they're fairly new, but they came with holes in them.

303
00:21:48,000 --> 00:21:54,000
I think they were mouse eaten or they were ripped or something, because in my back there's actually little holes.

304
00:21:54,000 --> 00:21:59,000
I probably should sew up.

305
00:21:59,000 --> 00:22:01,000
Not that that's here or there.

306
00:22:01,000 --> 00:22:03,000
But I know these are fine in Asia.

307
00:22:03,000 --> 00:22:08,000
It'll be a little hot, but there's upper robe I don't wear a lot of the time.

308
00:22:08,000 --> 00:22:13,000
In my room, in my room, I rarely wear this.

309
00:22:13,000 --> 00:22:18,000
Sometimes I just go bare chest or sometimes I have a basic, what's called an onset.

310
00:22:18,000 --> 00:22:32,000
It's just a very thin shoulder cloth.

311
00:22:32,000 --> 00:22:38,000
But these aren't that hot. It's just cotton, so it's actually not that hot.

312
00:22:38,000 --> 00:22:41,000
But it's going to be hot in June.

313
00:22:41,000 --> 00:22:45,000
So I'll try to go up and live on the mountain.

314
00:22:45,000 --> 00:22:50,000
I'm going to try to go up on a mountain in Sri Lanka.

315
00:22:50,000 --> 00:22:52,000
I'm arguing with Sangha about that.

316
00:22:52,000 --> 00:22:56,000
Sangha wants me to stay in Colombo for two weeks.

317
00:22:56,000 --> 00:22:59,000
I'm trying to push to take a week out.

318
00:22:59,000 --> 00:23:02,000
He says, a week isn't enough.

319
00:23:02,000 --> 00:23:03,000
I know it isn't.

320
00:23:03,000 --> 00:23:05,000
I mean, a month isn't enough.

321
00:23:05,000 --> 00:23:14,000
I could spend months since Sri Lanka giving talks every day, but not really keen on it.

322
00:23:14,000 --> 00:23:23,000
And then in Thailand, maybe go back up on the mountain as well.

323
00:23:23,000 --> 00:23:28,000
Probably not stay in John Tom very long, because it's probably pretty hot in June.

324
00:23:28,000 --> 00:23:35,000
Unless it started raining, it should be okay.

325
00:23:35,000 --> 00:23:38,000
Do we only have a few months left?

326
00:23:38,000 --> 00:23:42,000
Have we really gone through most of the year?

327
00:23:42,000 --> 00:23:47,000
Have we started when do we start August or something?

328
00:23:47,000 --> 00:23:52,000
July, maybe?

329
00:23:52,000 --> 00:23:59,000
Time flies.

330
00:23:59,000 --> 00:24:06,000
Time flies.

331
00:24:06,000 --> 00:24:13,000
Time flies.

332
00:24:13,000 --> 00:24:30,000
See you all next time.

333
00:24:30,000 --> 00:24:51,000
Thank you.

