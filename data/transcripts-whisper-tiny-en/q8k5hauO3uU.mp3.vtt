WEBVTT

00:00.000 --> 00:05.960
There's a question, if a monk is not allowed to touch money, what happens to the money

00:05.960 --> 00:11.240
that people pay for retreats?

00:11.240 --> 00:14.080
Everyone, everyone.

00:14.080 --> 00:20.680
Well, you don't have to pay when you come here for a retreat.

00:20.680 --> 00:23.760
You can't pay when you come here, you can't pay when you come here, you can't pay

00:23.760 --> 00:29.760
when you come here, you can't give it a nation, you're allowed to give it a nation if

00:29.760 --> 00:30.760
you want to.

00:30.760 --> 00:32.920
We're not asking for it now.

00:32.920 --> 00:40.480
Money that we receive here is being used as a construction, innovation, things that have

00:40.480 --> 00:45.720
to be bought so that meditation can take place here.

00:45.720 --> 00:53.520
And we have here someone is still who is actually taking care of money affairs.

00:53.520 --> 00:57.760
So monks don't touch money here, that's correct.

00:57.760 --> 01:07.400
I mean, if you've read the, I wrote a couple blog posts on this issue because we're having

01:07.400 --> 01:13.920
difficulty with the idea of continuing to run a meditation center and it's really the

01:13.920 --> 01:17.120
most monks don't run meditation centers.

01:17.120 --> 01:23.520
The majority will have a monastic center, a monastery and most of the residents will be monks

01:23.520 --> 01:26.120
and they will just go off and do their own thing.

01:26.120 --> 01:37.160
But here we're trying to move things a little bit more globally and more universally available.

01:37.160 --> 01:42.520
So running a meditation center and allowing people to come for a week or a month and then

01:42.520 --> 01:48.320
leave, it creates some difficulty you need to have extra food for the meditators.

01:48.320 --> 01:53.920
You need to have water and facilities because sometimes they will fly from another country

01:53.920 --> 01:57.400
and they've only got just enough time to do the course.

01:57.400 --> 02:09.520
So you can't expect them to prepare or to develop and to fit in with the monastic lifestyle.

02:09.520 --> 02:11.520
You can't expect them to go on on and on and so on.

02:11.520 --> 02:16.040
You can't, I mean, they don't have the time to study into ordain and so on.

02:16.040 --> 02:22.280
So you have to have a room ready for them, food ready for them and everything available

02:22.280 --> 02:29.240
for them to allow them the time to develop.

02:29.240 --> 02:35.680
And that takes resources, the resources can't come naturally where you just receive whatever

02:35.680 --> 02:43.480
food that is available from the local community.

02:43.480 --> 02:50.720
So the use of money really makes that possible without any money in the monastery it

02:50.720 --> 02:56.840
would, it's just a timing thing really, because we have the patience, we can bring water

02:56.840 --> 03:04.480
up from the river and we can live with whatever clothes, whatever food that we have.

03:04.480 --> 03:10.160
We can go hungry or not eat for one day or so on and our practice will recover in the

03:10.160 --> 03:12.440
next days.

03:12.440 --> 03:18.960
But for the meditators coming for the meditation center, we need things when they're

03:18.960 --> 03:27.960
needed to make a meditator's practice really fruitful.

03:27.960 --> 03:35.560
They have to have the resources on hand when they arrive.

03:35.560 --> 03:41.480
So you know, money is kind of the simplest answer to that problem.

03:41.480 --> 03:43.120
And for that reason we have a steward.

03:43.120 --> 03:47.520
So normally we wouldn't need that, we wouldn't need someone, you know, a treasure actually

03:47.520 --> 03:53.160
using money or a bank account or so on, but the point is that the monks are still on the

03:53.160 --> 03:54.160
monastic side.

03:54.160 --> 03:57.820
It's just that we have lay people involved and we have an organization that includes

03:57.820 --> 04:01.920
lay people, simply because we're running a meditation center and there's nothing to do

04:01.920 --> 04:07.160
with buying things for the monks or necessities for the monks, it's buying things for the

04:07.160 --> 04:08.160
meditation center.

04:08.160 --> 04:14.400
So the monastic life really still has very little to do with money and we don't need

04:14.400 --> 04:19.120
it's not like the donations that come or for the purpose of buying stuff for the monks.

04:19.120 --> 04:24.800
Not directly anyway, I mean obviously things that the monks use to teach and things

04:24.800 --> 04:31.360
to allow the monks, the ability to teach and it's going to be painful.

04:31.360 --> 04:36.160
But there are lay people involved and the reason there are lay people involved is basically

04:36.160 --> 04:37.640
because we're running a meditation center.

04:37.640 --> 04:44.840
So we have one layman who is our official treasurer and he looks after receiving and

04:44.840 --> 04:52.960
using donations that arrive.

04:52.960 --> 04:54.720
So go ahead and explain that again.

04:54.720 --> 05:02.600
I was just saying that it's actually part of the Buddhist ideals to not ask for a payment

05:02.600 --> 05:10.120
for meditation for any teachings that actually they work on down on donation which

05:10.120 --> 05:16.080
is how much you want to give, not some set amount of money and that when I was looking

05:16.080 --> 05:22.840
to meditation retreats in my home country in England, there were many centers that asked

05:22.840 --> 05:29.600
for quite a steep payment, not donation and it was set and these places actually when

05:29.600 --> 05:35.880
I looked into them turned out to be not really in line with Buddhist ideals and not

05:35.880 --> 05:40.280
already somewhere I would recommend so if someone was asking you for a payment I would

05:40.280 --> 05:42.640
be slightly suspicious.

05:42.640 --> 05:47.480
What we're at works and so what I was saying is that we're actually trying to go one step

05:47.480 --> 05:52.640
further and because the understanding that I have of a donation is that it's from someone

05:52.640 --> 05:59.480
who wants to give that person has the intention to give and it may be because they

05:59.480 --> 06:09.200
want to give up, they have some resources that they want to see used in a certain way

06:09.200 --> 06:15.600
and so they want to see the cultivation of this or that and so they ask to enhance the

06:15.600 --> 06:22.280
meditation center they may not be in need by the meditation center but they think hey

06:22.280 --> 06:27.280
could I build another kuti and allow you to have another meditator come because I really

06:27.280 --> 06:34.520
want to see this progress so they want to expand things because they have resources and

06:34.520 --> 06:40.840
the other one is because there may be a need so someone might hear that the meditation

06:40.840 --> 06:48.520
center needs electricity for example or maybe the water pump is broken and they want

06:48.520 --> 06:54.880
to donate a water pump because there's a need it should not be from my point of view

06:54.880 --> 06:59.840
that we expect a donation from any meditator or that we expect our donations should come

06:59.840 --> 07:04.480
from meditators the donation should come from whatever person appreciates the things

07:04.480 --> 07:17.640
that we do it has the resources and there sees the need or the usefulness of their

07:17.640 --> 07:21.920
donation of their resources and this is what I was talking about in regards to the free

07:21.920 --> 07:27.400
lunch idea the free lunches is they say there's no such thing as a free lunch but I think

07:27.400 --> 07:32.760
that's exactly what is being explained here is that you give something because it needs

07:32.760 --> 07:37.640
to be given the Buddha didn't go out and teach because of his expectation of getting

07:37.640 --> 07:44.400
anything in return or exactly his idea that he would gain something from it he was fully

07:44.400 --> 07:51.120
enlightened he had no need for merit he had no need for good karma he had no need for anything

07:51.120 --> 07:55.600
and wouldn't gain anything from it because he was already where he needed to be and yet

07:55.600 --> 08:02.400
he still taught so this was his giving a free lunch to people giving people free teachings

08:02.400 --> 08:07.560
as in because there was a need because there was a benefit and because it was there was

08:07.560 --> 08:14.600
the invitation and the request and I think that's really what we're doing it so the

08:14.600 --> 08:20.360
people who would give to the Buddha the point would be that they were giving because

08:20.360 --> 08:25.240
there was the need the Buddha needed the food to continue teaching or or you could even say

08:25.240 --> 08:30.200
needed the food to survive and this is how people do it with Buddhist monks they may have

08:30.200 --> 08:33.640
their ideas of what they're going to get in return but ultimately it comes down to the

08:33.640 --> 08:40.280
fact that the monk needs food to continue the people need teachings to practice and so

08:40.280 --> 08:46.840
you give according to the need you don't give according to the reciprocity of expecting

08:46.840 --> 08:52.200
something in return or even expecting that that person will pay it forward so there was

08:52.200 --> 08:57.720
it looking at these three models one is where you expect something in return two is you expect

08:57.720 --> 09:03.720
that person to continue the to continue the giving so to expect them to give something to someone

09:03.720 --> 09:09.000
else to pay it forward idea and three is that you don't expect anything at all you give because

09:09.000 --> 09:14.760
there's a need because there's a request or even just because there's a benefit to be gained

09:14.760 --> 09:24.040
or there's a there's a beneficence there's a benefit to be accrued by the gift for the person

09:24.040 --> 09:30.120
who receives it and I think this is really how it goes so if anyone wants to give a donation

09:30.120 --> 09:34.760
to our organization they're of course welcome to I mean that whatever the donation is if it's

09:34.760 --> 09:39.000
an object then we'll use it for for various for the purpose that it was intended if it's

09:39.000 --> 09:46.200
a monetary donation then the treasurer and the lay organization will take it and will use it to

09:46.200 --> 09:51.880
pay for as yes I said construction and we'll use it to pay for maybe just for the meditators or

09:51.880 --> 10:03.720
oats or whatever things to unnecessary but but it really should be on an intention so

10:03.720 --> 10:14.120
the to relate this back to your question really the point is that if we have resources if there

10:14.120 --> 10:20.760
is money then it will be used by the lay people and if there's a need then the monks will request

10:20.760 --> 10:25.480
this or that from the lay people and the lay people who use the money to acquire that if there

10:25.480 --> 10:31.800
is no money then then then then you know what is the answer the answer is that we we we stop

10:31.800 --> 10:37.160
we stop doing what we're doing if we have no money then we go back to our arms round and we just

10:37.160 --> 10:45.160
have no meditators come but when money when money comes it is always kind of really answer that it

10:45.160 --> 10:51.880
it's used by the meditator by the lay people but but mostly as I said for the meditation center

10:51.880 --> 11:01.400
okay so actually quite a comprehensive and and answer to many questions that weren't even asked

11:02.360 --> 11:04.840
all in all in regards to to money and

11:06.520 --> 11:15.160
really me and also also mentioned that where she is in Mexico the places are asking for payment

11:15.160 --> 11:18.280
and I just say if places are asking for a set payment

11:18.280 --> 11:23.800
to maybe research more and try to find somewhere that isn't asking for a set payment because

11:25.080 --> 11:31.080
if somewhere is really in line with Buddhism then they're not going to ask you for a set payment

11:32.040 --> 11:39.560
I mean in my experience the same I mean there is something there is an exception to that

11:39.560 --> 11:50.360
and that is that in in western countries I mean the whole point is that then then there is the

11:50.360 --> 11:54.520
desire to run a course and once you have the desire to run a course then of course you need

11:55.240 --> 12:03.480
money to make it work but if the if the organization if the center is run by lay people

12:03.480 --> 12:09.800
then they can agree amongst themselves that everyone pitch in and in that sense you could think of

12:09.800 --> 12:17.320
it as a pitching in there's the requirement for you know everyone has to eat food and so you need

12:17.320 --> 12:24.440
enough food you need enough food for each person so you can say one way of making that simplifying

12:24.440 --> 12:30.120
that is to have everyone give a set amount for the food for the day and then you need to rent

12:30.120 --> 12:34.040
the place and since you don't have a sponsor to rent the place then everyone has to pay

12:34.040 --> 12:39.720
some room and some something for the room and so on and often that's what the payment is for

12:39.720 --> 12:44.280
that it's just because of exorbitant you know they they find a resort or something and do a

12:44.280 --> 12:49.800
meditation course and you're paying resort prices not to them organization but because everyone

12:49.800 --> 12:54.920
has to pitch in and yeah you might be paying a little bit to the organization for their clerical work

12:54.920 --> 13:00.440
and and even for salaries for certain people and so on and so then it really depends on what are

13:00.440 --> 13:06.280
the intentions of the people because there are pretty serious expenses and even we have expensive

13:06.280 --> 13:14.520
so our model is this way because we are uh well because I think because we have that one extra

13:14.520 --> 13:19.320
element of not really caring if we don't have enough money we'll just shut down so we're not

13:19.880 --> 13:24.440
really doing it out of desire we're doing it because there are people who want to come and people

13:24.440 --> 13:29.560
who are supporting it's because the people are donating money that we are continuing doing what we're

13:29.560 --> 13:34.600
doing that's what I probably put it out there it was like if it if it comes down to it then

13:34.600 --> 13:41.080
then we'll just have to stop what we do and we'll go back to just running a monastery and

13:41.080 --> 13:47.400
know the meditation center so you might say that it's missing that element but I think you could

13:47.400 --> 13:52.840
come up with some situation where it was just we all want to practice meditation and so we all

13:52.840 --> 13:59.240
have to pitch in as many people as we can.

