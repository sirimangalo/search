1
00:00:00,000 --> 00:00:04,580
Hello, and welcome back to our study of the Dhamupanda.

2
00:00:04,580 --> 00:00:10,520
Today we continue with verse number 15, which goes as follows.

3
00:00:10,520 --> 00:00:18,260
In the Sotrati, Beecha Sotrati, Pāpakādī, Ubaya Tha, Sotrati, Sotrati, Sotrati, Sotrati,

4
00:00:18,260 --> 00:00:20,300
Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati,

5
00:00:20,300 --> 00:00:27,140
Sotrati, Sotrati, Kilaitamatana, which means in the Sotrati,

6
00:00:27,140 --> 00:00:32,460
we hear he greeves, pages, sotrati, he hear after he greeves.

7
00:00:32,460 --> 00:00:38,660
Pāpakādī, Ubaya Tha, Sotrati, the evil doer, greeves in both places.

8
00:00:38,660 --> 00:00:47,580
Sotrati, Sotrati, Sotrati, he greeves, he is afflicted, he is destroyed.

9
00:00:47,580 --> 00:01:04,780
Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati, Sotrati.

10
00:01:04,780 --> 00:01:06,180
We have two verses here.

11
00:01:06,180 --> 00:01:08,460
The next verse is in regards to a different story.

12
00:01:08,460 --> 00:01:16,980
So the story with this verse said in the time of the Buddha there was a pig butcher.

13
00:01:16,980 --> 00:01:21,540
And when the monks would go on Armstrong, they would walk past and they would hear the screams

14
00:01:21,540 --> 00:01:25,380
where they would even see the pigs being killed.

15
00:01:25,380 --> 00:01:32,540
And the way the commentary explains the killing went on is this man, he would tie the pig

16
00:01:32,540 --> 00:01:38,860
to a post so it couldn't move and then beat it with a club to make its skin off and

17
00:01:38,860 --> 00:01:42,300
its flesh tender, but it was still alive.

18
00:01:42,300 --> 00:01:48,020
And then take boiling hot water and pour it down the pigs throat and have all the feces

19
00:01:48,020 --> 00:01:55,540
and undigested food pour out the bottom and he would continue to pour boiling water in

20
00:01:55,540 --> 00:02:03,700
until the water that came out was clean and meaning there was no feces left inside.

21
00:02:03,700 --> 00:02:09,300
Then he would pour the rest of the water over the pig's skin to peel off the outer skin

22
00:02:09,300 --> 00:02:13,500
and use the torch to burn off all the pig's hair.

23
00:02:13,500 --> 00:02:17,220
And then he would cut off the pig's head and then it would die.

24
00:02:17,220 --> 00:02:23,740
And he would collect the blood from the pig's neck and roast the pig in its own blood, eat

25
00:02:23,740 --> 00:02:27,220
as much as he could with his family and sell the rest.

26
00:02:27,220 --> 00:02:32,340
And they say for 55 years he lived his life like this and one day the monks were walking

27
00:02:32,340 --> 00:02:45,700
by 55 years later and they saw that today all the doors of the house were closed up and

28
00:02:45,700 --> 00:02:52,180
they could still hear the screams of pigs and grunting noises and scuffling and so they

29
00:02:52,180 --> 00:02:55,700
assumed that there must be something great going on and so they started talking about this.

30
00:02:55,700 --> 00:03:03,460
Now amazing it was 55 years this man had never learned the weight of the evil of his deeds

31
00:03:03,460 --> 00:03:09,140
and had never had never even come to listen to the Buddha's teaching or come to pay

32
00:03:09,140 --> 00:03:12,940
respect to the Buddha at all.

33
00:03:12,940 --> 00:03:17,820
And the Buddha heard them talking and asked them what are you talking about?

34
00:03:17,820 --> 00:03:23,420
They said they explained what it was and the Buddha said they're not and they said

35
00:03:23,420 --> 00:03:30,980
no and today we see that he's for seven days now and so seven days later we see that

36
00:03:30,980 --> 00:03:35,140
they have been closed up for seven days so they must be working on an even bigger slaughter

37
00:03:35,140 --> 00:03:40,300
or a special slaughter of some sort and the Buddha said no that's not really what's going

38
00:03:40,300 --> 00:03:41,900
on at all.

39
00:03:41,900 --> 00:03:48,900
Seven days ago it turns out that Tchunda this pork butcher became very ill and his affliction

40
00:03:48,900 --> 00:03:54,420
went to his head and he became quite deranged and as a result of the sickness and the

41
00:03:54,420 --> 00:04:00,980
derangement he started walking around the house on his hands and knees and grunting like

42
00:04:00,980 --> 00:04:01,980
a pig.

43
00:04:01,980 --> 00:04:05,980
Screaming like a pig and this is what they heard and the people in the house would try to

44
00:04:05,980 --> 00:04:08,820
restrain him but they couldn't do it.

45
00:04:08,820 --> 00:04:13,700
Also because as the commentary said he began to see his future and this brought him

46
00:04:13,700 --> 00:04:21,740
great derangement on his deathbed the fear and the concentration of the mind at that point

47
00:04:21,740 --> 00:04:30,660
allowed him to see his future and he began to draw him crazy and as a result all he could

48
00:04:30,660 --> 00:04:36,220
think about was the killing of the pigs and as a result dwelt that way for seven days

49
00:04:36,220 --> 00:04:42,820
and then on that day on the seventh day it was over and he died and went to hell so

50
00:04:42,820 --> 00:04:45,940
the Buddha explained what was really going on and then he said to the monks this is how

51
00:04:45,940 --> 00:04:51,460
it goes it's not just in the next life that a person suffers the person who who does

52
00:04:51,460 --> 00:04:57,780
even deeds person who does even deeds suffers broken this life in the next.

53
00:04:57,780 --> 00:05:06,660
So how this relates to our practice I think should be quite clear and it becomes quite clear

54
00:05:06,660 --> 00:05:11,580
when when a person undertakes practice because for the first time they're able to realize

55
00:05:11,580 --> 00:05:17,660
in the full magnitude of their deeds I think or it's clear that people who don't practice

56
00:05:17,660 --> 00:05:23,140
meditation are unable to understand the law of karma and they think of it as some kind

57
00:05:23,140 --> 00:05:29,100
of belief or some kind of theory and they don't really understand because when they do

58
00:05:29,100 --> 00:05:33,780
evil deeds they look around and they don't see anything happening so like I killed someone

59
00:05:33,780 --> 00:05:37,820
or I killed an animal and now I'm going to be killed they look around and not killed

60
00:05:37,820 --> 00:05:41,500
so they think well and there's nothing wrong with doing evil deeds because they can't see

61
00:05:41,500 --> 00:05:47,100
really what is the result of an good or an evil deed when a person comes to practice

62
00:05:47,100 --> 00:05:51,780
meditation they're watching every moment and they're seeing quite clearly what are the

63
00:05:51,780 --> 00:05:56,100
results of their deeds when they cling to something what is the result when they are

64
00:05:56,100 --> 00:06:00,180
angry or upset about something what is the result how does it affect their body how does

65
00:06:00,180 --> 00:06:09,220
it affect their mind how does it affect their clarity and their character and so as a

66
00:06:09,220 --> 00:06:17,460
result the law of karma becomes quite clear and quite evident but for evil for people

67
00:06:17,460 --> 00:06:22,300
who are engaged in great evil it's often not not clear and it can often be the fact

68
00:06:22,300 --> 00:06:29,340
that because of their position and because of good past deeds and their relative stability

69
00:06:29,340 --> 00:06:37,020
of mind they're unable to see the small seed growing inside and moreover they're often

70
00:06:37,020 --> 00:06:43,140
able through the power of the mind to push it away and to avoid looking at it to avoid

71
00:06:43,140 --> 00:06:48,900
thinking about it so that they aren't aware of evil deeds until it becomes so great

72
00:06:48,900 --> 00:06:52,660
and so gross that they can no longer hide it especially when they become sick and on

73
00:06:52,660 --> 00:06:58,780
on their deathbed and their power of mind becomes much weaker an ordinary person spends

74
00:06:58,780 --> 00:07:04,500
most of their time covering up their deeds now it does happen that their good people do

75
00:07:04,500 --> 00:07:10,540
bad deeds and to feel guilty and feel remorseful and are able to see how it's affected

76
00:07:10,540 --> 00:07:15,620
them when a person does or if a person starts going down a wrong path and they start to

77
00:07:15,620 --> 00:07:20,260
realize the nature of their mind right they get they burst out at someone and they realize

78
00:07:20,260 --> 00:07:25,420
that they have a lot of anger inside then they might feel guilty and that they that there's

79
00:07:25,420 --> 00:07:30,660
something that should be done and as a result they will they will begin to refrain from

80
00:07:30,660 --> 00:07:37,820
that in the future but it can often be the case that as in the case of Judah that it

81
00:07:37,820 --> 00:07:43,020
only can't be realization only comes from it to mate so people often ask well why if

82
00:07:43,020 --> 00:07:48,500
there's this law of karma why do we see good people suffering and bad people prospering

83
00:07:48,500 --> 00:07:54,420
but when you practice meditation when you actually look and examine what's going on

84
00:07:54,420 --> 00:07:58,980
inside you'll see that it's true that good people might suffer but their suffering isn't

85
00:07:58,980 --> 00:08:03,660
because of their goodness and it's true that evil people may prosper but their prospering

86
00:08:03,660 --> 00:08:09,580
is not because of their evil it can't be and you can see this by watching clearly what

87
00:08:09,580 --> 00:08:14,420
as I said what happens when these minds states arise what do they lead to you can see

88
00:08:14,420 --> 00:08:21,860
that a good state can never lead to suffering and an evil state can never lead to good

89
00:08:21,860 --> 00:08:29,220
gender to to happiness or to to prosperity it's only it's only generally in this life

90
00:08:29,220 --> 00:08:35,980
is because of our position and because of the the static nature of the material body

91
00:08:35,980 --> 00:08:41,900
and of the material world so our person might be born into a position of power or a position

92
00:08:41,900 --> 00:08:47,540
of wealth and as a result they can do many evil deeds without great repercussions except

93
00:08:47,540 --> 00:08:53,940
in the mind which of course is is much more dynamic and much quicker to pick up on the

94
00:08:53,940 --> 00:08:58,580
changes but eventually even the physical will begin to pick it up you will lose your friends

95
00:08:58,580 --> 00:09:04,540
you will gain corrupt friends your whole environment will slowly change but it changes

96
00:09:04,540 --> 00:09:12,740
a lot slower and so the results can be a lot slower coming and quite a bit less evidence

97
00:09:12,740 --> 00:09:17,700
now at the moment of death all of that changes because the physical is is removed from the

98
00:09:17,700 --> 00:09:23,300
equation the mind is now only depending on the mind there is no going back to seeing hearings

99
00:09:23,300 --> 00:09:29,220
now and you see there's nothing based on the body there's only the mental activity so

100
00:09:29,220 --> 00:09:36,980
it's a it's a repeated experience of the deeds that one has done or memories or whatever

101
00:09:36,980 --> 00:09:42,340
is going on in the mind so if one has a mind that is full of corruption then this will continue

102
00:09:42,340 --> 00:09:47,220
repeatedly arise in the mind thoughts of the evil deeds that we've done or memories of the evil

103
00:09:47,220 --> 00:09:54,180
deeds or thoughts about the future a feeling of where you're being pulled the future that

104
00:09:54,180 --> 00:09:58,500
you're developing for yourself and this is clear many people actually experiences on their

105
00:09:58,500 --> 00:10:03,860
death and can relate this some people actually leave their bodies and can see their bodies or

106
00:10:03,860 --> 00:10:08,900
can see their loved ones or so on and find themselves floating away if they have a near death

107
00:10:08,900 --> 00:10:13,140
experience they'll come back and they'll be able to relate good things or bad things that they

108
00:10:13,140 --> 00:10:18,580
saw that happened to them and it will be based on whatever's being clung to in their mind whatever

109
00:10:18,580 --> 00:10:26,100
their mind is attaching importance to at that time so if a person is is greatly engaged in evil

110
00:10:26,100 --> 00:10:32,740
deeds then they will be born in a place that is full of suffering because of their corrupt

111
00:10:32,740 --> 00:10:40,420
state of mind as the Buddha said when they see the evil they will finally realize so in some

112
00:10:40,420 --> 00:10:46,260
sense it's much better to to to actually see the evil and to to sort to feel sorrow where the

113
00:10:46,260 --> 00:10:51,620
discussion about this as to whether this is what the Buddha meant by Hiri and Otapa and it isn't

114
00:10:51,620 --> 00:10:58,180
really what he meant by Hiri and Otapa to feel shame and feel guilt but to see that you've done

115
00:10:58,180 --> 00:11:03,300
bad things can be a great waking up and to and can lead you to feel shame and to feel

116
00:11:03,300 --> 00:11:09,380
Hiri and Otapa meaning in the future you will when when the opportunity presents itself to do

117
00:11:09,380 --> 00:11:15,620
an evil deed you will shy away from it you will you will recoil from it the mind will naturally

118
00:11:15,620 --> 00:11:23,860
incline against it because of the because of seeing the evil of it and seeing the evil nature

119
00:11:23,860 --> 00:11:29,220
and this is what we gain very much from meditation from just introspection the person just begins

120
00:11:29,220 --> 00:11:35,060
to come to meditate in the very beginning they will see all of what they have carried with them

121
00:11:35,060 --> 00:11:41,780
and built up over the years through not meditating they'll see all of the habit habits and tendencies

122
00:11:41,780 --> 00:11:48,740
often a big wave of defilement of corruption you know it can at times it can be great anger and hatred

123
00:11:48,740 --> 00:11:58,020
at times it can be great lust and and need and wanting and so they they're able to

124
00:11:58,820 --> 00:12:03,780
to realize as this says here though they will grieve in the beginning they will feel

125
00:12:03,780 --> 00:12:10,100
great suffering in the beginning but eventually they will it will be a cause for them to change their

126
00:12:10,100 --> 00:12:14,900
way and in the future they won't want to do these things because they know how how much it

127
00:12:14,900 --> 00:12:21,460
hurts and the fact of their mind so this is the story of two under the pork butcher

128
00:12:21,460 --> 00:12:27,460
and the Buddha's teaching on the suffering that comes from doing evil deed so thanks for

129
00:12:27,460 --> 00:12:49,380
tuning in this has been another episode of our study of the Dhamupada

