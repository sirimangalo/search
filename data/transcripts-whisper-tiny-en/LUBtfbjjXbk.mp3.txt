Hello everyone. Today is January 1st 2014. I just like to take this opportunity to
wish everyone a happy new year. A new year that is filled with good things
most especially well-being. You may all find well-being in the new year. We
consider well-being to be more important than what we might call happiness. Because
the word happiness implies some sort of a feeling that is ephemeral subject to the
changes in life. So get it sometimes without it's most of the time or much of the
time. Whereas well-being though it implies happiness it's a sort of
happiness that is lasting. It's a sense of contentment. And so we tend to
wish each other well-being in the new year. The Buddha gave a specific
teaching on well-being. He used this word to describe what he thought was the
goal for beings in the world. You'd be the humans or any type of being that
might exist. What we're all looking for is well-being. You could say we're looking
for happiness but the word he used was well-being. He said there are four things
that you need to find well-being in this universe. And so I just like to
pass these on as a sort of a new year dumber. The first is that we need wisdom.
Obviously this is the most important Buddhism. Without wisdom you can't be
expected to understand what is for your benefit and to your detriment. Wisdom is
what allows you to make decisions that lead to your own happiness and
happiness of others, your own well-being and the well-being of others. It's what
allows you to find the right path and to find the right way for you in the
world, to make the right decisions and so on. Because it isn't to be the
highest that all other qualities of mind, whether they be compassion or love
or joy or contentment, equanimity, these kind of things, humility. They all
require wisdom. If they don't have wisdom it's just a fake, artificial construct.
They don't come from the heart. There's something to come from the heart. It
requires a true understanding of its benefit. An innate understanding of the
benefit of it. And likewise to remove unwholesomeness, requires understanding. To
abstain from anger and greed and delusion requires wisdom. So that's the first
one. The second quality is effort, exertion. Nothing is to be gained through
losing around. What has said, we are able to overcome suffering through effort.
It's not something that can be done without putting out effort. To be moral, to
avoid unwholesomeness, to avoid killing and stealing. It's easy to tell people not
to get angry and want to hurt each other and to greedy and want to steal from
each other and deluded and want to take drugs in alcohol and so on. But our
addictions and our habits lead us back over and again to these things. So the
breaking of habits requires effort. And the cultivation of wisdom also requires
effort. Effort in meditation, abstaining from bad unwholesomeness deeds, cultivating
good deeds, both of these require effort. And the purification of the mind
requires effort. We're going to be a good person and help others. We're going to be
charitable and generous and kind. We have to pull ourselves out of our inertia,
our habits of being lazy and indulgent and so on. Number three, we need to guard
our senses. The eye, the ear, the nose, the tongue, the body and the mind. Guard
here doesn't mean to, we have to avoid seeing and hearing and smelling and
tasting and feeling thinking. The point is not to avoid experience. The point is
to guard the extrapolation of experience. So the judgment, the partiality.
In what we're trying to limit our experience, we're just trying to
limit our reaction to our experience. The one that is objective and wise. So
when you see, you should know that you're seeing. You shouldn't know that
that's bad, that's good. And me and mine and get angry and upset or
addicted and attached to it. When you hear, when you smell, it's not that we
can't experience the whole range of experience. It's that we have to be very
careful to stay objective so that we don't cultivate addiction. We don't
cultivate partiality. We don't cultivate these memories in the mind. That
brought me pleasure. That was a good thing. That brought me suffering. That was a
bad thing. And therefore, cultivate a version and attachment, which of course
leads to pushing and pulling and eventually suffering. They're inevitably
suffering. So guarding our senses is very important. It's not that we
repress our experiences, but filtering is so that the dirt stays out in the
experience alone comes in. And seeing is just seeing, hearing is just hearing
is fine. And number four is we have to give up everything. This is the claim
in Buddhism is that happiness doesn't come from clinging. The way of the world
is that the more you get, the more you hold on, the more happiness you get, the
more the more pleasure you attain. And the Buddha noticed that this isn't
the case. Many religious people have noticed that this isn't the case, that
it's through letting go and giving up, that you find true happiness. So in
Buddhism, this is the claim that only through letting go can you be truly happy
and truly free? Because true happiness has to be what we call an nimisa
suka. An namasa means a namasa means a niramasa.
Niramisa, sorry, niramisa, which means having nothing to do with an object,
not taking an object. Happiness that it is dependent on something,
it's dependent on what is impermanent. Happiness should be this well-being,
should be our contentment with the sister's of life. If your happiness
depends on something, unless that thing is eternal as permanent, then it's
going to create some instability and the potential for loss. Loss is a common
experience in the world for people who cling to things, for people who hold on to
things. They will inevitably have to suffer loss if not in this life than in
future lives. Because of impermanence, you can't hold on to anything forever.
So true happiness, the way to go to find true happiness is not clinging, it's
not depending, it's not hedging your bet that something is going to last and
hoping that you get through life without losing the things that you love. True
happiness is through giving that out, giving up the whole worry and concern
about losing this or that thing that's finding true contentment. And so the
similarly is like the difference between someone who clings to something, being
afraid that they're going to fall, to be cling to the side of a cliff, being afraid
that you're going to fall. This is how most people of their lives always worry
about what the consequences of letting go are going to be. As opposed to a bird
that let's go of something knowing that they're going to fly or realizes that
they're able to fly and so they let go. And this is the change that we have to
come about and we have to realize that to be truly happy, to fly, to not be
stuck to anything. We have to let go, to find true happiness, we have to give up
our attachment. And that happiness that comes from letting go, we call well-being.
You know, it's a state of being that is content and that piece with itself for
eternity. So that is the Buddhist teaching on well-being and that's the
dhamma for the new year that we may all find well-being. If we want to find
well-being we should cultivate these four things. It's the kind of framework for
resolution that we should have in the new year. So some food for that. Thank you
all for tuning in and wishing you all a happy and well-new year, 2014 or
whatever year it might be in your calendar. So wishing you all the best
peace and happiness.
