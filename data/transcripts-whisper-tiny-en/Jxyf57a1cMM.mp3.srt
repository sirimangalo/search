1
00:00:00,000 --> 00:00:05,960
In order to follow Buddhism, I must not cling to happiness or people that give me happiness.

2
00:00:05,960 --> 00:00:11,440
Does this mean that I shouldn't make friends end or stop associating with my friends as I

3
00:00:11,440 --> 00:00:14,840
enjoy being around my friends and it gives me happiness?

4
00:00:14,840 --> 00:00:20,840
Okay, the word happiness, again the ambiguity.

5
00:00:20,840 --> 00:00:25,920
Buddhism says that none of that brings you happiness, and that's the unfortunate thing.

6
00:00:25,920 --> 00:00:30,080
If these things give you happiness, true happiness, then you should cling to them and

7
00:00:30,080 --> 00:00:31,880
hold on to them.

8
00:00:31,880 --> 00:00:35,840
But because they don't give you happiness, clinging to them is a cause of suffering.

9
00:00:35,840 --> 00:00:37,320
That's what Buddhism says.

10
00:00:37,320 --> 00:00:44,880
Buddhism doesn't say must not, should not, Buddhism says, if then.

11
00:00:44,880 --> 00:00:47,880
So it's much more powerful.

12
00:00:47,880 --> 00:00:53,120
It's not trying to convince your course, you into being one way or another.

13
00:00:53,120 --> 00:01:00,360
It's telling you the truth, and it's up to you to realize the truth, appreciate it,

14
00:01:00,360 --> 00:01:02,360
or deny the truth.

15
00:01:02,360 --> 00:01:14,080
So if you cling to people, the fact that it, or the idea that it makes you happy is an illusion.

16
00:01:14,080 --> 00:01:19,520
And as you practice meditation, you'll realize that it's not actually making you happy.

17
00:01:19,520 --> 00:01:25,360
It's actually just giving you some pleasure, which entices you to cling stronger, which

18
00:01:25,360 --> 00:01:32,880
eventually leads to repeated distress and suffering when every time you don't get what

19
00:01:32,880 --> 00:01:40,840
you want or every time you're forced to strive for the pleasant states again.

20
00:01:40,840 --> 00:01:45,720
So the enjoying being around your friends, except where it's involved with wholesomeness.

21
00:01:45,720 --> 00:01:51,280
There's the case where you're helping each other, where you're appreciating each other,

22
00:01:51,280 --> 00:01:56,040
where you're compassionate, and so on, where you're able to support each other.

23
00:01:56,040 --> 00:01:58,560
That is positive.

24
00:01:58,560 --> 00:02:06,080
But the enjoyment, the liking or the attachment to pleasurable states, is un also, and will

25
00:02:06,080 --> 00:02:09,800
lead you to suffering when you have to leave your friends when you're surrounded by unpleasant

26
00:02:09,800 --> 00:02:11,720
people, et cetera, et cetera.

27
00:02:11,720 --> 00:02:12,720
It means you're vulnerable.

28
00:02:12,720 --> 00:02:19,160
You're always vulnerable to change, and therefore suffering is in your future, because

29
00:02:19,160 --> 00:02:49,000
of that attachment.

