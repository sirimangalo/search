1
00:00:00,000 --> 00:00:06,000
Sometimes I hear stories of people who are assumed to be our hands, getting terrible illnesses.

2
00:00:06,000 --> 00:00:14,000
For example, Ajancha, is it wrong to assume that an our hand would be free from such states because of their purity?

3
00:00:14,000 --> 00:00:22,000
A free of such, from such states of severe and terrible illness?

4
00:00:22,000 --> 00:00:33,000
Well, you know the story about Ajancha, he actually seemed to live as a vegetable for many years, I think, something like that.

5
00:00:33,000 --> 00:00:42,000
It is wrong to assume that an our hand would be free from such states because of their purity.

6
00:00:42,000 --> 00:00:59,000
Our hands and even a Buddha had to cope with the past karma and it has to be worn out.

7
00:00:59,000 --> 00:01:19,000
To be expired, so our hands can experience a lot of suffering and a lot of terrible illness, but they do react very different from normal people.

8
00:01:19,000 --> 00:01:28,000
They would rather let go of it and not make a big deal out of it.

