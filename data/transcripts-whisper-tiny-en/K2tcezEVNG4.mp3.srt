1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:01:00,000 --> 00:01:02,000
You

4
00:01:30,000 --> 00:01:32,000
You

5
00:02:00,000 --> 00:02:02,000
You

6
00:02:30,000 --> 00:02:32,000
You

7
00:03:00,000 --> 00:03:02,000
You

8
00:03:30,000 --> 00:03:32,800
Good evening everyone welcome to our live broadcast

9
00:03:35,300 --> 00:03:38,900
Tonight we're looking at Tango tournique. I have a book of for us

10
00:03:41,400 --> 00:03:46,500
And it's it does one one sixty-one and one sixty-two

11
00:03:46,500 --> 00:03:52,500
You

12
00:03:54,200 --> 00:03:56,200
These are the four types of practice

13
00:04:00,300 --> 00:04:04,200
An important and well-known classification of practice

14
00:04:07,200 --> 00:04:09,200
Based on two criteria

15
00:04:09,200 --> 00:04:14,700
So we have four types of practice do kapatipada dandha vinya

16
00:04:16,900 --> 00:04:19,900
Practice which is painful and

17
00:04:21,600 --> 00:04:23,600
gives

18
00:04:23,600 --> 00:04:25,600
Realizations slowly

19
00:04:28,400 --> 00:04:32,100
Number two do kapatipada keep a vinya

20
00:04:34,200 --> 00:04:36,200
Practice which is

21
00:04:36,200 --> 00:04:40,200
Painful but brings realization quickly

22
00:04:42,800 --> 00:04:45,900
Number three so kapatipada dandha vinya

23
00:04:47,400 --> 00:04:49,400
Practice which is pleasant

24
00:04:51,200 --> 00:04:54,400
But brings realization slowly and

25
00:04:55,600 --> 00:04:58,700
Number four so kapatipada keep a vinya

26
00:04:58,700 --> 00:05:04,700
Practice which is pleasant and brings realization quickly

27
00:05:07,700 --> 00:05:13,400
Imako Vikouet at the soul that you pada is these monks are the four types of practice

28
00:05:14,600 --> 00:05:16,600
That's one sixty-one

29
00:05:16,600 --> 00:05:18,600
Simple

30
00:05:18,600 --> 00:05:25,400
I might think well, there's not much to that but in and of itself that gives what they give the important lesson of this that

31
00:05:25,400 --> 00:05:29,400
How progress in

32
00:05:31,400 --> 00:05:33,400
Cultivating understanding is

33
00:05:34,400 --> 00:05:37,400
Independent of our enjoyment of meditation

34
00:05:38,400 --> 00:05:40,400
It's independent of whether

35
00:05:40,400 --> 00:05:42,400
Meditation is painful and pleasant

36
00:05:43,400 --> 00:05:45,400
That's the important lesson here

37
00:05:45,900 --> 00:05:47,900
So lesson which

38
00:05:48,400 --> 00:05:50,400
You often have to learn the hard way

39
00:05:50,400 --> 00:05:54,400
Through

40
00:05:54,400 --> 00:05:56,400
Going through different types of practice where

41
00:05:57,400 --> 00:06:03,400
It's pleasant but you're not gaining anything until you finally realize and learn to let go of your pleasant

42
00:06:04,400 --> 00:06:06,400
Sensations and then

43
00:06:06,400 --> 00:06:11,400
Going through unpleasant meditation and feeling discouraged because you feel like you're not getting anywhere

44
00:06:12,400 --> 00:06:16,400
Or you feel like it's a sign that your practice is failing

45
00:06:16,400 --> 00:06:21,400
To finally realize well there's nothing to do with that and actually I'm learning a lot

46
00:06:23,400 --> 00:06:31,400
And of course the other two sometimes it can be pleasant and you can learn a lot sometimes it can be unpleasant and you don't learn anything

47
00:06:32,400 --> 00:06:34,400
You just suffer

48
00:06:37,400 --> 00:06:40,400
Let me go back and forth between these types of practice

49
00:06:40,400 --> 00:06:48,400
Generally there's a sense that a person is mostly practicing one or the other

50
00:06:50,400 --> 00:06:52,400
And that's where 162 comes in

51
00:06:53,400 --> 00:06:55,400
162 gives us a little bit more detail about

52
00:06:57,400 --> 00:07:00,400
One way of understanding these four types of practice

53
00:07:00,400 --> 00:07:11,400
So a person who has a painful practice and slow realization

54
00:07:11,400 --> 00:07:13,400
Well there's two factors here as well

55
00:07:13,400 --> 00:07:15,400
The first one is they have

56
00:07:16,400 --> 00:07:19,400
Ti-ba-ra-ra-ra-gajatika

57
00:07:19,400 --> 00:07:21,400
They're a person who is of a nature

58
00:07:21,400 --> 00:07:32,400
whose nature is to have very sharp, strong lust

59
00:07:35,400 --> 00:07:37,400
Acute lust

60
00:07:37,400 --> 00:07:52,400
And because of that acute lust, they suffer

61
00:07:53,400 --> 00:07:56,400
They experience bodily and mental suffering

62
00:07:57,400 --> 00:08:00,400
So a person who has lust, it's a fever in the body

63
00:08:00,400 --> 00:08:09,400
It can actually cause great stress and sickness when you want things that you can't get

64
00:08:10,400 --> 00:08:17,400
And in the mind as well the anguish from not getting what you want, from having one thing and going through withdrawal

65
00:08:18,400 --> 00:08:22,400
Whenever you don't get it, then you can't get it when you want to get it

66
00:08:22,400 --> 00:08:52,400
And likewise, one has is Ti-ba-ro-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra-ra

67
00:08:52,400 --> 00:09:02,880
and mental suffering that is born of Dosa, and be kind of often.

68
00:09:02,880 --> 00:09:09,080
So anger is even easier to see anger is in and of itself at a type of anguish and so

69
00:09:09,080 --> 00:09:17,400
much stress and suffering physically and mentally comes from anger.

70
00:09:17,400 --> 00:09:24,000
Even like those with Mohat, Tiba Mohat at the core, so with acute delusion in this result

71
00:09:24,000 --> 00:09:27,840
of that delusion, they suffer physically and mentally.

72
00:09:27,840 --> 00:09:33,040
Pollution puffs you up, it gives you headaches, it gives you tension in the body when

73
00:09:33,040 --> 00:09:38,700
you're worried and stress about things, when you think too much about things and obsess

74
00:09:38,700 --> 00:09:40,200
is all delusion-based.

75
00:09:40,200 --> 00:09:46,600
Based on misunderstanding, we heard our bodies, we heard our minds.

76
00:09:46,600 --> 00:09:56,600
So that's the suffering, that's where suffering comes from, comes from these three things.

77
00:09:56,600 --> 00:10:08,320
Mohatum, Raghajandosatum, Mohatum, born of lust, anger and delusion.

78
00:10:08,320 --> 00:10:17,240
But that pain that comes from those defilements is independent, to some extent, independent

79
00:10:17,240 --> 00:10:26,080
progress one makes, because when one's mind there's something, well, whether one puts

80
00:10:26,080 --> 00:10:29,120
one's mind to something, that's what's important, so that's the other factor here.

81
00:10:29,120 --> 00:10:32,920
Here we have someone who has slow realization, well, it's not because of the pain that

82
00:10:32,920 --> 00:10:47,040
they're feeling, it's because Banchindriyani-mudu-ni-pattu-bawanti, there are five faculties,

83
00:10:47,040 --> 00:10:56,920
come, become manifest or manifest themselves weekly, not weekly, but in a weak way, mudu-duk,

84
00:10:56,920 --> 00:11:08,320
manifest it as mild, so this means confidence, they have very weak confidence, shaky confidence,

85
00:11:08,320 --> 00:11:17,720
media effort, they have very effort from lazy, satindriya, not very mindful, their ability

86
00:11:17,720 --> 00:11:26,880
to remember themselves, to stay, recollecting the present moments, not very good, often

87
00:11:26,880 --> 00:11:31,720
lost in the past in future, and samadhi, they have no concentration or weak concentration,

88
00:11:31,720 --> 00:11:39,920
they're distracted in a lot, and Banchindriyani-mudriyani have not so much wisdom, they're

89
00:11:39,920 --> 00:11:50,800
able to see things clearly, it's because of these, that one's realization is slow, and

90
00:11:50,800 --> 00:11:54,480
this is clear, a person who has not much confidence, well, think of a person who has

91
00:11:54,480 --> 00:11:59,160
not much confidence, they're not able to accomplish anything, this we know in the world,

92
00:11:59,160 --> 00:12:07,360
but it's even doubly important as a meditator, looking at the mind, you know, the confidence

93
00:12:07,360 --> 00:12:13,160
you don't have confidence in yourself, you never get anywhere, if you're a person who

94
00:12:13,160 --> 00:12:18,320
doubts all the time, that's the reason why your practice might be going slowly, you have

95
00:12:18,320 --> 00:12:25,840
to realize I'm full of doubt and this isn't useful for me, this isn't workable, so you

96
00:12:25,840 --> 00:12:34,000
learn to let go of the doubt, for example, doubting, doubting, and we really recognize that

97
00:12:34,000 --> 00:12:42,360
you're somewhat lazy, then you give up your laziness, set deep, and you learn how to

98
00:12:42,360 --> 00:12:49,160
be mindful, that's what we're doing in the meditation, but you cultivate better and

99
00:12:49,160 --> 00:12:57,080
more acute mindfulness, concentration, you try to focus your mind, you need effort, but

100
00:12:57,080 --> 00:13:02,080
you also need concentration, so concentration means bringing your mind back again and

101
00:13:02,080 --> 00:13:08,880
again to the present moment, focusing on what's here, what's now, and wisdom, try to

102
00:13:08,880 --> 00:13:13,880
see things clearly, don't just walk and say stepping right to the right, actually be

103
00:13:13,880 --> 00:13:24,720
aware, this is stepping right, see the nature of it, arising and seeing, so those are the

104
00:13:24,720 --> 00:13:30,240
two factors, and so the other three are based on that person with painful practice of

105
00:13:30,240 --> 00:13:34,560
quick wisdom, well there's still got lots of pain and suffering from their defilements,

106
00:13:34,560 --> 00:13:41,120
but they're able to remove them quickly because of their sharp faculties, so they apply

107
00:13:41,120 --> 00:13:49,640
their mental faculty, which they never applied before and therefore suffer, and they

108
00:13:49,640 --> 00:13:56,840
attain quick realization, a person who has pleasant practice but slow realization, doesn't

109
00:13:56,840 --> 00:14:02,000
be a person who's done a lot of good deeds and has cultivated wholesome mind state,

110
00:14:02,000 --> 00:14:07,960
so they aren't obsessed with lust, anger and delusion, but their practice is slow because

111
00:14:07,960 --> 00:14:12,560
they are not applying their faculties, they're not cultivating their faculties, perhaps

112
00:14:12,560 --> 00:14:21,600
their faculties have become weaker, and finally a person who's practiced is pleasant

113
00:14:21,600 --> 00:14:33,480
and quick, quick realization, doesn't because, again, they are not obsessed, not consumed

114
00:14:33,480 --> 00:14:41,360
by lust, anger and delusion, and likewise, their faculties are sharp, they have confidence,

115
00:14:41,360 --> 00:14:48,840
they have effort, they have mindfulness, they have concentration, they have wisdom, and

116
00:14:48,840 --> 00:14:57,280
so they're quickly able to realize the truth, for types of practice you want to know

117
00:14:57,280 --> 00:15:03,720
about whether your practice is stagnating, whether why it might be that your practice is

118
00:15:03,720 --> 00:15:16,560
not progressing quickly as quickly as you'd like, these are the reasons, nothing to do

119
00:15:16,560 --> 00:15:32,120
with happiness or suffering again, just set up another camera, look at this guy, he just

120
00:15:32,120 --> 00:15:43,120
finished his course, putting in one display, look at how quietly he sits there, he's been

121
00:15:43,120 --> 00:15:50,120
practicing ardently for the past four days and nights, he's been practicing quite strenuously,

122
00:15:50,120 --> 00:16:12,120
quite different from before when they were all fidgeting though, just give me a second,

123
00:16:12,120 --> 00:16:38,200
I'll send it, come and say something please, and without talking about the details of your

124
00:16:38,200 --> 00:16:47,680
practice, can you just say something about what you feel you've gained from it, or how

125
00:16:47,680 --> 00:17:05,600
you feel improved by my clearness of seeing things as this, or to my cup, like this, seeing

126
00:17:05,600 --> 00:17:22,080
pain, that's just pain, it's not my pain, something that rises and ceases, I'm just

127
00:17:22,080 --> 00:17:34,680
not to run away from it, are you happy after all this, yeah, I've been making this

128
00:17:34,680 --> 00:17:50,080
a little bit, I've learned a lot, I'm very happy about this, good, okay, thank you,

129
00:17:50,080 --> 00:18:18,080
I'll send her, he's from Norway I think, so hello everyone, do we have questions tonight,

130
00:18:18,080 --> 00:18:30,440
so we do, we do, so I'll do to Alexander, got one more in the basement still, he's just

131
00:18:30,440 --> 00:18:39,880
finishing up tonight, I'm realizing a change in some of my noting approach, so wanted

132
00:18:39,880 --> 00:18:47,560
to clarify it if it's correct instead of assuming it is, with pain as an example, true

133
00:18:47,560 --> 00:18:55,080
for feeling, et cetera, to, first I used to note pain as a general sense, now if I put

134
00:18:55,080 --> 00:19:00,240
in more effort and really go to the source, it gets to a single point of pain, which magically

135
00:19:00,240 --> 00:19:07,320
disappears before I can truly get to it and get another point, arising, ceasing, until

136
00:19:07,320 --> 00:19:14,040
all points fade, question is should one put in this extra effort versus keeping it general,

137
00:19:14,040 --> 00:19:22,400
keeping it at a general sense of pain at an overall level, thank you, I don't know that

138
00:19:22,400 --> 00:19:28,520
either is absolutely proper, it sounds like two extremes there, you should be aware of

139
00:19:28,520 --> 00:19:33,880
pain locally at that one specific pain, but I wouldn't put too much effort into trying

140
00:19:33,880 --> 00:19:39,680
to pinpoint it down to a single point, you know, if you have pain in your head you focus

141
00:19:39,680 --> 00:19:46,240
on your head and say pain, pain in your leg, focus on the leg and say pain, but you don't

142
00:19:46,240 --> 00:19:56,440
have to find a specific point because you're altering it when you do that, that's going

143
00:19:56,440 --> 00:20:11,560
to far I think.

144
00:20:11,560 --> 00:20:16,840
Today I worked eight hours on five hours sleep, when meditating in the afternoon, come

145
00:20:16,840 --> 00:20:21,480
and close to the end of the thirty minutes, I felt my head bob down and felt myself

146
00:20:21,480 --> 00:20:26,400
falling asleep, I was angry that I wasn't mindful like other times in my meditation,

147
00:20:26,400 --> 00:20:33,080
do I just accept this rather than trying to find a trick to stop, to a trick from stopping

148
00:20:33,080 --> 00:20:35,080
myself from dosing off?

149
00:20:35,080 --> 00:20:41,360
Well there are some tricks, but yeah if you've been sleeping enough or if you've been

150
00:20:41,360 --> 00:20:47,800
working too hard, probably going to fall asleep no matter what trick you find, a clarification

151
00:20:47,800 --> 00:20:52,800
that you know it doesn't sound like you weren't being mindful, probably weren't being

152
00:20:52,800 --> 00:20:57,000
mindful either, but it's not because of mindful lack of mindfulness that you're falling

153
00:20:57,000 --> 00:21:03,080
asleep, mindfulness would be acknowledging tired, tired even if you do fall asleep, when

154
00:21:03,080 --> 00:21:06,240
you wake up it would be knowledge of knowing, when you're angry, well that's a sign

155
00:21:06,240 --> 00:21:10,800
you're not mindful, the anger itself is not mindful, it's important to be careful that

156
00:21:10,800 --> 00:21:15,960
you don't misunderstand what is meant by the word mindfulness, mindfulness means to

157
00:21:15,960 --> 00:21:20,720
know something as it is, it doesn't have anything to do with whether you're falling asleep

158
00:21:20,720 --> 00:21:28,960
or not, not directly, so don't, certainly don't fuss too much about anything else, but

159
00:21:28,960 --> 00:21:33,160
absolutely when you're fussing you're no longer mindful, so in your anger you should

160
00:21:33,160 --> 00:21:39,040
say to yourself, angry, angry, angry, there's no accepting either, it's another problem

161
00:21:39,040 --> 00:21:47,800
I would say, don't use the word accept, you acknowledge, it's a bit maybe a bit nitpicky

162
00:21:47,800 --> 00:21:52,840
but I don't like to use the word accept because there's no state in the world that we

163
00:21:52,840 --> 00:21:59,920
should ever accept, but we should not cling to, so we kind of reject actually, we reject

164
00:21:59,920 --> 00:22:08,360
everything in the sense of not buying into it, so it's almost like accepting, it's

165
00:22:08,360 --> 00:22:13,400
why I say it might be nitpicky, I don't like that word I guess, because it's said

166
00:22:13,400 --> 00:22:19,840
that implies complacency, you know, one that falls into complacency, you want to improve

167
00:22:19,840 --> 00:22:27,400
yourself, but improve yourself by being more rejecting in terms of not getting caught

168
00:22:27,400 --> 00:22:33,520
up in it, okay, so I fell asleep, I'm not going to let that get to me, I'm not going

169
00:22:33,520 --> 00:22:37,880
to let that trip me up, so as soon as you're conscious again, you're going to say knowing,

170
00:22:37,880 --> 00:22:50,600
come back to rising, falling, you know.

171
00:22:50,600 --> 00:22:55,000
In one video I've watched about mindfulness on YouTube, this speaker said that

172
00:22:55,000 --> 00:23:00,600
thoughts are rising and our heads are only as much of something we own as the sounds we

173
00:23:00,600 --> 00:23:05,720
hear from other objects walking down the street, would you say that's an accurate way

174
00:23:05,720 --> 00:23:12,800
to put it, thank you, I don't understand, okay, only as much, it's an awkward way of

175
00:23:12,800 --> 00:23:24,960
phrasing it, so sounds we hear, sounds we hear are not ours and neither are the thoughts,

176
00:23:24,960 --> 00:23:30,920
so I kind of get it, it's the idea that thoughts are just another object of the sense,

177
00:23:30,920 --> 00:23:38,120
thoughts are like sound sounds are a sense of the ear door, thoughts are sense material

178
00:23:38,120 --> 00:23:45,000
at the mind or technically that's the case, or not technically but roughly speaking

179
00:23:45,000 --> 00:23:53,440
that that's one way of understanding it and it's true, that being said there is a difference

180
00:23:53,440 --> 00:24:01,960
between thoughts and sounds, thoughts are qualitatively different, they have different qualities

181
00:24:01,960 --> 00:24:08,240
to them and the Abidhamma talks about that, I guess for all intents and purposes practically

182
00:24:08,240 --> 00:24:13,160
speaking yes they're just another object of the sense and right as far as being ours

183
00:24:13,160 --> 00:24:19,560
neither one of them is ours, we don't own either of them, I mean the point is you should

184
00:24:19,560 --> 00:24:25,520
never think of owning them and the more you think about them as being yours, the more entangled

185
00:24:25,520 --> 00:24:30,600
you become with them, trying to control them, trying to fix them, both with sounds and

186
00:24:30,600 --> 00:24:41,080
with thoughts.

187
00:24:41,080 --> 00:24:46,080
Does your tradition believe in the Dark Knight of the Soul, if so why does this happen

188
00:24:46,080 --> 00:24:53,840
and how do we escape it?

189
00:24:53,840 --> 00:24:59,720
When I go from awareness of an object to thinking about it, I lose what the object is,

190
00:24:59,720 --> 00:25:06,040
anything described in thought is no longer what it is, concepts are built from thought, so

191
00:25:06,040 --> 00:25:12,200
they are farther from the truth, reality is only observed before thought, do your teachings

192
00:25:12,200 --> 00:25:26,120
and meditation help people remain in the reality that I am discussing, yeah well concepts

193
00:25:26,120 --> 00:25:32,680
are far from reality but thoughts are real, this is what you have to admit, so you have

194
00:25:32,680 --> 00:25:40,160
to accept that you have an object and that the experience of that object is a real

195
00:25:40,160 --> 00:25:49,320
experience, so you suppose you feel the seat that you are sitting on, well that feeling

196
00:25:49,320 --> 00:25:57,000
is real, I mean insofar as anything can be real that is pretty clearly the closest you

197
00:25:57,000 --> 00:26:03,120
are going to get or the most clearly real thing you are going to get, but then suppose

198
00:26:03,120 --> 00:26:08,880
you start thinking about that feeling, well yes you have lost the reality of the feeling,

199
00:26:08,880 --> 00:26:13,240
you are no longer involved with that, but there is a new reality and that is the thought,

200
00:26:13,240 --> 00:26:20,120
so the thought becomes a thought of a concept, but the thought itself is real, so as far

201
00:26:20,120 --> 00:26:25,040
as our tradition, yes our tradition is absolutely about remaining in the reality, that is

202
00:26:25,040 --> 00:26:30,840
why we use this word, like a mantra, the mantra keeps us focused on reality, when you

203
00:26:30,840 --> 00:26:37,320
say to yourself feeling, feeling it keeps your mind focused on reality, so that your thoughts

204
00:26:37,320 --> 00:26:44,600
are about the object, your awareness is on the reality, when you are thinking you would

205
00:26:44,600 --> 00:26:50,160
say to yourself thinking, thinking, so you are able to stay aware of thinking, just

206
00:26:50,160 --> 00:27:04,400
as thinking, do you agree that many questions we ask you will stop once we understand

207
00:27:04,400 --> 00:27:10,080
your reference frame, I don't think a lot of questions are within your school of teachings,

208
00:27:10,080 --> 00:27:14,600
I think we will progress on the meditation you teach and question you better once we remain

209
00:27:14,600 --> 00:27:23,560
in this reference frame, where everything is arising and ceasing, more I would more say

210
00:27:23,560 --> 00:27:28,600
the more you practice the fewer questions you have, which is why I sometimes discourage

211
00:27:28,600 --> 00:27:34,880
people who ask lots of questions, because it is a sign that they are not really meditating

212
00:27:34,880 --> 00:27:42,480
most likely, you know we don't have here I don't think, it doesn't tell us who has meditated,

213
00:27:42,480 --> 00:27:48,320
if you go to your chat it does, but we haven't added that yet, if you go to the chat

214
00:27:48,320 --> 00:27:52,600
you see people with a green ring around them, those are the meditators that they don't

215
00:27:52,600 --> 00:28:08,840
have that in ask, of course, some of these questions are hours old, so, yeah, it's work.

216
00:28:08,840 --> 00:28:16,800
Bante, I experience something with LSD that may have benefited me while meditating, I

217
00:28:16,800 --> 00:28:21,440
learn something out of that experience, I feel like I shouldn't be taking it or any other

218
00:28:21,440 --> 00:28:26,720
in talks, I can anymore, I'm divided about selling, giving, or throwing away the rest,

219
00:28:26,720 --> 00:28:32,080
I possess a LSD, please be my guide in this, you wouldn't want to sell or give it away

220
00:28:32,080 --> 00:28:39,720
because it's considered to be a problematic thing, there's, you can learn things from LSD,

221
00:28:39,720 --> 00:28:48,040
from psilocybin, from pay, maybe ecstasy or ketamine, you can learn from anything, you

222
00:28:48,040 --> 00:28:55,080
can learn from hitting yourself over the head with a bat, you can learn from all sorts

223
00:28:55,080 --> 00:29:02,680
of things, doesn't mean they're actually the sum of their results, it is positive, usually

224
00:29:02,680 --> 00:29:13,640
the sum of these things is negative, so the problems that come from them, so if you're

225
00:29:13,640 --> 00:29:19,600
in the mind to think 200, I would say, I've never taken LSD, so I actually can't comment

226
00:29:19,600 --> 00:29:31,320
exactly, except for that, as I commented before about drugs, it's not real, it's a crutch

227
00:29:31,320 --> 00:29:39,200
and it's like a cheat, you know, and you can't cheat in this, it's too real, it's real

228
00:29:39,200 --> 00:29:45,040
that the, it's so real that the act of cheating actually sullies it because you're cheating,

229
00:29:45,040 --> 00:29:51,360
you're not, you know, the cheating changes you, if you cheat on something, so trying

230
00:29:51,360 --> 00:30:00,200
to get something from a tablet, or you're calling it a tablet, or you're calling it a tablet,

231
00:30:00,200 --> 00:30:10,160
so that LSD is laziness, so that, that in itself is a bad thing, whether or not the

232
00:30:10,160 --> 00:30:20,280
drug trip is, is it all beneficial or harmful, so from the get-go-anything like that, even

233
00:30:20,280 --> 00:30:26,160
if it's thought of as being for the purpose of spiritual enlightenment, you know, to

234
00:30:26,160 --> 00:30:30,440
debate whether it does bring spiritual enlightenment because you're already being lazy,

235
00:30:30,440 --> 00:30:33,600
just the biggest problem I had with drugs, even before I was meditating, so I found

236
00:30:33,600 --> 00:30:38,960
them to be artificial, it wasn't real, it wasn't really me doing it, you know, it wasn't

237
00:30:38,960 --> 00:30:47,960
through the sweat of my brow, so it's just some dumb drug, so I'd throw them away, and

238
00:30:47,960 --> 00:30:54,160
I'd do, flush it, it would feel better.

239
00:30:54,160 --> 00:31:01,160
No, don't flush them, they'll get into the water supply, yeah, I'm not sure.

240
00:31:01,160 --> 00:31:07,320
Yeah, well, I wonder what would be a safe way, I think there's some like medicine collections,

241
00:31:07,320 --> 00:31:11,400
just try to keep those things out of the, I don't even see LSD, I thought it was just

242
00:31:11,400 --> 00:31:16,280
a little piece of paper, it's just like, I don't think it's that much, well I don't

243
00:31:16,280 --> 00:31:23,480
know, it's funny, I never see it, I don't think I've ever seen LSD, to look up recycling

244
00:31:23,480 --> 00:31:30,480
of LSD, it's the burden I've done.

245
00:31:30,480 --> 00:31:38,000
Yes, that's pardon me, as I've become more mindful, I've noticed I slip into lucid dreaming

246
00:31:38,000 --> 00:31:43,960
more frequently, I've tried meditating in this lucid state and was unable to find my breath,

247
00:31:43,960 --> 00:31:48,680
uprising and falling, what's the best way to deal with lucid dreaming in order to further

248
00:31:48,680 --> 00:31:53,680
my practice?

249
00:31:53,680 --> 00:31:59,000
I mean, I wouldn't encourage lucid dreaming, I would concern myself much like this, it might

250
00:31:59,000 --> 00:32:03,240
be a time where you want to tell yourself to wake up, because you're going to be awake

251
00:32:03,240 --> 00:32:09,680
sleeping, it might as well be awake awake, and you can actually meditate, and then lucid

252
00:32:09,680 --> 00:32:15,680
dreaming is not my cup of tea, I don't think that through the practice eventually you

253
00:32:15,680 --> 00:32:23,680
dream less, that's sort of what we tend to notice.

254
00:32:23,680 --> 00:32:29,680
I wonder if you could meditate in a lucid dream, people who ask that before and it doesn't

255
00:32:29,680 --> 00:32:40,680
sound like it.

256
00:32:40,680 --> 00:32:46,280
I have just found that if I go slowly enough and concentrate in both walking and sitting meditation,

257
00:32:46,280 --> 00:32:50,480
I can catch any thinking before I've moved down to the next object.

258
00:32:50,480 --> 00:32:58,680
Is this concentration, this intention, watching the catch thoughts correct practice?

259
00:32:58,680 --> 00:33:04,160
Probably not, sounds like you might be going slow to make it easier, I wanted to be

260
00:33:04,160 --> 00:33:10,360
challenging, I remember, and this sounds like you're probably cultivating a trick, a short

261
00:33:10,360 --> 00:33:19,800
kind of cheat, don't cheat, don't go too slow, don't concentrate too much, just try and

262
00:33:19,800 --> 00:33:26,120
be mindful, otherwise you're controlling it, it makes it easy, but also quite stressful

263
00:33:26,120 --> 00:33:31,280
because you have to work hard at that and you can't maintain it, I wanted to feel natural,

264
00:33:31,280 --> 00:33:36,120
it shouldn't feel slow, it shouldn't feel fast, it shouldn't feel forced or focused,

265
00:33:36,120 --> 00:33:45,280
it should just feel real.

266
00:33:45,280 --> 00:33:48,280
Speaking of cheats here, that's good for this one.

267
00:33:48,280 --> 00:33:56,280
Hello Bante, is it okay to use isolation tanks for meditation?

268
00:33:56,280 --> 00:34:06,080
Sure, if you like avoiding problems in order to solve them, no, it's not okay, it's

269
00:34:06,080 --> 00:34:10,280
the worst kind of cheating ever.

270
00:34:10,280 --> 00:34:22,480
All of our problems come from these things, if you don't allow them to arise, how can

271
00:34:22,480 --> 00:34:26,080
you fix them?

272
00:34:26,080 --> 00:34:31,080
Hi Bante, how does a meditator prevent losing the wisdom gain through meditation?

273
00:34:31,080 --> 00:34:34,880
Keep meditating until you become enlightened, it's become enlightened until the wisdom will

274
00:34:34,880 --> 00:34:37,880
never go.

275
00:34:37,880 --> 00:34:47,080
That's it, I mean surround yourself with people who are also meditating also wise, read books,

276
00:34:47,080 --> 00:34:55,080
do good deeds, help others, be kind, be moral, lots of things that protect you, practice

277
00:34:55,080 --> 00:35:00,080
loving kindness, practice mindfulness and death, there's lots of things that protect your wisdom.

278
00:35:00,080 --> 00:35:05,080
It's a good question.

279
00:35:05,080 --> 00:35:10,080
It's really an important question, I suppose.

280
00:35:10,080 --> 00:35:12,080
You're all caught up on questions now.

281
00:35:12,080 --> 00:35:19,080
Alright, I guess that's it.

282
00:35:19,080 --> 00:35:24,080
Thank you Robin for your help, thanks everyone for your questions, have a good night.

283
00:35:24,080 --> 00:35:25,080
Thank you Bante, good night.

