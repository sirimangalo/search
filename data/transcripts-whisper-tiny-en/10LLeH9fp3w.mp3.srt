1
00:00:00,000 --> 00:00:08,480
You ready? Go. Mm-hmm. So the question is should one wear a jacket or hat or gloves during the winter?

2
00:00:10,080 --> 00:00:11,120
and

3
00:00:11,120 --> 00:00:12,800
because

4
00:00:12,800 --> 00:00:17,400
If if one wears a hat or a jacket, they're creating a version towards the cold

5
00:00:17,400 --> 00:00:29,080
Yeah, I mean there's certainly that potential and I think certainly that that's a real

6
00:00:30,600 --> 00:00:32,600
That's a real thing that really happens

7
00:00:34,360 --> 00:00:43,400
And so to some extent we have to we have to deal with that way when I was here 15 years ago in this same monastery

8
00:00:43,960 --> 00:00:46,040
This is where I spent my first year as a monk

9
00:00:46,040 --> 00:00:48,040
And

10
00:00:48,040 --> 00:00:54,040
I had to go to university every day. I was still in even as a monk. I was in university

11
00:00:56,600 --> 00:00:58,600
and

12
00:01:00,200 --> 00:01:05,320
It was darn cold even before that after I started meditating I came back to Canada

13
00:01:06,040 --> 00:01:08,760
and went back to university as a layperson

14
00:01:09,800 --> 00:01:11,800
in this same monastery

15
00:01:11,800 --> 00:01:20,520
And I remember dealing with the cold at this funny situation where I was to meet the abbot they had monk

16
00:01:21,320 --> 00:01:23,320
and we were going to Toronto together

17
00:01:24,120 --> 00:01:27,320
and so we made an appointment to meet at a specific street corner

18
00:01:28,280 --> 00:01:30,280
at a specific time

19
00:01:30,600 --> 00:01:33,080
four o'clock I think it seems to me it was four o'clock

20
00:01:34,840 --> 00:01:37,560
and at four o'clock I was there

21
00:01:38,760 --> 00:01:40,440
waiting

22
00:01:40,440 --> 00:01:42,680
You know if this is the corner I was here

23
00:01:43,880 --> 00:01:46,920
and I waited and I could see the intersection

24
00:01:48,760 --> 00:01:50,760
And I waited for a half an hour

25
00:01:51,480 --> 00:01:55,400
Getting colder. It was freezing and I was like quite

26
00:01:57,560 --> 00:01:59,560
No, quite

27
00:02:00,360 --> 00:02:02,360
Getting quite sick as a result

28
00:02:02,760 --> 00:02:06,600
But I had this interesting experience where just simply by being mindful

29
00:02:06,600 --> 00:02:09,960
I could lose the sense of cold

30
00:02:10,520 --> 00:02:12,520
You know by not acknowledging cold

31
00:02:14,440 --> 00:02:20,040
The shivering would stop and the pain and the upset of course would stop

32
00:02:20,040 --> 00:02:22,520
But also the cold would just sort of disappear

33
00:02:25,480 --> 00:02:27,320
And it entered into this sort of

34
00:02:29,080 --> 00:02:31,320
Good state where the body was comfortable

35
00:02:31,320 --> 00:02:36,280
Of course it didn't last and I ended up getting quite frustrated

36
00:02:38,520 --> 00:02:42,120
And then as I guess it's crazy and so I started walking back to the towards the monastery

37
00:02:43,160 --> 00:02:45,080
And I got I walked to the intersection

38
00:02:45,080 --> 00:02:49,480
I looked over and he was around the corner waiting in this car

39
00:02:50,440 --> 00:02:55,080
And he was just as frustrated as me or probably not as much because he was sitting in his warm car

40
00:02:55,800 --> 00:03:00,120
But he had been sitting in his warm car for the past half hours so we both were there on time

41
00:03:00,120 --> 00:03:03,240
And he scolded me and I scolded him

42
00:03:05,160 --> 00:03:07,160
And it taught us the lesson in

43
00:03:08,440 --> 00:03:10,440
Looking around the corner

44
00:03:14,200 --> 00:03:19,480
But definitely that's part of it and that was something I realized over time is that you're part of our

45
00:03:20,200 --> 00:03:22,200
Version to cold is just that in a version

46
00:03:22,760 --> 00:03:28,520
But that's not the only reason why you wear the hats. I mean and the coat and so on

47
00:03:28,520 --> 00:03:34,440
There there's a difference between being cold and making yourself sick

48
00:03:34,440 --> 00:03:36,440
You can't get sick from the cold

49
00:03:36,600 --> 00:03:44,200
But as I understand it, it reduces your immune system and can do that quite severely to the point where it you're quite likely to catch

50
00:03:44,360 --> 00:03:46,360
Whatever virus is going around quite

51
00:03:48,200 --> 00:03:55,000
It's it increases your odds of getting sick, which can be a hindrance to your practice etc etc

52
00:03:55,000 --> 00:03:59,960
Of course, you won't be able to teach you wouldn't it wouldn't be able to study for example

53
00:04:00,680 --> 00:04:04,840
When you're sick, so there's a lot of there's quite that some detriments

54
00:04:05,800 --> 00:04:07,800
detrimental effects

55
00:04:07,800 --> 00:04:09,480
um

56
00:04:09,480 --> 00:04:11,480
To the cold

57
00:04:11,480 --> 00:04:17,880
You know, I mean here in Canada. You can't just go out in the cold. You'll get really sick

58
00:04:17,880 --> 00:04:24,040
You know, so I was wondering how do you decide when to wear a hat?

59
00:04:24,920 --> 00:04:27,960
Well, you should wear a hat as a matter of course. I mean

60
00:04:29,000 --> 00:04:34,280
wearing a hat doesn't in and of itself cultivate an aversion to cold

61
00:04:36,680 --> 00:04:40,840
A version cultivates a version. It's habitual. So if you

62
00:04:40,840 --> 00:04:47,880
uh, if you don't change the habit, it will it will increase. You know a version

63
00:04:49,080 --> 00:04:51,080
will create further aversion. So

64
00:04:51,720 --> 00:04:56,040
By noting, you know, upset, upset, disliking, disliking, disliking, you know

65
00:04:56,600 --> 00:05:02,680
That's what gets rid of the aversion or when you feel colds in cold cold. If you never feel cold because you're always wearing a hat

66
00:05:02,680 --> 00:05:04,680
That's not going to create a version to cold

67
00:05:05,720 --> 00:05:07,720
It's only when you feel the cold

68
00:05:07,720 --> 00:05:12,600
And you allow yourself to to dislike it. That's what's cultivating the aversion

69
00:05:13,640 --> 00:05:15,640
Now the disliking of it

70
00:05:16,440 --> 00:05:21,640
Is what leads you to put often leads you to put the hat on, but it's not the putting the hat on. That's the problem

71
00:05:22,200 --> 00:05:25,240
It's the encouraging of the disliking

72
00:05:26,040 --> 00:05:29,640
So should you determine to wear the hat before going outside?

73
00:05:30,440 --> 00:05:31,320
So

74
00:05:31,320 --> 00:05:33,960
Otherwise, if you go outside and you realize it's cold

75
00:05:33,960 --> 00:05:38,200
Then you prone the hat then you're creating a vision aversion to the cold

76
00:05:38,840 --> 00:05:42,280
No, it's not because you put on the hat. It's because you dislike the cold

77
00:05:42,520 --> 00:05:44,760
It's the actual disliking that becomes habitual

78
00:05:45,400 --> 00:05:49,960
So if you say to yourself cold cold and there's no disliking you say, well here. I am outside. Let me put on the hat

79
00:05:51,400 --> 00:05:53,640
There's nothing to do with necessarily with aversion

80
00:05:54,920 --> 00:05:58,440
So good question, but the mechanics of it are

81
00:05:58,440 --> 00:06:02,600
Or otherwise

82
00:06:03,960 --> 00:06:05,960
It's habit. It's about what forms a habit

83
00:06:06,200 --> 00:06:11,320
So it's not that it seems like it seems like we're putting on the hat and you become more of averse to it

84
00:06:11,480 --> 00:06:16,120
And I've often said sort of that sort of thing, but technically speaking that's not what happens

85
00:06:16,680 --> 00:06:18,680
So this is why when you feel pain

86
00:06:19,800 --> 00:06:24,360
It's not a problem to adjust the adjusting the moving your body is shifting is not the problem

87
00:06:24,360 --> 00:06:31,720
But what is pushing you to to move? That's the problem. And so we encourage you not to move. We encourage you to instead

88
00:06:33,480 --> 00:06:40,920
Try to learn about it. It doesn't mean that the actual moving is causing the aversion

89
00:06:40,920 --> 00:06:51,960
So in certain cases

90
00:06:52,920 --> 00:06:54,920
There'd be no reason

91
00:06:54,920 --> 00:06:57,080
For example when you feel pain there's no reason to move

92
00:06:57,800 --> 00:06:59,800
So in most cases you don't have to move

93
00:07:00,760 --> 00:07:02,760
Here in this case even though

94
00:07:04,680 --> 00:07:06,680
Part of it is even though

95
00:07:06,680 --> 00:07:10,280
There is the potential to put on the hat out of aversion

96
00:07:10,760 --> 00:07:13,560
There's also the reason to put out the hat put on the hat

97
00:07:14,360 --> 00:07:15,400
to not get sick

98
00:07:15,640 --> 00:07:16,600
So

99
00:07:16,600 --> 00:07:21,960
Also to not get distracted because it can be very difficult to focus if you had a headache from the cold or

100
00:07:23,800 --> 00:07:28,600
You know cold we don't function very well at at sub zero temperatures, right?

101
00:07:29,320 --> 00:07:31,080
so

102
00:07:31,240 --> 00:07:33,960
And then as soon as you go inside you get a huge headache and

103
00:07:33,960 --> 00:07:40,360
And it's not a very good result especially going from hot to cold. It's it's quite hard on the body and

104
00:07:40,920 --> 00:07:42,920
That's not useful

105
00:07:42,920 --> 00:07:46,360
It's not helpful to do that. So I mean that these are excuses you can give

106
00:07:46,360 --> 00:07:51,720
It's not always the case and sometimes it's actually useful to experience all that the headaches and so on and so

107
00:07:52,360 --> 00:07:56,600
There's no hard and fast rule for the most you know for a hardcore meditator. You can put up with it

108
00:07:57,240 --> 00:07:59,240
Mahasi Sayada talks about a monk

109
00:07:59,240 --> 00:08:05,800
in one of the commentaries. I think who meditated on in the snow

110
00:08:05,800 --> 00:08:11,000
You know he just did sitting meditation while it was snowing around him which of course we hear about with the Tibet to

111
00:08:11,000 --> 00:08:13,000
Battens and so on

112
00:08:13,000 --> 00:08:15,000
meditating in the snow

113
00:08:15,400 --> 00:08:17,400
I've got that picture of me

114
00:08:17,720 --> 00:08:22,440
I'm quite a nice picture sitting in my parents patio furniture

115
00:08:22,440 --> 00:08:28,760
With the snow coming deep and I was sitting with my legs crossed and snowshoes wearing snowshoes

116
00:08:30,760 --> 00:08:33,880
That about sums it up. You can do it. I mean it was it to me

117
00:08:33,880 --> 00:08:39,240
It was sort of an experiment on my on my own part to to just experience that you know here

118
00:08:39,240 --> 00:08:43,640
I'm going to experience in monks robes, which are not very very much protection

119
00:08:44,600 --> 00:08:51,000
But to just go through it and it's quite quite freeing really to not have to avoid

120
00:08:51,000 --> 00:08:53,320
So certainly there's that

121
00:08:54,200 --> 00:08:57,720
But wearing a hat there's nothing wrong with it. It can be quite useful

122
00:08:58,440 --> 00:09:00,440
I think there's many reasons to wear it

123
00:09:00,840 --> 00:09:01,880
I mean

124
00:09:01,880 --> 00:09:06,760
But it's not like it's certainly not something you should obsess over. I have to wear a hat etc

125
00:09:07,640 --> 00:09:10,120
I don't think you have to obsess about not wearing a hat

126
00:09:10,120 --> 00:09:20,120
Just do you understand that you can at times go with that

