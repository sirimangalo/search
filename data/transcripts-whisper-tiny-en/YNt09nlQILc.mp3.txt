Hey welcome back to Ask a Monk. Next question comes from Jay who asks media addiction TV shows fiction books popular music
These things can be a distraction when meditating for sure
But do you think they are okay to enjoy in moderation? What are your thoughts on media in general?
Yeah, okay, well the the agreement is that
They are distraction from meditation. This is I think easy to understand
So easy for people to
Keep this sort of a rule during a meditation course most people. I do know people who have brought their guitars to meditation centers or
You know brought marijuana and tried to get high during their meditation course
so on but mostly we can accept that we're trying to do something very specific here and
And that these things shouldn't shouldn't play a part in that and that's really why when you read about the formation of
moral moral precepts in in the Buddha's teaching
You you don't have you don't see these in in terms of a lay
morality the rules against entertainment and so on for monks were not allowed to listen to music or
Or what shows or so on that this is something that is not
acceptable for a
Monastic but for ordinary people
There are only the five rules not to kill not to steal not to cheat not to lie and not to take drugs in alcohol
These are the five moral precepts because these five things are are obviously immoral and
Intrinsically immoral they can't be separated from an
Immoral mind
That being said my thoughts on the media. I mean the more important point than then is it allowed or is it not allowed is?
Is whether it's it's beneficial
Whether it actually brings happiness and the truth is these things don't bring peace and happiness
They don't create a state of contentment in the mind. I mean Buddhism or the or meditation is for the purpose of of learning to just just be learning to
To be here and now to exist in the present moment without having to be something or get something or
Without having to make our experience a specific
Pleasing experience something that is going to stimulate us to be able to be here and now in any situation and the truth about these things is they actually make us irritable
discontent
board
You even depressed and so on we we are finding ourselves in this world more and more
Unsatisfied more and more depressed and unhappy and frustrated and angry and
Feeling a sense of less self-worth the more we engage in this greater of this
Entertainment with greater and greater intensity
now it's it's it's become you know the
So instantaneous that we can get whatever we want and yet we're finding you know people are more and more
turning to drugs and and alcohol and even prescription medication to to
relieve the the suffering that this is causing you can
If you have any sort of awareness self awareness you can verify this for yourself that these things don't make you happy
They don't make you a more content and peaceful person
They create states of discontent the inability to sit still the inability to be happy without the stimulation that you require more and more
stimulation just to attain the same state of
of
contentment or of
pleasure
and
So I would say that that's in a Buddhist sense what makes things immoral is what do they do to you?
I mean killing is not evil
It's it's evil only in the sense that it creates suffering both for you and for the other person
Most especially for yourself because a person who is killed can theoretically be free from suffering if you know
If you're murdered, but your mind is pure you can theoretically
Be at peace with that, but a person who kills cannot the person who kills is is affecting their mind is
Perverting their mind and and is causing suffering for themselves
So that's what makes it evil that the in that sense you can say that these things are her in a sense evil
I mean they they are an addiction and really everything is like that anything that you cling to
becomes an addiction
The
There is a tendency to want to deny this and I'm sure probably I'm gonna get comments to this saying that
Oh, no, I don't agree music is something that you know brings you into this trance state and so on
and
I suppose to some extent that's that's
Perfectly true and I would agree with that that there are certain
sounds like
Chanting or even especially music as well that sends you into a trance sort of state and people say well, that's meditative
Obviously, that's not the meditation that I teach and it's kind of beside the point the point is the
Attachment to these things your addiction to them the fact that you like them even the fact that you like that trance state that state of
meditative bliss and absorption that is a hindrance to you either Buddha said even
Even trance states are our dangerous in the sense that one might become attached to them
There's nothing wrong with these states in and of themselves and they can be actually useful for meditation
The point is that that things like music have an appeal to them have create this stimulation in the mind in the brain
that gives rise to a pleasure stimulus and
And you know and as a result in addiction because you you get you get what you want and
There's the chemicals arise
But the next time when you want to get it you need more and it has to be more intense and so on as I've talked about in other videos and
As can be verified by modern biology and chemistry
so
There are distraction from meditation, but they're also a path that is is
Not really conducive to to peace happiness and freedom from suffering. So
the
use of these things in moderation is not a problem
It's not going to send you to to a bad place when you die
But it will keep you bound to to the wheel of some. Sorry. I will keep you
You know going on and on because your mind is still clinging your mind is still creating
It's still fixed on on coming back. So I mean, I think for most people in this world
That's not a bad thing in fact for many
Theistic
Religious people who have under who have come to understand Buddhism
It's quite a relief to know that you have another chance and that you can come back
But this is obviously a fairly immature sort of
Position or understanding when once you take the next step and start to realize the ramifications you think oh
Well, yeah, well, then I have to come back and do this again and maybe again and again and again and again and again
Because you don't likely remember everything and eventually you're going to forget it and fall back into a place that it is very coarse and
Deterministic like the human world and it'll be very difficult and you'll fall into the same difficulties that you've had in this life
maybe even worse
So yeah, that's a little bit off track, but that's what these things lead to. This is the
Opiate it's something that keeps us asleep keeps us
You know, it's like that it's exactly the opiate of the masses
It's something that keeps us from asking the questions that we should ask keeps us from having any
Get up and go keeps us from
Developing and improving ourselves music is a very obvious one. It's something that
You know, they have all these conspiracy theories that the entertainment industry is controlled by the government who's trying to just keep us all happy
Well, you don't have to go so far. I mean, we're doing it to ourselves where
We're it or you can go even further and say the whole universe is is based on this way. It's keeping us attached to rebirth so
our
Addiction to these things is is helping us to pass the time and allowing us to forget about the important questions like
All the age sickness and death and suffering and why these things exist and how to become free from them
Then where do we go when we die and so on?
so
From a Buddhist point of view, it doesn't really make sense to give these things and more than a basic
Appreciation if you want to listen to music. It's not a problem. It's not going to make you not a Buddhist
but
Buddhists do have an understanding that these things are addictive and
that
they can they generally lead us to waste time and
And lead to more and more suffering so unless unless that is faction
When you practice meditation you start to realize this and you start to give these things up naturally
You start to become bored of them and say that you know
There's really no happiness or peace to be gained from this and and as a result you you find yourself just being able to just
live life as it were and to be present
Without needing anything without needing any kind of stimulus or special experience
Okay, so long answer, but
Someone did ask that I talk about music. So there's my take on it and
I'm just a little bit more personal information. It's not that I'm a person who never
Enjoyed music. I was a guitarist. I had a
Less Paul deluxe
$1,200 electric guitar
I had a fairly extensive music collection and was in
Two or three different by bands
So you know music is something that was very difficult for me to let go of but
In the beginning. I was very decisive about these things. I understood
Intellectually what the teaching meant and I appreciated that and I thought that has real meaning to me
So regardless
It's kind of like this
I knew that these things were attractive to me
I knew that I liked music, but I also knew that that liking was the cause of my suffering because I was suffering
I was you know deeply
Mired in these things
They were my life and I was suffering terribly
I was I was incredibly depressed and had no meaning in my life
I felt like it was
No, it was just these feelings that come up and they're based on addiction. It's a withdrawal thing or it's it's the addictive cycle
so
That's basically how I took all of these things so many different things. I said to myself
Yeah, I like it, but I couldn't I couldn't I didn't need to convince myself to let them go
I knew that this liking was the problem the problem was not not getting the thing the problem was the liking of it in the first place
Which was causing me suffering
so
I'm not a stranger to these things and I understand where many people are coming from but
With serious meditation practice you do change the way you look at these things and for the better because you are
Obviously, we are obviously suffering when we're addicted to these things. There's no way around that and I don't think you can convince me otherwise
With the exception of those people who use it to gain a sort of a trans state and sort of look at that as being the goal is this
state of
Intense calm but even that is not permanent. It's something that is conditioned and
You know is a sort of an addiction and we'll create a dissatisfaction in your lives
It's has to do with ego and and you you can see when you practice the type of meditation that I teach that this
You know this state is inferior and there's still
The mind is still not clear and
not pure
so
No, that's how I look at it, and I'm sure there are people with other opinions, but
You know to each of their own you ask the monk. This is the monks reply. Thanks for the question. Keep practicing
