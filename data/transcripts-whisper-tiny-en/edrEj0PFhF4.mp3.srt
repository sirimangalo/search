1
00:00:00,000 --> 00:00:08,240
Is it possible to be mindful of an experience without applying a mantra?

2
00:00:08,240 --> 00:00:16,160
In other words, to be mindful of one's seeing, without seeing, seeing, seeing?

3
00:00:16,160 --> 00:00:20,640
This is a point to the problem with the word mindful.

4
00:00:20,640 --> 00:00:27,520
It's a very bad translation of the word set-beat, because of course you assume if that's

5
00:00:27,520 --> 00:00:30,400
all it is, then why do you need to use the mantra?

6
00:00:30,400 --> 00:00:37,160
But set-beat means to remember or to remind yourself.

7
00:00:37,160 --> 00:00:43,120
In this case, I like remind yourself of those, possibly not a literal translation either,

8
00:00:43,120 --> 00:00:46,560
or remembers is better.

9
00:00:46,560 --> 00:00:47,800
But what are you trying to remember?

10
00:00:47,800 --> 00:00:54,360
You're trying to remember the essential quality of the experience, the objective reality.

11
00:00:54,360 --> 00:01:03,200
You're trying to avoid any kind of extrapolation of the object.

12
00:01:03,200 --> 00:01:08,320
And to do that, you require something called Tirasanya.

13
00:01:08,320 --> 00:01:10,440
This is the proximate cause of set-beat.

14
00:01:10,440 --> 00:01:16,120
Tirasanya means the recognition of something, Tirasanya means recognition.

15
00:01:16,120 --> 00:01:19,440
Tiram means firm or strong.

16
00:01:19,440 --> 00:01:24,880
So you need to augment your sunya.

17
00:01:24,880 --> 00:01:25,880
Sunya arises anyway.

18
00:01:25,880 --> 00:01:28,880
Tirasanya means you augment it.

19
00:01:28,880 --> 00:01:31,040
You reaffirm it.

20
00:01:31,040 --> 00:01:34,480
So the augmenting has to come about.

21
00:01:34,480 --> 00:01:42,400
So the question then is, what are you doing to bring that about if not reminding yourself?

22
00:01:42,400 --> 00:01:47,360
Now, as I mentioned in the first question, as I explained in the first question, exactly

23
00:01:47,360 --> 00:01:49,080
what you're doing.

24
00:01:49,080 --> 00:01:54,000
You're bringing it back to just this is there.

25
00:01:54,000 --> 00:01:56,280
So you're keeping it there.

26
00:01:56,280 --> 00:02:00,840
So it goes along and there comes a point where the mind can interject and say, this

27
00:02:00,840 --> 00:02:01,840
is good.

28
00:02:01,840 --> 00:02:02,840
This is bad.

29
00:02:02,840 --> 00:02:03,840
This is me.

30
00:02:03,840 --> 00:02:04,840
This is mine.

31
00:02:04,840 --> 00:02:05,840
Instead, you hold it there.

32
00:02:05,840 --> 00:02:11,800
The function of mindfulness is to hold the object, to keep it from or to hold the mind

33
00:02:11,800 --> 00:02:24,320
and keep it on the object, to keep the mind from moving away from the object.

34
00:02:24,320 --> 00:02:26,520
So that's what this noting is.

35
00:02:26,520 --> 00:02:32,160
So the challenge that I give to anyone who asks this question is, can you explain a better

36
00:02:32,160 --> 00:02:36,800
way to do what we quite clearly and effectively accomplish?

37
00:02:36,800 --> 00:02:42,240
The point is that people don't want to do this strange thing of using a mantra.

38
00:02:42,240 --> 00:02:47,520
It's really kind of, it's not a very good question.

39
00:02:47,520 --> 00:02:48,520
I mean, everyone asks this.

40
00:02:48,520 --> 00:02:53,920
I get this question on again and again and again, but it's not a very good question.

41
00:02:53,920 --> 00:02:59,240
And I think it comes about because of our preconceptions and because of how we've come

42
00:02:59,240 --> 00:03:09,000
to this through sort of this tradition, the sort of intellectual understanding of meditation

43
00:03:09,000 --> 00:03:19,320
as being some kind of sort of vague sort of nothing of just sitting there and I don't

44
00:03:19,320 --> 00:03:24,080
know, watching the breath, I think, is a big one, but not really doing anything.

45
00:03:24,080 --> 00:03:29,320
But in fact, before we came to Buddhism, most people's understanding of meditation was

46
00:03:29,320 --> 00:03:30,320
a mantra.

47
00:03:30,320 --> 00:03:32,320
What is the word mantra?

48
00:03:32,320 --> 00:03:33,320
Where does it come from?

49
00:03:33,320 --> 00:03:34,880
Where have we heard this?

50
00:03:34,880 --> 00:03:35,880
It's a Hindu thing.

51
00:03:35,880 --> 00:03:39,920
You know, when we knew of meditation as a Hindu thing, we have a meditation is where you

52
00:03:39,920 --> 00:03:43,560
do all, you know, we have a mantra and what good is the mantra?

53
00:03:43,560 --> 00:03:44,720
It focuses the mind.

54
00:03:44,720 --> 00:03:51,200
It has a real definite purpose and benefit to it.

55
00:03:51,200 --> 00:03:52,760
That's what this is all about.

56
00:03:52,760 --> 00:03:58,960
It's nothing new and special, it's that, it's meditation the old way, it's old school.

57
00:03:58,960 --> 00:04:03,720
This new idea of just sitting there and zoning out, it's a very hippie thing.

58
00:04:03,720 --> 00:04:07,240
You know, when you go to these folk festivals and they think, yeah, you just become one

59
00:04:07,240 --> 00:04:08,240
with the universe.

60
00:04:08,240 --> 00:04:12,160
It's not very technical or very professional.

61
00:04:12,160 --> 00:04:16,080
Professional meditation, what we're doing is professional, that's what it is, professional

62
00:04:16,080 --> 00:04:21,360
meditation, where you are really taking this seriously and you're not taking this as some

63
00:04:21,360 --> 00:04:24,800
kind of feel-good thing.

64
00:04:24,800 --> 00:04:32,640
This is something you're going to do systematically, methodically, and it's going to have

65
00:04:32,640 --> 00:04:34,840
real results, definite results.

66
00:04:34,840 --> 00:04:37,240
It's going to be scientific.

67
00:04:37,240 --> 00:04:44,160
To be scientific, you have to be sure, it's like, why do you need a double-blind in a science

68
00:04:44,160 --> 00:04:45,160
experiment?

69
00:04:45,160 --> 00:04:51,080
Because of the potential for bias, it's very clear, it's exactly that.

70
00:04:51,080 --> 00:04:53,920
You could say, I'm just going to sit and observe things.

71
00:04:53,920 --> 00:04:56,640
Well, okay, potentially, you can become enlightened, absolutely.

72
00:04:56,640 --> 00:04:57,640
Is it possible?

73
00:04:57,640 --> 00:05:00,560
Yes, possible.

74
00:05:00,560 --> 00:05:02,560
Likely, not likely, why?

75
00:05:02,560 --> 00:05:08,360
Because the potential for bias, if you just let it go and, you know, it's like conducting

76
00:05:08,360 --> 00:05:12,720
experiments and saying, well, let's just have people come in and tell us, you know, what

77
00:05:12,720 --> 00:05:17,880
happened, you know, tell us their experiences.

78
00:05:17,880 --> 00:05:22,040
How can you do that without introducing bias?

79
00:05:22,040 --> 00:05:25,320
Even when you have just an experiment, or they say it's not enough, you need to have someone

80
00:05:25,320 --> 00:05:30,960
observing the experiment, you know, the person doing the experiment has to be watching

81
00:05:30,960 --> 00:05:36,360
the person actually administering the drugs and et cetera, you know, double-blind.

82
00:05:36,360 --> 00:05:40,880
Can I ask something related to this?

83
00:05:40,880 --> 00:05:41,880
Sure.

84
00:05:41,880 --> 00:05:47,960
Now, you want to do this during your daily tasks as well, right, you're just sort of always

85
00:05:47,960 --> 00:05:48,960
noting what you're doing.

86
00:05:48,960 --> 00:05:54,000
But if you're doing something very detail oriented that has many, many steps, when

87
00:05:54,000 --> 00:05:59,160
I've tried to do this with very detail oriented work, you know, it's kind of frantic,

88
00:05:59,160 --> 00:06:03,960
you know, in your mind, to try to keep up with every little piece of a detail oriented

89
00:06:03,960 --> 00:06:10,200
task, would you just kind of consolidate things so that you're not in your mind saying,

90
00:06:10,200 --> 00:06:15,360
you know, flipping, turning, you know, just really fast in your mind as you're doing

91
00:06:15,360 --> 00:06:16,360
something?

92
00:06:16,360 --> 00:06:25,960
Well, it has a potential to change your, you can think of it as we don't always get angry,

93
00:06:25,960 --> 00:06:28,440
you know, or greedy, you know, but it's not all defiled.

94
00:06:28,440 --> 00:06:36,560
Our mind is not constantly defiled, but it slips into us sort of a stream of defilement.

95
00:06:36,560 --> 00:06:46,800
So it shifts, so you'll find, you know, you are generally angry, you know, that's why

96
00:06:46,800 --> 00:06:52,840
you can be angry for a period of time, right, because it's, it's that period of anger.

97
00:06:52,840 --> 00:06:58,680
Likewise, you don't have to be mindful, you don't have to remind yourself every moment

98
00:06:58,680 --> 00:07:02,360
in order to be mindful, in order to have set the arise.

99
00:07:02,360 --> 00:07:05,280
All we're doing is nudging it in that direction.

100
00:07:05,280 --> 00:07:08,400
So there's an explanation of why it doesn't have to be every moment.

101
00:07:08,400 --> 00:07:13,600
The point being, you can do general acknowledgement and that nudges you in the direction

102
00:07:13,600 --> 00:07:16,760
of wholesomeness, you know, that's enough.

103
00:07:16,760 --> 00:07:23,520
So when I said what we're trying to do is to stop that moment of reactivity, right, that

104
00:07:23,520 --> 00:07:31,760
moment is every, it's way too fast for us to do every moment, but by nudging ourselves

105
00:07:31,760 --> 00:07:39,640
in that direction by catching, like in a pulse, it, it inclines you in that direction.

106
00:07:39,640 --> 00:07:43,160
This is why you'll be mindful and you're still beginning angry because it's very, you

107
00:07:43,160 --> 00:07:49,760
can't catch them all, but by nudging in that direction, the anger will subside or the anger,

108
00:07:49,760 --> 00:07:53,640
the greed, the bad stuff will subside and you'll be pulling yourself in that direction

109
00:07:53,640 --> 00:07:56,200
until all the minds are in that way.

110
00:07:56,200 --> 00:08:05,200
All you need is, or all you can do is occasionally, but occasionally we mean like once

111
00:08:05,200 --> 00:08:07,280
a second kind of thing.

112
00:08:07,280 --> 00:08:10,520
So Mahasi Sayada actually puts a definite value there.

113
00:08:10,520 --> 00:08:13,600
He says once a second is a good acknowledgement.

114
00:08:13,600 --> 00:08:14,600
Hello?

115
00:08:14,600 --> 00:08:17,000
Can you hear me?

116
00:08:17,000 --> 00:08:32,200
No.

