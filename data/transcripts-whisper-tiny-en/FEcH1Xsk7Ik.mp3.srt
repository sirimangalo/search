1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapanda.

2
00:00:05,000 --> 00:00:13,000
Today, we continue on with verse 159, which reads as follows.

3
00:00:13,000 --> 00:00:19,000
Attan and Jaitataka yirah, yatanya monosacity.

4
00:00:19,000 --> 00:00:22,000
So don't know what the Dhammata.

5
00:00:22,000 --> 00:00:26,000
Atahikirah do the moh.

6
00:00:26,000 --> 00:00:31,000
Which means...

7
00:00:31,000 --> 00:00:46,000
If one makes one's self, if one does to one's self as one instructs others,

8
00:00:46,000 --> 00:00:54,000
then indeed such a well-trained one should train other others.

9
00:00:54,000 --> 00:01:00,000
For indeed difficult, the training of the...it is the self.

10
00:01:00,000 --> 00:01:04,000
It is the self indeed that is difficult to train.

11
00:01:04,000 --> 00:01:08,000
Atahikirah do the moh.

12
00:01:08,000 --> 00:01:13,000
Dhammata is training.

13
00:01:13,000 --> 00:01:17,000
Su dantat, one who is well-trained.

14
00:01:17,000 --> 00:01:21,000
Dhammata makes other people train.

15
00:01:21,000 --> 00:01:25,000
Do the moh difficult to train.

16
00:01:25,000 --> 00:01:28,000
The self is difficult to train.

17
00:01:28,000 --> 00:01:29,000
Dhammati.

18
00:01:29,000 --> 00:01:34,000
It's an important verb in Buddhism.

19
00:01:34,000 --> 00:01:40,000
The Buddha said to be a Buddhist, a dharma, a sārati.

20
00:01:40,000 --> 00:01:46,000
A trainer is a...

21
00:01:46,000 --> 00:01:55,000
a trainer of those who are trainable.

22
00:01:55,000 --> 00:02:06,000
So this was told in relation to...in regards to a story of a monk called Padānikatisa.

23
00:02:06,000 --> 00:02:08,000
Padānam means effort.

24
00:02:08,000 --> 00:02:10,000
So Padānikah is one who has effort.

25
00:02:10,000 --> 00:02:14,000
This self is his name, but they call them Padānikah.

26
00:02:14,000 --> 00:02:16,000
It's an ironic name.

27
00:02:16,000 --> 00:02:22,000
It's a good kind of evidence that there might be a sense of humor among the...

28
00:02:22,000 --> 00:02:29,000
among the compilers of the Pali again, or a sense of humor at the time of the Buddha.

29
00:02:29,000 --> 00:02:33,000
At the very least, it's a sign of...

30
00:02:33,000 --> 00:02:39,000
It's a sense of...a sense of shame, I suppose.

31
00:02:39,000 --> 00:02:43,000
It's how this monk is remembered, which is unfortunate,

32
00:02:43,000 --> 00:02:48,000
because the story is actually quite unflattering, in regards to effort, especially.

33
00:02:48,000 --> 00:02:59,000
So the story goes that he received a meditation object, a practice of meditation,

34
00:02:59,000 --> 00:03:03,000
from the Buddha, and together with a large group of monks,

35
00:03:03,000 --> 00:03:07,000
went off into the forest and entered into the rains.

36
00:03:07,000 --> 00:03:11,000
So right now we're in the rainy season, and this is a three-month period

37
00:03:11,000 --> 00:03:14,000
where we don't go anywhere.

38
00:03:14,000 --> 00:03:19,000
We can go, but we can't stay overnight, normally.

39
00:03:19,000 --> 00:03:23,000
We try to stay put for three months, and focus on our practice.

40
00:03:23,000 --> 00:03:26,000
So that's what they did.

41
00:03:26,000 --> 00:03:33,000
And when they were in the forest, this Padānikatisa,

42
00:03:33,000 --> 00:03:47,000
he says to the other monks, we've received a meditation from the Buddha himself.

43
00:03:47,000 --> 00:03:52,000
Let us not...let us be a pamata, let us not be heedless.

44
00:03:52,000 --> 00:04:05,000
Samana Damangarota, do the duties of a samana, of a shaman, or a reckless, of an ascetic.

45
00:04:05,000 --> 00:04:12,000
And having said this, he went off into his room and laid down to sleep.

46
00:04:12,000 --> 00:04:16,000
And the other monks, in the first watch of the night,

47
00:04:16,000 --> 00:04:20,000
they did walking and sitting meditation as normal.

48
00:04:20,000 --> 00:04:28,000
So understand the watches, we have a 12-hour period.

49
00:04:28,000 --> 00:04:31,000
The day has 12 hours, the night has 12 hours.

50
00:04:31,000 --> 00:04:37,000
So from 6 a.m. to 6 p.m. is daytime, from 6 p.m. to 6 a.m. is nighttime.

51
00:04:37,000 --> 00:04:43,000
So the first watch is 6 p.m. until 10 p.m. approximately.

52
00:04:43,000 --> 00:04:47,000
And during that time, they did intensive meditation practice.

53
00:04:47,000 --> 00:04:50,000
They really put out effort.

54
00:04:50,000 --> 00:04:56,000
And then there's the second watch of the night from 10 a.m. 10 p.m. until 2 a.m.

55
00:04:56,000 --> 00:05:01,000
They went back into the residence, the dwelling,

56
00:05:01,000 --> 00:05:05,000
or their dwellings, and laid down to go to sleep.

57
00:05:05,000 --> 00:05:08,000
Mindfully.

58
00:05:08,000 --> 00:05:11,000
Right as they were lying down, or after they had just fallen asleep,

59
00:05:11,000 --> 00:05:15,000
they said their monk having slept soundly, wakes up,

60
00:05:15,000 --> 00:05:18,000
goes into the residence, the other residences,

61
00:05:18,000 --> 00:05:20,000
and sees the monks lying down asleep.

62
00:05:20,000 --> 00:05:25,000
And he bangs a drum, or what does he do?

63
00:05:25,000 --> 00:05:33,000
He just went and said to them,

64
00:05:33,000 --> 00:05:35,000
he said, hey, what did you come here?

65
00:05:35,000 --> 00:05:38,000
He's thinking to yourself, we'll come here to sleep.

66
00:05:38,000 --> 00:05:40,000
He says, get it back out there,

67
00:05:40,000 --> 00:05:43,000
and devote yourself to the meditation.

68
00:05:43,000 --> 00:05:46,000
And so they all wake up, and they're kind of groggy,

69
00:05:46,000 --> 00:05:48,000
and of course, being woken up from sleep

70
00:05:48,000 --> 00:05:50,000
is worse than not having slept at all,

71
00:05:50,000 --> 00:05:54,000
because their days didn't confuse them.

72
00:05:54,000 --> 00:05:56,000
They went out, and they tried their best

73
00:05:56,000 --> 00:05:58,000
and stumbling around for the four hours

74
00:05:58,000 --> 00:06:01,000
when they'd normally be asleep.

75
00:06:01,000 --> 00:06:03,000
And in the third watch of the night,

76
00:06:03,000 --> 00:06:06,000
exhausted again, they laid down to sleep.

77
00:06:06,000 --> 00:06:09,000
And again, this monk having told them

78
00:06:09,000 --> 00:06:11,000
what to do went back to sleep,

79
00:06:11,000 --> 00:06:13,000
as soon as they lay down to sleep,

80
00:06:13,000 --> 00:06:15,000
or sometime in the third watch,

81
00:06:15,000 --> 00:06:17,000
the night he gets up again,

82
00:06:17,000 --> 00:06:19,000
and yells at them again,

83
00:06:19,000 --> 00:06:20,000
and tells them to go out there.

84
00:06:20,000 --> 00:06:22,000
So they didn't get really to sleep at all.

85
00:06:29,000 --> 00:06:32,000
And after, of course,

86
00:06:32,000 --> 00:06:34,000
this had a badani katisa got a lot of sleep.

87
00:06:34,000 --> 00:06:37,000
He slept through all three watches, basically.

88
00:06:37,000 --> 00:06:39,000
Being interrupted having to get up

89
00:06:39,000 --> 00:06:41,000
and struck the other monks.

90
00:06:44,000 --> 00:06:46,000
So, and he did this repeatedly,

91
00:06:46,000 --> 00:06:48,000
basically every night it sounds like.

92
00:06:48,000 --> 00:06:52,000
And the monks after a while,

93
00:06:52,000 --> 00:06:55,000
they weren't getting anywhere with their meditation,

94
00:06:55,000 --> 00:06:57,000
because they were constantly being disturbed

95
00:06:57,000 --> 00:07:00,000
and disoriented by lack of sleep,

96
00:07:00,000 --> 00:07:07,000
but also this sort of constant disruption

97
00:07:07,000 --> 00:07:10,000
of their ordinary sleep cycle.

98
00:07:12,000 --> 00:07:13,000
And they thought to themselves,

99
00:07:13,000 --> 00:07:16,000
wow, this is really a tough practice.

100
00:07:16,000 --> 00:07:18,000
Our teacher must be,

101
00:07:18,000 --> 00:07:21,000
the elder must be a really,

102
00:07:21,000 --> 00:07:23,000
must be full of energy,

103
00:07:23,000 --> 00:07:26,000
and quite energetic if he's able to do this.

104
00:07:26,000 --> 00:07:28,000
Let's go watch him.

105
00:07:28,000 --> 00:07:29,000
And so they went,

106
00:07:29,000 --> 00:07:30,000
and they, in this first watch,

107
00:07:30,000 --> 00:07:32,000
the night they went to look into his room

108
00:07:32,000 --> 00:07:35,000
and found that he was sleeping.

109
00:07:35,000 --> 00:07:37,000
And they watched him when they realized

110
00:07:37,000 --> 00:07:40,000
that he was sleeping throughout the whole night.

111
00:07:40,000 --> 00:07:44,000
They felt like they felt betrayed, of course.

112
00:07:44,000 --> 00:07:46,000
And they said,

113
00:07:46,000 --> 00:07:48,000
like our teachers teaching something

114
00:07:48,000 --> 00:07:51,000
that he himself doesn't even practice.

115
00:07:51,000 --> 00:07:53,000
It's empty words, they said.

116
00:07:53,000 --> 00:07:56,000
Duchya, what's it called?

117
00:07:56,000 --> 00:07:58,000
round.

118
00:08:03,000 --> 00:08:08,600
Dude Channelung,

119
00:08:09,000 --> 00:08:11,000
empty,

120
00:08:11,000 --> 00:08:13,000
our lack.

121
00:08:13,000 --> 00:08:18,000
Empty sounds and empty words.

122
00:08:18,000 --> 00:08:23,000
And so as a result of that,

123
00:08:23,000 --> 00:08:25,000
they felt lost,

124
00:08:25,000 --> 00:08:28,920
And they spent the rains retreat, not really gaining much out of it.

125
00:08:28,920 --> 00:08:32,280
At the end of the rains, they left and went back to see the Buddha and the Buddha said,

126
00:08:32,280 --> 00:08:34,040
hey, how did it go?

127
00:08:34,040 --> 00:08:37,440
Did you were you heedful, and did you perform?

128
00:08:37,440 --> 00:08:39,080
Did you meditate?

129
00:08:39,080 --> 00:08:44,440
Well, and they told him the story, and he said, oh, this monk has always been like this.

130
00:08:44,440 --> 00:08:46,600
And he told the story from the Jataka.

131
00:08:46,600 --> 00:08:54,280
This also appears in the Jataka where he says, once this monk was a rooster, and he

132
00:08:54,280 --> 00:08:57,880
was a rooster that had never been really taught when to crow.

133
00:08:57,880 --> 00:09:03,040
And so he would crowed all day and all night because he had no sense.

134
00:09:03,040 --> 00:09:09,080
One of the Jatakas.

135
00:09:09,080 --> 00:09:14,360
And then he told, then he told this verse, he said, look, this is the case, someone before

136
00:09:14,360 --> 00:09:15,360
teaching.

137
00:09:15,360 --> 00:09:18,840
It's not really proper, it's obviously not proper.

138
00:09:18,840 --> 00:09:26,480
If you haven't well developed yourself to go out and teach people, because it's difficult.

139
00:09:26,480 --> 00:09:32,360
What is really difficult is training yourself, training someone else while it's easy to

140
00:09:32,360 --> 00:09:36,160
tell other people what to do.

141
00:09:36,160 --> 00:09:37,800
And so let's talk about that a little bit.

142
00:09:37,800 --> 00:09:42,520
That's the obvious lesson here when we talk about how this relates to our practice is

143
00:09:42,520 --> 00:09:45,240
regards to the difference between teaching and practicing.

144
00:09:45,240 --> 00:09:50,200
But first, there's another point that also relates to our practice.

145
00:09:50,200 --> 00:09:58,360
It's in regards to, hey, what is proper practice, sleeping, waking, being hateful, dedicating

146
00:09:58,360 --> 00:10:01,280
to yourself to your meditation?

147
00:10:01,280 --> 00:10:02,720
What's the deal with sleep here?

148
00:10:02,720 --> 00:10:05,080
Was this monk wrong in his teachings?

149
00:10:05,080 --> 00:10:10,960
Anyway, that relates to the second part.

150
00:10:10,960 --> 00:10:17,880
And yes, the question of what's the right amount of effort?

151
00:10:17,880 --> 00:10:27,760
So the Buddha taught, generally speaking, for intensive meditator to try and get to the

152
00:10:27,760 --> 00:10:35,280
point where you can manage on four hours of sleep, manage only to sleep during the middle

153
00:10:35,280 --> 00:10:37,080
watch of the night.

154
00:10:37,080 --> 00:10:41,400
And that's not really a theoretical thing.

155
00:10:41,400 --> 00:10:45,800
You can see how that plays out in practice for someone who's undertaking intensive meditation

156
00:10:45,800 --> 00:10:46,800
practice.

157
00:10:46,800 --> 00:10:50,400
Four hours of sleep is really optimal.

158
00:10:50,400 --> 00:10:53,240
In the beginning, it's difficult.

159
00:10:53,240 --> 00:10:59,520
And that's really the most important point here is not exactly that this monk was advocating

160
00:10:59,520 --> 00:11:02,960
not to sleep, because four hours is optimal.

161
00:11:02,960 --> 00:11:11,520
But for someone who's truly in the zone, like really on a roll and in a meditative state,

162
00:11:11,520 --> 00:11:13,040
they don't really need to sleep at all.

163
00:11:13,040 --> 00:11:18,240
It's possible for them to go without sleep for a long period of time.

164
00:11:18,240 --> 00:11:19,840
And this is also something you see.

165
00:11:19,840 --> 00:11:26,000
You hear stories about it, but you also have examples of people who continue to practice

166
00:11:26,000 --> 00:11:28,080
day and night.

167
00:11:28,080 --> 00:11:36,080
So the point is that they've developed and they've cultivated that.

168
00:11:36,080 --> 00:11:40,680
And so really, from my point of view, the two problems, first of all, that this monk

169
00:11:40,680 --> 00:11:50,320
was himself not a very energetic fellow, but also that there was no buildup to it.

170
00:11:50,320 --> 00:11:51,320
Right?

171
00:11:51,320 --> 00:11:56,680
And these monks, seemingly coming fresh off the boat, they hadn't really done much meditation

172
00:11:56,680 --> 00:11:59,520
or intensive meditation of this sort.

173
00:11:59,520 --> 00:12:05,360
And then to just dump them into a practice of not sleeping at all, certainly not going

174
00:12:05,360 --> 00:12:07,560
to have positive effects, obviously.

175
00:12:07,560 --> 00:12:12,760
And then worse than that, he was waking them up in the middle of the night, in the middle

176
00:12:12,760 --> 00:12:22,320
of their sleep pattern, which is disturbing.

177
00:12:22,320 --> 00:12:27,560
And to some extent, and most especially with the beginner meditator, you want to create

178
00:12:27,560 --> 00:12:33,120
a sense of comfort and routine.

179
00:12:33,120 --> 00:12:38,800
You want things to go smoothly, and so it's more about reducing the amount that you need

180
00:12:38,800 --> 00:12:44,840
to sleep as you start to become more energetic and as you start to become more focused.

181
00:12:44,840 --> 00:12:51,200
And really this whole question about how much you should sleep is very much strongly

182
00:12:51,200 --> 00:12:54,680
dependent on your state of mind, right?

183
00:12:54,680 --> 00:12:58,920
And your state of body, if you're exercising a lot, your body's just going to shut down.

184
00:12:58,920 --> 00:13:03,320
But more importantly, if your mind is stressed, if your mind is stressed, you'll find

185
00:13:03,320 --> 00:13:10,960
it impossible to stay alert, to stay focused because you're taxing your system.

186
00:13:10,960 --> 00:13:20,760
So such an extreme, an ordinary life living in a complicated society with all of our machines

187
00:13:20,760 --> 00:13:29,040
and computers and so on, it's not easy to get into this state where you're able to

188
00:13:29,040 --> 00:13:36,920
stay focused, stay awake, stay alert.

189
00:13:36,920 --> 00:13:44,440
So it relates to our practice in terms of thinking about how much sleep we need and having

190
00:13:44,440 --> 00:13:54,200
a sense of the potential goodness involved in listening, involved in this practice of getting

191
00:13:54,200 --> 00:14:02,360
to the point where you need less sleep, certainly as someone who's really progressed in

192
00:14:02,360 --> 00:14:09,320
the meditation practice, it needs a lot less sleep as a result.

193
00:14:09,320 --> 00:14:15,280
So in that sense, it's not exactly wrong with this moment was teaching in the sense

194
00:14:15,280 --> 00:14:20,360
of advocating less sleep, of course, he went about it all wrong.

195
00:14:20,360 --> 00:14:21,880
And that leads to the second part.

196
00:14:21,880 --> 00:14:25,480
So the question here, there's some questions involved.

197
00:14:25,480 --> 00:14:34,160
First the first question is, can someone who's never practiced actually instruct others?

198
00:14:34,160 --> 00:14:42,040
And it's hard to believe, really, that there's another story, it's not this story, but

199
00:14:42,040 --> 00:14:47,000
there's a story of a monk who had never really practiced at all, or maybe he hadn't

200
00:14:47,000 --> 00:14:52,120
gotten good results, and yet all of his students became enlightened.

201
00:14:52,120 --> 00:14:54,480
And that's hard to believe.

202
00:14:54,480 --> 00:15:00,720
And so there's more to this story than just someone teaching but not practicing.

203
00:15:00,720 --> 00:15:05,320
There's the fact that someone who doesn't practice or hasn't practiced is going to teach

204
00:15:05,320 --> 00:15:07,320
his students all wrong.

205
00:15:07,320 --> 00:15:10,320
I mean, this guy went about it all wrong.

206
00:15:10,320 --> 00:15:15,120
He'd heard, I guess, that one has to put an effort and be vigilant and he may be heard

207
00:15:15,120 --> 00:15:19,200
about monks who didn't sleep at all, and so he thought, well, that's what my students

208
00:15:19,200 --> 00:15:23,520
should do, and not having any experience in the training of the mind himself.

209
00:15:23,520 --> 00:15:28,720
He didn't have any sense of the gradual progression involved in that practice.

210
00:15:28,720 --> 00:15:37,040
So the idea that someone who hadn't practiced could adequately teach their students, and

211
00:15:37,040 --> 00:15:43,600
they'll put his last words about how difficult it is to teach others.

212
00:15:43,600 --> 00:15:48,560
I think it actually points to something else, because for this guy, I think it was quite

213
00:15:48,560 --> 00:15:49,880
difficult to teach others.

214
00:15:49,880 --> 00:15:56,320
I think it was near impossible for him to actually help other people.

215
00:15:56,320 --> 00:16:01,680
It's easy, I think, in regards to this story, it's easy to give advice.

216
00:16:01,680 --> 00:16:07,800
It's much harder to give advice that actually helps your students.

217
00:16:07,800 --> 00:16:16,320
Anyone can give advice.

218
00:16:16,320 --> 00:16:40,840
But the other question here is of the question of the difficulty in teaching yourself

219
00:16:40,840 --> 00:16:42,480
versus teaching others.

220
00:16:42,480 --> 00:16:47,760
And I think this is more of a problem for people who are on the path.

221
00:16:47,760 --> 00:16:50,840
Someone who has practiced and has gained results.

222
00:16:50,840 --> 00:16:54,520
It's easy to get distracted teaching others.

223
00:16:54,520 --> 00:16:58,200
And I don't think that's really what this story is talking about, because this guy

224
00:16:58,200 --> 00:17:02,520
went about it all wrong and clearly didn't have a good understanding of how meditation

225
00:17:02,520 --> 00:17:03,920
practice works.

226
00:17:03,920 --> 00:17:07,160
He ruined these other monks' meditation.

227
00:17:07,160 --> 00:17:11,960
But I would really agree with what the Buddha says at the end about how easy it is, but

228
00:17:11,960 --> 00:17:16,280
how hard it is, what's really hard is teaching yourself.

229
00:17:16,280 --> 00:17:21,640
It's quite easy for someone who does have positive results and meaningful results in

230
00:17:21,640 --> 00:17:24,560
the practice to teach others.

231
00:17:24,560 --> 00:17:29,040
It's easy to give advice and say, hey, do this, do that.

232
00:17:29,040 --> 00:17:32,600
And it's quite a distraction on the path.

233
00:17:32,600 --> 00:17:41,200
And so there's two cautions here in regards to how this relates to our practice as meditators.

234
00:17:41,200 --> 00:17:54,720
The first one is a strong encouragement to never, ever, ever teach another person unless

235
00:17:54,720 --> 00:18:00,440
you yourself have gained something from the practice, because the potential for you to

236
00:18:00,440 --> 00:18:06,280
teach it all wrong is incredibly great, is more likely.

237
00:18:06,280 --> 00:18:14,440
I've seen teachers and heard about teachers who are of this sort, whose students get

238
00:18:14,440 --> 00:18:19,080
on the wrong path, because their teacher has never gained any understanding of how the practice

239
00:18:19,080 --> 00:18:22,680
works for themselves.

240
00:18:22,680 --> 00:18:31,120
But the second caution is that once you do get results, don't stop and don't get sidetracked

241
00:18:31,120 --> 00:18:36,040
into making your life all about teaching others.

242
00:18:36,040 --> 00:18:43,160
Our lives as Buddhists should be very much about our own practice.

243
00:18:43,160 --> 00:18:46,440
And you can go on and on and on teaching others.

244
00:18:46,440 --> 00:18:51,480
But what you end up with is a lot of people practicing on a very shallow level, because

245
00:18:51,480 --> 00:18:58,960
no one ever gets, or will you never get, to understand the teachings on a deeper level.

246
00:18:58,960 --> 00:19:06,200
You have to push on yourself, work for your own salvation.

247
00:19:06,200 --> 00:19:09,600
And so this is really a problem for those who are on the path.

248
00:19:09,600 --> 00:19:15,840
Of course, for someone who has finished the path, who has become pulley and night, and

249
00:19:15,840 --> 00:19:21,440
they have nothing to do more for themselves, and often it can happen that they do spend

250
00:19:21,440 --> 00:19:25,120
a lot of their time helping others.

251
00:19:25,120 --> 00:19:33,400
Quite often they're certainly most content not to help anyone, but they often find themselves

252
00:19:33,400 --> 00:19:42,400
helping people much of their time, because they're such a demand on their, they're in

253
00:19:42,400 --> 00:19:53,200
such high demand for their greatness, for their ability and understanding wisdom and so on.

254
00:19:53,200 --> 00:19:58,720
So two aspects, first the caution and the importance of right understanding in regards

255
00:19:58,720 --> 00:20:04,680
to sleep, in regards to putting out right effort, so let's be clear, the Buddha was

256
00:20:04,680 --> 00:20:12,160
advocating intensive practice for 20 hours a day.

257
00:20:12,160 --> 00:20:20,240
And sometimes more, practicing throughout the day and the night, at times without sleep.

258
00:20:20,240 --> 00:20:22,040
But it has to be done right.

259
00:20:22,040 --> 00:20:28,640
And the second thing is in regards to teaching, which is really the primary lesson of

260
00:20:28,640 --> 00:20:29,640
this.

261
00:20:29,640 --> 00:20:34,680
Don't get caught up in teaching, certainly don't think about teaching others until you

262
00:20:34,680 --> 00:20:39,560
yourself have settled yourself in what's right.

263
00:20:39,560 --> 00:20:47,920
And even then, teaching is fine, it's good, but it's not something that should take over

264
00:20:47,920 --> 00:20:53,160
your life to the point that you stop practicing yourself.

265
00:20:53,160 --> 00:21:23,000
So another demo part of verse, thank you all for tuning in, I'll show you all the best.

