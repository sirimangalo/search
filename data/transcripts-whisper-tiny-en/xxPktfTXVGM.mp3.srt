1
00:00:00,000 --> 00:00:06,000
If the scriptures were preserved in memory for so long before written down,

2
00:00:06,000 --> 00:00:13,000
is it possible that they could have changed from what the Buddha originally said even slightly?

3
00:00:13,000 --> 00:00:18,000
Yes, very possible indeed.

4
00:00:18,000 --> 00:00:22,000
This isn't so much of a concern.

5
00:00:22,000 --> 00:00:31,000
It obviously is a concern in any teaching of this sort because...

6
00:00:31,000 --> 00:00:40,000
...it's like the sound effects, no, because none of us are Buddha, no.

7
00:00:40,000 --> 00:00:47,000
So no matter how well any of us teaches,

8
00:00:47,000 --> 00:00:51,000
we'll never be able to teach the same way as the Buddha taught,

9
00:00:51,000 --> 00:00:56,000
unless we go to the length and become a Buddha ourselves.

10
00:00:56,000 --> 00:01:00,000
So we do have to rely on the scriptures,

11
00:01:00,000 --> 00:01:07,000
the scriptures being, we do have to rely on the words of the Buddha.

12
00:01:07,000 --> 00:01:12,000
But why I say it's not such a big problem,

13
00:01:12,000 --> 00:01:17,000
it's because Buddhism is in faith-based.

14
00:01:17,000 --> 00:01:24,000
It's experience-based, or it's realized based on realization in the here and now.

15
00:01:24,000 --> 00:01:34,000
So the whole idea of whether something might have been added or subtracted from the teachings,

16
00:01:34,000 --> 00:01:41,000
doesn't take away from the fact that the core of the Buddha's teaching is still there.

17
00:01:41,000 --> 00:01:51,000
I mean, people are always nitpicking about the sorts of things that were added later and made up and so on.

18
00:01:51,000 --> 00:01:58,000
What I will say is I'm skeptical about most of the claims people have of parts of the Buddha's teaching,

19
00:01:58,000 --> 00:02:00,000
having been made up.

20
00:02:00,000 --> 00:02:08,000
And there was a good example in this discussion we had about

21
00:02:08,000 --> 00:02:14,000
the role or the Bhikuni, the arising of the female monk order.

22
00:02:14,000 --> 00:02:19,000
And some of my views can be found there on our question and answer for them.

23
00:02:19,000 --> 00:02:29,000
Because people are very easy for us to jump to conclusions if we want to make that conclusion.

24
00:02:29,000 --> 00:02:33,000
We're taught to be skeptical in secular society.

25
00:02:33,000 --> 00:02:36,000
While we're taught to different things, some people are taught to just believe everything,

26
00:02:36,000 --> 00:02:39,000
some people are taught to be skeptical.

27
00:02:39,000 --> 00:02:45,000
The people who come naturally to Buddhism are those who have a good dose of skepticism.

28
00:02:45,000 --> 00:02:51,000
The problem with skepticism is it can be as blind as faith.

29
00:02:51,000 --> 00:02:58,000
If you're skeptical about everything, it can actually get in the way of honest inquiry.

30
00:02:58,000 --> 00:03:15,000
Because you become arrogant and dogmatic and attached to your views just as people who are faithful will do.

31
00:03:15,000 --> 00:03:25,000
So as a result of this skepticism, a lot of modern commentators and modern Buddhist leaders

32
00:03:25,000 --> 00:03:29,000
will say things like this suit does, made up for that suit does, made up for this.

33
00:03:29,000 --> 00:03:31,000
The Buddha didn't actually say this.

34
00:03:31,000 --> 00:03:36,000
And then they have this whole, there's even people who set up this whole order of the teachings

35
00:03:36,000 --> 00:03:38,000
where this one came later than this one.

36
00:03:38,000 --> 00:03:43,000
This group is later, this group is earlier.

37
00:03:43,000 --> 00:03:56,000
I'm talking about things that are 2,500 years ago. It's a little bit dangerous thing to do.

38
00:03:56,000 --> 00:03:59,000
And it's dangerous territory.

39
00:03:59,000 --> 00:04:10,000
And from what I've seen, at least many of the times where people think they have conclusive proof that this or that discourse was made up later.

40
00:04:10,000 --> 00:04:20,000
Just a simple examination shows that it doesn't pass any standard of scientific inquiry.

41
00:04:20,000 --> 00:04:25,000
And some of the examples we talked about on the question and answer form, I'm going to go into that.

42
00:04:25,000 --> 00:04:28,000
I'm not going to go into it again.

43
00:04:28,000 --> 00:04:38,000
But what I wanted to say is that you can verify for yourself whether the teachings in the Deepika are the teachings of the Buddha.

44
00:04:38,000 --> 00:04:46,000
And this is because the meaning of the word Buddha is one who knows, one who knows the truth.

45
00:04:46,000 --> 00:04:56,000
So regardless of whether the historic Buddha was perfectly enlightened as we claim him to be, whether he really knew the truth,

46
00:04:56,000 --> 00:05:05,000
what we're claiming in Buddhism is that this is the truth that can be personally realized by you here.

47
00:05:05,000 --> 00:05:11,000
And now if you put it into, if you subject it to honest inquiry.

48
00:05:11,000 --> 00:05:25,000
And so rather than having a definite answer as to whether the teachings that we have are the teachings of,

49
00:05:25,000 --> 00:05:31,000
it doesn't really matter whether the teachings of the historic Buddha, it matters whether they are the truth,

50
00:05:31,000 --> 00:05:41,000
whether they are truly going to lead us to the truth, to peace, happiness, and freedom from suffering.

51
00:05:41,000 --> 00:05:45,000
And that's what we come to see through the meditation.

52
00:05:45,000 --> 00:05:49,000
We come to figure out for ourselves whether this is the truth.

53
00:05:49,000 --> 00:05:58,000
And all you need to do is give it honest inquiry and put it into practice with an open mind.

54
00:05:58,000 --> 00:06:01,000
It's open mindedness is much more important than skepticism.

55
00:06:01,000 --> 00:06:08,000
It's much more important to have an open and a clear mind and to just doubt everything.

56
00:06:08,000 --> 00:06:12,000
In fact, I would give everyone the benefit of the doubt.

57
00:06:12,000 --> 00:06:22,000
Every conclusion anyone ever comes to should be given 50-50% probability,

58
00:06:22,000 --> 00:06:25,000
which means you're not skeptical of it.

59
00:06:25,000 --> 00:06:29,000
It may be true and it may be false. And then you start to investigate.

60
00:06:29,000 --> 00:06:32,000
First you look at it, is it worth investigating?

61
00:06:32,000 --> 00:06:35,000
If it's not worth investigating, don't investigate it.

62
00:06:35,000 --> 00:06:39,000
If it's worth investigating, investigate and then you slowly adjust it.

63
00:06:39,000 --> 00:06:44,000
So when we practice the Buddha's teaching, we're slowly...

64
00:06:44,000 --> 00:06:47,000
Well, we can slowly adjust it in a positive way.

65
00:06:47,000 --> 00:06:52,000
We come to see that what the Buddha said or what is written in the Topitika actually

66
00:06:52,000 --> 00:06:54,000
and does relate directly to reality.

67
00:06:54,000 --> 00:06:59,000
It does correspond with our experiences of reality.

68
00:06:59,000 --> 00:07:05,000
And so we start to add percentage until it gets to like 80 or 90%.

69
00:07:05,000 --> 00:07:09,000
And then when you have the realization, say, of Nibbana,

70
00:07:09,000 --> 00:07:12,000
let's say once you have the realization of Nibbana,

71
00:07:12,000 --> 00:07:14,000
then it becomes 100%.

72
00:07:14,000 --> 00:07:18,000
At that point, that's why one is called a Sotapana because it's unshakable.

73
00:07:18,000 --> 00:07:23,000
One has realized the truth, the core for oneself.

74
00:07:23,000 --> 00:07:36,000
And so there's no need to rely on faith or on any kind of faith in the likelihood of the Topitika.

75
00:07:36,000 --> 00:07:39,000
Because you know what is the truth.

76
00:07:39,000 --> 00:07:44,000
And so you're able to filter out what is not the truth.

77
00:07:44,000 --> 00:07:48,000
That being said, the Topitika is an amazing body of literature.

78
00:07:48,000 --> 00:07:59,000
I don't think there's anything, any particular fault that I've found people can attribute to it reasonably.

79
00:07:59,000 --> 00:08:04,000
So the basic answer to your question is that it's not the most important thing.

80
00:08:04,000 --> 00:08:07,000
The most important thing is whether it's the truth.

81
00:08:07,000 --> 00:08:17,000
And that's something you can investigate for yourself.

