1
00:00:00,000 --> 00:00:06,360
Yesterday my dad had to go to the hospital for emergency when I was driving I was

2
00:00:06,360 --> 00:00:09,840
praying that nothing was going to happen to him. It turns out that it was nothing

3
00:00:09,840 --> 00:00:16,560
serious. It got me thinking what can we do to help our parents prepare for death

4
00:00:16,560 --> 00:00:21,760
and have a better rebirth or state? Can we do anything to help them even though

5
00:00:21,760 --> 00:00:39,920
they are not religious? It comes back to can you help anyone at all really and the

6
00:00:39,920 --> 00:00:45,200
answer is you can is only indirectly you can provide people with information

7
00:00:45,200 --> 00:00:56,880
that they're already seeking and you can prompt them when they already have

8
00:00:56,880 --> 00:01:02,560
good intentions but it's like the old adage you can lead a horse to water but

9
00:01:02,560 --> 00:01:11,560
you can't make it drink. So I mean there's no need for your father to be

10
00:01:11,560 --> 00:01:20,640
religious not in the sense of believing in anything but the word religion

11
00:01:20,640 --> 00:01:25,720
doesn't just mean to believe in things. Religious the word religious means to

12
00:01:25,720 --> 00:01:32,280
take something seriously. We use it in secular life as well. He was dieting

13
00:01:32,280 --> 00:01:41,400
religiously or he kept to his diet religiously or so on. People who do things

14
00:01:41,400 --> 00:01:46,600
religiously means they take it seriously. So the point being that they need

15
00:01:46,600 --> 00:01:53,240
to have some realization or some knowledge of the severity of the situation

16
00:01:53,240 --> 00:02:02,480
that they're in which is being unprepared for death. If people don't realize

17
00:02:02,480 --> 00:02:12,480
the importance for preparing for death then it's quite a dangerous situation.

18
00:02:12,480 --> 00:02:22,600
The point is to first understand the importance of it and then of course take

19
00:02:22,600 --> 00:02:27,280
the steps necessary to prepare for it. If the question was how do I prepare

20
00:02:27,280 --> 00:02:30,440
for death and there's a lot of tips that I can give and of course the best

21
00:02:30,440 --> 00:02:35,320
one is to practice lots of meditation. You can learn about you can think

22
00:02:35,320 --> 00:02:41,080
about death and spend some time considering the fact that you're going to have

23
00:02:41,080 --> 00:02:46,720
to die and preparing yourself for it mentally. There's a story of the Bodhisatta

24
00:02:46,720 --> 00:02:52,200
in the time of the Buddha. Bodhisatta in the time well before the Buddha in one

25
00:02:52,200 --> 00:03:05,520
of the Jataka tales. It's the Uragajataka I think. Uragajataka it's a story of

26
00:03:05,520 --> 00:03:11,640
I think it's the Uragajataka maybe wrong. Anyway it's about the snake bites his

27
00:03:11,640 --> 00:03:18,840
son and the son dies immediately but the Bodhisatta has been teaching his

28
00:03:18,840 --> 00:03:28,080
family about death and reminding them so he always made sure that every day

29
00:03:28,080 --> 00:03:31,440
they thought about it they were aware of it and they understood that they

30
00:03:31,440 --> 00:03:34,880
weren't going to live so he was practicing mindfulness of death. It seems kind

31
00:03:34,880 --> 00:03:39,400
of morbid I suppose and it it shouldn't be understood to have encompassed his

32
00:03:39,400 --> 00:03:43,800
life so that they were always standing around in fear of death. The point

33
00:03:43,800 --> 00:03:49,960
was to not be afraid of it to understand or to bring ourselves out of this

34
00:03:49,960 --> 00:03:54,480
conventional reality of being a human being and realizing this this state of

35
00:03:54,480 --> 00:03:58,240
being a human being is not going to last whether there's something after or

36
00:03:58,240 --> 00:04:03,160
before it or not it's only a very small part of reality a small part of the

37
00:04:03,160 --> 00:04:10,560
universe small part of the infinity of time and so thinking about death is it's a

38
00:04:10,560 --> 00:04:16,440
part of bringing yourself out of this and so he did this and when his son died he

39
00:04:16,440 --> 00:04:22,080
looked over and saw his son was dead and he called some some guy was walking past

40
00:04:22,080 --> 00:04:26,160
the road and so he called him and said can you go tell my my wife to just bring

41
00:04:26,160 --> 00:04:31,560
one lunch today and he said and tell her to to bring her daughter bring our

42
00:04:31,560 --> 00:04:41,640
daughter our daughter-in-law as well and the servant the maid or whatever and come

43
00:04:41,640 --> 00:04:46,600
dressed in your best clothes this is what he said please tell my wife this so

44
00:04:46,600 --> 00:04:52,520
the guy goes to their house and tells his wife and she knows oh my son is dead

45
00:04:52,520 --> 00:04:56,960
she knows right away with with the with the dealers my son must have died she

46
00:04:56,960 --> 00:05:00,040
puts on her best clothes gets the whole family together and takes them out with

47
00:05:00,040 --> 00:05:04,400
one lunch so he sits down eats his lunch and then he says look my our son died

48
00:05:04,400 --> 00:05:09,200
we got to do get him a funeral and so they prepare the funeral for him and

49
00:05:09,200 --> 00:05:16,080
suddenly up in heaven saka the king of the angels his throne gets hot and

50
00:05:16,080 --> 00:05:20,640
you know you hear this a lot in the Jatakas and the Dhamabada no I think most

51
00:05:20,640 --> 00:05:26,160
of the Jatakas when when saka's throne gets hot it means someone's

52
00:05:26,160 --> 00:05:32,920
threatening to dethrone him it means someone's someone's got some great power

53
00:05:32,920 --> 00:05:38,000
of mind that that might threaten it and it means that he might have to he

54
00:05:38,000 --> 00:05:42,800
might be dethrone so he says who is it that he always looks down and so who is

55
00:05:42,800 --> 00:05:47,040
it that's going to dethrone who is that that's threatening me with this but

56
00:05:47,040 --> 00:05:53,240
he's a good guy and that's how he got to be become saka but he he always wants

57
00:05:53,240 --> 00:05:58,840
to test these people so he goes down and he he puts himself in the form of a

58
00:05:58,840 --> 00:06:09,440
brahman just a just a guy really no high-class fellow and he walks up to them

59
00:06:09,440 --> 00:06:14,440
and he sees them burning this he looks at them burning their son and he says oh

60
00:06:14,440 --> 00:06:19,160
look at you burning you must be burning a beast or an enemy of some sort when

61
00:06:19,160 --> 00:06:24,680
the Bodhisatt looks at him and says no we're burning my son and he looks at him

62
00:06:24,680 --> 00:06:28,360
he says what do you mean you how could this be your son you're not crying you're

63
00:06:28,360 --> 00:06:35,560
not upset you're not you're not grieving at all he said must have been a

64
00:06:35,560 --> 00:06:40,400
pretty poor son and and the Bodhisatt this as well and he gives this this very

65
00:06:40,400 --> 00:06:45,840
famous verse that mama Buddha in Buddhist circles maybe actually not so

66
00:06:45,840 --> 00:06:52,720
famous a very profound verse anyway whether it's famous or not that that goes

67
00:06:52,720 --> 00:06:59,640
something like just just as you can't you can't like don't cry over

68
00:06:59,640 --> 00:07:04,400
basically basically don't cry over spilled milk he says our tears won't bring

69
00:07:04,400 --> 00:07:11,760
won't bring back something that is broken and something like our

70
00:07:11,760 --> 00:07:17,880
our cries won't bring the moon down from up above and and the verse after

71
00:07:17,880 --> 00:07:22,560
verse he asks the man's wife he asks the daughter he says this this was your

72
00:07:22,560 --> 00:07:26,320
husband he must have beaten you that you're not you're not crying or you're not

73
00:07:26,320 --> 00:07:31,320
upset he said women are women are tenderers he says something like that and

74
00:07:31,320 --> 00:07:38,160
women cry you know and and she says not me you know I loved him and I was he

75
00:07:38,160 --> 00:07:41,760
was he was a good he was you know a great man and I was faithful and he was

76
00:07:41,760 --> 00:07:46,920
faithful to me but he's gone now oh and the verses go that something like he

77
00:07:46,920 --> 00:07:52,200
fares the way he has to tread and so they didn't grieve at all it's a fairly

78
00:07:52,200 --> 00:07:56,320
famous story not exactly what you're asking but it does give you some idea as to

79
00:07:56,320 --> 00:08:02,400
how people prepare themselves the what I always say the whole idea of helping

80
00:08:02,400 --> 00:08:08,160
other people of course it just to point out how problem problem how trouble

81
00:08:08,160 --> 00:08:13,720
summit is but at the best way to help people is to help yourself to make

82
00:08:13,720 --> 00:08:19,440
yourself an an example and not only to make yourself an example but to create

83
00:08:19,440 --> 00:08:26,920
the power of mind necessary to feed them good vibes you'll find even just by

84
00:08:26,920 --> 00:08:31,560
sitting in meditation or after you do insight meditation spend some time

85
00:08:31,560 --> 00:08:35,480
sending love to them sending your good thoughts wishing them to be

86
00:08:35,480 --> 00:08:40,080
happy wishing them to find peace and you'll find it's incredible how they

87
00:08:40,080 --> 00:08:45,840
respond to that just that that power of mind how they how it changes them and it

88
00:08:45,840 --> 00:08:50,560
changes their attitude towards you it's interesting to find that the next time

89
00:08:50,560 --> 00:08:55,080
you talk to that person they're just like ecstatic to hear from you and so

90
00:08:55,080 --> 00:09:00,960
happy to hear from you and so kind and and and you know just just well intentioned

91
00:09:00,960 --> 00:09:11,200
towards you so the the the power of our minds and the power of our intentions it's

92
00:09:11,200 --> 00:09:15,120
kind of like how theists will use prayer they they set their mind on something

93
00:09:15,120 --> 00:09:20,080
and sometimes the the power can carry through or it at the point as it gives them

94
00:09:20,080 --> 00:09:25,760
the the sets their mind in the right way then when they approach other people

95
00:09:25,760 --> 00:09:29,000
or so on they will be kind and generous and some good things will happen to

96
00:09:29,000 --> 00:09:35,960
them in return beyond that I mean you can you can see what you can do I mean as I

97
00:09:35,960 --> 00:09:43,120
said if he's not religious and and if that means that he's not he's not concerned

98
00:09:43,120 --> 00:09:48,880
about that there he's not taking the time to prepare himself then it might be

99
00:09:48,880 --> 00:09:56,920
quite difficult but you know you can you can you can give the prompt you can

100
00:09:56,920 --> 00:10:01,880
prompt people if they have the goodness in them or if they have what it takes

101
00:10:01,880 --> 00:10:08,080
in them to to realize the importance of such a thing I mean I'm obviously

102
00:10:08,080 --> 00:10:11,600
avoiding saying teach him meditation because it sounds like probably he's

103
00:10:11,600 --> 00:10:18,560
he's still far from that but it may be such you know and and you you're not

104
00:10:18,560 --> 00:10:25,680
living his life so obviously you can't enlighten him no one can but you can

105
00:10:25,680 --> 00:10:31,040
give him the prompt and you can be an example and you can be a support and a

106
00:10:31,040 --> 00:10:35,520
friend that that's really an important thing when people are dying is to be a

107
00:10:35,520 --> 00:10:40,280
good support to be someone who's stable it calms people down to be with them

108
00:10:40,280 --> 00:10:45,160
and do not be crying and and morning like when my grandfather grandmother was

109
00:10:45,160 --> 00:10:48,240
dying what they did with her is they had her sing these songs she would have

110
00:10:48,240 --> 00:10:53,480
pain and horrible pain and they had her sing songs and so on and it was it

111
00:10:53,480 --> 00:10:56,520
seemed kind of that but that accurately because it wasn't helping the pain it

112
00:10:56,520 --> 00:11:03,520
was just kind of helping her pretend that it wasn't there but I did some

113
00:11:03,520 --> 00:11:08,200
chanting for her some let her listen and she she quieted down and during the

114
00:11:08,200 --> 00:11:11,600
time that I was chanting I didn't know at that time it was a long time ago I

115
00:11:11,600 --> 00:11:19,880
didn't know how to teach meditation so what if had a lot more to say but you

116
00:11:19,880 --> 00:11:26,520
know being that stability for the people for people and being a reminder for

117
00:11:26,520 --> 00:11:34,120
them because when your father is on on death's door or whatever and his last

118
00:11:34,120 --> 00:11:37,800
moment he's gonna think about everything that happened in his life and you

119
00:11:37,800 --> 00:11:44,400
will go through his mind as well for sure so you your job now is to make an

120
00:11:44,400 --> 00:11:48,320
impression on him I mean he's that these are the least that you can do if you

121
00:11:48,320 --> 00:11:52,040
can get him to meditate then by all means that's that's wonderful and this

122
00:11:52,040 --> 00:11:55,480
can often happen for people who've had strokes or who've had to go to the

123
00:11:55,480 --> 00:12:01,320
had seizures or so on they can become religious they they can see this the

124
00:12:01,320 --> 00:12:05,920
importance because when you're in that position your mind becomes focused and

125
00:12:05,920 --> 00:12:11,640
concentrated and you do start to remember things you've done remember reflect

126
00:12:11,640 --> 00:12:15,040
on your life and you you feel bad about the things people who have near-death

127
00:12:15,040 --> 00:12:20,520
experiences often change their lives as a result well well they will

128
00:12:20,520 --> 00:12:23,960
realize that they only have so so long and it could come to them at any time

129
00:12:23,960 --> 00:12:39,600
the invincible ideas leaves them so I hope that helps

