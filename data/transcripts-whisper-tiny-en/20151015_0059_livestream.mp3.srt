1
00:00:00,000 --> 00:00:20,960
Okay, good evening everyone, welcome back, no more broadcasting life, October 14th, 2015 so we have more

2
00:00:20,960 --> 00:00:38,400
Dhammapada, this is when we have three verses, so let's get started, hello and welcome back

3
00:00:38,400 --> 00:00:46,560
towards the day of the Dhammapada, tonight we continue on with verses 87 to 89 what tree that's

4
00:00:46,560 --> 00:00:58,320
follows, kan hang dhammang vipaha yasu kan bhawetapandito, oka anokamanga oka anokamanga

5
00:00:58,320 --> 00:01:10,840
kamma, vivakayyataduramang, tatra bhiratimitjaya yidwakami akinjano, paryodhapayya attanang titang titak

6
00:01:10,840 --> 00:01:27,600
kleshehapandito, yasang sambhu dhyang gesu, samajitang subhavitang, adanapatimitsagay, anupadayya yidwata, kina sava jutimanto,

7
00:01:27,600 --> 00:01:44,280
daylokay parynibhata, it's three verses, the meaning of which is as follows, giving up or

8
00:01:44,280 --> 00:02:00,360
abandoning black dhammas, vipaha yasu kan bhawetapandito, a wise one cultivates, white dhammas,

9
00:02:00,360 --> 00:02:13,240
cultivates the white, oka anokamanga, vivakayyataduramang, having gone from home to

10
00:02:13,240 --> 00:02:34,120
homelessness, one dwells in a solitude that is hard to enjoy, difficult to enjoy.

11
00:02:34,120 --> 00:02:46,400
And the next verse, tatra bhiratimitjaya, one should wish for such, wish for great enjoyment

12
00:02:46,400 --> 00:03:00,440
or appreciation, such happiness, having itwakami akinjano, having destroyed or cut off sensuality,

13
00:03:00,440 --> 00:03:11,240
a kinjanoans, has one who has nothing, baryodhapayya adanam, one should cleanse or purify

14
00:03:11,240 --> 00:03:22,280
oneself, jitaklacy, adito, as a wise one one should cleanse the mind, one's own mind, of

15
00:03:22,280 --> 00:03:37,760
all the famines, gisang sambodhiyang gisu, samajitang subhavita, one who has well cultivated

16
00:03:37,760 --> 00:03:46,600
one's own mind rightly, in regards to the sambodhiyanga, the anger, the factors relating

17
00:03:46,600 --> 00:04:00,880
to enlightenment, adanapatinisage, having abandoned clinging, anupada yarata, who dwells

18
00:04:00,880 --> 00:04:10,960
happy without clinging, tina sava who has destroyed the tanes or the stains of one's

19
00:04:10,960 --> 00:04:21,700
mind, jutimanto, one who is brilliant, dayloghe, baryenibutas, such a one becomes completely

20
00:04:21,700 --> 00:04:30,320
calmed or tranquilized or extinguished in the world, so quite a bit, this is again, as

21
00:04:30,320 --> 00:04:36,600
I said, a part of this famous set of verses, well known in Thailand anyway, it's something

22
00:04:36,600 --> 00:04:46,920
that we chant, quite often I haven't chanted it in a while, but still quite familiar.

23
00:04:46,920 --> 00:04:52,840
And as it happens, there is no story to go with these verses, and there's only, the story

24
00:04:52,840 --> 00:05:16,720
that we have is 50 monks who came to see the buddha from Gosala, and the kingdom of

25
00:05:16,720 --> 00:05:23,040
Gosala, 50 monks came to see the buddha, and when they got there the buddha asked them

26
00:05:23,040 --> 00:05:29,000
and had them relate to him, the things that went on, or they're going on, and then that

27
00:05:29,000 --> 00:05:35,000
was it, he taught them this verse, so this is just simply an example of teaching the

28
00:05:35,000 --> 00:05:42,200
dhamma, so now my job is to explain this dhamma, that's really the key here, because

29
00:05:42,200 --> 00:05:47,640
this is one verse that has quite a bit of pith to it, quite a bit of meaning to it, there's

30
00:05:47,640 --> 00:05:55,160
quite a bit in here, so let's try and break it into pieces and relate it to our practice.

31
00:05:55,160 --> 00:06:01,040
Kanhang dhamma, the black dhammas, so we have this black and white, this is common, I guess

32
00:06:01,040 --> 00:06:07,320
you could say, imagery that the buddha used, black refers to the bad, and white refers

33
00:06:07,320 --> 00:06:16,320
to the good, I have a friend who many years ago asked me about this, he said, so did the

34
00:06:16,320 --> 00:06:24,800
buddha have a problem with black, so this friend of mine was dark skinned, African heritage,

35
00:06:24,800 --> 00:06:29,120
and so he asked me, did the buddha have a problem with black, with the color black, I

36
00:06:29,120 --> 00:06:37,840
mean why is, why do meditators wear white, and the idea is that white is about purity, that's

37
00:06:37,840 --> 00:06:46,480
something to do with purity, but it's quite a charged statement and a charged cult tradition,

38
00:06:46,480 --> 00:06:55,520
because in India, the idea of light skin was associated with higher caste, or it's actually

39
00:06:55,520 --> 00:07:01,960
unclear whether that was truly the case, but there is some sense of that, especially

40
00:07:01,960 --> 00:07:11,560
I think after the British were colonized and controlled the country for so long, it seems

41
00:07:11,560 --> 00:07:22,080
they may have promoted the idea, but I don't think, I don't think, I mean my response

42
00:07:22,080 --> 00:07:31,320
to him was that, I mean, it's nothing to the skin color, because white skin would look, no

43
00:07:31,320 --> 00:07:36,800
one has white skin and no one has black skin, there's no such thing, we all have various

44
00:07:36,800 --> 00:07:49,600
shades of pink to tan, I guess, and this is just, it's more to do with light and dark

45
00:07:49,600 --> 00:07:57,240
lightness and darkness, black, and anyway, it's just a word, it's nothing, it's not that

46
00:07:57,240 --> 00:08:01,440
the Buddha liked the color white, more than the color black, but there is suitable imagery

47
00:08:01,440 --> 00:08:06,520
that corresponds with it, because white is associated with light, which you can use to

48
00:08:06,520 --> 00:08:15,080
see, black is associated with darkness, which implies the inability to see, and corresponding

49
00:08:15,080 --> 00:08:21,840
the problems and difficulties, but here it refers to evil and good, so one should give

50
00:08:21,840 --> 00:08:32,000
up evil and cultivate good, destroy evil in one's heart and cultivate the good, so we're

51
00:08:32,000 --> 00:08:43,400
talking about unwholesome bodily acts and acts of speech and unwholesome states of mind,

52
00:08:43,400 --> 00:08:47,880
and then we're talking about their opposites when we refer to the white, so wholesome

53
00:08:47,880 --> 00:08:55,600
needs of action in the speech and wholesome thoughts, so the first two are in regards

54
00:08:55,600 --> 00:09:02,640
to morality, the third one is in regards to concentration and wisdom, but this is how it

55
00:09:02,640 --> 00:09:14,000
starts, we start by somehow monitoring our body and our speech and watching our movements,

56
00:09:14,000 --> 00:09:19,520
this is how meditation starts, we start by looking at the body and it may not be clear,

57
00:09:19,520 --> 00:09:25,400
but simply watching the stomach rising and falling is an example of morality, doesn't

58
00:09:25,400 --> 00:09:29,120
seem like it's an ethical thing, it's an ethically charged thing to do, but it actually

59
00:09:29,120 --> 00:09:35,560
is, it's an ethically sound action, which sounds kind of funny, but it's an action watching

60
00:09:35,560 --> 00:09:41,080
your stomach rise and fall is an action that is free from any unethical or violations

61
00:09:41,080 --> 00:09:45,440
of ethics from a Buddhist point of view, when you walk, stepping right, stepping left,

62
00:09:45,440 --> 00:09:51,080
you have ethics and you're monitoring your actions, and by doing that, I mean the extension

63
00:09:51,080 --> 00:09:54,960
is that when you're doing sitting or walking meditation this way, you're less likely to

64
00:09:54,960 --> 00:10:04,600
commit an infraction or you're less likely to commit an act or a speech that is unwholesome,

65
00:10:04,600 --> 00:10:11,760
not simply because it's a ritualistic movement that's more to do with protecting you while

66
00:10:11,760 --> 00:10:16,080
you're training, but also because you're mindful and the point is you're cultivating

67
00:10:16,080 --> 00:10:23,000
mindfulness so that then when you take that out into the ordinary world, you'll be able

68
00:10:23,000 --> 00:10:28,400
to apply it so that when a mosquito lands on your arm, you want to immediately react by

69
00:10:28,400 --> 00:10:39,120
killing, by attacking, so, I mean, from the very get go that sweat meditation is all about,

70
00:10:39,120 --> 00:10:44,400
and then later on it begins to affect your mind, begins to change so that you don't even

71
00:10:44,400 --> 00:10:52,120
want to kill, want to hurt, want to do, do these such things, you're more inclined to

72
00:10:52,120 --> 00:10:58,000
do good things to help people, as you see how it brings peace and happiness and how it's

73
00:10:58,000 --> 00:11:06,240
just a more efficient and productive thing to do, do good deeds.

74
00:11:06,240 --> 00:11:14,120
Oka no kamagama, so going from home to homelessness, this is a big, obviously a big part

75
00:11:14,120 --> 00:11:19,960
of the Buddhist monastic path, now it's not for everyone, but here he is talking to monks

76
00:11:19,960 --> 00:11:24,400
and it is a great, considered a great thing to do.

77
00:11:24,400 --> 00:11:33,080
You could also look at this sort of, I guess, symbolically because it refers to Oka as an

78
00:11:33,080 --> 00:11:42,880
interesting word, you could think of it simply as going from owning things to not owning

79
00:11:42,880 --> 00:11:49,000
things in the sense of not letting things own you, not identifying with your possessions,

80
00:11:49,000 --> 00:11:55,880
because obviously monks would live in huts and they live in nowadays living, even houses

81
00:11:55,880 --> 00:12:01,320
or some monks live in apartment buildings and that kind of thing, so it's not the case

82
00:12:01,320 --> 00:12:05,840
that they don't have a dwelling, it's that they don't have anything that they call

83
00:12:05,840 --> 00:12:11,480
their own, and lay people can do this as well, not in the sense of legally giving up

84
00:12:11,480 --> 00:12:18,320
the right to things, but mentally and in terms of the ego, giving up on detachment of

85
00:12:18,320 --> 00:12:25,600
things and using things, not having things own you in that sense, and as a result of

86
00:12:25,600 --> 00:12:47,360
this, one attains a solitude, one's mind isn't constantly splitting off to one's belongings.

87
00:12:47,360 --> 00:12:51,560
It's hard to find such solitude, the Buddha said, hard to enjoy, and he talks about

88
00:12:51,560 --> 00:12:57,360
what he uses this word often, because most of us would like possessions and are obsessed

89
00:12:57,360 --> 00:13:02,320
with our possessions and getting more possessions and protecting and maintaining and even

90
00:13:02,320 --> 00:13:09,760
just looking at and touching and using our possessions, it's hard to enjoy the solitude

91
00:13:09,760 --> 00:13:15,520
of giving things up, akin to no which we see in verse number 88, being with nothing, akin

92
00:13:15,520 --> 00:13:22,080
to no means someone who has nothing, means no, it could be in regards to physical possessions,

93
00:13:22,080 --> 00:13:25,960
it could be in regards to mental attachments.

94
00:13:25,960 --> 00:13:39,200
So he says, tatrabi, rati michi, one should strive for, one should wish for, and it's

95
00:13:39,200 --> 00:13:51,000
worth worth wanting, in a sense, such, in a sense of enjoyment, enjoying being at peace,

96
00:13:51,000 --> 00:13:59,920
it's hard to walk on me, having destroyed calm, having destroyed sensual pleasure, sensual

97
00:13:59,920 --> 00:14:05,600
desire, the attachment to sensual pleasure, sensual, the objects of sensual pleasure,

98
00:14:05,600 --> 00:14:13,720
having given them up, and purifying one's mind of defoundance.

99
00:14:13,720 --> 00:14:15,800
This is what a wise one does.

100
00:14:15,800 --> 00:14:19,600
You notice the pattern of the talk about a wise one, right?

101
00:14:19,600 --> 00:14:24,960
This whole chapter is the Panditava, which, with these three verses, we've now finished,

102
00:14:24,960 --> 00:14:32,720
but this is how the Dhammapada is set up, each chapter is associated by the mention

103
00:14:32,720 --> 00:14:39,120
of a certain specific word, so there's, I think, 22 chapters, 20-some, more than 22, I think,

104
00:14:39,120 --> 00:14:48,280
20-some chapters, and 423 verses, and we've just finished the sixth one, got the sixth chapter,

105
00:14:48,280 --> 00:14:58,840
which is the Pandita chapter, anyway, so we'll clear in our minds in this tradition, in the

106
00:14:58,840 --> 00:15:06,520
Buddha's tradition, that clinging to things is a problem, and that if we can be free from

107
00:15:06,520 --> 00:15:14,080
such clinging, if we can come to just live without this clinging, then we would be free

108
00:15:14,080 --> 00:15:20,400
from suffering, that this would be a better thing, and so the Buddha in 88 gives a description,

109
00:15:20,400 --> 00:15:24,800
it's just a very brief description of how we go about this, and this is by cleansing

110
00:15:24,800 --> 00:15:28,400
our minds of the attachment.

111
00:15:28,400 --> 00:15:33,960
So the answer is not to always get what you want, it's to stop wanting, to stop wanting

112
00:15:33,960 --> 00:15:39,920
for things, clear your mind of defilement, it's clear your mind of the things that make

113
00:15:39,920 --> 00:15:48,640
you want this and want that, or make you hate this or hate that, and this is what, this

114
00:15:48,640 --> 00:15:54,800
is our practice and meditation, so we're constantly working on this, we're not concerned

115
00:15:54,800 --> 00:16:00,400
about what we get or don't get, and we look at our, we watch our minds, observe our minds,

116
00:16:00,400 --> 00:16:09,800
monitor our minds as we get, and as we lose, and as change comes to us again and again,

117
00:16:09,800 --> 00:16:19,280
we work out these mental states, slowly changing and purifying our minds, this is the

118
00:16:19,280 --> 00:16:28,160
practice, this is the Buddha's exhortation, to these 50 monks, and then finally 89 gives

119
00:16:28,160 --> 00:16:35,840
more details, we have in regards to the anger of the sambodi, the factors of enlightenment,

120
00:16:35,840 --> 00:16:41,160
this could be the bhodjanga, or it could also be the bhodi pakya dama, probably I think

121
00:16:41,160 --> 00:16:45,760
the commentary says it's the seven bhodjangas, so it means cultivating the bhodjangas,

122
00:16:45,760 --> 00:16:52,560
but if you don't know what those are, we have sati starts with, of course sati which is

123
00:16:52,560 --> 00:17:00,400
the ability to remember or remind yourself the remembrance of the things as they are, seeing

124
00:17:00,400 --> 00:17:09,680
things as they are, not how you wish them to be, or how you like them to be, not based

125
00:17:09,680 --> 00:17:16,800
on liking or disliking, not judging them, or extrapolating upon them, and then with sati you have

126
00:17:16,800 --> 00:17:22,960
dama vi jaya, which means this investigation that goes on or not investigation, but more like

127
00:17:22,960 --> 00:17:36,320
starting to categorize and separate and to figure out, to understand them, let's put it that way,

128
00:17:36,320 --> 00:17:42,400
to understand reality, so as we meditate and we cultivate sati, we start to see the difference

129
00:17:42,400 --> 00:17:47,280
between right and wrong, we start to see what we're doing wrong, we start to change our habits

130
00:17:47,280 --> 00:17:52,880
naturally, our minds start to change as we see how we're causing our self-suffering, it's like

131
00:17:52,880 --> 00:17:59,200
that's not useful if I continue that, it's going to cause me suffering better than I

132
00:17:59,200 --> 00:18:04,000
change that, and it's not even intellectual, it's just naturally through the observation,

133
00:18:04,000 --> 00:18:07,920
through monitoring and seeing what we're doing right and wrong, we're able to

134
00:18:09,680 --> 00:18:14,640
remember the change, so this is dama vi jaya, and then there's the vi jaya, which is effort,

135
00:18:15,680 --> 00:18:21,120
this helps us put our effort and we also have to put our effort, putting our effort is important,

136
00:18:22,400 --> 00:18:28,160
we have to work at it, we have to constantly bring your mind back to the present moment,

137
00:18:28,160 --> 00:18:32,320
bring your mind back to reality again and again and again, it's something that has to become

138
00:18:32,320 --> 00:18:37,760
habitual, you have to cultivate the habit and you have to work at it, so that's really good,

139
00:18:37,760 --> 00:18:47,280
then beauty, beauty, it means you have to be, you have to have some kind of stimulation in the sense

140
00:18:47,280 --> 00:18:55,840
or you have to be into it, you have to get into a groove and you have to work at getting into a

141
00:18:55,840 --> 00:19:03,600
routine and a groove and getting caught up, so that it just becomes natural, this is beauty,

142
00:19:03,600 --> 00:19:09,600
and busting, it's tranquility, part of the practice is to calm the mind, the mind becomes

143
00:19:09,600 --> 00:19:17,760
calmer as you sort things out in your mind, and then some ideas concentrated, the mind becomes

144
00:19:17,760 --> 00:19:26,240
focused and concentrated seeing things clearly not superficially, they're not being distracted by

145
00:19:26,240 --> 00:19:32,880
everything that comes along, and finally, Upeika, the mind eventually gets to a state where it

146
00:19:34,800 --> 00:19:40,640
sees everything just as it is, the judgements fade away and the mind is simply noting

147
00:19:41,600 --> 00:19:46,960
experience after experience, after experience, so all seven of these are the seven boat

148
00:19:46,960 --> 00:19:52,720
junglers that the Buddha is talking about, and it's a really good, simple explanation of the

149
00:19:52,720 --> 00:19:59,920
various things that we have to cultivate in our practice, then this is called samadhi-dang-subha

150
00:19:59,920 --> 00:20:09,920
vitang, the right cultivation, good, well cultivated mind, well and rightly cultivated mind,

151
00:20:09,920 --> 00:20:18,640
more or less, and dana-patimi-saghi, one should give up clinging, so it all comes back to our

152
00:20:18,640 --> 00:20:29,760
clinging, we mean by judgements, we mean our inability to adapt and to keep up with change, basically,

153
00:20:29,760 --> 00:20:37,840
no things change, something comes or something goes, and we cling to what it was or what it is,

154
00:20:37,840 --> 00:20:45,600
so it is right now and that rather than roll with it and change, we want for it to change,

155
00:20:45,600 --> 00:20:51,520
either to get closer to it or to get farther away from it, we wanted to come, we wanted to stay,

156
00:20:51,520 --> 00:20:59,440
or we wanted to go, and that's called clinging, and that's what we're giving up, so the

157
00:20:59,440 --> 00:21:06,880
buddha says, we give this up and dwell happily without it, kina-sova having destroyed the

158
00:21:06,880 --> 00:21:14,880
assova, the various taines which relates back to the defilements that we already talked to,

159
00:21:14,880 --> 00:21:23,280
so it's talked about, so it's desire for sensuality and desire for becoming, wanting to be this

160
00:21:23,280 --> 00:21:30,160
wanting to be that, thoughts that arise, hey, be great if I was bossed or something like that,

161
00:21:30,160 --> 00:21:39,200
president or whatever, and then views of self and so on, all of these things that cloud our mind

162
00:21:39,200 --> 00:21:48,880
or cloud our judgment, jutimando brilliant because one is full of wisdom, that's when practices

163
00:21:48,880 --> 00:21:55,360
one comes to understand oneself and by extension really the whole universe, and that's a sort of

164
00:21:55,360 --> 00:22:02,160
brilliance, such a person becomes free, so as a result it's not just a static state where, okay,

165
00:22:02,160 --> 00:22:08,960
now I understand reality, now what, it's not static because once you understand reality, you let

166
00:22:08,960 --> 00:22:14,240
it go, and that's how cessation comes about, that's how the experience of new nirvana or new

167
00:22:14,240 --> 00:22:20,720
nirvana comes about, and eventually that's how one frees oneself from the rounds of samsara being

168
00:22:20,720 --> 00:22:29,440
born, old, sick, and dying, and that's what is meant by Parini Buddha, one frees oneself from the

169
00:22:29,440 --> 00:22:38,480
cycle that we find ourselves trapped in, so that's all, these are teachings for us to remember,

170
00:22:38,480 --> 00:22:45,600
it's like one of those quotables of the Buddha, something you could quote, although as you can see

171
00:22:45,600 --> 00:22:51,280
by the translation it's not easy to be literal and to quote this literally, but there are some

172
00:22:51,280 --> 00:22:56,880
good translations of the Dhamapada out there, so I'm just trying to get the sense across

173
00:22:57,840 --> 00:23:04,960
and give some interesting teachings, so we have several in here, and at the heart of it are the

174
00:23:04,960 --> 00:23:14,080
seven bojangas which have now explained in brief, so that's the teaching on the Dhamapada today,

175
00:23:14,640 --> 00:23:20,880
thank you all for tuning in, wishing you all the best and keep practicing.

176
00:23:20,880 --> 00:23:44,640
All right, let's that, do we have any questions? We do, is a Pacheka Buddha just like a self,

177
00:23:44,640 --> 00:23:51,840
self-taught our hands for Parini more than that?

178
00:23:55,120 --> 00:24:00,800
Yeah, I mean, I don't think all Pacheka Buddhas will be exactly the same, but that's about the just of it.

179
00:24:00,800 --> 00:24:16,080
I mean, it's not just because it takes a lot to become a Pacheka Buddha, but that's the definition.

180
00:24:17,920 --> 00:24:23,520
Our hands are always happy, correct? Is it simply that their happiness or is it simply that

181
00:24:23,520 --> 00:24:29,200
their happiness is not dependent on anything? When we happiness is a difficult word, do you mean

182
00:24:29,200 --> 00:24:36,080
they always experience a happy feeling? No, no, that's not the case, but are they always

183
00:24:38,080 --> 00:24:42,560
at peace? I would say that that's a better description than the answer is yes.

184
00:24:46,080 --> 00:24:52,160
On the other hand, they have physical suffering, but it's only physical, they have no mental

185
00:24:52,160 --> 00:25:00,080
suffering, until they attain final Nirvana and then there's no physical suffering as well.

186
00:25:04,480 --> 00:25:10,800
And a meditation question, every few minutes my eyes would feel painful and after saying pain,

187
00:25:10,800 --> 00:25:17,200
pain, pain for a bit, tears would flow out of them. Is this a common experience that doesn't

188
00:25:17,200 --> 00:25:24,240
usually happen to me? Yeah, it can be related to what we call PT, cry, tears is common

189
00:25:25,360 --> 00:25:29,040
in that sense. It can come from concentration, but the body does strange things.

190
00:25:30,160 --> 00:25:35,040
It's showing you what my teacher would say, he said, you see that? You know what that tells you?

191
00:25:35,040 --> 00:25:42,080
In permanence. That way, when you said that this hasn't, you'll say, do you feel strange?

192
00:25:42,080 --> 00:25:53,760
Who's it back? I said, cry, cry, cry, a nichol, impermanent. That's really it, you know.

193
00:25:54,960 --> 00:26:00,000
It's the sense of strangeness, like, oh, it never happened to me before,

194
00:26:00,960 --> 00:26:06,800
is helping cultivate this sense of flexibility that is an important part of insight meditation.

195
00:26:06,800 --> 00:26:16,480
So that sense of flexibility is going to be important in terms of, I mean, it's an important

196
00:26:16,480 --> 00:26:22,800
part of the progress and it's going to increase as you get further on. You're going to be

197
00:26:23,440 --> 00:26:29,920
surprised and thrown off guard and it's the cultivate, the practices to cultivate

198
00:26:29,920 --> 00:26:37,520
and the flexibility to be able to just see the new experience as it is, rather than reacting to it.

199
00:26:44,880 --> 00:26:50,640
Dante, what is the cause of dullness? How can dullness of mind be eliminated and how can it be

200
00:26:50,640 --> 00:26:56,320
prevented from arising in the future? I ask this question because I am a student and sometimes find

201
00:26:56,320 --> 00:27:04,880
my mind just dull. I mean, sometimes the brain just gets overworked, but there's a mental aspect.

202
00:27:04,880 --> 00:27:09,840
That's what we would call sloth and torpor. It's old English terms that don't really mean all that

203
00:27:09,840 --> 00:27:14,960
much. Probably should update those because no one uses words like sloth and torpor, although torpor's

204
00:27:14,960 --> 00:27:21,680
a good one. I think some people have tried to update it, but we're stuck pretty much stuck with

205
00:27:21,680 --> 00:27:29,920
sloth and torpor. It's the physical and the mental, I guess more it's considered the mental

206
00:27:31,440 --> 00:27:33,920
hindrance of stiffness in the mind.

207
00:27:37,440 --> 00:27:42,000
And the only way to really overcome it is to remindfulness. That's the only surefire way.

208
00:27:42,800 --> 00:27:48,160
But sometimes it's, as I said, can be physical, can be organic. Either way, it's not something that you

209
00:27:48,160 --> 00:27:53,680
can just do away with and have it never come back. And that's not our goal in meditation. I mean,

210
00:27:53,680 --> 00:27:58,160
that's not our practice in meditation. Ultimately, it will go away and never come back. That's great.

211
00:27:59,040 --> 00:28:05,840
But in the meantime, that's not our immediate goal. Our goal is to see it clearly as it is.

212
00:28:07,120 --> 00:28:12,960
We're not worried if it comes or if it goes. It stays or if it goes. We're just concerned with

213
00:28:12,960 --> 00:28:18,480
seeing it as it is. So you can acknowledge dull, dull, or feeling, feeling. Just remind yourself

214
00:28:18,480 --> 00:28:21,360
that's all it is and stop wishing for it to go away.

215
00:28:28,560 --> 00:28:33,200
One day is sloth and the physical side of torpor on the mental side.

216
00:28:33,200 --> 00:28:37,840
No, they're both mental. I think it's, I mean, the commentary does funny things like this,

217
00:28:37,840 --> 00:28:47,360
but, you know, it's interesting. It says, Tina, I think, is the mind, is in the mind.

218
00:28:47,360 --> 00:28:54,480
And media is in the mental and competence. You have to look it up in the Abidham,

219
00:28:54,480 --> 00:29:00,240
exactly what it means. But it's, you know, I don't know, they get so technical sometimes.

220
00:29:01,360 --> 00:29:04,640
But they're both mental. Neither one is considered physical.

221
00:29:04,640 --> 00:29:07,920
Okay, thank you.

222
00:29:09,200 --> 00:29:13,440
One day, good evening, and thank you for your time. This evening after my meditation time

223
00:29:13,440 --> 00:29:19,520
was sounded at the end of my meditation session, it startled me. I noted my reaction as startled

224
00:29:19,520 --> 00:29:25,280
startled. That question is, since I was startled, does that tell me I was not practicing correctly?

225
00:29:25,280 --> 00:29:31,840
Thank you. Good, B. Yeah. I mean, there is one stage of knowledge where

226
00:29:31,840 --> 00:29:38,880
the meditator becomes acutely aware of such things, and almost at the point of paranoia,

227
00:29:38,880 --> 00:29:44,640
but anything will set them off. Like they'll look at, the classic example is they'll look at

228
00:29:44,640 --> 00:29:49,760
their bed and be freaked out. Like they'll get a sense of just extreme fear by looking at a simple

229
00:29:49,760 --> 00:29:56,640
object. There's reasons for that, and it's a specific stage of knowledge. And it can come,

230
00:29:56,640 --> 00:30:05,120
you know, I mean, people can get this sense of, I mean, it has to do with seeing everything cease

231
00:30:05,120 --> 00:30:12,720
and just getting worked up and worked up about it. It's not actually a good thing, but it's a side

232
00:30:12,720 --> 00:30:19,280
effect. Experiencing knowledge and understanding and not being able to react to it. So in the

233
00:30:19,280 --> 00:30:24,640
beginning, this sort of jumpiness can come off in the meditator, and that, yeah, no, we'll,

234
00:30:24,640 --> 00:30:32,880
we'll feel as if they're falling over and, and freak and, and just jump like as though they,

235
00:30:32,880 --> 00:30:40,320
they suddenly fall into this feeling that they're about to fall. That's a common characteristic.

236
00:30:41,520 --> 00:30:49,760
But I mean, probably more common is just a general sense of jumpiness that, you know, I mean,

237
00:30:49,760 --> 00:30:55,360
and I think a good reason for it to come is distraction, right? When you're caught up in something

238
00:30:55,360 --> 00:31:05,520
and someone walks up behind you, we're not allowed to scare fellow monks. And I went, when I went

239
00:31:05,520 --> 00:31:09,920
back to Stony Creek recently, I walked up behind the head monk and just stood there watching him,

240
00:31:11,280 --> 00:31:15,360
oh boy, did I scare him? And I felt really bad because, you know, knowing his history and what

241
00:31:15,360 --> 00:31:22,880
he's been through in his life, scaring him is not very nice. So yeah, you have to be careful to

242
00:31:24,560 --> 00:31:31,440
make your presence known. But yeah, it happens. That sort of thing happens. So

243
00:31:32,320 --> 00:31:40,240
I mean, whatever, it's an experience and the correct response is to acknowledge the fear or the

244
00:31:40,240 --> 00:31:46,480
feeling or whatever is in the present moment.

245
00:31:50,400 --> 00:31:55,440
Hello, Bandai. If I help someone out and they pay me for it, is there still good karma?

246
00:31:58,320 --> 00:32:03,760
Karma is momentary. So every moment you're creating karma, that's why mindfulness is so important.

247
00:32:03,760 --> 00:32:10,960
So karma isn't one big deed that you did. In fact, after you do the deed, what you think about

248
00:32:10,960 --> 00:32:15,600
it is more karma. So if you're worried about, was it good karma, was it bad karma? Well, that's

249
00:32:15,600 --> 00:32:22,080
more karma. If you feel happy about helping someone, that's good karma. If you feel happy about

250
00:32:22,080 --> 00:32:28,560
the money you made, that's kind of probably more on the greed side. At the moment when you took

251
00:32:28,560 --> 00:32:36,080
the person's money, there may have been greed as well. Economics is very subtle and not

252
00:32:36,080 --> 00:32:41,360
something that's going to lead you to hell or anything, but most economic transactions are greed-based.

253
00:32:41,360 --> 00:32:45,600
You know, even when you require, when you write up someone to build, I would say there's

254
00:32:46,800 --> 00:32:55,920
a fairly likely unwholesumness associated with that. But it's very minor and very small.

255
00:32:55,920 --> 00:33:01,360
It's just this is why an anangami can't do it. They can't charge people. There's an anangami who

256
00:33:01,360 --> 00:33:05,840
just sat by the side of the road and said, take what you want and leave what you think it's worth.

257
00:33:06,480 --> 00:33:10,240
Because he couldn't put a price tag or tell people, you must pay this or this is how much.

258
00:33:11,760 --> 00:33:16,160
I'm just saying, you know, take it and help me live so I can, you know, basically what a monk would say.

259
00:33:19,040 --> 00:33:23,040
Speaking of tomorrow, I'm going for alms. My first alms round, and

260
00:33:23,040 --> 00:33:29,120
well, since I've been here anyway. So I think they just called actually.

261
00:33:32,800 --> 00:33:40,240
Let me see. I don't know. Someone else. I think it's the Vietnamese monk in Toronto. Anyway,

262
00:33:41,120 --> 00:33:48,320
we'll call on back later. But she called tonight and they live downtown. It's this

263
00:33:48,320 --> 00:33:54,000
Laotian family couple. And we were talking about it. And they said, oh, no, I don't want to.

264
00:33:54,800 --> 00:33:58,480
We don't want to make it hard on you. And I said, but this is the way the Buddha would have it.

265
00:33:59,680 --> 00:34:05,520
So it's a proper thing to do. At least until it gets really cold.

266
00:34:06,880 --> 00:34:12,320
But tomorrow I should be fine. So I'm going to take a bus downtown to her house, to their house,

267
00:34:12,320 --> 00:34:17,680
and bring my ball. Let me get food for the day.

268
00:34:21,200 --> 00:34:22,160
So that's kind of neat.

269
00:34:29,120 --> 00:34:33,520
Fontaine, your book like you mentioned that during walking meditation that we should keep our gaze

270
00:34:33,520 --> 00:34:38,480
six feet ahead of the body, I find this difficult to do. And instead look at my feet as I'm

271
00:34:38,480 --> 00:34:43,280
stepping in order to stay more mindful. Is this from practice? Thanks.

272
00:34:44,320 --> 00:34:49,360
Yeah, there are problems with it. Again, tricks are not a good idea in general.

273
00:34:50,480 --> 00:34:56,800
There's no reason why looking six feet out should make it difficult to be mindful, except

274
00:34:56,800 --> 00:35:03,200
you know, it probably does for many people because we're not accustomed to this. But

275
00:35:03,200 --> 00:35:09,040
like any training, it's something you just have to work at. It's better to keep your eyes away

276
00:35:09,040 --> 00:35:13,920
from the object. Otherwise, your mind is more focused on the eyes. You're not more mindful. You're

277
00:35:13,920 --> 00:35:19,920
just more focused and it's not helpful for the practice. It's much more helpful to put your

278
00:35:19,920 --> 00:35:24,320
eyes somewhere where they're not going to, you know, where they're not going to get you in trouble.

279
00:35:24,320 --> 00:35:29,760
And that's really the best place is 45 degree angle because if you look up, well, they're going

280
00:35:29,760 --> 00:35:33,680
to get you in trouble. And if you look down at your feet, again, they're going to get you in trouble.

281
00:35:34,800 --> 00:35:40,000
So no, I wouldn't recommend it. And you're just going to have to. I mean, we're always looking

282
00:35:40,000 --> 00:35:44,800
for tricks. How I can be more quote unquote mindful, but it doesn't make you more mindful. It's

283
00:35:44,800 --> 00:35:51,760
just a trick that makes you more focused. And that's not really useful. It makes you tricky. It makes

284
00:35:51,760 --> 00:35:58,080
you look for tricks. How I can, how I can make it easier. I'm not trying to make it easier. It's

285
00:35:58,080 --> 00:36:04,160
really hard. And that's, you know, that's a part of it. It's kind of like cheating really.

286
00:36:05,680 --> 00:36:08,640
You have to do the difficult thing.

287
00:36:14,720 --> 00:36:18,640
For an hour or a half, is physical suffering really suffering since you're not affected

288
00:36:18,640 --> 00:36:29,200
mentally? I don't know. It depends how you look at it.

289
00:36:34,960 --> 00:36:41,840
Is there a commentary for the Jatakas? Yes. The Jatak, the commentary is the stories. Although

290
00:36:41,840 --> 00:36:47,520
there's also a word commentary. So I think it explains, yeah, there's an explanation of each of

291
00:36:47,520 --> 00:36:53,920
the verses, like word by word, just like with the Dhamapada. But those have never been translated

292
00:36:53,920 --> 00:36:58,000
either for the Dhamapada or the Jatakas, because it's hard to translate. You're dealing with

293
00:36:58,000 --> 00:37:03,040
poly etymology and it's not really all that useful to translate a lot of it.

294
00:37:05,520 --> 00:37:10,880
But we do have the stories translated in there on sacred dash texts dot com.

295
00:37:10,880 --> 00:37:21,200
I have a Buddha image at home and I want to know if there are any instructions on making

296
00:37:21,200 --> 00:37:31,440
an altar for it. Not for me, no. I mean, there's Buddha images weren't even endorsed by the Buddha.

297
00:37:31,440 --> 00:37:37,200
He seems to have been against them, saying that they are only based on imagination and they're

298
00:37:37,200 --> 00:37:50,160
problematic. So yeah. On the other hand, I don't want to be hypocritical because using a

299
00:37:51,520 --> 00:37:58,400
Buddha image was quite helpful for me when I was practicing. It seemed like it at the time.

300
00:37:59,840 --> 00:38:06,400
So it's useful for people who need that sort of thing, especially in the beginning of your

301
00:38:06,400 --> 00:38:16,880
practice. Buddha wasn't very interested in them. So I mean, it just means there's no sense

302
00:38:16,880 --> 00:38:19,440
that we would ever have any instruction on how to build an altar.

303
00:38:22,160 --> 00:38:27,040
There are some customs, though, like keeping it higher up, not putting it on the floor.

304
00:38:27,040 --> 00:38:37,280
Yeah. Don't put it on the subway card. Sorry. Don't mean to pick on those people who did that.

305
00:38:37,280 --> 00:38:42,320
Getting back to that conversation about protocol. If you're going to have an image of the Buddha,

306
00:38:42,320 --> 00:38:48,960
it should be a pretty high. Maybe the highest shelf or whatever? Yep.

307
00:38:48,960 --> 00:38:56,880
Monty, how do you remove physical desires such as the desire to eat or the desire to not feel

308
00:38:56,880 --> 00:39:04,160
physical pain? My desire isn't physical. There may be physical manifestations of desire or

309
00:39:04,160 --> 00:39:12,880
corresponding mental, or brain states, etc., but they're not mental. So the desire itself is

310
00:39:12,880 --> 00:39:22,720
always mental. Sorry, they're not desire. They are physical. Desire is mental. So the body's

311
00:39:24,480 --> 00:39:31,680
hunger response is not really hunger. It's just interpreted in that way by us, and it's been

312
00:39:31,680 --> 00:39:36,400
cultivated by us from lifetime after lifetime after lifetime.

313
00:39:36,400 --> 00:39:44,560
But all of that's neither here nor there. The more important is that there is some implication

314
00:39:44,560 --> 00:39:49,840
of what you're saying that perhaps you believe that being physical, they have nothing to do with

315
00:39:49,840 --> 00:39:56,160
meditation, but that's the point is that there's a separation between one's actual desire to eat,

316
00:39:56,880 --> 00:40:02,640
and the body's quote-unquote hunger response, because one is just physical, and you just can say

317
00:40:02,640 --> 00:40:10,560
hungry, hungry, and it stays or it goes. But when you want to eat, that's a problem, and then you

318
00:40:10,560 --> 00:40:18,560
say wanting, wanting, and try to do away with that.

319
00:40:18,560 --> 00:40:39,760
But our hands scratch themselves, and don't see why not. I mean, they wouldn't be disturbed by

320
00:40:39,760 --> 00:40:53,680
itches, but, you know, and with that, you're all caught up. All right, let's run while we

321
00:40:53,680 --> 00:41:23,520
still can. Thank you, Robin. Have a good night, everyone.

