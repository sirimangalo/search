Good evening everyone in broadcasting live April 25th.
This quote is about anger or it's about people.
Or it's about our reactions, our papancha.
There's this Buddhist word called papancha.
That means making more out of something than it is.
Translated sometimes as diversification, diversifying something simple.
Exaggeration over reaction.
It's when we take an object and we interpret it beyond the object.
Beyond what is truly impartial.
So anything beyond just seeing is seeing, hearing is hearing.
Anything beyond seeing something exactly as it is.
It's called papancha.
So the Buddha gives five examples.
There's a person who is impure indeed.
They do, they do unhosing things.
It's a choppungalow.
Upper isudha kaya samataro.
Their behavior of body is impure.
But parisundha, once he is samataro,
they are pure of verbal conduct.
So their speech is pure, but their actions are impure.
Some people are impure of speech, but of pure bodily conduct.
So they might speak harshly, but their actions are actually kind and gentoned.
Well thought, well reason mindful,
but their speech sometimes gets the better of them.
And they say things that are unpleasant.
Some people are impure in speech and impure in action.
So they do evil things and they say bad things.
But then there are some, but that person,
wait, let's look at the English.
A third person, their body and verbal behavior are impure,
but from time to time their mind is pure.
And time to time Kali in a kalong.
Labatitya, Kali in a kalong jita.
So we were wrong jita, so passadang.
They are quiet minds sometimes.
Even though they do bad things and they say bad things,
some time to time they are clear-minded.
What may verb those pure verbose impure?
Right.
A fourth type of person, their bodily behavior and verbal behavior impure.
And their mind is never, their mind is never pure.
And a fifth type of person, their bodily behavior and verbal behavior are pure.
And their mind, at least from time to time, becomes pure.
It becomes plastid.
It is an interesting.
And commentary says it's from time to time they have faith in good things and so on.
But sorry, this is sorry, put action.
This isn't the Buddha.
This isn't Buddha whatsoever.
This is sorry, put time.
And he says, for each of these five people one should remove resentment.
So it's interesting to think about this because even the fifth type of person,
a person who's bodily behavior and verbal behavior are pure.
And their mind is, at least occasionally pure.
So sometimes they still have defilements, but they're able to suppress them,
they're able to free themselves from them.
And they don't spill over into their bodies, their actions in their speech.
But even such a person is subject to resentment.
And so I think that's what's most interesting about this quote is,
it's not just about ways of, because then it goes on to have,
to give examples of how we should overcome this resentment and drive it out.
But do we need to?
But I think what's also interesting is the idea of,
as I said, our reactions to things and our overreactions to things.
So how we relate to other people specifically here.
We get angry, even that people who don't deserve our anger.
We get angry at people who ostensibly deserve it.
Who most people would agree deserve anger.
And you could say the same goes with greed.
We want things that are worth wanting and we want things that are not worth wanting.
It's conventionally speaking.
But the interesting thing about this is that it turns out our desire and our aversion is not related to the,
it is not dependent upon the nature of the object.
So it's not solely because something, it's not at all actually because something is bad that we get angry about it.
It's not because something is good that we get greedy about it.
Because it turns out we want things that are not worth wanting.
And we dislike things that are not like this.
We get angry at people who are pure.
And David Atlas, a good example.
David Atlas was the Buddhist cousin and he got very angry at the Buddha.
Super Buddha was his stepfather.
No, his father-in-law, sorry.
When he was in the Bodhisattva, the Bodhisattva got married and her father was quite angry when he left home.
And so he stood in the Buddha was going for alms and Super Buddha came and stood in his way.
He was very angry.
He was one of the five people for whom the earth opened up and swallowed him whole and dragged him down to hell.
Apparently that's a thing.
If you do something evil enough, the earth opens up and swallows you whole.
Super Buddha is one of the people.
David Atlas was another.
But to the Buddha, you know, so there was no reason.
There was again done nothing wrong.
He had done nothing worthy of resentment.
He had no...
It's interesting.
It's an interesting thing to say because sometimes people haven't done something wrong.
But they are pure.
There's the case where someone has done something worth resenting, worth hating.
They've been conventionally anyway.
Obviously we would argue that nothing is worth hating.
But for other reasons.
But it's interesting because it has nothing to do with the person.
There is the case of Angulimala.
Angulimala killed many people.
And then when he went for arms, he was beaten.
He was attacked.
But he was an arrow.
He was not an arrow hunt, but he was pure anyway.
He was trying to do the right thing.
I don't think he was an arrow hunt at that time.
I can't remember.
Maybe he already was an arrow hunt.
But he was pure.
And yet people still attacked him.
And so you might argue, well, he did bad things.
And so that's why people are angry.
But that's interesting.
It's useful.
And it's interesting how the...
Sorry put phrases this...
I mean, insofar as we can consider the difference between what people have done
and who they are.
Because it affects our ability to let go of resentment.
If we hold on to the past, it turns out to be kind of silly.
When you think about the justification.
You think about someone's past deeds as a justification for hating them.
Someone has done something terrible and then they change their way.
Often we want people to regret what they've done, but I'm not even sure that's necessary.
The person realizes that the bad things that they've done and that it was bad.
And they change their ways and they decide not to do bad again.
I don't think it's necessary to feel bad, but you've done feel sad, feel upset.
Or, you know, while I was such a terrible person.
But it's definitely necessary to, you know, if someone has changed their ways.
So the point of all this, the idea is that we resent for reasons that turn out to be unhelpful.
And so, sorry put out, offers these ways, these sort of conventional ways by which we can look at these people
and remind ourselves about their qualities of mind.
So if someone is, if someone is in pure body but pure of speech.
You should consider them like a rag.
If you find monks in all the days and even to date, to some extent they'll find cast off cloth.
And sometimes part of it would be rotten.
And just because part of the cloth is rotten doesn't mean you should throw the whole cloth away.
So instead you tear off the part that's rotten.
Throw that away and keep the part that's good.
And so that's how you should look at a person whose bodily behavior is bad but whose speech is good.
Who speaks kindly and who says good things but maybe someone who is unable to practice them, you know.
Someone who talks the talk but doesn't walk the walk.
You can at least think well, they do good things with their speech.
They say things that are kind, they say things that are pleasant and so on.
Even though they can't help themselves with their actions.
So the idea is to, you could say focus on the good, right?
Focus on the positive.
Which is an interesting strategy because, to some extent you don't want to ignore the bad.
At least in ourselves.
But in others it seems to be more useful to focus on the good.
You know, because that's what we want to gravitate towards.
We can't fix some desire.
We can't fix the world.
But if we focus on what's good in people, you know.
And just remove ourselves from what is bad.
So when people do bad things or say bad things, if we keep our distance, not get involved, not, you know.
Try and change them or try and convince them that it's wrong.
But we focus on the good.
That's what appears to me to be most beneficial.
When bad things happen, try and remove yourself.
Keep your work with the good.
Don't involve yourself with people's bad deeds.
Because you get caught up in the world if you don't.
So we're talking about social activism and bringing about peace and so on.
If we focus on the peace, don't get angry, upset about the bad.
Because otherwise resentment comes right.
You just focus on the good.
That's clearly what I put to saying.
The person's verbal behavior is impure, but bodily behavior is pure.
It's like a pool of water he says, covered with algae and water plants, covered with milk,
covered with scum.
And you're thirsty.
So you want to drink from this pool, but it's got this stuff on top that's kind of gross.
Well, what do you do?
You put your hands in the water and you part the algae and water and drink from the water.
So you go, you see you penetrate through their speech.
Someone might be harsh of speech.
Sometimes we react to people's speech.
If someone speaks not eloquently, not well.
Maybe they use harsh words.
There was this monk in Thailand who was very well-known for using,
actually, what turned out to be just old-school words,
but they become insulting terms when you address someone in Thailand as moon.
Moon is a way of saying you, but it's a very coarse way of saying it.
It's become that way.
And so he was kind of an embarrassment to the other monks.
And he was actually quite famous, but he had this bad habit of calling himself
goo and calling other people moon, which sounds quite improper.
And one time the king, I think, came or something.
And they said, oh, Venerable Suri, please, just don't talk.
Just be quiet and don't say we're afraid that you're going to say these things.
And so just be quiet when the king comes.
I think it was the king.
And King Kamsa, or maybe it's the supreme patriarch.
I can't remember some big person.
Kamsa notices that the old monk is not talking.
And he says, hey, what's the matter?
And he doesn't say anything.
What's wrong?
What's wrong?
And suddenly, bursts out.
And he says, moon won't let goo the fuck.
Maybe it was more funny in the time.
Sometimes people speak to their manner of speaking.
It gets to us.
I've had that happen to me where using the wrong words as a teacher.
And I've had people say, you can't just say that.
You have to be more tactful.
Sometimes some people are really bad at this.
Their speech is, they say the wrong things.
You know, maybe they express bigotry.
This happens a lot.
People say things that are hurtful.
Because speech is tricky.
It's easy to say something you don't mean, right?
But it's also easy to take some speech that someone really didn't mean
and assume that they meant it.
That happened where you say something and someone just takes it to heart
didn't realize that they were going to take it so poorly.
Take it so hard.
So we shouldn't do this.
When someone's speech, they say,
in Thai they say,
fangu wai hoo.
You hear something at the ear.
Keep it at the ear.
Don't let it get to your heart.
Whatever you hear, keep it at the ear.
Don't let it get to you.
You'll look at it for what it is.
See it objectively.
Because behind that there may be a person who doesn't really believe that.
Doesn't really believe what they're saying.
Doesn't really follow that.
It has purity.
So that's like the water.
And then if a person is both the impure in speech and body,
well, then you look at their mind.
Because sometimes their mind is pure.
So this is like a mud puddle.
If you find a mud puddle,
if you know anything about mud puddles,
when I grew up there were lots of mud puddles in our driveway.
As we lived in the forest,
we had a long driveway and it had potholes in it.
And so we always had mud puddles.
If you leave it alone, after some time,
it becomes pure on the top.
But you can't drink from it.
If you can't put your hands in it and take the water out
because you'll stir up the mud on the bottom.
This is like a person whose mind can be pure,
but their action in speech is not.
So I think the implication is that for the most part,
they do their minds are corrupt.
At least they do bad things and they say bad things.
But if you're kind to them,
and if you are soft on them,
then they have the potential to have a pure mind.
They're potential to do and say good things as well.
And so with this mud puddle,
what you do is you bend down
and you slurp it up like a cow.
Just put your mouth in it
and you can actually take the pure water.
So this is how you deal with people.
You think of people like,
well, if you, and he says,
and then you depart.
So the idea is don't hang around too much.
Don't get, don't, don't make, don't stir them up.
Because their mind can be pure.
Let's leave them, leave them to be pure.
Don't get caught up in the bad things.
And the fourth type of person,
it doesn't even have your deep mind.
He said, this sort of person
you should think of them as a traveler
who is gravely ill
and has left their village
to try and find a doctor,
maybe in another village.
But it was too far away.
So they're so far away from any help.
And they have no medicine.
They have no opportunity to meet with anyone
who can help them.
You see such a person,
what would you do?
You would be compassionate towards them.
You would have sympathy.
You would have concern for them.
And you would think, oh,
may they find suitable food and medicine
and someone to help them.
Because if they don't,
they will encounter calamity
and disaster right here.
If they don't, they will be in big trouble.
They will die.
Don't kiss a hate to my young,
my young police will be there
and I am beyond some opportunity.
So it's may they not,
may not this person meet with,
he translates that as wrong.
It's,
if they, it's may they not,
why should we think that?
Oh, yeah, no, he does translate.
That's not this man encounter calamity
and disaster right here.
So we should pity people who are totally evil.
They think, wow,
if this guy doesn't get some help,
like a sick person wondering along the highway,
there's no one to guide them.
Lost in the woods.
And the fifth type of person
who's pure and body, speech and mind,
so you think of them as a clear,
sweet, cool,
a pool pond with clear, sweet, cool water,
clean with smooth banks and so on.
So think of how great they are
because even such a person can come,
can,
uh,
arouse resentment in us.
It can be angry and such a person,
towards such a person.
Sorry, put the had monks angry at him
for a silly reason.
It's easy for us to get angry
if we have,
it's like fire,
fire can easily spread.
If we're not careful,
if we have the fire inside us,
it will burn,
it can burn us, it can burn others.
But, so the,
the suit is,
mainly about the sort of conventional ways
of dealing with our problems.
And this is somewhat important.
It's kind of like why we practice loving kindness,
meditation, why we practice mindfulness of death.
I mean, these are not
meditations that lead directly
to insight about impermanence,
suffering and oneself.
They're not technically what leads us to Nirvana.
But they're very supportive.
And there's lots of things that are supportive
of our practice.
But it was often talking about anger.
Especially because when you're living in a monastic community
or in a meditation community,
meditative community,
overtime tempers flare,
you know, being cooked up,
not having the opportunity to go out
and engage in your,
the addictions and desires,
day in and day out having to stay put,
eating food that you haven't chosen to eat,
eating whatever is given,
sleeping on the floor,
it's easy to get irritable.
So anger was a big one
that the Buddha was often talking about.
But,
I think,
it's quite interesting,
but I think also quite interesting
is to address this idea of how our reactions don't,
don't have anything to do with the object.
There's no reason to like something
or dislike something.
This is very hard for us to understand.
Because we think that pain is worth disliking.
Pleasure is worth liking.
It's why we call it.
It's somehow intrinsic,
but it's not.
There's nothing intrinsically desirable
about pleasure,
nothing intrinsically undesirable about pain.
There, in fact,
none of them worth desiring or presenting.
And you can see that with,
this is an example of this.
You can hate someone who's pure.
It's perfectly pure.
You can hate the Buddha.
It's possible.
We know that it's not a deep truth.
It's just kind of,
what's deep is that it penetrates to have everything.
Turns out that our likes and dislikes
don't have any rational cause to them.
Not only do they not have any benefit,
but it's also very important.
But they don't have any relationship.
There's nothing about pain that
isn't intrinsically bad.
It's kind of interesting to think about why we dislike pain.
From a Buddhist perspective,
because from a Buddhist perspective,
we've latched onto this body
from conception,
watching it grow,
growing with it,
as a part of ourselves,
seeing it as us, as me, as I.
And so pain kind of develops
as this worrying phenomenon
and experience.
That threatens that.
No pain is pain threatens
the stability and the cohesion of our self.
You know, what we've come to identify as I.
As an example,
your pleasure is associated with the health
and the well-being.
When you get good food,
you feel pleasure.
When you feel strong, you feel healthy.
You know?
And so we develop these.
It's part of how we develop
our attachment to pleasure and our aversion to pain.
Because of how attached we are to this body,
how we attach to our experiences
and how we become attached
to everything in the world around us.
But we do that to ourselves.
We do that on our own.
And there just have it.
There are conditioned behavior
that you can unlearn,
that you can change.
I was having an interesting conversation
of a week or so ago.
We're speculating on whether
a person who is homosexual
can become straight,
because Christians claim
some Christians claim that that's possible.
That's what they should do.
So we were saying,
well,
I think we both agreed that he's not Buddhist,
but he's in my philosophy class.
No, he isn't.
He's in my Latin class,
but he's in philosophy.
I think he's a philosopher.
And he was...
We agreed that it's probably possible.
I mean, actually,
I don't know.
It couldn't be genetically
wired or something.
Almost sexuality,
heterosexuality,
but we would argue
it has a lot to do with past lives.
But whether it then becomes genetically coded
or there's a relationship with,
because pleasure also depends
on the body.
Certain experiences,
they're interpreted by the blame,
brain,
and then they can produce certain hormones, right?
So it made me that some people are only
able to physically
able to give rise to pleasure
for the opposite gender.
It seemed kind of silly
to think that,
but it made me awesome.
Anyway,
what we were saying is,
well, why?
You know, what's the difference
between heterosexuality
and homosexuality?
I guess that's a good example of this,
is that our judgment
of what's good
and what's bad,
what's proper,
what's improper.
It turns out to be totally unrelated
to reality.
I mean, there are certain
basic truths
beyond those...
beyond those...
it's all conventional.
So the basic truths are
impermanent suffering
and non-self, basically,
but everything we experience
turns out to be unstable,
unsatisfying,
and uncontrollable.
Certainly not worth our
efforts at trying to maintain
or fix.
So that's what we see in meditation.
We start to see the way our
mind doesn't work,
according to our desires,
our body doesn't work
the way,
according to our desires,
and the world around us doesn't.
And so we use these conventional
means of showing ourself
that,
and, well,
and looking at this
suit is about looking
at the...
looking at ways
or looking at things
in a way that helps us
free ourselves
from these attachments
and resentment.
So there are conventional ways
of doing that.
If it's very strong anger like this,
this is a practical teaching
because in the world
it's not just about meditation
for most of us.
We carry grudges.
We get angry
and that prevents you from actually.
You can't just
say meditate on it because
we're too...
we're too caught up
in the anger to meditate.
So in order to help us meditate
we use these
to calm ourselves down.
Think about the good
in the person
that will calm you down
and it will help you
become more objective.
And then you'll be able
to meditate on it.
Easier.
Okay, so that's...
that's all I have to say about that.
That's the number for this evening.
Thank you all for tuning in.
Anyone has any questions?
I'm happy to answer them
otherwise we'll call it a night.
Two meditators here now.
Thank you guys, saw Michael.
We have...
We have...
Shoot.
How come I forgot your name?
Mark.
I got Mark.
Michael and Mark.
Mark, where are you from?
I'm sorry.
Right.
German.
He came all the way from Germany.
Michael's from America.
You guys can go.
That's all for today.
Oh, is Robin here?
Robin, you want to come on the hangout?
I mean, I think there's just a link, but...
Right.
So Robin puts up a link.
So yeah, it's just if anyone wants to,
you can go and click on that link.
We're not, you know, actively seeking donations,
but yeah, yeah, this is a passive thing.
There's no need for this.
This is, if you want to join in this...
Great.
Indeed with us.
Hello, punty.
Hey, Robin.
I just linked our campaign that you were talking about last night
for anyone that might want to join in our efforts to send you to Thailand
with some robes, garage on top.
Mm-hmm.
So we've got a little campaign put together.
I just linked it in YouTube and the meditation site.
But is this going to be similar to what you did a couple of years ago?
Yeah, not on the big scale, right?
Probably not, yes.
I mean, we're not pushing this.
Who knows if we get so many, many people,
but yeah, if we get many people, then it will be.
Large scale.
It's large as it gets.
But that was like, that was a lot more.
Each robe is $108.
Is that what we're estimating?
I think that's what they were a couple of years ago.
You just know...
I made because hundred and eight is a good number,
so it's approximate.
Maybe a little more, doesn't really matter.
Maybe a little less.
No, a little less actually.
It turns out because we're getting them from a...
Yeah, there's...
Where we usually get them is quite expensive,
or I would get them, but there are better places to get them,
especially in bulk.
And not so expensive.
I don't think it's $108, but I can't remember enough.
Anyway, they're shipping and handling and all of that.
So...
But for people that maybe haven't been watching the last few weeks,
Dante is going to Thailand and Sri Lanka in June.
We'll be there between the two countries for the full month of June.
And in going to Thailand, he'll be able to see his teacher
and a couple of people in our meditation group thought it would be nice
to have a gift to Central and Chantam.
You know, kind of on all of our behalf, so anyone who's interested,
there is a link for that.
How we should trip to New York, Dante?
Yeah, it was okay.
We had this interesting...
I like to talk, you know?
I'm at that one.
So we had the three of us.
Sudaso and Sudaso.
And this Aya Yeshi, is your name?
She's Mahayana Bhikuni.
And myself.
And we each had to say a little bit on social...
What was it?
Responsible social action.
Turns out I didn't know this when we talked.
We talked that she runs some kind of charity or orphanage
or something.
She takes care of children in India.
I didn't know this.
And so they said something quite brief.
And I hadn't prepared anything.
But I just started talking.
And then when the organizers started laughing,
and other people started laughing at some of the things
I was saying, because they're saying, you know,
usually a lot of what I say here,
it's just that on the ostensibly social activism
is useless because in the world,
the humanity is doomed.
The Earth is going to burn to a crisp.
I didn't realize that this wasn't something
that all Buddhists kind of considered.
And one woman misinterpreted it.
She interpreted that as being my view
that I think it's useless.
But she wasn't really listening to me because I said
that we have to address that.
Because arguably there's that argument can be made.
And as a result, I think we can't focus on goals.
We shouldn't focus on attaining this or that goal
or being worried about actual outcomes.
The reason why we do good deeds
is for the purification of the mind.
And so our minds and the minds of others
were the help that we're giving to people.
There's nothing to do with the outcome in this world
or saving the climate or whatever.
It has to do with the actual activity
of doing good things, cultivating goodness.
And no one was angry.
Except this one woman really didn't really
didn't thought I was wrong.
I don't think she really understood what I was saying.
Because, and they were saying,
another person said it sounded nihilistic.
Anyway, it was quite an interesting time.
So focus on the efforts and the outcome.
Yeah, it didn't really go for all that.
But the other thing with people whose Buddhism
is not exactly like my Buddhism.
Our Buddhism, I think, as a community.
They're much more socially conscious, I think.
But they perhaps don't know you as well either.
I mean, you're not a person who's just throwing up your hands
and just giving up.
I mean, you do notice it just to say, because the first two,
he gave a very terra vada outlook.
She gave a very Mahayana outlook.
And I gave some kind of totally way out there.
I was like, see.
And I ended up monopolizing it.
I think I took far more time than the other two.
Because I had a lot to say.
It turned out.
I didn't know if I would.
I just started talking.
So two things.
The first was that.
Why did we do social work?
Why did we try and help?
And my argument was that it was interesting.
I'm more excited with doing social work.
What helping society?
So it wasn't actually that I was saying.
And I actually argued with Sudhassur afterwards
because he thought it wasn't important.
And I said, I don't know.
I agreed with IASI, I think that you're not truly inclined.
If you see someone hurting, you have to suppress
the desire to help them.
If it's proper to help a person,
it takes the filement not to help them think.
Maybe that's going too far.
But I would think the natural inclination is to do good thing.
And then the other part of what I said was how we should do good things.
And that's very terrifying.
I mean, it's very Buddhist.
I think to look at your intentions.
You can't just, you know, they're saying,
don't just do something, sit there.
So I said to them, I said, if you've ever heard this Buddhist saying,
don't just do something, sit there.
Why do people say that?
Because we do, we just do something.
I have a problem.
I'm just going to fix it.
How are you going to fix it?
Do you know how to fix it?
If you don't know how to fix it, don't go and try and fix it, right?
If you're not a plumber, don't try and fix the plumbing.
Not an electrician.
You don't do the wire.
You're an electrician.
Don't do the wire.
And then you get people who are doing good things,
but they're doing it in like a militant, angry way.
You know, people get involved in good causes,
but they become angry and militant about it.
And it's got to work.
I told my dad this.
And he was just talking to him this afternoon.
And he said, yeah, I mean, you see that a lot in the
environmental movement.
He was asking me my trip in New York.
And I mentioned this.
I said, yeah, I had this interesting experience.
And they said, well, yeah, no, I agree.
Because he is very much involved in the environmental
movement for many decades.
And I knew that following him and following in his footsteps,
I've seen him as well.
People get angry.
And then they burn out.
He said, and then they don't actually get a complacent thing.
They're so passionate about something.
And then they burn out.
And then they move on and do something else.
And they never accomplish their goals.
So yeah, that's what I was talking about.
We meditate in a subway station.
It's just that they're immeditated.
People just took our pictures.
It's really interesting.
Everyone wanted to take a picture of the monks meditating in
the sunboard.
So which was, yeah, I was kind of skeptical about it.
But I think you're doing a great thing.
This is cultivating consciousness to do.
Because what's happening in the subway
is there's all these performers.
Some guy that's right beside us, I think
just to find them sort of an out of the way
place that we wouldn't be in people's way.
And they were not so happy about that.
But it worked.
At first, it just wasn't quite edgy enough.
I don't think.
But we actually went under this September 11th
moment with all the names from all the victims of September 11th.
I thought that was quite neat place to sit and meditate.
And people were walking by and so on.
Some people took the pamphlets that we had.
But then they suggested we go right.
The busiest are right beside it.
It was a little indent where we could sit.
We weren't in their way.
But we were right beside people walking.
Very busy.
And right across from us, there was a guy
with a saxophone or a woman.
I don't know.
Someone with a saxophone.
And I was so, it was quite loud.
It was so sad.
It was quite difficult to meditate.
But first, for our tradition, it's not hard.
But it's another good thing to show that the meditation
can happen.
But it's kind of a performance.
You know, you're doing something.
Of course, those people are there for money,
which we of course would.
It was kind of the opposite.
We had a suitcase, which was a common street
performer thing to have.
But our suitcase said free take one.
And it was at a pamphlets and then postcards.
Still, I think I heard that some people didn't put money
in it.
I'm not sure.
But I think that was worthwhile.
It sounds like fun.
The New York subways are awesome.
And the quality of the street performers
is really outstanding sometimes.
I love the New York subways.
Yeah.
Okay, so no questions.
There's a link to the campaign if you want to join us
in offering robes.
So here is in the hangout here.
I'm not sure if they have a question.
Oh.
Hi, Samir.
Maybe it doesn't have a mic.
It's called the night.
Have a good night, everyone.
Good night, Andre.
Yeah, tomorrow.
Good night.
