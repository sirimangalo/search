1
00:00:00,000 --> 00:00:05,200
Hi, welcome to this edition of Ask a Monk.

2
00:00:05,200 --> 00:00:09,000
The latest question is, I have a fear of connecting to people.

3
00:00:09,000 --> 00:00:12,360
Harsh criticisms or dirty looks seem to cripple me.

4
00:00:12,360 --> 00:00:17,240
When a person insults me or if I feel someone is angry with me, I lose all energy and

5
00:00:17,240 --> 00:00:19,520
really do feel deeply cut.

6
00:00:19,520 --> 00:00:22,080
Do you have any advice?

7
00:00:22,080 --> 00:00:27,520
Yeah, well meditation really helps with this.

8
00:00:27,520 --> 00:00:34,280
I think it's a direct answer to your question is to acknowledge the feelings that come

9
00:00:34,280 --> 00:00:35,280
up.

10
00:00:35,280 --> 00:00:40,560
The point is that the other person's reality and our reality are two different things.

11
00:00:40,560 --> 00:00:42,320
No one can hurt you.

12
00:00:42,320 --> 00:00:48,080
There's no one in this earth in the universe that can cause suffering for you.

13
00:00:48,080 --> 00:00:51,720
All they can do is impinge upon your six senses.

14
00:00:51,720 --> 00:00:53,880
They can make you see certain things.

15
00:00:53,880 --> 00:00:55,920
They can make you hear certain things.

16
00:00:55,920 --> 00:01:00,880
They can make you smell, taste, feel, or even possibly think certain things.

17
00:01:00,880 --> 00:01:04,680
But they can't make you react to those things in a certain way.

18
00:01:04,680 --> 00:01:09,040
They can't say, when I show you this, may you get angry.

19
00:01:09,040 --> 00:01:15,040
When I let you listen to that, may you hear this, may you get attached or so on.

20
00:01:15,040 --> 00:01:17,040
May this or that arise in your mind.

21
00:01:17,040 --> 00:01:18,040
Only you can do that.

22
00:01:18,040 --> 00:01:20,280
And that's where your choice arises.

23
00:01:20,280 --> 00:01:25,960
So when someone says something to you, when someone criticizes you or gives you a dirty

24
00:01:25,960 --> 00:01:30,760
look, the problem is not the dirty look or the criticism.

25
00:01:30,760 --> 00:01:33,120
The problem is your reaction to it.

26
00:01:33,120 --> 00:01:38,520
Your mind is generally just waiting there for something to cling to.

27
00:01:38,520 --> 00:01:43,720
And so they say the bad things in our minds are like a snake in the grass.

28
00:01:43,720 --> 00:01:49,320
When you look at a field of grass and it looks very peaceful and very wonderful, it's

29
00:01:49,320 --> 00:01:50,320
easy to be deceived.

30
00:01:50,320 --> 00:01:54,400
And you don't know what's in the field and you go walking through it, feeling happy

31
00:01:54,400 --> 00:01:55,400
and peaceful.

32
00:01:55,400 --> 00:02:00,320
And then when the snake, when you get near the snake, it jumps up and bites you.

33
00:02:00,320 --> 00:02:03,440
But until that time it was quite a peaceful scene.

34
00:02:03,440 --> 00:02:04,680
And the mind is like that.

35
00:02:04,680 --> 00:02:07,920
The mind is just waiting for its prey.

36
00:02:07,920 --> 00:02:13,960
So we'll just be sitting there and as soon as someone looks at us, we jump on it.

37
00:02:13,960 --> 00:02:16,480
As soon as someone says something to jump on it.

38
00:02:16,480 --> 00:02:18,680
And we can change that through the meditation.

39
00:02:18,680 --> 00:02:22,760
So when you see some things they're seeing, seeing when you hear someone, when someone's

40
00:02:22,760 --> 00:02:27,200
saying nasty things, do you say hearing, hearing, that's quite difficult.

41
00:02:27,200 --> 00:02:31,400
That takes a lot of training, but we are training to get to that level.

42
00:02:31,400 --> 00:02:37,840
And it does work if it's continuous and you can eventually remind yourself, even in daily

43
00:02:37,840 --> 00:02:43,040
life, when it's very difficult to do so, if you're careful and if you are, especially if

44
00:02:43,040 --> 00:02:48,280
you're prepared for it, there's a lot of people have told me that they knew there was

45
00:02:48,280 --> 00:02:53,240
a confrontation coming up and so they reminded themselves and they read it themselves.

46
00:02:53,240 --> 00:02:57,040
They said, if they yell at me, I'm going to say hearing, hearing, if they looked at

47
00:02:57,040 --> 00:03:00,280
me, seeing, seeing and so on.

48
00:03:00,280 --> 00:03:04,320
Just to be mindful and if you prepare yourself in advance, then when the situation comes

49
00:03:04,320 --> 00:03:09,520
up, you find that you're really able to deal with it in a much more profitable way.

50
00:03:09,520 --> 00:03:13,520
The easier thing to do is once these emotions have already arisen, by then it's too late,

51
00:03:13,520 --> 00:03:15,120
then you're already suffering.

52
00:03:15,120 --> 00:03:19,800
That you can minimize the impact and you can stop it from snowballing because anger leads

53
00:03:19,800 --> 00:03:23,560
to thinking about it again and thinking about it again leads to getting angry again and

54
00:03:23,560 --> 00:03:24,560
so on.

55
00:03:24,560 --> 00:03:25,560
And it's a cycle.

56
00:03:25,560 --> 00:03:28,840
And you could break that at any time when you say to yourself, for instance, thinking,

57
00:03:28,840 --> 00:03:37,040
thinking, or even angry, angry when you're upset at someone, just to pick that as your

58
00:03:37,040 --> 00:03:43,200
meditation object and focus on it and see it clearly as it is and don't make the link.

59
00:03:43,200 --> 00:03:48,840
Oh, I'm angry, I have to do this and this and this to them and then thinking about what

60
00:03:48,840 --> 00:03:54,320
they did to me and getting angry again and so on, going in this cycle, cut it off wherever

61
00:03:54,320 --> 00:03:55,320
you can.

62
00:03:55,320 --> 00:04:00,040
When you feel angry, say angry, angry, when you feel sad, sad, when you feel depressed,

63
00:04:00,040 --> 00:04:03,840
depressed, depressed, whatever.

64
00:04:03,840 --> 00:04:07,160
One thing you mentioned about losing all energy, I wouldn't worry about that.

65
00:04:07,160 --> 00:04:13,880
That's common associating with people who are critical and mean and nasty and so on.

66
00:04:13,880 --> 00:04:17,520
I think even if you're enlightened, it can be very tiresome to be around.

67
00:04:17,520 --> 00:04:21,760
They say even the Buddha was very tired when he was around such people and he would often

68
00:04:21,760 --> 00:04:25,080
dismiss them and say, go and meditate.

69
00:04:25,080 --> 00:04:29,440
So one thing that you have to acknowledge is that if you're going to live your life in

70
00:04:29,440 --> 00:04:33,600
a peaceful and happy way, you have to surround yourself with peaceful and happy people, with

71
00:04:33,600 --> 00:04:34,920
nice people.

72
00:04:34,920 --> 00:04:38,080
If you can't do that, just try to stay to yourself.

73
00:04:38,080 --> 00:04:41,680
The greatest treasure that we have is our solitude.

74
00:04:41,680 --> 00:04:45,800
If we can manage to stay to ourselves as much as possible, we'll find that we're a lot more

75
00:04:45,800 --> 00:04:49,600
peaceful and a lot happier and a lot less lonely.

76
00:04:49,600 --> 00:04:52,720
We find that when we learn to live alone, we learn to stay alone.

77
00:04:52,720 --> 00:04:59,520
We can then be around anybody and we don't feel so much stress and suffering.

78
00:04:59,520 --> 00:05:05,480
We learn to understand ourselves and come to grips with who we are and we live like an

79
00:05:05,480 --> 00:05:10,480
island unto ourselves, we don't rely on anyone else.

80
00:05:10,480 --> 00:05:14,880
The part of the problem is that is that our happiness generally relies on other people.

81
00:05:14,880 --> 00:05:16,680
It depends on other people.

82
00:05:16,680 --> 00:05:21,920
We want other people to accept us and to like us and that's the only way we can feel happy.

83
00:05:21,920 --> 00:05:25,400
And until we learn to overcome that and just be happy in ourselves and not need other

84
00:05:25,400 --> 00:05:32,080
people's praise and appreciation and acceptance, then we'll really be able to stand on

85
00:05:32,080 --> 00:05:36,440
our own two feet and be an island unto ourselves as the Buddha recommended.

86
00:05:36,440 --> 00:06:03,040
Okay, so I hope that helps and if there's more questions, please keep them coming.

