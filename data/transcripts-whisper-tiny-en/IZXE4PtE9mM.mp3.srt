1
00:00:00,000 --> 00:00:08,960
Hi, so this is a question on whether awful states of mind or negative states of

2
00:00:08,960 --> 00:00:13,660
mind are addictions and so on are considered wrong concentration or

3
00:00:13,660 --> 00:00:22,360
micha-semati. Now I'm not really sure about the technical details here but

4
00:00:22,360 --> 00:00:29,640
just at the top of my head, micha-semati is wrong concentration is when you

5
00:00:29,640 --> 00:00:34,920
focus your mind on something that is associated with negative states. So this

6
00:00:34,920 --> 00:00:41,280
is at least bordering on wrong concentration but I think the point here is that

7
00:00:41,280 --> 00:00:46,280
these things are probably arising on their own so there's probably no

8
00:00:46,280 --> 00:00:51,160
conscious focus on it. Wrong concentration is when you deliberately focus on

9
00:00:51,160 --> 00:00:55,640
something negative and and encourage it or or incorporate it into your

10
00:00:55,640 --> 00:01:00,080
meditation like greed or anger but when these states come up then we're not

11
00:01:00,080 --> 00:01:04,200
talking about wrong concentration. We're talking about meditation practice where

12
00:01:04,200 --> 00:01:08,080
you're considering and contemplating the negative and positive states which

13
00:01:08,080 --> 00:01:13,760
according to the tradition it's it's perfectly proper and correct to

14
00:01:13,760 --> 00:01:18,760
contemplate so I would encourage the contemplation of these things to see

15
00:01:18,760 --> 00:01:22,760
that they are are negative and to see that they are unhelpful to see for

16
00:01:22,760 --> 00:01:27,080
yourself not just pushing them away and and trying to forget about them. They

17
00:01:27,080 --> 00:01:32,120
shouldn't be repressed they shouldn't be forgotten about or ignored they

18
00:01:32,120 --> 00:01:36,600
should be analyzed and understood and once you understood understand them

19
00:01:36,600 --> 00:01:40,800
you'll you'll be able to let go of them simply by saying to yourself like

20
00:01:40,800 --> 00:01:45,680
king like king wanting wanting or focusing on the feelings associated with

21
00:01:45,680 --> 00:01:51,800
it as in happy happy if it's a negative one angry angry pain pain or

22
00:01:51,800 --> 00:01:58,120
so okay so I wouldn't worry about these things but obviously paying very

23
00:01:58,120 --> 00:02:02,920
good very close attention to them not encouraging them not tolerating them

24
00:02:02,920 --> 00:02:08,600
in the sense of thinking it's okay to to follow your addictions and so on

25
00:02:08,600 --> 00:02:14,600
they should be very carefully acknowledged contemplated and and discarded

26
00:02:14,600 --> 00:02:20,920
for being negative okay I hope that clears that up it's not it's not not

27
00:02:20,920 --> 00:02:23,640
wrong that these things are coming up but they shouldn't be encouraged and

28
00:02:23,640 --> 00:02:53,480
they certainly shouldn't be focused on okay give the questions coming

