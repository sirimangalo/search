1
00:00:00,000 --> 00:00:07,000
I'm beginning to see that finding true happiness in worldly activities is not possible.

2
00:00:07,000 --> 00:00:10,000
However, I need to have a working life.

3
00:00:10,000 --> 00:00:17,000
I feel apathy or a lack of motivation for these regular worldly duties.

4
00:00:17,000 --> 00:00:27,000
Will my motivation for daily regular work return as I continue to develop my practice?

5
00:00:27,000 --> 00:00:34,000
Yes.

6
00:00:34,000 --> 00:00:45,000
Well, that's the main part of the end, so let's say so.

7
00:00:45,000 --> 00:00:54,000
These things happen in a course of meditation or when you practice throughout daily life,

8
00:00:54,000 --> 00:01:03,000
but this state of mind is as impermanent as any other state of mind.

9
00:01:03,000 --> 00:01:20,000
When you live your life and are aware or are not aware of what you are doing when you're mindful

10
00:01:20,000 --> 00:01:28,000
or were not being mindful, everything changes, especially the mind states.

11
00:01:28,000 --> 00:01:40,000
So this apathy and lack of motivation is happening in your mind.

12
00:01:40,000 --> 00:01:49,000
It is something that when you say you feel apathy, it's probably not a physical feeling.

13
00:01:49,000 --> 00:01:55,000
It might become physical, but probably in the beginning it's really mental

14
00:01:55,000 --> 00:02:00,000
and then takes over of your body and then you don't move anymore.

15
00:02:00,000 --> 00:02:13,000
But as you describe it, I would say it started as a mental thing that the motivation just slid away.

16
00:02:13,000 --> 00:02:20,000
This has to happen, this should happen, and it's not the bad sign that it happens

17
00:02:20,000 --> 00:02:25,000
and you shouldn't worry too much about it that it has happened.

18
00:02:25,000 --> 00:02:37,000
When you continue and you understand and you let go of your apathy and your motivation

19
00:02:37,000 --> 00:02:43,000
or lack of motivation, when you just accept things as they really are,

20
00:02:43,000 --> 00:02:47,000
then the motivation will come back.

21
00:02:47,000 --> 00:02:54,000
It will of course be different than before, it will not be so enthusiastic anymore.

22
00:02:54,000 --> 00:02:59,000
Like, oh, I'm going to work, I don't know if you ever had that.

23
00:02:59,000 --> 00:03:12,000
But it might become more seeing the need of doing things that have to be done and doing them

24
00:03:12,000 --> 00:03:28,000
in order, in the order as they come and in the order that they have to be done.

25
00:03:28,000 --> 00:03:37,000
So you should really just continue to practice as you practiced before and be mindful

26
00:03:37,000 --> 00:03:51,000
and to see that you don't find happiness and worldly activity is a great gift

27
00:03:51,000 --> 00:03:58,000
that you have found or that you got from the practice.

28
00:03:58,000 --> 00:04:06,000
Don't throw that away because you want to be ready for your work.

29
00:04:06,000 --> 00:04:09,000
This is what a therapist would do with you.

30
00:04:09,000 --> 00:04:18,000
They would tell you stop practicing and make your mind available for all the worldly things again.

31
00:04:18,000 --> 00:04:22,000
Don't throw away that gift that you got.

32
00:04:22,000 --> 00:04:28,000
Try to go through it and acknowledge it as it is.

33
00:04:28,000 --> 00:04:39,000
It is maybe hard now, but it will not be always like that.

34
00:04:39,000 --> 00:04:49,000
There is a danger, of course, when you give in too much into your lack of motivation

35
00:04:49,000 --> 00:05:00,000
and when you indulge in apathy, when you say, oh, I'm so lazy, oh, I'm so demotivated

36
00:05:00,000 --> 00:05:16,000
and you let your mind and your body get drowsy, then there is a real danger that you will not find out of it.

37
00:05:16,000 --> 00:05:27,000
So the most important thing for you now is to keep practicing, to keep being mindful.

38
00:05:27,000 --> 00:05:37,000
Yeah, I mean, the first thing I wanted to say was, of course,

39
00:05:37,000 --> 00:05:38,000
it's going to demotivate you.

40
00:05:38,000 --> 00:05:43,000
It's going to make you want to go and live in the forest and leave your job behind.

41
00:05:43,000 --> 00:05:54,000
But then I came to the same conclusion that, no, actually, it doesn't mean that you have to be

42
00:05:54,000 --> 00:05:58,000
that there will be anything that gets in the way of your job.

43
00:05:58,000 --> 00:06:07,000
I think maybe the mistake is thinking that you somehow have to be excited or motivated in order to do work.

44
00:06:07,000 --> 00:06:17,000
You should, through the practice, you learn the basic principle of only doing things that have purpose.

45
00:06:17,000 --> 00:06:22,000
And so you find yourself unable to do things that have no purpose.

46
00:06:22,000 --> 00:06:31,000
You'd be unable to work for the purpose of getting high status in society, or for becoming rich,

47
00:06:31,000 --> 00:06:36,000
or for building up an empire, or a career, or becoming famous, or any of these things.

48
00:06:36,000 --> 00:06:42,000
You'll be unable to do that eventually in your practice because you're unable to do useless things.

49
00:06:42,000 --> 00:06:45,000
And these are all things that are useless or even unbeneficial.

50
00:06:45,000 --> 00:07:02,000
And they create problems in your practice. So the problem is thinking that you need to truly appreciate the work in order to do it.

51
00:07:02,000 --> 00:07:12,000
The work itself has some purpose. The purpose in most cases is to fulfill the needs.

52
00:07:12,000 --> 00:07:20,000
You know, your basic needs of food and shelter and clothing and so on.

53
00:07:20,000 --> 00:07:25,000
And in that regard, you will find that the meditation is actually a great boon for your practice.

54
00:07:25,000 --> 00:07:35,000
People who continue to work in the world after having practiced find themselves in general able to work quite efficiently.

55
00:07:35,000 --> 00:07:43,000
And their employers appreciate them more and will often favor them.

56
00:07:43,000 --> 00:07:53,000
In fact, because of their lack of, you know, contrary mind states, like gossiping and laziness and so on.

57
00:07:53,000 --> 00:08:04,000
A person who practices meditation, of course, has a great amount of energy and the ability to put out energy in repetitive monotonous tasks through their practice.

58
00:08:04,000 --> 00:08:25,000
So I think it actually is a false demodivation that meditators often get because they read in the suitors about these monks living often caves and so they think, oh, I can't I do that and they feel depressed because they have to work an office job and thinking that they're not really living a lie or something like that.

59
00:08:25,000 --> 00:08:34,000
When you're clear about your path and about how to get to the point where you're able to live a meditator life.

60
00:08:34,000 --> 00:08:42,000
So slowly I'm going to work and I'm going to clear up my debts and my burdens and so on.

61
00:08:42,000 --> 00:08:46,000
Then you find that you're able to put all of your effort into work.

62
00:08:46,000 --> 00:08:59,000
Like for instance, when I knew that I was needed money to save up to go to Thailand to become a monk, it was really easy for me to raise money to work in any job.

63
00:08:59,000 --> 00:09:09,000
I did tree planting and it was really easy for me to put out an intense amount of effort and really actually push the limits even though not being a physical person.

64
00:09:09,000 --> 00:09:21,000
When I was working in a restaurant, I found I had great patience and they were really happy to have me because I wasn't cheating them and I wasn't trying to get out of work and so on.

65
00:09:21,000 --> 00:09:29,000
I was willing to take the worst jobs with patience because I was able to be mindful of it and so on.

66
00:09:29,000 --> 00:09:39,000
I would think that it should help you in your work once you can give up that idea that somehow this is meaningless and so on and focus on what is the meaning of it.

67
00:09:39,000 --> 00:09:48,000
You're working to fulfill some purpose and so it's actually irrational for you to spend your time moping and apathetic during the work.

68
00:09:48,000 --> 00:09:54,000
You have to see clearly what is the point of it and do it just for that reason.

69
00:09:54,000 --> 00:10:00,000
There's one other thing I want to mention.

70
00:10:00,000 --> 00:10:14,000
In the eightfold novel path is the right livelihood mentioned and I think when you have a job that is not going against right livelihood,

71
00:10:14,000 --> 00:10:34,000
then it is perfectly all right to live the working life and to live the lay life and to work and you should get over your lack of motivation because it's not a bad job that you are doing.

72
00:10:34,000 --> 00:10:54,000
When you are doing a job which is going against the right livelihood, then you should consider to do something else and then your lack of motivation has possibly other reasons than we mentioned now.

73
00:10:54,000 --> 00:11:04,000
So that is one thing you should consider.

