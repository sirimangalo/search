okay so welcome everyone to our weekly deer park session and because we only
have a session once a week I thought it's probably useful not just to give
talks but to also have some sort of guided introduction to actual meditation
practice so this talk is going to be a little bit more hands-on so to speak
today's talk the topic is wisdom because of course wisdom and Buddhism is the
highest virtue the highest quality which we're hoping to attain hoping to gain
from our practice the Buddha said compared wisdom like the moon amongst
the stars when you look at the night sky you see a lot of bright lights and you
say it it's amazing how bright the stars are but then when the moon comes up
you can see that it's it's on a whole other level it's a whole other volume of
brightness in comparison to the stars and so we should keep this in mind when
we think of what is it that we're trying to gain trying to get crying to attain
in our practice and we should take this as an understanding that what we're
really hoping to gain is is wisdom and so this is of course a very important
topic of discussion and I think it's something that perhaps many people who
surf the internet and virtual reality and so on are not thinking to
to obtain in fact often the internet and these sort of game-like worlds are
considered to be sort of like a past time or a break from our spiritual
quest and and many people come here because perhaps they're though they know
they're looking for something they don't realize the importance of wisdom and
so it may have never entered their minds that wisdom was of any benefit but we
can see even in our daily lives how important wisdom is how important it is
simply to live our lives simply to be successful if we want to succeed in our
work we need a certain amount of wisdom we need wisdom having having learned
things we have to have gone to school and we have to have heard and and seen
and read many things which are appropriate we have to kept it in mind and we
have to have thought about it and put it into practice and we have to have
had experience and all of these things are part of what it means by the
word wisdom if we want to be successful in relationships if we want to have
good friends if we want to be surrounded by kind and caring people then we
have to know how to relate to them and we have to have social skills and we
have to have interpersonal skills and we have to know how to tell the difference
between a person who is looking for good things and a person who is headed
towards bad things or unwholesome things a person who is going to lead us in a
bad path and a person who is going to lead us to improvement is going to have
all the qualities of a good friend we have to know how to deal with these people
if we're kind of a person who doesn't have a certain level of wisdom then
it's quite likely that people who are somehow high-minded are not going to be
at all interested in associating with that person so that really everything
everything we do and all of our achievements in this world all of the people
who we esteem as the greats it really all has to do with some sort of level of
wisdom we think of Socrates or we think of Einstein or any of the great even the
great authors or the great scientists or philosophers we can think of what makes
a person great and we can see how wisdom is actually very important now so
what we're dealing with in Buddhism is a wisdom teaching a teaching on some sort
of knowledge or some sort of understanding similar you could think of in terms
of Plato or or or Socrates where they had some kind of philosophy and way of
looking at the world and they had teachings and they had a outlook and a method
and so on when we talk about Buddhism we're not talking about a faith-based
religion or something based on magic or based on superstition or or even based on
super mundane states we're talking about a practice for understanding that
teaches us more about ourselves and more about the world around this
helps us to understand those things that we're confronted with on a daily
basis helps us to understand all of those huge big questions which in the
end turn out to be very very mundane such as what is the meaning of life what
is the meaning of the universe what is the meaning of reality so on and so this is
really where we start in Buddhism and it's where we finish where we start in
Buddhism as we start by teaching we start by by learning by gaining that sort
of wisdom which is theoretical similar to going to school and studying or
listening to people talk or reading books or so on all wisdom all attainment of
understanding comes or starts from this basis of theoretical knowledge some
people some many Buddhists in the beginning they can mistake this and they
think that theoretical knowledge is useless it's unnecessary that the
only wisdom that's worth anything is experiential wisdom and then
this sort of you can seem kind of naive from someone who has really put
into practice these teachings when you realize how how subtle and how
difficult it is to to get on sort of this this path of understanding to
avoid all and to avoid all the pitfalls of some sort of delusion or
practice which which is leading one down a dead into a dead end or so on
that without without the theory without a teaching it's basically like having
to to reinvent the wheel or reinvent the entire path and so it's possible and
it's shown the fact that it's possible is shown by the fact that the Buddha
actually did it he had no one to teach him but it's very very difficult and
it it's actually impossible you can say that the Buddha had no teacher but he
did a lot of theoretical study he did a lot of laying down the axioms and
the basics talking about what is the basics of reality and through
experience experiment he was able to come up with this theory and so unless
we're willing to do that we shouldn't think that we can just go and sit and
somehow enlightenment is going to come to us that you have to do all of this
preparatory work at least with certain extent so so what I'm going to try to
give today is a little bit of theory that's important when we talk about
wisdom teachings we're generally talking about vast amounts of information so
whereas people might think the Buddhism is all about just sitting still it's
that's certainly very big part of it but the theory is so profound and the
path is so subtle that there has accumulated an incredible body of
literature even just speaking of the Buddha's teachings himself and the Buddha
didn't just say go and sit though he said that many times he also gave many
teachings that were useful to help people to sit and to meditate in a proper
manner and in a manner that would allow them to see things clearly and not get
lost and distracted so it's it's perhaps a little naive to think by any
stretch of the imagination on a single day or a half an hour in the next 10
minutes I'm somehow going to give you an overview of the Buddha's teaching but
we can come back a little ways towards this idea that Buddhism is just a
teaching a practical teaching it's a teaching of getting experiential wisdom
we don't want to give up theoretical knowledge altogether and in fact the
more theoretical knowledge we have the better equipped we are but we have to
find some sort of core and some sort of basis which we can then say okay if I
practice according to these principles I'm practicing according to the
Buddha's teaching and luckily for us of course this does exist there are a few
ways of approaching this and one of the best ways is laid down in the Buddha's
own words in his teachings on the four foundations of mindfulness which is a
very common teaching that you'll find both in the Buddha's teachings and in
those who even today spread the Buddha's teachings so not only are many
Buddhist practicing this but you can actually find it in the Buddha's
teachings as one of the core practical teachings where many times the Buddha
said that if you want a very condensed version of his teachings or if you don't
have a lot of time suppose your person who has a busy schedule or is is
already grown up and gotten beyond the time where they could put great amounts of
time to study then what you should do to focus your efforts is to set
yourself up in morality first and then start practicing the four foundations of
mindfulness to get a basic understanding of what it means to be moral sorry
end to and get an understanding of of the truth you know get this basic
doctrinal teaching you know what shouldn't I do and what is the ultimate goal
or what is what is the truth just from a theoretical level then start practicing
the four foundations of mindfulness so this is the the basics that we have to
give a person the first theoretical thing we have to give is to teach them what
it is to be moral and basically this means the kind of morality that you would
find in most religious systems at least in theory if not in practice not to
kill not to steal not to commit adultery or cheat not to lie and particularly
the meditation teaching not to take drugs or alcohol because of course this is
something that clouds the mind it's the heading in the opposite direction
from mindfulness so these basic rules these are called the five precepts or the
five training rules in Buddhism if someone can keep them then they've got the
basics of morality so from a theoretical level it's important if we're talking
about gaining wisdom that we start here why because these are things that the
reason that they're considered immoral is because of the effect that they
have on one's mind the effect that they have on one's peace of mind the
effect they have on one's clarity of mind people who betrayed these precepts
though it may not seem to to us readily if we're not if we're not looking
carefully they they haven't profound effect on your mind if your person
who's ever killed a substantial being large animals or or even worse killed a
human being for these people they can generally readily appreciate the
effect that it has on their mind people who steal people who cheat people who
lie and people of course take drugs and alcohol if you're objective about it
you can see how the the effect they have in your mind and so this is very
important in the beginning the second thing is this understanding of
reality or the truth from a theoretical perspective so we skip all the way to
the end and we say what is it that we're trying to achieve or what is it that
we're trying to gain what is the truth and of course this is something that
you know you can you can explain on various levels of complexity but the
easiest way to understand that the Buddha gave is that all things are indeed not
worth clinging to that there is nothing in the world worth clinging to nothing in
existence worth clinging to this is what we should understand is the truth
this is something that we've if we want to say that we understand the Buddha's
teaching the Buddha said understand this point and then you have this basic
understanding of the Buddha's teaching you don't have to go studying the 84,000
teachings of the Buddha understand that there is no thing and it's only an
intellectual or it's only a theoretical understanding you don't yet
understand if this is true or not but you understand this is what the Buddha
taught and that's enough and you say okay this is going to be the mantra or
the the motto you can say for the rest of my practice if I keep this in mind
it's going to lead me along the path of the Buddha if at some point I say
no this path is not for me then fine but at least I know this is what the Buddha
taught that all things no thing is worth clinging to once we have these two
foundations this is where the Buddha said okay now go on to practice the four
foundations of mindfulness so theoretically once we have these these
foundation teachings in our minds all we have to do is learn what are the four
foundations of mindfulness and then start practicing so I'm going to give
you give everyone a teaching now you're welcome to follow along you're
welcome to just listen but the idea would be for now for all of us to stop
texting and maybe if you can close your eyes preferably close your eyes you
can put your avatar on busy if you get lots of texts or I am and let's start
to learn about what is it that the Buddha taught or the four foundations of
mindfulness and what is it that we're taught to be mindful of the first
foundation that the Lord Buddha taught is the foundation of mindfulness of the
body and this is the reason it's first or perhaps the best explanation is to
why it's first is because it's the easiest to to grasp to see and this is
agreed upon by the whole long tradition of Buddhist meditation teachers
that the easiest and most course most obvious object of attention in terms of
what's really there is the body and so the Buddha taught us that we should
know the body for the body know the body as body know the body for what it is
because as with everything else this is something that we're going to cling
to as the Buddha said nothing in the world is worth clinging to and what we're
going to at least test we're going to test this theory we're going to do a
series of tests and experiments to see whether this is true and so we're going
to learn about how we cling to things like the body and we're going to see
whether that's actually of any benefit to us and with the idea that
according to the Lord Buddha there was no benefit from clinging so we're going
to watch the body how we how we do this how we practice all four foundations
of mindfulness according to the teaching that I profess and of course
there are many interpretations of what is meant by being mindful of these four
things but according to the teaching that that I have we use a mantra which is
similar to any ancient Indian meditation technique where you have a special
mantra which focuses your attention on the object or on the thing on the
phenomenon which is summed up by the mantra so it's usually something like God
or some special experience or special state or special object in Buddhist
meditation of course our object is reality because we want to see things as
they are and as I said we want to learn about our clinging to things and we want
to come to see the truth that nothing is worth clinging to because when we see
that of course we won't cling to anything when we don't cling we'll be free
we'll be like a bird flying and attached to anything so the our use of the
mantra is the same same technique but a different object it's going to be a
very ordinary object and the first one is the body so the technique that we
often give to people is to start watching at the stomach because the most
obvious part of the body when you're sitting still and not doing anything is the
movement of the stomach that rises and falls as the breath goes in and out
for many people nowadays this isn't that obvious because nowadays we have a lot
of stresses that probably weren't apparent in the time of the Buddha or at
least for monks meditating off in the forest but if it's not readily apparent
in the beginning I guarantee you that it will be as you go on as you get more
proficient in the practice as your breath calms down as your body loosens up
it will become very very obvious because your breath will become more natural if
you want to see this is true you can lie in your back if you lie in your back and
just feel your your breath you'll see that you're just like a baby again and your
belly is indeed rising and falling naturally but the problem is when we sit up
we become all tense and stressed and get back into this forest unnatural
state and so our breath becomes shallow and and and based in the chest and so
if you want the best way to to bridge the gap is to put your hand on your
stomach and just watch it or feel it rising and falling with your hand as it
rises we're just going to use this mantra rising we just say to ourselves
rising and when it falls we say falling
rising
falling and we don't say it out loud we're just saying it in our mind and
our mind is at the stomach as we do this we're getting into the second type of
wisdom which is wisdom sort of the mental exercise putting into practice the
things that we've learned and and trying to understand how they work so as we
say to ourselves rising it's a sort of wisdom it's a affirmation of the
truth of that experience it's not the highest sort of wisdom yet but it's a
understanding of things as they are this is rising it's putting into practice
the theory that we've learned testing or thinking about or experimenting on
that theory
now as we start to watch the body as I said they're all together four
foundations and the reason there are is because our mind doesn't stay with the
body all the time it wonders it goes here and there and everywhere another
thing about meditation is this sort of meditation is according to the
teaching that nothing is worth clinging to we're also not trying to cling to
the meditation object or trying to cling to the idea that our mind has to be
stationary we're going to watch the mind as it is and we're going to learn and
not cling and because we're going to start to see that clinging is the reason
our mind is so flustered and confused and and scattered as it is so when our
mind does wonder we're going to focus on the other various objects of its
attention the next one being feelings that arise so when we're sitting watching
the rising and falling we find our mind wandering away to certain sensations
that arise in the body and in the mind pleasant sensations unpleasant sensations
or neutral comms sensations many times sitting cross-legged in meditation
we'll find pain arising in the back or in the legs sometimes we'll find pain in
the head if we're if we think a lot of work hard every day so instead of
seeing these as a distraction or an obstacle towards our practice we should
focus them on them and use them as as our meditation practice they're part of
reality and there's something that it's very common for us to cling to it's
generally pretty obvious that we're clinging to them we're saying this is
bad don't want this want this to leave me to go away so here instead of doing
that we're going to say to ourselves pain or aching or sore or however just
reminding ourselves of that of what it is we use this mantra it's going to focus
objectively on the object not seeing it as good or bad just seeing it for what
it is with wisdom so we say to ourselves pain pain pain pain and till it goes
away if we feel happy we do the same we don't want to cling to happy
ness either there's nothing wrong with happiness in fact most people would
agree that happiness is a good thing but what's what's really not a good thing is
our clinging to it we're saying oh I hope this stays for a long time I hope
this is permanent because it doesn't stay for a long time it isn't permanent it
isn't subject to our control so we should just know that we're happy and just
be happy not clinging to it not judging it not giving rise to anything beyond
just a knowledge that this is happiness say to ourselves happy happy happy
if we feel calm the same thing many people as they practice as their
practice progresses they'll feel calm they'll feel quiet and they'll find that
they aren't able to meditate any longer because all that's left is a sense of
quiet and peace but the Buddha was very clever and was very clear in pointing
absolutely everything that we were to be mindful of that that could arise that
includes peaceful feelings and so when we feel peaceful we should say to
ourselves as well calm calm or peaceful peaceful
this is the second foundation the third foundation is the foundation of the
mind focusing on the mind itself as we practice we'll see the next thing the next
problem is that we're thinking and we say oh this is a big problem because we're
no longer focusing on our object now instead we're thinking remind us wandering
away from the object but the great thing here as I said is that that can be
that as well can be the object the mind can whatever can be the object whatever
arises can be the object of our intention so when we think we can say to ourselves
thinking thinking whatever we're thinking about instead of turning it into a
huge issue a big deal making more of it than than just what it is we simply see
it as thinking thinking thinking and the final one is our emotions and the final
one actually as many many things but I'm just gonna simplify it down to our
emotions it's many of the various other teachings of the Buddha which we
should start to focus in on actually the first three are the main foundations
the fourth one is things that we're going to have to focus in on our practice
becomes more focused then simply anything we're going to focus on the
important the salient points of our existence the first one is our hindrances
the the things that you're going to get in the way of our practice when we
like something when we don't like something when we feel angry or bored or
frustrated or sad or depressed all of these negative emotions even including
liking or or enjoy desire or lust or so and we consider them to be negative in
the sense that they are judgments they're clinging and we're not going to judge
these things we're just going we should understand that these are five things
which are actually going to change our state of mind and take us out of the
meditative state if we give power to these emotions then we'll find our
mindfulness dissipating so it's important that not that we judge these not
that we feel guilty about them when we say bad bad bad but that is with
everything else we we pick up these as well as I sort of especially pick these
up keep them in our radar when they arise be very quick to catch them just
like everything else so when you like something say to yourself liking liking or
wanting when you don't like something say disliking disliking simply knowing
that now you're disliking we're going to see it for what it is if it's a good
thing to like or dislike something then we'll know it we'll have wisdom we'll
have understanding if we feel bored if we feel distracted
if we feel sad or depressed we should just say to ourselves sad sad depressed
depressed
if we if we feel drowsy or lazy or tired we should simply know what for what it
is just see if what it is not getting upset about it but just saying okay now we
have this emotion in our minds now we have this state of mind we say to ourselves
drowsy drowsy drowsy tired tired these are things which are going to get in the
way of our practice very clearly and from the very very beginning so they're
important for especially beginner meditators and the final one that the Buddha
mentioned was doubt if you have doubt in your minds doubt about yourself whether
you can do this practice doubt about the practice itself whether it's
right then his advice for you is to practice doubt practice focusing on the
doubt practice learning about the doubt itself give up the idea that you're
practicing anything and just look at the doubt and say to yourself what is
doubt make that your practice because once you learn about doubt and give up the
doubt then there's nothing that you need to practice this is really in one
sense this is the teaching of the Buddha that you come to see you come to give up
your doubt you come to see everything for what it is and not have anything left
to doubt there's no other practice beside that there's nothing else that you
have to achieve
when there's nothing left to be mindful of we just always come back to our
base meditation of being mindful of the rising and the falling because it's
always there the stomach rising and falling rising and falling we don't have to
go looking for other objects when they come up we pay attention to them when
they're not there we come back to the body it can really be any part of the
body you can focus on the walking when you're walking around saying to yourself
walking walking or someone when you're sitting still it's generally easiest to
focus on the stomach because it's very clear it's always there so the
practice is another type of wisdom it's it's a putting into practice it's
getting the skills it's an intellectual exercise but what comes from the
practice this is true is that this is really what we're getting we're trying to
gain we're going to start seeing things about ourselves that we didn't see
before we're going to learn things about ourselves that we didn't know before
we're going to give up things that we thought were inherent or intrinsic to
who we were and we'll give them up because we'll see that they're useless
they're unhelpful they're unbeneficial they're not under our control and they
have no use whatsoever and we're going to see that we're better off without
them when we see that we're better off without them this is true wisdom and
it's it's a fact it's a fact of life that's simply knowing that it's wrong for
you when you really know that something is unhelpful that you do give it up the
problem is we always say this is bad for me that's bad for me we don't
really know we actually still feel pleasure and attachment to it we still
think have this idea that it's somehow good for us when we see deeply that
things are not worth clinging to then we actually don't cling to them we do
follow our wisdom we do follow our knowledge it's just that most of our
knowledge is only theoretical it's a sort of false knowledge we say this is
good and this is bad but we don't really have any understanding of wisdom
that is able to tell us that this is good or this is bad so here I've I've given
a talk a little bit longer than I normally would but I sort of intended to do
so thinking that it was going to also be a meditation practice and therefore
cut into some of our question and answer time but now of course we have time
for discussion questions and answer and probably I'm I'm I'm happy to be here
after one one o'clock if there's still people liking or hanging on so I'll stop
there and I'd like to thank you all for coming if you have any questions I'm
happy to answer them via text from here on
