1
00:00:00,000 --> 00:00:14,600
So, tonight's question is about fun.

2
00:00:14,600 --> 00:00:20,720
I don't think it's really about fun, but my answer is going to be about fun.

3
00:00:20,720 --> 00:00:27,560
So, the person asks whether they're practicing correctly, they say they've been doing this

4
00:00:27,560 --> 00:00:35,600
for some time, noting, using the technique, but they're not sure how to know whether

5
00:00:35,600 --> 00:00:41,080
they're doing it right because it's not fun.

6
00:00:41,080 --> 00:00:47,760
So, I thought I would expand this answer to discuss the topic of fun, it won't take

7
00:00:47,760 --> 00:00:57,520
a long time hopefully, but just go over some of the issues around meditation and fun.

8
00:00:57,520 --> 00:01:06,520
So, the reason why your meditation might not be fun, I can think of three reasons.

9
00:01:06,520 --> 00:01:09,880
That's what I'll talk about.

10
00:01:09,880 --> 00:01:15,760
First reason why your meditation might not be fun is because you're doing it wrong.

11
00:01:15,760 --> 00:01:23,520
It's quite easy to do, to practice incorrectly, especially if you don't have a teacher.

12
00:01:23,520 --> 00:01:28,320
If you're just going by my booklet on how to meditate, it's quite possible that you're

13
00:01:28,320 --> 00:01:30,840
practicing wrong.

14
00:01:30,840 --> 00:01:37,880
And by wrong, I mean getting something wrong, you're, you haven't quite understood perhaps

15
00:01:37,880 --> 00:01:43,800
how to do the noting and so you're trying to make the things go away, sometimes you have

16
00:01:43,800 --> 00:01:49,440
extreme problems to the extent that someone rather than when they have pain saying to

17
00:01:49,440 --> 00:01:57,120
themselves pain, pain, that repeat to themselves, no pain, no pain, or if they're afraid

18
00:01:57,120 --> 00:02:04,440
they'll say calm, calm, because the point, their misunderstanding is that they should repeat

19
00:02:04,440 --> 00:02:13,560
what they want to happen, a less extreme, but still wrong perspective is to say to

20
00:02:13,560 --> 00:02:18,240
yourself, for example, pain and want the pain to go away, or to say thinking, thinking

21
00:02:18,240 --> 00:02:23,920
and be frustrated when it comes back, and without any input from a teacher, a feedback

22
00:02:23,920 --> 00:02:32,080
from a teacher, it can be quite discouraging because you have a perspective, an attitude

23
00:02:32,080 --> 00:02:39,800
of trying to make, control things and make things change based on your whim, which really

24
00:02:39,800 --> 00:02:46,160
is the whole point of the meditation to see that that's not possible.

25
00:02:46,160 --> 00:02:54,200
And practicing wrongly can make the meditation painful, unpleasant, especially if you have

26
00:02:54,200 --> 00:03:00,400
a wrong perspective, if you have a wrong attitude, a wrong outlook.

27
00:03:00,400 --> 00:03:05,920
I think I could suggest that wanting your meditation to be fun, I don't really think what

28
00:03:05,920 --> 00:03:13,560
this person was saying is they wanted it to be fun, but it's, fun is not really inherently

29
00:03:13,560 --> 00:03:16,080
different from any other kind of pleasure seeking.

30
00:03:16,080 --> 00:03:21,600
We call things fun, we seek out fun as a part of our pleasure seeking, it's really just

31
00:03:21,600 --> 00:03:29,880
another word for a type of pleasure seeking, similar to eating or having sex or listening

32
00:03:29,880 --> 00:03:38,480
to music, dancing we say is fun, but deep down what we really mean, what we're really

33
00:03:38,480 --> 00:03:43,280
responding to is the pleasure involved.

34
00:03:43,280 --> 00:03:52,480
So the idea that your meditation should be pleasurable is wrong and has problematic consequences

35
00:03:52,480 --> 00:03:58,640
because you'll be frustrated when the things that you find pleasurable are mixed with the

36
00:03:58,640 --> 00:04:08,080
things you find un-displeasurable or displeasing.

37
00:04:08,080 --> 00:04:16,400
The second reason why your practice might not be fun is because you're not good at it.

38
00:04:16,400 --> 00:04:24,640
And really being not good at meditation is essentially doing it wrong, but it's a different

39
00:04:24,640 --> 00:04:32,720
kind of doing it wrong, I think, because you say someone's doing it wrong when there's

40
00:04:32,720 --> 00:04:41,640
something fundamental about their technique, about their perspective, about their approach,

41
00:04:41,640 --> 00:04:50,760
that is problematic, that can be easily corrected through explanation, through advice,

42
00:04:50,760 --> 00:04:55,640
not even exactly advice, but just pointing it out when you're doing that wrong, like

43
00:04:55,640 --> 00:05:03,160
take weightlifting, for example, you might try to lift too much, or you might try to lift

44
00:05:03,160 --> 00:05:10,880
the wrong way, and you might hurt yourself as a result if you're training in sports and

45
00:05:10,880 --> 00:05:14,960
on golfing, it's one thing to not be good at golfing, but it's another to maybe hold

46
00:05:14,960 --> 00:05:23,760
to stick the wrong way, the club the wrong way, or use the wrong club, right?

47
00:05:23,760 --> 00:05:28,240
Just doing it wrong, if you're trying to hit it very far and you use the putter, you're

48
00:05:28,240 --> 00:05:36,240
doing it wrong, and meditation is similar, similar problems, of course.

49
00:05:36,240 --> 00:05:41,720
But not being good at something means you have an understanding of the theory, which hopefully

50
00:05:41,720 --> 00:05:47,720
you do if you've read the booklet and we've gone over it and reassured ourselves together

51
00:05:47,720 --> 00:05:52,200
that you're doing it, you understand the theory, that shouldn't be something you should

52
00:05:52,200 --> 00:05:56,520
worry about, because there's no deep theory that you have to understand, and I think

53
00:05:56,520 --> 00:06:02,400
you have to make a break as I'll make a line, because you'll drive yourself crazy if

54
00:06:02,400 --> 00:06:08,320
you think, oh, is there some theory that I don't know, am I doing it wrong, it's a common

55
00:06:08,320 --> 00:06:15,080
common question, and you have to separate, no, you're not doing it wrong per se, you're

56
00:06:15,080 --> 00:06:20,440
doing it wrong in the sense that you're not good at it, and it's important to understand

57
00:06:20,440 --> 00:06:25,200
that that's a part, that's true about meditation, that you have to get good at it, it

58
00:06:25,200 --> 00:06:33,480
is a training, it's not a switch that you turn on, it's not just a time out where, okay,

59
00:06:33,480 --> 00:06:38,680
all that craziness in my life, I'm going to put aside and do nothing, and poofing

60
00:06:38,680 --> 00:06:41,200
and be peaceful.

61
00:06:41,200 --> 00:06:46,720
It is in fact, like that, meditation is in the end to let go of all the things that

62
00:06:46,720 --> 00:06:51,080
cause the suffering, stop doing the things that cause the suffering, but your mind's not

63
00:06:51,080 --> 00:06:58,960
going to let you do that, because we habitually seek out suffering, not intentionally,

64
00:06:58,960 --> 00:07:04,360
we think it's going to make us happy, but it doesn't, and so because of those habits

65
00:07:04,360 --> 00:07:09,000
having been built up, we're in a real bind, we try to sit still, and we're not going

66
00:07:09,000 --> 00:07:14,160
to sit still, we try to focus and we're not going to focus, we try to just observe something

67
00:07:14,160 --> 00:07:23,920
and our mind's got a whole other idea.

68
00:07:23,920 --> 00:07:30,560
So not being good at it, when you say to yourself, pain, pain, but your mind is maybe angry

69
00:07:30,560 --> 00:07:36,440
about the pain, or you try to say it, and then you're distracted by something else.

70
00:07:36,440 --> 00:07:42,560
You try to focus on the stomach, but your mind is distracted, you try to sit still, but

71
00:07:42,560 --> 00:07:46,760
you rest this or agitated, or you're falling asleep.

72
00:07:46,760 --> 00:07:53,040
There's much about the imbalance of the faculties, right, just effort and concentration

73
00:07:53,040 --> 00:07:59,520
or very common, there's not much you can do about that, except get good, as you practice

74
00:07:59,520 --> 00:08:07,720
more, you're going to get better at it, and part of that, it's not something you can push

75
00:08:07,720 --> 00:08:16,120
or pull, it's just through repetition and application and diligence, and become more energetic

76
00:08:16,120 --> 00:08:20,880
and less distracted, so concentration and energy will start to work together rather than

77
00:08:20,880 --> 00:08:29,280
fight each other, or sometimes you're restless, sometimes you're falling asleep.

78
00:08:29,280 --> 00:08:33,640
So it's important to understand that, that you can be doing everything right, and it's

79
00:08:33,640 --> 00:08:37,200
just can still be a horrible experience, because you're not very good at it, and that's like

80
00:08:37,200 --> 00:08:42,560
any training, you're playing golf, it's a horrible experience in the beginning, but

81
00:08:42,560 --> 00:08:51,280
meditation is just like that, when you finally hit the ball and goes up to that grass,

82
00:08:51,280 --> 00:08:56,600
when you finally, just for a moment, you know, it was really mindful there, and it totally

83
00:08:56,600 --> 00:09:04,520
didn't bother me, as you realize you're starting to get good at it.

84
00:09:04,520 --> 00:09:12,200
And the third reason why meditation might not be fun is because the goal of meditation

85
00:09:12,200 --> 00:09:21,560
is not to have fun, fun is a, as I said already, an inferior state, fun is not the goal

86
00:09:21,560 --> 00:09:27,440
nor should it be the goal of meditation or one's life, having fun is just another means

87
00:09:27,440 --> 00:09:30,320
of seeking pleasure.

88
00:09:30,320 --> 00:09:34,680
Consider anything we call fun or we think of as fun.

89
00:09:34,680 --> 00:09:42,600
How long can you engage in that activity before it becomes boring, right?

90
00:09:42,600 --> 00:09:47,040
Because of course the flip side of something being fun is something being boring, meditation

91
00:09:47,040 --> 00:09:53,760
is very boring, but it's only boring because you see it that way, and the only reason

92
00:09:53,760 --> 00:10:00,720
you see it that way is because you stuck to certain things as being fun, and you're addicted

93
00:10:00,720 --> 00:10:01,720
to those things.

94
00:10:01,720 --> 00:10:08,440
This is the addiction cycle, fun does not escape and cannot escape the addiction cycle,

95
00:10:08,440 --> 00:10:14,840
and the addiction cycle cannot escape suffering, it's possible to engage in something without

96
00:10:14,840 --> 00:10:22,600
suffering for a time, but the build up, the lead up, the result is a susceptibility to

97
00:10:22,600 --> 00:10:23,600
suffering.

98
00:10:23,600 --> 00:10:29,640
There's no question, you might say, I don't suffer, I have fun and I don't suffer.

99
00:10:29,640 --> 00:10:34,160
First of all, you're deluding yourself most likely because after time we don't realize

100
00:10:34,160 --> 00:10:39,000
how stressful and unpleasant our lives are, we try to forget, we've cultivated the ability

101
00:10:39,000 --> 00:10:42,720
to ignore and forget how much suffering we have.

102
00:10:42,720 --> 00:10:48,240
The second of all, it's not even so much that you do suffer, it's that you're caught

103
00:10:48,240 --> 00:10:51,600
up in suffering, you're vulnerable to it.

104
00:10:51,600 --> 00:10:59,680
What happens if you get sick, you're a sports, you're a sports person, an athlete, and

105
00:10:59,680 --> 00:11:06,520
then you get sick, and you get an illness, or you do rock climbing, and I was really quite,

106
00:11:06,520 --> 00:11:08,080
I was good at it.

107
00:11:08,080 --> 00:11:18,160
But then I got tendonitis in my elbow, tendonitis in my fingers, and I can no longer do it.

108
00:11:18,160 --> 00:11:24,440
And that wasn't even the big deal, but you can, people can like tiger woods, they hurt

109
00:11:24,440 --> 00:11:27,040
himself very bad, I think.

110
00:11:27,040 --> 00:11:36,880
I only know that because his mother's Buddhist, so here's things about him.

111
00:11:36,880 --> 00:11:44,320
You can't escape the potential for suffering, man, what about getting old and sick and dying,

112
00:11:44,320 --> 00:11:46,560
how are you going to be when you die?

113
00:11:46,560 --> 00:11:54,640
Your state of mind predicts your rebirth, predicts your future, predicts your peace of

114
00:11:54,640 --> 00:12:00,560
mind that when you die, and because of the addiction cycle we're constantly seeking.

115
00:12:00,560 --> 00:12:05,080
When you die, you'll see these bright lights, and chase after them, and oh, you'll be

116
00:12:05,080 --> 00:12:06,080
reborn.

117
00:12:06,080 --> 00:12:13,280
It's funny how other religions talk about the bright light, and I saw the light, and that

118
00:12:13,280 --> 00:12:16,600
was a sign of heaven, well, maybe.

119
00:12:16,600 --> 00:12:21,200
We have other stories where people who chase after those lights end up, end up in hell

120
00:12:21,200 --> 00:12:27,120
and suffering.

121
00:12:27,120 --> 00:12:31,000
Meditation isn't supposed to be for the purpose of fun, and it isn't even for the purpose

122
00:12:31,000 --> 00:12:32,480
of pleasure.

123
00:12:32,480 --> 00:12:41,960
We have to understand that it's kind of a paradox, where the end goal, of course, is peace,

124
00:12:41,960 --> 00:12:51,240
but it's not the peace that you enjoy, and really the whole idea of enjoying things has

125
00:12:51,240 --> 00:12:57,640
to be done away with, and that's the profound and radical aspect of Buddhism, because it seems

126
00:12:57,640 --> 00:13:04,640
horrific that you should even suggest to get rid of enjoying things, but the whole concept

127
00:13:04,640 --> 00:13:11,000
and system of enjoyment is wrong-minded, wrong-headed, wrong-intentioned.

128
00:13:11,000 --> 00:13:13,840
Happiness doesn't work that way.

129
00:13:13,840 --> 00:13:18,680
Enjoying involves liking, it involves wanting, it involves needing, it involves addiction,

130
00:13:18,680 --> 00:13:24,360
and so it never frees you from the vulnerability of suffering.

131
00:13:24,360 --> 00:13:32,040
Happiness, peace, have to be independent of things, events, people, places, things, and

132
00:13:32,040 --> 00:13:34,920
events, experiences.

133
00:13:34,920 --> 00:13:40,200
If your happiness is dependent on sitting there peaceful and calm, you'll never be peaceful

134
00:13:40,200 --> 00:13:41,200
and calm.

135
00:13:41,200 --> 00:13:46,640
It's a paradox, sort of, or whatever it is, it's ironic, no, it's something.

136
00:13:46,640 --> 00:13:52,400
It's unfortunate, it would be nice that I can have want to be peaceful and just be peaceful,

137
00:13:52,400 --> 00:14:00,040
but one thing of course disturbs your peace, catch 22, maybe I don't know, it's a problem,

138
00:14:00,040 --> 00:14:04,720
and so our focus can never be on happiness, I've talked about this before.

139
00:14:04,720 --> 00:14:08,960
You can never focus on peace and happiness.

140
00:14:08,960 --> 00:14:15,800
Your focus has to be on goodness, and really by goodness in this deep meditative sense,

141
00:14:15,800 --> 00:14:19,720
it means a good state of mind, purity.

142
00:14:19,720 --> 00:14:25,400
So when we say to ourselves, pain, pain, or even just rising, falling, thinking, thinking,

143
00:14:25,400 --> 00:14:29,560
we're trying to cultivate what we consider to be the purest state of mind, the purest

144
00:14:29,560 --> 00:14:35,840
state of mind, the purest state of mind, a risen state of mind is the one that knows an

145
00:14:35,840 --> 00:14:40,440
object just as it is without judgment, without reaction.

146
00:14:40,440 --> 00:14:42,560
It is what it is, pain is what?

147
00:14:42,560 --> 00:14:48,000
Not good, not bad, not mean, not mind, not a problem, not something to fix.

148
00:14:48,000 --> 00:14:53,960
Pain is pain, rising is rising, it's a training that we're doing, why are we focusing

149
00:14:53,960 --> 00:14:58,280
on the stomach and the feet, there's nothing special about them.

150
00:14:58,280 --> 00:15:03,560
But there's nothing, there shouldn't be dismissed either, there's nothing inferior about

151
00:15:03,560 --> 00:15:13,120
them, this is an experience moving the foot, this is an experience.

152
00:15:13,120 --> 00:15:19,760
Our focus is not on having fun, absolutely, but it's also not even on peace and happiness,

153
00:15:19,760 --> 00:15:27,360
and it never can be, our focus always has to be on goodness and purity, and that shouldn't

154
00:15:27,360 --> 00:15:32,680
be so bad, I mean, that shouldn't sound so bad, imagine having a perfectly pure mind,

155
00:15:32,680 --> 00:15:41,960
the only result could ever be peace, happiness, that's an important distinction because

156
00:15:41,960 --> 00:15:49,400
we'd go the other way, right, who in this world thinks of, oh, well, I shouldn't put

157
00:15:49,400 --> 00:15:55,200
us down too much, but we're in society, we're in modern society, are we taught about

158
00:15:55,200 --> 00:15:59,680
goodness, I mean, I think in religion we often are, and it's not the same, many people

159
00:15:59,680 --> 00:16:05,800
do have ethics and ideas of helping others and so on, but what are we told, what is much

160
00:16:05,800 --> 00:16:12,920
more prevalent and in front of us, seek out pleasure, seek out happiness, do what makes

161
00:16:12,920 --> 00:16:21,960
you happy and so on, and so our focus is on the result, but the only thing that brings

162
00:16:21,960 --> 00:16:34,400
that result truly and sustainably brings it is goodness, is the purity of life, so I don't

163
00:16:34,400 --> 00:16:40,040
think this person was, it wasn't literally saying it should be having fun like playing

164
00:16:40,040 --> 00:16:46,280
a game, but they certainly did seem to be discouraged, and many people are discouraged,

165
00:16:46,280 --> 00:16:51,080
I don't think people think of meditation as a game very much, if you got this far you're

166
00:16:51,080 --> 00:16:57,480
not here to play a game and have fun, but we say something is not fun, and by that we mean

167
00:16:57,480 --> 00:17:02,160
it's not pleasurable, which really is the same thing, again, it's just another fun, it's

168
00:17:02,160 --> 00:17:10,200
just another type of pleasure, so don't ever try and find peace and happiness and meditation,

169
00:17:10,200 --> 00:17:19,560
try and find purity and goodness and clarity of mind, strength as well of mind, because

170
00:17:19,560 --> 00:17:26,320
happiness is a result that's not a practice, it's not the cause, happiness is not the cause

171
00:17:26,320 --> 00:17:36,420
of happiness, right, goodness is, so some thoughts on fun and meditation, leave it to the

172
00:17:36,420 --> 00:17:42,760
buddhist to take the fun out of everything, that should be our motto, we take the fun out

173
00:17:42,760 --> 00:17:50,960
of everything, because it's not good for you, it's in fear here, so that's the answer

174
00:17:50,960 --> 00:17:56,800
to that, thank you for listening.

