1
00:00:00,000 --> 00:00:10,360
Hello welcome back to Ask a Monk. Today I will be answering a question from the new forum

2
00:00:10,360 --> 00:00:21,620
ask.surrimundalud.org which is our replacement for the Ask a Monk platform. The new

3
00:00:21,620 --> 00:00:26,080
forum one thing I'd like to say for those people who are getting involved in it is that

4
00:00:26,080 --> 00:00:34,000
is two things. First of all, that the content on the form should be Buddhist. So I'm probably

5
00:00:34,000 --> 00:00:41,200
going to do a little bit of moderating or at least, you know, posting notices for questions

6
00:00:41,200 --> 00:00:48,080
that are asked or answers that are out of line with the Buddhist teaching. I would prefer

7
00:00:48,080 --> 00:00:56,080
that, all right, I think I'm going to insist that questions and answers both be of a Buddhist

8
00:00:56,080 --> 00:01:03,840
nature. So we're not looking for answers from other schools. The other thing is, I'm probably,

9
00:01:04,400 --> 00:01:10,640
I haven't 100% decided this, but I'm pretty sure that we're going to require or limit the questions

10
00:01:10,640 --> 00:01:20,640
and answers to Theravada Buddhism or what is in line with the sort of things that I teach because

11
00:01:20,640 --> 00:01:27,600
Buddhism is, of course, a broad subject. So this isn't meant to be as far as I've sort of

12
00:01:27,600 --> 00:01:33,040
designed it or have it. The concept in my mind is that it's not meant to be a broad-based

13
00:01:33,040 --> 00:01:41,200
forum, so there's probably going to be some moderating going on and notifications

14
00:01:43,040 --> 00:01:49,440
because I'd rather, you know, it's not supposed to be so broad. It's just a chance for the group of

15
00:01:49,440 --> 00:01:55,120
people who are interested in the things that I teach and practice and the sort of similar things

16
00:01:55,120 --> 00:02:00,080
to what I practice and teach, not just my students, but people who are into the same sort of things

17
00:02:00,080 --> 00:02:09,600
in a fairly narrowly defined circle to ask and to answer questions. That's my hope anyway.

18
00:02:09,600 --> 00:02:16,400
I'm, you know, in my change. So, okay, today's question is of interest to me specifically

19
00:02:16,400 --> 00:02:25,040
because I happened in the car driving here in California and now back in Moore Park, California,

20
00:02:25,040 --> 00:02:35,040
I'll be leaving tomorrow for Canada. To over here a news bulletin bulletin that Stephen Hawking,

21
00:02:35,920 --> 00:02:45,680
the renowned theoretical physicist, has said that he's not afraid of death, which is a good thing,

22
00:02:45,680 --> 00:02:52,880
but he says that he doesn't believe in rebirth because to him the brain is a

23
00:02:52,880 --> 00:03:00,880
machine is just a machine, a computer, and he doesn't believe in rebirth. That's just a

24
00:03:01,920 --> 00:03:14,560
fairy tale for people who are afraid of the dark. That's the quote. So, because I think Stephen

25
00:03:14,560 --> 00:03:21,360
Hawking is a very well-known person and very well-respected in his field, I thought it would be

26
00:03:21,360 --> 00:03:28,880
important to talk about the concept of rebirth. So, the question today is going to be,

27
00:03:30,000 --> 00:03:35,600
this is going to be posted as a response to it, so you can read it up there for yourself,

28
00:03:35,600 --> 00:03:42,240
is basically in regards to rebirth, but it also touches on the idea of Nirvana or Nirvana,

29
00:03:42,240 --> 00:03:54,000
and so I want to address both of these topics. The gist of it is that many people will

30
00:03:56,240 --> 00:04:04,080
not see a reason for exerting themselves in the meditation practice. Don't see a reason to

31
00:04:04,080 --> 00:04:14,320
try and change who they are and to work hard to purify their mind. See, the reason being that

32
00:04:15,600 --> 00:04:22,800
an impure person can theoretically gain the same sorts of happiness and peace in this life,

33
00:04:22,800 --> 00:04:28,560
perhaps even more, and as much more it is potentially in this life than a person who struggles

34
00:04:28,560 --> 00:04:37,120
and strives for some theoretical enlightenment. So, the one thing in the question that I really

35
00:04:37,120 --> 00:04:46,160
think needs to be addressed is where the person, I assume, identifies themselves as a skeptical

36
00:04:46,160 --> 00:04:57,440
materialist or is referring to people who are skeptical materialists. And the idea that they don't

37
00:04:57,440 --> 00:05:05,680
take anything on faith and therefore these promises of the future, bliss or future happiness or

38
00:05:05,680 --> 00:05:11,920
future enlightenment seem rather shallow, even the attainment of those things seem meaningless.

39
00:05:13,120 --> 00:05:18,560
And the person even talks about suicide, he says, well, wouldn't suicide, he or she,

40
00:05:18,560 --> 00:05:26,080
he says, wouldn't suicide obtain the same result. So, for this reason I want to talk about

41
00:05:26,080 --> 00:05:31,200
Buddhist understanding of rebirth and of Buddhist understanding of enlightenment and Nirvana.

42
00:05:33,680 --> 00:05:39,600
First, I've explained my understanding of rebirth in another video. It's a video on the nature

43
00:05:39,600 --> 00:05:45,920
of reality. So, if you Google that or look up under my videos, the nature of reality, you'll get

44
00:05:45,920 --> 00:05:55,920
my understanding of what is the Buddhist concept of rebirth. And basically, and this also touches

45
00:05:55,920 --> 00:06:03,600
on Stephen Hawking's claim, it's not that we believe in rebirth, it's that we don't believe

46
00:06:03,600 --> 00:06:10,480
in death. So, when Stephen Hawking says that he doesn't believe in reincarnation or rebirth,

47
00:06:10,480 --> 00:06:18,080
that's just a fairytale because he sees things from a very empirical point of view. He's actually

48
00:06:18,080 --> 00:06:35,920
going against both what a meditator or a person who is studying the nature of experience,

49
00:06:35,920 --> 00:06:44,000
what they experience. And he's also going against many, many accepted principles in theoretical

50
00:06:44,000 --> 00:06:49,920
physics or many points that should be accepted. And why they should be accepted is because it

51
00:06:49,920 --> 00:06:57,280
was people, the people who began to study quantum physics who developed these. If you look at

52
00:06:57,280 --> 00:07:08,240
orthodox quantum physics, people like Niels Bohr, Heisenberg, the people who put together the

53
00:07:08,240 --> 00:07:17,040
foundations of quantum physics, what they did with quantum physics is came to see that reality

54
00:07:17,040 --> 00:07:25,280
isn't an objective state, that three-dimensional or four-dimensional space, or space time,

55
00:07:25,280 --> 00:07:33,760
is theoretical. It's something that we postulate, we say that this exists, we say that the brain

56
00:07:33,760 --> 00:07:43,440
exists, we say that the body exists. But even the brain, as we understand it, is only a grouping

57
00:07:45,280 --> 00:07:53,360
of potential states that require consciousness, require our observation without our

58
00:07:53,360 --> 00:08:03,040
examination of reality. You can't even talk about, this is according to the original

59
00:08:03,920 --> 00:08:09,280
orthodox interpretation of quantum physics, you can't even begin to talk about the

60
00:08:10,800 --> 00:08:15,200
standards in the world around us. So when Stephen Hawking talks about death,

61
00:08:16,400 --> 00:08:22,400
we have to ask him, what is the death of what? It's the death of what sort of death is he talking

62
00:08:22,400 --> 00:08:28,560
about, he's talking about the death of a theoretical object, something that only exists in the

63
00:08:28,560 --> 00:08:34,480
minds of human beings. It's something that we have created as a theory, and we can never prove

64
00:08:34,480 --> 00:08:45,280
that it exists. And of course, this is the nature of material science. So on the other hand,

65
00:08:45,280 --> 00:08:58,800
from conversely, what is empirical is the consciousness is our experience. So when we look at

66
00:08:58,800 --> 00:09:04,560
the theoretical world around us, this three-dimensional or four-dimensional space time,

67
00:09:06,640 --> 00:09:11,040
then we can see things dying. We can see the brain falling apart, we can see the body falling

68
00:09:11,040 --> 00:09:16,160
apart, we can see something that was working together as a system, no longer working together as a

69
00:09:16,160 --> 00:09:23,920
system. But all that we're talking about is this theoretical system or theoretical

70
00:09:25,680 --> 00:09:35,840
matrix of energy, of electrons and subatomic particles working together and coming together

71
00:09:35,840 --> 00:09:42,000
in a sort of a wave and then falling apart, again and again and again and doing this in order.

72
00:09:42,000 --> 00:09:47,440
And it's all theoretical. It's something that we have built on this science around.

73
00:09:47,440 --> 00:09:55,120
But what we experience in reality is the sites and sounds and smells and tastes and feelings

74
00:09:55,120 --> 00:09:59,440
and thoughts. We experience things. There's the physical side of experience and there's the

75
00:09:59,440 --> 00:10:08,000
mental side of experience and this continues on incessantly. And it is also governed by natural

76
00:10:08,000 --> 00:10:16,400
laws. The laws of cause and effect for example. So when you, the intent, the mental intention,

77
00:10:16,960 --> 00:10:23,120
to do things, to become something, to change things, to act and to speak and to think,

78
00:10:23,120 --> 00:10:33,040
has an effect on what comes next. It creates the next moment. And these laws are easily verifiable

79
00:10:33,040 --> 00:10:39,600
by anyone who practices, not practices as a specific meditation, but one who studies them.

80
00:10:39,600 --> 00:10:44,400
If you study the empirical reality in front of you, this experience, right now you're experiencing

81
00:10:44,400 --> 00:10:50,880
things. If you study this, you can use this technique that I have been spreading and all of your

82
00:10:50,880 --> 00:10:56,640
label things as they are. It's not a subjective practice. It's not a religious practice. When you

83
00:10:56,640 --> 00:11:03,360
see something, you say you're self-seeing. So you label it as seeing to straighten your mind in

84
00:11:03,360 --> 00:11:07,680
regards to the object that it is what it is. This is seeing, this is hearing, this is smelling.

85
00:11:07,680 --> 00:11:12,880
When you feel pain, you say you're self pain and you know it for what it is. Your mind starts to

86
00:11:12,880 --> 00:11:16,800
become clear. Your mind starts to straighten and you're able to see things as they are.

87
00:11:16,800 --> 00:11:22,960
If you do this, you'll be able to see cause and effect. You'll see what it is that's causing yourself

88
00:11:22,960 --> 00:11:27,200
for which causing you happiness. You'll see what's causing you to speak, what's causing you to

89
00:11:27,200 --> 00:11:31,840
think, what's causing you to act. You'll see the cause and effect and you'll see that this is the

90
00:11:31,840 --> 00:11:41,120
way things work. So to say, and this is the catch, this is the main point, to say that at the moment

91
00:11:41,120 --> 00:11:48,480
of death, there is nothing, is really to postulate a violation of the experience, the laws of

92
00:11:48,480 --> 00:11:56,400
experience. The reality that we are experiencing right here and the laws that are easily verifiable,

93
00:11:56,400 --> 00:12:04,080
if not codifiable, because it's not like material science where you can put a number to it.

94
00:12:05,200 --> 00:12:09,520
Not unless you're really, really, really good. Apparently the Buddha was able to put

95
00:12:09,520 --> 00:12:13,600
numbers on these things and say how many in this and how many of this, how many of that,

96
00:12:13,600 --> 00:12:19,520
followed by this, followed by that. But you can see the way things, the general way things work,

97
00:12:19,520 --> 00:12:28,560
to postulate death, to postulate a moment where that stops happening requires some explanation

98
00:12:29,680 --> 00:12:36,880
and requires a leap of faith, actually. So this is a way of turning the tables on the materialists

99
00:12:36,880 --> 00:12:43,920
and saying to them, well, you're not the skeptics, you're the faith believers, you're the believers,

100
00:12:43,920 --> 00:12:49,760
you believe in something that is theoretical, you believe in a theoretical death that is somehow

101
00:12:49,760 --> 00:12:55,920
influencing reality in a way that is not experiential, that you've never experienced,

102
00:12:55,920 --> 00:13:00,720
you don't know anyone else who has experience, you can't verify it with anyone else,

103
00:13:00,720 --> 00:13:05,040
and you can't verify it inside yourself. So there's a belief going on there, you may very well

104
00:13:05,040 --> 00:13:11,120
be right. It may be that at the moment of physical death, there is a mental death as well.

105
00:13:11,120 --> 00:13:19,680
But that's a violation, or yeah, it's a violation of the laws of experiential reality

106
00:13:21,120 --> 00:13:25,840
of cause and effect, because all of the causes at the moment of death then don't have an effect.

107
00:13:25,840 --> 00:13:31,120
And so there's something going on there, there's something that has to be explained there.

108
00:13:31,120 --> 00:13:42,160
Now in Buddhism, we only postulate one example of this, or one instance where this is possible,

109
00:13:42,160 --> 00:13:48,320
and that is where no more causes are created, where the mind has given up that which causes,

110
00:13:48,880 --> 00:13:53,600
and that's what Nurban is, that's what's so special about this. That's why it's such an important

111
00:13:53,600 --> 00:13:59,600
thing. It's like, you know, we're positing something that requires an explanation, that

112
00:13:59,600 --> 00:14:04,800
is an exception. Nurban is not just something all well, then you die, and that's it.

113
00:14:04,800 --> 00:14:13,440
Nurban is, well, because unless you reach this very special state, death means nothing.

114
00:14:14,640 --> 00:14:20,400
There's no end to our experience, to the seeing, the hearing, the smelling, the tasting,

115
00:14:20,400 --> 00:14:26,400
the feeling of thinking, to becoming, to wandering on and on and on and on again.

116
00:14:26,400 --> 00:14:34,880
And this is experientially verifiable. I mean, there are actually a lot of evidence to support

117
00:14:36,080 --> 00:14:40,480
this. But the point that I'm trying to make is you don't need that evidence. The evidence of

118
00:14:40,480 --> 00:14:47,200
out-of-body experiences, of near-death experiences, of past-like memories. That's really interesting,

119
00:14:47,200 --> 00:14:53,840
and I encourage people to look at that, to say that, oh yeah, there actually is some

120
00:14:53,840 --> 00:14:57,680
consistency here. You know, this isn't just a theory, but there's something here that's

121
00:14:57,680 --> 00:15:06,240
pointing in that direction. But without any of that evidence, it's the default to assume

122
00:15:06,240 --> 00:15:13,760
that the experience continues on, because in order to say that physical death is

123
00:15:13,760 --> 00:15:18,720
the mental death, you have to enter into the theoretical realm of four-dimensional spacetime,

124
00:15:18,720 --> 00:15:29,440
which has seemed to be dependent on observation, until, because the theory goes that until a scientist

125
00:15:29,440 --> 00:15:35,520
performs an experiment, you can't even talk about the result that they're going together. You

126
00:15:35,520 --> 00:15:42,880
can't even talk about the things that they're experimenting on. It requires the scientist to make

127
00:15:42,880 --> 00:15:49,280
a decision, which experiment they're going to perform, which basically in practical terms means

128
00:15:49,280 --> 00:15:55,760
that until there is intention, until there is the focus, the mind which is paying attention to

129
00:15:55,760 --> 00:16:00,480
something, you can't even talk about that which it's paying attention to. This is exactly in line

130
00:16:00,480 --> 00:16:07,360
with the Buddhist teaching, and this is quantum physics, orthodox quantum physics, and the theory.

131
00:16:07,360 --> 00:16:14,400
So it's interesting to hear Stephen Hawking talking like this, but it's not surprising because

132
00:16:14,400 --> 00:16:21,200
there are quite a lot of physicists who side with them, and they have many theories that have gone

133
00:16:21,200 --> 00:16:27,040
beyond orthodox quantum physics. Now there's even string theory and so on. But the point being

134
00:16:27,040 --> 00:16:38,160
that this is all theoretical, the accepted fact is that the only thing that's empirical is our

135
00:16:38,160 --> 00:16:46,320
experience, our experimentation, our observations of reality, the rest of it, all of our conclusions,

136
00:16:46,320 --> 00:16:55,840
and so on are all theoretical. So I think that should clear some of it up here, but just to

137
00:16:55,840 --> 00:17:08,240
the corollary from this, having to do with why one should strive, takes a little bit more

138
00:17:08,240 --> 00:17:15,440
explaining why one should strive for enlightenment. I think there's at least a little bit of impetus

139
00:17:15,440 --> 00:17:21,600
out there already, there should be, in the fact that if you don't strive, death isn't going to

140
00:17:21,600 --> 00:17:30,320
bring any change, you're going to be the same person that you are now after you die. So in more

141
00:17:30,320 --> 00:17:34,400
so, you're going to be going along the course of cause and effect. So if you're a person who

142
00:17:34,400 --> 00:17:42,400
kills, deals, lies, cheats, takes drugs and alcohol and dilutes yourself and so on, guess what?

143
00:17:43,120 --> 00:17:49,840
The effects of that are going to become the causes for future effects.

144
00:17:49,840 --> 00:17:58,240
It's a chain, and I think this should wake people up to some of the terrible things that we do,

145
00:17:59,040 --> 00:18:09,440
and really it really needs to be said, this sort of thing, because we're violating it so

146
00:18:09,440 --> 00:18:18,240
terribly. We believe that at the moment of death, that's it. Just put it off. It's okay, you can do a

147
00:18:18,240 --> 00:18:24,720
little bit of this stuff, these bad things, thinking that in the end, we all die, and just sort of

148
00:18:24,720 --> 00:18:33,040
rushing towards death. When you die, you'll find some sort of relief route. Well, guess what? That's

149
00:18:33,040 --> 00:18:40,320
a theory. It's based on belief. It's not sustainable by the facts, and it's not true. It's not the

150
00:18:40,320 --> 00:18:45,440
nature of reality. The nature of reality has experience that continues on, and it's based on cause

151
00:18:45,440 --> 00:18:53,600
and effect. I mean, it makes so much sense. Irrigardless of any sort of proof or any sort of

152
00:18:54,720 --> 00:19:07,760
experience, any sort of evidence to back it up, it's so easy to see, and it's so easy to understand,

153
00:19:08,480 --> 00:19:14,640
and it makes such sense of the universe. Something that was previously so full of questions,

154
00:19:14,640 --> 00:19:21,040
why, and how, and what, and what is the meaning of life, and so on, becomes so simple. It suddenly

155
00:19:21,040 --> 00:19:25,760
makes sense. Why are some people rich? Why are some people poor? Why is there suffering in the

156
00:19:25,760 --> 00:19:30,960
world, and so on, and so on? Suddenly, all of these questions of why are we here, and why is there

157
00:19:30,960 --> 00:19:40,560
the universe? Suddenly becomes so clear. For many points of view, it's easy to understand why this

158
00:19:40,560 --> 00:19:48,400
might be the case. Why we should practice to better ourselves is for a better future. This is one

159
00:19:48,400 --> 00:19:54,080
way of looking at it. But there's something that's much more fundamental, a reason for practicing

160
00:19:54,080 --> 00:20:01,920
that's much more fundamental than I'd like to explain, and the explanation is that I use the

161
00:20:01,920 --> 00:20:13,120
analogy of a sweater. When we begin to practice, the reason why we begin to practice is because

162
00:20:13,120 --> 00:20:21,280
we have suffering. We have something in our lives that is causing us pain, causing us discomfort.

163
00:20:21,280 --> 00:20:26,160
So we start with that, and this is analogous to pulling on the sweater. Because you like the

164
00:20:26,160 --> 00:20:30,960
sweater. You think the sweater's fine. It's just this one thread that's causing problems. So you pull

165
00:20:30,960 --> 00:20:37,840
on it. And as you pull on the thread, the whole sweater starts to unravel. And so finally, there's

166
00:20:37,840 --> 00:20:42,320
no sweater left. You pull and you pull and you pull and one thing leads to another and suddenly

167
00:20:42,320 --> 00:20:49,760
it's ungone. Proper practice of the Buddha's teaching is in this way. There is no leap of faith.

168
00:20:49,760 --> 00:20:58,160
There is no practice that is out of proportion to the observations or the need to practice.

169
00:20:58,160 --> 00:21:02,240
You practice according to the need, according to the pain and the suffering that you have in your

170
00:21:02,240 --> 00:21:11,040
lives. The result is to finding more and more and more, you know, one problem leads to another,

171
00:21:11,040 --> 00:21:18,400
one realization leads to another. You realize that the cause of suffering is not as simple as

172
00:21:18,400 --> 00:21:22,720
you thought. It's not getting rid of the headaches or getting rid of the backaches or the tooth

173
00:21:22,720 --> 00:21:28,000
things or getting rid of this, you know, you had a problem with a person or relationship that ended

174
00:21:28,000 --> 00:21:33,200
or so on. You lost your job. It's not about just getting your job back about getting the relationship

175
00:21:33,200 --> 00:21:39,440
back about fixing this or fixing that. Once you start to really see the reasons why you're suffering,

176
00:21:39,440 --> 00:21:44,240
you want to practice more and more and your practice becomes much more part of your life.

177
00:21:44,240 --> 00:21:48,400
Because you see that the answers are much more fundamental than you thought.

178
00:21:48,400 --> 00:22:01,200
Until finally, you realize that there is nothing worth clinging to. There is no stable reality

179
00:22:01,200 --> 00:22:08,240
in this universe. That brings up another point that I wanted to talk about. The idea of

180
00:22:09,920 --> 00:22:16,880
Buddhist practice being suicide and the end of a self. This is very clearly discussed in the

181
00:22:16,880 --> 00:22:27,920
Buddhist teaching and with a very simple response that only for a person who postulates a self

182
00:22:28,720 --> 00:22:38,160
is there any possibility of suicide or of death of that self. So the reason why we are afraid

183
00:22:38,160 --> 00:22:44,960
of enlightenment, we're afraid of Nirvana or we think of it as something like suicide is because

184
00:22:44,960 --> 00:22:53,200
we postulate an existence, a being that exists to life, that is. In Buddhism, we don't do that.

185
00:22:53,200 --> 00:22:57,680
In Buddhism, we understand experience to be experienced. We don't postulate an experienced

186
00:22:57,680 --> 00:23:03,600
sir of being a self that experiences. I think it may have been mentioned in your state that you

187
00:23:03,600 --> 00:23:08,320
in your question that you don't believe in the self. I'm not sure if it's in there, but someone

188
00:23:08,320 --> 00:23:13,120
was talking about how it's only for people who believe in the self and that's absolutely

189
00:23:13,120 --> 00:23:22,880
I think a proper thing to say. Without a self, Nirvana is not the death of a self because there

190
00:23:22,880 --> 00:23:29,760
was no self to begin with. It's a death of this concept that there was a self. Nirvana is simply

191
00:23:29,760 --> 00:23:38,000
the ending of a rising which each rising is impermanent suffering in non-self. It's something that

192
00:23:38,000 --> 00:23:44,240
in and of itself has no insistence. It comes and goes. It's something that's instantaneous

193
00:23:44,240 --> 00:23:50,240
anyway. Nirvana is simply when this doesn't happen anymore. When there is freedom from all of

194
00:23:50,240 --> 00:23:59,360
this phenomenon, the arising of things. And that is something that comes naturally. It's something

195
00:23:59,360 --> 00:24:06,000
that comes through an understanding of reality and giving up of our attachment to things that

196
00:24:06,000 --> 00:24:14,640
are causing us suffering. So I hope this is helped. And I think the reasons why you meditate and

197
00:24:14,640 --> 00:24:19,760
read suit this is because you find them wholesome and interesting. That's the best reason to do it.

198
00:24:20,480 --> 00:24:26,880
I think any kind of, as people have said in the answers already, any kind of intellectual

199
00:24:26,880 --> 00:24:35,520
arguments about why you should practice and why you should, or about Nirvana and some hypothetical

200
00:24:35,520 --> 00:24:39,680
state that you haven't experienced for yourself. Because the other thing about Nirvana is the

201
00:24:39,680 --> 00:24:45,840
reason why we postulate is because the people who have practiced have experienced it. Once you

202
00:24:45,840 --> 00:24:50,160
experience it, then you start talking about it. You say, yeah, there's this thing called Nirvana

203
00:24:50,160 --> 00:24:58,000
and it's the freedom from suffering. And so then you start to teach this sort of thing because you

204
00:24:58,000 --> 00:25:02,880
have experienced it. We don't start there and say, okay, there's Nirvana and I'm going to try to

205
00:25:02,880 --> 00:25:07,440
reach it. You start practicing because you have suffering. And once you practice and practice,

206
00:25:07,440 --> 00:25:11,440
you eventually reach Nirvana and then you can say for yourself that this is the ultimate

207
00:25:11,440 --> 00:25:16,640
freedom from suffering. You've gone along and reduced and reduced and reduced your suffering

208
00:25:16,640 --> 00:25:21,440
until you reach Nirvana. And in that moment, there is no suffering. When you realize it,

209
00:25:21,440 --> 00:25:27,200
you say, that's the goal and you strive for that and you work harder to obtain that only because

210
00:25:27,200 --> 00:25:31,200
you realize it and only because you know for yourself that that's what it is, not out of faith,

211
00:25:31,200 --> 00:25:41,360
not because I said it because the Buddha said it. So practicing just to better yourself is the

212
00:25:41,360 --> 00:25:54,320
best reason already. As far as talking about Arahans and why Arahans won't commit suicide,

213
00:25:54,320 --> 00:25:59,840
I don't, I can't think of any instance of where an Arahan committed suicide. There may be one

214
00:25:59,840 --> 00:26:06,000
because it may be that an Arahan says to himself, here I'm sick and I'm a burden on my friends

215
00:26:06,000 --> 00:26:12,080
and so on. Therefore, I have no reason to live. I don't know that they would actually, I've never

216
00:26:12,080 --> 00:26:17,840
heard what an Arahan would actually commit suicide. But it may be the case that it's in there because

217
00:26:21,040 --> 00:26:27,840
to me, what an Arahan, what an enlightened being, what it wouldn't do is for quite a bit of

218
00:26:27,840 --> 00:26:35,600
speculation. But mostly an Arahan would not commit suicide because they're not interested in it.

219
00:26:35,600 --> 00:26:43,280
They don't have any attachments anymore. But as for a person who hasn't become enlightened,

220
00:26:43,280 --> 00:26:47,200
they might commit suicide because they still have an attachment and they feel the suffering and

221
00:26:47,200 --> 00:26:52,080
they don't want to suffer anymore. They also have the wrong view and wrong idea that by killing

222
00:26:52,080 --> 00:27:00,240
themselves somehow they will attain some kind of peace. Now as I hope I've at least given an

223
00:27:00,240 --> 00:27:06,240
interpretation of reality, that that points out that that's not the case and whether you agree

224
00:27:06,240 --> 00:27:13,440
with what I've said or not is totally up to you. And I hope this is helped to give some

225
00:27:13,440 --> 00:27:21,520
background and shed some light on a teravada Buddhist interpretation or teravada Buddhist

226
00:27:21,520 --> 00:27:28,720
explanation of things like rebirth and nirvana, which I think neither requires

227
00:27:28,720 --> 00:27:35,040
belief and certainly the practice and meditation doesn't require belief. It requires a little bit

228
00:27:35,040 --> 00:27:39,440
of faith that what you're going to gain, that you're going to gain something out of the practice

229
00:27:39,440 --> 00:27:44,560
in the beginning. And then once you start to practice, you gain something. So then your faith

230
00:27:45,360 --> 00:27:50,320
was well rewarded and is no longer required. And the more you practice, the more you gain.

231
00:27:51,280 --> 00:27:56,000
When you stop gaining, you can stop practicing. If the practice somehow doesn't bring you

232
00:27:56,000 --> 00:28:04,880
any good results, then you're obviously bound to stop. And so the only reason people really

233
00:28:04,880 --> 00:28:10,000
succeed in meditation is because they continue to gain benefit. And that's really the wonderful

234
00:28:10,000 --> 00:28:16,800
thing about meditation practice and experiential examination and study of reality.

235
00:28:17,360 --> 00:28:21,280
It's not the more you do it, the more benefit you gain and the more you want to do it.

236
00:28:21,280 --> 00:28:30,080
So I hope that helps. This has been an answer to this sort of question. And hopefully to

237
00:28:30,080 --> 00:28:39,680
counteract some of the, what would believe to be the wrong understanding that is going to be

238
00:28:39,680 --> 00:28:48,720
created by such a famous person's wrong use. So maybe you think I wrong use, but to reach their own.

239
00:28:48,720 --> 00:28:58,720
So all the best, let me take care.

