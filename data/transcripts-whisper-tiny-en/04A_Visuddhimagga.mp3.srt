1
00:00:00,000 --> 00:00:07,000
I think we were on page 37, paragraph 100.

2
00:00:15,000 --> 00:00:20,000
Now, there are full kinds of virtue or sealer.

3
00:00:20,000 --> 00:00:26,000
And the first is called Bartimocha restraint.

4
00:00:26,000 --> 00:00:34,000
That is keeping the rules in the disciplinary code for monks and nuns.

5
00:00:34,000 --> 00:00:39,000
And that Bartimocha restraint is undertaken out of faith.

6
00:00:39,000 --> 00:00:49,000
So having faith in the Buddha and his teachings, monks and nuns keep these Bartimocha rules intact.

7
00:00:49,000 --> 00:00:59,000
And the next one, the restraint of these sense faculties is to be undertaken with mindfulness.

8
00:00:59,000 --> 00:01:08,000
In fact, the restraint of sense faculties is not strictly sealer of virtue.

9
00:01:08,000 --> 00:01:12,000
It is a control of one senses.

10
00:01:12,000 --> 00:01:21,000
So control of senses can be done by mindfulness or should be done by mindfulness.

11
00:01:21,000 --> 00:01:38,000
When someone sees an object, one must keep mindfulness with her so that he or she does not get unwholesome thoughts

12
00:01:38,000 --> 00:01:43,000
or unwholesome mental states with regard to that object.

13
00:01:43,000 --> 00:01:50,000
And in order to guard against unwholesome thoughts entering our minds, we need mindfulness.

14
00:01:50,000 --> 00:02:06,000
So the second sealer of the restraint of the sense faculties is to be undertaken by mindfulness.

15
00:02:06,000 --> 00:02:17,000
So by mindfulness, one tries not to take the signs or not to apprehend the signs and not to apprehend the particulars,

16
00:02:17,000 --> 00:02:32,000
but just one tries to stop at seeing, hearing and so on.

17
00:02:32,000 --> 00:02:45,000
In paragraph 101, when not undertaken thus, virtue of Bartimocha restraint,

18
00:02:45,000 --> 00:02:51,000
a word is missing there, restraint also is enduring.

19
00:02:51,000 --> 00:02:57,000
If we do not undertake the restraint of sense faculties with mindfulness,

20
00:02:57,000 --> 00:03:07,000
then the virtue of Bartimocha restraint, the first one also cannot endure long and not cannot stay long.

21
00:03:07,000 --> 00:03:19,000
So it does not last like a crop not fenced in with branches.

22
00:03:19,000 --> 00:03:25,000
As it is rated by the robot, the fireman says a village with open gauge, is by thieves and so on.

23
00:03:25,000 --> 00:03:38,000
So the Bartimocha restraint becomes enduring when the restraint of sense faculties is undertaken by mindfulness.

24
00:03:38,000 --> 00:03:50,000
Because when we apply mindfulness and try not to take the signs or particulars of the object we experienced,

25
00:03:50,000 --> 00:04:00,000
then we will not come to the transgression of the rules in the Bartimocha.

26
00:04:21,000 --> 00:04:31,000
And with regard to the restraint of sense faculties, the story of Elder Wangisa was killed in.

27
00:04:31,000 --> 00:04:38,000
So he went out for arms and he saw a woman and he had thoughts of us arise in his mind.

28
00:04:38,000 --> 00:04:41,000
And so he asked, Fender will another what to do.

29
00:04:41,000 --> 00:04:51,000
And another said, because you perceive mistakenly, because you see that person as a person or as a woman,

30
00:04:51,000 --> 00:04:56,000
you have these kind of thoughts.

31
00:04:56,000 --> 00:05:12,000
So try to concentrate on the formations as alien, as suffering, as not self.

32
00:05:12,000 --> 00:05:23,000
And if you have this attitude, this understanding, then you can extinguish this fire of lust in your mind.

33
00:05:23,000 --> 00:05:28,000
So the Elder expelled his lesson then went on with his arms round.

34
00:05:28,000 --> 00:05:42,000
So he tried to expel these thoughts from his mind while he was going on arms.

35
00:05:42,000 --> 00:05:45,000
And then two stories are given.

36
00:05:45,000 --> 00:05:55,000
So these stories are inspiring stories, how monks control their senses.

37
00:05:55,000 --> 00:06:08,000
One monk did not know that there was painting and the dwelling and the other monk that was the same monk.

38
00:06:08,000 --> 00:06:18,000
He doesn't know whether when the king bowed down before him, he did not know whether it was a king or a queen.

39
00:06:18,000 --> 00:06:32,000
So he always made the king be happy, whoever bowed down before him.

40
00:06:32,000 --> 00:06:41,000
And down the page, there's a verse, let him not be hungry eyed.

41
00:06:41,000 --> 00:06:48,000
The party word is Lola.

42
00:06:48,000 --> 00:06:56,000
Lola means unsteady, or looking this way or that way, that is called Lola.

43
00:06:56,000 --> 00:07:00,000
Hungry eyed in the same thing.

44
00:07:00,000 --> 00:07:15,000
Here, hungry should be understood, not in the sense of wanting to eat something, but hungry for the visible objects.

45
00:07:15,000 --> 00:07:18,000
Is it the sense of distractable?

46
00:07:18,000 --> 00:07:20,000
Yes.

47
00:07:20,000 --> 00:07:28,000
So monks are taught to keep their eyes down, especially when they go into the village or into the city.

48
00:07:28,000 --> 00:07:34,000
So they are allowed to see about six feet in front of them.

49
00:07:34,000 --> 00:07:36,000
So they keep their eyes down.

50
00:07:36,000 --> 00:07:39,000
So they are not to look at this thing or that thing.

51
00:07:39,000 --> 00:07:49,000
And so when your eyes is turning from this object to that object, then it is called Pali and Pali Lola, Lola.

52
00:07:49,000 --> 00:07:54,000
Hungry eyed is not a particularly well often used English word.

53
00:07:54,000 --> 00:08:11,000
I see, maybe the word Lola can be translated as hungry maybe, but Lola is something like not steady, not composed.

54
00:08:11,000 --> 00:08:22,000
That's Lola.

55
00:08:22,000 --> 00:08:25,000
And then another story on page 40.

56
00:08:25,000 --> 00:08:27,000
L, D, Maha made them.

57
00:08:27,000 --> 00:08:30,000
So these stories are not difficult to understand.

58
00:08:30,000 --> 00:08:37,000
So the third, a livelihood purification, that is a third virtue for monks.

59
00:08:37,000 --> 00:08:43,000
So livelihood purification is to be undertaken by means of energy, by means of effort.

60
00:08:43,000 --> 00:08:48,000
Because if you are lazy and if you do not, if you are a monk and you are lazy and do not go out for arms,

61
00:08:48,000 --> 00:08:52,000
then your livelihood will not be pure.

62
00:08:52,000 --> 00:09:03,000
So livelihood purification is to be undertaken by means of effort or energy.

63
00:09:03,000 --> 00:09:19,000
So in order to keep the livelihood pure, Emma has to avoid wrong search or unsuitable search for things.

64
00:09:19,000 --> 00:09:30,000
In order not to resort to such things, then he has to go out for arms every day.

65
00:09:30,000 --> 00:09:41,000
And down, about the bottom of the page that is saying, and if he has caught future during, with mixed cold nuts,

66
00:09:41,000 --> 00:09:47,000
actually, this is cold nuts soaked in urine.

67
00:09:47,000 --> 00:09:52,000
And it does not mean necessarily mean future during.

68
00:09:52,000 --> 00:09:57,000
Although the word booty and parley is used here.

69
00:09:57,000 --> 00:10:03,000
So it is just urine cow urine especially.

70
00:10:03,000 --> 00:10:06,000
Do you know gore nut?

71
00:10:06,000 --> 00:10:10,000
No.

72
00:10:10,000 --> 00:10:13,000
I don't know.

73
00:10:13,000 --> 00:10:17,000
The other name is a myroblum.

74
00:10:17,000 --> 00:10:30,000
M-Y-R-O-B-E-L-E-M-O-B-L-A-M, myroblum.

75
00:10:30,000 --> 00:10:32,000
It is a bitter taste.

76
00:10:32,000 --> 00:10:34,000
And it is soaked in cow urine.

77
00:10:34,000 --> 00:10:40,000
And it is supposed to be medicinal.

78
00:10:40,000 --> 00:10:48,000
So if a person gets that kind of gold nut or that kind of myroblum, or the four streets.

79
00:10:48,000 --> 00:10:53,000
So they are supposed to be medicine for months.

80
00:10:53,000 --> 00:10:57,000
The one is not so good, but the four streets are good things.

81
00:10:57,000 --> 00:11:09,000
Then he who keeps his livelihood, she should take what is not good and give go to others.

82
00:11:09,000 --> 00:11:17,000
The four streets are a medicinal street made of four ingredients.

83
00:11:17,000 --> 00:11:20,000
What are the four?

84
00:11:20,000 --> 00:11:23,000
In Buddhist countries, in our countries, it is very usual.

85
00:11:23,000 --> 00:11:25,000
I mean, very common.

86
00:11:25,000 --> 00:11:34,000
Almost everybody know, at least the thing which monks can eat in the afternoon.

87
00:11:34,000 --> 00:11:44,000
And these four things, four sweet things are ghee, honey, oil, and molasses.

88
00:11:44,000 --> 00:11:49,000
They are mixed.

89
00:11:49,000 --> 00:11:56,000
And if you are to be strict, then you are not to cook on a fire.

90
00:11:56,000 --> 00:12:09,000
You can put it in the sun, and then stir, and sometime afterwards, the mixture become a little thick.

91
00:12:09,000 --> 00:12:13,000
So that could be eaten in the afternoon.

92
00:12:13,000 --> 00:12:21,000
If you are weak, or if there is some kind of ailment to be got it off.

93
00:12:21,000 --> 00:12:31,000
So these are called four streets, just to medura.

94
00:12:31,000 --> 00:12:41,000
So this last part means if you are offered both of these medicines, and you take the one that is not so good tasting,

95
00:12:41,000 --> 00:12:47,000
and leave it for other people, a good tasting medicine for others, that is a virtuous act.

96
00:12:47,000 --> 00:12:50,000
That is right.

97
00:12:50,000 --> 00:12:58,000
So such a monk is called supreme in noble ones heritage.

98
00:12:58,000 --> 00:13:11,000
Now, some kind of hinting or indication of these things are allowed with regard to some

99
00:13:11,000 --> 00:13:21,000
ragosits and not with regard to other ragosits.

100
00:13:21,000 --> 00:13:24,000
So paragraph 133 talks about that.

101
00:13:24,000 --> 00:13:26,000
As to the rope and other ragosits, no hint indication roundabout talk or intimation about ropes and arms

102
00:13:26,000 --> 00:13:31,000
food is allowable for a beakoo who is purifying his livelihood.

103
00:13:31,000 --> 00:13:40,000
So a monk must not ask for ragosits, not even hint or indicate or use a roundabout talk to get to do.

104
00:13:40,000 --> 00:13:45,000
To get to get a rope or to get food.

105
00:13:45,000 --> 00:13:55,000
But a hint indication or roundabout talk about a resting place, about a dwelling place, is allowable for one who has not taken up the aesthetic practices.

106
00:13:55,000 --> 00:14:03,000
So these are allowable for monks with regard to dwelling place.

107
00:14:03,000 --> 00:14:10,000
A hint is when one who is getting the preferring of the crown, etc., done for the purpose of making a resting place is us.

108
00:14:10,000 --> 00:14:13,000
What is being done when a looser, who is having it done?

109
00:14:13,000 --> 00:14:15,000
And then he replies, no one.

110
00:14:15,000 --> 00:14:19,000
That means there is no one who is going to build this place.

111
00:14:19,000 --> 00:14:22,000
So if you can, please build something like that.

112
00:14:22,000 --> 00:14:28,000
If you can, please build this place for me, something like that.

113
00:14:28,000 --> 00:14:34,000
Or any other such giving us an indication is the leaf follower, where do you live?

114
00:14:34,000 --> 00:14:45,000
Any mentioned when a looser, but leaf follower, a mention is not allowed for because this is an incorrect translation.

115
00:14:45,000 --> 00:14:50,000
But leaf follower is a mention not allowed for monks?

116
00:14:50,000 --> 00:14:51,000
No.

117
00:14:51,000 --> 00:14:55,000
Because a mention is allowed for monks.

118
00:14:55,000 --> 00:14:58,000
I say.

119
00:14:58,000 --> 00:15:00,000
So is it not allowed for monks?

120
00:15:00,000 --> 00:15:05,000
You live in a mansion and I live in a hut.

121
00:15:05,000 --> 00:15:11,000
So that is indication or any other such giving indication.

122
00:15:11,000 --> 00:15:16,000
Roundabout talk is saying the resting place for the community of bikhus is crowded.

123
00:15:16,000 --> 00:15:22,000
So the monastery is too small and it is not enough for monks.

124
00:15:22,000 --> 00:15:27,000
Or maybe able to assemble and that is something like a roundabout talk.

125
00:15:27,000 --> 00:15:35,000
That means build a new monastery at an extension to the monastery.

126
00:15:35,000 --> 00:15:41,000
So these are allowed with regard to resting place or dwelling place.

127
00:15:41,000 --> 00:15:47,000
But with regard to rope and arms food, they are not allowed.

128
00:15:47,000 --> 00:15:51,000
All however is allowed in the case of medicine.

129
00:15:51,000 --> 00:15:54,000
So medicine is a necessity.

130
00:15:54,000 --> 00:15:58,000
And if you have an ailment of disease, you really need medicine.

131
00:15:58,000 --> 00:16:04,000
And so with regard to medicine, all these hinting and honors are allowed.

132
00:16:04,000 --> 00:16:13,000
But when the disease is cured, is it not allowed to use the medicine obtained in this way?

133
00:16:13,000 --> 00:16:17,000
So there is two difference of opinion with regard to this.

134
00:16:17,000 --> 00:16:20,000
We need a specialist, it is all right.

135
00:16:20,000 --> 00:16:24,000
But so does the specialist say no.

136
00:16:24,000 --> 00:16:31,000
We need a specialist say Buddha has permitted.

137
00:16:31,000 --> 00:16:41,000
And so it is right to use medicine obtained by hinting and others even after the disease is cured.

138
00:16:41,000 --> 00:16:44,000
But so does the specialist say no.

139
00:16:44,000 --> 00:16:51,000
Because although there is no offense mean, although there is no breaking of a sudden rule,

140
00:16:51,000 --> 00:16:54,000
there is no breaking of a rule.

141
00:16:54,000 --> 00:16:57,000
Nevertheless, there's livelihood is solid.

142
00:16:57,000 --> 00:17:02,000
So it is not allowable.

143
00:17:02,000 --> 00:17:10,000
And if you want to purify your livelihood to the utmost, then do not use them.

144
00:17:10,000 --> 00:17:15,000
And so the story of Venerable Ananda and Maglana has given.

145
00:17:15,000 --> 00:17:23,000
I mean, Venerable Saripoda and Maglana has given here.

146
00:17:23,000 --> 00:17:30,000
And also, Elder Mahathis are the Mango-eida who live at Tiragumba.

147
00:17:30,000 --> 00:17:35,000
And the story is given in the food notes.

148
00:17:35,000 --> 00:17:45,000
Now, virtue dependent on requisites, that is the last of the four kinds of virtue.

149
00:17:45,000 --> 00:17:55,000
Virtue dependent on requisites means actually reflecting on the four requisites.

150
00:17:55,000 --> 00:18:04,000
Whenever you make use of them, or sometimes when you get them and also make use of them.

151
00:18:04,000 --> 00:18:09,000
So this is to be undertaken by means of understanding.

152
00:18:09,000 --> 00:18:11,000
Because you have to reflect.

153
00:18:11,000 --> 00:18:20,000
I use the rope just to get rid of heat or just to what of heat, coal, bites of insects and so on.

154
00:18:20,000 --> 00:18:24,000
So you have to use your understanding to reflect.

155
00:18:24,000 --> 00:18:33,000
Therefore, it is to be undertaken by means of understanding.

156
00:18:33,000 --> 00:18:38,000
And these reflections are given before, right?

157
00:18:38,000 --> 00:18:41,000
Yeah.

158
00:18:41,000 --> 00:18:50,000
Now, paragraph 144, here and reviewing is of two kinds at the time of receiving requisites.

159
00:18:50,000 --> 00:18:52,000
And at the time of using them.

160
00:18:52,000 --> 00:18:55,000
Sometimes you receive a rope today.

161
00:18:55,000 --> 00:18:59,000
And you may not use it until a month later or two months later.

162
00:18:59,000 --> 00:19:07,000
So there are two kinds of reviewing at receiving and at the time of using them.

163
00:19:07,000 --> 00:19:11,000
For use is blameless in one, who at the time of receiving ropes,

164
00:19:11,000 --> 00:19:16,000
etc., reviews them either as mere elements or as repulsive.

165
00:19:16,000 --> 00:19:20,000
And puts them aside for later use.

166
00:19:20,000 --> 00:19:27,000
Here also, the translation is a little inaccurate.

167
00:19:27,000 --> 00:19:38,000
So here, what is meant is, among when he receives the rope and others,

168
00:19:38,000 --> 00:19:44,000
he reflects or reviews on them as mere elements or as repulsive.

169
00:19:44,000 --> 00:19:51,000
And then put them aside and later make use of them.

170
00:19:51,000 --> 00:19:59,000
Just not just put them aside for later use, but put them aside and then later make use of them.

171
00:19:59,000 --> 00:20:04,000
And in one who reviews them does at the time of using them.

172
00:20:04,000 --> 00:20:12,000
That means you have to review both at receiving and at the time of using.

173
00:20:12,000 --> 00:20:24,000
Although this message may imply that even though you review at the moment of receiving.

174
00:20:24,000 --> 00:20:31,000
And if you do not review at the time when you use, then it is all right.

175
00:20:31,000 --> 00:20:40,000
But the emphasis here must be on reviewing at both times at receiving.

176
00:20:40,000 --> 00:20:48,000
And at the time when you make use of them.

177
00:20:48,000 --> 00:20:51,000
And here is an explanation to settle the matter.

178
00:20:51,000 --> 00:20:53,000
So there are four kinds of use.

179
00:20:53,000 --> 00:20:55,000
So four kinds of use are mentioned here.

180
00:20:55,000 --> 00:20:57,000
Use as theft.

181
00:20:57,000 --> 00:20:58,000
Use as debt.

182
00:20:58,000 --> 00:21:00,000
Use as inheritance.

183
00:21:00,000 --> 00:21:07,000
And use as a master.

184
00:21:07,000 --> 00:21:16,000
Now use as theft is by one who is unvirtuous and makes use of records.

185
00:21:16,000 --> 00:21:22,000
Even sitting in the midst of community is called use as theft.

186
00:21:22,000 --> 00:21:34,000
And here unvirtuous really mean who has broken a monk who has broken one of the four most important rules.

187
00:21:34,000 --> 00:21:41,000
Because if a monk breaks one of the four most important rules, he is no longer a monk.

188
00:21:41,000 --> 00:21:49,000
He may be wearing robes and he may be claiming as a monk, but in reality he is not a monk.

189
00:21:49,000 --> 00:21:56,000
And so his use of records is said to be by theft.

190
00:21:56,000 --> 00:22:02,000
Because he is not entitled to receive offerings given by the lay people.

191
00:22:02,000 --> 00:22:06,000
And so it is like stealing something.

192
00:22:06,000 --> 00:22:09,000
So it is called use as theft.

193
00:22:09,000 --> 00:22:20,000
So here unvirtuous means the person who is no longer a monk.

194
00:22:20,000 --> 00:22:27,000
And use without reviewing is use as a debt.

195
00:22:27,000 --> 00:22:34,000
Monks depend on lay people for these records and lay people offered them to monks.

196
00:22:34,000 --> 00:22:41,000
And so they have this responsibility to reflect or to review on these four records.

197
00:22:41,000 --> 00:22:53,000
And so if a monk does not review when accepting or when making use of them, then he is said to be using as a debt.

198
00:22:53,000 --> 00:22:58,000
Or he is indebted.

199
00:22:58,000 --> 00:23:06,000
And so he must review them when the rope should be reviewed every time it is used.

200
00:23:06,000 --> 00:23:14,000
Every time you pick up your rope and you have to review that I use this rope and just do what of cold,

201
00:23:14,000 --> 00:23:20,000
heat, bites of insects and so on.

202
00:23:20,000 --> 00:23:25,000
And the arms foot lump by lump.

203
00:23:25,000 --> 00:23:30,000
Everybody called this every more cell, right?

204
00:23:30,000 --> 00:23:35,000
At every mouthful and you have to have to review.

205
00:23:35,000 --> 00:23:43,000
So that is why talking while eating is discouraged for monks.

206
00:23:43,000 --> 00:23:52,000
A monk who talks while eating is supposed to be of bad behavior.

207
00:23:52,000 --> 00:24:00,000
Because instead of reviewing, he is speaking and when he speaks, then he is reviewing.

208
00:24:00,000 --> 00:24:06,000
In our meal chain, we say the first portion believes to say mouthful.

209
00:24:06,000 --> 00:24:16,000
The portion is for the precepts, the second is for the practice of samadhi, the third is to say mouthful.

210
00:24:16,000 --> 00:24:29,000
So you have to be doing something when eating, thinking of something.

211
00:24:29,000 --> 00:24:35,000
So without reviewing, you are using it as a debt.

212
00:24:35,000 --> 00:24:38,000
So the arms foot lump by lump.

213
00:24:38,000 --> 00:24:40,000
One who cannot do this?

214
00:24:40,000 --> 00:24:46,000
So review it before the meal that means in the morning, after the meal in the afternoon,

215
00:24:46,000 --> 00:24:50,000
or in the first watch of the night or in the middle watch or in the last watch of the night.

216
00:24:50,000 --> 00:24:57,000
If Don breaks on him without his having reviewed it, he finds himself in the position of one who has used it as a debt.

217
00:24:57,000 --> 00:25:08,000
So before Don, because in Videa, in the disciplinary proofs of monks, days are reckoned by Don.

218
00:25:08,000 --> 00:25:16,000
So Don is the beginning of one day, not midnight or sometime after midnight.

219
00:25:16,000 --> 00:25:23,000
So the day is reckoned by Don.

220
00:25:23,000 --> 00:25:39,000
So when the Don breaks and a monk does not review at all, then he is said to be using the things, using the requisite as a debt.

221
00:25:39,000 --> 00:25:51,000
Also, the resting place should be reviewed each time it is used when you go into the monastery and go out of the monastery.

222
00:25:51,000 --> 00:25:57,000
It recodes to mindfulness both in the accepting and the use of medicine is proba.

223
00:25:57,000 --> 00:26:04,000
So with regard to medicine, you have to review both in accepting and in making use of it.

224
00:26:04,000 --> 00:26:12,000
But while this is so, though there is an offense for one who uses it without mindfulness after mindful acceptance,

225
00:26:12,000 --> 00:26:18,000
there is no offense for one who is mindful in using after accepting without mindfulness.

226
00:26:18,000 --> 00:26:25,000
You may accept it without reviewing, but when you are using you must really review it.

227
00:26:25,000 --> 00:26:28,000
That is what is meant here.

228
00:26:28,000 --> 00:26:34,000
Now, purification is of four kinds.

229
00:26:34,000 --> 00:26:41,000
Purification by the teaching purification by restraint, purification by search and purification by reviewing.

230
00:26:41,000 --> 00:26:48,000
This is also, this has also to do with the full virtues or full sealers.

231
00:26:48,000 --> 00:26:51,000
So the first purification by teaching.

232
00:26:51,000 --> 00:26:58,000
Now, the word teaching is not correct here.

233
00:26:58,000 --> 00:27:03,000
There are two translations of Visodimava, the English translation.

234
00:27:03,000 --> 00:27:10,000
One was made by a Burmese gentleman and the other by Nyanamori.

235
00:27:10,000 --> 00:27:18,000
But both of them did not get the right translation here.

236
00:27:18,000 --> 00:27:29,000
The probably word here is Desana, D-E-S-A-N-A.

237
00:27:29,000 --> 00:27:39,000
And the word Desana generally means a summer or a teaching or a preaching.

238
00:27:39,000 --> 00:27:45,000
But here, Desana is used in a technical sense in Vinea.

239
00:27:45,000 --> 00:27:57,000
And Desana in Vinea means revealing or revealing ones of fans or confession.

240
00:27:57,000 --> 00:28:01,000
So purification by confession.

241
00:28:01,000 --> 00:28:11,000
So when a man has broken a rule or more rules, he has to confess it to another monk.

242
00:28:11,000 --> 00:28:20,000
So when he has confessed, then he gets free from the offense of breaking that rule.

243
00:28:20,000 --> 00:28:28,000
So there is a kind of sealer which is called purification by confession.

244
00:28:28,000 --> 00:28:34,000
So by just by confession, you can get rid of these offense.

245
00:28:34,000 --> 00:28:40,000
These are minor rules in the body marker, like cutting a tree.

246
00:28:40,000 --> 00:28:42,000
So monks are not allowed to cut trees.

247
00:28:42,000 --> 00:28:52,000
So if a monk cuts a tree or plough a flower or fruit, then he comes to a minor offense.

248
00:28:52,000 --> 00:29:04,000
And in order to get free from that minor offense, he just have to confess it to another monk.

249
00:29:04,000 --> 00:29:16,000
But there are some graver offenses which needs not only confession,

250
00:29:16,000 --> 00:29:23,000
but staying under provision for a period of time.

251
00:29:23,000 --> 00:29:37,000
And also there is the graver's offense, transgressing which a monk becomes,

252
00:29:37,000 --> 00:29:41,000
or to magically become a non-munk.

253
00:29:41,000 --> 00:29:47,000
So in that case, giving up rules is called purification.

254
00:29:47,000 --> 00:29:53,000
So giving up rules and becoming a, or giving up monk.

255
00:29:53,000 --> 00:30:04,000
I mean, becoming a, becoming a novice or becoming a layman, layperson is called purification there.

256
00:30:04,000 --> 00:30:14,000
So by purification, the potty mocha offenses incurred by breaking potty mocha rules can be,

257
00:30:14,000 --> 00:30:18,000
by, by confession can be purified.

258
00:30:18,000 --> 00:30:21,000
And purification by restraint.

259
00:30:21,000 --> 00:30:26,000
That is the sense, the restraint of sense faculties.

260
00:30:26,000 --> 00:30:29,000
That is called purification by restraint.

261
00:30:29,000 --> 00:30:34,000
And the third one, the livelihood purification is called purification by search,

262
00:30:34,000 --> 00:30:39,000
because you have to search for food, especially food.

263
00:30:39,000 --> 00:30:45,000
And search for means, search for by suitable means,

264
00:30:45,000 --> 00:30:53,000
and not by asking or not by begging, by what of mouth.

265
00:30:53,000 --> 00:30:58,000
And the last one is purification by reviewing.

266
00:30:58,000 --> 00:31:05,000
So in order to purify the last of the four sealers,

267
00:31:05,000 --> 00:31:11,000
you make review, you review whenever you receive a,

268
00:31:11,000 --> 00:31:14,000
a requisite or make use of a requisite.

269
00:31:14,000 --> 00:31:23,000
So these are the four purifications corresponding to the four sealers.

270
00:31:23,000 --> 00:31:33,000
And then use of the recognizes by the seven kinds of trainers is called use as an inheritance.

271
00:31:33,000 --> 00:31:39,000
Do you know trainers? What, what are trainers?

272
00:31:39,000 --> 00:31:52,000
Now, I think you, you know, there are eight, eight noble persons or eight kinds of noble persons.

273
00:31:52,000 --> 00:31:55,000
You don't know?

274
00:31:55,000 --> 00:31:59,000
There are four stages of enlightenment.

275
00:31:59,000 --> 00:32:09,000
You know that, four stages of enlightenment and eight types of consciousness

276
00:32:09,000 --> 00:32:16,000
that arise at the four stages of enlightenment.

277
00:32:16,000 --> 00:32:23,000
The part of stream entrant, the fruition of stream entrant.

278
00:32:23,000 --> 00:32:28,000
The part of one's returner, the fruition of one's returner.

279
00:32:28,000 --> 00:32:31,000
The part of non-returner, the fruition of non-returner,

280
00:32:31,000 --> 00:32:34,000
and the part of Arahan and fruition of Arahan.

281
00:32:34,000 --> 00:32:39,000
So they're the eight, eight types of consciousness.

282
00:32:39,000 --> 00:32:48,000
And it is said that there are eight, eight enlightened persons or noble persons.

283
00:32:48,000 --> 00:32:51,000
In the, in Pali, the word is aria.

284
00:32:51,000 --> 00:32:59,000
A-R-I-Y-A, you, you, you, you may be familiar with the word Arahan.

285
00:32:59,000 --> 00:33:08,000
So the person at the moment of stream entrant, third consciousness

286
00:33:08,000 --> 00:33:15,000
is called, it's called a noble, then a first noble person.

287
00:33:15,000 --> 00:33:24,000
And then the person at the fruition moment of stream entrant

288
00:33:24,000 --> 00:33:27,000
is called the second noble person and so on.

289
00:33:27,000 --> 00:33:31,000
So there are eight noble persons.

290
00:33:31,000 --> 00:33:41,000
Eight kinds of noble persons, not eight noble persons.

291
00:33:41,000 --> 00:33:44,000
Now, what are trainers?

292
00:33:44,000 --> 00:33:52,000
Trainers here means those who have attained to

293
00:33:52,000 --> 00:33:57,000
lower stages of enlightenment.

294
00:33:57,000 --> 00:34:04,000
Briefly, trainers are those who are neither ordinary persons, neither

295
00:34:04,000 --> 00:34:07,000
puto-genas, nor an Arahan.

296
00:34:07,000 --> 00:34:12,000
So they are stream, stream, and friends.

297
00:34:12,000 --> 00:34:16,000
Once returners and non-returners, they are called trainers.

298
00:34:16,000 --> 00:34:19,000
In Pali, they are called seekers.

299
00:34:19,000 --> 00:34:23,000
They are, they are not puto-genas, they are not ordinary persons.

300
00:34:23,000 --> 00:34:25,000
And they are not Arahan.

301
00:34:25,000 --> 00:34:28,000
So they are used, they use, use as an inheritance.

302
00:34:28,000 --> 00:34:32,000
But how then is it, is it the blessed ones,

303
00:34:32,000 --> 00:34:39,000
requisites or the lateies, requisites that are used or that they use?

304
00:34:39,000 --> 00:34:43,000
Although given by the lateie, they actually belong to the blessed one

305
00:34:43,000 --> 00:34:46,000
because it is by the blessed one that they are permitted.

306
00:34:46,000 --> 00:34:53,000
So because Buddha permitted us to, to accept things from late people,

307
00:34:53,000 --> 00:35:01,000
it is virtually the, the requisites of Buddha and not of the late people.

308
00:35:01,000 --> 00:35:08,000
And so when a son uses his father's belonging,

309
00:35:08,000 --> 00:35:13,000
his father's requisites, he said to be using as an inheritance.

310
00:35:13,000 --> 00:35:18,000
That is why it should be understood that the blessed ones, requisites are used.

311
00:35:18,000 --> 00:35:23,000
The confirmation here is in the Dhamma Daya Dasuda.

312
00:35:23,000 --> 00:35:26,000
That, Dasuda is in the Majima Nikaya, middle-length,

313
00:35:26,000 --> 00:35:28,000
saying that that's that.

314
00:35:28,000 --> 00:35:35,000
Buddha said in Dasuda that be the, in, be the inheritance of Dhamma

315
00:35:35,000 --> 00:35:38,000
and not of requisites.

316
00:35:38,000 --> 00:35:43,000
Buddha, Buddha, urged his disciples.

317
00:35:43,000 --> 00:35:49,000
Used by those whose gankas are destroyed. That means used by those who are arhans

318
00:35:49,000 --> 00:35:52,000
is called use as a master.

319
00:35:52,000 --> 00:36:00,000
They make use of them as masters because they have escaped the slavery of craving.

320
00:36:00,000 --> 00:36:07,000
So try to, from among these four, try to,

321
00:36:07,000 --> 00:36:14,000
make use as the, use as a master is the best one.

322
00:36:14,000 --> 00:36:21,000
But for those ordinary people who have not yet attained any state of enlightenment,

323
00:36:21,000 --> 00:36:31,000
there can be no use as an inheritance and also no use as a master.

324
00:36:31,000 --> 00:36:38,000
So there are only two, are they two?

325
00:36:38,000 --> 00:36:41,000
Use as theft or use as debt.

326
00:36:41,000 --> 00:36:48,000
But the commented said, they use made by those who keep their,

327
00:36:48,000 --> 00:36:55,000
their precepts, who keep their virtue pure,

328
00:36:55,000 --> 00:37:03,000
could be included in the use as inheritance.

329
00:37:03,000 --> 00:37:08,000
Because it is the opposite of the use as a debt.

330
00:37:08,000 --> 00:37:14,000
So for one possessed of virtue is called a trainer to, because of possessing his training.

331
00:37:14,000 --> 00:37:18,000
So this is stretching the meaning of the word.

332
00:37:18,000 --> 00:37:25,000
The word trainer or the Pali wadseka really means those persons who are neither

333
00:37:25,000 --> 00:37:27,000
Bhutu Janas nor arhans.

334
00:37:27,000 --> 00:37:32,000
But here, with the stretching of the meaning, a person who has good sealer,

335
00:37:32,000 --> 00:37:36,000
although he is a Bhutu Janas, can be called a trainer here.

336
00:37:36,000 --> 00:37:51,000
So his use of requisites could be included in the use as inheritance.

337
00:37:51,000 --> 00:37:56,000
As regards these three kinds of use, since use as a master is best.

338
00:37:56,000 --> 00:37:59,000
When a Bhutu undertakes virtue dependent on requisites,

339
00:37:59,000 --> 00:38:05,000
he should aspire to debt and use them after reviewing them in the way described.

340
00:38:05,000 --> 00:38:08,000
And there is one sentence missing here.

341
00:38:08,000 --> 00:38:10,000
I don't know why he left it out.

342
00:38:10,000 --> 00:38:19,000
For he who does, for he who so does is one who does what is to be done.

343
00:38:19,000 --> 00:38:24,000
That, that sentence is missing.

344
00:38:24,000 --> 00:38:27,000
That comes after he should aspire to do.

345
00:38:27,000 --> 00:38:30,000
After them in the way described.

346
00:38:30,000 --> 00:38:54,000
For he who so acts, for he who so acts, for he who so acts is one who does what is to be done.

347
00:38:54,000 --> 00:39:03,000
And then the, these tensors are given.

348
00:39:03,000 --> 00:39:08,000
And on page 45, in connection with the fulfilling of this virtue dependent on requisites,

349
00:39:08,000 --> 00:39:13,000
there should be two, the story of the novice, Sankarakita, the nephew.

350
00:39:13,000 --> 00:39:19,000
So he was eating and his preceptors said, don't let your, your tongue burn,

351
00:39:19,000 --> 00:39:21,000
or don't burn your tongue.

352
00:39:21,000 --> 00:39:25,000
So he was not eating hot food at that moment.

353
00:39:25,000 --> 00:39:30,000
But the preceptors said, don't, don't get your tongue burned.

354
00:39:30,000 --> 00:39:34,000
That means don't eat without reviewing.

355
00:39:34,000 --> 00:39:42,000
And so he became an Arahan later.

356
00:39:42,000 --> 00:39:52,000
Now, in paragraph 131, about the middle of the paragraph, that of magnanimous ordinary man,

357
00:39:52,000 --> 00:39:55,000
devoted to profitable things.

358
00:39:55,000 --> 00:40:05,000
So magnanimous ordinary man means a puto-gena, an informed puto-gena or a good puto-gena.

359
00:40:05,000 --> 00:40:18,000
And devoted to profitable things really means devoted to be personal meditation.

360
00:40:18,000 --> 00:40:25,000
And the number of precepts given here.

361
00:40:25,000 --> 00:40:34,000
Yanamali has a different number than we traditionally is interpreted.

362
00:40:34,000 --> 00:40:48,000
So traditionally, the number of the rules for monks is, how do I say?

363
00:40:48,000 --> 00:40:54,000
There are too many rules here.

364
00:40:54,000 --> 00:40:59,000
9,000 millions and 180 millions than as well.

365
00:40:59,000 --> 00:41:01,000
And 50 plus 100,000.

366
00:41:01,000 --> 00:41:03,000
And that is six again to swell.

367
00:41:03,000 --> 00:41:06,000
The total restraint disciplines.

368
00:41:06,000 --> 00:41:11,000
These rules, the enlightened one explains toll underheads for filling out,

369
00:41:11,000 --> 00:41:13,000
which the discipline restraint contains.

370
00:41:13,000 --> 00:41:31,000
So traditionally, the number is, would you write down, 9, 1, 8, 0, 5, 0, 3, 6,

371
00:41:31,000 --> 00:41:41,000
0, 0, 0, 0.

372
00:41:41,000 --> 00:41:48,000
Come on.

373
00:41:48,000 --> 00:41:55,000
Because for one rule, there are many minor offenses, minor, minor rules for anyone.

374
00:41:55,000 --> 00:42:01,000
I don't know.

375
00:42:01,000 --> 00:42:05,000
1,000, 1,000,000.

376
00:42:05,000 --> 00:42:09,000
2,000, yeah.

377
00:42:09,000 --> 00:42:31,000
91, 8, 0, 5, 0, 3, 6, 0, 0, 0.

378
00:42:31,000 --> 00:42:48,000
So we take pride in saying that I'm giving 91 billion precepts.

379
00:42:48,000 --> 00:42:58,000
Now, in paragraph 134, the magnanimous ordinary man's virtue,

380
00:42:58,000 --> 00:43:02,000
which from the time of admission to the order, is devoid even of the strain,

381
00:43:02,000 --> 00:43:06,000
a stain of a wrong thought because of its extreme purity,

382
00:43:06,000 --> 00:43:12,000
like a gem of what purest water.

383
00:43:12,000 --> 00:43:18,000
What does that mean?

384
00:43:18,000 --> 00:43:20,000
It doesn't make sense, right?

385
00:43:20,000 --> 00:43:23,000
Like a gem well polished.

386
00:43:23,000 --> 00:43:25,000
That is a meaning.

387
00:43:25,000 --> 00:43:30,000
Well washed, well polished.

388
00:43:30,000 --> 00:43:38,000
Not purest water.

389
00:43:38,000 --> 00:43:56,000
And then a story.

390
00:43:56,000 --> 00:43:59,000
So we'll keep the story.

391
00:43:59,000 --> 00:44:06,000
Stories are not difficult to understand.

392
00:44:06,000 --> 00:44:15,000
And then most stories on page 48, the one who broke his legs

393
00:44:15,000 --> 00:44:24,000
and asked the robbers to let them practice meditation for the night.

394
00:44:24,000 --> 00:44:30,000
And he practiced meditation on the pain caused by the broken bones.

395
00:44:30,000 --> 00:44:36,000
And at dawn he became an other hand, so he meditated on the pain.

396
00:44:36,000 --> 00:44:46,000
Pain becomes his object of meditation.

397
00:44:46,000 --> 00:44:53,000
And then on page 49, the quotation from Batizambida,

398
00:44:53,000 --> 00:44:57,000
that's not so easy to understand.

399
00:44:57,000 --> 00:45:01,000
We can just keep it.

400
00:45:01,000 --> 00:45:05,000
The case of killing living things, abandoning his virtue.

401
00:45:05,000 --> 00:45:08,000
Abstention his virtue, volition his virtue.

402
00:45:08,000 --> 00:45:11,000
Restraint his virtue, non-transgression his virtue.

403
00:45:11,000 --> 00:45:15,000
And then the others are given here.

404
00:45:15,000 --> 00:45:16,000
One by one.

405
00:45:16,000 --> 00:45:20,000
And the case of taking what is not given, in the case of sexual misconduct,

406
00:45:20,000 --> 00:45:22,000
in the case of four speech and so on.

407
00:45:22,000 --> 00:45:25,000
Even in the case of us, Jana, the hindrances,

408
00:45:25,000 --> 00:45:28,000
the abandoning of hindrances is virtue.

409
00:45:28,000 --> 00:45:32,000
In the case of second Jana, the abandoning of applied thought

410
00:45:32,000 --> 00:45:35,000
and sustained thought or initial application and sustained

411
00:45:35,000 --> 00:45:41,000
application is the virtue and so on.

412
00:45:49,000 --> 00:45:53,000
So on page 51, paragraph 141.

413
00:45:53,000 --> 00:45:56,000
And here there is no state called abandoning,

414
00:45:56,000 --> 00:46:00,000
other than the mere non-arising of the killing or living things,

415
00:46:00,000 --> 00:46:02,000
et cetera, as stated.

416
00:46:02,000 --> 00:46:07,000
Now, abandoning really means not letting them arise.

417
00:46:07,000 --> 00:46:12,000
Because if they have arisen, then they have already arisen

418
00:46:12,000 --> 00:46:15,000
and you cannot do anything about them.

419
00:46:15,000 --> 00:46:24,000
The abandoning really means...

