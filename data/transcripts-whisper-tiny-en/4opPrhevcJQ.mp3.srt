1
00:00:00,000 --> 00:00:28,760
Good evening everyone, can you hear me, hello, good evening everyone.

2
00:00:28,760 --> 00:00:38,760
Are there a group tonight?

3
00:00:38,760 --> 00:00:45,560
I don't know last night's talk, I gave a talk about Dhamma or Dhamma.

4
00:00:45,560 --> 00:00:48,160
I think it was particularly awful.

5
00:00:48,160 --> 00:00:56,560
I got several down votes already on YouTube and today half of our crowd is missing.

6
00:00:56,560 --> 00:01:02,560
I don't know what was wrong with it, but I've driven everyone away.

7
00:01:02,560 --> 00:01:09,560
I never could of course be another explanation.

8
00:01:09,560 --> 00:01:20,560
I didn't think it was particularly awful, but I don't know the internet now.

9
00:01:20,560 --> 00:01:27,560
The rest is not important, we should never dwell on the past.

10
00:01:27,560 --> 00:01:35,560
Don't worry about the future.

11
00:01:35,560 --> 00:01:49,560
Simon thinks yesterday's talk was awesome, but he thinks all my talks are awesome.

12
00:01:49,560 --> 00:02:13,560
Anyway, today's talk is about the present moment.

13
00:02:13,560 --> 00:02:19,560
There's a book that everyone says I should read or everyone asks me about.

14
00:02:19,560 --> 00:02:34,560
I'm constantly asked about this book called The Power of Now by this famous author, speaker, motivation,

15
00:02:34,560 --> 00:02:44,560
or spiritual guide, I don't know Eckhart Tolle.

16
00:02:44,560 --> 00:02:51,560
Something to do with Oprah Winfrey or something.

17
00:02:51,560 --> 00:02:56,560
The Power of Now is right though, that's a good title for a book.

18
00:02:56,560 --> 00:03:03,560
It's a good subject, it's a good phrase.

19
00:03:03,560 --> 00:03:26,560
It's not to be taken lightly.

20
00:03:26,560 --> 00:03:33,560
There's a story, well there's a verse.

21
00:03:33,560 --> 00:03:42,560
It actually appears in a couple of places, but one well-known places in the Tamiya Jotica.

22
00:03:42,560 --> 00:03:49,560
If you don't know about the Tamiya Jotica, it's a long story, it's a really good story.

23
00:03:49,560 --> 00:03:56,560
It's one of the ten great Jotica's great meaning, meaning more than it has a lot of verses,

24
00:03:56,560 --> 00:04:04,560
but also great in that it's one of the more powerful moving stories.

25
00:04:04,560 --> 00:04:13,560
The story goes that there was this prince who, even before he was born, he was an angel in heaven.

26
00:04:13,560 --> 00:04:23,560
They say this king on earth needed a son and the king of the gods, the king of the angels persuaded the body sat there to be reborn.

27
00:04:23,560 --> 00:04:36,560
As a human being, I guess the angels sort of decided where they want to be reborn.

28
00:04:36,560 --> 00:04:48,560
He was reborn as this prince, but when he was very young, he was sitting on his father's lap.

29
00:04:48,560 --> 00:04:53,560
His father was sitting in judgment, and these robbers came to the king and the king.

30
00:04:53,560 --> 00:05:04,560
Sentence them all to being stabbed with spears and paled upon stakes, having their heads cut off.

31
00:05:04,560 --> 00:05:11,560
Their hands cut off and on typical king's stuff.

32
00:05:11,560 --> 00:05:17,560
But the Bodhisattas lying there was just horrified.

33
00:05:17,560 --> 00:05:26,560
The young boy, and as he was sitting there, he became all very familiar to him,

34
00:05:26,560 --> 00:05:31,560
and he remembered his past life that in the past he had been a king.

35
00:05:31,560 --> 00:05:37,560
He had done this same sort of thing.

36
00:05:37,560 --> 00:05:47,560
He had engaged in sentencing robbers and really being a fairly nasty person,

37
00:05:47,560 --> 00:05:56,560
whether the people deserved it or not, engaging in all sorts of nasty torture and punishment.

38
00:05:56,560 --> 00:06:03,560
As a result, it had been reborn in hell in this next life.

39
00:06:03,560 --> 00:06:09,560
He had lived in hell for many years, and he was just starting to get on the way up

40
00:06:09,560 --> 00:06:15,560
when he had made it to the lower angel realms.

41
00:06:15,560 --> 00:06:17,560
He said, what am I doing back here?

42
00:06:17,560 --> 00:06:24,560
I'm back where I started one day, I'll be king, and I'll have to do that same thing.

43
00:06:24,560 --> 00:06:29,560
And so he decided that he would pretend to be...

44
00:06:29,560 --> 00:06:35,560
pretend to be crippled, he would pretend to be dumb, he would pretend to be an invalid.

45
00:06:35,560 --> 00:06:39,560
And the story goes on and on, and for 16 years,

46
00:06:39,560 --> 00:06:49,560
he made as though he couldn't see or hear or he was deaf and dumb and mute and stupid.

47
00:06:49,560 --> 00:06:52,560
But the doctors looked at him and they said, there's nothing wrong,

48
00:06:52,560 --> 00:06:58,560
they tried to test him and they tested him in all these ways by putting...

49
00:06:58,560 --> 00:07:03,560
When he was sitting in his playhouse, they would light set fire to it.

50
00:07:03,560 --> 00:07:07,560
Put him in this playhouse and then set fire to it, thinking that he'd run out.

51
00:07:07,560 --> 00:07:10,560
But he'd think to himself, the fires of hell are...

52
00:07:10,560 --> 00:07:14,560
These fires are nothing compared to the fires of hell.

53
00:07:14,560 --> 00:07:19,560
And he would lie there and be prepared to burn and so they put the fires out.

54
00:07:19,560 --> 00:07:24,560
Anyway, eventually he leaves home.

55
00:07:24,560 --> 00:07:31,560
Eventually they decide they're going to have to just get rid of him.

56
00:07:31,560 --> 00:07:35,560
And so they're going to bury him and kill him and just bury him.

57
00:07:35,560 --> 00:07:38,560
When he gets out of the city, where they're going to...

58
00:07:38,560 --> 00:07:43,560
Where this guy's going to kill him, he says, oh, here I am out,

59
00:07:43,560 --> 00:07:45,560
and they all think I'm going to die.

60
00:07:45,560 --> 00:07:51,560
So he just wanders off and goes and lives in the forest.

61
00:07:51,560 --> 00:07:58,560
Eventually the king and all the queen and all of their retinue and the whole city follows him.

62
00:07:58,560 --> 00:07:59,560
It's a long story.

63
00:07:59,560 --> 00:08:02,560
I really don't want to get into the details.

64
00:08:02,560 --> 00:08:05,560
But when they hear he's gone forth, the king says he's going to go forth.

65
00:08:05,560 --> 00:08:11,560
The queen follows him and the courters and the royal palace all decide to follow.

66
00:08:11,560 --> 00:08:19,560
And when the populace hears about it, they all follow the king and everybody decides to go off into the forest.

67
00:08:19,560 --> 00:08:23,560
And they set up a monastery in Ashram.

68
00:08:23,560 --> 00:08:28,560
But when they get to the forest to find the prince,

69
00:08:28,560 --> 00:08:35,560
they find that he's wearing some kind of bark cloth or something and sitting.

70
00:08:35,560 --> 00:08:37,560
And it was quite alert.

71
00:08:37,560 --> 00:08:41,560
They talk to him and they say, you're not an invalid, and he's a no-no.

72
00:08:41,560 --> 00:08:43,560
I just knew I couldn't be king.

73
00:08:43,560 --> 00:08:46,560
And this was my way of getting out of it.

74
00:08:46,560 --> 00:08:48,560
And so they asked him about his life.

75
00:08:48,560 --> 00:08:52,560
And anyway, the point of this story is it comes to this verse where they ask him, how is it?

76
00:08:52,560 --> 00:09:02,560
You know, you were a prince and we had to feed you and bathe you and you lived in such luxury.

77
00:09:02,560 --> 00:09:05,560
But here you are living in the forest, and yet you look radiant.

78
00:09:05,560 --> 00:09:08,560
You look more alive than ever.

79
00:09:08,560 --> 00:09:21,560
And they asked him what he was eating and found that he was just eating simple leaves and the courses of course, food.

80
00:09:21,560 --> 00:09:32,560
And so the king asked him, how on this food can you subsist and look so wonderful, so alive.

81
00:09:32,560 --> 00:09:38,560
And so he told this rather poignant verse.

82
00:09:38,560 --> 00:09:43,560
He said, for the past I do not mourn nor for the future weep.

83
00:09:43,560 --> 00:10:03,560
Take the present as it comes and thus my color keep.

84
00:10:03,560 --> 00:10:04,560
There's another end.

85
00:10:04,560 --> 00:10:09,560
The same, almost the same verse appears somewhere else where an angel comes to see the Buddha, I think,

86
00:10:09,560 --> 00:10:23,560
and asks why the monks are, I don't know, maybe this is the same jot that I get.

87
00:10:23,560 --> 00:10:32,560
Anyway, this is the verse.

88
00:10:32,560 --> 00:10:43,560
When it highlights this, this important concept of the present moment.

89
00:10:43,560 --> 00:10:53,560
And then the second and the next verse, the verse after it says, when you worry about the past or some uncertain future need,

90
00:10:53,560 --> 00:11:06,560
it dries a fool's color up as when you cut the fresh green reed.

91
00:11:06,560 --> 00:11:16,560
So this is the other side of the equation or the other side of the coin.

92
00:11:16,560 --> 00:11:25,560
The present moment is such a powerful concept or reality because it's real.

93
00:11:25,560 --> 00:11:37,560
Because the present moment is what is really and truly occurring, which is really and truly exists.

94
00:11:37,560 --> 00:11:45,560
Why the past and the future are inferior and they are inferior, why they are a cause of so much problem.

95
00:11:45,560 --> 00:11:54,560
It's because they don't exist because they arise conceptually in the mind.

96
00:11:54,560 --> 00:12:05,560
And so the power, the amount of energy it takes

97
00:12:05,560 --> 00:12:14,560
and to live in the past and to live in the future is far more far greater than in the present.

98
00:12:14,560 --> 00:12:21,560
And on top of that, it's much more complicated or it's much more open to complication because it doesn't exist.

99
00:12:21,560 --> 00:12:27,560
The nature of concepts is they can be infinite, they're infinite.

100
00:12:27,560 --> 00:12:31,560
And you can always add something to them.

101
00:12:31,560 --> 00:12:35,560
Imagination is infinite, possibilities are endless.

102
00:12:35,560 --> 00:12:45,560
And so all of our complicated obsessions, desires, aversions, all of our egos, it's all caught up in these concepts.

103
00:12:45,560 --> 00:12:53,560
These end other concepts, but most especially the past and the future, what we're going to be, what we used to be,

104
00:12:53,560 --> 00:12:58,560
can also be caught up in concepts in the present.

105
00:12:58,560 --> 00:13:13,560
But past and future themselves are some of the worst concepts worrying about the future, reminiscing or bemoaning the past.

106
00:13:13,560 --> 00:13:17,560
And so it really is like that you have root yourself.

107
00:13:17,560 --> 00:13:24,560
The present moment is like this, this rooted experience,

108
00:13:24,560 --> 00:13:35,560
where you're well well connected with reality, just like a read or grass that is well rooted in the soil.

109
00:13:35,560 --> 00:13:39,560
And so it grows and it thrives.

110
00:13:39,560 --> 00:13:42,560
It's just the reality of the present moment.

111
00:13:42,560 --> 00:13:46,560
The person is in the past and the future is cut off from that.

112
00:13:46,560 --> 00:13:49,560
It's cut off from what's real.

113
00:13:49,560 --> 00:14:02,560
And with no grounding point, with no anchor gets lost and spun around and around in concepts.

114
00:14:02,560 --> 00:14:11,560
All of our theories and philosophies, all of our views and opinions, all of our likes and dislikes,

115
00:14:11,560 --> 00:14:24,560
all of the things we identify with or seek to escape from, all of this is caught up in concepts.

116
00:14:24,560 --> 00:14:29,560
And so we waste away.

117
00:14:29,560 --> 00:14:38,560
Anyone who lives in the future or lives in the past, they wither up, they dry up, their energies used up.

118
00:14:38,560 --> 00:14:48,560
They don't have this color, this radiance, they aren't alive, they don't even feel alive.

119
00:14:48,560 --> 00:15:02,560
A person who's never lived in the present moment might not realize it, but a person who has a person who's practiced and who's seen and who's experienced what it's like to live now.

120
00:15:02,560 --> 00:15:08,560
We'll be able to tell you the clear difference, and I mean this is the real reassurance in the practice.

121
00:15:08,560 --> 00:15:17,560
Again talking about how you know whether your practice is good, how do you know whether meditation is actually beneficial, how does it feel?

122
00:15:17,560 --> 00:15:28,560
If you haven't yet felt the difference between the present moment and dwelling in concepts, then you haven't really meditated, then you can say you don't yet get it.

123
00:15:28,560 --> 00:15:33,560
But at the moment when you're present, just that moment.

124
00:15:33,560 --> 00:15:42,560
In that moment you can feel the difference, suddenly you're powerful, you're strong, you're in fact invincible.

125
00:15:42,560 --> 00:15:48,560
The present moment solves every problem, problems are conceptual.

126
00:15:48,560 --> 00:15:58,560
In the present moment any problem is immediately solved.

127
00:15:58,560 --> 00:16:04,560
For that moment there is no problem because problems are not real.

128
00:16:04,560 --> 00:16:16,560
Now of course this conflicts with all of our conceptual desires and ambitions and relationships and our status and our ego and our personality.

129
00:16:16,560 --> 00:16:24,560
For most of us it's not living in the world, it's not feasible for us to always be in the present moment.

130
00:16:24,560 --> 00:16:36,560
But by no means is that the fault of reality that's our fault for getting caught up, got up in ego and identity and desires and aversion.

131
00:16:36,560 --> 00:16:47,560
I think it caught up in these things, relationships with people, places, things, attachments of versions and so on.

132
00:16:47,560 --> 00:16:57,560
All conceptual, nothing to do with reality.

133
00:16:57,560 --> 00:17:08,560
It kind of makes you want to go off in the forest and just live, be real.

134
00:17:08,560 --> 00:17:16,560
I remember meeting the king of Uganda or something.

135
00:17:16,560 --> 00:17:22,560
In Thailand he was a friend with a prince of Thailand and I was at this very famous royal monastery.

136
00:17:22,560 --> 00:17:32,560
All the royalty came through there and I happened to meet, you know, he came to, I got to tour him around this monastery.

137
00:17:32,560 --> 00:17:36,560
He was a bit of a jerk.

138
00:17:36,560 --> 00:17:41,560
Very Christian and very pro-Christianity.

139
00:17:41,560 --> 00:17:46,560
So he was asking me about the difference of Christianity.

140
00:17:46,560 --> 00:17:52,560
He said, well, do you believe that Jesus is the son of God or something?

141
00:17:52,560 --> 00:17:54,560
Do you know the problem with that?

142
00:17:54,560 --> 00:17:55,560
What do you have a problem with?

143
00:17:55,560 --> 00:18:01,560
Well, we have a problem with the eternal soul, the idea of an eternal soul.

144
00:18:01,560 --> 00:18:03,560
He said, you don't believe in an eternal soul.

145
00:18:03,560 --> 00:18:06,560
I said, we don't really believe in a soul at all.

146
00:18:06,560 --> 00:18:15,560
There's really a bizarre conversation with this king of another country.

147
00:18:15,560 --> 00:18:17,560
I don't remember what my point was.

148
00:18:17,560 --> 00:18:19,560
What was I talking about?

149
00:18:19,560 --> 00:18:21,560
What was I talking about?

150
00:18:21,560 --> 00:18:24,560
Yeah, I know.

151
00:18:24,560 --> 00:18:29,560
What was the point of that?

152
00:18:29,560 --> 00:18:34,560
Are you living in the forest?

153
00:18:34,560 --> 00:18:41,560
I remember I had a point of reason for telling you about this guy.

154
00:18:41,560 --> 00:18:45,560
I got caught up in my story.

155
00:18:45,560 --> 00:18:51,560
It makes me want to go off and live in the forest.

156
00:18:51,560 --> 00:18:57,560
How did I get talking about the king of Rwanda of Uganda?

157
00:18:57,560 --> 00:19:13,560
It'll come to me someday.

158
00:19:13,560 --> 00:19:22,560
Maybe I'll replay this video back and remember what I was talking about.

159
00:19:22,560 --> 00:19:30,560
I was just talking about concepts.

160
00:19:30,560 --> 00:19:32,560
Anyway, stay in the present moment.

161
00:19:32,560 --> 00:19:34,560
That's the deal.

162
00:19:34,560 --> 00:19:37,560
I wonder if it's to be in the present moment.

163
00:19:37,560 --> 00:19:40,560
It solves all your problems.

164
00:19:40,560 --> 00:19:43,560
There's a real power to it.

165
00:19:43,560 --> 00:19:45,560
I think I don't remember.

166
00:19:45,560 --> 00:19:48,560
I'll tell you a good story tomorrow.

167
00:19:48,560 --> 00:19:50,560
Anyway, there you go.

168
00:19:50,560 --> 00:19:52,560
That's the demo for tonight.

169
00:19:52,560 --> 00:19:54,560
That the mind is also impermanent.

170
00:19:54,560 --> 00:19:56,560
Memory is also impermanent.

171
00:19:56,560 --> 00:19:58,560
It just disappears like that.

172
00:19:58,560 --> 00:20:00,560
It's non-self.

173
00:20:00,560 --> 00:20:02,560
Okay, I can draw it.

174
00:20:02,560 --> 00:20:04,560
Okay, thank you all.

175
00:20:04,560 --> 00:20:12,560
Let's the demo for tonight.

176
00:20:12,560 --> 00:20:22,560
I do have something else to talk about.

177
00:20:22,560 --> 00:20:25,560
I'm assuming I don't...

178
00:20:25,560 --> 00:20:26,560
I know some of you.

179
00:20:26,560 --> 00:20:28,560
Some of you, I think I know some of you.

180
00:20:28,560 --> 00:20:33,560
I don't recognize, but maybe just because of your online names.

181
00:20:33,560 --> 00:20:34,560
But here's an idea.

182
00:20:34,560 --> 00:20:38,560
There's Robin here, Robin's here.

183
00:20:38,560 --> 00:20:43,560
I was thinking, I was talking actually with May,

184
00:20:43,560 --> 00:20:47,560
one of the Thai women who helps us out.

185
00:20:47,560 --> 00:20:52,560
And she was going to come here to meditate in early May.

186
00:20:52,560 --> 00:20:55,560
And I said, oh, early May, I'm hoping to go back to early May

187
00:20:55,560 --> 00:20:56,560
is my birthday.

188
00:20:56,560 --> 00:21:01,560
And I was hoping to go back to my place of birth for my birthday.

189
00:21:01,560 --> 00:21:04,560
I'm meaning to go back to man until an island.

190
00:21:04,560 --> 00:21:10,560
Just, I guess, to sort of say that I've been back.

191
00:21:10,560 --> 00:21:15,560
But I thought, well, why not go to where I was born?

192
00:21:15,560 --> 00:21:18,560
And we got talking, but I said, well, why don't we do a...

193
00:21:18,560 --> 00:21:20,560
What if we were to do a meditation?

194
00:21:20,560 --> 00:21:22,560
She said, oh, you should bring some people up with you.

195
00:21:22,560 --> 00:21:25,560
I said, what if we did a meditation course up there?

196
00:21:25,560 --> 00:21:31,560
And I was thinking there are a bunch of really nice resorts up on

197
00:21:31,560 --> 00:21:34,560
Man until an island.

198
00:21:34,560 --> 00:21:37,560
And we could actually just book a resort for a week

199
00:21:37,560 --> 00:21:43,560
and invite people to come and stay at this island resort.

200
00:21:43,560 --> 00:21:48,560
Now, I was looking, and it's actually not quite as cheap as I thought it would be.

201
00:21:48,560 --> 00:21:52,560
But we could talk about it.

202
00:21:52,560 --> 00:21:54,560
And if there's people who would like to go,

203
00:21:54,560 --> 00:21:56,560
man, it's an island's beautiful.

204
00:21:56,560 --> 00:21:57,560
It's really...

205
00:21:57,560 --> 00:21:59,560
In May, it's the best time of year,

206
00:21:59,560 --> 00:22:02,560
because it's not too hot. There's no mosquitoes.

207
00:22:02,560 --> 00:22:10,560
And yet, it's very much in the forest, sort of in nature.

208
00:22:10,560 --> 00:22:14,560
So, you know, if people wanted to book a room at this,

209
00:22:14,560 --> 00:22:17,560
at some resort, we could find the right resort,

210
00:22:17,560 --> 00:22:21,560
and we could just get people to pay their room

211
00:22:21,560 --> 00:22:24,560
if they're talking about people who have money.

212
00:22:24,560 --> 00:22:27,560
I don't know, maybe it's a ridiculous idea.

213
00:22:27,560 --> 00:22:30,560
And there are other ways we could book...

214
00:22:30,560 --> 00:22:35,560
One of my friends on Man at Tillan is looking into it.

215
00:22:35,560 --> 00:22:45,560
Maybe we could book some cheaper space to sort of hold some kind of meditation course.

216
00:22:45,560 --> 00:22:51,560
Anyway, something to think about if anyone's interested in that sort of idea,

217
00:22:51,560 --> 00:22:54,560
coming up to Man at Tillan Island,

218
00:22:54,560 --> 00:22:57,560
and do a real forest retreat in Canada.

219
00:22:57,560 --> 00:23:00,560
Yeah, let me know.

220
00:23:00,560 --> 00:23:07,560
Okay, so we do have some questions.

221
00:23:07,560 --> 00:23:10,560
We'd have to take a van probably.

222
00:23:10,560 --> 00:23:14,560
We'd have to drive ourselves.

223
00:23:14,560 --> 00:23:17,560
Okay, questions from the site.

224
00:23:17,560 --> 00:23:25,560
The question about offering meals and food.

225
00:23:25,560 --> 00:23:27,560
Maybe anyone can go to the food card.

226
00:23:27,560 --> 00:23:29,560
How would you go about adding money to that account?

227
00:23:29,560 --> 00:23:34,560
Well, there's really no need because there's like almost $2,000 on that account still,

228
00:23:34,560 --> 00:23:39,560
so it's already been paid up for the next couple of years.

229
00:23:39,560 --> 00:23:44,560
And I'm getting lots of food from the Sri Lankan community here.

230
00:23:44,560 --> 00:23:51,560
And more Sri Lankan people in the area are signing up to offer food.

231
00:23:51,560 --> 00:23:58,560
But from time to time, I still do use some way Tim Hortons, Peter Pitt, Starbucks card.

232
00:23:58,560 --> 00:24:04,560
The most useful so far has been Starbucks and not so far, but this year,

233
00:24:04,560 --> 00:24:11,560
because Starbucks is just the only convenient location.

234
00:24:11,560 --> 00:24:15,560
Starbucks is like on campus. It's on my way to campus.

235
00:24:15,560 --> 00:24:20,560
And so a Starbucks card is, and it's not for coffee.

236
00:24:20,560 --> 00:24:23,560
I don't drink coffee at Starbucks, but they have oatmeal,

237
00:24:23,560 --> 00:24:26,560
and they have lunch sandwiches.

238
00:24:26,560 --> 00:24:30,560
And they have, they actually have juice as well.

239
00:24:30,560 --> 00:24:32,560
Nice juices in the evening.

240
00:24:32,560 --> 00:24:36,560
They even have salads, but the problem with the salads is they leave them out

241
00:24:36,560 --> 00:24:39,560
and you have to pick them up yourself, which technically isn't allowed.

242
00:24:39,560 --> 00:24:50,560
I'm only allowed to do this, because theoretically they're offering on behalf of whoever is left money with them.

243
00:24:50,560 --> 00:24:54,560
But they don't give everything to you anyway.

244
00:24:54,560 --> 00:24:58,560
But thank you, Cathy, for asking, but it's really fine.

245
00:24:58,560 --> 00:25:06,560
There's lots and lots of support. I'm getting enough food.

246
00:25:06,560 --> 00:25:13,560
Can I move my legs if they fall asleep during meditation and should the head stay in one position?

247
00:25:13,560 --> 00:25:18,560
You don't have to, because if they fall asleep, you just be mindful of it and keep sitting.

248
00:25:18,560 --> 00:25:31,560
But if you need to move more for pain, pain would be something that would really cause you to want to move.

249
00:25:31,560 --> 00:25:36,560
Then you would just say to yourself wanting to move, wanting to move, moving, moving.

250
00:25:36,560 --> 00:25:39,560
And the head is the head the same if you want to move the head.

251
00:25:39,560 --> 00:25:52,560
If it does move, you can just raise it back up and tending to raise, raising, raising.

252
00:25:52,560 --> 00:25:55,560
Does the approach of what can this meditation give me?

253
00:25:55,560 --> 00:25:58,560
Will it make me happy prevent progress?

254
00:25:58,560 --> 00:26:11,560
I mean, it can. Yeah, it can certainly become a hindrance when you're worried about that or when you're doubting or unsure.

255
00:26:11,560 --> 00:26:18,560
How can one ensure equanimity simply knowing that disliking frustration is not necessary to try to unblexurize it,

256
00:26:18,560 --> 00:26:22,560
trying to replace my wrong attitude, to write attitude?

257
00:26:22,560 --> 00:26:28,560
No, write attitude has to come from wisdom, come from understanding.

258
00:26:28,560 --> 00:26:33,560
You can't artificially create or intellectually create, write attitude.

259
00:26:33,560 --> 00:26:36,560
It has to come from seeing things clearly as they are.

260
00:26:36,560 --> 00:26:43,560
Once you see things see reality clearly as it is, the write attitude falls.

261
00:26:43,560 --> 00:26:48,560
Excuse me.

262
00:26:48,560 --> 00:26:55,560
My question is on free will and non-self. With observation, I see whatever I do is because the things are rising into my mind.

263
00:26:55,560 --> 00:26:59,560
My legs and dislikes and judge it based on knowledge and dumbness.

264
00:26:59,560 --> 00:27:09,560
Everyone incline my mind to focus on one direction or the other.

265
00:27:09,560 --> 00:27:17,560
It's based on these things are happening. The only thing that I can do is observe

266
00:27:17,560 --> 00:27:21,560
I don't get it.

267
00:27:21,560 --> 00:27:25,560
I see whatever I do is because the things are rising in my mind.

268
00:27:25,560 --> 00:27:30,560
Even when I incline my mind to focus on some factors, these things are happening.

269
00:27:30,560 --> 00:27:33,560
The only thing I can do is observe.

270
00:27:33,560 --> 00:27:36,560
There is no good view about free will.

271
00:27:36,560 --> 00:27:46,560
If you're worried about free will, you should say worried or speculating or confused or doubting or so on, just let it go.

272
00:27:46,560 --> 00:27:59,560
Yeah, I think you're overthinking. You know what I'm going to say already.

273
00:27:59,560 --> 00:28:02,560
I mean, it's fine. It's normal and meditation to think.

274
00:28:02,560 --> 00:28:07,560
Just remind yourself thinking, thinking.

275
00:28:07,560 --> 00:28:20,560
The present moment. Do we have some questions over here? You're copying them in.

276
00:28:20,560 --> 00:28:27,560
If you want to support the monastery, that's always good.

277
00:28:27,560 --> 00:28:32,560
You don't have to give me food.

278
00:28:32,560 --> 00:28:41,560
But do you think King King Bimbisara wouldn't have punished criminals since he was a Sotapana?

279
00:28:41,560 --> 00:28:48,560
Well, yeah, King Bimbisara would never have had anyone killed, of course.

280
00:28:48,560 --> 00:28:53,560
It doesn't mean he wouldn't. No, he was in the cases.

281
00:28:53,560 --> 00:28:58,560
Actually, I'm not sure. But no, certainly he was a great king.

282
00:28:58,560 --> 00:29:08,560
No booking. He would never have punished. Not in that way.

283
00:29:08,560 --> 00:29:13,560
Fortunately, his reign was cut short by his son.

284
00:29:13,560 --> 00:29:15,560
But yeah, during the time that he...

285
00:29:15,560 --> 00:29:19,560
I mean, he wasn't always a Buddhist. He didn't originally.

286
00:29:19,560 --> 00:29:23,560
He was a king for many years before he met the Buddha.

287
00:29:23,560 --> 00:29:28,560
But once he met the Buddha and then became a Sotapana, from that time on you,

288
00:29:28,560 --> 00:29:32,560
would have never done anything cruel.

289
00:29:32,560 --> 00:29:38,560
Not overly cruel. You know, Sotapana can still be mean, but not...

290
00:29:38,560 --> 00:29:43,560
Not, you know, reason. Not...

291
00:29:43,560 --> 00:29:52,560
Not mean in the way most of us think, but they could still get angry and be somehow cruel to people.

292
00:29:52,560 --> 00:29:58,560
And they wouldn't torture them. Certainly not cause them to be put to death.

293
00:30:23,560 --> 00:30:29,560
I remember what it was. So this king of Uganda,

294
00:30:29,560 --> 00:30:35,560
he had the last minute after I've told him this thing about the soul,

295
00:30:35,560 --> 00:30:39,560
and he's not really impressed by my whole philosophy.

296
00:30:39,560 --> 00:30:41,560
He says to me in this other monk,

297
00:30:41,560 --> 00:30:42,560
and I've got a picture right before it.

298
00:30:42,560 --> 00:30:44,560
They took a picture of us.

299
00:30:44,560 --> 00:30:46,560
I've still got this picture right before he said it.

300
00:30:46,560 --> 00:30:48,560
Then he turns to me and he says,

301
00:30:48,560 --> 00:30:53,560
Well, enjoy your vacation in the most snide way possible.

302
00:30:53,560 --> 00:30:56,560
He's just totally condescending.

303
00:30:56,560 --> 00:30:59,560
And so the point was,

304
00:30:59,560 --> 00:31:05,560
people think of going off into the forest as a sort of vacation.

305
00:31:05,560 --> 00:31:08,560
It means of escaping your problems.

306
00:31:08,560 --> 00:31:13,560
And the thought was that people say it's about escaping real life.

307
00:31:13,560 --> 00:31:18,560
Right? It's a vacation and real life.

308
00:31:18,560 --> 00:31:19,560
People often ask me,

309
00:31:19,560 --> 00:31:23,560
well, how can I incorporate meditation to real life?

310
00:31:23,560 --> 00:31:26,560
Which is kind of absurd from our point of view.

311
00:31:26,560 --> 00:31:30,560
We go off into the forest in order to live real life.

312
00:31:30,560 --> 00:31:38,560
The life in society is totally fake and contrived.

313
00:31:38,560 --> 00:31:40,560
So there you go.

314
00:31:40,560 --> 00:31:45,560
That was how the talk was supposed to end.

315
00:31:45,560 --> 00:32:10,560
Thank you all. Have a good night.

