1
00:00:00,000 --> 00:00:12,000
Anisha, Vatasankara, Upa, Dvaya, Dhammino, Upa, Jidpa, Niru, Janti, Desangu, Bhasamosukha,

2
00:00:12,000 --> 00:00:19,500
impermanent indeed are all formations of a nature to arise and pass away.

3
00:00:19,500 --> 00:00:22,500
Having arisen, they all cease.

4
00:00:22,500 --> 00:00:25,300
Their stealing is happiness.

5
00:00:25,300 --> 00:00:36,200
Achirang Watayankayo, Bhatawing, Adhesay Sati, Chudho, Appetawinyano, Niratangwakalinkaran.

6
00:00:36,200 --> 00:00:47,200
Before long, indeed, this body will lie on the earth, discarded, left by the mind, as useless as a wooden log.

