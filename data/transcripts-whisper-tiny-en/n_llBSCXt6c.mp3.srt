1
00:00:00,000 --> 00:00:20,240
Hello, today I'll be talking about one of the most crucial aspects of the Buddha's teaching.

2
00:00:20,240 --> 00:00:32,280
This is the quality of the Buddha's teaching that is one of cause and effect that the teaching

3
00:00:32,280 --> 00:00:42,080
of the Buddha has the quality of being true to the nature of how things arise and how things

4
00:00:42,080 --> 00:00:50,760
cease, that everything that arises comes from a cause, and the teachings of the Buddha

5
00:00:50,760 --> 00:00:58,440
are able to describe the actual cause that gives rise to the state of affairs that we find

6
00:00:58,440 --> 00:01:10,240
ourselves in, and also the effects that come from the way that we respond and interact

7
00:01:10,240 --> 00:01:12,200
with the world around us.

8
00:01:12,200 --> 00:01:21,920
So our ability to understand why it is that we are the way we are, and what consequences

9
00:01:21,920 --> 00:01:26,880
will come from our actions and from our state of mind.

10
00:01:26,880 --> 00:01:34,240
This is the Buddha's teaching on dependent origination, or Patitya Samupada, which says

11
00:01:34,240 --> 00:01:44,520
that all of our happiness and suffering, all of the difficulties and upsets, stress and

12
00:01:44,520 --> 00:01:50,800
dissatisfaction that we meet with in the world, as well as all of the peace and happiness

13
00:01:50,800 --> 00:02:00,000
and well-being that comes, has a cause, and the teaching of the Buddha is able to describe

14
00:02:00,000 --> 00:02:09,280
this cause, the teaching of dependent origination is the perfect method for us to understand

15
00:02:09,280 --> 00:02:17,160
and to thereby adjust and overcome the causes of suffering and thereby overcome all of

16
00:02:17,160 --> 00:02:26,760
suffering, any type of suffering and stress that might come in the future.

17
00:02:26,760 --> 00:02:35,760
The teaching of dependent origination in the Buddha is best seen as a practical teaching.

18
00:02:35,760 --> 00:02:40,920
Often when people approach the teachings of the Buddha, they will, as I've said before,

19
00:02:40,920 --> 00:02:46,680
look at it from an intellectual point of view, trying to understand it logically to think

20
00:02:46,680 --> 00:02:56,720
of examples by which they can understand and accept and agree with the teachings

21
00:02:56,720 --> 00:03:03,160
as they're presented, ways of explaining and ways of assimilating it with their own view

22
00:03:03,160 --> 00:03:05,720
and their own understanding of reality.

23
00:03:05,720 --> 00:03:15,240
But this is not really the most useful beneficial and proper way to approach the Buddha's

24
00:03:15,240 --> 00:03:19,400
teaching, most especially the Buddha's teaching on cause and effect, because this teaching

25
00:03:19,400 --> 00:03:25,760
is something that can be seen, can be understood based on our experience of reality,

26
00:03:25,760 --> 00:03:29,880
most especially our experience in the meditation practice.

27
00:03:29,880 --> 00:03:37,120
So when we undertake to practice meditation or in our daily lives, when we encounter difficulties

28
00:03:37,120 --> 00:03:42,800
in problems, when we encounter situations that give rise to one things and desires or

29
00:03:42,800 --> 00:03:54,720
aversions and dislikes that give rise to our conceit and detachment and delusions, jealousy and

30
00:03:54,720 --> 00:04:01,400
being so on, that we are able to see this process in action.

31
00:04:01,400 --> 00:04:10,160
We should be able to see the nature of reality in terms of cause and effect that when this

32
00:04:10,160 --> 00:04:12,880
arises, that will follow.

33
00:04:12,880 --> 00:04:20,200
When this doesn't arise, if you're able to somehow give up the cause, give up the

34
00:04:20,200 --> 00:04:25,880
behavior that is causing the problem, then the problem has no possible way of arising.

35
00:04:25,880 --> 00:04:32,280
So it says that reality is really functions both physically and mentally on a scientific

36
00:04:32,280 --> 00:04:42,640
basis, that nothing arises by chance or by magic or by any supernatural means, that

37
00:04:42,640 --> 00:04:50,160
there is no possible way for any phenomenon to arise without its cause, without the appropriate

38
00:04:50,160 --> 00:04:51,800
cause.

39
00:04:51,800 --> 00:04:58,640
And so in this way, once we understand the causes, we understand the relationship between

40
00:04:58,640 --> 00:05:05,600
phenomena, between suffering and unwholesome states and between happiness and wholesome

41
00:05:05,600 --> 00:05:10,240
states, between those states that lead to suffering and the suffering and between those

42
00:05:10,240 --> 00:05:17,480
states that lead to happiness and the happiness, then our mind will naturally incline

43
00:05:17,480 --> 00:05:23,560
towards the development of those deans and those mind states that lead to happiness, because

44
00:05:23,560 --> 00:05:26,120
there's no one in the world that wants to suffer.

45
00:05:26,120 --> 00:05:32,200
The problem is not that we want to hurt ourselves, intrinsically all beings are always looking

46
00:05:32,200 --> 00:05:39,560
for that which is pleasant or that which is peaceful, that which is true happiness.

47
00:05:39,560 --> 00:05:44,920
The problem is that we don't understand the nature of cause and effect, we do certain things

48
00:05:44,920 --> 00:05:51,320
and we create certain mind states thinking that this is going to somehow lead to our benefit

49
00:05:51,320 --> 00:05:56,560
when in fact, those things that we create, those states of mind and those actions are

50
00:05:56,560 --> 00:06:00,160
only for our detriment, our cause for our suffering.

51
00:06:00,160 --> 00:06:09,240
And so in simply by not understanding, by this ignorance and delusion, we create states

52
00:06:09,240 --> 00:06:16,360
that are contradictory to our purpose, where we want to be happy and we're actually

53
00:06:16,360 --> 00:06:18,880
causing ourselves suffering.

54
00:06:18,880 --> 00:06:22,680
And this is how the Buddha's teaching on dependent origination starts.

55
00:06:22,680 --> 00:06:28,400
The first part of the teaching is that ignorance creates formations.

56
00:06:28,400 --> 00:06:35,760
It is due to ignorance that our mind gives rise to its mental formations or gives rise

57
00:06:35,760 --> 00:06:43,040
to concepts, it conceives things, it gives rise to ideas and all sorts of intentions and

58
00:06:43,040 --> 00:06:52,000
volitions and the very root of karma of all of our ethical action, both ethical and unethical,

59
00:06:52,000 --> 00:06:53,720
simply due to our ignorance.

60
00:06:53,720 --> 00:06:59,680
If we understood that these formations, these conceptions, these intentions, we're going to

61
00:06:59,680 --> 00:07:02,800
cause our suffering, we wouldn't give rise to them.

62
00:07:02,800 --> 00:07:10,440
If we didn't have our ignorance, if we understood that there was no way to find happiness

63
00:07:10,440 --> 00:07:19,240
in this way, that even creating stable lifestyles where we have a nice car and a house

64
00:07:19,240 --> 00:07:23,800
and a job and all these things, if we understood that this wasn't able to bring us happiness,

65
00:07:23,800 --> 00:07:26,560
even these good things, then we wouldn't strive for them.

66
00:07:26,560 --> 00:07:31,680
The intention wouldn't arise to find these things, to cling, to attach even to people

67
00:07:31,680 --> 00:07:33,960
or to places or to things.

68
00:07:33,960 --> 00:07:36,840
If we understood reality, we wouldn't cling to these things.

69
00:07:36,840 --> 00:07:41,320
We wouldn't even give rise to those ethical acts that were for the benefit of giving

70
00:07:41,320 --> 00:07:50,080
or creating pleasant circumstances where we had good friends and good food and good society

71
00:07:50,080 --> 00:07:51,080
and so on.

72
00:07:51,080 --> 00:07:55,000
We wouldn't even think to give rise to those intentions.

73
00:07:55,000 --> 00:08:00,480
We would be content and at peace with ourselves, we would have no need for anything.

74
00:08:00,480 --> 00:08:10,040
We wouldn't strive if we understood that striving, this giving rise to intention or the

75
00:08:10,040 --> 00:08:16,880
desire to create, the desire to destroy, the desire to change, to force, to build up some

76
00:08:16,880 --> 00:08:23,880
concept of me, of identity in the world.

77
00:08:23,880 --> 00:08:30,200
If we understood that this was suffering, we wouldn't give rise to it.

78
00:08:30,200 --> 00:08:35,760
This is incredibly powerful teaching because normally we would think that without this

79
00:08:35,760 --> 00:08:41,960
intention, nothing happens, without this intention, no good can come from your life.

80
00:08:41,960 --> 00:08:47,960
Really the ultimate truth of reality is that it is what it is.

81
00:08:47,960 --> 00:08:54,000
It arises and its reality comes and goes and there is no one thing in the world that can

82
00:08:54,000 --> 00:08:56,120
truly make you happy in that peace.

83
00:08:56,120 --> 00:08:59,440
There's nothing that you can create that won't be destroyed.

84
00:08:59,440 --> 00:09:02,240
There's nothing that you can build up that won't fall apart.

85
00:09:02,240 --> 00:09:05,800
There's nothing that can truly make you happy or satisfied.

86
00:09:05,800 --> 00:09:12,960
If you can't find happiness in peace as you are, as things are in all of reality, then

87
00:09:12,960 --> 00:09:19,440
you'll inevitably fall into suffering and disappointment when things change and inevitably

88
00:09:19,440 --> 00:09:24,120
go against your wishes.

89
00:09:24,120 --> 00:09:29,680
And once we give rise to understanding, if we understand reality, the point is that we will

90
00:09:29,680 --> 00:09:34,920
never have any wishes or any hopes or any desires because we're truly happy.

91
00:09:34,920 --> 00:09:36,360
We will never want for anything.

92
00:09:36,360 --> 00:09:42,320
We will never hope for anything or wish for anything because we're content the way things

93
00:09:42,320 --> 00:09:43,320
are.

94
00:09:43,320 --> 00:09:45,280
We're happy with reality as it is.

95
00:09:45,280 --> 00:09:51,640
And this is really the key not to create anything that is inevitably going to fall apart

96
00:09:51,640 --> 00:09:59,160
or disappear, but to be content with whatever comes, whatever should arise.

97
00:09:59,160 --> 00:10:05,560
So this is the key principle in Buddhism that understanding sets you free.

98
00:10:05,560 --> 00:10:11,280
It's not attaining anything, it's not bowing for anything, it's not creating anything.

99
00:10:11,280 --> 00:10:14,840
Simply understanding things as they are will set you free.

100
00:10:14,840 --> 00:10:23,160
And this is why insight meditation or meditation where one contemplates reality is so crucial.

101
00:10:23,160 --> 00:10:28,040
Simply by watching reality, you change your whole way of looking and you change your whole

102
00:10:28,040 --> 00:10:32,560
way of being and you change your whole universe so that nothing can bring you suffering.

103
00:10:32,560 --> 00:10:33,960
This is the key.

104
00:10:33,960 --> 00:10:38,280
Many people begin to practice meditation thinking that they're going to attain or create

105
00:10:38,280 --> 00:10:45,520
or perceive or experience something special that is not going to fall apart, that is not

106
00:10:45,520 --> 00:10:50,920
going to disappear, that is somehow going to make them satisfied.

107
00:10:50,920 --> 00:10:55,440
And as a result, they're dissatisfied with the meditation when they realize that there's

108
00:10:55,440 --> 00:10:58,160
nothing that is going to satisfy them.

109
00:10:58,160 --> 00:11:04,240
And this is an important point for us to understand that the meditation is not for building

110
00:11:04,240 --> 00:11:10,960
up, it's for letting go, for giving up and for accepting and being at peace and at harmony

111
00:11:10,960 --> 00:11:14,880
with things as they are, just simply understanding.

112
00:11:14,880 --> 00:11:19,720
So when we practice meditation, say watching the breath, the stomach when it rises and falls

113
00:11:19,720 --> 00:11:24,360
or watching our feet move when we walk or watching any part of reality, simply being

114
00:11:24,360 --> 00:11:30,480
aware that we're standing or sitting, being aware of pains and aches in the body, as I've

115
00:11:30,480 --> 00:11:38,240
said before, when you feel a pain, say, pain, pain, simply seeing it for what it is.

116
00:11:38,240 --> 00:11:42,040
You accomplish the goal of the Buddha's teaching.

117
00:11:42,040 --> 00:11:45,600
You don't have to create anything, you don't have to change anything, you don't have

118
00:11:45,600 --> 00:11:48,680
to get rid of the pains and the aches, you don't have to get rid of the thoughts and

119
00:11:48,680 --> 00:11:49,680
the mind.

120
00:11:49,680 --> 00:11:52,760
But simply saying to yourself, it is what it is.

121
00:11:52,760 --> 00:11:58,520
This is pain, this is thought, this is emotion, this is movements of the body.

122
00:11:58,520 --> 00:12:03,680
When we're walking, saying walking, walking, there's all so on, simply reminding yourself

123
00:12:03,680 --> 00:12:10,120
and creating this clear awareness of the phenomenon as it is, is what it freezes from

124
00:12:10,120 --> 00:12:11,120
suffering.

125
00:12:11,120 --> 00:12:16,920
So this is the core of the Buddha's teaching on dependent origination that ignorance leads

126
00:12:16,920 --> 00:12:18,320
to formations.

127
00:12:18,320 --> 00:12:23,280
It's actually a good summary of the whole of the dependent origination, because that's

128
00:12:23,280 --> 00:12:24,600
really how it works.

129
00:12:24,600 --> 00:12:29,480
Our ignorance gives rise to formation and those formations in turn, we're ignorant about

130
00:12:29,480 --> 00:12:33,520
them thinking that somehow they're going to satisfy us, and so we give rise to more and

131
00:12:33,520 --> 00:12:36,520
more of these formations again and again.

132
00:12:36,520 --> 00:12:44,040
But the next part of the cycle goes into great detail about how this works, because

133
00:12:44,040 --> 00:12:50,240
those formations are what give rise to our lives, they give rise to our birth, our becoming.

134
00:12:50,240 --> 00:12:57,040
It's based on this, that when we pass away from one life, we create a new life, we cling

135
00:12:57,040 --> 00:13:04,200
and we develop this existence that we see in front of us, that we have a brain and we have

136
00:13:04,200 --> 00:13:07,120
a body where we have a world around us.

137
00:13:07,120 --> 00:13:11,920
We create this again and again and again, and this gives rise to consciousness.

138
00:13:11,920 --> 00:13:19,360
So the Buddha said, these formations give rise to our conscious awareness in this life.

139
00:13:19,360 --> 00:13:22,480
This is the next, the next link.

140
00:13:22,480 --> 00:13:28,440
So when we're first born, there arises a consciousness, say, in the womb of our mother.

141
00:13:28,440 --> 00:13:37,120
And from that moment on, we will see the cause and effect arising.

142
00:13:37,120 --> 00:13:40,240
This is the detailed exposition.

143
00:13:40,240 --> 00:13:45,040
So the first part is just talking about life after life, after life where ignorance gives

144
00:13:45,040 --> 00:13:47,840
rise to our intentions to do this or that.

145
00:13:47,840 --> 00:13:53,320
Now when you get into a single life, then we start with the first moment that has been

146
00:13:53,320 --> 00:13:56,200
created, this consciousness.

147
00:13:56,200 --> 00:14:01,040
And through our whole life, it's going to be this consciousness that is most important.

148
00:14:01,040 --> 00:14:07,480
So in the Buddha's teaching, the mind is the most important aspect.

149
00:14:07,480 --> 00:14:14,600
And I think it's easy to understand based on talking about how our misunderstandings are

150
00:14:14,600 --> 00:14:15,760
what creates suffering.

151
00:14:15,760 --> 00:14:21,400
So when we understand how ignorance creates our intentions, we can understand how important

152
00:14:21,400 --> 00:14:22,400
the mind is.

153
00:14:22,400 --> 00:14:23,800
So this is the beginning.

154
00:14:23,800 --> 00:14:24,800
How does it work?

155
00:14:24,800 --> 00:14:29,800
The mind, the consciousness, gives rise to our experience of reality.

156
00:14:29,800 --> 00:14:32,920
We experience the physical and we experience the mental.

157
00:14:32,920 --> 00:14:41,680
So it gives rise to this psychophysical or mental physical reality, for instance, the experience

158
00:14:41,680 --> 00:14:47,080
of the stomach when we're breathing, when the stomach rises and the stomach falls.

159
00:14:47,080 --> 00:14:48,680
There's the physical and the mental.

160
00:14:48,680 --> 00:14:53,120
The rising is the physical and the knowing of the rising is the mental.

161
00:14:53,120 --> 00:14:55,760
When we walk, there's the foot moving is the physical.

162
00:14:55,760 --> 00:14:57,480
The mind knowing it is the mental.

163
00:14:57,480 --> 00:15:03,360
When we feel pain, there's the physical experience and there's the mind that knows it

164
00:15:03,360 --> 00:15:10,520
and doesn't like it and so on and says that's painful and so on.

165
00:15:10,520 --> 00:15:17,400
The whole of our lives, thus circle around in these with these two realities, the mind

166
00:15:17,400 --> 00:15:23,520
and the experience of the objects around us.

167
00:15:23,520 --> 00:15:27,880
Now this in and of itself isn't really a problem, obviously we have to experience, we

168
00:15:27,880 --> 00:15:30,920
have to live our lives and there's no suffering that comes from this.

169
00:15:30,920 --> 00:15:34,120
The suffering doesn't come from the things that we experience.

170
00:15:34,120 --> 00:15:37,560
The suffering, as I said, comes from our misunderstanding.

171
00:15:37,560 --> 00:15:45,880
Once we have the psychophysical matrix, the physical and the mental aspects of reality,

172
00:15:45,880 --> 00:15:51,200
the experiences of the body and the mind, then we get the six senses.

173
00:15:51,200 --> 00:15:55,960
The next link is once you have the physical and the mental, then you're going to have

174
00:15:55,960 --> 00:15:56,960
sensations.

175
00:15:56,960 --> 00:16:02,080
You're going to have the seeing, the hearing, smelling, tasting, feeling, thinking.

176
00:16:02,080 --> 00:16:06,040
This is where the physical and the mental arise.

177
00:16:06,040 --> 00:16:11,800
The eye itself is physical, the light touching the eye is physical.

178
00:16:11,800 --> 00:16:20,240
The mind that knows the seeing is mental, hearing the sound in the ear or physical and the

179
00:16:20,240 --> 00:16:25,280
knowing of the sound or the hearing is mental and so on.

180
00:16:25,280 --> 00:16:29,160
The six senses are our experience of reality.

181
00:16:29,160 --> 00:16:32,680
Here we have three, we have the consciousness, which is aware of the physical and the

182
00:16:32,680 --> 00:16:35,920
mental realities at the six senses.

183
00:16:35,920 --> 00:16:41,760
So knowing the mental is in this case, knowing the mind, so the mind is aware of the

184
00:16:41,760 --> 00:16:42,760
thoughts.

185
00:16:42,760 --> 00:16:51,760
It's aware of the aspects of the mind, the formations that arise in the mind.

186
00:16:51,760 --> 00:16:56,400
Once you have the six senses, then you have contact.

187
00:16:56,400 --> 00:17:03,600
These four together make up the neutral aspects, sorry, these four along with the next

188
00:17:03,600 --> 00:17:10,360
one, which is feeling or sensation.

189
00:17:10,360 --> 00:17:16,680
Once you have consciousness, then you will have the mental and physical experience

190
00:17:16,680 --> 00:17:24,200
at the six senses because of the contact, the contact between the mind and the physical.

191
00:17:24,200 --> 00:17:28,280
When the mind goes out to the body, when the mind goes out to the eye, the ear, the nose,

192
00:17:28,280 --> 00:17:33,880
the tongue, or when the mind goes out to the thoughts, then there is contact.

193
00:17:33,880 --> 00:17:39,920
Once there is contact, then there arises sensation, there arises pleasant sensation, an unpleasant

194
00:17:39,920 --> 00:17:43,160
sensation or a neutral sensation.

195
00:17:43,160 --> 00:17:48,280
These five aspects of reality are the neutral aspect of reality.

196
00:17:48,280 --> 00:17:52,160
Our whole life would be fine if this is all we had.

197
00:17:52,160 --> 00:18:03,600
And thus these five are the most important aspects of reality for us to examine and to analyze.

198
00:18:03,600 --> 00:18:08,160
Once we come to understand these clearly, to see them for what they are and to not go

199
00:18:08,160 --> 00:18:11,520
the next step, which is going to get us into trouble.

200
00:18:11,520 --> 00:18:15,400
Simply knowing when we feel pain, knowing when we feel happy, knowing when we see, knowing

201
00:18:15,400 --> 00:18:18,400
when we hear, when we see something, we know it as seeing.

202
00:18:18,400 --> 00:18:24,560
When we hear something, we know it as hearing, we say to ourselves, hearing, hearing.

203
00:18:24,560 --> 00:18:28,680
When we feel pain, we know it as pain, we say to ourselves, pain, pain.

204
00:18:28,680 --> 00:18:34,360
Just reminding ourselves, it's just simply pain without attaching to it.

205
00:18:34,360 --> 00:18:37,120
No problem would arise for us if we were able to do this.

206
00:18:37,120 --> 00:18:42,560
The problem is that once we, if we're unable to understand reality in this way, we're

207
00:18:42,560 --> 00:18:44,800
going to give rise to craving.

208
00:18:44,800 --> 00:18:49,480
Because of the ignorance that exists in our minds, our inability to see these things as

209
00:18:49,480 --> 00:18:56,680
they are and to see them simply as arising and ceasing reality, then we're going to

210
00:18:56,680 --> 00:18:58,440
give rise to craving.

211
00:18:58,440 --> 00:19:00,680
We will have this intention arise.

212
00:19:00,680 --> 00:19:06,840
When we see something, we'll conceive of it as good or we'll conceive of it as bad.

213
00:19:06,840 --> 00:19:10,800
We will have some kind of desire, some kind of intention arise.

214
00:19:10,800 --> 00:19:18,880
When we hear sounds, we will recognize it as a pleasant sound or an unpleasant sound,

215
00:19:18,880 --> 00:19:23,560
some a person that we like, if it's a music we will enjoy it and so on.

216
00:19:23,560 --> 00:19:26,880
We'll give rise to some intention in regards to it.

217
00:19:26,880 --> 00:19:30,000
This is how all of addiction works.

218
00:19:30,000 --> 00:19:37,480
For people who suffer from addiction to substances or addiction to any kind of stimulus,

219
00:19:37,480 --> 00:19:44,480
you'll find that the mind goes back and forth between these different parts of reality,

220
00:19:44,480 --> 00:19:47,600
the cause and effect.

221
00:19:47,600 --> 00:19:52,440
Sometimes you'll be aware of the seeing, sometimes you'll be aware of the craving, sometimes

222
00:19:52,440 --> 00:19:57,720
you'll be aware of the mind and knowing of it and the experience of it.

223
00:19:57,720 --> 00:20:01,680
When you practice meditation, you'll be able to break this into pieces.

224
00:20:01,680 --> 00:20:07,640
The mind goes through these again and again and again and it creates more and more craving.

225
00:20:07,640 --> 00:20:10,240
When you're able to understand them, you're able to break them up.

226
00:20:10,240 --> 00:20:14,160
When you see something, it's simply seeing, it's neither good nor bad because there's

227
00:20:14,160 --> 00:20:18,200
nothing intrinsically good or bad about seeing after all.

228
00:20:18,200 --> 00:20:21,560
It's simply because of our misunderstanding, our ignorance.

229
00:20:21,560 --> 00:20:31,360
Because of our past habits, our habitual ways of experience, experiencing things, remembering

230
00:20:31,360 --> 00:20:36,560
that certain sites are going to be a cause for pleasure, remembering that certain sounds

231
00:20:36,560 --> 00:20:41,040
are a cause for pleasure and so on and remembering that certain sites and sounds and experiences

232
00:20:41,040 --> 00:20:42,640
are a cause for displeasure.

233
00:20:42,640 --> 00:20:48,720
We'll give rise to our craving, our wanting to develop, to cultivate, to increase, to attain,

234
00:20:48,720 --> 00:20:55,400
to obtain the phenomenon and more of the phenomenon and our aversion, our desire to be

235
00:20:55,400 --> 00:21:03,880
free, to remove, to destroy, to banish the phenomenon in the case of those things that

236
00:21:03,880 --> 00:21:12,240
habitually we remember as bringing suffering, bringing displeasure or bringing pain.

237
00:21:12,240 --> 00:21:19,400
Because of our conception, conceiving things as more than simply what they are, for instance,

238
00:21:19,400 --> 00:21:23,600
when we hear someone's voice, right away we're going to put a value judgment on it, whether

239
00:21:23,600 --> 00:21:27,200
they're a good person or a bad person, we like them or we don't like them, a friend or

240
00:21:27,200 --> 00:21:28,200
an enemy.

241
00:21:28,200 --> 00:21:32,000
When we see someone, we're right away going to get angry and upset because we don't

242
00:21:32,000 --> 00:21:36,160
like them or we're going to become attracted and pleased because we do like them maybe

243
00:21:36,160 --> 00:21:37,760
they're beautiful or so on.

244
00:21:37,760 --> 00:21:43,040
We're going to give rise to these cravings and this is what's going to get us into trouble

245
00:21:43,040 --> 00:21:45,360
because this is how addiction works.

246
00:21:45,360 --> 00:21:50,160
If addiction didn't cause trouble, people wouldn't have to look for a way out of it.

247
00:21:50,160 --> 00:21:57,160
But the truth of reality is that our cravings lead us to a cycle of addiction that is very

248
00:21:57,160 --> 00:22:02,560
difficult and more and more difficult the longer it lasts to break free from.

249
00:22:02,560 --> 00:22:07,800
So this is the next link that craving wouldn't be a problem if it didn't lead to the

250
00:22:07,800 --> 00:22:13,040
next cycle and that is addiction.

251
00:22:13,040 --> 00:22:17,200
Most people who haven't studied the Buddhist teaching, whether they be Buddhists or non-

252
00:22:17,200 --> 00:22:20,000
Buddhists, don't see the danger inherent in craving.

253
00:22:20,000 --> 00:22:22,560
We don't see the inherent danger in liking.

254
00:22:22,560 --> 00:22:27,200
We think that our likes and dislikes are what make us who we are.

255
00:22:27,200 --> 00:22:32,520
And this is exactly the problem because we have some conception of self, of eye, of what

256
00:22:32,520 --> 00:22:36,440
eye like and dislike.

257
00:22:36,440 --> 00:22:45,040
We stumble right over this egregious error of judgment that actually there is no eye

258
00:22:45,040 --> 00:22:51,280
that we're creating this as we go along, we'll say this, it's our way of justifying liking

259
00:22:51,280 --> 00:22:52,280
and disliking.

260
00:22:52,280 --> 00:22:56,640
We say, I like this, I like that, and we say this as a justification.

261
00:22:56,640 --> 00:23:03,440
It's the connection, if you will, between our craving and our addiction.

262
00:23:03,440 --> 00:23:11,560
At the time when we crave something, we think, yes, that's something I like.

263
00:23:11,560 --> 00:23:19,800
This is my preference, then we'll give rise to this habitual addiction where we cling

264
00:23:19,800 --> 00:23:25,040
to the object, we're unable to let go of it, we're unable to be without it.

265
00:23:25,040 --> 00:23:30,040
To the extent we'll actually cause great suffering for ourselves if we don't get it.

266
00:23:30,040 --> 00:23:38,360
And this is the danger inherent in simple liking because we are not static creatures,

267
00:23:38,360 --> 00:23:45,480
we are dynamic and everything we do and think affects who we are, changes who we are.

268
00:23:45,480 --> 00:23:51,280
Craving gives rise to clinging, you can't stop it simply by force of will, you can't simply

269
00:23:51,280 --> 00:23:56,440
wish for it to stay simply as liking, where liking will not give rise to craving.

270
00:23:56,440 --> 00:24:06,120
Liking has, as it's nature, the propensity to give rise to clinging, clinging gives rise

271
00:24:06,120 --> 00:24:14,600
to creating or becoming, creating really, creating circumstances where we can get what we want,

272
00:24:14,600 --> 00:24:17,600
cultivating and developing and building.

273
00:24:17,600 --> 00:24:26,080
Building up, in many cases, a huge ego or identification with reality, where we have who

274
00:24:26,080 --> 00:24:34,520
I am, my status in life, my stable reality, my home, my car, my family, and so on.

275
00:24:34,520 --> 00:24:38,200
We build this up based on our cravings, based on our clings.

276
00:24:38,200 --> 00:24:44,320
We will build up our whole reality, why people, why beings are so diversified, why some

277
00:24:44,320 --> 00:24:49,920
people are rich, some people are poor, is totally based on where we have led ourselves

278
00:24:49,920 --> 00:24:58,280
from lifetime after lifetime, where we've directed our minds and what we've clung to,

279
00:24:58,280 --> 00:25:03,160
what we've associated and identified with.

280
00:25:03,160 --> 00:25:11,600
And it's because of this becoming that we have all of our suffering, all of our dissatisfaction,

281
00:25:11,600 --> 00:25:19,480
because it's this creating, this conceiving, this seeking after that gives rise to old

282
00:25:19,480 --> 00:25:26,920
age sickness and death, that gives rise to this form-formed existence of being a human,

283
00:25:26,920 --> 00:25:35,440
of being an animal, of being any type of creature that we actually create for ourselves,

284
00:25:35,440 --> 00:25:40,800
that gives rise to our sicknesses, gives rise to our pains, gives rise to our conflicts

285
00:25:40,800 --> 00:25:48,160
and our sufferings, the war and famine and poverty and so on, that exists in the world.

286
00:25:48,160 --> 00:26:02,000
It's all created simply by our erroneous identification and the idea that somehow we can

287
00:26:02,000 --> 00:26:04,960
find true happiness in this way.

288
00:26:04,960 --> 00:26:10,320
This is the core of the Buddhist teaching, it's really what the Buddha realized on the

289
00:26:10,320 --> 00:26:16,160
night that he became enlightened, that reality does work in terms of cause and effect.

290
00:26:16,160 --> 00:26:22,560
It wasn't a theoretical realization, he saw reality working, he saw that it's because

291
00:26:22,560 --> 00:26:30,720
of our ignorance, that we give rise to intention, even the intention to help ourselves, to

292
00:26:30,720 --> 00:26:36,800
do good things for ourselves, to create a life that is supposedly going to make us happy.

293
00:26:36,800 --> 00:26:41,640
Even giving rise to this intention is due to ignorance, because if we understood, as I said,

294
00:26:41,640 --> 00:26:46,660
reality for what it was, we wouldn't see any need to create anything, we would be content

295
00:26:46,660 --> 00:26:50,320
and comfortable and happy with things as they are.

296
00:26:50,320 --> 00:26:55,720
We wouldn't give rise to this craving that comes from experience.

297
00:26:55,720 --> 00:27:00,560
Our experience of reality would simply be the conscious experience of the physical and

298
00:27:00,560 --> 00:27:03,040
mental at the six senses.

299
00:27:03,040 --> 00:27:06,400
It would stop at the contact and the sensation.

300
00:27:06,400 --> 00:27:12,800
We would feel happy, we would feel pain, we would feel calm, but we wouldn't attach

301
00:27:12,800 --> 00:27:15,280
to any of these feelings as good or bad.

302
00:27:15,280 --> 00:27:18,640
Anything that we saw would simply be what it is.

303
00:27:18,640 --> 00:27:25,160
We would see it clearly as it is for what it is and not put any value judgment on it

304
00:27:25,160 --> 00:27:36,880
other than what was immediately apparent as being what it was, as being a risen phenomenon

305
00:27:36,880 --> 00:27:39,360
that comes about and ceases.

306
00:27:39,360 --> 00:27:43,760
We would live our lives in the way that many of us truly think that we are living our

307
00:27:43,760 --> 00:27:44,760
lives already.

308
00:27:44,760 --> 00:27:49,480
We think that we are now here living our lives in a very ordinary way.

309
00:27:49,480 --> 00:27:55,760
And we think that we in general experience a great peace and happiness, but as a result

310
00:27:55,760 --> 00:28:00,120
of our craving and our clinging, we actually create great suffering and we're not really

311
00:28:00,120 --> 00:28:03,560
living in true reality.

312
00:28:03,560 --> 00:28:07,680
Our experience of reality is, as they say, tangential.

313
00:28:07,680 --> 00:28:13,040
We experience reality for a moment and then more often on a conception, the idea of it

314
00:28:13,040 --> 00:28:16,320
being a good or a bad thing that we experience.

315
00:28:16,320 --> 00:28:21,000
When we see someone, something, we hear something, smell, taste immediately, it takes us

316
00:28:21,000 --> 00:28:28,120
away into our craving, craving, leading to clinging, clinging, leading to this, becoming

317
00:28:28,120 --> 00:28:39,240
or creating of some kind of intention to attain or to be free from and so on.

318
00:28:39,240 --> 00:28:44,960
Which in turn gives rise to conflict and suffering and not getting what we want and getting

319
00:28:44,960 --> 00:28:55,120
what we don't want and being dissatisfied and disappointed and sorrow, lamentation and

320
00:28:55,120 --> 00:28:59,880
despair, all of these things come from our attachments, our judgments, our ideas that things

321
00:28:59,880 --> 00:29:01,400
are good and bad.

322
00:29:01,400 --> 00:29:07,720
So this is an incredibly important teaching, as I said, and incredibly important for

323
00:29:07,720 --> 00:29:08,720
meditators.

324
00:29:08,720 --> 00:29:13,200
It's really the core of the meditation practice and the core theory that we should understand

325
00:29:13,200 --> 00:29:19,960
when we start to practice meditation, because this is how we're going to change our minds

326
00:29:19,960 --> 00:29:22,080
during the meditation practice.

327
00:29:22,080 --> 00:29:24,240
So thank you for tuning in.

328
00:29:24,240 --> 00:29:28,080
I hope this has been useful in that those of you who are watching are able to put this

329
00:29:28,080 --> 00:29:29,080
into practice.

330
00:29:29,080 --> 00:29:34,080
Please don't be satisfied simply by intellectual understanding of the Buddhist teaching.

331
00:29:34,080 --> 00:29:38,040
Do take the time and effort to put this into practice.

332
00:29:38,040 --> 00:29:42,120
And I wish for this teaching to be of benefit to all of you and that you are able to

333
00:29:42,120 --> 00:29:47,240
throw this teaching and throw your practice of the Buddhist teaching to find true peace,

334
00:29:47,240 --> 00:29:49,240
happiness and freedom from suffering.

335
00:29:49,240 --> 00:30:19,080
Thank you and have a good day.

