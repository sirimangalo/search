1
00:00:00,000 --> 00:00:06,000
Good evening, everyone. Welcome to our evening dhamma.

2
00:00:06,000 --> 00:00:11,000
Tonight, all our meditators are absent.

3
00:00:11,000 --> 00:00:20,000
They are in their rooms. We've locked them away.

4
00:00:20,000 --> 00:00:34,000
In their cells.

5
00:00:34,000 --> 00:00:37,000
Our three meditators are on the last.

6
00:00:37,000 --> 00:00:42,000
They've synchronized, so they're all on the last bit of the course.

7
00:00:42,000 --> 00:00:46,000
They're very intent on their practice. It's really good to see.

8
00:00:46,000 --> 00:00:50,000
Which is a thing to rejoice over. We have three meditators now.

9
00:00:50,000 --> 00:00:58,000
One of them finished this morning, but she's continuing on.

10
00:00:58,000 --> 00:01:02,000
It's so great to see people when they're in their own,

11
00:01:02,000 --> 00:01:06,000
where their practice is really succeeding.

12
00:01:06,000 --> 00:01:11,000
It's so great every time someone succeeds at the course.

13
00:01:11,000 --> 00:01:15,000
Every time someone sticks it out.

14
00:01:15,000 --> 00:01:20,000
You never know.

15
00:01:20,000 --> 00:01:23,000
It's not an easy thing.

16
00:01:23,000 --> 00:01:27,000
Tonight, I thought I'd talk about success.

17
00:01:27,000 --> 00:01:35,000
The Buddha's teaching on success is...

18
00:01:35,000 --> 00:01:40,000
I would say one of the most widely applicable teachings.

19
00:01:40,000 --> 00:01:45,000
The most useful, broadly speaking.

20
00:01:45,000 --> 00:01:48,000
When we get out... and I get all these questions about people,

21
00:01:48,000 --> 00:01:52,000
how they can solve their problems in life.

22
00:01:52,000 --> 00:01:59,000
I think this teaching will be very useful.

23
00:01:59,000 --> 00:02:03,000
They're useful to answer these sorts of questions.

24
00:02:03,000 --> 00:02:05,000
People have challenges in their lives.

25
00:02:05,000 --> 00:02:14,000
How to succeed at anything.

26
00:02:14,000 --> 00:02:17,000
How to succeed in the world.

27
00:02:17,000 --> 00:02:21,000
How to succeed in relationships.

28
00:02:21,000 --> 00:02:27,000
How to succeed in business.

29
00:02:27,000 --> 00:02:33,000
How to succeed in spirituality, of course.

30
00:02:33,000 --> 00:02:43,000
How to be prosperous and successful.

31
00:02:43,000 --> 00:02:50,000
The Buddha taught what are called the for idipada.

32
00:02:50,000 --> 00:03:11,000
Idipada are the four roads to power to mastery to success.

33
00:03:11,000 --> 00:03:21,000
I don't suppose the real translation is success, it's more just about mastery.

34
00:03:21,000 --> 00:03:37,000
I don't know that the Buddha would actually have thought to apply these for to the world of the world of the life.

35
00:03:37,000 --> 00:03:47,000
There are the four ways to succeed in anything.

36
00:03:47,000 --> 00:03:54,000
Four things you need to succeed.

37
00:03:54,000 --> 00:03:58,000
It clearly are.

38
00:03:58,000 --> 00:04:11,000
It's such a good teaching that the Buddha gave us very simple and clear something to always keep in mind no matter what you do in life.

39
00:04:11,000 --> 00:04:14,000
Most especially when you're meditating.

40
00:04:14,000 --> 00:04:19,000
Keep these four things in mind and understand them.

41
00:04:19,000 --> 00:04:30,000
Your meditation will succeed.

42
00:04:30,000 --> 00:04:33,000
You can't succeed without these four.

43
00:04:33,000 --> 00:04:51,000
The first is chanda, person needs to be interested in what one's doing.

44
00:04:51,000 --> 00:05:11,000
It's very hard to succeed at something if it's not something you are inclined to do, something you like doing.

45
00:05:11,000 --> 00:05:27,000
If you're doing a job that you really hate, it works against your success.

46
00:05:27,000 --> 00:05:33,000
If you don't like to meditate, not easy to succeed.

47
00:05:33,000 --> 00:05:48,000
If you're just forcing yourself to meditate, holding your nose and doing it, not easy to succeed.

48
00:05:48,000 --> 00:05:55,000
That's why some people criticize insight meditation because it's not comfortable.

49
00:05:55,000 --> 00:06:02,000
They talk about more comfortable the types of meditation that's being better.

50
00:06:02,000 --> 00:06:04,000
I don't think anyone would disagree.

51
00:06:04,000 --> 00:06:09,000
It's much better to have a pleasant meditation.

52
00:06:09,000 --> 00:06:11,000
All other things considered.

53
00:06:11,000 --> 00:06:24,000
Very much better if we didn't have to struggle.

54
00:06:24,000 --> 00:06:37,000
But of course, the problem with comfortable meditations and pleasant ones is they don't actually generally challenge you or bring you out of your comfort zone.

55
00:06:37,000 --> 00:06:41,000
They generally don't lead you to enlightenment as a result.

56
00:06:41,000 --> 00:06:53,000
How do you practice something that challenges you, something that forces you to change without hating it?

57
00:06:53,000 --> 00:07:01,000
I think the only mindfulness meditation has the power to allow you to do this.

58
00:07:01,000 --> 00:07:09,000
Can't imagine.

59
00:07:09,000 --> 00:07:15,000
I think with anything else, you just have to change your mind about it.

60
00:07:15,000 --> 00:07:24,000
You have to actually make it comfortable, make it enjoyable or I'll stop doing it.

61
00:07:24,000 --> 00:07:37,000
Of course, the mindfulness is quite different because our dislike of anything, including meditation, is independent of the actual thing, including the meditation.

62
00:07:37,000 --> 00:07:46,000
If you dislike meditating, there's nothing to do with meditation. It's your own reaction and judgment of it, which is independent.

63
00:07:46,000 --> 00:07:50,000
That's part of what we're learning in meditation.

64
00:07:50,000 --> 00:07:59,000
Meditation itself isn't exempt from our defilements and isn't exempt from our study of our defilements.

65
00:07:59,000 --> 00:08:12,000
If you don't like meditating, well, it's still a disliking, just like any other disliking.

66
00:08:12,000 --> 00:08:25,000
How to overcome the dislike of meditation is not to force yourself to do it even though you dislike it, even though you don't want to meditate.

67
00:08:25,000 --> 00:08:29,000
The answer is to meditate on the disliking.

68
00:08:29,000 --> 00:08:36,000
When it becomes the object, it's no longer a problem.

69
00:08:36,000 --> 00:08:54,000
When instead of using the dislike and having it be a part of your approach to meditation, yes, I dislike it, but I'm meditating on the disliking.

70
00:08:54,000 --> 00:08:56,000
There's no problem.

71
00:08:56,000 --> 00:09:06,000
But be clear, if you dislike meditating, it's going to be very much against your potential for success.

72
00:09:06,000 --> 00:09:12,000
This isn't something that you should do even though you don't want to do it.

73
00:09:12,000 --> 00:09:21,000
If you don't want to do it, of course, the other option is to just stop, but, of course, that's not really an option.

74
00:09:21,000 --> 00:09:30,000
Be mindful of the disliking, meditate on it.

75
00:09:30,000 --> 00:09:36,000
First, the second is really an effort.

76
00:09:36,000 --> 00:09:41,000
In a mundane sense, of course, this is true with any work you do.

77
00:09:41,000 --> 00:09:44,000
If you don't put out effort, you don't succeed.

78
00:09:44,000 --> 00:09:48,000
But with meditation, again, it's a little bit tricky.

79
00:09:48,000 --> 00:09:55,000
It's a little bit different, because if you just put out effort, it's very wrong, actually.

80
00:09:55,000 --> 00:10:10,000
If you just push and push and push, it's not actually right effort.

81
00:10:10,000 --> 00:10:12,000
Right effort is a momentary thing.

82
00:10:12,000 --> 00:10:19,000
It's in the moment that you actually do it.

83
00:10:19,000 --> 00:10:23,000
It doesn't have a value.

84
00:10:23,000 --> 00:10:26,000
You either have it or you don't.

85
00:10:26,000 --> 00:10:30,000
Are you mindful of the object or are you not?

86
00:10:30,000 --> 00:10:33,000
If you're mindful, there's effort there as well.

87
00:10:33,000 --> 00:10:36,000
The effort to be mindful, right?

88
00:10:36,000 --> 00:10:43,000
You have made the choice to be mindful to me.

89
00:10:43,000 --> 00:10:48,000
That's the effort.

90
00:10:48,000 --> 00:10:58,000
The effort to do that repeatedly.

91
00:10:58,000 --> 00:11:09,000
The effort to return again and again to mindfulness.

92
00:11:09,000 --> 00:11:13,000
It's not the effort to walk or the effort to sit.

93
00:11:13,000 --> 00:11:17,000
If you're tired of walking, we do a lot of meditation.

94
00:11:17,000 --> 00:11:23,000
If you do a lot of walking meditation and it becomes tiresome.

95
00:11:23,000 --> 00:11:38,000
Or you're tired of sitting, then that has to be the object of your meditation.

96
00:11:38,000 --> 00:11:45,000
It's interesting how if you're really mindful fatigue doesn't bother you.

97
00:11:45,000 --> 00:11:49,000
There's a great energy involved.

98
00:11:49,000 --> 00:11:54,000
But practically speaking, in a mundane level, it just means do it.

99
00:11:54,000 --> 00:11:58,000
You have to walk even when you may be feel tired.

100
00:11:58,000 --> 00:12:02,000
Just sit even when you feel tired.

101
00:12:02,000 --> 00:12:07,000
Doing it and being mindful of the tired rather than mindful of the fatigue

102
00:12:07,000 --> 00:12:18,000
rather than letting the fatigue control your practice.

103
00:12:18,000 --> 00:12:20,000
You need to do it.

104
00:12:20,000 --> 00:12:26,000
We get home from work sometimes and feel too tired to meditate.

105
00:12:26,000 --> 00:12:32,000
The effort is making this decision to be mindful.

106
00:12:32,000 --> 00:12:38,000
Again, focusing on the fatigue as the object rather than letting it control you.

107
00:12:38,000 --> 00:12:39,000
You need effort.

108
00:12:39,000 --> 00:12:44,000
If you don't do it, it doesn't get done with any work.

109
00:12:44,000 --> 00:12:48,000
This is applicable in any situation, of course.

110
00:12:48,000 --> 00:12:50,000
You don't put out effort.

111
00:12:50,000 --> 00:12:53,000
No success.

112
00:12:53,000 --> 00:12:56,000
The third thing you need is get that.

113
00:12:56,000 --> 00:13:00,000
You have to keep your mind on it.

114
00:13:00,000 --> 00:13:03,000
You have to pay attention.

115
00:13:03,000 --> 00:13:12,000
You have to focus on it.

116
00:13:12,000 --> 00:13:15,000
You don't focus on your work or even study.

117
00:13:15,000 --> 00:13:19,000
You don't study for your exams.

118
00:13:19,000 --> 00:13:25,000
If you study out in party or you don't ever spend time doing your work,

119
00:13:25,000 --> 00:13:28,000
it doesn't get done.

120
00:13:28,000 --> 00:13:31,000
Or if you do it, but you're not focused on it.

121
00:13:31,000 --> 00:13:36,000
You're not paying attention.

122
00:13:36,000 --> 00:13:42,000
You need to do a sloppy job and do a poor job.

123
00:13:42,000 --> 00:13:53,000
Of course, this isn't true or true or nowhere else than meditation.

124
00:13:53,000 --> 00:13:58,000
If you're not paying attention and meditation, what are you doing?

125
00:13:58,000 --> 00:14:01,000
If you're meditating, but your mind is elsewhere.

126
00:14:01,000 --> 00:14:08,000
Your mind is focused on something else and not going to succeed.

127
00:14:08,000 --> 00:14:18,000
If you're not here and now, paying attention is a big buzzword in sight meditation.

128
00:14:18,000 --> 00:14:27,000
We talk about this a lot, bear attention, people talk about it.

129
00:14:27,000 --> 00:14:31,000
A sign that you're being mindful is attention.

130
00:14:31,000 --> 00:14:36,000
If you have a lot of mindfulness, it feels like you're paying attention.

131
00:14:36,000 --> 00:14:40,000
Mindfulness grasps the object, confronts it.

132
00:14:40,000 --> 00:14:56,000
They're not the same thing I think, but paying attention or being attentive is certainly a result of mindfulness.

133
00:14:56,000 --> 00:15:05,000
Whenever you do, you have to keep it in mind.

134
00:15:05,000 --> 00:15:13,000
Keep focused on it.

135
00:15:13,000 --> 00:15:20,000
Both in terms of remembering to do it, and in terms of when you do it,

136
00:15:20,000 --> 00:15:26,000
be present. Whatever you're going to do in life, if you want to succeed at it,

137
00:15:26,000 --> 00:15:30,000
you have to be there and be present.

138
00:15:30,000 --> 00:15:34,000
The fourth is we monks, we monks.

139
00:15:34,000 --> 00:15:36,000
We monks is an interesting word grammatically.

140
00:15:36,000 --> 00:15:41,000
I just found out it actually comes from man.

141
00:15:41,000 --> 00:15:46,000
I think it's root having to do a thinking in the mind.

142
00:15:46,000 --> 00:15:52,000
It should have been me monks, but it becomes V.

143
00:15:52,000 --> 00:15:55,000
There's nothing to do with the prefix V, which means special.

144
00:15:55,000 --> 00:15:59,000
It's V, but anyway.

145
00:15:59,000 --> 00:16:04,000
I always thought it had something to read for special, but it doesn't.

146
00:16:04,000 --> 00:16:09,000
It just means me monks, it's an interesting, if you think grammatically,

147
00:16:09,000 --> 00:16:16,000
it's a re-duplication of the beginning, monk city, me monks city.

148
00:16:16,000 --> 00:16:21,000
They do this in poly, but if you think of that mentally,

149
00:16:21,000 --> 00:16:23,000
it's not just thinking about something.

150
00:16:23,000 --> 00:16:26,000
You're me monks city.

151
00:16:26,000 --> 00:16:30,000
You roll it around.

152
00:16:30,000 --> 00:16:32,000
It has this feeling to it.

153
00:16:32,000 --> 00:16:39,000
It's easy to understand why they pick this forum to talk about this concept,

154
00:16:39,000 --> 00:16:41,000
how language evolves.

155
00:16:41,000 --> 00:16:44,000
It's very natural.

156
00:16:44,000 --> 00:16:47,000
So this means to consider.

157
00:16:47,000 --> 00:16:51,000
It's not like chitta, chitta means to keep focused on it, to keep it in mind.

158
00:16:51,000 --> 00:16:53,000
Chitta means mind.

159
00:16:53,000 --> 00:16:59,000
So chitta means you have to mind it or be mindful of it.

160
00:16:59,000 --> 00:17:04,000
My monk says, when you're focused on it, because when you're focused on something,

161
00:17:04,000 --> 00:17:09,000
it's easy to just get tunnel vision.

162
00:17:09,000 --> 00:17:13,000
And even it reinforces your bad habit.

163
00:17:13,000 --> 00:17:19,000
If you're not careful, you can focus on something, doing it, doing it, doing it,

164
00:17:19,000 --> 00:17:25,000
and never change, never improve.

165
00:17:25,000 --> 00:17:29,000
So your efficiency is very low.

166
00:17:29,000 --> 00:17:33,000
Someone might do walking and sitting meditation for many hours,

167
00:17:33,000 --> 00:17:39,000
but without stepping back and saying, hey, am I doing this properly?

168
00:17:39,000 --> 00:17:44,000
Without considering their progress.

169
00:17:44,000 --> 00:17:51,000
I think a lot of this goes on when people, when we have these interviews,

170
00:17:51,000 --> 00:17:54,000
people are forced to reflect on their practice.

171
00:17:54,000 --> 00:18:03,000
Without this, it's easy to reinforce your bad habits and get stuck.

172
00:18:03,000 --> 00:18:13,000
After some time, you slowly, slowly get off track.

173
00:18:13,000 --> 00:18:17,000
You can get caught up in something.

174
00:18:17,000 --> 00:18:26,000
We monks, as it brings you back.

175
00:18:26,000 --> 00:18:29,000
I'm not seeing this clearly, I'm developing,

176
00:18:29,000 --> 00:18:34,000
veering off path, developing bad habits.

177
00:18:34,000 --> 00:18:36,000
It goes with anything in life.

178
00:18:36,000 --> 00:18:38,000
You can't just work like an ox.

179
00:18:38,000 --> 00:18:43,000
You have to be clever.

180
00:18:43,000 --> 00:18:47,000
You want to succeed, you want to progress.

181
00:18:47,000 --> 00:18:52,000
You have to be able to reflect and consider that your methods might be wrong,

182
00:18:52,000 --> 00:18:55,000
just because you've been doing something a certain way for so long.

183
00:18:55,000 --> 00:18:57,000
Like Ananda, right?

184
00:18:57,000 --> 00:19:00,000
The famous story we monks had,

185
00:19:00,000 --> 00:19:06,000
he was meditating all night, dead set on becoming enlightened.

186
00:19:06,000 --> 00:19:09,000
And it just wasn't happening.

187
00:19:09,000 --> 00:19:13,000
And just before dawn, he just thought to himself, he said,

188
00:19:13,000 --> 00:19:14,000
wait a minute.

189
00:19:14,000 --> 00:19:16,000
I've been walking all night.

190
00:19:16,000 --> 00:19:19,000
I've put up too much effort.

191
00:19:19,000 --> 00:19:23,000
So I went if I were to lie down and balance my faculty

192
00:19:23,000 --> 00:19:25,000
to cultivate concentration.

193
00:19:25,000 --> 00:19:27,000
And that's all it took, as he was lying down,

194
00:19:27,000 --> 00:19:29,000
he became enlightened.

195
00:19:29,000 --> 00:19:32,000
Before his head touched the pillow,

196
00:19:32,000 --> 00:19:35,000
but his feet were already off the ground.

197
00:19:35,000 --> 00:19:38,000
He became enlightened.

198
00:19:38,000 --> 00:19:41,000
So he's special in that he became enlightened

199
00:19:41,000 --> 00:19:45,000
in none of the four postures.

200
00:19:45,000 --> 00:19:47,000
But this was among us.

201
00:19:47,000 --> 00:19:50,000
This was considering maybe I've got to change.

202
00:19:50,000 --> 00:19:53,000
This is the ability to adapt and adjust.

203
00:19:53,000 --> 00:19:56,000
It's, of course, and you can see this in the world

204
00:19:56,000 --> 00:20:00,000
when people have a job that they do

205
00:20:00,000 --> 00:20:02,000
and they do it the same way, the same way.

206
00:20:02,000 --> 00:20:05,000
Never thinking that maybe they could do it differently

207
00:20:05,000 --> 00:20:08,000
and improve.

208
00:20:08,000 --> 00:20:10,000
And if anyone suggests that they could improve,

209
00:20:10,000 --> 00:20:13,000
they're often quite reactionary,

210
00:20:13,000 --> 00:20:16,000
thinking that it works fine the way they do it.

211
00:20:16,000 --> 00:20:18,000
This is a bad side.

212
00:20:18,000 --> 00:20:21,000
The inability to change the inability to adapt.

213
00:20:21,000 --> 00:20:27,000
And so on.

214
00:20:27,000 --> 00:20:30,000
It will hinder your ability to succeed.

215
00:20:30,000 --> 00:20:34,000
Flexibility is important, adaptability.

216
00:20:34,000 --> 00:20:38,000
These are all signs of someone who is mindful,

217
00:20:38,000 --> 00:20:42,000
who is not stuck in the attached to anything

218
00:20:42,000 --> 00:20:45,000
or stuck in ego and so on.

219
00:20:45,000 --> 00:20:48,000
Their ability to adapt when things change,

220
00:20:48,000 --> 00:20:49,000
they don't get upset.

221
00:20:49,000 --> 00:20:51,000
They change.

222
00:20:51,000 --> 00:20:55,000
And they adapt.

223
00:20:55,000 --> 00:21:00,000
There's a sign of ability.

224
00:21:00,000 --> 00:21:04,000
I mean, we monks had the ability to consider

225
00:21:04,000 --> 00:21:08,000
and adjust, not just plow on.

226
00:21:12,000 --> 00:21:13,000
So there you go.

227
00:21:13,000 --> 00:21:16,000
There's the demo for tonight.

228
00:21:16,000 --> 00:21:19,000
I'm supposed there are some questions.

229
00:21:19,000 --> 00:21:22,000
I'm going to check that out.

230
00:21:22,000 --> 00:21:25,000
If I can log in here.

231
00:21:25,000 --> 00:21:29,000
Let's see here.

232
00:21:29,000 --> 00:21:32,000
What type of experience is intuition?

233
00:21:32,000 --> 00:21:34,000
Sensing danger is sensing the correct way

234
00:21:34,000 --> 00:21:38,000
to respond to something without really thinking.

235
00:21:38,000 --> 00:21:40,000
Sensing something isn't clear to me,

236
00:21:40,000 --> 00:21:42,000
which sense it belongs to.

237
00:21:42,000 --> 00:21:43,000
Let's mind.

238
00:21:50,000 --> 00:21:52,000
The intuition can be wrong,

239
00:21:52,000 --> 00:21:56,000
but it involves how we process things,

240
00:21:56,000 --> 00:22:00,000
how we put information together,

241
00:22:00,000 --> 00:22:04,000
how the answer we come up with.

242
00:22:04,000 --> 00:22:07,000
When I say, what is 2 plus 2?

243
00:22:07,000 --> 00:22:09,000
4 is your answer.

244
00:22:09,000 --> 00:22:11,000
That's simple intuition.

245
00:22:11,000 --> 00:22:14,000
You're faced with a question 2 plus 2.

246
00:22:14,000 --> 00:22:17,000
And it's that, I don't know.

247
00:22:17,000 --> 00:22:19,000
I mean, it's probably more complicated than this,

248
00:22:19,000 --> 00:22:23,000
but it's basically the ability to answer that.

249
00:22:23,000 --> 00:22:35,000
When you're given a set of phenomena,

250
00:22:35,000 --> 00:22:39,000
or stimuli,

251
00:22:39,000 --> 00:22:41,000
it's much more complicated equation,

252
00:22:41,000 --> 00:22:45,000
but the answer you come up with.

253
00:22:45,000 --> 00:22:47,000
And so, hey, this is danger.

254
00:22:47,000 --> 00:22:52,000
Some of this person's lying to me or something like that.

255
00:22:52,000 --> 00:22:55,000
If you're talking about some sort of

256
00:22:55,000 --> 00:23:00,000
extra sensory intuition,

257
00:23:00,000 --> 00:23:02,000
then all that is,

258
00:23:02,000 --> 00:23:06,000
I would say, there's more to the equation than we understand.

259
00:23:06,000 --> 00:23:08,000
Suppose someone comes to you,

260
00:23:08,000 --> 00:23:10,000
and they're not doing anything,

261
00:23:10,000 --> 00:23:13,000
but you just intuit that they've got it out for you.

262
00:23:13,000 --> 00:23:15,000
I mean, sometimes that's wrong.

263
00:23:15,000 --> 00:23:18,000
Sometimes you're wrong and they don't actually have it out for you.

264
00:23:18,000 --> 00:23:20,000
But sometimes it's remarkable that,

265
00:23:20,000 --> 00:23:23,000
hey, it turns out that I knew it.

266
00:23:23,000 --> 00:23:25,000
This person had it out for me.

267
00:23:25,000 --> 00:23:28,000
The equation is just more complicated.

268
00:23:28,000 --> 00:23:31,000
It probably has something to do with past lives.

269
00:23:31,000 --> 00:23:37,000
Maybe even have something to do with being able to read people's emotions,

270
00:23:37,000 --> 00:23:38,000
that kind of thing.

271
00:23:38,000 --> 00:23:42,000
Read their minds or something like that.

272
00:23:42,000 --> 00:23:44,000
But if you're talking about which sense it is,

273
00:23:44,000 --> 00:23:45,000
it's mental.

274
00:23:45,000 --> 00:23:47,000
It's how the mind,

275
00:23:47,000 --> 00:23:50,000
sun works, it comes down to the basics of sun,

276
00:23:50,000 --> 00:23:56,000
how we perceive things.

277
00:23:56,000 --> 00:23:58,000
And the correct way to respond,

278
00:23:58,000 --> 00:24:00,000
of course, it's not always the correct way.

279
00:24:00,000 --> 00:24:02,000
Again, you can be very wrong.

280
00:24:02,000 --> 00:24:06,000
Intuitively, you know this is the correct way and be very wrong.

281
00:24:06,000 --> 00:24:09,000
Or you can be mildly wrong.

282
00:24:09,000 --> 00:24:12,000
It's just how we answer the equation.

283
00:24:12,000 --> 00:24:16,000
It's a good question.

284
00:24:16,000 --> 00:24:20,000
A little bit, a little bit intellectual

285
00:24:20,000 --> 00:24:24,000
and all that entirely useful.

286
00:24:24,000 --> 00:24:26,000
I mean, for a meditation point of view.

287
00:24:26,000 --> 00:24:29,000
But interesting.

288
00:24:29,000 --> 00:24:32,000
Protection, you draw a line on killing.

289
00:24:32,000 --> 00:24:34,000
Staying that killing plants is okay,

290
00:24:34,000 --> 00:24:38,000
but any sentient beings is wrong.

291
00:24:38,000 --> 00:24:39,000
Okay, well, let's be clear.

292
00:24:39,000 --> 00:24:40,000
Nothing is wrong.

293
00:24:40,000 --> 00:24:41,000
Killing is not wrong.

294
00:24:41,000 --> 00:24:44,000
You can murder.

295
00:24:44,000 --> 00:24:49,000
You can kill everyone on earth and it will never be wrong.

296
00:24:49,000 --> 00:24:51,000
Acts are never wrong.

297
00:24:51,000 --> 00:24:53,000
What's wrong is the intention.

298
00:24:53,000 --> 00:24:57,000
And it's wrong because it perverts your mind, right?

299
00:24:57,000 --> 00:25:00,000
It's not wrong to kill people because they die.

300
00:25:00,000 --> 00:25:05,000
It's wrong to kill people because it hurts you.

301
00:25:05,000 --> 00:25:08,000
I mean, there's not really a distinction.

302
00:25:08,000 --> 00:25:11,000
It hurts you because it's awful to kill people.

303
00:25:11,000 --> 00:25:15,000
It just makes you a psychopath, but that's the reason.

304
00:25:15,000 --> 00:25:21,000
So killing plants is, look, that is as innocuous

305
00:25:21,000 --> 00:25:26,000
because no one really feels guilty or bad or sorry

306
00:25:26,000 --> 00:25:29,000
for having killed a plant.

307
00:25:29,000 --> 00:25:35,000
The plants aren't really capable of suffering.

308
00:25:35,000 --> 00:25:37,000
They wither.

309
00:25:37,000 --> 00:25:38,000
That's about it.

310
00:25:38,000 --> 00:25:42,000
But if you kill an insect, I mean, insects clearly don't want to die.

311
00:25:42,000 --> 00:25:47,000
They're clearly afraid of you.

312
00:25:47,000 --> 00:25:51,000
If you have even a mosquito, if you grab it by the legs,

313
00:25:51,000 --> 00:25:54,000
it freaks out, right?

314
00:25:54,000 --> 00:25:59,000
And there's mine there.

315
00:25:59,000 --> 00:26:03,000
I mean, there are stories of angels living in trees,

316
00:26:03,000 --> 00:26:05,000
tree spirits.

317
00:26:05,000 --> 00:26:10,000
And when you cut the tree, they freak out.

318
00:26:10,000 --> 00:26:15,000
So I mean, I think you could argue that there is some,

319
00:26:15,000 --> 00:26:20,000
perhaps some, because remember, this body doesn't have mine.

320
00:26:20,000 --> 00:26:23,000
The mind is what's associated.

321
00:26:23,000 --> 00:26:25,000
The mind is just clinging to it.

322
00:26:25,000 --> 00:26:30,000
It has been clinging to it since it was formed in the womb.

323
00:26:30,000 --> 00:26:37,000
So a mind can cling to anything.

324
00:26:37,000 --> 00:26:41,000
It's just that.

325
00:26:41,000 --> 00:26:50,000
So it really, you know, killing a sentient being is just that

326
00:26:50,000 --> 00:26:55,000
we're clear that there's a mind that is very, very much dependent

327
00:26:55,000 --> 00:27:03,000
and caught up with this body, such that if you do kill it,

328
00:27:03,000 --> 00:27:08,000
it's really going to cause a lot of stress and suffering for that mind

329
00:27:08,000 --> 00:27:10,000
on a very intense level.

330
00:27:10,000 --> 00:27:13,000
You know, even to the point of, of course, feeling physical pain,

331
00:27:13,000 --> 00:27:18,000
but also great mental distress.

332
00:27:18,000 --> 00:27:21,000
If there's a tree spirit living in a tree, you might think,

333
00:27:21,000 --> 00:27:26,000
shouldn't cut these big trees.

334
00:27:26,000 --> 00:27:29,000
If you know there's a tree spirit living in the tree,

335
00:27:29,000 --> 00:27:31,000
it's probably better not to.

336
00:27:31,000 --> 00:27:35,000
For the same reason, right?

337
00:27:35,000 --> 00:27:37,000
So it's not about being wrong.

338
00:27:37,000 --> 00:27:41,000
It's about not being a jerk.

339
00:27:41,000 --> 00:27:45,000
Not cultivating mind states that are that cause stress and suffering

340
00:27:45,000 --> 00:27:52,000
for other beings if you can help it.

341
00:27:52,000 --> 00:27:54,000
Torturing living beings for the same reason.

342
00:27:54,000 --> 00:28:00,000
If you torture someone's body, it causes reaction in their minds.

343
00:28:00,000 --> 00:28:05,000
And if it's done out of malice, if it's done when you do it,

344
00:28:05,000 --> 00:28:09,000
intending to cause suffering others, it perverts your own mind.

345
00:28:09,000 --> 00:28:11,000
That's the real problem.

346
00:28:11,000 --> 00:28:18,000
It's an interesting dilemma. I mean, it's not, it's not cut and dried the way people think

347
00:28:18,000 --> 00:28:20,000
that, oh, plants just don't have mind.

348
00:28:20,000 --> 00:28:23,000
It's not really that because body doesn't have mind as well.

349
00:28:23,000 --> 00:28:30,000
It's about the mind that is attached to the body.

350
00:28:30,000 --> 00:28:34,000
You know, if you if you if you take a baseball bat to someone's car,

351
00:28:34,000 --> 00:28:37,000
it also causes suffering to that person.

352
00:28:37,000 --> 00:28:42,000
So because they're very much attached to their car,

353
00:28:42,000 --> 00:28:44,000
it's like killing a person.

354
00:28:44,000 --> 00:28:47,000
It's like torturing a person if you beat up their car because the anguish

355
00:28:47,000 --> 00:28:52,000
that they feel is very real.

356
00:28:52,000 --> 00:28:57,000
I mean, it's it's an order of magnitude greater when our attachment to the body

357
00:28:57,000 --> 00:29:01,000
when you hurt someone's body or kill the body.

358
00:29:01,000 --> 00:29:05,000
It's a very deep attachment.

359
00:29:05,000 --> 00:29:10,000
It's common to see things as they are.

360
00:29:10,000 --> 00:29:13,000
See how non beneficial indulgence are.

361
00:29:13,000 --> 00:29:19,000
So do we try to avoid them in order to see the hindrance more clearly?

362
00:29:19,000 --> 00:29:24,000
I feel that when I resist it, my practice is more pure and I can put

363
00:29:24,000 --> 00:29:26,000
a truth that doesn't cure me from them.

364
00:29:26,000 --> 00:29:29,000
I still attached to them.

365
00:29:29,000 --> 00:29:32,000
It takes, you know, I mean, I get where you're asking,

366
00:29:32,000 --> 00:29:37,000
it takes a lot more effort. You really have to put in intensive effort sometimes

367
00:29:37,000 --> 00:29:48,000
for months or years to for yourself from all indulgence and lust.

368
00:29:48,000 --> 00:29:53,000
So I mean, avoiding, yes, free is you from them,

369
00:29:53,000 --> 00:29:57,000
but it doesn't it's not a permanent fix.

370
00:29:57,000 --> 00:30:00,000
Eventually you have to face them.

371
00:30:00,000 --> 00:30:10,000
Messiah be mukka. You have to face them face the experience with mindfulness.

372
00:30:10,000 --> 00:30:14,000
Not avoiding it. You know, I mean, you're back to start off.

373
00:30:14,000 --> 00:30:20,000
It's important that you back off, but eventually you have to face and see through it.

374
00:30:20,000 --> 00:30:25,000
I mean, well, it's only by seeing it clear by observing it,

375
00:30:25,000 --> 00:30:30,000
you know, objectively that you can see how useless it is.

376
00:30:30,000 --> 00:30:35,000
Avoiding it isn't going to do that.

377
00:30:35,000 --> 00:30:39,000
So I understand this tension,

378
00:30:39,000 --> 00:30:43,000
but the answer is really just much more intensive practice,

379
00:30:43,000 --> 00:30:48,000
the point where you're able to, it's like a laboratory where you're able to see

380
00:30:48,000 --> 00:30:56,000
moments of attachment and cut them off as they arrive

381
00:30:56,000 --> 00:31:02,000
or, you know, see them as stressful and so on.

382
00:31:02,000 --> 00:31:07,000
And it's through intensive practice seeing that again and again,

383
00:31:07,000 --> 00:31:10,000
and that's what frees you.

384
00:31:10,000 --> 00:31:13,000
Should times seem to go by faster and deep meditation,

385
00:31:13,000 --> 00:31:18,000
sometimes it does. Maybe we're not practicing deep meditation,

386
00:31:18,000 --> 00:31:23,000
so sometimes it goes quick, sometimes it goes slow, it's impermanent.

387
00:31:23,000 --> 00:31:28,000
There are any teachings related to humor.

388
00:31:28,000 --> 00:31:33,000
Humor is not generally seen in a positive light.

389
00:31:33,000 --> 00:31:36,000
We frown on humor.

390
00:31:36,000 --> 00:31:40,000
I know there are many monks who are very funny and humorous.

391
00:31:40,000 --> 00:31:43,000
In Thailand it's a big thing. They're very funny monks.

392
00:31:43,000 --> 00:31:47,000
Some Thai monks are very funny.

393
00:31:47,000 --> 00:31:52,000
You know, I think there's nothing wrong with seeing the humor in something,

394
00:31:52,000 --> 00:32:01,000
but to make that, to actively purposefully try to make people laugh,

395
00:32:01,000 --> 00:32:04,000
is problematic because it creates attachment.

396
00:32:04,000 --> 00:32:07,000
It creates liking. You know, you like that.

397
00:32:07,000 --> 00:32:11,000
And we like dhamma talks. If you like the talk,

398
00:32:11,000 --> 00:32:14,000
I think the only liking would be the liking.

399
00:32:14,000 --> 00:32:18,000
The only liking that's of any value is the liking of the dhamma.

400
00:32:18,000 --> 00:32:24,000
If you like, the Buddha's teaching on the four roads to success,

401
00:32:24,000 --> 00:32:29,000
then I would say that's generally considered positive.

402
00:32:29,000 --> 00:32:34,000
But if you like the way I deliver it, that's dangerous.

403
00:32:34,000 --> 00:32:39,000
Maybe you like the sound of my voice, or maybe I tell a story and you think,

404
00:32:39,000 --> 00:32:45,000
what a good story makes you smile and makes you laugh.

405
00:32:45,000 --> 00:32:49,000
But generally problematic, because that's liking something that is not dhamma.

406
00:32:49,000 --> 00:32:52,000
It's liking something superfluous.

407
00:32:52,000 --> 00:32:57,000
If you laugh at something, there's a pleasure there, and you like that pleasure.

408
00:32:57,000 --> 00:33:08,000
I mean, not always, if you're laughing mindfully, and it's not a problem.

409
00:33:08,000 --> 00:33:15,000
But it'll be mindful.

410
00:33:15,000 --> 00:33:21,000
Is there a way to see for myself that rebirth is bound to happen when they still have attachment at death,

411
00:33:21,000 --> 00:33:25,000
instead of just believing in the concept?

412
00:33:25,000 --> 00:33:28,000
You don't know.

413
00:33:28,000 --> 00:33:31,000
See suffering in life, but not able to believe in rebirth.

414
00:33:31,000 --> 00:33:34,000
It's causing issues because I'm not able to commit myself.

415
00:33:34,000 --> 00:33:47,000
Well, if that's, if that's, no, I don't go for it.

416
00:33:47,000 --> 00:33:52,000
You don't need to know your, you don't need to remember rebirth past life

417
00:33:52,000 --> 00:33:55,000
in order to commit yourself to the practice completely.

418
00:33:55,000 --> 00:34:01,000
That's an excuse that you're making.

419
00:34:01,000 --> 00:34:05,000
You don't need to believe in past lives or future lives.

420
00:34:05,000 --> 00:34:10,000
I mean, I think a belief that there is nothing after death is dangerous.

421
00:34:10,000 --> 00:34:12,000
I mean, that's a belief.

422
00:34:12,000 --> 00:34:16,000
But if you just give up that belief and don't think about future lives and so on,

423
00:34:16,000 --> 00:34:22,000
you'll be okay because the future lives is just, it's not anything different.

424
00:34:22,000 --> 00:34:24,000
It's just an extrapolation of this.

425
00:34:24,000 --> 00:34:28,000
So as long as you understand how this works and that's all,

426
00:34:28,000 --> 00:34:32,000
if that's how you understand it to be in the future, you'll be fine.

427
00:34:32,000 --> 00:34:36,000
Don't worry about death.

428
00:34:36,000 --> 00:34:43,000
Yes, it's, it's conceptually useful to think, hey, I might go to hell if I'm a really bad person,

429
00:34:43,000 --> 00:34:47,000
you're into meditation that's not really an issue.

430
00:34:47,000 --> 00:34:53,000
Focus on, focus on understanding how the mind works and at death,

431
00:34:53,000 --> 00:34:56,000
it's no different, not categorically.

432
00:34:56,000 --> 00:34:59,000
It's still just experience.

433
00:34:59,000 --> 00:35:02,000
So if you're not able to commit yourself to the practice completely,

434
00:35:02,000 --> 00:35:04,000
there's something else wrong.

435
00:35:04,000 --> 00:35:10,000
Maybe you don't like the practice and then you have to develop chanda.

436
00:35:10,000 --> 00:35:16,000
So the term Buddha put reserved for noble disciples or any monk in general.

437
00:35:16,000 --> 00:35:21,000
I don't know.

438
00:35:21,000 --> 00:35:23,000
I think it's a loose term.

439
00:35:23,000 --> 00:35:27,000
I think it's something the Buddha used rhetorically.

440
00:35:27,000 --> 00:35:31,000
There's a rhetorical value to it.

441
00:35:31,000 --> 00:35:36,000
But my inclination would be to say it refers to noble in light and when

442
00:35:36,000 --> 00:35:41,000
our heart is really, no, I don't think, I don't think it's something

443
00:35:41,000 --> 00:35:45,000
that was ever defined as only this and only that.

444
00:35:45,000 --> 00:35:53,000
But I would, I would, I would be surprised if it meant anything other than

445
00:35:53,000 --> 00:35:58,000
our heart or at least enlighten me.

446
00:35:58,000 --> 00:36:01,000
Isn't killing more about ending life, cutting down a tree?

447
00:36:01,000 --> 00:36:04,000
Probably does not kill the tree spirit.

448
00:36:04,000 --> 00:36:07,000
The car won't kill the owner, but neither does killing the body.

449
00:36:07,000 --> 00:36:09,000
If you kill the body, it doesn't kill the mind.

450
00:36:09,000 --> 00:36:14,000
The mind is always being born and died.

451
00:36:14,000 --> 00:36:16,000
You're thinking of something.

452
00:36:16,000 --> 00:36:19,000
See, this is the danger of getting too conceptual.

453
00:36:19,000 --> 00:36:21,000
There's nothing category.

454
00:36:21,000 --> 00:36:23,000
It's not categorically different.

455
00:36:23,000 --> 00:36:25,000
Cut down the tree spirit, cut down the tree.

456
00:36:25,000 --> 00:36:27,000
The tree spirit has to move on.

457
00:36:27,000 --> 00:36:28,000
You kill the body.

458
00:36:28,000 --> 00:36:30,000
The mind has to move on.

459
00:36:30,000 --> 00:36:33,000
It's not categorically different.

460
00:36:33,000 --> 00:36:34,000
You destroy the car.

461
00:36:34,000 --> 00:36:36,000
The car owner has to get a new car.

462
00:36:36,000 --> 00:36:39,000
It's actually not from a point of view of the mind.

463
00:36:39,000 --> 00:36:40,000
It's not categorically.

464
00:36:40,000 --> 00:36:46,000
I mean, this may be a heretic thing to say, but I think I'm on solid ground.

465
00:36:46,000 --> 00:36:50,000
I know we talk a lot about all these special mind states, but they're very much

466
00:36:50,000 --> 00:36:51,000
contrived.

467
00:36:51,000 --> 00:36:56,000
They're not really what's really going on.

468
00:36:56,000 --> 00:37:01,000
Anyway, that might be, maybe some debate over that one.

469
00:37:01,000 --> 00:37:07,000
But I'm going to stand by my words.

470
00:37:07,000 --> 00:37:09,000
You can't end the life.

471
00:37:09,000 --> 00:37:11,000
You only end the physical life.

472
00:37:11,000 --> 00:37:16,000
The mind is born and dies every moment.

473
00:37:16,000 --> 00:37:17,000
There you go.

474
00:37:17,000 --> 00:37:21,000
There's all the questions for tonight.

475
00:37:21,000 --> 00:37:22,000
Thank you all for tuning in.

476
00:37:22,000 --> 00:37:39,000
Have a good night.

