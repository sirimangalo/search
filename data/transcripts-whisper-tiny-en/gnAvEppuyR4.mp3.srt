1
00:00:00,000 --> 00:00:06,880
I was actually thinking about problems I'm having when you're talking about the happiness

2
00:00:08,400 --> 00:00:10,400
because I'm a teacher

3
00:00:10,400 --> 00:00:11,920
in high school

4
00:00:11,920 --> 00:00:13,920
math teacher, so

5
00:00:13,920 --> 00:00:18,960
there's many times where I have to employ some kind of structure and discipline in my classroom

6
00:00:20,560 --> 00:00:24,000
and you know I'll get you know sometimes I'll get angry

7
00:00:24,720 --> 00:00:28,160
and I'll kind of condemn myself for getting angry and sometimes

8
00:00:28,160 --> 00:00:35,360
you know even employing the discipline you know it's like an area of you know

9
00:00:36,800 --> 00:00:43,360
what's what's right what's my my am I doing things wrong you know in my

10
00:00:45,040 --> 00:00:48,800
some of the some of the biggest views is that I might be confused about but

11
00:00:50,960 --> 00:00:54,240
they kind of make me think like

12
00:00:54,240 --> 00:00:57,040
am I doing the right way

13
00:00:58,640 --> 00:01:02,080
well from what I remember from high school the best teacher said the hardest job

14
00:01:02,800 --> 00:01:05,360
because when you when you give people

15
00:01:08,160 --> 00:01:11,600
somehow freedom right if you if you're if you're a good person

16
00:01:12,880 --> 00:01:16,720
there's going to be people to take advantage of it and you're going to let people

17
00:01:19,520 --> 00:01:23,760
you're going to have to deal with a lot more because you're trying to do something that most

18
00:01:23,760 --> 00:01:28,480
teachers don't try to do if you're just oppressive repressive and and structured

19
00:01:31,280 --> 00:01:32,560
you know too much then you

20
00:01:35,920 --> 00:01:41,600
you don't actually allow the students to think right you don't actually allow them to think

21
00:01:41,600 --> 00:01:47,440
outside the box and you become part of the system that dries people away from public education

22
00:01:48,240 --> 00:01:51,680
math teachers I think have at the worst because how many people like math

23
00:01:51,680 --> 00:02:00,240
there's not a lot there's not a lot now but but I get my math teachers really hard time it was

24
00:02:00,240 --> 00:02:11,680
awful oh it can be a hard subject to keep the motivation going through the whole period but yeah

25
00:02:13,360 --> 00:02:16,880
but you know what I see I because I remember a couple of math teachers and one of the math

26
00:02:16,880 --> 00:02:22,640
teachers at some point he would just say okay break everybody put your pen down and and take a rest

27
00:02:22,640 --> 00:02:30,400
because it's just too much thinking all right so they stop us from doing too much work

28
00:02:32,880 --> 00:02:42,480
and I have two classes that are kind of lower level and very unruly so if I know if I give them

29
00:02:42,480 --> 00:02:52,720
the freedom you know it'll be chaos and uh that that's usually the uh some of the things I have

30
00:02:52,720 --> 00:03:02,000
to deal with the struggles of weighing things out well you can be very you can be very

31
00:03:03,840 --> 00:03:08,000
structured about it knowing without getting angry I mean that's the that's your struggle

32
00:03:08,000 --> 00:03:14,560
right yes how to be a bit disciplined in the mood that's right because the uh the wanting

33
00:03:15,760 --> 00:03:19,840
I guess the wanting them to be a certain way not so then I

34
00:03:22,720 --> 00:03:24,160
am getting a certain way about it

35
00:03:28,960 --> 00:03:29,360
indeed

36
00:03:29,360 --> 00:03:37,760
I could never I could never imagine teaching teaching children again

37
00:03:39,520 --> 00:03:44,480
because sometimes they bring kids to me to teach them meditation and it's just the normal

38
00:03:44,480 --> 00:03:51,200
experience because the kids aren't interested they're not looking at for me in meditation

39
00:03:51,200 --> 00:04:00,000
there was one case where this mother really good person really she's a wonderful really

40
00:04:01,120 --> 00:04:08,960
have the greatest appreciation for her and she has two kids young kids and she had gained so

41
00:04:08,960 --> 00:04:15,920
much from the meditation that she was so keen to pass it on to them and so she made a

42
00:04:15,920 --> 00:04:24,240
um a bargain with her son that I can't remember what it was she would do something for him

43
00:04:24,800 --> 00:04:31,120
if he came and spent a day with me I think I was a part of the bargain I must have talked to

44
00:04:31,120 --> 00:04:36,880
but and this isn't at all related to what we're talking about is it but it's a funny story

45
00:04:37,440 --> 00:04:43,280
and I had him in my room you know I was I was hanging out with this kid all day and I I would

46
00:04:43,280 --> 00:04:47,360
who's doing my thing I was sitting in meditation with him or I was answering emails

47
00:04:47,360 --> 00:04:53,120
whatever I was doing and I think I left the room at someone but his job was to do walking

48
00:04:53,120 --> 00:05:01,520
and sitting walking that kind of work but I think at my point was that that was kind of the

49
00:05:01,520 --> 00:05:07,440
exception when you get to the point was that he had some impetus for doing it was kind of like

50
00:05:07,440 --> 00:05:13,920
the carrot he had a carrot leading him on for most kids there there isn't that

51
00:05:16,320 --> 00:05:19,840
so they bring a whole group of kids to me and say well you know now you have to do this

52
00:05:19,840 --> 00:05:24,800
meditation and they're not really they're not really interested they're not there because

53
00:05:24,800 --> 00:05:28,560
they want to practice they're there because part of the curriculum and that's really tough

54
00:05:29,840 --> 00:05:34,720
so when you've got these math these kids in math class well do any them want to learn math

55
00:05:34,720 --> 00:05:41,200
probably very few that's really the whole problem with the whole problem with the public school

56
00:05:41,200 --> 00:05:50,480
system is that the kids don't want to learn they don't want these they don't care which you know

57
00:05:50,480 --> 00:05:54,800
you you I think you that's maybe the point is you have to address that you have to

58
00:05:56,400 --> 00:06:02,640
you have to instill a sense of care which some parents I think are successful with like fear

59
00:06:02,640 --> 00:06:10,320
like carrots or sticks you know like this whole idea that well if you if you work hard in the

60
00:06:10,320 --> 00:06:14,720
school you can get a job and make lots of money that's what turned me off of school the most is

61
00:06:14,720 --> 00:06:20,880
that I didn't want the job and I didn't want to money I couldn't imagine all my dear sweet

62
00:06:22,720 --> 00:06:29,600
god I don't know what they say what the Buddhists say oh dear me goodness me that's not what I

63
00:06:29,600 --> 00:06:34,320
want that was the most horrific thing in the world to think that I would have to get into the

64
00:06:35,680 --> 00:06:39,440
not the thing that I have to get a job I don't mean it in terms of being lazy it was just

65
00:06:39,440 --> 00:06:48,000
getting stuck that was the whole point but that that's carrot doesn't seem to be that effective

66
00:06:49,920 --> 00:06:55,360
it's effective in keeping kids in school but it doesn't make them want to learn math anymore

67
00:06:55,360 --> 00:07:03,680
so I don't know if this isn't really the original question but I think it's kind of useful

68
00:07:03,680 --> 00:07:09,120
for teachers is to think about that that how do you get them to want to learn math you know how

69
00:07:09,120 --> 00:07:17,680
do you make it something that is of interest to them well you know there's ways of teaching it

70
00:07:17,680 --> 00:07:22,800
that I guess more interesting than others but when you get down to it it's still math

71
00:07:22,800 --> 00:07:29,680
and the hard part is when you do have kids that do are trying to you know understand it and then

72
00:07:29,680 --> 00:07:36,240
you have a different section who are impeding that yeah I would have distractions and stuff and

73
00:07:37,280 --> 00:07:42,480
that's where you get tested a lot too but you want to help because I want to get it and at the

74
00:07:42,480 --> 00:07:51,120
same time you don't want to you know get angry or hurt in any way the ones that don't want to do

75
00:07:51,120 --> 00:08:02,080
it but just thought I'd ask if there was any views on that have a have a meditation

76
00:08:02,080 --> 00:08:08,880
session before class spend the first 10 minutes meditating can't you do that yes I tried that

77
00:08:10,560 --> 00:08:14,880
classes it works better than others but I'll do like two three minutes of just

78
00:08:14,880 --> 00:08:25,920
you know focusing on the the belly or sometimes I'll let them choose a sound maybe

79
00:08:25,920 --> 00:08:34,400
the clap ticking just something to put their mind on hearing one of the senses but again

80
00:08:34,400 --> 00:08:40,480
like you said when the kids really don't want to do it let's see yeah I mean sometimes you can

81
00:08:40,480 --> 00:08:45,040
say you can even I've heard if I remember if I've been teachers saying that you know look I know

82
00:08:45,040 --> 00:08:49,840
you don't you don't want to do it but I have a job to do here I know this is not interesting to you

83
00:08:49,840 --> 00:08:56,240
but can we just you know have an agreement that this is my class and you're not going to disrupt it

84
00:08:56,240 --> 00:09:01,520
kind of thing I don't know somehow reach out to them and make it practical and look this is painful for

85
00:09:01,520 --> 00:09:06,800
you and painful for me but can we not try to get through it together or something I don't know

86
00:09:06,800 --> 00:09:14,960
how do you reach these kids just got to be away but for sure the teachers have a have a the

87
00:09:14,960 --> 00:09:22,640
hardest teacher a teacher is is it's an that's why it makes an excellent precursor to meditation

88
00:09:25,840 --> 00:09:33,040
I don't have it it makes sense but objectively or empirically the observation is that people who

89
00:09:33,040 --> 00:09:38,880
are public school teachers teachers of anything tend to have a much easier time in meditation

90
00:09:38,880 --> 00:09:42,640
because of what they had to put up with them because of the goodness that comes from it

91
00:09:42,640 --> 00:09:50,560
you're really helping people you're changing them much more than any much more than most

92
00:09:50,560 --> 00:09:59,200
professions you're really doing doing good deeds for the kids not just by giving the math but

93
00:09:59,200 --> 00:10:04,320
by being their teacher you're their mentor you're someone you're you're molding them you're

94
00:10:04,320 --> 00:10:15,040
shaping them you're you're helping them cultivate the ability to learn a good teacher and someone

95
00:10:15,040 --> 00:10:20,480
who teaches kids how to learn doesn't just give them information but teaches them how

96
00:10:20,480 --> 00:10:30,480
teaches them some very valuable skills

