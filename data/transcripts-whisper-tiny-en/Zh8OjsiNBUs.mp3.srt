1
00:00:00,000 --> 00:00:07,000
Welcome. Today we're going to give it a tour of the meditation center in monastery.

2
00:00:07,000 --> 00:00:12,000
So, we're starting at the cookie where I'm staying, which is actually a cave.

3
00:00:12,000 --> 00:00:19,000
If you turn over here, you can see. This is where I've been staying.

4
00:00:19,000 --> 00:00:32,000
You can see it's just a shallow cave that's been turned into a cookie.

5
00:00:32,000 --> 00:00:38,000
This is one of our caves. We have three caves too, which we're using.

6
00:00:38,000 --> 00:00:43,000
The third one we have to mix.

7
00:00:43,000 --> 00:00:52,000
So, we'll go down here, I'll take it to the whole place.

8
00:00:52,000 --> 00:01:05,000
We have an assisting who's working in the camera.

9
00:01:05,000 --> 00:01:14,000
This is the Bodhi tree. This is an offspring of the original Bodhi tree in India.

10
00:01:14,000 --> 00:01:21,000
I've taken from no one in another corner.

11
00:01:44,000 --> 00:01:53,000
This is the second cave area.

12
00:01:53,000 --> 00:01:57,000
If you turn over there, you can see.

13
00:01:57,000 --> 00:02:06,000
This is where you can see the third cave, the second cave.

14
00:02:06,000 --> 00:02:19,000
Then we have one meditator, our first meditator is staying in there.

15
00:02:19,000 --> 00:02:46,000
Here's an outhouse, but we can make cookies all along here.

16
00:02:46,000 --> 00:02:57,000
This is an idea here to pick up against the quick fall, we can make cookies.

17
00:03:17,000 --> 00:03:27,000
Now we're down below the place where we're just up there, and we're heading back underneath it.

18
00:03:27,000 --> 00:03:32,000
So, all these cliffs we could actually make cookies in here, but there's a pool in there.

19
00:03:32,000 --> 00:03:55,000
I think there's a big lizard that goes in there.

20
00:03:55,000 --> 00:04:24,000
So, again, if we go up here, we get back to the cave where we started, but I'll take you back to the other way.

21
00:04:24,000 --> 00:04:50,000
So, we're going to go up here, and we're going to go up here, and we're going to go up here.

22
00:04:50,000 --> 00:05:06,000
Now we're heading into the main monastery area.

23
00:05:06,000 --> 00:05:21,000
This is where the kitchen is, and you may have a couple of other things.

24
00:05:21,000 --> 00:05:25,000
Those are new bathrooms being built.

25
00:05:25,000 --> 00:05:41,000
Here is the new kitchen and dining hall that's being built.

26
00:05:41,000 --> 00:05:52,000
There's the old monk who is to run the place.

27
00:05:52,000 --> 00:05:59,000
This is the main hall you can go in there, but sure is not.

28
00:05:59,000 --> 00:06:00,000
This is the main hall.

29
00:06:00,000 --> 00:06:06,000
Right now this is being used as everything, office, meditation hall, kitchen dining room.

30
00:06:06,000 --> 00:06:12,000
So, once that dining room is finished, and another cookie is finished, we'll take everything out of here.

31
00:06:12,000 --> 00:06:23,000
This will be our team just for meditation. It's still small, but it's usable.

32
00:06:23,000 --> 00:06:40,000
Then I'll get your shoes.

33
00:06:40,000 --> 00:06:50,000
So, this is the way, and this is where we're going into the bathroom, on the ground, and into the village, and into the main road, and back to...

34
00:06:50,000 --> 00:07:14,000
Here are two rooms. This is where we want to put cement down.

35
00:07:14,000 --> 00:07:38,000
This is the main hall.

36
00:07:38,000 --> 00:08:02,000
This is the main hall, and this is the main hall.

37
00:08:02,000 --> 00:08:11,000
So, here is the place that was offered.

38
00:08:11,000 --> 00:08:38,000
Here is the new cookie that's being built.

39
00:08:38,000 --> 00:08:48,000
We'll go over there in a second, but I'm going to take it in there.

40
00:08:48,000 --> 00:09:16,000
I'm going to get up there.

41
00:09:16,000 --> 00:09:45,000
Thank you.

42
00:09:45,000 --> 00:10:12,000
So, this piece of property actually doesn't belong to us. It's a little piece of property in between, but at one point we use another pair of uses.

43
00:10:12,000 --> 00:10:36,000
I'm going to clean up the rocks.

44
00:10:36,000 --> 00:10:48,000
I'm going to clean it up.

45
00:10:48,000 --> 00:10:57,000
We're going up.

46
00:10:57,000 --> 00:11:11,000
Okay, but here we are.

47
00:11:11,000 --> 00:11:40,000
This is a rubber forest. It doesn't belong to us.

48
00:11:40,000 --> 00:12:01,000
It's all just forest. This is above the caves. The second cave that I took you to. This is above.

49
00:12:01,000 --> 00:12:12,000
I don't think we can go right to the edge, but we'll go close and then we'll be right above that cave.

50
00:12:12,000 --> 00:12:36,000
Oh, yeah.

51
00:12:36,000 --> 00:13:01,000
I'm potentially it has a nice new view, but you have to clear out all.

52
00:13:01,000 --> 00:13:26,000
That's a little bit. I won't take it for it. It's just part of our monitor.

53
00:13:26,000 --> 00:13:51,000
I'm just going to clean it up.

54
00:13:51,000 --> 00:14:16,000
I'm going to clean it up.

55
00:14:16,000 --> 00:14:41,000
I'm going to clean it up.

56
00:14:41,000 --> 00:15:06,000
I'm going to clean it up.

57
00:15:06,000 --> 00:15:31,000
I'm going to clean it up.

58
00:15:31,000 --> 00:15:56,000
I'm going to clean it up.

59
00:15:56,000 --> 00:16:21,000
I'm going to clean it up.

60
00:16:21,000 --> 00:16:50,000
I'm going to clean it up.

61
00:16:50,000 --> 00:17:15,000
Now we're heading back up to where we started.

62
00:17:15,000 --> 00:17:40,000
This is another cave that I can try and do quickly.

63
00:17:40,000 --> 00:18:05,000
There you have it. That is our monastery and meditation center so far.

64
00:18:05,000 --> 00:18:16,000
Not so far away from the village, the city that it's difficult to get to.

65
00:18:16,000 --> 00:18:26,000
It's actually quite reachable.

66
00:18:26,000 --> 00:18:41,000
I'm going to find peace and solitude to undertake the learning about ourselves.

67
00:18:41,000 --> 00:18:51,000
It's open if people would like to visit.

68
00:18:51,000 --> 00:19:16,000
If you're interested, send us a note and we're going to do our best.

69
00:19:16,000 --> 00:19:26,000
It's not that scary.

70
00:19:26,000 --> 00:19:35,000
It's actually quite civilized.

