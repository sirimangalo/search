1
00:00:00,000 --> 00:00:08,000
The practice of noting, as I understand, it involves thought vocalizing in language the noted phenomenon.

2
00:00:08,000 --> 00:00:19,000
Is there also a noting without vocalizing the phenomenon in thought or is the thought vocalization intrinsic to the practice?

3
00:00:19,000 --> 00:00:31,000
Ask another way, if one observes the phenomenon but bypasses the mental enunciation in language of what is observed, does that qualify as noting?

4
00:00:31,000 --> 00:00:45,000
I think there's a bit of a misunderstanding about the practice.

5
00:00:45,000 --> 00:00:51,000
What's going on in your mind at all times?

6
00:00:51,000 --> 00:00:59,000
When you don't practice, what's going on in your mind?

7
00:00:59,000 --> 00:01:11,000
Are you able to stop thinking for a minute?

8
00:01:11,000 --> 00:01:23,000
And if you were able to stop thinking for a minute, would it be possible for you to give rise to wisdom, for example?

9
00:01:23,000 --> 00:01:32,000
So thinking is an intrinsic part of our mental activity. We're always thinking.

10
00:01:32,000 --> 00:01:38,000
The practice of meditation isn't the creation of a verbal word. You're not verbalizing anything.

11
00:01:38,000 --> 00:01:44,000
You're thinking. But the thought is a clear thought.

12
00:01:44,000 --> 00:01:52,000
The thought is a one-to-one correlation with reality.

13
00:01:52,000 --> 00:01:58,000
When you see something, it's not me, it's not mine, it's not all of the other things that you might think of it.

14
00:01:58,000 --> 00:02:02,000
It's just seeing.

15
00:02:02,000 --> 00:02:09,000
I don't want to sound harsh, but I don't have a lot of sympathy for this sort of question.

16
00:02:09,000 --> 00:02:21,000
I know there's a lot of people, even a lot of very strong meditators who ask this question or who give the answer that you're better off to not use the noting.

17
00:02:21,000 --> 00:02:25,000
And that's fine. I mean, different interpretations and different practices are fine.

18
00:02:25,000 --> 00:02:31,000
I can see you could become enlightened without practicing our way, for sure. There are different ways of practice.

19
00:02:31,000 --> 00:02:38,000
But the reason I think it's not such a useful question is that...

20
00:02:38,000 --> 00:02:43,000
I mean, and I don't mean to sound harsh because obviously you're not the only person to ask this question.

21
00:02:43,000 --> 00:02:46,000
This isn't the first by any means time that I've been asked this question.

22
00:02:46,000 --> 00:03:00,000
Is that when you practice it, to me it's a bit obvious, the benefit of the noting.

23
00:03:00,000 --> 00:03:10,000
And I suppose what happens is that based on people's natural inclination to doubt things,

24
00:03:10,000 --> 00:03:17,000
and natural inclination away from a one-to-one relationship to reality,

25
00:03:17,000 --> 00:03:23,000
and a natural inclination to proliferate.

26
00:03:23,000 --> 00:03:32,000
As soon as you start doing this one-to-one stuff, the mind revolts, the mind rejects it because it's not natural.

27
00:03:32,000 --> 00:03:35,000
This is what people say, but think about what is natural.

28
00:03:35,000 --> 00:03:45,000
Natural is following your defilements. Natural is thinking, this is me, this is mine, this is I am practicing, I am noting, I am walking, I am sitting.

29
00:03:45,000 --> 00:03:51,000
It's the belief that there's an entity, my stomach is moving, my back is straight, maybe there's a pain in the back.

30
00:03:51,000 --> 00:03:56,000
I have to adjust my back and so on. It's all I, I, me, me, mine, mine.

31
00:03:56,000 --> 00:04:06,000
If you were here yesterday, you maybe got a little bit of an earful of what the orthodox Buddhist interpretation of all of this is,

32
00:04:06,000 --> 00:04:12,000
Nick of the Satipatanasut. But it's such a very simple interpretation, it's nothing dogmatic or anything.

33
00:04:12,000 --> 00:04:15,000
It's that all of these things are non-self.

34
00:04:15,000 --> 00:04:24,000
There is no person doing these things. And the, the commentary is very clear that dogs know when they're moving, it says,

35
00:04:24,000 --> 00:04:30,000
but dogs know with views and with delusion, with heavy, heavy delusion.

36
00:04:30,000 --> 00:04:35,000
The knowing that we're trying to give rise to is a very specific type of knowing.

37
00:04:35,000 --> 00:04:51,000
If you were there earlier at our Satipatanasut study sessions, you would know how I explain the words that the Buddha used, like Patisati Mata,

38
00:04:51,000 --> 00:05:01,000
which means for the Satisati Matai, for the purpose of just knowing the object as it is, or remembering the object as it is.

39
00:05:01,000 --> 00:05:06,000
So the Sati, the mindfulness that we're trying to get is a one-to-one relationship.

40
00:05:06,000 --> 00:05:10,000
You, you don't have this in your natural state.

41
00:05:10,000 --> 00:05:16,000
The natural state of observation doesn't, doesn't contain this.

42
00:05:16,000 --> 00:05:21,000
Doesn't, doesn't, isn't comprised of, of this one-to-one relationship.

43
00:05:21,000 --> 00:05:27,000
Even though you say, yes, I know, I know it as walking, I know it as sitting.

44
00:05:27,000 --> 00:05:31,000
You, you know much more than that as well.

45
00:05:31,000 --> 00:05:38,000
You have the inclination to view it as, as you, as yours, and so on.

46
00:05:38,000 --> 00:05:46,000
And, and as a result, what you wind up with is a state of tranquility or calm that, that you identify with.

47
00:05:46,000 --> 00:05:50,000
You wind up in a very good place and you become attached to it.

48
00:05:50,000 --> 00:05:55,000
And you think this is the real self, the true self, the eternal, the permanent self.

49
00:05:55,000 --> 00:05:57,000
And you can wind up in the Brahma world.

50
00:05:57,000 --> 00:06:06,000
I mean, you have to think that just because you're a powerful, strong meditator who's doing great meditation and getting somewhere,

51
00:06:06,000 --> 00:06:08,000
doesn't mean that you're going to become enlightened.

52
00:06:08,000 --> 00:06:14,000
Brahma, Brahma is not enlightened.

53
00:06:14,000 --> 00:06:17,000
There are meditations that lead you to become a Brahma.

54
00:06:17,000 --> 00:06:19,000
And that's not a simple thing to do.

55
00:06:19,000 --> 00:06:22,000
It's not easy to become a Brahma by any means in God.

56
00:06:22,000 --> 00:06:26,000
You know, you can practice meditation to become God.

57
00:06:26,000 --> 00:06:29,000
But from Buddhist point of view that God is not enlightened.

58
00:06:29,000 --> 00:06:37,000
So I think people get the idea that they're able to meditate without this one-to-one relationship,

59
00:06:37,000 --> 00:06:47,000
without training their minds, without developing their minds or fixing the interaction that we have with reality.

60
00:06:47,000 --> 00:06:55,000
And this is my take. Obviously, there are many people who fiercely disagree with me, and that's fine.

61
00:06:55,000 --> 00:07:05,000
But the answer that I would give to this question is that, no, if you're, it doesn't qualify for the meditation as I practice it.

62
00:07:05,000 --> 00:07:10,000
And I don't understand that there would be any reason to stop using the noting.

63
00:07:10,000 --> 00:07:18,000
The reason why people stop it is from my understanding, because it seems unnatural,

64
00:07:18,000 --> 00:07:23,000
because it goes against what their natural inclination is.

65
00:07:23,000 --> 00:07:30,000
And that's the point, really. That's why we're doing it, because our intention is to change our natural inclination

66
00:07:30,000 --> 00:07:34,000
to be one that sees things just as they are.

67
00:07:34,000 --> 00:07:36,000
And that takes a lot of work.

68
00:07:36,000 --> 00:07:40,000
It takes more work than I think most people are aware of.

69
00:07:40,000 --> 00:07:47,000
And that's the real problem. We think that it should be difficult, but we don't realize exactly how difficult.

70
00:07:47,000 --> 00:07:52,000
And we think we're going to learn new things, but we don't realize exactly how new.

71
00:07:52,000 --> 00:07:56,000
We think we're going to be challenged, but we don't realize what a challenge is going to be.

72
00:07:56,000 --> 00:08:02,000
Any expectations you have of what the meditation is going to be and is going to bring you should be shattered.

73
00:08:02,000 --> 00:08:15,000
So even when you think, wow, I'm going to learn something new, you're going to be shocked when you learn the truth.

74
00:08:15,000 --> 00:08:20,000
It should go beyond what your expectations are.

75
00:08:20,000 --> 00:08:30,000
So it's not an easy thing for us to understand, which is why in many ways we have to be willing to go with the technique

76
00:08:30,000 --> 00:08:33,000
for some time before we make some judgment.

77
00:08:33,000 --> 00:08:39,000
And I think if you do that, you'll see that this technique is not from my perspective.

78
00:08:39,000 --> 00:08:49,000
It's a perfectly valid technique that leads people to come to see the truth and to understand things that is there.

79
00:08:49,000 --> 00:09:05,000
I hope that helps.

