1
00:00:00,000 --> 00:00:12,160
Welcome back to Ask a Month. Today's question is on envy and competitiveness. When we acknowledge

2
00:00:12,160 --> 00:00:23,440
the fact that these states are common in the world, how can we hope to remedy them?

3
00:00:23,440 --> 00:00:30,000
So there are two different answers to this question depending on which way you look at it. If you're

4
00:00:30,000 --> 00:00:40,080
talking about remedying it within yourself, then it's a much easier question to answer. If you're

5
00:00:40,080 --> 00:00:47,040
talking about remedying it in the world, then it requires perhaps a little bit of extra explanation.

6
00:00:47,040 --> 00:00:56,560
But I'll try to go through both of these. As for remedying it in ourselves, the remedy for states like

7
00:00:56,560 --> 00:01:12,080
envy and competitiveness are a reassessment of our idea of what is worth, what has intrinsic

8
00:01:12,080 --> 00:01:28,800
worth, what is beneficial. I think too often we mistake the cause or we think of the things that

9
00:01:28,800 --> 00:01:36,400
we are competing for as intrinsically valuable. And so even though we might acknowledge the fact

10
00:01:36,400 --> 00:01:45,680
that envy and competitiveness are negative states, we limit our attempts to mitigate them or to

11
00:01:46,480 --> 00:02:03,040
eradicate them to the suppression or the limiting of our desires. So we agree that the

12
00:02:03,040 --> 00:02:10,240
things that we're competing for are worthwhile, but because everyone wants them, we have to

13
00:02:10,240 --> 00:02:17,760
either compete for them, which we think is wrong, or we have to suppress our desire for them,

14
00:02:17,760 --> 00:02:24,000
and we have to come to some sort of compromise where everyone gets part of what they would

15
00:02:24,000 --> 00:02:37,600
like to what they want. I think this is an unfortunate state of affairs, and it's of course part

16
00:02:37,600 --> 00:02:47,600
of our inability to see things clearly, that we have limited ourselves or our solutions to the

17
00:02:47,600 --> 00:02:57,920
problems of the world by our inability to reevaluate our position or to come out of this

18
00:02:57,920 --> 00:03:12,240
narrow-minded and subjective state of affairs. The Buddha said that when we cling to something,

19
00:03:12,240 --> 00:03:22,720
it's either through our craving for sensuality, for simple sensations, we want to see beautiful

20
00:03:22,720 --> 00:03:28,240
things, we want to hear beautiful sounds, we want to smell good smells, and we want to taste

21
00:03:28,240 --> 00:03:37,600
delicious taste, we want to feel pleasant sensations, and we want to have pleasant ideas or pleasant

22
00:03:37,600 --> 00:03:45,840
thoughts. And this is the first way we can cling to something. The second way we cling to something

23
00:03:45,840 --> 00:03:54,800
is our desire to be something, our desire to attain some abstract notion wanting to be famous,

24
00:03:54,800 --> 00:04:02,480
wanting to be rich, wanting to be powerful, and so on, wanting to be something, or to have some

25
00:04:02,480 --> 00:04:10,800
state of affairs arise. And the third way is by desiring for some state of affairs or some state

26
00:04:10,800 --> 00:04:17,360
to cease, desiring not to be something, I don't want to be this, I don't want to be that,

27
00:04:19,280 --> 00:04:30,240
not wanting to come into contact with certain states, not wanting to be poor, not wanting to be

28
00:04:30,240 --> 00:04:44,800
in this or that state, or have this or that state of affairs to be present. So based on these

29
00:04:44,800 --> 00:04:53,040
three sorts of craving or desire, we come into conflict with others because of course the other

30
00:04:53,040 --> 00:05:00,240
people want the same sorts of things that we want or are also trying to attain the same sorts

31
00:05:00,240 --> 00:05:06,560
of things, we've come to accept that certain sensations, certain experiences are pleasant,

32
00:05:06,560 --> 00:05:16,160
we've come to accept that certain states of being are preferable, being famous, being rich,

33
00:05:16,160 --> 00:05:21,760
being powerful, and so on. And we've come to accept that certain states of affairs are inferior,

34
00:05:21,760 --> 00:05:28,480
certain ways of being like living in a cave, or wearing rags, or so on are inferior, and we want

35
00:05:28,480 --> 00:05:42,640
to be free from these states. So the true way for ourselves to overcome these is to see

36
00:05:42,640 --> 00:05:52,080
through them, to see that the sensations that we have are our experiences of the world are

37
00:05:52,080 --> 00:06:02,400
actually objective. They're neither good or bad, and to be able to see through this partiality

38
00:06:02,400 --> 00:06:07,680
that when we see something, it's merely seeing, when we hear something, it's merely hearing,

39
00:06:07,680 --> 00:06:15,520
when we smell, it's merely smelling. We have to actually overcome this addiction that we have

40
00:06:15,520 --> 00:06:23,120
for sensuality. This is the first way that we overcome these states. The second way,

41
00:06:24,320 --> 00:06:31,040
understanding that there is no intrinsic benefit in a specific state of affairs coming to understand

42
00:06:31,040 --> 00:06:38,560
that being rich has no intrinsic value of being powerful, being famous, being the boss,

43
00:06:38,560 --> 00:06:45,920
or being the head of a company, or the head of a department, or so on, and so on. Having lots of

44
00:06:45,920 --> 00:06:53,760
clothes, having a nice house, having a car, a beautiful car, a beautiful family, having lots of

45
00:06:53,760 --> 00:06:59,760
children, and so on. There's nothing intrinsically positive or negative about these things,

46
00:06:59,760 --> 00:07:06,560
but based on our ability to see through them, we become in tune with reality, and we are

47
00:07:06,560 --> 00:07:14,720
able to accept things as they are. So when other people want certain things, we have no desire

48
00:07:14,720 --> 00:07:20,400
for those things, and we're able to give them up, we're able to let them go. And this has a lot

49
00:07:20,400 --> 00:07:28,640
to do with ego, the ability to give up our ego, not having to be right, not having to be the winner

50
00:07:28,640 --> 00:07:34,560
to be successful, to be victorious, not having to fight with people. So when someone wants something,

51
00:07:35,360 --> 00:07:43,120
we don't have any need to be seen as the victor or to be something. When we're able to give it up,

52
00:07:43,120 --> 00:07:48,240
no matter if people ridicule us or call us a loser or call us a bomb or so on.

53
00:07:50,080 --> 00:07:56,960
These words really have no meaning. We have no desire for other people's praise, we have no desire

54
00:07:56,960 --> 00:08:03,840
for other people's approval, we have no desire for other people's envy or so on. We can see that

55
00:08:03,840 --> 00:08:09,280
these things have no intrinsic value. This also comes about through the practice of meditation.

56
00:08:09,280 --> 00:08:15,120
When you start to see, when you're able to break reality up into pieces and into its ultimate

57
00:08:15,120 --> 00:08:21,920
components, you can see that there is no meaning in being rich or famous or powerful or successful

58
00:08:21,920 --> 00:08:30,080
or so on. Even the praise of other people is simply sound coming from our ears and thoughts

59
00:08:30,080 --> 00:08:36,000
arising in our mind of how people love us and esteem us. And it has no real

60
00:08:37,600 --> 00:08:43,600
elastic effect on our true peace and happiness. It brings no elastic peace.

61
00:08:43,600 --> 00:08:54,080
And the same goes with our desire for things not to be wanting to be free from certain states.

62
00:08:54,080 --> 00:09:01,440
So when we see that there is nothing intrinsically wrong with certain states of affairs like

63
00:09:01,440 --> 00:09:09,200
living in poverty or living in living as nobody or not having education or so on,

64
00:09:09,200 --> 00:09:18,000
any sort of state of affairs that is undesirable. We come to see that it's only undesirable

65
00:09:18,000 --> 00:09:24,160
because of our expectations, because of our partiality. And if we're impartial, then we're able

66
00:09:24,160 --> 00:09:31,360
to live with anything when there are people in our lives that we'd rather be free from when

67
00:09:31,360 --> 00:09:36,480
there are states of affairs when we're sick or so on. We don't feel disturbed by these states

68
00:09:36,480 --> 00:09:43,760
or these people. We're able to live our lives in peace even with difficult situations or

69
00:09:43,760 --> 00:09:51,360
difficult phenomenon, phenomena, things that would cause other people suffering and difficult

70
00:09:51,360 --> 00:10:01,040
things that are accepted by the world to be negative states. We come to see rather that it is

71
00:10:01,040 --> 00:10:07,280
the attachment and the partiality, the anger and the greed and the delusion and the egotism

72
00:10:07,280 --> 00:10:13,520
and so on that are truly negative and we desire to get rid of these. And through getting rid of

73
00:10:13,520 --> 00:10:20,320
these, there's no competition. I suppose some might say and I would probably agree to some extent

74
00:10:20,320 --> 00:10:24,960
that a certain level of competition in this regard is useful. Competing to get rid of the

75
00:10:24,960 --> 00:10:31,280
defilements, it comes in with meditators when people are meditating in a group. They find themselves

76
00:10:31,280 --> 00:10:37,360
wanting to unnecessarily show off, but at least keep up with the group. So you find yourself

77
00:10:37,360 --> 00:10:44,720
sitting straighter and longer and better and easier because of this so-called peer pressure.

78
00:10:45,600 --> 00:10:50,320
There are other people watching you or other people who are going to judge you and so on. And so

79
00:10:50,320 --> 00:10:55,760
to a limited extent it's helpful at least in the beginning. I would say in the long term even that

80
00:10:55,760 --> 00:11:08,400
has to be given up and practice should be done alone specifically because of these difficulties

81
00:11:08,400 --> 00:11:13,520
that when we don't have competition we're lazy and so on. Because it's that laziness and that

82
00:11:13,520 --> 00:11:19,120
inertia that we're trying to overcome and if we're always reliant on a group then we'll never have

83
00:11:19,120 --> 00:11:24,880
a chance to face these states and to overcome them even though it may be helpful in the beginning

84
00:11:24,880 --> 00:11:32,800
to have support. Eventually one should be able to go directly inside and not have funds

85
00:11:34,160 --> 00:11:43,440
instead of mine to be dependent on external phenomena. As for eradicating these sorts of states in

86
00:11:43,440 --> 00:11:52,880
the world well I guess the simple explanation is the by extension the explanation of two people

87
00:11:53,440 --> 00:12:00,320
that of what is really valuable because we've come to value things that are useless that are

88
00:12:00,320 --> 00:12:07,360
meaningless. And we've given up our valuing of things that are useful like meditation,

89
00:12:07,360 --> 00:12:16,160
introspection staying alone and spending time to yourself and so on. We esteem people who are

90
00:12:16,160 --> 00:12:22,880
gregarious outgoing who are fun and exciting and so on and people who are introspective

91
00:12:22,880 --> 00:12:33,360
thoughtful and so on are less esteemed by the masses at any rate. And so I think this sort of paradigm

92
00:12:33,360 --> 00:12:40,960
shift needs to take place where we become less social and more introspective or at least

93
00:12:42,400 --> 00:12:52,160
more in tune with reality which really does put us as an island where even though we might be

94
00:12:52,160 --> 00:12:58,960
involved with other people our relationships with other people are conceptual and there's nothing

95
00:12:58,960 --> 00:13:04,320
wrong with that sort of thing but in an ultimate sense we are alone, we were born alone, we'll

96
00:13:04,320 --> 00:13:12,000
die alone, we live our lives alone, our thoughts are our own and so on to to a great extent

97
00:13:13,200 --> 00:13:19,840
we are solitary beings no matter we might be surrounded by loved ones people who love us and so on

98
00:13:19,840 --> 00:13:26,160
and still be lonely still be feel alone if we're not able to understand and to come to terms

99
00:13:26,160 --> 00:13:34,400
with the nature of our own minds and our own reality so no I think it's obviously a difficult

100
00:13:34,400 --> 00:13:41,280
thing to do and in any way changing society but even changing society comes from within as you

101
00:13:41,280 --> 00:13:47,280
change yourself you become an example for others and the things that you say and the advice that

102
00:13:47,280 --> 00:13:57,040
you give the impression that you give to others is a great support for them to also find the

103
00:13:57,040 --> 00:14:01,600
results and to gain the sorts of results that you've gained through your practice so I hope

104
00:14:01,600 --> 00:14:31,440
that helps thanks for the question of the best.

