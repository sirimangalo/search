1
00:00:30,000 --> 00:00:33,000
.

2
00:00:33,000 --> 00:00:38,000
..

3
00:00:38,000 --> 00:00:39,000
..

4
00:00:39,000 --> 00:00:42,000
..

5
00:00:42,000 --> 00:00:45,000
..

6
00:00:45,000 --> 00:00:49,000
..

7
00:00:49,000 --> 00:00:54,000
...

8
00:00:54,000 --> 00:00:57,000
..

9
00:00:57,000 --> 00:00:58,000
..

10
00:00:58,000 --> 00:01:17,560
Ladies and gentlemen, now before we start to practice, we have to

11
00:01:17,560 --> 00:01:23,440
respect to the Buddha first, and you're just watching or you can follow me just

12
00:01:23,440 --> 00:01:43,740
to the

13
00:02:13,740 --> 00:02:40,140
The first of all, I would like to let you know that peace pay attention.

14
00:02:40,140 --> 00:02:48,740
Actually, before we start to do anything, it is tight tradition or tight culture or it's a Buddhist tradition.

15
00:02:48,740 --> 00:02:51,540
This is the balance of flowers.

16
00:02:51,540 --> 00:02:58,940
Usually, every time before we start to teaching, the Lord Buddha is our headmaster.

17
00:02:58,940 --> 00:03:03,940
I would like to be respectful to Him as our Father.

18
00:03:03,940 --> 00:03:08,740
So before we start to do anything, we have to get respect to Him first.

19
00:03:08,740 --> 00:03:13,740
That is the reason why I have to pay homage to the Buddha first.

20
00:03:13,740 --> 00:03:18,740
And then this is what it is, what it means.

21
00:03:18,740 --> 00:03:28,540
Usually, Thai people, before they start to learn meditation, they have to pay respect to the master or teacher first.

22
00:03:28,540 --> 00:03:34,340
But for all of you, it's not necessary for you to do.

23
00:03:34,340 --> 00:03:44,540
Because of you just coming here and you are not understanding what does it mean, why you have to do.

24
00:03:44,540 --> 00:03:49,340
I just let you know that this is tight tradition, tight culture.

25
00:03:49,340 --> 00:03:58,340
Before we start to learn meditation, we have to take this to the master.

26
00:03:58,340 --> 00:04:06,140
And you might have to give them the precept first. Maybe you don't understand about precepts.

27
00:04:06,140 --> 00:04:14,740
If I tell you that precepts, it is a little bit difficult for you to understand what does it mean precepts.

28
00:04:14,740 --> 00:04:21,840
But if I tell you that five rules is easy for you to understand.

29
00:04:21,840 --> 00:04:29,640
Five rules are five precepts. There are number one, no killing.

30
00:04:29,640 --> 00:04:36,740
Number two, no stealing. Number three, not committing sexual adultery.

31
00:04:36,740 --> 00:04:43,940
Number four, not care lie. Number five, no drinking alcohol.

32
00:04:43,940 --> 00:04:51,740
These are five precepts or five rules, let people or a person like you are should have it to keep it.

33
00:04:51,740 --> 00:05:03,840
You can imagine that if a population of the world takes or five rules or five precepts, this will be peaceful, right?

34
00:05:03,840 --> 00:05:21,940
But it is impossible because of this world, many, many people, many kind of people, the people in this world came from different levels, different position, different situation.

35
00:05:21,940 --> 00:05:31,640
Over here, today, you are coming here to learn about Buddhist meditation, yes or no.

36
00:05:31,640 --> 00:05:39,740
This is recent, you are coming here, right? And you would like to know some about the book, about Buddhism.

37
00:05:39,740 --> 00:05:49,440
You read some? Somebody read the books, about Buddhism? Never? You did or you never did.

38
00:05:49,440 --> 00:06:02,740
And how I would like to let you know that the adoption of the Buddha, there are 84,000 articles, 84,000 main articles.

39
00:06:02,740 --> 00:06:11,140
But it is impossible for me to tell you all of them now.

40
00:06:11,140 --> 00:06:21,840
I just let you know that the adoption of the Buddha, there are 84 main articles, but we can summarize only three.

41
00:06:21,840 --> 00:06:29,840
Number one, you are doing you will or you are not doing bad things. Number one.

42
00:06:29,840 --> 00:06:35,740
And number two, you have to do the cool things or go deep.

43
00:06:35,740 --> 00:06:44,340
Number three, here you find your mind. This is a conversation of the doctrine of the Buddha.

44
00:06:44,340 --> 00:06:55,440
You are coming here, you drive to know more about number three, here you find your mind.

45
00:06:55,440 --> 00:07:01,540
If I tell you that you are not doing you will or bad things, you can understand.

46
00:07:01,540 --> 00:07:08,840
It's easy for you to understand. What does it mean? For example, like you are not killing, you are not stealing,

47
00:07:08,840 --> 00:07:18,840
you are not doing sexual comedy, you are not doing that, you are doing cool things, right?

48
00:07:18,840 --> 00:07:29,740
But here you find your mind, you cannot just reading, you have to practice only one in the world, that is meditation.

49
00:07:29,740 --> 00:07:38,740
So, there is a reason all of you are coming here to learn more, how to clean and create your mind.

50
00:07:38,740 --> 00:07:46,540
Because when you practice meditation, it means you come to clean and create your mind,

51
00:07:46,540 --> 00:07:53,640
clean and create your mind from work, from great hatred, delusion.

52
00:07:53,640 --> 00:08:00,940
I think you are standing with great hatred, delusion, because all of these kill people,

53
00:08:00,940 --> 00:08:05,640
the people kill each other because of these.

54
00:08:05,640 --> 00:08:18,640
So, if you can quit or if you can overcome all of these, it means your mind purify.

55
00:08:18,640 --> 00:08:27,140
How many kinds of meditation in the Buddhism? There are many, many kinds of techniques in the Buddhist meditation,

56
00:08:27,140 --> 00:08:35,640
but we can tell that we can say that they are all the two main kinds, Buddhist meditation, or the two main kinds.

57
00:08:35,640 --> 00:08:46,640
Number one, samata or trangurti meditation. Number one, samata or trangurti meditation.

58
00:08:46,640 --> 00:08:55,640
Number two, vipasana or inside meditation. These are two main kinds.

59
00:08:55,640 --> 00:09:03,640
Over here, we are teaching vipasana or inside meditation.

60
00:09:03,640 --> 00:09:09,640
Even though vipasana or inside meditation, many, many kinds of techniques.

61
00:09:09,640 --> 00:09:17,640
But the techniques, what we are teaching here, namely four foundations of mindfulness.

62
00:09:17,640 --> 00:09:24,640
Four foundations of mindfulness is the name of the vipasana or vipasana or vipasana here.

63
00:09:24,640 --> 00:09:35,640
I will try to explain to you step by step. Four foundations of mindfulness, it means before you start to practice meditation.

64
00:09:35,640 --> 00:09:42,640
You have to try to keep your mind empty, make your mind empty.

65
00:09:42,640 --> 00:09:47,640
It means you don't want to be thinking of past harm.

66
00:09:47,640 --> 00:09:51,640
You don't want to be thinking of future harm.

67
00:09:51,640 --> 00:09:57,640
You try to keep your mindfulness or the present moment to moment.

68
00:09:57,640 --> 00:10:02,640
I think you understand? You don't want to be thinking of past harm.

69
00:10:02,640 --> 00:10:07,640
It means you don't want to think of what you did before.

70
00:10:07,640 --> 00:10:12,640
And you don't want to think of what you are going to do tomorrow or after this.

71
00:10:12,640 --> 00:10:16,640
Just try to keep your mindfulness or the present moment to moment.

72
00:10:16,640 --> 00:10:20,640
All of you, you fixed your eyes on me now, right?

73
00:10:20,640 --> 00:10:26,640
Because you would like to know, you will pay attention to know what I am talking, what I am saying.

74
00:10:26,640 --> 00:10:31,640
This moment you try to understand, what I am saying, what I am talking about.

75
00:10:31,640 --> 00:10:40,640
But if you are not paying attention, if you look around, you are somewhere, you cannot put concentration.

76
00:10:40,640 --> 00:10:45,640
And then you cannot understand what I am talking about, what I am saying, right?

77
00:10:45,640 --> 00:10:49,640
So please try to pay attention.

78
00:10:49,640 --> 00:10:55,640
I try to explain to you about Buddhist meditation clearly.

79
00:10:55,640 --> 00:11:05,640
I told you that there are the meditation, what we are teaching here, namely, for ourations of mindfulness.

80
00:11:05,640 --> 00:11:16,640
It means number one, you have to pay attention and keep your mindfulness on a bodily action.

81
00:11:16,640 --> 00:11:22,640
Bodyly actions, it means every time when you are standing, you know, you are standing.

82
00:11:22,640 --> 00:11:25,640
When you are sitting, you know, you are sitting.

83
00:11:25,640 --> 00:11:32,640
When you are walking or while you are walking, you know, you are walking, this is bodily action.

84
00:11:32,640 --> 00:11:37,640
And number two, contemplation of your feelings or sensations.

85
00:11:37,640 --> 00:11:47,640
It means while you are sitting meditation, especially like now, all of you are sitting cross leg, someone sideways.

86
00:11:47,640 --> 00:11:56,640
But sometimes you feel your legs pain, sometimes you feel your legs numb, sometimes you feel your back pain.

87
00:11:56,640 --> 00:11:59,640
So this is sensations.

88
00:11:59,640 --> 00:12:09,640
So if any kind of sensations happen to you, you try to notice contemplation of your feelings or sensations.

89
00:12:09,640 --> 00:12:19,640
Number three, contemplation of your mind, it means while you are sitting meditation or walking meditation.

90
00:12:19,640 --> 00:12:27,640
Sometimes you think of something or maybe you think of many things in five minutes or ten minutes.

91
00:12:27,640 --> 00:12:37,640
If you are thinking of something or anything, please try to notice thinking, thinking, thinking.

92
00:12:37,640 --> 00:12:41,640
And number four, contemplation of your mind objects.

93
00:12:41,640 --> 00:12:43,640
Mind objects, what does it means?

94
00:12:43,640 --> 00:12:50,640
Mind objects, it means we talk about fine kinds of hindrance.

95
00:12:50,640 --> 00:12:57,640
Number one, sense desires, desires, make many kinds of desires.

96
00:12:57,640 --> 00:13:05,640
For example, like you are underneath money, you need good job, you need good friends, you need to cry, you need to house.

97
00:13:05,640 --> 00:13:09,640
And desires, these are desires.

98
00:13:09,640 --> 00:13:13,640
And number two, anger or angry.

99
00:13:13,640 --> 00:13:17,640
Number three, safety, lazy, proper.

100
00:13:17,640 --> 00:13:21,640
Number four, distracting or distraction.

101
00:13:21,640 --> 00:13:24,640
Number five, doubt.

102
00:13:24,640 --> 00:13:28,640
All of these happen to everybody, more or less.

103
00:13:28,640 --> 00:13:34,640
But if you be happy to everybody, male, female, every nationality is same.

104
00:13:34,640 --> 00:13:46,640
So if all of these happen to you, namely five hindrance, if it is happen to you, you just try to notice.

105
00:13:46,640 --> 00:13:53,640
And please remember that while you are practicing meditation, these kind of techniques,

106
00:13:53,640 --> 00:13:56,640
we will show you how to walk, how to sit.

107
00:13:56,640 --> 00:14:01,640
You have to know walking meditation and sitting meditation.

108
00:14:01,640 --> 00:14:06,640
I will show you first before we start to do.

109
00:14:06,640 --> 00:14:13,640
And I will try to explain to you, when or while you are walking what you should do,

110
00:14:13,640 --> 00:14:15,640
while you are sitting what you should do.

111
00:14:15,640 --> 00:14:25,640
I told you that meditation practice, you try to pay attention,

112
00:14:25,640 --> 00:14:30,640
try to understand what you are doing.

113
00:14:30,640 --> 00:14:39,640
So before you start to practice, you have to try to pay attention and fix your mind inside your body.

114
00:14:39,640 --> 00:14:48,640
Please remember that when you are walking or while you are walking, you fix your mind on your feet.

115
00:14:48,640 --> 00:14:50,640
Fix your mind on your feet.

116
00:14:50,640 --> 00:14:52,640
It means you pay attention.

117
00:14:52,640 --> 00:14:54,640
When you are standing, you know you are standing.

118
00:14:54,640 --> 00:15:01,640
You say inside your mind standing standing standing three times.

119
00:15:01,640 --> 00:15:04,640
You try to understand you are standing now.

120
00:15:04,640 --> 00:15:11,640
And then when you start to walk, every moment you are stepping, you will be aware.

121
00:15:11,640 --> 00:15:18,640
Our awareness mindfulness, it is very, very important for meditators.

122
00:15:18,640 --> 00:15:24,640
Mindfulness means, why every step you are stepping, when you are sleeping, that is,

123
00:15:24,640 --> 00:15:27,640
lifting, you are not lifting.

124
00:15:27,640 --> 00:15:31,640
When you drop a dressing, you say resting.

125
00:15:31,640 --> 00:15:33,640
When you are dropping, you say dropping.

126
00:15:33,640 --> 00:15:35,640
I will show you again.

127
00:15:35,640 --> 00:15:38,640
This is walking meditation.

128
00:15:38,640 --> 00:15:45,640
Please remember that while you are walking, you don't want to close your eyes.

129
00:15:45,640 --> 00:15:50,640
Keep your eyes open and then walking.

130
00:15:50,640 --> 00:15:54,640
But fix your mind on your feet.

131
00:15:54,640 --> 00:15:57,640
Every step you are stepping, you will be aware.

132
00:15:57,640 --> 00:15:59,640
That is walking meditation.

133
00:15:59,640 --> 00:16:03,640
And when you are walking, you do close your hands.

134
00:16:03,640 --> 00:16:09,640
Because if you let your hand free, it is difficult to have full concentration.

135
00:16:09,640 --> 00:16:13,640
So you do close your hands and walking normally.

136
00:16:13,640 --> 00:16:18,640
When you practice meditation, you try to relax.

137
00:16:18,640 --> 00:16:20,640
You don't want to close yourself.

138
00:16:20,640 --> 00:16:22,640
You don't want to press yourself.

139
00:16:22,640 --> 00:16:25,640
Just try to walk in normally.

140
00:16:25,640 --> 00:16:27,640
Remember.

141
00:16:27,640 --> 00:16:33,640
And then after you finish walking, you sit in.

142
00:16:33,640 --> 00:16:35,640
Cross leg, that is like that.

143
00:16:35,640 --> 00:16:36,640
Cross leg.

144
00:16:36,640 --> 00:16:39,640
And then you put your hands like this.

145
00:16:39,640 --> 00:16:46,640
And then move your hand by hand, that is, moving, moving, moving, turning, turning,

146
00:16:46,640 --> 00:16:51,640
turning, lowering, touching, moving, moving, moving, turning, turning, lowering, touching.

147
00:16:51,640 --> 00:16:56,640
That is, after that, keep your body straight.

148
00:16:56,640 --> 00:16:57,640
Straight.

149
00:16:57,640 --> 00:17:02,640
Because if your body is not straight, it is difficult when you're ready.

150
00:17:02,640 --> 00:17:07,640
Because when you are sitting meditation, you have to fix your mind on your abdomen.

151
00:17:07,640 --> 00:17:11,640
When you're breathing in, your abdomen is rising.

152
00:17:11,640 --> 00:17:13,640
You just notice rising.

153
00:17:13,640 --> 00:17:17,640
But when you are breathing out, your abdomen is falling.

154
00:17:17,640 --> 00:17:19,640
You just notice falling.

155
00:17:19,640 --> 00:17:24,640
But this moment, maybe you are thinking of something.

156
00:17:24,640 --> 00:17:29,640
If you're thinking of something, you have to notice suddenly.

157
00:17:29,640 --> 00:17:32,640
Not thinking, thinking, thinking.

158
00:17:32,640 --> 00:17:37,640
Think in three times, or three times, and then come back to your abdomen.

159
00:17:37,640 --> 00:17:45,640
Please remember that while you are sitting meditation, you have to fix your mind on the abdomen only.

160
00:17:45,640 --> 00:17:50,640
If you're thinking of something, you're not thinking, thinking, thinking, three times.

161
00:17:50,640 --> 00:17:53,640
If you hear something, you're not hearing hearing hearing.

162
00:17:53,640 --> 00:17:56,640
Then come back to your abdomen.

163
00:17:56,640 --> 00:18:01,640
Try to sit quite as long as possible.

164
00:18:01,640 --> 00:18:07,640
So after this, we can show you how to walk how to sit.

165
00:18:07,640 --> 00:18:22,640
And please remember, please remember that meditation practice anyone can practice anywhere, any time.

166
00:18:22,640 --> 00:18:29,640
Remember, anyone can practice anywhere, any day, any time.

167
00:18:29,640 --> 00:18:35,640
And maybe you are wondering that what are the purpose of what is meditation?

168
00:18:35,640 --> 00:18:38,640
If you practice meditation, what do we get?

169
00:18:38,640 --> 00:18:39,640
What do we reach?

170
00:18:39,640 --> 00:18:41,640
What are the benefits?

171
00:18:41,640 --> 00:18:44,640
What are the results of what is meditation?

172
00:18:44,640 --> 00:18:52,640
I can tell you that there are five purpose of practice meditation.

173
00:18:52,640 --> 00:19:02,640
Number one, purify your mind.

174
00:19:02,640 --> 00:19:07,640
Number three, hear a physical and mental suffering.

175
00:19:07,640 --> 00:19:12,640
Number four, understand the truth of life.

176
00:19:12,640 --> 00:19:18,640
Number five, they think is suffering and tell you what.

177
00:19:18,640 --> 00:19:21,640
I expect you step by step.

178
00:19:21,640 --> 00:19:23,640
Number one, purify your mind.

179
00:19:23,640 --> 00:19:24,640
What does it mean?

180
00:19:24,640 --> 00:19:30,640
It means when your practice meditation, even one minute, five minutes, ten minutes,

181
00:19:30,640 --> 00:19:36,640
that moment will be purified from free hands with illusion.

182
00:19:36,640 --> 00:19:39,640
That means purify your mind.

183
00:19:39,640 --> 00:19:42,640
Number two, get rid of sorrows and limitations.

184
00:19:42,640 --> 00:19:45,640
I think you can understand them.

185
00:19:45,640 --> 00:19:50,640
Number three, get rid of physical and mental suffering.

186
00:19:50,640 --> 00:19:55,640
Number four, understand the truth of life.

187
00:19:55,640 --> 00:20:00,640
The truth of life, it means when you practice meditation,

188
00:20:00,640 --> 00:20:08,640
you can understand yourself that every moment your body always changes.

189
00:20:08,640 --> 00:20:13,640
Everything impermanent is always changed, even your breathing.

190
00:20:13,640 --> 00:20:17,640
You're breathing every moment, but never smoothly.

191
00:20:17,640 --> 00:20:20,640
Sometimes it's slow, sometimes it's fast.

192
00:20:20,640 --> 00:20:24,640
Sometimes it's deep, sometimes it's narrow, for example.

193
00:20:24,640 --> 00:20:26,640
So it's always changed.

194
00:20:26,640 --> 00:20:33,640
Even your body, your always changed, your hair, your hands always changed.

195
00:20:33,640 --> 00:20:37,640
It's longer and longer every moment, but you don't see it, you can see that.

196
00:20:37,640 --> 00:20:45,640
You're stiff, your face, everything changed all the time, but you don't know.

197
00:20:45,640 --> 00:20:49,640
But if you practice meditation, you can see that.

198
00:20:49,640 --> 00:20:53,640
When you're breathing in, breathing out is always changed.

199
00:20:53,640 --> 00:20:58,640
Sometimes it's far, sometimes it's slow, sometimes it's deep, for example.

200
00:20:58,640 --> 00:21:04,640
Number five, it's thing is suffering and it's near new water.

201
00:21:04,640 --> 00:21:11,640
And sometimes you feel you have destruction.

202
00:21:11,640 --> 00:21:16,640
If you have destruction, it means you're thinking too much, thinking of many things.

203
00:21:16,640 --> 00:21:19,640
You try to test your thought, as soon as possible.

204
00:21:19,640 --> 00:21:27,640
If you have doubt, you try to test your thought and note it, doubt, doubt, doubt.

205
00:21:27,640 --> 00:21:32,640
All of these, you have to try to understand.

206
00:21:32,640 --> 00:21:37,640
Now I give you instruction and explaining to you about 30 minutes already.

207
00:21:37,640 --> 00:21:47,640
And then now we start to walk, start to walk about 15 minutes and then sitting.

