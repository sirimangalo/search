1
00:00:00,000 --> 00:00:05,360
I lived in a western Tibetan Buddhist monastery for a while where I came across many

2
00:00:05,360 --> 00:00:10,800
contradictions in my opinion. A few examples, eating more than two meals a day, eating cake

3
00:00:10,800 --> 00:00:17,920
and candy, etc. Praying to Ganesh, Ganapadi. Many superstitions also lack of meditation

4
00:00:17,920 --> 00:00:24,320
practice. Do you see some of these examples also in the teravada tradition? I think this

5
00:00:24,320 --> 00:00:31,240
is a really good example of what I was explaining before as far as the difference between

6
00:00:31,240 --> 00:00:37,320
teravada, Tibetan, whatever you want to call Western. There isn't one. All of those things

7
00:00:37,320 --> 00:00:45,960
that you described could have taken place in any of thousands, maybe that's over hundreds,

8
00:00:45,960 --> 00:00:54,360
and probably thousands of teravada Buddhist monasteries. Thousands, it's got to be thousands.

9
00:00:54,360 --> 00:00:57,720
Eating more than two meals a day, check. Eating cake and candy, I assume you mean in the

10
00:00:57,720 --> 00:01:04,040
evening, check. Praying to Ganeshak, check. Many superstitions, check. Lack of meditation

11
00:01:04,040 --> 00:01:09,720
practice, check. The tradition is not going to save you. Calling yourself Tibetan Buddhist,

12
00:01:09,720 --> 00:01:14,560
calling yourself to Rada Buddhist, calling yourself Thai Buddhist, Cambodian Buddhist,

13
00:01:14,560 --> 00:01:20,240
Chinese Buddhist, Pure Land Buddhist, whatever. It all depends on the teacher and whether

14
00:01:20,240 --> 00:01:30,720
they are teaching what is of truly a benefit, which is truly of ultimate importance and

15
00:01:30,720 --> 00:01:36,640
ultimate significance and ultimately real that they teach the ultimate truth and teach

16
00:01:36,640 --> 00:01:47,000
the realization of what is ultimately true, which is, as we understand it, the truth

17
00:01:47,000 --> 00:01:51,640
of suffering, the cause of suffering, the truth of the cause of suffering, the truth of the

18
00:01:51,640 --> 00:01:56,760
cessation of suffering, and the truth of the path which leads to the cessation of suffering.

19
00:01:56,760 --> 00:02:02,960
If they are able to explain these things, practice them themselves and are able to teach

20
00:02:02,960 --> 00:02:09,720
you how to practice them yourself, then that is somewhere something you should follow.

21
00:02:09,720 --> 00:02:14,520
But you have a very simple question. Yes, these examples also exist in what we call the

22
00:02:14,520 --> 00:02:25,840
terror about the tradition. With the qualifier that what we mean, you have to be careful

23
00:02:25,840 --> 00:02:33,520
because it is actually mixing two things. Western Tibetan Buddhist monastery and

24
00:02:33,520 --> 00:02:41,760
teravada are not on the same level. Teravada means the doctrine of the elders, a specific

25
00:02:41,760 --> 00:02:50,000
group of elders, who we understand are responsible for what we call the teravada commentaries,

26
00:02:50,000 --> 00:02:57,280
which are explanations of the polycanon, which we understand to be the Buddhist teachings.

27
00:02:57,280 --> 00:03:02,880
You won't find anything in there in regards to eating more than two meals a day, eating

28
00:03:02,880 --> 00:03:08,680
cake and candy in the evening. If you eat in the morning, it's not, I don't see the problem.

29
00:03:08,680 --> 00:03:12,240
Or if you're a lay person and you eat cake and candy in the evening, then there's still

30
00:03:12,240 --> 00:03:18,880
no problem. But you won't see anything about praying about any of this. So in the teravada

31
00:03:18,880 --> 00:03:25,040
tradition, you won't see any of that. But in countries that claim to be following the

32
00:03:25,040 --> 00:03:31,520
teravada tradition, you will find problems like that. Now, in various Tibetan traditions

33
00:03:31,520 --> 00:03:37,120
that existed in Tibet, or exist in what is now called Tibetan Buddhism, in many of those

34
00:03:37,120 --> 00:03:44,800
traditions, there is similar strict adherence to monastic discipline. And so in those traditions,

35
00:03:44,800 --> 00:03:50,240
you also wouldn't find these things. So what I'm talking about, these teravada monasteries,

36
00:03:50,240 --> 00:03:55,520
I'm actually referring to Thai monasteries, Sri Lankan monasteries, Cambodian monasteries,

37
00:03:55,520 --> 00:03:59,760
Burmese monasteries, the monasteries that claim to be following the teravada, but in fact

38
00:03:59,760 --> 00:04:06,920
are not. In these monasteries, you will find all of these things and far, far worse, you

39
00:04:06,920 --> 00:04:17,240
will find horrible things going on. There are cases of child abuse, there are lots of

40
00:04:17,240 --> 00:04:23,000
horrible things that go on. And kind of horrific when you think about it, but better that

41
00:04:23,000 --> 00:04:28,480
we know about it, better that we're clear and we admit to these things instead of pretending

42
00:04:28,480 --> 00:04:35,360
it doesn't exist and hiding it away, like we see in, for example, the Catholic Church,

43
00:04:35,360 --> 00:04:42,600
as in the end, it comes out and causes horrible misunderstanding of the core teachings. People

44
00:04:42,600 --> 00:04:49,960
start to think that Buddhists approve of these sort of things, not simply eating in the

45
00:04:49,960 --> 00:05:02,760
evening, but doing all sorts of bad things, gambling, sexuality, I mean there was this article

46
00:05:02,760 --> 00:05:08,000
I read about a Buddhist dating service, for example, and people begin to think that that's

47
00:05:08,000 --> 00:05:12,800
what we approve of that if we start to, if we don't speak out and say this is wrong, this

48
00:05:12,800 --> 00:05:21,600
is not appropriate for a Buddhist monastic to date other Buddhist monastics, for example,

49
00:05:21,600 --> 00:05:37,520
for child abuse and all sorts of unpleasant things.

