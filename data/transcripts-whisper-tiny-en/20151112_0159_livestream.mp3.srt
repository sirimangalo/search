1
00:00:00,000 --> 00:00:08,640
Good evening everyone broadcasting live November 11th so it's an

2
00:00:08,640 --> 00:00:14,440
especially Buddhist day to day or is that only in Canada is it a special day in

3
00:00:14,440 --> 00:00:19,980
America as well. Today's Veterans Day in America. Well you call it Veterans Day

4
00:00:19,980 --> 00:00:25,560
well we have a better name for it a very Buddhist name. Today is Remembrance Day

5
00:00:25,560 --> 00:00:39,120
okay and I mean it's to remember Veterans but yeah probably shouldn't steal

6
00:00:39,120 --> 00:00:47,400
it but it's in remembrance is a good word it is good to remember not to forget

7
00:00:47,400 --> 00:00:52,880
Buddhism it has a bit of a different connotation. Okay we're doing

8
00:00:52,880 --> 00:01:15,960
Namapada and we have to turn some stuff on. Test. Mike is good. Using the

9
00:01:15,960 --> 00:01:22,760
old mic the one that failed on me before because the new one's failing now.

10
00:01:22,760 --> 00:01:27,280
So tomorrow we're going to have a different layout. I think we're going to move

11
00:01:27,280 --> 00:01:33,200
into the outer room which has a nicer, nicer, Robbins idea that we move into

12
00:01:33,200 --> 00:02:02,080
the outer room. So we don't have to use this fake white

13
00:02:04,160 --> 00:02:13,940
Okay ready. Hello everyone and welcome back to our study of the Namapada. Today

14
00:02:13,940 --> 00:02:42,000
we continue on with first number 108 which reads as follows. It's actually a bit

15
00:02:42,000 --> 00:02:48,120
difficult because the grammar doesn't really work in English. So whatever

16
00:02:48,120 --> 00:02:58,640
whatever sacrifice or worship there is in the world. If for 100 years

17
00:02:58,640 --> 00:03:05,760
San Guacherang Yaje Tipunyapiko one desiring Bungya desiring merit should perform

18
00:03:05,760 --> 00:03:12,320
such as sacrifice or such a worship and yes it's actually a very very similar

19
00:03:12,320 --> 00:03:18,340
verse and story the the last two.

20
00:03:18,340 --> 00:03:24,880
Sabampitang Ttubagameti it doesn't come to a quarter of the value. All of that

21
00:03:24,880 --> 00:03:29,760
doesn't come to a quarter of the value. The hundred years of sacrifice or

22
00:03:29,760 --> 00:03:41,000
worship that one pays. Abiwadana Ujugate Sussayo for the greater in comparison to

23
00:03:41,000 --> 00:03:50,040
the greater Abiwadana or respect or reverence that one pays to and today our

24
00:03:50,040 --> 00:03:56,600
word is Ujugate. In regards to those who are Ujugate gone to or become

25
00:03:56,600 --> 00:04:07,760
straight those who have gained rectitude moral or ethical or mental straightness

26
00:04:07,760 --> 00:04:18,040
uprightness those who are upright. So same story we have Saripudad and it's

27
00:04:18,040 --> 00:04:25,000
it's becoming apparent that he was really instrumental in helping a lot of his

28
00:04:25,000 --> 00:04:33,400
relatives and now his friends become enlightened. So now we have Saripudad's

29
00:04:33,400 --> 00:04:40,360
friend Sahaya Kambramana in Brahmin who was a friend of Saripudad.

30
00:04:40,360 --> 00:04:46,640
Prandasaripudas from before and so Saripudad went to him and asked him do you

31
00:04:46,640 --> 00:04:52,560
do any good deeds and he says oh yes what do you do? Oh I offer sacrificial

32
00:04:52,560 --> 00:04:59,160
slaughter and of course Saripudad doesn't see that as being a wholesome deed.

33
00:04:59,160 --> 00:05:05,760
This is about the really the crux of these three stories is the idea that

34
00:05:06,040 --> 00:05:16,640
good deeds our concept of good deeds is often quite misled and this even

35
00:05:16,640 --> 00:05:21,120
occurs in Buddhism you'll see Buddhists performing deeds that they think are

36
00:05:21,120 --> 00:05:32,960
good deeds when in fact they are perhaps useless. So the case of I've told

37
00:05:32,960 --> 00:05:42,320
the story before but once we had a Katina ceremony where I was staying and all

38
00:05:42,320 --> 00:05:48,160
the villagers came together to do the Katina ceremony and so what they did is

39
00:05:48,160 --> 00:05:53,840
they had this very important I was in the in a rural area so it was there

40
00:05:53,840 --> 00:05:59,140
where people didn't have a lot of money and there was one man who had built a

41
00:05:59,140 --> 00:06:04,840
house in the area from Bangkok and he was quite rich and so he got his rich

42
00:06:04,840 --> 00:06:09,440
friends from Bangkok to come up and they put together a bunch of money and

43
00:06:09,440 --> 00:06:12,080
they put the money up on a tree and they brought this money tree to the

44
00:06:12,080 --> 00:06:17,440
monastery and they wanted to do a Katina ceremony. Well it turns out half of them

45
00:06:17,440 --> 00:06:25,800
were drunk they had been drinking and and the funny the funniest part was

46
00:06:25,800 --> 00:06:30,440
and what sort of made it a bit ridiculous was that they didn't have a single

47
00:06:30,440 --> 00:06:35,720
robe with what just to do the Katina so they brought this money tree thinking

48
00:06:35,720 --> 00:06:38,640
well that's Katina Katina has a money tree because that's really when it's

49
00:06:38,640 --> 00:06:43,400
become it's all about money trees trees that have are literally the made of

50
00:06:43,400 --> 00:06:50,440
money the leaves are are folded up bits of money or just money sticking up on

51
00:06:50,440 --> 00:06:58,360
sticks and so I said okay already to Katina what do we do and I said well

52
00:06:58,360 --> 00:07:05,800
where's the robe and they didn't have a robe so I said well can we borrow one of

53
00:07:05,800 --> 00:07:10,840
yours and I said okay I took off I actually took off anyway and I want to

54
00:07:10,840 --> 00:07:18,560
talk about it I'm sure it wasn't yeah you know point being not really what I

55
00:07:18,560 --> 00:07:24,080
would call wholesome engagement especially considering the alcohol involved

56
00:07:24,080 --> 00:07:29,880
and the fact that after it was all over they took the money back and on some

57
00:07:29,880 --> 00:07:32,880
of the half of the gifts that were supposed to go to the monastery they took

58
00:07:32,880 --> 00:07:40,440
back for the village when it was really there was a bit of a system that they

59
00:07:40,440 --> 00:07:46,280
had set up and yeah this happens in Buddhist circles sometimes where we lose

60
00:07:46,280 --> 00:07:49,980
sight of why are we doing this I think what we're doing it left and they're

61
00:07:49,980 --> 00:07:55,640
doing it just because they're they're excited to have their village and

62
00:07:55,640 --> 00:08:04,640
their community grow so the money goes to the community I think the stuff

63
00:08:04,640 --> 00:08:12,880
goes for the community and they don't realize the great benefit to actually

64
00:08:12,880 --> 00:08:16,760
giving a gift and in fact they don't really they're really interested in

65
00:08:16,760 --> 00:08:20,760
giving gifts on the other hand there was that that very same one of the same

66
00:08:20,760 --> 00:08:28,720
village ended up being quite awesome about giving arms they had another

67
00:08:28,720 --> 00:08:34,000
funny aspect of being there I was staying with an old monk two old monks so

68
00:08:34,000 --> 00:08:36,960
this was later when I was staying with a second old monk but I stayed with a

69
00:08:36,960 --> 00:08:47,120
first old monk deeper in the forest and he told me when you go on he said well

70
00:08:47,120 --> 00:08:50,720
you can go on arms around to the village but you won't get anything you might

71
00:08:50,720 --> 00:08:57,040
get some some dried noodles or or canned fish but that's about it won't be

72
00:08:57,040 --> 00:09:01,120
enough to eat but you can go and it was three and a half kilometers walk along a

73
00:09:01,120 --> 00:09:07,520
dirt gravel road that was actually somewhat painful to walk but I did walk

74
00:09:07,520 --> 00:09:11,680
three and a half kilometers and often had to walk three and a half kilometers

75
00:09:11,680 --> 00:09:16,680
back but not always sometimes someone going to the park there was a national

76
00:09:16,680 --> 00:09:21,880
park near where the monastery was it would give us a ride back or give me

77
00:09:21,880 --> 00:09:31,080
or eventually us when I had followers but but the great thing once I went

78
00:09:31,080 --> 00:09:38,040
and they could see that I was interested in meditation and wasn't some

79
00:09:38,040 --> 00:09:46,920
crabby old monk they they were really keen on and eventually the whole village

80
00:09:46,920 --> 00:09:51,000
began to give and and they really got the sense of the greatness people who

81
00:09:51,000 --> 00:09:54,360
who I think had never really gotten into it and there was one man who was a

82
00:09:54,360 --> 00:09:58,240
really devout Buddhist and very interested in the Dhamma and very knowledgeable

83
00:09:58,240 --> 00:10:02,080
about the Dhamma and we used to talk about the Dhamma and he was really impressed

84
00:10:02,080 --> 00:10:05,960
and he's you know it's a great thing that you know people who I've never seen

85
00:10:05,960 --> 00:10:11,840
these people give before are now into giving so so there was that but that

86
00:10:11,840 --> 00:10:14,720
is really what's it what it's about it's the difference this this

87
00:10:14,720 --> 00:10:18,920
versus are about the difference between spiritual practices we have this

88
00:10:18,920 --> 00:10:23,760
idea of the spiritual practice in in many Buddhist cultures of paying respect

89
00:10:23,760 --> 00:10:28,440
to gods or angels and they've started making up angels it's they've taken

90
00:10:28,440 --> 00:10:34,720
taking them from Hindu myths like Ganesha people worship this this monkey I

91
00:10:34,720 --> 00:10:41,720
know sorry the elephant god and they have all these myths about Ganesha and

92
00:10:41,720 --> 00:10:47,920
Hanuman in this same village there was a woman who's who claimed to be a avatar

93
00:10:47,920 --> 00:10:55,240
an avatar is a big thing in in Thailand and in Thailand anyway an avatar for

94
00:10:55,240 --> 00:11:01,280
Hanuman and of course Hanuman is this legend it's a story really I mean there's

95
00:11:01,280 --> 00:11:05,840
no there's no even there's not even any ancient religious texts I think that

96
00:11:05,840 --> 00:11:10,560
have Hanuman in them it's this tale of the Ramayana which it's a fairly modern

97
00:11:10,560 --> 00:11:15,440
tale and yet in India as well they worship Hanuman or they seem to they have

98
00:11:15,440 --> 00:11:20,720
monkeys monkeys statues in in front of their fields to ward away evil demons and

99
00:11:20,720 --> 00:11:29,240
stuff but yeah these sort of things not worth a quarter of the he says

100
00:11:29,240 --> 00:11:38,120
hmm not a not a fourth part not just to bhagami it's a to bhagami they don't

101
00:11:38,120 --> 00:11:40,680
come to a quarter they don't even come to a thousandth part as we were

102
00:11:40,680 --> 00:11:46,160
talking about in the earlier one anyway so he tells him this it's a little bit

103
00:11:46,160 --> 00:11:54,000
different he takes him he says he says look you have to come to to see the

104
00:11:54,000 --> 00:11:57,800
teacher and he takes him to see the teacher in this time he actually says one

105
00:11:57,800 --> 00:12:02,920
tape please tell this man the way to the Brahma world and the Buddha but the

106
00:12:02,920 --> 00:12:06,800
Buddha says the same thing Buddha asks him what he does and he says you could do

107
00:12:06,800 --> 00:12:14,600
that for a year yet it would be not be worth the smallest piece of offering and

108
00:12:14,600 --> 00:12:19,600
and he here he says something also a bit different

109
00:12:19,600 --> 00:12:31,960
Lokeyam Mahajana Sadinadana so if you if you were to give if you were

110
00:12:31,960 --> 00:12:38,800
instead to give to the great to the populace you know if you're instead to give

111
00:12:38,800 --> 00:12:44,240
to to poor people for example that would be that was one thing would be a

112
00:12:44,240 --> 00:12:48,880
greater value so he distinguishes there and it's an interesting point because

113
00:12:48,880 --> 00:12:54,800
here the Buddha is sort of seems to be advocating giving of arms I'm I'd have

114
00:12:54,800 --> 00:12:57,480
to that's what the English translation says and that looks like what the

115
00:12:57,480 --> 00:13:05,480
Poly says but it's something to do with giving giving gifts to worldly people

116
00:13:05,480 --> 00:13:09,560
it's much better because he's slaughtering animals which of course is actually

117
00:13:09,560 --> 00:13:19,520
unwholesome but then he he goes on to saying persona jithyana mama sahvakana

118
00:13:19,520 --> 00:13:27,520
to pay respect to my disciples the Buddha says

119
00:13:30,920 --> 00:13:37,720
yeah the the Kuzana Jita the the wholesome mind that comes is far more powerful

120
00:13:37,720 --> 00:13:43,360
I mean I think he's quite understanding the truth here because honestly

121
00:13:43,360 --> 00:13:50,160
sacrificing animals it's not only a portion it's not only a fraction as good

122
00:13:50,160 --> 00:13:53,680
it's actually harmful there's nothing good about offering slaughtered

123
00:13:53,680 --> 00:13:58,680
and with a slaughtering animals as an offering so the Buddha as I think being

124
00:13:58,680 --> 00:14:02,760
a little bit going a little bit easy on him because probably if you if you were

125
00:14:02,760 --> 00:14:07,920
to say you're actually that's actually an unwholesome thing then they wouldn't

126
00:14:07,920 --> 00:14:13,440
perhaps set the guy off and make him upset at the Buddha so he couches it a bit

127
00:14:13,440 --> 00:14:20,440
nicer he he doesn't say how much less good it is than a quarter but it's at

128
00:14:20,440 --> 00:14:26,240
least not it's less than a quarter is good it's actually harmful and that's

129
00:14:26,240 --> 00:14:32,600
a true of many spiritual practices so again as I've talked about we have to

130
00:14:32,600 --> 00:14:37,000
be aware of the true benefit of our spiritual practices we can't just think

131
00:14:37,000 --> 00:14:42,080
I'm doing this and you know this is what people tell me to do this is

132
00:14:42,080 --> 00:14:45,920
spiritual and therefore it's good spiritual practice can actually be

133
00:14:45,920 --> 00:14:51,560
harmful you know this was a spiritual practice that obviously was offering

134
00:14:51,560 --> 00:14:58,240
killing animals in the name of God that kind of thing sometimes we take people

135
00:14:58,240 --> 00:15:03,320
have have the idea that taking drugs is spiritual and I'm not gonna go out and

136
00:15:03,320 --> 00:15:08,480
say that that's harmful but my suspicion is that it has a great potential for

137
00:15:08,480 --> 00:15:12,320
harm if not being outright harmful I mean I think a lot of people would say

138
00:15:12,320 --> 00:15:16,880
that taking even psychedelic drugs and hoping that it somehow is beneficial as a

139
00:15:16,880 --> 00:15:21,120
spiritual practice I think people some people would say that that is outright

140
00:15:21,120 --> 00:15:25,720
harmful I'm not sure that I go quite so far because there is an argument to be

141
00:15:25,720 --> 00:15:31,240
made that it opens up your mind to alternate states of reality but I think there's

142
00:15:31,240 --> 00:15:36,160
a lot of delusion involved like the idea that those states have some meaning or

143
00:15:36,160 --> 00:15:41,520
purpose or value when in fact they're all messed up with delusion and you know

144
00:15:41,520 --> 00:15:47,240
I've been there done that and it's just all it's just all it's a mess of our

145
00:15:47,240 --> 00:15:52,840
mind you know what our mind can come up with when it's when it's doped up and

146
00:15:52,840 --> 00:16:01,440
it's high so these kind of things and and you compare that for example

147
00:16:01,440 --> 00:16:06,040
taking taking drugs as a spiritual practice to a Buddhist spiritual practice of

148
00:16:06,040 --> 00:16:09,880
like a giving charity or of even just holding your hands up and paying respect

149
00:16:09,880 --> 00:16:14,360
to someone who's worthy of respect why would you know who would think of that

150
00:16:14,360 --> 00:16:20,120
it shows how far we many people many people are from true spiritual practice

151
00:16:20,120 --> 00:16:25,800
maybe people say oh taking drugs that's a spiritual practice or you know

152
00:16:25,800 --> 00:16:32,240
doing expansive rituals or offering copious sacrifices or this kind of thing

153
00:16:32,240 --> 00:16:38,960
you know doing very complex or very mystical things maybe dancing people say in the

154
00:16:38,960 --> 00:16:43,400
West we become very hedonistic with our spirituality so maybe people say group

155
00:16:43,400 --> 00:16:47,680
orgies and karmic sex and that kind of thing these are spiritual come

156
00:16:47,680 --> 00:16:52,000
contrast that to the Buddha's idea of what true spirituality is holding your hands

157
00:16:52,000 --> 00:16:58,040
up and paying respect that is far more spiritual so such a mundane seeming

158
00:16:58,040 --> 00:17:04,480
thing but far more spiritual far more beneficial even just for a moment and a

159
00:17:04,480 --> 00:17:11,280
hundred years of dancing spiritual dancing or spiritual music or spiritual

160
00:17:11,280 --> 00:17:23,280
sex drugs you know all these things so for someone looking for punya which we

161
00:17:23,280 --> 00:17:28,200
all are punya is goodness even if we're just set on meditation it's important

162
00:17:28,200 --> 00:17:35,120
not to become self self centered you know in the sense of me me me I want to

163
00:17:35,120 --> 00:17:39,560
become spiritually enlightened you know enlightenment is about letting go

164
00:17:39,560 --> 00:17:44,880
it's a lot of self sacrifice and enlightened being would be able to give up

165
00:17:44,880 --> 00:17:50,200
anything if someone asks them for something they would have no sense of self

166
00:17:50,200 --> 00:17:55,600
you know of helping themselves you have no qualms there and they consider what

167
00:17:55,600 --> 00:18:00,240
is proper to do and they do it without any attachment without any greed and

168
00:18:00,240 --> 00:18:04,840
they're very respectful they honor those who have helped them they're grateful

169
00:18:04,840 --> 00:18:09,400
to those who have helped them that kind of thing and they're very down to earth so

170
00:18:09,400 --> 00:18:16,360
noble spirituality is like this with spiritual well paying respect to those

171
00:18:16,360 --> 00:18:21,080
who are worthy of respect giving gifts to those who are worthy of gifts and mostly

172
00:18:21,080 --> 00:18:26,520
just being aware of being here being present being with mundane reality

173
00:18:26,520 --> 00:18:33,320
understanding truth true reality and not being concerned with mysticism or

174
00:18:33,320 --> 00:18:37,840
altered states of existence or that kind of thing astral travel anyone who

175
00:18:37,840 --> 00:18:41,840
subsessed with astral travel I mean it's not to say enlightened people can't

176
00:18:41,840 --> 00:18:48,600
practice it but people who are striving for that are really missing the point

177
00:18:48,600 --> 00:18:55,000
Mahasi Sayyada tells a story of a woman who he knew who was working very hard

178
00:18:55,000 --> 00:19:00,440
to try and see out of her ears this was she was trying to gain the spiritual

179
00:19:00,440 --> 00:19:07,360
ability to see out of her ears and he said and this one her eye is worked

180
00:19:07,360 --> 00:19:14,200
perfectly fine so we get often on the wrong path true spirituality is hard

181
00:19:14,200 --> 00:19:22,320
is often easy to mistake for ordinary reality I would say that enlightened

182
00:19:22,320 --> 00:19:26,320
people are what we think we are we all think of ourselves as living normally

183
00:19:26,320 --> 00:19:32,680
we think ourselves as normal but we're not our defilements take us away from

184
00:19:32,680 --> 00:19:39,120
what we think we are take us away from mundane reality so I've made much of

185
00:19:39,120 --> 00:19:43,160
this very simple verse which is very much like the other two verses and I

186
00:19:43,160 --> 00:19:47,960
promise the next one is different still still along the same vein but it's a

187
00:19:47,960 --> 00:19:53,600
totally different context and story everything and it's a very actually one of

188
00:19:53,600 --> 00:20:01,600
them probably the if not one of one of the if not the most famous verses in

189
00:20:01,600 --> 00:20:09,600
the double pad coming up next next time and then after that we have the story

190
00:20:09,600 --> 00:20:14,960
of Sunkitsha which is a very nice story and so on and so on not all the stories

191
00:20:14,960 --> 00:20:18,720
are long someone were very short as you can see some of them are very long I

192
00:20:18,720 --> 00:20:24,320
think the longest ones have already gone by so we're not going to have great

193
00:20:24,320 --> 00:20:31,880
storytelling not all of it is going to be great stories but always something

194
00:20:31,880 --> 00:20:36,000
new and some new message to be found in the Dhamapanda and of course the

195
00:20:36,000 --> 00:20:41,520
verses themselves are things you can reflect upon and what is important this is

196
00:20:41,520 --> 00:20:45,800
the Sahasuaga which is a thousand so it's contrasting single things to

197
00:20:45,800 --> 00:20:50,640
thousands of things for the most part a thousand of something useless better

198
00:20:50,640 --> 00:20:57,960
is one thing that is useful so that's the Dhamapanda for tonight thank you

199
00:20:57,960 --> 00:21:04,600
all for tuning in keep practicing and be well

200
00:21:04,600 --> 00:21:13,960
okay hopefully that turned out

201
00:21:16,120 --> 00:21:20,280
now I have to turn some things off

202
00:21:20,280 --> 00:21:34,600
it's like I had it on mute just asked me if I wanted to turn mute off I wonder

203
00:21:34,600 --> 00:21:38,760
if I had it on mute the whole time I wouldn't have to be funny no the comments

204
00:21:38,760 --> 00:21:43,000
from the chat panel are that the audio is great but the camera did go out of

205
00:21:43,000 --> 00:21:48,200
focus a little bit on the that audio is different from this audio the audio

206
00:21:48,200 --> 00:21:53,440
is you think I don't think it was what I couldn't have been on mute but that would be

207
00:21:53,440 --> 00:22:21,080
funny if it were

208
00:22:21,080 --> 00:22:27,960
You know, I could do, for the dumbapada portion, I don't really have to record a video.

209
00:22:27,960 --> 00:22:35,200
We could just do the audio stream for that part and start doing the live hangout after

210
00:22:35,200 --> 00:22:36,640
I do the dumbapada.

211
00:22:36,640 --> 00:22:42,440
Because, I mean, my record, then otherwise I have it recorded on YouTube twice.

212
00:22:42,440 --> 00:22:48,520
Maybe I still have the videos be entirely different, right?

213
00:22:48,520 --> 00:22:55,720
So, could you say that again, Monday, or to do the dumbapada and then do the hangout afterwards?

214
00:22:55,720 --> 00:22:56,720
Yeah.

215
00:22:56,720 --> 00:23:05,080
Just do the audio stream during the dumbapada so people could listen, they want it.

216
00:23:05,080 --> 00:23:07,800
And if they want to watch, they can wait.

217
00:23:07,800 --> 00:23:14,160
And then we could put the, you know, if you could get the, do you see the link in the

218
00:23:14,160 --> 00:23:15,160
bottom right corner?

219
00:23:15,160 --> 00:23:16,760
It says links?

220
00:23:16,760 --> 00:23:20,960
No, that's only on the page of the person who creates a hangout.

221
00:23:20,960 --> 00:23:21,960
Okay.

222
00:23:21,960 --> 00:23:30,280
So I can put the link in the, when people know, uh, know to go there, maybe we will think

223
00:23:30,280 --> 00:23:35,720
about doing that in the future.

224
00:23:35,720 --> 00:23:38,400
Are you, am I echoing you?

225
00:23:38,400 --> 00:23:41,000
I don't think so.

226
00:23:41,000 --> 00:23:43,960
Okay.

227
00:23:43,960 --> 00:23:54,120
And I don't need you loud so I can even turn you down because that's not coming anymore.

228
00:23:54,120 --> 00:23:59,120
So camera, camera is okay, a little bit out of focus.

229
00:23:59,120 --> 00:24:00,960
That's a little blurry.

230
00:24:00,960 --> 00:24:03,360
It kind of goes in and out.

231
00:24:03,360 --> 00:24:06,360
That's okay.

232
00:24:06,360 --> 00:24:07,360
Questions?

233
00:24:07,360 --> 00:24:08,360
Do we have questions?

234
00:24:08,360 --> 00:24:10,800
We have questions, yes.

235
00:24:10,800 --> 00:24:12,360
Why do monks chant?

236
00:24:12,360 --> 00:24:16,440
I ask because I have recently been shown a breathing technique where you breathe in through

237
00:24:16,440 --> 00:24:21,840
your nose and then exhale through your mouth, making an a sound.

238
00:24:21,840 --> 00:24:26,240
This technique has been scientifically proven to benefit the nervous system.

239
00:24:26,240 --> 00:24:30,440
How exactly I don't know, but it got me thinking about monks chanting.

240
00:24:30,440 --> 00:24:34,560
Has the West finally caught up with what the East has known for centuries?

241
00:24:34,560 --> 00:24:35,560
Can you repeat that?

242
00:24:35,560 --> 00:24:38,360
It's been scientifically wild.

243
00:24:38,360 --> 00:24:40,800
Scientifically proven to benefit the nervous system.

244
00:24:40,800 --> 00:24:44,000
Okay, right away I know there's a problem.

245
00:24:44,000 --> 00:24:48,600
Science never, ever, ever proves anything.

246
00:24:48,600 --> 00:24:51,720
Science can't prove anything, can only disprove.

247
00:24:51,720 --> 00:24:55,120
Why I say because you have to be careful.

248
00:24:55,120 --> 00:25:01,280
I don't think this isn't quite your question, but be very careful when you believe claims

249
00:25:01,280 --> 00:25:04,560
that someone says something is scientifically proven.

250
00:25:04,560 --> 00:25:10,760
You can provide evidence in favor of something, but you can't ever prove it.

251
00:25:10,760 --> 00:25:24,880
So you can show what appears to be linked and usually with science, usually science involves

252
00:25:24,880 --> 00:25:31,000
taking lots of people and putting them through a trial and measuring the benefits.

253
00:25:31,000 --> 00:25:36,720
So then you have questions about how it was performed, whether it was a double blind,

254
00:25:36,720 --> 00:25:45,080
whether there was bias involved or whether there was some sort of...

255
00:25:45,080 --> 00:25:52,960
Because studies show that placebos are beneficial to people.

256
00:25:52,960 --> 00:25:58,880
Placebo meaning medicine that is just sugar, it's not medicine, but when it's given to people,

257
00:25:58,880 --> 00:26:08,280
so as a depression medication for example, placebos work, I think it was they work as good

258
00:26:08,280 --> 00:26:11,640
as drugs or something, sometimes even better.

259
00:26:11,640 --> 00:26:22,760
No, placebos don't put noxbos to them, noxbos being non-drugs, fake drugs that also

260
00:26:22,760 --> 00:26:25,720
have unpleasant side effects.

261
00:26:25,720 --> 00:26:31,800
So if a drug had unpleasant side effects, it actually did better, performed better, I think

262
00:26:31,800 --> 00:26:42,440
than certain drugs or something, I mean just ridiculous studies that showed that anoxebo

263
00:26:42,440 --> 00:26:49,160
performed better than the actual depression drugs.

264
00:26:49,160 --> 00:26:53,960
I guess people thought they were taking real drugs.

265
00:26:53,960 --> 00:27:04,480
So the question is how much of a benefit and how reliable is it to say that it's beneficial?

266
00:27:04,480 --> 00:27:07,000
That being said, your question is different, right?

267
00:27:07,000 --> 00:27:09,960
Your question is why do monks do chanting?

268
00:27:09,960 --> 00:27:12,680
How are you related exactly to your...

269
00:27:12,680 --> 00:27:13,960
Can you clarify that from your robin?

270
00:27:13,960 --> 00:27:18,360
How is it related to this idea of saying ah or something?

271
00:27:18,360 --> 00:27:22,720
Yes, he was recently shown a breathing technique where you breathe in through your nose

272
00:27:22,720 --> 00:27:28,680
and how does that, how does that, the idea is that the act of chanting might be beneficial

273
00:27:28,680 --> 00:27:29,680
to your nervous system?

274
00:27:29,680 --> 00:27:30,840
Is that the point?

275
00:27:30,840 --> 00:27:31,840
I think so, yes.

276
00:27:31,840 --> 00:27:34,360
Okay, well that's not why we practice.

277
00:27:34,360 --> 00:27:43,360
We're not so worried about our nervous system, not really at all anyway, actually.

278
00:27:43,360 --> 00:27:52,560
We chant in order to one express respect and veneration and remembrance of the Buddha, but

279
00:27:52,560 --> 00:27:56,680
we also chant to remember the dhamma, so that's to remember the teachings of the Buddha,

280
00:27:56,680 --> 00:27:59,920
so we chant and recite in order to keep them in mind.

281
00:27:59,920 --> 00:28:04,520
Let's see, those are the two main reasons.

282
00:28:04,520 --> 00:28:09,200
There's another reason that people often chant today and probably most Buddhist chanting

283
00:28:09,200 --> 00:28:15,680
is done today as a protection because people want this kind of physical and mundane

284
00:28:15,680 --> 00:28:25,760
protection, which is okay, it's just somewhat well clingy, really, it falls clingy if

285
00:28:25,760 --> 00:28:29,840
you're worried about it, but they people are rightly worried and they have all these worries

286
00:28:29,840 --> 00:28:36,840
about protecting their belongings and stuff in their life and their health, and so we're

287
00:28:36,840 --> 00:28:40,600
able to, it's kind of a cusul paya really.

288
00:28:40,600 --> 00:28:43,520
You tell people while these teachings, if you remember these teachings, it'll be good

289
00:28:43,520 --> 00:28:48,720
for you, and so they memorized, they recite these teachings, but actually the teachings

290
00:28:48,720 --> 00:28:55,480
will teach them how to be good people, sort of a basic means of giving people an introduction

291
00:28:55,480 --> 00:29:03,600
to goodness, in a bit though they're doing it, too, as a means to, for worldly gain for

292
00:29:03,600 --> 00:29:12,360
the most part, like those people who pay respect to the Bodhi tree in order to get pregnant,

293
00:29:12,360 --> 00:29:21,120
how about chanting when you don't, you don't understand what you're saying, is that still

294
00:29:21,120 --> 00:29:22,120
a benefit?

295
00:29:22,120 --> 00:29:25,480
I mean, if you're doing it for the right reason, right, if you've got a heart full of

296
00:29:25,480 --> 00:29:33,600
goodness, like there was this, a gentong always talks about this, this bird that went to

297
00:29:33,600 --> 00:29:42,000
stay with the Bhikunis, and they had it memorized apit, which means bones, bones, I didn't

298
00:29:42,000 --> 00:29:51,640
even know what it was saying, but it kept reciting it, and it was so powerful that this

299
00:29:51,640 --> 00:29:56,920
eagle came and grabbed it, and when it grabbed it, the bird said, Ati ati, it spoke at

300
00:29:56,920 --> 00:30:04,560
that lot, and the eagle was shocked, somehow by the power of the word or something, and

301
00:30:04,560 --> 00:30:15,440
Bhikunis thought that this was indicative of the power of good words, and somehow that's

302
00:30:15,440 --> 00:30:21,320
in the commentary to the Sati Bhatanasu, that gentong always says that the bird went

303
00:30:21,320 --> 00:30:28,280
to heaven, but I don't think the actual suit says that, or the comment or any way says

304
00:30:28,280 --> 00:30:32,480
that, I don't know if there's a place where it does say that the bird went to heaven,

305
00:30:32,480 --> 00:30:37,160
but that would be a better story, and I think that's why he tells it that the bird went

306
00:30:37,160 --> 00:30:42,200
to heaven because it had memorized this word, even though it didn't know what it was

307
00:30:42,200 --> 00:30:43,200
saying.

308
00:30:43,200 --> 00:30:47,120
So at gentong always gets us to memorize the four Sati Bhatanas, and then he said, now

309
00:30:47,120 --> 00:30:51,040
you're as good as animals, you're all going to go to heaven, all these animals who

310
00:30:51,040 --> 00:30:55,400
didn't understand what they were saying, but they still went to heaven, so all of

311
00:30:55,400 --> 00:30:59,440
you are going to heaven, but you're human being, so you can be better than that, and

312
00:30:59,440 --> 00:31:05,080
so now we're going to teach you the meaning, and he gives this talk again, often says

313
00:31:05,080 --> 00:31:13,040
this, so he's very clear, and it's quite clear that if your heart is in the right place

314
00:31:13,040 --> 00:31:16,920
doesn't matter whether you understand, he also would bring up that story when talking

315
00:31:16,920 --> 00:31:21,680
about us back when I didn't understand, whenever there are meditators coming to listen

316
00:31:21,680 --> 00:31:25,120
to the dhamma talk, he always wanted the Western meditators to come and listen to the Thai

317
00:31:25,120 --> 00:31:29,400
dhamma talk, he said, even if you don't understand, you're sitting there and you have

318
00:31:29,400 --> 00:31:34,840
a respectful mind and you know that what's being said is dhamma is the Buddha's teaching,

319
00:31:34,840 --> 00:31:41,440
so good things come, you're good at heaven.

320
00:31:41,440 --> 00:31:48,800
Hello, Bhante, why do we know sitting if it's considered a concept?

321
00:31:48,800 --> 00:31:58,000
That's a good point, because sitting is a description of ultimate realities, or can be seen

322
00:31:58,000 --> 00:32:00,600
as a description of ultimate realities.

323
00:32:00,600 --> 00:32:07,120
When you say sitting, you become aware of something present, which is the feeling of sitting

324
00:32:07,120 --> 00:32:10,320
or the experience of sitting.

325
00:32:10,320 --> 00:32:14,720
Sometimes you won't notice that you're sitting, there's no feeling or there's no sense

326
00:32:14,720 --> 00:32:27,200
when you're really still, but it comes from ultimate realities.

327
00:32:27,200 --> 00:32:31,560
When the sitting meditation is the focus to be on the feeling in the body, or no sing

328
00:32:31,560 --> 00:32:34,560
the action.

329
00:32:34,560 --> 00:32:41,600
It's the feeling, I mean the action is only apparent because of the feeling, so it's the

330
00:32:41,600 --> 00:32:49,640
feeling, physical sensation.

331
00:32:49,640 --> 00:32:57,960
And that's all the questions for tonight.

332
00:32:57,960 --> 00:33:07,080
Okay, well then, have a good night, thanks everyone for tuning in, 34 of yours on YouTube.

333
00:33:07,080 --> 00:33:13,280
Hello everyone, thank you for tuning in, it's always nice to see that people are still

334
00:33:13,280 --> 00:33:15,520
interested in this.

335
00:33:15,520 --> 00:33:23,400
I might have more going on in the future, I've thought about just thinking about doing

336
00:33:23,400 --> 00:33:30,080
maybe a once a week, a talk on something a bit more directly related to meditation, but

337
00:33:30,080 --> 00:33:37,640
that might come in the new year because I might be quitting university in January in order

338
00:33:37,640 --> 00:33:41,160
to do more teaching.

339
00:33:41,160 --> 00:33:50,040
So there's, I could add more slots to the weekly meetings because it's full, mostly.

340
00:33:50,040 --> 00:33:56,680
Anyway, for now this is what we've got, so thank you for tuning in, thanks Robin for your

341
00:33:56,680 --> 00:33:57,680
help.

342
00:33:57,680 --> 00:34:11,560
Thank you, bye everyone, good night.

