1
00:00:00,000 --> 00:00:08,600
Okay, so here we are at the new forest monastery. Here's St. Anne's Minnetoba. Here is the

2
00:00:08,600 --> 00:00:15,400
house. We're staying at. We've got a little deck over on the side there covered

3
00:00:15,400 --> 00:00:28,000
in snow. And then we're surrounded by trees, snow, and the bright blue sky.

4
00:00:28,000 --> 00:00:44,000
Over here we have two sheds. This one on the left here is quite well built.

5
00:00:44,000 --> 00:00:49,400
It's really new. This could be a good day in the future. The one on the right, not so sure,

6
00:00:49,400 --> 00:00:56,000
I haven't been inside it. And the garage, which is probably where I'm going to be living hopefully,

7
00:00:56,000 --> 00:01:03,000
hopefully, is a bit poorly made and falling apart and so on. So hopefully we can get that fixed up,

8
00:01:03,000 --> 00:01:14,000
at least somewhat. And here's the driveway leading down to the main road.

9
00:01:14,000 --> 00:01:20,000
As you can see, it's quite long. It keeps us included from our neighbors.

10
00:01:20,000 --> 00:01:27,000
We're surrounded by trees and then there's this one big tree. I don't know what it is in the yard right by the house.

11
00:01:27,000 --> 00:01:36,000
And that's about it. As you can see, we're in a bit of a clearing here, so we are definitely in the forest.

12
00:01:36,000 --> 00:01:56,000
So now I'll take you inside and we'll take a look at the actual house.

13
00:01:56,000 --> 00:02:01,000
So first thing you see when you get in the house is this strange looking staircase,

14
00:02:01,000 --> 00:02:08,000
right? That's one of the door. Beside it, there is a small bedroom.

15
00:02:08,000 --> 00:02:13,000
All of the heating is baseboard heating. Nice new windows.

16
00:02:13,000 --> 00:02:26,000
And then up the stairs, you'll see sort of a partial room.

17
00:02:26,000 --> 00:02:34,000
A partial, I say, because the roof is not quite full or gives you not full,

18
00:02:34,000 --> 00:02:40,000
quite full space. This strange little look here.

19
00:02:40,000 --> 00:02:51,000
Then you come around here and you see a nice view over the driveway.

20
00:02:51,000 --> 00:02:57,000
And we're like, it's livable, no? Maybe a little small.

21
00:02:57,000 --> 00:03:01,000
You can do walking meditation if you think about it here.

22
00:03:01,000 --> 00:03:09,000
You can walk all the way to the end, which is enough space.

23
00:03:09,000 --> 00:03:15,000
So that's two bedroom spaces so far.

24
00:03:15,000 --> 00:03:23,000
And then over here, under the stairs, we have a closet, which has the circuit,

25
00:03:23,000 --> 00:03:31,000
not a switchboard. You call that thing that has all the breakers.

26
00:03:31,000 --> 00:03:41,000
And here's another room, another little closet space.

27
00:03:41,000 --> 00:03:45,000
From here, we have the living room.

28
00:03:45,000 --> 00:03:53,000
So a fairly large size, good for holding toxins and so on.

29
00:03:53,000 --> 00:03:58,000
So we are meeting space, good for doing walking meditation, etc.

30
00:03:58,000 --> 00:04:03,000
Here we have bedroom number three, which is, I guess, the master bedroom.

31
00:04:03,000 --> 00:04:09,000
It's where I'll be staying in a couple of nights until we get the garage fixed up.

32
00:04:09,000 --> 00:04:12,000
It's got a closet on the left here.

33
00:04:12,000 --> 00:04:20,000
And something that probably should have been a bathroom, but isn't, so now it's just a closet.

34
00:04:20,000 --> 00:04:23,000
For now.

35
00:04:23,000 --> 00:04:27,000
Through here, we come to bedroom number, no.

36
00:04:27,000 --> 00:04:30,000
I come to bathroom number one.

37
00:04:30,000 --> 00:04:35,000
There's only one bathroom in the house, which is probably going to be the Achilles heel.

38
00:04:35,000 --> 00:04:41,000
For the time being until we can remedy that, maybe build an outhouse or something.

39
00:04:41,000 --> 00:04:43,000
That's it.

40
00:04:43,000 --> 00:04:49,000
Well, functional, I hope.

41
00:04:49,000 --> 00:04:51,000
Here's the kitchen.

42
00:04:51,000 --> 00:04:55,000
This is where I just had my first lunch.

43
00:04:55,000 --> 00:04:59,000
Here alone today, which is nice.

44
00:04:59,000 --> 00:05:03,000
It's nice to be out in the forest alone again.

45
00:05:03,000 --> 00:05:07,000
With the time being, tomorrow is going to be full of people.

46
00:05:07,000 --> 00:05:13,000
So, this is bedroom number, and last count.

47
00:05:13,000 --> 00:05:16,000
This is number five, because there are five of them.

48
00:05:16,000 --> 00:05:22,000
So we have one upstairs, two over by the stairs.

49
00:05:22,000 --> 00:05:29,000
One off the living room is four, and this is number five off the kitchen.

50
00:05:29,000 --> 00:05:35,000
Here we have a little pantry area, and the closet, and a door.

51
00:05:35,000 --> 00:05:38,000
So, there are two entrances, which is always nice.

52
00:05:38,000 --> 00:05:46,000
It means we can walk this, which means people can come into the kitchen.

53
00:05:46,000 --> 00:05:48,000
They don't have to disturb the meditators.

54
00:05:48,000 --> 00:05:50,000
And here's that little deck.

55
00:05:50,000 --> 00:05:53,000
So we can't go out too, because of the snow.

56
00:05:53,000 --> 00:05:55,000
We don't have to come from that.

57
00:05:55,000 --> 00:06:00,000
Anyway, that's it. That is our new monastery.

58
00:06:00,000 --> 00:06:03,000
The Baridaha Forest monastery.

59
00:06:03,000 --> 00:06:09,000
Anyway, I hope you've enjoyed this tour, and look forward to seeing you all here in the future.

60
00:06:09,000 --> 00:06:33,000
All the best.

