1
00:00:00,000 --> 00:00:08,480
Hi, so the next question comes from someone called kiddibu kiddi, and the question is,

2
00:00:08,480 --> 00:00:14,880
what do monks eat and how do you view food with regards to your existence?

3
00:00:14,880 --> 00:00:25,280
Monks eat, almost whatever is put in their bowls with the qualifier that we do tend

4
00:00:25,280 --> 00:00:31,640
to separate the food out into that food that is considered to be healthy and beneficial

5
00:00:31,640 --> 00:00:38,040
to the body in that food which is unhealthy and unbeneficial, so a lot of fatty foods and

6
00:00:38,040 --> 00:00:41,040
sweet foods and so on.

7
00:00:41,040 --> 00:00:43,640
We're allowed to separate them out.

8
00:00:43,640 --> 00:00:54,440
We don't have to just eat blindly whatever we get, but once that hurdle has been passed,

9
00:00:54,440 --> 00:01:00,560
we don't generally express much preference in regards to this food or that food.

10
00:01:00,560 --> 00:01:06,080
Though we're not allowed to eat meat that has been killed for our benefit, and we're not

11
00:01:06,080 --> 00:01:10,640
allowed to eat raw flesh and so on, we're not allowed to eat certain types of meat like

12
00:01:10,640 --> 00:01:20,720
snake and elephant and lion and a lot of maybe the more repulsive or exotic foods.

13
00:01:20,720 --> 00:01:26,160
How do we view food with regards to, how do I view food with regards to my existence?

14
00:01:26,160 --> 00:01:31,160
Well, the Buddha taught that there are four kinds of food and the first kind is the food

15
00:01:31,160 --> 00:01:32,600
that we eat.

16
00:01:32,600 --> 00:01:40,800
This is the rice and the curries and the hamburgers and the sandwiches and so on or whatever

17
00:01:40,800 --> 00:01:49,120
you get in whatever culture, anything that is of nutritious value to a person.

18
00:01:49,120 --> 00:01:55,120
The second type of food is contact and the meaning of food here is something that brings

19
00:01:55,120 --> 00:01:57,920
fruit, something that brings a result.

20
00:01:57,920 --> 00:02:07,800
So the result of the physical food is that it brings strength and sustenance to the body.

21
00:02:07,800 --> 00:02:13,280
But another type of food, something that feeds you or brings some kind of fruit, is contact.

22
00:02:13,280 --> 00:02:16,840
So when you see, when you hear, when you smell, when you taste, when you feel anything,

23
00:02:16,840 --> 00:02:19,080
this is also a sort of a type of food.

24
00:02:19,080 --> 00:02:24,720
It feeds the mind and as a result, there are rice, all sorts of defilements, you know,

25
00:02:24,720 --> 00:02:28,960
when you see food, for instance, it makes your mouth water, even though you haven't even

26
00:02:28,960 --> 00:02:34,720
tasted it, because there's the contact between the eye and the food, it brings about

27
00:02:34,720 --> 00:02:36,640
a liking or disliking.

28
00:02:36,640 --> 00:02:41,400
This is the cause of all of the good things and bad things that exist in our mind, why

29
00:02:41,400 --> 00:02:47,040
we get attached to things, why we get repulsed by things.

30
00:02:47,040 --> 00:02:52,600
It's also how we become enlightened through connecting with reality and coming to see it

31
00:02:52,600 --> 00:02:55,040
clearly for what it is.

32
00:02:55,040 --> 00:03:02,280
The third type of food is called volition or intention, and this is karma.

33
00:03:02,280 --> 00:03:06,720
This is the good and the bad deeds that we decide to do.

34
00:03:06,720 --> 00:03:12,140
When we decide to hurt someone, when we decide to help someone, when we decide to say something

35
00:03:12,140 --> 00:03:17,240
nasty, when we decide to say something nice and helpful.

36
00:03:17,240 --> 00:03:22,720
It's our intention that is a food, it's something that feeds the world around us, it feeds

37
00:03:22,720 --> 00:03:23,960
our reality.

38
00:03:23,960 --> 00:03:31,080
So when we intend to hurt someone, it's that which brings about the all of our actions

39
00:03:31,080 --> 00:03:34,280
that hurt and do nasty things to others.

40
00:03:34,280 --> 00:03:38,640
When we have the intention to help, whether people let it feed, all of our actions in

41
00:03:38,640 --> 00:03:40,680
the other direction.

42
00:03:40,680 --> 00:03:52,280
The fourth type of food is consciousness, so it's our consciousness which feeds all of

43
00:03:52,280 --> 00:03:55,560
our experience, all of our reality.

44
00:03:55,560 --> 00:03:57,680
Our reality is based on consciousness.

45
00:03:57,680 --> 00:04:02,240
If there were no consciousness, then we wouldn't have any awareness, obviously, of reality.

46
00:04:02,240 --> 00:04:07,040
There wouldn't be no reality for us, there wouldn't be no world, there wouldn't be no existence.

47
00:04:07,040 --> 00:04:11,160
So consciousness is considered the fourth type of food.

48
00:04:11,160 --> 00:04:16,480
So that's probably a little bit more of a philosophical answer than you were expecting,

49
00:04:16,480 --> 00:04:18,040
but that's how I regard food.

50
00:04:18,040 --> 00:04:20,000
Food is something that brings a result.

51
00:04:20,000 --> 00:04:25,880
Whatever feeds my experience or feeds my reality, I consider that to be food, as far as

52
00:04:25,880 --> 00:04:32,920
the physical lumps of rice and curry and so on, that's really just a part of it, and it's

53
00:04:32,920 --> 00:04:40,360
something that generally feeds the negative side of our reality, the addictive side where

54
00:04:40,360 --> 00:04:44,840
we become addicted to certain types of food, and that's certainly something that we try

55
00:04:44,840 --> 00:04:46,720
not to feed and we try to cut off.

56
00:04:46,720 --> 00:04:53,040
And so I only generally eat one meal a day and try to be very mindful when I eat being

57
00:04:53,040 --> 00:04:57,280
aware of the tastes and aware of the feeling of the food and the mouth and going down

58
00:04:57,280 --> 00:05:04,080
the throat, and just being aware of it for what it is, seeing it as something ordinary

59
00:05:04,080 --> 00:05:12,280
and something phenomenal, something in terms of the phenomenology of the reality of it.

60
00:05:12,280 --> 00:05:17,560
It's a phenomenon that arises and ceases, it's something that comes and goes, and it has

61
00:05:17,560 --> 00:05:23,840
no substance in and of itself, and the act of eating brings about a fruit for the body

62
00:05:23,840 --> 00:05:27,600
that it allows the body to continue on for one more day.

63
00:05:27,600 --> 00:05:55,120
Okay, so I hope that's of interest, thanks for the question.

