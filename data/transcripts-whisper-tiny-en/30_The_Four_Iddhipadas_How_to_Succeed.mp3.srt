1
00:00:00,000 --> 00:00:07,000
Meditation, progress, before it departs all to succeed.

2
00:00:07,000 --> 00:00:12,000
How can I succeed in life and in meditation?

3
00:00:12,000 --> 00:00:17,000
People living in the world have many questions on solving problems in worldly life.

4
00:00:17,000 --> 00:00:26,000
We want to know how to succeed when confronted with the challenges of relationships, livelihoods and of course personal well-being.

5
00:00:26,000 --> 00:00:34,000
The Buddha's teaching on success is applicable to all challenges we face both worldly and religious.

6
00:00:34,000 --> 00:00:38,000
The Buddha taught for Idipada, rose to power and mastery.

7
00:00:38,000 --> 00:00:44,000
They are the four means by which to succeed in any and therefore meditation included.

8
00:00:44,000 --> 00:00:52,000
If you can understand and keep these four principles in mind, your meditation and your life in general will succeed.

9
00:00:52,000 --> 00:00:56,000
The first Idipada principle is gender interest.

10
00:00:56,000 --> 00:01:00,000
A person needs to be interested in what they are doing.

11
00:01:00,000 --> 00:01:03,000
One thing to do is be inclined towards it.

12
00:01:03,000 --> 00:01:09,000
It is hard to succeed at something if it is not something you are inclined to do.

13
00:01:09,000 --> 00:01:11,000
Something you like doing.

14
00:01:11,000 --> 00:01:16,000
If you dislike a job, this of course works against your success.

15
00:01:16,000 --> 00:01:22,000
If you do not like to meditate, it is not easy to gain the benefits of meditation.

16
00:01:22,000 --> 00:01:28,000
Some people criticize mindfulness meditation because it is not comfortable.

17
00:01:28,000 --> 00:01:33,000
They talk about more comfortable types of meditation as being better.

18
00:01:33,000 --> 00:01:40,000
I do not think anyone would disagree that it is much better to have a person to meditation.

19
00:01:40,000 --> 00:01:44,000
It would be much better if we do not have to struggle.

20
00:01:44,000 --> 00:01:53,000
The problem with comfortable and pleasant meditations is that they do not generally challenge you or bring you out of your comfort zone.

21
00:01:53,000 --> 00:01:58,000
As a result, they do not generally lead to enlightenment.

22
00:01:58,000 --> 00:02:06,000
But how do you practice something that challenges you, something that forces you to change without hating it?

23
00:02:06,000 --> 00:02:12,000
I think only mindfulness meditation has the power to allow you to do this.

24
00:02:12,000 --> 00:02:17,000
I think with anything else, you just have to change what you are doing.

25
00:02:17,000 --> 00:02:21,000
Make it comfortable, make it enjoyable, or stop doing it.

26
00:02:21,000 --> 00:02:29,000
Mindfulness is quite different because this life of anything, including meditation, then be an object of mindfulness.

27
00:02:29,000 --> 00:02:34,000
This liking is independent of the actual disliked thing.

28
00:02:34,000 --> 00:02:40,000
If you dislike meditating, it is not directly caused by the meditation.

29
00:02:40,000 --> 00:02:46,000
Your reaction and judgment is independent of the things you judge and react to.

30
00:02:46,000 --> 00:02:50,000
This is part of what we learn in meditation.

31
00:02:50,000 --> 00:02:59,000
Meditation itself is not exempt from our reactions and is not exempt from our study of our reactions.

32
00:02:59,000 --> 00:03:09,000
If you do not like meditating, it is still a disliking, just like any other disliking, and you can be mindful of that.

33
00:03:09,000 --> 00:03:18,000
Out to overcome the dislike of meditation is not to force yourself to meditate, even though we do not want to meditate.

34
00:03:18,000 --> 00:03:22,000
The answer is to meditate on the disliking.

35
00:03:22,000 --> 00:03:27,000
When the disliking becomes the object, it is no longer a problem.

36
00:03:27,000 --> 00:03:40,000
The second idipada is video. With any work you do, if you do not put in a thought, you do not succeed, but with meditation, it is a bit different.

37
00:03:40,000 --> 00:03:48,000
If you just try to meditate harder, you will not really benefit like you might in other kinds of activity.

38
00:03:48,000 --> 00:03:51,000
Right, a thought is a momentary thing.

39
00:03:51,000 --> 00:03:54,000
It is in the moment that you actually do it.

40
00:03:54,000 --> 00:03:56,000
You either have it or you do not.

41
00:03:56,000 --> 00:03:59,000
You are mindful of the object or are you not.

42
00:03:59,000 --> 00:04:02,000
If you are mindful, there is efforts there as well.

43
00:04:02,000 --> 00:04:07,000
You have made the choice to be mindful, and that choice takes effort.

44
00:04:07,000 --> 00:04:10,000
It is not the efforts to walk or sit longer.

45
00:04:10,000 --> 00:04:21,000
If you do a lot of meditation, whether it is sitting or walking and it becomes tiresome, then the fatigue has to be the object of your meditation.

46
00:04:21,000 --> 00:04:25,000
If you are truly mindful, fatigue does not bother you.

47
00:04:25,000 --> 00:04:29,000
Practically speaking, right, effort means just to do it.

48
00:04:29,000 --> 00:04:37,000
When you choose to walk, even when you may feel tired or sit, even when you feel tired, this is right, efforts.

49
00:04:37,000 --> 00:04:42,000
Be mindful of the fatigue rather than letting the fatigue control your practice.

50
00:04:42,000 --> 00:04:47,000
When you get home from work sometimes, then feel like you are too tired to meditate.

51
00:04:47,000 --> 00:04:55,000
Make a decision to be mindful and focused on the fatigue as the object rather than letting it control you.

52
00:04:55,000 --> 00:04:57,000
This takes effort.

53
00:04:57,000 --> 00:05:00,000
If you do not do it, it does not get done.

54
00:05:00,000 --> 00:05:02,000
With any work, this is applicable.

55
00:05:02,000 --> 00:05:07,000
As in any work, if you do not put out efforts, there is no hope or success.

56
00:05:07,000 --> 00:05:11,000
The third idipada is chita, focus.

57
00:05:11,000 --> 00:05:14,000
Chita means keeping your mind on your work.

58
00:05:14,000 --> 00:05:19,000
Those are to succeed, you need to pay attention, focus on what you are doing.

59
00:05:19,000 --> 00:05:26,000
If you are not focused on your work, not paying attention, you will do a sloppy and poor job.

60
00:05:26,000 --> 00:05:29,000
This is even more true in meditation.

61
00:05:29,000 --> 00:05:36,000
If you are not paying attention in meditation, your mind is distracted by abstract conceptual ideas.

62
00:05:36,000 --> 00:05:39,000
There is no way your meditation can progress.

63
00:05:39,000 --> 00:05:47,000
If you are not paying attention to the present real experiences that presents themselves at every moment,

64
00:05:47,000 --> 00:05:51,000
there is no way to succeed in mindfulness meditation.

65
00:05:51,000 --> 00:05:54,000
Attention is a sign of mindfulness.

66
00:05:54,000 --> 00:05:57,000
If you are mindful, you are paying attention.

67
00:05:57,000 --> 00:06:04,000
Mindfulness grasps the object and confronts it, which goes hand in hand with paying attention.

68
00:06:04,000 --> 00:06:11,000
Whatever you are going to do in life, if you want to succeed at it, you have to be there and be present.

69
00:06:11,000 --> 00:06:20,000
This is one of the great benefits of mindfulness meditation that it improves your attention span as part of the training.

70
00:06:20,000 --> 00:06:24,000
The fourth idipada is vymanksa, consideration.

71
00:06:24,000 --> 00:06:30,000
Vymanksa is not like chita, which refers to the focus on the task at hand.

72
00:06:30,000 --> 00:06:39,000
When you focus on something without reflection or consideration, it is easy to become oblivious to the imperfections of your work.

73
00:06:39,000 --> 00:06:42,000
This may even reinforce bad habits.

74
00:06:42,000 --> 00:06:58,000
Someone might practice walking and sitting meditation for many hours without great benefits as they have built to recognize the lack of mindfulness due to lack of reflection about their mental activity.

75
00:06:58,000 --> 00:07:12,000
A part of the benefit of interviews with a teacher during a meditation course is that the meditator is forced to reflect on their practice when the teacher asks how their practice was that day.

76
00:07:12,000 --> 00:07:21,000
Without such questioning, it is easy to reinforce bad habits and get started in unmindful mental activity.

77
00:07:21,000 --> 00:07:31,000
Vymanksa is what allows a meditator to correct their practice. The same goes with anywhere you might undertake in life.

78
00:07:31,000 --> 00:07:41,000
You cannot just work like an ox. You have to be clever, you have to be able to reflect and consider that your methods might be wrong.

79
00:07:41,000 --> 00:08:00,000
That's maybe you need to change something. Vymanksa involves the ability to adopt and adjust. You can see this in the case where a webtop performs the same job every day without ever thinking that maybe they could improve if they did something different.

80
00:08:00,000 --> 00:08:11,000
And quite often, if anyone suggests that they could improve by changing their methods they become upset. Thinking that it works fine the way they do it.

81
00:08:11,000 --> 00:08:28,000
Without of inability to change and adapt based on a seated reflection will inevitably hinder your ability to succeed. Flexibility and adaptability are important both in the world and in meditation.

82
00:08:28,000 --> 00:08:42,000
Minds of someone who is mindful, adaptable, not attached and not stuck up or equitistical. When confronted with the needs to change they do not get upset. They change and they adapt.

83
00:08:42,000 --> 00:09:06,000
Vymanksa is the ability to consider and adjust further than just how on ignorantly like an ox.

