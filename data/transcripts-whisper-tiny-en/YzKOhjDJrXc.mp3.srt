1
00:00:00,000 --> 00:00:16,960
Okay, hello everyone, we're broadcasting live November 10, 2015.

2
00:00:16,960 --> 00:00:22,480
What's our quote I'd like today?

3
00:00:22,480 --> 00:00:35,920
And with one tone, good and the good of others.

4
00:00:35,920 --> 00:00:39,960
How is one concerned with his own good and the good of others?

5
00:00:39,960 --> 00:00:42,200
Concerning this, a certain person is quick.

6
00:00:42,200 --> 00:00:44,840
He grasps teachings that are profitable.

7
00:00:44,840 --> 00:00:49,360
He has learned by heart and by understanding both the letter and spirit of the dhamma and walks

8
00:00:49,360 --> 00:00:51,160
in accordance with it.

9
00:00:51,160 --> 00:00:57,920
He also has a beautiful voice and delivery, possesses or vain speech, distinctly and

10
00:00:57,920 --> 00:01:01,680
clearly enunciate so as to make his meaning clear.

11
00:01:01,680 --> 00:01:14,400
He teaches, urges, insights and gladness his companions in holy life.

12
00:01:14,400 --> 00:01:24,320
So it's actually four types of people in the sutta.

13
00:01:24,320 --> 00:01:32,560
There's the person who works for their own benefit and not for the benefit of others.

14
00:01:32,560 --> 00:01:41,280
When it works for the benefit of others and not for one's own benefit, and when it works

15
00:01:41,280 --> 00:01:48,080
for one's own benefit of others, then one who works for both.

16
00:01:48,080 --> 00:01:49,880
So what is to one's own benefit?

17
00:01:49,880 --> 00:01:54,760
One's own benefit is being quick in attending to wholesome teachings, being able to retain

18
00:01:54,760 --> 00:02:01,160
in mind the teachings one has heard, and examining the meaning of the teachings, and

19
00:02:01,160 --> 00:02:05,360
then practicing and according to the dhamma.

20
00:02:05,360 --> 00:02:10,360
So we have attending to the teachings, retaining them in mind, examining the meaning and

21
00:02:10,360 --> 00:02:11,360
practicing.

22
00:02:11,360 --> 00:02:17,840
These are the four qualities of a Bahu sutta.

23
00:02:17,840 --> 00:02:24,160
Someone who has said to have learned much in the Buddha's teaching is attending to them.

24
00:02:24,160 --> 00:02:26,720
Keep a nipati, I think, something else.

25
00:02:26,720 --> 00:02:37,800
Quick to attend, keep a nisanti, nisanti.

26
00:02:37,800 --> 00:02:48,680
Quick observation, quick to pay attention, maybe.

27
00:02:48,680 --> 00:02:59,640
Sautanan, sautanan, jatamanan, dharakat, jatikali, second is someone who memorizes, and

28
00:02:59,640 --> 00:03:08,040
unable to retain in mind the teachings he has heard. Number three, examine the meanings.

29
00:03:08,040 --> 00:03:18,240
Meaning, sorry, examine the meaning of the teachings, you have retained. So it's not enough

30
00:03:18,240 --> 00:03:24,680
just to listen, but you have to be someone who is keen to listen, keen to hear. You have

31
00:03:24,680 --> 00:03:29,920
to be able to retain. Apart from that, you have to be able to keep it in mind. This is a skill

32
00:03:29,920 --> 00:03:40,280
we have to learn, but to find ways to remember things, write it down. I remember recording

33
00:03:40,280 --> 00:03:49,640
all of Ajahn Tung's teachings on minidisc. I don't know where those minidisc are. I think

34
00:03:49,640 --> 00:03:55,320
I left them in Sri Lanka, but I have the wave files somewhere, hopefully. I transferred it

35
00:03:55,320 --> 00:04:00,600
all the wave, actually. But I used to listen to these talks and try to figure out what he was

36
00:04:00,600 --> 00:04:16,640
saying, because he'd quote Pauli, but his pronunciation is a little bit unique. So I'm trying to

37
00:04:16,640 --> 00:04:21,840
look it up, look up to Pauli. I would spend a lot of time searching for these things. Once I figured

38
00:04:21,840 --> 00:04:37,160
out exactly what he was quoting, I would write it down and commit it to memory. So that's

39
00:04:37,160 --> 00:04:43,920
important, but then not only keeping it in memory, you also have to consider it. You have to

40
00:04:43,920 --> 00:04:49,280
think about how it relates to you and your practice. You have to think about what exactly it

41
00:04:49,280 --> 00:05:04,120
really means. You have to consider what is the true meaning of teaching. And finally, you have

42
00:05:04,120 --> 00:05:10,960
to put it into practice, practice and according to it. That's the fourth. So that is what's good

43
00:05:10,960 --> 00:05:18,400
for oneself. That's someone who does that is working for one's own welfare. But if one does all

44
00:05:18,400 --> 00:05:24,800
that and is not a good speaker or is a speaker, but it doesn't have a good delivery, it doesn't

45
00:05:24,800 --> 00:05:29,840
brush up their speech that it becomes polished, clear, articulate, expressive of the meaning.

46
00:05:31,520 --> 00:05:36,960
And one does not instruct, encourage, inspire, and glad in one's fellow, one's fellow monks,

47
00:05:36,960 --> 00:05:42,960
then one is not working for the welfare of others. So to work for their welfare of others,

48
00:05:43,920 --> 00:05:49,680
you have to be a good speaker with a good delivery. You have to be gifted with speech that is

49
00:05:49,680 --> 00:05:55,200
polished, clear, articulate, expressive of the meaning. And you have to instruct, you have to use

50
00:05:55,200 --> 00:06:00,400
that speech to instruct, encourage, inspire, and glad in others in the dhamma.

51
00:06:00,400 --> 00:06:09,600
So it's, again, the Buddha is not saying that you have to actually do one or the other.

52
00:06:09,600 --> 00:06:15,120
He's just pointing out, well, what is to one's own welfare? What is to the welfare of others?

53
00:06:16,480 --> 00:06:22,640
You choose. Just don't be, I think there's an implication, don't be the person who works

54
00:06:22,640 --> 00:06:28,320
neither for one's own welfare or another. And probably don't be the one, pretty clearly,

55
00:06:28,320 --> 00:06:33,520
they don't be the one who works only for the welfare of others. Because that's counterproductive.

56
00:06:33,520 --> 00:06:39,280
If everyone's working in the benefit of others, then no one actually ever benefits.

57
00:06:44,480 --> 00:06:46,080
So do we have any questions today?

58
00:06:46,880 --> 00:06:51,600
We do. We can, we can start with the three we answered last night when we thought we were still on

59
00:06:51,600 --> 00:06:57,520
air after the technical difficulties. Right. We had a good session after you all went away.

60
00:06:57,520 --> 00:07:00,400
After we were sitting there, it's not live.

61
00:07:01,920 --> 00:07:06,800
But I'm pretty sure after you crashed, it did say live at one time, but for a moment,

62
00:07:06,800 --> 00:07:11,360
and then it was gone all of a sudden, and recorded in the audience's questions.

63
00:07:12,160 --> 00:07:17,440
So the first one in that category was, is it considered a good deed to care for the environment?

64
00:07:18,800 --> 00:07:25,920
Right. And I said, it's a bit of a roundabout deed. It's a good deed and so far as it helps

65
00:07:25,920 --> 00:07:31,840
beings, but a tree isn't considered a being. So helping a tree isn't considered a good deed.

66
00:07:33,200 --> 00:07:37,360
Intrinsically, now that being said, good deeds are all in the mind. So if you help a tree

67
00:07:37,360 --> 00:07:42,560
thinking that the tree feels and that the tree benefits from it, then it actually is to some extent

68
00:07:42,560 --> 00:07:49,040
to good deed. For some extent, it's problematic because it involves falsity. It involves,

69
00:07:49,040 --> 00:07:55,280
it's like a child who takes care of their stuffed animal and then taking care of their stuffed

70
00:07:55,280 --> 00:07:59,680
animal, they develop love and they develop compassion and so on. But there is a problem

71
00:08:00,320 --> 00:08:04,640
because it's not real and there's a bit of delusion involved with the child.

72
00:08:05,440 --> 00:08:08,320
We would say probably with the person who looks after the tree because they

73
00:08:08,320 --> 00:08:13,600
anthropomorphize it and they give it qualities that it doesn't possess, saying the tree can

74
00:08:13,600 --> 00:08:18,560
feel pain, et cetera, et cetera. But in fact, there's no reason to think that the tree cares.

75
00:08:18,560 --> 00:08:25,360
People, I think some people would argue that and say, oh no, a tree is feeling

76
00:08:26,640 --> 00:08:32,880
or whatever, but there's not enough. Even if it is considered to be sentient, which I think

77
00:08:32,880 --> 00:08:41,760
it's pretty clearly not. If a tree were sentient, I don't think a tree is sentient.

78
00:08:41,760 --> 00:08:49,520
Is all thinking useless?

79
00:08:53,600 --> 00:09:00,240
Right, so it's not useless because thinking, some thinking can lead to benefit,

80
00:09:00,240 --> 00:09:05,440
can lead you closer to the environment, can be a support on the path. But thinking is intrinsically

81
00:09:05,440 --> 00:09:09,120
valueless in the sense that in the end you have to throw away all thought.

82
00:09:09,120 --> 00:09:14,000
In the end you don't keep any of it. You don't hold on to any of it as having any value.

83
00:09:15,600 --> 00:09:20,960
But that's, that has to come through some thought to some extent. We have to use thought

84
00:09:21,520 --> 00:09:31,600
in order to realize that thought is, is valueless. I mean, you don't have to, but generally it's

85
00:09:31,600 --> 00:09:40,000
useful for most people. But intrinsically, not intrinsically valueless.

86
00:09:42,000 --> 00:09:45,600
How did monks measure their meditation time in ancient times?

87
00:09:47,120 --> 00:09:52,960
One way that my teacher taught us, my teacher mentioned, was to use incense sticks.

88
00:09:52,960 --> 00:10:02,720
So there's, you measure walking by one incense stick when it's burnt down and then you do

89
00:10:03,840 --> 00:10:06,800
sitting with one incense sticks. And of course you could cut them in half.

90
00:10:06,800 --> 00:10:10,880
Anything with fire, there's lots of things. Fire, water is another way sand is another way

91
00:10:10,880 --> 00:10:26,800
time pieces. You know, there were ancient ways to keep time. Even the sun you could use.

92
00:10:29,280 --> 00:10:34,880
Hello, Bunthy. I want to start learning poly language. Can you please give me some suggestions. Thanks.

93
00:10:34,880 --> 00:10:43,600
Spend your time meditating. Here's a person who's yellow. Oh, that was, that was yesterday.

94
00:10:43,600 --> 00:10:53,840
Oh, yeah. Okay. So it's, yeah, yellow for a reason. Yeah, well, don't bother too much. If you're

95
00:10:53,840 --> 00:10:58,880
a monk, it's a good thing. If you're not a monk, it's, it's generally a bit difficult because

96
00:10:58,880 --> 00:11:03,920
you've got lots of things and other things to think about. So the amount of poly that you're going

97
00:11:03,920 --> 00:11:13,360
to learn and that you're going to use is limited. If you're still dedicated to learning poly,

98
00:11:18,240 --> 00:11:21,600
well, you have to find someone who teaches it properly. There are ways of learning it

99
00:11:23,600 --> 00:11:28,560
out there, but most of them are not based in the English ones. Most of them are not based on

100
00:11:28,560 --> 00:11:34,240
the traditional way of learning. So I tried to put together a course, put together a little,

101
00:11:34,240 --> 00:11:40,640
sort of a guidebook and a course that went along traditional lines, but I've never really had enough

102
00:11:40,640 --> 00:11:42,000
time to take it seriously.

103
00:11:53,840 --> 00:11:57,360
Are there other states during meditation that can seem like nabana?

104
00:11:57,360 --> 00:12:02,480
In other words, a peaceful trance where one doesn't feel body or time, and there's nothing to

105
00:12:02,480 --> 00:12:07,040
note. That's nothing like nibana. What you're describing is nothing like nibana.

106
00:12:14,160 --> 00:12:20,640
I don't know. I mean, it's, it's all words, right? But if it seems like anything, then it's not

107
00:12:20,640 --> 00:12:30,560
nibana. If it feels peaceful, then it's not nibana. It feels quiet. It's still not nibana. There's

108
00:12:30,560 --> 00:12:31,760
no feeling in nibana.

109
00:12:37,520 --> 00:12:43,440
A little earlier, the person had noted he was not sure if nibana, he or she was not sure if

110
00:12:43,440 --> 00:12:52,080
nibana or fell asleep. So maybe fell asleep.

111
00:12:54,320 --> 00:12:59,280
Sometimes when I thought the former rises, I just note it and it stops, but other times I get

112
00:12:59,280 --> 00:13:02,320
engrossed and lose myself in it. How can I stop that?

113
00:13:04,240 --> 00:13:09,840
You can. It's not under your control. It's a part of what we're learning that it's that we're not in

114
00:13:09,840 --> 00:13:15,760
charge. Of course, if you practice more, you'll be, you'll, they'll cultivate the habit of being

115
00:13:15,760 --> 00:13:23,200
able to catch things and it'll slowly lean in that direction, but it's a habit. It's not a,

116
00:13:24,080 --> 00:13:31,200
you're not in control. You can't just stop things, but you can do is build habits and change your

117
00:13:31,200 --> 00:13:37,680
habits. That's what it takes. That's what the meditation is, is meant to accomplish. Take, just

118
00:13:37,680 --> 00:13:40,400
takes time though.

119
00:13:52,240 --> 00:13:58,480
Sadly. We are all cut up and still lies. Right.

120
00:13:58,480 --> 00:14:11,040
Okay. Well, that's all then. Demo pad again tomorrow for the today. We have a woman from Switzerland

121
00:14:11,040 --> 00:14:17,760
listening. She just came for the evening. I think she's left already. That she visits her and

122
00:14:17,760 --> 00:14:25,040
in Toronto. So she came by here, took a bus all the way from Toronto and a bus back in the rain.

123
00:14:25,040 --> 00:14:27,760
That's dedication.

124
00:14:32,160 --> 00:14:36,160
That's good to hear that people are finding that where you are there. That's wonderful.

125
00:14:36,160 --> 00:14:53,360
Okay. Good night. Thank you, Robin. Thank you, Dante. Good night.

