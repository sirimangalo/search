1
00:00:00,000 --> 00:00:05,000
I find myself becoming rather antisocial as I progress in my meditation.

2
00:00:05,000 --> 00:00:08,000
It's this normal.

3
00:00:08,000 --> 00:00:10,000
I don't know if I'm qualified to answer this.

4
00:00:10,000 --> 00:00:13,000
I've always been antisocial in my whole life.

5
00:00:13,000 --> 00:00:16,000
So, for me to become a monk, it's just like,

6
00:00:16,000 --> 00:00:19,000
well yeah, that's how I live anyway.

7
00:00:19,000 --> 00:00:24,000
But that's kind of a joke.

8
00:00:24,000 --> 00:00:30,000
To do meditators normally become antisocial.

9
00:00:30,000 --> 00:00:35,000
I think so in one sense.

10
00:00:35,000 --> 00:00:37,000
And why I think so.

11
00:00:37,000 --> 00:00:43,000
And it's something that I think many Buddhists don't appreciate at all.

12
00:00:43,000 --> 00:00:47,000
Some many Buddhists miss this.

13
00:00:47,000 --> 00:00:49,000
And even Buddhist meditators.

14
00:00:49,000 --> 00:00:54,000
We have meditation centers where meditators will one meditator.

15
00:00:54,000 --> 00:00:57,000
He was telling me I'm sitting in meditation.

16
00:00:57,000 --> 00:01:00,000
And the person in the Kutti on this side of me,

17
00:01:00,000 --> 00:01:05,000
telephones the person in the Kutti on this side of me.

18
00:01:05,000 --> 00:01:08,000
And you could hear it.

19
00:01:08,000 --> 00:01:10,000
This one's calling this one, hello, hello.

20
00:01:10,000 --> 00:01:15,000
They're carrying out a conversation with this person in between.

21
00:01:15,000 --> 00:01:27,000
The Buddha was horribly adamant about this.

22
00:01:27,000 --> 00:01:37,000
That a person who is undertaking the practice should not engage in any sort of socializing.

23
00:01:37,000 --> 00:01:48,000
The acupa we wake up on Patis, Patisala, and the woods in the Ovada, Patimoka.

24
00:01:48,000 --> 00:01:55,000
Patanja, Sainas, and these words all mean finding in a secluded place to live,

25
00:01:55,000 --> 00:01:58,000
place to dwell.

26
00:01:58,000 --> 00:02:03,000
The Buddha extolled people who are content.

27
00:02:03,000 --> 00:02:11,000
Narin Chati, Patisala, and a person doesn't throw away the opportunity for solitude, for seclusion.

28
00:02:11,000 --> 00:02:18,000
And again, and again, he would talk about this, leaving behind society.

29
00:02:18,000 --> 00:02:23,000
Because the point is we're going back to basic principles.

30
00:02:23,000 --> 00:02:30,000
The understanding that reality is phenomenological in a sense, or even solipsistic,

31
00:02:30,000 --> 00:02:37,000
but in the vein of solipsism where my reality is only me.

32
00:02:37,000 --> 00:02:41,000
And when I'm with other people, I don't learn about reality.

33
00:02:41,000 --> 00:02:45,000
I don't learn about existence.

34
00:02:45,000 --> 00:02:56,000
I only actually give rise to more and more proliferation in clinging and views and ideas inside of my own head.

35
00:02:56,000 --> 00:03:01,000
Because I don't live their life and they don't live my life.

36
00:03:01,000 --> 00:03:05,000
When we come together, we still only create stuff in our heads, thinking,

37
00:03:05,000 --> 00:03:11,000
oh, that person's nice, oh, that person's mean, oh, that person's this acute,

38
00:03:11,000 --> 00:03:15,000
all of these ideas that come up.

39
00:03:15,000 --> 00:03:17,000
And we create more and more.

40
00:03:17,000 --> 00:03:21,000
That's what a relationship is.

41
00:03:21,000 --> 00:03:27,000
A relationship is the creation of something that isn't really there,

42
00:03:27,000 --> 00:03:31,000
that wasn't there in the first place, that doesn't exist in ultimate reality.

43
00:03:31,000 --> 00:03:38,000
So the only way you can start to learn about ultimate reality is to pull back

44
00:03:38,000 --> 00:03:43,000
and to come back to first principles, seeing, hearing, smelling, tasting, feeling, thinking.

45
00:03:43,000 --> 00:03:47,000
A person who sees reality in terms of these six things,

46
00:03:47,000 --> 00:03:53,000
is obviously not going to be a very social person, at least not until they can fake it.

47
00:03:53,000 --> 00:04:00,000
Because on the other side, I would say, it's possible for someone who's been practicing for a long time to be,

48
00:04:00,000 --> 00:04:07,000
to seem somewhat social in the sense that they often interact with other people,

49
00:04:07,000 --> 00:04:14,000
but they won't do so much of the interacting in terms of asking about people's families and so on,

50
00:04:14,000 --> 00:04:17,000
and they won't get caught up in it.

51
00:04:17,000 --> 00:04:23,000
But they'll be able to interact in such a way that people think that they're being social,

52
00:04:23,000 --> 00:04:27,000
but actually they're thinking, well, maybe they're even thinking, get me out here,

53
00:04:27,000 --> 00:04:29,000
I've got to go meditate or so on.

54
00:04:29,000 --> 00:04:37,000
But for someone who is enlightened, they would be in no way clinging to the people who are there,

55
00:04:37,000 --> 00:04:41,000
not thinking, get them out of here, but not thinking, oh, I hope they stay,

56
00:04:41,000 --> 00:04:45,000
and attached to this.

57
00:04:45,000 --> 00:04:49,000
But that's, I think, quite a bit later on in the beginning.

58
00:04:49,000 --> 00:04:57,000
There are several reasons. One might become antisocial out of this terror that can come in meditation.

59
00:04:57,000 --> 00:05:01,000
The terror, the realization, when I'm around people, I can be a real horrible person.

60
00:05:01,000 --> 00:05:05,000
I can give rise to all sorts of defilements.

61
00:05:05,000 --> 00:05:10,000
And then when you're alone, you feel guilty, you feel, oh, I said, first, what a stupid thing I said and so on.

62
00:05:10,000 --> 00:05:19,000
And the realization that you do nasty and you get into lots of trouble when you're in a group.

63
00:05:19,000 --> 00:05:24,000
And there's a lot of stress in it and a lot of pretension.

64
00:05:24,000 --> 00:05:29,000
Most of our relationship with people are all just fake.

65
00:05:29,000 --> 00:05:31,000
We pretend to be someone.

66
00:05:31,000 --> 00:05:34,000
We put on pretenses when we're around people.

67
00:05:34,000 --> 00:05:42,000
And this is why people who begin to meditate will just freak out and like, oh, I can't handle it.

68
00:05:42,000 --> 00:05:49,000
Which is sort of how I was feeling in the beginning, because my parents were pretty hard on me,

69
00:05:49,000 --> 00:05:56,000
and saying that something was wrong with me, and I should stop what I was stopped this meditation thing.

70
00:05:56,000 --> 00:06:02,000
I just thought to myself, well, eventually, I'll figure it out. They'll figure it out. It'll all be good.

71
00:06:02,000 --> 00:06:07,000
And so I didn't have this concern that somehow I should fit in and be social.

72
00:06:07,000 --> 00:06:12,000
I was like content for now to be antisocial, and like eventually I'll figure it out.

73
00:06:12,000 --> 00:06:22,000
Because when you understand that the meditation is a benefit, it doesn't really bother you that you might be antisocial,

74
00:06:22,000 --> 00:06:26,000
or you might be offending other people, people might be getting angry.

75
00:06:26,000 --> 00:06:32,000
If someone gets angry at you, because for example, you're not interested in, because you're boring,

76
00:06:32,000 --> 00:06:35,000
or you don't want to go out for drinks, hey, well, let's go out to the bar.

77
00:06:35,000 --> 00:06:41,000
I'm sorry, I don't drink. You, you know, purest or so on.

78
00:06:41,000 --> 00:06:44,000
And why don't you drink? Well, I believe it's unethical.

79
00:06:44,000 --> 00:06:53,000
Oh, you, you know, then you judge mental or so on and so on.

80
00:06:53,000 --> 00:06:58,000
So anyway, just some thoughts on the idea of being antisocial, I don't know.

81
00:06:58,000 --> 00:07:02,000
Anyone else have more thoughts on it?

82
00:07:02,000 --> 00:07:04,000
Oh, China.

83
00:07:04,000 --> 00:07:10,000
It's kind of ironic for me really, because before I started meditating,

84
00:07:10,000 --> 00:07:16,000
I always had trouble talking to people and was like, oh, I wish, I wish I could manage to,

85
00:07:16,000 --> 00:07:23,000
like, I don't know, like, especially with work and stuff, just trying to talk to everybody and be like,

86
00:07:23,000 --> 00:07:25,000
yeah, he's a nice guy on all this stuff.

87
00:07:25,000 --> 00:07:26,000
And I couldn't do it.

88
00:07:26,000 --> 00:07:30,000
And then once I started meditating, I found it a lot easier to talk to people,

89
00:07:30,000 --> 00:07:35,000
because you start to, you see the mind, you see what it's inclined to.

90
00:07:35,000 --> 00:07:40,000
So you could just say something and feel like, oh, yeah, yeah, and go with it.

91
00:07:40,000 --> 00:07:49,000
And I found that it became a lot easier, but I lost the desire to, so it was,

92
00:07:49,000 --> 00:07:51,000
I don't know, I think that's just natural.

93
00:07:51,000 --> 00:07:55,000
Because you just start to see, it's like you're saying there's no usefulness to it.

94
00:07:55,000 --> 00:07:59,000
He's saying it becomes harder and harder to tolerate gossip or delusion of others.

95
00:07:59,000 --> 00:08:00,000
For sure.

96
00:08:00,000 --> 00:08:01,000
Yeah.

97
00:08:01,000 --> 00:08:04,000
You can get bored and you do start to feel like, oh, get me out of here.

98
00:08:04,000 --> 00:08:05,000
You're like, oh, yeah.

99
00:08:05,000 --> 00:08:09,000
Oh, let me get me back to my room where I can meditate.

100
00:08:09,000 --> 00:08:11,000
Because it's useless.

101
00:08:11,000 --> 00:08:15,000
You know, you're not gaining anything and you can feel them pulling you in.

102
00:08:15,000 --> 00:08:18,000
You can feel they want you to stay, they want.

103
00:08:18,000 --> 00:08:26,000
And maybe you even want to stay, because you still have attachments to people and laziness as well,

104
00:08:26,000 --> 00:08:29,000
because you think if I don't stay here, I got to go back and meditate.

105
00:08:29,000 --> 00:08:31,000
And that's no fun.

106
00:08:31,000 --> 00:08:36,000
That's a hard work.

107
00:08:36,000 --> 00:08:46,000
But you, yeah, for sure, you begin to feel the stress involved in group situations.

108
00:08:46,000 --> 00:08:57,000
What I've said before, how I think we can understand the idea of Calliana Mita, a good friend.

109
00:08:57,000 --> 00:09:00,000
I think a good way to understand what it means by a good friend.

110
00:09:00,000 --> 00:09:03,000
A good friend is someone who leaves you alone.

111
00:09:03,000 --> 00:09:08,000
A good friend is someone who gives you the opportunity to learn more about yourself,

112
00:09:08,000 --> 00:09:10,000
who creates that opportunity.

113
00:09:10,000 --> 00:09:16,000
A good friend is not someone who sits there and lectures you and preaches to you.

114
00:09:16,000 --> 00:09:24,000
A good friend is someone who provides you with a practice.

115
00:09:24,000 --> 00:09:29,000
The Buddha said, I think at least in one place, the Buddha said,

116
00:09:29,000 --> 00:09:36,000
when he teaches, he teaches thinking, what can I say that will get rid of them as quickly as possible?

117
00:09:36,000 --> 00:09:39,000
Basically something like this was something very close to that.

118
00:09:39,000 --> 00:09:44,000
So very interesting teaching that he wasn't interested in keeping them there and listening.

119
00:09:44,000 --> 00:09:51,000
He was like, what can I do that's going to get them to go and obviously to go and practice meditation as quickly as possible?

120
00:09:51,000 --> 00:09:57,000
Because the point is not to sit around and talk.

121
00:09:57,000 --> 00:10:08,000
Yes, I experienced the same that what was described earlier,

122
00:10:08,000 --> 00:10:19,000
that it came to a point where it felt kind of almost painful to have to socialize with other people.

123
00:10:19,000 --> 00:10:24,000
And I just wanted to go back to my room and meditate.

124
00:10:24,000 --> 00:10:29,000
And interestingly, this calmed a little bit down.

125
00:10:29,000 --> 00:10:31,000
It's not painful anymore.

126
00:10:31,000 --> 00:10:38,000
It's just I don't see the sense in socializing.

127
00:10:38,000 --> 00:10:45,000
I see sense in doing something like this sitting here in a group and talking about the Dharma that,

128
00:10:45,000 --> 00:10:52,000
yes, and that is kind of a social event and that is good enough.

129
00:10:52,000 --> 00:11:02,000
So yeah, first it was very kind of painful, but then it can relax again as well.

130
00:11:02,000 --> 00:11:07,000
So when you're not so tense anymore.

131
00:11:07,000 --> 00:11:13,000
Yeah, I think you start to learn how to be true to yourself in a social situation,

132
00:11:13,000 --> 00:11:20,000
which doesn't mean keeping quiet, but see the problem in the beginning is not that you're quiet.

133
00:11:20,000 --> 00:11:25,000
The problem is that you're stressed, you're tense, even as a meditator.

134
00:11:25,000 --> 00:11:30,000
Because you have an example of that, like Ajahn Tang, these people come to them.

135
00:11:30,000 --> 00:11:35,000
People will come up, our teachers, it's so much fun to sit there.

136
00:11:35,000 --> 00:11:40,000
I sit there all day and I met the widest variety of people you could ever imagine.

137
00:11:40,000 --> 00:11:45,000
And the widest variety of experiences, people came up and started yelling in front of them.

138
00:11:45,000 --> 00:11:56,000
He's a buffalo and just the most horrible arguments and Ajahn would just sit there

139
00:11:56,000 --> 00:12:03,000
and basically acknowledging it all, giving the right advice at the right time.

140
00:12:03,000 --> 00:12:08,000
These two monks came up and they were complaining about another monk.

141
00:12:08,000 --> 00:12:14,000
They said they were senior monks and one of them was like 20 years and they said,

142
00:12:14,000 --> 00:12:21,000
this young monk, he was teaching this senior monk.

143
00:12:21,000 --> 00:12:24,000
I wanted to ask Ajahn Tang whether that's appropriate.

144
00:12:24,000 --> 00:12:29,000
Because there's this monk in my teacher's monastery who likes to boss people around

145
00:12:29,000 --> 00:12:31,000
and he was only a junior monk at that time.

146
00:12:31,000 --> 00:12:34,000
He's not even a, he's still a Nabok, a junior monk.

147
00:12:34,000 --> 00:12:39,000
And Ajahn just said, you know, anyone can be anyone's teacher.

148
00:12:39,000 --> 00:12:49,000
He just couldn't be your teacher and they were all quieted down and the other monk is like,

149
00:12:49,000 --> 00:12:52,000
don't, don't, don't, don't, don't bring it up.

150
00:12:52,000 --> 00:12:55,000
Let's go, let's go.

151
00:12:55,000 --> 00:13:00,000
Oh, and just the so many different stories and the people who would just come up

152
00:13:00,000 --> 00:13:08,000
to meet my teacher and just sit there and talk to him about the most useless things.

153
00:13:08,000 --> 00:13:15,000
And he would just sit there, mmm, mmm, mmm, mmm.

154
00:13:15,000 --> 00:13:19,000
And it was fun to watch because we're sitting there and tortures like, oh,

155
00:13:19,000 --> 00:13:20,000
maybe get me out of here an hour.

156
00:13:20,000 --> 00:13:24,000
She's been talking about her, her arthritis.

157
00:13:24,000 --> 00:13:34,000
And he was like, oh, and he was like, oh, looking at her, her arthritis and stuff.

158
00:13:34,000 --> 00:13:38,000
So, it does get easier, but you're no longer socializing.

159
00:13:38,000 --> 00:13:44,000
He's just being mindful and being, you know, present and being perfect for them.

160
00:13:44,000 --> 00:14:08,000
Alright, that's my teacher.

