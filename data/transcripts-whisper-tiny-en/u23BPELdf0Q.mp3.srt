1
00:00:00,000 --> 00:00:07,000
Well, everyone, broadcasting live in November for us.

2
00:00:07,000 --> 00:00:13,000
So we're trying them up again.

3
00:00:13,000 --> 00:00:16,000
I'm really trying with this.

4
00:00:16,000 --> 00:00:21,000
Really, and problem is we're in cramped quarters.

5
00:00:21,000 --> 00:00:26,000
I tried several things tonight and just makes me,

6
00:00:26,000 --> 00:00:30,000
reminds me of all the troubles involved with this recording.

7
00:00:30,000 --> 00:00:36,000
The battery is in lights.

8
00:00:36,000 --> 00:00:40,000
Big white sheets tacked to your wall.

9
00:00:40,000 --> 00:00:52,000
Anyway, we keep trying.

10
00:00:52,000 --> 00:00:55,000
Hello, and welcome back to our study of the Dumbo Panda.

11
00:00:55,000 --> 00:01:02,000
And we continue on with verses 104 and 105, which read as follows.

12
00:01:25,000 --> 00:01:32,000
The Danga era, Tataru, Bhasa, Jandunu.

13
00:01:32,000 --> 00:01:38,000
Which means, it's actually similar to the last one.

14
00:01:38,000 --> 00:01:48,000
Conquest of the self is better than the conquest of others, of other people.

15
00:01:48,000 --> 00:02:03,000
But, Bhasa, Nithin, Zantata, you know, for one who has conquered themselves and is constantly restrained.

16
00:02:03,000 --> 00:02:13,000
Sun Nithata, Jadino, one who fares restrained or controlled.

17
00:02:13,000 --> 00:02:27,000
Sent to not giving rise to uncontrolled behavior or activity that causes harm to others.

18
00:02:27,000 --> 00:02:36,000
Nithu, they want to Ghandambu, not an angel, nor a Ghandamba, which is also another type of angel, I guess.

19
00:02:36,000 --> 00:02:45,000
Namaarosa, Habra, Munan, Nadamara, which is like the evil Satan, nor Brahma's gods.

20
00:02:45,000 --> 00:02:59,000
Jitanga, Bajidanga, Ira, can turn their victory into the feet. Jitanga, Apajitanga, Apajita is the feet.

21
00:02:59,000 --> 00:03:14,000
Jitanga, Apajita is turning the victory into your feet. They will not be able to turn the victory of such a person to defeat.

22
00:03:14,000 --> 00:03:25,000
So, great verse shows the power of self-conquest.

23
00:03:25,000 --> 00:03:37,000
Simple story, story goes, there was a Brahmin who came to the Buddha and asked him a question.

24
00:03:37,000 --> 00:03:47,000
I guess maybe he was a follower of the Buddha or he understood how worthy the Buddha was, or how wise the Buddha was.

25
00:03:47,000 --> 00:03:59,000
And so he had this question. He wondered to himself whether the Buddha, whether Buddha is, whether those who are enlightened,

26
00:03:59,000 --> 00:04:15,000
know only what is a benefit, or do they also know what leads to detriment? Do they only know about gain or do they know about loss as well?

27
00:04:15,000 --> 00:04:44,000
Or do they also know what is determined, which is harmful or leads one away from success, leads one away from what is useful, which is useless, or harmful.

28
00:04:44,000 --> 00:04:58,000
And so he went to ask the Buddha this. I guess he had some idea that the Buddha only taught people good things. He only taught several was praising how the Buddha's teachings would lead people to good things.

29
00:04:58,000 --> 00:05:07,000
And they were talking about how great the Buddha's teaching Buddha would teach you, do this to that. And so he hadn't heard the Buddha talk about, don't do this, don't do that.

30
00:05:07,000 --> 00:05:19,000
And so he went to the Buddha and the Buddha gave him an interesting quote. When I was just having a terrible time with it because the English is, it's incredible how sloppy they were with it.

31
00:05:19,000 --> 00:05:38,000
And translate fierceness, Chandikal, as moonlight, which is, well, I mean, it's understandable because Chanda would be the moon, but Chanda with the, with the, how the

32
00:05:38,000 --> 00:05:52,000
intellectual ending, anyway, doesn't mean moon at all, unless there's another version that that confuses the two, but it means porosity. So the Buddha was, the Buddha says gives a list of six things.

33
00:05:52,000 --> 00:05:57,000
And I think I've got them all down.

34
00:05:57,000 --> 00:06:13,000
So he goes and asks the Buddha, do you know about loss as well, and the Buddha says, of course, and he tells them. These, he says, a tancha, hung brahmana, jhana, me, a nacha, I know both that which is a benefit and that which is to detriment.

35
00:06:13,000 --> 00:06:34,000
And so he taught, says, please then tell me about what is to detriment and the Buddha says, well, then listen. And he gives this verse, which isn't the dumb upon verse, but there's, this is often the case with these stories, there will be other verses that, where they come from exactly we don't know where they've been recorded.

36
00:06:34,000 --> 00:06:47,000
I don't know, but it's supposed to have been said by the Buddha as well. So he says, usura say young, which means clear, that one's clear, it means sleeping beyond sunrise, so sleeping during the day.

37
00:06:47,000 --> 00:06:53,000
It's time to get up and go to work, one instead stays in bed and doesn't do one's work.

38
00:06:53,000 --> 00:07:14,000
Al as young laziness, and one is just lazy and doesn't want to do good things. Tendi kang means again, ferocity, so being mean or nasty or cruel, being, you know, when people are overbearing and just constantly irritable.

39
00:07:14,000 --> 00:07:30,000
And nika sondi young is an interesting one, but I think, because nika means long, but sondi means, means addiction to intoxication or addiction to drink, usually refers to, and maybe intoxication is better.

40
00:07:30,000 --> 00:07:58,000
I think it says something to do with drinking and toxicating drinks, and it may be a corruption, but I probably just don't know what it means. Anyway, but the English is terrible, it doesn't have any event, what does it say, long continued prosperity, which is ridiculous, because nika is long, but long continued prosperity is not a cause for ruin, except it can make you negligent, I suppose, but that's certainly not what's being said here.

41
00:07:58,000 --> 00:08:19,000
And then we've got, I think it's addiction to strong drink, why? Because these come from the, or these are echoed in the, and the degan ikaya suta.

42
00:08:19,000 --> 00:08:33,000
The sigalawada suta, the discourse, the sigalawada suta talks about these various states that lead to loss, right? And addiction to drink is, of course, one of them.

43
00:08:33,000 --> 00:09:00,000
Nika sada nangamana, so go, I believe means traveling alone, which they just translate as going on long journeys, but going on journeys doesn't lead to loss, going on long journeys alone, can lead to problems in the time of the Buddha, it certainly could because you could get robbed, was a big deal if you traveled alone, wasn't it, just wasn't a good idea.

44
00:09:00,000 --> 00:09:12,000
And finally, Barada rupa siva nam, which means chasing after other people's wives, which was apparently a thing, or is really a thing.

45
00:09:12,000 --> 00:09:23,000
People come in adultery all the time, bad idea leads to loss, leads to disrespect, leads, leads to loss of friendship, etc.

46
00:09:23,000 --> 00:09:35,000
So he taught these things and maybe he taught others, but that's the sort of things he taught, certainly in the sigalawada suta, he taught many other things that are to your detriment.

47
00:09:35,000 --> 00:09:46,000
And it's curious, you know, because he doesn't say gambling, and yet gambling is considered one of the things that leads to loss.

48
00:09:46,000 --> 00:09:51,000
I don't believe it's in here, but I've translated it correctly, it's not there.

49
00:09:51,000 --> 00:09:55,000
And in the sigalawada suta, it's clearly there.

50
00:09:55,000 --> 00:10:04,000
And that's interesting because the Brahman is then impressed and it says very good, very good, that's awesome.

51
00:10:04,000 --> 00:10:13,000
Well spoken, you're really, really a great teacher and you really know what is a benefit and what is a detriment.

52
00:10:13,000 --> 00:10:19,000
And then the Buddha asks him, so Brahman, how do you make your livelihood?

53
00:10:19,000 --> 00:10:25,000
And turns out, the Brahman makes this livelihood through gambling.

54
00:10:25,000 --> 00:10:27,000
That's how he makes a living.

55
00:10:27,000 --> 00:10:39,000
I guess he must be sort of one of these people who are a con artist, right?

56
00:10:39,000 --> 00:10:47,000
Or something like that, how to make is, I guess there are professional poker players nowadays, it's a big thing, apparently, to gamble online.

57
00:10:47,000 --> 00:10:56,000
I have people who tell me about that, how they do gambling online.

58
00:10:56,000 --> 00:11:09,000
And so it's interesting that the Buddha didn't include it, but that is common to be very careful not to attack a person directly, right?

59
00:11:09,000 --> 00:11:18,000
He had brought up gambling.

60
00:11:18,000 --> 00:11:29,000
He doesn't bring it up, but yet he wants to make it clear that gambling is in there as well, without actually saying all gambling is going to lead you to ruin.

61
00:11:29,000 --> 00:11:47,000
It's as though he purposefully left it out or it's this story, part of the reason for telling this story is to, or part of the meaning behind the story is to show that he didn't attack him directly.

62
00:11:47,000 --> 00:11:50,000
It's quite curious.

63
00:11:50,000 --> 00:12:02,000
But can be a really important aspect of teaching, so anyway, point is he tells him, doesn't make the ramen says he makes his living by gambling.

64
00:12:02,000 --> 00:12:18,000
And this is the funny sort of inside joke that we can kind of smile because we know it's kind of what's going through the Buddha's mind to some extent that he's.

65
00:12:18,000 --> 00:12:26,000
He knows that this ramen is on a state of loss and is heading in a bad way.

66
00:12:26,000 --> 00:12:27,000
Anyway.

67
00:12:27,000 --> 00:12:31,000
And so then the Buddha asks him, so who wins when you gamble?

68
00:12:31,000 --> 00:12:34,000
Do you win or does your opponents win?

69
00:12:34,000 --> 00:12:39,000
He says, oh, well, sometimes I win, sometimes my opponents win.

70
00:12:39,000 --> 00:12:47,000
And then the Buddha says, and Buddha lays down the law and says, Ramana, that's not a real victory.

71
00:12:47,000 --> 00:12:50,000
As you can see, that's the nature of victory.

72
00:12:50,000 --> 00:12:53,000
Sometimes you win, sometimes you lose.

73
00:12:53,000 --> 00:12:56,000
We have this saying, you win some, you lose some.

74
00:12:56,000 --> 00:13:00,000
Because it's not a really, it means that on balance you come out with nothing, right?

75
00:13:00,000 --> 00:13:02,000
You never come out ahead.

76
00:13:02,000 --> 00:13:05,000
Or it's certainly not guaranteed that you're going to come out ahead.

77
00:13:05,000 --> 00:13:12,000
And there must be some other factors often people would say luck is all that allows you to come out ahead.

78
00:13:12,000 --> 00:13:17,000
And as we've talked about, of course, it doesn't matter whether you come on ahead or not.

79
00:13:17,000 --> 00:13:24,000
When you come on ahead, what happens while you, you get enmity, your opponents are upset because they've lost.

80
00:13:24,000 --> 00:13:27,000
When you win, someone has to lose.

81
00:13:27,000 --> 00:13:31,000
That kind of victory is not really, he says, a trifling.

82
00:13:31,000 --> 00:13:33,000
And that's how the English translate it.

83
00:13:33,000 --> 00:13:35,000
You can't really trust the English.

84
00:13:35,000 --> 00:13:48,000
That's a, that's a low sort of, believe, jail, no, JOP, or deep, but on JOP.

85
00:13:48,000 --> 00:13:49,000
That's a question.

86
00:13:49,000 --> 00:13:50,000
Sorry.

87
00:13:50,000 --> 00:13:53,000
From an upper medical, yeah, so it's a trifle.

88
00:13:53,000 --> 00:14:00,000
It doesn't say that's a bad kind of victory or that kind of victory is actually leading you to ruin, which in fact it is.

89
00:14:00,000 --> 00:14:08,000
He says that's a trifling victory. So he, he, in a roundabout way, he's trying to get this brahmana to see the error of his ways.

90
00:14:08,000 --> 00:14:10,000
You'll get provided with some friendly advice.

91
00:14:10,000 --> 00:14:13,000
The brahmana did ask after all what his leads to loss.

92
00:14:13,000 --> 00:14:19,000
And so he's trying to somehow tell him that his lifestyle does that.

93
00:14:19,000 --> 00:14:24,000
And he's, but instead he says, so to be very gentle, he says that's a trifling victory.

94
00:14:24,000 --> 00:14:26,000
A real victory is self victory.

95
00:14:26,000 --> 00:14:29,000
Real conquest, that's conquering yourself.

96
00:14:29,000 --> 00:14:34,000
Why? Because that kind of conquest, you'll never lose.

97
00:14:34,000 --> 00:14:41,000
And so it's poignant in, in relation to the story, the idea of being invincible.

98
00:14:41,000 --> 00:14:48,000
Because again, the, the win of a gambler is always, it's always subject to threat.

99
00:14:48,000 --> 00:14:57,000
The next role of the dice, the next deal of the cards couldn't be, couldn't change your fortune, couldn't spell ruin.

100
00:14:57,000 --> 00:15:07,000
Whereas, and much victory is like this victory in war, victory in business, victory in romance, victory.

101
00:15:07,000 --> 00:15:16,000
Of all kinds is unpredictable and is, in stable is imperman.

102
00:15:16,000 --> 00:15:18,000
It doesn't last forever.

103
00:15:18,000 --> 00:15:27,000
No, it's always subject to the potential of being overdone overthrown.

104
00:15:27,000 --> 00:15:29,000
But that's really the claim that's being made.

105
00:15:29,000 --> 00:15:32,000
So this is how it relates to our practices.

106
00:15:32,000 --> 00:15:40,000
It's, it's an understanding of what of the, the extent in the magnitude of the practice that we're undertaking.

107
00:15:40,000 --> 00:15:42,000
I mean, this isn't just stress really.

108
00:15:42,000 --> 00:15:47,000
I was thinking today about advertising the benefits of meditation.

109
00:15:47,000 --> 00:15:51,000
We're probably going to put up posters.

110
00:15:51,000 --> 00:16:00,000
And I mean, we think of, you know, people really, really, it's really catchy to say mindfulness based stress reduction.

111
00:16:00,000 --> 00:16:03,000
Because it's a non-sectarian.

112
00:16:03,000 --> 00:16:04,000
It's non-religious.

113
00:16:04,000 --> 00:16:09,000
It's really something that people can sort of rolls off the tongue.

114
00:16:09,000 --> 00:16:14,000
But then I thought it's kind of, you know, petty to say stress reduction.

115
00:16:14,000 --> 00:16:18,000
Because that's not really what we're talking about, is it in Buddhism.

116
00:16:18,000 --> 00:16:22,000
We go quite, it's mindfulness based or it's based on setting.

117
00:16:22,000 --> 00:16:25,000
But it goes farther than just reducing stress.

118
00:16:25,000 --> 00:16:27,000
It up roots.

119
00:16:27,000 --> 00:16:30,000
Anything that could possibly cause you stress in the future.

120
00:16:30,000 --> 00:16:37,000
There's no potential for future stress.

121
00:16:37,000 --> 00:16:39,000
Meaning invincible.

122
00:16:39,000 --> 00:16:41,000
And it goes beyond heaven.

123
00:16:41,000 --> 00:16:46,000
It goes beyond God, it goes beyond the universe.

124
00:16:46,000 --> 00:16:51,000
Something that goes beyond loss.

125
00:16:51,000 --> 00:16:55,000
Something that can never be taken from you.

126
00:16:55,000 --> 00:17:00,000
We're talking about invincibility.

127
00:17:00,000 --> 00:17:02,000
How does this come about?

128
00:17:02,000 --> 00:17:11,000
It comes about by a person who is at the Danta, Bosa.

129
00:17:11,000 --> 00:17:19,000
Neepjang Sanyatta Charino, who is constantly, constantly restrained.

130
00:17:19,000 --> 00:17:24,000
I mean, they never, when seeing a form with the eye,

131
00:17:24,000 --> 00:17:27,000
they never give rise to likes or dislikes.

132
00:17:27,000 --> 00:17:31,000
They see it as it is and they're objective.

133
00:17:31,000 --> 00:17:38,000
They free themselves from the potential for,

134
00:17:38,000 --> 00:17:46,000
probably free themselves from, from involvement in this game of gain and loss.

135
00:17:46,000 --> 00:17:50,000
Happiness and suffering.

136
00:17:50,000 --> 00:17:57,000
It sounds, sounds actually quite, quite fearsome for people who are addicted to pleasure

137
00:17:57,000 --> 00:18:03,000
and who are caught up in this game of gain and loss.

138
00:18:03,000 --> 00:18:10,000
So it's something that you do have to kind of finesse when you talk to people.

139
00:18:10,000 --> 00:18:16,000
And people often accuse, make the, make the accusation that such a person would be

140
00:18:16,000 --> 00:18:18,000
someone like a zombie, right?

141
00:18:18,000 --> 00:18:22,000
When they are restrained,

142
00:18:22,000 --> 00:18:28,000
you know, means they are just suppressing their urges, which isn't at all the case.

143
00:18:28,000 --> 00:18:30,000
This is suppressing urges wouldn't work.

144
00:18:30,000 --> 00:18:34,000
It's not something that is sustainable.

145
00:18:34,000 --> 00:18:38,000
It's not the true victory.

146
00:18:38,000 --> 00:18:46,000
True victory is, is, is invincible,

147
00:18:46,000 --> 00:18:48,000
imperturbable.

148
00:18:48,000 --> 00:18:52,000
I was thinking about this today, the whole idea of the zombie, right?

149
00:18:52,000 --> 00:18:58,000
So we have this idea that a person who doesn't like and dislike things is just like a zombie.

150
00:18:58,000 --> 00:19:01,000
Their life has no flavor, that's the meaning.

151
00:19:01,000 --> 00:19:04,000
But then if you look at people who are caught up,

152
00:19:04,000 --> 00:19:07,000
you know, if you think of how, how,

153
00:19:07,000 --> 00:19:12,000
the state of mind, the person who's caught up in chasing after things,

154
00:19:12,000 --> 00:19:14,000
the objects of their desire,

155
00:19:14,000 --> 00:19:17,000
how much like a zombie they are,

156
00:19:17,000 --> 00:19:22,000
how unaware they are of their own mind, their own state,

157
00:19:22,000 --> 00:19:25,000
how inflamed the mind becomes,

158
00:19:25,000 --> 00:19:32,000
with passion and hate and how the mind just in general becomes inflamed.

159
00:19:32,000 --> 00:19:37,000
And so it's subject to greed, anger and delusion constant.

160
00:19:37,000 --> 00:19:43,000
Sometimes a such intense greed that they will do anything to get what they want,

161
00:19:43,000 --> 00:19:50,000
even at the, at the detriment to other people, even at the cost of friendship.

162
00:19:50,000 --> 00:19:53,000
Sometimes I'll give rise to such states,

163
00:19:53,000 --> 00:20:00,000
some delusion that they alienate all of their friends

164
00:20:00,000 --> 00:20:04,000
due to their oppressive arrogant,

165
00:20:04,000 --> 00:20:07,000
conceited nature.

166
00:20:07,000 --> 00:20:11,000
The level of conceit that were capable of the level of arrogance

167
00:20:11,000 --> 00:20:17,000
and how awful it is and how harmful it is.

168
00:20:17,000 --> 00:20:20,000
And anger, how it can be.

169
00:20:20,000 --> 00:20:27,000
So caught up with rage and hatred constantly

170
00:20:27,000 --> 00:20:32,000
depressed or angry, sad, afraid,

171
00:20:32,000 --> 00:20:37,000
all of these negative states.

172
00:20:37,000 --> 00:20:42,000
It's kind of like a zombie, like one of those zombie movies or zombie shows

173
00:20:42,000 --> 00:20:45,000
that everyone's watching.

174
00:20:45,000 --> 00:20:49,000
Evil zombies, how horrific they are.

175
00:20:49,000 --> 00:20:50,000
That's kind of what we're talking about.

176
00:20:50,000 --> 00:20:53,000
The lightning people are nothing like that.

177
00:20:53,000 --> 00:20:55,000
The lightning one is the opposite of a zombie.

178
00:20:55,000 --> 00:20:57,000
They are awake.

179
00:20:57,000 --> 00:20:59,000
They are alive.

180
00:20:59,000 --> 00:21:01,000
They never die.

181
00:21:01,000 --> 00:21:03,000
They're invincible.

182
00:21:03,000 --> 00:21:07,000
So this is what we're striving for. It's something quite profound.

183
00:21:07,000 --> 00:21:09,000
I teach meditation,

184
00:21:09,000 --> 00:21:12,000
not teaching at the university, and I teach to all sorts of people.

185
00:21:12,000 --> 00:21:18,000
And a lot of people are just in it for stress reductions, stress relief.

186
00:21:18,000 --> 00:21:21,000
And so it kind of pulls you in and you start to realize,

187
00:21:21,000 --> 00:21:22,000
oh, it goes a little bit deeper.

188
00:21:22,000 --> 00:21:25,000
And you end up down the rabbit hole, so to speak,

189
00:21:25,000 --> 00:21:30,000
where you don't recognize the world around you anymore.

190
00:21:30,000 --> 00:21:32,000
Your whole world changes.

191
00:21:32,000 --> 00:21:34,000
It's not like it's a trap or something.

192
00:21:34,000 --> 00:21:37,000
It's just the sweater starts to unravel.

193
00:21:37,000 --> 00:21:45,000
The whole illusion that you built up around yourself.

194
00:21:45,000 --> 00:21:48,000
And who you thought you were starts to unravel.

195
00:21:48,000 --> 00:21:50,000
And you see how asleep you've been.

196
00:21:50,000 --> 00:21:53,000
You're like a sleepwalk or like a zombie.

197
00:21:53,000 --> 00:21:54,000
That's how it should be.

198
00:21:54,000 --> 00:21:56,000
That's how it really is in meditation.

199
00:21:56,000 --> 00:21:59,000
You start to wake up and see, oh, I've got these problems.

200
00:21:59,000 --> 00:22:04,000
My mind has agreed, my mind has anger, my mind has delusion.

201
00:22:04,000 --> 00:22:06,000
Well, boy, I'm in trouble.

202
00:22:06,000 --> 00:22:11,000
And so he's starting to do the hard work of changing that.

203
00:22:11,000 --> 00:22:13,000
That's what we do in meditation.

204
00:22:13,000 --> 00:22:17,000
So, and that is the highest victory.

205
00:22:17,000 --> 00:22:20,000
It's a victory that no one can take from us.

206
00:22:20,000 --> 00:22:23,000
That's the point of this verse, and it's very well said.

207
00:22:23,000 --> 00:22:26,000
As are all the things the Buddha said.

208
00:22:26,000 --> 00:22:31,000
So, I think that we should appreciate and remember to give us confidence

209
00:22:31,000 --> 00:22:34,000
about what we're aiming for is not something simple.

210
00:22:34,000 --> 00:22:40,000
It's not just a hobby that you can pick up and put down something

211
00:22:40,000 --> 00:22:45,000
that really and truly changes your life, transforms your way, wakes you up

212
00:22:45,000 --> 00:22:49,000
and helps you rise above your troubles.

213
00:22:49,000 --> 00:22:52,000
So that even though the situation may not change,

214
00:22:52,000 --> 00:22:55,000
no matter what the situation is,

215
00:22:55,000 --> 00:22:57,000
you are above it.

216
00:22:57,000 --> 00:22:59,000
You are beyond it.

217
00:22:59,000 --> 00:23:02,000
So, that's the teaching for tonight.

218
00:23:02,000 --> 00:23:05,000
This verse is 104 and 105.

219
00:23:05,000 --> 00:23:08,000
We're almost a quarter of the way through the Dhammapanda.

220
00:23:08,000 --> 00:23:14,000
I think when we get to 108, that'll be one quarter of the way.

221
00:23:14,000 --> 00:23:15,000
So, thank everyone.

222
00:23:15,000 --> 00:23:17,000
Thank you all for tuning in.

223
00:23:17,000 --> 00:23:21,000
I'm wishing you all the best on your path to invincibility.

224
00:23:21,000 --> 00:23:26,000
We will.

225
00:23:26,000 --> 00:23:45,000
We're still there, Robin.

226
00:23:45,000 --> 00:23:50,000
I am, yes.

227
00:23:50,000 --> 00:23:57,000
And we have questions.

228
00:23:57,000 --> 00:24:00,000
It wasn't live until then.

229
00:24:00,000 --> 00:24:05,000
What happened there?

230
00:24:05,000 --> 00:24:09,000
It wasn't on the live stream.

231
00:24:09,000 --> 00:24:10,000
Sorry.

232
00:24:10,000 --> 00:24:13,000
It wasn't on the live stream.

233
00:24:13,000 --> 00:24:23,000
No, the hangout didn't broadcast.

234
00:24:23,000 --> 00:24:26,000
It's just broadcast.

235
00:24:26,000 --> 00:24:34,000
There was a new button that says post publicly or something.

236
00:24:34,000 --> 00:24:40,000
I'm not going to move the camera.

237
00:24:40,000 --> 00:24:45,000
So, this is the way I'm facing.

238
00:24:45,000 --> 00:24:46,000
That looks fine.

239
00:24:46,000 --> 00:24:47,000
That looks fine.

240
00:24:47,000 --> 00:24:50,000
The computer is over there.

241
00:24:50,000 --> 00:24:51,000
All right.

242
00:24:51,000 --> 00:24:52,000
So, we have questions.

243
00:24:52,000 --> 00:25:01,000
We do.

244
00:25:01,000 --> 00:25:06,000
Sorry.

245
00:25:06,000 --> 00:25:10,000
Before Chapter 5 of how to meditate to powerful stuff.

246
00:25:10,000 --> 00:25:13,000
How long did it take you to write the entire chapter?

247
00:25:13,000 --> 00:25:15,000
The articulation was beautiful.

248
00:25:15,000 --> 00:25:16,000
Thank you.

249
00:25:16,000 --> 00:25:20,000
Well, it's good to get feedback.

250
00:25:20,000 --> 00:25:21,000
It's funny.

251
00:25:21,000 --> 00:25:26,000
I mean, I probably could never have done this without meditation.

252
00:25:26,000 --> 00:25:27,000
I think.

253
00:25:27,000 --> 00:25:28,000
That took me.

254
00:25:28,000 --> 00:25:30,000
I wrote that in one sitting.

255
00:25:30,000 --> 00:25:33,000
I've written almost all the chapters in one sitting.

256
00:25:33,000 --> 00:25:36,000
There's editing that goes into it.

257
00:25:36,000 --> 00:25:39,000
But it really comes a lot easier.

258
00:25:39,000 --> 00:25:43,000
I guess also because it's the dumb of it.

259
00:25:43,000 --> 00:25:48,000
Writing things, studying, using the mind is a lot easier once you practice meditation.

260
00:25:48,000 --> 00:25:49,000
Right.

261
00:25:49,000 --> 00:25:52,000
I shouldn't brag like that.

262
00:25:52,000 --> 00:25:54,000
That is kind of neat.

263
00:25:54,000 --> 00:25:58,000
You just write stuff.

264
00:25:58,000 --> 00:26:01,000
I don't think that's uncommon, I think.

265
00:26:01,000 --> 00:26:07,000
The people who meditate, their mind works better.

266
00:26:07,000 --> 00:26:12,000
Monday, my daughter Elizabeth, who is 14, is interested in Buddhism and meditation.

267
00:26:12,000 --> 00:26:15,000
Are your meditation techniques the same for youths?

268
00:26:15,000 --> 00:26:18,000
And would you recommend any readings for her to study?

269
00:26:18,000 --> 00:26:19,000
I do.

270
00:26:19,000 --> 00:26:20,000
I do.

271
00:26:20,000 --> 00:26:21,000
I do.

272
00:26:21,000 --> 00:26:23,000
14.

273
00:26:23,000 --> 00:26:28,000
And I do have your writings on meditation for her.

274
00:26:28,000 --> 00:26:35,000
Well, I'd recommend potentially how to meditate for children.

275
00:26:35,000 --> 00:26:42,000
And you can warn her that it's actually designed maybe for younger children, but she can choose.

276
00:26:42,000 --> 00:26:44,000
I would offer her both.

277
00:26:44,000 --> 00:26:47,000
Because adults have found the kids videos useful.

278
00:26:47,000 --> 00:26:48,000
They're quite simple.

279
00:26:48,000 --> 00:26:53,000
So I'd recommend my videos on how to meditate for kids.

280
00:26:53,000 --> 00:27:00,000
But certainly she could also use the book.

281
00:27:00,000 --> 00:27:07,000
I mean, the meditations for kids aren't exclusively insight meditation.

282
00:27:07,000 --> 00:27:16,000
So it may, it's more to the point to give her the actual booklet.

283
00:27:16,000 --> 00:27:19,000
I don't think it's difficult to understand.

284
00:27:19,000 --> 00:27:36,000
But you can also go through it with her, as long as you're following the book, you can teach her to explain to her how to practice.

285
00:27:36,000 --> 00:27:52,000
Venerable you to download is it better to practice compassion towards enemies than meta in the sense that it might be easier for the meditator.

286
00:27:52,000 --> 00:27:59,000
So I got her to practice compassion, then made the question, then love.

287
00:27:59,000 --> 00:28:09,000
It really depends, actually, not actually compassion as we're learning from the VCD manga is best used when you have cruelty in your mind.

288
00:28:09,000 --> 00:28:20,000
So certainly you can feel cruel towards an enemy, but more often I would say you feel anger.

289
00:28:20,000 --> 00:28:31,000
Cruelty you might feel towards someone who is suffering, or someone who is under your care, that kind of thing.

290
00:28:31,000 --> 00:28:38,000
So helping them is out of compassion.

291
00:28:38,000 --> 00:28:52,000
I mean, just to give an example that as a teacher, why? Because it's common referred talked about in Buddhism, not teaching when someone needs someone could use your teaching.

292
00:28:52,000 --> 00:29:07,000
So the Buddha had great compassion because he went out of his way to teach people who needed it.

293
00:29:07,000 --> 00:29:19,000
Would you say that mindfulness slows down the process of the evolution of our rampant minds, or would you say it guides it?

294
00:29:19,000 --> 00:29:39,000
I mean, honestly, I wouldn't say either of those things. So I'm really not quite sure where you're going with that.

295
00:29:39,000 --> 00:29:57,000
I don't understand the evolution, slows down the evolution of our rampant minds. I mean, dude, remember, teach you how to be mindful, you'll overcome suffering.

296
00:29:57,000 --> 00:30:14,000
I guess, okay, if I'm going to talk about slowing down the mind, let's forget about the evolution of the rampant, I don't quite understand the context. But slowing down or guiding, I would say it does both.

297
00:30:14,000 --> 00:30:35,000
But it doesn't naturally, you see? I mean, it's not either one of those things are our goal, our immediate goal and the practice, our goal is to see clearly, to look so that we can see and understand, and that guides and slows the mind naturally.

298
00:30:35,000 --> 00:30:54,000
I mean, the mind generally, because generally the mind, our minds tend to go faster than they should. It also can speed the mind up, make the mind quicker, less lethargic, less lazy, depends on the person.

299
00:30:54,000 --> 00:31:07,000
What should I do when I'm meditating and I feel like it's too hard to continue anymore?

300
00:31:07,000 --> 00:31:28,000
Okay, so look at why it's hard. I mean, sitting still is actually not that hard to do, surprisingly enough. That sitting still is not intrinsically a difficult thing. That's not surprising at all. Walking back and forth, also not all that difficult, unless you've only got one leg or no legs.

301
00:31:28,000 --> 00:31:48,000
It's a pretty simple thing to do. I mean, I had one woman who had multiple sclerosis, and she would do walking by laying herself on her walker and paddling with her legs, like basically pushing against the floor with her legs.

302
00:31:48,000 --> 00:32:12,000
She could move her legs, but just barely. So it doesn't talk about, to me, about heart. I mean, she had it hard. But what you're referring is to is not the fact that it's hard to do the meditation, not that the meditation is hard, but that your mind has something that finds the meditation or palent or a verse to the meditation.

303
00:32:12,000 --> 00:32:23,000
And so you have to figure out what that is. It means you've got something in your mind that dislikes what you're doing or that wants something different. And there can be many reasons for that.

304
00:32:23,000 --> 00:32:36,000
It's often because of our addiction to sensuality. We want to be doing something else. It can be due to our aversion to pain when we have to, it can be a sense of boredom.

305
00:32:36,000 --> 00:32:46,000
I mean, they're actually all quite related, but how it appears to you. And you have to figure out how it appears to you and deal with it according to that.

306
00:32:46,000 --> 00:32:57,000
Noting those things, breaking them up into their individual parts and noting them.

307
00:32:57,000 --> 00:33:05,000
Is noting experience necessary for being mindful or can you be mindful without noting things?

308
00:33:05,000 --> 00:33:10,000
Depends who you ask. In our tradition, the noting is necessary. So if you don't do it, you're not really meditating.

309
00:33:10,000 --> 00:33:19,000
It's not that you're not mindful. Mindfulness is just a part of, I mean, it's a tricky concept. Mindfulness can mean many different things.

310
00:33:19,000 --> 00:33:25,000
But we would say rather that you're not meditating. If you're not noting, you're not really meditating. You're just sitting there.

311
00:33:25,000 --> 00:33:34,000
And so it's hidden miss as to whether you'll progress from our point of view. I know that's highly contentious, but that's how it goes with us.

312
00:33:34,000 --> 00:33:51,000
Dear Bhante, I have questions I'd love to talk to you about, but I can't do it on this public forum. Can I send you an email or even may we talk on the phone if you have time?

313
00:33:51,000 --> 00:34:07,000
It's hard, you know, because I get lots of emails. And I've kind of, you know, just in the end, you have to cut some. You have to at some point say, there's the limit.

314
00:34:07,000 --> 00:34:24,000
So I'm I'm I'm happy to talk over the phone, but only if it's related to your meditation practice. There's lots of questions you might have to ask me.

315
00:34:24,000 --> 00:34:32,000
I don't know what your questions are about, but there's not much I can do to solve your problems.

316
00:34:32,000 --> 00:34:44,000
If you want to learn how to meditate on practice meditation, that can certainly help your practice, help your life, your problem.

317
00:34:44,000 --> 00:35:00,000
So I'd recommend that reading my booklet. And then if you're interested in if you have questions about the meditation, problems with the meditation, then I mean those are the kind of things you should be able to ask on here.

318
00:35:00,000 --> 00:35:21,000
In fact, it's probably not probably it may be a surprise, but for me to say this and maybe not even very welcome to say it, but really anything you can't ask on here, it's not a question that is really necessary to ask.

319
00:35:21,000 --> 00:35:41,000
Or maybe not even necessary, but it's just it probably falls too close too far into the realm of of that home life for me to even really see because then I have to give you an answer about your life, which has so many variables that I don't know and is so complicated.

320
00:35:41,000 --> 00:35:53,000
And it ends up not really being the problem and the problem ends up just being states of mind that can be dealt with in meditation. So in the end it just does come back to questions about meditation.

321
00:35:53,000 --> 00:36:00,000
So that's what I'd recommend. Learn how to meditate and then start asking the right questions.

322
00:36:00,000 --> 00:36:07,000
Probably not a very popular answer, but it really does come down to that.

323
00:36:07,000 --> 00:36:19,000
If if having heard that you still feel that you need some private time, then I'm sure we can range it. In fact, a lot of meditation questions are private, but that's why we're doing these courses.

324
00:36:19,000 --> 00:36:22,000
Again, it's about prioritizing.

325
00:36:22,000 --> 00:36:26,000
So I have 28 slots per week.

326
00:36:26,000 --> 00:36:43,000
It was 28 different people right now. It's about 24. And those are people who are doing two hours or one at least up to at least two hours, eventually up to two hours, at least the meditation a day.

327
00:36:43,000 --> 00:36:47,000
And that keeps me pretty busy.

328
00:36:47,000 --> 00:36:58,000
So then I come on here to answer general questions where people who aren't able to do those courses. There we hope that kind of clarifies my stance.

329
00:36:58,000 --> 00:37:19,000
If you can get through to me and I won't, I won't turn down a phone call, but I'll leave it up to you to figure out where to go next.

330
00:37:19,000 --> 00:37:29,000
It looks like we're all quite a bunch of questions.

331
00:37:29,000 --> 00:37:57,000
Thank you, Dante. Thank you, Robin, for your help. And thanks everyone for tuning in. Have a good night. Good night.

