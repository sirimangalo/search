1
00:00:00,000 --> 00:00:06,160
In addition to the meditation retreats, do you have classes in Pali and regular

2
00:00:06,160 --> 00:00:12,000
pseudo-study sessions for the meditators and monastics?

3
00:00:12,000 --> 00:00:23,040
It's a good question. We haven't established a time table yet. We did do

4
00:00:23,040 --> 00:00:39,040
Pali courses before Pali and Yani went back to Thailand. So we stopped them when she went away and we never reconvened them because Nagasein lost interest.

5
00:00:39,040 --> 00:00:49,040
I don't know. They're not begging me to give Pali courses, so I'm not doing them. If they beg me, we wanted to wait until...

6
00:00:49,040 --> 00:00:56,040
Just a little bit postponed. We wanted to wait because we have another

7
00:00:56,040 --> 00:01:06,040
sumeda, our new name is Sumeda, which means wise one.

8
00:01:06,040 --> 00:01:11,040
We wanted to wait until she has finished her courses and can join in on the Pali.

9
00:01:11,040 --> 00:01:21,040
So to study we haven't done either. Well, we did the Saturday suit to study online and that was actually Nagasein as ideally.

10
00:01:21,040 --> 00:01:28,040
So he was listening in on that and that was kind of appeasing that. But now that we have a larger community, I think it would be good.

11
00:01:28,040 --> 00:01:36,040
We could sit around and even discuss the suit. I wouldn't have to lead it. I could just give my comments.

12
00:01:36,040 --> 00:01:42,040
That might be interesting. But yeah, I've done it before. Actually, I did it into a suit. We started up a bunch of courses.

13
00:01:42,040 --> 00:01:52,040
It's always nice to put those on the website to really let people know that we have a fully functioning place of learning.

14
00:01:52,040 --> 00:02:02,040
That on top of just coming here for meditation meditation meditation, you can also broaden your understanding of the meditation teachings.

15
00:02:02,040 --> 00:02:12,040
So even to the extent of learning and the language of the Buddha, which is a very, you know, if you have the time and the effort, it's an incredible thing.

16
00:02:12,040 --> 00:02:20,040
Anyone who's learned it will, we're very much appreciate the ability to get that much closer to what the Buddha actually said.

17
00:02:20,040 --> 00:02:35,040
T.J. writes something in regards to that. He says, not him.

18
00:02:35,040 --> 00:02:45,040
I mean, we had the online course study session and then there's also the poly forum, which is not really active yet.

19
00:02:45,040 --> 00:02:54,040
The part that I thought was interesting is letting people know that we do have local, local things.

20
00:02:54,040 --> 00:03:02,040
I think Maranu, the city, has actually, he was in this person was involved with the suit to study or was visiting it.

21
00:03:02,040 --> 00:03:12,040
I'm quite sure they know about it anyway. I think they were asking about local, which we are trying to set up.

22
00:03:12,040 --> 00:03:17,040
Of course, we're really a lot of these issues. We're still in the infancy here.

23
00:03:17,040 --> 00:03:23,040
So we're just trying to make sure we have electricity and we're still trying to figure out our water situation.

24
00:03:23,040 --> 00:03:29,040
I'm still asking every day why the well isn't being dug.

25
00:03:29,040 --> 00:03:32,040
And every day Manju says, I'll call him, I'll call him.

26
00:03:32,040 --> 00:03:36,040
And then I ask the next day, yes, yes, I have to call him.

27
00:03:36,040 --> 00:03:42,040
Yes, no, so my answer is always to say today.

28
00:03:42,040 --> 00:03:48,040
They'll have to dig the well with this machine when they do it today.

29
00:03:48,040 --> 00:03:54,040
Because I know they won't do it today, but that makes it clear that it should be done soon.

30
00:03:54,040 --> 00:04:03,040
Anyway, we have, we're sort of a bit preoccupied on many fronts as well.

31
00:04:03,040 --> 00:04:13,040
So once we get established and settle down, I think for sure we will have various activities.

32
00:04:13,040 --> 00:04:21,040
I mean, one thing is, these daily talks that I've been giving could be supplanted sometimes with other people,

33
00:04:21,040 --> 00:04:25,040
even if they didn't want to give a talk, they could do readings from suitors.

34
00:04:25,040 --> 00:04:41,040
So that might be beneficial in terms of helping us to study proactively study the Buddha's teaching by hearing people recite the suitors in English, of course.

