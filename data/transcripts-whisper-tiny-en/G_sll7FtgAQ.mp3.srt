1
00:00:00,000 --> 00:00:05,000
Hi everyone, welcome back to Ask a Monk.

2
00:00:05,000 --> 00:00:12,000
I'm getting a lot of different questions here, and I'm probably not going to be able to answer all of them. I'll try my best.

3
00:00:12,000 --> 00:00:22,000
But some of the questions I get reoccur are related, and so I'm going to try to answer some of them in groups.

4
00:00:22,000 --> 00:00:34,000
The first group that I often get and I've got for a long time and I've always avoided answering is about monks' life and about my life in specific.

5
00:00:34,000 --> 00:00:46,000
And honestly, I don't think my life is a particularly good example of what a monk should be. I've had an interesting and tumultuous monks' life.

6
00:00:46,000 --> 00:00:57,000
Although, you know, it might be indicative of the times and exemplary or a good example of what you have to put up with in becoming a monk.

7
00:00:57,000 --> 00:01:10,000
But becoming a monk is basically giving up all of the external attachments that we have in favor of a more protected lifestyle.

8
00:01:10,000 --> 00:01:22,000
And by protected, I really mean protected from oneself. So a lot of the temptations in an external sense are gone.

9
00:01:22,000 --> 00:01:42,000
We're surrounded by Buddha's teaching, we're surrounded by meditators, we're surrounded by other people doing the same sort of thing or interested in mental development. And so we have a much greater chance of developing ourselves in the meditation practice.

10
00:01:42,000 --> 00:01:54,000
Well, at the same time, a lot of the things that would tempt us to get caught up in addiction and get caught up in other ways and wasting our time are gone.

11
00:01:54,000 --> 00:02:08,000
Of course, it's not perfect and it certainly is an external aspect of our life. There's nothing to say that a monk is immediately going to become enlightened or that a lay person, an ordinary person can't become enlightened.

12
00:02:08,000 --> 00:02:22,000
But it certainly helps. The reason I became a monk is specifically that that I figured it would protect me from a lot of the bad habits and bad tendencies that I had and indeed it has.

13
00:02:22,000 --> 00:02:32,000
It's been a really good aid for me to allow me to grow without getting off track.

14
00:02:32,000 --> 00:02:44,000
As far as becoming a monk, it can be quite difficult nowadays. It's hard to find a place that's going to accept you to ordain for more than a short period of time.

15
00:02:44,000 --> 00:03:05,000
I myself am very much keen on having people or day-night simply because I don't see much of a difference between the two ordinary life and the monk's life. Since it's simply an externality, I mean, a person who comes to meditate with me and doesn't become a monk.

16
00:03:05,000 --> 00:03:21,000
A person who comes to become a monk, I don't see the difference. I don't differentiate the two. If you come to meditate, you come to meditate. The reason you become a monk, in my mind, is because you want to dedicate yourself to long-term meditation and following this path.

17
00:03:21,000 --> 00:03:35,000
I'm keen to have people ordain once I become eligible to ordain people who are going to try to ordain our students as novices first and give them the basic precepts.

18
00:03:35,000 --> 00:03:52,000
The basic precepts of being a monk are chastity and poverty, and the other one is the abstinence from entertainment. But the big ones are chastity and poverty.

19
00:03:52,000 --> 00:04:10,000
The difference between a meditator and a monk, the monk has to give up money. Dedicate themselves to the life of poverty, so you're wearing only one set of robes, and when you watch, when I watch this one, I wear my second robe. You have three robes all together.

20
00:04:10,000 --> 00:04:29,000
And not keeping food, not touching money, and so on. So using things which are of benefit, I mean I even go so far as using obviously a computer, but not keeping things and not clinging to things and not living luxuriously.

21
00:04:29,000 --> 00:04:40,000
So you sleep on the floor, but a lot of this is very similar to how meditators live. The difference is a meditator is short-term, and for a monk it's long-term.

22
00:04:40,000 --> 00:04:52,000
As far as ceremonies and preparing and so on, that's really just technical details. The ordination of a monk is not a ceremony. It shouldn't be considered to be a ceremony.

23
00:04:52,000 --> 00:05:08,000
It's an act. It's a formal act of the Buddhist monks. It's like the inauguration of a president or so on or of a member of Congress. You have to go through a certain process, screening process and a confirmation process.

24
00:05:08,000 --> 00:05:17,000
And once you've gone through that, then you can ordain. It shouldn't be considered a ritual, a ceremony, or some kind of religious observance.

25
00:05:17,000 --> 00:05:26,000
You know, joining a club, so to speak, or joining a society, and you have to be screened and so on.

26
00:05:26,000 --> 00:05:39,000
Okay, so those are some thoughts on the monk's life. One last thing that I'd like to do is give you a link to a very good, something that's much better than anything I could say on the monk's life.

27
00:05:39,000 --> 00:05:59,000
It's very poetic and very to the point. It's something that I always like to read again and again, written by an novice monk, actually, who ordained when he was old, and wrote this in defense of the monastic life.

28
00:05:59,000 --> 00:06:14,000
Okay, so there's a link in the description of this video. Please follow that link if you're interested in understanding that the real nature of going forth, of leaving behind the home life, which is what we do in our day.

29
00:06:14,000 --> 00:06:21,000
Okay, so thanks for the questions. I'll try to get to some more of them tonight and in the next few days. Of course, I'm busy.

30
00:06:21,000 --> 00:06:30,000
In real life, but I'll do my best. Okay.

