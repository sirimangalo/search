1
00:00:00,000 --> 00:00:04,000
Something I have noticed with myself is lost.

2
00:00:04,000 --> 00:00:12,000
Despite my meditation, I keep seeming to get dragged down by this back to square one.

3
00:00:12,000 --> 00:00:19,000
There are times it doesn't get to me, but is this normal for someone that still feels new to Buddhism?

4
00:00:19,000 --> 00:00:27,000
Yes, this is very normal because Buddhism isn't a quick fix.

5
00:00:27,000 --> 00:00:35,000
Buddhism is in fact teaching you that there is no fix, that you can't turn your emotions on and off.

6
00:00:35,000 --> 00:00:44,000
And the way to deal with any emotion, including last, is to realize this, is to come to let go of it.

7
00:00:44,000 --> 00:00:52,000
You actually get to the point where you're so disgusted by not the emotions themselves, but by the whole process,

8
00:00:52,000 --> 00:00:58,000
which you become kind of horrified in the sense of how uncontrollable it all is.

9
00:00:58,000 --> 00:01:04,000
Whereas before you thought, yeah, I like this and I'll just be able to turn it on and off and on.

10
00:01:04,000 --> 00:01:10,000
And when you practice, you start coming up and it comes up when it's not supposed to come up.

11
00:01:10,000 --> 00:01:15,000
It doesn't always does anyway, but you didn't notice it until you started meditating.

12
00:01:15,000 --> 00:01:31,000
You see that it comes up when it's not supposed to and you start, you know, lusting after your best friend's girlfriend and so on, or whatever.

13
00:01:31,000 --> 00:01:46,000
And it begins, you know, in meditation, it begins to horrifying, or it begins to begin to become disgusted with this whole process and realize that it's not really manageable, it's not really positive.

14
00:01:46,000 --> 00:01:50,000
It's not bringing you happiness, it's not bringing you any benefit.

15
00:01:50,000 --> 00:02:00,000
It's only disturbing your peace and bringing you more chaos in your mind and dulling your senses and so on.

16
00:02:00,000 --> 00:02:03,000
And so you slowly give it up.

17
00:02:03,000 --> 00:02:15,000
During that time, you have to understand that you've been doing the opposite for most of your life, most likely you've been encouraging it.

18
00:02:15,000 --> 00:02:26,000
You've been doing things that bring about more or less and reaffirm the idea of how good it is.

19
00:02:26,000 --> 00:02:31,000
And how pleasant it is, and how wonderful it is.

20
00:02:31,000 --> 00:02:35,000
So that's what you're seeing in meditation, and that's the truth.

21
00:02:35,000 --> 00:02:37,000
That's the truth that you have to live with.

22
00:02:37,000 --> 00:02:39,000
It doesn't just go away because you meditate.

23
00:02:39,000 --> 00:02:46,000
It's like you've got a closet and you've been putting stuff into your closet and just closing the door and saying,

24
00:02:46,000 --> 00:02:49,000
oh yes, here's one more, one more, one more.

25
00:02:49,000 --> 00:02:55,000
And in meditation, you open the door and you say, oh no, you see all this stuff that's inside and you have to pull it all out.

26
00:02:55,000 --> 00:03:05,000
But more than that, it's like the force of karma, the force of everything you've done has been built up.

27
00:03:05,000 --> 00:03:11,000
Like kind of like stuff in the closet, but it's so much that it's packed in and it's breaking down the door.

28
00:03:11,000 --> 00:03:17,000
But the reason why it reoccurs is because of this habit, because it's become habitual.

29
00:03:17,000 --> 00:03:19,000
It's become a part of who you are.

30
00:03:19,000 --> 00:03:26,000
And so, yeah, even powerful people are doing intensive meditation, it comes back when they practice and they say,

31
00:03:26,000 --> 00:03:29,000
oh, lust is not a good thing and they're able to do with it.

32
00:03:29,000 --> 00:03:34,000
And then boom, it comes back and then they're lusting again.

33
00:03:34,000 --> 00:03:39,000
So it's not meditation practice, it's not the case that meditation practice just does away with it.

34
00:03:39,000 --> 00:03:43,000
Or once you see, oh, this is bad, it's gone.

35
00:03:43,000 --> 00:03:49,000
You have to really change the way the mind looks at the world.

36
00:03:49,000 --> 00:03:57,000
The mind has to actually change its habit, change its way of looking, way of seeing, change its view.

37
00:03:57,000 --> 00:04:11,000
And this happens slowly that eventually or slowly the mind becomes less interested and eventually becomes disinterested and actually gives up.

38
00:04:11,000 --> 00:04:14,000
Can you see this in the progression of insight?

39
00:04:14,000 --> 00:04:22,000
A soda panda, for example, still has lust and or still can have lust, still does have lust and still does have anger.

40
00:04:22,000 --> 00:04:24,000
So it isn't the first thing to go.

41
00:04:24,000 --> 00:04:27,000
The first thing to go is the idea that these are good things.

42
00:04:27,000 --> 00:04:35,000
The soda panda is someone who has realized that these aren't really pleasant, aren't really happiness because they have something to compare it to.

43
00:04:35,000 --> 00:04:37,000
They've realized true happiness, which is nibanda.

44
00:04:37,000 --> 00:04:43,000
And as a result of seeing nibanda, they're no longer attached to ordinary pleasures.

45
00:04:43,000 --> 00:04:47,000
They have found true peace.

46
00:04:47,000 --> 00:04:50,000
So the habit might come back, but the view will not.

47
00:04:50,000 --> 00:04:54,000
They know that this is a habit and it's something that they have to kick.

48
00:04:54,000 --> 00:05:03,000
And that's why at that point all it's going to take its time because they understand.

49
00:05:03,000 --> 00:05:07,000
Yeah, so what you're saying is you see it causing problems.

50
00:05:07,000 --> 00:05:08,000
Now that's what meditation does.

51
00:05:08,000 --> 00:05:12,000
That's the first step is to see the negative side of it.

52
00:05:12,000 --> 00:05:15,000
The second step is to be patient with it.

53
00:05:15,000 --> 00:05:25,000
And practically speaking, this is what happens is that the first wave is really being in the second wave is smaller and eventually the waves get to just ripples.

54
00:05:25,000 --> 00:05:32,000
But what you have to understand is that it's the same experience.

55
00:05:32,000 --> 00:05:40,000
So the same lust will come back again and again and again and again and again and again.

56
00:05:40,000 --> 00:05:45,000
But the reassuring, what you have to reassure yourself is that it's actually getting less and less.

57
00:05:45,000 --> 00:05:49,000
And if you're practicing meditation properly, it will become less and less and less.

58
00:05:49,000 --> 00:05:54,000
Same experience that you thought you were rid of because suddenly it's gone, comes back.

59
00:05:54,000 --> 00:05:59,000
And it will just come back, but it will come back less and less and less.

60
00:05:59,000 --> 00:06:15,000
See, I used to be like, wow, she's hot, but I'm trying to make more like me, just another female move along.

61
00:06:15,000 --> 00:06:20,000
Yeah, I mean, that's not going to be your answer in the end.

62
00:06:20,000 --> 00:06:27,000
It's a nice try, but probably it's only going to have the effect of repressing the feelings for some time.

63
00:06:27,000 --> 00:06:29,000
It's a good temporary solution.

64
00:06:29,000 --> 00:06:32,000
But the only solution is to confront it head on.

65
00:06:32,000 --> 00:06:36,000
And when you do that, the female doesn't have anything to do with it.

66
00:06:36,000 --> 00:06:38,000
The female is just a trigger.

67
00:06:38,000 --> 00:06:41,000
The actual reality is the lust.

68
00:06:41,000 --> 00:06:44,000
And it's the attachment not to a female at all.

69
00:06:44,000 --> 00:06:46,000
It's the attachment to the pleasure.

70
00:06:46,000 --> 00:06:51,000
It's the attachment to pleasant feelings that you're attached to.

71
00:06:51,000 --> 00:06:58,000
Even though it's not quite clear about that, the woman is just the trigger for a pleasant feeling.

72
00:06:58,000 --> 00:07:00,000
You see a beautiful woman.

73
00:07:00,000 --> 00:07:01,000
Pleasure arises.

74
00:07:01,000 --> 00:07:02,000
Pleasure arises.

75
00:07:02,000 --> 00:07:04,000
You want more pleasure.

76
00:07:04,000 --> 00:07:08,000
And this is because you know that that woman can bring you more pleasure.

77
00:07:08,000 --> 00:07:13,000
But it's actually only indirectly because your body is actually bringing you the pleasure.

78
00:07:13,000 --> 00:07:15,000
So in the end, you're just becoming...

79
00:07:15,000 --> 00:07:16,000
And in the end, it's a chemical.

80
00:07:16,000 --> 00:07:19,000
It's actually the chemicals in your brain that are bringing you the pleasure.

81
00:07:19,000 --> 00:07:21,000
So it's a chemical addiction.

82
00:07:21,000 --> 00:07:22,000
It's just drug addiction.

83
00:07:22,000 --> 00:07:25,000
And you deal with it the way you deal with any drug addiction.

84
00:07:25,000 --> 00:07:26,000
That's what's really going on.

85
00:07:26,000 --> 00:07:33,000
Sorry to break it to people who think there's some magic in romance.

86
00:07:33,000 --> 00:07:35,000
Not your life part.

87
00:07:35,000 --> 00:07:37,000
It's so crass of me to laugh.

88
00:07:37,000 --> 00:07:43,000
But you have to understand you're talking to someone who lives in a hut in the forest.

89
00:07:43,000 --> 00:07:46,000
You're not talking to an ordinary human being.

90
00:07:46,000 --> 00:07:51,000
So please excuse my unordinary views and language.

91
00:07:51,000 --> 00:07:54,000
But this is our religion.

92
00:07:54,000 --> 00:07:57,000
This is our viewpoint.

93
00:07:57,000 --> 00:07:58,000
We have looking at things.

94
00:07:58,000 --> 00:07:59,000
Yeah.

95
00:07:59,000 --> 00:08:00,000
Get it straight.

96
00:08:00,000 --> 00:08:02,000
It's a chemical addiction.

97
00:08:02,000 --> 00:08:07,000
And like all addiction, you'd be better without it.

98
00:08:07,000 --> 00:08:10,000
I guess the problem is we've been so taught that that's all there is to life.

99
00:08:10,000 --> 00:08:13,000
What you're taking sex away from me?

100
00:08:13,000 --> 00:08:16,000
What's left? It's true.

101
00:08:16,000 --> 00:08:17,000
Sex is probably the highest.

102
00:08:17,000 --> 00:08:22,000
The Buddha said there's nothing more attractive to a man than a woman.

103
00:08:22,000 --> 00:08:25,000
There's nothing more attractive than a woman than a man.

104
00:08:25,000 --> 00:08:28,000
Of course, this is talking about heterosexuals and homosexuals,

105
00:08:28,000 --> 00:08:31,000
whatever, the point being a human being.

106
00:08:31,000 --> 00:08:34,000
You find that's the most attractive thing.

107
00:08:34,000 --> 00:08:35,000
It's not food.

108
00:08:35,000 --> 00:08:37,000
It's not music.

109
00:08:37,000 --> 00:08:38,000
It's the human body.

110
00:08:38,000 --> 00:08:40,000
The sound of a woman.

111
00:08:40,000 --> 00:08:43,000
The sight of a woman. The smell of a woman.

112
00:08:43,000 --> 00:08:50,000
What the Buddha said.

113
00:08:50,000 --> 00:08:57,000
So this is a hard teaching.

114
00:08:57,000 --> 00:09:03,000
You just take what I say with the grain of salt.

115
00:09:03,000 --> 00:09:07,000
If you don't want to follow it, I say no problem.

116
00:09:07,000 --> 00:09:09,000
You won't be able to refute it.

117
00:09:09,000 --> 00:09:12,000
All of what we call magic of relationships.

118
00:09:12,000 --> 00:09:14,000
The magic of love.

119
00:09:14,000 --> 00:09:15,000
What we call love.

120
00:09:15,000 --> 00:09:16,000
It's not real love.

121
00:09:16,000 --> 00:09:17,000
It's attachment.

122
00:09:17,000 --> 00:09:22,000
Love is the other way around where you set people free.

123
00:09:22,000 --> 00:09:29,000
All of that is just illusion.

124
00:09:29,000 --> 00:09:30,000
It's delusion.

125
00:09:30,000 --> 00:09:31,000
It's a creation.

126
00:09:31,000 --> 00:09:34,000
It's a concept that you create in your mind.

127
00:09:34,000 --> 00:09:37,000
It's like theater.

128
00:09:37,000 --> 00:09:40,000
You build up this identity for yourself,

129
00:09:40,000 --> 00:09:43,000
just like you're an actor, because that's all we are.

130
00:09:43,000 --> 00:09:46,000
This Shakespeare was totally correct.

131
00:09:46,000 --> 00:09:49,000
The world is a stage where just playing parts.

132
00:09:49,000 --> 00:09:52,000
You pick your part and you play it as you like,

133
00:09:52,000 --> 00:09:56,000
or you're casted as a casted as a certain part,

134
00:09:56,000 --> 00:09:59,000
and you have to play it.

135
00:09:59,000 --> 00:10:03,000
The only thing you left out is the point is to get off stage

136
00:10:03,000 --> 00:10:07,000
and if I get a real job, no.

137
00:10:07,000 --> 00:10:11,000
Stop acting.

138
00:10:11,000 --> 00:10:14,000
I'm not sure.

139
00:10:14,000 --> 00:10:41,000
I think that answered your question.

