1
00:00:00,000 --> 00:00:11,000
In some Buddhist traditions, it seems lay Buddhists should just kind of do their best and support the monks while not bothering to meditate.

2
00:00:11,000 --> 00:00:23,000
It's being a lay Buddhist with pressures of work and family, really just about firefighting, rather than aiming for Total Nirvana, firefighting.

3
00:00:23,000 --> 00:00:31,000
I think this is a metaphor for putting out the fires, but not stopping the fires.

4
00:00:31,000 --> 00:00:35,000
Not actually quenching the fires.

5
00:00:35,000 --> 00:00:37,000
Are you going to start it?

6
00:00:37,000 --> 00:00:39,000
Can you do it?

7
00:00:39,000 --> 00:00:41,000
Can you please?

8
00:00:41,000 --> 00:00:46,000
No.

9
00:00:46,000 --> 00:01:05,000
I was in Thailand, and I came in contact with those traditions, where lay people are really just those who bring the food, and then better go off again.

10
00:01:05,000 --> 00:01:13,000
It's not everywhere like that in Thailand and in other countries, but there are these places.

11
00:01:13,000 --> 00:01:18,000
And it is, of course, not so.

12
00:01:18,000 --> 00:01:30,000
In the Buddha's time, there were many lay people practicing and attaining nibana, or if not nibana, other high achievements.

13
00:01:30,000 --> 00:01:42,000
It is possible, and to be just a supporter for the monks is rust under estimating.

14
00:01:42,000 --> 00:01:44,000
That is a correct word for that.

15
00:01:44,000 --> 00:01:55,000
It's just under estimating the value and the potential of a person.

16
00:01:55,000 --> 00:02:21,000
Not everybody has to become a monk, although I heard in the video an interview with Dalai Lama that he said that more people should become monks and nuns, because then we could damn the increase of birth rate on earth.

17
00:02:21,000 --> 00:02:26,000
So our sources would be lasting longer.

18
00:02:26,000 --> 00:02:43,000
Anyway, so no, as a lay person, one should definitely strive for nibana and not just be firefighting, as you say.

19
00:02:43,000 --> 00:02:57,000
And not just be, look, that one is not too evil and not too greedy and not too angry, but really work on it as the monks should do.

20
00:02:57,000 --> 00:03:17,000
There are upholsata days which allow lay people to practice that more intensely and to get used to it, and to practice meditation on those days to be able to take it in their weekly days.

21
00:03:17,000 --> 00:03:28,000
And this is very important, and that is something that in the West is still not very common that people keep the eight precepts on special days.

22
00:03:28,000 --> 00:03:33,000
But I think it is very, very helpful to do it.

23
00:03:33,000 --> 00:03:36,000
Yeah, so please, now you.

24
00:03:36,000 --> 00:03:52,000
Yeah, just because you're wearing white clothes and just because you're living in a house doesn't have any intrinsic need for work in the family.

25
00:03:52,000 --> 00:04:02,000
It goes without saying that everyone has work that they have to do, and lay people have more worldly affairs for sure that they have to take care of.

26
00:04:02,000 --> 00:04:10,000
But the first thing is avoid the family, avoid getting married and having kids.

27
00:04:10,000 --> 00:04:12,000
If you don't already have them, right?

28
00:04:12,000 --> 00:04:31,000
So the point I mean is often in these sorts of traditions or in any traditional cultural Buddhist society, people have the idea that they should be Buddhist, but they also should do many things that in fact from a Buddhist point of view they shouldn't do.

29
00:04:31,000 --> 00:04:45,000
I mean, if you have kids, you should take care of them, but you shouldn't be concerned about sending them abroad or pushing them through university or trying to make them rich and involved in the worldly life.

30
00:04:45,000 --> 00:04:47,000
You shouldn't be involved in politics.

31
00:04:47,000 --> 00:05:00,000
People think that they still have to be involved in society and reading the news and getting involved in speaking up and being active in this and that.

32
00:05:00,000 --> 00:05:04,000
You don't have to become caught up with these things.

33
00:05:04,000 --> 00:05:07,000
The simpler your life, the better it will be for you.

34
00:05:07,000 --> 00:05:14,000
A lay person can live a life like this gatakarika that we always mention when we have these sorts of questions.

35
00:05:14,000 --> 00:05:22,000
He went to the river, got some clay and made pots and put them by the side of the road and just sat by the side of the road selling pots and eked out a living.

36
00:05:22,000 --> 00:05:31,000
Basically, the very minimal amount, he didn't even charge for them, people asked how much you want for that pot and he'd say, just leave what you think it's worth.

37
00:05:31,000 --> 00:05:37,000
Leave some beans, leave some rice, whatever you think it's worth so I can continue my life.

38
00:05:37,000 --> 00:05:45,000
In this way, he was able to make a living and I think take care of his parents as well.

39
00:05:45,000 --> 00:05:56,000
You can live a very simple chased life if you're really serious about it, then you as a lay person can take the eight precepts constantly.

40
00:05:56,000 --> 00:06:01,000
They're in the Buddhist time, there were lay people who kept the eight precepts and lived in lay life.

41
00:06:01,000 --> 00:06:04,000
I've had students like this, people who are unmarried.

42
00:06:04,000 --> 00:06:08,000
I've had them keep the eight precepts on a daily basis, even going to work.

43
00:06:08,000 --> 00:06:22,000
The doctor once, who went to work as a doctor, keeping the eight precepts at a teacher, who went to school, teaching young children wearing white clothes.

44
00:06:22,000 --> 00:06:28,000
It's up to you to arrange your life.

45
00:06:28,000 --> 00:06:33,000
This is the great thing about Buddhism is you have the power, we are not powerless.

46
00:06:33,000 --> 00:06:48,000
It's not a fatalistic religion, or it's not even a dependent religion where you depend on any outside source for salvation or for your development.

47
00:06:48,000 --> 00:06:50,000
You have to take charge of your own.

48
00:06:50,000 --> 00:06:54,000
Atahyya ta no na toh, you are your own refuge and you have to make that effort.

49
00:06:54,000 --> 00:07:03,000
On the one hand, you have to wake up and you have to take charge and you have to change your life.

50
00:07:03,000 --> 00:07:20,000
It's your responsibility to do these things rather than just be negligent and think that it's somehow my duty to have a family and have children and buy a house and get a mortgage and a nice car and so on in order to be a lay person.

51
00:07:20,000 --> 00:07:23,000
You don't have to do that on the one hand.

52
00:07:23,000 --> 00:07:50,000
Another thing that I would say that even in cases where you have a family and you have many responsibilities because I've had many meditators who have small children and then realize, oops, wasn't the best because then they find meditation and they realize that there's something much more important than helping our fellow human beings to be born again or our fellow beings to be born in the human realm again.

53
00:07:50,000 --> 00:08:07,000
So even if it is just about firefighting, even if you aren't able to take the time, if you've got huge debts or you have burdens of children and spouses and jobs and mortgages and all of these things,

54
00:08:07,000 --> 00:08:29,000
even just firefighting as you say or to use a more technical term, even just the development of wholesome karma and the determination to purify your mind and to do five minutes of meditation a day and then to make a determination may I be free from suffering for wishing for all beings to be free from suffering.

55
00:08:29,000 --> 00:08:37,000
Even this is taking a refuge, is taking a stand and is making a start on the path.

56
00:08:37,000 --> 00:08:48,000
The great thing about Buddhism is it's not fatalistic or it's not finite.

57
00:08:48,000 --> 00:08:57,000
If you don't get it in this life, if you don't become enlightened in this life, you have a chance in the next life based on the work that you've done before.

58
00:08:57,000 --> 00:09:06,000
If you start yourself on the right path, set yourself in the right direction, there's no reason that you have to stop when you die.

59
00:09:06,000 --> 00:09:13,000
So even if it is supporting monks and this is why many people do it, they'll just do the best they can support the monks.

60
00:09:13,000 --> 00:09:29,000
In general, the ones who are really keen on the practice will find a way to come to the monastery and spend five minutes, ten minutes meditating, talking to the monks once a week and getting information about meditation.

61
00:09:29,000 --> 00:09:43,000
Maybe they don't ever have a chance in this life to do a meditation course, but they've gotten a start and they have this desire or this intention that sets them on the path.

62
00:09:43,000 --> 00:09:57,000
It goes both ways. You have to try your best, but when you're in a position where you can't become a monk and where you can't dedicate yourself to meditation, rather than be discouraged, you should take it as the first step.

63
00:09:57,000 --> 00:10:04,000
You're making the first step in practicing as you can and developing as you can.

64
00:10:04,000 --> 00:10:17,000
You'll always find there's more that you could be doing than you actually do, and we're very good at making excuses or falling into ruts and laziness when we could be doing more.

65
00:10:17,000 --> 00:10:27,000
We could be developing and we could be giving up some of our burdens.

66
00:10:27,000 --> 00:10:33,000
There was one funny story from a Jansu Pandin in Watlampung.

67
00:10:33,000 --> 00:10:44,000
Some people from Bangkok came to practice at Watlampung and they said they could only stay for a few days because they had to go home to water their plants.

68
00:10:44,000 --> 00:10:48,000
Their home was in Bangkok, so they had to drive back to Bangkok because otherwise their plants would die.

69
00:10:48,000 --> 00:10:51,000
He said, well, plant new plants.

70
00:10:51,000 --> 00:11:01,000
It was terrible because, of course, many potted plants and trees and wonderful things, but they were strong meditators.

71
00:11:01,000 --> 00:11:03,000
They realized the importance of it and they did.

72
00:11:03,000 --> 00:11:10,000
They let their plants die and they stayed for, I think, almost a month, and then they had to go back and plant new plants.

73
00:11:10,000 --> 00:11:21,000
Rather than letting things get in their way, we have to make choices and ask ourselves what's really important watering our plants or purifying our minds.

74
00:11:21,000 --> 00:11:25,000
I want to come back to the first sentence of your question.

75
00:11:25,000 --> 00:11:30,000
At the end, you write, while not bothering to meditate.

76
00:11:30,000 --> 00:11:38,000
I know that there are monks out there who pray that it's not necessary to meditate.

77
00:11:38,000 --> 00:11:47,000
But this is such a dangerous formulation.

78
00:11:47,000 --> 00:11:51,000
It's never bothering to meditate.

79
00:11:51,000 --> 00:12:07,000
Whoever says something else is not really understanding, probably what meditation is about, but meditation is what leads you to the good.

80
00:12:07,000 --> 00:12:18,000
If it doesn't lead you to me, Nibbana, it leads you at least to more wholesomeness, more goodness in your life.

81
00:12:18,000 --> 00:12:41,000
So it is never bothering to meditate and should be done whenever possible.

