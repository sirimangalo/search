1
00:00:00,000 --> 00:00:11,760
Namarupa is interesting, it's a bit of a dilemma really, and it's the dilemma that science

2
00:00:11,760 --> 00:00:23,560
has faced and fought against in many ways, you know, the popular view according to science

3
00:00:23,560 --> 00:00:35,320
is that, according to many scientists, there's only the material, right, and materialism

4
00:00:35,320 --> 00:00:40,440
has become quite popular, or has always been, I guess, quite popular among, well, material

5
00:00:40,440 --> 00:00:46,120
scientists, people who study the physical side of reality.

6
00:00:46,120 --> 00:00:51,960
So they want to be able to say that the mental side of reality is an epiphenomenon, it's

7
00:00:51,960 --> 00:00:58,440
something that arises based on the physical, and it's totally dependent on the physical,

8
00:00:58,440 --> 00:01:07,920
has no power to influence reality, it's like harmony that arises, or sound that arises

9
00:01:07,920 --> 00:01:13,800
when you clap your hands, the sound is dependent on the hands.

10
00:01:13,800 --> 00:01:25,520
And so Buddhism isn't that different, I mean, much consciousness does come from the physical,

11
00:01:25,520 --> 00:01:35,560
and it's conditioned very much by the physical, but it's really making an assumption

12
00:01:35,560 --> 00:01:41,800
to say that the relationship is one way, when you could just as easily say the relationship

13
00:01:41,800 --> 00:01:47,120
goes the other way, you could postulate and run experiments in the same way as people

14
00:01:47,120 --> 00:01:52,160
do in the material realm, you could run these experiments in the mental realm and come

15
00:01:52,160 --> 00:01:56,360
to the conclusion that the physical doesn't exist.

16
00:01:56,360 --> 00:02:02,440
The physical is just an epiphenomenon that is conditioned by the mind, in other words,

17
00:02:02,440 --> 00:02:09,880
all that exists is like a dream, and it's kind of, I suppose, similar to solipsism,

18
00:02:09,880 --> 00:02:21,400
although it's not really solipsistic, it's just purely mental reality that all is just

19
00:02:21,400 --> 00:02:25,960
mind, the mind is the only thing that exists.

20
00:02:25,960 --> 00:02:32,080
Now it's pretty clear that the Buddha didn't ascribe to either of these theories, and

21
00:02:32,080 --> 00:02:36,720
he didn't really ascribe to theories at all, this is why I don't particularly like to

22
00:02:36,720 --> 00:02:43,280
talk about such things, because, or why I might be hesitant to talk about it, because,

23
00:02:43,280 --> 00:02:45,000
you know, what's to talk about?

24
00:02:45,000 --> 00:02:49,720
Namarupa is something that you experience, and it's something that you come to in the very

25
00:02:49,720 --> 00:02:57,040
early stages of meditation, but why I think, on the other hand, it can be good to talk

26
00:02:57,040 --> 00:03:01,200
to it about people who are meditating is, because when you just start out, you really

27
00:03:01,200 --> 00:03:03,800
don't know what it is that you're seeing.

28
00:03:03,800 --> 00:03:07,960
When you look at it, then you wonder, might it be this, might it be that, and you get

29
00:03:07,960 --> 00:03:13,040
the idea of me and mine, and self.

30
00:03:13,040 --> 00:03:17,520
So the idea of Namarupa, the Buddha didn't say one way or other, whether nama comes from

31
00:03:17,520 --> 00:03:23,320
Rupa, Rupa comes from nama, or whether they both exist independently or so on.

32
00:03:23,320 --> 00:03:31,960
He likened them to two men, two people, one of them, I believe it was the Buddha, or

33
00:03:31,960 --> 00:03:34,200
one of his disciples, I can't remember.

34
00:03:34,200 --> 00:03:40,720
Anyway, there's the simile of these two men, one who is crippled and one who is blind.

35
00:03:40,720 --> 00:03:47,800
And so the body is like this blind man who can't see, but has good legs and can walk.

36
00:03:47,800 --> 00:03:55,080
And the mind is like a crippled person who can see fine, but can't walk.

37
00:03:55,080 --> 00:04:00,080
And so they work together, the cripple gets on the back of the blind man, and they walk

38
00:04:00,080 --> 00:04:04,640
around, and the cripple says, go this way, go that way.

39
00:04:04,640 --> 00:04:09,080
And basically the idea here is that they're interdependent, they rely on each other, and

40
00:04:09,080 --> 00:04:14,160
they perform different functions, but it isn't really a philosophical teaching as to whether

41
00:04:14,160 --> 00:04:19,000
one or the other is real or is not real.

42
00:04:19,000 --> 00:04:25,080
And I think that's an important, that's the best way to look at the Buddha's teaching.

43
00:04:25,080 --> 00:04:32,040
I don't think we should get into the idea of existence or reality, because there's such

44
00:04:32,040 --> 00:04:33,800
arbitrary words.

45
00:04:33,800 --> 00:04:37,160
How do you define whether something really exists?

46
00:04:37,160 --> 00:04:40,520
Besides just saying that it's experienced.

47
00:04:40,520 --> 00:04:46,080
The importance of Nama-Rupa, especially for meditators, is not whether one or the other is

48
00:04:46,080 --> 00:04:48,840
real or whether they're both real or so on.

49
00:04:48,840 --> 00:04:58,840
It's that they are all that is real, Nama-Rupa or the physical, the mental and physical experience.

50
00:04:58,840 --> 00:05:05,080
And we don't even have to separate them, but the experience that is partly described

51
00:05:05,080 --> 00:05:09,960
as mental and partly described as physical is all that exists.

52
00:05:09,960 --> 00:05:13,880
And so we're not trying to define one in contrast with the other.

53
00:05:13,880 --> 00:05:21,600
We're trying to define them in contrast with illusion, which is the self.

54
00:05:21,600 --> 00:05:27,120
The first thing you realize as a meditator is that it appears that there are two things.

55
00:05:27,120 --> 00:05:30,960
There's the physical, which is like the stomach rising and falling.

56
00:05:30,960 --> 00:05:33,320
And then there's the mind that goes to know it.

57
00:05:33,320 --> 00:05:36,560
They arise together, they cease together.

58
00:05:36,560 --> 00:05:40,840
But neither one continues on after there are a lot of time.

59
00:05:40,840 --> 00:05:43,320
The rising starts and it stops.

60
00:05:43,320 --> 00:05:50,200
And the stopping is total cessation, that phenomenon is gone.

61
00:05:50,200 --> 00:05:56,240
The knowing of the rising is also gone, it's gone at the moment that the rising ends.

62
00:05:56,240 --> 00:05:58,800
So they arise and cease together.

63
00:05:58,800 --> 00:06:07,440
The foot moving, when you say step being right, stepping left, the foot moving is physical.

64
00:06:07,440 --> 00:06:10,760
The mind that knows the foot is moving, that's mental.

65
00:06:10,760 --> 00:06:17,800
Now whether you say they're one or two or so on, they are clearly distinguishable.

66
00:06:17,800 --> 00:06:22,320
If you want to explain what's going on, you have to say, well, there's the foot moving

67
00:06:22,320 --> 00:06:24,640
and then there's the mind that knows it.

68
00:06:24,640 --> 00:06:31,160
But if you look carefully at it, you'll see that what there isn't is some being involved

69
00:06:31,160 --> 00:06:33,160
in the process.

70
00:06:33,160 --> 00:06:37,960
And look carefully, it just means if you practice it again and again and again and look

71
00:06:37,960 --> 00:06:46,920
again and again eventually, your mind will start to focus and to see clearly what's going

72
00:06:46,920 --> 00:06:57,000
on, that this is a process of phenomena that arise and cease and arise and cease incessantly.

73
00:06:57,000 --> 00:07:02,280
This is called momentary death and this is one of the first things we realize.

74
00:07:02,280 --> 00:07:06,640
If people don't understand it, we often give it, my teacher would often give a talk.

75
00:07:06,640 --> 00:07:13,320
It's just a short talk explaining this, the different kinds of death, like normally

76
00:07:13,320 --> 00:07:16,080
death we think of a person dying.

77
00:07:16,080 --> 00:07:22,760
And so he says that that kind of death is just conceptual, it's not real, it's not a part

78
00:07:22,760 --> 00:07:26,400
of reality, reality is that the mind continues on and on and on and on.

79
00:07:26,400 --> 00:07:29,720
The physical actually continues on and on, it just changes.

80
00:07:29,720 --> 00:07:33,800
So at the moment of death, well, it's just like a wave crashing against the shore.

81
00:07:33,800 --> 00:07:36,960
The ocean remains the same.

82
00:07:36,960 --> 00:07:45,960
So a wave has a beginning and an end, but the stuff that it's made of, in this case water,

83
00:07:45,960 --> 00:07:52,360
or in our case physical and mental phenomena, or experiences, continues.

84
00:07:52,360 --> 00:07:56,640
And it's not really important whether it does or not.

85
00:07:56,640 --> 00:07:59,560
Our experience right now is that it continues on and on.

86
00:07:59,560 --> 00:08:06,120
People want to debate as to whether rebirth is important, considering that we can't

87
00:08:06,120 --> 00:08:07,120
experience it.

88
00:08:07,120 --> 00:08:09,200
It's not really so important.

89
00:08:09,200 --> 00:08:12,040
If you wait long enough eventually, you'll experience it.

90
00:08:12,040 --> 00:08:15,320
But what we're experiencing now is what is real.

91
00:08:15,320 --> 00:08:18,520
The idea that we might die is also in the future.

92
00:08:18,520 --> 00:08:23,480
What is real is our death at every moment, our birth in death, birth in death, and this

93
00:08:23,480 --> 00:08:27,640
cuts through the delusion of self, which is incredibly important.

94
00:08:27,640 --> 00:08:31,520
If you haven't, many of you are aware of why it's so important.

95
00:08:31,520 --> 00:08:39,400
But if you haven't heard much about Buddhism, the reason why it's so important is this

96
00:08:39,400 --> 00:08:44,440
is really the true cause of our suffering, the fact that we attach to things, identify

97
00:08:44,440 --> 00:08:52,440
with them, think of them as me and mine as I, and therefore begin to judge them.

98
00:08:52,440 --> 00:08:56,080
The reason why we say something is good is we think it's good for me.

99
00:08:56,080 --> 00:09:01,080
We want to get it for ourselves, because we have the idea of a self that experiences pleasure

100
00:09:01,080 --> 00:09:03,000
and pain.

101
00:09:03,000 --> 00:09:07,480
When you start to see in this way, you realize that pleasure and pain arise and cease on

102
00:09:07,480 --> 00:09:12,320
their own, and there's really no experience or there's the mind, but that mind arises

103
00:09:12,320 --> 00:09:16,560
and ceases with the pleasure and with the pain.

104
00:09:16,560 --> 00:09:21,520
And again, this, I think for many people it'll just sound like an intellectual theory

105
00:09:21,520 --> 00:09:25,040
and go way over their head if you're not meditating.

106
00:09:25,040 --> 00:09:29,600
But my hope is that many of you are meditating and therefore this should actually make

107
00:09:29,600 --> 00:09:34,000
some sense and relate in some way to what you're practicing.

108
00:09:34,000 --> 00:09:39,480
So that's a little bit about Nama and Rupa, if, for those of you who don't know these

109
00:09:39,480 --> 00:09:45,200
words, Nama, the definition of Nama is that which knows an object.

110
00:09:45,200 --> 00:09:51,280
The definition of Rupa is something that doesn't know an object, doesn't have awareness.

111
00:09:51,280 --> 00:09:55,200
So the mind is the awareness. When you move the foot, there's the Rupa, which doesn't

112
00:09:55,200 --> 00:09:56,200
know anything.

113
00:09:56,200 --> 00:10:02,400
It's just the movement and then there's the knowing of it and they go together.

114
00:10:02,400 --> 00:10:06,200
The Rupa occurs and the Nama knows it.

115
00:10:06,200 --> 00:10:36,080
So it just means body and mind really, that's the language that we use.

