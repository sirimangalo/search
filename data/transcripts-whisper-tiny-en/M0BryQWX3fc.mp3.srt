1
00:00:00,000 --> 00:00:03,840
Hi, and welcome back to Ask a Monk.

2
00:00:03,840 --> 00:00:07,720
So today's question is, how would a lay person begin

3
00:00:07,720 --> 00:00:09,920
to resolve the conflict created by the fact

4
00:00:09,920 --> 00:00:14,160
that societies in general expect, and to some degree demand,

5
00:00:14,160 --> 00:00:17,720
that the people inside them must work a job that usually flies

6
00:00:17,720 --> 00:00:19,840
in the face of their better judgment?

7
00:00:19,840 --> 00:00:23,440
And this is from Tony in Fremantle.

8
00:00:23,440 --> 00:00:27,840
Well, Tony, I think it's important to understand the difference

9
00:00:27,840 --> 00:00:31,280
between expectations and demands in regards

10
00:00:31,280 --> 00:00:34,200
to what society expects and what society demands.

11
00:00:34,200 --> 00:00:36,880
Because you'll find that in general,

12
00:00:36,880 --> 00:00:38,720
there's an incredible discrepancy.

13
00:00:38,720 --> 00:00:43,400
And society demands literally demands a lot less of us

14
00:00:43,400 --> 00:00:47,200
than we might think, but it expects generally a lot more

15
00:00:47,200 --> 00:00:49,360
than we would like.

16
00:00:49,360 --> 00:00:53,200
And so you'll find that in order just to get by and just

17
00:00:53,200 --> 00:00:58,200
to live in society and be a Buddhist meditator and live

18
00:00:58,200 --> 00:01:00,920
a peaceful life, you don't need to do that much.

19
00:01:00,920 --> 00:01:05,760
And what you'll find instead is people will be looking

20
00:01:05,760 --> 00:01:09,720
at you funny and you will not fit in with society

21
00:01:09,720 --> 00:01:14,520
and you'll find yourself living alone most of the time.

22
00:01:14,520 --> 00:01:18,240
Which, you know, that's the truth of being a Buddhist

23
00:01:18,240 --> 00:01:27,720
or being a meditator is, you don't make much fun at parties.

24
00:01:27,720 --> 00:01:30,360
You live alone and you're looking inside.

25
00:01:30,360 --> 00:01:35,920
Your way of behaving is not one that is exciting

26
00:01:35,920 --> 00:01:40,720
to other people because you're contemplating yourself.

27
00:01:40,720 --> 00:01:45,920
You're looking inside more often than out.

28
00:01:45,920 --> 00:01:50,080
And so I would resolve the conflict by separating the two,

29
00:01:50,080 --> 00:01:53,960
by seeing what society demands and what demands are placed

30
00:01:53,960 --> 00:01:56,840
on you by the people who you support,

31
00:01:56,840 --> 00:02:03,680
say your parents or your family and the demands of law

32
00:02:03,680 --> 00:02:14,680
and civil requirements, civil contracts and so on that you have.

33
00:02:14,680 --> 00:02:18,800
And, you know, as far as working jobs you think of the job

34
00:02:18,800 --> 00:02:23,240
as being something that it's not meant to bring meaning.

35
00:02:23,240 --> 00:02:25,560
The job that you're doing is meant to bring money for you.

36
00:02:25,560 --> 00:02:29,040
So, in the time of the Buddha there was a man

37
00:02:29,040 --> 00:02:32,160
who sold pots or not in the time of the Buddha,

38
00:02:32,160 --> 00:02:34,200
the Buddha gave this story of an ancient times

39
00:02:34,200 --> 00:02:36,120
a man who sold pots.

40
00:02:36,120 --> 00:02:41,000
And he wouldn't ask for money even for the pots.

41
00:02:41,000 --> 00:02:42,600
He would put these pots on the side of the road

42
00:02:42,600 --> 00:02:44,560
and when people asked how much is this pot,

43
00:02:44,560 --> 00:02:45,320
how much is that pot?

44
00:02:45,320 --> 00:02:46,680
He would just say, oh, you know,

45
00:02:46,680 --> 00:02:50,280
leave some rice, leave some beans and take whichever one you like.

46
00:02:50,280 --> 00:02:54,960
And he lived his life incredibly pure and simple.

47
00:02:54,960 --> 00:03:00,840
And yet he didn't have any problem getting by.

48
00:03:00,840 --> 00:03:05,960
It was sort of a mode of life that most people would find

49
00:03:05,960 --> 00:03:07,200
perhaps even repulsive.

50
00:03:07,200 --> 00:03:14,200
But he was able to do what was absolutely necessary.

51
00:03:14,200 --> 00:03:17,880
And another thing you'll find is that if you stick to your guns,

52
00:03:17,880 --> 00:03:26,880
if you're really set on virtue and honesty and purity,

53
00:03:27,520 --> 00:03:30,760
and as a result, feel like you're being pushed out

54
00:03:30,760 --> 00:03:33,280
of certain positions and so on, you'll actually find

55
00:03:33,280 --> 00:03:36,200
that in the long run people respect you more for it.

56
00:03:36,200 --> 00:03:38,080
When you refuse to do certain jobs

57
00:03:38,080 --> 00:03:43,600
that are certain intertakes certain employment that is immoral,

58
00:03:43,600 --> 00:03:46,800
you'll find that in the short term you do suffer

59
00:03:46,800 --> 00:03:49,960
because you have to give up certain opportunities

60
00:03:49,960 --> 00:03:52,960
which immoral people would be able to take advantage of.

61
00:03:52,960 --> 00:03:55,560
But you'll find in the long run that of course what you get

62
00:03:55,560 --> 00:03:59,120
is people trust you and you find that you're surrounded

63
00:03:59,120 --> 00:04:02,000
by people who appreciate you.

64
00:04:02,000 --> 00:04:04,800
You'll find that people appreciate the work that you do.

65
00:04:04,800 --> 00:04:06,440
They say good things about you.

66
00:04:06,440 --> 00:04:08,320
They recommend you to other people.

67
00:04:08,320 --> 00:04:12,080
And that actually the idea that people who are honest

68
00:04:12,080 --> 00:04:16,160
and moral aren't able to get by in the world is really a myth.

69
00:04:16,160 --> 00:04:18,720
And it's something that is of course perpetuated

70
00:04:18,720 --> 00:04:23,600
by people who are immoral and who think that who feels

71
00:04:23,600 --> 00:04:27,920
perhaps threatened by the idea that someone would be able

72
00:04:27,920 --> 00:04:34,080
to get by without having to do evil things.

73
00:04:34,080 --> 00:04:36,880
And the other thing is that in the long run

74
00:04:36,880 --> 00:04:39,640
you're going to want to leave society.

75
00:04:39,640 --> 00:04:43,200
That it's clear everyone since the Buddha's time has said

76
00:04:43,200 --> 00:04:48,480
that living in society is a, it's like a dusty, dusty road.

77
00:04:48,480 --> 00:04:53,440
I'm very full of crowded with people traveling and coming

78
00:04:53,440 --> 00:04:55,080
and going.

79
00:04:55,080 --> 00:05:00,000
And the homeless life or leaving society is like the open air.

80
00:05:00,000 --> 00:05:04,280
It's life that's free from all of the hustle and bustle

81
00:05:04,280 --> 00:05:07,920
of the streets and the highways.

82
00:05:07,920 --> 00:05:10,160
And so if you can't become a monk,

83
00:05:10,160 --> 00:05:12,120
if you can't go to live in a monastery,

84
00:05:12,120 --> 00:05:17,920
you can at least begin to become a recluse

85
00:05:17,920 --> 00:05:23,240
or to leave society to find the way to look inside yourself.

86
00:05:23,240 --> 00:05:28,160
If this is your choice is to spend your life contemplating

87
00:05:28,160 --> 00:05:31,440
the teachings of the Buddha or coming to understand more

88
00:05:31,440 --> 00:05:35,280
about yourself and more about the experience, experiential

89
00:05:35,280 --> 00:05:39,200
reality, then you do have to start leaving society.

90
00:05:39,200 --> 00:05:44,960
And so the true resolution of the conflict

91
00:05:44,960 --> 00:05:48,840
is to leave the battlefield.

92
00:05:48,840 --> 00:05:51,720
Don't engage in this uphill struggle,

93
00:05:51,720 --> 00:05:53,960
what they call a rat race.

94
00:05:53,960 --> 00:05:56,200
You'll leave it behind if you can become a monk great,

95
00:05:56,200 --> 00:05:58,440
if not, then at least get involved

96
00:05:58,440 --> 00:06:01,880
with Buddhist monasteries or meditation centers

97
00:06:01,880 --> 00:06:04,800
or start a meditation group.

98
00:06:04,800 --> 00:06:11,800
And build up as a circle of friends, a circle of companions,

99
00:06:11,800 --> 00:06:17,000
people who can support you and you can support each other.

100
00:06:17,000 --> 00:06:22,120
And try to put it all behind you in society in this world.

101
00:06:22,120 --> 00:06:24,520
It doesn't seem to have done a heck of a lot

102
00:06:24,520 --> 00:06:28,200
for the spiritual well-being of humankind.

103
00:06:28,200 --> 00:06:30,880
In fact, it seems the more social we are,

104
00:06:30,880 --> 00:06:35,360
the more thick and deep our defilements become.

105
00:06:35,360 --> 00:06:39,920
So you can never hope to change the world.

106
00:06:39,920 --> 00:06:41,960
You have to look inside and change yourself

107
00:06:41,960 --> 00:06:46,200
and by extension, everyone around you benefits.

108
00:06:46,200 --> 00:06:47,560
OK, so I hope that helps.

109
00:06:47,560 --> 00:06:49,800
And I wish you all the best on your path.

110
00:06:49,800 --> 00:06:50,800
Good luck.

111
00:06:50,800 --> 00:06:54,800
Thank you.

