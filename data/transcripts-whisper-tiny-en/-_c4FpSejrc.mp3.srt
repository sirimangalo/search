1
00:00:00,000 --> 00:00:09,000
For longer than a year now, I have moments of de-realization, mainly after meditation, studying Buddhism, but also out of nothing, is this normal.

2
00:00:12,500 --> 00:00:14,500
De-realization.

3
00:00:16,500 --> 00:00:23,500
I'm going to make a shot in the dark and guess at what you're saying there.

4
00:00:23,500 --> 00:00:36,500
I assume you're referring to a sense of surreality where suddenly everything around you seems surreal or unreal.

5
00:00:37,500 --> 00:00:50,500
This is normal, this is very important indication that you're starting to actually see reality, because reality is not the things around us.

6
00:00:50,500 --> 00:00:56,500
The feeling itself, it's important to be mindful of, because it is just a feeling.

7
00:00:56,500 --> 00:01:07,500
But the reason why it arises is because actual reality is much less than what we think it is.

8
00:01:07,500 --> 00:01:14,500
There really isn't this room that I'm sitting in with all these books, and you're really not hearing my voice.

9
00:01:14,500 --> 00:01:22,500
It's just experience, there's just the seeing, there's just the hearing, there's just the smelling, the tasting, the feeling and the thinking.

10
00:01:22,500 --> 00:01:27,500
When we get this glimpse suddenly everything around us, it can actually be quite frightening.

11
00:01:27,500 --> 00:01:35,500
People become quite disturbed by this at times, and suddenly they look at, everything they look at seems scary and horrifying.

12
00:01:35,500 --> 00:01:50,500
Because of seeing the stripping away of the delusions, the comfort of, oh there's my table, there's my chair, there's my bed and so on.

13
00:01:50,500 --> 00:02:11,500
The comfort of having entities that you can cling to, when that goes away then there can be these confusion about where did my body go, where did my, you know, sit in meditation and suddenly the body disappears, for example.

14
00:02:11,500 --> 00:02:20,500
But it's important that any feelings that arise out of this that you might call derealization or depersonalization or so on, be acknowledged as well.

15
00:02:20,500 --> 00:02:30,500
Because otherwise they can snowball and become hindrances to the path and can actually mess up your mind if you chase after them.

16
00:02:30,500 --> 00:02:43,500
Anything that you cultivate that you worry about and obsess over can cause problems, not only for your meditation but for your state of mental stability, mental sanity.

17
00:02:43,500 --> 00:02:54,500
So meditation is not dangerous, but when you stop meditating, when a state arises that makes you give up your meditation practice, that's dangerous.

18
00:02:54,500 --> 00:03:02,500
So once these states arise they're actually harmless, but because you turn them into a problem, you see it's a problem.

19
00:03:02,500 --> 00:03:08,500
Every time it comes up you'll get more and more agitated about it and that agitation, that stress drives you crazy.

20
00:03:08,500 --> 00:03:12,500
It's like winding up a clock until you break it.

21
00:03:12,500 --> 00:03:22,500
So when you have this experience don't be worried about it, acknowledge the worry, acknowledge the confusion, the doubt, and then acknowledge the state it sounds.

22
00:03:22,500 --> 00:03:29,500
And be aware of the state just for what it is, it's just a feeling, you can just say feeling, feeling, or so.

