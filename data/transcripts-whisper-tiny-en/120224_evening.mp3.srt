1
00:00:00,000 --> 00:00:14,720
So, the nay continuing on, more food for that, before we pack this together.

2
00:00:14,720 --> 00:00:28,080
A lot of the Buddha's teaching is just saying the same thing in different words, we're

3
00:00:28,080 --> 00:00:30,480
using different terms.

4
00:00:30,480 --> 00:00:40,920
So, he talks about that which is of most help, but I may have many things that are of most

5
00:00:40,920 --> 00:00:41,920
help.

6
00:00:41,920 --> 00:00:50,880
So, actually, when you look at it, it's talking about the same thing, but from a different

7
00:00:50,880 --> 00:00:52,640
point of view.

8
00:00:52,640 --> 00:01:05,560
So, he says at first he says, I can't think of anything, or I don't know of anything,

9
00:01:05,560 --> 00:01:20,200
but I said, I know of no dhamma that causes unwholesomeness to arise more than negligent.

10
00:01:20,200 --> 00:01:31,440
And that which is wholesome to stay in the way.

11
00:01:31,440 --> 00:01:46,160
And he says, I know of nothing that is as potent in giving rise to wholesomeness and driving

12
00:01:46,160 --> 00:01:57,960
away unwholesomeness as vigilance.

13
00:01:57,960 --> 00:02:04,760
And then he goes on to talk about other dhammas that are of equal value, so in terms

14
00:02:04,760 --> 00:02:24,880
of one being useful and one being helpful and one being a hindrance, and then he says,

15
00:02:24,880 --> 00:02:35,840
of course the dhammas, which means influence, laziness, or lounging around, finding

16
00:02:35,840 --> 00:03:02,080
the wasting time, and the opposite is putting out effort.

17
00:03:02,080 --> 00:03:11,280
And so on, so saying the same thing using the converse with it, so the point being that

18
00:03:11,280 --> 00:03:20,160
in regards to wholesomeness, there are certain dhammas that are going to cultivate wholesomeness

19
00:03:20,160 --> 00:03:24,480
and do away with unwholesomeness in the way.

20
00:03:24,480 --> 00:03:38,480
We have vigilance versus negligence versus vigilance, we have idonness, laziness versus

21
00:03:38,480 --> 00:03:50,480
effort, and then we have maha, et cetera, having many desires, many wants wanting this

22
00:03:50,480 --> 00:04:09,520
and wanting that versus having few desires, having few ambitions, and then we have discontent

23
00:04:09,520 --> 00:04:36,800
and contentments, and then we have wise attention, and unwise attention, yoni somena,

24
00:04:36,800 --> 00:04:49,480
yoni somena sifare, unwise contemplation or thought, and wise thought, wise contemplation,

25
00:04:49,480 --> 00:04:57,480
and we have clear awareness, full awareness, and lack of full awareness and full awareness,

26
00:04:57,480 --> 00:05:08,040
so there are quite a few things here, the Buddhists are useful, and the final one is having

27
00:05:08,040 --> 00:05:14,280
evil friends and having good friends, and so we can look at all each of these in turn

28
00:05:14,280 --> 00:05:20,920
and see that actually, just many ways of explaining our practice, and these are all things

29
00:05:20,920 --> 00:05:25,280
that are very useful for us to know and understand, so the first one we talked about

30
00:05:25,280 --> 00:05:32,600
a lot in the demo part of videos we've been talking about, vigilance, I don't think

31
00:05:32,600 --> 00:05:38,800
I mentioned to you what is the Buddhist teaching of vigilance, basically I mentioned already

32
00:05:38,800 --> 00:05:44,360
that it means in one place the Buddha says it means to have mindfulness all the time, to

33
00:05:44,360 --> 00:05:51,160
never be without mindfulness, so there's this story I don't know if I told you when

34
00:05:51,160 --> 00:05:58,360
I was teaching in Thailand once, and all of the foreign meditators had been instructed

35
00:05:58,360 --> 00:06:04,520
to practice a certain number of hours a day, so today's six hours to more eight hours

36
00:06:04,520 --> 00:06:11,280
and so on, and then they came to me and asked me how many hours a day they had to practice

37
00:06:11,280 --> 00:06:17,760
and they said, today you have to practice 18 hours, and his eyes look like we're going

38
00:06:17,760 --> 00:06:23,960
to pop out of his head, this is very quite stressed out about having to even take eight

39
00:06:23,960 --> 00:06:31,200
hours to practice eight hours a day, so now he's trying to do the math and think, how can

40
00:06:31,200 --> 00:06:38,640
I get out of this one, and then I explained to him what I meant, when you wake up first

41
00:06:38,640 --> 00:06:46,360
thing in the morning you have to be mindful of lying, you sit up, sitting, sitting, you

42
00:06:46,360 --> 00:06:50,640
watch the rising and falling of the stomach and then standing, standing when you go to the

43
00:06:50,640 --> 00:06:56,840
washroom and you brush your teeth, when you eat your food, when you shower, when you use

44
00:06:56,840 --> 00:07:03,120
the toilet and so on, be mindful of all of these things, it doesn't matter how I don't,

45
00:07:03,120 --> 00:07:09,200
I'm not concerned about how many hours of formal practice you do, but you're not to talk

46
00:07:09,200 --> 00:07:21,360
with other meditators, you're not to get involved in reading or idle pursuits or so on,

47
00:07:21,360 --> 00:07:27,160
you have to practice for 18 hours a day, when you sleep, that's your brain, you don't have

48
00:07:27,160 --> 00:07:33,160
to be mindful, but the point is you can do anything, you can have tea, you can, that's

49
00:07:33,160 --> 00:07:38,960
explaining to them, but it really actually calmed him down and made him less stressed

50
00:07:38,960 --> 00:07:44,880
than before I had given him this arduous task, because the point is simply to be mindful,

51
00:07:44,880 --> 00:07:50,040
it's not to do this or do that, to have to walk for so many hours or six or so many hours,

52
00:07:50,040 --> 00:07:58,960
you can live your natural life, but do it mindfully, this is the meaning of upper monitor,

53
00:07:58,960 --> 00:08:03,120
to live your life mindfully, whenever you do something, to do it mindfully, when you're

54
00:08:03,120 --> 00:08:09,280
walking to walk life, you're eating to eat life, to be aware of it also, it's actually quite

55
00:08:09,280 --> 00:08:13,120
different, it's nothing like how you normally live your life, but to the outside observer,

56
00:08:13,120 --> 00:08:18,200
it looks quite normal, when you're eating your food, scooping, and raising, you're aware

57
00:08:18,200 --> 00:08:21,720
of all of these, when you teeth, they're chewing, you're aware of the tension in the

58
00:08:21,720 --> 00:08:27,640
jaw and so on, when you swallow, you're aware of the feeling in the throat swallowing,

59
00:08:27,640 --> 00:08:34,480
you make a note of everything as it occurs, seeing if this is this, this is being aware

60
00:08:34,480 --> 00:08:43,320
of it, just as it is in reminding yourself of it, the opposite is opposite is pamanda,

61
00:08:43,320 --> 00:08:50,680
where a person spends all their time in idle pursuits, and getting caught up in things

62
00:08:50,680 --> 00:08:56,680
that are unrelated to, and in goal, and have no purpose watching television, or sitting

63
00:08:56,680 --> 00:09:01,600
around chatting, and what lampingly would sit around, and spend, so as they had the eight

64
00:09:01,600 --> 00:09:05,160
hours they had to do, and then there's ten hours that they're awake, but they don't have

65
00:09:05,160 --> 00:09:09,320
to do anything, and so they thought they could just sit around and talk with each other,

66
00:09:09,320 --> 00:09:16,520
and go to the store, and so on, and all these things that they would talk, what lampings

67
00:09:16,520 --> 00:09:21,120
in the city, so they would think it quite distracted, and cause a lot of problems for

68
00:09:21,120 --> 00:09:27,120
the other monk, so when we put them on this routine, it changed the whole atmosphere,

69
00:09:27,120 --> 00:09:35,120
and then they were quiet and separate, and contemplating their own, their own minds and

70
00:09:35,120 --> 00:09:40,480
their own behavior, their own action, and every moment. During a meditation course, it's

71
00:09:40,480 --> 00:09:44,680
quite necessary. Actually, for all of us, this is something we have to strive to do, even

72
00:09:44,680 --> 00:09:52,400
living in our daily life. We'll go back home and join, trying to be as mindful as possible.

73
00:09:52,400 --> 00:09:57,440
That's basically what that one means. It's the core of the Buddhist teaching, just

74
00:09:57,440 --> 00:10:03,480
to be this way, to train in this way, because it's what leads us to enlightenment, it leads

75
00:10:03,480 --> 00:10:13,880
us to understand ourselves, and then we have laziness and effort, and we have to be careful

76
00:10:13,880 --> 00:10:18,440
to understand what we mean by effort. You remember what it says? It talks about effort.

77
00:10:18,440 --> 00:10:23,560
He doesn't mean just pushing yourself in whatever you're doing, whatever you have to do

78
00:10:23,560 --> 00:10:29,560
to work hard at it. That's not really what he means. He means putting out effort to be mindful,

79
00:10:29,560 --> 00:10:34,680
and because when we look at the Buddhist meaning of effort, the meaning of effort is

80
00:10:34,680 --> 00:10:40,960
for thing. The unholciveness in your mind, you work to get rid of it. When it comes

81
00:10:40,960 --> 00:10:46,320
up, you work, you do the work that's necessary to remove it from the mind, to see it clearly

82
00:10:46,320 --> 00:10:51,720
and to see the situation clearly, so you don't have aversion or attachment in regards

83
00:10:51,720 --> 00:10:58,120
to the experience, and you don't have any delusion. To work hard to keep the bad states

84
00:10:58,120 --> 00:11:03,960
out. When there is no greed or anger, but you know that something is coming up that will

85
00:11:03,960 --> 00:11:08,840
lead to it, you're careful to be mindful of it again, and again, and again, a happy feeling

86
00:11:08,840 --> 00:11:13,920
comes up, you're careful to be mindful of it as a happy feeling. You work hard at staying

87
00:11:13,920 --> 00:11:20,960
with the object, knowing that at any moment unholciveness could arise if you're not mindful.

88
00:11:20,960 --> 00:11:27,560
This takes quite a bit of effort. It takes an exceptional amount of effort to continue

89
00:11:27,560 --> 00:11:34,640
throughout the day to keep it away, and then the development, the effort is the other

90
00:11:34,640 --> 00:11:40,440
two are the development of unholciveness that has it. This practice, when you're not

91
00:11:40,440 --> 00:11:44,640
good at it yet, it's hard to keep the defilement away, and the mind keeps going back,

92
00:11:44,640 --> 00:11:51,240
and so the work to keep it, to keep it going, this is just the opposite. Once you have

93
00:11:51,240 --> 00:11:55,640
attained it, so once you get good at the practice, to stay on track, it's very easy

94
00:11:55,640 --> 00:12:00,880
once you get good at the practice to become negative, I'm good at this already, and it's

95
00:12:00,880 --> 00:12:08,680
back off. It's also easy to become so intensely focused on the practice or eager to reach

96
00:12:08,680 --> 00:12:14,680
the goal that you over exert yourself. Both of these are two extremes that we have to guard

97
00:12:14,680 --> 00:12:19,480
against. When your practice is good, you also have to make your practice good, and you

98
00:12:19,480 --> 00:12:25,240
have to have the effort to keep yourself on track. What is really meant here is the effort

99
00:12:25,240 --> 00:12:32,320
to stay mindful, the effort to keep your mind straight and to straighten the mind. Just

100
00:12:32,320 --> 00:12:36,480
like something that is bent, you have to work hard to straighten it, and then you have

101
00:12:36,480 --> 00:12:46,360
to work hard to keep it straight. We have many wishes and few wishes, so if you have many

102
00:12:46,360 --> 00:12:53,040
wishes, here I think there's not such an opportunity to have many wishes, but you can still

103
00:12:53,040 --> 00:12:57,600
be thinking a lot about what you want to do when you go home, or even what you want to

104
00:12:57,600 --> 00:13:06,920
do here, wanting to become a teacher, or so on, wanting to build a meditation center,

105
00:13:06,920 --> 00:13:10,840
all of these things can get in the way when you have many wishes. This is something very

106
00:13:10,840 --> 00:13:16,360
important to keep in mind, and we shouldn't be wanting to have this or wanting to have

107
00:13:16,360 --> 00:13:21,600
that. When you're thinking about when you go home, you're going to do this, and do that,

108
00:13:21,600 --> 00:13:27,120
very much get in the way of your practice. They get in the way of your contentment, which

109
00:13:27,120 --> 00:13:32,960
is the next one. When a person has many wishes, there's a Buddha said it leads to unholes

110
00:13:32,960 --> 00:13:37,840
of us, and it leads to wholesomeness to disappear, because you want this. You can never

111
00:13:37,840 --> 00:13:42,880
be satisfied when you get what you want and you want more in anything, not just things

112
00:13:42,880 --> 00:13:51,240
like food and material, but can be in becoming this and becoming that, or in removing this

113
00:13:51,240 --> 00:13:56,360
and removing that, you'll never be satisfied. As long as you're happiness depends on the

114
00:13:56,360 --> 00:14:03,000
situation that you're in. And even here, we can have many opportunities for the arising

115
00:14:03,000 --> 00:14:08,640
of desires. Maybe the food is good, or maybe the food is not good, and so you're thinking,

116
00:14:08,640 --> 00:14:13,080
oh, I wish I had this kind of food or that kind of food. Maybe you're thinking about

117
00:14:13,080 --> 00:14:17,160
home, or you have a nice bend, or you have an air conditioner, or you have this, or you

118
00:14:17,160 --> 00:14:22,920
have that. All of these things are our desires that we have to be careful about, and

119
00:14:22,920 --> 00:14:31,320
we're trying to give up. Contentment and discontent goes very much along with that. It's

120
00:14:31,320 --> 00:14:41,280
really another way of saying having a few desires. But contentment is a particularly special

121
00:14:41,280 --> 00:14:50,000
mindset. It means being content with whatever comes when there's unpleasantness to be content

122
00:14:50,000 --> 00:14:56,640
with that, when there's pleasantness to be content with as much comes. It means to be level

123
00:14:56,640 --> 00:15:06,120
headed, or balanced in the mind, unperturbed by good things or bad things. It's really

124
00:15:06,120 --> 00:15:13,120
the same as another way of talking about wants and needs. But the Buddha said, this is the

125
00:15:13,120 --> 00:15:21,400
greatest game, because when you have contentment, you don't want for anything. And so I

126
00:15:21,400 --> 00:15:27,120
think we have to be content there, because I've lost the light. I just want to talk about

127
00:15:27,120 --> 00:15:32,360
it. I think there was a couple more in there, but the last one was having good friends

128
00:15:32,360 --> 00:15:41,360
and having people friends. So we consider that the community here is a good friend for us.

129
00:15:41,360 --> 00:15:46,120
The other people here are good support for us. I consider that all of you are a good support

130
00:15:46,120 --> 00:15:53,560
for my own life in this sense that you provide a community for me. All of us, I think for

131
00:15:53,560 --> 00:15:59,640
sure we can appreciate having the community. If you try to do this at home, try to practice

132
00:15:59,640 --> 00:16:04,560
even this that we do at night once in the morning night to try to get up at 4.30 in the

133
00:16:04,560 --> 00:16:09,880
morning or 4.00 in the morning to be able to practice together at 4.30. If you try to

134
00:16:09,880 --> 00:16:17,800
do it alone, you find that it's quite difficult. You have this incredible push that comes

135
00:16:17,800 --> 00:16:25,520
from having other people around who are likewise dedicated to development of mindfulness

136
00:16:25,520 --> 00:16:31,440
and being around and getting to know each other. I always used to tell people in Thailand

137
00:16:31,440 --> 00:16:37,320
is that the best meditator is someone who doesn't even know the names of the other meditators.

138
00:16:37,320 --> 00:16:44,160
If you can come, if you can arrive and leave the center without knowing the names of the other

139
00:16:44,160 --> 00:16:52,560
meditators, I consider you to be an exceptional meditator. I think it's hard to find such

140
00:16:52,560 --> 00:16:59,920
meditators. If you haven't fulfilled that one, you can see where it's going. The point

141
00:16:59,920 --> 00:17:05,920
about having good friends is not that we spend time together. The Buddha is very critical.

142
00:17:05,920 --> 00:17:11,720
At the same time, he would say, have good friends. It's very important to have good friends

143
00:17:11,720 --> 00:17:18,920
and then he would say, don't spend time in other people's company. Don't spend time

144
00:17:18,920 --> 00:17:25,920
in society or socializing. We might wonder, what's the contradiction here? Why say one thing

145
00:17:25,920 --> 00:17:33,000
or the other? Why tell us to have friends and then not even know their names, for example?

146
00:17:33,000 --> 00:17:36,800
I think you can get the idea where I'm going is that the best friend is an example to

147
00:17:36,800 --> 00:17:42,800
us. The best friend is someone who gives us the opportunity, gives us the time and the

148
00:17:42,800 --> 00:17:49,800
opportunity and the place. All of the work that has gone into this place and making it

149
00:17:49,800 --> 00:17:58,680
as fairly suitable or minimally suitable, plays for us to practice meditation. I can

150
00:17:58,680 --> 00:18:05,280
say that this is because of the friendship and the kindness of the people who have supported us.

151
00:18:05,280 --> 00:18:10,600
That we give as a sign of our friendship to you. You don't even have to know my name. You

152
00:18:10,600 --> 00:18:15,560
come here and you see there's the meditation practice and meet them as they once of day

153
00:18:15,560 --> 00:18:20,680
and I'll give you the new exercise apart from that. You consider that I've been a friend

154
00:18:20,680 --> 00:18:27,320
to you and by giving you the teaching and giving you the place and so on. The other people

155
00:18:27,320 --> 00:18:34,880
here, we consider them to be friends and that they don't bother us. If they were a PAPA

156
00:18:34,880 --> 00:18:42,920
group, they would waste all our time in idol gossip and dragging us away to do this or

157
00:18:42,920 --> 00:18:50,040
do that and coming up and disturbing us all the time. So we consider that we have very

158
00:18:50,040 --> 00:18:58,120
good friends here that they give us the opportunity and the space and the space and the

159
00:18:58,120 --> 00:19:03,480
time. I'm not a good friend to you because I come and knock on your door every day and say

160
00:19:03,480 --> 00:19:08,280
how you're going. I want to talk. I want to go for coffee or something. You can consider

161
00:19:08,280 --> 00:19:12,440
me a good friend and all the people who have good friends in that way. We've given you

162
00:19:12,440 --> 00:19:19,760
your space. We've done something that most people won't do and that is maybe alone and

163
00:19:19,760 --> 00:19:26,480
give you the time and the space and the teaching and to learn more about yourself and

164
00:19:26,480 --> 00:19:34,080
to come to understand how your mind works and to become free from a Kuzula d'Amman and

165
00:19:34,080 --> 00:19:39,600
the states of mind developing a wholesome d'Amman, a Kuzula d'Amman, the wholesome states

166
00:19:39,600 --> 00:19:47,400
of mind which is the Buddha makes it clear that this is the important thing. He says

167
00:19:47,400 --> 00:19:52,280
these are for the purpose of wholesomeness and for giving up an wholesomeness. So what

168
00:19:52,280 --> 00:19:56,880
he's saying there is that this is what we should be aiming for. We need these things because

169
00:19:56,880 --> 00:20:01,680
they lead us to develop wholesomeness. A Kuzula d'Amman is according to the Buddha the

170
00:20:01,680 --> 00:20:07,800
goal and development of wholesome mind states. Earth is the path. Our development hinges

171
00:20:07,800 --> 00:20:13,600
on developing wholesome mind states and discarding unwholesome mind states. Coming to see

172
00:20:13,600 --> 00:20:23,800
through the delusion that leads to the wholesome mind states. So that's the pep top for

173
00:20:23,800 --> 00:20:51,400
tonight. I want you to hear it try to do mindful frustration walking and sitting.

