1
00:00:00,000 --> 00:00:20,840
Hey, good evening everyone, welcome to our weekly Ndama broadcast, so just mirrors that

2
00:00:20,840 --> 00:00:26,000
out of focus, there we go.

3
00:00:26,000 --> 00:00:53,840
So tonight I thought I would talk about suffering, everyone's favorite topic, but I want

4
00:00:53,840 --> 00:01:02,320
to talk about suffering because I think the word and the topic which it describes

5
00:01:02,320 --> 00:01:12,320
from a Buddhist perspective is a great way to understand Buddhism, to help us to put

6
00:01:12,320 --> 00:01:25,680
everything together, put all the different pieces of Buddhism together.

7
00:01:25,680 --> 00:01:34,760
The suffering I think is often a topic that we don't want to focus on, maybe not the

8
00:01:34,760 --> 00:01:41,160
first thing we think of when we think of spiritual practice, but that's usually because

9
00:01:41,160 --> 00:01:45,600
spiritual practice is something we see as the opposite.

10
00:01:45,600 --> 00:01:56,600
Spiritual practice should be joyful, peaceful, should be happy, and all of those things

11
00:01:56,600 --> 00:01:59,400
are really the opposite of suffering.

12
00:01:59,400 --> 00:02:10,800
We can end a spirituality really because of suffering, because of the desire for the opposite,

13
00:02:10,800 --> 00:02:11,800
right?

14
00:02:11,800 --> 00:02:17,600
Many people do get into spirituality because they're suffering horribly, they're looking

15
00:02:17,600 --> 00:02:32,360
for a way out, or for many of us there's just a sense that something's wrong, life

16
00:02:32,360 --> 00:02:40,760
isn't terrible, my situation isn't horrible, but there's something missing, we don't

17
00:02:40,760 --> 00:02:46,120
really know what, something's wrong.

18
00:02:46,120 --> 00:02:50,640
And that's related to suffering as well, there are meant for many of us, we don't know

19
00:02:50,640 --> 00:02:57,720
that we're suffering, we don't know that there's possible there's a possibility for something

20
00:02:57,720 --> 00:03:00,640
better.

21
00:03:00,640 --> 00:03:10,680
We think this is as good as it gets, and so we're content with suffering, we're not

22
00:03:10,680 --> 00:03:23,720
content, but we are resigned to it, to resign to our discontent.

23
00:03:23,720 --> 00:03:28,680
Where other people, they're not clear about this, they don't think they're suffering,

24
00:03:28,680 --> 00:03:34,000
but these are often the people who need teachings on suffering the most, because they're

25
00:03:34,000 --> 00:03:47,360
causing suffering in others, they're cultivating addiction, they're setting themselves up

26
00:03:47,360 --> 00:03:56,560
for great suffering, some people, suffering is an important topic, it really is the, it's

27
00:03:56,560 --> 00:03:58,760
a problem-based approach, right?

28
00:03:58,760 --> 00:04:10,320
You find out what the problem is, and you try and solve it, that's sort of how the Buddha

29
00:04:10,320 --> 00:04:18,680
looked at it, but much has been said on this, and I'm going to try to not focus solely

30
00:04:18,680 --> 00:04:26,320
on suffering, but use it as a means of understanding, something deeper about Buddhism.

31
00:04:26,320 --> 00:04:32,400
And I think there is something deeper than just saying, there is suffering, let's get rid

32
00:04:32,400 --> 00:04:33,400
of it.

33
00:04:33,400 --> 00:04:39,280
It's much deeper than that impact, and that will help us understand Buddhism, it's what

34
00:04:39,280 --> 00:04:42,880
I say.

35
00:04:42,880 --> 00:04:47,920
So the Buddha's description of suffering, he starts off quite simply talking about things

36
00:04:47,920 --> 00:04:53,160
that are suffering.

37
00:04:53,160 --> 00:04:58,160
He starts of birth, I guess, because it's just the first in the order, it's not the

38
00:04:58,160 --> 00:05:01,760
first thing that comes to mind for most of us.

39
00:05:01,760 --> 00:05:02,760
What is suffering?

40
00:05:02,760 --> 00:05:08,960
Birth, well, not really, but in the larger scheme of things it makes sense, but moving

41
00:05:08,960 --> 00:05:14,680
on we have ones we can certainly agree on all day just suffering, right?

42
00:05:14,680 --> 00:05:21,000
When you get old, not just growing old, it means like becoming decrepit, that's stressful,

43
00:05:21,000 --> 00:05:30,440
let's suffering, bad, death, death, certainly, yes, for many of us death is clearly understood

44
00:05:30,440 --> 00:05:40,760
to be suffering, sickness is suffering, pain is suffering, right?

45
00:05:40,760 --> 00:05:48,480
Sorrow, lamentation, despair, these are all suffering.

46
00:05:48,480 --> 00:05:50,200
These are straightforward suffering.

47
00:05:50,200 --> 00:05:55,560
This one's no one can deny, yes, they're part of reality.

48
00:05:55,560 --> 00:06:03,880
It's an important question, why do these things exist and given that these things do exist

49
00:06:03,880 --> 00:06:10,560
and may very well exist for us, may very well, will most certainly exist for us at some

50
00:06:10,560 --> 00:06:20,880
point or another?

51
00:06:20,880 --> 00:06:25,080
If we do do something about it, isn't that going to solve every problem?

52
00:06:25,080 --> 00:06:30,680
If we can get rid of all suffering, wouldn't that be the solution to life?

53
00:06:30,680 --> 00:06:35,200
Do we really have to worry about being happy if there's no suffering?

54
00:06:35,200 --> 00:06:38,600
This is why problem-based approaches are useful.

55
00:06:38,600 --> 00:06:41,960
You'll eliminate the problem, there's no problem.

56
00:06:41,960 --> 00:06:50,600
There's no need to look for a solution, there's no need to avoid life if the pain and

57
00:06:50,600 --> 00:06:59,440
suffering of this world, you know, you walk outside, the world doesn't seem to be stressful,

58
00:06:59,440 --> 00:07:01,320
seems quite peaceful.

59
00:07:01,320 --> 00:07:02,320
What's wrong?

60
00:07:02,320 --> 00:07:03,320
What's the problem?

61
00:07:03,320 --> 00:07:06,440
Why is it that we're not happy all the time?

62
00:07:06,440 --> 00:07:19,600
It's something's wrong, we can get rid of that, but then he goes deeper and he points

63
00:07:19,600 --> 00:07:30,840
out that it's not really these experiences that are suffering, and we say it's suffering

64
00:07:30,840 --> 00:07:38,040
is physical pain that's suffering, but is it really?

65
00:07:38,040 --> 00:07:42,200
Can you not have physical pain without suffering from it?

66
00:07:42,200 --> 00:07:45,480
That's quite dreadful to think, isn't it?

67
00:07:45,480 --> 00:07:54,160
If that's the case, the only way to not suffer is to kill yourself or to die because otherwise

68
00:07:54,160 --> 00:08:09,760
there's always going to be physical suffering.

69
00:08:09,760 --> 00:08:39,680
So he points out that it's the, the fact that we get what we don't want.

70
00:08:39,680 --> 00:08:48,280
And then we don't get what we want, it's really they're not getting what you want.

71
00:08:48,280 --> 00:08:55,360
The one thing and the getting are not the same thing, want this, get that, don't want

72
00:08:55,360 --> 00:09:08,480
this, get this, you know, want this, get this, don't want this, get this, right?

73
00:09:08,480 --> 00:09:19,280
It's not one thing, ex getting ex, it's not, it's not right there, that's the problem.

74
00:09:19,280 --> 00:09:26,640
In all these things, it's not the experience because it's, it turns out to be quite arbitrary.

75
00:09:26,640 --> 00:09:38,720
The same pain experienced by one person, quite comfortably in a piece, will be quite unpleasant

76
00:09:38,720 --> 00:09:48,200
for another, you know, no one sees this, I think, more clearly than during meditation.

77
00:09:48,200 --> 00:09:54,080
So many things that we normally wouldn't be able to bear with, we realize, wow, you

78
00:09:54,080 --> 00:10:02,320
know, there's nothing wrong with that experience, that experience really isn't stressful,

79
00:10:02,320 --> 00:10:13,760
isn't painful, isn't suffering, sounds, smells, tastes, feelings, you think heat, heat is

80
00:10:13,760 --> 00:10:19,360
unbearable, heat is hard to endure, when you meditate and you think, well, what's so hard

81
00:10:19,360 --> 00:10:33,520
to endure about it, heat is heat, so you realize, oh wait, it's the aspect of not getting

82
00:10:33,520 --> 00:10:44,160
what we want, getting what we don't want, that's, that's where suffering lies.

83
00:10:44,160 --> 00:10:48,680
So he states that and it's a different kind of suffering, it's a different way of looking

84
00:10:48,680 --> 00:10:55,200
at it, it says that the experience isn't the important thing, the important thing is that

85
00:10:55,200 --> 00:11:22,280
the experience is one that is not wanted, it's the experiences when we cannot endure.

86
00:11:22,280 --> 00:11:32,600
That's the second aspect of the teaching, but he goes further, and this is really what

87
00:11:32,600 --> 00:11:39,760
I want to talk about tonight, the last thing he says on suffering in the canonical description

88
00:11:39,760 --> 00:11:53,520
of suffering is, sankitainapantupada nakantadukha, sankitaina in brief, or the summit all up, or

89
00:11:53,520 --> 00:12:11,560
to put a fine point on it, nakantupada nakantadukha, the five khandas, the five khandas of

90
00:12:11,560 --> 00:12:22,200
upadana are suffering, it's hard to translate that because it's very terse, in poly they

91
00:12:22,200 --> 00:12:29,440
can shorten everything to a couple of words and let the context explain what is being

92
00:12:29,440 --> 00:12:38,280
said, in English we have all these little words that help us to understand exactly, you

93
00:12:38,280 --> 00:12:49,320
could criticize the language and say it's not articulate enough, but there are benefits

94
00:12:49,320 --> 00:12:58,800
to it as well, English we often are very specific and we can be because we have many different

95
00:12:58,800 --> 00:13:10,360
little words that help us, but upadana khanda just means clinging, clinging, aggregate plural,

96
00:13:10,360 --> 00:13:17,480
clinging khandas or khandas actually doesn't really mean aggregate, it means heap or big

97
00:13:17,480 --> 00:13:34,880
mass, that's a khanda, the five of them, five masses of or related to, these are the words

98
00:13:34,880 --> 00:13:41,920
that are missing that we don't clearly see, and I don't think it's the five aggregates

99
00:13:41,920 --> 00:13:48,920
of clinging because it's deeper than that, I would rather use something like related to,

100
00:13:48,920 --> 00:14:15,920
the five khandas, for lack of a good translation, the five khandas of clinging.

101
00:14:15,920 --> 00:14:34,840
And what this says, what it's saying is that anything related to clinging is going to

102
00:14:34,840 --> 00:14:41,680
be related to suffering, that's basically what it's saying, to sum it up or in brief, to

103
00:14:41,680 --> 00:14:54,760
put a fine point on it, it's this clinging thing, that's the problem, now that's not

104
00:14:54,760 --> 00:15:02,440
something revolutionary, that should be something quite familiar in Buddhism, I mean it is

105
00:15:02,440 --> 00:15:08,000
a revolutionary thing and it is an important teaching, but it's something I talk a lot about,

106
00:15:08,000 --> 00:15:13,120
so briefly, go over it again, it's not our experiences that cause us suffering, it's

107
00:15:13,120 --> 00:15:18,680
our reactions to them, suffering doesn't come because we have physical pain, it comes

108
00:15:18,680 --> 00:15:25,200
because we don't like the physical pain, that's a kind of a clinging, we're not able

109
00:15:25,200 --> 00:15:35,920
to go with the flow, this is experience, we get hung up on it, it's like I remember once

110
00:15:35,920 --> 00:15:47,200
when I was young, stupid kids, we were, I was at a mall and I grew up in the countryside

111
00:15:47,200 --> 00:15:52,280
so we didn't often go to malls and it was an escalator and escalators were kind of cool,

112
00:15:52,280 --> 00:15:58,560
I mean we don't have escalators where I grew up on an island, a big island, I don't think

113
00:15:58,560 --> 00:16:05,360
there's, pretty sure there was still no escalator on the entire island, we haven't been

114
00:16:05,360 --> 00:16:13,840
back in a while, and so I thought it would be fun to try and stop the escalator and I pushed

115
00:16:13,840 --> 00:16:21,760
it and I used my arm and I just, and I gave myself ten to nine days and to this day I think

116
00:16:21,760 --> 00:16:33,120
I'd still have an elbow problem, is that kind of thing, right, that's where you could

117
00:16:33,120 --> 00:16:38,040
try to stop something and you get, you just create friction and suffering and service

118
00:16:38,040 --> 00:16:57,240
up, but what's more interesting here, or interesting tonight, is what is this everything?

119
00:16:57,240 --> 00:17:02,360
Hey, one of these five things, wait a minute, you skipped over an important part, I

120
00:17:02,360 --> 00:17:09,640
don't know, I'm coming to it, this is the suffering part, suffering is something very

121
00:17:09,640 --> 00:17:19,040
important, in fact it's most important, the Buddha said, this is the first noble truth

122
00:17:19,040 --> 00:17:25,280
and he said it's to be fully understood, what are we supposed to do about suffering and

123
00:17:25,280 --> 00:17:29,400
there's something I've said many times, pointed out many times, and you have to point

124
00:17:29,400 --> 00:17:38,080
it out, because even as Buddhists we ignore this part, I ask people as an audience sometimes,

125
00:17:38,080 --> 00:17:45,160
what's the first number of truths, often they'll say life is suffering, no, no, no, no,

126
00:17:45,160 --> 00:17:50,280
no life, life is suffering is not the first number of truths, no matter what, there's this

127
00:17:50,280 --> 00:17:55,640
really good book called What the Buddha Taught, but he makes a cardinal mistake in it

128
00:17:55,640 --> 00:18:02,360
and I can't forgive him for it, he says that life is suffering, I'm pretty sure, yes,

129
00:18:02,360 --> 00:18:08,200
it's that book, he says life is suffering and so anyone who knows a little bit about Buddhism

130
00:18:08,200 --> 00:18:13,360
is, well that's the first noble truth, that's not what it says, it's an important distinction

131
00:18:13,360 --> 00:18:19,880
because it's a very bad way of teaching, you don't get anywhere by telling someone,

132
00:18:19,880 --> 00:18:28,200
okay, life is suffering, that's the basis of our, that's the basis of our religious practice.

133
00:18:28,200 --> 00:18:38,160
No, the first noble truth is that suffering, suffering, period, no qualifier, no, this

134
00:18:38,160 --> 00:18:44,040
is suffering, that is suffering, suffering is the first noble truth, and it's the noble

135
00:18:44,040 --> 00:18:49,040
truth for this reason, because it's the problem, I mean we're not focusing on suffering

136
00:18:49,040 --> 00:18:53,200
because we want to suffer, I mean no one, that's a ridiculous accusation, if anyone

137
00:18:53,200 --> 00:18:59,280
were to level it, I say okay, let's talk about suffering, do you really think it's because

138
00:18:59,280 --> 00:19:06,840
I want us all to suffer? No, clearly I'm a problem, that means I'm a problem, approach-based

139
00:19:06,840 --> 00:19:14,880
sort of person, problem-based approach sort of person, and I want to tackle this problem

140
00:19:14,880 --> 00:19:21,120
because the definition of suffering, if it's not something that you don't want, if it's

141
00:19:21,120 --> 00:19:27,040
not something that is not undesirable, then I can't be called suffering, I think it's

142
00:19:27,040 --> 00:19:42,720
pretty much tied up in the definition.

143
00:19:42,720 --> 00:19:50,080
This was the first noble truth, and he said, Paris nier, it should be nier is just means

144
00:19:50,080 --> 00:19:58,960
known, should be known, Paris, Paris nier, Paris means all around, it means all encompassing,

145
00:19:58,960 --> 00:20:09,640
fully, Paris nier means it should be fully known, fully understood, that's what we should

146
00:20:09,640 --> 00:20:20,880
do about suffering, that's an important point, I mean before we go on to what I really

147
00:20:20,880 --> 00:20:27,200
want to talk about, this is all a part of it, it's an important part of it, is that it's

148
00:20:27,200 --> 00:20:33,720
not about running away, and then again I've talked about this before, the way out

149
00:20:33,720 --> 00:20:45,040
of suffering is not to try and fix the problem, just to understand the problem, and this

150
00:20:45,040 --> 00:21:00,000
gets into the idea of the other part of this equation, the five aggregates, the five things,

151
00:21:00,000 --> 00:21:09,160
because it says that of any given experience, let's say going to the dentist, of going

152
00:21:09,160 --> 00:21:21,840
to the dentist, our problem is not that we have to go to the dentist, our problem is that

153
00:21:21,840 --> 00:21:29,920
there are certain aspects of that experience, of that event, certain experiences that

154
00:21:29,920 --> 00:21:45,660
are a part of that event, that are unacceptable to us, that we react to, and that's

155
00:21:45,660 --> 00:21:55,000
an important distinction, because it says those experiences aren't suffering either, those

156
00:21:55,000 --> 00:22:07,200
experiences aren't able to cause us stress with suffering, but we ourselves react in such

157
00:22:07,200 --> 00:22:19,320
a way with a view, and it's not a very clear view, often it's a knee-jerk reaction

158
00:22:19,320 --> 00:22:26,360
with a view to finding happiness, freedom from suffering, freedom from that suffering, and

159
00:22:26,360 --> 00:22:35,160
we're just horrible at it, our ways of reacting to things are just horribly unskillful,

160
00:22:35,160 --> 00:22:39,680
so someone says something you don't like, you yell at them, you get angry, what purpose

161
00:22:39,680 --> 00:22:44,800
did that serve, did that solve your problem?

162
00:22:44,800 --> 00:22:50,520
Did that a solution to the problem?

163
00:22:50,520 --> 00:22:57,360
You stub your foot on the table, you kick the table, yell at the table, good solution,

164
00:22:57,360 --> 00:23:12,120
way to go, right, no, not a good solution, didn't solve your problem, made it worse, we

165
00:23:12,120 --> 00:23:18,560
can do something wrong sometimes, well oftentimes our solution is to do something that

166
00:23:18,560 --> 00:23:48,400
actually makes it worse.

167
00:23:48,400 --> 00:23:57,400
And so the real problem, this is a claim and it's an important claim, is that we don't

168
00:23:57,400 --> 00:24:04,560
understand what we're doing to ourselves, we don't understand suffering, we don't see

169
00:24:04,560 --> 00:24:14,640
that, hey wrong, bad suffering, we get angry and we don't think, yeah this is making

170
00:24:14,640 --> 00:24:20,440
me suffer, maybe we do intellectually and often we do as Buddhists until actually think,

171
00:24:20,440 --> 00:24:26,720
oh yes anger that's just going to cause me more suffering, but we see how messed up we are

172
00:24:26,720 --> 00:24:33,800
and how deep down our reaction is, yes this is the solution, this is the way to find peace,

173
00:24:33,800 --> 00:24:40,360
happiness and freedom from suffering all the way, all freak out over it, all react violently

174
00:24:40,360 --> 00:24:51,480
to it, it's just absurd, right, it's it's it's it's harmful as the opposite reaction,

175
00:24:51,480 --> 00:25:05,120
the opposite outcome, we don't understand suffering, the things that we cling to as satisfying

176
00:25:05,120 --> 00:25:12,720
are not satisfied, the things that we cling to as the problem are not the problem, we

177
00:25:12,720 --> 00:25:18,480
don't understand suffering, it's very important, I mean this is this this part of the

178
00:25:18,480 --> 00:25:26,480
equation is, I mean this is it, this is Buddhism, so I think what I could say then is

179
00:25:26,480 --> 00:25:30,680
there's these two sides that we really have to bring together, this is the first side,

180
00:25:30,680 --> 00:25:37,560
the suffering side, the suffering part of, it's not an equation, but part of the statement,

181
00:25:37,560 --> 00:25:43,200
but there's the other part of the statement and it has to do with understanding, it's

182
00:25:43,200 --> 00:25:49,040
the link here is the understanding, well okay why is guy, usually I don't understand

183
00:25:49,040 --> 00:25:57,720
suffering, tell me what is suffering, tell me how I can understand suffering and so this

184
00:25:57,720 --> 00:26:03,120
is this is why Buddhism is complicated and hard to understand, if you don't have this

185
00:26:03,120 --> 00:26:08,280
kind of putting things together, it can be quite confusing because suddenly the Buddha's

186
00:26:08,280 --> 00:26:13,120
off talking about non-self, right, another very big part of Buddhism and I think for this

187
00:26:13,120 --> 00:26:19,400
talk it's an important point, what the heck does that have to do with suffering, right,

188
00:26:19,400 --> 00:26:31,280
and so we try to understand non-self, we try to understand impermanence, how did these

189
00:26:31,280 --> 00:26:37,880
relate and so intellectually we sometimes get an understanding like oh yes, I try to control

190
00:26:37,880 --> 00:26:44,320
things that I can't control, right, that's suffering, but there's an important, there's another

191
00:26:44,320 --> 00:26:51,960
important aspect that links everything together, helps us understand not just intellectually

192
00:26:51,960 --> 00:27:02,400
but helps us really affect this kind of change and that's experience, changing the way

193
00:27:02,400 --> 00:27:09,320
we look at things, non-self, why I bring it up because it doesn't just mean that things

194
00:27:09,320 --> 00:27:15,040
aren't under your control, it doesn't just mean that you don't have a soul, we don't

195
00:27:15,040 --> 00:27:35,640
have souls, we don't have egos, we're not snowflakes after all, we're just experiences

196
00:27:35,640 --> 00:27:52,640
but that reality has nothing to do with entities, beings, souls, so the idea of a soul

197
00:27:52,640 --> 00:28:02,240
is something atomic, something unbreakable, indivisible, the idea of things is the same

198
00:28:02,240 --> 00:28:11,480
the very same idea, if you look at this and you say hey, this is a person, that's an idea

199
00:28:11,480 --> 00:28:18,880
that comes up in your mind, that's the concept of a being, right, this might be done

200
00:28:18,880 --> 00:28:29,200
my teacher said what is this, it's a fist, so that fist is a thing, right, now watch, where

201
00:28:29,200 --> 00:28:40,920
the fist go, is it real, what's the fist real, fist, no fist, here, go on, this is another

202
00:28:40,920 --> 00:28:48,360
funny thing about it is this is ata, this is self, and this is the five aggregates, right,

203
00:28:48,360 --> 00:28:58,600
what's behind the self is just the five aggregates, but what's on my agenda to do, because

204
00:28:58,600 --> 00:29:05,160
many people have heard of the five aggregates is to differentiate here, because if this is

205
00:29:05,160 --> 00:29:12,640
the self, the five aggregates are not simply dividing it up into five things, five aggregates

206
00:29:12,640 --> 00:29:19,600
are something completely different from the self, categorically different, they're a different

207
00:29:19,600 --> 00:29:29,120
kind of thing, let's understand that, and I've talked about this is my explanation of it,

208
00:29:29,120 --> 00:29:35,080
so I'm not getting this from the suit, I'm getting it from my readings and what's been

209
00:29:35,080 --> 00:29:40,440
passed on to me, and this is my way of explaining what are they talking about, maybe this

210
00:29:40,440 --> 00:29:48,360
helps, the way we look at things, the way we look at reality, yes, here we are sitting

211
00:29:48,360 --> 00:29:58,240
in this room, there's the room, there's us, there's things, there's distances and so on, spatial

212
00:29:58,240 --> 00:30:12,040
reality, and that's one way of looking at, of approaching, of seeing reality, so I put

213
00:30:12,040 --> 00:30:18,080
it to you that that's not the Buddhist way of looking at reality, that reality is the reality

214
00:30:18,080 --> 00:30:25,920
of self, and there's a lot of problems that come from that, that involves our understanding

215
00:30:25,920 --> 00:30:39,400
of suffering, you made me suffer, the dentist made me suffer, this situation, these things,

216
00:30:39,400 --> 00:30:43,840
our approach to suffering is to get away from people, this person's making suffering,

217
00:30:43,840 --> 00:30:53,800
get away, dentist made me suffering, don't go to the dentist, when the reality is quite different,

218
00:30:53,800 --> 00:31:02,240
it's not this person that's making us suffer, it's the experiences, this experience particularly,

219
00:31:02,240 --> 00:31:08,280
out of all the many experiences, this one here, because I react to that one, I get upset

220
00:31:08,280 --> 00:31:16,040
about that one, that's the jumping off point, suppose I see someone while seeing them

221
00:31:16,040 --> 00:31:23,240
isn't suffering, right, stressful, but then I remember what they did to me, that person hit

222
00:31:23,240 --> 00:31:35,360
me before, that person called me a nasty name, that moment where I remember that is a jumping

223
00:31:35,360 --> 00:31:43,040
off point, I react to that badly, I get angry, there you go, that's where suffering comes

224
00:31:43,040 --> 00:31:50,640
from, I reacted to that memory, I really had nothing to do with the person except incidentally

225
00:31:50,640 --> 00:32:01,240
they were the trigger, and so this is a very different way of looking at reality, experiences,

226
00:32:01,240 --> 00:32:06,160
this one, this one, this one, this one, this one, this one, and this is why we see, I'm not

227
00:32:06,160 --> 00:32:11,880
making this up, read the Buddha Sutras, this is why we see, okay, suffering, suffering, suffering,

228
00:32:11,880 --> 00:32:18,040
why is he suddenly talking about the six senses, why is he breaking it up, breaking himself

229
00:32:18,040 --> 00:32:25,480
up into five things, what is this talking about, this is how, this is the means by which

230
00:32:25,480 --> 00:32:32,680
you come to understand reality and thereby suffering, because suffering is in reality, it's

231
00:32:32,680 --> 00:32:39,520
like, you're driving down the street and suddenly your car stops working, and you say

232
00:32:39,520 --> 00:32:47,200
the car stopped working, and the mechanic, you tell the mechanic might stop working, the

233
00:32:47,200 --> 00:32:55,400
mechanic knows it's just a very poor way of describing the situation, it's true, right,

234
00:32:55,400 --> 00:33:04,760
going to the dentist's suffering is true for most people, in the same way that the car broke

235
00:33:04,760 --> 00:33:09,880
down, but the mechanic will tell you that's not really the case, to say that the car broke

236
00:33:09,880 --> 00:33:19,600
down is just horribly insufficient, what we have to say is that the carburetor, flood

237
00:33:19,600 --> 00:33:25,520
it or whatever, I know nothing about cars, but the carburetor flooded, I've heard that before,

238
00:33:25,520 --> 00:33:31,440
right, carburetors are things, round things, I think, and they flood, or they used to

239
00:33:31,440 --> 00:33:42,000
when I was young, so I tell you that, I tell you that and yes, there's, and it can explain,

240
00:33:42,000 --> 00:33:47,680
even the carburetor flooded, it's just an expression, it can explain the mechanics, probably

241
00:33:47,680 --> 00:33:52,800
wouldn't bother, and just say your carburetor is flooded, I'll fix it, but a good mechanic

242
00:33:52,800 --> 00:34:06,680
should know, what does it mean to say carburetor flooded, and that's the mechanics behind

243
00:34:06,680 --> 00:34:13,720
going to the dentist or any kind of unpleasant experience, is that it's the moment, and

244
00:34:13,720 --> 00:34:20,960
the real problem is this, this, this, and this, and if you see that, then you're on the

245
00:34:20,960 --> 00:34:26,040
way to solving the problem, right?

246
00:34:26,040 --> 00:34:43,520
Because in reality, in our lives, our responses to things are based on our state of

247
00:34:43,520 --> 00:34:55,960
mind at that moment, are based on our outlook in our inclination, I react violently

248
00:34:55,960 --> 00:35:13,440
to pain, because I'm inclined to react violently to pain.

249
00:35:13,440 --> 00:35:22,640
If I were inclined to experience pain peacefully, then I won't suffer from it.

250
00:35:22,640 --> 00:35:29,200
And so by understanding suffering, by understanding what's going on here, oh yes, reacting

251
00:35:29,200 --> 00:35:37,520
in that way, that makes me suffer, that's stress, like take anxiety, for example, we think

252
00:35:37,520 --> 00:35:43,800
I get anxious about this and while it's a big problem, but break it down, observe it, meditate

253
00:35:43,800 --> 00:35:52,600
on it, and you'll see, you'll be so overwhelmed by how stressful it is to get anxious

254
00:35:52,600 --> 00:35:58,360
that you'll just not want to get anxious anymore, but it sounds stupid, it sounds simple,

255
00:35:58,360 --> 00:35:59,960
but it's true.

256
00:35:59,960 --> 00:36:06,600
If your mind flying is anxious, you'll start to say, why am I getting anxious, you know,

257
00:36:06,600 --> 00:36:08,600
what purpose does this serve?

258
00:36:08,600 --> 00:36:14,800
It's like, we know there's no purpose, but we just haven't deeply understood it, and

259
00:36:14,800 --> 00:36:19,960
so another way of describing what it means to understand suffering, it just means to see

260
00:36:19,960 --> 00:36:27,360
deeper, to see clearer, because it's not even so much that we have a wrong understanding,

261
00:36:27,360 --> 00:36:34,320
it's that we have no understanding, we have no idea about the car, we have no idea

262
00:36:34,320 --> 00:36:40,720
that you have to put in the clutch, right?

263
00:36:40,720 --> 00:36:49,800
Someone drives standard for the first time, and they turn on the car and it goes, right?

264
00:36:49,800 --> 00:36:59,120
I can't remember, I used to drive, we just have no clue, and so we suffer, so learn

265
00:36:59,120 --> 00:37:10,080
about the car and learn how to push in the clutch, put it in gear, slowly clutch gas,

266
00:37:10,080 --> 00:37:24,640
so I remember correctly, learn how the system works, but first you have to understand the

267
00:37:24,640 --> 00:37:29,840
system, and so this is where we get into the five aggregates, so the Buddha here is not

268
00:37:29,840 --> 00:37:37,720
only saying that suffering is related to clinging, but he's saying it's related to clinging

269
00:37:37,720 --> 00:37:47,680
to experience, not things, it's not proper to say, yes, I cling to food and so food is

270
00:37:47,680 --> 00:37:54,400
a cause for stress for me, when I don't kill food I want, I'm dissatisfied, I cling

271
00:37:54,400 --> 00:38:01,440
to people, so when I see this person I get very angry, no, it's not the people, it's not

272
00:38:01,440 --> 00:38:07,080
the places, it's not the things, it's not the souls or the selves or the entities, it's

273
00:38:07,080 --> 00:38:17,480
the experiences, which is the five aggregates, a five aggregates thing is an experience,

274
00:38:17,480 --> 00:38:29,800
each experience we have is made up of five parts, there's the physical aspect to it, there's

275
00:38:29,800 --> 00:38:40,040
the feeling, the sensual aspect to it, I guess is the word to you, meaning it's pleasurable,

276
00:38:40,040 --> 00:38:47,240
it's painful, it's neutral, there's the perception of it, or that's probably not the

277
00:38:47,240 --> 00:38:56,920
right word, but the recognition of it, the processing of it, like oh yeah, that's a person,

278
00:38:56,920 --> 00:39:06,240
that's red, that's yellow, that's white, the recognition and processing, neutral, neutral

279
00:39:06,240 --> 00:39:13,880
processing of the experience, and then there's the reacting to it, it's good, it's bad,

280
00:39:13,880 --> 00:39:22,880
it's me, it's mine, it's tall, it's short, maybe not tall short, but the liking, disliking

281
00:39:22,880 --> 00:39:33,640
and stuff, most important is that part, and then there's the fifth one, there's the experience

282
00:39:33,640 --> 00:39:41,480
of all this, the fact that we're aware of this, this is not a brain doing things, it's

283
00:39:41,480 --> 00:39:52,160
a problem that physicalists have with, I mean it seemed to really fudge things and can't answer

284
00:39:52,160 --> 00:39:58,040
and a hard problem which is consciousness, what is consciousness, I mean this is the most

285
00:39:58,040 --> 00:40:03,880
important part, it's not a robot, these things aren't happening to robots, the program

286
00:40:03,880 --> 00:40:11,840
a robot to react to suffering, the robot's not feeling pain, I mean theoretically it's

287
00:40:11,840 --> 00:40:22,240
possible for a consciousness to arise, if you build a complex enough machine for a consciousness

288
00:40:22,240 --> 00:40:30,880
to then get caught up in it, I want to radically say it's theoretically possible, but

289
00:40:30,880 --> 00:40:37,800
it would have to be, it would have to be in a really, on the level of a human being, a

290
00:40:37,800 --> 00:40:46,720
human fetus, for a mind to actually become caught up in it and react to and be able to

291
00:40:46,720 --> 00:40:55,640
experience in conjunction with it, but the point is it's still a consciousness, without

292
00:40:55,640 --> 00:41:02,120
consciousness, you don't have any of this, so that's the five.

293
00:41:02,120 --> 00:41:08,280
And that's what we're doing here, that's what we're focusing on, that's what our activity

294
00:41:08,280 --> 00:41:19,280
should be in the meditation practice, more clearly, that's what you're going to see when

295
00:41:19,280 --> 00:41:30,240
you meditate. And so this is kind of a support for new meditators, because when you come

296
00:41:30,240 --> 00:41:36,400
to meditate it's going to look very unfamiliar, and you're going to rely, try to rely on

297
00:41:36,400 --> 00:41:45,320
past paradigms, ways of looking at reality, and it's just not going to fit the facts,

298
00:41:45,320 --> 00:41:51,440
because when you meditate, all you have are experiences, it seems nothing like real life.

299
00:41:51,440 --> 00:41:57,200
Real life is full of people, places, and things, meditations, just full of experience.

300
00:41:57,200 --> 00:42:03,720
Until you start to see that, ah, wait, isn't my daily life full of all those experiences

301
00:42:03,720 --> 00:42:04,720
as well?

302
00:42:04,720 --> 00:42:11,320
Well, duh, there's really no difference. All that's happening, everything that happens

303
00:42:11,320 --> 00:42:21,680
to me during meditation happens to me as well, during my daily life. And there's no difference.

304
00:42:21,680 --> 00:42:28,200
The difference is the way we look at things. In our life, we very rarely stop to think

305
00:42:28,200 --> 00:42:33,320
about what am I experiencing now. That's why therapists will always ask you, how do you

306
00:42:33,320 --> 00:42:40,560
feel? Very clever, and they've picked up a lot from Buddhism I would like to, ah, proudly

307
00:42:40,560 --> 00:42:46,720
assert? Well, not proudly, but if I were, if I were a sort of proud sort of person, maybe

308
00:42:46,720 --> 00:43:01,840
I would, how are you feeling? Wait a minute, wait a minute, stop talking about the situation.

309
00:43:01,840 --> 00:43:08,400
How does it, how did it make you feel? Right, please say a question. How does that make

310
00:43:08,400 --> 00:43:14,120
you feel? It's probably one that therapists don't use anymore because it's so cliche,

311
00:43:14,120 --> 00:43:19,720
but it really is the essence of it. This experience was important. You can sit here and

312
00:43:19,720 --> 00:43:26,880
tell me all day about what happened. And explain all your, I get that. Sometimes meditators

313
00:43:26,880 --> 00:43:30,960
will want to explain to me things that happened to them. It's not really important. You

314
00:43:30,960 --> 00:43:37,920
can. I mean, I don't mind. But what is all that's important is your reactions to it.

315
00:43:37,920 --> 00:43:43,920
And understanding the experience of it. How did that process work? And finding out what

316
00:43:43,920 --> 00:43:49,560
went wrong? The experience wasn't wrong. The experiences are just experiences. What went

317
00:43:49,560 --> 00:44:02,760
wrong is how you interacted with it, how you dealt with it. And so that I think is a good

318
00:44:02,760 --> 00:44:08,800
way of, it's a good sort of framework. These things I've been talking about, I don't

319
00:44:08,800 --> 00:44:16,520
know, I wasn't trying to describe what is Buddhism, but trying to give some sort of a

320
00:44:16,520 --> 00:44:21,960
framework. What are we talking about when we say Buddhism, Buddhism? What are we talking

321
00:44:21,960 --> 00:44:32,440
about? We're talking about these things, really suffering, but experience, the experience

322
00:44:32,440 --> 00:44:39,440
of suffering, and the suffering that comes from experience or involved with experience.

323
00:44:39,440 --> 00:45:06,440
So, that's the demo for tonight. Thank you all for tuning in. Have a good night.

