1
00:00:00,000 --> 00:00:26,360
Good evening.

2
00:00:26,360 --> 00:00:52,360
So we're starting up again, I've been away for most of the month, the past month.

3
00:00:52,360 --> 00:01:05,680
Tonight we have two new meditators staying here, and as usual it's a chance to have an opportunity

4
00:01:05,680 --> 00:01:29,520
to go over some of the basics.

5
00:01:29,520 --> 00:01:46,680
So I thought it might be a good idea to do something fairly traditional, and so what I'm

6
00:01:46,680 --> 00:02:01,400
going to do actually is to give you the poly words that describe what it is that we do.

7
00:02:01,400 --> 00:02:18,200
So it's just really a reminder of what it is that we're here to practice.

8
00:02:18,200 --> 00:02:25,520
And so there are only six words, it's not a lot, but we recommend that you remember

9
00:02:25,520 --> 00:02:39,600
these six words in your practice, of course it helps if you understand poly, but some

10
00:02:39,600 --> 00:02:48,520
of them are quite familiar words, so if I can have you guys repeat after me okay, I don't

11
00:02:48,520 --> 00:02:51,800
have them written out here, maybe in the future I'll have them written out, but just

12
00:02:51,800 --> 00:02:56,640
repeat, you can humor me with this, and I'll explain what they are.

13
00:02:56,640 --> 00:03:10,360
So the first word is Vipassana, can you say that, so it's more with a W than a Vipassana,

14
00:03:10,360 --> 00:03:35,520
the second one is Satipatana, Vipassana, Satipatana, Kaya, Vedana, Jita, Dhamma, Vipassana,

15
00:03:35,520 --> 00:04:02,640
Satipatana, Kaya, Vedana, Jita, Dhamma, Vipassana, Satipatana, Kaya, Vedana, Jita, Dhamma.

16
00:04:02,640 --> 00:04:08,600
So my teacher used to make everyone, or still does make everyone memorize these, this

17
00:04:08,600 --> 00:04:17,480
is the memorization portion of the learning for new students, your students, you come here

18
00:04:17,480 --> 00:04:26,840
and you're like kindergarten students, now I think both of you have background and some

19
00:04:26,840 --> 00:04:36,520
basis in meditation, but it doesn't matter, we put you in kindergarten anyway.

20
00:04:36,520 --> 00:04:41,920
And that's fine, learning basics again and again is always good, because actually these

21
00:04:41,920 --> 00:04:51,840
six words describe of complete practice, they don't, they aren't just the basic beginner

22
00:04:51,840 --> 00:05:04,360
steps that you start with and then move on to something else.

23
00:05:04,360 --> 00:05:09,560
So my, I brought up my teachers because one thing interesting that he always says is just

24
00:05:09,560 --> 00:05:18,120
just memorizing them like that is a very good thing, it's interesting we skip over this

25
00:05:18,120 --> 00:05:31,000
quality of just being here, so much appreciation to you too for being here, because just

26
00:05:31,000 --> 00:05:43,000
being here has the great intention or the great aspiration, that's very much worthy of appreciation,

27
00:05:43,000 --> 00:05:50,200
your intentions on coming here are wonderful.

28
00:05:50,200 --> 00:05:57,960
And on memorizing, there's a similar quality to memorizing these teachings because you've

29
00:05:57,960 --> 00:06:10,400
made it, you've made it to the, the practice, and you've now come in contact with the practice.

30
00:06:10,400 --> 00:06:15,720
And even though you don't understand these six words, repassana, satipatana, kaioidana,

31
00:06:15,720 --> 00:06:27,840
jitatana, just hearing them, and reciting them is enough to be reborn in heaven, just

32
00:06:27,840 --> 00:06:35,680
for your good intentions, if you don't believe that sort of thing, it's important to

33
00:06:35,680 --> 00:06:42,440
understand that that's a big part of what we're doing is cultivating good intentions, and

34
00:06:42,440 --> 00:06:51,520
our good thoughts, our good positive states of mind, our even just our appreciation of

35
00:06:51,520 --> 00:07:00,680
the Dhamma, our willingness to learn the Dhamma is, is there important qualities to cultivate?

36
00:07:00,680 --> 00:07:05,760
Of course, it's not enough to become enlightened, so you have to actually understand what

37
00:07:05,760 --> 00:07:14,880
these words mean, repassana starting at the top, repassana, repassana is often translated

38
00:07:14,880 --> 00:07:24,520
as insight, I don't think that's the best translation of it, bassana means seeing, or

39
00:07:24,520 --> 00:07:36,120
sight, perhaps, but seeing is closer, and we means clearly, good meaning to, or likely

40
00:07:36,120 --> 00:07:51,520
means clearly, seeing clearly, and why that's important is because it, it captures quite

41
00:07:51,520 --> 00:07:59,160
well the essence of what we're trying to do, repassana is a word that describes why we're

42
00:07:59,160 --> 00:08:04,000
doing what we're doing, it's not the part of this that represents what we're actually

43
00:08:04,000 --> 00:08:13,080
doing, but it's the, even just the momentary goal that we try to accomplish through

44
00:08:13,080 --> 00:08:24,120
the rest of this, through the actual practice, seeing clearly, so keep that in mind because

45
00:08:24,120 --> 00:08:31,160
what are some other goals you might have from practicing meditation to feel calm, to find

46
00:08:31,160 --> 00:08:40,560
peace, to stop this or that, or to change this or that, to fix this or that, to become

47
00:08:40,560 --> 00:08:51,920
this or that, and many of those goals are part of general Buddhist description of what

48
00:08:51,920 --> 00:08:57,440
we're aiming for, right, what is the goal of Buddhism while it pays for freedom from suffering

49
00:08:57,440 --> 00:09:09,640
absolutely, but it's not central to, it doesn't centrally describe the immediate outcome

50
00:09:09,640 --> 00:09:16,640
of the practice, sort of like the state of mind we're trying to achieve, which then leads

51
00:09:16,640 --> 00:09:23,680
to so many positive things like peace, happiness, and freedom from suffering, even letting

52
00:09:23,680 --> 00:09:31,160
go, right, and talk about okay, your practice is for letting go, but how do you let go?

53
00:09:31,160 --> 00:09:36,720
There's many theories on how to let go.

54
00:09:36,720 --> 00:09:42,280
The most common one is just listening to people and they tell you just let it go, right,

55
00:09:42,280 --> 00:09:49,480
someone tells you let it go, they seem to be saying, on hearing this, gain the power to

56
00:09:49,480 --> 00:09:58,960
just suddenly let go of the things that you cling to, or it's funny because not really

57
00:09:58,960 --> 00:10:10,520
how it works, another one is forcing yourself to let go, being working hard and letting

58
00:10:10,520 --> 00:10:16,560
go, I mean it's working hard is part of what we do, for sure you can see it's hard work,

59
00:10:16,560 --> 00:10:31,320
but in some sense the hard work is not working, it's not pushing, not controlling.

60
00:10:31,320 --> 00:10:35,080
When you force yourself to let go, you're not, it's not an act of letting go, it doesn't

61
00:10:35,080 --> 00:10:44,160
work that way, I mean, even letting go comes from seeing clearly, right, the premise

62
00:10:44,160 --> 00:10:56,040
is that the way we see things now is what causes us to cling, what we don't see, our lack

63
00:10:56,040 --> 00:11:02,640
of clear vision, our lack of clarity and our relationship with our experiences is what

64
00:11:02,640 --> 00:11:15,760
leads us to cling to them, try to fix them, try to control them, try to possess them to

65
00:11:15,760 --> 00:11:19,920
sustain them and so on.

66
00:11:19,920 --> 00:11:27,920
Why we react to things, why we cause ourselves suffering and cause it to people suffering.

67
00:11:27,920 --> 00:11:34,360
We think it's a good idea, it's based on our vision and our understanding of the situation

68
00:11:34,360 --> 00:11:48,240
which is often no vision at all, it's often just a habitual feeling or conception

69
00:11:48,240 --> 00:11:53,040
of what's the right response, what's a good response.

70
00:11:53,040 --> 00:12:01,240
Getting angry is good when people do things that you don't like, getting worried is a

71
00:12:01,240 --> 00:12:11,600
good response for bad thoughts or concerns about the future.

72
00:12:11,600 --> 00:12:22,880
Getting depressed is a good response to loss and failure and so on.

73
00:12:22,880 --> 00:12:35,680
Everything in sadness is a good response to loss, to loss and death and so on, but craving

74
00:12:35,680 --> 00:12:46,240
something or striving to obtain things is a good response to pleasure and so on.

75
00:12:46,240 --> 00:12:50,320
And why this is is because we fail to see three things and so this is what we pass in

76
00:12:50,320 --> 00:12:59,760
a to in summary, so there's many things we could see clearly about, the three things we

77
00:12:59,760 --> 00:13:05,400
need to see clearly about are one that you have everything inside of ourselves and in the

78
00:13:05,400 --> 00:13:13,600
world around us is impermanent, overcome this concept that some part of our experience

79
00:13:13,600 --> 00:13:23,520
is something exists that lasts forever and it's predictable and it's sustainable.

80
00:13:23,520 --> 00:13:31,200
You know our pleasure, our pain, to see clearly how it doesn't last more than a moment.

81
00:13:31,200 --> 00:13:39,040
The actual experience has arrived and sees anything that we might cling to or react to

82
00:13:39,040 --> 00:13:46,680
is just our own imagination, that's good or bad, it's all you know.

83
00:13:46,680 --> 00:13:58,240
And unpredictable as well, so our reactions are unhelpful, wanting something to stay, wanting

84
00:13:58,240 --> 00:14:11,480
something to go and so on, it's not, we can't predict, it's unstable.

85
00:14:11,480 --> 00:14:24,240
The second thing is that inside of ourselves and in the world around us everything is unsatisfying.

86
00:14:24,240 --> 00:14:33,400
The things that we cling to cannot satisfy us, things that we don't like, we can't fix them.

87
00:14:33,400 --> 00:14:39,800
Because they're unstable, they're unpredictable.

88
00:14:39,800 --> 00:14:49,040
Suffering this is often called, what it means is that you suffer when you get caught

89
00:14:49,040 --> 00:14:56,920
up in it and suffer if you try to fix it, try to control it, try to keep it, try to get rid

90
00:14:56,920 --> 00:14:57,920
of it.

91
00:14:57,920 --> 00:15:04,480
I think they're called, it's called suffering because it's like a hot fire, a hot fire

92
00:15:04,480 --> 00:15:09,560
is very painful, but it's not painful if you're over here and it's over there, it's

93
00:15:09,560 --> 00:15:14,600
painful when you jump in it when you get caught up in it, so experience we get caught

94
00:15:14,600 --> 00:15:24,320
up in experience, and all it takes is getting involved in it, having some kind of desire

95
00:15:24,320 --> 00:15:32,120
in regards to it, some kind of intention, I'll get caught up in this, that causes stress

96
00:15:32,120 --> 00:15:39,800
and suffering, so seeing that, about everything, every part of our experience, it's not worth

97
00:15:39,800 --> 00:15:45,960
clinging to it, it's not worth getting caught up in, it's just the cause of stress, happiness

98
00:15:45,960 --> 00:15:53,240
can't come from the objects who experience, and the third is that everything inside of ourselves

99
00:15:53,240 --> 00:16:05,160
and in the world around us is non-self, non-self, without everything, our ordinary way of

100
00:16:05,160 --> 00:16:09,600
looking at things is that there's people and places and things, and when you talk about

101
00:16:09,600 --> 00:16:19,840
a person, a person has a self, a personal person in experience, that the reality of that

102
00:16:19,840 --> 00:16:27,760
person is experiences of seeing and hearing and conceptualizing, all of which are impermanent

103
00:16:27,760 --> 00:16:38,440
or momentary, we hold on to things as self being, it's me, it's mine, we hold on to people

104
00:16:38,440 --> 00:16:45,160
it's a, this is you, and when my concept of you, why wouldn't people change do we suffer

105
00:16:45,160 --> 00:16:46,640
itself?

106
00:16:46,640 --> 00:16:57,360
You used to be this, why can't you be more this?

107
00:16:57,360 --> 00:17:04,160
Because things are impermanent, we can't control them, people we can't control, the reality

108
00:17:04,160 --> 00:17:13,400
is there is no self, there is no self involved in a person or place or a thing, an

109
00:17:13,400 --> 00:17:21,240
experience, what we have is momentary experiences, doesn't mean there's anything, when

110
00:17:21,240 --> 00:17:26,960
you say that it's not like, is anything lacking, the person is still there, but in that

111
00:17:26,960 --> 00:17:36,640
person it's not quite how we see it, a person is just a whole bunch of experiences that

112
00:17:36,640 --> 00:17:41,960
are creating new experiences, or a whole bunch of phenomena that are creating new phenomena,

113
00:17:41,960 --> 00:17:52,160
and there's nothing, there's no entity in there, there's no core, no soul, that belies

114
00:17:52,160 --> 00:17:57,240
the reality, what you see when you practice of everything changing, even our own minds

115
00:17:57,240 --> 00:18:04,640
changing, and I don't really have a soul or a self in that sense, who I am is constantly

116
00:18:04,640 --> 00:18:10,560
changing, you can see that about yourself, but you see that about everything, that there

117
00:18:10,560 --> 00:18:22,200
is no core and no control over these things, so that's what you pass the know, you can

118
00:18:22,200 --> 00:18:26,160
think in general it's just you're going to see more clearly and that will lead you to

119
00:18:26,160 --> 00:18:33,680
cling less, suffer less, so that's the first word, the other words are all related,

120
00:18:33,680 --> 00:18:40,760
the sati patana, sati is a word that we should be familiar with if we're not, we should

121
00:18:40,760 --> 00:18:46,080
make ourselves familiar, sati is the word that we translate as mindfulness, sati means

122
00:18:46,080 --> 00:18:51,800
to remember, that's the best way to translate and describe what it means, it means

123
00:18:51,800 --> 00:19:03,640
the capacity, the ability to remember, so we're cultivating this, this ability to remember,

124
00:19:03,640 --> 00:19:15,600
satana is just a word that means a locus or a means, or a base on which to develop sati,

125
00:19:15,600 --> 00:19:20,520
and that's what these other four words are, gai, oi, denat, jeet dandam, if you don't remember

126
00:19:20,520 --> 00:19:27,280
the first two words, just remember the four, the later words, you can remember them

127
00:19:27,280 --> 00:19:34,360
in English as well, but it's important to stress that these four are sort of the framework

128
00:19:34,360 --> 00:19:41,720
with which we're working in our practice, so sati means to remember, it means to remember

129
00:19:41,720 --> 00:19:47,680
not the past or the future, it means to remember the present, and not just the present,

130
00:19:47,680 --> 00:19:55,280
but to remember the experiential present, so when you're in pain, to remember the

131
00:19:55,280 --> 00:20:07,240
pain, to remember and to stay with and to be with the pain, to remember that it's just

132
00:20:07,240 --> 00:20:14,400
pain, when you see something, to remember that it's just seeing, when you think something,

133
00:20:14,400 --> 00:20:22,200
to remember that it's just thinking, this is the language that the Buddha used, ditay, ditamat

134
00:20:22,200 --> 00:20:29,200
and bhisati, let's see, just be seeing, just be seeing.

135
00:20:29,200 --> 00:20:36,400
We try to change the way we look at our experiences, and this is how we cultivate the

136
00:20:36,400 --> 00:20:44,600
bus on the ordinary way of relating to experiences reacting to them, creating thoughts

137
00:20:44,600 --> 00:20:54,400
that are sort of pre-pregidists at the point, so when we have pain, we're prejudiced

138
00:20:54,400 --> 00:20:59,160
against it, which means we have a habit of reacting in certain ways of seeing it in a certain

139
00:20:59,160 --> 00:21:07,400
way of a narrative about the pain, things we already know about the pain, we know that

140
00:21:07,400 --> 00:21:15,840
it's bad, it's a problem, it's a danger even, thoughts, we know that thoughts are bad or

141
00:21:15,840 --> 00:21:21,480
a danger or a problem, but we know these things and it's not knowledge, it's based on

142
00:21:21,480 --> 00:21:29,520
our ignorance, and we try to change that, we try to show ourselves that pain is just

143
00:21:29,520 --> 00:21:35,520
pain, thoughts are just thoughts and so on, try to cultivate this, remembrance of things

144
00:21:35,520 --> 00:21:40,680
just as they are, and so the way we do that, of course, is saying to ourselves, pain,

145
00:21:40,680 --> 00:21:46,920
pain, that's cultivating sati, when you say to yourself, pain, pain, you're bringing your

146
00:21:46,920 --> 00:21:54,920
mind, you're reminding yourself, helping yourself remember the experience as pain, it's

147
00:21:54,920 --> 00:22:03,920
just pain, it's not bad, it's not good, it's not me, it's not mine, it's not a problem.

148
00:22:03,920 --> 00:22:12,920
And you start to see it, actually, not worth getting upset about, there's no benefit that

149
00:22:12,920 --> 00:22:24,880
comes from that, so that's sati, and we have the four sati patan and the four bases

150
00:22:24,880 --> 00:22:32,000
of developing sati, which are kai, and we don't know chitatana, so kai is the body, we start

151
00:22:32,000 --> 00:22:39,200
by focusing on the body, this is why we use the stomach as a basic object, and the stomach

152
00:22:39,200 --> 00:22:45,040
rises, saying to yourself, rising, the stomach falls, falling, there's nothing special

153
00:22:45,040 --> 00:22:54,480
about it, the body, in fact, it's the best object to start with and to take as a base,

154
00:22:54,480 --> 00:23:04,400
because it's always there, it's much less situational than the rest of the three, you're

155
00:23:04,400 --> 00:23:08,160
always going to have the body, you can even just say, if you can't find the breath, you

156
00:23:08,160 --> 00:23:16,120
can just say, sitti, sitti, and you'll find that that brings you back, reminds you how

157
00:23:16,120 --> 00:23:27,800
to remember the present moment, you can try that now, close your eyes, I say try it now

158
00:23:27,800 --> 00:23:31,840
because you're sitting here listening to me as one thing, but you're just listening

159
00:23:31,840 --> 00:23:38,040
intellectually, then what's going on in your mind, you're judging it, liking, disliking,

160
00:23:38,040 --> 00:23:46,080
so there's a disconnect, so to connect it, let's take minute, take listening to the dumb

161
00:23:46,080 --> 00:23:58,960
eyes, the meditation practice, if you want, you can put your hand on your stomach in the

162
00:23:58,960 --> 00:24:05,440
beginning, if you've never done this, otherwise just focus on the rising, falling, and

163
00:24:05,440 --> 00:24:15,840
say rising, falling, the other one, of course, is the walking meditation, when you

164
00:24:15,840 --> 00:24:26,160
walk and say, step being right, step being left, it's important when you walk to separate

165
00:24:26,160 --> 00:24:35,240
the movements, the movement begins, you start saying step being and then right when you

166
00:24:35,240 --> 00:24:41,800
put the foot down, and then you're not doing both steps at once in a flow or anything

167
00:24:41,800 --> 00:24:55,920
right, we want to be with the experience, and that means one experience at a time, body's

168
00:24:55,920 --> 00:25:03,680
a great way to learn how to see clearly, it's a great object, but it's something we're

169
00:25:03,680 --> 00:25:17,720
probably not immediately going to react to, so it's a relatively simple and easy to

170
00:25:17,720 --> 00:25:34,800
begin with, to begin to learn how to be mindful.

171
00:25:34,800 --> 00:25:44,640
The second is weighed and now, so weighed and now is translated as feelings, but it's

172
00:25:44,640 --> 00:25:56,960
just referring to specific category or type of feeling, happy, unhappy and neutral, everything

173
00:25:56,960 --> 00:26:07,120
we experience, we have one of these, mostly neutral, but sometimes there will be pain

174
00:26:07,120 --> 00:26:14,000
or displeasure, sometimes there will be pleasure, sometimes there's a specific neutral feeling

175
00:26:14,000 --> 00:26:22,400
of calm, when these we should all note as well, so as we're focusing on the body, we

176
00:26:22,400 --> 00:26:27,640
pay attention to other experiences when they come up, if you feel pain, forget about

177
00:26:27,640 --> 00:26:36,480
the body, focus on the feelings, pain, pain, pain, this helps you see the pain as pain,

178
00:26:36,480 --> 00:26:42,960
helps you see it more clearly, helps you understand your reactions to the pain and

179
00:26:42,960 --> 00:26:52,480
to change your reactions, many of which are just based on ignorance and poor understanding,

180
00:26:52,480 --> 00:27:00,040
poor appreciation of the reality and the situation, pain isn't a problem and understanding

181
00:27:00,040 --> 00:27:15,040
that is a very powerful thing, when you feel happy, saying to yourself, happy, happy,

182
00:27:15,040 --> 00:27:16,680
when you feel calm, saying calm, just stay with it until it goes away once it's gone,

183
00:27:16,680 --> 00:27:27,560
come back again to the body, the third one is jitta, jitta means the mind, so there are

184
00:27:27,560 --> 00:27:38,960
many mind states, but most importantly is the thinking, so in this category we focus mostly

185
00:27:38,960 --> 00:27:47,440
on the fact that you're thinking the idea of the state of thinking, you start thinking

186
00:27:47,440 --> 00:27:55,360
about your experiences, you have pain and then you think about it, you have a memory

187
00:27:55,360 --> 00:28:04,120
and then you think about it, even just sounds you might start thinking about them, some

188
00:28:04,120 --> 00:28:14,440
idea, start thinking about it, not trying to stop thinking, not say prejudice that people

189
00:28:14,440 --> 00:28:20,480
come to the practice, but thinking is part of the practice, pain is part of the practice,

190
00:28:20,480 --> 00:28:29,640
happiness is part of the practice, they're not good or bad or extra, so when you think

191
00:28:29,640 --> 00:28:35,440
you're also taking it as an object, whenever you remember and realize that you're thinking,

192
00:28:35,440 --> 00:28:42,240
say to yourself thinking, thinking, helps remind you, not get rid of the thought exactly,

193
00:28:42,240 --> 00:28:50,960
but to remind you that it's just thinking, past, future, and good, bad, doesn't matter.

194
00:28:50,960 --> 00:29:02,280
I hope you see impermanence, hope to see the nature of nature of reality, thoughts aren't

195
00:29:02,280 --> 00:29:11,320
also a problem or a good or bad about them, it's our reactions to them to create this

196
00:29:11,320 --> 00:29:17,320
idea of good or bad.

197
00:29:17,320 --> 00:29:26,320
And finally, dhamma, dhamma is the hardest of these six words to translate, but it's

198
00:29:26,320 --> 00:29:32,920
easy to get a sense of it, it's not an esoteric word exactly, it's the teaching of the

199
00:29:32,920 --> 00:29:42,520
Buddha or the things the Buddha learned, so kind of reality and truth.

200
00:29:42,520 --> 00:29:52,440
But the point is, I think, that when we practice, when we try to be mindful, we're

201
00:29:52,440 --> 00:30:10,320
engaging in a path, we're engaging in a practice to progress and to learn and to cultivate,

202
00:30:10,320 --> 00:30:18,320
an understanding about certain things and to do away with, reject an abandoned certain

203
00:30:18,320 --> 00:30:23,560
things, so the first group is those that were going to abandon, under dhammas there are

204
00:30:23,560 --> 00:30:28,880
several groups.

205
00:30:28,880 --> 00:30:36,000
The first one is the hindrances, these are the ones to abandon, so as we're practicing

206
00:30:36,000 --> 00:30:41,120
mindfulness, they're going to be, there's more that's going to arise, we're going to react

207
00:30:41,120 --> 00:30:50,840
to, to our experiences, and this causes stress, this causes trouble, certainly disturbs

208
00:30:50,840 --> 00:30:58,640
our practice, it gets in the way of us seeing clearly, so these five, we often have people

209
00:30:58,640 --> 00:31:06,960
memorize these as well, you can just remember them, simple words to refer to them liking,

210
00:31:06,960 --> 00:31:17,320
this liking, drowsiness, distraction, doubt, and you can think of some of these as just

211
00:31:17,320 --> 00:31:24,240
headings, like under disliking, there's many, but with liking, there's liking and one thing.

212
00:31:24,240 --> 00:31:33,280
And we call these hindrances, so they are things we want to do away with, but more practically

213
00:31:33,280 --> 00:31:41,080
they're things that as you become more mindful, you, you, you abandon naturally, so we're

214
00:31:41,080 --> 00:31:44,120
not going to judge them either.

215
00:31:44,120 --> 00:31:49,240
When you like something, you say liking, liking, it's important to have an attitude of not

216
00:31:49,240 --> 00:31:56,880
trying to get rid of the experience, just say they're self-liking, liking, and dislike

217
00:31:56,880 --> 00:32:09,400
something, say, disliking, disliking, or frustrated, angry, worry, frustrated, angry,

218
00:32:09,400 --> 00:32:18,400
angry, bored, sad, depressed, those all come under disliking, but find a word, you can say

219
00:32:18,400 --> 00:32:26,600
to yourself, bored, bored, think, bored is a good one, sounds kind of funny to say it, thinking,

220
00:32:26,600 --> 00:32:34,960
wouldn't I get more bored if I did that, so it's important when you use this technique

221
00:32:34,960 --> 00:32:46,640
mantra that you're objective about it, you're not adding some venom to it, angry, angry

222
00:32:46,640 --> 00:32:51,880
if you're saying it angrily, it's probably not going to help, but if you're sincerely trying

223
00:32:51,880 --> 00:32:57,080
to appreciate the fact that you're angry, you find the anger goes away quite quickly because

224
00:32:57,080 --> 00:33:06,320
that's a very different attitude from actually being angry, and so it evokes different

225
00:33:06,320 --> 00:33:21,320
qualities, not angry qualities, but mindful and wise, clear states of mind.

226
00:33:21,320 --> 00:33:27,800
If you're tired or drowsy, say to yourself, tired or drowsy, drowsy, if you're distracted

227
00:33:27,800 --> 00:33:35,360
or worried, say distracted or distracted or worried, or restless, restless, distracted

228
00:33:35,360 --> 00:33:39,520
is a good one if you're thinking a lot, you don't know, just say thinking, thinking

229
00:33:39,520 --> 00:33:47,280
all the time, if it's a lot, say to yourself, distracted and distracted, and doubt or confusion

230
00:33:47,280 --> 00:33:54,800
if you have doubt about yourself or practice, doubt about me or anything, just say doubting,

231
00:33:54,800 --> 00:34:04,600
doubting, if you're confused, say confused, confused, with all these stay with them until

232
00:34:04,600 --> 00:34:17,040
they go away and then when they're gone, just come back again to the body, and that's

233
00:34:17,040 --> 00:34:18,040
the force that you put on there.

234
00:34:18,040 --> 00:34:23,600
The other thing about the dhamma that I mentioned is the senses, but we go into those

235
00:34:23,600 --> 00:34:28,360
in detail as you progress in the practice.

236
00:34:28,360 --> 00:34:34,120
So just remember when you start, if you're just starting out, the important one is seeing,

237
00:34:34,120 --> 00:34:40,320
if you have your eyes closed and you see lights or colors, pictures, just say to yourself,

238
00:34:40,320 --> 00:34:48,640
seeing, don't get attached to it, try not to get excited and think, oh, this is a good

239
00:34:48,640 --> 00:34:56,160
sign or maybe this is a bad sign or get afraid of it or something, it's just seeing.

240
00:34:56,160 --> 00:35:05,600
It happens in meditation that you start hallucinating kind of in a way, so for some people,

241
00:35:05,600 --> 00:35:11,080
so don't sit there waiting for it to happen, but if you do see, just say seeing and let

242
00:35:11,080 --> 00:35:12,080
it go.

243
00:35:12,080 --> 00:35:14,040
It's just an experience.

244
00:35:14,040 --> 00:35:21,400
Okay, so those are the basics of the practice, vipasana, satipatana, kaya, vita, najita,

245
00:35:21,400 --> 00:35:22,400
dhamma.

246
00:35:22,400 --> 00:35:29,240
If you guys have questions now, we can talk a little bit more, just make sure everything's

247
00:35:29,240 --> 00:35:35,520
okay in the first day, but that's the dhamma for tonight, and thank you internet for

248
00:35:35,520 --> 00:35:42,800
listening, watching, and tuning in and practicing as well, I'll try to get back into answering

249
00:35:42,800 --> 00:36:12,640
questions as well from the internet, now that I'm back, have a good night.

