1
00:00:00,000 --> 00:00:07,600
why everyone should meditate. Five reasons. The practice of meditation allows us to live our lives

2
00:00:07,600 --> 00:00:14,720
free from suffering. It allows us to give up grasping and clinging so that everywhere and anywhere

3
00:00:14,720 --> 00:00:21,520
we can be truly happy at peace with ourselves and the world around us. This is because meditation

4
00:00:21,520 --> 00:00:31,360
benefits us in five ways as follows. One, meditation purifies the mind. What does it mean to have a

5
00:00:31,360 --> 00:00:39,680
pure mind? A pure mind involves mental clarity. Our minds give rise to many different conflicting

6
00:00:39,680 --> 00:00:46,640
mind states. Some mind states bring happiness and some bring suffering. There are three general

7
00:00:46,640 --> 00:00:53,920
characteristics of mind states that bring suffering. Positively inclined states, greed, wanting, etc.

8
00:00:54,800 --> 00:01:03,440
Negatively inclined states, anger, disliking, hatred, etc. and neutral states. Association without

9
00:01:03,440 --> 00:01:10,320
judgment. These states are considered to be mental impurities because they cause suffering.

10
00:01:10,320 --> 00:01:17,040
They cause us to perform harmful deeds towards others and ourselves. Positively inclined states

11
00:01:17,040 --> 00:01:23,920
lead to addiction. Negatively inclined states lead to immediate mental suffering. Neutral states

12
00:01:23,920 --> 00:01:31,600
involve ideas of possession, meat and mind, concepts, views and conceit. Ideas which lead us to

13
00:01:31,600 --> 00:01:39,040
hold ourselves higher than others or put ourselves down. Meditation helps to overcome

14
00:01:39,040 --> 00:01:46,800
unwholesome mind states that arise. When we see something, we may like it, positive or dislike it,

15
00:01:46,800 --> 00:01:53,520
negative or create some kind of idea of self-righteousness or conceit related to it, neutral.

16
00:01:55,440 --> 00:02:01,600
Each moment we practice meditation, we create a clear state of awareness instead of reacting

17
00:02:01,600 --> 00:02:07,520
to the experience. When we see something, we simply remind ourselves that it is seeing

18
00:02:07,520 --> 00:02:15,120
that this is an experience of seeing. When we feel pain, we remind ourselves that it is pain

19
00:02:15,760 --> 00:02:23,840
and simply create the clear awareness of it as pain. When we do this, our minds are diverted

20
00:02:23,840 --> 00:02:30,400
from the habitual next step of reacting to the experience as they are fixed on the ultimate reality

21
00:02:30,400 --> 00:02:38,800
of the experience as it is. Neither good nor bad, not me or mine, etc. It simply is.

22
00:02:40,400 --> 00:02:44,560
This is what it means to purify the mind by creating clear awareness.

23
00:02:45,440 --> 00:02:49,840
The purity that comes from clear awareness is the first benefit of meditation.

24
00:02:50,480 --> 00:02:56,960
It helps us to get rid of anger, addiction and all sorts of conceit and allows us to move away

25
00:02:56,960 --> 00:03:00,240
from the unwholesome mind states that cause suffering.

26
00:03:01,520 --> 00:03:08,880
Number two, meditation allows us to overcome mental illness. Mental illness refers not

27
00:03:08,880 --> 00:03:15,520
to the states that calls one to enter a psychiatric ward. Mental illness refers to any sort

28
00:03:15,520 --> 00:03:22,320
of mental distress or disease. In this case, we understand diseases as quote unquote disease.

29
00:03:22,320 --> 00:03:32,400
Not being at ease, states like depression, stress, anxiety. All these are mental illness that

30
00:03:32,400 --> 00:03:38,720
can cause such creates suffering as to lead those who suffer from them to seek solutions.

31
00:03:39,520 --> 00:03:45,760
Like pills, therapy and other external means, temporarily removing them from our minds.

32
00:03:45,760 --> 00:03:54,320
Unfortunately, these solutions generally felt sort of a full cure as they do not engage directly

33
00:03:54,320 --> 00:04:01,040
with the root cause of mental illness. The mind, meditation practice is the most successful means

34
00:04:01,040 --> 00:04:07,600
of overcoming mental illness because it does engage directly with the mind. We do not ever have to

35
00:04:07,600 --> 00:04:14,240
take medication for something which is purely mental. We can eradicate on our own states of mind

36
00:04:14,240 --> 00:04:22,640
like depression, stress, anxiety, insomnia, worry, fear without any need to see our clinical

37
00:04:22,640 --> 00:04:28,880
therapist or doctor. Without training in meditation, one may never realize that we have

38
00:04:28,880 --> 00:04:34,800
such an incredible tool that allows us to build ourselves of harmful mind states on a real and

39
00:04:34,800 --> 00:04:41,200
profound level. When we practice meditation, we are creating clear remembrance of the present

40
00:04:41,200 --> 00:04:48,240
reality at every moment. We give up minds states that foster a new way of looking at the world.

41
00:04:48,240 --> 00:04:56,640
Instead of perceiving our experiences as problems, stressful, depressing, fearful, etc. We simply

42
00:04:56,640 --> 00:05:02,720
see them as they are. Before our mind is able to just the experience we say to ourselves,

43
00:05:02,720 --> 00:05:09,680
quote unquote, seeing, quote unquote, hearing, hearing, quote unquote, thinking, thinking,

44
00:05:09,680 --> 00:05:16,720
etc. And there is no opportunity for reactionary states like stress, depression or fear to arise.

45
00:05:17,920 --> 00:05:23,840
We can even note quote unquote, stressed, stressed, quote unquote, depressed, depressed,

46
00:05:23,840 --> 00:05:31,040
or quote unquote, afraid, afraid. And the chain reaction that causes those states of overwhelmed

47
00:05:31,040 --> 00:05:38,960
us is cut off causing them to fall away naturally by reminding ourselves in this way that

48
00:05:38,960 --> 00:05:44,400
experience are just what they are. Unhealthy minds states have no opportunity to become

49
00:05:44,400 --> 00:05:50,880
a habitual response to a particular experience. Instead, we develop a new habit of seeing things

50
00:05:50,880 --> 00:05:57,040
simply as they are. Because of the change in perspective that comes from such practice

51
00:05:57,040 --> 00:06:02,160
over the long term, bad habits are abandoned and mental illness is overcome directly.

52
00:06:02,160 --> 00:06:09,600
In this way, my teachers successfully overcome issues like insomnia, depression,

53
00:06:09,600 --> 00:06:15,440
societal ideation, alcoholism and all sorts of addiction and harmful minds states.

54
00:06:16,400 --> 00:06:21,120
Often though simple direction and encouragement for our qualified mentorship teacher,

55
00:06:21,680 --> 00:06:27,440
everyone experience mental disease of various forms to some degree. For this reason,

56
00:06:27,440 --> 00:06:33,200
everyone should practice meditation. Three, meditation does away with suffering. There are two kinds

57
00:06:33,200 --> 00:06:38,880
of suffering that exists. The first kind of suffering is bodily suffering. Suffering caused by

58
00:06:38,880 --> 00:06:46,560
pain in our bed, our legs, our head, stomach and various other parts of the body. The other kind

59
00:06:46,560 --> 00:06:52,160
of suffering is mental suffering. This kind of suffering is the distress that comes from mental

60
00:06:52,160 --> 00:06:58,080
illness as it is discussed above. When we feel anguish in the mind, it can be much worse

61
00:06:58,080 --> 00:07:03,680
than physical pain. Meditation is able to do away with both of these kinds of suffering.

62
00:07:04,640 --> 00:07:10,320
It allows us to understand that physical pain is just a part of our existence. The physical

63
00:07:10,320 --> 00:07:16,080
discomfort is actually not an uncomfortable thing and that mental discomfort is caused by

64
00:07:16,080 --> 00:07:23,360
our unnecessary judgment of experience like pain and discomfort. Physical pain is simply a phenomenon

65
00:07:23,360 --> 00:07:29,120
which arises and ceases. There is nothing intrinsic to pain that tells us that it is bad.

66
00:07:29,120 --> 00:07:34,720
It is simply the pain telling us that there is pain. The mind itself means the judgment of

67
00:07:34,720 --> 00:07:41,040
our experience is good or bad. The mind can be trained to interpret phenomena in any number of

68
00:07:41,040 --> 00:07:48,240
face. For example, it is possible to perceive pain as good thing, a pleasurable experience even.

69
00:07:48,240 --> 00:07:54,240
In meditation, we will learn to see pain as simply pain. When the experience pain, we see it

70
00:07:54,240 --> 00:08:00,960
to ourselves, who doesn't put pain, pain, reminding ourselves that it is just pain rather than

71
00:08:00,960 --> 00:08:06,800
allowing it to produce stress and something. We do not have to experience pain as negative,

72
00:08:06,800 --> 00:08:13,520
something to get rid of or do away with. The reason we are unable to experience pain as it is

73
00:08:13,520 --> 00:08:19,680
without reacting to it is because we normally lack the right tool. Meditation is the tool.

74
00:08:19,680 --> 00:08:24,400
It allows us to come to the realization of seeing things as they actually are.

75
00:08:25,360 --> 00:08:31,440
When we see the objects of experience clearly as they are, we are able to fear ourselves from suffering.

76
00:08:31,440 --> 00:08:39,680
Less obvious than suffering from pain, perhaps more pervasive, is suffering as a result of thinking.

77
00:08:40,480 --> 00:08:46,080
When we think about something, recall memories or worry about the future, we will feel

78
00:08:46,080 --> 00:08:54,560
sadness, anger, excitement, anticipation, etc. Our judgment complicated, all of our many experiences

79
00:08:54,560 --> 00:09:00,080
that are actually completely neutral. When we say to ourselves in our mind,

80
00:09:00,080 --> 00:09:06,560
quote unquote, thinking, thinking. We define ourselves that thought is just thought. We see

81
00:09:06,560 --> 00:09:13,600
thinking simply as it is. When we say to ourselves, quote unquote, worrying, worrying. We see

82
00:09:13,600 --> 00:09:19,360
that there is nothing actually worth worrying over. We see that the experience is not under our

83
00:09:19,360 --> 00:09:27,120
control. We cannot change it or make it accord with our wishes. If our mind is clear, it's

84
00:09:27,120 --> 00:09:33,600
simply arises and false or without leading to stress or suffering. Many physical ailments arise

85
00:09:33,600 --> 00:09:39,600
from stress and the build of attention within the body. Meditation can permanently leave this

86
00:09:39,600 --> 00:09:44,880
tension. Though it may take time for the night to let go of the stress, in the end,

87
00:09:44,880 --> 00:09:50,720
pain caused by the stress can be cured completely. Changing our perspective to see the pain

88
00:09:50,720 --> 00:09:56,800
as just pain is part of the letting go that allows for healing. It is common for those

89
00:09:56,800 --> 00:10:02,320
who have suffered stress-based illnesses with which doctors could not have them come to realize

90
00:10:02,320 --> 00:10:08,400
that illness is based on stress. And when they do away with the stress, they do away with the illness.

91
00:10:10,080 --> 00:10:16,800
Meditation allows us to free ourselves of physical and mental. Some may say that suffering is

92
00:10:16,800 --> 00:10:22,160
actually an important part of life and that without suffering, we would not learn anything

93
00:10:22,160 --> 00:10:27,920
or that life would be less interesting. It is absolutely true that something does offer us

94
00:10:27,920 --> 00:10:33,120
opportunities to learn. But we really take the time to truly understand anything from it.

95
00:10:33,760 --> 00:10:39,840
Meditation is the tool that allows us to truly learn everything. There is to know about suffering.

96
00:10:40,560 --> 00:10:46,160
Once we have learned all there is to know about suffering, there will be no reason for us to

97
00:10:46,160 --> 00:10:54,400
suffer as more. For, meditation helps you follow the right path. There are many different paths

98
00:10:54,400 --> 00:11:01,280
in life. You can practice a path which leads to suffering. You can practice one that is

99
00:11:01,280 --> 00:11:09,040
charitable and moral. There are many meditative lifestyles. Yoga, Tai Chi, various traditions in

100
00:11:09,040 --> 00:11:14,640
Hindu or even in the Western religions that teach you to fix and focus with mind on a single

101
00:11:14,640 --> 00:11:21,680
object leading to great state of peace and tranquility. These meditative or religious practices

102
00:11:21,680 --> 00:11:27,360
are not what we considered to be the right path. It is not that any of these meditative paths are

103
00:11:27,360 --> 00:11:34,080
wrong. It is just that these paths are not what is meant by the right path. Just as meaning of right

104
00:11:34,080 --> 00:11:41,120
path is not the path of becoming a monk, but this is a lawyer, a doctor, etc. All of these paths

105
00:11:41,120 --> 00:11:46,320
exist and may be good or bad. They simply are not the right path according to the Buddha.

106
00:11:47,600 --> 00:11:53,760
The right path of Buddha relates to the quality of one's mind. It involves perfect clarity and

107
00:11:53,760 --> 00:12:00,160
understanding of reality as it really is. It is called the path because it leads to the

108
00:12:00,160 --> 00:12:05,760
goal of freedom from suffering. Mindfulness might teach the needs to freedom from suffering because

109
00:12:05,760 --> 00:12:11,360
it puts us on the right path by helping us to see clearly purifying our minds from the

110
00:12:11,360 --> 00:12:17,520
departments that cloud and distort our perception whether we choose the path of a lawyer or doctor,

111
00:12:18,160 --> 00:12:24,240
Christian or Buddhist, whatever path we choose. Once we create this clarity of mind and understand

112
00:12:24,240 --> 00:12:30,800
things as they are, we do not give rise to greed and attachment which would color our perception.

113
00:12:30,800 --> 00:12:36,480
We do not give rise to anger which would inflame our mind. We do not give rise to delusion

114
00:12:36,480 --> 00:12:42,800
and conceit and views which would model our mind and confuse us. We simply understand things

115
00:12:42,800 --> 00:12:48,400
as they are, as they arise. Slowly but surely through mindfulness, meditation, practice,

116
00:12:48,400 --> 00:12:54,400
you will find yourself better able to help yourself and others. You will be able to work,

117
00:12:54,400 --> 00:13:00,720
study and live more efficiently than ever before. Simply by understanding reality more clearly,

118
00:13:00,720 --> 00:13:08,320
than before. That is what wisdom means to understand things as they are. Not as we want them to be

119
00:13:08,320 --> 00:13:14,080
or want them not to be. Not understanding things in terms of our mental requirements or impurities.

120
00:13:14,960 --> 00:13:19,680
Once we come to clearly see things as they are, we have found the right path.

121
00:13:20,320 --> 00:13:25,120
The right path involves knowing the difference between the path which leads to our development

122
00:13:25,120 --> 00:13:30,320
and the path which leads to our destruction. Being on the right path does not mean that we should

123
00:13:30,320 --> 00:13:36,320
leave our lives in a certain way but that when we do live our lives, we see our way to make the right

124
00:13:36,320 --> 00:13:44,160
decisions. Five, meditation makes you free. We practice meditation to gain freedom. We call this

125
00:13:44,160 --> 00:13:50,320
Nirvana which is the highest sort of freedom. Freedom not to suffer, freedom from development and

126
00:13:50,320 --> 00:13:57,600
mental impurity. Freedom from addiction, hatred, bigotry, freedom from wrong views, conceit

127
00:13:57,600 --> 00:14:05,600
and all sorts of unpleasant, unpleasant, unskillful and useless states of mind. Total freedom.

128
00:14:06,720 --> 00:14:12,480
It is not freedom to do anything. Of course, you are always free to do what you want. But

129
00:14:12,480 --> 00:14:18,400
true freedom can severely limit the things that you decide to do because a lot of things we decide

130
00:14:18,400 --> 00:14:24,800
to do are actually based on delusion or ignorance and lead to suffering. Freedom according to

131
00:14:24,800 --> 00:14:32,000
Buddhism is not freedom to, it is freedom from. Real and true freedom is like a bird in flight,

132
00:14:32,000 --> 00:14:39,680
not clinging to anything, taking its two wings as its only possessions. Wherever we go and whatever

133
00:14:39,680 --> 00:14:48,400
situation that we are in, mindfulness allows us not to attach to anything. Not people, places or things.

134
00:14:48,400 --> 00:14:55,600
Meditation helps us live our lives. Whatever our situation and not be caught up with,

135
00:14:55,600 --> 00:15:01,280
quote unquote, should be, quote unquote, should not be, quote unquote, wish it were,

136
00:15:02,000 --> 00:15:08,080
quote unquote, wish it weren't for any kind of judgment whatsoever. A mindful person lives

137
00:15:08,080 --> 00:15:13,520
like a bird. Wherever they go, they eat the fruit from the tree upon which they learn.

138
00:15:13,520 --> 00:15:19,680
The practice of meditation is something that you have to practice by yourself. It is not

139
00:15:19,680 --> 00:15:25,280
something that you can just take anyone's word for. With a little bit of time and a little bit

140
00:15:25,280 --> 00:15:55,120
of patience, you yourself will come to understand the benefits of meditation.

