O T, everyone welcome to our Saturday broadcast, so as usual we'll start with a little
bit of meditation together, fallen by a Q&A session, so if anyone has any
questions already or welcome to post questions in the chat at any time, please
don't answer each other's questions, that's not what this is for, and it is that
people are posting questions, they're looking for an answer for me, so if you
don't have a question you can just close your eyes, and we'll spend about the
first 15-20 minutes practicing meditation together, so we start by focusing on
the stomach, there's nothing special about the stomach, but it's preferred
because it's coarse and it's obvious and it's very very physical, and the
benefit of focusing on the physical is that it's coarse and obvious, but also
that it's very real, and real being very real is important because it's out of
our control, it's an object that will teach us about reality, will teach us some
important lessons about our relationship with reality that aren't evident in
ordinary daily life where we are in a constant struggle to seek out and gain
what we want and to chase away what we don't want, we miss and overlook some
important truths about reality, mainly that reality is unpredictable, so we're
constantly seeking out that which is stable, predictable, dependable, we want to
depend on things and find our happiness and our peace in things, because things
are not like that we suffer, we feel stressed and constantly in a battle to try
to find and attain the unattainable stability, that it will be satisfying and that
we can control, maintain and manipulate and fix all of our problems, so we're
constantly looking to change the world around us, change our experience, and
by looking at just the simple reality of the stomach rising and falling we'll
see that that's not really a tenable, a tenable proposition or a tenable
perspective, let me see that it's misland, misguided, so just start
observing the stomach rising and falling, you can't feel it, it's maybe
unfamiliar if you haven't practiced it before, just you can put your hand on
your stomach just to begin with until you're able to feel it, you're not trying
to control it, I'm trying to condition it, deepen it or smooth it out, it's
unpredictable,
and when the stomach rises, just say to yourself,
it's rising and when it falls falling, not out loud just in your mind, the word is
like a mantra, it's used to focus your attention on the object, don't just repeat
it mindlessly in your head, use it as a means to remind yourself, when you
experience the rising, that you perceive it just as rising, not as a problem to be
fixed or something to be controlled or conditioned, not as me or mine, good or
bad, to teach yourself how to just see something as it is without judgment.
you
just watching this
stomach rising and falling, this becomes a good base to become the
aware of all of the other aspects of your experience, so while your mind is
focused on the rising and falling you might become aware of other aspects of
your experience, feelings for example, pain, pleasure, calm and just try and
be aware of those as well when you feel pain, just say to yourself pain, pain or
aching, making and if you feel happy, say happy, happy and just repeat it to
yourself until the feeling goes away, once it's gone just go back to the
rising, fall in the stomach and continue seeing yourself rising, falling, if you feel
calm, sit yourself calm, calm, calm.
Another aspect of experience is the thought process is in the mind thinking
about the past or future, good thoughts, bad thoughts, many different kinds of
thought, but in the end, whatever you're thinking, the thinking part is just a
thought, so just say to yourself thinking, thinking and generally shouldn't last
once you've noted it, once you've noted it a few times just go back to the
rising, falling, but we're not trying to stop ourselves from thinking and
there's no value judgment on the distraction getting distracted by other
experiences, not to sign that you're practicing poorly, we're not trying to
control our experiences, we're trying to change the way we react to them, so
whatever you do experience, just try and apply mindfulness to see it clearly,
without judgment, so thinking is just thinking, just say to yourself thinking
and thinking, and that's it, nothing more, nothing less
apart from the thoughts, there's also the emotions, the state of mind, many
different kinds of state of mind, for emotions, they generally can be separated
into liking and disliking, liking or wanting, just say to yourself liking, liking or
wanting, wanting, just like something, say to yourself disliking, disliking, or if
you feel sad or angry, frustrated, bored, depressed, afraid, just find a word for
what you're experiencing and remind yourself the nature of it until it goes away
just to keep yourself from reacting to it, disliking, disliking, frustrated,
frustrated, bored, bored, whatever it is, if you feel tired or drowsy, just focus on
that, say to yourself, tired, tired, drowsy, or if you feel restless and anxious and
distracted, any of those things stressed, say to yourself anxious, distracted,
restless, restless, or if you have doubt or confusion doubting yourself, doubting
your practice, say to yourself, doubting, doubting, confused, confused, really any
state of mind, whatever your state of mind is focused or unfocused, judgmental, any
kind of state, just find a word for it, just trying to break the chain so that
liking is just liking, disliking is just disliking and you don't perpetuate it, you
don't feed it, you don't encourage it to grow.
And finally, mindful of the senses seeing, hearing, smelling, tasting, feeling, try
and notice as well, if you hear something with your eyes close to hearing, if you see
something even with your eyes close, you might say, you might see lights or
colors or pictures, just say to yourself, seeing, seeing, smelling is smell a good smell
or bad smell, say smelling, smelling, if you taste something sweet, sour, salty, whatever
tastes, just say tasting, tasting, if you feel something on the body hot, cold, hard
and soft, just say feeling, feeling, and once it's gone or after a while, just go back
to the rising, falling, so we just always come back to the stomach, just because it's
a good base.
All right, so thank you for meditating together with us.
I see there are many questions in the chat, so we'll move right along into the Q&A session.
If you have questions, you can still post them in the chat to being organized, we'll
start from the ones that have already been asked, but from here on, only questions in the
chat, anything that's not a question would just be removed just to keep it tidy.
If you don't have a question, just close your eyes and stay mindful with us.
Do you want to hear the first question?
In the height of stress and worry, is the focus still on the stomach?
But what else?
No, when you have stress and worry, focus on the stress and worry.
It's just a base, there's nothing special or preferable about it, it's a good indicator.
If you're not with the stomach very much at all, then it's a good indicator that your
mind does not very focus, but apart from that, just make sure you're noting whatever
it is that does distract you, and that once you've done that, then you go back to the
stomach, but don't avoid or ignore something that distracts you.
Stress and worry should be at the forefront of, at the top of your list of things to
note, and you can note physical sensations associated with it, which are distinct from the
worry and stress, but note them as well.
Would we practice loving-kindness, meta-meditation?
Is this distinct from noting meditation, Vipassana?
Is Maita love a concept, or is it as real as all other objects of experience?
So Maita doesn't really mean loving love, it means friendship or friendliness, friendliness
is the best, it literally means friendliness or friendship.
So meta-meditation is friendliness, or maybe even kindness, but friendliness is probably
the best way of understanding it.
And friendliness is a quality of mind, meta is a quality of mind, but when you practice
meta-meditation, your focus is not on the kind, the friendliness, it's on the person
who you're being friendly towards, or you're extending friendship towards.
So the object is a person, you think of them, and you apply, or you cultivate friendliness,
and because the object is the person, it's called summit-meditation, it is conceptual.
If you were to ever focus on the friendliness itself, and say to yourself, friendly, friendly,
or loving, loving or kind, focusing on your own quality of mind, then it would be focusing
on a real object, but that's not how you practice meta-meditation, meta you think about
a person, and because you thought your focus is the person, you will never see impermanence
suffering in non-self.
So it's a good meditation, it's helpful, especially for people who have anger issues,
but only on a basic level, it can never cut through delusion and allow you to see clearly,
but can't lead directly to freedom from suffering.
Is it really necessary to use this technique of mental notation?
Is it not possible to cultivate mindfulness without this device?
It's possible, it's just not very effective, it's much easier to cultivate concentration
and forceful concentration without it, because the noting neutralizes your quality's
of mind, it sets the mind in an objective state, it's highly preferable to just letting
your mind focus on things according to its whim, according to its preconceived mind state.
Everything is an artificial means of conditioning the mind to not condition your perception
of the object, so you set your mind in a way that just sees the object as it is, a means
or a method of accomplishing something, so I'm skeptical of anyone who thinks they can
develop substantial mindfulness without it.
It really is a core technique of meditation, mantra meditation is really ancient and perhaps
the most well established and approved of meditation form where you use a word to focus
your attention on an object, and it's very much an ancient Buddhist way.
If you're not using the noting technique, it'll tell me exactly what are you doing to cultivate
mindfulness, so it's not possible to cultivate mindfulness spontaneously without applying
some artifice to do so, it's not just going to spontaneously arise because you didn't
do anything, so for very special individuals who are already on the right track, it might
be quite simple for them to just adjust their mind a little bit and suddenly be mindful
and see things clearly, for most of us it takes a lot of work and reprogramming the habits
of the mind and that's what the mantra technique is used for, the reminding of setting
means reminding or remembering, not mindfulness, so does it do it.
This question comes up I think because of the word mindfulness, it seems like something
that doesn't have anything to do with saying yourself, rising, falling, or thinking
or pain, but reminding yourself certainly does, and that's the actual word, it's not mindfulness,
it's a poor English translation.
Will practicing samata have any effects on vipassana?
It can, I can't say what will have effects for an individual, generally speaking samata
is beneficial for the cultivation of vipassana, it creates a baseline in the mind of focus
and strength and so you can apply that strength and vipassana meditation.
In one feels nervous, it contains tension in the body, a fast heartbeat, rising and falling
of the stomach, should I pick one of those to note, or should I note the feeling nervous
as a whole?
We're feeling nervous isn't the whole, feeling nervous is the mental aspect, so it doesn't
contain those things, right, feeling nervous is just the one part of it, feeling nervous
doesn't contain tension in the body, that would be a mistake to think that, so there will
be the nervousness in the mind, there will be the tension in the body, there will be a feeling
of the heart and the heart, there will be the stomach rising and falling, any of those
is fine, honestly, usually you want to pick the nervousness in the mind, the anxious feeling
worry or whatever it is, it's fleeting and momentary, which is why you think of it as containing
all the rest, because when it's gone, all your left is with you, with is the other things
which can then retrigger the nervousness, so it seems like they're all one big thing, but
they're just triggering each other, and you just know whatever's present, but if you notice
nervousness, you should note that.
So to practice, when facing a non-accidental death, well, I don't really have given advice
on how to live their lives, it's just far too complicated, and not really to the point.
The idea of what you should do in any given situation, how you should live your life or
something is a bit barking up the wrong tree, you don't need to answer such questions.
So I mean, the point they think is this is a very vague and general question, you haven't
asked what sort of meditation you're doing, you're just asking how to practice, you might
be meaning what meditation should I practice, in which case, of course, I can offer advice,
but it doesn't have anything to do with non-accidental death, it has to do with experience,
and so there's nothing specific to practice when faced with trauma or loss or that sort
of thing, and so why I say it's a wrong focus to focus on how to live your life and what
to do and so on is because those questions get answered when your mind is clear, so the
best thing is to focus on clarity of mind, you know, you can give guidelines on how to live
your life, what things you shouldn't do like killing and stealing and lying and cheating,
things you should do like coming kind and generous and helpful and thoughtful and so on.
But ultimately, all of that comes through the practice of mindfulness, so whether you're
talking about how you should live your life or you're asking about how to practice meditation,
I would still recommend mindfulness, I don't know if you've read our booklet on how to
meditate, you might find that helpful.
We also have an at-home meditation course, if you want to sign up for that, you can
go to our website, it's all free, so feel free to look up the resources that we have
that might help you.
Throughout the day, it's it better to note consistently or accurately in an ultimate sense.
So you mean, if I have a choice between noting consistently but inaccurately or accurately
but inconsistently, which is better, I don't think the answer is very hard to understand,
I mean, neither sounds very good, I mean consistency is an important quality of practice
and accuracy is an important quality of practice, I mean it's a bit speculative to ask
which one is better, but I would say accuracy is more important in the sense that it's
dangerous, inaccuracy is more dangerous than inconsistency, potentially, simply because inaccuracy
can lead you to craziness and so on, like I mean if you're not noting things as they
are, if you're noting something else, like it's this but you're noting that that can drive
you crazy, you know, it can do all sorts of crazy things to the mind, I mean it doesn't
mean that you have to be prestigious about accuracy, it just means make sure you're noting
what's actually happening, right, like two things, first if you're hearing and you'd say
to yourself seeing, obviously I don't know that anyone would ever do that, but that would
be crazy because it's hearing, it's not seeing, right, and the other one is note the actual
experience, so if you see a dog, just say to yourself seeing, don't say dog, dog, those are
the two accuracy that are important, consistency is just about quality and the efficacy,
if you're inconsistent it's not going to be very efficacious, I find it hard to be mindful
when thinking, I say to myself thinking, but the thinking disappears when I do so, even
when hearing, I say hearing, but I can't comprehend what the other person is saying, do
you have any advice?
Well, sometimes you might not want to be, you might be focused on gaining information,
so you might not want to put too much effort in noting hearing, but you can still know
it, you can still note hearing and understand what's being said, you just have to be a little
more skillful about it, and you don't have to note all the time, just try and note
when you can, when it's convenient, the fact that you're trying is a great thing to hear,
so keep it up, just do what you can, ultimately these sorts of questions indicate a sort
of the struggle that a meditator goes through to get their feet, find their feet, learning
any skill is going to take some struggling and some wrestling in the beginning, but just
be patient and you don't have high expectations or force yourself to live up to some kind
of expectation or requirement that you might have, just try and be mindful when you
can and develop it as a skill, as a practice, I mean, there's other things there that
the fact that it disappears is seeing impermanence, so that's a good sort of thing to start
to understand that thinking is not a constant state, it's not me thinking, it's something
that arises and sees it, so seeing that is quite helpful actually, and when you can't comprehend
what the other person is saying, it helps you start to see non-self, so that's another
useful thing to see that you're not in control, you can't even understand someone when
you want to understand them, which is kind of silly, right, they're right there, the sound
is right there, why can't I understand it, it helps you start to see non-selfs, these
are useful to help you start to let go, how do I meditate to deal with living alone, same
as how you meditate with anything, there's nothing special about that, I mean, you just
rephrase what you're asking and say meditation helps you deal with living alone, that's
the answer, mind, you know, set deep autonomy, meditation certainly does, so if you haven't
read our booklet, read our booklet, you can try doing our atom meditation course, living
alone goes from being a sad and scary thing to being a sort of pleasant and peaceful
sort of thing, so you're able to be mindful and quiet, is it better to meditate in the
belly or nostrils in the long term, but it's not technically either better, we recommend
the stomach because it's more course and more easily identifiable as a physical sensation,
the nostrils are kind of subtle and so it can become a conceptual object, but you know,
it doesn't have to be, you don't have to pick and choose, so if you feel something at
the nostril, just say to yourself, feeling, feeling, and then go back to the stomach.
How do I meditate with intention to increase my willpower and taking action?
I don't quite know what that means, how do I meditate with intention?
I mean, you wouldn't meditate on the intention if you have an intention say wanting or
intending.
I know those, but if you're asking how do I meditate in order to cultivate the intentions
or increase your willpower or so on, that's not what this is about.
I mean, willpower is sort of just because willpower is related to patience, if you have
strong willpower, we describe someone who's patient and doesn't give in to their desires
or have strong desires or cling or react to things, but it's a bit misleading because
it's not really willpower, it's just strength of mind and purity of mind.
I am wondering if when doing Buddha meditation, we are still to focus on the breath and the
four things to be mindful of due to it, or only focus on the Buddha.
So according to the texts and really reasonably so, you should focus on one object at
a time.
So if your focus is Buddha, it should not have anything to do with the breath or the
force at Deepatana, those people who mix them, I'm critical of that.
I think it's proper to be critical of that.
I know it's a very well-established sort of meditation technique, especially in Southeast
Asia, but it certainly isn't in accords with the ancient tradition.
So I mean, generally meditation, you can't go wrong if you're practicing carefully
and thoughtfully, it's usually helpful for you, but we've talked, I've talked about
this before, like mixing objects has very strange consequences, it seems, and it looks
like people who do that start to get themselves mixed up thinking, and this is again the
accuracy.
If you're watching the breath and you say, Buddha, Buddha, you're in accurately noting
your mantra is inaccurate, and that inaccuracy creates a distortion in the mind, so a conflation
in the mind, and you see that in the writings of such people, they start to see the Buddha
in themselves and the Buddha in their breath and that sort of thing, which is not at all
what those meditations are for, that's not at all the Buddhist way to practice, it's
a very sort of new age-y sort of way to practice, it's not Buddhism, I'd recommend reading
the Wisudimaga, it gives some very detailed descriptions of those sorts of meditation.
If I notice I was distracted, but the distracting object has passed, should I dig back to
examine whatever distracted me?
No you don't go back to the past, what's gone is gone, just know what's in the present,
if you're usually in that case we would know knowing, just knowing that we were distracted
because there's the knowledge or the awareness in the present moment, so note that awareness,
just saying knowing, knowing and move on, it's not magic, it's not like a game where you
have to catch things, like fruit ninja, who you're talking about, fruit ninja this morning,
this game where you cut fruit I think is something like that, you don't have to catch anything,
just try and be present, it's not a game, it's not magic, that sort of thing shows you
imperman, it shows you non-self that you're not in control, you can't simply be mindful
magically, you notice that you miss things and so on.
I want to take on the sixth precept, is it permissible to eat my one meal in the evening
after 6 p.m. instead of in the morning?
It seems appropriate since this is our family dinner, a tradition, first of all there's no,
there's no taking one precept, that's just not the tradition, either you take eight precepts
or you take five precepts, if you decide to only eat in the evening that could be a helpful
practice but don't try and see it as keeping a certain precept, it's important just for
tradition sake that we maintain the purity, it may seem kind of dogmatic, probably seems
quite dogmatic but there's a value to being dogmatic so that we don't just do whatever
we want, follow the out of respect for the tradition, it's an important thing, it's something
that is not very well appreciated in modern times, so out of respect for the tradition
if you're going to keep eight precepts, keep eight precepts, so if you're going to keep
five precepts, keep five precepts, and on top of that if you, whatever practice you think
is helpful, absolutely go for it, it doesn't mean you shouldn't do that sort of thing,
but it's not in, it's not in keeping with the eight precepts to eat in the afternoon, you
eat in the evening, the whole point of eating in the morning is as a means of sustaining
you for the day, if you eat in the evening it's not really all that helpful, it's just
to satiate your hunger, but we eat in the morning because that gives us energy to live
throughout the day, it's also better for your health, it's just a better way to live,
it's much more natural to eat in the morning and then just live your life, have some juice
in the evening kind of thing, but if you're eating family dinner, then just eat family
dinner, if that's your one meal then sure, it might be helpful for your practice, but
I wouldn't put too much emphasis on it, no I mean go ahead it could be very helpful, I
don't want to discourage you, I'm beginning to see good results from my noting practice,
so I want to help people, is there a specific amount or how much I should try to engage
other people in the practice, probably close to zero specific amount, because you say I want
to help people, it's not a very good argument, I'm seeing good results and that is
why I want to help people, the one thing is not caused by the good results, it's caused
by other things, it's caused by a liking, generally for Buddhists it's a liking of your
results and the liking leads to wanting a greater experience like this, it's what leads
people to perpetuate religion because they're excited for others and when other people are
practicing it they feel good again, so it's kind of an addiction actually and the point
is that it's not really all that helpful, wanting to help other people's never really
all that helpful, but what you have to do and what people everyone should do who practice
is meditation is leave yourself open and be kind and thoughtful when other people are interested
in meditation or when people have problems that you think would be helped with meditation,
so if someone comes to you with a problem asking for your advice and you're in a position
where they would appreciate advice about meditation, I've talked to people about meditation
and it just turned them right off, it just made them think of me as kind of a freak,
so it's not the case that just because you can give someone something good that it's
a good thing to give them, it has to be something that they can receive and benefit
from and usually that's only when they approach you themselves, looking for help, someone
who is in a position of a friend or a sibling, a child, that sort of thing, those are
the cases where it's useful, when I went to practice intensive meditation in Asia and
I came back, I had a couple of people come up to me and I heard you were in Asia, you
know, I could teach me your meditation, I taught both of them and they both did really well,
one of them is now a meditation teacher, he runs courses in Toronto, but I got him started,
I mean he was very interested, it was, it had very little to do with me and much more
to do with him, that he ended up succeeding because I was just giving him the information
passing it on and he wanted it, that's when it's most beneficial, my father's practicing
meditation and he came to me, he asked me, he had some strong personal reasons to take
up meditation and thought that it would help him and he's seen great results, he says
it's really helped him, but that was because he was looking for it.
The more one meditates, the less one sees entertainment, possessions and modern fast life
as satisfactory or as a goal, what life goals are sensible for a meditator to bring purpose.
Purpose and goals are not really a part of the path, I mean the only goal is freedom,
it's a very simple goal, but it's also freedom from goals, you know, life doesn't have
purpose, I mean there's a saying, the purpose of life is a life of purpose, I don't
know if that's accurate either, the purpose of it, but it is in the sense of if by a life
of purpose, you mean a life that is really lived, that is really mindful, really present,
really aware, really living, you know, but a better way to say it is just freedom,
entertainment, peace, goals are just something to be mindful, to be aware of and to see
as they are and let go.
Is it unwholesome or even lying to use the appear of line option on discord or similar
websites, I just don't want the pressure of people expecting a timely response or to say
something, et cetera, the trials and tribulations of the online world, I mean, it sounds
like you're asking the website to live for you, right?
That's not very wholesome, maybe there's some other you can put yourself as unavailable
or something, I don't know, it's not a huge issue, but it is something to consider.
I mean, it's an surprisingly interesting question because it might seem kind of silly,
but appear offline when you're actually online does sound a bit like a lie, right?
I'm going to close my window.
How can I bring myself to doing what I know needs to be done, procrastination and laziness
and resisting work has become a pattern, how do I break this pattern because it's made
my life stagnant?
Well, you have to include the perception that it has made your life a certain way, the
disliking of it, the depression about it, those sorts of things, make sure you're noting
those as well.
I mean, there's no way through, but through you do the work or you don't, I don't have a magic
trick and there is no magic trick that's going to fix things for you, it might take
lifetimes before you do it and there is no special thing you can do.
It's up to you if you do the work, I mean, I would recommend doing courses and even
intensive courses if you have a chance, but to me, he could jump up upon it's up to you to do
the work.
Is it okay for me to eat one meal a day?
I'm 14.
I can't advise you on that, I don't think I'm not your parent or guardian, you should
talk to your parents about it, I think I can get in trouble if I advise you in that regard.
But I guess I could say from a religious perspective, there's nothing against it.
So if you're asking about whether Buddhism has anything about that, there's certainly cases
where seven year olds only ate one meal a day.
So there's nothing religiously wrong about it, but I can't, I don't think I can encourage
that.
I'm not trying to discourage you and that I just can't take a stand on it, I think.
Sorry, I'm glad that you're asking this question, it's great to see you here.
Hang a stick around and please, if you haven't read our booklet, read our booklet and
maybe with your parents, find a way you can do one of our courses, if your parents approve.
Even as one starts practicing vipassana, one gains confidence about its effectiveness.
What doubt is removed is one attains so tapana stage.
Well, the potential for doubt is removed, so you can say you gain confidence, but you
can lose that confidence and doubt again, the only way you can have perfect confidence
is if you see the result, see the goal, true freedom from suffering.
So once you've seen that, then there's perfect confidence.
Like you say you have confidence, what is that confidence based on?
If you haven't actually experienced the fruit, what kind of confidence is it?
It could be blind faith, it could be limited.
It's never going to be perfectly stronger, or well established.
Once one has entered deep samadhi and comes to have a clear, powerful knowing, how does
that transfer to permanent nipana?
It always passes away eventually.
Well, it doesn't transfer to permanent nipana, but what you're seeing is the difference
between samadhi and vipassana.
So you can use the strength of mind that you've developed, but that clear, powerful knowing
can be misleading because you think somehow that that's an attainment, it is a attainment,
it's just a samadhi, and it isn't a true knowing or knowledge of reality.
So you can apply that to the practice of vipassana and get results quite quickly.
If you're interested, you could read our booklet, take one of our courses, and we tried
to do an intensive course and see the difference.
I recently started suffering from tinnitus after a panic attack.
In the power of meditation, help me in coping with this condition.
Yeah.
Absolutely.
I mean, it's good that you phrased it that way because it probably won't make your
tinnitus go away, but it should certainly help you have a better relationship with panic
and anxiety so that you don't get panic attacks.
I mean, don't try to be careful about creating narratives about how the tinnitus came
because it becomes your story where you get depressed about how, oh, if only I hadn't
had this happen, then I wouldn't have that happen and that sort of thing.
So don't worry about what happened in the past.
If you have tinnitus, there's not the hearing and if you have panic, just note the panic
and try and break things apart into their actual experiences.
How does samsara come pass on to another being after death, if the Buddha taught nonself?
Well, they don't and there is no being in the first place let alone another being.
That's what nonself is.
So I mean, I guess that's not quite what you're asking, but the answer is the same as
how does yesterday's me transfer on to today's me?
There's no actual difference.
We think there's a difference because we say, well, because of the physical, but it's
not because of the physical.
The mind doesn't depend on the physical.
Reality doesn't depend on the physical physical is actually kind of conceptual.
I mean, the body is a concept, the brain is a concept.
Reality is our experience and if anything, the physical body is a filter, like a prison
that keeps the mind in a specific pattern or specific set of experiences, the senses.
But moment to moment, the mind continues and the same thing happens at death.
That's all.
Anything really mysterious?
How do you understand the exact same thought scenario or repetitive thought appearing endlessly
coming from a place of anger?
I feel like using Mita is suppressing it.
It just comes back again and again.
Mita is really less effective than mindfulness.
So a part of the practice of Satyapatana is changing our relationship with things so that
rather than trying to stop them from coming back again and again, we become less impressed
or upset by them coming back again and again.
Seeing that things come back again and again helps you be more familiar with this quality
of reality of impermanence and non-salve that you're not in control and that things are
not predictable or according to your wishes.
And those realizations lead to a freedom, lead the mind to letting go, they lead the mind
to abandon clinging and craving and desire and so that's the benefit.
But the key is to take a stance of trying to become more familiar with the experiences
and less triggered by the experiences rather than trying to change them or get rid of them
or fix them.
Mindfulness, Satyapatana is all about wisdom, it's about seeing things clearly rather than
fixing them or changing them, changing the way we perceive them so that they no longer
cause us suffering.
If our aim is to be aware at all times, thus reaching Nibana, would not the lengthy meditation
sessions cultivate the patience, the attention that we need?
Yes, sure, lengthy meditation sessions, but one thing to be aware of is that lengthy but
inconsistent meditation sessions are going to be less effective than moderate but consistent
meditation sessions.
So you have to consider two factors, how often you practice and how long you practice.
If you're doing many short practices, that's not so helpful.
If you're doing very few long sessions, that's also not very helpful.
So the best solution is somewhere in the middle and also to do walking and sitting together,
that's recommended.
I have a wooden tile from Thailand depicting the Buddha.
As I fix my eyes on this tile, I start walking backwards.
Am I breaking some rules?
I feel I am progressing nicely.
I'm progressing backwards.
You have to be careful of statements like I feel I'm progressing nicely.
The question is progressing towards what?
I wouldn't say you're going backwards perhaps, but that was more just a joke, I guess.
You're not breaking any rules, you're not practicing mindfulness.
So you might experience peace and calm and that might feel like practice somehow, but it's
not mindfulness.
It's not going to lead you to see clearly and attain the goal of Buddhism.
So not to discourage you, it doesn't sound like an unwholesome practice.
The walking backwards part is a little concerning because I don't understand the point
of it, it sounds like a ritualistic sort of thing and that can be problematic if you get
obsessed with it and start finding meaning in it.
These sorts of ritualistic things can lead to finding meaning and thinking there's some
meaning like maybe walking backwards has some value or something, but so the mind will
start to encourage it and it will become a ritual or a habit of the mind and I've seen
people fall into very, very elaborate physical patterns of hand movement and just it kind
of feels involuntary, but it's like the mind gets into this habit and starts evolving
it and so on.
So I wouldn't say moving backwards has any value and I would recommend trying to be a little
more mindful and just if you're going to stare at the Buddha, you can just say to yourself,
Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha think about the Buddha's qualities and that's
quite wholesome, still not mindfulness but can be helpful.
Until we've crossed the hour, there's one question left in tier one, do you have time to
answer?
Go ahead, thank you.
How can I forgive my doctors past actions whose cortisol injections gave me tinnitus?
Can meditation help?
You don't have to forgive per se, I mean, it ends up looking a lot like forgiveness
but you have to let go.
So the best way is to focus on your anger and not to trivialize it or dismiss it.
Try and take it as an object of meditation, don't see it as right or if you do feel righteous
about it to make sure you're noting that as well, but just try and note the anger and
the sadness and the disliking of the tinnitus and that sort of thing.
There's nothing special there, just don't make it into something it's not, it's just
experiences.
The anger is an experience and then the forgiving is just sort of a natural expression
of that.
There's nothing really to forgive because you don't see it as some problem, something
that you dislike, right?
It's only we only forgive people if they've done something bad and the problem is seeing
things as bad, I mean, qualitatively it could appear to you as obviously bad, but it's
not, it's just an experience and only when you see experiences as experiences and let
go of the judgments of bad, can you move past, grow holding grudges and that sort of thing?
Thank you, Dante.
That's all the questions we're prepared to ask today.
Great.
Well, it's a good session.
Have a good week.
I wish you all good practice, peace, happiness and freedom from suffering, sadhu, sadhu.
