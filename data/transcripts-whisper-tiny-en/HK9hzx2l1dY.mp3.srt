1
00:00:00,000 --> 00:00:15,000
Okay, good morning.

2
00:00:15,000 --> 00:00:27,000
This works at the beginning of our experiment.

3
00:00:27,000 --> 00:00:29,000
Well this isn't really an experiment.

4
00:00:29,000 --> 00:00:36,000
Although the posting it on the net is, it is to get back into giving daily talks.

5
00:00:36,000 --> 00:00:45,000
And our center.

6
00:00:45,000 --> 00:00:50,000
So I'll just find some random topic on the number.

7
00:00:50,000 --> 00:01:04,000
Sometimes we're going to be relevant to current affairs.

8
00:01:04,000 --> 00:01:08,000
Today's topic is especially relevant.

9
00:01:08,000 --> 00:01:19,000
Today I'll be talking about sickness.

10
00:01:19,000 --> 00:01:22,000
So I'm concerned about sickness these days.

11
00:01:49,000 --> 00:02:12,000
I'm going to talk about four kinds of sickness.

12
00:02:12,000 --> 00:02:41,000
Meaning there are four sources of sickness.

13
00:02:41,000 --> 00:02:48,000
And where does sickness come from?

14
00:02:48,000 --> 00:02:55,000
Well I don't know that it's so much about wondering where sickness comes from as it is about.

15
00:02:55,000 --> 00:03:10,000
Understanding sickness and becoming more familiar and just reflecting on sickness really.

16
00:03:10,000 --> 00:03:19,000
So I'm going to talk about sickness.

17
00:03:19,000 --> 00:03:48,000
We reflect on sickness.

18
00:03:48,000 --> 00:03:58,000
It allows us to understand, understand our health better.

19
00:03:58,000 --> 00:04:13,000
It allows us to be more in tune and in touch with the causes of suffering.

20
00:04:13,000 --> 00:04:25,000
I'm going to say Arongya Paramala, freedom from sickness is the greatest gain.

21
00:04:25,000 --> 00:04:37,000
So he meant something more by sickness than simply.

22
00:04:37,000 --> 00:04:55,000
Indigestion or a virus.

23
00:04:55,000 --> 00:05:10,000
It's clearly being free from sickness isn't sufficient to be considered the greatest gain.

24
00:05:10,000 --> 00:05:24,000
So if we put in perspective we can categorize all kinds of sickness as the same category.

25
00:05:24,000 --> 00:05:31,000
I can put in the same category all types of sickness.

26
00:05:31,000 --> 00:05:48,000
Something that constitutes suffering causes suffering.

27
00:05:48,000 --> 00:05:59,000
Something that disturbs peace, happiness.

28
00:05:59,000 --> 00:06:09,000
The four sources of suffering to really give a sense of the whole spectrum of sickness.

29
00:06:09,000 --> 00:06:19,000
The four kinds of sickness, four sources of sickness.

30
00:06:19,000 --> 00:06:38,000
First of all, Ahara, food, second utu, means environment, third, chitat, the mind.

31
00:06:38,000 --> 00:07:01,000
Fourth, comma, action is the four sources of sickness.

32
00:07:01,000 --> 00:07:08,000
To be free from all four of these, this would be the greatest gain indeed.

33
00:07:08,000 --> 00:07:09,000
First two are simple.

34
00:07:09,000 --> 00:07:16,000
They relate to the physical types of sickness, but they have an importance in the dhamma as

35
00:07:16,000 --> 00:07:17,000
well.

36
00:07:17,000 --> 00:07:30,000
Food, for example, to start with food seems like something mundane, not really spiritual.

37
00:07:30,000 --> 00:07:46,000
But food is a very important dhamma, very important part of dhamma practice.

38
00:07:46,000 --> 00:07:52,000
A very important dhamma lesson.

39
00:07:52,000 --> 00:08:08,000
Studying food gives us a very good perspective on how the mind works, how the mind clings.

40
00:08:08,000 --> 00:08:21,000
Food shows us very clearly, more clearly than pretty much anything, because it's the one thing

41
00:08:21,000 --> 00:08:29,000
that we can't live without food, water, put them together.

42
00:08:29,000 --> 00:08:40,000
Besides Ahara, food is unique because of its diversity and its potential for clinging.

43
00:08:40,000 --> 00:08:50,000
Food is the one thing that we need.

44
00:08:50,000 --> 00:09:00,000
It has potential to cause craving, so more than anything else.

45
00:09:00,000 --> 00:09:12,000
It shows us clearly, it provides a clear opportunity for us to understand,

46
00:09:12,000 --> 00:09:19,000
understand how the mind works, how the mind clings, how the mind craves,

47
00:09:19,000 --> 00:09:28,000
how the mind chooses.

48
00:09:28,000 --> 00:09:43,000
Food is actually quite simple, it's meant to nourish the body.

49
00:09:43,000 --> 00:09:53,000
There's no reason for food to bring us sickness, why would it?

50
00:09:53,000 --> 00:10:02,000
It wouldn't eat something explicitly for the purpose of making you sick, not usually, not normally.

51
00:10:02,000 --> 00:10:13,000
Yet, quite normally, we do eat, consume things that make us quite sick.

52
00:10:13,000 --> 00:10:19,000
It might seem banal and worldly to talk about things like heart disease,

53
00:10:19,000 --> 00:10:25,000
or high blood pressure, diabetes, even cancer.

54
00:10:25,000 --> 00:10:33,000
And yet, all of these are very much related to our food consumption, not always, but have a strong correlation.

55
00:10:33,000 --> 00:10:41,000
In many cases, diabetes, we eat too much sugar, too much starch, too much.

56
00:10:41,000 --> 00:10:48,000
Carbohydrates, heart disease, we eat too much fat, too much, whatever.

57
00:10:48,000 --> 00:10:56,000
I'm blood pressure, too much salt. Why would you do such a thing?

58
00:10:56,000 --> 00:11:08,000
We see how our choices are not rational, how we do things very much against our own benefit.

59
00:11:08,000 --> 00:11:29,000
When we make ourselves sick, we see how our dangerous our minds can be through, through food.

60
00:11:29,000 --> 00:11:38,000
In fact, taken to its ultimate conclusion, we come to see that we don't need to eat very much.

61
00:11:38,000 --> 00:11:44,000
Much of our food consumption, even though it might not make us sick,

62
00:11:44,000 --> 00:11:51,000
might not make us overtly, acutely, physically sick.

63
00:11:51,000 --> 00:12:07,000
They still involve sickness. Our desire to be strong plump.

64
00:12:07,000 --> 00:12:22,000
The desire to look handsome, beautiful, our desire to grow tall.

65
00:12:22,000 --> 00:12:34,000
Even our desire for good health, it becomes a sickness, very easily becomes a sickness.

66
00:12:34,000 --> 00:12:47,000
Food, the Buddha very much, was very often very frequently.

67
00:12:47,000 --> 00:12:53,000
Careful to exert the monks in moderation and food.

68
00:12:53,000 --> 00:13:02,000
Single it out, really just single it out, because it is the one thing we need.

69
00:13:02,000 --> 00:13:08,000
It is so familiar to us, we don't realize.

70
00:13:08,000 --> 00:13:18,000
If you never try to practice to be mindful, you don't realize how attached we become.

71
00:13:18,000 --> 00:13:23,000
Food seems like something, you're not really attached to it all for many people.

72
00:13:23,000 --> 00:13:27,000
People who have gone hungry sometimes have a sense.

73
00:13:27,000 --> 00:13:35,000
People who are mindful, hungry or not, or they can see very clearly.

74
00:13:35,000 --> 00:13:39,000
Apart from almost anything, it's the one thing we cling to.

75
00:13:39,000 --> 00:13:42,000
It's one of the things.

76
00:13:42,000 --> 00:13:47,000
So food is an important dhamma lesson.

77
00:13:47,000 --> 00:13:51,000
Try to be mindful when you eat.

78
00:13:51,000 --> 00:14:01,000
The second is utu really encompasses any sickness that comes from the external environment.

79
00:14:01,000 --> 00:14:09,000
Could be from radiation, could be from the sun, which is radiation.

80
00:14:09,000 --> 00:14:26,000
Could be from viruses, could be from bacteria, allergies.

81
00:14:26,000 --> 00:14:31,000
This one, again you might think, not very dhamma, not very much dhamma.

82
00:14:31,000 --> 00:14:38,000
The clearest lesson you could give maybe is to be mindful of these things.

83
00:14:38,000 --> 00:15:02,000
People who have allergies can have difficulty practicing.

84
00:15:02,000 --> 00:15:09,000
I did hard to breathe, hard to sit still when you have allergies.

85
00:15:09,000 --> 00:15:21,000
With any sickness, if you have a cold, if you have the flu, even if you're dying, it often

86
00:15:21,000 --> 00:15:28,000
presents a great opportunity to practice, to cultivate patience, mindfulness, objectivity.

87
00:15:28,000 --> 00:15:35,000
It's a real challenge.

88
00:15:35,000 --> 00:15:41,000
But there is another lesson, and I think it's a lesson that includes the food.

89
00:15:41,000 --> 00:15:52,000
Is that our environment and our food is not entirely independent of our dhamma practice,

90
00:15:52,000 --> 00:15:59,000
or of our state of mind.

91
00:15:59,000 --> 00:16:06,000
You may see our environment is beyond our control, it really isn't in many ways.

92
00:16:06,000 --> 00:16:15,000
Where we choose to hang out, where we choose to live our lives,

93
00:16:15,000 --> 00:16:23,000
and how our environment changes based on how we live our lives.

94
00:16:23,000 --> 00:16:28,000
As a society, you can see this on the societal level.

95
00:16:28,000 --> 00:16:36,000
As a society, we destroy our environment.

96
00:16:36,000 --> 00:16:50,000
Even just the slaughter and consumption of animals, all the bacteria and sickness that has come from that.

97
00:16:50,000 --> 00:16:58,000
That's very karmic, very much related to the sickness of killing.

98
00:16:58,000 --> 00:17:03,000
Our greed for consumption, how it's polluted the air and the water.

99
00:17:03,000 --> 00:17:07,000
The water that we drink, we've poisoned our own drinking water.

100
00:17:07,000 --> 00:17:10,000
You can drink it anymore.

101
00:17:10,000 --> 00:17:11,000
That would have greed.

102
00:17:11,000 --> 00:17:12,000
Nothing but greed.

103
00:17:12,000 --> 00:17:15,000
There's no other excuse.

104
00:17:15,000 --> 00:17:42,000
There's no other reason.

105
00:17:42,000 --> 00:17:46,000
There's a dhamma perspective on this as well.

106
00:17:46,000 --> 00:17:50,000
The third is jitta.

107
00:17:50,000 --> 00:17:57,000
Jitta is where our real dhamma practice begins.

108
00:17:57,000 --> 00:18:08,000
The main focus of our practice is the healing of the mind.

109
00:18:08,000 --> 00:18:12,000
The mind really can make us very sick.

110
00:18:12,000 --> 00:18:23,000
Physically, we might get very angry.

111
00:18:23,000 --> 00:18:29,000
Our blood pressure spikes from the anger can spike because of greed as well.

112
00:18:29,000 --> 00:18:47,000
For a long exposure to anger, greed, even delusion, I would say, these have notable

113
00:18:47,000 --> 00:18:55,000
measurable effects on the body.

114
00:18:55,000 --> 00:19:05,000
For a long exposure, for a long engagement, for a long exposure might be the best way to put it.

115
00:19:05,000 --> 00:19:08,000
We think of anger as something we do.

116
00:19:08,000 --> 00:19:12,000
Many ways it's something that happens to us.

117
00:19:12,000 --> 00:19:15,000
It's like a sickness.

118
00:19:15,000 --> 00:19:25,000
Both would call them a kantuka.

119
00:19:25,000 --> 00:19:30,000
They invade our minds.

120
00:19:30,000 --> 00:19:34,000
They aren't our minds exactly.

121
00:19:34,000 --> 00:19:43,000
They're afflictions.

122
00:19:43,000 --> 00:20:03,000
For exposed to this affliction, or if we are infected by anger, greed, it grows like bacteria.

123
00:20:03,000 --> 00:20:14,000
It spreads like fire, grows like a virus.

124
00:20:14,000 --> 00:20:18,000
It can have real effects on the body.

125
00:20:18,000 --> 00:20:21,000
But that's not the only way that it's a sickness, of course.

126
00:20:21,000 --> 00:20:31,000
In fact, the most important aspect of how greed and anger are sicknesses is mental.

127
00:20:31,000 --> 00:20:36,000
They lead to mental suffering.

128
00:20:36,000 --> 00:20:38,000
They are mental sickness.

129
00:20:38,000 --> 00:20:41,000
They hurt causes to hurt other people.

130
00:20:41,000 --> 00:20:49,000
They cause for so much suffering, greed, anger and delusion.

131
00:20:49,000 --> 00:21:04,000
You can see jealousy, arrogance and so on and so on.

132
00:21:04,000 --> 00:21:11,000
The real sickness.

133
00:21:11,000 --> 00:21:21,000
And here's where we see most clearly that mindfulness is effective.

134
00:21:21,000 --> 00:21:29,000
I practice mindfulness to cure the sicknesses of the mind.

135
00:21:29,000 --> 00:21:38,000
It's very much like an anti-biotic or something.

136
00:21:38,000 --> 00:21:42,000
We don't have to stop ourselves from getting angry or greedy.

137
00:21:42,000 --> 00:21:47,000
What we have to do is take our medicine and it's like a cure-all.

138
00:21:47,000 --> 00:21:48,000
Why is it a cure-all?

139
00:21:48,000 --> 00:21:55,000
Because it's the opposite of reaction, all defilements.

140
00:21:55,000 --> 00:21:57,000
They're all reactions.

141
00:21:57,000 --> 00:22:02,000
They are all responses to experience.

142
00:22:02,000 --> 00:22:06,000
Mindfulness is a different type of response.

143
00:22:06,000 --> 00:22:16,000
It replaces the sickness, the responses that are an wholesome and pleasant and beneficial

144
00:22:16,000 --> 00:22:29,000
harmful with a neutral response, a response that is harmless, that is peaceful, that is healthy.

145
00:22:29,000 --> 00:22:45,000
The fourth sickness is kamma.

146
00:22:45,000 --> 00:22:52,000
Kamma in some ways is just an extrapolation of mental sickness.

147
00:22:52,000 --> 00:23:04,000
But it is a different category simply because our mental illness doesn't just make us sick as it's happening.

148
00:23:04,000 --> 00:23:13,000
It can have profound effects on our future existence as well.

149
00:23:13,000 --> 00:23:19,000
Because, well, let's not even talk about future lives, but because our sickness,

150
00:23:19,000 --> 00:23:24,000
suppose you're very angry, we're getting angry, we'll hurt you.

151
00:23:24,000 --> 00:23:28,000
Just as you're angry, it's painful.

152
00:23:28,000 --> 00:23:31,000
And as I said, it can have...

153
00:23:31,000 --> 00:23:37,000
We can see that it has prolonged effects, if there's prolonged exposure,

154
00:23:37,000 --> 00:23:41,000
to make you really sick and so on.

155
00:23:41,000 --> 00:23:46,000
But beyond that, it causes us to do and say things.

156
00:23:46,000 --> 00:23:52,000
There are many things we wouldn't say, or we'd say differently if we didn't have anger.

157
00:23:52,000 --> 00:23:56,000
Sometimes we regret those things.

158
00:23:56,000 --> 00:24:01,000
But once they're done, there's no taking them back.

159
00:24:01,000 --> 00:24:06,000
Sometimes it's even beyond what we intended.

160
00:24:06,000 --> 00:24:10,000
It's one thing to be angry at someone, but if you strike them,

161
00:24:10,000 --> 00:24:16,000
you've gone the next step, you've done something beyond just getting angry.

162
00:24:16,000 --> 00:24:19,000
If you slash out, speak to someone in anger.

163
00:24:19,000 --> 00:24:22,000
You've performed a real karma.

164
00:24:22,000 --> 00:24:26,000
It's on another level in some ways.

165
00:24:26,000 --> 00:24:30,000
If you kill, you can't take that back.

166
00:24:30,000 --> 00:24:38,000
If you're angry, well, you can't take that back either, but it's not yet of on the same magnitude.

167
00:24:38,000 --> 00:24:52,000
Let's see, there's something very, sort of, exceptional about actual acts based on our development.

168
00:24:52,000 --> 00:24:58,000
Because you see that once you've done it, once you've engaged with the universe,

169
00:24:58,000 --> 00:25:06,000
the universe will respond, killing, stealing, lying, cheating.

170
00:25:06,000 --> 00:25:10,000
All of these things have consequences in the universe.

171
00:25:10,000 --> 00:25:14,000
They change our universe.

172
00:25:14,000 --> 00:25:16,000
They change the way people interact with us.

173
00:25:16,000 --> 00:25:21,000
They change the way we live our lives.

174
00:25:21,000 --> 00:25:29,000
The guilt that comes, we have to live with that.

175
00:25:29,000 --> 00:25:42,000
When we pass away, there's the idea that our minds, the sickness of the mind will affect our future life.

176
00:25:42,000 --> 00:25:48,000
Where we choose to be reborn will be affected by our clarity of mind.

177
00:25:48,000 --> 00:25:51,000
How the fetus develops.

178
00:25:51,000 --> 00:25:52,000
I was reading about it.

179
00:25:52,000 --> 00:25:57,000
I think there is something to this idea that in the fetus,

180
00:25:57,000 --> 00:26:02,000
they call them mutations, I think.

181
00:26:02,000 --> 00:26:06,000
Why would mutations occur?

182
00:26:06,000 --> 00:26:16,000
Of course, most people would say it's just random or chance or just the way it happens.

183
00:26:16,000 --> 00:26:19,000
But as we would say, there's more to it than that.

184
00:26:19,000 --> 00:26:29,000
That the mind is active than the mind's engagement, obsessions, and attention that the mind pays.

185
00:26:29,000 --> 00:26:37,000
Just the repeated attention in certain ways will affect things, can affect the fetus.

186
00:26:37,000 --> 00:26:43,000
Can lead to great sickness, lifetime sickness.

187
00:26:43,000 --> 00:26:49,000
It's different from mental sickness because some people can be very good people.

188
00:26:49,000 --> 00:26:57,000
Some people are very healthy and wicked, mean, nasty.

189
00:26:57,000 --> 00:27:01,000
Speaking, nasty things, doing nasty things.

190
00:27:01,000 --> 00:27:07,000
Other people are quite nice and yet suffer from debilitating illnesses.

191
00:27:07,000 --> 00:27:13,000
That you may think are based on karma.

192
00:27:13,000 --> 00:27:21,000
And that they are generally nice people.

193
00:27:21,000 --> 00:27:28,000
Mental illness. Mental illness can hurt you.

194
00:27:28,000 --> 00:27:35,000
But it can also cause you to do things that have a far greater lasting effect.

195
00:27:35,000 --> 00:27:41,000
It may be reborn in a place that doesn't allow you to develop spiritually,

196
00:27:41,000 --> 00:27:49,000
or in a body that makes it difficult, and so on.

197
00:27:49,000 --> 00:27:52,000
So some thoughts on sickness.

198
00:27:52,000 --> 00:27:55,000
Again, just random teachings.

199
00:27:55,000 --> 00:27:58,000
Just pick something every morning, maybe something someone said to me,

200
00:27:58,000 --> 00:28:05,000
who wants to buy your attention. Thank you all for listening. Have a good day.

