1
00:00:00,000 --> 00:00:07,680
And Masaki asked, is one able to ordain with the medical conditions such as epilepsy that

2
00:00:07,680 --> 00:00:11,680
they take medicine for?

3
00:00:11,680 --> 00:00:17,520
I'll start now.

4
00:00:17,520 --> 00:00:20,840
Yes, it's possible to meditate.

5
00:00:20,840 --> 00:00:25,600
The question of course is what the medication does to your mind.

6
00:00:25,600 --> 00:00:33,840
I would, if possible, much rather have, especially with epilepsy, have a person slowly

7
00:00:33,840 --> 00:00:39,480
begin to wend themselves off the medication in favor of the meditation.

8
00:00:39,480 --> 00:00:45,120
And when an epileptic fit occurs, because as I understand, they're mainly physical.

9
00:00:45,120 --> 00:00:51,760
I don't know how it affects the mind and maybe you can enlighten us, but if you're having

10
00:00:51,760 --> 00:00:55,480
an epileptic fit and you're still able to be mindful of it, then just be mindful

11
00:00:55,480 --> 00:00:56,480
of it as best you can.

12
00:00:56,480 --> 00:01:02,480
If you want to take some medication, then take some medication, but...

13
00:01:02,480 --> 00:01:09,800
Oh, no, you think that is not allowed.

14
00:01:09,800 --> 00:01:17,640
Right, yeah, I think this is not possible when you have epilepsy.

15
00:01:17,640 --> 00:01:20,480
This is one of the questions of the sicknesses.

16
00:01:20,480 --> 00:01:26,880
So it's a simple answer, no, you can.

17
00:01:26,880 --> 00:01:29,760
It depends how extreme it is, I suppose.

18
00:01:29,760 --> 00:01:34,920
If you can find, if you can, if you can do, what you should do is as it has, when I

19
00:01:34,920 --> 00:01:41,360
said we'll practice meditation, sorry, I missed read the question, but then do that

20
00:01:41,360 --> 00:01:42,360
as a test.

21
00:01:42,360 --> 00:01:50,400
If through meditation practice, your epilepsy is even cured, then you could ordain.

22
00:01:50,400 --> 00:01:56,960
Because the word is just a word, no, if certain things occur in the body, they may change

23
00:01:56,960 --> 00:01:57,960
over time.

24
00:01:57,960 --> 00:02:02,760
Of course, these sicknesses, if a person becomes cured of these things, then they can ordain.

25
00:02:02,760 --> 00:02:07,960
So you should test it out in the meditation, see how your meditation practice affects it,

26
00:02:07,960 --> 00:02:13,560
because you should certainly have these things dealt with before you ordain taking, having

27
00:02:13,560 --> 00:02:20,360
any condition for what you have to take medication for, what you have to take, or in

28
00:02:20,360 --> 00:02:26,520
order to function properly, have to take medication for is a real assignment, you shouldn't,

29
00:02:26,520 --> 00:02:31,400
you may not be able to ordain, because you can't be sure that you're going to get that

30
00:02:31,400 --> 00:02:38,400
medication, even though you'll be allowed to, there's allowances to ask for it.

31
00:02:38,400 --> 00:02:44,600
It becomes a burden, and because of the uncertainty of being a monk and the necessity of

32
00:02:44,600 --> 00:02:51,800
being content with whatever you get, makes it quite a burden to the song and a barrier

33
00:02:51,800 --> 00:02:57,480
to your own ability to carry out all of the duties of a monk and keep the hundreds of rules

34
00:02:57,480 --> 00:03:08,040
and the practices and the communal lifestyle.

35
00:03:08,040 --> 00:03:20,920
There are certain sicknesses, I don't remember all, leprosy is one, and ringworms, I think

36
00:03:20,920 --> 00:03:30,520
is one, and I think epilepsy is one of them, there are certain sicknesses, you can not

37
00:03:30,520 --> 00:03:39,800
have, or if you have them, you cannot ordain, there is a part of the ordination procedure

38
00:03:39,800 --> 00:03:50,920
where your questions do you have this, do you have that, and if you say yes, I do, then you

39
00:03:50,920 --> 00:04:04,920
will probably be advised not to ordain them, okay?

