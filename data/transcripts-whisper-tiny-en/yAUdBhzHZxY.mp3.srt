1
00:00:00,000 --> 00:00:04,000
What is the curriculum for your meditation course?

2
00:00:04,000 --> 00:00:08,000
I'm not sure I thought this was on the website.

3
00:00:08,000 --> 00:00:10,000
But good to go through it.

4
00:00:10,000 --> 00:00:14,000
What is the curriculum of our meditation course?

5
00:00:14,000 --> 00:00:16,000
Hmm.

6
00:00:20,000 --> 00:00:24,000
Well, you show up on day one.

7
00:00:24,000 --> 00:00:28,000
We give you a tour of the place.

8
00:00:28,000 --> 00:00:30,000
There's the kitchen.

9
00:00:30,000 --> 00:00:32,000
That's where you get food.

10
00:00:32,000 --> 00:00:34,000
Food comes at this time.

11
00:00:34,000 --> 00:00:38,000
Eat whenever you like between the hours that we specify.

12
00:00:38,000 --> 00:00:42,000
But probably the best is to eat when the food arrives because it's hot.

13
00:00:42,000 --> 00:00:45,000
There's your room.

14
00:00:45,000 --> 00:00:46,000
Leaks.

15
00:00:46,000 --> 00:00:50,000
So you got these buckets here to keep the water out.

16
00:00:50,000 --> 00:00:53,000
Oh, and by the way, that's a leech on your foot.

17
00:00:53,000 --> 00:00:57,000
So don't let it stay there because it's going to make it the idea.

18
00:00:57,000 --> 00:01:02,000
Make you on a tour and we lead you through all that stuff.

19
00:01:02,000 --> 00:01:07,000
Then we ask if you've been practicing already because nowadays it's everything's on the internet.

20
00:01:07,000 --> 00:01:13,000
So many people have actually already started before they arrive, which is great.

21
00:01:13,000 --> 00:01:18,000
And no problem if you haven't because either way, we have to see whether you're doing it correctly.

22
00:01:18,000 --> 00:01:21,000
If you have never done it before, we show it to you off in the beginning.

23
00:01:21,000 --> 00:01:26,000
If you have, we just check and make sure you're doing it correctly and adjust you accordingly.

24
00:01:26,000 --> 00:01:32,000
And then we say you're to practice now as much as you can comfortably.

25
00:01:32,000 --> 00:01:34,000
So we have no schedule.

26
00:01:34,000 --> 00:01:36,000
We have to do practice this time this time this time.

27
00:01:36,000 --> 00:01:37,000
We don't have that.

28
00:01:37,000 --> 00:01:39,000
We don't say how many hours you're supposed to do.

29
00:01:39,000 --> 00:01:43,000
My teacher always said do some in the morning, some in the afternoon, some at night.

30
00:01:43,000 --> 00:01:49,000
He said the most important is not how many hours you do, but how many moments.

31
00:01:49,000 --> 00:01:57,000
One thing that we specify is that we ask you not to sleep more than six hours at night.

32
00:01:57,000 --> 00:02:00,000
So if you sleep less than that, that's no problem.

33
00:02:00,000 --> 00:02:03,000
Some people have a shock there that they think, wow, six hours.

34
00:02:03,000 --> 00:02:07,000
Some people don't realize it's important to get that clear.

35
00:02:07,000 --> 00:02:12,000
Don't sleep more than six hours, so that really doesn't leave you much option.

36
00:02:12,000 --> 00:02:17,000
If you can't sleep all day, then you got to do something and since there's nothing else to do,

37
00:02:17,000 --> 00:02:19,000
you probably do lots of meditation.

38
00:02:19,000 --> 00:02:22,000
But we try to make it comfortable and make it your pace.

39
00:02:22,000 --> 00:02:26,000
Don't let us set goals for you and go for it.

40
00:02:26,000 --> 00:02:36,000
Anyway, I shouldn't try to push in one way because there are other centers that have group meditation all day,

41
00:02:36,000 --> 00:02:38,000
and there are centers that tell you this.

42
00:02:38,000 --> 00:02:40,000
I'm just going through what we do.

43
00:02:40,000 --> 00:02:42,000
Try to be more objective.

44
00:02:42,000 --> 00:02:48,000
For our center, we tell you in this way.

45
00:02:48,000 --> 00:02:53,000
We send you off to meditate, and then we say, at three o'clock or four o'clock in the afternoon,

46
00:02:53,000 --> 00:02:56,000
you'll have to meet with your teacher.

47
00:02:56,000 --> 00:03:03,000
Maybe if you're lucky, I'll be giving talks and telling you stories from the Dhamapada.

48
00:03:03,000 --> 00:03:09,000
Otherwise, if you're out of luck, we give you a book, and we say, this is the only book you can read while you're here.

49
00:03:09,000 --> 00:03:15,000
It's about this thick, these ones on the back there.

50
00:03:15,000 --> 00:03:18,000
A really good book, I think.

51
00:03:18,000 --> 00:03:21,000
I can't even say that, can I?

52
00:03:21,000 --> 00:03:24,000
No, it's really good because it doesn't say anything.

53
00:03:24,000 --> 00:03:26,000
Everything in there is stuff I got from my teacher.

54
00:03:26,000 --> 00:03:30,000
It's just my translations and explanations that I've used.

55
00:03:30,000 --> 00:03:32,000
And the great thing about a book is you can edit it.

56
00:03:32,000 --> 00:03:35,000
So when I did all these videos on meditation, they're not so good.

57
00:03:35,000 --> 00:03:39,000
But the book is great because I've been able to edit it.

58
00:03:39,000 --> 00:03:43,000
And so give that to the people and to the meditators, and you can read that.

59
00:03:43,000 --> 00:03:46,000
I say you can read it anyway.

60
00:03:46,000 --> 00:03:48,000
I hope I'm okay there.

61
00:03:48,000 --> 00:03:56,000
But no other reading, no other communication, no internet, no music, no fun.

62
00:03:56,000 --> 00:03:58,000
Just go off and meditate in the forest.

63
00:03:58,000 --> 00:04:00,000
And that's all you do.

64
00:04:00,000 --> 00:04:07,000
And then you're totally under our power, under the power of your teacher.

65
00:04:07,000 --> 00:04:11,000
No, you're totally in one sense, you're totally dependent on your teacher.

66
00:04:11,000 --> 00:04:13,000
The rest, I mean, and I mean that in a good way.

67
00:04:13,000 --> 00:04:15,000
It means the rest, you don't have to worry about it.

68
00:04:15,000 --> 00:04:20,000
The rest, there is no requirement from your end.

69
00:04:20,000 --> 00:04:22,000
You just keep meditating.

70
00:04:22,000 --> 00:04:27,000
And every day, it won't get monotonous because every day your teacher is going to push you

71
00:04:27,000 --> 00:04:30,000
ahead, but like, adjust you.

72
00:04:30,000 --> 00:04:32,000
You're going the wrong way, and they'll push you in this way.

73
00:04:32,000 --> 00:04:34,000
And then they'll give you something new.

74
00:04:34,000 --> 00:04:36,000
And they'll give you a new way to look at things.

75
00:04:36,000 --> 00:04:40,000
So it doesn't mean that, wow, a 21-day meditation course, that's so long.

76
00:04:40,000 --> 00:04:41,000
Every day it changes.

77
00:04:41,000 --> 00:04:43,000
And every day you learn something new.

78
00:04:43,000 --> 00:04:47,000
And every day you open up more, every day you're challenged.

79
00:04:47,000 --> 00:04:55,000
And you'll find repeatedly through the course that you really didn't know what you were getting yourself into.

80
00:04:55,000 --> 00:05:00,000
You heard about this thing called wisdom, but it so blows you away with true wisdom is.

81
00:05:00,000 --> 00:05:02,000
And what true understanding is.

82
00:05:02,000 --> 00:05:13,000
And all of your expectations and all of your understandings about Buddhism and enlightenment are just shattered.

83
00:05:13,000 --> 00:05:16,000
Because they're all just ideas, they're all just concepts.

84
00:05:16,000 --> 00:05:19,000
And you start to see reality.

85
00:05:19,000 --> 00:05:24,000
You start to realize what it was that the Buddha taught.

86
00:05:24,000 --> 00:05:28,000
And you do that for 21 days, and then at the end we say,

87
00:05:28,000 --> 00:05:31,000
congratulations, you've finished the course.

88
00:05:31,000 --> 00:05:34,000
Wanna become a monk?

89
00:05:34,000 --> 00:05:37,000
Or a female monk?

90
00:05:37,000 --> 00:05:41,000
And that's the curriculum of our course.

91
00:05:41,000 --> 00:05:43,000
That's the foundation course.

92
00:05:43,000 --> 00:05:47,000
The advanced course does the same thing in 10 days.

93
00:05:47,000 --> 00:05:50,000
Because you're already skilled, so you don't need all the fluff,

94
00:05:50,000 --> 00:05:52,000
and you don't need all the adjustment.

95
00:05:52,000 --> 00:05:56,000
You're fine-tuned already, and you do an advanced course, same thing in 10 days.

96
00:05:56,000 --> 00:06:00,000
Those are the two courses that we have to offer.

97
00:06:00,000 --> 00:06:05,000
You also have an intermediate course that we sometimes offer to people and different courses.

98
00:06:05,000 --> 00:06:07,000
There's lots of fun we can have.

99
00:06:07,000 --> 00:06:13,000
But these are the two main potatoes courses.

100
00:06:13,000 --> 00:06:23,000
No, nothing to add.

