1
00:00:00,000 --> 00:00:04,800
Hello, welcome back to Ask a Monk.

2
00:00:04,800 --> 00:00:15,400
Next question comes from Bobby, who asks, is there a way to get more motivated while meditating?

3
00:00:15,400 --> 00:00:26,000
Sometimes I want to meditate for an hour, but I end up unable to meditate longer than 20 minutes.

4
00:00:26,000 --> 00:00:37,000
But I think an important part of the answer is the answer I gave to a recent video that meditation is something that is momentary.

5
00:00:37,000 --> 00:00:47,000
It's something that is to be done at every moment, so you can meditate for an hour.

6
00:00:47,000 --> 00:00:53,000
You've been only meditate for one instant, and that instant is either meditative or it's not.

7
00:00:53,000 --> 00:00:57,000
At this moment, are you mindful or are you not?

8
00:00:57,000 --> 00:01:12,000
When you ask how to get motivated during meditation, what you mean is, how do I, well, a better question is, how do I best make use of this hour so that it's meditative, so that I'm meditating?

9
00:01:12,000 --> 00:01:31,000
For the most of it, and I think if you've changed this around you, you won't worry so much about how long you sit, why you set these goals for yourself for one hour or sitting and then you become discouraged.

10
00:01:31,000 --> 00:01:41,000
So I talked about that before in another video, but an interesting part of this, the inability to sit for a long period,

11
00:01:41,000 --> 00:01:50,000
has to do with really the reason why we're meditating in the first place, which is the problems that exist in the mind.

12
00:01:50,000 --> 00:02:03,000
So what's stopping you from sitting for an hour in the same position, what you call meditating, are the things that you should be meditating on.

13
00:02:03,000 --> 00:02:14,000
And this is really crucial in meditation practice. It's something that's so easy to forget, and the reason why it's so easy to forget it because these are the problems.

14
00:02:14,000 --> 00:02:32,000
These are what's causing us problems throughout our life, if they were, if we realized that they were a problem that they were coming on, they wouldn't cause problems for us in our life because, but there's so much a part of who we are.

15
00:02:32,000 --> 00:02:40,000
And we meditate in order to work these problems out.

16
00:02:40,000 --> 00:02:49,000
So you'll be sitting for some time in the same sort of condition as would arise in your ordinary, every new life arises.

17
00:02:49,000 --> 00:02:53,000
You want something, and right away you chase after it.

18
00:02:53,000 --> 00:03:01,000
And so you can't sit, you have to chase after it, or you're sitting for some time and something bothers you.

19
00:03:01,000 --> 00:03:06,000
It could be the pain, it could be just having to sit still, it could be the meditation technique.

20
00:03:06,000 --> 00:03:13,000
Maybe you don't like having to use this mantra, so that seems stupid, it seems boring, so on.

21
00:03:13,000 --> 00:03:22,000
These same negative, unwholesome qualities will arise in the mind that that's when meditation should be carried out.

22
00:03:22,000 --> 00:03:26,000
That's when it has the best results and the greatest impact.

23
00:03:26,000 --> 00:03:39,000
So one trick you can use is at the time when you want to stop, understand that that's when you have to start.

24
00:03:39,000 --> 00:03:44,000
And ask yourself what is the problem?

25
00:03:44,000 --> 00:03:51,000
What is it that's causing me to want to stop minutes, to stop sitting here, to get up and do something else?

26
00:03:51,000 --> 00:03:55,000
What's the difference we've been sitting here and going somewhere else?

27
00:03:55,000 --> 00:04:03,000
It could be there's a reason you have to do something, or your life's going to fall off,

28
00:04:03,000 --> 00:04:09,000
or you're going to get permanent damage if you sit in this position.

29
00:04:09,000 --> 00:04:20,000
But most likely in the almost every case, it's simply because of those things which are the reason why we're meditating in the first place.

30
00:04:20,000 --> 00:04:26,000
They're coming up and here's your chance to work them out, here's your chance to examine them,

31
00:04:26,000 --> 00:04:31,000
and to really see that they're causing you only stress and suffering.

32
00:04:31,000 --> 00:04:34,000
They're not any benefit to you.

33
00:04:34,000 --> 00:04:41,000
This is one way, the best way really to allow you to sit for long periods.

34
00:04:41,000 --> 00:04:50,000
There's another way of addressing this question, the question of motivation.

35
00:04:50,000 --> 00:04:55,000
I don't think this is exactly what you meant by motivation, but to help you get motivated,

36
00:04:55,000 --> 00:05:03,000
you feel like you'd rather be doing other things, and meditation doesn't seem that important,

37
00:05:03,000 --> 00:05:08,000
and you find yourself slowly giving up meditation and not interested in it.

38
00:05:08,000 --> 00:05:14,000
There's many different ways that one can motivate oneself.

39
00:05:14,000 --> 00:05:20,000
You can motivate yourself based on negative things, you can motivate yourself, based on positive things.

40
00:05:20,000 --> 00:05:32,000
Positive things are the benefits of meditation, the benefits to yourself, the peace, and the clarity of mind that comes from meditation.

41
00:05:32,000 --> 00:05:40,000
You can think of how much you would be helping other people, you can think of all of the people in your family,

42
00:05:40,000 --> 00:05:42,000
or stressed and suffering, and so on.

43
00:05:42,000 --> 00:05:49,000
Your friends who you're not able to help, the number of people that you'd be able to help just by meditating.

44
00:05:49,000 --> 00:06:01,000
I don't think that there's anything special about what I do, but the fact that I meditate gives me the ability to share some little tidbits with other people.

45
00:06:01,000 --> 00:06:11,000
I think anyone who's practiced meditation has that ability to share with other people, the benefits that it has in your life,

46
00:06:11,000 --> 00:06:21,000
or study many people are motivated to meditate because of study, many people are motivated due to the benefits that has towards their work,

47
00:06:21,000 --> 00:06:24,000
their stress levels are lower.

48
00:06:24,000 --> 00:06:34,000
Their efficiency, their success in business, their success in their life is increased.

49
00:06:34,000 --> 00:06:42,000
Moon is to have a deeper understanding of the nature of reality and cosmos and so on.

50
00:06:42,000 --> 00:06:53,000
We'll think of it as a way to gain special states of mind, special states of existence, to be born a way to be born in heaven, for example.

51
00:06:53,000 --> 00:06:56,000
A way to become free from reverb.

52
00:06:56,000 --> 00:07:01,000
So these are positive motivations.

53
00:07:01,000 --> 00:07:04,000
Negative motivations are what happens if you don't meditate.

54
00:07:04,000 --> 00:07:15,000
The defilements that exist to remind the problems that we're causing for other people that we're causing for our families and our family and our friends.

55
00:07:15,000 --> 00:07:27,000
The hurt that we're causing for ourselves to stress, the addiction, the suffering that we're causing for ourselves in the world around us.

56
00:07:27,000 --> 00:07:36,000
The dangers that come from allowing states to exist over the long term, the danger.

57
00:07:36,000 --> 00:07:45,000
When we get sick, how are we going to be able to deal with sickness, when we get old, how are we going to be able to get a goal?

58
00:07:45,000 --> 00:07:52,000
If we get an accident, if something terrible happens to us, how will we be able to react to deal with it?

59
00:07:52,000 --> 00:07:57,000
When we're not able to deal with simple, simple pains and stresses and so on.

60
00:07:57,000 --> 00:08:06,000
You're not able to sit for more than 20 minutes, what happens if you become invalid and have to stay in bed for days? How will you be able to manage that?

61
00:08:06,000 --> 00:08:13,000
And there's a way to help us give us a kick in the pants and allow us to see the benefits of meditation.

62
00:08:13,000 --> 00:08:22,000
I don't really think this is exactly what your problem is, but if so, and for many people, this is a great help.

63
00:08:22,000 --> 00:08:28,000
We should think about these negative and positive sides.

64
00:08:28,000 --> 00:08:37,000
There's great benefit that comes from meditation and there's great danger in non-meditating the dangers of having to be reborn again.

65
00:08:37,000 --> 00:08:43,000
I'm not knowing where you're going to be reborn because your mind is not clear.

66
00:08:43,000 --> 00:08:52,000
I hope that helps to answer your question and thanks for tuning in on the best.

