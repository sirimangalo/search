1
00:00:00,000 --> 00:00:04,000
Can someone take the same meditation course several times? Absolutely.

2
00:00:04,000 --> 00:00:08,000
It should be taken as many times as possible.

3
00:00:08,000 --> 00:00:13,000
If it's a meditation course that leads you or helps you to see reality, take it again and again and again.

4
00:00:13,000 --> 00:00:20,000
That's always a good thing.

