1
00:00:00,000 --> 00:00:06,000
Is an Arahand as enlightened as the Buddha? Should he, she not know as much as the Buddha?

2
00:00:08,000 --> 00:00:11,000
It's an interesting way of phrasing it.

3
00:00:11,000 --> 00:00:16,000
The word Arahand is not the same as the word Buddha.

4
00:00:16,000 --> 00:00:20,000
They're spelled differently for starters.

5
00:00:20,000 --> 00:00:26,000
But the point being that an Arahand is a different thing for the Buddha.

6
00:00:26,000 --> 00:00:29,000
But the Buddha was an Arahand.

7
00:00:29,000 --> 00:00:32,000
So if you have one of these, would it be those Venn diagrams?

8
00:00:32,000 --> 00:00:34,000
The Buddha?

9
00:00:42,000 --> 00:00:43,000
Right.

10
00:00:43,000 --> 00:00:45,000
And I was going to draw a Venn diagram.

11
00:00:45,000 --> 00:00:47,000
Big circle is the Arahand.

12
00:00:47,000 --> 00:00:50,000
And then inside it is a little circle of Buddhas.

13
00:00:50,000 --> 00:00:53,000
So all Buddhas are Arahans.

14
00:00:53,000 --> 00:00:56,000
Just as all sort of rectangles.

15
00:00:56,000 --> 00:00:59,000
But that's a small circle. The number of Arahans is much bigger.

16
00:00:59,000 --> 00:01:01,000
So there are many Arahans that are not Buddhas.

17
00:01:01,000 --> 00:01:06,000
But if you look at the two circles, all Buddhas are Arahans.

18
00:01:08,000 --> 00:01:11,000
So what is the difference?

19
00:01:11,000 --> 00:01:17,000
I hope I'm correct in saying this, but this is how I've often explained it.

20
00:01:17,000 --> 00:01:21,000
And maybe someone can correct me if I'm wrong.

21
00:01:21,000 --> 00:01:30,000
But my understanding of putting the difference into one sentence is that

22
00:01:30,000 --> 00:01:35,000
a Buddha has to know everything.

23
00:01:35,000 --> 00:01:41,000
An Arahand only has to let go of everything.

24
00:01:41,000 --> 00:01:46,000
So in one sentence, that's my answer is to the difference.

25
00:01:46,000 --> 00:01:51,000
And really answering your question from my understanding.

26
00:01:51,000 --> 00:01:56,000
So it depends really what you mean by enlightenment.

27
00:01:56,000 --> 00:02:02,000
If you're meaning of enlightenment, does an Arahand have more greed, anger, and delusion than a Buddha?

28
00:02:02,000 --> 00:02:03,000
No.

29
00:02:03,000 --> 00:02:05,000
Neither one has any greed, anger, and delusion.

30
00:02:05,000 --> 00:02:08,000
They both let go of everything.

31
00:02:08,000 --> 00:02:11,000
And they both come to understand the four noble truths.

32
00:02:11,000 --> 00:02:15,000
But the Buddha on top of that knows everything.

33
00:02:15,000 --> 00:02:18,000
In top of letting go of everything and being an Arahand,

34
00:02:18,000 --> 00:02:23,000
he's also come to know everything and be a Buddha.

35
00:02:23,000 --> 00:02:27,000
To clarify that, that's the one sentence answer, but to clarify it a little bit.

36
00:02:27,000 --> 00:02:30,000
It doesn't mean that the Buddha has a big, brainful of facts.

37
00:02:30,000 --> 00:02:33,000
It means that whatever question the Buddha would ask,

38
00:02:33,000 --> 00:02:35,000
say, what is the answer to this?

39
00:02:35,000 --> 00:02:38,000
He could find the answer to it, or whether there was an answer.

40
00:02:38,000 --> 00:02:42,000
He would find out the truth behind anything that he said is wind on.

41
00:02:42,000 --> 00:02:46,000
That's what it means to be a Buddha, according to our tradition.

42
00:02:46,000 --> 00:02:51,000
So what we should understand from that is it may be that we're wrong,

43
00:02:51,000 --> 00:02:58,000
that the ancient Buddha, the historic Buddha, didn't have that.

44
00:02:58,000 --> 00:03:00,000
That's not what our claim is.

45
00:03:00,000 --> 00:03:04,000
Our claim is that for someone to consider themselves a fully enlightened Buddha,

46
00:03:04,000 --> 00:03:06,000
they have to know everything.

47
00:03:06,000 --> 00:03:13,000
They have to be able to send their mind and have unobstructed knowledge and vision.

48
00:03:13,000 --> 00:03:16,000
So that anywhere they send their mind, they're able to know the truth of it.

49
00:03:16,000 --> 00:03:17,000
That's our understanding.

50
00:03:17,000 --> 00:03:18,000
We may be wrong.

51
00:03:18,000 --> 00:03:21,000
It may be that a Buddha, but that's impossible.

52
00:03:21,000 --> 00:03:23,000
But that's what the texts say.

53
00:03:23,000 --> 00:03:26,000
And that's our understanding of what it means to be a Buddha.

54
00:03:26,000 --> 00:03:31,000
It's important thing to say, because if it's true, then it means becoming a Buddha is not an easy thing.

55
00:03:31,000 --> 00:03:37,000
In comparison to the incredibly difficult thing of becoming an Arahant.

56
00:03:37,000 --> 00:03:42,000
So becoming an Arahant, giving up all greed, anger and delusion,

57
00:03:42,000 --> 00:03:45,000
a very difficult thing.

58
00:03:45,000 --> 00:03:51,000
So how much more difficult would it be to have this on top of that, this unobstructed knowledge and vision,

59
00:03:51,000 --> 00:03:57,000
so that anything you think of, anything you consider, you immediately know the answer to.

60
00:03:57,000 --> 00:04:03,000
And without any reserve, without any qualification,

61
00:04:03,000 --> 00:04:11,000
except for those things that have no answers, then you come to know there's no answer to that and so on.

62
00:04:11,000 --> 00:04:16,000
But as to whether they should, I guess it's just the grammar that you're using.

63
00:04:16,000 --> 00:04:23,000
But an Arahant who is not a Buddha will not have this unobstructed knowledge and vision.

64
00:04:23,000 --> 00:04:28,000
An Arahant who is also a Buddha will have that knowledge unobstructed knowledge and vision.

65
00:04:28,000 --> 00:04:36,000
But as for the should, if you really mean, if you're really using that as a proper for them,

66
00:04:36,000 --> 00:04:44,000
it's proper for them to learn as much as they can and to get as close to becoming fully Buddha as possible in their lifetime.

67
00:04:44,000 --> 00:04:50,000
And it's what they would do, I should think, always studying, always developing themselves.

68
00:04:50,000 --> 00:04:54,000
And their knowledge and their clarity of mind, even though they have no green anchor in delusion,

69
00:04:54,000 --> 00:05:00,000
they would just naturally incline towards that sort of thing.

70
00:05:00,000 --> 00:05:05,000
Or not, maybe they would just sit in meditation until they pass away.

71
00:05:05,000 --> 00:05:28,000
So I hope that helps.

