1
00:00:00,000 --> 00:00:06,000
I have what feels like a metal block when it comes to finding my motivation to do things.

2
00:00:06,000 --> 00:00:09,000
I love doing such as creating music.

3
00:00:09,000 --> 00:00:12,000
I'm not depressed at all and I don't take drugs.

4
00:00:12,000 --> 00:00:15,000
Will meditation help me unblock and find myself again?

5
00:00:15,000 --> 00:00:19,000
Thank you much love.

6
00:00:19,000 --> 00:00:30,000
Will meditation motivate you?

7
00:00:30,000 --> 00:00:35,000
Will meditative motivate you to...

8
00:00:35,000 --> 00:00:45,000
Will meditation create desire in your passion?

9
00:00:45,000 --> 00:00:49,000
No, no meditation won't do this.

10
00:00:49,000 --> 00:01:01,000
Unfortunately for you, unfortunately, but unfortunately in the sense that we say unfortunately in these situations,

11
00:01:01,000 --> 00:01:03,000
it'll do the opposite.

12
00:01:03,000 --> 00:01:14,000
Meditation will probably motivate you to give up and to denounce things like creating music.

13
00:01:14,000 --> 00:01:26,000
Most people don't appreciate the Dhamma, the Buddha's teaching.

14
00:01:26,000 --> 00:01:32,000
It goes perfectly counter to the world.

15
00:01:32,000 --> 00:01:36,000
It goes perfectly counter to those things that people hold.

16
00:01:36,000 --> 00:01:45,000
Ordinary people hold valuable like creating music.

17
00:01:45,000 --> 00:01:51,000
I'm not depressed at all and I do not take drugs and that is somehow significant.

18
00:01:51,000 --> 00:01:59,000
Significant because some people's motivation, lack of motivation, might be due to depression.

19
00:01:59,000 --> 00:02:02,000
But in your case, you just can't be bothered.

20
00:02:02,000 --> 00:02:08,000
Because, and I will tell you, because the reason why I'm assuming, actually,

21
00:02:08,000 --> 00:02:23,000
but I think it's a good assumption is because you're feeling the arbitrariness of your attachment.

22
00:02:23,000 --> 00:02:33,000
We don't cling to things because they're valuable.

23
00:02:33,000 --> 00:02:38,000
We cling to things because we've come to convince ourselves that they're valuable.

24
00:02:38,000 --> 00:02:47,000
Arbitrarely, completely 100% arbitrarily in the beginning that they are valuable.

25
00:02:47,000 --> 00:02:59,000
That's the profound thing that I don't think is understood well enough.

26
00:02:59,000 --> 00:03:04,000
You assume that it goes back to the human thing as well.

27
00:03:04,000 --> 00:03:08,000
You assume that liking music means that music has some intrinsic value.

28
00:03:08,000 --> 00:03:12,000
It doesn't have any intrinsic value.

29
00:03:12,000 --> 00:03:17,000
There's nothing even to indicate an intrinsic value in it.

30
00:03:17,000 --> 00:03:25,000
To understand that, though, requires you to deconstruct entirely what it means to be human,

31
00:03:25,000 --> 00:03:37,000
what it means to be a sophisticated, what it means to be happy.

32
00:03:37,000 --> 00:03:44,000
All of our presumptions and assumptions about right and wrong, natural, unnatural,

33
00:03:44,000 --> 00:03:48,000
all of these things are getting in the way of that.

34
00:03:48,000 --> 00:04:06,000
We wind up with, I believe, that music has artistic philosophical.

35
00:04:06,000 --> 00:04:14,000
The music is good for the soul, that it has value, soul value, value to the soul.

36
00:04:14,000 --> 00:04:17,000
How many constructs does it take to get there?

37
00:04:17,000 --> 00:04:28,000
You have to construct a soul which is, in turn, predicated on a presumption of entities,

38
00:04:28,000 --> 00:04:39,000
of things being able to exist outside of the momentary experience of them.

39
00:04:39,000 --> 00:04:50,000
It's predicated on, or it stems from, let me see.

40
00:04:50,000 --> 00:04:58,000
It stems from the belief that pleasure is somehow satisfying.

41
00:04:58,000 --> 00:05:06,000
That's the basic premise that is required.

42
00:05:06,000 --> 00:05:14,000
At the core of the reasons why we like music to take this example is a simple,

43
00:05:14,000 --> 00:05:24,000
apparently, a simple pleasure that comes from being able to anticipate.

44
00:05:24,000 --> 00:05:33,000
And what music does is it provides us a rhythm that we can get into.

45
00:05:33,000 --> 00:05:39,000
And this allows the, actually, I'm grasping here, but I'm pretty sure what it's doing

46
00:05:39,000 --> 00:05:44,000
is allowing the pleasure centers in the brain to fire repeatedly.

47
00:05:44,000 --> 00:05:52,000
And rhythmically, and so you get that pleasure.

48
00:05:52,000 --> 00:05:58,000
This is why you're in that, and then you suddenly get a stimulus that is unpleasant.

49
00:05:58,000 --> 00:06:03,000
So I'm listening to my music, and then my mother comes in and starts yelling at me to turn it off.

50
00:06:03,000 --> 00:06:13,000
Why that immediately infuriates me has to do with the disruption of the perception of pleasure,

51
00:06:13,000 --> 00:06:16,000
from the music, from the rhythm, actually.

52
00:06:16,000 --> 00:06:21,000
This is why jazz music, again, I'm grasping, I don't have scientific evidence for this,

53
00:06:21,000 --> 00:06:27,000
but this is an assumption that this is why jazz music is jazzy,

54
00:06:27,000 --> 00:06:31,000
because it's exciting. You're not actually able to predict.

55
00:06:31,000 --> 00:06:36,000
So you're constantly being challenged by the music to fall into that.

56
00:06:36,000 --> 00:06:43,000
Never so much that it's going to jar you out of it, but it's not quite, you know, it's playing with you.

57
00:06:43,000 --> 00:06:50,000
So it's the spice. It's no longer just pop rock or kids' music.

58
00:06:50,000 --> 00:06:58,000
It's now kind of pleasure spiked with pain, which, you know, creates tension when you can't get what you want for a second,

59
00:06:58,000 --> 00:07:05,000
and then you can get it, right? The chase leads to excitement and adrenaline.

60
00:07:05,000 --> 00:07:08,000
So it's playing with actually all of the different chemicals.

61
00:07:08,000 --> 00:07:11,000
The music is able to play with all these chemicals, but in the end that's what it is.

62
00:07:11,000 --> 00:07:22,000
Now, this has evolved into assumptions based on concepts which are totally artificial, like art, culture,

63
00:07:22,000 --> 00:07:30,000
the soul, humanity, and so on and so on.

64
00:07:30,000 --> 00:07:38,000
That has turned us into real snobs, and most people are snobbish about their music, especially musicians.

65
00:07:38,000 --> 00:07:43,000
So people who create music, I was a musician, we can get very snobbish,

66
00:07:43,000 --> 00:07:53,000
and it actually takes on an ego of its own.

67
00:07:53,000 --> 00:07:58,000
But the point that I'm making is that in the end it's all artificial. It's meaningless.

68
00:07:58,000 --> 00:08:04,000
It's nothing. Even that pleasure centers in the brain, even the fact that those are pleasurable,

69
00:08:04,000 --> 00:08:08,000
is something that is an understanding that we've acquired. It's an ignorance.

70
00:08:08,000 --> 00:08:13,000
It's based on ignorance. It's a delusion that we've acquired.

71
00:08:13,000 --> 00:08:16,000
More or less arbitrarily.

72
00:08:16,000 --> 00:08:25,000
But systematically, and as a species, we've come to agree on it for the most part.

73
00:08:25,000 --> 00:08:27,000
Nothing.

74
00:08:27,000 --> 00:08:32,000
Chanting, food is chanting. It can be sing song. Not unlike music.

75
00:08:32,000 --> 00:08:33,000
No, it shouldn't be.

76
00:08:33,000 --> 00:08:36,000
In fact, monks are not allowed to sing song. They're chanting.

77
00:08:36,000 --> 00:08:38,000
Really?

78
00:08:38,000 --> 00:08:43,000
The nowadays, you can see how it's falling apart. It's funny how more and more and more.

79
00:08:43,000 --> 00:08:46,000
You'll see crazy sing song chanting.

80
00:08:46,000 --> 00:08:48,000
Really crazy.

81
00:08:48,000 --> 00:09:00,000
It's embarrassing, really, because it's like when your friends come over and they see your father drunk or something.

82
00:09:00,000 --> 00:09:07,000
Sorry.

83
00:09:07,000 --> 00:09:13,000
When your friends come over and catch your parents drunk or misbehaving themselves,

84
00:09:13,000 --> 00:09:16,000
like I had a party or I'm not going to cry.

85
00:09:16,000 --> 00:09:21,000
No, well, we've all had that.

86
00:09:21,000 --> 00:09:24,000
That's kind of what it's like because I deal with Westerners.

87
00:09:24,000 --> 00:09:30,000
So whenever Western people come to Thailand or so on and see the monks,

88
00:09:30,000 --> 00:09:35,000
and then they get this where monks are singing, for example.

89
00:09:35,000 --> 00:09:36,000
I don't know what to say.

90
00:09:36,000 --> 00:09:38,000
It's like, do you denounce them?

91
00:09:38,000 --> 00:09:41,000
Well, you can't denounce your parents. Do you defend them?

92
00:09:41,000 --> 00:09:45,000
Well, you can't defend something that's indefensible.

93
00:09:45,000 --> 00:09:52,000
So you put in an awkward position, which is why I don't like to spend too much time in cultural monasteries.

94
00:09:52,000 --> 00:09:57,000
I hate no idea.

95
00:09:57,000 --> 00:10:03,000
So did I completely ruin your chance for asking that question?

96
00:10:03,000 --> 00:10:05,000
That's good information.

97
00:10:05,000 --> 00:10:06,000
Good to know.

98
00:10:06,000 --> 00:10:10,000
Yeah, the Buddha said singing is like whaling, is considered whaling in Buddhism.

99
00:10:10,000 --> 00:10:16,000
Dancing is considered insanity.

100
00:10:16,000 --> 00:10:23,000
And he said that monks aren't allowed to sing the chance,

101
00:10:23,000 --> 00:10:25,000
put a melody to the chance.

102
00:10:25,000 --> 00:10:27,000
He gave reasons for that.

103
00:10:27,000 --> 00:10:32,000
He said, if you chant beautifully, you become intoxicated by your voice.

104
00:10:32,000 --> 00:10:35,000
The listener becomes intoxicated by your voice.

105
00:10:35,000 --> 00:10:38,000
And there's a third, I think, reason.

106
00:10:38,000 --> 00:10:40,000
There's at least three reasons I can't remember.

107
00:10:40,000 --> 00:10:43,000
But he gave specific reasons for it.

108
00:10:43,000 --> 00:10:48,000
I always think about that, and even though I can't now remember them all.

109
00:10:48,000 --> 00:10:50,000
And it's so obvious.

110
00:10:50,000 --> 00:10:55,000
How could you, we're just deluding ourselves into thinking that somehow it's not there.

111
00:10:55,000 --> 00:10:58,000
Anytime your chanting is beautiful, what's going to happen?

112
00:10:58,000 --> 00:11:01,000
How could you not become intoxicated by it?

113
00:11:01,000 --> 00:11:03,000
How could you not become enchanted by it?

114
00:11:03,000 --> 00:11:08,000
And therefore, cultivate desire for it, attachment to it?

115
00:11:08,000 --> 00:11:11,000
How could it possibly not?

116
00:11:11,000 --> 00:11:16,000
So chanting should be actually quite, a Christian chanting is much better.

117
00:11:16,000 --> 00:11:19,000
Because while there is something kind of haunting about it,

118
00:11:19,000 --> 00:11:22,000
but, you know, Catholic monks or so on,

119
00:11:22,000 --> 00:11:27,000
their chanting is much more inspiring.

120
00:11:27,000 --> 00:11:30,000
Now, some Buddhist chanting is quite inspiring,

121
00:11:30,000 --> 00:11:34,000
and I'm just particular about which chanting that is.

122
00:11:34,000 --> 00:11:37,000
I like Sri Lankan chanting.

123
00:11:37,000 --> 00:11:39,000
Ordinary is Sri Lankan chanting.

124
00:11:39,000 --> 00:11:41,000
There's the funny stuff as well.

125
00:11:41,000 --> 00:11:43,000
They've got a real tradition now of funny stuff.

126
00:11:43,000 --> 00:11:48,000
The sing song stuff.

127
00:11:48,000 --> 00:11:53,000
But ordinary Sri Lankan chanting I like because it's quite simple.

128
00:11:53,000 --> 00:11:57,000
I guess it's my version of Sri Lankan chanting.

129
00:11:57,000 --> 00:12:01,000
I found that a two-tone chant is best.

130
00:12:01,000 --> 00:12:06,000
I tried doing a mono tone, and I'm not sure why,

131
00:12:06,000 --> 00:12:11,000
but that's harder on the voice,

132
00:12:11,000 --> 00:12:14,000
probably because of how the vocal cords work.

133
00:12:14,000 --> 00:12:18,000
Probably a high tone and a low tone use different parts of the vocal cord,

134
00:12:18,000 --> 00:12:20,000
or one of them is tensing it,

135
00:12:20,000 --> 00:12:22,000
and so you can't keep it tense that long.

136
00:12:22,000 --> 00:12:23,000
I don't know.

137
00:12:23,000 --> 00:12:26,000
But if I do two tones, it's more comfortable.

138
00:12:26,000 --> 00:12:28,000
So I'll do it.

139
00:12:28,000 --> 00:12:51,000
So it sounds almost like I'm singing it with the two tones,

140
00:12:51,000 --> 00:12:54,000
but I'm doing it for my own benefit.

141
00:12:54,000 --> 00:12:57,000
And I think that's appropriate, for example.

142
00:12:57,000 --> 00:13:01,000
I'm going to further than that.

143
00:13:01,000 --> 00:13:04,000
Anyway.

144
00:13:04,000 --> 00:13:07,000
So that's the first part of your question.

145
00:13:07,000 --> 00:13:11,000
But the assumption that you have here will meditation help me unblock?

146
00:13:11,000 --> 00:13:14,000
Let's go more general and talk about that.

147
00:13:14,000 --> 00:13:19,000
Will meditation help us to unblock our

148
00:13:19,000 --> 00:13:22,000
the things that are good about us?

149
00:13:22,000 --> 00:13:25,000
So to some extent it will.

150
00:13:25,000 --> 00:13:28,000
It will help you to straighten out your mind.

151
00:13:28,000 --> 00:13:39,000
And I would bet I'm going to have to retract something.

152
00:13:39,000 --> 00:13:42,000
Because I think in the beginning it would help you

153
00:13:42,000 --> 00:13:48,000
become more in tune with your music potentially.

154
00:13:48,000 --> 00:13:54,000
Because something is blocking those blockages.

155
00:13:54,000 --> 00:13:59,000
And that could be organic.

156
00:13:59,000 --> 00:14:01,000
Or that could be mental.

157
00:14:01,000 --> 00:14:05,000
Because it would be physical or it could be a mental block.

158
00:14:05,000 --> 00:14:08,000
Meditation will help to sort out those blocks.

159
00:14:08,000 --> 00:14:11,000
Put you more in tune with your desire for music,

160
00:14:11,000 --> 00:14:13,000
your attachment for music, your love for music.

161
00:14:13,000 --> 00:14:16,000
It will put you more in tune with those core,

162
00:14:16,000 --> 00:14:22,000
strong emotions and desires that you have.

163
00:14:22,000 --> 00:14:24,000
That doesn't mean those desires are in any way good or useful.

164
00:14:24,000 --> 00:14:27,000
So everything I said is still valid.

165
00:14:27,000 --> 00:14:32,000
Except where I said that it will immediately distance you from.

166
00:14:32,000 --> 00:14:33,000
No, it won't.

167
00:14:33,000 --> 00:14:40,000
In the beginning, in fact, it will may actually increase your desire for music.

168
00:14:40,000 --> 00:14:43,000
Unfortunately.

169
00:14:43,000 --> 00:14:47,000
Unfortunately, because that's going to then lead you on the wrong path.

170
00:14:47,000 --> 00:14:52,000
But, you know, eventually then you'll come back to where you are again.

171
00:14:52,000 --> 00:14:54,000
Potentially come back to write where you are again.

172
00:14:54,000 --> 00:15:02,000
Because the music will, in the end, hurt you.

173
00:15:02,000 --> 00:15:03,000
That's the word.

174
00:15:03,000 --> 00:15:10,000
It'll dull your mind to the extent that you can no longer lose your passion again.

175
00:15:10,000 --> 00:15:16,000
Anyway, the meditation helps you get in touch with these things.

176
00:15:16,000 --> 00:15:26,000
Because you're no longer cultivating the qualities that you're no longer as complicated.

177
00:15:26,000 --> 00:15:27,000
You're much more streamlined.

178
00:15:27,000 --> 00:15:30,000
The brain becomes more streamlined, more efficient.

179
00:15:30,000 --> 00:15:35,000
And the mind also becomes more streamlined and more efficient.

180
00:15:35,000 --> 00:15:37,000
But finding yourself again.

181
00:15:37,000 --> 00:15:41,000
So will meditation help me unblock and find myself again?

182
00:15:41,000 --> 00:15:47,000
Yes, I think it will also help you find yourself in a symbolic way.

183
00:15:47,000 --> 00:15:52,000
Doesn't mean you will find your soul, but it will help you to center yourself.

184
00:15:52,000 --> 00:15:54,000
And it won't be again.

185
00:15:54,000 --> 00:15:55,000
It will be a new.

186
00:15:55,000 --> 00:16:00,000
And if you keep practicing meditation, it will help you see things that you didn't realize.

187
00:16:00,000 --> 00:16:08,000
That in fact, your love of music, to some extent, probably ironically,

188
00:16:08,000 --> 00:16:14,000
was instrumental, partially instrumental, in leading you away from music.

189
00:16:14,000 --> 00:16:19,000
Because your love of music will pollute your mind and will dull in your mind

190
00:16:19,000 --> 00:16:25,000
to the extent that you can no longer produce the music that your pure and fresh mind was able to do.

191
00:16:25,000 --> 00:16:30,000
That's not necessarily the case in your situation,

192
00:16:30,000 --> 00:16:40,000
but quite often and quite potentially, it's a real potential.

193
00:16:40,000 --> 00:16:50,000
And if not directly, then the livelihood involved with the life involved with creating music,

194
00:16:50,000 --> 00:16:58,000
the drugs, the alcohol, the sex, the debauchery that's generally associated with it.

195
00:16:58,000 --> 00:17:06,000
Not always, but often will certainly play a part, in my case anyway,

196
00:17:06,000 --> 00:17:10,000
play a part, and your inability to be as creative as you used to be.

197
00:17:10,000 --> 00:17:19,000
People who are truly creative have to have a very pure and sharp and profound mind.

198
00:17:19,000 --> 00:17:24,000
I guess a good example, and it's just grasping at straws, would be Bob Dylan,

199
00:17:24,000 --> 00:17:30,000
who is one of my big, one of the guys I always looked up to.

200
00:17:30,000 --> 00:17:34,000
I'm not certainly the only one.

201
00:17:34,000 --> 00:17:38,000
But it's interesting, if you hear what he has to say about himself,

202
00:17:38,000 --> 00:17:41,000
he says he can't do the things he used to do before.

203
00:17:41,000 --> 00:17:44,000
I mean, I guess that part of it has to do with age as well.

204
00:17:44,000 --> 00:17:47,000
But when he was young, he just had such a...

205
00:17:47,000 --> 00:17:49,000
He didn't even know where it was coming from,

206
00:17:49,000 --> 00:17:59,000
but he was just able to come up with the most incredible creativity that eventually went.

207
00:17:59,000 --> 00:18:04,000
He said it went not dry, but it disappeared, it faded away,

208
00:18:04,000 --> 00:18:08,000
and he was no longer able to do that.

209
00:18:08,000 --> 00:18:13,000
And so an argument could be made for the actual lifestyle that it led to,

210
00:18:13,000 --> 00:18:17,000
a lot of drugs and alcohol,

211
00:18:17,000 --> 00:18:23,000
or I don't know how much, but just the attachment to sensuality involved with it.

212
00:18:23,000 --> 00:18:26,000
Anyway, not to say anything about him particularly.

213
00:18:26,000 --> 00:18:29,000
Maybe I shouldn't actually pick people out like that,

214
00:18:29,000 --> 00:18:35,000
but that argument could be made.

215
00:18:35,000 --> 00:18:38,000
So it will help you hopefully define yourself anew

216
00:18:38,000 --> 00:18:41,000
and to realize some things you didn't realize about music

217
00:18:41,000 --> 00:18:44,000
and the fact that it is an artificial construct

218
00:18:44,000 --> 00:18:47,000
and not really of any intrinsic benefit to you.

219
00:18:47,000 --> 00:19:14,000
It's not actually making you happy.

