1
00:00:00,000 --> 00:00:06,000
Yes, as I'm getting a regular heartbeat during and after meditation.

2
00:00:06,000 --> 00:00:11,500
Do I just note it and what would be the possible causes?

3
00:00:11,500 --> 00:00:15,400
I like to just note it. I get that sometimes.

4
00:00:15,400 --> 00:00:20,500
But you know, some people say that it can be a sign that you have a heart problem.

5
00:00:20,500 --> 00:00:29,800
I have one meditator friend in California who has a heart problem, a heart irregularity.

6
00:00:29,800 --> 00:00:35,800
So, you know, he started taking medication for it.

7
00:00:35,800 --> 00:00:39,300
But he admitted that it's probably due to stress.

8
00:00:39,300 --> 00:00:42,800
Probably his job is, he knows his job is causing him stress.

9
00:00:42,800 --> 00:00:44,800
And, you know, it's due to his physique as well.

10
00:00:44,800 --> 00:00:46,800
Probably my physique as well.

11
00:00:46,800 --> 00:00:48,800
It can be due to the fact that you don't eat.

12
00:00:48,800 --> 00:00:53,300
I mean, these are all questions that I really shouldn't be answers that I shouldn't be giving,

13
00:00:53,300 --> 00:00:57,300
because I'm not a doctor, but a lot of it just has to do with your physique.

14
00:00:57,300 --> 00:01:03,800
If I assume your question has something with the idea whether meditation can be causing your regularity.

15
00:01:03,800 --> 00:01:11,300
And I think, yes, because of the stress that can be caused by meditation as you're stirring stuff up and learning.

16
00:01:11,300 --> 00:01:24,800
Meditation is like a laboratory, and so all of the aspects of your existence are put under the microscope.

17
00:01:24,800 --> 00:01:27,800
And so if you have a heart problem, it's going to be looked at under the microscope.

18
00:01:27,800 --> 00:01:29,300
That might kill you.

19
00:01:29,300 --> 00:01:30,300
You might die.

20
00:01:30,300 --> 00:01:31,800
I mean, it rare.

21
00:01:31,800 --> 00:01:36,300
I've never heard of it, but the point being that this is real.

22
00:01:36,300 --> 00:01:41,300
You're looking at reality, and so it could, you know, it could be that you have a heart problem.

23
00:01:41,300 --> 00:01:47,800
And it could be that by saying feeling, feeling, or even acknowledging the emotions and so on.

24
00:01:47,800 --> 00:01:53,800
That, oh, it's so doubtful because meditation is so good for the heart, right?

25
00:01:53,800 --> 00:02:04,800
But it could be that by, no, it could be that by messing around and muddling around and not practicing fully,

26
00:02:04,800 --> 00:02:10,800
because you can actually, in the beginning, create stress that it could aggravate your physical condition.

27
00:02:10,800 --> 00:02:17,800
I mean, meditation can actually, there's a story in the commentaries about a monk who wasn't eating enough

28
00:02:17,800 --> 00:02:22,800
and was doing too much walking meditation and ended up snapping his spinal cord or something.

29
00:02:22,800 --> 00:02:29,800
And nerves in his spinal cord just gave away because he wasn't, he was undernourished and he died doing walking meditation.

30
00:02:29,800 --> 00:02:30,800
He was born as an angel.

31
00:02:30,800 --> 00:02:35,800
That's the good news is he was born as an angel because he was doing walking meditation.

32
00:02:35,800 --> 00:02:44,800
Apparently, if you die, if you die when meditating, there's no, there's no possible outcome except for going to heaven.

33
00:02:44,800 --> 00:02:46,800
I don't think that's completely true.

34
00:02:46,800 --> 00:02:51,800
The commentary says something like that, but if you, if you've done horrible things, you can't go to heaven.

35
00:02:51,800 --> 00:02:58,800
So, if you're, it must just be much easier to go to heaven, which makes sense.

36
00:02:58,800 --> 00:02:59,800
I'm meditating.

37
00:02:59,800 --> 00:03:28,800
Here's a question going back to what you were talking about earlier.

