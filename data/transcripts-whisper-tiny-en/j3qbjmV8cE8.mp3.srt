1
00:00:00,000 --> 00:00:05,160
What do you think about crying? The Buddhist view about crying. Sometimes I think

2
00:00:05,160 --> 00:00:10,160
about death of myself and my loved ones and I get terrified. Does the Buddha's

3
00:00:10,160 --> 00:00:15,120
teaching? Is the Buddha's teaching to watch such hard experiences in life without

4
00:00:15,120 --> 00:00:20,600
judging? Seems hard to apply. The Buddha's teaching in general is hard to

5
00:00:20,600 --> 00:00:27,480
apply. It's for most of us. It's not an easy thing. So that's a red herring.

6
00:00:27,480 --> 00:00:36,120
Don't write anything off as being hard to apply because impossible to apply

7
00:00:36,120 --> 00:00:43,800
or in applicable would be something else but hard to apply doesn't apply.

8
00:00:43,800 --> 00:00:49,200
Thinking about death, that's a good thing. A good thing helps us to overcome

9
00:00:49,200 --> 00:00:53,280
the state of being terrified. So you say it makes you terrified. Well that's a

10
00:00:53,280 --> 00:01:00,000
sign for you that you have clinging. You have attachment to those things that

11
00:01:00,000 --> 00:01:04,680
you're terrified about of losing because death isn't terrifying. It's something

12
00:01:04,680 --> 00:01:11,000
that comes to all beings. It arises. It ceases. It's inevitable. But the fact

13
00:01:11,000 --> 00:01:15,600
that you're terrified about something is that you isn't an aversion to that

14
00:01:15,600 --> 00:01:21,920
thing happening in this case losing loved one. So you have the Buddha's

15
00:01:21,920 --> 00:01:27,360
teaching Biototayatayasoko that which is dear from that which is dear comes

16
00:01:27,360 --> 00:01:35,160
sadness or in this case crying. The Buddhist view on crying is twofold. In some

17
00:01:35,160 --> 00:01:39,240
cases it can be rapture based. So people who practice meditation can get so

18
00:01:39,240 --> 00:01:45,560
enthralled with the practice that they start to cry without any reason. But in

19
00:01:45,560 --> 00:01:50,480
this case it sounds like crying with a reason which is a product of sadness

20
00:01:50,480 --> 00:01:56,200
and unhostedness. Either way the physical manifestation of tears is I'm pretty

21
00:01:56,200 --> 00:01:59,360
sure and I keep telling people this so I'm hoping the science is correct is

22
00:01:59,360 --> 00:02:06,040
accompanied by endorphins. So which are some kind of happy drug. They actually

23
00:02:06,040 --> 00:02:11,640
make you happy and so they're soothing. And so it's evolutionary. That's

24
00:02:11,640 --> 00:02:19,400
something we've evolved to allow us to keep us from being

25
00:02:19,400 --> 00:02:24,960
paralyzed when pain or suffering comes to us. As a result it's quite

26
00:02:24,960 --> 00:02:36,240
addictive and it becomes a response to unpleasant situations. And in a way

27
00:02:36,240 --> 00:02:42,800
that is actually addictive in a way that we actually use to feel happy to

28
00:02:42,800 --> 00:02:47,480
be feel pleasure. And so it's quite negative. It can be quite negative.

29
00:02:47,480 --> 00:02:55,320
Crying itself is neutral. But the cultivation of crying where it becomes a

30
00:02:55,320 --> 00:02:59,400
habit is quite negative because it's easy to be because we become addicted to

31
00:02:59,400 --> 00:03:03,560
the actually pleasant states that come from crying. People say crying is a

32
00:03:03,560 --> 00:03:09,120
sad is an unpleasant thing. Crying is actually quite a pleasant thing for

33
00:03:09,120 --> 00:03:13,040
whatever reason to arise is because of the physical pleasure that comes from

34
00:03:13,040 --> 00:03:17,800
the drugs that you're feeding yourself. So important to watch that when you

35
00:03:17,800 --> 00:03:23,600
cry, you should remind yourself crying crying. When you feel the pleasure of

36
00:03:23,600 --> 00:03:26,880
the chemicals that the endorphins or whatever they are that come from

37
00:03:26,880 --> 00:03:31,880
crying, you should say feeling feeling or happy, happy or so. If you like it

38
00:03:31,880 --> 00:03:35,760
you should say liking, liking and that's all. It's something that comes in

39
00:03:35,760 --> 00:03:40,720
goes crying itself. It's not big deal. Sadness is a problem and of course

40
00:03:40,720 --> 00:03:44,440
attachment to the crying is a problem. These are things that should be seen as

41
00:03:44,440 --> 00:04:11,800
they are and discarded as an essential.

