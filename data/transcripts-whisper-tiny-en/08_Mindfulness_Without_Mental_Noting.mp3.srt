1
00:00:00,000 --> 00:00:07,300
Mindfulness, without mental noting, question, I have been practicing mindfulness without

2
00:00:07,300 --> 00:00:14,280
mental noting and focusing on the sensations, thoughts, etc. For me it is easier to be

3
00:00:14,280 --> 00:00:21,920
mindful this way. What is your opinion on this? Answer, nobody wants to note, nobody wants

4
00:00:21,920 --> 00:00:28,160
to put words in their heads, why? Because it is difficult. You have to ask yourself,

5
00:00:28,160 --> 00:00:33,680
what is the purpose of meditation? Is the purpose of meditation to make things easy? Or

6
00:00:33,680 --> 00:00:39,680
is the purpose in meditation to overcome what is difficult? Some people make the observation

7
00:00:39,680 --> 00:00:45,280
that noting makes it more difficult to recognize the object for what it is, because

8
00:00:45,280 --> 00:00:51,760
noting forces you to see in permanence, suffering and non-so. It forces you to see that

9
00:00:51,760 --> 00:00:57,760
things are not as you would like them. It forces you to see your mind's inclination to control

10
00:00:57,760 --> 00:01:04,480
things and then be unable to control them. Because of the regimented nature of this meditative

11
00:01:04,480 --> 00:01:11,120
practice, you are not able to have this smooth mindfulness, which I would just call concentration.

12
00:01:11,120 --> 00:01:16,960
The word mindfulness can be used so loosely as to become rather meaningless. It is not a good

13
00:01:16,960 --> 00:01:23,520
translation of the word sati, which is what we are practicing. The word sati means specifically

14
00:01:23,520 --> 00:01:29,520
the recognition of something as it is. It is caused by something called tirasana,

15
00:01:29,520 --> 00:01:36,000
sana means the recognition of something. This is this, this is that and tira means firm

16
00:01:36,000 --> 00:01:42,640
strong or fortified. Sati means fortifying the recognition of the object for what it is,

17
00:01:42,640 --> 00:01:48,240
and this is why we use the noting technique. The problem is that we do not like it,

18
00:01:48,240 --> 00:01:54,320
noting breaks up the continuity of the mind's incarnations, a stream of habitual clinging to

19
00:01:54,320 --> 00:02:01,360
conceit, craving, and to the ideas also are disrupted. The whole idea that something is easier for

20
00:02:01,360 --> 00:02:08,880
me implies a preference and possibly an identification with the experience that it is me meditating.

21
00:02:08,880 --> 00:02:15,280
It is me being mindful. The practice of noting results in you feeling like you are forcing

22
00:02:15,280 --> 00:02:20,960
the experience. The experience feels jarring, chaotic, and uncontrollable, which is the

23
00:02:20,960 --> 00:02:28,080
actual reality of it. Noting forces you to see that. Noting itself is not uncomfortable. It is

24
00:02:28,080 --> 00:02:33,280
the discord between the way we would like things to be and the way things really are that feels

25
00:02:33,280 --> 00:02:39,360
unpleasant. Once you come to see things that they are moment to moment and that they do not carry

26
00:02:39,360 --> 00:02:45,360
on from one moment to the next and noting becomes a very easy thing to do. Catching things

27
00:02:45,360 --> 00:02:52,160
up in your attention one by one becomes not true because you are able to see an acceptorality

28
00:02:52,160 --> 00:03:00,960
just as it is without continuous entities only moment to moment experiences. Our ordinary perception

29
00:03:00,960 --> 00:03:06,720
is of things that continue from moment to moment because the mind does not find such things in

30
00:03:06,720 --> 00:03:12,320
practice. It becomes quite disturbed by the simple observation of moments of experience.

31
00:03:12,960 --> 00:03:19,280
Instead of perceiving the momentary experiences for what they are, it attempts to create stability

32
00:03:19,280 --> 00:03:25,760
by force and it may feel like the noting itself is a useful force but in actuality it is just

33
00:03:25,760 --> 00:03:32,560
our misguided desire for stability and control. In my opinion, practice without noting

34
00:03:32,560 --> 00:03:38,880
is not truly sadhi. Why? Because the word sadhi means to remind yourself, sadhi means the firm

35
00:03:38,880 --> 00:03:45,680
recognition that comes from reminding yourself of the object for what it is. The Buddha said

36
00:03:45,680 --> 00:03:54,400
kachan-go-wa-gachami-ti-pajan-ati, which means when walking one nose, I am walking kachami means walking.

37
00:03:54,400 --> 00:04:00,560
Without emphasizing the eye or the mean, it is the simplest way to focus one's attention on the

38
00:04:00,560 --> 00:04:07,520
object. Jana-ti means one nose and the power prefix means fully, which implies an exceptional

39
00:04:07,520 --> 00:04:14,080
sort of knowing beyond the ordinary sense that it is me who is walking. Instead of noting,

40
00:04:14,080 --> 00:04:20,000
I try to use the word mantra to describe this practice. Mantra is the word that we already know.

41
00:04:20,000 --> 00:04:26,560
Mantra meditation is a very ancient practice as a widely accepted means of focusing the mind's

42
00:04:26,560 --> 00:04:33,760
attention on an object. Ordinarily, however, the object of mantra is a concept because concepts

43
00:04:33,760 --> 00:04:41,120
are stable, satisfying and controllable. In mind, finesse meditation, our focus is on experiential

44
00:04:41,120 --> 00:04:48,400
reality, and because experiences are neither stable, satisfying, uncontrollable, it can be a source

45
00:04:48,400 --> 00:04:54,640
of some distress to the mind, seeking out those qualities. The fact that you experience

46
00:04:54,640 --> 00:05:00,560
impermanence, suffering, and non-self when applying a mantra to things that are impermanent,

47
00:05:00,560 --> 00:05:07,280
suffering, and non-self is a sign that you are practicing correctly. Feeling uncomfortable as a result

48
00:05:07,280 --> 00:05:13,440
of seeing these characteristics of reality is an important part of the training to take you out of

49
00:05:13,440 --> 00:05:19,120
your comfort zone, helping you to release your dependency on things that are really not dependable.

50
00:05:19,680 --> 00:05:25,760
From our point of view, it is forcing you to look at things objectively. If you are interested

51
00:05:25,760 --> 00:05:32,640
in seeing reality clearly as it is, I can only recommend that you go practice noting the objects.

52
00:05:32,640 --> 00:05:38,160
If you prefer something more peaceful, calm, and soothing that takes you away from

53
00:05:38,160 --> 00:06:08,000
ordinary reality, then you may have to look elsewhere.

