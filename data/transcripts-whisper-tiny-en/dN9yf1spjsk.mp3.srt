1
00:00:00,000 --> 00:00:10,860
Hi, so this is the inception of my new series, Ask a Monk, and this series, the reason

2
00:00:10,860 --> 00:00:19,460
for it is that I get a lot of questions on the internet, either via YouTube or through

3
00:00:19,460 --> 00:00:24,940
my personal email, about the meditation practice, and I'm really not able to answer all

4
00:00:24,940 --> 00:00:33,240
of them, as I might like. So I thought this might be a sort of a good way to sort of

5
00:00:33,240 --> 00:00:40,140
address that problem, not having to answer every single question, but sort of picking

6
00:00:40,140 --> 00:00:46,600
and choosing and giving more public answers so that they're a benefit to wider audience.

7
00:00:46,600 --> 00:00:51,160
So this is the first question that just happens to be the one I got most recently, and

8
00:00:51,160 --> 00:00:56,120
that is the state you're trying to reach is something like where you focus on your breath

9
00:00:56,120 --> 00:01:00,480
to the point where you're hardly breathing. Well, to stop there first of all, that's not

10
00:01:00,480 --> 00:01:06,360
the point of meditation practice, as I've mentioned in other videos, such things of the

11
00:01:06,360 --> 00:01:13,880
point where you're hardly breathing are just result of concentration, tranquility, a certain

12
00:01:13,880 --> 00:01:21,680
state of mind that brings about this quietude, where you no longer feel the breath or

13
00:01:21,680 --> 00:01:30,040
the breath is no longer a course. It can even stop if you get very deep enough. There's

14
00:01:30,040 --> 00:01:34,720
certainly isn't the practice and it should be acknowledged as with everything else.

15
00:01:34,720 --> 00:01:40,120
Is it normal if I can do this almost instantly? Well, the word normal is a tricky word

16
00:01:40,120 --> 00:01:47,120
for most cases. I wouldn't even know what was normal. If we're talking about what is

17
00:01:47,120 --> 00:01:51,320
a normal human being or what is a normal state of mind, there really isn't one, and there's

18
00:01:51,320 --> 00:01:57,480
such a variety of experience that it's not really possible to address that. In fact,

19
00:01:57,480 --> 00:02:01,520
you can do it almost instantly. It does say something about your state of concentration.

20
00:02:01,520 --> 00:02:06,120
It means that you have strong concentration, which can be nice and good, but you should

21
00:02:06,120 --> 00:02:13,040
be careful not to attach to it or think that somehow that makes it special and you should

22
00:02:13,040 --> 00:02:17,720
be able to let go of it and move on and accept everything when you have the state or when

23
00:02:17,720 --> 00:02:22,720
you don't have a state. I also get a funny feeling in the bridge of my nose and forehead

24
00:02:22,720 --> 00:02:28,440
when I reach this state. Is that normal? That's normal for this state. If you have a state

25
00:02:28,440 --> 00:02:33,760
where you focus on your breath to the point where you're hardly breathing, it's quite

26
00:02:33,760 --> 00:02:39,520
likely that the intensity of the concentration will give pressure in the head, funny feelings

27
00:02:39,520 --> 00:02:46,400
as you say. It can create all sorts of strange experiences. It can even lead to supernatural

28
00:02:46,400 --> 00:02:52,760
experiences, out of body experiences and so on. Again, none of this is what I teach or what

29
00:02:52,760 --> 00:02:58,720
I would expect someone to strive for in the practice. I would encourage this person and

30
00:02:58,720 --> 00:03:03,640
anyone who's in a similar situation. There's a lot of questions like this. What do I do

31
00:03:03,640 --> 00:03:08,080
when this happens or is this normal, is that normal? It's just acknowledge it. See it for

32
00:03:08,080 --> 00:03:13,600
what it is and go on and move on and don't get excited about it or worried about it or

33
00:03:13,600 --> 00:03:21,680
so on. See it for what it is and go back to your meditation practice and I'd like you

34
00:03:21,680 --> 00:03:28,680
to click on the link if you're interested in more on this topic of what is and what is not

35
00:03:28,680 --> 00:03:33,960
the practice. So thanks for tuning in and please leave your questions, other questions

36
00:03:33,960 --> 00:03:39,320
in the form of comments or to personal messages or even to my email address and I'll

37
00:03:39,320 --> 00:03:45,880
try to answer the ones I can and the ones I think are relevant and useful for a large

38
00:03:45,880 --> 00:04:02,280
number of people. Okay, thanks. Have a good day.

