Okay, so here we are at the new forest monastery. Here's St. Anne's Minnetoba. Here is the
house. We're staying at. We've got a little deck over on the side there covered
in snow. And then we're surrounded by trees, snow, and the bright blue sky.
Over here we have two sheds. This one on the left here is quite well built.
It's really new. This could be a good day in the future. The one on the right, not so sure,
I haven't been inside it. And the garage, which is probably where I'm going to be living hopefully,
hopefully, is a bit poorly made and falling apart and so on. So hopefully we can get that fixed up,
at least somewhat. And here's the driveway leading down to the main road.
As you can see, it's quite long. It keeps us included from our neighbors.
We're surrounded by trees and then there's this one big tree. I don't know what it is in the yard right by the house.
And that's about it. As you can see, we're in a bit of a clearing here, so we are definitely in the forest.
So now I'll take you inside and we'll take a look at the actual house.
So first thing you see when you get in the house is this strange looking staircase,
right? That's one of the door. Beside it, there is a small bedroom.
All of the heating is baseboard heating. Nice new windows.
And then up the stairs, you'll see sort of a partial room.
A partial, I say, because the roof is not quite full or gives you not full,
quite full space. This strange little look here.
Then you come around here and you see a nice view over the driveway.
And we're like, it's livable, no? Maybe a little small.
You can do walking meditation if you think about it here.
You can walk all the way to the end, which is enough space.
So that's two bedroom spaces so far.
And then over here, under the stairs, we have a closet, which has the circuit,
not a switchboard. You call that thing that has all the breakers.
And here's another room, another little closet space.
From here, we have the living room.
So a fairly large size, good for holding toxins and so on.
So we are meeting space, good for doing walking meditation, etc.
Here we have bedroom number three, which is, I guess, the master bedroom.
It's where I'll be staying in a couple of nights until we get the garage fixed up.
It's got a closet on the left here.
And something that probably should have been a bathroom, but isn't, so now it's just a closet.
For now.
Through here, we come to bedroom number, no.
I come to bathroom number one.
There's only one bathroom in the house, which is probably going to be the Achilles heel.
For the time being until we can remedy that, maybe build an outhouse or something.
That's it.
Well, functional, I hope.
Here's the kitchen.
This is where I just had my first lunch.
Here alone today, which is nice.
It's nice to be out in the forest alone again.
With the time being, tomorrow is going to be full of people.
So, this is bedroom number, and last count.
This is number five, because there are five of them.
So we have one upstairs, two over by the stairs.
One off the living room is four, and this is number five off the kitchen.
Here we have a little pantry area, and the closet, and a door.
So, there are two entrances, which is always nice.
It means we can walk this, which means people can come into the kitchen.
They don't have to disturb the meditators.
And here's that little deck.
So we can't go out too, because of the snow.
We don't have to come from that.
Anyway, that's it. That is our new monastery.
The Baridaha Forest monastery.
Anyway, I hope you've enjoyed this tour, and look forward to seeing you all here in the future.
All the best.
