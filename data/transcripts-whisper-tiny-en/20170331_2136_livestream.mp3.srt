1
00:00:00,000 --> 00:00:05,000
understand the reasons or the, you know,

2
00:00:05,000 --> 00:00:08,000
to understand the professors.

3
00:00:16,000 --> 00:00:18,000
But the whole system is flawed.

4
00:00:18,000 --> 00:00:21,000
The university system has many flaws.

5
00:00:21,000 --> 00:00:25,000
It's not, in my mind, it's not this wonderful thing

6
00:00:25,000 --> 00:00:27,000
that people make it out to me.

7
00:00:27,000 --> 00:00:31,000
It's a cause for a lot of stress and a lot of useless

8
00:00:31,000 --> 00:00:35,000
wasting of time, and a lot of conditioning

9
00:00:35,000 --> 00:00:39,000
that's actually harmful.

10
00:00:39,000 --> 00:00:44,000
I don't think it's really a pinnacle of learning

11
00:00:44,000 --> 00:00:46,000
that everyone makes out to me.

12
00:00:46,000 --> 00:00:48,000
You do learn a lot.

13
00:00:48,000 --> 00:00:51,000
You do learn things like how to, how to speak,

14
00:00:51,000 --> 00:00:56,000
how to read, how to study, but it's inefficient.

15
00:00:56,000 --> 00:01:01,000
It's expensive, and there's a lot of ego involved,

16
00:01:01,000 --> 00:01:05,000
as I said, a lot of stress involved.

17
00:01:31,000 --> 00:01:50,000
I mean, I think I'm in agreement with those professors who don't.

18
00:01:50,000 --> 00:01:52,000
We had one professor who said, you know,

19
00:01:52,000 --> 00:01:56,000
I have to grade you guys, but it's not really the point of this course.

20
00:01:56,000 --> 00:02:00,000
And so she actually gave us, I think we all got fairly good grades

21
00:02:00,000 --> 00:02:01,000
because she just didn't care.

22
00:02:01,000 --> 00:02:18,000
She said, and I'm here to do.

