1
00:00:00,000 --> 00:00:22,560
Good evening, everyone broadcasting live July 3, 2016 as usual.

2
00:00:22,560 --> 00:00:40,360
Today's quote, this quote is about, would he, would he mean growth or progress and expansion

3
00:00:40,360 --> 00:00:55,880
increase, would he, would he want, would he, would one have means to get to grow to, to grow,

4
00:00:55,880 --> 00:00:56,880
to increase?

5
00:00:56,880 --> 00:01:07,900
So we're talking about the area one day, area means noble, so the noble, the betterment,

6
00:01:07,900 --> 00:01:12,920
noble growth I guess, it just sounds weird because we use the word growth to mean something

7
00:01:12,920 --> 00:01:32,280
else, but to grow to, to progress, spiritual, spiritual growth.

8
00:01:32,280 --> 00:01:36,640
And curiously, it's directed towards a woman, no, I'm not sure what that's all about.

9
00:01:36,640 --> 00:01:43,240
It doesn't use the word woman, but it uses the feminine forms of the word sawika, sawika

10
00:01:43,240 --> 00:01:52,240
means, means a female disciple, so you think these might be a little gender stereotyped.

11
00:01:52,240 --> 00:02:09,540
Well, I don't see that uses the word opacity as well, so it is, it is directed towards women.

12
00:02:09,540 --> 00:02:15,140
So maybe he was talking to a bunch of women, could have been, he uses the word bikave,

13
00:02:15,140 --> 00:02:24,720
which is masculine, so he's talking to, but that doesn't really mean anything, no, suppose

14
00:02:24,720 --> 00:02:34,020
it does, and be a stretch to say humans talking to women here, probably talking to the monks,

15
00:02:34,020 --> 00:02:45,900
talking about women, which is, oh, yeah, because this is the mantu gama, which is a

16
00:02:45,900 --> 00:02:54,660
discursive about mothers, about house mothers, which means household women, of course

17
00:02:54,660 --> 00:03:03,240
is, I guess, a favorite, subject of monks, probably not, there are things to be said, how

18
00:03:03,240 --> 00:03:15,580
to relate, how, how male, celibate, monks, heterosexual, celibate, males, should relate to the

19
00:03:15,580 --> 00:03:21,380
women that they have to come in contact with, because for some time there is only men,

20
00:03:21,380 --> 00:03:32,020
there is only male monks, then he read, as I said, I don't think this is terribly gender

21
00:03:32,020 --> 00:03:42,500
specific, these five things, sadha, sadha, sanda, sila, supta, jagapanya, I'm a pretty

22
00:03:42,500 --> 00:03:52,380
standard, so we can treat this just as five, five dumb ones that we should all cultivate,

23
00:03:52,380 --> 00:04:00,340
we cultivate these, it leads to our spiritual well-being, our spiritual growth, increase

24
00:04:00,340 --> 00:04:22,340
in all things spiritual, allow us to grasp the, grasp the sara, the sara, daya, sara daya,

25
00:04:22,340 --> 00:04:35,340
and grasp the essence to grasp the highest, that which is special, excellent, so sadha

26
00:04:35,340 --> 00:04:42,780
means we try to have confidence, we cultivate confidence not only in ourselves, but in the

27
00:04:42,780 --> 00:04:51,460
practice, not only in the practice, I suppose, but also in the Buddha, mainly in the practice

28
00:04:51,460 --> 00:04:57,660
if you want to streamline it, the most important confidence you have is in the path that

29
00:04:57,660 --> 00:05:02,700
you're following, of course it helps to think that the Buddha knew what he was teaching,

30
00:05:02,700 --> 00:05:10,380
but you don't have to quite go that, you don't have to necessarily go that abstract,

31
00:05:10,380 --> 00:05:18,820
you can take the practice for what it is as long as you are clear about the benefits

32
00:05:18,820 --> 00:05:26,460
of it, and our focus on the goodness of it, is why I tell people not to, not to tell

33
00:05:26,460 --> 00:05:32,940
students, not to worry about success or progress, am I doing better than I was yesterday

34
00:05:32,940 --> 00:05:42,220
and is this meditation helping me as well, don't look at it in those terms, look at

35
00:05:42,220 --> 00:05:48,340
it as it happens, when you're meditating, when you have a clear mind, look at the quality

36
00:05:48,340 --> 00:05:55,420
of that mind, and remind yourself of how pure the mind is when you're objective, and

37
00:05:55,420 --> 00:06:05,500
how it solves the situation, it resolves the problems that arise, the challenges that

38
00:06:05,500 --> 00:06:13,940
come, the conflicts that arise in the mind, it resolves them and see how it does that,

39
00:06:13,940 --> 00:06:18,380
and then you don't have to worry about what the results are going to mean, because clearly

40
00:06:18,380 --> 00:06:24,340
that's a positive change in your life, and building up that habit is, of course, going

41
00:06:24,340 --> 00:06:29,140
to bring good things, there's no question that it might bring bad things, and so you just

42
00:06:29,140 --> 00:06:35,620
be patient, let the good things come rather than checking and seeing how they come in,

43
00:06:35,620 --> 00:06:41,420
and they come in and they haven't come giving rise to irrational doubt, I think it's irrational

44
00:06:41,420 --> 00:06:47,580
because if you look at what you're doing, you're doing something positive, if you want real

45
00:06:47,580 --> 00:06:51,820
confidence, focus on what you're doing, are you doing something positive and don't worry

46
00:06:51,820 --> 00:06:59,100
about the outcome, you should be very confident if you're doing something positive,

47
00:06:59,100 --> 00:07:09,260
you're doing something that's purely good for you and good for others? Seela, we need to have

48
00:07:09,260 --> 00:07:19,620
ethics and we want to reach the essence, we want to get to the core of the truth of the

49
00:07:19,620 --> 00:07:26,980
nature of reality, we need some kind of ethical guidelines to keep you sober, to keep

50
00:07:26,980 --> 00:07:37,140
you sane, to keep your mind settled and balanced so that you're not overcome by negative

51
00:07:37,140 --> 00:07:51,820
emotions, of inventions and fear and mistrust, distrust, let me feel that, let me show

52
00:07:51,820 --> 00:08:01,100
you and kill, steal, lie, cheat, take drugs or alcohol, we shouldn't gamble, we shouldn't

53
00:08:01,100 --> 00:08:13,260
engage in unethical practices, unethical business practices, etc. It will help our minds

54
00:08:13,260 --> 00:08:24,260
focus if we avoid these things, we feel more confident in ourselves. Number three,

55
00:08:24,260 --> 00:08:32,620
sootah, we have to learn, sootah means listening, that which is heard, which should be

56
00:08:32,620 --> 00:08:40,300
full of that, which is heard, so knowledge, it's back then they would all be password

57
00:08:40,300 --> 00:08:51,620
fast, word of mouth, teachings would be passed on from teacher to disciple, so you have

58
00:08:51,620 --> 00:08:58,300
to listen, listen carefully, doesn't mean just sitting down and keeping your ears open,

59
00:08:58,300 --> 00:09:08,420
it means inclining your ear, in a sense of being interested and focusing your attention,

60
00:09:08,420 --> 00:09:16,740
you're not like listening to me and checking Facebook or playing, whatever it is people

61
00:09:16,740 --> 00:09:33,100
play, farmbell, you should be focused, and then not only should you be focused, but you

62
00:09:33,100 --> 00:09:39,020
should keep it in mind, what you've learned, but I talked about someone who carries the

63
00:09:39,020 --> 00:09:48,140
dumb on their lap, and that means like if a person carries something like their soup on

64
00:09:48,140 --> 00:09:53,820
their lap, and if they forget about it and they stand up, it flies all over the place,

65
00:09:53,820 --> 00:09:59,460
spills all over, the person who carries the dumb on their lap is someone who when they're

66
00:09:59,460 --> 00:10:06,420
sitting listening, they keep it in mind, but as soon as they get up, they forget about it,

67
00:10:06,420 --> 00:10:10,940
so they don't take it into their lives, they don't take it seriously, it doesn't become

68
00:10:10,940 --> 00:10:21,580
a part of who they are, so soup doesn't just mean listening, it means the whole spectrum

69
00:10:21,580 --> 00:10:32,820
of attention and retention of knowledge and wisdom. Number four is Jaga. Jaga is, this

70
00:10:32,820 --> 00:10:39,580
is a specific, fairly specific layperson quality, so it's not just for women, it's for men

71
00:10:39,580 --> 00:10:46,740
as well, but Jaga means generosity because, I mean it's not that monks aren't generous

72
00:10:46,740 --> 00:10:54,340
and don't be generous, but it's more visibly and obviously a quality of labor because

73
00:10:54,340 --> 00:11:06,900
they support them, the monks, and they keep them alive with food and monks named in the

74
00:11:06,900 --> 00:11:16,060
robes or medicine, they provide them, but I guess more importantly, they have material

75
00:11:16,060 --> 00:11:26,060
wealth, so to be generous with that, not just with other Buddhists, but with poor people,

76
00:11:26,060 --> 00:11:33,340
with their family, with relatives, even with strangers, you know hospitality to people

77
00:11:33,340 --> 00:11:39,100
who come to visit. Many cultures, when you enter their house, you immediately offer you

78
00:11:39,100 --> 00:11:47,340
something, food or drink, find something, to share with you. They're very least asking

79
00:11:47,340 --> 00:11:55,340
what they need, is there anything I can get for you? Someone comes into your house to

80
00:11:55,340 --> 00:12:05,340
be generous, to be giving, monks can do it as well, we all can, but this is a great support

81
00:12:05,340 --> 00:12:11,460
for our religious practice, because again it builds confidence, it builds happiness, in

82
00:12:11,460 --> 00:12:18,980
a sense of not just a happy feeling, but feeling good about yourself, in a sense of giving

83
00:12:18,980 --> 00:12:35,260
energy, not depressing you, making it hard for you to making you uninterested, unresolute,

84
00:12:35,260 --> 00:12:41,380
not able to pull yourself together and move forward.

85
00:12:41,380 --> 00:12:48,460
And number five wisdom, we want to find the truth, we need wisdom, banya, banya means

86
00:12:48,460 --> 00:12:56,860
to nya means knowledge, but it means thoroughly, they're all complete, perfect, so real

87
00:12:56,860 --> 00:13:04,620
knowledge, wisdom, banya means wisdom. Wisdom is different from learning,

88
00:13:04,620 --> 00:13:19,780
true wisdom comes from seeing, doesn't come from speculating or extrapolating or rationalizing,

89
00:13:19,780 --> 00:13:27,860
true wisdom comes from just seeing, clearing up, clearing your mind so perfectly that

90
00:13:27,860 --> 00:13:35,180
you can see everything, calm your mind down through the meditation, not letting things

91
00:13:35,180 --> 00:13:43,540
distract you or not reacting to things, and eventually seeing things as they are, and

92
00:13:43,540 --> 00:13:50,460
seeing the things that we cling to or not worth clinging to, seeing the difference between

93
00:13:50,460 --> 00:13:55,500
wholesome mind states and unwholesome mind states, seeing that nothing is worth clinging

94
00:13:55,500 --> 00:14:06,580
to, and finally seeing nymana, so these five things are, that which leads to growth,

95
00:14:06,580 --> 00:14:12,140
so that's what we're all concerned with here, right? We want progress, we want to better

96
00:14:12,140 --> 00:14:21,660
ourselves, so why we practice meditation, we're not perfect, we are people who have problems,

97
00:14:21,660 --> 00:14:33,060
who react, who have reactions, who have problems inside, who ourselves are not yet satisfactory,

98
00:14:33,060 --> 00:14:42,920
we're not satisfied with our own state. Okay, so that's the demo for tonight, if you

99
00:14:42,920 --> 00:14:51,140
have any questions, there was a question from earlier, someone asking, do you have a breathing

100
00:14:51,140 --> 00:14:57,780
technique that helps you breathe better? Well, no, it's not about better, better as a judgment,

101
00:14:57,780 --> 00:15:05,380
so again, we're trying not to judge, trying not to react. We're not concerned about breathing

102
00:15:05,380 --> 00:15:10,780
better, we're concerned about how we react to the way we breathe naturally, whether

103
00:15:10,780 --> 00:15:19,140
that be good or bad. If it's really causing your problems, then sometimes you can address

104
00:15:19,140 --> 00:15:26,140
it, but mostly you just address your reactions to it, your mind flow that it feels not

105
00:15:26,140 --> 00:15:33,500
nice, feels strange, feeling, feeling, but if you dislike it or you're frustrated by

106
00:15:33,500 --> 00:15:58,620
it, so just liking, just liking. How much of the Buddha's teaching is confirmed as authentic

107
00:15:58,620 --> 00:16:08,300
and not invented, well, it was all invented, the Buddha invented it. But if you mean invented

108
00:16:08,300 --> 00:16:12,140
by someone other than the Buddha, well, it doesn't really matter, what matters is whether

109
00:16:12,140 --> 00:16:20,780
it works, right? You practice it and you see that it's called holes in it, then you

110
00:16:20,780 --> 00:16:25,980
can doubt as to whether a Buddha taught it. If you practice it and you see that it does

111
00:16:25,980 --> 00:16:31,420
actually do what it says, and it is more or less cohesive than you can say, well, it's

112
00:16:31,420 --> 00:16:40,980
probably taught by Buddha. How beneficial is it to participate in society and still hope

113
00:16:40,980 --> 00:16:47,380
for spiritual progress? Well, it's not beneficial to participate in society except in terms

114
00:16:47,380 --> 00:16:57,500
of duties, but you can still hope, you can still hope for spiritual progress. You don't

115
00:16:57,500 --> 00:17:02,100
have to leave it, but of course dedicating your life to spiritual progress is of course

116
00:17:02,100 --> 00:17:07,620
much better. So no, you don't have to become a monk against spiritual

117
00:17:07,620 --> 00:17:13,300
man. It's just easier because you don't have to do things that are unrelated to spiritual

118
00:17:13,300 --> 00:17:20,300
housing, you don't have to go to work, you don't have to deal with family, you don't

119
00:17:20,300 --> 00:17:45,300
have to take care of a house, possessions and all those things.

120
00:17:45,300 --> 00:17:58,740
Let's move on to questions. Can we try to do it down the pond at tomorrow or what some

121
00:17:58,740 --> 00:18:05,500
day soon? I think the weekend is probably better for, I assume it was better for questions

122
00:18:05,500 --> 00:18:14,940
because people were not working, but I'm not so many questions yet. Here's one.

123
00:18:14,940 --> 00:18:18,420
What are the chances of enlightenment in the present day and age? Well, it's not a matter

124
00:18:18,420 --> 00:18:25,980
of chance, it's up to you. Are you able to become enlightened? That's all. I mean, yeah,

125
00:18:25,980 --> 00:18:31,580
you could probably do some statistic analysis if you could figure out who's enlightened,

126
00:18:31,580 --> 00:18:37,780
not an easy task itself.

127
00:18:37,780 --> 00:18:51,340
And enlightenment isn't just about, you know, it isn't just the thing where you flick

128
00:18:51,340 --> 00:19:03,860
a switch, it just happens. So it's a gradual process. So you can start to practice. There's

129
00:19:03,860 --> 00:19:09,860
really no idea whether you can become enlightened. It's not putting one foot in front of

130
00:19:09,860 --> 00:19:13,860
the other and doing work.

131
00:19:13,860 --> 00:19:23,780
I want to donate something to your establishment. Well, we do function through donations

132
00:19:23,780 --> 00:19:30,980
so that's always appreciated, not expected, but if it doesn't come, we'll just have

133
00:19:30,980 --> 00:19:35,020
to shut down. So with different things, you can donate. I mean, I have to stay alive to

134
00:19:35,020 --> 00:19:45,100
eat. So I've gone back to going to restaurants that have been given money for me. So you

135
00:19:45,100 --> 00:19:52,980
can go to my web blog. And there's a wish list. That's about as direct as giving to me.

136
00:19:52,980 --> 00:20:01,980
But then we have this organization. And there's a support page for the organization. That's

137
00:20:01,980 --> 00:20:11,980
on the main theory mongolow.org site.

138
00:20:11,980 --> 00:20:29,900
If you go to serimongolow.org, you'll find probably 500 you need. Is there, is it right

139
00:20:29,900 --> 00:20:35,780
to say that you're in control of decision making? One is born as an animal. It can't make

140
00:20:35,780 --> 00:20:49,100
a decision like a human. There's no you to be in control of decision making. It's a hard

141
00:20:49,100 --> 00:20:55,420
subject to talk about because reality isn't quite the way we think in terms of there being

142
00:20:55,420 --> 00:21:04,180
a soul or a self. It's not exactly deterministic either. Reality is this. It's experience.

143
00:21:04,180 --> 00:21:15,140
So in any conventional terms, yes, you are responsible for each choice to make. But it's

144
00:21:15,140 --> 00:21:23,080
hard to be specific about it. Hard to be exact. Animals can still make decisions. They're

145
00:21:23,080 --> 00:21:32,980
just much less aware. You know, most humans don't make decisions. Most humans just go

146
00:21:32,980 --> 00:21:38,660
according to habit. It's hard to define someone with mindfulness. But even animals can

147
00:21:38,660 --> 00:21:48,500
cultivate it. I think actually I'm not sure every demo might tell otherwise. It's a good

148
00:21:48,500 --> 00:22:05,580
question because I know they talk about them being I hate to cause something. How can we deal

149
00:22:05,580 --> 00:22:17,060
with judgmental thoughts? For example, noticing how attached to someone else's. Well,

150
00:22:17,060 --> 00:22:22,940
you start by the disliking of it. Because if you're guilty, if you're mad about it, that's

151
00:22:22,940 --> 00:22:29,940
also a problem that prevents you from seeing it clearly. Remember, meditation is not about

152
00:22:29,940 --> 00:22:37,940
judging things. So even judging the bad things. But seeing them. And if you see them enough,

153
00:22:37,940 --> 00:22:43,100
you'll start to change. So if you see yourself being judgmental again and again, and you

154
00:22:43,100 --> 00:22:49,420
really start to see how stressful that is and how unpleasant that is, it'll start to give

155
00:22:49,420 --> 00:23:02,060
it out. How useless it is, I guess, most important. And we shouldn't keep ourselves over

156
00:23:02,060 --> 00:23:11,340
the fact that we still have defilements. We should learn about them, study them. Well,

157
00:23:11,340 --> 00:23:23,100
lots of questions. What are the benefits of Dutanga aesthetic practices? Well, they push you,

158
00:23:23,100 --> 00:23:31,100
they challenge you. It's like if you have someone who's a sport who plays a sport, they might

159
00:23:31,100 --> 00:23:39,660
do challenges, you know, or someone that's a BMX cross-country biking or something that

160
00:23:39,660 --> 00:23:45,660
challenges themselves to, or when I was doing rock climbing, we would challenge ourselves

161
00:23:45,660 --> 00:23:59,180
to harder and harder climbs. So it's like a challenge. It pushes you. It's like that. You don't

162
00:23:59,180 --> 00:24:06,220
have to do it. You can do ordinary biking, but you want to do tricks on your bike. Because

163
00:24:06,220 --> 00:24:15,100
it challenges. No, and then there's speaking of bicycling in the woods, bicycling in the woods,

164
00:24:15,100 --> 00:24:27,100
can it be a meditation? Nothing is meditation. Meditation is not what you're doing. Walking isn't

165
00:24:27,100 --> 00:24:32,460
meditation unless you're being meditative. So biking in the woods is not meditation unless you're

166
00:24:32,460 --> 00:24:37,420
being meditative. And the question is, does it mean by being meditative? And of course, there are

167
00:24:37,420 --> 00:24:44,620
many different answers to that. In our tradition, we have a very specific definition of the word

168
00:24:44,620 --> 00:24:50,060
meditation, not that we don't accept other things are meditation. But when we talk about meditation,

169
00:24:50,700 --> 00:24:54,460
we mean something fairly specific. So if you're doing that when you're biking, then yes, you're

170
00:24:54,460 --> 00:25:04,060
meditative. If not, no. My real question, how does one put into tongue of practices into effect

171
00:25:04,060 --> 00:25:10,460
in today's society, or an unceation, the willingness is not looked highly upon? Well, if we were all

172
00:25:10,460 --> 00:25:14,540
concerned about what was looked highly upon, we wouldn't ever get anything spiritual done.

173
00:25:15,580 --> 00:25:23,980
We will all be capitalists and materialists. Just put them into effect who cares what people

174
00:25:23,980 --> 00:25:31,740
think. If you're worried about the century of others, then maybe you've got a

175
00:25:31,740 --> 00:25:39,420
time to look for new friends and you're associated with it. Maybe that's being a little bit facetious

176
00:25:39,420 --> 00:25:48,540
but a condescending, I don't know what the word is. Get to a position where you can, but don't worry

177
00:25:48,540 --> 00:25:53,660
so much about the two tongueers. If you're, I mean yes, they're mostly for people who are living

178
00:25:53,660 --> 00:26:01,900
in forests. So if you're worried about, I can't practice the dutanga. They're not, they're not

179
00:26:01,900 --> 00:26:09,900
essential. I mean, I was going to stop you from meeting one meal a day. I was going to stop you

180
00:26:09,900 --> 00:26:15,500
from not wearing extra robes. Society might, I only wear one set of robes, so that's a dutanga,

181
00:26:15,500 --> 00:26:22,860
isn't it? I don't have a change. But it gets problematic. You just have to remember to wash your

182
00:26:22,860 --> 00:26:36,460
robes often, as soon as they start to smell. How does becoming a monk affect one's connection

183
00:26:36,460 --> 00:26:43,900
with family members? It's pretty profound, it affects, as you can probably surmise, it affects

184
00:26:43,900 --> 00:26:53,500
your many aspects of your life fairly profoundly. So you're no longer a part of the family unit

185
00:26:53,500 --> 00:26:59,260
from your own perspective. Now your family might see it otherwise, so there are, there are

186
00:27:02,460 --> 00:27:09,580
allowances made for family members. There's quite a few allowances made that make it more

187
00:27:09,580 --> 00:27:15,980
comfortable, but there's a change in the dynamic because you're no longer able to interact

188
00:27:15,980 --> 00:27:23,900
in the same way. So it takes some adjustment, but eventually it seems to be a good thing. I mean,

189
00:27:23,900 --> 00:27:29,740
my family, I think, it's been a great thing for me to become a monk. It's enriched their lives

190
00:27:29,740 --> 00:27:35,020
just as many different things do, but I think they would say it enriches their lives to have

191
00:27:35,020 --> 00:27:41,580
another exotic piece of the puzzle, you know, to have me as part of their family. And Buddhist

192
00:27:41,580 --> 00:27:47,180
monk is, it's not the best thing about their family, but it's one kind of like a jewel that they

193
00:27:47,180 --> 00:27:52,940
have. I think most of my family feels that way now that it's something pretty neat, that they

194
00:27:52,940 --> 00:27:58,380
have a monk who is teaching meditation, who seems to be doing good things for people, helping the

195
00:27:58,380 --> 00:28:06,380
world, you know, maybe makes them feel good to know that they're, I think. I don't suppose all my

196
00:28:06,380 --> 00:28:12,380
family members feel like that all the time, but in general, it seemed, I mean, it's always thought

197
00:28:12,380 --> 00:28:19,740
it was a great thing. Obviously, I think it is becoming a monk is great for your family in the long

198
00:28:19,740 --> 00:28:29,660
run. It just causes some tension as change does because people have a hard time dealing with change.

199
00:28:29,660 --> 00:28:35,100
Animals I care for is they're bad karma and not letting them be wild. Bad karma is not in such

200
00:28:35,100 --> 00:28:41,260
general things, bad karma is in moments. So at the moments when you give rise to greed and

201
00:28:41,260 --> 00:28:49,180
anger or delusion, there's bad karma. The moments you give rise to non-greed, non-engured, non-delution,

202
00:28:49,180 --> 00:29:02,140
as in wisdom, love and pronunciation, then there is good karma. The problem with animals is it's

203
00:29:02,140 --> 00:29:08,620
very easy to give rise to bad karma in regards to it. So either in terms of our dependence on them,

204
00:29:08,620 --> 00:29:13,740
or in terms of our anger and frustration when they don't do what we want, when we have to

205
00:29:16,540 --> 00:29:21,580
discipline them that kind of thing. It's just like small children. I wouldn't ever want to have

206
00:29:21,580 --> 00:29:27,340
children because I wouldn't ever want to have students who didn't ask me to be their teacher.

207
00:29:28,540 --> 00:29:35,660
Loving children to me seems horrific. Having kids, you don't know, you don't know whether they're

208
00:29:35,660 --> 00:29:41,100
going to listen to or not. They have no reason to. Well, besides self-preservation,

209
00:29:43,580 --> 00:29:48,540
that's maybe not so bad because kids do tend to listen to your parents if you're good parents,

210
00:29:49,660 --> 00:29:53,260
but not certainly. And the kids can be terrible.

211
00:29:57,020 --> 00:30:03,580
Again, so as regards to not letting them be wild, you have to see, does it necessitate

212
00:30:03,580 --> 00:30:10,540
states? And probably it does because animals are fairly unmanageable, except if you are

213
00:30:10,540 --> 00:30:16,700
unwholesome towards them, like hitting them, scolding them, punishing them that can do.

214
00:30:19,260 --> 00:30:27,500
And then there's also the coddling room, which is certainly unwholesome when you coddle someone,

215
00:30:27,500 --> 00:30:34,860
coddling them. Make sure it always has pleasure so it doesn't ever get angry or frustrated

216
00:30:34,860 --> 00:30:39,900
or depressed, diverting its attention with pleasure, not really wholesome.

217
00:30:44,060 --> 00:30:45,500
Meditation on the body.

218
00:30:47,980 --> 00:30:53,180
Would it be appropriate to look into the mirror when noting hair or nose nose? Don't say nose

219
00:30:53,180 --> 00:31:01,580
nose, but hair or hair, yes, nose isn't one of them. Well, you start by looking at it. That's a

220
00:31:01,580 --> 00:31:06,940
great way to start looking at the mirror to start. But a good thing to do if you're really interested

221
00:31:06,940 --> 00:31:12,860
is read the Wisudimanga. This is one part, one meditation where reading the Wisudimanga

222
00:31:12,860 --> 00:31:20,140
would be a great help because there's some really in-depth descriptions of the 32 parts of the body

223
00:31:20,140 --> 00:31:25,660
that go beyond with anyone that I would have ever thought of to describe these things. It's so

224
00:31:25,660 --> 00:31:33,340
very clear, you know, excruciatingly clear and precise about the description of these things

225
00:31:33,340 --> 00:31:38,380
that really helps you keep them in mind. So this is a manual that's been designed to

226
00:31:39,180 --> 00:31:44,380
facilitate this. Of course, a mirror can help. But the mirror is, it's still said we didn't

227
00:31:44,380 --> 00:31:52,380
be inferior to the Wisudimanga description. But the idea is eventually then you don't need

228
00:31:52,380 --> 00:31:56,620
the mirror. You wouldn't need it. You wouldn't go and close your eyes.

229
00:31:58,620 --> 00:32:04,060
We began looking into the forest tradition, concerned that this desire is based on attachment

230
00:32:04,060 --> 00:32:12,140
for natural environment. It's a great aversion for people. I already have an issue with feeling

231
00:32:12,140 --> 00:32:15,500
inversion towards groups of people, so perhaps they'd be compounding those.

232
00:32:17,980 --> 00:32:23,500
See, the thing about the Thai forest tradition is that that's just their name for it.

233
00:32:23,500 --> 00:32:28,300
And it's somewhat objectionable because there are lots of forest monks in Thailand that have

234
00:32:28,300 --> 00:32:35,260
nothing to do with that group. They just got famous. And rightly so, you know, they are a good

235
00:32:35,260 --> 00:32:41,180
group of monks. But it's somewhat objectionable that they should call themselves the forest tradition.

236
00:32:41,180 --> 00:32:45,500
I've lived in the forest. You know, my teacher spent a lot of time in the forest.

237
00:32:47,020 --> 00:32:53,340
You have to understand where that word came from. There was one monk who, a gen man,

238
00:32:53,980 --> 00:32:59,980
and his sort of other monks who were strict about staying in the forest. And so they started

239
00:32:59,980 --> 00:33:06,780
calling them what bah, what bah. And their their monasteries would be called what bah, this, what

240
00:33:06,780 --> 00:33:11,980
bah, that bah means forest. And because their monasteries all took on the name what bah,

241
00:33:11,980 --> 00:33:17,260
what bah, they called it sai what bah. And then today they still call it sai what bah. But it's

242
00:33:17,260 --> 00:33:27,660
just a short form of this. The sai means the tradition or the need, but what bah means the forest

243
00:33:27,660 --> 00:33:32,940
monasteries. But it was referring to specific forest monasteries, a specific group before it's

244
00:33:32,940 --> 00:33:42,460
monasteries. And so it's come to be a little bit deceptive in that sense. So if you ask, you're

245
00:33:42,460 --> 00:33:45,980
actually asking two questions or you have to separate those that question out because

246
00:33:47,580 --> 00:33:52,700
there's nothing to do with the time forest tradition. That's true that your question doesn't

247
00:33:52,700 --> 00:33:59,180
exactly. But let's just focus on the idea of living in the forest. It's living in the forest

248
00:33:59,180 --> 00:34:05,980
greater version potentially. But I think the benefits of living in the forest far outweigh that

249
00:34:06,860 --> 00:34:13,100
because any aversion that you have towards something can be worked out through introspection now

250
00:34:13,100 --> 00:34:20,620
living in the forest helps with introspection. So in the beginning it might be just an indulgence

251
00:34:21,340 --> 00:34:28,380
by attachment because of attachment to nature. But over the long term, living that the benefits

252
00:34:28,380 --> 00:34:37,580
will come and they'll wash away all that. It's true living in the forest long term. You gain a lot,

253
00:34:37,580 --> 00:34:44,380
you gain a lot of strength of mind and wisdom. And that wisdom is very applicable in society among

254
00:34:44,380 --> 00:34:49,820
groups of people. I wouldn't say that person who's lived in the forest for a long time is a verse

255
00:34:49,820 --> 00:34:59,180
too. They may be disinclined, but they'll be much less likely to give rise to aversion because

256
00:34:59,180 --> 00:35:04,300
they've been able to work it out. I mean, not just by living in the forest, but by doing the things

257
00:35:04,300 --> 00:35:10,300
that are meant to be done in the forest. Practice of insight meditation, which is much better done

258
00:35:10,300 --> 00:35:26,700
in the forest, I would say. Not necessary to be in the forest, but there are benefits.

259
00:35:28,860 --> 00:35:39,420
I think that's enough for tonight, lots of questions. That's another question. Okay, I'm going to cut

260
00:35:39,420 --> 00:35:50,380
it off there. Thank you all for coming tonight. We'll see you out tomorrow.

