Hi, today I'd like to talk to you all about a project that we've begun here in Ontario, Canada.
As many of you may know, I've returned to Canada with the intention of continuing to spread the dhamma here in my own country.
The reasons for that include the fact that it's easier and more convenient for me in a place where first of all I went past part.
I don't have to worry about visas and also where the culture is familiar and where I'm able to integrate with the society.
So that was the idea and I've been back in Canada for a while now and we've decided I've got a few people here who have gotten into helping.
So we've decided to take it to the next level.
Our goal is to move into a house of our own nearby to the university here in Hamilton, Ontario.
In September I'll be going back to university and part of the reason for that is to be closer to the sort of people who'd be interested in the things that we do.
Because one of the things that I found difficult is actually getting involved or getting into back into the society here.
Back in with fellow Buddhists because of our location right now I'm in an area where there are not so many people and certainly not so many Buddhists.
And so by going back to university the idea is to get back into the swing of things.
I'm going to be involved with the Religious Studies Department there and we've applied to start a Buddhism Society.
The idea will be that there will be more of a local community here interested in the meditation practice.
I know I've got a lot of people following on the internet but the difficulty with that is arranging things like food for meditators and the lodging and sort of local coordination.
Even just things like recording videos.
Most they have to do by myself because there's no one here who is of a mind to help.
So the goal is that in January I'll be halfway done the year at McMaster University.
By January 1st we're going to try to rent a house near the university.
There's a lot of student housing available and so we'll find a house that's not too expensive and begin by renting.
And we will establish that as our meditation center and monastery and it will be a place that we can run and use to set up a studio.
Right now I mean a single room here that's what I've got but we would be able to set up a studio and have someone from our new Buddhism Society at McMaster help out with recording videos.
Maybe even have someone move in to take care of the place.
But so the reason for making this video is that to let you know about the project and how it's being run by some very kind and devoted people who are interested in making this happen.
So there's information on one of the crowdsourcing websites where this video is going to be placed and they're looking for support.
If you would like to support this project please get in contact with the organizers and how about help us to make this possible.
It's most likely going to happen. What we're looking for now is the support to make it happen in January.
To be able to move into a place and take over a house for a year and get a year's lease on a building where we can hold meditation courses.
That will be close enough to people close enough to the university that will be able to hold daily sessions, give daily talks, live to a live audience and teach meditation on a daily basis.
And to just sort of begin to create a presence where we can eventually turn into a real and lasting resource, both locally and by extension internationally.
So there you are. I don't want to say too much. I obviously can't get too much involved in the workings of it, but thank you all for your support and wishing you all the best.
We'll keep track.
