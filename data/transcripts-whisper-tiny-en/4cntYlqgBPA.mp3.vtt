WEBVTT

00:00.000 --> 00:21.120
So tonight's question is about protecting one's practice as a lay person, the person

00:21.120 --> 00:33.400
asking expresses their distress at and seeing how easy it is to fall away from the practice,

00:33.400 --> 00:42.360
even for those who have done practice before, but find themselves back in worldly life and

00:42.360 --> 00:53.680
find it very hard to keep up their practice, so I thought about this and there were three

00:53.680 --> 00:58.160
things I can say about this, that's what I came up with.

00:58.160 --> 01:05.520
The first thing I would say is that in some respects, and this is probably a little bit

01:05.520 --> 01:11.240
of a disappointment, but in some respects this question is outside of the sort of question

01:11.240 --> 01:20.200
I'm comfortable answering, but I think there's at least a small lesson there and that's

01:20.200 --> 01:34.600
in regards to our practice and our perspective as Buddhist practitioners.

01:34.600 --> 01:46.880
I teach meditation, I think it helps my practice, it fulfills my sort of duty I can say to other

01:46.880 --> 01:56.920
beings, it feels comfortable, something I can do for others and it's not a great distraction

01:56.920 --> 02:03.880
from my own spiritual development, but that's the same for anyone and that's for

02:03.880 --> 02:12.800
people who are interested in practicing, trying to help people who are not interested

02:12.800 --> 02:17.720
in practicing find ways to be interested in practicing, has always felt a little bit

02:17.720 --> 02:24.200
outside of the realm of what is useful and beneficial.

02:24.200 --> 02:29.080
So we're starting to enter into that realm I think with this question.

02:29.080 --> 02:34.840
We'll ask me often, I often get adults wanting me to teach their children how to meditate

02:34.840 --> 02:45.280
and I've always found that profoundly wrong or fundamentally unpleasant, not unpleasant

02:45.280 --> 02:56.200
word is improper, giving an awkward sort of feeling like not quite right for this reason.

02:56.200 --> 03:01.800
Because you're talking about people who don't want to meditate, but want to want to meditate

03:01.800 --> 03:10.160
and hear their parents want them to want to meditate, so if you take a person who's not

03:10.160 --> 03:15.480
interested in meditation but really would like to be interested in meditation, that's

03:15.480 --> 03:27.960
the sort of problem that comes out, you know, lay Buddhists who like the idea of meditating

03:27.960 --> 03:35.560
but find themselves distracted by so many things, so there's certainly no sense of criticizing

03:35.560 --> 03:45.440
or blaming anyone who gets distracted by worldly life, but there should be I think

03:45.440 --> 03:53.720
no expectation for anyone but themselves to find a way to overcome that.

03:53.720 --> 04:00.880
To some extent, I think we have to understand that the Buddha wasn't even a savior.

04:00.880 --> 04:05.320
The Buddha didn't teach lay people and he taught some things that were quite useful for

04:05.320 --> 04:16.160
lay people and very much outside of the purview of the realm or the limit or the range

04:16.160 --> 04:29.680
of meditation practice, but even the Buddha had very little to say beyond describing right

04:29.680 --> 04:36.400
ways to live your life.

04:36.400 --> 04:43.240
There's no limit to our potential to help other beings if we want to go further, we could

04:43.240 --> 04:49.760
help beings in hell, we could help animals figure out how to get dogs to be interested

04:49.760 --> 04:55.000
in good things that would eventually perhaps allow them to be born as humans.

04:55.000 --> 05:01.400
And in the week, there's no limit to the amount of help we could give to other beings, but

05:01.400 --> 05:02.720
that's not certainly my goal.

05:02.720 --> 05:08.320
I don't think it's a goal that the Buddha advised in his followers.

05:08.320 --> 05:12.880
And so I think it's a good lesson for us to remind ourselves of a certain perspective

05:12.880 --> 05:19.480
and a balance, and the balance always has to be weighted in favor of our own spiritual

05:19.480 --> 05:20.480
development.

05:20.480 --> 05:26.120
It really helps others if our attention is always on helping others.

05:26.120 --> 05:30.520
There's of course helping anyone yourself or another person depends upon your own spiritual

05:30.520 --> 05:37.680
development, and one's own spiritual development has to require preponderance of attention

05:37.680 --> 05:47.440
to one's own practice, first thing I'll say, but one other thing that's also a little

05:47.440 --> 05:55.640
bit discouraging, I think, is that fundamentally we're asking, the question is asking,

05:55.640 --> 06:05.080
or in a part of it asking, or something that is impossible or contradictory, it's

06:05.080 --> 06:11.640
like the expression of having your cake and eating it too, it's like saying, how can

06:11.640 --> 06:14.880
I be a really good meditator and live in the world?

06:14.880 --> 06:21.920
Honestly, you can't, and the part of one of the most important, one important support

06:21.920 --> 06:27.280
of meditation practice is leaving the world physically, of course, mentally much more

06:27.280 --> 06:35.680
important, and technically, is it possible for someone to live a spiritual life in the

06:35.680 --> 06:36.680
world?

06:36.680 --> 06:43.400
Technically, it's possible, but to find a way for an ordinary average person like you

06:43.400 --> 06:54.000
or me to do that, and then be distressed by how difficult it is, and wonder how you can

06:54.000 --> 06:56.480
make it easier, it's really not the case.

06:56.480 --> 07:01.040
How you can make it easier is to be a lot stronger, a lot more perfect, and a lot more

07:01.040 --> 07:10.360
pure, which I'm not expecting anyone to be, I don't consider myself to be like that.

07:10.360 --> 07:19.200
It's always by people who practice Buddhism by the Buddha himself and described, been

07:19.200 --> 07:34.400
said that lay life is a dusty road, it's all confined and restricted and mixed up, it's

07:34.400 --> 07:41.840
not like, in the olden days, in the time of the Buddha, when there was a lot of traffic

07:41.840 --> 07:48.200
there would be a great amount of dust, you'd have horses and just people walking and it

07:48.200 --> 07:54.040
would be a crowded road, it would be very dusty and dry and not very pleasant because

07:54.040 --> 07:58.840
people would be defecating on the side of the road and that sort of thing, garbage on

07:58.840 --> 08:10.520
the side of the road, and they say in comparison, leaving home and going to live in seclusion

08:10.520 --> 08:22.040
a simple life is the open air, it feels much more fresh, much more pure and free, and

08:22.040 --> 08:30.640
so to ask about how to make meditation in lay life easier is problematic at its core

08:30.640 --> 08:38.080
because I think you're much better served understanding how difficult it is and it's for

08:38.080 --> 08:45.400
some people it's not possible for them to leave it behind and so important for them

08:45.400 --> 08:49.080
will be to understand that it's not going to be easy and I think part of our practice

08:49.080 --> 08:57.600
is always about understanding and coming to terms with difficulty and I think to some

08:57.600 --> 09:07.640
extent, all ever trying to make your practice easier is problematic so instead of looking

09:07.640 --> 09:12.960
at it as something, wow this is really difficult and I'm struggling, try and reframe

09:12.960 --> 09:18.600
it and learn to be mindful of the struggle and mindful of what it is that is causing

09:18.600 --> 09:28.320
the struggle and so on, which brings me to the third thing which is more hopeful, a little

09:28.320 --> 09:32.840
bit critical as well, I don't mean to be entirely critical but it's critical in general

09:32.840 --> 09:42.360
of us as Buddhists, as meditators, we often focus solely on meditation practice and so

09:42.360 --> 09:47.520
you come here to practice and it can be forgiven because that's how we teach you and

09:47.520 --> 09:51.320
that's really what we give you so you come here you learn how to meditate and you say

09:51.320 --> 09:56.960
okay, I've done meditation, then you go home and you try to continue to meditate like

09:56.960 --> 10:04.860
that and it doesn't work or it's problematic and this is because even here meditation

10:04.860 --> 10:12.640
requires support, much of your support is given to you artificially, your livelihood

10:12.640 --> 10:17.760
is pure here and you don't have to even go to work for it, you don't have to demean

10:17.760 --> 10:25.320
yourself doing something meaningless in order to get food and shelter and so on but your

10:25.320 --> 10:34.000
environment is pure, there's no distractions, there's nothing pulling you here or there,

10:34.000 --> 10:40.240
desire, there's no conflict with other people, there's no manipulation or anything like

10:40.240 --> 10:47.920
that and so it feels like wow this is okay all I have to do is meditate but you go home

10:47.920 --> 10:58.680
and none of those supports are there and of course even here there is, there can be

10:58.680 --> 11:03.960
the lack of support, many people come to practice and aren't able to continue or finish

11:03.960 --> 11:11.360
the course because they lack the support, so it's been good in general for us to do

11:11.360 --> 11:18.080
this correspondence course, the online course because it first of all provides a filter

11:18.080 --> 11:23.920
if you're really not ready to do a course here, here you'll figure it out in the online

11:23.920 --> 11:33.920
course, if you're not able to do that then you would make it, it means it means

11:33.920 --> 11:41.360
it selects the people who are really keen to do it but it also prepares you, it's a great

11:41.360 --> 11:48.160
way to understand the dynamics of the practice and what is necessary to use as a support

11:48.160 --> 11:56.960
how to begin to organize your life in a way that is supportive of meditation practice but

11:56.960 --> 12:05.600
deeper than that there are qualities of individuals that are required and there's something

12:05.600 --> 12:14.520
we call Upanishaya, Upanishaya is what you come into this world with, well that's how it's

12:14.520 --> 12:22.480
talked about in Thai I think, probably the word is we say it, we say it means Upanishaya

12:22.480 --> 12:26.680
maybe as well anyway there's this idea of what you bring into this life meaning some

12:26.680 --> 12:33.800
people are just never going to be into practice or capable of practicing, capable of being

12:33.800 --> 12:42.480
interested in practicing and for most of us we're kind of halfway we have some interest

12:42.480 --> 12:48.520
and capability of practicing but it's rough and it's difficult, that's because we haven't

12:48.520 --> 12:54.480
brought a great support into this life and it can also be because we're not developing

12:54.480 --> 13:04.080
or our development of support is often incomplete or partial and so these are things like

13:04.080 --> 13:10.480
being a good person, being kind, being generous, having a pure heart because what we mean

13:10.480 --> 13:14.760
by being a good person is really that, having a pure mind and a pure heart and the pure

13:14.760 --> 13:21.960
or your foundation as an individual is of course the easier it is to have pure thoughts,

13:21.960 --> 13:32.200
clear thoughts, steady and coherent thoughts and so this applies to lay people, it's a

13:32.200 --> 13:39.920
big reason I think why you can, I can agree that much of the teaching that is given to

13:39.920 --> 13:47.200
lay people, lay Buddhists is proper, we often hear criticism of monks who teach lay people

13:47.200 --> 13:52.400
only very shallow dhamma and they should be teaching deeper dhamma and it's true to some

13:52.400 --> 14:00.560
extent but it's also forgivable because really things like being generous and keeping

14:00.560 --> 14:06.920
morality and practicing basic meditation like loving kindness and so on may not get you

14:06.920 --> 14:16.840
to nibana but it certainly creates a great foundation, it is wrong to stop there and

14:16.840 --> 14:23.320
never hint at something more, you should always be leaving the door open for people who

14:23.320 --> 14:28.680
are ready to make the jhamma because they are always, of course, lay many lay people to

14:28.680 --> 14:35.200
be a monk to want to practice meditation but the foundation is equally important and it's

14:35.200 --> 14:42.280
not just the foundation, it's a support, I would make this analogy of a tree, if you want

14:42.280 --> 14:48.920
to grow a tree from a sapling, you can't just plant it and leave it, you need support

14:48.920 --> 14:53.520
through the winters especially here in Canada we know this because even just the snow

14:53.520 --> 14:58.520
and the cold you have to protect it from them and do other sticks to prop it up and

14:58.520 --> 15:08.040
so on and so even when you're practicing meditation especially not here where you have everything

15:08.040 --> 15:15.720
sort of laid out for you, you need to set up your life in ways that are wholesome, conduct

15:15.720 --> 15:22.680
good works, good deeds, do good things for the world around you even in terms of teaching

15:22.680 --> 15:28.720
helping other people come to the practice can be a very good way to support your own practice

15:28.720 --> 15:34.800
because it creates, it gives you this environment, it surrounds you with people who are interested

15:34.800 --> 15:46.440
in meditation, it challenges you and tests you, it keeps you straight, another important

15:46.440 --> 15:53.520
one is something like your association with other people, not something like that in and

15:53.520 --> 16:00.640
of itself is one of the most important qualities, right so having teaching other people

16:00.640 --> 16:06.240
is a part of that but just associating with other practitioners, having a meditation

16:06.240 --> 16:11.600
group that you go to sit with or whatever, being surrounded by other people is very important

16:11.600 --> 16:20.000
for people who are in a good place where you live, what you do, your work, there's so

16:20.000 --> 16:26.640
much and it starts to get as I said outside of the purview of what I, the realm of what

16:26.640 --> 16:33.840
I consider mind duty as a, as a meditation teacher but it's just the general understanding

16:33.840 --> 16:38.080
that there's more to the practice than the practice you need, it's why I teach the

16:38.080 --> 16:43.320
Sabasa who took quite often because it reminds us of all the different things we need

16:43.320 --> 16:49.880
to, different aspects of our life that we need to consider as a protection for our

16:49.880 --> 17:00.360
people. And the last part of that is something that I think could really help is our closeness

17:00.360 --> 17:10.240
with Buddhism which to some extent is artificial, right our connection with concepts like

17:10.240 --> 17:16.320
the concept of the Buddha, the Dhamma, the Sangha, even just the statues, having some

17:16.320 --> 17:25.680
kind of image in our mind, the visualization of a Buddha image, the association with monks

17:25.680 --> 17:37.280
and chanting and rituals, candles, flowers, incense, these kind of things and the sort

17:37.280 --> 17:43.120
of determinations that we make may I always be close to the Buddha sassana, they can often

17:43.120 --> 17:50.640
be artificial, but they can also be quite helpful meaning a person could be a very good

17:50.640 --> 17:57.560
person, have a very good qualities of mind and to some extent in the headget here because

17:57.560 --> 18:02.280
it's not entirely true but to some extent even though they have great good qualities if

18:02.280 --> 18:09.960
they've never had any connection with Buddhism as a concept, as a construct, as an institution,

18:09.960 --> 18:14.480
they may just never meet up with another Buddhist, they may not have the karmic connection

18:14.480 --> 18:23.440
or the direction to ever find a way to go further because goodness in and of itself

18:23.440 --> 18:28.920
doesn't necessarily on a conventional level lead to enlightenment, they could just lead

18:28.920 --> 18:34.560
you to happy rebirths, heaven and so on. There are many non-Buddhists who are good people,

18:34.560 --> 18:40.640
Christians and Muslims and Jews and Hindus and all religions really have these people

18:40.640 --> 18:45.680
and because these religions have a part of them that teach us goodness, some people

18:45.680 --> 18:51.440
take them up on that and cultivate it really quite well but they never make it to Buddhism

18:51.440 --> 19:02.960
often because of their connection, the images of the crucifix or any other religious symbol

19:02.960 --> 19:09.280
artificially, it sends them in that direction and it inclines them to meet up with those

19:09.280 --> 19:14.880
people, the dhamma wheel if you have these sort of symbols. So what I'm trying, what this

19:14.880 --> 19:19.720
all means and how this carries out in practice is that things like getting involved with

19:19.720 --> 19:25.800
Buddhist culture can be very useful, culture, Buddhist culture can be a very useful thing

19:25.800 --> 19:32.240
as a shall, as a means of supporting what is really and truly Buddhism. And a part of this

19:32.240 --> 19:38.360
is, as I said, the determinations, I think a part of this is where we make a determination

19:38.360 --> 19:45.640
for example, people make determinations to be reborn in the time of Matea, the next Buddha.

19:45.640 --> 19:52.240
That's a good determination to make, I don't think it's any in any way a replacement

19:52.240 --> 19:57.680
for actual practice, it should never be, but as a part of our practice to remember and

19:57.680 --> 20:04.200
to think and to even people will send prayers to Matea and not prayers, but in the sense

20:04.200 --> 20:11.640
of chanting and making a determination, paying respect to Matea, paying respect to the

20:11.640 --> 20:18.400
Buddha, the dhamma, the sanga in the form of chanting can be very helpful to set your

20:18.400 --> 20:25.480
mind in a way that's close to the Buddha's teaching. And it's not just for a future,

20:25.480 --> 20:30.600
it's for your own present life, this isn't just so that one day I might be reborn

20:30.600 --> 20:36.920
in with Matea, it keeps you close to the Buddha's ass and it keeps you in touch with

20:36.920 --> 20:42.520
other Buddhists, even if they aren't practicing, maybe they've studied Buddhism and can

20:42.520 --> 20:47.960
remind you of certain things. We often learn a lot as meditators from scholars because

20:47.960 --> 20:55.040
they have a lot of knowledge and they're able to explain things in a very, sometimes

20:55.040 --> 21:03.600
not always, but often in a very clear and logical manner. But the other side of that

21:03.600 --> 21:11.520
and it's the last thing I'll say is that to some extent it's just our purity of mind

21:11.520 --> 21:17.120
and to some extent goodness is enough. Really Buddhism ultimately isn't an artificial,

21:17.120 --> 21:25.440
religious thing institution, it's just about seeing reality. And so in regards to life as

21:25.440 --> 21:35.920
practice being difficult in lay life and the struggle to many, in many respects this is just

21:35.920 --> 21:47.440
our failing as Buddhist practitioners and it's what we have to recognize that we are in

21:47.440 --> 21:57.360
complete. Often we make goals, we fixate on the goal and I'm going to practice and it's

21:57.360 --> 22:02.080
going to work and I'm going to become enlightened. When in fact, it may take your lifetimes

22:02.080 --> 22:06.720
to become fully enlightened and it certainly may take much more than just sitting down

22:06.720 --> 22:13.040
and practicing meditation, not to trivialize the core importance of meditation, of course,

22:13.040 --> 22:24.800
it's by far the most important thing. But rather to say that our meditation might be difficult

22:24.800 --> 22:30.560
and problematic, take the example of a person who stops meditating. Once to get back into meditation

22:30.560 --> 22:37.200
it finds themselves distracted by so many things. They're perspective I think it's important

22:37.200 --> 22:42.960
that it changes, that they rather than think of it, oh I'm not meditating and therefore I'm

22:42.960 --> 22:47.360
failing or so on or this person is not meditating and therefore they're failing. We should look

22:47.360 --> 22:56.000
rather at our situation and how we can get closer to freedom from suffering and that's not

22:56.000 --> 23:04.000
always through meditation but I think it always is sort of things like mindfulness. Walking down

23:04.000 --> 23:08.720
the street and mindfully. Having a different perspective instead of saying I'm a failure so I

23:08.720 --> 23:13.840
just might as well give up to say no. When I was walking down the street I was mindful right now

23:13.840 --> 23:20.400
I'm walking on the street and I can be mindful. We don't need Buddhist rituals and they certainly

23:20.400 --> 23:27.680
don't save us from our lack of meditation practice. But mindfulness is much more important of course

23:29.280 --> 23:33.920
than rituals and actually in some ways more important than what we call practice. A person

23:33.920 --> 23:38.880
can do hours and hours of meditation practice and never get anywhere if they're not actually being

23:38.880 --> 23:45.920
mindful. My teacher said this and he said in opposition there's a story of one monk who

23:45.920 --> 23:52.080
became enlightened with three steps I think it was. Walking down the street if you say walking

23:52.080 --> 23:55.440
walking you can become enlightened just there if conditions are right.

23:58.880 --> 24:08.960
So I don't, to some extent that isn't entirely satisfying answer I don't think but

24:08.960 --> 24:17.040
I think for the reasons and based on the explanation we have to understand clearly this

24:17.760 --> 24:29.600
concept of worldly life and enlightenment and all the points in between and how we can connect

24:29.600 --> 24:38.080
the thoughts from one to the other. So ultimately what I do is teach, try to teach meditation

24:38.080 --> 24:45.200
practice to those who are interested and I think at the very least helping people understand

24:45.200 --> 24:49.840
how to be mindful which of course is applicable anywhere. Practice, practiceable,

24:51.680 --> 24:56.560
practiceable anywhere. Practiceable even the word.

24:56.560 --> 25:10.880
Anyway, that's the answer to that question. Thank you for asking. Thank you for listening.

