1
00:00:00,000 --> 00:00:20,720
I'm leaving everyone in broadcasting live July 8th today's quote it's about sickness

2
00:00:20,720 --> 00:00:37,480
Two kinds of sickness. It's a nice little quote. Two kinds of sickness. Physical and mental.

3
00:00:37,480 --> 00:01:00,540
Kai, Kota, Rook, Mo, Jada, Sipo, Jada, Rook. He says, the difference is with physical

4
00:01:00,540 --> 00:01:12,140
sickness. Well, you see people without physical sickness for days, weeks, months, years on

5
00:01:12,140 --> 00:01:26,380
end. The nobody escapes mental sickness, even for our moment, except for those who are in

6
00:01:26,380 --> 00:01:37,340
life. Mental illness is something that most people that are are plagued by, moment after,

7
00:01:37,340 --> 00:01:56,980
day in, day out, type of sickness that, and everyone carries with them. So in Buddhism,

8
00:01:56,980 --> 00:02:10,140
mental illness isn't simply the extreme cases of clinical depression or anxiety or OCD or

9
00:02:10,140 --> 00:02:21,380
phobias, psychosis or all of these things are just extreme to the extent that an ordinary

10
00:02:21,380 --> 00:02:39,380
an instructed person can recognize them and latch on to them and identify them as a problem.

11
00:02:39,380 --> 00:02:47,940
But most of the time we don't recognize and identify our mentalness is for what they

12
00:02:47,940 --> 00:03:07,020
are, just illness and discord, distress until we come to meditate. Many people when they begin

13
00:03:07,020 --> 00:03:16,340
to meditate for the first time, it's like a revelation, a realization that our minds aren't

14
00:03:16,340 --> 00:03:33,700
quite as healthy as we thought they were. Our minds are plagued by illness, greed,

15
00:03:33,700 --> 00:03:53,060
illness, anger, illness and delusion and illnesses. That really is the illness and the problem

16
00:03:53,060 --> 00:04:02,260
that we should focus on and should understand Buddhism to focus on more so than simply

17
00:04:02,260 --> 00:04:12,220
suffering. More important is the cause of suffering and the suffering will, we can live with

18
00:04:12,220 --> 00:04:20,340
it. It is possible to be at peace with what we would normally term suffering. But there's

19
00:04:20,340 --> 00:04:26,860
no peace for one who has mental illness. The suffering comes because we have mental illness

20
00:04:26,860 --> 00:04:35,860
and it's really suffering. It becomes suffering because of our mental illness, because

21
00:04:35,860 --> 00:04:49,940
of our greed, our anger, our delusion. We'll put in another place that's on a subjective

22
00:04:49,940 --> 00:04:57,220
sickness. After about four types of sickness, there's sickness from utu, which means the

23
00:04:57,220 --> 00:05:10,660
environment. Sickness from food, ahara. Sickness from, there's on the mind, jita, on jada

24
00:05:10,660 --> 00:05:25,060
sickness, and sickness from karma. Come on. Four types of sickness. So I know a little more

25
00:05:25,060 --> 00:05:33,940
detail. Physical sickness is either from the environment or from what is around you or

26
00:05:33,940 --> 00:05:44,820
from what you're taking inside of you. So things like viruses and radiation and environmental

27
00:05:44,820 --> 00:05:55,260
distress and environmental discomfort, these would be utu. Ahara from food, what we ate

28
00:05:55,260 --> 00:06:04,420
and drink causes a sickness. These are the causes of physical sickness. Mental sickness

29
00:06:04,420 --> 00:06:12,060
caused by the mind or caused by karma, which is basically the same thing, but karma means

30
00:06:12,060 --> 00:06:25,100
in the past. So you've done nasty things in the past. As a result, you have physical and

31
00:06:25,100 --> 00:06:37,340
mental impairment of why people are born with certain disabilities. I think why we suddenly

32
00:06:37,340 --> 00:06:49,740
get cancer or any debilitating disease, multiple sclerosis, etc. Not to mention the diseases

33
00:06:49,740 --> 00:06:58,900
that come from our greed and the top of those are more than the food type. But the fourth

34
00:06:58,900 --> 00:07:06,900
one from the mind means simply the sickness that comes from being mentally sick, from being

35
00:07:06,900 --> 00:07:13,660
angry. Anytime we're anger, this is angry, this is considered a mental illness. The fact

36
00:07:13,660 --> 00:07:19,300
that we have anger is considered to be a mental illness. When we are addicted to things,

37
00:07:19,300 --> 00:07:25,260
when we want things, just wanting something, it's considered to be mental illness. It's

38
00:07:25,260 --> 00:07:35,460
a wrong in the mind. It's a cause of suffering for the mind. It's cause of stress and distress.

39
00:07:35,460 --> 00:07:44,420
Anytime we have delusion, arrogance, conceit, or the fact that we have these in our mind,

40
00:07:44,420 --> 00:07:59,460
this is a cause for illness. It's a type of illness. This is the dumb of the Buddha.

41
00:07:59,460 --> 00:08:05,660
This is how we look at things. Sounds kind of depressing. I suppose I've actually had people

42
00:08:05,660 --> 00:08:14,820
say to me that it's not really fair to call these things illness when they're all manageable.

43
00:08:14,820 --> 00:08:19,100
As though there's a categorical difference between someone who is clinically depressed

44
00:08:19,100 --> 00:08:24,100
and someone who is just depressed. There isn't. There's not a categorical difference.

45
00:08:24,100 --> 00:08:30,980
You can tell you create a category because it's just a matter of degree, and eventually

46
00:08:30,980 --> 00:08:41,380
the perpetuation, the feedback loop that becomes so strong that it becomes unmanageable.

47
00:08:41,380 --> 00:08:49,460
You dislike something or you depress about something and that makes you depressed and more depressed

48
00:08:49,460 --> 00:08:54,900
and then you enter into this feedback loop where you're actually feeding the depression with depression.

49
00:08:54,900 --> 00:09:00,420
You get angry and then you're angry at the fact that someone's been to your angry.

50
00:09:00,420 --> 00:09:09,340
You're still in your anger and it becomes unmanageable. Anxiety becomes anxious that your

51
00:09:09,340 --> 00:09:20,740
anxious afraid of your fear and so on. You just feed them and I feed them and you end up

52
00:09:20,740 --> 00:09:27,140
clinical. It's just a matter of degree and so certainly something that we should be concerned

53
00:09:27,140 --> 00:09:46,740
about and we should be with the gym towards so it doesn't become unmanageable. No questions today.

54
00:09:46,740 --> 00:09:59,140
Do we have questions from us today? Look at the way we have a long argument here. I have the bread in it.

55
00:09:59,140 --> 00:10:06,140
Oh, we have lots of talk.

56
00:10:06,140 --> 00:10:17,180
Why is entertainment dangerous? Understanding the nature of it being impermanence, suffering

57
00:10:17,180 --> 00:10:36,140
a non-stop, what makes it dangerous? Attachment. I've got some good talk I suppose. I'm going to read through it all.

58
00:10:36,140 --> 00:10:46,780
Really think you should put the cube before the question otherwise it's hard to go through here.

59
00:10:46,780 --> 00:10:57,420
I'm going to have to insist. I'm not going to go back to all that talk to find questions.

60
00:10:57,420 --> 00:11:03,260
So to make a question, click on the yellow or green question mark. That puts the right

61
00:11:03,260 --> 00:11:10,060
tang before your question and I can easily identify them. Command satisfying lack of progress

62
00:11:10,060 --> 00:11:21,020
and meditation, my lack of confidence in seeing the hindrance has been due to karma.

63
00:11:21,020 --> 00:11:26,140
Do you have a doubt about your practice? Tell it about your progress? Because that's a hindrance.

64
00:11:29,180 --> 00:11:36,460
I can say doubting, doubting. Maybe not easy to understand what is the progress,

65
00:11:36,460 --> 00:11:41,180
but if you're practicing correctly, it's hard to imagine that you're not progressing.

66
00:11:41,180 --> 00:11:52,380
You're not running to see yourself clearly and refine your character.

67
00:11:52,380 --> 00:12:05,180
When faced with such continued adversary, violence, racism in the world today,

68
00:12:05,900 --> 00:12:10,220
how do we teach others to come to understand the same benefits of meditation?

69
00:12:10,220 --> 00:12:21,500
Without seeing it and concerned with our non-reaction,

70
00:12:29,340 --> 00:12:34,140
I find that it's constantly considered unjust or cruel not to have a fighting opinion

71
00:12:34,140 --> 00:12:46,860
in an existing in a world that demands outrage over every negative situation that arises.

72
00:12:54,540 --> 00:12:57,980
Well, yes, society has messed up nothing new there.

73
00:12:57,980 --> 00:13:08,300
We esteem all the wrong things. We esteem addiction for its worth.

74
00:13:11,260 --> 00:13:16,940
We certainly esteem opinions, views, we esteem outrage,

75
00:13:16,940 --> 00:13:25,020
and esteem for things that in the end, don't turn out to be all that useful.

76
00:13:29,580 --> 00:13:32,860
Let me get caught up in things that end up being inconsequential.

77
00:13:32,860 --> 00:13:58,220
What is fundamental or what is essential as being unessential or what is unessential,

78
00:13:58,220 --> 00:14:05,820
being essential? They saron adiqathay, adiqathay, adiqathay,

79
00:14:07,980 --> 00:14:09,660
micha gohar as encapra.

80
00:14:13,420 --> 00:14:21,340
Adiqathay, they don't obtain such people don't come to this essential

81
00:14:21,340 --> 00:14:33,740
Because they're always feeding on the wrong, feeding in the wrong pasture, nor they let

82
00:14:33,740 --> 00:14:48,540
their mind wander and minds wander in the wrong pastures.

83
00:14:48,540 --> 00:15:03,660
So to answer your question, it's not easy to teach yourself, let alone others.

84
00:15:03,660 --> 00:15:09,020
This is why we focus on our own of being, you become an example to others, it's a better

85
00:15:09,020 --> 00:15:18,380
way, you can see a way to free themselves and to help the world.

86
00:15:18,380 --> 00:15:33,060
But only if you become an example, so work on yourself, if you hold fast, if you hold fast

87
00:15:33,060 --> 00:15:41,860
at the fact that you're not outraged at things.

88
00:15:41,860 --> 00:15:48,260
There will be people who appreciate that and who esteem that and who are affected positively

89
00:15:48,260 --> 00:15:49,900
by that.

90
00:15:49,900 --> 00:15:59,340
Those people who get angry at you, well, it says more about them than about you.

91
00:15:59,340 --> 00:16:11,420
Sometimes better to let people who are raging, let them rage, I mean, yes, but to be patient

92
00:16:11,420 --> 00:16:21,100
with them unmoved.

93
00:16:21,100 --> 00:16:26,380
They kind of silly to get outraged about everything when this is sort of thing outrageous

94
00:16:26,380 --> 00:16:34,380
things have been sort of the norm of humanity and be surprised by them as naive and

95
00:16:34,380 --> 00:16:43,340
a fight for that to change, it's much better to accept it as part of the human condition

96
00:16:43,340 --> 00:16:50,380
and to work with it in the sense of actually trying to change, not getting angry at the

97
00:16:50,380 --> 00:16:56,300
fact that that's part of humanity, that there's evil in humanity, but to work on good

98
00:16:56,300 --> 00:17:03,180
to better yourself to better those around you, to help people to come to let go of their

99
00:17:03,180 --> 00:17:09,260
evil inside, to help people to see they can angry at bad things, is making things worse.

100
00:17:09,260 --> 00:17:16,060
It's just cultivating a habit of anger.

101
00:17:16,060 --> 00:17:21,820
You just end up burnt out, you don't believe this theory, look at those people who get

102
00:17:21,820 --> 00:17:27,940
burnt out from activism, they'll get burnt out from social justice, that kind of thing.

103
00:17:27,940 --> 00:17:33,660
They don't end up helping people not in any meaningful way, well, it's maybe not true,

104
00:17:33,660 --> 00:17:39,700
they do often end up because of, but not because of the anger, they often end up, because

105
00:17:39,700 --> 00:17:45,260
of their determination, they'll end up changing the system, you don't even be angry

106
00:17:45,260 --> 00:17:52,260
to change the system, like some of the civil rights movement in America, for example,

107
00:17:52,260 --> 00:17:57,660
that we hear, but a lot of it wasn't very angry, it was a matter of just having sit-ins

108
00:17:57,660 --> 00:18:06,060
and sitting in the wrong place on the bus and that kind of thing, just to be adamant.

109
00:18:06,060 --> 00:18:10,180
I mean, you still might argue from a Buddhist point of view, but that sort of thing

110
00:18:10,180 --> 00:18:22,700
is much more, much more consumable, or much more acceptable to the Buddhist, but to himself

111
00:18:22,700 --> 00:18:28,780
give silent protesters this one case where the skin was going, it's a long story, and

112
00:18:28,780 --> 00:18:38,180
basically this guy was, I think I was a bastard son of one of the Buddhist relatives,

113
00:18:38,180 --> 00:18:44,540
and so they wouldn't recognize him, but they didn't want to anger him, so they pretended

114
00:18:44,540 --> 00:18:50,180
to recognize him in the end, they wouldn't even let him eat at the same table with him,

115
00:18:50,180 --> 00:19:00,420
I'm so proud, and the skin got so angry, that he had to say about to kill all the

116
00:19:00,420 --> 00:19:07,780
Buddhist relatives, so he went to his army one time, and the Buddha intercepted him, and

117
00:19:07,780 --> 00:19:13,340
the Buddha was sitting there under a tree, but the tree had no leaves, or very few leaves

118
00:19:13,340 --> 00:19:21,380
it was a very, it's almost dead tree, and the king was marching by and he was told

119
00:19:21,380 --> 00:19:28,580
that the Buddha was sitting there and so he got down and went to see the Buddha, and

120
00:19:28,580 --> 00:19:32,420
he said to the Buddha, he was actually, he actually had some faith in the Buddha, even

121
00:19:32,420 --> 00:19:41,580
though he was a more mongering king, and he said to the Buddha, what are you doing when

122
00:19:41,580 --> 00:19:46,540
the Buddha was sitting under this tree with no shade, why don't you find a better tree,

123
00:19:46,540 --> 00:19:53,700
and the Buddha said, oh that's okay, I live under the cool shade of my relatives, so

124
00:19:53,700 --> 00:19:59,380
you didn't even go to the king and tell him, look you're wrong, stop it, and he's

125
00:19:59,380 --> 00:20:11,500
thought to, remind the king, and his relatives were, had good in them, they may have been

126
00:20:11,500 --> 00:20:20,900
pompous jerks, but they were still people, they were still a support to humanity and

127
00:20:20,900 --> 00:20:32,420
some way of support to the Buddha in some way, so the king, they realized so the Buddha benefits

128
00:20:32,420 --> 00:20:39,700
from his relatives, so he turned around, called off the war, except he couldn't keep

129
00:20:39,700 --> 00:20:44,380
down his anger, so he ended up heading off to war again, and again the Buddha did the

130
00:20:44,380 --> 00:20:49,340
same thing, and then the third time, it's really interesting story, the third time the

131
00:20:49,340 --> 00:20:56,300
king says look, I'm just going to do it, and the Buddha sort of reflected on the situation

132
00:20:56,300 --> 00:21:02,180
and didn't go, and decided it wasn't, this wasn't going to come to a good end, there's

133
00:21:02,180 --> 00:21:07,540
nothing he could do to stop it, and many of the Buddha's relatives were wiped out, some

134
00:21:07,540 --> 00:21:16,060
majority of them, because the Buddha just let it happen, and he felt he couldn't, he

135
00:21:16,060 --> 00:21:26,780
would have had to get angry, he would have had to get upset, the sense that you can't

136
00:21:26,780 --> 00:21:33,980
stop people from dying, you can't stop bad things from happening, what we have to stop

137
00:21:33,980 --> 00:21:41,100
is things like anger, and it's like any angry doesn't help, it sends the wrong message,

138
00:21:41,100 --> 00:21:48,860
and much more important is to send the message, stop killing each other, taxi, taxi

139
00:21:48,860 --> 00:21:55,820
to try and stop one killing and one, one catastrophe, it's not all that important, it's

140
00:21:55,820 --> 00:22:03,660
beings, they're in cycle, they're born in dying, born in dying, it's part of the circle

141
00:22:03,660 --> 00:22:17,900
of life, but the mind doesn't die, it dies every moment, but the mind of a person keeps

142
00:22:17,900 --> 00:22:34,500
going, there you go, sickness, alright, we're into questions, we're only one in two questions,

143
00:22:34,500 --> 00:22:45,020
must have answered all the questions last night, in that case, have a good, have a good

144
00:22:45,020 --> 00:22:52,020
evening, everyone, tomorrow I might be busy, I might not, I should be here, I should

145
00:22:52,020 --> 00:23:16,020
be here, I'm not here some days, apologies, I should be here, anyway, have a good night.

