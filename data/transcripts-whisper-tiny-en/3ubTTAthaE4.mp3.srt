1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamupanda.

2
00:00:04,000 --> 00:00:10,000
Today we continue on with 1st 148, which reads as follow.

3
00:00:34,000 --> 00:00:45,000
This agent completely worn out an agony around a nest of disease sickness.

4
00:00:45,000 --> 00:00:58,000
A bhangurang, it is subject to, for it is weak, it is subject to destruction, it is fragile.

5
00:00:58,000 --> 00:01:13,000
Bijityi, Burizandess, this mass of filth breaks.

6
00:01:13,000 --> 00:01:19,000
This one is a reference to the idea of a pot filled with,

7
00:01:19,000 --> 00:01:25,000
because the idea of the body being a pot full of urine and feces, and when you die in your brain breaks,

8
00:01:25,000 --> 00:01:31,000
or a pot that has many holes, it's leaky, it's cracked, and there are leaks.

9
00:01:31,000 --> 00:01:34,000
That's what the body is like.

10
00:01:34,000 --> 00:01:39,000
And when you die, it breaks.

11
00:01:39,000 --> 00:01:44,000
Marandantei ji witau for death is the end of life.

12
00:01:44,000 --> 00:01:50,000
Marandantei ji witau.

13
00:01:50,000 --> 00:01:58,000
Not a very pleasant or upbeat, upbeat verse, but a useful one nonetheless.

14
00:01:58,000 --> 00:02:08,000
So the story goes, and this is the story of the naan utara.

15
00:02:08,000 --> 00:02:13,000
I'm not sure which utara had seen, there were several utara nuns.

16
00:02:13,000 --> 00:02:24,000
But utara was committed, she was quite a committed or devoted recluse bikini.

17
00:02:24,000 --> 00:02:31,000
And for all of her life she went on arms around until she was 120 years old.

18
00:02:31,000 --> 00:02:33,000
Arms are an interesting thing.

19
00:02:33,000 --> 00:02:40,000
You start to appreciate food in a whole new way, and people give it to you.

20
00:02:40,000 --> 00:02:47,000
They have to give you what's left over, or they give to you what is most dear to them.

21
00:02:47,000 --> 00:02:54,000
But having to go out into the village really makes your mind full of the naan to eat.

22
00:02:54,000 --> 00:02:56,000
It's the one thing that we can't escape.

23
00:02:56,000 --> 00:02:58,000
It's this one sickness that we have.

24
00:02:58,000 --> 00:02:59,000
It's the need to eat.

25
00:02:59,000 --> 00:03:03,000
We're addicted to food.

26
00:03:03,000 --> 00:03:12,000
This body requires constant and repeated nourishment day after day after day.

27
00:03:12,000 --> 00:03:17,000
And so you sort of look at food in a whole new way, not as this thing that I must have,

28
00:03:17,000 --> 00:03:19,000
but this thing that I may get.

29
00:03:19,000 --> 00:03:21,000
And if I get, it means my life will continue.

30
00:03:21,000 --> 00:03:24,000
And if I don't get it, my life may end.

31
00:03:24,000 --> 00:03:27,000
You become somewhat liberated.

32
00:03:27,000 --> 00:03:33,000
All I'm trying to say is liberating, I mean it's incredible liberating through its humility.

33
00:03:33,000 --> 00:03:35,000
It's almost as though you're begging.

34
00:03:35,000 --> 00:03:39,000
Of course we're not allowed to beg, but you would walk through the village.

35
00:03:39,000 --> 00:03:48,000
And it's wonderful because people are delighted to offer food to monks.

36
00:03:48,000 --> 00:03:53,000
Not just in Asia, but also in North America.

37
00:03:53,000 --> 00:04:00,000
And then Europe and throughout the world really.

38
00:04:00,000 --> 00:04:05,000
So I would take that as the basis of this story,

39
00:04:05,000 --> 00:04:07,000
because the story goes that there's none.

40
00:04:07,000 --> 00:04:14,000
120 years old having practiced alms around our whole life.

41
00:04:14,000 --> 00:04:21,000
At one time, she saw a particular monk walking on alms around.

42
00:04:21,000 --> 00:04:24,000
It doesn't say anything about this monk.

43
00:04:24,000 --> 00:04:25,000
It says a certain monk.

44
00:04:25,000 --> 00:04:32,000
But perhaps he was enlightened, perhaps he appeared enlightened,

45
00:04:32,000 --> 00:04:38,000
or appeared somehow worthy of respect.

46
00:04:38,000 --> 00:04:42,000
Or perhaps not, perhaps it was a means of humbling him.

47
00:04:42,000 --> 00:04:44,000
She gave him all of his food.

48
00:04:44,000 --> 00:04:46,000
She asked permission if she could give him his food.

49
00:04:46,000 --> 00:04:50,000
And she gave him all of her food.

50
00:04:50,000 --> 00:04:51,000
I don't think I've ever done that before.

51
00:04:51,000 --> 00:04:52,000
I've given some of my food off.

52
00:04:52,000 --> 00:04:53,000
None of the moms around.

53
00:04:53,000 --> 00:04:56,000
You'll meet another monk or a nun.

54
00:04:56,000 --> 00:04:59,000
And especially the nuns, I would often give them food.

55
00:04:59,000 --> 00:05:05,000
Because in Asia, it's not always sure that they're going to get food the same as the monks.

56
00:05:05,000 --> 00:05:08,000
Or honestly, I think I would have realized that they often get more

57
00:05:08,000 --> 00:05:14,000
because people are so appreciative of the rarity of the nun.

58
00:05:14,000 --> 00:05:20,000
Anyway, she gave him all of her food and had none for herself.

59
00:05:20,000 --> 00:05:26,000
I think another thing he could say is that 120, she wasn't really all that concerned with living on.

60
00:05:26,000 --> 00:05:30,000
Whatever her reason she did and went without food for that day.

61
00:05:30,000 --> 00:05:34,000
The next day, she met the monk again and did the same thing.

62
00:05:34,000 --> 00:05:36,000
And so on for a third day.

63
00:05:36,000 --> 00:05:39,000
I wish when she started to become quite weak.

64
00:05:39,000 --> 00:05:43,000
It was an interesting choice that she made of practice.

65
00:05:43,000 --> 00:05:46,000
Probably not a wise one in the end.

66
00:05:46,000 --> 00:05:49,000
On the fourth day, she met the Buddha.

67
00:05:49,000 --> 00:05:59,000
Perhaps the Buddha was interested in sort of maybe jolting her back into proper practice.

68
00:05:59,000 --> 00:06:02,000
Not sure.

69
00:06:02,000 --> 00:06:06,000
But when she met the teacher, I guess it was crowded.

70
00:06:06,000 --> 00:06:10,000
So she stepped back to let the teacher go by.

71
00:06:10,000 --> 00:06:15,000
And when she stepped back, she stepped on her robe and tripped

72
00:06:15,000 --> 00:06:20,000
and fell sprawling on the ground.

73
00:06:20,000 --> 00:06:23,000
Well, the Buddha is an interesting way about it.

74
00:06:23,000 --> 00:06:29,000
Obviously, quite a unique mannerism.

75
00:06:29,000 --> 00:06:34,000
Very much in terms of letting people deal with their karma.

76
00:06:34,000 --> 00:06:37,000
It doesn't appear that he actually tried to help the woman out,

77
00:06:37,000 --> 00:06:40,000
or get someone to help her up.

78
00:06:40,000 --> 00:06:42,000
It was a learning experience for her.

79
00:06:42,000 --> 00:06:44,000
She could get up on her own, I guess.

80
00:06:44,000 --> 00:06:47,000
But what he did is he walked over and he gave her a teaching.

81
00:06:47,000 --> 00:06:50,000
This was a teaching for her.

82
00:06:50,000 --> 00:06:52,000
And he didn't criticize her.

83
00:06:52,000 --> 00:06:54,000
He didn't say, hey, why are you giving?

84
00:06:54,000 --> 00:06:55,000
Why are you so weak?

85
00:06:55,000 --> 00:06:58,000
Why are you not eating that kind of thing?

86
00:06:58,000 --> 00:07:01,000
But he gave her a teaching that was actually somewhat appropriate

87
00:07:01,000 --> 00:07:04,000
because it wasn't really wrong what she was doing.

88
00:07:04,000 --> 00:07:07,000
It was something that could easily be realized in her situation.

89
00:07:07,000 --> 00:07:10,000
And this is the teaching and gamer about her body.

90
00:07:10,000 --> 00:07:13,000
Look at this body that you've made very weak

91
00:07:13,000 --> 00:07:16,000
and then it's weak by very nature.

92
00:07:16,000 --> 00:07:18,000
It's so fragile.

93
00:07:18,000 --> 00:07:22,000
Perhaps it was even the...

94
00:07:22,000 --> 00:07:25,000
It was even a reminder that this body is not something

95
00:07:25,000 --> 00:07:26,000
that you can take lightly.

96
00:07:26,000 --> 00:07:28,000
You can't just abuse it.

97
00:07:28,000 --> 00:07:32,000
Anyway, he taught her this verse.

98
00:07:32,000 --> 00:07:34,000
As a result of the verse, apparently,

99
00:07:34,000 --> 00:07:36,000
she became a sort up on it.

100
00:07:36,000 --> 00:07:41,000
Many beings around were able to gain benefit.

101
00:07:41,000 --> 00:07:44,000
Satika and Tamadees and the whole thing.

102
00:07:44,000 --> 00:07:47,000
The teaching was a great benefit to many.

103
00:07:51,000 --> 00:07:54,000
It's a great benefit to us, I think.

104
00:07:54,000 --> 00:07:59,000
And then all of us are quite young here in Hamilton.

105
00:07:59,000 --> 00:08:01,000
Everybody in second life.

106
00:08:01,000 --> 00:08:03,000
Of course, looks young and beautiful.

107
00:08:03,000 --> 00:08:05,000
We don't really know the truth.

108
00:08:08,000 --> 00:08:09,000
But it doesn't matter.

109
00:08:09,000 --> 00:08:13,000
The commentary says about this Babangurang.

110
00:08:13,000 --> 00:08:15,000
Babangurang.

111
00:08:15,000 --> 00:08:21,000
Babangurang means fragile or subject to Babangity.

112
00:08:21,000 --> 00:08:24,000
I mean, the breakage or destruction.

113
00:08:24,000 --> 00:08:26,000
Easily broken.

114
00:08:26,000 --> 00:08:31,000
It says about this word,

115
00:08:31,000 --> 00:08:37,000
that it means even a young person is frail.

116
00:08:37,000 --> 00:08:42,000
Even young people are fragile.

117
00:08:42,000 --> 00:08:45,000
So the lesson for us, I think,

118
00:08:45,000 --> 00:08:48,000
not so there's so much that can be said,

119
00:08:48,000 --> 00:08:51,000
but to start at the beginning is,

120
00:08:51,000 --> 00:08:54,000
we think of all the ways we look at the body

121
00:08:54,000 --> 00:08:57,000
as being powerful, healthy.

122
00:08:57,000 --> 00:08:59,000
Some people even think of the body as a temple.

123
00:08:59,000 --> 00:09:03,000
We have many ways of looking at the body.

124
00:09:03,000 --> 00:09:08,000
Most of us just have a concept of it being our body.

125
00:09:08,000 --> 00:09:15,000
Subject to our whim, capable of so much.

126
00:09:15,000 --> 00:09:19,000
We may not consciously think of it by this body as capable of so much,

127
00:09:19,000 --> 00:09:22,000
but we take for granted that the body is,

128
00:09:22,000 --> 00:09:25,000
we can do anything we want with the body.

129
00:09:25,000 --> 00:09:27,000
We take our legs for granted that we can walk.

130
00:09:27,000 --> 00:09:30,000
We take our arms and our hands for granted

131
00:09:30,000 --> 00:09:32,000
that we can manipulate all the many things,

132
00:09:32,000 --> 00:09:38,000
anything that we like.

133
00:09:38,000 --> 00:09:42,000
We take our eyes for granted that we can see our ears that we can hear.

134
00:09:42,000 --> 00:09:44,000
We take our teeth for granted.

135
00:09:44,000 --> 00:09:47,000
We take everything for granted.

136
00:09:47,000 --> 00:09:49,000
And you can see where I'm going with this,

137
00:09:49,000 --> 00:09:51,000
that it's quite misleading.

138
00:09:51,000 --> 00:09:53,000
It's one of those many things.

139
00:09:53,000 --> 00:09:57,000
The body is one of many things that we become incredibly attached to,

140
00:09:57,000 --> 00:10:01,000
to our detriment, to our eventual detriment.

141
00:10:01,000 --> 00:10:07,000
People think that old age, the suffering of old age is a part of life

142
00:10:07,000 --> 00:10:09,000
and just have to bear with it.

143
00:10:09,000 --> 00:10:12,000
Really the truth is we set ourselves up for it.

144
00:10:12,000 --> 00:10:15,000
We set ourselves up for a great amount of fear

145
00:10:15,000 --> 00:10:19,000
and worry and anxiety about growing old,

146
00:10:19,000 --> 00:10:23,000
about getting sick and about dying.

147
00:10:23,000 --> 00:10:26,000
How many people are afraid of death?

148
00:10:26,000 --> 00:10:29,000
Afraid of their own death?

149
00:10:29,000 --> 00:10:32,000
Or have a pulse by the thought of dying

150
00:10:32,000 --> 00:10:35,000
because of how intoxicated they are.

151
00:10:35,000 --> 00:10:42,000
The body is such a great producer of chemicals as well and pleasure.

152
00:10:42,000 --> 00:10:47,000
When we run and jump and play and sing and dance,

153
00:10:47,000 --> 00:10:50,000
so many wonderful hormones in the brain

154
00:10:50,000 --> 00:10:55,000
become activated and give us such wonderful pleasure.

155
00:10:55,000 --> 00:11:00,000
So we become intoxicated by this.

156
00:11:00,000 --> 00:11:03,000
To our detriment.

157
00:11:03,000 --> 00:11:05,000
Because we don't become happier,

158
00:11:05,000 --> 00:11:07,000
we don't start to live happier lives.

159
00:11:07,000 --> 00:11:10,000
We just get more and more addicted.

160
00:11:10,000 --> 00:11:16,000
We don't smile more, we don't feel more at peace.

161
00:11:16,000 --> 00:11:19,000
These things come from somewhere else.

162
00:11:19,000 --> 00:11:21,000
These things come from wisdom and understanding

163
00:11:21,000 --> 00:11:23,000
and in fact letting go.

164
00:11:23,000 --> 00:11:25,000
That's why you see old people often quite at peace

165
00:11:25,000 --> 00:11:27,000
because they've had to come to terms

166
00:11:27,000 --> 00:11:30,000
and they've had to realize I can't get all this pleasure

167
00:11:30,000 --> 00:11:33,000
that I wanted.

168
00:11:33,000 --> 00:11:37,000
Old people once they realize they can't have sexual intercourse anymore

169
00:11:37,000 --> 00:11:39,000
once they realize they can't dance anymore

170
00:11:39,000 --> 00:11:44,000
once all of these things fade away.

171
00:11:44,000 --> 00:11:47,000
We become much more peaceful, perhaps not happier

172
00:11:47,000 --> 00:11:50,000
because there still will be residual addiction

173
00:11:50,000 --> 00:11:53,000
and they'll find other ways to find pleasure

174
00:11:53,000 --> 00:11:59,000
but when generally they have to come to terms.

175
00:11:59,000 --> 00:12:01,000
But that which is a good thing

176
00:12:01,000 --> 00:12:04,000
because as I said they tend to be more peaceful

177
00:12:04,000 --> 00:12:06,000
so a person who wants to find peace

178
00:12:06,000 --> 00:12:08,000
will go through our lives agitated, stressed,

179
00:12:08,000 --> 00:12:11,000
thinking we're happy, thinking the body is bringing us happy

180
00:12:11,000 --> 00:12:14,000
but with such stress and suffering

181
00:12:14,000 --> 00:12:16,000
whereas if we took this kind of teaching

182
00:12:16,000 --> 00:12:19,000
even now even as young people

183
00:12:19,000 --> 00:12:23,000
and we saw the body for what it is

184
00:12:23,000 --> 00:12:26,000
a nest of disease, a nest of sickness.

185
00:12:26,000 --> 00:12:29,000
A place where sickness grows, right?

186
00:12:29,000 --> 00:12:31,000
It really is a nest.

187
00:12:31,000 --> 00:12:33,000
It's bacteria and viruses.

188
00:12:33,000 --> 00:12:35,000
They just love our body.

189
00:12:35,000 --> 00:12:37,000
It's a breeding ground for them.

190
00:12:37,000 --> 00:12:40,000
Oh, look at this wonderful nest that has been put here.

191
00:12:40,000 --> 00:12:42,000
It's just walking around touching things

192
00:12:42,000 --> 00:12:45,000
and picking up, picks me up, puts me in its mouth.

193
00:12:45,000 --> 00:12:48,000
Oh, wonderful place to live and grow.

194
00:12:52,000 --> 00:12:55,000
To our benefit, seeing this is not a pessimistic teaching.

195
00:12:55,000 --> 00:12:58,000
It's not meant to make you feel awful.

196
00:12:58,000 --> 00:13:00,000
It's in fact meant to free you

197
00:13:00,000 --> 00:13:03,000
from addiction to the body.

198
00:13:03,000 --> 00:13:06,000
Body is such a curious thing, right?

199
00:13:06,000 --> 00:13:09,000
Scientists have convinced us that it's just sort of

200
00:13:09,000 --> 00:13:14,000
by natural selection, right?

201
00:13:14,000 --> 00:13:16,000
But we would argue,

202
00:13:16,000 --> 00:13:18,000
science has never been well able

203
00:13:18,000 --> 00:13:21,000
to take into account the mind.

204
00:13:25,000 --> 00:13:28,000
Just as I was preparing for this talk,

205
00:13:28,000 --> 00:13:32,000
one of my Sri Lankan friends in Florida sent me a quote

206
00:13:32,000 --> 00:13:34,000
we've been arguing about science

207
00:13:34,000 --> 00:13:37,000
and whether science actually says anything about the nature

208
00:13:37,000 --> 00:13:38,000
of the world.

209
00:13:38,000 --> 00:13:41,000
They sent me a bore quote, a quote of Niels Bohr

210
00:13:41,000 --> 00:13:43,000
and Bohr is wonderful.

211
00:13:43,000 --> 00:13:48,000
He says science doesn't have physics.

212
00:13:48,000 --> 00:13:51,000
He says there is no quantum world.

213
00:13:51,000 --> 00:13:56,000
Physics doesn't attempt to explain the nature of reality.

214
00:13:56,000 --> 00:13:59,000
It just describes our experience

215
00:13:59,000 --> 00:14:07,000
of it or our encounters with it or whatever.

216
00:14:07,000 --> 00:14:11,000
My bring that up is because it calls into question

217
00:14:11,000 --> 00:14:13,000
the idea that the body even exists

218
00:14:13,000 --> 00:14:15,000
or that this idea of natural selection

219
00:14:15,000 --> 00:14:19,000
is even the right way of looking at things.

220
00:14:19,000 --> 00:14:21,000
Certainly from a Buddhist point of view,

221
00:14:21,000 --> 00:14:23,000
we look at it quite differently.

222
00:14:23,000 --> 00:14:25,000
It was the mind playing a large part.

223
00:14:25,000 --> 00:14:27,000
The mind did this.

224
00:14:27,000 --> 00:14:29,000
We did this to ourselves,

225
00:14:29,000 --> 00:14:33,000
which sort of makes you look at it in a different way.

226
00:14:33,000 --> 00:14:36,000
When you look at the body of something we've done this,

227
00:14:36,000 --> 00:14:40,000
obviously, well, Jesus, this is the best I could have done.

228
00:14:40,000 --> 00:14:47,000
No titanium bones or skin that was a little tougher.

229
00:14:47,000 --> 00:14:51,000
Maybe this facial hair I could have done without that.

230
00:14:51,000 --> 00:14:54,000
Right.

231
00:14:54,000 --> 00:14:57,000
So much we would have done better.

232
00:14:57,000 --> 00:15:00,000
Teeth, make my teeth a little stronger.

233
00:15:00,000 --> 00:15:02,000
Sugar, what is what's up with this?

234
00:15:02,000 --> 00:15:04,000
Sugar rots my teeth.

235
00:15:04,000 --> 00:15:07,000
How puny.

236
00:15:07,000 --> 00:15:10,000
Bacteria, bacteria get on your teeth

237
00:15:10,000 --> 00:15:12,000
and they eat the sugar and they rot.

238
00:15:12,000 --> 00:15:14,000
They corrode.

239
00:15:14,000 --> 00:15:16,000
They corrode the enamel of your teeth.

240
00:15:16,000 --> 00:15:20,000
What a worthless, poorly made instrument.

241
00:15:20,000 --> 00:15:24,000
It gets a little sick and dies.

242
00:15:24,000 --> 00:15:33,000
Their machines that we make now last longer than this body.

243
00:15:33,000 --> 00:15:38,000
But the body is a manifestation of all that we are.

244
00:15:38,000 --> 00:15:40,000
It's pretty pitiful, really.

245
00:15:40,000 --> 00:15:42,000
This is all we are.

246
00:15:42,000 --> 00:15:46,000
We don't think like this, but we should.

247
00:15:46,000 --> 00:15:48,000
Couldn't I have done better?

248
00:15:48,000 --> 00:15:50,000
We can. Angels do better.

249
00:15:50,000 --> 00:15:51,000
God do better.

250
00:15:51,000 --> 00:15:54,000
Wow, their bodies must be wonderful.

251
00:15:54,000 --> 00:16:00,000
They can always become an angel or a God.

252
00:16:00,000 --> 00:16:02,000
So these are different ways.

253
00:16:02,000 --> 00:16:06,000
There's so much that we can learn from this idea,

254
00:16:06,000 --> 00:16:08,000
this way of looking at the world.

255
00:16:08,000 --> 00:16:11,000
They talk about the nature of the body for a long time.

256
00:16:11,000 --> 00:16:15,000
But the delved right into how it's useful for us as meditators.

257
00:16:15,000 --> 00:16:19,000
That's the most important.

258
00:16:19,000 --> 00:16:24,000
I think this is sort of a good description of the sort of thing

259
00:16:24,000 --> 00:16:27,000
that you'll come to understand through meditation.

260
00:16:27,000 --> 00:16:31,000
You see that the body is how we contact.

261
00:16:31,000 --> 00:16:33,000
The body is how we experience sensations.

262
00:16:33,000 --> 00:16:40,000
The body is responsible for much of our pleasure and pain.

263
00:16:40,000 --> 00:16:47,000
Well, for all of the physical pleasure and physical pain that we experience,

264
00:16:47,000 --> 00:16:53,000
to become to see that the body is something we have to carry around with us.

265
00:16:53,000 --> 00:16:58,000
When you meditate, well, it'd be nice if we can meditate and leave our bodies.

266
00:16:58,000 --> 00:17:01,000
Do some transcendental meditation.

267
00:17:01,000 --> 00:17:04,000
The more you meditate when you're here doing a meditation course,

268
00:17:04,000 --> 00:17:08,000
you start to realize, this body isn't something useful.

269
00:17:08,000 --> 00:17:11,000
Something that I have to carry for and take care of.

270
00:17:11,000 --> 00:17:14,000
All I want to do is meditate and look, I have to go and eat.

271
00:17:14,000 --> 00:17:16,000
I have to sleep.

272
00:17:16,000 --> 00:17:17,000
I have to adjust my position.

273
00:17:17,000 --> 00:17:18,000
I can't just sit.

274
00:17:18,000 --> 00:17:20,000
I have to get up and walk.

275
00:17:20,000 --> 00:17:21,000
I can't walk forever.

276
00:17:21,000 --> 00:17:24,000
I have to sit down.

277
00:17:24,000 --> 00:17:25,000
I itch.

278
00:17:25,000 --> 00:17:26,000
It gets hot.

279
00:17:26,000 --> 00:17:27,000
It gets cold.

280
00:17:27,000 --> 00:17:28,000
I urinate.

281
00:17:28,000 --> 00:17:29,000
I defecate.

282
00:17:29,000 --> 00:17:33,000
Sometimes I get sick.

283
00:17:33,000 --> 00:17:39,000
Never mind if you fall down and break a limb or get cut,

284
00:17:39,000 --> 00:17:44,000
get bruised, poke an eye out.

285
00:17:44,000 --> 00:17:49,000
So many things can happen to this body.

286
00:17:49,000 --> 00:17:52,000
So as meditators, this sort of thing comes up,

287
00:17:52,000 --> 00:17:56,000
and I think talking about it is always talking about the things that we might experience in meditation

288
00:17:56,000 --> 00:17:58,000
or should experience in meditation.

289
00:17:58,000 --> 00:18:01,000
It's quite useful because when we do realize them,

290
00:18:01,000 --> 00:18:04,000
we affirm what we're experiencing and clarify it for us.

291
00:18:04,000 --> 00:18:05,000
Yeah.

292
00:18:05,000 --> 00:18:08,000
That is what I'm seeing.

293
00:18:08,000 --> 00:18:10,000
Realizing that this body is not quick,

294
00:18:10,000 --> 00:18:11,000
what I thought it was.

295
00:18:11,000 --> 00:18:16,000
Realizing how silly we were to think of how wonderful this body is.

296
00:18:16,000 --> 00:18:18,000
It's so silly.

297
00:18:18,000 --> 00:18:22,000
You want to understand how silly it is.

298
00:18:22,000 --> 00:18:25,000
Dogs think that same things about their bodies.

299
00:18:25,000 --> 00:18:27,000
They look at our bodies and say,

300
00:18:27,000 --> 00:18:31,000
oh, that's ugly, but their own bodies.

301
00:18:31,000 --> 00:18:32,000
Wow, those are beautiful.

302
00:18:32,000 --> 00:18:41,000
And bodies of other dogs, rats, bugs, insects.

303
00:18:41,000 --> 00:18:45,000
Insects will look at each other and think, wow, you're beautiful.

304
00:18:45,000 --> 00:18:47,000
They'll look at their bodies.

305
00:18:47,000 --> 00:18:49,000
If you have no birds, birds are wonderful in this way.

306
00:18:49,000 --> 00:18:51,000
Always preening their feathers.

307
00:18:51,000 --> 00:18:54,000
I think they have to do it to fly as well,

308
00:18:54,000 --> 00:18:58,000
pretty clear that they're per their vein.

309
00:18:58,000 --> 00:19:02,000
I would attach their to their bodies,

310
00:19:02,000 --> 00:19:07,000
flee infested bodies,

311
00:19:07,000 --> 00:19:12,000
smelly, dirty bodies.

312
00:19:12,000 --> 00:19:15,000
Yeah, we don't have to be negative, so negative.

313
00:19:15,000 --> 00:19:17,000
In fact, that's all conceptual.

314
00:19:17,000 --> 00:19:20,000
Body is not even all conceptual.

315
00:19:20,000 --> 00:19:24,000
On a deep level in our meditation practice,

316
00:19:24,000 --> 00:19:27,000
this teaching has to be taken on a momentary level.

317
00:19:27,000 --> 00:19:31,000
So the real destruction of the body is every moment.

318
00:19:31,000 --> 00:19:34,000
When you extend your arm, that arises.

319
00:19:34,000 --> 00:19:36,000
When you stop, it ceases.

320
00:19:36,000 --> 00:19:39,000
When you flex your arm, you're born and die.

321
00:19:39,000 --> 00:19:43,000
That movement is what we mean by body.

322
00:19:43,000 --> 00:19:47,000
It arises and it ceases in that moment.

323
00:19:47,000 --> 00:19:50,000
When you feel pain, physical pain,

324
00:19:50,000 --> 00:19:56,000
that's an experience that arises and ceases.

325
00:19:56,000 --> 00:19:59,000
So we come to this sort of thing that Bohr came to,

326
00:19:59,000 --> 00:20:01,000
that this quantum physicist came to.

327
00:20:01,000 --> 00:20:04,000
The world doesn't really exist.

328
00:20:04,000 --> 00:20:12,000
All that we can say about the world is what we experience.

329
00:20:12,000 --> 00:20:15,000
So we have to live, and we have to work on both these levels.

330
00:20:15,000 --> 00:20:19,000
We have to work on the conceptual level to help us

331
00:20:19,000 --> 00:20:23,000
see through our attachments to concepts.

332
00:20:23,000 --> 00:20:32,000
Once we see that the body is not a source of refuge,

333
00:20:32,000 --> 00:20:36,000
we see that it's subject to dissolution and impermanence.

334
00:20:36,000 --> 00:20:39,000
When we see deeper that it doesn't actually even exist,

335
00:20:39,000 --> 00:20:41,000
that we die every moment.

336
00:20:41,000 --> 00:20:46,000
The body is born and dies with every experience.

337
00:20:46,000 --> 00:20:49,000
Then we really let go of the body.

338
00:20:49,000 --> 00:20:52,000
I mean that's where the greatest piece comes from.

339
00:20:52,000 --> 00:20:54,000
Because when you see the body is not even existing,

340
00:20:54,000 --> 00:20:59,000
it's really just experiences arising and ceasing.

341
00:20:59,000 --> 00:21:02,000
Then old age sickness and death can't come to you.

342
00:21:02,000 --> 00:21:06,000
That's a real good reason to give us impetus to practice.

343
00:21:06,000 --> 00:21:10,000
When we realize that our attachment to the body is a real problem,

344
00:21:10,000 --> 00:21:12,000
we're going to die soon.

345
00:21:12,000 --> 00:21:14,000
We're going to get old, we're going to get sick,

346
00:21:14,000 --> 00:21:16,000
and we're going to die.

347
00:21:16,000 --> 00:21:18,000
Much to our benefit if we can practice meditation

348
00:21:18,000 --> 00:21:22,000
and learn to let go of it and learn to see through it.

349
00:21:22,000 --> 00:21:27,000
We're going to learn to see this truth that the body doesn't even exist.

350
00:21:27,000 --> 00:21:31,000
Then when it gets old, sick and dies, you don't lose anything.

351
00:21:31,000 --> 00:21:33,000
People say Buddhism is nihilistic.

352
00:21:33,000 --> 00:21:36,000
Buddhism is denialistic, I suppose.

353
00:21:36,000 --> 00:21:43,000
We deny the existence of so many things that most people take for granted as existing.

354
00:21:43,000 --> 00:21:47,000
I think that Boricode is more apt than he perhaps understood.

355
00:21:47,000 --> 00:21:53,000
Although I think he was a rather wise individual.

356
00:21:53,000 --> 00:22:00,000
In that it's really only experience that exists.

357
00:22:00,000 --> 00:22:02,000
So there you go.

358
00:22:02,000 --> 00:22:07,000
That's the demo part of this evening that's our demo talk for today.

359
00:22:07,000 --> 00:22:33,000
I wish you a good practice and a peaceful happy life.

