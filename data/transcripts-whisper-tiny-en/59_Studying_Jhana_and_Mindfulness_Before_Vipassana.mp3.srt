1
00:00:00,000 --> 00:00:06,320
Studying Janus and mindfulness before we pass on a question

2
00:00:07,680 --> 00:00:21,120
Should the four foundations of mindfulness in the five Janus factors of concentration and absorption be learned before we pass on a meditation can be correctly used

3
00:00:21,120 --> 00:00:38,640
Answer a person might cultivate the five Janus factors based on a conceptual object before cultivating be passed on this way of practice can be quite powerful

4
00:00:39,200 --> 00:00:49,200
but it seems to generally take longer as the time span focused on a conceptual object could have been used in cultivating the

5
00:00:49,200 --> 00:01:17,840
purpose and as well those who practice we pass on from the beginning seem to attain results in a few weeks or at most a month whereas those who practice tranquility first seem generally to take months or even years to get the same results assuming they ever get around to be pass on a meditation at all

6
00:01:17,840 --> 00:01:30,800
Some people seem to get stuck on the calm and bliss of tranquility never realizing that there is something higher than those states

7
00:01:30,800 --> 00:01:47,800
As for the four foundations of mindfulness if the question is do you have to practice them before practicing we pass on a meditation then this is a misunderstanding about we pass on a meditation

8
00:01:47,800 --> 00:02:14,800
because we pass on a meditation is the practice of the four foundations of mindfulness you practice the four foundations of mindfulness to see clearly we pass on a there are ways to use the four foundations of mindfulness to cultivate tranquility but mostly they are used to focus on and see clearly ultimate reality

9
00:02:14,800 --> 00:02:31,800
If the question is whether one has to study the four foundations before practicing you do not have to study them in detail but obviously you have to know about them because they are the practice itself

10
00:02:31,800 --> 00:02:52,800
knowing and understanding how to practice in regard to each of the four foundations at least on a basic level is essential without understanding what it is that you intend to practice you cannot be expected to practice properly

