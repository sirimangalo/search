WEBVTT

00:00.000 --> 00:26.000
Okay, good evening everyone, welcome to our evening talk.

00:26.000 --> 00:47.000
We have a new meditator today from Winnipeg who is actually an old meditator.

00:47.000 --> 01:03.000
He's finally found the time to coordinate and do the full foundation course in our tradition.

01:03.000 --> 01:16.000
At the same time we're losing a meditator, a graduate who has just finished the foundation course

01:16.000 --> 01:27.000
passed with flying colors, graduated and moving on, but he'll be back.

01:27.000 --> 01:35.000
The dhamma is addictive, well not really is it.

01:35.000 --> 01:40.000
That's the problem with the dhamma actually is that it's not addictive.

01:40.000 --> 01:53.000
It's the opposite of clinging and so you don't create an addiction towards it.

01:53.000 --> 01:58.000
But you do create an attraction, it's kind of a paradox in a sense.

01:58.000 --> 02:08.000
You do somehow become addicted to it, addicted through wisdom.

02:08.000 --> 02:16.000
It appears though you're an addict, person can't stop meditating, you must be addicted to it.

02:16.000 --> 02:20.000
Which is just a fool's way of showing their misunderstanding.

02:20.000 --> 02:35.000
The inability to understand the lack of alarm to superficial, just a superficial comment.

02:35.000 --> 02:39.000
Wisdom is a lot like addiction in that way.

02:39.000 --> 02:47.000
When you know something's right, you can't avoid it.

02:47.000 --> 03:00.000
When you know something's wrong, you can't take part in it.

03:00.000 --> 03:06.000
So congratulations for that.

03:06.000 --> 03:14.000
I propose tonight's talk, as tonight's talk I was going to talk.

03:14.000 --> 03:21.000
I thought the topic would be desire, but there's a little bit more to it.

03:21.000 --> 03:35.000
I'm going to talk about the general concept of the path.

03:35.000 --> 03:44.000
Meaning the path of desire versus the path of renunciation.

03:44.000 --> 03:51.000
Is there a way of understanding our options?

03:51.000 --> 03:53.000
Because this is a true dichotomy.

03:53.000 --> 03:56.000
You can't, there's no in between.

03:56.000 --> 03:58.000
It's one or the other.

03:58.000 --> 04:01.000
Mahasi Sayedas says they're diametrically opposed.

04:01.000 --> 04:09.000
I don't know what the actual Burmese words he used are probably not exact translation, but

04:09.000 --> 04:14.000
this is their opposites.

04:14.000 --> 04:19.000
You can't do both.

04:19.000 --> 04:30.000
Or rather, they lead an opposite direction, so most of us find ourselves in the middle being

04:30.000 --> 04:40.000
pulled in both direction.

04:40.000 --> 04:44.000
Sometimes engaging in enjoying nice sensations.

04:44.000 --> 04:50.000
You might have pleasant sights or sounds or smells or tastes or feelings.

04:50.000 --> 04:55.000
You might engage in them repeatedly.

04:55.000 --> 05:19.000
But they're not complementary, it's the point.

05:19.000 --> 05:32.000
So the context is we had an interesting class last night on the theory of the study of religion.

05:32.000 --> 05:54.000
That was on the question of whether phenomena like sports, sports, loyalty, team loyalty,

05:54.000 --> 06:03.000
or fandom, like the Trekkies or the Jedi Knights.

06:03.000 --> 06:10.000
Our guest speaker was a put down Jedi Knight on the census as his religion.

06:10.000 --> 06:16.000
He explained why that wasn't actually the word, I think.

06:16.000 --> 06:27.000
Not obvious reasons for doing it, but the discussion was whether certain phenomena that appeared

06:27.000 --> 06:37.000
to be quite religious or at least involving a great amount of sincerity and dedication,

06:37.000 --> 06:44.000
whether they could be called religious.

06:44.000 --> 06:55.000
As I won't go into great details, an interesting conversation, a couple of points that came up were...

06:55.000 --> 07:09.000
Well, the main point was that they don't touch upon anything meaningful.

07:09.000 --> 07:19.000
Could you argue that something has to touch upon what is ultimately meaningful for it to be a religion?

07:19.000 --> 07:26.000
And so I mean, I was the one who posed this and I suggested that you could argue that something is only a religion

07:26.000 --> 07:30.000
and this is what the courts often argue.

07:30.000 --> 07:35.000
There's been lots of challenges to the tax exempt status of religions,

07:35.000 --> 07:45.000
and that it should include things like smoking marijuana or it should include vegan movements,

07:45.000 --> 07:49.000
veganism or raw food movements.

07:49.000 --> 08:06.000
It should include the Church of the Flying Spaghetti Monster, for example.

08:06.000 --> 08:18.000
But they said that these various movements don't have cohesive doctrine that is sincerely held regarding ultimate questions of ultimate concern.

08:18.000 --> 08:23.000
The meaning of life, etc.

08:23.000 --> 08:28.000
Which in the end I think is an unfair assessment, but that's not the point.

08:28.000 --> 08:39.000
The point in the end was that loyalty to a sports team may seem quite religious.

08:39.000 --> 08:46.000
And so I said, well, I can understand, I can accept that it might be a religion, but it's a pretty silly religion.

08:46.000 --> 08:48.000
It's the point.

08:48.000 --> 08:50.000
And this is the interesting point of it all.

08:50.000 --> 08:58.000
Not what is a religion and what is not a religion, but the fact that we can rank religions.

08:58.000 --> 09:04.000
We're not allowed to, and this is sort of the real problem with the word religion is that

09:04.000 --> 09:08.000
call something a religion and suddenly you have to respect it.

09:08.000 --> 09:15.000
Suddenly it's equal to all other religions and so the concern with allowing something to be called

09:15.000 --> 09:21.000
a religion is that well, then you have to respect it and who wants to think of

09:21.000 --> 09:31.000
fan worship of Captain Kirk as the same as the worship of Jesus Christ or Buddha or so on.

09:31.000 --> 09:38.000
I want to be able to distinguish, I don't think I'm setting up a straw man.

09:38.000 --> 09:44.000
I think many people want to distinguish because they consider religion to be something in a category of its own.

09:44.000 --> 09:50.000
Two e-gennarists, that's Latin.

09:50.000 --> 10:08.000
It's a fancy Latin phrase that means of its own type, something in its own, in a category of its own.

10:08.000 --> 10:19.000
And so where this is all going is the recognition that there are many paths.

10:19.000 --> 10:32.000
But denying that all paths lead to the same goal, that all paths are equally valid, that all paths should be considered equally,

10:32.000 --> 10:54.000
be given the same sort of attention.

10:54.000 --> 11:07.000
And so that led today during my French lesson, I was talking to my French tutor and I gave him a lesson in Buddhism at the end, trying to explain it in French and then switching to English.

11:07.000 --> 11:17.000
And his question was why shouldn't you engage in enjoyable things if they make you happy?

11:17.000 --> 11:24.000
And he was trying to struggle with it and to argue, but be careful because he didn't want to threaten my religion.

11:24.000 --> 11:29.000
He obviously didn't agree with me, but he didn't want to get into her that just debate.

11:29.000 --> 11:42.000
So he phrased how things must be or that you can't do things, because all things you do must be, must advance you as a person.

11:42.000 --> 11:47.000
I said, well, no, actually, it's not like that.

11:47.000 --> 11:53.000
It's a question of what you want out of life and where you're heading.

11:53.000 --> 12:09.000
I think this is what is unfamiliar to a lot of ordinary people is that there exists paths, there exist methods,

12:09.000 --> 12:18.000
that lead beyond this ordinary state of existence that most people are intellectually comfortable with.

12:18.000 --> 12:23.000
Most people are intellectually comfortable with suffering.

12:23.000 --> 12:28.000
They are physically or practically comfortable with it.

12:28.000 --> 12:38.000
They moan and complain like everybody else, but if you ask them, hey, wouldn't you like to change?

12:38.000 --> 12:43.000
The way the world works?

12:43.000 --> 12:46.000
I mean, really, if you ask them, would you like to practice meditation?

12:46.000 --> 12:52.000
I've got this practice where you could become completely free from all the sufferings of life.

12:52.000 --> 12:59.000
Most people, I think their initial reaction is, okay, suffering is okay.

12:59.000 --> 13:04.000
I'm okay with this.

13:04.000 --> 13:10.000
But really, what's saying that is the desire they have and an attachment,

13:10.000 --> 13:19.000
there are many attachments to pleasures and to ambitions and so on.

13:19.000 --> 13:25.000
And then the ego behind it, the identification, so the idea of leaving behind some sorrow,

13:25.000 --> 13:37.000
the idea of nibana is frightening, horrific even, abhorrent to most people, to not be born again.

13:37.000 --> 13:45.000
I mean, actually, many people are struggling under the delusion that we only have one life.

13:45.000 --> 13:51.000
And to deny this, the pleasures of this life is one life.

13:51.000 --> 14:09.000
How horrible would that be? What a horrific thought.

14:09.000 --> 14:18.000
So it's important to clarify, we're not talking about giving up happiness in order to give up suffering.

14:18.000 --> 14:22.000
We're not talking about an even trade here.

14:22.000 --> 14:30.000
These aren't on an equal footing.

14:30.000 --> 14:34.000
When we talk about the ordinary path of desire and diversion,

14:34.000 --> 14:41.000
the ordinary path of desire, aversion, anger, delusion, that most people are on.

14:41.000 --> 14:47.000
We have all these things and engage in them regularly.

14:47.000 --> 14:51.000
This is a very coarse path.

14:51.000 --> 15:01.000
This is a path that is fraught with problems and is working on a level of constant delusion,

15:01.000 --> 15:07.000
of constant confusion and darkness, lack of clarity.

15:07.000 --> 15:18.000
The mind for such an individual, for most individuals, is a sleep, diffused, distracted,

15:18.000 --> 15:22.000
muddied, muddled, all these things.

15:22.000 --> 15:33.000
Of course, constantly agitated, constantly excited,

15:33.000 --> 15:42.000
and fraught with worry and fear and anxiety and stress.

15:42.000 --> 15:49.000
Even when seeking out the objects of pleasure or even when attaining them,

15:49.000 --> 15:59.000
there is a distraction, there is lack of clarity in the mind.

15:59.000 --> 16:04.000
And so really all we're talking about in Buddhism is not giving anything up,

16:04.000 --> 16:08.000
per se, not directly.

16:08.000 --> 16:14.000
It's about a path that is on a higher level and higher in a very pejorative sense,

16:14.000 --> 16:17.000
or whatever the opposite or pejorative is.

16:17.000 --> 16:20.000
It really is a pejorative lower.

16:20.000 --> 16:29.000
That's even a phrase. It's a really, truly lower path to seek out central pleasure.

16:29.000 --> 16:36.000
He no gamo, pouto, pouto, genico, analeo, another sanchito,

16:36.000 --> 16:39.000
but called it inferior.

16:39.000 --> 16:46.000
Gamo, the way of the village.

16:46.000 --> 16:53.000
All we're doing in meditation practice is creating a higher state of consciousness,

16:53.000 --> 17:00.000
a greater sense of clarity.

17:00.000 --> 17:04.000
And through the increase of clarity, things change.

17:04.000 --> 17:09.000
We refine our pursuits.

17:09.000 --> 17:17.000
We discard the pursuits that are clearly causing us stress and suffering and problem.

17:17.000 --> 17:20.000
Clearly now that our minds are more clear.

17:20.000 --> 17:22.000
It really is that simple.

17:22.000 --> 17:27.000
It's not a matter of deciding, should I give up happiness in order to be free from suffering?

17:27.000 --> 17:29.000
It's not at all like that.

17:29.000 --> 17:31.000
In fact, greater happiness comes.

17:31.000 --> 17:40.000
Our refined, pure, exalted, blissful, wonderful sense of happiness comes.

17:40.000 --> 17:44.000
Of course, my meditators are all like, yeah, right.

17:44.000 --> 17:51.000
I'm here torturing people in Canada, and then I talk about bliss and happiness.

17:51.000 --> 17:53.000
Well, our graduates know.

17:53.000 --> 17:56.000
Our graduates can tell you.

17:56.000 --> 18:04.000
It's a great struggle to rise above the, rise above the dirt, the merc,

18:04.000 --> 18:11.000
to break free from the chains, the chains of desire.

18:11.000 --> 18:24.000
But it's a struggle unequivocally worthwhile.

18:24.000 --> 18:40.000
And this isn't just a claim that I make him, and this is really how we should understand

18:40.000 --> 18:44.000
the practice and how we should be clearly evident from the practice that we do.

18:44.000 --> 18:45.000
We're not judging.

18:45.000 --> 18:48.000
We're not even trying to change ourselves.

18:48.000 --> 18:59.000
Beyond the simple change of seeing our experiences, even our own minds, and our own habits, clearly.

18:59.000 --> 19:03.000
The wheat puss in the, seeing clearly.

19:03.000 --> 19:06.000
It's not some hard to understand concept.

19:06.000 --> 19:09.000
Things arise and cease.

19:09.000 --> 19:20.000
The ordinary state of mind is to, to fair blindly grasping and clinging to anything,

19:20.000 --> 19:27.000
and cultivating habits with no rhyme or reason.

19:27.000 --> 19:29.000
It's like turning on the light.

19:29.000 --> 19:36.000
When you shine the light in the darkness disappears.

19:36.000 --> 19:40.000
When you're able to see it quite easily.

19:40.000 --> 19:45.000
And suddenly, quite clear, come all the things that were previously mysterious.

19:45.000 --> 19:46.000
Why do I suffer?

19:46.000 --> 19:48.000
Why am I depressed?

19:48.000 --> 19:56.000
How do I free myself from desire and anger and aversion?

19:56.000 --> 20:05.000
I'll become clear.

20:05.000 --> 20:09.000
So there you go.

20:09.000 --> 20:14.000
I mean, that's about all I had to say tonight, just some food for thought.

20:14.000 --> 20:23.000
And I guess including the encouragement, not to be, not to lose, lose sight of this,

20:23.000 --> 20:28.000
or to lose faith, or to be caught up in the pursuit of desire,

20:28.000 --> 20:39.000
the pursuit of ordinary happiness,

20:39.000 --> 20:45.000
to engage in, and pursue, clarity of mind,

20:45.000 --> 20:50.000
ability to see things as they are more clearly than before.

20:50.000 --> 20:56.000
It allows us to refine our happiness and to create greater states of peace

20:56.000 --> 21:02.000
and goodness in our hearts and to free ourselves from all the troubles that come from

21:02.000 --> 21:06.000
evil and wholesome mind states.

21:06.000 --> 21:08.000
So good for you, everyone.

21:08.000 --> 21:11.000
Keep up the good work.

21:11.000 --> 21:13.000
All right, there you go.

21:13.000 --> 21:17.000
That's the dhamma for tonight.

21:17.000 --> 21:21.000
Thank you all.

21:21.000 --> 21:26.000
If anyone has any questions, I'm happy to answer them.

21:51.000 --> 21:56.000
Thank you.

22:21.000 --> 22:30.000
Thank you for coming, everyone.

22:30.000 --> 22:32.000
I guess there are no questions.

22:32.000 --> 22:36.000
If there are no questions, we'll say goodnight then.

22:36.000 --> 22:40.000
And then there's a question.

22:40.000 --> 22:42.000
He started by mentioning meditation as addicting,

22:42.000 --> 22:47.000
and so my question is when did the Buddha mention the path as being beautiful in the beginning,

22:47.000 --> 22:52.000
in the middle, and as we most often do not focus in the beauties of life?

22:52.000 --> 22:56.000
Yeah, the word isn't quite beautiful.

22:56.000 --> 22:57.000
It's caliana.

22:57.000 --> 23:05.000
Caliana means good, bright, or beautiful would be so bad.

23:05.000 --> 23:08.000
Super.

23:08.000 --> 23:10.000
Super would be beautiful.

23:10.000 --> 23:14.000
But caliana, I can't remember the etymology,

23:14.000 --> 23:17.000
but it's a specific word.

23:17.000 --> 23:20.000
We use it for a friend, like a caliana mitta is a beautiful friend,

23:20.000 --> 23:23.000
but there's nothing to do with their good looks.

23:23.000 --> 23:31.000
It's just a wonderful friend, an awesome friend.

23:31.000 --> 23:35.000
So caliana means good in the beginning,

23:35.000 --> 23:37.000
and that's often translated in that way.

23:37.000 --> 23:40.000
It's good in the beginning, good in the middle.

23:40.000 --> 23:40.040
It's the bhayana, brahma chari

23:45.000 --> 23:47.000
Thang sa bhayan jianang,

23:47.000 --> 23:40.440
Kayvala paripun nang par RM

23:54.440 --> 23:57.080
And it is the brahma chari fast, that is,

23:57.080 --> 24:01.060
raw complete, in both its letter and its meaning,

24:01.060 --> 24:03.980
meaning that all the teachings are there,

24:03.980 --> 24:07.000
and the meaning is perfect,

24:07.000 --> 24:21.000
Gave a parypundang totally and completely complete, parysudang and pure.

24:21.000 --> 24:26.000
The practice isn't addictive, I was trying to say it seems addictive, because people

24:26.000 --> 24:39.000
who engage in it, it leads you on, the more you gain, the more you are the inclination

24:39.000 --> 24:53.000
to gain or to strive for gain, because of wisdom, not because of attachment.

24:53.000 --> 25:02.000
Gave a parypundang.

25:02.000 --> 25:26.000
Gave a parypundang.

25:26.000 --> 25:55.000
I've posted a bug report for the broadcasting software that I use.

25:55.000 --> 26:03.000
Other people are confirming the bug, and they say just wait for the next version of this

26:03.000 --> 26:10.000
software to come out, unless I can figure out how to compile it, which is a mystery

26:10.000 --> 26:19.000
to me how that works, but there are ways of getting the software.

26:19.000 --> 26:27.000
Anyway, so at some point soon the point is I'll probably be doing live YouTube broadcasts again.

26:27.000 --> 26:34.000
Which doesn't mean we can't do second life as well, because I can always log into second life and just minimize it.

26:34.000 --> 26:48.000
The thing about Linux, it seems like I can port the audio to four different sources at once, which is kind of cool.

26:48.000 --> 26:56.000
I don't know if that happens in Windows as well, but anyway, let's end it there then.

26:56.000 --> 26:58.000
Thank you all for coming out.

26:58.000 --> 27:01.000
It's great to have a crowd every night.

27:01.000 --> 27:25.000
We'll try to be back here again tomorrow.

