1
00:00:00,000 --> 00:00:19,360
This morning, I continued talking about the things which lead to success and prosperity,

2
00:00:19,360 --> 00:00:29,000
and often the sense, we call it the highest blessings.

3
00:00:29,000 --> 00:00:52,000
Today, we continue on with Qarau, this translated as respect, or image, or something like that.

4
00:00:52,000 --> 00:00:59,000
Today, we talk about respect for the practice of a pamanda.

5
00:00:59,000 --> 00:01:09,000
A pamanda is a difficult word to translate to English, although the meaning is clear.

6
00:01:09,000 --> 00:01:17,000
The pamanda means negligence.

7
00:01:17,000 --> 00:01:24,000
The pamanda means the opposite of negligence or non-negligence.

8
00:01:24,000 --> 00:01:33,000
You can translate it as vigilance or diligence or heatfulness,

9
00:01:33,000 --> 00:01:49,000
meaning of pamanda is that we should be mindful at all times.

10
00:01:49,000 --> 00:02:01,000
Not only be mindful at all times, but altogether for things.

11
00:02:01,000 --> 00:02:08,000
We should be free from anger or ill will towards other beings.

12
00:02:08,000 --> 00:02:14,000
We should be always mindful.

13
00:02:14,000 --> 00:02:20,000
We should be equanimists or level-headed.

14
00:02:20,000 --> 00:02:28,000
Our minds should be unmoved by good things or things which arise.

15
00:02:28,000 --> 00:02:37,000
We should stay equanimists in the face of whatever might arise.

16
00:02:37,000 --> 00:02:41,000
Composed, our minds be composed of whatever arises.

17
00:02:41,000 --> 00:02:50,000
When good things arise, our minds instead get greedy or attached to them.

18
00:02:50,000 --> 00:02:57,000
Instead, we set ourselves on being mindful.

19
00:02:57,000 --> 00:03:03,000
We don't let green come and take over our minds.

20
00:03:03,000 --> 00:03:07,000
When anger might arise, when we see your hair, smell,

21
00:03:07,000 --> 00:03:12,000
how bad things arise.

22
00:03:12,000 --> 00:03:21,000
Instead of letting anger arise, we need to be internally composed or level-headed,

23
00:03:21,000 --> 00:03:29,000
balanced in the face of the thing of this.

24
00:03:29,000 --> 00:03:40,000
And to train ourselves to become free from craving.

25
00:03:40,000 --> 00:03:50,000
This is what we are going to call the kapamanda for vigilance or heatfulness.

26
00:03:50,000 --> 00:04:05,000
The respect for this means that we should practice respecting the Lord with teaching on kapamanda.

27
00:04:05,000 --> 00:04:10,000
In fact, the last words of the Lord Buddha before He passed away,

28
00:04:10,000 --> 00:04:21,000
we had a masam kara, a pamatina, pamatina, pamatina.

29
00:04:21,000 --> 00:04:33,000
Everything which arises is of the nature to pass away.

30
00:04:33,000 --> 00:04:41,000
For this reason, we should strive on work hard to fulfill the teaching of kapamanda,

31
00:04:41,000 --> 00:04:54,000
fulfill the teaching of vigilance.

32
00:04:54,000 --> 00:05:02,000
When we practice meditation, we should not think that there is also some occasion

33
00:05:02,000 --> 00:05:11,000
for taking a vacation from our practice.

34
00:05:11,000 --> 00:05:20,000
Maybe in the morning we practice, but in the afternoon we let our minds wander

35
00:05:20,000 --> 00:05:24,000
and fall into greed or fall into anger.

36
00:05:24,000 --> 00:05:35,000
If it is work hard to cut off the wandering mind, cut off the mind which attaches

37
00:05:35,000 --> 00:05:43,000
to external objects or internal objects.

38
00:05:43,000 --> 00:05:50,000
It is like a simile yesterday of the man carrying a pot of oil.

39
00:05:50,000 --> 00:05:55,000
I have already talked about kapamanda.

40
00:05:55,000 --> 00:05:59,000
Today I just talked briefly about kapamanda.

41
00:05:59,000 --> 00:06:06,000
I have to give another story to talk about kapamanda.

42
00:06:06,000 --> 00:06:11,000
I am going to go to kapamanda.

43
00:06:11,000 --> 00:06:18,000
The most negligent person, no person that existed in the time of kapamanda.

44
00:06:18,000 --> 00:06:22,000
He killed altogether 999 people.

45
00:06:22,000 --> 00:06:29,000
His plan was to kill 1000 people.

46
00:06:29,000 --> 00:06:37,000
His plan was to kill his mother who would be the 1000th victim.

47
00:06:37,000 --> 00:06:41,000
The Lord Buddha knew that this was going to happen.

48
00:06:41,000 --> 00:06:47,000
Of course, if he killed his mother, he would never dance to become free from suffering in this life.

49
00:06:47,000 --> 00:07:07,000
The Lord Buddha went to visit Angulimala and so many people protested.

50
00:07:07,000 --> 00:07:20,000
In the end, Angulimala became a student of the Lord Buddha.

51
00:07:20,000 --> 00:07:23,000
He became a monk.

52
00:07:23,000 --> 00:07:26,000
Cut off his hair and beard.

53
00:07:26,000 --> 00:07:30,000
Put on the orange robes.

54
00:07:30,000 --> 00:07:32,000
In the end, he became an Arab.

55
00:07:32,000 --> 00:07:37,000
The Lord Buddha said, you hope to be from a chitmapatasu and from a chitm.

56
00:07:37,000 --> 00:07:47,000
So, among those kambapasi, he became a pamut algat and you are.

57
00:07:47,000 --> 00:07:51,000
Though in the past, someone was negligent.

58
00:07:51,000 --> 00:07:56,000
Later on, they are no longer negligent.

59
00:07:56,000 --> 00:08:03,000
When later on, they become someone who is not negligent, they light up the whole world,

60
00:08:03,000 --> 00:08:15,000
just like the moon, which lights up the world when it comes up behind it, from behind a cloud.

61
00:08:15,000 --> 00:08:21,000
This is the teaching of a pamanda.

62
00:08:21,000 --> 00:08:30,000
As we should always be mindful, we should not be negligent in our practice or negligent in our training of our minds.

63
00:08:30,000 --> 00:08:41,000
We should work very hard to fight against our natural tendencies, to greed or to anger,

64
00:08:41,000 --> 00:08:50,000
that we can develop the tendency to not be greedy, to not be angry.

65
00:08:50,000 --> 00:09:00,000
The last one in the series on respect is respect for hospitality.

66
00:09:00,000 --> 00:09:11,000
That we should receive guests.

67
00:09:11,000 --> 00:09:16,000
We should be able to receive other people.

68
00:09:16,000 --> 00:09:21,000
Now, receiving other people is in two ways.

69
00:09:21,000 --> 00:09:29,000
One, we should be ready to receive other people in terms of giving them objects that they might need.

70
00:09:29,000 --> 00:09:37,000
Food and shelter and clothing and medicine.

71
00:09:37,000 --> 00:09:51,000
We should also receive hospitality in terms of giving guests or visitors, giving them the dhamma.

72
00:09:51,000 --> 00:09:55,000
We should always remember that whenever people come to visit us.

73
00:09:55,000 --> 00:09:58,000
What we can give to them is two things.

74
00:09:58,000 --> 00:10:05,000
We can give them items that they might need and we can give them the dhamma.

75
00:10:05,000 --> 00:10:14,000
In the dhamma, it doesn't mean we should sit around and talk with them.

76
00:10:14,000 --> 00:10:20,000
Try to teach them this or teach them that.

77
00:10:20,000 --> 00:10:34,000
It means we have to encourage them to go and practice, encourage them to carry out the practice of bypassing the meditation.

78
00:10:34,000 --> 00:10:40,000
In fact, this is an important point in receiving visitors.

79
00:10:40,000 --> 00:10:55,000
We have to be careful not to think that we have to find out all sorts of information about them or sit around and talk with them for a long time.

80
00:10:55,000 --> 00:10:58,000
Visit with them.

81
00:10:58,000 --> 00:11:12,000
When visitors come, we have to encourage them quickly to go and practice because they don't have so much time and we also don't have so much time.

82
00:11:12,000 --> 00:11:18,000
We don't know when is going to be the end of our time here.

83
00:11:18,000 --> 00:11:22,000
No one knows whether death will come tomorrow.

84
00:11:22,000 --> 00:11:29,000
We waste our time talking or visiting.

85
00:11:29,000 --> 00:11:40,000
This is not the way to receive guests, receive people who come to us or wherever we go when we go home or when we stay here.

86
00:11:40,000 --> 00:11:45,000
We have to receive guests by putting them to practice right away.

87
00:11:45,000 --> 00:11:53,000
Giving them the opportunity to practice, not wasting their time visiting the chatting or so on.

88
00:11:53,000 --> 00:11:59,000
Giving them the things that they need to practice and then give them them to teach them how to practice.

89
00:11:59,000 --> 00:12:04,000
Once they know how to practice and send them off right away to practice.

90
00:12:04,000 --> 00:12:15,000
For more time we have for them to practice.

91
00:12:15,000 --> 00:12:22,000
We can really be said to have been hospitable in the highest sense.

92
00:12:22,000 --> 00:12:27,000
This is the end of respect thing, having respect.

93
00:12:27,000 --> 00:12:36,000
All in all we have to have a respectful attitude towards these six things.

94
00:12:36,000 --> 00:12:45,000
The training and not being negligent and didn't receiving guests.

95
00:12:45,000 --> 00:13:05,000
Next we go on, the next blessing is new at Oja.

96
00:13:05,000 --> 00:13:16,000
I think highly of oneself and put other people down.

97
00:13:16,000 --> 00:13:21,000
It is not to hold on to oneself.

98
00:13:21,000 --> 00:13:26,000
Not to hold on to the idea of self that I am this, I am that.

99
00:13:26,000 --> 00:13:37,000
This is me, that is me, this is mine, this is under my control.

100
00:13:37,000 --> 00:13:44,000
New at Oja means to be humble.

101
00:13:44,000 --> 00:13:58,000
Like Sariputah, Sariputah is an very good example of humility.

102
00:13:58,000 --> 00:14:15,000
Like in himself to an outcast in the time of the Buddha there were people who were outcast from Hindu society.

103
00:14:15,000 --> 00:14:27,000
They had to hit a stick against the ground, they had to carry a walking stick and beat it against the ground to make noise wherever they went.

104
00:14:27,000 --> 00:14:45,000
So that the higher class people, the Brahmins and the Katya's and other classes would know that they were coming and wouldn't be able to avoid their touch.

105
00:14:45,000 --> 00:15:01,000
Sariputah likened himself to one of these people, he said, as far as how I look at myself, it is the same.

106
00:15:01,000 --> 00:15:16,000
Because it happened that when he was walking by a young monk, his robe touched the monk as he was walking.

107
00:15:16,000 --> 00:15:26,000
And the monk sprinted around that Sariputah had hit him, Sariputah had hit this monk.

108
00:15:26,000 --> 00:15:38,000
And Sariputah said for someone who was attached to themselves, then maybe they could commit such a deed, but for me he said, I am like an outcast.

109
00:15:38,000 --> 00:15:44,000
He said, I am like the earth.

110
00:15:44,000 --> 00:15:49,000
People may do what they want to the earth, but the earth never complain.

111
00:15:49,000 --> 00:15:58,000
If we want to be really humble, we have to be like the earth.

112
00:15:58,000 --> 00:16:07,000
We want to be like the water, we want to be like the air.

113
00:16:07,000 --> 00:16:20,000
Like the air is because anyone can come and write on the earth or attack the air.

114
00:16:20,000 --> 00:16:25,000
You can come and attack the air and write on the earth.

115
00:16:25,000 --> 00:16:32,000
Nothing stays just in the same way someone who is humble,

116
00:16:32,000 --> 00:16:40,000
whether someone says bad things to them or does bad things to them, it doesn't stay with the person who is humble.

117
00:16:40,000 --> 00:16:43,000
They start to attach to self.

118
00:16:43,000 --> 00:16:50,000
They see that these things are only body and mind, sound or sight or smell or taste or feelings or thoughts.

119
00:16:50,000 --> 00:16:58,000
And they come and they go, and one doesn't hold on to them.

120
00:16:58,000 --> 00:17:02,000
One should be like the water in the same way.

121
00:17:02,000 --> 00:17:12,000
When you write on the water or when you people can urinate in the water or defecate in the water or throw things in the water and the water never complain.

122
00:17:12,000 --> 00:17:15,000
Water accepts it all.

123
00:17:15,000 --> 00:17:23,000
People write on the water or cut the water or hit the water and never complain.

124
00:17:23,000 --> 00:17:28,000
On the same with the earth, they will do what they want with the earth.

125
00:17:28,000 --> 00:17:30,000
They dig the earth and cut the earth.

126
00:17:30,000 --> 00:17:34,000
They spoil the earth and the earth never complain.

127
00:17:34,000 --> 00:17:36,000
I'm going to say, this is how we should be.

128
00:17:36,000 --> 00:17:40,000
This is the teaching of Neemat on the child.

129
00:17:40,000 --> 00:17:42,000
Very important for meditators.

130
00:17:42,000 --> 00:17:44,000
We have to let go of ourselves.

131
00:17:44,000 --> 00:17:48,000
Not always to think about the bad things that people did to us.

132
00:17:48,000 --> 00:17:51,000
No matter what they did to us.

133
00:17:51,000 --> 00:17:55,000
They didn't do it to anyone, no one did anything.

134
00:17:55,000 --> 00:17:58,000
These things come and go there and I see.

135
00:17:58,000 --> 00:18:01,000
They have to let go.

136
00:18:01,000 --> 00:18:04,000
Let go in the mind.

137
00:18:04,000 --> 00:18:07,000
These are only things that happened in the past.

138
00:18:07,000 --> 00:18:14,000
They happened because of causes and conditions.

139
00:18:14,000 --> 00:18:17,000
All that's left now is our own thought.

140
00:18:17,000 --> 00:18:21,000
We just let go of our own thought.

141
00:18:21,000 --> 00:18:26,000
We don't go back to it at all.

142
00:18:26,000 --> 00:18:33,000
Next, we have Sun2T.

143
00:18:33,000 --> 00:18:38,000
Sun2T means contentment.

144
00:18:38,000 --> 00:18:40,000
This is a great blessing.

145
00:18:40,000 --> 00:18:45,000
But in Buddhism, we need to have more Sun2T and Sun2T.

146
00:18:45,000 --> 00:18:48,000
Both of them have been praised by the Buddha.

147
00:18:48,000 --> 00:18:57,000
Contentment and discontent are both been praised by the Buddha.

148
00:18:57,000 --> 00:19:00,000
Contentment means we have to be content with what are

149
00:19:00,000 --> 00:19:05,000
requisites we have.

150
00:19:05,000 --> 00:19:08,000
Whatever clothing we have, we have to be content with it.

151
00:19:08,000 --> 00:19:12,000
This is why monks and nuns, they wear ragrobes.

152
00:19:12,000 --> 00:19:20,000
They wear robes made of pieces of cloth, stitched together.

153
00:19:20,000 --> 00:19:27,000
Then with whatever clothing they get.

154
00:19:27,000 --> 00:19:31,000
Content with food, we have to be content with whatever

155
00:19:31,000 --> 00:19:36,000
alms would we get.

156
00:19:36,000 --> 00:19:39,000
Content with shelter, whatever lodging we get, we have to be

157
00:19:39,000 --> 00:19:44,000
content, not worrying that it's too hard or too cold or too

158
00:19:44,000 --> 00:19:48,000
noisy.

159
00:19:48,000 --> 00:19:54,000
This is not too small.

160
00:19:54,000 --> 00:20:01,000
We should be content with whatever lodging we have.

161
00:20:01,000 --> 00:20:11,000
With what medicines we have, not to worry about finding this

162
00:20:11,000 --> 00:20:15,000
medicine or that medicine that we should be content to

163
00:20:15,000 --> 00:20:20,000
take whatever medicine we get.

164
00:20:20,000 --> 00:20:23,000
We need not to be worried about sicknesses which might arise

165
00:20:23,000 --> 00:20:27,000
and the body always trying to fix the body with this or

166
00:20:27,000 --> 00:20:35,000
that medicine.

167
00:20:35,000 --> 00:20:38,000
But us and to team is we have to be discontent.

168
00:20:38,000 --> 00:20:43,000
Discontent means discontent with whatever

169
00:20:43,000 --> 00:20:48,000
of attainment which we have gained in the practice.

170
00:20:48,000 --> 00:20:51,000
We have to always think to ourselves that there is a higher

171
00:20:51,000 --> 00:20:57,000
state which we have to get to gain.

172
00:20:57,000 --> 00:21:01,000
Though we have heard or we know about the

173
00:21:01,000 --> 00:21:09,000
attainment of Arahan, the attainment of freedom from

174
00:21:09,000 --> 00:21:12,000
suffering, we still haven't reached the attainment of freedom from

175
00:21:12,000 --> 00:21:13,000
suffering.

176
00:21:13,000 --> 00:21:16,000
We still have greed, we still have anger, we still have

177
00:21:16,000 --> 00:21:19,000
delusion in these things can still arise in the future.

178
00:21:19,000 --> 00:21:22,000
If we are not careful, they will come back again and again to

179
00:21:22,000 --> 00:21:25,000
haunt us.

180
00:21:25,000 --> 00:21:29,000
So we should not be content with the level of practice which we

181
00:21:29,000 --> 00:21:33,000
have attained, we should never rest content.

182
00:21:33,000 --> 00:21:36,000
This is one thing we should never be content with.

183
00:21:36,000 --> 00:21:38,000
Just the word Buddha is teaching.

184
00:21:38,000 --> 00:21:43,000
If we rest content with those spiritual attainment we have, it's

185
00:21:43,000 --> 00:21:47,000
most likely that more bad and evil states will come into our

186
00:21:47,000 --> 00:21:49,000
hearts.

187
00:21:49,000 --> 00:21:54,000
This is being negligent neglecting our duties.

188
00:21:54,000 --> 00:21:59,000
And so greed, more greed and anger and delusion will arise and

189
00:21:59,000 --> 00:22:03,000
cover over our minds again.

190
00:22:05,000 --> 00:22:10,000
If we content with a small attainment, not even be

191
00:22:10,000 --> 00:22:16,000
content with the scriptures they say, not even the

192
00:22:16,000 --> 00:22:20,000
Buddha said, not even be content with an academy.

193
00:22:20,000 --> 00:22:23,000
We can know we've cut off greed and anger, we should not be

194
00:22:23,000 --> 00:22:28,000
content with that until we have cut off delusion.

195
00:22:32,000 --> 00:22:36,000
This is the teaching on some community.

196
00:22:36,000 --> 00:22:46,000
Next, we have Kattan.

197
00:22:46,000 --> 00:22:52,000
Kattan, you can call in and have some of them.

198
00:22:52,000 --> 00:22:56,000
I think I'll see those for tomorrow.

199
00:22:56,000 --> 00:22:59,000
Time for morning chanting.

200
00:22:59,000 --> 00:23:04,000
Please continue to practice this morning.

201
00:23:04,000 --> 00:23:07,000
I guess we have an ordination.

202
00:23:07,000 --> 00:23:10,000
Congratulations.

203
00:23:10,000 --> 00:23:35,000
You can go ahead and probably start chanting.

