1
00:00:00,000 --> 00:00:04,340
Hi, welcome back to Buddhism 101.

2
00:00:04,340 --> 00:00:09,820
Today we'll be continuing to talk about morality with part two, which is called the

3
00:00:09,820 --> 00:00:12,680
Garden of the Census.

4
00:00:12,680 --> 00:00:20,600
In Buddhism, we recognize there to be six senses, and it's nothing magical or spiritual.

5
00:00:20,600 --> 00:00:27,400
It's just the five ordinary physical senses, seeing, hearing, smelling, tasting, feeling,

6
00:00:27,400 --> 00:00:31,440
and an additional six sense being just the mind, which is thinking.

7
00:00:31,440 --> 00:00:35,400
So seeing, hearing, smelling, tasting, feeling, thinking, these are considered to be the

8
00:00:35,400 --> 00:00:41,280
six doors, if you will, to experience or doors to perception.

9
00:00:41,280 --> 00:00:48,560
So all of our experiences occur at one or another of these doors or over these senses.

10
00:00:48,560 --> 00:00:55,360
The Garden of the Census simply means to prevent the mind from cultivating judgements,

11
00:00:55,360 --> 00:01:04,280
projections, extrapolations of reality, so subjectivity to try to keep the mind at some level

12
00:01:04,280 --> 00:01:09,960
of objectivity, where we experience the object as it is, because this is what will allow

13
00:01:09,960 --> 00:01:17,680
us to focus and to see things as they are, cultivating concentration in wisdom, which are

14
00:01:17,680 --> 00:01:20,200
what morality is meant to lead to.

15
00:01:20,200 --> 00:01:27,280
So the practice of guarding the Census is actually a much more intrinsic or essential

16
00:01:27,280 --> 00:01:30,360
form of morality from a Buddhist perspective.

17
00:01:30,360 --> 00:01:38,000
The consideration or the idea is that extrapolations, judgements, projections, these are

18
00:01:38,000 --> 00:01:47,200
all somehow immoral in the sense that in the specific sense that they lead more to suffering

19
00:01:47,200 --> 00:01:59,480
and to stress, they lead us to misunderstand or to follow our addictions and our desires

20
00:01:59,480 --> 00:02:04,480
and our versions and our hatreds and our fears and our worries, they tend to cultivate

21
00:02:04,480 --> 00:02:14,280
these negative states of mind as opposed to simply states of peace and clarity of mind.

22
00:02:14,280 --> 00:02:21,960
They tend to dull and to confuse and to darken the mind.

23
00:02:21,960 --> 00:02:32,560
So this is really the beginning or the entry level for meditation, practicing in a Buddhist

24
00:02:32,560 --> 00:02:39,920
sense, where we begin to pull the mind back from its judgements, from its partialities,

25
00:02:39,920 --> 00:02:48,920
categorizing of reality and arbitrary categories of acceptable, unacceptable, good and bad,

26
00:02:48,920 --> 00:02:54,120
right, wrong, mean, mind and so on, without having taken the time to really understand

27
00:02:54,120 --> 00:02:59,560
what is good, what is bad, what is right and what is wrong from an objective point of view.

28
00:02:59,560 --> 00:03:06,080
So from this point of view, morality is simply bringing the mind back to reality.

29
00:03:06,080 --> 00:03:09,240
When you see something to have it just be seeing, when you hear something to have it

30
00:03:09,240 --> 00:03:14,560
just be hearing and nothing else, when you experience something to have it just be experiencing

31
00:03:14,560 --> 00:03:20,880
that thing and this is accomplished by, well, it's accomplished in many ways, it's

32
00:03:20,880 --> 00:03:26,440
firstly accomplished by simply forcing the mind, guarding the mind in the sense of when

33
00:03:26,440 --> 00:03:33,400
the mind goes out, bringing it back, saying no, no, no, just forbidding the mind to

34
00:03:33,400 --> 00:03:39,200
go out, it can be done by clamping the mind down or repressing the desires and so on.

35
00:03:39,200 --> 00:03:45,160
This is a conventional sort of guarding the senses.

36
00:03:45,160 --> 00:03:52,040
One way is physically to prevent yourself from giving rise to likes or dislikes.

37
00:03:52,040 --> 00:03:58,160
So when you're walking around in the city or driving around, driving or whatever, riding

38
00:03:58,160 --> 00:04:04,960
in the car, to not be looking around sightseeing or trying to take in what's going on around

39
00:04:04,960 --> 00:04:11,080
you, you have to not allow yourself to see the things that would create desire or aversion

40
00:04:11,080 --> 00:04:13,800
or worry or fear or so on.

41
00:04:13,800 --> 00:04:17,800
It doesn't really close your eyes or lower your gaze or so on.

42
00:04:17,800 --> 00:04:25,640
To avoid specific situations, to not go to bars or go to not watch entertainment, entertaining

43
00:04:25,640 --> 00:04:31,600
shows or listen to music or so on, knowing that it somehow excites you and creates habits

44
00:04:31,600 --> 00:04:35,240
of addiction and attachment.

45
00:04:35,240 --> 00:04:41,360
But obviously this is only going to be a temporary fix and I think it's worth noting

46
00:04:41,360 --> 00:04:49,760
that this will never give rise to concentration or wisdom by itself.

47
00:04:49,760 --> 00:04:55,520
So from the Buddhist perspective, morality has to be something deeper on an experiential

48
00:04:55,520 --> 00:05:02,280
level where even when you see something that would potentially attract you, you are able

49
00:05:02,280 --> 00:05:03,760
to be objective about it.

50
00:05:03,760 --> 00:05:08,160
You're able to focus your mind just on the essence of the object.

51
00:05:08,160 --> 00:05:10,600
So again, seeing is just seeing.

52
00:05:10,600 --> 00:05:12,960
And this is accomplished by something that we call mindfulness.

53
00:05:12,960 --> 00:05:17,080
This is something that I'll get into in a later video.

54
00:05:17,080 --> 00:05:24,760
But for now, basically the practice of morality requires this ability to capture the

55
00:05:24,760 --> 00:05:26,320
object in its essence.

56
00:05:26,320 --> 00:05:30,920
So when you see something, you remind yourself, this is seeing, just saying to yourself

57
00:05:30,920 --> 00:05:31,920
seeing.

58
00:05:31,920 --> 00:05:35,800
When you hear something, you remind yourself hearing, when you smell smelling, when you

59
00:05:35,800 --> 00:05:42,040
taste tasting, when you feel feeling, when you think thinking, guarding the senses from

60
00:05:42,040 --> 00:05:50,000
any sort of extrapolation or projection, so the mind grasps the seeing as just an experience

61
00:05:50,000 --> 00:05:54,000
and nothing more grasps the experience just as it is.

62
00:05:54,000 --> 00:06:02,400
This is considered to be morality because it keeps the mind from any judgment that would

63
00:06:02,400 --> 00:06:13,200
cause one to perform any moral deeds, any speech that is performed as a result of our experiences

64
00:06:13,200 --> 00:06:19,520
as therefore pure, any actions that are performed as a result of our experience of the

65
00:06:19,520 --> 00:06:22,720
world that are therefore pure and objective.

66
00:06:22,720 --> 00:06:27,760
So instead of when we see something getting angry about it and speaking or acting in

67
00:06:27,760 --> 00:06:36,760
a way to come suffering, we're here or smell or so on, we're able to act rationally and

68
00:06:36,760 --> 00:06:39,040
with clarity of mind.

69
00:06:39,040 --> 00:06:45,720
So not much to say about it, it's quite a simple concept, but it's really the very essence

70
00:06:45,720 --> 00:06:50,720
of Buddhist meditation practice, this beginning of the path of cultivating morality where

71
00:06:50,720 --> 00:06:56,280
we begin to pull the mind back and eventually the mind will become accustomed to seeing

72
00:06:56,280 --> 00:06:57,280
things as they are.

73
00:06:57,280 --> 00:07:01,200
This will give rise to concentration and therefore wisdom to things that we'll talk about

74
00:07:01,200 --> 00:07:02,760
in future videos.

75
00:07:02,760 --> 00:07:09,400
But for now, another aspect of morality, really probably the essence of what we mean by

76
00:07:09,400 --> 00:07:11,960
morality in a Buddhist context.

77
00:07:11,960 --> 00:07:18,800
So very important and important for us to understand at the very outset so that we can get

78
00:07:18,800 --> 00:07:23,200
an idea of what is meant by Buddhist morality and Buddhist meditation practice.

79
00:07:23,200 --> 00:07:50,280
So thank you for tuning in, this is Buddhism 101.

