1
00:00:00,000 --> 00:00:05,000
Why don't we remember our past lives?

2
00:00:05,000 --> 00:00:10,000
Let's give the simple answers.

3
00:00:10,000 --> 00:00:15,000
The simple answer is because you don't have the power to remember things that far back.

4
00:00:15,000 --> 00:00:17,000
It's just that simple.

5
00:00:17,000 --> 00:00:21,000
But we have this idea while I was an adult and I should remember all those things.

6
00:00:21,000 --> 00:00:32,000
But this was before you were this, you know, fetus in your mother's womb, which you also don't remember.

7
00:00:32,000 --> 00:00:34,000
No, everyone can verify for themselves.

8
00:00:34,000 --> 00:00:36,000
They were in their mother's womb.

9
00:00:36,000 --> 00:00:50,000
It's not really, you know, there's ultrasounds and, you know, there's baby pictures of my birth.

10
00:00:50,000 --> 00:00:55,000
I was born in our house at home, not in a hospital and we had pictures of it all.

11
00:00:55,000 --> 00:00:59,000
So I'm pretty clear that I came out of my mother's womb, but I don't remember it.

12
00:00:59,000 --> 00:01:04,000
No recollection whatsoever.

13
00:01:04,000 --> 00:01:07,000
So that's the short answer, is that it's just too far back.

14
00:01:07,000 --> 00:01:09,000
It's before all that.

15
00:01:09,000 --> 00:01:18,000
So the long answer is, or the long answer.

16
00:01:18,000 --> 00:01:22,000
The long answer is that you can remember your past lives.

17
00:01:22,000 --> 00:01:26,000
You just have to work at it, just like with any kind of memory.

18
00:01:26,000 --> 00:01:31,000
Now we don't think of actually working and remembering things because our minds don't work very well in general.

19
00:01:31,000 --> 00:01:38,000
When your mind is untrained, when you haven't tried to train your mind, you don't even think it possible.

20
00:01:38,000 --> 00:01:41,000
I think memory is something that comes by itself, right?

21
00:01:41,000 --> 00:01:47,000
Pops on that times, but you can actually train your mind and there are people who do it.

22
00:01:47,000 --> 00:01:55,000
We all heard of these people with photographic memories or people who memorize telephone books.

23
00:01:55,000 --> 00:02:02,000
They train themselves by memorizing telephone books, developing this memory.

24
00:02:02,000 --> 00:02:03,000
Monks do it as well.

25
00:02:03,000 --> 00:02:10,000
They memorize all of the Buddhist teaching, 45 volumes or 47 volumes.

26
00:02:10,000 --> 00:02:15,000
They brag about this and we hear about these monks who memorize it all.

27
00:02:15,000 --> 00:02:20,000
And then I talked to a monk about it and he said, oh no, they never really remember it all.

28
00:02:20,000 --> 00:02:21,000
That's just what they say.

29
00:02:21,000 --> 00:02:27,000
He said, you couldn't remember it all, but you get good.

30
00:02:27,000 --> 00:02:34,000
You know where everything is because the point is just to be able to pass the exams.

31
00:02:34,000 --> 00:02:38,000
And the examiner can't ask you everything.

32
00:02:38,000 --> 00:02:43,000
So you just have to know what he's going to ask you and you have to know where everything is.

33
00:02:43,000 --> 00:02:47,000
Basically, get the gist of the entire diptica.

34
00:02:47,000 --> 00:02:55,000
Anyway, so they do this and that's still quite an incredible accomplishment.

35
00:02:55,000 --> 00:03:03,000
So the mind can be trained in memory, but it can also be trained to remember things that you've forgotten.

36
00:03:03,000 --> 00:03:10,000
It can be trained to go backwards, but you have to work at it really, really hard.

37
00:03:10,000 --> 00:03:13,000
And the work is in your meditation.

38
00:03:13,000 --> 00:03:18,000
When you practice some at the meditation, you develop yourself as I was saying,

39
00:03:18,000 --> 00:03:25,000
switching objects, then switching genres, then switching objects in Janus.

40
00:03:25,000 --> 00:03:31,000
So you'd enter into the first five Janus with the earth casino.

41
00:03:31,000 --> 00:03:35,000
And then you'd enter in the first five Janus with the air casino.

42
00:03:35,000 --> 00:03:41,000
And then the water, the earth element, air element, water element, fire element.

43
00:03:41,000 --> 00:03:46,000
And then you'd switch, you'd go first Janus, third Janus, fifth Janus, or something like that.

44
00:03:46,000 --> 00:03:52,000
Then you'd switch objects, first Janus, earth element, first Janus, air element, first Janus.

45
00:03:52,000 --> 00:03:59,000
And you'd get, you know, you're actually playing this game and it's scientific.

46
00:03:59,000 --> 00:04:09,000
You know, the development, like check, check, check, and going back and forth, to the point that your mind is incredibly sharp

47
00:04:09,000 --> 00:04:17,000
and able to direct itself in any direction that it chooses.

48
00:04:17,000 --> 00:04:25,000
So you're able to develop these states of intense tranquility.

49
00:04:25,000 --> 00:04:35,000
And then what you do is you sit down and you think of what you just did before you sat down.

50
00:04:35,000 --> 00:04:39,000
And then you think about what you just did before that.

51
00:04:39,000 --> 00:04:44,000
And then you think about what happened before that before that and you go back like this.

52
00:04:44,000 --> 00:04:50,000
As soon as you get stuck and can't remember anything, can't remember something, you stop.

53
00:04:50,000 --> 00:04:57,000
And you develop the Janus, again, you develop your meditation and bring it to the earth, the earth element, air element, fire element, water element.

54
00:04:57,000 --> 00:05:02,000
And there's these methods, it's in the, it's in the, we see the manga and you can read about exactly.

55
00:05:02,000 --> 00:05:11,000
I don't remember exactly what they do, but it's something about going from one to the other and then skipping and then going backwards and forwards and so on.

56
00:05:11,000 --> 00:05:15,000
And you do that again, and then you come back and try and start all over again.

57
00:05:15,000 --> 00:05:21,000
Sit down, remember the first thing you did before you sat down and the first thing before that.

58
00:05:21,000 --> 00:05:29,000
And with work, eventually you can go back days, months, years.

59
00:05:29,000 --> 00:05:36,000
Yeah, it's kind of like regressive, if no cis except it's incredibly systematic.

60
00:05:36,000 --> 00:05:41,000
It's been systematized to the point where it's no longer just chance.

61
00:05:41,000 --> 00:05:51,000
I know people do, I've heard of one man who took it a lazy route and he was actually able to do this and actually able to remember something that seemed like past life.

62
00:05:51,000 --> 00:05:57,000
But he did sort of just a hypnosis where he didn't go one, one moment by one moment by one moment.

63
00:05:57,000 --> 00:06:00,000
But technically speaking, that's what you should do.

64
00:06:00,000 --> 00:06:10,000
If you really want to be clear about it, you have to be clear that it's going in sequence and remember back, back, back, back, all the way to your birth.

65
00:06:10,000 --> 00:06:18,000
And then into the womb, to the point where you can remember what was going on in the womb, all the way back to the point of conception.

66
00:06:18,000 --> 00:06:21,000
Now at the point of conception, it's much more difficult.

67
00:06:21,000 --> 00:06:27,000
And this is why regressive hypnosis takes several sessions.

68
00:06:27,000 --> 00:06:33,000
But as the texts say, eventually you can break through that.

69
00:06:33,000 --> 00:06:36,000
And once you've broken through to a past life, it gets easier.

70
00:06:36,000 --> 00:06:40,000
And then you can go back further and further from one life to the next one.

71
00:06:40,000 --> 00:06:44,000
Because the familiarity is there.

72
00:06:44,000 --> 00:06:53,000
The technique has been learned and the mind becomes familiar with the technique.

73
00:06:53,000 --> 00:06:56,000
So there's the long answer. You can remember your past life.

74
00:06:56,000 --> 00:06:58,000
I mean, we don't remember them now.

75
00:06:58,000 --> 00:07:01,000
It's an easy answer because we can't remember that far back.

76
00:07:01,000 --> 00:07:08,000
But that's just because of the state of our minds, because there's so much information that we're inundated with.

77
00:07:08,000 --> 00:07:12,000
That we can barely remember what we did last week.

78
00:07:12,000 --> 00:07:15,000
I can barely remember what I did this morning.

79
00:07:15,000 --> 00:07:19,000
But if you think about it, you can develop it and you can bring it back.

80
00:07:19,000 --> 00:07:35,000
So.

