1
00:00:00,000 --> 00:00:14,920
Good evening, everyone broadcasting live October, November 9, 2015, November 5th, November

2
00:00:14,920 --> 00:00:22,760
5th, that's correct.

3
00:00:22,760 --> 00:00:26,720
And I think we've got proper sound going on.

4
00:00:26,720 --> 00:00:31,320
Do you want to say hello, Robin, so I can test?

5
00:00:31,320 --> 00:00:32,320
Hello.

6
00:00:32,320 --> 00:00:33,320
Oh, no.

7
00:00:33,320 --> 00:00:36,200
Why aren't you coming through?

8
00:00:36,200 --> 00:00:38,560
Um, I'm not sure.

9
00:00:38,560 --> 00:00:44,440
Test, test, maybe you're not allowed, I don't know.

10
00:00:44,440 --> 00:00:51,480
Oh wait, can I see?

11
00:00:51,480 --> 00:00:57,360
There, okay, now say, now say hello.

12
00:00:57,360 --> 00:00:58,360
Hello.

13
00:00:58,360 --> 00:00:59,360
Yes.

14
00:00:59,360 --> 00:01:01,560
That's okay.

15
00:01:01,560 --> 00:01:04,960
Now we're both, now we both should be coming through the audio broadcast.

16
00:01:04,960 --> 00:01:05,960
Okay.

17
00:01:05,960 --> 00:01:06,960
Okay.

18
00:01:06,960 --> 00:01:07,960
Questions?

19
00:01:07,960 --> 00:01:08,960
We have questions.

20
00:01:08,960 --> 00:01:09,960
Yes.

21
00:01:09,960 --> 00:01:15,100
First of all, let's do the announcements first because tonight we don't have a dumb

22
00:01:15,100 --> 00:01:22,620
upon the video, so what's going on here, we have two things, um, well, actually, only one

23
00:01:22,620 --> 00:01:28,620
thing, but I have two posters, but I don't think it's worth showing posters, is it?

24
00:01:28,620 --> 00:01:30,700
Yes, we just want to say our new posters.

25
00:01:30,700 --> 00:01:31,700
Yes.

26
00:01:31,700 --> 00:01:40,140
You can comment on the screen share.

27
00:01:40,140 --> 00:01:55,340
There, reduction of meditation, I want to get something that will hit them, it will make

28
00:01:55,340 --> 00:01:58,540
them wake up and say, hey, why aren't you all meditating?

29
00:01:58,540 --> 00:01:59,540
Yeah.

30
00:01:59,540 --> 00:02:04,300
Why do you go out and drinking every night, whatever you say you do?

31
00:02:04,300 --> 00:02:05,300
That's very nice.

32
00:02:05,300 --> 00:02:14,900
Okay, so that's the first one, and did it change?

33
00:02:14,900 --> 00:02:16,340
No, not yet.

34
00:02:16,340 --> 00:02:32,580
No, then we'll have to stop it and there, how's your new thing?

35
00:02:32,580 --> 00:02:33,580
This one isn't me.

36
00:02:33,580 --> 00:02:39,340
Yeah, this is Carolyn, one of our exact members, but I did the poster for her.

37
00:02:39,340 --> 00:02:43,860
She did a poster, but then it, well, I redid it.

38
00:02:43,860 --> 00:02:46,660
Come on, nice.

39
00:02:46,660 --> 00:02:47,660
Yeah.

40
00:02:47,660 --> 00:02:52,460
The McMaster Buddhism logo is a little bit like the Seremonko logo.

41
00:02:52,460 --> 00:02:56,900
It's not the McMaster Buddhism logo, that's the McMaster students union clubs.

42
00:02:56,900 --> 00:02:57,900
Oh, okay.

43
00:02:57,900 --> 00:03:06,980
They forced us to put their logo on our posters, so the clubs is very hard to read, but

44
00:03:06,980 --> 00:03:13,260
it's not important to me, because their logo isn't all that important.

45
00:03:13,260 --> 00:03:18,380
But yeah, so we're having a campfire, this is our one big announcement.

46
00:03:18,380 --> 00:03:24,020
This was someone's ideas, I think it was Carolyn's idea, to go and have a campfire in

47
00:03:24,020 --> 00:03:31,940
the evening and talk about Buddhism and maybe do some meditation together, but in nature.

48
00:03:31,940 --> 00:03:36,300
Monks aren't actually allowed to sit around campfires together, it's an interesting thing.

49
00:03:36,300 --> 00:03:41,700
I have to look up whether I can take part of the campfire, but it's a curious rule.

50
00:03:41,700 --> 00:03:45,700
Oh, we're not presenting, we have here, okay.

51
00:03:45,700 --> 00:03:51,580
It's a curious rule, and the speculation is that it's somehow sitting around a campfire

52
00:03:51,580 --> 00:03:58,340
somehow leads to negligence, it's just not an appropriate behavior for a Buddhist monk,

53
00:03:58,340 --> 00:03:59,340
but whatever.

54
00:03:59,340 --> 00:04:06,020
I think I'll be okay with attending, because I'm the only monk and I'm going to talk

55
00:04:06,020 --> 00:04:07,980
to lay people not to talk to monks.

56
00:04:07,980 --> 00:04:12,220
And if there happens to be a campfire there?

57
00:04:12,220 --> 00:04:18,140
Yeah, I think the idea is they're going to have s'mores or something.

58
00:04:18,140 --> 00:04:28,740
So, I mean, it's sort of in the vein of lay Buddhism, you know, if you're in the yeah,

59
00:04:28,740 --> 00:04:34,140
John Chagroop has like Buddhist songs that they sing and Buddhist sing alongs and

60
00:04:34,140 --> 00:04:39,140
Buddhist plays that are kind of humorous about Buddhism.

61
00:04:39,140 --> 00:04:48,100
And you wonder where the line is going to be drawn, but sometimes you just accept

62
00:04:48,100 --> 00:04:56,220
they go solo by a whole skillful means to get people interested in.

63
00:04:56,220 --> 00:05:02,100
So it's the idea is to create a community of like-minded individuals.

64
00:05:02,100 --> 00:05:10,540
Again, this isn't my idea, but it's interesting, so that's part of what we'll be doing.

65
00:05:10,540 --> 00:05:13,540
That's next Friday.

66
00:05:13,540 --> 00:05:18,540
So that's a couple of things that have been going on here.

67
00:05:18,540 --> 00:05:20,540
What else?

68
00:05:20,540 --> 00:05:22,340
Tomorrow I'm going to London.

69
00:05:22,340 --> 00:05:28,540
For Saturday, I'm going to London, Ontario to someone's house.

70
00:05:28,540 --> 00:05:36,540
Oh, and we've got 27 of 28 slots filled on the appointments page.

71
00:05:36,540 --> 00:05:42,540
That means I'm meeting with 27 different people a week.

72
00:05:42,540 --> 00:05:43,540
It's pretty good.

73
00:05:43,540 --> 00:05:44,540
That's great.

74
00:05:44,540 --> 00:05:45,540
Yeah.

75
00:05:45,540 --> 00:05:49,540
So that's all.

76
00:05:49,540 --> 00:06:02,540
Any announcements from the volunteer group?

77
00:06:02,540 --> 00:06:08,540
If anyone has been following along on Facebook and hasn't really noticed much activity,

78
00:06:08,540 --> 00:06:15,540
it's because most of our activity now is on a website called slack.com, and it's kind

79
00:06:15,540 --> 00:06:19,540
of taking the place of those email blasts that we're going out and taking a place with a lot

80
00:06:19,540 --> 00:06:22,540
of the talk on Facebook.

81
00:06:22,540 --> 00:06:26,540
So if anyone is interested, just send me a message.

82
00:06:26,540 --> 00:06:32,540
I'll post the information into the meditator chat panel as well, because it's still a very active

83
00:06:32,540 --> 00:06:33,540
group.

84
00:06:33,540 --> 00:06:40,540
It's kind of changed our communication means a little bit.

85
00:06:40,540 --> 00:06:43,540
Ready for questions, Bante?

86
00:06:43,540 --> 00:06:44,540
Yep.

87
00:06:44,540 --> 00:06:48,540
Okay.

88
00:06:48,540 --> 00:06:49,540
Then we will serve.

89
00:06:49,540 --> 00:06:53,540
What would be your translation of Lord Buddha's final words?

90
00:06:53,540 --> 00:06:56,540
Thank you.

91
00:06:56,540 --> 00:07:04,540
Thank you.

92
00:07:04,540 --> 00:07:33,540
Thank you.

93
00:07:33,540 --> 00:07:37,540
You could say up a mod.

94
00:07:37,540 --> 00:07:45,540
So come to fulfillment in regards to vigilance or mindfulness really.

95
00:07:45,540 --> 00:07:47,540
So it becomes sober, really.

96
00:07:47,540 --> 00:07:59,540
Sober up if you wanted to sort of a grass, grass, and paraphrase.

97
00:07:59,540 --> 00:08:06,540
So all this stuff, all this stuff is going to disappear.

98
00:08:06,540 --> 00:08:09,540
Sober up.

99
00:08:09,540 --> 00:08:21,540
They're all stuff disappears, all stuff, all stuff breaks, all stuff breaks, stuff breaks

100
00:08:21,540 --> 00:08:22,540
sober up.

101
00:08:22,540 --> 00:08:23,540
There you go.

102
00:08:23,540 --> 00:08:30,540
It's about as grass as I can get.

103
00:08:30,540 --> 00:08:33,540
There's Buddhism in a nutshell.

104
00:08:33,540 --> 00:08:34,540
Stuff breaks sober up.

105
00:08:34,540 --> 00:08:36,540
There's a meme for you.

106
00:08:36,540 --> 00:08:39,540
And it's actually a fairly literal translation.

107
00:08:39,540 --> 00:08:40,540
Interestingly, stuff.

108
00:08:40,540 --> 00:08:42,540
Sankara is stuff.

109
00:08:42,540 --> 00:08:46,540
It depends what you mean by stuff, but it's one way of stuff.

110
00:08:46,540 --> 00:08:53,540
The music is a good translation of Sankara breaks.

111
00:08:53,540 --> 00:09:01,540
Why I mean it's, you know, it's more like dissipates or ceases, but fades away breaks

112
00:09:01,540 --> 00:09:06,540
as I think good.

113
00:09:06,540 --> 00:09:19,540
Sober, because Bhamadeh has to do with mud, mud is the root of being intoxicated.

114
00:09:19,540 --> 00:09:22,540
So Bhamadeh is a kind of mental intoxication.

115
00:09:22,540 --> 00:09:29,540
Sobering up or non-intoxication, unintoxicated.

116
00:09:29,540 --> 00:09:36,540
But sambhadeh is like stronger.

117
00:09:36,540 --> 00:09:39,540
Samh is fully sambhadeh.

118
00:09:39,540 --> 00:09:41,540
Bhadh is not strong.

119
00:09:41,540 --> 00:09:45,540
Bhadh is to become sambhadeh, become full.

120
00:09:45,540 --> 00:09:55,540
So sambhadeh is to become full or to fulfill or to succeed.

121
00:09:55,540 --> 00:09:59,540
So it does have a sense of up, I think.

122
00:09:59,540 --> 00:10:05,540
Not exactly, but there's my translation.

123
00:10:05,540 --> 00:10:06,540
Stuff breaks.

124
00:10:06,540 --> 00:10:10,540
Sober up.

125
00:10:10,540 --> 00:10:13,540
Can make a poster for that too.

126
00:10:13,540 --> 00:10:14,540
Put it on Facebook.

127
00:10:14,540 --> 00:10:15,540
Stuff breaks.

128
00:10:15,540 --> 00:10:17,540
Sober up the Buddha.

129
00:10:17,540 --> 00:10:19,540
And if anyone pause you out on it,

130
00:10:19,540 --> 00:10:24,540
then we'll get it sent to fake Buddha quotes and have him go at it.

131
00:10:24,540 --> 00:10:29,540
And then we can all laugh at him when he says it's fake.

132
00:10:29,540 --> 00:10:31,540
No, he's a good friend.

133
00:10:31,540 --> 00:10:32,540
Don't do that.

134
00:10:32,540 --> 00:10:39,540
But it'd be funny to have that conversation.

135
00:10:39,540 --> 00:10:41,540
Hello, Bhanteh.

136
00:10:41,540 --> 00:10:45,540
I recently attained third path by following instructions from Mahasisadha's

137
00:10:45,540 --> 00:10:47,540
practical insight meditation.

138
00:10:47,540 --> 00:10:51,540
But I still feel pleasant sensations when I perceive pleasant objects

139
00:10:51,540 --> 00:10:56,540
and feel sensations of wanting to incline towards pleasant objects,

140
00:10:56,540 --> 00:10:59,540
although not attached in the same way as before.

141
00:10:59,540 --> 00:11:03,540
Could you please share your own experience post third path with respect to

142
00:11:03,540 --> 00:11:10,540
sensual desire and whether fourth path makes a difference in perceiving pleasant objects?

143
00:11:10,540 --> 00:11:16,540
Although I still experience theorizing of a thought that that object is pleasant

144
00:11:16,540 --> 00:11:22,540
when contracting pleasant objects, contacting pleasant objects.

145
00:11:22,540 --> 00:11:25,540
I mean, it doesn't sound like you've reached the third path.

146
00:11:25,540 --> 00:11:29,540
The third fruit actually path is just one moment.

147
00:11:29,540 --> 00:11:32,540
So if you've attained the third path,

148
00:11:32,540 --> 00:11:35,540
you've also attained the third fruit.

149
00:11:35,540 --> 00:11:38,540
This path is only one moment.

150
00:11:38,540 --> 00:11:48,540
It's followed immediately and necessarily by third fruit.

151
00:11:48,540 --> 00:11:54,540
Any sensations of wanting to incline towards pleasant objects are a sign of

152
00:11:54,540 --> 00:11:57,540
low by men of Kamaraga.

153
00:11:57,540 --> 00:12:05,540
So this is my feeling is that it's someone who hasn't detained

154
00:12:05,540 --> 00:12:10,540
Kami as to my own attainment.

155
00:12:10,540 --> 00:12:12,540
How do you know I've attained third path?

156
00:12:12,540 --> 00:12:16,540
Whoever told you that?

157
00:12:16,540 --> 00:12:23,540
It's not something, not something ordinary to become an anagami.

158
00:12:23,540 --> 00:12:27,540
It's actually probably pretty rare in this day and age.

159
00:12:27,540 --> 00:12:31,540
But I have a policy not to talk about myself,

160
00:12:31,540 --> 00:12:35,540
so can't answer the next question.

161
00:12:35,540 --> 00:12:39,540
Dear Bhante, my other leg is on the path.

162
00:12:39,540 --> 00:12:43,540
My other leg is on the path, the other one in sensual world.

163
00:12:43,540 --> 00:12:46,540
We're trying to progress on the path, for example,

164
00:12:46,540 --> 00:12:50,540
practicing a lot during a few days and being mostly alone.

165
00:12:50,540 --> 00:12:54,540
And afterwards handling lay life issues, going to the bank and meeting people

166
00:12:54,540 --> 00:12:56,540
etc., I feel miserable.

167
00:12:56,540 --> 00:12:58,540
I see how worthless everything is.

168
00:12:58,540 --> 00:13:02,540
Then I feel I'm making people I meet, feel miserable just by interacting with them

169
00:13:02,540 --> 00:13:06,540
in the way of not being capable to show my interest whatsoever.

170
00:13:06,540 --> 00:13:12,540
Then making them miserable makes me want to adjust myself by what I feel

171
00:13:12,540 --> 00:13:16,540
is creating delusional ideals about reality in order to make those people

172
00:13:16,540 --> 00:13:20,540
find me less intimidating so they will feel better.

173
00:13:20,540 --> 00:13:23,540
And this seems to take me backwards on the path.

174
00:13:23,540 --> 00:13:25,540
So this is a kind of a sea-sawing motion.

175
00:13:25,540 --> 00:13:29,540
What can I do? Eventually, I must totally dive into the path

176
00:13:29,540 --> 00:13:32,540
and let go completely, but I can't do that yet.

177
00:13:32,540 --> 00:13:34,540
How to look at the situation.

178
00:13:38,540 --> 00:13:44,540
Well, you go from where you are, you can't start somewhere else.

179
00:13:44,540 --> 00:13:48,540
But the path starts from where you are.

180
00:13:48,540 --> 00:13:53,540
That's really the important thing to get across.

181
00:13:53,540 --> 00:14:02,540
Because we moan and complain because we're not able to do, to start where we want.

182
00:14:02,540 --> 00:14:05,540
But the path is right in front of you.

183
00:14:05,540 --> 00:14:07,540
It starts right where you are.

184
00:14:07,540 --> 00:14:09,540
Doesn't mean you have to become a monk.

185
00:14:09,540 --> 00:14:11,540
It's not all there nothing.

186
00:14:11,540 --> 00:14:15,540
That would be jumping from where you are to somewhere else.

187
00:14:15,540 --> 00:14:20,540
In fact, becoming a monk often doesn't solve the problem at all.

188
00:14:20,540 --> 00:14:24,540
The only way out is through the practice of mindfulness.

189
00:14:24,540 --> 00:14:32,540
So the Buddha's advice still applies stuff breaks sober up.

190
00:14:35,540 --> 00:14:40,540
Which means all of the things that you make you feel miserable.

191
00:14:40,540 --> 00:14:42,540
That's what you have to get sober about.

192
00:14:42,540 --> 00:14:44,540
You have to realize that it's impermanent.

193
00:14:44,540 --> 00:14:47,540
It breaks. It happens. That's part of nature.

194
00:14:47,540 --> 00:14:52,540
Teach yourself that. Teach yourself to let go.

195
00:14:55,540 --> 00:14:59,540
The part about I feel I'm making the people I meet,

196
00:14:59,540 --> 00:15:03,540
feel miserable just by interacting with them and not being able to show my interest

197
00:15:03,540 --> 00:15:06,540
whatsoever in what they're doing.

198
00:15:06,540 --> 00:15:10,540
And that happens.

199
00:15:10,540 --> 00:15:20,540
I mean, sometimes you have to give a cursory approval of people's interest

200
00:15:20,540 --> 00:15:24,540
when something interests people you have to smile and nod and say,

201
00:15:24,540 --> 00:15:26,540
well, good for you.

202
00:15:26,540 --> 00:15:30,540
People say I just got married and you can say congratulations because that's what people say

203
00:15:30,540 --> 00:15:33,540
even though you think, oh, no, marriage.

204
00:15:33,540 --> 00:15:36,540
This could be actually marriage is a good thing.

205
00:15:36,540 --> 00:15:39,540
Marriage is better than the alternative of promiscuity and so on.

206
00:15:39,540 --> 00:15:42,540
Because marriage is a teamwork kind of thing.

207
00:15:42,540 --> 00:15:45,540
And it's a morality kind of thing.

208
00:15:45,540 --> 00:15:49,540
I mean, of course, it's not better than the alternative.

209
00:15:49,540 --> 00:15:53,540
Which is not to be to become asexual.

210
00:15:53,540 --> 00:15:56,540
It sounds a bit fun.

211
00:15:56,540 --> 00:16:00,540
For a layperson marriage is reasonable, I think.

212
00:16:00,540 --> 00:16:04,540
But yeah, no.

213
00:16:04,540 --> 00:16:11,540
At Gentong, it's very interesting because the people around him have very little patience.

214
00:16:11,540 --> 00:16:12,540
It's sad.

215
00:16:12,540 --> 00:16:13,540
Or some of the people.

216
00:16:13,540 --> 00:16:15,540
I mean, some good people are sometimes around him.

217
00:16:15,540 --> 00:16:21,540
But sometimes men are just to get surrounded by not so mindful people.

218
00:16:21,540 --> 00:16:26,540
And there will be always very angry at the people who try to come and waste his time.

219
00:16:26,540 --> 00:16:30,540
But at Gentong, just I would have to sit there for hours.

220
00:16:30,540 --> 00:16:37,540
And sometimes one of the old nuns would come up and just talk is ear off about nothing, about meaningless things.

221
00:16:37,540 --> 00:16:45,540
Like maybe their aches and pains and the medicines that they're taking and talking about how they saw someone that they both know.

222
00:16:45,540 --> 00:16:48,540
Because these are people who have known at Gentong for 30, 40 years.

223
00:16:48,540 --> 00:16:55,540
And so they're talking to him about, you know, I met this person, remember this person and now they're doing this right now.

224
00:16:55,540 --> 00:17:01,540
And he just sits and he'll actually, oh, oh, oh, oh, oh.

225
00:17:01,540 --> 00:17:13,540
So you learn this very important word for a Buddhist monk and it's very non-committal.

226
00:17:13,540 --> 00:17:22,540
Someone asks you, is that good?

227
00:17:22,540 --> 00:17:26,540
So now when you make that sound, we know.

228
00:17:26,540 --> 00:17:29,540
We know.

229
00:17:29,540 --> 00:17:33,540
Yeah, it's funny because I was told that it actually sounds kind of weird.

230
00:17:33,540 --> 00:17:36,540
Why are you like, what does it mean to say?

231
00:17:36,540 --> 00:17:39,540
But it's a very common thing in Thailand actually.

232
00:17:39,540 --> 00:17:43,540
I think it's what a Gentong does.

233
00:17:43,540 --> 00:17:48,540
What is kind of perfect to your acknowledging someone said something, but you're not.

234
00:17:48,540 --> 00:17:55,540
You're not really approving or disapproving.

235
00:17:55,540 --> 00:17:57,540
So what time is daily down?

236
00:17:57,540 --> 00:18:00,540
Really, it didn't happen at the time the app indicated.

237
00:18:00,540 --> 00:18:01,540
That's right.

238
00:18:01,540 --> 00:18:03,540
The app is still on the store.

239
00:18:03,540 --> 00:18:05,540
Yeah, the app is still on the day.

240
00:18:05,540 --> 00:18:07,540
Don't use the app.

241
00:18:07,540 --> 00:18:09,540
Don't use the app until February.

242
00:18:09,540 --> 00:18:13,540
That's the answer.

243
00:18:13,540 --> 00:18:16,540
Actually, that's funny because.

244
00:18:16,540 --> 00:18:18,540
Yeah, that means what?

245
00:18:18,540 --> 00:18:19,540
No, that means it's, yeah.

246
00:18:19,540 --> 00:18:22,540
I got lazy and just changed it in the JavaScript.

247
00:18:22,540 --> 00:18:29,540
But if I changed it in the database, that would work.

248
00:18:29,540 --> 00:18:32,540
I think.

249
00:18:32,540 --> 00:18:37,540
Why did it work like that?

250
00:18:37,540 --> 00:18:41,540
Yeah, should have changed it in the in the PHP.

251
00:18:41,540 --> 00:18:42,540
I can do that.

252
00:18:42,540 --> 00:18:47,540
I can fix that.

253
00:18:47,540 --> 00:18:53,540
It's not like I have a million other things to be doing.

254
00:18:53,540 --> 00:18:57,540
Is that all for questions?

255
00:18:57,540 --> 00:18:59,540
I think so.

256
00:18:59,540 --> 00:19:00,540
Oh, no.

257
00:19:00,540 --> 00:19:03,540
Since changing from an Asian diet to a Western one,

258
00:19:03,540 --> 00:19:09,540
have you observed any changes in the way your body and mind is functioning?

259
00:19:09,540 --> 00:19:14,540
Yeah, I mean, a lot of the Asian diet that I would have from time to time.

260
00:19:14,540 --> 00:19:17,540
It wasn't about being an Asian diet.

261
00:19:17,540 --> 00:19:23,540
There's some some Asian societies are less health conscious than the West.

262
00:19:23,540 --> 00:19:25,540
And that's actually maybe not fair to say.

263
00:19:25,540 --> 00:19:29,540
I just find more health health alternatives here.

264
00:19:29,540 --> 00:19:30,540
I don't know.

265
00:19:30,540 --> 00:19:34,540
That's not even fair to say because Western food can be really terrible as well.

266
00:19:34,540 --> 00:19:36,540
It's not like that.

267
00:19:36,540 --> 00:19:41,540
It's more like I've been in places where the food that I ate was not very healthy.

268
00:19:41,540 --> 00:19:44,540
And you can feel that that changes your body.

269
00:19:44,540 --> 00:19:45,540
It's an interesting state.

270
00:19:45,540 --> 00:19:51,540
It's actually can can be quite useful for your practice to be in a state of under nutrition.

271
00:19:51,540 --> 00:19:56,540
And I've been in other places where I get, you know,

272
00:19:56,540 --> 00:20:00,540
really rich food and luck and some Swiss food.

273
00:20:00,540 --> 00:20:02,540
Los Angeles comes to mind when I was in LA.

274
00:20:02,540 --> 00:20:09,540
I got the top creme de la creme of food and all super healthy, super nutritious.

275
00:20:09,540 --> 00:20:12,540
But it's not to say that that's better for your practice.

276
00:20:12,540 --> 00:20:14,540
That connects the need to negligence.

277
00:20:14,540 --> 00:20:17,540
So, you know, whatever.

278
00:20:17,540 --> 00:20:22,540
It's just food.

279
00:20:22,540 --> 00:20:26,540
And that is the last question.

280
00:20:26,540 --> 00:20:30,540
Or, okay, last question.

281
00:20:30,540 --> 00:20:31,540
One more?

282
00:20:31,540 --> 00:20:36,540
No, that was the last question.

283
00:20:36,540 --> 00:20:46,540
Oh, so is it wrong?

284
00:20:46,540 --> 00:20:56,540
Does that mean the website got it wrong as well at time?

285
00:20:56,540 --> 00:21:02,540
I didn't have this thing because it looks like I didn't actually change it.

286
00:21:02,540 --> 00:21:27,540
Looks like I was preparing to change it and then I didn't.

287
00:21:27,540 --> 00:21:34,540
Yeah, I can fix that.

288
00:21:34,540 --> 00:21:35,540
Should be able to fix it.

289
00:21:35,540 --> 00:21:37,540
I'll take a look anyway.

290
00:21:37,540 --> 00:21:39,540
That's all for tonight then.

291
00:21:39,540 --> 00:21:40,540
Yes.

292
00:21:40,540 --> 00:21:44,540
Well, just there was a lot of question earlier about time changes and things.

293
00:21:44,540 --> 00:21:50,540
The individual meetings that you're doing, did they change with the time change?

294
00:21:50,540 --> 00:21:56,540
There's been some confusion there, but I think we've got it worked out.

295
00:21:56,540 --> 00:21:58,540
Anyway, it's not perfect.

296
00:21:58,540 --> 00:22:01,540
No, I'm just wondering from us breaks.

297
00:22:01,540 --> 00:22:05,540
So, it does change with the daylight savings time or the lack of.

298
00:22:05,540 --> 00:22:07,540
Well, it won't change here.

299
00:22:07,540 --> 00:22:14,540
I mean, for me, 7am and 6pm, those are the times.

300
00:22:14,540 --> 00:22:19,540
7am, 730, 6pm, 630.

301
00:22:19,540 --> 00:22:21,540
So, figure that out.

302
00:22:21,540 --> 00:22:22,540
Those are the times.

303
00:22:22,540 --> 00:22:24,540
I don't think there was an issue and a different issue today.

304
00:22:24,540 --> 00:22:26,540
It wasn't to do with that.

305
00:22:26,540 --> 00:22:27,540
Okay.

306
00:22:27,540 --> 00:22:28,540
Confusion.

307
00:22:28,540 --> 00:22:31,540
Anyway, good night, everyone.

308
00:22:31,540 --> 00:22:32,540
Thank you for that.

309
00:22:32,540 --> 00:22:35,540
Well, we should have a demo product again tomorrow.

310
00:22:35,540 --> 00:22:37,540
Okay, good night.

311
00:22:37,540 --> 00:22:38,540
Good night.

312
00:22:38,540 --> 00:22:39,540
Thanks for coming.

313
00:22:39,540 --> 00:23:02,540
Thank you.

