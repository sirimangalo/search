1
00:00:00,000 --> 00:00:10,080
anger, mindfulness, and repression question. When I experience a situation where I feel angry

2
00:00:10,080 --> 00:00:18,960
and try to be mindful, I feel that I am repressing it by saying angry and trying to investigate it.

3
00:00:20,000 --> 00:00:27,920
Afterwards, I feel frustrated. Am I doing something wrong in the practice? Am I catching

4
00:00:27,920 --> 00:00:36,560
the thought too late or am I still not advanced enough? Answer. From what I've seen,

5
00:00:37,360 --> 00:00:44,640
the reason why people feel like they are repressing anything by acknowledging or noting is because

6
00:00:44,640 --> 00:00:53,600
strong emotions like anger are associated with a painful feeling. Anger will often create physical

7
00:00:53,600 --> 00:01:01,840
suffering like headaches, tension, and pain throughout the body. When you say to yourself angry,

8
00:01:01,840 --> 00:01:09,440
angry, the anger disappears in the mind, but you are still left with the unpleasant feelings,

9
00:01:09,440 --> 00:01:16,080
which are actually a result of the anger, not a result of mindfulness.

10
00:01:16,080 --> 00:01:24,000
Another example of this is when you are distracted, thinking about many things.

11
00:01:25,120 --> 00:01:30,400
When you finally realize you have been distracted and say thinking, thinking,

12
00:01:31,040 --> 00:01:38,160
or distracted, distracted, if you notice a headache from the brain activity you might think,

13
00:01:38,160 --> 00:01:47,200
oh, being mindful causes headaches, but that is not the case. It is actually the excessive

14
00:01:47,200 --> 00:01:55,200
thinking that causes the headaches. It can also happen that when you say angry, angry,

15
00:01:55,840 --> 00:02:03,360
you say it with anger or frustration, so instead of being mindful and having a wholesome state of

16
00:02:03,360 --> 00:02:12,000
mind, you are still biased and clinging to the experience. It can also happen that you do not have

17
00:02:12,000 --> 00:02:20,080
fate in the practice, so you note without, you say, angry, angry, but you are not focusing

18
00:02:20,080 --> 00:02:27,680
with confidence on the experience. You might just say the words and hope something good happens

19
00:02:27,680 --> 00:02:35,680
like magic. Finally, it can also happen that you are practicing correctly, but your mind is

20
00:02:35,680 --> 00:02:43,920
not working correctly. You are doing everything right, but your mind is not cooperating, which means

21
00:02:43,920 --> 00:02:51,120
you try to be mindful and you cannot be mindful. You try to catch something and you catch it too late,

22
00:02:51,120 --> 00:02:59,760
you try to clearly see something and it is already gone, and as a result of these shortcomings,

23
00:03:00,320 --> 00:03:09,200
you get frustrated. The truth is that this sort of experience is a sign that you are seeing clearly,

24
00:03:10,320 --> 00:03:19,280
seeing impermanence that even your own mind is unpredictable. Sometimes you can be mindful,

25
00:03:19,280 --> 00:03:26,080
and sometimes you cannot, seeing suffering as your mind is not the way you want it to be,

26
00:03:26,080 --> 00:03:34,640
and seeing oneself as you cannot control your own mind. Until you are familiar with these qualities

27
00:03:34,640 --> 00:03:43,680
of reality, you may be susceptible to reactions like frustration. Those reactions are the problem,

28
00:03:43,680 --> 00:03:49,760
not the inability to make your experience stable, satisfying, and controllable.

29
00:03:51,520 --> 00:04:00,320
Impermanence, suffering, and non-self is the unavoidable nature of reality, but we do not like

30
00:04:00,320 --> 00:04:08,480
reality, we cannot bear it, we cannot bear impermanence, we cannot bear suffering, and we cannot bear

31
00:04:08,480 --> 00:04:18,240
non-self. Even just watching your stomach rise and fall may lead to frustration. The experience

32
00:04:18,240 --> 00:04:25,200
might feel pleasant and peaceful at first, but when it changes you think, what am I doing wrong?

33
00:04:26,720 --> 00:04:32,000
Then you try again and again to make the experience pleasant and peaceful,

34
00:04:32,000 --> 00:04:41,360
and you become frustrated thinking, I cannot do this, this is useless, but the truth is you are

35
00:04:41,360 --> 00:04:48,080
doing it, the problem is not the experience of impermanence, suffering, and non-self,

36
00:04:48,720 --> 00:04:57,440
the problem is the frustration. When frustration arises in the mind, you must focus on the frustration

37
00:04:57,440 --> 00:05:07,920
saying to yourself frustrated, frustrated. If you focus on it objectively, you will see that

38
00:05:07,920 --> 00:05:16,720
just like everything else, it disappears in its own time. Every time you get frustrated,

39
00:05:17,280 --> 00:05:25,680
it is a chance for you to build patience, and patience is an integral part of what we are trying to

40
00:05:25,680 --> 00:05:36,240
cultivate. Patience is associated with wisdom, true patience means just seeing things as they are

41
00:05:36,240 --> 00:05:45,920
and not getting upset about them when they come or go. Develop patience and the ability to see

42
00:05:45,920 --> 00:05:54,320
moment to moment experiences as they are. Your frustration about the meditation is stopping you

43
00:05:54,320 --> 00:06:03,520
from doing this. The frustration will hinder that ability to see the moment to moment experiences.

44
00:06:04,480 --> 00:06:12,080
Frustration will hinder it, desire will hinder it, all kinds of delusion will hinder it.

45
00:06:13,680 --> 00:06:22,160
All of our judgments about our practice will stop us from cultivating it. If people think

46
00:06:22,160 --> 00:06:30,640
the practice is not working for them, this is just wrong view and wrong thought. I have heard people

47
00:06:30,640 --> 00:06:38,640
tell me that they have been stuck in meditation for five or even ten years. When someone feels like

48
00:06:38,640 --> 00:06:46,000
that they are stuck, the first thing for them to get rid of is this idea that they could possibly

49
00:06:46,000 --> 00:06:56,560
be stuck because there is no you and there is no stuck. There is only an experience and it is

50
00:06:56,560 --> 00:07:03,920
amazing that people could waste five years thinking that they are stuck. But in fact, at any moment

51
00:07:03,920 --> 00:07:11,840
you can become enlightened if you are clearly aware of the moment. All we are trying to do

52
00:07:11,840 --> 00:07:19,120
is cultivate that clear awareness and frustration and any kind of judgment about your practice

53
00:07:19,120 --> 00:07:28,080
will stop you from doing that. You are seeing the truth, you are seeing impermanence, suffering and

54
00:07:28,080 --> 00:07:36,080
non-self. It is the frustration that is the problem which is coming from your inability to accept those

55
00:07:36,080 --> 00:07:45,520
very important aspects of reality. Once you can accept them and you can see that that is the

56
00:07:45,520 --> 00:07:55,360
truth of life and this is the path to enlightenment. You will see that s amago zudya, this is the

57
00:07:55,360 --> 00:08:11,280
path to purification.

