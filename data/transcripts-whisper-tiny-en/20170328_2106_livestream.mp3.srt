1
00:00:00,000 --> 00:00:07,000
very nature, is detrimental to themselves and to those who would call them friends.

2
00:00:07,000 --> 00:00:11,000
And those people you have to be careful with.

3
00:00:11,000 --> 00:00:20,000
You can be compassionate and kind, but you should never consider them as proper friends.

4
00:00:20,000 --> 00:00:34,000
So the Buddha talked about in the Deganikai, he talked about different types of friends.

5
00:00:34,000 --> 00:00:38,000
Those who appear to be friends, but are not really friends.

6
00:00:38,000 --> 00:00:46,000
And these are the most common mistaken friends that we come across.

7
00:00:46,000 --> 00:00:55,000
If someone's unfriendly and someone is clearly not your friend, you're not likely to see them as your friend.

8
00:00:55,000 --> 00:01:03,000
But there are ways by which, as I said, we mistakenly come into friendship.

9
00:01:03,000 --> 00:01:27,000
But outlined a number of these, he talked about the friends who call we only take the taker.

10
00:01:27,000 --> 00:01:48,000
So people who are, people who are constantly after you, for benefit, for their own benefit.

11
00:01:48,000 --> 00:01:58,000
And you can see this in, you can see the insincerity and the mental illness involved.

12
00:01:58,000 --> 00:02:06,000
It's quite pitiful to me when people are not able to give.

13
00:02:06,000 --> 00:02:13,000
It's a common, it might say mental illness starts.

14
00:02:13,000 --> 00:02:19,000
And I phrase it in this way, because normally we think, well, that person is just selfish, and mean, and we get very upset at them.

15
00:02:19,000 --> 00:02:21,000
But you really shouldn't be.

16
00:02:21,000 --> 00:02:37,000
If you should pity such a person, a person who constantly wants, who constantly takes.

17
00:02:37,000 --> 00:02:47,000
And take everything together.

