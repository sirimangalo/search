1
00:00:00,000 --> 00:00:06,360
Okay, next question, should a lay person direct their energy to overcoming fears and

2
00:00:06,360 --> 00:00:12,360
insecurities in the sensual world, or are these better resolved through meditation?

3
00:00:12,360 --> 00:00:16,880
Is it better to become successful in the sensual world as Siddhartha was, or to strive

4
00:00:16,880 --> 00:00:19,240
for Nibana?

5
00:00:19,240 --> 00:00:26,160
First of all, Siddhartha is your referring to him.

6
00:00:26,160 --> 00:00:31,400
Isn't the actual historical Siddhartha Gautama?

7
00:00:31,400 --> 00:00:38,200
Siddhartha was book written by Herman Hess, and it's probably more based on Hinduism

8
00:00:38,200 --> 00:00:42,600
than Buddhism.

9
00:00:42,600 --> 00:00:47,960
The point being that according to the Buddha's teaching, let's not even say according

10
00:00:47,960 --> 00:00:53,200
to the Buddha's teaching, according to meditation experience, or let's say Buddhist

11
00:00:53,200 --> 00:01:03,120
meditation, insight meditation, you will find that the indulgence and sensuality never

12
00:01:03,120 --> 00:01:11,200
helps you to overcome it, that the acceptance of your feelings and your wants and your desires

13
00:01:11,200 --> 00:01:18,840
and the pleasure that comes from wanting something is good to acknowledge, but the development,

14
00:01:18,840 --> 00:01:32,320
the creation of that desire or the following after that desire in terms of suppose I want

15
00:01:32,320 --> 00:01:38,680
to, suppose I have the sexual lust and I think, okay, well, I feel repressed.

16
00:01:38,680 --> 00:01:47,400
I should go and find a prostitute or however Siddhartha did it to sort of work that out.

17
00:01:47,400 --> 00:01:50,080
It doesn't work because you're reinforcing.

18
00:01:50,080 --> 00:01:55,080
You're creating a reinforcing behavior, reinforcing the addiction.

19
00:01:55,080 --> 00:02:01,920
I guess I've heard, I've been told that it works to overcome repression for people

20
00:02:01,920 --> 00:02:06,840
who grew up in a repressed state where they weren't allowed to experiment with such things

21
00:02:06,840 --> 00:02:10,480
where they weren't allowed where it was considered sin or taboo or you had to meet to

22
00:02:10,480 --> 00:02:16,920
your marriage or something, that it helped them to come to terms with that, and I can

23
00:02:16,920 --> 00:02:23,880
appreciate that, but I would say rather than dealing with the issue, it's going to the

24
00:02:23,880 --> 00:02:24,880
opposite extreme.

25
00:02:24,880 --> 00:02:33,120
It's trying to beat up one extreme with another extreme, and that may sound good because

26
00:02:33,120 --> 00:02:38,080
in physics, you know, you cancel a positive with a negative, but what it does to the mind

27
00:02:38,080 --> 00:02:44,360
actually is spinning back and forth from one extreme to the other.

28
00:02:44,360 --> 00:02:47,000
It doesn't give you a balanced mind.

29
00:02:47,000 --> 00:02:52,880
There's no canceling out of an extreme practice with another extreme practice in Buddhism

30
00:02:52,880 --> 00:02:54,440
and in reality.

31
00:02:54,440 --> 00:02:58,800
The way of working out one extreme is to slowly bring yourself towards the center.

32
00:02:58,800 --> 00:03:06,520
When you go over to the other extreme, you're creating an even more imbalance in the mind.

33
00:03:06,520 --> 00:03:14,520
It can help to work out some of the repression sides, but it's just giving the contrary

34
00:03:14,520 --> 00:03:20,600
another wrong practice, which is the addiction to the sensuality.

35
00:03:20,600 --> 00:03:28,520
If you feel repressed, if you feel like you're not comfortable with your addictions, then

36
00:03:28,520 --> 00:03:35,080
just start accepting them, start accepting the wants and the needs, and on the other hand,

37
00:03:35,080 --> 00:03:41,080
if you're addicted to them, in the same sense, accept them, or I guess another way of putting

38
00:03:41,080 --> 00:03:42,080
it from that side.

39
00:03:42,080 --> 00:03:47,800
If you're addicted, if you're constantly striving after them, just catch yourself.

40
00:03:47,800 --> 00:03:49,760
Say I don't need that.

41
00:03:49,760 --> 00:03:55,120
Wanting something doesn't mean that I should strive after it, that I need it.

42
00:03:55,120 --> 00:03:59,720
Something is not needing, and if you can teach yourself both of those things, then there's

43
00:03:59,720 --> 00:04:01,400
no need to go to either extreme.

44
00:04:01,400 --> 00:04:06,240
That wasn't exactly your question, but that has to do with the books at heart, which

45
00:04:06,240 --> 00:04:11,960
I read when I was in high school.

46
00:04:11,960 --> 00:04:17,040
The question is whether it's better to stay in the sensual world or strive for nibana,

47
00:04:17,040 --> 00:04:22,600
but I guess that's the important thing is that the Buddha himself never didn't gain anything

48
00:04:22,600 --> 00:04:33,760
from sensuality as it had to go to, he said he learned nothing for 29 years in sensuality.

49
00:04:33,760 --> 00:04:34,760
I think that's the truth.

50
00:04:34,760 --> 00:04:41,280
I don't think much is to be gained by living in the world because you are constantly

51
00:04:41,280 --> 00:04:45,600
bombarded by sensual pleasure.

52
00:04:45,600 --> 00:04:52,480
If your mind is not trained in meditation, you're constantly following after your desires.

53
00:04:52,480 --> 00:04:56,240
Your desires and your aversion, something good comes up, you follow it.

54
00:04:56,240 --> 00:05:00,000
Something bad comes up, you run away from it.

55
00:05:00,000 --> 00:05:01,560
Everything starts with meditation.

56
00:05:01,560 --> 00:05:07,320
There's no question, there should be no balancing of your practice.

57
00:05:07,320 --> 00:05:11,760
The first thing you have to do is settle yourself in the truth, in reality, and that comes

58
00:05:11,760 --> 00:05:13,560
from meditation.

59
00:05:13,560 --> 00:05:14,960
The more you can meditate the better.

60
00:05:14,960 --> 00:05:21,560
If you have the time, the energy, the ability, the good fortune to be able to pull yourself

61
00:05:21,560 --> 00:05:26,760
away from society and strive for nibana.

62
00:05:26,760 --> 00:05:32,080
If you can reach nibana, then there's no worry for you, there's no problem, and that should

63
00:05:32,080 --> 00:05:37,160
be the basis of your path in life.

64
00:05:37,160 --> 00:05:41,560
Reach nibana first, worry about everything else later.

65
00:05:41,560 --> 00:05:47,440
The closer you become, the more enlightened a person becomes, then the easier it is for

66
00:05:47,440 --> 00:05:55,120
them to live in the world without becoming addicted or attached or caught up in the objects

67
00:05:55,120 --> 00:05:59,120
of the sense.

68
00:05:59,120 --> 00:06:03,240
To each of their own, but if you ask me, I would say strive for nibana first, strive

69
00:06:03,240 --> 00:06:05,160
for freedom from suffering.

70
00:06:05,160 --> 00:06:09,840
Strive for enlightenment means wisdom, means understanding.

71
00:06:09,840 --> 00:06:16,200
Practice meditation is the examination of reality, the studying of reality, to see it

72
00:06:16,200 --> 00:06:19,840
from what it is, to understand it and do not get caught up in it.

73
00:06:19,840 --> 00:06:24,320
Once you can do that, then there's nothing that can stop you in the world.

74
00:06:24,320 --> 00:06:36,120
There's nothing which can give rise to clinging, craving, wanting, needing, or aversion

75
00:06:36,120 --> 00:06:37,120
and stress.

76
00:06:37,120 --> 00:06:44,120
Okay, so I hope that helps, that's my take, go for it, start meditating.

