Hello and welcome back to our study of the Tamil Father.
Today we look at verse 173 which reads as follows.
Yes, a papan, kattang, kamang, kusalena, padeet.
Padeet.
So, mang, lo, kamabaset, abhupa, abhapa, mouto, vatandimah, which means
whose evil deeds, whose karma, the evil deeds
that they have done is covered over by wholesomeness.
Such a person lights up this world, illuminates this world,
just like the moon coming out from behind a cloud.
So, it should sound very familiar,
very similar to the last verse.
We have the opposite, two opposite lines here.
One covers over evil with wholesomeness.
That's like the moon when it comes out from behind a cloud,
the uncovering of the moon.
What it means is the change of a person.
A person may have done very bad deeds, but when they do good,
it's the change.
It's going from dark to light.
So, the story here, it's a very famous story.
It's in reference to Angulimala.
I'm sure many of you are familiar with the name Angulimala.
It's a famous figure in Buddhism and Buddhist culture.
Angulimala killed many people.
Some sources say 999 people.
His goal was to kill a thousand,
and his mother came to find him and he was going to kill her.
And then the Buddha intervened.
The story in the midgimani kaya just says that the Buddha went to see him.
He was going to kill all these people.
He was killing all these people,
and he just wouldn't stop until the Buddha walked to see him.
He converted him, and it became a monk.
It's the real story.
Even evil, bloodthirsty, serial killer,
abandoned, could change his ways.
But this verse is actually not so much about the story.
It's about the fact that he became enlightened.
He spoke verse 172, which is the verse we look at last week.
This is what it says.
The opal bepamantitwapachas, on opamantiti.
That's the famous, the more famous verse, I think.
One who was negligent before,
but it's no longer negligent.
Such a one lights up the world.
But 173 is more closely related to the Angulimala Sutta.
As it refers to this shocking turn of events,
that Angulimala, such an evil person,
could not just become a monk,
which is from our complete itself,
but could actually become an arrow on.
And so the monks didn't have any idea that this was possible.
They saw Angulimala ordained,
and of course that was quite a remarkable thing.
Even the king was shocked to hear that Angulimala had become a monk.
People wouldn't believe that anything good would come of it.
They would attack him when he went for arms, beating him,
throwing rocks at him and sticks at him.
So the monks saw this, and it was remarkable,
but no one ever thought that he'd become anywhere near enlightened,
not after all the evil.
So they asked when he passed away,
maybe he was killed.
I don't know.
I think the story says that he was beaten bloody,
but he became enlightened,
and then passed away into...
I know he became enlightened,
and he got beaten up, and after being beaten up,
he spoke some verses and passed away.
So he may have been beaten so severely that he died.
The monks asked where he was reborn.
They went to see the Buddha,
and as was often there, they asked the Buddha,
where was Angulimala reborn?
And the Buddha said,
I know Angulimala, but I knew Buddha would be going to the Buddha.
Having killed so many people, he could possibly become a unbound.
And they said, yes.
And the Buddha said, yes.
In the past, not even one...
And this is an important part of the story,
not even one good friend.
He found not even one good friend.
That's why he did so much evil.
But after having gained a good friend,
he became diligent.
He lost his negligence,
and completely obliterated the evil with the good.
And then he taught the verse.
So I think the story of Angulimala teaches us many things.
It's a long story, but we're not going to go into that,
because this verse isn't so much about the story,
but it teaches us, I think,
mainly two things.
First, that we shouldn't be discouraged about our own state,
coming into the practice.
Some people would think, oh, I'm too far gone.
I've done bad things.
How could I possibly practice to become a knight?
So I always think, well, look at Angulimala.
Often it's true that in many cases a very bad person
is not going to get far in the practice.
It's very difficult.
But on the other hand, it's not so much about your past.
It's about your present.
And doing lots of evil deeds is going to have consequences,
but much of the evil is in external consequences,
not in the state of the mind.
All you have to deal with, and it's really not that complicated
to deal with the emotions in your own mind,
the kind of your own mind, the chaos in your own mind.
And when you deal with that, well, the consequences don't end.
It's possible you might still be arrested for your evil deeds.
It's possible you might be subject to revenge and so on.
But maybe that's another thing that it teaches us.
It's not to be so concerned about external consequences.
We often mix the two external and the internal.
They become discouraged by external consequences.
I do all these good things and bad things still happen to me.
And we're not so concerned about the external bad, concerned about
we're not really concerned about happiness.
Because happiness doesn't need to happiness.
If your concern is always with happiness and freedom from suffering,
you're never really going to be free from suffering because
such a state, such concerns, don't actually make you happy
or free of from suffering.
It's goodness.
It's gusala.
The body uses the word here, gusala in the gusala.
Awesomeness, goodness.
We should always be concerned with goodness.
Best beful of goodness, best beful of goodness.
Best beful of goodness.
But the other lesson, the other lesson that the Buddha mentioned,
the Buddha brings up here that I think is really the crux.
And the Buddha really hits the heart of the matter.
It's how easy it is to fall on the wrong path without a good friend.
Of course, the word good friend, Kali and the mitta.
In Buddhism, it refers to the teacher.
It refers generally to the Buddha.
Because people did not meet the Buddha,
or we could say more generally with the Buddha's teaching,
because of that they fall into.
All sorts of evil.
When you don't have someone who can set you down the right path,
like now we have the Buddha,
and we still have the Buddha through his teachings.
When you don't have that,
sometimes we blame, we blame evil people.
We look at them and they say,
what a terrible evil person.
And sometimes often times,
in fact, you could say all the time,
the only reason people do evil is through delusion.
It's not that anyone wants to be an evil person,
or not exactly.
It's not that anyone purposefully and knowingly wants evil.
It's that they think somehow that some good will come out of being evil,
doing evil things.
And oftentimes there's not even a sense that it's evil,
killing.
For many people, killing is a good thing.
Recently, what there was a politician in America
who said, just today, what did he say?
He said, we should kill all the drug dealers.
And first of all, for many people,
I think that they resonate,
that resonates with it.
They say, yeah, yeah, kill bad people.
Wanting to shoot others,
as in America, a lot of people have guns,
and don't really,
there are many people who don't really have a problem with.
There was one, a talk to one vet,
a veteran of the war,
and he had a lot of mental issues,
but he said, he told me,
he said, someone asked him what he missed most about being a sergeant.
He was a sergeant,
and some part of the armed forces,
he said, what he missed most about it having retired,
and he said, being able to kill people.
There's people that exist in the world.
Now, there are various varying degrees of delusion,
but it's all delusion.
When people think that stealing is not wrong,
or, you know, this story about Mark Zuckerberg,
this had a Facebook,
I don't know if I should mention names,
but this guy,
very famous guy who said,
he decided to start killing very influential
because of how famous he is.
He decided to start killing his own animals,
so he went and he killed a goat on his own,
because he thought that was better,
that was the best thing to do if you're going to eat the meat.
Like he couldn't have just become a vegetarian, right?
It's like, if you feel that strongly about it,
why is it better somehow to kill yourself?
A lot of delusion,
I should go on and on about the types of delusion.
Just the five precepts, you know?
I always remark, if I had some inkling of killing being wrong,
or that drugs and alcohol didn't open your mind
or make you a happier, more fun-loving person,
but in fact, dulled your mind
and led to a greater emotional turmoil.
That's what I had said.
Drugs and alcohol, that's fundamentally wrong for humans to engage in.
Maybe not, but it seems to me that if there had been more instruction,
I might have avoided much of the suffering in my life,
much of the evil.
As for what the verse teaches us,
it has a bit of a different lesson.
The verse itself is talking about the possibility
of mitigating evil through good.
This is a good question.
How does someone become pure
when their mind is so full of evil?
And it doesn't so much evil.
And so we talk about different kinds of karma,
and the Buddha reaffirms that this is the case,
and I think if you look both at external circumstances,
how our deeds and our acts and our minds
that affects the world around us,
and how our actions affect our own minds.
You can see that it's more complicated
than just do good, get good, do evil, get evil.
I mean, that simply put is how karma works,
but the more complex formulas
that there are four types of karma.
There's karma that creates results,
good or bad.
If you do a good deed,
something good comes from it internally
and become a better person with you.
And externally, people appreciate you.
You do a bad deed likewise.
But there are other types of karma.
Some karma is just supportive.
Some aspects of some deeds that you do.
Sometimes you do a good deed,
and it supports some...
So you do one good deed,
and then you do another good deed,
and people recognize the first one
because of the second one,
or the first one bears fruit,
even mentally, because of the power of the second one.
It's repeated actions affect each other.
So supportive karma is a thing.
And then there's a reductive karma
that reduces the effect of other karma.
So you do something good and something good.
Something good comes.
Maybe you're very nice to people and they appreciate you.
But then you do something bad.
And your reputation is reduced,
even though you maybe you could be.
Sometimes someone does very good deeds all their life,
and then one bad mistake,
and their reputation is ruined.
And the fourth is nullifying karma,
karma that destroys other karma.
And that's what he's talking about here.
That, especially internally,
there's so much you can do to change
your mind,
change the landscape of your mind.
All these habits that we have,
all these qualities that make up who we are.
As much we can do to change that.
Meditation is the prime example.
It's quite shocking for people who are familiar with,
for friends, for relatives,
of someone who goes to do a meditation course.
Because when they come back,
they can be quite a changed individual.
And many things about them are different.
And they're quite shocking.
And all those years of building up habits have suddenly been erased
or reduced, usually not erased,
but some bad habits are erased.
But throughout the meditation course,
this is a big part of what we're doing.
It's a good explanation of the work that we do in meditation.
They work on nullifying our bad habits.
I mean, nullifying really is not about covering up,
which is the word that they use in the Buddha used in the verse.
But it's about smothering them, really.
Extinguishing them, I guess, is the best way to explain it.
Simply by gaining the clarity of mind that was missing,
you know, you light up the world.
This illuminating imagery of lighting up the world,
like the moon coming up from behind the clouds,
is apt because you realize that all the people that you did
was just because of ignorance.
That you were totally blind.
That you didn't have a good reason for doing any of the bad things
that you did.
And simply didn't know any better.
But you knew wrong.
We're given all these wrong views by all sorts of sources.
What our parents do, what our friends do,
what our society tells us, what our religions tell us
often religions have bad ideas about killing and stealing and so on.
Not stealing, but many religions condone various forms of violence.
I mean, I think violence towards animals is an obvious one.
But many religions will be against killing humans,
but then have no problems or even in favor of killing animals.
And often in fairly inhumane ways,
so many sources of wrong view.
And many of us, I think,
this resonates, this idea that if only I'd had a good friend,
we feel good now that we feel blessed now.
We feel we found a refuge.
Now that we've found a good friend in the Buddha,
so we can light up the world.
So that's the number part of her tonight.
Thank you all for tuning in. Have a good night.
