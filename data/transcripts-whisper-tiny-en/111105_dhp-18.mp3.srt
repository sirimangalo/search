1
00:00:00,000 --> 00:00:10,700
Okay, so continuing our study of the Dhamupanda with verse number 18, today I brought out

2
00:00:10,700 --> 00:00:11,700
a bigger book.

3
00:00:11,700 --> 00:00:19,540
This is the commentary on the Dhamupanda, and so it has the same verses, but it also has

4
00:00:19,540 --> 00:00:26,160
the story, because today's story has a bit of a problem to it, but it's a very simple

5
00:00:26,160 --> 00:00:31,660
story, and the verse itself is very much the same as the past three verses.

6
00:00:31,660 --> 00:00:42,960
So this one is Ita nandati, page anandati, katapun yo uba yata nandati, pun yangmi katanti nandati,

7
00:00:42,960 --> 00:00:59,460
katati, so katanti, so katanti nandati, so katanti nandati, so here she is blissful, and here

8
00:00:59,460 --> 00:01:11,600
after she is blissful, or has joy atapun yo the person, the doer of goodness, rejoices

9
00:01:11,600 --> 00:01:21,300
in both places, blissful in both places, pun yangmi katanti, thinking I have done good, she

10
00:01:21,300 --> 00:01:28,400
rejoices, biyo nandati, so katanti nandati, and even more she rejoices having gone

11
00:01:28,400 --> 00:01:29,400
to heaven.

12
00:01:29,400 --> 00:01:34,800
So I use the feminine here, there's no need to use the masculine all the time, but specifically

13
00:01:34,800 --> 00:01:49,000
here.

