1
00:00:00,000 --> 00:00:12,000
Living in everyone, broadcasting live, May 11th.

2
00:00:12,000 --> 00:00:22,000
Today's topic is friend, friendship, friends.

3
00:00:22,000 --> 00:00:29,000
That's from the Sigilawad as it then.

4
00:00:29,000 --> 00:00:35,000
Exortation or the teaching to Sigilah or Singalika,

5
00:00:35,000 --> 00:00:40,000
depending on how you say it.

6
00:00:40,000 --> 00:00:50,000
That's why they considered to be the definitive, definitive source for

7
00:00:50,000 --> 00:00:53,000
people.

8
00:00:53,000 --> 00:00:59,000
Worldly,

9
00:00:59,000 --> 00:01:05,000
worldly right conduct and

10
00:01:05,000 --> 00:01:09,000
department and practice.

11
00:01:09,000 --> 00:01:15,000
So how to practice the dhamma in the world, living in the world.

12
00:01:15,000 --> 00:01:21,000
So a lot of it is fairly, I guess, relatively superficial,

13
00:01:21,000 --> 00:01:27,000
in the sense that there's much of it isn't about deep,

14
00:01:27,000 --> 00:01:30,000
spiritual truths.

15
00:01:30,000 --> 00:01:32,000
But it's all very dhamma.

16
00:01:32,000 --> 00:01:37,000
It's about very righteous, very good,

17
00:01:37,000 --> 00:01:40,000
about living a good life, living an honest life,

18
00:01:40,000 --> 00:01:43,000
living a successful life.

19
00:01:43,000 --> 00:01:47,000
Finding happiness in this life, happiness in the next life.

20
00:01:47,000 --> 00:01:53,000
And cultivating spirituality, even as a lay person.

21
00:01:53,000 --> 00:01:58,000
This is one of the better-known passages from it.

22
00:01:58,000 --> 00:02:03,000
It's about the four types of people who are called

23
00:02:03,000 --> 00:02:08,000
Amitamitapati Rupa.

24
00:02:08,000 --> 00:02:13,000
Amitamitam means they are not friends.

25
00:02:13,000 --> 00:02:20,000
Amitamitapati Rupa means they appear to be friends.

26
00:02:20,000 --> 00:02:22,000
They have a parent,

27
00:02:22,000 --> 00:02:27,000
sort of the...

28
00:02:27,000 --> 00:02:30,000
They're like friends.

29
00:02:30,000 --> 00:02:33,000
So there should be known as appearing to be friends,

30
00:02:33,000 --> 00:02:35,000
but not friends.

31
00:02:35,000 --> 00:02:40,000
And then we have the four types of people who are called

32
00:02:40,000 --> 00:02:43,000
Amitasu Suhanda.

33
00:02:43,000 --> 00:02:45,000
Amitamitam means they are friends.

34
00:02:45,000 --> 00:02:48,000
Suhanda means they are real friends.

35
00:02:48,000 --> 00:02:50,000
I don't know where that word comes from.

36
00:02:50,000 --> 00:02:57,000
Suhanda.

37
00:02:57,000 --> 00:02:59,000
And so we need about these passages.

38
00:02:59,000 --> 00:03:02,000
It does go into some detail about each of the four,

39
00:03:02,000 --> 00:03:07,000
but firstly step back and let's talk about this distinction.

40
00:03:07,000 --> 00:03:12,000
Because the interesting point being made here

41
00:03:12,000 --> 00:03:19,000
is a questioning that we must question

42
00:03:19,000 --> 00:03:25,000
analyze and apply wisdom to friendship

43
00:03:25,000 --> 00:03:31,000
and that this is an important aspect of our spiritual practice.

44
00:03:31,000 --> 00:03:34,000
And what makes a person a friend?

45
00:03:34,000 --> 00:03:38,000
Is it the fact that you have known them for a long time?

46
00:03:38,000 --> 00:03:45,000
Is it the fact that they bring you pleasure or joy?

47
00:03:45,000 --> 00:03:58,000
Is it the fact that even gauge in amusement or entertainment with them?

48
00:03:58,000 --> 00:04:05,000
And all of these are reasons why we choose friends.

49
00:04:05,000 --> 00:04:14,000
And too often you hear people lamenting the relationship

50
00:04:14,000 --> 00:04:16,000
that they have with their friends,

51
00:04:16,000 --> 00:04:19,000
but yet still trying to make it work,

52
00:04:19,000 --> 00:04:20,000
because why?

53
00:04:20,000 --> 00:04:23,000
Because he or she is my friend.

54
00:04:23,000 --> 00:04:26,000
Which is curious, because if the person is a

55
00:04:26,000 --> 00:04:28,000
friend through and through corrupt individual,

56
00:04:28,000 --> 00:04:30,000
why are they your friend?

57
00:04:30,000 --> 00:04:33,000
You see, so the reason why we pick friends

58
00:04:33,000 --> 00:04:37,000
or why we consider people to be our friends is

59
00:04:37,000 --> 00:04:39,000
often someone arbitrary.

60
00:04:39,000 --> 00:04:44,000
I remember in high school a very interesting time

61
00:04:44,000 --> 00:04:48,000
in a sort of a sick and twisted way,

62
00:04:48,000 --> 00:04:52,000
because it could be friends in a group

63
00:04:52,000 --> 00:04:56,000
and then suddenly one person would become ostracized

64
00:04:56,000 --> 00:04:58,000
and forced out of the group,

65
00:04:58,000 --> 00:05:01,000
just because it just seemed seemingly on a whim,

66
00:05:01,000 --> 00:05:04,000
suddenly everyone shunned someone.

67
00:05:04,000 --> 00:05:05,000
I remember this.

68
00:05:05,000 --> 00:05:07,000
I was never on the receiving end of it.

69
00:05:07,000 --> 00:05:11,000
But I remember being part of that.

70
00:05:11,000 --> 00:05:16,000
And the cleats and really bizarre looking back

71
00:05:16,000 --> 00:05:17,000
and wondering why?

72
00:05:17,000 --> 00:05:21,000
Why did we pick the people we picked?

73
00:05:21,000 --> 00:05:25,000
Often it's mob mentality,

74
00:05:25,000 --> 00:05:30,000
very scary.

75
00:05:30,000 --> 00:05:33,000
And the friends that I chose were often

76
00:05:33,000 --> 00:05:36,000
for how excited they, you know,

77
00:05:36,000 --> 00:05:39,000
we choose friends for how excited we become around

78
00:05:39,000 --> 00:05:42,000
them often because we look up to them

79
00:05:42,000 --> 00:05:44,000
or we envy them,

80
00:05:44,000 --> 00:05:47,000
sometimes because they provide us simply

81
00:05:47,000 --> 00:05:50,000
with entertainment or we consider them to be witty.

82
00:05:50,000 --> 00:05:55,000
And why is even usually just smart or witty

83
00:05:55,000 --> 00:06:02,000
or sometimes there's an amount of competition involved.

84
00:06:02,000 --> 00:06:07,000
People who we compete with in friendly competition

85
00:06:07,000 --> 00:06:10,000
remember when I returned back from having meditated

86
00:06:10,000 --> 00:06:12,000
I lost most of my own friends.

87
00:06:12,000 --> 00:06:13,000
I tried to hook up with them,

88
00:06:13,000 --> 00:06:17,000
but I was only really interested in bringing them

89
00:06:17,000 --> 00:06:21,000
to come and practice meditation,

90
00:06:21,000 --> 00:06:25,000
which they generally weren't interested in.

91
00:06:25,000 --> 00:06:30,000
And I was doing a poor job of being human relationships

92
00:06:30,000 --> 00:06:36,000
very much indoctrinated at the time.

93
00:06:36,000 --> 00:06:43,000
But yeah, this is the key is

94
00:06:43,000 --> 00:06:46,000
the importance of right friendship.

95
00:06:46,000 --> 00:06:49,000
You know, the most important thing for us

96
00:06:49,000 --> 00:06:51,000
is our inner practice,

97
00:06:51,000 --> 00:06:54,000
but the second most important thing is

98
00:06:54,000 --> 00:07:00,000
got to be friendship.

99
00:07:00,000 --> 00:07:05,000
Because it's your place

100
00:07:05,000 --> 00:07:09,000
in what it means to be a being, you know,

101
00:07:09,000 --> 00:07:16,000
in sentience where you place yourself

102
00:07:16,000 --> 00:07:20,000
on the scale of sentience of the positive

103
00:07:20,000 --> 00:07:24,000
or the negative, the good or the bad.

104
00:07:24,000 --> 00:07:27,000
It's very much to do with your friends.

105
00:07:27,000 --> 00:07:29,000
If you place yourself in a group of friends,

106
00:07:29,000 --> 00:07:33,000
group of people who are committed to unwholesumness,

107
00:07:33,000 --> 00:07:37,000
that's where you place yourself

108
00:07:37,000 --> 00:07:41,000
in your state of being.

109
00:07:41,000 --> 00:07:47,000
So it's your external, it's your environment,

110
00:07:47,000 --> 00:07:49,000
it's the external.

111
00:07:49,000 --> 00:07:51,000
But the internal is all about your own meditation,

112
00:07:51,000 --> 00:07:54,000
and that doesn't matter who you're surrounded by.

113
00:07:54,000 --> 00:08:02,000
But the external is that which supports

114
00:08:02,000 --> 00:08:13,000
and that which nourishes your spiritual practice.

115
00:08:13,000 --> 00:08:17,000
And so surrounding ourselves with whoever

116
00:08:17,000 --> 00:08:22,000
is really a poor way of going about it.

117
00:08:22,000 --> 00:08:27,000
But it gives us some fairly clear guidelines

118
00:08:27,000 --> 00:08:35,000
and a sharp sort of distinction.

119
00:08:35,000 --> 00:08:38,000
How should we go about making friends

120
00:08:38,000 --> 00:08:42,000
or choosing friends, choosing who we associate with?

121
00:08:42,000 --> 00:08:43,000
Now, it's important to understand

122
00:08:43,000 --> 00:08:47,000
that it doesn't refer to who we should be friendly towards.

123
00:08:47,000 --> 00:08:49,000
I think that's fairly obvious,

124
00:08:49,000 --> 00:08:53,000
but sometimes we confuse the two.

125
00:08:53,000 --> 00:08:55,000
Just because someone you don't consider

126
00:08:55,000 --> 00:09:00,000
someone worthy of friendship doesn't mean you shouldn't be friendly to them.

127
00:09:00,000 --> 00:09:02,000
I think because of distinction,

128
00:09:02,000 --> 00:09:05,000
because I don't think everyone should be worthy of our friendship.

129
00:09:05,000 --> 00:09:08,000
I've eventually considered worthy of our friendship.

130
00:09:08,000 --> 00:09:11,000
Some people are simply not worthy of it.

131
00:09:11,000 --> 00:09:17,000
I don't know if that sounds somewhat arrogant or cold or so.

132
00:09:17,000 --> 00:09:20,000
Maybe it's the wrong way of placing it worthy unworthy

133
00:09:20,000 --> 00:09:22,000
is not maybe not the best word.

134
00:09:22,000 --> 00:09:26,000
I think in a technical sense, it does make sense.

135
00:09:26,000 --> 00:09:29,000
Like, it's not worth having this friendship.

136
00:09:29,000 --> 00:09:33,000
This friendship isn't good for either of you.

137
00:09:33,000 --> 00:09:36,000
This friendship isn't something that is beneficial.

138
00:09:36,000 --> 00:09:39,000
There's no benefit, therefore it's not worth it.

139
00:09:39,000 --> 00:09:41,000
That's all I really mean.

140
00:09:41,000 --> 00:09:44,000
Don't mean to put ourselves above others.

141
00:09:44,000 --> 00:09:47,000
Just there's no benefit to it.

142
00:09:47,000 --> 00:09:50,000
If all you do is drag each other down,

143
00:09:50,000 --> 00:09:53,000
not a good thing.

144
00:09:53,000 --> 00:09:56,000
You know, having letting evil people

145
00:09:56,000 --> 00:09:59,000
take advantage of you is very bad for them, right?

146
00:09:59,000 --> 00:10:02,000
So it's not something you want to cultivate

147
00:10:02,000 --> 00:10:07,000
not just for your own benefits for theirs as well.

148
00:10:07,000 --> 00:10:12,000
So the four types of English,

149
00:10:12,000 --> 00:10:16,000
these are pretty good.

150
00:10:16,000 --> 00:10:19,000
I better not think I'll get mixed up.

151
00:10:19,000 --> 00:10:23,000
So there's the greedy person.

152
00:10:23,000 --> 00:10:25,000
That's not the right translation.

153
00:10:25,000 --> 00:10:27,000
Anyway, the greedy person, the one who speaks,

154
00:10:27,000 --> 00:10:29,000
but does not act.

155
00:10:29,000 --> 00:10:32,000
He's not translating directly.

156
00:10:32,000 --> 00:10:39,000
And yet that to a hero.

157
00:10:39,000 --> 00:10:50,000
One who...

158
00:10:50,000 --> 00:10:53,000
One who looks only for their own benefit

159
00:10:53,000 --> 00:10:54,000
to something like that.

160
00:10:54,000 --> 00:10:56,000
I'm not quite sure.

161
00:10:56,000 --> 00:10:59,000
One key, Paramo, one who is only entirely

162
00:10:59,000 --> 00:11:02,000
saying, the Tappo.

163
00:11:02,000 --> 00:11:05,000
Means they say good things, but that's it.

164
00:11:05,000 --> 00:11:08,000
Paramo, I mean, that's it.

165
00:11:08,000 --> 00:11:11,000
They extend what she is speech.

166
00:11:11,000 --> 00:11:16,000
You're only a daily talk of good game.

167
00:11:16,000 --> 00:11:27,000
Anopia, Bani, one who speaks.

168
00:11:27,000 --> 00:11:30,000
One who speaks, one who flatters.

169
00:11:30,000 --> 00:11:42,000
Right, Anopia, Bani, one who speaks to flattery in the flatter.

170
00:11:42,000 --> 00:11:46,000
And number four, Abaya, Sahayo.

171
00:11:46,000 --> 00:11:48,000
But the squander.

172
00:11:48,000 --> 00:11:52,000
Abaya, Sahayo, one who is your companion in ruin.

173
00:11:52,000 --> 00:11:54,000
One who leans you to ruin.

174
00:11:54,000 --> 00:11:56,000
So these four.

175
00:11:56,000 --> 00:12:01,000
The person who is only out for their own game.

176
00:12:01,000 --> 00:12:06,000
The one who is only good for...

177
00:12:06,000 --> 00:12:13,000
Only good for empty promises, that kind of thing.

178
00:12:13,000 --> 00:12:18,000
The person who is flatter, the flatter.

179
00:12:18,000 --> 00:12:25,000
And the companion in ruin.

180
00:12:25,000 --> 00:12:31,000
And then he gives four reasons for each.

181
00:12:31,000 --> 00:12:34,000
So the greedy one, the one who is only looking for their own.

182
00:12:34,000 --> 00:12:40,000
They're greedy, they give little and ask much.

183
00:12:40,000 --> 00:12:42,000
Anyway, you can read through these.

184
00:12:42,000 --> 00:12:46,000
They're in the Degan Nikaya, the Siegel of Adasupa.

185
00:12:46,000 --> 00:12:49,000
Degan Nikaya, number 31.

186
00:12:49,000 --> 00:12:51,000
Definitely worth reading.

187
00:12:51,000 --> 00:12:59,000
They're going to go into it, just talking a little bit about friendship and good friends.

188
00:12:59,000 --> 00:13:04,000
The four kinds of good friends, people who should be known as friends.

189
00:13:04,000 --> 00:13:12,000
Let's go to the Pali.

190
00:13:12,000 --> 00:13:22,000
Pakaaru, one who does, one who helps.

191
00:13:22,000 --> 00:13:29,000
Samana Sukanduku, one who is the same and happiness and suffering.

192
00:13:29,000 --> 00:13:36,000
Atakai, one who...

193
00:13:36,000 --> 00:13:43,000
One who speaks, one who speaks, one who speaks, one who is beneficial.

194
00:13:43,000 --> 00:13:48,000
Anukampako, one who is compassionate.

195
00:13:48,000 --> 00:13:53,000
That's sympathetic.

196
00:13:53,000 --> 00:13:56,000
Yeah, sympathetic.

197
00:13:56,000 --> 00:13:59,000
These are four real friends.

198
00:13:59,000 --> 00:14:03,000
So the first one, let's talk at least a little bit explaining these, right?

199
00:14:03,000 --> 00:14:06,000
So the first one is the greedy one.

200
00:14:06,000 --> 00:14:11,000
This is the kind of person who just is only your friend for their own benefit.

201
00:14:11,000 --> 00:14:17,000
Maybe they borrow money from you, or maybe they mooch off of you.

202
00:14:17,000 --> 00:14:20,000
They're only in it because of the benefit it is to them.

203
00:14:20,000 --> 00:14:24,000
The material benefit that you give them.

204
00:14:24,000 --> 00:14:29,000
Not beneficial for either of you, really.

205
00:14:29,000 --> 00:14:40,000
One who speaks, this is a person who talks about it, it says in the description.

206
00:14:40,000 --> 00:14:43,000
They talk about things they've done for you in the past.

207
00:14:43,000 --> 00:14:46,000
They talk about things they'll do for you in the future.

208
00:14:46,000 --> 00:14:49,000
But they never do anything.

209
00:14:49,000 --> 00:14:52,000
They don't do anything to help others.

210
00:14:52,000 --> 00:14:57,000
But they're always bragging about how much they've done for you in the past.

211
00:14:57,000 --> 00:15:00,000
Or how much they'll do for you in the future.

212
00:15:00,000 --> 00:15:06,000
Once had a man, when I was in Thailand, he just lied through his teeth.

213
00:15:06,000 --> 00:15:08,000
It was really impressive.

214
00:15:08,000 --> 00:15:15,000
He told us he had all this money coming to him, and he was going to give us $250,000 to start a monastery.

215
00:15:15,000 --> 00:15:18,000
And then he just up and disappeared.

216
00:15:18,000 --> 00:15:21,000
So he got me to help him with a bunch of stuff.

217
00:15:21,000 --> 00:15:30,000
And after I realized that was it, he was just buttering me up with all these flat-out lies.

218
00:15:30,000 --> 00:15:34,000
These people actually do exist.

219
00:15:34,000 --> 00:15:38,000
Call on artists.

220
00:15:38,000 --> 00:15:41,000
Anopia Bahani, one who flatters you.

221
00:15:41,000 --> 00:15:43,000
Yes, I've been through this.

222
00:15:43,000 --> 00:15:44,000
I've been to people.

223
00:15:44,000 --> 00:15:47,000
When I was in Sri Lanka this happened, it was incredible.

224
00:15:47,000 --> 00:15:50,000
This man, oh, he was such a flatter.

225
00:15:50,000 --> 00:15:51,000
I didn't.

226
00:15:51,000 --> 00:15:52,000
That really didn't get to me.

227
00:15:52,000 --> 00:16:02,000
But I had no clue that he was a bad person because I was given into his care by a monk who

228
00:16:02,000 --> 00:16:08,000
was very, very famous, fairly well-known monk in the US.

229
00:16:08,000 --> 00:16:14,000
And it was really bizarre finding out that people were giving him donations for me.

230
00:16:14,000 --> 00:16:21,000
And he was, he was squandering among women and drink and gambling.

231
00:16:21,000 --> 00:16:22,000
Incredible.

232
00:16:22,000 --> 00:16:25,000
He had a long story.

233
00:16:25,000 --> 00:16:28,000
But he was definitely a flatter.

234
00:16:28,000 --> 00:16:37,000
It was very dangerous because it's easy to get a big head when people flatter you.

235
00:16:37,000 --> 00:16:44,000
And once you have this, you're objectivity.

236
00:16:44,000 --> 00:16:46,000
Abaya Sahayo.

237
00:16:46,000 --> 00:16:48,000
This is a person who leads you to ruins.

238
00:16:48,000 --> 00:16:52,000
This is the kind of person we often become friends with.

239
00:16:52,000 --> 00:16:59,000
There was an interesting thing coming back and looking at people who I knew and realizing

240
00:16:59,000 --> 00:17:06,000
that many of the people who I had focused my energy on were exactly the type of people

241
00:17:06,000 --> 00:17:15,000
who were, you know, detrimental leading to ruin.

242
00:17:15,000 --> 00:17:24,000
And people who liked to drink, the people who liked to, you know, people who were obsessed

243
00:17:24,000 --> 00:17:27,000
with sensuality, sexuality, that kind of thing.

244
00:17:27,000 --> 00:17:35,000
And a lot of the people who I had ignored and have marginalized because they were boring

245
00:17:35,000 --> 00:17:41,000
because they were straight, you know, on the straight and narrow.

246
00:17:41,000 --> 00:17:47,000
Looking back and looking at them, they really became, wow, these are the kind of people

247
00:17:47,000 --> 00:17:50,000
I would have liked to have stayed in contact with.

248
00:17:50,000 --> 00:17:53,000
Some of them I have actually gotten back in contact with.

249
00:17:53,000 --> 00:17:56,000
They weren't good people.

250
00:17:56,000 --> 00:18:09,000
And that's the thing is we often, our friends have friends not because the good they do

251
00:18:09,000 --> 00:18:22,000
for us because of how they reaffirm or they assuage our guilty feelings at our indulgence.

252
00:18:22,000 --> 00:18:27,000
And it's because they indulge with us.

253
00:18:27,000 --> 00:18:31,000
And so we're not the only one in there for.

254
00:18:31,000 --> 00:18:35,000
We're, we're less concerned about our own wholesomeness.

255
00:18:35,000 --> 00:18:38,000
We have someone else doing it as well.

256
00:18:38,000 --> 00:18:43,000
Can't be all that bad, right?

257
00:18:43,000 --> 00:18:48,000
And the four who are real friends.

258
00:18:48,000 --> 00:18:55,000
It actually helps you, you know, friends who help each other who, you know, provide actual

259
00:18:55,000 --> 00:18:56,000
support.

260
00:18:56,000 --> 00:19:01,000
You know, somebody is your friend when they think to help you and they think to do something

261
00:19:01,000 --> 00:19:11,000
to your benefit when they guard you and they, they got your back, you know.

262
00:19:11,000 --> 00:19:15,000
Samana Sukaduko is someone that doesn't abandon you in times of trouble.

263
00:19:15,000 --> 00:19:19,000
When the going gets hard, it gets, going gets tough.

264
00:19:19,000 --> 00:19:22,000
That's when, you know, you're real friends when they stick with you.

265
00:19:22,000 --> 00:19:26,000
They don't abandon you and you're in, in difficulty.

266
00:19:26,000 --> 00:19:28,000
You know, this person's no fun anymore.

267
00:19:28,000 --> 00:19:31,000
Now he's all got all these problems.

268
00:19:31,000 --> 00:19:37,000
Let's go find people who don't have all these problems so they can have a happy time again, you know.

269
00:19:37,000 --> 00:19:43,000
Now someone who sticks with you in happy times and in unhappy times.

270
00:19:43,000 --> 00:19:46,000
Now the other people are like that when things get good.

271
00:19:46,000 --> 00:19:48,000
When things are bad, they stick with you.

272
00:19:48,000 --> 00:19:57,000
When things get good, they become intoxicated with their pleasure and they treat you poorly.

273
00:19:57,000 --> 00:20:01,000
Atakayi is someone who speaks, speaks benefit.

274
00:20:01,000 --> 00:20:04,000
It speaks meaningful things.

275
00:20:04,000 --> 00:20:06,000
This is a person who you definitely have to stick with.

276
00:20:06,000 --> 00:20:08,000
This is like the Buddha.

277
00:20:08,000 --> 00:20:14,000
Buddha is the greatest friend because he speaks that which is beneficial, that which is helpful.

278
00:20:14,000 --> 00:20:17,000
Stay with such people.

279
00:20:17,000 --> 00:20:25,000
People who say good things, people who instruct or advise or remind you of what's good and what's bad.

280
00:20:25,000 --> 00:20:32,000
Even if it hurts sometimes, sometimes it's unpleasant to be around people who point out your faults.

281
00:20:32,000 --> 00:20:41,000
But you should consider them as pointing a buried treasure. It's far more valuable.

282
00:20:41,000 --> 00:20:50,000
Anukampako is one who sympathizes. One who sympathizes is one who wishes to help.

283
00:20:50,000 --> 00:20:53,000
One who is compassionate when you're in suffering.

284
00:20:53,000 --> 00:20:59,000
You try to find a way for you to get out of suffering.

285
00:20:59,000 --> 00:21:09,000
Someone who is in tune with you when you need a friend to listen.

286
00:21:09,000 --> 00:21:14,000
When you need someone to listen to you, they listen.

287
00:21:14,000 --> 00:21:19,000
When you need someone to support, they're there to support you.

288
00:21:19,000 --> 00:21:34,000
They sympathize.

289
00:21:34,000 --> 00:21:37,000
Right?

290
00:21:37,000 --> 00:21:48,000
The sad earless fortunes or joys is in your good fortune or strains others from speaking ill of you and he commands those who speak rather.

291
00:21:48,000 --> 00:21:58,000
A little bit about friendship, definitely worth reading the whole system.

292
00:21:58,000 --> 00:22:02,000
We got some questions here from last night.

293
00:22:02,000 --> 00:22:07,000
Wasn't Anne under being altruistic by tending to the Buddha's needs.

294
00:22:07,000 --> 00:22:11,000
Potentially altruistic.

295
00:22:11,000 --> 00:22:19,000
But he was only a sort upon this.

296
00:22:19,000 --> 00:22:24,000
He didn't ask to be a Buddha's attendant.

297
00:22:24,000 --> 00:22:27,000
He was asked to do it.

298
00:22:27,000 --> 00:22:31,000
He was doing it out of gratitude.

299
00:22:31,000 --> 00:22:42,000
But my point about altruism is that people are altruistic because it pleases them to be altruistic.

300
00:22:42,000 --> 00:22:59,000
If being altruistic was thoroughly repulsive to you, how could you be altruistic?

301
00:22:59,000 --> 00:23:04,000
It was thoroughly unpleasing to you.

302
00:23:04,000 --> 00:23:09,000
So what we do, we do because it pleases us.

303
00:23:09,000 --> 00:23:11,000
So it's not really altruistic.

304
00:23:11,000 --> 00:23:15,000
We help others because it pleases us to do so.

305
00:23:15,000 --> 00:23:18,000
We may not say it and we'll say no, that's not why we do it.

306
00:23:18,000 --> 00:23:19,000
But it is.

307
00:23:19,000 --> 00:23:29,000
We do it because it has a positive association in our minds.

308
00:23:29,000 --> 00:23:36,000
Still for our own benefit, that's the thing.

309
00:23:36,000 --> 00:23:42,000
Due to also having unhealthy thinking and views, I noticed that it was difficult to make progress.

310
00:23:42,000 --> 00:24:01,000
That we're practicing when and or how can I assess that I'm wholesome and guarded well enough to start incitementation again.

311
00:24:01,000 --> 00:24:06,000
Having unhealthy thinking and views, well thinking isn't unhealthy.

312
00:24:06,000 --> 00:24:13,000
This is the problem. If you judge your thoughts, that's the problem, not the thoughts themselves.

313
00:24:13,000 --> 00:24:18,000
So try and let the thought speed just acknowledge the thinking, thinking, I mean, it's a process.

314
00:24:18,000 --> 00:24:25,000
You should never put aside incitementation unless you're insane.

315
00:24:25,000 --> 00:24:27,000
You know, then you've got to become unsafe.

316
00:24:27,000 --> 00:24:33,000
If you can't be mindful period, then you can't practice.

317
00:24:33,000 --> 00:24:39,000
But as long as you can my teacher said, if you can show someone a cup of water and a cup of rice.

318
00:24:39,000 --> 00:24:47,000
And if they can tell the difference, they can practice incitementation.

319
00:24:47,000 --> 00:24:49,000
Difficult to make progress.

320
00:24:49,000 --> 00:24:53,000
You say it was difficult to make progress.

321
00:24:53,000 --> 00:24:55,000
Absolutely, it's difficult to make progress.

322
00:24:55,000 --> 00:24:58,000
If it was easy, we'd all be enlightened.

323
00:24:58,000 --> 00:25:01,000
That's not a reason to stop.

324
00:25:01,000 --> 00:25:06,000
In fact, it's a sign that you're probably doing something right.

325
00:25:06,000 --> 00:25:08,000
That's if it was very, very easy.

326
00:25:08,000 --> 00:25:11,000
You might want to be suspicious.

327
00:25:11,000 --> 00:25:21,000
Not always. Some people have easy practice, but for the most part, it should be difficult because it challenges you.

328
00:25:21,000 --> 00:25:25,000
So just because you're banging your head against the wall, it feels like banging your head against the wall.

329
00:25:25,000 --> 00:25:27,000
It doesn't mean you're doing it wrong.

330
00:25:27,000 --> 00:25:31,000
At very least, you're learning that banging your head against the wall hurts.

331
00:25:31,000 --> 00:25:35,000
That's very much a part of what our practice is.

332
00:25:35,000 --> 00:25:39,000
Learning how much it hurts to bang their heads against the wall.

333
00:25:39,000 --> 00:25:47,000
So having the future, we don't even think to do it.

334
00:25:47,000 --> 00:25:51,000
And then we have someone named Melinda teaching here.

335
00:25:51,000 --> 00:25:55,000
Always wary of people coming on here and teaching because I don't know it.

336
00:25:55,000 --> 00:25:57,000
They're teaching what I teach.

337
00:25:57,000 --> 00:25:59,000
So everything here should be taken with a grain of salt.

338
00:25:59,000 --> 00:26:02,000
And we don't endorse the views of anyone who posts.

339
00:26:02,000 --> 00:26:06,000
We should have a little disclaimer somewhere.

340
00:26:06,000 --> 00:26:10,000
Are there advantages or disadvantages to meditating outside a nature?

341
00:26:10,000 --> 00:26:13,000
Who is this?

342
00:26:13,000 --> 00:26:17,000
Looks familiar.

343
00:26:17,000 --> 00:26:24,000
Former academic.

344
00:26:24,000 --> 00:26:27,000
I've answered this before actually.

345
00:26:27,000 --> 00:26:30,000
A couple of times, I think.

346
00:26:30,000 --> 00:26:37,000
So nature, I, my view, is that nature is beneficial not for what it is, but for what it isn't.

347
00:26:37,000 --> 00:26:51,000
Nature is about the most recognizable or comfortable environment for human being to be in,

348
00:26:51,000 --> 00:26:55,000
because it's still very much in our psyche.

349
00:26:55,000 --> 00:27:04,000
That's why when we go to nature, we feel peaceful because it doesn't have all this stuff that is not familiar.

350
00:27:04,000 --> 00:27:13,000
So because nature is free from all of this jarring stimuli,

351
00:27:13,000 --> 00:27:16,000
therefore I think there is a benefit to practicing in nature.

352
00:27:16,000 --> 00:27:18,000
I mean, the Buddha was clear about it.

353
00:27:18,000 --> 00:27:24,000
It seems to be, you know, it's not the kind of thing you want to put too much emphasis on obviously,

354
00:27:24,000 --> 00:27:31,000
because in the end, it's all just seeing hearings, not tasting, feeling, thinking, but nature makes it easier,

355
00:27:31,000 --> 00:27:37,000
which may or may not be a good thing, but in the beginning is generally a good thing.

356
00:27:37,000 --> 00:27:42,000
And it makes your practice go quicker and makes it stronger, so generally a good,

357
00:27:42,000 --> 00:27:46,000
not because of what it is, but because of what it is.

358
00:27:46,000 --> 00:27:58,000
Because it isn't jarring, it isn't stress-inducing, so it makes you calm quite quickly.

359
00:27:58,000 --> 00:28:08,000
Considering taking refuge in the five percent,

360
00:28:08,000 --> 00:28:16,000
what considering or change, consideration should I make, consider to these arrangements after formulating?

361
00:28:16,000 --> 00:28:21,000
I've already made arrangements for cremation of our bodies.

362
00:28:21,000 --> 00:28:23,000
I wouldn't worry about the body.

363
00:28:23,000 --> 00:28:30,000
The body's just ashes to ashes and dust and dust, whatever that means.

364
00:28:30,000 --> 00:28:36,000
It's just dust and the wind.

365
00:28:36,000 --> 00:28:39,000
But good for you, we're taking refuge in five percent.

366
00:28:39,000 --> 00:28:42,000
I mean, it's a bit...

367
00:28:42,000 --> 00:28:47,000
I mean, the kind of the cynic in me wants to say,

368
00:28:47,000 --> 00:28:52,000
there really isn't a ceremony.

369
00:28:52,000 --> 00:28:58,000
I want to ruin this whole thing for y'all, because taking refuge means taking refuge.

370
00:28:58,000 --> 00:29:00,000
Do you take refuge?

371
00:29:00,000 --> 00:29:06,000
Listen to the Buddhist teaching, follow it, and appreciate it.

372
00:29:06,000 --> 00:29:09,000
And the five precepts, do you keep them?

373
00:29:09,000 --> 00:29:13,000
But over time, there has evolved to be a ceremony.

374
00:29:13,000 --> 00:29:16,000
So there is a ceremony.

375
00:29:16,000 --> 00:29:18,000
And I'm happy to do it.

376
00:29:18,000 --> 00:29:24,000
In fact, we do it every week when we have our super study class,

377
00:29:24,000 --> 00:29:27,000
or we should be manga study class.

378
00:29:27,000 --> 00:29:31,000
We just haven't had any of these classes, but for that class,

379
00:29:31,000 --> 00:29:36,000
we've been doing it every time before we do the class.

380
00:29:36,000 --> 00:29:39,000
Now, like in Thailand, when they come for...

381
00:29:39,000 --> 00:29:42,000
When they come to offer lunch to the monks,

382
00:29:42,000 --> 00:29:47,000
often the monks will give them the five precepts and the three refuges.

383
00:29:47,000 --> 00:29:49,000
They'll do a ceremony.

384
00:29:49,000 --> 00:29:57,000
I mean, it's done every day in Buddhist monasteries in Thailand.

385
00:29:57,000 --> 00:30:04,000
In Sri Lanka, also for Sri Lankans, when they offer lunch to the monks,

386
00:30:04,000 --> 00:30:10,000
monks give them the five precepts.

387
00:30:10,000 --> 00:30:12,000
The three refuges.

388
00:30:12,000 --> 00:30:17,000
So it's not that big of a deal, but there is a ceremony

389
00:30:17,000 --> 00:30:18,000
and it's something you can do anytime.

390
00:30:18,000 --> 00:30:19,000
I don't...

391
00:30:19,000 --> 00:30:22,000
I mean, I see a lot of Westerners really making a big deal out of it

392
00:30:22,000 --> 00:30:26,000
and telling me about other people who did the ceremony.

393
00:30:26,000 --> 00:30:29,000
I'm like, well, you know, do that every day in Thailand.

394
00:30:29,000 --> 00:30:32,000
I don't know people that once a week or whenever they do it,

395
00:30:32,000 --> 00:30:35,000
some people chanted every day.

396
00:30:35,000 --> 00:30:38,000
I used to chant the five precepts and the eight precepts.

397
00:30:38,000 --> 00:30:41,000
I chanted the eight precepts on the impulse of the day.

398
00:30:41,000 --> 00:30:44,000
And the five precepts regularly.

399
00:30:44,000 --> 00:30:46,000
That was so wonderful for me,

400
00:30:46,000 --> 00:30:49,000
because I had broken all the precepts, right?

401
00:30:49,000 --> 00:30:54,000
And to just have this power where now I was finally doing something

402
00:30:54,000 --> 00:30:56,000
that I could see was right.

403
00:30:56,000 --> 00:30:58,000
I could know was right.

404
00:30:58,000 --> 00:31:00,000
It's pretty powerful.

405
00:31:00,000 --> 00:31:03,000
I'd say just go ahead and do it from the most part.

406
00:31:03,000 --> 00:31:06,000
I don't think that was quite your question.

407
00:31:06,000 --> 00:31:16,000
I wouldn't worry about those arrangements.

408
00:31:16,000 --> 00:31:19,000
How close to Chan, CNC and Tai Chi?

409
00:31:19,000 --> 00:31:25,000
I don't know what you're saying.

410
00:31:25,000 --> 00:31:29,000
Sorry.

411
00:31:29,000 --> 00:31:31,000
It's not a very good...

412
00:31:31,000 --> 00:31:42,000
Well, well-worded question, statement sentence.

413
00:31:42,000 --> 00:31:44,000
Do we need to see the rising and falling

414
00:31:44,000 --> 00:31:46,000
and the stomach in terms of the primary element?

415
00:31:46,000 --> 00:31:50,000
If so, does it become Dhamma Nupasana at that point

416
00:31:50,000 --> 00:31:52,000
instead of Kaheh Nupasana?

417
00:31:52,000 --> 00:31:55,000
Sanka, you're overthinking things as usual.

418
00:31:55,000 --> 00:32:01,000
You're overthinking things to mind.

419
00:32:01,000 --> 00:32:04,000
Rising and falling is the wild doctor.

420
00:32:04,000 --> 00:32:07,000
It's stiffness, that's M.

421
00:32:07,000 --> 00:32:09,000
You can't help but see it.

422
00:32:09,000 --> 00:32:10,000
It is.

423
00:32:10,000 --> 00:32:13,000
It's like, should we see a tiger as a tiger?

424
00:32:13,000 --> 00:32:16,000
When you see a tree, should you see the tree in it?

425
00:32:16,000 --> 00:32:17,000
It is a tree.

426
00:32:17,000 --> 00:32:20,000
You don't have to go looking for the tree.

427
00:32:20,000 --> 00:32:25,000
This is why I'm a dad, so it is.

428
00:32:25,000 --> 00:32:28,000
That's the problem in Dhamma Nupasana.

429
00:32:28,000 --> 00:32:35,000
It's a really difficult one to explain because it's much more about...

430
00:32:35,000 --> 00:32:39,000
It's much more about what it is.

431
00:32:39,000 --> 00:32:46,000
That's highly unexplanatory, but it's about them being groups.

432
00:32:46,000 --> 00:32:50,000
It's not about the individual part, so the five aggregates.

433
00:32:50,000 --> 00:32:56,000
It's about the concept of the five aggregates, the teaching, the Dhamma, the five aggregates.

434
00:32:56,000 --> 00:33:03,000
Five aggregates as Dhamma is different from the five aggregates.

435
00:33:03,000 --> 00:33:10,000
As Dhamma, it's an idea, the idea of that we are in the original self,

436
00:33:10,000 --> 00:33:15,000
that we are just made up of the five aggregates.

437
00:33:15,000 --> 00:33:21,000
I mean, that's not even entirely comprehensive,

438
00:33:21,000 --> 00:33:26,000
but the Dhamma is very hard to explain for me.

439
00:33:26,000 --> 00:33:30,000
I've never had a really satisfactory explanation,

440
00:33:30,000 --> 00:33:35,000
which makes me feel that it's actually not something that can be explained

441
00:33:35,000 --> 00:33:37,000
homogenously.

442
00:33:37,000 --> 00:33:42,000
The different aspects of Dhamma refer to different things

443
00:33:42,000 --> 00:33:45,000
and different meanings of the word Dhamma even,

444
00:33:45,000 --> 00:33:48,000
but they all are teachings of the Buddha.

445
00:33:48,000 --> 00:33:50,000
Dhamma Nupasana is much more...

446
00:33:50,000 --> 00:33:54,000
I like to translate Dhamma and there is teachings.

447
00:33:54,000 --> 00:33:59,000
So when you're practicing Satipatana, you need to keep these things in mind as teachings,

448
00:33:59,000 --> 00:34:03,000
because they play an important role in your meditation.

449
00:34:03,000 --> 00:34:08,000
That's about the best I can do with Dhamma Nupasana.

450
00:34:08,000 --> 00:34:12,000
There is a sipto where the Buddha instructs Mogulana

451
00:34:12,000 --> 00:34:15,000
and how to overcome drowsiness.

452
00:34:15,000 --> 00:34:19,000
Mogulana is practicing the sineless concentration.

453
00:34:19,000 --> 00:34:21,000
It's just the same as seeing the impermanence

454
00:34:21,000 --> 00:34:25,000
that the noting practice leads to, yes, sineless.

455
00:34:25,000 --> 00:34:27,000
Sineless is anmita.

456
00:34:27,000 --> 00:34:28,000
It's not anmita.

457
00:34:28,000 --> 00:34:31,000
It would be animitta.

458
00:34:31,000 --> 00:34:33,000
Animitta is a sign.

459
00:34:33,000 --> 00:34:35,000
Animitta means no sign.

460
00:34:35,000 --> 00:34:37,000
It means no warning, basically.

461
00:34:37,000 --> 00:34:43,000
There's no precursor.

462
00:34:43,000 --> 00:34:46,000
There's nothing that bad.

463
00:34:54,000 --> 00:34:57,000
I can't think of the word foreshadow or another thing

464
00:34:57,000 --> 00:35:04,000
that gives you an indication that something's going to happen.

465
00:35:04,000 --> 00:35:17,000
Remember the word?

466
00:35:17,000 --> 00:35:22,000
So it refers to impermanence, because things don't have warning,

467
00:35:22,000 --> 00:35:25,000
they change chaotically.

468
00:35:25,000 --> 00:35:33,000
You can't predict, you can't expect.

469
00:35:33,000 --> 00:35:52,000
So trying to fix, trying to hold on is a sure recipe for disaster and suffering.

470
00:35:52,000 --> 00:35:59,000
Are you trying to ask how close we are to these things?

471
00:35:59,000 --> 00:36:03,000
Are we close to being chan, dharma realm?

472
00:36:03,000 --> 00:36:06,000
Because that's none of us, none of that is us.

473
00:36:06,000 --> 00:36:09,000
I would recommend that you read my booklet on how to meditate

474
00:36:09,000 --> 00:36:11,000
because that's where you start.

475
00:36:11,000 --> 00:36:17,000
And then you can take it from there and see what you think.

476
00:36:17,000 --> 00:36:20,000
Anyway, let's stop there then.

477
00:36:20,000 --> 00:36:21,000
Thank you all.

478
00:36:21,000 --> 00:36:23,000
Have a good night.

479
00:36:23,000 --> 00:36:29,000
We will probably see you tomorrow.

480
00:36:29,000 --> 00:36:31,000
Are you seeing me tomorrow?

481
00:36:31,000 --> 00:36:54,000
Thank you.

