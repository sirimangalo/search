1
00:00:00,000 --> 00:00:15,880
Okay, good evening everyone, and we are broadcasting live, and the sound is going out.

2
00:00:15,880 --> 00:00:24,280
I can almost guarantee it.

3
00:00:24,280 --> 00:00:35,120
The night's quote, I'm curious, because the source doesn't have this part where it says

4
00:00:35,120 --> 00:00:42,160
they are of great help to one who has become human, and someone inclined to assume that the

5
00:00:42,160 --> 00:00:49,720
author or the translator has added that passage in completely, which would be kind of shameful

6
00:00:49,720 --> 00:00:56,600
if he just goes about adding stuff in. Maybe that it exists somewhere else, because

7
00:00:56,600 --> 00:01:02,840
it wasn't the author, the translator, who found these quotes. So the person who found these

8
00:01:02,840 --> 00:01:08,440
quotes is a friend of mine, who's a Buddhist monk, spent a lot of time finding

9
00:01:08,440 --> 00:01:14,960
painstakingly each of these quotes. So he may have found it in the wrong place anyway.

10
00:01:14,960 --> 00:01:25,820
Let's see if it is the right quote. Yep, it's at least the four-right dhamma. So the

11
00:01:25,820 --> 00:01:33,040
Buddha says, jata rumi bhikavidhamma, there are these four dhamma's monks.

12
00:01:33,040 --> 00:01:42,400
The Buddha was a common way for him to teach, and if you think this isn't an actual thing

13
00:01:42,400 --> 00:01:49,840
that the Buddha probably didn't teach this way, who would teach this way. A lot of monks

14
00:01:49,840 --> 00:01:55,520
do teach according to this tradition. It works really well. My teacher always does this.

15
00:01:55,520 --> 00:02:01,800
He basically says, you know, he'll talk for five minutes about four things, go over one

16
00:02:01,800 --> 00:02:07,520
in a second. Pretty much exactly how the Buddha does it here, but then as the Buddha would

17
00:02:07,520 --> 00:02:15,520
do another places, he says, you know, where is this? And he talks about them in detail

18
00:02:15,520 --> 00:02:23,120
one by one by one. Which, you know, is the way I go through these monks as well.

19
00:02:23,120 --> 00:02:28,000
So there are these four dhamma's. So there's the four dhamma's. What are these dhammas?

20
00:02:28,000 --> 00:02:37,360
Four panya vudya sambhatanti. Do they exist or they lead to the increase in wisdom,

21
00:02:37,360 --> 00:02:43,120
growth in wisdom? They cause someone to grow in wisdom. And that's all it says.

22
00:02:43,120 --> 00:02:58,640
Kattamitataro, which four? Excuse me. Sapurisa sambhurisa sambhurisa sambhurisa sambhur. Association

23
00:02:58,640 --> 00:03:09,120
with sapurisa, good people, good fellows. Sadaam sambhana listening to the dhamma, listening

24
00:03:09,120 --> 00:03:26,960
to the good dhamma. Jhoni sohmanasikharu, wise attention, or wise mental activity, cultivating

25
00:03:26,960 --> 00:03:38,560
wisdom in the mind or paying wise attention. Dhamma and number four dhamma nudhamma katipati,

26
00:03:38,560 --> 00:03:49,840
facing the dhamma in order to realize the dhamma. It's an interesting word. Dhamma nudhamma

27
00:03:49,840 --> 00:03:58,000
vatipati. First of all, patipati, according to the tradition, means, and there's this one teacher

28
00:03:58,000 --> 00:04:04,800
in Bangkok, he's passed away, but he was very smart and wise and on pretty awesome meditation

29
00:04:04,800 --> 00:04:11,800
teacher too. And he said, he always translated this word, patipati.

30
00:04:11,800 --> 00:04:20,480
Patipati means specifically, and the first patipati means specifically, patipati, the second part

31
00:04:20,480 --> 00:04:30,560
means to attain. So, patipati means an activity by which you are specifically going for a goal.

32
00:04:30,560 --> 00:04:38,880
So, your intention is focused on a goal. That's what patipati means, practice, which

33
00:04:38,880 --> 00:04:45,760
translates, but it has a bit of a deeper meaning in terms of something that you're doing

34
00:04:45,760 --> 00:04:54,320
for a reason. So, what are we doing? We're practicing the dhamma, dhamma, which means

35
00:04:54,320 --> 00:05:05,360
practicing the dhamma, you know, specific, specific focus on dhamma. So, focus on the five aggregates,

36
00:05:05,360 --> 00:05:12,320
focus on the six senses, focus on the hindrances, focus on the faculties, focus on the noble truths,

37
00:05:12,320 --> 00:05:18,160
any full noble path and all these good dhammas. Why are we doing it? What are we hoping to attain?

38
00:05:18,160 --> 00:05:29,120
What is this, patipati? In order to patipati, in order to attain, patipati means we're specifically

39
00:05:29,120 --> 00:05:34,560
practicing the dhamma, patipati means to attain the dhamma. It's a very interesting word,

40
00:05:34,560 --> 00:05:44,800
self-referential. But the first dhamma, the second in terms of the explanation, but the first in the

41
00:05:44,800 --> 00:05:53,520
word refers to the truth. It's the higher dhamma, the result. So, dhamma is in two parts. There's the

42
00:05:55,600 --> 00:06:07,280
pubanga manga, or there's the practice, the kusala dhamma, and then there is the vipaka dhamma,

43
00:06:07,280 --> 00:06:19,920
or there is more than vipaka, even there is the super mundane result. I guess which is vipaka

44
00:06:19,920 --> 00:06:27,200
in the sense. There's the super mundane vipaka, I think. It's called vipaka. Anyway, there's the

45
00:06:27,200 --> 00:06:37,200
attainment of nibanda, which is the patipati, which we're trying to gain. So, these are the four

46
00:06:37,200 --> 00:06:44,960
imiko bikho eta darodhamma, bhanya utya samwadanti. These four dhammas lead to a growth in wisdom. Pretty

47
00:06:44,960 --> 00:06:50,640
simple. What's neat about these kind of lists is you can just remember these four things,

48
00:06:50,640 --> 00:06:56,080
and it gives you a framework. You've got a framework by which to practice. So, it's the first one.

49
00:06:56,080 --> 00:07:03,760
Associate with good people. Don't listen to people who are saying bad things,

50
00:07:03,760 --> 00:07:13,360
and they're saying harmful things that lead you to your detriment. It's same Patrick's day coming

51
00:07:13,360 --> 00:07:20,640
up soon, and I know this because at university there is a table set up by the Wellness Center.

52
00:07:20,640 --> 00:07:26,240
This Wellness Center McMaster is just awesome. I should go visit them again. They're coming to our

53
00:07:26,240 --> 00:07:30,880
piece soon, but they're just neat people. So, they're having, they had this table set up, and I

54
00:07:30,880 --> 00:07:37,920
stopped to ask, what's this all about? It was all alcohol, paraphernalia, and stuff, and the

55
00:07:37,920 --> 00:07:49,760
person manning it said to me, we're trying to talk to people about drinking alcohol, and she said,

56
00:07:51,760 --> 00:07:55,040
they got first, she was sort of beating around, but she said, we're trying to

57
00:07:55,040 --> 00:07:59,840
teach them how to consume alcohol and moderation, and she said kind of apologetically,

58
00:07:59,840 --> 00:08:03,600
because we can't, you know, we can't tell them not to drink alcohol, and I wanted to ask her,

59
00:08:03,600 --> 00:08:09,280
do you drink alcohol? Because from the sounds of it, she probably didn't. There are lots of

60
00:08:09,280 --> 00:08:16,720
people out there who just don't drink alcohol for the cultural or religious or just reasons

61
00:08:17,920 --> 00:08:21,280
from their upbringing, just pretty awesome. I wasn't one of those people.

62
00:08:23,920 --> 00:08:29,200
So, I was on the other side. I was trying to convince my roommate and all my friends to drink with me

63
00:08:29,200 --> 00:08:34,240
and to drugs and all that stuff, very bad and caramel.

64
00:08:39,440 --> 00:08:47,120
So, yeah, don't hang out with people like me if kind of person I was. I think I'm sufficiently

65
00:08:47,120 --> 00:08:53,040
or substantially changed. I think some people would still not consider that,

66
00:08:53,040 --> 00:09:00,720
consider me, rather takes all kinds, right? But hang out with people and listen to people and

67
00:09:00,720 --> 00:09:07,280
talk with people and practice with people. You consider to be good fellas, sub-porisa,

68
00:09:08,080 --> 00:09:15,520
in people who are on a good path, people who appreciate and encourage

69
00:09:15,520 --> 00:09:26,880
and work towards good things. Hang out with those people, associate with them. This leads to an

70
00:09:26,880 --> 00:09:31,760
increase in wisdom because you're going to hear wise things and because you're going to practice

71
00:09:31,760 --> 00:09:38,640
good things and that's going to make you wiser and make you lead you to understand better understanding

72
00:09:38,640 --> 00:09:46,320
better wisdom. Number one, number two, sadhamma, sadhamma, sadhamma. So, don't just hang out with

73
00:09:46,320 --> 00:09:51,600
such people, listen to what they have to say. Listen to the Buddha's teaching or read the Buddha's

74
00:09:51,600 --> 00:10:01,840
teaching. Listen to talks on the dhamma and so on. Of course, this leads to wisdom. Not only does it

75
00:10:01,840 --> 00:10:08,240
lead to information, which is called sutamaya panya. It also leads you to thinking different ways.

76
00:10:08,240 --> 00:10:13,360
So, it leads to jintamaya panya. You start to think about things you never thought about before,

77
00:10:13,360 --> 00:10:19,920
say, oh yeah, it's like that. Never thought of that before. And then, finally, of course, it leads

78
00:10:19,920 --> 00:10:26,800
you to change the way you see the world, which is power namaya panya. It leads you to practice

79
00:10:27,600 --> 00:10:33,920
meditation and just in general, leads you to see things differently. It leads to a different outlook.

80
00:10:33,920 --> 00:10:42,480
It leads to a real visceral change in the way you understand the world. And that's bhava namaya panya.

81
00:10:44,800 --> 00:10:49,680
So, sadhamma, sadhamma, sadhamma, listening to the dhamma, that's a good way to cultivate wisdom. Number two,

82
00:10:50,560 --> 00:10:57,760
number three, yoni soma nasi karo. Yoni soma nasi karo, I think. How does he translate it here? I don't

83
00:10:57,760 --> 00:11:02,640
think I'm happy with this translation. Well, no, why is attention that's good?

84
00:11:02,640 --> 00:11:08,480
Yeah. So, why is attention? Yoni, so I've talked about this for yoni, so it means to the womb,

85
00:11:09,200 --> 00:11:14,400
womb, yoni is womb, like a woman's womb, where the baby is born.

86
00:11:17,440 --> 00:11:23,760
But what it means is getting back to the root, get back to the source. Yoni, you can also just

87
00:11:23,760 --> 00:11:29,760
mean the source. So, yoni soma means to the source. And it's just an idiomatic way of saying

88
00:11:29,760 --> 00:11:36,240
with wisdom. But in the context of meditation, it's quite interesting. So, yoni soma means to the source,

89
00:11:37,280 --> 00:11:48,240
manasi means in the mind, kara means to make or to hold, to make, to form, kind of kara,

90
00:11:48,240 --> 00:11:54,160
it's just karma, it just means action. So, to act in such a way, or to make something

91
00:11:54,160 --> 00:12:01,520
be in the mind to the source, which is very, very interesting from the point of view of insight

92
00:12:01,520 --> 00:12:08,640
meditation, where we do this, right? When you focus on pain and you say pain, pain, you're keeping

93
00:12:08,640 --> 00:12:18,480
that thing in mind, or you're establishing it in your mind at the source. So, only the source.

94
00:12:18,480 --> 00:12:23,520
You're not worried about, is it good pain, bad pain, problem pain, my pain,

95
00:12:24,880 --> 00:12:30,640
did you cause this pain, that kind of thing? All we know is the source. We get to the source,

96
00:12:30,640 --> 00:12:39,680
and we see it just as pain, which of course is described elsewhere. But this word itself is very

97
00:12:39,680 --> 00:12:45,600
interesting. So, proper attention. It can also refer to just proper attention when you're listening

98
00:12:45,600 --> 00:12:50,880
to the dumb, I mean it has worldly definitions as well. Like when you're listening to the

99
00:12:50,880 --> 00:12:55,280
dumb, you should pay attention, keep it in mind with wisdom, you know, reflect wisely and what's

100
00:12:55,280 --> 00:13:00,160
being said, a lot of people interpret it that way. But if you look at the texts, it's really not,

101
00:13:00,160 --> 00:13:06,880
that's not enough. You only saw manasi kara has to be a meditative state, but you can apply it

102
00:13:06,880 --> 00:13:14,320
to thinking wisely, listening wisely, the paying attention can be thought of just as that.

103
00:13:14,320 --> 00:13:20,400
So, it does relate back to the other one, but more importantly, you should pay attention to the

104
00:13:20,400 --> 00:13:28,640
dumb, the teachings, meaning the realities that are being taught about, you should pay attention

105
00:13:28,640 --> 00:13:40,000
to those realities. Put the teaching to work. But of course, you could also say, well, that's

106
00:13:40,000 --> 00:13:44,160
fine with you only saw manasi kara has been, just pay attention when you're listening to the dumb,

107
00:13:44,160 --> 00:13:48,720
but number four is where you actually practice, right? We've already talked about it, but this

108
00:13:48,720 --> 00:13:58,640
one means dhamma, dumma, dapati, means practicing specifically the dhamma in order to attain

109
00:13:59,280 --> 00:14:08,320
specifically the dhamma. So, practicing before foundations of mindfulness and so on in order to attain

110
00:14:08,320 --> 00:14:19,280
nibana, super mundane dhamma. So, this is a teaching. These four lead to wisdom. Wisdom has to come

111
00:14:19,280 --> 00:14:24,800
from practice. You can't have just the first three, unless you count the third one as meditation,

112
00:14:24,800 --> 00:14:29,600
but you can't just associate with good people and listen to good things. If you're not paying

113
00:14:29,600 --> 00:14:35,520
attention and practicing the dhamma, wisdom motorized, but these four altogether.

114
00:14:35,520 --> 00:14:40,240
The other hand, if you just practice without listening, it's very easy to get on the wrong path.

115
00:14:40,240 --> 00:14:46,800
For many people, you know, part, a huge part of what I do is just correcting people's practice.

116
00:14:46,800 --> 00:14:53,040
Most people, well, all people who practice until you become enlightened, you're doing it wrong.

117
00:14:53,840 --> 00:14:59,920
You're not doing it perfectly. And that's an important point. It's not just, if I do this

118
00:14:59,920 --> 00:15:04,640
moral, become enlightened, that's not the case. That's not how enlightenment is based on wisdom.

119
00:15:04,640 --> 00:15:12,320
It's not based on work. It's not effort. Of course, it takes effort to change your view, but

120
00:15:12,320 --> 00:15:16,960
it's not just effort. You have to change your view. You have to look at things differently.

121
00:15:16,960 --> 00:15:25,600
This is where, actually, where you only sow Manasikara comes in and the wisdom actually has to

122
00:15:25,600 --> 00:15:30,800
take over in the practice. You have to be clever and wise in this world. We mung sad that I

123
00:15:30,800 --> 00:15:36,080
talk about often. It's not enough to just keep your attention on something. You have to be

124
00:15:36,080 --> 00:15:41,040
discriminating and discerning and saying, mmm, I'm doing this wrong. Ah, I'm not paying attention

125
00:15:41,040 --> 00:15:48,800
to that. Oh, I'm not really being mindful here. Unmeditation is a constant adapting, refining.

126
00:15:49,520 --> 00:15:54,480
That's what it's really all about. And that's what leads to success, not just plowing through it

127
00:15:54,480 --> 00:16:00,320
and saying, I did so many hours a day. You can do lots and lots of hours of walking, sitting and

128
00:16:00,320 --> 00:16:07,760
getting nothing out of it. That's possible. If you're not doing it right, so you have to constantly,

129
00:16:07,760 --> 00:16:12,000
and I can't tell you how to do it. You have to, the teachings are very simple, but you have to look,

130
00:16:12,000 --> 00:16:15,200
and you have to say, am I doing it the way it's said, and you have to go back to the teachings

131
00:16:15,200 --> 00:16:20,240
and say, oh, wait, I'm not actually doing that. So there's, and you need to teach her, well,

132
00:16:20,240 --> 00:16:27,200
it's good to have a teacher who can tell you. You have to adjust that. So you have to, you need

133
00:16:27,200 --> 00:16:33,280
people, good people, and you need, listen, listen to the dumb and you have to keep it in mind,

134
00:16:34,160 --> 00:16:40,800
you have to practice it. It's basically what you think is that here. So that's the dumb

135
00:16:40,800 --> 00:16:45,920
for tonight. I'm going to copy paste.

136
00:16:45,920 --> 00:16:59,200
The hangout if anybody wants to come on and ask questions. Do that for a few minutes to see

137
00:16:59,200 --> 00:17:09,120
if anybody has questions. If there are, we'll stay on if we're hurt. We'll consider that

138
00:17:09,120 --> 00:17:20,640
our dumb number for tonight.

139
00:17:20,640 --> 00:17:47,360
All right, it looks like there are no, no questioners. So I'm just going to say,

140
00:17:47,360 --> 00:18:00,320
we can make that. See you out tomorrow.

