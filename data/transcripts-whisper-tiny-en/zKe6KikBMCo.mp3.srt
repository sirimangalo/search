1
00:00:00,000 --> 00:00:09,000
I waste too much time lost in Samsara the events of the day without paying attention to Buddhist practice

2
00:00:09,000 --> 00:00:14,000
and he tips on keeping alert and mindful during the day.

3
00:00:17,000 --> 00:00:20,000
Well I wonder where I have to answer this question.

4
00:00:20,000 --> 00:00:23,000
No, no, no, I didn't mean anything, but I think it's you'll be good at answering.

5
00:00:23,000 --> 00:00:33,000
Well I think the good thing is first of all to notice that I'm spending too much time in Samsara, too many events going on.

6
00:00:33,000 --> 00:00:36,000
So this is the first step.

7
00:00:36,000 --> 00:00:48,000
I think the second question is everyone has to answer for himself how much is enough time in practice.

8
00:00:48,000 --> 00:00:55,000
One person might say one hour a day is fine for me, but if I feel that there's a lack,

9
00:00:55,000 --> 00:01:03,000
and it's certainly worthwhile looking at the time I'm spending during the day wasting my time.

10
00:01:03,000 --> 00:01:07,000
Because obviously there is a problem.

11
00:01:07,000 --> 00:01:15,000
I don't think there's a guideline where you can say it's okay to spend one hour or two hours with events.

12
00:01:15,000 --> 00:01:21,000
Some people need it for, some people need six.

13
00:01:21,000 --> 00:01:30,000
I certainly spend a lot of time wasting, but while I was out there in the Samsara world,

14
00:01:30,000 --> 00:01:36,000
I thought that meditating in the morning and the evening was good enough for me,

15
00:01:36,000 --> 00:01:45,000
but still I spent many hours wasting some. I felt the same, so I can relate to the question very well.

16
00:01:45,000 --> 00:01:47,000
Got any tips for them?

17
00:01:47,000 --> 00:01:52,000
Good answer, I mean that's...

18
00:01:52,000 --> 00:01:59,000
Well, they're coming among core, you know, and it's certainly a way, it's a possibility.

19
00:01:59,000 --> 00:02:07,000
But I got rid of... Well, I never had a television really, so I spent a lot of time in computer.

20
00:02:07,000 --> 00:02:13,000
We discussed it here too, we said, because it is a problem if you spend...

21
00:02:13,000 --> 00:02:16,000
I think this goes for a young generation, especially.

22
00:02:16,000 --> 00:02:22,000
If you spend half a day in front of the computer, it's just not healthy, it's not wholesome.

23
00:02:22,000 --> 00:02:27,000
So if you notice that there is something coming up, we feel bad about it,

24
00:02:27,000 --> 00:02:34,000
you might have to put up a rule and say, okay, if it's possible, I mean at work, probably you still have to work in computer,

25
00:02:34,000 --> 00:02:37,000
and it's usually eight hours more.

26
00:02:37,000 --> 00:02:42,000
So if you spend even private time and computer playing games and stuff,

27
00:02:42,000 --> 00:02:46,000
you certainly are going to be very bad on that.

28
00:02:46,000 --> 00:02:50,000
But I didn't have that problem, but I don't know how many people do so.

29
00:02:50,000 --> 00:02:55,000
Everyone has to find out, I think for himself, what is good, what is wholesome, and what can be done about it.

30
00:02:55,000 --> 00:03:03,000
But certainly putting up a rule saying, if possible, not more than half a day in front of any kind of machine,

31
00:03:03,000 --> 00:03:08,000
it just makes you, it drains you out and keeps you away from practice.

32
00:03:14,000 --> 00:03:20,000
I say things for keeping mine full throughout the day, just be simple,

33
00:03:20,000 --> 00:03:26,000
because say you're in a waiting room, you could sit there, watch the breath,

34
00:03:26,000 --> 00:03:32,000
you can add specific things to remind you to be mindful,

35
00:03:32,000 --> 00:03:36,000
like start with something simple, like whenever you walk through your hallway,

36
00:03:36,000 --> 00:03:38,000
be aware that you're walking through your hallway.

37
00:03:38,000 --> 00:03:41,000
Use the hallway as a time to go step right, step away.

38
00:03:41,000 --> 00:03:44,000
Become aware that if you're going through your doorway,

39
00:03:44,000 --> 00:03:48,000
just be aware that you're walking through the door, going into a different area.

40
00:03:48,000 --> 00:03:53,000
Just little things to bring the mind back to what's happening right then,

41
00:03:53,000 --> 00:03:58,000
because that's all it is, it's just being aware of what's happening right now.

42
00:03:58,000 --> 00:04:04,000
Our mother thing that I think is incredibly important to mention,

43
00:04:04,000 --> 00:04:08,000
and not just in regards to this question,

44
00:04:08,000 --> 00:04:12,000
that's the main of the question has been answered,

45
00:04:12,000 --> 00:04:18,000
to add something.

46
00:04:18,000 --> 00:04:25,000
For Westerners especially, people who live in this,

47
00:04:25,000 --> 00:04:27,000
what we call the Western world know,

48
00:04:27,000 --> 00:04:35,000
we tend to feel a lot of guilt when we don't perform up to some standard.

49
00:04:35,000 --> 00:04:42,000
And as opposed to other societies or societies that feel not enough guilt.

50
00:04:42,000 --> 00:04:46,000
So when they're doing really bad things, they don't feel ashamed of it at all.

51
00:04:46,000 --> 00:04:53,000
And it can, both ways are incredibly dangerous when you're too serious about something,

52
00:04:53,000 --> 00:04:56,000
and when you're not serious enough.

53
00:04:56,000 --> 00:05:01,000
And the incredibly important thing that I wanted to mention is,

54
00:05:01,000 --> 00:05:06,000
the Buddha's words on one moment,

55
00:05:06,000 --> 00:05:13,000
the Buddha said, if a person spends one moment developing clarity of mind,

56
00:05:13,000 --> 00:05:18,000
it's better that they had that one moment and died the next moment,

57
00:05:18,000 --> 00:05:24,000
then they should live for a hundred years and never have that one moment.

58
00:05:24,000 --> 00:05:37,000
And one moment of mindfulness, one moment of love, kindness, compassion,

59
00:05:37,000 --> 00:05:43,000
and a listed off several states,

60
00:05:43,000 --> 00:05:47,000
is of great benefit.

61
00:05:47,000 --> 00:05:54,000
They said, so just think of what happens when you do it for a long time.

62
00:05:54,000 --> 00:05:59,000
I think this is a much better way to approach things like meditation,

63
00:05:59,000 --> 00:06:03,000
kind of like money in the bank.

64
00:06:03,000 --> 00:06:09,000
We should think of, or one way to think of, a good way for people who are feeling guilty

65
00:06:09,000 --> 00:06:13,000
and thinking I'm not doing enough, is to look at it in a different way

66
00:06:13,000 --> 00:06:16,000
and think of every meditation you do as a good thing.

67
00:06:16,000 --> 00:06:19,000
The point being, you want to feel good about meditating.

68
00:06:19,000 --> 00:06:21,000
You don't want it to be like a chore

69
00:06:21,000 --> 00:06:23,000
and be something that you're pushing yourself to do.

70
00:06:23,000 --> 00:06:26,000
Because most likely, the reason why you're pushing yourself to do it

71
00:06:26,000 --> 00:06:29,000
is because it's not comfortable.

72
00:06:29,000 --> 00:06:32,000
Or because there's some aspect about it,

73
00:06:32,000 --> 00:06:36,000
that because of it's being unnatural or taking it out of your natural state,

74
00:06:36,000 --> 00:06:39,000
it seems unpleasant and your mind rejects it

75
00:06:39,000 --> 00:06:42,000
because actually meditation, when you're doing it,

76
00:06:42,000 --> 00:06:45,000
is a lot less unpleasant than you think it is, right?

77
00:06:45,000 --> 00:06:48,000
We think, oh, an hour of meditation, that's going to be tough,

78
00:06:48,000 --> 00:06:50,000
but when you're actually doing it, and when you're finished doing it,

79
00:06:50,000 --> 00:06:53,000
you feel like, ooh, I'm really getting something here.

80
00:06:53,000 --> 00:06:57,000
You feel like it's benefiting you.

81
00:06:57,000 --> 00:07:01,000
But it's easy to get these negative tendencies

82
00:07:01,000 --> 00:07:03,000
and they get worse when you feel guilty about it.

83
00:07:03,000 --> 00:07:06,000
When you say, I should be meditating, I should be meditating.

84
00:07:06,000 --> 00:07:11,000
You develop more aversion, more anger inside.

85
00:07:11,000 --> 00:07:16,000
And the quality of anger, according to the Buddha,

86
00:07:16,000 --> 00:07:24,000
is it's the dissatisfaction that leads one to seek out the central pleasures.

87
00:07:24,000 --> 00:07:29,000
So by creating this aversion to the fact that you're not meditating,

88
00:07:29,000 --> 00:07:33,000
this disliking of the fact that it's anger in regards to self-hatred

89
00:07:33,000 --> 00:07:35,000
in regards to the fact that you're not meditating,

90
00:07:35,000 --> 00:07:37,000
you make it harder to meditate.

91
00:07:37,000 --> 00:07:39,000
You make yourself less likely and more likely

92
00:07:39,000 --> 00:07:41,000
to get caught up in samsara.

93
00:07:41,000 --> 00:07:44,000
Your natural tendency will be more towards getting this central pleasures,

94
00:07:44,000 --> 00:07:46,000
because you're not happy.

95
00:07:46,000 --> 00:07:49,000
When you look at meditation as something that brings you benefit,

96
00:07:49,000 --> 00:07:53,000
and you think, wow, if I meditate every moment that I'm mindful,

97
00:07:53,000 --> 00:07:55,000
this is something that one of our teachers always said,

98
00:07:55,000 --> 00:07:57,000
every moment that you're mindful,

99
00:07:57,000 --> 00:08:02,000
your defilements die, seven lifetimes worth of defilements die off.

100
00:08:02,000 --> 00:08:05,000
This is according to the Abhidhamma.

101
00:08:05,000 --> 00:08:06,000
You can work it out like that.

102
00:08:06,000 --> 00:08:08,000
It's kind of a tricky way of saying things,

103
00:08:08,000 --> 00:08:10,000
but it's actually true.

104
00:08:10,000 --> 00:08:12,000
Every moment that you say to yourself rising

105
00:08:12,000 --> 00:08:14,000
and you're clearly aware of the rising,

106
00:08:14,000 --> 00:08:16,000
the defilements in your mind die,

107
00:08:16,000 --> 00:08:20,000
seven lifetimes worth of defilements die in one moment.

108
00:08:20,000 --> 00:08:24,000
It's because every thought moment has every perception

109
00:08:24,000 --> 00:08:27,000
as seven karmic moments in a row,

110
00:08:27,000 --> 00:08:30,000
and all seven of those go out into samsara

111
00:08:30,000 --> 00:08:35,000
and can give rise to result in any lifetime.

112
00:08:35,000 --> 00:08:39,000
So you destroy all of those seven lifetimes worth.

113
00:08:39,000 --> 00:08:41,000
I think it's a bit of a stretch,

114
00:08:41,000 --> 00:08:44,000
but you destroy seven bad karmus for sure.

115
00:08:44,000 --> 00:08:45,000
One moment.

116
00:08:45,000 --> 00:08:48,000
So if you do an hour and suppose your mindful,

117
00:08:48,000 --> 00:08:50,000
you know, 10% of the time,

118
00:08:50,000 --> 00:08:54,000
which is when you're just starting out 20% of the time,

119
00:08:54,000 --> 00:08:57,000
think of how many thought moments you're being mindful of.

120
00:08:57,000 --> 00:08:58,000
That's goodness.

121
00:08:58,000 --> 00:09:00,000
That's the accumulation of goodness.

122
00:09:00,000 --> 00:09:02,000
When you look at it this way as a plus,

123
00:09:02,000 --> 00:09:07,000
instead of something that you're adding,

124
00:09:07,000 --> 00:09:09,000
instead of something that you're missing.

125
00:09:09,000 --> 00:09:11,000
When you do it, you're giving yourself something,

126
00:09:11,000 --> 00:09:13,000
instead of when you don't do it,

127
00:09:13,000 --> 00:09:19,000
you're neglecting something.

128
00:09:19,000 --> 00:09:22,000
Now, it's true that you actually are neglecting something.

129
00:09:22,000 --> 00:09:29,000
It's not wrong that your assessment of your life is not wrong,

130
00:09:29,000 --> 00:09:32,000
and we should be practicing quite a bit.

131
00:09:32,000 --> 00:09:34,000
But the only way to get there is to feel good about it,

132
00:09:34,000 --> 00:09:39,000
to enjoy it, and to see it from here as a good thing.

133
00:09:39,000 --> 00:09:41,000
Not up here where you think,

134
00:09:41,000 --> 00:09:43,000
oh, which is how we normally do things.

135
00:09:43,000 --> 00:09:47,000
We feel guilty when we're not doing it.

136
00:09:47,000 --> 00:09:50,000
And I think that goes for all goodness.

137
00:09:50,000 --> 00:09:53,000
We should, good deeds should not become a burden.

138
00:09:53,000 --> 00:09:56,000
They should become something that is liberating

139
00:09:56,000 --> 00:10:00,000
and pleasant for us.

140
00:10:00,000 --> 00:10:02,000
The other thing I wanted to say about,

141
00:10:02,000 --> 00:10:03,000
in regards to this question,

142
00:10:03,000 --> 00:10:07,000
is you should never underestimate the value of a community

143
00:10:07,000 --> 00:10:10,000
when the Buddha was clear on this.

144
00:10:10,000 --> 00:10:14,000
The holy life is lived based on your companions,

145
00:10:14,000 --> 00:10:15,000
your friends.

146
00:10:15,000 --> 00:10:17,000
If you're surrounded by good people,

147
00:10:17,000 --> 00:10:20,000
you're more likely to do while you're naturally inclined

148
00:10:20,000 --> 00:10:24,000
to do good deeds and to remember to avoid bad deeds.

149
00:10:24,000 --> 00:10:28,000
If you're surrounded by people who are doing bad things,

150
00:10:28,000 --> 00:10:30,000
then you're much more likely to do bad things.

151
00:10:30,000 --> 00:10:34,000
So that's not an option for some people.

152
00:10:34,000 --> 00:10:38,000
I know living in rural areas or living in places,

153
00:10:38,000 --> 00:10:42,000
even in the city where people are not so mindful.

154
00:10:42,000 --> 00:10:45,000
But you should do your best to at least avoid people who are

155
00:10:45,000 --> 00:10:50,000
unmindful and try to find a way to stay to yourself

156
00:10:50,000 --> 00:10:54,000
so that you're active and even better to find people

157
00:10:54,000 --> 00:10:56,000
who are mindful and stay with them

158
00:10:56,000 --> 00:11:21,000
you will certainly rub off both things.

