1
00:00:00,000 --> 00:00:03,680
I've been practising meditation for a few months.

2
00:00:03,680 --> 00:00:07,680
I feel much calmer, compassionate and thoughtful,

3
00:00:07,680 --> 00:00:14,960
but I feel alienated by my family as they think I'm putting on an act.

4
00:00:14,960 --> 00:00:16,320
Any advice?

5
00:00:18,160 --> 00:00:18,960
Bob?

6
00:00:20,880 --> 00:00:21,440
What do you think?

7
00:00:23,440 --> 00:00:26,080
I can only speak from a personal experience.

8
00:00:26,080 --> 00:00:30,720
But I don't feel alienated from my family, really.

9
00:00:30,720 --> 00:00:33,200
Have you tried talking to them?

10
00:00:46,240 --> 00:00:48,720
Yeah, I mean, acting normal helps.

11
00:00:48,720 --> 00:00:55,280
I don't have to act differently as a Buddhist or a meditator.

12
00:00:58,880 --> 00:01:00,400
Be happy, I think, is the best thing.

13
00:01:00,400 --> 00:01:01,520
People want you to be happy.

14
00:01:02,160 --> 00:01:04,880
If you're happy, they've done nothing they can say.

15
00:01:05,920 --> 00:01:12,000
But if you're calm and quiet, it's too conspicuous.

16
00:01:12,000 --> 00:01:22,000
So be happy, laugh a little bit, smile, smile a lot when someone says something nice, smile.

17
00:01:22,000 --> 00:01:25,440
I think that's what you learn, is you learn that people expect these cues,

18
00:01:26,160 --> 00:01:28,640
and you don't have to stop giving those cues.

19
00:01:28,640 --> 00:01:34,320
And in fact, the point is that you have a responsibility or a duty to give those,

20
00:01:34,320 --> 00:01:35,840
to provide those cues.

21
00:01:35,840 --> 00:01:43,680
Someone says something funny, expecting you to react, you have a responsibility to smile or something.

22
00:01:46,320 --> 00:01:49,920
Unless they're saying, hey, I wouldn't beat somebody up, isn't that funny?

23
00:01:51,680 --> 00:01:56,560
That's the case where you purposefully create conflict in order to show something,

24
00:01:56,560 --> 00:01:58,400
or you might purposefully create conflict.

25
00:01:58,400 --> 00:02:03,200
But otherwise, you want to react appropriately.

26
00:02:03,200 --> 00:02:07,280
You don't want to actually react to it and find it funny, for example,

27
00:02:07,280 --> 00:02:10,720
when they tell a funny joke that you want to smile and appreciate,

28
00:02:12,960 --> 00:02:17,360
not exactly even appreciate, but extend your love to them,

29
00:02:17,360 --> 00:02:20,480
which is a part of our reactions, our friendliness.

30
00:02:20,480 --> 00:02:23,840
I mean, we expect this from each other, monkeys expect this from each other.

31
00:02:26,160 --> 00:02:29,760
And they interpret the facial expressions.

32
00:02:29,760 --> 00:02:34,480
We interpret facial expressions, habitually.

33
00:02:34,480 --> 00:02:37,520
So when you're not giving them, yeah, it's going to create conflict.

34
00:02:37,520 --> 00:02:39,280
Question is why are you creating conflict?

35
00:02:39,280 --> 00:02:41,040
Do you want to create that conflict?

36
00:02:41,040 --> 00:02:42,320
In certain cases, you might.

37
00:02:42,320 --> 00:02:45,440
I mean, I try to create that conflict with my students sometimes.

38
00:02:45,440 --> 00:02:48,880
When students are getting boisterous and talking and chatting,

39
00:02:48,880 --> 00:02:51,200
I might come in and look very serious.

40
00:02:54,400 --> 00:02:56,960
So, and if students are very upset,

41
00:02:56,960 --> 00:03:00,320
you want to seem very buoyant and hopeful.

42
00:03:00,320 --> 00:03:02,240
You want to give them hope and encouragement and say,

43
00:03:02,240 --> 00:03:03,360
oh, it's not.

44
00:03:03,360 --> 00:03:04,000
It's a goal.

45
00:03:04,000 --> 00:03:06,720
You're on the right path, good for you, and so on.

46
00:03:06,720 --> 00:03:08,720
You want to sometimes create conflict.

47
00:03:08,720 --> 00:03:12,240
But in regards to your family who is antagonistic,

48
00:03:13,600 --> 00:03:16,480
no, for sure, the best thing you can do is to...

49
00:03:20,000 --> 00:03:22,240
It might sound like you seem like you're almost pretending,

50
00:03:22,240 --> 00:03:26,320
but you're not, and you'll get that eventually.

51
00:03:26,320 --> 00:03:27,680
It'll come to you one way or the other,

52
00:03:27,680 --> 00:03:29,120
because right now you're creating conflict.

53
00:03:29,840 --> 00:03:33,280
And that's a cause for stress, and eventually you'll work that out.

54
00:03:33,920 --> 00:03:38,400
So what long-term meditators find is they learn how to give the cues

55
00:03:38,400 --> 00:03:43,200
without actually interrupting their meditation.

56
00:03:44,000 --> 00:03:48,880
So they're able to have conversations with people and not be emotionally affected.

57
00:03:48,880 --> 00:03:50,720
I'm not thinking, oh, I've got to get out of here.

58
00:03:50,720 --> 00:03:52,320
These people are worrying the hell out of me.

59
00:03:53,280 --> 00:03:57,440
No, they'll be like, it's a suffering and well, but everything's suffering.

60
00:04:00,080 --> 00:04:02,240
They'll be happy, and they'll think of it as a meditation.

61
00:04:02,240 --> 00:04:04,720
This conversation is a meditation for me.

62
00:04:12,800 --> 00:04:14,000
Okay, another question.

63
00:04:15,600 --> 00:04:16,000
Sorry.

64
00:04:16,000 --> 00:04:20,400
I'm just wondering if one has any thoughts on how to deal with family.

65
00:04:22,400 --> 00:04:25,520
Well, my family pretty good about everything,

66
00:04:25,520 --> 00:04:33,120
so I haven't met any obstructions, just questioning and why, and stuff like that.

67
00:04:35,120 --> 00:04:38,640
And then confusion on traditions, because everyone,

68
00:04:40,560 --> 00:04:45,040
you say they've been a Buddhist, they were, what does the Dalai Lama think about this,

69
00:04:45,040 --> 00:04:50,400
and that, and I'm like, well, I don't really pay much attention to the Dalai Lama,

70
00:04:50,400 --> 00:04:55,120
and then I explain that there are different teachings,

71
00:04:56,480 --> 00:04:58,240
and I don't follow that kind of teaching.

72
00:05:00,400 --> 00:05:05,600
But, like I say, a lot of the interaction I have with family is curiosity,

73
00:05:06,240 --> 00:05:09,680
question, and on my part is education.

74
00:05:09,680 --> 00:05:18,800
I'm from time to time, I do read different discourses and dharma,

75
00:05:18,800 --> 00:05:24,720
and I do read to different family members and friends who are interested

76
00:05:24,720 --> 00:05:34,160
on different topics, and find that very interesting, but yeah, on a whole different, pretty good.

77
00:05:34,160 --> 00:05:41,440
What would you recommend to someone who doesn't have that positive interaction?

78
00:05:42,480 --> 00:05:48,400
Well, in this case, there's some sort of stress and conflict.

79
00:05:50,640 --> 00:05:55,440
I think it would be dependent on what you'll promote in Edwards.

80
00:05:55,440 --> 00:06:05,200
If you're not kind of explaining your actions,

81
00:06:08,240 --> 00:06:14,960
if you're not taking part in big family events, or you're not sitting around watching a family

82
00:06:14,960 --> 00:06:23,360
and maybe together anymore, things like that, it depends if you're following precepts,

83
00:06:23,360 --> 00:06:29,760
if you're sticking to the five precepts, and you're trying to stay away from certain aspects,

84
00:06:31,920 --> 00:06:37,760
I would say in a family situation, that would be, you know,

85
00:06:38,800 --> 00:06:43,760
interested in such a, one minute you were doing it all the time, and now you're not doing it at all,

86
00:06:43,760 --> 00:06:45,200
so why have you stopped doing that?

87
00:06:46,880 --> 00:06:50,240
I think that's mostly where some of the problems occur,

88
00:06:50,240 --> 00:06:57,120
when you go from living your life in one way, and then you kind of flick a switch,

89
00:06:57,920 --> 00:07:03,360
and you're no longer participating as much as you were.

90
00:07:05,840 --> 00:07:08,560
Yeah, I think everyone's different in every situation.

91
00:07:12,400 --> 00:07:17,440
Yeah, I don't think it's wrong, necessarily, that you're creating this conflict,

92
00:07:17,440 --> 00:07:21,680
but you have to understand what you're doing when you create the conflict,

93
00:07:21,680 --> 00:07:25,200
because it's leading you somewhere, it's leading you away from your family,

94
00:07:25,200 --> 00:07:28,080
is your intention to become a monk can go and live in the forest as well?

95
00:07:28,960 --> 00:07:33,600
That's probably where you're heading with this sort of behavior, or it's the direction that you're

96
00:07:33,600 --> 00:07:44,320
heading, and that would be the appropriate move to make based on, you know, if you choose to act

97
00:07:44,320 --> 00:07:51,280
in this way, because once you're a monk living off in the forest, then yeah, you really can get away

98
00:07:51,280 --> 00:08:01,440
with acting in that way, acting aloof, acting reserved, and if your family doesn't like it,

99
00:08:01,440 --> 00:08:04,960
well, it's really no skin off your back, because you're a monk living off in the forest.

100
00:08:04,960 --> 00:08:17,760
But if your decision is to remain, or if your situation is such that you have to stay with your

101
00:08:17,760 --> 00:08:25,280
parents, then it's not really to any point to create this stress.

102
00:08:25,280 --> 00:08:34,480
I mean, unless you, it's functionally wrong, it's not like you have bad intentions to

103
00:08:34,480 --> 00:08:40,960
them, but you're creating stress based on the incongruity of it, and it's going to break things up.

104
00:08:42,560 --> 00:08:49,120
It might encourage your family to meditate more, but it might cause them to turn away, it can

105
00:08:49,120 --> 00:08:57,840
actually be too hard for them. So the point is you have to, in our hand, for example, act

106
00:08:57,840 --> 00:09:06,880
separately in every situation, and understands clearly how to deal with individual people.

107
00:09:06,880 --> 00:09:11,520
You can't deal with everyone the same. So for some people, acting reserved and aloof

108
00:09:12,080 --> 00:09:18,480
is a bad thing. If a person is already antagonistic, and suppose it's a family member of

109
00:09:18,480 --> 00:09:24,000
things, you're being brainwashed, well, to act like as though your brainwashed is probably not going

110
00:09:24,000 --> 00:09:34,880
to endear you or your practice to them. If, on the other hand, you're open and interactive with

111
00:09:34,880 --> 00:09:43,040
them, you might very well bring them around and be able to at least come to some sort of harmony,

112
00:09:43,040 --> 00:09:50,400
or some level of harmony with them. So yeah, I would suggest is take each situation

113
00:09:50,400 --> 00:10:00,080
as distinct and try to act appropriately. If you want to create tension in certain situations

114
00:10:00,080 --> 00:10:07,200
in order to help people understand how their behaviors are causing problems for them and for other

115
00:10:07,200 --> 00:10:13,760
people, sometimes conflict can be useful. But if the conflict is just creating more antagonism,

116
00:10:13,760 --> 00:10:21,440
then yeah, it's probably not a skillful response to the situation,

117
00:10:23,040 --> 00:10:28,800
and it's not something that our heart would do. It's just a commutative state.

118
00:10:29,520 --> 00:10:33,040
Now, as you get more and more meditative, you're going to incline more and more

119
00:10:33,040 --> 00:10:38,960
towards the monastic life. So eventually it's going to get very, very difficult to live at home.

120
00:10:38,960 --> 00:10:48,640
One example is this man who became an anacami, and so he had this sort of difficulty.

121
00:10:48,640 --> 00:10:56,160
He wasn't at all accustomed to how to deal with people after becoming an anacami. It wasn't that

122
00:10:56,160 --> 00:11:06,000
he was deluded or anything, but he didn't have a response or means or dealing with means of

123
00:11:06,000 --> 00:11:14,400
interacting with people, and he may not have had any desire to find the response to interact with him

124
00:11:14,400 --> 00:11:18,480
because when he came home, he went to this another, but his teaching became an anacami,

125
00:11:18,480 --> 00:11:22,960
came back home and his wife was waiting for him at the door, and he just walked right past her,

126
00:11:23,680 --> 00:11:33,520
and without saying hello without giving the usual embrace and sat down at the table to eat his

127
00:11:33,520 --> 00:11:38,400
dinner, and he ate the dinner in silence, and then he went upstairs and he laid down on the floor

128
00:11:38,400 --> 00:11:48,000
to sleep, wouldn't even get into bed with her. And as a result, she was totally overwhelmed

129
00:11:48,000 --> 00:11:59,040
and distraught by this, unable to cope with this, and he knew that this was no good. He

130
00:11:59,040 --> 00:12:04,960
then realized, he said, if I don't tell her she's probably going to go crazy or freak out or

131
00:12:04,960 --> 00:12:11,200
even kill herself or something, because he was really freaked out by this, which understandably,

132
00:12:11,200 --> 00:12:17,440
no one, someone changes so completely so drastically, but it's a situation where he could get

133
00:12:17,440 --> 00:12:24,000
away with this because there wasn't, you know, she had no support but him, so he could shock her

134
00:12:24,000 --> 00:12:30,240
in this way to bring her around, you know, maybe without even intending it, but the situation allowed

135
00:12:30,240 --> 00:12:43,200
for it, so that once she was freaked out, he was able to pass the dhamma along to her. She had,

136
00:12:43,200 --> 00:12:51,280
he then said, look, here's how it is. I've listened to the Buddha's teaching and have now given

137
00:12:51,280 --> 00:13:00,160
up all attachment to central pleasures. I'm happy to stay with you and I'll treat you as a brother,

138
00:13:00,160 --> 00:13:09,200
as I'll treat you as a sister, if you want, and if you choose to find another husband, I will

139
00:13:09,200 --> 00:13:15,120
certainly not object, so you can get a complete freedom and try to, you know, use that to reassure her.

140
00:13:15,120 --> 00:13:24,400
As a result, her response to this was to ask him whether women could also practice this teaching

141
00:13:24,400 --> 00:13:32,320
of the Buddha and become realized for themselves, and he said, yeah, sure, anyone can.

142
00:13:33,920 --> 00:13:40,480
Maybe he didn't say it like that, but he said, yes, anyone can. And so she asked him if she could

143
00:13:40,480 --> 00:13:46,480
go to the monastery, and he took her to see the Buddha, I think, and I can't remember whether

144
00:13:46,480 --> 00:13:52,080
she became an arahand first, or she became a bikkhuni, but she actually asked to ordain,

145
00:13:52,080 --> 00:13:57,520
he didn't ordain, he stayed at home and continued to live as a lay person, and she ordained

146
00:13:57,520 --> 00:14:04,880
as a bikkhuni, and either before or after that became an arahand. And there's a sutta, the

147
00:14:04,880 --> 00:14:12,640
julawedala sutta, that it's attributed to her and her conversation, later conversation with her

148
00:14:12,640 --> 00:14:18,560
husband, her name was Dhamadhi Nith, no, no, was it? Was it Dhamadhi Nith? I think it was.

149
00:14:19,200 --> 00:14:25,840
And her husband was, yes, and her husband was Visaka, and so there's this conversation between

150
00:14:25,840 --> 00:14:31,520
the two of them. It's recording, it's a bit difficult. So in certain cases, the conflict can be

151
00:14:31,520 --> 00:14:41,760
useful, but I'm assuming you're not an anigami, and you might want to take it slowly and cautiously

152
00:14:41,760 --> 00:14:46,720
so that you don't create too much disruption, because that can get in your way, it can create

153
00:14:46,720 --> 00:14:53,760
resentment and negativity towards the Buddha's teaching, which is very harmful for people. You don't

154
00:14:53,760 --> 00:15:03,280
want people to have a bad impression of meditation and meditators. So yeah, I mean, it's not

155
00:15:03,280 --> 00:15:08,480
near fault, but you might want to be a little bit skillful about it, at least until you are able

156
00:15:08,480 --> 00:15:24,400
to find a life style that allows you to be a loop and meditative without creating content.

