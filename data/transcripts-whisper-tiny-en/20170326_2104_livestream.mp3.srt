1
00:00:00,000 --> 00:00:08,280
and I think it's worth mentioning to sort of understand the pastana means

2
00:00:08,280 --> 00:00:14,040
so it's actually a figure of speech and it makes sense that this wouldn't be

3
00:00:14,040 --> 00:00:19,560
something the Buddha used chiefly, chiefly the Buddha used the word toward

4
00:00:19,560 --> 00:00:30,800
or phrases in construct, panja or panja ananti, so panja yapasatim you see with wisdom, panja

5
00:00:30,800 --> 00:00:44,760
panja means to know, slowly, or know, truly, truly know something, and translate it as wisdom, but it means to

