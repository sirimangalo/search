1
00:00:00,000 --> 00:00:09,600
Okay, so the latest question comes from one of the commenters, asking whether I'd recommend

2
00:00:09,600 --> 00:00:13,640
to go to a 10-day v-passenomititation course.

3
00:00:13,640 --> 00:00:20,440
I guess the short answer here is no, because I wouldn't recommend something that I myself

4
00:00:20,440 --> 00:00:26,080
didn't teach or practice, and generally a 10-day meditation, v-passenomititation course

5
00:00:26,080 --> 00:00:30,720
has offered around the world is not something that I teach.

6
00:00:30,720 --> 00:00:36,880
As for how it might be a problem, I also can't say, and understand that many courses require

7
00:00:36,880 --> 00:00:44,360
you to sit for many hours a day, sometimes for an hour at a time, even for a beginner

8
00:00:44,360 --> 00:00:45,360
meditator.

9
00:00:45,360 --> 00:00:52,280
The way I teach is to do a course maybe two or three weeks, and some people might come

10
00:00:52,280 --> 00:00:56,960
for a part of a course, because it'll come just for a week, which is fine, but a full course

11
00:00:56,960 --> 00:01:03,920
for a beginner would take, say, three weeks, not more than that, but not much less, if

12
00:01:03,920 --> 00:01:05,800
you've never practiced meditation.

13
00:01:05,800 --> 00:01:10,800
And the way that works is we start the meditator off easily, and the whole course, the whole

14
00:01:10,800 --> 00:01:13,440
progression is much more natural.

15
00:01:13,440 --> 00:01:17,920
It's much more natural also because half of your meditation time is done in walking, whereas

16
00:01:17,920 --> 00:01:23,240
many courses are done with sitting for eight to ten hours a day.

17
00:01:23,240 --> 00:01:31,880
The practice that I teach is, according to the Buddha's teaching, that a monk or by extrapolation

18
00:01:31,880 --> 00:01:36,880
anyone who's serious in meditation should spend half their time walking, half their

19
00:01:36,880 --> 00:01:43,640
time sitting, so walk and sit in alternation for the whole of the day, and only breaking

20
00:01:43,640 --> 00:01:47,800
for eating and for sleeping.

21
00:01:47,800 --> 00:01:53,000
Now, for a beginner meditator, this is quite difficult, and so we start you off simply.

22
00:01:53,000 --> 00:01:57,440
We have you do mindful frustration, ten minutes walking, ten minutes sitting, and you're

23
00:01:57,440 --> 00:02:06,720
allowed to take a break, and do a courting as you see fit, or you feel capable.

24
00:02:06,720 --> 00:02:12,120
You can take a break and come back, and we don't have very much group practice, generally

25
00:02:12,120 --> 00:02:17,080
we'll do one or two group practices during the day, otherwise it's up to you to make

26
00:02:17,080 --> 00:02:22,200
your own schedule and we expect you to practice as much as you can comfortably.

27
00:02:22,200 --> 00:02:26,360
In this way, we slowly increase to the point where you don't even notice that all of a

28
00:02:26,360 --> 00:02:30,760
sudden you're walking one hour and sitting one hour, and you also don't notice it because

29
00:02:30,760 --> 00:02:35,960
you don't have to sit the whole of the day, you're allowed to get up and do walking,

30
00:02:35,960 --> 00:02:40,400
you're required to get up and do walking meditation.

31
00:02:40,400 --> 00:02:44,200
I would only recommend what I teach and what I practice and that is as I've explained

32
00:02:44,200 --> 00:02:52,440
it to a three week course, where you do walking and sitting meditation in tandem together

33
00:02:52,440 --> 00:02:57,400
and that you do it gradually, slowly building up to the point where you don't even notice

34
00:02:57,400 --> 00:03:02,160
that you're suddenly practicing eight, ten, twelve, even more hours of meditation

35
00:03:02,160 --> 00:03:03,160
in the day.

36
00:03:03,160 --> 00:03:04,160
It's not difficult.

37
00:03:04,160 --> 00:03:09,880
If you do it like this, this part isn't difficult, the hours aren't difficult.

38
00:03:09,880 --> 00:03:14,920
It's a difficult course and it's difficult to meditate, but you don't feel like it's

39
00:03:14,920 --> 00:03:16,920
an insurmountable obstacle.

40
00:03:16,920 --> 00:03:22,960
You suddenly feel like a pro over the course of the training.

41
00:03:22,960 --> 00:03:24,840
So I hope that helps.

42
00:03:24,840 --> 00:03:26,400
You're always welcome to give contact me.

43
00:03:26,400 --> 00:03:31,840
I can maybe give pointers as to courses in your area that I would recommend or maybe

44
00:03:31,840 --> 00:03:36,840
give some more detailed analysis of the various courses that are offered out there.

45
00:03:36,840 --> 00:03:40,840
You're welcome to contact me for more information, okay?

46
00:03:40,840 --> 00:03:43,840
And if you have any more questions, just leave them.

47
00:03:43,840 --> 00:04:10,840
See you.

