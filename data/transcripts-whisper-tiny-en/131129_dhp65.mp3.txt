Welcome back to our study of the Dhamapanda.
Today we continue on with verse number 65, which reads as follows.
Muhu Tampi, J. We knew Panditang by Yuru Bhasati.
Kipang dhamang wijana ti jyuhasu bharasang daa yatta.
Which means very similar to the last one.
But here we're talking about the opposite.
When you a wise person, Muhu Tampi ti, if a wise person, even for a moment Panditang by Yuru Bhasati,
should attend upon or hang out with a wise person.
Kipang dhamang wijana ti, they're quick to know the, to understand the dhamma.
Jyuhasu bharasang yatta, just as the tongue is quick to know the taste of the soup.
Just as the tongue to the soup of a wise person is able to quickly understand the dhamma of a wise person,
by hanging out with a wise person, even for a short time.
So this, again, a rather short story.
But we can provide a little bit of background.
This is in relation to these, a group of 30 friends,
who the story goes were out,
cavorting in the forest with their wives.
And one of them didn't have a wife, so they hired a courtesan for him, an escort, I guess.
Contact and looked in the yellow pages and found an escort service.
Something like the equivalent of the yellow pages would be to find an escort in those times.
The court is in and invited her along to be his wife for the day.
So they went into the forest and they're playing in the water.
And they took off all their clothes, jump in the water and are sporting around.
And weather in the water, if I'm not making this up, I didn't even know the water.
They were asleep.
I can't remember what it was.
I can't remember why they were negligent.
This courtesan runs off with all their jewels.
It goes picking through their clothes and gathers up all their valuables and runs off into the forest.
When they get back and find their clothes, they're missing all the jewels.
They realize right away what happened and they start.
They send their wives home, I guess.
It doesn't say about that, but they go off into the forest.
These 30 men trying to find this courtesan and get back their jewels.
It just so happens that the Buddha having taught the group of five ascetics in Isipatana,
having turned the wheel in Dhamma, and then having enlightened Yasa and all of his 54 friends.
And having sent the total of 60, Arahan's off into the world to spread the teaching.
No two of them going in one direction.
So he sent them all through India. He went himself off to Rajagaha to spread the Dhamma there.
Oruila.
There he would find Isipat, Oruila Kasaband, the other two Kasapabrasas and eventually go off and teach
Bimbisar and spread his teaching in Rajagaha.
But first, he did something that appears in the texts.
He stopped off where these 30 men were running around the forest trying to find this courtesan.
And he stopped under a tree, which is sat there, and meditation.
And these 30 men wandered upon him.
And they looked and they said, oh, he looks like a wise person.
They bet he can give us some good advice.
And so they wandered up to him and paid respect, gave him respect.
And then asked him, a vendor will serve.
You see, it looked like he'd been sitting here in the forest for a while.
And we'd wonder if you happen to have seen this woman.
And we're looking for a woman who stole our jewels.
I'm not exactly the sort of advice that most people would ask the Buddha for.
But the Buddha gives them something much more valuable.
He looks at them.
And he knows, the reason he's there is because he knows the ability or the qualities of these 30 men
that are highly cultivated individuals, even though they don't realize it.
And so he asks them, he says, what do you think is better that you should search after a woman?
I think you should search after yourself.
Search after yourselves.
I'm not exactly the advice they were looking for.
And of course, this is the Buddha.
And so they take in what he's saying.
And of course, it's much more important, really, even all the jewels that we lost are not really all that valuable in comparison to finding oneself.
Which actually sounds kind of non-Buddhist if you think about it.
But it's all about giving up yourself, right?
Giving up the idea of self.
But it's a lot just made of this passage, actually, for those people who think that the Buddha actually taught that there is a self or had some idea of there being self.
But it's quite clear from the Buddha's teaching that's not the case.
And this is simply a teaching instrument.
How to get the people, how to get them to look for something that's truly valuable.
And they're probably working news, or whatever would have been, they were Brahmin religion.
So they would have believed in a self.
And so it's a good way to get people's attention to say, well, why don't you seek out what is truly important?
Seek out yourselves.
Come to know yourselves and learn about yourselves.
And they say, well, let's sit down and I'll help you find yourself.
And have them sit down and he teaches them the dhamma, and they all became enlightened right there.
I don't know how many people have ever become enlightened listening to my dhamma.
I'm probably not the same category as both.
But it can't happen.
You know, sometimes listening to a talk, if you're clearly aware of the experience,
and the time that you're listening to the talk, it's useful because one, you can't go anywhere.
And two, the teaching constantly reminds you of good things.
Three, the company of the fellow audience members are all ideally on the same path.
And so there are good qualities to the experience of listening to the dhamma that you don't get elsewhere.
I mean, that timing is actually quite, can be quite easy to realize the teachings,
realize the truth, and to find the truth, to understand reality.
And so they did these 30 young men, became our hunts, and decided to become,
and obviously decided to become monks and proceeded along with the Buddha.
I'm not sure where they went, actually, maybe they went off on their own somewhere.
But the other monks were talking about this later, and this is how this verse came out.
They said, well, this is kind of crazy. You know, all of us would take so much work,
and maybe these monks weren't yet our hunts, but they were, or maybe they were,
but how long it took them to practice over the days and weeks and months and even years to become our hunt.
But these 30 young men who were earlier in the same day,
are frolicking in negligence, you know, in indolence.
And then suddenly they can become our hunts, and how is that?
And the Buddha came in and asked them, what are you talking about?
And they said, I'm talking about this, and then the Buddha said, well,
and then he actually told one of the Jatakas, based on this.
He reminded them of Jatakas once in a past life.
He had taught these 30 men were coming to kill him, and he was able to
help them realize the ways, and as a result they gave up their evil
and became disciples of it.
But the meaning behind it is that they had a lot coming into this.
They called Upanishaya. They brought from their past lives.
They all bring this from our past lives.
We come into this life with qualities, both positive and negative.
The fact that we're born human means we definitely still are born with
unwholesome qualities. Everyone, even the Bodhisattva is born as a human being.
I still have greed, still have anger, still have delusion.
Without those we wouldn't be born again as a human.
We have negative qualities, but we also carry all the positive qualities,
all that we've cultivated in our past lives, all the good work that we've done
in Buddhism and outside of Buddhism, helping other people,
being moral and ethical and a high-minded and spiritually-minded
and being inclined toward the spiritual life inside Buddhism, within Buddhism
and outside of it as well, all the wisdom that we've gained,
all the learning that we've gained from lifetime to lifetime.
It doesn't leave us except insofar as our memories go.
The memories, of course, fade away as they always do over time,
but the quality of the mind changes.
Yes, we're all aware of it, but it's...
And so the Bodhisattva said, it's because of we knew,
because these men are we knew, they're people who understand.
People who have this resonance in them.
They're the potential to resonate with the dhamma.
Just as the tongue has the potential, a person without their tongue damaged,
their sense of taste damaged, unless their sense of taste is damaged,
they have the potential to receive whatever the taste of the soup is.
Quite quickly, without any effort, unlike the spoon which we talked about last week,
the spoon no matter how long it touches the soup, it never tastes anything.
It's all about one's potential.
So the point is that the dhamma is good in and of itself.
There's nothing wrong with the dhamma just because we can't become enlightened off of it.
It's up to our quality, since we talked about last week.
If we don't have those qualities, we spend all the time we want with enlightened beings.
And if our deans and our speech and our thoughts are unwholesome,
and they are not inclined towards enlightenment, they will never get there.
And seriously, it's like being the spoon with the soup.
But the quality of a wise person is that they quickly understand the truth.
Even when they just taught the Buddha's teachings from the books,
they immediately get the meaning of it.
They very quickly get the meaning of it.
Some people when they hear the Buddha's teaching are confused and disinterested,
turned off even repelled by the Buddha's teaching.
When they hear about it being for the giving up,
the discarding of central desire and giving up of the whole world.
I think of this as a ridiculous, horrible thing, a bad teaching.
Teaching that is built on repression and so on and so on.
They're the misunderstandings that arise.
Come from a different sort of person.
Divas are a personality.
Those people who are inclined towards the world.
They're inclined towards craving and attachment.
They aren't able to understand the teaching.
And so that's a common question.
How do these monks in the time of the Buddha hear the Buddha giving a talk?
And some people just immediately became around.
Very quickly, like, sorry, Buddha.
So he did not even have to hear from the Buddha.
He heard from Asaji this one of the first five monks within the five ascetics.
And he just heard half of a stanza.
And he became a Swatapana.
Just from hearing half of the stanza.
He was a Swatapana.
He was ready for it.
Very sensitive tongue.
Quickly, he was able to realize the teaching.
So what does this mean for all of us?
It means that realistically, first of all, we should never fault the teaching.
It doesn't bring us anything.
If we can find fault with the teaching in an intellectual sense,
this is what we should give up the teaching.
But when we find the teaching doesn't work, we have to ask ourselves one of two things.
The teaching is effective, or are we unable to understand the teaching?
Are we unable to understand the Buddha and the practice?
There's these two ways.
When you see enlightened beings, and this is the key,
if your teacher is full of greed and full of anger and full of delusion,
maybe it's my teacher's fault, or the teaching is impure.
But when you know that your teacher lives like the Buddha,
we know that we have the Buddha, and when we see that people who practice meditation
are able to free themselves from these things,
then we have to understand that it's most likely our own fault.
We ourselves are just lacking in the requisites to quickly understand the teaching.
The second part is not to be discouraged.
Not to be quickly discouraged because after a week of practicing,
I'm still not enlightened.
Here I have been sitting for ten minutes, and I don't feel anything yet.
They're kind of saying.
To understand that part of this is the cultivation.
Tolkien, Tolkien, the Buddha said,
dropped by drop, or little by little.
The goodness doesn't come to you all at once.
It's something that you cultivate little by little every moment.
Every moment that we're mindful.
We're more that we're aware of something that we're clearly aware of.
Experience, just as it is without liking,
or judgment, or attachment, or conceit, or views,
that moment is blessed.
That moment is great.
If we can cultivate those moments again and again continuously,
eventually becomes habitual,
and that's when we're able to understand the number.
For example, when you learn about Buddhism,
before you practice meditation, it's a lot less clear
than after you actually sit down and practice the teaching
and then approach the same text.
You find that you understand them so much better and so much clearer
and so much more fruitfully.
And your studies are far greater fruit because of your practice.
So it's not to be discouraged.
We shouldn't think that we're useless.
We shouldn't think we're the spoon.
We should always look at our own attitude as the point
that we shouldn't be like Udayi, this guy who just hung out
with the Buddha and pretended to be wise,
but never really listened to what the Buddha had to say
or put it into practice or tried to understand it even.
We should always try to put the teachings into practice,
try to understand the more and try to apply them to our own life,
not to just keep them up in our head,
or chant them or teach them to others
or think about them,
but we should actually try every day
and in every aspect of our lives to incorporate them into our lives.
How can we be more generous?
How can we be more moral?
How can we be more focused and concentrated
and clear in our minds?
We're cultivating goodness,
the three types of goodness
and cultivating morality and concentration in wisdom.
Most importantly, using mindfulness to cultivate it all
because mindfulness is what clears the mind
and allows you to see the difference between wholesome and unwholesome
and allows you to keep the mind from wandering in unwholesome ways,
focus the mind and see things clearly.
It's what leads us to niband it, freedom from suffering.
It's just the meaning is it's all on us.
Up to us whether we're going to be the spoon or the tongue.
So the analogy ends there.
It doesn't mean that if you're a spoon, you're always going to be a spoon.
It just means don't be a spoon.
Be a tongue and try to receive the teachings.
Cultivate knowledge and understanding,
so you can understand the teachings.
And don't think that just because you've memorized all the Buddha's teachings
or you know how to practice meditation,
that you're a Buddhist or that you're practicing the Buddha's teaching.
You should ever take that for granted.
You find them the more you practice and the longer that you.
Associate with Buddhism with the Buddha's teaching.
The deeper your understanding of the same teachings gets.
And you realize that there are still things that you're missing.
It gets more and more refined and your sense of taste becomes more and more refined
until you can taste the dhamma, which has the taste of freedom.
So simple verse, simple story, simple meaning.
You have to cultivate knowledge and understanding.
Understanding of the Buddha's teaching requires,
has to be ready for it or has to be in its condition to understand the truth.
So that's the story for tonight.
Teaching on the dhamma pada.
Thank you for tuning in and for coming out.
All those who are here locally.
I wish you all to find true peace, happiness and freedom from suffering.
