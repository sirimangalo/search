Since it's a complicated world to what extension monks and nuns, for example in the years
of formative training, be aware of or learned in secular laws, enough so that they don't
break them, I suppose, I wouldn't do to have monks breaking laws, otherwise I'm trying
to think, what exactly does that mean?
Yeah, that's really it, no?
If you mean it's, should they know that what they're doing is against the law in this
way or that way, then yeah, I think that's quite useful, depending on the law, I mean
the whole copyright laws is maybe an exception, but I don't quote me on that, don't make
a video and put it up on the internet with me saying that, but other than that, I don't
can't think of a reason why they wouldn't need to know the laws.
To my understanding, we should not learn, we should be probably be aware of the laws of a country,
but we should not study it, because it is worldly studies and we should do that, I think
that is so, and I think as a monk or nun, we have the rules, we have the party mocha, and
when we keep it, when we take it serious, we hardly ever can break seriously any law of
a country, I mean there are some laws like the former mentioned, but in general, if you
keep your, if you keep your precepts as a monk, the party mocha, you can't break really
the laws of a country, so there is not really the need to learn, secure, secure laws.
Yeah, I mean the whole idea of premise that somehow the complexity of the world requires
it, it's not really, not in, not in, not a sufficient reason to learn about that complexity.
There's nothing wrong with learning complex subjects if they're inherently useful, like
how to sow a Rome, for example, it's kind of complicated, but there's nothing wrong with
learning, it's a good thing to learn because it, it supports your, you're monastically
learning how to use a computer, for example, can be useful to spread the teaching to people
who use computers, so the complexity is not, not sufficient, but there's nothing wrong
with learning complex subjects to an extent, and maybe you could go even further and say
that because of the complexity, we want to try to avoid that sort of thing as much as
possible.
I mean, hopefully you're not suggesting that monks and nuns should try to make their lives
more complex as the world becomes more complex.
We're trying to give up the world, then we're trying to give up the complexity, so
our lives should be as simple as possible, especially when the world becomes more and
more complicated, I would think that's a reasonable thing.
