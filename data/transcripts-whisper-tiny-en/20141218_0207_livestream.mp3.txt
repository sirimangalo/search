Okay, good morning.
Broadcasting lies.
Tomorrow I'm off to the airport.
I've been near the airport, spent tomorrow, last day in Bangkok, and then early morning
Saturday to Sri Lanka.
I'm told that the internet is good in Sri Lanka, I might be able to even do a
long radio this weekend, let's see, going to be busy, it looks like there's an
whole schedule set up for Sri Lanka, teaching every day, almost every day.
Meditation is just like any other work that we undertake, it's got a lot
in common with training the body, and that's how it should be seen, if you want real
results from meditation lasting results, it's the difference, it's like the
difference between getting drunk or taking drugs to please the body, to make
you feel good, versus training the body to really make you good, make it work
good, healing the body of its sicknesses, cultivating strength and abilities.
Meditation is also a lot like other mental trainings, training to be a lawyer or an
accountant or a doctor, training in chess or training, mathematics, sciences,
engineering, it's similar, it's using the same building blocks, so the
strategies all have to be the same, and there's one strategy that stands out as
sort of the key to success, the key to achievement to progress, it's called the
path to idhi, or the things that lead to idhi, idhi meaning power or strength or
accomplishment, success, before things that lead to success in any sphere, not
just in meditation, useful for us to remember, really all you need to succeed for
things, for general principles, called the for idhi pata, if you don't have these
you can succeed, if you have them succeed, they are essential, and in order we
have chanda means, somewhere between contentment with the activity and interest in the activity,
it's not quite desire, if you want to do something that's not necessarily helpful, it can
actually get in your way because you're too eager, it can actually be too keen, but maybe
that's the word keen, being keen about something, you do have to be keen, it's not
exactly desire, but for some reason or other you're keen on doing it, in some cases it's
keen because it brings you pleasure like keen on eating food or keen on romance or sexuality,
keen on drugs and alcohol, keen on candy, but it doesn't have to be that, it can be keen
on working, keen on helping other people, keen on cultivating goodness in yourself, keen
on on anything, but if you're not keen about something, success is most generally outside
of your grasp, very difficult to achieve success when you are not keen on what you're
doing, it's possible to succeed, it's just going against the grain, it's like the oil in
your, the motor oil, the oil in your motor, without it, it's very hard, it's hard on
the system, because you don't want to do what you do, meditation of course is the worst
with this, if you don't want to meditate, it becomes a chore, you have a problem, this
is a problem, but obviously a very common problem, it's easy to get to the point, especially
in insight meditation where you're dealing with suffering, where you don't really want
to meditate anymore, you're not happy, you're not comfortable doing it anymore, but the
neat thing about insight meditation is that that can become a meditation, and if you're
clever, if you really know what you're doing, if you really understand what your principles
of, the basic principles of insight meditation, then it's not a barrier, because that's
a meditation, you meditate on the disliking, you meditate on the boredom and the frustration,
paying in the suffering, and you find there's no excuse, you don't need excuses not to
do it, you don't have any reason for your disliking, for your aversion, you may find
this doesn't really get in the way, and if I'm contentment, it's very hard to want to meditate
after a while, because it's not something you can cling to, no meditation is an activity,
insight meditation is not something you can cling to.
You can't think about meditation, if you really understand it, and if I'm practicing
you, you can't think about it and say, boy, I want to do that, you can't feel attraction
towards it, once you understand it, because it's nothing, there's nothing, there's no
there's no object of attraction, there's only the consideration, those things that are
undesirable, and when you think about them, you're mind of course requires, and yet the
interest comes, the more you practice, the more interested you are in practicing, it's
not even practicing so much as returning to a state of clarity of mind, cleaning away
the garbage, ceasing to engage in the cultivation of unwholesiveness, ceasing to engage
in evil in that which causes suffering for doing others.
So anyway, something for us to keep in mind that we need to be interested in, keen
on what they're doing, keen on meditation, so something to be cultivated.
So as a sign, if you're not keen on it, don't just dismiss that and keep going and
plugging away anyway, don't judge through it, you have to actually work through that,
the drudgery, the dissatisfaction with the meditation, meditate on that, it's important
because you won't get anywhere without being keen about it, without reading yourself
without aversion, just going against the grain all the time.
Number two is effort, when you need effort in order to succeed, this is of course true
in the world, if you don't work hard, you don't get anywhere.
In the dhamma as well, if you don't work at it, you don't get anywhere, you need the
effort to rid yourself of unwholesome states, you need the effort to prevent wholesome
states from arising, the effort to cultivate wholesome states, and the effort to maintain
and protect wholesome states that have already risen, these are the four right efforts
in Buddhism.
In a way, it's kind of a different sort of effort from what we would normally understand
by the term with the effort, it's just pushing harder and harder, and that's not really
what it means, it means working certainly, but working in a specific way, working to maintain
wholesomeness and stay away from unwholesome states.
That's all.
It doesn't mean pushing harder, it doesn't mean meditating hours and hours and hours
and hours, it can be a good thing, if it's done properly, it's a wonderful thing.
But there has to be a company by right effort, not wrong effort, you can't just push
and push and push yourself, you go crazy, right effort is a specific type of effort, the
effort to be pure of mind, effort taken to be clear in mind, and you need that if you
don't have effort, your mind will be, you won't really be mindful, really meditating,
you can walk and sit for hours and hours and it won't be any good, and put effort.
Of course, you just need effort just to actually meditate, it takes effort to get up early
in the morning, takes effort to sit up straight, it takes effort to walk back and forth
without stopping, it just takes, it does take effort, if you don't have it, that's going
to be a hindrance.
So as with anything, this is part of our success, how to find success.
The third intibanda is called jita, jita means mind, so it's actually a curious usage
of the word mind, but it means keeping in mind, if you want to succeed at anything, the
third quality that you need is to keep it in mind, not lose sight of the act of the
goal, do not lose sight of the act, do not forget your mission, do not forget your goals,
do not lose track of them, do not get sidetracked by other things, do not get distracted
by other things, and this is of course nowhere more important than in meditation,
where the object is the mind itself, the object is reality, it's the practice of the
mind, it involves the mind, so keeping the mind on the task is the whole thing, keeping
it in mind takes on a whole new meaning in meditation, because that's the essence of meditation,
when your mind isn't on the meditation, you can't progress at all whatsoever, perhaps most
crucial before, because it is the practice itself, the practice is keeping in mind that
in the world, in the world the affairs take your mind off of it, sometimes it's fine
because it's worth running by itself, your business will continue, even your physical
activities, you can be lifting weights and let your mind wander, you just have to keep
your mind on it when it's necessary, meditation isn't like that, meditation is obviously
requires this to a much greater degree, but it still involves keeping your mind
on the fact that you, it's time to do, do sitting meditation, time to do walking and
sitting, time to, okay, not to be mindful in all your activities when you're eating, reminding
yourself that you have, remembering that you should be mindful when you eat chewing, chewing,
swallowing, tasting, when you're showering, when you're washing your clothes, whatever
you're doing, the ability to remember, to remind yourself, ah, I should be mindful of
this, remind yourself and keep yourself in the present moment, this is what has been
completed without this meditation will never succeed, one hundred percent. Number three, number four is called V-Mungsau,
V-Mungsau means out or, ah, to divide it, now I would say guess better, since out,
mungsau, mungsau, I'm not even sure where that comes from, but V-Mungsau means the ability
to discriminate, it's another word for wisdom, but discrimination is what is used to
because that's what it's, the aspect of wisdom that's important is the ability not just
to keep your mind on something, but to, ah, be clever and figure out ways and, and
refine your, your attention, refine your, find your, find your, and adjust your activity.
So obviously, so obviously quite important in all, activities were successes, the goal
when we were trying to succeed at something, it's not just enough to, it's, well, it's
true in some spheres, it might just be enough to plaw it along, but in most cases, you
need some, it wouldn't be enough just to have the first three without having this wisdom
to be able to adjust and discriminate and refine your activity.
So, in, at work, finding better ways to do things, making the right decisions in business,
in family and society, in, in school, being clever, studying, being clever, choosing
the right topics and having the right tools and, in general, just being able to be efficient
having the wisdom to, just to see when you're being inefficient and so on, to see when
you're actually on the wrong path, practicing incorrectly, working incorrectly, working
improperly.
So then we'll see it to see a better way, and then to have wisdom, to not just plaw
along, but to do it with wisdom, so you, you do it right, and you're able to adjust your
flexible mistakes with the, the meditation, this is also obviously a very important aspect
of the practice of the path, and that you can't just do walking, sitting, walking, sitting,
you think you're going to become enlightened, you need to be clear about the object,
your mind has to be clear, your mind has to be, you know, on the right path, in the
right way, in the right place.
So when the stomach is rising, your mind has to be aware of the stomach rising, it goes
around, it really, it doesn't, shouldn't really need to be said, but your mind has to be there.
Well that's, that's Jita, I know you know that your mind has to be there, but that you
have to be clear about nature of your practice, you have to be able to adjust your
practice, you have to be able to see when you're doing things right and doing things wrong,
you have to be able to, for example when you're walking and you find yourself very distracted,
you have to be, discriminate, you have to be able to see that walking isn't helping
you right now, so you sit down, you're sitting down and you feel very tired, drowsing
off, then you have to discriminate, you have to be, discriminate, you have to be clever
and decide to walk instead, you have to find ways like on and on when he was, when he
became enlightened, he was doing walking all night and he didn't get anywhere, and so
last moment he decided, he realized that he'd probably better lie down because he had
too much effort, and so he lay down and right away he became enlightened, this kind
of thing, it actually can happen, it's quite, quite possible that you just adjust something,
you just have the wisdom to adjust your practice, to be able to catch the things that
you're not noting, realize that when you're struggling to have the wisdom to catch what
it is that's causing problems, catch what it is that's holding you back, you can't just
force yourself to practice properly, you can't force the practice to succeed, it's not
like any other work in that way, any wisdom, you have to find the way through wisdom,
then yeah, yeah, but he's so jutting, you become pure through wisdom, you can discriminate
this very important, not just walks it, walks it, walks it for hours and hours, you can
do it for lifetimes and not get anywhere if you're not doing it properly, mind isn't
in the right place and you're not adjusting, constantly adjusting your practice, constantly
flexible, it's got this is most apparent in regards to the layers of practice and the types
of experience, in the way that a meditator figures out, in a sense figures out an experience
or figures out an engram, a type of mental activity, figures out how to be mindful of
something and then they stick to that and they try to do that again and again and again
and that's not how meditation works because every moment is going to be different, you're
always changing, even your changing is changed, even your changing is changed, you have
to remember that, it's never going to be predictable, that's the truth of it, it's not
just that it comes again and again and again but that it's always changing and if you're
just trying to use the old wave without having we mung sound, without being able to break
apart and see the truth behind the current experience, understand the current experience,
you have a very hard type of doing the same thing over a little bit of information and
the ability to understand the current experience, you know, it's uniqueness, so there
you go, those are the four independent, they're very useful, something for us to think
about during our practice, keep in mind so that our practice may succeed, thank you all
for tuning in and may all of your practice, everyone's practice succeed, be well.
