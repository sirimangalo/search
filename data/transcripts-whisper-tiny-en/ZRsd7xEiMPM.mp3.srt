1
00:00:00,000 --> 00:00:05,000
How to deal with swallowing during meditation?

2
00:00:05,000 --> 00:00:08,000
It's a good question, it seems simple enough.

3
00:00:08,000 --> 00:00:09,000
But it can be quite a district.

4
00:00:09,000 --> 00:00:14,000
It happens to be something that comes up as meditators often.

5
00:00:14,000 --> 00:00:17,000
And then you get concerned about your swallowing and it makes you worried

6
00:00:17,000 --> 00:00:19,000
and then worrying causes you to swallow more.

7
00:00:19,000 --> 00:00:23,000
And that's one of these things that actually can snowball.

8
00:00:23,000 --> 00:00:28,000
The simple answer is just be mindful of it.

9
00:00:28,000 --> 00:00:34,000
With all of these things, convince yourself that there's no problem with it.

10
00:00:34,000 --> 00:00:37,000
It's not wrong to swallow.

11
00:00:37,000 --> 00:00:39,000
It's not wrong for these things to come up.

12
00:00:39,000 --> 00:00:42,000
There's no problem with them.

13
00:00:42,000 --> 00:00:50,000
When you do that, you'll find it less of a distraction.

14
00:00:50,000 --> 00:00:55,000
The key is that you can meditate on anything in any position.

15
00:00:55,000 --> 00:00:59,000
And people don't, quite good at forgetting that.

16
00:00:59,000 --> 00:01:06,000
Even those of us who have been indoctrinated and instructed in the practice.

17
00:01:06,000 --> 00:01:08,000
We forget and we think, well, what do I do with this?

18
00:01:08,000 --> 00:01:12,000
Not realizing that it's just be mindful of it.

19
00:01:12,000 --> 00:01:14,000
Because when it's teaching, it's non-cell.

20
00:01:14,000 --> 00:01:16,000
You don't want to swallow.

21
00:01:16,000 --> 00:01:18,000
You say, what can I do about it?

22
00:01:18,000 --> 00:01:22,000
And the meaning behind that is, how can I make it stop?

23
00:01:22,000 --> 00:01:25,000
How can I, or how can I deal with this situation?

24
00:01:25,000 --> 00:01:27,000
And how can I fix the situation?

25
00:01:27,000 --> 00:01:29,000
I mean, often that's the case.

26
00:01:29,000 --> 00:01:31,000
People want to fix the situation.

27
00:01:31,000 --> 00:01:35,000
That being the case, it's the wrong attitude.

28
00:01:35,000 --> 00:01:42,000
And the right attitude is to simply be mindful and objective about it.

29
00:01:42,000 --> 00:01:44,000
And that's what it will teach you.

30
00:01:44,000 --> 00:01:47,000
As you see, it's not actually you're swallowing.

31
00:01:47,000 --> 00:01:50,000
It's a product of causes and conditions.

32
00:01:50,000 --> 00:01:53,000
And it comes and then it goes.

33
00:01:53,000 --> 00:01:54,000
And that's it.

34
00:01:54,000 --> 00:01:57,000
There's nothing to do about it, except let go of it.

35
00:01:57,000 --> 00:01:58,000
It's rejected.

36
00:01:58,000 --> 00:02:21,000
Stop clinging to it.

