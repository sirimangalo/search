1
00:00:00,000 --> 00:00:19,860
I don't hear it on my end.

2
00:00:19,860 --> 00:00:28,160
You won't because it's the same way I didn't hear that you were echoing through my speakers.

3
00:00:28,160 --> 00:00:29,160
Not anymore.

4
00:00:29,160 --> 00:00:30,160
Go ahead and speak.

5
00:00:30,160 --> 00:00:31,160
Wait.

6
00:00:31,160 --> 00:00:32,960
No, it's me who speaks.

7
00:00:32,960 --> 00:00:33,960
Now I don't hear it.

8
00:00:33,960 --> 00:00:34,960
No, it's fine.

9
00:00:34,960 --> 00:00:35,960
Okay.

10
00:00:35,960 --> 00:00:36,960
Let me know.

11
00:00:36,960 --> 00:00:47,120
I can put on a headset if we'd be.

12
00:00:47,120 --> 00:00:56,840
So yeah, I was invited to something else at 9 p.m., but I rejected it.

13
00:00:56,840 --> 00:01:02,440
It's never nice to refuse an invitation, right?

14
00:01:02,440 --> 00:01:05,640
But it was too similar to what I'm doing here.

15
00:01:05,640 --> 00:01:10,840
Yeah, am I going again?

16
00:01:10,840 --> 00:01:15,640
On and off.

17
00:01:15,640 --> 00:01:22,440
So they wanted me to teach meditation and answer questions.

18
00:01:22,440 --> 00:01:29,480
I don't know really what the group is, and I was on this group before once, and I did this.

19
00:01:29,480 --> 00:01:38,120
But to teach meditation again, it's, you know, I don't know that it's really worthwhile.

20
00:01:38,120 --> 00:01:44,240
If they want, they can come here and learn meditation, they can read my booklet, it teaches

21
00:01:44,240 --> 00:01:48,120
how to meditate.

22
00:01:48,120 --> 00:01:53,520
We have 28 people signed up, I think, for the meetings, that's great, a little bit of

23
00:01:53,520 --> 00:01:54,520
problem.

24
00:01:54,520 --> 00:01:59,320
If you're one of those people and you're, we haven't met yet, you have to really get

25
00:01:59,320 --> 00:02:06,160
a sense of how it works, and you really should visit the hangouts.google.com link, because

26
00:02:06,160 --> 00:02:07,320
that's your homepage.

27
00:02:07,320 --> 00:02:09,320
That should be your hub.

28
00:02:09,320 --> 00:02:13,000
You don't really need to use the meeting page after you've signed up, as long as you

29
00:02:13,000 --> 00:02:19,200
get the time, right?

30
00:02:19,200 --> 00:02:26,920
There's a link that comes up, and you push that button, and it starts to hang out for

31
00:02:26,920 --> 00:02:29,120
you to get you into it.

32
00:02:29,120 --> 00:02:37,360
But from there, I'm going to use the actual hangouts.google.com page to send you stuff.

33
00:02:37,360 --> 00:02:41,840
So you need to have that open, and you need to know that you're calling me, I'm not calling

34
00:02:41,840 --> 00:02:47,560
you, because, well, I don't have your email, I don't have your contact information.

35
00:02:47,560 --> 00:02:51,120
First of all, and second of all, I'm not coming, I'm mucking around.

36
00:02:51,120 --> 00:02:58,440
It's up to you to, if the mountain doesn't go to Muhammad, as I can go, Muhammad has to

37
00:02:58,440 --> 00:02:59,440
go to the mountain.

38
00:02:59,440 --> 00:03:06,560
I don't know if that's proper, but I can't, I don't have the, I don't really have the,

39
00:03:06,560 --> 00:03:13,520
well, it's easier for 29 people to contact you than for you to contact 29 people.

40
00:03:13,520 --> 00:03:28,120
That's right, maybe, yeah, because I have to do it 29 times, yes.

41
00:03:28,120 --> 00:03:37,160
And so I've been thinking to maybe add another slot, but, or two or three or four, but

42
00:03:37,160 --> 00:03:40,160
that might require quitting university.

43
00:03:40,160 --> 00:03:45,680
So we'll see how that works, it might just decide that this university thing wasn't worth

44
00:03:45,680 --> 00:03:46,680
anything after all.

45
00:03:46,680 --> 00:03:53,800
I mean, I already know it's not worth that much, but, I mean, I wouldn't expect this

46
00:03:53,800 --> 00:03:58,560
really to, to be set up and doing so much teaching.

47
00:03:58,560 --> 00:04:08,160
So I have to change based on the situation, it's not useful, no, it's not, so now the light

48
00:04:08,160 --> 00:04:16,840
over here, I'll do that later.

49
00:04:16,840 --> 00:04:18,840
So do we have any questions?

50
00:04:18,840 --> 00:04:26,040
We do, first question, do you have puls audio, the loop back devices and dark ice turned

51
00:04:26,040 --> 00:04:27,040
on?

52
00:04:27,040 --> 00:04:28,040
Yes.

53
00:04:28,040 --> 00:04:29,040
Okay.

54
00:04:29,040 --> 00:04:35,080
Very good, then we're ready for real questions, but we have a group of Tibetan monks coming

55
00:04:35,080 --> 00:04:40,040
to our college this coming week and their schedule involves building a sand mandala.

56
00:04:40,040 --> 00:04:44,080
Is that a sort of tradition for monks or does it have any spiritual value?

57
00:04:44,080 --> 00:04:45,080
Thank you.

58
00:04:45,080 --> 00:04:48,720
It's not work, it's not going to work.

59
00:04:48,720 --> 00:05:05,920
That's what I need, go ahead and speak, test, test, test, test, loop back to no output

60
00:05:05,920 --> 00:05:21,320
from the back of monitor, right, I forget, don't know, can you speak again?

61
00:05:21,320 --> 00:05:22,320
Yes.

62
00:05:22,320 --> 00:05:23,320
Can you hear me?

63
00:05:23,320 --> 00:05:40,720
Yeah, but why isn't that a hand test, test, test, no, I think it's working, maybe maybe

64
00:05:40,720 --> 00:05:41,720
not.

65
00:05:41,720 --> 00:05:42,720
Go ahead.

66
00:05:42,720 --> 00:05:43,720
Can you say again?

67
00:05:43,720 --> 00:05:44,720
Yes.

68
00:05:44,720 --> 00:05:45,720
I'm not.

69
00:05:45,720 --> 00:05:46,720
Go for a question.

70
00:05:46,720 --> 00:05:51,200
Oh, the question, okay.

71
00:05:51,200 --> 00:05:54,880
And as Bantay, we have a group of Tibetan monks coming to our college this coming week

72
00:05:54,880 --> 00:05:58,240
and their schedule involves building a sand mandala.

73
00:05:58,240 --> 00:06:00,480
Is that a sort of tradition for monks?

74
00:06:00,480 --> 00:06:02,560
Does it have any spiritual value?

75
00:06:02,560 --> 00:06:03,560
Thank you.

76
00:06:03,560 --> 00:06:09,200
Yeah, they're Tibetan monks, I'm not a Tibetan monk, so I'm not really well equipped to answer

77
00:06:09,200 --> 00:06:15,280
that question, sorry.

78
00:06:15,280 --> 00:06:18,160
Bantay, I have a question concerning right livelihood.

79
00:06:18,160 --> 00:06:22,360
What are your thoughts about working in the games industry and potentially making games

80
00:06:22,360 --> 00:06:25,720
or simulations that contain violent aspects?

81
00:06:25,720 --> 00:06:31,520
I'm not so concerned about the violent aspects, I understand that concern, but it's not,

82
00:06:31,520 --> 00:06:37,280
I think as concerning as people make it out to me, you know, I think there is a concern

83
00:06:37,280 --> 00:06:44,400
there, sure, but the bigger concern is the entertainment, you know, it's maybe not the

84
00:06:44,400 --> 00:06:45,720
bigger concern, I don't know.

85
00:06:45,720 --> 00:06:51,560
But personally, to me, it's more about the entertainment, the doing something that's

86
00:06:51,560 --> 00:06:56,640
not of any value to people, not of any use to them, and in fact, it's helping people

87
00:06:56,640 --> 00:07:07,800
to waste time and to become negligent.

88
00:07:07,800 --> 00:07:13,560
So anything to do with entertainment, you can, I think you can look at the eight precepts.

89
00:07:13,560 --> 00:07:17,640
So breaking the five precepts, if livelihood involves breaking the five precepts, that's

90
00:07:17,640 --> 00:07:18,640
really bad.

91
00:07:18,640 --> 00:07:30,600
But if it involves breaking the eight precepts, then it's problematic because you're involved

92
00:07:30,600 --> 00:07:46,280
in activities that encourage other people to engage in activities that hinder them

93
00:07:46,280 --> 00:07:51,080
on the path, obstruct them on the path.

94
00:07:51,080 --> 00:07:59,600
So that isn't the case with livelihood like selling food or necessities, things that

95
00:07:59,600 --> 00:08:00,600
people need.

96
00:08:00,600 --> 00:08:07,400
You know, so many things that you can offer or you can provide that actually, either

97
00:08:07,400 --> 00:08:14,760
neutral or actually are conducive to spiritual benefit.

98
00:08:14,760 --> 00:08:21,400
So it's definitely a problem, it's definitely not without its problems, it's not

99
00:08:21,400 --> 00:08:33,080
wrong wrong, but it's maybe a little wrong.

100
00:08:33,080 --> 00:08:38,280
As to the violent aspect, I mean, you're not actually killing anything.

101
00:08:38,280 --> 00:08:42,920
And I wouldn't say you're necessarily, when I used to, when I was young, we played doom

102
00:08:42,920 --> 00:08:53,720
in Wolfenstein, we played Wolfenstein before it was 3D and warcraft, starcraft, a lot

103
00:08:53,720 --> 00:09:01,400
of killing, a lot of games with killing Duke Nukem, we played the killing aspect, it's

104
00:09:01,400 --> 00:09:07,480
not really, you know, it's more like tag, really, or yeah, it's more like tag than anything

105
00:09:07,480 --> 00:09:11,240
or when I was younger, we played paintball.

106
00:09:11,240 --> 00:09:15,920
We actually went out into the foil, had paint guns and we went out and went into the forest

107
00:09:15,920 --> 00:09:19,320
and tried to, we spent more time trying to find each other than actually shooting each

108
00:09:19,320 --> 00:09:20,320
other.

109
00:09:20,320 --> 00:09:24,760
But shooting each other, no one was, there was no sense that you were actually hurting each

110
00:09:24,760 --> 00:09:34,360
other, it was a competition and so really that aspect, I think, isn't nearly as problematic.

111
00:09:34,360 --> 00:09:39,100
I mean, if someone is, they're the problem I suppose is if someone is psychopathic or

112
00:09:39,100 --> 00:09:46,920
sociopathic, then they'll take it in a whole other way and for them it's completely, it's

113
00:09:46,920 --> 00:10:02,360
not tag anymore, it's actually a means of the cruel or enacting fantasies of cruelty and evil.

114
00:10:02,360 --> 00:10:08,240
So there is that, there is a problem, but for most people I don't think it's really, all

115
00:10:08,240 --> 00:10:10,720
that it's made out to me.

116
00:10:10,720 --> 00:10:19,760
I spent a lot of time playing video games online and greed and delusion are rampant.

117
00:10:19,760 --> 00:10:26,000
Yeah, online I imagine it's even worse because you've got the internet community, I heard

118
00:10:26,000 --> 00:10:33,040
something about, oh there's a lot of awful stuff that goes on in the chats and they actually

119
00:10:33,040 --> 00:10:34,040
talk to each other, right?

120
00:10:34,040 --> 00:10:40,720
We talk on Mumble, we do rates on Mumble, right?

121
00:10:40,720 --> 00:10:46,160
And so there's a lot of people calling each other names and insulting each other's mothers

122
00:10:46,160 --> 00:10:49,640
apparently, it's a big thing, am I right?

123
00:10:49,640 --> 00:10:58,160
Well, I mean there's a lot of trolling, sure, but there's text chat as well, so they call

124
00:10:58,160 --> 00:11:03,600
it, that's where there's a lot of trolling, which is a chat for the whole realm, but there's

125
00:11:03,600 --> 00:11:10,600
a lot of greed and a lot of delusion, if you're trying to meditate and control that kind

126
00:11:10,600 --> 00:11:28,000
of thing, online games are probably the wrong direction, at least that type.

127
00:11:28,000 --> 00:11:31,960
How to accept people who treat you with no respect, I'm finding it hard to let go of my

128
00:11:31,960 --> 00:11:33,960
feelings.

129
00:11:33,960 --> 00:11:41,960
Yeah, well, a lot of meditation, have you read my booklet?

130
00:11:41,960 --> 00:11:47,560
Recommend greeting my booklet, starting to practice, it should help you let go.

131
00:11:47,560 --> 00:11:52,200
I mean that ingos isn't something you can do, something that comes naturally when you

132
00:11:52,200 --> 00:12:02,160
see clearly.

133
00:12:02,160 --> 00:12:07,520
And then a follow-up question on the right livelihood about the game industry, would

134
00:12:07,520 --> 00:12:12,440
the response differ if the games were nonviolent, in other words encouraging collaboration

135
00:12:12,440 --> 00:12:14,480
and cooperation?

136
00:12:14,480 --> 00:12:23,080
I mean, maybe a little bit, but again, it depends on whether they're actually helping

137
00:12:23,080 --> 00:12:32,080
it cultivate wholesome mind-states, again, it's fake, right, so you're not actually

138
00:12:32,080 --> 00:12:37,560
doing anything for anybody, not most of the time, so what's the point of it is to have

139
00:12:37,560 --> 00:12:46,440
fun or to spend time with each other, then it's probably not so problematic, it's still

140
00:12:46,440 --> 00:12:48,920
probably a problem with the eight precepts.

141
00:12:48,920 --> 00:12:51,800
Isn't that what Second Life is like though?

142
00:12:51,800 --> 00:12:55,920
I've never actually signed into Second Life's platform.

143
00:12:55,920 --> 00:13:01,120
It's not a game, there's no objective, it's a world, it's virtual reality, it's what we

144
00:13:01,120 --> 00:13:07,400
all expected to be already happening, we expected everyone to be on Second Life, but it's

145
00:13:07,400 --> 00:13:11,560
never really taken off the way it was expected to, not yet anyway.

146
00:13:11,560 --> 00:13:16,440
Now with maybe all this new virtual reality stuff, it'll actually happen, but Second Life

147
00:13:16,440 --> 00:13:24,480
is a world, you buy land, you pay real money to buy land and set up a house, so there's

148
00:13:24,480 --> 00:13:31,760
a lot of dating and stuff, people walk around half naked, and you pay real money or some

149
00:13:31,760 --> 00:13:36,480
kind of a game gold, it's game money, but you buy it, you have to buy it.

150
00:13:36,480 --> 00:13:42,720
With real money, there's no real way to make it, people, you can give each other, people

151
00:13:42,720 --> 00:13:51,160
gave me like 4,000, I have like 4,000 Linden dollars, which I think is like $40 US or

152
00:13:51,160 --> 00:13:52,160
something.

153
00:13:52,160 --> 00:13:54,160
That's awesome, virtual Donna.

154
00:13:54,160 --> 00:14:12,280
Yeah, well, I said, please stop because I can't use it.

155
00:14:12,280 --> 00:14:18,520
I'm 6'7", and when I sit cross-legged, my foot ends up hurting as the position, I'm

156
00:14:18,520 --> 00:14:24,400
in, my right foot is under my left cross-legged, what position should my left foot be in,

157
00:14:24,400 --> 00:14:28,400
so I can stop this from happening?

158
00:14:28,400 --> 00:14:39,040
Yeah, I tried to just put one leg under, one leg in front of the other with the heel touching

159
00:14:39,040 --> 00:14:45,640
the chin of the back leg, but it's still probably not going to be easy in the beginning,

160
00:14:45,640 --> 00:14:50,280
it just takes time to get used to it, and start, you can start with cushions under your

161
00:14:50,280 --> 00:14:56,320
knees and your legs, you can start sitting with your butt a little bit higher, it's just

162
00:14:56,320 --> 00:14:59,160
a matter of time, it's like anything it takes training.

163
00:14:59,160 --> 00:15:02,440
Is that what's called the Burmese position?

164
00:15:02,440 --> 00:15:04,840
Or is that different?

165
00:15:04,840 --> 00:15:09,840
Yeah, I think that's what it's called.

166
00:15:09,840 --> 00:15:28,240
I'm not sure why, but someone in Burma must have been the first, okay, so apparently you

167
00:15:28,240 --> 00:15:34,640
can't hear me on the feed, but as long as they can hear you, yeah, George said, can't

168
00:15:34,640 --> 00:15:38,240
hear me on the feed, but as long as they can hear the answer, I guess that's the...

169
00:15:38,240 --> 00:15:42,880
Here you know, that must have been, that's a bit that's outdated because I'm seeing it

170
00:15:42,880 --> 00:15:43,880
pick you up.

171
00:15:43,880 --> 00:15:47,440
Oh, okay, that was seven minutes ago.

172
00:15:47,440 --> 00:15:52,040
Bante, I tend to let my word slip and curse somewhat often, and from what I understand,

173
00:15:52,040 --> 00:15:54,680
this is not in line with right speech.

174
00:15:54,680 --> 00:15:59,640
Is it just a matter, a case of practice and being more mindful of when I speak, or is it

175
00:15:59,640 --> 00:16:01,480
not so much of a problem?

176
00:16:01,480 --> 00:16:09,200
I feel like it's not good to do.

177
00:16:09,200 --> 00:16:19,600
What cursing is in the big deal, it's not really wrong speech, not exactly, but it's usually

178
00:16:19,600 --> 00:16:23,560
associated with negative mind states.

179
00:16:23,560 --> 00:16:28,000
Now, in worst speeches, if you call someone names, harsh speech is like calling someone

180
00:16:28,000 --> 00:16:36,320
a dog, or a pig, or a monkey, or a goat, or you know, calling people nasty names, hurting

181
00:16:36,320 --> 00:16:39,400
other people with speech.

182
00:16:39,400 --> 00:16:46,040
If you just say, damn it, or like this Israeli monk who kept saying bullshit all the

183
00:16:46,040 --> 00:16:51,280
time, and I tried to explain to him, you know, that's probably not the best thing to say

184
00:16:51,280 --> 00:16:56,840
around Western people, about American people, because we, I don't know how it is in Israel,

185
00:16:56,840 --> 00:17:02,440
but that word means that it's a little bit to course, but it's not, that's not considered

186
00:17:02,440 --> 00:17:08,080
harsh speech, harsh speech is if someone says something, you say, oh, you're speaking,

187
00:17:08,080 --> 00:17:24,680
you know, all you ever speak is bullshit or something, that would be harsh speech.

188
00:17:24,680 --> 00:17:30,120
Or could up on questions, are there any announcements?

189
00:17:30,120 --> 00:17:41,160
No, no, we have this, no, I've already mentioned that, not for me, it's more on another

190
00:17:41,160 --> 00:18:10,840
demo, Panda, tonight, that's all for now.

191
00:18:12,160 --> 00:18:21,160
Well, hopefully that worked with the, uh, lives with the audio, I don't really know,

192
00:18:21,160 --> 00:18:25,160
but I'm using, I'm using, I'm using, I'm using these which are Bluetooth, and it's all

193
00:18:25,160 --> 00:18:26,160
nice stuff.

194
00:18:26,160 --> 00:18:32,160
So hopefully that worked anyway, thank you all for coming out, thanks Robin for your

195
00:18:32,160 --> 00:18:33,160
help.

196
00:18:33,160 --> 00:18:34,160
Thank you, Dante.

197
00:18:34,160 --> 00:18:35,160
Good night.

198
00:18:35,160 --> 00:18:42,160
Good night.

