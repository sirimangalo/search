1
00:00:00,000 --> 00:00:23,000
Good evening everyone, welcome to our evening dhamma group.

2
00:00:23,000 --> 00:00:30,000
And they went to teach meditation at McMaster.

3
00:00:30,000 --> 00:00:41,000
There was a conference and the conference on breaking down borders or something like that.

4
00:00:41,000 --> 00:00:44,000
Something about borders.

5
00:00:44,000 --> 00:00:53,000
Anyway, they wanted to have various workshops to bring people together and so they invited me to keep a workshop.

6
00:00:53,000 --> 00:00:59,000
We have two workshops on meditation.

7
00:00:59,000 --> 00:01:10,000
Teaching meditation is always an interesting thing.

8
00:01:10,000 --> 00:01:18,000
Every person is a universe in themselves.

9
00:01:18,000 --> 00:01:25,000
And each have our unique personalities.

10
00:01:25,000 --> 00:01:29,000
It was most interesting in each group had about four people.

11
00:01:29,000 --> 00:01:30,000
It was a small thing.

12
00:01:30,000 --> 00:01:34,000
It was teaching hundreds of people or anything.

13
00:01:34,000 --> 00:01:37,000
Four people in each group.

14
00:01:37,000 --> 00:01:43,000
It was curious when they asked them about how it went afterwards, what they thought of it.

15
00:01:43,000 --> 00:01:47,000
Each one of them had very different things to say about it.

16
00:01:47,000 --> 00:01:50,000
The same practice, the same teaching, the same room.

17
00:01:50,000 --> 00:01:55,000
And each individual had different things to say.

18
00:01:55,000 --> 00:01:57,000
Something rare happened.

19
00:01:57,000 --> 00:02:02,000
One of them actually said, right to me, right to me, I don't think this is for me.

20
00:02:02,000 --> 00:02:08,000
I think there are other techniques that I'd rather have more come through it.

21
00:02:08,000 --> 00:02:11,000
It didn't feel like it worked for me.

22
00:02:11,000 --> 00:02:12,000
That's rare.

23
00:02:12,000 --> 00:02:15,000
It's rare to find someone who says that.

24
00:02:15,000 --> 00:02:17,000
So I said, well, that's fine.

25
00:02:17,000 --> 00:02:20,000
If you want to practice other techniques, that's fine.

26
00:02:20,000 --> 00:02:24,000
But I would challenge that.

27
00:02:24,000 --> 00:02:31,000
Because meditation isn't supposed to be comfortable.

28
00:02:31,000 --> 00:02:36,000
It should be comfortable isn't a sign that it's working.

29
00:02:36,000 --> 00:02:38,000
It's meant to challenge you.

30
00:02:38,000 --> 00:02:53,000
It's meant to force you to actually look at your habits and your ordinary proclivities,

31
00:02:53,000 --> 00:02:58,000
your ordinary way of inclination, your ordinary inclination.

32
00:02:58,000 --> 00:03:03,000
And this is key.

33
00:03:03,000 --> 00:03:10,000
My meditation, there are so many ways we can evaluate our meditation.

34
00:03:10,000 --> 00:03:13,000
And not all of them are proper, right?

35
00:03:13,000 --> 00:03:19,000
If the meditation technique agrees with your views and your beliefs,

36
00:03:19,000 --> 00:03:23,000
that's not a sign that you should accept it.

37
00:03:23,000 --> 00:03:32,000
If it's comfortable, as I said, it's not a sign that you should accept it.

38
00:03:32,000 --> 00:03:41,000
If it brings you exceptional, extraordinary experiences,

39
00:03:41,000 --> 00:03:46,000
that's not a sign that it's proper meditation.

40
00:03:46,000 --> 00:03:53,000
There's a not criteria by which you should certainly in terms of insight meditation.

41
00:03:53,000 --> 00:04:01,000
These are not the criteria you should use to discern whether your meditation is succeeding.

42
00:04:01,000 --> 00:04:06,000
In fact, I think one of the criteria that you should use is whether it's challenging you.

43
00:04:06,000 --> 00:04:11,000
Is it questioning? Is it forcing you to question your beliefs and your opinions?

44
00:04:11,000 --> 00:04:17,000
Is it forcing you to deal with things you don't normally want to look at?

45
00:04:17,000 --> 00:04:20,000
Is it taking you out of your comfort zone?

46
00:04:20,000 --> 00:04:26,000
I think that's a very good criteria because a lot of meditation doesn't do that.

47
00:04:26,000 --> 00:04:33,000
It reaffirms, it pacifies, it placates, your defilements, including delusion,

48
00:04:33,000 --> 00:04:45,000
which is wrapped up in views and opinions.

49
00:04:45,000 --> 00:04:51,000
In what's true that meditation should bring peace, meditation being peaceful.

50
00:04:51,000 --> 00:04:56,000
It's not even a good criteria itself because

51
00:04:56,000 --> 00:05:03,000
meditations are there many ways we can look at meditation.

52
00:05:03,000 --> 00:05:06,000
They sort of understand it's not complicated.

53
00:05:06,000 --> 00:05:10,000
It's not something that should be hard to understand or even hard to discern

54
00:05:10,000 --> 00:05:14,000
whether you're practicing properly or whether you're getting something out of it.

55
00:05:14,000 --> 00:05:19,000
I think the big problem is we can't be overcomplicate

56
00:05:19,000 --> 00:05:30,000
and we tend to have selective memory where we forget the benefits.

57
00:05:30,000 --> 00:05:33,000
One moment we'll be getting good results and so we like the practice.

58
00:05:33,000 --> 00:05:39,000
The next moment we'll be getting good results and so we think the practice is problematic.

59
00:05:39,000 --> 00:05:42,000
Down is funny that way.

60
00:05:42,000 --> 00:05:47,000
It makes you forget the benefits.

61
00:05:47,000 --> 00:05:52,000
Your meditation is a simple procedure when you're actually meditating.

62
00:05:52,000 --> 00:05:55,000
It should be quite clear to you the benefit.

63
00:05:55,000 --> 00:05:58,000
The problem is when you stop and when you lose track,

64
00:05:58,000 --> 00:06:03,000
the importance of having a teacher is that they remind you.

65
00:06:03,000 --> 00:06:06,000
Even just being there when you come to see your teacher, you remember,

66
00:06:06,000 --> 00:06:10,000
oh, it's going to ask me about this and I wasn't mindful.

67
00:06:10,000 --> 00:06:18,000
It forces you to stay honest and to stay objective, to stay with the system,

68
00:06:18,000 --> 00:06:21,000
to stay mindful.

69
00:06:21,000 --> 00:06:25,000
A teacher who can call you out and remind you to be mindful.

70
00:06:25,000 --> 00:06:27,000
In many ways we can look at meditation.

71
00:06:27,000 --> 00:06:36,000
I remember once we went with Adjunctong.

72
00:06:36,000 --> 00:06:43,000
I can't remember why I think we were going to see a place, a forest monastery,

73
00:06:43,000 --> 00:06:49,000
a place that someone was going to donate to us to him as a meditation center.

74
00:06:49,000 --> 00:06:54,000
So we went to see that but then afterwards we went to see this park.

75
00:06:54,000 --> 00:06:58,000
The person who was donating the land was very rich.

76
00:06:58,000 --> 00:07:02,000
And they also had a garden that they wanted Adjunctong to see.

77
00:07:02,000 --> 00:07:08,000
I remember going with them to see this garden and it had all sorts of trees in it.

78
00:07:08,000 --> 00:07:14,000
All rare types of trees and incredible amount of money

79
00:07:14,000 --> 00:07:21,000
have gone into this huge forest full of exotic trees and flowers and meticulously groomed.

80
00:07:21,000 --> 00:07:23,000
It was quite a special experience.

81
00:07:23,000 --> 00:07:26,000
Adjunctong wasn't all that impressed.

82
00:07:26,000 --> 00:07:29,000
But he gave a talk afterwards.

83
00:07:29,000 --> 00:07:33,000
It's not that he was disdainful but he was unmoved by it.

84
00:07:33,000 --> 00:07:36,000
In fact he gave a talk afterwards and he said,

85
00:07:36,000 --> 00:07:39,000
oh he went to see this garden.

86
00:07:39,000 --> 00:07:48,000
And he said, but I think we should consider the inner garden.

87
00:07:48,000 --> 00:07:54,000
And so that's one very good way of relating to the meditation practice.

88
00:07:54,000 --> 00:08:01,000
It's our inner garden. It's the cultivation of who we are inside.

89
00:08:01,000 --> 00:08:08,000
We have so many different aspects of who we are.

90
00:08:08,000 --> 00:08:12,000
All these habits that we built up to become our personality.

91
00:08:12,000 --> 00:08:17,000
And we may never remove or just because we become enlightened

92
00:08:17,000 --> 00:08:22,000
or if we become enlightened doesn't mean we'll lose that personality.

93
00:08:22,000 --> 00:08:26,000
There are those aspects of who we are.

94
00:08:26,000 --> 00:08:29,000
What we do is we do some tending and some cultivating.

95
00:08:29,000 --> 00:08:38,000
Maybe even some planting of new seeds.

96
00:08:38,000 --> 00:08:44,000
But we engage in cultivating and nourishing the garden.

97
00:08:44,000 --> 00:08:49,000
So at the same time we're moving, cutting down and uprooting

98
00:08:49,000 --> 00:08:55,000
all the weeds, all the thorns,

99
00:08:55,000 --> 00:09:01,000
all the stuff we want to be free from.

100
00:09:01,000 --> 00:09:05,000
So it's quite a simple activity, right?

101
00:09:05,000 --> 00:09:08,000
Obviously tending a garden is not that complex.

102
00:09:08,000 --> 00:09:12,000
It's just about tending and caring and cultivating.

103
00:09:12,000 --> 00:09:14,000
It's a skill that you have to develop.

104
00:09:14,000 --> 00:09:19,000
And meditation is just a skill. It's a simple skill.

105
00:09:19,000 --> 00:09:21,000
It should be simple.

106
00:09:21,000 --> 00:09:26,000
So if you're doubting about your practice, about whether it's bringing results,

107
00:09:26,000 --> 00:09:29,000
and perhaps you're overcomplicating it.

108
00:09:29,000 --> 00:09:31,000
So meditation is simple. You're mindful.

109
00:09:31,000 --> 00:09:34,000
And you say to yourself, see.

110
00:09:34,000 --> 00:09:37,000
In the beginning, it's easy to be skeptical.

111
00:09:37,000 --> 00:09:39,000
What is this doing?

112
00:09:39,000 --> 00:09:44,000
Even a verse to it feels uncomfortable.

113
00:09:44,000 --> 00:09:48,000
It's not pleasant. It's not familiar.

114
00:09:48,000 --> 00:09:51,000
It's not easy, right?

115
00:09:51,000 --> 00:09:54,000
I always remember the days when I tried to learn tennis.

116
00:09:54,000 --> 00:09:58,000
I don't play tennis, but for some reason tennis I found incredibly difficult

117
00:09:58,000 --> 00:10:01,000
because they were teaching us the serve.

118
00:10:01,000 --> 00:10:06,000
I had to throw the ball up and then have some special way of wacking it.

119
00:10:06,000 --> 00:10:10,000
And I'll never forget how awful it was for me because I was horrible at it.

120
00:10:10,000 --> 00:10:13,000
I always think back to that.

121
00:10:13,000 --> 00:10:17,000
It doesn't mean that I couldn't learn tennis.

122
00:10:17,000 --> 00:10:23,000
It doesn't mean there's anything wrong with tennis.

123
00:10:23,000 --> 00:10:27,000
When you begin to practice something, it's not easy.

124
00:10:27,000 --> 00:10:33,000
But it's undeniable. It's undeniable about meditation, about mindfulness.

125
00:10:33,000 --> 00:10:38,000
When you are mindful, at the moment when you enter into that,

126
00:10:38,000 --> 00:10:42,000
just grasp the object as it is.

127
00:10:42,000 --> 00:10:47,000
And that's what's wonderful about teaching all these people is because they can taste that.

128
00:10:47,000 --> 00:10:50,000
They plant these seeds.

129
00:10:50,000 --> 00:10:55,000
We plant seeds in their garden, whether they'll cultivate it.

130
00:10:55,000 --> 00:10:57,000
The first thing I said to them when they walk in the room,

131
00:10:57,000 --> 00:11:00,000
I'm not sure if it's a good opener because it's not very pleasant.

132
00:11:00,000 --> 00:11:05,000
When I said, this isn't a magic switch that you can pull

133
00:11:05,000 --> 00:11:11,000
and you sit down and suddenly poof and wonderful things happen.

134
00:11:11,000 --> 00:11:14,000
Meditation is a training. It's something you have to work at.

135
00:11:14,000 --> 00:11:16,000
I don't think that's what people want to hear.

136
00:11:16,000 --> 00:11:18,000
They want to come in and do a meditation session and say,

137
00:11:18,000 --> 00:11:21,000
oh, that was nice. Go back to my life.

138
00:11:21,000 --> 00:11:25,000
It's not that useful, right?

139
00:11:25,000 --> 00:11:28,000
So it's about something you have to cultivate.

140
00:11:28,000 --> 00:11:34,000
But perhaps I should leave that till the end of the talk because

141
00:11:34,000 --> 00:11:39,000
they'll let them taste it first, right? Plant the seed.

142
00:11:39,000 --> 00:11:44,000
This is really important when we talk about teaching and spreading the dhamma.

143
00:11:44,000 --> 00:11:48,000
This question of whether it's worth it, whether it's beneficial,

144
00:11:48,000 --> 00:11:51,000
whether I'm even capable of teaching, right?

145
00:11:51,000 --> 00:11:55,000
What do I know? Not the Buddha.

146
00:11:55,000 --> 00:12:00,000
Maybe you've done a little bit of meditation yourself and then you talk to someone else

147
00:12:00,000 --> 00:12:03,000
and they ask you, they're wondering about anything.

148
00:12:03,000 --> 00:12:07,000
Should I explain to them the meditation practice?

149
00:12:07,000 --> 00:12:11,000
I mean, a wonderful thing we have now is we have this booklet and we have videos

150
00:12:11,000 --> 00:12:15,000
and we have a lot of recorded materials, stuff I've written,

151
00:12:15,000 --> 00:12:17,000
there's you can go to my housey scientist.

152
00:12:17,000 --> 00:12:20,000
So you can hand on or pass on other things.

153
00:12:20,000 --> 00:12:24,000
We shouldn't be afraid to try and plant the seed in people's minds.

154
00:12:24,000 --> 00:12:29,000
I mean, helping them cultivate it unless you're an expert gardener,

155
00:12:29,000 --> 00:12:33,000
or an expert tennis player, maybe you shouldn't.

156
00:12:33,000 --> 00:12:39,000
But you can explain the rudimentary fundamentals of the practice.

157
00:12:39,000 --> 00:12:43,000
And you can help plant that seed in their garden.

158
00:12:43,000 --> 00:12:48,000
Because it's just a moment if you just get that taste of our suddenly

159
00:12:48,000 --> 00:12:54,000
your mindful, suddenly you're here and now, suddenly you're present.

160
00:12:54,000 --> 00:12:59,000
It's undeniable and incontrovertible.

161
00:12:59,000 --> 00:13:01,000
You'll have no doubt in your mind.

162
00:13:01,000 --> 00:13:06,000
In that moment, you'll see it's so easy to doubt afterwards and forget.

163
00:13:06,000 --> 00:13:08,000
But remind yourself, I had that moment.

164
00:13:08,000 --> 00:13:10,000
There's no question.

165
00:13:10,000 --> 00:13:12,000
And bring yourself back to that moment.

166
00:13:12,000 --> 00:13:16,000
Because the wonderful thing about mindfulness is it's not an occasional thing.

167
00:13:16,000 --> 00:13:18,000
It's an every moment thing.

168
00:13:18,000 --> 00:13:19,000
Any moment.

169
00:13:19,000 --> 00:13:21,000
Now, OK, I'm going to be mindful again.

170
00:13:21,000 --> 00:13:22,000
Does this work?

171
00:13:22,000 --> 00:13:24,000
Oh, yes, that worked.

172
00:13:24,000 --> 00:13:27,000
You can see every moment.

173
00:13:27,000 --> 00:13:28,000
We overcomplicate it.

174
00:13:28,000 --> 00:13:34,000
And we think about, oh, I've been practicing for days or I've got days left to practice.

175
00:13:34,000 --> 00:13:36,000
Am I really getting anything out of this?

176
00:13:36,000 --> 00:13:38,000
It's not how you should look at it.

177
00:13:38,000 --> 00:13:41,000
That's harmful because it's dealing an abstract.

178
00:13:41,000 --> 00:13:44,000
So it'll never work.

179
00:13:44,000 --> 00:13:50,000
Because you can't expect things to always be a specific way.

180
00:13:50,000 --> 00:13:53,000
And you can't always be mindful.

181
00:13:53,000 --> 00:14:02,000
But when you are mindful, in that moment, it's the most profound,

182
00:14:02,000 --> 00:14:06,000
the most powerful of all acts.

183
00:14:06,000 --> 00:14:09,000
It's just that moment.

184
00:14:09,000 --> 00:14:18,000
You're liking it to rainfall.

185
00:14:18,000 --> 00:14:22,000
Mindfulness is like those raindrops.

186
00:14:22,000 --> 00:14:33,000
But I think that's a bit misleading, but

187
00:14:33,000 --> 00:14:42,000
it's an underestimation of the power of what meditation is water in.

188
00:14:42,000 --> 00:14:46,000
It's this incredible pure substance.

189
00:14:46,000 --> 00:14:49,000
And even just a drop of it.

190
00:14:49,000 --> 00:14:52,000
It's like planting a seed in your garden.

191
00:14:52,000 --> 00:14:59,000
That may one day grow into a beautiful flower tree.

192
00:14:59,000 --> 00:15:04,000
It may grow into a tree of enlightenment really.

193
00:15:04,000 --> 00:15:10,000
So we focus very much on moments.

194
00:15:10,000 --> 00:15:16,000
Anyway, I just wanted to sort of contemplate this idea of

195
00:15:16,000 --> 00:15:19,000
spreading the dumber to ourselves.

196
00:15:19,000 --> 00:15:22,000
I remember talking to a monk once,

197
00:15:22,000 --> 00:15:25,000
and I was talking about spreading the dumber.

198
00:15:25,000 --> 00:15:29,000
And he kept saying to me, he said, spread it to yourself first.

199
00:15:29,000 --> 00:15:32,000
I don't think he had a very high estimation of me.

200
00:15:32,000 --> 00:15:38,000
And with good reason, we have to practice for ourselves.

201
00:15:38,000 --> 00:15:40,000
And we spread it to ourselves.

202
00:15:40,000 --> 00:15:42,000
We plant things in our garden.

203
00:15:42,000 --> 00:15:47,000
We cultivate wholesome qualities in our minds.

204
00:15:47,000 --> 00:15:49,000
Or whether we help others.

205
00:15:49,000 --> 00:15:54,000
We don't have to be an expert gardener to provide people with seeds.

206
00:15:54,000 --> 00:15:58,000
And with meditation, you provide these moments.

207
00:15:58,000 --> 00:16:00,000
Help someone to see.

208
00:16:00,000 --> 00:16:03,000
Help someone to have that moment of mindfulness.

209
00:16:03,000 --> 00:16:09,000
That moment probably won't trigger an experience of enlightenment.

210
00:16:09,000 --> 00:16:11,000
But it will plant a seed.

211
00:16:11,000 --> 00:16:14,000
It will plant a seed in their garden.

212
00:16:14,000 --> 00:16:25,000
So we talk about seeds.

213
00:16:25,000 --> 00:16:30,000
Seeds in garden.

214
00:16:30,000 --> 00:16:31,000
So there you go.

215
00:16:31,000 --> 00:16:34,000
There's a little bit of dumber for tonight.

216
00:16:34,000 --> 00:16:36,000
Thank you all for tuning in.

217
00:16:36,000 --> 00:16:37,000
Have a good night.

