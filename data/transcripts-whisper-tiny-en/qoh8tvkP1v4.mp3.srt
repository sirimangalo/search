1
00:00:00,000 --> 00:00:02,000
.

2
00:00:04,000 --> 00:00:06,000
.

3
00:00:08,000 --> 00:00:10,000
.

4
00:00:10,000 --> 00:00:12,000
.

5
00:00:12,000 --> 00:00:14,000
.

6
00:00:14,000 --> 00:00:16,000
.

7
00:00:16,000 --> 00:00:18,000
.

8
00:00:18,000 --> 00:00:20,000
.

9
00:00:20,000 --> 00:00:22,000
..

10
00:00:22,000 --> 00:00:24,000
..

11
00:00:24,000 --> 00:00:24,000
..

12
00:00:24,000 --> 00:00:26,000
..

13
00:00:26,000 --> 00:00:28,000
..

14
00:00:28,000 --> 00:00:32,000
..

15
00:00:32,000 --> 00:00:32,000
..

16
00:00:32,000 --> 00:00:32,000
..

17
00:00:32,000 --> 00:00:32,000
..

18
00:00:32,000 --> 00:00:32,000
..

19
00:00:32,000 --> 00:00:32,000
..

20
00:00:32,000 --> 00:00:32,000
..

21
00:00:32,000 --> 00:00:32,000
..

22
00:00:32,000 --> 00:00:34,000
..

23
00:00:34,000 --> 00:00:32,000
..

24
00:00:32,000 --> 00:00:34,000
..

25
00:00:34,000 --> 00:00:32,000
..

26
00:00:32,000 --> 00:00:34,000
..

27
00:00:34,000 --> 00:00:32,000
..

28
00:00:32,000 --> 00:00:32,000
..

29
00:00:32,000 --> 00:00:32,000
..

30
00:00:32,000 --> 00:00:32,000
..

31
00:00:32,000 --> 00:00:38,000
..

32
00:00:32,000 --> 00:00:32,000
...

33
00:00:32,000 --> 00:00:32,000
..

34
00:00:32,000 --> 00:00:32,000
..

35
00:00:32,000 --> 00:00:34,000
...

36
00:00:34,000 --> 00:00:32,000
..

37
00:00:32,000 --> 00:00:32,000
..

38
00:00:32,000 --> 00:00:32,000
..

39
00:00:32,000 --> 00:00:32,000
..

40
00:00:32,000 --> 00:00:32,000
..

41
00:00:32,000 --> 00:00:32,500
..

42
00:00:32,500 --> 00:00:34,000
– Good evening everyone...

43
00:00:34,000 --> 00:00:34,000
..

44
00:00:34,000 --> 00:00:34,000
..

45
00:00:34,000 --> 00:00:34,000
..

46
00:00:34,000 --> 00:00:35,000
..

47
00:00:34,000 --> 00:00:35,000
– Welcome to our live broadcast..

48
00:00:35,000 --> 00:00:36,000
..

49
00:00:37,000 --> 00:00:40,000
..

50
00:00:34,000 --> 00:00:37,000
….

51
00:00:37,000 --> 00:00:37,000
..

52
00:00:37,000 --> 00:00:40,000
..

53
00:00:40,000 --> 00:00:41,000
..

54
00:00:41,000 --> 00:00:41,000
...

55
00:00:41,000 --> 00:00:42,000
..

56
00:00:42,000 --> 00:00:42,000
..

57
00:00:42,000 --> 00:00:43,000
..

58
00:00:43,000 --> 00:00:44,000
..

59
00:00:44,000 --> 00:00:45,000
..

60
00:00:45,000 --> 00:00:42,000
..

61
00:00:42,000 --> 00:00:46,000
..

62
00:00:46,000 --> 00:00:47,000
..

63
00:00:47,000 --> 00:00:48,000
..

64
00:00:48,000 --> 00:00:49,000
..

65
00:00:49,000 --> 00:00:50,000
..

66
00:00:50,000 --> 00:00:55,560
and we got a piece club as well, so I just came from our piecewalk on the

67
00:00:55,560 --> 00:01:06,240
international day of peace. So it's been a long day and I'm concerned that

68
00:01:06,240 --> 00:01:12,220
might affect my ability to answer questions and teach

69
00:01:12,220 --> 00:01:20,220
down less of apologies. I'm going to try to keep up this daily broadcast, but it may

70
00:01:20,220 --> 00:01:33,140
not always be as fulfilling satisfying as one language. So tonight we're

71
00:01:33,140 --> 00:01:44,340
looking at a good tourniquy of book of fours, so to one set 177, Raoula Sutta.

72
00:01:54,940 --> 00:02:00,580
Why I stopped on this one is just so that we could take a moment to reflect on

73
00:02:00,580 --> 00:02:12,740
the four elements. So what the Buddha says in this, first he defines, he's

74
00:02:12,740 --> 00:02:18,460
defining reality or a part of reality, the physical aspect of reality. I was

75
00:02:18,460 --> 00:02:27,700
having four parts and those four parts are split into two as being internal and

76
00:02:27,700 --> 00:02:34,420
external and those four parts, of course, of the element. So all the physical

77
00:02:34,420 --> 00:02:45,420
reality is summarized under these headings. It's either internal or external

78
00:02:45,420 --> 00:02:53,540
and it's made up of the four elements, every physical, every aspect of

79
00:02:53,540 --> 00:03:02,420
physical reality. Now it's important to remember and keep in mind the paradigm

80
00:03:02,420 --> 00:03:09,340
under which this functions, functions under the paradigm of experience. So

81
00:03:09,340 --> 00:03:18,460
reality is defined as moments of experience, contact between the physical

82
00:03:18,460 --> 00:03:32,860
and the mental. And as for the physical it's made up of the four elements, that's

83
00:03:32,860 --> 00:03:37,660
the physical aspect of experience. It says nothing about particles, subatomic

84
00:03:37,660 --> 00:03:47,380
particles, quarks, strings, quantum fields, so only referring to what can be

85
00:03:47,380 --> 00:03:56,580
observed directly. And what can be observed directly is earth, air, water, fire,

86
00:03:56,580 --> 00:04:06,220
that's it. And so when we learned about and in grade nine science we learned

87
00:04:06,220 --> 00:04:12,740
about the four elements and we learned that that was the primitive way of

88
00:04:12,740 --> 00:04:17,100
understanding matter before we actually knew what matter was. That's

89
00:04:17,100 --> 00:04:22,620
funny because we don't consider it primitive at all. We consider it real and we

90
00:04:22,620 --> 00:04:31,180
consider that physics has actually gone off-script learning about things

91
00:04:31,180 --> 00:04:38,700
that are you can argue practically useful, terms of building bombs and that

92
00:04:38,700 --> 00:04:47,860
kind of thing, splitting atoms, but not ultimately useful in terms of relating

93
00:04:47,860 --> 00:04:54,220
to ultimate reality. That's what these four elements do. The four elements

94
00:04:54,220 --> 00:05:03,780
are useful, ultimate in ultimate science because they bring us closer to the

95
00:05:03,780 --> 00:05:18,620
truth and clear up our misunderstandings and our useless improper unwholesome

96
00:05:18,620 --> 00:05:29,180
unproductive problematic, dangerous reactions to reality. We want to see reality

97
00:05:29,180 --> 00:05:39,860
for what it is, we find peace in regards to reality no longer fighting, no

98
00:05:39,860 --> 00:05:54,100
longer chasing or chasing away the objects of our experience. And so this is

99
00:05:54,100 --> 00:06:02,180
important framework to remember defining matter in terms of the four

100
00:06:02,180 --> 00:06:10,020
elements. Shifts are our point of view away from concepts like body and hand and

101
00:06:10,020 --> 00:06:27,660
foot and face and man and woman and so on and so on to experience. We have the

102
00:06:27,660 --> 00:06:40,700
earth element which is hardness and feel something hard, softness, feel something is soft.

103
00:06:40,700 --> 00:06:48,420
The water element is cohesion when you have this something sticky or not sticky.

104
00:06:48,420 --> 00:07:00,660
The fire element, you have heat and you have cold and the air element, you have

105
00:07:00,660 --> 00:07:09,300
tension and non-tension or fluidity and that's all you have. It makes up how we

106
00:07:09,300 --> 00:07:19,300
experience the physical realm and physical aspect of reality. And that's it, that's all.

107
00:07:19,300 --> 00:07:25,580
And so what the Buddha is saying in the suta which is of course the whole point is to see

108
00:07:25,580 --> 00:07:55,180
it, this should be seen correctly with right wisdom, you have tahabu, tahabu, tahabu,

109
00:07:55,180 --> 00:08:06,380
that, naitangmama is not mine, naisohamasumi, I am not that, nahamisohata, this is not

110
00:08:06,380 --> 00:08:21,700
myself. This is not mine, this I am not, this is not myself. Those three are the description

111
00:08:21,700 --> 00:08:37,300
The description of the manifestation of the three types of clinging, you cling through views.

112
00:08:37,300 --> 00:08:44,140
This is myself, you cling through conceit, I am this, and you cling through craving.

113
00:08:44,140 --> 00:08:45,540
This is mine.

114
00:08:45,540 --> 00:08:58,180
So these three things are giving up views, conceit, and craving, the three causes of clinging.

115
00:08:58,180 --> 00:09:15,380
And once you see this clearly, that actually, all these are not worth clinging to, they are not me, they are not mine, they are not mine, they are not mine.

116
00:09:15,380 --> 00:09:27,780
And you see that clearly, everyone in this way, having seen a poor dad who you are, but to be dad who you are,

117
00:09:27,780 --> 00:09:45,360
nibindati, when it becomes disenchanted, you can see that there is no control, there is no ownership, there is no reckoning with these things, they come and go and they change in ways that are

118
00:09:45,360 --> 00:09:52,360
almost completely out of our hands.

119
00:09:52,360 --> 00:10:04,360
When you see that everybody has started to get disenchanted, you lose all this lust and desire for them, because you see they are just

120
00:10:04,360 --> 00:10:14,860
taking you on a wild goose chase, looking for the wild goose of satisfaction which will never find.

121
00:10:14,860 --> 00:10:21,860
You end up chasing your tail.

122
00:10:21,860 --> 00:10:34,860
So when it becomes disenchanted, loses interest, that it can often be surprised by how the things that you stay interest them no longer interest them.

123
00:10:34,860 --> 00:10:43,860
They are just no longer give rise to the desire for or aversion towards anything.

124
00:10:43,860 --> 00:11:01,860
You just don't see the point, it's not out of, it's not because they are ignorant or uninterested.

125
00:11:01,860 --> 00:11:07,860
It's out of wisdom, because those things are not actually pleasant, those things actually don't.

126
00:11:07,860 --> 00:11:16,860
There's no resolution that can be gained from chasing them or chasing them away.

127
00:11:16,860 --> 00:11:23,860
They are unsatisfying.

128
00:11:23,860 --> 00:11:41,860
But the way that we are Jitang Viraji, when it becomes dispassionate, the mind becomes when it hatches the mind from it.

129
00:11:41,860 --> 00:11:56,860
The mind that is clinging from that clinging, that obsession.

130
00:11:56,860 --> 00:12:04,860
That's the suit for tonight, that's as far as I'm going to go, because I think tonight it's going to be a short session.

131
00:12:04,860 --> 00:12:11,860
We had a piecewalk, and then we went on this. We had a piecewalk, and then we had a bonfire.

132
00:12:11,860 --> 00:12:20,860
We sat around talking about all sorts of projects that were interested in in terms of cultivating peace.

133
00:12:20,860 --> 00:12:35,860
It's really a good group. I'm not interested in mental health meditation, so I hope to get into some things in that regard.

134
00:12:35,860 --> 00:12:44,860
Don't further ado, let's look at some questions, some questions, not all these questions.

135
00:12:44,860 --> 00:12:51,860
What can I do?

136
00:12:51,860 --> 00:13:01,860
Let's just want to go to the chat.

137
00:13:01,860 --> 00:13:10,860
What can I do when in meditation, without a word for a concentration, there are places and things that I cannot find now words for?

138
00:13:10,860 --> 00:13:15,860
Well, try me. You'll find a word for pretty much anything. Feeling or knowing is often a good one.

139
00:13:15,860 --> 00:13:25,860
Calm, quiet is one that people stumble over or forget about quite often.

140
00:13:25,860 --> 00:13:31,860
Which is knowing or feeling often works as well.

141
00:13:31,860 --> 00:13:38,860
Many stories we see many individuals wishing for something.

142
00:13:38,860 --> 00:13:46,860
Should one practice like that? Are these the ways of the wise?

143
00:13:46,860 --> 00:13:49,860
What should be the intentional doing good deeds if the goal is to become an artist?

144
00:13:49,860 --> 00:13:55,860
The goal is to become an artist, and you should do good deeds. Thinking this will help me become an hour hunt.

145
00:13:55,860 --> 00:14:00,860
The goal is to go to heaven, you should think this will help me get to heaven if it was to be rich.

146
00:14:00,860 --> 00:14:05,860
You should do things with the intention that they should make you rich.

147
00:14:05,860 --> 00:14:15,860
Yeah, it's an hour hunt. And that's so much concern with me. I get this without it.

148
00:14:15,860 --> 00:14:21,860
My practice of meditation, I intend to feel my defilements. Good for you.

149
00:14:21,860 --> 00:14:28,860
But I can't help myself to feel a superiority in regards to people who are not aware.

150
00:14:28,860 --> 00:14:34,860
Sometimes I catch myself thinking that the hour hunt goal is just to pursue it based on ego.

151
00:14:34,860 --> 00:14:38,860
How can I reduce? Well, no, no. You see, when you meditate, all your defilements come up.

152
00:14:38,860 --> 00:14:42,860
Just like when you do anything, actually. The meditation is no different.

153
00:14:42,860 --> 00:14:47,860
So when you meditate, you're going to feel egotistical about things like your meditation.

154
00:14:47,860 --> 00:14:52,860
It's not a problem. It is a problem, but it's not unexpected.

155
00:14:52,860 --> 00:14:57,860
But the fact that you see the egotistical is great. I mean, that's what's unique about meditation.

156
00:14:57,860 --> 00:15:18,860
The meditation is you're in a unique position to understand these experiences and to see them for what they are and to see how disgusting and unpleasant and desirable they are slowly get rid of them.

157
00:15:18,860 --> 00:15:28,860
Well, do the enlightened mind have a need to do formal meditation practice? Assuming they're perfect mindfulness.

158
00:15:28,860 --> 00:15:36,860
So the enlightened mind doesn't have a need to do anything, but they do do things like meditate, just because they keep doing things.

159
00:15:36,860 --> 00:15:52,860
But having no need and no desire, much is done away with and when they pass away, there's no more. No more rising.

160
00:15:52,860 --> 00:16:02,860
What constitutes a skillful question? I think it's relative, I suppose.

161
00:16:02,860 --> 00:16:08,860
What makes something skillful? And you're good at asking a question.

162
00:16:08,860 --> 00:16:20,860
And if you're just asking, what is it? What makes a good question? Well, a good question is one that allows you to get an answer that helps you.

163
00:16:20,860 --> 00:16:36,860
I'm seeing it closer to enlightenment. That's a good point. Why are you asking the question? Are you asking a question? When someone asks a question? Are they asking it? Because they're just curious. Well, it's not a very skillful thing.

164
00:16:36,860 --> 00:16:49,860
They're asking it because it's something that they believe is necessary for them to know in order to become a enlightened mind. That seems a good question. Even if they're wrong, maybe the answer is no.

165
00:16:49,860 --> 00:17:04,860
You don't need to know that, but it's good that they ask. If they sincerely believe, but sometimes we just ask questions because we're lazy or if that's what curious, because whatever, those are not so skillful.

166
00:17:04,860 --> 00:17:14,860
I'm doing walking practice and becoming steady. That seems to happen when I'm being mindful and walking practice.

167
00:17:14,860 --> 00:17:22,860
Just be my mind trying to pull away from my mindfulness. My drifts, if I'm walking steady.

168
00:17:22,860 --> 00:17:35,860
Well, it's, yes, in the beginning, I mean, it's awkward. You're doing something that is maybe like you could think of it as trying to chew gum and juggle at the same time or something.

169
00:17:35,860 --> 00:17:44,860
I'm doing two things at once. It's something that your mind is struggling with. When your mind struggles, it stumbles.

170
00:17:44,860 --> 00:17:59,860
Therefore, the body also stumbles. But eventually, it becomes more, it becomes more comfortable with it.

171
00:17:59,860 --> 00:18:09,860
I wouldn't worry about it. I guess it can come even for an advanced meditator as well. It has to do with the brain being limited and imperfect.

172
00:18:09,860 --> 00:18:20,860
It's why stumbling is an interesting experience.

173
00:18:20,860 --> 00:18:30,860
On last night's broadcast, there was some kind of flying bug in the room. I caught the bug in my hand and I put it out the window.

174
00:18:30,860 --> 00:18:39,860
I don't kill. I certainly wouldn't, well, I'm out of it. I don't kill my attention.

175
00:18:39,860 --> 00:18:45,860
That's something I can tell you for sure I don't do it.

176
00:18:45,860 --> 00:18:55,860
That should be the speed of walking meditation compared to regular walking.

177
00:18:55,860 --> 00:18:59,860
While regular walking, if you start being mindful, you realize that you're actually going somewhat quickly.

178
00:18:59,860 --> 00:19:09,860
So it's probably a bit slower, but it shouldn't feel slow. If it feels slow, you're going to slow. If it feels faster, you're going to pass.

179
00:19:09,860 --> 00:19:18,860
Mind you, an ordinary person, when you're just starting meditating, you're probably geared to doing things quickly.

180
00:19:18,860 --> 00:19:31,860
So you have to maybe spend some time being fairly careful, start a bit slow, and eventually get a sense of a natural calm rhythm.

181
00:19:31,860 --> 00:19:41,860
And then, of course, change from time to time. It shouldn't be fast for me to be slow. Imagine talking to yourself, not too fast, not too slow.

182
00:19:41,860 --> 00:19:52,860
How long should we know? How long should we not standing? Where in the body do we not standing?

183
00:19:52,860 --> 00:20:01,860
So, up to you, you can actually not standing for hours if you want. I think this was the one monk who did 12 hours of standing meditation.

184
00:20:01,860 --> 00:20:08,860
I wouldn't recommend going back to the rise of falling. It's probably a better object.

185
00:20:08,860 --> 00:20:13,860
When we do walking meditation, we know that three times standing. Standing. Standing.

186
00:20:13,860 --> 00:20:23,860
Turn. Or walk. Where in the body do you not standing? It's just a general sense of the feelings in the body. It doesn't matter. Sometimes it's different spots.

187
00:20:23,860 --> 00:20:35,860
Just being aware that's the feeling of standing. Are you feeling better? Better than what?

188
00:20:35,860 --> 00:20:44,860
I've been having burping, so I guess that's some kind of suffering. It's not that unpleasant. I got medication today.

189
00:20:44,860 --> 00:20:51,860
That prevents acid production in the body. They want to test and make sure that that's all it is.

190
00:20:51,860 --> 00:20:58,860
So, for two weeks, I get to test. It's actually, I think, a little bit better today after taking that medicine.

191
00:20:58,860 --> 00:21:05,860
If you please give an example of experiences associated with air element in the earth element.

192
00:21:05,860 --> 00:21:10,860
The air element, if you feel in the stomach, the tension when you breathe in, and the release when you breathe out,

193
00:21:10,860 --> 00:21:17,860
that's an example of the air element. Or when you sit up straight, the pressure you feel in your back, that's the air element of the pressure.

194
00:21:17,860 --> 00:21:27,860
Earth element is when you step on the on a hard floor and it feels hard. That's the earth element. When you step on a flush carpet, it feels soft. That's also the earth element.

195
00:21:27,860 --> 00:21:34,860
I don't answer questions about my practice or anybody else's practice.

196
00:21:34,860 --> 00:21:45,860
Sorry. And that's all. Thank you for being going easy on me or maybe I just would spend two of those questions, but that's it.

197
00:21:45,860 --> 00:21:55,860
Those are the questions for tonight. Apologies for not being a hundred percent, you know, if I can just do one thing,

198
00:21:55,860 --> 00:22:00,860
but I find myself, there's an idea to start a chaplain.

199
00:22:00,860 --> 00:22:05,860
Actually, there's a new chaplain at McMaster, an ecumenical chaplain.

200
00:22:05,860 --> 00:22:12,860
Seems like a nice guy. And he wants to bring non-Christian religions to the table.

201
00:22:12,860 --> 00:22:22,860
Not sure how sincere he is, but he seems nice. Nice and perhaps a Christian way, I'm not sure yet, but we'll have to see.

202
00:22:22,860 --> 00:22:28,860
If he's genuinely nice, then it would be nice. I've just been to say I'm involved with different things.

203
00:22:28,860 --> 00:22:34,860
Peace, mostly, and Buddhism. But good stuff.

204
00:22:34,860 --> 00:22:51,860
Trying to get involved with the local community and sort of align myself in ways that allow for the cultivation of wholesome interactions and groups and communities.

205
00:22:51,860 --> 00:22:59,860
And it allows me to, you know, there's some sense of experimenting from my point of view.

206
00:22:59,860 --> 00:23:10,860
Experimenting and reaching out and dialoguing with people who might be interested in what I do, or not even just that,

207
00:23:10,860 --> 00:23:19,860
who's not about that, who's not even who dialoguing with people in an effort to cultivate peace.

208
00:23:19,860 --> 00:23:25,860
And maybe not even an effort, just dialoguing with people in ways that are peaceful.

209
00:23:25,860 --> 00:23:39,860
You know, when dealing with the people you have, I've made a decision to live here in Canada, so doing what comes naturally, I should think, in terms of cultivating peace and working together.

210
00:23:39,860 --> 00:23:47,860
We complain about society, and we complain about the world, but it's all changeable. It's all malleable.

211
00:23:47,860 --> 00:23:54,860
All it takes is doing what needs to be done. You complain about something. It's changeable.

212
00:23:54,860 --> 00:24:03,860
Everything is. Do you have the patience? Do you have the purity of mind to see it through?

213
00:24:03,860 --> 00:24:05,860
Doing goodness is always worth it.

214
00:24:05,860 --> 00:24:28,860
Mom, because we don't be afraid of goodness. Apologies for perhaps in the near future, we're doing some of my effort put into the online community.

215
00:24:28,860 --> 00:24:35,860
And I only say that because it's actually the most important part of what I do.

216
00:24:35,860 --> 00:24:44,860
So I'm not suggesting that I'm going to give it up, but it may no longer be 100%. Hopefully, it'll still be 80% or 90%.

217
00:24:44,860 --> 00:25:00,860
Goodnight, everyone. See you all again soon.

