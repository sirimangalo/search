1
00:00:00,000 --> 00:00:18,200
Good evening, everyone, broadcasting live May 24th.

2
00:00:18,200 --> 00:00:28,640
Tonight's quote is an answer to an often voiced criticism of Buddhism and meditation practice

3
00:00:28,640 --> 00:00:38,120
in general, they call meditation navel gazing, where you just obsess with yourself, navel

4
00:00:38,120 --> 00:00:47,880
gazing as though it's a useless or potentially narcissistic, auto-erotic, that's what the

5
00:00:47,880 --> 00:00:57,960
pope called it, one of the previous popes, yeah, meditation gets a bad rap, and we think of

6
00:00:57,960 --> 00:01:06,480
it as an escape, running away from your problems of vacation, something you do at a spa

7
00:01:06,480 --> 00:01:22,520
or something, something like yoga, not to malign or to speak badly of yoga, but I don't

8
00:01:22,520 --> 00:01:28,640
think meditation or meditation that we do as much like yoga, even though people often equate

9
00:01:28,640 --> 00:01:36,280
to two or compare the two, but nonetheless, not speaking about yoga, meditation Buddhist

10
00:01:36,280 --> 00:01:48,120
meditations, most sincerely, is not an escape or a vacation, right, my meditator's

11
00:01:48,120 --> 00:01:54,640
nodding, it's a hard work, it's training, anyone who's coming through this course can tell

12
00:01:54,640 --> 00:02:02,000
you, it's not more than you usually signed up for, usually not what we had expected coming

13
00:02:02,000 --> 00:02:07,600
in, and that's good, it's not, if it were what you expected, then it wouldn't be working,

14
00:02:07,600 --> 00:02:12,040
and that's an important point, because it's in light, we're talking about in light, and

15
00:02:12,040 --> 00:02:16,880
we're talking about waking up, and we're waking up, we mean waking up to truths that we

16
00:02:16,880 --> 00:02:22,960
didn't know, we didn't know, how could it not surprise you, how could it not shock

17
00:02:22,960 --> 00:02:34,080
you, how could it not challenge you, if it didn't, it couldn't really say that it was

18
00:02:34,080 --> 00:02:41,240
something new or something, something that would change you, something that would bring

19
00:02:41,240 --> 00:02:46,400
about positive change, or negative change could be similar, it's not to say that the

20
00:02:46,400 --> 00:02:54,000
change or the hard work is, that all hard work is necessarily positive, the meditation

21
00:02:54,000 --> 00:03:04,440
is hard work, nonetheless, the criticism being leveled here is that he makes a comparison

22
00:03:04,440 --> 00:03:13,560
as Brahmin does, he says they perform sacrifices to God, I guess, and so that benefits

23
00:03:13,560 --> 00:03:18,400
many people, he's talking about general religious practice, they're putting aside this Brahmin's

24
00:03:18,400 --> 00:03:24,520
really ridiculous ideas of what is beneficial, like they would kill, they would slaughter

25
00:03:24,520 --> 00:03:32,120
animals, or they would sacrifice butter to the fire, a lot of silly things, and say

26
00:03:32,120 --> 00:03:42,160
that was wholesome, but let's look at wholesome activity, like people who are, work for

27
00:03:42,160 --> 00:03:49,600
social justice, or people who run charities, soup kitchens, or teachers, these kind of

28
00:03:49,600 --> 00:03:56,720
things, people who helped the world, right, and they say, well that's true goodness,

29
00:03:56,720 --> 00:04:03,000
what is this meditation, and he says the meditation, okay, we can accept that it helps

30
00:04:03,000 --> 00:04:10,600
you, but that's all it does, it's helped you, it's only good for yourself, I say that

31
00:04:10,600 --> 00:04:17,200
it's such a person is practicing something that benefits only himself, which to anyone

32
00:04:17,200 --> 00:04:25,800
who's put sincere, serious thought into the matter, certainly must sound ridiculous,

33
00:04:25,800 --> 00:04:31,520
but for people who are unsure who are new to spirituality, it sounds kind of convincing,

34
00:04:31,520 --> 00:04:39,680
yeah, why am I wasting my time in this meditation, doing things that only benefit myself,

35
00:04:39,680 --> 00:04:45,560
and agreed that there are meditations that I would say, for the most part, only benefit

36
00:04:45,560 --> 00:04:51,200
yourself, if you enter into a trance of bliss, the peace will state, that would be really

37
00:04:51,200 --> 00:04:56,520
only benefiting yourself, and in the end not benefiting yourself substantially anyway,

38
00:04:56,520 --> 00:05:03,400
because it's temporary, but this is an important point, because it helps clear up this

39
00:05:03,400 --> 00:05:09,880
misunderstanding, this misconception, this prejudice, we have preconception that we have

40
00:05:09,880 --> 00:05:16,280
that meditation should be pleasant, the meditation should be enjoyable, it can be, of

41
00:05:16,280 --> 00:05:20,440
course, even this meditation can be at times enjoyable, that's not what it's supposed to

42
00:05:20,440 --> 00:05:29,880
be, it's actually supposed to better you, make you, you could say a better person, but

43
00:05:29,880 --> 00:05:37,760
more technically, just to make you more pure, more clear, more mindful, more wise, more

44
00:05:37,760 --> 00:05:44,720
good, and if you hear these things, and then you ask yourself, is this not beneficial

45
00:05:44,720 --> 00:05:51,360
to other people, or you could say, well, how is that beneficial to other people?

46
00:05:51,360 --> 00:05:58,880
And so we can take a look at those people who try to help the world, right, for social

47
00:05:58,880 --> 00:06:08,120
justice, or all these things I mentioned, and they're not all this, they're not all equal,

48
00:06:08,120 --> 00:06:16,960
right, they're not equally successful, many of them burn out, there's a high, in the environmental

49
00:06:16,960 --> 00:06:22,000
movement, which my father was very much involved in, he recently told me, I think there's

50
00:06:22,000 --> 00:06:28,400
a very high burnout, right, and people who are into social justice will tell you that

51
00:06:28,400 --> 00:06:33,840
they flame bright and burn out quick, and then they're on to something else that the

52
00:06:33,840 --> 00:06:39,880
passion doesn't last, it's because they're not trained, it's because they don't have

53
00:06:39,880 --> 00:06:47,120
this ability, this power, and you don't really realize the power and the strength and

54
00:06:47,120 --> 00:06:52,280
clarity of mind that comes from meditation until you actually do it, that's why I say

55
00:06:52,280 --> 00:06:58,360
it surprises you, surprises us in how challenging it is, but it also surprises us

56
00:06:58,360 --> 00:07:04,840
in how deep it goes, and how fundamentally it changes us, it strengthens us, straightens

57
00:07:04,840 --> 00:07:10,000
us out, you come out of this feeling all crooked and like you're all bent out of shape

58
00:07:10,000 --> 00:07:18,360
and that you've just been wrenched back into more or less straight state, that's how

59
00:07:18,360 --> 00:07:24,120
it feels, it feels like you're untying knots, so it should feel if you're doing it properly,

60
00:07:24,120 --> 00:07:30,520
you're untying knots, like you're working out kinks, like you're straightening out, the

61
00:07:30,520 --> 00:07:38,200
crookedness in your mind, mind is all bent out of shape, men bent out of shape is a simplification,

62
00:07:38,200 --> 00:07:43,200
it's all messed up, right, mixed up, because what we do doesn't have rhyme or reason,

63
00:07:43,200 --> 00:07:50,160
much of it is just based on when our habits are not well thought out, we've been working

64
00:07:50,160 --> 00:07:55,400
on our habits since we were children, how could we know what was right and what was wrong,

65
00:07:55,400 --> 00:08:01,800
and so we go through life kind of with a half-assed understanding, half-assed, probably

66
00:08:01,800 --> 00:08:10,680
not the technical term, half-baked, not fully formed, understanding of what's right and

67
00:08:10,680 --> 00:08:17,200
what's wrong, and so we make lots of mistakes, we do right sometimes, we figure some things

68
00:08:17,200 --> 00:08:23,720
out, we all have varying degrees of wisdom, so we use that, but it's not fully formed,

69
00:08:23,720 --> 00:08:33,040
it's not coherent, it's all mixed up, and so the meditation's quite simple, and it's not

70
00:08:33,040 --> 00:08:38,720
something you can doubt, because it's quite the simple activity, it's straight and everything

71
00:08:38,720 --> 00:08:47,440
out, straightening out our minds, working to understand our desires, our versions, our

72
00:08:47,440 --> 00:08:54,520
conceits, and our arrogance, and our views and opinions, and overcome all of the delusion

73
00:08:54,520 --> 00:09:03,320
that we have inside, meditation really straightens us out, and there's nothing better

74
00:09:03,320 --> 00:09:09,760
for other people than to be straightened out yourself. If you're straight yourself, first

75
00:09:09,760 --> 00:09:14,440
of all, if everyone did this, there would be no need to help everyone else, right? If everyone

76
00:09:14,440 --> 00:09:18,600
practiced meditation, then no one would need to teach meditation, no one would need to

77
00:09:18,600 --> 00:09:24,960
help the world. We have more than enough for everyone, and even enough, it's not really

78
00:09:24,960 --> 00:09:31,640
meaningful because the human state is just an artifice, we don't really need all of the

79
00:09:31,640 --> 00:09:37,360
things that a human has, we can give up this human state and go to a pure state, we can

80
00:09:37,360 --> 00:09:44,400
change the whole fabric of reality, maybe that's going too far from most people's understanding,

81
00:09:44,400 --> 00:09:53,480
but we can certainly change the world if we're all positive. But more than just all being

82
00:09:53,480 --> 00:10:02,480
well inside, our interactions with others, how much suffering do we cause in the name of

83
00:10:02,480 --> 00:10:13,120
beneficence, trying to do good things, the Spanish inquisition was ostensibly meant for

84
00:10:13,120 --> 00:10:22,240
benefit. Hitler had an idea of beneficence that he was actually helping the world by

85
00:10:22,240 --> 00:10:29,840
calling the lesser. But these are extreme examples, but we're all in this way. We try to

86
00:10:29,840 --> 00:10:35,960
help the world and we end up yelling and screaming and getting frustrated and burning out.

87
00:10:35,960 --> 00:10:43,120
We still get addicted and attached to our desires that mess up and color our work and

88
00:10:43,120 --> 00:10:55,520
our beliefs. So this should be a really, this sort of argument is voiced far too often by

89
00:10:55,520 --> 00:11:00,320
people who clearly, if they voice and clearly have not thought or investigated the topic

90
00:11:00,320 --> 00:11:09,360
at all. It's easily debunked, but only if you've taken the time to think and to work

91
00:11:09,360 --> 00:11:15,440
and follow the meditation practice to see the benefit, to see that it's not just naval gazing.

92
00:11:16,400 --> 00:11:23,680
Although the truth is right at our navel, if you do watch your stomach rising and falling,

93
00:11:24,640 --> 00:11:30,320
you can change the world and you can become enlightened just by watching your stomach rise and

94
00:11:30,320 --> 00:11:41,440
fall. It's quite profound how simple it is. So that's a bit of a demo for tonight.

95
00:11:43,280 --> 00:11:47,680
I think that's all I have to say about that. Look at some of the questions.

96
00:11:49,760 --> 00:11:59,520
Okay, question verse 37. I have a part-time job, live at home with my mum and dad, not a very

97
00:11:59,520 --> 00:12:05,840
complicated person. Is it not advisable to live a simple life and still practice meditation, of

98
00:12:05,840 --> 00:12:12,720
course? I mean that's what monastic life is supposed to be. The monk life is, I don't know what

99
00:12:12,720 --> 00:12:21,680
verse 37 was, I can't think that far back, but living simply is great. So the idea of becoming

100
00:12:21,680 --> 00:12:28,240
a monk is the claim is it's the simplest way of life. You put aside everything just to cultivate

101
00:12:28,240 --> 00:12:41,200
spirituality. We'll be a more question, a long question. I've been having doubts as to whether I

102
00:12:41,200 --> 00:12:44,960
actually want to free my life of suffering and desire. While there are two different things,

103
00:12:45,920 --> 00:12:52,400
desire wouldn't be a problem if it didn't link to suffering. Sounds quite strange and ignorant.

104
00:12:52,400 --> 00:12:58,400
Well, it's natural. Most of us, this was the demo part of verse last night. I just recorded it.

105
00:12:58,400 --> 00:13:04,160
It'll be up soon. So maybe you can watch that. Maybe that'll help. That suffering and desire

106
00:13:04,160 --> 00:13:07,600
and all the things that go along with them are part of the typical human experience.

107
00:13:07,600 --> 00:13:12,400
And in some way, I feel that I would be missing out. I tried to remove them from my life.

108
00:13:13,280 --> 00:13:17,040
So many of the greatest human achievements have been inspired or motivated by these things.

109
00:13:17,040 --> 00:13:21,520
What would you say about this? It's a very good question. I mean, it's basically

110
00:13:21,520 --> 00:13:29,920
the demo product from last night. We do things for last night. It's a long in the same vein.

111
00:13:32,320 --> 00:13:37,120
We do things in the world. We all have these things in the world. We want to achieve things. We

112
00:13:37,120 --> 00:13:46,320
want to obtain things. We have many desires. I mean, the first thing I can say before getting

113
00:13:46,320 --> 00:13:57,680
really into it is that it's not a reason to desire. Your desire is not a reason to desire.

114
00:13:57,680 --> 00:14:03,280
You see what I mean? You say, I'm not sure if I want to give up desire. It's kind of funny,

115
00:14:03,280 --> 00:14:10,960
because of course you don't. That's what desire means. We desire it. Therefore, of course,

116
00:14:10,960 --> 00:14:19,920
we don't want to get rid of them. So that kind of points to the means of overcoming the problem.

117
00:14:19,920 --> 00:14:25,840
You can't approach desire directly. This is why the Buddha, he put desire in a special category.

118
00:14:25,840 --> 00:14:31,600
Ajentong brought this up and really drew my attention to this. The difference between anger,

119
00:14:31,600 --> 00:14:36,560
for example, anger and greed. Anger is something you give up. You know it's bad. It feels bad.

120
00:14:36,560 --> 00:14:42,080
It's not something we want. We don't want to be anger, but greed. Greed is something that

121
00:14:42,080 --> 00:14:47,520
can be pleasant. So Manasas, Sahagatanya, can come associated with pleasure.

122
00:14:49,920 --> 00:14:54,000
And so it's something that you have to train yourself out of. It's not something you can just say,

123
00:14:54,000 --> 00:14:59,440
no, I don't want. So that's something that we can approach directly. But this is why we have to

124
00:14:59,440 --> 00:15:04,400
separate desire and suffering. Don't worry about desire. Don't worry about the things that you want.

125
00:15:04,400 --> 00:15:11,200
That's not useful. It's not where we have to focus. That's not focused on that. Okay,

126
00:15:11,200 --> 00:15:16,160
you want all these things fine. Let's look at the fact that you're suffering. Why are you suffering?

127
00:15:16,160 --> 00:15:24,800
Let's learn about that. Let's study that. And this helps you not get mixed up because desire,

128
00:15:24,800 --> 00:15:32,960
when the mind has desire in it, it's unamble, incapable of seeing the detriment, seeing the problem.

129
00:15:32,960 --> 00:15:39,120
This is what I was saying about the dumb pun is, it's not rational. You can't convince yourself

130
00:15:39,120 --> 00:15:43,280
not to want something. The mind is in a state that it won't hear those arguments.

131
00:15:45,120 --> 00:15:54,080
So that's not where you put your attention. When you focus on the suffering and try to learn

132
00:15:54,080 --> 00:16:00,000
why you're suffering and try to experience and see what's going on that's causing your suffering,

133
00:16:00,000 --> 00:16:04,720
then you really can break it apart. Then you can see things rationally and clearly.

134
00:16:05,360 --> 00:16:12,400
And you can see how your desires are causing you stress. It's not intellectual. You'll just feel

135
00:16:12,400 --> 00:16:20,640
kind of exhausted. The one thing, the same thing again and again getting it and then not being

136
00:16:20,640 --> 00:16:29,200
satisfied. Not getting it and being dissatisfied and again and again and again and again and eventually

137
00:16:29,200 --> 00:16:38,080
you get bored of it. This is how spiritual people are. Someone who is very spiritual will feel like

138
00:16:38,080 --> 00:16:44,400
this. They'll feel generally bored of life. Like they've tried everything and it just, they saw

139
00:16:44,400 --> 00:16:52,240
through it. This is a sign of high-mindedness. People who are depressed and want to kill themselves often.

140
00:16:52,240 --> 00:16:59,280
It comes from a sort of always an understanding that there's just nothing to life like is in the

141
00:16:59,280 --> 00:17:07,200
end just a game, you know, you play the game enough, you get tired of it. You know, most of us are

142
00:17:07,200 --> 00:17:12,560
not tired of it. We're still keen on going after it and that won't come in this humanity.

143
00:17:13,440 --> 00:17:19,840
It starts with what is most course and most obvious. People who are addicted to really unpleasant

144
00:17:19,840 --> 00:17:26,000
things like killing or stealing or lying or cheating or drugs or alcohol. You're addicted to those

145
00:17:26,000 --> 00:17:35,120
things are pretty well able to let go of them because they're intense suffering. Now attachment

146
00:17:35,120 --> 00:17:40,800
to music or food or something like that is much harder to see and most of us can't or won't ever

147
00:17:40,800 --> 00:17:46,240
see it. But when you look, the truth is regardless of whether we'll see it or not or whether we

148
00:17:46,240 --> 00:17:53,920
want it or not. The truth is when you look clearly, you will give it up because you will see

149
00:17:55,440 --> 00:18:00,240
undeniably. You don't have to be convinced and you don't have to convince yourself. You will see

150
00:18:00,240 --> 00:18:06,880
without any doubt, any shred of doubt that it's not worth it. It's not beneficial. It's not pleasant.

151
00:18:06,880 --> 00:18:10,640
It doesn't lead to happiness. It doesn't make you a better person. It doesn't make you a happier

152
00:18:10,640 --> 00:18:17,520
person. And the funny thing about these things is if they did in general make us happier people,

153
00:18:17,520 --> 00:18:22,720
then we would see ourselves constantly getting happier as we pursued these things, which in fact

154
00:18:22,720 --> 00:18:28,400
is not the case. There are things in life that do make us happier, but they are not seeking out

155
00:18:28,400 --> 00:18:33,920
pleasure. You don't become happier the more you listen to music. When you listen to the music that

156
00:18:33,920 --> 00:18:38,960
you like, you're happy and if you vary it enough so that it's not repetitive, your mind is

157
00:18:38,960 --> 00:18:44,480
caught, your brain is constantly stimulated. You can maintain that, but you don't become happier.

158
00:18:45,360 --> 00:18:52,880
Food doesn't make you happier. Sex doesn't make you happier. It makes you happy. Pleasure in that

159
00:18:52,880 --> 00:19:02,960
moment. Not happier as a person. Unhappy. If you're a person who isn't intent upon this,

160
00:19:02,960 --> 00:19:08,960
have them stop and put them in a room where they no longer have these things. See how they fare,

161
00:19:09,520 --> 00:19:14,720
compared to a person, an ordinary person. They behave very much like a drug addict.

162
00:19:16,480 --> 00:19:22,480
This is why jail is torture for so many of us. There was this sensory deprivation chamber

163
00:19:22,480 --> 00:19:28,000
that they're talking about where you can't hear anything. It's a perfect silence. And they were

164
00:19:28,000 --> 00:19:32,720
saying people couldn't stay in there for more than 45 minutes. They just started to go insane.

165
00:19:34,320 --> 00:19:39,840
Now I can imagine it has an effect on the brain. It's disorienting, but most of that is simply

166
00:19:39,840 --> 00:20:00,720
because we desire stimulus. Anyway, so I hope that helps. When a person sleeps too few hours,

167
00:20:00,720 --> 00:20:05,200
they may feel drowsy, forgetful, have difficulty being aware, have difficulty being mindful.

168
00:20:05,200 --> 00:20:10,560
It can feel a little like being drunk. But in Buddhism we are taught to limit our sleep.

169
00:20:10,560 --> 00:20:19,200
How do we understand this apparent conflict? Well, the reason we feel so drowsy and drunk

170
00:20:19,200 --> 00:20:31,200
is because of our bodies being accustomed to sleeping, but also not even just that, also because

171
00:20:31,200 --> 00:20:37,440
of how tired out we become from our mental activity. If you're meditating intensely, you don't

172
00:20:37,440 --> 00:20:42,160
even need to sleep. There are people who go days, weeks, months without sleep. Your body

173
00:20:42,160 --> 00:20:47,840
acclimentizes to it, so that drunk feeling reduces and eventually doesn't even arise.

174
00:20:49,600 --> 00:20:53,680
And also your mind becomes more refined and more streamlined.

175
00:20:53,680 --> 00:21:01,840
In the beginning, it's also quite useful and end in the long term. It's useful in terms of

176
00:21:02,640 --> 00:21:09,040
pushing yourself. When you push yourself beyond what you're comfortable with,

177
00:21:10,560 --> 00:21:15,120
and most of us are not comfortable with sleeping a little, then it agitates. You're able to see your

178
00:21:15,120 --> 00:21:22,080
reactions and you're able to assess your reactions. You're able to see what reactions are

179
00:21:22,080 --> 00:21:28,480
wholesome and what you're unwholesome. If you always get what you want, you'll never come to see

180
00:21:28,480 --> 00:21:34,960
the problem with desire, because with the one thing, because you're always getting it, you say,

181
00:21:34,960 --> 00:21:40,000
well, that's good. I want and therefore I get when I want to get. So when you start depriving

182
00:21:40,000 --> 00:21:45,680
yourself of things that you want, sleep being a very good example. When you start to see how we're

183
00:21:45,680 --> 00:21:54,800
just like baby cows, crying out for our mother's milk. So you want to train the cow to grow up to

184
00:21:54,800 --> 00:22:01,920
be a good, an ox, you know, in old times they use ox and they had to train the ox.

185
00:22:03,120 --> 00:22:08,480
But a baby ox was kind of useless. So we had to train it and to take it away from its mother,

186
00:22:09,200 --> 00:22:14,400
kind of cruel, I know. And in the long term, it was for the betterment of the ox's training.

187
00:22:14,400 --> 00:22:19,520
Now our minds are, let's maybe cruel to ox, but it's not cruel to our minds. Our minds,

188
00:22:19,520 --> 00:22:24,320
we do have to train. They are something that it's necessary. Without training, our minds,

189
00:22:25,520 --> 00:22:30,960
as I said, they're all mixed up. A natural mind state is a chaotic mind state. That doesn't

190
00:22:31,760 --> 00:22:36,320
work for one's benefit at all times. You might say it's natural and you say, well, you know, isn't

191
00:22:36,320 --> 00:22:41,680
that the way of life? Isn't that the way of humans? Sure. And look at the world. Look at the way

192
00:22:41,680 --> 00:22:46,720
of humans, what it's brought. It doesn't have to be this way. Just because it has been this way,

193
00:22:46,720 --> 00:22:51,680
or just because it seems like evolution has made it this way. There's a no way indicative

194
00:22:51,680 --> 00:22:57,440
that it should be this way, or that it's better this way. In fact, if anything, human beings

195
00:22:57,440 --> 00:23:05,280
have shown in terms of evolution is that evolution was a no way a God gift from God.

196
00:23:05,280 --> 00:23:19,520
It was just a random chaotic sort of way of promoting certain genes, you know, certain genetic

197
00:23:19,520 --> 00:23:25,360
material. And being human has showed us that we can get beyond our genetics, beyond evolution.

198
00:23:26,320 --> 00:23:34,880
Beyond natural selection, we can be compassionate to cripples and sick, and to all types of people

199
00:23:34,880 --> 00:23:42,400
who would, in a survival of the fittest type of society, just not survive. Anyway,

200
00:23:43,120 --> 00:23:49,760
it's a little off tracker, but you know, I think that's a fairly comprehensive answer. There's

201
00:23:49,760 --> 00:23:58,320
several reasons to challenge you, but also to refine, force you to refine your mind. And that eventually

202
00:23:58,320 --> 00:24:07,280
those things go in. Is nibana the greatest human achievement? You sound like humans

203
00:24:07,280 --> 00:24:13,520
sound like you're saying, you're asking whether humans created nibana. The greatest achievement

204
00:24:13,520 --> 00:24:18,160
the human can achieve, but it's not something that humans created or something.

205
00:24:20,400 --> 00:24:25,680
But yeah, the greatest thing the human could ever achieve is nibana for sure.

206
00:24:25,680 --> 00:24:29,040
It's the only thing that's a real benefit.

207
00:24:32,880 --> 00:24:40,560
Because it's permanent, it takes satisfying. It's not controllable, but it's not important.

208
00:24:45,120 --> 00:24:50,800
Okay, still we're about 50-50 people who meditate people who don't meditate.

209
00:24:50,800 --> 00:24:57,120
If your name is an orange, that means you don't meditate. I'd like to see a greater percentage.

210
00:24:57,120 --> 00:25:19,280
Anyway, that's all for tonight. We're showing you all a good night. See you all next time.

