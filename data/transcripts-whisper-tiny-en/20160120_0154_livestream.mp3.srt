1
00:00:00,000 --> 00:00:27,760
Okay.

2
00:00:27,760 --> 00:00:48,600
Okay, good evening everyone broadcasting live, I think, broadcasting live, today we're

3
00:00:48,600 --> 00:01:13,560
doing Damapada. So, y'all can listen and fill your video up on YouTube.

4
00:01:48,600 --> 00:01:58,440
Okay, so I'm just going to go ahead and start the low early here.

5
00:01:58,440 --> 00:02:26,800
Okay, cool.

6
00:02:26,800 --> 00:02:35,800
.

7
00:02:35,800 --> 00:02:40,800
..

8
00:02:40,800 --> 00:02:43,800
..

9
00:02:43,800 --> 00:02:47,800
..

10
00:02:47,800 --> 00:02:51,800
..

11
00:02:51,800 --> 00:02:54,800
..

12
00:02:54,800 --> 00:02:55,800
..

13
00:02:55,800 --> 00:03:04,800
..

14
00:03:04,800 --> 00:03:08,000
Welcome back to our study of the Damepada.

15
00:03:08,000 --> 00:03:15,800
Today we continuing on with first No. 1.27 which wreaths as follows –

16
00:03:15,800 --> 00:03:23,700
.

17
00:03:23,700 --> 00:03:53,660
navinti, so chagatib, pade, so yatatitomu, jia, papa kamma, which means not up in the sky or in

18
00:03:53,660 --> 00:04:09,660
the middle of the ocean, nantalikay, nasamudamajay, napapata, nangvivarangpavisa, not having entered

19
00:04:09,660 --> 00:04:19,340
into a cave, a cavern in a mountain, entered into the middle of a mountain, center of a mountain.

20
00:04:19,340 --> 00:04:31,300
navititi, so chagatib, pade, so it cannot be found such a place on this earth, one cannot

21
00:04:31,300 --> 00:04:42,340
find a place on this earth on the earth, yatatitomu, where standing, moochia, papa kamma, one might

22
00:04:42,340 --> 00:04:54,500
be free from evil karma, evil deed, there's nowhere, not up in the sky, not under the ocean,

23
00:04:54,500 --> 00:05:05,620
not in the deepest, darkest cave, there's nowhere on earth, that's the quote.

24
00:05:05,620 --> 00:05:15,620
So an interesting story, actually three stories, that goes with this verse, it seems there

25
00:05:15,620 --> 00:05:26,620
were three groups of monks, and so we have three stories, the Buddha was living in jaytawana,

26
00:05:26,620 --> 00:05:35,620
a first group of monks set out to meet the Buddha, and on their way they entered a village,

27
00:05:35,620 --> 00:05:46,620
a certain village for alms, and while they were sitting, or after they'd eaten, or while

28
00:05:46,620 --> 00:05:55,620
they were waiting for food, someone who was cooking food in the morning made their fire

29
00:05:55,620 --> 00:06:06,380
to hot, and suddenly the fire flame burst up, and lit the satch, roof of their hut, and

30
00:06:06,380 --> 00:06:14,860
the satch, when flying up in a ball of flame or floating through the air, being carried

31
00:06:14,860 --> 00:06:25,380
away by the wind, and at that moment a crow was flying, and so this tuft of satch caught

32
00:06:25,380 --> 00:06:32,180
the crow, and immediately burst into flames, and it was on fire, so the crow burnt

33
00:06:32,180 --> 00:06:40,220
to a crisp, fell to the ground dead, right in front of the monks, and they thought it

34
00:06:40,220 --> 00:06:47,020
was kind of remarkable because just one tuft of thatch flew up and caught the crow

35
00:06:47,020 --> 00:06:58,940
squarely as it was flying by, and so they wondered to themselves sort of monk talk, wonder

36
00:06:58,940 --> 00:07:05,340
what the karma of this bird was, that this would happen, and they said who would know

37
00:07:05,340 --> 00:07:13,780
besides the Buddha, so when we get there we should ask him, and so they continued on their

38
00:07:13,780 --> 00:07:17,900
way, that's the first group.

39
00:07:17,900 --> 00:07:22,340
The second group of monks also on their way to see the Buddha, I mean it seems like probably

40
00:07:22,340 --> 00:07:27,620
this was a thing where monks would come to the Buddha with these sorts of questions, so

41
00:07:27,620 --> 00:07:32,100
I think we shouldn't be surprised that there were three groups in the second group.

42
00:07:32,100 --> 00:07:39,620
On their way to see the Buddha they took a boat across the ocean, somehow, not sure

43
00:07:39,620 --> 00:07:46,460
where they were, maybe they were in Burma, or maybe Thailand, maybe they were in Sri Lanka

44
00:07:46,460 --> 00:07:54,980
who knows, but probably not, so it's interesting to think of them being on a ship because

45
00:07:54,980 --> 00:08:06,700
I'm not much record of monks outside of India, but I'm really not clear about that sort

46
00:08:06,700 --> 00:08:15,140
of thing, but happened that in the middle of the ocean, their boat stopped, the wind stopped

47
00:08:15,140 --> 00:08:23,420
and they couldn't sail anymore, and this was a thing for sailors that if the wind stopped

48
00:08:23,420 --> 00:08:28,780
for a long period of time they would get superstitious and they would think somebody

49
00:08:28,780 --> 00:08:35,900
on board is their karma is not allowing us to continue, and so they'd throw that person

50
00:08:35,900 --> 00:08:41,340
overboard, if they could figure out what it was, and the way they did it was they found

51
00:08:41,340 --> 00:08:47,580
the scientific method, they'd draw lots, and whoever drew the shortest straw would get

52
00:08:47,580 --> 00:08:55,780
thrown overboard, it seems reasonable to know, so they did this, don't make that well,

53
00:08:55,780 --> 00:09:05,740
whoever gets the shortest straw gets thrown over, that's just the way karma works, I guess.

54
00:09:05,740 --> 00:09:12,820
Except lo and behold, the captain's wife drew the lot, now the captain's wife was loved

55
00:09:12,820 --> 00:09:20,980
by everyone, she was young, she was pretty, she was kind, she was great, just all around,

56
00:09:20,980 --> 00:09:25,980
not the sort of person you want to throw overboard to say at least, especially since she

57
00:09:25,980 --> 00:09:31,020
was the captain's wife and so everyone agreed they couldn't throw her overboard, so

58
00:09:31,020 --> 00:09:39,580
they said well we'll draw lots again, again they drew lots and again for a second time

59
00:09:39,580 --> 00:09:48,860
the captain's wife drew the shortest straw, and they still couldn't, they said there's

60
00:09:48,860 --> 00:09:54,180
no way we can't do this, and so a third time it was dangerous straws, and a third time

61
00:09:54,180 --> 00:10:05,500
the captain's wife drew the shortest straw, so they went to the captain and they said

62
00:10:05,500 --> 00:10:13,780
this is what happened three times, it's got to be her, she's the one with a bad luck after

63
00:10:13,780 --> 00:10:20,580
throw her overboard, and so they grabbed her, then the captain said well then yes I guess

64
00:10:20,580 --> 00:10:26,180
that's how it has to go, scientifically shown that she's the one, she's the blame for

65
00:10:26,180 --> 00:10:37,460
the wind, and so they said throw overboard, and as they started to throw overboard, they

66
00:10:37,460 --> 00:10:44,700
started screaming, reasonably, she doesn't seem to be that confident in the scientific

67
00:10:44,700 --> 00:10:52,060
method that they used, but it just certainly doesn't seem to have wanted to be thrown overboard,

68
00:10:52,060 --> 00:11:00,700
so interestingly the captain has them take her jewels away, says well there's no need

69
00:11:00,700 --> 00:11:05,180
to, when he heard this he saw and he said oh there's no need to, or for her jewels

70
00:11:05,180 --> 00:11:09,740
to go to waste, so he took her jewels and had them wrap her up in a cloth and tie her rope

71
00:11:09,740 --> 00:11:15,700
around her neck, so that she couldn't, or wrap her up in a cloth so that she wouldn't

72
00:11:15,700 --> 00:11:22,740
scream, and then tie her rope around her neck and tie it to a big heavy pot of sand,

73
00:11:22,740 --> 00:11:31,340
so that they wouldn't see her, so that he wouldn't have to see her, because he was

74
00:11:31,340 --> 00:11:40,980
kind of fond of her, and so they threw overboard, and as soon as she hit the ocean, sharks

75
00:11:40,980 --> 00:11:47,700
and turtles and fish and so on, ate her, tore her to bits and she died, and the monks

76
00:11:47,700 --> 00:11:52,900
on board were watching this, and of course didn't really have a say in it all, but they

77
00:11:52,900 --> 00:11:58,700
were shocked as well, they couldn't believe that this sort of thing could happen that

78
00:11:58,700 --> 00:12:04,940
she couldn't really be at the mercy of these people, because it was a bit of coincidence

79
00:12:04,940 --> 00:12:12,340
that she drew the short straw three times, that's quite, unless there were only a few

80
00:12:12,340 --> 00:12:19,620
people on board that was quite a coincidence, and so they said I wonder what karma she

81
00:12:19,620 --> 00:12:32,020
did to deserve such a horrible fate, and likewise they said well that's asked the Buddha

82
00:12:32,020 --> 00:12:34,620
to find out.

83
00:12:34,620 --> 00:12:41,980
It's important to point out as we go along, because I'm sure the question coming up in

84
00:12:41,980 --> 00:12:46,620
people's minds is, well what about the people who did that to her, I mean it's not karma,

85
00:12:46,620 --> 00:12:54,260
it's those people, and it has to be mentioned that karma isn't like one thing in the

86
00:12:54,260 --> 00:12:57,900
past, you blame things in your past life, that's not true at all.

87
00:12:57,900 --> 00:13:03,940
Those people who threw that woman into the ocean did a very, very bad thing, there's

88
00:13:03,940 --> 00:13:09,780
no question about it, that was an evil thing, Buddhism doesn't condone that, but how

89
00:13:09,780 --> 00:13:21,820
she got herself in that situation we see, where the likelihood of her being subject to

90
00:13:21,820 --> 00:13:30,300
that, because these people were not doing it out of hate for her, they were doing it

91
00:13:30,300 --> 00:13:39,740
out of ignorance and superstition, but they didn't just randomly pick someone, so

92
00:13:39,740 --> 00:13:45,300
how did she get herself in that situation, the theory is that there's more behind it,

93
00:13:45,300 --> 00:13:56,980
how we, our life comes to these points, anyway, that was the second one.

94
00:13:56,980 --> 00:14:04,820
The third, the third story, there were seven monks who likewise set out to see the Buddha,

95
00:14:04,820 --> 00:14:13,860
and on their way, they came to a certain monastery, and they asked to stay the night, and

96
00:14:13,860 --> 00:14:20,980
the seven of them were invited to stay in a special cave in the side of the mountain that

97
00:14:20,980 --> 00:14:28,860
was designated for visiting monks, and so they went there, and they settled down, and

98
00:14:28,860 --> 00:14:31,780
they fell asleep for the night.

99
00:14:31,780 --> 00:14:37,060
During the night, a huge boulder, it says the size of a pagoda, which would be very, very

100
00:14:37,060 --> 00:14:47,060
large, fell down the mountain and covered the entrance to the cave where they were staying,

101
00:14:47,060 --> 00:15:01,580
just out of the blue, blocking their exit, making them possible for them to get out.

102
00:15:01,580 --> 00:15:07,980
When the resident monks found out what happened, they said we got to move that rock, there's

103
00:15:07,980 --> 00:15:14,180
monks trapped in there, and so they gathered, gathered men, strong people from all around

104
00:15:14,180 --> 00:15:22,980
the countryside, and they worked tirelessly for seven days to remove this rock with the

105
00:15:22,980 --> 00:15:29,620
rock wooden butt, until finally, on the seventh day, after seven days, the rock moved

106
00:15:29,620 --> 00:15:36,500
as though it had never been stuck there, the rock moved very easily, I think it even says

107
00:15:36,500 --> 00:15:49,980
that it moved by itself, away from the entrance, just suddenly became dislodged, and so

108
00:15:49,980 --> 00:15:56,620
these seven monks had spent seven days without food, without water, were almost dead,

109
00:15:56,620 --> 00:16:02,300
and yet when they came out, they were able to get water and food and survive, but they

110
00:16:02,300 --> 00:16:09,500
said to themselves, what we did, it seems very strange sort of thing to happen, I wonder

111
00:16:09,500 --> 00:16:16,140
if this is a cause of past karma, and so likewise they decided to ask the Buddha.

112
00:16:16,140 --> 00:16:21,140
So these three groups of monks met up and this is the story, and then the Buddha tells

113
00:16:21,140 --> 00:16:28,660
three stories about their past, the first the past of the crow, they go to see the Buddha

114
00:16:28,660 --> 00:16:39,020
and the Buddha says, the crow is suffering for past deeds, and it seems like the story

115
00:16:39,020 --> 00:16:45,020
is kind of suggesting that it's not just one past deed, but it's sort of a habit of

116
00:16:45,020 --> 00:16:53,820
bad deeds, but he gives examples, so he says, for a long time ago the crow was a farmer

117
00:16:53,820 --> 00:17:00,100
and he had an ox, and he was trying to get this ox to do work for him, but try as he

118
00:17:00,100 --> 00:17:06,660
might, he couldn't get the ox to, he couldn't tame the ox, he'd get it to work, and

119
00:17:06,660 --> 00:17:12,140
then it would work for a little bit, and then it would come right down, and then he'd

120
00:17:12,140 --> 00:17:17,980
get it to move, and it would move, this ox was just terribly, terribly stubborn, and so

121
00:17:17,980 --> 00:17:26,260
he got increasingly more and more angry until he finally got angry enough that the ox

122
00:17:26,260 --> 00:17:33,100
just laid down, that he just covered it up in straw and lit it on fire, and the Buddha

123
00:17:33,100 --> 00:17:39,780
said, because of that evil deed, he was born in hell actually, and after being born

124
00:17:39,780 --> 00:17:44,380
in hell, he was born back in the human realm, and then he was born back in the animal realm

125
00:17:44,380 --> 00:17:54,540
as a crow, and still suffering from it to this day, in fact it says he was seven times

126
00:17:54,540 --> 00:18:02,260
in succession more reborn as a crow, and then we have the story of the woman on the

127
00:18:02,260 --> 00:18:11,300
boat, this woman in the past, she was a woman who lived in Benerys, who I don't see

128
00:18:11,300 --> 00:18:18,860
as it's known now, and she had a dog, she was, she did, she was a housewife, and so she

129
00:18:18,860 --> 00:18:23,340
did all these chores, but there was a dog in the house that would follow her around,

130
00:18:23,340 --> 00:18:30,700
her around everywhere, and for some reason people were, it would tease her, because this

131
00:18:30,700 --> 00:18:39,140
dog was following her like a shadow and very, very much very, very affectionate, actually

132
00:18:39,140 --> 00:18:43,700
like normal dogs are, but people were joking about it, because I guess it wasn't a big

133
00:18:43,700 --> 00:18:48,420
thing for women to have dogs following them around, in fact it was a common thing for

134
00:18:48,420 --> 00:18:55,980
hunters to have dogs, as we learned in our previous story.

135
00:18:55,980 --> 00:19:02,380
So these young men joked about it and said, oh, here comes the hunter with their dog,

136
00:19:02,380 --> 00:19:09,820
we're going to have meat to eat tonight, you know, they'll be meat coming and joking,

137
00:19:09,820 --> 00:19:16,620
just joking about her looking like a hunter having this big dog go along with.

138
00:19:16,620 --> 00:19:25,340
And this woman was, I guess, of a cruel event, and so getting angry and feeling embarrassed

139
00:19:25,340 --> 00:19:31,540
and ashamed, she picked up a stick and beat the dog, almost the dead.

140
00:19:31,540 --> 00:19:38,180
But dogs being the way they are have a funny resiliency to these sorts of things, and so

141
00:19:38,180 --> 00:19:45,620
the dog was actually unmoved and was still very much in love with this woman, you know.

142
00:19:45,620 --> 00:19:52,940
It turns out, actually the commentary says that this dog used to be her husband, and

143
00:19:52,940 --> 00:19:59,420
so that was a reason it was recently her husband in one of her recent births.

144
00:19:59,420 --> 00:20:05,220
And so even though it's impossible to find, they say, is you can't find someone who hasn't

145
00:20:05,220 --> 00:20:09,540
been your husband, your wife, your son, your daughter, your mother, your father.

146
00:20:09,540 --> 00:20:21,620
But recent births, there tends to still be some sort of affinity or enmity, in cases

147
00:20:21,620 --> 00:20:24,500
when there was enmity before.

148
00:20:24,500 --> 00:20:29,500
And so she beat this dog, and it still came back.

149
00:20:29,500 --> 00:20:36,300
And so she was increasingly angry, irrationally angry at this dog.

150
00:20:36,300 --> 00:20:45,780
And so lo and behold, she, when it came close, she picked up a rope and she made a loop

151
00:20:45,780 --> 00:20:49,900
and waited for the dog, when the dog came close, she wrapped the loop around the dog and

152
00:20:49,900 --> 00:20:56,340
tied it to a pot full of sand and threw the pot of sand into this big pool, and it rolled

153
00:20:56,340 --> 00:21:02,220
down into the pool, and the dog was pulled and was dragged after it into the pool and

154
00:21:02,220 --> 00:21:06,460
it drowned.

155
00:21:06,460 --> 00:21:15,860
And that was the karma that caused her to be thrown overboard.

156
00:21:15,860 --> 00:21:20,460
As well as spend many years in hell.

157
00:21:20,460 --> 00:21:22,300
That's story number two.

158
00:21:22,300 --> 00:21:29,540
Story number three, it tells you monks are also done bad things in the past.

159
00:21:29,540 --> 00:21:41,980
And so at one time you were cowhertz and you came upon this huge lizard, and I guess it

160
00:21:41,980 --> 00:21:45,740
was something that they would like to eat, people would like to eat.

161
00:21:45,740 --> 00:21:50,420
And so they ran after it, trying to catch this big lizard.

162
00:21:50,420 --> 00:21:55,140
But it ran into an ant, an ant, that had seven holes.

163
00:21:55,140 --> 00:21:57,580
Some reason seven is a big number.

164
00:21:57,580 --> 00:22:00,100
I think it probably just means there were a bunch of holes.

165
00:22:00,100 --> 00:22:08,620
And so they plugged up all these holes, and then they, you know, thinking that they

166
00:22:08,620 --> 00:22:11,460
could catch it at one of the holes, but then they said, you know, we just don't have time

167
00:22:11,460 --> 00:22:12,460
for this.

168
00:22:12,460 --> 00:22:15,580
Well, we'll plug up all the holes and we'll come back tomorrow.

169
00:22:15,580 --> 00:22:19,460
We'll catch this lizard because there's now no way out of this big, I guess, a termite

170
00:22:19,460 --> 00:22:24,180
mound or something, something with the zooms, I don't know, something that lizards

171
00:22:24,180 --> 00:22:25,660
like to stay in.

172
00:22:25,660 --> 00:22:26,660
Big lizard though.

173
00:22:26,660 --> 00:22:30,100
So we'll come back tomorrow and we'll catch it.

174
00:22:30,100 --> 00:22:34,180
So they went home, but then they forgot all about it.

175
00:22:34,180 --> 00:22:38,540
And so for seven days they went about their business, attending cows elsewhere, but then

176
00:22:38,540 --> 00:22:43,220
on the seventh day they came back and they were attending cows, and they saw the ant

177
00:22:43,220 --> 00:22:47,740
hell again, and they realized, oh, I wonder what happened in the head, lizard.

178
00:22:47,740 --> 00:22:55,940
And so they opened up the holes and the lizard at this point starved and dehydrated,

179
00:22:55,940 --> 00:23:02,660
not afraid of its first life at all, and at it's basically at the end of its tether.

180
00:23:02,660 --> 00:23:12,460
I had to come out and so came out and they said to themselves, they felt pity to pity

181
00:23:12,460 --> 00:23:17,900
on this and oh, that's not kill it, this poor thing, we tortured it terribly.

182
00:23:17,900 --> 00:23:22,660
So they nursed it and they actually brought it back to life and he said, the Buddha said,

183
00:23:22,660 --> 00:23:27,740
see, because of that you were able to escape, because you came back for this lizard, if

184
00:23:27,740 --> 00:23:32,700
not, that would have been in freedom.

185
00:23:32,700 --> 00:23:37,180
I think these stories are interesting, whether you believe them or not, but they give

186
00:23:37,180 --> 00:23:42,580
some idea of the nature of karma according to Buddhism, and some of the ways, they're

187
00:23:42,580 --> 00:23:48,780
just examples, doesn't mean they're not law, like this has to be like this, but apparently

188
00:23:48,780 --> 00:23:55,020
the way things sometimes turn out, like our past deeds influence both in this life and

189
00:23:55,020 --> 00:23:59,900
the necks, the influence, the things that happen to us.

190
00:23:59,900 --> 00:24:05,540
And then they said, but is it really that way that you can't escape your karma, isn't

191
00:24:05,540 --> 00:24:09,620
there somewhere you could go to escape it, couldn't you run away and the Buddha said,

192
00:24:09,620 --> 00:24:14,900
no, you couldn't run away, there's no place on earth that you can go to run away from

193
00:24:14,900 --> 00:24:15,900
your karma.

194
00:24:15,900 --> 00:24:25,620
There's another job to cut the talks about this, there's a goat that, this Brahmin, the

195
00:24:25,620 --> 00:24:30,380
goat talks to him and the goat says, you know, it's this crying laughing job to go, where

196
00:24:30,380 --> 00:24:35,380
he cries and then he laughs, but he laughs and then he cries and he's about to be

197
00:24:35,380 --> 00:24:40,820
killed and the goat starts laughing and then, and he says, why are you laughing?

198
00:24:40,820 --> 00:24:46,660
He said, because this is my last life, I was a brah, I'm now, this is the last life

199
00:24:46,660 --> 00:24:53,260
that I have to be born as a goat for 500 years, I've been a sacrificial goat, this is

200
00:24:53,260 --> 00:24:54,260
it.

201
00:24:54,260 --> 00:24:58,380
And then he starts crying and the guy's, the Brahm Brahm is why are you crying and he's

202
00:24:58,380 --> 00:25:04,620
a, because I'm thinking of you, why I was a goat for 500 years being sacrificed, having

203
00:25:04,620 --> 00:25:10,060
my head cut off is because before that I was a brahmin just like you, who killed goats.

204
00:25:10,060 --> 00:25:14,900
So I know this is where you are going to go and the brahmin said, oh, then I'll protect

205
00:25:14,900 --> 00:25:20,860
you, I won't let them kill you, and he said, there's nothing you can do, there's

206
00:25:20,860 --> 00:25:25,060
no way you can stop it, and sure enough, the brahmin tried to protect him and made sure

207
00:25:25,060 --> 00:25:30,940
nobody came near him, but a rock fell actually on this, on this goat.

208
00:25:30,940 --> 00:25:40,740
There's some really strange coincidence, you ended up dying, karma is like that.

209
00:25:40,740 --> 00:25:45,140
You see this, you see potentially these sorts of things in the world, very strange things

210
00:25:45,140 --> 00:25:53,260
happened, there was a woman once, the wife of a top Monsanto exec, not that that means

211
00:25:53,260 --> 00:25:59,780
anything, but it's interesting, walking down the road, read this in the paper some years

212
00:25:59,780 --> 00:26:09,580
ago, walking down the road and was suddenly hit by a car and pulled under the car and

213
00:26:09,580 --> 00:26:19,300
dragged screaming for several blocks before she died, dragged under the car, turns out

214
00:26:19,300 --> 00:26:23,980
the woman who was driving the car was an old lady who can barely see above the dash and

215
00:26:23,980 --> 00:26:29,380
had no idea what she'd done, and probably to this day has never been told what she

216
00:26:29,380 --> 00:26:40,260
did, the story said they hadn't told her, so it wasn't a bad karma, she didn't have

217
00:26:40,260 --> 00:26:44,700
the intention to kill, probably some bad karma involved with driving when you shouldn't

218
00:26:44,700 --> 00:26:52,420
be driving, but that's a bit different, but it's a kind of sort of, I mean we have no

219
00:26:52,420 --> 00:27:01,260
idea why that happened, so people would say it's just a coincidence, but it's interesting

220
00:27:01,260 --> 00:27:13,340
to look and see, I mean if you think in terms of sort of cause and effect, the problem

221
00:27:13,340 --> 00:27:18,380
I think is that people focus too much on physical cause and effect and they call the

222
00:27:18,380 --> 00:27:26,580
mind, there's this term epiphenomenal, that the mind is at best, just a byproduct that

223
00:27:26,580 --> 00:27:33,540
is ineffectual, that has no consequence, that the mind can't affect the body, can't

224
00:27:33,540 --> 00:27:43,100
affect reality, so mind is just this thing that happens, sort of like just a byproduct

225
00:27:43,100 --> 00:27:47,940
of sine proc, that's meaningless, but if you think of the mind as being powerful, it's

226
00:27:47,940 --> 00:27:58,220
being potent, then it makes sense to think that such a powerful experience should have

227
00:27:58,220 --> 00:28:08,660
some cause, like physical things don't just happen coincidentally, an explosion takes

228
00:28:08,660 --> 00:28:20,260
gunpowder, a supernova takes a lot of energy, and so the idea that these experiences

229
00:28:20,260 --> 00:28:28,180
of being dragged under a car should take some, couldn't, should not just happen randomly,

230
00:28:28,180 --> 00:28:31,500
I think there's something to that, I think there's something that we're missing, often

231
00:28:31,500 --> 00:28:37,220
when we say it's just random, it's just coincidence, I think there's a much more, there's

232
00:28:37,220 --> 00:28:45,180
an argument, even not from, you know, meditation or so on, that it would take some kind

233
00:28:45,180 --> 00:28:52,220
of structure, some kind of constant effect, but for meditative purposes, I mean this is

234
00:28:52,220 --> 00:29:03,540
a great part to us, the idea that our intentions, our minds have consequences, this is

235
00:29:03,540 --> 00:29:10,540
something that moves people to meditate, moves meditators, not to do unholstled meditations,

236
00:29:10,540 --> 00:29:15,780
meditation for this reason will change your life because you start to see how powerful

237
00:29:15,780 --> 00:29:23,820
and how poisonous the mind can be, how harmful the mind can be when misdirected, how dangerous

238
00:29:23,820 --> 00:29:32,100
it is, you can see these things building, you can see how poisonous, you can imagine what

239
00:29:32,100 --> 00:29:37,700
it's like to do these things, and you can remember the things that you've done, and

240
00:29:37,700 --> 00:29:46,180
without any, without any, without any prompting, like anyone telling you it's wrong or

241
00:29:46,180 --> 00:29:51,220
it's bad, you just start to feel really bad about the things that you've done, bad things

242
00:29:51,220 --> 00:29:55,980
you've done, it doesn't take someone to tell you that's bad karma or so on, that happens

243
00:29:55,980 --> 00:30:01,820
as well, people feel guilty because they, they're told that things are bad, but you

244
00:30:01,820 --> 00:30:06,700
just can't abide by it because it's so powerful, there's actually, this is where you

245
00:30:06,700 --> 00:30:12,540
start to feel the power of these things that's karmic and it's built up inside of you,

246
00:30:12,540 --> 00:30:16,140
it's why our life flashes before our eyes, our life doesn't really flash before our

247
00:30:16,140 --> 00:30:25,380
eyes when we die, and the things that I've had an impact on our mind, that's karma,

248
00:30:25,380 --> 00:30:34,140
those things flash before our eyes, so this has importance in our practice and to remind

249
00:30:34,140 --> 00:30:40,900
us and to reinforce the things that we see during our practice, but also to encourage

250
00:30:40,900 --> 00:30:46,140
people to meditate, to learn about these things, if you want to become a better person,

251
00:30:46,140 --> 00:30:54,940
look at your mind, learn about your mind, these, these things occur throughout our lives,

252
00:30:54,940 --> 00:31:00,380
you know, people do evil things, evil things happen to people who, who don't seem to deserve

253
00:31:00,380 --> 00:31:16,900
them, who don't seem to have ever done anything to deserve them, but when through meditation,

254
00:31:16,900 --> 00:31:22,540
we see that there's actually is this cause and effect relationship, and it's quite reasonable

255
00:31:22,540 --> 00:31:29,620
to suggest that it continues into the future, it's quite reasonable to think whether you

256
00:31:29,620 --> 00:31:34,220
have evidence or proof of it or not, it's quite reasonable to think that it's going to

257
00:31:34,220 --> 00:31:39,580
have an effect on your next life, it's going to have effect on an effect on your choices

258
00:31:39,580 --> 00:31:46,660
of rebirth, it's going to have an effect on how people react to you in the future,

259
00:31:46,660 --> 00:31:51,420
that things don't go away, the key to this verse, so they don't just go away, you can't

260
00:31:51,420 --> 00:31:57,100
outrun it, the only way to outrun it would be to become an aura hunt in light and being,

261
00:31:57,100 --> 00:32:01,620
before it's going to catch up with you, and if you, if you die as an aura hunt in the

262
00:32:01,620 --> 00:32:11,860
future, karma can't, doesn't have an opportunity to bear fruit, anyway, so some stories

263
00:32:11,860 --> 00:32:17,860
about karma, some ideas of karma, that's the them of and of for tonight, thank you

264
00:32:17,860 --> 00:32:24,860
for tuning in, wish you all good practice.

