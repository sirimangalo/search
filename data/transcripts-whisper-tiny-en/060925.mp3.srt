1
00:00:00,000 --> 00:00:10,000
So today we continue on talking about

2
00:00:10,000 --> 00:00:14,000
God of what?

3
00:00:14,000 --> 00:00:19,000
Respect.

4
00:00:19,000 --> 00:00:24,000
The blessing or the auspiciousness.

5
00:00:24,000 --> 00:00:31,000
The omen, the sign of good things to come, which is called respect.

6
00:00:31,000 --> 00:00:34,000
Respect is the sign of good things to come.

7
00:00:34,000 --> 00:00:39,000
It shows that a person is sincere in what they're doing.

8
00:00:39,000 --> 00:00:44,000
We've talked about respect for the Buddha, respect for the Buddha's teaching.

9
00:00:44,000 --> 00:00:55,000
Respect for the people who have practiced the Buddha's teaching.

10
00:00:55,000 --> 00:01:02,000
We have three more objects of respect.

11
00:01:02,000 --> 00:01:09,000
The next one is the respect for the training.

12
00:01:09,000 --> 00:01:18,000
Respect for the training is respect for those things, which we have to study, and we have to train ourselves in.

13
00:01:18,000 --> 00:01:29,000
We have to train ourselves with the respectful and the reverential attitude.

14
00:01:29,000 --> 00:01:42,000
With our whole heart, in which rain we can, we can try to train our minds.

15
00:01:42,000 --> 00:01:58,000
We should be careful to train our minds with us here, sincerely, and wholeheartedly.

16
00:01:58,000 --> 00:02:16,000
The Lord Buddha gave a simile, a comparison with a man who was instructed to carry a pot full of oil, full to the brim.

17
00:02:16,000 --> 00:02:28,000
It means completely full of oil, so that not one drop of oil could fit in the bowl.

18
00:02:28,000 --> 00:02:41,000
He was instructed to carry the bowl of oil through a crowd, through a parade, through a carnival.

19
00:02:41,000 --> 00:02:54,000
Through the city and the time when there was a carnival and there was dancing and singing and music and festivities and all sorts of displays.

20
00:02:54,000 --> 00:03:04,000
Beautiful women, beautiful sounds and dancing and this and that and shows and displays.

21
00:03:04,000 --> 00:03:26,000
He had acrobats and so many things and elephants and he was instructed to carry this bowl full of oil through the city and they said they would have a man, another man followed behind him with a sword.

22
00:03:26,000 --> 00:03:42,000
If the man holding the oil were to spill even a drop, the man walking behind him was ordered to cut his head off with a sword.

23
00:03:42,000 --> 00:04:07,000
If that were the case, do you think the man would waste any time looking at the shows or looking at the beautiful women or looking at the acrobats and the elephants and many various sights and sounds and smells and so on.

24
00:04:07,000 --> 00:04:15,000
But he wasted any time doing so and of course the monks had no, he wouldn't do so.

25
00:04:15,000 --> 00:04:24,000
His mind would be fully intended on the bowl of oil or fear of his life.

26
00:04:24,000 --> 00:04:37,000
We would instruct the monks that they should treat the training in this way.

27
00:04:37,000 --> 00:04:37,000
You should have such sincere dedication to the training.

28
00:04:37,000 --> 00:04:44,000
I always remember the simile of the man with the pot of oil.

29
00:04:44,000 --> 00:04:49,000
The things which are necessary to train in the Buddha's teaching, there's not many.

30
00:04:49,000 --> 00:05:01,000
In fact, there's three things which the Buddha taught that we have to train in these are the base of the Buddhist practice.

31
00:05:01,000 --> 00:05:07,000
They are the training which comes from the practice of mindfulness.

32
00:05:07,000 --> 00:05:12,000
When we practice the four foundations of mindfulness, we are training ourselves in three things.

33
00:05:12,000 --> 00:05:25,000
One, we're training ourselves in morality to avoid all evil deeds of body, speech and even bad deeds of mind.

34
00:05:25,000 --> 00:05:37,000
Number two, training ourselves in concentration to cut off those hindrances in the mind which lead us to do bad deeds.

35
00:05:37,000 --> 00:05:51,000
Three, wisdom to develop the understanding which will cut off all bad deeds so they will never arise again.

36
00:05:51,000 --> 00:06:01,000
Cut off all ignorance which leads to the arising of greed of anger, which leads to the arising of the hindrances,

37
00:06:01,000 --> 00:06:13,000
internally to the arising of bad deeds of body of speech and of mind.

38
00:06:13,000 --> 00:06:20,000
In developing morality, we develop at the time we're practicing.

39
00:06:20,000 --> 00:06:28,000
First, we have mundane moralities so keeping the precepts.

40
00:06:28,000 --> 00:06:36,000
Keeping the precepts is not synonymous with the training of morality which the Lord Buddha taught.

41
00:06:36,000 --> 00:06:44,000
Keeping the precepts is simply a means of encouraging one to develop moral, true morality.

42
00:06:44,000 --> 00:06:52,000
Whether we have five precepts or eight precepts or ten precepts or 227 precepts.

43
00:06:52,000 --> 00:06:59,000
These are things which lead to peace and happiness in the world.

44
00:06:59,000 --> 00:07:10,000
They can be considered essential for leading a good life, for living a proper life in the world.

45
00:07:10,000 --> 00:07:21,000
They're essential for undertaking the practice but they and themselves don't lead to freedom from suffering.

46
00:07:21,000 --> 00:07:33,000
Until we develop morality on a practical level, we can keep the precepts for our whole life and still never reach freedom from suffering.

47
00:07:33,000 --> 00:07:40,000
Morality on a practical level means that when we practice, we stop our minds from wandering.

48
00:07:40,000 --> 00:07:51,000
We stop our minds from becoming distracted by lustful or greedy thoughts.

49
00:07:51,000 --> 00:07:57,000
Stop our minds from being distracted by angry or hateful thoughts.

50
00:07:57,000 --> 00:08:08,000
We stop our minds from falling into delusion, doubt or confusion or wonder or worry, restlessness.

51
00:08:08,000 --> 00:08:14,000
We stop our minds from becoming distracted by these thoughts.

52
00:08:14,000 --> 00:08:20,000
When we think these thoughts, we acknowledge them with our word.

53
00:08:20,000 --> 00:08:32,000
We see angry, angry, liking, liking, wanting, distracted, doubting, worrying.

54
00:08:32,000 --> 00:08:37,000
And bring our minds back to the present moment, back to the object at hand.

55
00:08:37,000 --> 00:08:43,000
When we bring our minds back to the present moment, this is morality on a practical level.

56
00:08:43,000 --> 00:08:47,000
For bidding the mind from indulging and these thoughts are that time.

57
00:08:47,000 --> 00:08:50,000
Using mindfulness to stop.

58
00:08:50,000 --> 00:08:53,000
Stop the mind from wandering.

59
00:08:53,000 --> 00:09:01,000
This is the practice of morality.

60
00:09:01,000 --> 00:09:08,000
When we practice this morality, it leads to concentration.

61
00:09:08,000 --> 00:09:11,000
Concentration is of two kinds.

62
00:09:11,000 --> 00:09:20,000
Monday in concentration and concentration which leads to freedom from suffering.

63
00:09:20,000 --> 00:09:34,000
Monday in concentration we have concentration which fixes on a conceptual object.

64
00:09:34,000 --> 00:09:55,000
The focusing on a white disc, a blue disc, a red disc, focusing on a bowl of earth, focusing on a bowl of water, focusing on a flame, focusing on many things which are concepts which we take up.

65
00:09:55,000 --> 00:10:06,000
For we take up the concept of white, the concept of red, the concept of blue, yellow, fire of earth of air of water.

66
00:10:06,000 --> 00:10:07,000
And so on.

67
00:10:07,000 --> 00:10:13,000
We take up the Buddha, we take up the Dhamma, the Sangha.

68
00:10:13,000 --> 00:10:23,000
For Altik at their 40, they recognize 40 subjects which the Lord Buddha taught.

69
00:10:23,000 --> 00:10:26,000
Which I'll have a concept as their object.

70
00:10:26,000 --> 00:10:40,000
But because the object of contemplation here is a concept, the contemplation can lead to freedom from suffering.

71
00:10:40,000 --> 00:10:46,000
One cannot see about the object that it is impermanent, that it is suffering that it is non-self.

72
00:10:46,000 --> 00:10:55,000
Because as a concept it can be permanent, it can be happy, it can be self.

73
00:10:55,000 --> 00:11:00,000
We can control it, we can keep it forever and so on.

74
00:11:00,000 --> 00:11:05,000
Because it's only a concept, it's not real.

75
00:11:05,000 --> 00:11:11,000
The concentration which leads us out of suffering is concentration which focuses on reality.

76
00:11:11,000 --> 00:11:16,000
It focuses on the body, it focuses on the mind.

77
00:11:16,000 --> 00:11:19,000
And we have the four foundations of mindfulness.

78
00:11:19,000 --> 00:11:25,000
These are one description of reality.

79
00:11:25,000 --> 00:11:32,000
Mindfulness of the body, the feelings of mind, and the Dhamma.

80
00:11:32,000 --> 00:11:37,000
The Dhammas, the Dhammas in this case are Parma, the Dhammas.

81
00:11:37,000 --> 00:11:41,000
The Dhammas which are real.

82
00:11:41,000 --> 00:11:45,000
And when we focus on these, acknowledging, rising, falling.

83
00:11:45,000 --> 00:11:48,000
And the mind is focused on the rising, and on the falling.

84
00:11:48,000 --> 00:11:53,000
The focus on the feelings of pain, or aching, and so on.

85
00:11:53,000 --> 00:11:59,000
Focused on the mind, focused on the mind object, on the Dhammas.

86
00:11:59,000 --> 00:12:06,000
In the time of the Buddha there was one man who became a monk,

87
00:12:06,000 --> 00:12:11,000
simply to learn the magical powers of the Buddha.

88
00:12:11,000 --> 00:12:15,000
That the Lord Buddha was supposed to have.

89
00:12:15,000 --> 00:12:20,000
And then he was going to disrobe.

90
00:12:20,000 --> 00:12:30,000
He wanted to steal the fame of the Lord Buddha and bring it back to his own religious teachers once he had learned the magic.

91
00:12:30,000 --> 00:12:49,000
In the time of the Buddha there were many people who did this, trying to steal away the fame and the renown of the Lord Buddha by gaining the same magical powers which the Lord Buddha was supposed to have.

92
00:12:49,000 --> 00:13:04,000
And he once he ordained, he heard about there were some arahants in the area, some enlightened ones without oath, and I'll go ask them for the magical powers and the attainments which they have.

93
00:13:04,000 --> 00:13:25,000
And so he went to ask them, do you have this magical power or that magical power can you see far away or your far away?

94
00:13:25,000 --> 00:13:33,000
Do you have this attainment or that attainment and they kept saying no, no, no.

95
00:13:33,000 --> 00:13:37,000
So it's a special attainment they have, man.

96
00:13:37,000 --> 00:13:43,000
And he said, well then how can you claim to be free from suffering?

97
00:13:43,000 --> 00:13:47,000
A group of them they explained to him.

98
00:13:47,000 --> 00:13:56,000
He said, we are free from free through wisdom.

99
00:13:56,000 --> 00:14:10,000
The Lord Buddha is teaching me, there are two ways to become free, there are freedom through concentration and freedom through wisdom.

100
00:14:10,000 --> 00:14:18,000
Some people practice, some atari practice, tranquilizing the mind first and then they develop wisdom.

101
00:14:18,000 --> 00:14:27,000
Some people just practice to see impermanence suffering and not so.

102
00:14:27,000 --> 00:14:30,000
In this man said, I don't understand that meaning.

103
00:14:30,000 --> 00:14:35,000
I didn't understand the meaning of that and they said, what do you understand the meaning or not?

104
00:14:35,000 --> 00:14:38,000
We are free through wisdom.

105
00:14:38,000 --> 00:14:48,000
It means the concentration, which the gain was not enough to give magical powers or high spiritual attainment.

106
00:14:48,000 --> 00:14:53,000
But it was enough for them to give up greed, to give up anger, to give up delusion.

107
00:14:53,000 --> 00:14:55,000
Which is the true goal of the Buddha's teaching.

108
00:14:55,000 --> 00:15:06,000
Whether one gains magical powers or high spiritual attainment or peace and calm, which would lead to being reborn in the brahma realms.

109
00:15:06,000 --> 00:15:09,000
Whether one gains these things are not enough.

110
00:15:09,000 --> 00:15:15,000
The goal of the Buddha's teaching, the concentration, which we need is momentary concentration.

111
00:15:15,000 --> 00:15:24,000
It was at every moment we have to be focused on the present moment.

112
00:15:24,000 --> 00:15:31,000
This is the training and concentration, the training in wisdom.

113
00:15:31,000 --> 00:15:36,000
The training in wisdom is, of course, the wisdom is of two kinds.

114
00:15:36,000 --> 00:15:42,000
There is mundane wisdom and wisdom which leads to freedom from suffering.

115
00:15:42,000 --> 00:15:57,000
The mundane wisdom is the wisdom we use in the world to make money to keep our livelihood going.

116
00:15:57,000 --> 00:16:09,000
For monks and nuns, it is the wisdom which allows us to live our lives and to teach other people and so on.

117
00:16:09,000 --> 00:16:14,000
To help other people.

118
00:16:14,000 --> 00:16:26,000
The wisdom to study and to live, to keep the moral, to keep the precepts and so on.

119
00:16:26,000 --> 00:16:38,000
For lay people, it means the wisdom to live their lives properly in the world without making mistakes in their work.

120
00:16:38,000 --> 00:16:43,000
Knowing their duties, knowing their duties to their father and mother, their children,

121
00:16:43,000 --> 00:16:53,000
their duties to their friends and their teachers, their duties to their employees and so on.

122
00:16:53,000 --> 00:17:02,000
Knowing what are good things, what are bad things, knowing to stay away from bad friends.

123
00:17:02,000 --> 00:17:05,000
Knowing how to find good, what is a good friend and so on.

124
00:17:05,000 --> 00:17:07,000
This is always them in the world.

125
00:17:07,000 --> 00:17:09,000
Things which lead to peace and happiness.

126
00:17:09,000 --> 00:17:12,000
And the Lord Buddha also taught this kind of wisdom.

127
00:17:12,000 --> 00:17:22,000
Make no mistake that mundane morality, mundane concentration, mundane wisdom is the things which a Lord Buddha taught over and over again.

128
00:17:22,000 --> 00:17:32,000
And they are very important if we are going to develop the path which leads out of suffering.

129
00:17:32,000 --> 00:17:35,000
But they are never enough to lead us out of suffering.

130
00:17:35,000 --> 00:17:44,000
We simply have to be able to tell the difference and develop both mundane morality, concentration, wisdom,

131
00:17:44,000 --> 00:17:49,000
and super mundane morality concentration in wisdom.

132
00:17:49,000 --> 00:17:59,000
Super mundane wisdom or the wisdom which leads out of suffering is a very simple wisdom.

133
00:17:59,000 --> 00:18:11,000
One last teacher in Thailand said it was like frog wisdom, wisdom of a frog.

134
00:18:11,000 --> 00:18:19,000
Meaning that it is not some high intellectual wisdom.

135
00:18:19,000 --> 00:18:26,000
It is not the wisdom which you gain from going to university or studying or even memorizing the whole of the Buddha's teaching.

136
00:18:26,000 --> 00:18:30,000
The wisdom which comes when you sit and acknowledge rising and falling.

137
00:18:30,000 --> 00:18:33,000
Even knowing the right belly rising and knowing the belly falling.

138
00:18:33,000 --> 00:18:43,000
One time, the Lord Buddha said, knowing rising, one time, is of incredible benefit.

139
00:18:43,000 --> 00:18:50,000
It is a far more beneficial wisdom than memorizing the whole of the typical.

140
00:18:50,000 --> 00:18:56,000
When you know one time rise, the one time fall.

141
00:18:56,000 --> 00:19:00,000
Knowing the rising, knowing the falling, this is a far better wisdom.

142
00:19:00,000 --> 00:19:02,000
It is the wisdom of the present moment.

143
00:19:02,000 --> 00:19:05,000
It is a taste of the present moment.

144
00:19:05,000 --> 00:19:11,000
Even just one time when I was one to see what is the present moment.

145
00:19:11,000 --> 00:19:15,000
You should never be discouraged when we think we are not mindful all the time.

146
00:19:15,000 --> 00:19:18,000
Even being mindful one time rising and falling.

147
00:19:18,000 --> 00:19:21,000
Knowing the rising, knowing the falling, this is wisdom.

148
00:19:21,000 --> 00:19:30,000
It is the very base of the soil of understanding.

149
00:19:30,000 --> 00:19:36,000
It is because when we acknowledge rising and falling, we know the rising, knowing the falling.

150
00:19:36,000 --> 00:19:41,000
Afterwards, we will come to know impermanence, suffering and non-self.

151
00:19:41,000 --> 00:19:46,000
We will come to find the way out of suffering.

152
00:19:46,000 --> 00:19:57,000
We will come to see how can we be free from our attachments, free from our addictions, free from our cravings.

153
00:19:57,000 --> 00:20:02,000
How can we come to be truly content?

154
00:20:02,000 --> 00:20:06,000
We see how can we see impermanence.

155
00:20:06,000 --> 00:20:08,000
We won't hold on to things as per minute.

156
00:20:08,000 --> 00:20:12,000
When we see suffering, we won't hold on to things as pleasurable.

157
00:20:12,000 --> 00:20:17,000
When we see non-self, we won't hold on to things as self.

158
00:20:17,000 --> 00:20:20,000
We won't hold on to anything in the world.

159
00:20:20,000 --> 00:20:28,000
We won't attach to anything in the world.

160
00:20:28,000 --> 00:20:31,000
Not one thing.

161
00:20:31,000 --> 00:20:34,000
Not one thing.

162
00:20:34,000 --> 00:20:37,000
Nothing in the world.

163
00:20:37,000 --> 00:20:40,000
Nothing in the world.

164
00:20:40,000 --> 00:20:44,000
We won't attach to anything.

165
00:20:44,000 --> 00:20:46,000
We will attach to nothing.

166
00:20:46,000 --> 00:20:48,000
This is the way out of suffering.

167
00:20:48,000 --> 00:20:53,000
Then we will come to see the highest wisdom, which is the freedom from suffering.

168
00:20:53,000 --> 00:21:11,000
The realization of freedom from suffering is the highest because it frees one from attachment to the world.

169
00:21:11,000 --> 00:21:26,000
It allows one to see the true happiness and truth peace, so one never is confused and mistakenly trying to find happiness in the world.

170
00:21:26,000 --> 00:21:32,000
One will never try to find happiness in the world again.

171
00:21:32,000 --> 00:21:42,000
We will be able to let go of all things at all times because one knows truth peace and truth happiness.

172
00:21:42,000 --> 00:21:46,000
This is a respect for the training.

173
00:21:46,000 --> 00:21:55,000
Again, we are very slowly through this blessing, showing respect for the Buddhist teaching on respect.

174
00:21:55,000 --> 00:22:05,000
We will see how important it is.

