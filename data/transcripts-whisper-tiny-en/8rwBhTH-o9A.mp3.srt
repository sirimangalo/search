1
00:00:00,000 --> 00:00:14,460
Okay, so welcome everyone to monk radio on Sunday, March, something. So announcements just

2
00:00:14,460 --> 00:00:27,200
one announcement that we want to express our appreciation and gratitude and the encouragement

3
00:00:27,200 --> 00:00:32,560
that it brings to see people getting involved with our organization. We've started

4
00:00:32,560 --> 00:00:39,280
delegating and organizing people to keep the organization going and help with administrative

5
00:00:39,280 --> 00:00:46,320
tasks and various things that can be done over the internet. So we started a website

6
00:00:46,320 --> 00:00:54,880
called org.ceremungalow.org and that will have more detailed information about our organization

7
00:00:54,880 --> 00:01:01,880
or sort of administrative site. So people want to know how our organization works and how

8
00:01:01,880 --> 00:01:08,680
it runs. And it's great to see the best thing from my point of view is that it's going

9
00:01:08,680 --> 00:01:17,680
to give me more time personally to focus on teaching. And last time on running things,

10
00:01:17,680 --> 00:01:26,560
or delegating is always a good thing. The plan I think is to start doing more, get back

11
00:01:26,560 --> 00:01:32,680
to do more videos. I'm going to try to do daily teachings here. Just audio teachings

12
00:01:32,680 --> 00:01:39,240
in the evening and then maybe once or twice a week we'll make them upon the videos or

13
00:01:39,240 --> 00:01:47,560
maybe even some asgamunk videos out in the wilderness. One problem I have now is the cameras

14
00:01:47,560 --> 00:01:55,480
dying. The battery is not working and it's not switching modes and there's fungus growing

15
00:01:55,480 --> 00:02:02,040
in the lens. At least I have to get, if we're going to do any outdoor videos now I have

16
00:02:02,040 --> 00:02:10,920
to get a new battery for it. So I'll have to work on that. But anyway, good to thanks

17
00:02:10,920 --> 00:02:17,280
everyone and if anyone else is interested in getting involved in helping out to keep

18
00:02:17,280 --> 00:02:24,280
the organization going please do go to see our new site and please get involved.

