1
00:00:00,000 --> 00:00:22,320
Okay, good evening, everyone, welcome to a number, I'm reading an interesting article by a woman

2
00:00:22,320 --> 00:00:39,640
named Iris Young for Peace Study. It's called Throw Like A Girl, Throw Like A Girl,

3
00:00:39,640 --> 00:01:08,920
like a girl named a famous article, paper. And the subject is about gender. Based on the premise

4
00:01:08,920 --> 00:01:23,440
that we treat genders differently. We treat women different from the way we treat men

5
00:01:23,440 --> 00:01:33,320
and society and generally it's not an equal sort of distinction. So it's what we would

6
00:01:33,320 --> 00:01:43,640
call a sexist distinction, where one of the genders is favored and given more and better

7
00:01:43,640 --> 00:02:02,680
opportunities than the other. What's interesting about the article, for me, or Buddhist

8
00:02:02,680 --> 00:02:24,480
and the meditator. There's the way that it describes gender. It talks about femininity.

9
00:02:24,480 --> 00:02:44,480
Why is it that women throw like a girl? It turns out it's not physical. Women are mostly

10
00:02:44,480 --> 00:02:55,160
physically as this capable as men. But it's how they're taught and how they learn about

11
00:02:55,160 --> 00:03:04,640
them, how they perceive gender, how they perceive themselves. She says that women are taught

12
00:03:04,640 --> 00:03:23,720
to see themselves as objects, taught to see themselves as a thing, as an object of action,

13
00:03:23,720 --> 00:03:29,360
and not just actors, or as men are taught that they're actors. They're given unlimited

14
00:03:29,360 --> 00:03:41,320
potential. They think themselves mainly as the doer. Women are taught that they must also

15
00:03:41,320 --> 00:03:55,840
be observed and be aware of the fact that they will be observed and acted upon. As a

16
00:03:55,840 --> 00:04:14,400
result, they generally fail to live up to their potential as physically. Because they don't

17
00:04:14,400 --> 00:04:24,560
have the full confidence in their physical ability, they're for various reasons. They wind

18
00:04:24,560 --> 00:04:42,960
up throwing like a girl. What makes it so interesting is how one's mental attitude can first

19
00:04:42,960 --> 00:04:50,320
be shaped by the environment, shaped by the society in which one lives, whereby we see

20
00:04:50,320 --> 00:04:58,640
that much of who we think is who I am, like my gender, winds up being a social construct,

21
00:04:58,640 --> 00:05:05,600
turns out that I don't have to throw like a girl, just because I'm a girl. And also what

22
00:05:05,600 --> 00:05:16,640
that then does, what the perception we gender then does to our bodies, does to our experience.

23
00:05:16,640 --> 00:05:38,840
I'm credible it is that you can be crippled not by chains or prison, but you can be shackled

24
00:05:38,840 --> 00:05:50,920
by your own mind, which in turn is conditioned by so many different things including

25
00:05:50,920 --> 00:05:56,720
society. Being that we're not as free as we think we are and we're not as real as we

26
00:05:56,720 --> 00:06:08,040
think we are. As human beings, our personalities are made up of little more than habits

27
00:06:08,040 --> 00:06:22,760
whereas very little about who we are that is immutable. To some extent our physical gender

28
00:06:22,760 --> 00:06:33,480
what they call the sex is immutable. There are changes you can make, but you can't get rid

29
00:06:33,480 --> 00:06:43,040
of the distinction between chromosomes. That's some extent if you're born male you're always

30
00:06:43,040 --> 00:06:55,000
going to be x, y. If you're born female you're always going to be y, y, I think. Different

31
00:06:55,000 --> 00:07:00,320
chromosomes. And this is what Buddhism recognizes as gender. When we talk about gender

32
00:07:00,320 --> 00:07:07,580
we are talking about a physical phenomenon, a quality and derived quality of matter. It

33
00:07:07,580 --> 00:07:16,880
doesn't actually exist, but if you examine matter, the human form, physical body, you will

34
00:07:16,880 --> 00:07:26,320
see that there is a gender distinction. Women have a feminine, the iti, indria, iti indria,

35
00:07:26,320 --> 00:07:35,840
and men have the Buddhist indria. But how little, what little meaning that actually

36
00:07:35,840 --> 00:07:48,920
has in our lives compared to how society defines and how we define and how we as individuals

37
00:07:48,920 --> 00:07:58,120
meaning, you know, society, how we construct gender to the point that it becomes sexist

38
00:07:58,120 --> 00:08:11,720
and unequal and so on. And so this points to our task in meditation. It's not just as simple

39
00:08:11,720 --> 00:08:18,560
as turning a switch and becoming enlightened. It's about deconstructing and working

40
00:08:18,560 --> 00:08:44,840
through and untying all these tangles that the Buddha talked about. And we have the outer

41
00:08:44,840 --> 00:08:54,880
tangles influenced by the world around us. We become very much akin to the people in the

42
00:08:54,880 --> 00:09:04,160
situations that were surrounded by. We are personalities and just because we decided, hey,

43
00:09:04,160 --> 00:09:10,760
I think I'll be this sort of person as much to do with the sort of people and the relationships

44
00:09:10,760 --> 00:09:22,480
we have with other people around us. And so I think it's quite approach this text. It's

45
00:09:22,480 --> 00:09:32,840
not something foreign to our, it's very much to do with our, our task as meditators, gender,

46
00:09:32,840 --> 00:09:42,000
the deconstruction of gender. I think this whole idea of gender identity, well, understandable

47
00:09:42,000 --> 00:09:48,840
is misguided in the sense that people are trying to affirm their gender identity and they

48
00:09:48,840 --> 00:09:54,440
concern themselves a lot with gender identity. I mean, acknowledging the fact that some

49
00:09:54,440 --> 00:10:03,360
people who are physically male feel very feminine, very much like women mentally and that

50
00:10:03,360 --> 00:10:10,440
some people who are born women feel very much like men mentally. And to acknowledge that

51
00:10:10,440 --> 00:10:18,920
is important, to curious and interesting phenomenon in Buddhism when we would argue it.

52
00:10:18,920 --> 00:10:26,880
It's another issue. I mean, part of this idea that we have, we carry with us from life

53
00:10:26,880 --> 00:10:36,880
to life, some trace memories or inclinations based on our past lives. If you've been born

54
00:10:36,880 --> 00:10:40,960
one gender for a long time and suddenly are born the other gender, it's going to feel

55
00:10:40,960 --> 00:10:49,920
a pretty awkward and unfamiliar. It doesn't mean that you are mentally male or mentally

56
00:10:49,920 --> 00:11:00,040
female. It doesn't mean that there's any inherent reality to the makeup of your mind.

57
00:11:00,040 --> 00:11:03,960
I think if anything, what you can see through meditation is that men and women become very

58
00:11:03,960 --> 00:11:13,080
similar. They lose a lot of that, not all, generally, but there is a lot of what makes them

59
00:11:13,080 --> 00:11:22,080
masculine or feminine, except the physical. Besides looking like men and looking like women,

60
00:11:22,080 --> 00:11:29,480
mentally, this is something you see in the sitters often. In regards to the bikinis,

61
00:11:29,480 --> 00:11:35,560
tomorrow would often come to them and say, how can you become enlightened? You're just

62
00:11:35,560 --> 00:11:41,600
a woman with your two finger wisdom, two fingers is something to do with measuring or something.

63
00:11:41,600 --> 00:11:46,480
I don't know. There's some saying in India about two fingers is the wisdom of a woman

64
00:11:46,480 --> 00:11:52,840
because something to do with measuring, pinching rice or pinching spice or something. You're

65
00:11:52,840 --> 00:12:06,120
wisdom is just knowing how to cook, basically get back into the kitchen. They would say,

66
00:12:06,120 --> 00:12:17,160
for one who sees clearly, where is there a woman? To be clear, this idea of gender identity

67
00:12:17,160 --> 00:12:25,160
is really just ego. It's problematic. It's not to say we shouldn't be compassionate and

68
00:12:25,160 --> 00:12:31,280
understanding of people who feel what we call gender dysphoria. This isn't about prejudice,

69
00:12:31,280 --> 00:12:35,880
but it's about understanding that identity and of any sort is not good. It's just a claim

70
00:12:35,880 --> 00:12:45,160
and it's nothing to the gender. That's really the heart of what I'm pointing at here is that

71
00:12:45,160 --> 00:12:54,320
when we identify, when we reify something, if I don't know if there's even a word, we turn

72
00:12:54,320 --> 00:13:08,400
a thing into an identity. I am male. I am female. When we identify with it, then it becomes

73
00:13:08,400 --> 00:13:14,520
immutable. Then it becomes real. The truth behind these things is that they're merely habits.

74
00:13:14,520 --> 00:13:22,800
They're merely a consequence of patterns of behavior, patterns of interaction, patterns

75
00:13:22,800 --> 00:13:30,280
of experience, or by we've been indoctrinated and cultured into believing we are this sort

76
00:13:30,280 --> 00:13:37,080
of person. This goes for everything. Gender is just a very curious and extreme example of it,

77
00:13:37,080 --> 00:13:43,360
but it goes with our habits of addiction and aversion, our personality, everything that

78
00:13:43,360 --> 00:13:49,240
makes us who we are and that we identify with and say, yeah, that's me. When people say,

79
00:13:49,240 --> 00:13:57,720
oh, that you're the dumbo, he's like this and like that. People think of me like this,

80
00:13:57,720 --> 00:14:07,040
think of me like that. All of us, we have this. You build this up on the internet. You build

81
00:14:07,040 --> 00:14:24,080
up an internet personality here in second life. Personality believes. The idea that these

82
00:14:24,080 --> 00:14:28,520
things actually exist, that's wrong with you. It's the first one of the first things to go

83
00:14:28,520 --> 00:14:36,680
with the experience and the realization of sotdapana. Once you've seen nibana, you realize

84
00:14:36,680 --> 00:14:46,400
none of this is actually immutable. How it changes and the ways in which it changes is up

85
00:14:46,400 --> 00:14:55,680
for question, but that it changes is not up for question. Everything about who we are is just

86
00:14:55,680 --> 00:15:06,800
a consequence of our particular and exact position in samsara. There's nothing immutable or

87
00:15:06,800 --> 00:15:12,920
unchangeable or real about it beyond it being an experience and a sequence and a series

88
00:15:12,920 --> 00:15:23,000
and a pattern of experience. We have for a while and that changes over time and eventually

89
00:15:23,000 --> 00:15:31,760
fade away through replaced by something else. Our job as meditators is really just to see

90
00:15:31,760 --> 00:15:38,200
through this so that we don't clean, so that we don't get attached to I am this, I am that

91
00:15:38,200 --> 00:15:54,000
because when it changes, we then aren't left in a lurch unable to cope. Because that's

92
00:15:54,000 --> 00:16:04,560
the thing. There are women who are able to free themselves from this oppressive prejudice

93
00:16:04,560 --> 00:16:14,520
and the system of limiting one's potential by one's perceived gender. They will break

94
00:16:14,520 --> 00:16:26,240
free from it mentally and though physically they're still women and even though they may be

95
00:16:26,240 --> 00:16:34,920
subject to attacks and subject to discrimination, they can free themselves mentally of it

96
00:16:34,920 --> 00:16:43,200
and not have any sense, not suffer from it. In the same way that one who takes it on

97
00:16:43,200 --> 00:16:50,240
and takes on a role, is curious for all the femininity that makes one throw like a girl.

98
00:16:50,240 --> 00:16:57,240
It's a silly example in Buddhism or none of us. I mean I probably throw like a girl

99
00:16:57,240 --> 00:17:07,880
from which it's worth. But we become indoctrinated in thinking I am this sort of person

100
00:17:07,880 --> 00:17:15,360
I am that sort of breathing. You see this a lot. I see this a lot in Asia in more traditional

101
00:17:15,360 --> 00:17:24,160
societies. I don't see it quite so much in the West. But in traditional, very traditional

102
00:17:24,160 --> 00:17:30,440
societies it's blatantly obvious that men have specific roles, women have specific roles

103
00:17:30,440 --> 00:17:37,440
and people are so brainwashed into thinking monks have specific roles. That's the worst.

104
00:17:37,440 --> 00:17:43,760
The roles that monks have been given is the one I felt most acutely because it didn't

105
00:17:43,760 --> 00:17:52,120
jive with my understanding of what a monk was. And I realized that this was culture.

106
00:17:52,120 --> 00:18:00,840
This is what culture is. It's the building up of construct. Some can be good. Some

107
00:18:00,840 --> 00:18:07,760
constructs are useful. But when the constructs replace reality, when you take the constructs

108
00:18:07,760 --> 00:18:16,240
as being an ultimate real and important and meaningful entity, then you've gotten

109
00:18:16,240 --> 00:18:31,960
you've fallen into wrong view, fallen into personality belief, that sort of thing.

110
00:18:31,960 --> 00:18:36,440
So it's quite simple. I mean there's not a lot to say as meditators. But there's a lot

111
00:18:36,440 --> 00:18:43,640
of work to be done to deconstruct who we are. And the first step is making this shift,

112
00:18:43,640 --> 00:18:48,560
this realization that we're not a person, we're not a human, we're not a being, we don't

113
00:18:48,560 --> 00:18:58,840
have a personality. All we are is a hodgepodge, a mishmash, a mess of habits, a mess

114
00:18:58,840 --> 00:19:04,040
of patterns of behavior that we've developed over time and that have been impressed upon

115
00:19:04,040 --> 00:19:13,520
us by our environment. And that's the most liberating. And that's the, the primary

116
00:19:13,520 --> 00:19:22,040
liberating realization. It's not the end of course. But it's what ignites the spark,

117
00:19:22,040 --> 00:19:27,640
realization that I can change who I am. Who I am is changeable. I'm not stuck, I'm not

118
00:19:27,640 --> 00:19:37,800
doomed. I'm not resigned to my fate. And all that comes next is trying to find the

119
00:19:37,800 --> 00:19:42,800
direction to go. Which way will I change myself? What is good about me? What is bad

120
00:19:42,800 --> 00:19:49,320
about me? Really? Not because society tells me or not because I believe it. What is really

121
00:19:49,320 --> 00:20:00,640
good and bad? And then it's about building the good and deconstructing the bad. And leaving,

122
00:20:00,640 --> 00:20:11,040
leaving alone, would just neither good nor bad. So some thoughts on habits in general and

123
00:20:11,040 --> 00:20:15,880
personalities in general and gender in specific. It's an interesting topic, it's one that

124
00:20:15,880 --> 00:20:24,200
comes up a lot because Buddhist cultures can be quite misogynistic as well. And Buddhism

125
00:20:24,200 --> 00:20:33,360
itself has some problems institutionally and even the texts have some dilemmas that we

126
00:20:33,360 --> 00:20:39,720
might very easily want to say our sexes. And then we'd be like, Buddha couldn't be

127
00:20:39,720 --> 00:20:46,000
sexist, could he? And then we wonder, were these texts actually written by the Buddha? Or

128
00:20:46,000 --> 00:20:50,760
is there a way to explain them that makes them not sexist? Certainly we don't want to believe

129
00:20:50,760 --> 00:20:58,240
that the Buddha was sexist. It may very well be that the Buddha saw this was the state.

130
00:20:58,240 --> 00:21:05,240
If you want to try and excuse some of the gender disparity in Buddhism, even in the early

131
00:21:05,240 --> 00:21:12,200
texts, I would say that he saw the state of women and thought, they do have special

132
00:21:12,200 --> 00:21:21,160
problems as my Thai counter, Thai monk friends explained to me, women have special problems.

133
00:21:21,160 --> 00:21:30,840
It could be seen as true in a conventional sense that in cultural and traditional societies,

134
00:21:30,840 --> 00:21:38,840
women do have special problems that may make it harder for them to become enlightened.

135
00:21:38,840 --> 00:21:44,280
Hard to really be convinced by when men have their own special problems as well that

136
00:21:44,280 --> 00:21:51,880
make them far less than many cases inclined to meditate. So, it's an interesting argument,

137
00:21:51,880 --> 00:21:57,120
though. Either way, gender is important, it's important for us to be clear what we mean

138
00:21:57,120 --> 00:22:02,320
by it, that in Buddhism we don't have a sense that women are inherently different.

139
00:22:02,320 --> 00:22:09,720
From men, besides the very, very small, insignificant fact that their bodies are a little

140
00:22:09,720 --> 00:22:17,840
bit different than bodies, women's bodies, but it's only physical. Mentally, mentally it depends

141
00:22:17,840 --> 00:22:25,000
on the individual. It depends on the culture, depends on the circumstance which can be changed

142
00:22:25,000 --> 00:22:35,360
and should be understood to be mutable, changeable. So, there you go. Some thoughts, some

143
00:22:35,360 --> 00:23:02,720
are done for tonight. Thank you all for tuning in and have a good night.

