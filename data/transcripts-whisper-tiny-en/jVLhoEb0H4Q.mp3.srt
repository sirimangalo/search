1
00:00:00,000 --> 00:00:05,200
How much do you have to note in your daily life when you take a cup you can take you note taking

2
00:00:05,440 --> 00:00:10,180
But you can also note more like stretching taking bringing so how much do you have to note?

3
00:00:10,920 --> 00:00:12,920
You don't have to note anything

4
00:00:13,280 --> 00:00:19,820
But every time that you note you create clarity in your mind your mind you're creating a habit of clarity a

5
00:00:20,320 --> 00:00:21,480
habit of

6
00:00:21,480 --> 00:00:23,480
Mindfulness in the mind

7
00:00:23,480 --> 00:00:28,960
Every time that you do it, you're you're adjusting your mind your mind is becoming clearer if you do it correctly

8
00:00:29,680 --> 00:00:34,200
If you really become skillful at it, the mind becomes altered

9
00:00:34,680 --> 00:00:39,440
Your mind becomes clearer simpler that mine state becomes straight and

10
00:00:40,600 --> 00:00:42,400
clear

11
00:00:42,400 --> 00:00:44,400
So the more you can do it the better

12
00:00:44,800 --> 00:00:46,160
reaching

13
00:00:46,160 --> 00:00:49,440
grasping lifting no we used to do

14
00:00:49,440 --> 00:00:52,600
There's one place they taught us very very

15
00:00:53,440 --> 00:00:54,480
Specifically

16
00:00:54,480 --> 00:00:56,720
First before you even take your food

17
00:00:57,120 --> 00:00:59,440
Seeing if you smell the food smelling

18
00:01:00,080 --> 00:01:02,800
Wanting to take wanting to take moving

19
00:01:03,440 --> 00:01:10,160
Scoop being raising closing your eyes closing opening him up opening placing on the tongue placing closing

20
00:01:10,440 --> 00:01:12,440
closing lowering touching

21
00:01:13,440 --> 00:01:14,640
chewing

22
00:01:14,640 --> 00:01:15,680
chewing

23
00:01:15,680 --> 00:01:16,880
chewing

24
00:01:16,880 --> 00:01:24,200
And until it becomes liquefied and swallowing and then again chewing and again and again

25
00:01:25,000 --> 00:01:27,000
That's great if you could do it

26
00:01:27,560 --> 00:01:37,120
It gets to be too much when you're looking for things to to be mindful of or when your concentration is not strong enough and you're forcing yourself to do it

27
00:01:37,280 --> 00:01:39,280
Because it should come naturally

28
00:01:39,280 --> 00:01:46,400
No, not not by itself, but it should be something that you can coax the mind to do

29
00:01:47,080 --> 00:01:52,480
Bring the mind to do again and again and when the mind gets off bring it back and try again

30
00:01:53,960 --> 00:02:00,720
So the more in general for most of us the more you can do the better and the more you're able to do

31
00:02:00,720 --> 00:02:06,760
The the better it will be for you until a certain point

32
00:02:09,440 --> 00:02:15,240
But what you wouldn't want is to feel bad when you're not mindful because that's more anger or

33
00:02:16,200 --> 00:02:18,440
Feel guilty whether you're not mindful enough

34
00:02:18,720 --> 00:02:23,680
You should think of it as a good thing and something like something special you can give to yourself and the more

35
00:02:23,840 --> 00:02:26,320
You can do it the more special you can give to yourself

36
00:02:26,320 --> 00:02:31,660
And that's I think the most wholesome that encourages you to make you want to do it more

