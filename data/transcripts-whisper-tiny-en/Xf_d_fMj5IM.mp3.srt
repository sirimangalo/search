1
00:00:00,000 --> 00:00:05,280
Hello and welcome back to our study of the Dhamapada. Today we continue on with

2
00:00:05,280 --> 00:00:30,960
first one of six which read as follows.

3
00:00:30,960 --> 00:00:42,440
Which means one can give 1,000 a month after month means 1,000 here is just

4
00:00:42,440 --> 00:00:48,120
lying for 1,000 pieces of money or we would think a thousand dollars 1,000

5
00:00:48,120 --> 00:00:52,480
pieces of money. 1,000 gold coins.

6
00:00:52,480 --> 00:01:05,480
1,000 for a hundred years. 1 can sacrifice. Every month after month, 1,000 every

7
00:01:05,480 --> 00:01:09,400
month for a hundred years.

8
00:01:09,400 --> 00:01:19,380
2,000 a kang jah. 1 time. A kang jah muhu tambi. Not 1 time. 1 moment. Bhuy jah. If 1

9
00:01:19,380 --> 00:01:29,820
should pay or pay homage for 1 moment. Bha wita panang. 2,1 who is self-controlled

10
00:01:29,820 --> 00:01:40,140
or not controlled, who is of developed self, who is self-developed or cultivated.

11
00:01:40,140 --> 00:01:47,740
Sayyewa pujana seyo, this puja is greater.

12
00:01:47,740 --> 00:01:59,020
Yanjay was a satangutana, greater than the 100 years sacrifice,

13
00:01:59,020 --> 00:02:08,140
the 100 years of sacrifice.

14
00:02:08,140 --> 00:02:14,460
So this verse was spoken in regards to Sari puja's uncle.

15
00:02:14,460 --> 00:02:18,940
We're told that Sari puja had an uncle who was a brahman, Sari puja's whole family,

16
00:02:18,940 --> 00:02:20,460
were brahmanas.

17
00:02:20,460 --> 00:02:26,300
And before the Sari puja passed away, he actually went back to teach his mother,

18
00:02:26,300 --> 00:02:34,140
who was still of wrong views, still had the idea that the puja tat was wrong,

19
00:02:34,140 --> 00:02:40,540
and Sari puja had lost his way by becoming a Buddhist monk.

20
00:02:40,540 --> 00:02:46,380
And so we have that story elsewhere, but here we have a story of an uncle that is,

21
00:02:46,380 --> 00:02:53,180
who is a brahman, who also had wrong views.

22
00:02:53,180 --> 00:02:58,300
And so it's kind of a funny, it's a very short story, but it's kind of a funny little story.

23
00:02:58,300 --> 00:03:03,100
Sari puja went to see his uncle and asked him,

24
00:03:03,100 --> 00:03:07,660
straight up, do you ever do any good deeds?

25
00:03:07,660 --> 00:03:10,540
And his uncle right away said, oh yes, actually.

26
00:03:10,540 --> 00:03:21,020
Every month I give or per month I give what amounts to a thousand gold coins worth of offerings.

27
00:03:21,020 --> 00:03:26,140
I give offerings amounting to a thousand gold coins a month.

28
00:03:26,140 --> 00:03:32,380
And he says, okay, who do you give them? Who do you give this to?

29
00:03:32,380 --> 00:03:36,060
And I give it to the naked aesthetics.

30
00:03:36,060 --> 00:03:43,340
These guys who, as their religious practice, take to not wearing clothes,

31
00:03:43,340 --> 00:03:48,540
because somehow that's meaningful to get considered to be

32
00:03:48,540 --> 00:03:52,540
more hardcore. It is pretty hardcore.

33
00:03:52,540 --> 00:03:55,980
If you want to be go extreme, not wearing clothes is the way to go,

34
00:03:55,980 --> 00:04:01,180
because it means in the sun you're at the mercy of the sun and the

35
00:04:01,180 --> 00:04:06,540
mercy of the cold, at the mercy of the flies and the insects.

36
00:04:06,540 --> 00:04:10,380
Couldn't imagine doing that in a place where there were mosquitoes.

37
00:04:10,380 --> 00:04:18,300
And certainly makes you stand out, makes it

38
00:04:18,300 --> 00:04:23,740
difficult for you to fit in in society.

39
00:04:25,980 --> 00:04:28,940
But these were the people that he was paying respect to.

40
00:04:28,940 --> 00:04:34,620
And Sari puja says, well, why do you

41
00:04:34,620 --> 00:04:36,620
what do you hope to get from that?

42
00:04:36,620 --> 00:04:39,340
Okay, so you give money to these naked aesthetics.

43
00:04:39,340 --> 00:04:40,620
What do you hope to get from that?

44
00:04:40,620 --> 00:04:45,980
He said, oh, well, because of this, my aspiration is to go to the

45
00:04:45,980 --> 00:04:48,780
Brahma realms.

46
00:04:51,020 --> 00:04:56,220
Sari puja asks, was that the way to go to the Brahma realms?

47
00:04:56,220 --> 00:04:59,660
And he says, oh, yes, I'm smoothly.

48
00:04:59,660 --> 00:05:02,140
And he said, okay, where did you hear this?

49
00:05:02,140 --> 00:05:06,860
He says, oh, my teachers, the naked aesthetics told me,

50
00:05:06,860 --> 00:05:11,660
which is kind of how you get, what you get in all religions.

51
00:05:11,660 --> 00:05:18,060
The teachers will tell you, if you give to us, it is of greatest benefit.

52
00:05:18,060 --> 00:05:19,660
It's actually an interesting story.

53
00:05:19,660 --> 00:05:27,740
And in some ways, it's a bit, well, it's not one of my favorite stories.

54
00:05:27,740 --> 00:05:32,540
But I think we have to talk about this and sort of come to an understanding

55
00:05:32,540 --> 00:05:34,620
of what's meant here.

56
00:05:34,620 --> 00:05:37,420
I think one way you can look at this, and of course,

57
00:05:37,420 --> 00:05:40,220
it's this is me finessing it.

58
00:05:40,220 --> 00:05:44,300
But, okay, so the story goes.

59
00:05:44,300 --> 00:05:46,620
Sari puja shakes his head and says,

60
00:05:46,620 --> 00:05:50,140
Brahmin, that's not the way, well, we all know that's not the way the Brahma

61
00:05:50,140 --> 00:05:50,460
realms.

62
00:05:50,460 --> 00:05:54,220
And he says, you're not on the path to the Brahma realms,

63
00:05:54,220 --> 00:05:56,780
and your teachers don't know the way to the Brahma realms.

64
00:05:56,780 --> 00:06:01,900
You don't know, and neither your teachers know the way to the world of God,

65
00:06:01,900 --> 00:06:04,460
basically.

66
00:06:04,460 --> 00:06:08,780
And he says, the Buddha is the only one who knows.

67
00:06:08,780 --> 00:06:09,900
So come and see the Buddha.

68
00:06:09,900 --> 00:06:12,700
Come with me, and I will ask him to tell you the way to Brahma.

69
00:06:16,860 --> 00:06:21,900
So the idea here is that, I mean, the problem here is that

70
00:06:21,900 --> 00:06:23,820
it's just still just an appeal out to authority.

71
00:06:23,820 --> 00:06:26,860
Who says the Buddha is going to know better than these guys?

72
00:06:26,860 --> 00:06:31,100
If they say X and the Buddha says Y, how do you tell which one is right

73
00:06:31,100 --> 00:06:32,700
and which one is wrong?

74
00:06:32,700 --> 00:06:39,580
So he goes to the Buddha, and I think you have to take it.

75
00:06:39,580 --> 00:06:42,380
We have to see exactly what's being said here.

76
00:06:42,380 --> 00:06:45,580
And of course, the story, the actual words of the story,

77
00:06:45,580 --> 00:06:47,740
may just be an abbreviation.

78
00:06:47,740 --> 00:06:51,260
But basically, the Buddha says to him,

79
00:06:51,260 --> 00:06:56,140
giving to those guys is worthless, giving to monks in my religion.

80
00:06:56,140 --> 00:07:00,060
That's what will lead you to, or that's

81
00:07:00,060 --> 00:07:06,140
a worth 1,000 times, or it's not worth the

82
00:07:06,140 --> 00:07:13,980
thousandth part of simply looking at,

83
00:07:13,980 --> 00:07:19,500
simply looking at one of my disciples,

84
00:07:19,500 --> 00:07:22,860
and then he teaches this verse.

85
00:07:22,860 --> 00:07:26,780
And so you kind of get the sense, well, you know,

86
00:07:26,780 --> 00:07:29,020
it's just the Buddha saying the same thing as everyone else.

87
00:07:29,020 --> 00:07:32,220
If you give to my disciples.

88
00:07:32,220 --> 00:07:35,420
But first of all, you would have to agree.

89
00:07:35,420 --> 00:07:37,580
If you follow the Buddha's teaching,

90
00:07:37,580 --> 00:07:43,340
if you have some understanding of the idea of karma,

91
00:07:43,340 --> 00:07:44,380
then you'd have to agree.

92
00:07:44,380 --> 00:07:46,940
Well, you look at how the Buddha's monks practice,

93
00:07:46,940 --> 00:07:49,260
and you'd say we're not just the Buddhist monks,

94
00:07:49,260 --> 00:07:51,260
but people who follow the Buddha's teaching.

95
00:07:51,260 --> 00:07:52,700
You look at how they practice, and you say,

96
00:07:52,700 --> 00:07:55,180
wow, he has giving to them its own great fruit.

97
00:07:55,180 --> 00:07:57,500
But of course, we believe that as Buddhists.

98
00:07:57,500 --> 00:08:02,060
The question is, how do we explain it beyond just

99
00:08:02,060 --> 00:08:05,500
making it sound like we're promoting ourselves?

100
00:08:05,500 --> 00:08:08,220
Because it really does just sound like self-promotion.

101
00:08:08,220 --> 00:08:12,460
And as I've said before, some of these verses,

102
00:08:12,460 --> 00:08:15,580
are not the verses, but some, more of the stories

103
00:08:15,580 --> 00:08:19,740
tend to be self-aggrandizing or self-promoting.

104
00:08:19,740 --> 00:08:24,140
So in a sense, you kind of have to,

105
00:08:24,140 --> 00:08:26,540
well, maybe not even, maybe not necessarily be skeptical

106
00:08:26,540 --> 00:08:29,500
yourself, but you have to accept that people are going to look

107
00:08:29,500 --> 00:08:32,860
at this and say, this is just like religion always is.

108
00:08:32,860 --> 00:08:36,140
It's all about giving, giving, giving.

109
00:08:36,140 --> 00:08:40,220
Because some of the verses are about giving

110
00:08:40,220 --> 00:08:45,900
and tend to be, or the stories, tend to be

111
00:08:45,900 --> 00:08:50,460
instructive in regards to giving to the monks,

112
00:08:50,460 --> 00:08:56,380
giving to the Buddhist religion being the best way.

113
00:08:56,380 --> 00:09:00,700
But the saving grace, if it needs one,

114
00:09:00,700 --> 00:09:04,620
comes from the verse itself because the verse doesn't actually

115
00:09:04,620 --> 00:09:06,860
say that.

116
00:09:06,860 --> 00:09:09,100
The verse makes the important distinction

117
00:09:09,100 --> 00:09:11,420
that we would all make as Buddhists,

118
00:09:11,420 --> 00:09:15,660
that it doesn't actually matter what religion a person is

119
00:09:15,660 --> 00:09:20,060
in or even what teachings they follow necessarily.

120
00:09:20,060 --> 00:09:24,220
It's that they are bawitata, which means they have,

121
00:09:24,220 --> 00:09:27,660
they are self-cultivated.

122
00:09:27,660 --> 00:09:31,180
I mean, that of course has to be qualified as well.

123
00:09:31,180 --> 00:09:34,060
What kind of cultivation do you mean?

124
00:09:34,060 --> 00:09:38,300
But it means enlightened if a person is truly,

125
00:09:38,300 --> 00:09:43,180
has truly done something to deserve a gift

126
00:09:43,180 --> 00:09:46,060
then it's a greater fruit.

127
00:09:46,060 --> 00:09:51,580
In Buddhism, we recognize four kinds of four aspects.

128
00:09:51,580 --> 00:09:59,100
There is the, well, the two, there is the, sorry, three aspects.

129
00:09:59,100 --> 00:10:06,140
The purity of the giver, the purity of the recipient

130
00:10:06,140 --> 00:10:09,340
and the purity of the gift.

131
00:10:09,340 --> 00:10:17,580
So if the gift is pure, then it's considered to be a greater benefit,

132
00:10:17,580 --> 00:10:20,140
a greater fruit than if the gift is impure.

133
00:10:20,140 --> 00:10:23,500
So for example, if it's giving alcohol,

134
00:10:23,500 --> 00:10:28,620
or if it's a gift of poison, then that would be a less beneficial gift.

135
00:10:28,620 --> 00:10:35,340
But if it's a gift of food or a gift of shelter or a gift of medicine

136
00:10:35,340 --> 00:10:38,700
or a gift of something like clothing,

137
00:10:38,700 --> 00:10:47,180
then that would be of greater benefit

138
00:10:47,180 --> 00:10:51,340
then that would be of a greater benefit.

139
00:10:51,340 --> 00:10:55,740
Or if it's a gift of dhamma, a gift of truth,

140
00:10:55,740 --> 00:11:09,500
then that would be a most benefit.

141
00:11:09,500 --> 00:11:12,060
And if the person is, if the giver is pure,

142
00:11:12,060 --> 00:11:14,060
so if the person giving is a moral person,

143
00:11:14,060 --> 00:11:17,660
is an ethical person, is a developed person,

144
00:11:17,660 --> 00:11:20,060
then of course their intentions are going to be pure.

145
00:11:20,060 --> 00:11:23,660
And so the gift is purified in that way.

146
00:11:23,660 --> 00:11:26,860
If the recipient is pure,

147
00:11:26,860 --> 00:11:30,620
then it's better than if the recipient is a moral,

148
00:11:30,620 --> 00:11:35,500
unwholesome, uncultivated individual.

149
00:11:35,500 --> 00:11:39,260
So that's the sort of the detail teaching here.

150
00:11:39,260 --> 00:11:43,340
Basically it's a teaching about

151
00:11:43,340 --> 00:11:45,580
where we should pay homage and respect,

152
00:11:45,580 --> 00:11:49,340
who we should respect. And so it's not a deep teaching,

153
00:11:49,340 --> 00:11:52,380
it's not the kind of teaching that you

154
00:11:52,380 --> 00:11:53,900
would say, well, if I follow this,

155
00:11:53,900 --> 00:11:55,100
I'm going to become enlightened.

156
00:11:55,100 --> 00:11:57,100
How does it relate to our meditation practice?

157
00:11:57,100 --> 00:12:06,620
Well, there is a sense that our practice of charity

158
00:12:06,620 --> 00:12:09,420
is supportive of our meditation practice,

159
00:12:09,420 --> 00:12:11,260
even as Buddhist monks.

160
00:12:11,260 --> 00:12:13,980
So giving the dhamma, teaching the dhamma,

161
00:12:13,980 --> 00:12:21,180
is a kind of a gift and it's a support of the practice of the person teaching.

162
00:12:21,180 --> 00:12:28,060
And for those people who have monetary resources,

163
00:12:28,060 --> 00:12:30,940
giving in this way means of support.

164
00:12:30,940 --> 00:12:32,860
And it is a question that comes up.

165
00:12:32,860 --> 00:12:34,060
What do I do with my money?

166
00:12:34,060 --> 00:12:37,980
I have this money and part of it,

167
00:12:37,980 --> 00:12:42,540
I want to give to charity in order to support my own spiritual development.

168
00:12:42,540 --> 00:12:47,180
I want to be kind and generous and supportive of good things.

169
00:12:47,180 --> 00:12:49,580
So what are the good things I should support?

170
00:12:49,580 --> 00:12:50,940
Should I support these guys who are

171
00:12:50,940 --> 00:12:53,980
going naked or should I support these guys who are

172
00:12:53,980 --> 00:12:58,060
cultivating insight meditation?

173
00:12:58,060 --> 00:13:03,900
And as Buddhist, we would tend to agree that going naked isn't really all that.

174
00:13:03,900 --> 00:13:04,860
A great benefit.

175
00:13:04,860 --> 00:13:07,820
And in fact, we're going to get fairly soon

176
00:13:07,820 --> 00:13:11,100
talking about people going naked and so on.

177
00:13:11,100 --> 00:13:12,620
Or I think actually we've had the verse.

178
00:13:12,620 --> 00:13:21,740
We've had the verse about the man who pretended to be an ascetic

179
00:13:21,740 --> 00:13:23,420
and it's something about month after month.

180
00:13:27,580 --> 00:13:32,540
But it is important that our livelihood

181
00:13:32,540 --> 00:13:39,500
and the other rest of our life, our activity in the world these fear,

182
00:13:39,500 --> 00:13:44,780
does conducive to meditation, practice, and development.

183
00:13:44,780 --> 00:13:47,420
And so this is a question that comes up.

184
00:13:47,420 --> 00:13:53,180
How do we interact with spiritual people?

185
00:13:53,180 --> 00:13:54,860
What do we do with our resources?

186
00:13:54,860 --> 00:13:57,500
Where are our resources best spent?

187
00:13:57,500 --> 00:14:03,740
And it's not just monetary resources, it's our energy, our intellect,

188
00:14:03,740 --> 00:14:11,100
our study, where are we best to engage ourselves?

189
00:14:11,100 --> 00:14:16,220
Not just in meditation but in the rest of our life because this verse doesn't actually

190
00:14:16,220 --> 00:14:21,420
deal with meditation except in so far as it talks about the recipient

191
00:14:21,420 --> 00:14:30,780
as being really only a factor in regards to their own

192
00:14:30,780 --> 00:14:36,620
that person's spiritual development. It's not just because they are a placeholder,

193
00:14:36,620 --> 00:14:41,820
like someone who becomes a priest and you give them because they are in a place

194
00:14:41,820 --> 00:14:46,140
or a person who becomes a monk and you give to them

195
00:14:46,140 --> 00:14:50,140
because they are a monk. This person is a monk and you give to them.

196
00:14:50,140 --> 00:14:56,540
It's not actually considered to be very great fruit just because the person is a Buddhist monk.

197
00:14:56,540 --> 00:14:59,740
There is something there. You think this is a representative of the sangha.

198
00:14:59,740 --> 00:15:05,020
I'm giving it to the Buddhist religion but there's none of the sense

199
00:15:05,020 --> 00:15:08,060
unless they are a developed person, someone who is cultivated

200
00:15:08,060 --> 00:15:15,020
meditation, someone who has done something worthy and therefore is

201
00:15:15,020 --> 00:15:24,540
a person whose actions and whose support,

202
00:15:24,540 --> 00:15:33,260
the energy and the power, the strength that comes from supporting such a person

203
00:15:33,260 --> 00:15:36,300
will go to good things for themselves and for others.

204
00:15:36,300 --> 00:15:43,660
Will not go to hurt others or hurt themselves, will not be wasted.

205
00:15:43,660 --> 00:15:48,700
So as meditators, as Buddhists, this is the sort of thing we have to think about.

206
00:15:48,700 --> 00:15:55,420
It's very clear and simple. Maybe it's a little abbreviated to abbreviated

207
00:15:55,420 --> 00:15:57,580
because actually there's more to it than that.

208
00:15:57,580 --> 00:16:01,580
You can't just give to an enlightened being and hope that it's going to

209
00:16:01,580 --> 00:16:05,740
bring good benefit. You yourself have to be mindful. You yourself have to be

210
00:16:05,740 --> 00:16:10,220
conscious of why you're giving and giving for the right reasons.

211
00:16:10,220 --> 00:16:14,700
You can't be giving to brag to others and so on. I mean you can but it's of less

212
00:16:14,700 --> 00:16:20,300
fruit and you can't just give them anything, giving them

213
00:16:20,300 --> 00:16:26,300
people often like to give money which is what was talked about in this verse.

214
00:16:26,300 --> 00:16:33,740
Money is actually when I was in Thailand there's a big thing because most monks use

215
00:16:33,740 --> 00:16:37,500
money and so it was a sense that what people want to give you, why aren't you

216
00:16:37,500 --> 00:16:43,500
accepting money? They want to give and you're preventing them from

217
00:16:43,500 --> 00:16:47,340
doing this good deed and I said well you know giving money is actually a fairly

218
00:16:47,340 --> 00:16:53,580
poor gift to give because then you're saying the other person go into it

219
00:16:53,580 --> 00:16:58,940
yourself. Go and get it yourself as opposed to bringing the person something

220
00:16:58,940 --> 00:17:03,180
beneficial, something that they can actually use. You can't eat food. It's very

221
00:17:03,180 --> 00:17:08,140
difficult to wear food. You can't use it as medicine. It's actually quite

222
00:17:08,140 --> 00:17:14,300
useless. It's just paper and moreover it's a dangerous thing because you give

223
00:17:14,300 --> 00:17:18,700
money and you force the person to think about even if their mind is

224
00:17:18,700 --> 00:17:21,580
pure, you force them to think about doing this, doing that.

225
00:17:21,580 --> 00:17:25,180
Where am I going to go? What am I going to get? I'm supposed to bring

226
00:17:25,180 --> 00:17:29,980
them food, thinking it out for them. It's a very much more powerful gift to give

227
00:17:29,980 --> 00:17:34,860
something that they will actually use. Some people have gone to the

228
00:17:34,860 --> 00:17:39,740
convenience of, at the holidays just giving money right or

229
00:17:39,740 --> 00:17:43,980
gift certificates or something but it's not the same as actually putting some

230
00:17:43,980 --> 00:17:47,420
thought into it and saying well this person could use that or

231
00:17:47,420 --> 00:17:53,740
or even better asking them what they need is there anything you saw it.

232
00:17:53,740 --> 00:17:56,540
But type people have the sense that you can't ask a monk what they need

233
00:17:56,540 --> 00:18:00,780
and so on anyway. But it's not just about giving to

234
00:18:00,780 --> 00:18:05,260
monks. I mean if you give to a person on the side of the road who's hungry

235
00:18:05,260 --> 00:18:10,300
that's a great thing. If you give them a meal to eat

236
00:18:10,300 --> 00:18:13,500
or ask them if there's anything they need or you help them

237
00:18:13,500 --> 00:18:18,700
go to the doctor's or so and it helps them find a job.

238
00:18:18,700 --> 00:18:22,540
All great things and very powerful things that you can do

239
00:18:22,540 --> 00:18:26,060
and if they come from your own heart then you actually purify the

240
00:18:26,060 --> 00:18:30,780
you actually purify the deed. So it's not necessary to just give to people who are

241
00:18:30,780 --> 00:18:33,740
cultivated. If you give and you yourself are cultivated

242
00:18:33,740 --> 00:18:36,620
then there's the benefit of you helping the world right.

243
00:18:36,620 --> 00:18:40,060
That's considered to be a pure deed as well.

244
00:18:40,060 --> 00:18:43,900
And then the other hand if you give the dhamma then the dhamma purifies it

245
00:18:43,900 --> 00:18:48,060
or if you give something very powerful and the gift is very powerful

246
00:18:48,060 --> 00:18:52,460
then the gift itself makes it a pure deed and makes it a powerful

247
00:18:52,460 --> 00:18:58,300
gift. So anyway not a lot to say

248
00:18:58,300 --> 00:19:01,340
except another thing that we might say about this is

249
00:19:01,340 --> 00:19:08,860
it points to the error of magnitude that people sometimes fall into. They

250
00:19:08,860 --> 00:19:12,460
think of it in terms of thousands right and that's part of what the Buddha is

251
00:19:12,460 --> 00:19:16,540
saying here. He's saying really if just for a moment

252
00:19:16,540 --> 00:19:19,420
you you think a good thought towards someone who

253
00:19:19,420 --> 00:19:24,140
really deserves it. If you say I pay respect to the Buddha or I pay

254
00:19:24,140 --> 00:19:28,620
respect to the enlightened being then that one moment is actually more

255
00:19:28,620 --> 00:19:33,100
powerful than a hundred years of giving a thousand gold pieces a month

256
00:19:33,100 --> 00:19:38,460
which is a huge sum of money because people think that it's how much you give

257
00:19:38,460 --> 00:19:43,820
and it's actually certainly not. I mean rich people often brag about how much

258
00:19:43,820 --> 00:19:47,420
charity they give, how much money they give but

259
00:19:47,420 --> 00:19:50,380
it's meaningless because it has nothing to do with with your state of

260
00:19:50,380 --> 00:19:53,180
mind. It says nothing about what your state of mind is like

261
00:19:53,180 --> 00:19:58,940
and whether the gift is is rightly given. So if you give

262
00:19:58,940 --> 00:20:03,180
millions of dollars to fund war or if you give millions of dollars

263
00:20:03,180 --> 00:20:09,740
to fund wrong view for example people who put all this money into

264
00:20:09,740 --> 00:20:14,460
maybe certain religious teachings or religious traditions that are

265
00:20:14,460 --> 00:20:27,660
hateful or spiteful or misguided it's actually like throwing away money

266
00:20:27,660 --> 00:20:31,180
or maybe even worse than throwing it away. You support a cause that is

267
00:20:31,180 --> 00:20:35,740
and is harmful to people for example.

268
00:20:37,340 --> 00:20:41,420
Because it's all about the mind. One moment of actually just practicing

269
00:20:41,420 --> 00:20:45,660
meditation yourself is a much much greater thing to do

270
00:20:45,660 --> 00:20:49,740
but if you're going to give and giving is good then you should give with

271
00:20:49,740 --> 00:20:52,140
the pure mind yourself. You should give with this pure

272
00:20:52,140 --> 00:20:57,340
and you should give to those people who are engaged in pure things.

273
00:20:57,340 --> 00:21:02,380
So another verse about giving that's the Dhamma Padha for tonight.

274
00:21:02,380 --> 00:21:12,380
Thank you all for tuning in and keep practicing.

