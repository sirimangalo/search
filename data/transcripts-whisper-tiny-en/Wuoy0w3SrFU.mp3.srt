1
00:00:00,000 --> 00:00:03,040
Hi, welcome back to Ask a Monk.

2
00:00:03,040 --> 00:00:07,160
Today's question comes from M. Regal, who asks,

3
00:00:07,160 --> 00:00:11,560
what do you think of standing meditation?

4
00:00:11,560 --> 00:00:14,920
And the answer is, standing meditation

5
00:00:14,920 --> 00:00:17,920
is a perfectly valid form of meditation.

6
00:00:17,920 --> 00:00:21,040
There are four basic postures in the body,

7
00:00:21,040 --> 00:00:24,000
walking, standing, sitting, and lying down.

8
00:00:24,000 --> 00:00:26,440
Any one of these four can be used for the purpose

9
00:00:26,440 --> 00:00:28,920
of practicing meditation.

10
00:00:28,920 --> 00:00:31,920
In fact, any posture you're in, as I've said before,

11
00:00:31,920 --> 00:00:36,200
can be used to, as a means of contemplating

12
00:00:36,200 --> 00:00:38,000
the present moment.

13
00:00:38,000 --> 00:00:39,640
Standing meditation fits somewhere

14
00:00:39,640 --> 00:00:42,200
between walking, meditation, and sitting meditation.

15
00:00:42,200 --> 00:00:46,080
So we put the postures in the order

16
00:00:46,080 --> 00:00:48,640
in terms of their degree of effort as opposed

17
00:00:48,640 --> 00:00:51,920
to their degree of concentration, walking, meditation,

18
00:00:51,920 --> 00:00:56,600
having the highest degree of effort, lying meditation,

19
00:00:56,600 --> 00:01:02,200
having the highest degree of concentration.

20
00:01:02,200 --> 00:01:07,320
So, standing meditation is a sort of a moderate meditation.

21
00:01:07,320 --> 00:01:13,360
It's something that it's a form that can be used to calm you

22
00:01:13,360 --> 00:01:18,360
down or wake you up, depending on your situation.

23
00:01:18,360 --> 00:01:19,960
If you're sitting in meditation and you're

24
00:01:19,960 --> 00:01:22,440
starting to feel drowsy, you can stand up

25
00:01:22,440 --> 00:01:24,520
and do standing meditation.

26
00:01:24,520 --> 00:01:27,080
If you're walking and you're starting to feel agitated,

27
00:01:27,080 --> 00:01:32,440
you can stop and do standing meditation as well.

28
00:01:32,440 --> 00:01:34,080
On the other hand, standing meditation

29
00:01:34,080 --> 00:01:36,560
fits more in with sitting meditation

30
00:01:36,560 --> 00:01:38,800
than walking meditation in terms of the technique,

31
00:01:38,800 --> 00:01:40,640
because there's no movement in the body,

32
00:01:40,640 --> 00:01:46,120
except you could say the breath as it rises,

33
00:01:46,120 --> 00:01:47,320
the stomach rises and falls.

34
00:01:47,320 --> 00:01:49,480
So you could take it as a time to do sitting

35
00:01:49,480 --> 00:01:51,840
meditation, to do a form of sitting meditation,

36
00:01:51,840 --> 00:01:54,680
or a form of meditation like the sitting meditation,

37
00:01:54,680 --> 00:01:57,960
where you say rising, falling.

38
00:01:57,960 --> 00:01:59,800
You can also simply say to yourself,

39
00:01:59,800 --> 00:02:01,960
standing, standing, standing.

40
00:02:01,960 --> 00:02:04,560
And it's a good time during the walking meditation

41
00:02:04,560 --> 00:02:06,800
to contemplate those things that are really bothering you.

42
00:02:06,800 --> 00:02:09,320
As I've said in the video on walking meditation,

43
00:02:09,320 --> 00:02:13,360
suppose you have some pain in the back or pain in your head

44
00:02:13,360 --> 00:02:18,360
or some uncomfortable or some clear object

45
00:02:20,920 --> 00:02:23,240
that is taking your attention away from your feet,

46
00:02:23,240 --> 00:02:26,440
you can stop and instead of letting your mind go back

47
00:02:26,440 --> 00:02:28,440
and forth between the two objects,

48
00:02:28,440 --> 00:02:29,800
focus on the new object,

49
00:02:29,800 --> 00:02:32,120
focus on the pain and just say to yourself,

50
00:02:32,120 --> 00:02:34,720
pain, pain, pain, focus on the thought,

51
00:02:34,720 --> 00:02:36,760
focus on the emotion, whatever it is

52
00:02:36,760 --> 00:02:38,520
that's taking your attention away.

53
00:02:38,520 --> 00:02:39,840
And once that object is gone,

54
00:02:39,840 --> 00:02:42,040
then go back to the walking meditation.

55
00:02:42,040 --> 00:02:44,680
And as well it can also be used if you're just thinking too much.

56
00:02:44,680 --> 00:02:48,240
If you find the walking is distracting you,

57
00:02:48,240 --> 00:02:50,800
rather than keeping you in the present moment,

58
00:02:50,800 --> 00:02:54,280
it's giving you too much of an energy boost

59
00:02:54,280 --> 00:02:57,960
that you don't have enough concentration

60
00:02:57,960 --> 00:03:00,560
to do proper walking meditation.

61
00:03:00,560 --> 00:03:02,080
You can build up concentration

62
00:03:02,080 --> 00:03:03,520
by doing standing meditation,

63
00:03:03,520 --> 00:03:07,040
where you focus on some stable object

64
00:03:07,040 --> 00:03:09,720
like the rising and falling of the abdomen.

65
00:03:09,720 --> 00:03:11,120
And as you focus on that,

66
00:03:11,120 --> 00:03:12,640
you'll find your mind calms down

67
00:03:12,640 --> 00:03:15,080
and once it calms down you can continue with the walking.

68
00:03:16,200 --> 00:03:17,800
And on the other hand,

69
00:03:17,800 --> 00:03:19,840
so as you're sitting and you're falling asleep,

70
00:03:21,040 --> 00:03:23,840
you can either do walking or just standing meditation.

71
00:03:23,840 --> 00:03:26,560
Standing meditation, you just stand still

72
00:03:26,560 --> 00:03:28,720
as though you were going to do the walking

73
00:03:28,720 --> 00:03:33,480
and contemplate the breath as if you were sitting rising,

74
00:03:33,480 --> 00:03:37,520
falling, rising, falling, or just standing, standing.

75
00:03:37,520 --> 00:03:39,880
Or as a more advanced meditation technique

76
00:03:39,880 --> 00:03:41,600
that we normally give to someone

77
00:03:41,600 --> 00:03:44,240
after they had been practicing for some time,

78
00:03:44,240 --> 00:03:48,200
we would have them say rising, falling, sitting, rising,

79
00:03:48,200 --> 00:03:50,000
falling, sitting, switching back and forth

80
00:03:50,000 --> 00:03:52,440
between the breath and the posture.

81
00:03:52,440 --> 00:03:53,760
You can do the same in standing

82
00:03:53,760 --> 00:03:57,840
where you say rising, falling, standing, rising, falling,

83
00:03:57,840 --> 00:03:59,840
standing, switching back and forth.

84
00:03:59,840 --> 00:04:01,280
At the time when you're watching the breath,

85
00:04:01,280 --> 00:04:04,280
you're rising, falling, once you say falling,

86
00:04:04,280 --> 00:04:06,520
forget about the breath and let it continue on,

87
00:04:06,520 --> 00:04:07,960
then focus on the standing.

88
00:04:07,960 --> 00:04:10,080
That's sort of an advanced technique

89
00:04:10,080 --> 00:04:12,600
that we'd give to people after they've been practicing

90
00:04:12,600 --> 00:04:15,840
for some time, trying to give it a one more level

91
00:04:15,840 --> 00:04:20,800
of difficulty, increasing the intensity of it

92
00:04:20,800 --> 00:04:23,240
to train the mind to a higher level.

93
00:04:23,240 --> 00:04:27,160
But against standing meditation perfectly fine,

94
00:04:27,160 --> 00:04:29,960
as a basic practice, you can just say standing, standing,

95
00:04:29,960 --> 00:04:32,640
or rising, falling, or acknowledging anything

96
00:04:32,640 --> 00:04:35,240
that's preventing you from doing the walking

97
00:04:35,240 --> 00:04:37,160
or the sitting meditation.

98
00:04:37,160 --> 00:04:40,480
Okay, so good question, thanks, and keep them coming.

