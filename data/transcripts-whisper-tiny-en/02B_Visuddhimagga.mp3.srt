1
00:00:00,000 --> 00:00:16,320
It doesn't sound, and what the restraint here means is not to have akusala when we come

2
00:00:16,320 --> 00:00:20,000
across these objects.

3
00:00:20,000 --> 00:00:42,000
And the third paragraph C is the virtue of livelihood purification.

4
00:00:42,000 --> 00:00:49,840
It doesn't sound, and what the restraint here means is not to have akusala when we

5
00:00:49,840 --> 00:00:53,720
come across these objects.

6
00:00:53,720 --> 00:01:01,960
And the third paragraph C is the virtue of livelihood purification.

7
00:01:01,960 --> 00:01:21,240
Now for monks, pure livelihood means getting requisite by proper means, that means by going

8
00:01:21,240 --> 00:01:33,720
for arms round, monks get food, and by picking up pieces of cloth and making these cloth

9
00:01:33,720 --> 00:01:39,480
pieces of cloth into a robe and wearing it, and so on.

10
00:01:39,480 --> 00:01:44,880
So these are the good livelihoods of the monks.

11
00:01:44,880 --> 00:01:52,640
And if monks do not follow these rules, and there is no pureity of virtue of livelihood

12
00:01:52,640 --> 00:01:58,680
purification, it will also be explained in detail later.

13
00:01:58,680 --> 00:02:05,280
Here from such wrong, abstain is from such wrong livelihoods and tails transgression

14
00:02:05,280 --> 00:02:12,360
of the six training precepts, announced with respect to livelihood and tails, the evil

15
00:02:12,360 --> 00:02:20,880
states beginning talking, hinting, belittling, pursuing gain with gain is virtue of livelihood

16
00:02:20,880 --> 00:02:22,520
purification.

17
00:02:22,520 --> 00:02:30,400
If you have read through this chapter, you will know what these are, and I am afraid

18
00:02:30,400 --> 00:02:44,040
that you know more about the tricks, monks, that's a bad monk's use in obtaining what

19
00:02:44,040 --> 00:02:46,920
they want.

20
00:02:46,920 --> 00:03:00,320
Now, the six training precepts, we will find in paragraph 60, in paragraph 6.

21
00:03:00,320 --> 00:03:11,640
These are given, one by one.

22
00:03:11,640 --> 00:03:17,520
With livelihoods caused with livelihoods as reason, one of evil wishes, a pretty wishes

23
00:03:17,520 --> 00:03:23,680
lays claim to a higher than human state that does not exist and not affect the contravention

24
00:03:23,680 --> 00:03:36,560
of which is defeat, that means, because, suppose, I want to be rich, so I want to pose

25
00:03:36,560 --> 00:03:39,520
as many records as many things.

26
00:03:39,520 --> 00:03:46,920
And so, even though I have not attained any of the stages of enlightenment, I will

27
00:03:46,920 --> 00:03:52,960
say, say, I am enlightened or something like that, so that you think much of me and

28
00:03:52,960 --> 00:04:04,520
you, you think of giving me, offering me things, something like that, so such a breach

29
00:04:04,520 --> 00:04:13,040
of the rule is caused by something like livelihood, that means, I don't want to go out

30
00:04:13,040 --> 00:04:22,520
for arms, I want to be at the monastery and just eat what people bring to me, maybe rich

31
00:04:22,520 --> 00:04:24,280
food and so on.

32
00:04:24,280 --> 00:04:36,880
So, these rules, six rules are proclaimed or laid down with regard to monk's livelihood.

33
00:04:36,880 --> 00:04:43,440
So, defeat, you see the word defeat there, right?

34
00:04:43,440 --> 00:04:52,440
And these are technical words, defeat means, if a monk,

35
00:04:52,440 --> 00:04:58,080
it breaks this rule, then he is finished as a monk, he is defeated as a monk, he is no

36
00:04:58,080 --> 00:05:04,920
longer a monk, even though he may be still going on wearing the ropes and even though

37
00:05:04,920 --> 00:05:14,720
he may still go on admitting himself as a monk, but he is in fact in reality, not a monk,

38
00:05:14,720 --> 00:05:22,440
the movement, he says this and the other understands it.

39
00:05:22,440 --> 00:05:34,440
So, there are these six rules, we will come with these six rules later.

40
00:05:34,440 --> 00:05:44,640
If the person spoken to does not understand at that moment, then he is not defeated.

41
00:05:44,640 --> 00:05:53,560
But, if the person to whom he is speaking knows at the moment that this monk is saying

42
00:05:53,560 --> 00:06:00,000
that he is enlightened and the monk is not really enlightened, then there is defeat.

43
00:06:00,000 --> 00:06:25,920
Yeah, understanding, yes.

44
00:06:25,920 --> 00:06:35,720
And I want you to read the, I think you have read the resource and others for monks,

45
00:06:35,720 --> 00:06:39,640
which are resource for monks and which are not resource for monks.

46
00:06:39,640 --> 00:06:47,080
The resource here means which monks must engage in and which monks must not engage in.

47
00:06:47,080 --> 00:07:01,680
Now, on page 6, 17, in paragraph 44, about middle of the paragraph you find, here someone

48
00:07:01,680 --> 00:07:07,360
makes a livelihood by gifts of bamboos, or by gifts of leaves, or by gifts of flowers, fruits,

49
00:07:07,360 --> 00:07:14,920
bathing powder, and tooth sticks, or by flattery, or by bean supray.

50
00:07:14,920 --> 00:07:30,360
So that, by fondling, now I will come to bean supray later, by fondling really means by

51
00:07:30,360 --> 00:07:39,160
by babysitting, by picking up a baby and taking care of it, not just by fondling, by

52
00:07:39,160 --> 00:07:46,440
taking care of a baby, if somebody leaves a baby with you and with a monk and if he takes

53
00:07:46,440 --> 00:07:53,800
care of the baby in order to please that man, that person, then it is called, here fondling.

54
00:07:53,800 --> 00:08:04,800
Now, bean supray, that means living by a livelihood which resembles bean supray, and you

55
00:08:04,800 --> 00:08:21,040
don't still understand, right? Please turn to page 28, paragraph 75, bean supray is resemblance

56
00:08:21,040 --> 00:08:28,680
to bean supray, for just as when beans are being cooked, only a few do not get cooked,

57
00:08:28,680 --> 00:08:34,600
the rest get cooked, so to the person in whose speech, only a little is true, the rest

58
00:08:34,600 --> 00:08:43,200
being false, it's called bean supray.

59
00:08:43,200 --> 00:08:57,520
That's called bean supray, so most of what he said, it just lies, there's an explanation

60
00:08:57,520 --> 00:09:03,600
in English, but not meaning the same thing, has baked or something, but that doesn't

61
00:09:03,600 --> 00:09:18,920
mean that, but why is creaking? I don't know, if what most of what I say is not true,

62
00:09:18,920 --> 00:09:32,600
and only a little is true, then I am guilty of this bean supray.

63
00:09:32,600 --> 00:09:42,200
And on page 18, the resort, we have to understand this properly, now, proper resort, there

64
00:09:42,200 --> 00:09:48,640
is proper resort in proper resort, hearing what is in proper resort, here someone has

65
00:09:48,640 --> 00:09:55,400
prostitutes as resort, or he has videos, old mates, unags, bikunas or taverns as resort.

66
00:09:55,400 --> 00:10:02,480
Having them as resort means being friends with them, being intimate with them, frequenting

67
00:10:02,480 --> 00:10:18,520
their houses, that is what is meant by having them as resort.

68
00:10:18,520 --> 00:10:30,320
So there are some places, which monks must always avoid the places of these people.

69
00:10:30,320 --> 00:10:40,920
And then on page 19, paragraph 49, another kind of resort, three kinds of resort, proper

70
00:10:40,920 --> 00:10:48,960
resort as support, proper resort as guardian and proper resort as anchoring, hearing what

71
00:10:48,960 --> 00:10:54,680
is proper resort as support, a good friend who exhibits the 10 top instances of talk we

72
00:10:54,680 --> 00:11:01,640
are given in the footnote, in whose presence one hears what has not been heard and so on,

73
00:11:01,640 --> 00:11:09,600
in whose presence really means, depending on whom, not just in his presence, that means

74
00:11:09,600 --> 00:11:16,600
from him, so you hear something from him, that is what is meant here, and here is that

75
00:11:16,600 --> 00:11:22,960
what has not been heard, corrects what has been heard, gets rid of doubt, rectifies one's

76
00:11:22,960 --> 00:11:25,720
view and gains confidence.

77
00:11:25,720 --> 00:11:34,720
Now, these are the benefits of hearing to Dhamma talk, there are Buddha said in one

78
00:11:34,720 --> 00:11:43,480
sudha, there are five benefits to be gained from listening to Dhamma talk and the first

79
00:11:43,480 --> 00:11:51,680
one is you hear what you have not heard before, the new information that is and then

80
00:11:51,680 --> 00:11:57,880
you corrects what has been heard, that means you can clarify what you have heard before

81
00:11:57,880 --> 00:12:04,560
when you hear it again and then gets rid of doubt, that is a third benefit, you can

82
00:12:04,560 --> 00:12:11,760
remove doubt about something when you hear Dhamma talk and rectifies one's view, if you

83
00:12:11,760 --> 00:12:18,760
have a wrong view then you can set it right when you listen to Dhamma talk and gains

84
00:12:18,760 --> 00:12:29,600
confidence that means your mind becomes full of confidence, so these are the five benefits

85
00:12:29,600 --> 00:12:38,560
to be gained from listening to Dhamma talk or by training under whom and actually not

86
00:12:38,560 --> 00:12:46,240
training under whom, by following his example, by following his example, one grows in faith

87
00:12:46,240 --> 00:12:58,080
but you learn in generosity and understanding and next paragraph, what is proper resort

88
00:12:58,080 --> 00:13:04,440
as garden, here a bikhu, having entered inside a house, having gone into a street and

89
00:13:04,440 --> 00:13:24,320
so on, he always made this mistake, Janam Holy, no, the poly word here is Antara Gara, now

90
00:13:24,320 --> 00:13:37,280
Gara means a house and Antara means between or within, so Antara Gara, the poly word, he translated

91
00:13:37,280 --> 00:13:45,280
as inside a house because Antara can mean inside and Gara means house but the real meaning

92
00:13:45,280 --> 00:13:59,840
is a place which has houses in it, so it means a village, so having entered a village,

93
00:13:59,840 --> 00:14:05,840
having gone to a street goes with Dhamma's eyes, seeing the length of a plough yolk,

94
00:14:05,840 --> 00:14:13,640
so not inside a house, monks must keep their eyes down when they go out, when they go

95
00:14:13,640 --> 00:14:24,920
out into the village or into the town, not just inside a house, so this is the wrong

96
00:14:24,920 --> 00:14:34,160
rendering of the poly word and then seeing the length of a plough yolk, in fact it is

97
00:14:34,160 --> 00:14:46,600
not plough yolk, it is courage yolk and a yolk is said to be about 4 qubits long, that

98
00:14:46,600 --> 00:14:55,360
means 6 feet, 6 feet long, so a monk is to look about 6 feet on the ground, not look

99
00:14:55,360 --> 00:15:08,840
about, look sideways, it may be possible where it is not so crowded, but in modern cities

100
00:15:08,840 --> 00:15:15,720
it is impossible, you have to look, if you just look down and walk, you will be knocked

101
00:15:15,720 --> 00:15:29,720
on by a car or something, but monks are trying to keep their eyes down, not looking at

102
00:15:29,720 --> 00:15:33,600
an elephant, not looking at a house, not looking at courage, a pedestrian, a woman, man

103
00:15:33,600 --> 00:15:52,160
and so on, and then, so now we come to the end of party mocha, restraint, seeing fear

104
00:15:52,160 --> 00:15:58,880
in the slightest fault, seeing danger in the slightest fault, so even if very slight fault

105
00:15:58,880 --> 00:16:08,280
or a transgression can do harm to you, because especially when you practice meditation

106
00:16:08,280 --> 00:16:18,200
this can be a great obstacle to your progress or to your concentration, now a monk is instructed

107
00:16:18,200 --> 00:16:31,280
to keep the rules intact, even if he has broken a minor rule and if he broke it intentionally

108
00:16:31,280 --> 00:16:40,080
then the feeling of guilt is always with him, and this feeling of guilt will torment him

109
00:16:40,080 --> 00:16:53,000
when he practices meditation, so the slightest translation is, there is danger and even

110
00:16:53,000 --> 00:16:59,600
in slightest transgression, so seeing danger in slightest translation, he keeps him, his

111
00:16:59,600 --> 00:17:14,720
healer, his virtue are really pure, so we come to the end of the party mocha restraint

112
00:17:14,720 --> 00:17:35,760
today, how many pages do we have to go until the end of first chapter, 58 pages, so two

113
00:17:35,760 --> 00:17:55,560
more weeks maybe, so please read on as much as as much as you can, yes,

114
00:17:55,560 --> 00:18:22,560
okay, that's close to 9 o'clock now.

115
00:18:22,560 --> 00:18:39,160
I didn't turn it on, so I was not aware of the kind of thing, no, no, no, no, no, no, no,

116
00:18:39,160 --> 00:19:06,600
no, no, that's one of the things that I don't have, but no.

117
00:19:36,600 --> 00:19:39,600
.

118
00:19:40,600 --> 00:19:42,600
.

119
00:19:43,600 --> 00:19:47,600
..

120
00:19:48,600 --> 00:19:51,600
..

121
00:19:51,600 --> 00:19:54,600
..

122
00:19:54,600 --> 00:19:59,600
..

123
00:20:00,600 --> 00:20:01,600
..

124
00:20:00,600 --> 00:20:02,600
..

125
00:20:02,600 --> 00:20:05,600
..

