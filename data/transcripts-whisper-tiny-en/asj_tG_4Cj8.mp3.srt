1
00:00:00,000 --> 00:00:07,920
I'm going to visit, where, no, don't scroll yet. I'm going to visit a monastery in the

2
00:00:07,920 --> 00:00:17,240
tradition of Ajahn chaff for a week and a day. What would be a suitable gift? Well,

3
00:00:17,240 --> 00:00:26,280
that is a suitable gift. You're visiting the monastery as a suitable gift. It's a horrible

4
00:00:26,280 --> 00:00:33,520
answer, isn't it? Well, a nice that you want to give a gift. It's always nice. It's a very

5
00:00:33,520 --> 00:00:44,000
good tradition. I'm going to visit someone to bring them a gift. The problem is that, you know,

6
00:00:44,000 --> 00:00:57,160
I don't know. I have problems with gifts because it's just more stuff. That's why I like

7
00:00:57,160 --> 00:01:01,920
the wish list idea. I put up a wish list of things that I need. Well, as I have an

8
00:01:01,920 --> 00:01:06,080
updated it because I don't really need anything these days. What I do is there's someone

9
00:01:06,080 --> 00:01:12,520
there to help me out with it. It's much more local and immediate. But maybe in Canada,

10
00:01:12,520 --> 00:01:19,280
we'll start it up again. I just, it's easy to forget. But if you have, if the monastery

11
00:01:19,280 --> 00:01:24,440
that you're visiting had a wish list, right, things that they need on a regular basis, then

12
00:01:24,440 --> 00:01:30,480
you could go to the wish list and find out what they need. That's the most rewarding

13
00:01:30,480 --> 00:01:37,960
on both sides. Like light bulbs are always useful, especially here where they'd burn out

14
00:01:37,960 --> 00:01:46,280
in every thunderstorm. So people always give light. They used to candles in the old days,

15
00:01:46,280 --> 00:01:51,480
but another give light. Light bulbs, of course, then you have to know what kind of

16
00:01:51,480 --> 00:01:57,360
lights they're using. And there's something symbolic there as well, not giving the light

17
00:01:57,360 --> 00:02:04,440
of wisdom, giving light that allows them to see. Razors are always good because it's sharp

18
00:02:04,440 --> 00:02:18,200
like the mind of wisdom, embracing all the gift also because monks need razors. Honey,

19
00:02:18,200 --> 00:02:23,720
I know adjuncts are coffee. They drink a lot of coffee. No, I shouldn't say that. You have

20
00:02:23,720 --> 00:02:29,760
to find out what they need. That's why wish lists are the best. But it's just a gift, right?

21
00:02:29,760 --> 00:02:44,280
Just as a token. Medicines, like a first aid kit, first aid kits are good. Medicines.

22
00:02:44,280 --> 00:03:02,480
But honey, because they can keep it for seven days, it's always like honey. Food. If you're

23
00:03:02,480 --> 00:03:06,400
going as a late person, you can bring food of the sort that you can keep and give to the monks

24
00:03:06,400 --> 00:03:12,040
every day. So you could bring food that you could keep in your room and every morning

25
00:03:12,040 --> 00:03:18,200
go and give some to the monks. I mean, just some dry food or I don't know. Like, I know in

26
00:03:18,200 --> 00:03:30,480
one monastery, a tennis role, because monastery in Sacramento, in San Diego, they gave, they

27
00:03:30,480 --> 00:03:36,600
had fish oil. So I guess every day the monks eat fish oil, at least little fish oil capsules.

28
00:03:36,600 --> 00:03:43,600
So that would be something if you brought some fish oil and give it a capsule to the monks

29
00:03:43,600 --> 00:03:52,000
every day. Yeah, there's lots of interesting things you can think of.

30
00:03:52,000 --> 00:03:58,400
Robes, if you can find monks from, I guess that's not so easy. And it's definitely not easy.

31
00:03:58,400 --> 00:04:14,800
Nowadays, honey is nice, honey is a good idea. And medicines, yeah.

