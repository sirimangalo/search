1
00:00:00,000 --> 00:00:05,000
Can you cover proper noting during meditation?

2
00:00:05,000 --> 00:00:10,000
Do you always come back to the breath?

3
00:00:10,000 --> 00:00:19,000
Yeah, I always come back to the breath because for the soul reason that the body is the most obvious.

4
00:00:19,000 --> 00:00:26,000
Be clear, if you're talking about the tradition that we follow, we're not actually being mindful of the breath.

5
00:00:26,000 --> 00:00:31,000
It's just a usage of this word. The Buddha used the word Anapanasati.

6
00:00:31,000 --> 00:00:36,000
But Anapana, the mindfulness of the in-breaths and the out-breaths, is a summit of meditation.

7
00:00:36,000 --> 00:00:45,000
If you're talking about Vipasana, it's actually technically not mindfulness of the breath because breath is not an ultimate reality.

8
00:00:45,000 --> 00:00:47,000
The ultimate reality is the body.

9
00:00:47,000 --> 00:00:50,000
So you don't actually always come back again to the breath.

10
00:00:50,000 --> 00:00:59,000
You come back always again to the sensations that arise based on the breath, whether it's at the nose or whether it's at the stomach.

11
00:00:59,000 --> 00:01:01,000
And it may not even be the breath at all.

12
00:01:01,000 --> 00:01:08,000
You can come back to the body just sitting and say to yourself, sitting, sitting over and you're walking, walking, walking.

13
00:01:08,000 --> 00:01:12,000
Meditation is not just sitting on the mat.

14
00:01:12,000 --> 00:01:16,000
Obviously when you're doing walking meditation, you wouldn't focus on the breath.

15
00:01:16,000 --> 00:01:25,000
You'd focus on the movements of the feet. We have even a prostration meditation where you focus on the hands, the movements of the hands and the bending of the back and so on.

16
00:01:25,000 --> 00:01:31,000
If you're eating, you have an eating meditation, so being mindful of chewing, chewing and swallowing.

17
00:01:31,000 --> 00:01:37,000
Point being the easiest to be mindful of the most course and most obvious is the body.

18
00:01:37,000 --> 00:01:50,000
And Buddha Gosay in the Risudhi Magha says, actually, the more time you spend focusing on the body, the more clear the mind becomes.

19
00:01:50,000 --> 00:01:53,000
The more clearly obvious the mind becomes.

20
00:01:53,000 --> 00:02:03,000
So by focusing on the body, you're actually indirectly learning about the mind, which should make perfect sense because the mind is what is looking.

21
00:02:03,000 --> 00:02:11,000
So as you look at the stomach, for instance, rising and falling, you'll learn all sorts of things about the mind and this is why many people don't like this practice.

22
00:02:11,000 --> 00:02:22,000
Because you have to see your liking and your disliking and your boredom and your aversion and your control, your controlling nature and so on.

23
00:02:22,000 --> 00:02:34,000
And to see all sorts of unpleasant things about yourself and so you start to blame the meditation. This meditation is no good. I can't get into it. You can't get into it because of the eye and I can't get into it.

24
00:02:34,000 --> 00:02:50,000
Once you give up the eye, then it's not really a problem. Once you give up, once you learn about the mind, once you understand and let go, then you're able to easily focus on it.

25
00:02:50,000 --> 00:02:59,000
In the beginning, or the whole of the practice really, is using the body in order to see the mind.

26
00:02:59,000 --> 00:03:05,000
And when you see the mind, whether it be feelings or whether it be thoughts or whether it be emotions, you focus on those.

27
00:03:05,000 --> 00:03:12,000
And when they're gone, you come back again to the body because it's still there.

28
00:03:12,000 --> 00:03:20,000
I just wanted to add that the good thing about the breath is that it's always there.

29
00:03:20,000 --> 00:03:35,000
So when there's nothing else that you can note, then you can go back to the rising and the falling of the breath of the abdomen, which is caused by the breath.

30
00:03:35,000 --> 00:03:49,000
And to see the rising and the falling can help you to have insights about other of rising and ceasing, for example,

31
00:03:49,000 --> 00:03:59,000
because this is very related. You can understand how it goes in waves and how everything is rising and ceasing.

32
00:03:59,000 --> 00:04:09,000
It is when there's nothing else to note good to go back to the rising and the falling.

