WEBVTT

00:00.000 --> 00:10.000
Is it beneficial to know the stages of insight before one goes through them or should one wait until one arrives at one stage to learn how to work through them?

00:14.000 --> 00:23.000
Yeah, I don't think it's proper to...

00:23.000 --> 00:34.000
In an ideal sense, it's not proper to... in an ideal setting, it's not proper to know anything about the stages of knowledge.

00:34.000 --> 00:47.000
It's important to leave that up to your teacher who has gone through them himself or at the very least has a very thorough knowledge of them and the theories surrounding them

00:47.000 --> 00:53.000
and allow them to guide you and use them as a benchmark to see your progress.

00:53.000 --> 01:04.000
A beginner meditator will generally not have a very clear progress through the stages of knowledge.

01:04.000 --> 01:19.000
So they will go through them and eventually they will get through them, but the first time through it won't be so clear and it gets clearer as they go through them again and again.

01:19.000 --> 01:47.000
And so the best way to, in an ideal setting where you have a teacher, the best thing is to slowly learn about them from one course to the next and have the teacher, slowly give you more and more information about them until it's matched by your observations through the practice.

01:47.000 --> 02:08.000
So as you learn more about the stages of knowledge through your practice of them, it's beneficial to get some knowledge about them to know that when you're on the right track and when you're not on the right track and in a sense to mark your progress through the stages.

02:08.000 --> 02:17.000
Now if you know about the stages in advance, it can actually be a major hindrance to your practice.

02:17.000 --> 02:31.000
It can even stop you from making it through the stages because of your expectation, because of your distraction, worry and paranoia and you're doubt about whether you're actually going to attain them and so on.

02:31.000 --> 02:42.000
Doubt about whether it's actually the true progress that leads to Nibbana, whether the mind actually goes through those.

02:42.000 --> 02:49.000
The nice thing with Nagasena, he's been sitting in and watching, so he was asking what happened for everyone, I said, we'll watch and see.

02:49.000 --> 02:54.000
And so he's been watching the meditator, we have one meditator now, watching.

02:54.000 --> 03:11.000
And very careful, he can vouch for the fact that I'm not giving any kind of what do you call hints or signals or hypnotizing or brainwashing the meditator into saying this or saying that and just asking what's going on.

03:11.000 --> 03:22.000
And it's very clear you can see the progression that in fact, at least for the meditator that he has seen, they are going through these stages and for all the meditators that I've seen, it does occur that way.

03:22.000 --> 03:43.000
It should happen naturally and you should get to the point where you're in this position where you're learning how to teach before you really start to examine the course of the knowledge is because they're much more useful for a teacher to be clear whether the meditator is on the right path.

03:43.000 --> 03:51.000
And if the signs are right, then the teacher can rest content that the meditators doing the right thing now.

03:51.000 --> 03:58.000
If the meditators not experiencing them, then the teacher will be able to point out they're doing this or doing that and that's the reason why.

03:58.000 --> 04:10.000
If they're not experiencing the stages of knowledge clearly, then at the same time you will be clearly able to see that they are clinging to something, they're clinging to a pleasant state.

04:10.000 --> 04:15.000
They're clinging to unpleasant states, they're clinging to views, they're clinging to this, clinging to that.

04:15.000 --> 04:21.000
And as soon as they let go of those, they will naturally progress through the stages of knowledge without any prompting.

04:21.000 --> 04:27.000
So it's for the teacher to much more for the teacher to gauge, at least in the beginning to gauge the student's knowledge.

04:27.000 --> 04:38.000
Now why I said in an ideal setting, because for many people this is an option, so you have to kind of find your way like a Bodhisattva through the stages of knowledge yourself.

04:38.000 --> 04:41.000
And in that case, the stages of knowledge aren't really enough.

04:41.000 --> 04:57.000
You need to know just about everything that's in the tepitika and you have to have a very strong background in theory and in a specific tradition.

04:57.000 --> 05:07.000
And in the theory behind a specific tradition, for example, if you follow the Mahasi Sayyad, our tradition, the tradition that we follow, this interpretation, this understanding of the Buddha's teaching.

05:07.000 --> 05:18.000
And this particular meditation method, then you have to get a strong background in this training and this practice in order to go through it on your own.

05:18.000 --> 05:26.000
But whether you'll actually make it through is a whole other issue.

05:26.000 --> 05:40.000
So teachers will say that it will actually stop you. If you know them in advance, if you've read about them and then come to do a course, it will actually block you and prevent you from progressing through the stages.

05:40.000 --> 05:44.000
So definitely not something to worry about.

05:44.000 --> 05:53.000
And if you don't have a teacher, I still, I think it wouldn't be so good to focus on the stages.

05:53.000 --> 06:03.000
It's much better to focus on general theory behind giving up and looking at what the Buddha said and looking at the Buddha's teaching.

06:03.000 --> 06:12.000
Because you will never teach someone the stages of knowledge. You will never give that as a discourse and something that is practical for a meditator.

06:12.000 --> 06:27.000
You know, we'll be an encouragement and the things that the Buddha gave in the Sutras, the Buddha never gave any talks on the stages of knowledge. They're much more of a technical reference for teachers and advanced students.

06:27.000 --> 06:30.000
It's never recommended by any teacher that they should begin out.

06:30.000 --> 06:46.000
So even if you don't have a teacher, try to stick to the Sutras and get what you can out of the meditation books. And if I recommend books that were written by the Mahasizaya and all these books that are on vipassana meditation, excellent guides.

06:46.000 --> 06:51.000
There's many of them that are excellent guides for beginner meditators.

06:51.000 --> 07:01.000
And after some time, you might want to slowly read something about the stages of knowledge.

07:01.000 --> 07:06.000
But certainly not worth focusing your effort on until you get to an advanced stage.

07:06.000 --> 07:30.000
Instead, you've gone through the maximally.

