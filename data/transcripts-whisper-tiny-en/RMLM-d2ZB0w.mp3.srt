1
00:00:00,000 --> 00:00:07,000
Welcome, everyone.

2
00:00:07,000 --> 00:00:10,000
We are daily meditations and succession.

3
00:00:10,000 --> 00:00:12,000
Please sit in the chair.

4
00:00:12,000 --> 00:00:15,000
It's fine.

5
00:00:15,000 --> 00:00:17,000
Oh, no, don't worry about it.

6
00:00:17,000 --> 00:00:24,000
Sit comfortably.

7
00:00:24,000 --> 00:00:29,000
We have, this is our largest audience yet, I think.

8
00:00:29,000 --> 00:00:37,000
Seven of us here, and we've got five people watching from the internet.

9
00:00:37,000 --> 00:00:40,000
They're a live broadcast.

10
00:00:40,000 --> 00:00:44,000
So tonight is the English session.

11
00:00:44,000 --> 00:00:52,000
I've been missed most of the Thai sessions lately.

12
00:00:52,000 --> 00:01:04,000
We'll try to get, we'll try to get up so that we're here every day.

13
00:01:04,000 --> 00:01:12,000
Tonight I wanted to talk about dealing with the truth.

14
00:01:12,000 --> 00:01:15,000
Because Buddhism deals specifically with the truth.

15
00:01:15,000 --> 00:01:17,000
I'm just going to talk here.

16
00:01:17,000 --> 00:01:20,000
You're welcome to close your eyes and meditate.

17
00:01:20,000 --> 00:01:22,000
Normally when I talk, I close my eyes.

18
00:01:22,000 --> 00:01:26,000
I'm not trying to be entertaining.

19
00:01:26,000 --> 00:01:28,000
I'm trying to be meditative.

20
00:01:28,000 --> 00:01:39,000
So even when I, while I talk, I'll try to stay in touch with the meditation practice.

21
00:01:39,000 --> 00:01:43,000
When you listen, you should also try to stay in touch with the meditation practice.

22
00:01:43,000 --> 00:01:52,000
Use it as an opportunity to practice yourself.

23
00:01:52,000 --> 00:01:55,000
So Buddhism deals specifically with the truth.

24
00:01:55,000 --> 00:02:07,000
It's a very, it's the core issue in Buddhism is the realization of the truth.

25
00:02:07,000 --> 00:02:17,000
Or the realization of specific truths.

26
00:02:17,000 --> 00:02:25,000
In, in, in brief the realization of the truth of reality.

27
00:02:25,000 --> 00:02:31,000
Or coming to understand the truth about reality as it is.

28
00:02:31,000 --> 00:02:43,000
And by reality we're talking here not about theoretical reality or the reality of the physicists.

29
00:02:43,000 --> 00:02:45,000
The reality of philosophers.

30
00:02:45,000 --> 00:02:49,000
We're talking about experiential reality.

31
00:02:49,000 --> 00:02:52,000
The reality which you can experience.

32
00:02:52,000 --> 00:02:57,000
Some people call it subjective reality.

33
00:02:57,000 --> 00:03:01,000
But actually in Buddhism we're not dealing with subjective reality.

34
00:03:01,000 --> 00:03:11,000
And we deny the claim that first person experience of reality is necessarily subjective.

35
00:03:11,000 --> 00:03:19,000
Subjective here meaning particularly in different for everyone.

36
00:03:19,000 --> 00:03:26,000
Or unscientific.

37
00:03:26,000 --> 00:03:30,000
Because in a sense reality for us is subjective.

38
00:03:30,000 --> 00:03:34,000
We are the subject or there is a subject and that's the mind.

39
00:03:34,000 --> 00:03:40,000
As opposed to looking at things objectively in terms of the object.

40
00:03:40,000 --> 00:03:45,000
Or in terms of a third person perspective.

41
00:03:45,000 --> 00:03:51,000
But it's not subjective in the sense and my reality is not different from your reality.

42
00:03:51,000 --> 00:03:56,000
This is the truth that we're going, that we try to come to realize.

43
00:03:56,000 --> 00:04:01,000
It's an objective truth about subjective reality.

44
00:04:01,000 --> 00:04:07,000
So a scientist would look at meditation and say you can't get truth from that.

45
00:04:07,000 --> 00:04:10,000
It's subjective.

46
00:04:10,000 --> 00:04:13,000
How do you know you're seeing the things you see?

47
00:04:13,000 --> 00:04:16,000
How do you know you're hearing the things you hear?

48
00:04:16,000 --> 00:04:19,000
How do you know it's real?

49
00:04:19,000 --> 00:04:24,000
It's subjective.

50
00:04:24,000 --> 00:04:31,000
There's no way of telling whether it's real or not.

51
00:04:31,000 --> 00:04:38,000
And that's the kind of reality that they're talking about is not the reality we're talking about.

52
00:04:38,000 --> 00:04:45,000
When we talk about reality and Buddhism we're talking about experience.

53
00:04:45,000 --> 00:04:50,000
And in that we claim that my experience is the same as your experience.

54
00:04:50,000 --> 00:04:53,000
Because no matter what you see you still see.

55
00:04:53,000 --> 00:05:05,000
No matter what I see it's still seeing and my seeing and your seeing is the same in the sense that it's both seeing.

56
00:05:05,000 --> 00:05:19,000
There are some essential building blocks of experience which are common to all beings, the experience of all beings.

57
00:05:19,000 --> 00:05:27,000
When we break experience down into those building blocks then we have an objective understanding of reality.

58
00:05:27,000 --> 00:05:40,000
This is what we mean by reality.

59
00:05:40,000 --> 00:05:43,000
So what is the truth that we're trying to realize?

60
00:05:43,000 --> 00:05:48,000
Well there's a lot of truth that can come from experience or reality.

61
00:05:48,000 --> 00:05:56,000
There are many truths that were going to realize in the course of the practice.

62
00:05:56,000 --> 00:06:07,000
But in particular we're focusing on those truths that are going to free our mind.

63
00:06:07,000 --> 00:06:20,000
Those truths that are going to allow us to react appropriately to reality.

64
00:06:20,000 --> 00:06:41,000
We're not interested in what is it like to taste ice cream or what is it like to see a supernova or what is it like to hear an angel or any mystical or magical experience which is very much a part of reality.

65
00:06:41,000 --> 00:07:10,000
We're focusing on those truths that are particularly useful and have to do with the building blocks of experience, have to do with our interaction with the world around us.

66
00:07:10,000 --> 00:07:15,000
Because our experience of reality is not always pleasant.

67
00:07:15,000 --> 00:07:24,000
We want to be happy. Everyone wants to be happy.

68
00:07:24,000 --> 00:07:34,000
And yet among the many, many people who wish to be happy, most of us are less than 50% happy,

69
00:07:34,000 --> 00:07:46,000
less than 50% of the time, which means we're losing, where our reality is contrary to our wishes.

70
00:07:46,000 --> 00:07:50,000
So, embrace something's wrong.

71
00:07:50,000 --> 00:07:58,000
You can never say this is a proper state of being where we want to be happy and yet we're happy less than 50% of the time.

72
00:07:58,000 --> 00:08:03,000
Some people live most of their lives unhappy.

73
00:08:03,000 --> 00:08:26,000
Even just sitting here, you can see that your experience of reality is not entirely pleasant.

74
00:08:26,000 --> 00:08:35,000
Maybe it's too hot. Maybe it's too cold. Maybe your head hurts. Maybe your back hurts. Maybe your legs hurt.

75
00:08:35,000 --> 00:08:42,000
Maybe your itching and scratching. Maybe you're thinking about unpleasant things.

76
00:08:42,000 --> 00:08:51,000
Maybe you don't like what I'm hearing or up in the air, philosophical.

77
00:08:51,000 --> 00:08:58,000
Dr. Northieri, we're talking about the building blocks of our experience.

78
00:08:58,000 --> 00:09:05,000
You're hot. Well, he does a building block. He does reality. It's really hot.

79
00:09:05,000 --> 00:09:11,000
How do you feel about the heat you don't like it? Well, this liking is part of reality.

80
00:09:11,000 --> 00:09:20,000
It's part of your reality. That's your reaction to the heat or the cold or the itching or the pain.

81
00:09:20,000 --> 00:09:49,000
In the meditation we're going to see these things clearer than we're going to see the way reality works.

82
00:09:49,000 --> 00:09:55,000
See the way reality works clearer than before in a way that we couldn't see it before.

83
00:09:55,000 --> 00:10:00,000
Normally when there's pain in the body, for example, right away we don't like it.

84
00:10:00,000 --> 00:10:04,000
Right away we've made up our mind. It's bad.

85
00:10:04,000 --> 00:10:11,000
And not only that, we immediately take action without even thinking,

86
00:10:11,000 --> 00:10:17,000
without even really looking at it and examining it to see, what is this pain?

87
00:10:17,000 --> 00:10:23,000
Is it really bad? What's bad about it?

88
00:10:23,000 --> 00:10:28,000
And you can get to the point where even just hearing a simple sound.

89
00:10:28,000 --> 00:10:35,000
Once we get a hatred or a dislike for a phenomenon,

90
00:10:35,000 --> 00:10:40,000
and we become so react, we could become reactionary towards it.

91
00:10:40,000 --> 00:10:51,000
Even just hearing someone's voice, when we get it in our minds that this person is unwelcomed or unappreciated,

92
00:10:51,000 --> 00:10:56,000
is unpleasant person. As soon as we hear the voice, there's nothing wrong with the voice.

93
00:10:56,000 --> 00:11:01,000
It's a sound that arises in the ear.

94
00:11:01,000 --> 00:11:07,000
We're right away getting angry. We've already made up our mind and we're ready to block them out

95
00:11:07,000 --> 00:11:19,000
or do whatever we can to not have to hear the sound.

96
00:11:19,000 --> 00:11:32,000
This is our ordinary experience of reality.

97
00:11:32,000 --> 00:11:44,000
The truth of reality that we're trying to realize to cut to the chase tune to do your work for you.

98
00:11:44,000 --> 00:11:47,000
So you don't have to practice. I give you the answer.

99
00:11:47,000 --> 00:11:50,000
There's nothing good or bad about reality.

100
00:11:50,000 --> 00:12:00,000
The realization that we gain from meditation is that there's no positive or negative experience whatsoever.

101
00:12:00,000 --> 00:12:26,000
It's our reaction to things based on an accumulated preference and habit of needing things to be a certain way of compartmentalizing reality.

102
00:12:26,000 --> 00:12:30,000
Of thinking that somehow these things are going to make us happy.

103
00:12:30,000 --> 00:12:36,000
Somehow we're going to arrange our lives so that everything that we like is going to come to us

104
00:12:36,000 --> 00:12:40,000
and everything we don't like is going to stay away from us.

105
00:12:40,000 --> 00:12:43,000
We compartmentalize reality.

106
00:12:43,000 --> 00:12:55,000
We build up these false characterizations, judgements of innocent experience.

107
00:12:55,000 --> 00:13:00,000
And it's to the point that we've stopped looking.

108
00:13:00,000 --> 00:13:02,000
We've stopped examining.

109
00:13:02,000 --> 00:13:08,000
We've stopped appreciating reality for what it is.

110
00:13:08,000 --> 00:13:09,000
We already know the answer.

111
00:13:09,000 --> 00:13:11,000
Pay in bad, okay.

112
00:13:11,000 --> 00:13:14,000
Run. Change position.

113
00:13:14,000 --> 00:13:18,000
Go see a doctor. Get rid of it.

114
00:13:18,000 --> 00:13:36,000
And when we look at it, when we look at the pain, we see that it's totally not what we thought.

115
00:13:36,000 --> 00:13:42,000
It's something completely different from what we thought of it.

116
00:13:42,000 --> 00:13:49,000
When we look at it, we watch it and we remind ourselves that what it is.

117
00:13:49,000 --> 00:13:51,000
We teach ourselves, okay.

118
00:13:51,000 --> 00:13:56,000
We see this. This is pain.

119
00:13:56,000 --> 00:13:58,000
Okay. That wasn't so bad.

120
00:13:58,000 --> 00:14:00,000
And we look at it again and we're paying.

121
00:14:00,000 --> 00:14:04,000
We just remind ourselves that this pain and pain.

122
00:14:04,000 --> 00:14:11,000
And as we open up to the experience, we see that there's nothing negative about pain at all.

123
00:14:11,000 --> 00:14:25,000
It's one of the most liberating experiences that come in the meditation practice during the time of your practicing.

124
00:14:25,000 --> 00:14:34,000
It's the ability to conquer the pain.

125
00:14:34,000 --> 00:14:42,000
You can get to the point where pains that have been haunting you through your whole practice for days and days.

126
00:14:42,000 --> 00:14:46,000
Suddenly vanish. Suddenly you conquer the pain.

127
00:14:46,000 --> 00:14:50,000
Your mind changes. Your mind shifts.

128
00:14:50,000 --> 00:14:53,000
And it gives in. You know, you keep going back to it.

129
00:14:53,000 --> 00:14:55,000
You say, you know, it's only pain.

130
00:14:55,000 --> 00:14:59,000
Reminding yourself. It's only pain. It is what it is.

131
00:14:59,000 --> 00:15:04,000
It's not bad. It's not good. It's not me. It's not mine.

132
00:15:04,000 --> 00:15:09,000
It's not a problem. It's pain.

133
00:15:09,000 --> 00:15:16,000
And if you do this enough, your mind shifts and your mind says, oh, it's pain.

134
00:15:16,000 --> 00:15:23,000
And it's like you've conquered an enemy. Suddenly there's no fear.

135
00:15:23,000 --> 00:15:31,000
There's no anger or upset towards that pain.

136
00:15:31,000 --> 00:15:36,000
There's a confidence.

137
00:15:36,000 --> 00:15:41,000
There's a understanding and a surety.

138
00:15:41,000 --> 00:15:46,000
You're certain in your mind.

139
00:15:46,000 --> 00:15:53,000
You're not worried about it anymore. The pain comes. Let it come.

140
00:15:53,000 --> 00:15:56,000
And then it just disappears.

141
00:15:56,000 --> 00:16:01,000
Like it was never there in the first place.

142
00:16:01,000 --> 00:16:06,000
It's just an example. All of our experience is like this.

143
00:16:06,000 --> 00:16:12,000
No pain is a difficult one because it's ingrained in our minds that pain

144
00:16:12,000 --> 00:16:16,000
means there's a problem. The body is trying to tell us something we say.

145
00:16:16,000 --> 00:16:21,000
But there's so much more that we can point out that it's in the very same vein.

146
00:16:21,000 --> 00:16:28,000
When we hear things, as I said, we can get so angry when children come

147
00:16:28,000 --> 00:16:34,000
and they're bothering us. Children can be very annoying sometimes.

148
00:16:34,000 --> 00:16:37,000
But there's nothing annoying about the child. It's in our own mind.

149
00:16:37,000 --> 00:16:42,000
We get annoyed. We get angry.

150
00:16:42,000 --> 00:16:47,000
We don't really hear the sound. Maybe we're busy with something else.

151
00:16:47,000 --> 00:16:52,000
And so we've compartmentalized reality. This sound is bad.

152
00:16:52,000 --> 00:16:56,000
This work that I'm focusing on is good.

153
00:16:56,000 --> 00:16:59,000
And you can only accept part of reality.

154
00:16:59,000 --> 00:17:03,000
The part that you can't accept, you have to stop. You have to block.

155
00:17:03,000 --> 00:17:10,000
You have to shout at the child or hit it or so on.

156
00:17:10,000 --> 00:17:15,000
And we work this way.

157
00:17:15,000 --> 00:17:24,000
One of the interesting things people often, we have this

158
00:17:24,000 --> 00:17:34,000
we have this adage, this idiom about truth.

159
00:17:34,000 --> 00:17:39,000
And it doesn't take much explaining to understand where this comes from.

160
00:17:39,000 --> 00:17:41,000
We do believe this, you know, truth hurts.

161
00:17:41,000 --> 00:17:49,000
And it's kind of like a necessary evil sometimes to tell people the truth.

162
00:17:49,000 --> 00:17:54,000
You know, we say, you know, it would hurt someone if you said, you know, you're fat by the way you're fat,

163
00:17:54,000 --> 00:17:59,000
or you're ugly, or you're too tall, or this or that.

164
00:17:59,000 --> 00:18:05,000
I mean, there's certain truths that we say you would never say to someone.

165
00:18:05,000 --> 00:18:08,000
Boy, you're old, there's one.

166
00:18:08,000 --> 00:18:15,000
Look at all those wrinkles on your face or whatever.

167
00:18:15,000 --> 00:18:19,000
You look very bad in those glasses or whatever.

168
00:18:19,000 --> 00:18:24,000
These truths that dress looks terrible on you.

169
00:18:24,000 --> 00:18:30,000
Sometimes we say telling the truth is painful.

170
00:18:30,000 --> 00:18:37,000
But in all of these examples, even these examples that are so taboo,

171
00:18:37,000 --> 00:18:43,000
and in fact, it's funny because in Thailand many of them are not taboo.

172
00:18:43,000 --> 00:18:48,000
It's funny how culture defines what is right and what is wrong.

173
00:18:48,000 --> 00:18:52,000
I mean, in America, if you call someone fat, it's just so, I don't know in America.

174
00:18:52,000 --> 00:19:05,000
In Canada, it would be just, it would be a total disgrace to call someone fat.

175
00:19:05,000 --> 00:19:07,000
But in Thailand, it's not such a big deal.

176
00:19:07,000 --> 00:19:10,000
And they're often, it's kind of funny in that way.

177
00:19:10,000 --> 00:19:14,000
In Thailand, they ask women how old they are.

178
00:19:14,000 --> 00:19:21,000
And you're supposed to tell them how old you are, things like that.

179
00:19:21,000 --> 00:19:29,000
So we build up these, in some cultures, it's not wrong to be large.

180
00:19:29,000 --> 00:19:31,000
And it's not such a big deal.

181
00:19:31,000 --> 00:19:35,000
Or at least it's not a very negative thing.

182
00:19:35,000 --> 00:19:40,000
I had these two monks in Thailand.

183
00:19:40,000 --> 00:19:47,000
And I think it was going over the edge, but I think it was going over the edge.

184
00:19:47,000 --> 00:19:52,000
And especially as a Western, it was kind of terrible to listen to.

185
00:19:52,000 --> 00:19:55,000
But this one monk, he was making fun of this dark skin month.

186
00:19:55,000 --> 00:19:58,000
The one monk had lights in the other monk had dark skin.

187
00:19:58,000 --> 00:20:02,000
And I guess in Thailand, it's okay to make fun of people because of their skin color.

188
00:20:02,000 --> 00:20:05,000
You know, we were up on this mountain and he said,

189
00:20:05,000 --> 00:20:06,000
oh, we'll leave you up here.

190
00:20:06,000 --> 00:20:09,000
And when we come back, the clouds will all be dark.

191
00:20:09,000 --> 00:20:14,000
The clouds will all be dark because your skin color rubbed off on them.

192
00:20:14,000 --> 00:20:16,000
And I'm just making jokes like this.

193
00:20:16,000 --> 00:20:23,000
And that was horrifying.

194
00:20:23,000 --> 00:20:28,000
But the point is, in some cases, making fun of people I think is always wrong.

195
00:20:28,000 --> 00:20:32,000
I don't think we should ever go that way.

196
00:20:32,000 --> 00:20:36,000
The other one, the other funny thing is the other monk didn't seem offended by it.

197
00:20:36,000 --> 00:20:44,000
Whereas if you said something like that in the West, you might get hit for it.

198
00:20:44,000 --> 00:20:49,000
And so on. If you call someone a buffalo in Thailand, you might get hit in America.

199
00:20:49,000 --> 00:20:51,000
People laugh at it.

200
00:20:51,000 --> 00:21:00,000
And the buffalo's are strong. It's kind of a good thing.

201
00:21:00,000 --> 00:21:03,000
The point is that our perceptions cloud reality.

202
00:21:03,000 --> 00:21:06,000
We don't see things for what they are.

203
00:21:06,000 --> 00:21:13,000
My teacher said, if someone ever calls you a buffalo, just turn around and see if you've got a tail.

204
00:21:13,000 --> 00:21:20,000
If you don't have a tail, you're not a buffalo.

205
00:21:20,000 --> 00:21:27,000
And that's really the, that's really sums it all up.

206
00:21:27,000 --> 00:21:36,000
There's no reason to be upset if it's true, it's true if it's false, it's false.

207
00:21:36,000 --> 00:21:43,000
And so even when someone calls you a nasty name or points out even a real flaw,

208
00:21:43,000 --> 00:21:51,000
you know, we say, oh, the truth hurts. Sometimes people say, look, I hate to tell you, but,

209
00:21:51,000 --> 00:21:56,000
you know, and so on.

210
00:21:56,000 --> 00:21:59,000
So we say, oh, the truth hurts. But the truth doesn't hurt.

211
00:21:59,000 --> 00:22:04,000
And this is one thing I want to stress.

212
00:22:04,000 --> 00:22:09,000
And it's important to stress, I think, because Buddhism is probably

213
00:22:09,000 --> 00:22:15,000
one of the most boring religions out there.

214
00:22:15,000 --> 00:22:21,000
Buddhism in its essence is the ultimate boredom.

215
00:22:21,000 --> 00:22:25,000
It's not, but that's how it's easily perceived.

216
00:22:25,000 --> 00:22:28,000
I mean, there's nothing fancy.

217
00:22:28,000 --> 00:22:31,000
There's nothing, the core of Buddhism.

218
00:22:31,000 --> 00:22:36,000
Now the cultural expression of Buddhism is often very fancy and

219
00:22:36,000 --> 00:22:41,000
flowery and magical and mystical.

220
00:22:41,000 --> 00:22:45,000
But when you get deeper into it and start talking amongst and

221
00:22:45,000 --> 00:22:50,000
start hanging out in monasteries, it's actually, it can be quite dull.

222
00:22:50,000 --> 00:22:54,000
I'm expected to sit still.

223
00:22:54,000 --> 00:22:59,000
You're expected to look at very mundane things.

224
00:22:59,000 --> 00:23:04,000
Most of my Thai students would rather be seeing bright lights and

225
00:23:04,000 --> 00:23:08,000
angels or ghosts or it's exciting for them.

226
00:23:08,000 --> 00:23:12,000
And to them that's Buddhism, but it's not Buddhism.

227
00:23:12,000 --> 00:23:16,000
It's not the Buddhist teaching.

228
00:23:16,000 --> 00:23:19,000
And many people are turned off by this.

229
00:23:19,000 --> 00:23:23,000
I've had people, you know, right away in some of my videos,

230
00:23:23,000 --> 00:23:27,000
I think I use the wrong word or I use the word that's easily

231
00:23:27,000 --> 00:23:32,000
taken the wrong word and that's the word enjoy.

232
00:23:32,000 --> 00:23:37,000
Because the word enjoy is such a positive thing for us to enjoy

233
00:23:37,000 --> 00:23:42,000
something, you know, to enjoy an experience.

234
00:23:42,000 --> 00:23:46,000
And so I would say things like instead of enjoying it.

235
00:23:46,000 --> 00:23:52,000
But the problem, the problem with the word enjoyment is it

236
00:23:52,000 --> 00:23:56,000
means to, it means it carries a connotation of experiencing

237
00:23:56,000 --> 00:23:59,000
something to its fullest.

238
00:23:59,000 --> 00:24:02,000
But that's not what the word means.

239
00:24:02,000 --> 00:24:04,000
The word means to like something.

240
00:24:04,000 --> 00:24:08,000
But we give it this connotation of experiencing it to the fullest.

241
00:24:08,000 --> 00:24:12,000
So people would say, look at what you're telling us to block

242
00:24:12,000 --> 00:24:15,000
things out, to avoid things, to not experiencing things,

243
00:24:15,000 --> 00:24:18,000
experience things to the fullest.

244
00:24:18,000 --> 00:24:30,000
Because this is what the word enjoy means.

245
00:24:30,000 --> 00:24:35,000
And so people say that this is drool, it's dry.

246
00:24:35,000 --> 00:24:40,000
And people are so afraid, like afraid of things like Nirvana.

247
00:24:40,000 --> 00:24:43,000
Remember when I first heard about it, when I first started

248
00:24:43,000 --> 00:24:46,000
practicing Buddhism, that was the major hurdle for me.

249
00:24:46,000 --> 00:24:50,000
It was this idea of Nirvana.

250
00:24:50,000 --> 00:24:52,000
Because that's like no more.

251
00:24:52,000 --> 00:24:57,000
No more sensuality, no more pleasure, nothing.

252
00:24:57,000 --> 00:24:59,000
And it was theoretically it made some sense.

253
00:24:59,000 --> 00:25:03,000
But I was like, man, that's so hard to grasp.

254
00:25:03,000 --> 00:25:12,000
So hard to accept.

255
00:25:12,000 --> 00:25:15,000
And we think of non-enjoyment to pleasures.

256
00:25:15,000 --> 00:25:17,000
People look at the monks' life, and they think of it as

257
00:25:17,000 --> 00:25:19,000
such a terrible, austere life.

258
00:25:19,000 --> 00:25:24,000
Must be full of pain and suffering.

259
00:25:24,000 --> 00:25:26,000
Many people even look at meditation this way.

260
00:25:26,000 --> 00:25:31,000
I think of meditation as torture.

261
00:25:31,000 --> 00:25:35,000
The idea of sitting still with your eyes closed.

262
00:25:35,000 --> 00:25:38,000
And they make up all sorts of, I think it's just rationalizing

263
00:25:38,000 --> 00:25:41,000
when they say how pointless it is and so on.

264
00:25:41,000 --> 00:25:46,000
I think what they really mean is I can't do it.

265
00:25:46,000 --> 00:25:48,000
It's torture.

266
00:25:48,000 --> 00:25:49,000
It's unpleasant.

267
00:25:49,000 --> 00:25:54,000
It's boring.

268
00:25:54,000 --> 00:26:00,000
And so I think this idiom applies to Buddhism the truth hurts.

269
00:26:00,000 --> 00:26:05,000
It applies in the sense that people think this about

270
00:26:05,000 --> 00:26:11,000
practices like Buddhism, and again we're claiming to understand

271
00:26:11,000 --> 00:26:12,000
the truth.

272
00:26:12,000 --> 00:26:14,000
And I would say that's the way it is.

273
00:26:14,000 --> 00:26:24,000
People would much rather wander around in illusion.

274
00:26:24,000 --> 00:26:28,000
I teach on the internet in many different arenas.

275
00:26:28,000 --> 00:26:31,000
And one of them is virtual reality.

276
00:26:31,000 --> 00:26:34,000
So we go in and there's the Deer Park and we're sitting

277
00:26:34,000 --> 00:26:36,000
in front of the Buddha.

278
00:26:36,000 --> 00:26:40,000
And people are coming in with such costumes.

279
00:26:40,000 --> 00:26:44,000
Men dressed up as women, women not wearing clothes.

280
00:26:44,000 --> 00:26:46,000
We're very little clothes.

281
00:26:46,000 --> 00:26:53,000
We've got people with furry heads and tails and wings.

282
00:26:53,000 --> 00:26:58,000
Why we would rather have this is because it's exactly what we want.

283
00:26:58,000 --> 00:26:59,000
We can get what we want.

284
00:26:59,000 --> 00:27:01,000
We can control it.

285
00:27:01,000 --> 00:27:06,000
You can't control reality.

286
00:27:06,000 --> 00:27:08,000
You can't compartmentalize reality.

287
00:27:08,000 --> 00:27:10,000
As I said, you can say, this is good, this is bad.

288
00:27:10,000 --> 00:27:13,000
But you can't say let the bad not come and only the good come.

289
00:27:13,000 --> 00:27:17,000
In illusion you can do that.

290
00:27:17,000 --> 00:27:19,000
Your illusion can be whatever you want.

291
00:27:19,000 --> 00:27:21,000
You go and do this virtual reality and you say,

292
00:27:21,000 --> 00:27:24,000
I want this, I want that, I don't want it.

293
00:27:24,000 --> 00:27:30,000
And 90% of the experience is exactly what you like it to be.

294
00:27:30,000 --> 00:27:33,000
You have to work for it, you have to pay for it.

295
00:27:33,000 --> 00:27:39,000
It becomes an addiction, but you can get it.

296
00:27:39,000 --> 00:27:48,000
So we'd rather be an illusion as we say truth hurts.

297
00:27:48,000 --> 00:27:51,000
But I said it before I'll say it again, truth doesn't hurt.

298
00:27:51,000 --> 00:27:56,000
What hurts is our inability to accept the truth.

299
00:27:56,000 --> 00:27:58,000
Buddhism isn't a boring religion.

300
00:27:58,000 --> 00:28:00,000
It isn't dry.

301
00:28:00,000 --> 00:28:04,000
Being a monk, being a meditator isn't a painful experience.

302
00:28:04,000 --> 00:28:08,000
There's nothing painful about sitting still.

303
00:28:08,000 --> 00:28:14,000
What's painful is our inability to accept things for what they are.

304
00:28:14,000 --> 00:28:21,000
And that comes from our inability to understand things as they are.

305
00:28:21,000 --> 00:28:23,000
Once we understand things for what they are,

306
00:28:23,000 --> 00:28:25,000
we see they're not scary.

307
00:28:25,000 --> 00:28:29,000
They're not negative, they're not unpleasant.

308
00:28:29,000 --> 00:28:33,000
We see that all of that pleasure and happiness we were trying to attain

309
00:28:33,000 --> 00:28:37,000
and chasing after and spending so much of our effort,

310
00:28:37,000 --> 00:28:43,000
just to get the very smallest piece of.

311
00:28:43,000 --> 00:28:53,000
All of that happiness could have been amplified hundreds and hundreds thousands of times.

312
00:28:53,000 --> 00:28:57,000
It's simply by coming to understand the reality in front of us.

313
00:28:57,000 --> 00:29:02,000
Turning this reality into heaven, good things as they are.

314
00:29:02,000 --> 00:29:04,000
We see something that's seen.

315
00:29:04,000 --> 00:29:07,000
We hear something that's hearing.

316
00:29:07,000 --> 00:29:12,000
It's not good, it's not bad, it's not me, it's not mine.

317
00:29:12,000 --> 00:29:17,000
It's not us and them and good, right and wrong and so on.

318
00:29:17,000 --> 00:29:20,000
It is what it is.

319
00:29:20,000 --> 00:29:27,000
Right now there's experience going on every moment.

320
00:29:27,000 --> 00:29:32,000
Once we understand this experience and we can live with it and accept it for what it is,

321
00:29:32,000 --> 00:29:39,000
there's nothing anywhere at any time that can cause suffering for it.

322
00:29:39,000 --> 00:29:46,000
We're said to be freed, we're said to be released from bonded,

323
00:29:46,000 --> 00:29:50,000
and released from suffering.

324
00:29:50,000 --> 00:29:55,000
So this is why we practice.

325
00:29:55,000 --> 00:30:00,000
Why we torture ourselves and go through all this pain and suffering

326
00:30:00,000 --> 00:30:03,000
because we realize that it's not the meditation that's causing our suffering,

327
00:30:03,000 --> 00:30:18,000
it's our own minds, our inability to accept this very ordinary.

328
00:30:18,000 --> 00:30:23,000
A very simple reality.

329
00:30:23,000 --> 00:30:25,000
The truth doesn't hurt.

330
00:30:25,000 --> 00:30:29,000
There's another idiom and that goes, the truth will set you free.

331
00:30:29,000 --> 00:30:35,000
And that's exactly what is meant by the practice of the Buddha's teaching.

332
00:30:35,000 --> 00:30:39,000
When you realize the truth, it will set you free.

333
00:30:39,000 --> 00:30:43,000
So that's the dumb I thought to give today.

334
00:30:43,000 --> 00:30:48,000
Now we'll go on to the more practical part of the evening

335
00:30:48,000 --> 00:30:50,000
and that's still the meditation practice.

336
00:30:50,000 --> 00:31:00,000
So first we'll do mindful frustration and then walk in and consider.

