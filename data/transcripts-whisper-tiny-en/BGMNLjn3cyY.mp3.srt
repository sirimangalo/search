1
00:00:00,000 --> 00:00:07,000
Hello everyone, this is me, I'm here in Thailand, just staying at this

2
00:00:07,000 --> 00:00:25,000
foot seat, and then we'll move a portrait, which is right behind a shopping mall in the middle of Bangkok for any outskirts of Bangkok.

3
00:00:25,000 --> 00:00:51,000
So I just wanted to first of all, make a video to let you know that I'm still alive, and still planning to, from time to time make videos, hopefully this little device here will let me make videos on the go, and some of the quality will be not as good as the regular videos.

4
00:00:51,000 --> 00:01:00,000
It will allow for more flexibility, because I still might be traveling for a little while.

5
00:01:00,000 --> 00:01:20,000
And also to try to revive this, ask a monk thing, so that when people have questions, or when I receive questions that I think might be suitable to make a video about, or make people responded to in this format,

6
00:01:20,000 --> 00:01:23,000
I can make a video.

7
00:01:23,000 --> 00:01:33,000
So, I thought I'd started off by asking a question myself, and giving a hopefully fairly brief answer,

8
00:01:33,000 --> 00:01:35,000
fairly complex issue.

9
00:01:35,000 --> 00:01:40,000
So the question is, how do we solve all of the world's problems?

10
00:01:40,000 --> 00:01:52,000
That's the question, because it can get quite depressing to think about all of the world's problems, to think about global warming, for example.

11
00:01:52,000 --> 00:02:02,000
This orchard over here used to have many, many trees in it, and over here in front of me, pointed out.

12
00:02:02,000 --> 00:02:16,000
There are quite a few trees, and for Bangkok it's quite green, but they say half the trees were destroyed in last year's flooding.

13
00:02:16,000 --> 00:02:26,000
What some people say is because of global warming, or is going to get even worse, because of degradation of the environment.

14
00:02:26,000 --> 00:02:43,000
So as an example, and then the poverty and inequality in the world, and all of the many problems that exist in the world, all of the many challenges that we face as a human race.

15
00:02:43,000 --> 00:02:48,000
So I'm going to try to tackle this in a very short YouTube video.

16
00:02:48,000 --> 00:03:17,000
So the main premise of the answer that we have to understand first, which I'm going to base my answer on, is the importance in distinguishing between the world and reality, or distinguishing between the issues that we face, or the conventional playing field,

17
00:03:17,000 --> 00:03:27,000
or the conventional framework in which we look at the issues that we're faced with, and ultimate reality.

18
00:03:27,000 --> 00:03:52,000
Because ultimate reality isn't a three-dimensional space, it isn't a world, it doesn't admit of people, it doesn't admit societies, it doesn't admit even the earth, it doesn't admit of any of the problems, or so-called problems that we talk about.

19
00:03:52,000 --> 00:04:02,000
I'm going to try to tackle this in being, ultimate reality doesn't have any problems, this is a very important part of the answer here.

20
00:04:02,000 --> 00:04:23,000
Ultimate reality is individual experience, so my experience is ultimate reality, your experience is ultimate reality, this is ultimate reality, and all together, all of these experiences make up the world, and all of our experiences from one moment to the next make up our reality.

21
00:04:23,000 --> 00:04:33,000
And give us this idea, or create in our minds, this concept, so there being problems, there being issues that have to be tackled.

22
00:04:33,000 --> 00:04:52,000
And as a result, we wind up dwelling in conceptual reality, our solutions are all conceptual, about restoring the environment, or creating programs for cycling, or so,

23
00:04:52,000 --> 00:05:06,000
and finding political solutions for war, and political solutions for inequality, and famine, and disease, and so on, and finding cures for sicknesses and so on.

24
00:05:06,000 --> 00:05:20,000
All of this is not the Buddhist answer to the world's problems, because all of the world's problems in ultimate reality are just experience.

25
00:05:20,000 --> 00:05:42,000
The only thing that exists is the experience of the phenomena that arise, and the reactions to them, and the actions that we base, that are based on these reactions and based on these experiences.

26
00:05:42,000 --> 00:05:56,000
So, you know, well, it may seem quite noble and honorable to try to build hospitals, and schools, and developing countries, and to plant trees, and so on.

27
00:05:56,000 --> 00:06:06,000
All of these things, and actually they are wholesome, and they help people, and they make our minds, in some sense, more elevated.

28
00:06:06,000 --> 00:06:13,000
They aren't really solving the problem, and they have no pretend no hope of ever solving the world's problem.

29
00:06:13,000 --> 00:06:22,000
Because they are dealing with the effect, or they are dealing with the extrapolation on the problem.

30
00:06:22,000 --> 00:06:25,000
It's like chasing a shadow.

31
00:06:25,000 --> 00:06:37,000
We are trying to fix the shadows, and we're trying to get rid of a shadow, for example, and we know that a shadow is dependent on the object that casts the shadow.

32
00:06:37,000 --> 00:06:43,000
So, all of these issues in the world are merely based on our individual experiences.

33
00:06:43,000 --> 00:06:54,000
Now, this can be verified from a conventional point of view, for example, we talk about greed being a problem, and greed being a source of many of the world's problems.

34
00:06:54,000 --> 00:06:58,000
Now, from a conventional point of view, it's as easy to see.

35
00:06:58,000 --> 00:07:08,000
You can even understand how people stopped consuming so much, and stopped needing so much, and stopped with so much excess of greed and desire.

36
00:07:08,000 --> 00:07:18,000
The resources on Earth would really be enough to feed everyone, and many, many more people than we already have on Earth, if we were to be content.

37
00:07:18,000 --> 00:07:24,000
But, of course, we're not content. We aren't able to live with what we have, and we always want more and more and more.

38
00:07:24,000 --> 00:07:34,000
And because of this, there's the degradation of the environment, there's more, and there's conflict, and there's economic disparity, and so on.

39
00:07:34,000 --> 00:07:56,000
Even you could say, there's disease that comes from pollution that comes from even just eating meat or livestock production, and all these diseases that come from our inability to be content with simple lives and with simple experiences.

40
00:07:56,000 --> 00:08:10,000
But from an ultimate point of view, it actually goes deeper than that, that when you give up the causes and conditions that make up the framework of our reality, they're causing these problems.

41
00:08:10,000 --> 00:08:17,000
The problems disappear and dissolve for you, because your reality is that experience.

42
00:08:17,000 --> 00:08:32,000
Your reality is your experience of the six senses and the world around you. When you change your reaction, when you change your intentions, then your experience changes.

43
00:08:32,000 --> 00:08:49,000
Why things like the concept of heaven or on angel, the concept of hell, these ideas of being born in such places are not really far-fetched from a Buddhist point of view. This is an effect, a result of having a pure mind.

44
00:08:49,000 --> 00:09:03,000
The person with a pure mind, they have no need to worry about such things as global warming or famine or so on. And if we look a little bit more broadly, then we mostly do.

45
00:09:03,000 --> 00:09:28,000
You see that actually, this is necessary. It's necessary to take this kind of an outlook, or it's logical to take such an outlook. If we're going to be honest with ourselves, because anyone who dedicates their whole existence to fixing the world's problems is really trying to fix something that is impossible to fix.

46
00:09:28,000 --> 00:09:38,000
They're finding a lost cause. Why? Because we know that the earth is unsavable. This earth is going to eventually go to put.

47
00:09:38,000 --> 00:09:48,000
It's going to burn up to a crisp and it's going to eventually put a sun is going to explode or whatever and eventually in some point in the future. There will be no human race.

48
00:09:48,000 --> 00:10:06,000
There will be no humans in this solar system. Somehow we find a way to go to another planet, but the point being that the universe is far more than the issues that we run or we chase around and we try to solve.

49
00:10:06,000 --> 00:10:35,000
The true issues are our universal and unlimited eternal experiences, an ending experience and how to interact with that and how to react to that, how to become free from the problems that are inherent in this chasing after things and continuing on.

50
00:10:35,000 --> 00:10:50,000
Okay, so that is how I want to frame this. Now, that's basically the answer to this is coming back to the present moment and obviously undertaking the practice and obviously what I'm trying to say undertake the practice of meditation.

51
00:10:50,000 --> 00:11:15,000
Because once we come to look at reality here and now and to see things as they are and to just get real to stop having all these ideas and all these grand delusions, really delusions of grand or the idea that, oh, we're going to save the world and oh, becoming so passionate about causes like economic equality and so on and so on.

52
00:11:15,000 --> 00:11:24,000
Rather than actually doing something to purify our mind, we wind up getting more and more caught up in that.

53
00:11:24,000 --> 00:11:32,000
So the answer is for us to pull out of it and to become free and this has an effect from both ourselves and for the people around us and so on.

54
00:11:32,000 --> 00:11:48,000
And it's really the only way out because it's the only the only solution that is based on on through reality what it's really going on. So that's the first that's the basic framework now based on this.

55
00:11:48,000 --> 00:12:03,000
And sort of as an outline of how we how would what I mean by the practice of meditation I'd like to outline for four points to keep in mind or for aspects of the solution.

56
00:12:03,000 --> 00:12:08,000
The first one is our solution to the world's problems have to be here.

57
00:12:08,000 --> 00:12:25,000
And we can't solve other people's problems, you can give people this sort of advice that I'm giving or you can give people other advice and you know, if you think that that's the right way, you explain to them this is how you're solving and so on.

58
00:12:25,000 --> 00:12:38,000
But it's up to each and every one of us to solve our problems, you can't go and solve the problems in another country or or even solve the problems in the person next to you, you can only solve the problems that are in your own mind.

59
00:12:38,000 --> 00:13:01,000
This is what do we mean by meditation doesn't mean to go around and preach to people, the meditation is not this video it's not spreading these teachings or even watching these meditation is when you yourself begin to look at your reality begin to take apart this experience and see it for what it is piece by piece by piece.

60
00:13:01,000 --> 00:13:12,000
Until finally your mind becomes straight about it, you no longer have greed, you no longer have anger and you no longer have the delusion that causes you to do and say and think that thing.

61
00:13:12,000 --> 00:13:17,000
And so first thing is it has to be here and here means you have to start with yourself.

62
00:13:17,000 --> 00:13:34,000
Number two, it has to be now, we can't be worrying about problems in the future, we can't be thinking oh maybe 10 years, 20 years, 100 years down the road or sometime in the future there's going to be these problems and we have to worry about them and we have to concern about the future.

63
00:13:34,000 --> 00:14:02,000
If you do that you're no longer in touch with reality, the Buddha said this is like grass that when you cut it off it's no longer able to receive the water from its roots and therefore it dries up, your mind dries up and you wind up in great suffering and this is what leads to of course worry and fear and as a result anger and greed and so on and all of these things.

64
00:14:02,000 --> 00:14:12,000
Just like grass that is cut off at the root so the person who is uprooted from the present moment is unable to find the answers to their problems.

65
00:14:12,000 --> 00:14:21,000
The past as well we can't go back to we can be angry about the past or trying to make up for the past or we've heard other people who have done bad things.

66
00:14:21,000 --> 00:14:41,000
We have to be here in the present doing the best we can we have to give up the past if you've done bad things if you've had many problems in the past you have to stop seeing your problems as something that have been going on for years and years like I have an addiction or or I have this who grudge in this hatred and I have this problem with this person.

67
00:14:41,000 --> 00:15:03,000
You have to take it here and now we have to be able to give up the past to make a break from the past and say that's who I was that's completely gone and we'll never arise again and it doesn't have to affect the way I act right now I can act in any way I want in this moment.

68
00:15:03,000 --> 00:15:14,000
We have to take that and change our life for you know once and for all we have to make a break from the past and stay with the future so number.

69
00:15:14,000 --> 00:15:32,000
Number three we have to our our our solutions have to be based on reality so I've already discussed as much as the basis of our solutions here are here and now but they can't be based on.

70
00:15:32,000 --> 00:15:48,000
Our living conditions or they can't be based on our our our our physical health or they can't be based on on our relationships with other people they have to be based on alternate reality and what we mean by ultimate reality is experience.

71
00:15:48,000 --> 00:16:00,000
So when you see something that is your reality and there's a problem because when you see things you become attached when you hear things that's where the problem arises because when you hear you become attached.

72
00:16:00,000 --> 00:16:10,000
When you feel pain in the body you become attached and angry and upset about when you feel pleasure in the body you become attached to it you like it you want more.

73
00:16:10,000 --> 00:16:26,000
These are the problems this is the ultimate reality this is where the problem exists and this is where we have to where we have to tackle the fourth aspect of the solution is that our.

74
00:16:26,000 --> 00:16:33,000
Once we focus on reality it has to be a focus on the goodness of reality so the fourth aspect is goodness.

75
00:16:33,000 --> 00:16:42,000
That we can't simply be doing this for intellectual purposes and we have to be clear that our intention is to become a better person is to purify our mind.

76
00:16:42,000 --> 00:16:58,000
The Buddhist teaching is for the purpose of purifying the mind this is what makes it special that it actually has a way of becoming a bringing people to freedom from greed freedom from anger freedom from delusion to have no.

77
00:16:58,000 --> 00:17:22,000
Unwholesome unskillful useless mind state to have a mind that is pure a mind that acts and speaks and things from a pure and the wise mind in an appropriate way at all times so as we practice meditation as we learn more about ourselves our focus should be on for the purpose.

78
00:17:22,000 --> 00:17:32,000
Making ourselves a better person is this the ultimate test because once we're a better person it's like the source of a river.

79
00:17:32,000 --> 00:17:48,000
If the source of the river is impure and every all the way down the river the whole river will be tainted and is useless and drinkable but if the source of the water is pure then all the way down the river meaning whatever we do when our mind is pure.

80
00:17:48,000 --> 00:18:11,000
Will be will be benefit to other beings any any other being who comes in contact with it based on that pure mind all of the world will begin to change the whole world around us our goodness our purity will free us from suffering and will make us a truly useful truly skillful truly beneficial.

81
00:18:11,000 --> 00:18:29,000
Part of this world and a good example is the Buddha himself one person was able to change so much with the able to do so much for the world and even now people are practicing his teachings and are freeing themselves from so many unpleasant and wholesome.

82
00:18:29,000 --> 00:18:42,000
It's a mind and and state to being people who are addictions and people who have a version of worries and phobias and so on are able to become free from me because of the Buddha session one person.

83
00:18:42,000 --> 00:19:01,000
Is able to influence so many other people who are then able to influence so many other people and even today are able to influence people and change things in the world so this I think is at least one answer on what we can do to solve all of the problems in the world because I truly believe.

84
00:19:01,000 --> 00:19:23,000
And not just believe I believe at a faith but based on what I've come to see in the practice that the answer to the problems lie within ourselves answer to all the problems in the world and that the problems that you see in the world around you that make you depressed that make you feel hopeless and so on are completely solvable completely.

85
00:19:23,000 --> 00:19:34,000
I'm fixable here and now in the present moment here now if you base yourself on reality and develop goodness for aspect.

86
00:19:34,000 --> 00:19:40,000
Please make up one.

87
00:19:40,000 --> 00:19:55,000
The solution that I would offer here to the all of the problems in the world so here's another ask among I've asked myself and I've answered myself and hopefully the system of use for people that you'll be able to put these.

88
00:19:55,000 --> 00:20:17,000
A concept into practice or use this as an encouragement for your practice not just encouragement to watch more of my videos or watch more videos or learn more or so on, but to actually say yeah I should take the time to do some meditation and I hope that you all do and that your meditation is fruitful and that you are able to free yourself from the problems that exist in your mind.

89
00:20:17,000 --> 00:20:27,000
And just for myself this is my task to free myself from the problems that exist in my mind so pushing you all the best and see you next time.

