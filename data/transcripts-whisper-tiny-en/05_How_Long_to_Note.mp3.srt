1
00:00:00,000 --> 00:00:07,920
How long to note, question, for how long should one note a single phenomena answer?

2
00:00:07,920 --> 00:00:19,560
Unwholesome phenomena, such as liking, disliking, worry, fear, doubt, etc, should be noted until they go away.

3
00:00:19,560 --> 00:00:29,200
Unwholesome triggering phenomena like pain, pleasure, or any object one finds interesting, exciting, fearsome,

4
00:00:29,200 --> 00:00:35,040
or otherwise productive of judgment of any kind, should be noted for as long as the mind remains

5
00:00:35,040 --> 00:00:40,640
interested or until they disappear of their own accord, whichever comes first.

6
00:00:40,640 --> 00:00:45,920
In the case of neutral phenomena, one can still note them repeatedly in order to develop

7
00:00:45,920 --> 00:01:15,760
familiarity with their nature of arising and passing away.

