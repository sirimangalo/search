1
00:00:00,000 --> 00:00:01,000
Thank you.

2
00:00:30,000 --> 00:00:59,640
Good evening everyone, today's the second, we're live, but it's just going to turn on

3
00:00:59,640 --> 00:01:00,640
the video.

4
00:01:00,640 --> 00:01:20,640
Good evening everyone, broadcasting live, March 29th.

5
00:01:20,640 --> 00:01:46,640
So, this is an interesting quote, it's fairly specific, but it's talking about anger.

6
00:01:46,640 --> 00:02:12,640
There are these three people existing, found to be existing in the world.

7
00:02:12,640 --> 00:02:38,640
One, two, three, bass, bass, and another leg, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom,

8
00:02:38,640 --> 00:02:47,640
boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom,

9
00:02:47,640 --> 00:02:57,000
a person who is like a carving in a rock, lake, and it's perhaps who are writing the word for

10
00:02:57,000 --> 00:03:05,480
writing game for me, means a scratch, second one, but to be the cool, boom, boom, boom,

11
00:03:05,480 --> 00:03:16,480
a person who is like upa mam means a comparison, it's going to be compared to a scratch

12
00:03:16,480 --> 00:03:28,480
in the earth, a mine drawn in the earth, and the third one, unda kali ku, boom, boom, boom,

13
00:03:28,480 --> 00:03:49,380
boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom,

14
00:03:49,380 --> 00:03:51,640
boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom, boom,

15
00:03:51,640 --> 00:03:57,800
boom, boom, boom, boom, boom, boom, boom, boom, boom boom, boom, boom, boom, boom, boom,

16
00:03:57,800 --> 00:04:06,200
Yeah, it can be either an inscription or a right or a writing.

17
00:04:06,200 --> 00:04:13,120
Bumbly quidges in regards to writing.

18
00:04:13,120 --> 00:04:19,880
So these three types of people, someone, one type of person is like a writing and a rock,

19
00:04:19,880 --> 00:04:26,880
one type of person is like writing and a birth, one person, type of person is like writing

20
00:04:26,880 --> 00:04:31,840
on water.

21
00:04:31,840 --> 00:04:38,640
At the moja bikowie passar nale kupamogogolo, and what sort of person is like writing and

22
00:04:38,640 --> 00:04:39,640
rock?

23
00:04:39,640 --> 00:04:52,240
Oh, someone is a bhinhang gujatee, angry constantly, always going to get angry, and when they're

24
00:04:52,240 --> 00:05:02,640
angry, when they get angry, dig Haratan anusi, they stay angry, where the anger lasts

25
00:05:02,640 --> 00:05:19,120
for a long time, right, yeah, the anger lasts for a long time, just like a carving in

26
00:05:19,120 --> 00:05:20,120
the rock.

27
00:05:20,120 --> 00:05:38,280
That's not so soon, worn away, and then another person is like, what is the person like

28
00:05:38,280 --> 00:05:47,200
writing on earth, they get angry often, a bhinhang gujatee, but their anger doesn't last

29
00:05:47,200 --> 00:05:57,200
for a long time, so just like you can write on earth, you can leave a mark, but the mark

30
00:05:57,200 --> 00:06:08,200
fades away quickly, not like rock, after a good rain, so this sort of person, people get

31
00:06:08,200 --> 00:06:14,560
angry, this is a sort of your average person, someone who gets angry and stays angry, well

32
00:06:14,560 --> 00:06:20,920
that's a, they've got a problem, the person is not able to let go of their anger, that's

33
00:06:20,920 --> 00:06:25,880
a big problem, people who get angry are just ordinary people, they can actually be good

34
00:06:25,880 --> 00:06:34,200
people, good people can get angry, it's not, it's not a good thing, but they can otherwise

35
00:06:34,200 --> 00:06:39,840
be good, it's a question of how they deal with the anger, do they take it out on others,

36
00:06:39,840 --> 00:06:51,360
do they, let it stew and try to find ways to express their anger through an wholesome

37
00:06:51,360 --> 00:06:57,120
deeds, or do they let go of it, do they see the anger for what it is, and this goes

38
00:06:57,120 --> 00:07:03,480
with all of the defounded, it's important because when we meditate, these emotions are going

39
00:07:03,480 --> 00:07:13,080
to come up, we're training, this is why we call it practice, it's not perfect, so when

40
00:07:13,080 --> 00:07:21,200
you practice, sometimes you fail and quite often we fail in terms of seeing things clearly

41
00:07:21,200 --> 00:07:29,160
as a result we get angry, we want something, we're greedy attached to something, or we're

42
00:07:29,160 --> 00:07:37,720
greedy or conceited or self-righteous, any number of things come up, the question is what

43
00:07:37,720 --> 00:07:43,800
we do then, are we able to be mindful of those things, so the best is if you can catch

44
00:07:43,800 --> 00:07:54,040
the experience before anger even comes out or greed or delusion, once it does come up,

45
00:07:54,040 --> 00:08:04,560
it's still possible to salvage the situation, let it go of it, seeing angry, angry,

46
00:08:04,560 --> 00:08:17,840
one thing, one thing, thinking, thinking, and then there's the third type of person

47
00:08:17,840 --> 00:08:23,480
like the water, this is totally different, you can't write on water, it's kind of funny

48
00:08:23,480 --> 00:08:35,280
to think and why it's funny to think of writing on water is because you can't do it,

49
00:08:35,280 --> 00:08:39,680
everybody uses this sort of similarly often, he talks about someone who tries to attack

50
00:08:39,680 --> 00:08:53,120
water, tries to attack it or it tries to splash the water around and destroy the ocean

51
00:08:53,120 --> 00:09:05,680
kind of thing, we should be like water, water is never, you can't write on water, you can't

52
00:09:05,680 --> 00:09:12,360
affect the water substantially, you can't leave your mark, no matter what happens, no matter

53
00:09:12,360 --> 00:09:27,440
what happens to them, when they're spoken to roughly, Paramahana, when they're spoken

54
00:09:27,440 --> 00:09:37,640
to unkindly, a manapana, there's spoken to words that are detestful words that are

55
00:09:37,640 --> 00:09:56,480
not pleasant, but they stay Sunday, whatever that means, Sunday I think, you should

56
00:09:56,480 --> 00:10:12,400
mean when they're, you know, they're probably not good enough.

57
00:10:12,400 --> 00:10:41,840
There we are, at least, there we go, there we go, there we go, there we go, there we go,

58
00:10:41,840 --> 00:10:57,520
that's the line etched in water, remains on friendly terms, mingles and greets him, Sunday

59
00:10:57,520 --> 00:11:04,520
the sun, the ting-y-y-wa, the mo-d-ting-y-wa, the mo-d-ting-y-wa.

60
00:11:04,520 --> 00:11:11,520
The mo-d-ting-y means a degree of a friend.

61
00:11:11,520 --> 00:11:18,520
I don't think he's got this in the right or a Sunday, ting-y means continues to stay with that person.

62
00:11:18,520 --> 00:11:25,520
Doesn't have any sort of, doesn't hope, have any some grudge when people are mean to them

63
00:11:25,520 --> 00:11:33,520
when people do and say things.

64
00:11:33,520 --> 00:11:37,520
What this made me think of, today I taught meditation throughout the day.

65
00:11:37,520 --> 00:11:41,520
Well, I was at till about three, thirty.

66
00:11:41,520 --> 00:11:48,520
And two people, actually more than two, a bunch of skeptics.

67
00:11:48,520 --> 00:11:53,520
I think everyone's thinking too much because of exams coming up or something.

68
00:11:53,520 --> 00:12:00,520
But one of them was really bad.

69
00:12:00,520 --> 00:12:06,520
It's this, people take the easy way out of an argument,

70
00:12:06,520 --> 00:12:08,520
but it's called the eel wrigglers.

71
00:12:08,520 --> 00:12:14,520
In the Buddhist time, they call them people who wriggle like an eel.

72
00:12:14,520 --> 00:12:17,520
I think it's the kind of sort of what we call it.

73
00:12:17,520 --> 00:12:24,520
But today the moral relativist, for example, someone who refuses to take a stand and says

74
00:12:24,520 --> 00:12:29,520
there is no absolute truth.

75
00:12:29,520 --> 00:12:34,520
And so whatever I said to this guy, he said to someone, it's like this.

76
00:12:34,520 --> 00:12:36,520
I said, we're talking about fear.

77
00:12:36,520 --> 00:12:38,520
He said, well, fear can be useful.

78
00:12:38,520 --> 00:12:41,520
And I said, well, I disagree.

79
00:12:41,520 --> 00:12:46,520
I think you don't need fear to do this or that or something that you need fear for.

80
00:12:46,520 --> 00:12:53,520
But then I said, well, what about let's talk about irrational fear?

81
00:12:53,520 --> 00:12:56,520
Like suppose you see a mouse and you get afraid of the mouse.

82
00:12:56,520 --> 00:12:59,520
And he said, well, the mouse could jot me.

83
00:12:59,520 --> 00:13:02,520
He actually, I think, has some mental problems.

84
00:13:02,520 --> 00:13:09,520
Like he was laughing good maniacally and talking very quickly.

85
00:13:09,520 --> 00:13:16,520
Or maybe he was on drugs, I don't know.

86
00:13:16,520 --> 00:13:19,520
But he said, well, the mouse could jump up and bite you.

87
00:13:19,520 --> 00:13:23,520
But I mean, it was a ridiculous conversation in the end.

88
00:13:23,520 --> 00:13:27,520
And then I said something that I think is a good point that has to be made.

89
00:13:27,520 --> 00:13:29,520
But I'll mention that.

90
00:13:29,520 --> 00:13:36,520
But why I bring it up is this idea that, well, actually,

91
00:13:36,520 --> 00:13:39,520
it does tie in with my point.

92
00:13:39,520 --> 00:13:40,520
And he tried to respond.

93
00:13:40,520 --> 00:13:42,520
I said, no, you don't talk.

94
00:13:42,520 --> 00:13:43,520
You let me talk.

95
00:13:43,520 --> 00:13:45,520
And I said, and he tried to talk.

96
00:13:45,520 --> 00:13:48,520
He said, no, let me finish.

97
00:13:48,520 --> 00:13:51,520
And I said, if you're going to learn meditation,

98
00:13:51,520 --> 00:13:54,520
if you're going to sit here and we're going to progress,

99
00:13:54,520 --> 00:13:56,520
you have to agree.

100
00:13:56,520 --> 00:13:59,520
There are certain things we have to agree on.

101
00:13:59,520 --> 00:14:01,520
You don't agree with me on these things then I can't help you.

102
00:14:01,520 --> 00:14:06,520
And so we have to agree on is that there are certain things

103
00:14:06,520 --> 00:14:07,520
that we could do with them.

104
00:14:07,520 --> 00:14:10,520
Certain things that we would be better off with them.

105
00:14:10,520 --> 00:14:12,520
And he said, well, you want to hear my take on that?

106
00:14:12,520 --> 00:14:14,520
I said, no.

107
00:14:14,520 --> 00:14:16,520
And I said, do you either agree with me?

108
00:14:16,520 --> 00:14:17,520
Or you don't?

109
00:14:17,520 --> 00:14:19,520
I said, well, I don't agree with you.

110
00:14:19,520 --> 00:14:20,520
I don't.

111
00:14:20,520 --> 00:14:21,520
And I said, OK.

112
00:14:21,520 --> 00:14:22,520
And he left.

113
00:14:22,520 --> 00:14:27,520
I was like, why this made me think of that.

114
00:14:27,520 --> 00:14:34,520
And this is something, this idea of getting angry.

115
00:14:34,520 --> 00:14:37,520
I mean, it shows a progression here of three types of people.

116
00:14:37,520 --> 00:14:43,520
And there's an obvious implication here is that anger's not a good thing.

117
00:14:43,520 --> 00:14:47,520
And we should strive to be like a person.

118
00:14:47,520 --> 00:14:52,520
I mean, I think the Buddha was sort of exceptional in just laying these out.

119
00:14:52,520 --> 00:14:57,520
And he often did this.

120
00:14:57,520 --> 00:14:59,520
He didn't so often say, here's a person who is angry and that's bad.

121
00:14:59,520 --> 00:15:01,520
Don't be that person.

122
00:15:01,520 --> 00:15:05,520
He didn't so often talk like that.

123
00:15:05,520 --> 00:15:10,520
Because he wanted to lay things out in terms of reality.

124
00:15:10,520 --> 00:15:13,520
To say there are these three types of person.

125
00:15:13,520 --> 00:15:16,520
But why is he teaching this?

126
00:15:16,520 --> 00:15:20,520
If just to lay them out and say, OK, yeah, all three of these are equal.

127
00:15:20,520 --> 00:15:24,520
And there's no, let's not have any judgment about them.

128
00:15:24,520 --> 00:15:27,520
The way he lays it out is quite beautiful.

129
00:15:27,520 --> 00:15:31,520
And anyone who looks at this, the idea is that it resonates with you.

130
00:15:31,520 --> 00:15:37,520
Just by reading it, you resonate and you incline towards the person.

131
00:15:37,520 --> 00:15:42,520
It was like writing on water because we're familiar with these.

132
00:15:42,520 --> 00:15:46,520
And it is innately unpleasant to us.

133
00:15:46,520 --> 00:15:54,520
It's discordant for someone to be often angry and moreover to hold on to anger alone.

134
00:15:54,520 --> 00:15:56,520
Anger is inherently bad.

135
00:15:56,520 --> 00:15:58,520
This is the point.

136
00:15:58,520 --> 00:16:05,520
If you don't agree with that, and it's such an easy thing to disagree and say,

137
00:16:05,520 --> 00:16:09,520
well, maybe there's something good about it.

138
00:16:09,520 --> 00:16:14,520
This guy took it to the extreme where there's this idea that you see a mouse.

139
00:16:14,520 --> 00:16:17,520
And maybe it would jump on you.

140
00:16:17,520 --> 00:16:21,520
Really, you're really going to go there.

141
00:16:21,520 --> 00:16:26,520
And I'm just trying to point out that we can act irrationally and you wouldn't even accept that.

142
00:16:26,520 --> 00:16:30,520
Well, maybe it's rational to them and that kind of thing.

143
00:16:30,520 --> 00:16:34,520
This is really a proper stance you can take.

144
00:16:34,520 --> 00:16:41,520
But it does sort of exemplify the idea of things like moral relatives

145
00:16:41,520 --> 00:16:45,520
where everything is relative.

146
00:16:45,520 --> 00:16:50,520
There's nothing really good or bad.

147
00:16:50,520 --> 00:16:57,520
But I think there's some truth to it in the sense that everything is just things.

148
00:16:57,520 --> 00:16:59,520
But our minds don't work that way.

149
00:16:59,520 --> 00:17:02,520
Our minds do have an intrinsic compass.

150
00:17:02,520 --> 00:17:09,520
And so when you read this, you don't have to be told you'd better be the person without anger.

151
00:17:09,520 --> 00:17:15,520
And the Buddha lays it out just so beautifully that you can read it and get the impression.

152
00:17:15,520 --> 00:17:23,520
And you think, yeah, I don't want to be that person who is like a scratch on rock.

153
00:17:23,520 --> 00:17:27,520
And I want to be the person who holds on to anger.

154
00:17:27,520 --> 00:17:30,520
I don't even want to be the person who gets angry.

155
00:17:30,520 --> 00:17:34,520
Think of this person who is, you know, when someone speaks harshly to them

156
00:17:34,520 --> 00:17:40,520
and they're still friendly and kind to the person, wow, you know, that resonates.

157
00:17:40,520 --> 00:17:44,520
You don't have to say which one is better, we know.

158
00:17:44,520 --> 00:17:50,520
Unless we're, you know, this is the problem is we've gotten so messed up.

159
00:17:50,520 --> 00:17:58,520
And now we have these television shows.

160
00:17:58,520 --> 00:18:01,520
There's this one called, you know, everyone knows of it.

161
00:18:01,520 --> 00:18:04,520
I'm just hearing about it called Game of Thrones.

162
00:18:04,520 --> 00:18:08,520
And apparently it's full of morally corrupt characters.

163
00:18:08,520 --> 00:18:09,520
And this is the kind of thing.

164
00:18:09,520 --> 00:18:12,520
And then there was another one, my brother, I said, you know, I'm not watching this,

165
00:18:12,520 --> 00:18:13,520
called Breaking Bad.

166
00:18:13,520 --> 00:18:15,520
And it's apparently about a guy.

167
00:18:15,520 --> 00:18:16,520
I mean, all of you know about this.

168
00:18:16,520 --> 00:18:19,520
I'm sure all of you probably watched it.

169
00:18:19,520 --> 00:18:24,520
It's about this guy who makes mess.

170
00:18:24,520 --> 00:18:26,520
And so, you know, he's a good guy.

171
00:18:26,520 --> 00:18:31,520
And then he over, my father explained it to me over seven seasons.

172
00:18:31,520 --> 00:18:34,520
He breaks bad, he turns bad.

173
00:18:34,520 --> 00:18:37,520
And I don't know.

174
00:18:37,520 --> 00:18:41,520
I mean, they were arguing saying, you know, it's actually not glorifying this.

175
00:18:41,520 --> 00:18:48,520
But my point being, I mean, I think all of this is indicative of some kind of,

176
00:18:48,520 --> 00:18:53,520
you know, I don't want to say lack of compass because I think the compass is there.

177
00:18:53,520 --> 00:18:58,520
I think inherently in reality there is ethics.

178
00:18:58,520 --> 00:19:03,520
You know, ethics, reality is not atoms.

179
00:19:03,520 --> 00:19:05,520
Reality is experience.

180
00:19:05,520 --> 00:19:08,520
And it has an inherent ethic to it.

181
00:19:08,520 --> 00:19:10,520
We don't want to suffer.

182
00:19:10,520 --> 00:19:13,520
We can try to convince ourselves that we do want to,

183
00:19:13,520 --> 00:19:15,520
just for the sake of convincing ourselves.

184
00:19:15,520 --> 00:19:19,520
But if you're just twisting yourself, tying yourself in knots.

185
00:19:19,520 --> 00:19:20,520
Anger is bad.

186
00:19:20,520 --> 00:19:22,520
You can pretend that it's good.

187
00:19:22,520 --> 00:19:24,520
And you can start to believe that it's good.

188
00:19:24,520 --> 00:19:26,520
And you'll just fall flat on your face.

189
00:19:26,520 --> 00:19:28,520
Tying yourself in knots.

190
00:19:28,520 --> 00:19:34,520
Until finally you get tired and you come back to square one where anger is bad.

191
00:19:34,520 --> 00:19:36,520
So you say anger is good.

192
00:19:36,520 --> 00:19:40,520
And then you try it out and it ends up messing everything up for you.

193
00:19:40,520 --> 00:19:45,520
At the very least we can say there are desires in the mind.

194
00:19:45,520 --> 00:19:53,520
And our desires conflict with their goals.

195
00:19:53,520 --> 00:19:56,520
They're inconsistent.

196
00:19:56,520 --> 00:20:05,520
So I want to beat someone up because that will make me happy.

197
00:20:05,520 --> 00:20:09,520
You beat someone up and then you're not happy.

198
00:20:09,520 --> 00:20:13,520
I want to rob a bank so I can be rich and enjoy the money.

199
00:20:13,520 --> 00:20:20,520
And if you get away with it, the world doesn't work that way.

200
00:20:20,520 --> 00:20:26,520
You end up just perverting your experience.

201
00:20:26,520 --> 00:20:28,520
And you become inconsistent.

202
00:20:28,520 --> 00:20:30,520
So you can say you want well.

203
00:20:30,520 --> 00:20:31,520
It's all things are equal.

204
00:20:31,520 --> 00:20:32,520
It doesn't matter.

205
00:20:32,520 --> 00:20:34,520
But you don't think that way.

206
00:20:34,520 --> 00:20:37,520
You're full of desires and your desires are conflicting

207
00:20:37,520 --> 00:20:40,520
and your desires are inconsistent.

208
00:20:40,520 --> 00:20:46,520
And you do something because you want to be happy, but it makes you unhappy.

209
00:20:46,520 --> 00:20:50,520
So you can say, well that's fine, but no, it's really not.

210
00:20:50,520 --> 00:20:56,520
You're not because you're not impartial.

211
00:20:56,520 --> 00:21:03,520
And your impartiality or your partiality is messing you up.

212
00:21:03,520 --> 00:21:05,520
Because partial to things that are hurting you.

213
00:21:05,520 --> 00:21:17,520
And partial to things that aren't fulfilling your wishes.

214
00:21:17,520 --> 00:21:21,520
I just thought it was an opportunity to talk about this ridiculous.

215
00:21:21,520 --> 00:21:24,520
Because it seems like a sound idea.

216
00:21:24,520 --> 00:21:29,520
Why is there this moral absolutism or any kind of absolutism?

217
00:21:29,520 --> 00:21:32,520
Absolute truth kind of thing.

218
00:21:32,520 --> 00:21:37,520
But finding all it is is, I would say it's intellectual dishonesty

219
00:21:37,520 --> 00:21:40,520
to suddenly come out and say, oh, there is no truth.

220
00:21:40,520 --> 00:21:41,520
Well anyone can say that.

221
00:21:41,520 --> 00:21:45,520
I mean, how hard is it to say, I don't believe this.

222
00:21:45,520 --> 00:21:46,520
I don't believe that.

223
00:21:46,520 --> 00:21:50,520
And so this is the Buddha himself attacked this idea.

224
00:21:50,520 --> 00:21:54,520
You know, that there's, that you can't pin someone down.

225
00:21:54,520 --> 00:21:55,520
What do you believe?

226
00:21:55,520 --> 00:21:59,520
Oh, well, you know, what's there to believe?

227
00:21:59,520 --> 00:22:01,520
I don't say this.

228
00:22:01,520 --> 00:22:06,520
I don't say that and, you know, I don't say anything basically.

229
00:22:06,520 --> 00:22:10,520
And when anyone tries to try is the hard work.

230
00:22:10,520 --> 00:22:13,520
Tries to do the hard, the difficult thing.

231
00:22:13,520 --> 00:22:14,520
And find an answer.

232
00:22:14,520 --> 00:22:17,520
Find the truth, right?

233
00:22:17,520 --> 00:22:23,520
I mean, it's like in this day and age, trying to find the truth is seen as naive.

234
00:22:23,520 --> 00:22:30,520
Is seen as brainwashed or fanatic, you know, all the religions of the world claim to have truth.

235
00:22:30,520 --> 00:22:34,520
You know, obviously none of them do, right?

236
00:22:34,520 --> 00:22:38,520
So they have all these ridiculous claims about God and Heaven and so on.

237
00:22:38,520 --> 00:22:41,520
That are unsubstantiated.

238
00:22:41,520 --> 00:22:45,520
And I mean, so that's kind of the backing as a result of all that.

239
00:22:45,520 --> 00:22:47,520
People have given up the search for the truth.

240
00:22:47,520 --> 00:22:53,520
And they've come upon this with seems like a better idea because it works.

241
00:22:53,520 --> 00:22:55,520
It resonates.

242
00:22:55,520 --> 00:22:57,520
But what it resonates because it's just so easy.

243
00:22:57,520 --> 00:22:59,520
You don't have to believe anything.

244
00:22:59,520 --> 00:23:03,520
You drink and be married.

245
00:23:03,520 --> 00:23:04,520
But it's easy.

246
00:23:04,520 --> 00:23:07,520
It's lazy, I guess, is what I'm trying to say.

247
00:23:07,520 --> 00:23:12,520
Much harder to try and find the truth.

248
00:23:12,520 --> 00:23:23,520
Obviously, millions of orders of magnitude are more difficult because everyone tries to find the truth and family.

249
00:23:23,520 --> 00:23:29,520
And so now it's like we're just giving up, which is just laziness.

250
00:23:29,520 --> 00:23:31,520
So we make the same claim.

251
00:23:31,520 --> 00:23:33,520
We claim to have found the truth.

252
00:23:33,520 --> 00:23:36,520
And not only the truth, but the noble truth.

253
00:23:36,520 --> 00:23:39,520
So we don't worry about all truths.

254
00:23:39,520 --> 00:23:43,520
We just worry about the truths that are important.

255
00:23:43,520 --> 00:23:47,520
And so we have these four noble truths.

256
00:23:47,520 --> 00:23:54,520
And that's basically what I was saying, if you don't agree that certain things are suffering.

257
00:23:54,520 --> 00:24:01,520
If you don't want to be free from suffering, if you don't understand this idea and it doesn't resonate with you,

258
00:24:01,520 --> 00:24:02,520
then I've got nothing to say.

259
00:24:02,520 --> 00:24:03,520
It's fine.

260
00:24:03,520 --> 00:24:06,520
You know, he wanted to talk about everything's okay.

261
00:24:06,520 --> 00:24:07,520
I'm sure it's okay.

262
00:24:07,520 --> 00:24:12,520
But I have nothing to say to you.

263
00:24:12,520 --> 00:24:18,520
I mean, I guess I would even say even further is that you're all messed up if you don't understand this idea.

264
00:24:18,520 --> 00:24:19,520
But people don't.

265
00:24:19,520 --> 00:24:22,520
And then they say, well, what about wanting to be free from suffering?

266
00:24:22,520 --> 00:24:25,520
A lot of people are antagonistic.

267
00:24:25,520 --> 00:24:29,520
A lot of people, but I do get people who are kind of skeptic, I guess.

268
00:24:29,520 --> 00:24:31,520
Not antagonistic, just skeptic.

269
00:24:31,520 --> 00:24:37,520
Either because of their own religion, which conflicts with mind or because they've given up a religion,

270
00:24:37,520 --> 00:24:41,520
and therefore they're skeptical about all of the genes.

271
00:24:41,520 --> 00:24:46,520
But that's why people don't like to call Buddhism religion because they don't believe in religion.

272
00:24:46,520 --> 00:24:49,520
So I get asked that a lot.

273
00:24:49,520 --> 00:24:52,520
Actually, one of the questions today was Buddhism.

274
00:24:52,520 --> 00:24:55,520
Is Buddhism religion basically?

275
00:24:55,520 --> 00:24:56,520
I didn't answer it.

276
00:24:56,520 --> 00:24:59,520
I was saying, is it really useful at the top of it?

277
00:24:59,520 --> 00:25:03,520
Maybe I was a little bit mean to him, but he was very argumentative.

278
00:25:03,520 --> 00:25:07,520
This is the second guy who is an ex-Seek.

279
00:25:07,520 --> 00:25:10,520
But he still wears the turban, so I thought he was a Seek, and he said,

280
00:25:10,520 --> 00:25:13,520
I just worked for my parents.

281
00:25:13,520 --> 00:25:23,520
I lost my tree, that's not enough.

282
00:25:23,520 --> 00:25:30,520
That's about anger.

283
00:25:30,520 --> 00:25:36,520
It's been a long day, a top meditation today, and just a five minute meditation,

284
00:25:36,520 --> 00:25:40,520
and it teaches not too much.

285
00:25:40,520 --> 00:25:53,520
And then I went to a lecture on perspectives on peace,

286
00:25:53,520 --> 00:25:56,520
and the future of peace studies at McMaster.

287
00:25:56,520 --> 00:26:02,520
They didn't actually, I was thinking they might talk about

288
00:26:02,520 --> 00:26:07,520
peace studies in the future at McMaster, but they didn't really.

289
00:26:07,520 --> 00:26:09,520
I'm interested in peace studies.

290
00:26:09,520 --> 00:26:16,520
I mean, it's a broad topic.

291
00:26:16,520 --> 00:26:21,520
Obviously, I'm more interested in inner peace, but that's a valid part of it.

292
00:26:21,520 --> 00:26:26,520
A lot of it has to do with things like war and conflict.

293
00:26:26,520 --> 00:26:30,520
What we're dealing with in this class are three documents,

294
00:26:30,520 --> 00:26:35,520
basically the Truth and Reconciliation Commission report,

295
00:26:35,520 --> 00:26:40,520
or executive summary, I think it's about the happiness report,

296
00:26:40,520 --> 00:26:50,520
and the UN resolution 70.1, or 70 slash 1, depending on where you write it.

297
00:26:50,520 --> 00:26:54,520
So the TRC is about the First Nations people.

298
00:26:54,520 --> 00:26:59,520
I talked about that before.

299
00:26:59,520 --> 00:27:02,520
And it's interesting as a Buddhist,

300
00:27:02,520 --> 00:27:04,520
how do we relate to this?

301
00:27:04,520 --> 00:27:18,520
Because land claims and culture and self-governance and all these things are not really of interest.

302
00:27:18,520 --> 00:27:21,520
But there's so much more to the situation in Canada.

303
00:27:21,520 --> 00:27:26,520
I mean, we're living on a legacy of not only apartheid,

304
00:27:26,520 --> 00:27:34,520
but cultural genocide is what the TRC calls it.

305
00:27:34,520 --> 00:27:37,520
And that may not sound all that menacing cultural genocide,

306
00:27:37,520 --> 00:27:39,520
but it's just culture.

307
00:27:39,520 --> 00:27:41,520
But it's really more than that.

308
00:27:41,520 --> 00:27:46,520
I guess genocide in general, because today,

309
00:27:46,520 --> 00:27:50,520
she, Dr. Double Day, the professor, gave the lecture.

310
00:27:50,520 --> 00:27:55,520
You know, whatever you call this is the talk.

311
00:27:55,520 --> 00:28:01,520
She mentioned a person, a man who had grown up in the residential schools,

312
00:28:01,520 --> 00:28:06,520
and then tried to go back to his community in the north.

313
00:28:06,520 --> 00:28:10,520
And they didn't recognize him.

314
00:28:10,520 --> 00:28:15,520
Nobody remembered him, but he kind of remembered some of the people in the community.

315
00:28:15,520 --> 00:28:18,520
But it didn't matter because they couldn't talk to each other.

316
00:28:18,520 --> 00:28:20,520
He didn't speak their language.

317
00:28:20,520 --> 00:28:29,520
He had any inkling of his own culture because he'd grown up being told that his culture was wrong.

318
00:28:29,520 --> 00:28:34,520
And so, here's a person who has no place in the world.

319
00:28:34,520 --> 00:28:38,520
Doesn't know who he is and has grown up abused, basically.

320
00:28:38,520 --> 00:28:41,520
It was child abuse for the most part.

321
00:28:41,520 --> 00:28:42,520
I don't know.

322
00:28:42,520 --> 00:28:45,520
I mean, in some cases, they argue it wasn't,

323
00:28:45,520 --> 00:28:48,520
but there was sexual abuse, child abuse.

324
00:28:48,520 --> 00:28:54,520
And it just is child abuse to take people and take kids away from their families and their culture.

325
00:28:54,520 --> 00:28:57,520
Anyway, so what we're dealing with in Canada is,

326
00:28:57,520 --> 00:29:07,520
and what's most interesting to Buddhism is a lot of issues of mental health, suffering,

327
00:29:07,520 --> 00:29:16,520
and suffering in many levels.

328
00:29:16,520 --> 00:29:18,520
So that's interesting.

329
00:29:18,520 --> 00:29:19,520
It is interesting.

330
00:29:19,520 --> 00:29:22,520
It's not immediately something I would focus on,

331
00:29:22,520 --> 00:29:34,520
but I'm interested in it especially in terms of accepting and recognizing the suffering of these people

332
00:29:34,520 --> 00:29:45,520
and doing what we can to set it to move towards some kind of amelioration of the situation.

333
00:29:45,520 --> 00:29:51,520
I've got an exam on Thursday on this, so it is highly on their mind.

334
00:29:51,520 --> 00:29:53,520
Day after tomorrow, the exam.

335
00:29:53,520 --> 00:29:59,520
And the second one is the happiness report, which was put together between,

336
00:29:59,520 --> 00:30:03,520
was commissioned or was started by the king, I think, of Bhutan.

337
00:30:03,520 --> 00:30:06,520
Anyway, Bhutan was involved with this.

338
00:30:06,520 --> 00:30:08,520
But the happiness report is interesting.

339
00:30:08,520 --> 00:30:12,520
It talks about what leads to happiness.

340
00:30:12,520 --> 00:30:17,520
And they identify six factors.

341
00:30:17,520 --> 00:30:19,520
The first one is economy.

342
00:30:19,520 --> 00:30:28,520
And economics is, a country's wealth is actually a good indicator of happiness, apparently.

343
00:30:28,520 --> 00:30:31,520
Which, romantically, we don't want to think of that.

344
00:30:31,520 --> 00:30:39,520
Money is not the cause of happiness, but the object poverty and the country that has a low GDP per capita.

345
00:30:39,520 --> 00:30:46,520
Apparently, the object poverty is causing a lot of suffering.

346
00:30:46,520 --> 00:30:51,520
It's hard to be happy in a piece when your basic needs aren't being.

347
00:30:51,520 --> 00:30:55,520
But another interesting part of that is this idea of equality.

348
00:30:55,520 --> 00:30:59,520
Because in Canada, there are a lot of happy people.

349
00:30:59,520 --> 00:31:04,520
And Canada ranks fairly high on happiness in the world.

350
00:31:04,520 --> 00:31:10,520
But one point a professor made is that not everyone is happy.

351
00:31:10,520 --> 00:31:17,520
They've got happy people, but then you've got the indigenous peoples of Canada suffering terribly.

352
00:31:17,520 --> 00:31:21,520
And Buddhism, that's interesting from the point of view of karma.

353
00:31:21,520 --> 00:31:24,520
So we don't think of peoples as peoples.

354
00:31:24,520 --> 00:31:27,520
We're all just people, and we're all just beings.

355
00:31:27,520 --> 00:31:29,520
So we take turns.

356
00:31:29,520 --> 00:31:34,520
I probably haven't been white my whole life.

357
00:31:34,520 --> 00:31:35,520
I was probably Asian.

358
00:31:35,520 --> 00:31:38,520
That's why I'm not such a connection with Asian.

359
00:31:38,520 --> 00:31:40,520
But we switch.

360
00:31:40,520 --> 00:31:46,520
And so the first one now will later be last.

361
00:31:46,520 --> 00:31:50,520
We might be on top now.

362
00:31:50,520 --> 00:31:56,520
But if we crush those underneath us, guess where we're going in the next life.

363
00:31:56,520 --> 00:32:00,520
I mean, it makes sense. It's logical that it's probably happened like that.

364
00:32:00,520 --> 00:32:06,520
Not exactly. It's very complicated, but there's that kind of trend.

365
00:32:06,520 --> 00:32:10,520
And so the idea of equality may be from a Buddhist point of view.

366
00:32:10,520 --> 00:32:23,520
I mean, like at all issues, it's only going to happen when we start acting with equality.

367
00:32:23,520 --> 00:32:32,520
We start basing our actions on helping other people, which I guess is the point.

368
00:32:32,520 --> 00:32:50,520
Anyway, so we've got GDP, and then you've got healthy life expectancy, social support, trust in the society.

369
00:32:50,520 --> 00:32:58,520
Trust in the government, when the government is corrupt and so on, that has an effect on happiness.

370
00:32:58,520 --> 00:33:04,520
And then perceived freedom and generosity.

371
00:33:04,520 --> 00:33:06,520
Number six is generosity.

372
00:33:06,520 --> 00:33:09,520
I think it's number six because it's the least important, which is odd.

373
00:33:09,520 --> 00:33:17,520
I'm not really probably have to do more studying to really understand these six, but I've been a bit busy.

374
00:33:17,520 --> 00:33:27,520
But you know, the idea of happiness report, the whole point of it is to look at progress in the world and development in a different way,

375
00:33:27,520 --> 00:33:36,520
to not look at it in terms of simply money, look at it in terms of happiness.

376
00:33:36,520 --> 00:33:41,520
Look at it, and I'll put the people first instead of the state.

377
00:33:41,520 --> 00:33:49,520
It's a state doing well as our state rich and so on. It's not so important as are the people happy.

378
00:33:49,520 --> 00:34:00,520
So that's the idea of the happiness report is to change the way we look at progress, and to focus more on happiness.

379
00:34:00,520 --> 00:34:07,520
This is very Buddhist, there's Bhutan's Buddhist country, so it's got the same sort of ideas as we do.

380
00:34:07,520 --> 00:34:16,520
The third one we're looking at is the UN Resolution 70-1, which is based on the Millennium Goals.

381
00:34:16,520 --> 00:34:25,520
It's now these 2030 goals, but by the year 2030 we should have ended poverty.

382
00:34:25,520 --> 00:34:39,520
And we should have ended climate change, ended our, we should have solved.

383
00:34:39,520 --> 00:34:48,520
At this point it's probably too late to solve, but we should have done what we can by 2030 to get things back on track.

384
00:34:48,520 --> 00:34:56,520
And so our prop is raving about this. She's really an exceptional person who had done so much.

385
00:34:56,520 --> 00:35:05,520
She's got, she was reading off, she's got like two, she's got a PhD and Bachelor of Education and the law degree.

386
00:35:05,520 --> 00:35:13,520
And she's been just about everything, done a lot of work with the UN and stuff.

387
00:35:13,520 --> 00:35:18,520
So I'm really keen to have the potential to work with her in the future. It was a really good talk.

388
00:35:18,520 --> 00:35:28,520
She really understands the issues and she's worked so much, so it's neat to be able to work with her.

389
00:35:28,520 --> 00:35:37,520
So anyway, this resolution is in regards to sustainable development.

390
00:35:37,520 --> 00:35:48,520
So trying to figure out what is required for the human race to sustain itself.

391
00:35:48,520 --> 00:35:53,520
And that's not that interesting from a Buddhist perspective because it's short-sighted.

392
00:35:53,520 --> 00:35:57,520
Eventually the human race is going to go up in a ball of flames.

393
00:35:57,520 --> 00:36:13,520
But the factors that lead to sustainable or contribute to sustainable development are interesting.

394
00:36:13,520 --> 00:36:19,520
They have these six principles, six P's, five P's, five P's.

395
00:36:19,520 --> 00:36:31,520
People, planet, peace, people, planet, prosperity, peace, and the planet, and partnership.

396
00:36:31,520 --> 00:36:36,520
So people putting people first and this is in regards to poverty.

397
00:36:36,520 --> 00:36:40,520
The planet, that's in regards to climate change.

398
00:36:40,520 --> 00:36:43,520
Prosperity is in regards to economics.

399
00:36:43,520 --> 00:36:49,520
Number four, it's peace. And peace is important, of course, we're in peace studies, so we talk about it especially.

400
00:36:49,520 --> 00:37:03,520
But there's a quote in the resolution, I think, that says it's just sustainable development is not possible without peace.

401
00:37:03,520 --> 00:37:09,520
Depends on peace. And peace depends on sustainable development.

402
00:37:09,520 --> 00:37:15,520
So they stress that quote, that they work together.

403
00:37:15,520 --> 00:37:25,520
And she said this was pretty awesome because apparently it's the first time they've used this sort of language where they really focused on the idea of peace.

404
00:37:25,520 --> 00:37:30,520
The language is new to the UN apparently.

405
00:37:30,520 --> 00:37:35,520
Something about how normally they don't concern themselves so much about peace.

406
00:37:35,520 --> 00:37:41,520
So it doesn't include that was apparently significant.

407
00:37:41,520 --> 00:37:46,520
And then fifth one is partnership.

408
00:37:46,520 --> 00:37:53,520
Partnership is interesting working with not just governments working together but all peoples on all levels.

409
00:37:53,520 --> 00:37:56,520
I mean, you could include religion in there.

410
00:37:56,520 --> 00:38:04,520
But NGOs and just individuals who have a stake in things, grassroots organizations that can be.

411
00:38:04,520 --> 00:38:20,520
So maybe that's not that. I mean, it's not all what I would want to focus on as a Buddhist, but in general talking about peace, I think it's quite useful.

412
00:38:20,520 --> 00:38:23,520
But as you can see, it's not a bit world's thing.

413
00:38:23,520 --> 00:38:26,520
But it's worldly in terms of the big things.

414
00:38:26,520 --> 00:38:29,520
What affects people? How people find happiness?

415
00:38:29,520 --> 00:38:36,520
And so talking about these things is interesting learning about these things somehow useful.

416
00:38:36,520 --> 00:38:45,520
As I said, obviously more focused on mental health and inner peace.

417
00:38:45,520 --> 00:38:49,520
Of course, that's what I'm studying.

418
00:38:49,520 --> 00:38:53,520
And then I got my Lotus Sutra essay, which is almost done.

419
00:38:53,520 --> 00:39:09,520
I'm just going to write the conclusion that's due tomorrow, but there's an extension, so we have to work on it till Friday.

420
00:39:09,520 --> 00:39:17,520
Anyway, so I've opened the hang-up out. I hang out up. Nobody joined, so I'm assuming there are no questions.

421
00:39:17,520 --> 00:39:21,520
I think we'll stop it there.

422
00:39:21,520 --> 00:39:33,520
I may, we'll have to see how the next days go, probably tomorrow, I'm just not going to broadcast in preparation for my exam.

423
00:39:33,520 --> 00:39:45,520
So I guess that's all for tonight. Have a good night.

424
00:39:45,520 --> 00:39:52,520
Thank you.

