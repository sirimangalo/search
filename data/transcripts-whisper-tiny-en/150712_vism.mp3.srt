1
00:00:00,000 --> 00:00:09,720
Okay, so welcome everyone, we're here to study with Cindy Manda.

2
00:00:09,720 --> 00:00:19,680
We are at chapter 4, Robin, you want to just start us, introduce us?

3
00:00:19,680 --> 00:00:32,080
Sure, we're on chapter 4, on page 154, and section 172, and Adam, I'm not sure, are you able to read with us today?

4
00:00:36,080 --> 00:00:41,980
Looks like you've got yourself muted, that's okay, if you are able to read today, that's fine.

5
00:00:41,980 --> 00:00:49,880
If you wanted to just unmute yourself and we'll know, otherwise Aurora, can you start with 172?

6
00:00:49,880 --> 00:00:55,280
Now as to mindful and fully aware, here he remembers, as he's mindful.

7
00:00:55,280 --> 00:00:58,480
He has full awareness, thus he's fully aware.

8
00:00:58,480 --> 00:01:02,980
This is mindfulness and full awareness stated as personal attributes.

9
00:01:02,980 --> 00:01:06,580
Here in, mindfulness has the characteristic of remembering.

10
00:01:06,580 --> 00:01:11,180
Its function is not to forget, it is manifested as guarding.

11
00:01:11,180 --> 00:01:14,780
Full awareness has the characteristic of non-confusion.

12
00:01:14,780 --> 00:01:19,180
Its function is to investigate, it is manifested as scrutiny.

13
00:01:24,680 --> 00:01:30,180
Thank you, Dan, are you able to read 173?

14
00:01:30,180 --> 00:01:32,580
No, I'm not 38, I'm still looking.

15
00:01:32,580 --> 00:01:33,980
Okay, we'll catch you next time around.

16
00:01:33,980 --> 00:01:40,180
What we do is we just kind of go down the list and people take turns reading, so we'll catch you next time around.

17
00:01:40,180 --> 00:01:44,680
And David, you just popped in, we're on page 154 on 173.

18
00:01:44,680 --> 00:01:47,180
Glenn, are you able to read that one?

19
00:01:51,180 --> 00:01:58,180
Here in, although this mindfulness and this full awareness exists in the earlier Johnus as well,

20
00:01:58,180 --> 00:02:05,680
for one who is forgetful and not fully aware, does not attain even access, let alone absorption.

21
00:02:05,680 --> 00:02:12,180
Yet because of the comparative grossness of those Johnus, the mind's going is easy there.

22
00:02:12,180 --> 00:02:20,380
Like that of a man on level ground, and so the functions of mindfulness and full awareness are not evident in him.

23
00:02:20,380 --> 00:02:27,880
But it's only stated here because the subtlety of this Johnus, which is due to the abandoning of the gross factors,

24
00:02:27,880 --> 00:02:37,880
requires that the mind's going always includes the function of mindfulness and full awareness, like that of a man on a razor's edge.

25
00:02:41,880 --> 00:02:50,880
I just wanted to give a little note here about actually 172.

26
00:02:50,880 --> 00:02:58,880
We can see that actually the word, here's a good example of how the word mindfulness is a poor translation of sati.

27
00:02:58,880 --> 00:03:08,880
Sati means one who is mindful, but it actually means it's sati plus ah, so it should be, so that comes from the word sati.

28
00:03:08,880 --> 00:03:19,880
But the word mindful is obviously a problem, it is actually clearly having to do with memory and not what we understand to be mindfulness.

29
00:03:19,880 --> 00:03:32,880
And except in the sense that the word mindfulness is our ability to remember ourselves, our mindfulness is in a sense, our ability to.

30
00:03:32,880 --> 00:03:44,880
So it's just a poor, just not exact, remembrance is clear because sati can have to do with remembering things in the past as well.

31
00:03:44,880 --> 00:03:53,880
Except here it means the ability to not not lose track of the John itself.

32
00:03:53,880 --> 00:03:58,880
Anyway, I guess it's not that bad of a word, it's just not exact.

33
00:03:58,880 --> 00:04:04,880
Well, thank you for clarifying that bounty.

34
00:04:04,880 --> 00:04:11,880
What is more just as a calf that follows a cow returns to the cow when taken away from her, if not prevented.

35
00:04:11,880 --> 00:04:21,880
So too, when the third John is led away from happiness, it would return to happiness if not prevented by mindfulness and full awareness and would rejoin happiness.

36
00:04:21,880 --> 00:04:27,880
And besides beings are greedy for bliss, and this kind of bliss is exceedingly sweet, since there is none greater.

37
00:04:27,880 --> 00:04:36,880
But here there is none greed for the bliss owing to the influence of the mindfulness and full awareness, not for any other reason.

38
00:04:36,880 --> 00:04:50,880
And so it should also be understood that it is stated only here in order to emphasize this meaning too.

39
00:04:50,880 --> 00:05:10,880
Sankar, can you read 175?

40
00:05:10,880 --> 00:05:14,880
Yes, sorry, I just couldn't find my press button because he didn't be handling that.

41
00:05:14,880 --> 00:05:25,880
Now as to the cross, he feels bliss with his body. Here, although in one actually possessed of the John and there is no concern about feeding this.

42
00:05:25,880 --> 00:05:31,880
Nevertheless, he would feel the bliss associated with his mental body.

43
00:05:31,880 --> 00:05:43,880
And after emerging from the John, he would also feel bliss since his material body would have been affected by the exceeding this superior method originated by that bliss associated

44
00:05:43,880 --> 00:05:46,880
with that mental body.

45
00:05:46,880 --> 00:05:59,880
It is in order to point to this meaning that the words he feels bliss with his body are said.

46
00:05:59,880 --> 00:06:15,880
Now as to the cross, that an account of which the number one is announced. It draws in bliss who has economically and is mindful.

47
00:06:15,880 --> 00:06:34,880
Here it is the John, an account of which is caught, which is caught, an account of which as reason. The number one that is to say the enlightened ones, etc.

48
00:06:34,880 --> 00:07:00,880
announced, teach, declare, establish, reveal, the expound, explain, clarify, the person who possess the third John, the praise is what is intended, why?

49
00:07:00,880 --> 00:07:08,880
Because he draws in bliss, which has economically and is mindful.

50
00:07:08,880 --> 00:07:21,880
He enters upon the drills in that third John, is how the construction should be understood here.

51
00:07:21,880 --> 00:07:38,880
But why do they praise him thus? Because he is worthy of praise.

52
00:07:38,880 --> 00:07:53,880
For this man is worthy of praise since he has equanimity towards the third John, though it possesses exceedingly sweet bliss and has reached the perfection of bliss, and he has not drawn towards it by a liking for the bliss.

53
00:07:53,880 --> 00:07:59,880
And he is mindful with the mindfulness established in order to prevent the arising of happiness.

54
00:07:59,880 --> 00:08:22,880
And he feels with his mental body the undefiled bliss beloved of noble ones, cultivated by noble ones. Because he is worthy of praise in this way, it should be understood, noble ones praise him with the words, he dwells in bliss, who has equanimity and is mindful, thus declaring the special qualities that are worthy of praise.

55
00:08:22,880 --> 00:08:33,880
Third, it is the third in the numerical series, and it is third because it is entered upon third.

56
00:08:33,880 --> 00:08:52,880
Then it was said, which abandoned one factor possesses two factors, here the abandoning of one factor should be understood as the abandoning of happiness. But that is abandoned only at the moment of absorption, as applied thought and sustained thought are at that of the second John.

57
00:08:52,880 --> 00:09:01,880
Hence it is called its factor of abandoning.

58
00:09:01,880 --> 00:09:05,880
Thank you or again, could you read 179?

59
00:09:05,880 --> 00:09:15,880
Okay, the possession of the two factors should be understood as the arising of the two, namely bliss and unification.

60
00:09:15,880 --> 00:09:27,880
So when it is said in the Bhagang Vibanga, Jana, equanimity, mindfulness, full awareness, bliss, unification of mind.

61
00:09:27,880 --> 00:09:32,880
This is said figuratively in order to show that Jana with its equipment.

62
00:09:32,880 --> 00:09:40,880
But accepting the equanimity in mindfulness and full awareness, this Jana has literally only two factors,

63
00:09:40,880 --> 00:09:45,880
qua factors that have attained to the characteristic of lighting.

64
00:09:45,880 --> 00:09:51,880
According, as it is said, what is the Jana of two factors on that occasion?

65
00:09:51,880 --> 00:09:59,880
It is bliss and unification of mind.

66
00:09:59,880 --> 00:10:08,880
The rest is as in the case of the first Jana.

67
00:10:08,880 --> 00:10:22,880
Once this has been obtained in this way, and once he has mastery in five ways already described, and on emerging from the now familiar third Jana, he can regard the flaws in it thus.

68
00:10:22,880 --> 00:10:33,880
This attainment is threatened by the nearness of happiness, whatever there is in it of mental concern about bliss proclaims its grossness.

69
00:10:33,880 --> 00:10:38,880
And its factors are weakened by the grossness of the bliss so expressed.

70
00:10:38,880 --> 00:10:57,880
He can bring forth the fourth Jana to mind as quieter and so end his attachment to the third Jana and set about doing what is needed for attainment of the fourth.

71
00:10:57,880 --> 00:11:13,880
When he has emerged from the third Jana, the bliss and other words, the mental joy appears gross to him as he reviews the Jana factors with mindfulness and full awareness, while the equanimity as feeling and the unification of mind appear peaceful.

72
00:11:13,880 --> 00:11:25,880
Then as he brings that same sign to mind as earth, earth again and again with the purpose of abandoning the gross factor and obtaining the peaceful factors, knowing.

73
00:11:25,880 --> 00:11:28,880
Now the fourth Jana will arise.

74
00:11:28,880 --> 00:11:36,880
Their arises in him, mind door averting with that same earth casino as its object, interrupting the life continuum.

75
00:11:36,880 --> 00:11:46,880
After that either four or five impulses impel on that same object, the last one of which is an impulsion of the final material sphere according to the fourth Jana.

76
00:11:46,880 --> 00:11:52,880
The rests are of the kinds already stated.

77
00:11:52,880 --> 00:12:02,880
But there is this difference, this pleasant feeling is not a condition, has repetition condition for neither painful nor pleasant feeling.

78
00:12:02,880 --> 00:12:09,880
And the preliminary work must be aroused in the case of fourth Jana with neither painful nor pleasant feeling.

79
00:12:09,880 --> 00:12:19,880
Consequently this consciousness of a preliminary work associated with neither painful nor pleasant feeling.

80
00:12:19,880 --> 00:12:28,880
And here happiness finishes simply all into the association with equanimity.

81
00:12:28,880 --> 00:12:44,880
The fourth Jana, and at this point with the abandoning of pleasure and pen, and with the previous disappearance of Joey and grief,

82
00:12:44,880 --> 00:12:59,880
he enters upon and drills in the fourth Jana, which has either and nor pleasure and has purity of mindfulness due to equanimity.

83
00:12:59,880 --> 00:13:28,880
And so he has attend in the fourth Jana, which abundance one factor possess two factors is good in three ways possess ten characteristics and is of the earth casino.

84
00:13:28,880 --> 00:13:36,880
Here in with the abandoning of pleasure and pain means with the abandoning of bodily pleasure and bodily pain.

85
00:13:36,880 --> 00:13:43,880
With the previous means which took place before, not in the moment of the fourth Jana.

86
00:13:43,880 --> 00:14:01,880
Disappearance of joy and grief means with the previous disappearance of the two, that is mental bliss and mental pain with the abandoning is what is meant.

87
00:14:01,880 --> 00:14:23,880
But when does the abandoning of these take place at the moment of access of the four genres, poor mental joy is only abandoned at the moment of the fourth Jana access while bodily pain until grief and bodily bliss pleasure are abandoned respectively at the moments of access of the first second and third Jana's.

88
00:14:23,880 --> 00:14:48,880
Although the order in which their abandoned is not actually mentioned, nevertheless, the abandoning of the pleasure, pain, joy and grief is stated here according to the order in which the faculties are summarized in the India with Banga.

89
00:14:48,880 --> 00:14:56,880
Okay, thank you, sorry, and I apologize for talking over someone else in my last reading.

90
00:14:56,880 --> 00:15:07,880
But if these are only abandoned at the moments of access of the several Jana's, why is there a cessation said to take place in the Jana itself in the following passage?

91
00:15:07,880 --> 00:15:11,880
And where does the risen pain faculty sees without remainder?

92
00:15:11,880 --> 00:15:22,880
Here Biku's quite secluded from sense desires secluded from unprofitable things, a Biku enters upon and dwells in the first Jana, which is born as occlusion.

93
00:15:22,880 --> 00:15:26,880
It is here that the risen pain faculty sees is without remainder.

94
00:15:26,880 --> 00:15:31,880
Where does the risen grief faculty sees without remainder in the second Jana?

95
00:15:31,880 --> 00:15:35,880
Where does the risen pleasure faculty sees without remainder in the third Jana?

96
00:15:35,880 --> 00:15:39,880
Where does the risen joy faculty sees without remainder?

97
00:15:39,880 --> 00:15:53,880
Here Biku's with the abandoning of pleasure and pain, with the previous disappearance of joy and grief, a Biku enters upon and dwells in the fourth Jana, which has mindfulness purified by equanimity.

98
00:15:53,880 --> 00:15:57,880
It is here that the risen joy faculty sees is without remainder.

99
00:15:57,880 --> 00:16:10,880
It is said in that, in that way, they're referring to reinforced cessation for in the first Jana, etc. It is their reinforced cessation, not just their cessation that takes place.

100
00:16:10,880 --> 00:16:21,880
At the moment of access, it is just their cessation, not their reinforced cessation that takes place.

101
00:16:21,880 --> 00:16:36,880
For accordingly, during the first Jana access, which has multiple adverting, there could be rearising of the bodily pain faculty due to contact with gadflies, flies, and so on.

102
00:16:36,880 --> 00:16:55,880
Where the discomfort of an uneven seat, though that pain faculty had already ceased, but not so during absorption, where else, though it has ceased during access, it has not absolutely ceased there since it is not quite beaten out by opposition.

103
00:16:55,880 --> 00:17:17,880
But during absorption, the whole body is showered with bliss, owing to provision by happiness, and the pain faculty has absolutely ceased in one whose body is showered with bliss, since it is beaten out, then by opposition.

104
00:17:17,880 --> 00:17:36,880
And during the second Jana access to, which has multiple adverting, there could be rearising of the mental grief faculty, although it had already ceased there, because it arises when there is bodily weariness and mental vexation, which have applied thought and sustained thought as their condition.

105
00:17:36,880 --> 00:17:48,880
But it does not arise when applied and sustained thought are absent, when it arises it does so in the presence of applied and sustained thought, and they are not abandoned in the second Jana access.

106
00:17:48,880 --> 00:17:56,880
But this is not so in the second Jana itself, because its conditions are abandoned there.

107
00:17:56,880 --> 00:18:13,880
Likewise, in the third Jana access there could be rearising of the abandoned bodily pressure faculty in one whose body was provided, provided by the superior materiality originated by the consciousness associated with the happiness.

108
00:18:13,880 --> 00:18:42,880
But not so in the third Jana itself, for in the third Jana, the happiness that is conditioned for the body, this pressure has ceased entirely. Likewise, in the fourth Jana access there could be rearising of the abandoned mental joy faculty, because of its nearness and because it has not been properly surmounted, owing to the absence of the economy, brought to absorption strength.

109
00:18:42,880 --> 00:18:52,880
But not so in the fourth Jana itself, and that is why in each case the words without remained are included thus.

110
00:18:52,880 --> 00:19:02,880
It is here that the recent pain faculty ceases without remained there.

111
00:19:02,880 --> 00:19:31,880
Here it may be asked when if this kind of feeling are abandoned in the asses in this way, why are they brought in here? It is down so that they can be readily grasped for the either painful nor pleasant feeling described here.

112
00:19:31,880 --> 00:19:45,880
By the words which has neither pain nor pleasure is subtle, hard to recognize and not readily grasp.

113
00:19:45,880 --> 00:20:13,880
So just as when a cattle hurt wants to catch a rip, fractory ox that cannot be caught at all by approaching it, it collects all the cattle into open pain and doesn't out one by one.

114
00:20:13,880 --> 00:20:22,880
And then he says that is it, catch it and so it gets caught as well.

115
00:20:22,880 --> 00:20:42,880
So too the last one has collected all these five kinds of feeling together so that they can be grasped readily for when they are shown, collected together in this way.

116
00:20:42,880 --> 00:21:05,880
Then what is not bodily pleasure, bliss or bodily pain or mental joy or mental grief can still be grasped in this way, this is neither painful nor pleasant feeling.

117
00:21:05,880 --> 00:21:19,880
And we often get these sort of passages that we've seen several already. This is dealing with controversy and a lot of this has been dealing with controversies who can see as we've gone through.

118
00:21:19,880 --> 00:21:36,880
I hear the problem as well, it's a question as to why the Buddha is talking about pain when pain has already disappeared or why is he talking about feelings that, why is he saying they're all removed and so it's an explanation of why that is.

119
00:21:36,880 --> 00:21:47,880
I'm not sure how clear this sort of reason for this sort of paragraph is to everyone.

120
00:21:47,880 --> 00:21:54,880
But a lot of this is the commentary trying to clear up questions that they had.

121
00:21:54,880 --> 00:22:02,880
Some of them are fairly esoteric, the question, some of them I don't even quite understand, but this one is just saying it's interesting actually.

122
00:22:02,880 --> 00:22:11,880
Many of them are, they give sort of insight into teaching methods.

123
00:22:11,880 --> 00:22:14,880
Why did the Buddha express it in this way?

124
00:22:14,880 --> 00:22:25,880
And giving some insight into possible reasons why the Buddha is choice of words, something nit because you can say they break it up into individual words.

125
00:22:25,880 --> 00:22:36,880
Why did he say this? I think it's a good explanation. It makes sense and it's not something I would have thought of.

126
00:22:36,880 --> 00:22:47,880
Why is he mentioning all the feelings while it's to show that there is a figure of speech to show that all the feelings are gone.

127
00:22:47,880 --> 00:22:57,880
Not just some of them, but all the feelings are gone except for another pleasant or painful feeling. That's why he mentions pain as well.

128
00:22:57,880 --> 00:23:17,880
One day, can I just ask back in 189 where it said towards the end, in the 4th John, it is here that there was some pain faculty ceases without remainder. Does that literally mean in the 4th John, a person wouldn't feel any pain no matter how intense, like if they were suddenly sitting in a fire or something?

129
00:23:17,880 --> 00:23:33,880
Yes, I actually think he's saying, if you look at, I think it was a footnote, maybe footnote 49, but he is, I could not sure exactly where it is, but he is saying, I think that even in the 1st John, there is no pain.

130
00:23:33,880 --> 00:23:55,880
Because if you were in a fire, there's an example of monks sitting in fire or getting attacked. There's an example of a video manga of a monk. Once we get into the magical powers, you'll see that monks who were in forest fires or so and were not hurt, if they entered into the genus.

131
00:23:55,880 --> 00:24:03,880
So even physically, their bodies would not be burned. That's right, that's amazing.

132
00:24:03,880 --> 00:24:23,880
Many things like that, like there was this novice who entered into the Jat first John, or one of the Janas, just as he was, about to be killed by some thieves and some bandits, and the blade bent back on itself, wouldn't enter into the novices body.

133
00:24:23,880 --> 00:24:34,880
They couldn't kill him, they tried several times to kill him with his knife and the blade kept folding on them. So that's the belief, no, this is what they say happened.

134
00:24:34,880 --> 00:24:40,880
So the truth, I don't know, but that's the truth.

135
00:24:40,880 --> 00:24:50,880
As I have said, I have heard that it is the Niro, the Samapati that has that power.

136
00:24:50,880 --> 00:25:14,880
That's possibly true. Just a second.

137
00:25:14,880 --> 00:25:22,880
There's someone where it says that there's no pain in any of the Janas. So yeah, maybe the body could be hurt, but there would be no experience of it.

138
00:25:22,880 --> 00:25:37,880
I think during the Janas, it might pull you out of the Jana though.

139
00:25:37,880 --> 00:25:44,880
The status when they can stay for about a week without any food or water.

140
00:25:44,880 --> 00:25:51,880
I think you can enter the Janas for a week, I'm not sure, isn't it?

141
00:25:51,880 --> 00:26:09,880
No idea, man.

142
00:26:09,880 --> 00:26:25,880
In 187, it says that no pain can arise during access concentration of the first Jana. It's not so during absorption, meaning during absorption, there will be no pain, pretty sure that's clear.

143
00:26:25,880 --> 00:26:42,880
In absorption concentration is one type of consciousness, that's why it's absorption, the consciousness doesn't change. Every moment is the same type of concentration, the first Jana has pleasure.

144
00:26:42,880 --> 00:26:57,880
There's no pain, not possible. But access is still possible, but they pain anyway.

145
00:26:57,880 --> 00:27:12,880
So it was some of the meditation, I mean, I can sit an hour in the full-loaded position doing my best in them, but in the kids in days, and if I were to do all day, you've loaded this position, I wouldn't be able to do it the next day.

146
00:27:12,880 --> 00:27:20,880
Whereas in some of the meditation, you can sit back straight full-loaded position and never suffer the suffer bad consequences from it.

147
00:27:20,880 --> 00:27:35,880
It's just the intensity of concentration. I don't think it's quite exactly that either. It's the fact that the mind is so calm, and the body is therefore so calm, that there's no stress.

148
00:27:35,880 --> 00:27:53,880
There's no movement, so there's no stress put on the joints. So it's not just that you don't feel the pain, it's that you're not stressing the body as you would in reverse in the meditation. There's no tension, there's no distraction in the mind of the beginning.

149
00:27:53,880 --> 00:28:07,880
That makes sense.

150
00:28:07,880 --> 00:28:30,880
Besides, this may be understood as said in order to show the condition for the neither painful nor pleasant mind deliverance. For the abandoning of bodily pain, etc., our conditions for that, according as it is said, there are four conditions for the attainment of neither pleasant, painful nor pleasant mind deliverance.

151
00:28:30,880 --> 00:28:40,880
We are friends with the abandoning of pleasure and pain, and with the previous disappearance of joy and grief, a vehicle enters upon and dwells in the fourth chana, equanimity.

152
00:28:40,880 --> 00:28:50,880
These are the four conditions for the attainment of the neither painful nor pleasant mind deliverance.

153
00:28:50,880 --> 00:29:05,880
Alternatively, just as although mistaken view of individuality, etc. have already been abandoned in the earlier paths, there are nevertheless mentioned as abandoned in the description of the third path for the purpose of recommending it.

154
00:29:05,880 --> 00:29:21,880
So, to these kinds of feeling can be understood as mentioned here for the purpose of recommending this jhana. Or alternatively, they can be understood as mentioned for the purpose of showing that greed and hate very far away owing to the removal of their conditions.

155
00:29:21,880 --> 00:29:41,880
Four of these pleasureless is a condition for joy and joy for greed. Pain is a condition for grief and grief for hate. So, with the removal of pleasure, bliss, etc., greed and hate are very far away since they are removed along with their conditions.

156
00:29:41,880 --> 00:29:59,880
Which has neither pain nor pleasure, no pain owing to absence of pain, no pleasure owing to absence of pleasure, bliss. By this he indicates the third kind of feeling that is in opposition both to pain and pleasure, not the mere absence of pain and pleasure.

157
00:29:59,880 --> 00:30:25,880
This third kind of feeling, neither pain nor pleasure, is also called equanimity. It has a characteristic of experiencing what is contrary to both desirable and undesirable. Its function is neutral. Its manifestation is unevident. Its proximate cause should be understood as a cessation of pleasure, bliss.

158
00:30:25,880 --> 00:30:41,880
And has purity of mindfulness due to equanimity. Has purity of mindfulness brought about by equanimity. For the mindfulness in this jhana is quite purified and as purification is affected by equanimity, not by anything else.

159
00:30:41,880 --> 00:30:58,880
That is why it is said to have purity of mindfulness due to equanimity. Also it is said in the vibanga, this mindfulness is cleared, purified, clarified by equanimity. Hence it is said to have purity of mindfulness due to equanimity.

160
00:30:58,880 --> 00:31:08,880
And the equanimity due to which there comes to be this purity of mindfulness should be understood as specific neutrality and meaning.

161
00:31:08,880 --> 00:31:21,880
And not only mindfulness is purified by it here, but also all associated states. However the teaching is given under the heading of mindfulness.

162
00:31:21,880 --> 00:31:41,880
Here in this equanimity exists in the lower three jhanas too, but just as although a crescent moon exists by day, but is not purified or clear since it is out shown by the sun's radiance in the daytime, or since it is deprived of the night, which is its ally owing to gentleness and owing to helpfulness to it.

163
00:31:41,880 --> 00:31:58,880
So too this crescent moon of equanimity consisting in specific neutrality exists in the first jhana, etc. But it is not purified since it is out shown by the glare of the opposing states consisting in applied thought, etc.

164
00:31:58,880 --> 00:32:13,880
So it is deprived of the night of equanimity as feeling for its ally, and because it is not purified, the conescent mindfulness and other states are not purified either, like the unpurified crescent moon's radiance by day.

165
00:32:13,880 --> 00:32:30,880
I know among these first three jhanas is said to have purity of mindfulness due to equanimity, because here this crescent moon consisting in specific neutrality is utterly pure because it is not out shown by the glare of the opposing states consisting in applied thought, etc.

166
00:32:30,880 --> 00:32:54,880
And because it has the night of equanimity as feeling for its ally, and since it is purified, the conescent mindfulness and other states are purified and clear also, like the purified crescent moon's radiance, that it should be understood is why only this jhana is said to have purity of mindfulness due to equanimity.

167
00:32:54,880 --> 00:33:04,880
Fourth, it is fourth in numerical series, and it is fourth because it is entered upon fourth.

168
00:33:04,880 --> 00:33:28,880
Then it was said which abundance one factor possessed two factors here, the bending of the one factor should be understood as the bending of joy, but that joy is actually abandoned in the first

169
00:33:28,880 --> 00:33:49,880
impressions of the same cognitive series, as it is called its factor of abandoning, the process of the two factors should be understood as the arising of the two

170
00:33:49,880 --> 00:34:17,880
namely equanimity as feeling and in implication of mind, the rest is as stated in the case of the first jhana, this in the first place is according to the powerful reckoning of jhana.

171
00:34:17,880 --> 00:34:27,880
Right now, however, he is developing fivefold jhana, then, on emerging from the now familiar first jhana, he can regard the flaws in it in this way.

172
00:34:27,880 --> 00:34:34,880
This attainment is threatened by the nearness of the hindrances, and as factors are weakened by the grossness of applied thought.

173
00:34:34,880 --> 00:34:44,880
He could bring the second jhana to mind, that's quieter, and so end is attachment to the first jhana, and set about doing what is needed for attaining the second.

174
00:34:44,880 --> 00:34:53,880
So the point is again, just to recap, the fivefold jhana separates the first two factors and delivers them one by one.

175
00:34:53,880 --> 00:35:17,880
So what you're saying here is that you can you can practice either way so you can go directly to the jhana without that is lost two factors or here you can remove the factors one by one.

176
00:35:17,880 --> 00:35:30,880
Now, he emerges from the first jhana, mindfully and fully aware, and only applied thought appears gross to him as he reviews the jhana factors, while the sustained thought etc appear peaceful.

177
00:35:30,880 --> 00:35:45,880
Then, as he brings that same sign to mind, as earth, again and again with the purpose of abandoning the gross factor and obtaining the peaceful factors, the second jhana arises in him in the way already described.

178
00:35:45,880 --> 00:35:50,880
It's factor of abandoning is applied thought only.

179
00:35:50,880 --> 00:35:54,880
We're beginning with sustained what are the factors that it possesses.

180
00:35:54,880 --> 00:36:02,880
The rest is already stable.

181
00:36:02,880 --> 00:36:11,880
So that which is the second in the fourfold reckoning becomes the second and third in the fivefold reckoning by being divided into two.

182
00:36:11,880 --> 00:36:19,880
And those which are the third and fourth in the former reckoning become the fourth and fifth in this reckoning.

183
00:36:19,880 --> 00:36:24,880
The first remains the first in each case.

184
00:36:24,880 --> 00:36:51,880
The fourth chapter called the description of the earth casino in the treaties on the development of concentration in the path of purification composed for the purpose of gladdening good people.

185
00:36:51,880 --> 00:36:57,880
I think we might have missed a couple. I think we might have missed a 200.

186
00:36:57,880 --> 00:37:01,880
Could you go back to 200?

187
00:37:01,880 --> 00:37:03,880
Okay.

188
00:37:03,880 --> 00:37:15,880
When this has been obtained in this way, and once he is mastery in the five ways already described, then on emerging from the now familiar second jhana, he can regard the flaws in it this way.

189
00:37:15,880 --> 00:37:22,880
This attainment is threatened by the nearness of applied thought and its factors are weakened by the grossness of sustained thought.

190
00:37:22,880 --> 00:37:33,880
He can bring the third jhana to mind is quieter and so end his attachment to the second jhana instead of doing what is needed for attaining the third.

191
00:37:33,880 --> 00:37:46,880
Now he emerges from the second jhana mindfully and fully aware, only sustained thought appears gross to him as he reviews the jhana factors, while happiness, etc., appear peaceful.

192
00:37:46,880 --> 00:38:03,880
Then as he brings that same sign to mind as earth, earth again and again with the purpose of abandoning the gross factors and obtaining the peaceful factors, the third jhana rises in him in the way already described.

193
00:38:03,880 --> 00:38:20,880
Its factors of abandoning is sustained thought only, the three beginning with happiness as in the second jhana in the fourfold reckoning are the factors that it possesses, the rest is as already stated.

194
00:38:20,880 --> 00:38:36,880
So that which is the second in the fourfold reckoning becomes the second and third in the fivefold reckoning by being divided into two, and those which are the third and fourth in the former reckoning become the fourth and fifth in this reckoning, the first remains the first in each case.

195
00:38:36,880 --> 00:38:52,880
The fourth chapter called the description of the earth casino in the treatise of the development of concentration in the path of purification composed for the purpose of gladdening good people.

196
00:38:52,880 --> 00:39:11,880
And that one was a tough one as well, that says I said the example chapter so we're not going to have to go through all the jhana's for the other 39 meditation types, shouldn't be a lot more streamlined and more directly related to talking about each of the meditation practices.

197
00:39:11,880 --> 00:39:29,880
Still detailed, but it'll be more interesting, I suppose, I mean this is interesting for people wanting to really understand the four jhana's that's about the best you're going to get as far as a detailed explanation of how to how to achieve the four jhanas.

198
00:39:29,880 --> 00:39:46,880
But as to how to actually practice the meditation, it's only obviously a small part of this chapter, so we'll see that the rest of the chapter should be a little the least you're going I think.

199
00:39:46,880 --> 00:40:00,880
Anyway, I guess we'll stop short there and come back in 10 minutes for Polly. Sounds good, thank you.

