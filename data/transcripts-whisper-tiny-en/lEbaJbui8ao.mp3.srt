1
00:00:00,000 --> 00:00:08,360
So, could you talk a little bit about ordination, not necessarily the process, but more

2
00:00:08,360 --> 00:00:12,480
about the right and wrong reasons for going forth?

3
00:00:12,480 --> 00:00:23,680
Well, the right reason to go forth is for the end of suffering, for the complete ending

4
00:00:23,680 --> 00:00:36,160
and freedom from suffering. The wrong reason is any other reason, but we're not really

5
00:00:36,160 --> 00:00:47,040
that picky and we're not expecting people to really understand. It's like the meditators

6
00:00:47,040 --> 00:00:56,280
here now, we have this one meditator, a younger meditator from Canada, who we're talking

7
00:00:56,280 --> 00:00:59,800
about this and I explained to him. I said, you know, he said, oh, I've had it all wrong

8
00:00:59,800 --> 00:01:04,240
and I really didn't understand what meditation was and I said, really, that's how I was

9
00:01:04,240 --> 00:01:09,880
as well and some, most of us are. We come to meditation looking for something, but we're

10
00:01:09,880 --> 00:01:14,360
not looking for, we're not even looking for the right thing.

11
00:01:14,360 --> 00:01:24,080
And so, the meditation actually doesn't generally give us what we're looking for, but it forces

12
00:01:24,080 --> 00:01:33,320
us to restate the question that we're asking. We may have thought that we're looking

13
00:01:33,320 --> 00:01:40,080
for some kind of wisdom or insight, but what we find really blows us away because we

14
00:01:40,080 --> 00:01:47,600
realized that our idea of wisdom and insight was totally off base and was totally shallow

15
00:01:47,600 --> 00:02:00,040
and superfluous or inconsequential. It's only up to your start meditating that you really

16
00:02:00,040 --> 00:02:06,440
realize why you should meditate. That's been my experience and the experience of people

17
00:02:06,440 --> 00:02:14,520
follow after me. So I'd say the same goes with, goes for a monasticism. You don't have

18
00:02:14,520 --> 00:02:24,080
to be ordaining to realize Nimbana and the person who ordains just because they want to

19
00:02:24,080 --> 00:02:30,840
get away from society or so on, could still be a good candidate for ordination, you

20
00:02:30,840 --> 00:02:37,280
know, provided that they are meditating. I would say the worst candidate for ordination

21
00:02:37,280 --> 00:02:48,920
but not the worst, but a problem candidate for ordination that occurs quite often is the

22
00:02:48,920 --> 00:02:58,000
person who is ordaining because they want to become a monk. And this is a, so it's a person

23
00:02:58,000 --> 00:03:04,400
whose goal is to become a monk and then after they become a monk, you have to convince

24
00:03:04,400 --> 00:03:10,960
them to meditate. This is much more difficult than a person who comes to meditate and then

25
00:03:10,960 --> 00:03:15,920
you convince them to become a monk or they decide for themselves to become a monk. It's

26
00:03:15,920 --> 00:03:22,640
much better to go the other way around and I'm much prefer a person who's not sure that

27
00:03:22,640 --> 00:03:28,640
they want to ordain but is well said in the meditation to a person who really, really wants

28
00:03:28,640 --> 00:03:35,120
to become a monk but is kind of so, so on the meditation side. People who are set on

29
00:03:35,120 --> 00:03:43,120
monasticism can often overshoot the goal and really miss the point of ordaining and

30
00:03:43,120 --> 00:03:48,880
there's a lot of monks out there believe it or not who don't meditate and so that's a problem.

31
00:03:48,880 --> 00:03:58,240
I would say on a fairly superficial level, a simple, simply put the best reason to ordain

32
00:03:58,240 --> 00:04:04,600
is because you want more time to meditate and you could also include in there wanting

33
00:04:04,600 --> 00:04:09,720
to help spread the teaching or because of your respect for the teaching and wanting to

34
00:04:09,720 --> 00:04:23,680
live your life in support and in a dumbic or darmic sort of way. But the core should

35
00:04:23,680 --> 00:04:28,400
be the desire to have more time and to be more focused on the meditation, to have the

36
00:04:28,400 --> 00:04:38,440
protection from your own defilements, to have the support in your spiritual growth. It should

37
00:04:38,440 --> 00:04:43,600
be as a means not an end. Why are you ordaining? It's not because you want to become a monk.

38
00:04:43,600 --> 00:04:50,760
You want to become a monk because it's the best way to accomplish your goals which are

39
00:04:50,760 --> 00:05:16,040
the freedom from suffering and the end of our suffering.

