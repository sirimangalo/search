1
00:00:00,000 --> 00:00:07,000
I was wondering if you could talk a bit more about how meditation benefits the body.

2
00:00:07,000 --> 00:00:14,000
I know you mentioned it in a video, the one about exercise, but I thought this was interesting.

3
00:00:14,000 --> 00:00:27,000
Well, the funny thing about meditation is that people get all sorts of ridiculous ideas about what it's going to do for them.

4
00:00:27,000 --> 00:00:32,000
I remember when I was meditating, and I've heard other people, so I'm not.

5
00:00:32,000 --> 00:00:39,000
Please don't laugh at me too hard, because I've heard other people with the same ideas that it was going to fix my teeth and fix my eyes,

6
00:00:39,000 --> 00:00:44,000
and stop me from going bald and so on.

7
00:00:44,000 --> 00:00:51,000
I've heard other people share this sort of idea that you think that someone was saying,

8
00:00:51,000 --> 00:00:58,000
how could this guy be practicing meditation? He still has to wear glasses, kind of thing. Someone who actually asked that.

9
00:00:58,000 --> 00:01:09,000
So the first thing to say is that I wouldn't expect miracles in regards to meditations effect on the body.

10
00:01:09,000 --> 00:01:16,000
There certainly is some indication that meditation does help the body, but that's, I think that's quite,

11
00:01:16,000 --> 00:01:22,000
there's nothing mystical or magical about that, because meditation makes the mind calm.

12
00:01:22,000 --> 00:01:37,000
And we pass on the meditation, causes the mind to let go, and you can feel this in the body.

13
00:01:37,000 --> 00:01:42,000
You begin to feel the tension, and you see how stressed you are.

14
00:01:42,000 --> 00:01:56,000
And so you relearn your interaction with reality to become such that you don't cause tension in the body,

15
00:01:56,000 --> 00:02:08,000
and as a result of that lack of tension so much of our physical discomfort is relieved or is done away with.

16
00:02:08,000 --> 00:02:15,000
The Buddha said that there are four types of sickness.

17
00:02:15,000 --> 00:02:23,000
I don't know if it's important to go into detail with this, but if you look at the four types of sickness that the Buddha mentioned,

18
00:02:23,000 --> 00:02:34,000
then it's clear that meditation plays a part, but certainly can't help you to be in perfect physical health,

19
00:02:34,000 --> 00:02:41,000
because there's sicknesses that come from food, there's sicknesses that come from environment,

20
00:02:41,000 --> 00:02:50,000
there's sicknesses that come from karma, one's past deeds, and then there's sicknesses that come from the mind.

21
00:02:50,000 --> 00:02:56,000
Meditation, I would say, can only help with the fourth one directly.

22
00:02:56,000 --> 00:03:08,000
The sicknesses that are caused by the mind's tension and the mind's stress and the mind's grip that it has on the body,

23
00:03:08,000 --> 00:03:14,000
or effect that it has through the brain on the rest of the body.

24
00:03:14,000 --> 00:03:21,000
This is the extent of the benefit that one can see in meditation.

25
00:03:21,000 --> 00:03:32,000
As for the other three, certainly meditation isn't going to allow you to abuse your body with food or with the things that you intake.

26
00:03:32,000 --> 00:03:39,000
This isn't one of those religious teachings where you can take poison and so on.

27
00:03:39,000 --> 00:03:43,000
But even there, you could make an argument.

28
00:03:43,000 --> 00:03:52,000
The one's digestion becomes better through meditation, especially walking meditation.

29
00:03:52,000 --> 00:03:56,000
The walking meditation allows the one's digestion to improve.

30
00:03:56,000 --> 00:03:59,000
This is one of the five benefits.

31
00:03:59,000 --> 00:04:06,000
So it can actually ameliorate diseases that are brought on by food, but if you have the sickness,

32
00:04:06,000 --> 00:04:12,000
if you have the disease, if you get diabetes, for example, it's not going to cure diabetes.

33
00:04:12,000 --> 00:04:19,000
It's not going to suddenly make your organs, your kidneys start working again.

34
00:04:19,000 --> 00:04:29,000
But it may prevent you from getting diabetes or heart disease or so on caused by eating the wrong food.

35
00:04:29,000 --> 00:04:36,000
Because of it, it'll prevent you or it'll slow down the process of acquiring the disease.

36
00:04:36,000 --> 00:04:41,000
Because of the ability that it gives you to digest your food better.

37
00:04:41,000 --> 00:04:51,000
As far as sicknesses that come from the environment, it can help there as well in a roundabout way.

38
00:04:51,000 --> 00:04:53,000
Because it gives you a better immune system.

39
00:04:53,000 --> 00:05:08,000
So your ability to fight off diseases is increased because of the energy and the strength that you give back to the body by practicing meditation by letting go and so on.

40
00:05:08,000 --> 00:05:13,000
People who practice meditation will find that...

41
00:05:13,000 --> 00:05:21,000
Well, we'll find in the beginning that actually it can have a deleterious effect on their health because they're dealing with so much stress.

42
00:05:21,000 --> 00:05:24,000
And meditation can actually bring up physical stress.

43
00:05:24,000 --> 00:05:29,000
It can bring up tension, you practice meditation, you feel more tense for some time.

44
00:05:29,000 --> 00:05:34,000
Because your mind is going over these and you're not finding a way to relieve the tension.

45
00:05:34,000 --> 00:05:39,000
You're just going over the bad states and so again and again, getting angry again and again and again.

46
00:05:39,000 --> 00:05:47,000
For the purpose of letting yourself get angry, for the purpose of learning about anger and seeing that oh actually it's causing me a lot of suffering.

47
00:05:47,000 --> 00:05:56,000
To the point that you can let go instead of when you get angry, go off and do something that you like, something that you enjoy in order to be happy again.

48
00:05:56,000 --> 00:06:05,000
So you might find that actually your physical health is weakened through the practice of meditation in the beginning.

49
00:06:05,000 --> 00:06:14,000
But through extended practice, you'll find that even with only eating a little bit of food or even food that is not the healthiest.

50
00:06:14,000 --> 00:06:20,000
These people who think to keep the body healthy, you have to eat this and this and this and have a strict regimen.

51
00:06:20,000 --> 00:06:36,000
It totally throw out the window in the practice of meditation because you find that even with a little bit of food or as I said food that is not of the best quality, you can find that you're radiant and that you are, you feel healthy, you look healthy and so on.

52
00:06:36,000 --> 00:06:50,000
So these are the first two types of disease that can't really be directly affected. If you're sick with diabetes and so on or if you've got a cold, I wouldn't look to meditation to be a miracle cure for the cold though.

53
00:06:50,000 --> 00:06:54,000
It certainly can help as it strengthens the immune system and so on.

54
00:06:54,000 --> 00:07:07,000
And number three, disease is based on karma. This is like when you're born cripple or when you're born with bad vision or crooked teeth or so on, certainly not going to help those.

55
00:07:07,000 --> 00:07:22,000
The best meditation, the best thing meditation can do for any sickness, for any physical impairment is to allow you to let go of it and to use the impairment actually as a teacher to help you see that this state of being a human is not perfect and there's no way you're going to make it perfect.

56
00:07:22,000 --> 00:07:30,000
And it's not the way out of suffering to try to make it good and lasting and perfect and wonderful.

57
00:07:30,000 --> 00:07:35,000
So I hope that's a fairly good answer from my end.

58
00:07:35,000 --> 00:07:57,000
Yeah, I don't have much to add just that you anyway will get old and the body will decay. So even if you meditate, so don't expect too much from from it.

59
00:07:57,000 --> 00:08:07,000
Yeah, the Buddha said one of the great negligence or or intoxications that we have is the intoxication with health.

60
00:08:07,000 --> 00:08:34,000
But I, she's six, so she's dying to graphics. We are all dying once sooner or later. But I had before I was meditating a lot, as I do know, I had kind of, I think it's called an English restless legs,

61
00:08:34,000 --> 00:08:50,000
so that my legs sometimes in the evenings, especially just moved by itself and it's, I don't know where it comes from, but from meditation it's gone.

62
00:08:50,000 --> 00:09:13,000
Yeah, I'd say I think all four of them can be ameliorated and could even possibly be cured. There's people who say they cured cancer through practice of meditation, whether it's true or not, I've seen people die trying to cure cancer through meditation that never witnessed someone actually curing cancer, but there are stories about it.

63
00:09:13,000 --> 00:09:30,000
You know, tumors anyway. So yeah, it, it makes you want to go off in the forest eat one meal a day and live in, where, where rags.

64
00:09:30,000 --> 00:09:51,000
And go barefoot and eat food that's not the healthiest and, you know, eat just whatever you're given, you only one meal a day and live on the floor and sleep on the floor and give up, you know, personal hygiene and so on.

65
00:09:51,000 --> 00:10:04,000
So, so in a sense, meditations, probably not the best thing to do if you're concerned with things like diseases and sicknesses and so on. I mean, there's lots of stories amongst who get malaria and so on.

66
00:10:04,000 --> 00:10:25,000
But I mean, that's one side of it. It's kind of, I'm trying to make a joke that no meditation may not be, may be hazardous to your health, because, because it will, it will eventually teach you to let go of your health and to let go of your, your desire to, you know, on our hand, if someone is totally enlightened, they have no concern for their physical health.

67
00:10:25,000 --> 00:10:50,000
And if they're dying, they're dying, if they're living, they don't have this concern. I mean, there is, if, if it's getting in the way with their, with the things that, that they are planning to do or that are natural for them to do, then they might do something naturally do something to overcome it.

68
00:10:50,000 --> 00:11:12,000
Like, there's even a story of the Buddha who had, had this monk recite to him, the Bodhjangas, whether he really did or not. It's a story that we have, recite the factors of enlightenment and as a result, he became free from his sickness through the power of the, of the fact, just even the Buddha hearing the factors of enlightenment.

69
00:11:12,000 --> 00:11:40,000
So, so there's potentially even, even in our hand might, might do that, but certainly is not worried about their health. The Buddha himself before he died, he was in great, a great amount of suffering and was able to overcome it in order to, in order to give a last teaching, but eventually passed away that, that same day.

70
00:11:40,000 --> 00:11:52,000
So, yeah, I mean, I, I wouldn't, and this goes back to what I said in the beginning, please don't expect meditation to, to do crazy things for your health and, and so on.

71
00:11:52,000 --> 00:12:06,000
Whether it does or not, the most important thing is that meditation should teach you to let go and to be free from this human life so that you don't have to come back and suffer and go through it all again.

72
00:12:06,000 --> 00:12:16,000
At the worst, you'll be able to come back as an angel or in a, in a realm that is more pure based in the purity of mind.

