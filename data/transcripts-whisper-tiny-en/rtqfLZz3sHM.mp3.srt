1
00:00:00,000 --> 00:00:06,500
Okay, question, isn't it pretty dangerous to sit in meditation for too long?

2
00:00:06,500 --> 00:00:07,500
Formal meditation.

3
00:00:07,500 --> 00:00:10,300
I have a friend who is a monk and an advanced meditator.

4
00:00:10,300 --> 00:00:17,000
He knows monks who have had strokes and blood clots by doing this, by sitting for too long,

5
00:00:17,000 --> 00:00:19,500
doing this all day and all night meditation.

6
00:00:19,500 --> 00:00:23,500
He did not recommend this.

7
00:00:23,500 --> 00:00:26,500
Blood clots, anyone?

8
00:00:26,500 --> 00:00:33,000
Well, the biggest key is to do walking meditation.

9
00:00:33,000 --> 00:00:36,000
The problem is, yeah, human beings, guess what?

10
00:00:36,000 --> 00:00:43,000
We're not meant to sit in a static position for 24 hours a day.

11
00:00:43,000 --> 00:00:47,500
Have you ever tried lying in bed for 24 hours?

12
00:00:47,500 --> 00:00:50,000
Try that and see how it goes.

13
00:00:50,000 --> 00:00:53,000
Everyone thinks, oh, sleeping is so wonderful and lying down.

14
00:00:53,000 --> 00:00:54,500
The pleasure that comes from it.

15
00:00:54,500 --> 00:00:56,000
I'll try doing it for 24 hours.

16
00:00:56,000 --> 00:00:58,000
No, we're not meant to be static.

17
00:00:58,000 --> 00:01:06,000
Walking meditation really answers your question, I think, and shows that what that monk is saying,

18
00:01:06,000 --> 00:01:09,000
from the sounds of it is not accurate.

19
00:01:09,000 --> 00:01:13,000
It's a misunderstanding of what the real problem is.

20
00:01:13,000 --> 00:01:15,000
The real problem is not continuous meditation.

21
00:01:15,000 --> 00:01:23,000
The real problem is thinking that meditation is only sitting still and attributing it.

22
00:01:23,000 --> 00:01:26,000
The blood clots and so on therefore to the meditation, which of course is not.

23
00:01:26,000 --> 00:01:31,000
Meditation is a state of mind and it's something that can be performed in any position.

24
00:01:31,000 --> 00:01:33,000
It should be performed in every position.

25
00:01:33,000 --> 00:01:36,000
It's the Buddha said, gachun-tou-wa-gachami-ti-bhajana-ti.

26
00:01:36,000 --> 00:01:39,000
When walking, he knows I walk.

27
00:01:39,000 --> 00:02:01,320
Sthit-tou-mithi-ti-tou-tou.

28
00:02:01,320 --> 00:02:03,280
Just testing.

29
00:02:03,280 --> 00:02:07,120
When standing, you know, I stand.

30
00:02:07,120 --> 00:02:12,080
Nissino, Nissino, Miti, Pajan, Miti, Miti, Miti, Miti.

31
00:02:12,080 --> 00:02:16,040
Sayyanova, Sayyanova, Miti, Pajanati.

32
00:02:16,040 --> 00:02:18,040
When lying, you know, I'm lying down.

33
00:02:18,040 --> 00:02:28,200
When walking, going forth, when going back at the Kanto,

34
00:02:28,200 --> 00:02:35,760
Pajanati, Miti, Miti, I know what I think.

35
00:02:35,760 --> 00:02:39,120
When going forth, when going back, when extending his arm,

36
00:02:39,120 --> 00:02:42,080
when flexing his arm, and so on, he knows it all.

37
00:02:42,080 --> 00:02:44,800
That's how meditation should be performed, according

38
00:02:44,800 --> 00:02:46,760
to the Satipatanas Center.

39
00:02:46,760 --> 00:02:50,040
So, yeah, dangerous to sit in meditation for too long.

40
00:02:50,040 --> 00:02:52,280
I never encouraged people to meditate more than an hour.

41
00:02:52,280 --> 00:02:55,360
My teacher once had me do two hours walking, one hour

42
00:02:55,360 --> 00:02:57,920
sitting, and then one hour walking, two hours sitting,

43
00:02:57,920 --> 00:02:59,560
which is okay.

44
00:02:59,560 --> 00:03:01,960
It was actually quite torture.

45
00:03:01,960 --> 00:03:08,040
But that's not so bad for two hours, you can do it.

46
00:03:08,040 --> 00:03:12,400
And the other thing is that blood clots and such often

47
00:03:12,400 --> 00:03:18,480
come from a wider range of issues.

48
00:03:18,480 --> 00:03:21,240
In Thailand, for example, the food is horrible.

49
00:03:21,240 --> 00:03:24,440
The food that the monks get is way too much meat, and sugar,

50
00:03:24,440 --> 00:03:27,880
and fat, and so on.

51
00:03:27,880 --> 00:03:30,520
And that's the kind of thing that I would assume leads

52
00:03:30,520 --> 00:03:34,640
to blood clots, an incredible amount of meat and fat,

53
00:03:34,640 --> 00:03:38,320
and so on.

54
00:03:38,320 --> 00:03:41,720
So food is definitely an issue.

55
00:03:41,720 --> 00:03:44,360
And another one is probably the type of meditation

56
00:03:44,360 --> 00:03:47,080
that one practices, because we pass in a meditation

57
00:03:47,080 --> 00:03:52,640
actually liberates things like the blood and the body

58
00:03:52,640 --> 00:03:54,600
into a great extent.

59
00:03:54,600 --> 00:03:57,480
So as a result, I don't know if I can say that,

60
00:03:57,480 --> 00:03:59,480
because in the end, it's all the body,

61
00:03:59,480 --> 00:04:01,920
and you still might get blood clots.

62
00:04:01,920 --> 00:04:08,200
But there's a lot of fat monks in the world.

63
00:04:08,200 --> 00:04:11,160
And it's because either they're not practicing correctly,

64
00:04:11,160 --> 00:04:12,520
or even if they're practicing correctly,

65
00:04:12,520 --> 00:04:14,960
people are feeding them horrible food.

66
00:04:14,960 --> 00:04:17,480
And yeah, when they do meditation,

67
00:04:17,480 --> 00:04:21,120
it can be when they sit in the wrong position,

68
00:04:21,120 --> 00:04:24,560
and they sit for a long time, it can have effect on the mind.

69
00:04:24,560 --> 00:04:29,120
But today, the truth, I've never heard of people

70
00:04:29,120 --> 00:04:31,760
having strokes and blood clots based on meditation.

71
00:04:31,760 --> 00:04:32,920
But it may happen.

72
00:04:32,920 --> 00:04:34,720
I did hear about one monk, who sat still

73
00:04:34,720 --> 00:04:37,920
for a long, long time, not in meditation.

74
00:04:37,920 --> 00:04:41,200
But he sat still for a long, long time teaching people.

75
00:04:41,200 --> 00:04:43,000
He got so famous that he would sit there,

76
00:04:43,000 --> 00:04:45,760
and he had to teach people, and sit in the same position

77
00:04:45,760 --> 00:04:48,040
for hours and hours and hours.

78
00:04:48,040 --> 00:04:49,440
And I think he's in a wheelchair now.

79
00:04:49,440 --> 00:04:54,880
He's still very famous monk, but his legs stopped working somehow.

80
00:04:54,880 --> 00:04:56,880
This is what I heard.

81
00:04:56,880 --> 00:05:26,720
Anyway, hope that answers your question.

