1
00:00:00,000 --> 00:00:23,600
Hey, good evening, everyone, it's very starting a little late today.

2
00:00:23,600 --> 00:00:35,600
We are doing questions and answers. Were you sending your questions on our website?

3
00:00:35,600 --> 00:00:52,600
And I have, so we have 31 questions. I've been having read them, so it's all going to be new.

4
00:00:52,600 --> 00:01:06,600
So, someone here talking about the view, and the view that relaxing bodily mental formations is a critical step without which it's hard to get deeper into the

5
00:01:06,600 --> 00:01:25,600
front of the John, it's okay. We're not interested really in going deeper into the John.

6
00:01:25,600 --> 00:01:31,600
So, there are different ways you can practice meditation if you're interested in becoming enlightened,

7
00:01:31,600 --> 00:01:45,600
but ultimately you have to practice in sight meditation, it's just a question of how you work it because why I say it like that is because there are many types of meditation that aren't going to lead to enlightenment.

8
00:01:45,600 --> 00:01:57,600
And the key ingredient that's missing is insight, so it's possible to practice only somewhat to meditation, tranquility meditation, and never become enlightened.

9
00:01:57,600 --> 00:02:07,600
But it's not possible to practice insight meditation, and I mean, it's not possible for practicing it correctly for it to not lead to enlightenment.

10
00:02:07,600 --> 00:02:24,600
Meaning, it's insufficient ingredient, or it's a necessary ingredient.

11
00:02:24,600 --> 00:02:33,600
But it's sufficient as well because it's possible to practice insight meditation, and as a result of practicing insight meditation develop calm.

12
00:02:33,600 --> 00:02:37,600
The calm can come along with insight meditation, obviously.

13
00:02:37,600 --> 00:02:49,600
We're starting to see things more clearly. I think this is a big thing that the critics of dry insight meditation don't see is that you're obviously becoming more calm as you understand things more clearly.

14
00:02:49,600 --> 00:02:58,600
Unless they say, no, no, it's not possible to see things clearly without going into the Johnus first, which I think is patently false.

15
00:02:58,600 --> 00:03:18,600
We have meditators come here through here who can all verify that they learn many things about themselves that drastically and quite impressively reduce their stress levels, make them more calm.

16
00:03:18,600 --> 00:03:21,600
So calm comes along with insight.

17
00:03:21,600 --> 00:03:28,600
Now, if you practice tranquility first, of course your mind is very strong when you begin to practice insight meditation.

18
00:03:28,600 --> 00:03:32,600
So it's just a whole other path.

19
00:03:32,600 --> 00:03:38,600
But I'm not going to go into too much detail about other people's views.

20
00:03:38,600 --> 00:03:43,600
It's all just intellectual discussion.

21
00:03:43,600 --> 00:03:56,600
It's questions like these that are more interesting. So here's a question about giving attention to the rising and falling of the stomach and become sense and awareness that I'm somehow watching myself.

22
00:03:56,600 --> 00:04:00,600
It's just experience something I should cultivate or do I simply know didn't return to breathing.

23
00:04:00,600 --> 00:04:03,600
There will never be an experience that you should cultivate.

24
00:04:03,600 --> 00:04:10,600
The idea of cultivating, the only thing you should ever cultivate is mindfulness, and that's through the noting.

25
00:04:10,600 --> 00:04:15,600
Cultivating any experience is the way of the ordinary mind.

26
00:04:15,600 --> 00:04:24,600
Our minds are inclined to seek out and become absorbed in experiences, and that is what we're trying to change.

27
00:04:24,600 --> 00:04:31,600
We're trying to see things objectively and not get absorbed in them or caught up in them.

28
00:04:31,600 --> 00:04:39,600
So any kind of cultivation, whatever that might mean, is not the path.

29
00:04:39,600 --> 00:04:45,600
So you should definitely know that feeling.

30
00:04:45,600 --> 00:04:51,600
Okay, you tend to have an extreme personality, I think that's what you're saying.

31
00:04:51,600 --> 00:04:54,600
I get caught up obsession with certain subjects.

32
00:04:54,600 --> 00:04:57,600
When I meditate, I get obsessed with the practice itself.

33
00:04:57,600 --> 00:05:01,600
It leads me depressed and seek a good feeling in unhealthy ways.

34
00:05:01,600 --> 00:05:05,600
Well, meditation can make you very depressed because it challenges you.

35
00:05:05,600 --> 00:05:09,600
I mean, meditation is not meant to make things...

36
00:05:09,600 --> 00:05:16,600
It's not meant to be an easy path where you avoid or where you escape all your problems.

37
00:05:16,600 --> 00:05:20,600
So when you feel depressed in meditation, the important thing is to not be discouraged by that.

38
00:05:20,600 --> 00:05:30,600
And to see that as a challenge, and some thing to work on, not to change the moments of the loop that you're caught up in.

39
00:05:30,600 --> 00:05:33,600
It's really a loop of experiences and reactions.

40
00:05:33,600 --> 00:05:37,600
And when you start to be mindful, you break that up and you change it.

41
00:05:37,600 --> 00:05:42,600
So when you're depressed, you would just say to yourself, depressed, depressed.

42
00:05:42,600 --> 00:05:50,600
When you feel obsessed about the meditation and how it's sort of feeling that it's like a wanting or a stress about it,

43
00:05:50,600 --> 00:06:00,600
whatever the feeling is, just note that.

44
00:06:00,600 --> 00:06:06,600
So don't get constantly switching from object or should I start to stay with an object for a longer period of time.

45
00:06:06,600 --> 00:06:11,600
Try to stay with something until it ceases if possible.

46
00:06:11,600 --> 00:06:17,600
It's not momentary concentration, but it's just a good habit.

47
00:06:17,600 --> 00:06:27,600
It's sort of a habit that you're trying to form.

48
00:06:27,600 --> 00:06:35,600
Because certain habits and techniques are going to be more conducive to developing concentration.

49
00:06:35,600 --> 00:06:39,600
And that's one of them. If you jump from object to object, there's a potential.

50
00:06:39,600 --> 00:06:47,600
It's not wrong per se, but it's more likely to lead to distraction.

51
00:06:47,600 --> 00:07:02,600
If you stay with one object for longer, you can be momentarily aware of it, mindfulness can arise, but it's also more less distracting.

52
00:07:02,600 --> 00:07:06,600
And it's also a good way to ensure that you're not avoiding an object.

53
00:07:06,600 --> 00:07:13,600
If you flip from object to object, it's easy to then when something bad comes up while you jump to something else.

54
00:07:13,600 --> 00:07:19,600
It can't handle or you jump to something else. It has a potential to lead to bad habits.

55
00:07:19,600 --> 00:07:28,600
Can you say something about fasting? Well, it's not recommended because this meditation is not going to make you calm.

56
00:07:28,600 --> 00:07:31,600
It's going to be disturbing.

57
00:07:31,600 --> 00:07:37,600
I mean, the calm comes through working out your problems, which is quite disturbing and often stressful.

58
00:07:37,600 --> 00:07:46,600
So, there's going to be a need for food. I wouldn't recommend eating a lot, but not eating at all is not a good idea.

59
00:07:46,600 --> 00:07:54,600
It's not going to be healthy because of the nature, the state of your mind during insight meditation.

60
00:07:54,600 --> 00:08:00,600
How does one practice with me directly? Well, you can practice online. We do online courses.

61
00:08:00,600 --> 00:08:19,600
And you can also come here. You can look on our website and SiriMungalow.org and look at our schedule and find a way to come and stay.

62
00:08:19,600 --> 00:08:30,600
The sudden onset of panic attacks, negative thoughts, the heart starts racing. How to handle something like this while driving.

63
00:08:30,600 --> 00:08:33,600
The more I note the breath, the more I feel like suffocating, which means you're there.

64
00:08:33,600 --> 00:08:41,600
So, don't focus only on the breath. I mean, you should be mindful. Our meditation is not just about the breath, not really about the breath at all.

65
00:08:41,600 --> 00:08:48,600
It's about your experiences. So, when you feel afraid, with panic attacks, it's important to be able to separate the physical and the mental.

66
00:08:48,600 --> 00:09:00,600
So, the mental is the feeling of fear and the disliking and the desire to be free from it when you try to stop it, for example.

67
00:09:00,600 --> 00:09:09,600
And then the physical is the heart racing and the stomach tension and the shoulders tense and the headache and whatever kind of feeling.

68
00:09:09,600 --> 00:09:12,600
There is the shaking. There can be many different things.

69
00:09:12,600 --> 00:09:20,600
All of that is just physical and it's important to remind yourself, even sort of without meditating to remind yourself, you know, that's not panic.

70
00:09:20,600 --> 00:09:25,600
That's just physical. That's okay. That's not a problem.

71
00:09:25,600 --> 00:09:31,600
The real problem is the fear. But even the fear, you know, you don't want to see it as a problem.

72
00:09:31,600 --> 00:09:40,600
So, you want to look at the fear as just fear and try and experience it without reacting.

73
00:09:40,600 --> 00:09:48,600
The feeling of motion is strong and permanent as it's useful to note, not on the feeling itself, but on resistance, aversion or wanting to change it.

74
00:09:48,600 --> 00:09:54,600
It's definitely useful to note the resistance, the aversion to the feeling.

75
00:09:54,600 --> 00:10:11,600
You know, it's layers or it's a sequence. Our habits are patterns and you can cut it at any, you should cut it at whatever level is readily apparent.

76
00:10:11,600 --> 00:10:18,600
So, find the concept of rebirth, re-variant, confusing.

77
00:10:18,600 --> 00:10:26,600
Current consequences go far beyond our so-called death. But I don't see how you go from there to the conclusion that an insect could have been a human in previous life.

78
00:10:26,600 --> 00:10:34,600
How can it be so linear, one particular human as far as one particular insect?

79
00:10:34,600 --> 00:10:45,600
Yeah, well, consciousness is an individual consciousness. Our consciousness does not like it disperses and goes to all different insects.

80
00:10:45,600 --> 00:10:59,600
Reality is linear, so there's, after death, there's only, you have to, so you've been misinformed that consciousness is somehow not individual.

81
00:10:59,600 --> 00:11:07,600
It is an individual thing, an individual strain or chain of consciousness, and that doesn't change.

82
00:11:07,600 --> 00:11:21,600
But being born a human just means that stream of consciousness taking up residence in the fetus, so the same with an insect and so on.

83
00:11:21,600 --> 00:11:28,600
I'm not doing very good about hitting this button right there. Hit the button, then it's right there.

84
00:11:28,600 --> 00:11:35,600
Is it dressing in black detrimental to the practice? No.

85
00:11:35,600 --> 00:11:46,600
So, wearing white clothes is sort of a reminder of purity, cleanliness, dark black is, it's really just a tradition.

86
00:11:46,600 --> 00:11:55,600
It's not even a Buddhist tradition, but it's something that was picked up by Buddhists because in India they wore white because it was signified pure.

87
00:11:55,600 --> 00:12:18,600
Things get dirty, they get darker. When you clean things, I mean, they don't always get whiter, and darkness is associated with ignorance and light is associated with knowledge because when you light a fire, light a candle, it's, I guess it's white or I don't know.

88
00:12:18,600 --> 00:12:30,600
White and black just have these, even in India, they have these connotations of having, you know, these significance, but it's really just cultural.

89
00:12:30,600 --> 00:12:38,600
I mean, one of my African-Canadian friends, he asked me, he said, so it's this deal about white and black, and I said, what?

90
00:12:38,600 --> 00:12:53,600
You know, honestly, human, it doesn't apply to humans because we're not black, there's no such thing as a black human, not that it would matter if there was, because there's a good, a jotica about that.

91
00:12:53,600 --> 00:13:00,600
Someone jotica 440, I think, remember it because I always go back to it, it's about a black guy who's meditating.

92
00:13:00,600 --> 00:13:09,600
The body's at this bar in black, dark skin. Everyone calls him blacky, and this angel comes down and talks to him and says,

93
00:13:09,600 --> 00:13:14,600
what are you, how do you expect to do you blacky?

94
00:13:14,600 --> 00:13:29,600
Like, because black, of course, has this connotation of being bad, and this is a very cultural thing because people who work in the fields all day, the poor people would, of course, have darker skin and the rich people who would live inside would have lighter skin, so there's that connotation.

95
00:13:29,600 --> 00:13:32,600
So it does have a racist connotation.

96
00:13:32,600 --> 00:13:46,600
It's in fact interesting white and black are somehow tied up with racism, or it's not even racism, it's whatever the word would be, colorism.

97
00:13:46,600 --> 00:14:07,600
Dark skin people treated very poorly in many societies, not just this whole American slave trade thing, but in India and even in Sri Lanka, even in Thailand, as well as places, dark skin people.

98
00:14:07,600 --> 00:14:16,600
Anyway, no, it has nothing to do with anything. It's just culture.

99
00:14:16,600 --> 00:14:23,600
I want to start my journey in Buddhism from the basic or beginning, so Buddhist library, I wish to learn, learn like the Buddhists.

100
00:14:23,600 --> 00:14:32,600
So any proper method that you go through the teachings, it depends on the country, every country has their own way.

101
00:14:32,600 --> 00:14:50,600
And generally just encourage people to meditate, and as they're living as a meditator to then spend times reading the Buddhist teaching.

102
00:14:50,600 --> 00:14:55,600
How can I try to avoid or try to have distance towards entertainment?

103
00:14:55,600 --> 00:15:04,600
I wouldn't worry too much about that if you're living as a householder. It's difficult, certainly possible.

104
00:15:04,600 --> 00:15:11,600
The best way would be to come and do a meditation course or find a place to do a meditation course, because it's a difficult one.

105
00:15:11,600 --> 00:15:26,600
But if you work on it, you can, ideally living in a meditation center would be the best way.

106
00:15:26,600 --> 00:15:40,600
I don't want to really talk about other people who claim to be enlightened, so I'm going to just skip that.

107
00:15:40,600 --> 00:15:49,600
The guilt seems to cripple me, both my time.

108
00:15:49,600 --> 00:15:53,600
Simple things that I'm aware I should not be guilty about, what do I do?

109
00:15:53,600 --> 00:15:58,600
Wouldn't worry too much about it. I mean, that's an important part of the practice is not to worry too much about things.

110
00:15:58,600 --> 00:16:06,600
Yeah, you're feeling guilty. Don't react to the guilt, because this is what someone mentioned earlier, you react to your reactions.

111
00:16:06,600 --> 00:16:11,600
You feel guilty, then you react to that and you're upset because you're feeling guilty.

112
00:16:11,600 --> 00:16:18,600
So just not feeling, feeling guilt is kind of like a disliking or frustration, an unpleasant feeling.

113
00:16:18,600 --> 00:16:22,600
So note those.

114
00:16:22,600 --> 00:16:27,600
You have to be really the best way is to just out. It's like a stare down, really.

115
00:16:27,600 --> 00:16:33,600
If you blink, it wins. If you don't blink, you win. You wait until it blinks.

116
00:16:33,600 --> 00:16:48,600
Mindfulness is really just like a stare down. You stop reacting. You stop letting your emotions and experiences get to you.

117
00:16:48,600 --> 00:16:52,600
Did the Buddha invent the mindfulness Satipatana?

118
00:16:52,600 --> 00:16:58,600
I think you could say it's something that each Buddha comes up with, but yes, it is a mindfulness.

119
00:16:58,600 --> 00:17:05,600
Mindfulness is a Satipatana is a construct. It's not a ultimate reality. These are something.

120
00:17:05,600 --> 00:17:15,600
This is an example of something that doesn't exist in reality. It's a form, it's a format or a construct created by the Buddha.

121
00:17:15,600 --> 00:17:20,600
So not all constructs are bad. Constructs are often very good.

122
00:17:20,600 --> 00:17:27,600
Yoga Nidra, again, not going to talk about other techniques.

123
00:17:27,600 --> 00:17:30,600
To be clear, we only teach one technique.

124
00:17:30,600 --> 00:17:34,600
Is this useful? Is that useful? This technique is full.

125
00:17:34,600 --> 00:17:40,600
I mean, I could. It's not that it's a bad question. It's too much.

126
00:17:40,600 --> 00:17:47,600
Let's keep it simple. The answer is probably no.

127
00:17:47,600 --> 00:18:05,600
I mean, the answer is probably maybe, but maybe. I guess the answer is probably maybe.

128
00:18:05,600 --> 00:18:12,600
So theoretically, we can avoid contact by redirecting our consciousness to another object.

129
00:18:12,600 --> 00:18:18,600
Where we deliberately redirect consciousness inside that scary thoughts arise.

130
00:18:18,600 --> 00:18:25,600
Okay. That's an interesting question. I don't think I've ever had this question before or anything like it.

131
00:18:25,600 --> 00:18:33,600
I mean, it's not a very uncommon concept. The idea to divert your consciousness.

132
00:18:33,600 --> 00:18:39,600
And there are cases where it can be wholesome. It's not insight meditation, but for example, if you're angry at someone,

133
00:18:39,600 --> 00:18:48,600
you divert your mind by cultivating love. Or really with mindfulness, it's a sort of way of doing that.

134
00:18:48,600 --> 00:18:53,600
If you have ignorance about something, you change that by diverting your mind.

135
00:18:53,600 --> 00:18:59,600
But diverting your mind from something that you don't like, something that's scary.

136
00:18:59,600 --> 00:19:07,600
First of all, contact has arisen. Now, if you divert your mind, if you're able to divert your mind before the fear arises,

137
00:19:07,600 --> 00:19:17,600
that would be interesting. But the concept of avoiding, you're creating a new kind of contact.

138
00:19:17,600 --> 00:19:22,600
I mean, there's many, many minds involved with that process of diverting your attention.

139
00:19:22,600 --> 00:19:28,600
It involves contact many times, because contact is in every moment where you have some kind of volition.

140
00:19:28,600 --> 00:19:33,600
I'm going to divert my attention. So there's contact going on there.

141
00:19:33,600 --> 00:19:38,600
And I would say generally on wholesome way, because you're avoiding problems.

142
00:19:38,600 --> 00:19:42,600
You have aversion to it. You're cultivating a habit of aversion.

143
00:19:42,600 --> 00:19:49,600
So it's certainly not a good idea.

144
00:19:49,600 --> 00:19:58,600
Okay, I made a new account.

145
00:19:58,600 --> 00:20:02,600
Everything's not working. I just want this scream right now.

146
00:20:02,600 --> 00:20:11,600
Well, we have a contact form somewhere you can use.

147
00:20:11,600 --> 00:20:18,600
Someone help this person. I'll redirect you to our support team, hopefully someone can help you.

148
00:20:18,600 --> 00:20:24,600
Sometimes it's possible. It's possible that it's very difficult to do good things.

149
00:20:24,600 --> 00:20:31,600
Doing good things is not always easy, and sometimes bad past karma can just get in the way in really strange ways.

150
00:20:31,600 --> 00:20:36,600
So I would say, take that as your path, the part of your path.

151
00:20:36,600 --> 00:20:42,600
These are the obstacles that are placed before you, not by God or something, but this is the situation that you're in,

152
00:20:42,600 --> 00:20:48,600
which can often be very bizarre in terms of the obstacles that are put in front of us again and again and again.

153
00:20:48,600 --> 00:20:52,600
So consider that to be something you just have to work through.

154
00:20:52,600 --> 00:20:56,600
If you've read my booklet, hopefully you have access to my booklet on how to meditate.

155
00:20:56,600 --> 00:20:58,600
That's really the most important thing.

156
00:20:58,600 --> 00:21:02,600
That's Buddhism. That should be your doorway to Buddhism.

157
00:21:02,600 --> 00:21:07,600
Don't worry about everything else too much. Try and just be mindful of everything else in the wanting to scream.

158
00:21:07,600 --> 00:21:12,600
Well, it's a good meditation object.

159
00:21:12,600 --> 00:21:29,600
Okay, so I'm going to talk about casino. No, I don't teach this so much. Don't teach that. No, don't teach that.

160
00:21:29,600 --> 00:21:32,600
Don't teach that.

161
00:21:32,600 --> 00:21:38,600
Yeah, okay, I'd recommend reading my booklet because I'm just going to skip on us.

162
00:21:38,600 --> 00:21:43,600
What's your view on someone close to your cheats you in any way?

163
00:21:43,600 --> 00:21:50,600
Well, don't forget that and be wary next time, but also be mindful of your reactions to it.

164
00:21:50,600 --> 00:21:54,600
It doesn't mean you have to let people walk over you all over you necessarily.

165
00:21:54,600 --> 00:21:58,600
I mean, it's an interesting practice to just let people walk all over you.

166
00:21:58,600 --> 00:22:02,600
They'd have to be quite enlightened to allow that to be a fruitful practice.

167
00:22:02,600 --> 00:22:05,600
Mostly it just makes you very upset and angry.

168
00:22:05,600 --> 00:22:16,600
So, you know, be just be careful around such people in the future, but also work through your emotions that having been cheated.

169
00:22:16,600 --> 00:22:21,600
How to not identify with this memory of myself as I?

170
00:22:21,600 --> 00:22:24,600
I mean, you can't just not identify with it. That's our habit.

171
00:22:24,600 --> 00:22:30,600
That's our accustomed to dealing with it in that way.

172
00:22:30,600 --> 00:22:40,600
You know, meditation is about changing that by seeing that it's a ridiculous sort of perspective because there's nothing there that's me or mine or I.

173
00:22:40,600 --> 00:22:43,600
Reality is just moments of experience.

174
00:22:43,600 --> 00:22:55,600
So, the more comfortable and accustomed you become to being mindful, the less accustomed you will be to thinking of things as me and mine and I.

175
00:22:55,600 --> 00:22:59,600
How should we understand the middle way?

176
00:22:59,600 --> 00:23:02,600
Okay, the middle way is a razor, really.

177
00:23:02,600 --> 00:23:22,600
The ultimate middle way is the perfect point where you are neither hurting yourself, like pushing anything away or pulling anything towards you, really.

178
00:23:22,600 --> 00:23:28,600
The perfect middle way is the moment when you're perfectly mindful and you're not reacting in any way.

179
00:23:28,600 --> 00:23:33,600
So, it's not just some general sense of moderation.

180
00:23:33,600 --> 00:23:36,600
It certainly has really nothing to do with moderation.

181
00:23:36,600 --> 00:23:45,600
It means neither torturing yourself, doing anything that hurts you, nor doing anything for the purpose of cultivating pleasure.

182
00:23:45,600 --> 00:23:49,600
That's the middle way.

183
00:23:49,600 --> 00:23:57,600
And really, the perfect way of doing that is mindful.

184
00:23:57,600 --> 00:23:59,600
I wonder what questions I would ask.

185
00:23:59,600 --> 00:24:06,600
So, you have no questions because meditation has the same answer for whatever question, not feeling and keep noting.

186
00:24:06,600 --> 00:24:12,600
I mean, basically, you can see answering a lot of these questions, there's other information to provide.

187
00:24:12,600 --> 00:24:21,600
But, if you're comfortable with that, if you don't have any questions, consider yourself in a very good way.

188
00:24:21,600 --> 00:24:27,600
Because, tomorrow, Maga Puja, very well, might be, and I'm not so up on that.

189
00:24:27,600 --> 00:24:34,600
But, it's around this time every year.

190
00:24:34,600 --> 00:24:42,600
Very well, maybe.

191
00:24:42,600 --> 00:24:46,600
So, those are traditional holidays.

192
00:24:46,600 --> 00:24:51,600
In fact, I think only really in Thailand, in Sri Lanka, they don't celebrate.

193
00:24:51,600 --> 00:24:58,600
The Thailand's, the Thailand Cambodia, Laos, in Sri Lanka, they don't celebrate them.

194
00:24:58,600 --> 00:25:02,600
Not as anything beyond the poi, they call it the poi a day.

195
00:25:02,600 --> 00:25:18,600
But, I don't think they have a Maga Puja Thai thing, or it's a Southeast Asian thing.

196
00:25:32,600 --> 00:25:42,600
Okay, someone having jolts?

197
00:25:42,600 --> 00:25:44,600
Jolts are part of your body.

198
00:25:44,600 --> 00:25:49,600
I mean, there are many strange experiences that can come from meditation.

199
00:25:49,600 --> 00:25:59,600
It sounds like you might be taking more from this than it actually has inherently.

200
00:25:59,600 --> 00:26:04,600
I mean, it's just an experience. It's nothing to do with wisdom.

201
00:26:04,600 --> 00:26:12,600
It's just the experience that you're having at this point in your meditation, which is not a good sign or a bad sign.

202
00:26:12,600 --> 00:26:17,600
It's just an experience that's impermanent, it's unsatisfying, it's uncontrollable.

203
00:26:17,600 --> 00:26:25,600
That's how you should look at it. You should just see it arising and ceasing, note it, and let it go.

204
00:26:25,600 --> 00:26:31,600
That sounds like you're doing great in meditating and not sleeping too much.

205
00:26:31,600 --> 00:26:41,600
Can you talk about the kukka, kukka jupa masu at that?

206
00:26:41,600 --> 00:26:48,600
I always get these ones mixed up, which one is the kukka jupa.

207
00:26:48,600 --> 00:26:56,600
It's the song. I don't know what you want me to talk about.

208
00:26:56,600 --> 00:26:59,600
He says that if someone is...

209
00:26:59,600 --> 00:27:01,600
I mean, it's really a powerful simile.

210
00:27:01,600 --> 00:27:03,600
If someone's cutting your limbs off, you should not...

211
00:27:03,600 --> 00:27:08,600
Anyone who feels anger for the person sowing your limbs off.

212
00:27:08,600 --> 00:27:15,600
Anyone who feels anger at that moment is not a follower of mind.

213
00:27:15,600 --> 00:27:20,600
That's a long story. There's a story about...

214
00:27:20,600 --> 00:27:28,600
And there's many different similes.

215
00:27:28,600 --> 00:27:31,600
I don't know that there's much to talk about.

216
00:27:31,600 --> 00:27:34,600
You'd have to give me a more specific question.

217
00:27:34,600 --> 00:27:44,600
Because it's just fairly straightforward, I think.

218
00:27:44,600 --> 00:27:49,600
I'm going to skip this one because it's nothing to do with what we teach.

219
00:27:49,600 --> 00:28:02,600
Not a question, but welcome.

220
00:28:02,600 --> 00:28:06,600
What a person understands Buddhist principles at a very young age.

221
00:28:06,600 --> 00:28:08,600
Isn't it boring to wait to die for liberation?

222
00:28:08,600 --> 00:28:11,600
Well, an enlightened person is never bored, so...

223
00:28:11,600 --> 00:28:13,600
No.

224
00:28:13,600 --> 00:28:19,600
Things are sent to hell, heaven.

225
00:28:19,600 --> 00:28:25,600
Is it on the basis of mental state of the being or on the basis of wholesome or on wholesome acts?

226
00:28:25,600 --> 00:28:31,600
Well, the thing about acts is they're very powerful, they involve very powerful mind state.

227
00:28:31,600 --> 00:28:35,600
If you kill someone, it's much more powerful than just wanting to kill them.

228
00:28:35,600 --> 00:28:38,600
But it's still all to do with the mind state.

229
00:28:38,600 --> 00:28:40,600
Your state of mind.

230
00:28:40,600 --> 00:28:43,600
On the other hand, you kill someone without intending to.

231
00:28:43,600 --> 00:28:45,600
It's not an unwholesome thing.

232
00:28:53,600 --> 00:28:57,600
Maybe with Tantra? No.

233
00:28:57,600 --> 00:29:03,600
So, our benefit to breath control, don't teach it, but I don't think it's that useful.

234
00:29:03,600 --> 00:29:07,600
Again, I'm not really interested in answering questions about this practice or that practice.

235
00:29:07,600 --> 00:29:10,600
What do you think of this? What do you think of it?

236
00:29:10,600 --> 00:29:23,600
There's infinite number of practices. It's not really what this is for.

237
00:29:23,600 --> 00:29:29,600
How to differentiate being conscious from just a state of high concentration.

238
00:29:29,600 --> 00:29:31,600
Or is it the same?

239
00:29:31,600 --> 00:29:36,600
I mean, states of high concentration tend to have their own qualities to them.

240
00:29:36,600 --> 00:29:40,600
And they're fairly easy to distinguish between ordinary consciousness.

241
00:29:40,600 --> 00:29:54,600
But I'm really sure about that question.

242
00:29:54,600 --> 00:30:09,600
It's been a little bit interesting question.

243
00:30:09,600 --> 00:30:12,600
Well, it depends very much on the person.

244
00:30:12,600 --> 00:30:17,600
It's not everyone is going to become enlightened in a week or a month or even a year.

245
00:30:17,600 --> 00:30:20,600
For some people, it's going to take many lifetimes.

246
00:30:20,600 --> 00:30:23,600
Does it take longer? Again, it depends on the individual.

247
00:30:23,600 --> 00:30:27,600
It may very well be that your...

248
00:30:27,600 --> 00:30:31,600
When you've built up your wholesomeness is only good enough to get you that far.

249
00:30:31,600 --> 00:30:35,600
And now you've got a lot more work to do to get the rest of the stages.

250
00:30:35,600 --> 00:30:44,600
But it depends much on the person, on the individual.

251
00:30:44,600 --> 00:30:50,600
How to discern the difference between fruition knowledge and path knowledge at the stage of second again.

252
00:30:50,600 --> 00:30:53,600
It seems a bit grays since no fetters are eradicated.

253
00:30:53,600 --> 00:30:58,600
So fetters are eradicated, but not categorically.

254
00:30:58,600 --> 00:31:14,600
At that stage, a certain level of greed and anger, greed and aversion, have been eradicated.

255
00:31:14,600 --> 00:31:23,600
But I mean, the thing to note about really all of these states is that it's the same experience.

256
00:31:23,600 --> 00:31:25,600
It's the experience of nibana that doesn't change.

257
00:31:25,600 --> 00:31:33,600
They're given names like path and fruit because of the effect, not because of the nature of the mind.

258
00:31:33,600 --> 00:31:35,600
The mind has the same nature.

259
00:31:35,600 --> 00:31:39,600
The experience has the same nature if you want to call it an experience.

260
00:31:39,600 --> 00:31:44,600
The process or the moments are identical.

261
00:31:44,600 --> 00:31:48,600
It's just the first one in the sequence. It's called path fruition.

262
00:31:48,600 --> 00:31:52,600
Because it's the one that has the effect.

263
00:31:52,600 --> 00:31:55,600
If you pour water on a fire, the fire is out.

264
00:31:55,600 --> 00:32:00,600
If you then pour more water on the fire to cool it down, it's not going to put the fire more out.

265
00:32:00,600 --> 00:32:02,600
The flame is already out.

266
00:32:02,600 --> 00:32:08,600
So it's only the first time, the first moment that we call path consciousness.

267
00:32:08,600 --> 00:32:18,600
In the scene, it's a good one.

268
00:32:18,600 --> 00:32:24,600
In the scene, only the scene and the heard, only the heard and the sense, only the sense and the cognizant on the cognizant.

269
00:32:24,600 --> 00:32:30,600
It's basically that this is a very good quote for an insight meditation practitioner.

270
00:32:30,600 --> 00:32:34,600
Because this is basically an explanation of insight.

271
00:32:34,600 --> 00:32:39,600
Let's see, just be seeing when you see something and try it to be just that.

272
00:32:39,600 --> 00:32:42,600
It's where the Buddha talks again and again.

273
00:32:42,600 --> 00:32:45,600
He says, Nanyimita Ghahi Nanyam Yanjana Ghahi.

274
00:32:45,600 --> 00:32:51,600
One doesn't grasp at the particulars or the characteristics.

275
00:32:51,600 --> 00:32:55,600
One is only aware of seeing as seeing.

276
00:32:55,600 --> 00:32:57,600
That's where insight comes from.

277
00:32:57,600 --> 00:33:02,600
Because then you see the building blocks of reality aren't really just seeing hearing spelling.

278
00:33:02,600 --> 00:33:05,600
It's not just spelling tasting feeling thinking.

279
00:33:05,600 --> 00:33:17,600
And you see them arising and seeing and you lose your attachment to all these other things that you realize just don't even exist.

280
00:33:17,600 --> 00:33:18,600
All right.

281
00:33:18,600 --> 00:33:22,600
Well, that was good.

282
00:33:22,600 --> 00:33:24,600
A bunch of questions tonight.

283
00:33:24,600 --> 00:33:34,600
We got through them.

284
00:33:34,600 --> 00:33:36,600
So that's all the questions for tonight.

285
00:33:36,600 --> 00:33:37,600
Thank you all for tuning in.

286
00:33:37,600 --> 00:33:40,600
Good number, even though I was late.

287
00:33:40,600 --> 00:33:43,600
It's good to see people interesting.

288
00:33:43,600 --> 00:33:46,600
Interested.

289
00:33:46,600 --> 00:33:55,600
Thank you all for tuning in. Have a good night.

