1
00:00:00,000 --> 00:00:28,000
Good evening everyone, broadcasting line April 24th, 2016.

2
00:00:28,000 --> 00:00:48,000
Today's quote is part of a famous story about a monk who had dysentery.

3
00:00:48,000 --> 00:01:00,000
And if you read this, the Buddha asks him, why are the other monks looking after you?

4
00:01:00,000 --> 00:01:03,000
He says, because I'm not useful to them.

5
00:01:03,000 --> 00:01:07,000
I'm of no use to them.

6
00:01:07,000 --> 00:01:12,000
And then he goes and asks them, the monks and the monks say, why didn't you look after them?

7
00:01:12,000 --> 00:01:21,000
And they confirm it's because he is no use to them.

8
00:01:21,000 --> 00:01:30,000
He says, I'm not a monk.

9
00:01:30,000 --> 00:01:38,000
He says, I'm not a monk.

10
00:01:38,000 --> 00:01:46,000
For what reason did the bikhu's not look after?

11
00:01:46,000 --> 00:01:48,000
So now we have to this monk.

12
00:01:48,000 --> 00:02:02,000
He says, I'm not a monk.

13
00:02:02,000 --> 00:02:11,000
He says, I'm not a monk.

14
00:02:11,000 --> 00:02:18,000
And for that reason, because of that, they don't look after him.

15
00:02:18,000 --> 00:02:26,000
But it says something, nati wo bikho emata.

16
00:02:26,000 --> 00:02:32,000
There is no, you have no mother monks.

17
00:02:32,000 --> 00:02:43,000
Nati pita, you have no father.

18
00:02:43,000 --> 00:03:07,000
He says, I'm not a monk.

19
00:03:07,000 --> 00:03:16,000
I'm not a monk.

20
00:03:16,000 --> 00:03:24,000
If you don't look after each other, monks, one to the other, each other.

21
00:03:24,000 --> 00:03:32,000
At the kocharahi, then who now will look after you?

22
00:03:32,000 --> 00:03:41,000
And he says something that's quite famous.

23
00:03:41,000 --> 00:03:44,000
Who would look after me?

24
00:03:44,000 --> 00:03:48,000
Should look after sick people.

25
00:03:48,000 --> 00:03:52,000
It doesn't actually say monks, but that's the implication here.

26
00:03:52,000 --> 00:03:54,000
Many would argue that that's what it means.

27
00:03:54,000 --> 00:04:00,000
But it's been used to interpret the importance of looking after sick people.

28
00:04:00,000 --> 00:04:06,000
I think that stretching is probably more like, because monks aren't really allowed to look after late people,

29
00:04:06,000 --> 00:04:07,000
or sick.

30
00:04:07,000 --> 00:04:09,000
That wouldn't be appropriate.

31
00:04:09,000 --> 00:04:16,000
But it's interesting if you think of it as a late person, if you're not a monk.

32
00:04:16,000 --> 00:04:19,000
I think about the idea of helping sick people.

33
00:04:19,000 --> 00:04:28,000
It's a very important thing that Buddha actually found this to be important for the monks at least,

34
00:04:28,000 --> 00:04:45,000
if not for general purposes or in society looking after sick people in general.

35
00:04:45,000 --> 00:04:49,000
But what's the most interest to me is the part where he says,

36
00:04:49,000 --> 00:04:59,000
you don't have a mother or a father who would look after you.

37
00:04:59,000 --> 00:05:02,000
He's making a very good point here, and I think it goes beyond monasticism.

38
00:05:02,000 --> 00:05:07,000
Obviously for monks, it's an important point, is that we don't have family.

39
00:05:07,000 --> 00:05:09,000
We don't have caretakers.

40
00:05:09,000 --> 00:05:15,000
If we don't look after each other, who will look after them?

41
00:05:15,000 --> 00:05:28,000
But I think on a broader level, this applies to Buddhists in general.

42
00:05:28,000 --> 00:05:43,000
In Thailand they talk about nyati, dhamma, dhamma nyati, relatives by the dhamma, sisters and brothers and mothers and fathers in the dhamma.

43
00:05:43,000 --> 00:05:51,000
Because beyond monastics, it's true that especially when you get on a spiritual path,

44
00:05:51,000 --> 00:05:58,000
when you start to practice Buddhism, you're less and less able to rely upon your families

45
00:05:58,000 --> 00:06:03,000
and the people around you who are not Buddhist or not practicing.

46
00:06:03,000 --> 00:06:09,000
Even in Buddhist countries, if your family is not meditating in Buddhist countries,

47
00:06:09,000 --> 00:06:17,000
they drink alcohol, they kill, they lie, they cheat, all these things.

48
00:06:17,000 --> 00:06:20,000
Because someone calls themselves even Buddhist.

49
00:06:20,000 --> 00:06:28,000
But much more for those of us who have become Buddhist or come to practice Buddhism,

50
00:06:28,000 --> 00:06:40,000
maybe against the hopes of our family, maybe against their wishes.

51
00:06:40,000 --> 00:06:43,000
But certainly we leave them behind.

52
00:06:43,000 --> 00:06:47,000
We go somewhere that they don't follow.

53
00:06:47,000 --> 00:06:55,000
In a sense we leave them behind, meaning they're in one place for another spiritually.

54
00:06:55,000 --> 00:07:04,000
So we stop killing, they still kill, we stop stealing, maybe they don't concern themselves about morality.

55
00:07:04,000 --> 00:07:10,000
They have no concentration, they don't use, they don't cultivate wisdom,

56
00:07:10,000 --> 00:07:16,000
they don't have the same understanding which we would call wisdom.

57
00:07:16,000 --> 00:07:25,000
It's interesting because last night, I just got back from New York,

58
00:07:25,000 --> 00:07:29,000
but last night yesterday was my mother's birthday.

59
00:07:29,000 --> 00:07:37,000
And so we had a hangout, we had one of these, basically what I'm doing now,

60
00:07:37,000 --> 00:07:41,000
not live of course, or not public.

61
00:07:41,000 --> 00:07:46,000
But we got together from all around the world, my brothers in Taiwan,

62
00:07:46,000 --> 00:07:51,000
my other two brothers are in, well they were actually with my father.

63
00:07:51,000 --> 00:07:55,000
So I think they were only, but they were with my father,

64
00:07:55,000 --> 00:08:05,000
and then my mother's in Florida and I was in New York, so four different places.

65
00:08:05,000 --> 00:08:11,000
And so we got together, we joined the hangout,

66
00:08:11,000 --> 00:08:21,000
and it was actually somewhat disappointing if you're going to let yourself get disappointed.

67
00:08:21,000 --> 00:08:31,000
I mean what is the right word, it was awkward maybe.

68
00:08:31,000 --> 00:08:40,000
For a couple, because of one of the members of the hangout was clearly drunk.

69
00:08:40,000 --> 00:08:44,000
And because it was passed over last night, if you know the Jewish holiday,

70
00:08:44,000 --> 00:08:52,000
my father's family's Jewish, on pass over the drink a lot of wine.

71
00:08:52,000 --> 00:08:55,000
Get, can get somewhat drunk.

72
00:08:55,000 --> 00:09:05,000
So at least one person, if not more, was somewhat inebriated and slurring their words.

73
00:09:05,000 --> 00:09:12,000
So the conversation quickly turned into things like alcohol and marijuana,

74
00:09:12,000 --> 00:09:22,000
and they're talking about how, I mean, I was quiet through most of it.

75
00:09:22,000 --> 00:09:25,000
My mother's really great, she doesn't do any of that.

76
00:09:25,000 --> 00:09:28,000
I think she smokes marijuana, she used to anyway.

77
00:09:28,000 --> 00:09:35,000
But she really gave up alcohol and she's interested in a clear mind.

78
00:09:35,000 --> 00:09:40,000
I think, I think she doesn't drink.

79
00:09:40,000 --> 00:09:48,000
But then my brother, in Taiwan, he started talking about this,

80
00:09:48,000 --> 00:09:52,000
something hairier, this hairier group.

81
00:09:52,000 --> 00:09:55,000
And they'd play this game where one, I wasn't listening really,

82
00:09:55,000 --> 00:09:59,000
but it was listening to the part where at the end of the game,

83
00:09:59,000 --> 00:10:05,000
they come together and they have a champagne breakfast, I think it was called.

84
00:10:05,000 --> 00:10:10,000
And he was showing pictures of caves like crate,

85
00:10:10,000 --> 00:10:17,000
no coolers full, large coolers full of champagne, whiskey and beer and every kind of alcohol.

86
00:10:17,000 --> 00:10:25,000
And he said he drank for 20 hours straight, 20 hours.

87
00:10:25,000 --> 00:10:30,000
And he said, you know, I don't want to talk about it because it sounds like a bragging.

88
00:10:30,000 --> 00:10:33,000
And the rest of us are kind of well, you know.

89
00:10:33,000 --> 00:10:36,000
But then he started saying something that's really interesting.

90
00:10:36,000 --> 00:10:42,000
I mean, there's a point why I'm telling this and why I'm talking about things that probably I shouldn't.

91
00:10:42,000 --> 00:10:47,000
It doesn't the kind of thing you should really broadcast, but, you know, we take this.

92
00:10:47,000 --> 00:10:52,000
I'm not really criticizing. I'm looking at the state of people.

93
00:10:52,000 --> 00:11:09,000
It's interesting because then even on about how much better it is, this kind of life,

94
00:11:09,000 --> 00:11:13,000
how much healthier it is.

95
00:11:13,000 --> 00:11:18,000
And how he looks at people, the rest of us back, or people back in Canada,

96
00:11:18,000 --> 00:11:24,000
all of his friends and their hair is turning gray and they look old.

97
00:11:24,000 --> 00:11:29,000
And it's because they're kind of serious. They're too serious.

98
00:11:29,000 --> 00:11:32,000
They take things too seriously.

99
00:11:32,000 --> 00:11:44,000
And I mean, it was quite clear that drinking for 20 hours was somehow the fountain of youth or something.

100
00:11:44,000 --> 00:11:47,000
Living in Taiwan.

101
00:11:47,000 --> 00:11:53,000
I'm sorry, I don't mean to bring my brother into this really, but it's such a good example.

102
00:11:53,000 --> 00:12:00,000
Because we think like this, we think of, we think happiness leads to happiness.

103
00:12:00,000 --> 00:12:07,000
I mean, it sounded so much like the angels, how angels think of heaven.

104
00:12:07,000 --> 00:12:12,000
They think that it's the good life.

105
00:12:12,000 --> 00:12:15,000
They've won. They think they won.

106
00:12:15,000 --> 00:12:20,000
They've beaten some sara and they've found the right way to live.

107
00:12:20,000 --> 00:12:27,000
They've earned it and they've found the right path.

108
00:12:27,000 --> 00:12:31,000
And they're doing the right thing. What in fact they're doing the thing?

109
00:12:31,000 --> 00:12:37,000
Like Risaka said, they're eating stale food.

110
00:12:37,000 --> 00:12:46,000
I mean, just they're eating up their good karma from the past.

111
00:12:46,000 --> 00:12:49,000
We all have this potential in life, right?

112
00:12:49,000 --> 00:12:56,000
We have the potential to use this great opportunity we have

113
00:12:56,000 --> 00:12:58,000
as human beings. We have such power.

114
00:12:58,000 --> 00:13:01,000
We can do so many things as human.

115
00:13:01,000 --> 00:13:04,000
And people say, well, what's so great about being a human?

116
00:13:04,000 --> 00:13:06,000
Human is going to be horrible people.

117
00:13:06,000 --> 00:13:12,000
I was just reading about some of the things that have gone on,

118
00:13:12,000 --> 00:13:20,000
like in South and Central America, the dictatorships that were set up by the CIA

119
00:13:20,000 --> 00:13:26,000
or whatever, right? But there's dictatorships.

120
00:13:26,000 --> 00:13:31,000
There was this.

121
00:13:31,000 --> 00:13:40,000
They had this prison in an auditorium and they sat all the prisoners in the crowds.

122
00:13:40,000 --> 00:13:45,000
And then they just like opened fire on them, shot them with machine guns.

123
00:13:45,000 --> 00:13:47,000
But that was an it.

124
00:13:47,000 --> 00:13:51,000
Like they would dry every so often they would drag someone down from the stands

125
00:13:51,000 --> 00:13:55,000
and like torture them in front of everyone like Peter.

126
00:13:55,000 --> 00:13:59,000
And at the point, human beings can do terrible things.

127
00:13:59,000 --> 00:14:03,000
And so we have this power.

128
00:14:03,000 --> 00:14:07,000
We have the power to live a hedonistic lifestyle.

129
00:14:07,000 --> 00:14:12,000
To just enjoy as much pleasure as we can.

130
00:14:12,000 --> 00:14:17,000
To not take anything seriously.

131
00:14:17,000 --> 00:14:21,000
And no matter what we do, it appears that we get away with it.

132
00:14:21,000 --> 00:14:23,000
Maybe for the most part, not always.

133
00:14:23,000 --> 00:14:27,000
But much of the time for many of the people in this world,

134
00:14:27,000 --> 00:14:32,000
it appears that we get away with whatever we do.

135
00:14:32,000 --> 00:14:35,000
But it's quite lazy to think like that.

136
00:14:35,000 --> 00:14:37,000
Like take alcohol, for example.

137
00:14:37,000 --> 00:14:42,000
Someone who says, look at alcohol and drink and drink and be merry.

138
00:14:42,000 --> 00:14:45,000
And it's a good way to live.

139
00:14:45,000 --> 00:14:46,000
Look at me.

140
00:14:46,000 --> 00:14:49,000
I'm doing great.

141
00:14:49,000 --> 00:14:51,000
This anecdotal evidence.

142
00:14:51,000 --> 00:14:53,000
Look at how great I am.

143
00:14:53,000 --> 00:14:55,000
I'm doing this.

144
00:14:55,000 --> 00:14:57,000
There was a funny story.

145
00:14:57,000 --> 00:15:02,000
A monk once told me, or I overheard him telling someone else.

146
00:15:02,000 --> 00:15:14,000
This man walked into a wrinkled sort of with a pockmarked face.

147
00:15:14,000 --> 00:15:20,000
It looked to be just really aged.

148
00:15:20,000 --> 00:15:23,000
This man walked in.

149
00:15:23,000 --> 00:15:26,000
But he was he had bright eyes.

150
00:15:26,000 --> 00:15:29,000
He looked ancient.

151
00:15:29,000 --> 00:15:32,000
He had some radiance about him.

152
00:15:32,000 --> 00:15:39,000
And he started talking about how he's never been in the hospital.

153
00:15:39,000 --> 00:15:43,000
And he's never been sick.

154
00:15:43,000 --> 00:15:45,000
It all has his life.

155
00:15:45,000 --> 00:15:51,000
He's never had any serious health issues.

156
00:15:51,000 --> 00:15:56,000
And these young men were asking him, oh, how do you do?

157
00:15:56,000 --> 00:16:01,000
And he says, oh, I drink a bottle of whiskey every day.

158
00:16:01,000 --> 00:16:03,000
Smoke a carton of cigarettes.

159
00:16:03,000 --> 00:16:05,000
And another carton.

160
00:16:05,000 --> 00:16:08,000
I say a pack of cigarettes a day.

161
00:16:08,000 --> 00:16:10,000
And a bottle of whiskey.

162
00:16:10,000 --> 00:16:15,000
And I just sit around and do nothing.

163
00:16:15,000 --> 00:16:16,000
And I'm like, wow.

164
00:16:16,000 --> 00:16:17,000
And that works.

165
00:16:17,000 --> 00:16:18,000
Yeah, it works so far.

166
00:16:18,000 --> 00:16:19,000
It works for me.

167
00:16:19,000 --> 00:16:24,000
And then someone asked him, how old are you?

168
00:16:24,000 --> 00:16:28,000
He said, 25.

169
00:16:28,000 --> 00:16:33,000
The joke sets him up to seem like he's very old.

170
00:16:33,000 --> 00:16:36,000
Like this old guy is bragging about how he's old.

171
00:16:36,000 --> 00:16:40,000
Turns out he's a young guy who looks very old because,

172
00:16:40,000 --> 00:16:43,000
anyway, it was just a silly joke.

173
00:16:43,000 --> 00:16:51,000
But their studies on alcohol, what it does to the brain,

174
00:16:51,000 --> 00:16:54,000
that is I understand it's not good for the brain.

175
00:16:54,000 --> 00:16:58,000
It causes long-term brain damage.

176
00:16:58,000 --> 00:17:04,000
I mean, not severe brain damage, but it reduces your brain capacity.

177
00:17:04,000 --> 00:17:12,000
That does marijuana according to this study.

178
00:17:12,000 --> 00:17:19,000
But moreover, happiness doesn't lead to happiness.

179
00:17:19,000 --> 00:17:28,000
Anybody can be happy when things are good,

180
00:17:28,000 --> 00:17:32,000
as long as they have the ability to enjoy.

181
00:17:32,000 --> 00:17:38,000
That's not how you measure someone's greatness.

182
00:17:38,000 --> 00:17:43,000
You don't measure someone's greatness by how good they are to join pleasure.

183
00:17:43,000 --> 00:17:51,000
You measure someone's greatness at how good they are at bearing with adversity.

184
00:17:51,000 --> 00:17:55,000
When the going gets tough, that's when you know the character of the person.

185
00:17:55,000 --> 00:17:57,000
You can't say, wow, that guy sure knows how to have fun.

186
00:17:57,000 --> 00:18:00,000
Well, it's not that hard to have.

187
00:18:00,000 --> 00:18:01,000
OK, yes, it's true.

188
00:18:01,000 --> 00:18:07,000
Some people don't know how to have fun, and you can argue that.

189
00:18:07,000 --> 00:18:13,000
It's much easier when you're in your comfort zone.

190
00:18:13,000 --> 00:18:18,000
If someone is good at having fun, well, yes, I'm drinking 20 hours a day.

191
00:18:18,000 --> 00:18:19,000
That is impressive.

192
00:18:19,000 --> 00:18:24,000
I'll admit it's a skill that you've developed.

193
00:18:24,000 --> 00:18:26,000
Let's see what happens.

194
00:18:26,000 --> 00:18:30,000
As a result of developing that skill and that ability,

195
00:18:30,000 --> 00:18:39,000
let's see what happens when you're placed in the situation that is challenging to you.

196
00:18:39,000 --> 00:18:45,000
Does this behavior increase your ability to deal with challenges?

197
00:18:45,000 --> 00:18:51,000
If suddenly the country that you're in becomes a fascist dictatorship,

198
00:18:51,000 --> 00:18:57,000
and they start to torture people and you are being tortured,

199
00:18:57,000 --> 00:19:04,000
how do you react? How do you bear with it?

200
00:19:04,000 --> 00:19:12,000
This story of this auditorium, this stadium,

201
00:19:12,000 --> 00:19:16,000
there was a guy, one of the leaders, he was a musician, I think,

202
00:19:16,000 --> 00:19:23,000
and he was leading people in chanting the anthem,

203
00:19:23,000 --> 00:19:29,000
the national anthem or something, and so they called them down,

204
00:19:29,000 --> 00:19:31,000
and they asked him to singing.

205
00:19:31,000 --> 00:19:36,000
So he started singing, and then they strapped him to this table,

206
00:19:36,000 --> 00:19:39,000
and they smashed his fingers.

207
00:19:39,000 --> 00:19:43,000
He was playing a guitar, they got them to play the guitar,

208
00:19:43,000 --> 00:19:46,000
and then they smashed his fingers, or cut them off his fingers,

209
00:19:46,000 --> 00:19:47,000
or something like that.

210
00:19:47,000 --> 00:19:50,000
Cut off his fingers and then smashed his hands to a bloody pulp.

211
00:19:50,000 --> 00:19:53,000
They did this, this happened, apparently,

212
00:19:53,000 --> 00:19:59,000
and then they said, now you now sing, now play your guitar,

213
00:19:59,000 --> 00:20:04,000
and so he stood up, and he turned to the crowd,

214
00:20:04,000 --> 00:20:08,000
to the other prisoners in the stadium,

215
00:20:08,000 --> 00:20:12,000
and he let them in the anthem or something like that.

216
00:20:12,000 --> 00:20:16,000
It's an interesting story because of how he dealt with the adversity.

217
00:20:16,000 --> 00:20:22,000
Yeah, okay, singing is not such a noble thing in our books,

218
00:20:22,000 --> 00:20:28,000
but the nobility of being able to deal with adversity.

219
00:20:28,000 --> 00:20:35,000
I mean, I'm not saying what is it that leads to the ability to deal with adversity,

220
00:20:35,000 --> 00:20:39,000
but that's the question, what is it that leads to adversity?

221
00:20:39,000 --> 00:20:44,000
If you believe that the skill of being able to drink a lot,

222
00:20:44,000 --> 00:20:49,000
helps you deal with adversity, that's one theory.

223
00:20:49,000 --> 00:20:55,000
But we can't say what the future is going to bring,

224
00:20:55,000 --> 00:20:58,000
and certainly in Cambodia, for example,

225
00:20:58,000 --> 00:21:02,000
before the Pol Pot massacre.

226
00:21:02,000 --> 00:21:04,000
You know, people were living good,

227
00:21:04,000 --> 00:21:07,000
there were probably a lot of people were saying, oh, look, life is good,

228
00:21:07,000 --> 00:21:10,000
everything's great.

229
00:21:10,000 --> 00:21:13,000
Eat drink and be merry.

230
00:21:13,000 --> 00:21:15,000
That doesn't help you.

231
00:21:15,000 --> 00:21:23,000
I mean, I would argue it probably doesn't do much to prepare you for being tortured.

232
00:21:23,000 --> 00:21:26,000
That's an extreme example,

233
00:21:26,000 --> 00:21:28,000
but examples abound.

234
00:21:28,000 --> 00:21:31,000
Suppose you get cancer,

235
00:21:31,000 --> 00:21:38,000
and the ability to enjoy pleasure going to help you with that.

236
00:21:38,000 --> 00:21:44,000
So, if you begin to practice meditation,

237
00:21:44,000 --> 00:21:46,000
you start to see the benefits.

238
00:21:46,000 --> 00:21:49,000
In this way, you can reflect on this as a sort of benefit.

239
00:21:49,000 --> 00:21:50,000
Wow.

240
00:21:50,000 --> 00:21:53,000
I'm really able to deal with things that I wouldn't have been able to deal with before.

241
00:21:53,000 --> 00:21:58,000
That's really something quite obvious in the meditation practice.

242
00:21:58,000 --> 00:22:02,000
You see your reactions,

243
00:22:02,000 --> 00:22:05,000
and you learn how to avoid those,

244
00:22:05,000 --> 00:22:12,000
how to navigate and experience carefully without reacting.

245
00:22:12,000 --> 00:22:19,000
To be careful, to be conscientious.

246
00:22:19,000 --> 00:22:21,000
Anyway, that's a bit of a tangent,

247
00:22:21,000 --> 00:22:24,000
but my point being there,

248
00:22:24,000 --> 00:22:28,000
in many ways, we are a family,

249
00:22:28,000 --> 00:22:32,000
you know, meditators, Buddhists.

250
00:22:32,000 --> 00:22:37,000
In many ways, we are the ones that care for each other.

251
00:22:37,000 --> 00:22:40,000
We are the ones that,

252
00:22:40,000 --> 00:22:42,000
it's each and every one of us,

253
00:22:42,000 --> 00:22:46,000
Anya Manyam, for each other.

254
00:22:46,000 --> 00:22:49,000
Who is a support to each other?

255
00:22:49,000 --> 00:22:53,000
Who is who we would do well to look to for support?

256
00:22:53,000 --> 00:22:56,000
I mean, I can't look to my family so much for support.

257
00:22:56,000 --> 00:22:59,000
I can't even visit with them half the time,

258
00:22:59,000 --> 00:23:03,000
drinking alcohol, or, you know.

259
00:23:03,000 --> 00:23:05,000
But I know when I'm with meditators,

260
00:23:05,000 --> 00:23:08,000
you know, when I was in New York at the monastery,

261
00:23:08,000 --> 00:23:09,000
I knew it was all good.

262
00:23:09,000 --> 00:23:12,000
I knew I was around people who I could trust,

263
00:23:12,000 --> 00:23:22,000
who I could count on.

264
00:23:22,000 --> 00:23:25,000
Who I could relate to.

265
00:23:25,000 --> 00:23:35,000
Who I could sit with, be with, and not feel the impetus to leave.

266
00:23:35,000 --> 00:23:41,000
I'm feeling awkward like everyone around me is drunk.

267
00:23:41,000 --> 00:23:43,000
I'm around me as lost.

268
00:23:43,000 --> 00:23:45,000
It's on a different path.

269
00:23:45,000 --> 00:23:47,000
I'm putting ourselves above anyone,

270
00:23:47,000 --> 00:23:51,000
but on different paths.

271
00:23:51,000 --> 00:23:55,000
So, you know, on that note,

272
00:23:55,000 --> 00:23:59,000
I really like to thank everyone for all of the support.

273
00:23:59,000 --> 00:24:05,000
We have this house, and I'm able to live

274
00:24:05,000 --> 00:24:08,000
because people are supporting me.

275
00:24:08,000 --> 00:24:11,000
That's many of you.

276
00:24:11,000 --> 00:24:13,000
I'd like to thank you.

277
00:24:13,000 --> 00:24:17,000
And also appreciate the meditation,

278
00:24:17,000 --> 00:24:19,000
the amount of meditation,

279
00:24:19,000 --> 00:24:22,000
and the number of people involved with this community

280
00:24:22,000 --> 00:24:24,000
who are practicing meditation.

281
00:24:24,000 --> 00:24:27,000
It's another wonderful support because it means

282
00:24:27,000 --> 00:24:31,000
there's all these people who we can relate to.

283
00:24:31,000 --> 00:24:34,000
We have each other, and we can relate to each other,

284
00:24:34,000 --> 00:24:39,000
and we can talk with each other, and encourage each other,

285
00:24:39,000 --> 00:24:43,000
and push each other further on the path.

286
00:24:43,000 --> 00:24:47,000
Push each other to meditate and to cultivate good things.

287
00:24:47,000 --> 00:24:52,000
So, we care for each other,

288
00:24:52,000 --> 00:24:56,000
and true to this is true family.

289
00:24:56,000 --> 00:24:58,000
We don't depend on our father and mother,

290
00:24:58,000 --> 00:25:01,000
we don't depend on our brothers and sisters.

291
00:25:01,000 --> 00:25:06,000
In spiritual matters, right?

292
00:25:06,000 --> 00:25:09,000
In important matters.

293
00:25:09,000 --> 00:25:11,000
Many ways we can't depend on them

294
00:25:11,000 --> 00:25:14,000
because they just end up leading us down the wrong path

295
00:25:14,000 --> 00:25:19,000
because rather their understanding is,

296
00:25:19,000 --> 00:25:22,000
yes, we would say wrong,

297
00:25:22,000 --> 00:25:27,000
but at the very least, their understanding is different from ours.

298
00:25:27,000 --> 00:25:33,000
And so it would be conflicting for us to depend too much

299
00:25:33,000 --> 00:25:35,000
upon our family, right?

300
00:25:35,000 --> 00:25:37,000
We have a problem, what do we do?

301
00:25:37,000 --> 00:25:40,000
We'll buy a big bag of pot and smoke it all.

302
00:25:40,000 --> 00:25:43,000
That's what they were talking about on this hangout.

303
00:25:43,000 --> 00:25:46,000
They've got to find a way and they're talking about how to find a pot,

304
00:25:46,000 --> 00:25:48,000
you know, or they can get a big bag of...

305
00:25:48,000 --> 00:25:53,000
It's my family, you know?

306
00:25:53,000 --> 00:26:01,000
So, this quote is somewhat apropos to my life.

307
00:26:01,000 --> 00:26:06,000
I mean, in that part of it is.

308
00:26:06,000 --> 00:26:09,000
Is anybody here?

309
00:26:09,000 --> 00:26:11,000
Anybody have any questions?

310
00:26:11,000 --> 00:26:14,000
I'm going to take text questions.

311
00:26:14,000 --> 00:26:18,000
She got 24 viewers on YouTube.

312
00:26:18,000 --> 00:26:20,000
Hello, everyone.

313
00:26:20,000 --> 00:26:22,000
Somebody's watching.

314
00:26:22,000 --> 00:26:30,000
And we got a bunch of people on her meditation page.

315
00:26:30,000 --> 00:26:40,000
We have the usual suspects.

316
00:26:40,000 --> 00:26:51,000
So, Robin, are we going to do some sort of...

317
00:26:51,000 --> 00:26:54,000
Are we going to give people the opportunity?

318
00:26:54,000 --> 00:26:57,000
Not obviously pushing for it, but give people the opportunity

319
00:26:57,000 --> 00:27:02,000
if they want to offer a robe to Agentang?

320
00:27:02,000 --> 00:27:12,000
Like, join us?

321
00:27:12,000 --> 00:27:17,000
Because I think Aurora, I think I can wait.

322
00:27:17,000 --> 00:27:23,000
Someone was interested in offering a robe to Agentang.

323
00:27:23,000 --> 00:27:31,000
And because it's quite easy for us to add sets of robes to our order.

324
00:27:31,000 --> 00:27:35,000
It's just a matter of giving people the opportunity if they want to.

325
00:27:35,000 --> 00:27:37,000
And we're not coercion.

326
00:27:37,000 --> 00:27:40,000
It's not like trying to push people to say this is a good thing.

327
00:27:40,000 --> 00:27:43,000
It's something we really would be keen to do that, I think.

328
00:27:43,000 --> 00:27:46,000
We're going to understand.

329
00:27:46,000 --> 00:27:52,000
I mean, I don't like to put pressure and make people think,

330
00:27:52,000 --> 00:27:55,000
like, oh, that's what we're all about is the sitting donations.

331
00:27:55,000 --> 00:27:57,000
We're not.

332
00:27:57,000 --> 00:28:01,000
But we're about giving the gifts.

333
00:28:01,000 --> 00:28:08,000
If you want to give them with us, you're welcome to give them with us.

334
00:28:08,000 --> 00:28:13,000
Yeah, we could, you know, who knows if people want.

335
00:28:13,000 --> 00:28:20,000
The thing is, there were some people who supported me to get a ticket.

336
00:28:20,000 --> 00:28:22,000
But there was lots of support.

337
00:28:22,000 --> 00:28:26,000
So I thought, well, we can get a gift for Agentang as well.

338
00:28:26,000 --> 00:28:29,000
Because that's important.

339
00:28:29,000 --> 00:28:33,000
And we've thought about what kind of gifts and I thought robes.

340
00:28:33,000 --> 00:28:35,000
Robes are the best.

341
00:28:35,000 --> 00:28:38,000
Because robes symbolize monasticism.

342
00:28:38,000 --> 00:28:41,000
So if you are interested in the idea of becoming a monk,

343
00:28:41,000 --> 00:28:46,000
or if you want to be close to the monastic life,

344
00:28:46,000 --> 00:28:50,000
the ascetic, renunciant life.

345
00:28:50,000 --> 00:28:56,000
Robes are a really good symbol that gift and practical gift as well,

346
00:28:56,000 --> 00:29:04,000
because you're actually helping to close with us the monks.

347
00:29:04,000 --> 00:29:05,000
Awesome.

348
00:29:05,000 --> 00:29:06,000
So there's still time.

349
00:29:06,000 --> 00:29:11,000
We can set up a campaign.

350
00:29:11,000 --> 00:29:13,000
I mean, I make it clear that this, well, I mean,

351
00:29:13,000 --> 00:29:25,000
it's pretty clear that we just have to, we don't want it to sound like we're looking for money or something.

352
00:29:25,000 --> 00:29:27,000
I don't think we should make a big deal out of it.

353
00:29:27,000 --> 00:29:37,000
I don't want them but we had a couple of other crowns funding things.

354
00:29:37,000 --> 00:29:40,000
This is just if there was someone out there we're doing it already

355
00:29:40,000 --> 00:29:43,000
so we can always add a robe.

356
00:29:43,000 --> 00:29:51,000
A set of robes if someone happened to want to do that.

357
00:29:51,000 --> 00:29:52,000
Okay.

358
00:29:52,000 --> 00:30:02,000
Well, let me know how if it can work something out there.

359
00:30:02,000 --> 00:30:03,000
Awesome.

360
00:30:03,000 --> 00:30:04,000
Thank you.

361
00:30:04,000 --> 00:30:12,000
Tomorrow.

362
00:30:12,000 --> 00:30:13,000
Okay.

363
00:30:13,000 --> 00:30:17,000
So we'll talk about how you can come on the hangout tomorrow then.

364
00:30:17,000 --> 00:30:20,000
Talk about it maybe.

365
00:30:20,000 --> 00:30:23,000
Anyway, we'll say good night for tonight then.

366
00:30:23,000 --> 00:30:25,000
No questions.

367
00:30:25,000 --> 00:30:26,000
Have a good night everyone.

368
00:30:26,000 --> 00:30:27,000
Thanks for tuning in.

369
00:30:27,000 --> 00:30:36,000
We should know all the best.

