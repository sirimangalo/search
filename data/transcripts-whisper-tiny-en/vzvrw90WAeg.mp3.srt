1
00:00:00,000 --> 00:00:08,640
Welcome back to Ask a Monk. This is going to be my last Ask a Monk installment before

2
00:00:08,640 --> 00:00:17,680
leaving for Asia for two months. So this wraps it up and thanks everyone for your questions.

3
00:00:17,680 --> 00:00:24,480
And thanks for the feedback and showing that this format works. The point of the whole experiment

4
00:00:24,480 --> 00:00:34,080
wasn't to add something to what I do. I'm already quite busy as it is with my own practice

5
00:00:34,080 --> 00:00:40,720
and with teaching and real life and so on. But the actually the idea was that it would make

6
00:00:40,720 --> 00:00:46,880
my life easier not having to answer the same questions over and over again, but to give hopefully

7
00:00:46,880 --> 00:00:55,440
reasonable, reasonably satisfactory answers to the majority of the questions and at least give people

8
00:00:55,440 --> 00:01:02,560
enough of a broad scope to answer some questions that they might have that they wouldn't have to

9
00:01:03,840 --> 00:01:09,840
have to ask themselves. And I think in that sense it was a great success. It made things

10
00:01:09,840 --> 00:01:14,640
a lot simpler for me and it seems to have reached quite a few people. I've got a lot of

11
00:01:14,640 --> 00:01:21,120
different topics out there and you know hopefully we can keep going. Another thing I'd like to do is

12
00:01:23,440 --> 00:01:31,200
discuss Buddhism on on camera with some of the people around here. So maybe in the new year I'll

13
00:01:31,200 --> 00:01:40,320
be doing a sort of a monk chat, a dialogue talk show kind of thing. So just where I get together

14
00:01:40,320 --> 00:01:47,760
with one of the local Buddhists and we talk about something. But I'll probably be giving talks

15
00:01:47,760 --> 00:01:55,520
and so on when I get back. Looks like I'm coming back in January and I'm going to try to set

16
00:01:55,520 --> 00:02:01,520
something up. The forest thing might not be on the near horizon. We might have to set up something

17
00:02:01,520 --> 00:02:13,040
here in the city for this small city of Moore Park and go from there. Once I can build up more

18
00:02:13,040 --> 00:02:21,280
support then start moving towards the forest. So this is the last one. And the final question

19
00:02:21,280 --> 00:02:30,000
comes from Niddish 9644 who asks, oh venerable, we all know that the Buddha said certain things

20
00:02:30,000 --> 00:02:35,760
are imponderables. What is the significance for this? I find everything else very rational about

21
00:02:35,760 --> 00:02:43,840
Buddhism but this just seems to be escapism. Okay, first of all I'm going to go on a limit

22
00:02:43,840 --> 00:02:50,880
say that most people don't know that the Buddha said certain things are imponderables. That's

23
00:02:50,880 --> 00:02:59,200
actually I would say a fairly esoteric teaching. It's found in one place in the Buddha's teaching.

24
00:02:59,200 --> 00:03:10,080
And it certainly isn't play a prominent role. What plays a much more prominent role is those things

25
00:03:10,080 --> 00:03:17,920
which the Buddha knew or had explanations for but didn't give explanations for. The Buddha said

26
00:03:18,480 --> 00:03:25,440
just like the leaves on the tree are many and the leaves in one's hand that one can fit in one's

27
00:03:25,440 --> 00:03:32,480
hand are few. So are the the teachings that I give to you. What I know is like the forest

28
00:03:32,480 --> 00:03:38,160
where the teachings I give to you is like a handful of leaves. And the reason for that being

29
00:03:39,040 --> 00:03:45,200
it's certainly not escapism. It's for two reasons. It's one for expediency's sake. The Buddha

30
00:03:45,200 --> 00:03:50,480
didn't teach all of his teachings to every person even the things that he did teach

31
00:03:50,480 --> 00:03:56,080
were specific to individuals. There are many things that certain students got another certain

32
00:03:56,080 --> 00:04:01,040
students didn't get because the important point is to give you what's important and what's going

33
00:04:01,040 --> 00:04:08,800
to get you on the right track. Whatever else you learn is really inconsequential in comparison.

34
00:04:09,440 --> 00:04:14,480
And the other reason being that there are certain things when you pursue them even if you can find

35
00:04:14,480 --> 00:04:22,720
answers that they don't lead to happiness and peace and freedom from suffering. Things like

36
00:04:24,560 --> 00:04:31,680
the origin of the world, the big bang and so on. And many worldly things, politics,

37
00:04:33,280 --> 00:04:43,600
the society, even issues which we might, which many high-minded people think are important. The Buddha

38
00:04:43,600 --> 00:04:50,160
didn't focus on because of his deep understanding of reality. For instance, the environment or

39
00:04:52,720 --> 00:05:00,240
animal cruelty or war and torture and so on. The Buddha didn't go into, it wasn't a social

40
00:05:00,240 --> 00:05:05,200
activist trying to stop these things because he was going to the core of things. He's saying that

41
00:05:05,200 --> 00:05:10,160
the reason being suffer, the reason their social injustice, the reason the environment

42
00:05:10,160 --> 00:05:16,000
is being destroyed is because of the greed, the anger, and the delusion that exists in our minds.

43
00:05:16,000 --> 00:05:21,040
We suffer at the hands of others because we have made them suffer at our hands before it's

44
00:05:21,040 --> 00:05:26,560
a cycle. And the only way to break out of it is for someone to stop, for the consciousness

45
00:05:26,560 --> 00:05:33,600
of beings to be lifted up. So those are for the things which are ponderable, which you can think about,

46
00:05:33,600 --> 00:05:40,720
but shouldn't, are going to be of no use to you. There are some teachings of the Buddha which you

47
00:05:40,720 --> 00:05:45,840
don't have to worry about depending on who you are in your position. There are many people who are

48
00:05:45,840 --> 00:05:53,120
not going to learn and are not required and shouldn't think of the necessity of it as being in

49
00:05:53,120 --> 00:05:57,280
necessity to learn these things, even though it is the teachings of the Buddha. Not to mention

50
00:05:57,280 --> 00:06:02,320
those things that the Buddha didn't teach, but new. And the reason he didn't teach them is because

51
00:06:02,320 --> 00:06:07,920
they didn't have any perceivable benefit to beings. When thought about, they lead to more

52
00:06:07,920 --> 00:06:15,760
confusion, lead to more vexation, and lead to more clinging. Further to that, there's this group

53
00:06:15,760 --> 00:06:23,120
of four things that are imponderable. And I think it's really unfair to say that it's escapism for

54
00:06:23,120 --> 00:06:28,240
the Buddha not to talk about certain things, you know, on the one hand the things that he knew,

55
00:06:28,240 --> 00:06:33,440
but not talk about them, but to say that because the Buddha didn't know, he was just saying that

56
00:06:33,440 --> 00:06:38,640
they're imponderable. No, first of all, they're only four of them. That's a pretty profound thing

57
00:06:38,640 --> 00:06:43,600
that in the whole of the universe, there are only four things that you can't think about that

58
00:06:43,600 --> 00:06:49,680
shouldn't be thought about. And second of all, the reason you can't think about them is because

59
00:06:49,680 --> 00:07:02,640
there's no answer. There is no limit. There is no resolution that comes from it. The more you

60
00:07:02,640 --> 00:07:10,160
think about it, the more crazy you become, the more your mind becomes diffuse and unwieldy.

61
00:07:10,160 --> 00:07:18,720
The first one is the limit of the Buddha, the limit of an enlightened being, the limit of

62
00:07:18,720 --> 00:07:23,920
their abilities of their knowledge because it's unlimited. The part of being who is enlightened

63
00:07:23,920 --> 00:07:29,200
was perfectly in tune with reality, can set their mind on anything and can understand any topic

64
00:07:29,200 --> 00:07:34,800
just by thinking about it. It's impossible to understand the limit of, in essence, the limit

65
00:07:34,800 --> 00:07:42,720
of how far you can take your mind, how far you can go with the power of purity of clarity of

66
00:07:42,720 --> 00:07:48,720
mind. And the second one is related to that, the power, the limit of the powers of meditation.

67
00:07:49,360 --> 00:07:56,640
So even for people who are not enlightened, the limit that one can go to in one's meditation

68
00:07:56,640 --> 00:08:04,560
practice, so as far as gaining magical powers, as far as recollecting caste lives, seeing

69
00:08:04,560 --> 00:08:13,600
heaven, seeing hell, levitating and so on, the limit, there is no limit. And to think and to ponder

70
00:08:13,600 --> 00:08:21,360
and even to push the limits in meditation, to be developing oneself further and further,

71
00:08:21,360 --> 00:08:25,600
there's so many books out there that you can read that have all sorts of wacky experiences,

72
00:08:25,600 --> 00:08:32,640
that the number of different experiences that you can have is unlimited. You couldn't ever

73
00:08:32,640 --> 00:08:38,080
come to any framework to say what is possible and what is impossible, because the mind can keep

74
00:08:38,080 --> 00:08:43,760
going and going and going. And the further you go, the more diffuse your mind becomes the less

75
00:08:45,520 --> 00:08:52,400
set and one direction it becomes. And you just end up wandering through some sorrow, really.

76
00:08:53,520 --> 00:09:02,000
The third one is the limit or the workings of karma, of cause and effect, because cause and

77
00:09:02,000 --> 00:09:08,560
effect it has to do with all of reality and to try to understand why you are the way you are,

78
00:09:08,560 --> 00:09:14,800
even why one person is the way they are. Now it takes you further and further out into

79
00:09:15,440 --> 00:09:20,080
all of the other workings of the universe until you have to examine every part of the universe.

80
00:09:21,440 --> 00:09:26,160
And then, according to quantum physics, even that's not enough, right? You can't say

81
00:09:26,160 --> 00:09:33,760
that the world is not deterministic. As you start to investigate, your investigation changes things.

82
00:09:33,760 --> 00:09:39,440
So this is something that is imponderable, at least only to vexation. The Buddha said these things

83
00:09:39,440 --> 00:09:43,840
lead to madness. They lead you to, you drive yourself crazy by thinking about them.

84
00:09:45,280 --> 00:09:55,840
And the fourth one is the nature of the world. Thinking about the creation of the world,

85
00:09:55,840 --> 00:10:03,600
the wide as the world exists, the reasons behind things. And this is probably the most difficult

86
00:10:03,600 --> 00:10:09,760
for Westerners, because we expect answers. These are the profoundly religious questions. But

87
00:10:11,120 --> 00:10:18,960
in essence framed in the wrong way, they are dealing with certain presumptions about the universe,

88
00:10:18,960 --> 00:10:22,320
like the fact that the universe has the beginning, or the fact that time is linear.

89
00:10:22,320 --> 00:10:27,120
When, in fact, what we're dealing with is the present moment, and we're dealing with

90
00:10:29,760 --> 00:10:37,600
a system based on delusion, on ignorance. And so by thinking about the way the world is,

91
00:10:38,080 --> 00:10:45,840
as I said, even you're thinking about it changes your existence. It's this pondering,

92
00:10:45,840 --> 00:10:54,880
this examining, this questioning, which actually sets up a new type of universe. And that's

93
00:10:54,880 --> 00:11:02,160
why physicists live in a three-dimensional or four-dimensional or 11-dimensional space, because they

94
00:11:02,160 --> 00:11:09,520
create this through their experiments and through their questioning. Buddhism is a practical

95
00:11:09,520 --> 00:11:20,640
religion, so it deals with those things that lead to clear and realizable benefits. Those things

96
00:11:20,640 --> 00:11:26,080
which are obvious, they have benefit to us. And if it's a benefit to us, then there's really

97
00:11:26,080 --> 00:11:31,520
no question as to whether it should be developed. And you can see that clearly through your

98
00:11:31,520 --> 00:11:35,680
meditation. There's no one who could come up to you and say, well, you know, that's a benefit to

99
00:11:35,680 --> 00:11:46,160
you in this way, but it's going to hurt you in some other way. It's undeniable that the path

100
00:11:46,160 --> 00:11:51,840
at the Buddha taught, the path of meditation, of developing oneself, and seeing things clearly,

101
00:11:52,480 --> 00:12:00,880
is the right way. It's self-evident. The more you practice, the more you realize

102
00:12:00,880 --> 00:12:07,280
that this practice thing in this way is useful. It's beneficial. It's the right way. It's the way

103
00:12:07,280 --> 00:12:15,040
to become free from suffering. The more you think about these other things in a certain philosophical

104
00:12:15,040 --> 00:12:25,520
question, the more crazy you become. So it's not that the Buddha was trying to avoid certain

105
00:12:25,520 --> 00:12:32,480
subjects because he didn't know them or because he had the wrong answer or the wrong way of

106
00:12:32,480 --> 00:12:38,320
looking at things. It's because what is meant by that is you'll go crazy thinking about these things.

107
00:12:38,320 --> 00:12:43,040
You'll never come to an answer and you'll just go further and further and your mind will become

108
00:12:43,040 --> 00:12:50,320
less and less focused on any one goal. Your mind will not settle. You'll just tangle your mind

109
00:12:50,320 --> 00:12:56,880
that as you look at them in many cases, it's going to change your experience just by examining

110
00:12:56,880 --> 00:13:02,080
just by investigating. So they shouldn't be thought about thinking about them as of no benefit.

111
00:13:03,040 --> 00:13:05,760
In fact, it's a great detriment. That's what is meant by those.

112
00:13:07,120 --> 00:13:14,080
Thanks for the question. I hope that we have a suitable answer. Again, that's all for now.

113
00:13:14,080 --> 00:13:19,840
Thanks everyone for tuning in and for all your questions, comments. Don't forget to

114
00:13:19,840 --> 00:13:25,680
promote these videos if you like them. If you think they're useful, put them on your website,

115
00:13:26,400 --> 00:13:33,600
subscribe, favorite, etc., etc., and most important of all, keep meditating. Thanks a lot.

116
00:13:33,600 --> 00:13:37,840
Thanks for everything and you're welcome to all those people who

117
00:13:37,840 --> 00:13:45,200
have registered. Thank you for me. It's been great. Keep going.

