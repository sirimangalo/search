1
00:00:00,000 --> 00:00:12,160
Okay, good evening everyone broadcasting live November 14th, 2015.

2
00:00:12,160 --> 00:00:23,000
Today I was in Toronto for the inauguration or the promotion ceremony at a Chinese monastery,

3
00:00:23,000 --> 00:00:34,000
the Chinese monastery. They have a new abbot and the abbot is a very good, well, a friend of mine, someone is becoming a very good friend.

4
00:00:34,000 --> 00:00:48,000
We're good friends at this point, so he invited a bunch of monks and I was one of them and it was quite a ceremony.

5
00:00:48,000 --> 00:01:00,000
They have quite intricate ceremonies in that tradition, but it was quite well done.

6
00:01:00,000 --> 00:01:07,000
And then they gave us lunch, which wasn't nearly as well organized as the actual ceremony,

7
00:01:07,000 --> 00:01:13,000
but I think they were so busy trying to make perfect ceremony that.

8
00:01:13,000 --> 00:01:18,000
It was all good, it was all good.

9
00:01:18,000 --> 00:01:31,000
And then rushed back just in time to teach, to lead this or to help with this presentation,

10
00:01:31,000 --> 00:01:42,000
to the residence life council, which is all these people who look after the first years in residence.

11
00:01:42,000 --> 00:01:50,000
And people came from all over Canada actually, from various universities.

12
00:01:50,000 --> 00:02:01,000
So these are undergrad university students who work for their universities to support new students.

13
00:02:01,000 --> 00:02:15,000
And so we had a 50-minute presentation. I talked for about 25 minutes.

14
00:02:15,000 --> 00:02:21,000
And it was good. They went really well. They were quite appreciative.

15
00:02:21,000 --> 00:02:29,000
A few people actually came up to me afterwards and in the hallway and took my booklet and took my card.

16
00:02:29,000 --> 00:02:33,000
They're interested in getting in learning more.

17
00:02:33,000 --> 00:02:40,000
One man from Dubai actually is, I guess, an immigrant, I mean.

18
00:02:40,000 --> 00:02:50,000
And he was saying in Dubai, it's very, very consumeristic materialist.

19
00:02:50,000 --> 00:02:59,000
And so he just wants to get away from that lifestyle and wants to simplify his life, give up his possessions and so on.

20
00:02:59,000 --> 00:03:07,000
We had a good talk.

21
00:03:07,000 --> 00:03:16,000
That's about it. That's what happened in the Dhamma today.

22
00:03:16,000 --> 00:03:21,000
Anything new and exciting with you, Robin?

23
00:03:21,000 --> 00:03:25,000
No, I watched a lot of the news out of Paris today.

24
00:03:25,000 --> 00:03:32,000
Oh, you're right. I heard last night we were having a bonfire.

25
00:03:32,000 --> 00:03:35,000
Last night we had a bonfire. That's why I wasn't here.

26
00:03:35,000 --> 00:03:41,000
And right, there was something in Paris, what happened?

27
00:03:41,000 --> 00:03:52,000
I think seven or eight coordinated ISIS terrorists who three blew themselves up and others shot people.

28
00:03:52,000 --> 00:03:54,000
They themselves up and shot people.

29
00:03:54,000 --> 00:03:57,000
Yeah, 129 people did.

30
00:03:57,000 --> 00:04:05,000
So, like over 500 people involved in the art guild, one way or another.

31
00:04:05,000 --> 00:04:10,000
So it's very sad.

32
00:04:10,000 --> 00:04:29,000
And it's very sad.

33
00:04:29,000 --> 00:04:33,000
There's a line and a song.

34
00:04:33,000 --> 00:04:49,000
It's a good line.

35
00:04:49,000 --> 00:04:56,000
It's an interesting song. It's acapella.

36
00:04:56,000 --> 00:05:03,000
I did a lot of acapella songs. And it starts off. It's about the war and the gulf, actually.

37
00:05:03,000 --> 00:05:06,000
So it's not taking sides, I don't think.

38
00:05:06,000 --> 00:05:13,000
But it says, we got a call to write a song about the war and the gulf, but we couldn't hurt anyone's feelings.

39
00:05:13,000 --> 00:05:17,000
So we tried, then gave up, because there was no such song.

40
00:05:17,000 --> 00:05:20,000
But the trying was very revealing.

41
00:05:20,000 --> 00:05:31,000
What makes a person's or poisonous righteous that they'll send hell to anyone, which is just a breeze?

42
00:05:31,000 --> 00:05:37,000
I don't know about the war and the gulf. I'm not quite sure what they're referring to, but I get the sentiment.

43
00:05:37,000 --> 00:05:43,000
It's a really good question. I mean, the answer is not.

44
00:05:43,000 --> 00:05:49,000
Like it's hard to understand. It's just amazing now.

45
00:05:49,000 --> 00:05:56,000
It's sad. That's the saddest part that people will be so poisonous righteous.

46
00:05:56,000 --> 00:06:04,000
Of course. And then, of course, the response is, you know, so many people are saying, well, we have to go and, you know, and kill these people.

47
00:06:04,000 --> 00:06:18,000
But of course, that's only going to, you know, that's only going to just perpetuate this over and over.

48
00:06:18,000 --> 00:06:32,000
But I think the point is you have to turn to the people who are rational.

49
00:06:32,000 --> 00:06:39,000
And I don't think that's the people who are going to respond with violence.

50
00:06:39,000 --> 00:06:48,000
I mean, I mean, to some extent, you have to understand that the people on the other side are complicit and responsible.

51
00:06:48,000 --> 00:06:54,000
And there's a lot of, I think, interest in war and fear and that kind of thing.

52
00:06:54,000 --> 00:06:58,000
It sells in the people who the arms industry.

53
00:06:58,000 --> 00:07:08,000
Politicians tend to like it because it makes people make people vote for them.

54
00:07:08,000 --> 00:07:18,000
It's a good way of controlling people, controlling through fear.

55
00:07:18,000 --> 00:07:29,000
So you have to be clear of a risk. And I think we have to, we can't take sides.

56
00:07:29,000 --> 00:07:41,000
We take sides ideologically. We should take an ideological side in a sense that violence is wrong.

57
00:07:41,000 --> 00:07:49,000
And stealing fear in people is wrong, bigotry is wrong, arrogance, righteousness, all of these things are wrong.

58
00:07:49,000 --> 00:07:54,000
Well, not righteousness, but this arrogance.

59
00:07:54,000 --> 00:07:59,000
Righteousness actually is good, but it has to be truly righteous.

60
00:07:59,000 --> 00:08:08,000
There's the idea being self-righteous, which is the sense of, you're right because you think it.

61
00:08:08,000 --> 00:08:10,000
Well, that's the only reason why.

62
00:08:10,000 --> 00:08:13,000
It's right because I think I'm right.

63
00:08:13,000 --> 00:08:16,000
You can't be right.

64
00:08:16,000 --> 00:08:19,000
It's the ideas you have are at the right or wrong.

65
00:08:19,000 --> 00:08:23,000
The rightness exists and the ideas intrinsically.

66
00:08:23,000 --> 00:08:28,000
It's nothing to do with you being right.

67
00:08:28,000 --> 00:08:42,000
Well, I mean, there is that, but focusing on that is the problem when you feel conceded because you're right.

68
00:08:42,000 --> 00:08:52,000
I guess we also should talk about not being afraid of death because terrorism relies on people being afraid.

69
00:08:52,000 --> 00:08:59,000
I mean, killing a hundred and some people is not going to change the world, right?

70
00:08:59,000 --> 00:09:04,000
But it will if everyone gets very afraid of it.

71
00:09:04,000 --> 00:09:14,000
So, I mean, I don't know whether that sounds kind of cruel, but my point being, this doesn't a war make.

72
00:09:14,000 --> 00:09:24,000
It's a horrible thing that a hundred and some people died. I mean, that's just really horrific, but I mean, tsunami was far more horrific.

73
00:09:24,000 --> 00:09:32,000
That tsunami devastated Asia, right? It was far worse.

74
00:09:32,000 --> 00:09:40,000
Yeah, the president of France called it an act of war, and the Pope said it was the start of World War III.

75
00:09:40,000 --> 00:09:43,000
Uh-huh.

76
00:09:43,000 --> 00:09:52,000
It may very well be. I mean, that could be what's happening because, I mean, the point is, we don't have bad guys on one side and good guys on another.

77
00:09:52,000 --> 00:10:02,000
The bad guys on the other on our side are going to push for war, I suppose.

78
00:10:02,000 --> 00:10:07,000
I suppose there's probably a general sense of not wanting to go to war.

79
00:10:07,000 --> 00:10:12,000
I don't know how it is now as of yesterday. That's a big deal, right?

80
00:10:12,000 --> 00:10:17,000
Yeah.

81
00:10:17,000 --> 00:10:27,000
You know, and not so long ago, I mean, in the United States, you know, people were burnt out of war, but probably within the last week with, uh,

82
00:10:27,000 --> 00:10:40,000
ISIS had bomb-direction plane, and now this within one week's time, it brightens people, changes things.

83
00:10:40,000 --> 00:10:45,000
Yeah.

84
00:10:45,000 --> 00:10:54,000
It's not good for Bernie Sanders.

85
00:10:54,000 --> 00:10:58,000
No, it's not good at all for Bernie Sanders before Bernie.

86
00:10:58,000 --> 00:11:03,000
You're having a democratic debate tonight, and it was supposed to be an economic debate.

87
00:11:03,000 --> 00:11:14,000
And in that, Bernie would have, you know, really shown that's really his thing, and they switched gears and they're making the focus, uh, foreign relations due to the sense of it.

88
00:11:14,000 --> 00:11:22,000
And, um, they said on the news, one of his, one of his aides flipped out or something because he didn't want that.

89
00:11:22,000 --> 00:11:26,000
I'm not sure if it was exactly what it was, but they portrayed him very poorly.

90
00:11:26,000 --> 00:11:30,000
They said his aide flipped out about the change in questioning or something.

91
00:11:30,000 --> 00:11:35,000
So, yeah.

92
00:11:35,000 --> 00:11:39,000
Probably won't be as best night.

93
00:11:39,000 --> 00:11:44,000
I guess I mean, uh, the point is that he's, he's very anti-war.

94
00:11:44,000 --> 00:11:46,000
He said he's a good guy.

95
00:11:46,000 --> 00:11:48,000
He seems to be one of us.

96
00:11:48,000 --> 00:11:49,000
Oh, yes, definitely.

97
00:11:49,000 --> 00:11:54,000
So, so now that people are starting to want to go to war, that's not good for,

98
00:11:54,000 --> 00:11:56,000
yes, good for the peace movement.

99
00:11:56,000 --> 00:12:03,000
Yes, and people wanting to talk about, you know, this says the focus up tonight's debate that, that really.

100
00:12:03,000 --> 00:12:05,000
That's true.

101
00:12:05,000 --> 00:12:13,000
The amount most of it wanted to say is the conspiracy against him.

102
00:12:13,000 --> 00:12:17,000
Mm-hmm.

103
00:12:17,000 --> 00:12:19,000
Oh, wow.

104
00:12:19,000 --> 00:12:21,000
We're all gonna die anyway.

105
00:12:21,000 --> 00:12:27,000
The earth is slowly but surely moving closer to the sun or is the sun gonna explode?

106
00:12:27,000 --> 00:12:28,000
I can't remember what it is.

107
00:12:28,000 --> 00:12:31,000
I think the sun is gonna explode first.

108
00:12:31,000 --> 00:12:36,000
And burn us all to a crisp.

109
00:12:36,000 --> 00:12:38,000
Oh, obviously we won't be here.

110
00:12:38,000 --> 00:12:43,000
But if you are, well, you'll be burned to a crisp.

111
00:12:43,000 --> 00:12:49,000
Actually, I think what will happen is all the, all the water will, will evaporate first, right?

112
00:12:49,000 --> 00:12:52,000
So you'll die of thirst.

113
00:12:52,000 --> 00:12:56,000
I don't know what you'll die of, you'll die of them.

114
00:12:56,000 --> 00:13:04,000
There's a good jotica quote.

115
00:13:04,000 --> 00:13:12,000
The...

116
00:13:12,000 --> 00:13:20,000
Something, something, the...

117
00:13:20,000 --> 00:13:25,000
Outcast and the well-born, the fool and Ick the wives.

118
00:13:25,000 --> 00:13:32,000
For Richard, poor, one end is sure each man among them dies.

119
00:13:32,000 --> 00:13:46,000
I'm very true.

120
00:13:46,000 --> 00:13:49,000
Do we have any questions?

121
00:13:49,000 --> 00:13:54,000
Thank you, Jim.

122
00:13:54,000 --> 00:14:05,000
Venerable Bante in Satipatanasuta, what is the meaning of observing the body internally, externally, and both internally and externally?

123
00:14:05,000 --> 00:14:14,000
I understand observing body internally, but not very clear on what is observing body externally means.

124
00:14:14,000 --> 00:14:25,000
Well, there's two senses in which you could be understood.

125
00:14:25,000 --> 00:14:28,000
The internal and external sense spaces.

126
00:14:28,000 --> 00:14:31,000
So there are...

127
00:14:31,000 --> 00:14:35,000
The eye is internal and sights are external.

128
00:14:35,000 --> 00:14:45,000
So if you say, if you say light, light, then you're focusing on the external aspect.

129
00:14:45,000 --> 00:14:51,000
If you say eye, I mean, you wouldn't really say these things, but if you focus on...

130
00:14:51,000 --> 00:14:56,000
When you say seeing, if you're aware of the eye seeing, then that's internal.

131
00:14:56,000 --> 00:15:00,000
If you're aware of what you see, the light, that's external.

132
00:15:00,000 --> 00:15:06,000
It just means there's two aspects, or when you hear there is the...

133
00:15:06,000 --> 00:15:10,000
If you're aware of the ear hearing, that's the internal.

134
00:15:10,000 --> 00:15:14,000
If you're aware of the sound, it's just your focus.

135
00:15:14,000 --> 00:15:15,000
It can be both.

136
00:15:15,000 --> 00:15:18,000
It can be one or the other, it can be both.

137
00:15:18,000 --> 00:15:22,000
That's one way of interpreting, but the other one is if you look at the Satipatanasuta,

138
00:15:22,000 --> 00:15:26,000
there are certain sections that are external.

139
00:15:26,000 --> 00:15:33,000
Not many of them, but there's the section on the cemetery front of the place.

140
00:15:33,000 --> 00:15:39,000
So that's mindfulness of someone else's body, mindfulness of an external body.

141
00:15:39,000 --> 00:15:45,000
And so that word internally and externally are both internally and externally.

142
00:15:45,000 --> 00:15:49,000
That phrase occurs at the end of each section.

143
00:15:49,000 --> 00:15:53,000
But this kind of happens in the polity where they just repeat it.

144
00:15:53,000 --> 00:15:59,000
It's just repeated at the end of each section, even though it only applies to certain sections.

145
00:15:59,000 --> 00:16:07,000
So it may very well be that all that means is in this Satipat, there are certain sections that are external.

146
00:16:07,000 --> 00:16:09,000
And certain that are internal.

147
00:16:09,000 --> 00:16:22,000
Of course, most of them are internal, but you could think of ways in which you could be aware of external feelings

148
00:16:22,000 --> 00:16:25,000
and thoughts and emotions.

149
00:16:25,000 --> 00:16:31,000
If you're aware of someone else who watch them getting angry, that's kind of a mindfulness, or it could be.

150
00:16:31,000 --> 00:16:33,000
But it's conceptual.

151
00:16:33,000 --> 00:16:34,000
That's not Vipassana.

152
00:16:34,000 --> 00:16:38,000
That won't work for Vipassana, but it would work in certain instances for someone.

153
00:16:38,000 --> 00:16:43,000
Or it works for a general mental development in terms of seeing how anger affects other people.

154
00:16:43,000 --> 00:16:46,000
For example, seeing how greed affects other people.

155
00:16:46,000 --> 00:16:55,000
It's something you can be mindful of, theoretically, or conceptually mindful of, and can help you in your spiritual development.

156
00:16:55,000 --> 00:17:01,000
It's not Vipassana, but the Satipatana Sutta isn't just Vipassana.

157
00:17:01,000 --> 00:17:08,000
About movement, walking, sitting, is that internal or external?

158
00:17:08,000 --> 00:17:10,000
That's the internal.

159
00:17:10,000 --> 00:17:20,000
The body is internal, but external is the, so when you touch the floor, the floor aspect, the hardness aspect comes from the floor.

160
00:17:20,000 --> 00:17:26,000
The softness from the foot is internal, but the hardness from the floor is external.

161
00:17:26,000 --> 00:17:30,000
When your teeth go together, they're both hard, that's both internal.

162
00:17:30,000 --> 00:17:36,000
But if you put a spoon in your mouth and you bite on that, or food, you put food in your mouth and you bite on that.

163
00:17:36,000 --> 00:17:38,000
The food is external.

164
00:17:38,000 --> 00:17:47,000
I mean, it gets blurred there because the food becomes internal, but it might be considered internal at that point.

165
00:17:47,000 --> 00:17:54,000
Thank you.

166
00:17:54,000 --> 00:18:09,000
I'll comment that we might cross Entromita and the Milky Way will collapse with it before the sun turns Nova in a matter of billions of years.

167
00:18:09,000 --> 00:18:10,000
Perhaps.

168
00:18:10,000 --> 00:18:12,000
Okay.

169
00:18:12,000 --> 00:18:15,000
Put that on my calendar.

170
00:18:15,000 --> 00:18:23,000
One of those perpetual calendars for billions of years from now.

171
00:18:23,000 --> 00:18:38,000
I'm not sure if this is a question for you, but they are just the general discussion about the sun and all, but when that happens, I wonder if people will be reborn in the nearest M class planet, wherever that is.

172
00:18:38,000 --> 00:18:40,000
Wondering, wondering.

173
00:18:40,000 --> 00:18:44,000
Yes, wondering what an M class planet is.

174
00:18:44,000 --> 00:18:50,000
Wondering, wondering, wondering.

175
00:18:50,000 --> 00:18:53,000
And someone asking will you be answering questions on here?

176
00:18:53,000 --> 00:18:54,000
Yes.

177
00:18:54,000 --> 00:18:56,000
Submit your question.

178
00:18:56,000 --> 00:18:59,000
I wonder if they're not getting the audio or video.

179
00:18:59,000 --> 00:19:01,000
Maybe you can post a link to the YouTube.

180
00:19:01,000 --> 00:19:02,000
Sure.

181
00:19:02,000 --> 00:19:06,000
Do you have the link or only I do, right?

182
00:19:06,000 --> 00:19:09,000
I can get it through YouTube.

183
00:19:09,000 --> 00:19:13,000
Well, I got it here.

184
00:19:13,000 --> 00:19:20,000
Copy.

185
00:19:20,000 --> 00:19:23,000
The YouTube is like, what, 30 seconds behind or something?

186
00:19:23,000 --> 00:19:47,000
Yes.

187
00:19:47,000 --> 00:19:50,000
So how was the bug fire?

188
00:19:50,000 --> 00:19:54,000
It was good. No one came, but it was good.

189
00:19:54,000 --> 00:19:57,000
We had two new people come, and they left early.

190
00:19:57,000 --> 00:20:04,000
We started talking about some fairly theoretical stuff, and then they just got up and left.

191
00:20:04,000 --> 00:20:05,000
That's it.

192
00:20:05,000 --> 00:20:06,000
Goodbye.

193
00:20:06,000 --> 00:20:07,000
We wonder, oh, did we scare them away?

194
00:20:07,000 --> 00:20:09,000
I don't know.

195
00:20:09,000 --> 00:20:17,000
Well, we stayed for two hours.

196
00:20:17,000 --> 00:20:20,000
Yeah, it's an interesting group.

197
00:20:20,000 --> 00:20:23,000
They're not all meditators.

198
00:20:23,000 --> 00:20:28,000
Most of them are interested in meditation.

199
00:20:28,000 --> 00:20:33,000
But we had about six or eight people.

200
00:20:33,000 --> 00:20:34,000
That's good.

201
00:20:34,000 --> 00:20:38,000
It sounds nice.

202
00:20:38,000 --> 00:20:45,000
We did talk about the idea of doing public meditations in the student

203
00:20:45,000 --> 00:20:52,000
center. There's a big open area where there's a huge crowds of people walking through,

204
00:20:52,000 --> 00:20:57,000
and we could just sit down and do meditation in the public space there.

205
00:20:57,000 --> 00:21:06,000
We could even just use it to teach meditation like I could teach people as they came in.

206
00:21:06,000 --> 00:21:12,000
And then if we got a group going, we could start doing flash moms, meditation, flash one,

207
00:21:12,000 --> 00:21:16,000
and where did you ever see that video we did in Winnipeg?

208
00:21:16,000 --> 00:21:17,000
We did.

209
00:21:17,000 --> 00:21:18,000
Yeah.

210
00:21:18,000 --> 00:21:23,000
I walked in sat down, and 30 seconds later a second person came and sat down,

211
00:21:23,000 --> 00:21:26,000
and 30 seconds we added a person every 30 seconds.

212
00:21:26,000 --> 00:21:31,000
It was only about 20 of us, but it was fun.

213
00:21:31,000 --> 00:21:32,000
Yeah.

214
00:21:32,000 --> 00:21:37,000
It definitely sounds a little more comfortable in the student center than outdoors this time of year.

215
00:21:37,000 --> 00:21:41,000
Yeah.

216
00:21:41,000 --> 00:21:42,000
So maybe we'll try that.

217
00:21:42,000 --> 00:21:47,000
It's just a matter of scheduling.

218
00:21:47,000 --> 00:21:57,000
Darwin explained M-class planet is life supporting planet.

219
00:21:57,000 --> 00:22:00,000
Thank you, Darwin.

220
00:22:00,000 --> 00:22:07,000
I hadn't heard that term before.

221
00:22:07,000 --> 00:22:13,000
What is a good strategy when trying to get people to meditate?

222
00:22:13,000 --> 00:22:16,000
Friends and family.

223
00:22:16,000 --> 00:22:19,000
I acknowledge your desire for other people to meditate.

224
00:22:19,000 --> 00:22:23,000
That's important.

225
00:22:23,000 --> 00:22:26,000
Because I can't think of a good strategy.

226
00:22:26,000 --> 00:22:29,000
It's not about getting people to meditate.

227
00:22:29,000 --> 00:22:30,000
Meditation's hard.

228
00:22:30,000 --> 00:22:36,000
If you don't really want to do it, it's not really likely to succeed.

229
00:22:36,000 --> 00:22:44,000
Maybe that's pessimistic, but it can't help but be a bit pessimistic or skeptical.

230
00:22:44,000 --> 00:22:49,000
Because you can get people to do a little bit of meditation, but you find you're pushing them in its effort,

231
00:22:49,000 --> 00:22:51,000
and then they just give it up anyway.

232
00:22:51,000 --> 00:22:53,000
A person wants to do it.

233
00:22:53,000 --> 00:22:55,000
It's something that you won't need to push them.

234
00:22:55,000 --> 00:23:03,000
I think a good strategy is to talk about it, but not have any emotional investedness.

235
00:23:03,000 --> 00:23:07,000
Don't let it get to you emotionally.

236
00:23:07,000 --> 00:23:09,000
I want them to meditate.

237
00:23:09,000 --> 00:23:11,000
Just do it as a matter of course.

238
00:23:11,000 --> 00:23:13,000
Of course, you're going to tell people to meditate.

239
00:23:13,000 --> 00:23:15,000
Of course, you're going to talk about it.

240
00:23:15,000 --> 00:23:17,000
Don't be afraid to talk about it.

241
00:23:23,000 --> 00:23:31,000
It's so hard when you see someone who is struggling and you know how much it would help them.

242
00:23:31,000 --> 00:23:36,000
That's the thing is for many people that wouldn't help them.

243
00:23:36,000 --> 00:23:39,000
They'd try to practice and they'd fail.

244
00:23:39,000 --> 00:23:43,000
When they'd get discouraged.

245
00:23:43,000 --> 00:23:47,000
Yeah, it would help people if they suddenly had no more defilements,

246
00:23:47,000 --> 00:23:51,000
but it's getting from here to there.

247
00:23:51,000 --> 00:23:54,000
For most people, it's just too far.

248
00:23:54,000 --> 00:23:58,000
So you have to be ready for it in other words?

249
00:23:58,000 --> 00:24:02,000
Yeah, when you have to have some potentiality,

250
00:24:02,000 --> 00:24:13,000
we don't have time or resources to bring people from zero to a hundred.

251
00:24:13,000 --> 00:24:16,000
It's not really an invest interest to do so.

252
00:24:16,000 --> 00:24:20,000
You're better off taking people who are really ready because they'll turn around

253
00:24:20,000 --> 00:24:25,000
and help the people who are less ready and less ready and so on.

254
00:24:25,000 --> 00:24:28,000
It'll work like that.

255
00:24:28,000 --> 00:24:34,000
That makes sense.

256
00:24:34,000 --> 00:24:38,000
How can one truly become enlightened if no matter how much he meditates,

257
00:24:38,000 --> 00:24:41,000
he will still require food?

258
00:24:41,000 --> 00:24:44,000
Is this a person who's done some meditation with us?

259
00:24:44,000 --> 00:24:48,000
I think this is a new person.

260
00:24:48,000 --> 00:24:51,000
Well, I'm going to ask that.

261
00:24:51,000 --> 00:24:55,000
I'm going to skip this question and say,

262
00:24:55,000 --> 00:24:59,000
you have to start meditating with us if you want me to answer your questions.

263
00:24:59,000 --> 00:25:06,000
It's not a hard and fast for all, but I want you to be meditating before you answer,

264
00:25:06,000 --> 00:25:11,000
before you ask this question.

265
00:25:11,000 --> 00:25:15,000
Maybe you have a started meditating, but you should read my booklet if you haven't.

266
00:25:15,000 --> 00:25:23,000
And if you have, or once you have, and start meditating, and come back when you're green,

267
00:25:23,000 --> 00:25:32,000
because your question thing is yellow or orange, which means you haven't meditated.

268
00:25:32,000 --> 00:25:33,000
Why?

269
00:25:33,000 --> 00:25:36,000
Because the question is a little bit neat.

270
00:25:36,000 --> 00:25:45,000
It has to have a nuanced answer, and it's a question that may be not coming from lack of information,

271
00:25:45,000 --> 00:25:47,000
or lack of understanding about meditation.

272
00:25:47,000 --> 00:25:53,000
So I want to be sure that you understand meditation before you ask that if you want to ask.

273
00:25:53,000 --> 00:26:03,000
Maybe you'll meditate, and you'll say, oh, I don't need to ask that.

274
00:26:03,000 --> 00:26:10,000
I think that's why people have been meditating for a while to ask fewer questions.

275
00:26:10,000 --> 00:26:12,000
Yeah.

276
00:26:12,000 --> 00:26:16,000
Yeah, I mean, in the meditation course, that happens.

277
00:26:16,000 --> 00:26:21,000
The beginner meditators, you have a lot to tell them to explain, and you have to guide them.

278
00:26:21,000 --> 00:26:25,000
But as they get on in the course, it's really less and less for you to do,

279
00:26:25,000 --> 00:26:48,000
except just confirm that they're doing OK and giving the new exercise.

280
00:26:48,000 --> 00:26:56,000
One asked me, and this man from Dubai asked me, he asked a question in the group,

281
00:26:56,000 --> 00:27:01,000
whether it's OK to listen to music when you meditate.

282
00:27:01,000 --> 00:27:05,000
And so I explain, so it depends what you mean by meditation, but in our meditation,

283
00:27:05,000 --> 00:27:07,000
we're trying to become objective.

284
00:27:07,000 --> 00:27:10,000
So everything is music.

285
00:27:10,000 --> 00:27:19,000
And the point is that if you are partial towards the music, and the music makes it easier for you to focus.

286
00:27:19,000 --> 00:27:22,000
Actually, he asks us, in the beginning, I just said it like that.

287
00:27:22,000 --> 00:27:26,000
But then he came up later and he said, well, the music is like nature, music, waterfalls,

288
00:27:26,000 --> 00:27:27,000
and that kind of thing.

289
00:27:27,000 --> 00:27:32,000
And I said, well, it makes it easier for you to focus than your eats of crunch for you.

290
00:27:32,000 --> 00:27:34,000
And we're not interested in that.

291
00:27:34,000 --> 00:27:39,000
We're interested in how you react when it's difficult.

292
00:27:39,000 --> 00:27:48,000
And so we could make it very, very easy to meditate.

293
00:27:48,000 --> 00:27:56,000
One meditator here recently, he was, I mean, always meditators will try to find a trick or a way to make the meditation easier.

294
00:27:56,000 --> 00:27:59,000
And I said, do I can make it really easy for you?

295
00:27:59,000 --> 00:28:00,000
And he said, yeah, you can.

296
00:28:00,000 --> 00:28:05,000
And he said, yeah, you do 10 minutes of lying meditation that day, and that's your meditation.

297
00:28:05,000 --> 00:28:06,000
Very easy.

298
00:28:06,000 --> 00:28:09,000
But what do you get from it?

299
00:28:09,000 --> 00:28:11,000
You get nothing.

300
00:28:11,000 --> 00:28:13,000
We're trying to make it more difficult.

301
00:28:13,000 --> 00:28:15,000
We're trying to challenge ourselves.

302
00:28:15,000 --> 00:28:20,000
We're trying to see what happens, and we're trying to learn how we react when the going gets done,

303
00:28:20,000 --> 00:28:22,000
so that we can change the way we react.

304
00:28:22,000 --> 00:28:30,000
If you don't have things that trigger you, then you'll never learn how to overcome the trigger.

305
00:28:30,000 --> 00:28:41,000
So making it easy to meditate is not useful.

306
00:28:41,000 --> 00:28:44,000
It's great that someone from Dubai is interested in meditation.

307
00:28:44,000 --> 00:28:51,000
And everything is so flashy, and so many spectacular things there.

308
00:28:51,000 --> 00:28:59,000
So just great that somebody is interested in something that doesn't impress by that.

309
00:28:59,000 --> 00:29:04,000
Hmm.

310
00:29:04,000 --> 00:29:19,000
I find it easier to note experiences during walking meditation, during walking meditation quietly out loud.

311
00:29:19,000 --> 00:29:22,000
Is this okay? Thanks.

312
00:29:22,000 --> 00:29:26,000
Yeah, again, you find it easier than that's something that you shouldn't do.

313
00:29:26,000 --> 00:29:29,000
We're not trying to make it easy.

314
00:29:29,000 --> 00:29:35,000
We're trying to get stronger.

315
00:29:35,000 --> 00:29:43,000
We'll never get stronger if you just make it easier on yourself.

316
00:29:43,000 --> 00:29:54,000
It's okay, but it's less useful.

317
00:29:54,000 --> 00:29:58,000
It's also distracting because then your mind isn't with the object.

318
00:29:58,000 --> 00:29:59,000
It's with your mouth.

319
00:29:59,000 --> 00:30:01,000
It's like a mouth.

320
00:30:01,000 --> 00:30:07,000
It's a big stop trying to walk and talk at the same time your mind is doing two things.

321
00:30:07,000 --> 00:30:11,000
It's the way to get more distracted.

322
00:30:11,000 --> 00:30:12,000
You want to make it easy.

323
00:30:12,000 --> 00:30:14,000
Stop doing walking meditation.

324
00:30:14,000 --> 00:30:15,000
Just start dancing.

325
00:30:15,000 --> 00:30:17,000
Turn on some music and start dancing.

326
00:30:17,000 --> 00:30:20,000
That'll be easy.

327
00:30:20,000 --> 00:30:24,000
And shout out the lyrics to the song.

328
00:30:24,000 --> 00:30:27,000
It's not about making it easy.

329
00:30:27,000 --> 00:30:32,000
It's actually more about making it fairly difficult.

330
00:30:32,000 --> 00:30:37,000
I mean, not to the extent that you should punch yourself to understand pain or something.

331
00:30:37,000 --> 00:30:45,000
But difficult in the sense of patience, requiring patience and focus.

332
00:30:45,000 --> 00:30:57,000
But if something is so distracting you that it won't leave you, then don't recommend to stand and note that for a little bit.

333
00:30:57,000 --> 00:30:58,000
Yeah.

334
00:30:58,000 --> 00:31:07,000
I mean, that's only because again, walking and noting something else are too diverse, better to be focused on one thing.

335
00:31:07,000 --> 00:31:22,000
So, stopping makes it.

336
00:31:22,000 --> 00:31:38,000
I think we're all caught up on day.

337
00:31:38,000 --> 00:31:40,000
Okay.

338
00:31:40,000 --> 00:31:43,000
Well, then, good night, everyone.

339
00:31:43,000 --> 00:31:44,000
Thank you, Valentine.

340
00:31:44,000 --> 00:31:45,000
Turning in.

341
00:31:45,000 --> 00:31:48,000
Thanks, Robin, for your help.

342
00:31:48,000 --> 00:31:49,000
Thank you.

343
00:31:49,000 --> 00:31:52,000
Thank you.

