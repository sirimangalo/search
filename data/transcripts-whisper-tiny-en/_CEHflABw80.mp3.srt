1
00:00:00,000 --> 00:00:05,000
Hey, so here I am in Mississippi of Chiang Mai.

2
00:00:05,000 --> 00:00:14,000
Under the Great, what locally is the city of Wat-Nokloli?

3
00:00:14,000 --> 00:00:21,000
And here's the wonderful traffic.

4
00:00:21,000 --> 00:00:25,000
And you can see in the distance there.

5
00:00:25,000 --> 00:00:31,000
And here's the remade, chanting hall.

6
00:00:31,000 --> 00:00:34,000
Made in the inter-traditional move back.

7
00:00:34,000 --> 00:00:39,000
You can see, you can hear music, Wat-Nokloli music.

8
00:00:39,000 --> 00:00:43,000
Anyway, so here I am in Chiang Mai.

9
00:00:43,000 --> 00:00:46,000
Getting ready to make my trip back to America.

10
00:00:46,000 --> 00:00:49,000
Just thought I'd say hi that I'm doing well.

11
00:00:49,000 --> 00:00:53,000
I'm walking through the crowded city streets on Armstrong

12
00:00:53,000 --> 00:00:55,000
around every morning.

13
00:00:55,000 --> 00:00:59,000
And I mean, to understand a lot about what it means

14
00:00:59,000 --> 00:01:03,000
to be a month, what it means to be a meditator.

15
00:01:03,000 --> 00:01:07,000
And it doesn't have so much to do with living off in the forest

16
00:01:07,000 --> 00:01:12,000
as it does to living in the present moment, living in the here and now.

17
00:01:12,000 --> 00:01:17,000
And coming to understand the world inside,

18
00:01:17,000 --> 00:01:20,000
I'm not so concerned with the nature of the world outside,

19
00:01:20,000 --> 00:01:25,000
which we can all verify the outside world is terribly

20
00:01:25,000 --> 00:01:30,000
impermanent, changing all the time, completely out of our control,

21
00:01:30,000 --> 00:01:34,000
and not something which can satisfy it no matter which way it goes.

22
00:01:34,000 --> 00:01:36,000
Why? Because it's not permanent.

23
00:01:36,000 --> 00:01:38,000
And we've changed it.

24
00:01:38,000 --> 00:01:42,000
So when we let go of that, and when we start to see that it's not ours,

25
00:01:42,000 --> 00:01:44,000
it's not us.

26
00:01:44,000 --> 00:01:49,000
We can see that we let go of all of these strings that are attached to our hearts.

27
00:01:49,000 --> 00:01:54,000
Our hearts are free from strings.

28
00:01:54,000 --> 00:01:58,000
Those strings are attached.

29
00:01:58,000 --> 00:02:02,000
And nothing pulling on our hearts strings, that they say.

30
00:02:02,000 --> 00:02:06,000
So anyway, I'll be in Chiang Mai and the Chiang Mai area for a little while

31
00:02:06,000 --> 00:02:08,000
then down to Bangkok, then off to LA,

32
00:02:08,000 --> 00:02:11,000
and hopefully I'll see some view in Los Angeles.

33
00:02:11,000 --> 00:02:13,000
Especially mom and dad.

34
00:02:13,000 --> 00:02:20,000
Okay, all of that.

