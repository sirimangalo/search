1
00:00:00,000 --> 00:00:09,000
What could one do with the vows which one made with an unclear mind and regret it?

2
00:00:11,000 --> 00:00:19,000
It's a good question. I normally answer this by saying that it's possible to make bad vows.

3
00:00:20,000 --> 00:00:26,000
I think that's the really the clearest answer you can give to this is that

4
00:00:26,000 --> 00:00:41,000
the most important thing that needs to be said is that first and foremost, if a vow is truly immoral,

5
00:00:42,000 --> 00:00:53,000
impractical or improper, unbeneficial, then there has to be room for breaking the vow.

6
00:00:53,000 --> 00:00:59,000
I don't see any other way around it. It's difficult thing to say because we don't want to encourage people to break promises.

7
00:01:00,000 --> 00:01:09,000
It's a disruptive practice to break a promise, disruptive for your own mind,

8
00:01:10,000 --> 00:01:16,000
disruptive for your own development of the ability to do what you say you're going to do.

9
00:01:16,000 --> 00:01:21,000
And disruptive for the other person who is then disappointed.

10
00:01:22,000 --> 00:01:25,000
I mean, suppose you promised to kill someone.

11
00:01:26,000 --> 00:01:35,000
You made a vow that before my 20th birthday, I'm going to kill my parents or something like that.

12
00:01:36,000 --> 00:01:41,000
I'm going to rob a bank.

13
00:01:41,000 --> 00:01:47,000
Before I turn 30, I'm going to rob a bank. And then you come and meet with Buddhism.

14
00:01:48,000 --> 00:01:54,000
Or you take for Angulimala, for example, Angulimala, I vow to kill 1,000 people.

15
00:01:55,000 --> 00:02:01,000
So I killed 999 and then the 1000th was going to be his mother and then suddenly the Buddha appears.

16
00:02:01,000 --> 00:02:14,000
And he learns from the Buddha, learns the Buddha's teaching. And so then the question is, would you expect him to follow through with his vow?

17
00:02:15,000 --> 00:02:22,000
Even if he had promised to his mother that he would kill 1,000 people or promised someone to do such a horrible deed.

18
00:02:22,000 --> 00:02:30,000
I promised to his teacher, this is what they say. Should he go through with it? I think the answer is no.

19
00:02:31,000 --> 00:02:50,000
So I guess you have to do a little bit of weighing there and you'd have to somehow find a way to balance the unwholesomeness of the act and the unwholesomeness of breaking a promise.

20
00:02:50,000 --> 00:03:09,000
Because it doesn't seem to be technically or necessarily intrinsically immoral to break a promise.

21
00:03:09,000 --> 00:03:24,000
You can, possibly, you can omit to follow through with your promise one would think without giving rise to an immoral mind state. Most people would feel guilty or worried about breaking the promise or afraid of breaking the promise.

22
00:03:25,000 --> 00:03:34,000
But one would think that it were possible to break a promise without an immoral act. It's not a nice thing to do as I said.

23
00:03:34,000 --> 00:03:52,000
But it seems fairly low on the immorality scale. So you'd have to find a balance if, for example, you have married someone.

24
00:03:52,000 --> 00:04:10,000
And you have this vow to stay with them until death to be part. Yeah, in many marriage vows, there's the vow until death to be part. And that was the traditional marriage vow, I don't know what it is now.

25
00:04:10,000 --> 00:04:22,000
But so suppose you made this vow, this is a problem that people come up with and come against, up against. Because then you find out that you're incompatible or you find out that you want to become a monk.

26
00:04:23,000 --> 00:04:35,000
And suddenly you've got a problem. So then the question is, well, is the marriage vow more important than the conflict or more important than becoming a monk or, for example.

27
00:04:35,000 --> 00:04:45,000
So this is where it starts to get difficult, where you can't draw a clear line. You can't clearly say that keeping the vow is immoral or useless or so on.

28
00:04:45,000 --> 00:05:13,000
I think for someone who's intent on becoming a monk, on the practice of the Buddha's teaching, that they would very well break this vow if necessary.

29
00:05:13,000 --> 00:05:32,000
But probably in such a way, I'm thinking of the cases that are on record, you do it in such a way as to make it clear to the other person what your plans are, that you can't possibly continue with this life.

30
00:05:32,000 --> 00:05:44,000
You know, in fact, the one case that I can think of is the case of Wisaka, the man who was wife was Damadina.

31
00:05:45,000 --> 00:05:58,000
And Wisaka became an anagami after he heard the Buddha's teaching. He actually didn't break his marriage vow. He didn't go to become a monk and it might have been very well because he was married.

32
00:05:58,000 --> 00:06:14,000
It might have been the marriage itself that stopped him because he said to her that if she wanted, he would stay with her as a brother.

33
00:06:14,000 --> 00:06:30,000
And look after her, like as though she were his sister, if she wanted to go and find another husband, he wouldn't be averse to that. He wouldn't stand in her way.

34
00:06:31,000 --> 00:06:38,000
So he didn't actually break the promise and this is where you would see that it's a little bit more difficult.

35
00:06:38,000 --> 00:07:00,000
But when it comes to something like the difference between staying married and becoming a monk, I wouldn't say in some cases you have to be realistic because a vow that is made in one state of mind.

36
00:07:00,000 --> 00:07:13,000
I don't see how it should be applicable to a state of mind that has realized that that state of mind was totally off-track.

37
00:07:14,000 --> 00:07:22,000
So suppose you make a marriage vow under the impression that getting married is the best thing to do.

38
00:07:22,000 --> 00:07:30,000
The best thing, the best course of the best way of life.

39
00:07:31,000 --> 00:07:37,000
And then you begin to practice Buddhist meditation and you totally change your outlook on life.

40
00:07:37,000 --> 00:07:51,000
Now, for what reason should you be forced to keep a vow that was made by a totally different person, by a person who is totally different outlook on life.

41
00:07:51,000 --> 00:08:05,000
Once you totally change your outlook or once you change the way of thinking that led you to make that vow, for what reason should you be forced to keep it.

42
00:08:05,000 --> 00:08:31,000
Of course, the concern is for the other person, but I don't see it as an unethical thing to break a vow that was made under a misunderstanding, this idea that this or that vow would lead you to happiness or was made just under ignorance without realizing the consequences.

43
00:08:31,000 --> 00:08:46,000
Suppose you make a vow or you promise someone that you'll go to university, promise that you'll get a job, promise that you'll do this or do that, and then you realize that it's all useless.

44
00:08:46,000 --> 00:09:04,000
You realize that you're just setting yourself up for a life of imprisonment in this rat race world, or simply realize that there's no point in that.

45
00:09:04,000 --> 00:09:27,000
So, the fact that those vows, those promises, or whatever they were, were made under the influence of ignorance when we would call ignorance and Buddhism made under the influence of wrong views, wrong understandings made under the influence of attachments.

46
00:09:27,000 --> 00:09:44,000
Once those attachments disappear, I wouldn't see a strong reason anyway to keep the promises, even if there are expectations of other people, because I think it's a valid excuse to say I was ignorant when I made those vows, when I made those promises.

47
00:09:44,000 --> 00:10:07,000
I think that's a valid. For my point of view, and I'm couching this, I'm trying to be careful here, because I don't want to encourage people to break promises, and I know it's unpleasant to have people break promises to you, but I think for people who are on the other, and they should understand, or they should be sympathetic with people's change of ideas.

48
00:10:07,000 --> 00:10:26,000
So, I would like to say, I would like to be able to say that a promise should be limited to the confines of the state of mind under which it was made.

49
00:10:26,000 --> 00:10:33,000
So, if you still believe that marriage is a good thing, but you want to get married with someone else, for example.

50
00:10:33,000 --> 00:11:00,000
Or whatever, if you can think of some example where it's simply a matter of convenience, or a matter of irresponsibility, simply a matter of changing your mind, or wanting to shirk your responsibilities.

51
00:11:00,000 --> 00:11:14,000
Like, suppose you promise to go and work for one company, and then it turns out that there's some perk for working with another company.

52
00:11:14,000 --> 00:11:28,000
Then, generally, we would expect people to keep their promise and not follow after their desires, or so on, simply because they change their mind, or something better came up, for example.

53
00:11:28,000 --> 00:11:51,000
If you promised, and then something else came up, in the same realm, I would say, don't be so quick to break your promises, but if you realize later that a promise was a bad promise, from my point of view, I'm fully open to letting people encouraging people to break their promises.

54
00:11:51,000 --> 00:12:01,000
And be careful not to make such promises in the future, because it's not pleasant to have promises broken on you, and it's not pleasant to break your promises either.

55
00:12:01,000 --> 00:12:25,000
Anyway, no, I haven't ever answered that question, so there's my sort of, not totally clear thoughts on it, so I hope that helps.

