1
00:00:00,000 --> 00:00:04,580
Hello and welcome back to our study of the Dhamabanda.

2
00:00:04,580 --> 00:00:10,580
Today, we continue with verse number 15, which comes as follows.

3
00:00:10,580 --> 00:00:19,080
In the Sotrati, page of Sotrati, papa kari, ubhayatha, sotrati, sotrati, sotrati, sotrati, sotrati,

4
00:00:19,080 --> 00:00:26,280
sotrati, sotrati, sotrati, sotrati, sotrati, sotrati, sotrati, sotrati, sotrati, sotrati,

5
00:00:26,280 --> 00:00:34,880
sotrati, here he grieves, pages of sotrati, here after he grieves, papa kari, ubhayatha, sotrati,

6
00:00:34,880 --> 00:00:38,680
the evil doer, grieves in both places.

7
00:00:38,680 --> 00:00:43,880
So sotrati, sotrati, sotrati, sotrati, he grieves, he is afflicted,

8
00:00:43,880 --> 00:00:47,680
he is destroyed.

9
00:00:47,680 --> 00:00:57,680
He sua kamakimitamatana, seeing the evil or the defilement of his own actions.

10
00:00:57,680 --> 00:01:04,680
So this is the teaching on negative, on evil karma, evil needs.

11
00:01:04,680 --> 00:01:08,480
When we have two verses here, the next verse is in regards to a different story.

12
00:01:08,480 --> 00:01:13,280
So the story with this verse, said in the time of the Buddha there was a pig butcher.

13
00:01:13,280 --> 00:01:21,280
And when the monks would go on arms round they would walk past and they would hear the

14
00:01:21,280 --> 00:01:25,280
screams or they would even see the pigs being killed.

15
00:01:25,280 --> 00:01:32,280
And the way the commentary explains the killing went on is this man he would tie the pig

16
00:01:32,280 --> 00:01:39,280
to a post so it couldn't move and then beat it with a club to make its skin off and its

17
00:01:39,280 --> 00:01:44,280
flesh tender but it was still alive.

18
00:01:44,280 --> 00:01:48,280
And then take boiling hot water and pour it down the pigs throat and have all the feces

19
00:01:48,280 --> 00:01:55,280
and undigested food pour out the bottom and he would continue to pour boiling water

20
00:01:55,280 --> 00:02:03,280
in until the water that came out was clean and meaning there was no feces left inside.

21
00:02:03,280 --> 00:02:09,280
Then he would pour the rest of the water over the pig's skin to peel off the outer skin

22
00:02:09,280 --> 00:02:15,280
and use the torch to burn off all the pig's hair and then he would cut off the pig's head

23
00:02:15,280 --> 00:02:17,280
and then it would die.

24
00:02:17,280 --> 00:02:23,280
And he would collect the blood from the pig's neck and roast the pig in its own blood

25
00:02:23,280 --> 00:02:27,280
eat as much as he could with his family and sell the rest.

26
00:02:27,280 --> 00:02:30,280
And they say for 55 years he lived his life like this.

27
00:02:30,280 --> 00:02:40,280
And one day the monks were walking by 55 years later and they saw that all of the doors

28
00:02:40,280 --> 00:02:49,280
of the house were closed up and they could still hear the screams of pigs and grunting

29
00:02:49,280 --> 00:02:54,280
noises and scuffling and so they assumed that there must be something great going on

30
00:02:54,280 --> 00:03:01,280
and so they started talking about this. How amazing it was 55 years this man had never learned the weight

31
00:03:01,280 --> 00:03:07,280
of the evil of his deeds and had never had never even come to listen to the Buddha's teaching

32
00:03:07,280 --> 00:03:12,280
or come to pay respect to the Buddha at all.

33
00:03:12,280 --> 00:03:17,280
And the Buddha heard them talking and asked them what are you talking about?

34
00:03:17,280 --> 00:03:20,280
They said they explained what it was.

35
00:03:20,280 --> 00:03:27,280
And the Buddha said they're not and they said oh and today we see that he's for seven days

36
00:03:27,280 --> 00:03:32,280
now and so it's seven days later we see that they have been closed up for seven days

37
00:03:32,280 --> 00:03:38,280
so they must be working on an even bigger slaughter or a special slaughter of some sort.

38
00:03:38,280 --> 00:03:41,280
And the Buddha said no that's not really what's going on at all.

39
00:03:41,280 --> 00:03:47,280
Seven days ago it turns out that the Judah this pork butcher became very ill

40
00:03:47,280 --> 00:03:51,280
and his affliction went to his head and he became quite deranged.

41
00:03:51,280 --> 00:03:58,280
And as a result of the sickness and the derangement he started walking around the house

42
00:03:58,280 --> 00:04:01,280
on his hands and knees and grunting like a pig.

43
00:04:01,280 --> 00:04:04,280
Screaming like a pig and this is what they heard.

44
00:04:04,280 --> 00:04:07,280
And the people in the house would try to restrain him but they couldn't do it.

45
00:04:07,280 --> 00:04:12,280
Also because as the commentary said he began to see his future

46
00:04:12,280 --> 00:04:17,280
and this brought in great derangement on his deathbed

47
00:04:17,280 --> 00:04:23,280
the fear and the concentration of the mind at that point allowed him to see his future

48
00:04:23,280 --> 00:04:28,280
and he began to draw in crazy.

49
00:04:28,280 --> 00:04:32,280
And as a result all he could think about was the killing of the pigs

50
00:04:32,280 --> 00:04:36,280
and as a result dwelt that way for seven days.

51
00:04:36,280 --> 00:04:42,280
And then on that day on the seventh day it was over and he died and went to hell.

52
00:04:42,280 --> 00:04:44,280
So the Buddha explained what was really going on.

53
00:04:44,280 --> 00:04:46,280
And then he said to the monks, this is how it goes.

54
00:04:46,280 --> 00:04:49,280
It's not just in the next life that a person suffers.

55
00:04:49,280 --> 00:04:52,280
A person who does evil deeds.

56
00:04:52,280 --> 00:04:57,280
A person who does evil deeds suffers both in this life and the next.

57
00:04:57,280 --> 00:05:05,280
So how this relates to our practice I think should be quite clear.

58
00:05:05,280 --> 00:05:08,280
And it becomes quite clear when a person undertakes practice

59
00:05:08,280 --> 00:05:11,280
because for the first time they're able to realize

60
00:05:11,280 --> 00:05:14,280
in the full magnitude of their deeds.

61
00:05:14,280 --> 00:05:18,280
I think or it's clear that people who don't practice meditation

62
00:05:18,280 --> 00:05:21,280
are unable to understand the law of karma

63
00:05:21,280 --> 00:05:24,280
and they think of it as some kind of belief

64
00:05:24,280 --> 00:05:27,280
or some kind of theory.

65
00:05:27,280 --> 00:05:29,280
And they don't really understand because when they do evil deeds

66
00:05:29,280 --> 00:05:32,280
they look around and they don't see anything happening.

67
00:05:32,280 --> 00:05:36,280
I killed someone or I killed an animal and now I'm going to be killed.

68
00:05:36,280 --> 00:05:37,280
They look around and not killed.

69
00:05:37,280 --> 00:05:40,280
So they think, well, then there's nothing wrong with doing evil deeds

70
00:05:40,280 --> 00:05:45,280
because they can't see really what is the result of an good or an evil deed.

71
00:05:45,280 --> 00:05:49,280
When a person comes to practice meditation they're watching every moment

72
00:05:49,280 --> 00:05:52,280
and they're seeing quite clearly what are the results of their deeds

73
00:05:52,280 --> 00:05:55,280
when they cling to something, what is the result?

74
00:05:55,280 --> 00:05:58,280
When they are angry or upset about something, what is the result?

75
00:05:58,280 --> 00:05:59,280
How does it affect their body?

76
00:05:59,280 --> 00:06:01,280
How does it affect their mind?

77
00:06:01,280 --> 00:06:07,280
How does it affect their clarity and their character?

78
00:06:07,280 --> 00:06:14,280
And so as a result the law of karma becomes quite clear and quite evident.

79
00:06:14,280 --> 00:06:18,280
But for evil doer, for people who are engaged in great evil,

80
00:06:18,280 --> 00:06:20,280
it's often not clear.

81
00:06:20,280 --> 00:06:25,280
And it can often be the fact that because of their position

82
00:06:25,280 --> 00:06:30,280
and because of good past deeds and their relative stability of mind

83
00:06:30,280 --> 00:06:35,280
they're unable to see the small seed growing inside

84
00:06:35,280 --> 00:06:39,280
and moreover they're often able through the power of the mind

85
00:06:39,280 --> 00:06:44,280
to push it away and to avoid looking at it, to avoid thinking about it

86
00:06:44,280 --> 00:06:49,280
so that they aren't aware of evil deeds until it becomes so great

87
00:06:49,280 --> 00:06:51,280
and so gross that they can no longer hide it

88
00:06:51,280 --> 00:06:54,280
especially when they become sick and on their deathbed

89
00:06:54,280 --> 00:06:57,280
and their power of mind becomes much weaker.

90
00:06:57,280 --> 00:07:01,280
An ordinary person spends most of their time covering up their deeds.

91
00:07:01,280 --> 00:07:05,280
Now it does happen that good people do bad deeds

92
00:07:05,280 --> 00:07:11,280
and do feel guilty and feel remorseful and are able to see how it's affected them.

93
00:07:11,280 --> 00:07:15,280
When a person does, or if a person starts going down a wrong path

94
00:07:15,280 --> 00:07:18,280
and they start to realize the nature of their mind,

95
00:07:18,280 --> 00:07:22,280
they burst out at someone and they realize that they have a lot of anger inside.

96
00:07:22,280 --> 00:07:26,280
Then they might feel guilty and that there's something they should be done.

97
00:07:26,280 --> 00:07:33,280
And as a result they will begin to refrain from that in the future.

98
00:07:33,280 --> 00:07:37,280
But it can often be the case that as in the case of Judah,

99
00:07:37,280 --> 00:07:40,280
it only realisation only comes from next to night.

100
00:07:40,280 --> 00:07:43,280
So people often ask, well why, if there is this law of karma,

101
00:07:43,280 --> 00:07:49,280
why do we see good people suffering and bad people prospering?

102
00:07:49,280 --> 00:07:55,280
But when you practice meditation, when you actually look and examine what's going on inside,

103
00:07:55,280 --> 00:07:58,280
you'll see that it's true that good people might suffer,

104
00:07:58,280 --> 00:08:00,280
but their suffering isn't because of their goodness.

105
00:08:00,280 --> 00:08:03,280
And it's true that evil people may prosper,

106
00:08:03,280 --> 00:08:06,280
but their prospering is not because of their evil.

107
00:08:06,280 --> 00:08:07,280
It can't be.

108
00:08:07,280 --> 00:08:10,280
And you can see this by watching clearly what, as I said,

109
00:08:10,280 --> 00:08:14,280
what happens when these mind states arise, what do they lead to?

110
00:08:14,280 --> 00:08:18,280
You can see that a good state can never lead to suffering,

111
00:08:18,280 --> 00:08:24,280
and an evil state can never lead to happiness or to prosperity.

112
00:08:24,280 --> 00:08:29,280
It's only generally in this life is because of our position

113
00:08:29,280 --> 00:08:36,280
and because of the static nature of the material body and of the material world.

114
00:08:36,280 --> 00:08:42,280
So our person might be born into a position of power or a position of wealth.

115
00:08:42,280 --> 00:08:46,280
And as a result they can do many evil deeds without great repercussions,

116
00:08:46,280 --> 00:08:51,280
except in the mind which of course is much more dynamic,

117
00:08:51,280 --> 00:08:54,280
and much quicker to pick up on the changes.

118
00:08:54,280 --> 00:08:57,280
But eventually even the physical will begin to pick it up.

119
00:08:57,280 --> 00:09:01,280
If you will lose your friends who will gain corrupt friends,

120
00:09:01,280 --> 00:09:05,280
your whole environment will slowly change because it changes a lot slower.

121
00:09:05,280 --> 00:09:11,280
And so the results can be a lot slower coming and quite a bit less evident.

122
00:09:11,280 --> 00:09:14,280
Now at the moment and death all of that changes

123
00:09:14,280 --> 00:09:17,280
because the physical is removed from the equation,

124
00:09:17,280 --> 00:09:21,280
and the mind is now only depending on the mind.

125
00:09:21,280 --> 00:09:25,280
There is no going back to seeing hearings now and to see there's nothing based on the body.

126
00:09:25,280 --> 00:09:28,280
There's only the mental activity.

127
00:09:28,280 --> 00:09:35,280
So it's a repeated experience of the deeds that one has done,

128
00:09:35,280 --> 00:09:38,280
memories or whatever is going on in the mind.

129
00:09:38,280 --> 00:09:41,280
So if one has a mind that is full of corruption,

130
00:09:41,280 --> 00:09:44,280
then this will continue repeatedly, arise in the mind.

131
00:09:44,280 --> 00:09:48,280
There are thoughts of the evil deeds that we've done or memories of the evil deeds,

132
00:09:48,280 --> 00:09:53,280
or thoughts about the future, a feeling of where you're being pulled,

133
00:09:53,280 --> 00:09:56,280
the future that you're developing for yourself.

134
00:09:56,280 --> 00:10:00,280
And this is clear many people actually experience this on their death bed and can relate this.

135
00:10:00,280 --> 00:10:05,280
Some people actually leave their bodies and can see their bodies or can see their loved ones or so on,

136
00:10:05,280 --> 00:10:08,280
and find themselves floating away.

137
00:10:08,280 --> 00:10:11,280
If they have a near death experience, they'll come back and they'll be able to relate.

138
00:10:11,280 --> 00:10:14,280
Good things are bad things that they saw that happen to them.

139
00:10:14,280 --> 00:10:18,280
And it will be based on whatever's being clung to in their mind,

140
00:10:18,280 --> 00:10:22,280
whatever their mind is attaching importance to at that time.

141
00:10:22,280 --> 00:10:26,280
So if a person is greatly engaged in evil deeds,

142
00:10:26,280 --> 00:10:30,280
then they will be born in a place that is full of suffering

143
00:10:30,280 --> 00:10:34,280
because of their corrupt state of mind as the Buddha said.

144
00:10:34,280 --> 00:10:38,280
When they see the evil, they will finally realize.

145
00:10:38,280 --> 00:10:43,280
So in some sense, it's much better to actually see the evil,

146
00:10:43,280 --> 00:10:45,280
and to sort, to feel sorrow.

147
00:10:45,280 --> 00:10:51,280
We had a discussion about this as to whether this is what the Buddha meant by Hiri and Otapah.

148
00:10:51,280 --> 00:10:55,280
And it isn't really what he meant by Hiri and Otapah to feel shame or to feel guilt.

149
00:10:55,280 --> 00:11:00,280
But to see that you've done bad things can be a great waking up

150
00:11:00,280 --> 00:11:04,280
and can lead you to feel shame and to feel Hiri and Otapah.

151
00:11:04,280 --> 00:11:10,280
Meaning in the future, when the opportunity presents itself to do an evil deed,

152
00:11:10,280 --> 00:11:12,280
you will shy away from it.

153
00:11:12,280 --> 00:11:14,280
You will recoil from it.

154
00:11:14,280 --> 00:11:21,280
The mind will naturally incline against it because of seeing the evil of it,

155
00:11:21,280 --> 00:11:24,280
and seeing the evil nature of it.

156
00:11:24,280 --> 00:11:28,280
And this is what we gain very much from meditation, from just introspection.

157
00:11:28,280 --> 00:11:31,280
A person just begins to come to meditate in the very beginning.

158
00:11:31,280 --> 00:11:35,280
They will see all of what they have carried with them

159
00:11:35,280 --> 00:11:38,280
and built up over the years through not meditating.

160
00:11:38,280 --> 00:11:41,280
They'll see all of the habits and tendencies,

161
00:11:41,280 --> 00:11:45,280
often a big wave of defilement, of corruption.

162
00:11:45,280 --> 00:11:48,280
At times it can be great anger and hatred.

163
00:11:48,280 --> 00:11:53,280
At times it can be great lust and need and wanting.

164
00:11:53,280 --> 00:12:01,280
And so they're able to realize, as is here,

165
00:12:01,280 --> 00:12:03,280
that they will grieve in the beginning.

166
00:12:03,280 --> 00:12:05,280
They will feel great suffering in the beginning.

167
00:12:05,280 --> 00:12:10,280
But eventually they will be a cause for them to change their ways.

168
00:12:10,280 --> 00:12:13,280
And in the future they won't want to do these things

169
00:12:13,280 --> 00:12:17,280
because they know how much it hurts and affects their mind.

170
00:12:17,280 --> 00:12:21,280
So this is the story of June of the Pork Butcher

171
00:12:21,280 --> 00:12:26,280
and the Buddha's teaching on the suffering that comes from doing evil deeds.

172
00:12:26,280 --> 00:12:28,280
So thanks for tuning in.

173
00:12:28,280 --> 00:12:55,280
This has been another episode of our study of the Damupada.

