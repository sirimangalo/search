1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:14,000
Today we continue with verse 221, which reads as follows.

3
00:00:14,000 --> 00:00:21,000
Godhang jahi vipajahi yamanang.

4
00:00:21,000 --> 00:00:24,000
Sanyo janam atikam.

5
00:00:24,000 --> 00:00:28,000
Sanyo janang sabamatikamahi yah.

6
00:00:28,000 --> 00:00:33,000
Thang nam rupas mim asad jamanang.

7
00:00:33,000 --> 00:00:39,000
A kim janang na nah nah nupatantidoka.

8
00:00:39,000 --> 00:00:56,000
Which means one should give up anger, one should abandon conceit.

9
00:00:56,000 --> 00:01:08,000
That person who has no clinging or who is not hung up on nama and rupa,

10
00:01:08,000 --> 00:01:14,000
the physical and the mental.

11
00:01:14,000 --> 00:01:24,000
There arises or no suffering before such a person.

12
00:01:24,000 --> 00:01:32,000
There arises no suffering for such a person.

13
00:01:32,000 --> 00:01:46,000
This verse was taught regarding a story about Anurudha and his sister.

14
00:01:46,000 --> 00:02:01,000
Anurudha, when the story goes, when to visit Kapilawatu, Anurudha was one of the Buddha's cousins.

15
00:02:01,000 --> 00:02:08,000
He went back to Kapilawatu to visit family.

16
00:02:08,000 --> 00:02:14,000
When he was staying in Nigrodarama, the monastery near Kapilawatu,

17
00:02:14,000 --> 00:02:21,000
all of his relatives came to visit him except for his sister, Rohini.

18
00:02:21,000 --> 00:02:30,000
He asked his relatives, they came and paid respect to him and greeted him and spoke amicably.

19
00:02:30,000 --> 00:02:35,000
He asked them where was my sister and they said,

20
00:02:35,000 --> 00:02:40,000
she didn't come, she didn't want to come.

21
00:02:40,000 --> 00:02:43,000
They said, we'll call her.

22
00:02:43,000 --> 00:02:49,000
She has this rash, a skin problem.

23
00:02:49,000 --> 00:02:54,000
Big boils or something.

24
00:02:54,000 --> 00:03:04,000
And slightly skin condition, she doesn't want to be seen, she's ashamed of it.

25
00:03:04,000 --> 00:03:08,000
And Adhara said, oh, call her, call her, call her out.

26
00:03:08,000 --> 00:03:11,000
Make her come and see me.

27
00:03:11,000 --> 00:03:14,000
And they, or convince her, no, not make her this.

28
00:03:14,000 --> 00:03:17,000
So they went to find her, they called her, said,

29
00:03:17,000 --> 00:03:19,000
look, he wants you to come and see him.

30
00:03:19,000 --> 00:03:23,000
So finally she was convinced to come to the monastery and visit her brother.

31
00:03:23,000 --> 00:03:25,000
And he said, what's wrong?

32
00:03:25,000 --> 00:03:27,000
And she said, oh, I've got this skin condition.

33
00:03:27,000 --> 00:03:34,000
She was all covered up, covered up her face and didn't want to show herself.

34
00:03:34,000 --> 00:03:38,000
And he said, what was his response?

35
00:03:38,000 --> 00:03:40,000
It wasn't, have you seen a doctor or something?

36
00:03:40,000 --> 00:03:44,000
It was, why aren't you doing any good deeds?

37
00:03:44,000 --> 00:03:49,000
And I'm not sure how that was meant whether he meant just because you don't have,

38
00:03:49,000 --> 00:03:52,000
just because you have a skin condition doesn't mean you can't come to the monastery

39
00:03:52,000 --> 00:03:54,000
and listen to the demand.

40
00:03:54,000 --> 00:03:58,000
Maybe offer food to the monks or that sort of thing.

41
00:03:58,000 --> 00:04:02,000
But the way it's presented is more like,

42
00:04:02,000 --> 00:04:08,000
is there something, because I'm good deed you can do to alleviate your skin condition.

43
00:04:08,000 --> 00:04:11,000
And she just goes, because she asked him, what can I do?

44
00:04:11,000 --> 00:04:15,000
And he said, well, you should build an assembly hall

45
00:04:15,000 --> 00:04:19,000
and all these people coming to visit and monks coming to the monastery

46
00:04:19,000 --> 00:04:24,000
with this place needs to build an assembly hall for the monks

47
00:04:24,000 --> 00:04:28,000
and for the people to come in when they visit the monk.

48
00:04:30,000 --> 00:04:34,000
And she said, well, where am I going to get the kind of, how am I going to do that?

49
00:04:34,000 --> 00:04:36,000
And he said, well, don't do you have any jewelry

50
00:04:36,000 --> 00:04:38,000
and she had apparently this very expensive jewelry

51
00:04:38,000 --> 00:04:42,000
and he said, well, saw that.

52
00:04:42,000 --> 00:04:45,000
And you know, do this good deed, this would be something.

53
00:04:45,000 --> 00:04:50,000
This is, this was his cure, basically, as I understand it.

54
00:04:51,000 --> 00:04:54,000
And so she, I think the iron the details out

55
00:04:54,000 --> 00:04:56,000
and unwritten overseas at all.

56
00:04:56,000 --> 00:04:59,000
He's in the monastery and he oversees the building

57
00:04:59,000 --> 00:05:01,000
and he says to her, as soon as it's finished,

58
00:05:01,000 --> 00:05:05,000
you take up the duty to sweep it out

59
00:05:05,000 --> 00:05:08,000
and to clean it and to keep it maintained.

60
00:05:08,000 --> 00:05:11,000
And she did that, and as she was doing that, the story says,

61
00:05:11,000 --> 00:05:15,000
her skin got better, started to get better.

62
00:05:15,000 --> 00:05:18,000
Her skin, well, she was sweeping out this hall

63
00:05:18,000 --> 00:05:22,000
that she had built with, by sacrificing what was dear to her,

64
00:05:22,000 --> 00:05:24,000
her dual jewelry.

65
00:05:24,000 --> 00:05:30,000
And her skin cleared, started clearing up.

66
00:05:33,000 --> 00:05:38,000
And then at some point the Buddha came with all of the monks

67
00:05:38,000 --> 00:05:43,000
and she organized this great gift giving of the assembly hall

68
00:05:44,000 --> 00:05:48,000
and offered food to all the monks with the Buddha there.

69
00:05:48,000 --> 00:05:52,000
But she didn't come out herself because her skin condition

70
00:05:52,000 --> 00:05:55,000
was still not cured.

71
00:05:55,000 --> 00:06:00,000
It was better, I guess, but still she was quite ashamed of it.

72
00:06:00,000 --> 00:06:02,000
And so the Buddha asked, where's Rohini?

73
00:06:02,000 --> 00:06:06,000
Where's the donor of this new new hall?

74
00:06:06,000 --> 00:06:09,000
And they said, oh, she doesn't want to come.

75
00:06:09,000 --> 00:06:13,000
And he said, well, call her anyway.

76
00:06:13,000 --> 00:06:15,000
And so she came out finally.

77
00:06:15,000 --> 00:06:18,000
She was convinced to come to the hall.

78
00:06:18,000 --> 00:06:22,000
Remember, I said, why didn't you want to come?

79
00:06:24,000 --> 00:06:27,000
And she said, when I'm both sorry, I'm too ashamed

80
00:06:27,000 --> 00:06:30,000
of my skin condition.

81
00:06:30,000 --> 00:06:35,000
And the Buddha said, do you know why you have that skin condition?

82
00:06:35,000 --> 00:06:38,000
And he said, no, no, I don't.

83
00:06:38,000 --> 00:06:42,000
And he told her a story of the past.

84
00:06:42,000 --> 00:06:47,000
Once upon a time Rohini was a queen.

85
00:06:49,000 --> 00:06:51,000
By the way, they say the story.

86
00:06:51,000 --> 00:06:54,000
Once upon a time there was a queen.

87
00:06:54,000 --> 00:06:58,000
And this queen was a little bit vain,

88
00:06:58,000 --> 00:07:03,000
a little bit subject to cruelty.

89
00:07:03,000 --> 00:07:10,000
And one day she took a disliking to one of the king's dancers.

90
00:07:10,000 --> 00:07:14,000
I guess the thing about king's back then, or throughout time,

91
00:07:14,000 --> 00:07:17,000
is they can do whatever they want.

92
00:07:17,000 --> 00:07:19,000
Not only do they have multiple queens,

93
00:07:19,000 --> 00:07:26,000
but on top of the queens, they also have concubines and dancers.

94
00:07:26,000 --> 00:07:30,000
It seems like probably in many cases it was the ultimate sort of debauchery.

95
00:07:30,000 --> 00:07:34,000
Because there's no law higher than you, you are the law.

96
00:07:34,000 --> 00:07:37,000
You're certainly above the law.

97
00:07:42,000 --> 00:07:46,000
So I guess maybe one of these dancers got the king's eye,

98
00:07:46,000 --> 00:07:48,000
and she was jealous, or it doesn't really say,

99
00:07:48,000 --> 00:07:52,000
she was just angry at that dancer.

100
00:07:52,000 --> 00:07:56,000
And so what she did is she got this,

101
00:07:56,000 --> 00:08:01,000
Maha-kachupala, Maha-kachupalaani.

102
00:08:01,000 --> 00:08:05,000
And the text I read in English originally said,

103
00:08:05,000 --> 00:08:07,000
Scabs.

104
00:08:07,000 --> 00:08:12,000
She got some scabs, which is absolutely not what it means.

105
00:08:12,000 --> 00:08:14,000
So I looked up a different translation,

106
00:08:14,000 --> 00:08:17,000
and I looked at the actual poly.

107
00:08:17,000 --> 00:08:23,000
Kachu means scab, but it also means it's also the name of a fruit

108
00:08:23,000 --> 00:08:27,000
that causes skin rash.

109
00:08:27,000 --> 00:08:32,000
It's called something like violet, violet bean or something.

110
00:08:32,000 --> 00:08:38,000
In Africa, they apparently call it devils bean or

111
00:08:38,000 --> 00:08:42,000
insane or crazy bean or something like that.

112
00:08:42,000 --> 00:08:46,000
Because if you touch the bean pods or the flowers,

113
00:08:46,000 --> 00:08:50,000
these violet flowers, the oil in it,

114
00:08:50,000 --> 00:08:53,000
or something the chemical is very nasty, and it spreads on your skin.

115
00:08:53,000 --> 00:08:56,000
And if you spread it on your skin or to your eyes,

116
00:08:56,000 --> 00:09:01,000
causes violent rash and itching and scratching and eventually

117
00:09:01,000 --> 00:09:02,000
scabs, I guess.

118
00:09:02,000 --> 00:09:06,000
So she found some of this plant,

119
00:09:06,000 --> 00:09:09,000
and she made powder out of it.

120
00:09:09,000 --> 00:09:15,000
And she took some of the powder, and she spread it all in this dancer's bed.

121
00:09:15,000 --> 00:09:20,000
In her sheets, in her blankets, and all of her clothes and everything.

122
00:09:20,000 --> 00:09:25,000
And then she went up to her and threw some on her.

123
00:09:25,000 --> 00:09:28,000
That's the nasty people can be.

124
00:09:28,000 --> 00:09:35,000
When the course of this dancer was struck with incredible itching and

125
00:09:35,000 --> 00:09:38,000
violent, I would break on her skin.

126
00:09:38,000 --> 00:09:41,000
And so she went to her bed to rest.

127
00:09:41,000 --> 00:09:46,000
Then when she got into her bed, it got even worse.

128
00:09:46,000 --> 00:09:51,000
That's the story.

129
00:09:51,000 --> 00:09:54,000
And then having told this story, the Buddha said,

130
00:09:54,000 --> 00:09:59,000
anger, anger has great and lasting consequences.

131
00:09:59,000 --> 00:10:04,000
And then he taught this verse.

132
00:10:04,000 --> 00:10:09,000
So from the story, we get this similar lesson to last week.

133
00:10:09,000 --> 00:10:15,000
This verse is the first verse in the Godhawaga, the chapter on anger.

134
00:10:15,000 --> 00:10:17,000
So we're going to see lots more about anger here.

135
00:10:17,000 --> 00:10:21,000
But similar to the last verse it talks about,

136
00:10:21,000 --> 00:10:26,000
or it points to the idea that the mind can affect the physical world.

137
00:10:26,000 --> 00:10:28,000
In this case, the body.

138
00:10:28,000 --> 00:10:30,000
And we see that in two examples.

139
00:10:30,000 --> 00:10:33,000
We see that her anger in a past life.

140
00:10:33,000 --> 00:10:36,000
Again, this is classic Buddhist conception of karma.

141
00:10:36,000 --> 00:10:40,000
The evil that she had done in a past life scarred her mind

142
00:10:40,000 --> 00:10:44,000
and created the potential for her to be reborn,

143
00:10:44,000 --> 00:10:50,000
created this sickness in her next life, this skin condition.

144
00:10:50,000 --> 00:10:54,000
And on the other side, we see how the doing of good deeds alleviated.

145
00:10:54,000 --> 00:11:00,000
It's the first one she was sweeping and had given this great gift

146
00:11:00,000 --> 00:11:04,000
of an assembly hall to the community.

147
00:11:04,000 --> 00:11:07,000
How it affected her physical appearance.

148
00:11:07,000 --> 00:11:10,000
It alleviated her sickness.

149
00:11:10,000 --> 00:11:13,000
And the story actually continues after the Buddha taught the story.

150
00:11:13,000 --> 00:11:17,000
Her skin condition completely cleared up and she became beautiful.

151
00:11:17,000 --> 00:11:19,000
She became radiant.

152
00:11:19,000 --> 00:11:21,000
Golden skin, there's something.

153
00:11:27,000 --> 00:11:30,000
And this is, I think in general, an important lesson.

154
00:11:30,000 --> 00:11:34,000
It's a classic Buddhist doctrine.

155
00:11:34,000 --> 00:11:44,000
The idea that good thoughts or curative are healing.

156
00:11:44,000 --> 00:11:50,000
And so it stands in comparison to ideas like positive thinking.

157
00:11:50,000 --> 00:11:58,000
We hear a lot about the power of positive thinking people ask me about.

158
00:11:58,000 --> 00:12:02,000
And it's touted as sort of a healing sort of thing.

159
00:12:02,000 --> 00:12:04,000
And at least heels the mind and it has the potential.

160
00:12:04,000 --> 00:12:06,000
They say to bring you good things in your life.

161
00:12:06,000 --> 00:12:11,000
If you think I deserve good things, good things will come to me.

162
00:12:11,000 --> 00:12:15,000
That sort of thing that somehow that brings positive results.

163
00:12:15,000 --> 00:12:18,000
And I can't really refute that.

164
00:12:18,000 --> 00:12:21,000
I don't know what the studies say or the science says of it.

165
00:12:21,000 --> 00:12:25,000
But what you can say about it is it's incredibly self-serving.

166
00:12:25,000 --> 00:12:32,000
And so the quality of mind that it evokes is not going to be by a Buddhist standard pure.

167
00:12:32,000 --> 00:12:41,000
In general, the idea of positive thinking is generally greedy or desirous.

168
00:12:41,000 --> 00:12:46,000
It has to do with desire, it can have to do with conceit and so on.

169
00:12:46,000 --> 00:12:55,000
And so Buddhism that taken Buddhism is different, it's distinct when you say not positive thoughts.

170
00:12:55,000 --> 00:12:58,000
Think positive thought, things good thoughts in that sense.

171
00:12:58,000 --> 00:13:01,000
Think good thoughts in another sense.

172
00:13:01,000 --> 00:13:06,000
And it's not even think it's have good mind states.

173
00:13:06,000 --> 00:13:15,000
And so in the doing of good deeds and charity and ethics and meditation and all sorts of

174
00:13:15,000 --> 00:13:18,000
help that you perform to others and that sort of thing.

175
00:13:18,000 --> 00:13:23,000
There's such a wholesome and good state of mind that that is said in Buddhism to be curative.

176
00:13:23,000 --> 00:13:29,000
That is an important part of basic Buddhist practice.

177
00:13:29,000 --> 00:13:35,000
That we find things to do and ways to act, ways to speak, even ways to think

178
00:13:35,000 --> 00:13:41,000
that are beneficial, that are good.

179
00:13:41,000 --> 00:13:45,000
As they have far-reaching benefits, not just in this life but in the next.

180
00:13:45,000 --> 00:13:47,000
It's something that Buddha taught quite often.

181
00:13:47,000 --> 00:13:54,000
It's a basic teaching, it's not profound or enlightening, but it's a great support in the spiritual path.

182
00:13:54,000 --> 00:13:57,000
And that's really the best way that goodness should be understood.

183
00:13:57,000 --> 00:14:01,000
From Buddhist perspective, it's a great support for our meditation practice.

184
00:14:01,000 --> 00:14:04,000
So it is a very relevant.

185
00:14:04,000 --> 00:14:11,000
The verse has a more profound lesson and it offers a fairly detailed.

186
00:14:11,000 --> 00:14:20,000
And for such a brief teaching, it offers a fairly detailed teaching on the path.

187
00:14:20,000 --> 00:14:26,000
First, Godhang Jahi, abandoned, one should abandon anger.

188
00:14:26,000 --> 00:14:32,000
In general, the verse is talking about the overall theme of the verse, of course,

189
00:14:32,000 --> 00:14:40,000
the overcoming of suffering, the freedom from suffering, which is the Buddhist goal.

190
00:14:40,000 --> 00:14:46,000
And so the first way that one frees oneself from suffering is by abandoning anger.

191
00:14:46,000 --> 00:14:49,000
One should abandon anger.

192
00:14:49,000 --> 00:14:57,000
This is contested, I think, in the world people are often interested in

193
00:14:57,000 --> 00:15:06,000
or keen or attached to their anger, of the view that anger is at times beneficial.

194
00:15:06,000 --> 00:15:10,000
If you didn't have anger, people might walk all over you.

195
00:15:10,000 --> 00:15:14,000
If you didn't have anger, you wouldn't get things done and so on.

196
00:15:14,000 --> 00:15:16,000
And it may very well be.

197
00:15:16,000 --> 00:15:20,000
It certainly is the case that many times anger is what propels people to do.

198
00:15:20,000 --> 00:15:24,000
Even perhaps what you could argue were good things.

199
00:15:24,000 --> 00:15:28,000
But from a Buddhist perspective, it doesn't have much meaning.

200
00:15:28,000 --> 00:15:32,000
The doing of things, the desire for things to be done,

201
00:15:32,000 --> 00:15:35,000
the worry or concern that people might walk all over you.

202
00:15:35,000 --> 00:15:39,000
All of those things are ultimately based in attachment.

203
00:15:39,000 --> 00:15:45,000
So on a basic practical level, getting angry once in a while isn't going to condemn you

204
00:15:45,000 --> 00:15:46,000
in a Buddhist sense.

205
00:15:46,000 --> 00:15:49,000
It isn't going to send you to hell or something.

206
00:15:49,000 --> 00:15:52,000
It isn't going to make you unable to practice.

207
00:15:52,000 --> 00:15:55,000
But ultimately, anger is at its very core.

208
00:15:55,000 --> 00:16:02,000
One of the things that gets in the way of the practice gets in the way of seeing clearly.

209
00:16:02,000 --> 00:16:12,000
And on a very practical, basic level, it's something that leads to sickness in the body as well.

210
00:16:12,000 --> 00:16:17,000
It's considered a mental sickness because of it being inherently stressful.

211
00:16:17,000 --> 00:16:26,000
In many cases, anger is the easiest one for us to see as being harmful because of how painful it is.

212
00:16:26,000 --> 00:16:32,000
Compared to something like greed or desire, which can be quite pleasant.

213
00:16:32,000 --> 00:16:37,000
I often get at people asking how they can overcome or be free from anger.

214
00:16:37,000 --> 00:16:39,000
Because it's unpleasant, they can see that.

215
00:16:39,000 --> 00:16:41,000
And they can also see the results.

216
00:16:41,000 --> 00:16:46,000
The results of anger are incredibly damaging.

217
00:16:46,000 --> 00:17:01,000
We, we, we, we, people are physically violent with people who they would otherwise wish happiness toward because of anger.

218
00:17:01,000 --> 00:17:02,000
Go into hunger.

219
00:17:02,000 --> 00:17:07,000
Hey, if you want to be free from suffering both physically and mentally, it makes you sick.

220
00:17:07,000 --> 00:17:08,000
You can feel sick.

221
00:17:08,000 --> 00:17:11,000
You can feel how sick it is when you're very, very angry.

222
00:17:11,000 --> 00:17:18,000
Sometimes people get bloody nose because blood coming out of their nose when they're anxious or so angry.

223
00:17:18,000 --> 00:17:24,000
There's tension, headaches, strokes can come because people are so angry.

224
00:17:24,000 --> 00:17:35,000
And ultimately, the lasting impacts on the body, even in this life, are certainly measurable.

225
00:17:35,000 --> 00:17:43,000
Now, the idea is simply that when one dies, that doesn't stop, and there's some, you can get if you want to understand how these things work.

226
00:17:43,000 --> 00:17:48,000
You get the idea that in the womb, the fetus is very sensitive.

227
00:17:48,000 --> 00:17:55,000
And because we're dealing with fewer cells and a simpler organism, the effect that the mind will have on the fetus.

228
00:17:55,000 --> 00:18:03,000
And it's a speculative, but it's easy to see how this could create states of, of, you know, change in the physical body.

229
00:18:03,000 --> 00:18:11,000
From a Buddhist perspective, I'm sure the materialist, people who are not, who think this is superstition.

230
00:18:11,000 --> 00:18:15,000
There's a sense that that's probably not the case.

231
00:18:15,000 --> 00:18:19,000
That the mind is simply an epiphenomenon and so on.

232
00:18:19,000 --> 00:18:28,000
Anyway, not to get too off track, but how these things happen has a lot to do with the interactions between the body and the mind.

233
00:18:28,000 --> 00:18:33,000
It's not so much that the mind affects the body, right?

234
00:18:33,000 --> 00:18:37,000
Because that would be talking about a mind and a body. You see, it's different.

235
00:18:37,000 --> 00:18:44,000
The mind is not a thing that pokes at the body or turns switches on or something.

236
00:18:44,000 --> 00:18:50,000
The mind and the body are concepts that are a part of the, the same ultimate reality.

237
00:18:50,000 --> 00:18:55,000
And so if you just talk about the body, you're only talking about part of the picture and you miss things.

238
00:18:55,000 --> 00:18:59,000
You don't understand or can't explain how certain things happen.

239
00:18:59,000 --> 00:19:01,000
How do, how does sickness come about?

240
00:19:01,000 --> 00:19:05,000
They can look and see genetics, but genetics doesn't explain everything.

241
00:19:05,000 --> 00:19:07,000
Some of it, absolutely.

242
00:19:07,000 --> 00:19:09,000
But there's more to it than that.

243
00:19:09,000 --> 00:19:15,000
And the mind plays a part.

244
00:19:15,000 --> 00:19:26,000
The second one, the second one is conceit.

245
00:19:26,000 --> 00:19:35,000
Concedent Buddhism is the same as what in the West we might call self-esteem.

246
00:19:35,000 --> 00:19:39,000
And it's often seen again in a positive light.

247
00:19:39,000 --> 00:19:42,000
You should have high self-esteem.

248
00:19:42,000 --> 00:19:50,000
If you have a self-esteem issue, it's usually not because you have too much self-esteem though.

249
00:19:50,000 --> 00:19:54,000
Sometimes people use it that way.

250
00:19:54,000 --> 00:19:57,000
Usually, high self-esteem is a good thing.

251
00:19:57,000 --> 00:19:59,000
And low self-esteem is a bad thing.

252
00:19:59,000 --> 00:20:02,000
And Buddhism, they're both bad because you're esteeming the self.

253
00:20:02,000 --> 00:20:07,000
You're creating value, giving a valued judgment to a concept, really.

254
00:20:07,000 --> 00:20:17,000
The idea of who you think you are, which is usually just a snapshot that is often highly inaccurate or moderately inaccurate.

255
00:20:17,000 --> 00:20:23,000
At the least.

256
00:20:23,000 --> 00:20:29,000
And conceit is in many ways more dangerous than something like anger.

257
00:20:29,000 --> 00:20:32,000
Anger is what causes great suffering.

258
00:20:32,000 --> 00:20:39,000
It's what propelled this queen to do that terrible thing, but it alone is usually not enough.

259
00:20:39,000 --> 00:20:43,000
Anger alone is not very powerful.

260
00:20:43,000 --> 00:20:51,000
Concedent views, the delusion, even just simple ignorance, are the scaffold that allow it to grow.

261
00:20:51,000 --> 00:20:53,000
They support it.

262
00:20:53,000 --> 00:21:00,000
You can be angry and let it come and let it go and realize that you're angry.

263
00:21:00,000 --> 00:21:06,000
But if you're conceited, if someone does something that makes you angry and you believe,

264
00:21:06,000 --> 00:21:11,000
I don't deserve to be treated that way.

265
00:21:11,000 --> 00:21:13,000
Let's conceit.

266
00:21:13,000 --> 00:21:25,000
And that is what not only makes you act, but if you're going to act, it's what propels you and gives you the confidence to act with a whole heart.

267
00:21:25,000 --> 00:21:30,000
Act with complete conviction.

268
00:21:30,000 --> 00:21:32,000
I don't deserve this.

269
00:21:32,000 --> 00:21:35,000
Arrogance conceit.

270
00:21:35,000 --> 00:21:37,000
Same goes with low self esteem.

271
00:21:37,000 --> 00:21:42,000
If you have low self esteem, you believe you deserve bad things to happen.

272
00:21:42,000 --> 00:21:48,000
And so you make no effort to better yourself.

273
00:21:48,000 --> 00:21:52,000
Make no effort to help to change situations.

274
00:21:52,000 --> 00:21:55,000
A steam is not a good thing.

275
00:21:55,000 --> 00:22:03,000
This is why the idea of being worthless is somehow in some ways useful to think about.

276
00:22:03,000 --> 00:22:08,000
The idea that we have no worth, no value.

277
00:22:08,000 --> 00:22:13,000
And it doesn't mean that we are, and it could be problematic if you thought of it as,

278
00:22:13,000 --> 00:22:17,000
if you think you're worthless in a sense of having low self esteem,

279
00:22:17,000 --> 00:22:20,000
mostly that's how we would look at that statement.

280
00:22:20,000 --> 00:22:24,000
But beyond, it goes beyond that. It simply means you can't put a value on things.

281
00:22:24,000 --> 00:22:30,000
It's not that we're priceless or beyond with value beyond measure.

282
00:22:30,000 --> 00:22:37,000
It's that putting worth on things is problematic to say the least.

283
00:22:37,000 --> 00:22:40,000
Conceit is a value judgment about yourself.

284
00:22:40,000 --> 00:22:44,000
It creates and enforces the idea of self and triggers,

285
00:22:44,000 --> 00:22:50,000
greed and anger. I deserve this, so you go after it even more than if you just wanted it.

286
00:22:50,000 --> 00:22:54,000
If you believe you deserve it, you'll often do things to get things,

287
00:22:54,000 --> 00:23:02,000
to get the object of your desire that you wouldn't do with simple greed or desire.

288
00:23:02,000 --> 00:23:07,000
So a great cause of suffering and probably a big factor in this queen,

289
00:23:07,000 --> 00:23:12,000
the acts of this queen.

290
00:23:12,000 --> 00:23:14,000
The third part,

291
00:23:14,000 --> 00:23:20,000
tongue, nama, rupas, mim, asan, jamana.

292
00:23:20,000 --> 00:23:24,000
Asanja, asanja, not clinging,

293
00:23:24,000 --> 00:23:30,000
or not having an interesting word, it means to be hung.

294
00:23:30,000 --> 00:23:38,000
To be hung on, hung to, hung by, hung on.

295
00:23:38,000 --> 00:23:45,000
So in English we would say hung up on, hung up on nama, rupa.

296
00:23:45,000 --> 00:23:49,000
Which in fact is not, I don't think possible.

297
00:23:49,000 --> 00:23:52,000
You don't get caught up on nama, rupa.

298
00:23:52,000 --> 00:23:55,000
I don't mean to criticize what the Buddha said,

299
00:23:55,000 --> 00:23:58,000
but it seems to me there's something deeper here that what he's,

300
00:23:58,000 --> 00:24:00,000
he may be saying,

301
00:24:00,000 --> 00:24:02,000
he seems to be saying,

302
00:24:02,000 --> 00:24:07,000
well, he's pointing out that these things that you're hung up on,

303
00:24:07,000 --> 00:24:10,000
this body, that you will refuse to show,

304
00:24:10,000 --> 00:24:12,000
if you're covering yourself up,

305
00:24:12,000 --> 00:24:15,000
that we're very careful to put on makeup,

306
00:24:15,000 --> 00:24:17,000
and comb our hair,

307
00:24:17,000 --> 00:24:21,000
and I hear one of the most unpleasant things for people,

308
00:24:21,000 --> 00:24:25,000
is during this quarantinedness to not get a haircut,

309
00:24:25,000 --> 00:24:27,000
which is a bit surprising,

310
00:24:31,000 --> 00:24:35,000
having to close ourselves, having to wash ourselves,

311
00:24:35,000 --> 00:24:38,000
and so on. These things that were hung up on,

312
00:24:38,000 --> 00:24:42,000
they're only nama and rupa.

313
00:24:42,000 --> 00:24:44,000
And what I say that is because if you see,

314
00:24:44,000 --> 00:24:48,000
if and when you see reality as just nama and rupa,

315
00:24:48,000 --> 00:24:54,000
there's nothing to get hung up about.

316
00:24:54,000 --> 00:25:00,000
There's nothing to hang yourself on.

317
00:25:00,000 --> 00:25:03,000
Nama means the mental aspects,

318
00:25:03,000 --> 00:25:05,000
when you have the stomach rises,

319
00:25:05,000 --> 00:25:07,000
the knowing of the rising, that's nama,

320
00:25:07,000 --> 00:25:10,000
that knowing arises and ceases,

321
00:25:10,000 --> 00:25:12,000
the rising of the stomach,

322
00:25:12,000 --> 00:25:14,000
the falling of the stomach,

323
00:25:14,000 --> 00:25:16,000
the movement of the foot,

324
00:25:16,000 --> 00:25:19,000
even the sound of my voice, that's rupa.

325
00:25:19,000 --> 00:25:22,000
The sound doesn't know anything.

326
00:25:22,000 --> 00:25:25,000
The sound is just a thing,

327
00:25:25,000 --> 00:25:29,000
a phenomenon, it's there.

328
00:25:29,000 --> 00:25:34,000
If you're focused on something else,

329
00:25:34,000 --> 00:25:36,000
as you're watching television or reading a book,

330
00:25:36,000 --> 00:25:39,000
someone might say something and you don't even hear it.

331
00:25:39,000 --> 00:25:42,000
But the sound is there.

332
00:25:42,000 --> 00:25:45,000
That's rupa, nama and rupa.

333
00:25:45,000 --> 00:25:47,000
That's all it is.

334
00:25:47,000 --> 00:25:49,000
That's all everything is.

335
00:25:49,000 --> 00:25:54,000
That's all we can really, truly know.

336
00:25:54,000 --> 00:25:57,000
Everything else that we think we are aware of

337
00:25:57,000 --> 00:26:00,000
is the experience, this whole experience around us.

338
00:26:00,000 --> 00:26:05,000
That's all arising in the mind.

339
00:26:05,000 --> 00:26:08,000
Tung nama rupa, smigas, and jamanan.

340
00:26:08,000 --> 00:26:11,000
So someone who is not hung up on nama and rupa,

341
00:26:11,000 --> 00:26:15,000
through absolutely through meditation practice.

342
00:26:15,000 --> 00:26:18,000
A kinchenang, na nutapan, na nutapan,

343
00:26:18,000 --> 00:26:21,000
na nutapan tindukha,

344
00:26:21,000 --> 00:26:24,000
dukha, all kinds of suffering.

345
00:26:24,000 --> 00:26:29,000
A kinchenang dukha,

346
00:26:29,000 --> 00:26:33,000
not even the slightest sufferings.

347
00:26:33,000 --> 00:26:36,000
Anupata, patanti, not even the slightest sufferings

348
00:26:36,000 --> 00:26:42,000
before such a person.

349
00:26:42,000 --> 00:26:44,000
So it's again not so much about,

350
00:26:44,000 --> 00:26:48,000
not at all about the experiences that we have.

351
00:26:48,000 --> 00:26:51,000
It's about our reactions to them.

352
00:26:51,000 --> 00:26:54,000
We interpret them.

353
00:26:54,000 --> 00:26:57,000
How they create so much.

354
00:26:57,000 --> 00:27:01,000
So much of what we take for granted as a part of who we are,

355
00:27:01,000 --> 00:27:05,000
a part of the world around us.

356
00:27:05,000 --> 00:27:08,000
It's just all fabrication in our mind.

357
00:27:08,000 --> 00:27:11,000
All the relationships we have with people,

358
00:27:11,000 --> 00:27:13,000
say what you will, the good, bad,

359
00:27:13,000 --> 00:27:18,000
without putting any value in judgment on them or anything like that.

360
00:27:18,000 --> 00:27:23,000
They are absolutely entirely all in our minds.

361
00:27:23,000 --> 00:27:26,000
They're not real.

362
00:27:26,000 --> 00:27:29,000
Our parents, our children, our friends, our family,

363
00:27:29,000 --> 00:27:34,000
our loved ones, our societies, our economies,

364
00:27:34,000 --> 00:27:40,000
our social lives, our belongings,

365
00:27:40,000 --> 00:27:45,000
even our own bodies and all the things they're made up of.

366
00:27:45,000 --> 00:27:51,000
Our skin that we take so much pride in our teeth,

367
00:27:51,000 --> 00:27:54,000
that we whiten and clean,

368
00:27:54,000 --> 00:28:01,000
and our nails that we file and clean and buff and paint,

369
00:28:01,000 --> 00:28:06,000
even we paint these ugly nails.

370
00:28:06,000 --> 00:28:10,000
Well, these nails does not be judgmental.

371
00:28:10,000 --> 00:28:13,000
Our hair, what we do to our hair,

372
00:28:13,000 --> 00:28:22,000
is smelly oily plant that's planted in the blood and oil of our scalp.

373
00:28:22,000 --> 00:28:33,000
What we do to this strange and organic part of our body,

374
00:28:33,000 --> 00:28:37,000
all of this is just in our minds.

375
00:28:37,000 --> 00:28:41,000
The underlying reality, which is very hard to see if you don't have

376
00:28:41,000 --> 00:28:44,000
such a wonderful teacher like the Buddha,

377
00:28:44,000 --> 00:28:49,000
such an accomplished and enlightened teacher.

378
00:28:49,000 --> 00:28:56,000
The underlying reality is just experienced.

379
00:28:56,000 --> 00:28:59,000
So, that's the demo part of her tonight.

380
00:28:59,000 --> 00:29:15,000
Thank you all for listening.

