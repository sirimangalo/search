1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damopada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with verse 165, which reads as follows.

3
00:00:13,000 --> 00:00:24,000
Atana, he katang bhapang, atana, sanki-dizati, atana, a katang bhapang, atana bhapang, atana bhasu jati,

4
00:00:24,000 --> 00:00:41,000
sudhi, asudhi, pachatang, nanyo, anyang, we sow dayi, which means evil is done by oneself,

5
00:00:41,000 --> 00:00:51,000
by oneself is one defiled, by the self evil is left undone,

6
00:00:51,000 --> 00:01:00,000
by the self indeed, one is purified.

7
00:01:00,000 --> 00:01:11,000
Purity and impurity are individual, sudhi, asudhi, pachatang,

8
00:01:11,000 --> 00:01:18,000
purity and impurity are private, nanyo, anyang, we sow dayi,

9
00:01:18,000 --> 00:01:24,000
one cannot purify another.

10
00:01:24,000 --> 00:01:35,000
One of the more famous Damopada verses.

11
00:01:35,000 --> 00:01:38,000
There was a monk I used to know who,

12
00:01:38,000 --> 00:01:43,000
every time he gave a damataki, would start with his verse.

13
00:01:43,000 --> 00:01:49,000
Sudhi, sudhi, sudhi, sudhi, pachatang, nanyo, anyong, we sow dayi,

14
00:01:49,000 --> 00:01:56,000
which is kind of funny because a monk had a gun and he ended up chasing me through a forest,

15
00:01:56,000 --> 00:02:08,000
one night screaming at the top of his tongues, stories.

16
00:02:08,000 --> 00:02:13,000
So we have a story behind this one.

17
00:02:13,000 --> 00:02:20,000
The story is of julakala, which is kind of interesting.

18
00:02:20,000 --> 00:02:25,000
It's the same as the story of Mahakala.

19
00:02:25,000 --> 00:02:31,000
So if any of you happen to remember, I think it was verse 161 or something,

20
00:02:31,000 --> 00:02:35,000
recently we had the story of Mahakala,

21
00:02:35,000 --> 00:02:43,000
who was a man who seemed to have been innocently blamed for having stolen some goods.

22
00:02:43,000 --> 00:02:49,000
He was listening to the Damai thing at the monastery on his way back.

23
00:02:49,000 --> 00:02:57,000
Some thieves were, I don't know, he was bathing or something.

24
00:02:57,000 --> 00:03:05,000
Anyway, some thieves came and caught him there or passed by him.

25
00:03:05,000 --> 00:03:13,000
They were being chased by the authority and they dropped their loot by him.

26
00:03:13,000 --> 00:03:20,000
And the authorities thought it was him and so they beat him to death.

27
00:03:20,000 --> 00:03:23,000
He wasn't a monk, this is a layman.

28
00:03:23,000 --> 00:03:28,000
So that was our last story in this story. The same thing happened.

29
00:03:28,000 --> 00:03:37,000
Only some servant girls.

30
00:03:37,000 --> 00:03:40,000
If any of you are going, reading along with the English text,

31
00:03:40,000 --> 00:03:43,000
it says cortisans, like prostitutes or something,

32
00:03:43,000 --> 00:03:46,000
but it's not at all what the poly says.

33
00:03:46,000 --> 00:03:53,000
It says some slaves with servants with pots, you know, with water pots.

34
00:03:53,000 --> 00:03:58,000
From their way to fill the water, fill their pots, just servant girls.

35
00:03:58,000 --> 00:04:04,000
Saw them beating this man and they knew he was innocent and they somehow,

36
00:04:04,000 --> 00:04:08,000
I guess maybe they knew where he had come from.

37
00:04:08,000 --> 00:04:14,000
And they said stop, stop this man wasn't, he wasn't the one who stole those,

38
00:04:14,000 --> 00:04:19,000
stole your belongings.

39
00:04:19,000 --> 00:04:26,000
And somehow they convinced them and they stopped beating this innocent man.

40
00:04:26,000 --> 00:04:38,000
And so the trailer goes back to the monastery and talks to the monks

41
00:04:38,000 --> 00:04:41,000
and he says, wow, it's amazing. These slave girls,

42
00:04:41,000 --> 00:04:46,000
these servant girls saved me. I would have been dead if it weren't for them.

43
00:04:46,000 --> 00:04:49,000
They went and told the Buddha in this, they were thinking, I guess,

44
00:04:49,000 --> 00:04:51,000
thinking in relation to Mahakala.

45
00:04:51,000 --> 00:05:00,000
It's an interesting how, how he would have died.

46
00:05:00,000 --> 00:05:04,000
He should have died in a very same way, but not through karma,

47
00:05:04,000 --> 00:05:08,000
but through the acts of these other, these servant girls.

48
00:05:08,000 --> 00:05:11,000
I think it's because of them, not because of karma.

49
00:05:11,000 --> 00:05:13,000
I guess the implication.

50
00:05:13,000 --> 00:05:18,000
And the Buddha says, his life was indeed saved,

51
00:05:18,000 --> 00:05:25,000
both by the intercession of these servant girls.

52
00:05:25,000 --> 00:05:30,000
Kumba dasa yu jiwa nisaya, Atanotja.

53
00:05:30,000 --> 00:05:35,000
Also because, you know, Atanotja, Akkarna bhavina.

54
00:05:35,000 --> 00:05:40,000
Also because, because of these girls, but also because he had not done,

55
00:05:40,000 --> 00:05:45,000
he had not done it. He was not guilty.

56
00:05:45,000 --> 00:05:49,000
Tiwitamam.

57
00:05:49,000 --> 00:05:52,000
And there's a double meaning, of course, there.

58
00:05:52,000 --> 00:05:57,000
It's not just that he was not guilty of having committed the crime,

59
00:05:57,000 --> 00:06:02,000
but he didn't have any basis of guilt by which he should be beaten to death.

60
00:06:02,000 --> 00:06:07,000
Sometimes perhaps he might say he was beaten,

61
00:06:07,000 --> 00:06:13,000
so he had some karma that caused him to be beaten,

62
00:06:13,000 --> 00:06:19,000
but not to death.

63
00:06:19,000 --> 00:06:24,000
And then he spoke this verse, so it's a rather short story.

64
00:06:24,000 --> 00:06:26,000
But again, we're back to karma.

65
00:06:26,000 --> 00:06:31,000
And the reason why there's a lot of stories here on karma is because this is the Atawaga,

66
00:06:31,000 --> 00:06:35,000
the chapter on the self.

67
00:06:35,000 --> 00:06:47,000
And so the stories are all about self-reliance and self-responsibility.

68
00:06:47,000 --> 00:06:51,000
You can't blame someone else for your problems.

69
00:06:51,000 --> 00:06:57,000
And you can look to someone else for your salvation, which is basically this verse puts it quite well.

70
00:06:57,000 --> 00:07:19,000
So there's a couple of lessons I think we can take, especially trying to focus on specifically on meditation practice.

71
00:07:19,000 --> 00:07:34,000
The first is to talk about this idea of somehow being saved from the results,

72
00:07:34,000 --> 00:07:48,000
or being saved from disaster by simply not having committed certain karma's that would lead to such a disaster.

73
00:07:48,000 --> 00:08:02,000
And so I get lots of questions about karma and about this idea that it's just belief and the idea that it sounds very magical.

74
00:08:02,000 --> 00:08:10,000
And the idea of how we want to think about karma is that it's,

75
00:08:10,000 --> 00:08:23,000
or I want to think about the reason why we get what we get is that it's random or chaotic with no pattern.

76
00:08:23,000 --> 00:08:32,000
And I think to some extent the Buddha acknowledges that it's chaotic or a better word might be complicated.

77
00:08:32,000 --> 00:08:47,000
And insanely complicated. I think that's more scientific as not to talk about it as a more chaotic but complicated complex.

78
00:08:47,000 --> 00:08:57,000
But there's no denying that what we experience is based on causes of various sorts.

79
00:08:57,000 --> 00:09:08,000
I always think back to the tsunami, the great tsunami that hit Southeast Asia.

80
00:09:08,000 --> 00:09:14,000
Because when I was in Thailand when it hit and so many stories we heard,

81
00:09:14,000 --> 00:09:17,000
then all the stories were different.

82
00:09:17,000 --> 00:09:23,000
And so there's a question people always ask about, how can you talk about karma

83
00:09:23,000 --> 00:09:28,000
when you're dealing with something like a tsunami where it hits everyone and discriminate.

84
00:09:28,000 --> 00:09:33,000
The stories aren't indiscriminate, yes a lot of people lost their lives,

85
00:09:33,000 --> 00:09:44,000
but the way they lost their lives and the distinction between those who lost their lives and those who were saved.

86
00:09:44,000 --> 00:09:55,000
First of all, we admit that there is the cause of the tsunami. It wasn't just random people.

87
00:09:55,000 --> 00:10:06,000
It wasn't very complicated in that since there was a clear cause and effect relationship between the tsunami and death.

88
00:10:06,000 --> 00:10:15,000
And so there's a cause, we have cause and effect.

89
00:10:15,000 --> 00:10:20,000
We also, to some extent, acknowledge the cause and effect relationship of the mind.

90
00:10:20,000 --> 00:10:28,000
And this is where the meditation practice takes the forefront.

91
00:10:28,000 --> 00:10:36,000
Most of our meditation practice isn't interested in past lives, right?

92
00:10:36,000 --> 00:10:49,000
For the most part, our practice of mindfulness were only really interested in something that we can clearly see as a cause and effect relationship here and now.

93
00:10:49,000 --> 00:10:57,000
And from the past leading up to now, the habits that we've cultivated,

94
00:10:57,000 --> 00:11:01,000
we aren't just magically the way we are and we aren't permanently the way we are.

95
00:11:01,000 --> 00:11:13,000
Who we are is based on the causes that we have put in place to lead us here.

96
00:11:13,000 --> 00:11:29,000
You know, we're constantly striving after some certain thing, be it sensual pleasure or be it a goal or an ambition.

97
00:11:29,000 --> 00:11:40,000
It's because of that, that we are in the present time partial or addicted to certain things.

98
00:11:40,000 --> 00:11:46,000
Well, so why we are restless and uncomfortable and we don't care what we want.

99
00:11:46,000 --> 00:11:50,000
So the very basis of karma is in this.

100
00:11:50,000 --> 00:12:01,000
And this is why in meditation practice it's actually quite important for what to have an understanding of karma and why karma is thought of as a very important stage.

101
00:12:01,000 --> 00:12:11,000
It's this sort of karma that is to, if you cultivate certain habits they're going to have consequences.

102
00:12:11,000 --> 00:12:21,000
Moreover, if you perform any ethically charged act of volition or inclination,

103
00:12:21,000 --> 00:12:23,000
it's going to have results.

104
00:12:23,000 --> 00:12:29,000
If you want something you're going to be stressed and disappointed when you're not able to get what you want.

105
00:12:29,000 --> 00:12:38,000
If you dislike something you're going to be stressed and disappointed when you get it.

106
00:12:38,000 --> 00:12:46,000
Learning that our responses to experience are important.

107
00:12:46,000 --> 00:12:50,000
Have a weight to them. How we respond to something.

108
00:12:50,000 --> 00:13:02,000
We respond by liking, disliking, if we respond with worry, if we respond with fear, all of these have consequences.

109
00:13:02,000 --> 00:13:05,000
So we acknowledge these cause and effect relationships.

110
00:13:05,000 --> 00:13:10,000
What we really have a problem with isn't so much I think past life karma.

111
00:13:10,000 --> 00:13:16,000
It's just getting past this idea that the mind and the body are separate.

112
00:13:16,000 --> 00:13:36,000
Because if the mind is separate from the body then it's going to continue after death and that manner of continuing is going to depend very much upon the state of mind and the habits that exist at the moment of death.

113
00:13:36,000 --> 00:13:49,000
And given that we know the place of birth is a womb if we were to accept that it does actually happen.

114
00:13:49,000 --> 00:14:05,000
Then we're dealing with the formation of a fetus which is going to be influenced by many things but influenced by the mind that continues on into the fetus.

115
00:14:05,000 --> 00:14:12,000
Also the choosing, the forming of the volition, the wish to be born here or born there.

116
00:14:12,000 --> 00:14:17,000
It's going to be very much dependent on the state of mind.

117
00:14:17,000 --> 00:14:34,000
And so it's not that called the concept of past life karma is somehow a categorically different concept from present life cause and effect.

118
00:14:34,000 --> 00:14:42,000
But it's really an extension and an extrapolation of it.

119
00:14:42,000 --> 00:14:52,000
So if you want to understand why we are the way we are here and now and where our acts are going to lead us in future lives.

120
00:14:52,000 --> 00:15:03,000
Based on past lives in future lives you really only have to look at the state of mind and how states of mind and the present moment have consequences.

121
00:15:03,000 --> 00:15:20,000
How someone who is miserly stingy as a result is going to be unable to, unable unwilling is going to have this corrupt state of mind where they're not able to enjoy.

122
00:15:20,000 --> 00:15:29,000
They're constantly worried about having to share and concerned and jealous often.

123
00:15:29,000 --> 00:15:33,000
I'm obsessed about their wealth, not wanting to spend it.

124
00:15:33,000 --> 00:15:37,000
I mean the funny thing about people who are miserly is they don't ever enjoy life.

125
00:15:37,000 --> 00:15:49,000
I think a person who saves and scrimps and saves their money up would be happy because they have all this money but it's a corruption of mind.

126
00:15:49,000 --> 00:15:58,000
The only way to be truly happy, truly happy is to be able to let go completely and not be worried.

127
00:15:58,000 --> 00:16:01,000
It's not being worried concerned about whether you have anything.

128
00:16:01,000 --> 00:16:10,000
That's really the hardest thing to do.

129
00:16:10,000 --> 00:16:22,000
But the point being that our actions change us and they corrupt us generally in the same way that we've expressed them.

130
00:16:22,000 --> 00:16:29,000
So if you hurt others, you stand to be hurt yourself simply because of the way you set up your mind.

131
00:16:29,000 --> 00:16:42,000
I mean one good example of this is how people who are angry and mean and unpleasant weather tend to be negligent in regards to their own care, not purposefully.

132
00:16:42,000 --> 00:16:48,000
But they're the kind of people who move quickly, who move without thought, who don't have.

133
00:16:48,000 --> 00:16:57,000
There really is this kind of a mirror effect whereby the way you treat others has an effect on how you treat yourself.

134
00:16:57,000 --> 00:16:59,000
We don't realize this.

135
00:16:59,000 --> 00:17:07,000
I mean this isn't immediately intuitive, but there is a mirror effect.

136
00:17:07,000 --> 00:17:10,000
Moreover, there's an effect on others.

137
00:17:10,000 --> 00:17:23,000
In this life, and if you are to suspend your doubt, this horrible doubt that people have, that it's actually possible to live after the death of a body.

138
00:17:23,000 --> 00:17:37,000
You're able to suspend that.

139
00:17:37,000 --> 00:17:57,000
You're able to accept the idea of the continuation.

140
00:17:57,000 --> 00:18:10,000
Then you can appreciate the how our relationships are affected by our actions.

141
00:18:10,000 --> 00:18:21,000
And you can appreciate how not only our mind continues, but the minds of those people who we hurt and the minds of those people who we help.

142
00:18:21,000 --> 00:18:31,000
And so a sort of a magic takes over. If you open up your paradigm from birth to death and that's it.

143
00:18:31,000 --> 00:18:46,000
And everything happens from here to there. If you open it up, then you have to start to create a theory and a framework for how this works and how minds interact with other minds.

144
00:18:46,000 --> 00:18:58,000
Even when they've lost their ability to remember, because the brain makes memory very easy, it provides us reminders.

145
00:18:58,000 --> 00:19:04,000
It's like sticky notes. The brain is like having sticky notes. It's easy to find.

146
00:19:04,000 --> 00:19:08,000
The brain will always give you feedback and say, oh yeah, that's this.

147
00:19:08,000 --> 00:19:20,000
It's not that the brain contains memories. It contains what are the mnemonic devices of a sort, symbols, imagery.

148
00:19:20,000 --> 00:19:30,000
It's something and a stimuli, stimulus that reminds us of something.

149
00:19:30,000 --> 00:19:39,000
How we use mnemonic devices in the first place is because it's easier to store. Something that's a simple concept.

150
00:19:39,000 --> 00:19:47,000
And then you can bring it back, or how we store acronyms and that kind of thing.

151
00:19:47,000 --> 00:19:52,000
All sorts of mnemonic devices.

152
00:19:52,000 --> 00:20:07,000
So that's that's lost, that immediate filing system. It's like losing your smartphone and then you don't know your, you have to think of your schedule from the back, from the top of your head, off the top of your head.

153
00:20:07,000 --> 00:20:16,000
So we lose a lot of the memories with the people, but there's some kind of memory that's still there.

154
00:20:16,000 --> 00:20:24,000
And of course, all these memories can be gained through meditation practice. Memories of past lives can be gained, but it's not easy.

155
00:20:24,000 --> 00:20:30,000
What's much easier is this sort of a sense of people and it's sometimes wrong.

156
00:20:30,000 --> 00:20:44,000
But it's if you accept that there's a continuation, it's not hard to understand how these guys who are beating this innocent man

157
00:20:44,000 --> 00:20:55,000
would be easily persuaded that he was innocent because there's not a feeling of any sort of memory of him having done anything bad to them in the past, whereas with the other guy.

158
00:20:55,000 --> 00:21:00,000
I guess there was some past life karma where he had actually hurt these people in the past.

159
00:21:00,000 --> 00:21:11,000
And so, you know, it seemed like the other guy was innocent, but in fact, he was receiving, it's not even a matter of being innocent or guilty.

160
00:21:11,000 --> 00:21:21,000
And this is another problem people have with karma is that it seems to be, it seems to be victim blaming, right?

161
00:21:21,000 --> 00:21:33,000
This whole verse here that someone is suffering, it's because of their own deeds.

162
00:21:33,000 --> 00:21:44,000
Someone is born poor or born crippled, born this way or born that. If someone is born rich, well, they deserve to be rich, right?

163
00:21:44,000 --> 00:22:00,000
It's highly convenient. It's an interesting argument that, you know, this whole line of argument that the law of karma is convenient or it's, it serves a purpose and a various sort of ulterior motive purpose.

164
00:22:00,000 --> 00:22:08,000
It's an interesting argument because it doesn't really matter. It's not like we present the law of karma and like, look at how wonderful this is.

165
00:22:08,000 --> 00:22:17,000
We more presented as look at how horrible this is. Look at how scary and dangerous this is.

166
00:22:17,000 --> 00:22:32,000
Yes, it is used, I think, to create social order and can thereby be perverted and corrupted by people in power to convince poor people that they should just accept their lot in life.

167
00:22:32,000 --> 00:22:38,000
Of course, that's an extrapolation that has nothing to do with law of karma.

168
00:22:38,000 --> 00:22:49,000
In fact, if you accept that you are poor, for example, because you were miserly in a past life, which I think is totally plausible.

169
00:22:49,000 --> 00:22:57,000
I mean, understanding that it's more complicated than that generally.

170
00:22:57,000 --> 00:23:10,000
If anything, that should stimulate one to work hard and to even be more concerned for rich people who are acting stingy and miserly, right?

171
00:23:10,000 --> 00:23:36,000
It's not a reason to say, oh, okay, all you rich people keep your wealth because you deserve it.

172
00:23:36,000 --> 00:23:52,000
It's meant to provide a single system. Past life karma is not as to reiterate, it's not categorically different from present life karma in the sense of what we can see through meditation practice.

173
00:23:52,000 --> 00:24:02,000
If you want to understand the law of past life karma and future karma, again, just understand it as an extension of what the Buddha is saying here.

174
00:24:02,000 --> 00:24:07,000
The purity and impurity, that's what it's all about.

175
00:24:07,000 --> 00:24:16,000
It doesn't matter past life, future life, present life, between lives or within a single life.

176
00:24:16,000 --> 00:24:22,000
It all comes down to the purity and impurity that we can see through meditation practice.

177
00:24:22,000 --> 00:24:25,000
When you meditate, you can see all the problems.

178
00:24:25,000 --> 00:24:47,000
But one thing that, if you doubt the meditation practice, it's common for people to come and doubt the practice, whether it's useful, helpful, whether one is capable of performing the required practice, whether it's capable of it.

179
00:24:47,000 --> 00:24:58,000
But one thing that's very hard to deny is what's wrong, the problem that we're trying to address, which is really, I think, should be a great reassurance.

180
00:24:58,000 --> 00:25:09,000
People are, we get a lot of complaints that meditation doesn't work because your bad problems just keep coming back and back and back.

181
00:25:09,000 --> 00:25:17,000
You practice and practice and you've still got all these problems, but you're missing the real point.

182
00:25:17,000 --> 00:25:24,000
The most important point for a beginner minute here is that you see the problem.

183
00:25:24,000 --> 00:25:26,000
That's really what karma is all about.

184
00:25:26,000 --> 00:25:31,000
It's about seeing that there is a problem.

185
00:25:31,000 --> 00:25:34,000
You don't get away with anything.

186
00:25:34,000 --> 00:25:43,000
Like I bring back Udi, I'm always thinking about Udi, he wasn't a great monk, but he was a great soul.

187
00:25:43,000 --> 00:25:53,000
He was a great being in his own way, and he said, Noah, I tell you, no one can escape their karma.

188
00:25:53,000 --> 00:26:01,000
He was very strong in this, and it was really always nice to listen to him talk.

189
00:26:01,000 --> 00:26:08,000
He passed away, he got rectal cancer, there's an awful way to die.

190
00:26:08,000 --> 00:26:16,000
Speaking of karma, perhaps.

191
00:26:16,000 --> 00:26:23,000
This is an important point for us to understand that, yes, it's more complicated.

192
00:26:23,000 --> 00:26:26,000
There's lots of factors and who really knows how and all works.

193
00:26:26,000 --> 00:26:28,000
It's such a complicated system.

194
00:26:28,000 --> 00:26:37,000
But there is no doubt that we have problems inside and that they are a problem.

195
00:26:37,000 --> 00:26:51,000
That these states of mind and habits are causing suffering and will continue to cause suffering.

196
00:26:51,000 --> 00:26:57,000
Now it's of course neat in stories like this to think about how over the long term that can actually work

197
00:26:57,000 --> 00:26:58,000
into the next life.

198
00:26:58,000 --> 00:27:12,000
I mean, that's really a neat concept, and it's really scary, but quite interesting to imagine that that might actually be true.

199
00:27:12,000 --> 00:27:20,000
But there's such noise at that point that, again, it's really hard to pinpoint exactly what it is that's causing what

200
00:27:20,000 --> 00:27:29,000
why certain people died in the tsunami and certain other people survive seemingly miraculously.

201
00:27:29,000 --> 00:27:33,000
There could be many reasons, obviously.

202
00:27:33,000 --> 00:27:44,000
But there is one reason and one aspect of the nature of reality, and that is the commonyama, the orderliness of karma.

203
00:27:44,000 --> 00:27:47,000
That's what this talks about.

204
00:27:47,000 --> 00:27:52,000
In fact, it says something a little more, that's the sort of this idea behind the story.

205
00:27:52,000 --> 00:27:59,000
But the verse itself, as usual, is not completely related to the story.

206
00:27:59,000 --> 00:28:06,000
I think sometimes it's easy to read these stories as just being made up to fit the verses loosely.

207
00:28:06,000 --> 00:28:12,000
But I think it's equally plausible to understand, yes, stories were exaggerated.

208
00:28:12,000 --> 00:28:19,000
That in each case, the Buddha wasn't really all that interested in what was being talked about, but he wanted to give a lesson.

209
00:28:19,000 --> 00:28:22,000
That was actually useful for the people listening.

210
00:28:22,000 --> 00:28:25,000
That was usually related, in some way.

211
00:28:25,000 --> 00:28:33,000
That's common for a teacher to not actually answer a question directly, but to give an answer that's actually useful.

212
00:28:33,000 --> 00:28:38,000
Green directing back towards important.

213
00:28:38,000 --> 00:28:43,000
The big point of this verse is that you have to purify yourself.

214
00:28:43,000 --> 00:28:50,000
This is another aspect of the teaching and an important point to understand about the practice that someone else can't save.

215
00:28:50,000 --> 00:28:52,000
Someone else can't harm you.

216
00:28:52,000 --> 00:28:54,000
There are two things here.

217
00:28:54,000 --> 00:28:57,000
We blame other people for our suffering, right?

218
00:28:57,000 --> 00:29:01,000
Big part of the problem is how we blame others.

219
00:29:01,000 --> 00:29:08,000
The other side is that we look to others, and people look to my videos to try and save them.

220
00:29:08,000 --> 00:29:11,000
Let's save them, but try and help them.

221
00:29:11,000 --> 00:29:18,000
Beyond what they're able to do, I mean a lot of the questions some of you have seen are, how do I solve this problem?

222
00:29:18,000 --> 00:29:23,000
It's not really something that I can solve for you.

223
00:29:23,000 --> 00:29:38,000
I get people on the internet asking for advice, I guess, general advice on how to live their lives.

224
00:29:38,000 --> 00:29:42,000
It's not really how it works.

225
00:29:42,000 --> 00:29:50,000
I can give you the tools by which you can find answers yourself, but it's not really for me to give you answers.

226
00:29:50,000 --> 00:29:58,000
It's not really how it's not how purification, that's not how suffering works and happiness comes.

227
00:29:58,000 --> 00:30:02,000
It comes from purifying yourself.

228
00:30:02,000 --> 00:30:10,000
This is, of course, a feature of many religions looking to others.

229
00:30:10,000 --> 00:30:21,000
If you're reading the Mahabharata, how you destroy, I think it is lies, or marginalized as well, right?

230
00:30:21,000 --> 00:30:24,000
No, you destroy lies.

231
00:30:24,000 --> 00:30:26,000
I don't know.

232
00:30:26,000 --> 00:30:38,000
The good guys behave kind of badly, and then it appears that they're going to be sent to hell or something like that, but then God forgives them.

233
00:30:38,000 --> 00:30:48,000
In the end, God forgives them because they were doing what the order would God want to do, or Krishna, I guess, or whoever.

234
00:30:48,000 --> 00:30:54,000
Anyway, I'm oversimplifying it terribly, but this is a feature of theistic religions generally.

235
00:30:54,000 --> 00:31:02,000
God's plan, God's forgiveness, all you have to do is X, and you will be saved.

236
00:31:02,000 --> 00:31:13,000
The idea of being saved by someone else, putting your faith, putting your salvation in the hands of someone else.

237
00:31:13,000 --> 00:31:17,000
That is a very powerful movement.

238
00:31:17,000 --> 00:31:22,000
The Abrahamic religions are very much about putting your trust in someone else to save you.

239
00:31:22,000 --> 00:31:26,000
It's an important feature of Buddhism, and it's not like that.

240
00:31:26,000 --> 00:31:33,000
It's a distinction of, let's say, tarolada Buddhism, and most Buddhism.

241
00:31:33,000 --> 00:31:45,000
Let's say most Buddhism is about self liberation.

242
00:31:45,000 --> 00:31:52,000
Something we should keep in mind both as Buddhists, and more specifically as Buddhists meditative.

243
00:31:52,000 --> 00:32:03,000
So as Buddhists, you shouldn't think that these talks or that reading books or that following teachers is going to free from suffering.

244
00:32:03,000 --> 00:32:09,000
This is why meditation is emphasized so strongly.

245
00:32:09,000 --> 00:32:17,000
That's why so many Buddhists turn to actual practice, because that should be very clear from teachings like this.

246
00:32:17,000 --> 00:32:22,000
You're not going to find salvation in my words, or anyone's.

247
00:32:22,000 --> 00:32:24,000
Buddha is only pointing the way the Buddha is in.

248
00:32:24,000 --> 00:32:34,000
I cut the road to target, the target is only show the way.

249
00:32:34,000 --> 00:32:42,000
And for meditators, more specifically, when you're meditating, you can't rely on me,

250
00:32:42,000 --> 00:32:47,000
and you can't rely even on the technique. It's not magic.

251
00:32:47,000 --> 00:32:51,000
If you repeat these words to yourself, you become enlightened.

252
00:32:51,000 --> 00:33:00,000
It has to come from you, from your understanding, your awareness.

253
00:33:00,000 --> 00:33:06,000
I think this is why this is a problem, or people rely upon the technique,

254
00:33:06,000 --> 00:33:13,000
he said to walk and sit while I did all that walking and sitting, so I'm not going to become enlightened because I did what someone else told me.

255
00:33:13,000 --> 00:33:17,000
It's again, following someone else, they're going to save you.

256
00:33:17,000 --> 00:33:20,000
The practice isn't going to save you.

257
00:33:20,000 --> 00:33:25,000
The practice is providing you the tools by which you can save yourself.

258
00:33:25,000 --> 00:33:31,000
That's an important distinction, because it's not about walking for an hour.

259
00:33:31,000 --> 00:33:38,000
It's about having the awareness and the clarity of mind to know that this foot is moving,

260
00:33:38,000 --> 00:33:40,000
and then this foot is moving.

261
00:33:40,000 --> 00:33:45,000
The stomach is rising, the stomach is falling, and of course to know how the mind reacts to that,

262
00:33:45,000 --> 00:33:51,000
and to be there, and to be aware.

263
00:33:51,000 --> 00:33:53,000
It comes from yourself.

264
00:33:53,000 --> 00:33:56,000
You want to be pure, you want to be free.

265
00:33:56,000 --> 00:34:01,000
Certainly, all this Buddhism that ends up just being ritualistic about chanting,

266
00:34:01,000 --> 00:34:10,000
and pouring water, and shaking sticks, and all of that stuff is not going to save you.

267
00:34:10,000 --> 00:34:13,000
No one else can save you, nothing external to you can save you.

268
00:34:13,000 --> 00:34:17,000
You can't pray to the Buddha for salvation.

269
00:34:17,000 --> 00:34:22,000
It's not how it works.

270
00:34:22,000 --> 00:34:34,000
An important verse, generally, and just for the meaning that it gives about what Buddhism is all about,

271
00:34:34,000 --> 00:34:39,000
it must save yourself, and your actions and non-actions,

272
00:34:39,000 --> 00:34:51,000
they will determine your future happiness and salvation.

273
00:34:51,000 --> 00:34:52,000
There you go.

274
00:34:52,000 --> 00:34:54,000
That's the demo for tonight.

275
00:34:54,000 --> 00:34:56,000
Thank you all for tuning in.

276
00:34:56,000 --> 00:35:22,000
Have a good night.

