1
00:00:00,000 --> 00:00:03,000
Hello, and welcome back to Askamund.

2
00:00:03,000 --> 00:00:10,000
Today I'll be answering the question as to what a Buddhist does when someone in all dies,

3
00:00:10,000 --> 00:00:15,000
which is not directly related to the meditation practice,

4
00:00:15,000 --> 00:00:19,000
but as Buddhists and meditators in the Buddhist tradition,

5
00:00:19,000 --> 00:00:24,000
this is something that we experience that we have to deal with

6
00:00:24,000 --> 00:00:30,000
that for most of us will be on the extreme end,

7
00:00:30,000 --> 00:00:33,000
something that is quite difficult to deal with,

8
00:00:33,000 --> 00:00:35,000
and to come to terms with.

9
00:00:35,000 --> 00:00:40,000
So simply meditating in a way is not really sufficient.

10
00:00:40,000 --> 00:00:44,000
So there are things that we should talk about in regards to this.

11
00:00:44,000 --> 00:00:48,000
From a practical point of view, a Buddhist point of view,

12
00:00:48,000 --> 00:00:53,000
I would say there are three things that we should do

13
00:00:53,000 --> 00:00:58,000
that we should keep in mind, three duties as Buddhists or three

14
00:00:58,000 --> 00:01:01,000
points to keep in mind when a person we know dies.

15
00:01:01,000 --> 00:01:06,000
The first one is that we should never, ever grieve

16
00:01:06,000 --> 00:01:10,000
or think that grieving is somehow appropriate,

17
00:01:10,000 --> 00:01:13,000
somehow useful, somehow good.

18
00:01:13,000 --> 00:01:18,000
The reason why we grieve is because it's great suffering for us.

19
00:01:18,000 --> 00:01:22,000
You can think of a small child who has to stay with a babysitter

20
00:01:22,000 --> 00:01:27,000
the first time when their parents go away will often cry all night,

21
00:01:27,000 --> 00:01:32,000
thinking that they're not even thinking that their parents have left,

22
00:01:32,000 --> 00:01:37,000
but simply suffering terribly because of the clinging that they have,

23
00:01:37,000 --> 00:01:40,000
that they've developed as a small child for their parents.

24
00:01:40,000 --> 00:01:46,000
So this terrible suffering that goes on at loss,

25
00:01:46,000 --> 00:01:49,000
which really carries over and everything in our lives.

26
00:01:49,000 --> 00:01:51,000
People cry over many different things.

27
00:01:51,000 --> 00:01:54,000
Now, the point being that we tend to cling to people

28
00:01:54,000 --> 00:01:58,000
a lot more than we cling to any other object, really.

29
00:01:58,000 --> 00:02:03,000
It's that our other people are things that we can interact with

30
00:02:03,000 --> 00:02:08,000
that have great deep meaning for us in our lives,

31
00:02:08,000 --> 00:02:10,000
and so we cling to them greatly.

32
00:02:10,000 --> 00:02:15,000
This is why we feel suffering when a person passes away.

33
00:02:15,000 --> 00:02:23,000
It's not based on any logic or reason that a person should grieve,

34
00:02:23,000 --> 00:02:32,000
and we should really get this point across that there is no reason to mourn.

35
00:02:32,000 --> 00:02:35,000
It is something that is based on our addiction.

36
00:02:35,000 --> 00:02:42,000
The reason why we cry is because of the chemicals involved with crying,

37
00:02:42,000 --> 00:02:49,000
crying is a reaction that we have developed as a species

38
00:02:49,000 --> 00:02:51,000
to deal with difficult situations.

39
00:02:51,000 --> 00:02:55,000
It's something that releases pleasure chemicals,

40
00:02:55,000 --> 00:03:00,000
and orphans I think it makes you feel calm and happy and peaceful,

41
00:03:00,000 --> 00:03:03,000
so it becomes an addiction, actually.

42
00:03:03,000 --> 00:03:06,000
And this is how grieving works.

43
00:03:06,000 --> 00:03:09,000
It can be crying, some people take to alcohol,

44
00:03:09,000 --> 00:03:14,000
or any sort of addiction, but crying is again another addiction.

45
00:03:14,000 --> 00:03:17,000
And so we should never think that this is somehow important

46
00:03:17,000 --> 00:03:20,000
or a part of the process, a part of the grieving process.

47
00:03:20,000 --> 00:03:27,000
Many people will go on for years suffering because a person has passed away.

48
00:03:27,000 --> 00:03:30,000
And the point is that there's no benefit to it.

49
00:03:30,000 --> 00:03:32,000
You don't help the person who passed away.

50
00:03:32,000 --> 00:03:34,000
You don't help yourself.

51
00:03:34,000 --> 00:03:36,000
You don't do something that they would want for you.

52
00:03:36,000 --> 00:03:41,000
There's no one who wants their loved ones to suffer,

53
00:03:41,000 --> 00:03:45,000
to mourn, to have to be without.

54
00:03:45,000 --> 00:03:48,000
And so in the end, it's really just useless.

55
00:03:48,000 --> 00:03:50,000
And in fact, worse than useless.

56
00:03:50,000 --> 00:03:52,000
It drags you down in many ways.

57
00:03:52,000 --> 00:04:01,000
It drags you down into a cycle of suffering and depression.

58
00:04:01,000 --> 00:04:02,000
And also addiction.

59
00:04:02,000 --> 00:04:06,000
It makes you become, you know, because you start to find a way out.

60
00:04:06,000 --> 00:04:10,000
You try to find a way to block it out and not have to think about it,

61
00:04:10,000 --> 00:04:15,000
which gives rise to many different types of addiction.

62
00:04:15,000 --> 00:04:22,000
Now, this brings up a good point that often people are unable to follow this advice.

63
00:04:22,000 --> 00:04:26,000
This first point, important as it is,

64
00:04:26,000 --> 00:04:30,000
it's best phrased as don't see it as a good thing.

65
00:04:30,000 --> 00:04:33,000
Because if you say that people don't mourn,

66
00:04:33,000 --> 00:04:35,000
well, unless they had no attachment to the person,

67
00:04:35,000 --> 00:04:37,000
they're going to feel sad.

68
00:04:37,000 --> 00:04:40,000
And I've given this talk before at funerals.

69
00:04:40,000 --> 00:04:43,000
And then after you give a talk, the people come up and cry.

70
00:04:43,000 --> 00:04:47,000
And so you think, well, why did I explain to them about not to cry?

71
00:04:47,000 --> 00:04:50,000
And how it's not really useful or any benefit.

72
00:04:50,000 --> 00:04:52,000
But they didn't intend to.

73
00:04:52,000 --> 00:04:56,000
You can't blame people for crying when a loved one dies.

74
00:04:56,000 --> 00:05:03,000
But some people go to the next level and think,

75
00:05:03,000 --> 00:05:05,000
yes, yes, this is good and this is important.

76
00:05:05,000 --> 00:05:11,000
So they encourage it in themselves and they encourage the phenomenon.

77
00:05:11,000 --> 00:05:15,000
And so they end up crying more and they think of it as somehow a good thing.

78
00:05:15,000 --> 00:05:19,000
And so they can get caught up and actually waste a lot of time and energy doing so.

79
00:05:19,000 --> 00:05:29,000
But this also raises another point that we most often get ourselves into this position.

80
00:05:29,000 --> 00:05:34,000
Why, when a person dies, we can go on for days and weeks and months and even years,

81
00:05:34,000 --> 00:05:38,000
mourning is because we've been negligent.

82
00:05:38,000 --> 00:05:42,000
We've been heedless in regards to our mind.

83
00:05:42,000 --> 00:05:45,000
We haven't been paying attention to our attachments.

84
00:05:45,000 --> 00:05:48,000
We live our lives thinking that attaching to other people is good.

85
00:05:48,000 --> 00:05:53,000
It's intimacy and relationships and love and so on.

86
00:05:53,000 --> 00:05:56,000
But this really has nothing to do with love.

87
00:05:56,000 --> 00:05:58,000
It has to do with our own clinging.

88
00:05:58,000 --> 00:06:02,000
When you love someone, it's not what they can do for you and what they bring to you

89
00:06:02,000 --> 00:06:05,000
and the great things that they give to you.

90
00:06:05,000 --> 00:06:08,000
That's a kind of, we use the word love,

91
00:06:08,000 --> 00:06:10,000
but it really is an attachment.

92
00:06:10,000 --> 00:06:13,000
Love is when you wish for good things for other people.

93
00:06:13,000 --> 00:06:16,000
And there's nothing good that comes from crying.

94
00:06:16,000 --> 00:06:22,000
You don't somehow respect the person's memory by crying, by bringing yourself suffering.

95
00:06:22,000 --> 00:06:25,000
You don't do something that they would want you to do,

96
00:06:25,000 --> 00:06:27,000
or that they would want for you.

97
00:06:27,000 --> 00:06:29,000
So this is the first point.

98
00:06:29,000 --> 00:06:32,000
But what I was going to say is that for this reason,

99
00:06:32,000 --> 00:06:35,000
because we get ourselves into this mess,

100
00:06:35,000 --> 00:06:38,000
part of this is even before a person dies,

101
00:06:38,000 --> 00:06:42,000
we should be quite careful with our relationships.

102
00:06:42,000 --> 00:06:44,000
Our relationships should be meaningful,

103
00:06:44,000 --> 00:06:47,000
but the meaning should come from our love for each other,

104
00:06:47,000 --> 00:06:49,000
our wish for others each other to be happy.

105
00:06:49,000 --> 00:06:54,000
And in fact, our wish for all beings to be happy.

106
00:06:54,000 --> 00:06:59,000
The only way we can really be purely have pure love for other beings

107
00:06:59,000 --> 00:07:01,000
is to have it be impartial,

108
00:07:01,000 --> 00:07:04,000
where it's not that we don't love anyone.

109
00:07:04,000 --> 00:07:05,000
We end up loving everyone.

110
00:07:05,000 --> 00:07:09,000
This is the Buddha said that we should strive to love all beings,

111
00:07:09,000 --> 00:07:13,000
just as a mother might love her only child.

112
00:07:13,000 --> 00:07:15,000
And if we can get to this point,

113
00:07:15,000 --> 00:07:17,000
if we really have love for people,

114
00:07:17,000 --> 00:07:19,000
it won't matter who comes and who goes.

115
00:07:19,000 --> 00:07:21,000
Once it's true love,

116
00:07:21,000 --> 00:07:26,000
then the next person you see is a loved one for you.

117
00:07:26,000 --> 00:07:28,000
It's the one you love.

118
00:07:28,000 --> 00:07:31,000
And it continues on when they go.

119
00:07:31,000 --> 00:07:36,000
There's nothing in love that says they need to do anything for you,

120
00:07:36,000 --> 00:07:38,000
or be anything for you.

121
00:07:38,000 --> 00:07:41,000
So when they're gone, there's no suffering.

122
00:07:41,000 --> 00:07:43,000
There's no stress, there's no sadness.

123
00:07:43,000 --> 00:07:48,000
So this is an important reason why we should be practicing meditation,

124
00:07:48,000 --> 00:07:51,000
because when we don't practice meditation,

125
00:07:51,000 --> 00:07:53,000
we cling and cling and cling,

126
00:07:53,000 --> 00:07:57,000
and we set ourselves up for so much stress and suffering

127
00:07:57,000 --> 00:07:59,000
that we see all around us.

128
00:07:59,000 --> 00:08:02,000
Many people are living their lives in great peace and happiness,

129
00:08:02,000 --> 00:08:06,000
simply because they haven't met with the inevitable crash.

130
00:08:06,000 --> 00:08:09,000
They have an inevitable loss of youth,

131
00:08:09,000 --> 00:08:13,000
when they get old, so many things that they can't enjoy,

132
00:08:13,000 --> 00:08:18,000
and suddenly they're confronted with all age or sickness

133
00:08:18,000 --> 00:08:21,000
when people get cancer and eventually death.

134
00:08:21,000 --> 00:08:24,000
The death of loved ones or the death of our own death

135
00:08:24,000 --> 00:08:27,000
was totally unprepared for.

136
00:08:27,000 --> 00:08:30,000
Meditation practices to allow us to have meaningful,

137
00:08:30,000 --> 00:08:37,000
but non-attached relationships with other people,

138
00:08:37,000 --> 00:08:42,000
where we give and we spread goodness all around us

139
00:08:42,000 --> 00:08:48,000
and try our best to make other people happy

140
00:08:48,000 --> 00:08:52,000
and to bring peace to them instead of wishing for them

141
00:08:52,000 --> 00:08:55,000
to bring good things to us,

142
00:08:55,000 --> 00:08:59,000
which is, of course, a totally uncertain something we can't depend on.

143
00:08:59,000 --> 00:09:04,000
So this is the first point that is really useless to grieve,

144
00:09:04,000 --> 00:09:07,000
to grieve, and it's something that as Buddhists we should not do,

145
00:09:07,000 --> 00:09:11,000
we should, on the other hand, spend our lives thinking about

146
00:09:11,000 --> 00:09:13,000
the inevitability of death.

147
00:09:13,000 --> 00:09:17,000
And this brings me to the second point that when a person dies,

148
00:09:17,000 --> 00:09:20,000
it should be a reminder to us.

149
00:09:20,000 --> 00:09:22,000
And the Buddha was very clear about this

150
00:09:22,000 --> 00:09:25,000
for monks when a person dies,

151
00:09:25,000 --> 00:09:28,000
and even for lay people when a person dies,

152
00:09:28,000 --> 00:09:32,000
that we should consider this to be a sign,

153
00:09:32,000 --> 00:09:34,000
a sign that we too will have to go.

154
00:09:34,000 --> 00:09:37,000
We should be clear that this is something that we can't avoid.

155
00:09:37,000 --> 00:09:39,000
It's a fact of life.

156
00:09:39,000 --> 00:09:42,000
So the contemplation on the reality of death

157
00:09:42,000 --> 00:09:49,000
is a very important part of our acceptance when a person dies.

158
00:09:49,000 --> 00:09:57,000
So first of all, the realization that this was a part of that person's life,

159
00:09:57,000 --> 00:09:59,000
and that it's a part of nature,

160
00:09:59,000 --> 00:10:02,000
and not getting caught up in this idea of a person,

161
00:10:02,000 --> 00:10:06,000
or this one life where this being existed,

162
00:10:06,000 --> 00:10:08,000
and now they're gone or so on.

163
00:10:08,000 --> 00:10:11,000
But thinking in terms of reality, in terms of our experience,

164
00:10:11,000 --> 00:10:15,000
and that this is the nature of reality,

165
00:10:15,000 --> 00:10:18,000
that all things that arise in our mind,

166
00:10:18,000 --> 00:10:20,000
the idea of a person will eventually cease.

167
00:10:20,000 --> 00:10:25,000
Everything that we cling to, a person, a place, a thing,

168
00:10:25,000 --> 00:10:29,000
even an ideology.

169
00:10:29,000 --> 00:10:32,000
Eventually, we will have to give it up.

170
00:10:32,000 --> 00:10:34,000
Nothing will last forever.

171
00:10:34,000 --> 00:10:43,000
And so we use death as a really good wake-up call

172
00:10:43,000 --> 00:10:45,000
for us to realize the truth of reality,

173
00:10:45,000 --> 00:10:48,000
and also to reflect on our own mortality.

174
00:10:48,000 --> 00:10:53,000
When a person dies, we should take that as a warning to ourselves.

175
00:10:53,000 --> 00:10:56,000
We should watch and see, and look,

176
00:10:56,000 --> 00:10:58,000
and think of the person who is dead,

177
00:10:58,000 --> 00:11:01,000
and think of this will also come to me one day.

178
00:11:01,000 --> 00:11:03,000
Will I be ready for it?

179
00:11:03,000 --> 00:11:08,000
This is an important, not only is it a good meditation

180
00:11:08,000 --> 00:11:09,000
and useful for ourselves,

181
00:11:09,000 --> 00:11:12,000
but it's a really good way to come to terms with a person to death,

182
00:11:12,000 --> 00:11:14,000
because instead of grieving or thinking,

183
00:11:14,000 --> 00:11:16,000
oh, now that person's gone,

184
00:11:16,000 --> 00:11:22,000
you realize that you remind yourself that this is reality,

185
00:11:22,000 --> 00:11:27,000
and the real problem is our attachments to a specific state of being,

186
00:11:27,000 --> 00:11:30,000
of a person who plays a thing being like this,

187
00:11:30,000 --> 00:11:34,000
not being like that or being with us and not being away from us.

188
00:11:34,000 --> 00:11:37,000
Once we start to look at reality,

189
00:11:37,000 --> 00:11:42,000
and look at the experience of reality for a moment to moment now,

190
00:11:42,000 --> 00:11:44,000
what's happening and so on,

191
00:11:44,000 --> 00:11:47,000
then the idea of how things should be,

192
00:11:47,000 --> 00:11:50,000
or how things could be,

193
00:11:50,000 --> 00:11:53,000
how things used to be, or how things will be, or so on,

194
00:11:53,000 --> 00:11:55,000
is really irrelevant,

195
00:11:55,000 --> 00:11:57,000
and it doesn't have the same hold on us.

196
00:11:57,000 --> 00:12:00,000
We're able to live here and now in peace and happiness,

197
00:12:00,000 --> 00:12:03,000
and that here and now extends forever, of course.

198
00:12:03,000 --> 00:12:05,000
When you're able to be happy here and now,

199
00:12:05,000 --> 00:12:08,000
then you're able to be happy for eternity,

200
00:12:08,000 --> 00:12:11,000
and that's the reality of life.

201
00:12:11,000 --> 00:12:13,000
So that's number two,

202
00:12:13,000 --> 00:12:15,000
is that we should use it as an opportunity

203
00:12:15,000 --> 00:12:17,000
to reflect on our own mortality

204
00:12:17,000 --> 00:12:20,000
and reflect on the inevitability of death,

205
00:12:20,000 --> 00:12:21,000
that it comes to all beings,

206
00:12:21,000 --> 00:12:22,000
and we should remind ourselves

207
00:12:22,000 --> 00:12:25,000
that this is really the reality of that person,

208
00:12:25,000 --> 00:12:29,000
and it shouldn't actually be any,

209
00:12:29,000 --> 00:12:34,000
any positive or negative significance,

210
00:12:34,000 --> 00:12:37,000
it should be seen as the way things are.

211
00:12:37,000 --> 00:12:39,000
We should not be happier unhappy about it,

212
00:12:39,000 --> 00:12:44,000
should not be regretful or mourning or so on.

213
00:12:44,000 --> 00:12:45,000
This is what we should do,

214
00:12:45,000 --> 00:12:52,000
is we should take it as an opportunity to reflect on the way things go in life.

215
00:12:52,000 --> 00:12:54,000
Now, this is more for ourselves,

216
00:12:54,000 --> 00:12:56,000
for the person who passed away,

217
00:12:56,000 --> 00:12:59,000
most people want to know what we should do for the person.

218
00:12:59,000 --> 00:13:01,000
This is the third point.

219
00:13:01,000 --> 00:13:05,000
As Buddhist, we should do things on behalf of the person who's passed away.

220
00:13:05,000 --> 00:13:10,000
We should, whatever good things that person undertook,

221
00:13:10,000 --> 00:13:13,000
or even if they didn't do any good things,

222
00:13:13,000 --> 00:13:16,000
we should carry on their name

223
00:13:16,000 --> 00:13:21,000
or celebrate their life

224
00:13:21,000 --> 00:13:25,000
by giving, by building things,

225
00:13:25,000 --> 00:13:29,000
by building things that'll be of use to people,

226
00:13:29,000 --> 00:13:31,000
dedicating our works to them,

227
00:13:31,000 --> 00:13:33,000
and so on, dedicating books,

228
00:13:33,000 --> 00:13:36,000
or whatever we do, dedicating our work,

229
00:13:36,000 --> 00:13:38,000
our good deeds to that person,

230
00:13:38,000 --> 00:13:41,000
be it things that they did in their life,

231
00:13:41,000 --> 00:13:44,000
we can continue on or just things done in their name,

232
00:13:44,000 --> 00:13:47,000
with some mention of them.

233
00:13:47,000 --> 00:13:52,000
Because this is really a way of celebrating the person's life,

234
00:13:52,000 --> 00:13:55,000
and it's a way of bringing meaning to that person's life.

235
00:13:55,000 --> 00:13:58,000
It's a way of saying that this person's life had real meaning.

236
00:13:58,000 --> 00:14:01,000
If this person hadn't lived,

237
00:14:01,000 --> 00:14:03,000
then these good deeds wouldn't come.

238
00:14:03,000 --> 00:14:06,000
We do these deeds especially for that person,

239
00:14:06,000 --> 00:14:10,000
and therefore, those persons' good deeds that have been done.

240
00:14:10,000 --> 00:14:14,000
It's a way of respecting and celebrating

241
00:14:14,000 --> 00:14:17,000
and honoring the person who passed away.

242
00:14:17,000 --> 00:14:19,000
It's also a way of coming to terms with the death,

243
00:14:19,000 --> 00:14:21,000
in the best way possible,

244
00:14:21,000 --> 00:14:23,000
celebrating the person's life,

245
00:14:23,000 --> 00:14:25,000
saying, this person passed away.

246
00:14:25,000 --> 00:14:28,000
Most people, the best they can do is to remember

247
00:14:28,000 --> 00:14:30,000
the good things the person did and say,

248
00:14:30,000 --> 00:14:31,000
oh yeah, it was such a good person,

249
00:14:31,000 --> 00:14:33,000
so they celebrate and they have a party

250
00:14:33,000 --> 00:14:35,000
and they get drunk and so on, and that's it.

251
00:14:35,000 --> 00:14:38,000
But that's not really celebrating a person's life.

252
00:14:38,000 --> 00:14:41,000
When you celebrate a person's life, it should be all the good things

253
00:14:41,000 --> 00:14:43,000
that they did should be continued on,

254
00:14:43,000 --> 00:14:45,000
or goodness should be done in their names.

255
00:14:45,000 --> 00:14:48,000
It's a person who means something to you

256
00:14:48,000 --> 00:14:50,000
and you think their life had meaning.

257
00:14:50,000 --> 00:14:53,000
Then you should do something, it may not be a lot,

258
00:14:53,000 --> 00:14:56,000
but you should make effort to do something in their name,

259
00:14:56,000 --> 00:15:01,000
and that will be your way of finding closure

260
00:15:01,000 --> 00:15:05,000
and of ending that person's life on a meaningful note.

261
00:15:05,000 --> 00:15:09,000
This person died and the result was that we have done this

262
00:15:09,000 --> 00:15:12,000
on behalf of that person.

263
00:15:12,000 --> 00:15:15,000
Sometimes what that means is an inheritance

264
00:15:15,000 --> 00:15:18,000
that we get, we use part of the inheritance

265
00:15:18,000 --> 00:15:21,000
to honor the person to do something,

266
00:15:21,000 --> 00:15:24,000
not because out of duty or something,

267
00:15:24,000 --> 00:15:27,000
but as a good deed for us and a good deed for them

268
00:15:27,000 --> 00:15:29,000
and something bringing goodness to the world

269
00:15:29,000 --> 00:15:31,000
on behalf of that person.

270
00:15:31,000 --> 00:15:33,000
What you find more often,

271
00:15:33,000 --> 00:15:36,000
which is really the worst thing a person could possibly do

272
00:15:36,000 --> 00:15:40,000
in another person dies, is people fight over the inheritance

273
00:15:40,000 --> 00:15:43,000
of their parents or people who passed away,

274
00:15:43,000 --> 00:15:45,000
which is really a horrible thing,

275
00:15:45,000 --> 00:15:48,000
but apparently goes on quite often.

276
00:15:48,000 --> 00:15:52,000
This is something as good as we should guard against totally.

277
00:15:52,000 --> 00:15:54,000
We should never think of a person's,

278
00:15:54,000 --> 00:15:59,000
another person's possessions as somehow belonging to us

279
00:15:59,000 --> 00:16:02,000
or somehow we deserve them.

280
00:16:02,000 --> 00:16:04,000
We should never look at a person and think this person

281
00:16:04,000 --> 00:16:08,000
owes me something as good as we should never think like that.

282
00:16:08,000 --> 00:16:11,000
So the idea, even if your parents, you help them

283
00:16:11,000 --> 00:16:13,000
or whatever, you should never think that somehow

284
00:16:13,000 --> 00:16:15,000
that's going to give you some inheritance

285
00:16:15,000 --> 00:16:17,000
and you're going to get something.

286
00:16:17,000 --> 00:16:19,000
We should be very careful to guard against this

287
00:16:19,000 --> 00:16:21,000
because it will crop up and when they die,

288
00:16:21,000 --> 00:16:26,000
you'll find yourself fighting and breaking apart your family.

289
00:16:26,000 --> 00:16:29,000
Really, could you imagine what your parents would think

290
00:16:29,000 --> 00:16:31,000
when they see their children,

291
00:16:31,000 --> 00:16:34,000
you know, add each other's throats over things

292
00:16:34,000 --> 00:16:36,000
that they actually left behind,

293
00:16:36,000 --> 00:16:38,000
that their parents had to throw away.

294
00:16:38,000 --> 00:16:39,000
That's really garbage.

295
00:16:39,000 --> 00:16:42,000
It could be money, it could be home possession.

296
00:16:42,000 --> 00:16:44,000
It's all garbage because in the end,

297
00:16:44,000 --> 00:16:47,000
they had to leave it behind and you have to leave it behind.

298
00:16:47,000 --> 00:16:50,000
Much better is whatever good things that they have.

299
00:16:50,000 --> 00:16:53,000
You can take some of it if it's useful to support you,

300
00:16:53,000 --> 00:16:56,000
but making sure that everyone else has taken care of

301
00:16:56,000 --> 00:16:59,000
and giving things in their name.

302
00:16:59,000 --> 00:17:04,000
If the least you do is giving up some hold

303
00:17:04,000 --> 00:17:08,000
that you have on what this fights

304
00:17:08,000 --> 00:17:12,000
that people have over inheritance,

305
00:17:12,000 --> 00:17:18,000
in order that harmony might be bestowed upon your family.

306
00:17:18,000 --> 00:17:21,000
For the sake of harmony and out of respect

307
00:17:21,000 --> 00:17:24,000
for the person who passed away, I'm not going to fight over this.

308
00:17:24,000 --> 00:17:27,000
If that's the least you, then you've done a great thing

309
00:17:27,000 --> 00:17:30,000
about your memory and you've honored their memory.

310
00:17:30,000 --> 00:17:33,000
There are many ways that you can do this,

311
00:17:33,000 --> 00:17:38,000
but this could be the least is to not, for goodness sake,

312
00:17:38,000 --> 00:17:41,000
don't fight over people who passed away,

313
00:17:41,000 --> 00:17:43,000
fight over their belongings.

314
00:17:43,000 --> 00:17:47,000
It's a terrible, terrible thing and be very careful

315
00:17:47,000 --> 00:17:50,000
not to let that happen to you because it might come into the mind.

316
00:17:50,000 --> 00:17:51,000
We can never be sure.

317
00:17:51,000 --> 00:17:53,000
We might find ourselves doing that.

318
00:17:53,000 --> 00:17:55,000
So these are three points that I think

319
00:17:55,000 --> 00:17:57,000
you should keep in mind when the person you know passes away

320
00:17:57,000 --> 00:18:02,000
as a Buddhist or as a person who follows these sorts of teachings

321
00:18:02,000 --> 00:18:04,000
and really any good teachings,

322
00:18:04,000 --> 00:18:07,000
it's nothing specifically to do with Buddhism.

323
00:18:07,000 --> 00:18:08,000
So hope this helps.

324
00:18:08,000 --> 00:18:29,000
Thanks for tuning in and all the best.

