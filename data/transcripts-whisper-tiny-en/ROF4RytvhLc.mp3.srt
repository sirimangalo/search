1
00:00:00,000 --> 00:00:07,000
Can you explain arms? How do you help a monk? Can you buy them food?

2
00:00:07,000 --> 00:00:12,000
Yeah, I mean, this is a big thing, really. Westerners, they appreciate monks.

3
00:00:12,000 --> 00:00:17,000
Many Western people appreciate monks and get teachings from them.

4
00:00:17,000 --> 00:00:22,000
But they don't know what to do. I guess there's also monks that you don't get teachings from,

5
00:00:22,000 --> 00:00:25,000
but you think somebody should help them, which is good.

6
00:00:25,000 --> 00:00:30,000
No, the feeding stray cats is good. Why isn't feeding stray monks good?

7
00:00:30,000 --> 00:00:33,000
Or feeding monks good? Yeah, you can buy them food.

8
00:00:33,000 --> 00:00:35,000
Don't buy them food in the evening.

9
00:00:35,000 --> 00:00:40,000
Buy them food for the day. Monks aren't allowed to keep food.

10
00:00:40,000 --> 00:00:44,000
Teravada Buddhist monks aren't allowed to store food.

11
00:00:44,000 --> 00:00:49,000
So if there are teravada monks, you should give them food that they can eat in the morning.

12
00:00:49,000 --> 00:00:54,000
It should be given after dawn and before noon and it should be consumed on that day.

13
00:00:54,000 --> 00:01:05,000
If you're interested in learning about arms, I wrote an article that I don't know.

14
00:01:05,000 --> 00:01:15,000
Maybe it's probably not what you're looking for, but there's an interesting article I was talking about the idea of giving arms and what it means.

15
00:01:15,000 --> 00:01:20,000
It shouldn't be considered an exchange. It's considered fulfilling a need.

16
00:01:20,000 --> 00:01:26,000
So when monks go on arms, it's because they need food.

17
00:01:26,000 --> 00:01:30,000
Just like when cats come to your door and start meowing, it's because they need food.

18
00:01:30,000 --> 00:01:37,000
So yeah, giving them food is a great thing, especially because you consider that, wow, they're probably doing good things for people.

19
00:01:37,000 --> 00:01:42,000
They're certainly, yeah, they're probably doing good things for themselves and good things for other people.

20
00:01:42,000 --> 00:01:49,000
So it's a better bet than giving to a cat.

21
00:01:49,000 --> 00:01:55,000
That's the best way to help you. You can monks are not allowed to ask for things. They're not allowed to beg.

22
00:01:55,000 --> 00:02:05,000
But unless a person says to the monk, and you have to be careful about this, but if you say to a monk or a male or female monk,

23
00:02:05,000 --> 00:02:10,000
look, if you need anything, let me know. I'll be happy to do them. I'll try my best to do it.

24
00:02:10,000 --> 00:02:16,000
If it's within my power, I'll try to get it for you, then they can ask.

25
00:02:16,000 --> 00:02:24,000
And they're allowed to ask for four months. This is the official allowance.

26
00:02:24,000 --> 00:02:28,000
From that point, from that point in time, they can ask you for another four months.

27
00:02:28,000 --> 00:02:35,000
Unless you say to them, for the rest of my life, you can ask me, or something like that, or for the rest of this week, you can ask me.

28
00:02:35,000 --> 00:02:40,000
Unless you said a limit, then it's four months.

29
00:02:40,000 --> 00:02:51,000
Anyway, then you don't have to guess what they need, and you can supply them with something that they may not have.

30
00:02:51,000 --> 00:02:58,000
The best thing you can do to help monks is to go and learn from them, and become part of the community.

31
00:02:58,000 --> 00:03:05,000
Because there's so much work that needs to be done to facilitate teaching, for example.

32
00:03:05,000 --> 00:03:12,000
So I'm getting involved in the organization. Anyway, this article I wrote, I don't know if it's actually relating to this.

33
00:03:12,000 --> 00:03:24,000
But it relates to the idea of supporting an organization, and how we should best view supporting the organization, especially with this organization.

34
00:03:24,000 --> 00:03:30,000
It's called, there is too such thing as a free lunch.

35
00:03:30,000 --> 00:03:37,000
That's on my web blog.

