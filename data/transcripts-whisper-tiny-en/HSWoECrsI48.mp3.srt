1
00:00:00,000 --> 00:00:26,500
and we're live, evening everyone, March 18th, 9pm as usual, start by looking at the quote.

2
00:00:26,500 --> 00:00:34,500
This is half, this quote is half of a suit, it's the second half.

3
00:00:34,500 --> 00:00:52,400
So the record, it starts actually with five Asamaya, Asamaya, Asamaya Padana, five

4
00:00:52,400 --> 00:01:01,400
and on time for striving.

5
00:01:01,400 --> 00:01:06,400
So you've got Asamaya and you've got Asamaya.

6
00:01:06,400 --> 00:01:17,900
There are times for striving and there are times where you should not strive.

7
00:01:17,900 --> 00:01:24,900
And I'm looking at the commentaries trying to get some clarification here because

8
00:01:24,900 --> 00:01:30,900
I think you have to qualify this to some extent.

9
00:01:30,900 --> 00:01:38,900
But there's a valuable lesson to be learned here.

10
00:01:38,900 --> 00:01:56,900
I suppose the first lesson to consider is other ways of deciding what's the right time to meditate and to strive are often misguided.

11
00:01:56,900 --> 00:02:05,900
So we have to think well, I'd like to meditate but I just can't find the time.

12
00:02:05,900 --> 00:02:11,900
I just can't find the right time to do it.

13
00:02:11,900 --> 00:02:19,900
I don't have enough time or it's just not the time.

14
00:02:19,900 --> 00:02:26,900
Buddhist countries they often say things like, well, when I get old I'll meditate.

15
00:02:26,900 --> 00:02:38,900
And I'm young, I've got work to do, but when I get old I'll take the time to meditate.

16
00:02:38,900 --> 00:02:43,900
And so we put it aside.

17
00:02:43,900 --> 00:02:51,900
I think to some extent this suit as I said has to be qualified because

18
00:02:51,900 --> 00:02:56,900
I would also say that mindfulness is always useful.

19
00:02:56,900 --> 00:03:03,900
I think important that striving is not the only way of meditating,

20
00:03:03,900 --> 00:03:09,900
not the only way of progressing or practicing.

21
00:03:09,900 --> 00:03:19,900
Anyway, let's go through these because it's important because there is a certain amount of striving that you shouldn't do at certain times.

22
00:03:19,900 --> 00:03:29,900
The bikhavay bikhavay bikhugin o ho tea.

23
00:03:29,900 --> 00:03:39,900
Here monks, a monk, a meditator is old.

24
00:03:39,900 --> 00:03:44,900
who told us to become very elderly.

25
00:03:45,700 --> 00:03:47,540
I am bikowee, but the most,

26
00:03:47,540 --> 00:03:49,420
as some of you open down, I am.

27
00:03:49,420 --> 00:03:51,460
So how do you interpret this?

28
00:03:51,460 --> 00:03:53,660
Is he saying that old people shouldn't meditate,

29
00:03:53,660 --> 00:03:57,940
shouldn't practice, shouldn't strive spiritually?

30
00:03:57,940 --> 00:03:59,820
No, in fact, I think what is being said here

31
00:03:59,820 --> 00:04:03,540
is this is a bad time.

32
00:04:03,540 --> 00:04:04,860
It's harder.

33
00:04:04,860 --> 00:04:05,980
It's a harder time.

34
00:04:05,980 --> 00:04:10,420
And the important lesson there is, don't wait till you're old.

35
00:04:10,420 --> 00:04:11,740
Because old people will tell you,

36
00:04:11,740 --> 00:04:14,260
and there's, there's, there's difficulties.

37
00:04:16,700 --> 00:04:18,460
I mean, interestingly enough, as I've said,

38
00:04:18,460 --> 00:04:19,820
it's when people,

39
00:04:19,820 --> 00:04:21,060
mostly there's an excuse,

40
00:04:21,060 --> 00:04:24,980
them too young and they'll do when I'm older.

41
00:04:24,980 --> 00:04:26,460
So interestingly enough,

42
00:04:26,460 --> 00:04:28,540
a lot of old people do take that meditation

43
00:04:28,540 --> 00:04:29,660
and get good results,

44
00:04:29,660 --> 00:04:33,500
but you communicate so much baggage

45
00:04:33,500 --> 00:04:36,500
that there's a lot more work to be done.

46
00:04:36,500 --> 00:04:38,540
And you have the Buddhist words here

47
00:04:38,540 --> 00:04:41,820
in another places that this is not the right way

48
00:04:41,820 --> 00:04:43,780
to look at things.

49
00:04:43,780 --> 00:04:48,660
So if we skip down to the first one on the positive side,

50
00:04:50,100 --> 00:04:51,460
in the bikowee, bikowee,

51
00:04:51,460 --> 00:04:53,540
dah-ha-ro-ho-ti,

52
00:04:53,540 --> 00:04:57,220
one is a chai, a yu-s,

53
00:04:57,220 --> 00:05:01,660
you are young, susu,

54
00:05:01,660 --> 00:05:04,780
you know, susu means they're young.

55
00:05:04,780 --> 00:05:07,260
And when we say in young,

56
00:05:07,260 --> 00:05:09,900
call a gay soul with black hair

57
00:05:09,900 --> 00:05:12,940
because everyone in India had black hair

58
00:05:12,940 --> 00:05:14,140
until it turned great.

59
00:05:15,420 --> 00:05:17,140
Badrina.

60
00:05:20,700 --> 00:05:24,140
No, badrina, yo-pania.

61
00:05:24,140 --> 00:05:32,620
You know, banana, use in the height of their youth, something like that.

62
00:05:32,620 --> 00:05:44,540
Samana Agatou is possessed of the height of youth, the best part of youth, the strength

63
00:05:44,540 --> 00:05:51,580
of youth, the greatness of youth.

64
00:05:51,580 --> 00:06:01,660
Dr. Mina Vayasa, the first Vayana, the first part of life, the first angel life.

65
00:06:01,660 --> 00:06:12,820
I am Bhikkhuvay Patamosa mayo Badana, this is the first occasion, right time for scribing.

66
00:06:12,820 --> 00:06:21,820
And do it while you're young, because every moment that we're unmined for is accumulating

67
00:06:21,820 --> 00:06:27,700
karma, it's accumulating habits, and so you can say that all people in benefit from

68
00:06:27,700 --> 00:06:37,940
meditation absolutely, they also have a lot of habits built up, and so as far as how far

69
00:06:37,940 --> 00:06:45,060
they can get, you know, it depends on the person of course, but people have greater

70
00:06:45,060 --> 00:06:52,300
potential, they've got greater strength, if you've seen some of these young monks meditating

71
00:06:52,300 --> 00:07:00,660
day and night, able to walk very well, they will sit very still, their minds are sharp.

72
00:07:00,660 --> 00:07:08,780
Then when it goes both ways, because all the people have a lot of wisdom that's required

73
00:07:08,780 --> 00:07:13,340
to understand why we're meditating and people meditate for a little while and then get

74
00:07:13,340 --> 00:07:20,580
bored and give it up.

75
00:07:20,580 --> 00:07:25,420
Nonetheless, something that you should start young, get ahead and start, and the old meditator

76
00:07:25,420 --> 00:07:30,260
will tell you, don't wait until you're old, it's not much harder.

77
00:07:30,260 --> 00:07:38,860
Number two, when you're sick, when you're sick, don't put out effort, it's interesting

78
00:07:38,860 --> 00:07:43,220
because you should meditate, you should be very mindful as I said, but in other places

79
00:07:43,220 --> 00:07:53,260
I said that sickness is a great catalyst for inside, but you shouldn't work hard because

80
00:07:53,260 --> 00:07:59,260
you can aggravate the sickness and prolong the sickness, you should take care of your

81
00:07:59,260 --> 00:08:06,260
body, rest a lot, lie down, don't do lots of walking, don't push yourself, don't push

82
00:08:06,260 --> 00:08:14,060
your body anyway.

83
00:08:14,060 --> 00:08:18,580
When you're healthy, that's when you're sheminity, because when you're sick, it gets

84
00:08:18,580 --> 00:08:19,580
hard.

85
00:08:19,580 --> 00:08:25,140
It may be a great time to be mindful, but it's much harder to be mindful, and of course

86
00:08:25,140 --> 00:08:30,340
you can't do all the exercises, so the point is, when you're healthy, take that time to

87
00:08:30,340 --> 00:08:31,340
meditate.

88
00:08:31,340 --> 00:08:32,340
Don't be negligent.

89
00:08:32,340 --> 00:08:39,420
A lot of talk about the negligence of health, oh, I'm healthy now.

90
00:08:39,420 --> 00:08:44,060
I don't need to worry about suffering, I don't need to worry about my defilements, I'm

91
00:08:44,060 --> 00:08:48,980
healthy, I can get what I want, I can do what I want, if I have problems, I just chase them

92
00:08:48,980 --> 00:08:52,660
away or I run away from them.

93
00:08:52,660 --> 00:09:02,660
When we're sick, we don't know what to do, we're unprepared, so it's a good lesson,

94
00:09:02,660 --> 00:09:07,480
you're rather than thinking, oh, I'm happy, so why should I worry, should I work, why

95
00:09:07,480 --> 00:09:14,260
should I train myself, we should think that when you're happy, when you're well, you

96
00:09:14,260 --> 00:09:20,700
should prepare yourself for being unwell, you should prepare yourself, it's a good time

97
00:09:20,700 --> 00:09:25,140
to train yourself, and to make yourself stronger, because making yourself stronger once

98
00:09:25,140 --> 00:09:28,060
you're already unwell is much more difficult.

99
00:09:28,060 --> 00:09:32,580
A person's never practiced meditation, they never practiced mindfulness, and then they

100
00:09:32,580 --> 00:09:37,580
get sick, it's very hard to teach them meditation at that point, because they won't

101
00:09:37,580 --> 00:09:45,540
hear it, their mind is too reactionary, better to start with when it's easy, right, just

102
00:09:45,540 --> 00:09:54,460
start when it's hard, much harder to start, it doesn't mean you shouldn't be mindful

103
00:09:54,460 --> 00:09:58,780
and you shouldn't try your best to be mindful when you're sick, when you're sick, it's

104
00:09:58,780 --> 00:10:08,020
a great time to learn, and to learn to let go, because you have suffering and suffering

105
00:10:08,020 --> 00:10:17,060
is something that we react to very strongly, number three, do we come, do we come, hold

106
00:10:17,060 --> 00:10:30,460
deep, one at a time when there is not much food, do sassan, do sassan is sisasa, crops,

107
00:10:30,460 --> 00:10:37,460
right, when there are poor crops, do love up in and down, when it's hard to get on,

108
00:10:37,460 --> 00:10:58,500
and the soup, karang, unchain, it's not easy to gather, something something asks, all right,

109
00:10:58,500 --> 00:11:05,540
begging the opportunity, I don't know, why does it say?

110
00:11:05,540 --> 00:11:22,100
It's not easy to keep on life going on sassanants or something like that, when it's

111
00:11:22,100 --> 00:11:29,140
hard to get food and food is scarce, so this is an issue for monks more than lay people

112
00:11:29,140 --> 00:11:36,300
I suppose, but it can happen when you're poor, that food is scarce, well, don't push your

113
00:11:36,300 --> 00:11:41,860
body at that point, remember we had one person who came to meditate and wanted to do a

114
00:11:41,860 --> 00:11:49,100
fast, so they were just going to have fruit for like three weeks, and so after two weeks

115
00:11:49,100 --> 00:11:57,660
he came to my hut and said he had like pulled a ligament in his leg, because it's hard,

116
00:11:57,660 --> 00:12:03,060
you know, this isn't like some yoga retreat where you just lie on your mat and stretch

117
00:12:03,060 --> 00:12:07,620
all of them, you know, and do simple stretches, you're actually doing hours and hours

118
00:12:07,620 --> 00:12:15,300
of walking, you know, and it's insight, so you're dealing with an ordinary state, you're

119
00:12:15,300 --> 00:12:24,540
not entering into very high and special states, you're dealing with a lot of difficulties

120
00:12:24,540 --> 00:12:28,340
because you're trying to learn and understand how your mind works, you're digging right

121
00:12:28,340 --> 00:12:44,780
in there with the problems, it's very strenuous, no, it's very taxing on your system,

122
00:12:44,780 --> 00:12:51,300
so when you have to nourish your body, when you're not able to find nourishment, that's

123
00:12:51,300 --> 00:13:11,100
the time you have to be careful, and then by young hoat, the art of his sankopo, when

124
00:13:11,100 --> 00:13:26,580
there is danger, jakka, samma, janupanda, pariayanti, and go have to look up a translation

125
00:13:26,580 --> 00:13:33,780
of familiar with any of this, there's peril, turbulence in the wilderness, and the people

126
00:13:33,780 --> 00:13:40,380
of the countryside mounted on their vehicles flee on all sides, thank you, be kubody.

127
00:13:40,380 --> 00:13:48,860
Yeah, there's danger, and there's danger, you should be careful, shouldn't go out and

128
00:13:48,860 --> 00:13:57,860
do walking, if there's a war in your area, you have to take care of your body, it's not

129
00:13:57,860 --> 00:14:02,740
a good time for striving because you'll be worried about dangers and you might even

130
00:14:02,740 --> 00:14:13,100
be attacked and harmed, and so, I mean, part of the lesson here is that when you don't

131
00:14:13,100 --> 00:14:19,540
have danger, when life is good, this is when take advantage of the moment, take advantage

132
00:14:19,540 --> 00:14:37,100
of the opportunity, don't let the moment pass you by.

133
00:14:37,100 --> 00:14:48,740
So when there's harmony in society, manusas, samangas, samodamana, avivandamana, people are dwelling

134
00:14:48,740 --> 00:14:57,540
in harmony, when they're not fighting, hero that keyboard having become like milk and

135
00:14:57,540 --> 00:15:05,740
water, mixing like milk and water without any turbulence, people can mix, and when there's

136
00:15:05,740 --> 00:15:11,660
nothing, when people are looking at each other, fondly, pia, chakuhi, sampasanta, we had

137
00:15:11,660 --> 00:15:28,740
an routine, went well, harmony, eyeing each other favorably, kindly, dearly.

138
00:15:28,740 --> 00:15:36,340
Number five is when your community, when the sun guy is dwelling, when the sun guy is split

139
00:15:36,340 --> 00:15:42,940
up, when your meditation community, when your community, when your family, that's a time

140
00:15:42,940 --> 00:15:46,660
where it's difficult to strive.

141
00:15:46,660 --> 00:15:55,980
So when your family is in harmony, you should take that opportunity to strive.

142
00:15:55,980 --> 00:16:02,860
And so I don't think this really means that you shouldn't try and be mindful, you just

143
00:16:02,860 --> 00:16:06,980
give up and throw in the towel when things are difficult.

144
00:16:06,980 --> 00:16:14,300
It's more of a lesson of how difficult it is with those things, and so to take advantage

145
00:16:14,300 --> 00:16:15,300
of the time.

146
00:16:15,300 --> 00:16:20,500
But it's also a warning that you shouldn't push too hard when there are things that

147
00:16:20,500 --> 00:16:27,420
will be affected by striving, so when your body might be injured, because you're striving,

148
00:16:27,420 --> 00:16:31,820
because you're not getting enough nourishment, and you might be killed, because there's

149
00:16:31,820 --> 00:16:40,260
an, there's, you know, war going on, and that kind of thing, or you might be the subject

150
00:16:40,260 --> 00:16:45,100
to all sorts of problems, because people are fighting in your family or your community

151
00:16:45,100 --> 00:16:51,020
as to fighting, you should resolve some of these problems.

152
00:16:51,020 --> 00:17:02,340
Anyway, interesting sutta, and giving the five good times to strive, and the five times

153
00:17:02,340 --> 00:17:09,060
when striving becomes difficult.

154
00:17:09,060 --> 00:17:20,020
So next up, I'm going to post the Hangout link on meditation.ceremungalow.org.

155
00:17:20,020 --> 00:17:22,620
I want to join the Hangout.

156
00:17:22,620 --> 00:17:25,460
You're welcome to come and ask questions.

157
00:17:25,460 --> 00:17:33,780
This is where I take live questions for anyone brave enough to join me with a video webcam

158
00:17:33,780 --> 00:17:35,780
and a microphone.

159
00:17:35,780 --> 00:17:43,500
Give me a couple of minutes, if nobody shows up, then we say a good night.

160
00:17:43,500 --> 00:17:50,140
Tomorrow I'm giving a talk in second life as far as I know, and it's at 12 p.m.

161
00:17:50,140 --> 00:17:54,780
12 noon, second life time, in the Buddha Center.

162
00:17:54,780 --> 00:17:59,460
So if you know what that means, come on out, if you don't know what that means, don't

163
00:17:59,460 --> 00:18:01,940
worry about it.

164
00:18:01,940 --> 00:18:13,900
Oh, and it looks like I'm going to New York City.

165
00:18:13,900 --> 00:18:21,780
If you're in the New York City area, it looks like I might be there from 16th of April

166
00:18:21,780 --> 00:18:24,500
to the 23rd of April.

167
00:18:24,500 --> 00:18:28,020
Looks sometime around that time.

168
00:18:28,020 --> 00:18:37,380
It's not confirmed yet, but it's decided upon a grid upon, so it looks like I'll be doing

169
00:18:37,380 --> 00:18:43,580
some meditation teachings in New York City.

170
00:18:43,580 --> 00:18:56,300
Some special memories of New York City, I was flying to Thailand as a monk, and I brought

171
00:18:56,300 --> 00:19:01,260
along a novice, a Cambodian novice, and we'd agree.

172
00:19:01,260 --> 00:19:07,380
This was of course back when I was with monks who were all using money, but I decided

173
00:19:07,380 --> 00:19:13,380
this was the first time I decided that I was going to stop using money, and so I said,

174
00:19:13,380 --> 00:19:18,860
we're not bringing any money on this trip, and we got to New York City, and I had left

175
00:19:18,860 --> 00:19:25,300
my passport on the plane in the seat in front of me, and it went back to Toronto, and

176
00:19:25,300 --> 00:19:43,300
we missed our plane from New York City to Korea, Canada, Korea, Taiwan, probably Taiwan.

177
00:19:43,300 --> 00:19:49,940
And so we spent two nights, I think, in, you know, just maybe one night, it spent about

178
00:19:49,940 --> 00:19:59,180
20, almost 24 hours in over 24 hours in New York City, and we wouldn't have survived

179
00:19:59,180 --> 00:20:02,060
if the novice hadn't secretly.

180
00:20:02,060 --> 00:20:05,540
He pulled ice and I don't know what we're going to do, so he pulls at like $100, he

181
00:20:05,540 --> 00:20:15,220
just stuck some money, so he had money to get a taxi, and then to get a train back to the

182
00:20:15,220 --> 00:20:16,220
airport.

183
00:20:16,220 --> 00:20:27,300
We got a taxi to the Kimbo deal monastery, and we slept some time in the JFK airport,

184
00:20:27,300 --> 00:20:35,340
but I was there a couple years ago as well, just passed through, ended up staying a couple

185
00:20:35,340 --> 00:20:42,300
nights in Long Island, maybe I'll go to Long Island again this time to the Sri Lankan

186
00:20:42,300 --> 00:20:49,100
Meditations Center there. That's a really nice, good friend of mine, Nanda.

187
00:20:49,100 --> 00:20:58,700
Oh, for me, we have low Stephen or Stephen.

188
00:20:58,700 --> 00:21:12,220
Hello, your mic was tired, I can hear you, but I don't know what you're saying.

189
00:21:12,220 --> 00:21:41,940
You have a question, hello, interesting, all right, I'm assuming no questions.

190
00:21:41,940 --> 00:21:53,380
Where will I be teaching in New York City?

191
00:21:53,380 --> 00:21:55,220
I don't know yet.

192
00:21:55,220 --> 00:21:56,220
Nothing's confirmed.

193
00:21:56,220 --> 00:22:09,380
It's all very perlim, I'll let you know, maybe night everyone, see some of you in second

194
00:22:09,380 --> 00:22:12,860
life tomorrow, good night.

