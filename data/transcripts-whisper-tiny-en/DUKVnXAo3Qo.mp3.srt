1
00:00:00,000 --> 00:00:08,240
Here we have something unique.

2
00:00:08,240 --> 00:00:23,400
These men are prisoners, well, convict, convict, I guess we see, and they've been engaged

3
00:00:23,400 --> 00:00:35,640
to come to help us, per day, so they're bringing sand and rocks, and this man is offered

4
00:00:35,640 --> 00:00:45,600
to cut down this tree for me, it's already dead, I think, he's just going to get it out of here.

5
00:00:45,600 --> 00:00:54,160
So this is a very welcome surprise, 20 men coming to do their work, this is what is

6
00:00:54,160 --> 00:01:08,040
main building here, really difficult, and rocks cement all the way down here, and so this

7
00:01:08,040 --> 00:01:14,880
will make a lot easier to build, now we have the sand just coming to the next step in

8
00:01:14,880 --> 00:01:21,320
the rocks, and today the engineer is coming, said he was coming yesterday, he's changed,

9
00:01:21,320 --> 00:01:27,160
and so here's where we're going to build two good ones, I think the idea now is to build

10
00:01:27,160 --> 00:01:35,240
two rooms here, two rooms together, because it's a little bit cheaper to make two rooms

11
00:01:35,240 --> 00:01:41,240
joined with one wall, joining them, and people feel safer that way, not only than that

12
00:01:41,240 --> 00:01:48,240
but someone else, so we have two here, and this one we've demolished, I demolished, pushed,

13
00:01:48,240 --> 00:01:55,280
I'm going to demolishing, building is not, I'm not so proficient, but I think we'll leave

14
00:01:55,280 --> 00:01:58,720
this for now, this one's going to be more difficult, because we have to join it with

15
00:01:58,720 --> 00:02:13,720
the cave wall, and eventually we'll build the third, so there's another update.

