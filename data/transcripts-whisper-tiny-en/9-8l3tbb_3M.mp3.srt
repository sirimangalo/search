1
00:00:00,000 --> 00:00:10,000
I don't believe in matter or Rupa, because I've never experienced it directly and I don't believe in anything I don't see with my own eyes.

2
00:00:10,000 --> 00:00:13,000
Is this a problem?

3
00:00:13,000 --> 00:00:21,000
That's interesting. I mean, you have experienced Rupa directly. You experience it all the time.

4
00:00:21,000 --> 00:00:38,000
That's what you're experiencing. You're experiencing states of hardness and softness and stiffness and lucidity and hardness and hot and cool.

5
00:00:38,000 --> 00:00:46,000
You're empirically, objectively speaking, unless you're a strange person, you're experiencing all of those.

6
00:00:46,000 --> 00:00:50,000
The only one you're not able to experience is the water element.

7
00:00:50,000 --> 00:00:56,000
But with the other three elements, we're experiencing all the time. That's what it means by Rupa or matter.

8
00:00:56,000 --> 00:01:02,000
Matter is this aspect of experience. Not all experience is formed with matter.

9
00:01:02,000 --> 00:01:07,000
Some experiences are mental where you're thinking about something.

10
00:01:07,000 --> 00:01:15,000
But the majority of our experiences do have a physical component.

11
00:01:15,000 --> 00:01:20,000
Even when you see something, that's a physical and experience of the physical.

12
00:01:20,000 --> 00:01:32,000
I think perhaps the problem is intellectually, you categorize all of this as mental because it's the experience.

13
00:01:32,000 --> 00:01:37,000
There's the experience or the mind. You think, well, it's just a mental experience.

14
00:01:37,000 --> 00:01:47,000
But that's only intellectual. The reality is you are experiencing heat and cold and hardness and softness and stiffness and lucidity.

15
00:01:47,000 --> 00:01:59,000
These six things, everyone experiences every human being, experiences these aspects of reality,

16
00:01:59,000 --> 00:02:07,000
which are considered material. They're not mental. The experience is based on the mind.

17
00:02:07,000 --> 00:02:12,000
Without the mind, there is no experience. But the matter does exist.

18
00:02:12,000 --> 00:02:18,000
It might be a bit of a problem if you don't see that clearly as it is.

19
00:02:18,000 --> 00:02:22,000
On the other hand, the most important thing is to see experience for what it is.

20
00:02:22,000 --> 00:02:26,000
If you classify it as physical or as mental, it's not really important.

21
00:02:26,000 --> 00:02:31,000
The point is that you understand that when there is heat, you understand it to be heat,

22
00:02:31,000 --> 00:02:35,000
and you don't understand it to be me and to be mine and to be I.

23
00:02:35,000 --> 00:02:40,000
So, when you feel hot, you know this is hot. When you feel cold, you know this is cold.

24
00:02:40,000 --> 00:02:47,000
And you're clear only in the mirror awareness, but the septimata, as we learned yesterday.

25
00:02:47,000 --> 00:02:57,000
The bare and specific awareness or recognition of the object as it is, whether you call that physical or mental,

26
00:02:57,000 --> 00:03:03,000
they're just names and classifications. But there is something to that because through the classification,

27
00:03:03,000 --> 00:03:14,000
because the hot is not coming from the mind, it's coming from the world, the physical world that we say is around us.

28
00:03:14,000 --> 00:03:21,000
That for on some level is all around us. And so when there's fire, you feel the heat.

29
00:03:21,000 --> 00:03:31,000
So that's physical. It's not coming from your mind. There actually is heat there coming from the physical world, whatever that is.

30
00:03:31,000 --> 00:03:44,000
And when you do walking meditation, you experience the walking. You feel the foot on the floor. You experience the movements.

31
00:03:44,000 --> 00:03:55,000
When you are sitting in meditation, you have the breath going in and out. You've experienced or you feel the rising and the falling of the abdomen.

32
00:03:55,000 --> 00:04:01,000
So that as well is body. That's better.

33
00:04:01,000 --> 00:04:11,000
I think it's probably an intellectualization, which is why it's important to give up our ideas of what we believe in.

34
00:04:11,000 --> 00:04:18,000
No, it's not. It's not a problem if you don't believe in something. We're trying to give up all beliefs, right?

35
00:04:18,000 --> 00:04:28,000
No, I wouldn't attach to the view that matter doesn't exist or so on.

36
00:04:28,000 --> 00:04:34,000
Or that this is all meant, that the heat that you feel is only mental or something like that.

37
00:04:34,000 --> 00:04:40,000
Because that's still a view. And you should accept it for what it is. Heat is heat.

38
00:04:40,000 --> 00:04:55,000
You know, to think of that as being mental. I did a video a long time ago about this, about the argument against body or materialism and what the opposite is.

39
00:04:55,000 --> 00:05:01,000
I guess mysticism or something or mentalism, whatever it's actually called.

40
00:05:01,000 --> 00:05:11,000
So there are two arguments where one says that only the material exists and one says that only the mental exists. And these two arguments are found in the world.

41
00:05:11,000 --> 00:05:17,000
From the sounds of it, you might be on the latter side thinking that only the mind exists.

42
00:05:17,000 --> 00:05:30,000
But the argument against both of these is that either one requires something extra. It requires a belief or a view that what you're experiencing is something other than what it is.

43
00:05:30,000 --> 00:05:38,000
Because clearly we have what appears to be physical. And we have what appears to be mental.

44
00:05:38,000 --> 00:05:48,000
That's what empirically what we have. So the materialists create the argument that the physical is created by the mental is created by the physical.

45
00:05:48,000 --> 00:05:54,000
It's an epiphenomenon or it's illusion or so on. They add that in.

46
00:05:54,000 --> 00:06:01,000
People who say there is only the mind do the same thing in the other direction. They say the physical is just an illusion.

47
00:06:01,000 --> 00:06:05,000
It's something that is created by the mind that appears to be physical.

48
00:06:05,000 --> 00:06:20,000
Now if you had a good reason to suggest either of these, if you had some proof or some argument to make in favor of them then all well and good and we can talk about that.

49
00:06:20,000 --> 00:06:27,000
But I think a very strong point that you don't need to take up either extreme.

50
00:06:27,000 --> 00:06:34,000
There's no reason that I can see for the need to say that the physical doesn't exist or that the mental doesn't exist.

51
00:06:34,000 --> 00:06:40,000
Because for all intents or as it appears to us they both do exist.

52
00:06:40,000 --> 00:06:51,000
And really that's I think quite key in Buddhism that what appears is reality.

53
00:06:51,000 --> 00:07:00,000
And reality is actually as it appears. It's just that we don't spend enough time looking at what appears.

54
00:07:00,000 --> 00:07:13,000
It's not that it seems different than what it is. It's that we rather than sticking with the reality or with what appears.

55
00:07:13,000 --> 00:07:29,000
We go on and extrapolate on it. We create an additional idea or perception or belief or partiality towards the object.

56
00:07:29,000 --> 00:07:34,000
But they really are as they appear. That's I think quite important.

57
00:07:34,000 --> 00:07:46,000
And this goes against really material science or many religious traditions who say no reality isn't what it has to appear.

58
00:07:46,000 --> 00:07:53,000
And the argument or the point is that there's no reason to believe that.

59
00:07:53,000 --> 00:07:59,000
There's no reason to say that beyond this kind of attachment to a belief.

60
00:07:59,000 --> 00:08:06,000
Because what does it mean to say that things aren't as they appear? Things appear that way.

61
00:08:06,000 --> 00:08:12,000
There appears to be heat. What does it mean to say that reality is other than that?

62
00:08:12,000 --> 00:08:18,000
That reality is other than experience. It just becomes an intellectual activity where you say no reality is not this.

63
00:08:18,000 --> 00:08:26,000
Actually, you're not experiencing heat like this idea of Maya in Hinduism. It's an intellectual exercise. It's not real.

64
00:08:26,000 --> 00:08:29,000
Yes, there can be states where there is no heat. There is no cold and so on.

65
00:08:29,000 --> 00:08:34,000
But at the moment that you experience heat that you experience cold, there is heat and there is cold.

66
00:08:34,000 --> 00:08:52,000
What else could you say then that? How could you deny the fact that what you're experiencing is real without appealing to an intellectual activity?

67
00:08:52,000 --> 00:09:09,000
To what you write, I don't see with my own eyes or I don't believe in anything. I don't see with my own eyes. I just wanted to say that this is a good quality.

68
00:09:09,000 --> 00:09:29,000
I would say and the Buddha himself encouraged us to only believe what we can prove. So not to believe blindly what we are told and what others say and so on.

69
00:09:29,000 --> 00:09:42,000
This is not a problem. But if you go, you can overdo it. You can become too fixed upon your own opinion about things.

70
00:09:42,000 --> 00:10:02,000
And that might be a problem then when you are not open anymore for what people say or for what teachers say or you might not be able to see reality as it is anymore.

71
00:10:02,000 --> 00:10:14,000
It doesn't say anything to say I believe eggs when you have an experience. But it also doesn't say anything to say I don't believe in eggs when you have an experience.

72
00:10:14,000 --> 00:10:31,000
Eggs doesn't mean it doesn't exist. So people always take up the columnists and there is this quote that is going around the internet that is not what the Buddha says in the columnists. They quote the columnists are incorrectly and so it is just a paraphrasing.

73
00:10:31,000 --> 00:10:45,000
Don't believe anything unless it agrees with your common sense and reason. But both of those things can be totally wrong. Common sense is what people sense the people have in common.

74
00:10:45,000 --> 00:11:02,000
And your reason can be totally wrong. Because you can reason anything based on logic, based on reasoning. There is this argument as to why God doesn't exist or something.

75
00:11:02,000 --> 00:11:22,000
But just you can come to any conclusion through logic. The point of the columnists is that yes you shouldn't believe in something. But as Panyan said, that doesn't mean that you should be actively skeptical.

76
00:11:22,000 --> 00:11:38,000
Buddha didn't teach skepticism. He taught balance and impartiality. So someone comes and says something to you. It should be you should give them the 50-50% chance.

77
00:11:38,000 --> 00:12:00,000
And if based on your experiences in the past, there is something that goes against what they say. Then you start to adjust that. But to say I don't give it any credence or any possibility or any percentage of being true just because I haven't experienced it.

78
00:12:00,000 --> 00:12:12,000
I think it's quite simplistic and it's a little bit of a cop out. And it's a cause for a great amount of argument. How do you know this? How do you know that?

79
00:12:12,000 --> 00:12:26,000
Well there's a lot of things that I don't know but I've heard other people talk about them or based on my own experience. I can understand how it makes the most sense to be this or be that.

80
00:12:26,000 --> 00:12:34,000
And therefore I go with it as opposed to trying to prove everything.

81
00:12:34,000 --> 00:12:44,000
So if it were really important to believe in inrupa or matter then maybe it have something.

82
00:12:44,000 --> 00:12:51,000
But it's not really important to believe in it or not believe in. It's not really an important part of the practice.

83
00:12:51,000 --> 00:12:59,000
So the most important thing is to when you do experience what appears to be matter. You experience it objectively.

84
00:12:59,000 --> 00:13:23,000
That's what it is.

