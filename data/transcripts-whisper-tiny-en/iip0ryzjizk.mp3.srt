1
00:00:00,000 --> 00:00:17,400
We're doing broadcasting live from Stony Creek, oh no, I'm well, I'm in Stony Creek.

2
00:00:17,400 --> 00:00:28,200
September 25th, 2015.

3
00:00:28,200 --> 00:00:42,200
And we have a quote on the second genre, kind of a poetic sort of description.

4
00:00:42,200 --> 00:00:47,200
You want me to read it for us?

5
00:00:47,200 --> 00:00:49,000
Yes.

6
00:00:49,000 --> 00:00:54,120
By the stopping of logical and wandering thoughts, by gaining inner tranquility and

7
00:00:54,120 --> 00:01:00,000
one pointedness of mind, one enters and abides in the second genre, which is without logical

8
00:01:00,000 --> 00:01:05,680
and wandering thought, and is filled with a joy and happiness born of concentration.

9
00:01:05,680 --> 00:01:12,080
And with that joy and happiness born of concentration, one suffuses, drenches, fills

10
00:01:12,080 --> 00:01:17,120
and permeates the whole body so that there is no spot in the entire body that is untouched

11
00:01:17,120 --> 00:01:21,560
by that joy and happiness born of concentration.

12
00:01:21,560 --> 00:01:26,960
Just as in a pool fed by a spring with no inlets in any direction, where the rain God sends

13
00:01:26,960 --> 00:01:32,160
down light showers from time to time, the cool waterwelling up from a spring below would

14
00:01:32,160 --> 00:01:38,920
suffuse, fill and permeate that pool with cool water so that no part would remain untouched

15
00:01:38,920 --> 00:01:40,840
by it in the same way.

16
00:01:40,840 --> 00:01:47,080
In the same way, one diffuses, drenches, film, fills and permeates the whole body so that

17
00:01:47,080 --> 00:01:52,080
no spot is untouched.

18
00:01:52,080 --> 00:01:58,680
So this is a description of kind of meditation we call samata, which is this great

19
00:01:58,680 --> 00:02:08,280
blissful feeling, a blissful state that comes about and will focus the mind, usually

20
00:02:08,280 --> 00:02:18,280
on a concept.

21
00:02:18,280 --> 00:02:24,040
So this is the kind of meditation that comes to meditators in our tradition beginning,

22
00:02:24,040 --> 00:02:26,520
but we don't develop on it.

23
00:02:26,520 --> 00:02:38,720
We try to streamline it a bit and move on towards inside, but it's definitely a positive

24
00:02:38,720 --> 00:02:46,120
aspect of mental development.

25
00:02:46,120 --> 00:02:49,600
So I don't have much to say about it actually.

26
00:02:49,600 --> 00:02:58,160
Today we're in a bit of a hiccup in the appointment slots.

27
00:02:58,160 --> 00:03:04,160
Those of you who are involved with that, I'm not really sure what's going on, but my

28
00:03:04,160 --> 00:03:11,960
feeling is it might be something to do with being here in Stony Creek, I don't know.

29
00:03:11,960 --> 00:03:21,400
But if so, then it should work again tomorrow when I go back to Hamilton.

30
00:03:21,400 --> 00:03:30,880
And in the meantime, I'm going to look into, looking to maybe changing to back to Google

31
00:03:30,880 --> 00:03:39,360
Hangouts, we can instead post a hangout link, I don't really know if that will work.

32
00:03:39,360 --> 00:03:46,480
We won't work quite the same, we'll look into it and see how that works.

33
00:03:46,480 --> 00:03:53,080
But maybe tomorrow morning, we can do a hangout, hangouts, instead.

34
00:03:53,080 --> 00:04:01,200
For those people who are signed up, you need hangouts, you need Google Hangouts for that,

35
00:04:01,200 --> 00:04:10,400
and we can meet using Google Hangouts.

36
00:04:10,400 --> 00:04:13,600
I don't really know what went wrong or what's going wrong.

37
00:04:13,600 --> 00:04:23,400
Maybe some ports on the server action, that was something I was going to try.

38
00:04:23,400 --> 00:04:34,640
Last night we were going to test out the room again, I didn't ever go in it.

39
00:04:34,640 --> 00:04:43,080
I popped into it and Charlie was in there, but it was just a black box, didn't seem

40
00:04:43,080 --> 00:04:57,280
to work to show both of us, still not quite perfect.

41
00:04:57,280 --> 00:05:20,000
Yeah, there were some questions posted from earlier today.

42
00:05:20,000 --> 00:05:33,880
Question for about day to night, how do you argue against the theory of animal overpopulation

43
00:05:33,880 --> 00:05:35,960
if none of them are killed?

44
00:05:35,960 --> 00:05:41,280
I have also heard about such things as mad cow disease, which only exist because of cows

45
00:05:41,280 --> 00:05:52,240
reaching a certain age.

46
00:05:52,240 --> 00:06:18,680
Out of concern for world population and human health, we should continue to kill things.

47
00:06:18,680 --> 00:06:35,760
It's funny, it would have took a different view of sickness, and one thing he did say,

48
00:06:35,760 --> 00:06:41,200
which is an example of his view is of the first order of cows, there were only three

49
00:06:41,200 --> 00:06:51,420
sicknesses in existence, and I think one of them was old age, and after they started

50
00:06:51,420 --> 00:07:05,880
slaughtering cows, he sent over 100 diseases in the world, so the Buddhist response

51
00:07:05,880 --> 00:07:23,160
or claim, I guess, is that it's actually karma that the most part anyway, or smart

52
00:07:23,160 --> 00:07:34,160
and wise, and took care of the world in the right way, sickness, and the wilds, so that's

53
00:07:34,160 --> 00:07:45,080
to do with sickness, as for overpopulation, there's the real answer and the better answer,

54
00:07:45,080 --> 00:07:53,120
it is the latter.

55
00:07:53,120 --> 00:08:00,000
There's a short story by Ursula Lagin, who is an old science fiction author that I read

56
00:08:00,000 --> 00:08:06,680
a long time ago, but she has this story that just paints it so perfectly, because there's

57
00:08:06,680 --> 00:08:13,880
a society that has become a utopia, everything is perfect, they have no problems, they

58
00:08:13,880 --> 00:08:19,720
have no troubles, everything is wonderful, but in order to make this happen, there's

59
00:08:19,720 --> 00:08:30,800
a child tied to a chair in a cellar somewhere, and it's neglected and fed just barely

60
00:08:30,800 --> 00:08:36,120
enough to keep, we kept alive and things of atrophy and being tied to the chair, maybe

61
00:08:36,120 --> 00:08:43,320
it's not tied to a chair was locked in a room, in a dank room in a cellar, and that

62
00:08:43,320 --> 00:08:52,240
child's, or the utopia's existence, is dependent on that child's state, you know, it's

63
00:08:52,240 --> 00:09:05,080
a fantastical hypothetical situation, but the point of the story is that it's morally

64
00:09:05,080 --> 00:09:20,360
untenable, happiness, true, true well-being can never be morally, it's not morally

65
00:09:20,360 --> 00:09:29,160
tenable, you know, as long as there's still, you're causing suffering, because of it,

66
00:09:29,160 --> 00:09:35,160
unknowingly engaging in something that is causing the suffering to this child, so the

67
00:09:35,160 --> 00:09:44,360
question being asked is whether you could live with yourself, and Buddhism sort of looks

68
00:09:44,360 --> 00:09:55,040
at it all that way, you would never sacrifice your moral principles, your ethics, in order

69
00:09:55,040 --> 00:10:04,760
to find happiness, I mean it's selfishness, so if it means one's own death, one would

70
00:10:04,760 --> 00:10:09,160
never kill, I mean it's not even really hard thing to understand, there's hard to put

71
00:10:09,160 --> 00:10:15,360
into practice, I guess, for people who aren't familiar with Buddhism, but who aren't comfortable

72
00:10:15,360 --> 00:10:29,720
with that, with the level of moral or ethical principle that a Buddhist aspires to, but

73
00:10:29,720 --> 00:10:37,280
it's really, I mean it's not, I don't consider them very difficult argument to counter,

74
00:10:37,280 --> 00:10:48,320
because you're weighing two things that are very different value, one's happy, one's

75
00:10:48,320 --> 00:10:59,720
comfort, let's say, and one's ethical integrity, which is more valuable, and this is the

76
00:10:59,720 --> 00:11:12,240
debate, not even really a debate, but the battle between good and comfort, this is

77
00:11:12,240 --> 00:11:19,560
why rich people can be very, very stingy because they've found goodness, they've, they've

78
00:11:19,560 --> 00:11:29,280
been way, their own comfort, higher than goodness, much, I mean for a Buddhist, it's really a no-brainer,

79
00:11:29,280 --> 00:11:36,560
you'll never kill just because it meant inconvenient, even if it meant the end of the world,

80
00:11:36,560 --> 00:11:46,560
you'll still wouldn't kill.

81
00:11:46,560 --> 00:11:50,560
Thank you, Monte.

82
00:11:50,560 --> 00:11:55,040
Another question I've been meditating for a little over a year, and I'm consistently getting

83
00:11:55,040 --> 00:12:00,320
a feeling that I'm not controlling my body, it feels like it's just moving by itself, should

84
00:12:00,320 --> 00:12:18,200
I be concerned about this?

85
00:12:18,200 --> 00:12:22,760
I mean, not quite clear what you mean, there could be one of two things, I assume you're

86
00:12:22,760 --> 00:12:29,720
talking about your ordinary movement, like, you're putting it in your mouth or walking

87
00:12:29,720 --> 00:12:34,280
down the street, if that's what you're talking about, then that's a normal experience

88
00:12:34,280 --> 00:12:39,520
to have, I mean, it's a sign that you're seeing what's really a part of what we're seeing

89
00:12:39,520 --> 00:12:44,960
into the nature of it, that really there is no self in control, the body initiates,

90
00:12:44,960 --> 00:12:54,360
but even the mind is, it's not a cell, the mind is just moments that arise and see.

91
00:12:54,360 --> 00:13:00,240
But if you're talking about these involuntary movements, like, rocking back and forth, that's

92
00:13:00,240 --> 00:13:05,440
something a little bit different than that, sometimes you want to tell that you are instigating

93
00:13:05,440 --> 00:13:12,040
the end, and you have to be careful, don't be careful, but you shouldn't stop it, and

94
00:13:12,040 --> 00:13:19,560
say to yourself, stop, I was kind of involuntary rocking movement.

95
00:13:19,560 --> 00:13:32,880
I've been involved with the Buddhist group here in the UK, but have felt sometimes unsure

96
00:13:32,880 --> 00:13:37,960
of the teachings they give, sometimes feels like it's not real Buddhist teachings.

97
00:13:37,960 --> 00:13:40,360
How much should I follow my feelings?

98
00:13:40,360 --> 00:13:44,600
Don't always understand where the feelings can be come from, but it just feels like hippie

99
00:13:44,600 --> 00:13:45,600
Buddhism.

100
00:13:45,600 --> 00:13:59,600
If you don't follow your feelings, follow my feelings, I mean, I'm being someone

101
00:13:59,600 --> 00:14:12,000
she's sitting, but I'm asking me, and so my answer has to be one of the way I teach.

102
00:14:12,000 --> 00:14:18,080
At the point of me saying that is that, what do you mean, Buddhists, do you call themselves

103
00:14:18,080 --> 00:14:25,480
Buddhist, and voila, there you have that kind of Buddhism, they believe that to become

104
00:14:25,480 --> 00:14:33,480
a Buddha, you do certain things, or in relation to the concept of a Buddha, and therefore

105
00:14:33,480 --> 00:14:39,280
that's their brand of Buddhism, so if you want to create your own brand of Buddhism, then

106
00:14:39,280 --> 00:14:43,800
follow your own feelings, but if you want to follow my brand of Buddhism, then you have

107
00:14:43,800 --> 00:14:49,680
to prepare what they do, so they teach, if you want to follow the Pauli Canon's

108
00:14:49,680 --> 00:14:57,920
version of Buddhism, then you'd have to read the whole Pauli Canon and weigh it against

109
00:14:57,920 --> 00:15:04,640
what the Pauli Canon said, but even then, actually, that's a bit inferior in the sense

110
00:15:04,640 --> 00:15:15,160
that the Pauli Canon is his books, you won't get a clear mistake, a clear comparison in one

111
00:15:15,160 --> 00:15:20,520
cases, like you can't ask the Pauli Canon what it thinks about practice x, right, you could

112
00:15:20,520 --> 00:15:29,360
ask me, sort of practice that is and so on, you know, to believe me or not, but that's

113
00:15:29,360 --> 00:15:35,240
where the Edd is, you have to decide what brand of Buddhism are you going to follow, what

114
00:15:35,240 --> 00:15:43,000
tradition, what lineage, and what everything has to be compared to that, you can also

115
00:15:43,000 --> 00:15:48,200
go by some of the things that are said in the Pauli Canon, there are general principles

116
00:15:48,200 --> 00:15:53,080
that you can follow, I mean, they would say follow the Pauli Canon and even the common

117
00:15:53,080 --> 00:15:59,640
techniques because they're old and not original, but they would have been in the importance

118
00:15:59,640 --> 00:16:07,600
of getting as close to the original teaching as we believe to bend in a real Buddha, Pauli

119
00:16:07,600 --> 00:16:15,360
Canon is pretty, as far as close as we could get, and there are principles in it that

120
00:16:15,360 --> 00:16:19,760
you can follow to what the Buddha said, this is how you can know what is a pure teaching

121
00:16:19,760 --> 00:16:41,080
of Islam, Charlie just had a comment that he he fixed his webcam and was able to see

122
00:16:41,080 --> 00:16:48,040
in the room today, so that's good, maybe it's maybe it's just minor problems with the

123
00:16:48,040 --> 00:16:53,080
new, what is that, what is it called, I forgot, what technology that is that?

124
00:16:53,080 --> 00:16:59,000
Web RTC, but we're just saying thanks a lot working across the board, it seems like some

125
00:16:59,000 --> 00:17:10,400
people both bond and I now have had problems, so inconsistent, yeah, that doesn't really

126
00:17:10,400 --> 00:17:17,800
move well, yeah, can you talk about the importance of precepts, precepts, if one doesn't follow

127
00:17:17,800 --> 00:17:26,360
precepts can one still get benefit from meditation, but it's not the precepts per se, it's

128
00:17:26,360 --> 00:17:36,960
what's behind them, it's the orderliness of the mind, the controls, but it's not really

129
00:17:36,960 --> 00:17:43,360
controlled, it's the orderliness of the nature, so there are certain mind states that cause

130
00:17:43,360 --> 00:17:51,000
disorder in the mind, and that is all equal to one's integration and makes it more difficult

131
00:17:51,000 --> 00:17:57,000
for one to attain wisdom, bugs the mind, colors the mind, colors one's perceptions

132
00:17:57,000 --> 00:18:02,160
that they think, so that's what we want to avoid, the precepts are just a guide by which

133
00:18:02,160 --> 00:18:09,480
we can know of the types of activities that are going to inevitably be rise to this, just

134
00:18:09,480 --> 00:18:18,080
that is disorderliness, so states of anger, states of greed, states of delusion and the

135
00:18:18,080 --> 00:18:30,360
guilty feelings that we have the worry and the fear from being a bad person, no, I mean,

136
00:18:30,360 --> 00:18:33,760
you should never take an increase in your life, you can still meditate, but if you're doing

137
00:18:33,760 --> 00:18:39,600
things that cause, and states of greed, anger and delusion and guilt and fear and worry

138
00:18:39,600 --> 00:18:49,920
and all that to arise, that is a very difficult period to meditate.

139
00:18:49,920 --> 00:18:55,960
I am meditating for 30 minutes, and almost all days I notice a great fear at the end of

140
00:18:55,960 --> 00:19:08,360
my meditation, should I extend my meditation periods to confront my fear?

141
00:19:08,360 --> 00:19:21,000
Well, extending a meditation is always good if you have the time, whether you have fear

142
00:19:21,000 --> 00:19:26,800
or not, but that's a valid part of your meditation, and that's is a reason why we're meditating,

143
00:19:26,800 --> 00:19:33,800
so I'm going to be free from fear and patience with it, so that we should go for it.

144
00:19:33,800 --> 00:19:40,840
At the score, I mean, you don't, I don't think you really want to necessarily just, because

145
00:19:40,840 --> 00:19:50,920
of the fear you want to just in general, extend it, but are you also doing walking meditation?

146
00:19:50,920 --> 00:19:57,000
In the beginning of 30 minutes, and that's 15 minutes, walking 15 minutes, sitting up in

147
00:19:57,000 --> 00:20:05,360
both 20 minutes, walking 20 minutes, 30 minutes, walking 30 minutes, and so on.

148
00:20:05,360 --> 00:20:09,440
Dante, did you explore many traditions before settling on this one?

149
00:20:09,440 --> 00:20:12,920
Do you feel it's important to explore different Buddhist paths?

150
00:20:12,920 --> 00:20:13,920
No.

151
00:20:13,920 --> 00:20:20,840
And only if you're not doing this path, you're already on this path for good.

152
00:20:20,840 --> 00:20:21,840
What's the rest?

153
00:20:21,840 --> 00:20:26,840
Anyone who's explored any of this?

154
00:20:26,840 --> 00:20:33,320
It's been your say, well, now do you know, or do you know there's not a better one, okay?

155
00:20:33,320 --> 00:20:34,640
I explored other ones.

156
00:20:34,640 --> 00:20:37,640
This is the best one.

157
00:20:37,640 --> 00:20:39,640
Yes.

158
00:20:39,640 --> 00:20:48,080
You're most humble.

159
00:20:48,080 --> 00:20:51,560
If it were discovered at some point in the future that the cycle of death and rebirth

160
00:20:51,560 --> 00:20:56,240
was in this understanding, would it be immoral or unethical to omit this knowledge in

161
00:20:56,240 --> 00:21:01,040
order to reduce suffering for the good of living beings?

162
00:21:01,040 --> 00:21:22,040
I mean, since it's, since it is a fact of life, it's one of the questions that work some.

163
00:21:22,040 --> 00:21:25,040
I even ask.

164
00:21:25,040 --> 00:21:28,520
But if you're, if you should be asking, it's just a general question, because there's

165
00:21:28,520 --> 00:21:34,600
nothing to do with rebirth, your question is nothing good, bad, has to do with the idea

166
00:21:34,600 --> 00:21:37,640
of whether you should hold on to teach things that go against reality, which of course

167
00:21:37,640 --> 00:21:38,640
you shouldn't.

168
00:21:38,640 --> 00:21:44,280
But if any teaching goes against reality, then the question, it's pretty much no

169
00:21:44,280 --> 00:22:03,640
brainer.

170
00:22:03,640 --> 00:22:09,280
One day while meditating, I try to observe the experiences as inestable in satisfactory

171
00:22:09,280 --> 00:22:14,400
and uncontrollable, and to observe cause and effect, but this distracts me.

172
00:22:14,400 --> 00:22:18,400
Should I just be mindful and forget about trying to realize these things?

173
00:22:18,400 --> 00:22:22,240
That's the most stable, I think, unstable.

174
00:22:22,240 --> 00:22:27,320
Yeah, I'm sure that is, yes.

175
00:22:27,320 --> 00:22:36,120
And that's the real problem with trying to observe impermanence suffering in oneself.

176
00:22:36,120 --> 00:22:41,360
And your best, you have the best of intentions to try to understand impermanence suffering

177
00:22:41,360 --> 00:22:50,360
in important pride, but it's, you know, the mind is changing all the time and it wasn't

178
00:22:50,360 --> 00:22:56,720
pleasant and it didn't look for impermanence, you know, if it's, if meditation is unpleasant,

179
00:22:56,720 --> 00:23:01,720
how could you possibly find the impermanence suffering in oneself?

180
00:23:01,720 --> 00:23:09,080
You don't have the peace of mind to do it, and you can't control your mind long enough

181
00:23:09,080 --> 00:23:13,440
to be able to see impermanence suffering in oneself, so I think it's possible to see this,

182
00:23:13,440 --> 00:23:14,440
really.

183
00:23:14,440 --> 00:23:16,440
All right.

184
00:23:16,440 --> 00:23:24,160
It's like, it's like looking for your tail, right, or trying to chase your chasing your

185
00:23:24,160 --> 00:23:33,120
tail soon, or like, it's like looking for your glasses, a gentong secretary one day was looking

186
00:23:33,120 --> 00:23:37,120
for his glasses and he was looking everywhere and he was so upset.

187
00:23:37,120 --> 00:23:44,080
He was not really admitted to say the least, he's not admitted to him, but running around

188
00:23:44,080 --> 00:23:50,800
and running around and you know, and I put him in, and then had gentong finally after some

189
00:23:50,800 --> 00:24:04,800
time I had gentong, you know, his attention turns on the, they're on your head, I'm not

190
00:24:04,800 --> 00:24:13,000
really making fun of you, it's just, it is a problem, definitely definitely don't go looking

191
00:24:13,000 --> 00:24:14,000
for these things.

192
00:24:14,000 --> 00:24:19,240
The big problem is, the problem was looking for them, it's not what I said, there was

193
00:24:19,240 --> 00:24:28,480
a big surface, actually, which is probably not very nice, but that look for them, that's

194
00:24:28,480 --> 00:24:33,080
intellectual knowledge, you won't see them, like part of the reason why you won't see them

195
00:24:33,080 --> 00:24:39,080
is because the reality is impermanence suffering in oneself, you can't make something

196
00:24:39,080 --> 00:24:48,240
like that happen, you can't create that kind of knowledge, you know, you do it, you

197
00:24:48,240 --> 00:24:53,240
can't be experiencing impermanence suffering in oneself trying to do that.

198
00:24:53,240 --> 00:25:00,080
We have to say, we have to be very clear, we're not practicing the positive, we're practicing

199
00:25:00,080 --> 00:25:06,880
the Satypatans, we're practicing the Satypatana, we're passing them up from rises, we

200
00:25:06,880 --> 00:25:18,840
have to be practicing, we're passing, we're passing, we're passing, it's not how it goes.

201
00:25:18,840 --> 00:25:26,840
It'd be nice if it was guaranteed like that, but not quite, okay, we have a few questions,

202
00:25:26,840 --> 00:25:31,960
why do people want happiness, why do people want to stay alive, and this piece more important

203
00:25:31,960 --> 00:25:36,880
than happiness in the sense of gain and pleasure, is peace the ultimate goal and what is

204
00:25:36,880 --> 00:25:37,880
some?

205
00:25:37,880 --> 00:25:41,680
We technically had our questioners as to whether they've meditated before.

206
00:25:41,680 --> 00:25:47,000
Let it be meditated, so no, you don't get to ask all those questions, you want to ask

207
00:25:47,000 --> 00:25:54,880
ask me questions, not a rule, but in this case, too many questions, the sign that you

208
00:25:54,880 --> 00:26:01,920
have to do some meditation, you have to read my booklet, it's important to help

209
00:26:01,920 --> 00:26:04,920
you decide on the right question.

210
00:26:04,920 --> 00:26:11,160
Bontay, is the way of teaching by saying thus have I heard particular to the Theravada

211
00:26:11,160 --> 00:26:16,520
teachings, where is that considered the way in all the images of Buddhism?

212
00:26:16,520 --> 00:26:22,600
Interesting question, the idea of that's of I heard is really interesting and it's a

213
00:26:22,600 --> 00:26:28,000
matter of, they actually talk about it more than you would think, just by the commentaries

214
00:26:28,000 --> 00:26:36,880
and there's this wonderful project completed in Burma, but they tried to start it in Thailand,

215
00:26:36,880 --> 00:26:44,160
I don't know how far they got, where they take apart the Buddha's teaching word by word,

216
00:26:44,160 --> 00:26:54,000
which word is dissected for me, so the first word in the Brahmajalasu, and I think the

217
00:26:54,000 --> 00:26:59,880
entire Brahmajalasu was translated and it was a whole volume, I think volume just on the

218
00:26:59,880 --> 00:27:09,920
surface, on the whole page, just the body one may suit, at least the page, maybe more.

219
00:27:09,920 --> 00:27:21,400
The idea behind it is that you are describing the island that was describing what he had

220
00:27:21,400 --> 00:27:33,360
heard, received from the Buddha himself, and I think some part of it is making it a statement

221
00:27:33,360 --> 00:27:38,640
of truth, you don't actually know that the Buddha, this is what we have heard, this

222
00:27:38,640 --> 00:27:44,640
is what has been heard, but I don't know that that's really how it's explained, the

223
00:27:44,640 --> 00:27:51,640
blocks is more like, you can believe this because it was heard by a man in the front of

224
00:27:51,640 --> 00:27:56,880
the Buddha, the Buddha himself told this to a man and the Buddha would come back and tell

225
00:27:56,880 --> 00:28:17,360
and under, during one session, there's a little bit more than that, terror about a Buddhism

226
00:28:17,360 --> 00:28:26,200
doesn't have that, it's from the polycannon, and it appears to have been from the original

227
00:28:26,200 --> 00:28:33,440
transmission, and this was how they were remembered to start as starting with the words

228
00:28:33,440 --> 00:28:36,280
able to suit up.

229
00:28:36,280 --> 00:28:40,600
I don't know whether it's in the Sanskrit version, it's in the Sanskrit version left,

230
00:28:40,600 --> 00:28:49,560
but whether it's in the Chinese, or the Chinese version, similar to the poly, I don't

231
00:28:49,560 --> 00:28:54,320
know whether it's in the Chinese version, but it's not about terror about the Mayan, it's

232
00:28:54,320 --> 00:29:01,600
about groups of texts to the text, it's how they write, the polycannon has it, and it's

233
00:29:01,600 --> 00:29:06,600
right in the Sanskrit version, I don't know, it's probably very easy to find out, but

234
00:29:06,600 --> 00:29:16,840
I'm just going to put that question to the test.

235
00:29:16,840 --> 00:29:27,320
During one session of meditation, can we focus on only one of the Satipatana?

236
00:29:27,320 --> 00:29:32,120
What's a good answer to that?

237
00:29:32,120 --> 00:29:47,800
They're all there, so yeah, you could, it'd be ignoring a lot of stuff, so I'm walking

238
00:29:47,800 --> 00:29:53,800
on one leg, we've got two of them, okay, everyone get very far.

239
00:29:53,800 --> 00:29:57,440
What do you think of the Dharma Overground, is it a good source of information?

240
00:29:57,440 --> 00:30:05,720
We don't really think about the Dharma Overground all that much, but that's a lot of these,

241
00:30:05,720 --> 00:30:17,200
it's hard for me to answer those questions, but it's finally under a rock.

242
00:30:17,200 --> 00:30:29,000
Some qualities should fulfill to arise different knowledges.

243
00:30:29,000 --> 00:30:35,920
Satipatana, don't worry about the rest, it's hard to be some pajano, Satin, there's three

244
00:30:35,920 --> 00:30:48,960
qualities.

245
00:30:48,960 --> 00:30:52,840
One thing my respect is deep, I would like to ask you, do you know whether meditation

246
00:30:52,840 --> 00:30:54,760
is more important than happiness?

247
00:30:54,760 --> 00:31:08,520
Again, someone who's not meditating, you're meditator, not you, I appreciate your respect,

248
00:31:08,520 --> 00:31:16,120
thank you that's kind, I'm not going to answer it because it sounds like a question

249
00:31:16,120 --> 00:31:19,320
if someone who hasn't really done meditation.

250
00:31:19,320 --> 00:31:23,600
Yeah, I think there's a lot of new people here tonight, maybe not know about the green

251
00:31:23,600 --> 00:31:26,720
and the orange names and all that.

252
00:31:26,720 --> 00:31:33,320
We can tell whether you've been meditating with this, yes, the orange name tells us that

253
00:31:33,320 --> 00:31:35,840
you haven't logged in to meditate.

254
00:31:35,840 --> 00:31:39,360
Just because your orange doesn't mean I won't answer your question, that's not the case,

255
00:31:39,360 --> 00:31:43,800
but and just because you've never meditated with this doesn't, we won't answer your question,

256
00:31:43,800 --> 00:31:45,840
but it's a good excuse not to.

257
00:31:45,840 --> 00:31:52,040
I'm going to hear that from time to time, don't use that excuse, then you have to meditate

258
00:31:52,040 --> 00:31:53,760
with.

259
00:31:53,760 --> 00:31:58,360
So if someone meditates you'll answer anything then, more likely, why can't you use that

260
00:31:58,360 --> 00:32:08,680
excuse, that's all, I can share my head hurts, you're making me head hurt.

261
00:32:08,680 --> 00:32:19,040
My brain has stopped working, I like that one, that happens, what should be the frequency

262
00:32:19,040 --> 00:32:23,480
of noting, or how often should we strive to know?

263
00:32:23,480 --> 00:32:30,400
My housey side, I remember reading somewhere where he said once a second, that seems

264
00:32:30,400 --> 00:32:37,320
recent, that's kind of like, wouldn't go any faster than that, but it's a bit slower,

265
00:32:37,320 --> 00:32:39,520
that's fine.

266
00:32:39,520 --> 00:32:47,640
I have a lot of doubts about the practice, doubt that meditation works and doubt that it's

267
00:32:47,640 --> 00:32:49,840
possible to attain nibana.

268
00:32:49,840 --> 00:32:50,840
How can I deal with this?

269
00:32:50,840 --> 00:32:56,000
How do you feel about that, doubt does that, doubt means you're happy, do you enjoy it

270
00:32:56,000 --> 00:32:57,720
when you have that doubt?

271
00:32:57,720 --> 00:33:05,840
Most likely that doubt is unpleasant and all the stress and the ability, the ability to

272
00:33:05,840 --> 00:33:19,560
most likely, I would recommend that you meditate, saying to yourself, doubt the doubt you

273
00:33:19,560 --> 00:33:30,400
should see is at which point you'll feel better, I don't know, it's very important.

274
00:33:30,400 --> 00:33:37,040
So bearing does meditate daily, so you just have to log in here to do your meditation

275
00:33:37,040 --> 00:33:42,800
and then you create a record and then your questions will be, welcome.

276
00:33:42,800 --> 00:33:52,120
You should be practicing, maybe take, here we are, this is kind of an odd question, meditation

277
00:33:52,120 --> 00:34:01,120
is supposed to leave the happiness, it should be more important than happiness, it's like,

278
00:34:01,120 --> 00:34:08,080
it's the cause more important than the effect, well, I guess so, because the effect is

279
00:34:08,080 --> 00:34:32,760
supposed to be the effect, but the cause is the effect, I think we're all caught up,

280
00:34:32,760 --> 00:34:47,320
I've been fairly rude to my audience today, not answering your questions, hey, you guys

281
00:34:47,320 --> 00:34:54,840
want to go into the, should we try going into this chat room, we'll do a chat for a bit,

282
00:34:54,840 --> 00:35:01,680
and the broadcast, and let's have an informal video chat and see how, see if it works,

283
00:35:01,680 --> 00:35:05,120
it probably won't work for me, I'm guessing, I won't see any of you, and then it'll just

284
00:35:05,120 --> 00:35:12,480
be the end for me, the night and I'll see you in tomorrow, if not, see you in the chat room.

285
00:35:12,480 --> 00:35:39,600
Thank you, Bhante, thank you, welcome.

