1
00:00:00,000 --> 00:00:23,000
And did say, Tom, one is Gaia, body, two is Wade, and feelings, three is did that and it means mind, and four is dhamma, which means dhamma can't really be translated.

2
00:00:23,000 --> 00:00:36,000
But refers to the five hindrances, the six senses, the five aggregates, et cetera.

3
00:00:36,000 --> 00:00:41,000
So the question is, what do we mean by v-passana?

4
00:00:41,000 --> 00:00:49,000
Because we described the four setipatana and all of our practice is based around the setipatana, but what does v-passana mean?

5
00:00:49,000 --> 00:00:54,000
Why do we call it v-passana?

6
00:00:54,000 --> 00:01:00,000
What do we just call it setipatana or mindfulness or sati meditation meditation?

7
00:01:00,000 --> 00:01:02,000
Sati.

8
00:01:02,000 --> 00:01:04,000
What is the use of the word v-passana?

9
00:01:04,000 --> 00:01:07,000
What does it mean?

10
00:01:07,000 --> 00:01:10,000
v-passana means three things.

11
00:01:10,000 --> 00:01:19,000
Well, v-passana means to see clearly, v-passana means clearly, b-passana means seeing clearly, this is v-passana.

12
00:01:19,000 --> 00:01:23,000
So the question really is, what are we trying to see clearly?

13
00:01:23,000 --> 00:01:28,000
The answer is we're trying to see clearly three things.

14
00:01:28,000 --> 00:01:39,000
With the claim that when you practice v-passana meditation in mind with the four foundations of mindfulness,

15
00:01:39,000 --> 00:01:49,000
you will see first of all that everything inside of yourself and in the world around you, everything.

16
00:01:49,000 --> 00:01:58,000
It's impermanent, uncertain, unstable, changing, chaotic.

17
00:01:58,000 --> 00:02:11,000
Number two, you'll see that inside of yourself and in the whole world around you, everything is dukka suffering or unsatisfying,

18
00:02:11,000 --> 00:02:15,000
not happiness, not a source for happiness.

19
00:02:15,000 --> 00:02:23,000
No thing in the world can be a cause for true happiness because it's unsatisfying.

20
00:02:23,000 --> 00:02:33,000
And number three, you'll see that inside of yourself and in the world around you, nothing is self.

21
00:02:33,000 --> 00:02:36,000
Nothing is yours belongs to you.

22
00:02:36,000 --> 00:02:43,000
Nothing has its own independent existence.

23
00:02:43,000 --> 00:02:48,000
Nothing belongs to anything.

24
00:02:48,000 --> 00:02:52,000
Nothing is you or yours, please.

25
00:02:52,000 --> 00:02:56,000
These three things mean just what it means to see clearly.

26
00:02:56,000 --> 00:03:06,000
And this is really the whole of the path of insight and as you practice setting,

27
00:03:06,000 --> 00:03:15,000
the insight into these three things will arise and look at deeper and deeper your understanding of it.

28
00:03:15,000 --> 00:03:23,000
Now, some of you, I think, understand my take or take the hard tradition looks at these three things,

29
00:03:23,000 --> 00:03:37,000
but I think there are still lots of people who hear about this and then have a difficult time understanding, impermanent suffering, and on top.

30
00:03:37,000 --> 00:03:46,000
Because I think of it far too intellectually or they doubt or, you know, if you tell someone what to expect,

31
00:03:46,000 --> 00:03:54,000
it's a weird psychological truth that more clearly you detail what the person can expect.

32
00:03:54,000 --> 00:04:04,000
The harder it is for them to actually believe that it's going to happen or just let it happen because they become filled with expectations

33
00:04:04,000 --> 00:04:13,000
and ruin any opportunity for natural experience.

34
00:04:13,000 --> 00:04:22,000
So people who meditators will often waste a lot of time and energy,

35
00:04:22,000 --> 00:04:29,000
wondering and doubting about impermanence, I think, and on top for a few like they're not cut out for it,

36
00:04:29,000 --> 00:04:35,000
so they're not able to accomplish the goal of the practice.

37
00:04:35,000 --> 00:04:46,000
And so I've done a video on this to try and make clear that make clear that with these three concepts,

38
00:04:46,000 --> 00:04:53,000
these three characteristics mean, I think it's called the nature of reality.

39
00:04:53,000 --> 00:05:05,000
So the idea was that meditators will often complain.

40
00:05:05,000 --> 00:05:17,000
They'll be practicing for a while and they'll complain that when meditation's not going so well.

41
00:05:17,000 --> 00:05:25,000
And so the mind is in chaos, it's uncomfortable, and they can't control the experience.

42
00:05:25,000 --> 00:05:29,000
I just can't hack it, I can't do this, it's just not working.

43
00:05:29,000 --> 00:05:37,000
Of course that's exactly, that's an incredible practice if a person gets to that stage you'll be very happy that they got there.

44
00:05:37,000 --> 00:05:41,000
Because it means they see impermanence suffering in non-self.

45
00:05:41,000 --> 00:05:47,000
Yes, it's not working, that's the point, the point is that the world doesn't work the way we think it does.

46
00:05:47,000 --> 00:05:49,000
This is why we suffer.

47
00:05:49,000 --> 00:05:52,000
All this time we're wondering why, what's wrong?

48
00:05:52,000 --> 00:05:54,000
Why is it that we have to suffer?

49
00:05:54,000 --> 00:05:57,000
What is the cause of our suffering when we start to see the cause?

50
00:05:57,000 --> 00:06:05,000
Not the experiences we have, it's our understanding of that.

51
00:06:05,000 --> 00:06:18,000
As being satisfied, as stable when they're unstable, satisfying when they're unsatisfying and controllable and we can't control them, they don't belong to us.

52
00:06:18,000 --> 00:06:27,000
So it's not a matter of fixing your practice, it's a matter of changing the way you look at it and learning to let go as you start to see that you can't do anything about it.

53
00:06:27,000 --> 00:06:32,000
It's not due, it's not due, it's not due to control.

54
00:06:32,000 --> 00:06:43,000
It's exactly what we expect to happen, it should be chaotic, it should be unsatisfying, it should be uncontrollable and to be finally let go of it, all of it.

55
00:06:43,000 --> 00:06:55,000
Then your practice is much better because you just watch and you're unfazed by the changes in the chaos.

56
00:06:55,000 --> 00:07:00,000
So that's a little bit of background, talking about the three characteristics, and there's some good opportunity to bring them up.

57
00:07:00,000 --> 00:07:11,000
Because here we have the six numbers that read your body and are conducive to enlightenment.

58
00:07:11,000 --> 00:07:19,000
And the first three are in regards to the three characteristics, but one reason why this suit is specifically interesting.

59
00:07:19,000 --> 00:07:24,000
It's one of the examples where the Buddha relates the three characteristics.

60
00:07:24,000 --> 00:07:30,000
So it's actually says something else about the three characteristics.

61
00:07:30,000 --> 00:07:43,000
So we have a Nietzsche-Sanya means perception or something like that, recognition, recognizing something as impermanent.

62
00:07:43,000 --> 00:07:49,000
So when you perceive impermanent, you recognize impermanence.

63
00:07:49,000 --> 00:07:57,000
That's a very important thing for seeing that this mind that I thought was stable is unstable.

64
00:07:57,000 --> 00:08:04,000
And then this body, this reality, this world, stability is illusion.

65
00:08:04,000 --> 00:08:14,000
Seeing things arise and seeing that the only reality is a momentary experience that arises and sees this moment after moment after moment.

66
00:08:14,000 --> 00:08:19,000
A Nietzsche-Sanya.

67
00:08:19,000 --> 00:08:28,000
But the second one says a Nietzsche-Dook-Sanya, which means perception or recognition of Dook-S offering.

68
00:08:28,000 --> 00:08:34,000
But a Nietzsche in regards to what is impermanent.

69
00:08:34,000 --> 00:08:46,000
So it says that basically it says that one way of interpreting it is to say that we see suffering because things are impermanent.

70
00:08:46,000 --> 00:08:52,000
As you watch things are impermanent, you realize that they're not satisfying, they're not happiness.

71
00:08:52,000 --> 00:08:57,000
You give up the idea that these things can bring you happiness, even pleasure.

72
00:08:57,000 --> 00:09:05,000
You see pleasure as Dooka, because it's not Sooka, it can't make you happy.

73
00:09:05,000 --> 00:09:07,000
Pleasure is something that comes and goes.

74
00:09:07,000 --> 00:09:12,000
And ends up being meaningless, pointless, useless.

75
00:09:12,000 --> 00:09:18,000
And if you cling to it, you're disappointed because it changes.

76
00:09:18,000 --> 00:09:21,000
You don't end up with any satisfaction or peace.

77
00:09:21,000 --> 00:09:31,000
You just end up with more wanting, because it's here and then it's gone.

78
00:09:31,000 --> 00:09:34,000
And the third one is Dooka-Sanya.

79
00:09:34,000 --> 00:09:40,000
So based on Dooka, we see Anatha.

80
00:09:40,000 --> 00:09:45,000
And so the idea being that once you see suffering, you let go of it.

81
00:09:45,000 --> 00:09:54,000
You give up the sense of trying to control, maintain, keep, or avoid or fix things.

82
00:09:54,000 --> 00:09:57,000
Once you see suffering, you let go of it.

83
00:09:57,000 --> 00:09:59,000
So Anatha is all about letting go.

84
00:09:59,000 --> 00:10:01,000
It's how the letting go occurs.

85
00:10:01,000 --> 00:10:06,000
Once you see something suffering, you have no desire for it to be.

86
00:10:06,000 --> 00:10:08,000
You are yours.

87
00:10:08,000 --> 00:10:12,000
You lose any attachment to it.

88
00:10:12,000 --> 00:10:16,000
And you drop it whenever you experience it.

89
00:10:16,000 --> 00:10:23,000
Find no benefit to getting caught up in it, clinging to it.

90
00:10:23,000 --> 00:10:26,000
Trying to keep it, trying to keep it away.

91
00:10:26,000 --> 00:10:30,000
Is it seeing Anatha in Dooka-Sanya?

92
00:10:34,000 --> 00:10:36,000
I mean, this sounds kind of awful.

93
00:10:36,000 --> 00:10:39,000
I think if you just hear about the three characters, it sounds like,

94
00:10:39,000 --> 00:10:41,000
wow, that's an awful pessimistic way of looking at things.

95
00:10:41,000 --> 00:10:43,000
Yeah, I guess it is.

96
00:10:43,000 --> 00:10:46,000
I mean, it's a very negative way of looking at things.

97
00:10:46,000 --> 00:10:48,000
I mean, we don't claim that this is a good thing.

98
00:10:48,000 --> 00:10:52,000
This is, in fact, considered to be the problem.

99
00:10:52,000 --> 00:10:56,000
But the wonderful thing is that once you realize that you really do let go,

100
00:10:56,000 --> 00:11:01,000
you really do find true peace and happiness in freedom from suffering.

101
00:11:01,000 --> 00:11:10,000
It's very real and undeniable, unshakeable.

102
00:11:10,000 --> 00:11:13,000
So if you're seeing these things, it's actually wonderful.

103
00:11:13,000 --> 00:11:15,000
It's what really frees you.

104
00:11:15,000 --> 00:11:18,000
If you think of a person who is addicted to drugs,

105
00:11:18,000 --> 00:11:21,000
it's only when they see these three things about the drugs.

106
00:11:21,000 --> 00:11:23,000
And it's not stable.

107
00:11:23,000 --> 00:11:24,000
It's not satisfying.

108
00:11:24,000 --> 00:11:25,000
It's not controllable.

109
00:11:25,000 --> 00:11:27,000
They're not in charge.

110
00:11:27,000 --> 00:11:30,000
I think it's scared enough to try and give up the drug.

111
00:11:30,000 --> 00:11:33,000
Once they give it up, they feel a lot better.

112
00:11:33,000 --> 00:11:38,000
This is the first three.

113
00:11:38,000 --> 00:11:43,000
This is the last three are more getting closer to enlightenment

114
00:11:43,000 --> 00:11:46,000
or they're more on the side of the actual enlightenment.

115
00:11:46,000 --> 00:11:51,000
So pahana, pahana, sanya, pahana, sanya means giving up.

116
00:11:51,000 --> 00:11:54,000
This is when you give up your attachment.

117
00:11:54,000 --> 00:11:55,000
So you give up greed.

118
00:11:55,000 --> 00:11:56,000
You give up anger.

119
00:11:56,000 --> 00:11:58,000
You give up delusion.

120
00:11:58,000 --> 00:12:03,000
You give up bad mind states on wholesome unpleasant

121
00:12:03,000 --> 00:12:07,000
and beneficial mind states as a result.

122
00:12:07,000 --> 00:12:10,000
Pahana.

123
00:12:10,000 --> 00:12:14,000
You destroy or give up or remove.

124
00:12:14,000 --> 00:12:17,000
They're seeing permanent suffering in themselves.

125
00:12:17,000 --> 00:12:18,000
You stop trying to fix things.

126
00:12:18,000 --> 00:12:21,000
You stop trying to make it better.

127
00:12:21,000 --> 00:12:23,000
And then your mind is free.

128
00:12:23,000 --> 00:12:28,000
You give up your attachment to you.

129
00:12:28,000 --> 00:12:32,000
Number four, number five, viragasanya.

130
00:12:32,000 --> 00:12:36,000
Viraga means the absence of desire.

131
00:12:36,000 --> 00:12:39,000
So once you give up the thoughts, then all the clinging is gone.

132
00:12:39,000 --> 00:12:42,000
Viraga is actually another word for nibana.

133
00:12:42,000 --> 00:12:47,000
So this is the moment where the mind releases.

134
00:12:47,000 --> 00:12:49,000
Viraga.

135
00:12:49,000 --> 00:12:55,000
Pahana means when you abandon and you're free, nibilaga.

136
00:12:55,000 --> 00:12:58,000
Nibro, and number six, nibro, dasanya.

137
00:12:58,000 --> 00:13:01,000
Nibro, dasanya means cessation.

138
00:13:01,000 --> 00:13:08,000
So nibro is the fruit where you enter into cessation.

139
00:13:08,000 --> 00:13:21,000
There is no arising and no ceasing from the peace.

140
00:13:21,000 --> 00:13:27,000
So these are the six dumbers that lead to knowledge.

141
00:13:27,000 --> 00:13:32,000
Technically speaking, this all happens.

142
00:13:32,000 --> 00:13:40,000
And you can look at it both ways.

143
00:13:40,000 --> 00:13:48,000
But the last three, they come about because of a perfect understanding of impermanence of

144
00:13:48,000 --> 00:13:49,000
bringing on some.

145
00:13:49,000 --> 00:13:54,000
As you practice, it's going to become clear and clear until eventually you get to the point

146
00:13:54,000 --> 00:14:00,000
where you see just in one moment, so clearly impermanence or suffering or non-self, one of them

147
00:14:00,000 --> 00:14:02,000
become more clear than the other.

148
00:14:02,000 --> 00:14:08,000
And based on that, it will be the giving up pahana.

149
00:14:08,000 --> 00:14:11,000
And then Viraga, the freedom.

150
00:14:11,000 --> 00:14:16,000
Nibro, das cessation.

151
00:14:16,000 --> 00:14:20,000
And once you've seen nibana, that's what's understood as vidja here.

152
00:14:20,000 --> 00:14:25,000
Vidja means knowledge of nibana, knowledge of enlightenment.

153
00:14:25,000 --> 00:14:26,000
And that changes you.

154
00:14:26,000 --> 00:14:29,000
It changes the way you look at the world.

155
00:14:29,000 --> 00:14:34,000
It's a pretty useful for attachment and clinging.

156
00:14:34,000 --> 00:14:43,000
So I'm not a big teaching tonight, but I thought important to always important to go over

157
00:14:43,000 --> 00:14:47,000
the M3 characteristics again and remind us that that's what we're aiming for.

158
00:14:47,000 --> 00:14:50,000
So once you see that, it's quite simple.

159
00:14:50,000 --> 00:14:56,000
To see those things and the things that you were clinging to, you let go of.

160
00:14:56,000 --> 00:15:00,000
It was interesting specifically about the symptoms, how it relates to the three.

161
00:15:00,000 --> 00:15:05,000
And dukha comes from anita, and anata comes from dukha.

162
00:15:05,000 --> 00:15:09,000
So if you're worrying about anata, it's actually in many.

163
00:15:09,000 --> 00:15:16,000
One explanation of it is that it's the, this is not worth seeing as self.

164
00:15:16,000 --> 00:15:22,000
It means that when you see something as impermanence, and then as a result see that it's unsatisfied,

165
00:15:22,000 --> 00:15:30,000
you give it up, you give up any idea, that's not mine, I don't want that.

166
00:15:30,000 --> 00:15:33,000
We lose all desire for it.

167
00:15:33,000 --> 00:15:41,000
We're all concerned over it in terms of trying to fix it or control it.

168
00:15:41,000 --> 00:15:46,000
So there you go, that's the dumbo for tonight.

169
00:15:46,000 --> 00:15:54,000
And again, I forgot to start recording, we got to start recording notification.

170
00:15:54,000 --> 00:16:05,000
Although they were setting up automation for that, so maybe it's automated by now.

171
00:16:05,000 --> 00:16:18,000
I don't think, are you ready for some questions?

172
00:16:18,000 --> 00:16:21,000
You can, you can hear me okay?

173
00:16:21,000 --> 00:16:23,000
Yes.

174
00:16:23,000 --> 00:16:33,000
I've read that there are four kinds of clear comprehension, one of purpose, two of suitability, three of resort, and four of non dilution.

175
00:16:33,000 --> 00:16:35,000
Are those practical advices?

176
00:16:35,000 --> 00:16:45,000
In other words, to actively know and recall the purpose and suitability of an action while before or after doing it?

177
00:16:45,000 --> 00:16:46,000
Yeah, right.

178
00:16:46,000 --> 00:16:55,000
So the first three are not what is referred to in set at the time of meditation.

179
00:16:55,000 --> 00:16:58,000
In our meditation, it's the fourth one of non dilution.

180
00:16:58,000 --> 00:17:03,000
So it's seeing things clear, but the other three are clear comprehension in a more mundane sense.

181
00:17:03,000 --> 00:17:05,000
But they're very important and useful to teach.

182
00:17:05,000 --> 00:17:08,000
So purpose is why am I doing something?

183
00:17:08,000 --> 00:17:13,000
Because it is the reason for doing it.

184
00:17:13,000 --> 00:17:16,000
I'm realizing that you're doing something, why are you eating?

185
00:17:16,000 --> 00:17:21,000
I'm eating to keep my body alive, so comprehension of that.

186
00:17:21,000 --> 00:17:28,000
suitability is it suitable to do this, when you're going to say or do some things that's suitable.

187
00:17:28,000 --> 00:17:32,000
If you're distracted, maybe it's more suitable to sit.

188
00:17:32,000 --> 00:17:38,000
You're tired, maybe it's more suitable to walk or the drowsy.

189
00:17:38,000 --> 00:17:40,000
Sort of looks like an upper walk.

190
00:17:40,000 --> 00:17:49,000
Resort is knowing the right place to what is appropriate, where is an appropriate place to be.

191
00:17:49,000 --> 00:17:56,000
So knowing where you shouldn't go, if someone invites you to a party, you generally shouldn't go.

192
00:17:56,000 --> 00:17:58,000
That kind of thing.

193
00:17:58,000 --> 00:18:08,000
Staying away from situations that are going to take you out of your spiritual path.

194
00:18:08,000 --> 00:18:13,000
So the first three are much more practical, worldly.

195
00:18:13,000 --> 00:18:20,000
But worldly in the spiritual sense, I mean, among mundane, only the fourth one is directly related to the path.

196
00:18:20,000 --> 00:18:27,000
They're intrinsically tied to the practice.

197
00:18:27,000 --> 00:18:29,000
And there was another question.

198
00:18:29,000 --> 00:18:31,000
Is there a reason why clear comprehension?

199
00:18:31,000 --> 00:18:38,000
Sumpajana is within the first foundation of mindfulness, the body, in the Satyepatana Sutta.

200
00:18:38,000 --> 00:18:49,000
Naively, I would associate it with the mind from hearing the words clear comprehension in contrast to mindfulness of posture, breathing, or repulsiveness of the body.

201
00:18:49,000 --> 00:18:51,000
You know, sumpajana is in all four.

202
00:18:51,000 --> 00:18:54,000
And if you read the Satyepatana Sutta, it's early.

203
00:18:54,000 --> 00:18:55,000
You'll see that.

204
00:18:55,000 --> 00:18:58,000
It says, ah, dapi, sumpajana, sapimana.

205
00:18:58,000 --> 00:19:02,000
So the three characteristics of mindfulness practice.

206
00:19:02,000 --> 00:19:08,000
Ah, dapi is effort or energy. Sumpajana is with the non-delusion.

207
00:19:08,000 --> 00:19:21,000
And Satyepatana is the recollection of the remembrance of the clear understanding of the object as it is.

208
00:19:21,000 --> 00:19:27,000
Or is it the reminding of yourself, what it is?

209
00:19:27,000 --> 00:19:38,000
So it's in all three, but you have the sumpajana baba, which is just a group, or just a set of teachings about the body that use the word sumpajana.

210
00:19:38,000 --> 00:19:40,000
It's just using that word.

211
00:19:40,000 --> 00:19:49,000
What it's really talking about is you use mindfulness when you're sominji-dapahadi day, you know, extender.

212
00:19:49,000 --> 00:20:03,000
So easy day, you know, you can't remember all the words, but it's like when you're walking forward, walking back a day.

213
00:20:03,000 --> 00:20:12,000
Come at the sumpajana.

214
00:20:12,000 --> 00:20:22,000
Ah, when drinking, when eating, when you're an eating, which are, which are the, which are not currently possible.

215
00:20:22,000 --> 00:20:25,000
Which are not possible, what got her name, that's a bit of it.

216
00:20:25,000 --> 00:20:30,000
When you're an eating and defecating.

217
00:20:30,000 --> 00:20:34,000
One pat, one is one, the sumpajana kari hote.

218
00:20:34,000 --> 00:20:40,000
One is one who cultivates sumpajana, clear comprehension.

219
00:20:40,000 --> 00:20:45,000
It's actually just a sort of a whole heart, a mode of expression.

220
00:20:45,000 --> 00:20:53,000
He's still referring about meditation practice, but he's just detailing the, what we call, the area about the,

221
00:20:53,000 --> 00:21:00,000
the true, that area about the minor posturcy body, minor movement.

222
00:21:00,000 --> 00:21:14,000
So by extension, everything you do, I'm removing your name, being mindful.

223
00:21:14,000 --> 00:21:22,000
What are some good books to read to help advance knowledge of Buddhism for some, someone who already knows the basics?

224
00:21:22,000 --> 00:21:32,000
I read the Buddha's teaching, I read the sutas.

225
00:21:32,000 --> 00:21:37,000
Mostly, the Buddha is translated.

226
00:21:37,000 --> 00:21:42,000
I would read the way of mindfulness in that last question, that's a good book.

227
00:21:42,000 --> 00:21:46,000
It's on the internet, there's a link to it.

228
00:21:46,000 --> 00:21:56,000
Anything by Mahasu Saiyada, that's what I would recommend personally.

229
00:21:56,000 --> 00:22:04,000
You can also read there's a book we just got.

230
00:22:04,000 --> 00:22:06,000
Great disciples of the Buddha.

231
00:22:06,000 --> 00:22:09,000
Does that have a camera?

232
00:22:09,000 --> 00:22:13,000
No, that's got a little glare on it, there you go.

233
00:22:13,000 --> 00:22:15,000
Great disciples of the Buddha.

234
00:22:15,000 --> 00:22:24,000
That's not as immediately useful, but if you're a real Buddhist, it's a must read, I think.

235
00:22:24,000 --> 00:22:31,000
If you've got the time, it's not a must read, but it's the kind of thing that you want to really get a feeling for Buddhism as a Buddhist.

236
00:22:31,000 --> 00:22:40,000
It's good to learn about the saints, so to speak, the hagiography, that's what that is.

237
00:22:40,000 --> 00:22:55,000
When I just began my meditation, my meditating, my first to realize that I emotion.

238
00:22:55,000 --> 00:23:00,000
For example, I have thoughts that the bottom line for them is an emotion leading to a defilement.

239
00:23:00,000 --> 00:23:06,000
For example, if I'm feeling insecure, I begin to plan ahead for gaining more spiritual influence over people,

240
00:23:06,000 --> 00:23:12,000
or being in a status that is overpowering others, which is exactly greed, and in general,

241
00:23:12,000 --> 00:23:18,000
I believe that it's just my emotions or defilements that trigger any thought, at least in my case.

242
00:23:18,000 --> 00:23:26,000
So when realizing this, while meditating, finding a source would once say, feeling, feeling,

243
00:23:26,000 --> 00:23:33,000
I imagine you would say that I should have stopped at thinking thinking, but isn't that not really true,

244
00:23:33,000 --> 00:23:39,000
since that is just acknowledging a consequence and not the source of thinking?

245
00:23:39,000 --> 00:23:42,000
Okay, let's point this apart.

246
00:23:42,000 --> 00:23:45,000
I believe it's a very bad statement, don't go with that.

247
00:23:45,000 --> 00:23:51,000
Whatever you believe is useless, it doesn't mean anything to us.

248
00:23:51,000 --> 00:23:58,000
It's in fact not true that defilements always trigger thoughts, thoughts can come from all sorts of causes.

249
00:23:58,000 --> 00:24:08,000
They can come from indigestion, and they may sometimes be caused by, or part of the cause,

250
00:24:08,000 --> 00:24:13,000
or part of the cause would be a defilement, but don't go ever by belief because your conjecture,

251
00:24:13,000 --> 00:24:15,000
it's conjecture, and that's not useful for us.

252
00:24:15,000 --> 00:24:18,000
We don't care about conjecture.

253
00:24:18,000 --> 00:24:23,000
We only want to see the nature of things as they are, and so at least to the second point is that you say,

254
00:24:23,000 --> 00:24:29,000
well, meditating, finding a source. Now, I hope that I never in my booklet

255
00:24:29,000 --> 00:24:33,000
explain meditation as a practice of finding a source.

256
00:24:33,000 --> 00:24:35,000
That's not what our meditation is.

257
00:24:35,000 --> 00:24:40,000
When we talk about meditation on this side, there's nothing to do with finding a source.

258
00:24:40,000 --> 00:24:45,000
Meditation is about as I just explained, using the four foundations of mindfulness,

259
00:24:45,000 --> 00:24:51,000
to understand that things are impermanent, things, and by things I mean things that arise here and now,

260
00:24:51,000 --> 00:24:54,000
in the present moment, are impermanent, unsatisfying, and uncontrollable.

261
00:24:54,000 --> 00:24:59,000
So our object will always be the thing that is arising and ceasing in the present moment,

262
00:24:59,000 --> 00:25:03,000
not its causes, not its effect, will always be that thing.

263
00:25:03,000 --> 00:25:06,000
Now, in the beginning, you do see cause and effect.

264
00:25:06,000 --> 00:25:08,000
That's the very beginning of the practice.

265
00:25:08,000 --> 00:25:14,000
You'll start to see what leads to what, and you'll understand karma,

266
00:25:14,000 --> 00:25:19,000
but as the practice progresses, it's much, much more about just seeing things

267
00:25:19,000 --> 00:25:22,000
in the present moment as they arise and cease.

268
00:25:22,000 --> 00:25:25,000
There'll be a lot of cause and effect that you'll see.

269
00:25:25,000 --> 00:25:28,000
You'll see this causes, this causes that, that's not the practice.

270
00:25:28,000 --> 00:25:33,000
The path is to see things arising and ceasing.

271
00:25:33,000 --> 00:25:39,000
And so, when you think of something, you should say thinking, thinking,

272
00:25:39,000 --> 00:25:41,000
there's no going beyond that.

273
00:25:41,000 --> 00:25:47,000
If you think that feeling is what caused it, that's just the more thinking.

274
00:25:47,000 --> 00:25:58,000
So when you ask, if you think something, I assume you're referring to a thought.

275
00:25:58,000 --> 00:26:04,000
And if a thought arises, what you're to do is to say to yourself, thinking, thinking,

276
00:26:04,000 --> 00:26:07,000
because it reminds you that that's just a thought.

277
00:26:07,000 --> 00:26:11,000
Nothing more, nothing less, with no reference to the cause of it,

278
00:26:11,000 --> 00:26:13,000
or what may have caused it, et cetera, et cetera.

279
00:26:13,000 --> 00:26:18,000
You would never ever say feeling, feeling, because that's not what you're experiencing.

280
00:26:18,000 --> 00:26:21,000
That has nothing to do with what's currently on your mind.

281
00:26:21,000 --> 00:26:24,000
They're currently in your realm of experience.

282
00:26:24,000 --> 00:26:29,000
That would be wrong practice unequivocally.

283
00:26:29,000 --> 00:26:31,000
It would be related to the past.

284
00:26:31,000 --> 00:26:36,000
So it would be wrong mindfulness, which I have to give a talk on next June.

285
00:26:36,000 --> 00:26:48,000
And I'm still not 100% sure what they're expecting from that talk.

286
00:26:48,000 --> 00:26:51,000
The idea behind noting is so that we avoid reacting.

287
00:26:51,000 --> 00:26:54,000
That's all we're trying to do is to remind ourselves, it's just that.

288
00:26:54,000 --> 00:26:55,000
It's not good.

289
00:26:55,000 --> 00:26:56,000
It's not bad.

290
00:26:56,000 --> 00:26:57,000
It's not neat.

291
00:26:57,000 --> 00:26:58,000
It's not mine.

292
00:26:58,000 --> 00:26:59,000
It's not easy.

293
00:26:59,000 --> 00:27:05,000
And that'll help us to see it clear it more clearly, and eventually understand there's nothing we're clinging to.

294
00:27:05,000 --> 00:27:13,000
Is there a relationship between faculties and the object of focus?

295
00:27:13,000 --> 00:27:18,000
For example, after focusing more on pain, I feel as though my effort has increased,

296
00:27:18,000 --> 00:27:23,000
and focusing on abdomen breathing seems to hone my concentration.

297
00:27:23,000 --> 00:27:29,000
Is there a correlation between them, or is there some place to read about it?

298
00:27:29,000 --> 00:27:36,000
No, it's not a correlation on it in that way.

299
00:27:36,000 --> 00:27:39,000
I mean, there'd be a correlation probably in terms of how you perceive them, and how you are meditating on each of them.

300
00:27:39,000 --> 00:27:42,000
So we apply our mind differently out of habit.

301
00:27:42,000 --> 00:27:50,000
You have to say out of lack of proficiency, because as you get better at it,

302
00:27:50,000 --> 00:27:56,000
you'll see you're able to note everything in the same way, and it improves your faculties in the same way.

303
00:27:56,000 --> 00:28:01,000
If one of them is giving you more of one faculty than another, it's generally a sign

304
00:28:01,000 --> 00:28:06,000
that there's a bit of a lack of a little bias in regards to each of the objects.

305
00:28:06,000 --> 00:28:07,000
It is normal.

306
00:28:07,000 --> 00:28:10,000
It's not true, because you're still learning how to be mindful.

307
00:28:10,000 --> 00:28:19,000
But eventually, you should see that the same result comes from noting everything.

308
00:28:19,000 --> 00:28:21,000
It's a messy practice.

309
00:28:21,000 --> 00:28:26,000
You may find tomorrow or the next day or next week or next month, that it's the exact opposite,

310
00:28:26,000 --> 00:28:29,000
that when you note the rising and falling, you get more effort than you know.

311
00:28:29,000 --> 00:28:32,000
Something else to become more focused.

312
00:28:32,000 --> 00:28:43,000
Hopefully, eventually, you'll feel that you're just getting more mindful and more clear and more natural by noting all of them.

313
00:28:43,000 --> 00:28:51,000
In which of the four elements to subtle energy, chakra type sensations fall in?

314
00:28:51,000 --> 00:28:59,000
We don't use the word chakra that has no meaning to us, but subtle energy.

315
00:28:59,000 --> 00:29:05,000
This really a good question.

316
00:29:05,000 --> 00:29:12,000
Some of it might be mental.

317
00:29:12,000 --> 00:29:17,000
It's a good question.

318
00:29:17,000 --> 00:29:18,000
I don't know.

319
00:29:18,000 --> 00:29:22,000
It falls under the heading of a sensation.

320
00:29:22,000 --> 00:29:31,000
But since it's very, very subtle, it'll be hard to determine which of the four it would be.

321
00:29:31,000 --> 00:29:40,000
The five or two guests.

322
00:29:40,000 --> 00:29:43,000
It's a good question.

323
00:29:43,000 --> 00:29:54,000
A good speculative question, which makes it not a very good question at all to say feeling, feeling.

324
00:29:54,000 --> 00:30:00,000
Regarding tonight's talk, could you elaborate more about how the last three perceptions arise?

325
00:30:00,000 --> 00:30:11,000
On my translation, it says perception of...

