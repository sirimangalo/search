1
00:00:00,000 --> 00:00:18,120
Okay, good evening everyone, tonight we're doing question and answers.

2
00:00:18,120 --> 00:00:23,000
If you're looking to ask questions, if you're not here with us, if you're here with

3
00:00:23,000 --> 00:00:38,000
us, you get to ask me questions every day anyway. But if you're online wondering how do you ask questions for this, you have to go to our website at

4
00:00:38,000 --> 00:00:58,000
meditation.ceremungalow.org and ask questions there. And you have to sign up, but that's purposeful because we want to make it hard for you.

5
00:00:58,000 --> 00:01:03,000
It's too easy people go and ask too many questions.

6
00:01:03,000 --> 00:01:11,000
We may get questions that are off topic, so we want to ensure that people are serious before they ask questions.

7
00:01:11,000 --> 00:01:23,000
Also, we're only really trying to answer questions about this tradition, about our meditation practice that are going to be useful for our group.

8
00:01:23,000 --> 00:01:38,000
So without further ado, we have nine questions so far. First, we have a couple of questions. I would prefer if you could separate them out.

9
00:01:38,000 --> 00:01:55,000
I think that I'm not 100% on that, but if you got multiple questions, please do separate them out. It's not really fair. It's just not as clean.

10
00:01:55,000 --> 00:02:21,000
Okay, so first question, some people criticize Mahasi Sayadha's teachings saying they are overly technical. How would you refute that?

11
00:02:21,000 --> 00:02:33,000
Well, my immediate feeling about this is that I probably wouldn't. I'm not really that keen on going out of my way to refute people.

12
00:02:33,000 --> 00:02:47,000
If people want to see something as overly technical, I mean, ultimately I imagine that most people who say that are looking to find fault, first of all.

13
00:02:47,000 --> 00:03:00,000
But then if we are to examine the question, what is it that you find too technical? The book city road? Because I don't find them any more technical than other traditional Buddhist teachings.

14
00:03:00,000 --> 00:03:10,000
They might be a little more technical than the suit does only a little bit. In fact, in some ways they're less technical than the suit does because they involve all sorts of stories from the commentaries.

15
00:03:10,000 --> 00:03:25,000
The great thing about Mahasi Sayadha is he's able to inspire at the same time as gives such deep and you might call technical teachings. There's nothing wrong with being technical.

16
00:03:25,000 --> 00:03:39,000
And I think that's a feeling that people have acronyms. My would give back is that people are looking for, often looking for teachings that are vague or not vague, but simple.

17
00:03:39,000 --> 00:03:59,000
And we might use the word touchy feeling like they feel good. And I have listened to talks and to be honest.

18
00:03:59,000 --> 00:04:16,000
But sometimes you listen to them and you ask yourself, what did I learn? I didn't really learn much, but I enjoyed myself. Dhamma talks are sometimes like that. And that's what we wouldn't want.

19
00:04:16,000 --> 00:04:32,000
Technical is often much preferable because it provides you with tools. Technical, don't look it up on the internet, but technical has to do with skills and tools.

20
00:04:32,000 --> 00:04:49,000
That's something that makes you feel good. Like I give you a talk and you feel good afterwards. Well, I don't feel like I did the job of inspiring you perhaps. And that's useful. But it's not nearly enough and not nearly as good as providing a technique.

21
00:04:49,000 --> 00:05:08,000
Technical advice. I think the Master Scientist teachings, I think they're brilliant. I've never read anything, even that I'm reading them in translation. I've never read anything as wonderful, apart from, in terms of an explanation of the Buddhist teaching.

22
00:05:08,000 --> 00:05:14,000
Apart from the teachings themselves, of course.

23
00:05:14,000 --> 00:05:30,000
And if you're talking about the actual practice, I would have to ask for a quorum here in the people in the room. I won't ask, but I imagine asking all the people in the room here what they think, whether this practice is too technical.

24
00:05:30,000 --> 00:05:40,000
It's fairly formal, but I would use words like concrete. It's very concrete. You don't have to guess about what you're supposed to do.

25
00:05:40,000 --> 00:05:53,000
It's rather involved and rather taxing, you know, challenging, involved in the sense there's quite a technique to it. So it is quite technical, I guess in that sense.

26
00:05:53,000 --> 00:05:56,000
I don't know much more to say about that.

27
00:05:56,000 --> 00:06:13,000
Then about Ajahn Chow, I'm not interested in comparing with Ajahn Chats, a different teacher in Thailand. But he says that basically insight and insight has to develop out of peace and tranquility.

28
00:06:13,000 --> 00:06:26,000
Maybe you could argue that. I think without any sort of tranquility in peace, it's hard to imagine insight becoming mature.

29
00:06:26,000 --> 00:06:34,000
But then he says the entire process will happen naturally of its own accord. You can't force it, we can't force it.

30
00:06:34,000 --> 00:06:49,000
And I imagine, again, this is a translation from Thai obviously, but I imagine he had the best of intentions when he said that, and his meaning was given the benefit of the doubt was good.

31
00:06:49,000 --> 00:07:02,000
It was right. But I have a problem with that statement as it stands if you interpret it to mean that if you practice tranquility, if you calm your mind down, insight will grow out of it naturally.

32
00:07:02,000 --> 00:07:20,000
And that's not true. It will not arise naturally of its own accord unless you're actually practicing a sort of meditation that's capable of providing insight, which very simply is a meditation focused on reality.

33
00:07:20,000 --> 00:07:42,000
If you're not focused on reality, insight will not arise by very definition, insight is seeing impermanent suffering in non-self. If you're practicing certain types of meditation designed to calm you down, make that for example, you'll never see impermanent suffering in non-self because the objects are not impermanent or not unsatisfying or not uncontrollable.

34
00:07:42,000 --> 00:08:07,000
There's a sense of stability and there involves entities, people, beings who are actors and entities, all sorts of things associated with self, so your question, do you need to calm the mind in order for insight to arise or will calm naturally arise through insight practice?

35
00:08:07,000 --> 00:08:21,000
Again, going back to the Yuganada Sutta, we're lucky that we have this Sutta that says sometimes our hunts will have practiced insight first and then develop tranquility after.

36
00:08:21,000 --> 00:08:36,000
It's quite clear from the Sutta that the Buddha had, these sort of questions were being asked and he gave very clear instruction, which I think is instructive for those people who want to say it has to be like this.

37
00:08:36,000 --> 00:08:41,000
It has to be like this or it has to be like that.

38
00:08:41,000 --> 00:08:50,000
When the Buddha quite clearly said it can be like this or it can be like that.

39
00:08:50,000 --> 00:08:54,000
How to use mindfulness to overcome panic attacks for Bia?

40
00:08:54,000 --> 00:08:58,000
I'm sure you've been asked this question before, so sorry for a repetitive question.

41
00:08:58,000 --> 00:09:21,000
I would normally have deleted such a question because yes, I do answer this one a lot, but this one particularly, I have a soft spot for it because it is one that we get a lot and it's not a trivial question, but I would recommend I'm only going to answer by recommending that you look up some of my other videos on anxiety

42
00:09:21,000 --> 00:09:28,000
and that you undertake, you read my booklet and try to practice accordingly because that's about it.

43
00:09:28,000 --> 00:09:36,000
To stop taking medication, don't get interested in medication for any of this stuff.

44
00:09:36,000 --> 00:09:38,000
Start meditating.

45
00:09:38,000 --> 00:09:42,000
Am I allowed to say that? Stop taking medication? No, you shouldn't.

46
00:09:42,000 --> 00:09:46,000
I don't know. That was a mistake.

47
00:09:46,000 --> 00:09:51,000
When you say I started working with doctors and taking meds, honestly they don't have.

48
00:09:51,000 --> 00:09:53,000
I apologize for saying don't take your medication.

49
00:09:53,000 --> 00:09:57,000
That's what I want to say, but I can't say it because I'm not a doctor.

50
00:09:57,000 --> 00:10:07,000
What I'll say instead is I agree that medication is from what I've seen not as helpful as people think it is.

51
00:10:07,000 --> 00:10:16,000
I would recommend meditation as a solution because I have seen it to be very helpful.

52
00:10:16,000 --> 00:10:24,000
If you're on medication, it's not likely that meditation will be all that fruitful, so I wouldn't recommend mixing them.

53
00:10:24,000 --> 00:10:29,000
Sorry, it's all I can say, I guess.

54
00:10:29,000 --> 00:10:36,000
Here's one.

55
00:10:36,000 --> 00:10:48,000
Is it okay to be mindful of breath and body movements during the day then not distracted or memories and go back to noting?

56
00:10:48,000 --> 00:10:53,000
I think the answer is yes.

57
00:10:53,000 --> 00:11:01,000
I mean, you're talking about being mindful of your experiences, which shows that you've got a good grasp of what we're talking about.

58
00:11:01,000 --> 00:11:09,000
You're feeling responsible.

59
00:11:09,000 --> 00:11:21,000
You feel like you've done something bad then yes.

60
00:11:21,000 --> 00:11:32,000
If you feel sorry for yourself, note that, sorry, sorry, and then just go back to your practice.

61
00:11:32,000 --> 00:11:38,000
Is it possible to cultivate faith or is that something that is spontaneously developed?

62
00:11:38,000 --> 00:11:40,000
It's possible.

63
00:11:40,000 --> 00:11:46,000
I don't think it's very stable in a lot of theists and other religious people develop great faith.

64
00:11:46,000 --> 00:11:53,000
I mean, I think like any mental state, there are practices you can do to encourage it.

65
00:11:53,000 --> 00:11:55,000
It's probably better to say.

66
00:11:55,000 --> 00:12:00,000
You can't just say, I'm going to have faith, but you can engage in practices like prayer.

67
00:12:00,000 --> 00:12:05,000
We do chanting, which creates faith.

68
00:12:05,000 --> 00:12:08,000
Nothing is ever spontaneously developed.

69
00:12:08,000 --> 00:12:10,000
Everything has conditions.

70
00:12:10,000 --> 00:12:17,000
So there are ways of cultivating faith and they'll differ depending on your situation and your condition.

71
00:12:17,000 --> 00:12:23,000
For some people reading the Buddha's teaching is a great way to gain faith for some people, for other people.

72
00:12:23,000 --> 00:12:31,000
I mean, with the proviso that faith is not what we're really trying to focus on building.

73
00:12:31,000 --> 00:12:35,000
You should really just focus on mindfulness and everything else balances out.

74
00:12:35,000 --> 00:12:40,000
What's really useful is to see which ones you're lacking and then be mindful of that.

75
00:12:40,000 --> 00:12:43,000
If you have doubts or no doubting, doubting.

76
00:12:43,000 --> 00:12:51,000
That's the teachings on the five faculties are much more for helping you pinpoint what you have to be mindful of.

77
00:12:51,000 --> 00:12:58,000
Then saying, okay, now I'm going to build faith. It's not really that useful.

78
00:12:58,000 --> 00:13:05,000
Again, three questions, so I'd prefer if you broke them up.

79
00:13:05,000 --> 00:13:08,000
Makes it easier to read at the very least.

80
00:13:08,000 --> 00:13:13,000
Before I start to meditate, must I chant texts? No.

81
00:13:13,000 --> 00:13:17,000
Would you recommend to lay people to chant in the morning evening?

82
00:13:17,000 --> 00:13:25,000
Yes. If it's, I wouldn't recommend people who weren't thinking about it, but if someone's looking to gain encouragement,

83
00:13:25,000 --> 00:13:28,000
that's, sure. Yes, it's a good thing.

84
00:13:28,000 --> 00:13:36,000
I'm making very good progress in meditation, meditating for about 30 to 45 minutes, almost every second day.

85
00:13:36,000 --> 00:13:40,000
Great. But for a few weeks, something strange has happened.

86
00:13:40,000 --> 00:13:43,000
I have to swallow constantly. Yes.

87
00:13:43,000 --> 00:13:47,000
And the fear of swallowing makes you swallow. That's fine.

88
00:13:47,000 --> 00:13:54,000
The fear is an emotion you would say afraid or worried or anxious or however it appears to you.

89
00:13:54,000 --> 00:13:58,000
Because you're swallowing, you would say swallowing. It's an interesting object of meditation.

90
00:13:58,000 --> 00:14:02,000
There's no problem there. There's no need to handle it.

91
00:14:02,000 --> 00:14:07,000
Again, handling things is our go-to reaction, but that's not how this works.

92
00:14:07,000 --> 00:14:17,000
We're just trying to stop trying to handle, learn to bear and to coexist with that experience,

93
00:14:17,000 --> 00:14:21,000
and it'll go away by itself. It's not actually a problem.

94
00:14:21,000 --> 00:14:25,000
I have a restless mind that quickly becomes frightened.

95
00:14:25,000 --> 00:14:31,000
Our emotions, as object, correct in the sense of the dhamma.

96
00:14:31,000 --> 00:14:35,000
I've achieved very good results in just watching with sati, my emotions, and finally,

97
00:14:35,000 --> 00:14:40,000
to solve only by the observation. Well, that's not how we practice.

98
00:14:40,000 --> 00:14:52,000
So we would have you note afraid, afraid, or restless, restless.

99
00:14:52,000 --> 00:14:56,000
Okay, a question about reading.

100
00:14:56,000 --> 00:15:00,000
I wonder if reading doesn't keep us in a world of concepts.

101
00:15:00,000 --> 00:15:04,000
At least unnecessarily involved in someone else's dhamma.

102
00:15:04,000 --> 00:15:10,000
You mean reading fiction. See, reading itself, you're asking, is reading a wholesome activity.

103
00:15:10,000 --> 00:15:14,000
Reading itself is not wholesome or unwholesome.

104
00:15:14,000 --> 00:15:23,000
Reading is an act that occurs with the eye, and then with the brain, and then with the mind.

105
00:15:23,000 --> 00:15:26,000
It's a functional activity.

106
00:15:26,000 --> 00:15:34,000
Now, thinking gives rise to, or reading gives rise to thinking, and thinking, of course,

107
00:15:34,000 --> 00:15:39,000
can give rise to all sorts of emotions, good or bad, positive or negative.

108
00:15:39,000 --> 00:15:43,000
If you read the Buddhist teachings, it's quite possible that as a result of that,

109
00:15:43,000 --> 00:15:46,000
there will arise all sorts of good qualities.

110
00:15:46,000 --> 00:15:52,000
If you're reading romance, harlequin romance, less likely.

111
00:15:52,000 --> 00:15:56,000
Much more likely that to give rise to unwholesome thoughts.

112
00:15:56,000 --> 00:16:00,000
Now, they do say it's good for the brain, but that's something quite different.

113
00:16:00,000 --> 00:16:05,000
They say reading even reading fiction is good to stimulate the brain, and maybe even keep you healthy.

114
00:16:05,000 --> 00:16:07,000
You could argue, perhaps.

115
00:16:07,000 --> 00:16:14,000
We've done studies on it, but that's totally unrelated to Buddhist practice, of course.

116
00:16:14,000 --> 00:16:18,000
During my past, and I'm not able to note the start of thinking,

117
00:16:18,000 --> 00:16:23,000
because you give me any tips. You don't worry about it.

118
00:16:23,000 --> 00:16:25,000
If you catch the beginning, that's great.

119
00:16:25,000 --> 00:16:27,000
If you catch it at the end, that's fine.

120
00:16:27,000 --> 00:16:29,000
Whenever you catch it, try to be mindful.

121
00:16:29,000 --> 00:16:35,000
I mean, there's so much going on inside that we're not even thinking of trying to be perfect.

122
00:16:35,000 --> 00:16:39,000
We're just trying to catch whenever we experience something.

123
00:16:39,000 --> 00:16:41,000
Oh, I was thinking there.

124
00:16:41,000 --> 00:16:47,000
Then say to yourself, thinking, thinking, once you realize it.

125
00:16:47,000 --> 00:16:51,000
Is celibacy related to mettia in any way?

126
00:16:51,000 --> 00:16:56,000
You could probably relate them.

127
00:16:56,000 --> 00:16:59,000
I mean, mettia can be problematic for celibacy.

128
00:16:59,000 --> 00:17:04,000
If you start sending good thoughts to someone that was an object of attraction,

129
00:17:04,000 --> 00:17:08,000
they say that in the texts. Be careful about that.

130
00:17:08,000 --> 00:17:11,000
But I don't think that's what you're asking.

131
00:17:11,000 --> 00:17:16,000
If a person is celibate, are they better able to practice mettia?

132
00:17:16,000 --> 00:17:19,000
Or is it a practice of mettia?

133
00:17:19,000 --> 00:17:20,000
You could argue that.

134
00:17:20,000 --> 00:17:26,000
I mean, celibacy is great for the mind because it drains a lot of that passion.

135
00:17:26,000 --> 00:17:31,000
And it makes you much more stable and calm in the mind.

136
00:17:31,000 --> 00:17:38,000
So not just mettia, but many kinds of states are much easier to cultivate.

137
00:17:38,000 --> 00:17:43,000
If you're not being celibate, then things like mettia are more difficult

138
00:17:43,000 --> 00:17:51,000
as your more subject to partiality, which is like if you think about your girlfriend, your boyfriend,

139
00:17:51,000 --> 00:17:55,000
and you send them mettia, well, it's much more likely to become an attachment,

140
00:17:55,000 --> 00:18:00,000
which is quite dangerous, emotionally dangerous,

141
00:18:00,000 --> 00:18:04,000
because of the suffering, of course, that can follow.

142
00:18:04,000 --> 00:18:10,000
How does one let go without feeling the want to explode?

143
00:18:10,000 --> 00:18:13,000
How does one let go? You don't let go.

144
00:18:13,000 --> 00:18:17,000
Just, if you want to explode out of the human shell

145
00:18:17,000 --> 00:18:21,000
and let out the howl of anger, hate, and disruptive, destructive nature,

146
00:18:21,000 --> 00:18:23,000
well, that's what you try to be mindful of,

147
00:18:23,000 --> 00:18:25,000
and eventually let go of that.

148
00:18:25,000 --> 00:18:30,000
Because that desire is a response to experience,

149
00:18:30,000 --> 00:18:34,000
and probably a lot of physical energy.

150
00:18:34,000 --> 00:18:38,000
But it's a bad habit, and so you try to change those habits through meditation.

151
00:18:38,000 --> 00:18:45,000
Read my booklet, be a good practice for that.

152
00:18:45,000 --> 00:18:49,000
According to Buddhism, is there a difference between compassion and empathy?

153
00:18:49,000 --> 00:18:51,000
We don't use the word empathy much.

154
00:18:51,000 --> 00:18:54,000
I don't even know that you could find a word that translates,

155
00:18:54,000 --> 00:18:57,000
maybe you could, but I can't think of one that translates as empathy.

156
00:18:57,000 --> 00:19:03,000
Empathy means to feel what other people are feeling.

157
00:19:03,000 --> 00:19:11,000
You can sympathize. If sympathy means you appreciate the scope of what people are feeling,

158
00:19:11,000 --> 00:19:15,000
but to empathize means you feel it with them.

159
00:19:15,000 --> 00:19:17,000
And that's quite different from compassion.

160
00:19:17,000 --> 00:19:20,000
Compassion isn't about feeling sad when someone's sad.

161
00:19:20,000 --> 00:19:25,000
It's about appreciating and having a wish for them to be

162
00:19:25,000 --> 00:19:30,000
or having an inclination so that everything you do and say

163
00:19:30,000 --> 00:19:39,000
is towards them being free from that sadness or that suffering.

164
00:19:39,000 --> 00:19:44,000
And that's it.

165
00:19:44,000 --> 00:19:48,000
Everyone here is a fairly advanced meditator,

166
00:19:48,000 --> 00:19:55,000
so I don't feel any great need to teach them lots of things.

167
00:19:55,000 --> 00:20:00,000
I'm very glad that we have a full, hopeful house.

168
00:20:00,000 --> 00:20:07,000
It's some idea that we might be moving soon, but it's a tricky thing.

169
00:20:07,000 --> 00:20:21,000
Does anybody out there who has a place for us to move that's bigger or happy to take donations?

170
00:20:21,000 --> 00:20:26,000
But we're doing fine. I can define anywhere.

171
00:20:26,000 --> 00:20:30,000
No, someone slipped another question and should we take it?

172
00:20:30,000 --> 00:20:35,000
Let's take it.

173
00:20:35,000 --> 00:20:42,000
That's not a question, so bye-bye.

174
00:20:42,000 --> 00:20:50,000
Okay, that's all. Thank you. Have a good night.

175
00:20:50,000 --> 00:20:58,000
Oh, no. I didn't... Oh, now you can see me.

176
00:20:58,000 --> 00:21:00,000
That's funny.

177
00:21:00,000 --> 00:21:03,000
That whole teaching was just...

178
00:21:03,000 --> 00:21:07,000
I got this transition and that picture of me was...

179
00:21:07,000 --> 00:21:10,000
That's just the intro. I'm supposed to transition out of that.

180
00:21:10,000 --> 00:21:21,000
Well, hello, everyone. Have a good night.

