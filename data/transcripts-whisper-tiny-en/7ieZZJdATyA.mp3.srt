1
00:00:00,000 --> 00:00:04,000
Hello, welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:12,000
Just a reminder to everyone still waiting on videos of everyone meditating.

3
00:00:12,000 --> 00:00:14,000
So far I have six video responses.

4
00:00:14,000 --> 00:00:17,000
Don't be shy, please.

5
00:00:17,000 --> 00:00:22,000
You're doing a great service to the meditation practice.

6
00:00:22,000 --> 00:00:28,000
The other thing is that if you want to support this work, support the things that I

7
00:00:28,000 --> 00:00:34,000
do support these videos, don't forget to click on the like button if you like the video.

8
00:00:34,000 --> 00:00:38,000
Because that affects the search results and gets more people.

9
00:00:38,000 --> 00:00:43,000
It allows more people to see it and takes it higher and the search results makes it feature

10
00:00:43,000 --> 00:00:44,000
or something.

11
00:00:44,000 --> 00:00:51,000
Remember to subscribe and post these videos to your favorite so that more people can see them.

12
00:00:51,000 --> 00:00:53,000
If you like them.

13
00:00:53,000 --> 00:00:58,000
Okay, so next question comes from CNN who asks,

14
00:00:58,000 --> 00:01:01,000
Hi, I'm confused about morals.

15
00:01:01,000 --> 00:01:06,000
In reading no more Mr. Nice Guy, it seems many people are being nice to get approval.

16
00:01:06,000 --> 00:01:09,000
Being selfish, is it really that bad?

17
00:01:09,000 --> 00:01:12,000
Is it really bad to have desires and needs?

18
00:01:12,000 --> 00:01:15,000
Is it always bad to hurt or make others suffer?

19
00:01:15,000 --> 00:01:18,000
I don't know if this is a book, a plug or something.

20
00:01:18,000 --> 00:01:24,000
I've never heard of this book, but maybe we'll keep it out of the question.

21
00:01:24,000 --> 00:01:27,000
But yeah, there's several parts here.

22
00:01:27,000 --> 00:01:33,000
So obviously being nice to get approval is less than noble.

23
00:01:33,000 --> 00:01:36,000
It's ignoble and it's a kind of desire.

24
00:01:36,000 --> 00:01:38,000
You want something.

25
00:01:38,000 --> 00:01:41,000
Being selfish, is it really that bad?

26
00:01:41,000 --> 00:01:44,000
And is it really bad to have desires and needs?

27
00:01:44,000 --> 00:01:48,000
Being selfish isn't necessarily bad.

28
00:01:48,000 --> 00:01:52,000
The problem is that we don't know what's really for our benefit.

29
00:01:52,000 --> 00:01:57,000
The Buddha said, when you help yourself, you help other people.

30
00:01:57,000 --> 00:01:59,000
When you help other people, you help yourself.

31
00:01:59,000 --> 00:02:01,000
It's one and the same.

32
00:02:01,000 --> 00:02:10,000
So by extrapolation, when you hurt other people, you hurt yourself.

33
00:02:10,000 --> 00:02:23,000
You hurt other people, because you affect your own mind and you affect the relationship that other people have towards you when you do either one.

34
00:02:23,000 --> 00:02:38,000
So when we talk about being selfish, we mean being concerned with our own benefit, with our own welfare.

35
00:02:38,000 --> 00:02:46,000
So being stingy or not wanting to help other people is actually contrary to one's own benefit or welfare.

36
00:02:46,000 --> 00:02:52,000
Often people who have a great amount of craving and attachment don't want to help other people.

37
00:02:52,000 --> 00:02:55,000
That doesn't mean that they're looking out for their best interests.

38
00:02:55,000 --> 00:02:58,000
They think they are, but they don't realize what is in their best interest.

39
00:02:58,000 --> 00:03:02,000
And that in fact, it's in their best interest to help other people.

40
00:03:02,000 --> 00:03:07,000
So selfishness is fine in the sense of wanting to help yourself.

41
00:03:07,000 --> 00:03:20,000
But in the sense of not wanting to help other people or denying other people's request for help is detrimental to your own well-being.

42
00:03:20,000 --> 00:03:23,000
So you can't be said to be looking out for your own best interest.

43
00:03:23,000 --> 00:03:27,000
This is because it's caught up in this kind of craving.

44
00:03:27,000 --> 00:03:32,000
So when you ask, is it really bad to have desires and needs?

45
00:03:32,000 --> 00:03:46,000
Yes, because of the nature of desires and needs, the more needy you are, the more clingy you are, the more desires you have, the more suffering you have, the less of reality you're able to accept.

46
00:03:46,000 --> 00:03:51,000
The fewer desires and needs and wants that you have, the more of reality that you're able to accept.

47
00:03:51,000 --> 00:03:57,000
And as a result, the more content peaceful and happy you are.

48
00:03:57,000 --> 00:04:04,000
As far as it always being bad to hurt or make other people suffer, I think is very much included in this.

49
00:04:04,000 --> 00:04:07,000
I think for most people this isn't really a question.

50
00:04:07,000 --> 00:04:17,000
We feel, but if you're asking an interesting question here, because for most people it's obvious you don't want to hurt other people because it causes suffering for them.

51
00:04:17,000 --> 00:04:30,000
Well, we don't have to enter into that kind of mental gymnastics in reality in terms of finding a real and a scientific moral.

52
00:04:30,000 --> 00:04:38,000
Because you don't have to think about the other person's well-being, sacrificing your own well-being.

53
00:04:38,000 --> 00:04:48,000
When you do something that is truly beneficial to someone else, you're benefiting yourself. When you are helping a person, you're also helping yourself.

54
00:04:48,000 --> 00:04:54,000
Or in helping someone, you factor in your own well-being as well.

55
00:04:54,000 --> 00:05:03,000
So you do something, not thinking them or us, you do something, thinking about bringing the greatest amount of peace and happiness.

56
00:05:03,000 --> 00:05:09,000
But in fact, I would say that you one should put one's self first.

57
00:05:09,000 --> 00:05:12,000
Simply because there's only so much you can do for someone else.

58
00:05:12,000 --> 00:05:22,000
So when we look at the benefit of sacrificing our self for someone else's benefit, we often are thinking in terms of helping them externally.

59
00:05:22,000 --> 00:05:31,000
Because besides explaining to them the way for them to help themselves, you can't bring someone closer to enlightenment.

60
00:05:31,000 --> 00:05:36,000
You can't bring someone to the realization of the truth, they have to do that for themselves.

61
00:05:36,000 --> 00:05:46,000
So I would recommend, as I've said in another video, a sort of selfishness in the sense that everyone looks out for themselves.

62
00:05:46,000 --> 00:05:50,000
Everyone looks after their own minds, purifies themselves.

63
00:05:50,000 --> 00:05:54,000
And I mean, this has been repeated over and over again throughout spirituality.

64
00:05:54,000 --> 00:06:03,000
If you want to change the world, change yourself, start from within the path to peace lies inside of all of ourselves.

65
00:06:03,000 --> 00:06:14,000
If we are accepting open caring beings, then we've achieved world peace if we can start from within.

66
00:06:14,000 --> 00:06:20,000
So I would say that one should look to one's own benefit and not sacrifice it for other people's benefit.

67
00:06:20,000 --> 00:06:26,000
That's actually what the Buddha said, when you know it's really in your own best interest, you should follow that.

68
00:06:26,000 --> 00:06:29,000
And the thing is that that is for the best interest of the world.

69
00:06:29,000 --> 00:06:36,000
Because that is doing your job to promote world peace, to promote happiness, to promote harmony in the world.

70
00:06:36,000 --> 00:06:48,000
When you have cleaned your mind and your reality, then everything you touch, everything you affect is benefited.

71
00:06:48,000 --> 00:06:57,000
So you have done your job as opposed to trying to change other people or change the world and forgetting about your own benefit.

72
00:06:57,000 --> 00:07:05,000
I've talked about this before in another video, but I think it's an interesting question.

73
00:07:05,000 --> 00:07:13,000
And the most interesting part of it is the fact that if you ask me, and if you read the Buddha's teaching, the best thing to do is to help yourself.

74
00:07:13,000 --> 00:07:29,000
That's considered doing your job. And if everyone were to do their own job, to do their own work, and to clean their own minds, to purify themselves, then the world would be free from suffering, stress, and evil of all kinds.

75
00:07:29,000 --> 00:07:45,000
Okay, so thanks for the question.

