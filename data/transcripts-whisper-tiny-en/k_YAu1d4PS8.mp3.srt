1
00:00:00,000 --> 00:00:11,200
Okay, hello everyone, I'm broadcasting live December 23rd, 2015. No dumb

2
00:00:11,200 --> 00:00:16,920
up out of this week. I think it's a bit too much to get set up here. It did

3
00:00:16,920 --> 00:00:26,160
bring the camera but you know it's Christmas and my brother's coming. I think

4
00:00:26,160 --> 00:00:37,600
I'll wait until I get back to the back to the bat cave. We'll just do regularly

5
00:00:37,600 --> 00:00:53,040
scheduled broadcast. So I'm in Florida, site here, route side, near this thing

6
00:00:53,040 --> 00:01:01,400
they call the night. But I spent the better part of a year in Thailand, one

7
00:01:01,400 --> 00:01:10,080
living inside a mosquito screen like this. It's made of tea wood, but it wasn't

8
00:01:10,080 --> 00:01:18,000
nearly as good quality. But you know, tea wood, it was actually a lot more

9
00:01:18,000 --> 00:01:23,600
awesome than it had more purpose because there were thick mosquitoes where we

10
00:01:23,600 --> 00:01:32,240
were staying. So I can be out in the forest. My cookie was basically a

11
00:01:32,240 --> 00:01:37,920
scooter screen. They built it for a jantong, I think, or at some point became a

12
00:01:37,920 --> 00:01:43,720
jantong scooter. And I don't know that he ever stayed in it or he may be in the

13
00:01:43,720 --> 00:01:56,240
past he'd stayed in it. But then they built him this fancy luxury sort of house

14
00:01:56,240 --> 00:02:02,960
to stay in. So when I went to this place the first time to do a 10-day course, I got

15
00:02:02,960 --> 00:02:07,560
to stay in his cookie. It was the only cookie left. So there were two of us who

16
00:02:07,560 --> 00:02:12,680
were his one of his attendants and me who was learning how to teach. So I was

17
00:02:12,680 --> 00:02:18,640
following along to listen in and to learn how to be a teacher. So the two of

18
00:02:18,640 --> 00:02:24,480
us were going to stay in this cookie. And most of it was this big mosquito

19
00:02:24,480 --> 00:02:30,720
screen. And then there was a very small room that was actually wood and closed

20
00:02:30,720 --> 00:02:34,600
up. And his attendants said to me, no, you sleep, you can sleep out here, I'll

21
00:02:34,600 --> 00:02:40,400
sleep in there. And this was a Bangkok monk. He was among from Bangkok.

22
00:02:40,400 --> 00:02:49,840
Really nice guy. But quite Bruce, I think, is the word. You stay out here

23
00:02:49,840 --> 00:02:54,080
all stay in there. And he went into the room and he banged around for a bit. And

24
00:02:54,080 --> 00:02:58,000
I was just looking around thinking, oh, this is nice. I'll just sleep out here. And

25
00:02:58,000 --> 00:03:02,880
then he comes storming out and he says, I'm going to stay with the abbot. You

26
00:03:02,880 --> 00:03:09,600
can stay here. And he just left. And I thought it was odd that he just suddenly

27
00:03:09,600 --> 00:03:16,480
went away. So whatever. And so I opened up the small room thinking, well, maybe

28
00:03:16,480 --> 00:03:20,800
I can sleep in there. I don't know. And I looked in. And there was a dead gecko

29
00:03:20,800 --> 00:03:29,080
right in the middle of the fire. So this dead gecko had scared him away. I'm

30
00:03:29,080 --> 00:03:33,480
sorry. We picked up the dead gecko through it outside.

31
00:03:33,480 --> 00:03:42,880
He ended up leaving the next day, the next morning. We went for breakfast. And

32
00:03:42,880 --> 00:03:46,040
as we went for alms around through the village, they drove us to the village.

33
00:03:46,040 --> 00:03:51,200
And we went walking through the village on alms, came back. I'm sure we're

34
00:03:51,200 --> 00:03:57,080
sitting down to eat. There's sticky rice. And there's this hot. They have this

35
00:03:57,080 --> 00:04:00,600
paste that they make that's very, very spicy and vegetables that you've

36
00:04:00,600 --> 00:04:04,400
been in it and pork grind and all this stuff. I'm sitting there eating and

37
00:04:04,400 --> 00:04:09,160
we're eating whatever food we got. And he, this guy who ditched me the night

38
00:04:09,160 --> 00:04:15,400
before, he looks over at me. When he looks back at the food and he says, I can't

39
00:04:15,400 --> 00:04:20,200
even eat this food. And I'm tired. How is it that you can eat? And he ended up

40
00:04:20,200 --> 00:04:25,280
leaving. He was leaving that day. Really nice guy. But you know, a city monk

41
00:04:25,280 --> 00:04:31,480
couldn't hack it in the countryside. So I was there for 10 days. And that was, it

42
00:04:31,480 --> 00:04:34,440
was quite nice. But I ended up going back there and staying the better part of

43
00:04:34,440 --> 00:04:39,480
it here. And then then I really had to learn to deal with geckos because

44
00:04:39,480 --> 00:04:42,960
they were living in my bathroom and they were living in the roof. So I go into

45
00:04:42,960 --> 00:04:49,840
the bathroom and there'd be a big gecko on the wall staring at me. And then

46
00:04:49,840 --> 00:04:52,800
crap all over the place. So I tried to get them out actually and use a

47
00:04:52,800 --> 00:04:59,560
Tupperware, kept them in the Tupperware and put them on the site. Then there

48
00:04:59,560 --> 00:05:03,680
were snakes. I remember coming into the Kootian sitting down one and it was,

49
00:05:03,680 --> 00:05:06,880
you know, I was wearing glasses. I didn't have my glasses on and I was sitting

50
00:05:06,880 --> 00:05:09,920
down in the middle of the Kootian. And I turned and it looked like there was

51
00:05:09,920 --> 00:05:13,880
something had, had defecated on the floor. There was a brown pile and they turned

52
00:05:13,880 --> 00:05:18,400
and so I put my glasses on and it was a snake about this big one of these

53
00:05:18,400 --> 00:05:22,320
highly poisonous teeny tiny snakes sitting right beside my butt. It's about an

54
00:05:22,320 --> 00:05:28,520
inch away from where I had sat down. It had somehow gotten in through the floor.

55
00:05:28,520 --> 00:05:33,360
Then we had big snakes like the really long ones. Then we had these monitor

56
00:05:33,360 --> 00:05:36,880
lizards that I never saw that were apparently there. And they're about when

57
00:05:36,880 --> 00:05:43,600
you can see how big they ever like even long. Lots of those in Sri Lanka. But I

58
00:05:43,600 --> 00:05:48,920
never saw one in Thailand. Currently they had them. Just that the Thai people in

59
00:05:48,920 --> 00:05:53,200
the forest, they eat pretty much everything. So all the monitor lizards were

60
00:05:53,200 --> 00:06:05,160
probably undefeated. But none of that here. Unfortunately, the most dangerous

61
00:06:05,160 --> 00:06:09,760
animal here is their cat that kills stuff.

62
00:06:09,760 --> 00:06:16,440
Winter Gekos in Florida, Bombay? Oh, probably. I know there are little lizards,

63
00:06:16,440 --> 00:06:23,160
little tiny salamanders and things, I think. Let's salamanders some kind of

64
00:06:23,160 --> 00:06:30,680
those really, really fast little lizards and run their heads up. The big

65
00:06:30,680 --> 00:06:37,120
gekos haven't seen any problem. It's a lot like Thailand here.

66
00:06:37,120 --> 00:06:49,160
So it's my story of the day. Do you have any questions? We do have questions.

67
00:06:49,160 --> 00:06:56,360
We have someone who last night had stayed up in his country and tell like

68
00:06:56,360 --> 00:07:00,800
4 a.m. because he wanted to ask you questions. That was my mood. So I'm going to

69
00:07:00,800 --> 00:07:05,920
ask my moods questions tonight. Hopefully I'll get to see the video. Which is

70
00:07:05,920 --> 00:07:11,040
they meditation helps meditators beat procrastination?

71
00:07:15,480 --> 00:07:21,040
It depends. I mean generally, yeah, meditation helps with a lot of things. It

72
00:07:21,040 --> 00:07:28,960
helps you sort of a lot of your issues. It helps you clear your mind. But I think

73
00:07:28,960 --> 00:07:35,880
it can in certain cases lead to procrastinating or reassessing priority.

74
00:07:35,880 --> 00:07:43,200
Especially in the short term, it can lead you to have difficulty performing

75
00:07:43,200 --> 00:07:51,040
duties that seem meaningless. In fact, can lead to procrastination. I think in the

76
00:07:51,040 --> 00:07:57,720
long term, you get your priority straightened out and those things that are

77
00:07:57,720 --> 00:08:03,920
duties. You see them just as duties and you do them without letting your not

78
00:08:03,920 --> 00:08:09,320
letting any kind of aversion arise. Because you see, it's still a version to be

79
00:08:09,320 --> 00:08:16,200
upset about doing useless things. A meditator should be able to do something

80
00:08:16,200 --> 00:08:23,480
that's useless without being upset about it. But you have this, you don't

81
00:08:23,480 --> 00:08:27,480
you've taken out the desire to do it. So when there's no desire, I think, well,

82
00:08:27,480 --> 00:08:33,320
why am I doing this? And so eventually you come to the concept of a duty of

83
00:08:33,320 --> 00:08:37,120
doing something just for doing it and doing it as a meditation. So doing it

84
00:08:37,120 --> 00:08:40,000
as mindfully as you can. And it doesn't matter whether you're doing walking

85
00:08:40,000 --> 00:08:47,520
meditation or cooking meditation or construction meditation, you know, whatever

86
00:08:47,520 --> 00:08:52,000
it is, it can be a meditation, typing meditation, speaking meditation. And

87
00:08:52,000 --> 00:08:56,840
I'll be meditation. But I think in the beginning that's difficult and as a

88
00:08:56,840 --> 00:09:02,160
result leads, it makes it difficult sometimes to do certain things. I think

89
00:09:02,160 --> 00:09:06,280
procrastination is there. And I think you could argue that in the long

90
00:09:06,280 --> 00:09:13,720
term, it makes it really difficult to increasingly difficult to do the sorts of

91
00:09:13,720 --> 00:09:19,840
things that you're accustomed to doing. And this may lead to falling behind in

92
00:09:19,840 --> 00:09:23,160
your work and eventually just saying, I'm sorry, I can't do this, I can't bring

93
00:09:23,160 --> 00:09:29,520
myself to do it. But that's sort of the outlier, I'd say in general, it's

94
00:09:29,520 --> 00:09:34,520
absolutely going to help you because why do we procrastinate? Have we procrastinated

95
00:09:34,520 --> 00:09:41,200
of laziness? We procrastinate out of our addictions to, to, you know,

96
00:09:41,200 --> 00:09:48,800
entertainment and so on. And we procrastinate out of a version to hard work,

97
00:09:48,800 --> 00:09:53,880
a version to pain, a version of your life in the church, both bright lights

98
00:09:53,880 --> 00:10:01,600
so I'm there and left them both on. Come see everybody, mom. I was, I was the

99
00:10:01,600 --> 00:10:07,840
internet. Oh, so I'm saying hello. Hi. Well, this is the internet. I'm sorry,

100
00:10:07,840 --> 00:10:11,880
I didn't know he was in the middle of the podcasting in there. I just moved out

101
00:10:11,880 --> 00:10:16,600
here to see if it would work. All right. I'll let you go. It's my mom. Hi,

102
00:10:16,600 --> 00:10:24,200
Robin. Hi, how are you going to see the internet? Come say hello. Hi. This is my younger brother,

103
00:10:24,200 --> 00:10:29,000
Ruben. Hi, how are you? Hi, good. How are you? You're the internet.

104
00:10:29,000 --> 00:10:37,000
What is more internet? Small part of the internet. So yeah, my mom just went to pick up

105
00:10:37,000 --> 00:10:44,280
my younger brother at your port and they just got back. So yeah, yesterday we

106
00:10:44,280 --> 00:10:48,760
went to the children's home and that's actually what it's called. The children's

107
00:10:48,760 --> 00:10:55,480
home incorporated and met with a guy named Adam and he showed us around. We

108
00:10:55,480 --> 00:11:00,840
even got to go into one of the, uh, one of the residences and, and kind of

109
00:11:00,840 --> 00:11:05,840
see the kids. I was kind of, I didn't want to get. I was kind of shy about it

110
00:11:05,840 --> 00:11:09,480
thinking, oh, I don't want these people to be on display. It feels kind of weird,

111
00:11:09,480 --> 00:11:13,880
you know, like having a tour, like a zoo or something, you know, I didn't. I was

112
00:11:13,880 --> 00:11:18,440
like, no, I don't need to see them. It was happy that we got to see them, but

113
00:11:18,440 --> 00:11:24,840
kind of the wrong circumstances, like as donors, she's just giving go, but I

114
00:11:24,840 --> 00:11:30,200
don't know. It was really nice to see them and it was nice to just to

115
00:11:30,200 --> 00:11:35,800
get a feel of of the good that we're doing. So there's that for sure.

116
00:11:36,200 --> 00:11:41,240
And Adam was, uh, he said he actually looked up my videos on YouTube and

117
00:11:41,240 --> 00:11:45,320
he watched a couple of them and he said he was interested and he was going to,

118
00:11:45,320 --> 00:11:49,400
and maybe he's watching right now. I don't know, but it really, they were really

119
00:11:49,400 --> 00:11:58,120
happy and, uh, send a money and their group brought some, some, uh, baby stuff

120
00:11:58,120 --> 00:12:03,000
because they actually take care of pregnant mothers. They have five now, I

121
00:12:03,000 --> 00:12:08,840
think, um, pregnant teenagers, I think, or mothers, I'm not sure, but

122
00:12:08,840 --> 00:12:14,680
mothers anyway. Um, and so there's, they brought some baby stuff, baby wipes and

123
00:12:14,680 --> 00:12:21,240
stuff and other things. All in all, it was really great. So I'm

124
00:12:21,240 --> 00:12:25,720
welcome. Congratulations, everyone. Thank you, everyone. We're taking part. It was

125
00:12:25,720 --> 00:12:26,360
really neat thing.

126
00:12:31,400 --> 00:12:37,080
Absolutely meditation, um, meditation makes you do things. Interesting about

127
00:12:37,080 --> 00:12:44,280
procrastination is, um, my stepfather, who's house, this is with my mother. Um,

128
00:12:44,280 --> 00:12:47,880
he came in the beginning. I didn't tell them what it was. I was talking to my mom.

129
00:12:47,880 --> 00:12:53,560
I said, uh, so I've got a Chris, we're giving this, um, on on Tuesday.

130
00:12:53,560 --> 00:12:57,480
We have to go somewhere for a Christmas present. Uh, it's my

131
00:12:57,480 --> 00:13:01,800
Christmas or it was like, I just was very big about who was giving it was

132
00:13:01,800 --> 00:13:05,160
as it's just a Christmas present. He said, oh, is it something

133
00:13:05,160 --> 00:13:11,800
Alan's going to like because, you know, I mean, he's not on my trip. Uh,

134
00:13:11,800 --> 00:13:14,280
because she was like, is it going to be something Buddhist? I said, oh, it's,

135
00:13:14,280 --> 00:13:17,240
I'm actually, I'm sure he will. And she was kind of like, well, I'll just tell you.

136
00:13:17,240 --> 00:13:21,080
So I told her what it was. And I talked with him afterwards. And he, he said,

137
00:13:21,080 --> 00:13:26,520
he's been trying to do this kind of thing, uh, for years with his family. Uh,

138
00:13:26,520 --> 00:13:30,360
like every Christmas, they, his family and, you know, an American family get

139
00:13:30,360 --> 00:13:34,520
together and give gifts. And it's like, we've all got good jobs when none of us

140
00:13:34,520 --> 00:13:39,480
need this need these things. We don't need more stuff. So we're getting gifts

141
00:13:39,480 --> 00:13:42,920
for each other. It's kind of ridiculous. So they've tried many times with my

142
00:13:42,920 --> 00:13:48,520
mother as well to organize some kind of something like this. And so he was

143
00:13:48,520 --> 00:13:52,280
really happy about it. Uh, but, but like it's, it's that kind of thing,

144
00:13:52,280 --> 00:13:57,400
like, like, we want to do good deeds, but do we ever get out and do them?

145
00:13:57,400 --> 00:14:04,440
And, uh, it's exciting being among us to have the, um, to be in a position

146
00:14:04,440 --> 00:14:09,080
to, to initiate this sort of thing and to gather people together like this.

147
00:14:09,080 --> 00:14:18,120
I mean, this is the idea of, um, community and working together as an

148
00:14:18,120 --> 00:14:21,240
organization. It's really awesome. It's the kind of thing we couldn't do

149
00:14:21,240 --> 00:14:29,240
alone. So we often do feel, um, powerless. What am I going to be able

150
00:14:29,240 --> 00:14:36,680
to do? Or how am I going to be able to succeed spiritually? And this is

151
00:14:36,680 --> 00:14:42,840
why organized religion is, of course, so, um, enticing and, and dangerous,

152
00:14:42,840 --> 00:14:47,480
actually, you know, because people go to religion not because of the views

153
00:14:47,480 --> 00:14:53,800
it espouses, but because it's organized and because it's powerful. And so

154
00:14:53,800 --> 00:14:59,400
often they end up with whatever views the religious organization espouses.

155
00:14:59,400 --> 00:15:02,520
Now, of course, we're lucky because our organization has right and perfect

156
00:15:02,520 --> 00:15:07,480
views, but, uh, that's just a coincidence. That

157
00:15:07,480 --> 00:15:12,760
mean, not exactly. It's not quite fair. But it can be dangerous. There is

158
00:15:12,760 --> 00:15:19,800
that hypnotic sort of pole of religion. But, but really in a good way, I mean,

159
00:15:19,800 --> 00:15:28,040
it's power, power is, is, uh, the good in a sense of good for the person. If you

160
00:15:28,040 --> 00:15:34,600
have whatever ambitions, it's good for those ambitions. You're going to

161
00:15:34,600 --> 00:15:39,400
accomplish more in a group in an institution. So it's great to be in that kind

162
00:15:39,400 --> 00:15:44,440
of position. Um, now that's not exactly talking about meditation, but

163
00:15:44,440 --> 00:15:51,640
meditation's a big part of that because meditation clears your mind. And it

164
00:15:51,640 --> 00:15:58,280
frees you from the inertia, sometimes of just a fear or powerlessness or

165
00:15:58,280 --> 00:16:03,160
depression. So that you're actually able to accomplish things. You actually

166
00:16:03,160 --> 00:16:12,280
do get off your butt and, well, sit on your butt.

167
00:16:12,280 --> 00:16:15,960
More questions? Yeah.

168
00:16:15,960 --> 00:16:30,680
Sorry, I was so involved with listening. I forgot to look for the next one.

169
00:16:35,480 --> 00:16:39,480
I'm gonna bubble you to Damo. I have a question. If you meditate and therefore have a

170
00:16:39,480 --> 00:16:44,280
clear awareness of your feelings and see them as they are, you constantly do

171
00:16:44,280 --> 00:16:47,960
that for a long time, would you get to a point that you have

172
00:16:47,960 --> 00:16:52,120
enough experience to sort of outsmart your emotions and therefore not feel

173
00:16:52,120 --> 00:16:56,520
them? In other words, you get to a point where emotions are merely a part of your

174
00:16:56,520 --> 00:16:59,000
past.

175
00:17:00,520 --> 00:17:03,640
It sounds right off that you're trying to intellectual,

176
00:17:03,640 --> 00:17:07,080
trying to figure out the meditation. How does it work? What is this

177
00:17:07,080 --> 00:17:11,480
thing that we're, where is this leading? It sounds like it might be over

178
00:17:11,480 --> 00:17:15,160
analyzing things. It's a little bit more simply,

179
00:17:15,160 --> 00:17:20,840
simple than that. You, I think, I mean, I'm not quite clear on what exactly you're

180
00:17:20,840 --> 00:17:25,000
asking, but it sounds like just a little bit over

181
00:17:25,000 --> 00:17:31,560
intellectualizing. You'll see when you meditate, what is going on in your mind.

182
00:17:31,560 --> 00:17:36,920
You'll see how your emotions arise and you'll see what they lead to.

183
00:17:36,920 --> 00:17:43,320
And you'll see their nature. You'll see the nature of the things that you

184
00:17:43,320 --> 00:17:47,720
desire and the things that you're averse to.

185
00:17:47,720 --> 00:17:57,080
And you'll see them as not being worthy of your reactions to them

186
00:17:57,080 --> 00:18:01,800
that they're simply phenomena that arise and see.

187
00:18:01,800 --> 00:18:06,520
So it's not about outwitting your emotions. In fact, that's a dangerous.

188
00:18:06,520 --> 00:18:11,960
The idea that you can have an emotion and be

189
00:18:11,960 --> 00:18:15,160
intent upon a certain emotion and that you can somehow

190
00:18:15,160 --> 00:18:19,960
outwit that or outsmart that or arise about it. That's not the case.

191
00:18:19,960 --> 00:18:24,040
That emotion has to change. I mean, if you like something or

192
00:18:24,040 --> 00:18:27,960
want something or if you're averse to something or if you hate something,

193
00:18:27,960 --> 00:18:33,800
then that's the issue. The issue is that that is your desire. You can't

194
00:18:33,800 --> 00:18:39,240
avoid that. You can't go around it or somehow circumvent it.

195
00:18:39,240 --> 00:18:43,720
You have to deal with that. You have to change that.

196
00:18:43,720 --> 00:18:50,040
It's not about forcing or controlling or switching off.

197
00:18:50,040 --> 00:18:54,600
It's about changing your view so that you don't have that emotion.

198
00:18:54,600 --> 00:18:59,800
So something arises and you like it. Now there's a reason why you like it. You

199
00:18:59,800 --> 00:19:02,520
like it because you think it's going to bring you happiness. You think

200
00:19:02,520 --> 00:19:05,320
there's something intrinsically good about that.

201
00:19:05,320 --> 00:19:10,200
You have a view or an idea or a conception, a notion

202
00:19:10,200 --> 00:19:14,600
that's artificial about that. And I mean, really that's the

203
00:19:14,600 --> 00:19:20,200
key is it's artificial because as you look closer, you start to see

204
00:19:20,200 --> 00:19:24,360
about everything in the world that there's no

205
00:19:24,360 --> 00:19:28,520
inherent desirability in them. There's nothing about the object that is

206
00:19:28,520 --> 00:19:34,840
desirable that makes it desirable. We make this up. It's arbitrary.

207
00:19:34,840 --> 00:19:41,160
It's subjective. It's not related to the nature of the object.

208
00:19:41,160 --> 00:19:44,280
And we do this with things that we don't like. Something arises.

209
00:19:44,280 --> 00:19:47,000
You don't like it. There's a reason you don't like it. You don't like it

210
00:19:47,000 --> 00:19:49,720
because you believe that it's going to cause you suffering.

211
00:19:49,720 --> 00:19:58,440
Believe that it's bad, that it's going to have a negative effect on you. And as you are mindful,

212
00:19:58,440 --> 00:20:02,040
you start to see that no, that's actually not the case. This thing has no power to

213
00:20:02,040 --> 00:20:06,840
hurt you whatsoever. Pain that arises also has no power to cause you suffering.

214
00:20:06,840 --> 00:20:12,360
It's just pain. Then as a result, you don't give rise to those emotions.

215
00:20:12,360 --> 00:20:15,880
See that those emotions are useless, are unwarranted,

216
00:20:15,880 --> 00:20:21,880
are harmful, are the true source of suffering and stress.

217
00:20:21,880 --> 00:20:25,640
And you just give them up. You've changed your views.

218
00:20:25,640 --> 00:20:31,880
Your views therefore don't give rise or don't lead to those emotions.

219
00:20:31,880 --> 00:20:36,360
It's not about tricking. It's not about, uh, there's no trick.

220
00:20:36,360 --> 00:20:40,200
There's no way to, there's no shortcut.

221
00:20:40,200 --> 00:20:44,040
You learn, you change, and you just don't want the things that right now you want.

222
00:20:44,040 --> 00:20:49,080
You just don't want them anymore. That's the way. There's only one way.

223
00:20:49,080 --> 00:20:54,040
And it's a hard way. It's the, uh,

224
00:20:54,040 --> 00:21:03,960
direct and, uh, honest, systematic, methodical way.

225
00:21:03,960 --> 00:21:05,720
Thank you, Banthay.

226
00:21:05,720 --> 00:21:07,800
You're welcome.

227
00:21:07,800 --> 00:21:11,640
If one's free from suffering and unattached to anything,

228
00:21:11,640 --> 00:21:16,200
wouldn't that make worldly pursuits pointless?

229
00:21:16,200 --> 00:21:20,120
What makes them pointful pointed?

230
00:21:20,120 --> 00:21:22,520
What's the opposite of pointless?

231
00:21:22,520 --> 00:21:24,360
Purposeful?

232
00:21:24,360 --> 00:21:26,200
Pointed is pointed over it.

233
00:21:26,200 --> 00:21:27,080
Hmm.

234
00:21:27,080 --> 00:21:29,000
Like, yes.

235
00:21:29,000 --> 00:21:32,200
It might not mean something else. It means blunt.

236
00:21:32,200 --> 00:21:35,880
That's supposed pointful is a word.

237
00:21:35,880 --> 00:21:39,480
No, it doesn't.

238
00:21:39,480 --> 00:21:42,600
It doesn't quite have any point in that way.

239
00:21:42,600 --> 00:21:44,600
Yeah.

240
00:21:45,880 --> 00:21:48,680
Um, you know, the, the pointless anyway,

241
00:21:48,680 --> 00:21:51,640
regardless of whether you meditate or not,

242
00:21:51,640 --> 00:21:54,680
worldly pursuits have no point.

243
00:21:54,680 --> 00:21:59,560
Um, any point that they lead to is, is just one point in time and,

244
00:21:59,560 --> 00:22:02,600
and life goes on after that.

245
00:22:02,600 --> 00:22:06,600
So you haven't accomplished anything permanent.

246
00:22:06,600 --> 00:22:10,040
Anything you've accomplished will eventually fade away.

247
00:22:10,040 --> 00:22:11,960
You'll wind up back where you've started.

248
00:22:11,960 --> 00:22:15,240
We do. That's why we're here today. We wind up

249
00:22:15,240 --> 00:22:20,120
again, back where we started again and again and again.

250
00:22:20,120 --> 00:22:25,320
And so meditation just helps you, um,

251
00:22:25,320 --> 00:22:30,360
realize that helps you give up your ambitions.

252
00:22:30,360 --> 00:22:33,160
As you see that really it's just the same mode.

253
00:22:33,160 --> 00:22:40,520
It's like a, a rat, a mouse on the wheel.

254
00:22:40,520 --> 00:22:43,880
You know, that's what our,

255
00:22:43,880 --> 00:22:47,160
that's what our, it ever is amount to in this world.

256
00:22:47,160 --> 00:22:48,920
Even helping people, you know,

257
00:22:48,920 --> 00:22:52,680
in the big secret is you can help people until you're blue in the face and

258
00:22:52,680 --> 00:22:55,960
there's still more people to help.

259
00:22:56,920 --> 00:23:02,040
So helping people becomes not, and not a means to and,

260
00:23:02,040 --> 00:23:04,920
you know, not helping people isn't the end itself.

261
00:23:04,920 --> 00:23:06,920
It's more of a means.

262
00:23:06,920 --> 00:23:09,480
Helping people is about doing the right thing

263
00:23:09,480 --> 00:23:15,080
and spiritual development and being on the right path yourself.

264
00:23:15,080 --> 00:23:19,640
That's the right way to live. That's about the best thing you can say of it.

265
00:23:19,640 --> 00:23:23,720
Not that it's going to change the world. I'm going to change the world. What does that mean?

266
00:23:23,720 --> 00:23:27,400
More always going to be more beings to help.

267
00:23:27,400 --> 00:23:34,520
Even after the universe explodes or collapses.

268
00:23:34,520 --> 00:23:37,640
It's going to start all over again. Eventually it will start all over again.

269
00:23:37,640 --> 00:23:40,680
There'll be more beings coming.

270
00:23:42,040 --> 00:23:44,440
All pointless in the end.

271
00:23:44,440 --> 00:23:49,320
It's like the book of kings in the Bible,

272
00:23:49,320 --> 00:23:52,120
which is kind of a funny book because he says everything's pointless.

273
00:23:52,120 --> 00:23:56,120
Everything's pointless. Therefore, therefore worship God.

274
00:23:56,120 --> 00:24:00,920
So it's a great book. It's just that completion isn't a bit weird.

275
00:24:00,920 --> 00:24:04,440
Because if it's all pointless, well, wouldn't you want to blame God and say,

276
00:24:04,440 --> 00:24:08,840
stupid God, what are you doing making this universe so pointless?

277
00:24:08,840 --> 00:24:14,040
But no, the world is pointless. Life is pointless.

278
00:24:14,040 --> 00:24:18,200
Therefore, spend your time preaching God.

279
00:24:18,200 --> 00:24:26,840
I don't know. Not very impressive.

280
00:24:26,840 --> 00:24:30,840
This question is probably a result of my poor understanding of this subject.

281
00:24:30,840 --> 00:24:34,920
Because when I think about it logically, this is from the same person.

282
00:24:34,920 --> 00:24:38,280
When I think about it logically, that's what I feel it comes down to.

283
00:24:38,280 --> 00:24:41,640
But even if I were completely unattached to anything,

284
00:24:41,640 --> 00:24:44,760
I imagine I'd still have the desire to travel to new places,

285
00:24:44,760 --> 00:24:49,480
read new books, and learn new stuff.

286
00:24:49,480 --> 00:24:51,880
That was a really important more of a comment.

287
00:24:51,880 --> 00:24:56,600
No, you wouldn't. When your desire is an attachment,

288
00:24:56,600 --> 00:25:00,040
it's just semantics. Whether you're talking about desire or attachment, there's

289
00:25:01,480 --> 00:25:06,440
something you're stuck on. That's leading you on,

290
00:25:06,440 --> 00:25:09,080
is going to lead to further becoming.

291
00:25:09,080 --> 00:25:13,400
Further becoming means something is going to come of it.

292
00:25:13,400 --> 00:25:16,120
So if you didn't have the desire to travel, there wouldn't be

293
00:25:17,080 --> 00:25:21,240
travel. So travel is going to come of it. There wouldn't be all the issues surrounding

294
00:25:21,240 --> 00:25:28,280
travel, like visas, and expenses, and well, meeting new people.

295
00:25:28,280 --> 00:25:30,680
I mean, travel isn't bad. I'm not suggesting that.

296
00:25:30,680 --> 00:25:35,560
But your desire to travel creates something for you,

297
00:25:35,560 --> 00:25:40,920
and more becoming. And it also creates the desire to travel.

298
00:25:40,920 --> 00:25:44,600
It becomes habitual. It's not an end to itself.

299
00:25:44,600 --> 00:25:49,240
When you desire to travel, it creates a more liking of the traveling experience,

300
00:25:49,240 --> 00:25:55,080
and you become what we'd say intoxicated by it, and wanted more.

301
00:25:55,080 --> 00:25:58,200
And then when you can't travel, or when people say traveling sucks,

302
00:25:58,200 --> 00:26:01,720
you shouldn't travel. Why don't you stay at home? Why don't you settle down?

303
00:26:01,720 --> 00:26:06,440
You get upset. Why do you get upset? Because you're attached.

304
00:26:06,440 --> 00:26:09,400
It's all very subtle. It's not a big deal. It's not like you're going to

305
00:26:09,400 --> 00:26:16,120
health for it. It's not evil evil. But it's part of what leads to more rebirth,

306
00:26:16,120 --> 00:26:19,800
and it's just in the end, same old, same old.

307
00:26:21,320 --> 00:26:25,880
We're excited by new things, and they become old, and we need new earth things.

308
00:26:28,520 --> 00:26:31,720
And then we die, and forget it all, and come back, and do it again.

309
00:26:31,720 --> 00:26:33,480
And in the meantime, we suffer a lot.

310
00:26:34,680 --> 00:26:37,880
Eventually start to say, is it really worth all the effort?

311
00:26:37,880 --> 00:26:42,600
And then we start to meditate, and we say, no, it's not really worth the effort.

312
00:26:42,600 --> 00:26:47,640
Better I just let go.

313
00:26:56,520 --> 00:26:58,680
And that catches up all the questions for tonight.

314
00:26:59,960 --> 00:27:04,200
Is it? It'd be no way for over a week. I did three exams,

315
00:27:04,200 --> 00:27:11,560
and I think I got a 90, a 95, and a 99.

316
00:27:12,440 --> 00:27:13,000
Very nice.

317
00:27:15,000 --> 00:27:21,960
Yeah. So, made me think that I did learn stuff, especially in philosophy,

318
00:27:21,960 --> 00:27:24,440
which was surprising, because I thought, oh, this is rubbish.

319
00:27:24,440 --> 00:27:28,840
This western philosophy. But they really frame their arguments in an interesting way,

320
00:27:28,840 --> 00:27:32,440
and they're talking about a lot of the same things that they were talking about in India.

321
00:27:32,440 --> 00:27:40,600
They are talking about important topics, even from a meditative from a Buddhist point of view.

322
00:27:40,600 --> 00:27:44,920
Most of them have it all wrong. In fact, all of them have most of it all wrong.

323
00:27:45,880 --> 00:27:51,800
But this is the kind of important argument that we have to be able to address.

324
00:27:51,800 --> 00:28:01,160
So, like the idea of free will, of desire,

325
00:28:04,760 --> 00:28:05,560
but it exists.

326
00:28:07,560 --> 00:28:11,080
Yeah, it's not entirely relevant, but much of it is.

327
00:28:12,520 --> 00:28:16,040
Then learning languages, even learning Latin was, I think, a great thing.

328
00:28:16,040 --> 00:28:20,920
I realized that Latin is actually the language.

329
00:28:20,920 --> 00:28:25,320
It is the linga Franca for the world, so it is something that you can actually use to talk to people.

330
00:28:27,320 --> 00:28:32,040
Funny, I always wanted to use poly in that way, but there's very few people who speak poly anymore.

331
00:28:34,680 --> 00:28:35,320
Even love.

332
00:28:38,600 --> 00:28:40,440
At least you know what the chanting is all about.

333
00:28:40,440 --> 00:28:48,200
Well, by knowing poly, you know what the chanting is, you know what you're chanting.

334
00:28:48,200 --> 00:28:49,720
And like most of us.

335
00:28:49,720 --> 00:28:52,600
I can read the tepitika. The chanting isn't a big deal for me.

336
00:28:53,720 --> 00:28:56,920
But being able to read the text, that's awesome.

337
00:28:58,040 --> 00:29:02,040
Absolutely, there's nothing wrong with learning, but that's why I'd like to be able to speak it with people.

338
00:29:02,040 --> 00:29:11,320
It would keep me sharp and help me train, train it more, but no one has speak it.

339
00:29:14,920 --> 00:29:18,520
Well, then, that was half an hour, so I guess that's all we get tonight.

340
00:29:19,400 --> 00:29:22,040
But if you have more questions, I'll try to be back every night.

341
00:29:23,240 --> 00:29:25,800
Tomorrow is Christmas Eve.

342
00:29:26,920 --> 00:29:29,560
Tomorrow might not be able to, I think, tomorrow,

343
00:29:29,560 --> 00:29:37,880
tomorrow's autumn, also my father's birthday, so we'll probably be doing a family hangout with him.

344
00:29:39,480 --> 00:29:45,160
And then the next day is Christmas, which of course is meaningless.

345
00:29:46,440 --> 00:29:54,440
So my family will probably be picking out on Turkey, but at nine, I should be able to broadcast

346
00:29:54,440 --> 00:30:00,360
Christmas day, probably available Christmas day in the morning. Yes.

347
00:30:01,720 --> 00:30:07,880
Let's plan to be here, probably not tomorrow, but probably day after day.

348
00:30:08,600 --> 00:30:12,840
That sounds good. And that a question, but Lee, Lee had a comment that it looks like

349
00:30:12,840 --> 00:30:17,160
your auditioning for Star Wars with the black background. I thought the same thing, there's a new

350
00:30:17,160 --> 00:30:28,200
Star Wars out now. Yeah, it has a bit of a Star Wars look in your background.

351
00:30:28,920 --> 00:30:31,560
You know, I look like a vocal or something.

352
00:30:31,560 --> 00:30:38,600
Yes, a little bit, a little bit. And there is one more question if you have a couple more minutes,

353
00:30:38,600 --> 00:30:44,680
one day. All right. Okay, I constantly feel very stressed when I meditate,

354
00:30:44,680 --> 00:30:49,320
and this makes it difficult to feel any movement at the abdomen. What should I do?

355
00:30:50,040 --> 00:30:54,280
Feels stressed constantly? Yes. Well, focus on the stress.

356
00:30:55,880 --> 00:31:00,440
I would try lying meditation when you're really stressed. Lying meditation can be great,

357
00:31:01,960 --> 00:31:08,760
but focus on the stress and say stress, stress, anxious, or frustrated. However, there's

358
00:31:08,760 --> 00:31:14,680
different flavors of stress, right? So whatever the flavor is, you're worried, or anxious, or if

359
00:31:14,680 --> 00:31:23,240
you're angry, you're depressed, or just stressed. Stress, say stress. It's overwhelmed if you're

360
00:31:23,240 --> 00:31:29,880
overwhelmed. But if you can't feel the abdomen at all, try lying down. It'll help. Eventually,

361
00:31:29,880 --> 00:31:35,000
it'll work itself out. If you meditate regularly, you'll find yourself getting less stressed,

362
00:31:35,000 --> 00:31:40,200
and it'll work. It'll eventually be easier to sit up and meditate. So in the meantime,

363
00:31:40,200 --> 00:31:47,000
you've got to approach it from the side through lying meditation,

364
00:31:48,840 --> 00:31:58,280
through walking meditation, break it up a bit. It'll get better. It sounds like probably your

365
00:31:58,280 --> 00:32:05,240
beginner. Your beginner is at everything. You're not very proficient. Take your time. You'll get there.

366
00:32:06,040 --> 00:32:14,120
If you want, it looks like we got lots of slots free. So you can sign up to meet with me

367
00:32:14,120 --> 00:32:19,800
once a week. You can go through a course. You're able to do at least an hour a day, preferably

368
00:32:19,800 --> 00:32:28,840
two or eventually up to two hours a day. Can you sign up there? That's right. I was supposed to

369
00:32:28,840 --> 00:32:33,640
ask you, Bante, are you conducting the individual meetings while you're away? Or is everything

370
00:32:33,640 --> 00:32:40,600
cancelled? I think I thought I missed actually. I didn't miss because there weren't any. There's

371
00:32:40,600 --> 00:32:46,760
a lot of free free ones. I don't know the amount of missed, but I didn't. No one came. Okay.

372
00:32:46,760 --> 00:32:55,640
We'll stand up. But yeah, I'm doing them. Today I did one, two. I guess I did one in the morning as

373
00:32:55,640 --> 00:33:04,840
well. No, Adam wasn't here. He's away for Christmas. But tomorrow I have three. I'll go to

374
00:33:04,840 --> 00:33:17,080
his friend, one on Friday, and then I've added two extras on Saturday. And there's four people on

375
00:33:17,080 --> 00:33:28,200
Sunday. Yeah, lots of free spots. Maybe I'll make a push. Maybe I'll do a YouTube video about that

376
00:33:28,200 --> 00:33:37,720
at some point, so that people know they can sign up for Christmas. Be good. Okay. Thanks, Robin,

377
00:33:37,720 --> 00:33:44,520
for joining me. Thank you. Thank you. Have a good night. Thank you.

378
00:33:44,520 --> 00:34:00,280
Oh, Christmas. Happy Christmas. Take care.

