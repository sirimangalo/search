1
00:00:00,000 --> 00:00:02,000
Okay, go ahead.

2
00:00:02,000 --> 00:00:08,000
Should you communicate with your parents if they engage in illegal drugs, go to jail frequently,

3
00:00:08,000 --> 00:00:12,000
have dangerous friends, and constantly feed their defilements? Thank you.

4
00:00:20,000 --> 00:00:24,000
I think it's even more important that you communicate with your parents,

5
00:00:24,000 --> 00:00:34,000
but it's not so much about communicating as being a presence in their lives.

6
00:00:34,000 --> 00:00:47,000
Because a great part of our direction in life has to do with the people we associate with.

7
00:00:47,000 --> 00:00:53,000
But it's that association with good people is the entirety of the holy life.

8
00:00:53,000 --> 00:00:57,000
It's everything.

9
00:00:57,000 --> 00:01:04,000
And so if you're the only good that they have in their life,

10
00:01:04,000 --> 00:01:07,000
then by all means you have to stick to that, cling to that,

11
00:01:07,000 --> 00:01:13,000
in fact, and be that good thing in their lives.

12
00:01:13,000 --> 00:01:16,000
But, of course, you have to be strong.

13
00:01:16,000 --> 00:01:24,000
A better question would be should you communicate with your parents if you're angry, frustrated,

14
00:01:24,000 --> 00:01:33,000
worried, scared about the fact that they engage in illegal drugs, go to jail frequently, etc, etc.

15
00:01:33,000 --> 00:01:36,000
In that case, no, you've got a problem.

16
00:01:36,000 --> 00:01:46,000
If you react to, or maybe you're also engaging in those things, then you're a bad influence in their lives.

17
00:01:46,000 --> 00:01:56,000
But just because they are engaging in all of those things,

18
00:01:56,000 --> 00:02:01,000
isn't it enough for a reason not to interact with them?

19
00:02:01,000 --> 00:02:04,000
Unless it's affecting you as well.

20
00:02:04,000 --> 00:02:16,000
So in a sense you have to balance here because you have some kind of potential duty towards your parents in the conventional sense.

21
00:02:16,000 --> 00:02:25,000
In a real sense you have a debt to them for whatever good they did for you.

22
00:02:25,000 --> 00:02:30,000
But you have to balance that with your own well-being and with the situation,

23
00:02:30,000 --> 00:02:34,000
the circumstances and the reality of the situation.

24
00:02:34,000 --> 00:02:39,000
If being around them is not helping you and is not helping them,

25
00:02:39,000 --> 00:02:42,000
which in the end turn out to be very much the same thing,

26
00:02:42,000 --> 00:02:47,000
then no, you should probably step back.

27
00:02:47,000 --> 00:02:53,000
But if you find that it's helpful for both you and them for you to be near them,

28
00:02:53,000 --> 00:03:01,000
then by all means continue.

29
00:03:01,000 --> 00:03:06,000
I guess the point is that you often can't get away from your parents,

30
00:03:06,000 --> 00:03:11,000
and so rather than wishing you could get away from them,

31
00:03:11,000 --> 00:03:23,000
you should be steadfast and mindful.

32
00:03:23,000 --> 00:03:31,000
The only way you can really get away from people is by working out your karmic involvement with them.

33
00:03:31,000 --> 00:03:36,000
Maybe that's not even fair because you can break through that by attaining enlightenment.

34
00:03:36,000 --> 00:03:45,000
You can free yourself from your karmic even for future karmic involvement.

35
00:03:45,000 --> 00:03:48,000
So yeah, I mean in the end you can break through.

36
00:03:48,000 --> 00:03:53,000
There's nothing wrong with not having contact with people.

37
00:03:53,000 --> 00:03:55,000
You shouldn't feel maybe something's wrong.

38
00:03:55,000 --> 00:04:01,000
I'm not involved at all with anyone.

39
00:04:01,000 --> 00:04:09,000
But you shouldn't feel that. There's no reason to be involved with any certain individual.

40
00:04:09,000 --> 00:04:20,000
But our attachments that are often due to feelings of gratitude and indebtedness towards our parents.

41
00:04:20,000 --> 00:04:24,000
It's very strong and it tends to lead us back to them.

42
00:04:24,000 --> 00:04:31,000
It led Sarri put it back to his mother before he passed away he went back to see his mother and he was already in our heart.

43
00:04:31,000 --> 00:04:37,000
Because he wanted to, he thought, how can I go into Parinibana without?

44
00:04:37,000 --> 00:04:43,000
How can I leave the world without having helped my own mother?

45
00:04:43,000 --> 00:04:49,000
But I guess the point is that there's no hard and fast rule.

46
00:04:49,000 --> 00:04:55,000
Often this question is asked when actually the case is that you're trying to get away from them.

47
00:04:55,000 --> 00:05:00,000
And feeling bad that you can't, that you're always pulled in.

48
00:05:00,000 --> 00:05:09,000
But if you're pulled in it means you're a part of the situation and you have to extricate yourself from the situation through mindfulness.

49
00:05:09,000 --> 00:05:13,000
It's not through running away. It won't help. It won't make things better.

50
00:05:13,000 --> 00:05:19,000
Just end up running back to them once you're pendulum back and forth.

