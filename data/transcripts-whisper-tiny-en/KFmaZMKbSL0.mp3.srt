1
00:00:00,000 --> 00:00:02,000
He was a virgin creature.

2
00:00:04,500 --> 00:00:09,000
And so he, he practically started trying himself for six years.

3
00:00:09,000 --> 00:00:14,000
He thought of him, he would go to India and see the place where he practiced.

4
00:00:16,000 --> 00:00:18,000
Practice, up in the hills, in the caves.

5
00:00:18,000 --> 00:00:21,000
Now there's a Tibetan one that's going to be left.

6
00:00:21,000 --> 00:00:30,000
And then he realized that this wasn't going to use it, so he gave it up.

7
00:00:30,000 --> 00:00:33,000
And he went down from the village below and went to work.

8
00:00:35,000 --> 00:00:44,000
And the other five ascetics decided that this was unacceptable, so they left.

9
00:00:44,000 --> 00:00:46,000
He said, you're not going to torture yourself.

10
00:00:46,000 --> 00:00:53,000
When you hurt yourself, you burn up all the disciples inside because they believed that

11
00:00:53,000 --> 00:01:01,000
when you torture yourself, when you hurt yourself, you burn up any evil that's missing.

12
00:01:01,000 --> 00:01:07,000
This is even the day in India, this is the day, this is the day, this is the day.

13
00:01:07,000 --> 00:01:10,000
And so they left it.

14
00:01:10,000 --> 00:01:15,000
And he decided that he had to find the middle way, but he fell back to you.

15
00:01:15,000 --> 00:01:20,000
And indulging in the central place, like in India, he could be the pastor of India,

16
00:01:20,000 --> 00:01:21,000
and he was also the one who worked with him.

17
00:01:21,000 --> 00:01:23,000
So he found him.

18
00:01:27,000 --> 00:01:32,000
And when he went back to meet these five months, they didn't want to listen.

19
00:01:32,000 --> 00:01:36,000
They didn't even want to read it because they're a teacher from the right beginning.

20
00:01:36,000 --> 00:01:45,000
So normally they would have to receive his role, they would receive his role,

21
00:01:45,000 --> 00:01:48,000
and would have to see for him.

22
00:01:48,000 --> 00:01:52,000
He'd stand up to read him.

23
00:01:52,000 --> 00:01:55,000
And they said, we're not going to do any of those things.

24
00:01:55,000 --> 00:01:57,000
We see he's not incoming as an object or something.

25
00:01:57,000 --> 00:01:59,000
That's not stand up.

26
00:01:59,000 --> 00:02:01,000
Let's not give him a negative role.

27
00:02:01,000 --> 00:02:03,000
It's not receiving like a teacher.

28
00:02:03,000 --> 00:02:07,000
He's not going to put out a seed for him.

29
00:02:11,000 --> 00:02:17,000
And so the Buddha, in other words, walked 120 miles to 120 kilometers from India.

30
00:02:17,000 --> 00:02:19,000
And the London.

31
00:02:19,000 --> 00:02:23,000
From the time where we last met, we think of how all that we've been doing,

32
00:02:23,000 --> 00:02:25,000
we think that the Buddha was walking.

33
00:02:25,000 --> 00:02:28,000
From we sat up with a judge, I sat down with him.

34
00:02:28,000 --> 00:02:31,000
He was walking.

35
00:02:31,000 --> 00:02:36,000
That's why I used the Buddha to be a refuge.

36
00:02:36,000 --> 00:02:39,000
I used the Buddha to be a refuge.

37
00:02:43,000 --> 00:02:47,000
And when he got there, this is how they were going to receive him.

38
00:02:47,000 --> 00:02:52,000
But as he got close, he was a wonderful present.

39
00:02:52,000 --> 00:02:55,000
And this is the meaner.

40
00:02:55,000 --> 00:02:57,000
He made them decide to think about it.

41
00:02:57,000 --> 00:03:01,000
I think when he decided to write all of what the Buddha used,

42
00:03:01,000 --> 00:03:03,000
and they involved really had to stand up.

43
00:03:03,000 --> 00:03:06,000
One of the peoples was one of them to grow.

44
00:03:06,000 --> 00:03:09,000
One of them was that the fan had started expanding.

45
00:03:12,000 --> 00:03:15,000
And then when he sat down on the seat that they had prepared.

46
00:03:21,000 --> 00:03:23,000
They addressed it by his family.

47
00:03:23,000 --> 00:03:25,000
They brought them up.

48
00:03:25,000 --> 00:03:28,000
And they didn't address it like a picture.

49
00:03:28,000 --> 00:03:30,000
One of them went over to the present.

50
00:03:30,000 --> 00:03:33,000
Each another from the end of the day.

51
00:03:35,000 --> 00:03:36,000
Done.

52
00:03:36,000 --> 00:03:38,000
Away from the past.

53
00:03:39,000 --> 00:03:42,000
And it's amazing that even though they felt this present,

54
00:03:42,000 --> 00:03:44,000
they really knew how it felt.

55
00:03:44,000 --> 00:03:45,000
It's incredible.

56
00:03:47,000 --> 00:03:49,000
It's still a little bit slack.

57
00:03:49,000 --> 00:03:53,000
That's what he said to me.

58
00:03:53,000 --> 00:03:55,000
It's not proper to do this.

59
00:03:55,000 --> 00:03:59,000
The proper way to address someone was to become a holy fighter.

60
00:03:59,000 --> 00:04:01,000
It just wanted him up.

61
00:04:01,000 --> 00:04:05,000
The way he said it is not just me.

62
00:04:05,000 --> 00:04:07,000
It was a bit awesome.

63
00:04:07,000 --> 00:04:12,000
He was making those things around about the way that he became a holy fighter.

64
00:04:12,000 --> 00:04:14,000
He was like, oh, he was like, oh, he was the one.

65
00:04:14,000 --> 00:04:18,000
He was not the way to address the holy fighter.

66
00:04:18,000 --> 00:04:22,000
He was like, oh, he was just a copyright judge.

67
00:04:22,000 --> 00:04:26,000
He was just like, oh, he was claiming that he was a holy fighter.

68
00:04:29,000 --> 00:04:32,000
And they replied to him, oh, go to the back.

69
00:04:32,000 --> 00:04:34,000
Someone like, go to the back.

70
00:04:36,000 --> 00:04:39,000
Even when you were torturing yourself, you could have a nice value

71
00:04:39,000 --> 00:04:40,000
to come back to you.

72
00:04:40,000 --> 00:04:41,000
You didn't lose.

73
00:04:43,000 --> 00:04:44,000
It was torturing himself.

74
00:04:44,000 --> 00:04:47,000
When they purchased himself, it was a hero that he was a leader.

75
00:04:47,000 --> 00:04:49,000
Not enough to root in you.

76
00:04:53,000 --> 00:04:56,000
Some people were Startrutes thousand holy guys

77
00:04:56,000 --> 00:04:58,000
They re-purposed hand styles.

78
00:04:58,000 --> 00:05:00,000
There's so much that they can miss their lives.

79
00:05:00,000 --> 00:05:02,000
Help me help you.

80
00:05:04,000 --> 00:05:06,000
I was thinking that one themselves was the type

81
00:05:07,000 --> 00:05:09,000
and the one they sent them out.

82
00:05:10,000 --> 00:05:12,000
I felt this a prime stuff home and movement.

83
00:05:13,000 --> 00:05:15,000
Even when you purchased yourself,

84
00:05:15,000 --> 00:05:16,000
you got to think of the power that they bring.

85
00:05:16,000 --> 00:05:23,000
I think I was just really thinking about the life stuff that I have to say.

86
00:05:23,000 --> 00:05:30,000
And if they stop my life, it's just the only way that we come to life.

87
00:05:30,000 --> 00:05:35,000
You can see it's given that up. It must not be, you know.

88
00:05:35,000 --> 00:05:40,000
And again, if we don't address it, it's important to ask that I found it,

89
00:05:40,000 --> 00:05:44,000
just to bring the life into the system, what I have to say.

90
00:05:44,000 --> 00:05:48,000
I think we can do the way other things.

