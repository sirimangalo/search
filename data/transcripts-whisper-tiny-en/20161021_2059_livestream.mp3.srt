1
00:00:00,000 --> 00:00:03,000
First one, I don't know.

2
00:00:07,000 --> 00:00:09,000
Well, that's a good one, both.

3
00:00:14,000 --> 00:00:16,000
Right, we'll look at 215 and 216,

4
00:00:16,000 --> 00:00:18,000
because there's some good things.

5
00:00:18,000 --> 00:00:24,000
But the reason, the real reason I chose this is because I wanted to talk about patients.

6
00:00:24,000 --> 00:00:35,000
I think we're, well, these two students talk about the dangers of being impatient.

7
00:00:35,000 --> 00:00:37,000
It's useful to know.

8
00:00:37,000 --> 00:00:44,000
But I just wanted to talk in general about patients, at least.

9
00:00:44,000 --> 00:00:48,000
Patients is a virtue.

10
00:00:48,000 --> 00:00:51,000
We hear a lot about patients.

11
00:00:51,000 --> 00:00:55,000
When you think about patients, no one says,

12
00:00:55,000 --> 00:00:59,000
I don't ever hear anyone say, man, that person is too patient for me.

13
00:00:59,000 --> 00:01:23,000
They can be around them.

