1
00:00:00,000 --> 00:00:29,560
A little bit messy, which is still trying to get this, I think I figured out a little bit

2
00:00:29,560 --> 00:00:38,480
about how to make shits, I don't know, whatever, so today we're going to do another

3
00:00:38,480 --> 00:00:55,720
dumbapada, we're ready, test, yes, hello and welcome back to our study of the dumbapada,

4
00:00:55,720 --> 00:01:08,440
today we continue on with verse number 106, which reads as follows, not like that,

5
00:01:08,440 --> 00:01:32,600
come on body behave, hello and welcome back to our study of the dumbapada, today we continue

6
00:01:32,600 --> 00:01:54,300
on with verse 106, which reads as follows,

7
00:01:54,300 --> 00:02:12,300
one can give 1000, 1000 month after month, 1000 years just lying for 1000 pieces of

8
00:02:12,300 --> 00:02:20,300
money or when you think 1000 dollars, 1000 pieces of money, 1000 gold coins,

9
00:02:20,300 --> 00:02:37,340
one can sacrifice every month after month, 1000 every month for 100 years,

10
00:02:37,340 --> 00:02:51,340
one time, one time, one moment, if one should pay homage for one moment,

11
00:02:51,340 --> 00:03:03,540
one who is self-controlled or not controlled, who is a developed self, who is self-developed

12
00:03:03,540 --> 00:03:16,780
or cultivated, saiye wa pujana saiyo, this, this pujah is greater, yanjay wa sasatam

13
00:03:16,780 --> 00:03:35,980
go down greater than the 1000, the greater than the 100 years, the 100 years of sacrifice.

14
00:03:35,980 --> 00:03:43,380
So this verse was spoken in regards to sari putas uncle, we told that sari putas

15
00:03:43,380 --> 00:03:50,140
had an uncle who was a brahman sari putas whole family were brahman and before the sari

16
00:03:50,140 --> 00:03:57,940
putas passed away, he actually went back to teach his mother, who was still a wrong view,

17
00:03:57,940 --> 00:04:05,860
still had the idea that the bhutatat was wrong and sari putatat had lost his way by becoming

18
00:04:05,860 --> 00:04:13,100
a bhutas monk and so we have that story elsewhere, but here we have a story of an

19
00:04:13,100 --> 00:04:21,940
uncle that is who is a brahman, who also had wrong views and so it's kind of a funny

20
00:04:21,940 --> 00:04:27,660
and it's very short story but it's kind of a funny little story, so sari putat went

21
00:04:27,660 --> 00:04:35,980
to see his uncle and asked him straight up, do you ever do any good deeds and his uncle

22
00:04:35,980 --> 00:04:45,140
right away said, oh yes, actually, every month I give or per month I give what amounts

23
00:04:45,140 --> 00:04:51,100
to a thousand gold coins worth of offerings, I give offerings amounting to a thousand gold

24
00:04:51,100 --> 00:05:00,780
coins a month and he says, okay who do you give them, who do you give this to and I give

25
00:05:00,780 --> 00:05:08,060
it to the naked aesthetics, these guys who, as their religious practice, take to not

26
00:05:08,060 --> 00:05:17,740
wearing clothes because somehow that's meaningful, it's considered to be more hardcore, it

27
00:05:17,740 --> 00:05:24,420
is pretty hardcore, if you want to be go extreme, not wearing clothes is the way to go because

28
00:05:24,420 --> 00:05:31,700
it means in the sun you're at the mercy of the sun and the mercy of the cold, the mercy

29
00:05:31,700 --> 00:05:43,220
of the flies and the insects, couldn't imagine doing that in a place where there were mosquitoes

30
00:05:43,220 --> 00:05:53,980
and certainly makes you stand out, makes it difficult for you to fit in in society.

31
00:05:53,980 --> 00:06:00,580
But these were the people that he was paying respect to and, so sorry put to says, well

32
00:06:00,580 --> 00:06:06,740
why do you, what do you hope to get from that, okay, so you give money to these naked

33
00:06:06,740 --> 00:06:11,420
aesthetics, what do you hope to get from that, he said, oh well, because of this I'm,

34
00:06:11,420 --> 00:06:22,780
I, I, my aspiration is to go to the Brahma realms and, sorry put to asks, was that the

35
00:06:22,780 --> 00:06:28,620
way to go to the Brahma realms and he says, oh yes, I'm slowly and he said, okay, where

36
00:06:28,620 --> 00:06:35,860
did you hear this, he says, oh, my teachers, the naked aesthetics told me, which is kind

37
00:06:35,860 --> 00:06:40,980
of how you get, what you get in all religions, the teachers will tell you, if you give to

38
00:06:40,980 --> 00:06:49,500
us, it is a greatest benefit, it's a very, it's actually an interesting story and in some

39
00:06:49,500 --> 00:06:57,180
ways it's a bit, well, it's not one of my favorite stories, but I think we have to talk

40
00:06:57,180 --> 00:07:02,700
about this and, and sort of come to an understanding of what's meant here, but I think

41
00:07:02,700 --> 00:07:08,260
one way you can look at this and of course, you know, it's, this is me finessing it, but

42
00:07:08,260 --> 00:07:15,380
okay, so the story goes, sorry about the Shakespeare's head and says, Brahmin, that's not

43
00:07:15,380 --> 00:07:20,340
the way, well, we all know that's not the way, the Brahma realms and he says, you're not

44
00:07:20,340 --> 00:07:24,700
on the path to the Brahma realms and your teachers don't know the way to the Brahma realms,

45
00:07:24,700 --> 00:07:30,380
you don't know and neither your teachers know the way to the world of God, basically.

46
00:07:30,380 --> 00:07:36,660
And he says, the Buddha is the only one who knows.

47
00:07:36,660 --> 00:07:44,740
So come and see the Buddha, come with me and I ask him to tell you the way to Brahma.

48
00:07:44,740 --> 00:07:50,500
So the idea here is that, I mean, it's, the problem here is that it's just still just

49
00:07:50,500 --> 00:07:54,660
an appeal out to authority who says the Buddha is going to know better than these guys.

50
00:07:54,660 --> 00:07:59,220
If they say X and the Buddha says, why, how do you tell which one is right and which one

51
00:07:59,220 --> 00:08:00,220
is wrong?

52
00:08:00,220 --> 00:08:08,740
So he goes to the Buddha and I think, I think you have to take it, we have to see exactly

53
00:08:08,740 --> 00:08:13,460
what's being said here and, and of course, the story, the actual words of the story may

54
00:08:13,460 --> 00:08:15,620
just be an abbreviation.

55
00:08:15,620 --> 00:08:23,020
But basically the Buddha says to him, giving to those guys is worthless, giving to monks

56
00:08:23,020 --> 00:08:31,900
in my religion, that's what will lead you to or that's a worth a thousand times or it's

57
00:08:31,900 --> 00:08:43,420
not worth a thousandth part of simply looking at, simply looking at one of my disciples

58
00:08:43,420 --> 00:08:50,820
and so, and then he teaches this verse.

59
00:08:50,820 --> 00:08:55,660
And so you kind of get this sense, well, you know, it's just the Buddha saying the

60
00:08:55,660 --> 00:09:00,180
same thing as everyone else, give, if you give to, to my disciples.

61
00:09:00,180 --> 00:09:05,500
But first of all, you would have to agree if you, if you follow the Buddha's teaching,

62
00:09:05,500 --> 00:09:12,060
if you, if you have some understanding of the idea of karma, then you'd have to agree

63
00:09:12,060 --> 00:09:16,420
well, you'd look at how the Buddha's monks practice and you'd say, we're not just the

64
00:09:16,420 --> 00:09:19,660
Buddha's monks, but people have followed the Buddha's teaching, you look at how they

65
00:09:19,660 --> 00:09:23,100
practice and you say, wow, he has giving to them in some great fruit.

66
00:09:23,100 --> 00:09:28,380
Of course, we believe that as Buddhists, the question is how do we, how do we explain

67
00:09:28,380 --> 00:09:33,580
it beyond just making it sound like we're promoting ourselves?

68
00:09:33,580 --> 00:09:38,340
Because that really does just sound like self-promotion and some said before, some of

69
00:09:38,340 --> 00:09:43,820
these verses, but not the verses, but some, most of the, more of the stories tend to be

70
00:09:43,820 --> 00:09:47,700
self-aggrandizing or self-promoting.

71
00:09:47,700 --> 00:09:53,700
So in a sense, you, you kind of have to, well, maybe not even, maybe not necessarily

72
00:09:53,700 --> 00:09:57,740
be skeptical yourself, but you have to accept that people are going to look at these and

73
00:09:57,740 --> 00:10:04,260
say, this is just like religion always is, it's all about giving, giving, giving.

74
00:10:04,260 --> 00:10:13,620
Because some of the verses are about giving and tend to be, or the stories tend to be

75
00:10:13,620 --> 00:10:21,900
instructive in regards to giving to the monks, giving to the Buddhist religion, being

76
00:10:21,900 --> 00:10:24,300
the best way.

77
00:10:24,300 --> 00:10:31,940
But the saving grace, if in his one, comes from the verse itself because the verse doesn't

78
00:10:31,940 --> 00:10:34,700
actually say that, right?

79
00:10:34,700 --> 00:10:39,460
The verse, verse makes the important distinction that we would all make as Buddhists, that

80
00:10:39,460 --> 00:10:45,460
it doesn't actually matter what religion a person is in or even what teachings they

81
00:10:45,460 --> 00:10:48,060
follow necessarily.

82
00:10:48,060 --> 00:10:54,380
It's that they are bawitata, which means they have, they are self-cultivated.

83
00:10:54,380 --> 00:10:59,100
I mean, that, that of course has to be qualified as well.

84
00:10:59,100 --> 00:11:02,020
It's kind of cultivation, do you mean?

85
00:11:02,020 --> 00:11:11,020
But it means enlightened if a person is truly, has truly done something to deserve a gift

86
00:11:11,020 --> 00:11:13,940
and it's a greater fruit.

87
00:11:13,940 --> 00:11:19,540
In Buddhism, we recognize four kinds of, four or four aspects.

88
00:11:19,540 --> 00:11:27,580
There is the, well, the two, there is the, sorry, three aspects, the purity of the

89
00:11:27,580 --> 00:11:37,340
giver, the purity of the, the recipient and the purity of the gift.

90
00:11:37,340 --> 00:11:44,700
So if the gift is pure, then that, then it's then, then it's considered to be a greater

91
00:11:44,700 --> 00:11:47,940
benefit, a greater fruit than if the gift is impure.

92
00:11:47,940 --> 00:11:54,020
So for example, if it's giving alcohol, or if it's a gift of poison, then that would

93
00:11:54,020 --> 00:12:01,140
be a less beneficial gift, but if it's a gift of food, or a gift of shelter, or a gift

94
00:12:01,140 --> 00:12:14,940
of medicine, or a gift of something like clothing, then that would be a greater benefit

95
00:12:14,940 --> 00:12:37,420
or if it's a gift of dhamma, a gift of truth, then that would be a most benefit.

96
00:12:37,420 --> 00:12:41,820
And if the person is, if the giver is pure, so if the person giving is a moral person

97
00:12:41,820 --> 00:12:46,780
is an ethical person, is a developed person, then of course their intentions are going

98
00:12:46,780 --> 00:12:51,700
to be pure, and so the gift is, is purified in that way.

99
00:12:51,700 --> 00:12:59,900
If the recipient is pure, then it's better than if the recipient is a immoral, unwholesome,

100
00:12:59,900 --> 00:13:03,380
uncultivated individual.

101
00:13:03,380 --> 00:13:07,420
So that, that's the, sort of the detail teaching here.

102
00:13:07,420 --> 00:13:13,860
Basically, it's a teaching about where we should pay homage and respect, who we should

103
00:13:13,860 --> 00:13:15,340
respect.

104
00:13:15,340 --> 00:13:20,540
So it's, it's not a deep teaching, it's not the kind of teaching that you would say,

105
00:13:20,540 --> 00:13:23,020
well, if I follow this, I'm going to become enlightened.

106
00:13:23,020 --> 00:13:24,980
How does it relate to our meditation practice?

107
00:13:24,980 --> 00:13:36,180
Well, there is a sense that our practice of charity is supportive of our meditation

108
00:13:36,180 --> 00:13:39,180
practice, even as, as Buddhist monks.

109
00:13:39,180 --> 00:13:45,060
So giving the dhamma, teaching the dhamma is, is a kind of a gift, and it's a support of

110
00:13:45,060 --> 00:13:49,460
the practice of the person teaching.

111
00:13:49,460 --> 00:13:58,780
And for those people who have monetary resources, giving in this way is, means of support.

112
00:13:58,780 --> 00:14:01,820
And it is a question that comes up, what do I do with my money?

113
00:14:01,820 --> 00:14:09,140
I have this money, and part of it I want to give to charity in order to support my own

114
00:14:09,140 --> 00:14:10,300
spiritual development.

115
00:14:10,300 --> 00:14:14,980
I want to be kind and generous and supportive of good things.

116
00:14:14,980 --> 00:14:19,300
So what are the good things I should support, should I support these guys who are going naked

117
00:14:19,300 --> 00:14:26,300
or should I support these guys who are cultivating insight meditation?

118
00:14:26,300 --> 00:14:32,700
And as Buddhist, we would tend to agree that going naked isn't really all that great benefit.

119
00:14:32,700 --> 00:14:38,820
And in fact, we're going to get fairly soon talking about people going naked and so on.

120
00:14:38,820 --> 00:14:40,660
Or I think actually we've had the verse, right?

121
00:14:40,660 --> 00:14:50,140
We've had the verse about the man who pretended to be an ascetic and it's something

122
00:14:50,140 --> 00:14:55,540
about month after month.

123
00:14:55,540 --> 00:15:06,300
But it is important that our livelihood and the other, the rest of our life, our activity

124
00:15:06,300 --> 00:15:13,220
in the world these sphere does, can do to meditation practice and development.

125
00:15:13,220 --> 00:15:21,100
And so this is a question that comes up, how do we interact with spiritual people?

126
00:15:21,100 --> 00:15:26,020
What do we do with our resources, where are our resources best spent and it's not just

127
00:15:26,020 --> 00:15:39,020
monetary resources, our energy, our intellect, our study, where are we best to engage ourselves?

128
00:15:39,020 --> 00:15:43,980
Not just in meditation, but in the rest of our life because this verse doesn't actually

129
00:15:43,980 --> 00:15:50,820
deal with meditation except in so far as it talks about the recipient as being

130
00:15:50,820 --> 00:16:00,820
really only a factor in regards to their own, that person's spiritual development, it's

131
00:16:00,820 --> 00:16:07,420
not just because they are a placeholder, like someone who becomes a priest and you give

132
00:16:07,420 --> 00:16:14,260
them because they are in a place or a person who becomes a monk and you give to them because

133
00:16:14,260 --> 00:16:18,740
they are a monk, okay, this person is a monk and you give to them, it's not actually

134
00:16:18,740 --> 00:16:24,540
considered to be very great fruit just because the person is a Buddhist monk.

135
00:16:24,540 --> 00:16:27,860
There is something there, you think this is a representative of the sangha, I'm giving

136
00:16:27,860 --> 00:16:34,620
it to the Buddhist religion, but there's none of the sense unless they are a developed person,

137
00:16:34,620 --> 00:16:41,940
someone who is cultivated meditation, someone who has done something worthy and therefore

138
00:16:41,940 --> 00:16:58,780
is a person whose actions and whose support, the energy and the power, the strength that

139
00:16:58,780 --> 00:17:04,220
comes from supporting such a person will go to good things for themselves and for others,

140
00:17:04,220 --> 00:17:12,380
will not go to hurt others or hurt themselves, will not be wasted.

141
00:17:12,380 --> 00:17:18,260
As meditators, as Buddhists, this is the sort of thing we have to think about and it's

142
00:17:18,260 --> 00:17:23,780
very clear and simple, maybe it's a little abbreviated to abbreviated because actually

143
00:17:23,780 --> 00:17:28,380
there's more to it than that, you can't just give to an enlightened being and hope that

144
00:17:28,380 --> 00:17:33,020
it's going to bring good benefit to yourself, have to be mindful of yourself, have to

145
00:17:33,020 --> 00:17:38,780
be conscious of why you're giving and giving for the right reasons, you can't be giving

146
00:17:38,780 --> 00:17:43,380
to brag to others and so on, I mean you can but it's of less fruit and you can't just

147
00:17:43,380 --> 00:17:52,820
give them anything, giving them people often like to give money, which is what was talked

148
00:17:52,820 --> 00:18:00,700
about in this verse, money is actually when I was in Thailand, there's a big thing because

149
00:18:00,700 --> 00:18:05,100
most monks use money and so it's a sense that what people want to give you, why aren't

150
00:18:05,100 --> 00:18:08,220
you accepting money?

151
00:18:08,220 --> 00:18:13,340
They want to give and you're preventing them from doing this good deed and I said well

152
00:18:13,340 --> 00:18:20,220
you know giving money is actually a fairly poor gift to give because then you're saying

153
00:18:20,220 --> 00:18:25,700
the other person go and do it yourself and go and get it yourself as opposed to bringing

154
00:18:25,700 --> 00:18:30,860
the person something beneficial, something that they can actually use, you can't eat food,

155
00:18:30,860 --> 00:18:35,860
it's very difficult to wear food, you can't use it as medicine, it's actually quite

156
00:18:35,860 --> 00:18:42,660
useless, it's just paper and moreover it's a dangerous thing because you give money and

157
00:18:42,660 --> 00:18:47,460
you force the person to think about, even if their mind is pure, you force them to think

158
00:18:47,460 --> 00:18:52,180
about doing this, doing that, where am I going to go, what am I going to get?

159
00:18:52,180 --> 00:18:57,100
As opposed to bringing them food, thinking it out for them, it's a very much more powerful

160
00:18:57,100 --> 00:19:03,140
gift to give something that they will actually use, so some people have gone to the convenience

161
00:19:03,140 --> 00:19:10,220
of, at holidays just giving money right or give certificates or something but it's not

162
00:19:10,220 --> 00:19:14,260
the same as actually putting some thought into it and saying well this person could use

163
00:19:14,260 --> 00:19:21,900
that or even better asking them what they need is there anything and so on, but type

164
00:19:21,900 --> 00:19:27,140
of people have the sense that you can't ask a monk what they need anyway, but it's

165
00:19:27,140 --> 00:19:31,780
not just about giving to monks, I mean if you give to a person on the side of the road

166
00:19:31,780 --> 00:19:39,380
who's hungry, that's a great thing, give them a meal to eat or ask them if there's anything

167
00:19:39,380 --> 00:19:46,900
they need or you help them go to the doctors or so and help them find a job, all great

168
00:19:46,900 --> 00:19:51,860
things and very powerful things that you can do and if they come from your own heart then

169
00:19:51,860 --> 00:19:59,220
you actually purify the deed, it's not necessary to just give to people who are cultivated

170
00:19:59,220 --> 00:20:03,500
if you give a new yourself are cultivated then there's the benefit of you helping the

171
00:20:03,500 --> 00:20:08,860
world, that's considered to be a pure deed as well and then the other hand if you give

172
00:20:08,860 --> 00:20:14,260
the dhamma then the dhamma purifies it or if you give something very powerful or the gift

173
00:20:14,260 --> 00:20:22,140
is very powerful then the gift itself makes it a pure deed and makes it a powerful gift.

174
00:20:22,140 --> 00:20:29,820
So anyway, not a lot to say, except another thing that we might say about this is it points

175
00:20:29,820 --> 00:20:37,460
to the error of magnitude that people sometimes fall into, they think of it in terms

176
00:20:37,460 --> 00:20:42,660
of thousands right and that's part of what the Buddha is saying here, he's saying really

177
00:20:42,660 --> 00:20:48,860
if just for a moment you think a good thought towards someone who really deserves it and

178
00:20:48,860 --> 00:20:54,940
you say I pay respect to the Buddha or I pay respect to the light being then that one

179
00:20:54,940 --> 00:21:00,260
moment is actually more powerful than a hundred years of giving a thousand gold pieces

180
00:21:00,260 --> 00:21:05,940
of months which is a huge sum of money because people think that it's how much you

181
00:21:05,940 --> 00:21:11,980
give and it's actually certainly not and rich people often brag about how much charity

182
00:21:11,980 --> 00:21:17,660
they give, how much money they give but it's meaningless because it has nothing to do with

183
00:21:17,660 --> 00:21:18,660
your state of mind.

184
00:21:18,660 --> 00:21:24,300
It says nothing about what your state of mind is like and whether the gift is rightly

185
00:21:24,300 --> 00:21:30,980
given so if you give millions of dollars to fund war or if you give millions of dollars

186
00:21:30,980 --> 00:21:38,980
to fund wrong view for example people who put all this money into maybe certain religious

187
00:21:38,980 --> 00:21:54,180
teachings or religious traditions that are hateful or spiteful or misguided, it's actually

188
00:21:54,180 --> 00:21:58,380
like throwing away money or maybe even worse than throwing it away, you support the cause

189
00:21:58,380 --> 00:22:07,220
that is harmful to people for example because it's all about the mind.

190
00:22:07,220 --> 00:22:12,820
One moment of actually just practicing meditation yourself is a much, much greater thing

191
00:22:12,820 --> 00:22:17,820
to do but if you're going to give and giving is good then you should give with the pure

192
00:22:17,820 --> 00:22:22,980
mind yourself, you should give with this pure and you should give to those people who are

193
00:22:22,980 --> 00:22:30,340
engaged in pure things so another verse about giving that's the Dhamma Padha for tonight.

194
00:22:30,340 --> 00:22:42,500
Thank you all for tuning in and keep practicing.

195
00:22:42,500 --> 00:22:55,660
So right, perhaps that looks for a camera okay.

196
00:22:55,660 --> 00:23:02,060
Very bright Monday, actually it's a little hard to see you snap right.

197
00:23:02,060 --> 00:23:03,900
Hard to see me because it's too bright huh?

198
00:23:03,900 --> 00:23:09,300
Yeah, you're like funny because I turned off the other one.

199
00:23:09,300 --> 00:23:21,500
That's much more natural yeah there you go.

200
00:23:21,500 --> 00:23:36,700
Okay, so today I went and tried to get some posters put up, why was that an ordeal?

201
00:23:36,700 --> 00:23:43,740
Full color printouts, if I have them, I showed them already, I've got the printouts now

202
00:23:43,740 --> 00:23:48,380
and of course the printed one out and then there's actually the they have to put a sticker

203
00:23:48,380 --> 00:23:56,140
on it and then you have to photocopy color photocopy which actually ruins the color so

204
00:23:56,140 --> 00:24:00,860
they ended up turning out not so good but we put up posters or we had we're having posters

205
00:24:00,860 --> 00:24:07,420
put up for a campfire and for a weekly meditation group asking people what are you waiting

206
00:24:07,420 --> 00:24:08,420
for?

207
00:24:08,420 --> 00:24:13,420
Don't let the moment pass you by.

208
00:24:13,420 --> 00:24:25,100
So I'm going to echo if I'm doing that right I have to, I don't know is there an echo?

209
00:24:25,100 --> 00:24:29,820
That's probably not much because now the speakers are much further away.

210
00:24:29,820 --> 00:24:31,820
Yeah, I don't hear anything.

211
00:24:31,820 --> 00:24:39,620
Okay, maybe we can do without it except it should be fine though because you're now

212
00:24:39,620 --> 00:24:51,500
probably rooted through monitor of no output, oh, but the audio stream hasn't heard

213
00:24:51,500 --> 00:24:52,500
me at all.

214
00:24:52,500 --> 00:24:56,780
That's no good, why does it do that?

215
00:24:56,780 --> 00:25:09,220
Test, yeah, okay, there's probably 20 minutes of silence on the audio stream.

216
00:25:09,220 --> 00:25:18,420
Anyway, yeah, that's right I have to go in and change it every time.

217
00:25:18,420 --> 00:25:25,020
What's my mic, what mic am I using, it's recording it after my place, okay.

218
00:25:25,020 --> 00:25:28,820
Technology, you wouldn't believe how much little, how many little things come together

219
00:25:28,820 --> 00:25:39,900
to make of this happen, it's more than I care to think about.

220
00:25:39,900 --> 00:25:45,580
If you want to make a little pre-flight checklist, I can check with you before you start

221
00:25:45,580 --> 00:25:47,780
the download pattern and make sure it's offset.

222
00:25:47,780 --> 00:25:50,500
Okay, that's a good idea.

223
00:25:50,500 --> 00:25:53,740
Way to think.

224
00:25:53,740 --> 00:26:01,580
Now there's too much, it's all convoluted, but okay, for pre-flight, what we have to check

225
00:26:01,580 --> 00:26:30,260
is to check whether I've set up pulse audio and whether the loop back devices are set up correctly.

226
00:26:30,260 --> 00:26:43,220
Whether I've turned on dark ice, I think those three things will do it.

227
00:26:43,220 --> 00:26:50,420
Okay, and there's much more that could go wrong, but knows that only for when you're

228
00:26:50,420 --> 00:26:55,500
doing dumapata videos within the hangout, or is that for all hangouts?

229
00:26:55,500 --> 00:26:57,060
That should be for all hangouts.

230
00:26:57,060 --> 00:27:08,100
Okay, for the dumapata, I mean I also have to check, but most of the other stuff is working.

231
00:27:08,100 --> 00:27:13,220
Most of the other stuff is working.

232
00:27:13,220 --> 00:27:14,940
So questions?

233
00:27:14,940 --> 00:27:16,340
Yes.

234
00:27:16,340 --> 00:27:19,980
Does anybody know if it matters what language I meditate in?

235
00:27:19,980 --> 00:27:29,580
If the ING suffix is the ING suffix important, for example, seeing versus IC?

236
00:27:29,580 --> 00:27:44,580
No, no, not important, in fact the Buddha said IC, but because in poly it's one word.

237
00:27:44,580 --> 00:27:49,540
And the question was about noting specifically, so it doesn't matter which language a person

238
00:27:49,540 --> 00:27:52,300
notes in.

239
00:27:52,300 --> 00:28:01,380
No, certainly it doesn't, hopefully it's a language you understand, you know and understand.

240
00:28:01,380 --> 00:28:09,740
It is, it's a tool that because we use language to represent things, so it's a way for

241
00:28:09,740 --> 00:28:16,660
the human mind to represent an idea and to therefore be directed towards it.

242
00:28:16,660 --> 00:28:22,020
Like if I shout fire, everyone's immediately thinking of fire and how to, like if there's

243
00:28:22,020 --> 00:28:25,220
a fire and someone else's fire, why did they do that?

244
00:28:25,220 --> 00:28:28,940
They shout fire, so people will know that there's a fire and we'll direct their attention

245
00:28:28,940 --> 00:28:35,140
accordingly.

246
00:28:35,140 --> 00:28:45,300
To alert our minds to the reality, to bring our minds, create alertness.

247
00:28:45,300 --> 00:28:52,180
Here in Toronto are you giving a talk, this was actually from last night.

248
00:28:52,180 --> 00:29:00,980
I'm not, I'm going to Toronto next Saturday for lunch, I've been invited for lunch, pretty

249
00:29:00,980 --> 00:29:08,940
sure I'm not giving a talk, but you never know if I do doubt it.

250
00:29:08,940 --> 00:29:13,420
Fun thing, I've noticed that you have an exceptional memory, minus really terrible.

251
00:29:13,420 --> 00:29:16,180
Would you give some tips on improving my memory?

252
00:29:16,180 --> 00:29:19,900
I mean, I haven't done much to improve it.

253
00:29:19,900 --> 00:29:22,540
I think I was born with a pretty good memory.

254
00:29:22,540 --> 00:29:31,420
I remember when I was, when I was in high school, they were showing reruns of Monty Python's

255
00:29:31,420 --> 00:29:40,740
flying circus and there was this one show where they all, this is really not Buddhism,

256
00:29:40,740 --> 00:29:46,580
I'm going to say, well memory, it does say something.

257
00:29:46,580 --> 00:29:51,860
Each one, by the end of the show, each one of them, so it starts off with this sketch

258
00:29:51,860 --> 00:29:57,620
where they have this name, that's just obscenely long, and by the end of the show each of

259
00:29:57,620 --> 00:30:02,500
the guys had said the name and it just, wow, it made me think, wow, these guys have

260
00:30:02,500 --> 00:30:07,780
such good memory, and so I went and I think this was back when the internet was really,

261
00:30:07,780 --> 00:30:14,340
really new, but somehow I found a website that was dedicated to Monty Python and managed

262
00:30:14,340 --> 00:30:23,340
to get a transcription of it, and I spent about a week, along with it on a piece of paper,

263
00:30:23,340 --> 00:30:30,060
memorizing it, and the interesting thing was that after that week, I couldn't get it out

264
00:30:30,060 --> 00:30:31,060
in my head.

265
00:30:31,060 --> 00:30:36,500
This is how habits are formed, and this shows nonselfily, I would be sitting there and

266
00:30:36,500 --> 00:30:45,380
wow, it starts to play itself back in my head, because I had become obsessive about it.

267
00:30:45,380 --> 00:30:51,500
The name was Johann Gamble-Patty, D. Von Ashford, Splendin Spatter, Kraskren, Von Fry,

268
00:30:51,500 --> 00:30:56,540
D. G. Dingle, Dingle, Dingle, Dingle, Dingle, Bernstein, Von Nackathrasha, Apo-Wingahore,

269
00:30:56,540 --> 00:30:59,480
with Ticlins and Grando-Nothin, Grando-Nothin, Grando-Nothin, Grando-Nothin, Grando-Nothin,

270
00:30:59,480 --> 00:31:21,460
there is a point though that is actually quite interesting for us, and the fact that.

271
00:31:21,460 --> 00:31:26,180
It's just an organic thing. I mean, memory isn't something to be proud of or be happy about

272
00:31:26,180 --> 00:31:33,540
that even necessarily. It was my day because you know what? There was a phone call and I just unplugged

273
00:31:33,540 --> 00:31:38,660
the phone to fix it.

274
00:31:45,060 --> 00:31:48,660
I better plug the phone back in. Let's see who called.

275
00:31:48,660 --> 00:31:57,540
They're not going to be all that happy I suppose. Richard Evans. I wonder who Richard. Does anyone

276
00:31:57,540 --> 00:32:03,140
know who Richard Evans? You're on the air.

277
00:32:08,820 --> 00:32:11,780
Let it ring once and then if they want to come back, they can come back.

278
00:32:11,780 --> 00:32:15,780
That's how you do it, right?

279
00:32:16,980 --> 00:32:23,780
Entire they call it, Miss Call. But it becomes a verb. Miss Call me, they say.

280
00:32:25,620 --> 00:32:29,380
So what? Miss Call me? Let it ring once and then they'll call you back.

281
00:32:29,380 --> 00:32:36,420
Yeah. Yeah. Because in Asia, you only pay if you're calling.

282
00:32:36,420 --> 00:32:42,980
You don't lose minutes if you're being called. So it's a big thing to call someone and just

283
00:32:42,980 --> 00:32:47,300
let it ring once so that you don't have to pay and they pay.

284
00:32:50,660 --> 00:32:55,620
Or I mean, it's just the way actually it's more used as a way to give someone your number.

285
00:32:55,620 --> 00:33:08,020
What's your number? Oh, just Miss Call me. More questions?

286
00:33:09,620 --> 00:33:14,500
Not sure. Right. We're talking about memory anyway.

287
00:33:19,700 --> 00:33:24,180
There are ways. I mean, the point is it's not really a Buddhist thing.

288
00:33:24,180 --> 00:33:28,260
No memory is something you can build is something you can work at.

289
00:33:29,620 --> 00:33:34,580
To some extent, I suppose you could argue on the other hand that to some extent it's organic.

290
00:33:36,100 --> 00:33:39,620
But nonetheless, it's not something that I would teach. It's not something that I would

291
00:33:40,340 --> 00:33:42,180
encourage you to focus your efforts on.

292
00:33:44,660 --> 00:33:48,100
If you don't have a good memory, don't work in a job that requires you to have a good memory.

293
00:33:48,100 --> 00:33:56,740
That's all. Find a job that allows you to build habits and work based on your skills.

294
00:34:00,100 --> 00:34:04,980
No, there was this monk who couldn't remember a single stanza and he still became an hour hunt.

295
00:34:09,540 --> 00:34:11,860
Was that the monk that was given a claw?

296
00:34:11,860 --> 00:34:16,660
That was him.

297
00:34:18,500 --> 00:34:22,260
Dante, what is the next step after the basic meditation you teach?

298
00:34:22,260 --> 00:34:24,820
Is something added to walking and sitting meditation?

299
00:34:25,860 --> 00:34:30,260
Yeah, that's why we started doing these courses. There's a lot that's added

300
00:34:31,940 --> 00:34:33,300
to both walking and sitting.

301
00:34:33,300 --> 00:34:44,020
I've started doing that with people, but it's based on our weekly meetings. Every week I'll

302
00:34:44,020 --> 00:34:50,020
meet with people and give them extra meditation exercises.

303
00:34:56,260 --> 00:35:00,340
Dante, you made a mention of habit being associated with non-self.

304
00:35:00,340 --> 00:35:03,620
Could you please elaborate?

305
00:35:08,820 --> 00:35:09,700
It's a funny question.

306
00:35:13,140 --> 00:35:18,660
It's like points to a thirst to understand non-self, which is common.

307
00:35:19,380 --> 00:35:22,980
People, whenever they hear the word non-self, they want to know, what does that mean? How can

308
00:35:22,980 --> 00:35:28,020
that help me understand non-self? You can't really understand non-self unless you practice,

309
00:35:28,020 --> 00:35:32,580
and you shouldn't. It's not something that we over intellectualize and therefore get all confused

310
00:35:32,580 --> 00:35:39,380
about. It's quite simple. Habits are constructs. When they're built, you can't just turn them

311
00:35:39,380 --> 00:35:46,900
off, but therefore they're non-self. It's not a hard concept really. But deeper, I think you have

312
00:35:46,900 --> 00:35:52,580
to sort of let go of attention. I don't know who this is or what they're coming from, but this kind of

313
00:35:52,580 --> 00:36:02,020
question. It points to something, the idea of our confusion about non-self.

314
00:36:05,860 --> 00:36:11,940
Because we have the strong idea of self, and so we think in terms of that, we think in terms of our

315
00:36:11,940 --> 00:36:19,300
view of self, but it's not like that. It's just plain and simple facts and realities. Habits form,

316
00:36:19,300 --> 00:36:27,940
and you can't just turn them on and suddenly have the habit, but through the repeated behavior,

317
00:36:28,580 --> 00:36:38,820
the habit forms, and it's only a repeated change in behavior that will change the habit.

318
00:36:40,340 --> 00:36:43,140
So they're not self in the sense that they can be turned on and off.

319
00:36:43,140 --> 00:36:47,060
There's no control over them.

320
00:37:02,980 --> 00:37:04,740
I think we're all caught up on questions.

321
00:37:04,740 --> 00:37:12,180
Okay. Have a good night. Thank you all for coming. Thank you Robin for your help.

322
00:37:12,180 --> 00:37:42,020
Thank you one day. Good night.

