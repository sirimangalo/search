1
00:00:00,000 --> 00:00:03,920
Hello and welcome back to our study of the Dhamapada.

2
00:00:03,920 --> 00:00:11,120
Tonight we continue on with 1st number AD, which reigns as follows.

3
00:00:33,920 --> 00:00:46,800
Uda kang is water, uda kang hi, na yang ti, they lead, netika, the people who lead water,

4
00:00:46,800 --> 00:00:53,880
the leaders or the irrigators is often translated.

5
00:00:53,880 --> 00:01:03,880
Direct the water, irrigators direct water, Uso kara, the arrow makers or fletchers, nama yan

6
00:01:03,880 --> 00:01:17,120
ti, tei janang, straighten their shafts, the shaft to the arrow, da rung nama yan ti,

7
00:01:17,120 --> 00:01:25,520
da jhaka carpenters, straighten wood or carve wood, lead, guide wood.

8
00:01:25,520 --> 00:01:30,320
The word here is nama yan ti means to guide or to lead, but the mind is they finesse it

9
00:01:30,320 --> 00:01:32,800
or they work with it.

10
00:01:32,800 --> 00:01:44,040
Adhanang, da miyand ti, banditah, but the wives train their minds.

11
00:01:44,040 --> 00:01:51,120
So we have some fairly powerful imagery here, and we have one of the more heartwarming

12
00:01:51,120 --> 00:01:59,040
stories of the Dhamapada, and again it's a back story, so it has to do with past lives

13
00:01:59,040 --> 00:02:05,560
and so on, and there's some that's potentially an exaggeration and there's a lot that many

14
00:02:05,560 --> 00:02:11,800
people will find hard to swallow, but I'll streamline it anyway, so it shouldn't be that

15
00:02:11,800 --> 00:02:12,800
big of a deal.

16
00:02:12,800 --> 00:02:17,120
Remember, it's just a story and whether or not any of it actually happened, it makes

17
00:02:17,120 --> 00:02:22,160
a good story and has a good moral to it.

18
00:02:22,160 --> 00:02:29,800
First the moral has a lot to do with giving which a cynic might say is convenient for monks

19
00:02:29,800 --> 00:02:36,400
to talk about giving to Buddhist monasteries and giving to Buddhism, and I understand

20
00:02:36,400 --> 00:02:44,400
that, I mean, I wouldn't say it if I didn't, but it still stands regardless of how important

21
00:02:44,400 --> 00:02:53,080
I think giving is, giving is good, so this has to do with giving, but it has more to do with

22
00:02:53,080 --> 00:02:55,000
one's good intentions.

23
00:02:55,000 --> 00:03:06,440
The story goes, there was this in the time of Kasupabuddha, there was a town or a kingdom,

24
00:03:06,440 --> 00:03:20,560
what was it, it was in Beneris, Varanasi, and the Buddha was dwelling there, Kasupabuddha

25
00:03:20,560 --> 00:03:28,280
was dwelling there, and so people would invite monks as they saw fit, so they might invite

26
00:03:28,280 --> 00:03:33,960
eight monks, this family might invite ten monks, this family might invite one monk, but

27
00:03:33,960 --> 00:03:40,720
they would invite their own family would invite a monk to come and eat at their house,

28
00:03:40,720 --> 00:03:46,520
and there was some kind of system set up for that, but then one day when they were listening

29
00:03:46,520 --> 00:03:56,040
to the Dhamma, one man who was sort of a wise man, was listening to the Dhamma and the

30
00:03:56,040 --> 00:04:03,840
Buddha mentioned the different results of giving, if you give, but you don't urge others

31
00:04:03,840 --> 00:04:15,200
to give, then the result is that you will receive in the future opulence, but you won't

32
00:04:15,200 --> 00:04:25,640
have a retinue, you won't have friends to share it with, whereas if you urge others to

33
00:04:25,640 --> 00:04:30,200
give, but you don't give yourself, then you'll have a lot of friends, but you won't

34
00:04:30,200 --> 00:04:37,720
have much wealth yourself or opulence, your affluence yourself, but if on the other hand,

35
00:04:37,720 --> 00:04:42,760
you don't give and you don't urge others to give, then you'll have neither, but if you

36
00:04:42,760 --> 00:04:49,320
give and you urge others to give, then you can generalize this and say, if you do good things,

37
00:04:49,320 --> 00:04:56,040
help others, are kind, are moral, this kind of thing, and you urge others in the same,

38
00:04:56,040 --> 00:05:04,880
then you'll both receive good karma and have friends to share it with you.

39
00:05:04,880 --> 00:05:09,160
And this man listening to this, he thought to himself, I'm going to get both of those

40
00:05:09,160 --> 00:05:11,360
for myself.

41
00:05:11,360 --> 00:05:14,480
And so he went to the Buddha afterwards and he asked, how many monks are there living

42
00:05:14,480 --> 00:05:15,480
with you?

43
00:05:15,480 --> 00:05:20,480
I think the story says there were 20,000, but let's just say it was a lot, a lot of monks.

44
00:05:20,480 --> 00:05:32,080
I mean, hey in Thailand now, there's got to be 100,000 or a couple hundred thousand.

45
00:05:32,080 --> 00:05:35,360
But in one city, there were a lot of monks.

46
00:05:35,360 --> 00:05:42,800
And so he said, I would like to invite them all for a meal tomorrow.

47
00:05:42,800 --> 00:05:49,520
And so the Buddha accepted by staying silent, and this man went back to the town and he

48
00:05:49,520 --> 00:05:54,240
told everyone, he said, I've invited all the monks for lunch, and I want all of you to

49
00:05:54,240 --> 00:05:55,840
help me.

50
00:05:55,840 --> 00:05:58,640
And so he wrote down on a piece of paper.

51
00:05:58,640 --> 00:06:01,640
He came up to people and he said, you know, I had people come to him and tell him how

52
00:06:01,640 --> 00:06:04,760
many monks they were going to support.

53
00:06:04,760 --> 00:06:06,080
And he arranged it all.

54
00:06:06,080 --> 00:06:11,520
So the idea was that it would be a single arms giving as a group, so that they could

55
00:06:11,520 --> 00:06:18,520
all share in the goodness of their deed together, as opposed to each house doing it on

56
00:06:18,520 --> 00:06:21,160
their own.

57
00:06:21,160 --> 00:06:25,880
And so he went around to everybody, and some people would take many, some people would

58
00:06:25,880 --> 00:06:27,560
take a few.

59
00:06:27,560 --> 00:06:32,960
Some people would say 20, some people would say 100, some people would say 500.

60
00:06:32,960 --> 00:06:42,560
And he went up eventually to this poor man, who his name was Mahadukata, Mahadukata means

61
00:06:42,560 --> 00:06:49,240
one who is greatly at disadvantage or in a really bad place.

62
00:06:49,240 --> 00:06:56,760
Dukata means one who has gone to a bad, to suffering is in hardship.

63
00:06:56,760 --> 00:06:57,760
So in great hardship.

64
00:06:57,760 --> 00:07:00,680
So his name literally means one who is in great hardship.

65
00:07:00,680 --> 00:07:04,200
So whether it was his name or just a description of him, but that's how he is referring

66
00:07:04,200 --> 00:07:06,640
to his Mahadukata.

67
00:07:06,640 --> 00:07:12,800
He came up to him and he said, would you like to help, and he said, how can I get involved?

68
00:07:12,800 --> 00:07:14,120
I have no money.

69
00:07:14,120 --> 00:07:18,280
It's very hard for me to even make enough money to live by.

70
00:07:18,280 --> 00:07:24,520
And the man said, well, don't you think looking at these people who live in affluence and

71
00:07:24,520 --> 00:07:29,680
have everything that they want, while doing nothing to deserve it, don't you think that

72
00:07:29,680 --> 00:07:34,560
that, well, you work hard, so they don't have to work at all, and you have to work hard

73
00:07:34,560 --> 00:07:35,560
and don't get anything.

74
00:07:35,560 --> 00:07:39,880
So I think there's a result, this is a result from the past, because they had heard all

75
00:07:39,880 --> 00:07:46,920
about karma and how it affects you in the next life, and so they said, you're probably

76
00:07:46,920 --> 00:07:52,880
right, you know, there's probably a cause from the past for this.

77
00:07:52,880 --> 00:07:56,880
And the man said, well, then, you know, find some way, don't you think you should find

78
00:07:56,880 --> 00:08:01,800
some way to do good deeds, he said to you, sure, you can surely you can find some way

79
00:08:01,800 --> 00:08:08,240
to do, to work and to make enough money to support one monk.

80
00:08:08,240 --> 00:08:14,720
And the man was moved with a sort of sense of religious urgency, and he said, put me down

81
00:08:14,720 --> 00:08:16,680
for one monk.

82
00:08:16,680 --> 00:08:24,680
I don't, you know, come hell or high water, I'll find a way to support one monk.

83
00:08:24,680 --> 00:08:29,160
But this man who is supposed to have been a wise man made a mistake.

84
00:08:29,160 --> 00:08:35,240
He didn't write down the name, he didn't write down the man's name on the paper, thinking

85
00:08:35,240 --> 00:08:40,720
what is one monk, surely there will be, I guess it was like, surely there will be a monk

86
00:08:40,720 --> 00:08:45,560
left over, I don't need to write that down, I don't need to reserve him one monk.

87
00:08:45,560 --> 00:08:49,920
We'll see about that.

88
00:08:49,920 --> 00:08:56,400
Mahadu Gattaghu's home to his wife and tells her, and being a wise woman herself, she

89
00:08:56,400 --> 00:08:59,120
doesn't reprove him by saying, how could you do that?

90
00:08:59,120 --> 00:09:05,000
When we have nothing to give, what were you thinking, she says, we'll find a way.

91
00:09:05,000 --> 00:09:15,720
And so he goes out and finds some wealthy man who's actually working to provide for like

92
00:09:15,720 --> 00:09:23,120
a hundred monks or even 500 monks, the 300 monks, and he says, you know, is there any work

93
00:09:23,120 --> 00:09:24,120
you have for me?

94
00:09:24,120 --> 00:09:25,120
Well, what can you do?

95
00:09:25,120 --> 00:09:29,840
He says, whatever you want me to do, okay, then he says, split some wood.

96
00:09:29,840 --> 00:09:38,000
And this poor man goes at it with such vigor and such zest and what's such happiness

97
00:09:38,000 --> 00:09:44,680
that the master, the rich guy is really impressed and he asks, why are you working with

98
00:09:44,680 --> 00:09:47,320
such energy?

99
00:09:47,320 --> 00:09:54,600
And he says, with the fruit of my labor, I have the opportunity to provide a monk with

100
00:09:54,600 --> 00:10:05,240
food for a day, and the rich guy is impressed by this and moved by it, right?

101
00:10:05,240 --> 00:10:16,680
Because it's moving, that someone would really think to work so hard to do a good deed

102
00:10:16,680 --> 00:10:34,040
for another person, and so he remarks himself on how special this is, and when the poor

103
00:10:34,040 --> 00:10:40,680
man is finished his labor, instead of giving him his ordinary wage, he gives him a portion

104
00:10:40,680 --> 00:10:45,560
of rice, he gives him twice as much as he normally would.

105
00:10:45,560 --> 00:10:47,880
Meanwhile his wife goes and does the same thing.

106
00:10:47,880 --> 00:10:56,720
She goes and goes to find work with the merchants wife actually, and the same thing happens.

107
00:10:56,720 --> 00:11:00,760
She works with such vigor that the woman gives her twice her pay, and at this time she

108
00:11:00,760 --> 00:11:07,240
gives her some money, and on top of that she gives her some other things as well.

109
00:11:07,240 --> 00:11:13,000
And they're so happy, because now they have enough rice to give a simple meal to this

110
00:11:13,000 --> 00:11:16,960
one monk that's going to come and visit them.

111
00:11:16,960 --> 00:11:22,960
They get back home, but then they realize they don't have any herbs or vegetables to

112
00:11:22,960 --> 00:11:29,600
go with it, any kind of greens to go with it, and what are we going to do?

113
00:11:29,600 --> 00:11:41,720
And the wife says, go down to the river and pick some weeds, pick some wild greens, it's

114
00:11:41,720 --> 00:11:45,960
not going to be great fair, but what can we do this is all we've got.

115
00:11:45,960 --> 00:11:54,400
And so the man goes down and he finds, he finds these green leaves that are sort of a

116
00:11:54,400 --> 00:11:59,640
coarse fair that no one else would bother picking, but he's so happy, because he has

117
00:11:59,640 --> 00:12:01,960
this opportunity to give.

118
00:12:01,960 --> 00:12:08,080
And there just happens to be a fisherman fishing in the river.

119
00:12:08,080 --> 00:12:10,040
And he sees him and he says, why are you so happy?

120
00:12:10,040 --> 00:12:13,160
And he said, I'm giving food to a monk.

121
00:12:13,160 --> 00:12:19,280
And he said, with those leaves, boy, you're really going to spoil him or something like

122
00:12:19,280 --> 00:12:20,280
that.

123
00:12:20,280 --> 00:12:21,360
And he says, well, what can I do?

124
00:12:21,360 --> 00:12:26,680
I says, all I've got, I don't have any money to buy anything else to put.

125
00:12:26,680 --> 00:12:33,400
So basically all I had was rice and some milk or some ghee, some butter, but they had

126
00:12:33,400 --> 00:12:35,600
no vegetables, no meat or anything.

127
00:12:35,600 --> 00:12:40,640
So this was what he was going to put with the rice, was just some weeds, basically.

128
00:12:40,640 --> 00:12:42,960
They said, what can I do?

129
00:12:42,960 --> 00:12:44,960
I'm not a rich man.

130
00:12:44,960 --> 00:12:48,480
And the fisherman says, well, very well, then you take these fish that I've caught and

131
00:12:48,480 --> 00:12:55,760
string them up in bunches, meaning work for me and I'll see I'll hop you out.

132
00:12:55,760 --> 00:13:01,520
And so he works for him and by the time they're finished telling all the fish, there's

133
00:13:01,520 --> 00:13:05,880
none left and the man says, are there any fish left?

134
00:13:05,880 --> 00:13:09,680
And so he says, okay, well, these are the fish that I had left over and they are some

135
00:13:09,680 --> 00:13:20,560
kind of red-scaled fish or something, red fish, and he gives them to these four fish.

136
00:13:20,560 --> 00:13:25,120
And so now not only does he have greens, but he has fish to offer to this monk.

137
00:13:25,120 --> 00:13:33,120
I mean, at this point, something has to be said about the, you have to remark on how

138
00:13:33,120 --> 00:13:37,320
goodness attracts goodness.

139
00:13:37,320 --> 00:13:38,960
This is a part of how karma works.

140
00:13:38,960 --> 00:13:42,760
You know, when you're a good person doing good things, people want to help you want to

141
00:13:42,760 --> 00:13:43,760
get involved.

142
00:13:43,760 --> 00:13:48,800
Anyway, we'll talk more about that after.

143
00:13:48,800 --> 00:13:56,720
So he goes back with the fish and they begin to prepare their food.

144
00:13:56,720 --> 00:14:04,840
They get back to the house and meanwhile, a couple of things have been happening.

145
00:14:04,840 --> 00:14:10,200
First of all, the Buddha has noticed and the Buddha has noticed that this guy has not been

146
00:14:10,200 --> 00:14:13,400
written down on a leaf on the leaf.

147
00:14:13,400 --> 00:14:15,040
He hasn't been written down on the list.

148
00:14:15,040 --> 00:14:17,360
His name isn't on the list.

149
00:14:17,360 --> 00:14:23,360
The second thing that happens is Sakha, the king of the God, the king of the angels in

150
00:14:23,360 --> 00:14:30,960
the heaven of the 33, his throne begins to heat up.

151
00:14:30,960 --> 00:14:36,600
And his throne heats up, it's apparently a thing that his throne heats up whenever anyone

152
00:14:36,600 --> 00:14:44,000
is doing something so good that they might have the potential to overthrow him, to rise

153
00:14:44,000 --> 00:14:50,400
up to a state better than him and thus become the leader of the angels of the 33.

154
00:14:50,400 --> 00:14:51,400
It's something like that.

155
00:14:51,400 --> 00:14:53,200
Anyway, it gets warm.

156
00:14:53,200 --> 00:14:57,400
So it's not hot yet, but it's quite warm because Sakha himself did great things to be

157
00:14:57,400 --> 00:15:00,040
born as Sakha.

158
00:15:00,040 --> 00:15:04,200
So it's not like he's going to be overthrown by any simple dean, but it gets warmed

159
00:15:04,200 --> 00:15:05,560
on the lesson.

160
00:15:05,560 --> 00:15:13,480
So he looks to see who is it that has been doing this great dean and he finds out.

161
00:15:13,480 --> 00:15:18,880
And he says, this is going to be an amazing thing that's going to happen today.

162
00:15:18,880 --> 00:15:26,000
And so he says, I better get involved, I better go down and help this guy out.

163
00:15:26,000 --> 00:15:33,200
And so he goes down and he disguises himself as an ordinary person and he walks up near

164
00:15:33,200 --> 00:15:39,600
this hut where this poor man and his wife live, this poor couple live.

165
00:15:39,600 --> 00:15:44,640
And he says, is anyone looking for someone to, is anyone looking to hire a cook or something

166
00:15:44,640 --> 00:15:47,080
like that?

167
00:15:47,080 --> 00:15:53,520
Or is there anything, are you looking for someone to work for you and they say, well,

168
00:15:53,520 --> 00:15:54,520
what can you do?

169
00:15:54,520 --> 00:15:56,280
They come out and they say, what is this?

170
00:15:56,280 --> 00:15:57,560
What can you do to help us?

171
00:15:57,560 --> 00:15:58,560
I can cook.

172
00:15:58,560 --> 00:16:00,640
And he says, well, we have no money to pay.

173
00:16:00,640 --> 00:16:06,640
And he says, well, that's why I understand you intend to provide food for a monk.

174
00:16:06,640 --> 00:16:15,400
Well, just let me share in the merit and that will be my pay and indeed it will be.

175
00:16:15,400 --> 00:16:21,400
And so he says, look, I'll start cooking here, you'll go and fetch the monk.

176
00:16:21,400 --> 00:16:26,360
And so this man has overjoyed all the help that he's gotten, which is pretty neat when

177
00:16:26,360 --> 00:16:31,280
you have good intentions and then people help you out with it.

178
00:16:31,280 --> 00:16:36,360
So he goes to the monastery and sure enough, he goes up to the man who's arranging

179
00:16:36,360 --> 00:16:40,400
sending monks off and all the monks have gone off by this time.

180
00:16:40,400 --> 00:16:46,840
And he says, okay, give me my one monk, tell me which monk it is.

181
00:16:46,840 --> 00:16:57,120
And the man freezes and he says, I'm sorry, I didn't write you down and there is no

182
00:16:57,120 --> 00:17:08,720
monk for you, and this guy, it says he felt as though a sharp dagger had been thrust

183
00:17:08,720 --> 00:17:09,720
in his belly.

184
00:17:09,720 --> 00:17:16,880
Can you imagine just being so worked up about this and so pushing yourself so hard,

185
00:17:16,880 --> 00:17:22,320
I mean, it's not an easy thing to do, most of us wouldn't think to put ourselves out

186
00:17:22,320 --> 00:17:25,600
so much just to do an ordinary deed of goodness.

187
00:17:25,600 --> 00:17:32,200
But this was something significant for him, he'd never had the thought even to do, to

188
00:17:32,200 --> 00:17:38,360
give charity, I mean being a poor person, why would he have that thought, right?

189
00:17:38,360 --> 00:17:45,840
And so how hard it was for him to come to just swallow this and undertake it, and then

190
00:17:45,840 --> 00:17:51,440
the work that he had put in and the expectation and the joy that had come with it only

191
00:17:51,440 --> 00:17:57,920
to be denied and it's true, there were no monks left and he starts crying and the people

192
00:17:57,920 --> 00:18:03,600
who were gathered in the monastery, they came and asked what's going on and he told them

193
00:18:03,600 --> 00:18:09,720
and oh, they turned on this guy and they said, how could you, how could you, how could

194
00:18:09,720 --> 00:18:13,520
you, because he had encouraged this guy, he had been the one to instigate the whole thing

195
00:18:13,520 --> 00:18:23,040
and then to not even save him a monk, how terrible and they were ready to lynch him and

196
00:18:23,040 --> 00:18:29,360
so he turns to the guy and he thinks quick and he was quite smart, it turns out he did

197
00:18:29,360 --> 00:18:35,920
have quite a bit of wisdom, just made a pretty silly mistake and he says to him, please

198
00:18:35,920 --> 00:18:42,960
don't ruin me, I'm going to be ruined if we don't solve this, he says, all the monks

199
00:18:42,960 --> 00:18:48,400
are gone, I don't even have a monk for my home, there's no monks left for me to feed even,

200
00:18:48,400 --> 00:18:54,960
otherwise I'd give you the monk in my home for sure, but I can't take a monk from someone

201
00:18:54,960 --> 00:19:04,720
else who I've promised them this, he says, but there's one thing you could do, take your

202
00:19:04,720 --> 00:19:13,040
ball, no, not take your ball, it doesn't have a ball, go to the book, go, the Buddha right now is sitting in his

203
00:19:13,040 --> 00:19:27,120
kutti, surrounding the kutti are kings, princes, rich men and people of all, stationed in

204
00:19:27,120 --> 00:19:32,560
society, have high station in society, waiting for the Buddha to come out and give one of, give his

205
00:19:32,560 --> 00:19:47,360
ball to one of them, but Buddha's, Buddha's are wise, so inestimably wise and often show favor

206
00:19:47,360 --> 00:19:55,840
to those with the greatest intention and with the greatest need, so if you have merit he will

207
00:19:55,840 --> 00:20:10,640
bestow this favor on you, so the man goes up, walks up to the hut, walks up to walks to the

208
00:20:10,640 --> 00:20:16,240
monastery, he wasn't in the monastery, he goes up to the monastery, and the people in the

209
00:20:16,240 --> 00:20:23,040
monastery, the kings and the princes and the rich people say to him and said, look this isn't the

210
00:20:23,040 --> 00:20:28,800
time for a handout, because normally he would come to beg, so they say, what are you doing here,

211
00:20:28,800 --> 00:20:35,520
beg her, there's not the time to beg, you really said, I know, I know it's not time to

212
00:20:35,520 --> 00:20:41,360
time for me to get a meal, I'm here to pay respect to the teacher, and so he walked up to the

213
00:20:41,360 --> 00:20:49,120
kutti, tears in his eyes and bowed down on the porch of the kutti where the Buddha was living,

214
00:20:49,120 --> 00:20:57,600
paid respect with his head and said, Reverend sir, in this city there is no man poorer than I be

215
00:20:57,600 --> 00:21:10,960
my refuge, bestow favor upon me, and the Buddha opened the door, and ignoring the princes and the

216
00:21:10,960 --> 00:21:30,800
kings and the rich folk, gave his bowl to the poor man. All of the kings and the princes and the

217
00:21:30,800 --> 00:21:40,000
rich people immediately were shocked and scrambled to talk to this poor man and said, look,

218
00:21:40,000 --> 00:21:44,720
we'll buy it from you, what need do you have for arms giving, we'll give you money,

219
00:21:44,720 --> 00:21:51,040
we'll give you 1000 gold coins, 10,000 gold coins, 100,000 gold coins, and he said I don't need

220
00:21:51,040 --> 00:21:58,640
money, all I need is to give food to this one monk, and so he led the Buddha to his house,

221
00:21:58,640 --> 00:22:06,720
but the king of the city followed after thinking, there's no way, there's no way this guy's

222
00:22:06,720 --> 00:22:12,640
going to have any food to give, so once the Buddha sees that there's not sufficient food to give,

223
00:22:14,240 --> 00:22:16,800
I'll take him back to the palace and feed him with proper food.

224
00:22:19,600 --> 00:22:25,040
Meanwhile, Saka, the king of the gods, the king of the angels is preparing

225
00:22:25,760 --> 00:22:30,880
angel food, and we all remember from Anurudad, didn't we have the story of Anurudad,

226
00:22:30,880 --> 00:22:38,000
where we know what angel food cake is like, angel food is like, so they get there and of course

227
00:22:38,000 --> 00:22:48,400
it's a sumptuous meal and the king departs and the Buddha eats and other magical things happen

228
00:22:48,400 --> 00:22:55,120
that I'm not going to relate, but through all of the goodness of this guy, this was the guy who

229
00:22:55,120 --> 00:23:03,760
was born when he passed away, he passed away and was born in heaven, yada, yada, and eventually

230
00:23:04,960 --> 00:23:15,840
was reborn as Bharnita, in the time of Arbuddha. So, it's a common theme in the Dhamapada to have

231
00:23:15,840 --> 00:23:21,920
these grandwah stories of goodness and from the past really has nothing to do with our verse

232
00:23:21,920 --> 00:23:26,240
to tell you the truth. It's just a nice story to tell. I don't know, it's a little bit,

233
00:23:26,800 --> 00:23:34,160
as I said, it's a little bit self-serving in a sense here I am talking about giving food to monks,

234
00:23:34,160 --> 00:23:40,000
but that's not really the point, it's the point is goodness, you know, and charity in general,

235
00:23:40,800 --> 00:23:46,880
and a person's intentions and how it leads to good things, but point is, with all that goodness

236
00:23:46,880 --> 00:23:52,720
and all of the good intentions in his mind, he was born as this young boy who at seven years old

237
00:23:52,720 --> 00:23:58,960
knew what he wanted. In fact, the story goes that when he was in his mother's womb, his mother

238
00:23:58,960 --> 00:24:05,040
had the urge to wear orange-colored robes and go and listen to the Buddha's teaching,

239
00:24:07,040 --> 00:24:14,400
and also to eat red fish, which is kind of funny because it red fish was what he fed to the Buddha,

240
00:24:14,400 --> 00:24:19,200
but she had this craving during her pregnancy to eat red fish. I wonder if that's

241
00:24:19,200 --> 00:24:22,320
they talk about pregnancy cravings, I wonder if there's anything to that.

242
00:24:27,760 --> 00:24:32,160
But okay, so when he was born, when he was seven years old, he asked his mother permission to

243
00:24:32,160 --> 00:24:39,600
become a novice. She gave him permission and he went forth, and on his first day, sorry, put the

244
00:24:39,600 --> 00:24:49,360
was his teacher, and sorry, put to lend him into the village in the late morning because he wanted

245
00:24:49,360 --> 00:24:54,160
to, sorry, put to generally went late because he wanted to check and make sure that the monastery

246
00:24:54,160 --> 00:25:00,640
was in order, and also because this novice was new, and so he wanted to take him separately,

247
00:25:00,640 --> 00:25:07,520
so he could make sure that he didn't, he behaved himself. But on the way, this is how the story

248
00:25:07,520 --> 00:25:14,160
goes and it's how it relates to the verse. On the way, he saw these things, he saw these men

249
00:25:14,160 --> 00:25:23,920
leading water irrigators directing water into the fields, and he saw how they could direct water,

250
00:25:26,000 --> 00:25:33,120
and then he saw these, the Fletcher's, straightening the shafts of wood, heating it up and

251
00:25:33,120 --> 00:25:38,640
straightening it to make arrow shafts, and he thought that these realized how they can, how they

252
00:25:38,640 --> 00:25:46,720
work to straighten things out, and finally he saw carpenters carving wood or shaping wood,

253
00:25:49,920 --> 00:25:51,120
and he thought to himself,

254
00:25:54,560 --> 00:26:00,880
these people do this with these inanimate objects. This is exactly how I should act towards the

255
00:26:00,880 --> 00:26:07,200
mind, seven years old somehow he knew this, and so he turned around, didn't even go for arms

256
00:26:07,200 --> 00:26:14,880
food. In fact, the story says he sent Sariputa on arms for him, which Sariputa did without thinking,

257
00:26:14,880 --> 00:26:24,800
Sariputa was the most humble person imaginable, it's quite amazing, and a good example for all of us.

258
00:26:24,800 --> 00:26:30,240
Here he is, the chief disciple of the Buddha, the number one guy, and the right hand man of the

259
00:26:30,240 --> 00:26:36,640
Buddha, and here's the seven-year-old novice saying, because the novice would carry Sariputa's ball

260
00:26:36,640 --> 00:26:44,240
for him, giving Sariputa back his ball and saying, get me some food as well. He told him to get

261
00:26:44,240 --> 00:26:55,120
him some redfish, and Sariputa did it without thinking, because he was completely humble,

262
00:26:55,120 --> 00:27:02,720
had no arrogance about him. Probably he would have admonished the novice if the novice had bad

263
00:27:02,720 --> 00:27:07,840
intentions, but he also could see that this novice had only the best of intentions, so the novice

264
00:27:07,840 --> 00:27:15,840
went back, and was in Sariputa's good tea and meditating, and the long story short, there's a

265
00:27:15,840 --> 00:27:22,640
little bit more, but long story short he became late, became in Iran, and so the Buddha gave

266
00:27:22,640 --> 00:27:32,880
this talk in that regard, and he gave this spoke this verse according to Pandita's thoughts on

267
00:27:32,880 --> 00:27:44,080
the subject. Indeed, irrigators direct water, flexures straighten their arrows, carpenters,

268
00:27:44,960 --> 00:27:52,320
straighten their wood, but we wise people train themselves. It's a nice verse, I mean,

269
00:27:52,320 --> 00:28:00,160
regardless of the story, it's really a useful one to describe the practice of insight meditation,

270
00:28:00,160 --> 00:28:05,440
straightening the mind, or the practice of goodness in general, because all types of goodness

271
00:28:05,440 --> 00:28:12,240
straighten the mind, right? We talk about the crookedness of the mind. We talk about someone who is

272
00:28:12,240 --> 00:28:20,080
crooked, right? We have this sense that the mind of a person who does evil deeds is not straight,

273
00:28:20,080 --> 00:28:25,520
it's crooked, and so we talk about goodness as straightening the mind, because a good person

274
00:28:25,520 --> 00:28:31,760
doesn't have as much complication to them, right? It's more complicated when you tell a lie,

275
00:28:31,760 --> 00:28:37,680
or when you live by lives, or when you live by deception, or when you live by manipulation.

276
00:28:40,720 --> 00:28:45,600
It's more complicated when you hurt others. It's more complicated the more you need,

277
00:28:45,600 --> 00:28:51,920
the more you're addicted to. It's complicated when you are stingy, it's complicated when you are

278
00:28:51,920 --> 00:28:58,160
jealous, it's complicated. It becomes all twisted and tired. It's complicated when you hold views

279
00:28:58,160 --> 00:29:04,000
and beliefs, right? This is the criticism we have of other religions is they have all these

280
00:29:04,000 --> 00:29:10,160
unnecessary beliefs that complicate things, that tie them up and not, trying to describe why this

281
00:29:10,160 --> 00:29:18,080
belief is necessary to go to heaven, how you can justify that belief that has no evidence to

282
00:29:18,080 --> 00:29:30,800
support it, and so on. Whereas goodness, pure goodness, straightens the mind. So this is what

283
00:29:30,800 --> 00:29:36,400
wise people do, they spend time straightening their mind. Another word is that the Buddha uses

284
00:29:36,400 --> 00:29:43,200
digital, straightening one's view, straightening the way one looks at the world. So another

285
00:29:43,200 --> 00:29:47,840
aspect of straightening is what we do in Vipassana to see things as they are, to not have

286
00:29:47,840 --> 00:29:54,320
misconceptions about reality, so to not see things that are impermanent as permanent, to not see

287
00:29:54,320 --> 00:30:00,320
things that are unsatisfying as satisfied, to not see things that are not self as self,

288
00:30:00,320 --> 00:30:10,880
so to not cling to things that are not not capable of satisfying, to learn to let go,

289
00:30:13,200 --> 00:30:20,080
and to flow with life, not get stuck on things.

290
00:30:20,080 --> 00:30:31,680
So that's the teaching, not much more you can say about it, except that's what we're doing and

291
00:30:31,680 --> 00:30:36,000
straightening our minds with all of the cultivation of goodness that we do.

292
00:30:39,280 --> 00:30:43,200
And so this is the goal that we have. When we think about Buddhism, the Buddha's teaching,

293
00:30:43,200 --> 00:30:52,800
we should really think in terms of this straightness, goodness, rightness, knowledge and wisdom

294
00:30:52,800 --> 00:31:02,960
and being in line with the truth of reality, because once you see things as they are, then there

295
00:31:02,960 --> 00:31:11,840
is no potential for crookedness, for evil. You see that it's a complication, you see that the

296
00:31:11,840 --> 00:31:15,440
evil and unwholesome things are just making life more difficult for you,

297
00:31:16,640 --> 00:31:23,520
just leading to more stress and suffering. And when you learn to let go and for yourself,

298
00:31:24,720 --> 00:31:27,920
you free yourself from suffering, untie the knots,

299
00:31:27,920 --> 00:31:38,800
untouge the taba, hejata, jata, jata, jata, jata. With the inner knots and the outer knots, this

300
00:31:38,800 --> 00:31:46,480
generation is entangled, up in knots, go among which at the age of dang, who indeed will

301
00:31:46,480 --> 00:31:55,440
free themselves from this tangle. So it's through the practice of morality, concentration and wisdom

302
00:31:55,440 --> 00:32:03,760
that we untangle the tangle and free ourselves from crookedness, training our minds, just like

303
00:32:04,480 --> 00:32:10,880
people train inanimate objects. If they can do it with inanimate things, why can't we do it with the

304
00:32:10,880 --> 00:32:19,120
mind? That was the analysis reasoning at seven years old. So it should put us all to some shame and

305
00:32:19,120 --> 00:32:25,520
make us think, why am I not having such profound thoughts? Why am I not engaging in the practice?

306
00:32:25,520 --> 00:32:33,280
That leads to straightness of mind. And we should undertake it in earnest. So that's the

307
00:32:33,280 --> 00:32:38,160
Dhammapada teaching for today. Thank you all for tuning in, wishing you all a great practice

308
00:32:38,160 --> 00:32:54,080
and progress on the path to peace, happiness and freedom from suffering. Thank you.

