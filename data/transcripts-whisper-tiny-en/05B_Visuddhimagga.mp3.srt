1
00:00:00,000 --> 00:00:02,600
After he has sat down, he has sat down.

2
00:00:02,600 --> 00:00:08,000
If his teacher opens, preceptor arrives, while the meal is still unfinished.

3
00:00:08,000 --> 00:00:14,000
So he sat down on the stage, and then say, he is eating.

4
00:00:14,000 --> 00:00:23,800
Then if a teacher or preceptor arrives or comes in, it is allowable for him to get up and do the duties.

5
00:00:23,800 --> 00:00:29,800
Actually, it is not only allowable, he should or he must get up and do the duties.

6
00:00:29,800 --> 00:00:36,800
Because we have duties to our teachers, to our preceptors.

7
00:00:36,800 --> 00:00:42,800
So if we are sitting and a preceptor or a teacher comes in, we must stand up and create him,

8
00:00:42,800 --> 00:00:46,800
and then do whatever we can for his comfort.

9
00:00:46,800 --> 00:00:57,800
So if a monk is sitting and eating, and at that time, his teacher or preceptor comes, what must he do?

10
00:00:57,800 --> 00:01:02,800
And he is one sitting eater.

11
00:01:02,800 --> 00:01:07,800
So he should get up and do the duty.

12
00:01:07,800 --> 00:01:14,800
And after getting up, he must not eat again.

13
00:01:14,800 --> 00:01:19,800
But the elder T. Villagerchula, as I said, he should either keep his seat and finish his meal,

14
00:01:19,800 --> 00:01:26,800
or if he gets up, he should leave the rest of his meal, and order not to break the acidic practice.

15
00:01:26,800 --> 00:01:29,800
So he could do either of the two.

16
00:01:29,800 --> 00:01:41,800
He should ignore his duty to his teachers and go on eating, or he should get up and do duties to his teachers.

17
00:01:41,800 --> 00:01:50,800
And for the meal, and this is one whose meal is still unfinished.

18
00:01:50,800 --> 00:01:56,800
Therefore, let him do the duties, but in that case, let him not eat for the rest of the meal.

19
00:01:56,800 --> 00:02:15,800
So this is what the elder said.

20
00:02:15,800 --> 00:02:27,800
As long as one wants to, there is no fixed duration of time.

21
00:02:27,800 --> 00:02:29,800
These are so usual.

22
00:02:29,800 --> 00:02:33,800
And now is very unusual.

23
00:02:33,800 --> 00:02:37,800
Right, monks do not practice this much now.

24
00:02:37,800 --> 00:02:49,800
Out of 13, the monks practice most is eating at one sitting and one bowl eating.

25
00:02:49,800 --> 00:02:55,800
We haven't come to one bowl eating.

26
00:02:55,800 --> 00:03:09,800
And sometimes, staying at a semi-semitary and sometimes under a tree, but not for long.

27
00:03:09,800 --> 00:03:18,800
You can practice two or three or four.

28
00:03:18,800 --> 00:03:20,800
We'll come to that later.

29
00:03:20,800 --> 00:03:27,800
It is said that if you can get the suitable place, open air place, and then close to a semi-semitary

30
00:03:27,800 --> 00:03:32,800
or any semi-semitary, then you can practice all 13 at the same time.

31
00:03:32,800 --> 00:03:44,800
And the elder Mahakasabha is said to be the one who practiced all 13, all 13 practices, all through his life.

32
00:03:44,800 --> 00:04:01,800
He is the foremost among those who undertake the ascetic practices.

33
00:04:01,800 --> 00:04:09,800
So one who is strict may not take anything more than the food that he has laid his hand on,

34
00:04:09,800 --> 00:04:12,800
whether it is little or much.

35
00:04:12,800 --> 00:04:17,800
And if people bring him deep, etc., thinking the elder has eaten nothing,

36
00:04:17,800 --> 00:04:22,800
while these are allowable for the purpose of medicine, they are not so for the purpose of food.

37
00:04:22,800 --> 00:04:25,800
As medicine, they are allowable, but not as food.

38
00:04:25,800 --> 00:04:31,800
The medium one may take more as long as the meal in the bowl is not exhausted.

39
00:04:31,800 --> 00:04:37,800
For he is called one who stops when the food is finished.

40
00:04:37,800 --> 00:04:42,800
The mild one may eat as long as he does not get out on his seat.

41
00:04:42,800 --> 00:04:48,800
He is either one who stops with the water because he eats until he takes water for washing the bowl,

42
00:04:48,800 --> 00:04:52,800
or one who stops with the session because he eats until he gets up.

43
00:04:52,800 --> 00:05:00,800
So there is a joke among monks that you can just say from the morning until noon,

44
00:05:00,800 --> 00:05:05,800
and eat as much as you like.

45
00:05:05,800 --> 00:05:12,800
If you do not break your position, so it is sitting position,

46
00:05:12,800 --> 00:05:17,800
then you can sit from morning till noon.

47
00:05:25,800 --> 00:05:32,800
And in the benefits, he has little affliction and little sickness because he eats less.

48
00:05:32,800 --> 00:05:36,800
He has lightness, strength, and a happy life.

49
00:05:36,800 --> 00:05:42,800
There is no contravening rules about food that is not what is left over from a meal.

50
00:05:42,800 --> 00:05:45,800
I'll talk about it later.

51
00:05:45,800 --> 00:05:47,800
Craving for taste is eliminated.

52
00:05:47,800 --> 00:05:51,800
His life conforms to the principles of fewness of wishes and so on.

53
00:05:51,800 --> 00:05:58,800
So this is one session either or one sitting either.

54
00:05:58,800 --> 00:06:03,800
The next one is one bowl eating.

55
00:06:03,800 --> 00:06:06,800
It's not so easy.

56
00:06:06,800 --> 00:06:11,800
You use only one bowl when you eat.

57
00:06:11,800 --> 00:06:18,800
So when at the time of drinking rice croo,

58
00:06:18,800 --> 00:06:22,800
the bowl food eater gets curry that is put in a dish.

59
00:06:22,800 --> 00:06:26,800
He can first either eat the curry or drink the rice croo.

60
00:06:26,800 --> 00:06:29,800
Not both at the same time.

61
00:06:29,800 --> 00:06:36,800
So drink rice croo and after finishing it, it dries the fish curry.

62
00:06:36,800 --> 00:06:42,800
If he puts it in the rice croo, the rice croo becomes repulsive when a curry made with...

63
00:06:42,800 --> 00:06:43,800
What's all?

64
00:06:43,800 --> 00:06:50,800
Cured fish, etc. is put into it.

65
00:06:50,800 --> 00:06:56,800
In Bama, we have what we call fish faced. It's very smelly.

66
00:06:56,800 --> 00:07:01,800
So it is allowable to do this only in order to use it without making it repulsive.

67
00:07:01,800 --> 00:07:05,800
Consequently, this is said with reference to such curry as that.

68
00:07:05,800 --> 00:07:10,800
But what is unrepulsive, such as honey, sugar, etc. should be put into it.

69
00:07:10,800 --> 00:07:14,800
And in taking it, he should take the right amount.

70
00:07:14,800 --> 00:07:20,800
It is allowable to take green vegetables with the hand, with one hand and eat them.

71
00:07:20,800 --> 00:07:23,800
But he should not do so.

72
00:07:23,800 --> 00:07:25,800
Not unless he does.

73
00:07:25,800 --> 00:07:27,800
But he should not do so.

74
00:07:27,800 --> 00:07:32,800
But he should put the vegetable into the bowl.

75
00:07:32,800 --> 00:07:37,800
So unless he does not just also not correct here.

76
00:07:37,800 --> 00:07:46,800
Although he can take the vegetable, put the vegetable in his hand and eat it.

77
00:07:46,800 --> 00:07:48,800
It is not proper for him to do so.

78
00:07:48,800 --> 00:07:51,800
So he should put it in the bowl.

79
00:07:51,800 --> 00:07:55,800
Because his second vessel has been refused.

80
00:07:55,800 --> 00:07:57,800
It is not allowable to use anything else.

81
00:07:57,800 --> 00:07:59,800
Not even the leaf of a tree.

82
00:07:59,800 --> 00:08:03,800
Sometimes people use leaf of a tree as a pool or as a receptacle.

83
00:08:03,800 --> 00:08:08,800
Even that is not allowed.

84
00:08:08,800 --> 00:08:09,800
And there are three grades.

85
00:08:09,800 --> 00:08:12,800
For one who is strict, except at the time of eating sugar cane,

86
00:08:12,800 --> 00:08:16,800
it is not allowed while eating to throw rubbish away.

87
00:08:16,800 --> 00:08:20,800
Maybe some bones or something rubbish.

88
00:08:20,800 --> 00:08:26,800
And it is not allowed while eating to break up rice lumps, fish meat and cakes.

89
00:08:26,800 --> 00:08:30,800
The rubbish should be thrown away and the rice lumps, etc.

90
00:08:30,800 --> 00:08:39,800
The medium one is allowed to break them up with one hand while eating and he is called hand capacity.

91
00:08:39,800 --> 00:08:41,800
The mild one is called a bull as a dick.

92
00:08:41,800 --> 00:08:46,800
Anything that can be put into his bowl, he is allowed while eating to break up.

93
00:08:46,800 --> 00:08:48,800
That is rice lumps, etc.

94
00:08:48,800 --> 00:08:56,800
With his hand or with his teeth.

95
00:08:56,800 --> 00:09:02,800
The moment any one of these three agrees to be a second vessel, his ascetic practices program.

96
00:09:02,800 --> 00:09:06,800
So he could just only one bowl.

97
00:09:06,800 --> 00:09:12,800
Then in our country, after taking milk, we drink water.

98
00:09:12,800 --> 00:09:15,800
It is a customary.

99
00:09:15,800 --> 00:09:21,800
So when we want to drink water, we put the water in the bowl and drink from the bowl.

100
00:09:21,800 --> 00:09:31,800
So when I saw Zen Zen people eating in a bowl and then wash the bowl and drink the water,

101
00:09:31,800 --> 00:09:33,800
I was reminded of this practice.

102
00:09:33,800 --> 00:09:36,800
It may have some connection with this practice.

103
00:09:36,800 --> 00:09:42,800
Here the practice is to have only one bowl.

104
00:09:42,800 --> 00:09:53,800
We put everything in one bowl and eat from this bowl, including water and other beverages.

105
00:09:53,800 --> 00:09:57,800
In the benefits, craving for variety of taste is eliminated.

106
00:09:57,800 --> 00:09:59,800
Excessiveness of which is abandoned.

107
00:09:59,800 --> 00:10:03,800
He sees the purpose and the right amount in you to know.

108
00:10:03,800 --> 00:10:08,800
Now, and the right amount should go.

109
00:10:08,800 --> 00:10:17,800
What is man here is, he sees the mere purpose in taking food.

110
00:10:17,800 --> 00:10:27,800
Now, Buddha said, you take food or must take food not to be proud of themselves,

111
00:10:27,800 --> 00:10:41,800
but just enough to keep him alive so that he could practice Buddha's teachings.

112
00:10:41,800 --> 00:10:44,800
So that is the purpose in food.

113
00:10:44,800 --> 00:10:52,800
So here also I think food may be better than nutrement, although it is not not wrong.

114
00:10:52,800 --> 00:11:02,800
So the purpose of taking food is not to beautify oneself, not to take pride in one's strength and so on.

115
00:11:02,800 --> 00:11:13,800
Here it is only in one bowl and so he cannot have that kind of pride in other things.

116
00:11:13,800 --> 00:11:21,800
He is not bothered with carrying sources, etc. about his life conforms to the principles of humanness of research and so on.

117
00:11:21,800 --> 00:11:30,800
Now, one, one word is left here and translated and that is, he is not distracted while eating.

118
00:11:30,800 --> 00:11:38,800
Because he used only one bowl, he doesn't have to be looking for other bowls, so he is not distracted.

119
00:11:38,800 --> 00:11:49,800
That what is, that the translation of that what is missing and the translation.

120
00:11:49,800 --> 00:11:51,800
So this is one bowl either.

121
00:11:51,800 --> 00:11:57,800
The next one, it is difficult to understand the next one.

122
00:11:57,800 --> 00:12:02,800
Later food refuses practice.

123
00:12:02,800 --> 00:12:28,800
Now, one among eats and if he refuses to take some more, then he must not eat other food after changing his posture.

124
00:12:28,800 --> 00:12:35,800
Suppose I am eating sitting and I am eating and somebody comes taking something and say,

125
00:12:35,800 --> 00:12:39,800
offer something to me and I say, no, I don't want that.

126
00:12:39,800 --> 00:12:40,800
It is enough.

127
00:12:40,800 --> 00:12:47,800
Then if I have done so, I can eat all until I finish my meal.

128
00:12:47,800 --> 00:12:49,800
That is why sitting.

129
00:12:49,800 --> 00:12:57,800
But if I stand up or if I walk and I want to eat again, then I cannot eat.

130
00:12:57,800 --> 00:13:07,800
There is some kind of a act of linear to perform in order for me to eat.

131
00:13:07,800 --> 00:13:10,800
So that is what is meant here.

132
00:13:10,800 --> 00:13:12,800
Later food refuses me.

133
00:13:12,800 --> 00:13:24,800
Now, as soon as I sit down, I cannot, I am not said to be refusing anything.

134
00:13:24,800 --> 00:13:33,520
But after eating something, after eating one lump of food and then I refuse, then I cannot

135
00:13:33,520 --> 00:13:40,800
eat other food if I change my position, if I change my posture.

136
00:13:40,800 --> 00:13:54,640
Now, this mark, who undertakes this ascetic practice, cannot take food after having

137
00:13:54,640 --> 00:13:57,640
made it allowable for him.

138
00:13:57,640 --> 00:14:11,560
Now, suppose I refuse the offer, then if I want to eat after breaking this posture, then

139
00:14:11,560 --> 00:14:18,040
I must take that food to another month and let him eat a little and say, that is enough

140
00:14:18,040 --> 00:14:19,040
for me.

141
00:14:19,040 --> 00:14:25,920
That means it is left over of him and then I can eat it.

142
00:14:25,920 --> 00:14:30,080
It is something like a punishment, they used to refuse and then you know you want to eat.

143
00:14:30,080 --> 00:14:37,520
So another person is left over, something like that.

144
00:14:37,520 --> 00:14:47,080
So a man who does not undertake this ascetic practice can eat that way if he wants to eat

145
00:14:47,080 --> 00:14:56,880
more, but a monk who undertakes this practice must not eat in any way.

146
00:14:56,880 --> 00:15:05,780
He must avoid and picking up the food and going to other monk and have him made it

147
00:15:05,780 --> 00:15:08,440
left over, he cannot do that.

148
00:15:08,440 --> 00:15:11,800
So that is what is meant in here.

149
00:15:11,800 --> 00:15:20,160
The translation is not so convincing here and the words in these square breakers, I don't

150
00:15:20,160 --> 00:15:24,560
know where he got them from.

151
00:15:24,560 --> 00:15:29,920
They don't help much in understanding.

152
00:15:29,920 --> 00:15:37,680
So there are three greats here.

153
00:15:37,680 --> 00:15:45,800
There is no showing that he has it enough with respect to the first lung, but there is

154
00:15:45,800 --> 00:15:49,640
when he refuses more while that is being swallowed.

155
00:15:49,640 --> 00:16:02,280
That means in the rule it is said that a monk who refuses while eating or a monk who refuses

156
00:16:02,280 --> 00:16:06,920
after he has started eating.

157
00:16:06,920 --> 00:16:17,120
So if he has not eaten at all there can be no no refusal, but he eats one lung and then

158
00:16:17,120 --> 00:16:19,320
the next lung he refuses.

159
00:16:19,320 --> 00:16:24,560
So there can be refusal only at the second and other lungs.

160
00:16:24,560 --> 00:16:30,320
So one one who is strict has just shown that he has it enough that means he has refused.

161
00:16:30,320 --> 00:16:37,320
He does not eat the second lung and so he must stop there only only one monk full of that

162
00:16:37,320 --> 00:16:38,320
finish.

163
00:16:38,320 --> 00:16:46,240
The medium one is also that food with respect to which he has shown that he has it enough.

164
00:16:46,240 --> 00:16:56,600
So the medium one could eat, go on eating, but the mild one goes on eating until he gets

165
00:16:56,600 --> 00:17:07,560
up from his seeds, that means he could eat as much as he likes unless he does not change

166
00:17:07,560 --> 00:17:13,280
his posture.

167
00:17:13,280 --> 00:17:23,240
So it involves a sudden rule and that the rule is that if you have refused to accept something

168
00:17:23,240 --> 00:17:30,440
and then if you want to eat it again then you must do something, if you change posture.

169
00:17:30,440 --> 00:17:37,320
That is why monks do not want to say no when something is offered to them.

170
00:17:37,320 --> 00:17:45,720
He may accept it and he may not eat it, but he does not want to say no because that amounts

171
00:17:45,720 --> 00:17:52,720
to refusal and so he could not eat later.

172
00:17:52,720 --> 00:18:05,600
And there is something like talking in a roundabout way and in Burmese we have a certain

173
00:18:05,600 --> 00:18:13,920
expression for that which goes around that rule and so when someone offers something

174
00:18:13,920 --> 00:18:20,720
to me and I want to say no I will not say no, but I will say there is something like

175
00:18:20,720 --> 00:18:25,360
it is completely translated, I will say it is complete or something like that.

176
00:18:25,360 --> 00:18:35,840
But sometimes little people do not know the monks language, so there is often a problem.

177
00:18:35,840 --> 00:18:42,960
I will say it is complete or something like that, but he does not know that I refuse

178
00:18:42,960 --> 00:18:55,040
to accept, so it is better to accept it and you can leave it.

179
00:18:55,040 --> 00:19:04,280
So this is the or later food refuses practice, so this practice involves a rule in the party

180
00:19:04,280 --> 00:19:20,680
mocha, I think we should stop here, let us take two weeks to finish this chapter.

181
00:19:35,280 --> 00:19:41,880
Are the others from the party mocha to the other practices from the party mocha?

182
00:19:41,880 --> 00:19:49,280
No, these practices are not from party mocha, such you said this one was, but it involves

183
00:19:49,280 --> 00:19:58,080
this one involves party mocha rule because the party mocha rule is there, I must not

184
00:19:58,080 --> 00:20:07,040
leave if I have refused, now even though I have refused if I want to eat later then I can

185
00:20:07,040 --> 00:20:13,080
have it, they left over by another monk, but if I keep this practice then I cannot do

186
00:20:13,080 --> 00:20:17,080
that.

187
00:20:17,080 --> 00:20:33,480
So it would seem from this that fasting is not permissible as a practice, or not eating

188
00:20:33,480 --> 00:20:50,520
altogether, no, that is not acceptable, even for one or two or three days, it is okay

189
00:20:50,520 --> 00:20:59,160
to first if you want to, but not as not as a practice, because one has to eat, so to keep

190
00:20:59,160 --> 00:21:05,160
it in, to keep himself alive.

191
00:21:05,160 --> 00:21:12,880
So fasting in Buddhism is fasting for half a day when lay people keep eight precepts,

192
00:21:12,880 --> 00:21:19,160
then they do not eat from noon until the next morning.

193
00:21:19,160 --> 00:21:31,720
Does it happen to go with food, does it happen to go with food when you go for arms?

194
00:21:31,720 --> 00:21:37,640
Whether you go for arms or not, you are not to eat after noon, sometimes when you go

195
00:21:37,640 --> 00:21:43,320
after arms, you do not get food in the morning, when you try to go in and give it to

196
00:21:43,320 --> 00:21:44,320
you.

197
00:21:44,320 --> 00:21:51,200
You eat out of noon, because no one gives you food, you must go without food, you cannot

198
00:21:51,200 --> 00:22:08,920
eat after noon on any account, sometimes one travelling, sometimes I had to skip meals.

199
00:22:08,920 --> 00:22:15,920
You know when you go to play and they do not serve meals until after noon.

200
00:22:15,920 --> 00:22:21,440
The problem is that the answer to the question is, you know, wait, in Burma you will never

201
00:22:21,440 --> 00:22:22,440
have that problem.

202
00:22:22,440 --> 00:22:30,040
That is right, because in Burma you will always get enough for enough to eat.

203
00:22:30,040 --> 00:22:37,200
People are very willing, they are, they are glad to be able to give to monks.

204
00:22:37,200 --> 00:22:45,160
So it is not a problem in Buddhist countries, but here, in other countries, there is a

205
00:22:45,160 --> 00:22:46,160
problem.

206
00:22:46,160 --> 00:22:47,160
Yes.

207
00:22:47,160 --> 00:23:08,320
I do not know if you have seen this movie of the heart of Burma, it is different, that time

208
00:23:08,320 --> 00:23:14,800
I don't think he was really properly paid, but he knows that way he can get food and initially

209
00:23:14,800 --> 00:23:21,800
he can get a properly ordained and not very interested in eating.

210
00:23:21,800 --> 00:23:36,160
And another question that I think he asked about, you know, the ropes, they are done

211
00:23:36,160 --> 00:23:44,040
to actually, and also I think we have one season on the festival where the people

212
00:23:44,040 --> 00:24:02,760
know, actually what Libibu do is to make cloth, not ropes, what they call the rope material.

213
00:24:02,760 --> 00:24:13,640
You know, now it is a season for offering katina ropes and katina rope is different

214
00:24:13,640 --> 00:24:17,840
from the ordinary ropes.

215
00:24:17,840 --> 00:24:27,640
And the difference is we must make the rope in one day.

216
00:24:27,640 --> 00:24:37,600
So suppose a labor in comes to a monastery and offer not a finished rope, but a cloth to

217
00:24:37,600 --> 00:24:45,680
be used as katina rope, if we accept it today that we must finish it today until it becomes

218
00:24:45,680 --> 00:24:53,840
a rope, that is in the olden days where when no ready made ropes or a variable or they

219
00:24:53,840 --> 00:24:57,120
don't want to offer ready made ropes.

220
00:24:57,120 --> 00:25:04,840
So in that case, all the monks and the monostreamers work together, they are some boiling

221
00:25:04,840 --> 00:25:09,480
a die and some touching some cutting and so on.

222
00:25:09,480 --> 00:25:16,240
And then everybody must lend a hand.

223
00:25:16,240 --> 00:25:25,480
So following in that tradition and nowadays in Obama, people have a festival weaving cloth

224
00:25:25,480 --> 00:25:36,000
and they transfer the expression and still to their weaving.

225
00:25:36,000 --> 00:25:48,560
And so they tried to weave from 6 o'clock this evening before dawn next morning.

226
00:25:48,560 --> 00:25:57,680
So they tried to weave and there is a contest, the girls can enter this contest and weaving

227
00:25:57,680 --> 00:26:01,120
cloth for ropes.

228
00:26:01,120 --> 00:26:04,960
I don't know how they decide to edit the winner.

229
00:26:04,960 --> 00:26:14,600
So actually the work and still is used among monks and that means you must make the rope on

230
00:26:14,600 --> 00:26:17,080
the very day you accept the cloth.

231
00:26:17,080 --> 00:26:23,040
It must not constale and go over to the next day.

232
00:26:23,040 --> 00:26:28,760
So in bummies we call, please excuse me, methothing man, right?

233
00:26:28,760 --> 00:26:38,120
So we call and still rope and and still rope means rope made on the same day.

234
00:26:38,120 --> 00:26:44,240
But now people call people say they are offering and still rope but actually what they

235
00:26:44,240 --> 00:26:49,800
do is just weave cloth and offer to the monks and the monks have to do that.

236
00:26:49,800 --> 00:26:55,480
But now we are in a better shape because they are already made ropes and so we don't

237
00:26:55,480 --> 00:26:57,040
have to do anything.

238
00:26:57,040 --> 00:27:06,320
But sometimes people wanted to do something as they did in the old days and when I was

239
00:27:06,320 --> 00:27:16,880
living in my country, it's a kind, a certain head of a monastery said, why not do something

240
00:27:16,880 --> 00:27:24,800
like like they did in the old days, so he had people brought cloth through the monastery

241
00:27:24,800 --> 00:27:33,760
and then everybody has to work, oh it's a great work because even the smallest of the

242
00:27:33,760 --> 00:27:34,920
rope.

243
00:27:34,920 --> 00:27:40,760
So you have to die at two of three times, not one time.

244
00:27:40,760 --> 00:27:47,040
And if there were rains, it would be very difficult.

245
00:27:47,040 --> 00:27:55,000
But it was just lucky because in Abu Baba it was again, there was not much frame.

246
00:27:55,000 --> 00:28:01,240
So we could, we were able to finish the rope in time.

247
00:28:01,240 --> 00:28:13,320
It's not really a dry but a little tam but it could be used as a rope.

248
00:28:13,320 --> 00:28:19,720
That is why there are no commercial ropes and so monks do not know how to make ropes

249
00:28:19,720 --> 00:28:20,720
now.

250
00:28:20,720 --> 00:28:27,720
It's the dimension of the parts and then how to stitch them.

251
00:28:27,720 --> 00:28:33,800
Most don't know now, most monks don't know.

252
00:28:33,800 --> 00:28:40,480
And when I came to this country first, people asked me to order ropes from bhamma, I said,

253
00:28:40,480 --> 00:28:43,280
why not make ropes here?

254
00:28:43,280 --> 00:28:49,040
So I gave them a dimension of the rope.

255
00:28:49,040 --> 00:28:54,040
And then made it here.

256
00:28:54,040 --> 00:29:10,280
Yes, actually they represent, according to our books, they represent the pattern of the

257
00:29:10,280 --> 00:29:11,280
fields.

258
00:29:11,280 --> 00:29:19,600
Once Buddha was travelling and he was up a mountain and he looked down and he saw the fields.

259
00:29:19,600 --> 00:29:26,040
You know, the fields are small fields in India, not like fields in the United States.

260
00:29:26,040 --> 00:29:30,520
Here they use machines and so these are the two, three miles long.

261
00:29:30,520 --> 00:29:35,600
There if you may be about 10, 20 yards wide and so on.

262
00:29:35,600 --> 00:29:40,400
So Buddha saw these, what do you call those?

263
00:29:40,400 --> 00:29:47,000
Boundaries of these fields, kasen, kasen, what do you call those?

264
00:29:47,000 --> 00:29:50,360
Those beds, beds, rows.

265
00:29:50,360 --> 00:29:53,040
No, no, the edges.

266
00:29:53,040 --> 00:29:58,000
Yeah, so Buddha saw this and Buddha saw an an an an an an an an an an an a could you make

267
00:29:58,000 --> 00:30:07,120
a rope like this, an an a suggest and so an an a made the rope which looks like the the

268
00:30:07,120 --> 00:30:11,640
field, the pattern of the field, so it came to me.

269
00:30:11,640 --> 00:30:18,640
And so this we usually have five five sections for this role.

270
00:30:18,640 --> 00:30:36,640
You cannot see this case as well, the sections, this is one section and this is the second

271
00:30:36,640 --> 00:30:45,640
section, the third section is actually wider than the other two, so the five sections that

272
00:30:45,640 --> 00:30:46,640
this strips.

273
00:30:46,640 --> 00:30:47,640
Yeah.

274
00:30:47,640 --> 00:30:57,560
And then there there is a small room here to two, two rooms in one section, it's it's

275
00:30:57,560 --> 00:31:11,880
a very much and they must be these stitches must be cut actually cut and then stitched

276
00:31:11,880 --> 00:31:19,160
together again, but now it is they just may make a particle this, they do not cut before

277
00:31:19,160 --> 00:31:28,560
it's just pull it in, it's this, and the idea behind this that you're getting these cloth

278
00:31:28,560 --> 00:31:29,960
from wherever that's right.

279
00:31:29,960 --> 00:31:36,640
Yeah, so you you you may not get the the the right size, you have to become small pieces

280
00:31:36,640 --> 00:31:49,000
of cloth and then you make them into a row, but everybody followed the same pattern.

281
00:31:49,000 --> 00:32:04,000
Oh yes, yes, and this has more sections, the number of sections is hot, 7, 9, 13, 15.

282
00:32:04,000 --> 00:32:08,120
That's right, okay.

283
00:32:08,120 --> 00:32:18,760
This is kind of a miniature, miniature, miniature of these, the stitches, yeah, the patterns

284
00:32:18,760 --> 00:32:19,760
of the stitches.

285
00:32:19,760 --> 00:32:25,760
Yes, but we have border, you have border, yes, this is the border, yeah, this is the border,

286
00:32:25,760 --> 00:32:34,040
I didn't even have the corners, corners like we of course, same corner.

287
00:32:34,040 --> 00:32:48,200
And this is used, we put some buttons here and when I, where this way, actually they

288
00:32:48,200 --> 00:32:59,800
must be tied together or they must be a button and put in here, or I can just tie them

289
00:32:59,800 --> 00:33:12,200
together.

290
00:33:12,200 --> 00:33:22,200
So when I walk, they log them and won't show, but yeah, I don't do this because sometimes

291
00:33:22,200 --> 00:33:44,200
it got caught in some of the door of the car, so I don't know how to do it.

292
00:33:44,200 --> 00:34:00,200
I don't know how to do it, but I don't know how to do it.

293
00:34:00,200 --> 00:34:05,200
.

294
00:34:05,200 --> 00:34:10,200
.

295
00:34:10,200 --> 00:34:15,200
.

296
00:34:15,200 --> 00:34:20,200
.

297
00:34:20,200 --> 00:34:25,200
..

298
00:34:25,200 --> 00:34:28,200
..

