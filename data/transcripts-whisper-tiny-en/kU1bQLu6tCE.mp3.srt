1
00:00:00,000 --> 00:00:07,000
Hey, good evening, everyone. Welcome to our evening dumb session.

2
00:00:12,000 --> 00:00:19,000
Where we come together and listen to me talk.

3
00:00:19,000 --> 00:00:26,000
People come to hear me talk because I'm a teacher.

4
00:00:50,000 --> 00:00:57,000
Teachers, it's an interesting, interesting concept, right?

5
00:01:05,000 --> 00:01:12,000
We live most of our lives without someone teaching us how to live.

6
00:01:12,000 --> 00:01:18,000
We have help from our parents, their first teachers.

7
00:01:18,000 --> 00:01:27,000
But they don't really, you know, we don't take courses on how to be a kid.

8
00:01:27,000 --> 00:01:30,000
Then we go to school.

9
00:01:30,000 --> 00:01:39,000
There we have teachers and they're teaching us something, but you know, they don't really teach us much.

10
00:01:39,000 --> 00:01:47,000
Most of the stuff we learn in school is indirect from our peers,

11
00:01:47,000 --> 00:01:58,000
from the drama of our friends and enemies and from all the trouble we get into.

12
00:01:58,000 --> 00:02:07,000
Teachers just give us lots of information and mess with our heads.

13
00:02:07,000 --> 00:02:12,000
Because think in certain ways and so on.

14
00:02:12,000 --> 00:02:17,000
But what we really learn is from the people, even the teachers themselves.

15
00:02:17,000 --> 00:02:25,000
Their personalities, if they're kind and generous, we learn that, if they're mean and evil.

16
00:02:25,000 --> 00:02:35,000
We react and we respond and we are shaped by that.

17
00:02:35,000 --> 00:02:42,000
And then we're set out in the world and there's certainly no, nothing to prepare us for that.

18
00:02:42,000 --> 00:02:44,000
You don't take courses.

19
00:02:44,000 --> 00:02:51,000
Well, there are some homemade courses, but there's no course for life.

20
00:02:59,000 --> 00:03:02,000
But a religious teacher is just that.

21
00:03:02,000 --> 00:03:06,000
We realize this, we say, hey, wait a minute.

22
00:03:06,000 --> 00:03:13,000
All this learning I've done, even my parents.

23
00:03:13,000 --> 00:03:17,000
Nobody taught me how to live.

24
00:03:17,000 --> 00:03:20,000
And so we go and find religion.

25
00:03:20,000 --> 00:03:25,000
Religion, I think, sort of designed to fill that role.

26
00:03:25,000 --> 00:03:28,000
Maybe not design, but it's come to fill that role.

27
00:03:28,000 --> 00:03:40,000
And for many people, that's what they use religion for.

28
00:03:40,000 --> 00:03:46,000
So people come see me so I can tell them how to live.

29
00:03:46,000 --> 00:03:50,000
It's not really funny, but it is kind of funny.

30
00:03:50,000 --> 00:03:52,000
It's a quite serious thing.

31
00:03:52,000 --> 00:03:55,000
It's quite an important thing.

32
00:03:55,000 --> 00:03:58,000
It's quite a responsibility.

33
00:03:58,000 --> 00:04:08,000
I'm supposed to tell people how to live.

34
00:04:08,000 --> 00:04:14,000
Which wouldn't be a problem if it was just up to me because I can teach you how I lived.

35
00:04:14,000 --> 00:04:22,000
I wouldn't wish it on anyone.

36
00:04:22,000 --> 00:04:25,000
But I'm not really a teacher.

37
00:04:25,000 --> 00:04:27,000
I'm not really the teacher, right?

38
00:04:27,000 --> 00:04:32,000
The Buddha is our teacher.

39
00:04:32,000 --> 00:04:35,000
The monks were all afraid when the Buddha was passing away.

40
00:04:35,000 --> 00:04:37,000
They said, look, none of us is the teacher.

41
00:04:37,000 --> 00:04:40,000
None of us is the Buddha.

42
00:04:40,000 --> 00:04:42,000
When we're going to do when the Buddha's gone.

43
00:04:42,000 --> 00:04:46,000
And the Buddha said, it's a dawn.

44
00:04:46,000 --> 00:04:55,000
And Mahapara Nibana said, he said, you might think that you have no teacher.

45
00:04:55,000 --> 00:05:03,000
Which would be a better thought than thinking that one of them was the teacher, right?

46
00:05:03,000 --> 00:05:11,000
Normally, when you leave your teacher, you go on and think, well, no, I'm the teacher.

47
00:05:11,000 --> 00:05:15,000
You learn enough from your teacher, and then you say, well, I'm going to know I'm the teacher.

48
00:05:15,000 --> 00:05:20,000
My teacher was a teacher, now I'm the teacher.

49
00:05:20,000 --> 00:05:27,000
So when the Buddha passed away, they could have just said, well, you know, we'll just make up our own stuff from now on.

50
00:05:27,000 --> 00:05:29,000
I will be the teacher.

51
00:05:29,000 --> 00:05:34,000
Or this monk or this nun will be the teacher.

52
00:05:34,000 --> 00:05:39,000
But these monks in Buddhism, it's not like that.

53
00:05:39,000 --> 00:05:47,000
Buddhism, it was proper for them, well, not proper, but it was better for them.

54
00:05:47,000 --> 00:05:51,000
And it was expected of them that they might think, now we have no teacher.

55
00:05:51,000 --> 00:05:54,000
Because none of us are the Buddha.

56
00:05:54,000 --> 00:05:59,000
They had such respect for him that, of course, they would think none of us can feel that role.

57
00:05:59,000 --> 00:06:05,000
There was one monk who tried, who thought he could.

58
00:06:05,000 --> 00:06:14,000
And then when the Buddha would teach, and then when he got tired, he would have one of his students teaching, he would lie down and listen.

59
00:06:14,000 --> 00:06:22,000
And this monk, when he taught for a while, then he got tired, and he lay down and fell asleep.

60
00:06:22,000 --> 00:06:32,000
And when he fell asleep, the monks who were teaching taught all his followers the right thing, and they all went back to the Buddha.

61
00:06:32,000 --> 00:06:38,000
And then this monk was asleep.

62
00:06:38,000 --> 00:06:43,000
And then he woke up and his hench, his right hand man was lying there.

63
00:06:43,000 --> 00:06:45,000
So where did all of our followers go?

64
00:06:45,000 --> 00:06:47,000
Oh, they went back to the Buddha.

65
00:06:47,000 --> 00:06:54,000
And his right hand man kicked him in the chest, because they were all a bunch of rotten scoundrels.

66
00:06:54,000 --> 00:06:57,000
He ended up dying.

67
00:06:57,000 --> 00:06:58,000
A long story.

68
00:06:58,000 --> 00:07:01,000
We'll get into that story.

69
00:07:01,000 --> 00:07:06,000
No, we don't think we're the teacher after the Buddha.

70
00:07:06,000 --> 00:07:09,000
And Buddha said, you might think this, but don't think this.

71
00:07:09,000 --> 00:07:13,000
Ah, so this is not correct either.

72
00:07:13,000 --> 00:07:18,000
He said, Yoko and Anda.

73
00:07:18,000 --> 00:07:32,000
Dhammo, Jai, Vinayo, Jai.

74
00:07:32,000 --> 00:07:45,000
Whatever Dhamma and Vinaya that has been taught has been presented by me.

75
00:07:45,000 --> 00:07:53,000
Soomamachayena sata.

76
00:07:53,000 --> 00:08:02,000
That will be your teacher after my passing away.

77
00:08:02,000 --> 00:08:08,000
The Dhamma and Vinaya will be your teacher.

78
00:08:08,000 --> 00:08:10,000
Which is great.

79
00:08:10,000 --> 00:08:12,000
We have all of this Dhamma.

80
00:08:12,000 --> 00:08:14,000
The Dhamma is the things we should do.

81
00:08:14,000 --> 00:08:18,000
We have all this Vinaya as the things we shouldn't do.

82
00:08:18,000 --> 00:08:19,000
It's pretty clear.

83
00:08:19,000 --> 00:08:24,000
We've got a handbook for our life.

84
00:08:24,000 --> 00:08:25,000
Do this.

85
00:08:25,000 --> 00:08:26,000
Don't do that.

86
00:08:26,000 --> 00:08:30,000
Pretty simple.

87
00:08:30,000 --> 00:08:33,000
Unfortunately, it takes up a whole bookshelf.

88
00:08:33,000 --> 00:08:40,000
And it's not actually all that easy to follow.

89
00:08:40,000 --> 00:08:44,000
So we still have teachers.

90
00:08:44,000 --> 00:08:50,000
We still have people we call teachers.

91
00:08:50,000 --> 00:08:51,000
Which is fine.

92
00:08:51,000 --> 00:08:52,000
Which is important.

93
00:08:52,000 --> 00:08:58,000
I mean, in the Buddhist time, it was...

94
00:08:58,000 --> 00:09:01,000
It's important that we have different words.

95
00:09:01,000 --> 00:09:05,000
One word is Sata and the other word is Aacharya.

96
00:09:05,000 --> 00:09:09,000
Aacharya means someone who teaches.

97
00:09:09,000 --> 00:09:12,000
But Sata is like a master.

98
00:09:12,000 --> 00:09:17,000
It's like the headmaster.

99
00:09:17,000 --> 00:09:24,000
The root guru as they say in Tibetan Buddhism.

100
00:09:24,000 --> 00:09:27,000
The Buddha was our root guru.

101
00:09:27,000 --> 00:09:29,000
He was the one who gave the teachings.

102
00:09:29,000 --> 00:09:34,000
But as far as explaining and handing on the teachings.

103
00:09:34,000 --> 00:09:38,000
Even in the Buddhist time, there were lots of monks like that.

104
00:09:38,000 --> 00:09:43,000
They didn't all go to the Buddha for instruction.

105
00:09:43,000 --> 00:09:53,000
Eventually they had to learn from other monks.

106
00:09:53,000 --> 00:10:06,000
So all this is from thinking about one specific teaching of the Buddha.

107
00:10:06,000 --> 00:10:08,000
I wanted to ask...

108
00:10:08,000 --> 00:10:11,000
I wanted to sort of answer the question.

109
00:10:21,000 --> 00:10:24,000
What did the Buddha have to say about teachers?

110
00:10:30,000 --> 00:10:32,000
Maybe think of this one teaching.

111
00:10:32,000 --> 00:10:36,000
That's actually quite important.

112
00:10:36,000 --> 00:10:38,000
Something to do really about teachers.

113
00:10:38,000 --> 00:10:42,000
But Buddha had two teachers.

114
00:10:42,000 --> 00:10:47,000
Two teachers, Alara and Utaka.

115
00:10:47,000 --> 00:10:52,000
Let's see, Alara and Utaka.

116
00:10:52,000 --> 00:10:54,000
Utaka was in his teacher.

117
00:10:54,000 --> 00:10:55,000
I don't think, wait.

118
00:10:55,000 --> 00:10:58,000
Yes, Utaka was in his teacher.

119
00:10:58,000 --> 00:11:01,000
Alara and Utaka.

120
00:11:01,000 --> 00:11:06,000
When he was with Alara...

121
00:11:06,000 --> 00:11:11,000
Alara Kalama.

122
00:11:11,000 --> 00:11:14,000
There was another man there.

123
00:11:14,000 --> 00:11:17,000
Apparently named Barandu.

124
00:11:17,000 --> 00:11:21,000
Another ascetic.

125
00:11:21,000 --> 00:11:26,000
So one time the Buddha went to Kapilawatu and there was nowhere for him to stay.

126
00:11:26,000 --> 00:11:28,000
Which is interesting.

127
00:11:28,000 --> 00:11:35,000
He went home and they didn't have a place for him.

128
00:11:35,000 --> 00:11:37,000
So they said, you go and stay with...

129
00:11:37,000 --> 00:11:38,000
Hey, can you...

130
00:11:38,000 --> 00:11:40,000
Is it okay if you go and stay with Barandu?

131
00:11:40,000 --> 00:11:41,000
He was your...

132
00:11:41,000 --> 00:11:51,000
He was your old companion in the holy life when you lived under Alara Kalama.

133
00:11:51,000 --> 00:11:55,000
And so the Buddha went and stayed there and...

134
00:11:55,000 --> 00:11:59,000
I got in a bit of an argument.

135
00:11:59,000 --> 00:12:03,000
Mahana became to see them and...

136
00:12:03,000 --> 00:12:05,000
The Buddha did an argument.

137
00:12:05,000 --> 00:12:07,000
Mahana became...

138
00:12:07,000 --> 00:12:16,000
And the Buddha said, Mahana there are three kinds of teachers in the world.

139
00:12:16,000 --> 00:12:20,000
Some teacher talks about...

140
00:12:20,000 --> 00:12:25,000
instructs their students to give up craving.

141
00:12:25,000 --> 00:12:30,000
Central pleasure.

142
00:12:30,000 --> 00:12:32,000
And that's it.

143
00:12:32,000 --> 00:12:36,000
And they focus solely on central pleasure.

144
00:12:36,000 --> 00:12:39,000
They don't focus on...

145
00:12:39,000 --> 00:12:43,000
They're two things specifically that they don't focus on.

146
00:12:43,000 --> 00:13:02,000
They don't talk to their students about understanding form or feeling.

147
00:13:02,000 --> 00:13:07,000
Just central pleasure.

148
00:13:07,000 --> 00:13:10,000
Full understanding of central pleasure.

149
00:13:10,000 --> 00:13:17,000
The second type of teacher prescribes the understanding of central pleasures and form,

150
00:13:17,000 --> 00:13:20,000
but not feeling.

151
00:13:20,000 --> 00:13:23,000
And the third type of teacher prescribes

152
00:13:23,000 --> 00:13:32,000
full understanding of central pleasure, form, and feeling.

153
00:13:32,000 --> 00:13:37,000
When I first read this, I had been looking for...

154
00:13:37,000 --> 00:13:42,000
I can't remember how it happened, but I've been looking for something...

155
00:13:42,000 --> 00:13:44,000
Something to do with central pleasures.

156
00:13:44,000 --> 00:13:46,000
So I'm way of dealing with central pleasures.

157
00:13:46,000 --> 00:13:49,000
Why I really like this teaching is because...

158
00:13:49,000 --> 00:13:53,000
Just this little kernel of teaching that we find buried in the...

159
00:13:53,000 --> 00:13:58,000
I'm good to any guy.

160
00:13:58,000 --> 00:14:00,000
It...

161
00:14:00,000 --> 00:14:06,000
Well, validated something that I had sort of put together on my own in the Buddha's teaching.

162
00:14:06,000 --> 00:14:10,000
I've talked about this before.

163
00:14:10,000 --> 00:14:16,000
When you're dealing with central pleasures, you can't just focus on central pleasure.

164
00:14:16,000 --> 00:14:22,000
If that's all, then you're just trying to stem the tide.

165
00:14:22,000 --> 00:14:27,000
You're not actually turning off the flow.

166
00:14:27,000 --> 00:14:29,000
Central pleasures come from somewhere.

167
00:14:29,000 --> 00:14:35,000
They come from form and they come from feeling.

168
00:14:35,000 --> 00:14:52,000
There's much more to the Buddha's teaching, but this really highlights something important.

169
00:14:52,000 --> 00:14:56,000
It puts together something very practical for us.

170
00:14:56,000 --> 00:15:01,000
And, of course, reminds us that our practice has to be more comprehensive

171
00:15:01,000 --> 00:15:09,000
and simply focusing on the problem.

172
00:15:09,000 --> 00:15:11,000
We're teaching that tell you all central pleasures.

173
00:15:11,000 --> 00:15:12,000
This is the problem.

174
00:15:12,000 --> 00:15:15,000
I mean, the problem is suffering, right?

175
00:15:15,000 --> 00:15:20,000
And so it is sophisticated to be able to say central pleasures are the problem.

176
00:15:20,000 --> 00:15:26,000
That, in and of itself, is a sophistication because an ordinary person doesn't think that.

177
00:15:26,000 --> 00:15:28,000
They think...

178
00:15:28,000 --> 00:15:30,000
Suffering central pleasure.

179
00:15:30,000 --> 00:15:33,000
Central pleasure is good, suffering is bad.

180
00:15:33,000 --> 00:15:36,000
How do we just get what we want all the time?

181
00:15:36,000 --> 00:15:46,000
How do we have these central pleasures without all the frustration and disappointment and so on?

182
00:15:46,000 --> 00:15:49,000
The anger and the conflict.

183
00:15:49,000 --> 00:15:59,000
The Buddha pointed out the reason we fight and go to war is all because of central pleasure.

184
00:15:59,000 --> 00:16:04,000
If we didn't want for anything, if we didn't have these desires,

185
00:16:04,000 --> 00:16:05,000
never fight.

186
00:16:05,000 --> 00:16:07,000
It causes friends to fight.

187
00:16:07,000 --> 00:16:15,000
Friends will hurt and kill each other and manipulate each other for central pleasure.

188
00:16:15,000 --> 00:16:20,000
Due to central pleasure in order to get what they want.

189
00:16:20,000 --> 00:16:31,000
They will neglect to care for each other, to help each other.

190
00:16:31,000 --> 00:16:39,000
The reason we don't just solve all the world's problems is because we're too busy trying to get what we want.

191
00:16:39,000 --> 00:16:47,000
If we really cared about the world, no, if we just didn't have all these desires,

192
00:16:47,000 --> 00:16:50,000
we'd have so much more time to...

193
00:16:50,000 --> 00:16:56,000
The world would just fix itself really.

194
00:16:56,000 --> 00:16:59,000
It's sophisticated to see that.

195
00:16:59,000 --> 00:17:02,000
It's important to see that.

196
00:17:02,000 --> 00:17:05,000
In the time of the Buddha, it was a big thing.

197
00:17:05,000 --> 00:17:08,000
The Buddha wasn't the only one to talk about that.

198
00:17:08,000 --> 00:17:10,000
When he went forth, that was the big thing.

199
00:17:10,000 --> 00:17:12,000
How do we get rid of central pleasures?

200
00:17:12,000 --> 00:17:19,000
Free ourselves from the bondage of sensuality.

201
00:17:19,000 --> 00:17:26,000
Even today, it's something that meditators often concern themselves.

202
00:17:26,000 --> 00:17:33,000
I was asked about how do I deal with it.

203
00:17:33,000 --> 00:17:37,000
It came to me that you have to be able to separate.

204
00:17:37,000 --> 00:17:41,000
You can't talk about craving without talking about what it is that you're craving.

205
00:17:41,000 --> 00:17:44,000
You realize there are two things there.

206
00:17:44,000 --> 00:17:48,000
There is the form, the physical thing that you're craving,

207
00:17:48,000 --> 00:17:51,000
and then there's the craving of it.

208
00:17:51,000 --> 00:17:53,000
That's good.

209
00:17:53,000 --> 00:17:57,000
If you study the thing that you're craving,

210
00:17:57,000 --> 00:18:01,000
well, then you've got another piece of the puzzle.

211
00:18:01,000 --> 00:18:05,000
Someone who focuses on those two is actually more sophisticated

212
00:18:05,000 --> 00:18:15,000
because they understand that there's a problem with the thing that you're clinging to as well.

213
00:18:15,000 --> 00:18:20,000
Because if the things that we cling to were actually satisfying,

214
00:18:20,000 --> 00:18:21,000
there would be no problem.

215
00:18:21,000 --> 00:18:22,000
What's wrong with sensuality?

216
00:18:22,000 --> 00:18:26,000
Why does sensuality cause stress and suffering?

217
00:18:26,000 --> 00:18:32,000
Well, it turns out to be really because of the nature of the physical world.

218
00:18:32,000 --> 00:18:37,000
If we study that, we try to understand that.

219
00:18:37,000 --> 00:18:45,000
That's much better than just focusing on the desire.

220
00:18:45,000 --> 00:18:50,000
But even just focusing on those two, anyone who talks about just those two,

221
00:18:50,000 --> 00:18:54,000
the things that you desire and the craving for them,

222
00:18:54,000 --> 00:18:59,000
still clear that they don't understand and that they themselves haven't understood

223
00:18:59,000 --> 00:19:02,000
what's really going on because there's actually three things.

224
00:19:02,000 --> 00:19:05,000
The third is the feelings.

225
00:19:05,000 --> 00:19:09,000
In order to crave for something, it has to make you happy,

226
00:19:09,000 --> 00:19:11,000
or at least calm.

227
00:19:11,000 --> 00:19:19,000
It has to give you a feeling that is satisfying or enjoyable,

228
00:19:19,000 --> 00:19:25,000
pleasurable.

229
00:19:25,000 --> 00:19:29,000
I think it could be argued there's even more that we have to focus on,

230
00:19:29,000 --> 00:19:36,000
and so this is sort of an entrance to that idea that even

231
00:19:36,000 --> 00:19:44,000
the idea that there are many aspects to experience.

232
00:19:44,000 --> 00:19:54,000
Or the overarching idea that we have to be aware of what is here and now.

233
00:19:54,000 --> 00:20:02,000
Or rather than be aware of the problem, just the problem.

234
00:20:02,000 --> 00:20:07,000
Like if craving is our problem, we can't just focus on the craving.

235
00:20:07,000 --> 00:20:12,000
We have to focus on the whole bigger picture.

236
00:20:12,000 --> 00:20:16,000
We have to focus on whatever reality arises here and now,

237
00:20:16,000 --> 00:20:21,000
and that changes for a moment to moment.

238
00:20:21,000 --> 00:20:31,000
We're focusing on what's here and now every moment we get the bigger picture.

239
00:20:31,000 --> 00:20:34,000
We understand all aspects of it.

240
00:20:34,000 --> 00:20:36,000
Another thing might be the thoughts.

241
00:20:36,000 --> 00:20:38,000
What you think about the craving.

242
00:20:38,000 --> 00:20:44,000
You think all of this thing that I like is so great.

243
00:20:44,000 --> 00:20:50,000
Or maybe you worry about it, or you feel guilty about it.

244
00:20:50,000 --> 00:20:58,000
But these three, when we talk about craving,

245
00:20:58,000 --> 00:21:00,000
these three will get you through the craving.

246
00:21:00,000 --> 00:21:05,000
We'll get you to understand addiction,

247
00:21:05,000 --> 00:21:10,000
to let you understand any problem really.

248
00:21:10,000 --> 00:21:16,000
And so to generalize it, it's the experience,

249
00:21:16,000 --> 00:21:22,000
the results of the experience, the experience of the experience,

250
00:21:22,000 --> 00:21:26,000
and then the reaction to that experience.

251
00:21:26,000 --> 00:21:30,000
Or the object of experience, the experience of it,

252
00:21:30,000 --> 00:21:32,000
and the reaction.

253
00:21:32,000 --> 00:21:36,000
So the object is the physical, in this case.

254
00:21:36,000 --> 00:21:39,000
You see something beautiful.

255
00:21:39,000 --> 00:21:47,000
Here's something beautiful, and so on.

256
00:21:47,000 --> 00:21:51,000
The experience of it is the pleasant feeling, or the peaceful feeling.

257
00:21:51,000 --> 00:21:57,000
And the reaction to that is the liking of it, or the disliking, even.

258
00:21:57,000 --> 00:22:03,000
And if you focus on those three, and you go back and forth between them,

259
00:22:03,000 --> 00:22:09,000
you understand all three of these. The Buddha didn't say this specifically.

260
00:22:09,000 --> 00:22:12,000
But this is what the Buddha taught.

261
00:22:12,000 --> 00:22:15,000
But it gave a very simple teaching, and he pointed out,

262
00:22:15,000 --> 00:22:19,000
and it's not enough, because it seems this other teacher,

263
00:22:19,000 --> 00:22:23,000
this other ascetic, who spent the night with the Buddha,

264
00:22:23,000 --> 00:22:27,000
was in one of the first two categories.

265
00:22:27,000 --> 00:22:30,000
So he went and he turned to Mahanam and he said,

266
00:22:30,000 --> 00:22:33,000
tell them that these are the Buddha, sorry, the Buddha asked,

267
00:22:33,000 --> 00:22:37,000
what do you think Mahanam? Are these three teachers the same?

268
00:22:37,000 --> 00:22:42,000
Or did they have the same goal? Or did they have different goals?

269
00:22:42,000 --> 00:22:46,000
And right away, Barandu says to him,

270
00:22:46,000 --> 00:22:50,000
say they have the same goal, Mahanam.

271
00:22:50,000 --> 00:22:56,000
Because he didn't want to look like he was inferior to the Buddha.

272
00:22:56,000 --> 00:23:02,000
And the Buddha says to Mahanam, say that they're different.

273
00:23:02,000 --> 00:23:04,000
In a second time, Barandu says,

274
00:23:04,000 --> 00:23:06,000
say they're the same, and the Buddha says,

275
00:23:06,000 --> 00:23:11,000
it's kind of a bit of a little tip.

276
00:23:11,000 --> 00:23:15,000
Third time, Barandu says, say the same, but Buddha says,

277
00:23:15,000 --> 00:23:17,000
say that they're different.

278
00:23:17,000 --> 00:23:21,000
And Barandu realized the Buddha,

279
00:23:21,000 --> 00:23:24,000
and the Buddha doesn't agree with me.

280
00:23:24,000 --> 00:23:34,000
And he left, and he left for good and never returned.

281
00:23:34,000 --> 00:23:42,000
And that's all that, that's all of that teaching.

282
00:23:42,000 --> 00:23:45,000
So it's not really about teachers at all,

283
00:23:45,000 --> 00:23:48,000
but it's interesting to think about teachers nonetheless.

284
00:23:48,000 --> 00:23:53,000
But no, this one is about,

285
00:23:53,000 --> 00:23:56,000
this one is about the teachings.

286
00:23:56,000 --> 00:23:58,000
And it's about the practice.

287
00:23:58,000 --> 00:24:03,000
It's really about our meditation, how we deal with cravings.

288
00:24:03,000 --> 00:24:07,000
We need to have a comprehensive practice that catches everything,

289
00:24:07,000 --> 00:24:12,000
every aspect of our pattern behavior.

290
00:24:12,000 --> 00:24:15,000
Not just what we experience, but how we feel about it,

291
00:24:15,000 --> 00:24:19,000
and how we react to it, even what we think about it,

292
00:24:19,000 --> 00:24:24,000
how we react to our reactions, and so on.

293
00:24:24,000 --> 00:24:26,000
So there you go, a little bit of dhamma for tonight.

294
00:24:26,000 --> 00:24:50,000
Thank you all for today.

