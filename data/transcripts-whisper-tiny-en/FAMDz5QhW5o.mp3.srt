1
00:00:00,000 --> 00:00:07,000
Would anything cause you to disrobe? Do you see any benefit to the lay life?

2
00:00:07,000 --> 00:00:12,000
Hmm, they're the kind of two different questions.

3
00:00:12,000 --> 00:00:15,000
Would anything cause me to disrobe?

4
00:00:15,000 --> 00:00:20,000
I have seen circumstances where people were forced to disrobe.

5
00:00:20,000 --> 00:00:25,000
People who had no interest in the lay life were forced to disrobe,

6
00:00:25,000 --> 00:00:29,000
generally dealing with their parents.

7
00:00:29,000 --> 00:00:33,000
One really impressive man in Thailand,

8
00:00:33,000 --> 00:00:38,000
who was a monk for 17 years and a very strong meditator.

9
00:00:38,000 --> 00:00:42,000
And by all appearances seems to be,

10
00:00:42,000 --> 00:00:47,000
perhaps in one of the four enlightened types of beings.

11
00:00:47,000 --> 00:00:51,000
Probably not in Arahat because they say that requires ordination.

12
00:00:51,000 --> 00:00:56,000
But certainly a very strong meditator and a very wise individual.

13
00:00:56,000 --> 00:01:01,000
And he collects garbage and sells garbage in Thailand,

14
00:01:01,000 --> 00:01:04,000
sells recycled, recyclables,

15
00:01:04,000 --> 00:01:06,000
which is actually a fairly lucrative trade.

16
00:01:06,000 --> 00:01:15,000
But he just gets on his motorcycle with a side car or a trailer or something.

17
00:01:15,000 --> 00:01:25,000
And he rides around looking for garbage to pick up and to sell or to trade or whatever.

18
00:01:25,000 --> 00:01:30,000
But he had disrobe because of his mother.

19
00:01:30,000 --> 00:01:34,000
I can't remember the exact story, but his mother was sick and couldn't take care of herself.

20
00:01:34,000 --> 00:01:35,000
And he was her only son.

21
00:01:35,000 --> 00:01:38,000
And so he felt he had disrobe.

22
00:01:38,000 --> 00:01:44,000
And he lives his life celibate and alone and a very simple life.

23
00:01:44,000 --> 00:01:50,000
He cooks food for his mother and then goes off to work and comes back in the evening.

24
00:01:50,000 --> 00:01:55,000
And they say he's called rice.

25
00:01:55,000 --> 00:01:58,000
And so it's quite a noble life.

26
00:01:58,000 --> 00:02:03,000
So what I mean to say is that even in his case,

27
00:02:03,000 --> 00:02:06,000
where he was obviously someone who was quite advanced,

28
00:02:06,000 --> 00:02:08,000
he still felt the need to disrobe.

29
00:02:08,000 --> 00:02:13,000
So I can't say that nothing would cause me to disrobe.

30
00:02:13,000 --> 00:02:17,000
But I think there are two pertinent questions because it points to the fact that

31
00:02:17,000 --> 00:02:22,000
you would have to see something, some benefit in the lay life to disrobe.

32
00:02:22,000 --> 00:02:28,000
People tend to make a mistake about disrobing that it's giving up something.

33
00:02:28,000 --> 00:02:32,000
You're giving up the path.

34
00:02:32,000 --> 00:02:39,000
It's not a mistake, but it's really only one way of looking at the issue.

35
00:02:39,000 --> 00:02:42,000
Because disrobing doesn't just mean taking off the robes.

36
00:02:42,000 --> 00:02:44,000
I mean if it just means taking off the robes,

37
00:02:44,000 --> 00:02:48,000
you become a naked aesthetic or something or become an animal again.

38
00:02:48,000 --> 00:02:58,000
But it means taking off the robes and generally means putting on another set of culturally

39
00:02:58,000 --> 00:03:02,000
and role-specific clothing.

40
00:03:02,000 --> 00:03:07,000
Because when you put on pants, it really does have quite a meaning.

41
00:03:07,000 --> 00:03:13,000
The pants are not the default and the t-shirt or whatever it is that you end up wearing.

42
00:03:13,000 --> 00:03:15,000
Or not the default.

43
00:03:15,000 --> 00:03:19,000
In fact, you might think that robes are much more a default clothing.

44
00:03:19,000 --> 00:03:22,000
And that's why the Buddha chose them.

45
00:03:22,000 --> 00:03:28,000
So to become a lay person again,

46
00:03:28,000 --> 00:03:34,000
I would think you would have to see some benefit in the lay life.

47
00:03:34,000 --> 00:03:38,000
You would have to think that some other was a benefit to lay life.

48
00:03:38,000 --> 00:03:40,000
Do I see any benefit in the lay life?

49
00:03:40,000 --> 00:03:46,000
Besides the benefit that I just mentioned, taking care of your parents,

50
00:03:46,000 --> 00:03:51,000
which I'm still skeptical of because in many cases you can,

51
00:03:51,000 --> 00:03:53,000
as a monk, take care of your parents.

52
00:03:53,000 --> 00:03:55,000
You can't do everything for them.

53
00:03:55,000 --> 00:04:00,000
But for myself, I wouldn't probably have such a concern.

54
00:04:00,000 --> 00:04:02,000
I can't say that.

55
00:04:02,000 --> 00:04:04,000
You never know what the future is going to bring.

56
00:04:04,000 --> 00:04:11,000
But there are always ways to even as a monk to support your parents.

57
00:04:11,000 --> 00:04:18,000
For example, given that I have a number of students who appreciate what I do

58
00:04:18,000 --> 00:04:22,000
and wish for me to continue doing what I do,

59
00:04:22,000 --> 00:04:29,000
I can explain to them if there is a desire for me to remain a monk.

60
00:04:29,000 --> 00:04:34,000
Then maybe I can have some people help me to help my parents.

61
00:04:34,000 --> 00:04:35,000
This is an example.

62
00:04:35,000 --> 00:04:39,000
So in my case, it's not that pressing of a concern,

63
00:04:39,000 --> 00:04:41,000
or not that big of a worry.

64
00:04:41,000 --> 00:04:46,000
I might someday want to go back to Canada to be closer to them.

65
00:04:46,000 --> 00:04:48,000
I'm always thinking of that,

66
00:04:48,000 --> 00:04:51,000
but that's, I think, another story.

67
00:04:51,000 --> 00:04:53,000
Obviously it wouldn't be to disroven,

68
00:04:53,000 --> 00:04:57,000
and I don't have any thought in that direction.

69
00:04:57,000 --> 00:05:02,000
But seeing a benefit, right, besides taking care of your parents,

70
00:05:02,000 --> 00:05:06,000
seeing a benefit in the lay life, no,

71
00:05:06,000 --> 00:05:11,000
no, I don't see any benefit in the lay life.

72
00:05:11,000 --> 00:05:16,000
It really speaks of where your mind is.

73
00:05:16,000 --> 00:05:23,000
The point is that personally, I don't see a great benefit in, for example,

74
00:05:23,000 --> 00:05:27,000
supporting or contributing to society,

75
00:05:27,000 --> 00:05:30,000
because I think maybe this is something that keeps people as lay people

76
00:05:30,000 --> 00:05:33,000
and makes them disroven, because they want to go

77
00:05:33,000 --> 00:05:36,000
and do good things in the world, and they feel like as a monk,

78
00:05:36,000 --> 00:05:38,000
or a nun that they're not able to do that,

79
00:05:38,000 --> 00:05:43,000
which I think is really, or I don't see things in that way.

80
00:05:43,000 --> 00:05:49,000
So as a result, I probably wouldn't follow such a thought.

81
00:05:49,000 --> 00:05:52,000
Because from my point of view,

82
00:05:52,000 --> 00:05:55,000
the best thing that helps people the most is meditation,

83
00:05:55,000 --> 00:05:57,000
and the practice of the Buddha's teaching.

84
00:05:57,000 --> 00:06:02,000
I mean, even today, just the wonderful things that went on in this ceremony,

85
00:06:02,000 --> 00:06:07,000
again and again, it's such a wonderful thing to give,

86
00:06:07,000 --> 00:06:11,000
and to be a part of giving, and even not to give,

87
00:06:11,000 --> 00:06:14,000
but to rejoice in the giving of others,

88
00:06:14,000 --> 00:06:19,000
to watch other people give and be generous and be kind and be helpful.

89
00:06:19,000 --> 00:06:25,000
That's just a wonderful thing, and it's a wonderful thing that becomes so very powerful

90
00:06:25,000 --> 00:06:29,000
in a Buddhist context, because it's our spiritual practice.

91
00:06:29,000 --> 00:06:34,000
Of course, it would be the same in, I suppose, in a Christian context,

92
00:06:34,000 --> 00:06:37,000
or Jewish context, whatever context,

93
00:06:37,000 --> 00:06:40,000
Muslim context, the Hindu context is giving.

94
00:06:40,000 --> 00:06:47,000
Well, of course, given our belief or our view,

95
00:06:47,000 --> 00:06:53,000
our opinion that the Buddha's teaching as much is the purest.

96
00:06:53,000 --> 00:06:57,000
Of course, because as very much the core of the Buddha's teaching

97
00:06:57,000 --> 00:07:00,000
is to do good deeds and to avoid bad deeds.

98
00:07:00,000 --> 00:07:05,000
There's no overhead.

99
00:07:05,000 --> 00:07:09,000
There's no extraneous.

100
00:07:09,000 --> 00:07:15,000
No extraneous views or opinions or beliefs or practices.

101
00:07:15,000 --> 00:07:17,000
Practice is really to make yourself a better person,

102
00:07:17,000 --> 00:07:24,000
to make your mind more pure by giving gifts and by keeping morality.

103
00:07:24,000 --> 00:07:27,000
This is really our spiritual practice.

104
00:07:27,000 --> 00:07:31,000
This is the wonderful thing,

105
00:07:31,000 --> 00:07:35,000
is that this is people getting together to do this in a spiritual sense.

106
00:07:35,000 --> 00:07:40,000
You can only really get that in a community like this,

107
00:07:40,000 --> 00:07:43,000
or it becomes much more powerful in a community like this,

108
00:07:43,000 --> 00:07:45,000
in a monastic community.

109
00:07:45,000 --> 00:07:54,000
There's no question in my mind that the benefit of being a monk is the greater benefit

110
00:07:54,000 --> 00:07:57,000
to oneself and to others, to the whole world.

111
00:07:57,000 --> 00:08:01,000
Even just as an example, even just as someone,

112
00:08:01,000 --> 00:08:04,000
it's a very powerful thing to be an example,

113
00:08:04,000 --> 00:08:09,000
to see someone doing good things,

114
00:08:09,000 --> 00:08:11,000
to see someone meditating.

115
00:08:11,000 --> 00:08:15,000
When we're sitting around and suddenly we see one of us is meditating.

116
00:08:15,000 --> 00:08:17,000
We all remember, oh yes,

117
00:08:17,000 --> 00:08:19,000
it brings us back to our meditation,

118
00:08:19,000 --> 00:08:21,000
but it can really change your life.

119
00:08:21,000 --> 00:08:23,000
And of course, as changed many people's lives,

120
00:08:23,000 --> 00:08:26,000
just to see other people ordained.

121
00:08:26,000 --> 00:08:28,000
When we go on arms round,

122
00:08:28,000 --> 00:08:31,000
or when people come to the monastery and see us, they're so happy.

123
00:08:31,000 --> 00:08:36,000
And it really changes people.

124
00:08:36,000 --> 00:08:43,000
So I don't see benefit because I'm not really concerned about society,

125
00:08:43,000 --> 00:08:45,000
or the direction that it's taking,

126
00:08:45,000 --> 00:08:48,000
or the benefits towards society.

127
00:08:48,000 --> 00:08:51,000
I'm concerned, I suppose, or concerned,

128
00:08:51,000 --> 00:09:00,000
I'm interested, or I don't know how to put it correctly.

129
00:09:00,000 --> 00:09:04,000
I enjoy, I suppose, and appreciate,

130
00:09:04,000 --> 00:09:09,000
and do as a matter of course,

131
00:09:09,000 --> 00:09:13,000
try to help people, help humanity.

132
00:09:13,000 --> 00:09:15,000
But I don't think that has anything to do with the lay life.

133
00:09:15,000 --> 00:09:17,000
I don't think the lay life is anything.

134
00:09:17,000 --> 00:09:19,000
The benefits people, because of course,

135
00:09:19,000 --> 00:09:20,000
when you put on the pants,

136
00:09:20,000 --> 00:09:21,000
when you put on the shirt,

137
00:09:21,000 --> 00:09:22,000
when you go to get a job,

138
00:09:22,000 --> 00:09:27,000
you're taking part in many of the things that we see as being the problem,

139
00:09:27,000 --> 00:09:31,000
the consumerism and materialism.

140
00:09:31,000 --> 00:09:35,000
And just so much uselessness that goes on out there,

141
00:09:35,000 --> 00:09:39,000
even just our interaction with people who come here,

142
00:09:39,000 --> 00:09:43,000
the comparison, what they get from us,

143
00:09:43,000 --> 00:09:48,000
and what we pick up from them is really two different things.

144
00:09:48,000 --> 00:09:53,000
So what we get from them is their greed and their stress

145
00:09:53,000 --> 00:09:57,000
and their frustrations and sadness

146
00:09:57,000 --> 00:10:00,000
and suffering that they have in their lives

147
00:10:00,000 --> 00:10:04,000
and living in the world.

148
00:10:04,000 --> 00:10:08,000
And when you compare it with the peace and the happiness

149
00:10:08,000 --> 00:10:11,000
and the simplicity of the life that we lead,

150
00:10:11,000 --> 00:10:16,000
it's really not difficult to make a choice.

151
00:10:16,000 --> 00:10:18,000
I would say the biggest reason,

152
00:10:18,000 --> 00:10:21,000
the only really powerful reason

153
00:10:21,000 --> 00:10:24,000
that leads people to disrupt is sensuality

154
00:10:24,000 --> 00:10:29,000
or sexuality to put it bluntly.

155
00:10:29,000 --> 00:10:31,000
I suppose you could expand that to sensuality.

156
00:10:31,000 --> 00:10:35,000
Some people are just attached to their food,

157
00:10:35,000 --> 00:10:38,000
for example.

158
00:10:38,000 --> 00:10:41,000
Funny how silly people can be wanting this food

159
00:10:41,000 --> 00:10:43,000
or wanting that food and knowing that

160
00:10:43,000 --> 00:10:46,000
as a monk, it's not convenient to get it,

161
00:10:46,000 --> 00:10:49,000
entertainment and so on.

162
00:10:49,000 --> 00:10:51,000
But at the core, I would say sexuality

163
00:10:51,000 --> 00:10:54,000
and it's something that leads people to not be able to ordain.

164
00:10:54,000 --> 00:10:59,000
Now, I've, you know, been down there that road

165
00:10:59,000 --> 00:11:03,000
and I'm happy to discuss that.

166
00:11:03,000 --> 00:11:07,000
The question of sexuality, it's really a,

167
00:11:07,000 --> 00:11:12,000
that's one of the more wonderful benefits

168
00:11:12,000 --> 00:11:16,000
of being a Buddhist monk

169
00:11:16,000 --> 00:11:22,000
is to look objectively at the sexual desire

170
00:11:22,000 --> 00:11:25,000
and the sexual impulse

171
00:11:25,000 --> 00:11:28,000
and to come to break it up as I've said in other videos

172
00:11:28,000 --> 00:11:30,000
and to see the pieces of it,

173
00:11:30,000 --> 00:11:32,000
to see the pleasure

174
00:11:32,000 --> 00:11:35,000
and to finally,

175
00:11:35,000 --> 00:11:37,000
because even as laypeople we repress

176
00:11:37,000 --> 00:11:41,000
and we feel guilty about our sexual desires

177
00:11:41,000 --> 00:11:43,000
and it's something that you have to do in private

178
00:11:43,000 --> 00:11:46,000
and something that you have to hide.

179
00:11:46,000 --> 00:11:54,000
But we still, even though we might say

180
00:11:54,000 --> 00:11:57,000
a Buddhist monk is someone who's repressing these desires,

181
00:11:57,000 --> 00:11:59,000
it's actually the opposite.

182
00:11:59,000 --> 00:12:04,000
You finally have a forum in which you can approach these,

183
00:12:04,000 --> 00:12:06,000
these issues.

184
00:12:06,000 --> 00:12:08,000
You know, you can sit in your kutti

185
00:12:08,000 --> 00:12:11,000
and you can, you know, not have to feel guilty

186
00:12:11,000 --> 00:12:13,000
and say, you know, I know I'm not going to follow it,

187
00:12:13,000 --> 00:12:15,000
I'm just sitting here and I have this desire

188
00:12:15,000 --> 00:12:16,000
and so on.

189
00:12:16,000 --> 00:12:19,000
And so actually it winds up being the opposite.

190
00:12:19,000 --> 00:12:21,000
But you know, finally you have a chance to say,

191
00:12:21,000 --> 00:12:24,000
yes, I have this sexual desire,

192
00:12:24,000 --> 00:12:27,000
I have this pleasure arising in the body,

193
00:12:27,000 --> 00:12:30,000
all these chemical reactions and so on.

194
00:12:30,000 --> 00:12:32,000
Anyway, that's not really what the question was about,

195
00:12:32,000 --> 00:12:43,000
but for myself I think I've been lucky

196
00:12:43,000 --> 00:12:57,000
to have good teachers and I'm able to look,

197
00:12:57,000 --> 00:13:01,000
honestly, at the issues that are inside myself.

198
00:13:01,000 --> 00:13:04,000
So, you know, these issues that where you might say,

199
00:13:04,000 --> 00:13:07,000
I know it's wrong, but I have to disrupt.

200
00:13:07,000 --> 00:13:11,000
It seems to me that when you look,

201
00:13:11,000 --> 00:13:14,000
you really look at them and when you are honest about them

202
00:13:14,000 --> 00:13:17,000
that they'll cease to become a problem

203
00:13:17,000 --> 00:13:20,000
and they simply become another object of meditation.

204
00:13:20,000 --> 00:13:25,000
So, a good question and these questions are always interesting,

205
00:13:25,000 --> 00:13:28,000
I think, for people because there's a lot of people out there

206
00:13:28,000 --> 00:13:30,000
looking to ordain or, you know, sorry,

207
00:13:30,000 --> 00:13:32,000
considering ordination.

208
00:13:32,000 --> 00:13:37,000
So, you know, interesting to talk about.

209
00:13:37,000 --> 00:13:39,000
And these are also the videos that become very contentious

210
00:13:39,000 --> 00:13:41,000
because half the people say wonderful,

211
00:13:41,000 --> 00:13:42,000
wonderful, I agree completely.

212
00:13:42,000 --> 00:13:46,000
And other people say, how can you denounce society and lay life?

213
00:13:46,000 --> 00:13:48,000
And the Buddha said that lay people

214
00:13:48,000 --> 00:13:50,000
could become enlightened as well and so on and so on.

215
00:13:50,000 --> 00:13:53,000
So, there's nothing wrong with being a lay person

216
00:13:53,000 --> 00:13:55,000
and you can practice meditation,

217
00:13:55,000 --> 00:14:00,000
but there's no question that from Buddhist point of view,

218
00:14:00,000 --> 00:14:05,000
the monastic life or the simple meditative life

219
00:14:05,000 --> 00:14:12,000
is to be, you know, you could say,

220
00:14:12,000 --> 00:14:16,000
really could say the monastic life is of greater benefit.

221
00:14:16,000 --> 00:14:20,000
So, you're giving up something that is of lesser benefit

222
00:14:20,000 --> 00:14:23,000
or is of lesser use or is less conducive

223
00:14:23,000 --> 00:14:25,000
for the practice of meditation

224
00:14:25,000 --> 00:14:28,000
because, of course, the monastic community has its benefits as well

225
00:14:28,000 --> 00:14:32,000
even though lay people can live meditative simple lives.

226
00:14:32,000 --> 00:14:34,000
It's very easy for them to get off track

227
00:14:34,000 --> 00:14:39,000
and to get lost because of the lack of discipline

228
00:14:39,000 --> 00:14:45,000
and community which is gained from being a Buddhist monastic.

229
00:14:45,000 --> 00:14:47,000
So, those are my thoughts.

230
00:14:47,000 --> 00:15:11,000
Just some thoughts.

