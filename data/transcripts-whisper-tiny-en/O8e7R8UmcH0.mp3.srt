1
00:00:00,000 --> 00:00:05,000
Hello and welcome again to our study of the Dhamal Pada.

2
00:00:05,000 --> 00:00:12,000
Today we go on to verses number 26 in 27, which read as follows.

3
00:00:35,000 --> 00:00:51,000
The Dhamal Pada is going to be mostly about the practice of meditation.

4
00:00:51,000 --> 00:01:00,000
Because the word upamanda, we translate as heedfulness, but it's really the core of what is meant by meditation, or meditation practice and Buddhism.

5
00:01:00,000 --> 00:01:05,000
We translate it directly by the Buddha as mindfulness.

6
00:01:05,000 --> 00:01:15,000
The meaning of the word upamanda is to be mindful all the time, to never be without mindfulness or without setting, without remembrance or recollection.

7
00:01:15,000 --> 00:01:20,000
So, the meaning of the direct meaning of these verses, pamada, manu yun jantim.

8
00:01:20,000 --> 00:01:33,000
They indulge or they partake of heedlessness or negligence, the fools and the unwise people.

9
00:01:33,000 --> 00:01:48,000
But those who are wise may not be the wise partake in upamanda, partake in heedfulness.

10
00:01:48,000 --> 00:01:57,000
And guard it, rakati, dhanang satan like a high or a special treasure.

11
00:01:57,000 --> 00:02:05,000
So, the wise, guard mindfulness or heedfulness as a great treasure.

12
00:02:05,000 --> 00:02:15,000
Then the Buddha says, mama, pamada, dhanu, pamada, manu yun jeta. Don't engage in heedlessness and negligence.

13
00:02:15,000 --> 00:02:28,000
Makama, rakati, santava, don't partake or indulge in or don't become intoxicated by sensuality.

14
00:02:28,000 --> 00:02:42,000
Because those who only those who are heedful and meditating attain great happiness or attain a profound happiness.

15
00:02:42,000 --> 00:02:48,000
Babu ti, vipulang sukhang, a profound happiness.

16
00:02:48,000 --> 00:02:52,000
So, this is a very important teaching to understand.

17
00:02:52,000 --> 00:02:58,000
It's something that really cuts right to the heart of our attachments in the worldly spirit.

18
00:02:58,000 --> 00:03:01,000
A really good story, very short story.

19
00:03:01,000 --> 00:03:08,000
But the story goes that insawati, there was a holiday.

20
00:03:08,000 --> 00:03:13,000
And it kind of seems like a strange holiday, but there are parallels even in modern times.

21
00:03:13,000 --> 00:03:18,000
It was a holiday where anything went, anything was allowed.

22
00:03:18,000 --> 00:03:24,000
So, no longer there was no sense of respect for anyone.

23
00:03:24,000 --> 00:03:34,000
The people who are took in this holiday would abandon all sense of respect for mother, father, for boss, for teacher and so on.

24
00:03:34,000 --> 00:03:40,000
And they would go from house to house, probably drunk, and abuse and revile everyone.

25
00:03:40,000 --> 00:03:45,000
You pig, you cow, you go calling them all sorts of horrible names.

26
00:03:45,000 --> 00:03:50,000
And the game was that if you wanted them to go away, you had to give them stuff.

27
00:03:50,000 --> 00:03:54,000
So, it was like trick or treat, basically, like Halloween, except they really did it.

28
00:03:54,000 --> 00:03:56,000
They were really mean and nasty.

29
00:03:56,000 --> 00:04:05,000
And they would stay there until you give them some money, or some wine, or whatever, food, and then they would go away.

30
00:04:05,000 --> 00:04:12,000
And so, the thing was that insawati, there was a great proportion of people who were enlightened.

31
00:04:12,000 --> 00:04:16,000
So, they were so tapana, saki, takami, you know, at some stage of enlightenment.

32
00:04:16,000 --> 00:04:19,000
So, they obviously wouldn't partake in such a festival.

33
00:04:19,000 --> 00:04:22,000
And so, they had a horrible time for seven days.

34
00:04:22,000 --> 00:04:27,000
And they asked the Buddha not to come to sawati, because if the Buddha went there, they would revile the Buddha.

35
00:04:27,000 --> 00:04:29,000
And that would be very bad karma for them.

36
00:04:29,000 --> 00:04:33,000
So, they said, the Buddha, please stay and stay at the one, and we'll bring you food every day.

37
00:04:33,000 --> 00:04:35,000
So, they brought food for seven days.

38
00:04:35,000 --> 00:04:38,000
And then after the seventh day, they came to see the Buddha and said,

39
00:04:38,000 --> 00:04:42,000
oh, venerable sir, we stayed so awful for these seven days.

40
00:04:42,000 --> 00:04:47,000
It was so, so, so tiring for us to have to put up with this silliness.

41
00:04:47,000 --> 00:04:49,000
Why are people like this?

42
00:04:49,000 --> 00:04:54,000
Why do people act in such a way?

43
00:04:54,000 --> 00:04:56,000
Isn't it crazy that they do?

44
00:04:56,000 --> 00:04:58,000
And the Buddha said, oh, yes.

45
00:04:58,000 --> 00:05:04,000
This is the difference between those who are enlightened, or those who have understood the truth,

46
00:05:04,000 --> 00:05:07,000
and those who are still mired in delusion.

47
00:05:07,000 --> 00:05:12,000
And so, the first verse here, and then he told these verses, and the first verse,

48
00:05:12,000 --> 00:05:15,000
explains the difference between the two.

49
00:05:15,000 --> 00:05:20,000
That an ordinary person, a person who has no idea about what is right,

50
00:05:20,000 --> 00:05:23,000
and what is wrong, and has never contemplated these things,

51
00:05:23,000 --> 00:05:27,000
or investigated what might be the truth,

52
00:05:27,000 --> 00:05:36,000
what the things that they value are the most base and useless qualities.

53
00:05:36,000 --> 00:05:40,000
For instance, negligence or drunkenness.

54
00:05:40,000 --> 00:05:45,000
So, an ordinary person and people in ordinary life will actually,

55
00:05:45,000 --> 00:05:50,000
they will, the things that they will cherish are the times when they were the least mindful,

56
00:05:50,000 --> 00:05:53,000
and the least clear in their mind.

57
00:05:53,000 --> 00:05:56,000
So, when you think in lay life, what people,

58
00:05:56,000 --> 00:05:59,000
or not in lay life, for people who haven't practiced meditation,

59
00:05:59,000 --> 00:06:01,000
the kind of things that they cherish.

60
00:06:01,000 --> 00:06:04,000
They cherish those times when they were drunk, or when they did something silly,

61
00:06:04,000 --> 00:06:08,000
and they were having a great time, and doing all sorts of crazy things,

62
00:06:08,000 --> 00:06:12,000
when they were young and crazy, and how wonderful it was, and so on.

63
00:06:12,000 --> 00:06:15,000
They cherish these times when they do the craziest things,

64
00:06:15,000 --> 00:06:18,000
and so people who, for instance, in this festival,

65
00:06:18,000 --> 00:06:21,000
they would always think back to, oh, it wasn't at a wonderful time,

66
00:06:21,000 --> 00:06:25,000
where we could say whatever we want, didn't we have a great time then?

67
00:06:25,000 --> 00:06:28,000
And they make it out to be this wonderful thing.

68
00:06:28,000 --> 00:06:32,000
And a person who is in, a person who has actually taken the time to see

69
00:06:32,000 --> 00:06:38,000
the consequences of our actions and the consequences of every individual mind-stinct,

70
00:06:38,000 --> 00:06:43,000
is totally the opposite, and actually cherish us as a treasure,

71
00:06:43,000 --> 00:06:46,000
those times when they are a cogent, those times when they are aware.

72
00:06:46,000 --> 00:06:51,000
And one of the big reasons for this, I think, is that a person who has no idea

73
00:06:51,000 --> 00:06:58,000
or no skill in understanding experience will have a very difficult time,

74
00:06:58,000 --> 00:07:00,000
just being with reality.

75
00:07:00,000 --> 00:07:03,000
Reality is the worst thing for them. The last thing they want to focus on,

76
00:07:03,000 --> 00:07:06,000
because it's unpleasant, it's difficult to deal with,

77
00:07:06,000 --> 00:07:09,000
so pain comes up in their, in tears in their eyes,

78
00:07:09,000 --> 00:07:13,000
and when someone's saying things to them, and they don't like,

79
00:07:13,000 --> 00:07:16,000
and they get angry, when they have to do work,

80
00:07:16,000 --> 00:07:21,000
and then they get become frustrated or bored or depressed or so on.

81
00:07:21,000 --> 00:07:25,000
The last thing they want to do is to focus on reality.

82
00:07:25,000 --> 00:07:35,000
And so the whole goal or the point in Buddhism in Buddhist teaching

83
00:07:35,000 --> 00:07:41,000
is to come back to reality and to find a way to be happy

84
00:07:41,000 --> 00:07:50,000
at all times in any situation, not have our happiness depend on some specific circumstance,

85
00:07:50,000 --> 00:07:56,000
and not have it required some specific situation.

86
00:07:56,000 --> 00:08:00,000
Because when your happiness depends on something, it becomes an addiction,

87
00:08:00,000 --> 00:08:06,000
and it becomes a partiality where you aren't able to accept the rest of reality,

88
00:08:06,000 --> 00:08:13,000
because you're thinking about or wanting or striving after a certain experience.

89
00:08:13,000 --> 00:08:21,000
And the experience has to become further and more and more extravagant

90
00:08:21,000 --> 00:08:23,000
in order to satisfy the cravings.

91
00:08:23,000 --> 00:08:29,000
Because our pleasure stimulus isn't a static system.

92
00:08:29,000 --> 00:08:32,000
When you get what you want, you want more of it.

93
00:08:32,000 --> 00:08:34,000
When you get more of it, you want even more,

94
00:08:34,000 --> 00:08:37,000
and it gets builds and builds and builds until you,

95
00:08:37,000 --> 00:08:41,000
the only way you can be happy is to have some crazy festival like this

96
00:08:41,000 --> 00:08:45,000
where you go and do something because you're repressing these desires,

97
00:08:45,000 --> 00:08:49,000
you're repressing your greed, your anger and so on.

98
00:08:49,000 --> 00:08:55,000
I heard about this one society, this one group of people

99
00:08:55,000 --> 00:09:02,000
where they will engage in group sex.

100
00:09:02,000 --> 00:09:08,000
They will get all together and have a big orgy, just like animals.

101
00:09:08,000 --> 00:09:12,000
And for them, this may sound coarse and rude to say,

102
00:09:12,000 --> 00:09:14,000
but it's actually a philosophy that they have that,

103
00:09:14,000 --> 00:09:17,000
well, if this is a way to find pleasure,

104
00:09:17,000 --> 00:09:24,000
then you should strive to indulge in it to the most extreme extent possible.

105
00:09:24,000 --> 00:09:26,000
And so it's a philosophy that they have.

106
00:09:26,000 --> 00:09:29,000
And the idea being that as long as you can get it,

107
00:09:29,000 --> 00:09:32,000
then what's wrong with it, because you can be happy all the time,

108
00:09:32,000 --> 00:09:34,000
this is the idea.

109
00:09:34,000 --> 00:09:37,000
And this is for people who don't see what they're doing to their mind.

110
00:09:37,000 --> 00:09:38,000
They think it's somehow static.

111
00:09:38,000 --> 00:09:40,000
Well, if I wanted any time, I can get it.

112
00:09:40,000 --> 00:09:42,000
And then if I don't want it, I can come back and not have it.

113
00:09:42,000 --> 00:09:45,000
Without seeing the effect that it's having on your mind

114
00:09:45,000 --> 00:09:49,000
and on the pleasure, stimulus systems in the brain and so on,

115
00:09:49,000 --> 00:09:52,000
and how actually you're creating an addiction,

116
00:09:52,000 --> 00:09:55,000
and you're creating an intense partiality.

117
00:09:55,000 --> 00:09:57,000
This is an example of an extreme state.

118
00:09:57,000 --> 00:10:00,000
There are societies where they don't allow this sort of thing,

119
00:10:00,000 --> 00:10:10,000
and where you have to be totally formal and polite and proper at all times,

120
00:10:10,000 --> 00:10:14,000
conservative, ethical, and so on.

121
00:10:14,000 --> 00:10:21,000
And any kind of showing of emotion is considered bad form.

122
00:10:21,000 --> 00:10:24,000
But then they still have these as ours,

123
00:10:24,000 --> 00:10:26,000
and they repressed them and repressed them and pressed them.

124
00:10:26,000 --> 00:10:28,000
And then they have these festivals.

125
00:10:28,000 --> 00:10:32,000
So they're, I've heard places where everything has to be proper,

126
00:10:32,000 --> 00:10:36,000
and everyone has a role and a hierarchy and so on.

127
00:10:36,000 --> 00:10:39,000
But then they have these parties where they get drunk and anything goes.

128
00:10:39,000 --> 00:10:42,000
And like you have, I heard about the boss getting together with,

129
00:10:42,000 --> 00:10:45,000
you know, having crazy things.

130
00:10:45,000 --> 00:10:47,000
And so these are the things that I hear in the world.

131
00:10:47,000 --> 00:10:49,000
And these things really do go on.

132
00:10:49,000 --> 00:10:54,000
I mean, it's actually kind of embarrassing to talk about these things,

133
00:10:54,000 --> 00:10:56,000
but this is what's really happening in the world.

134
00:10:56,000 --> 00:11:04,000
People are so lost that they actually think these things are a cause for happiness.

135
00:11:04,000 --> 00:11:09,000
And so there are scientists and researchers who have studied this,

136
00:11:09,000 --> 00:11:13,000
and see that it really goes step-by-step by step into finally.

137
00:11:13,000 --> 00:11:18,000
It becomes such a perverse thing where you're involving these groups,

138
00:11:18,000 --> 00:11:25,000
or even they were saying on this one article that I read.

139
00:11:25,000 --> 00:11:29,000
It eventually gets to with animals and so on.

140
00:11:29,000 --> 00:11:32,000
Where you find, this is where you find your pleasure.

141
00:11:32,000 --> 00:11:35,000
And so this is the kind of thing that we would have seen in Sabati.

142
00:11:35,000 --> 00:11:38,000
Of course, it just talks about reviling people.

143
00:11:38,000 --> 00:11:44,000
But for sure that they had other festivals in regards to sensuality.

144
00:11:44,000 --> 00:11:47,000
And this is where the whole Gama Sutra came up and so on.

145
00:11:47,000 --> 00:11:51,000
And this is how people in the world think that they can find happiness.

146
00:11:51,000 --> 00:11:55,000
So there are two ways that we understand this.

147
00:11:55,000 --> 00:11:59,000
For someone who practices meditation, it's obvious that these things don't lead to happiness.

148
00:11:59,000 --> 00:12:04,000
You engage in them, you indulge in them, and you find that you're just back where you started from.

149
00:12:04,000 --> 00:12:12,000
You're still unhappy inside, you're still unsatisfied, even with these things.

150
00:12:12,000 --> 00:12:19,000
But really, the idea is that we're trying to find happiness here and now.

151
00:12:19,000 --> 00:12:25,000
Even if you can say that, well, I'm maybe unhappy here, but when I get what I want, at that time I'm happy.

152
00:12:25,000 --> 00:12:27,000
So what's wrong with it? I'm getting a happiness.

153
00:12:27,000 --> 00:12:29,000
At the time when I get it, there's pleasure.

154
00:12:29,000 --> 00:12:38,000
But the point is that the whole fact that you bear a fact that you want that is a sign that you're not happy.

155
00:12:38,000 --> 00:12:41,000
If you were happy, you wouldn't need to strive for something.

156
00:12:41,000 --> 00:12:43,000
You wouldn't need to go for something.

157
00:12:43,000 --> 00:12:46,000
You wouldn't need to build something. You wouldn't need to create something.

158
00:12:46,000 --> 00:12:50,000
To attain something, or get something.

159
00:12:50,000 --> 00:12:58,000
So the radical task that we have is to find happiness without getting that, without getting anything.

160
00:12:58,000 --> 00:13:00,000
To find happiness here and now.

161
00:13:00,000 --> 00:13:05,000
And it's actually a really a simple thing, and if you think about it, it's how it should be.

162
00:13:05,000 --> 00:13:07,000
That we should find happiness here and now.

163
00:13:07,000 --> 00:13:10,000
Why should our happiness depend on something over there?

164
00:13:10,000 --> 00:13:13,000
Why can't our happiness be here and now?

165
00:13:13,000 --> 00:13:18,000
It's a very valid question, but it's one that we never ask and we always overlook.

166
00:13:18,000 --> 00:13:22,000
We think, obviously, if you want happiness, you have to strive for it.

167
00:13:22,000 --> 00:13:23,000
You have to go for it.

168
00:13:23,000 --> 00:13:27,000
Well, the truth is that even to find happiness here and now you have to strive for it.

169
00:13:27,000 --> 00:13:37,000
And so this is why actually often people will find that the practice of meditation is something that is uncomfortable and unpleasant.

170
00:13:37,000 --> 00:13:40,000
The point being, because you always want to go out for something.

171
00:13:40,000 --> 00:13:45,000
You want to go after that and you're trying to train yourself out of that.

172
00:13:45,000 --> 00:13:49,000
So sometimes you stop yourself and you say, no, and then you feel unhappy.

173
00:13:49,000 --> 00:13:52,000
Or sometimes you go after it and then you feel guilty or something.

174
00:13:52,000 --> 00:14:03,000
Until you finally are able to change your mind and straighten your mind so that you're able to see the goal,

175
00:14:03,000 --> 00:14:17,000
the object of your desire and to simply see it for what it is and to be able to live with the perception of something without having to chase after it.

176
00:14:17,000 --> 00:14:21,000
So when you think of something that you normally would like, you're just thinking of it.

177
00:14:21,000 --> 00:14:26,000
When you hear a sound that you find pleasant, you just hear the sound.

178
00:14:26,000 --> 00:14:29,000
When you see something beautiful, you just see it.

179
00:14:29,000 --> 00:14:37,000
And you're able to experience happiness in that moment without having your happiness be only when you get that object.

180
00:14:37,000 --> 00:14:41,000
When you find yourself at peace here and now.

181
00:14:41,000 --> 00:14:43,000
But it's something that you have to strive for.

182
00:14:43,000 --> 00:14:45,000
So we can say that there are these two strivings.

183
00:14:45,000 --> 00:14:52,000
This is what the Buddha said, people who strive after pleasure and trying to find some object of happiness.

184
00:14:52,000 --> 00:14:56,000
And people who strive to be free from that, to find happiness here and now.

185
00:14:56,000 --> 00:14:57,000
It's really two different paths.

186
00:14:57,000 --> 00:15:01,000
It's not that we're striving to achieve something or get something.

187
00:15:01,000 --> 00:15:07,000
We're trying to stop striving to stop achieving, to stop chasing after.

188
00:15:07,000 --> 00:15:08,000
It's also very difficult.

189
00:15:08,000 --> 00:15:12,000
So these are the two paths and this is what the Buddha lays out here.

190
00:15:12,000 --> 00:15:16,000
That for ordinary people they will just let themselves go.

191
00:15:16,000 --> 00:15:22,000
Drink alcohol and take drugs and engage in all sorts of foolishness.

192
00:15:22,000 --> 00:15:24,000
Enjoying the happiness.

193
00:15:24,000 --> 00:15:29,000
But they don't see how it's changing their minds and they don't see how it's building these mindsings.

194
00:15:29,000 --> 00:15:31,000
We're not static beings.

195
00:15:31,000 --> 00:15:36,000
Another thing is this shows how important the Buddha's teaching of karma is.

196
00:15:36,000 --> 00:15:40,000
And it goes a lot deeper than we actually think because I don't have karma.

197
00:15:40,000 --> 00:15:41,000
You don't have karma.

198
00:15:41,000 --> 00:15:43,000
Karma is the truth of reality.

199
00:15:43,000 --> 00:15:45,000
There only is karma.

200
00:15:45,000 --> 00:15:47,000
We create ourselves out of karma.

201
00:15:47,000 --> 00:15:50,000
The whole of our being is created out of karma.

202
00:15:50,000 --> 00:15:52,000
The whole of our life is created out of karma.

203
00:15:52,000 --> 00:15:55,000
There's no eye. There's only the action.

204
00:15:55,000 --> 00:15:56,000
The things that we do.

205
00:15:56,000 --> 00:15:59,000
So everything that we do creates us.

206
00:15:59,000 --> 00:16:02,000
The reason why we get angry is because we've built up anger.

207
00:16:02,000 --> 00:16:07,000
The reason why we have greed and want things is because we build up greed and want things year after year.

208
00:16:07,000 --> 00:16:09,000
That's all we are.

209
00:16:09,000 --> 00:16:14,000
Where there's accumulation or these conglomeration of minds things.

210
00:16:14,000 --> 00:16:16,000
Both positive and negative.

211
00:16:16,000 --> 00:16:19,000
Why we have love and kindness is because we've developed this.

212
00:16:19,000 --> 00:16:24,000
Because we've come to see the truth because we've come to see how horrible it is to suffer.

213
00:16:24,000 --> 00:16:28,000
And so we don't want to see other beings suffer and so on.

214
00:16:28,000 --> 00:16:30,000
We are what we do.

215
00:16:30,000 --> 00:16:35,000
We are according to the things that we do and the qualities that we build up.

216
00:16:35,000 --> 00:16:39,000
So it's important to understand the difference here.

217
00:16:39,000 --> 00:16:45,000
Because I think all of us can see in our own lives in our own past this attachment to negligence.

218
00:16:45,000 --> 00:16:49,000
It's attachment to having fun and enjoying life and so on.

219
00:16:49,000 --> 00:16:55,000
Without seeing how it's creating addiction, how it's actually feeding our dissatisfaction.

220
00:16:55,000 --> 00:16:59,000
So it's making it harder for us to just be here and now.

221
00:16:59,000 --> 00:17:05,000
When we come back to be just here and now we see how radically opposed it that these two paths are.

222
00:17:05,000 --> 00:17:08,000
Being just being here and now is something totally different.

223
00:17:08,000 --> 00:17:14,000
And completely opposed in the sense that the more we strive to achieve happiness

224
00:17:14,000 --> 00:17:20,000
and drive to achieve pleasure, the harder it is for us to find happiness and peace here and now.

225
00:17:20,000 --> 00:17:25,000
The more we find peace and happiness here and now the less we want, the less we need of anything.

226
00:17:25,000 --> 00:17:31,000
If you think just where we are right here right now, when we find happiness here, everything else disappears.

227
00:17:31,000 --> 00:17:39,000
Suddenly the whole world, all of who we are, our lives, our friends, our family, all of our thoughts,

228
00:17:39,000 --> 00:17:47,000
all of our ambitions have just disappeared, vanished in there as the first verse.

229
00:17:47,000 --> 00:17:50,000
The second verse is an injunction by the Buddha.

230
00:17:50,000 --> 00:17:55,000
But once you see this, if you're a wise person, you should not engage in negligence.

231
00:17:55,000 --> 00:17:59,000
Don't engage in it because it will make it harder for you to find happiness.

232
00:17:59,000 --> 00:18:03,000
It will make you less happy, less satisfied, less at peace with yourself.

233
00:18:03,000 --> 00:18:10,000
Don't be, don't get caught up in delight in sensuality.

234
00:18:10,000 --> 00:18:12,000
This is what the Buddha says.

235
00:18:12,000 --> 00:18:17,000
Because this sensuality is really the worst of them.

236
00:18:17,000 --> 00:18:19,000
It's the most course of our desires.

237
00:18:19,000 --> 00:18:27,000
We may have ambitions or so on, but our desire for sensuality, sights and sounds and smells and tastes and feelings

238
00:18:27,000 --> 00:18:33,000
is the worst because it involves the body and it involves chemicals and hormones and so on,

239
00:18:33,000 --> 00:18:36,000
and dopamine and the brain and so on.

240
00:18:36,000 --> 00:18:43,000
This addiction cycle, which, as I said, is actually creating what we call sankara.

241
00:18:43,000 --> 00:18:46,000
It's forming our reality.

242
00:18:46,000 --> 00:18:49,000
It's changing who we are from the very core.

243
00:18:49,000 --> 00:18:51,000
It's not us doing this.

244
00:18:51,000 --> 00:18:53,000
This doing becomes us.

245
00:18:53,000 --> 00:18:56,000
This is who we become.

246
00:18:56,000 --> 00:19:00,000
He says, why should you follow what I say?

247
00:19:00,000 --> 00:19:02,000
Why should you not be negligent?

248
00:19:02,000 --> 00:19:05,000
Why should you not indulge in delight and sensuality?

249
00:19:05,000 --> 00:19:14,000
He says, because it's only the upper motto, only by being heatful, jaiyanto and meditating.

250
00:19:14,000 --> 00:19:17,000
Does one attain ba-po-ti?

251
00:19:17,000 --> 00:19:20,000
We put lungs up profoundness.

252
00:19:20,000 --> 00:19:25,000
The happiness here is the happiness of peace.

253
00:19:25,000 --> 00:19:28,000
The Buddha said, nutty, santi, ba-ankara.

254
00:19:28,000 --> 00:19:30,000
There's no happiness without peace.

255
00:19:30,000 --> 00:19:36,000
The happiness is that we call happiness, the pleasures in the world, the things that we say are our happiness,

256
00:19:36,000 --> 00:19:43,000
or our way to find true satisfaction, are not real happiness because they're impermanent.

257
00:19:43,000 --> 00:19:45,000
They change, they're dependent.

258
00:19:45,000 --> 00:19:50,000
They require some attainment that is also important.

259
00:19:50,000 --> 00:19:54,000
They require an object and they create partiality.

260
00:19:54,000 --> 00:19:58,000
They create their opposite, which is suffering and dissatisfaction.

261
00:19:58,000 --> 00:20:05,000
So one, the more one indulges, the more one goes back and forth between one and the other pleasure and pain.

262
00:20:05,000 --> 00:20:07,000
And one is never at peace.

263
00:20:07,000 --> 00:20:10,000
One is never able to just be here and now.

264
00:20:10,000 --> 00:20:16,000
One's life is always about seeking after this, running away from that, seeking,

265
00:20:16,000 --> 00:20:19,000
running, seeking, running, being pushed and pulled back and forth.

266
00:20:19,000 --> 00:20:24,000
And one can never just be here and now.

267
00:20:24,000 --> 00:20:31,000
One never has the ability or the safety, the stability, just being here and now.

268
00:20:31,000 --> 00:20:38,000
They can build up some kind of pleasant state of being for some time, and then see it washed away.

269
00:20:38,000 --> 00:20:43,000
At the very end, when they die, they will be in the state of loss because of their attachment,

270
00:20:43,000 --> 00:20:46,000
because of the things that they still are clinging to.

271
00:20:46,000 --> 00:20:50,000
So the profound happiness that the Buddha is talking about is peace here and now.

272
00:20:50,000 --> 00:20:55,000
Being able to be in here and now without any clinging or any attachment.

273
00:20:55,000 --> 00:20:57,000
So that's the teaching today.

274
00:20:57,000 --> 00:21:00,000
That's Damapada versus 26 and 27.

275
00:21:00,000 --> 00:21:02,000
Thank you for tuning in.

276
00:21:02,000 --> 00:21:06,000
I wish you all to find peace, happiness and freedom for yourself.

277
00:21:06,000 --> 00:21:21,000
Thank you.

