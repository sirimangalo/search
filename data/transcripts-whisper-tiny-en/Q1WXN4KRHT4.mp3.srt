1
00:00:00,000 --> 00:00:05,720
I dreamt about my great grandmother dying and going into a light and moaning.

2
00:00:05,720 --> 00:00:10,640
I had no idea she was dying as I was sleeping and I only found out she had died a

3
00:00:10,640 --> 00:00:13,080
week after I had the dream.

4
00:00:13,080 --> 00:00:19,760
This Buddhism have a view on things like this.

5
00:00:19,760 --> 00:00:20,760
Not really.

6
00:00:20,760 --> 00:00:29,200
Remember, Buddhism is primarily a practice.

7
00:00:29,200 --> 00:00:35,760
So ask yourself is this practical and no it's not.

8
00:00:35,760 --> 00:00:40,400
It fits in with the idea of cultivating magical powers.

9
00:00:40,400 --> 00:00:47,640
And so in so far as Buddhism deals with the cultivation of magical powers, it is related.

10
00:00:47,640 --> 00:00:49,600
Buddhism has a view on this.

11
00:00:49,600 --> 00:00:57,080
It's lumped in there with supernatural occurrences which can be cultivated.

12
00:00:57,080 --> 00:01:03,800
So you can have more than just this accidental or circumstantial experience.

13
00:01:03,800 --> 00:01:08,200
You can actually watch people die and be born and so on.

14
00:01:08,200 --> 00:01:16,800
You can contact spirits, ghosts, angels.

15
00:01:16,800 --> 00:01:25,640
So far away that kind of thing through the cultivation of magical powers which are not

16
00:01:25,640 --> 00:01:32,160
essential and not really, not an essential part of the path anyway.

17
00:01:32,160 --> 00:01:39,680
They're a byproduct, a beneficial, not a beneficial, a positive byproduct in a world

18
00:01:39,680 --> 00:01:47,640
really sense of the practice and have in the end no clear purpose.

19
00:01:47,640 --> 00:01:54,960
The best they could do is help to open up one's mind to the idea of the importance of the

20
00:01:54,960 --> 00:02:04,680
mind and the idea of the survival of the mind after the physical death, that kind of thing.

21
00:02:04,680 --> 00:02:13,680
But I have some thoughts on this, especially because it's prevalent, it's common, it's

22
00:02:13,680 --> 00:02:19,560
hard to find people who haven't heard such stories, many people have themselves experienced

23
00:02:19,560 --> 00:02:27,480
these sort of things and when they're told it's whatever some kind of confabulation or

24
00:02:27,480 --> 00:02:33,920
wishful thinking or so on, it just doesn't, I was even told when I was talking about

25
00:02:33,920 --> 00:02:38,400
my experience, someone just said, well, you're lying or I think you're lying, but it doesn't

26
00:02:38,400 --> 00:02:44,480
help me at all because I know I'm not lying and then when you have thousands or hundreds

27
00:02:44,480 --> 00:02:49,880
and thousands of these stories, near death experiences, experiences of people who have

28
00:02:49,880 --> 00:02:57,160
died or dying, it's common and quite common for it to be the case where the person who's

29
00:02:57,160 --> 00:03:01,480
having the experience had no idea that the person who's dying didn't even know the

30
00:03:01,480 --> 00:03:05,280
person was sick in some cases.

31
00:03:05,280 --> 00:03:14,720
Lots of strange things of this sort and to just disregard them, I think, it's a symptom

32
00:03:14,720 --> 00:03:30,680
of closed-mindedness, bigotry and scientism, scientism means clinging to, clinging to that

33
00:03:30,680 --> 00:03:38,440
which is known, that which is believed in the sense, that which is accepted as known

34
00:03:38,440 --> 00:03:44,080
or clinging to accepted theories and ideas of materialism in the most part, the idea

35
00:03:44,080 --> 00:03:50,760
that everything is physical and therefore these sorts of things can't happen, but Buddhism

36
00:03:50,760 --> 00:04:06,360
not really because it's not that practical.

