1
00:00:00,000 --> 00:00:27,000
So tonight is the full moon, and on the one hand you want to say it's just another day.

2
00:00:27,000 --> 00:00:51,000
As we have in the Pali thing, Sunakatang, Sukhalau, Sukhalau, Sukhalau, Sukhalau, Sukhalau,

3
00:00:51,000 --> 00:01:00,000
Sukhalau, Sukhalau, Sukhalau, Sukhalau, Sukhalau.

4
00:01:00,000 --> 00:01:29,000
When we live, whenever we live, whatever moment we're living the holy life for the spiritual life.

5
00:01:29,000 --> 00:01:44,000
And we perform wholesome deans of thought, wholesome deans of speech, wholesome deans of body.

6
00:01:44,000 --> 00:02:05,000
This is a auspicious day. This is an auspicious day, auspicious day auspicious moment.

7
00:02:05,000 --> 00:02:15,000
This is an auspicious instant, that instant is auspicious.

8
00:02:15,000 --> 00:02:29,000
We remember this in the real reminder ourselves that it's not the time or the place or the environment,

9
00:02:29,000 --> 00:02:40,000
but it's from the actions and the speech and the leads, the actions, speech and thoughts that we perform.

10
00:02:40,000 --> 00:02:52,000
This is what makes something auspicious, this is what makes something special.

11
00:02:52,000 --> 00:03:21,000
Put on the other hand, you do kind of have to admit,

12
00:03:21,000 --> 00:03:41,000
meant this time and this place and this occasion with this environment.

13
00:03:41,000 --> 00:04:04,000
We're very lucky to be here and this is a very auspicious time for us.

14
00:04:04,000 --> 00:04:15,000
My teacher, when New Year came around next month is the time and the whole country will go crazy.

15
00:04:15,000 --> 00:04:22,000
Literally, I don't know the whole country, but many parts of it.

16
00:04:22,000 --> 00:04:40,000
We take the bus and see people getting in fist pipes on the street, blocking drunk, in the middle of traffic, throwing water and ice cubes and all sorts of things at each other.

17
00:04:40,000 --> 00:05:01,000
Some cars, as they went by, and motorcycles, they went by.

18
00:05:01,000 --> 00:05:16,000
People talk about having a happy New Year and they always wishing happy New Year to each other.

19
00:05:16,000 --> 00:05:22,000
But in Thailand anyway, and I think in other countries it's very much the same.

20
00:05:22,000 --> 00:05:45,000
December 31st is the most dangerous day of the year, the most vital fatalities of the whole year, the most unnatural deaths in the country.

21
00:05:45,000 --> 00:05:57,000
The second most, the first most is the period of the Thai New Year in April.

22
00:05:57,000 --> 00:06:02,000
He would explain how this is one way of rejoicing.

23
00:06:02,000 --> 00:06:07,000
This is one way of celebrating.

24
00:06:07,000 --> 00:06:19,000
When something good comes up, this is one way of, this is one way of being happy about being happy.

25
00:06:19,000 --> 00:06:28,000
A lot of them, so they are not learning the rejoice or the festive and celebrate.

26
00:06:28,000 --> 00:06:33,000
Then yes, how should we celebrate as putists?

27
00:06:33,000 --> 00:06:37,000
Then what should we celebrate? What should we be happy about?

28
00:06:37,000 --> 00:06:42,000
Why should we rejoice? It's a good question really.

29
00:06:42,000 --> 00:06:52,000
Sometimes everybody else gets all the fun.

30
00:06:52,000 --> 00:06:55,000
People in the world, they have good food to eat.

31
00:06:55,000 --> 00:07:07,000
They have nice clothes and houses and cars and so many wonderful things that they can enjoy.

32
00:07:07,000 --> 00:07:14,000
They can go where they want, they can live where they want.

33
00:07:14,000 --> 00:07:27,000
We have the picture of when your day is among you live in the perfect hunt, on the perfect mountain with the perfect sunset, the perfect forest.

34
00:07:27,000 --> 00:07:34,000
Then when you actually come to the monastery, you see the food is no good, the good days have weeks.

35
00:07:34,000 --> 00:07:49,000
The forest has leeches, snakes, scorpions, mosquitoes, the water is no good, nothing is ideal.

36
00:07:49,000 --> 00:08:07,000
Anything here I have to live, often this cold food, contaminated water, mosquitoes and snakes and scorpions and leeches,

37
00:08:07,000 --> 00:08:21,000
making roofs and so on, terminates. You know you think why? It's not much to celebrate.

38
00:08:21,000 --> 00:08:31,000
Of course we don't think this and hopefully we're all very happy. We should be very happy if you're not listening up.

39
00:08:31,000 --> 00:08:42,000
Because there's many things for us to be happy. We can use the 9th and example.

40
00:08:42,000 --> 00:08:53,000
Here we are under the fore moon. The only thing between us and the fore moon is the Bodhi tree.

41
00:08:53,000 --> 00:09:05,000
This Bodhi tree is a descendant from the tree under which the Buddha thinks outside.

42
00:09:05,000 --> 00:09:17,000
In ancient times they brought a branch from Bodhgaya to Sri Lanka and planted it in Anuradapura.

43
00:09:17,000 --> 00:09:28,000
The tree in Anuradapura has been cultivated and brought to many places around the country.

44
00:09:28,000 --> 00:09:46,000
One branch came here. This tree is descended from the original tree.

45
00:09:46,000 --> 00:10:04,000
There we are in a Buddhist monastery in a Buddhist country. Listening to the top communism about the practice of this meditation.

46
00:10:04,000 --> 00:10:18,000
Living a life as a Buddhist meditator. Striving to find the truth of life, freedom from suffering.

47
00:10:18,000 --> 00:10:31,000
Driving to make ones out, make ourselves better people or to purify our minds, to clean their minds of all the family.

48
00:10:31,000 --> 00:10:43,000
This in and of itself is something very rare in the world.

49
00:10:43,000 --> 00:10:53,000
Something very much worth rejoicing. Maybe sometimes we don't realize how lucky we are.

50
00:10:53,000 --> 00:11:04,000
We should rejoice in that at this, just this. Because why are we lucky when it makes us lucky?

51
00:11:04,000 --> 00:11:12,000
The first thing that makes us lucky is that we are born in a time when the Buddha is.

52
00:11:12,000 --> 00:11:19,000
The Buddha is teaching us here.

53
00:11:19,000 --> 00:11:26,000
The Buddha's aren't people that come to the world every three or four days.

54
00:11:26,000 --> 00:11:40,000
The Buddha didn't work for just a week or so to become a Buddha.

55
00:11:40,000 --> 00:11:44,000
You can look around and you can ask if is there anybody else since the time of the Buddha?

56
00:11:44,000 --> 00:11:57,000
In 2005, they say even 2500 years, it's not the time that it takes to become a Buddha.

57
00:11:57,000 --> 00:12:15,000
For uncountable eons, and 100,000 epochs are countable eons.

58
00:12:15,000 --> 00:12:28,000
It never happens to be crunch or whatever the end of the universe is.

59
00:12:28,000 --> 00:12:32,000
The big part is for uncountable eons.

60
00:12:32,000 --> 00:12:53,000
For the time from the time to the big bang to the big crunch, you can count.

61
00:12:53,000 --> 00:13:02,000
Here we are in a time in the time of the Buddha.

62
00:13:02,000 --> 00:13:09,000
We missed the Buddha himself, but we were doing when he was teaching.

63
00:13:09,000 --> 00:13:20,000
We were drinking gambling, and we weren't even given him somehow we missed the champ.

64
00:13:20,000 --> 00:13:24,000
So all that's left for us now is the dhamma.

65
00:13:24,000 --> 00:13:32,000
This is why we protect the dhamma and we revere the dhamma as well because it's all that we have left.

66
00:13:32,000 --> 00:13:34,000
The dhamma and the sangha.

67
00:13:34,000 --> 00:13:39,000
We have teachers, we have our right.

68
00:13:39,000 --> 00:13:49,000
The people who have passed on the Buddha's teaching have been an example to us in their practice.

69
00:13:49,000 --> 00:13:53,000
Then we have the teaching itself.

70
00:13:53,000 --> 00:13:55,000
We still have this.

71
00:13:55,000 --> 00:14:03,000
When it's gone, when it's gone, it will just be dark water.

72
00:14:03,000 --> 00:14:08,000
Now we have an island slowly sinking into the ocean.

73
00:14:08,000 --> 00:14:17,000
When the ocean is gone, when the island is gone, there will be no refuge for being.

74
00:14:17,000 --> 00:14:20,000
It will be like drowning and drowning in water.

75
00:14:20,000 --> 00:14:25,000
There will be no sign of shore.

76
00:14:25,000 --> 00:14:44,000
Rolling around amongst the sharks and the dangers of the storms of the ocean.

77
00:14:44,000 --> 00:14:49,000
We are in darkness.

78
00:14:49,000 --> 00:14:54,000
So here we have the island, we are on an island.

79
00:14:54,000 --> 00:14:58,000
We are on the island of the dhamma and the Buddha's teaching.

80
00:14:58,000 --> 00:15:04,000
We have this teaching with us now and something we can put into practice.

81
00:15:04,000 --> 00:15:08,000
This is something that's very lucky for all people now in the world.

82
00:15:08,000 --> 00:15:16,000
For all beings now, to be in, to be, this is an auspicious time.

83
00:15:16,000 --> 00:15:21,000
We still have the dhamma.

84
00:15:21,000 --> 00:15:32,000
The second thing that we should rejoice in, we are happy about it.

85
00:15:32,000 --> 00:15:44,000
Not only were we born in the time of the Buddha, but we have also been born a human being.

86
00:15:44,000 --> 00:16:00,000
We have been born a human being who has all of our arms and all of our legs and all of our body parts, including our brain, working in fairly good order.

87
00:16:00,000 --> 00:16:04,000
Good enough to be able to walk, good enough to be able to sit.

88
00:16:04,000 --> 00:16:19,000
Good enough to be able to think and to read and to study, good enough to practice.

89
00:16:19,000 --> 00:16:34,000
If you're born a dog or a cat or a pig or an insect or a snake or a scorpion, then it goes without saying that there's not much benefit that can be, can come from your life.

90
00:16:34,000 --> 00:16:39,000
I don't know what we did when we did something, we must have done something right.

91
00:16:39,000 --> 00:16:43,000
We were wondering about in the ocean if some saran somehow.

92
00:16:43,000 --> 00:16:52,000
The Buddha said like somehow this, we managed to be born a human being.

93
00:16:52,000 --> 00:16:57,000
The Buddha made a comparison with a turtle.

94
00:16:57,000 --> 00:17:07,000
If there's a turtle that lives at the bottom of the ocean and every 100 years it has to come up for air.

95
00:17:07,000 --> 00:17:24,000
Then someone were to throw a yolk. These yolks, these things that they put, this piece of wood that they put on, it has a blue band in the, to put around the neck of the, on the ox.

96
00:17:24,000 --> 00:17:37,000
Or maybe it's a ring or something. They put on the ox.

97
00:17:37,000 --> 00:17:48,000
The Buddha said that every 100 years coming up and in the middle, in the whole of the ocean there's one yolk floating.

98
00:17:48,000 --> 00:18:01,000
The Buddha said it's more likely that that turtle when he comes up after 100 years will come up with his neck in the yolk.

99
00:18:01,000 --> 00:18:09,000
Then it is for being to be born a heat, for an animal to be born a human being.

100
00:18:09,000 --> 00:18:19,000
Because there's an animal, there's not, there's not much you can do to, to cultivate your mind.

101
00:18:19,000 --> 00:18:26,000
As I said mostly it's killer be killed. The opportunity is to practice morality or develop concentration of wisdom.

102
00:18:26,000 --> 00:18:33,000
Somehow floating around in the ocean we managed to put our neck up in the yolk.

103
00:18:33,000 --> 00:18:42,000
Here we were born a human being, we don't know how we got here.

104
00:18:42,000 --> 00:18:47,000
I don't think any of us know how we did that we became human being.

105
00:18:47,000 --> 00:18:51,000
If you have special knowledge then you know what you did.

106
00:18:51,000 --> 00:18:56,000
Mostly we don't have a clue, we're just happy to be here.

107
00:18:56,000 --> 00:18:59,000
We should be happy to be here.

108
00:18:59,000 --> 00:19:04,000
We've met the Buddha and we met the Buddha as a human being.

109
00:19:04,000 --> 00:19:08,000
We're here, we're here.

110
00:19:08,000 --> 00:19:15,000
And this body is capable of how long have we been alive and yet we've managed to avoid death.

111
00:19:15,000 --> 00:19:23,000
We managed to avoid dismemberment with all the stupid things that we've done, all the crazy things that we've done.

112
00:19:23,000 --> 00:19:29,000
We're still managed to avoid death and dismemberment.

113
00:19:29,000 --> 00:19:39,000
We've managed to make it through school and job and make enough money to deliver lives in a way.

114
00:19:39,000 --> 00:19:54,000
So it allows us to make our way here to this place.

115
00:19:54,000 --> 00:19:59,000
So these things are very difficult to find.

116
00:19:59,000 --> 00:20:10,000
And the Buddha is a do-labor.

117
00:20:10,000 --> 00:20:17,000
A rising of the Buddha is a difficult thing to find.

118
00:20:17,000 --> 00:20:33,000
A difficult thing to find is difficult to find.

119
00:20:33,000 --> 00:20:49,000
A difficult to find is the life that is free from illness, free from difficulties.

120
00:20:49,000 --> 00:20:56,000
They have obstacles that allow you to come all the way across the world from all the parts of the world.

121
00:20:56,000 --> 00:21:03,000
Here we have people from all over the world coming.

122
00:21:03,000 --> 00:21:14,000
Very few people have this opportunity.

123
00:21:14,000 --> 00:21:25,000
The third thing that makes us lucky is that we actually want to take the opportunity.

124
00:21:25,000 --> 00:21:32,000
Because of all the people in the world who don't have the opportunity,

125
00:21:32,000 --> 00:21:40,000
very few are the people who have the opportunity.

126
00:21:40,000 --> 00:21:48,000
But among those people who have the opportunity, even fewer are those people who want to take the opportunity.

127
00:21:48,000 --> 00:21:55,000
How many people when you told them you were coming to meditate, put confused looks on their face,

128
00:21:55,000 --> 00:22:00,000
or gave you strange looks or ridiculed you or whatever.

129
00:22:00,000 --> 00:22:06,000
When you decided you want to ordain, you looked at you like you're crazy,

130
00:22:06,000 --> 00:22:14,000
or just ignored and abandoned you completely.

131
00:22:14,000 --> 00:22:30,000
How many people have you heard say, monks are useless, monks are meditators are wasting their time, selfish.

132
00:22:30,000 --> 00:22:37,000
How many people have the opportunity, but would never in a million years think about becoming a monk.

133
00:22:37,000 --> 00:22:48,000
Never in a million years think about coming to a meditation course, let alone become a monk, let alone dedicate their lives to.

134
00:22:48,000 --> 00:22:51,000
That's something worth rejoicing for sure.

135
00:22:51,000 --> 00:22:56,000
worth rejoicing about how lucky we are to just have this mind that says,

136
00:22:56,000 --> 00:22:58,000
that's not us, no?

137
00:22:58,000 --> 00:23:01,000
That's not like we chose to have this mind, but suddenly our mind says,

138
00:23:01,000 --> 00:23:07,000
I want to become a monk, or I want to go meditate.

139
00:23:07,000 --> 00:23:14,000
It's kind of funny really, because it's not like we chose to have that mind.

140
00:23:14,000 --> 00:23:20,000
It's not like other people chose to have the mind that didn't want to.

141
00:23:20,000 --> 00:23:22,000
We cultivated this.

142
00:23:22,000 --> 00:23:25,000
Somehow we managed to get something right.

143
00:23:25,000 --> 00:23:28,000
We should feel happy about that.

144
00:23:28,000 --> 00:23:31,000
We should rejoice in it.

145
00:23:31,000 --> 00:23:40,000
How wonderful that I have this wonderful mind that wants to meditate, that wants to devote my life to the Buddha's teacher.

146
00:23:40,000 --> 00:23:46,000
We maybe don't want to feel proud about it, but we can at least feel proud for a second though.

147
00:23:46,000 --> 00:23:48,000
Give us some encouragement.

148
00:23:48,000 --> 00:23:51,000
Pride can be useful as encouragement.

149
00:23:51,000 --> 00:23:54,000
You have to take it only to the step of encouragement.

150
00:23:54,000 --> 00:23:58,000
When you need encouragement, you can think of that.

151
00:23:58,000 --> 00:24:00,000
I'm not a horrible person.

152
00:24:00,000 --> 00:24:02,000
I'm not a useless person.

153
00:24:02,000 --> 00:24:07,000
I may not be the best meditator, but at least I want to meditate.

154
00:24:07,000 --> 00:24:11,000
At least I do meditate.

155
00:24:11,000 --> 00:24:25,000
This is the third blessing that we should be happy about.

156
00:24:25,000 --> 00:24:33,000
The fourth thing is that we actually take the opportunity.

157
00:24:33,000 --> 00:24:43,000
This is what the Buddha said.

158
00:24:43,000 --> 00:24:48,000
Understand in regards to that which needs to be done that it needs to be done.

159
00:24:48,000 --> 00:24:51,000
Understand that something needs to be done.

160
00:24:51,000 --> 00:24:53,000
That we can't live like this.

161
00:24:53,000 --> 00:24:57,000
We can't be negligent and just assume that when we die,

162
00:24:57,000 --> 00:25:02,000
we're going to go to a good place or nothing bad is going to happen to us.

163
00:25:02,000 --> 00:25:05,000
Actually, we're like walking a tightrope.

164
00:25:05,000 --> 00:25:10,000
And any moment we could snip off and fall to whatever it is that lies below.

165
00:25:10,000 --> 00:25:15,000
Being a human being is like walking a tightrope.

166
00:25:15,000 --> 00:25:21,000
If you were up here when the lightning was struck, that was a few nights ago.

167
00:25:21,000 --> 00:25:24,000
It's quite an experience for me.

168
00:25:24,000 --> 00:25:29,000
Realizing how thin is the rope on which we're walking.

169
00:25:29,000 --> 00:25:41,000
Any moment, any moment is something that could knock you off.

170
00:25:41,000 --> 00:25:49,000
If you haven't prepared yourself, haven't done anything useful in your life.

171
00:25:49,000 --> 00:25:52,000
How will you be ready for it?

172
00:25:52,000 --> 00:26:00,000
And die without any under any clarity of mind, any presence,

173
00:26:00,000 --> 00:26:09,000
any awareness, and die with a muddled confused mind?

174
00:26:09,000 --> 00:26:13,000
Some people have no idea.

175
00:26:13,000 --> 00:26:16,000
Some people say this.

176
00:26:16,000 --> 00:26:22,000
Some people say this, but then they do nothing about it.

177
00:26:22,000 --> 00:26:30,000
They think, yeah, it would be great if I could do some meditation or something spiritual anyway,

178
00:26:30,000 --> 00:26:33,000
but they don't do it.

179
00:26:33,000 --> 00:26:43,000
So he said, among those rare people who have that good intention or the understanding,

180
00:26:43,000 --> 00:26:55,000
they say very much more rare are the people who actually do something like that.

181
00:26:55,000 --> 00:27:01,000
So here we've accumulated quite a number of blessings in our lives,

182
00:27:01,000 --> 00:27:04,000
just by being in this one spot at this time.

183
00:27:04,000 --> 00:27:08,000
We've got the blessing of the Buddha and his teachings.

184
00:27:08,000 --> 00:27:10,000
We know what are the teachings of the Buddha.

185
00:27:10,000 --> 00:27:13,000
We know the force that the Patana, we know we pass on now.

186
00:27:13,000 --> 00:27:15,000
We know the four noble truths.

187
00:27:15,000 --> 00:27:18,000
We know the eight full noble path.

188
00:27:18,000 --> 00:27:22,000
These aren't just public school.

189
00:27:22,000 --> 00:27:27,000
These are the kind of things that they teach you that anyone can teach you.

190
00:27:27,000 --> 00:27:31,000
We take them for granted, sometimes G-I-I know them.

191
00:27:31,000 --> 00:27:37,000
Even just the four foundations of mindfulness is enough to get you to nibana.

192
00:27:37,000 --> 00:27:42,000
Enough to free you from the entire web of samsa.

193
00:27:42,000 --> 00:27:44,000
All suffering.

194
00:27:44,000 --> 00:27:46,000
Just the four foundations of mindfulness.

195
00:27:46,000 --> 00:27:48,000
Aka-ya-noa-yang-dikurimanga.

196
00:27:48,000 --> 00:27:52,000
This path is the straight path.

197
00:27:52,000 --> 00:27:57,000
The one way.

198
00:27:57,000 --> 00:28:01,000
Just the four foundations of mindfulness.

199
00:28:01,000 --> 00:28:07,000
And we have the ability to understand the teaching we're human.

200
00:28:07,000 --> 00:28:10,000
We have the ability to practice the teaching we can walk.

201
00:28:10,000 --> 00:28:14,000
We can sit, we can think.

202
00:28:14,000 --> 00:28:18,000
Our mind works, our body works.

203
00:28:18,000 --> 00:28:20,000
We have a good intention to practice.

204
00:28:20,000 --> 00:28:25,000
We have confidence.

205
00:28:25,000 --> 00:28:30,000
Just the knowledge that the Buddha's teaching was a good thing.

206
00:28:30,000 --> 00:28:33,000
Many people can't even come to this.

207
00:28:33,000 --> 00:28:40,000
Either they would pudiate or pudiate or pudiate.

208
00:28:40,000 --> 00:28:42,000
Yes, we're pudiate.

209
00:28:42,000 --> 00:28:46,000
The Buddha's teaching or they don't about it.

210
00:28:46,000 --> 00:28:49,000
Even if they think it might be good, they don't.

211
00:28:49,000 --> 00:28:54,000
They have so much doubt that they can practice.

212
00:28:54,000 --> 00:28:57,000
Maybe we don't have any of that.

213
00:28:57,000 --> 00:29:05,000
Or we have enough confidence to allow us to practice.

214
00:29:05,000 --> 00:29:10,000
We have enough confidence that we decided to come to ordain.

215
00:29:10,000 --> 00:29:13,000
The ordain.

216
00:29:13,000 --> 00:29:17,000
We have enough confidence to fly across the world.

217
00:29:17,000 --> 00:29:21,000
Stay in a hut in a forest.

218
00:29:21,000 --> 00:29:24,000
Our rarest path.

219
00:29:24,000 --> 00:29:30,000
We must have done something right to get here.

220
00:29:30,000 --> 00:29:35,000
In the fact that we've actually done it and succeeded, we made it.

221
00:29:35,000 --> 00:29:39,000
We're not there yet, but physically we've made it.

222
00:29:39,000 --> 00:29:40,000
We're here.

223
00:29:40,000 --> 00:29:42,000
Well, where we should be.

224
00:29:42,000 --> 00:29:44,000
Where we want to be.

225
00:29:44,000 --> 00:29:52,000
We're in a place where so many people have never had the chance to be.

226
00:29:52,000 --> 00:29:54,000
I consider this to be very lucky.

227
00:29:54,000 --> 00:29:59,000
I think we all should consider ourselves quite lucky.

228
00:29:59,000 --> 00:30:03,000
Consider this in a auspicious time, in a auspicious moment.

229
00:30:03,000 --> 00:30:11,000
We're developing good wholesome qualities of mind, wholesome qualities of speech, wholesome qualities of body,

230
00:30:11,000 --> 00:30:20,000
and love action.

231
00:30:20,000 --> 00:30:29,000
So it's quite fitting that this is what we're doing on the holy day.

232
00:30:29,000 --> 00:30:30,000
Holy day.

233
00:30:30,000 --> 00:30:33,000
I always used to joke at place and temp.

234
00:30:33,000 --> 00:30:36,000
So today is a holiday.

235
00:30:36,000 --> 00:30:40,000
You know, it doesn't mean you get a day off.

236
00:30:40,000 --> 00:30:44,000
You can check it to be holy.

237
00:30:44,000 --> 00:30:54,000
Buddhism, we take the holiness of the day on ourselves.

238
00:30:54,000 --> 00:30:57,000
We don't leave the holiness with the day.

239
00:30:57,000 --> 00:31:01,000
We take it to mean something for ourselves.

240
00:31:01,000 --> 00:31:04,000
That the holy day is for the time for us to become holy.

241
00:31:04,000 --> 00:31:10,000
We're us to increase our holiness.

242
00:31:10,000 --> 00:31:15,000
That's what Buddhism we don't look for someone else to be holy for us.

243
00:31:15,000 --> 00:31:21,000
We take the responsibility on ourselves.

244
00:31:21,000 --> 00:31:26,000
It's not that we don't believe in God or God's or angels or so.

245
00:31:26,000 --> 00:31:30,000
When we figure they've got enough work to do for themselves.

246
00:31:30,000 --> 00:31:33,000
So rather than asking them or praying to them,

247
00:31:33,000 --> 00:31:36,000
we'll do it for ourselves.

248
00:31:36,000 --> 00:31:41,000
The holiness isn't for the day, it isn't for God or an angel.

249
00:31:41,000 --> 00:31:43,000
The holiness is the day as for ourselves.

250
00:31:43,000 --> 00:31:45,000
This is a Buddhist holiday.

251
00:31:45,000 --> 00:31:47,000
Buddhist holy day.

252
00:31:47,000 --> 00:31:51,000
It's not a day off.

253
00:31:51,000 --> 00:31:57,000
So this is what we should feel lucky about.

254
00:31:57,000 --> 00:32:00,000
This is what we should rejoice in.

255
00:32:00,000 --> 00:32:09,000
So how do we rejoice as Buddhists?

256
00:32:09,000 --> 00:32:16,000
I think there's only one answer to the best way to rejoice.

257
00:32:16,000 --> 00:32:22,000
The best way to rejoice is to practice.

258
00:32:22,000 --> 00:32:26,000
Practice to purify our minds.

259
00:32:26,000 --> 00:32:31,000
I think it's kind of difficult really.

260
00:32:31,000 --> 00:32:42,000
Because the equivalent in the physical senses,

261
00:32:42,000 --> 00:32:47,000
on the holiday or on the festival or so on,

262
00:32:47,000 --> 00:32:53,000
the best way to rejoice is to go home and clean up your bathroom

263
00:32:53,000 --> 00:32:58,000
or clean up your kitchen.

264
00:32:58,000 --> 00:33:05,000
It doesn't seem like the best way to use the holy day.

265
00:33:05,000 --> 00:33:07,000
But when you do have the day off,

266
00:33:07,000 --> 00:33:12,000
when you do have the time to despair,

267
00:33:12,000 --> 00:33:16,000
this is when people do their house cleaning.

268
00:33:16,000 --> 00:33:24,000
Even if this were a day off, this would be the best use that we could have for us.

269
00:33:24,000 --> 00:33:27,000
But there's another way we can understand as the practicing.

270
00:33:27,000 --> 00:33:33,000
Because the practice is like an exaltation of ourselves.

271
00:33:33,000 --> 00:33:36,000
How difficult is it to even want to meditate?

272
00:33:36,000 --> 00:33:39,000
Let alone to meditate.

273
00:33:39,000 --> 00:33:41,000
It's a way of rejoicing.

274
00:33:41,000 --> 00:33:44,000
Meditation itself is a way of rejoicing.

275
00:33:44,000 --> 00:33:46,000
It's taking a victory lap.

276
00:33:46,000 --> 00:33:51,000
It's like boasting to the holy universe.

277
00:33:51,000 --> 00:33:56,000
It's like proclaiming to the holy universe

278
00:33:56,000 --> 00:33:59,000
that we've won with victorious.

279
00:33:59,000 --> 00:34:01,000
We are meditating,

280
00:34:01,000 --> 00:34:03,000
in spite of all the difficult,

281
00:34:03,000 --> 00:34:06,000
in spite of all the...

282
00:34:06,000 --> 00:34:11,000
All the...

283
00:34:11,000 --> 00:34:14,000
Improvability of us getting to this point,

284
00:34:14,000 --> 00:34:16,000
we made it.

285
00:34:16,000 --> 00:34:19,000
So our walking meditation or sitting meditation

286
00:34:19,000 --> 00:34:22,000
is actually a rejoicing.

287
00:34:22,000 --> 00:34:25,000
Sometimes it doesn't feel like it,

288
00:34:25,000 --> 00:34:27,000
but it's...

289
00:34:27,000 --> 00:34:30,000
It's victory.

290
00:34:30,000 --> 00:34:33,000
It's...

291
00:34:33,000 --> 00:34:36,000
It's the...

292
00:34:36,000 --> 00:34:38,000
It's the winning.

293
00:34:38,000 --> 00:34:42,000
This is the path to victory.

294
00:34:42,000 --> 00:34:46,000
The action that makes us victoria.

295
00:34:49,000 --> 00:34:52,000
And it's the act of purifies our minds.

296
00:34:52,000 --> 00:34:56,000
And so it's the act that brings us happiness.

297
00:35:00,000 --> 00:35:03,000
Without purity of mind, there's no happiness.

298
00:35:03,000 --> 00:35:06,000
So this act of meditating is...

299
00:35:06,000 --> 00:35:10,000
That which brings us to happiness.

300
00:35:10,000 --> 00:35:12,000
And so as a result,

301
00:35:12,000 --> 00:35:16,000
it shouldn't be something that is happy for us.

302
00:35:19,000 --> 00:35:21,000
So this is, I think,

303
00:35:21,000 --> 00:35:24,000
obviously it goes without saying how important

304
00:35:24,000 --> 00:35:26,000
on the Buddhist holiday,

305
00:35:26,000 --> 00:35:27,000
there's the meditate.

306
00:35:27,000 --> 00:35:31,000
This is just one way for us to think about it.

307
00:35:31,000 --> 00:35:36,000
That was the question today is,

308
00:35:36,000 --> 00:35:39,000
are we doing anything special on the Buddhist holiday?

309
00:35:39,000 --> 00:35:42,000
And I don't think we meditate more.

310
00:35:42,000 --> 00:35:44,000
But it's true, no.

311
00:35:44,000 --> 00:35:49,000
This is the time when people will practice more intensely.

312
00:35:49,000 --> 00:35:51,000
This is when the late people come to the monastery.

313
00:35:51,000 --> 00:35:54,000
Take the precepts.

314
00:35:54,000 --> 00:36:00,000
It's a time when people will undertake the practice

315
00:36:00,000 --> 00:36:03,000
of morality, concentration, and wisdom.

316
00:36:03,000 --> 00:36:14,000
Because it's an excuse or it's a good opportunity for them.

317
00:36:14,000 --> 00:36:17,000
So it's an even better opportunity for us who are living here

318
00:36:17,000 --> 00:36:21,000
and who have the chance to take the opportunity

319
00:36:21,000 --> 00:36:27,000
and take it as a joyful occasion for ourselves.

320
00:36:27,000 --> 00:36:29,000
Here I have the opportunity to meditate

321
00:36:29,000 --> 00:36:38,000
and we show it, we make it clear

322
00:36:38,000 --> 00:36:42,000
that you're on the holiday holiday day we practice

323
00:36:42,000 --> 00:36:45,000
with greater intention.

324
00:36:45,000 --> 00:36:48,000
It doesn't mean you even have to practice longer periods of time,

325
00:36:48,000 --> 00:36:52,000
but we'll try more to be mindful knowing that this is the holy day,

326
00:36:52,000 --> 00:36:55,000
knowing that there's the full moon under the Bodhi tree,

327
00:36:55,000 --> 00:37:00,000
thinking about the Buddha who sat under the Bodhi tree.

328
00:37:00,000 --> 00:37:05,000
We're twenty feet away from the Buddha.

329
00:37:05,000 --> 00:37:16,000
This we have the Bodhi tree here under which the Buddha sat.

330
00:37:16,000 --> 00:37:20,000
And we can also take it as a Buddha for the Buddha

331
00:37:20,000 --> 00:37:23,000
way of paying homage to the Buddha as well.

332
00:37:23,000 --> 00:37:26,000
I wonder what should we do on the holiday day to pay homage

333
00:37:26,000 --> 00:37:29,000
to the Buddha and the Buddha himself was very clear on this topic.

334
00:37:29,000 --> 00:37:32,000
When he was passing away and they were bringing flowers

335
00:37:32,000 --> 00:37:35,000
and paying homage to him, the Buddha said,

336
00:37:35,000 --> 00:37:38,000
this isn't how you pay homage to the Buddha.

337
00:37:38,000 --> 00:37:41,000
He said, do you ever practice this?

338
00:37:41,000 --> 00:37:46,000
Do you have to do with these things together?

339
00:37:46,000 --> 00:37:54,120
Do you have to have the root after constant

340
00:37:54,120 --> 00:38:00,400
or healthy or healthy or safe for you?

341
00:38:00,400 --> 00:38:09,580
Whether it's a bikur, a bikuni or novazika, or vazika, or vazika, or vazika, when they practice

342
00:38:09,580 --> 00:38:15,000
according to them teaching, realize the teaching for themselves.

343
00:38:15,000 --> 00:38:23,040
This is a person who pays proper homage, pays all proper respect, pays homage to the

344
00:38:23,040 --> 00:38:31,320
buddha with the highest form of on it, but they put it in itself.

345
00:38:31,320 --> 00:38:44,000
So every step that we take, this is like offering a flower to the Buddha, it's the highest

346
00:38:44,000 --> 00:38:50,880
form of the highest form of offering that you can give to the Buddha, the Buddha's teaching,

347
00:38:50,880 --> 00:38:56,200
to our monastery, to our meditation center, to the world, that one step that you take

348
00:38:56,200 --> 00:39:05,080
with mindfulness, clearly aware this is what it is, morality, concentration, and wisdom,

349
00:39:05,080 --> 00:39:16,160
understanding, so there's a pep talk for today, and then the Buddha's taught it, and

350
00:39:16,160 --> 00:39:22,680
you know, on the show, there's good and mindful frustration, then walking in and sitting.

