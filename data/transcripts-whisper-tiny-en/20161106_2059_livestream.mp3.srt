1
00:00:00,000 --> 00:00:28,400
Good evening, everyone, welcome to the Deer Park, and our night day, we're done

2
00:00:28,400 --> 00:00:31,000
so much, actually.

3
00:00:47,100 --> 00:00:56,000
Now I know that we are talking about A Sateba Tana Suha, it's a good one though,

4
00:00:56,000 --> 00:01:22,000
So to put on a suit that is one of the more important suitors for our meditation practice.

5
00:01:22,000 --> 00:01:33,000
As Buddhist meditators, that is where we get the word mindfulness from.

6
00:01:33,000 --> 00:01:51,000
The Satipatana suit, Maha Satipatana suit, Satipatana suit offers the best

7
00:01:51,000 --> 00:01:57,000
and depth look at how the practice mindfulness.

8
00:01:57,000 --> 00:02:16,000
It is the most comprehensive source of information on the four Satipatana.

9
00:02:16,000 --> 00:02:29,000
The four foundations and mindfulness.

10
00:02:29,000 --> 00:02:38,000
First the word about Sati, let's talk about this word, Satipatana, talk about the whole word.

11
00:02:38,000 --> 00:02:49,000
Satip means to be able to remember Kwamgradakdai, Patan Babatitang.

12
00:02:49,000 --> 00:03:07,000
Patan means a foundation, a place to establish or perhaps even the act of establishing can be called Patan.

13
00:03:07,000 --> 00:03:11,000
So the Sati actually doesn't mean mindfulness.

14
00:03:11,000 --> 00:03:23,000
This is sort of the understanding, I guess, that they're early translators got.

15
00:03:23,000 --> 00:03:34,000
When they read the Satipatana, when they read about Sati, when they understood what this kind of means,

16
00:03:34,000 --> 00:03:37,000
it's been mindfulness, but it's not what the word means.

17
00:03:37,000 --> 00:03:46,000
Sati is a fairly specific word, means the ability to remember.

18
00:03:46,000 --> 00:03:57,000
It's this quality of mind most often associated with the active participation of the meditator.

19
00:03:57,000 --> 00:04:03,000
Sati is active, it's the act of meditating.

20
00:04:03,000 --> 00:04:13,000
Meditating in a very specific way is meditating.

21
00:04:13,000 --> 00:04:25,000
The quality of mindfulness is fairly, the quality of Sati is easily understood when you think of a mantra.

22
00:04:25,000 --> 00:04:32,000
Because so much meditation, when we think of meditation, one of the most common sorts of meditation is mantra meditation.

23
00:04:32,000 --> 00:04:36,000
In Thailand, they say, Buddha, Buddha.

24
00:04:36,000 --> 00:04:41,000
Focusing on the name of the Buddha, what is it actually?

25
00:04:41,000 --> 00:04:43,000
Buddha and the Sati.

26
00:04:43,000 --> 00:04:48,000
So it's Sati or Anu Sati.

27
00:04:48,000 --> 00:04:54,000
Anu just means towards, so it's Sati towards the Buddha.

28
00:04:54,000 --> 00:05:03,000
It's recollecting about something.

29
00:05:03,000 --> 00:05:18,000
When you say Buddha, you're reminding yourself, you're bringing the mind back to the Buddha.

30
00:05:18,000 --> 00:05:28,000
When you, the Buddha says at one point, when you remember things that happened a long time ago, that's Sati.

31
00:05:28,000 --> 00:05:35,000
So your ability to bring the mind back to remind yourself about things that happened a long time.

32
00:05:35,000 --> 00:05:44,000
The ability to place the mind on something, to grasp some concept, some thought.

33
00:05:44,000 --> 00:05:49,000
That's Sati.

34
00:05:49,000 --> 00:05:56,000
Remind, bringing the mind back.

35
00:05:56,000 --> 00:05:59,000
So the same root is the word Saturna.

36
00:05:59,000 --> 00:06:06,000
In fact, the grammar says, Saturna and Sati are the same word Saturanangwa Sati.

37
00:06:06,000 --> 00:06:17,000
When we have understanding Satis of Saturna, so Saturna is this word, we transcend often as refuge, but it also means, as an object of reflection.

38
00:06:17,000 --> 00:06:23,000
So the Buddha, the Dhamma, and the Sangha are three objects of reflection.

39
00:06:23,000 --> 00:06:33,000
Buddha,ang, Saturna and Gatsami, I go to the Buddha as a refuge, but also as a reflection, an object of reflection.

40
00:06:33,000 --> 00:06:43,000
Dhamma and Saturna and Gatsami, I go to the Dhamma as a refuge, and also as an object of contemplation.

41
00:06:43,000 --> 00:06:45,000
Something to think about.

42
00:06:45,000 --> 00:06:50,000
Before you do anything, you ask yourself, would the Buddha approve?

43
00:06:50,000 --> 00:06:53,000
Is that in line with the Dhamma?

44
00:06:53,000 --> 00:07:03,000
Sangha and Saturna and Gatsami, I go to the Sangha as a refuge, but also as an object of reflection.

45
00:07:03,000 --> 00:07:16,000
If I do this, am I following the tradition of the Sangha, the enlightened followers of the Buddha?

46
00:07:16,000 --> 00:07:18,000
So we recollect.

47
00:07:18,000 --> 00:07:23,000
So what does this have to do with mindfulness? What does this have to do with insight?

48
00:07:23,000 --> 00:07:28,000
What does this have to do with the present moment, right?

49
00:07:28,000 --> 00:07:31,000
I think it's really easy to see.

50
00:07:31,000 --> 00:07:36,000
What is being said here, when the Buddha says we should have satty about the present moment,

51
00:07:36,000 --> 00:07:41,000
it means we should recollect the present moment, remind ourselves about the present moment.

52
00:07:41,000 --> 00:07:48,000
And bring the mind back to the present moment.

53
00:07:48,000 --> 00:07:56,000
And so just as we brought the mind back to the Buddha, we bring the mind back to the here and now.

54
00:07:56,000 --> 00:08:00,000
When you're walking, know you're walking.

55
00:08:00,000 --> 00:08:12,000
When walking, when knows, I am walking.

56
00:08:12,000 --> 00:08:18,000
So you don't have to use the word I am following, because when you say Gatsami, you just know who it's referring to,

57
00:08:18,000 --> 00:08:19,000
it's not referring.

58
00:08:19,000 --> 00:08:20,000
You've got to see.

59
00:08:20,000 --> 00:08:21,000
You walk.

60
00:08:21,000 --> 00:08:31,000
You've got to see.

