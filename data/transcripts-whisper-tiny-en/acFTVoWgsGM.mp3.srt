1
00:00:00,000 --> 00:00:09,180
Okay. So, okay, two questions here. First, we'll go in order. Here we have, I was looking

2
00:00:09,180 --> 00:00:13,360
at the five precepts. One of the precepts intrigued me. I might know the answer, but I wanted

3
00:00:13,360 --> 00:00:28,480
to know from your point of view, or Buddhist point of view, what is considered sexual misconduct?

4
00:00:28,480 --> 00:00:39,160
It's a funny question, and there are some funny, funny parts to it. I was, it really

5
00:00:39,160 --> 00:00:44,640
leads to how funny the precepts can be. The precepts are not funny, but how funny they,

6
00:00:44,640 --> 00:00:49,120
how funny people can be, especially when they want to try to wiggle their way around

7
00:00:49,120 --> 00:00:54,480
the precepts. And not only wiggle their way around the precepts, but want to accuse other

8
00:00:54,480 --> 00:01:00,640
people of breaking the precepts. This intellectual one-upmanship, or whatever you call it,

9
00:01:00,640 --> 00:01:07,200
or this kind of spiritual or religious one-upmanship where these people are breaking the precepts.

10
00:01:08,960 --> 00:01:16,560
A good example of that, this is an example, but a better example is the fifth precept. Many Buddhists

11
00:01:16,560 --> 00:01:27,200
want to be able to say that gambling is breaking the fifth precept. And I don't think many,

12
00:01:27,200 --> 00:01:31,600
I don't know of any Sri Lankans who do, because Sri Lankans are much better in their understanding,

13
00:01:31,600 --> 00:01:38,720
their technical understanding of the dhamma. It's incredible, the people here. Pretty much everyone,

14
00:01:38,720 --> 00:01:46,160
every Buddhist, and even non-Buddhas, are so up on what the Buddha taught and what's in the

15
00:01:46,160 --> 00:01:52,640
Topitika and Pali and just about everything. It's incredible, it's really great. But I've talked

16
00:01:52,640 --> 00:01:58,880
with other people who have some really strange ideas. I won't mention from where, but Sri Lanka

17
00:01:58,880 --> 00:02:04,320
seems pretty good. I did meet one man who had some very strange ideas, and I'll talk about that

18
00:02:04,320 --> 00:02:09,680
in relation to your question. But first, these people who talk about gambling is being breaking

19
00:02:09,680 --> 00:02:18,800
the fifth precept. The precepts are, they're curious in a way because they don't include everything.

20
00:02:19,440 --> 00:02:23,520
I mean, rape isn't breaking any of the five precepts.

21
00:02:26,560 --> 00:02:30,080
Wait a second, is it breaking the third precept? I might be getting ahead of myself.

22
00:02:30,080 --> 00:02:36,800
I'm meant to say, let me get back to that. Torture isn't breaking the first precept.

23
00:02:36,800 --> 00:02:39,120
A herding people isn't breaking the first precept.

24
00:02:43,280 --> 00:02:48,240
According to some people, rape wouldn't be breaking the third precept. And I'll talk about why

25
00:02:48,240 --> 00:02:55,360
that is. I probably don't agree with that. I hope you're all glad to hear that.

26
00:02:55,360 --> 00:03:02,160
But I just want to say how curious it is because not everything is in the five precepts.

27
00:03:03,440 --> 00:03:09,760
The five precepts are, in one way, very simple. One of the great things about them is how

28
00:03:09,760 --> 00:03:17,120
simple they are. There's no real complexity here. They're the highlights. Killing is one of the

29
00:03:17,120 --> 00:03:28,960
biggies. That's a very important event, death. It doesn't mean that death is the only evil act

30
00:03:28,960 --> 00:03:35,520
that you can perform towards another person, but it's clear. All of the precepts are simple.

31
00:03:35,520 --> 00:03:47,600
Kill, steal, cheat, lie, and intoxicate. They cover all the bases generally speaking.

32
00:03:47,600 --> 00:03:53,680
And so in that sense, they're kind of a guide. But that's the one great thing,

33
00:03:53,680 --> 00:04:00,560
as they're simple. The other great thing is, or the other important thing about them is that

34
00:04:00,560 --> 00:04:11,280
they show the extremes, because killing pretty much is the worst. It's the most final thing you

35
00:04:11,280 --> 00:04:18,080
can do to someone. If you torture them, there are worst things you could do, I suppose.

36
00:04:18,080 --> 00:04:29,840
But not as final and as absolute as killing. Stealing is in a category of its own, of course,

37
00:04:30,560 --> 00:04:36,480
and it's the extreme. You know, you can try to trick someone into giving you something,

38
00:04:37,600 --> 00:04:43,120
but it wouldn't be breaking the precept. So what I want to say is the precepts are not the

39
00:04:43,120 --> 00:04:49,120
be-all-end-all of morality. Gambling isn't breaking the fifth precept. In fact, it's not even,

40
00:04:49,760 --> 00:04:55,200
I would say it's not even close. It has nothing to do with the fifth precept. Gambling is simply

41
00:04:55,200 --> 00:05:01,040
an addiction. The fifth precept has nothing to do with addiction. It has to do with intoxication.

42
00:05:02,080 --> 00:05:05,760
And then people say, oh, well, you become intoxicated when you gamble, and that's ridiculous.

43
00:05:05,760 --> 00:05:14,160
You become intoxicated when you do anything. You watch television, you become intoxicated. You

44
00:05:14,640 --> 00:05:19,200
sit in front of the internet, you use the internet, you become intoxicated. You look at someone,

45
00:05:19,200 --> 00:05:23,840
you look at something, you look at a flower, you become intoxicated. You smell, you eat food,

46
00:05:23,840 --> 00:05:31,120
you become intoxicated. Now, the fifth precept is something that really intoxicates the mind.

47
00:05:31,120 --> 00:05:43,600
Something that clouds or impairs one's ability to think clearly. So drugs and alcohol,

48
00:05:43,600 --> 00:05:47,920
they're on a level of their own in terms of doing that. These are what breaks the precept.

49
00:05:48,480 --> 00:05:56,160
Otherwise, everything is breaking the fifth precept. And that's really not the point here.

50
00:05:56,160 --> 00:06:01,120
Another thing about the precept is it's important to remember that they are not rules.

51
00:06:02,800 --> 00:06:15,040
They are not commandments, and they're not forbidden acts. I once asked this group,

52
00:06:15,040 --> 00:06:22,240
I had this question about the fifth precept. And I asked people, okay, what's the first precept?

53
00:06:22,240 --> 00:06:36,880
And they said, in Thai, ham kasat, or ham kasat, I think was not forbidding to kill

54
00:06:38,000 --> 00:06:50,960
the forbidding of killing animals. And I said, wrong, zero, and then they're trying different ways

55
00:06:50,960 --> 00:06:54,160
of saying it. Of course, in Thai, there's not so many different ways of saying things.

56
00:06:55,200 --> 00:07:00,480
So they all just sat there kind of silently. And it was kind of tricky of me. I don't think

57
00:07:00,480 --> 00:07:05,120
anyone really got what I was trying to say in the end. But that's the point, is that it's not

58
00:07:05,120 --> 00:07:14,320
forbidden. It's not ham kasat. It's I undertake not to. So you're taking on a bunch of

59
00:07:14,320 --> 00:07:23,840
you know, well rules, I guess. But you're making a promise to yourself. It's not like

60
00:07:23,840 --> 00:07:30,720
anyone's going to come looking for you. But you're taking on this guideline of how to live your

61
00:07:30,720 --> 00:07:36,880
life. It doesn't really answer your question at all. But it kind of says, it hopefully says

62
00:07:36,880 --> 00:07:45,760
the tone. In relation to the third preset, people do this as well. When it hits home,

63
00:07:45,760 --> 00:07:55,840
they try to wiggle out of it. And when they feel, when they have this kind of ego and thinking of

64
00:07:55,840 --> 00:08:01,840
themselves as superior, they try to pin it on people when it might not apply.

65
00:08:01,840 --> 00:08:11,040
I would say, let's not go with what I would say first. Let's talk about technically speaking.

66
00:08:12,080 --> 00:08:18,880
This one man in Colombo, who was probably committing adultery. And apparently it was going to

67
00:08:18,880 --> 00:08:24,160
visit brothels daily, wasting all of his money on gambling and women and so on.

68
00:08:24,160 --> 00:08:32,720
And he even people gave him some money for me to keep for my benefit and he ran away with

69
00:08:32,720 --> 00:08:43,520
that money and I haven't seen him since. But when I was with him, he was talking about his adopted

70
00:08:43,520 --> 00:08:49,200
son who had some problems. And I saw his adopted son and he said, this is my adopted son and

71
00:08:49,200 --> 00:08:56,400
this is his girlfriend. And he's going to America soon. And then it came out he's getting married.

72
00:08:57,280 --> 00:08:59,520
Then it came out that he's getting married to a different woman.

73
00:09:01,120 --> 00:09:04,480
And he's getting married to a different woman, going to sleep with her,

74
00:09:05,440 --> 00:09:09,920
then coming back to be with his girlfriend and go with her to America to get married to her

75
00:09:09,920 --> 00:09:15,120
in America. Because he doesn't love the first woman who he's getting married to, but he feels

76
00:09:15,120 --> 00:09:23,440
responsible because he slept with her already. He feels like he has a duty to marry her because

77
00:09:23,440 --> 00:09:29,920
she's no longer a virgin or something like that. Anyway, I said to him, I said, well,

78
00:09:29,920 --> 00:09:36,640
doesn't he understand that's breaking the third precept? And he explained to me. He said,

79
00:09:36,640 --> 00:09:45,920
no, no, that's not breaking the third precept. There are certain types of women that you can't

80
00:09:45,920 --> 00:09:55,920
have relationships with. And technically speaking, he's got something there. And I'm sure he's

81
00:09:55,920 --> 00:10:01,440
heard this from monks who have been able to give people a very clever way out of breaking this

82
00:10:01,440 --> 00:10:07,040
precept. I mean, if that's the case, then this guy is doing nothing wrong, right? Here he is,

83
00:10:07,040 --> 00:10:13,280
breaking the hearts of two women. His girlfriend is devastated because she knows all about it.

84
00:10:13,280 --> 00:10:19,120
And in his apartment, he goes off to marry and sleep with another woman.

85
00:10:20,880 --> 00:10:29,200
And it doesn't bode well. I can picture these two going off to America. It would make a good

86
00:10:29,200 --> 00:10:36,960
movie, I think. I don't know if it would. It's absurd, really. I can't imagine where these people

87
00:10:36,960 --> 00:10:49,440
come up with these things. So, but he's not doing anything wrong according to these people

88
00:10:49,440 --> 00:10:57,760
because the women that he's sleeping with are not engaged to anyone. And where they get this

89
00:10:57,760 --> 00:11:03,360
is in a passage where the Buddha talked about this precept. He said, well, a man shouldn't have

90
00:11:06,240 --> 00:11:12,960
romantic relationships with women who are under the protection of someone else, basically,

91
00:11:13,920 --> 00:11:20,080
who are under the protection of their parents, who are under the protection of another man.

92
00:11:21,120 --> 00:11:26,160
And so, I guess at the time there would have been no marriage certificates or something,

93
00:11:26,160 --> 00:11:33,040
but basically at the end the Buddha said, even to the point where a man has, I think it was like

94
00:11:33,040 --> 00:11:41,200
put a garland of flowers around her, around the woman's neck, which I guess was at the time

95
00:11:41,200 --> 00:11:48,960
a sign of engagement. The man accepts the woman and puts this ring of flowers around her neck.

96
00:11:48,960 --> 00:11:58,080
And so, they say, well, that's fine. Then the man can go and have five wives and mistresses

97
00:11:58,080 --> 00:12:05,440
and they do, apparently, and not commit any violation of the precept at all.

98
00:12:06,320 --> 00:12:10,720
So, first of all, I think this is a total misunderstanding of what the Buddha was trying to say.

99
00:12:11,680 --> 00:12:16,800
This is a good advice in relation to this precept, but it certainly isn't the whole of the precept.

100
00:12:16,800 --> 00:12:24,960
It certainly is true that therefore, if you are committed to someone else, you should not have

101
00:12:24,960 --> 00:12:30,640
romantic engagement with anyone besides that one person. I mean, does that mean

102
00:12:33,520 --> 00:12:39,520
if the woman is engaged to the man, you can't have relationships with her, but she can have

103
00:12:39,520 --> 00:12:46,640
a relationship with you? Of course not. It's actually worse for her because she's promised.

104
00:12:46,640 --> 00:12:50,560
You haven't promised anything. You can go and have a relationship with the woman,

105
00:12:51,600 --> 00:12:57,200
not be breaking any of promise, but what you're doing is devastating, destroying people's

106
00:12:57,200 --> 00:13:04,640
trust, destroying people's relationship, and so on. So, what this man failed to see, and

107
00:13:04,640 --> 00:13:14,880
intentionally, because he also was an adult person, apparently, and had reason to want to find

108
00:13:14,880 --> 00:13:24,720
a better explanation than the correct one. But I would go even further and say, in general,

109
00:13:24,720 --> 00:13:36,800
that you break the precept when you harm someone with your romantic or sexual behavior,

110
00:13:36,800 --> 00:13:42,800
so I would include rape in here, I guess hesitantly. I mean, obviously, rape is one of the worst

111
00:13:42,800 --> 00:13:49,760
things that could ever happen to someone. I've dealt with people who have been victims of it.

112
00:13:49,760 --> 00:13:56,480
I mean, I think murder in some cases might be preferable. I don't know, but I think if you were

113
00:13:56,480 --> 00:14:02,400
killed, at least you can then forget about it and start fresh, right? But I had a student who

114
00:14:02,400 --> 00:14:10,960
was raped by her father. Yeah, and she was a tough case. I don't know where she is now,

115
00:14:10,960 --> 00:14:19,040
she's a lot better after practicing meditation, but people have their own lives. It was

116
00:14:19,040 --> 00:14:23,040
incredible, actually. I've given talked about this before. She came, she looked like a ghost.

117
00:14:23,840 --> 00:14:30,240
Her hair was like this in her eyes, and her face was white like a sheet, and she just had this

118
00:14:30,240 --> 00:14:50,480
look on her face like a horror always, and she would come and cry during the reporting sessions.

