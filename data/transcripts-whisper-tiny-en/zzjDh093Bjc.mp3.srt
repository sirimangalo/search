1
00:00:00,000 --> 00:00:08,400
Well good evening back for another question and answer video.

2
00:00:08,400 --> 00:00:15,080
Today's question isn't exactly a question.

3
00:00:15,080 --> 00:00:25,560
It's a bit of a request for help and then this person wants help on standing.

4
00:00:25,560 --> 00:00:36,060
Well once help on that practice really, once help on overcoming doubt specifically, this person

5
00:00:36,060 --> 00:00:45,880
has doubt about their ability to control the mind, train the mind.

6
00:00:45,880 --> 00:00:52,880
When there is no eye, they see that thoughts are out of their control and emotions popping

7
00:00:52,880 --> 00:00:57,880
in and out on their own.

8
00:00:57,880 --> 00:00:59,880
So what the heck can we do?

9
00:00:59,880 --> 00:01:03,760
What the heck?

10
00:01:03,760 --> 00:01:06,400
It seems paradoxical this day, right?

11
00:01:06,400 --> 00:01:15,320
You should try to control the mind when the mind itself is not self, when there is nobody

12
00:01:15,320 --> 00:01:19,600
able to control.

13
00:01:19,600 --> 00:01:27,080
So the general topic is on the self, in the existence of the self, the belief in the

14
00:01:27,080 --> 00:01:28,080
self.

15
00:01:28,080 --> 00:01:34,760
Practice based on one's beliefs in self and so on.

16
00:01:34,760 --> 00:01:39,200
There are a lot of theories about self.

17
00:01:39,200 --> 00:01:48,400
People say the self exists and that people say self doesn't exist.

18
00:01:48,400 --> 00:01:58,120
There is a soul, there is a free will in determinism, that's what this person talks about.

19
00:01:58,120 --> 00:02:07,440
There is a free will who has the free will if there is determinism, well then what?

20
00:02:07,440 --> 00:02:09,520
Then nothing I guess.

21
00:02:09,520 --> 00:02:21,160
Lots of theories, but in spite of all these theories and even not just theories, but

22
00:02:21,160 --> 00:02:29,360
outlooks, beliefs, views, perceptions, there are some people, I think it's not even a matter

23
00:02:29,360 --> 00:02:39,000
of having a theory or even a set view or belief, but yet we have a very very strong

24
00:02:39,000 --> 00:02:40,760
and ubiquitous.

25
00:02:40,760 --> 00:02:53,920
Everyone has these perceptions of self, of control, of possession, of entity, of existence,

26
00:02:53,920 --> 00:03:08,440
of eye, of ego, and so I'm not going to answer any, I'm not going to give you my theory

27
00:03:08,440 --> 00:03:16,360
because what I want to point out is that theories do just this, they cause doubt, they

28
00:03:16,360 --> 00:03:27,320
cause certainty or uncertainty and that certainty or uncertainty has ramifications.

29
00:03:27,320 --> 00:03:33,920
Here we have an example, this person who asked the question has doubts and so that's

30
00:03:33,920 --> 00:03:42,040
the result, the result of their state of mind in terms of their perceptions of regarding

31
00:03:42,040 --> 00:03:50,920
views, regarding self, have led them to doubt, to a state of mind that is, let me say

32
00:03:50,920 --> 00:03:57,120
unwholesome, but unwholesome just means it's not very useful, you know when you're

33
00:03:57,120 --> 00:04:04,640
doubting things, well there's not much you can do, you can't act upon doubt, doubt doesn't

34
00:04:04,640 --> 00:04:11,000
lend itself to strength of mind, fortitude of mind, conviction does, conviction allows us

35
00:04:11,000 --> 00:04:20,280
to all sorts of things good and bad, if you believe in violence then you'll be very good

36
00:04:20,280 --> 00:04:27,080
at being violent, if you believe in meditation you'll put a lot of effort into meditation,

37
00:04:27,080 --> 00:04:37,760
doubt, well doubt is a result, for other people there's a conviction in their views,

38
00:04:37,760 --> 00:04:47,040
and so following in the Buddhist tradition rather than offering a theory of shall, I will

39
00:04:47,040 --> 00:04:55,000
follow the Buddha's example and talk about what happens when you believe in self.

40
00:04:55,000 --> 00:05:01,640
The Buddha said to his monks, and probably the closest he ever got to giving an answer

41
00:05:01,640 --> 00:05:10,000
to this sort of question is, he said monks, this is what I tell you to do, hold on to

42
00:05:10,000 --> 00:05:18,280
any belief in self that doesn't cause suffering, so that's my advice to you, do you see

43
00:05:18,280 --> 00:05:24,680
such a few months, they said no, we don't see such a good good, I don't see such a

44
00:05:24,680 --> 00:05:33,400
few either, when you believe in self, any belief in self causes stress and suffering,

45
00:05:33,400 --> 00:05:41,280
and so this is what we see, this is what we try to first off point out that when you

46
00:05:41,280 --> 00:05:55,840
believe in an eye, then it leads to conceit, it leads to ego, it leads to possessiveness,

47
00:05:55,840 --> 00:06:05,080
it leads to craving, it leads to anger, it leads to manipulating others, we try to get things

48
00:06:05,080 --> 00:06:16,000
that we want, we fear and our jealous and stingy about our possessions, the things that

49
00:06:16,000 --> 00:06:33,800
we say I, we're afraid of losing, we're afraid of loss, we're afraid of change, if we

50
00:06:33,800 --> 00:06:40,680
were to lose a limb, because it's our limb, because of our perception of self, cause

51
00:06:40,680 --> 00:06:47,760
of stress and suffering, when we have any kind of pain, when we have any kind of pleasure,

52
00:06:47,760 --> 00:06:56,920
our attachment to these things is very much tied up, bound up with me and I, and I,

53
00:06:56,920 --> 00:07:09,360
when I mention our conceit and our arrogance, our ego and egoism, when we cling to the

54
00:07:09,360 --> 00:07:16,720
idea of self and compare ourselves with others, when we become arrogant and don't believe

55
00:07:16,720 --> 00:07:23,160
that we deserve such things or we do deserve this, we don't deserve that, when we hate

56
00:07:23,160 --> 00:07:33,520
ourselves, so one important thing that Buddhism stresses is pointing out the evils of

57
00:07:33,520 --> 00:07:38,600
clinging to these sorts of views, but there are evils of clinging to a view of determinism

58
00:07:38,600 --> 00:07:48,960
as well, so we're not trying to, or we don't work in such a way as to provide an answer,

59
00:07:48,960 --> 00:07:58,600
to even the idea of determinism or free will, that activity of trying to find an answer

60
00:07:58,600 --> 00:08:10,880
or of seeking out an answer has consequences, so a person who believes in determinism,

61
00:08:10,880 --> 00:08:17,000
the truth of it is not the question, the result of it is the question, when a person holds

62
00:08:17,000 --> 00:08:24,360
that view, when that view, if you want to look at it from purely experiential points,

63
00:08:24,360 --> 00:08:30,760
experiential point of view, when that view arises in the mind, there are going to be

64
00:08:30,760 --> 00:08:37,160
other states of mind that arise based on it, that they can't be avoided, so it's kind

65
00:08:37,160 --> 00:08:49,480
of deterministic, but you can see what the result is, and so what this points to is a

66
00:08:49,480 --> 00:09:02,240
very different way of looking at reality, that finding truths in this particular sense

67
00:09:02,240 --> 00:09:20,360
of what exists, the existence of something, and the nature of those things, what would

68
00:09:20,360 --> 00:09:25,720
we say is deterministic, what would we say has free will, we're referring to something,

69
00:09:25,720 --> 00:09:38,400
and all of that is a poor way of looking at reality, it doesn't really get to reality,

70
00:09:38,400 --> 00:09:44,680
it overlooks something, because again, it deals within the framework or within the realm

71
00:09:44,680 --> 00:09:51,640
of theory, all that can happen in regards to free will and determinism is you develop

72
00:09:51,640 --> 00:09:59,080
a view based on them, based on one or the other, and the reality that's working behind

73
00:09:59,080 --> 00:10:09,440
the scenes of experiences is going to change, based on that view, and neither one, the

74
00:10:09,440 --> 00:10:14,520
person who is deterministic, of course, is not going to practice if they're convinced

75
00:10:14,520 --> 00:10:19,360
and not going to do anything, because they're already doing everything, and as a result

76
00:10:19,360 --> 00:10:24,800
they do nothing, it's kind of paradoxical, or it's a self-fulfilling prophecy, in

77
00:10:24,800 --> 00:10:31,760
a sense, or it's defeating someone who has free will, a view of free will is going to

78
00:10:31,760 --> 00:10:41,000
butt up against very much against reality when they can't control certain things.

79
00:10:41,000 --> 00:10:48,200
The perspective or the approach that we take in Buddhism is, of course, to be mindful

80
00:10:48,200 --> 00:10:56,480
and to observe and to learn and to see more clearly, in regards to this very important

81
00:10:56,480 --> 00:11:04,080
topic, it's not that we should dismiss the topic, it's that the approach has to be different,

82
00:11:04,080 --> 00:11:11,760
the approach has to be, in terms of experientially finding the truth, not to create a

83
00:11:11,760 --> 00:11:19,000
theory, not to have some belief there is a self, there is no self, and that we have

84
00:11:19,000 --> 00:11:26,840
free will, the universe deterministic, that sort of thing, but to understand, to see,

85
00:11:26,840 --> 00:11:36,080
to appreciate, to become familiar, familiar is not strong enough for word, but I don't

86
00:11:36,080 --> 00:11:45,400
know if we even have the right words in the English language, to dwell in reality,

87
00:11:45,400 --> 00:11:54,960
to really be in tune with reality, the reality of non-self, that all dumbas are non-self

88
00:11:54,960 --> 00:11:58,480
to put it that way, but what does that mean?

89
00:11:58,480 --> 00:12:05,720
That means that the things that we observe, or all the aspects of experience, the reality

90
00:12:05,720 --> 00:12:14,760
of things, is non-self, is impermanent, it's suffering, it doesn't have the qualities

91
00:12:14,760 --> 00:12:27,160
of being an I, being an entity, of being me, like a quality, or of being mine, of a possession.

92
00:12:27,160 --> 00:12:37,960
So this isn't a deeply philosophical or intellectual framework, or philosophy, it's talking

93
00:12:37,960 --> 00:12:40,920
about things like pain, pain is not self, what does that mean?

94
00:12:40,920 --> 00:12:45,760
It means you can't control the pain, it means our relationship with the pain in terms

95
00:12:45,760 --> 00:12:51,240
of trying to control it is a cause for stress and suffering, and when we have things

96
00:12:51,240 --> 00:13:01,080
we like, we try to keep them, make them stay, make them come again, cause it's stress

97
00:13:01,080 --> 00:13:09,640
and suffering, it's a failure, it's out of touch with reality, when you start to observe

98
00:13:09,640 --> 00:13:13,320
the pain just observing it as it is, when you observe pleasure of observing it just as

99
00:13:13,320 --> 00:13:19,480
it is, you lose this, because you see it coming and going, as this person did.

100
00:13:19,480 --> 00:13:23,400
So their only real fault, I would say this person, not fault, the problem that this person

101
00:13:23,400 --> 00:13:30,120
is having, is this doubt which arises from a wrong approach, their approach is to try and

102
00:13:30,120 --> 00:13:34,920
figure it out, to find a theory that's in tune with reality.

103
00:13:34,920 --> 00:13:40,240
Theories are not reality, I mean, theories are mental activity, something that you have

104
00:13:40,240 --> 00:13:42,800
to let go of.

105
00:13:42,800 --> 00:13:50,600
And so, when you see these things, when you see experiences, emotions coming and going,

106
00:13:50,600 --> 00:13:55,440
that's enough, let it be just that, let your appreciation of the way things are, be what

107
00:13:55,440 --> 00:14:03,160
it is, then no one can ever tell you differently, but don't cultivate views as the point,

108
00:14:03,160 --> 00:14:07,640
when you have a view like, does that mean I'm observing this, or is this not me, is this

109
00:14:07,640 --> 00:14:13,440
just more mechanistic realities, all of that is, you know, what's going on at that moment

110
00:14:13,440 --> 00:14:18,000
is more thinking, more experiences, not very good ones, because they're caught up in

111
00:14:18,000 --> 00:14:27,720
doubt, speculation, and their base on our defilements, our beliefs themselves, our views

112
00:14:27,720 --> 00:14:35,680
of self, our perceptions of self, and they aren't mindfulness, they aren't reality.

113
00:14:35,680 --> 00:14:41,160
And when you just say to yourself, pain, pain, there's no self that could ever be a part

114
00:14:41,160 --> 00:14:50,400
of that, the idea of self has no place there, that activity, that approach is nothing

115
00:14:50,400 --> 00:14:56,920
to do, has nothing to do with views of self or theories of self.

116
00:14:56,920 --> 00:15:02,800
The importance of self that the Buddha taught, non-self, is to see that things are not what

117
00:15:02,800 --> 00:15:07,360
we thought they were, that they are not me, they are not mine, they are not I, as you

118
00:15:07,360 --> 00:15:12,680
watch them, you'll see they're out of your control, there you'll see that the control

119
00:15:12,680 --> 00:15:20,160
that you thought you had, the thoughts of control that had arisen before, were based

120
00:15:20,160 --> 00:15:30,840
on speculation, delusion, lack of observation, lack of clear vision of how things are.

121
00:15:30,840 --> 00:15:39,880
You'll see that, as this person says, they come and they go and they come of themselves.

122
00:15:39,880 --> 00:15:47,120
And the result, what it means to be enlightened is that you have let go, you have no sense

123
00:15:47,120 --> 00:15:52,200
of trying to control things or trying to force things, but you have no sense of things

124
00:15:52,200 --> 00:15:59,040
being deterministic either, there's no idea like that, there's something flawed about

125
00:15:59,040 --> 00:16:08,880
the idea of determinism, because of its very approach, it's a poor way of using your

126
00:16:08,880 --> 00:16:13,720
mind, or even to use, of course using language, right, I'm using the words you and I

127
00:16:13,720 --> 00:16:19,840
and so on, we can't avoid it, but I don't think it should be very hard to understand what

128
00:16:19,840 --> 00:16:27,120
I'm saying, I think the difficulty comes from getting around our own views and our own

129
00:16:27,120 --> 00:16:33,920
beliefs and our way of looking at things that so very caught up in entities and things

130
00:16:33,920 --> 00:16:43,560
and also theories and views, and all I'm saying, and all we're saying in Buddhism is to

131
00:16:43,560 --> 00:16:50,840
observe, to practice, to be here now, to not have any of the baggage, like when I say

132
00:16:50,840 --> 00:16:58,920
be here now, I'm not saying you let there be a self here now, right, it doesn't take self

133
00:16:58,920 --> 00:17:07,800
or free will or determinism or anything like that to be here now, to be mindful, to see

134
00:17:07,800 --> 00:17:18,080
things clearly, for there to arise clarity and insight, so I don't know if that makes

135
00:17:18,080 --> 00:17:28,040
it clear, but just to be clear, I'm not trying to make it clear, I'm not trying to, have

136
00:17:28,040 --> 00:17:35,640
you leave this video, have you watched this video, leave, go back to your life thinking,

137
00:17:35,640 --> 00:17:41,400
he gave me the right answer, I'm not giving you an answer, I'm giving you, we're giving

138
00:17:41,400 --> 00:17:49,280
you a direction, a way to see things more clearly, a way to free yourself from the need

139
00:17:49,280 --> 00:17:57,340
for theories, the need for speculation, the need for dogma and that's what we teach in

140
00:17:57,340 --> 00:18:03,600
mindfulness, meditation, practice, if you're interested, if you really want an answer, please

141
00:18:03,600 --> 00:18:08,520
read my booklet on how to meditate, I think it gives a good basic understanding of how

142
00:18:08,520 --> 00:18:15,720
to be mindful and try it, to practice and see, and they can pretty much guarantee you'll

143
00:18:15,720 --> 00:18:24,760
have no use for theories and views and ideas like that, just be mindful, okay, thank you

144
00:18:24,760 --> 00:18:40,920
so much.

