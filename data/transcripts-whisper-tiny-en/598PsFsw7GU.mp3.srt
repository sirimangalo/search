1
00:00:00,000 --> 00:00:06,000
Hi, so today I will continue this series on how to meditate.

2
00:00:06,000 --> 00:00:11,000
In this series, I will explain a little bit more about the technique during sitting meditation

3
00:00:11,000 --> 00:00:14,000
or even during walking meditation.

4
00:00:14,000 --> 00:00:20,000
And then I will give a demonstration of how to practice walking meditation.

5
00:00:20,000 --> 00:00:25,000
First of all, the foundations of meditation practice there are four.

6
00:00:25,000 --> 00:00:32,000
And these are the foundation objects on which we learn to build what is called mindfulness

7
00:00:32,000 --> 00:00:37,000
or remembrance, being aware and creating this clear thought in our minds,

8
00:00:37,000 --> 00:00:43,000
which is not clinging or running away from the objects of awareness.

9
00:00:43,000 --> 00:00:51,000
So the foundations on which we build mindfulness are the body, the feelings, the mind,

10
00:00:51,000 --> 00:00:55,000
and something called dhamma, which means reality.

11
00:00:55,000 --> 00:01:00,000
But it means something which exists in the mind or something which the mind takes as an object.

12
00:01:00,000 --> 00:01:03,000
We generally call it mind object.

13
00:01:03,000 --> 00:01:07,000
So we have body, feelings, mind and mind object.

14
00:01:07,000 --> 00:01:10,000
The body is either walking or sitting.

15
00:01:10,000 --> 00:01:14,000
And when we do the walking meditation, we are going to pay attention to the foot.

16
00:01:14,000 --> 00:01:18,000
Our mind should be with the foot and our mindfulness should be established,

17
00:01:18,000 --> 00:01:23,000
saying to ourselves, step being right, step being left,

18
00:01:23,000 --> 00:01:26,000
and this is mindfulness in the body.

19
00:01:26,000 --> 00:01:30,000
Now as we are practicing, or when we do sitting meditation, rising,

20
00:01:30,000 --> 00:01:33,000
falling, this is mindfulness in the body.

21
00:01:33,000 --> 00:01:38,000
But as we are practicing, we have the other three feelings, the mind and mind objects,

22
00:01:38,000 --> 00:01:41,000
things which the mind takes as an object.

23
00:01:41,000 --> 00:01:46,000
First of all, feelings, so when we are doing sitting or when we are doing walking meditation,

24
00:01:46,000 --> 00:01:49,000
they might arise pain or aching or soreness.

25
00:01:49,000 --> 00:01:53,000
When we are walking, we stop first and then say to ourselves,

26
00:01:53,000 --> 00:01:55,000
pain, pain, pain.

27
00:01:55,000 --> 00:01:59,000
When we are sitting, we simply switch our attention to the pain,

28
00:01:59,000 --> 00:02:01,000
or to the aching, or to the soreness.

29
00:02:01,000 --> 00:02:02,000
The miss is called feelings.

30
00:02:02,000 --> 00:02:05,000
We say pain, pain, pain.

31
00:02:05,000 --> 00:02:08,000
Or if we feel happy, happy, happy, happy.

32
00:02:08,000 --> 00:02:12,000
If we feel calm, we say to ourselves, calm, calm.

33
00:02:12,000 --> 00:02:17,000
And this creates a clear thought about the feeling, not attaching to it,

34
00:02:17,000 --> 00:02:21,000
wanting for it to continue or wanting for it to stop.

35
00:02:21,000 --> 00:02:24,000
Number three, mindfulness of the mind.

36
00:02:24,000 --> 00:02:27,000
The mind is any sort of thinking.

37
00:02:27,000 --> 00:02:31,000
When we are thinking about the past, or thinking about the future,

38
00:02:31,000 --> 00:02:34,000
either good thinking or bad thinking, whatever kind of thinking it is,

39
00:02:34,000 --> 00:02:37,000
we simply say to ourselves, thinking, thinking, thinking,

40
00:02:37,000 --> 00:02:40,000
and then bring the mind back to the rising, falling,

41
00:02:40,000 --> 00:02:43,000
or to the foot, and when we are walking.

42
00:02:43,000 --> 00:02:46,000
And number four is the mind objects.

43
00:02:46,000 --> 00:02:50,000
In the beginning, this refers simply to emotional states of mind.

44
00:02:50,000 --> 00:02:53,000
So here we have, we call the five hindrances.

45
00:02:53,000 --> 00:02:57,000
And these are states of mind, which will disturb the meditator's practice.

46
00:02:57,000 --> 00:03:01,000
It will become an obstacle in the practice of mindfulness.

47
00:03:01,000 --> 00:03:02,000
So we want to see clearly.

48
00:03:02,000 --> 00:03:03,000
We don't see clearly.

49
00:03:03,000 --> 00:03:04,000
We want to be mindful.

50
00:03:04,000 --> 00:03:05,000
We can't be mindful.

51
00:03:05,000 --> 00:03:09,000
We want to sit at peace and become, and we are not peaceful or calm.

52
00:03:09,000 --> 00:03:14,000
It's because of these five things, which still certainly exist in the mind.

53
00:03:14,000 --> 00:03:21,000
And these are liking, disliking, drowsiness, distraction, and doubt.

54
00:03:21,000 --> 00:03:22,000
Now, these exist in the mind.

55
00:03:22,000 --> 00:03:28,000
We simply say to ourselves, liking, liking, disliking, disliking, drowsiness,

56
00:03:28,000 --> 00:03:32,000
distracted, distracted, distracted, or doubting, doubting.

57
00:03:32,000 --> 00:03:36,000
And once they go away, then we come back again to the rising,

58
00:03:36,000 --> 00:03:40,000
calling, or when we're walking, we've stopped, and then we continue walking.

59
00:03:40,000 --> 00:03:42,000
Again, after we acknowledge.

60
00:03:42,000 --> 00:03:45,000
These are called the four foundations of mindfulness.

61
00:03:45,000 --> 00:03:50,000
And these are the foundation on which we will build clear thought,

62
00:03:50,000 --> 00:03:54,000
clear awareness, our clear understanding of the option.

63
00:03:54,000 --> 00:04:00,000
So now I'll complete this practice with a demonstration of walking meditation.

64
00:04:00,000 --> 00:04:03,000
This will complete around the practice.

65
00:04:03,000 --> 00:04:08,000
And you can do walking first and then sitting afterwards, which is a general method.

66
00:04:08,000 --> 00:04:12,000
So we might walk for 10 minutes and then sit for 10 minutes.

67
00:04:12,000 --> 00:04:16,000
And now I'll give it a demonstration.

68
00:04:16,000 --> 00:04:22,000
Walking meditation is performed with the feet close together,

69
00:04:22,000 --> 00:04:34,000
the right hand holding the left hand, either in front or in back.

70
00:04:34,000 --> 00:04:39,000
And the eyes looking at the floor in front of you about six feet or two meters out.

71
00:04:39,000 --> 00:04:43,000
And don't practice with our eyes closed.

72
00:04:43,000 --> 00:04:46,000
The feet start close together and we start moving with the right foot.

73
00:04:46,000 --> 00:04:53,000
We move the foot at a simple pace, one foot length, and say to ourselves, as we move the foot,

74
00:04:53,000 --> 00:05:03,000
step being right, step being left, step being right,

75
00:05:03,000 --> 00:05:09,000
step being left and so on until we come to the end of our path,

76
00:05:09,000 --> 00:05:12,000
which might be about three meters, four meters ahead.

77
00:05:12,000 --> 00:05:17,000
And then we bring the foot which is behind up to the front and say to ourselves,

78
00:05:17,000 --> 00:05:22,000
as we move the foot, stop being, stop being, stop being.

79
00:05:22,000 --> 00:05:27,000
Once we're standing still, we say to ourselves, standing, standing, standing,

80
00:05:27,000 --> 00:05:30,000
standing, becoming aware of the standing position.

81
00:05:30,000 --> 00:05:32,000
And then we turn around.

82
00:05:32,000 --> 00:05:35,000
The method of turning around, just so you can see,

83
00:05:35,000 --> 00:05:39,000
so with the feet close together, turn with the right foot first,

84
00:05:39,000 --> 00:05:43,000
bring it off the floor, turn at 90 degrees, and say to yourself,

85
00:05:43,000 --> 00:05:49,000
one time, turning, and then the left foot, turning,

86
00:05:49,000 --> 00:05:54,000
then the right foot, again, turning, and then turning,

87
00:05:54,000 --> 00:05:58,000
and then you're facing the opposite direction, say to yourself,

88
00:05:58,000 --> 00:06:04,000
standing, standing, standing, and start walking back in the opposite direction.

89
00:06:04,000 --> 00:06:09,000
Step being right, step being left and so on.

90
00:06:09,000 --> 00:06:13,000
And you'll walk back and forth, back and forth in this manner,

91
00:06:13,000 --> 00:06:17,000
when you come to the other end, when the foot comes,

92
00:06:17,000 --> 00:06:20,000
when you come to the end, bring the back foot up, say stop being,

93
00:06:20,000 --> 00:06:25,000
stop being, stop being, stop being, standing, standing,

94
00:06:25,000 --> 00:06:35,000
turning, turning, turning, turning, turning,

95
00:06:35,000 --> 00:06:39,000
turning, standing, standing, standing,

96
00:06:39,000 --> 00:06:42,000
start walking against step being right,

97
00:06:42,000 --> 00:06:45,000
step being left, when something comes that you have to be mindful of,

98
00:06:45,000 --> 00:06:50,000
if it's a thought or a feeling that is interrupting your concentration,

99
00:06:50,000 --> 00:06:53,000
so stop in the middle of your step, stop being,

100
00:06:53,000 --> 00:06:56,000
stop being, stop being, stop being, and acknowledge first.

101
00:06:56,000 --> 00:06:59,000
If you feel distracted, so distracted, distracted,

102
00:06:59,000 --> 00:07:04,000
if you feel upset, upset, upset, bored, bored,

103
00:07:04,000 --> 00:07:07,000
if you have aching or aching, aching, or if you're thinking,

104
00:07:07,000 --> 00:07:09,000
so thinking, thinking, and so on.

105
00:07:09,000 --> 00:07:12,000
And we do in this manner for 10 minutes.

106
00:07:12,000 --> 00:07:14,000
Once the walking meditation is finished,

107
00:07:14,000 --> 00:07:17,000
then we do sitting meditation for another 10 minutes.

108
00:07:17,000 --> 00:07:20,000
As you get comfortable, you can increase the time,

109
00:07:20,000 --> 00:07:23,000
walking 15 minutes and then sitting for 15 minutes.

110
00:07:23,000 --> 00:07:26,000
And yes, it's important that we do the walking meditation first,

111
00:07:26,000 --> 00:07:30,000
as it has benefits which carry on into the sitting meditation.

112
00:07:30,000 --> 00:07:54,000
So that's all for now.

