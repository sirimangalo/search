1
00:00:00,000 --> 00:00:07,000
Good evening, we're in life, May 9th, May 2016.

2
00:00:11,500 --> 00:00:14,000
Happy birthday, everyone.

3
00:00:19,700 --> 00:00:22,500
I said, that to someone today and they said, what?

4
00:00:22,500 --> 00:00:25,500
Didn't you?

5
00:00:25,500 --> 00:00:27,300
I guess it's not customary to wish out

6
00:00:27,300 --> 00:00:31,800
whether people to be happy on your birthday.

7
00:00:31,800 --> 00:00:36,800
And that's everybody's birthday anyway.

8
00:00:36,800 --> 00:00:45,800
We're all born every moment.

9
00:00:45,800 --> 00:00:49,800
So today's quote is about rejoicing.

10
00:00:49,800 --> 00:00:52,800
It's about doing good.

11
00:00:52,800 --> 00:00:56,500
It's about the benefits of doing good.

12
00:00:56,500 --> 00:01:00,900
I'm going to say there are four benefits of doing good.

13
00:01:00,900 --> 00:01:06,500
You feel good about yourself.

14
00:01:06,500 --> 00:01:10,200
Other people say, what are the four?

15
00:01:10,200 --> 00:01:14,700
Good things happened to you in this life.

16
00:01:14,700 --> 00:01:17,000
Why do I think there are four?

17
00:01:17,000 --> 00:01:20,200
Other people say good things about you.

18
00:01:20,200 --> 00:01:23,900
You feel happy because you've done good things.

19
00:01:23,900 --> 00:01:25,900
So good things happened to you.

20
00:01:25,900 --> 00:01:27,300
You know, I don't remember.

21
00:01:27,300 --> 00:01:34,300
The fourth one is good things happened in the next life.

22
00:01:34,300 --> 00:01:38,800
This is actually one of the Dhamapada verses that way.

23
00:01:38,800 --> 00:01:40,800
We've been over, right?

24
00:01:40,800 --> 00:01:46,400
Idhamodati, Pachamodati, Katapunya, Ubyata, Modati.

25
00:01:46,400 --> 00:01:50,200
So Modati is open Modati.

26
00:01:50,200 --> 00:01:56,400
This Vakamav is who Dhamata know, having seen the goodness,

27
00:01:56,400 --> 00:02:02,000
the purity of their own deeds.

28
00:02:02,000 --> 00:02:05,000
They were joyous.

29
00:02:05,000 --> 00:02:08,600
So Modati means to be happy or rejoice.

30
00:02:08,600 --> 00:02:18,000
But Modati is, it adds a path which means they're exceedingly joyful.

31
00:02:18,000 --> 00:02:27,600
They're happy, they're very happy when they see the good, the purity of their deeds.

32
00:02:27,600 --> 00:02:40,800
Idhamodati, they were joyous here, Pachamodati, they were joyous here after.

33
00:02:40,800 --> 00:02:56,200
One who has done good, the joyous is both times.

34
00:02:56,200 --> 00:03:13,000
Oh, sister Tikwondo, is that a sister formerly known as Valerie?

35
00:03:13,000 --> 00:03:18,000
Happy first number of truth, they have that right.

36
00:03:18,000 --> 00:03:30,400
Indoka, birth is suffering.

37
00:03:30,400 --> 00:03:43,680
Oh, this is someone in the US, who is Tikwondo, thought that was Valerie.

38
00:03:43,680 --> 00:03:55,440
So simple quote, you do good, you get good, you do bad, you get bad.

39
00:03:55,440 --> 00:04:02,480
We don't talk about rebirth too much, so it's not that important.

40
00:04:02,480 --> 00:04:06,480
The talking about thinking about death is important, it's important to understand that

41
00:04:06,480 --> 00:04:13,480
we die, the story of the weaver's daughter, who came to see the Buddha and the Buddha

42
00:04:13,480 --> 00:04:14,880
asked, where are you going?

43
00:04:14,880 --> 00:04:18,160
And she said, I don't know, and she said, where are you coming from?

44
00:04:18,160 --> 00:04:19,160
I don't know.

45
00:04:19,160 --> 00:04:24,880
And he said, don't you know, and she said, I do, and she said, do you know, and she said,

46
00:04:24,880 --> 00:04:25,880
I don't.

47
00:04:25,880 --> 00:04:34,280
And people were, that she was crazy, why don't you answer the Buddha's question?

48
00:04:34,280 --> 00:04:37,480
And then the Buddha said, what did you mean by those answers?

49
00:04:37,480 --> 00:04:42,640
Well, the Buddha doesn't care, the Buddha's not going to ask me, where am I going?

50
00:04:42,640 --> 00:04:47,320
Just like in this life, what he means by where am I going?

51
00:04:47,320 --> 00:04:51,040
Because she had spent a lot of time thinking about death after she'd heard the Buddha talk

52
00:04:51,040 --> 00:04:52,040
previously.

53
00:04:52,040 --> 00:04:57,560
So she said, where am I going means in the next life, of course, that's where I'm really

54
00:04:57,560 --> 00:05:02,120
going, but I don't know where I'm going.

55
00:05:02,120 --> 00:05:04,680
Or I've come from, I don't know that either.

56
00:05:04,680 --> 00:05:11,840
I don't know where I was before this life, I don't know.

57
00:05:11,840 --> 00:05:14,400
And then do you know, I don't you know?

58
00:05:14,400 --> 00:05:20,360
Well, yes, I do know something, I know I'm going to die, so yes, I do know.

59
00:05:20,360 --> 00:05:28,880
And then do you know, do I know when, how, where, five things we don't know?

60
00:05:28,880 --> 00:05:33,560
We don't know when we're going to die, we don't know where we're going to die, we don't

61
00:05:33,560 --> 00:05:38,480
know how we're going to die, we don't know where our body's going to go and we don't

62
00:05:38,480 --> 00:05:43,000
know where our mind is going to go.

63
00:05:43,000 --> 00:05:46,480
So I don't know.

64
00:05:46,480 --> 00:05:51,440
Death could come any time, it could come today, it could come tomorrow.

65
00:05:51,440 --> 00:06:06,720
Nobody knows, well, very few people know.

66
00:06:06,720 --> 00:06:12,440
And that's, that someone disturbing is a Buddhist, because are you ready?

67
00:06:12,440 --> 00:06:16,760
Ready for the big life, change, I mean death isn't really death.

68
00:06:16,760 --> 00:06:18,960
Death is just a big change, it's a life change.

69
00:06:18,960 --> 00:06:34,560
So you're ready for that life change, you're ready for what comes next.

70
00:06:34,560 --> 00:06:37,720
Most of us are not.

71
00:06:37,720 --> 00:06:42,240
But this is what we do, and this is what meditation does, it prepares you for death.

72
00:06:42,240 --> 00:06:46,640
When you meditate all of your life flashes before you eyes, your eyes, just as if you

73
00:06:46,640 --> 00:06:51,800
were going to die, you see all sorts of things that you thought you'd forgotten about, everything

74
00:06:51,800 --> 00:06:55,000
comes up.

75
00:06:55,000 --> 00:07:00,400
And so once you've worked it all out through meditation when you die, you're comfortable,

76
00:07:00,400 --> 00:07:05,640
there's no surprises, because you've seen the ins and outs of your mind, the good and

77
00:07:05,640 --> 00:07:06,640
the bad.

78
00:07:06,640 --> 00:07:12,280
So you're much better prepared for whatever comes, once the body fades away and all that's

79
00:07:12,280 --> 00:07:19,920
left is the mind, you have to taste your own mind, then it's real meditation, then it becomes

80
00:07:19,920 --> 00:07:36,240
real, all you're left with is your good and bad deeds.

81
00:07:36,240 --> 00:07:42,200
So this week's kind of quiet, but just got this at a video conference with my family,

82
00:07:42,200 --> 00:07:55,320
and talking about things, I don't know, they want me to come and stay, my brother's coming

83
00:07:55,320 --> 00:08:03,480
back from Taiwan on Saturday, and the other things they're talking about, he wants a

84
00:08:03,480 --> 00:08:09,920
case of beer, ready for him when he arrives, I mean it's not shocking, it's just like,

85
00:08:09,920 --> 00:08:15,600
what am I going to do with these people, how can I relate to them?

86
00:08:15,600 --> 00:08:20,200
And last week my stepmother was talking about having a big part of, if she was drunk at

87
00:08:20,200 --> 00:08:25,160
the time and she was talking about having a big part of weed, a big bag of weed ready for

88
00:08:25,160 --> 00:08:33,280
him, a marijuana ready for him, so I don't know what I'm going to do it, I was kind

89
00:08:33,280 --> 00:08:38,480
of, I suppose a little bit snarky on the hang on and I said, I'm sorry you're not going

90
00:08:38,480 --> 00:08:44,480
to be, because I think it's this weekend I'm actually busy and I've accepted some appointments

91
00:08:44,480 --> 00:08:50,560
because I don't know, I think there may be more important, so Saturday night I'm giving

92
00:08:50,560 --> 00:08:56,000
a talk in Mississauga, you're all welcome to come if you're in the area at the West

93
00:08:56,000 --> 00:09:03,480
End Buddhist monastery, and then Sunday we have this interfaith meeting with the interfaith

94
00:09:03,480 --> 00:09:12,200
for Penhamilton, and then Monday morning we're going to New York, I think I mentioned

95
00:09:12,200 --> 00:09:20,000
this, but I'm going to drive to Bicubodes monastery, and I just got a call this morning

96
00:09:20,000 --> 00:09:26,080
from the head monk in Stony Creek, and he's driving to New York's Monday morning, so he's

97
00:09:26,080 --> 00:09:32,640
going to drive with us, he's going to drive all the way, so I said, well gee, that's

98
00:09:32,640 --> 00:09:38,360
it's bizarre really, and on me and him have some kind of really strong connection, this

99
00:09:38,360 --> 00:09:43,600
isn't coincidence, I said, this isn't a coincidence, I don't know, I'm both going to New

100
00:09:43,600 --> 00:09:49,960
York and his name day, we've been very close for a long time even though always felt

101
00:09:49,960 --> 00:09:55,480
like he's very, we must have known each other before, he's someone I can say that about,

102
00:09:55,480 --> 00:10:03,720
not too many people I can say that about, he's one of them, so he'll be coming with us

103
00:10:03,720 --> 00:10:14,360
and we're going to go see Bicubode, go meet Bicubode for the first time, so there's all

104
00:10:14,360 --> 00:10:31,960
that, anybody have questions, go ahead, click the green, click the green question mark,

105
00:10:31,960 --> 00:10:51,960
and then write in your question.

106
00:10:51,960 --> 00:11:18,240
Thank you.

107
00:11:21,960 --> 00:11:30,680
How do meditate with more than one's kind of condi-meen?

108
00:11:30,680 --> 00:11:36,040
I mean, sometimes it's the same word, right?

109
00:11:36,040 --> 00:11:40,800
I don't understand.

110
00:11:40,800 --> 00:11:46,000
I'm sorry, I don't understand the question.

111
00:11:46,000 --> 00:11:59,760
There could be many different things.

112
00:11:59,760 --> 00:12:06,800
This month is Weisak, Weisakapuja, in the full moon of Weisakai, and on the 28th, we're having

113
00:12:06,800 --> 00:12:14,160
a big celebration in Mississauga, celebration square, if you're in the Toronto area, come on

114
00:12:14,160 --> 00:12:15,160
out.

115
00:12:15,160 --> 00:12:19,960
We'll have a meditation tent, and I think I'm giving a talk.

116
00:12:19,960 --> 00:12:25,960
There was some talk of me giving a talk, and so we'll see if that happens.

117
00:12:25,960 --> 00:12:27,960
It's okay if it doesn't.

118
00:12:27,960 --> 00:12:32,680
We'll just sit and put up our meditation side.

119
00:12:32,680 --> 00:12:35,680
Side sound tastes at the same time.

120
00:12:35,680 --> 00:12:37,720
Well, those are actually the same kind of.

121
00:12:37,720 --> 00:12:40,920
Those are the root-bacanda.

122
00:12:40,920 --> 00:12:49,680
It sounds and tastes are all root-bacanda, but there's no such state, and they can't

123
00:12:49,680 --> 00:12:51,280
happen at the same time.

124
00:12:51,280 --> 00:12:55,880
We focus on whatever is present at that moment.

125
00:12:55,880 --> 00:13:01,120
I mean, practically speaking, this means just pick one.

126
00:13:01,120 --> 00:13:03,120
Pick whichever one is clearest.

127
00:13:03,120 --> 00:13:06,040
Don't worry about, is this one at this moment, is this one?

128
00:13:06,040 --> 00:13:09,760
If something's happening, then just, if you see something, say seeing, if you hear

129
00:13:09,760 --> 00:13:18,000
something, say, here, it doesn't really matter which one.

130
00:13:18,000 --> 00:13:23,600
Educational contemplation and death is a meditation practice.

131
00:13:23,600 --> 00:13:28,920
You can look at my video on protective meditation, there's actually four types of meditation

132
00:13:28,920 --> 00:13:34,600
that protect you, protect your insight meditation, recommend looking at that video.

133
00:13:34,600 --> 00:13:43,000
Every one of my faithful followers here can link it, because there's some people are

134
00:13:43,000 --> 00:13:51,680
so good at linking videos, by and on the four protective meditations.

135
00:13:51,680 --> 00:13:53,840
Give you an answer to that question.

136
00:13:53,840 --> 00:13:58,760
There's also, if you don't know, there's a website called video.ceremungalow.org.

137
00:13:58,760 --> 00:14:04,040
I think it's still there.

138
00:14:04,040 --> 00:14:06,800
And it has, oops, it's lost it.

139
00:14:06,800 --> 00:14:11,960
It has quite a few of my videos categorized.

140
00:14:11,960 --> 00:14:17,920
I don't think it's up to date, but it has a lot of the older ones, Mac when I was doing

141
00:14:17,920 --> 00:14:35,360
ask a monk, is that kind of thing?

142
00:14:35,360 --> 00:14:49,680
Why does the doer delight?

143
00:14:49,680 --> 00:14:53,360
Isn't it best to give without thoughts of return?

144
00:14:53,360 --> 00:14:58,360
Yes, but that's because when you do that, you feel happy.

145
00:14:58,360 --> 00:15:10,120
If you're looking for happiness, no, it's not best to give without thoughts of return.

146
00:15:10,120 --> 00:15:13,280
If you're giving without thoughts of return, it's kind of stupid.

147
00:15:13,280 --> 00:15:14,280
Why are you giving?

148
00:15:14,280 --> 00:15:16,400
What is the purpose?

149
00:15:16,400 --> 00:15:21,000
You're giving to help the other person, how ridiculous?

150
00:15:21,000 --> 00:15:24,680
If everyone give it to help everyone else, the known one would ever benefit.

151
00:15:24,680 --> 00:15:33,240
The known one would ever benefit from giving, when given because it makes us happy.

152
00:15:33,240 --> 00:15:43,360
Altarism is wrong, altarism is problematic, and it's not real anyway.

153
00:15:43,360 --> 00:15:49,160
The only people who say they're altruistic are actually doing it because it makes them happy

154
00:15:49,160 --> 00:15:53,440
or makes them feel good about themselves or makes them feel proud of themselves.

155
00:15:53,440 --> 00:15:58,040
And boy, I'm so altruistic, that kind of thing, but normally just because it makes them

156
00:15:58,040 --> 00:16:04,240
happy, because it maintains their happiness, because it prevents them from suffering from

157
00:16:04,240 --> 00:16:16,720
the pain of jealousy and stinginess and so on.

158
00:16:16,720 --> 00:16:24,160
And just Google, you know, the dumbo, protective meditation, I think that's what it's called.

159
00:16:24,160 --> 00:16:29,520
I could find it for you.

160
00:16:29,520 --> 00:16:39,800
Yeah, I mean, happiness is not all associated with greed.

161
00:16:39,800 --> 00:16:44,000
There's happiness that is associated with greed, and there's happiness that's associated

162
00:16:44,000 --> 00:16:47,360
with wholesomeness.

163
00:16:47,360 --> 00:16:54,200
You can do a wholesome deed happily.

164
00:16:54,200 --> 00:17:08,240
What is pervasive suffering, I don't know, pervasive means constant, right, like it's

165
00:17:08,240 --> 00:17:21,040
all encompassing, you know, clear understanding of the word pervasive, spreading widely

166
00:17:21,040 --> 00:17:23,600
throughout, right?

167
00:17:23,600 --> 00:17:35,960
Now, the pandas are dukas, they're pretty pervasive, but I don't know what you're referring

168
00:17:35,960 --> 00:17:41,200
to, you have to give me a source.

169
00:17:41,200 --> 00:17:49,240
Where are you getting this word from?

170
00:17:49,240 --> 00:17:58,280
I think nobody can find there's no one looking.

171
00:17:58,280 --> 00:18:14,840
You get the dumbo, protective patient, it's not even there, you have to dumbo protection.

172
00:18:14,840 --> 00:18:26,680
Well, I think it's other meditation, right, you know, I mean a minute.

173
00:18:26,680 --> 00:18:30,200
It's not a very useful meditation practice, maybe it's not a very good video, maybe it's

174
00:18:30,200 --> 00:18:40,680
got a lot of downbox work, 72 out in front, it's probably okay.

175
00:18:40,680 --> 00:18:49,440
Yeah, and now I have to find it, because I remember how I titled it, other useful meditation

176
00:18:49,440 --> 00:18:56,440
practices or something like that.

177
00:18:56,440 --> 00:19:00,320
Simon, you're usually pretty good at that, and you missed this one.

178
00:19:00,320 --> 00:19:04,240
I kind of said, you know, on the wild goose chase, it wasn't protective, I think,

179
00:19:04,240 --> 00:19:14,720
label it as protective, yeah, those are the four, the number they are.

180
00:19:14,720 --> 00:19:40,720
I can't remember whether it was my teacher or one of them, but I heard these from first.

181
00:19:40,720 --> 00:20:03,040
Okay, well, we're not Tibetan Buddhists, so we're not Tibetan Buddhists, so better not

182
00:20:03,040 --> 00:20:09,720
to mix, you know, if you, I mean, grasp them about their explanations.

183
00:20:09,720 --> 00:20:14,640
If you follow Teravada Buddhism here, so if you want, I mean, if you're new, I suggest

184
00:20:14,640 --> 00:20:20,480
you read my booklet on how to meditate, it's linked at the top of this page, check out

185
00:20:20,480 --> 00:20:27,840
some of my videos on YouTube, that's, I mean, I'm afraid, I have a fairly specific focus,

186
00:20:27,840 --> 00:20:32,960
and so you have to kind of be into what I do to be interested in the things I'm going

187
00:20:32,960 --> 00:20:40,680
to say, I wouldn't say I'm very esoteric or specific, well, specific maybe, but fairly

188
00:20:40,680 --> 00:20:41,680
straight.

189
00:20:41,680 --> 00:20:47,720
It's not like I have specific things I want you to visualize or something, but fairly

190
00:20:47,720 --> 00:21:03,360
basic, you know, fundamental.

191
00:21:03,360 --> 00:21:08,680
Do you remember what initially helped you keep constant sense restraint, keeping your

192
00:21:08,680 --> 00:21:15,160
head down, not looking around, you know, monks have kind of a rule of that when we walk

193
00:21:15,160 --> 00:21:24,040
up in the world, we're not supposed to look at things, but mindfulness is of course the

194
00:21:24,040 --> 00:21:36,920
best.

195
00:21:36,920 --> 00:21:40,600
If you want to learn about sensory strain, there's some interesting stuff in the Visudhi

196
00:21:40,600 --> 00:21:47,600
manga, I think, under see the Nidasa, the first part of the Visudhi manga, look up, because

197
00:21:47,600 --> 00:21:55,080
morality is fourfold, look up fourfold morality, I may have even done a video on that actually,

198
00:21:55,080 --> 00:22:03,600
but it talks a little bit about sensory strain, they have been to give you some sort of

199
00:22:03,600 --> 00:22:10,760
fleshed out ideas, sensory strain, different kinds, different ways, and give some stories

200
00:22:10,760 --> 00:22:15,240
of monks who practice sensory strain.

201
00:22:15,240 --> 00:22:24,600
There was this monk who lived in a cave, and he never looked up, so he only knew what

202
00:22:24,600 --> 00:22:30,720
season it was by the flowers that they had these flowering trees, and when he saw the flowers

203
00:22:30,720 --> 00:22:36,720
on the ground, then he knew it was spring, and these monks came to visit him once, and

204
00:22:36,720 --> 00:22:42,440
they noticed that painted on the walls of his cave were all these beautiful murals and

205
00:22:42,440 --> 00:22:50,240
pictures of the Buddha's stages of the Buddha's life, and they said, wow, these are impressive,

206
00:22:50,240 --> 00:22:54,920
and he said to them, oh, yeah, I never noticed.

207
00:22:54,920 --> 00:23:00,680
He lived there for years, and he never even noticed that there was anything on the wall.

208
00:23:00,680 --> 00:23:11,840
The king heard about this, and he invited him to come and give a talk, and the monk refused.

209
00:23:11,840 --> 00:23:19,360
The king made an proclamation in order to have all the women in the kingdom had to have

210
00:23:19,360 --> 00:23:34,040
their breasts tied, so not with cloth, and against the law to feed their babies until

211
00:23:34,040 --> 00:23:41,520
this monk came down, his way of forcing the monk to come down was to starve off the babies

212
00:23:41,520 --> 00:23:58,640
in the kingdom, and finally the monk came down of compassion for these poor babies and

213
00:23:58,640 --> 00:24:06,920
their mothers, and so he came to see the king, the king and the queen, and before the king

214
00:24:06,920 --> 00:24:11,360
and the queen, and he said, when the king came and he paid respect to him, he said,

215
00:24:11,360 --> 00:24:16,120
may the king long live the king, and when the queen came, he respected him, he said, long

216
00:24:16,120 --> 00:24:22,200
live the king, and so they asked him, why do you say, why do you say long live the king

217
00:24:22,200 --> 00:24:23,200
twice?

218
00:24:23,200 --> 00:24:27,760
Well, I don't know which one of you it is, he doesn't even look up to see which one it

219
00:24:27,760 --> 00:24:33,200
was, so I just, you know, to be safe, he just said long live the king, or may the king live

220
00:24:33,200 --> 00:24:46,400
long as until they kind of, a brief explanation of how we are dying and being born for

221
00:24:46,400 --> 00:24:55,640
moment to moment, and well, have you read my booklet on how to meditate, recommend that

222
00:24:55,640 --> 00:25:06,600
as a start, I'm not going to explain it to you, I think you have to experience it, read

223
00:25:06,600 --> 00:25:16,320
my booklet, start practicing, and find it out that way, is there ever an exception to

224
00:25:16,320 --> 00:25:29,040
write speech, livelihoods, actions, I don't understand, like in me, and can you lie,

225
00:25:29,040 --> 00:25:36,520
and it's still be good, because you can't, I'm teravada, can the primary elements be

226
00:25:36,520 --> 00:25:42,400
noticed with hearings, being smelling as well, or can we only notice secondary elements

227
00:25:42,400 --> 00:25:54,560
with them? Why some, why do you give me these tough ones? They're derived, except for

228
00:25:54,560 --> 00:26:05,600
the physical, the physical is not derived, the physical you feel the direct. Last year

229
00:26:05,600 --> 00:26:12,720
asking, with hearing, seeing, smelling as well, no, you, they are, they have a special

230
00:26:12,720 --> 00:26:23,480
name, the, the pasaada is right, you, the jakupasada, pasaada means sensitivity or something

231
00:26:23,480 --> 00:26:34,120
like that, so it's not direct, and, but it's based on, you just don't, it's right,

232
00:26:34,120 --> 00:26:39,280
you don't, when you smell, you don't, you're not smelling the particles, you're smelling

233
00:26:39,280 --> 00:26:45,320
the, that which is derived from the contact of the particles, but it's still part, you know,

234
00:26:45,320 --> 00:27:03,800
it's still the, it's still root, but no, it's not, it's not the primary, it's derived.

235
00:27:03,800 --> 00:27:08,800
The whole story for the manko great sensory strain, I think it's in the wisudimanga, I think

236
00:27:08,800 --> 00:27:13,200
that's where I'm remembering it from. Yeah, yeah, it's from the wisudimanga, I'm not

237
00:27:13,200 --> 00:27:19,320
sure if the whole thing is, or if I've heard it elsewhere as well, but I'm pretty sure.

238
00:27:19,320 --> 00:27:28,320
Check out the, the end of the cilani desa, the end of the, the first part of the wisudimanga,

239
00:27:28,320 --> 00:27:37,720
the path of purification in English, it's on the internet. I don't answer questions

240
00:27:37,720 --> 00:27:43,400
about my own attainment, that would be against the monastic rules. It's just not appropriate.

241
00:27:43,400 --> 00:27:49,440
You don't know, I think it's all that helpful.

242
00:27:49,440 --> 00:28:06,520
Sangha, you should get the bicubodes, compendium of abidhamma, it's a called.

243
00:28:06,520 --> 00:28:15,760
The abidhamma, the sangha, the translation and explanation of the abidhamma, the sangha.

244
00:28:15,760 --> 00:28:29,080
Good for you, one. Well done. Sounds like you're doing the right thing, but I'll

245
00:28:29,080 --> 00:28:32,400
see you as mindfulness. You know, if you haven't read my booklet, read my booklet, you

246
00:28:32,400 --> 00:28:38,080
might find it useful. That kind of meditation is quite useful for

247
00:28:38,080 --> 00:29:03,000
the kickarding of the senses.

248
00:29:03,000 --> 00:29:10,000
I guess.

249
00:29:10,000 --> 00:29:15,000
I guess, probably, I don't know.

250
00:29:15,000 --> 00:29:18,000
Rheirokarni, what's his name?

251
00:29:18,000 --> 00:29:20,000
Rheirokarni.

252
00:29:20,000 --> 00:29:22,000
Rheirokarni?

253
00:29:22,000 --> 00:29:24,000
I think something about that.

254
00:29:24,000 --> 00:29:29,000
You know who I'm talking about?

255
00:29:29,000 --> 00:29:32,000
Rheirokarni.

256
00:29:32,000 --> 00:29:35,000
Now, I didn't know Sanko when I was in Sri Lanka the first time.

257
00:29:35,000 --> 00:29:37,000
I don't think we ever met, right?

258
00:29:37,000 --> 00:29:38,000
Did we?

259
00:29:38,000 --> 00:29:41,000
I don't think we met when I was in Sri Lanka the first time.

260
00:29:41,000 --> 00:29:43,000
Or I'm my forgetting.

261
00:29:43,000 --> 00:29:46,000
I don't think so.

262
00:29:46,000 --> 00:29:53,000
Didn't meet Sanko until I went back, right?

263
00:29:53,000 --> 00:30:01,000
I met Sanko on the internet, actually.

264
00:30:01,000 --> 00:30:05,000
Rheirokarni, Chandavima.

265
00:30:05,000 --> 00:30:07,000
Yeah, he's apparently really good.

266
00:30:07,000 --> 00:30:15,000
I got all of his books intending to learn Sanko's to read them and never didn't get around to learning Sanko's.

267
00:30:15,000 --> 00:30:39,000
Not well enough, I do.

268
00:30:39,000 --> 00:30:49,000
Okay, well then that's all for tonight.

269
00:30:49,000 --> 00:30:53,000
Thank you all for tuning in.

270
00:30:53,000 --> 00:31:17,000
See you all next time.

