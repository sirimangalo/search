WEBVTT

00:00.000 --> 00:07.000
Hi, now I'd like to talk about the number three reason why everyone should practice meditation.

00:07.000 --> 00:12.000
The number three reason why everyone should practice meditation is because meditation does away with suffering.

00:12.000 --> 00:17.000
Now in the meditative tradition we understand there to be two kinds of suffering that exists.

00:17.000 --> 00:20.000
The first kind of suffering is a bodily suffering.

00:20.000 --> 00:25.000
This is the kind of pain which we feel in our back, in our legs, in our head, in our stomach,

00:25.000 --> 00:29.000
in all of the various parts of our bodies.

00:29.000 --> 00:36.000
This is something which we tend to rely more on physical doctors.

00:36.000 --> 00:40.000
We've never think to practice meditation as a way to get rid of our physical pain.

00:40.000 --> 00:48.000
Actually meditation is something which really does allow us to, at the very least, overcome physical suffering.

00:48.000 --> 00:52.000
The other kind of suffering which we understand is, of course, mental suffering.

00:52.000 --> 01:03.000
And this I've already dealt with in some of my other videos, but it's important to specify in this video that specifically what we're trying to do is get rid of mental suffering.

01:03.000 --> 01:10.000
Because all of these things I've been talking about are a very real form of stress and suffering and pain in our lives.

01:10.000 --> 01:14.000
Pain is not just something which is physical, that actually the mind does feel pain.

01:14.000 --> 01:18.000
And when we feel pain in the mind it can be much worse than pain existing in the body.

01:18.000 --> 01:26.000
So how is it that meditation is able to do away with these two very important negative aspects of existence?

01:26.000 --> 01:31.000
Well, for starters it allows us to understand that these are just a part of our existence.

01:31.000 --> 01:35.000
That physical discomfort is actually not an uncomfortable thing.

01:35.000 --> 01:38.000
The discomfort arises in the mind.

01:38.000 --> 01:45.000
When we have what we call physical pain, actually it's simply a phenomenon which arises in ceases.

01:45.000 --> 01:48.000
Nothing intrinsically about it that says this is bad.

01:48.000 --> 01:52.000
You know, we often think that it's our body trying to tell us something.

01:52.000 --> 01:55.000
Well, it's our body simply saying that this is pain.

01:55.000 --> 02:00.000
It's up to us in our mind, it's up to the mind as to whether it's going to say this is good or this is bad.

02:00.000 --> 02:03.000
The body has no way of saying that something is good or is bad.

02:03.000 --> 02:07.000
It simply gives different responses to different phenomena.

02:07.000 --> 02:10.000
We could say that pain is good if we wanted.

02:10.000 --> 02:14.000
There are people who have trained themselves to think of pain as a good thing and something which they like.

02:14.000 --> 02:21.000
People who like to feel pain, this is often a sign of mental sickness or at least a very basic level.

02:21.000 --> 02:29.000
It's something that we consider to be a sort of a disease in the mind to consider that pain is a good thing.

02:29.000 --> 02:31.000
But it's possible.

02:31.000 --> 02:34.000
In meditation we learn to see pain as simply pain.

02:34.000 --> 02:39.000
So when we have pain in the back, we say to ourselves pain, pain reminding ourselves that it's just pain.

02:39.000 --> 02:40.000
We have pain in the head.

02:40.000 --> 02:45.000
Reminding ourselves that it's just pain doesn't have to be something that gives us stress and suffering.

02:45.000 --> 02:50.000
It doesn't have to be something that's seen as negative, that we have to get rid of, that we have to do away with.

02:50.000 --> 02:54.000
There's really nothing intrinsically wrong with us with it.

02:54.000 --> 03:01.000
But this is something very easy to say, but if we didn't have a tool to allow us to come to this realization, we could never do it in reality.

03:01.000 --> 03:03.000
The meditation is that tool.

03:03.000 --> 03:09.000
When we come to realize for ourselves what it is, really what it is, then we're able to do away with the suffering.

03:09.000 --> 03:16.000
But on a deeper level, when we do away with mental suffering, and of course mental suffering is the very same.

03:16.000 --> 03:22.000
When we think about something and it makes us sad or it makes us angry, makes us upset.

03:22.000 --> 03:24.000
This is only a judgment call in our part.

03:24.000 --> 03:29.000
Even thoughts, even memories, worries about the future, things which we're unsure of.

03:29.000 --> 03:34.000
All of these things can be very simply neutral if we understood them for what they are.

03:34.000 --> 03:38.000
So when we say to ourselves thinking, we remind ourselves that it's just a thought.

03:38.000 --> 03:42.000
When we say to ourselves, we're remembering, or planning, or worrying, or so on.

03:42.000 --> 03:47.000
We come to see that actually there's nothing about what we're worrying that is worth worrying about.

03:47.000 --> 03:49.000
We come to see that it's simply what it is.

03:49.000 --> 03:52.000
It's something that comes and goes, it's not really under our control.

03:52.000 --> 03:55.000
We can't change it, we can't make it be the way we want.

03:55.000 --> 04:01.000
When we're able to do away with a great deal of our mental suffering, the wonderful thing about meditation is,

04:01.000 --> 04:05.000
one of the wonderful things about meditation is that it does change the body.

04:05.000 --> 04:12.000
That many things about our body do change, and most of these are the sort of sicknesses which arise based on stress.

04:12.000 --> 04:18.000
So people who have to have massages because of tension in the back, they find that the tension disappears and never comes back.

04:18.000 --> 04:23.000
And of course they have to go through quite a bit of suffering to be able to get to that point.

04:23.000 --> 04:31.000
Because the meditation has to, you have to come to the point where you actually let go before you can get there.

04:31.000 --> 04:35.000
But it actually does, in the end, do away with these pains and they never come back.

04:35.000 --> 04:37.000
So it's not simply that you have to bear with them.

04:37.000 --> 04:44.000
Once you learn to accept them and let go of them, it's part of the letting go that allows them to heal.

04:44.000 --> 04:51.000
People who have suffered from many different sicknesses that doctors just couldn't fix bodily illnesses.

04:51.000 --> 04:54.000
In the end, turn out to be simply based on stress.

04:54.000 --> 04:58.000
And when you do away with the stress, you do away with the sickness.

04:58.000 --> 05:05.000
So I think this sort of gives a good understanding of how meditation is able to do away with physical suffering.

05:05.000 --> 05:07.000
A mental suffering, of course, is obvious.

05:07.000 --> 05:19.000
The doing away with all of the unpleasant mind states, such as anger, greed, delusion, conceit and views and so on, does away with,

05:19.000 --> 05:25.000
or is the very mental sickness which mental suffering which we're trying to do away with.

05:25.000 --> 05:31.000
Meditation really is something that allows us to get rid of suffering, to do away with all kinds of suffering.

05:31.000 --> 05:36.000
And I suppose some people will be saying that suffering is actually a very important part of life,

05:36.000 --> 05:39.000
and it weren't for suffering, we wouldn't learn anything or so on.

05:39.000 --> 05:45.000
But I think that's sort of the point here, is that suffering is there to learn, but most of us don't ever take the time to learn anything from it.

05:45.000 --> 05:50.000
And meditation is allowing us to understand it so that once we learn everything there is to know about suffering,

05:50.000 --> 05:52.000
we don't need to suffer anymore.

05:52.000 --> 05:57.000
So thank you for tuning in, this is number three, and please look forward in the future for the resting videos.

