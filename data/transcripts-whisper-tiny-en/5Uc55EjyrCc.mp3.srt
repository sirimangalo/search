1
00:00:00,000 --> 00:00:15,280
Good evening everyone, continuing our study of the Dhamapada, today verse 205, which reads

2
00:00:15,280 --> 00:00:39,720
as follows, but we wake a Rasang Pitva, Rasang Pasyma Satcha, Nidarohoti, Nipapo, Tama Piti, Rasang

3
00:00:39,720 --> 00:00:59,560
Pampiwang, which means having tasted, having drank, having drank the flavor of seclusion

4
00:00:59,560 --> 00:01:27,880
and the flavor of tranquility or quietude, Upasamur, one has no suffering, no pain, no sorrow

5
00:01:27,880 --> 00:01:46,160
and no evil, drinking the taste of rapture in the Dhamapada Piti, Rasang Piwang, there's

6
00:01:46,160 --> 00:01:59,020
no suffering and no cause of suffering, no evil, papa, Nipapo, for one who tastes the

7
00:01:59,020 --> 00:02:14,220
flavor of seclusion and quietude, Upasamur, the quenching or the pacifications of the

8
00:02:14,220 --> 00:02:27,540
pis is what it really means, so this verse was taught in regards to Disa, regards

9
00:02:27,540 --> 00:02:34,900
to the Buddha's passing away, there was a monk Disa, four months before the Buddha passed

10
00:02:34,900 --> 00:02:45,620
away, he declared to the monks, he said, four months I'm going to pass away, it was a big

11
00:02:45,620 --> 00:02:53,380
deal, they say the earth shook I think, it was a big deal for the monks, they say those

12
00:02:53,380 --> 00:03:06,900
monks who were not yet enlightened, cried, wailed, moaned, those monks who were already

13
00:03:06,900 --> 00:03:18,180
enlightened, were moved, removed with a sense of the impermanence of all things and

14
00:03:18,180 --> 00:03:28,540
the importance of the clarity and the peace and the wisdom that comes from enlightenment,

15
00:03:28,540 --> 00:03:38,300
removed by the gravity of the situation, not disturbed by it, but moved, some monks would

16
00:03:38,300 --> 00:03:42,420
get together and those that hadn't become enlightened yet and were depending on the

17
00:03:42,420 --> 00:03:48,780
Buddha would get together and think to themselves, something like King Karizama, which means

18
00:03:48,780 --> 00:04:02,660
what are we to do, King Noko Karizama, what in the world will we do, we spend a lot of

19
00:04:02,660 --> 00:04:09,980
time concerned with this and they say the monks and lay people as well of course, not

20
00:04:09,980 --> 00:04:19,580
just the monk, spent a lot more time being around the Buddha, trying to get their last

21
00:04:19,580 --> 00:04:24,980
chance to be in the presence of the Buddha and who wouldn't, right, what do you know

22
00:04:24,980 --> 00:04:35,460
who wouldn't, there was one monk his name was Tisa and while all this was going on, he

23
00:04:35,460 --> 00:04:43,460
was ignoring everybody, sitting off in a corner and the monks came up to him and asked

24
00:04:43,460 --> 00:04:49,420
them what he was doing and he ignored them and he said, hey, the Buddha said he's

25
00:04:49,420 --> 00:04:57,780
going to pass away in four months, don't you know, they didn't even reply, didn't say

26
00:04:57,780 --> 00:05:07,380
anything, so the monks thought to themselves, this isn't right and they went to the Buddha

27
00:05:07,380 --> 00:05:15,300
and they said, look, then they're both service, there's this monk, Tisa, they certainly

28
00:05:15,300 --> 00:05:20,740
doesn't care at all about you, doesn't come to see you since you've declared your

29
00:05:20,740 --> 00:05:25,100
passing away, all he does is sit in the corner, doesn't even pay any attention to anything

30
00:05:25,100 --> 00:05:36,860
something's wrong with this monk, he has no love for the Buddha and so the Buddha called

31
00:05:36,860 --> 00:05:45,380
said, well, bring him here and he said to say, is this true and is it confirmed and

32
00:05:45,380 --> 00:05:54,780
he said, what's the meaning of this? Maybe he didn't say exactly that, but it's a good

33
00:05:54,780 --> 00:06:01,100
phrase, a good question, what is the meaning of this? We say it in English one more angry

34
00:06:01,100 --> 00:06:10,180
I think, but he wasn't angry obviously. And Tisa said, I know most sir, I heard that

35
00:06:10,180 --> 00:06:19,420
you're passing away and I thought to myself, I'm not enlightened yet. And what should

36
00:06:19,420 --> 00:06:27,060
I be doing? You know, now the Buddha is here in front of us, for a sankaya, a sankaya

37
00:06:27,060 --> 00:06:34,260
means uncountable, an uncountable period of time, four of them. That's 100,000 great

38
00:06:34,260 --> 00:06:41,780
eras, no great eras, like a big bang to a big crunch kind of thing, like the universe,

39
00:06:41,780 --> 00:06:47,900
100,000 of those, and those are small compared to a sankaya, those are nothing compared

40
00:06:47,900 --> 00:06:54,380
to a sankaya plus four of those. That's how long it took for this Buddha to this being

41
00:06:54,380 --> 00:07:01,860
to become a Buddha. And after all that time, we have such a short time to hear what he

42
00:07:01,860 --> 00:07:11,980
has to say and learn from him and practice his teachings, and he thought, what a shame,

43
00:07:11,980 --> 00:07:17,620
how shameful of me if during the time that the Buddha was here, I didn't become enlightened.

44
00:07:17,620 --> 00:07:25,900
And so, reasonably spent his time in meditation. And the Buddha, of course, had sad

45
00:07:25,900 --> 00:07:35,140
to, very good. And he said, if anyone loves me, if anyone cares for me, respects me,

46
00:07:35,140 --> 00:07:42,860
appreciates me, they will do as decent as those of you who bring candles and flowers and

47
00:07:42,860 --> 00:07:50,500
incense to me, you're not doing the greatest homage, but the one who practices my teaching,

48
00:07:50,500 --> 00:07:57,660
dhamma nudhamma, patipa nul, one who practices my teaching to realize the truth. Practice

49
00:07:57,660 --> 00:08:04,900
is the truth. Practice is the dhamma to realize the dhamma. That's what it means. You practice

50
00:08:04,900 --> 00:08:09,260
the dhamma, so one part of the Buddha's teaching is how to practice. The other part of

51
00:08:09,260 --> 00:08:20,460
the Buddha's teaching is the truth. And you practice one to realize the other. That person

52
00:08:20,460 --> 00:08:27,420
pays homage with the greatest sort of homage. That person truly loves me, kind of thing.

53
00:08:27,420 --> 00:08:41,700
And then he thought this verse. And so the verse is not exactly, it's not exactly a summary

54
00:08:41,700 --> 00:08:47,860
of the story by any means. It's not, it's only related tangentially. But this seems

55
00:08:47,860 --> 00:08:53,460
like the sort of thing the Buddha did. It's the sort of thing a teacher does is when an

56
00:08:53,460 --> 00:08:58,340
opportunity for a lesson arises, sometimes the lesson they teach is only tangentially related

57
00:08:58,340 --> 00:09:05,380
to the opportunity because their intention is to teach the important lesson. That being

58
00:09:05,380 --> 00:09:12,260
said, it is related. It's clear why the Buddha taught this verse. Three lessons from this

59
00:09:12,260 --> 00:09:18,580
verse and the story. The first lesson comes from the story. It's the obvious lesson. It's

60
00:09:18,580 --> 00:09:27,660
a very classical Buddhist lesson. It's instructional for new Buddhists and that's that

61
00:09:27,660 --> 00:09:34,660
Buddhism is a practice. Whatever you want to say about Buddhism, you can call it what

62
00:09:34,660 --> 00:09:39,700
you like. You can cling to whichever. Hold on, focus on whichever part of Buddhism you

63
00:09:39,700 --> 00:09:48,580
like. It's impossible to deny with texts that we have. Buddhism is about practice. The

64
00:09:48,580 --> 00:09:55,100
Buddha said it himself. He often criticized monks who just study. He certainly criticized

65
00:09:55,100 --> 00:10:02,300
or not criticized, but put in their place, people who simply worship. Because of course

66
00:10:02,300 --> 00:10:06,100
there's nothing wrong with paying homage with candles and flowers and incense, which

67
00:10:06,100 --> 00:10:13,020
we see Buddhists do. There's nothing wrong with chanting and religious, religious ceremony.

68
00:10:13,020 --> 00:10:20,100
But there's nothing great about it either. There's nothing deeper profound or core about

69
00:10:20,100 --> 00:10:29,420
it. Certainly isn't the sort of thing the Buddha taught. The Buddha favored always those

70
00:10:29,420 --> 00:10:37,660
who practice. And so for all Buddhists, of course, it's a lesson because we've grew up

71
00:10:37,660 --> 00:10:44,980
Buddhist after we grew up with Buddhist culture and we are very focused on ceremony and

72
00:10:44,980 --> 00:10:52,260
ritual. Sometimes it's very much in line with the Buddha's teaching. Sometimes it's not.

73
00:10:52,260 --> 00:10:57,580
The problem of course with ritual and ceremony and culture is that they are not always

74
00:10:57,580 --> 00:11:03,260
grounded or connected to the religion and it's easy for them or it's possible for them

75
00:11:03,260 --> 00:11:13,620
to drift. And you see a lot of Buddhist cultures and societies engaging in practice is that

76
00:11:13,620 --> 00:11:21,220
it seems very un-Buddhist or have drifted from the original teachings. And so while

77
00:11:21,220 --> 00:11:27,300
these things are good often, they're only good if they're connected and supportive of

78
00:11:27,300 --> 00:11:34,260
the practice. Culture is like a shell. Ritual and all that is also like a shell that

79
00:11:34,260 --> 00:11:46,460
protects the egg, protects what's inside. It can be very useful as a support, but only

80
00:11:46,460 --> 00:11:55,740
if it's supportive, not if it's contradictory or unconnected. And it's useful for

81
00:11:55,740 --> 00:12:01,060
our new Buddhists as well who want to know what is Buddhism. So these sorts of teachings

82
00:12:01,060 --> 00:12:08,020
are very instrument and instructional in terms of giving a clear view of where Buddhism

83
00:12:08,020 --> 00:12:12,260
focuses. Because of course, if you read texts or if you read theory, if you read what

84
00:12:12,260 --> 00:12:17,620
people have written about Buddhism and theory, it seems like so much and there's so

85
00:12:17,620 --> 00:12:24,740
many aspects of it. And so it's good to have sort of a focus on lens through which

86
00:12:24,740 --> 00:12:32,140
to view all of the Buddha's teaching, not to be memorized or thought about, talked about,

87
00:12:32,140 --> 00:12:38,340
talked about, to be practiced. You want to follow the Buddha's teaching, go and sit

88
00:12:38,340 --> 00:12:45,860
in the corner or in a room in a quiet place like this and practice them.

89
00:12:45,860 --> 00:12:50,540
It's instructional when you're listening to a dhamma talk like this. Sometimes you'll

90
00:12:50,540 --> 00:12:55,340
be listening and you're doing, I know people watch these on YouTube or even listen to

91
00:12:55,340 --> 00:13:03,220
them when they're driving and it can be useful, I think, but it's important that you're

92
00:13:03,220 --> 00:13:10,140
mindful of the fact that this is meant to be practiced, not just to be thought about,

93
00:13:10,140 --> 00:13:14,660
enjoyed and that sort of thing. If you really want to follow my teaching or the teaching

94
00:13:14,660 --> 00:13:22,340
that I present through that comes through me that I say as the Buddha's teaching, you

95
00:13:22,340 --> 00:13:25,460
really have to be practicing it. And so when you're sitting here listening, you should

96
00:13:25,460 --> 00:13:30,180
really be trying to be mindful. That's your listening. When I give the talk, I should

97
00:13:30,180 --> 00:13:36,980
try to be mindful, even of my lips moving, being mindful of that feeling. When you hear

98
00:13:36,980 --> 00:13:43,220
my voice, you should be mindful of hearing. When you feel, of course, sitting cross-legged,

99
00:13:43,220 --> 00:13:51,300
you'll feel pain or you'll feel tension. You should note that, hot or cold, that sort

100
00:13:51,300 --> 00:14:00,820
of thing. All of this should be noted. Then you can really say you're listening to the

101
00:14:00,820 --> 00:14:07,220
dhamma. It's honestly, if you talk about listening, listening is a way of describing meditation.

102
00:14:07,220 --> 00:14:12,500
You're listening to the dhamma. It's not just the sound of my voice or the words.

103
00:14:15,860 --> 00:14:24,020
It's the sound, it's the experience. And of course, all the mental activity that goes on,

104
00:14:24,020 --> 00:14:32,340
you like it, you dislike it, you feel happy, you feel unhappy, doubt, confusion. All of these

105
00:14:32,340 --> 00:14:36,500
things are the dhamma, and you have to listen to them, because I'll tell you something, I'll teach

106
00:14:36,500 --> 00:14:52,820
you about yourself. So that's the first lesson. The second lesson is that Buddhism is a practice

107
00:14:52,820 --> 00:14:59,460
that very much is concerned with happiness. And you hear this expressed by different teachers

108
00:14:59,460 --> 00:15:05,460
in different ways. So we have to be a little bit careful, we have to be fairly precise about this.

109
00:15:05,460 --> 00:15:12,820
But no doubt, there's no doubt about the fact that Buddhism is very much focused on happiness

110
00:15:13,780 --> 00:15:20,340
and peace. And there's a sense that peace and happiness are one of the same or are interconnected.

111
00:15:22,180 --> 00:15:26,740
This is, I think we're still in the Sukhavaya, the happiness chapter of the dhamma Bhada.

112
00:15:27,700 --> 00:15:30,980
So all of these have been somehow related to the idea of happiness.

113
00:15:30,980 --> 00:15:38,580
And it really is the point of Buddhism. I don't want to quite say it's the goal though,

114
00:15:38,580 --> 00:15:44,820
you can say that as well, but I have a problem with language that talks about goals, because

115
00:15:45,460 --> 00:15:48,420
it's not how you should be thinking when you meditate, it's the point.

116
00:15:49,220 --> 00:15:54,580
This may be a better way of expressing it, because Buddhism, of course, isn't about the future,

117
00:15:54,580 --> 00:15:59,300
it isn't about trying to achieve something in a sense. It kind of is, but

118
00:15:59,300 --> 00:16:05,860
language is difficult, it's about the present moment. You're trying to achieve presence,

119
00:16:05,860 --> 00:16:11,380
you're trying to get to now, to have less and less concern about the future.

120
00:16:14,740 --> 00:16:19,620
But nonetheless, Buddhism is all about happiness. It really is the point,

121
00:16:19,620 --> 00:16:23,700
Buddhism isn't about doing the right thing, because God says it's right,

122
00:16:23,700 --> 00:16:30,900
to that sort of thing that comes in other religions, following some doctrine. That's why

123
00:16:30,900 --> 00:16:34,820
the focus in Buddhism is always on suffering and understanding suffering, not because we want

124
00:16:34,820 --> 00:16:41,940
to suffer, but because it's what's stopping us, it's in our way, it's between us and happiness.

125
00:16:41,940 --> 00:16:45,300
If there was none of that, the way would be clear to be happy.

126
00:16:45,300 --> 00:16:54,020
That's, again, it's not about a way that we would be happy. We're like a, the mind is like

127
00:16:56,260 --> 00:17:06,900
a shining gem, but it's a bhavasarangitang bhikavangitang. This mind is radiant, it's lustrous,

128
00:17:06,900 --> 00:17:15,140
it's beautiful, it's perfect, but it's soiled and covered over and solid by the filaments.

129
00:17:16,180 --> 00:17:20,020
So if you get rid of all of those, the things that are causing stress and suffering,

130
00:17:20,740 --> 00:17:24,180
you're left with something beautiful, something perfect.

131
00:17:24,180 --> 00:17:36,180
And there are two happiness. This verse gives a good example of the distinction between,

132
00:17:40,020 --> 00:17:44,580
well, it gives us an opportunity to talk about a couple of distinctions. The first one is

133
00:17:44,580 --> 00:17:50,420
mundane and super mundane, but the other one is tranquility and insight, because

134
00:17:50,420 --> 00:18:00,980
worldly happiness comes from meditation. When we talk about sensual pleasure and what most

135
00:18:01,700 --> 00:18:05,380
the vast majority of people think it was happiness, we don't consider that happiness.

136
00:18:06,580 --> 00:18:12,660
Yes, there's pleasure when you enjoy the senses, but we discard it. We don't even consider it

137
00:18:12,660 --> 00:18:20,260
because it's caught up with defound, it's caught up with attachment, and when you get when you want

138
00:18:20,260 --> 00:18:27,220
you, one more of it, it's very basic teaching and Buddhism, not an easy one and not to trivialize it,

139
00:18:27,220 --> 00:18:34,740
but it's important to understand it's a very core foundational idea that sensual pleasure we can.

140
00:18:34,740 --> 00:18:40,340
We don't have any sense that it could possibly be happiness. Our whole practice in a sense is to

141
00:18:40,340 --> 00:18:48,420
free ourselves from that, and so we discard it immediately. No, no, giving no quarter to the idea

142
00:18:48,420 --> 00:18:56,660
that sensual pleasure might be happiness. But worldly happiness does exist. It's this bhui waka,

143
00:19:00,260 --> 00:19:06,500
the seclusion, and it's mental seclusion, and it's of two types really, and that's where we get

144
00:19:06,500 --> 00:19:12,580
this distinction between tranquility and insight, because if you want to have a secluded mind, a

145
00:19:12,580 --> 00:19:20,100
mind that is secluded from defilement, a mind that is not harassed by worry and stress and anger

146
00:19:20,100 --> 00:19:27,540
and frustration and greed and desire. We have two choices. One, you secluded yourselves from those

147
00:19:27,540 --> 00:19:34,500
things that are causing those desires and aversions and worries and so on. And you focus on

148
00:19:34,500 --> 00:19:40,340
something that has no connection with that, like a simple object, often a light or a color,

149
00:19:41,220 --> 00:19:47,060
even the breath is a good object, and eventually your mind becomes so fixed and focused that you

150
00:19:47,060 --> 00:19:52,500
have no sense of any disturbance and your mind is very peaceful and quiet.

151
00:19:54,660 --> 00:19:59,940
The other option is to change the way you look at these things that cause you stress and suffering.

152
00:19:59,940 --> 00:20:06,340
So the things that cause you to get angry change the way you look at them, and this is what we

153
00:20:06,340 --> 00:20:11,540
do with our practice. When you feel pain, change the way you look at it, rather than getting harassed

154
00:20:11,540 --> 00:20:17,940
by disliking of the pain, see it as just pain, say to yourself pain, pain, you cultivate this ability

155
00:20:17,940 --> 00:20:30,660
to see it just as it is. And of the two, well, they are quite distinct. The first one, I would use

156
00:20:30,660 --> 00:20:38,180
the word artificial, and it's not really in a pejorative sense, it's in a limiting sense.

157
00:20:39,140 --> 00:20:47,460
Even the brahma world, rebirth in as a God, is artificial. In the basic sense that it's

158
00:20:47,460 --> 00:20:56,420
created and it's temporary, it's limited. When you cultivate this fixed nature of mind,

159
00:20:56,420 --> 00:21:01,380
big fixed state of mind, that's very peaceful, very pure, very strong, very good, very wholesome,

160
00:21:01,380 --> 00:21:10,340
very positive. You're avoiding or ignoring the relationship you have with all the other stuff.

161
00:21:10,340 --> 00:21:17,380
And so when you start practicing and the concentration dissipates, you're left back where you

162
00:21:17,380 --> 00:21:26,260
started with the potential and the eventual development of greed and anger and delusion.

163
00:21:27,860 --> 00:21:33,940
So it's temporary and it's artificial. And if you keep it up, you'll be reborn as a God,

164
00:21:33,940 --> 00:21:41,300
and you'll live for billions or billions of years or however long as a God, but even after that,

165
00:21:41,300 --> 00:21:46,980
you'll be born as a human again, and you might do all sorts of evil things. And the Buddha's time,

166
00:21:46,980 --> 00:21:53,380
or the Buddha's time, once the Buddha smiled at a pig, and we're walking on arms on any smile

167
00:21:53,380 --> 00:22:00,180
at this pig. And on the solemn smiling, and I thought, well, I'll have to ask him about that.

168
00:22:00,180 --> 00:22:04,500
And after they'd eaten, he'd said to the Buddha, he said, I saw you smile venomous, or why

169
00:22:04,500 --> 00:22:10,420
has he taken a smile? And the Buddha said, oh, that pig, did you see that pig? That pig was once

170
00:22:10,420 --> 00:22:19,380
a brahma. That pig was once a God. And it passed away from there and it was eventually born as a

171
00:22:19,380 --> 00:22:32,820
pig. And so there's this poem that I was in Burmese and translated into English. In Brahma's realm,

172
00:22:32,820 --> 00:22:41,060
she shines bright in pig's pen too. She finds delight. We're happy wherever we're reborn.

173
00:22:41,060 --> 00:22:51,300
We don't realize the danger of our situation, the precariousness of it.

174
00:22:53,700 --> 00:22:59,940
The other method, the one that we favor, not really trying to dismiss the other one,

175
00:22:59,940 --> 00:23:05,860
but there is a limit to it. And it can be very supportive in practicing insight meditation,

176
00:23:05,860 --> 00:23:12,660
but I think it can never be more than that. It can never be a replacement for ultimately changing

177
00:23:12,660 --> 00:23:18,980
your relationship with experience, which is what we do. We kind of streamline, simplify, and

178
00:23:20,340 --> 00:23:29,460
well, it can be a lot more unpleasant and a lot less grand or exalted. But it's a lot simpler

179
00:23:29,460 --> 00:23:38,500
and more straightforward. So meditators in this tradition often don't feel so happy or peaceful

180
00:23:39,700 --> 00:23:45,060
in the beginning at least. And it's important to understand that that's a sign that you're not

181
00:23:45,700 --> 00:23:52,180
proficient. I mean, that's really how the practice works. That's how you know where you are in

182
00:23:52,180 --> 00:23:57,300
your practice, how are you feeling? Are you full of greed, anger, and delusion? You're not there yet.

183
00:23:57,300 --> 00:24:03,860
We haven't cleaned off, polished and purified the mind completely yet.

184
00:24:08,340 --> 00:24:14,100
And this often leads to a great discouragement among meditators, and that's something to be

185
00:24:14,100 --> 00:24:21,380
addressed as well. This isn't shouldn't be a reason to be discouraged. So often a meditator

186
00:24:21,380 --> 00:24:25,620
will not feel peaceful and happy, especially when I talk. That's why I say we have to be careful,

187
00:24:25,620 --> 00:24:30,340
especially when we talk about happiness. It's often quite discouraging for those who are not

188
00:24:30,340 --> 00:24:35,540
happy in their meditation. Don't find it pleasant undertaking.

189
00:24:37,860 --> 00:24:46,020
And so I think the best and most honest way is to describe and explain the meditation in this way,

190
00:24:46,020 --> 00:24:59,780
that it's like polishing a dusty gem or maybe more viscerally, more clearly. It's about cleaning

191
00:24:59,780 --> 00:25:08,580
a pig's dye or it's about cleaning your house or a bathroom or something very dirty.

192
00:25:08,580 --> 00:25:17,140
So the reason why for some of us and for most of us I think meditation is difficult and often

193
00:25:17,140 --> 00:25:26,980
unpleasant is because it is very much corrupted. Their minds are not clean. And you shouldn't feel

194
00:25:26,980 --> 00:25:31,460
discouraged about that because that's the whole point of meditation. Don't feel like I can't

195
00:25:31,460 --> 00:25:38,660
practice meditation. I'm too corrupt and too defiled. I have too many attachments. What else are

196
00:25:38,660 --> 00:25:43,940
you going to do? I mean that's the whole point of meditation. That's why I would have created

197
00:25:43,940 --> 00:25:50,420
insight meditation. Why the Buddha taught? If you feel discouraged and if you say that sort of

198
00:25:50,420 --> 00:25:54,260
thing, you're really doing a disservice to Buddhism. It's a sign you haven't really understood.

199
00:25:54,260 --> 00:26:00,660
That's exactly what this is for. That's exactly the point. So don't be discouraged. Be encouraged

200
00:26:01,620 --> 00:26:08,100
that you're doing something that has the potential. Very real potential. Not even just potential,

201
00:26:08,100 --> 00:26:16,420
but has the effect of purifying the mind. That's the whole point. The Buddha called his teaching

202
00:26:16,420 --> 00:26:30,500
the Risudimanga, the path of purification. But nonetheless, and it shouldn't be denied that

203
00:26:30,500 --> 00:26:38,500
as you practice, you shouldn't and will gain happiness and peace. By the time you get through an

204
00:26:38,500 --> 00:26:43,940
intensive meditation course, it's an example. By the end of it, you often feel very peaceful,

205
00:26:43,940 --> 00:26:51,380
very happy, I won't use the term delicately, because it's not like you'll be jumping for joy

206
00:26:51,380 --> 00:26:58,100
or smiling or laughing. You might often smile, but it's not even about smiling. It's about peace.

207
00:26:58,100 --> 00:27:03,300
It's about freedom from suffering. It's about a sense and deep sense of peace,

208
00:27:03,300 --> 00:27:15,380
deep sense of contentment, satisfaction, because you stop clinging to things, because you've

209
00:27:15,380 --> 00:27:24,340
changed the way you look at things. You gain a profound temporary sense of equanimity

210
00:27:25,300 --> 00:27:32,900
towards things. I stress temporarily because it is as well. I don't want to say artificial,

211
00:27:32,900 --> 00:27:42,660
but it's fragile. It's preliminary. It's like you haven't quite come into focus when you

212
00:27:42,660 --> 00:27:49,620
have the camera lens. It's not quite in focus, so you can't quite say that it's going to be

213
00:27:49,620 --> 00:27:56,420
permanent. It's real. I would say it's natural in a way that tranquility meditation, the other

214
00:27:56,420 --> 00:28:01,700
kind of meditation isn't. I don't want to get in trouble with that. I don't mean to be

215
00:28:01,700 --> 00:28:06,020
pejorative about it, but it's different. It's much more natural because you're not avoiding,

216
00:28:06,020 --> 00:28:12,500
you're not limiting your experience to anything. You've created the same sense of clarity about

217
00:28:12,500 --> 00:28:17,460
everything, but you're not quite there yet. That's the distinction. The Buddha said,

218
00:28:17,460 --> 00:28:28,500
Bhavi Vita, Bhavi Vita, Bhavi Vayka, Risang. We look for the palana. Bhavi Vayka, Risang,

219
00:28:29,380 --> 00:28:42,100
Bhitwa, Bhavi Vayka, Risang, Vita. That's the Bhavi Vayka's seclusion. That's Monday. That's these two types.

220
00:28:42,100 --> 00:28:50,500
And the other one is Rasang Upasamasatcha, the flavor of Upasama. So he made a distinction between

221
00:28:50,500 --> 00:29:00,980
these two. The distinction is Upasama refers to the quietude that I said, quietude,

222
00:29:00,980 --> 00:29:07,540
peace, the quenching. That refers to something super mundane. It refers to Nibana.

223
00:29:07,540 --> 00:29:15,700
So once a meditator practices and sees things perfectly clearly, it doesn't stop there.

224
00:29:15,700 --> 00:29:23,700
It leads to something. It leads to this second type of peace, which is truly

225
00:29:28,100 --> 00:29:34,340
earth-shattering, mind-blowing. Mind-blowing might be a good adjective.

226
00:29:34,340 --> 00:29:49,780
It truly shatters the foundation of the mind and the sense of being stuck to some sorry. It

227
00:29:49,780 --> 00:29:57,460
changes the way you look at things in a very fundamental sense. It gives a perspective that wasn't

228
00:29:57,460 --> 00:30:05,300
there before. It's like when you're inside here, you don't know what the weather is like outside

229
00:30:05,300 --> 00:30:10,420
until you go outside. Once you go outside, you have that sense. You have the knowledge of

230
00:30:12,660 --> 00:30:17,780
what it's like. So you might think this air is okay to breathe, but when you go outside and you

231
00:30:17,780 --> 00:30:25,380
feel the fresh air and the sun on your face, you realize that these lights and all is very limited.

232
00:30:25,380 --> 00:30:31,220
You have a different perspective. It's like Plato. Plato talked about this very interesting,

233
00:30:32,260 --> 00:30:38,980
very famous simile of a cave. You know Plato's simile of a cave that you know what I'm talking about.

234
00:30:39,700 --> 00:30:44,500
He talks about people that's like cave and dwellers. We've never seen the outside.

235
00:30:45,940 --> 00:30:50,180
You'll read his simile. It's an interesting one. It's kind of the same idea here.

236
00:30:50,180 --> 00:30:59,780
We'll customize something very different. It's a piece that goes beyond a mundane.

237
00:31:01,380 --> 00:31:07,140
There's no, it's an effable in a sense that you can't describe it. You can't even remember it.

238
00:31:08,660 --> 00:31:12,260
When you have an experience of new bond, there's no memory of it because there's nothing to

239
00:31:12,260 --> 00:31:19,780
remember. There's no arising during that time. But it shakes you. It shatters something.

240
00:31:25,380 --> 00:31:34,340
And so that one is real and lasting and enlightening.

241
00:31:34,340 --> 00:31:42,340
So that's the second lesson. It's about happiness. The third lesson is just, I think,

242
00:31:42,340 --> 00:31:46,020
I separated it out because it's an interesting point. And that's this idea of

243
00:31:46,980 --> 00:31:52,820
Nipopble because it relates back to the story. It's how the verse relates to the story is that

244
00:31:54,020 --> 00:31:59,860
people who are criticizing this monk for his meditation practice and the Buddha said,

245
00:31:59,860 --> 00:32:08,980
Nipopble, he has no evil. There's nothing wrong. And it's the point that when your mind is pure,

246
00:32:09,540 --> 00:32:16,260
you have no evil, no corruption in your heart. And so criticism is unwarranted towards such a person.

247
00:32:17,540 --> 00:32:25,140
It is a bit of a debate. I think not so much among Buddhists, though it can come up,

248
00:32:25,140 --> 00:32:30,900
but it's often a debate that Buddhism has to enter in with other religions and non-religious people.

249
00:32:31,860 --> 00:32:38,020
I've heard people talk about Buddhist monks and meditators as lazy and parasites.

250
00:32:44,180 --> 00:32:49,380
And it's a little bit strange. I think it does have some credibility when we find that

251
00:32:49,380 --> 00:32:57,780
monks are encouraging people to give them things and support them and so on.

252
00:32:59,620 --> 00:33:04,580
And maybe when Buddhist meditators are just engaging in mundane pleasures,

253
00:33:05,460 --> 00:33:11,220
like tranquility meditation or just the pleasure of being in solitude, it can be a little bit

254
00:33:11,220 --> 00:33:20,740
self-serving and even can be a cause for evil to arise. It's possible that meditation centers,

255
00:33:20,740 --> 00:33:28,180
monastery is often you hear and you find about some places become corrupt.

256
00:33:31,380 --> 00:33:38,980
But the idea that practicing meditation is laziness or the idea that it's not contributing to

257
00:33:38,980 --> 00:33:47,380
society or the idea that one is a parasite if they're being supported for their spiritual practices

258
00:33:48,340 --> 00:33:54,580
really unfortunate because of how powerful and beneficial meditation is.

259
00:33:55,780 --> 00:34:01,460
If you have someone, if there exists someone in the world like the Buddha,

260
00:34:01,460 --> 00:34:08,340
they're the last thing, they're the farthest thing from the parasite because of the great

261
00:34:10,740 --> 00:34:17,060
beneficence they give to the world, even not intentionally by going around trying to teach

262
00:34:17,060 --> 00:34:26,740
people but first of all just by example, by providing the option or the vision of another way

263
00:34:26,740 --> 00:34:34,500
rather than trying to prop up a society that's based mostly on greed, we have an alternative

264
00:34:34,500 --> 00:34:39,060
and that's leaving society, even not just Buddhists but religious people in general who leave

265
00:34:39,060 --> 00:34:46,900
society and go and live in seclusion offer a really great, I mean a great alternative, I mean

266
00:34:46,900 --> 00:34:53,940
hearing about them myself when I was 13, I think from a computer game actually, I was playing

267
00:34:53,940 --> 00:35:00,020
at the time, I heard about the Dalai Lama in this video game and it got me learning about

268
00:35:01,700 --> 00:35:09,140
this idea of leaving society and it was very interesting, it was very interested in that it was

269
00:35:09,140 --> 00:35:12,580
one of the things that propelled me to go to Asia in the first place.

270
00:35:17,940 --> 00:35:21,300
But more importantly that when a person has accomplished

271
00:35:21,300 --> 00:35:28,820
all of the goodness that comes from meditation practice not only are they incapable of evil

272
00:35:30,420 --> 00:35:36,420
the relationship with them is so mind changing, I mean just having an association

273
00:35:37,060 --> 00:35:43,220
listening to the things they say, taking their advice, learning how to practice meditation from them

274
00:35:43,220 --> 00:35:54,500
and as an unmeasurable benefit and it benefits, the Buddha benefited his disciples who benefited

275
00:35:54,500 --> 00:36:01,940
their disciples who benefited, they benefited eventually, benefited my teacher and benefited me

276
00:36:01,940 --> 00:36:05,620
and then I benefit you hopefully, you benefited others.

277
00:36:05,620 --> 00:36:19,300
The greatness of our emphasis on self exploration and study and enlightenment is beyond

278
00:36:19,300 --> 00:36:27,780
compare, so much more beneficial than running around like all these monks did wailing and

279
00:36:27,780 --> 00:36:36,100
moaning about the Buddha going away and ensuring that they saw the Buddha before he passed, none

280
00:36:36,100 --> 00:36:41,700
of it's none of that is anywhere near or the social work that many religions and Buddhists

281
00:36:42,580 --> 00:36:48,900
engage in, there's nowhere near as beneficial as changing your mind because if you talk about good

282
00:36:48,900 --> 00:36:56,340
deeds and the avoidance of bad deeds, they only really are possible because of your purity of

283
00:36:56,340 --> 00:37:00,660
mind, so if you're a good person, it means you have some purity and you're able to do good things,

284
00:37:00,660 --> 00:37:07,060
but you're only able to do good things to the extent that you're a good person and so if you

285
00:37:07,060 --> 00:37:13,220
still have evil inside of you, you still have corruption, defilement, anger, greed, delusion,

286
00:37:14,420 --> 00:37:20,580
your good deeds will always be tainted by that and limited by that, affected by that and

287
00:37:20,580 --> 00:37:25,380
contrasted with the evil that you do based on your greed, your anger, your delusion,

288
00:37:27,380 --> 00:37:33,540
so your effect on others will not always be positive, your effect on the world will not be entirely

289
00:37:33,540 --> 00:37:40,100
positive and it will be negative if you talk about people who get involved with the world

290
00:37:40,100 --> 00:37:43,060
and you say well they're better because look at how hard they're working or so on.

291
00:37:44,980 --> 00:37:48,420
Quite often the work that they're doing is hurting people, hurting the world,

292
00:37:48,420 --> 00:37:54,740
giving people the wrong impression, leading people towards greater greed, anger, delusion.

293
00:37:57,140 --> 00:38:03,620
So we talk about them as industrious and meditators as lazy while better to be lazy in that case,

294
00:38:03,620 --> 00:38:10,340
better to not do anything, you go off in the world at the very least, you've freed all sorts of

295
00:38:10,340 --> 00:38:15,860
people from you and that's actually a Buddhist teaching, the Buddha talked about

296
00:38:15,860 --> 00:38:23,780
Bayada, which means freedom from fear when a person keeps the five precepts

297
00:38:25,540 --> 00:38:32,820
and they have freed countless of beings from them, from killing, from stealing, from lying,

298
00:38:32,820 --> 00:38:37,700
from cheating, drugs, alcohol, the first four anyway, very much related to others but drugs and

299
00:38:37,700 --> 00:38:43,300
alcohol as well, when you stop doing them you're much less likely to break the others or do anything

300
00:38:43,300 --> 00:38:49,940
evil. So keeping those five precepts has already been a great gift to society just by not doing

301
00:38:49,940 --> 00:39:00,340
lots of things. A person who practices in this way, a person who experiences the great peace

302
00:39:00,340 --> 00:39:06,740
and happiness of enlightenment and just meditation in general does the world a great service,

303
00:39:06,740 --> 00:39:17,620
they're incapable of evil, they have no sorrow or fear, need that rohote, nipapo and that's the third

304
00:39:17,620 --> 00:39:23,220
lesson. So that's the Dhammapada for tonight, thank you all for listening, wish you all the

305
00:39:23,220 --> 00:39:42,420
time.

