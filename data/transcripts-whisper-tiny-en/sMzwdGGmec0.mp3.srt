1
00:00:00,000 --> 00:00:29,800
Hello everyone, hello broadcasting live, February 7th, 2016.

2
00:00:29,800 --> 00:00:45,200
Today's quote is about well-spoken speech.

3
00:00:45,200 --> 00:00:56,800
A word is well-spoken if it is spoken at the right time, spoken in truth, spoken gently,

4
00:00:56,800 --> 00:01:18,000
spoken about the goal or spoken about things that are meaningful and spoken with love.

5
00:01:18,000 --> 00:01:26,400
So I'm in quite actually quite a simple teaching.

6
00:01:26,400 --> 00:01:28,400
I'm useful.

7
00:01:28,400 --> 00:01:36,000
A lot of the teachings like this are just useful reminders of

8
00:01:36,000 --> 00:01:40,600
how the importance of goodness.

9
00:01:40,600 --> 00:01:45,400
They said, tone, if you read through the Buddha's teaching, this is really

10
00:01:45,400 --> 00:02:03,400
all you see, I mean, there's no, there's no reading between the vines, there's no hidden

11
00:02:03,400 --> 00:02:23,200
agenda, so some religious movements and practices have a reason for why, why would we care

12
00:02:23,200 --> 00:02:25,600
about words that are ill-spoken or well-spoken?

13
00:02:25,600 --> 00:02:37,000
Because of them being good, you know, good for goodness sake, for example.

14
00:02:37,000 --> 00:02:40,400
That's not even quite exact because it's not for goodness sake.

15
00:02:40,400 --> 00:02:51,600
It's good because it's good, goodness is for goodness.

16
00:02:51,600 --> 00:03:01,200
goodness really is the certainly essential quality in Buddhist practice.

17
00:03:01,200 --> 00:03:12,480
And so when we talk about the goal or when we talk about the theory behind Buddhist practice,

18
00:03:12,480 --> 00:03:25,040
it really is just a natural outcome of cultivation of goodness.

19
00:03:25,040 --> 00:03:33,240
So in meditation, when we talk about the goal being Nirvana or we talk about it, cessation

20
00:03:33,240 --> 00:03:39,560
of this kind of thing, it really is just an, it's the same thing that we're talking about

21
00:03:39,560 --> 00:03:52,400
when we say practicing to give up our addictions, our aversion, our ego, practicing to

22
00:03:52,400 --> 00:04:00,840
give up on wholesomeness, that's really it, that's all we're talking about.

23
00:04:00,840 --> 00:04:06,280
So whether it be an action with a body, whether it be speech, or whether it be just thoughts

24
00:04:06,280 --> 00:04:13,280
in the mind, it really is just a matter of cultivating goodness.

25
00:04:36,280 --> 00:04:56,840
So I'm not too much to say tonight, we're doing, holding a peace symposium at McMaster

26
00:04:56,840 --> 00:05:05,560
and so I just had a meeting this evening about that and potentially starting a peace

27
00:05:05,560 --> 00:05:11,720
association at McMaster.

28
00:05:11,720 --> 00:05:16,080
And now that's what we're talking about, because it's curious that Buddhism is as much

29
00:05:16,080 --> 00:05:19,200
about peace work as it is about religion.

30
00:05:19,200 --> 00:05:27,080
So in university I find myself involved as much in peace studies as I am in religion.

31
00:05:27,080 --> 00:05:36,440
That goes to the point of what I was just saying, how Buddhism is just all about goodness,

32
00:05:36,440 --> 00:05:43,960
it's all about peace, not about Buddhism, you know, there's nothing really specifically

33
00:05:43,960 --> 00:05:58,960
Buddhist about the practice, as much as if it's about goodness, as much as it's about

34
00:05:58,960 --> 00:06:09,640
peace, as much as it's about wisdom, it's about universal qualities of the mind really.

35
00:06:09,640 --> 00:06:15,040
So it was specifically talking about peace, this quote about peace, speech, it's specifically

36
00:06:15,040 --> 00:06:27,800
talking about speech, you know, this is not, it's more about the mental intention

37
00:06:27,800 --> 00:06:40,520
behind the speech, I mean this is all this important, it's our mind that informs our

38
00:06:40,520 --> 00:06:52,360
speech, it's our mind that informs our actions, and so the big part of our practice really

39
00:06:52,360 --> 00:07:04,920
the main thrust of our practice is being conscientious and remembering ourselves, keeping

40
00:07:04,920 --> 00:07:15,640
track of our minds and track of our volitions and our intentions, why we do things, how

41
00:07:15,640 --> 00:07:30,240
we do things, the state of our minds when we do things, when we say things, and all

42
00:07:30,240 --> 00:07:38,440
this has to do with, not just Buddhism, but it has to do with peace, and it has to do

43
00:07:38,440 --> 00:07:51,480
with goodness.

44
00:07:51,480 --> 00:08:04,800
Also it, I just got word that our online project is doing well, which is awesome, looks

45
00:08:04,800 --> 00:08:18,880
like we're probably here to stay, anyway, not too much tonight though, nobody's coming

46
00:08:18,880 --> 00:08:26,960
on the hangout I guess to ask questions, post the link in our meditation page, so I guess

47
00:08:26,960 --> 00:08:32,600
we'll just call it a night, thank you all for coming on, thanks for coming out to

48
00:08:32,600 --> 00:08:37,040
meditate, got a good list tonight, have a good night everyone.

