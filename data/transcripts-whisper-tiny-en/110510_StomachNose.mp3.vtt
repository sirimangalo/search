WEBVTT

00:00.000 --> 00:07.360
Hello, welcome back to Azkamok. I assume some of you have noticed that I haven't really been posting

00:08.160 --> 00:14.480
in the past few months, and it's mainly because I've been trying to

00:16.560 --> 00:18.560
take some time alone, and

00:19.200 --> 00:21.200
there have been other

00:22.800 --> 00:27.600
engagements. I was traveling a little bit and I had to get to take it, and now I'll be traveling

00:27.600 --> 00:38.000
to America, so yeah, it's kind of broken up. I'm not just sitting around

00:39.760 --> 00:47.280
making videos. One thing I wanted to say is that for people who are waiting for the next

00:47.280 --> 00:50.560
videos, who are looking for me to make more videos, I just want to be sure that

00:50.560 --> 00:59.600
be clear that the purpose is not to listen to the Buddhist teaching or to listen to talks.

00:59.600 --> 01:06.560
People who download MP3s, VAST libraries of Dhammatops, or who

01:08.000 --> 01:10.640
read the whole of the Buddhist teaching again and again.

01:13.520 --> 01:19.520
It's not really the point, so the questions that I've been asking the answers that I'm giving,

01:19.520 --> 01:23.840
I'm hoping that they're for the purpose of taking a way to practice meditation. So really,

01:23.840 --> 01:29.040
in the end, it's not about how many questions are answered or how many videos are posted,

01:29.040 --> 01:34.880
how many talks you you listen to. It's how much time you actually spend practicing and how

01:34.880 --> 01:44.640
clear is your mind and your awareness of the present moment. So if you're looking for the next step,

01:44.640 --> 01:50.720
the next step is not just to sit around waiting for more videos. So I'll try, I will be doing more,

01:50.720 --> 01:59.360
but there's a lot of questions people have been asking. The next step is to find a way to take time

01:59.360 --> 02:06.080
to actually practice. And once you've really started to begin to engage in the meditation practice,

02:06.080 --> 02:10.720
there aren't nearly so many questions. Most of the questions that you have

02:10.720 --> 02:17.680
answered themselves. So just to make sure that I'm happy to answer questions, but it's not

02:17.680 --> 02:21.520
really worth it if people have just come back with more and more questions rather than

02:22.400 --> 02:30.800
seeking out the answers, which are ultimately within. So, but another thing that comes up,

02:30.800 --> 02:36.960
which is great about having this forum to answer questions, is that there are a lot of technical

02:36.960 --> 02:41.760
questions, which can't be answered. You can't sit down and practice and very easily come up with

02:41.760 --> 02:44.960
the technical answer. What should I be doing here? What should I be doing there?

02:47.120 --> 02:53.040
Often because it's quite subjective. What is the technique here? And unless you try it and test it

02:53.040 --> 02:57.200
out one way, you won't be able to tell whether it's more beneficial to do it this way or that way.

02:57.200 --> 03:06.320
So technical questions are always welcome. All questions are welcome. I just don't spend all your time

03:06.320 --> 03:12.240
asking questions. We should spend most of your time finding the answers to them. So here are some

03:12.240 --> 03:18.080
answers to a couple of technical questions. This should be short. First of all, when you say

03:18.080 --> 03:24.800
your mind shouldn't be in our mind shouldn't be in our head, but in our stomach when we contemplate

03:24.800 --> 03:34.000
rising and falling, what do you mean? Can you elaborate it more? The mind is not physical. The mind

03:34.000 --> 03:41.120
is that quality of experience or that aspect of experience that knows that there is aware of

03:41.120 --> 03:46.800
something. So when I say that the mind should be in the stomach, that's not really correct because

03:46.800 --> 03:52.000
the mind doesn't take up space and it doesn't go here or there. So it isn't one moment in the head,

03:52.000 --> 04:00.320
one moment in the stomach. It doesn't move. It is aware of the object. The idea of space only arises

04:00.320 --> 04:06.800
when you're talking about the physical and that's really, in a sense, only a concept. The ultimate

04:06.800 --> 04:14.400
reality is the experience which has a physical end and a mental end and that doesn't take up space.

04:14.400 --> 04:22.560
Space is only a convention when we're talking about the physical realm. So what I mean by that is

04:22.560 --> 04:29.840
that it should feel like the mind is in the stomach. In the sense that you're really

04:29.840 --> 04:38.480
knowing the rising. So when you say to yourself, rising, that's the mind recognizing and this

04:38.480 --> 04:43.040
is the rising. It's the mind with the clear awareness of what that is because this is what the

04:43.040 --> 04:50.560
mind does when it becomes aware of something. It immediately recognizes it as, well, first of all,

04:50.560 --> 04:56.000
as the basic realities of it seeing, hearing, smelling, tasting, feeling, thinking, but generally

04:56.000 --> 05:04.160
it will go on to recognize it as good, bad, me, mine, a source of pleasure, source of pain,

05:04.160 --> 05:08.720
something that is beneficial, something that is harmful, and so it makes all sorts of judgments

05:08.720 --> 05:14.880
and categorizations and it creates all sorts of intentions based on this and eventually leads

05:14.880 --> 05:22.960
to suffering and stress and busyness. So what we're trying to do in the meditation is to simply

05:22.960 --> 05:31.360
recognize it and to stop there. So the response in the mind should only be rising,

05:31.360 --> 05:39.600
shouldn't be arising and my stomach is smooth or the rising is smooth or it's stuck and it's

05:39.600 --> 05:46.240
uncomfortable or this or that or the idea of trying to, I should make it longer, I should make

05:46.240 --> 05:55.600
it shorter, I should try to keep it a constant speed and so on. None of these things will arise

05:55.600 --> 06:01.840
if you're simply recognizing what it is. So the point of saying that it should be in the

06:01.840 --> 06:08.960
stomach is to avoid making it a mental exercise where you put the brain to work and start to

06:08.960 --> 06:19.440
create a concept or a conventional or a mental creation based on the rising because it's very

06:19.440 --> 06:25.280
easy to sit there and say to yourself, rising, falling but it's very difficult to actually know

06:25.280 --> 06:31.440
that this is the rising, this is the falling and to have a strong awareness. Normally our awareness

06:31.440 --> 06:38.320
of the rising or the falling of anything is very superficial so we know it and then we're off on

06:38.320 --> 06:44.800
attention thinking we're back up in the head working with actually with the chemicals in the brain

06:44.800 --> 06:52.960
to create pleasure and the ones that create pain and stress and so on and the interactions

06:52.960 --> 07:01.600
with our thoughts and our reactions to what should have been a very simple object which

07:01.600 --> 07:07.920
should be the rising. So it should feel like the mind is in the rising in the stomach because there

07:07.920 --> 07:14.240
is a clear awareness at that moment of a rising motion which we understand to occur in the stomach

07:14.800 --> 07:22.480
and then the falling fall and the clear awareness of it. So you can really tell the difference,

07:22.480 --> 07:26.960
the difference between simply saying it and you know it for a second and then you're up in

07:26.960 --> 07:34.320
the brain saying rising and falling and actually knowing rising and that's really a trick,

07:34.320 --> 07:39.440
it's something that's quite difficult if you've never done it, it's something that takes time to

07:39.440 --> 07:46.240
perfect and time to develop. Okay and another question I'm going to pack them together here because

07:46.240 --> 07:52.880
they're fairly simple. Second one is sorry could you please answer a few questions. Well it's

07:52.880 --> 07:59.680
actually one question. Oh two questions. When meditating how to breathe can only breathe through your

07:59.680 --> 08:08.560
nose or inhale through the nose exhale through your mouth. Okay how to breathe? I suppose the

08:08.560 --> 08:15.360
simplest answer is to say however you normally breathe when you're relaxed, when you're not thinking

08:15.360 --> 08:20.800
and when you're not forcing your breath, when you're not running, when you're sitting still normally,

08:20.800 --> 08:28.160
how do you breathe? Because the idea of breathing and this is breathing in through your nose,

08:28.160 --> 08:34.960
inhale through the nose and exhale through the mouth. That sounds like a construct, it sounds like

08:34.960 --> 08:41.920
you're actively trying conscious, you're making a conscious effort to breathe in one and breathe

08:41.920 --> 08:48.320
out the other, we normally don't breathe that way. If you subconsciously, unconsciously breathe

08:48.320 --> 08:52.480
that way then there's no problem with it. But as soon as you start to say okay in through the mouth

08:52.480 --> 08:56.640
into the nose, out through the mouth or into the mouth, out through the nose or however you want

08:56.640 --> 09:03.600
to do it, if you start to say you know opening your mouth and trying to breathe through the mouth

09:03.600 --> 09:09.280
when normally you breathe through the nose or vice versa, then your mind isn't really with the

09:09.280 --> 09:14.560
present moment, it isn't with the breath, it isn't with the stomach, it isn't with the body with

09:14.560 --> 09:21.520
reality, it's on this intention okay now through the mouth, it's not natural. So you have this

09:23.120 --> 09:28.800
process on top of the observation, you're no longer simply observing the emotion which is

09:28.800 --> 09:33.040
really what we're trying to do here and it should be clearly understood that that's where the

09:33.040 --> 09:40.080
practice is. The practice is in simply observing emotion that is already occurring. And so in the

09:40.080 --> 09:46.000
technique that I follow it, it doesn't really matter what goes on up here or not focused on

09:46.000 --> 09:53.520
this aspect of the breathing, we're focused on the stomach aspect, the aspect of the contact

09:54.160 --> 10:01.360
with the body and the expansion of the body based on the breath going in and then the contraction

10:01.360 --> 10:07.600
based on it going out. So it shouldn't have any difference but if you're practicing mindfulness

10:07.600 --> 10:12.640
of breathing I would say you're focusing on the nose, I would say at the same go through

10:12.640 --> 10:19.920
you're focusing on the breath area, then you shouldn't try to control the nature of the breath

10:19.920 --> 10:28.800
you should be watching it. And I suppose if you were practicing for another purpose, if your

10:28.800 --> 10:38.000
purpose was to develop certain super mundane or supernatural states, magical powers or

10:38.000 --> 10:46.640
develop great strength of mind or great bliss and special meditative states as opposed to

10:46.640 --> 10:52.800
simply trying to understand things as they are, then some control can be useful because it develops

10:52.800 --> 11:00.640
your concentration. Pragna yoga, Prana yoga, I don't know how they say it, they're very much

11:00.640 --> 11:06.960
into deep breaths and when I did martial arts we were into deep breaths and so on and the idea

11:06.960 --> 11:12.640
was to get your breath, slow your breath down to one minute for breath and so on. So in that case

11:13.280 --> 11:17.760
some people even I think can breathe in through one nostril and out through the other they have

11:17.760 --> 11:24.000
down to such a side and this gives you great power. Now this is not what we're looking for here

11:24.000 --> 11:32.240
because this power is most often accompanied by delusion, the ego, the idea that I, that this is me,

11:32.240 --> 11:40.320
this is I am doing it with the control idea and it works for a while but eventually it breaks apart

11:40.320 --> 11:47.680
and breaks down falls apart and disappears and so it isn't really a self or an ego

11:47.680 --> 11:54.880
or me or mine and this kind of delusion is simply a waste of time and worse it leads you on the wrong

11:54.880 --> 12:01.040
path, it keeps you from understanding things and letting go of things, seeing things as they are.

12:01.040 --> 12:08.560
So no, the breath should be natural, it should be however it happens, however it happens when

12:08.560 --> 12:17.120
you're asleep, however it happens when you're just relaxed, when you're not thinking about it,

12:17.120 --> 12:25.200
it should continue on in that manner. And what you'll see is the difference between your normal

12:25.200 --> 12:30.560
breath and your meditative breath is the problem that when you're meditating that your breath is

12:30.560 --> 12:35.040
forced, your breath is controlled and that's the problem and that's why we're meditating because

12:35.680 --> 12:40.160
anytime we focus on something or there'd be something outside of it, anything we pay attention to

12:40.160 --> 12:47.920
it, we can't, we're unable ordinary beings to simply observe and be aware of it, we have to control,

12:47.920 --> 12:55.520
we have to obtain, possess and so on and this is the problem. Until we learn that this is suffering,

12:55.520 --> 13:01.520
this is a cause for stress, that this is a cause for all of the troubles in our lives.

13:01.520 --> 13:10.160
Well, then we'll always have difficulties, troubles and stress. Once we realize this,

13:10.160 --> 13:16.400
then our breath will come very much back to normal, our whole body, our whole brain, the whole of

13:16.400 --> 13:23.280
our being will be very much more natural. And when we focus on when we are aware of something,

13:24.480 --> 13:28.800
it won't change. When we're aware of the breath, the stomach rising and falling will be as if we

13:28.800 --> 13:34.000
weren't even paying attention to it, it won't change in the slightest. Now this is important,

13:35.680 --> 13:48.640
this is the, really what we're aiming for. So the breath definitely not a good idea to try to

13:48.640 --> 13:53.520
force it in one way or another unless you're practicing those kinds of meditation. If you're

13:53.520 --> 13:59.920
trying to understand things as they are, let the breath go as it will. And notice when it's not

13:59.920 --> 14:08.320
ordinary, when it's not unconditioned. And you'll see that that's where there is, there is

14:08.320 --> 14:13.680
delusion, there is the idea of control, forcing and so on. And it's a habit, it's something that

14:13.680 --> 14:18.320
we've developed and that we're trying to do away with. Because once you see that it's

14:18.320 --> 14:23.760
unpleasant that it's a cause for suffering, this forcing, this control, you'll let go of it

14:24.400 --> 14:30.800
after you repeatedly observe this eventually. Your mind will change the habit. It will say,

14:30.800 --> 14:36.160
no, this is not leading to happiness. It's actually leading to stress and stuff. Okay, so there's

14:36.160 --> 14:40.800
two questions. I'll try to answer some more now. Actually, I'm thinking I'm going to bring

14:40.800 --> 14:49.440
my video camera, this little portable video camera that this was donated three years ago and has

14:49.440 --> 14:56.400
seen a lot of use in this. Good little camera. I think I'll bring it with me when I go to America

14:56.400 --> 15:03.200
and because I'm not going to be doing so much. So probably I'll have some time to answer some

15:03.200 --> 15:09.360
videos on the road. Answer some questions on the road and I've got, I think I've still got about

15:09.360 --> 15:15.280
a hundred questions to answer. I'm sorry. I mean, I'd like to open it up. I'd like to take questions

15:15.280 --> 15:19.760
and answer questions that people have. But what do you do? And you've got a hundred waiting.

15:21.600 --> 15:28.960
It just keeps piling up and piling up. So you'll have to bear with me. And I know a lot of people

15:28.960 --> 15:35.280
have as a result of closing it down or stopping to accept questions. They've started sending

15:35.280 --> 15:40.480
questions to me directly, which I really can't do. That's the reason for, that's even worse for me

15:40.480 --> 15:48.000
because then I have to answer the same question when each person asks it. And so I find myself

15:49.120 --> 16:02.000
with far bigger workload than I'm actually actually able to handle. So this is the best I can do.

16:02.000 --> 16:09.520
If you really have questions, and this is what I'd really like to see, come on out. Come on over

16:09.520 --> 16:16.240
to Sri Lanka. It's a beautiful place. I haven't seen any. When there's a one poisonous snake,

16:16.240 --> 16:24.080
a couple of scorpions, lots of leeches, but that doesn't matter. There's no worries here.

16:24.640 --> 16:28.640
If you come on over, you can ask all the questions you like and I'm happy to answer them

16:28.640 --> 16:35.280
as long as you dedicate yourself to the meditation practice and join us in our practice.

16:35.280 --> 17:05.120
Okay, so thanks for tuning in on the best.

