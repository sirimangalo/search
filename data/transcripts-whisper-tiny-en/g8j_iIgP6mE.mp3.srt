1
00:00:00,000 --> 00:00:16,560
Okay. Good evening, everyone. Welcome to our evening demo session tonight, I've been asked

2
00:00:16,560 --> 00:00:33,840
to where I was asked last night to talk about evil. I'm sorry. I was asked to talk about

3
00:00:33,840 --> 00:01:00,640
evil. Because when you, when you look at the world,

4
00:01:00,640 --> 00:01:21,920
we have one big question and I think is among the biggest questions that we have. Maybe

5
00:01:21,920 --> 00:01:33,800
we don't realize it. But I think you could argue as one of the biggest questions we have.

6
00:01:33,800 --> 00:01:47,040
We wake up in the morning. The birds are singing. The sun is shining.

7
00:01:47,040 --> 00:02:00,680
Maybe we have, well, we live in a world in a universe that is just magnificent.

8
00:02:00,680 --> 00:02:27,320
You look at a microscope and you see the incredible nature of the universe and it's easy

9
00:02:27,320 --> 00:02:39,640
to believe that this universe is great. You look at the human body and the strength and

10
00:02:39,640 --> 00:02:53,600
the power and the potential human eye when a remarkable instrument knows our tongue, our

11
00:02:53,600 --> 00:03:06,960
ears, our body able to experience such a range of experiences. The world doesn't

12
00:03:06,960 --> 00:03:17,400
seem that bad really. It seems quite good, in fact. Where we're shown, I think, generally

13
00:03:17,400 --> 00:03:25,280
and I'm generalizing, I'm speaking for people who are living in functional societies who

14
00:03:25,280 --> 00:03:34,400
are functional individuals and functional families. Maybe I'm speaking to a myth, but it's

15
00:03:34,400 --> 00:03:40,400
important because we all share that myth. The myth of the functioning individual and the

16
00:03:40,400 --> 00:03:49,400
functioning family and the functioning society. I think many of us think that that's us.

17
00:03:49,400 --> 00:04:00,400
That is what we shouldn't be given our situation. Everything's fine. We're taught at a

18
00:04:00,400 --> 00:04:11,960
very early age to over-taught the existence and the worthiness of pleasure of the good things

19
00:04:11,960 --> 00:04:19,600
in the universe, in the world. How to get those good things. We're taught this. How to avoid

20
00:04:19,600 --> 00:04:30,360
the bad things. We're taught to fixate and focus on the good, on the happiness

21
00:04:30,360 --> 00:04:40,360
that exists in the world. We're given this image of the world as being something great,

22
00:04:40,360 --> 00:04:50,000
something good. The question is why do we suffer? What's wrong? Often it takes the

23
00:04:50,000 --> 00:05:07,400
form of, what's wrong with me? Why am I not happy? The world is such a wonderful place.

24
00:05:07,400 --> 00:05:23,440
Why am I not perfectly satisfied? I've got everything I want. What am I suffering? Or any

25
00:05:23,440 --> 00:05:39,120
number of such lines of questioning regarding this confusion? About why, when we've been

26
00:05:39,120 --> 00:05:52,800
told all the various ways of being happy? Why are we not happy? Often times the question

27
00:05:52,800 --> 00:05:59,840
doesn't come to us. It's still an important question, but it's even worse when we don't

28
00:05:59,840 --> 00:06:11,480
ask it, when we don't even realize when we barrel headlong forward looking for seeking

29
00:06:11,480 --> 00:06:18,680
out happiness, believing quite firmly that we're going to find it. But it's there. It's

30
00:06:18,680 --> 00:06:30,760
here believing that I'm happy. When I say believing, because one of the remarkable things

31
00:06:30,760 --> 00:06:35,360
that happens when you meditate, it doesn't probably sound so remarkable, but it is. It's

32
00:06:35,360 --> 00:06:51,000
quite important, is that your perception of your situation changes. I mean, it clears up,

33
00:06:51,000 --> 00:06:59,240
and you see how much you're suffering. Sometimes we don't even ask the question because

34
00:06:59,240 --> 00:07:20,760
we don't see how much stress there is. How imperfect, how... No, not what the word is, but

35
00:07:20,760 --> 00:07:39,080
how below perfect our lives are. So there's a question, why, why? What's wrong? What

36
00:07:39,080 --> 00:07:43,800
is it that we're going to see, perhaps, when we meditate, that's going to change our perception

37
00:07:43,800 --> 00:08:00,520
and help us to find the answer to why we suffer, help us to free ourselves from suffering?

38
00:08:00,520 --> 00:08:08,360
And if you probably guessed, because I gave it away, the answer is evil. The answer is

39
00:08:08,360 --> 00:08:25,880
that there is evil in the world. So I mentioned that there are Buddhism considered to

40
00:08:25,880 --> 00:08:35,120
be five types of evil. And someone asked where I got that from, and I think wrongly

41
00:08:35,120 --> 00:08:40,480
suggested that it was in the actual Topika, that was something the Buddha said. I don't

42
00:08:40,480 --> 00:08:46,600
have such a quote, but it's one of these traditional teachings. It's in the commentary.

43
00:08:46,600 --> 00:08:54,960
So we have this body of texts that tries to figure out, or not figure out, has some insight,

44
00:08:54,960 --> 00:08:59,360
and something to say about what the Buddha said to try and put it all together. Often

45
00:08:59,360 --> 00:09:04,080
it's just a matter of taking from various sources and seeing the Buddha use the word

46
00:09:04,080 --> 00:09:12,800
evil in different contexts. And so there is a list of five types of evil. The list I found,

47
00:09:13,440 --> 00:09:18,400
because of course I've heard this from other sources, but the list I actually found in my search,

48
00:09:19,520 --> 00:09:21,600
is in the commentary to the uddhana.

49
00:09:21,600 --> 00:09:42,640
Banchawidho Maro, the fourfold, the fourfold evil, fivefold evil. So that's what I'll talk

50
00:09:42,640 --> 00:09:49,360
about today. This is important, this is it. Be free yourself from this, you're free to find

51
00:09:49,360 --> 00:09:57,200
peace and happiness. It's not as simple as just removing a thorn and going back to playing in the

52
00:09:57,200 --> 00:10:06,400
fields, but it really is the only problem. In fact, I think I would argue that from a Buddhist point

53
00:10:06,400 --> 00:10:15,600
of view, there really is nothing wrong with the world besides evil. I don't know how

54
00:10:15,600 --> 00:10:20,480
it's a bit imprecise because it's more complicated than that, but we'll see.

55
00:10:23,520 --> 00:10:30,320
So the first evil, I mean the first evil really cuts to the chase and the first evil is called

56
00:10:30,320 --> 00:10:46,000
kandamara. So you might be in a position to experience great pleasure and happiness, but

57
00:10:47,680 --> 00:10:57,280
kandas is what we generally think of kandas is you as the being,

58
00:10:57,280 --> 00:11:04,080
but that's not really technically what it means. Another works for our purposes here.

59
00:11:04,960 --> 00:11:11,840
On a conventional level, it's just you that's the problem, not you in particular, but the you,

60
00:11:11,840 --> 00:11:21,920
any you, any person, the being that were imperfect. Maybe you go out in the sun is shining and

61
00:11:21,920 --> 00:11:30,240
you think, what a lovely day, but then you get a sunburn. Or you

62
00:11:35,840 --> 00:11:43,040
kind of see things and hear things that are wonderful. And then suddenly they change,

63
00:11:43,040 --> 00:11:54,080
you have to, you have the same sensitivity to the pleasant sensations or pleasant experiences

64
00:11:54,640 --> 00:12:03,600
makes you sensitive to the negative ones. This sensitivity, the inclination,

65
00:12:03,600 --> 00:12:14,400
may I hear this sound leads you to react equally strongly to other sounds.

66
00:12:15,760 --> 00:12:22,400
So the the deeper understanding of kandam is not the being, there's a lot of problems with

67
00:12:22,400 --> 00:12:32,960
beings. You may be like sweets, you get a lot of sweets, but then you get fat and you get rotten teeth,

68
00:12:32,960 --> 00:12:42,320
diabetes, lots of sicknesses, food is a good example. You go off into the wonderful forest and you

69
00:12:42,320 --> 00:12:49,440
get bitten by mosquitoes and so on. Forest is a wonderful place, but you've got to share it with

70
00:12:49,440 --> 00:13:00,400
all sorts of baddies. But deeper than that, more, more ultimate than that is the uncertainty.

71
00:13:03,120 --> 00:13:10,800
The good things will be unpredictably replaced by the, by bad things, meaning you can't predict

72
00:13:10,800 --> 00:13:23,200
when it's going to happen. You're not in control of what's going to happen. And so there can be no

73
00:13:23,200 --> 00:13:31,600
satisfaction. You can be satisfied by all the good things in the world. It's not to say there's

74
00:13:31,600 --> 00:13:41,520
anything wrong with sunrise or a sunset or even anything wrong with cheesecake, for example.

75
00:13:41,520 --> 00:13:46,320
That's just nothing satisfying. It's not it's not capable of satisfying because it's

76
00:13:46,320 --> 00:13:54,240
impermanent. The experience of whatever makes you happy is impermanent and unsatisfying and

77
00:13:54,240 --> 00:14:05,920
controllable. That's really what you see in meditation. You come to see that this process of

78
00:14:05,920 --> 00:14:11,920
experience that we used to think was capable of bringing us such happiness is just not capable

79
00:14:11,920 --> 00:14:28,560
of doing that. That way of that endeavor is futile, seeking happiness in things that are unpredictable

80
00:14:28,560 --> 00:14:43,040
and that are uncontrollable. It's not a way to find satisfaction. So it leads to dissatisfaction

81
00:14:43,040 --> 00:14:55,680
stress. It leads to an increase in, well, let's just say it's not capable of satisfying. We'll

82
00:14:55,680 --> 00:15:00,800
leave it at that because there's actually more kinds of more. There's four others, so we'll separate

83
00:15:00,800 --> 00:15:14,560
them out. But you could argue, if we just take this first one, you could argue that, well,

84
00:15:14,560 --> 00:15:24,560
there are ways of ameliorating that. Maybe not perfectly, but there are people in the world who

85
00:15:24,560 --> 00:15:37,680
are able to fix a good number of the problems. They're able to avoid, most of us are able to avoid

86
00:15:37,680 --> 00:15:48,800
the worst types of experiences, the most undesirable, or many of us are in a functioning society,

87
00:15:48,800 --> 00:16:01,680
a functioning family. We get good food. We have a nice place to live. We're surrounded by

88
00:16:02,480 --> 00:16:09,760
polite individuals in Canada. It's a good example of generally being surrounded by a polite society

89
00:16:09,760 --> 00:16:20,960
is generally speaking. So you would say, well, this condom are a thing. We can avoid that, can't we?

90
00:16:24,880 --> 00:16:31,680
And we would argue even further, some would argue even further, that maybe we can't,

91
00:16:32,640 --> 00:16:38,960
but we're getting there. We have the potential. I don't know that this argument, I mean, some do

92
00:16:38,960 --> 00:16:46,640
present it, but for most of us, I think it's just assumed that society and humanity is progressing.

93
00:16:48,160 --> 00:16:51,760
We've already come up with the iPhone. That's got to be part of the solution, right?

94
00:16:53,760 --> 00:17:02,640
No, I mean, technology in general. Technology is certainly our ability to

95
00:17:02,640 --> 00:17:09,920
to cure so many diseases, making steps towards eradicating mosquitoes.

96
00:17:12,080 --> 00:17:13,520
And it's apparently happening.

97
00:17:18,640 --> 00:17:25,920
AIDS is now no longer a thing or doesn't have to be in other ways. They've almost eradicated

98
00:17:25,920 --> 00:17:36,720
polio. We're on them way. We have the potential. There's another problem. There's another evil

99
00:17:36,720 --> 00:17:49,520
out there. And I think this line of thinking is easily countered, unfortunately. I mean, it would be

100
00:17:49,520 --> 00:17:53,920
nice if that were true. It would be nice to think that we're getting better as a society.

101
00:17:53,920 --> 00:18:02,240
And it would be nice to think as well, additionally, not only that things are getting better,

102
00:18:03,280 --> 00:18:05,840
but that they can perpetually get better.

103
00:18:07,920 --> 00:18:12,480
But there's something standing in the way of that. I'm not saying it's insurmountable,

104
00:18:12,480 --> 00:18:20,400
but it's important to understand if we only work on fixing our physical suffering.

105
00:18:20,400 --> 00:18:32,720
You may never have a problem, because the next Mara, there's even perhaps more important,

106
00:18:34,080 --> 00:18:36,720
more of a problem for us. It's called Kile Samara.

107
00:18:40,880 --> 00:18:48,800
So it's one thing to talk about Kanda Mara as being experiences that are suffering,

108
00:18:48,800 --> 00:18:54,000
that are unpleasant. But what does it that makes them unpleasant?

109
00:18:55,680 --> 00:18:59,840
It's something called Kile Samara. Kile Samara means

110
00:19:03,440 --> 00:19:11,520
Kile is literally means defilement, dirtiness, uncleanliness, something unclean,

111
00:19:11,520 --> 00:19:21,040
something defiled, tarnished. So Kile Samara, for gold, gold has Kile Samara.

112
00:19:22,000 --> 00:19:26,320
When the Kile Samara gold is when you take it out of the ground, it's not pure gold. It's defiled

113
00:19:26,320 --> 00:19:28,400
by other types of metal.

114
00:19:31,360 --> 00:19:34,320
So our minds also suffer from this problem.

115
00:19:34,320 --> 00:19:42,080
The goodness in our minds is soiled by, is defiled by impurities.

116
00:19:47,360 --> 00:19:54,160
So when we look at all these advances in humankind, the humanity,

117
00:19:55,840 --> 00:20:03,440
we have to temper that with the evil in humans. What is all the driving force of a lot of our

118
00:20:03,440 --> 00:20:15,840
innovation, greed, so many ways. How much of an innovator is war? A lot of our advances

119
00:20:15,840 --> 00:20:21,040
are based on war, based on our desire to kill each other, to take from each other,

120
00:20:22,240 --> 00:20:30,000
to conquer each other, to control each other. A lot of them are simply based on materialism,

121
00:20:30,000 --> 00:20:38,720
our desire for pleasant sensations, pleasant stimuli, which again with the problem,

122
00:20:38,720 --> 00:20:41,760
they're unsustainable and they're partial.

123
00:20:45,760 --> 00:20:54,400
You can't love every experience. It's not possible because loving one experience,

124
00:20:54,400 --> 00:21:01,760
being attached to one experience means intrinsically being dissatisfied when the opposite

125
00:21:01,760 --> 00:21:09,600
experience occurs, when the not ex makes me happy, not ex leaves me unhappy, unsatisfied.

126
00:21:09,600 --> 00:21:16,160
That's just the way it works. I mean, this is how you see how it works when you close your eyes

127
00:21:16,160 --> 00:21:23,440
and begin to look, when you examine your experience.

128
00:21:27,280 --> 00:21:33,120
Kilesa, our minds are not perfect. We could. I mean, really, the

129
00:21:34,240 --> 00:21:40,160
description of an enlightened being is someone who is at peace with everything.

130
00:21:40,160 --> 00:21:53,120
It's happy in any experience, but it can't come from craving, clinging,

131
00:21:53,120 --> 00:21:58,160
can't come from chasing. That's always going to be partial. It's always going to create

132
00:21:58,960 --> 00:22:03,920
this dichotomy, acceptable, unacceptable, pleasant, unpleasant.

133
00:22:03,920 --> 00:22:14,000
The only way it comes is from removing all of these causes of suffering from within us,

134
00:22:14,000 --> 00:22:22,320
the Kilesa, greed, our desire for anything. You want to be happy with everything,

135
00:22:22,320 --> 00:22:26,960
you have to give up your desire for anything. It's just the way it is. That's a claim,

136
00:22:28,800 --> 00:22:33,120
and I'm not going to try to logic. It's not something that I can logically explain

137
00:22:33,120 --> 00:22:38,800
why it is. It'd be nice if it wasn't, right? It'd be great if all of our defilements didn't

138
00:22:38,800 --> 00:22:46,640
lead to suffering, but that's not the way reality works. I mean, it's not even logical

139
00:22:47,600 --> 00:22:58,560
to think that wanting could lead to satisfaction. One thing is a habitual state. It only

140
00:22:58,560 --> 00:23:05,360
leads to more wanting, just like anger leads to being more angry. We talk about letting the

141
00:23:05,360 --> 00:23:12,800
anger out. Just release it. Some of that's going to help, and it doesn't. It makes you more

142
00:23:12,800 --> 00:23:18,640
habitually inclined to getting angry.

143
00:23:18,640 --> 00:23:32,880
The Kilesa Mara, evil. This is the evil. This is the one Buddhism talks about the most,

144
00:23:32,880 --> 00:23:42,320
it really deals with the most, because Kanda Mara, the Kanda being the things that lead to

145
00:23:42,320 --> 00:23:49,760
experiences, but we can't ever fix that. We can't ever say that my experiences only be

146
00:23:49,760 --> 00:24:01,200
that. Let them not be that. But our defilements, we can cleanse. We can work on the way we react

147
00:24:01,200 --> 00:24:12,160
to our experiences, the way we look at our experiences, the way we experience

148
00:24:12,160 --> 00:24:33,760
Kilesa Mara. The third type of Mara, third type of Mara really drives the point home,

149
00:24:33,760 --> 00:24:44,080
because you might argue, well, I get angry, but that's not really evil, is it? If I want to hurt

150
00:24:44,080 --> 00:24:53,680
someone, it doesn't mean I've done anything wrong, right? Maybe I had desire things,

151
00:24:53,680 --> 00:25:08,720
things, liking things. What's wrong with liking things? We might say, what's wrong with liking

152
00:25:08,720 --> 00:25:26,240
this or liking that? What's wrong with wanting? Why isn't it a problem? We might not see,

153
00:25:26,240 --> 00:25:34,720
we can't generally see this creating of opposites, how it creates, how wanting something creates

154
00:25:34,720 --> 00:25:46,000
the dissatisfaction. The third type of Mara is this process by which it does lead to dissatisfaction,

155
00:25:46,000 --> 00:25:57,280
because our defilements are not simply mental state. The mind is efficacious, meaning it does have

156
00:25:57,280 --> 00:26:10,400
results, it does affect, affect our experiences. This is Abisankarma, Abisankarma,

157
00:26:10,400 --> 00:26:17,120
which means really karma. On a course level, karma means killing and stealing and lying and

158
00:26:17,120 --> 00:26:25,760
cheating. You can't want something without engaging in some activity. The wanting leads directly

159
00:26:25,760 --> 00:26:30,480
to the activity. You say, well, I want lots of things. It doesn't mean I go and take them.

160
00:26:33,520 --> 00:26:40,160
But it does mean that you're cultivating habits and to be more precise,

161
00:26:40,160 --> 00:26:58,320
you're affecting the future arising of certain experiences. So a person who gives rise to

162
00:26:58,320 --> 00:27:14,000
whom their rise is desire, there will arise, consequentially, there will arise the chasing after

163
00:27:14,000 --> 00:27:24,720
or the imprinting in the brain, in the body, in the world around. The many states associated

164
00:27:24,720 --> 00:27:30,000
with wanting. You know, it changes your body. You want something enough. You release hormones

165
00:27:30,000 --> 00:27:41,600
and sweating. If your body tensing, just to speak of the physical reactions and then how that

166
00:27:41,600 --> 00:27:52,560
affects the brain, releasing certain chemicals. So when we talk about habits, it means the cultivating

167
00:27:52,560 --> 00:28:03,520
of this inertia, well, the breaking of inertia and creating a new sort of force that has

168
00:28:03,520 --> 00:28:10,240
an inertia, I think what you call it, an inertia of its own. It means it has a force to it,

169
00:28:11,440 --> 00:28:18,000
which is why you'll sit and meditate and think that somehow you can just turn off these emotions

170
00:28:18,000 --> 00:28:22,320
and you'll find, I know I'm, I know anger is bad. Why am I still getting angry?

171
00:28:31,040 --> 00:28:35,440
I know it's wrong to be addicted. I know addicted is addiction is pointless. Why am

172
00:28:35,440 --> 00:28:39,360
this is the force? The impetus that you can't just turn off.

173
00:28:39,360 --> 00:28:52,560
If you're patient, you can slowly work out and you stop feeding, feeding into the addiction or

174
00:28:52,560 --> 00:28:58,640
the aversion or so on. By simply seeing, I mean, this is what mindfulness is meant to do.

175
00:29:00,000 --> 00:29:06,080
Even when you're angry or wanting something is a wanting, wanting and it weakens that chain

176
00:29:06,080 --> 00:29:13,440
because your reaction is not to, yes, let me get it then if I wanted or know, let me run away

177
00:29:13,440 --> 00:29:19,120
from it if I don't want it. Instead of that, you say, I want it or they're wanting,

178
00:29:19,920 --> 00:29:29,440
don't add baggage, just be aware of now there is wanting. It's a neutralizer. It just says,

179
00:29:29,440 --> 00:29:38,240
this is this. It is what it is, which is really, you know, it's unfruitful. It doesn't give rise to

180
00:29:40,000 --> 00:29:43,840
habit beyond the habit of seeing things just as they are.

181
00:29:48,080 --> 00:29:54,000
I'll be some karma, the karma, which really just means the changes that come about

182
00:29:54,000 --> 00:30:03,440
in our psyche and our being based on our defilements, based on our desires, our aversion,

183
00:30:04,640 --> 00:30:11,360
based on those things that make us strive for and against.

184
00:30:11,360 --> 00:30:22,320
Someone still might be unconvinced, though. I'm building up to it because I think the fourth

185
00:30:22,320 --> 00:30:34,480
one, they all work. It's a really good set of teachings. So the fourth and the fifth are still

186
00:30:34,480 --> 00:30:43,200
missing from the equation because you might say, we can fix all that. We can work it out.

187
00:30:45,280 --> 00:30:58,240
We can protect ourselves. We can maybe do, well, engage in activities that keeps us from

188
00:30:58,240 --> 00:31:06,000
being dissatisfied so we can work out our defilements. Our defilements don't have to be a problem

189
00:31:06,000 --> 00:31:13,760
because we can always get what we want. We work at it. We work at it enough.

190
00:31:13,760 --> 00:31:25,840
So our karma will never catch up with us. These bad habits will never become a problem

191
00:31:28,240 --> 00:31:36,800
because we can protect ourselves, rich people, powerful people in our society, in this society,

192
00:31:36,800 --> 00:31:49,520
we're quite privileged in Canadian society. Generally speaking, I think we're quite privileged

193
00:31:49,520 --> 00:31:59,280
to have this protection from not getting what we want. We have a lot.

194
00:31:59,280 --> 00:32:08,240
We can say, well, let's just protect it. Let's just work on our society and keep our government

195
00:32:08,240 --> 00:32:15,360
stable. This is why people engage in building up societies because this will protect us.

196
00:32:17,520 --> 00:32:27,600
So what is still missing? I'm still missing if I get my life in order and I say, I'm doing fine.

197
00:32:27,600 --> 00:32:35,520
I haven't got this all worked out. I've got this life thing figured out. We're like Ferris

198
00:32:35,520 --> 00:32:42,480
Bueller. It's such a horrible movie. Everyone raves about this movie that I watched when I was

199
00:32:42,480 --> 00:32:48,480
a movie. We grew up with this movie. The movie about someone from everything. They do everything

200
00:32:48,480 --> 00:32:57,600
pretty awful, really. Steel of car. Anyway, and so on and so on. But this idea that everything is

201
00:32:57,600 --> 00:33:07,200
just going to work out if you just go with it. Anyway, there's probably some positive aspects to it.

202
00:33:07,920 --> 00:33:17,600
That sort of letting go. Let it make it getting everything to just work out for you. Everything's

203
00:33:17,600 --> 00:33:24,720
just going to work out. So we have two more problems with that. Two more evils. The fourth one,

204
00:33:24,720 --> 00:33:33,920
muchumara. Surprise, surprise. All the wealth and riches in the world. All the power and influence

205
00:33:35,440 --> 00:33:40,480
besides the fact that you could lose it. Most of us could lose it pretty quickly.

206
00:33:40,480 --> 00:33:48,480
Even those who couldn't, who won't lose it in this life are still going to die.

207
00:33:48,480 --> 00:34:12,400
You can't take it with you if you don't cultivate a healthy mind. All you're left with when you

208
00:34:12,400 --> 00:34:20,400
die is the defilement, the desires and the versions that you've cultivated in this life.

209
00:34:23,760 --> 00:34:33,440
Our minds are the only things we take with us. The good things that we find in this world are

210
00:34:33,440 --> 00:34:47,360
truly not able to satisfy. They're unsustainable, not just because they could just appear,

211
00:34:47,360 --> 00:34:52,880
and you could be left bereft in this life, but because they will, they have to.

212
00:34:52,880 --> 00:35:10,960
The causal power, the creative power of our mental inclinations, our defilements, our goodness,

213
00:35:10,960 --> 00:35:20,080
and our badness really will not be denied, cannot be put off forever. An evil person

214
00:35:20,080 --> 00:35:30,400
cannot prosper infinitely. They prosper because of this protection, the ability to protect yourself

215
00:35:30,400 --> 00:35:35,920
in life, surround yourself with all sorts of good things. Those good things that you surround

216
00:35:35,920 --> 00:35:41,600
yourself are not productive of more good things. The power that we have in this life is not

217
00:35:41,600 --> 00:35:48,320
productive. The wealth is not productive and more wealth. When you die, you can't buy your way to

218
00:35:48,320 --> 00:35:56,960
heaven. That's not what leads to heaven. In fact, it's usually the opposite. A person who is not

219
00:35:56,960 --> 00:36:02,960
attached to wealth will be much lighter in mind. Their mind will be much lighter and much

220
00:36:02,960 --> 00:36:10,000
freer, much more likely to go to heaven, or to be born rich, for example. A person who is very

221
00:36:10,000 --> 00:36:23,040
charitable. This is the nature of things. This opposites, you create opposites. When you want

222
00:36:23,040 --> 00:36:34,800
something, you create the dissatisfaction and so on. And the fifth one, the fifth one is the

223
00:36:34,800 --> 00:36:42,560
mara that most Buddhists are familiar with. They will put the mara. We have all sorts of stories,

224
00:36:42,560 --> 00:36:53,120
and these ones are actually in the canonical texts. The stories of beings that try their hardest to

225
00:36:53,120 --> 00:37:06,080
bring harm to make beings suffer. To keep beings from freeing themselves from suffering is how

226
00:37:06,080 --> 00:37:16,320
it's how they're portrayed in Buddhist texts. These beings, like monks and lay people who strive

227
00:37:16,320 --> 00:37:25,520
to become free from suffering, even angels who strive, are being harassed and targeted by other

228
00:37:25,520 --> 00:37:35,120
beings who don't want them to become free from suffering. For various reasons. So they're depicted

229
00:37:35,120 --> 00:37:44,720
as angels who really like things like conflict, they like excitement. And so the idea of people

230
00:37:44,720 --> 00:37:51,520
meditating silently is just abhorrent to them because they delight in the conflict and chaos and

231
00:37:53,600 --> 00:38:04,160
the excitement of creation. They like it when we go to war. I find this aspect of Buddhism

232
00:38:04,160 --> 00:38:10,480
difficult to teach because I know a lot of my audience doesn't have a real sense that such

233
00:38:10,480 --> 00:38:18,640
things exist. It's often quite turned off by the idea that there are angels or demons or satan

234
00:38:22,080 --> 00:38:30,320
because it seems so so far removed from this practical empirical practice of insight meditation

235
00:38:30,320 --> 00:38:41,600
and and and it is agreed. So I want to preface this part by reminding us, reminding everyone what's

236
00:38:41,600 --> 00:38:47,760
important. Important is not whether this or that type of being exists. When we talk about day

237
00:38:47,760 --> 00:38:58,960
we'll put the angel maras and angel as a manipulative angel. The important is what that means. What it

238
00:38:58,960 --> 00:39:06,480
means is evil comes from without not just in our experiences but from influence by other beings.

239
00:39:07,600 --> 00:39:13,360
And if we put it that way, I mean that's the teaching here. That's what's important about this

240
00:39:13,360 --> 00:39:20,560
that all of us can relate. Is there anyone who doesn't know someone else who is manipulative?

241
00:39:22,640 --> 00:39:27,920
Someone who outright wants to hurt us or more sinisterly wants to manipulate us

242
00:39:29,600 --> 00:39:40,720
wants to attract us and pull us into a trap or even just be a parasite.

243
00:39:40,720 --> 00:39:44,160
Sucking us dry.

244
00:39:47,360 --> 00:39:53,760
I'm using friendship and so on and trust. Lying to us. Bullying us.

245
00:39:56,400 --> 00:39:58,640
Controlling and manipulating and so on.

246
00:40:01,520 --> 00:40:08,240
Tons everywhere. Everywhere you go. People want to help me. Great. That's very kind of you.

247
00:40:08,240 --> 00:40:15,120
It turns out that it's all a con. The con artists of the world.

248
00:40:17,200 --> 00:40:24,640
Family members, relatives, friends, even husbands, wives, girlfriends, boyfriends, lovers

249
00:40:25,760 --> 00:40:36,160
can be very manipulative, needy, and we create much stress.

250
00:40:36,160 --> 00:40:45,520
The stress and suffering that comes from things like marriage and romance.

251
00:40:46,640 --> 00:40:52,800
What's wrong with us? What's wrong? We didn't have the fairy tale ending that we see in the movies.

252
00:40:52,800 --> 00:41:03,520
I think this whole talk is important to mention movies, mention our myths, our stories,

253
00:41:06,400 --> 00:41:12,640
and remind us how dangerous myths and legends and stories can be,

254
00:41:12,640 --> 00:41:24,080
because they don't rely upon reality. We're so keen to

255
00:41:26,640 --> 00:41:31,520
hear the stories, watch the stories. Be entertained by the stories

256
00:41:34,960 --> 00:41:41,760
because they can be how we wish things were. They can be how they can be how we think they should

257
00:41:41,760 --> 00:41:49,040
things should be. They can be anyway. They can manipulate. They can pull us away from happiness and

258
00:41:49,040 --> 00:41:55,520
bring us back to happiness. They can manipulate us in any way that we want. They're infinite,

259
00:41:57,040 --> 00:42:05,440
infinitely superior to reality in that way. There's only one thing that reality has going for it.

260
00:42:05,440 --> 00:42:12,720
Only one thing. If you compare reality and delusion, the illusion that the world is a wonderful

261
00:42:12,720 --> 00:42:21,520
place that I can find. Happiness? What's the one thing that reality has over our fantasies?

262
00:42:21,520 --> 00:42:35,760
Javen? It must be obvious. It's obvious, isn't it? It's real. Yes. Yes, it has something

263
00:42:38,000 --> 00:42:51,120
you'll never get that in fantasy. And it's important because you can't escape reality in order to

264
00:42:51,120 --> 00:42:58,800
have fantasy. You need reality. No matter what fantasy you might have. No matter what belief you might

265
00:42:58,800 --> 00:43:10,800
have. This is why belief is so problematic because we rely on belief too much. Generally, we'll say,

266
00:43:10,800 --> 00:43:16,800
well, I believe. And again, I've said many times this is meaningless. When you say I believe,

267
00:43:16,800 --> 00:43:23,600
it's a useful statement. It's not that you should never say such things,

268
00:43:25,360 --> 00:43:32,960
but you should be ready to acknowledge the evidence and the reasons you have for believing X.

269
00:43:32,960 --> 00:43:43,200
I mean, I believe many things. But your belief and the extent to which you cling to that belief

270
00:43:43,200 --> 00:43:47,040
should be proportionate to the reasons you have for believing.

271
00:43:51,760 --> 00:44:00,560
These are going, our belief is never going to be a replacement for reality. If our beliefs are

272
00:44:00,560 --> 00:44:11,760
out of line with reality, if we fantasize about this or that, if we have a rosy picture of the way

273
00:44:11,760 --> 00:44:23,040
things are looking on the bright side of life, there was another movie called Life is Beautiful

274
00:44:23,040 --> 00:44:31,440
or Something that I saw when I was in university many years ago again. And it's about the second

275
00:44:31,440 --> 00:44:38,240
world war. And it's this father's attempt to keep his son from experiencing the horrors of the

276
00:44:38,240 --> 00:44:49,600
Holocaust. And it was touching, I guess, it was, I think it was a comedy. But it does kind of

277
00:44:49,600 --> 00:44:59,600
miss the point. I mean, if we were at all, just live our lives seeing the beauty and the good

278
00:44:59,600 --> 00:45:09,600
and things, it really doesn't fix, it doesn't, it doesn't approach reality. It doesn't actually

279
00:45:11,680 --> 00:45:17,280
fix the problem. Right? If you've got an infection, suppose you've got an infection or a wound

280
00:45:17,280 --> 00:45:25,120
that's infected and you like put some makeup on it or some tie a bow around it or put sparkles on it.

281
00:45:25,120 --> 00:45:29,920
And you know, maybe make it look so that the infection is actually nice and you go around and

282
00:45:29,920 --> 00:45:34,800
show it to people. It's not going to tear the infection and it's kind of a silly thing to do.

283
00:45:34,800 --> 00:45:44,560
You think, well, why don't you just clean it out and see it? It sounds appealing to live in fantasy,

284
00:45:44,560 --> 00:45:48,800
you know, see that always look at the Monty Python, always look on the bright side of life

285
00:45:48,800 --> 00:45:57,520
or taught this unequivocally, I can say that's the wrong, it's a wrong-handed way of looking at

286
00:45:57,520 --> 00:46:07,360
things. It doesn't make you happier looking on the bright side of life. It's not being an optimist.

287
00:46:10,560 --> 00:46:18,240
It's a great thing for a while. It's always going to be circumscribed by the habits that it

288
00:46:18,240 --> 00:46:27,600
creates and their dissonance with reality, how our desires eventually catch up to us. We can be

289
00:46:28,960 --> 00:46:38,880
happy for a while. As long as we have the power, which doesn't come from being happy.

290
00:46:38,880 --> 00:46:44,880
The ability to maintain your happiness doesn't come from being happy. It comes from goodness. It comes

291
00:46:44,880 --> 00:46:53,600
from purity of mind. It comes from strength of mind. The strength of mind that we have as human

292
00:46:53,600 --> 00:46:58,320
beings takes a lot of work and effort. I mean, we've got here through some very powerful states.

293
00:47:00,320 --> 00:47:04,400
I mean, these are all claims. This is the sort of thing that we see in meditation. Do some

294
00:47:04,400 --> 00:47:08,720
meditation. You'll see there's no question. Happiness does not lead the happiness.

295
00:47:08,720 --> 00:47:15,120
Be happy. Just be happy. Yeah, what does being happy do for you?

296
00:47:16,960 --> 00:47:22,560
What good is it to be happy? It sounds like a silly question, but it's important because

297
00:47:23,280 --> 00:47:28,720
any meditator should be able to tell you. It doesn't actually lead to anything good.

298
00:47:28,720 --> 00:47:36,800
It doesn't lead to more happiness. If you want to be happy, there's something else you need to do.

299
00:47:40,000 --> 00:47:43,440
You really do. The only way to be happy is to overcome evil.

300
00:47:46,480 --> 00:47:53,440
And I don't want to sound like some religious zealot who is avoiding the pleasures of life.

301
00:47:53,440 --> 00:48:01,040
We don't want to fall into that trap by there of denying just being this pessimist, a pessimist

302
00:48:01,040 --> 00:48:07,040
is the point. We don't want to be pessimistic. So when we talk about evil,

303
00:48:11,120 --> 00:48:15,440
it's important to understand, and sort of this is concluding the talk, I guess,

304
00:48:15,440 --> 00:48:21,680
that we're really just talking about misunderstanding.

305
00:48:25,280 --> 00:48:31,920
We're not talking about bad things. Things we should hate. Things we should feel guilty about.

306
00:48:32,720 --> 00:48:38,080
You shouldn't be on guilty about evil. You shouldn't hate yourself because you're evil.

307
00:48:38,080 --> 00:48:46,800
We should look at it scientifically. Let's talk about the scientifically evil, something being

308
00:48:46,800 --> 00:48:53,040
scientifically evil. Something is evil because it leads to suffering. That's it.

309
00:48:56,400 --> 00:49:01,520
We don't have to deny happiness. We just have to meditate, try and see things objectively.

310
00:49:01,520 --> 00:49:06,880
Look at who you are. Look at your state. So I like this a lot. I'm addicted to this.

311
00:49:07,920 --> 00:49:12,960
That's what I should look at. That's what I should study. I don't have to judge it.

312
00:49:14,080 --> 00:49:22,240
Just look at it. You'll see all the evil. You'll see what is you. Because you'll see what is

313
00:49:22,240 --> 00:49:31,040
leading you to suffering. This is not the right way to act. It just isn't. This is not

314
00:49:31,040 --> 00:49:37,200
bringing me positive results. You'll see that. You'll see it again and again and again.

315
00:49:42,480 --> 00:49:47,760
You'll see the things that I thought were going to make me happy. They just don't have the

316
00:49:47,760 --> 00:49:56,720
ability. They're not of the sort of the nature of. In fact, nothing is. The whole concept of

317
00:49:56,720 --> 00:50:06,240
something making you happy is just it's unsustainable. It's wrongheaded. You lose your inclination

318
00:50:06,240 --> 00:50:13,440
to think that way. This will make me happy. That will make me happy. How much stress and how much

319
00:50:13,440 --> 00:50:18,000
suffering comes from that line of thinking. This is what you'll start to see.

320
00:50:18,000 --> 00:50:30,080
When we talk about evil, we just mean we're really just talking about the cause of suffering.

321
00:50:31,920 --> 00:50:32,880
We all want to be happy.

322
00:50:38,960 --> 00:50:41,360
There you have it. All five types of evil.

323
00:50:41,360 --> 00:50:51,760
We know what we have to fight against. That's the demo for tonight.

324
00:50:51,760 --> 00:51:21,600
Thank you all for tuning in. Have a good night.

