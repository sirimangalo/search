1
00:00:00,000 --> 00:00:05,000
Hi, I'm welcome back to Ask a Monk.

2
00:00:05,000 --> 00:00:09,000
Next question comes from Tommy Turntable's one.

3
00:00:09,000 --> 00:00:12,000
I have a question about monks and health care.

4
00:00:12,000 --> 00:00:15,000
If a monk is to live without employment or money,

5
00:00:15,000 --> 00:00:20,000
what do they do if they find themselves needing medical attention?

6
00:00:20,000 --> 00:00:25,000
Well, first of all, monks don't live without employment, generally.

7
00:00:25,000 --> 00:00:30,000
They do live without money, but their employment is to teach the

8
00:00:30,000 --> 00:00:33,000
teachings of the Buddha to teach meditation.

9
00:00:33,000 --> 00:00:40,000
And so generally they do have supporters that will give them

10
00:00:40,000 --> 00:00:44,000
or help them out in whatever they need.

11
00:00:44,000 --> 00:00:47,000
If they need medicines, the supporters will give the medicines.

12
00:00:47,000 --> 00:00:54,000
If they need medical attention, the supporters will provide them with the means to go to see a doctor,

13
00:00:54,000 --> 00:00:57,000
or so on.

14
00:00:57,000 --> 00:01:00,000
Just as the leader of any religion,

15
00:01:00,000 --> 00:01:06,000
the religious people in other religions receive support, generally monetarily.

16
00:01:06,000 --> 00:01:13,000
But monks are, the stipulation is that they're only allowed to receive those things that they need directly.

17
00:01:13,000 --> 00:01:19,000
The point being that the receiving of money allows for an incredible amount of freedom

18
00:01:19,000 --> 00:01:21,000
in what you then do with the money.

19
00:01:21,000 --> 00:01:27,000
And it allows or leads, generally, to things which are not conducive to the monks' life.

20
00:01:27,000 --> 00:01:32,000
It's not as though monks have to do without the necessities.

21
00:01:32,000 --> 00:01:36,000
The point is that medicine is a necessity.

22
00:01:36,000 --> 00:01:42,000
There are four necessities for Buddhist monks and considered to be necessities for all people.

23
00:01:42,000 --> 00:01:44,000
The first one is robes.

24
00:01:44,000 --> 00:01:53,000
The second one is food. The third one is shelter and the fourth one is medicines.

25
00:01:53,000 --> 00:02:01,000
Now all four of these can be done without or can be reduced to a great extent.

26
00:02:01,000 --> 00:02:08,000
robes, you can collect rags, food, you can just go on arms and receive scraps

27
00:02:08,000 --> 00:02:12,000
from people's houses, whatever leftover food they have,

28
00:02:12,000 --> 00:02:14,000
whatever they're willing to give out.

29
00:02:14,000 --> 00:02:16,000
Shelter, you can live at the root of a tree.

30
00:02:16,000 --> 00:02:21,000
You can live in the forest or live in a simple dwelling.

31
00:02:21,000 --> 00:02:25,000
Generally living wherever people have a place for you to live.

32
00:02:25,000 --> 00:02:29,000
And medicines, you can use natural medicines.

33
00:02:29,000 --> 00:02:36,000
There are certain medicines that are open to monks which are easy to find.

34
00:02:36,000 --> 00:02:42,000
There are two ways to answer this question.

35
00:02:42,000 --> 00:02:44,000
One is that, yes, we have support.

36
00:02:44,000 --> 00:02:47,000
The other one is we don't require nearly so much attention.

37
00:02:47,000 --> 00:02:54,000
A lot of medical conditions can be dealt with without medicine,

38
00:02:54,000 --> 00:02:59,000
simply by bearing with the condition, bearing with the difficulty.

39
00:02:59,000 --> 00:03:05,000
For those that can, there are doctors and people who are willing to help

40
00:03:05,000 --> 00:03:08,000
to support you with the fees.

41
00:03:08,000 --> 00:03:13,000
For myself, recently I stepped on a rusty nail

42
00:03:13,000 --> 00:03:18,000
and there was a big concern that I might get tetanus

43
00:03:18,000 --> 00:03:23,000
because I hadn't had a tetanus shot in 15 years or something.

44
00:03:23,000 --> 00:03:27,000
And so I didn't mention it to anyone at first.

45
00:03:27,000 --> 00:03:29,000
I mainly I was busy and I didn't think of it.

46
00:03:29,000 --> 00:03:33,000
But then that evening I said, hey, you know, probably I should go to a doctor on Monday.

47
00:03:33,000 --> 00:03:36,000
And they get a tetanus shot.

48
00:03:36,000 --> 00:03:38,000
You know, you have to do it every 10 years.

49
00:03:38,000 --> 00:03:40,000
And suddenly everyone is all freaking out and thought, you know,

50
00:03:40,000 --> 00:03:44,000
you have to go right now right now to get this shot.

51
00:03:44,000 --> 00:03:49,000
And so they rushed me to the emergency room and made sure I got this shot.

52
00:03:49,000 --> 00:03:53,000
And at first I wasn't going to do it because I thought, oh, the emergency room,

53
00:03:53,000 --> 00:03:54,000
that's got to be really expensive.

54
00:03:54,000 --> 00:03:56,000
And they said, oh, no, no, maybe $100 or something.

55
00:03:56,000 --> 00:03:58,000
And don't worry, they would take care of it.

56
00:03:58,000 --> 00:04:01,000
And it turned out that it was like going to be over $500.

57
00:04:01,000 --> 00:04:07,000
So at the hospital I just said, I expressed my surprise that it was so expensive.

58
00:04:07,000 --> 00:04:10,000
And it turns out, you know, emergency rooms are expensive.

59
00:04:10,000 --> 00:04:15,000
And so right away they gave me this application.

60
00:04:15,000 --> 00:04:18,000
It was a Catholic hospital and they gave me this application.

61
00:04:18,000 --> 00:04:21,000
You know, if you're hard up, there are people who will support you.

62
00:04:21,000 --> 00:04:27,000
And it turns out the administration there has a program for people who don't have the ability to pay.

63
00:04:27,000 --> 00:04:33,000
And so I feel that this forum explaining that I was a monk and that no,

64
00:04:33,000 --> 00:04:37,000
I don't touch money and I rely on the support of other people and so on.

65
00:04:37,000 --> 00:04:42,000
And then it just, it vanished.

66
00:04:42,000 --> 00:04:45,000
I didn't hear anything about it and I was worried that the bill might get lost.

67
00:04:45,000 --> 00:04:50,000
So I called them and they said it had been written off as charity.

68
00:04:50,000 --> 00:04:53,000
So I think that's one specific example.

69
00:04:53,000 --> 00:04:58,000
But I think on a more broad scale there are ways and there are places

70
00:04:58,000 --> 00:05:04,000
I've heard of clinics among the Asian people that will take monks for free and so on and so on.

71
00:05:04,000 --> 00:05:06,000
So there are ways to get around it.

72
00:05:06,000 --> 00:05:12,000
I don't have health insurance though my students have talked about getting it for me.

73
00:05:12,000 --> 00:05:20,000
But right now since I'm on, I think I'm enrolled in this program with the Catholics,

74
00:05:20,000 --> 00:05:23,000
the Catholic hospital system.

75
00:05:23,000 --> 00:05:26,000
So I don't really worry about it.

76
00:05:26,000 --> 00:05:33,000
I think it's something that you could get health insurance if you wanted as a monk,

77
00:05:33,000 --> 00:05:37,000
if you have students who are willing to support you in that.

78
00:05:37,000 --> 00:05:38,000
I've talked to them about it.

79
00:05:38,000 --> 00:05:46,000
But right now in the situation I'm not really worried about it because I've seen that there are ways to get around it.

80
00:05:46,000 --> 00:05:54,000
Okay, so there's a one specific question. Interesting, interesting to talk about the requisites and how you get around that with money.

81
00:05:54,000 --> 00:06:00,000
I really think often people have this misconception that without money you wouldn't be able to live,

82
00:06:00,000 --> 00:06:03,000
you wouldn't be able to acquire the necessities.

83
00:06:03,000 --> 00:06:12,000
And I suppose for many people that might be true for homeless people for people who are out of a job,

84
00:06:12,000 --> 00:06:18,000
who are homeless, not by choice, but as a religious person you have a lot of support.

85
00:06:18,000 --> 00:06:26,000
And the existence of all that support itself is one of the reasons why monks are not allowed to handle money.

86
00:06:26,000 --> 00:06:34,000
Because you can very easily abuse that support if you attach money.

87
00:06:34,000 --> 00:06:42,000
Then all the support that the faith that people have in you means that they're just willing to pile money onto you at all the time.

88
00:06:42,000 --> 00:06:44,000
And so this happens, there's a lot of corruption.

89
00:06:44,000 --> 00:06:48,000
I mean you see that in religions where religious people do accept money.

90
00:06:48,000 --> 00:06:55,000
There's a lot of corruption. You see it in Buddhism where monks, if monks accept money, it becomes quite corrupt.

91
00:06:55,000 --> 00:07:01,000
So it's not as though not touching money means we don't have support.

92
00:07:01,000 --> 00:07:07,000
And it's a rule that forbids us from taking advantage of people's support.

93
00:07:07,000 --> 00:07:10,000
So I hope that helps to clear things up.

94
00:07:10,000 --> 00:07:34,000
And thanks for the question, keep them coming.

