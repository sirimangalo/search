1
00:00:00,000 --> 00:00:27,400
The Buddha called the mind radiant papasaram and to be clear he wasn't implying that the

2
00:00:27,400 --> 00:00:41,400
mind had substance or luminosity. I give you went in a dark room with the mind.

3
00:00:41,400 --> 00:00:52,200
It would light up the room. It was a figure of speech that has been misinterpreted.

4
00:00:52,200 --> 00:01:03,200
But he simply meant that the mind is pure, pure like like a radiant gem crystal clear,

5
00:01:03,200 --> 00:01:10,200
like a crystal clear pool of water. There are no defilements inherent in the mind.

6
00:01:10,200 --> 00:01:22,400
It was a literary tool, oratory tool. When he said the mind is brilliant, he was trying

7
00:01:22,400 --> 00:01:37,200
to make a point that our ordinary experience of things is not defiled. It's not unwholesome.

8
00:01:37,200 --> 00:01:46,200
It's not bad ever. It's not good ever, either. It has no ethical quality to it whatsoever.

9
00:01:46,200 --> 00:02:02,200
It's innocent. No matter what we see or hear, smell or taste or feel or think, even our thoughts

10
00:02:02,200 --> 00:02:15,200
are innocent, which is an important point because quite often we feel guilty for having certain thoughts,

11
00:02:15,200 --> 00:02:25,200
for imagining certain situations. But our thoughts and our imaginations are not entirely

12
00:02:25,200 --> 00:02:40,200
caused by our inclinations. Thoughts can come based on past habits. At any rate, the thoughts

13
00:02:40,200 --> 00:02:54,200
themselves are ethically inert. So really, there's nothing wrong with the world.

14
00:02:54,200 --> 00:03:06,200
There's just no problem in the world, in our world, in our lives, except for three very

15
00:03:06,200 --> 00:03:19,200
small, tiny details, for only three things that are wrong with our world, wrong with our

16
00:03:19,200 --> 00:03:31,200
mind. These are called the Akusa the Mula, the root seven wholesomeness. Mula means

17
00:03:31,200 --> 00:03:52,200
root. By root is meant base of a mind. So an important quality of the mind that roots it on either

18
00:03:52,200 --> 00:04:04,200
side of the fence. If a mind is rootless, then you can't say it's good or bad. But if it's

19
00:04:04,200 --> 00:04:15,200
rooted in one of these three things, these three make it bad. One of the three there,

20
00:04:15,200 --> 00:04:36,200
low butt, desire, Dosa, aversion, and Moha delusion. Our colloquial speech would say

21
00:04:36,200 --> 00:04:47,200
greed, anger, and delusion these three. These are what's wrong with the world. All problems

22
00:04:47,200 --> 00:05:03,200
can be boiled down to these three from a Buddhist perspective. It's not easy to see this,

23
00:05:03,200 --> 00:05:12,200
it's quite likely to be contested. Not a very appealing teaching when we think of all

24
00:05:12,200 --> 00:05:17,200
that's wrong with the world. We want to say there's so many other things wrong with this

25
00:05:17,200 --> 00:05:25,200
world. How can you trivialize the evil of the world, ignore all that's wrong with this

26
00:05:25,200 --> 00:05:39,200
world? This sort of opposition is likely caused by our investment in solutions. We know

27
00:05:39,200 --> 00:05:45,200
what's wrong with the world. Anyone will tell you what's wrong with the world. And they'll

28
00:05:45,200 --> 00:06:01,200
offer up all sorts of solutions, maybe economical, societal, political. And then these are actually

29
00:06:01,200 --> 00:06:16,200
something at the root of the problem. Through the practice of mindfulness, it's a crucial shift

30
00:06:16,200 --> 00:06:24,200
in perspective to realize that there's nothing wrong with experience, experiences in the

31
00:06:24,200 --> 00:06:36,200
world. It is what it is. It's not worth clinging to. There's nothing about experience that

32
00:06:36,200 --> 00:06:48,200
it's worth getting upset about. I'm worth getting excited about. The cause of suffering

33
00:06:48,200 --> 00:06:59,200
is not our experiences. It's our relationship to them with greed, anger, and delusion.

34
00:06:59,200 --> 00:07:13,200
So greed, greed is our desire, liking and wanting. And there's no question, there should

35
00:07:13,200 --> 00:07:24,200
be no argument as to whether greed can be a problem. Obviously greed leads to addiction,

36
00:07:24,200 --> 00:07:45,200
it leads to rapaciousness, it leads to manipulation, it leads to ruin for those who become

37
00:07:45,200 --> 00:08:04,200
gamblers, for those who are addicted to the point of obsessive consumption. But it has

38
00:08:04,200 --> 00:08:15,200
more subtle ramifications. There's no, I don't think anyone with a basic appreciation of state

39
00:08:15,200 --> 00:08:22,200
of reality has an argument against rapacious greed, that somehow it can be okay or benevolent

40
00:08:22,200 --> 00:08:43,200
or innocuous. But what about romance? What about a love of food, music, poetry, art? What

41
00:08:43,200 --> 00:08:58,200
about love of friendship, love of family? What about these likings? Well, we can see some

42
00:08:58,200 --> 00:09:11,200
of the ways that some of our subtle desires have led to ruin, have created great suffering

43
00:09:11,200 --> 00:09:23,200
for the world. For example, the climate, the environment we're seeing, the results of

44
00:09:23,200 --> 00:09:32,200
seemingly innocuous greed, everybody wants stuff, but all this stuff, getting what we want

45
00:09:32,200 --> 00:09:42,200
when we want it has actually made our world less comfortable, has made the planet a less

46
00:09:42,200 --> 00:09:49,200
comfortable place to live. We've polluted our own air, water, we've destroyed our natural

47
00:09:49,200 --> 00:09:57,200
environment, made it ugly. There's no question that the world is uglier because of our

48
00:09:57,200 --> 00:10:11,200
rapaciousness. There's no question that greed has an effect on health. Our love of

49
00:10:11,200 --> 00:10:21,200
good food makes us fat, makes us unhealthy, leads to diabetes, heart disease, cancer,

50
00:10:21,200 --> 00:10:34,200
but it goes even deeper than that. We're not as happy as we think we are. Take love

51
00:10:34,200 --> 00:10:42,200
of our family, for example. The put romance will just skip romance because it's a little

52
00:10:42,200 --> 00:10:51,200
easier to see how disturbing that can be if you've ever been at the end of the receiving

53
00:10:51,200 --> 00:10:57,200
end of a breakup. You know how much stress and suffering is involved in this intense

54
00:10:57,200 --> 00:11:10,200
anguish, not being able to get what you want to put it very simply. But family takes

55
00:11:10,200 --> 00:11:19,200
a family, seems like a good thing. You know, you should love your family. What's wrong

56
00:11:19,200 --> 00:11:29,200
with that? It's one of the least threatening forms of desire, of liking, of attachment,

57
00:11:29,200 --> 00:11:38,200
let's say. It seems pretty innocent and even wholesome. Family is important, people will

58
00:11:38,200 --> 00:11:50,200
not. But consider what makes your family special? What makes your family worth your attachment?

59
00:11:50,200 --> 00:12:02,200
More than somebody else's family. What makes family? For some people, family is people unrelated

60
00:12:02,200 --> 00:12:11,700
to by blood. They call those people family. Friends become so close, you call them family.

61
00:12:11,700 --> 00:12:18,200
Close family becomes someone person on the non-grata. You no longer consider them family

62
00:12:18,200 --> 00:12:30,200
based on their character, based on their actions and so on. But my point is, what benefit

63
00:12:30,200 --> 00:12:38,200
comes from seeing certain individuals as special? Why is that a good thing? What's so good

64
00:12:38,200 --> 00:12:49,200
about that? How is that better than, say, having a friendly attitude towards all beings?

65
00:12:49,200 --> 00:12:57,200
Well, that's the greatness that we stress and we focus on in Buddhism, the greatness

66
00:12:57,200 --> 00:13:10,200
of universal friendliness, universal appreciation and cordiality and kindness, compassion.

67
00:13:10,200 --> 00:13:19,200
I don't want to say caring, but it looks a lot like caring. Kindness is a little more innocuous,

68
00:13:19,200 --> 00:13:31,200
a little more proper. Imagine if the whole world was family, if you just saw everyone.

69
00:13:31,200 --> 00:13:47,200
As you see your family, people with flaws, individuals with flaws, but worthy of appreciation

70
00:13:47,200 --> 00:13:59,200
and respect, respecting that they too have the potential to better themselves if given the opportunity.

71
00:13:59,200 --> 00:14:14,200
The world is not very equitable at the moment. If you look in rich societies like Canada, America, China,

72
00:14:14,200 --> 00:14:22,200
there's a great inequity in those rich countries, but there are also very, very poor countries

73
00:14:22,200 --> 00:14:29,200
where there may be a few rich corrupt politicians, but the majority of people live in what we would consider

74
00:14:29,200 --> 00:14:42,200
to be abject poverty without running water without medicine, often without food.

75
00:14:42,200 --> 00:15:00,200
Yes, this occurs in rich countries, but we see in the world there are places where it's the norm, the norm is to live in abject poverty.

76
00:15:00,200 --> 00:15:16,200
And my point being that we do nothing. We look after our own Canada's my family, Canada, what a great nation.

77
00:15:16,200 --> 00:15:32,200
I was thinking today about there was a story about someone who had been accused of murder. I was a person who, a Nazi.

78
00:15:32,200 --> 00:15:49,200
There's a Nazi in Germany who just ran away from a trial and they've been charged with 8,000 counts of not murder, but complicit complicity.

79
00:15:49,200 --> 00:16:05,200
But it's interesting how we call that murder.

80
00:16:05,200 --> 00:16:15,200
And certainly it sounds like a terrible thing that they were involved in. It was a terrible thing that it sounds like they were involved in.

81
00:16:15,200 --> 00:16:21,200
It's interesting how we call that murder, which it was.

82
00:16:21,200 --> 00:16:28,200
And yet, when we go to war, war is never considered murder.

83
00:16:28,200 --> 00:16:44,200
This isn't at all to trivialize Nazi Germany. The Holocaust is a terrible thing, like the killing fields in Cambodia, but we call it murder, but we kill people all the time.

84
00:16:44,200 --> 00:17:07,200
Soldiers kill people, why isn't the soldier convicted of murder? It's a ridiculous question, or it seems like one, but it does bring up an interesting point that we don't see the rest of the world as actual beings on the same level as our country.

85
00:17:07,200 --> 00:17:28,200
It's implicit in a lot of things. We are kind often to take in refugees, some of the monks here are refugees, but when it comes right down to it, if you're not a Canadian citizen, you're not one of us.

86
00:17:28,200 --> 00:17:42,200
Anyway, just an example of how the world does do itself a disservice to think in terms of us and them.

87
00:17:42,200 --> 00:18:00,200
So, just an example of greed not being actually all that useful. I got quite a far field, but I was making a point that this is based on greed. It's based on partiality, which requires greed.

88
00:18:00,200 --> 00:18:19,200
The only way for us, and this goes for so many things, but the only way for us to overcome this, us and them mentality, is to give up greed, because you can't talk yourself into loving all beings.

89
00:18:19,200 --> 00:18:41,200
It's a mistake to try to force yourself to practice meditation, without purifying yourself from greed, because it's artificial. It's temporary. It's not a sustainable solution.

90
00:18:41,200 --> 00:18:57,200
Someone can practice me to be very loving and kind, but until they can give up this partiality, they're always going to prefer and be kinder to certain people than others.

91
00:18:57,200 --> 00:19:06,200
We haven't even got into talking about anger yet.

92
00:19:06,200 --> 00:19:13,200
But giving up greed is incredibly powerful. It's just one example of it.

93
00:19:13,200 --> 00:19:25,200
If we gave up greed, the environment would benefit. If we gave up greed, our political system, what would it look like if politicians were not so corrupt and greedy?

94
00:19:25,200 --> 00:19:38,200
It would look like if we weren't so greedy when we voted and were more thoughtful and conscientious. What would our economic system look like if we were not so greedy?

95
00:19:38,200 --> 00:19:54,200
But most importantly, what would our mind be like? Imagine. No preference. Imagine being able to experience your life, your reality, without judgment, without partiality.

96
00:19:54,200 --> 00:20:19,200
The question, why is greed a bad thing can only come from a place of ignorance? If you are wondering, what is wrong with greed and you just can't see it, I tell you it's because the mind is unclear.

97
00:20:19,200 --> 00:20:48,200
There's no explanation I can give you to explain to you why greed, not on the same level of asking you to spend time present with your experiences, seeing them as they are, to cleanse your mind, to clear up this fog of delusion and anger and greed.

98
00:20:48,200 --> 00:20:58,200
To the point where you see how much stress it's causing, how it really limits you as a being.

99
00:20:58,200 --> 00:21:04,200
The more you cling, the more limited you become. Human beings are a good example.

100
00:21:04,200 --> 00:21:17,200
Apparently, the story of why there are human beings, why there are earthlings at all, is because we were these luminous beings in outer space.

101
00:21:17,200 --> 00:21:46,200
And we saw this luminous ball of whatever and got attached to it and clung to it. And clung and clung and evolved through our clinging and become corrupted through our clinging and through our consumption actually, through consuming the energy from the earth.

102
00:21:46,200 --> 00:21:57,200
And we came to be called earth. Our own beings became corsor. The earth became corsor and corsor.

103
00:21:57,200 --> 00:22:08,200
And here we are. And that's why we're stuck to this rock. So the short version of the story of why we're stuck to this rock.

104
00:22:08,200 --> 00:22:23,200
Anyway, that's not probably very convincing that people wouldn't believe such things, but it does present an interesting perspective. It certainly does fit with reality, the reality that when we cling, we become less.

105
00:22:23,200 --> 00:22:49,200
The more we desire, the less happiness we end up with, the cling and cling and cling. And the realm of what is acceptable to us shrinks, the realm of what we strive for, our horizons become.

106
00:22:49,200 --> 00:22:57,200
It's smaller.

107
00:22:57,200 --> 00:23:10,200
And narrower. So that's greed. Anger, anger we can see quite clearly.

108
00:23:10,200 --> 00:23:29,200
They say that greed is hard to see the problem with greed, the fault in greed. But it's slow to change.

109
00:23:29,200 --> 00:23:48,200
So it's bad. Most, I mean, it's hard to get rid of. It's slow to change.

110
00:23:48,200 --> 00:23:55,200
Anger is easier to see. It's more commonly faulted.

111
00:23:55,200 --> 00:24:01,200
When someone is angry, you don't hear people cheering them on. Not so much.

112
00:24:01,200 --> 00:24:12,200
Anger makes you ugly. It makes you sick. Greed can make you sick, as I said, but anger is a state of sickness and illness really.

113
00:24:12,200 --> 00:24:24,200
Your blood boils, they say. Not literally, but it does heat up your body. Great tension, stress.

114
00:24:24,200 --> 00:24:37,200
Headaches. Anger is related to cruelty, required for cruelty.

115
00:24:37,200 --> 00:24:46,200
It's what allows us to go to war. Think of all hatred or greed as a cause of war as well.

116
00:24:46,200 --> 00:24:56,200
When it comes down to the killing, you have to be cruel. You have to ignore, repress,

117
00:24:56,200 --> 00:25:04,200
your natural state of appreciation of life because we don't want to die away.

118
00:25:04,200 --> 00:25:08,200
We think of life as a positive, a happy thing.

119
00:25:08,200 --> 00:25:16,200
And we have to intentionally, knowingly, take that happiness away from others.

120
00:25:16,200 --> 00:25:29,200
When we hurt, when we harm others, then there are the more subtle versions of anger as well.

121
00:25:29,200 --> 00:25:49,200
Because anger bad is not such a hard question to answer, but on a subtle level, our anger

122
00:25:49,200 --> 00:25:55,200
can be directed even at ourselves.

123
00:25:55,200 --> 00:26:10,200
We have self-hatred, but ultimately it's simply the hatred of our experiences.

124
00:26:10,200 --> 00:26:21,200
We can have anger towards pain, anger towards the past, towards the future.

125
00:26:21,200 --> 00:26:36,200
Anger is easier to see the problem in, but it still gets hidden with things like pain, even boredom, frustration,

126
00:26:36,200 --> 00:26:50,200
and anytime it becomes self-righteous, you make me so angry. What's the problem? You are the problem.

127
00:26:50,200 --> 00:26:58,200
Totally overlooked the fact that you're angry that, oh yeah, wait, if I were in anger there would be no problem.

128
00:26:58,200 --> 00:27:12,200
But you deserve it. I don't deserve that. How could you? How dare you?

129
00:27:12,200 --> 00:27:27,200
Our inability to accept reality is not necessary to get angry, to strive for justice, for example, to fight for justice, even.

130
00:27:27,200 --> 00:27:34,200
There's no need to be angry with the Nazis in Germany. We can be horrified.

131
00:27:34,200 --> 00:27:44,200
I think that it's possible in some sense to be horrified, shocked, because those aren't big deal.

132
00:27:44,200 --> 00:27:50,200
We should never be angry. We don't do any good by being angry.

133
00:27:50,200 --> 00:27:55,200
It's not better to be angry than to be, say, wise.

134
00:27:55,200 --> 00:28:07,200
I mean, wisdom is not going to say, oh, it's okay. You killed all those people, no problem. Wisdom is going to say that was a very bad thing you did, just the same as anger, with or without anger.

135
00:28:07,200 --> 00:28:20,200
You don't need the anger to understand that it was a horrible thing that the Nazis did in Germany, or the Khmer Rouge did in Cambodia, or any other of the many atrocities that

136
00:28:20,200 --> 00:28:29,200
were done to the First Nations people in Canada and the residential schools.

137
00:28:29,200 --> 00:28:37,200
Today, I think, or if it's tomorrow, I'm not sure, is the truth, the truth, and reconciliation again.

138
00:28:37,200 --> 00:28:55,200
The horrible things were done, not just out of anger, but anger is required to be so cruel.

139
00:28:55,200 --> 00:29:07,200
And the third is delusion. Delusion is the worst of the three, because it's both highly blamed, and it's slow to change.

140
00:29:07,200 --> 00:29:17,200
Anger is quick to change, I didn't mention it. Highly blamed quick to change. Delusion, highly blamed slow to change, it's the worst.

141
00:29:17,200 --> 00:29:27,200
And you can't have greed or anger without delusion because they're wrong.

142
00:29:27,200 --> 00:29:32,200
The only way they can arise is if you don't know that they're wrong, if you aren't clear in your mind that they're wrong.

143
00:29:32,200 --> 00:29:42,200
When they have an opportunity to arise, there has to be, at that moment, some sort of delusion.

144
00:29:42,200 --> 00:29:52,200
They would never arise, and that's, of course, the great power of mindfulness.

145
00:29:52,200 --> 00:29:57,200
We best in seeing clearly.

146
00:29:57,200 --> 00:30:02,200
And it's something that we have to remind meditators over and over again.

147
00:30:02,200 --> 00:30:07,200
We're not trying to fix our problems.

148
00:30:07,200 --> 00:30:11,200
We're trying to see them clearly.

149
00:30:11,200 --> 00:30:23,200
It seems a bit disingenuous because, of course, we're trying to fix our problems.

150
00:30:23,200 --> 00:30:28,200
In some sense, we're trying to fix something anyway.

151
00:30:28,200 --> 00:30:36,200
We're trying to fix something, but the way of fixing turns out to be very different from what we think.

152
00:30:36,200 --> 00:30:41,200
Our idea of fixing is doing something to fix.

153
00:30:41,200 --> 00:30:54,200
When the real fix is the change in perspective, the clarity that the true fix is freedom from delusion.

154
00:30:54,200 --> 00:31:16,200
Let's go through it. When are some of the big delusions out there in the world, racism, bigotry, xenophobia?

155
00:31:16,200 --> 00:31:27,200
Religion. Religion is a huge delusion. Think of Israel, the situation in the Middle East.

156
00:31:27,200 --> 00:31:36,200
There's this place in Jerusalem that they've been fighting over for a thousand of years.

157
00:31:36,200 --> 00:31:47,200
I mean, at least we've been fighting over for decades now.

158
00:31:47,200 --> 00:31:52,200
But all the whole fight is over some ridiculous notion that it's unfathomable how much delusion is involved there.

159
00:31:52,200 --> 00:32:05,200
There's, of course, greed and anger, of course, anger, bitterness, us and them, the enemy, the dehumanization of other humans, other beings.

160
00:32:05,200 --> 00:32:16,200
That's so much delusion involved. There's a big one so we can see those arrogance on a personal level arrogance who likes an arrogant person.

161
00:32:16,200 --> 00:32:28,200
You can see self-righteousness, people who are preachy. Buddhist can be very preachy sometimes.

162
00:32:28,200 --> 00:32:44,200
We have, just because you say your Buddhist or become Buddhist doesn't mean you're going to be free from these kind of arrogance and conceit and self-righteousness.

163
00:32:44,200 --> 00:33:00,200
A good example of a good opportunity for a reminder that we should be thoughtful and respectful to our fellow Buddhists that they won't always be perfect.

164
00:33:00,200 --> 00:33:15,200
And we shouldn't overlook the good side of the Buddha said, put aside the bad side, try and focus on the good in people and help nourish that.

165
00:33:15,200 --> 00:33:24,200
But on a personal level we can see there's another people we can see in ourselves, especially when we start to practice meditation. It's ugly.

166
00:33:24,200 --> 00:33:36,200
You can see it arrogance. But the worst or the most ultimate form of delusion is quite simple. It's just ignorance.

167
00:33:36,200 --> 00:33:59,200
We jump at Jeyasankara. All of our inclinations, good or bad come from ignorance. There's nothing we need to do besides clearing this up. We spend all our time in meditation doing something very simple and it's so easy to misunderstand or overlook it.

168
00:33:59,200 --> 00:34:09,200
It's hard to really get what we're doing because not because it's something deeper, it's so simple. You miss it.

169
00:34:09,200 --> 00:34:19,200
When you're walking, what are you trying to do? You're trying to know that your foot is raising. But what else? What's behind that? What's the point of that?

170
00:34:19,200 --> 00:34:33,200
Honestly, the point is to give up asking what is the point? Because as long as you try to find a point in things, you're making more of them than they actually are. That's the whole point.

171
00:34:33,200 --> 00:34:48,200
That's it. Once you can see, seeing is just seeing, hearing is just hearing, then you will see clearly. Then you will be present. Then you will free yourself from all of this greed, anger and delusion stuff.

172
00:34:48,200 --> 00:35:05,200
It's caught up in ignorance. It comes because you don't see, because rather than having set the meaning remembering what you're experiencing or being with what you experience, you make it into something else.

173
00:35:05,200 --> 00:35:12,200
You get lost in conceptualization.

174
00:35:12,200 --> 00:35:25,200
As I've said before, reality is quite simple. Understanding it is not an easy thing, but it is a fairly simple thing.

175
00:35:25,200 --> 00:35:41,200
There's not much in the world that has a problem. If we can focus on these three things and appreciate them, most importantly, try to see our experiences clearly as they are freeing ourselves from ignorance.

176
00:35:41,200 --> 00:35:53,200
The whole chain of causation leading us to sorrow, sadness, limitation, despair, suffering.

177
00:35:53,200 --> 00:36:02,200
That whole chain of causality is broken, and experience is just experience.

178
00:36:02,200 --> 00:36:12,200
So that's the demo for today.

