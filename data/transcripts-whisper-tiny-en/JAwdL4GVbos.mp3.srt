1
00:00:00,000 --> 00:00:05,000
Hey, everyone. This is Adder, a volunteer with Sierra Mangolo International.

2
00:00:05,000 --> 00:00:11,000
I'm here today to give a brief update on our fundraising project.

3
00:00:11,000 --> 00:00:16,000
A week ago, we started our fundraiser for our Meditation Center project

4
00:00:16,000 --> 00:00:21,000
in hopes that we can, with enough support from our community,

5
00:00:21,000 --> 00:00:27,000
establish a permanent home of our own for our meditation practice,

6
00:00:27,000 --> 00:00:32,000
and serve more meditators, rather than operating out of a rented space

7
00:00:32,000 --> 00:00:35,000
with a relatively low capacity.

8
00:00:35,000 --> 00:00:39,000
And in this first week, it's been amazing to see

9
00:00:39,000 --> 00:00:44,000
all of the support and excitement that our community has.

10
00:00:44,000 --> 00:00:48,000
We've raised $14,000 in those first seven days,

11
00:00:48,000 --> 00:00:55,000
and I want to take a moment to share my thanks to everyone in our community

12
00:00:55,000 --> 00:01:01,000
and all those who have offered their support monetarily or through their volunteer efforts

13
00:01:01,000 --> 00:01:06,000
or through sharing our fundraiser with others.

14
00:01:06,000 --> 00:01:12,000
We don't see this project as just something that a few of us are working on

15
00:01:12,000 --> 00:01:16,000
and sort of putting out there to collect stuff back.

16
00:01:16,000 --> 00:01:22,000
It's really, we want this to be a community event, a community project.

17
00:01:22,000 --> 00:01:27,000
So, if there's any way that you're excited to contribute and be a part of this,

18
00:01:27,000 --> 00:01:28,000
let us know.

19
00:01:28,000 --> 00:01:32,000
We have some people running their own mini fund raisers

20
00:01:32,000 --> 00:01:37,000
or sharing this with their communities in their own way.

21
00:01:37,000 --> 00:01:42,000
If there's any way that you want to contribute or if you have any ideas of things

22
00:01:42,000 --> 00:01:46,000
that we could be doing, reach out to us, let us know.

23
00:01:46,000 --> 00:01:51,000
We want to support you and we want, we gladly accept

24
00:01:51,000 --> 00:01:57,000
any support that you want to give to the rest of the Sierra Mongolow community.

25
00:01:57,000 --> 00:02:02,000
Our GoFundMe page has a comments feature,

26
00:02:02,000 --> 00:02:08,000
and I'd like to ask people if you feel so inclined to post a brief comment,

27
00:02:08,000 --> 00:02:15,000
sharing something about your practice or about what this project means to you.

28
00:02:15,000 --> 00:02:19,000
Perhaps you have a dedication you want to make or a message you want to send

29
00:02:19,000 --> 00:02:22,000
to the Sierra Mongolow community.

30
00:02:22,000 --> 00:02:30,000
And we also will see more support the further reach that this fundraiser has.

31
00:02:30,000 --> 00:02:36,000
So, if you want to share this with the communities that you're involved in,

32
00:02:36,000 --> 00:02:42,000
outside of Sierra Mongolow, that would be a great blessing.

33
00:02:42,000 --> 00:02:48,000
Maybe that's your real-life community, maybe that's your friends and family,

34
00:02:48,000 --> 00:02:51,000
or a local sangha that you're involved in.

35
00:02:51,000 --> 00:02:55,000
But for many of us, that's an online community.

36
00:02:55,000 --> 00:02:59,000
If there's a subreddit that you're involved in or a Facebook group,

37
00:02:59,000 --> 00:03:04,000
these are great opportunities to share what we're doing.

38
00:03:04,000 --> 00:03:10,000
We'll also be doing a testimonial series,

39
00:03:10,000 --> 00:03:14,000
a series of videos in which people share their own story

40
00:03:14,000 --> 00:03:19,000
about their meditation practice and their experience with the Dhamma.

41
00:03:19,000 --> 00:03:24,000
So, we'll come up with a list of questions that people can use as guidelines,

42
00:03:24,000 --> 00:03:31,000
but really we're just looking for a wide collection of people's individual stories.

43
00:03:31,000 --> 00:03:36,000
And so, if you'd like to share yours, the rest of us would love to hear it.

44
00:03:36,000 --> 00:03:40,000
So, you can feel free to record a video on your own.

45
00:03:40,000 --> 00:03:48,000
We could even do a virtual interview, or you could arrange a live interview with someone.

46
00:03:48,000 --> 00:03:54,000
You could briefly share a few words or tell us in more detail about your journey.

47
00:03:54,000 --> 00:03:58,000
So, if you feel like sharing, we would love to hear that,

48
00:03:58,000 --> 00:04:05,000
and we will show those as part of our fundraising series of videos.

49
00:04:05,000 --> 00:04:09,000
So, please stay in touch.

50
00:04:09,000 --> 00:04:14,000
The rest of us will try to make sure to stay in touch with the community at large.

51
00:04:14,000 --> 00:04:20,000
We hope to keep you updated as we continue this fundraiser over the next three months.

52
00:04:20,000 --> 00:04:24,000
So, I look forward to staying in touch with you all,

53
00:04:24,000 --> 00:04:28,000
and I can't wait to see what happens next.

54
00:04:28,000 --> 00:04:35,000
Thank you all. May you be well.

