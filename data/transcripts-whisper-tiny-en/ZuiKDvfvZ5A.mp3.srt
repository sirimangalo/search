1
00:00:00,000 --> 00:00:10,960
Is it good to give up, a 19 out of college, I can't find a job.

2
00:00:10,960 --> 00:00:11,960
What should I do?

3
00:00:11,960 --> 00:00:18,440
I think a lot of people are stressed out by unemployment and you know, you've been frustrated

4
00:00:18,440 --> 00:00:27,800
by such a daily lives, but what can people do to at least keep their mind straight and

5
00:00:27,800 --> 00:00:34,400
focused and centered on only living.

6
00:00:34,400 --> 00:00:35,400
On what?

7
00:00:35,400 --> 00:00:36,400
On living?

8
00:00:36,400 --> 00:00:37,400
Living?

9
00:00:37,400 --> 00:00:38,400
Yes.

10
00:00:38,400 --> 00:00:39,400
Yes.

11
00:00:39,400 --> 00:00:44,320
This person seems to know, he has more of this given up.

12
00:00:44,320 --> 00:00:50,040
Should we ever give up or what secrets are there or what practices are there that can get

13
00:00:50,040 --> 00:00:55,600
us back in a mode of at least continuing to attempt?

14
00:00:55,600 --> 00:01:07,000
Well, if I can give a first little bit of answer here is that, and I want you to understand

15
00:01:07,000 --> 00:01:13,920
carefully and don't take in what I say until you've heard it all, there's something

16
00:01:13,920 --> 00:01:21,960
I told a, it's kind of tongue in cheek, so it would be very careful with it and let me explain

17
00:01:21,960 --> 00:01:24,160
it afterwards and then I'll give you probably a better answer.

18
00:01:24,160 --> 00:01:37,040
Now, the tongue in cheek part is that the first thing to do is to give up.

19
00:01:37,040 --> 00:01:46,400
This woman, this woman from Spain came to practice with me and she said she wanted to

20
00:01:46,400 --> 00:01:47,400
die.

21
00:01:47,400 --> 00:01:51,400
She wants to kill her, she wanted to kill herself, she was meditating, she said she wants

22
00:01:51,400 --> 00:01:52,400
to kill herself.

23
00:01:52,400 --> 00:01:57,240
I said, good, good, kill yourself and then keep meditating.

24
00:01:57,240 --> 00:02:03,000
And I was laughing very, very, you know, I was very clear with her that this was what

25
00:02:03,000 --> 00:02:07,920
I was saying because as I say, this is why you have to be careful with these things,

26
00:02:07,920 --> 00:02:13,240
but with her it was, it was really good and she recovered a lot.

27
00:02:13,240 --> 00:02:21,600
She was, she had some of her own problems.

28
00:02:21,600 --> 00:02:34,760
So the point, the point, the point of it is that even with meditation, many, many, many,

29
00:02:34,760 --> 00:02:44,640
many times during meditation you have to give up, stop meditating, stop, stop striving

30
00:02:44,640 --> 00:02:50,520
and then start again or and then keep going.

31
00:02:50,520 --> 00:02:55,680
Go away all of what you are doing, throw away all of what you are hoping for, what you

32
00:02:55,680 --> 00:03:06,520
are needing, what you are wanting, throw away your life and start again.

33
00:03:06,520 --> 00:03:12,120
Because from a point of view of why this occurs in practice and why it is important in

34
00:03:12,120 --> 00:03:16,880
practice is because practice doesn't exist, there is no meditation, you are not doing

35
00:03:16,880 --> 00:03:23,400
meditation, you are not taking on a practice.

36
00:03:23,400 --> 00:03:27,640
And the problem comes is that you do take on practices, you think it is a trick that you

37
00:03:27,640 --> 00:03:30,800
can say pain, pain, pain and all your pain is going to go away.

38
00:03:30,800 --> 00:03:34,320
And you get more and more frustrated when it doesn't work.

39
00:03:34,320 --> 00:03:38,280
You have this practice that doesn't work, people, you know, this person who said earlier

40
00:03:38,280 --> 00:03:42,960
breath meditation helps me balance, that could be true, but it could also be true that

41
00:03:42,960 --> 00:03:47,040
it is just a trick that you are using, it could be true that you are just covering it all

42
00:03:47,040 --> 00:03:52,000
up with this trick that you have to get to a state of calm and peace.

43
00:03:52,000 --> 00:03:55,280
In which case it is not going to work and eventually you are just going to get so frustrated

44
00:03:55,280 --> 00:03:59,760
that you are just going to want to give up and that is what you should do, you should

45
00:03:59,760 --> 00:04:06,680
give up, stop meditating, stop caring, stop wanting, stop striving, give up everything and

46
00:04:06,680 --> 00:04:08,240
then start again.

47
00:04:08,240 --> 00:04:12,640
Start again without all of that baggage, this is the point not start again but do it

48
00:04:12,640 --> 00:04:14,600
without all that baggage.

49
00:04:14,600 --> 00:04:19,480
So you say you want to give up, I think it can be extrapolated into your life, give

50
00:04:19,480 --> 00:04:26,280
up all of your life and start again without all that baggage, without all your expectations,

51
00:04:26,280 --> 00:04:28,680
without all your needs.

52
00:04:28,680 --> 00:04:37,000
Start again as a person living on the street and put your life back together in a way

53
00:04:37,000 --> 00:04:45,200
that is free from all the baggage.

54
00:04:45,200 --> 00:04:54,520
Now that is just the first little bit of answer because the next part is, well suppose

55
00:04:54,520 --> 00:05:01,640
you don't have hope, suppose all you can expect from, it is not that you have expectations

56
00:05:01,640 --> 00:05:10,880
is that you have no hope, that you are out of work, out of money, in debt and so on.

57
00:05:10,880 --> 00:05:20,720
You are in a state where it is pitiable, that we get into sometimes and some people get

58
00:05:20,720 --> 00:05:25,080
into, some people don't get into and really interesting to read what people have to say

59
00:05:25,080 --> 00:05:33,800
about this, people who are not in it have a very hard time being sympathetic or even

60
00:05:33,800 --> 00:05:41,480
believing people who are in these states and yet it can be so devastating and horrible

61
00:05:41,480 --> 00:05:49,920
to have to be in such a state, depends on the person, but what is the answer?

62
00:05:49,920 --> 00:05:59,640
Well the Buddhist answer, whether it comforts you or whether it helps you, is to accept

63
00:05:59,640 --> 00:06:05,680
and learn from it, I mean let me take an example that is very close to us here, is Larry

64
00:06:05,680 --> 00:06:06,680
for example.

65
00:06:06,680 --> 00:06:16,160
Larry had to go through several years of great stress and turmoil, but none of it was as

66
00:06:16,160 --> 00:06:24,680
I understand, was involving being destitute, you must have caused money problems, I think

67
00:06:24,680 --> 00:06:31,520
you mentioned that, but not to the point of, or let's use, maybe if it's not using Larry,

68
00:06:31,520 --> 00:06:36,880
but someone like this who is in this sort of a state and has such horrible, even though

69
00:06:36,880 --> 00:06:42,200
they have money, even though they have a home and family and people who care for them have

70
00:06:42,200 --> 00:06:48,080
to go through horrible states of physical and even mental suffering.

71
00:06:48,080 --> 00:06:53,120
So here is a person, you could say could even be a person who has a job or even a person

72
00:06:53,120 --> 00:06:56,560
who suppose they have an inheritance or their rich and so they don't really even have

73
00:06:56,560 --> 00:07:00,640
to work, a person who was born with a silver spoon and their mouth, suppose this sort

74
00:07:00,640 --> 00:07:05,200
of person, but they get sick and they have to go through this horrible suffering.

75
00:07:05,200 --> 00:07:12,200
Well, are you really in a worse position than that person?

76
00:07:12,200 --> 00:07:18,880
That's a good question, because no, in many cases you're not, you might go hungry for

77
00:07:18,880 --> 00:07:25,760
a day, even two days, I've gone hungry, I had a point where I had no money and for a few

78
00:07:25,760 --> 00:07:35,600
days I really was destitute and that's the word I'm looking for, malnourished, yes,

79
00:07:35,600 --> 00:07:36,600
this is the word.

80
00:07:36,600 --> 00:07:41,920
It's just a few days, I've never been horribly, horribly, I mean I am now, but because

81
00:07:41,920 --> 00:07:49,480
of my work and my position and my ability to set myself in the right place, there are

82
00:07:49,480 --> 00:07:54,080
people who support me and people who always give me food, so I'm not in that, but I have

83
00:07:54,080 --> 00:08:00,560
been and I know what it tastes like, if I don't know the full meal of being destitute,

84
00:08:00,560 --> 00:08:07,400
but I know how it feels, the first onset of it anyway, because it's never lasted very

85
00:08:07,400 --> 00:08:16,280
long, but the worst it can do is kill you, the worst it can do is make you starve to death

86
00:08:16,280 --> 00:08:23,440
or freeze to death or so on, and that sort of thing happens to people who are not destitute,

87
00:08:23,440 --> 00:08:29,840
it can happen to anyone at any time, so that's the truth of life, you might be a very

88
00:08:29,840 --> 00:08:35,960
rich person who has to suffer, a person who gets cancer and dies, even a horrible death,

89
00:08:35,960 --> 00:08:42,760
far worse than the death of someone, not far worse, but theoretically worse than someone

90
00:08:42,760 --> 00:08:51,640
who dies healthy, but malnourished, or I guess it would be from sickness or so on, on

91
00:08:51,640 --> 00:09:02,440
the street, so you take it like that and you see that, well, it's something that can

92
00:09:02,440 --> 00:09:12,440
be dealt with, it's not unlike many other conditions of human existence, and all of which

93
00:09:12,440 --> 00:09:20,740
are very, very, very much the reason, or a very, very important reason for the Buddha

94
00:09:20,740 --> 00:09:27,240
teaching, as teaching what he taught, and for the practice, very important reason for taking

95
00:09:27,240 --> 00:09:36,920
up the practice of the Buddha taught, because these, these states may become reality for

96
00:09:36,920 --> 00:09:49,360
us at any time, or any number of states probably will come to us, states of loss, states

97
00:09:49,360 --> 00:09:56,760
of inability to obtain the things that we want and so on, who come to us at any time

98
00:09:56,760 --> 00:10:00,680
and do come to us and cause great suffering, you know, people who are really well off,

99
00:10:00,680 --> 00:10:06,840
but then they fall in love and it's unrequited, you know, end up killing themselves just

100
00:10:06,840 --> 00:10:14,520
for that, so, and experience horrible suffering, they can experience thousands of times

101
00:10:14,520 --> 00:10:19,880
worse suffering than a person who's living in poverty, who has, who has come to accept

102
00:10:19,880 --> 00:10:27,720
and, and become trouble with it, or not comfortable, but be at peace with it, a person

103
00:10:27,720 --> 00:10:35,200
who is rich, but has some silly infatuation that is unrequited, or loses their job, or

104
00:10:35,200 --> 00:10:45,400
is embarrassed, or so on and so on and so on. The reasons why we suffer have very little

105
00:10:45,400 --> 00:10:53,720
to do, have much less any way to do with our external situation than they do with our internal

106
00:10:53,720 --> 00:11:02,960
reactions to the situation, they should write that down. So they know, they gotta remember

107
00:11:02,960 --> 00:11:10,360
these things, because then they can make it, they can write it to people and so on.

108
00:11:10,360 --> 00:11:16,840
So you're really not in a bad way, externally, you're in more of a bad way, it sounds

109
00:11:16,840 --> 00:11:20,800
like internally, go for it, Larry.

110
00:11:20,800 --> 00:11:28,640
If I'm not interject something, I know at the age of 18 or 19, less than 20 years

111
00:11:28,640 --> 00:11:37,240
old, understand that you can become perplexed, you can become overwrought with situations

112
00:11:37,240 --> 00:11:43,600
you may feel that there's a dire need to take another direction in being unable to do

113
00:11:43,600 --> 00:11:53,360
so. But at my age, looking back, that was many decades ago, at age 18 or 19, you're

114
00:11:53,360 --> 00:12:00,960
in your, as they say, your prime of life, life is just beginning for you. In my estimation,

115
00:12:00,960 --> 00:12:06,960
it's not a time to lie down the ground, it's not a time to quit, it's not a time to feel

116
00:12:06,960 --> 00:12:17,400
defeated, because you're not defeated, you're just on the very cutting edge of starting

117
00:12:17,400 --> 00:12:27,480
the life. So my advice, if you call it advisor, my outlook on that is don't get about staying

118
00:12:27,480 --> 00:12:35,880
there in front for it. And I think, you know, I think that things will turn there, and

119
00:12:35,880 --> 00:12:43,680
if you persevere, that things will get better. I mean, it's just inevitable that it will.

120
00:12:43,680 --> 00:12:49,840
I think, I've always heard, I think cultures used to say it's always easier to give up

121
00:12:49,840 --> 00:12:57,120
than it is to keep fighting. So, you know, you've got to keep fighting the fight.

122
00:12:57,120 --> 00:13:13,520
Yes, 19 is certainly that. I was thinking, I had this kind of irrational desire to write

123
00:13:13,520 --> 00:13:23,280
a chronicle of my teenage years. It's just so horrible. Looking back and I thought,

124
00:13:23,280 --> 00:13:28,960
wow, that would be wonderful to just put it out there and how embarrassing to have to show

125
00:13:28,960 --> 00:13:38,880
it. But if only we could all do that and put it all out there and really see what life is

126
00:13:38,880 --> 00:13:44,560
really like. I don't know. I only lived until I was 20. You say you died. You stopped growing

127
00:13:44,560 --> 00:13:50,080
at 36. Well, I stopped growing at 20. 20 was when I found Buddhism and I haven't grown since.

128
00:13:50,080 --> 00:13:58,720
I'm still, my life ended at 20. I didn't live after 20. I'm not alive anymore. That's not

129
00:13:58,720 --> 00:14:06,080
exactly true. But Noah, the person who I was, is gone. I'm now just like a pen. This one monk

130
00:14:06,080 --> 00:14:13,200
told me, one monk said it. I'm a tool. I'm something that is used or that is, you know,

131
00:14:13,200 --> 00:14:20,480
that's not entirely true. I practice for myself and cultivate. But my life is much, much more

132
00:14:20,480 --> 00:14:31,040
not entirely, but much more as a, you know, like a part of the, this tradition that I'm in

133
00:14:31,040 --> 00:14:37,520
to spread and to help and to and so on. So, point being, the only experiences I have

134
00:14:37,520 --> 00:14:44,560
were up until 19 years old, 20 years old. And 19 was a horrible time for other reasons. And

135
00:14:44,560 --> 00:14:48,480
what I really wanted to write down was all my romantic involvement because it's so funny.

136
00:14:48,480 --> 00:14:58,960
If you, if you, if you, it's as ridiculous. But, but so, well, no, I mean, no, really my,

137
00:14:58,960 --> 00:15:08,800
anyway, whatever. I mean, it's all I had. I never had a more serious relationship. So, for me,

138
00:15:08,800 --> 00:15:14,720
that was what love was when I was, when I was 19, I went through a ridiculous experience or

139
00:15:14,720 --> 00:15:21,040
several ridiculous experiences that just mean me say, no, I've told this before. I think I was in love

140
00:15:21,040 --> 00:15:30,800
with, with the woman and then suddenly I was in love with her best friend or kind of. It was

141
00:15:30,800 --> 00:15:34,320
weird. Suddenly I was together with her best friend, but still in love with the other one. And

142
00:15:34,320 --> 00:15:40,000
so I didn't know which one to choose, called up, called up the first one and got her roommate

143
00:15:40,000 --> 00:15:46,560
and started flirting with her roommate. And, and at that point, I just said, this is, this is absurd.

144
00:15:46,560 --> 00:15:58,880
Well, it was absurd. So, and it, you know, it, it made me realize this isn't certainly something

145
00:15:58,880 --> 00:16:07,040
that you could call love in the sense that I understood it even then. This is a fire. This is my

146
00:16:07,040 --> 00:16:13,280
mind going crazy. And, you know, seeking out central pleasure. That's really all it was. And it

147
00:16:13,280 --> 00:16:20,400
just, your hormones kicking. Yeah. So it's, it's discussed, it's disgusting. And so

148
00:16:23,120 --> 00:16:27,200
the only point I had with that was that it was a horrible time. And

149
00:16:29,040 --> 00:16:35,520
all the years leading up to 19 were horrible time. 20 was actually, even before I got into

150
00:16:35,520 --> 00:16:41,120
Buddhism, when I dropped out of university, that's what did it for me. You know, suddenly

151
00:16:41,120 --> 00:16:46,880
life began. I didn't know what I was going to do. And I even when I went to Thailand for the

152
00:16:46,880 --> 00:16:52,000
first time, what did I do? I sat around watching the Godfather movies in my, in my, in Thailand,

153
00:16:52,000 --> 00:16:56,400
in Bangkok. That's what I did in Bangkok. I didn't know what I was doing. I didn't know why I was

154
00:16:56,400 --> 00:17:00,640
there. I didn't know what I was looking for. Bought a couple of Zen books because that's what I was

155
00:17:00,640 --> 00:17:09,040
looking for, tried to meditate failed miserably. But, but it changed. It was, it was different at

156
00:17:09,040 --> 00:17:17,120
that point. So certainly I agree that in your specific situation, it's a horrible coming out,

157
00:17:17,120 --> 00:17:22,800
coming out of puberty. So in a puberty, yeah, puberty, isn't it? And that whole experience up until

158
00:17:22,800 --> 00:17:28,560
your, I don't know, I guess, 17, 18. But for me, it was all the way to 20 when I dropped out

159
00:17:28,560 --> 00:17:38,800
of the university and, and started my life. Well, talking about writing your life stories, of

160
00:17:38,800 --> 00:17:48,880
course, at my age, either, um, fortunately, or unfortunately, I was born at a time, of course,

161
00:17:48,880 --> 00:17:54,720
I was in a rural area, a farming area. I was like seven or eight years old before we had

162
00:17:54,720 --> 00:18:01,120
electricity. Well, so I mean, I definitely remember doing my homework in the evenings at the

163
00:18:01,120 --> 00:18:08,800
kitchen table with a care saying, wow, on, on the table, we had an old battery radio in the

164
00:18:08,800 --> 00:18:14,160
batteries stay down all the time. We've ordered about once a year, we'd have enough money to

165
00:18:14,160 --> 00:18:22,320
order new batteries from Sierra and Roeba for the radio. And we had a cook, a wood and cook stove

166
00:18:22,320 --> 00:18:28,240
that we couldn't cook, cooked our meals on. We had a heating stove in the bedroom that we

167
00:18:29,520 --> 00:18:37,120
burned wood. And, uh, I mean, that, that's the, the lights that I came up with, an outhouse,

168
00:18:37,120 --> 00:18:46,400
no, no, we had a water bucket, we went to our water, and a wash pan. I mean, that was,

169
00:18:46,400 --> 00:18:53,440
that was on. That was a rule, a rule of life. And I don't, I don't want to give those memories,

170
00:18:53,440 --> 00:18:59,120
uh, because I think it helps put me in a perspective as to where I am now. Like I said,

171
00:18:59,120 --> 00:19:03,920
I was about, I think, close to eight years old before we got electricity. And of course, we

172
00:19:03,920 --> 00:19:10,640
didn't have an ice box. And I can remember putting milk in the water bucket and letting it down

173
00:19:10,640 --> 00:19:17,920
in the well, keep it cool in the water, because we didn't have an ice box. Um, we had a 10,

174
00:19:17,920 --> 00:19:23,760
a number 10 wash tub that we kept in the basement. And the ice man came once a week,

175
00:19:24,480 --> 00:19:29,680
and we'd get a big chunk of ice sometimes and put that in the wash tub and cover it over with

176
00:19:29,680 --> 00:19:37,600
quits, and there were no chips at all. We said ice, but, and the only ice that we had, um, I mean,

177
00:19:37,600 --> 00:19:45,840
that was, and that was life for me for the first eight years of my life. And, um, so I,

178
00:19:45,840 --> 00:19:53,120
I wouldn't take anything for there, but no things got better. And, yes. And, um,

179
00:19:54,560 --> 00:20:02,160
but I look around my grand son today and all the, all the gadgets that kids house

180
00:20:02,160 --> 00:20:08,720
in today's society, even myself, the gadgets that I have as well. And I think we've come

181
00:20:08,720 --> 00:20:17,040
such a long ways to black and white, and 12 inch TV that I've got one channel. And you watched

182
00:20:17,040 --> 00:20:27,120
what was all you'd have watched. Yeah. So, uh, yeah, one thing someone said is it's very much

183
00:20:27,120 --> 00:20:34,800
your, your, your accustomed to that, that, uh, determines your suffering. So people who

184
00:20:35,680 --> 00:20:48,880
are accustomed to certain certain state will be hard, hard hit by a change in, in the states that

185
00:20:48,880 --> 00:20:58,480
they're most accustomed to. I mean, I, I explained because even I had the, our house had, uh, had

186
00:20:58,480 --> 00:21:06,320
an indoor outhouse and in-house we had no, no indoor plumbing. We had a wood stove, we had a cook stove,

187
00:21:06,320 --> 00:21:14,080
all that, not so long ago. Um, I don't know where that's, I just thought it was interesting that

188
00:21:14,080 --> 00:21:26,800
I kind of can relate. But, um, yeah, certainly don't, I mean, don't give up, give up, give up your

189
00:21:26,800 --> 00:21:32,720
expectations and start again, start fresh, always start fresh. That's very Zen, they always say

190
00:21:32,720 --> 00:21:38,560
beginner mind. So always start over and that's good. That's true, not just for Zen. It's,

191
00:21:38,560 --> 00:21:43,040
that's, that's a really good thing to say. Meditation should be about the beginner's

192
00:21:43,040 --> 00:21:48,320
mind. Life should be a beginner's mind. Don't continue life, start fresh, start a new life,

193
00:21:48,320 --> 00:22:18,160
every moment. That's very cliche, but I think it's true. Anyway, we're just...

