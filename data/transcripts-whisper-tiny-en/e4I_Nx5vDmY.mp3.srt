1
00:00:00,000 --> 00:00:03,000
Hi, and welcome back to Asma Monk.

2
00:00:03,000 --> 00:00:10,000
The next question comes from Chigoku, he2.0.

3
00:00:10,000 --> 00:00:12,000
I've never had a teacher in meditation.

4
00:00:12,000 --> 00:00:14,000
It's always come naturally to me.

5
00:00:14,000 --> 00:00:18,000
But lately, it's been rather difficult to get to that place inside.

6
00:00:18,000 --> 00:00:22,000
I have severe trust issues since childhood.

7
00:00:22,000 --> 00:00:26,000
I feel lost any tips.

8
00:00:26,000 --> 00:00:31,000
Well, the biggest tip is to get a teacher first him.

9
00:00:31,000 --> 00:00:37,000
But apart from that, it's important to understand what we mean by meditation.

10
00:00:37,000 --> 00:00:43,000
As I've explained many times from the point of view that I teach,

11
00:00:43,000 --> 00:00:49,000
meditation is the contemplation, the examination, the analysis,

12
00:00:49,000 --> 00:00:54,000
or analyzing of reality, not getting to some place.

13
00:00:54,000 --> 00:00:58,000
It sounds like probably you're getting into a state of peace,

14
00:00:58,000 --> 00:01:05,000
calm, tranquility, focus, some special state which I've talked about before

15
00:01:05,000 --> 00:01:11,000
as being actually not special in any real meaningful sense.

16
00:01:11,000 --> 00:01:17,000
And if you cling to that state, then obviously you're not going to be satisfied when you can't attain it.

17
00:01:17,000 --> 00:01:20,000
I would say there's no problem with the fact that you can't attain it.

18
00:01:20,000 --> 00:01:27,000
That's part of nature, the impermanence of everything that even good states have to change.

19
00:01:27,000 --> 00:01:32,000
What we try to realize in meditation is that everything changes, nothing is stable.

20
00:01:32,000 --> 00:01:37,000
So whatever we were clinging to is not worth clinging to.

21
00:01:37,000 --> 00:01:43,000
Also that therefore they're unsatisfying because they're changing all the time.

22
00:01:43,000 --> 00:01:47,000
And that we can't control them, that we can't control anything.

23
00:01:47,000 --> 00:01:51,000
We're inside of ourselves or in the world around us to be other than what it is,

24
00:01:51,000 --> 00:01:55,000
to be constant or permanent or satisfying.

25
00:01:55,000 --> 00:02:02,000
We have no control over the nature of things except to give rise to causes.

26
00:02:02,000 --> 00:02:05,000
And when we give rise to the cause, then there will be the effect.

27
00:02:05,000 --> 00:02:10,000
And as long as we work hard to maintain the cause, there will always be the effect.

28
00:02:10,000 --> 00:02:14,000
As soon as we stop working, it's going to disappear.

29
00:02:14,000 --> 00:02:24,000
As far as trust issues since childhood, I would warn against putting too much emphasis on the past.

30
00:02:24,000 --> 00:02:42,000
I would say that's misleading or it's a misunderstanding among the western world that somehow we have to address issues that arose in the past, especially when we were kids that were repressing and so on.

31
00:02:42,000 --> 00:02:44,000
We have to somehow drag these up.

32
00:02:44,000 --> 00:02:47,000
I would say that's not correct, that's not proper.

33
00:02:47,000 --> 00:02:51,000
The important thing is to deal with what's arising here and now.

34
00:02:51,000 --> 00:02:54,000
What is it that's coming up in our mind?

35
00:02:54,000 --> 00:02:59,000
Whatever it's a result of, that's not really, that's not important at all.

36
00:02:59,000 --> 00:03:07,000
Because in the end that becomes just conjecture and theory and this attachment or identification.

37
00:03:07,000 --> 00:03:15,000
This is my childhood, I am like this, I have this problem and so on and so on.

38
00:03:15,000 --> 00:03:19,000
The truth is that there is something arising right here and now.

39
00:03:19,000 --> 00:03:27,000
And if it's a state of mistrust or fear or worry or anxiety or stress or so on, then that's all it is.

40
00:03:27,000 --> 00:03:30,000
And the important thing is to see that for what it is.

41
00:03:30,000 --> 00:03:40,000
So there's some tips for you, I hope that helps.

