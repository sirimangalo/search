1
00:00:00,000 --> 00:00:06,000
When you are focusing on your breath, should you keep a peripheral awareness to your passing thoughts?

2
00:00:06,000 --> 00:00:10,000
Because I found that if I'm deeply aware of the breath or another object,

3
00:00:10,000 --> 00:00:14,000
I just noticed a thought after it has passed and not in the moment.

4
00:00:19,000 --> 00:00:21,000
Well, no, you shouldn't.

5
00:00:21,000 --> 00:00:24,000
The problem isn't that you're too focused on the breath.

6
00:00:24,000 --> 00:00:28,000
The problem is that your mind's still...

7
00:00:28,000 --> 00:00:33,000
Well, it's not even a problem, but it's not the ideal mind state.

8
00:00:33,000 --> 00:00:39,000
Of course, the ideal mind state is freedom from thought, even freedom from breath.

9
00:00:39,000 --> 00:00:46,000
The ideal mind state is extinguishing when there is release or freedom.

10
00:00:46,000 --> 00:01:06,000
But the problem that you have is that your lack of mindfulness is not allowing you to catch the thought when it starts.

11
00:01:06,000 --> 00:01:10,000
It has nothing to do with being tubes or in the breath.

12
00:01:10,000 --> 00:01:15,000
When you're watching the breath, if you're mindful, if you're clearly aware,

13
00:01:15,000 --> 00:01:18,000
you should be able to see as soon as the thought arises.

14
00:01:18,000 --> 00:01:23,000
And as soon as the thought arises, you'll be able to acknowledge it as thinking.

15
00:01:23,000 --> 00:01:27,000
It's true that you can't actually be aware of an object as it's happening.

16
00:01:27,000 --> 00:01:31,000
You can't actually be mindful of an object as it's happening.

17
00:01:31,000 --> 00:01:35,000
The object arises and there's the awareness, and then there's the mindfulness.

18
00:01:35,000 --> 00:01:41,000
So you're actually technically not mindful of something at the moment that it occurs,

19
00:01:41,000 --> 00:01:44,000
but it's so quick, it's thought moment.

20
00:01:44,000 --> 00:01:48,000
One thought moment in the next thought moment.

21
00:01:48,000 --> 00:01:56,000
But the fact that you can't catch it until after it's over is a sign that during that thought,

22
00:01:56,000 --> 00:01:57,000
there was lack of mindfulness.

23
00:01:57,000 --> 00:02:05,000
Each thought moment until you caught it was a mind with delusion in it.

24
00:02:05,000 --> 00:02:13,000
So that being the case, I would suggest that your attention on the breath was void of wisdom.

25
00:02:13,000 --> 00:02:16,000
And void of mindfulness.

26
00:02:16,000 --> 00:02:19,000
So perhaps had delusion involved in it.

27
00:02:19,000 --> 00:02:24,000
If there is a deluded mind state, even watching the breath,

28
00:02:24,000 --> 00:02:29,000
there will be conceit, there will be view of self, there will be craving.

29
00:02:29,000 --> 00:02:38,000
As a result, the moment of the thought arising will come without awareness of the thought.

30
00:02:38,000 --> 00:02:40,000
And that's how you get carried away.

31
00:02:40,000 --> 00:02:49,000
Practically speaking, a beginner will only catch the thoughts at the end after you've thought through the whole subject.

32
00:02:49,000 --> 00:02:56,000
A advanced meditator, as they progress, they'll be able to catch the thoughts earlier and earlier and earlier.

33
00:02:56,000 --> 00:03:02,000
And a meditator who is truly in the zone will be able to catch the thoughts as soon as they arise.

34
00:03:02,000 --> 00:03:08,000
So the thought arises in the mind, the meditators aware that a road is just one thought subject,

35
00:03:08,000 --> 00:03:14,000
and then it ceases, and the meditator comes back to the main object.

36
00:03:14,000 --> 00:03:23,000
So no, you shouldn't keep tabs on something else while you're focusing on a main object.

37
00:03:23,000 --> 00:03:26,000
And no, it isn't caused by the breath.

38
00:03:26,000 --> 00:03:36,000
It's caused by delusion, which may be indicative of improper observation of the breath.

39
00:03:36,000 --> 00:03:44,000
So starting out from a deluded state, which carries on into the thought, allows the thought towards the sneak in there.

