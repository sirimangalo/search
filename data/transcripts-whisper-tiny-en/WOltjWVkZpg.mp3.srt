1
00:00:00,000 --> 00:00:06,000
The Buddha seemed to have led an active life, traveling, meetings, teachings, etc.

2
00:00:06,000 --> 00:00:11,000
Is this inconsistent with the Buddha's idea of letting go of busyness and activity?

3
00:00:17,000 --> 00:00:24,000
No, I mean, I don't know that there is a Buddhist idea of letting go of business and activity.

4
00:00:24,000 --> 00:00:31,000
Buddhism is the letting go of clinging and letting go of suffering.

5
00:00:31,000 --> 00:00:41,000
So, if you need or want to teach, travel, meet, etc., then this is going to be a cause for suffering for you.

6
00:00:41,000 --> 00:00:46,000
But not because of the meeting, traveling, and teaching, because of the wanting.

7
00:00:46,000 --> 00:00:51,000
Because the wanting is going to cause you to create stress in your own being.

8
00:00:51,000 --> 00:00:57,000
And it's going to have you create some kara's formations that you'll then have to deal with.

9
00:00:57,000 --> 00:01:00,000
And the consequences and the repercussions.

10
00:01:00,000 --> 00:01:01,000
You will build something.

11
00:01:01,000 --> 00:01:07,000
So, once you become a teacher, if you say, I want to be a teacher and then you go on your teaching, you will suddenly have got students.

12
00:01:07,000 --> 00:01:09,000
And then you've got to deal with the students.

13
00:01:09,000 --> 00:01:15,000
So, if you want the good side, you'll have to have a bad side.

14
00:01:15,000 --> 00:01:17,000
That's the problem.

15
00:01:17,000 --> 00:01:30,000
But when you have let go of all this, the active life is still there because you're still living organism.

16
00:01:30,000 --> 00:01:34,000
A tree doesn't stop growing just because it doesn't have a mind and grows naturally.

17
00:01:34,000 --> 00:01:44,000
A human being travels and meets and talks and so on as a part of the developed habits.

18
00:01:44,000 --> 00:01:50,000
And these still continue going to the washroom, meeting and eating food.

19
00:01:50,000 --> 00:01:59,000
And if there's the opportunity to teach, for example, in the Buddhist case where he was invited to teach, then one does it naturally.

20
00:01:59,000 --> 00:02:08,000
But the Buddhism is all about letting go of our attachment to these things.

21
00:02:08,000 --> 00:02:17,000
And so, not the doing themselves, but because he was so wonderful, was invited.

22
00:02:17,000 --> 00:02:28,000
Quite often to teach and was asked to go here and go there and was a result, quite busy.

23
00:02:28,000 --> 00:02:39,000
But without any desire to be busy.

