1
00:00:00,000 --> 00:00:07,000
Okay, that the M would be

2
00:00:07,000 --> 00:00:30,000
Alright, so I think

3
00:00:37,000 --> 00:00:44,000
Let's see if this works.

4
00:00:59,000 --> 00:01:06,000
Anyway, we have a full house here in Hamilton.

5
00:01:06,000 --> 00:01:12,000
Good morning to our local meditators.

6
00:01:12,000 --> 00:01:19,000
I would like to always express my appreciation for the work that you do in our dungeon.

7
00:01:19,000 --> 00:01:41,000
We have four rooms in the basement and we have four meditators here.

8
00:01:41,000 --> 00:01:56,000
So what I wanted to talk about tonight was

9
00:01:56,000 --> 00:02:19,000
I'll answer a question that comes up often and that is how do you prepare for a meditation course?

10
00:02:19,000 --> 00:02:34,000
Right, we get this often where meditators want to come to meditate or they want to go somewhere to meditate and they wonder how do you prepare yourself?

11
00:02:34,000 --> 00:02:42,000
It's a very old question. It's a question that they were asking in the Buddhist time as well.

12
00:02:42,000 --> 00:03:08,000
It's a question that's answered quite well in the commentaries to the polycanon and as a result it's something that has been incorporated into the ceremony for new meditators to prepare them psychologically for their endeavor.

13
00:03:08,000 --> 00:03:15,000
But I think if you look at the format and if you look at the commentaries,

14
00:03:15,000 --> 00:03:22,000
look at the format of the opening ceremonies, I think perhaps even more useful.

15
00:03:22,000 --> 00:03:33,000
I know it's more useful if you're aware of these things in advance because the opening ceremony that we do in Thailand is inspiring.

16
00:03:33,000 --> 00:03:40,000
But it's quite spur of the moment or it's unprepared.

17
00:03:40,000 --> 00:04:01,000
So the idea behind these, the idea behind the opening ceremony is a list of ways one should prepare oneself for a meditation course.

18
00:04:01,000 --> 00:04:08,000
So quite useful, something to think about.

19
00:04:08,000 --> 00:04:18,000
So then I go through that tonight and I think I've gone through it before.

20
00:04:18,000 --> 00:04:27,000
The first thing we did to prepare ourselves should do or it's encouraged that we do.

21
00:04:27,000 --> 00:04:42,000
To prepare ourselves for meditation course is to put ourselves in the hands of the Buddha and our teacher to surrender ourselves.

22
00:04:42,000 --> 00:04:45,000
This is a religious thing.

23
00:04:45,000 --> 00:04:58,000
This is why I'm going to talk a lot about surrender to God, but you have to surrender yourself to the Buddha, surrender yourself to your teacher.

24
00:04:58,000 --> 00:05:05,000
In some ways it's an acknowledgement of the power our teacher has to some extent over their student.

25
00:05:05,000 --> 00:05:21,000
To the extent that it can be accomplished and the meditation becomes quite a bit more effective.

26
00:05:21,000 --> 00:05:42,000
Because if for whatever reason you're unable to surrender yourself to your teacher and to the Buddha, it will constantly be doubting and questioning what you're doing.

27
00:05:42,000 --> 00:05:57,000
And maybe that your teacher's not worth surrendering yourself to maybe you have no way of knowing whether your teacher is someone who's teaching will protect you and will lead you in a good direction.

28
00:05:57,000 --> 00:06:07,000
But if you such that if you surrender yourself to them and their teachings, you can be assured of beneficial outcome.

29
00:06:07,000 --> 00:06:18,000
So it's not to say that one should just blindly throw oneself at whatever teacher one comes across.

30
00:06:18,000 --> 00:06:31,000
What it means really is that you should wait to find a teacher who you really feel like you can dedicate yourself to, at least for the time of the course.

31
00:06:31,000 --> 00:06:41,000
So you have enough confidence and this is what this really says is that you shouldn't just sign up on the internet for a meditation course. You should get to know the center.

32
00:06:41,000 --> 00:06:51,000
One of my students here and I'm if he's listening I apologize he asked me about this he wants me to talk to his parents and because he wants to go to Thailand.

33
00:06:51,000 --> 00:06:59,000
And he wants me to reassure his parents that we pass in to meditation is not dangerous.

34
00:06:59,000 --> 00:07:07,000
I haven't replied yet but my answer is going to be we pass in a meditation can be very or meditation can be very dangerous.

35
00:07:07,000 --> 00:07:16,000
Meditation is dealing with your psyche it's the very most powerful thing that there is.

36
00:07:16,000 --> 00:07:25,000
And so there's really no way that I can guarantee that if you go to this meditation center or that meditation center.

37
00:07:25,000 --> 00:07:34,000
That you're not going to have difficulties with it and it depends also very much on you and your state of mind.

38
00:07:34,000 --> 00:07:51,000
Many people come to meditation centers with problems but I've seen and experienced and heard second hand of many situations where meditators were manipulated by their teachers or misled by their teachers or.

39
00:07:51,000 --> 00:08:07,000
Even with the best of intentions were misdirected or misguided by a teacher who was less than 100% incompetent.

40
00:08:07,000 --> 00:08:18,000
So it's important to have that sense of reassurance yes the teacher knows what they're talking about and there are various ways you can get their credentials where did they learn to teach what if they

41
00:08:18,000 --> 00:08:26,000
get done to get where they are you can of course and in this day and age you can have access to their teachings.

42
00:08:26,000 --> 00:08:33,000
I mean sometimes that means going to their center sometimes it means going on YouTube.

43
00:08:33,000 --> 00:08:45,000
You can find out what other people are saying about the technique in the center we're very interested in getting feedback from our students for our center for this reason because.

44
00:08:45,000 --> 00:08:55,000
We want to provide people with an honest sense of what they can expect going into a meditation center course in our center.

45
00:08:55,000 --> 00:08:58,000
So these are the various ways but.

46
00:08:58,000 --> 00:09:06,000
It's important to be clear about that you can't this isn't something you can take off hand and sort of sit back and.

47
00:09:06,000 --> 00:09:14,000
And maintain your skepticism it's something you really should be sure about going into.

48
00:09:14,000 --> 00:09:22,000
At least moderate to the extent that you can be sure.

49
00:09:22,000 --> 00:09:28,000
To that extent you it's going to be successful.

50
00:09:28,000 --> 00:09:36,000
If you're completely unsure of no clue whether this is going to benefit you at all it makes it more difficult.

51
00:09:36,000 --> 00:09:43,000
Of course during the course there are ways of reassuring students so it's not to say that you need.

52
00:09:43,000 --> 00:09:46,000
But it's a great preparation.

53
00:09:46,000 --> 00:09:51,000
To commit yourself and say for this time I'm going to.

54
00:09:51,000 --> 00:09:56,000
Dedicate myself to the Buddha and sort of.

55
00:09:56,000 --> 00:09:59,000
The word the Thai word is mop which means.

56
00:09:59,000 --> 00:10:02,000
To hand over yourself.

57
00:10:02,000 --> 00:10:04,000
Put yourself in.

58
00:10:04,000 --> 00:10:14,000
Mahasi Sayyadha says something about this as well he says.

59
00:10:14,000 --> 00:10:15,000
It protects you.

60
00:10:15,000 --> 00:10:19,000
Because suddenly the teacher is responsible.

61
00:10:19,000 --> 00:10:30,000
You're not you're you're no longer.

62
00:10:30,000 --> 00:10:34,000
You're no longer responsible for yourself.

63
00:10:34,000 --> 00:10:37,000
You now have someone who's going to guide you.

64
00:10:37,000 --> 00:10:40,000
You're now under the protection.

65
00:10:40,000 --> 00:10:46,000
And if this person is reliable then you have a great power and a great support.

66
00:10:46,000 --> 00:10:48,000
You don't have to go it alone.

67
00:10:48,000 --> 00:11:01,000
So that when you meet with as he says when you meet with frightening or upsetting experiences.

68
00:11:01,000 --> 00:11:12,000
You know that you have the support you've placed yourself in the under the protection of the Buddha.

69
00:11:12,000 --> 00:11:19,000
I think it says that when you do this goes to leave you alone.

70
00:11:19,000 --> 00:11:22,000
I mean it's a big thing in Thailand and Asia in general.

71
00:11:22,000 --> 00:11:30,000
Once you've placed yourself in the protection of the Buddha and the Dhamma and the Sangha.

72
00:11:30,000 --> 00:11:34,000
Non-human beings goes demons and so on will leave you alone.

73
00:11:34,000 --> 00:11:41,000
If they try to harm you their head will split into seven pieces he says.

74
00:11:41,000 --> 00:11:43,000
So that's the first thing.

75
00:11:43,000 --> 00:11:45,000
The first thing we do is we say,

76
00:11:45,000 --> 00:11:48,000
Ima hang bhaggo,

77
00:11:48,000 --> 00:11:51,000
Ata bhavang tum mha kang paritachami.

78
00:11:51,000 --> 00:11:54,000
O venerable blessed one.

79
00:11:54,000 --> 00:11:58,000
I hereby relinquish myself.

80
00:11:58,000 --> 00:12:00,000
Turn myself over.

81
00:12:00,000 --> 00:12:04,000
Turn this being over to you.

82
00:12:04,000 --> 00:12:12,000
And then Ima hang hacharya ata bhavang tum mha kang paritachami.

83
00:12:12,000 --> 00:12:14,000
O venerable teacher.

84
00:12:14,000 --> 00:12:23,000
I hereby hand over this ata bhava in itself to you.

85
00:12:23,000 --> 00:12:27,000
That's the first thing.

86
00:12:27,000 --> 00:12:31,000
In the opening ceremony the second thing we do is actually ask the teacher

87
00:12:31,000 --> 00:12:35,000
and make it clear what our intentions are.

88
00:12:35,000 --> 00:12:39,000
So we say,

89
00:12:39,000 --> 00:12:45,000
Nibanasa mai bhante sajikar nata ya kamata nante.

90
00:12:45,000 --> 00:12:49,000
Please, venerable teacher,

91
00:12:49,000 --> 00:12:59,000
give me a still upon me the meditation teaching for the purpose of realizing Nibana.

92
00:12:59,000 --> 00:13:01,000
So we make clear our intentions.

93
00:13:01,000 --> 00:13:02,000
I've talked about this before.

94
00:13:02,000 --> 00:13:05,000
I've gone through these before I think.

95
00:13:05,000 --> 00:13:08,000
Somewhere on YouTube.

96
00:13:08,000 --> 00:13:12,000
So we're not just practicing as a hambi or we're not just practicing

97
00:13:12,000 --> 00:13:14,000
for some kind of superficial stress relief.

98
00:13:14,000 --> 00:13:18,000
We're actually trying to free ourselves from suffering.

99
00:13:18,000 --> 00:13:23,000
This isn't just a vacation or a fun time.

100
00:13:23,000 --> 00:13:25,000
It's not just a retreat.

101
00:13:25,000 --> 00:13:32,000
This is a full on assault to tear down the walls

102
00:13:32,000 --> 00:13:36,000
and free ourselves from suffering.

103
00:13:36,000 --> 00:13:38,000
And attain Nibana.

104
00:13:38,000 --> 00:13:43,000
And attain perfect freedom from suffering.

105
00:13:43,000 --> 00:13:46,000
Another thing about this is that you have to ask.

106
00:13:46,000 --> 00:13:51,000
Often, from time to time, I'll get these.

107
00:13:51,000 --> 00:13:56,000
I'll find myself in these situations where people expect me

108
00:13:56,000 --> 00:14:05,000
to lead them to want somehow to lead them to meditation.

109
00:14:05,000 --> 00:14:07,000
Even just today, someone called me.

110
00:14:07,000 --> 00:14:11,000
And I think it was just the wording, but it was like,

111
00:14:11,000 --> 00:14:17,000
so we're coming to see if you want to teach us how to do walking meditation.

112
00:14:17,000 --> 00:14:21,000
And well, okay, it's just the wording, but let's be clear.

113
00:14:21,000 --> 00:14:29,000
It's important that we're clear because if people expect that the teacher wants to teach,

114
00:14:29,000 --> 00:14:35,000
then what kind of initiative do they have coming into it?

115
00:14:35,000 --> 00:14:39,000
If I went around teaching people on the side of the road or that I met,

116
00:14:39,000 --> 00:14:43,000
hey, sit down, I'm going to teach you how to meditate.

117
00:14:43,000 --> 00:14:49,000
What kind of initiative would they have?

118
00:14:49,000 --> 00:14:53,000
How sincere would their effort be?

119
00:14:53,000 --> 00:14:59,000
It's very important that we never, ever, ever push the teaching,

120
00:14:59,000 --> 00:15:03,000
push the meditation and practice on other people.

121
00:15:03,000 --> 00:15:08,000
It's very important that a person come to it with a sincere intention.

122
00:15:08,000 --> 00:15:11,000
And so it's an important part of the opening ceremony.

123
00:15:11,000 --> 00:15:15,000
It's important to be clear that if you want to learn how to meditate,

124
00:15:15,000 --> 00:15:19,000
it's huge that has to do the asking and the requesting and the chasing.

125
00:15:19,000 --> 00:15:23,000
I have to tell my students, I'm not following you.

126
00:15:23,000 --> 00:15:27,000
If you want to follow me, I don't follow my students.

127
00:15:27,000 --> 00:15:31,000
You come to me once you leave.

128
00:15:31,000 --> 00:15:33,000
Up to you if you come back.

129
00:15:33,000 --> 00:15:35,000
Some people I never see them again.

130
00:15:35,000 --> 00:15:37,000
I'm not sure if that's a good sign or not.

131
00:15:37,000 --> 00:15:45,000
How's the new YouTube live stream going?

132
00:15:45,000 --> 00:15:53,000
Let me do it on right there.

133
00:15:53,000 --> 00:15:57,000
A little bit of chatting going on on YouTube.

134
00:15:57,000 --> 00:15:59,000
That's why I turned it off before I thought,

135
00:15:59,000 --> 00:16:02,000
well, you can't chat while I'm talking, but it's okay.

136
00:16:02,000 --> 00:16:15,000
I don't mind.

137
00:16:15,000 --> 00:16:17,000
Sorry, Dara, I have the audio turned down.

138
00:16:17,000 --> 00:16:21,000
I can't hear what you're saying.

139
00:16:21,000 --> 00:16:25,000
I have to type it.

140
00:16:25,000 --> 00:16:29,000
Next thing is we have a couple of reflections.

141
00:16:29,000 --> 00:16:32,000
These are based on these supportive meditations.

142
00:16:32,000 --> 00:16:35,000
It's important that, as we practice insight,

143
00:16:35,000 --> 00:16:38,000
that we're also aware of at least two things.

144
00:16:38,000 --> 00:16:41,000
First, the inevitability and the

145
00:16:41,000 --> 00:16:50,000
the imminence of death, the inevitability and the imminence of death.

146
00:16:50,000 --> 00:16:53,000
That death is inevitable.

147
00:16:53,000 --> 00:16:57,000
That life is uncertain.

148
00:16:57,000 --> 00:16:59,000
So if we say,

149
00:16:59,000 --> 00:17:00,000
do one,

150
00:17:00,000 --> 00:17:06,000
do one more now.

151
00:17:06,000 --> 00:17:09,000
Do one more now.

152
00:17:09,000 --> 00:17:14,000
Do one more now.

153
00:17:14,000 --> 00:17:18,000
Life is unstable.

154
00:17:18,000 --> 00:17:22,000
Life is unsure.

155
00:17:22,000 --> 00:17:25,000
Death is sure.

156
00:17:25,000 --> 00:17:27,000
Life is uncertain.

157
00:17:27,000 --> 00:17:29,000
Death is certain.

158
00:17:29,000 --> 00:17:31,000
My death.

159
00:17:31,000 --> 00:17:35,000
My life has death as its end.

160
00:17:35,000 --> 00:17:38,000
And then entire we say,

161
00:17:38,000 --> 00:17:42,000
it's good luck that I was born in a time and a place.

162
00:17:42,000 --> 00:17:48,000
And then I've taken the opportunity to practice the Buddha's teaching.

163
00:17:48,000 --> 00:17:52,000
I haven't wasted this life as a human being.

164
00:17:52,000 --> 00:17:57,000
So this helps give us the encouragement,

165
00:17:57,000 --> 00:18:07,000
remind us that this body is just a worn out cart that's falling apart.

166
00:18:07,000 --> 00:18:13,000
Getting sick and wearing down.

167
00:18:13,000 --> 00:18:17,000
Till eventually it just falls apart.

168
00:18:17,000 --> 00:18:22,000
And if we're not ready for that,

169
00:18:22,000 --> 00:18:27,000
we caught quite off guard when we pass away.

170
00:18:27,000 --> 00:18:31,000
The second reflection is a mint reflection.

171
00:18:31,000 --> 00:18:34,000
We wish for all beings to be happy.

172
00:18:34,000 --> 00:18:42,000
This is a good way of overcoming any kind of bad blood we have with anyone else.

173
00:18:42,000 --> 00:18:48,000
Any hang-ups we might have towards our family, towards our loved ones,

174
00:18:48,000 --> 00:18:53,000
towards friends, enemies, whoever.

175
00:18:53,000 --> 00:18:57,000
So we say this traditional sabbais, sattas, tukita.

176
00:18:57,000 --> 00:18:58,000
No, no, no.

177
00:18:58,000 --> 00:18:59,000
We start off with ourselves.

178
00:18:59,000 --> 00:19:00,000
May I be happy.

179
00:19:00,000 --> 00:19:01,000
Ahang, tukita, homi.

180
00:19:01,000 --> 00:19:04,000
May I be happy.

181
00:19:04,000 --> 00:19:06,000
May I be free from suffering.

182
00:19:06,000 --> 00:19:07,000
May I find peace.

183
00:19:07,000 --> 00:19:09,000
May I be free from enmity and so on.

184
00:19:09,000 --> 00:19:12,000
But the real meta comes from we wish it to others.

185
00:19:12,000 --> 00:19:14,000
Some of the sattas, tukita, homi.

186
00:19:14,000 --> 00:19:16,000
And you can develop this.

187
00:19:16,000 --> 00:19:20,000
If you know anything about meta, it can be first to the people in this room,

188
00:19:20,000 --> 00:19:24,000
and the people in this building, and in this city,

189
00:19:24,000 --> 00:19:27,000
any of this country, and then in the world, all beings.

190
00:19:27,000 --> 00:19:29,000
You can do it by groups.

191
00:19:29,000 --> 00:19:31,000
You can do it by directions.

192
00:19:31,000 --> 00:19:33,000
You can start with people who you love,

193
00:19:33,000 --> 00:19:35,000
and people who you're neutral towards,

194
00:19:35,000 --> 00:19:37,000
and then people who you dislike.

195
00:19:37,000 --> 00:19:41,000
Many different ways.

196
00:19:41,000 --> 00:19:45,000
But meta is very good for clearing the air.

197
00:19:45,000 --> 00:19:47,000
Be clear about our intentions.

198
00:19:47,000 --> 00:19:51,000
No, I don't have bad intentions towards anyone.

199
00:19:51,000 --> 00:19:54,000
May this all be let go.

200
00:19:54,000 --> 00:20:03,000
May we no longer seek vengeance against each other.

201
00:20:03,000 --> 00:20:07,000
So the voice call hyperpitched again.

202
00:20:07,000 --> 00:20:10,000
Oh, that's too bad.

203
00:20:10,000 --> 00:20:12,000
See if I can fix that.

204
00:20:12,000 --> 00:20:22,000
I have to drop it here.

205
00:20:22,000 --> 00:20:24,000
Hello, hello.

206
00:20:24,000 --> 00:20:26,000
I have to go out of this.

207
00:20:26,000 --> 00:20:41,000
Thank you.

208
00:20:41,000 --> 00:20:50,000
Thank you.

209
00:20:50,000 --> 00:21:00,000
Hello, hello.

210
00:21:00,000 --> 00:21:17,000
Sound better now?

211
00:21:17,000 --> 00:21:21,000
Okay.

212
00:21:21,000 --> 00:21:32,000
So meta, we have for meta-friendliness towards all beings.

213
00:21:32,000 --> 00:21:34,000
And then a couple more things.

214
00:21:34,000 --> 00:21:40,000
We make an observation of the practice that we're going to undertake.

215
00:21:40,000 --> 00:21:50,000
We'll often make,

216
00:21:50,000 --> 00:21:53,000
memorize the meditation technique.

217
00:21:53,000 --> 00:21:56,000
For example, memorizing the four foundations of mindfulness.

218
00:21:56,000 --> 00:21:59,000
But we make clear what we're going to practice.

219
00:21:59,000 --> 00:22:00,000
So in the ceremony,

220
00:22:00,000 --> 00:22:03,000
we repeat actually it comes from the commentary.

221
00:22:03,000 --> 00:22:09,000
It's a passage that's in an old teravada commentary.

222
00:22:09,000 --> 00:22:33,600
Or we charge a

223
00:22:33,600 --> 00:22:49,720
I think that's just added in there to remind us of the greatness of the practice and

224
00:22:49,720 --> 00:22:56,800
the goal of the practice.

225
00:22:56,800 --> 00:23:02,200
And then finally the last thing we do in the opening ceremonies to pay respect to the

226
00:23:02,200 --> 00:23:05,200
triple gem.

227
00:23:05,200 --> 00:23:13,000
And moreover to make clear that this practice is a form of respect.

228
00:23:13,000 --> 00:23:15,400
The respect is interesting.

229
00:23:15,400 --> 00:23:23,680
I like to talk about this, but I'll just go over the format and the poly.

230
00:23:23,680 --> 00:23:40,680
I pay homage to the triple gem with this practice of dhammanu dhamma, so it's a word that

231
00:23:40,680 --> 00:23:51,120
the Buddha used, dhammanu dhamma, which means dhamma in the dhamma or dhamma of dhammas,

232
00:23:51,120 --> 00:23:58,120
but it's explained to me in practicing the dhamma to realize the dhamma.

233
00:23:58,120 --> 00:24:00,000
I've talked about this before.

234
00:24:00,000 --> 00:24:05,240
The dhamma is the forest at Ipatana to realize the dhamma, but that's vipassana and nibana.

235
00:24:05,240 --> 00:24:09,880
It's the four noble truths.

236
00:24:09,880 --> 00:24:15,560
There's two meanings of the word dhamma.

237
00:24:15,560 --> 00:24:21,040
The first one is the teachings of the Buddha, the techniques, the four foundations of the

238
00:24:21,040 --> 00:24:28,040
mindfulness, that kind of thing.

239
00:24:51,040 --> 00:25:21,000
So the idea with respect is something that it's useful for our own benefit.

240
00:25:21,000 --> 00:25:26,200
I mean, it's not like the Buddha can hear us, it's not like the Buddha is a some god

241
00:25:26,200 --> 00:25:32,400
up in heaven that is going to answer our prayers or is going to be pleased by our respect

242
00:25:32,400 --> 00:25:37,720
and our worship, but respect is a state of mind.

243
00:25:37,720 --> 00:25:41,800
Respect says that this is something that I take seriously.

244
00:25:41,800 --> 00:25:55,960
And this is not something that I am doing in an idol off-hand way.

245
00:25:55,960 --> 00:25:59,640
It's going to be something I dedicate myself to.

246
00:25:59,640 --> 00:26:04,640
And it's something that I see as worthwhile.

247
00:26:04,640 --> 00:26:10,640
Dedication is, well, the word worship is actually quite a good word.

248
00:26:10,640 --> 00:26:16,600
Your worship means you see something as worth, you give something, you express, you're

249
00:26:16,600 --> 00:26:24,520
belief that this thing is worth something.

250
00:26:24,520 --> 00:26:32,240
It has value and the best way to express the value of the dhamma, the value of the

251
00:26:32,240 --> 00:26:41,400
Buddha, the value of the sangha is to practice, is to follow the teachings, is to work

252
00:26:41,400 --> 00:26:51,080
to better yourself, work to obtain the purity and the clarity and the freedom of mind,

253
00:26:51,080 --> 00:27:01,080
so that you to become one of the sangha and continue this powerful and wonderful and

254
00:27:01,080 --> 00:27:07,880
liberating lineage and tradition and practice.

255
00:27:07,880 --> 00:27:15,440
Okay, so I think we're there, I'm getting some error on YouTube that I guess I have to

256
00:27:15,440 --> 00:27:22,600
figure out, but I think we're back up, and we potentially have a live stream going on

257
00:27:22,600 --> 00:27:26,600
YouTube again, which is good.

258
00:27:26,600 --> 00:27:30,320
So that's the dhamma for tonight.

259
00:27:30,320 --> 00:27:36,160
If anyone has any questions about what I just talked about or other questions, I'm happy

260
00:27:36,160 --> 00:27:41,120
to answer them, but I'd like to figure out what's wrong and let me know if you're buffering.

261
00:27:41,120 --> 00:27:44,120
It says that you'd probably be buffering.

262
00:27:44,120 --> 00:28:05,920
If anyone's getting buffering, let me know.

263
00:28:05,920 --> 00:28:22,220
Thank you.

264
00:28:22,220 --> 00:28:27,220
.

265
00:28:27,220 --> 00:28:32,220
.

266
00:28:32,220 --> 00:28:36,220
.

267
00:28:36,220 --> 00:28:40,220
.

268
00:28:40,220 --> 00:28:45,220
..

269
00:28:45,220 --> 00:28:46,220
..

270
00:28:46,220 --> 00:28:49,220
..

271
00:28:49,220 --> 00:28:50,220
..

272
00:28:50,220 --> 00:28:51,220
..

273
00:28:51,220 --> 00:29:08,560
No buffering, huh, okay, well YouTube's I don't know what YouTube's on about

274
00:29:08,560 --> 00:29:15,100
quality isn't the best. Yeah, I don't know how to adjust that YouTube's telling me as well

275
00:29:15,100 --> 00:29:27,280
Streams bit rate is not good. Bit rate should be 400 kbps, which it's not I guess.

276
00:29:27,280 --> 00:29:43,920
So maybe that bit rate's a little better.

277
00:29:57,280 --> 00:30:20,640
Stream health is excellent.

278
00:30:20,640 --> 00:30:29,640
Yeah, I mean, it's live streaming, right? So it's never going to be the best quality. But I can try and tweak it.

279
00:30:29,640 --> 00:30:42,880
Maybe if we had a better internet connection, but see what the internet connection's like.

280
00:30:42,880 --> 00:31:06,880
Sending, I mean, it's sending so very little. Why would it be? Hmm. I should be able to double that. Let me see here.

281
00:31:06,880 --> 00:31:25,880
Yeah, the audio is most important, right? It's really not that big of a deal. So let's see. It's probably going to buffer.

282
00:31:25,880 --> 00:31:39,880
So that's better, better quality, but you might find now that there's some buffering. I don't know. Okay, let's back to second life.

283
00:31:39,880 --> 00:31:46,880
Hey, how's it going over in second life? Sorry guys, I'm obsessed with YouTube here. Got some questions here.

284
00:31:46,880 --> 00:32:02,880
My dedicated second life following. Who if any do you follow not following your students? I'd imagine your teacher in the Buddha. Yes.

285
00:32:02,880 --> 00:32:13,880
Does the center have enough upload bandwidth to do both YouTube and second life live broadcast? Honestly, it seems like it's not taking much bandwidth at all.

286
00:32:13,880 --> 00:32:24,880
We're sending 64 kilobytes per second, which is really nothing. Well, there's a hundred and some kilobytes per second.

287
00:32:24,880 --> 00:32:32,880
We should be able to do a thousand kilobytes per second, I think.

288
00:32:32,880 --> 00:32:57,880
Yeah, YouTube says it's going to buffer, but I don't understand why that is. Anyway.

289
00:32:57,880 --> 00:33:10,880
Bring bug spray and turn off your phone. Well, yes. Yeah, I think it was a bit of a trick at the beginning of the talk where I talked about preparation.

290
00:33:10,880 --> 00:33:23,880
It's not the sort of preparation that people are expecting or are looking for when they ask that.

291
00:33:23,880 --> 00:33:34,880
Oh, yeah, it's the audio only one channel. That's a problem. I can mix it.

292
00:33:34,880 --> 00:33:44,880
I can figure out how to mix that. I don't really know how, but there's got to be a way.

293
00:33:44,880 --> 00:33:53,880
So, in opening ceremony for meditators, it's something to think about. Certainly.

294
00:33:53,880 --> 00:34:03,880
I haven't done it here in Canada much, but we could certainly start to find the documents and print them up.

295
00:34:03,880 --> 00:34:09,880
I think I've got the documents, the poly and English translation somewhere.

296
00:34:09,880 --> 00:34:15,880
They might be on our website, buried away somewhere.

297
00:34:15,880 --> 00:34:20,880
The word dumbah is things we know. Well, that's one meaning of it.

298
00:34:20,880 --> 00:34:28,880
The mind objects. The word dumbah is dumbah. I mean, it's an object of the mind.

299
00:34:28,880 --> 00:34:35,880
That's not the only meaning.

300
00:34:35,880 --> 00:34:47,880
The word dumbah has to do with holding, so it can mean things that hold their own in the sense of things that exist.

301
00:34:47,880 --> 00:35:04,880
It can mean things that one holds in the sense of one's doctrine and one's teaching or one's religion.

302
00:35:04,880 --> 00:35:10,880
Back to the low quality one.

303
00:35:10,880 --> 00:35:33,880
So, we should find the output will be better for the bandwidth, so it'll be better now.

304
00:35:33,880 --> 00:35:44,880
If you want, you won't have that loading thing, hopefully.

305
00:35:44,880 --> 00:35:59,880
Stream is healthy. We'll figure this out.

306
00:35:59,880 --> 00:36:05,880
Alright, well, if there's no other questions, then we'll say goodnight for tonight.

307
00:36:05,880 --> 00:36:25,880
We'll try this again, hopefully, sometime soon.

308
00:36:25,880 --> 00:36:35,880
Yeah, well, it's a choice. If on YouTube we either have bad quality or we buffer.

309
00:36:35,880 --> 00:36:39,880
I'm not sure why. It really shouldn't be that bad.

310
00:36:39,880 --> 00:36:48,880
It shouldn't be able to go a lot higher than this, but I'm not sure what's going on.

311
00:36:48,880 --> 00:36:55,880
Maybe I'll have to read up on how to do this more efficiently.

312
00:36:55,880 --> 00:37:21,880
Have a good night, everyone.

