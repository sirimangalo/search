1
00:00:00,000 --> 00:00:12,000
No, I lost you there, you're sound is turned up.

2
00:00:12,000 --> 00:00:13,000
Yes.

3
00:00:13,000 --> 00:00:17,000
Okay, sorry, go ahead.

4
00:00:17,000 --> 00:00:24,000
I am having a difficult time reconciling not having with my words while telling the truth.

5
00:00:24,000 --> 00:00:34,000
I know someone who asks me a question only to hear a soft response and gets upset when I answer honestly.

6
00:00:34,000 --> 00:00:36,000
Do I look fat in this dress?

7
00:00:36,000 --> 00:00:37,000
Yes.

8
00:00:37,000 --> 00:00:43,000
I was saying here is this example.

9
00:00:43,000 --> 00:00:45,000
Do I look fat in this dress?

10
00:00:45,000 --> 00:00:47,000
Yes, yes, you look fat in that dress.

11
00:00:47,000 --> 00:00:48,000
What do you do?

12
00:00:48,000 --> 00:00:53,000
How can you can you really answer and tell someone that they look fat in a dress?

13
00:00:53,000 --> 00:00:58,000
But his answer is...

14
00:00:58,000 --> 00:01:02,000
Well, I mean, you have to be clever.

15
00:01:02,000 --> 00:01:08,000
No, you have to be perfect.

16
00:01:08,000 --> 00:01:10,000
And you have to let go.

17
00:01:10,000 --> 00:01:12,000
There are two sides to this.

18
00:01:12,000 --> 00:01:14,000
This is the other side of lying.

19
00:01:14,000 --> 00:01:20,000
What do you do when you're in a position to lie?

20
00:01:20,000 --> 00:01:29,000
What do you do when telling the truth is going to disrupt your way of life?

21
00:01:29,000 --> 00:01:31,000
And so there's two sides.

22
00:01:31,000 --> 00:01:37,000
One, you become skillful so you don't disrupt your life.

23
00:01:37,000 --> 00:01:46,000
And two, you take it as a chance to point out to yourself that it's only because of your clinging that there's a problem here.

24
00:01:46,000 --> 00:01:57,000
In some cases, it's only because you're clinging to a relationship that's based on lies, even even white lies.

25
00:01:57,000 --> 00:02:03,000
Because people are so vain and self-conscious and so on.

26
00:02:03,000 --> 00:02:07,000
And you really want to be around people who ask, how do I look?

27
00:02:07,000 --> 00:02:08,000
How do I look?

28
00:02:08,000 --> 00:02:23,000
All the time. Or even can be even more sinister than that. People who need to be told lies just to be happy.

29
00:02:23,000 --> 00:02:26,000
And so is this really the kind of relationship that you want to have?

30
00:02:26,000 --> 00:02:31,000
Sam Harris, I would look up this book by Sam Harris lying, lying by Sam Harris.

31
00:02:31,000 --> 00:02:39,000
I think it's no longer for free, but you can find it for free on the script. I found it.

32
00:02:39,000 --> 00:02:44,000
I've got it for free. I don't know. It'd be even worth buying it.

33
00:02:44,000 --> 00:02:49,000
You know, this is things. See, I don't have money, so for me getting things for free is really important.

34
00:02:49,000 --> 00:02:52,000
But if you've got money, there's no problem with buying this book.

35
00:02:52,000 --> 00:02:55,000
I think it's $2 actually. That's ridiculous.

36
00:02:55,000 --> 00:03:01,000
So go and buy it on Amazon. This book that he wrote, lying by Sam Harris.

37
00:03:01,000 --> 00:03:07,000
And I agree with pretty much everything. Not pretty much almost everything he said.

38
00:03:07,000 --> 00:03:11,000
You know, I go a little further and saying lying is never good.

39
00:03:11,000 --> 00:03:18,000
But then I also would say that torture is never, never defensible.

40
00:03:18,000 --> 00:03:27,000
Whereas he seems to, I think he's just intellectualizing. So to the point where he finds that torture could be useful.

41
00:03:27,000 --> 00:03:34,000
Some could be beneficial and morally or ethically necessary or something.

42
00:03:34,000 --> 00:03:39,000
But he's a very good writer and he wrote really well in lying.

43
00:03:39,000 --> 00:03:49,000
Reality is not nearly as simple as we think. That goes without saying right.

44
00:03:49,000 --> 00:03:54,000
So we think it's either, A, I heard the person, A, I tell the truth and hurt the person or B, I lie and make them happy.

45
00:03:54,000 --> 00:04:00,000
It's never, it's not nearly as simple as that. What's the effect on your mind?

46
00:04:00,000 --> 00:04:05,000
What's the effect on your relationship with this person when you tell them a white lie?

47
00:04:05,000 --> 00:04:10,000
That your relationship is not, you don't feel totally free.

48
00:04:10,000 --> 00:04:14,000
You don't feel free to be totally honest with this person.

49
00:04:14,000 --> 00:04:17,000
And suppose it's your boss, right?

50
00:04:17,000 --> 00:04:22,000
And they want you to somehow lie to them or lie to other people.

51
00:04:22,000 --> 00:04:30,000
And so somehow you'll just say it's your boss happens to be a little bit overweight or heavier than most people.

52
00:04:30,000 --> 00:04:35,000
And so your boss asks, do I look fat in this dress?

53
00:04:35,000 --> 00:04:42,000
And you know if you're saying look fat that based on your past experiences with this person, they'll just fire you or they'll make your life very difficult.

54
00:04:42,000 --> 00:04:48,000
It's a good chance for you to realize that you're in the wrong life.

55
00:04:48,000 --> 00:04:56,000
You can't avoid these very, very difficult situations when you're surrounded by people who make things very, very difficult for you.

56
00:04:56,000 --> 00:05:01,000
Why the conclusion I can only ever come to is why don't you just start dating?

57
00:05:01,000 --> 00:05:04,000
Why don't you all just start dating?

58
00:05:04,000 --> 00:05:07,000
Why don't you all just give it up?

59
00:05:07,000 --> 00:05:10,000
What used to you find it? What good do you find it?

60
00:05:10,000 --> 00:05:13,000
Yes, you have attachments. Yes, you have desires.

61
00:05:13,000 --> 00:05:18,000
I have them. I'm not the perfect monk.

62
00:05:18,000 --> 00:05:22,000
I'm not perfect. I didn't become a monk because I was perfect.

63
00:05:22,000 --> 00:05:27,000
I'm not special. We are not special.

64
00:05:27,000 --> 00:05:30,000
Why not just do it?

65
00:05:30,000 --> 00:05:33,000
No, this is why I said there's two sides.

66
00:05:33,000 --> 00:05:42,000
If you have the view that there's some benefit in being with such people and being in such situations, then this is...

67
00:05:42,000 --> 00:05:53,000
I don't have much to say. We can never find a common ground in a certain case as I can persuade you otherwise, but I don't have that intention.

68
00:05:53,000 --> 00:06:01,000
If you want to be free from this, but you feel you're so attached to the world, this isn't a problem.

69
00:06:01,000 --> 00:06:05,000
This is why you become a monk. This is why you leave the world.

70
00:06:05,000 --> 00:06:13,000
Maybe not become a monk. Why you go to a meditation center? Because you know I have too many defilements to live in this place.

71
00:06:13,000 --> 00:06:18,000
If I continue this lifestyle, there's no way I'm going to progress in Dhamma.

72
00:06:18,000 --> 00:06:23,000
There's no way I'm going to become a better person. No way I might just go and find clarity.

73
00:06:23,000 --> 00:06:30,000
I can't progress here. This is why you leave and go off into first.

74
00:06:30,000 --> 00:06:35,000
You know that you've got a problem. That's why you do it. That's why you leave.

75
00:06:35,000 --> 00:06:39,000
This is the Buddha's teaching on gardening. You have to guard yourself.

76
00:06:39,000 --> 00:06:44,000
You have to do away with these complexities of life.

77
00:06:44,000 --> 00:06:49,000
This is why people become a monk. This is the best reason to become a monk.

78
00:06:49,000 --> 00:06:59,000
Or whatever, find a way to live a more simple life as a layperson. It's perfectly valid.

79
00:06:59,000 --> 00:07:23,000
People are able to live simpler lives than many monks. So, power to them.

