Hello and welcome back to our study of the Dhamupanda.
Today we continue on with verse 146, which reads as follows.
What is this? What is this laughter?
Wherefore the joy?
Neetong pajalites a day when there is fire everywhere.
When there is flames, there is always flames.
Neetong, there is constant flame burning there.
When there is constant burning.
Undergari in the Onanda for one who is shrouded by darkness.
Badipang nagavaseta.
For those you who are shrouded by darkness.
You don't seek out a light.
A lamp. Badipa.
This is another memorable story of the Onanda.
There is two versions of it.
There is the reasonable jotika version.
Some might say over the top Dhamupanda version.
I am not really sure which one to focus on.
The Dhamupanda version is quite entertaining.
Some people would argue it is not perhaps as accurate as the jotika version.
What they have in common is that it is about the companions of Wisaka.
Wisaka is a great Buddhist lay disciple.
She had these friends.
Wisaka was in an interesting position because she was married to a man who was not Buddhist.
There is a long story about that.
Sometimes marriage was often about securing connections between wealthy families and so she became a part of that.
But because she was so humble she didn't even argue or she didn't.
She didn't get upset about it at all.
But then she had great trouble and was ended up getting in a real fight with her stepfather.
Her father-in-law, her husband's father, he got very angry at her.
It is a long story. It is not our story here.
But the story we have here is of some of her friends who in this environment were perhaps,
clearly not so inclined towards purity and goodness.
Maybe they weren't such terrible people but they were perhaps misled.
And all of their husbands got together on a drinking festival and got really roaring, stinking drunk.
But the wives, of course, they had to do all the preparations and they weren't allowed to get drunk.
Now the Dhammapada story says that they got drunk anyway.
They convinced Wisaka to take them into this pleasure garden and they got drunk.
When they could be away from the eyes of their husbands and they took strong drink, it says.
And Wisaka asked them, what are you going to do?
Your husbands are going to be upset if you go home, don't know you're drunk and sure enough when they got back,
their husbands beat them all.
And they didn't learn, this is the Dhammapada story.
The Dhammapada story doesn't have any of this, it comes a bit later.
But then they still wanted to get drunk.
And so they asked Wisaka, they said, hey, that's going to garden again.
I said, no, no, I'm not.
I'm not having anything to do with you anymore.
And they said, well, then take us to see that for me I'm Buddhist and I follow the Buddhist teaching.
Strong drink is something that destroys your mindfulness.
That's what's important about this story for us as meditators.
And they said, well, then take us to see the Buddha.
There's anything to get them out of the house, right?
But they, so she took them to see the Buddha and they snuck, alcohol, jugs under their dresses, under their cloaks.
And so they all showed up in the monastery of the Buddha.
It would have been Jetawana, or been in India, it's a beautiful place.
And so they sat down and the Buddha started preaching.
They smuggled the alcohol out and they all got drunk.
And so the Buddha started, the Buddha was preaching, but they couldn't hear the tipping back and forth.
And then they started, and they got it in their heads that they would start dancing.
And so they all got up and started dancing in front of the Buddha.
And then the Dhammapada story says, again, this is where it gets a little bit exaggerated maybe.
But it says there was a Mara Angel, a deity of the Mara realms, the evil, the mischievous realms.
We decided it would be a good idea to get these women to act really improper and so she sort of possessed them
or something and cut them all to strip off their clothes and dance around naked in front of the Buddha.
Quite a sight.
And so both stories make clear that the Buddha sent forth some sort of special magical power, some projection of color, of very dark rays of not lightness,
but rays of darkness or dark blue light or something.
And it frightened them. He shocked them back into sobriety somehow.
And then he taught this verse.
Why are you laughing? Wherefore the joy?
When there is constant flame, it's burning constantly.
The world, the commentary says it's referring to the word, the world is burning actually.
So it sounds, I think, for the ordinary listener, it sounds kind of exciting.
I imagine for the meditators there might be a twin, a slight thought of dancing, singing, maybe even alcohol.
I suppose if you've started meditating alcohol, it's less and less appealing.
As you start to see how messed up your mind already is without alcohol.
But dancing and singing, they seem like wonderful things. It's exciting, right?
It releases all sorts of pleasant chemicals in the brain and makes you feel lots of pleasure.
We don't see the fire. Intoxication, it's an interesting topic.
The alcohol is something that intoxicates you.
We should talk about this because it's an issue in meditation, practice, the living of your life.
What are the things that are going to support your practice?
What are the things that are going to hinder it?
Alcohol is one of the things that's the antithesis of meditation.
It's the opposite.
The meditation is about clearing your mind, trying to cultivate the habit of seeing things clearly as they are,
and alcohol is for the explicit purpose of dulling and muddling things, so that you can't see things.
Your ordinary reactions to things are dull.
As a result, you never feel any pain or suffering.
You're just able to do whatever you want, cultivate whatever bad habits you like.
You get angry and there's no consequences because you're not aware of them.
That's really the thing about alcohol is that it blinds you to the nature of your actions.
I've told this story before, but there's one night.
Remember explicitly, I mean, I used to get drunk and thinking back.
I don't suppose I did a lot of terrible things, but there was one night that I remember explicitly.
I was just feeling with the wrong crowd and ended up going around stealing other people's alcohol and waking people up
and bothering people in the middle of the night and getting into all sorts of trouble and ended up getting seriously damaging friendships as a result.
There's so many stories and the results of alcohol, abuse comes from alcohol and violence of all sorts.
Of course, sexual promiscuity comes unwanted pregnancies, I imagine.
But it cultivates all sorts of terrible bad habits. It allows your mind to freedom to be as evil as you want.
You could be good under alcohol, I suppose. It's possible that under alcohol you could be quite nice to people.
There are times, you know, the happy drunk who sits there and says, I love you, man.
You could argue there's some wholesomeness going on there.
There's a lot of delusion and wholesomeness.
But it prevents you from being able to see clearly. It's a very essence of what it does.
So first of all, that's something you have to be clear about.
Things like drinking alcohol, most drugs as well are highly problematic for meditation practice.
And I think it's an obvious one.
It just makes things to distort it.
People who do marijuana might argue I've done it before, I was never really.
It a lot of marijuana for a time.
Propomatic doesn't allow you to have a clear mind and to see things clearly as there.
As men, these things are marijuana and alcohol.
In general, they're meant to dull things.
They're meant to bring positive states only and allow you free from the need to experience the harshness of reality.
Because reality can become quite harsh if you're not well-trained and sharp as a diamond.
It will cut you.
So the first part, the other part is that it allows us to talk about the other kind of intoxication.
It's the intoxication of the ordinary people, the intoxication that's present in all of us at all times.
Not at all times, but throughout our lives.
Regardless of what we're taking into our bodies, it's coming from our own minds.
Just the chemicals in the brain, this is enough to intoxicate anyone.
In Buddhism, the Buddha mentions, I think, four types of intoxication, or three types of intoxication,
intoxication with youth, intoxication with health, and intoxication with life.
I can't remember if there's a fourth bit.
So young people, young people are intoxicated with youth.
So it leads us to, to far more alcohol drugs in alcohol,
and then, well, get to do far more than we would otherwise.
Very young, right?
And teenage years, some people never grow out of it, but when we're young, we're inclined to do all sorts of crazy things,
thinking, we're never going to die, and there'll be no consequences.
It's unfortunate because there's so much potential, and that's what you get intoxicated by, infinite potential.
So do whatever you want, no consequences, but there are consequences.
It's unfortunate part is when we're young, that's when we have the power.
The Buddha said, this practice is not for old people.
They have all these Buddhists nowadays, and even in the Buddhist time saying,
oh, when I get old, then old practice, when I'm retired, right?
It was a thing in the Buddhist time, even.
Old men would go off into the forest and tend to the flame.
It was about all they were good for us, pouring butter on the flame.
It was really just practice. Forget about meditation.
Some old people can be very good meditators. It's not the same, but the Buddha said,
this is a hard practice if you've spent your life engaged in unholsiness.
It gets more and more difficult.
It's not to disparage people who are older than practice, but it's to wake up those people
who are intoxicated with youth and think, oh, I've got lots of time.
I'm sure you have lots of time, but lots of time spent doing cultivating unholsom habits
as just setting yourself up for disaster.
When you come around to do meditation, you've built up all these bad habits
and you're no longer in a state to be able to meditate.
We're intoxicated with health, healthy fit, healthy and fit.
Thinking illness will never come to me.
You can intoxicated by it.
It doesn't really matter whether to talk about getting old or getting sick.
Of course, that's the reality of it, right?
You could get sick at any time and you're going to get old and youth will not last forever.
But more to the point is just being attached and becoming intoxicated with youth and your health.
Healthy people get so wrapped up in materialism, exercising, running, dancing, singing, right?
You see, these women dancing and singing enjoying their bodies.
So much that they became intoxicated.
It's interesting because this is what you start to see in meditation.
That reality is quite a bit harsher than we think it is.
You have two options. You can spend your time intoxicating yourself either with external substances
or just with the chemicals in your brain.
Find ways to find pleasure again and again.
It's intoxicating.
It blinds you to the reality.
You feel pain, you feel stress, go off and take your drug, shoot up in the brain.
And it's repressed for a while. Of course, you get to worse the next time it comes
and worse and worse until you become a drug addict.
Whether it's external drugs or brain drugs, it's all the same.
And then eventually it's compounded by the fact that you do get old and you do get sick.
And then you're totally unprepared for death and you wind up a real mess.
Or report as a dog or a pig or something.
Really who knows when your mind is not clear.
The third intoxication is intoxication.
The intoxicated by life, intoxication with life.
I'm alive, carpy deums, he's the day, the stupidest thing in the world.
This precious thing called life, right? This precious life.
And then you say, yeah, so let's do whatever we can to mess it up
because we're going to die tomorrow, right? We could die anytime.
Well, it makes sense if there's nothing after.
If there's nothing after life, then no, it doesn't even make sense then.
This is what I've talked about, is that even if we were to die and never be reborn again,
it would make no sense to intoxicate yourself further.
That's what we don't get.
That's what you start to see in meditation.
Meditation seems so stressful.
Wouldn't it be nicer if we danced?
When I was in Florida, someone was talking about laughter yoga or something, laughing yoga.
I don't know, as a Buddhist, you sometimes have to bite your tongue with all these spiritual practices.
Arguably, some of them are positive, but I think they get a little bit indulgent.
When it'd be better, if I taught you all how to laugh and solve your problem by laughing, sounds more fun.
But that's what meditation shows, you know, meditation, insight meditation shows you something different.
It shows you the nature of reality, which shows you your experience.
Oops.
It teaches you to overcome your intoxication.
So recently, the meditation shows you that.
It's not actually happiness.
When you're meditating, you can see all of your impurities we'd call them.
But let's think of them as our attachments, our desires.
And you're able to reflect objectively on it.
And as you experience the other experience, the things that you want and the wanting of it,
you can feel the stress.
You can see how it's actually stressful.
Like a dog, a hungry dog who sees a bone smeared with blood and things.
Oh, I'll sit sheet my hunger with that bone, gnawing on the bone,
and can gnaw in the bone forever and will never be satisfied.
We'll always have this gnawing, stressful feeling of hunger.
That's what you start to see in the meditation.
And then, of course, the fact that there is life after death is compounds that because
people who die when they're on...
There's a story recently of someone who fell off a roof.
They were on acid and LSD and fell off a roof.
So, would you imagine what that would be like to die when you're on drugs?
Death can come at any time.
It's called genya marinang suui.
So, that's the verse.
That brings us to the verse.
Verse has some interesting aspects.
So, why laughter, why exaltation?
This is this.
There's a better translation.
Here's the fun translation from the jotica.
You cannot remember this.
Is it rhymes?
No place for laughter here, no room for joy.
The flames of passion suffering worlds destroy.
The flames of passion suffering worlds destroy.
Why overwhelmed and why overwhelmed and darkest night I pray seeking no torch to light you on your way?
It's a good question for all of us, not just these toxicated women,
all of us living in the world, chasing after useless things,
distracting ourselves until tragedy strikes.
And then, it's too late we're unable to deal with it.
We're untrained and prepared for it.
Meanwhile, we live our lives content with mediocrity, with stressful lives,
with interspersed with bits of happiness.
We don't see the flame.
We see only the light.
Oh, pretty light.
We don't see the burning.
We want to give a whole suit that we'll discuss on burning.
We're going to adhere, but we have the flames of defilement,
the flames of suffering, the flames of old age sickness and death.
There are consequences, and there are problematic.
There's problematic.
There's something problematic about craving and clinging and desire.
That's what we start to learn in meditation.
So you should be able to see as you stay here to continue to practice.
We're in darkness, means we're in delusion.
This is what intoxication does to you.
This is what our desires and our attachments do.
They blind us to which we're going on.
They blind us to the present moment in the here and now.
So mindfulness is this torch.
The Buddha's teaching, but more specifically mindfulness.
Mindfulness is this light that you shine in and it clears up all the darkness immediately.
There's a wonderful thing about mindfulness practice.
You don't have to actually cultivate it.
You can start right now.
You can apply it right now at any moment when you wake up and you say,
what am I going to do? Be mindful at that moment.
This is what you're cultivating.
This is what we're working on here.
There's this ability to manifest the presence of mind that keeps us here and now.
It keeps us aware, alert, awake.
It keeps us sober.
So there you go.
There's our Dhammapanda first for this evening.
An intoxication.
Thank you all for tuning in.
I'll see you on the next one.
