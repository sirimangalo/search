1
00:00:00,000 --> 00:00:27,000
Okay, but evening broadcasting live from British Columbia, Canada.

2
00:00:27,000 --> 00:00:43,000
Joining with the ardent group of online meditators that have assembled at meditation.ceremungalow.org

3
00:00:43,000 --> 00:00:50,000
And embarrassingly, my name is in orange.

4
00:00:50,000 --> 00:01:00,000
I've spent all day to day with meditators, but actually I've spent most of the day talking.

5
00:01:00,000 --> 00:01:07,000
And being mindful, there's nothing like being with meditators to remind you.

6
00:01:07,000 --> 00:01:16,000
And even when you're talking, you can be mindful, no matter what you're doing.

7
00:01:16,000 --> 00:01:23,000
So, I've been with meditators since 8 a.m. this morning.

8
00:01:23,000 --> 00:01:42,000
And then constantly up until 10 a.m. and 10 after 10 when I realized I was late for our study session.

9
00:01:42,000 --> 00:01:54,000
So, I heard her in back here to the basement to go online and study that we sued the manga for an hour.

10
00:01:54,000 --> 00:02:00,000
And then we skipped our poly because I had to go eat lunch.

11
00:02:00,000 --> 00:02:13,000
And then back to the meditation hall for more talk to 2 p.m.

12
00:02:13,000 --> 00:02:19,000
And then back here to the basement at 2 p.m. to talk to children.

13
00:02:19,000 --> 00:02:22,000
There was supposed to be a room full of children.

14
00:02:22,000 --> 00:02:31,000
Turned out a room full was four. We had four children, but I spent over an hour talking to them.

15
00:02:31,000 --> 00:02:33,000
They were a tough sell.

16
00:02:33,000 --> 00:02:45,000
But I think in the end, I was able to give some idea of why they might want to practice meditation.

17
00:02:45,000 --> 00:02:54,000
I asked one of the girls, what did she want out of life? And I said, but what I mean is, what do you wish?

18
00:02:54,000 --> 00:03:01,000
Not what is realistic, not what you expect, not what you've settled on in life, knowing it's possible.

19
00:03:01,000 --> 00:03:04,000
If anything were possible, what would you wish for?

20
00:03:04,000 --> 00:03:12,000
And she said, surprisingly, it surprised me. She said, freedom from suffering.

21
00:03:12,000 --> 00:03:22,000
I mean, it shouldn't really have surprised me, but no, coming from a kid, I would have, I thought, oh, I was expecting a career choice.

22
00:03:22,000 --> 00:03:30,000
But freedom from suffering is commendable. And she said, but I don't think it's possible.

23
00:03:30,000 --> 00:03:40,000
And then I said, okay. And we worked with that. And I said, well, what if I told you, what if I asked you, what if I told you I could provide you with

24
00:03:40,000 --> 00:03:51,000
freedom from mental suffering? And I started to explain the difference between physical suffering and mental suffering.

25
00:03:51,000 --> 00:03:54,000
She seemed to get that.

26
00:03:54,000 --> 00:03:58,000
And so I showed them, I said, well, close your eyes.

27
00:03:58,000 --> 00:04:02,000
And I said, sit still and don't move until you feel pain.

28
00:04:02,000 --> 00:04:15,000
And so they sat for about a minute. And then she said she felt pain. And so I had her note to her self pain pain.

29
00:04:15,000 --> 00:04:27,000
And I think she understood, I think the four of them understood, I mean, I was talking, we talked for over an hour, but this was basically what it came down to.

30
00:04:27,000 --> 00:04:33,000
And so I tried to provide the concept of the difference between physical pain and mental pain.

31
00:04:33,000 --> 00:04:42,000
Then we talked for, we talked about heaven, we talked about how you can really physical pain with your mind is calm and how

32
00:04:42,000 --> 00:04:48,000
if you're born in heaven, then you don't have physical pain of being born in human and so on and so on.

33
00:04:48,000 --> 00:05:04,000
It was interesting, one of the more interesting parts of the talk was I asked her something about her, her, her, her, I said, I asked them if they showered.

34
00:05:04,000 --> 00:05:16,000
And she said, yes, she showered almost every day. And I said, why do you showered if you keep clean? And I said, so what about your mind? Is your mind clean? Do you keep your mind clean?

35
00:05:16,000 --> 00:05:23,000
I said, how do you keep your mind clean? And she said, by thinking positive.

36
00:05:23,000 --> 00:05:30,000
So it's this idea of positive thinking, positive thinking is the answer.

37
00:05:30,000 --> 00:05:39,000
And so we talked about it for a bit. And then I said to her, I started describing a person who didn't think positively, but had good thoughts,

38
00:05:39,000 --> 00:05:48,000
and sought to help people. Positive thinking is, I can succeed, I can win, I will do it, I can, I can win.

39
00:05:48,000 --> 00:05:55,000
I'm going to succeed, I'm going to pass the test.

40
00:05:55,000 --> 00:06:06,000
It was funny, we talked for quite a while, but it came down to, I said, I described the person who had good thoughts.

41
00:06:06,000 --> 00:06:10,000
And I said, how does that compare in your mind to someone who has positive thoughts?

42
00:06:10,000 --> 00:06:17,000
So he thinks positively, he said, yes, the first one, the one who has good thoughts and helpful for others,

43
00:06:17,000 --> 00:06:22,000
who is kind and compassionate. And I said, it's actually quite a difference.

44
00:06:22,000 --> 00:06:30,000
You see, it's a different, it's two different things. Thinking positive was one thing. Being positive is another.

45
00:06:30,000 --> 00:06:39,000
Being a positive, being good.

46
00:06:39,000 --> 00:06:48,000
Anyway, so that lasted until after three, and then right back to the meditation hall where I was due at three,

47
00:06:48,000 --> 00:06:53,000
but I think I actually got stopped to talk to someone else to answer their questions.

48
00:06:53,000 --> 00:07:02,000
And then from 320 to four, I gave a talk. And then at four, I answered more questions.

49
00:07:02,000 --> 00:07:13,000
And then I discussed some things and sort of we were chatting for a while about how the course went and so on.

50
00:07:13,000 --> 00:07:21,000
They charged money for the course, and I said, I think if it's, if they're going to charge, I wouldn't come in the future.

51
00:07:21,000 --> 00:07:26,000
I think I have to make a statement because it didn't really feel right, even though the money has nothing to do with me.

52
00:07:26,000 --> 00:07:31,000
It's still kind of like people were paying to come and see me. That's kind of what it was like.

53
00:07:31,000 --> 00:07:39,000
So express that. They agreed. The people I talked to were in agreement. They didn't agree with the charging.

54
00:07:39,000 --> 00:07:57,000
It's, you know, and they're on the board so they were going to look into that.

55
00:07:57,000 --> 00:08:14,000
Oh, I know what it is. I'm on this guest network. Can I bet it's going to ask me to type in a password again?

56
00:08:14,000 --> 00:08:21,000
Can you mean me? Maybe not.

57
00:08:21,000 --> 00:08:27,000
No, no, I'm not even connected to the Internet here.

58
00:08:27,000 --> 00:08:32,000
Oh, here we are. Okay.

59
00:08:32,000 --> 00:08:39,000
This isn't the ideal Internet. Send up, that's for sure.

60
00:08:39,000 --> 00:08:44,000
Live stream connected again.

61
00:08:44,000 --> 00:08:53,000
Okay. So audio is back if you want to switch back to that.

62
00:08:53,000 --> 00:08:57,000
I haven't even started yet. I'm just complaining about my day so far.

63
00:08:57,000 --> 00:09:03,000
Not complaining, but giving excuses are still why my name is still in yellow orange.

64
00:09:03,000 --> 00:09:12,000
I haven't done my online meditation today.

65
00:09:12,000 --> 00:09:16,000
We have an interesting quote today about inclination.

66
00:09:16,000 --> 00:09:19,000
How someone who practices.

67
00:09:19,000 --> 00:09:24,000
The eight full noble path inclines towards the abandon of the Buddha often.

68
00:09:24,000 --> 00:09:31,000
Well, it's in on more than one occasion talked about a tree, how a tree inclines in one direction.

69
00:09:31,000 --> 00:09:35,000
And if the tree is inclined.

70
00:09:35,000 --> 00:09:42,000
Towards some sara, then oh, no, sorry, it's a person is inclined towards some sara.

71
00:09:42,000 --> 00:09:45,000
Then when they die, they are reborn.

72
00:09:45,000 --> 00:09:52,000
If the person is inclined towards the mano, then when they die, they're not reborn.

73
00:09:52,000 --> 00:10:06,000
So it's kind of an encouragement for us, the idea that it does make a difference.

74
00:10:06,000 --> 00:10:10,000
Your practice does make a difference even though you can't see it.

75
00:10:10,000 --> 00:10:14,000
You can envision yourself as inclining in one direction.

76
00:10:14,000 --> 00:10:21,000
It sort of speaks of our habits, how our behaviors inform our habits and cultivate habits.

77
00:10:21,000 --> 00:10:24,000
Become habits.

78
00:10:24,000 --> 00:10:33,000
If we are mindful by habit, that's an, it becomes our inclination.

79
00:10:33,000 --> 00:10:37,000
If we're inclined to do good, we do good often.

80
00:10:37,000 --> 00:10:40,000
We become inclined towards goodness.

81
00:10:40,000 --> 00:10:42,000
We're born in heaven.

82
00:10:42,000 --> 00:10:47,000
If we practice somewhat to meditation, we are inclined towards tranquility.

83
00:10:47,000 --> 00:10:51,000
Then we're born as a brahma.

84
00:10:51,000 --> 00:10:57,000
If we do evil deans, we're inclined in that direction.

85
00:10:57,000 --> 00:11:01,000
Then we'll be reborn in that way.

86
00:11:01,000 --> 00:11:05,000
If we do human things, if we act like a human being or a denarian living our life,

87
00:11:05,000 --> 00:11:11,000
then I would think chances are we'll be born as a human being.

88
00:11:11,000 --> 00:11:18,000
It seems reasonable.

89
00:11:18,000 --> 00:11:20,000
It's such a reasonable system.

90
00:11:20,000 --> 00:11:25,000
It's such a reasonable set of beliefs, if you want to call them that.

91
00:11:25,000 --> 00:11:31,000
Even without getting into why it's the most reasonable and why it's actually the default,

92
00:11:31,000 --> 00:11:36,000
what we call the null hypothesis.

93
00:11:36,000 --> 00:11:41,000
I mean, I laughed because I caused so much outrage.

94
00:11:41,000 --> 00:11:47,000
I suggested that rebirth is the null hypothesis.

95
00:11:47,000 --> 00:11:51,000
I didn't go over very well.

96
00:11:51,000 --> 00:11:53,000
But that's what I believe.

97
00:11:53,000 --> 00:12:01,000
That's my opinion, my view.

98
00:12:01,000 --> 00:12:06,000
Anyway, I don't have anything to say about this quote besides that.

99
00:12:06,000 --> 00:12:10,000
Maybe someone else has some comment to make that'll inspire me to say more,

100
00:12:10,000 --> 00:12:20,000
but otherwise, if anyone has any questions, I'm here to answer.

101
00:12:20,000 --> 00:12:32,000
Even though my name is an orange,

102
00:12:32,000 --> 00:12:41,000
define what constitutes a sangha.

103
00:12:41,000 --> 00:12:45,000
Well, I think technically there are only two types of sangha.

104
00:12:45,000 --> 00:12:49,000
The sangha is the group of monks.

105
00:12:49,000 --> 00:12:57,000
And the sangha is all enlightened beings from Sotapana on up.

106
00:12:57,000 --> 00:13:03,000
So, when we talk about a group of late dhamma practitioners as being a sangha,

107
00:13:03,000 --> 00:13:05,000
it's a bit of a loose interpretation.

108
00:13:05,000 --> 00:13:09,000
And it's not really how the texts use it.

109
00:13:09,000 --> 00:13:10,000
I don't think.

110
00:13:10,000 --> 00:13:16,000
I mean, I suppose there are usages where sangha just means a group of people or a group of individuals.

111
00:13:16,000 --> 00:13:21,000
But in a Buddhist sense, you don't call that a sangha.

112
00:13:21,000 --> 00:13:25,000
It wouldn't have been referred to as a sangha of late people.

113
00:13:25,000 --> 00:13:27,000
There are only two sanghas.

114
00:13:27,000 --> 00:13:29,000
There's the sangha of monks.

115
00:13:29,000 --> 00:13:32,000
And then there's the sangha of enlightened beings.

116
00:13:32,000 --> 00:13:37,000
But that doesn't mean that you shouldn't adapt the term and refer to you all as late people.

117
00:13:37,000 --> 00:13:45,000
Another thing you could say is those people who are practicing to become a sotapana.

118
00:13:45,000 --> 00:13:51,000
So, they're not a sotapana yet.

119
00:13:51,000 --> 00:13:54,000
It can also be considered to be Arya Sawaka.

120
00:13:54,000 --> 00:13:56,000
Like, they're students of the Arya.

121
00:13:56,000 --> 00:14:00,000
Even though they're not enlightened yet or Arya's areas.

122
00:14:00,000 --> 00:14:02,000
Google are yet.

123
00:14:02,000 --> 00:14:07,000
They could still be considered sangha, sawaka sangha.

124
00:14:07,000 --> 00:14:13,000
In a sense that there are people striving for sotapana.

125
00:14:13,000 --> 00:14:23,000
I think that's reasonable.

126
00:14:23,000 --> 00:14:25,000
No, these words were all adapted by the Buddha.

127
00:14:25,000 --> 00:14:29,000
So, you could adapt them as you like.

128
00:14:29,000 --> 00:14:30,000
Null hypothesis.

129
00:14:30,000 --> 00:14:33,000
What is the null hypothesis?

130
00:14:33,000 --> 00:14:37,000
A null hypothesis is the hypothesis that nothing happens.

131
00:14:37,000 --> 00:14:42,000
Suppose you do an experiment with a idea that something might happen.

132
00:14:42,000 --> 00:14:46,000
Like, if I mix these two chemicals, there will be an explosion.

133
00:14:46,000 --> 00:14:52,000
Well, the null hypothesis is that if you mix those two chemicals, there is no explosion.

134
00:14:52,000 --> 00:14:54,000
That's the null hypothesis.

135
00:14:54,000 --> 00:14:56,000
You can look it up on Wikipedia.

136
00:14:56,000 --> 00:15:01,000
I'm sure it's a bit more nuanced than that, but that's basically it.

137
00:15:01,000 --> 00:15:03,000
Nothing happens.

138
00:15:03,000 --> 00:15:07,000
It's when you hypothesize that nothing is going to happen.

139
00:15:07,000 --> 00:15:12,000
Nothing, no change, and I qualified by emphasizing the fact

140
00:15:12,000 --> 00:15:15,000
that what it really means is no change occurs.

141
00:15:15,000 --> 00:15:16,000
Sure, something happens.

142
00:15:16,000 --> 00:15:17,000
You mix the two chemicals.

143
00:15:17,000 --> 00:15:19,000
The two chemicals are still there.

144
00:15:19,000 --> 00:15:20,000
Something happened.

145
00:15:20,000 --> 00:15:23,000
You've got a mixture of two chemicals.

146
00:15:23,000 --> 00:15:25,000
Nothing out of the ordinary.

147
00:15:25,000 --> 00:15:27,000
Nothing changed.

148
00:15:27,000 --> 00:15:30,000
And that's important, an important distinction.

149
00:15:30,000 --> 00:15:33,000
So, at death,

150
00:15:33,000 --> 00:15:37,000
where nothing to change, the mind would continue on.

151
00:15:37,000 --> 00:15:39,000
Do you see where I'm going with this?

152
00:15:39,000 --> 00:15:44,000
It is the null hypothesis that the mind should continue on after death.

153
00:15:44,000 --> 00:15:45,000
Clever, isn't it?

154
00:15:45,000 --> 00:15:53,000
I thought it was kind of clever.

155
00:15:53,000 --> 00:15:54,000
They certainly did.

156
00:15:54,000 --> 00:15:57,000
The group I explained this to didn't.

157
00:15:57,000 --> 00:15:58,000
One man didn't.

158
00:15:58,000 --> 00:16:01,000
He exploded.

159
00:16:01,000 --> 00:16:11,000
No, no, no hypothesis for him.

160
00:16:11,000 --> 00:16:17,000
What are the differences between working on mental pain and body pain?

161
00:16:17,000 --> 00:16:24,000
I think we don't put any emphasis on working on body pain.

162
00:16:24,000 --> 00:16:26,000
It's not our interest.

163
00:16:26,000 --> 00:16:29,000
In fact, you could even say we don't put emphasis on

164
00:16:29,000 --> 00:16:32,000
quote-unquote working on mental pain.

165
00:16:32,000 --> 00:16:36,000
Because by working on you mean becoming free from.

166
00:16:36,000 --> 00:16:39,000
And that's not how the practice works.

167
00:16:39,000 --> 00:16:43,000
The practice works by seeing clearly both physical pain and mental pain.

168
00:16:43,000 --> 00:16:46,000
It's not about working on anything.

169
00:16:46,000 --> 00:16:49,000
It's like when you say working on it's like something's wrong

170
00:16:49,000 --> 00:16:50,000
that you have to fix it.

171
00:16:50,000 --> 00:16:52,000
That's not the way meditation works.

172
00:16:52,000 --> 00:16:58,000
It's not under the assumption that something is broken.

173
00:16:58,000 --> 00:17:02,000
Even though theoretically we have this idea that something's broken,

174
00:17:02,000 --> 00:17:08,000
our practice is not undertaken in that light.

175
00:17:08,000 --> 00:17:12,000
It's undertaken for the purpose of understanding, seeing,

176
00:17:12,000 --> 00:17:16,000
learning about studying.

177
00:17:16,000 --> 00:17:20,000
The knowledge of what is right and what is wrong comes as a result of our study.

178
00:17:20,000 --> 00:17:23,000
But our study is objective.

179
00:17:23,000 --> 00:17:25,000
So we are objective about bodily pain.

180
00:17:25,000 --> 00:17:27,000
We're objective about mental pain.

181
00:17:27,000 --> 00:17:30,000
And we start to change as a result.

182
00:17:30,000 --> 00:17:34,000
We experienced less mental pain.

183
00:17:34,000 --> 00:17:41,000
And we stop being upset about bodily pain.

184
00:17:41,000 --> 00:17:43,000
Just how important is posture while meditating?

185
00:17:43,000 --> 00:17:46,000
You mentioned our cushions should get lower and lower with time,

186
00:17:46,000 --> 00:17:51,000
but I find my posture getting poorer the lower I get to the floor.

187
00:17:51,000 --> 00:17:52,000
Yeah.

188
00:17:52,000 --> 00:17:53,000
Yeah.

189
00:17:53,000 --> 00:17:56,000
So there's something to be said about a little bit of cushion under

190
00:17:56,000 --> 00:18:01,000
your rear end to keep your posture up.

191
00:18:01,000 --> 00:18:05,000
I don't know that that's really, truly the case.

192
00:18:05,000 --> 00:18:08,000
Because I mean, I can keep good posture sitting on the floor.

193
00:18:08,000 --> 00:18:12,000
So I don't see why it has to necessarily mean bad posture,

194
00:18:12,000 --> 00:18:17,000
just because I'm flat here on the carpet.

195
00:18:17,000 --> 00:18:20,000
Uh,

196
00:18:20,000 --> 00:18:25,000
in reverse and the posture isn't really all that important at all.

197
00:18:25,000 --> 00:18:27,000
My teacher sits like this.

198
00:18:27,000 --> 00:18:30,000
He's 91 years old and he's probably the most awesome monk.

199
00:18:30,000 --> 00:18:31,000
I know.

200
00:18:31,000 --> 00:18:34,000
And he's the worst posture.

201
00:18:34,000 --> 00:18:36,000
I mean, I guess at 91,

202
00:18:36,000 --> 00:18:42,000
it's a different story because old people and, you know,

203
00:18:42,000 --> 00:18:49,000
but still it obviously is working for him even at this age.

204
00:18:49,000 --> 00:18:52,000
Um, you know, the Buddha said,

205
00:18:52,000 --> 00:18:57,000
you know, let me go to the party because I'm not going to be

206
00:18:57,000 --> 00:19:00,000
able to quote it exactly.

207
00:19:00,000 --> 00:19:07,000
Luckily now that we're on the computer, I can go to the

208
00:19:07,000 --> 00:19:13,000
suit that.

209
00:19:13,000 --> 00:19:15,000
Here we say,

210
00:19:15,000 --> 00:19:23,000
and then it says,

211
00:19:23,000 --> 00:19:44,000
and then it says,

212
00:19:44,000 --> 00:19:47,000
so,

213
00:19:47,000 --> 00:19:51,000
so the atom means however means in whatever way.

214
00:19:51,000 --> 00:19:55,000
Wow or in whatever way.

215
00:19:55,000 --> 00:19:59,000
Asakayo, the body of that person,

216
00:19:59,000 --> 00:20:03,000
but he to always dispossessed is, is, is, is,

217
00:20:03,000 --> 00:20:08,000
is a set, you know, whatever position is in.

218
00:20:08,000 --> 00:20:11,000
Tātātātātātā nānānānātī.

219
00:20:11,000 --> 00:20:20,000
one knows that in one knows clearly bhajananti, fully, it in that way, meaning

220
00:20:20,000 --> 00:20:24,960
whatever posture you're in. He literally says that, yataya tawa, however the body

221
00:20:24,960 --> 00:20:32,160
is disposed, bhanyito. So even if you're not walking, standing, sitting, or lying

222
00:20:32,160 --> 00:20:38,000
down, you could be crouching, leaning, stouching, whatever, then you know it as it

223
00:20:38,000 --> 00:20:51,000
is, that's the only important thing. Can you talk about smiling and one's

224
00:20:51,000 --> 00:20:55,880
demeanor? Sometimes people say I should smile more, but that expression doesn't

225
00:20:55,880 --> 00:21:06,320
come naturally to me. Right? Yeah, I mean, it's not about the smile. I would say

226
00:21:06,320 --> 00:21:10,080
that enlightened ones do smile, the Buddha smiled, but they smile for peculiar

227
00:21:10,080 --> 00:21:18,880
reasons. The Buddha smiles when he sees something remarkable. For example, a

228
00:21:18,880 --> 00:21:23,840
pig, the Buddha smiled when he saw a pig, but it turns out the pig was once a

229
00:21:23,840 --> 00:21:28,000
brahma. So he found that remarkable that a brahma should then be

230
00:21:28,000 --> 00:21:38,080
reborn as a pig. So he smiled at the pig. I just bring that up to say, well,

231
00:21:38,080 --> 00:21:42,640
smiling is a weird thing. I don't suppose if you smile that pigs people would

232
00:21:42,640 --> 00:21:52,520
be contented. They want you to smile at jokes. They want you to be happy. People

233
00:21:52,520 --> 00:22:00,080
like other people to be happy. It's one of the annoying things about people as

234
00:22:00,080 --> 00:22:05,120
they want you to be happy. And then you just say, it makes you feel less inclined

235
00:22:05,120 --> 00:22:14,280
to be happy. I'm kind of kidding. Smiling is a sign of happiness, I think, yes.

236
00:22:14,280 --> 00:22:20,720
But, you know, we're not practicing happiness. Sometimes the Buddha said it's

237
00:22:20,720 --> 00:22:28,200
better to practice properly with tears in your eyes, weeping, crying, moaning,

238
00:22:28,200 --> 00:22:34,520
wailing at the hardship of it than it is to be happy, practicing in the wrong

239
00:22:34,520 --> 00:22:40,040
way. We're not practicing happiness, better to be miserable doing the right

240
00:22:40,040 --> 00:22:46,040
thing because it is not happiness that leads to happiness. I'll say it again,

241
00:22:46,040 --> 00:22:51,800
happiness does not link to happiness. In other words, happiness is not useful in

242
00:22:51,800 --> 00:22:59,000
any way. It is goodness that leads to happiness. Therefore, goodness is useful.

243
00:22:59,000 --> 00:23:03,280
Goodness is that which has benefited. And if you focus completely on

244
00:23:03,280 --> 00:23:23,120
goodness, happiness is what we will result. It just might take some time. Most of

245
00:23:23,120 --> 00:23:27,520
the experiences that arise during my meditation are physical. Human to

246
00:23:27,520 --> 00:23:33,720
experience is, how should I adjust my practice according to this? I mean, if you

247
00:23:33,720 --> 00:23:37,760
really don't have any mental discomfort, practice longer, do long sessions and

248
00:23:37,760 --> 00:23:43,760
practice all the time, do meditation during the day. It sounds great. It sounds

249
00:23:43,760 --> 00:23:53,640
like an aha, really. I wouldn't say there's much problem. Just say pain, pain,

250
00:23:53,640 --> 00:23:58,080
itching and shame. It may be that you're not clearly aware of all the mental

251
00:23:58,080 --> 00:24:00,920
stuff that's going on. You have to take that into account. Maybe you're just

252
00:24:00,920 --> 00:24:06,720
missing all the mental stuff. But if we take what you say at face value, then

253
00:24:06,720 --> 00:24:11,040
that's fine. It's a sign that you don't have many problems to deal with. You

254
00:24:11,040 --> 00:24:15,920
don't have much wrong with you. Maybe you are already partially, maybe you're

255
00:24:15,920 --> 00:24:24,120
already a sort of problem. Maybe you're already an anacami. I don't know. But when

256
00:24:24,120 --> 00:24:29,040
it does come up, then you need the meditation to help you deal with it if it

257
00:24:29,040 --> 00:24:37,040
does come up. Could you speak a bit about dealing with loneliness through

258
00:24:37,040 --> 00:24:41,800
meditation? And do you ever feel lonely as a monk? I don't have time to feel

259
00:24:41,800 --> 00:24:47,680
lonely. Surrounded by people and I do too much. But no, absolutely not. I'm not

260
00:24:47,680 --> 00:24:55,040
lonely. When I was young, I was very lonely. So it may be unfair to say. So I got

261
00:24:55,040 --> 00:25:02,520
over loneliness at an early age. Maybe dealing with loneliness. Well, loneliness is

262
00:25:02,520 --> 00:25:09,160
silly, right? I mean, there's no rational reason to be lonely. Loneliness is a

263
00:25:09,160 --> 00:25:15,520
desire. It's based on the desire for friendship, for worth, you know, having

264
00:25:15,520 --> 00:25:20,840
people esteem you, having people, I think highly enough of you to want to be

265
00:25:20,840 --> 00:25:25,960
your friends, that kind of thing. Wanting friends, wanting recognition,

266
00:25:25,960 --> 00:25:36,280
wanting people around you. I think it just comes from low self-esteem, maybe in

267
00:25:36,280 --> 00:25:42,880
cases. So I mean, these are all states of mind that you can deal with

268
00:25:42,880 --> 00:25:46,400
through meditation. There's nothing special about them. You just practice

269
00:25:46,400 --> 00:25:50,920
according to the booklet that we have up at the top and you'll be less

270
00:25:50,920 --> 00:25:55,920
plagued with loneliness. I think I guess another thing is people seem to think

271
00:25:55,920 --> 00:26:00,320
that loneliness is valid. If you're lonely, that's a valid emotion that you

272
00:26:00,320 --> 00:26:07,000
should remedy by getting friends or something. It's not that case. People are

273
00:26:07,000 --> 00:26:13,960
thinking that being alone is a sign of problems is a sign of defect, some sort.

274
00:26:13,960 --> 00:26:19,760
Why are you alone? Why are you so alone? If you lived like that, someone said to

275
00:26:19,760 --> 00:26:26,440
me once, a monkey was, we were arguing. He was very upset at me and he said,

276
00:26:26,440 --> 00:26:31,400
if you continue like this, you're going to be alone. That was kind of funny

277
00:26:31,400 --> 00:26:35,040
because like, well, didn't the Buddha tell us that we're supposed to be alone? So

278
00:26:35,040 --> 00:26:38,800
I was like, thank you. Thank you.

279
00:26:40,080 --> 00:26:45,280
Oh, wouldn't that be wonderful? It was also, it was a funny, like looking back,

280
00:26:45,280 --> 00:26:51,280
it was a funny. I was, I was quite shaking out of here. He's yelling at me. He said,

281
00:26:51,280 --> 00:26:56,440
you need to have love. He's a mate. I use it with me. There's a meditation teacher

282
00:26:56,440 --> 00:27:07,920
has to have a mate that I was scolding with. Okay. There's a funny conversation. He

283
00:27:07,920 --> 00:27:20,120
was quite angry actually. My mother experience is pain, but as yet does not

284
00:27:20,120 --> 00:27:27,040
practice meditation or fully understands. I got to deal with something about

285
00:27:27,040 --> 00:27:34,400
this scroll scrolling. It is not understand the four normal truths eight fold

286
00:27:34,400 --> 00:27:43,320
path. How can I help her without pushing Buddhism meditation? Well, all you

287
00:27:43,320 --> 00:27:46,880
can do is tell her that it helps. Tell her that meditation is something that

288
00:27:46,880 --> 00:27:51,000
could help her with her pain. It's up to her if she decides that that's worth

289
00:27:51,000 --> 00:27:55,320
her time. You know, you're not pushing things. If she doesn't want to do it,

290
00:27:55,320 --> 00:28:00,280
you know, fine power to her. We're not on this earth to make everyone meditate.

291
00:28:00,280 --> 00:28:06,280
It's not your job to make her meditate. You say you provide a, like you

292
00:28:06,280 --> 00:28:11,440
provide a therapy option. It's up to her if she's going to take it. You open

293
00:28:11,440 --> 00:28:17,240
the door to her. That's how it works. I know as your mother, it's often we

294
00:28:17,240 --> 00:28:22,080
want them to practice and wish that they would practice, but it's not how it

295
00:28:22,080 --> 00:28:26,640
works. Wishing doesn't make other people want to practice makes them actually

296
00:28:26,640 --> 00:28:35,520
turned away from it. I noticed that you no longer wear glasses. I got the

297
00:28:35,520 --> 00:28:42,280
laser surgery when I was in Thailand. Is that wrong? Well, in my defense, many monks

298
00:28:42,280 --> 00:28:46,680
in Thailand got it done. It's a thing in Thailand. Forest monks, especially

299
00:28:46,680 --> 00:28:51,760
because when you're in the forest, its glasses can be awkward. If you lose your

300
00:28:51,760 --> 00:28:57,400
glasses in the forest, it's dangerous snakes and so on. So I thought, well, why not

301
00:28:57,400 --> 00:29:03,000
get rid of it once and for all. So I got the laser surgery in Bangkok. I know it

302
00:29:03,000 --> 00:29:25,320
was an expense, but my lay, my lay supporters paid for him. When I meditate, I

303
00:29:25,320 --> 00:29:30,960
feel the left side of my brain is not working like having lack of energy. Is

304
00:29:30,960 --> 00:29:37,080
this mental or body pain? Doesn't sound like pain at all. Sounds like a physical

305
00:29:37,080 --> 00:30:05,320
sensation. Feeling, then you would say feeling, feeling. Right, Fernando, not noticing

306
00:30:05,320 --> 00:30:09,560
things properly. So that's it. It's easy to see the physical ones. Without a

307
00:30:09,560 --> 00:30:18,000
teacher, there's a lot that you miss. A teacher often keep you in line. So you

308
00:30:18,000 --> 00:30:21,200
can see how that works. I just reminded you and it helps you to see things,

309
00:30:21,200 --> 00:30:27,760
points out things. Yes, yes. I'm not being completely clear in my mind. So it helps

310
00:30:27,760 --> 00:30:38,080
you see things. You may be missed. Yeah, this was a nice session, lots of

311
00:30:38,080 --> 00:30:46,920
questions. Hope we keep this up. Tomorrow I'm off, I think. No, did I say I'll be

312
00:30:46,920 --> 00:30:53,000
back in time? Maybe I will be back in time. Let me see. I can find my schedule

313
00:30:53,000 --> 00:31:04,360
along. Arrives at 6 p.m. and that is Eastern time. So hey, is anyone in Toronto

314
00:31:04,360 --> 00:31:09,640
want to give me a ride back to Hamilton? I don't know how I'm going to get back

315
00:31:09,640 --> 00:31:14,840
to Hamilton. That's the thing. Maybe I'm walking. Maybe I'm, what am I going to

316
00:31:14,840 --> 00:31:22,360
do in Toronto tomorrow? I don't have a ride yet. Anyone, is anyone in Toronto?

317
00:31:22,360 --> 00:31:33,120
Nope. Doesn't look like it. Do you have any, one evening in Canada here? Mason? No.

318
00:31:33,120 --> 00:31:44,360
Well, I may have to get in touch with someone in Toronto, but it's my guess that I

319
00:31:44,360 --> 00:31:50,360
will be back in time for nine o'clock tomorrow evening. Hopefully eight, so I

320
00:31:50,360 --> 00:31:56,760
can actually do meditation with everybody. Mm-hmm. Shouldn't be. If I get in

321
00:31:56,760 --> 00:32:09,720
it, was that six? I'm six. Six. Then I have seven, eight. Yeah, be cutting it

322
00:32:09,720 --> 00:32:20,840
close, but I should get there for eight. Anyway, I think that's it for tonight.

323
00:32:20,840 --> 00:32:26,440
No, if you have more questions, save them for tomorrow. Thank you all for tuning in.

324
00:32:26,440 --> 00:32:42,360
Have a good night.

