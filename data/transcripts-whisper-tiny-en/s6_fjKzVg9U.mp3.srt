1
00:00:00,000 --> 00:00:17,380
okay good evening so another question tonight and that we are looking at the

2
00:00:17,380 --> 00:00:30,220
topic of suicide. Someone asking for help.

3
00:00:30,220 --> 00:00:47,860
Asking for help they are having thoughts of suicide and they don't know where to

4
00:00:47,860 --> 00:01:06,580
turn. So two things to say about suicide. I'm going to structure this talk.

5
00:01:06,580 --> 00:01:14,020
It shouldn't be that long but when I will say I'll say two different things and

6
00:01:14,020 --> 00:01:21,220
then in each of those I have a few things to say. So the first thing to say about

7
00:01:21,220 --> 00:01:36,740
suicide is it doesn't fix the problem. People commit to a side because they

8
00:01:36,740 --> 00:01:42,700
believe that it's going to end suffering. It's going to fix the problem. The problem

9
00:01:42,700 --> 00:01:48,500
is suffering and this is a generalization. This video I don't think it's going

10
00:01:48,500 --> 00:01:52,980
to be applicable to all forms of suicide. Obviously there's many different

11
00:01:52,980 --> 00:02:01,480
reasons but the common one probably the most common in think. No suffering and

12
00:02:01,480 --> 00:02:09,460
that's general so it covers a lot of types of suicide or reasons for committing

13
00:02:09,460 --> 00:02:17,300
suicide. Suicide is something that we think will fix the problem and so from a

14
00:02:17,300 --> 00:02:22,780
from a Buddhist perspective I think it's not hard to guess where I'm going to

15
00:02:22,780 --> 00:02:31,460
go with this. The very simple answer to why suicide doesn't fix the problem it

16
00:02:31,460 --> 00:02:42,260
does. The problem doesn't fix the problem it intends to or it seeks to. It's

17
00:02:42,260 --> 00:02:56,540
because death isn't is in the end. That's a basic Buddhist tenet. But there's

18
00:02:56,540 --> 00:03:03,540
something more important to say than that. It's much more important than

19
00:03:03,540 --> 00:03:12,420
simply the fact that suicide doesn't end your problems. If we say okay everyone

20
00:03:12,420 --> 00:03:20,420
when they die they're reborn again. If you haven't become a fully enlightened

21
00:03:20,420 --> 00:03:28,060
being we're going to be reborn again. So yes it didn't fix your problem because

22
00:03:28,060 --> 00:03:37,900
you thought well the fix is going to be not living. Don't surprise you've got

23
00:03:37,900 --> 00:03:42,260
to live again right. But no it's much worse than that and the real problem with

24
00:03:42,260 --> 00:03:46,020
suicide isn't simply that you have to be reborn again. In that case it would

25
00:03:46,020 --> 00:03:52,260
actually make a lot of sense still right. If you have a debilitating illness if

26
00:03:52,260 --> 00:03:59,620
you're in great suffering you think well I'll just hit the reset button. It's

27
00:03:59,620 --> 00:04:06,180
like when you call the computer a pair of person. Something's wrong with

28
00:04:06,180 --> 00:04:09,980
your router. Something's wrong with your machine. What did I tell you? Did I try

29
00:04:09,980 --> 00:04:14,980
turning it off and on again? Just do that and then it'll fix your problems. It

30
00:04:14,980 --> 00:04:20,860
seems reasonable. I think a lot of people commit suicide even even with that

31
00:04:20,860 --> 00:04:29,860
sense. You hear about Buddhists who commit suicide with that sense. But it's

32
00:04:29,860 --> 00:04:35,140
highly problematic in I would say almost all cases. At the end all I think at

33
00:04:35,140 --> 00:04:45,700
the end I'll talk about one interesting case. Maybe I'll talk about it in the

34
00:04:45,700 --> 00:04:52,020
section anyway. Three reasons. Three factors. Three points to make about why

35
00:04:52,020 --> 00:04:58,100
suicide is especially problematic in a bad idea if you're trying to end

36
00:04:58,100 --> 00:05:09,620
suffering. The first is that of course suicide itself is a negative act. It's

37
00:05:09,620 --> 00:05:13,380
based on a negative perspective and again we're generalizing but generally

38
00:05:13,380 --> 00:05:27,660
speaking suicide is is thought of is considered from a perspective of

39
00:05:27,660 --> 00:05:37,020
negativity. You know disliking a version is the root mental factor.

40
00:05:37,020 --> 00:05:50,220
But you don't like something or you're afraid of something. And so engaging and

41
00:05:50,220 --> 00:06:04,980
accepting affirming that quality of mind has a defiling quality.

42
00:06:04,980 --> 00:06:13,060
It increases in the impurity in the mind in these two increased

43
00:06:13,060 --> 00:06:19,820
corruption in the mind. Meaning the time we take to consider to work ourselves

44
00:06:19,820 --> 00:06:32,300
up to killing ourselves is the acceptance and aligning ourselves with negative

45
00:06:32,300 --> 00:06:36,060
mind states with corrupt mind states with mind states that are averse to

46
00:06:36,060 --> 00:06:39,700
things. Now a version if you're not familiar it's a big problem in Buddhism

47
00:06:39,700 --> 00:06:50,300
because it's an unpleasant reaction to an experience and by encouraging it

48
00:06:50,300 --> 00:06:57,900
by augmenting it by reaffirming it. You become more averse to the object that

49
00:06:57,900 --> 00:07:03,260
you don't like. Meaning the next time it comes and every time it comes you

50
00:07:03,260 --> 00:07:07,100
and you become increasingly averse to it. It's why headaches can become

51
00:07:07,100 --> 00:07:10,940
debilitating. At first it's a headache. Second time you get it when you have

52
00:07:10,940 --> 00:07:16,180
chronic headaches it can become something that as soon as the slightest pain

53
00:07:16,180 --> 00:07:22,620
comes you're moaning and rolling on the floor or can't get out of bed and

54
00:07:22,620 --> 00:07:28,500
that sort of thing. Not because of the pain actually but because of your built up

55
00:07:28,500 --> 00:07:33,100
aversion to it. Now that goes with everything. Building up aversion, building up

56
00:07:33,100 --> 00:07:39,300
desire, building up arrogance, conceit all these things. They're habit forming

57
00:07:39,300 --> 00:07:47,020
and so you're forming a habit when you do this. The second point is that suicide

58
00:07:47,020 --> 00:07:55,860
itself as an act when you actually go through with it is destabilizing. It's

59
00:07:55,860 --> 00:08:03,860
unnatural. It's violent. No matter how sort of peaceful you might make it

60
00:08:03,860 --> 00:08:08,620
there are ways now even in Ontario, Canada where I'm from. Apparently you can

61
00:08:08,620 --> 00:08:15,460
actually take an injection you fall asleep and so on but it's violent even if

62
00:08:15,460 --> 00:08:21,780
done sort of peacefully because it's a disruption. You know there are two

63
00:08:21,780 --> 00:08:26,460
ways to die and they are distinct. One is your life force is cut off by suicide

64
00:08:26,460 --> 00:08:31,620
murder and accident that sort of thing and the other is you come to an end of

65
00:08:31,620 --> 00:08:41,140
your life force. Sort of your karma being born, the cause of whatever it

66
00:08:41,140 --> 00:08:49,660
was that led you to be to be born as worn itself out and when that does you

67
00:08:49,660 --> 00:08:53,740
die and that's quite natural and you find you hear about people in hospices who

68
00:08:53,740 --> 00:08:58,540
die this way. It may be in pain maybe not but they die naturally and there's

69
00:08:58,540 --> 00:09:03,900
sort of a closure involved. The suicide doesn't give you that suicide is a

70
00:09:03,900 --> 00:09:10,700
disruption, an artificial death and so it's disruptive. It's it's

71
00:09:10,700 --> 00:09:20,900
destabilizing and the result of both of these. The third factor is that not

72
00:09:20,900 --> 00:09:25,900
only are you reborn again but because of the way that you've left this world,

73
00:09:25,900 --> 00:09:31,500
the way that you've left this life, you're much more likely to be reborn in a

74
00:09:31,500 --> 00:09:38,860
bad place to be reborn to continue on in a bad way. Whatever comes to you in

75
00:09:38,860 --> 00:09:43,140
the future is probably not going to be favorable. Why? Because you've spent

76
00:09:43,140 --> 00:09:51,340
time and energy cultivating your aversion and delusion usually because

77
00:09:51,340 --> 00:09:57,820
there's usually a belief that there's nothing else. So you create this

78
00:09:57,820 --> 00:10:03,140
view that you found closure and you've had enough and you're going into oblivion

79
00:10:03,140 --> 00:10:15,140
there'll be nothing else. Which is it's kind of like being suddenly caught off

80
00:10:15,140 --> 00:10:22,340
guard because you you think it's over. If you die thinking I don't have to

81
00:10:22,340 --> 00:10:25,580
face anything then everything all the images that come when you die your

82
00:10:25,580 --> 00:10:29,580
life flashing before you eyes once the body ceases and all the mental activity

83
00:10:29,580 --> 00:10:34,300
that continues. Usually it's quite chaotic unless you've done some serious

84
00:10:34,300 --> 00:10:38,540
meditation is going to be a lot of different things come here. You want me

85
00:10:38,540 --> 00:10:41,980
prepared for that you're like wait I thought it was over. What's this? Okay

86
00:10:41,980 --> 00:10:46,420
maybe it'll maybe it'll end now. Well it's not ending what's going on and it

87
00:10:46,420 --> 00:10:50,820
can be it's quite overwhelming right? Your rebirth will be very you'll be

88
00:10:50,820 --> 00:10:57,260
very unprepared for your rebirth. Because you've cultivated negativity

89
00:10:57,260 --> 00:11:05,700
because you've violently sort of disrupted the flow of things. So that's the

90
00:11:05,700 --> 00:11:08,860
first thing I would say about suicide. It doesn't fix the problem it's not a

91
00:11:08,860 --> 00:11:20,580
very good solution. The second thing I would say second two things and any of

92
00:11:20,580 --> 00:11:26,100
you have heard me say this thing this sort of thing before but suicide is a

93
00:11:26,100 --> 00:11:35,340
prime example of engaging in the very perspective of trying to find

94
00:11:35,340 --> 00:11:42,980
solutions to problems. Solutions to problems is not the teachings of the

95
00:11:42,980 --> 00:11:48,060
Buddha. Now I'm giving my understanding of the Buddha's teaching and you

96
00:11:48,060 --> 00:11:51,660
probably won't find the Buddha say it exactly like I say it but based on my

97
00:11:51,660 --> 00:11:57,300
study of the Buddha's teaching it's quite clear. Problems and solutions is part

98
00:11:57,300 --> 00:12:02,420
of the problem. Our perspective of things this is a problem I want to fix it

99
00:12:02,420 --> 00:12:08,460
escape it or hold on to it find a way to fix it in place so it doesn't go

100
00:12:08,460 --> 00:12:20,100
away. All of this trying to make good or maintain good. Our based on reaction

101
00:12:20,100 --> 00:12:29,500
are based on liking, disliking, are based on ego, me and mine. This perspective

102
00:12:29,500 --> 00:12:36,540
I almost want to say ontology but it's more than that. Our way of looking at

103
00:12:36,540 --> 00:12:42,380
the world. The way we see things, it's kind of an ontology, it's how what

104
00:12:42,380 --> 00:12:49,700
exists. Problems and solutions. Ontology deals with frameworks of

105
00:12:49,700 --> 00:12:56,820
existence, the way we understand things that exist. So a Buddhist ontology

106
00:12:56,820 --> 00:13:01,500
is not that there are problems and we have to fix them. Practically speaking

107
00:13:01,500 --> 00:13:05,420
that's how most of us live. Everything is a problem in a solution. It's suicide

108
00:13:05,420 --> 00:13:13,300
there's a problem I'm depressed. I'm in physical pain or whatever. Physical

109
00:13:13,300 --> 00:13:20,140
pain, mental pain. Usually one or the other. I got to fix it. So we take on the

110
00:13:20,140 --> 00:13:29,020
perspective that not in Buddhism, ordinary people take on the perspective that

111
00:13:29,020 --> 00:13:41,980
there are problems and we have to find some way to fix them. And so what we

112
00:13:41,980 --> 00:13:50,020
fail to see is that the very basis of that is not that there is a problem. It's

113
00:13:50,020 --> 00:13:54,580
that there is a thing in fact an experience and that's the Buddhist way of

114
00:13:54,580 --> 00:14:01,100
understanding it. And we perceive it as a problem. And we miss that step. We

115
00:14:01,100 --> 00:14:05,700
miss that aspect of it. We say there's a problem. Well, there's an experience

116
00:14:05,700 --> 00:14:10,540
and you perceive it as a problem. Now objectively someone can look and say,

117
00:14:10,540 --> 00:14:14,460
oh yeah, that's a problem. And by the way we define problem, we could see it as a

118
00:14:14,460 --> 00:14:23,300
problem. But seeing things as problems is problematic. It's it's it's not a

119
00:14:23,300 --> 00:14:28,260
good way to to see things regardless of how you define problem and say there is

120
00:14:28,260 --> 00:14:31,940
a problem. There isn't a problem. It's not about what is. It's about your

121
00:14:31,940 --> 00:14:41,460
perspective. So once you have the perspective that something is a problem, you

122
00:14:41,460 --> 00:14:45,300
have two choices. You know, let's make it simple. We don't realize this. We say

123
00:14:45,300 --> 00:14:49,500
there's one choice. You have a problem. I see this as a problem. I see the

124
00:14:49,500 --> 00:14:55,380
problem. I have to fix it. Make it so that that thing that you perceive as a

125
00:14:55,380 --> 00:15:01,620
problem no longer exists is no longer that right in some way shape or form or

126
00:15:01,620 --> 00:15:08,900
is no longer the way it is. It's no longer problematic. The other option that

127
00:15:08,900 --> 00:15:15,260
the Buddha really pointed out is to change our perception. So a Buddhist

128
00:15:15,260 --> 00:15:23,420
ontology is not seeing problem. It is really about stopping to see problems

129
00:15:23,420 --> 00:15:32,740
and solutions and to start seeing experiences and understanding or

130
00:15:32,740 --> 00:15:37,460
perspectives, let's say. The understanding is going to be a good word the way

131
00:15:37,460 --> 00:15:41,460
we understand something. And as the point we have the perspective that it's a

132
00:15:41,460 --> 00:15:47,940
problem, that's how we understand it. Rather than problems and solutions that

133
00:15:47,940 --> 00:15:55,060
are experiences and understandings. And so understanding is a very, it's the

134
00:15:55,060 --> 00:16:01,660
basis of the Buddhist practice. We try and understand things because our

135
00:16:01,660 --> 00:16:06,620
understanding that something's a problem is actually a misunderstanding. It's

136
00:16:06,620 --> 00:16:12,300
or it's a problematic understanding. It's an understanding that leads to

137
00:16:12,300 --> 00:16:17,260
suffering for the reasons stated. It increases negativity. It glades to

138
00:16:17,260 --> 00:16:24,060
violent acts and it ultimately leads to greater suffering in the future.

139
00:16:24,060 --> 00:16:33,100
So meditation practice is very much mindfulness practice is very much

140
00:16:33,100 --> 00:16:38,820
about changing the way we look at things. So that when there is something that

141
00:16:38,820 --> 00:16:44,580
we perceive as a problem, we change the way we perceive it. Because seeing

142
00:16:44,580 --> 00:16:50,220
things as problems doesn't lead to a better life. It doesn't lead to peace

143
00:16:50,220 --> 00:16:56,180
happiness. It doesn't lead to prosperity. It just leads to greater stress and

144
00:16:56,180 --> 00:17:02,180
suffering. It's not that you know you might think well it's easy for you to

145
00:17:02,180 --> 00:17:05,540
say my life sucks. You know I have all these terrible things. I'm in debt. I'm

146
00:17:05,540 --> 00:17:12,380
sick. I have cancer. I'm dying. Now I can't change any of that. I don't

147
00:17:12,380 --> 00:17:17,180
know my answer to fix all of those things to change your situation. But I can

148
00:17:17,180 --> 00:17:24,260
say that much perhaps all of the problems that we face in the long term,

149
00:17:24,260 --> 00:17:28,380
life after life after life, are based on our reactions based on our

150
00:17:28,380 --> 00:17:33,940
negativity based on our defilements, our corruptions of mind. Even in this

151
00:17:33,940 --> 00:17:37,580
life you can see it. Take a cancer patient. Someone is dying. Let's say they're

152
00:17:37,580 --> 00:17:48,500
stage four untreatable cancer. They have, for his choices, let's say one choice

153
00:17:48,500 --> 00:17:54,420
they have is to be mindful and mindfulness will bring them peace. It won't

154
00:17:54,420 --> 00:17:59,300
cure their cancer not likely. I don't know. I think I think there are perhaps

155
00:17:59,300 --> 00:18:05,140
strange odd cases where cancer is somehow disappears or something through

156
00:18:05,140 --> 00:18:11,260
meditation. But I think it's probably very very rare and strange. But for the

157
00:18:11,260 --> 00:18:17,100
most part, no it's not about that. It's that the alternative is to moan and

158
00:18:17,100 --> 00:18:26,180
wail and maybe kill yourself. But to be mindful, if you choose to be mindful

159
00:18:26,180 --> 00:18:33,380
you can actually free yourself from the suffering of even great pain. Take

160
00:18:33,380 --> 00:18:41,220
someone who is poor in debt, lost their job and so on. You know, meditation is

161
00:18:41,220 --> 00:18:50,340
not going to make it rain gold or jewels for money. But it can very well help

162
00:18:50,340 --> 00:18:54,140
you see gain a better perspective, a more objective perspective on your

163
00:18:54,140 --> 00:19:01,620
problems. And it can help you organize your mind to become more capable. I'm

164
00:19:01,620 --> 00:19:10,380
getting a job of educating yourself, of finding a way to survive. But ultimately

165
00:19:10,380 --> 00:19:17,140
it comes down to the same thing. The very base is it helps you interact with

166
00:19:17,140 --> 00:19:26,060
reality in a more peaceful, wholesome, beneficial way. So I guess a third

167
00:19:26,060 --> 00:19:31,660
thing that I would say, and let's just sort of sum this all up, is that people

168
00:19:31,660 --> 00:19:39,940
who have suicidal ideation really are just looking at things wrong. It sounds

169
00:19:39,940 --> 00:19:48,500
right. It sounds like an easy thing to say. But I would say most people who are

170
00:19:48,500 --> 00:19:54,020
people who commit suicide because of their incapacity to deal with their

171
00:19:54,020 --> 00:19:59,900
problems are generally overreacting. I mean, I guess you could say they're

172
00:19:59,900 --> 00:20:04,820
from Buddhist perspective, everyone's overacting. But I mean to say is things

173
00:20:04,820 --> 00:20:15,260
like depression, even physical pain are actually not nearly as problematic

174
00:20:15,260 --> 00:20:20,700
as we think they are. It's quite magical how you're able to change your

175
00:20:20,700 --> 00:20:24,620
perspective on things so that depression is no longer from anxiety is no

176
00:20:24,620 --> 00:20:28,580
longer a problem. Fear is no longer a problem. Doesn't mean they don't come up

177
00:20:28,580 --> 00:20:34,340
anymore. It means you're able to, in the beginning, see them just as fear, just

178
00:20:34,340 --> 00:20:38,700
as anxiety, just as depression. Rather than seeing them as a problem that

179
00:20:38,700 --> 00:20:41,860
needs a solution, you see them as an experience and you try to learn to

180
00:20:41,860 --> 00:20:47,580
understand them. And when you change that to be your paths, everything gets

181
00:20:47,580 --> 00:20:52,580
better. Eventually they go away. I mean, it does fix your problems. But because

182
00:20:52,580 --> 00:20:55,700
you change your perspective, you stop seeing them as problems. You stop

183
00:20:55,700 --> 00:21:01,300
focusing in that way. Then you see pain as pain, you see depression as

184
00:21:01,300 --> 00:21:17,100
depression. I can say from experience that it's quite surprising how we

185
00:21:17,100 --> 00:21:22,420
amplify our problems, how amplified they are in our minds, where things just seem

186
00:21:22,420 --> 00:21:27,220
like there is no way. And then you just switch and suddenly your mind

187
00:21:27,220 --> 00:21:35,740
is just an experience. It's suddenly no longer a problem. Mindfulness is like

188
00:21:35,740 --> 00:21:45,220
water. It just washes it all away, neutralizes everything. So I guess as with

189
00:21:45,220 --> 00:21:49,140
everything else, people have suicidal ideation, shouldn't practice mindfulness

190
00:21:49,140 --> 00:21:55,100
meditation. But it should give a fairly good framework as to why it's

191
00:21:55,100 --> 00:22:00,260
especially applicable to people who have thoughts of suicide. Don't commit suicide.

192
00:22:00,260 --> 00:22:06,660
It's really dumb idea. The last thing I would say, so I hinted at there was one

193
00:22:06,660 --> 00:22:15,940
thing I wanted to say. It's that there is one case where suicide, one way you

194
00:22:15,940 --> 00:22:22,180
could think of. No, I don't even want to say that. One example of how one might

195
00:22:22,180 --> 00:22:28,980
think that suicide was somehow beneficial. It's a very roundabout way. It's

196
00:22:28,980 --> 00:22:34,260
that when you, it's happened, it apparently happened, that a monk tried to kill

197
00:22:34,260 --> 00:22:39,660
himself and he slit his throat. He slit his throat and died and then they asked

198
00:22:39,660 --> 00:22:42,260
the Buddha to say, oh, where was he born? Must have been a bad place in the

199
00:22:42,260 --> 00:22:46,580
Buddha said he wasn't reborn. He was in Iran. And it's a very confusing case

200
00:22:46,580 --> 00:22:51,140
because people say, oh, wow, Iran can kill themselves. That's not the case. He

201
00:22:51,140 --> 00:22:56,860
cut his throat and then he got so freaked out because he realized he was just an

202
00:22:56,860 --> 00:23:00,460
ordinary human being. He hadn't came to anything and that he was going to go to

203
00:23:00,460 --> 00:23:06,820
hell. As he was dying, all these bad thoughts overcame him and he just, well, to some

204
00:23:06,820 --> 00:23:13,740
extent, he freaked out. But he, more importantly, he pulled himself together as he

205
00:23:13,740 --> 00:23:20,900
was dying and he became enlightened as he was dying. But the point is because he

206
00:23:20,900 --> 00:23:25,220
saw how bad suicide was. And the only reason he was able to do it is because he

207
00:23:25,220 --> 00:23:30,140
done quite a bit of meditation and advance. So I think, I wanted to add that,

208
00:23:30,140 --> 00:23:34,780
the only reason I want to add it is because I think people might misunderstand

209
00:23:34,780 --> 00:23:40,500
in that way. Some Buddhist meditators might read that or hear about that or

210
00:23:40,500 --> 00:23:49,500
think about that. Maybe if I kill myself, I will somehow it will lead to enlightenment

211
00:23:49,500 --> 00:23:53,180
or something like that. And certainly not the case is the exact opposite. Maybe

212
00:23:53,180 --> 00:23:57,700
when you start to kill yourself, you'll realize what a dumb idea it is and maybe

213
00:23:57,700 --> 00:24:04,140
you'll stop yourself hopefully. Maybe you won't and by some chance you've done an

214
00:24:04,140 --> 00:24:09,700
intense and sufficient amount of cultivation of wholesome states that you will

215
00:24:09,700 --> 00:24:15,420
in the moments of death be able to become enlightened. But I would say it's the

216
00:24:15,420 --> 00:24:21,260
odds or not in your favor. And you're much more likely to pass away and

217
00:24:21,260 --> 00:24:26,820
miss the opportunity to be reborn as a human. I think something just as an

218
00:24:26,820 --> 00:24:32,540
addendum that Buddhist will often say is that you're wasting this precious

219
00:24:32,540 --> 00:24:38,700
opportunity. Things like efficient position assisted suicide even potentially

220
00:24:38,700 --> 00:24:49,100
even DNR do not resuscitate orders. Not because living in a coma without any

221
00:24:49,100 --> 00:24:56,140
brain activity is of any use, but because you just never know. And so I

222
00:24:56,140 --> 00:25:04,740
don't want to put a definite statement on things like DNR's and that sort of

223
00:25:04,740 --> 00:25:11,340
thing. But just that we shouldn't take them lightly and we shouldn't take

224
00:25:11,340 --> 00:25:16,940
lightly the human birth. And then we have this opportunity now. The Buddha's

225
00:25:16,940 --> 00:25:21,620
teaching is still here. As long as the eight full noble path is here, the Buddha

226
00:25:21,620 --> 00:25:29,180
said there will be Sotapana, Sakata, Kamiyana, Kamiyara. And as long as the

227
00:25:29,180 --> 00:25:37,900
week we said he made Chabikhu, Sammawi Harayum. As long as they live, as long as they

228
00:25:37,900 --> 00:25:49,500
should live, well, practice, well, Sammawi Harayum. Asunyuloko Arahandeeh. The world

229
00:25:49,500 --> 00:25:56,700
will not be devoid of Arahans. So it's really just all about practicing well.

230
00:25:56,700 --> 00:26:01,180
We have this opportunity. The Buddha's teaching is here, even though the Buddha isn't

231
00:26:01,180 --> 00:26:09,260
here. And we shouldn't take it lightly. So that's some thoughts on suicide.

232
00:26:09,260 --> 00:26:35,420
Thank you all for tuning in and listening. I'm going to show you the best.

