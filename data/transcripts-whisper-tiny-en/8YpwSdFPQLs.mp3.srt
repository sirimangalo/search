1
00:00:00,000 --> 00:00:09,000
Manuel says, if I close the eyes while meditating on in the evening, I enter in a dreamy state.

2
00:00:09,000 --> 00:00:12,000
Well, if I keep them open, the mind is quite clear.

3
00:00:12,000 --> 00:00:21,000
So the question is, I should experience the dreamy statement risking to fall asleep or keep them open to profit of the more mind clarity.

4
00:00:21,000 --> 00:00:24,000
It's an interesting question.

5
00:00:24,000 --> 00:00:26,000
I mean, you have an argument there.

6
00:00:26,000 --> 00:00:32,000
The question really is why that is the case.

7
00:00:32,000 --> 00:00:36,000
And I suppose the answer might be that it's just habit.

8
00:00:36,000 --> 00:00:37,000
Your habit of mind.

9
00:00:37,000 --> 00:00:42,000
Your habit is that when you close your eyes, your mind automatically says, okay, time to sleep.

10
00:00:42,000 --> 00:00:44,000
Oh, he wants to sleep now.

11
00:00:44,000 --> 00:00:46,000
Because it thinks you want to sleep.

12
00:00:46,000 --> 00:00:48,000
So it's just a conditioning.

13
00:00:48,000 --> 00:00:51,000
So in the beginning, yeah, sure.

14
00:00:51,000 --> 00:00:56,000
Do whatever works for you if you want to keep your eyes open, keep your eyes open.

15
00:00:56,000 --> 00:01:05,000
I think the point is that eventually, especially if you're in a meditation retreat or if you're in a meditation center,

16
00:01:05,000 --> 00:01:07,000
you won't need to do that.

17
00:01:07,000 --> 00:01:12,000
You'll find that closing your eyes is actually much better because it shuts off one sense.

18
00:01:12,000 --> 00:01:16,000
You only have to focus now on the stomach.

19
00:01:16,000 --> 00:01:19,000
The problem with the eyes, I guess, is that we use them so much.

20
00:01:19,000 --> 00:01:27,000
So keeping them open is, it's a great danger for our focus.

21
00:01:27,000 --> 00:01:33,000
Our mind will be too distracted, but you certainly can keep them open.

22
00:01:33,000 --> 00:01:37,000
There's really no problem with that.

23
00:01:37,000 --> 00:01:46,000
But you also might decide that you want to close them and fight with the drowsiness and alter that conditioning.

24
00:01:46,000 --> 00:01:52,000
And, you know, look at the state of attachment that's involved there.

25
00:01:52,000 --> 00:01:59,000
I don't know, it could be anything, but certainly you have an argument and there's nothing wrong with keeping your eyes open.

26
00:01:59,000 --> 00:02:07,000
Oh, the other thing I was going to say is that what you really should do when you feel tired is try to get up and do some walking meditation.

27
00:02:07,000 --> 00:02:10,000
Because walking meditation will wake you up.

28
00:02:10,000 --> 00:02:12,000
If you're working during the day, it might be difficult.

29
00:02:12,000 --> 00:02:17,000
You might feel like you don't have quite the energy for it, so you can go either way.

30
00:02:17,000 --> 00:02:21,000
But that's one standard answer that we would give if you're falling asleep.

31
00:02:21,000 --> 00:02:23,000
Don't do sitting at all.

32
00:02:23,000 --> 00:02:46,000
Get up and do walking meditation.

