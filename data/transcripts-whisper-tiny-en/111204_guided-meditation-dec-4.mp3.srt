1
00:00:00,000 --> 00:00:19,000
So I'm going to talk about the meditation that I practice, and this is kind of important

2
00:00:19,000 --> 00:00:26,000
to disclaim in the beginning because there are obviously many different kinds of meditation.

3
00:00:26,000 --> 00:00:31,000
So it may not be the same as the meditation that some of you practice.

4
00:00:31,000 --> 00:00:38,000
It may not be the same as any of you practice, but I'd ask that you keep an open mind,

5
00:00:38,000 --> 00:00:50,000
give it a try, and see what it does for you, see how it goes for you.

6
00:00:50,000 --> 00:01:04,000
In order to understand the meditation that I'm going to explain to you,

7
00:01:04,000 --> 00:01:11,000
there's one thing that you need to be clear about is what we mean by the word meditation.

8
00:01:11,000 --> 00:01:27,000
It's a word that can mean many different things.

9
00:01:27,000 --> 00:01:37,000
But in the practice that I'm going to explain to you, meditation means to

10
00:01:37,000 --> 00:01:53,000
reflect in a sense or to examine and to look at and to learn about the objects of our experience.

11
00:01:53,000 --> 00:02:02,000
So it's not necessarily the case that when you begin to practice you'll feel peaceful and calm,

12
00:02:02,000 --> 00:02:13,000
or happy, or you'll experience some kind of special or super mundane state of awareness.

13
00:02:13,000 --> 00:02:16,000
That's not what we mean by meditation.

14
00:02:16,000 --> 00:02:20,000
I mean by meditation is to look at what's already there.

15
00:02:20,000 --> 00:02:33,000
The very mundane experiences that you will have on a moment-to-moment basis,

16
00:02:33,000 --> 00:02:44,000
just sitting here in the room and to learn about them and to come to understand

17
00:02:44,000 --> 00:02:55,000
both the stimulus that you receive from the outside world and your reactions to it.

18
00:02:55,000 --> 00:03:06,000
Because therein really lives all of our difficulties and all of the problems in our lives.

19
00:03:06,000 --> 00:03:15,000
And therein lives all of the good things in our lives as well.

20
00:03:15,000 --> 00:03:34,000
But the point of meditating is to allow us to adjust our reactions

21
00:03:34,000 --> 00:03:40,000
that when we experience something from the outside world or when we think of something

22
00:03:40,000 --> 00:03:47,000
that rather than reacting in a judgmental way we're able to see it clearly for what it is

23
00:03:47,000 --> 00:03:58,000
and react in a way based on wisdom and understanding.

24
00:03:58,000 --> 00:04:04,000
So in order to do this we're actually going to meditate on the experiences that we have.

25
00:04:04,000 --> 00:04:09,000
We're not going to try to create some new experience or some new state of being,

26
00:04:09,000 --> 00:04:17,000
apart from simply clear awareness of the reality around us.

27
00:04:17,000 --> 00:04:23,000
To do this we have a tool that we use.

28
00:04:23,000 --> 00:04:30,000
And it's a tool that should be well familiar to people who know something about meditation.

29
00:04:30,000 --> 00:04:34,000
This tool is called a mantra.

30
00:04:34,000 --> 00:04:50,000
Now a mantra is a word that is most often used for the purposes of focusing on some super mundane or spiritual object.

31
00:04:50,000 --> 00:05:03,000
But the point of the mantra is really the focus and the clarity of focus that comes from putting a word to an object.

32
00:05:03,000 --> 00:05:12,000
This is one way to understand the word mindfulness or satin.

33
00:05:12,000 --> 00:05:19,000
In Buddhism we talk about this word satin which we translate as mindfulness.

34
00:05:19,000 --> 00:05:24,000
But satin really means to remind yourself or to remember something for what it is.

35
00:05:24,000 --> 00:05:26,000
This is the meaning of the word.

36
00:05:26,000 --> 00:05:31,000
This is how it's used in meditative sense.

37
00:05:31,000 --> 00:05:43,000
So it means rather than extrapolating on the object or projecting our own ideas about the object.

38
00:05:43,000 --> 00:05:46,000
We simply see it's intrinsic qualities.

39
00:05:46,000 --> 00:05:48,000
We see it for what it is.

40
00:05:48,000 --> 00:05:52,000
When we see something we know we're seeing, when we hear something we know we're hearing.

41
00:05:52,000 --> 00:05:55,000
When we walk we know that we're walking.

42
00:05:55,000 --> 00:05:58,000
And that's all that we know.

43
00:05:58,000 --> 00:06:02,000
The idea is that we have a one-to-one relationship with reality.

44
00:06:02,000 --> 00:06:17,000
We experience reality as it is rather than judging it or extrapolating upon it.

45
00:06:17,000 --> 00:06:19,000
So that's what the mantra gives us.

46
00:06:19,000 --> 00:06:23,000
It reminds us of that essential quality of the experience.

47
00:06:23,000 --> 00:06:28,000
It keeps us from projecting on it.

48
00:06:28,000 --> 00:06:37,000
Giving rise to projections or judgments and so on.

49
00:06:37,000 --> 00:06:43,000
So we're going to apply this mantra on our everyday experience

50
00:06:43,000 --> 00:06:52,000
to allow us to actually focus on many things that we wouldn't otherwise have any interest in looking at.

51
00:06:52,000 --> 00:06:59,000
The negative emotions that we have, if we get angry, we're actually going to meditate on the anger.

52
00:06:59,000 --> 00:07:08,000
Because until we come to understand the nature of things like anger or greed or conceit or arrogance or so on,

53
00:07:08,000 --> 00:07:23,000
all these many negative emotions, then it won't really be possible for us to be free from them.

54
00:07:23,000 --> 00:07:32,000
There'll always be the opportunity for them to arise because we don't yet understand how truly negative they are.

55
00:07:32,000 --> 00:07:41,000
We may believe them to be negative intellectually.

56
00:07:41,000 --> 00:07:53,000
But once we know the truth of them, once we understand the truth of them, we won't give rise to them.

57
00:07:53,000 --> 00:08:00,000
And moreover, when we understand the truth about the experiences that we have,

58
00:08:00,000 --> 00:08:08,000
we won't have any reason to become greedy or angry or conceited and so on.

59
00:08:08,000 --> 00:08:15,000
When we see things for what they are, we won't have any reason to give rise to any negative emotion.

60
00:08:15,000 --> 00:08:21,000
We won't see things in terms of good and bad, me and mine.

61
00:08:21,000 --> 00:08:28,000
We'll see things in terms of how they are, in terms of their intrinsic qualities.

62
00:08:28,000 --> 00:08:37,000
And we'll be able to live our lives at peace in any situation.

63
00:08:37,000 --> 00:08:43,000
And this is the idea of this practice.

64
00:08:43,000 --> 00:08:50,000
So when we experience something that we'd normally consider to be bad, for instance, when we have pain in the body,

65
00:08:50,000 --> 00:09:01,000
we'll try to simply see it as pain and keep ourselves from judging it as bad.

66
00:09:01,000 --> 00:09:07,000
And slowly our mind will come to accept the fact that actually pain has nothing intrinsically bad about it.

67
00:09:07,000 --> 00:09:10,000
The bad comes with the disliking.

68
00:09:10,000 --> 00:09:19,000
And as we look at the disliking, we'll come to see that the disliking is really the bad useless, unbeneficial quality.

69
00:09:19,000 --> 00:09:22,000
So this is basically how we're going to practice.

70
00:09:22,000 --> 00:09:32,000
When something arises, we're going to have a mantra, a word that allows us to focus and to see it clearly just for what it is and to remain objective about it.

71
00:09:32,000 --> 00:09:36,000
So instead of thinking in our mind, this is good, this is bad, I like it, I don't like it.

72
00:09:36,000 --> 00:09:42,000
We're going to think to ourselves, this is this, it is what it is.

73
00:09:42,000 --> 00:09:48,000
So for instance, when we feel the pain, we'll just remind ourselves that that's pain.

74
00:09:48,000 --> 00:10:02,000
When we can say to ourselves, pain, pain, pain, just simply reminding ourselves, that's what that is.

75
00:10:02,000 --> 00:10:11,000
In the beginning, this can be quite a difficult skill to acquire something that doesn't come easily because we're so used to proliferating.

76
00:10:11,000 --> 00:10:18,000
We're so used to projecting and creating many, many different thoughts about every object that arises.

77
00:10:18,000 --> 00:10:30,000
It's very difficult to focus the mind in such a way as we just think of it as what it is.

78
00:10:30,000 --> 00:10:39,000
So in the beginning, we have an exercise that we give meditators to train themselves in this skill.

79
00:10:39,000 --> 00:10:52,000
And so probably many of you are familiar with breath meditation where we're going to use breath meditation to allow us to see the reality of our body and mind.

80
00:10:52,000 --> 00:10:59,000
But we can't exactly use the breath because the breath is just an idea that we have in our minds.

81
00:10:59,000 --> 00:11:05,000
Our experience is actually the sensations that arise in the body.

82
00:11:05,000 --> 00:11:13,000
The breath coming in and going out is just an idea that we have in our mind that arises based on the sensations.

83
00:11:13,000 --> 00:11:16,000
The truth is that we're going to have certain sensations in the body.

84
00:11:16,000 --> 00:11:22,000
Most prominent is going to be in the stomach.

85
00:11:22,000 --> 00:11:29,000
If you haven't practiced meditation or if you live a stressful life,

86
00:11:29,000 --> 00:11:35,000
you may not have any sensations in the stomach or in the chest.

87
00:11:35,000 --> 00:11:45,000
But when you begin to practice meditation, when you've been practicing for some time, you'll find that as your body relaxes, as your breath becomes more natural,

88
00:11:45,000 --> 00:11:49,000
the natural tendency is to breathe through the stomach.

89
00:11:49,000 --> 00:11:52,000
You can verify this by lying on your back sometimes.

90
00:11:52,000 --> 00:12:01,000
If you lie on your back you'll find that naturally the stomach rises and falls quite prominently during the breath.

91
00:12:01,000 --> 00:12:06,000
So we're going to take this as our introductory exercise.

92
00:12:06,000 --> 00:12:13,000
This rising and falling of the stomach, which is the result of the breath.

93
00:12:13,000 --> 00:12:21,000
And if in the beginning it's not easy to experience, you can put your hand on your stomach.

94
00:12:21,000 --> 00:12:25,000
We'll try to get a feeling for it.

95
00:12:25,000 --> 00:12:28,000
But it's an easy object because it's very clear what's going on.

96
00:12:28,000 --> 00:12:33,000
There's the rising motion and then there's the falling motion.

97
00:12:33,000 --> 00:12:38,000
And all we're going to try to do is to see the rising for what it is and see the falling for what it is.

98
00:12:38,000 --> 00:12:49,000
It's just an example of something that we can see clearly, teaching the mind that when an experience arises.

99
00:12:49,000 --> 00:12:53,000
Teaching it to just see it for what it is.

100
00:12:53,000 --> 00:12:56,000
This is rising.

101
00:12:56,000 --> 00:13:01,000
This is falling, not judging it, not thinking about it, not wondering about it.

102
00:13:01,000 --> 00:13:06,000
Just knowing it is what it is.

103
00:13:06,000 --> 00:13:09,000
So the mantra we use is just rising and falling.

104
00:13:09,000 --> 00:13:15,000
When the stomach rises, we say to ourselves, rising.

105
00:13:15,000 --> 00:13:22,000
As it rises, when the stomach falls, we say to ourselves,

106
00:13:22,000 --> 00:13:23,000
falling.

107
00:13:23,000 --> 00:13:30,000
Rising, falling.

108
00:13:30,000 --> 00:13:33,000
We're not saying the mantra, loud, it's a meditation.

109
00:13:33,000 --> 00:13:39,000
So we're just using it in our minds to remind ourselves

110
00:13:39,000 --> 00:13:49,000
or to bring about an objective state of mind, reminding ourselves of the true nature of the object.

111
00:13:49,000 --> 00:13:53,000
So maybe we can try that all together now, everyone.

112
00:13:53,000 --> 00:14:01,000
If you want, you can put your hand on your stomach or otherwise, just put your hands on your lap

113
00:14:01,000 --> 00:14:08,000
and watch the stomach rising and falling.

114
00:14:08,000 --> 00:14:14,000
Again, we're just observing, so we're not trying to control the breath or force it.

115
00:14:14,000 --> 00:14:18,000
In the beginning, you might find the natural tendency is to try to force the breath.

116
00:14:18,000 --> 00:14:24,000
Force it to be smooth, force it to be deep.

117
00:14:24,000 --> 00:14:31,000
And this is an important observation because you begin to see how much suffering is caused by forcing things.

118
00:14:31,000 --> 00:14:43,000
So in the beginning, it might be quite unpleasant as you teach the mind to the disadvantages or the dangers in clinging or enforcing.

119
00:14:43,000 --> 00:14:50,000
And the mind begins to learn to just let the breath go as it will.

120
00:14:50,000 --> 00:14:57,000
So it's not a problem to go through this experience where it's uncomfortable and stressful.

121
00:14:57,000 --> 00:15:11,000
Because, again, we're trying to teach the mind the true nature of reality and teach the mind the...

122
00:15:11,000 --> 00:15:20,000
teach the mind about the negative qualities.

123
00:15:20,000 --> 00:15:30,000
Like clinging or wanting a version and so on.

124
00:15:30,000 --> 00:15:33,000
This can only be done by seeing the true nature of these things.

125
00:15:33,000 --> 00:15:38,000
I've seen the nature of clinging and seeing the nature of a version and so on.

126
00:15:38,000 --> 00:15:40,000
So just let it go with it.

127
00:15:40,000 --> 00:15:44,000
However your mind reacts, consider that to be a learning experience.

128
00:15:44,000 --> 00:15:49,000
We learn from our experiences, learn from our mistakes.

129
00:15:49,000 --> 00:15:58,000
And try to just let the breath go as it's going to go and remind yourself this is rising.

130
00:15:58,000 --> 00:16:05,000
It's just falling, say to yourself, rising, falling.

131
00:16:05,000 --> 00:16:31,000
I will pay attention to my notebooks.

132
00:16:31,000 --> 00:16:34,000
.

133
00:16:34,000 --> 00:16:37,000
..

134
00:16:37,000 --> 00:16:39,000
..

135
00:16:39,000 --> 00:16:41,000
..

136
00:16:41,000 --> 00:16:43,000
..

137
00:16:43,000 --> 00:16:45,000
..

138
00:16:45,000 --> 00:16:47,000
..

139
00:16:47,000 --> 00:16:49,000
..

140
00:16:49,000 --> 00:16:51,000
..

141
00:16:51,000 --> 00:16:53,000
..

142
00:16:53,000 --> 00:16:55,000
..

143
00:16:55,000 --> 00:16:59,000
..

144
00:16:59,000 --> 00:17:04,000
..

145
00:17:04,000 --> 00:17:09,000
..

146
00:17:09,000 --> 00:17:11,000
..

147
00:17:11,000 --> 00:17:12,000
..

148
00:17:12,000 --> 00:17:18,000
The first thing you should begin to see is how it's not as easy as it sounds.

149
00:17:18,000 --> 00:17:24,000
It's actually quite difficult to keep the mind focused on a single object.

150
00:17:24,000 --> 00:17:28,000
But it's not really the point of this meditation to focus on a single object.

151
00:17:28,000 --> 00:17:36,000
Our experience, our ordinary everyday experience, is not the experience of a single object.

152
00:17:36,000 --> 00:17:43,000
So, we can expand our meditation to include every object of our experience,

153
00:17:43,000 --> 00:17:52,000
and still consider it to be meditation, because we're still clearly seeing the experience for what it is.

154
00:17:52,000 --> 00:17:59,000
For instance, you might experience pain, or some sort of feeling arising in the body.

155
00:17:59,000 --> 00:18:03,000
It might be a painful feeling, you might feel happy or pleasant.

156
00:18:03,000 --> 00:18:14,000
You might experience great calm.

157
00:18:14,000 --> 00:18:19,000
And so, rather than ignoring the sensations or trying to push them away,

158
00:18:19,000 --> 00:18:26,000
and to find some way to be that keeps you free from the sensations.

159
00:18:26,000 --> 00:18:35,000
When you feel pain moving, adjusting your position, trying to avoid the experience,

160
00:18:35,000 --> 00:18:41,000
we're going to dive right in and be with the experience, and learn about the experience.

161
00:18:41,000 --> 00:18:44,000
Take it as our teacher.

162
00:18:44,000 --> 00:18:52,000
Because as I said, things like experiences like pain are not intrinsically negative.

163
00:18:52,000 --> 00:19:05,000
It's just the body expressing the tension or the nature of the experience.

164
00:19:05,000 --> 00:19:17,000
There's the sensation of pressure, or tension, or hardness, or so on, stiffness.

165
00:19:17,000 --> 00:19:27,000
But when picked up by the mind, it leads to disliking and worrying and stress about the pain.

166
00:19:27,000 --> 00:19:33,000
So, we're going to change the way we look at this pain, and just see it as pain.

167
00:19:33,000 --> 00:19:49,000
Say to yourself, pain, pain, pain, or aching, aching, or whoever it appears to.

168
00:19:49,000 --> 00:19:53,000
And you can just stay with the pain as long as it's there.

169
00:19:53,000 --> 00:19:57,000
Not trying to make it go away, not concerned about it.

170
00:19:57,000 --> 00:20:04,000
In any way, just being with it.

171
00:20:04,000 --> 00:20:11,000
This is obviously a very difficult thing to do in the beginning.

172
00:20:11,000 --> 00:20:22,000
But if you become skilled in this practice, you can get to the point where pain itself is no longer a problem.

173
00:20:22,000 --> 00:20:30,000
Because you become familiar with it, and when pain arises, it's not scary, it's not unpleasant, it's familiar.

174
00:20:30,000 --> 00:20:38,000
You know the truth of pain, it is what it is.

175
00:20:38,000 --> 00:20:44,000
You know that it's not going to go away just because you want it to go away.

176
00:20:44,000 --> 00:20:49,000
And you know that the more you want it to go away, the more suffering you have.

177
00:20:49,000 --> 00:20:53,000
And the more stressful it becomes for you.

178
00:20:53,000 --> 00:20:57,000
So you learn to just accept it for what it is.

179
00:20:57,000 --> 00:21:07,000
Say when you feel happier, you feel calm, you know that clinging to it and liking it.

180
00:21:07,000 --> 00:21:16,000
It's only a cause for more stress when it disappears, or more stress in trying to keep it and trying to maintain it.

181
00:21:16,000 --> 00:21:26,000
It means more work for you to have to keep developing the experience.

182
00:21:26,000 --> 00:21:30,000
And so you learn to let go of even pleasant experiences.

183
00:21:30,000 --> 00:21:37,000
Just to see happiness for what it is, happy, happy, happy or calm.

184
00:21:37,000 --> 00:21:54,000
See calm for what it is, calm, calm and let it be, let it come, let it go.

185
00:21:54,000 --> 00:21:56,000
Or there may be thoughts when you're meditating.

186
00:21:56,000 --> 00:22:01,000
You may start thinking about the past, thinking about the future.

187
00:22:01,000 --> 00:22:07,000
You may have good thoughts, you may have bad thoughts.

188
00:22:07,000 --> 00:22:10,000
Normally we think, oh, this is keeping me from meditating.

189
00:22:10,000 --> 00:22:14,000
My thoughts are getting in the way of my meditation, thinking too much.

190
00:22:14,000 --> 00:22:18,000
But really thinking too much is just a judgment.

191
00:22:18,000 --> 00:22:28,000
Too much of anything is just our own arbitrary measurement of what is enough and what is too much.

192
00:22:28,000 --> 00:22:34,000
The truth is that that's how much you're thinking, you're thinking, that's the truth.

193
00:22:34,000 --> 00:22:40,000
So the meditation practice is to remind ourselves that that's all that there is, it's just thinking.

194
00:22:40,000 --> 00:22:45,000
It's not good, it's not bad, it is what it is, thinking.

195
00:22:45,000 --> 00:22:55,000
So to remind ourselves, we say to ourselves, thinking, thinking every time that you think, just bring your mind back to what is real.

196
00:22:55,000 --> 00:22:58,000
This is just a thought.

197
00:22:58,000 --> 00:23:03,000
We can destroy ourselves with thoughts, thinking again and again about the same thing.

198
00:23:03,000 --> 00:23:11,000
I'm obsessing, worrying, stressing out until we have to take medication for our stress.

199
00:23:11,000 --> 00:23:19,000
And then we start stressing about the medication and so on.

200
00:23:19,000 --> 00:23:32,000
Once we can remind ourselves that it's just thoughts, they're just memories.

201
00:23:32,000 --> 00:23:42,000
That will cease to have any power over you.

202
00:23:42,000 --> 00:23:57,000
Thank you.

203
00:23:57,000 --> 00:24:21,000
And we can also be mindful or should be mindful of the emotions that arise in the mind.

204
00:24:21,000 --> 00:24:30,000
They can arise based on physical sensations, you'll be watching the rising and falling and suddenly you don't like it or you like it.

205
00:24:30,000 --> 00:24:42,000
Suddenly you start to doubt about it or you're confused or you're worried about something, worried that it's something's wrong with your body.

206
00:24:42,000 --> 00:24:58,000
Maybe you have feelings, painful feelings and you start to like it or you dislike them, happy feelings and you like them or you worry about your pain or you feel doubt about the feelings.

207
00:24:58,000 --> 00:25:09,000
Or maybe you have thoughts that you like or dislike, worry about them, stress about them, doubt about them.

208
00:25:09,000 --> 00:25:24,000
These sort of emotions are all considered to be hindrances in the meditation practice because they take you away from clear awareness of reality.

209
00:25:24,000 --> 00:25:36,000
But no fear, they're not to be rejected, they're to be understood and through understanding they will be given up naturally.

210
00:25:36,000 --> 00:25:45,000
The truth is someone who sees reality for what it is will never give rise to liking, disliking, wanting and addiction, clinging.

211
00:25:45,000 --> 00:25:52,000
They never give rise to boredom or fear or frustration or sadness.

212
00:25:52,000 --> 00:25:56,000
They won't have any reason for these emotions.

213
00:25:56,000 --> 00:26:05,000
Their mind will be at peace, at peace with reality, comfortable in any situation.

214
00:26:05,000 --> 00:26:16,000
Because they've seen the way reality goes, that it's not amenable to our will, to our control.

215
00:26:16,000 --> 00:26:24,000
And so they don't see the point of clinging or chasing away experience.

216
00:26:24,000 --> 00:26:34,000
Their happiness is no longer conditional, it's no longer dependent on certain types of experience.

217
00:26:34,000 --> 00:26:40,000
They can be happy in any situation.

218
00:26:40,000 --> 00:26:53,000
So to get to this point we have to come to understand the disadvantages of these negative emotion or these types of emotion.

219
00:26:53,000 --> 00:26:55,000
I have to come to see them for what they are objectively.

220
00:26:55,000 --> 00:26:59,000
We don't have to believe them to be evil or bad.

221
00:26:59,000 --> 00:27:09,000
We simply have to watch them and learn about all of our emotions, the good ones and the bad ones, to come to see whether they are truly good or truly bad.

222
00:27:09,000 --> 00:27:14,000
Useful or useless, beneficial or harmful.

223
00:27:14,000 --> 00:27:23,000
So when we like something we consider ourselves liking, liking, just seeing the liking for what it is.

224
00:27:23,000 --> 00:27:29,000
When we have disliking we consider ourselves disliking or disliking.

225
00:27:29,000 --> 00:27:37,000
Or when we want something we can say wanting, wanting angry or frustrated or bored or sad.

226
00:27:37,000 --> 00:27:45,000
We can say to ourselves, bored or frustrated or frustrated or sad or so.

227
00:27:45,000 --> 00:28:07,000
As the feeling appears to us.

228
00:28:07,000 --> 00:28:23,000
If we feel worried or stressed or distracted we can say to ourselves stressed or worried or distracted.

229
00:28:23,000 --> 00:28:33,000
If we have doubt or confusion we can say to ourselves doubting, doubting or confused, confused.

230
00:28:33,000 --> 00:28:38,000
If we feel drowsy or tired we can say to ourselves drowsy and drowsy.

231
00:28:38,000 --> 00:28:47,000
Whatever state of mind arises we just try to see it for what it is and let it be.

232
00:28:47,000 --> 00:29:00,000
Not becoming stressed about our stress or worried about our worried or angry about our anger.

233
00:29:00,000 --> 00:29:13,000
Giving them a fair hearing, letting them come up, judging them impartially and letting them go.

234
00:29:13,000 --> 00:29:20,000
So altogether this is a fairly comprehensive explanation on this sort of practice.

235
00:29:20,000 --> 00:29:36,000
Whenever there is nothing clearly visible or clearly perceivable in terms of thoughts or feelings or emotions.

236
00:29:36,000 --> 00:29:43,000
We can just come back to the breath, come back to the body because we will always have to breathe.

237
00:29:43,000 --> 00:29:51,000
When the stomach rises just say to ourselves, rise and the stomach falls.

238
00:29:51,000 --> 00:30:02,000
Or even just become aware of the fact that you are sitting if the breath doesn't seem like the easiest object to be mindful of.

239
00:30:02,000 --> 00:30:04,000
You can focus instead on the body.

240
00:30:04,000 --> 00:30:16,000
Just be aware that you are sitting, say to yourself, sitting, sitting.

241
00:30:16,000 --> 00:30:22,000
And then when something else comes up, shift your attention, focus on the new object.

242
00:30:22,000 --> 00:30:27,000
When it's gone come back again to the body.

243
00:30:27,000 --> 00:30:32,000
So maybe we can try practicing in this way for a little while without any talking.

244
00:30:32,000 --> 00:30:36,000
And then if people have questions I'm happy to take questions as well.

245
00:31:02,000 --> 00:31:25,000
Thank you.

246
00:31:25,000 --> 00:31:54,000
Thank you.

247
00:31:54,000 --> 00:32:17,000
Thank you.

248
00:32:17,000 --> 00:32:40,000
Thank you.

249
00:32:40,000 --> 00:33:03,000
Thank you.

250
00:33:03,000 --> 00:33:26,000
Thank you.

251
00:33:26,000 --> 00:33:49,000
Thank you.

252
00:33:49,000 --> 00:34:12,000
Thank you.

253
00:34:12,000 --> 00:34:35,000
Thank you.

254
00:34:35,000 --> 00:35:01,000
Thank you.

255
00:35:01,000 --> 00:35:27,000
Thank you.

256
00:35:27,000 --> 00:35:53,000
Thank you.

257
00:35:53,000 --> 00:36:19,000
Thank you.

258
00:36:19,000 --> 00:36:45,000
Thank you.

259
00:36:45,000 --> 00:37:11,000
Thank you.

260
00:37:11,000 --> 00:37:37,000
Thank you.

261
00:37:37,000 --> 00:38:03,000
Thank you.

262
00:38:03,000 --> 00:38:29,000
Thank you.

263
00:38:29,000 --> 00:38:55,000
Thank you.

264
00:38:55,000 --> 00:39:21,000
Thank you.

265
00:39:21,000 --> 00:39:47,000
Thank you.

266
00:39:47,000 --> 00:40:13,000
Thank you.

267
00:40:13,000 --> 00:40:39,000
Thank you.

268
00:40:39,000 --> 00:41:05,000
Thank you.

269
00:41:05,000 --> 00:41:31,000
Thank you.

270
00:41:31,000 --> 00:41:57,000
Thank you.

271
00:41:57,000 --> 00:42:23,000
Thank you.

272
00:42:23,000 --> 00:42:49,000
Thank you.

273
00:42:49,000 --> 00:43:15,000
Thank you.

274
00:43:15,000 --> 00:43:41,000
Thank you.

275
00:43:41,000 --> 00:44:07,000
Thank you.

276
00:44:07,000 --> 00:44:33,000
Thank you.

277
00:44:33,000 --> 00:44:59,000
Thank you.

278
00:44:59,000 --> 00:45:25,000
Thank you.

279
00:45:25,000 --> 00:45:51,000
Thank you.

280
00:45:51,000 --> 00:46:17,000
Thank you.

281
00:46:17,000 --> 00:46:43,000
Thank you.

282
00:46:43,000 --> 00:47:09,000
Thank you.

283
00:47:09,000 --> 00:47:35,000
Thank you.

284
00:47:39,000 --> 00:48:05,000
Thank you.

285
00:48:05,000 --> 00:48:31,000
Thank you.

286
00:48:31,000 --> 00:48:57,000
Thank you.

287
00:48:57,000 --> 00:49:23,000
Thank you.

288
00:49:23,000 --> 00:49:49,000
Thank you.

289
00:49:49,000 --> 00:50:15,000
Thank you.

290
00:50:19,000 --> 00:50:45,000
Thank you.

291
00:50:45,000 --> 00:51:11,000
Thank you.

292
00:51:11,000 --> 00:51:37,000
Thank you.

293
00:51:37,000 --> 00:52:03,000
Thank you.

294
00:52:03,000 --> 00:52:29,000
Thank you.

295
00:52:29,000 --> 00:52:55,000
Thank you.

296
00:52:55,000 --> 00:53:21,000
Thank you.

297
00:53:21,000 --> 00:53:47,000
Thank you.

298
00:53:47,000 --> 00:54:13,000
Thank you.

299
00:54:13,000 --> 00:54:39,000
Thank you.

300
00:54:39,000 --> 00:55:05,000
Thank you.

301
00:55:05,000 --> 00:55:31,000
Thank you.

302
00:55:31,000 --> 00:55:57,000
Thank you.

303
00:55:57,000 --> 00:56:23,000
Thank you.

304
00:56:23,000 --> 00:56:49,000
Thank you.

305
00:56:49,000 --> 00:57:15,000
Thank you.

306
00:57:15,000 --> 00:57:41,000
Thank you.

307
00:57:41,000 --> 00:58:07,000
Thank you.

308
00:58:07,000 --> 00:58:33,000
Thank you.

309
00:58:33,000 --> 00:58:59,000
Thank you.

310
00:58:59,000 --> 00:59:25,000
Thank you.

311
00:59:25,000 --> 00:59:51,000
Thank you.

312
00:59:51,000 --> 01:00:17,000
Thank you.

313
01:00:17,000 --> 01:00:43,000
Thank you.

314
01:00:43,000 --> 01:01:09,000
Thank you.

315
01:01:09,000 --> 01:01:35,000
Thank you.

316
01:01:35,000 --> 01:02:01,000
Thank you.

317
01:02:01,000 --> 01:02:27,000
Thank you.

318
01:02:27,000 --> 01:02:53,000
Thank you.

319
01:02:53,000 --> 01:03:19,000
Thank you.

320
01:03:19,000 --> 01:03:45,000
Thank you.

321
01:03:45,000 --> 01:04:11,000
Thank you.

322
01:04:11,000 --> 01:04:37,000
Thank you.

323
01:04:37,000 --> 01:05:03,000
Thank you.

324
01:05:03,000 --> 01:05:29,000
Thank you.

325
01:05:29,000 --> 01:05:55,000
Thank you.

326
01:05:55,000 --> 01:06:21,000
Thank you.

327
01:06:21,000 --> 01:06:47,000
Thank you.

328
01:06:47,000 --> 01:07:13,000
Thank you.

329
01:07:13,000 --> 01:07:39,000
Thank you.

330
01:07:39,000 --> 01:08:05,000
Thank you.

331
01:08:05,000 --> 01:08:31,000
Thank you.

332
01:08:31,000 --> 01:08:57,000
Thank you.

333
01:08:57,000 --> 01:09:23,000
Thank you.

334
01:09:23,000 --> 01:09:49,000
Thank you.

335
01:09:49,000 --> 01:10:15,000
Thank you.

336
01:10:15,000 --> 01:10:41,000
Thank you.

337
01:10:41,000 --> 01:11:07,000
Thank you.

338
01:11:07,000 --> 01:11:33,000
Thank you.

339
01:11:33,000 --> 01:11:59,000
Thank you.

340
01:11:59,000 --> 01:12:25,000
Thank you.

