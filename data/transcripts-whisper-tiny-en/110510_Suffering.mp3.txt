Hello, welcome back to Ask a Monk.
Today's question is on whether I think that there is more suffering in America, this is
the question, where it is almost impossible not to find food and I guess because we are
the people in America had developed their society, or more suffering in a tribal society.
This is the question.
So first of all, I'd like to sort of expand the question because I think the important
point is whether not whether America now or America when it was owned by the Native American
or First Nations people is better.
The question is whether there is more suffering to be found in a society with technological
advancements and politics and government and roads and cars and computers and iPods and so on.
Or whether there is more suffering in a lifestyle that is simple, unrefined, unadvanced,
unorganized, unorganized on a level similar to a tribe.
And it's a difficult question, but why it's difficult is because I think there's merits
to both sides.
I mean, I want to be able to say that material advancement in some way heightens people's
ability or interest in things like science, things like objective investigation and analysis
and so on.
I mean, the education side of things, I want to be able to say that that's a good thing
into some degree.
But I think given the choice, I would have to say that there's probably less suffering
to be found in a less advanced society.
And I think we have to advance this beyond the tribal societies of, say, North America
in olden times now.
There are still some tribal societies, but not really to the extent that you would have
found some time ago.
Because I think this is a rather limited sort of way of looking at things.
There are many societies or cultures or groups of individuals, groups of people living in
the world who are living in limited technological advancement and so on.
I think living in Thailand and living in Sri Lanka, I've been able to see some of these
groups.
I mean, Thailand, for example, is quite advanced in many ways.
But in the villages, in the countryside, you'll still find people who are very technologically
behind and living in fairly unorganized manners, in terms of political structure and so on.
And you're in Sri Lanka even more so.
You don't see the impact of modern society to such a great extent as you would in the
West.
And I think, beneficially, at least to some extent, that it has allowed people to be free
from some of the sufferings.
So stacking up the sufferings, I think the obvious suffering that was pointed out in the
question is living in a tribal society or an uncivilized quote-unquote society, it's going
to be more difficult to find food and I don't really think this is that big of a deal.
In fact, I probably would care to wager that North America, it was a lot easier to find
food before technology came into effect and now the food quality has gone down and people
have to work hard for it and so on.
I would say that anyway, that's maybe a bit speculative.
But the real suffering, I think, that comes, well, it was being pointed here, pointed
to here, is the lack of amenities, you know, living in rough conditions, having to put
up with insects, having to put up with leeches, having to put up with snakes and scorpions,
having to put up with mosquitoes because you don't have screens on your windows or you
don't have the technology to prevent these things, having to put up with limited medical
facilities and so on.
There are a lot of sufferings that come with living in a simple manner and I think it's
fair to point out that, you know, the monastic life is by its nature, the Buddhist monastic
life is of a sort of simple life and as a result, there is a certain amount of suffering.
This is why I think it was an interesting question and why I'd like to take some time
to answer it because I have to personally go through some of the sufferings that, you
know, the villagers here even have to go through, perhaps even more so than they have
to go through.
I might be going through some of the sufferings that you might expect from a tribal
society because, you know, for me food is limited, I only eat one meal a day so you could
say that's similar to the suffering that is brought about by having to hunt for your
food, not knowing where you're going to get your next meal, not having a farm land or
a grocery store or money that you can easily access these things with and having to
put up with limited medical facilities, but I think the real key here is that, or the
a real good way to answer this question is to point out the fact that so many people undertake
this lifestyle on purpose and voluntarily and why would they do it if it was just going
to lead to their suffering?
And I think that's a question that a lot of people ask why do people decide to live in
caves and where, you know, robes, you know, a single set of robes, et cetera, et cetera,
et cetera, are eating only one meal a day, putting up with all of these difficulties.
And I think it's because it's a trade-off.
If you give up or if you want to be free from all of the stresses and all of the sufferings
that are on the other side of the defense, then you have to, you can't expect to have
all the amenities.
I mean, they go hand in hand.
The sufferings on the other side of a technologically advanced society are the stresses
of having to work a job, you know, nine to five job, the stresses of having to think a
lot of having to deal with people, crowds of people having to deal with traffic, having
to deal with the complications that come with technology, like fixing your car and, you
know, your house and all of the laws and having to deal with laws and lawyers and people
suing you and so on.
There's a huge burden of stress having to be worried about thieves, having to be worried
about crooks, not crook butt, people cheating you, having to be worried about your
boss, your co-workers, your clients and so on and a million other things.
I mean, no matter what work you apply yourself to in an advanced society, there are more
complications and as a result, more stress and as a result, more suffering.
So I think the trade-off there is quite worth it in my mind because I'd like to, you
know, the answer for me is quite clear because having put up with all these sufferings
of having to have mosquitoes bite me and who knows what bite me and having to put up
with leeches and you know, the worries that I have to put up with worrying about stepping
on a snake, worrying about that forefoot lizard down below me and the monkey scrapping
on my head and so on.
I really don't feel upset or affected by these things, they're physical, a physical reality
and they're much more physical than they are mental.
I feel so much more at peace here living in the jungle which, you know, may look peaceful
but it's actually quite dangerous and it keeps you on your toes in many ways, even during
meditation, you have to do these little bugs that you can't even see them, they're
worse than mosquitoes and they bite you but it's not a stress for me, not in the same
way as living in, say, the big cities of Colombo, Bangkok, Los Angeles, the stress is
that's involved there, you know, it's an order of magnitude greater.
So I'm not sure if this exactly answers the question but this is how I would prefer
to approach it, I think there's far less stress to be had from a simple lifestyle because
the stress is much more physical than they are mental and the mental stress is far less.
Another thing about living in a simple society in a simple lifestyle is that it's much
more in tune with our programming physically, I mean we're still very much related to
the monkeys and so this kind of atmosphere jives with us, seeing the trees, why people
go to nature and they feel peaceful of a sudden is because of how, it's not because there's
anything special there, it's because of what's not there, there's nothing jarring with
your experience, no horns blasting, no bright lights, no beautiful pictures catching your
attention, no ugly sights and so on, it's very peaceful, very calm, very natural, right?
I mean that's why we're here and I think you'll find that in a traditional society.
So I think, and I think this is borne out by the Buddhist teaching, it's borne out by
the examples, obviously, of those people who have ordained us monks.
There was a story in the typical, in the Buddhist teaching that we always go back to,
I think in the commentaries actually, I'm not sure, well at least it's in the commentaries
about this king, relative of the king who became a monk, Mahakapina and after he became
a monk, he would sit under this tree, used to be a king, right?
So after he became a monk, he would sit under a tree and go, a horse, a horse, a horse,
a horse, which means, oh, what happiness, oh, what bliss, oh, what happiness.
So you might say he was actually contemplating the happiness, he was looking at it and examining
it in a much in the same way that we say to ourselves, happy, happy.
But the monks heard him saying this and remarking this and they took it the wrong way,
they took it to me and he was, for some reason, they took it that he must be thinking
about his kingly happiness, he was remembering, oh, what happiness it was to be a king.
And so they took him to the Buddha and the Buddha asked him, you know, what was it?
And he said, no, when I was a king, I had to worry about all of these stresses and concerns.
My life was always in danger of assassination, I had all this treasure that I had to guard
all these ministers that I had to manage and, you know, to stop from cheating me and all
the people who would come to me with their problems and all the laws I had to enforce
and so on and so on.
And I have none of that now.
Now I have this tree, three robes, my bull, my tree and this is the greatest happiness
I've ever known in my life, he said.
So there's a story to go with this question.
I think one final note is that regardless of the answer to this question, whether there's
more suffering in either one, I think the point is that whatever lifestyle you can undertake,
not that has the least amount of suffering, but which has the least amount of unwholesumness
involved and that is the best lifestyle.
So for those people, you know, who might be discouraged then by the fact that they have
to live in a technologically advanced society as a Buddhist and they feel like they should
be living in the forest, I don't mean to say that there's anything wrong with living
in the city.
I've lived in Los Angeles.
I've gone on arms round in North Hollywood and in places, you know, in Bangkok, in Colombo.
I've gone, I've lived in these places in fairly chaotic and stressful situations, but
the point is to live your life in such a way that it's free from unwholesumness.
So this is where the question gets interesting because many tribal societies are involved
in what Buddhists would call great unwholesumness.
Their way of lifestyle is hunting, as an example, and though they do it to serve, you
could say it to survive, it's by nature caught up with these mistakes of cruelty, states
of anger and delusion, and as a result, it keeps them bound to the wheel of life that they
are going to have to take turns with the deer.
You know, this is what the First Nations people believe.
They still believe that, you know, you take turns.
I hunt the deer this life and in the next life, I'm the deer and they hunt me and so on.
There are these kinds of beliefs and this is really in line with the Buddha's teaching.
So I mean, if you're fine with that, having to be hunted every other lifetime, every few
lifetimes, then so be it.
But this isn't the, from a Buddhist point of view, this isn't the way out of suffering.
It's because there's a lot of suffering.
I mean, tribes, of course, then there's tribal warfare and, you know, they're not free from
all of this.
And we're not free from it on the other side as living in technologically advanced societies.
You know, living in Canada, I wasn't, you know, free from these things.
I was hunting as well.
I didn't need to, but I went hunting with my father.
You know, we engage in a lot of unholesumness in advanced societies.
So that's the most important.
You can live in a city.
You can live in nature.
The reason why we choose to live in nature is, I think, overall, you know, the amount of
peace that you have is the one side, but there's also a lot less unholesumness that you're
free from having to deal with, you know, other people's unholesumness and having to see
all these beautiful, attractive sites and wanting to get this wanting to get that being
annoyed by people being upset by people.
So you're able to focus much clearer on the very core aspects of existence, you know, what
really is reality, without having to be, get caught up in unholesum mindset, stress and
addiction and so on.
And I think that's clear.
I think being here in the forest is much better for my meditation practice than being
in the city.
But in the end, it's not the biggest concern, and I'm not afraid to live in the city if
I have to, because, for me, the most important is to continue the meditation practice
and to learn more about my reactions and my interactions with the world around me.
And sometimes that can be tested by suffering.
Again, the Buddha's teaching is not to run away from suffering.
It's to learn and to come to understand suffering.
When you understand suffering, you'll understand what is really causing suffering, that
it's not the objects, it's your relationship and your reactions to them, your attachment
to things being in a certain way and not being in another way, your attachment to the
objects of the sense and so on.
When you can be free from that, it doesn't matter where you live, whether it be in a cave
or in an apartment complex.
So there's the answer to your question, thanks, and have a good day.
