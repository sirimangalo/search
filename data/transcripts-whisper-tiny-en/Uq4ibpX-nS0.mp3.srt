1
00:00:00,000 --> 00:00:03,000
Okay, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:07,000
Next question is about divorce,

3
00:00:07,000 --> 00:00:14,000
and I was asked the question about how to deal with a divorce

4
00:00:14,000 --> 00:00:17,000
when you are a third party,

5
00:00:17,000 --> 00:00:22,000
or in this case, your parents are divorced

6
00:00:22,000 --> 00:00:25,000
or divorcing in the process of it.

7
00:00:25,000 --> 00:00:28,000
How do you deal with it?

8
00:00:28,000 --> 00:00:32,000
And I guess, as always,

9
00:00:32,000 --> 00:00:35,000
we should put the question in context.

10
00:00:35,000 --> 00:00:38,000
And because here we're talking about several different things.

11
00:00:38,000 --> 00:00:43,000
We're talking about the idea of family and one's parents.

12
00:00:43,000 --> 00:00:47,000
We're talking about the concept of marriage.

13
00:00:47,000 --> 00:00:51,000
And we're talking about suffering.

14
00:00:51,000 --> 00:00:56,000
So these are all three of the issues that come up

15
00:00:56,000 --> 00:00:59,000
in relation to divorce.

16
00:00:59,000 --> 00:01:03,000
The concept of a family

17
00:01:03,000 --> 00:01:07,000
has to do with all of those concepts

18
00:01:07,000 --> 00:01:12,000
that most of us find to seem

19
00:01:12,000 --> 00:01:16,000
or that seem to most of us to be

20
00:01:16,000 --> 00:01:20,000
ultimately real, for instance, being human.

21
00:01:20,000 --> 00:01:25,000
We think that humanity is something real.

22
00:01:25,000 --> 00:01:30,000
We think of our gender as being something real.

23
00:01:30,000 --> 00:01:34,000
And as a result,

24
00:01:34,000 --> 00:01:40,000
these concepts form our opinion,

25
00:01:40,000 --> 00:01:42,000
our ideas about reality,

26
00:01:42,000 --> 00:01:44,000
our ideas of family,

27
00:01:44,000 --> 00:01:48,000
our idea, the concept of having parents.

28
00:01:48,000 --> 00:01:50,000
And I see these are concepts,

29
00:01:50,000 --> 00:01:52,000
and I think they should be broken apart.

30
00:01:52,000 --> 00:01:54,000
They should be broken down and understood

31
00:01:54,000 --> 00:01:57,000
to be simply concept.

32
00:01:57,000 --> 00:02:02,000
Reality doesn't admit of any of these concepts.

33
00:02:02,000 --> 00:02:05,000
In an ultimate sense,

34
00:02:05,000 --> 00:02:06,000
there is no human,

35
00:02:06,000 --> 00:02:08,000
there is no gender,

36
00:02:08,000 --> 00:02:09,000
there is no family,

37
00:02:09,000 --> 00:02:12,000
there is no mother and father and so on.

38
00:02:12,000 --> 00:02:15,000
And I'm going to get in trouble for that

39
00:02:15,000 --> 00:02:17,000
because on a conventional level,

40
00:02:17,000 --> 00:02:20,000
people understand that there is mother and father.

41
00:02:20,000 --> 00:02:22,000
If I say there is no mother and father,

42
00:02:22,000 --> 00:02:24,000
they are going to say what that means,

43
00:02:24,000 --> 00:02:27,000
we shouldn't respect our parents,

44
00:02:27,000 --> 00:02:30,000
or we shouldn't be thankful for the things they have done.

45
00:02:30,000 --> 00:02:33,000
And obviously that's not the case,

46
00:02:33,000 --> 00:02:37,000
but on an ultimate reality doesn't admit of these things.

47
00:02:37,000 --> 00:02:42,000
And when we can break away from these concepts,

48
00:02:42,000 --> 00:02:47,000
then we can break away from a lot of the suffering that comes from them.

49
00:02:47,000 --> 00:02:53,000
So for instance, in this case, our attachment to our parents,

50
00:02:53,000 --> 00:02:57,000
we understand them to be a specific entity.

51
00:02:57,000 --> 00:02:59,000
Our father is thus, our mother is thus,

52
00:02:59,000 --> 00:03:02,000
our family is this unit.

53
00:03:02,000 --> 00:03:04,000
There is an entity that we call our family.

54
00:03:04,000 --> 00:03:07,000
There is a relationship that we call family

55
00:03:07,000 --> 00:03:10,000
that we call the blood relationship and so on.

56
00:03:10,000 --> 00:03:13,000
But simply looking at,

57
00:03:13,000 --> 00:03:16,000
if we examine this from a scientific point of view,

58
00:03:16,000 --> 00:03:20,000
it's easy to see the falsehood of them.

59
00:03:20,000 --> 00:03:22,000
I mean, as human beings,

60
00:03:22,000 --> 00:03:26,000
our understanding is that by blood we're related to each and every one,

61
00:03:26,000 --> 00:03:31,000
each and every human on the planet, by blood ties.

62
00:03:31,000 --> 00:03:34,000
So we're all family.

63
00:03:34,000 --> 00:03:39,000
Obviously this is a beneficial realization as well,

64
00:03:39,000 --> 00:03:45,000
because it breaks down all of the cultural barriers,

65
00:03:45,000 --> 00:03:50,000
all of the racism and prejudice and so on.

66
00:03:50,000 --> 00:03:55,000
The us and them mentalities that pervade the world.

67
00:03:55,000 --> 00:03:59,000
But whether it's useful or not, it's the truth.

68
00:03:59,000 --> 00:04:04,000
The reality is that there is no barrier or boundary

69
00:04:04,000 --> 00:04:06,000
where we can see this as family and this is not.

70
00:04:06,000 --> 00:04:08,000
It's just a matter of degree.

71
00:04:08,000 --> 00:04:12,000
And that degree is important in one sense

72
00:04:12,000 --> 00:04:15,000
in terms of our understanding of each other,

73
00:04:15,000 --> 00:04:17,000
our ability to relate to each other,

74
00:04:17,000 --> 00:04:21,000
our knowledge of each other's past and so on.

75
00:04:21,000 --> 00:04:24,000
And this is the relationship we have with our parents.

76
00:04:24,000 --> 00:04:34,000
There are many emotions and many memories

77
00:04:34,000 --> 00:04:40,000
and many ultimate realities that are tied up with this concept of family.

78
00:04:40,000 --> 00:04:46,000
Once we have parents then we have many, many attachments

79
00:04:46,000 --> 00:04:51,000
that go along with that and we have many comforts

80
00:04:51,000 --> 00:04:53,000
and supports.

81
00:04:53,000 --> 00:04:55,000
There's many beneficial things that come obviously

82
00:04:55,000 --> 00:04:58,000
from having a stable family unit.

83
00:04:58,000 --> 00:05:02,000
But something that will help us to deal with divorce

84
00:05:02,000 --> 00:05:11,000
is to understand that there is really no change occurring

85
00:05:11,000 --> 00:05:15,000
that apart from the actual suffering

86
00:05:15,000 --> 00:05:19,000
that comes from a divorce where you lose the stability,

87
00:05:19,000 --> 00:05:24,000
where you lose the cohesion,

88
00:05:24,000 --> 00:05:28,000
where you lose the support of having two parents

89
00:05:28,000 --> 00:05:30,000
and a family unit.

90
00:05:30,000 --> 00:05:33,000
There's nothing wrong.

91
00:05:33,000 --> 00:05:36,000
There's nothing wrong.

92
00:05:36,000 --> 00:05:39,000
There's no problem except in your own mind

93
00:05:39,000 --> 00:05:43,000
where you've had your life disrupted.

94
00:05:43,000 --> 00:05:46,000
You've had your way of looking at reality change

95
00:05:46,000 --> 00:05:50,000
and in fact it's in a way that seems like something

96
00:05:50,000 --> 00:05:55,000
that was very real and whole has broken into pieces.

97
00:05:55,000 --> 00:05:57,000
When in fact that's not the case

98
00:05:57,000 --> 00:06:00,000
the idea of a family unit is in our mind.

99
00:06:00,000 --> 00:06:02,000
It's a concept.

100
00:06:02,000 --> 00:06:06,000
If our parents split up it just means that this entity,

101
00:06:06,000 --> 00:06:12,000
this mass of physical and mental realities has gone here

102
00:06:12,000 --> 00:06:15,000
and this one has gone here and they're relating to each other

103
00:06:15,000 --> 00:06:18,000
in a different way.

104
00:06:18,000 --> 00:06:22,000
The problem comes with our attachment

105
00:06:22,000 --> 00:06:26,000
and so this gets into our understanding of suffering.

106
00:06:26,000 --> 00:06:29,000
The other concept I'd like to talk about

107
00:06:29,000 --> 00:06:32,000
is the one of marriage

108
00:06:32,000 --> 00:06:36,000
because it relates to the first one.

109
00:06:36,000 --> 00:06:39,000
It relates to the idea of having parents.

110
00:06:39,000 --> 00:06:45,000
So our reliance on our parents is one attachment that we have.

111
00:06:45,000 --> 00:06:49,000
It's something that we've misled ourselves

112
00:06:49,000 --> 00:06:51,000
from our very birth.

113
00:06:51,000 --> 00:06:53,000
We've been misleading ourselves

114
00:06:53,000 --> 00:06:57,000
into thinking that there was something stable about the family unit

115
00:06:57,000 --> 00:07:01,000
and throughout our lives that stability will be eroded

116
00:07:01,000 --> 00:07:04,000
or shattered or taken away from us

117
00:07:04,000 --> 00:07:08,000
and it will generally lead to a great amount of suffering

118
00:07:08,000 --> 00:07:11,000
and as a result, learning, understanding

119
00:07:11,000 --> 00:07:14,000
that what we thought was stable is not in fact stable

120
00:07:14,000 --> 00:07:18,000
and it will allow us to open our minds up to the idea

121
00:07:18,000 --> 00:07:24,000
that life is transient, that we shouldn't cling to things

122
00:07:24,000 --> 00:07:27,000
and as a result we'll be able to accept change better.

123
00:07:27,000 --> 00:07:30,000
Most people come to this, so they come to a realization

124
00:07:30,000 --> 00:07:33,000
and they come to wisdom through the suffering

125
00:07:33,000 --> 00:07:36,000
and realizing that they were setting themselves up for this

126
00:07:36,000 --> 00:07:39,000
through their ignorance, through their misunderstanding

127
00:07:39,000 --> 00:07:41,000
that there was stability.

128
00:07:41,000 --> 00:07:47,000
Marriage is another concept

129
00:07:47,000 --> 00:07:52,000
and it's also tied up with a lot of clinging, obviously.

130
00:07:52,000 --> 00:07:58,000
So the idea of a divorce

131
00:07:58,000 --> 00:08:05,000
is one that we think of as breaking something up.

132
00:08:05,000 --> 00:08:11,000
There is something that has been shattered with the marriage

133
00:08:11,000 --> 00:08:15,000
and we feel bad about that even if we're not involved in it.

134
00:08:15,000 --> 00:08:20,000
We also feel like there is something incomplete.

135
00:08:20,000 --> 00:08:22,000
There's some knot that has been untied

136
00:08:22,000 --> 00:08:25,000
and you have to tie it back together and so on.

137
00:08:25,000 --> 00:08:28,000
But marriage is simply a contract.

138
00:08:28,000 --> 00:08:31,000
It's a conventional agreement between two people

139
00:08:31,000 --> 00:08:35,000
and that agreement has to do with morality.

140
00:08:35,000 --> 00:08:37,000
It has to do with not cheating on each other

141
00:08:37,000 --> 00:08:40,000
and has to do with supporting each other and so on.

142
00:08:40,000 --> 00:08:44,000
So there isn't that mutual support anymore.

143
00:08:44,000 --> 00:08:47,000
These people have come to an agreement

144
00:08:47,000 --> 00:08:54,000
that they're not going to act in those same ways anymore.

145
00:08:54,000 --> 00:08:57,000
It's really divorce is nothing.

146
00:08:57,000 --> 00:09:01,000
It's just a change of agreement, a change of our relationship.

147
00:09:01,000 --> 00:09:06,000
It's like leaving one job and going to another job.

148
00:09:06,000 --> 00:09:08,000
It's simply a change.

149
00:09:08,000 --> 00:09:12,000
So again, a lot of the suffering that comes from divorce

150
00:09:12,000 --> 00:09:15,000
just comes from our attachment to concepts,

151
00:09:15,000 --> 00:09:19,000
the idea of the solid family and the idea of stability,

152
00:09:19,000 --> 00:09:22,000
which is a false one.

153
00:09:22,000 --> 00:09:25,000
In the end, we have to accept the fact

154
00:09:25,000 --> 00:09:27,000
that reality is impermanent.

155
00:09:27,000 --> 00:09:29,000
Everything is changing.

156
00:09:29,000 --> 00:09:31,000
And if we, whatever we cling to,

157
00:09:31,000 --> 00:09:33,000
it's only going to lead us to suffering

158
00:09:33,000 --> 00:09:36,000
because it's not stable.

159
00:09:36,000 --> 00:09:39,000
So this is one side of things.

160
00:09:39,000 --> 00:09:41,000
And if this is causing you suffering,

161
00:09:41,000 --> 00:09:46,000
then you should immediately wipe that out of your mind and move on.

162
00:09:46,000 --> 00:09:49,000
The real suffering that comes from divorce

163
00:09:49,000 --> 00:09:52,000
and this is one that is much more difficult to overcome

164
00:09:52,000 --> 00:09:56,000
is the negative emotions that are caused by divorce,

165
00:09:56,000 --> 00:10:00,000
the anger when the greed,

166
00:10:00,000 --> 00:10:09,000
when there's jealousy involved with a divorce.

167
00:10:09,000 --> 00:10:13,000
So anger comes when you are blaming each other

168
00:10:13,000 --> 00:10:15,000
for the divorce and so on.

169
00:10:15,000 --> 00:10:17,000
There's guilt involved with it.

170
00:10:17,000 --> 00:10:22,000
The guilt in relation to one's children and so on.

171
00:10:22,000 --> 00:10:25,000
There is a lot of stress involved with the work

172
00:10:25,000 --> 00:10:30,000
that's required to split up.

173
00:10:30,000 --> 00:10:35,000
And there's a lot of greed involved in terms of splitting up

174
00:10:35,000 --> 00:10:40,000
and the belongings of the family who gets wet and how much

175
00:10:40,000 --> 00:10:45,000
and then lawyers get involved and there's more stress.

176
00:10:45,000 --> 00:10:51,000
There is jealousy in terms of competing

177
00:10:51,000 --> 00:10:54,000
for the love of the children,

178
00:10:54,000 --> 00:10:59,000
trying to not be seen as the bad guy.

179
00:10:59,000 --> 00:11:03,000
And so you'll have both parents vying for the love of their children

180
00:11:03,000 --> 00:11:09,000
saying how great they are and how trying to show off.

181
00:11:09,000 --> 00:11:15,000
And because they're afraid that the other parent is doing the same thing

182
00:11:15,000 --> 00:11:19,000
and it's a competition because they don't want to look like the bad guy

183
00:11:19,000 --> 00:11:21,000
and they care what their children think

184
00:11:21,000 --> 00:11:26,000
and they're afraid their children are going to judge them.

185
00:11:26,000 --> 00:11:32,000
And it's like this when two people are vying for the love

186
00:11:32,000 --> 00:11:35,000
of one person so the children get caught in the middle

187
00:11:35,000 --> 00:11:40,000
and the parents fight using the children.

188
00:11:40,000 --> 00:11:43,000
And that can be a great amount of suffering

189
00:11:43,000 --> 00:11:47,000
for both the parents and the children obviously.

190
00:11:47,000 --> 00:11:49,000
The children feel terrible having to be put in the middle

191
00:11:49,000 --> 00:11:55,000
and obviously they don't want to choose between their parents.

192
00:11:55,000 --> 00:11:57,000
So how do you deal with these sufferings?

193
00:11:57,000 --> 00:12:00,000
Once you can come to terms with the idea of the divorce

194
00:12:00,000 --> 00:12:05,000
and overcoming the concept of the life that you had

195
00:12:05,000 --> 00:12:08,000
that has now been shattered.

196
00:12:08,000 --> 00:12:14,000
This is where obviously meditation is of great benefit.

197
00:12:14,000 --> 00:12:19,000
And so if you begin to practice meditation,

198
00:12:19,000 --> 00:12:23,000
if you're able to look at the experience as it is,

199
00:12:23,000 --> 00:12:28,000
I think it'll be essential and be incredibly beneficial

200
00:12:28,000 --> 00:12:30,000
during that time.

201
00:12:30,000 --> 00:12:33,000
You have to understand that it's going to be a specific length

202
00:12:33,000 --> 00:12:35,000
of time, eventually your parents are going to move on.

203
00:12:35,000 --> 00:12:37,000
They may already have moved on.

204
00:12:37,000 --> 00:12:39,000
It's been a while since this question was posted.

205
00:12:39,000 --> 00:12:45,000
But whatever suffering comes from the divorce

206
00:12:45,000 --> 00:12:48,000
or even long-term suffering in terms of not having the same

207
00:12:48,000 --> 00:12:53,000
stability that you had before has to be ameliorated

208
00:12:53,000 --> 00:12:56,000
through understanding, through the ability to accept

209
00:12:56,000 --> 00:12:58,000
the reality in front of you.

210
00:12:58,000 --> 00:13:01,000
Just because it's different doesn't mean it's bad.

211
00:13:01,000 --> 00:13:05,000
Just because one is stressed and has headaches

212
00:13:05,000 --> 00:13:08,000
and is having to deal with difficult,

213
00:13:08,000 --> 00:13:10,000
more difficult situation than before,

214
00:13:10,000 --> 00:13:15,000
even having to work more or work to support one's family,

215
00:13:15,000 --> 00:13:19,000
work to support one's self and so on.

216
00:13:19,000 --> 00:13:21,000
That doesn't mean that one has to suffer.

217
00:13:21,000 --> 00:13:25,000
If you can accept and live with the reality

218
00:13:25,000 --> 00:13:29,000
and not cling to your idea of how it should be

219
00:13:29,000 --> 00:13:35,000
or be upset by disappointment,

220
00:13:35,000 --> 00:13:39,000
by not getting things the way you think they should be.

221
00:13:39,000 --> 00:13:43,000
When you're able to simply understand things as they are

222
00:13:43,000 --> 00:13:45,000
and to see that this is what's happening.

223
00:13:45,000 --> 00:13:47,000
When your relationship with reality is not,

224
00:13:47,000 --> 00:13:51,000
I wish it could be some other way or I hope it stays this way.

225
00:13:51,000 --> 00:13:54,000
Your relationship with reality is it is this way.

226
00:13:54,000 --> 00:13:58,000
And every moment knowing that now it is this way,

227
00:13:58,000 --> 00:13:59,000
now it is this way.

228
00:13:59,000 --> 00:14:03,000
When you're able to think in that way that this is reality,

229
00:14:03,000 --> 00:14:05,000
this is reality.

230
00:14:05,000 --> 00:14:07,000
And you see things and you know that you're seeing,

231
00:14:07,000 --> 00:14:09,000
you hear things and you know that you're hearing

232
00:14:09,000 --> 00:14:12,000
and your relationship with reality is

233
00:14:12,000 --> 00:14:15,000
an understanding and an acceptance

234
00:14:15,000 --> 00:14:20,000
and a clear awareness of it as it is

235
00:14:20,000 --> 00:14:23,000
with no thoughts of the past or the future

236
00:14:23,000 --> 00:14:29,000
in terms of regret or worry or feelings of loss and so on.

237
00:14:29,000 --> 00:14:33,000
Then you will be, obviously you will be far better off.

238
00:14:33,000 --> 00:14:37,000
This is clearly the preferable path.

239
00:14:37,000 --> 00:14:42,000
And so this is really the way out of suffering.

240
00:14:42,000 --> 00:14:44,000
This is the path of the Buddha taught.

241
00:14:44,000 --> 00:14:47,000
So you can take a look at some of the videos I posted

242
00:14:47,000 --> 00:14:48,000
on how to practice meditation.

243
00:14:48,000 --> 00:14:51,000
I think it will be a great help to anyone suffering

244
00:14:51,000 --> 00:14:55,000
with this sort of stress or any other stress

245
00:14:55,000 --> 00:14:57,000
and I hope those are views.

246
00:14:57,000 --> 00:14:59,000
Quickly, if you haven't looked at those videos,

247
00:14:59,000 --> 00:15:03,000
it's a very simple practice to see things as they are

248
00:15:03,000 --> 00:15:05,000
instead of wishing they were some other way

249
00:15:05,000 --> 00:15:07,000
or holding on to these things

250
00:15:07,000 --> 00:15:09,000
and hoping that they'd say this way.

251
00:15:09,000 --> 00:15:11,000
You simply remind yourself that it is what it is.

252
00:15:11,000 --> 00:15:14,000
You don't put any value judgment on it.

253
00:15:14,000 --> 00:15:16,000
When you see something, you say to yourself,

254
00:15:16,000 --> 00:15:18,000
seeing knowing that this is seeing.

255
00:15:18,000 --> 00:15:20,000
When you hear hearing, when you smell,

256
00:15:20,000 --> 00:15:22,000
you say to yourself, smelling.

257
00:15:22,000 --> 00:15:24,000
When you feel angry, you say to yourself,

258
00:15:24,000 --> 00:15:26,000
angry, when you feel sad, you say to yourself,

259
00:15:26,000 --> 00:15:28,000
sad, when you feel pain or headache.

260
00:15:28,000 --> 00:15:31,000
There's one in the pain or headache.

261
00:15:31,000 --> 00:15:33,000
Aching, aching is right.

262
00:15:33,000 --> 00:15:35,000
Simply knowing it for what it is,

263
00:15:35,000 --> 00:15:39,000
this teaches you how to accept and to understand reality

264
00:15:39,000 --> 00:15:43,000
for what it is instead of wishing it were some other way

265
00:15:43,000 --> 00:15:46,000
or holding on to and clinging to it

266
00:15:46,000 --> 00:15:49,000
and setting yourself up for greater suffering.

267
00:15:49,000 --> 00:15:52,000
But I'd encourage you to look at the videos that I've posted

268
00:15:52,000 --> 00:15:55,000
and if you have time to read the book,

269
00:15:55,000 --> 00:15:59,000
I have a book on my web blog that's better than the videos,

270
00:15:59,000 --> 00:16:01,000
at least in terms of the information.

271
00:16:01,000 --> 00:16:04,000
It contains and it should give a basic understanding

272
00:16:04,000 --> 00:16:08,000
of how to practice meditation in the tradition that I follow.

273
00:16:08,000 --> 00:16:10,000
So thanks for the question, hope that helped.

274
00:16:10,000 --> 00:16:20,000
All the best.

