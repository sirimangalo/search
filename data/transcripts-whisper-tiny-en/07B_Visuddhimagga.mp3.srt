1
00:00:00,000 --> 00:00:05,200
And then with regard to books, main responsibility for these scriptures.

2
00:00:12,240 --> 00:00:17,680
At the end of the page, the elder asked him, how are you in the scriptures friends?

3
00:00:18,960 --> 00:00:21,920
I am studying the Majima vulnerable side.

4
00:00:21,920 --> 00:00:32,000
Now, the party watch here means I am familiar with Majima Nigaia, not just studying.

5
00:00:32,640 --> 00:00:37,440
I am familiar with that. I am well acquainted with Majima Nigaia.

6
00:00:37,440 --> 00:00:56,000
And then on the next page, when a man is still learning the first 50 by heart,

7
00:00:56,000 --> 00:01:06,080
at me, the Hollywood is reciting, reciting the first 50. He is faced with the middle 50.

8
00:01:06,080 --> 00:01:11,120
And when he is still reciting that by heart, he is faced with the last 50.

9
00:01:11,760 --> 00:01:20,800
There are all together 150, actually, three more. 150 suitors in that collection

10
00:01:20,800 --> 00:01:29,520
of middle length scenes. And they are divided into three, the first, the second and the third group.

11
00:01:29,520 --> 00:01:34,240
So, when you are reciting the first group, the second group may come to you and you may mix up.

12
00:01:34,240 --> 00:01:39,440
And when you are reciting the second group, the third may come to your mind and you will mix them up.

13
00:01:39,440 --> 00:01:47,040
So, it is not an easy task being familiar with the Majima Nigaia, that is what is meant here.

14
00:01:47,040 --> 00:01:51,920
So, it is not learning, but reciting.

15
00:01:56,800 --> 00:02:04,960
And in paragraph 52, then he recited the Ghatukata to the because,

16
00:02:05,600 --> 00:02:12,240
recited him in thought then, not just, not just recite to them. He taught them Dhatukata,

17
00:02:12,240 --> 00:02:19,360
Dhatukata is the third book of Abidama.

18
00:02:26,800 --> 00:02:28,880
And then, in the next paragraph,

19
00:02:28,880 --> 00:02:46,240
about 10 lines from the bottom, go and learn it from our own teachers. You find that?

20
00:02:46,240 --> 00:03:01,600
How much should be your? Because, at this moment, through claim that he would expound the three pedagars

21
00:03:02,960 --> 00:03:12,240
without studying the commentaries. So, the other Majima wanted to make him realize that

22
00:03:12,240 --> 00:03:18,400
he was not qualified to do that. So, he asked a question and the other

23
00:03:18,400 --> 00:03:23,440
Majima gave an answer. And then, the Majima said, hmm, something like,

24
00:03:24,640 --> 00:03:30,880
many, it is not correct or something. Then, again, three times, he asked this question and three

25
00:03:30,880 --> 00:03:39,680
times, he said, hmm. So, he gave the different answer for different times. So, the other

26
00:03:39,680 --> 00:03:46,400
leader would say that the first answer you gave was a correct answer. But, since you have not

27
00:03:46,400 --> 00:03:53,920
learned from a teacher, you are not firm on your answers. And so, when I say whom, you give

28
00:03:53,920 --> 00:04:00,560
another different answer and then another. So, go and learn from your own teachers,

29
00:04:00,560 --> 00:04:13,520
not just reading books or something. Even in these modern times,

30
00:04:16,880 --> 00:04:21,040
just learning from books is not quite enough.

31
00:04:21,040 --> 00:04:31,600
Now, for example, Abirah, you need a teacher or a friend to help you. Otherwise,

32
00:04:32,640 --> 00:04:40,560
you will not understand properly. So, go and learn it from your own teachers.

33
00:04:40,560 --> 00:04:49,840
And then, where shall I go? And so, he went there. And then, on the next page,

34
00:04:51,520 --> 00:05:00,400
close to 55. What can I, what are you saying? When was, if I not heard it all from you?

35
00:05:00,400 --> 00:05:10,640
Now, after learning from that monk, the monk asked him to give, I mean, after teaching that monk,

36
00:05:11,760 --> 00:05:18,560
the teacher monk asked the pupil monk to give him a subject of meditation. So, the monk said,

37
00:05:18,560 --> 00:05:24,800
what I was saying, when was, have I not heard it all from you? You have been teaching me these

38
00:05:24,800 --> 00:05:31,200
things and now you are asking me to give you a subject of meditation. What can I explain to you

39
00:05:31,200 --> 00:05:36,800
that you do not already know? Then, the senior elder said, this part is different for one who has

40
00:05:36,800 --> 00:05:44,720
actually travelled by it. The Bali sentence really means, this is the part of one who has actually

41
00:05:44,720 --> 00:05:54,560
travelled by it. That means, I know only from books, but you have practiced meditation and

42
00:05:54,560 --> 00:06:03,760
you have gained some enlightenment. So, this part is different for me. So, please teach me meditation.

43
00:06:03,760 --> 00:06:11,520
Although, I have taught you the books. So, that is what is meant here. So, this part is different

44
00:06:11,520 --> 00:06:18,400
for one who is actually travelled by it or it means, this is the part of one who has actually travelled

45
00:06:18,400 --> 00:06:27,360
by it. So, those who have not travelled by it do not really know this part. And, I have not travelled

46
00:06:27,360 --> 00:06:35,840
this part. So, please teach me meditation, something like that. Then, in the next paragraph,

47
00:06:36,560 --> 00:06:41,440
the Arajan's path be fits our teacher. That means, the attainment of Arajan's shape.

48
00:06:41,440 --> 00:06:49,280
So, the teacher practiced meditation and became an Arajan. So, the people said, the Arajan

49
00:06:49,280 --> 00:06:58,640
shape be fits our teacher. And then, in the next paragraph, super normal powers regarding

50
00:06:58,640 --> 00:07:08,560
super normal powers. They are hard to maintain. Like a prone infant or like a, what?

51
00:07:10,800 --> 00:07:18,560
Young corn in my book, like a baby hair. First, prone infant, what is prone?

52
00:07:18,560 --> 00:07:37,440
Face down or face up? Both? Yeah, face down? I do not know, just mother flat.

53
00:07:37,440 --> 00:07:56,720
I do not know. In Pali, it means lying down, face up. And like a, what do you have there?

54
00:07:56,720 --> 00:08:06,800
Like a young corn. That means, you are a tender crow. That is fragile.

55
00:08:09,120 --> 00:08:19,040
In the first edition, he had a baby hair because he misread the Pali word. The Pali word is

56
00:08:19,040 --> 00:08:34,880
S-A-S-A. But he read it as S-A-S-A. I know, the Pali word is S-A-S-A. And he read it as S-A-S-A.

57
00:08:36,000 --> 00:08:46,000
S-A-S-A means a hair or a rabbit. S-A-S-A means a crop or corn or something that grows.

58
00:08:46,000 --> 00:08:54,880
S-A-S-A-S-A. So, I think the editors are he himself and corrected it in the second edition.

59
00:08:59,920 --> 00:09:08,320
So, super normal powers and infatement only for Vibasana Magidition because they are gained

60
00:09:08,320 --> 00:09:15,760
through the practice of samadhi. So, they are not the impediment for samadhi or samadha

61
00:09:15,760 --> 00:09:23,200
meditation. But for Vibasana Magidition, they are impediments. Now, approaching a good friend,

62
00:09:23,200 --> 00:09:31,280
the giver of a meditation subject. So, you must find a teacher who can give you a meditation subject.

63
00:09:31,280 --> 00:09:41,440
And approaching a teacher is described in detail. And it is very different from the practice

64
00:09:42,480 --> 00:09:48,960
especially in this country or in the West. Here, teachers want to attract few bulls and so

65
00:09:48,960 --> 00:10:01,200
they are very very willing and very eager to teach and something like that. But here,

66
00:10:01,200 --> 00:10:09,360
the one who wants to practice meditation has to approach a teacher very in a very careful way

67
00:10:10,320 --> 00:10:15,360
and then not to offend him and so on. So, it is very different.

68
00:10:15,360 --> 00:10:20,640
And here, teachers want to please those who come to them.

69
00:10:26,240 --> 00:10:33,200
Now, the practice of loving kindness meditation is mentioned here, right?

70
00:10:34,640 --> 00:10:39,680
May they be happy and free from reflection. Then, he should develop it towards all deities

71
00:10:39,680 --> 00:10:47,120
within the boundary or now, the boundary on page 98. Finally, Vibhiku takes up a meditation subject.

72
00:10:47,120 --> 00:10:51,920
He should first develop loving kindness towards the community of Vibhiku within the boundary

73
00:10:51,920 --> 00:11:11,040
means within the boundary of the monastery. The party what he used is Sima, which also means

74
00:11:11,040 --> 00:11:20,400
a consecrated place where formal acts of sangha are performed. But here, Sima simply means a boundary.

75
00:11:20,400 --> 00:11:31,920
So, there is the boundary of a monastery. So, loving kindness towards the monks living in the

76
00:11:31,920 --> 00:11:37,840
monastery, just that may be happy and free from reflection. Then, he should develop it towards

77
00:11:37,840 --> 00:11:43,680
all deities within the boundary that means within the monastery. Then, towards all principal people

78
00:11:43,680 --> 00:11:53,040
in the village, that is, his arms resort where he goes for arms. Then, to all human beings there and

79
00:11:53,040 --> 00:11:59,520
to all living beings dependent on the human beings, this is also misunderstanding of the war.

80
00:11:59,520 --> 00:12:20,480
One word in Pali, what really is meant here is that then to all beings beginning with human beings.

81
00:12:21,760 --> 00:12:28,000
So, after loving kindness thoughts to principal people in the village,

82
00:12:28,000 --> 00:12:35,600
that means a village had men or an official in the village, he should send thoughts to all beings

83
00:12:35,600 --> 00:12:42,480
beginning with to all human beings. So, may all human beings be well happy and peaceful and then

84
00:12:42,480 --> 00:12:50,880
may all beings be well happy and peaceful. So, not all human beings there and to all living

85
00:12:50,880 --> 00:12:57,280
beings dependent on the human beings. No, that is not so. So, that the translation should be

86
00:12:57,280 --> 00:13:03,360
then to all beings beginning with human beings.

87
00:13:06,400 --> 00:13:09,200
What does it mean to its all deities?

88
00:13:10,320 --> 00:13:19,040
The spirits, the tree spirits and the guardian spirits living in the precinct of the monastery.

89
00:13:19,040 --> 00:13:28,560
So, we believe that there are spirits all around and tree spirits,

90
00:13:29,920 --> 00:13:36,160
guardian spirits, guardian of the monastery, guardian of the person and so on. So, we send thoughts to them.

91
00:13:43,040 --> 00:13:47,600
And then with next paragraph, with mindfulness of death thinking, I have got to die,

92
00:13:47,600 --> 00:13:54,080
he gives up improper search. And with the growing sense of urgency, he comes to live without

93
00:13:54,080 --> 00:13:58,320
attachment. Actually, he comes to live without sluggishness, not attachment.

94
00:14:00,240 --> 00:14:06,480
When there is an urgent sense of urgency, when there is the thinking that I have got to die,

95
00:14:06,480 --> 00:14:13,600
then I can effort to be lazy. I can effort to be sluggish. I must practice meditation, I must make

96
00:14:13,600 --> 00:14:22,000
effort. So, the word here should be not attachment, but sluggishness. Is there another word for that,

97
00:14:22,880 --> 00:14:30,880
laziness, something like that? Indulant, right? To live without Indulance or sluggishness.

98
00:14:30,880 --> 00:14:47,200
That means to be energetic in the practice of meditation. And then, paragraph 61, the good friend,

99
00:14:47,200 --> 00:14:53,360
he is revered and dearly loved and the one who speaks and suffers speech. The speech he

100
00:14:53,360 --> 00:15:02,000
addresses is profound, he does not judge without a reason. That means to do improper things. He is

101
00:15:02,000 --> 00:15:10,560
wholly solicitous of welfare and partial to progress. What is partial to progress? Does it make sense?

102
00:15:10,560 --> 00:15:23,360
A partial, often means favors. But here, the meaning is, he is on the side of progress.

103
00:15:24,240 --> 00:15:32,880
That means he is making progress. So, I look at, I look at this in the dictionary, it says,

104
00:15:32,880 --> 00:15:41,200
to be found of something. But it does not mean to be found of progress. He is on the side of progress.

105
00:15:41,200 --> 00:15:43,040
So, he is progressing? Progressing, yes.

106
00:15:43,040 --> 00:16:07,280
And our next page, about seven lines from the top, the end of the paragraph, has reached the

107
00:16:07,280 --> 00:16:12,800
destruction of kankas by augmenting insight. That means by practicing insight.

108
00:16:18,960 --> 00:16:26,480
That is just Jana as a approximate cause. And then, next paragraph, I am one whose kankas

109
00:16:26,480 --> 00:16:33,120
are destroyed. Why not? He declares himself and he knows that his instructions will be carried out.

110
00:16:33,120 --> 00:16:41,440
Actually, he knows that the other one is a practitioner. Not that his instructions will be carried

111
00:16:41,440 --> 00:16:49,520
out. If you know that the other one is a meditator, then you may tell him what you have attained.

112
00:16:50,240 --> 00:16:56,240
But usually, normally, monks do not tell the attainment to other people.

113
00:16:56,240 --> 00:17:04,240
So, they will be practiced, instead of carrying out his instructions. Not even his instructions.

114
00:17:05,840 --> 00:17:17,200
But he could visit his instructions, but yeah, practiced. He knows that the other one is

115
00:17:17,200 --> 00:17:30,800
a practitioner. The other one is the one who practices meditation. And then, paragraph 70,

116
00:17:31,680 --> 00:17:38,800
on page 101, about four lines down. If he does not allow it when asked then,

117
00:17:38,800 --> 00:17:47,920
they can be done when the opportunity opens. When he does them, three, two sticks should be brought.

118
00:17:47,920 --> 00:17:53,520
That means should be given to him, should be presented if more a medium and a big one.

119
00:17:53,520 --> 00:17:57,840
And two kinds of mud, washing water and bathing water. That is hot and cold.

120
00:17:57,840 --> 00:18:11,200
And then on the next page, paragraph 72, if he does not ask, but agrees to the duties being done,

121
00:18:11,200 --> 00:18:20,400
then after 10 days or a fortnight have gone, he should make an opportunity by staying back one day

122
00:18:20,400 --> 00:18:29,840
at the time of his dismissal. That means, even though he is dismissed,

123
00:18:33,440 --> 00:18:42,240
he should ask permission and announce his purpose in coming to that month, to that teacher.

124
00:18:42,240 --> 00:18:52,960
So, making an opportunity, I don't know what that means. The value word here is to ask permission.

125
00:18:58,160 --> 00:19:01,760
So, when he is dismissed, he must not go away.

126
00:19:01,760 --> 00:19:13,840
So, that means, he must not go away, but ask him permission. I want to see something.

127
00:19:13,840 --> 00:19:18,400
And then when he tells him that he comes for the practice of meditation.

128
00:19:18,400 --> 00:19:26,080
So, it is how to approach a teacher.

129
00:19:33,280 --> 00:19:34,160
Okay, there.

130
00:19:38,000 --> 00:19:40,640
We will talk about temperaments next week.

131
00:19:40,640 --> 00:19:47,680
The explanation about temperaments is very interesting.

132
00:20:11,600 --> 00:20:26,800
I am already taking my next time about when we come next and how I look at the next chunk

133
00:20:26,800 --> 00:20:42,720
that we can do is quite long.

