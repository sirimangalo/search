1
00:00:00,000 --> 00:00:12,640
Okay, here we are under the Bodhi tree in front of the Mahabodhi Chaitya.

2
00:00:12,640 --> 00:00:21,120
Mahamakapuja, the Fomun of Maga in Bodhgaya India.

3
00:00:21,120 --> 00:00:29,960
And this is a place where the Bodhisattas journey came to fruition.

4
00:00:29,960 --> 00:00:44,800
So, to recap, sometime in the far, far distant past, there was a ascetic named Sumeda.

5
00:00:44,800 --> 00:00:51,600
And in all the world, he was one of the greatest, if not the greatest ascetic.

6
00:00:51,600 --> 00:00:59,400
He was well known, renowned throughout India as a very spiritually advanced individual.

7
00:00:59,400 --> 00:01:11,680
And one day he came down from the mountains and heard that the Buddha had arisen, and

8
00:01:11,680 --> 00:01:16,120
he thought to himself, for sure, if I listened to the Buddha's teaching, I would be able

9
00:01:16,120 --> 00:01:21,320
to free myself from suffering.

10
00:01:21,320 --> 00:01:27,320
And so all the people were clearing a path for the Buddha to walk, and he thought, well,

11
00:01:27,320 --> 00:01:36,360
this would be a great act of merit, and so he joined them.

12
00:01:36,360 --> 00:01:41,320
But then when he saw the Buddha coming, he realized something.

13
00:01:41,320 --> 00:01:48,800
He realized that with his great spiritual attainment, that maybe just maybe he would be able

14
00:01:48,800 --> 00:01:56,360
to himself become a Buddha, instead of listening to the Buddha's teachings, he could

15
00:01:56,360 --> 00:02:03,240
make a vow to himself, to one day, be just like that.

16
00:02:03,240 --> 00:02:09,000
Deepankarabuddha was the Buddha at the time.

17
00:02:09,000 --> 00:02:16,000
And so instead of waiting to listen to the Buddha's teaching, he lay himself down in the mud,

18
00:02:16,000 --> 00:02:25,640
and made a vow to himself that his body should be a bridge across the uneven muddy landscape.

19
00:02:25,640 --> 00:02:32,080
And even though he would probably die as a result of being trod on by all these monks,

20
00:02:32,080 --> 00:02:39,760
that that should be his sacrifice for the purpose of becoming a Buddha himself.

21
00:02:39,760 --> 00:02:45,840
Now, Deepankarab saw him lying in the mud and said, said to his monks, you see that

22
00:02:45,840 --> 00:02:54,000
ascetic lying in the mud, one day he will become a Buddha.

23
00:02:54,000 --> 00:03:03,600
And then for four Asankaya and 100,000 great Kapa, great Iones, this ascetic boat was born

24
00:03:03,600 --> 00:03:14,520
and died and born and died and spent countless lifetimes cultivating the perfection.

25
00:03:14,520 --> 00:03:21,480
The time that it took, you know, thinking about this moment that we're talking about,

26
00:03:21,480 --> 00:03:29,400
the moment of the Buddha's enlightenment, that this place is meant to venerate.

27
00:03:29,400 --> 00:03:37,560
And Asankaya, the word Asankaya means uncountable.

28
00:03:37,560 --> 00:03:42,000
But to get an idea, the question was, well, if it's uncountable, can you give an idea

29
00:03:42,000 --> 00:03:45,000
of what it's like?

30
00:03:45,000 --> 00:03:59,120
And so they similarly, suppose you had a, suppose you had a pit, one league wide, 16

31
00:03:59,120 --> 00:04:07,440
kilometers wide, 16 kilometers across, 16 kilometers deep.

32
00:04:07,440 --> 00:04:19,320
And every 100 angel years, 100 angel years, someone were to drop a grain of rice into the

33
00:04:19,320 --> 00:04:23,360
pit.

34
00:04:23,360 --> 00:04:30,360
An angel year, time in heaven is supposed to be different from time on earth.

35
00:04:30,360 --> 00:04:40,920
So they say that one day in the angel world is 100 years under.

36
00:04:40,920 --> 00:04:54,120
There's a story of an angel, two angels, a husband and a wife, the king and the queen maybe.

37
00:04:54,120 --> 00:04:58,320
And one morning the king woke up, the angel woke up and his wife, he couldn't see his

38
00:04:58,320 --> 00:05:08,240
wife and his wife had passed away and boy had been reborn as a human being.

39
00:05:08,240 --> 00:05:15,760
But she remembered being an angel and so she spent 60 years or so on earth doing good deeds

40
00:05:15,760 --> 00:05:19,800
to try and be reborn in heaven.

41
00:05:19,800 --> 00:05:23,200
And then in the afternoon, in the angel world, she was born again and her husband said,

42
00:05:23,200 --> 00:05:28,360
Oh, where did you go this morning?

43
00:05:28,360 --> 00:05:35,200
She explained, she explained, oh, she had passed away and she had been lived her life.

44
00:05:35,200 --> 00:05:40,520
On earth, they were always asking her, why are you doing all these good deeds?

45
00:05:40,520 --> 00:05:46,320
Her husband had died on earth, her husband had died and she did all these good deeds.

46
00:05:46,320 --> 00:05:47,320
And they asked, why are you doing it?

47
00:05:47,320 --> 00:05:50,080
Oh, when I die, I want to be with my husband again.

48
00:05:50,080 --> 00:05:51,880
And they thought, wow, she's really dedicated.

49
00:05:51,880 --> 00:05:54,320
It was her husband in heaven.

50
00:05:54,320 --> 00:05:58,280
So she explained this to her husband once she'd returned to heaven, when she'd been

51
00:05:58,280 --> 00:06:01,480
reborn there again.

52
00:06:01,480 --> 00:06:06,600
And he said to himself, wow, the life of humans is so short.

53
00:06:06,600 --> 00:06:12,560
And he asked if human beings do good deeds and she said, no, mostly they're negligent,

54
00:06:12,560 --> 00:06:14,280
mostly they don't.

55
00:06:14,280 --> 00:06:18,640
And he couldn't believe with such a short lifetime that they didn't do good deeds.

56
00:06:18,640 --> 00:06:23,600
Anyway, the point being that life in heaven is much, much longer.

57
00:06:23,600 --> 00:06:33,120
So 100 years as an angel is about three and a half million human years.

58
00:06:33,120 --> 00:06:37,320
So every three and a half million years, which is how long humans have been here on earth,

59
00:06:37,320 --> 00:06:47,960
I think something like that, drop a rice, grain of rice into the pit, every 100 years.

60
00:06:47,960 --> 00:06:55,560
And then eventually the pit would fill up with rice, the canyon basically great cousin.

61
00:06:55,560 --> 00:06:59,960
And once it was full, someone, suppose someone were to take a grain of rice out every 100

62
00:06:59,960 --> 00:07:02,960
angel years.

63
00:07:02,960 --> 00:07:08,720
The pit would become full and empty again before Anasankaya was up.

64
00:07:08,720 --> 00:07:10,160
That's how long Anasankaya is.

65
00:07:10,160 --> 00:07:13,400
That's not how long it's longer than that.

66
00:07:13,400 --> 00:07:21,840
The four of those, somehow you can count them, a long time.

67
00:07:21,840 --> 00:07:30,960
And then in his last birth, he was born, Siddhartha in Lumbini, and he grew up in Kapilawatu

68
00:07:30,960 --> 00:07:38,400
for 29 years after 29 years, he left home, practiced with two teachers, practiced for

69
00:07:38,400 --> 00:07:46,400
six years, nearby torturing himself, before coming to Bodhgaya under the Bodhi tree and

70
00:07:46,400 --> 00:07:59,320
realizing that neither torturing himself nor engaging in sensual pleasures was the way

71
00:07:59,320 --> 00:08:06,040
to find enlightenment, and so he found the middle way.

72
00:08:06,040 --> 00:08:10,920
He spent all night under the Bodhi tree in the first watch of the night.

73
00:08:10,920 --> 00:08:15,720
He remembered his past lives, we remembered he'd been so many things.

74
00:08:15,720 --> 00:08:26,320
He'd been a human, he'd been an animal, he'd been a god, he'd been an angel.

75
00:08:26,320 --> 00:08:32,400
In the second watch of the night, he started to think about what it means to be reborn,

76
00:08:32,400 --> 00:08:36,120
not just the fact that we're born as many different things, but that there's reasons

77
00:08:36,120 --> 00:08:41,760
why we're born as in different ways.

78
00:08:41,760 --> 00:08:46,680
So he saw beings arising and passing away according to their karma.

79
00:08:46,680 --> 00:08:53,680
At the state of our minds, the habits and the qualities of mind that we cultivate, he could

80
00:08:53,680 --> 00:08:55,480
see how that affected people.

81
00:08:55,480 --> 00:09:01,000
He would be able to look at people and understand how they'd be born as they are, look

82
00:09:01,000 --> 00:09:08,760
at animals, look at gods and angels and so on.

83
00:09:08,760 --> 00:09:13,360
And in the third watch of the night, he understood cause and effect, so it's basically

84
00:09:13,360 --> 00:09:20,240
a progression from that where not just born and die born and die, he started to see

85
00:09:20,240 --> 00:09:23,080
how it's going moment to moment.

86
00:09:23,080 --> 00:09:34,320
Because of their ignorance, because they don't see what they're doing, they do sometimes

87
00:09:34,320 --> 00:09:41,880
good things, sometimes bad things and they seem to be floating around aimlessly on an

88
00:09:41,880 --> 00:09:51,920
ocean of Samsar and then he saw more precisely Minyana Pachea Sankara Pachea Winyana,

89
00:09:51,920 --> 00:10:01,840
Minyana Pachea Namarupa, how consciousness leads to experience and experience leads to

90
00:10:01,840 --> 00:10:11,520
craving, which leads to clinging, which leads to becoming, which leads us to create

91
00:10:11,520 --> 00:10:17,440
and to be reborn.

92
00:10:17,440 --> 00:10:23,480
And it was based on that in summary that he was able to free himself from suffering,

93
00:10:23,480 --> 00:10:33,280
free himself from the causes of suffering and therefore free himself from suffering.

94
00:10:33,280 --> 00:10:37,760
And this is the place, this is the place where that the Buddha said,

95
00:10:37,760 --> 00:10:45,040
we are going to learn from the body, we are going to learn from the body, we are going

96
00:10:45,040 --> 00:10:49,560
to learn from the body, we are going to learn from the body.

97
00:10:49,560 --> 00:10:57,060
Our local light, so we talk about enlightenment, if you remember in Kucinara, I said it's

98
00:10:57,060 --> 00:11:03,360
the maybe the most peaceful place on earth.

99
00:11:03,360 --> 00:11:08,400
This place is, this place is like the brightest place on earth.

100
00:11:08,400 --> 00:11:09,760
This is where light came.

101
00:11:12,720 --> 00:11:13,920
The light of enlightenment.

102
00:11:19,680 --> 00:11:20,880
So we talk about...

103
00:11:25,440 --> 00:11:26,880
We talk about enlightenment.

104
00:11:26,880 --> 00:11:33,360
What it didn't mean that the Buddha became enlightened.

105
00:11:37,360 --> 00:11:40,800
So it's important to understand what isn't enlightenment I think.

106
00:11:42,240 --> 00:11:45,680
Because when we talk about the Buddha's enlightenment,

107
00:11:45,680 --> 00:11:49,360
we're not talking about the time that he spent to become enlightened.

108
00:11:49,360 --> 00:11:51,200
We're not talking about the six years.

109
00:11:51,200 --> 00:11:54,960
We're not even talking about the time all night that he spent on the Bodhi tree.

110
00:11:54,960 --> 00:11:57,920
We're actually talking about just a moment.

111
00:12:00,000 --> 00:12:05,920
Enlightenment is a momentary flash of realization.

112
00:12:07,680 --> 00:12:09,600
And it's what the Buddha called the four noble truths.

113
00:12:11,520 --> 00:12:13,040
So what did the Buddha become enlightened?

114
00:12:13,040 --> 00:12:19,120
We became enlightened to the four noble truths which are quite familiar to anyone who knows anything

115
00:12:19,120 --> 00:12:25,360
about Buddhism.

116
00:12:25,360 --> 00:12:29,360
And so what this means is, first of all, two things.

117
00:12:31,120 --> 00:12:33,120
We talk about what isn't enlightenment.

118
00:12:35,760 --> 00:12:46,320
For many people, there's this idea that helping other people is enlightenment,

119
00:12:46,320 --> 00:12:51,040
that it's enlightened to gain the right qualities of mind and do the right thing.

120
00:12:52,400 --> 00:12:58,960
You know, there are many types of Buddhism that talk about putting aside one's own freedom from suffering,

121
00:13:00,000 --> 00:13:05,920
putting aside one's own attainment as a means of enlightenment.

122
00:13:05,920 --> 00:13:17,760
Because when we talk about the Buddha's enlightenment, we see that it's actually limited.

123
00:13:17,760 --> 00:13:19,360
There's a limit to it.

124
00:13:19,360 --> 00:13:22,960
The Buddha didn't stay around, the Buddha didn't continue on in the world.

125
00:13:22,960 --> 00:13:23,920
The Buddha isn't here.

126
00:13:24,880 --> 00:13:30,880
What we have left is teachings, what we have left is the physical presence.

127
00:13:30,880 --> 00:13:39,600
And so looking for something greater than that, there's this idea of staying in the world.

128
00:13:39,600 --> 00:13:40,960
And that's not enlightenment.

129
00:13:44,000 --> 00:13:48,960
And the other thing that is not enlightenment is the path and the practice that we are doing.

130
00:13:48,960 --> 00:13:53,120
We have to be able to distinguish that our practice isn't enlightenment.

131
00:13:53,120 --> 00:14:07,200
Because there can be this idea that the gains that we get from mindfulness meditation

132
00:14:07,200 --> 00:14:14,720
that we get from inside from seeing clearly is somehow the goal of the practice, right?

133
00:14:14,720 --> 00:14:19,680
Sometimes the way we phrase and the way we present insight meditation is such that

134
00:14:19,680 --> 00:14:26,480
it appears that the peace and the clarity that comes from being mindful,

135
00:14:27,280 --> 00:14:31,520
that that's the goal, that it's somehow a gradual thing.

136
00:14:33,760 --> 00:14:38,080
And so we have to, at least technically, we have to distinguish the two.

137
00:14:38,720 --> 00:14:43,840
It's not to say that our practice is disconnected from enlightenment, but it's something separate.

138
00:14:43,840 --> 00:14:52,720
What we practice when we practice mindfulness is called the preliminary path.

139
00:14:55,040 --> 00:14:59,600
We're practicing all of the qualities of the 8th of noble path, we're practicing

140
00:15:01,120 --> 00:15:02,800
in line with the four noble truths.

141
00:15:04,800 --> 00:15:06,640
But all of it could disappear.

142
00:15:06,640 --> 00:15:14,400
It comes and it goes if we stop practicing, if we give it up, we might forget it all.

143
00:15:14,400 --> 00:15:26,400
Furthermore, it's not powerful enough to categorically or completely change the perspective of the mind.

144
00:15:28,560 --> 00:15:34,560
They liken it to heating up two pieces of wood when you want to lay the fire.

145
00:15:34,560 --> 00:15:42,240
The heat is the same, there might even be smoke, but until you have the ignition,

146
00:15:44,160 --> 00:15:50,480
you can't say that you've lit the fire, and as soon as you stop, the heat starts to disappear.

147
00:15:50,480 --> 00:16:04,480
So these two things are not enlightenment, referring to the idea of putting aside your own happiness

148
00:16:04,480 --> 00:16:05,760
and your own peace for others.

149
00:16:09,200 --> 00:16:15,440
Why you couldn't call it enlightenment and why we have to be careful to understand that we

150
00:16:15,440 --> 00:16:27,280
can't consider enlightenment. It lacks the power to actually bring about a goal, a meaningful goal.

151
00:16:28,720 --> 00:16:35,280
It rests on the assumption, first of all, that you could ever come to an end of helping.

152
00:16:37,760 --> 00:16:41,840
And even if you could come to an end to helping, meaning you could help everyone, really.

153
00:16:41,840 --> 00:16:46,320
That's the only way it could be meaningful.

154
00:16:46,320 --> 00:16:54,240
The other assumption is that it actually helps someone to interact with them without being enlightened.

155
00:16:56,240 --> 00:17:00,160
New yourself not being free from suffering that you should free someone from suffering.

156
00:17:01,200 --> 00:17:09,520
New yourself not being free from ignorance should help someone become free from ignorance.

157
00:17:09,520 --> 00:17:12,880
And it's simply not powerful enough.

158
00:17:15,440 --> 00:17:20,000
The reality of it is that your interactions with them are going to be good and bad or going to

159
00:17:21,760 --> 00:17:26,000
be dependent on your own defilements, your own ignorance and so on.

160
00:17:27,840 --> 00:17:31,840
And this is why without the sort of thing that the Buddha did,

161
00:17:31,840 --> 00:17:38,960
you see the world coming and going better and worse,

162
00:17:40,240 --> 00:17:45,760
you see people cultivating good qualities and cultivating evil qualities back and forth.

163
00:17:50,000 --> 00:17:58,560
In opposition, to that we have instead with the Buddha did as an example.

164
00:17:58,560 --> 00:18:03,280
We have his enlightenment that freed him from suffering,

165
00:18:04,800 --> 00:18:10,560
it allowed him to teach other people and free them from suffering.

166
00:18:12,240 --> 00:18:16,080
And it also allowed him to teach people to carry on his teaching.

167
00:18:19,760 --> 00:18:21,120
And so that's what we mean by

168
00:18:21,120 --> 00:18:28,800
that's the sort of enlightenment that we can call true enlightenment.

169
00:18:34,080 --> 00:18:42,000
So our understanding of enlightenment

170
00:18:42,000 --> 00:18:51,360
When we talk about what the Four Noble Truths means in the realization

171
00:18:52,720 --> 00:18:54,720
and the understanding of the Four Noble Truths,

172
00:18:54,720 --> 00:19:21,920
we're talking about the clearest possible experience of reality.

173
00:19:21,920 --> 00:19:25,520
So it is very much related to our practice.

174
00:19:30,160 --> 00:19:32,320
We talk about mindfulness and

175
00:19:34,400 --> 00:19:40,240
we talk about mindfulness and meditation in very simple terms in terms of when we see,

176
00:19:40,240 --> 00:19:46,800
when we hear very mundane terms relating to ordinary everyday experience.

177
00:19:46,800 --> 00:19:53,040
So it's important to understand enlightenment is not disconnected from that.

178
00:19:53,040 --> 00:19:57,760
It's not something esoteric or remote or mysterious.

179
00:20:01,600 --> 00:20:06,160
It's simply that when our experience, when our observation,

180
00:20:07,840 --> 00:20:10,800
when our clarity of mind becomes perfect,

181
00:20:10,800 --> 00:20:16,160
then it has the power to free us from suffering.

182
00:20:16,160 --> 00:20:21,920
It has the power to give rise to a moment of insight.

183
00:20:21,920 --> 00:20:24,400
And this is talking about the Four Noble Truths.

184
00:20:24,400 --> 00:20:29,440
So our practice of studying suffering,

185
00:20:31,280 --> 00:20:34,240
our practice of abandoning the origin of suffering,

186
00:20:34,240 --> 00:20:41,120
our practice of becoming free from suffering as a result,

187
00:20:42,000 --> 00:20:44,080
our practice of cultivating the path,

188
00:20:45,360 --> 00:20:48,160
all of these things build and build and build.

189
00:20:51,600 --> 00:20:55,600
And eventually we see, everything is dukkha,

190
00:20:55,600 --> 00:20:58,720
everything that arises is dukkha.

191
00:20:58,720 --> 00:21:04,800
It doesn't mean suffering, it means we see that these things are not worth clinging to.

192
00:21:06,640 --> 00:21:08,720
That there is nothing that can satisfy us.

193
00:21:08,720 --> 00:21:14,400
There is nothing that we can attain or obtain that will bring us through happiness.

194
00:21:14,400 --> 00:21:28,640
We see through craving that is not worth engaging and it's not worth giving rise to craving.

195
00:21:28,640 --> 00:21:30,720
We see how our craving leads to suffering.

196
00:21:37,840 --> 00:21:39,280
And we see the cessation.

197
00:21:39,280 --> 00:21:50,800
We have a moment where our minds let go and release from this samsara,

198
00:21:52,320 --> 00:21:57,200
this incessant arising of seeing and hearing and smelling and tasting and feeling and thinking.

199
00:22:02,000 --> 00:22:07,360
We have a moment where we're perfectly in line with what we call the Eightfold Noble Path,

200
00:22:07,360 --> 00:22:14,240
where the path, our way, our way of looking at the world is perfect.

201
00:22:16,080 --> 00:22:18,480
Right view and right thoughts and so on.

202
00:22:18,480 --> 00:22:37,920
There's nothing, there's no imperfections to our perception.

203
00:22:37,920 --> 00:22:43,600
And so when we talk about enlightenment, we mean two things.

204
00:22:43,600 --> 00:22:48,640
We mean the ability that the Buddha had to present this to teach this

205
00:22:51,040 --> 00:22:54,000
and the actual realization for a bit for himself.

206
00:22:55,520 --> 00:23:00,880
So the Buddha's knowledge that this was the truth, that this was the

207
00:23:03,040 --> 00:23:07,600
the truth of enlightenment, this was the meaning of enlightenment or true enlightenment.

208
00:23:07,600 --> 00:23:12,160
That's part of the Buddha's enlightenment.

209
00:23:14,160 --> 00:23:18,160
The other part was the actual, of course, the realization.

210
00:23:19,760 --> 00:23:23,840
So when the Buddha realized enlightenment, he became free from suffering,

211
00:23:24,800 --> 00:23:27,760
but he also understood what it was that he had attained.

212
00:23:29,040 --> 00:23:33,280
That's why we talk about the difference between someone who follows the Buddha

213
00:23:33,280 --> 00:23:36,640
and someone who becomes a Buddha.

214
00:23:37,680 --> 00:23:40,320
For all of us, we have followed after the Buddha.

215
00:23:41,360 --> 00:23:45,280
We have undertaken the practice according to his teaching.

216
00:23:46,800 --> 00:23:54,880
He has set forth the Four Noble Truths and the Eightfold Noble Path.

217
00:23:58,000 --> 00:23:59,040
And we follow the Kordi.

218
00:23:59,040 --> 00:24:06,240
But we ourselves, just simply because we practice, we have no, as a result,

219
00:24:06,960 --> 00:24:10,960
necessarily ability to explain and describe.

220
00:24:13,760 --> 00:24:22,800
Our simple practice doesn't allow us to explain or understand clearly what are the Four Noble Truths.

221
00:24:22,800 --> 00:24:29,440
And so that's what we understand as a difference.

222
00:24:29,440 --> 00:24:34,080
There's often, I think, confusion about what is the difference between becoming a

223
00:24:35,440 --> 00:24:37,120
Buddha and the follower of the Buddha.

224
00:24:41,760 --> 00:24:46,800
And so we have to understand the experience of freedom from suffering is the same.

225
00:24:46,800 --> 00:24:54,480
The Buddha quite clearly explained that there's no difference between his freedom from suffering

226
00:24:54,480 --> 00:24:59,440
and our freedom from suffering, his freedom from the filaments and our freedom from the family.

227
00:25:02,160 --> 00:25:06,960
But he also had the clarity of mind and the depth of mind to be able to teach,

228
00:25:07,520 --> 00:25:09,840
to understand and to be able to pass it on.

229
00:25:09,840 --> 00:25:15,840
And that's the difference.

230
00:25:39,840 --> 00:25:47,920
And so enlightenment is not a hard thing to understand.

231
00:25:49,280 --> 00:25:51,120
It's the culmination of practice.

232
00:25:54,080 --> 00:25:58,480
It's something that it's good to understand, it's good to think of, it's good to look at all the

233
00:25:58,480 --> 00:26:15,040
people here to experience and to give rise in our minds to this concept of enlightenment that

234
00:26:15,040 --> 00:26:29,520
everyone, many people are here wishing for, striving for, making vows to attain.

235
00:26:29,520 --> 00:26:35,840
But likewise it's important in our practice to focus on the cause.

236
00:26:36,640 --> 00:26:40,480
We call it the preliminary path, our practice is just the preliminary path.

237
00:26:40,480 --> 00:26:46,800
But what it means is it's the practice, enlightenment is never the practice,

238
00:26:46,800 --> 00:26:54,640
enlightenment is never the focus of practice, it's simply the perfection of practice.

239
00:26:56,400 --> 00:27:01,040
Because they say practice makes perfect, that's the good summary of what you mean.

240
00:27:05,200 --> 00:27:09,920
When we talk about the difference between Buddha and his followers, that's the only difference,

241
00:27:09,920 --> 00:27:12,640
is that he was able to pass it along.

242
00:27:12,640 --> 00:27:14,240
And for that he took all this time.

243
00:27:18,240 --> 00:27:20,080
When we talk about the difference between

244
00:27:23,120 --> 00:27:29,280
working towards that goal and not working towards that goal or being content, not reaching that goal,

245
00:27:33,360 --> 00:27:35,120
we're able to make that distinction.

246
00:27:35,120 --> 00:27:43,920
But someone who is content with simply being mindful without putting out effort

247
00:27:47,840 --> 00:27:51,920
until they realize enlightenment, but that's not enough.

248
00:27:51,920 --> 00:28:03,520
But someone who puts aside enlightenment to stay in the world to help others

249
00:28:07,680 --> 00:28:11,760
is ignoring the fact that to free someone from suffering you have to be free,

250
00:28:11,760 --> 00:28:23,120
you have to be able to engage with them in a way that is free from the causes of suffering.

251
00:28:24,160 --> 00:28:27,760
You have to understand that enlightenment is something that comes from within you.

252
00:28:28,560 --> 00:28:30,160
You can't enlighten someone else.

253
00:28:30,160 --> 00:28:44,880
You can't share your enlightenment and ultimately your enlightenment is not going to

254
00:28:44,880 --> 00:28:58,560
allow you to help everyone.

255
00:29:00,400 --> 00:29:04,560
And so we come here, we can come and think about Gaia

256
00:29:04,560 --> 00:29:18,720
as the ultimate expression of this perfection of our practice.

257
00:29:21,040 --> 00:29:29,840
We can take this time to practice mindfulness, to cultivate the clarity of mind,

258
00:29:29,840 --> 00:29:40,480
we can see it as the ultimate ideal, that all of our practice that we do in our daily life,

259
00:29:42,320 --> 00:29:48,560
in our walking meditation, in our sitting meditation, when we're at a meditation center or at home.

260
00:29:48,560 --> 00:30:06,240
We put it up as an ideal of perfection that we're working for,

261
00:30:08,160 --> 00:30:10,880
that we should understand suffering and not be bothered by it,

262
00:30:10,880 --> 00:30:18,640
to understand the things that cause us suffering and not let them cause us suffering,

263
00:30:21,200 --> 00:30:26,240
that we should see how our clinging to things, liking them, just liking them, trying to fix them,

264
00:30:26,960 --> 00:30:30,320
all the developments in our mind are only causing us suffering.

265
00:30:30,320 --> 00:30:39,200
That we should release and let go and our minds become free from suffering,

266
00:30:40,720 --> 00:30:47,760
we should cultivate further and further the qualities that allow us to be free from suffering,

267
00:30:47,760 --> 00:30:51,600
the wholesome qualities of mindfulness, concentration, effort,

268
00:30:51,600 --> 00:31:00,720
right view, right thought, right speech, right action, right livelihood, right effort,

269
00:31:00,720 --> 00:31:05,040
right concentration, right mindfulness, right concentration, right mind on this, right concentration.

270
00:31:05,040 --> 00:31:20,880
So that's enlightenment regarding our practice, we have what we call the four stages of enlightenment.

271
00:31:25,360 --> 00:31:31,760
So a person who hasn't practiced to the extent that they're able to have this experience of

272
00:31:31,760 --> 00:31:43,280
letting go, their old habits of mind are repressed and they can always come back,

273
00:31:45,040 --> 00:31:50,960
we can change from meditation, but we can change back if we stop meditating in our next life

274
00:31:50,960 --> 00:32:03,040
or future life, but a person who attains this experience, who attains this one moment

275
00:32:04,000 --> 00:32:13,280
where their experience of reality is perfect, that person changes their foundation of

276
00:32:13,280 --> 00:32:20,880
experience, their foundation of existence, they call it go-turbu, which means changing your lineage,

277
00:32:21,600 --> 00:32:30,880
changing your family, your bloodline, you're no longer part of the old family of ordinary individuals,

278
00:32:31,760 --> 00:32:33,120
you're a different kind of person.

279
00:32:36,240 --> 00:32:42,480
The first type of person we call a sotapana, simply having this experience once means you've entered

280
00:32:42,480 --> 00:32:48,320
sotapana means you've entered the stream, the Buddha said, such a person will only be born

281
00:32:48,320 --> 00:32:58,000
a maximum of seven more lifetimes, they've given a wrong view because they've seen

282
00:32:59,360 --> 00:33:05,760
what is right, they've seen something about reality that has no depth, you couldn't say

283
00:33:05,760 --> 00:33:13,280
it only goes to here and that's the limit of it, there's no depth limit, meaning there is no limit

284
00:33:13,280 --> 00:33:19,440
to it in the universe, everything ceases, when you have this experience of cessation there is nothing

285
00:33:19,440 --> 00:33:26,960
that is left out, nothing that is not ceased or is not subject to cessation, everything that arises

286
00:33:26,960 --> 00:33:36,160
is subject to cessation, you have no doubt, no doubt about this because you've seen it for yourself

287
00:33:37,600 --> 00:33:42,800
and you have no confusion about what is right practice, what is wrong practice,

288
00:33:44,160 --> 00:33:50,880
you have no idea that there are external rituals or prayers or wishes or anything that might be

289
00:33:50,880 --> 00:33:59,200
a part of the path or a cause or a precursor to enlightenment because it's very simple,

290
00:34:00,320 --> 00:34:05,600
you've practiced seeing clearly and you've come to understand that enlightenment simply means

291
00:34:06,640 --> 00:34:16,720
a moment of clear experience where the mind lets go and there's no connection,

292
00:34:16,720 --> 00:34:24,480
no contact with seeing or hearing or smelling or tasting or feeling or even cognizing,

293
00:34:24,480 --> 00:34:30,640
no mental or physical arising, no memory of it, it's beyond memory, you don't even remember it

294
00:34:30,640 --> 00:34:36,720
happening, but you know it happened, after the fact you know it happened, but you don't know

295
00:34:36,720 --> 00:34:45,760
what it was because there was no memory, no perception, no a perception, that's sort of on the

296
00:34:45,760 --> 00:34:54,400
planet, Sakadagami the second stage of enlightenment, such a person will only be born a maximum

297
00:34:54,400 --> 00:35:05,360
of one more time and this is because they've experienced this cessation repeatedly to the point

298
00:35:05,360 --> 00:35:10,800
where they have reduced any further amount of clinging, not eradicated it, but they've

299
00:35:10,800 --> 00:35:15,440
reduced it so much that you could say of such a person they only have one more lifetime.

300
00:35:19,200 --> 00:35:25,680
The third stage of enlightenment, Anagami, they have actually done away with

301
00:35:27,920 --> 00:35:35,760
a version and sensual desire, so they continue on and continue on and

302
00:35:35,760 --> 00:35:40,240
by repeated experience and further clarity and further

303
00:35:43,040 --> 00:35:44,880
experience of cessation,

304
00:35:48,640 --> 00:35:53,920
they come to see that there's no more anger in them, that there's no more craving

305
00:35:53,920 --> 00:35:59,440
for sensuality for beautiful sights or sounds, that they've eradicated it.

306
00:35:59,440 --> 00:36:09,920
And the fourth stage of enlightenment we call Arahan, Arahan simply means one who is worthy,

307
00:36:09,920 --> 00:36:15,120
but Arahan is so sorry, Ananagami, Anagami means they won't come back,

308
00:36:16,560 --> 00:36:22,720
but they will be reborn in the Brahma realms, Ananagami will still be reborn in the higher

309
00:36:22,720 --> 00:36:31,600
Brahma realms, but never again as a human or a Deva angel. But in Arahan, on Arahan when they pass away,

310
00:36:34,080 --> 00:36:41,920
like in Kusinara, there is no returning, there is no coming back, there is no further arising of

311
00:36:43,120 --> 00:36:44,640
mental or physical experience.

312
00:36:44,640 --> 00:36:55,600
And such a person, what they've done away with is ignorance, they've done away with conceit,

313
00:36:55,600 --> 00:37:05,280
they've done away with any kind of mental clutter or chatter, they've done away with any desire for

314
00:37:05,280 --> 00:37:25,680
even intellectual or spiritual pursuits, and they've attained the same freedom from suffering as the

315
00:37:25,680 --> 00:37:36,720
Buddha, and they still live, they continue their lives, they still teach, they still help. We can see

316
00:37:36,720 --> 00:37:43,680
that this is how, this is the reality, and this is what works. Without the Buddha's enlightenment,

317
00:37:43,680 --> 00:37:51,840
there would be no Arahan, without the Arahan's there would be no practice of mindfulness to this day.

318
00:37:51,840 --> 00:37:57,680
There wouldn't even be people thinking of putting aside enlightenment if there hadn't been a Buddha

319
00:37:57,680 --> 00:38:04,640
who had gained enlightenment. There certainly wouldn't be this catchphrase of mindfulness that

320
00:38:04,640 --> 00:38:10,080
we hear about in the West and throughout the world, of people practicing but being content with

321
00:38:10,080 --> 00:38:17,360
ordinary practice. So this is the power and the greatness of enlightenment, this is perhaps the

322
00:38:17,360 --> 00:38:23,600
most important aspect of the Buddha's teaching, the most important place in our journey,

323
00:38:24,960 --> 00:38:33,120
the most important part of Buddhism is the enlightenment. Buddha means one who is enlightened or one

324
00:38:33,120 --> 00:38:46,640
who is awakened, and it's an apt name because that is the focus in Buddhism, not on worship or ritual

325
00:38:46,640 --> 00:38:54,720
or moral precepts or anything, the focus is on wisdom, understanding and enlightening.

326
00:38:56,960 --> 00:39:01,040
So that's where we are today, the brightest, perhaps the brightest spot in the world.

327
00:39:01,040 --> 00:39:10,560
And this is our last day of the pilgrimage. So thank you all for listening and we're keeping up

328
00:39:10,560 --> 00:39:32,720
with the teachings, wish you all freedom from suffering and enlightenment. Thank you.

