1
00:00:00,000 --> 00:00:15,000
I started translating your book on meditation to German, is that okay with you?

2
00:00:15,000 --> 00:00:20,000
And is it okay to distribute it then to my friends once if you're translating it?

3
00:00:20,000 --> 00:00:28,000
I have no problem with people doing anything they want with my books or anything I make.

4
00:00:28,000 --> 00:00:29,000
Go ahead.

5
00:00:29,000 --> 00:00:37,000
I mean, I'm first of all wonderful, thank you, honored to have people even think about translating something and wrote.

6
00:00:37,000 --> 00:00:40,000
It's like, wow, thank you.

7
00:00:40,000 --> 00:00:42,000
That makes me happy to know.

8
00:00:42,000 --> 00:00:48,000
And on two levels, on the level that like, wow, I did something that people thought was worth translating.

9
00:00:48,000 --> 00:00:53,000
And on another level that, wow, I think this is worth translating and someone's doing it.

10
00:00:53,000 --> 00:01:03,000
So, what a good thing they're doing, because it's not my teaching, it's my teacher's teaching and I guess the Buddha's teaching.

11
00:01:03,000 --> 00:01:09,000
But just to be clear, I don't believe in copyright.

12
00:01:09,000 --> 00:01:15,000
I don't think it's useful, except in a monetary sense.

13
00:01:15,000 --> 00:01:30,000
If the market wants to, if economic systems want to say copyright is useful and for people to make money and make a living off of things,

14
00:01:30,000 --> 00:01:37,000
then I'm not convinced, but I'm not going to fight against it.

15
00:01:37,000 --> 00:01:42,000
And certainly, that has nothing to do with me or my work.

16
00:01:42,000 --> 00:01:52,000
So, if you do something bad with my work and use it to, I don't know, distort the Buddha's teachings, then that's your bad karma.

17
00:01:52,000 --> 00:01:57,000
I'll tell people not to read it or look at it or so.

18
00:01:57,000 --> 00:02:16,000
But certainly, I would never sue you or even demand anything from you if you did that.

19
00:02:16,000 --> 00:02:24,000
Now, if you're doing good things with it, if anyone does anything, if you do anything good with something that I've made, please, you don't have to ask me.

20
00:02:24,000 --> 00:02:28,000
Let me know that would be nice, but only because that would make me feel happy.

21
00:02:28,000 --> 00:02:32,000
If I found out that people were using my things for good use, that would make me happy.

22
00:02:32,000 --> 00:02:45,000
It's the only reason, not because I demanded or because I think you have a duty to tell me, just because it would be nice, if I heard about it, I would like to hear about it.

23
00:02:45,000 --> 00:02:59,000
So, yeah, go ahead with it. Thank you. The other thing is, if you do translate it, how to meditate book, please send it to me.

24
00:02:59,000 --> 00:03:03,000
Because I can put it up on my website and other people can read it as well.

25
00:03:03,000 --> 00:03:17,000
We've got Chinese and Spanish already, isn't it? It's wonderful. Someone actually translated it into Chinese. Tina Chen in their name. That was like, that was ultimate.

26
00:03:17,000 --> 00:03:24,000
We have some of the trends that you work into Chinese just to see something you've written in Chinese.

27
00:03:24,000 --> 00:03:28,000
I don't know, is that getting proud I don't know.

28
00:03:28,000 --> 00:03:33,000
Thank you.

