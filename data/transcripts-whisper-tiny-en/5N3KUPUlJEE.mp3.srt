1
00:00:00,000 --> 00:00:07,000
When wanting arises, when I meditate, I start meditating on the wanting, by saying wanting and wanting.

2
00:00:07,000 --> 00:00:09,000
The wanting becomes stronger.

3
00:00:09,000 --> 00:00:14,000
It seems like I am only suppressing the wanting rather than getting rid of it completely.

4
00:00:14,000 --> 00:00:17,000
How do I overcome this?

5
00:00:17,000 --> 00:00:27,000
I'm not, I'm not quite clear on the logic that you're using there, but when something becomes stronger,

6
00:00:27,000 --> 00:00:30,000
I'm not generally a sign of suppression.

7
00:00:30,000 --> 00:00:34,000
The suppression is when you make it weaker by pushing it down,

8
00:00:34,000 --> 00:00:40,000
and I would suggest that that's the root problem,

9
00:00:40,000 --> 00:00:43,000
because let's put it simply.

10
00:00:43,000 --> 00:00:48,000
Our negative mind states are not simple.

11
00:00:48,000 --> 00:00:53,000
The negativity that we have in our mind is not simply a drawer full of greed,

12
00:00:53,000 --> 00:00:56,000
a drawer full of anger, and a drawer full of illusion.

13
00:00:56,000 --> 00:01:00,000
It's so insanely mixed up,

14
00:01:00,000 --> 00:01:05,000
and I mean think about the constructs that you've created in your life, right?

15
00:01:05,000 --> 00:01:10,000
It's not difficult to understand how complex these habits can become,

16
00:01:10,000 --> 00:01:16,000
and these negative tendencies to cling and to hate,

17
00:01:16,000 --> 00:01:21,000
and to dislike how they can be to become, to be conceded, to be arrogant,

18
00:01:21,000 --> 00:01:26,000
to be arrogant, all of the various different formations.

19
00:01:26,000 --> 00:01:36,000
Now, as a result, you can never deal with a single emotion,

20
00:01:36,000 --> 00:01:38,000
and say, I'm just going to focus on this one.

21
00:01:38,000 --> 00:01:40,000
Poof, it's going to disappear.

22
00:01:40,000 --> 00:01:44,000
You've got more issues going on there.

23
00:01:44,000 --> 00:01:47,000
The pleasure that you're experiencing,

24
00:01:47,000 --> 00:01:49,000
if it's becoming stronger in meditation,

25
00:01:49,000 --> 00:01:53,000
and that's a clear sign that in your daily life, you're suppressing it,

26
00:01:53,000 --> 00:01:55,000
and you're probably suppressing it with all sorts of things,

27
00:01:55,000 --> 00:02:02,000
probably with guilt, probably with anger,

28
00:02:02,000 --> 00:02:06,000
which is a part and parcel of guilt with delusion,

29
00:02:06,000 --> 00:02:10,000
the idea that you are in control of your desires,

30
00:02:10,000 --> 00:02:12,000
so when I want to turn it on, it's on,

31
00:02:12,000 --> 00:02:14,000
when I want to turn it off, it's off.

32
00:02:14,000 --> 00:02:18,000
I love this person, but I won't go near that person, right?

33
00:02:18,000 --> 00:02:21,000
I love my wife, but these angels in heaven,

34
00:02:21,000 --> 00:02:25,000
when I see these nymphs in heaven, I won't become attacked to them.

35
00:02:25,000 --> 00:02:33,000
So there's delusion, there's many things that are suppressing the wanting.

36
00:02:33,000 --> 00:02:36,000
When you're practicing meditation,

37
00:02:36,000 --> 00:02:38,000
what you're experiencing is actually a good thing

38
00:02:38,000 --> 00:02:40,000
because you've stopped suppressing it.

39
00:02:40,000 --> 00:02:41,000
That's why it's getting stronger.

40
00:02:41,000 --> 00:02:45,000
It's not because you're meditating on it, not directly.

41
00:02:45,000 --> 00:02:48,000
It's like meditation is growing more desire,

42
00:02:48,000 --> 00:02:52,000
but you've got a huge lump of desire inside.

43
00:02:52,000 --> 00:02:58,000
You're just a massive desire, and that means you have massive...

44
00:02:58,000 --> 00:03:01,000
Well, you have the tendency or the propensity

45
00:03:01,000 --> 00:03:05,000
to give rise to massive amounts of desire,

46
00:03:05,000 --> 00:03:13,000
continuous succession of mind-states of desire-based mind-states.

47
00:03:13,000 --> 00:03:17,000
That's the habit that you've developed over the years,

48
00:03:17,000 --> 00:03:19,000
rather than dealing with it, you've been suppressing it,

49
00:03:19,000 --> 00:03:23,000
and that's why you don't experience it.

50
00:03:23,000 --> 00:03:26,000
You get some garbage, and you throw it in your closet,

51
00:03:26,000 --> 00:03:31,000
and you get some more garbage, and you just get this habit of throwing garbage into your closet.

52
00:03:31,000 --> 00:03:33,000
Well, guess what?

53
00:03:33,000 --> 00:03:35,000
You end up with a closet full of garbage,

54
00:03:35,000 --> 00:03:37,000
and then meditation is like you open the closet,

55
00:03:37,000 --> 00:03:40,000
and you get to have it all fall down on you,

56
00:03:40,000 --> 00:03:42,000
and then you're like, oh, I'll close that up again,

57
00:03:42,000 --> 00:03:45,000
and then you think, what's wrong with this meditation?

58
00:03:45,000 --> 00:03:49,000
It's bringing out all this bad stuff.

59
00:03:49,000 --> 00:03:50,000
So you have a choice.

60
00:03:50,000 --> 00:03:53,000
You can keep it in the closet, and in fact,

61
00:03:53,000 --> 00:03:56,000
keep throwing more in, or you can train yourself first of all

62
00:03:56,000 --> 00:04:00,000
to stop doing that, and start to empty out the closet,

63
00:04:00,000 --> 00:04:03,000
deal with it when it arises.

64
00:04:03,000 --> 00:04:07,000
In that sense, there's nothing really wrong with

65
00:04:07,000 --> 00:04:10,000
the experiences that you're having,

66
00:04:10,000 --> 00:04:13,000
and another thing I would note is that

67
00:04:13,000 --> 00:04:19,000
it's arguable that the desire is not which is increasing.

68
00:04:19,000 --> 00:04:24,000
What is increasing is the physical manifestations of the desire

69
00:04:24,000 --> 00:04:26,000
in the brain, in the body.

70
00:04:26,000 --> 00:04:31,000
So a good example is with lust, with physical attachment to the body.

71
00:04:31,000 --> 00:04:34,000
99% of it is not the desire.

72
00:04:34,000 --> 00:04:38,000
The chemical reactions, the bodily changes,

73
00:04:38,000 --> 00:04:41,000
the hormones, and so on, this is all totally physical.

74
00:04:41,000 --> 00:04:45,000
It's not the desire, and this is why it's so important

75
00:04:45,000 --> 00:04:53,000
in regards to attachment to sexuality, and that sort of thing,

76
00:04:53,000 --> 00:04:57,000
to be able to break up the experience into its component parts.

77
00:04:57,000 --> 00:05:01,000
When you meditate on the desire, you're allowing your body

78
00:05:01,000 --> 00:05:04,000
to give rise to these states that normally you would suppress.

79
00:05:04,000 --> 00:05:07,000
You would create a great tension in the body

80
00:05:07,000 --> 00:05:10,000
that would suppress so many of the systems

81
00:05:10,000 --> 00:05:14,000
that are now given the chance to release.

82
00:05:14,000 --> 00:05:18,000
And so the chemicals will arise and you'll have great pleasure.

83
00:05:18,000 --> 00:05:20,000
And then there arises the desire.

84
00:05:20,000 --> 00:05:23,000
But it can be that when you're meditating,

85
00:05:23,000 --> 00:05:27,000
the delusion and the guilt and so on is preventing you from seeing this.

86
00:05:27,000 --> 00:05:32,000
But when you're meditating, the desire can actually disappear,

87
00:05:32,000 --> 00:05:34,000
and you can feel such great pleasure

88
00:05:34,000 --> 00:05:36,000
that would normally have been associated with desire.

89
00:05:36,000 --> 00:05:40,000
But because of you're saying wanting or more important,

90
00:05:40,000 --> 00:05:42,000
saying happy, happy or pleasure, pleasure,

91
00:05:42,000 --> 00:05:46,000
or even feeling, feeling, and so on,

92
00:05:46,000 --> 00:05:51,000
is that what's occurring is this release, this bodily release,

93
00:05:51,000 --> 00:05:53,000
and by bodily we also mean the brain,

94
00:05:53,000 --> 00:05:55,000
so the chemical releases and so on,

95
00:05:55,000 --> 00:05:59,000
that are now given the chance because there's less tension in the body.

96
00:05:59,000 --> 00:06:01,000
That's one really good way of understanding this,

97
00:06:01,000 --> 00:06:14,000
is that you've been suppressing or restricting the flow of bodily systems.

98
00:06:14,000 --> 00:06:16,000
And that's what's coming out desire.

99
00:06:16,000 --> 00:06:21,000
Arguably is only a static mind state.

100
00:06:21,000 --> 00:06:24,000
So say we have more desire, less desire.

101
00:06:24,000 --> 00:06:27,000
I'm not sure that such a thing exists in Buddhist thought.

102
00:06:27,000 --> 00:06:30,000
Desire is the moment where you like something.

103
00:06:30,000 --> 00:06:33,000
Now, if you like something continuously,

104
00:06:33,000 --> 00:06:35,000
then you could call it strong desire,

105
00:06:35,000 --> 00:06:42,000
but it's still only moment to moment to moment desire.

106
00:06:42,000 --> 00:06:44,000
And that's only a very small part.

107
00:06:44,000 --> 00:06:46,000
That's a very mental part of it.

108
00:06:46,000 --> 00:06:49,000
It's where the mind creates partiality.

109
00:06:49,000 --> 00:06:52,000
And it says that this is positive identify something,

110
00:06:52,000 --> 00:06:55,000
categorizes it as a positive experience.

111
00:06:55,000 --> 00:06:57,000
It sounds like at that moment,

112
00:06:57,000 --> 00:06:59,000
you're probably having a lot of negative experiences

113
00:06:59,000 --> 00:07:02,000
because you have the suppression tendencies.

114
00:07:02,000 --> 00:07:03,000
Oh, no, more greed.

115
00:07:03,000 --> 00:07:05,000
And you think that's a bad thing, right?

116
00:07:05,000 --> 00:07:06,000
These feelings are coming out more.

117
00:07:06,000 --> 00:07:07,000
And that's a bad thing.

118
00:07:07,000 --> 00:07:09,000
So actually, you don't like it.

119
00:07:09,000 --> 00:07:12,000
You're not giving rise to so much greed.

120
00:07:12,000 --> 00:07:14,000
You're giving rise to a lot of anger,

121
00:07:14,000 --> 00:07:16,000
which is even worse because now you're mixing them up

122
00:07:16,000 --> 00:07:19,000
and creating more complications.

123
00:07:19,000 --> 00:07:21,000
So it's very important.

124
00:07:21,000 --> 00:07:24,000
And this is in the video that this very famous video,

125
00:07:24,000 --> 00:07:26,000
it's only famous because of the title

126
00:07:26,000 --> 00:07:29,000
on pornography and masturbation.

127
00:07:29,000 --> 00:07:32,000
I said, it's important to let the lust come up

128
00:07:32,000 --> 00:07:33,000
and to focus on it.

129
00:07:33,000 --> 00:07:36,000
Mostly it has nothing to do with the desire.

130
00:07:36,000 --> 00:07:38,000
It's just chemical reactions in the body.

131
00:07:38,000 --> 00:07:41,000
And to be able to see that, it's a profound,

132
00:07:41,000 --> 00:07:44,000
it can actually lead to great physical and mental pleasure.

133
00:07:44,000 --> 00:07:46,000
It can lead to great happiness.

134
00:07:46,000 --> 00:07:49,000
It's a release because you don't have to feel guilty anymore.

135
00:07:49,000 --> 00:07:51,000
It's just an experience.

136
00:07:51,000 --> 00:07:57,000
And it has nothing to do directly with the desire.

137
00:07:57,000 --> 00:07:59,000
The desire is that little moment where you say,

138
00:07:59,000 --> 00:08:00,000
this is good.

139
00:08:00,000 --> 00:08:03,000
If you're not saying that, if you're just experiencing it,

140
00:08:03,000 --> 00:08:07,000
this happiness, this pleasure, this chemicals, whatever,

141
00:08:07,000 --> 00:08:10,000
then you become free.

142
00:08:10,000 --> 00:08:12,000
And when it's gone, it's gone.

143
00:08:12,000 --> 00:08:17,000
It doesn't trouble you.

144
00:08:17,000 --> 00:08:22,000
So you could watch that video on pornography and masturbation.

145
00:08:22,000 --> 00:08:25,000
I think because it's also about addiction in general.

146
00:08:25,000 --> 00:08:27,000
It talks about some of these things.

147
00:08:27,000 --> 00:08:31,000
And hopefully what I've said now is basically along the same lines

148
00:08:31,000 --> 00:08:34,000
and useful as well.

149
00:08:34,000 --> 00:08:35,000
Go ahead.

150
00:08:35,000 --> 00:08:39,000
I think you already said it in a way, in another way.

151
00:08:39,000 --> 00:08:45,000
But my first thought on this was that when the wanting

152
00:08:45,000 --> 00:08:50,000
seems to become stronger, it can as well be

153
00:08:50,000 --> 00:08:56,000
that you just see it more clearly.

154
00:08:56,000 --> 00:08:58,000
That's the other thing, right?

155
00:08:58,000 --> 00:09:02,000
So it's actually, it has been there all the time,

156
00:09:02,000 --> 00:09:04,000
but you didn't see it.

157
00:09:04,000 --> 00:09:09,000
You covered it up and you thought it is whatever.

158
00:09:09,000 --> 00:09:13,000
But now you see it as wanting as what it really is.

159
00:09:13,000 --> 00:09:17,000
So that's important to know, I think.

160
00:09:17,000 --> 00:09:25,000
Yeah, because normally we have this self-created

161
00:09:25,000 --> 00:09:26,000
amnesia, right?

162
00:09:26,000 --> 00:09:27,000
We don't want to think about it.

163
00:09:27,000 --> 00:09:30,000
So when anger comes up, no, not supposed to get angry.

164
00:09:30,000 --> 00:09:33,000
When greed comes, no, not so.

165
00:09:33,000 --> 00:09:35,000
We don't keep that in mind.

166
00:09:35,000 --> 00:09:37,000
Oh, I was angry there.

167
00:09:37,000 --> 00:09:39,000
But we can get angry a million times a day

168
00:09:39,000 --> 00:09:41,000
and not really remember it.

169
00:09:41,000 --> 00:09:45,000
Because we're constantly throwing it under the cut.

170
00:09:45,000 --> 00:09:46,000
No, I didn't get angry.

171
00:09:46,000 --> 00:09:48,000
No, I'm not going to get angry.

172
00:09:48,000 --> 00:09:50,000
But we are already angry.

173
00:09:50,000 --> 00:09:51,000
It's just our reaction.

174
00:09:51,000 --> 00:09:57,000
That is, that is sort of switching the subject,

175
00:09:57,000 --> 00:09:59,000
trying to avoid the subject.

176
00:09:59,000 --> 00:10:02,000
So in meditation, it's not that we're getting angry more.

177
00:10:02,000 --> 00:10:04,000
It's that we're letting it come up.

178
00:10:04,000 --> 00:10:06,000
We're like, okay, let's see what anger's like.

179
00:10:06,000 --> 00:10:08,000
And boom, it comes up.

180
00:10:08,000 --> 00:10:11,000
And boom, then there's the physical.

181
00:10:11,000 --> 00:10:12,000
The same with us.

182
00:10:12,000 --> 00:10:14,000
Okay, let it come up and look at it.

183
00:10:14,000 --> 00:10:17,000
And this is very clear that most,

184
00:10:17,000 --> 00:10:20,000
or many people miss about the Buddha's teaching.

185
00:10:20,000 --> 00:10:22,000
If you read the Satyapatana Sutta,

186
00:10:22,000 --> 00:10:23,000
what did the Buddha say about these things?

187
00:10:23,000 --> 00:10:25,000
When you have sensual desire,

188
00:10:25,000 --> 00:10:28,000
you know to yourself, they're sensual.

189
00:10:28,000 --> 00:10:29,000
Or I have sensual desire.

190
00:10:29,000 --> 00:10:31,000
They're sensual desire in the mind.

191
00:10:31,000 --> 00:10:33,000
When you don't, when it's not present,

192
00:10:33,000 --> 00:10:35,000
you know it's not present.

193
00:10:35,000 --> 00:10:36,000
This is very important.

194
00:10:36,000 --> 00:10:40,000
This creates the equanimity that we're talking about.

195
00:10:40,000 --> 00:10:43,000
It allows you to see the progression of state.

196
00:10:43,000 --> 00:10:48,000
And so it allows you to see how greed is,

197
00:10:48,000 --> 00:10:52,000
how greed works and anger and so on,

198
00:10:52,000 --> 00:10:56,000
and how the mind works, to be objective about it.

199
00:10:56,000 --> 00:10:57,000
So yeah, very clear.

200
00:10:57,000 --> 00:11:00,000
It may not be that it's coming more.

201
00:11:00,000 --> 00:11:04,000
It's just that you're actually not only more aware of it,

202
00:11:04,000 --> 00:11:08,000
but I would say even further, you're admitting it.

203
00:11:08,000 --> 00:11:11,000
Because we pretend we weren't.

204
00:11:11,000 --> 00:11:15,000
I'm not an angry person, but it's because we suppress it all.

205
00:11:15,000 --> 00:11:17,000
And then meditation makes us angry people again,

206
00:11:17,000 --> 00:11:19,000
and we can fed up about the meditation.

207
00:11:19,000 --> 00:11:38,000
There is this.

