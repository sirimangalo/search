1
00:00:00,000 --> 00:00:07,000
So, welcome to Monk Radio, and Sunday, March 4th.

2
00:00:10,500 --> 00:00:15,000
Before we get into the question of the answer period, just a couple of announcements.

3
00:00:17,000 --> 00:00:29,800
The first one is that we're going to try to use Skype for the question of the answer period

4
00:00:29,800 --> 00:00:30,800
tonight.

5
00:00:30,800 --> 00:00:36,800
So, you can post them by text if you'd like, but if you happen to have Skype and you want to talk with us,

6
00:00:37,800 --> 00:00:44,800
then you can actually call, call my username you to demo, and we're going to see how that works.

7
00:00:45,800 --> 00:00:48,800
We got it kind of rigged here.

8
00:00:48,800 --> 00:00:50,800
We'll see how that goes.

9
00:00:50,800 --> 00:00:57,800
If it doesn't work, we'll just shut it down and go back to text, but you can call me on Skype,

10
00:00:57,800 --> 00:01:04,800
and you'll be on the air.

11
00:01:05,800 --> 00:01:12,800
And the other announcement is that we have decided to put together a book.

12
00:01:12,800 --> 00:01:23,800
Now, many of you might be aware of this, but this is going on to the wider audience that at the question and answer form on our site, on our website,

13
00:01:23,800 --> 00:01:32,800
we've started to put together a fairly sizable list of talks that are being transcribed,

14
00:01:32,800 --> 00:01:37,800
and we'll be put together in a sort of a collection in print form.

15
00:01:37,800 --> 00:01:46,800
And the idea is to put in some logical order and organization and bring it out and publish it as a book.

16
00:01:46,800 --> 00:01:54,800
And whether it be the video teachings on YouTube or the audio teachings that we've collected on the website,

17
00:01:54,800 --> 00:02:01,800
or even the question and answers through our forum and the ask among videos, of course.

18
00:02:01,800 --> 00:02:05,800
So we'll have a section in the book dealing just with questions and answers.

19
00:02:05,800 --> 00:02:10,800
But what we need is people who are interested in helping to transcribe these talks.

20
00:02:10,800 --> 00:02:17,800
So if there's anyone out there who would like to get involved, please visit our question and answer for you.

21
00:02:17,800 --> 00:02:32,800
And let us know that you're willing to get involved and you can see there's instructions there on the forum under the how about a book thread.

22
00:02:32,800 --> 00:02:37,800
And we'll let you know which ones are available and you can help to transcribe.

23
00:02:37,800 --> 00:02:42,800
So just thought I mentioned that as well for people, if anyone's interested in.

24
00:02:42,800 --> 00:02:46,800
And thanks to everyone who's already sent us transcriptions, it's great.

25
00:02:46,800 --> 00:02:53,800
We're well on our way to having a list of chapters for the book.

26
00:02:53,800 --> 00:02:56,800
So those are the announcements that I have.

27
00:02:56,800 --> 00:02:58,800
Any announcements?

28
00:02:58,800 --> 00:02:59,800
No.

29
00:02:59,800 --> 00:03:08,800
Okay, then on to the questions.

