1
00:00:00,000 --> 00:00:09,000
Okay, good evening everyone. Welcome to monk radio and next up is the announcements. So Panyani has an announcement.

2
00:00:09,000 --> 00:00:14,000
Hi, good evening everybody. We're good morning wherever you are.

3
00:00:14,000 --> 00:00:31,000
Yeah, I just wanted to say that Jens is coming back to enlarge our community here and probably join a not far future as a monastic.

4
00:00:31,000 --> 00:00:42,000
Just one announcement about the book project that there are still some people who have undertaken to do editing and if you're one of them.

5
00:00:42,000 --> 00:00:52,000
Just let us know if you're still planning to complete the editing and if you're still interested in editing other talks. You're welcome to pick one up.

6
00:00:52,000 --> 00:01:05,000
Another thing is I might be going to Thailand in May. So look for a break in monk radio or we might even try to do it from Thailand because my uncle's got cameras and stuff.

7
00:01:05,000 --> 00:01:08,000
So we'll see about that.

8
00:01:08,000 --> 00:01:14,000
And I don't want to do it alone from here.

9
00:01:14,000 --> 00:01:20,000
I think that's it. There was something else to get remember now.

10
00:01:20,000 --> 00:01:23,000
True, I'll tell him.

11
00:01:23,000 --> 00:01:51,000
All right, so that's all the new things.

