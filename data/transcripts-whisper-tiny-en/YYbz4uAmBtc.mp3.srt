1
00:00:00,000 --> 00:00:06,160
When a Christian friend of mine tries to use mindfulness meditation, after a while he says

2
00:00:06,160 --> 00:00:13,080
he feels the presence of Jesus and God, I told him that this arises because of his history

3
00:00:13,080 --> 00:00:14,440
with religion.

4
00:00:14,440 --> 00:00:27,200
What should I tell him about this and what should he be noting when this arises?

5
00:00:27,200 --> 00:00:36,720
What he told him so far is probably correct that the feeling of the presence of Jesus

6
00:00:36,720 --> 00:00:48,320
and God does arise in him because of his history to the Christian religion.

7
00:00:48,320 --> 00:01:02,280
There's the perception of a deep spiritual feeling and when he knows nothing else, then

8
00:01:02,280 --> 00:01:07,520
Jesus or God, then of course this must be Jesus and God.

9
00:01:07,520 --> 00:01:15,080
But when the mind opens up and says, okay, there might be something else out there then

10
00:01:15,080 --> 00:01:30,280
Jesus and God, then when the mind opens, then there can be the possibility to feel different

11
00:01:30,280 --> 00:01:46,680
to feel or to name different, the feeling would possibly be the same a deep spiritual feeling,

12
00:01:46,680 --> 00:01:57,400
but a person with a non-Christian background or with an open mind would maybe not say,

13
00:01:57,400 --> 00:02:05,200
oh, this is Jesus and this is God, but just say, this is the feeling.

14
00:02:05,200 --> 00:02:18,440
So we only can perceive or we can only name what we know or we cannot but we do usually

15
00:02:18,440 --> 00:02:29,720
give names of what we know, what you can tell him is that when there is that feeling

16
00:02:29,720 --> 00:02:40,920
of Jesus and of God, then he should say feeling, feeling, feeling and when there is liking

17
00:02:40,920 --> 00:02:53,040
he should say liking, liking, liking and you could tell him that it is not so important

18
00:02:53,040 --> 00:03:04,280
at that particular moment to feel Jesus or God, but to know what is going on in the present

19
00:03:04,280 --> 00:03:15,160
moment because that is the mindfulness meditation and to feel Jesus and God is another

20
00:03:15,160 --> 00:03:25,440
meditation which is good also for a Christian, but if you want to practice mindfulness meditation,

21
00:03:25,440 --> 00:03:36,640
then he should say feeling, feeling and whatever else is arising in that present moment

22
00:03:36,640 --> 00:03:45,720
and not get or not be attached with the thoughts of Jesus and God and not be attached

23
00:03:45,720 --> 00:03:57,760
to the liking of Jesus and God or to say it less be attached to that deep spiritual

24
00:03:57,760 --> 00:04:14,320
feeling, it depends also on the person, it depends on whether they are truly open-minded

25
00:04:14,320 --> 00:04:19,040
or whether they are just, I mean it can't even be in some cases where they are just

26
00:04:19,040 --> 00:04:24,320
doing it to make friends with you so that you come and be Christian with them or so

27
00:04:24,320 --> 00:04:25,320
on.

28
00:04:25,320 --> 00:04:33,040
So it depends on the individual, but another tact you can take depending on the person

29
00:04:33,040 --> 00:04:45,200
is that what they have made this, truth as assertion, this factual assertion that what they

30
00:04:45,200 --> 00:04:50,120
are feeling is the presence of Jesus and God.

31
00:04:50,120 --> 00:04:55,560
First thing you have to explain to them is what does it mean to note, what does it mean

32
00:04:55,560 --> 00:05:01,360
to be mindful, what does it mean to be aware or to meditate on something?

33
00:05:01,360 --> 00:05:07,120
You have to be clear that because many people misunderstand that by noting we are not trying

34
00:05:07,120 --> 00:05:14,000
to get rid of the object and the noting itself doesn't necessarily remove the experience,

35
00:05:14,000 --> 00:05:20,240
it can in fact make the experience stronger inadvertently but it can have that inadvertent

36
00:05:20,240 --> 00:05:25,120
effect because the mind that is going allows for it to arise even stronger, the mind

37
00:05:25,120 --> 00:05:27,280
is clear.

38
00:05:27,280 --> 00:05:32,440
So why this is important is because it can happen that when you have them say feeling,

39
00:05:32,440 --> 00:05:36,680
they don't want to do that because they feel they are afraid that it is going to disappear

40
00:05:36,680 --> 00:05:39,880
so they will be mindful of other things but they won't be mindful of the things that

41
00:05:39,880 --> 00:05:43,960
they are clinging to for fear that they will disappear for fear that they will lose them.

42
00:05:43,960 --> 00:05:48,280
So you have to explain the noting isn't for that purpose, the noting is for the purpose

43
00:05:48,280 --> 00:05:52,760
of understanding what exactly that feeling is.

44
00:05:52,760 --> 00:06:01,240
So if that feeling is God in Jesus, your investigation of it will just show you further

45
00:06:01,240 --> 00:06:08,200
and further that that is Jesus, that is God but to simply feel something and say this is

46
00:06:08,200 --> 00:06:12,480
Jesus and this is God is only a belief, it's not yet an attainment of the truth.

47
00:06:12,480 --> 00:06:19,320
So the practice of meditation is to investigate, to see clearly what is that feeling?

48
00:06:19,320 --> 00:06:27,480
Let's explain this to them and explain to them that through the practice we are going to

49
00:06:27,480 --> 00:06:31,560
try to find out what we are going to understand exactly what is that feeling.

50
00:06:31,560 --> 00:06:36,680
You have this assertion that this is God, this is Jesus, let's look and find out, let's

51
00:06:36,680 --> 00:06:39,640
look and see what we find the closer we look at the feeling.

52
00:06:39,640 --> 00:06:44,840
So then you say to yourself feeling and when you think of it as God say to yourself thinking

53
00:06:44,840 --> 00:06:49,800
and what will happen, they will come to realize that actually it's just a feeling, the thought

54
00:06:49,800 --> 00:06:54,800
is just the thought, it arises in Jesus and the idea of it being God, it being Jesus is

55
00:06:54,800 --> 00:07:09,800
actually a meaningless thought process, a meaningless recognition or association, those are just words,

56
00:07:09,800 --> 00:07:14,440
this is God, this is Jesus and eventually to find out to realize that something like God,

57
00:07:14,440 --> 00:07:19,480
something like Jesus cannot possibly exist because it's an entity and reality is only

58
00:07:19,480 --> 00:07:47,320
experiencing an experience arises in Jesus.

