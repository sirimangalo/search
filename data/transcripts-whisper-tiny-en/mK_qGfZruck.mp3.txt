Consciousness is not considered non-local, as I understand non-local in quantum physics.
Consciousness is considered undescribed by quantum physics, which is outside of physics, outside of what is
indescribable by physics, except as being what, what is the guy with the processes anyway?
Process 1, it is considered to be the process 1, the decision making, this whole thing that got everyone up in
arms about how it all depends on which decision you make. If you decide to do this experiment or if you decide to do
that experiment, actually determines the framework of what you are talking about.
So, actually to me it sounds a lot like dualism, with Henry Stapp and with Orthodox quantum physics
discusses there is process 1, process 2, process 2 is that which can be calculated in the
described by quantum physics, process 1 is what initiates, process 2, and so that is quite dual, actually.
But Stapp actually, if you look at his articles on, he has an article on non-dualism, for example,
and he says, there is no reason to think of it as being dualistic in a sense of two different entities.
It could be just separate poles, I think he says, you have an experience and you have the two aspects of it.
There could be just aspects of the experiment experience, which, I mean, what I don't get is why people are so fixed on finding
something non-dualist, I guess it just kind of makes you feel good to think that it can all be reduced to one thing.
And I don't really see that, I understand how it is kind of nice to think of, but I don't see how it is any more logical or rational.
It is just kind of this reductionist, or I don't know, this attempt by science, they are trying to find the grand unifying theory of everything.
They want to get it down to one, which, I don't really see the point, it is what it is, right?
Whether it's one or two, it is what it is, and that's much more important than counting, or it's actually many.
There was an argument between these two monks.
One said there was three monks or two people anyway.
One said there are three kinds of feeling, and the other one said there are five kinds of feeling,
and then the Buddha came to the Buddha and asked him who was right, and the Buddha said there are 108 types of feeling or something.
Just to show them they were being stupid.
