Hello and welcome back to our study of the Danpada. Today we continue on with first number 124, which reads as follows
Pani me J. Winona sa
Hariya Pani na visan
Nabana visan manvati nati papa nakubato
Which means
If one should not have a wound if there should not be a wound on the hand a cut on the hand
Hariya Pani na visan one
Can pick up one could or might pick up poison with the hand
Or for Nabana visan manvati for poison doesn't enter
It doesn't harm
One without a cut one without a wound
Nati papa nakubato
And likewise evil
There is no evil for one who does not do it
Nati papa nakubato for one a kubato for one who does not perform it
Nati papa there is no evil
So this is actually an important verse
kind of unique in
answering this question a certain question
And so then a bit of backstory
It's a fairly well-known story of the hunter Kukkukkuktamita
The story is more about his wife
Or it's about the two of them so it starts with his wife
His wife it seems became a soda panda when she was very young
So she must have maybe listened to what the Buddha had said or maybe even
Practice meditation based on what another monk can talk but
See it was in Rajagaha, she lived
And kind of like
like
Guesa
Goudna Casey
This woman who became an ascetic
They became a Buddha ascetic and then a Buddhist monk
When she was when she was young they kept her up on the seventh floor of this kind of palace to try and keep her away from men
They remember the story of Goudna the Gasey
How she snuck out while this
Woman the wife of Kukkuktamita did the same thing
But how it happened here is that she
She looked at a window one day and saw hunter Kukkuktamita coming into the city to sell his
Deer he killed deer so he brought 500 probably not 500 but many
many many
Many many deer put them in a cart and brought them into the city to sell
Or cut them up and was selling their flesh and when she saw him she fell in love with him
kind of love it first sight kind of thing and so
She snuck out with the servants by dressing herself in rags and dirtying her base and
and so on
And followed after the hunter as he left the city
Now he tried to get her to go away and she wouldn't go away until finally he realized what she was after and so he
picture up and brought Tukkuktam put her in his cart and carried her on on his way
And they lived together from that time on
Her mother and father of when they found that she was missing they looked for her everywhere couldn't find her and thought that she
Was dead and they had a funeral ceremony for her and everything
But meanwhile she's living in the forest with this hunter and over time had seven sons
And these seven sons got married and
lived with seven women and so all together there were 16 of them
This is the story
And this is the backstory so we come to the present time
Where the Buddha which he was want to do in the morning
They say when the Buddha lay down first to sleep
He he would spend his time
It would lie down during the night to rest and he would send his mind out to the universe trying to
discern just decide
Who would who he would
Teach that thing
Who was ready?
Well, it's it may be someone in the village that he was in or maybe it was someone far away or maybe it was even the angels up in heaven
in one of the heavens and
When he decided when he came upon someone who seemed ready
He would he would find a way to get to them
and so on this day
he saw these 16 people
And
He considered that all of them first of all
He knew that the wife was already a sort upon them
But he knew the rest of them would benefit if he were to go to them
And so he walked through the forest and came upon the nets
And it happened on that day that
The hunter didn't catch anything
So maybe it's somehow karmic or somehow related to the Buddha, but at any rate
He all of his nets were empty all of his nets and his snares and whereas normally he would always catch
Catch in deer on this day. He didn't catch anything and so the Buddha went to one of the nets and stepped
Stepped into the net with his foot leaving a footprint
That the hunter would see leaving tracks enough footprints by the
the net
And so the hunter came by to check on his nets go to meet the
He came to see what he had caught found that he hadn't caught anything and started getting more and more annoyed
to finally he came upon the net with the footprints
And he thought to himself someone's been
Someone's been
freeing these
These deer someone's been setting the deer free has been
Maybe robbing me of my game
And so he went and he followed the tracks and he came upon the Buddha
And here's the story gets a little bit
supernatural and
You know, I don't want to really
Be too hard on these stories because I don't know. I mean magical things could happen
But I know it's hard for a modern audience because we don't have these kind of magical powers now and
Many people are skeptical that anyone ever did have these magical powers. So here's how it here's how it goes down
He sees the Buddha and he and all of his sons
Draw their bows and are ready to shoot the Buddha
That part is is in the story and and that part
Don't have to get into the magic for that
The the white his wife sees
them draw with their bows drawn pointed at the Buddha and
cries out at them
Don't kill my father. She says
Don't kill my father. What are you doing? You're going to kill my father. Please don't
And they were shocked they and they all
Completely sobered up and
Well
We're in shock because they didn't know who her father was obviously because she had lost connection with her family
But here this was her father her father was among of course that wasn't the case, but
This is how Buddhists would look in light and
Buddhists from Sotapana on up will look at the Buddha as being their father
and
So they were all ashamed and and and they
Put their bows down went in pain respect him
This is my father-in-law. This is your grandfather and son and they were all friendly towards him
So see you see I didn't have to get into any supernatural know what that any magic now
The story says is he came and he drew his bow and the Buddha made him unable to shoot
So he froze him in place and then all the seven sons came slowly and they
Held up their bows and they likewise were frozen became frozen in place
And they stayed like that for a while actually but I had them frozen in place
and
The wife was wondering where they'd been and she where they went what happened to them
Why they were back for lunch maybe and so she went out and
Found where they were or the Buddha was just meditating there and they're all frozen in place and then she
cried out
Do not kill my father do not kill my father
So you take which version you prefer. I don't mind
And so they sat down and they asked forgiveness from the Buddha and they listened to his teaching
And upon listening to his teaching all of the other 15 and they called the daughters of the seven daughter-in-law as well
And all together all 16 of 15 of them became so dependent so along with
The wife that made 16 of them were now were now in light and beat or so dependent
Then he went back to the Buddha after preaching he went back to the monastery and Ananda saw him and Ananda
Asked him wherever he been and the Buddha told him what he had done and he said oh did they become
Did they all become so dependent? And he said well, they all did except for except for his wife
But she because she had been a she had been a soap upon it since she was a girl
And
Ananda was impressed and
whatever and
But the monks got winded this because you see the curious thing there is
The most they were talking about it. They thought well, that's understandable. I mean
Angulimala became an aran so
Hunters can become so dependent
But the the problem here is the problem that the monks had when they were discussing it is
She was a sort of benefit from a girl. So what the heck was she doing with a hunter?
Because the so-to-pane is incapable of killing intentionally
Really and a so-to-pane shouldn't be able to perform any
Significantly immoral act like killing or stealing or cheating or lying
Even drugs in alcohol. They wouldn't get involved
And so they said you know what what the heck's going on? How could she have been a so-to-pane when she's
cleaning his traps and his
His arrows and preparing them for him and putting out his bow and arrows and doing all these things that
Ostensibly she would have to do as a hunter's wife may be even cleaning carcasses and so on
You know cooking the meat that he had killed even eating the meat that he had killed
Certainly eating it right
And when he says bring me my bow bring me my arrows bring me my hunting knife knife bring me my net
She would obey him
And then he using that he would go and take life. So how could she do that if she was a so-to-pane
And here's was the Buddha's reply
Monks of course those that have obtained so-to-pane do not take life
Kukuta Mitra's wife did what she did
Because she was actuated by the thought I will obey the commands of my husband
What was the thing in India? It never occurred to her to think he will take what I give him and go hence to take and take life
Never occurred to her to think that
Well, I mean, I suppose that's maybe you're stretching it. Maybe the translations have been off
But she didn't think that wasn't the thought that she had she didn't think about him taking life
If a man's hand be free from wounds even though he take poison into his hand
Yet the poison will not harm him
Persuise precisely so a man who harbors no thoughts of wrong and who commits no evil
may take down bows and other similar objects and present them to another and yet be guiltless of sin
And then he gave his teeth this he thought that's worse
And this is I think this is quite controversial
You know this leads into the controversy that we often face about how Buddhists can eat meat some Buddhists
As Buddhists in my tradition
How can you eat meat? How can you be so hypocritical when you say killing is wrong and yet eating meat is okay?
And it's this verse that really drives that teaching home
Because ethics in Buddhism
is quite different from ethics as we think of it
and so this this kind of
confusion and criticism comes
Quite often as a result because people have such have
Ordinary ideas of ethics something is wrong because it hurts someone else
But you know since you can't know what is going to hurt someone else it makes many things potentially wrong
I mean, there's no you can't then easily draw a line in the sand. I mean, I don't know what
Consequences my actions are going to have
And then you might even you I mean you might argue that
Based on this there's a more like a greater likelihood or so on, but you see you can't draw a line in the sand
Which points to the how problematic that kind of ethics is but
It still misses the point that our ethics are categorically different our ethics don't involve the consequences to others
Ethics are solely how they affect you
So if I hurt you
That's unethical from Buddhist point of view because of the damage it does to my mind
The damage it does to your face is not not really
not
directly
Involved in the equation now if you get angry because I hit you then that's that in and of itself is unethical of you
so if if you
Are sad and are hurt and suffer because of what I did to you
Then that is unethical you suffering is unethical because you're hurting yourself
so it's very much
self-centered and that's another criticism that Buddhism faces
but
It's it's funny how
We really get a lot of hard criticism like this and
Vegetarians will often get quite upset about Buddhists and there's this divide that we can't bridge because
They think they're right and getting upset and we think we think they're wrong
We think you're the unethical and you getting angry at me for sitting here chewing something that is dead is a
Problem the anger that you have me sitting here eating something that is dead is not a problem
I'm just chewing chewing chewing swallow
And so on our ethics is our idea of ethics is quite different
and so if you
And carry the the if you carry poison in your hand it only it only hurts you if if you have a cut and the same idea is with
with acts
So if you if I shoot a bow
Add a treaty and it turns out that the tree is you know
Maybe it's someone dressed up in a tree costume
And the person dies or you know, maybe there's a person in the tree that I didn't see
I'm not guilty. There's no crime in that. There's no problem. On the other hand if I
If I shoot at the tree thinking I see a bird in the tree and it turns out there was no bird in the tree
I've still done a very very evil thing my intention was evil. You see it's still a bad karma
Because I've I had a bad intention
There's an interesting story of the past I suppose I shouldn't go into it because it'll take way too much time
But um, so the key here is understanding ethics and someone recently asked me about
And how it was about monks not being able to act as doctors
and the idea how cruel that would be if you came up on someone who was sick and didn't help them who was dying and didn't help them and
This idea of the cruelty
And the idea of lacking compassion and so on
It's it's really looking at things and this is why people get kind of misled by compassion because
It's just just like in this case of of not being able to do anything being totally paralyzed if you had to really consider the consequences of what you did
But the same token if you were required to help anyone anytime you could then we'd never
Rest we'd collapse in exhaustion
Because we'd always be capable of helping others so the
the idea that
Compassion means helping people when you can or even when you hear about it is
It's it's something that you have to you know has to definitely be tempered with wisdom and understanding
It has to be led by wisdom and understanding if it was just about helping when you can you
Then you never end and you never really help anyone because to and you never really
Change the world because there'd always be more people there will always be
People who need help and furthermore those people who you do help
There's no saying there's no telling what's going to happen in the future except that really in the end they're going to die anyway
So I mean all of it points to the idea of really having a wisdom based
concept of ethics and morality and goodness and compassion
Now definitely you should help people when you have the opportunity you know monks have certain rules and monks are very very
Devoted themselves to a path
but even still
the idea that you have to help people and and there are stories about how you should kill
You know these these dilemmas of how you should kill five people to save one or if there's a time bomb you should
Maybe
Let's put that one aside, but
You keep you doing a lesser evil to prevent a greater evil
but it it implies
that you
You are required to do something
That they're that
It's unethical to do nothing
Or it's an essay or in the case of this story. It's an ethical to
Do anything
related to evil
If if the consequences of the act are evil you see so the consequences of action or the consequences of omission
And this isn't how Buddhism looks at it now if if someone is suffering in front of me
Now technically I can be at peace with myself without helping them
I mean this is possible now
I would argue that in most cases you would not be at peace with yourself in most cases being at peace with yourself
Just naturally is helping the person. I mean that's I think how compassion works
but
in certain circumstances where
Maybe they're being at you know someone's attacking someone else, right?
and
The only way to to to fix the situation would be to maybe harm the first person
Maybe hit them so on
So hurt the other hurt the other person
I
Don't even know then. I think even then you could you could
argue that you should act
But there there is a line you see there's a line which requires
defilement that you wouldn't act
and
The same I think goes for a mid for for the for acts
Like if you if you were to not do it
It would cause more it would cause problems and doing it is
the
Going with the flow
We saw you say while her handing him
His bow and arrow was enabling him. Well, that's not how she thought of it for her
It was a mindful act my husband asked me for something. I get it for him
I remember when I was many many years ago. I was working in a restaurant
and
We had to serve alcohol and I was already practicing meditation. I wasn't a monk yet and
So I had to carry the alcohol to the people and I thought well, this isn't very ethical
You know here. I'm enabling them, but I thought of this verse actually I read the Dhammapada and I
Thought of this verse and so I thought okay. This is what I'm doing. I'm lifting the bottle lifting this thing
You know moving it and placing it. I'm actually doing a good deed
I'm serving someone I really enjoyed being a wager for the short time that I did it because I was serving people
It was it was a kindness really or it was a humbling
Activity
And so
This is important. I mean it's important as Buddhists philosophically to understand where we stand
Understand why we stand that way we stand that we stand
This way because
We're interested in happiness and we're interested in peace and
Because we have our understanding is that you have to find happiness and peace for yourself
No one else can do it for you. No one else can also either make you suffer
If someone hurts you, it's up to you how you react to that
So our our idea of ethics and morality are very much to do with our state of mind
When we do something and if you can eat meat
Without any thoughts of harm and the Buddha said you know you send love and kindness
compassion send out at least thoughts
Then you can do that while you're eating meat
And I know many people don't like this idea. They think this is horrible. I've actually had people get angry at me and
This is there's this divide where you they can't understand us but
My thought it was funny is because it doesn't really matter
You know if you get angry at me and you believe I'm evil and cruel because I won't make medicines for people or because I won't
Go out of my way to help people or so and it doesn't really matter
It's a problem for you because it makes you suffer
I feel sorry for you and I hope you can free yourself from that
but
It really fails, you know, I mean
You can say someone is cruel but cruelty is a state of mind
And we have these sort of nebulous ideas of ethics and morality and goodness and compassion right and wrong
That just
Have much more to do with our partiality. We don't want to see people suffer
We get angry and upset when people suffer when we hear about these animals that get that die or we hear about animals that are killed
We get arrogant and self-righteous and and angry and we think we're in the right
And that's why we end up getting burnt out
You know activists people who work for social justice often burn out because
They are full of emotion and they're full of want and desire for change
And it burns you out
And they never look carefully to see that. Oh, yeah, you know in the end of the universe the world
What the the sun is gonna explode and burn the earth to a crisp anyway and then you can't save the surface
You can't save mankind humankind
You can't solve the problems of the world
There's no logical reason to try
All you can do is what's right do good to do good things
Better yourself
Because your mind stays with you the only thing that stays with you is your mind
It's really a very very solid cystic sort of viewpoint
But I think that's proper it's not that we don't believe whether people exist just that they exist in their own world
We can't change their we can't live their world for them
And whatever good we can do for them is only as a catalyst
I think there's room for
for not for not supporting hunters for even telling hunters what's that it's a you know
problematic
I think there's even room you could argue for
A husband or a wife to tell their husband or their wife that they disagree with hunting that they wish
For beings to not not have to die or something
I think there's room for being a vegetarian if you like. I think it's a good thing
But we have to be very careful when we accuse or when we make claims of what is moral and what is ethical as these monks
And so that so and it has to be based on our understanding of proper understanding of around
So that's the teaching
How it relates to our meditation practice is this is
Meditation is a way that you really understand this
You understand what the universe is made of how it works
and you start to get very logical and rational about things
You stop freaking out or getting upset and judging acts and and people
Like when you see someone who does you hear about someone as evil things or you think
of someone or you know someone who does evil things
You don't think of them as an evil person because you know how it goes. They probably have some very good mind states at times
And even if they don't
It's still just temporary. They can always become a better person, but
More more clearly
They aren't really a person
Those states are just states. It's not an evil person. It's cause and effect
It's got reasons and causes
Where it comes from and it's just states of mind states of body that arise and sees
And to us it's just experiences this person yelling at me is just sound this person hitting me is just a feeling or then pain
This person stealing my things is just a thought that I have that that's mine and that's a person stealing something that belongs to me
Meditation is what helps you understand this and it really I was a vegetarian up until the time
I practiced meditation and then I stopped and even today. I'm still not a
Full of vegetarian. I mean, and I've always thought it's a good thing. You know, I understand that meat comes from a bad place usually
So I like to be vegetarian, but I think that's an important sort of
Attitude to have is not not be too serious or too obsessed with
Trying to fix the world, you know
Trying to solve all the problems. I think on the other hand
It's good not to be complacent and it is good to help. It's good to speak out. It's good to say what's right
It's good to try to help people
It's good to try to be kind and compassionate
But it's not about ethics it's about
really about going with the flow and
To something I mean not quite but going with not the flow of people necessarily
But with the flow of life, which can be
Somewhat friction and create conflict at times
But it has more to do with your state of mind not upsetting the equilibrium in your mind
Setting your mind that are giving rise to greed anger or delusion. That's really all because nothing unethical can come to you
Except for one of through one of these three things
Anyways, it's complex and I think people complicate it and I imagine that I'll probably
For this kind of talk I'll get a lot of people confused and uncertain. I think that's why it's important to meditate
I mean even rather than
Taking anything I've said or any of these things as solid views or dogma or so and
Most important is that you find out for yourself. You learn for yourself. What is ethical?
Don't believe you're you're in guts or you're feeling or you've been taught because we have a lot of wrong ideas and wrong
Habits of understanding
Try to look
Look at reality watch how your mind works learn how your mind works
And come to see for yourself. What is it truly ethical?
So
Anyway, that's the dumb upon of teaching for tonight
Thank you all for tuning in wish you all good practice
