1
00:00:00,000 --> 00:00:06,000
Number 225, right, 225, we think we were out.

2
00:00:06,000 --> 00:00:08,000
That was the last one we read.

3
00:00:08,000 --> 00:00:16,000
So now we're starting with 226 on page 280.

4
00:00:16,000 --> 00:00:25,000
And Anka, could you start us off?

5
00:00:25,000 --> 00:00:27,000
Can you hear me?

6
00:00:27,000 --> 00:00:32,000
We can now, yes, thank you.

7
00:00:32,000 --> 00:00:40,000
Now, since there is no separate method for developing the meditation subject in the case of the other dead drugs,

8
00:00:40,000 --> 00:00:46,000
their meaning therefore needs only to be understood according to the world commentary.

9
00:00:46,000 --> 00:00:52,000
He trains thus, I shall breath in, shall breath out experiencing happiness.

10
00:00:52,000 --> 00:00:56,000
That is, making happiness known, making it plain.

11
00:00:56,000 --> 00:01:00,000
Hearing the happiness is experienced in two ways.

12
00:01:00,000 --> 00:01:04,000
With the object, and with non-consciousness.

13
00:01:04,000 --> 00:01:15,000
Twenty-twenty-seven, I was the happiness experienced with the object.

14
00:01:15,000 --> 00:01:19,000
He takes into a job of us in which happiness present.

15
00:01:19,000 --> 00:01:24,000
At the time when he has actually entered upon them, the happiness is experienced with the object

16
00:01:24,000 --> 00:01:29,000
owned to the obtain of the genre, because of the experience of the object.

17
00:01:29,000 --> 00:01:31,000
How would non-confusion?

18
00:01:31,000 --> 00:01:37,000
When, after entering upon and emerging from one of the two genres accompanied by happiness,

19
00:01:37,000 --> 00:01:43,000
the comprehends with insight that happiness associated with the genre as liable to destruction and to fall,

20
00:01:43,000 --> 00:01:48,000
then at the actual time of the insight that happiness is experienced with non-confusion,

21
00:01:48,000 --> 00:01:54,000
owing to the penetration of its characteristic of impermanence and so on.

22
00:01:54,000 --> 00:01:58,000
With this, it is said, in the Patissan Bita,

23
00:01:58,000 --> 00:02:05,000
when he knows unification of mind and non-destruction through long inbreds,

24
00:02:05,000 --> 00:02:08,000
mindfulness is established in him.

25
00:02:08,000 --> 00:02:14,000
By means of that mindfulness and knowledge that happiness is experienced,

26
00:02:14,000 --> 00:02:19,000
when he knows unification of mind and non-destruction,

27
00:02:19,000 --> 00:02:24,000
through long outbreds, through short inbreds, through short outbreds,

28
00:02:24,000 --> 00:02:30,000
through inbreds, outbreds experiencing the whole body,

29
00:02:30,000 --> 00:02:37,000
through thebreds outbreds, trying quillists are tranquilized,

30
00:02:37,000 --> 00:02:48,000
trying quillists are rising, about dewy information mindfulness is established.

31
00:02:48,000 --> 00:02:55,000
In him, by means of that mindfulness is that knowledge that happiness is experienced.

32
00:02:55,000 --> 00:03:00,000
It is experienced by him, when he advert,

33
00:03:00,000 --> 00:03:07,000
when he knows sees, reviews, studies his mind,

34
00:03:07,000 --> 00:03:14,000
resolve with faith, exerts, energies established mindfulness,

35
00:03:14,000 --> 00:03:19,000
concentrates his mind, understands with understanding,

36
00:03:19,000 --> 00:03:23,000
directly knows what is to be directly known,

37
00:03:23,000 --> 00:03:27,000
fully understands what is to be fully understood,

38
00:03:27,000 --> 00:03:32,000
abandoned, what is to be abandoned, develops what is to be of it,

39
00:03:32,000 --> 00:03:36,000
developed, realize what is to be realized.

40
00:03:36,000 --> 00:03:42,000
It is this way that happiness is experienced.

41
00:03:42,000 --> 00:03:46,000
Party,

42
00:03:46,000 --> 00:03:50,000
oh, I'm sorry.

43
00:03:50,000 --> 00:03:57,000
The remaining three clauses should be understood in the same way as to meaning,

44
00:03:57,000 --> 00:04:00,000
but there is this difference here.

45
00:04:00,000 --> 00:04:06,000
The experiencing of bliss must be understood to be through three genres,

46
00:04:06,000 --> 00:04:10,000
and that of the mental formation through four.

47
00:04:10,000 --> 00:04:16,000
The mental formation consists of the two aggregates of feeling and perception.

48
00:04:16,000 --> 00:04:20,000
And in the case of the clause experiencing bliss,

49
00:04:20,000 --> 00:04:23,000
it is said in the party San Bida,

50
00:04:23,000 --> 00:04:27,000
in order to show the plane of insight here as well.

51
00:04:27,000 --> 00:04:29,000
Bliss,

52
00:04:29,000 --> 00:04:31,000
there are two kinds of bliss,

53
00:04:31,000 --> 00:04:33,000
bodily bliss and mental bliss,

54
00:04:33,000 --> 00:04:36,000
tranquilizing the mental formation,

55
00:04:36,000 --> 00:04:39,000
tranquilizing the gross mental formation,

56
00:04:39,000 --> 00:04:42,000
stopping it is the meaning.

57
00:04:42,000 --> 00:04:53,000
And this should be understood in detail in the same way as given under the bodily formation.

58
00:04:53,000 --> 00:04:58,000
Here, moreover, in the happiness clause feeling,

59
00:04:58,000 --> 00:05:02,000
which is actually being contemplated in this contract,

60
00:05:02,000 --> 00:05:06,000
is stated under the hidden of happiness,

61
00:05:06,000 --> 00:05:08,000
which is a formation,

62
00:05:08,000 --> 00:05:16,000
but in the police clause feeling is stated in its own form.

63
00:05:16,000 --> 00:05:19,000
In the two mental formation clauses,

64
00:05:19,000 --> 00:05:27,000
the feeling is that necessarily associated with perception because of the words,

65
00:05:27,000 --> 00:05:31,000
perception and feeling belong to the mind.

66
00:05:31,000 --> 00:05:37,000
These things being bound up with the mind are mental formations.

67
00:05:37,000 --> 00:05:47,000
So these the threat should be understood to deal with the contemplation of feeling.

68
00:05:47,000 --> 00:05:49,000
In the third tetrad,

69
00:05:49,000 --> 00:05:56,000
the experiencing of the manner of consciousness must be understood to be through four genres.

70
00:05:56,000 --> 00:05:59,000
Gladening the manner of consciousness,

71
00:05:59,000 --> 00:06:01,000
he trains with us,

72
00:06:01,000 --> 00:06:05,000
making the mind glad and stilling gladness into it,

73
00:06:05,000 --> 00:06:08,000
cheering it, rejoicing it,

74
00:06:08,000 --> 00:06:11,000
I shall breathe in, shall breathe out.

75
00:06:11,000 --> 00:06:14,000
Here in, there is gladening in two ways,

76
00:06:14,000 --> 00:06:17,000
through concentration and through insight,

77
00:06:17,000 --> 00:06:19,000
how through concentration.

78
00:06:19,000 --> 00:06:24,000
He attains the two genres in which happiness is present.

79
00:06:24,000 --> 00:06:27,000
At the time when he actually entered upon them,

80
00:06:27,000 --> 00:06:30,000
he inspires the mind with gladness,

81
00:06:30,000 --> 00:06:34,000
and stills gladness into it by means of the happiness associated

82
00:06:34,000 --> 00:06:38,000
with the genre, how through insight.

83
00:06:38,000 --> 00:06:44,000
After entering upon and emerging from one of the two genres accompanied by happiness,

84
00:06:44,000 --> 00:06:49,000
she comprehends with insight that happiness associated with the genre,

85
00:06:49,000 --> 00:06:53,000
as liable to destruction and to fall.

86
00:06:53,000 --> 00:06:56,000
Thus, at the actual time of insight,

87
00:06:56,000 --> 00:07:01,000
she inspires the mind with gladness and stills gladness into it,

88
00:07:01,000 --> 00:07:05,000
by making the happiness associated with the genre the object.

89
00:07:05,000 --> 00:07:08,000
It is of one progressing in the way,

90
00:07:08,000 --> 00:07:11,000
that the words he trains with us,

91
00:07:11,000 --> 00:07:14,000
I shall breathe in, shall breathe out,

92
00:07:14,000 --> 00:07:19,000
gladening the manner of consciousness are said.

93
00:07:19,000 --> 00:07:23,000
Concentrating some of them,

94
00:07:23,000 --> 00:07:26,000
the minute of consciousness,

95
00:07:26,000 --> 00:07:32,000
evenly some of them placing advantage of the mind,

96
00:07:32,000 --> 00:07:37,000
evenly putting it on its objects,

97
00:07:37,000 --> 00:07:41,000
by means of the first genre and so on.

98
00:07:41,000 --> 00:07:44,000
Or alternatively,

99
00:07:44,000 --> 00:07:50,000
when having entered upon those genres and emerge from them,

100
00:07:50,000 --> 00:07:55,000
he comprehends with insight the consciousness associated

101
00:07:55,000 --> 00:08:00,000
with the genre as liable to destruction and to fall.

102
00:08:00,000 --> 00:08:07,000
Then, at the actual time of the insight momentary unification of the mind,

103
00:08:07,000 --> 00:08:14,000
arises through the penetration of the characteristics of impermanence and so on.

104
00:08:14,000 --> 00:08:18,000
Thus, the words he trains us,

105
00:08:18,000 --> 00:08:22,000
I shall breathe in, shall breathe out,

106
00:08:22,000 --> 00:08:26,000
concentrate in the manner of consciousness.

107
00:08:26,000 --> 00:08:31,000
I said also of one who even replaces the mind,

108
00:08:31,000 --> 00:08:34,000
evenly puts us in,

109
00:08:34,000 --> 00:08:38,000
or puts us on its object,

110
00:08:38,000 --> 00:08:44,000
my means of the momentary unification of the mind arising to us.

111
00:08:44,000 --> 00:08:47,000
Liberating the manner of consciousness,

112
00:08:47,000 --> 00:08:50,000
he both breathes in and breathes out,

113
00:08:50,000 --> 00:08:55,000
varying, liberating the mind from the hindrances by means of the first genre,

114
00:08:55,000 --> 00:08:59,000
from applied and sustained thoughts by means of the second,

115
00:08:59,000 --> 00:09:02,000
from happiness by means of the third,

116
00:09:02,000 --> 00:09:05,000
from pleasure and pain by means of the force.

117
00:09:05,000 --> 00:09:07,000
Or alternatively,

118
00:09:07,000 --> 00:09:12,000
when having entered upon those genres and emerge from them,

119
00:09:12,000 --> 00:09:18,000
he comprehends with insight the consciousness associated with the genre,

120
00:09:18,000 --> 00:09:21,000
as liable to destruction and to fall.

121
00:09:21,000 --> 00:09:24,000
Then, at the actual time of insight,

122
00:09:24,000 --> 00:09:28,000
he delivers, liberates the mind from the perception of permanence,

123
00:09:28,000 --> 00:09:31,000
by means of the contemplation of impermanence,

124
00:09:31,000 --> 00:09:36,000
from the perception of pleasure by means of the contemplation of pain,

125
00:09:36,000 --> 00:09:41,000
from the perception of self, by means of the contemplation of not self,

126
00:09:41,000 --> 00:09:46,000
from delight, by means of the contemplation of dispassion,

127
00:09:46,000 --> 00:09:51,000
from greed, by means of the contemplation of fading away,

128
00:09:51,000 --> 00:09:56,000
from arousing, by means of the contemplation of cessation,

129
00:09:56,000 --> 00:10:01,000
from grasping, by means of the contemplation of ruling question.

130
00:10:01,000 --> 00:10:03,000
Hence, it is said,

131
00:10:03,000 --> 00:10:06,000
he trains, thus, I shall breathe in,

132
00:10:06,000 --> 00:10:11,000
shall breathe out, liberate in the manner of consciousness.

133
00:10:11,000 --> 00:10:15,000
So, this tetrach should be understood to deal with the contemplation of mind.

134
00:10:15,000 --> 00:10:18,000
Welcome, Bhante.

135
00:10:18,000 --> 00:10:22,000
Hello, Bhante.

136
00:10:22,000 --> 00:10:27,000
Word commentary continued, fourth tetrad,

137
00:10:27,000 --> 00:10:29,000
but in the fourth tetrad,

138
00:10:29,000 --> 00:10:31,000
as to contemplating impermanence,

139
00:10:31,000 --> 00:10:34,000
here, firstly, the impermanence should be understood,

140
00:10:34,000 --> 00:10:37,000
and impermanence, as the contemplation of the impermanence,

141
00:10:37,000 --> 00:10:40,000
and one contemplating impermanence.

142
00:10:40,000 --> 00:10:42,000
Here, in the five aggregates,

143
00:10:42,000 --> 00:10:45,000
are the impermanence.

144
00:10:45,000 --> 00:10:50,000
Why? Because there essence is rise and fall in change.

145
00:10:50,000 --> 00:10:54,000
Impermanence is the rise and fall in change in those same aggregates,

146
00:10:54,000 --> 00:10:57,000
or it is their non-existence after having been.

147
00:10:57,000 --> 00:11:01,000
The meaning is, it is the breakup of produced aggregates,

148
00:11:01,000 --> 00:11:03,000
through their momentary dissolution.

149
00:11:03,000 --> 00:11:06,000
Since they do not remain in the same mode,

150
00:11:06,000 --> 00:11:11,000
contemplation of impermanence is contemplation of materiality, etc.

151
00:11:11,000 --> 00:11:15,000
as impermanent in virtue of that impermanence.

152
00:11:15,000 --> 00:11:19,000
When contemplating impermanence possesses that contemplation,

153
00:11:19,000 --> 00:11:23,000
so it is when one such as this is breathing in and breathing out,

154
00:11:23,000 --> 00:11:27,000
that it can be understood of him, he trains thus.

155
00:11:27,000 --> 00:11:31,000
I shall breathe in, shall breathe out, contemplating the impermanence.

156
00:11:31,000 --> 00:11:39,000
Contemplating, fading away.

157
00:11:39,000 --> 00:11:41,000
There are two kinds of fading away,

158
00:11:41,000 --> 00:11:46,000
that is fading away as destruction, and absolute fading away.

159
00:11:46,000 --> 00:11:50,000
Here, in fading away as destruction is momentary dissolution,

160
00:11:50,000 --> 00:11:55,000
the sedution of formations, absolute fading away is nibbana,

161
00:11:55,000 --> 00:11:59,000
contemplation of fading away inside, and it is the path,

162
00:11:59,000 --> 00:12:02,000
which occurs as the seeing of these two.

163
00:12:02,000 --> 00:12:07,000
It is when he possesses this two-fold contemplation,

164
00:12:07,000 --> 00:12:10,000
that it can be understood of him.

165
00:12:10,000 --> 00:12:14,000
He trains thus, I shall breathe in, shall breathe out,

166
00:12:14,000 --> 00:12:16,000
contemplating fading away.

167
00:12:16,000 --> 00:12:19,000
The same method of explanation applies to cross,

168
00:12:19,000 --> 00:12:22,000
contemplating cessation.

169
00:12:22,000 --> 00:12:34,000
Contemplating, relinquishment is of two kinds, too,

170
00:12:34,000 --> 00:12:39,000
that is to say, relinquishment as giving up,

171
00:12:39,000 --> 00:12:44,000
and relinquishment as entering into.

172
00:12:44,000 --> 00:12:56,000
Relingishment itself as a way of contemplation is contemplation of relinquishment.

173
00:12:56,000 --> 00:13:03,000
For insight, it is called both relinquishment as giving up,

174
00:13:03,000 --> 00:13:07,000
and relinquishment as entering into.

175
00:13:07,000 --> 00:13:17,000
Since, firstly, through substitution of opposite qualities,

176
00:13:17,000 --> 00:13:27,000
it gives up defilements with their aggregate producing gamma formations,

177
00:13:27,000 --> 00:13:37,000
and secondly, through seeing the wrenchness of what is formed,

178
00:13:37,000 --> 00:13:45,000
it is also enters into nibbana by cleaning towards nibbana,

179
00:13:45,000 --> 00:13:49,000
which is the opposite of the formed form.

180
00:13:49,000 --> 00:13:56,000
Also, the path is called both relinquishment as giving up,

181
00:13:56,000 --> 00:14:00,000
and relinquishment as entering into.

182
00:14:00,000 --> 00:14:10,000
Since it gives up defilements with their aggregate producing gamma formations by cutting them off,

183
00:14:10,000 --> 00:14:19,000
and it enters into nibbana by making it its object.

184
00:14:19,000 --> 00:14:27,000
Also, both insight and path knowledge are called contemplation,

185
00:14:27,000 --> 00:14:37,000
anew, passana, because of their receiving successively,

186
00:14:37,000 --> 00:14:50,000
and anew, anew, passana, each preceding kind of knowledge.

187
00:14:50,000 --> 00:14:53,000
It is when he, sorry.

188
00:14:53,000 --> 00:14:56,000
Just if I can explain that well, we're here.

189
00:14:56,000 --> 00:14:59,000
In Polly, that's a common, because you're speaking to Polly,

190
00:14:59,000 --> 00:15:05,000
it's interesting to note that that's a common manner of speaking in Polly

191
00:15:05,000 --> 00:15:10,000
to repeat a word, when you repeat a word, it means end again.

192
00:15:10,000 --> 00:15:15,000
So, it's anno means, in regards to,

193
00:15:15,000 --> 00:15:20,000
so anno anno is just a colloquial way of saying again and again,

194
00:15:20,000 --> 00:15:24,000
seeing again and again.

195
00:15:24,000 --> 00:15:29,000
So, it's anno anno passana.

196
00:15:29,000 --> 00:15:36,000
Yeah, it's just common to repeat something that's what it means in Polly and again.

197
00:15:36,000 --> 00:15:41,000
Okay, thank you.

198
00:15:41,000 --> 00:15:55,000
It is when he possesses this two-fold contemplation that is can be understood of him.

199
00:15:55,000 --> 00:16:00,000
He translates, I shall breathe in, show, show,

200
00:16:00,000 --> 00:16:05,000
without contemplating relinquishment.

201
00:16:05,000 --> 00:16:14,000
This tetrad deals only with pure insight,

202
00:16:14,000 --> 00:16:18,000
while the previous three deal with serenity and insight.

203
00:16:18,000 --> 00:16:22,000
This is how the development of mindfulness of breathing with its sixteen bases

204
00:16:22,000 --> 00:16:26,000
in four tetrades should be understood.

205
00:16:26,000 --> 00:16:31,000
In conclusion, this mindfulness of breathing with its sixteen bases

206
00:16:31,000 --> 00:16:36,000
thus is of great fruit, of great benefit.

207
00:16:36,000 --> 00:16:42,000
It's great, efficientness should be understood here as usefulness,

208
00:16:42,000 --> 00:16:45,000
but because of the word.

209
00:16:45,000 --> 00:16:49,000
And because this concentration, through mindfulness of breathing,

210
00:16:49,000 --> 00:16:55,000
when developed a much practice, it's both peaceful and sublime, etc.

211
00:16:55,000 --> 00:17:00,000
And because of its ability to cut off applied thoughts,

212
00:17:00,000 --> 00:17:04,000
for it is because if it is peaceful, it's sublime,

213
00:17:04,000 --> 00:17:07,000
and then an adulterated place,

214
00:17:07,000 --> 00:17:14,000
abiding that it's cut, that it cuts off the mind's running higher and

215
00:17:14,000 --> 00:17:20,000
later with applied thoughts, obstructive to concentration,

216
00:17:20,000 --> 00:17:26,000
then gives the mind only on the blood adsorption.

217
00:17:26,000 --> 00:17:36,000
Hence, it is said mindfulness of breathing should be developed in order to cut off applied thoughts.

218
00:17:36,000 --> 00:17:41,000
Also, it's great, beneficialness should be understood as the root condition

219
00:17:41,000 --> 00:17:44,000
for the perfecting of clear vision and deliverance.

220
00:17:44,000 --> 00:17:47,000
For this has been said by the plus at one.

221
00:17:47,000 --> 00:17:52,000
Be curious, mindfulness of breathing on developed and much practice

222
00:17:52,000 --> 00:17:55,000
perfect the four foundations of mindfulness.

223
00:17:55,000 --> 00:17:59,000
The four foundations of mindfulness from developed and much practice

224
00:17:59,000 --> 00:18:02,000
perfect the seven enlightenment factors.

225
00:18:02,000 --> 00:18:06,000
The seven enlightenment factors on developed and much practiced

226
00:18:06,000 --> 00:18:10,000
perfect clear vision and deliverance.

227
00:18:10,000 --> 00:18:15,000
May I ask, we share the seven enlightenment factors?

228
00:18:17,000 --> 00:18:19,000
You may.

229
00:18:19,000 --> 00:18:24,000
They are set, which is mindfulness, dhamma vichaya,

230
00:18:24,000 --> 00:18:29,000
which is the discrimination of good and bad dhammas,

231
00:18:29,000 --> 00:18:42,000
effort, rapture or zeal or interest,

232
00:18:42,000 --> 00:18:48,000
tranquility, concentration and equanimity.

233
00:18:48,000 --> 00:18:51,000
Thank you.

234
00:18:51,000 --> 00:18:57,000
Again, it's great beneficialness should be understood to reside in the fact

235
00:18:57,000 --> 00:19:04,000
that it causes the final in-breaths and out in-breaths and out-breaths

236
00:19:04,000 --> 00:19:08,000
to be known for this is said by the plus at one.

237
00:19:08,000 --> 00:19:13,000
Raula, when mindfulness of breathing is thus developed,

238
00:19:13,000 --> 00:19:19,000
thus practiced much, the final in-breaths and out-breaths

239
00:19:19,000 --> 00:19:27,000
to our known as they cease not unknown.

240
00:19:27,000 --> 00:19:34,000
Hearing the three kinds of breaths that are final because of cessation,

241
00:19:34,000 --> 00:19:40,000
that is to say finally becoming final in-breaths and finally death

242
00:19:40,000 --> 00:19:45,000
for among the various kinds of becoming existence,

243
00:19:45,000 --> 00:19:50,000
interests and out-breaths occur in the central sphere becoming

244
00:19:50,000 --> 00:19:55,000
not in the five material and immaterial kinds of becoming.

245
00:19:55,000 --> 00:19:59,000
That is why there are final ones in becoming.

246
00:19:59,000 --> 00:20:06,000
In the jalas they occur in the first three, but not in the fourth.

247
00:20:06,000 --> 00:20:10,000
That is why there are final ones in jalam.

248
00:20:10,000 --> 00:20:17,000
Those that arise along with the 16th consciousness preceding the death consciousness

249
00:20:17,000 --> 00:20:20,000
cease together with the death consciousness.

250
00:20:20,000 --> 00:20:23,000
They are called final in-breaths.

251
00:20:23,000 --> 00:20:34,000
It is these last that are meant here by final.

252
00:20:34,000 --> 00:20:40,000
When a vehicle has devoted himself to this meditation subject,

253
00:20:40,000 --> 00:20:47,000
it seems if he adverts at the moment of arising of the 16th consciousness

254
00:20:47,000 --> 00:20:52,000
before the death consciousness to their arising,

255
00:20:52,000 --> 00:20:56,000
then their arising is evident to him.

256
00:20:56,000 --> 00:21:00,000
If he adverts to their presence,

257
00:21:00,000 --> 00:21:04,000
then their presence is evident to him.

258
00:21:04,000 --> 00:21:09,000
If he adverts to their dissolution,

259
00:21:09,000 --> 00:21:13,000
then their dissolution is evident to him.

260
00:21:13,000 --> 00:21:24,000
So it is so because he has thoroughly discerned in-breaths and out-breaths as object.

261
00:21:24,000 --> 00:21:31,000
When a vehicle has attained our haunt ship by developing some other meditation subject

262
00:21:31,000 --> 00:21:36,000
than this one, he may be able to define his life term or not.

263
00:21:36,000 --> 00:21:41,000
But when he has reached our haunt ship by developing this mindfulness of breathing

264
00:21:41,000 --> 00:21:46,000
with its 16 bases, he can always define his life term.

265
00:21:46,000 --> 00:21:52,000
He knows my vital formations will continue now for so long and no more.

266
00:21:52,000 --> 00:21:58,000
Automatically, he performs all the functions of attending to the body,

267
00:21:58,000 --> 00:22:01,000
dressing, and roving, and so on,

268
00:22:01,000 --> 00:22:10,000
after which he closes his eyes like the elder Tisa who lived in the kotabata monastery,

269
00:22:10,000 --> 00:22:19,000
like the elder Mahatisa who lived at the Maha-karan-gia monastery,

270
00:22:19,000 --> 00:22:25,000
like the elder Tisa, the alms food eater in the kingdom of Devaputa,

271
00:22:25,000 --> 00:22:34,000
like the elders who were brothers and lived at the Sita-la-pa-bata monastery.

272
00:22:34,000 --> 00:22:50,000
He is one story as an illusion. After recitation, the patimokha, it seems, on the opa-sata day of the full moon,

273
00:22:50,000 --> 00:22:58,000
one of two elders who know where brothers went to his own dwelling place,

274
00:22:58,000 --> 00:23:04,000
surrounded by the community of bicus.

275
00:23:04,000 --> 00:23:09,000
As he stood on the walk looking at the moon line,

276
00:23:09,000 --> 00:23:16,000
he calculated his own beta formations, and he said to the community of bicus,

277
00:23:16,000 --> 00:23:23,000
in what we have you seen bicus attaining banana up till now.

278
00:23:23,000 --> 00:23:31,000
To now, we have seen them attaining banana sitting in their seats.

279
00:23:31,000 --> 00:23:38,000
Others answer. We have seen them sitting close-lit in the air.

280
00:23:38,000 --> 00:23:45,000
The elder said, I shall now show you one attaining banana while walking.

281
00:23:45,000 --> 00:23:59,000
He then drew a line on this walk saying, I shall go from this end of the walk to the other end of the turn.

282
00:23:59,000 --> 00:24:04,000
When I reach this line, I shall attaining banana.

283
00:24:04,000 --> 00:24:20,000
So, same, he stepped on the walk and went to the far end. On his return, he attaining banana in the same moment in which he stepped on the line.

284
00:24:20,000 --> 00:24:35,000
So, let a man, if he is wise, was travelling both his days to maintain mindfulness of breathing, which rewards him always in this way.

285
00:24:35,000 --> 00:24:44,000
This is the section dealing with the mindfulness of breathing in the tail explanation.

286
00:24:44,000 --> 00:24:58,000
The collection of peace. One who wants to develop the recollection of peace mentioned next to mindfulness of breathing should go into solitary retreat and recollect the special qualities of nibana.

287
00:24:58,000 --> 00:25:03,000
In other words, the stealing of all suffering as follows.

288
00:25:03,000 --> 00:25:31,000
Because, insofar as there are dumbas, whether formed or unformed, fading away is pronounced the best of them. That is to say, the disillusionment of vanity, the elimination of thirst, the abolition of reliance, the termination of the round, the destruction of craving, fading away, cessation, nibana.

289
00:25:31,000 --> 00:25:52,000
Here in, so far as means as many as, as many as, dumbas means individual essences, whether formed or unformed, whether made by conditions going together, coming together, or not so made, fading away is pronounced the best of them.

290
00:25:52,000 --> 00:26:06,000
Of those formed and unformed dumbas, fading away is pronounced the best, is called the foremost, the highest.

291
00:26:06,000 --> 00:26:28,000
I just came back, sorry. Here in fading away is not the absence of greed, but rather it is that uniformed dhamma, which while given the names, disillusionment of vanity, etc.

292
00:26:28,000 --> 00:26:38,000
In that clause, that is to say, disillusionment of vanity, nibana is treated basically as riding away.

293
00:26:38,000 --> 00:26:56,000
It is called disillusionment of vanity, because I am coming to, it all kinds of vanity, intoxication, such as the vanity of conceit and vanity of manhood, or disillusioned, undone, done away with.

294
00:26:56,000 --> 00:27:07,000
And it is called elimination of thirst, because I am coming to eat all thirst for sense desires, is eliminated and crunched.

295
00:27:07,000 --> 00:27:17,000
But it is called abolition of reliance, because I am coming to its reliance on the vile courts of sense desire is abolished.

296
00:27:17,000 --> 00:27:32,000
It is called termination of the round birth, termination of the round, because I am coming to it, the round of the three planes of excess existence is terminated.

297
00:27:32,000 --> 00:27:42,000
It is called destruction of craving, because I am coming to it, craving is.

298
00:27:42,000 --> 00:28:05,000
It is called nibana extension, because it has gone away from, the counter has escaped from this, this is associated from craving, which has acquired in common usage the name.

299
00:28:05,000 --> 00:28:31,000
Fast, fastening, vana, because by ensuring success, you will be coming, craving, service, as we are joining together, binding together, releasing together of all kinds of generation, by destinations of concessions, and nine birds of beings.

300
00:28:31,000 --> 00:28:37,000
But there have a question regarding the previous chapter.

301
00:28:37,000 --> 00:28:49,000
In the previous chapter, it said that a big two who has used that meditation technique can define its lifetime.

302
00:28:49,000 --> 00:29:00,000
Does that mean the maximum lifespan of that time, like 100 years, during the time of the Buddha, 120 years?

303
00:29:00,000 --> 00:29:07,000
No, it means he knows when going to die.

304
00:29:07,000 --> 00:29:11,000
He knows.

305
00:29:11,000 --> 00:29:29,000
It is kind of curious, I am not convinced that that is the case for everyone who becomes an arahat bruh, and a vana sati, but he seems to think so that automatically by, because you are practicing this meditation, you automatically know.

306
00:29:29,000 --> 00:29:48,000
It is the association with breath, and because someone is so in tune with the breath, and because the idea was that the breath is equivalent to life, so when it is open to breath, when it is open, it is able to know when death is going to occur.

307
00:29:48,000 --> 00:30:01,000
That is something different, that is being able to extend one's life.

308
00:30:01,000 --> 00:30:09,000
There are stories of the monks who were able to know when they were going to die, so there is that.

309
00:30:09,000 --> 00:30:19,000
It seems like it is a doable thing, it is surprising that it is for everyone to practice a vana pana sati, but I mean who am I to know?

310
00:30:19,000 --> 00:30:24,000
I have a question one day.

311
00:30:24,000 --> 00:30:42,000
One knows if that is true, or if one knows when they are going to die, when that hinders the practice.

312
00:30:42,000 --> 00:31:00,000
If I knew when I was going to die, I think I would be thinking about it, and it would probably hinder the advancement of the practice maybe, because

313
00:31:00,000 --> 00:31:13,000
they are referring to an arahan, so they have no more practice left to do.

314
00:31:13,000 --> 00:31:41,000
This is how peace, in other words, nibana should be recollected according to its special qualities, beginning with this illusionment of vanity, but it should also be recollected according to the other special qualities of peace, stated by the blessed ones.

315
00:31:41,000 --> 00:32:10,000
In the sutas, beginning with because I shall teach you the unformed truth, the other sure, the hard to see, the undecaying, the lasting, the unidentified, the deathless, the auspicious, the safe, the marvelous,

316
00:32:10,000 --> 00:32:29,000
the intact, the unaflected, the purity, the island, the shelter.

317
00:32:29,000 --> 00:32:36,000
I have a question for Audrey.

318
00:32:36,000 --> 00:32:43,000
Do you want to read the next paragraph?

319
00:32:43,000 --> 00:32:52,000
Audrey?

320
00:32:52,000 --> 00:33:08,000
She ran away. She watched the DVD the second time.

321
00:33:08,000 --> 00:33:25,000
She says she was practicing, but it's natural.

322
00:33:25,000 --> 00:33:41,000
On that occasion, his mind is not obsessed by greed or obsessed by hate or obsessed by delusion.

323
00:33:41,000 --> 00:34:10,000
She is in the way already described under the recollection of the enlightened one, etc.

324
00:34:10,000 --> 00:34:34,000
This is so, it can nevertheless also be brought to mind by the ordinary person who values peace.

325
00:34:34,000 --> 00:34:42,000
But even by hearsay, the mind has confidence in peace.

326
00:34:42,000 --> 00:34:48,000
A beaker who is devoted to the recollection of peace sleeps in bliss and wakes in bliss.

327
00:34:48,000 --> 00:34:55,000
His faculties are peaceful, his mind is peaceful, as conscious and shame is confident.

328
00:34:55,000 --> 00:35:03,000
His resolve to attain the superior state, he is respected and honored by his fellows in his life of purity.

329
00:35:03,000 --> 00:35:10,000
And even if he penetrates no higher, he is at least headed for happy destiny.

330
00:35:10,000 --> 00:35:20,000
So, that is why a man of weight, entirely devotes his days to mind the noble peace, which can reward him in so many ways.

331
00:35:20,000 --> 00:35:25,000
This is the section dealing with the recollection of peace and the detailed explanation.

332
00:35:25,000 --> 00:35:41,000
The eighth chapter called the description of recollection as meditation subjects in the treaties of the development of concentration and the path of purification composed for the purpose of gladening good people.

333
00:35:41,000 --> 00:35:59,000
So, we have finished 30, I think, 30 of the meditation subjects, 10 more to go, I think, and then we get into the magical powers.

334
00:35:59,000 --> 00:36:13,000
A few more chapters to go. And then, after the magical powers, there are a couple of chapters on them, then we get into part three, which is wisdom.

335
00:36:13,000 --> 00:36:25,000
So, a lot of what the bulk of what we have been studying has been stuff that in, if you are following the tradition that I, we don't pay too much attention to it.

336
00:36:25,000 --> 00:36:29,000
It's interesting for us to know, but we are not that concerned with these things.

337
00:36:29,000 --> 00:36:37,000
Clearly, the Buddha also wasn't that concerned with them, though he had accomplished all of these and he enjoyed his students.

338
00:36:37,000 --> 00:36:44,000
To practice them, it was clear that it wasn't clearly the goal and it wasn't really particularly good as to either.

339
00:36:44,000 --> 00:36:53,000
It was a sense that other religions had these magical powers and these attainments.

340
00:36:53,000 --> 00:37:06,000
So, it was putting them in their place and categorizing them and showing them where they, the part that they played in the past, where in the past they were situated.

341
00:37:06,000 --> 00:37:14,000
So, we are making good time. We are going to, we are making progress on this.

342
00:37:14,000 --> 00:37:17,000
I guess that's all for today.

343
00:37:17,000 --> 00:37:24,000
Thanks for coming.

