1
00:00:30,000 --> 00:00:40,000
Sorry, I can't stop more broadcasting life, but I'm not ready to stop things wrong.

2
00:01:00,000 --> 00:01:24,000
I'm not ready to stop things wrong, but I'm not ready to stop things wrong.

3
00:01:24,000 --> 00:01:50,000
Hello, evening, broadcasting life November 15, 2015.

4
00:01:50,000 --> 00:02:06,000
Whoever is easy to speak to because of respecting, we're veering and honoring the dumber.

5
00:02:06,000 --> 00:02:34,000
Please speak to this gracious and I call him easy to speak to.

6
00:02:34,000 --> 00:02:46,000
What was that Dante?

7
00:02:46,000 --> 00:02:51,000
What was that Dante?

8
00:02:51,000 --> 00:03:17,000
I'm not ready to speak to you.

9
00:03:17,000 --> 00:03:32,000
In the first year, dumber studies in Thailand, the dumber that are great support, satika,

10
00:03:32,000 --> 00:03:53,000
who are hard to find in the world.

11
00:03:53,000 --> 00:04:06,000
Pumbakari, the one who does good deeds to others, un-promcted.

12
00:04:06,000 --> 00:04:19,000
Without being owed, it does good deeds for others, without being owed them.

13
00:04:19,000 --> 00:04:29,000
Then there's four, the third one.

14
00:04:29,000 --> 00:04:43,000
The time-tippen-lokaban, the time-tippen-kung-kung-low, the dumber that protect the world.

15
00:04:43,000 --> 00:04:55,000
The fear of shame of wrongdoing and fear of the results of wrongdoing.

16
00:04:55,000 --> 00:05:02,000
Number one, the time-tippen-lingan country comes from our cap.

17
00:05:02,000 --> 00:05:12,000
Comedy is patient.

18
00:05:12,000 --> 00:05:23,000
balanced.

19
00:05:23,000 --> 00:05:40,000
So that's sort of the whole team, Niwata, Niwata is an interesting word.

20
00:05:40,000 --> 00:05:50,000
What I mean is wind, what is, because why, what is, it means wind.

21
00:05:50,000 --> 00:05:58,000
Niwata means having the wind out, having, with wind removed, kind of thing.

22
00:05:58,000 --> 00:06:01,000
The mean banner, we're being unbounded.

23
00:06:01,000 --> 00:06:04,000
Niwata is unwinded.

24
00:06:04,000 --> 00:06:12,000
What it means is, not being popped up.

25
00:06:12,000 --> 00:06:18,000
It's a humility.

26
00:06:18,000 --> 00:06:19,000
Without wind out.

27
00:06:19,000 --> 00:06:25,000
I mean, that the power English dictionary just says, with the wind gone down, it doesn't

28
00:06:25,000 --> 00:06:33,000
make mention of the fact that it's often used to talk about a person that's being, without

29
00:06:33,000 --> 00:06:37,000
being off, not being off of them.

30
00:06:37,000 --> 00:06:46,000
Who person to person to who who.

31
00:06:46,000 --> 00:06:51,000
Who person to mean calm, or at peace, who.

32
00:06:51,000 --> 00:07:02,000
Purnings, gone to a fully mean.

33
00:07:02,000 --> 00:07:08,000
And Santa comes from some, which means to, to be quiet or peaceful.

34
00:07:08,000 --> 00:07:31,000
Santa, Santa means peaceful, but Santa is someone who is gone to peace, become peaceful.

35
00:07:31,000 --> 00:07:41,000
Such a person who is, these things, when, yeah, when the map, no, no wind.

36
00:07:41,000 --> 00:08:09,000
They are touched, 14.

37
00:08:09,000 --> 00:08:11,000
It's interesting.

38
00:08:11,000 --> 00:08:15,000
I think what he's saying is, oh, there are only these, some, for the only this way, when

39
00:08:15,000 --> 00:08:16,000
they're not touched.

40
00:08:16,000 --> 00:08:21,000
Yeah, when that, I'm not, ah, watching a tab with something.

41
00:08:21,000 --> 00:08:29,200
They're only this way, in so long as they are not touched by speech that is under

42
00:08:29,200 --> 00:08:35,000
speech that is unendearing, so harsh speech.

43
00:08:35,000 --> 00:08:53,000
And then I don't quite was being sent here, but I think it's basically that, and I don't consider

44
00:08:53,000 --> 00:08:57,000
them to be easy to talk to.

45
00:08:57,000 --> 00:09:01,000
Because sometimes you have to talk to people about things that they don't want to hear.

46
00:09:01,000 --> 00:09:05,000
And if they can't take it, then they're not easy to talk to.

47
00:09:05,000 --> 00:09:08,000
So being easy to talk to is important.

48
00:09:08,000 --> 00:09:10,000
It's in the main goal of Santa.

49
00:09:10,000 --> 00:09:18,000
So what you'll, it's an important quality of the student.

50
00:09:18,000 --> 00:09:23,000
It's an important quality of a meditator, a practitioner.

51
00:09:23,000 --> 00:09:24,000
Right?

52
00:09:24,000 --> 00:09:30,000
So this is why when people accuse us or attack us, we have to remember the teaching.

53
00:09:30,000 --> 00:09:33,000
And not yet.

54
00:09:33,000 --> 00:09:34,000
Not yet.

55
00:09:34,000 --> 00:09:36,000
And not yet.

56
00:09:36,000 --> 00:09:41,000
I love it.

57
00:09:41,000 --> 00:09:45,000
Is it near my glory?

58
00:09:45,000 --> 00:09:48,000
Is my picture of a glory to you, Albert?

59
00:09:48,000 --> 00:09:50,000
A little blurry.

60
00:09:50,000 --> 00:09:55,000
Wake up.

61
00:09:55,000 --> 00:09:58,000
Are you too far back?

62
00:09:58,000 --> 00:10:00,000
You shouldn't be.

63
00:10:00,000 --> 00:10:02,000
You can't focus on me.

64
00:10:02,000 --> 00:10:06,000
Maybe it's because down here is not blurry enough.

65
00:10:06,000 --> 00:10:10,000
Maybe it's focusing on your mouth on the floor there or something.

66
00:10:10,000 --> 00:10:12,000
Because that's closer.

67
00:10:12,000 --> 00:10:27,000
Maybe it's just, maybe it's just failing to do its job.

68
00:10:27,000 --> 00:10:29,000
And not that.

69
00:10:29,000 --> 00:10:33,000
So you know, it's not going to be angry.

70
00:10:33,000 --> 00:10:34,000
You get angry.

71
00:10:34,000 --> 00:10:37,000
Why if I just can't wait.

72
00:10:37,000 --> 00:10:40,000
And then you can't wait.

73
00:10:40,000 --> 00:10:44,000
And if you do get angry, it can't wait.

74
00:10:44,000 --> 00:10:51,000
And that would be not understood.

75
00:10:51,000 --> 00:10:56,000
And on the break, I think the idea was coming in well as far as I can tell.

76
00:10:56,000 --> 00:11:02,000
What good computers are being made and opportunities for the test test.

77
00:11:02,000 --> 00:11:04,000
Oh, yeah.

78
00:11:04,000 --> 00:11:07,000
Yes.

79
00:11:07,000 --> 00:11:08,000
Yes.

80
00:11:08,000 --> 00:11:10,000
They don't work well for two days in a row.

81
00:11:10,000 --> 00:11:15,000
There's always something.

82
00:11:15,000 --> 00:11:21,000
It's even worse when you can't really, you know, do much with your own computer and you have to depend on other people.

83
00:11:21,000 --> 00:11:23,000
That's the worst.

84
00:11:23,000 --> 00:11:30,000
Yes.

85
00:11:30,000 --> 00:11:35,000
Yes.

86
00:11:35,000 --> 00:11:41,000
What does it mean when you know, for example, rising?

87
00:11:41,000 --> 00:11:46,000
You mean is rising as a word or as a verb?

88
00:11:46,000 --> 00:11:50,000
Or you mean rising as happiness as a noun?

89
00:11:50,000 --> 00:11:55,000
Similar thing with feelings when you know happy, does it refer to me as I am happy?

90
00:11:55,000 --> 00:11:58,000
It doesn't go with the non-self concept.

91
00:11:58,000 --> 00:12:02,000
Or to the feeling as there is now happy feeling in me.

92
00:12:02,000 --> 00:12:06,000
So my question is, what is the meaning of the English labels used when noting?

93
00:12:06,000 --> 00:12:08,000
Could you please explain that?

94
00:12:08,000 --> 00:12:11,000
I meditate 20 to 40 minutes a day, but not on web.

95
00:12:11,000 --> 00:12:13,000
So answer, please.

96
00:12:13,000 --> 00:12:14,000
Thanks.

97
00:12:14,000 --> 00:12:20,000
First of all, this doesn't necessarily go on the non-self concept.

98
00:12:20,000 --> 00:12:23,000
Probably should be too much on it.

99
00:12:23,000 --> 00:12:31,000
But this is an example of this whole idea of non-self, meaning there is no self.

100
00:12:31,000 --> 00:12:34,000
The Buddha didn't ever say there is a self.

101
00:12:34,000 --> 00:12:39,000
Didn't give us any indication to believe that there is a self.

102
00:12:39,000 --> 00:12:46,000
We can sort of wrong view to think that something exists from one moment to the next,

103
00:12:46,000 --> 00:12:53,000
that something continues on from one life to the next, that it's our spirit, that it's reborn.

104
00:12:53,000 --> 00:12:55,000
But he never said there is no self.

105
00:12:55,000 --> 00:12:58,000
When asked point blank, he refused to answer.

106
00:12:58,000 --> 00:13:01,000
And there's a reason for it because it confuses to it.

107
00:13:01,000 --> 00:13:04,000
And you get the kind of semantic problems where you say,

108
00:13:04,000 --> 00:13:08,000
I can't say I am happy because that doesn't go as non-self.

109
00:13:08,000 --> 00:13:10,000
That's not understanding non-self.

110
00:13:10,000 --> 00:13:15,000
Non-self is when your camera doesn't focus on you and you get angry.

111
00:13:15,000 --> 00:13:18,000
You get angry because you don't understand that it's not self.

112
00:13:18,000 --> 00:13:21,000
It's not under your control. It doesn't belong to you.

113
00:13:21,000 --> 00:13:25,000
There is no sense of the universe working according to your command.

114
00:13:25,000 --> 00:13:28,000
That's just not the way the universe works.

115
00:13:28,000 --> 00:13:31,000
It doesn't mean that I don't exist.

116
00:13:31,000 --> 00:13:34,000
That's not what it means. It's not that we're denying that.

117
00:13:34,000 --> 00:13:38,000
It's not what we're saying, I do exist. We're not saying that either.

118
00:13:38,000 --> 00:13:46,000
But you have to understand the context and the direction in which we meant to understand non-self.

119
00:13:46,000 --> 00:13:50,000
That sounds like a attacking you have managed.

120
00:13:50,000 --> 00:13:56,000
It's a very common misconception that we have to clear up

121
00:13:56,000 --> 00:13:58,000
because it's not how we should be thinking.

122
00:13:58,000 --> 00:14:00,000
We shouldn't be thinking in terms of how you do this.

123
00:14:00,000 --> 00:14:05,000
I don't get this very confusing because of all things.

124
00:14:05,000 --> 00:14:10,000
An ultimate reality that question doesn't even make sense.

125
00:14:10,000 --> 00:14:12,000
The idea of self doesn't make sense.

126
00:14:12,000 --> 00:14:22,000
It doesn't have any place in a conversation about ultimate reality.

127
00:14:22,000 --> 00:14:25,000
I am walking.

128
00:14:25,000 --> 00:14:30,000
Well, it's just a little bit of what I'm saying.

129
00:14:30,000 --> 00:14:32,000
It's like you walk away.

130
00:14:32,000 --> 00:14:35,000
And I'm not just saying that without the going to themselves,

131
00:14:35,000 --> 00:14:38,000
they don't go on retirement.

132
00:14:38,000 --> 00:14:43,000
When walking, I walk.

133
00:14:43,000 --> 00:14:45,000
You see no meaning.

134
00:14:45,000 --> 00:14:47,000
You see no meaning.

135
00:14:47,000 --> 00:14:57,000
And he means I am some first person.

136
00:14:57,000 --> 00:15:00,000
You see no.

137
00:15:00,000 --> 00:15:01,000
It's sad.

138
00:15:01,000 --> 00:15:03,000
Basically, I am sad.

139
00:15:03,000 --> 00:15:04,000
I'm sad.

140
00:15:04,000 --> 00:15:05,000
I am sad.

141
00:15:05,000 --> 00:15:06,000
I am sad.

142
00:15:06,000 --> 00:15:07,000
I am sad.

143
00:15:07,000 --> 00:15:08,000
I am sad.

144
00:15:08,000 --> 00:15:09,000
I am sad.

145
00:15:09,000 --> 00:15:10,000
I am sad.

146
00:15:10,000 --> 00:15:18,000
I am sad.

147
00:15:18,000 --> 00:15:23,000
I am sad.

148
00:15:23,000 --> 00:15:25,000
I am sad.

149
00:15:25,000 --> 00:15:28,000
But it does relate to the question in terms of words.

150
00:15:28,000 --> 00:15:29,000
It's just being words.

151
00:15:29,000 --> 00:15:30,000
So it can be a verb.

152
00:15:30,000 --> 00:15:31,000
It can be a noun.

153
00:15:31,000 --> 00:15:34,000
That's not really the most important point here.

154
00:15:34,000 --> 00:15:40,000
We're just talking this morning, or noon, or one.

155
00:15:40,000 --> 00:15:42,000
I was pointing this out.

156
00:15:42,000 --> 00:15:44,000
It's a mantra.

157
00:15:44,000 --> 00:15:45,000
How ancient it is.

158
00:15:45,000 --> 00:15:49,000
It really is very much a part of the practice.

159
00:15:49,000 --> 00:15:51,000
And it's important to use the mantra.

160
00:15:51,000 --> 00:15:55,000
It's something, it's not a new concept that we just thought out.

161
00:15:55,000 --> 00:15:56,000
It's not something.

162
00:15:56,000 --> 00:15:57,000
Mahasi said about that.

163
00:15:57,000 --> 00:16:00,000
But it's what you get when you read the ancient texts.

164
00:16:00,000 --> 00:16:11,000
I was like, in the time of life, I'm only only over 50 of your years old.

165
00:16:11,000 --> 00:16:16,000
So, but it's the act of a mantra that you use.

166
00:16:16,000 --> 00:16:18,000
It's just not so important.

167
00:16:18,000 --> 00:16:21,000
And whether it's a verb or a noun, for example.

168
00:16:21,000 --> 00:16:26,000
It has to relate, and it should relate quite closely to the experience.

169
00:16:26,000 --> 00:16:28,000
And that's all that I want.

170
00:16:28,000 --> 00:16:34,000
How close it is and how related it is to reality.

171
00:16:34,000 --> 00:16:39,000
So, you say, I am walking, and you're focused on the eye.

172
00:16:39,000 --> 00:16:41,000
I am walking.

173
00:16:41,000 --> 00:16:42,000
I'm walking.

174
00:16:42,000 --> 00:16:46,000
I mean, it's so that puts yourself on your feet.

175
00:16:46,000 --> 00:16:48,000
Not someone else's feet.

176
00:16:48,000 --> 00:16:49,000
I am walking.

177
00:16:49,000 --> 00:16:50,000
It's full.

178
00:16:50,000 --> 00:16:54,000
In English, it's not so useful because it makes a separate phrase.

179
00:16:54,000 --> 00:16:58,000
It's separate words, but in certain languages, I am walking is one word.

180
00:16:58,000 --> 00:17:00,000
I think a chami, I go.

181
00:17:00,000 --> 00:17:02,000
So, then it makes perfect sense too.

182
00:17:02,000 --> 00:17:04,000
So, the body used to it.

183
00:17:04,000 --> 00:17:09,000
So, I could just can't criticize him.

184
00:17:09,000 --> 00:17:14,000
Glad to hear your meditating every day that's right.

185
00:17:14,000 --> 00:17:16,000
Yeah, Robin, I think you're echoing me.

186
00:17:16,000 --> 00:17:23,000
That's what I'm being on your hand.

187
00:17:23,000 --> 00:17:26,000
So, I don't know what this is.

188
00:17:26,000 --> 00:17:29,000
Again, more or not.

189
00:17:29,000 --> 00:17:31,000
So, maybe turn your speakers out.

190
00:17:31,000 --> 00:17:32,000
Okay.

191
00:17:32,000 --> 00:17:34,000
If it's quiet, it shouldn't be a problem.

192
00:17:34,000 --> 00:17:38,000
It's too loud a problem.

193
00:17:38,000 --> 00:17:41,000
I'm going to head so I'll be right back.

194
00:17:41,000 --> 00:17:43,000
Okay.

195
00:17:43,000 --> 00:17:54,000
What about the other one?

196
00:17:54,000 --> 00:17:57,000
She's definitely a woman.

197
00:17:57,000 --> 00:18:02,000
What do we have here today?

198
00:18:02,000 --> 00:18:06,000
We have someone named Amado, who is Orange.

199
00:18:06,000 --> 00:18:11,000
There's someone called Amy Amy, who is also Orange.

200
00:18:11,000 --> 00:18:14,000
I don't know why there are those people I don't think.

201
00:18:14,000 --> 00:18:15,000
I don't know those things.

202
00:18:15,000 --> 00:18:16,000
Hey, man.

203
00:18:16,000 --> 00:18:17,000
Sounds familiar.

204
00:18:17,000 --> 00:18:19,000
That's his Sri Lankan name.

205
00:18:19,000 --> 00:18:21,000
Joshua, I know he was physically visited here.

206
00:18:21,000 --> 00:18:24,000
He gave us some paint.

207
00:18:24,000 --> 00:18:27,000
Thanks for the paint, Joshua.

208
00:18:27,000 --> 00:18:29,000
But now that I'm out here, I'm not.

209
00:18:29,000 --> 00:18:34,000
I've lost the strong desire to paint my room.

210
00:18:34,000 --> 00:18:37,000
I have to see where that goes.

211
00:18:37,000 --> 00:18:38,000
Why?

212
00:18:38,000 --> 00:18:41,000
I don't know if I can do that.

213
00:18:41,000 --> 00:18:44,000
I don't know if I can do that.

214
00:18:44,000 --> 00:18:47,000
I don't know if I can do that.

215
00:18:47,000 --> 00:18:49,000
I don't know if I can do that.

216
00:18:49,000 --> 00:18:52,000
Then we have a worm that I can pronounce.

217
00:18:52,000 --> 00:18:54,000
Green, king, shan.

218
00:18:54,000 --> 00:18:57,000
I think we have this.

219
00:18:57,000 --> 00:18:59,000
Tina, I think I know Tina.

220
00:18:59,000 --> 00:19:00,000
Hi, Tina.

221
00:19:00,000 --> 00:19:02,000
Ken Pan.

222
00:19:02,000 --> 00:19:06,000
Anna Figuero.

223
00:19:06,000 --> 00:19:08,000
She's a...

224
00:19:08,000 --> 00:19:11,000
She's in our VCD Madhu classroom.

225
00:19:11,000 --> 00:19:14,000
Pended our volunteer group and coordinating the translations.

226
00:19:14,000 --> 00:19:16,000
The outstanding translations.

227
00:19:16,000 --> 00:19:17,000
Right.

228
00:19:17,000 --> 00:19:18,000
She's a volunteer.

229
00:19:18,000 --> 00:19:19,000
Way to go in.

230
00:19:19,000 --> 00:19:20,000
Yes.

231
00:19:20,000 --> 00:19:22,000
Aurora is an old meditator.

232
00:19:22,000 --> 00:19:24,000
She's going to Burma.

233
00:19:24,000 --> 00:19:25,000
Wow.

234
00:19:25,000 --> 00:19:27,000
She's been a Burma this month.

235
00:19:27,000 --> 00:19:30,000
I don't know if she wants to tell everyone, but she's pretty awesome.

236
00:19:30,000 --> 00:19:37,000
She's going to plant it around us so we can all stay sad and remote in that of her.

237
00:19:37,000 --> 00:19:38,000
I do.

238
00:19:38,000 --> 00:19:39,000
Aurora, that's wonderful.

239
00:19:39,000 --> 00:19:40,000
I do more than a...

240
00:19:40,000 --> 00:19:41,000
Aurora is...

241
00:19:41,000 --> 00:19:42,000
Aurora.

242
00:19:42,000 --> 00:19:43,000
Aurora.

243
00:19:43,000 --> 00:19:51,000
Oh, Aurora and Fernando are in our VCD Madhu group, which has been around for...

244
00:19:51,000 --> 00:19:56,000
I think we've been around for about nine, ten months now.

245
00:19:56,000 --> 00:20:01,000
Wow.

246
00:20:01,000 --> 00:20:04,000
And Fernando's in our group.

247
00:20:04,000 --> 00:20:07,000
He's also one of our weekly meditators.

248
00:20:07,000 --> 00:20:09,000
Kevin, I think.

249
00:20:09,000 --> 00:20:10,000
I don't know.

250
00:20:10,000 --> 00:20:12,000
First things.

251
00:20:12,000 --> 00:20:14,000
Patrick O.

252
00:20:14,000 --> 00:20:16,000
I probably know how many people.

253
00:20:16,000 --> 00:20:18,000
Someone named Robine.

254
00:20:18,000 --> 00:20:19,000
What's that?

255
00:20:19,000 --> 00:20:20,000
No idea.

256
00:20:20,000 --> 00:20:27,000
Oh, it's like a Middle Eastern named Robine.

257
00:20:27,000 --> 00:20:28,000
You'd be by or something.

258
00:20:28,000 --> 00:20:31,000
Sehan is from Sri Lanka.

259
00:20:31,000 --> 00:20:33,000
He's visited me several times.

260
00:20:33,000 --> 00:20:38,000
They'll be coming here in this way for becoming next week, I think.

261
00:20:38,000 --> 00:20:41,000
Simon, Simon, I think is an old Simon.

262
00:20:41,000 --> 00:20:43,000
He's been around for a while.

263
00:20:43,000 --> 00:20:46,000
Simon is in Denmark, I think.

264
00:20:46,000 --> 00:20:50,000
Simon, he's been around for a while, I think.

265
00:20:50,000 --> 00:20:51,000
Yeah.

266
00:20:51,000 --> 00:21:02,000
Tim's been around from the very beginning.

267
00:21:02,000 --> 00:21:05,000
High level.

268
00:21:05,000 --> 00:21:08,000
The higher up.

269
00:21:08,000 --> 00:21:11,000
Senior meditators.

270
00:21:11,000 --> 00:21:14,000
Senior because they're all.

271
00:21:14,000 --> 00:21:17,000
Tim's been around from the very beginning.

272
00:21:17,000 --> 00:21:19,000
Tim and Wayne are awesome.

273
00:21:19,000 --> 00:21:21,000
I got them right, right, Wayne.

274
00:21:21,000 --> 00:21:24,000
And Tim is the other one.

275
00:21:24,000 --> 00:21:25,000
No.

276
00:21:25,000 --> 00:21:28,000
Wayne and Tim.

277
00:21:28,000 --> 00:21:31,000
Yeah.

278
00:21:31,000 --> 00:21:33,000
They meet one after the other.

279
00:21:33,000 --> 00:21:35,000
I get to talk about them.

280
00:21:35,000 --> 00:21:38,000
They're doing it very well.

281
00:21:38,000 --> 00:21:40,000
And there's that little dumb guy.

282
00:21:40,000 --> 00:21:43,000
What a freak.

283
00:21:43,000 --> 00:21:44,000
You know, my call.

284
00:21:44,000 --> 00:21:47,000
Don't know my kind of mic.

285
00:21:47,000 --> 00:21:49,000
The slang don't know that.

286
00:21:49,000 --> 00:21:52,000
But the press doesn't spin here for a while.

287
00:21:52,000 --> 00:21:55,000
Ask questions.

288
00:21:55,000 --> 00:21:57,000
So by Lee, there's one of my meditators,

289
00:21:57,000 --> 00:21:59,000
getting into which one fits.

290
00:21:59,000 --> 00:22:01,000
Is that some old Tim?

291
00:22:01,000 --> 00:22:04,000
No, Sabadi is in Thailand, I believe.

292
00:22:04,000 --> 00:22:05,000
I don't think so.

293
00:22:05,000 --> 00:22:06,000
Oh, no, I'm sorry.

294
00:22:06,000 --> 00:22:08,000
I'm, I'm wrong.

295
00:22:08,000 --> 00:22:10,000
New Zealand.

296
00:22:10,000 --> 00:22:13,000
Who's somebody?

297
00:22:13,000 --> 00:22:15,000
Okay, remember that I think I just talked to them

298
00:22:15,000 --> 00:22:16,000
this morning or yesterday.

299
00:22:16,000 --> 00:22:18,000
I thought it was the month.

300
00:22:18,000 --> 00:22:25,000
No, Sabadi, I believe is also a woman hand from New Zealand.

301
00:22:25,000 --> 00:22:26,000
Yeah.

302
00:22:26,000 --> 00:22:27,000
Oh, especially.

303
00:22:27,000 --> 00:22:28,000
I'm sorry.

304
00:22:28,000 --> 00:22:29,000
I'm confusing it.

305
00:22:29,000 --> 00:22:30,000
Also, I didn't talk to someone.

306
00:22:30,000 --> 00:22:31,000
I'm sorry.

307
00:22:31,000 --> 00:22:33,000
I talked to Sashika.

308
00:22:33,000 --> 00:22:35,000
I'm confusing it too.

309
00:22:35,000 --> 00:22:38,000
Also, month is also on our list.

310
00:22:38,000 --> 00:22:39,000
Yes.

311
00:22:39,000 --> 00:22:40,000
I'm sorry.

312
00:22:40,000 --> 00:22:41,000
I'm sorry.

313
00:22:41,000 --> 00:22:42,000
I'm sorry.

314
00:22:42,000 --> 00:22:43,000
I'm sorry.

315
00:22:43,000 --> 00:22:44,000
I'm sorry.

316
00:22:44,000 --> 00:22:46,000
Yesterday was yesterday was, they came to get me early.

317
00:22:46,000 --> 00:22:54,000
Earlier than I expected because we had to be in Toronto by 10.

318
00:22:54,000 --> 00:22:57,000
But, you know, they got me really early.

319
00:22:57,000 --> 00:23:00,000
So I ended up leaving before.

320
00:23:00,000 --> 00:23:01,000
And then I forgot.

321
00:23:01,000 --> 00:23:02,000
Yeah, sorry.

322
00:23:02,000 --> 00:23:09,000
Sashika, I confused you as someone to the Sri Lankan or Sri Lankan national.

323
00:23:09,000 --> 00:23:16,000
Sri Lankan, more people.

324
00:23:16,000 --> 00:23:21,000
I knew who you were.

325
00:23:21,000 --> 00:23:24,000
I just couldn't see.

326
00:23:24,000 --> 00:23:25,000
Yeah.

327
00:23:25,000 --> 00:23:27,000
The names, you know, names in the faces.

328
00:23:27,000 --> 00:23:30,000
Trying to keep track of who so.

329
00:23:30,000 --> 00:23:34,000
General Shanti is Shahan's wife.

330
00:23:34,000 --> 00:23:37,000
So they too will be coming next weekend.

331
00:23:37,000 --> 00:23:40,000
I think I believe in the weekend.

332
00:23:40,000 --> 00:23:41,000
Very nice.

333
00:23:41,000 --> 00:23:42,000
There you go.

334
00:23:42,000 --> 00:23:45,000
I'll just spotlight on my attitude.

335
00:23:45,000 --> 00:23:48,000
Sabadi is Sashika.

336
00:23:48,000 --> 00:23:49,000
Thank you.

337
00:23:49,000 --> 00:23:50,000
I didn't.

338
00:23:50,000 --> 00:23:51,000
In chat.

339
00:23:51,000 --> 00:23:53,000
Government's done.

340
00:23:53,000 --> 00:23:59,000
They think we have a question.

341
00:23:59,000 --> 00:24:04,000
One day, lately, I've been doing meditation for about two to three days a week.

342
00:24:04,000 --> 00:24:05,000
Okay.

343
00:24:05,000 --> 00:24:06,000
Okay.

344
00:24:06,000 --> 00:24:07,000
Okay.

345
00:24:07,000 --> 00:24:08,000
I missed for those plans.

346
00:24:08,000 --> 00:24:10,000
Oh, I'm sorry.

347
00:24:10,000 --> 00:24:12,000
I apologize for this one.

348
00:24:12,000 --> 00:24:14,000
Our monks mostly at least.

349
00:24:14,000 --> 00:24:16,000
So, Topana plus level.

350
00:24:16,000 --> 00:24:22,000
I'm asking because it seems they're keeping eight or more precepts would be really hard otherwise.

351
00:24:22,000 --> 00:24:32,000
Well, it's easier when you're in a monastery when you're off in the forest.

352
00:24:32,000 --> 00:24:36,000
I mean, when you're living on a straw mat in the forest, not that hard to keep eight percent.

353
00:24:36,000 --> 00:24:38,000
It's easier.

354
00:24:38,000 --> 00:24:41,000
But no, you don't have to be.

355
00:24:41,000 --> 00:24:43,000
I mean, that's who you're striving for.

356
00:24:43,000 --> 00:24:47,000
And yeah, if you'd never get to that level, it can be.

357
00:24:47,000 --> 00:24:53,000
I mean, even a sort of panic and still have attachment desires for those things.

358
00:24:53,000 --> 00:25:01,000
Now, that's why you have to keep training.

359
00:25:01,000 --> 00:25:05,000
But don't put monks up on too high a pedestal.

360
00:25:05,000 --> 00:25:10,000
You'll be disappointed otherwise.

361
00:25:10,000 --> 00:25:15,000
And also, they're not supposed to talk about that, right?

362
00:25:15,000 --> 00:25:20,000
So, it's not like a lay person can go up and ask a monk if they're a sotopana.

363
00:25:20,000 --> 00:25:23,000
Yeah, I mean, that's not something that you're really worried about.

364
00:25:23,000 --> 00:25:26,000
Okay, your own practice.

365
00:25:26,000 --> 00:25:28,000
Is it teaching's work if they do?

366
00:25:28,000 --> 00:25:31,000
Someone recently contacted me and pointed like asked me.

367
00:25:31,000 --> 00:25:38,000
And they're afraid to take me as a teacher unless I let them know, you know, I miss them.

368
00:25:38,000 --> 00:25:41,000
I said, we're not allowed to.

369
00:25:41,000 --> 00:25:48,000
But she made a good point that she's had teachers who, you know,

370
00:25:48,000 --> 00:25:52,000
someone, one of her teachers told her that she has to find someone and make sure that they are realized.

371
00:25:52,000 --> 00:25:57,000
Because in Hinduism, they're very keen to tell you how realized they are.

372
00:25:57,000 --> 00:26:00,000
And I think in Mahayana, it's also Mahayana schools.

373
00:26:00,000 --> 00:26:03,000
It's very important to talk about it.

374
00:26:03,000 --> 00:26:08,000
Even Tara Radha's, a lot of people like to talk about how their teachers were like.

375
00:26:08,000 --> 00:26:11,000
But it's really not proper.

376
00:26:11,000 --> 00:26:14,000
It's not the way we should do anything.

377
00:26:14,000 --> 00:26:16,000
What will we should be focused on?

378
00:26:16,000 --> 00:26:17,000
We should be focused on the teaching.

379
00:26:17,000 --> 00:26:18,000
Do they work?

380
00:26:18,000 --> 00:26:22,000
Because that question you have to and you can answer yourself.

381
00:26:22,000 --> 00:26:24,000
That's a much more pertinent question.

382
00:26:24,000 --> 00:26:27,000
Do the teachings work?

383
00:26:27,000 --> 00:26:31,000
If you ask that question, then you know what you have to do.

384
00:26:31,000 --> 00:26:34,000
I tell you, yes, my teacher's in our hand, go practice with him.

385
00:26:34,000 --> 00:26:39,000
What does that really do?

386
00:26:39,000 --> 00:26:43,000
And I really teach every person, says that about the teacher.

387
00:26:43,000 --> 00:26:51,000
So many people get it wrong.

388
00:26:51,000 --> 00:26:55,000
But lately, I've been doing meditation for about two to three days a week.

389
00:26:55,000 --> 00:27:03,000
How many days of constant, consistent daily meditation should I do before I set up a formal meditation course with Bhante?

390
00:27:03,000 --> 00:27:06,000
Seven days.

391
00:27:06,000 --> 00:27:09,000
We've done it for a week, then we can meet.

392
00:27:09,000 --> 00:27:12,000
Probably not.

393
00:27:12,000 --> 00:27:14,000
Yeah.

394
00:27:14,000 --> 00:27:17,000
We have to wait till January when I had more.

395
00:27:17,000 --> 00:27:26,000
It's looking good for adding 28 more slots for January, which is awesome.

396
00:27:26,000 --> 00:27:34,000
And then I'm meeting with 56 people a week, 56 different people doing courses.

397
00:27:34,000 --> 00:27:37,000
I mean, they're not hardcore courses.

398
00:27:37,000 --> 00:27:44,000
It's not as great as awesome as people doing and coming and doing intensive course because that's tough.

399
00:27:44,000 --> 00:27:47,000
On the other hand, it's great.

400
00:27:47,000 --> 00:27:54,000
Well, the other side of it is, you know, doesn't happen sometimes that people go and do an intensive course and everything is great while they're there.

401
00:27:54,000 --> 00:27:58,000
But then they go back to their life and, you know, they don't continue with it.

402
00:27:58,000 --> 00:28:02,000
Here are people that are finding time to meditate consistently in their daily life.

403
00:28:02,000 --> 00:28:05,000
So maybe.

404
00:28:05,000 --> 00:28:07,000
No, I don't think so.

405
00:28:07,000 --> 00:28:14,000
I mean, we consider, we believe that many people, the course changes.

406
00:28:14,000 --> 00:28:17,000
I'm not sure that this course will be as life changing.

407
00:28:17,000 --> 00:28:22,000
It won't be because especially we're not going to the end.

408
00:28:22,000 --> 00:28:25,000
It won't be as life changing, I don't think.

409
00:28:25,000 --> 00:28:33,000
And those people decide to take the online course to another level and come and do in-person meditation as well.

410
00:28:33,000 --> 00:28:35,000
Yeah, that's a good problem for that.

411
00:28:35,000 --> 00:28:36,000
Yeah.

412
00:28:36,000 --> 00:28:45,000
Not a complete problem because it's a big wake up call to come and realize, oh, you have to do eight or ten or twelve hours a day.

413
00:28:45,000 --> 00:28:55,000
But they've had a little bit of, you know, experience with having the reporting session talking to you knowing what the.

414
00:28:55,000 --> 00:28:58,000
You know, how the steps go and.

415
00:28:58,000 --> 00:29:06,000
Yeah, I mean, this was kind of the point was a lot of people been meditating for a long time without going through the steps.

416
00:29:06,000 --> 00:29:07,000
Yes.

417
00:29:07,000 --> 00:29:14,000
Because then they think, well, this is it. It's just kind of a simple, you know, I think another little bit.

418
00:29:14,000 --> 00:29:18,000
It's a little mental game for mind training.

419
00:29:18,000 --> 00:29:20,000
I'm bald.

420
00:29:20,000 --> 00:29:40,000
I'll be something new.

421
00:29:40,000 --> 00:29:54,000
Sometimes I thought wondering how long I've been with, sorry, sometimes I thought wondering how long I've been meditating and how long I've left comes to me and it causes me to be restless and makes it harder to continue.

422
00:29:54,000 --> 00:29:59,000
Is there anything I can do beyond noting it and letting it pass?

423
00:29:59,000 --> 00:30:10,000
Well, you should note the aspect of it. It is, there's a, there's a conglomerate of things. There's a thought and then there's the reason for the thought.

424
00:30:10,000 --> 00:30:14,000
Usually it's nagging because you're bored.

425
00:30:14,000 --> 00:30:19,000
You're adverse to meditating longer.

426
00:30:19,000 --> 00:30:24,000
You want to do something else more that kind of thing.

427
00:30:24,000 --> 00:30:30,000
It's often some reason for it. If it's not, it's just a simple thought and it would be innocuous.

428
00:30:30,000 --> 00:30:32,000
Probably wouldn't be asking me about it.

429
00:30:32,000 --> 00:30:40,000
Most of these feelings come up when they are persistent and they're persistent because we really want to stop.

430
00:30:40,000 --> 00:30:47,000
We don't want to continue with it.

431
00:30:47,000 --> 00:30:49,000
I wouldn't say it causes you restless.

432
00:30:49,000 --> 00:30:59,000
I mean, I would suggest that probably the thought comes up if wondering comes up because you're already adverse to meditating.

433
00:30:59,000 --> 00:31:05,000
So you have to find that emotion, disliking, or wanting, or whatever it is, boredom.

434
00:31:05,000 --> 00:31:07,000
And note it as well.

435
00:31:07,000 --> 00:31:10,000
The thought as well, the emotion as well.

436
00:31:10,000 --> 00:31:24,000
And the experiences, if you feel pain or if you feel physical discomfort that's causing it, can you look at this one?

437
00:31:24,000 --> 00:31:30,000
A friend of mine said Buddhists believe in monism or that we all are one.

438
00:31:30,000 --> 00:31:34,000
He said the Dalai Lama said this as well as an academic.

439
00:31:34,000 --> 00:31:43,000
I said it might be a Mahayana belief, but I have not come across this in Theravada, although all beings are composed of the same thing, aggregates.

440
00:31:43,000 --> 00:31:47,000
I said it as view. Have you heard this?

441
00:31:47,000 --> 00:31:53,000
Yeah, I mean, many Mahayana teachers seem to believe that it's very Hindu concept.

442
00:31:53,000 --> 00:31:57,000
We are one.

443
00:31:57,000 --> 00:32:02,000
It's not a very Buddhist concept.

444
00:32:02,000 --> 00:32:09,000
I don't think, given the Mahayana teacher's monism,

445
00:32:09,000 --> 00:32:20,000
Advaita, they would call it Hinduism. Advaita means nondualism.

446
00:32:20,000 --> 00:32:33,000
No, it's nondualism, that's something else. No, no, no, Hinduism, there is the sense of oneness and everything.

447
00:32:33,000 --> 00:32:38,000
Very much, we are, I am God, you are God.

448
00:32:38,000 --> 00:32:43,000
The difference is, I know it, you don't. That's the kind of dignity.

449
00:32:43,000 --> 00:32:45,000
We don't exist.

450
00:32:45,000 --> 00:32:55,000
We're not all one. We're not all God. But yes, you, I think you did a service to Buddhism by stating the truth.

451
00:32:55,000 --> 00:33:01,000
We are made up the same stuff. We're made up one kind of stuff.

452
00:33:01,000 --> 00:33:03,000
Experience.

453
00:33:03,000 --> 00:33:09,000
There's no room for that one thing to exist. It's not a part of framework of reality.

454
00:33:09,000 --> 00:33:15,000
Reality is experiences, arising and ceasing. Each experience is one.

455
00:33:15,000 --> 00:33:19,000
So two different experiences aren't even one anymore.

456
00:33:19,000 --> 00:33:24,000
Just a view is just, you're right, it's just a view, it's just a view.

457
00:33:24,000 --> 00:33:29,000
It's meaningless in terms of reality. It's nothing about ultimate reality.

458
00:33:29,000 --> 00:33:38,000
We all cut up.

459
00:33:38,000 --> 00:33:41,000
I think we're all cut up on questions, Dante.

460
00:33:41,000 --> 00:33:43,000
All right.

461
00:33:43,000 --> 00:33:48,000
That's all then. If you have more questions,

462
00:33:48,000 --> 00:33:50,000
connect tomorrow.

463
00:33:50,000 --> 00:33:54,000
Tomorrow, probably have a demo product.

464
00:33:54,000 --> 00:33:57,000
I know. No, tomorrow I have to skip again.

465
00:33:57,000 --> 00:34:02,000
Tomorrow, there's an interfaith coffee house at 8 p.m.

466
00:34:02,000 --> 00:34:06,000
I promised the Muslim students association with income.

467
00:34:06,000 --> 00:34:11,000
So we'll see how that works. I think I have five to seven minutes to present.

468
00:34:11,000 --> 00:34:16,000
We're shorter and shorter. On Saturday, yesterday, I had to present in 20 minutes, and I thought that was short.

469
00:34:16,000 --> 00:34:21,000
I'm going to say in five to seven minutes.

470
00:34:21,000 --> 00:34:24,000
Well, that sounds important.

471
00:34:24,000 --> 00:34:29,000
It's the biggest, and let me skip two demo patterns in a row.

472
00:34:29,000 --> 00:34:33,000
And this is the good when you've been telling us that this is one of the most,

473
00:34:33,000 --> 00:34:35,000
one of the most well-known demo patterns.

474
00:34:35,000 --> 00:34:38,000
It's a good off-toise now.

475
00:34:38,000 --> 00:34:42,000
It's interesting. It's not actually that deep in the teachings or not actually that profound,

476
00:34:42,000 --> 00:34:44,000
but it's interesting story.

477
00:34:44,000 --> 00:34:46,000
And it's a well-known story.

478
00:34:46,000 --> 00:34:49,000
So we'll shoot for Wednesday.

479
00:34:49,000 --> 00:34:51,000
We will. Sounds good.

480
00:34:51,000 --> 00:34:53,000
Thank you, Dante.

481
00:34:53,000 --> 00:34:56,000
Thanks everyone for tuning in. Have a good night.

