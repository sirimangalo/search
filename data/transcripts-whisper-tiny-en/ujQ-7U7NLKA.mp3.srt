1
00:00:00,000 --> 00:00:03,000
Next question comes from Stilton.

2
00:00:03,000 --> 00:00:10,000
If everyone's mind is part of a wider consciousness and there is no you, me, or I,

3
00:00:10,000 --> 00:00:14,000
then what is the part of an individual that supposedly reincarnates

4
00:00:14,000 --> 00:00:19,000
through the cycle of rebirth and suffering?

5
00:00:19,000 --> 00:00:25,000
I don't think I ever said that everyone's mind is part of a wider consciousness.

6
00:00:25,000 --> 00:00:27,000
I hope I didn't.

7
00:00:27,000 --> 00:00:32,000
I think that is true of Hinduism.

8
00:00:32,000 --> 00:00:37,000
That in Hinduism there is this idea of the Brahman and the Atman.

9
00:00:37,000 --> 00:00:41,000
And the Brahman is the greater, the big self, the Atman is the little self.

10
00:00:41,000 --> 00:00:43,000
And the little self is part of the big self.

11
00:00:43,000 --> 00:00:46,000
We are one and so on.

12
00:00:46,000 --> 00:00:56,000
Often get this because it's often understood that Buddhism teaches we are one.

13
00:00:56,000 --> 00:01:02,000
I remember reading the dictionary definition of Nirvana when I was in high school.

14
00:01:02,000 --> 00:01:13,000
And it said the dissolving or the absolving of the whole of the one,

15
00:01:13,000 --> 00:01:17,000
the part into the whole or something like that, the self into the whole

16
00:01:17,000 --> 00:01:22,000
when you no longer have a sense of being separate from everyone else.

17
00:01:22,000 --> 00:01:31,000
Buddhism or the Buddhism that I follow says that we are separate.

18
00:01:31,000 --> 00:01:34,000
There is a stream of consciousness in every being.

19
00:01:34,000 --> 00:01:42,000
Just as there is a stream of growth in every plant or every tree.

20
00:01:42,000 --> 00:01:47,000
Every human being is an individual entity, an individual stream.

21
00:01:47,000 --> 00:01:54,000
Just like a river, there is an individual stream, a tree is an individual growth and so on.

22
00:01:54,000 --> 00:01:59,000
Just as the body is individual, my body is not the same body as anyone else's body.

23
00:01:59,000 --> 00:02:00,000
That is clear.

24
00:02:00,000 --> 00:02:06,000
There is no, you can't really philosophize your way out of that.

25
00:02:06,000 --> 00:02:11,000
My body is different from other people's body.

26
00:02:11,000 --> 00:02:21,000
The mind is a stream, so the mind here or the mind in this being that creates this part of physical

27
00:02:21,000 --> 00:02:28,000
physical reality is separate from other streams of consciousness.

28
00:02:28,000 --> 00:02:30,000
So that should be clear.

29
00:02:30,000 --> 00:02:43,000
The idea of non-self in Buddhism, of they are not being a self, is that no part of the stream is the self.

30
00:02:43,000 --> 00:02:48,000
A soul is me because it is only a part.

31
00:02:48,000 --> 00:02:51,000
It is like no part of the body is the whole.

32
00:02:51,000 --> 00:02:53,000
There is no body.

33
00:02:53,000 --> 00:02:56,000
Every part of the body can be taken off.

34
00:02:56,000 --> 00:03:04,000
There is no point that you could point to and say, that is the eternal soul or that is an entity in and of itself.

35
00:03:04,000 --> 00:03:06,000
It depends on each other.

36
00:03:06,000 --> 00:03:09,000
When it is put together, it becomes a self.

37
00:03:09,000 --> 00:03:16,000
So the parts of the mind, when they come together, they create this illusion of a self.

38
00:03:16,000 --> 00:03:24,000
So this stream of consciousness, because it is moving so quickly and because it is in the same place, it is a rising dependent on each other.

39
00:03:24,000 --> 00:03:26,000
It seems like it is one.

40
00:03:26,000 --> 00:03:29,000
When you practice meditation, you are able to break that down.

41
00:03:29,000 --> 00:03:34,000
You can see that there are thoughts arising, there are judgments arising, there are emotions arising,

42
00:03:34,000 --> 00:03:38,000
there are feelings arising, and there are consciousness arising.

43
00:03:38,000 --> 00:03:41,000
This consciousness and that consciousness and they arise and they cease.

44
00:03:41,000 --> 00:03:45,000
When you are able to break that apart, you lose that sense of self.

45
00:03:45,000 --> 00:04:00,000
That in this entity, there is no single thing that is permanent, lasting, stable.

46
00:04:00,000 --> 00:04:02,000
It is all arising and ceasing.

47
00:04:02,000 --> 00:04:08,000
And the other corollary to that is that there is no one in control.

48
00:04:08,000 --> 00:04:13,000
Because there is no one thing that is the eternal self, there is nothing in control.

49
00:04:13,000 --> 00:04:24,000
If this phenomenon, say this mind state acts in a certain way, then all it does is give rise to another mind state and disappears of itself.

50
00:04:24,000 --> 00:04:26,000
And another mind state arises.

51
00:04:26,000 --> 00:04:30,000
When I want to do something, that is because there is a wanting arise.

52
00:04:30,000 --> 00:04:42,000
That is because there is the causes which come together and give rise to a wanting based on previous causes, previous wanting, previous memories of previous experience and previous pleasure.

53
00:04:42,000 --> 00:04:50,000
That wanting leads to the intention to do something about it, the intention to get something.

54
00:04:50,000 --> 00:04:57,000
That intention leads me to do something, that doing something then leads to more and more state.

55
00:04:57,000 --> 00:05:07,000
So it works in terms of cause and effect and there is no, we can't sit here and arrange all of these things and say,

56
00:05:07,000 --> 00:05:13,000
okay, now this cause and now that cause and now this cause and bring them all together and force things to be the way we want.

57
00:05:13,000 --> 00:05:17,000
We are a fluid dynamic being.

58
00:05:17,000 --> 00:05:19,000
We come to see that we are not in control.

59
00:05:19,000 --> 00:05:22,000
When pain arises in the body, we can't turn it off.

60
00:05:22,000 --> 00:05:30,000
When sound arises, when there is loud noise coming at us, we can't make it stop.

61
00:05:30,000 --> 00:05:37,000
When someone is doing something we don't like, we can't force them to be quiet.

62
00:05:37,000 --> 00:05:39,000
It goes in terms of cause and effect.

63
00:05:39,000 --> 00:05:46,000
If we don't build the causes or create the causes, we can't expect to have the effect.

64
00:05:46,000 --> 00:05:49,000
You can't force yourself not to want the things that you want.

65
00:05:49,000 --> 00:05:51,000
You can't force yourself not to get angry.

66
00:05:51,000 --> 00:05:58,000
All you can do is train yourself to see things in a different way.

67
00:05:58,000 --> 00:06:02,000
So that actually, instead of going in this direction, you start to go in this direction.

68
00:06:02,000 --> 00:06:06,000
Or you're slowly doing a bout face and you're able to go in the opposite direction.

69
00:06:06,000 --> 00:06:10,000
You're able to see things simply for what they are rather than becoming attached to them.

70
00:06:10,000 --> 00:06:14,000
You're creating the causes for future effects.

71
00:06:14,000 --> 00:06:17,000
You're changing the stream.

72
00:06:17,000 --> 00:06:19,000
You're turning the stream around.

73
00:06:19,000 --> 00:06:22,000
That's what is meant by non-cell.

74
00:06:22,000 --> 00:06:29,000
As to what reincarnates, nothing reincarnates in this point.

75
00:06:29,000 --> 00:06:33,000
There is no body to reincarnate into.

76
00:06:33,000 --> 00:06:42,000
All that happens is the mind continues to arise and sees, changes and this wave,

77
00:06:42,000 --> 00:06:45,000
which we're being propelled through called life.

78
00:06:45,000 --> 00:06:48,000
The physical body is going to crash.

79
00:06:48,000 --> 00:06:49,000
It's going to fall apart.

80
00:06:49,000 --> 00:06:51,000
That's the nature of the body and that is verifiable.

81
00:06:51,000 --> 00:06:54,000
But the mind is just going to create another body.

82
00:06:54,000 --> 00:06:57,000
The mind is working in terms of these waves right now.

83
00:06:57,000 --> 00:07:03,000
There are states of being where we don't have these waves of building up and then death.

84
00:07:03,000 --> 00:07:05,000
Building up and then death.

85
00:07:05,000 --> 00:07:11,000
There's simply the slow change, as I've mentioned before, talking about heaven.

86
00:07:11,000 --> 00:07:16,000
There is the moving up and down based on our actions.

87
00:07:16,000 --> 00:07:23,000
That's really all we're doing, we become more and more coarse based on our actions in this life.

88
00:07:23,000 --> 00:07:25,000
Because of the coarseness, we build it up.

89
00:07:25,000 --> 00:07:31,000
We build it up and we build it up and at the moment of death, it explodes into another wave.

90
00:07:31,000 --> 00:07:33,000
We start fresh all over again.

91
00:07:33,000 --> 00:07:39,000
If we're able to give up these coarse thoughts, course ideas, this clinging that is in our minds,

92
00:07:39,000 --> 00:07:41,000
then we're able to fly free.

93
00:07:41,000 --> 00:07:47,000
At the moment of death, there's no crashing down, there's a freedom of flying up.

94
00:07:47,000 --> 00:07:51,000
We can go on to heaven or even not have to be born again.

95
00:07:51,000 --> 00:07:56,000
But in brief, all that there is is the arising and ceasing of phenomenon.

96
00:07:56,000 --> 00:08:03,000
There is no evidence that it's added in that.

97
00:08:03,000 --> 00:08:13,000
By practicing meditation, we're able to let go of this idea of self, the idea that death

98
00:08:13,000 --> 00:08:18,000
stops and something else starts and see that actually it's just a continuation.

99
00:08:18,000 --> 00:08:21,000
You'll see that that's what happens when you die.

100
00:08:21,000 --> 00:08:24,000
At the moment of death, the mind continues on.

101
00:08:24,000 --> 00:08:29,000
The mind clings to something, keeps going, starts all over again.

102
00:08:29,000 --> 00:08:36,000
It finds itself in based on the state of mind that we've built up to that moment.

103
00:08:36,000 --> 00:08:42,000
This is why meditation is very important because we aren't a being that is reincarnated.

104
00:08:42,000 --> 00:08:50,000
We're just a stream of consciousness and we don't take who we are now into the next life.

105
00:08:50,000 --> 00:08:58,000
It goes from that last moment, whatever we are at that last moment goes on to the next life.

106
00:08:58,000 --> 00:09:01,000
With the qualifier, that's dependent on everything that we do in this life.

107
00:09:01,000 --> 00:09:05,000
If you kill a lot of people at the last moment, what's going to happen is all of that's going to come back to you.

108
00:09:05,000 --> 00:09:08,000
To haunt you, you're going to think about all of that.

109
00:09:08,000 --> 00:09:09,000
You're going to feel guilty.

110
00:09:09,000 --> 00:09:12,000
You're going to be worried, scared.

111
00:09:12,000 --> 00:09:21,000
You're going to feel the oppression of those mind states that you've created and it's going to drag you down.

112
00:09:21,000 --> 00:09:30,000
No one consciousness, there are separate streams of consciousness and no reincarnation.

113
00:09:30,000 --> 00:09:38,000
There is only rebirth, rebirth again and again and again from moment to moment and at the moment of physical death, that continues.

114
00:09:38,000 --> 00:09:53,000
Thanks for the question.

