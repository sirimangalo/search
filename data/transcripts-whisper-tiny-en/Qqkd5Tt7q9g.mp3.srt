1
00:00:00,000 --> 00:00:29,760
Okay, good evening, everyone.

2
00:00:29,760 --> 00:00:44,720
Welcome to our weekly question and answer session.

3
00:00:44,720 --> 00:00:50,000
We have a bunch of questions, bunch of good questions.

4
00:00:50,000 --> 00:00:53,840
Some of them, I think, might have fairly quick answers.

5
00:00:53,840 --> 00:01:04,320
Yeah, there is. Well, we'll see. We'll get right into it.

6
00:01:04,320 --> 00:01:08,400
What is a good way of preparing oneself for death?

7
00:01:08,400 --> 00:01:22,640
What practices should one undertake?

8
00:01:22,640 --> 00:01:30,320
I guess the question here is kind of concerned with that moment of death, right?

9
00:01:30,320 --> 00:01:35,840
I mean, any question where you ask me, what is a good practice?

10
00:01:35,840 --> 00:01:39,360
What practices should one undertake?

11
00:01:39,360 --> 00:01:44,000
I'm going to always answer one mindfulness, of course.

12
00:01:44,000 --> 00:01:48,000
So preparing for oneself for death is really no different.

13
00:01:48,000 --> 00:01:54,960
But perhaps you're concerned that there is something special about death.

14
00:01:54,960 --> 00:02:01,920
And so, what do you do to prepare yourself for something very specific?

15
00:02:01,920 --> 00:02:06,080
Or are there any nuances that you should be concerned with?

16
00:02:06,080 --> 00:02:09,360
Hey, are you here to stay with us?

17
00:02:09,360 --> 00:02:13,440
Okay, very familiar.

18
00:02:13,440 --> 00:02:17,840
Okay, welcome.

19
00:02:17,840 --> 00:02:29,440
But death is...

20
00:02:29,440 --> 00:02:32,320
I mean, this is the important thing in Buddhism to understand about death.

21
00:02:32,320 --> 00:02:35,520
Death is only another moment in time.

22
00:02:35,520 --> 00:02:39,840
Yes, there are some very specific things that happen.

23
00:02:39,840 --> 00:02:44,800
That are different from things that happen during life.

24
00:02:44,800 --> 00:02:48,320
But there are still just things that happen.

25
00:02:48,320 --> 00:02:53,680
And the important part of this very vague things that happen

26
00:02:53,680 --> 00:02:56,480
is the reactions to those things that happen.

27
00:02:56,480 --> 00:03:02,080
Whatever happens during death, mindfulness is still going to be the most important.

28
00:03:02,080 --> 00:03:07,360
That being said, life isn't just meditation.

29
00:03:07,360 --> 00:03:15,520
And so, while it's still true that the most important thing in mind is going to be mindfulness,

30
00:03:15,520 --> 00:03:22,960
there are intellectual sorts of exercises that help put you in the right frame of mind,

31
00:03:22,960 --> 00:03:25,680
like reflecting on death.

32
00:03:25,680 --> 00:03:28,320
Remembering that you're going to die all the time, you know?

33
00:03:28,320 --> 00:03:31,760
Do that daily, the Buddha said.

34
00:03:31,760 --> 00:03:35,760
And you should be always aware of the fact that we're going to die.

35
00:03:35,760 --> 00:03:40,880
Otherwise, you'll be caught off guard and you're much more likely to be unmindful.

36
00:03:40,880 --> 00:03:41,880
What do I do?

37
00:03:41,880 --> 00:03:42,880
Do I do it?

38
00:03:42,880 --> 00:03:52,800
It's happening because you weren't familiar or ready for it, right?

39
00:03:52,800 --> 00:03:58,320
The other things you can do is do lots of good things in this life.

40
00:03:58,320 --> 00:03:59,960
So you won't be afraid of death.

41
00:03:59,960 --> 00:04:02,320
What might happen to me while you're not afraid of that?

42
00:04:02,320 --> 00:04:04,560
Because you know you're a good person.

43
00:04:04,560 --> 00:04:12,320
You know you've developed a good thing, but the best thing is still mindfulness.

44
00:04:12,320 --> 00:04:19,800
Another thing about that is the Mahasi Sayada wrote a commentary on the Pura-Bhadasut.

45
00:04:19,800 --> 00:04:26,320
So I recommend reading that if you're interested.

46
00:04:26,320 --> 00:04:48,640
It's said the English is not great, but still worth reading in my mind.

47
00:04:48,640 --> 00:05:00,120
This is not working anymore.

48
00:05:00,120 --> 00:05:04,680
Can formal meditation be done while not sitting, lying, or standing still?

49
00:05:04,680 --> 00:05:09,840
Could brushing teeth be a formal practice?

50
00:05:09,840 --> 00:05:18,840
And it's kind of where the line between formal and informal becomes blurred.

51
00:05:18,840 --> 00:05:21,360
I think it's a good question really.

52
00:05:21,360 --> 00:05:26,680
And in fact I think it's good to think of those things like that, eating.

53
00:05:26,680 --> 00:05:31,880
You can consider the act of eating to be a formal meditation practice where you particularly

54
00:05:31,880 --> 00:05:35,760
have a format to it.

55
00:05:35,760 --> 00:05:47,160
Having said probably not worth it, I would say to have like a format I'm going to eat

56
00:05:47,160 --> 00:05:51,640
each bite exactly the same.

57
00:05:51,640 --> 00:05:58,000
Probably more valuable for you to observe how you eat normally and try and be mindful

58
00:05:58,000 --> 00:06:00,400
of the different movements.

59
00:06:00,400 --> 00:06:05,720
So the difference between formal and informal is generally like do you have a format?

60
00:06:05,720 --> 00:06:10,680
Is it going to be formal formulaeic or not?

61
00:06:10,680 --> 00:06:16,400
Eating need not to be formulaeic, of course the mindfulness.

62
00:06:16,400 --> 00:06:20,880
Thinking of it as a formal practice is in the sense of yes, this eating will be an eating

63
00:06:20,880 --> 00:06:21,880
meditation.

64
00:06:21,880 --> 00:06:26,680
I will be mindful of everything, but it doesn't mean you have to scoop, raise, bite,

65
00:06:26,680 --> 00:06:30,480
scoop, raise, bite like as though we're a formal walking meditation.

66
00:06:30,480 --> 00:06:33,120
So I think that's the difference.

67
00:06:33,120 --> 00:06:39,040
Using the teeth as well, yeah maybe you do have a formula for brushing your teeth.

68
00:06:39,040 --> 00:06:45,080
Again blurring the lines, but most important is that anything can be a practice of mindfulness

69
00:06:45,080 --> 00:06:52,000
throughout the day.

70
00:06:52,000 --> 00:06:57,560
So our instructor course, do we officially authorize lay people to teach, to personally

71
00:06:57,560 --> 00:07:03,840
place value on the idea of a lineage of authorized teachers?

72
00:07:03,840 --> 00:07:07,680
So the answer is to your questions, no, no, no.

73
00:07:07,680 --> 00:07:12,120
Now I don't believe in authorizing people to teach.

74
00:07:12,120 --> 00:07:17,440
I don't believe in a lineage of authorized teachers.

75
00:07:17,440 --> 00:07:22,400
My teacher didn't either, and I've seen groups that do that are very big on handing out

76
00:07:22,400 --> 00:07:24,440
certificates.

77
00:07:24,440 --> 00:07:32,320
I don't have a certificate, but I didn't spend a year sitting at my teacher's feet,

78
00:07:32,320 --> 00:07:37,120
and I think I got quite knowledgeable about his meditation practice.

79
00:07:37,120 --> 00:07:44,160
I mean he's told other people after doing the foundation course, or maybe the advanced course,

80
00:07:44,160 --> 00:07:47,480
that they should go and teach people, yeah, spend the teaching, it's good, however

81
00:07:47,480 --> 00:07:50,760
you learn to teach, teach others in that way.

82
00:07:50,760 --> 00:07:59,760
It's important, I mean when you authorize people, wow, ego becomes a real issue, someone

83
00:07:59,760 --> 00:08:09,120
thinks of themselves as a teacher, lots and lots of problems there.

84
00:08:09,120 --> 00:08:19,000
So difficult to discern rising and falling, breath gets too shallow, and the breath is never

85
00:08:19,000 --> 00:08:20,000
too shallow to know.

86
00:08:20,000 --> 00:08:27,360
You can just not rise, fall, rise, fall, but it's also a matter of focus, it takes some time

87
00:08:27,360 --> 00:08:32,960
for, if you've never looked at this stomach, the abdomen, it takes some time to get your

88
00:08:32,960 --> 00:08:40,400
body and your mind in a position where it can be a valuable or viable meditation object.

89
00:08:40,400 --> 00:08:46,920
When you feel quiet, calm, alert and bright, as you say, equanimous, all of that can

90
00:08:46,920 --> 00:08:50,880
be noted, and that's what I would, the recommendation I would give.

91
00:08:50,880 --> 00:08:56,320
When it feels like noting is unneeded, depends what you mean by unneeded, I needed to make

92
00:08:56,320 --> 00:09:06,760
you stay calm, no, I needed to gain insight, yes, needed to gain, sorry, it's unneeded

93
00:09:06,760 --> 00:09:14,000
to stay calm, yes, that's true, unneeded to gain insight, no, needed to gain insight.

94
00:09:14,000 --> 00:09:17,840
So being calm and gaining insight are two very different things, just because you're sitting

95
00:09:17,840 --> 00:09:24,040
there equanimous, truly have no value unless you're gaining insight, which comes from

96
00:09:24,040 --> 00:09:28,720
objectivity and you need to be objective about your equanimity, for example, saying

97
00:09:28,720 --> 00:09:34,880
to yourself calm, calm or neutral, neutral feeling, so on.

98
00:09:34,880 --> 00:09:39,560
That's a common problem, people feel like the noting is just stopped, there's nothing

99
00:09:39,560 --> 00:09:45,520
to note, no, you should note all those things that you're talking about because it sounds

100
00:09:45,520 --> 00:09:52,320
like there's actually a lot going on, if you like it, you're not liking liking, but I do

101
00:09:52,320 --> 00:10:01,640
recommend if you can to try and find the stomach, it's a very good general object of meditation.

102
00:10:01,640 --> 00:10:06,680
If we are reborn based on the karma, how can animals be reborn as people if they kill

103
00:10:06,680 --> 00:10:10,120
to live?

104
00:10:10,120 --> 00:10:17,920
The answer, as you may suspect, is that they have a very hard time doing so, this isn't

105
00:10:17,920 --> 00:10:26,520
a fair game where there are somebody saying, oh, I know you have to live, so you have

106
00:10:26,520 --> 00:10:34,160
to kill to live, so that's okay, you can still become human, no, there's no God, this

107
00:10:34,160 --> 00:10:41,440
is not a fair game, if you're in that position, it's reality, if you're killing, you're

108
00:10:41,440 --> 00:10:49,800
not going to cultivate a good karma, it's just the way of it, so the path for a carnivore

109
00:10:49,800 --> 00:11:05,560
to become human is very dismal, very difficult, of course, they know they got themselves

110
00:11:05,560 --> 00:11:12,280
in that position, it's all very fair, actually, how you get to the position of being a

111
00:11:12,280 --> 00:11:20,320
carnivore, they've got to talk about that as well.

112
00:11:20,320 --> 00:11:26,920
When you're thinking and you're drawn into it, and then you don't, of course, able to

113
00:11:26,920 --> 00:11:31,840
note when you're thinking because you're lost and thought, once you come back, note that

114
00:11:31,840 --> 00:11:39,160
you were thinking by saying thinking, mindfulness or sati is just remembering that it

115
00:11:39,160 --> 00:11:45,040
is what it is, reminding yourself, so what happens after you come back is maybe you react

116
00:11:45,040 --> 00:11:48,680
to it saying, oh, I was thinking a lot or maybe you continue on with the thinking or

117
00:11:48,680 --> 00:11:54,160
however, we're just replacing that with a reminder, it was only thinking, so it's always

118
00:11:54,160 --> 00:11:59,640
going to be technically after the fact, that's not a problem, mindfulness is about reminding

119
00:11:59,640 --> 00:12:06,200
yourself, so just say to yourself thinking, thinking, or even just knowing, knowing when

120
00:12:06,200 --> 00:12:10,200
you have this consciousness I was thinking, you can say, knowing, knowing, and focus on

121
00:12:10,200 --> 00:12:16,240
that thought.

122
00:12:16,240 --> 00:12:21,720
Modern science tells us that insufficient sleep can lead to increases in risk for serious

123
00:12:21,720 --> 00:12:27,440
health conditions, including Alzheimer's, and traditional Buddhist advice seems to encourage

124
00:12:27,440 --> 00:12:31,840
sleep deprivation, what's a sensible thing to do?

125
00:12:31,840 --> 00:12:38,560
The sensible thing to do is to sleep as much as healthy, as best for you, which is really

126
00:12:38,560 --> 00:12:45,120
just not saying anything I suppose, but it's important because the answer there is that

127
00:12:45,120 --> 00:12:52,280
a Buddhist, an intensive Buddhist meditator is nothing like the people in those studies

128
00:12:52,280 --> 00:12:57,640
that tell you that it leads to Alzheimer's and et cetera, et cetera.

129
00:12:57,640 --> 00:13:04,120
Those people are people who need very little sleep if any, they're in a position where

130
00:13:04,120 --> 00:13:10,760
their mind is perfect and body, are perfectly capable of regenerating themselves without

131
00:13:10,760 --> 00:13:17,040
any sleep, sounds tough or even impossible, it's very tough to get to that position.

132
00:13:17,040 --> 00:13:26,720
Now we try, during our courses, to get people to very refined state, but it would only

133
00:13:26,720 --> 00:13:33,720
be in such a refined state that you would ever think in useful, valuable proper to go

134
00:13:33,720 --> 00:13:35,600
without sleep.

135
00:13:35,600 --> 00:13:39,520
I think, I think that's a valid answer.

136
00:13:39,520 --> 00:13:47,400
I would like to see a intensive meditator be tested for some of those problems, and I think

137
00:13:47,400 --> 00:14:11,640
the results might be revelatory.

138
00:14:11,640 --> 00:14:17,760
When we walk during the day, is it best to try and walk slowly or should you just keep to ordinary

139
00:14:17,760 --> 00:14:18,760
movements?

140
00:14:18,760 --> 00:14:26,760
I mean, you just have to be mindful, so the technical aspects are not the most important

141
00:14:26,760 --> 00:14:33,200
thing, but practically speaking, yeah, I think it would be best just to say left, right,

142
00:14:33,200 --> 00:14:40,560
left, right, left, right, left, rather than actually trying to break it down.

143
00:14:40,560 --> 00:14:49,000
It's not wrong, if you were to go very slowly, if you were to try to make everything

144
00:14:49,000 --> 00:14:56,080
into mindfulness, there's something, especially for new meditators.

145
00:14:56,080 --> 00:15:00,720
But just in general, there's something useful about trying to be mindful during natural

146
00:15:00,720 --> 00:15:06,520
movements, during the movements that are familiar to us, so it's actually potentially

147
00:15:06,520 --> 00:15:10,360
a good thing to move at normal speed.

148
00:15:10,360 --> 00:15:14,400
Instead of trying to say, okay, I have to go out, I have to go to the washroom, lifting,

149
00:15:14,400 --> 00:15:19,880
placing, lifting, placing or so on.

150
00:15:19,880 --> 00:15:25,000
It's actually in some ways more beneficial to make it a natural movement, because it helps

151
00:15:25,000 --> 00:15:31,520
you be mindful in what's familiar to you, it helps you learn about the way your mind works.

152
00:15:31,520 --> 00:15:37,840
And either way, you know, there are different styles and I think both are valid, and depending

153
00:15:37,840 --> 00:15:43,600
on the individual, really, one might be more valuable than the other, but I don't think

154
00:15:43,600 --> 00:15:50,520
either is really the most important thing to concern yourself with.

155
00:15:50,520 --> 00:15:57,280
According to brain scans, this is always a problem thing to say, because it's always

156
00:15:57,280 --> 00:16:06,120
very vague in general, when you say, according to brain scans, X, Y, Z, to convince me

157
00:16:06,120 --> 00:16:07,800
you have to be very specific.

158
00:16:07,800 --> 00:16:14,600
And this kind of statement is made about things that have certainly never, ever been verified

159
00:16:14,600 --> 00:16:15,600
through brain scans.

160
00:16:15,600 --> 00:16:22,320
It's often used in proper ways, you know, to say things, but this statement is problematic.

161
00:16:22,320 --> 00:16:26,320
You say, according to brain scans, there is an area of the brain in charge of consciousness.

162
00:16:26,320 --> 00:16:34,320
That statement is just loaded with problems.

163
00:16:34,320 --> 00:16:41,600
According to brain scans, which, by the way, require filtering out a whole bunch of data

164
00:16:41,600 --> 00:16:52,360
and can have useful results, but often say far less than people make them out to be saying.

165
00:16:52,360 --> 00:16:57,400
There's an area of the brain, yes, it's clear that there are areas of the brain, and they

166
00:16:57,400 --> 00:16:59,440
clearly perform different.

167
00:16:59,440 --> 00:17:10,360
I don't say, they activate at different times in different situations, and they have different

168
00:17:10,360 --> 00:17:18,720
causes and different effects, definitely areas of the brain, in charge of consciousness,

169
00:17:18,720 --> 00:17:28,640
in charge of consciousness, which would mean the brain-controlled consciousness.

170
00:17:28,640 --> 00:17:35,160
It sounds like something you might hear a scientist say.

171
00:17:35,160 --> 00:17:37,240
There's a problem with it though.

172
00:17:37,240 --> 00:17:42,680
I mean, there are studies that say all sorts of different things, and there are some interesting

173
00:17:42,680 --> 00:17:51,680
studies about times when the brain is completely inactive, with no measurable brain activity

174
00:17:51,680 --> 00:17:57,000
in any of the areas you're talking about, in any of the areas whatsoever, and yet consciousness

175
00:17:57,000 --> 00:18:06,280
is still verified, near death experiences, a common one.

176
00:18:06,280 --> 00:18:10,600
But then your question, actually, I don't quite understand, but I assume you're trying

177
00:18:10,600 --> 00:18:18,520
to ask something about, isn't consciousness just the brain, isn't the mind just the brain?

178
00:18:18,520 --> 00:18:27,560
We answer this many times in different ways, and at different lengths, but simply I would

179
00:18:27,560 --> 00:18:33,040
say that what I've said many times is that the brain doesn't even exist, brain is just

180
00:18:33,040 --> 00:18:41,840
something that we observe.

181
00:18:41,840 --> 00:18:47,240
Mind and brain appear to be intertwined, but what you're actually talking about is not

182
00:18:47,240 --> 00:18:56,200
quite clear, because brain is just, it's matter, which we still don't actually know everything

183
00:18:56,200 --> 00:19:04,320
there is to know about, we know that there are things like cells and molecules and atoms

184
00:19:04,320 --> 00:19:10,840
and subatomic particles, but we're still not entirely clear about what any of this is,

185
00:19:10,840 --> 00:19:24,760
and we're very generally ignorant of the context within which the physical realm is situated,

186
00:19:24,760 --> 00:19:32,200
which is in experience, like you have to actually experience all of this in order to

187
00:19:32,200 --> 00:19:36,440
even make observations, in order to say there is the physical realm whatsoever, you need

188
00:19:36,440 --> 00:19:38,640
consciousness.

189
00:19:38,640 --> 00:19:43,720
So to say that the brain creates consciousness is really putting the cart before the horse.

190
00:19:43,720 --> 00:19:48,800
You require consciousness to even make those observations to even have a concept of the

191
00:19:48,800 --> 00:19:51,000
physical realm.

192
00:19:51,000 --> 00:19:58,360
All of it, all of these observations that we make could be tricked, we could be in a

193
00:19:58,360 --> 00:20:03,000
simulation and there are scientists checking to see, oh, what if we do it like this and

194
00:20:03,000 --> 00:20:05,720
what if we do it like that?

195
00:20:05,720 --> 00:20:12,320
We don't really have any valid way of understanding, of verifying anything.

196
00:20:12,320 --> 00:20:16,280
All we can verify is that there is consciousness, so it actually comes first.

197
00:20:16,280 --> 00:20:22,040
It's more complicated, I mean generally I would say forget about such questions and work

198
00:20:22,040 --> 00:20:26,560
on what's in your most of your best benefit.

199
00:20:26,560 --> 00:20:41,400
Learn about what's in your benefit, what's to your benefit, your best interest.

200
00:20:41,400 --> 00:20:49,040
Learning boredom, I don't know how to manage this, be mindful.

201
00:20:49,040 --> 00:20:52,720
You know it to be a wrong state, it's not helpful.

202
00:20:52,720 --> 00:21:01,080
If and when you actually know something to be a wrong state, I actually actually know,

203
00:21:01,080 --> 00:21:04,040
it won't happen, that's how the mind works.

204
00:21:04,040 --> 00:21:12,360
The mind engages in reactions like boredom because it's under the impression that there's

205
00:21:12,360 --> 00:21:17,360
some benefit through observation and coming to see that there is no benefit to things

206
00:21:17,360 --> 00:21:22,280
like boredom, the mind gives them up, just use mindfulness.

207
00:21:22,280 --> 00:21:30,040
Here's an interesting question, done some other meditations, believe in God, but finding

208
00:21:30,040 --> 00:21:34,080
benefit in our meditation and different meditations.

209
00:21:34,080 --> 00:21:40,760
But still afraid that when you die, you might have been on the wrong path.

210
00:21:40,760 --> 00:21:47,040
So again I'm not under-resent clear, you're actual intent in asking this, but I get kind

211
00:21:47,040 --> 00:21:57,240
of a general gist and I know I have thoughts about these sorts of things like how unfortunate

212
00:21:57,240 --> 00:22:05,320
it is that religions play the fear card and say, believe this because if you don't, you'll

213
00:22:05,320 --> 00:22:07,960
go to hell.

214
00:22:07,960 --> 00:22:13,600
The problem is there are lots and lots of religions saying that about different things

215
00:22:13,600 --> 00:22:16,640
that you should believe.

216
00:22:16,640 --> 00:22:24,600
And so in fact, if you're really going to be afraid, then you're going to be afraid,

217
00:22:24,600 --> 00:22:33,440
well, then you're in big trouble because you've got to be really afraid of all these

218
00:22:33,440 --> 00:22:34,440
different ones.

219
00:22:34,440 --> 00:22:43,560
It's basically if that is your outlook, find the right path for when you die, then you're

220
00:22:43,560 --> 00:22:53,080
in big trouble because it's like playing roulette, roulette, it's like having a chamberful

221
00:22:53,080 --> 00:23:03,040
of bullets when you're playing Russian roulette, only one of them is empty, that's how it

222
00:23:03,040 --> 00:23:04,040
seems.

223
00:23:04,040 --> 00:23:10,800
In fact, I would say that none of that chambers are empty, they're all just going to get

224
00:23:10,800 --> 00:23:15,640
you killed in the end.

225
00:23:15,640 --> 00:23:26,640
So Buddhism's take on all this is learn the truth, don't rely on belief.

226
00:23:26,640 --> 00:23:29,920
I believe in God is a meaningless statement.

227
00:23:29,920 --> 00:23:35,640
I believe in God, really has no values of it.

228
00:23:35,640 --> 00:23:36,880
We put so much value on this.

229
00:23:36,880 --> 00:23:44,200
I believe in, yeah, so this mind state arises of belief.

230
00:23:44,200 --> 00:23:47,600
I believe in all sorts of crazy things.

231
00:23:47,600 --> 00:23:56,200
Much more valuable is I know I have evidence for, I have observed, these sorts of things

232
00:23:56,200 --> 00:24:04,360
have value and of course there should be hierarchical, more experience, more knowledge,

233
00:24:04,360 --> 00:24:10,600
more clarity, more firsthand knowledge, they say, all better.

234
00:24:10,600 --> 00:24:15,120
So you read about something in a book, that's one kind of knowledge, you know what the

235
00:24:15,120 --> 00:24:24,000
book says, you see it for yourself, that's a very different kind of knowledge and much

236
00:24:24,000 --> 00:24:26,040
more recommended.

237
00:24:26,040 --> 00:24:33,080
So I mean there's not really much to go in there, recommend meditating and you will find

238
00:24:33,080 --> 00:24:39,080
the truth for yourself.

239
00:24:39,080 --> 00:24:42,000
And you don't need to believe or worry about anything because it's like, yeah, I know what

240
00:24:42,000 --> 00:24:47,320
death is, I understand the mind, I've worked it all out.

241
00:24:47,320 --> 00:24:53,000
That doesn't scare me, that kind of thing.

242
00:24:53,000 --> 00:24:56,640
How do we help people if they don't want your help, at least to explain to them that

243
00:24:56,640 --> 00:25:02,520
yes, there is a way to see suffering?

244
00:25:02,520 --> 00:25:09,640
I suppose a better question is why help people if they don't want your help?

245
00:25:09,640 --> 00:25:14,680
I mean there's an answer there, I think the answer is that you want to help them.

246
00:25:14,680 --> 00:25:24,280
One thing things is a cause of suffering, one thing to help people no less.

247
00:25:24,280 --> 00:25:29,040
Another question we might ask is how do we help people if they do want your help?

248
00:25:29,040 --> 00:25:31,680
Let's answer that one first.

249
00:25:31,680 --> 00:25:37,280
I think a very basic answer is, it's very difficult.

250
00:25:37,280 --> 00:25:43,480
You might even answer, you can't, I'm talking about people that want your help, right?

251
00:25:43,480 --> 00:25:51,040
People want your help, how do you help them, you can't, you can, but it has to be qualified.

252
00:25:51,040 --> 00:25:54,080
You can help them learn how to help themselves.

253
00:25:54,080 --> 00:25:59,360
Can you do that with someone who doesn't want to be help?

254
00:25:59,360 --> 00:26:05,600
I suppose, potentially, you could convince them.

255
00:26:05,600 --> 00:26:10,160
I think the worst problem with it is not convincing them.

256
00:26:10,160 --> 00:26:17,840
The worst problem is that if they didn't have the honest, genuine interest in it, you're

257
00:26:17,840 --> 00:26:26,960
just going to be what, dragging them, kicking in the screen, you're just going to set yourself

258
00:26:26,960 --> 00:26:32,760
up for a great amount of stress and unpleasantness.

259
00:26:32,760 --> 00:26:41,080
Never take students who don't want your instruction, that's why I would never have children.

260
00:26:41,080 --> 00:26:44,160
That's just more difficult.

261
00:26:44,160 --> 00:26:53,800
If you want to go that route, I'd luck to you.

262
00:26:53,800 --> 00:26:56,720
I don't really get this question.

263
00:26:56,720 --> 00:27:00,440
Thinking about hard line Buddhist approach, I don't understand how that's true.

264
00:27:00,440 --> 00:27:07,560
I like actually the question, the Buddha said, wouldn't the Buddha consider simple things

265
00:27:07,560 --> 00:27:10,080
like starvation to be a case of mental in this?

266
00:27:10,080 --> 00:27:17,200
Well, I'm not quite sure what you're saying, but if you're saying intentional starvation,

267
00:27:17,200 --> 00:27:22,040
because people starve unintentionally, that's not mental in this, but intentionally starving

268
00:27:22,040 --> 00:27:28,000
yourself is that a mental illness, yeah, just like any wrong practice is some kind of

269
00:27:28,000 --> 00:27:29,000
mental illness.

270
00:27:29,000 --> 00:27:31,040
It's not a very terrible one.

271
00:27:31,040 --> 00:27:35,720
I mean, there are worse mental illnesses, or you might say that starving yourself

272
00:27:35,720 --> 00:27:38,600
purposely is caused by mental illness.

273
00:27:38,600 --> 00:27:41,760
And by that, we just mean delusion, really.

274
00:27:41,760 --> 00:27:51,040
The wrong idea that somehow starving yourself is going to have some positive effect.

275
00:27:51,040 --> 00:27:54,400
Otherwise I don't quite get everything that you're trying to say.

276
00:27:54,400 --> 00:28:01,040
It seems that the mind is clinging to food and getting agitated for not getting it.

277
00:28:01,040 --> 00:28:06,920
If that's true, then that's also a mental illness.

278
00:28:06,920 --> 00:28:12,760
We had this question recently, how should I approach the strong attachments for family

279
00:28:12,760 --> 00:28:16,560
and friends towards myself, a similar question recently?

280
00:28:16,560 --> 00:28:19,920
It seems I can't continue without causing suffering for them.

281
00:28:19,920 --> 00:28:23,960
However, I don't know when I am mindful enough to reduce this suffering.

282
00:28:23,960 --> 00:28:31,800
So let's put it out there that strong attachments are the cause of suffering.

283
00:28:31,800 --> 00:28:36,200
It's their fault.

284
00:28:36,200 --> 00:28:41,080
They put themselves in this position by their strong attachments.

285
00:28:41,080 --> 00:28:48,920
It's not to criticize them for it or even abandon them because of it, but it's called

286
00:28:48,920 --> 00:28:53,960
a spade of spade, that's the truth.

287
00:28:53,960 --> 00:28:59,200
We willingly put ourselves in a position, setting ourselves up for a great suffering.

288
00:28:59,200 --> 00:29:05,360
We know that intellectually that everyone we love is going to die, but we try to turn

289
00:29:05,360 --> 00:29:14,080
a blind eye and nonetheless cultivate such strong attachments that we do suffer, not just

290
00:29:14,080 --> 00:29:17,360
when they die, but when they change.

291
00:29:17,360 --> 00:29:23,040
So you have probably changed through practicing meditation, and I assume that's what you're

292
00:29:23,040 --> 00:29:24,840
talking about.

293
00:29:24,840 --> 00:29:31,440
You don't actually say, but if that's what you're talking about, then yeah, change is

294
00:29:31,440 --> 00:29:38,680
stressful for people who are attached to things that they think are stable.

295
00:29:38,680 --> 00:29:46,120
They have an image of you, and they have some pleasant memories of that image of you.

296
00:29:46,120 --> 00:29:54,440
So when you were this way, I had so much pleasure from, well, they don't talk like that,

297
00:29:54,440 --> 00:30:00,320
but that's really what they're saying, it's really the gist of it.

298
00:30:00,320 --> 00:30:05,360
Pleasure of a rose in my mind when you were this way, and now you're that way, and that

299
00:30:05,360 --> 00:30:10,560
pleasure no longer can arise because you no longer do x, y, or z.

300
00:30:10,560 --> 00:30:12,920
That's what I got when I started meditating.

301
00:30:12,920 --> 00:30:26,680
Very upset, family, friends, well, an unimpressed, but I mean, that's attachment.

302
00:30:26,680 --> 00:30:33,920
The problem, and I've said this before, is that the people that we come into this life

303
00:30:33,920 --> 00:30:42,560
with, when we're unmindful, our reasons for being close to them have very little to

304
00:30:42,560 --> 00:30:50,480
do with things like meditation, so it's no wonder that that should all collapse.

305
00:30:50,480 --> 00:30:54,520
We can't meditate and then say, okay, now I'll go back, and all my friends will love

306
00:30:54,520 --> 00:31:04,720
me for it, well, not if your life before was totally on a different path.

307
00:31:04,720 --> 00:31:10,080
I can only think of a couple of people who I grew up with, who there's a few people who

308
00:31:10,080 --> 00:31:15,160
I grew up with, who are actually interested in the sorts of things that I do.

309
00:31:15,160 --> 00:31:20,080
But for the most part, they're not people who I was actually close with, people on the

310
00:31:20,080 --> 00:31:24,920
periphery, or people who maybe I kind of dismissed because they were goody-goody, so that

311
00:31:24,920 --> 00:31:32,800
sort of thing, so it's kind of funny how, back then being a goody-goody was a bad thing,

312
00:31:32,800 --> 00:31:41,720
being a good person was uncool, something to be derided, if I thought a lot about it, I

313
00:31:41,720 --> 00:31:47,440
could probably find people who I was not nice to when I was younger who at this point

314
00:31:47,440 --> 00:31:59,600
I would think they were so much better than me, anyway, so the point being that you're

315
00:31:59,600 --> 00:32:05,160
not going to fix it, not necessarily, everyone has to lose everything that is dear to

316
00:32:05,160 --> 00:32:14,960
them, and your job is not to make people feel good about their attachments, I mean,

317
00:32:14,960 --> 00:32:22,840
not that you should make them feel bad intentionally, but it can't be the answer.

318
00:32:22,840 --> 00:32:31,520
Not being sent, of course, want to help people, doesn't help anyone if you just cut people

319
00:32:31,520 --> 00:32:38,400
off and make them very, very angry at you, so the only answer I really have for you that's

320
00:32:38,400 --> 00:32:45,400
probably useful is, take it slowly, take it easy, but keep clear in your mind that it's

321
00:32:45,400 --> 00:32:52,040
not wrong, people don't suffer because you stop feeding their addictions, they suffer

322
00:32:52,040 --> 00:32:56,720
because they're addicted, they have the addiction in the first place, they're like any

323
00:32:56,720 --> 00:33:02,760
addict, you slowly wean them off the drugs, I mean, it's all in the end, it's all chemicals

324
00:33:02,760 --> 00:33:16,200
in our brain that make us give us pleasure, it's all drugs in the end.

325
00:33:16,200 --> 00:33:21,240
This question I maybe should just not really answer, consciousness, some say consciousness

326
00:33:21,240 --> 00:33:28,120
leaves the body when it dies, so consciousness being the root of everything really, it's

327
00:33:28,120 --> 00:33:34,520
not that the consciousness leaves the body, it's that the body dies and is no longer, no

328
00:33:34,520 --> 00:33:45,200
longer like the thing that the mind clung to, so the mind abandons it, the mind rejects

329
00:33:45,200 --> 00:33:52,400
it because the body is no longer familiar, it happens when during access during your death

330
00:33:52,400 --> 00:33:58,160
experiences because the body again breaks, shuts down, stops being familiar and so there's

331
00:33:58,160 --> 00:34:02,760
a jolt and the mind is jolted out of it, it happens and out of body experiences when

332
00:34:02,760 --> 00:34:07,680
you get into states where you're disconnected from the body and the mind is able to jolt

333
00:34:07,680 --> 00:34:11,600
out of it and that sort of thing.

334
00:34:11,600 --> 00:34:17,480
But consciousness difference between alive and non-alive, if you're talking about physically,

335
00:34:17,480 --> 00:34:25,600
no, the consciousness stays the same, it doesn't matter what happens to the body, basically.

336
00:34:25,600 --> 00:34:33,800
Oh, and here we have new questions, oh we have a bunch of new questions, eight new questions,

337
00:34:33,800 --> 00:34:38,040
well that's not really fair.

338
00:34:38,040 --> 00:34:43,200
I think I'm going to put these aside because there were 13 or 14 when I started, so these

339
00:34:43,200 --> 00:34:48,840
ones are going to wait until next week because it gives a chance for people to, I don't

340
00:34:48,840 --> 00:34:53,160
know, to rate them, I guess, it gives me a chance to go over them and some I will just

341
00:34:53,160 --> 00:35:11,760
delete, I'm sorry to say, so that's all for tonight, thank you all for tuning in.

