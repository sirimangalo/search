Good evening and welcome to our live broadcast today we are continuing with the
Ungutranikaya. I'm just gonna go right to the next one because it's I mean it's
not terribly deep but it's interesting for those wondering about the role the
heavens play in in Buddhism so apologies that this isn't a deep
meditative meditation teaching I don't have any meditators here right now
so we're just gonna go on from number 36 to number 37 so apparently they're in
Buddhism that but it's cosmology the Buddha described various levels of angels so
there are bhuma devo which are angels that live around live on earth apparently
the living trees or whatever they delight in the fairies I guess you know
I think and then there are the successively higher planes of angels there are
the four great kings that if you you may be recognized some of you this from
Chinese mythology which gets it from Buddhism the four great kings that look
after the four directions apparently there are four angels who also are
involved with the earth but they take care of the four for quadrants of the
earth in directions doesn't really work because the earth is around so I don't
quite understand it but there are apparently four great kings and they report to
the angels of the tawatings which is the next level and tawatings is I guess
the highest angel realm that's directly interested in in the earth so tawatings
at tawam means three and things I mean 30 so it's the end they happen of the
33 and it's called that because there were these 33 companions who went to
heaven together and they're led by Sakka Sakka is the king of the angels of
the 33 and so I guess a group of angels who live in their their own realm but
somehow take an interest in the earth and so the four great kings report to
them four great kings are responsible for making sure that everything on
earth is I guess going according to whatever divine plan that they have I've all
this sounds fairly theistic or deistic well I suppose you could call it
deistic although I'm not even sure quite what that word means but there's
and there you know there is a sense of that in the time of the Buddha people
were obviously worshiping these angels and so the Buddha was acknowledging
the existence of the of such angels but what he denied was the efficacy of
prayer or the importance of prayer that sure you could you could pray and
beseech the gods for blessings or for mercy or so on but there's no reason to
think that they would comply nor is there any benefit from or any spiritual
benefit to such activities they certainly didn't have the power to enlighten
one or to bring the highest benefit and I guess that's really where Buddhism
goes beyond the theistic religion it's it doesn't deny the fact that angels
can and when you might call gods can potentially help human beings it's it's
not even the point to suggest that they that God can't move mountains as they
say in Christianity which anyway it's that moving mountains wouldn't really
help you know if if you pray to God and God were to answer all of your prayers
still wouldn't mean much because well the prayers that God can answer are me
I become enlightened me I'd be free from suffering me I'd be free from greed
anger and delusion me I become a better person it just can't happen doesn't
come from the grace of God the grace of God sure maybe you could get rich you
could live in luxury but you couldn't go to heaven God doesn't have the power
to bring someone to heaven or prevent them from entering heaven nor does any
God or angel have the power to send someone to hell not with some
qualification because of course beings can affect other beings but the
alternate power comes from within if your mind is pure no one can send you to
hell if you mind is impure it's possible to instigate that's certainly
possible that angels could manipulate you and trick you and into doing
leads that would send you to hell manipulate a person into sending them to
heaven so I mean the only it just means that there is an acknowledgement
whether you believe this or not that there are certain beings that are not
quantity not a qualitatively different from human beings in the sense that
they still have defilements and they still have lifespans and they're still
limited to affecting the physical realm they can't actually manipulate another
person's mind not directly but anyway there there is some order to the
angel realms if for those of you who are interested and curious about these
things what's interesting about this suit and what really makes it Buddhist
because obviously talking about angels isn't particular in Buddhist is that
Sakai is a Buddhist the king of the gods of the 33 is a follower of the
Buddha and they're concerned about the activities of human beings the
goodness of human beings it's a group and it's not to say that all angels will
be like this but this group of angels particularly is concerned as to
whether human beings are honoring religious people honoring elders behaving
properly towards their father and mother whether they are keeping holy days
in the sense of having religious observances spiritual practice on at least
once a week and doing good deeds are they giving charity are they keeping
morality are they practicing meditation but it's kind of funny why they're
interested in this and somewhat self-serving or pragmatic I guess they're
concerned with this because they say if the four great kings report to them
that human beings are doing all these things then they get really happy because
they say oh that means that's great that means there'll be more angels in the
future it means there'll be more of them more people coming and will have a
greater retinue will become it's actually somewhat of a base desire to swell
their ranks and they're and if they find out that people are doing bad things
and and not doing good things and they're sad because there won't be as many
angels in the future I mean for us it simply means it's a reminder that the
only way to go to heaven really is to be a good person is to have a pure
mind and all of that comes through doing good deeds being good to other people
being respectful to other people being kind having a pure mind not having
ulterior motives or a crooked treacherous mind not being obsessed with base
desires or or hatred or aversion grudging grudges if your mind can stay pure in
this to file the world then and only then can you free yourself from it from the
coarseness from the basis from the mundane to enter into something more pure
more peaceful more sublime and then and then there's something funny that
Saqa the King of the Angels reminds the angels he says the person who would be
like me should basically do lots of good deeds the person who'd be like me
should keep the holy keep the holy day he says keep the
both the time which means this day where people keep the eight preset and then
Buddhist says you know when he said this it wasn't well said it wasn't it
wasn't a proper verse to say not because it's it's not good to keep the religious
preset sort of practice goodness and I can say but because Saqa you know
what who should who no one should should strive to be like Saqa
because Saqa is still not devoid of last hatred and delusion and he says but I
say the same thing a person who would be like me should observe all of these
things because I am because I know not me sorry it's not me a beaucoup who has
who has become an aran someone who has become enlightened not just the Buddha
but anyone who's become enlightened should therefore say the person who would
be like me so I mean this kind of puts angels in their place and it's really
how his Buddhists we look at the world you know we don't see angels most
people will live their lives never having any evidence that these beings
exist but in so far as you might think that they do exist there's still
there's still subject to the same laws and and regulations of the universe
that goodness goodness is what leads to heaven and evil is what leads to
hell they don't these beings don't have the power to make people better people
but they're happy to know that people do they rejoice as all good people
should I mean we'd rejoice for we see more people meditating it means there
will be more meditators it's in fact not a terribly bad thing to think
because it makes our practice easier makes it easier to do good deeds when
other people are keen on good deeds when everyone's doing unwholesome
and wholesome deeds and obsessed with the filements becomes more difficult to do
good things because you get sucked you get drained by the negativity of others
anyway so that's a little bit of demo for today anyway I'm just working
and that's all about we have any questions we do Monday in practical way
we're just talking but I should mention it first just a reminder to people
that we have we may have young people coming on this site and any sort of adult
conversation about sexual intercourse or other things I guess drugs alcohol
should be moderated or moderate tempered so just be mindful of where we are
and the fact that we're not all of age so not that anything inappropriate has
been done or said but just a reminder that to keep it keep it PG or there's
potential problems with talking about such things to people who are
potentially underage okay that's all okay in practical insight meditation
Mahasi Sayyadha talks about reflecting on certain things but to also notice
reflecting to keep it to a minimum but you seem to be saying it's not
necessary to think or reflect at all during meditation and it's only
necessary to teach etc so are you disagreeing with him or am I missing
something and how is it possible to garner insight without reflecting on your
experience I don't think I said that it's not I like reflection has a place in
terms of stepping back and reflecting on on the quality of your practice but
the only benefit there is to adjust your practice if your practice is going
fine I can see you know you know there's no benefit to reflecting on things
that you've learned you know certainly reflecting is natural that it's
something you should be mindful of but it's not the practice and I mean read
with the Mahasi Sayyadha says about what we call nyana
is one of the ten uppercu they say when your knowledge the knowledge is
that you gain when you become obsessed with it fixate it on it when you start
thinking about it that's actually to the detriment of your practice so the
question about how could insight arise if you don't reflect I mean that's
not how in what insight is insight is a direct realization of things it's
not reflection when you see that things are arising and ceasing that's
insight and the more you see that the closer you'll get to letting go once you
see it clearly you'll just have kind of like an epiphany where you were not
even not even a thought but it's just an observation that everything that
arises ceases and there's nothing worth clinging to and in the mind that's
go there's no there's no place in there for reflection I think it's a
misunderstanding of what insight is inside is not a thought inside is actually
we pass and a bus and I mean seeing we means clearly or especially or whatever
you want to translate we really means with wisdom banya banya actually
just means banya knowledge but complete you know when you see when you know
that this is rising when you know that this is falling that's actually wisdom
when you see it arising and ceasing that's wisdom that's all you need
I don't think Mahasim Saiya does suggest otherwise but you'd have to
probably read the Burmese or be sure that it was properly translated and then
you have to understand that you know he's generally he generally makes
allowances you know if he's actually advising people to stop and reflect and
I'd be quite surprised but on the other hand there is an advice for stepping
back and reflecting in so far as it helps you see whether your practice is
correct whether you're actually doing things properly and reflect on
whether you're you're adequately you know you're really missing something or
there's something to be adjusted that's called the monks one of the four
you get part of one of the four roads to success
dear Bhante what is the difference between a sense of apathy versus an
impartial stance towards something thank you
I don't know I mean they're just words you'd have to see what the actual state
was someone who says I don't care I mean it's it sounds like it actually exists
but it's it's just words and it's just a view and it's a lot of ego in the
sense of I you know I am above that kind of thing but they're just words I
mean when an experience occurs are you actually an
un-un-un-reactive towards it because such people actually when they do when
they are presented with something that does affect them and they react
eventually the point is not everyone is reactive towards towards everything
for some people some things are not going to create a reaction but that's you
know it's not a sign that they are apathetic it's just a sign that they let
that thing is doesn't create it doesn't have any kind of history with that
person so you know for some people loud noises is obnoxious here in the West
if your neighbors are having a party you get quite upset in Asia if your
neighbors are having a party people don't even seem to hear it it's quite
interesting to see some of the differences from culture to culture the sort of
things that bothers bother people from different cultures a general sense of
apathy I mean there can be a delusion delusion is is generally associated
with with equanimity and I guess that's really the technical answer is that
questions whether it's associated with delusion but the delusion would be kind
of this distracted thought if a person is engaged in mental
fermentation if they're thinking about things and pondering and wondering and
confused and that kind of thing that's delusion and there's there's equanimity with it
and then there's the the other the wholesome kind is with equanimity one does
something good or one calculates wholesomeness in the mind through meditation
but the equanimity you know the I guess the point of the equanimity is the same
but there's nothing to do with the equanimity it's the delusion whether in the
mind there's ego or there's confusion or there's mental distraction whether the
mind is clear or the mind is is clouded
Thank you. Thank you. Is there any point being grateful karma is going to
manifest anyways could or bad how should we include gratitude in our
practice? Absolutely I mean that's not a good outlook. I mean you think okay I
shouldn't be grateful because that person's going to get what's coming to
them anyway who cares who cares whether they get what's coming to them or not
that's not the point gratitude is a wonderful state of mind gratitude is we
don't worry about other people we're not grateful so that it helps the other
person we're grateful so that we don't become mean and terrible people who have
no sense of appreciation for the good deeds of others
gratitude is something that should come naturally and if you find yourself
ungrateful or un-mindful of the good deeds of others you should not that
and try to understand why is it because himself is yourself absorbed
that's generally based in due to either one of the two a strong sense of greed
and or a strong sense of a strong delusion being self-absorbed and or you
know it can also come from anger I suppose if you're totally obsessed with your
own suffering so that when people help you you just fix and focus on your own
problems and how your own problems are solved or not solved than thinking
about the person who helped you gratitude is most apparent in those who have
the least attachment those who are at least concerned about their own well-being
people who are most able to appreciate the good things in others are those who
have good things in themselves or those who aren't concerned about their own
good things in the sense that they don't have feelings of guilt or worry the
people who have lots of unpleasant things inside tend to be too self-absorbed
worried about their own well-being concerned and greedy or angry and
frustrated and upset and deluded and conceded you can see how all of those
things get in the way of of gratitude so when all of those gone it's very easy
to appreciate the good things in others you're able to mindful be mindful of
the truth sometimes that this person helped you I wouldn't try to cultivate it
too much I'd just be more aware as to whether you have it or not and if you don't
have it there's a sign it's a sign that you've got something blocking it
can you change the life broadcast to a different time is there's someone else
who would want that question I can change it to another time this time happens
to be fairly convenient for me won't be convenient on Monday soon but apart
from that I can't think of a better time for me I can do it early afternoon
but that would probably be a bad choice for most of our viewers since most of
our viewers are in North America which I know doesn't really help those of you
who are in Europe but look at the if you look at the meditation list majority
or USA if you Canada and we got a bunch of Mexico and so on which I mean the
question is what is the cause what is the effect whether it's you know because
we're broadcasting evening right that could just be because of the time
there's the Europeans aren't going to be on much anymore except for rustlin
rustlin yeah I had just read down a little bit it was because the person
often had some follow-up questions to ask during the life broadcast but
decided that he would be patient and use it as a practice or you could get
really fancy but they have Tuesday and Thursday's early and Monday Wednesday
Friday at the normal time and I can't even keep track of my schedule as it is
I don't know what day it is at most days keeping it simple is good too
Monday during my last meditation session it suddenly felt like my legs were
not covered by my skin the legs still felt like mine but not the skin it was
sickening to my stomach and I still practicing right well that's not practice
you see that's that's experience the practice is how you relate to that
experience be very clear that these are two distinct things what you experience
and what arises based on your practice it's not the practice whether it be
knowledge or whether it be strange experiences like this or even whether it be
unpleasant experiences defilements if anger arises that's not the
practice it's not like oh I'm very angry I must be doing something wrong
anger is an experience it's a mental reaction it is a bad thing but don't
worry about that that's not your concern it happened your concern is what you're
going to do about it so when you have this experience how are you going to
react to it back there discussed it is potentially problematic because there's
a version disliking so that's not the proper reaction but now that that's
happened you should say disliking disliking or disgusting disliking is probably
better if you're just aware of it then you would say knowing knowing aware
or something it's a feeling you might say feeling and that's the
practice then you see it comes and it goes I think the problem is it's
practice is terribly unremarkable the practice itself is too simple it's
unremarkable that's it you just say thinking thinking seeing seeing feelings
in angry angry but it's made up for the fact that when you do that there's
fireworks you know the things that you're observing are incredibly
interesting there's a real transformation that goes on so be clear that the
practice is incredibly simple it's not much to it but the experiences that
you're going to have are diverse and very terribly exciting if you
let them be but of course you shouldn't it should be mindful of it
is it best practice to always keep the eyes open or can the eyes be closed
while chewing and swallowing it certainly can be closed while chewing and
swallowing there's no rule harder fast but they'll be really keen on it
closing your eyes is a good thing that's your focus on focus more on the the
mouth and the throat taste and so on
well the current Android app still be working when the new site goes live no
we probably could make it work if someone wanted to make a better Android app
and be great but on the other hand it might be nice just to use the website as it
is because then we only have to work on one thing but the new site looks a lot
like the Android app if you've seen it so yeah give up the Android app once
the what's the new site goes live no there's an iOS app apparently but I think
that may work
and you still can sign in to the regular site on a phone so if it's just a
matter of wanting to be portable you can still log your time in just through
regular browser on the phone and done that well yeah the new the new site
will look a lot like the Android app anyway it won't be as hard to do like
this one you have to find the little button but on the new one it'll be much
more mobile friendly
my sitting meditation is usually stressful and it seems to be getting worse
it feels like I'm forcing myself to stay seated do I just continue to work
really yeah I mean this is a case where you might want to use my lungs as step
back and reflect on what you might be doing wrong what you might be missing
not doing wrong exactly but the aversion is what you should be meditating
one you know if you feel stressed and focus on the stress it's not easy and
this is where the fireworks happen this is where the simple technique does a
number on you it really messes with you you know challenges you
it's called the it sounds like probably
why not probably you know from the sounds of it you're
you're doing fine because you're cultivating patience
if it feels like you're forcing then it's not that you're forcing it's that
you're missing something you're not actually being mindful of the aversion
try and be as mindful of the aversion as you can it's quick
catch what's really happening because there's not such thing as meditation
there's not you don't have sitting meditation it doesn't exist
what happens when you sit down and close your eyes
and then you watch the stomach now the tendency probably is to try and force
the stomach to rise and fall which is an interesting thing it's an
interesting fact that that your mind you know this is the case
that your mind is doing that so the things that your mind that our mind does
and is what we're interested in we're interested to learn how the mind works
to learn about the good things and the bad things in the mind
what we're doing right what we're doing wrong it sounds like probably like
all of us you're doing something wrong so just keep watching and you'll
slowly see oh I'm doing this all wrong boy I should just stop
can I do mindful meditation without mantras
you can but you'd have to find a different teacher and a different
tradition because some people say that you don't you shouldn't use mantras
in our tradition we say you should and it's actually important to use
mantras
so not in our tradition sorry I mean why would you want to I guess it's the
question of course I understand why people want to but
just believe you're shaking your head because they're so beneficial
that's the most powerful thing we have is this tool
reminding ourselves that the best meditation technique I can think of
people that don't use them are missing out and don't know what they're missing
out
you're all caught up on questions one day
all right then that's all for tonight thanks Robin for your help
thanks everyone for coming out and for meditating and for your questions
thank you
