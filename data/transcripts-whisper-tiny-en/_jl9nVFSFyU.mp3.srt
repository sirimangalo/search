1
00:00:00,000 --> 00:00:07,200
Okay, while meditating, I start noting the rising and falling like you teach, but when I completely follow

2
00:00:07,520 --> 00:00:10,720
Noting the mind. I usually never go back to the noting

3
00:00:11,160 --> 00:00:16,520
Rising and falling during the 10 to 20 minute meditation. Do I have to force my way back at some point?

4
00:00:22,960 --> 00:00:24,960
Well, no

5
00:00:24,960 --> 00:00:29,080
But I think what you do have to consider is whether you are

6
00:00:30,600 --> 00:00:31,880
prompting

7
00:00:31,880 --> 00:00:33,880
this is a common

8
00:00:34,440 --> 00:00:36,440
problem for meditators

9
00:00:37,440 --> 00:00:39,440
That that makes us

10
00:00:40,240 --> 00:00:42,240
prescribe a

11
00:00:42,560 --> 00:00:49,800
Specific technique of returning to the rising and falling every time you finish noting something

12
00:00:49,800 --> 00:00:53,720
So you make a kind of a conscious decision

13
00:00:54,840 --> 00:01:02,200
Whether or not you'll actually be able to carry it out to return to the rising and falling if you're not able to if there's more

14
00:01:04,960 --> 00:01:08,560
Experiences to be noted then you can note them. There's no there's no need

15
00:01:09,360 --> 00:01:11,360
Absolutely, you shouldn't force your way back

16
00:01:11,880 --> 00:01:17,060
But the problem is that some people actually prompt the next one though in a sense. They're asking the question

17
00:01:17,060 --> 00:01:21,700
What what else what's there?

18
00:01:22,580 --> 00:01:24,580
there they're actually

19
00:01:25,180 --> 00:01:31,180
Looking to find something as opposed to simply responding to something that is distracting them

20
00:01:32,380 --> 00:01:38,060
From the stomach from the rising and falling so you should look at it that way if if it's not distracting you

21
00:01:39,060 --> 00:01:41,060
From the rising and falling

22
00:01:41,060 --> 00:01:47,220
Or the only reason you would acknowledge it is if it's distracted you if it's already captured your attention

23
00:01:47,940 --> 00:01:52,340
You shouldn't be hopping from one experience to another

24
00:01:54,860 --> 00:02:01,260
In the sense of looking for the next one to avoid this after some after you've acknowledged

25
00:02:02,060 --> 00:02:04,060
thinking thinking

26
00:02:04,060 --> 00:02:06,060
your your

27
00:02:06,060 --> 00:02:13,460
Next move should be to return to the rising and falling the only reason you wouldn't is if you were in a sense interrupted

28
00:02:15,060 --> 00:02:19,780
Noting thinking thinking by pain first for example by an emotion for example

29
00:02:20,700 --> 00:02:26,660
If something else catches your attention while you are in the mid in the process of

30
00:02:27,180 --> 00:02:29,980
acknowledging the thought or or the experience

31
00:02:29,980 --> 00:02:37,460
Otherwise, the prompting will lead to increased distraction increased restlessness. It's not actually

32
00:02:38,820 --> 00:02:41,780
conducive to insight certainly not inclusive to

33
00:02:43,900 --> 00:02:47,220
Concentration which is an important part of the practice

34
00:02:47,740 --> 00:02:49,900
so no forcing but

35
00:02:49,900 --> 00:02:59,780
Intentional returning whenever possible just to keep yourself focused and keep yourself from from prompting from from jumping

36
00:02:59,780 --> 00:03:19,780
Intentionally

