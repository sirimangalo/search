1
00:00:00,000 --> 00:00:05,000
unable to perceive the rising and folding motion.

2
00:00:05,000 --> 00:00:13,000
Once if one is unable to perceive the rising and folding motion of the abdomen,

3
00:00:13,000 --> 00:00:19,000
it is common for new meditators, especially those who are exceptionally tense in body

4
00:00:19,000 --> 00:00:23,000
and mind to have difficulty breathing naturally.

5
00:00:23,000 --> 00:00:29,000
Meditators should rest assured that with practice the body and mind who relax sufficiently

6
00:00:29,000 --> 00:00:34,000
to allow ordinary observation of the movements of the abdomen,

7
00:00:34,000 --> 00:00:39,000
it may help to place the hand on the abdomen during meditation to familiarize

8
00:00:39,000 --> 00:00:41,000
oneself with the movements.

9
00:00:41,000 --> 00:00:46,000
If this does not work, one can practice meditation in the lying position

10
00:00:46,000 --> 00:00:50,000
where the movements should be clearly discernible.

11
00:00:50,000 --> 00:00:54,000
Once the movements of the abdomen are temporarily understandable,

12
00:00:54,000 --> 00:00:59,000
one may note sitting, sitting instead.

