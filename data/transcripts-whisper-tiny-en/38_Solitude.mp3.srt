1
00:00:00,000 --> 00:00:02,000
solitude

2
00:00:02,000 --> 00:00:03,200
Question

3
00:00:03,200 --> 00:00:06,440
Do some enlightened beings retreat into solitude?

4
00:00:07,000 --> 00:00:11,900
The Buddha did not do that, but do you think you would consider that decision respectable?

5
00:00:13,000 --> 00:00:15,400
Answer yes, absolutely

6
00:00:16,320 --> 00:00:22,520
The Buddha himself retreated into solitude more than once if the Buddha hadn't been asked to teach

7
00:00:22,800 --> 00:00:27,520
It is likely that he would have just retreated into solitude for the remainder of his life

8
00:00:27,520 --> 00:00:31,320
solitude is completely respectable

9
00:00:32,440 --> 00:00:35,280
There are many stories of arahans who lived in solitude

10
00:00:36,440 --> 00:00:40,160
The Buddha was clear that there are many different ways to live as a Buddhist

11
00:00:41,000 --> 00:00:44,160
You do not have to live in solitude, but it is not wrong

12
00:00:44,160 --> 00:00:50,880
The most important thing is that you practice mental solitude keeping yourself secluded from mental impurity

