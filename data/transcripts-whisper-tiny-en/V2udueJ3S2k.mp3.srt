1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
I

3
00:00:43,360 --> 00:00:49,120
Good evening, everyone and welcome to our daily broadcast

4
00:00:49,120 --> 00:01:01,120
Today we're looking at the good-turning guy a book of force

5
00:01:03,120 --> 00:01:06,120
Sueton number 28

6
00:01:11,120 --> 00:01:13,120
The Aria Wang's a suit

7
00:01:13,120 --> 00:01:23,120
Wang Sam means lineage or heritage Aria of the nobles

8
00:01:28,120 --> 00:01:37,120
So we're looking at the the tradition

9
00:01:37,120 --> 00:01:42,120
The tradition of the nobles are noble tradition

10
00:01:47,120 --> 00:01:49,120
There's so much that is noble about

11
00:01:52,120 --> 00:01:57,120
What we're engaging in here and there's so much tradition

12
00:02:00,120 --> 00:02:03,120
We're following the tradition of the ancient nobles

13
00:02:03,120 --> 00:02:09,120
The noble are maybe we need a better word than noble, but honorable

14
00:02:12,120 --> 00:02:14,120
Exalted

15
00:02:15,120 --> 00:02:17,120
Enlightened

16
00:02:17,120 --> 00:02:20,120
We're following the tradition of the enlightened ones

17
00:02:20,120 --> 00:02:31,120
When you undertake the five precepts, this is the lineage

18
00:02:34,120 --> 00:02:41,120
You intentionally decide not to kill, to steal, to cheat, to lie, to take drugs or alcohol

19
00:02:41,120 --> 00:02:46,120
We undertake these rules

20
00:02:50,120 --> 00:02:54,120
Before we talk about whether it's necessary or important, it's tradition

21
00:02:56,120 --> 00:03:02,120
It's tradition and it's the tradition that is most noble, passed on by the Buddha himself

22
00:03:02,120 --> 00:03:12,120
When you sit down, close your eyes and undertake to meditate, saying to yourself

23
00:03:13,120 --> 00:03:17,120
With this practice, may I improve my mind

24
00:03:20,120 --> 00:03:23,120
Undertaking the lineage, the heritage of the noble

25
00:03:23,120 --> 00:03:31,120
You're following in the footsteps of the Buddha and all of his enlightened disciples

26
00:03:34,120 --> 00:03:42,120
Further, when you learn about the Buddha's teaching and the force of the Patana

27
00:03:42,120 --> 00:03:50,120
We're not just sitting in meditation, you practice meditation in the Buddha's way

28
00:03:50,120 --> 00:04:09,120
Cultivating effort and mindfulness and clear comprehension of the reality of things

29
00:04:09,120 --> 00:04:19,120
This is the heritage of the noble ones

30
00:04:24,120 --> 00:04:26,120
So much

31
00:04:27,120 --> 00:04:31,120
This isn't something that you pick up at the YMCA

32
00:04:31,120 --> 00:04:39,120
You pick up from New Age Guru

33
00:04:41,120 --> 00:04:49,120
What we're practicing has tradition, has precedents, it has standing in stature

34
00:04:50,120 --> 00:04:53,120
It's ancient, more ancient, and Christianity

35
00:04:53,120 --> 00:05:00,120
It's a very old teaching

36
00:05:05,120 --> 00:05:10,120
It's where you're joining a community, a global community of

37
00:05:16,120 --> 00:05:20,120
Practitioners following noble way

38
00:05:20,120 --> 00:05:25,120
You may think you sit down, close your eyes and you're all alone

39
00:05:26,120 --> 00:05:29,120
Dealing with things that are very difficult to deal with

40
00:05:30,120 --> 00:05:33,120
It's good to remember that you're part of something bigger

41
00:05:34,120 --> 00:05:37,120
This is what religion offers, something bigger

42
00:05:38,120 --> 00:05:42,120
To protect you, to nourish you

43
00:05:42,120 --> 00:05:49,120
To nurture your burgeoning spirituality

44
00:05:54,120 --> 00:06:02,120
So within this tradition, this lineage, the Buddha talks about four Aria ones

45
00:06:02,120 --> 00:06:11,120
And they're well-known set in Buddhism, it's one of the things that we learn in the first year

46
00:06:12,120 --> 00:06:15,120
Dhamma studies in Thailand, in order to take an exam

47
00:06:16,120 --> 00:06:19,120
The first exam, this is one of the things we have to learn

48
00:06:19,120 --> 00:06:26,120
So four traditions

49
00:06:29,120 --> 00:06:33,120
And the religions have their own traditions, cultures have their traditions

50
00:06:33,120 --> 00:06:35,120
What are the traditions of Buddhism?

51
00:06:35,120 --> 00:06:37,120
They're quite simple

52
00:06:37,120 --> 00:06:42,120
This isn't to be all end, but these are very core of Buddhist tradition

53
00:06:42,120 --> 00:06:45,120
The first one is the robe

54
00:06:45,120 --> 00:06:51,120
We're clothing, you could say

55
00:06:52,120 --> 00:07:00,120
Here one is content with any kind of clothing, any kind of cloth

56
00:07:01,120 --> 00:07:06,120
And they speak in praise of contentment with clothing

57
00:07:06,120 --> 00:07:14,120
Not worrying about looks, not worrying about their appearance, or their image

58
00:07:15,120 --> 00:07:20,120
You're not fussing with jewelry or makeup, or hairdoos

59
00:07:20,120 --> 00:07:44,120
And they stop looking for clothing, or equipment, decorations

60
00:07:44,120 --> 00:07:52,120
And if they don't get, this is specifically for monks, but if they don't get robes, they're not agitated

61
00:07:53,120 --> 00:07:59,120
If they get one they use it without being tied to and saturated with it, or blindly absorbed in it

62
00:07:59,120 --> 00:08:16,120
Seeing the danger and understanding, they escape from it

63
00:08:17,120 --> 00:08:24,120
So this has to do with image, this has to do with our appearances, our personality

64
00:08:24,120 --> 00:08:28,120
Who we present ourselves as

65
00:08:28,120 --> 00:08:32,120
This is a noble tradition, noble lineage

66
00:08:33,120 --> 00:08:38,120
To not be concerned with appearances, not be concerned with their body

67
00:08:44,120 --> 00:08:48,120
Keeping the body healthy and clean, but not beautiful

68
00:08:48,120 --> 00:08:55,120
Not primping it up, not attaching to it, looking in the mirror, making sure you're beautiful

69
00:08:55,120 --> 00:09:05,120
And at the same time not being arrogant, or conceited because of that, being humble

70
00:09:09,120 --> 00:09:16,120
Robes are of anything in Thailand, they're all too small for Westerners, so we're all going around with these

71
00:09:16,120 --> 00:09:26,120
Now for many Western people, get robes, and they're all of one size

72
00:09:26,120 --> 00:09:31,120
And try to be one-size-fits-all

73
00:09:31,120 --> 00:09:39,120
It was a long time before I was able to get robes that actually fit me

74
00:09:39,120 --> 00:09:49,120
Or that we're actually caught in because they have all this polyester robes, and you have to be content with it, even though it doesn't breathe, it's not very comfortable

75
00:09:53,120 --> 00:09:58,120
But you don't go seeking for this or that kind of

76
00:09:58,120 --> 00:10:08,120
You know, they're all in general, trying to enhance your appearance

77
00:10:12,120 --> 00:10:18,120
This is one of the things that noble beings don't worry about, how they look, and by extension how they appear

78
00:10:18,120 --> 00:10:22,120
And how other people, what other people think of them

79
00:10:22,120 --> 00:10:27,120
By extension, if you look at this, it's a whole

80
00:10:27,120 --> 00:10:48,120
Intense sort of obsession that we have with how we look, and how we appear, and how other people, what others think of us

81
00:10:48,120 --> 00:11:03,120
Is worrying, and fretting, and judging ourselves, reacting when other people criticize, or speak poorly about us

82
00:11:03,120 --> 00:11:08,120
So we have to present an image, that's scary

83
00:11:08,120 --> 00:11:13,120
It's a lot of work, and a lot of stress, and a lot of suffering

84
00:11:13,120 --> 00:11:19,120
But we're a meditator for someone who's practicing the noble lineage, they stop worrying about that

85
00:11:19,120 --> 00:11:22,120
They start to see, it's not even real

86
00:11:22,120 --> 00:11:28,120
And more importantly, they see that it's not, or more clearly, they see that it's

87
00:11:28,120 --> 00:11:33,120
While not being real, it's not even dangerous

88
00:11:33,120 --> 00:11:39,120
Even if the whole world thought you were, why I thought you were, what's the worst they could do

89
00:11:39,120 --> 00:11:46,120
I think you are a terrible person, doesn't really mean anything

90
00:11:46,120 --> 00:11:51,120
The worst is that people are going to talk behind your back, we get so upset when people talk badly about us

91
00:11:51,120 --> 00:11:54,120
On the internet, it's lots of fun

92
00:11:54,120 --> 00:11:58,120
When you're on YouTube, and then people comment on your videos, and they tell you

93
00:11:58,120 --> 00:12:01,120
What a terrible person you are

94
00:12:01,120 --> 00:12:08,120
And someone recently write me a scathing email telling me I'm a spoiled brat

95
00:12:08,120 --> 00:12:12,120
And get lots of things like that

96
00:12:12,120 --> 00:12:16,120
And it's very easy to get upset about, right?

97
00:12:16,120 --> 00:12:21,120
Oh, you go and obsess and what a terrible person I am

98
00:12:21,120 --> 00:12:25,120
Well, you get angry at them, and you think, oh, I have terrible they are

99
00:12:25,120 --> 00:12:31,120
Say this nasty thing about me

100
00:12:31,120 --> 00:12:35,120
And all for nothing, you know, and what is it? It's just words

101
00:12:35,120 --> 00:12:39,120
Funny how words are, people's judgment of us

102
00:12:39,120 --> 00:12:43,120
Our judgment of ourselves gets in our way

103
00:12:43,120 --> 00:12:47,120
It's all because of image, because of personality who we think we are

104
00:12:47,120 --> 00:12:51,120
None of it's real

105
00:12:51,120 --> 00:12:55,120
So that's the first one relating to appearances

106
00:12:55,120 --> 00:12:59,120
The second one is food

107
00:12:59,120 --> 00:13:06,120
And so one is content with any kind of food

108
00:13:06,120 --> 00:13:09,120
And they speak in praise of content with any kind of food

109
00:13:09,120 --> 00:13:15,120
And they don't go looking for special food

110
00:13:15,120 --> 00:13:20,120
They don't want to try manipulating others for the right kind of food

111
00:13:20,120 --> 00:13:23,120
A food in Buddhism is a four kind

112
00:13:23,120 --> 00:13:32,120
There's real food, and then there's all sorts of mental kinds of food

113
00:13:32,120 --> 00:13:37,120
So we could look at this in the sense of the input

114
00:13:37,120 --> 00:13:41,120
The things we put into our body

115
00:13:41,120 --> 00:13:45,120
We can become very particular, or not our body, but also our mind

116
00:13:45,120 --> 00:13:49,120
We can become very particular about both what we put into our body

117
00:13:49,120 --> 00:13:54,120
And what we put into our mind, needing only certain experiences

118
00:13:54,120 --> 00:14:00,120
Be able to bear with other experiences

119
00:14:00,120 --> 00:14:07,120
Maybe it's too hot or too cold

120
00:14:07,120 --> 00:14:14,120
Maybe something is displeasing or unpleasant, boring, frustrating,

121
00:14:14,120 --> 00:14:21,120
agitating, terrifying, disgusting

122
00:14:21,120 --> 00:14:27,120
We're constantly judging our experiences, no, the things we

123
00:14:27,120 --> 00:14:32,120
The things we take in from the world around us

124
00:14:32,120 --> 00:14:36,120
It's all food, it's all nourishment

125
00:14:36,120 --> 00:14:49,120
It feeds our body, it feeds our mind, or the poisons our body, and the poisons our mind

126
00:14:49,120 --> 00:14:51,120
Most importantly, the poisons our mind

127
00:14:51,120 --> 00:14:53,120
Good things, right?

128
00:14:53,120 --> 00:14:56,120
Good to healthy food can poison your mind

129
00:14:56,120 --> 00:15:02,120
Even while it's nourishing your body, it's poisoning

130
00:15:02,120 --> 00:15:12,120
Poisoning your heart, filling it up with greed and attachment, addiction

131
00:15:12,120 --> 00:15:17,120
Filling it up with ego and arrogance, as you get stronger from the food or healthier

132
00:15:17,120 --> 00:15:24,120
You feel good, delusion and delusion of health

133
00:15:24,120 --> 00:15:31,120
We're going to be so healthy, and we're going to eat all the right things and live forever

134
00:15:31,120 --> 00:15:40,120
And we get very obsessed with what was in our body for health or for pleasure

135
00:15:40,120 --> 00:15:47,120
How attached we get to certain types of food, such that even the thought of them makes our mouth water

136
00:15:47,120 --> 00:15:56,120
Jesus burgers, pizza, potato chips

137
00:15:56,120 --> 00:16:02,120
All words that meditators never want to hear

138
00:16:02,120 --> 00:16:10,120
My meditators are over their cringe, cheesecake, actually the food here is pretty good

139
00:16:10,120 --> 00:16:21,120
But there's no potato chips, chocolate bars, hamburgers, pizza

140
00:16:21,120 --> 00:16:30,120
It's funny, you stay in the meditation center long enough, boy, the things you start to crave

141
00:16:30,120 --> 00:16:37,120
Good, dangerous, you see, man, it's good practice, good to see that

142
00:16:37,120 --> 00:16:40,120
I said silly, really, what is a potato chip?

143
00:16:40,120 --> 00:16:42,120
It's poison, really

144
00:16:42,120 --> 00:16:44,120
What is pizza?

145
00:16:44,120 --> 00:16:48,120
It just goes into your mouth, you feel good for a bit, and then it's gone

146
00:16:48,120 --> 00:16:56,120
It's gone, you just want more, didn't make you happier

147
00:16:56,120 --> 00:17:04,120
I mean lots of pizza in my life, I don't feel happier because of it

148
00:17:04,120 --> 00:17:13,120
There are addictions, things we can cut up, addicted to food, food is the basic

149
00:17:13,120 --> 00:17:19,120
Everything else, of course, is involved, now we're addicted to our cell phone

150
00:17:19,120 --> 00:17:25,120
Someone was here a few days ago saying, I have been wearing those, and it's true

151
00:17:25,120 --> 00:17:29,120
It's amazing, it's just exponentially worse

152
00:17:29,120 --> 00:17:35,120
Actually everyone has a sitting on the bus, get on the bus

153
00:17:35,120 --> 00:17:39,120
When you take the bus like I do, you get to see it

154
00:17:39,120 --> 00:17:44,120
But even not on the bus, people walk around, looking at their phones

155
00:17:44,120 --> 00:17:51,120
Drive, looking at their phones, getting accidents, looking at their phones

156
00:17:51,120 --> 00:18:01,120
Anyway, food, being content with food, learning how to free yourself from this need for certain types of entertainment

157
00:18:01,120 --> 00:18:13,120
And certain types of experience, input, certain types of input

158
00:18:13,120 --> 00:18:20,120
Being free from that, so you can be truly happy, not worrying about, was I get this, will I get that?

159
00:18:20,120 --> 00:18:33,120
When I not get this, will I not get that?

160
00:18:33,120 --> 00:18:42,120
The third one is in regards to lodging, lodging is your situation, we can extrapolate

161
00:18:42,120 --> 00:18:51,120
It's nice to extrapolate these and think of what they really represent

162
00:18:51,120 --> 00:19:00,120
So, lodging represents our situation

163
00:19:00,120 --> 00:19:06,120
This includes lots of things, includes the room that we have to sleep in

164
00:19:06,120 --> 00:19:15,120
For the park bench we have to sleep under, it includes the tree that we use for shade

165
00:19:15,120 --> 00:19:24,120
But it also includes our situation, our job, our status, our position in the family or society

166
00:19:24,120 --> 00:19:28,120
or our country or the world

167
00:19:28,120 --> 00:19:34,120
It includes our race and our nationality, our ethnicity, our culture

168
00:19:34,120 --> 00:19:41,120
All these things that we cling to make up much of who we are

169
00:19:41,120 --> 00:19:46,120
It includes the bed that we sleep on

170
00:19:46,120 --> 00:19:50,120
Sleep on the floor long enough

171
00:19:50,120 --> 00:19:55,120
Maybe you start to miss the bed, miss a comfortable bed, good night, sleep pool

172
00:19:55,120 --> 00:20:02,120
What I wouldn't give for a water bed or a downfield

173
00:20:02,120 --> 00:20:12,120
A mattress or a futon

174
00:20:12,120 --> 00:20:19,120
The grave comfortable chairs and seats

175
00:20:19,120 --> 00:20:27,120
The grave air conditioning, the grave

176
00:20:27,120 --> 00:20:36,120
The grave beauty, you come to our meditation center, you get an empty room for walls

177
00:20:36,120 --> 00:20:43,120
You might start to think, feel like you're going crazy because you don't have anything, any stimulation

178
00:20:43,120 --> 00:20:49,120
It's easy to distract yourself when there's beauty around you, beautiful things

179
00:20:49,120 --> 00:20:58,120
Or you can just turn on the music

180
00:20:58,120 --> 00:21:02,120
It's stressful, it's stressful to be without those things

181
00:21:02,120 --> 00:21:07,120
And so we cling, we crave them

182
00:21:07,120 --> 00:21:10,120
Just like it's stressful to be without the drugs that you're addicted to

183
00:21:10,120 --> 00:21:13,120
That's basically it, we're addicted to so many things

184
00:21:13,120 --> 00:21:19,120
We've gotten so dependent on their ability to distract ourselves

185
00:21:19,120 --> 00:21:27,120
Their ability to find enjoyment, pleasant and pleasure

186
00:21:27,120 --> 00:21:30,120
But we can't live without it for a long time

187
00:21:30,120 --> 00:21:32,120
We get upset and stressed

188
00:21:32,120 --> 00:21:36,120
That's what you feel when you come to meditate, it's quite stressful

189
00:21:36,120 --> 00:21:39,120
Not because of being alone

190
00:21:39,120 --> 00:21:46,120
Whatever a meditator tells me they're bored, I say, what's the difference between boredom and peace?

191
00:21:46,120 --> 00:21:50,120
I mentioned this recently, now I've gotten into asking this question

192
00:21:50,120 --> 00:21:53,120
It's a good question, I think, it's pertinent

193
00:21:53,120 --> 00:22:00,120
Because the only difference between boredom and peace is your attitude

194
00:22:00,120 --> 00:22:07,120
Why can't you sit alone in a room for walls, quiet?

195
00:22:07,120 --> 00:22:12,120
Why can't you, if not, for addiction?

196
00:22:12,120 --> 00:22:16,120
It's simply because we want things, we crave things

197
00:22:16,120 --> 00:22:24,120
We can't stand this random chaotic reality that we have to put up with

198
00:22:24,120 --> 00:22:27,120
That we're presented with, so we try to change it

199
00:22:27,120 --> 00:22:32,120
Okay, this is, you sit there for a while and something unpleasant comes up, okay

200
00:22:32,120 --> 00:22:36,120
When this comes up, I know what I have to do, run away

201
00:22:36,120 --> 00:22:38,120
Find something more pleasant

202
00:22:41,120 --> 00:22:47,120
And so we get a habit of running away from reality

203
00:22:47,120 --> 00:22:56,120
chasing after a specific subset of reality that's acceptable to us

204
00:22:56,120 --> 00:22:59,120
Anything else is unacceptable

205
00:22:59,120 --> 00:23:03,120
This is what causes suffering because you can't predict

206
00:23:03,120 --> 00:23:08,120
We're not able to control our experiences

207
00:23:08,120 --> 00:23:13,120
The more you try, the more you're just setting yourself up for disappointment when things change

208
00:23:18,120 --> 00:23:21,120
So that's number three, our situation

209
00:23:21,120 --> 00:23:23,120
Just stop worrying about our situation

210
00:23:23,120 --> 00:23:29,120
If you become poor, if you become a bum living on the street, you're still living

211
00:23:29,120 --> 00:23:36,120
And you're still alive

212
00:23:36,120 --> 00:23:42,120
No matter what do you, what happens to you, it's still just experience

213
00:23:42,120 --> 00:23:46,120
And so rather than worrying about your position too much

214
00:23:46,120 --> 00:23:49,120
You should be much more worried about your mind

215
00:23:49,120 --> 00:23:55,120
How you deal with situations, because you don't know what's going to happen next

216
00:23:55,120 --> 00:24:01,120
Anything can change in a minute

217
00:24:01,120 --> 00:24:04,120
That's number three, number four is

218
00:24:04,120 --> 00:24:06,120
So the first three are contentment

219
00:24:06,120 --> 00:24:12,120
It's the lineage or the heritage of the noble ones to be content with all these things

220
00:24:12,120 --> 00:24:18,120
To content with our image, content with our input

221
00:24:18,120 --> 00:24:25,120
Our food and our nourishment, and to be content with our situation

222
00:24:25,120 --> 00:24:33,120
And the fourth one is to find delight

223
00:24:33,120 --> 00:24:41,120
Let's see what the word is here

224
00:24:41,120 --> 00:24:50,120
I'm a rama, rama, rama means to enjoy

225
00:24:50,120 --> 00:24:57,120
To delight in bawana, the divide and development, mental development

226
00:24:57,120 --> 00:25:03,120
We should be delighting it, we should be very happy to be able to sit down, close your eyes

227
00:25:03,120 --> 00:25:12,120
Let me do something about this huge mass of stress and suffering that we've gotten ourselves into

228
00:25:12,120 --> 00:25:17,120
Do something about our minds, don't just live with the imperfections in your mind

229
00:25:17,120 --> 00:25:22,120
Your bad habits that cause you suffering, that cause others suffering

230
00:25:22,120 --> 00:25:27,120
Don't just live with them, learn to overcome them, learn to better yourself

231
00:25:27,120 --> 00:25:32,120
Find ways to better yourself, because what we should be doing

232
00:25:32,120 --> 00:25:49,120
Not just in development, but abandoning

233
00:25:49,120 --> 00:25:50,120
It is the two things.

234
00:25:50,120 --> 00:26:17,120
We should delight in developing, developing wholesome states, developing states of mind that are for our own benefit and for the benefit of others that make us better people, and abandoning those states that make us worse people, abandoning those states that cause suffering for others, for ourselves and for others.

235
00:26:20,120 --> 00:26:28,120
This is what comes from meditation. This is what comes from mental development.

236
00:26:28,120 --> 00:26:34,120
You develop wholesome states. You become a better person. This is what you should be focused on.

237
00:26:34,120 --> 00:26:40,120
Not being pleased or happy or peaceful.

238
00:26:40,120 --> 00:26:47,120
Everyone wants to meditate to bliss out and feel good. It's really a terrible reason to meditate.

239
00:26:47,120 --> 00:26:55,120
You meditate to better yourself, to learn, to become noble.

240
00:26:55,120 --> 00:27:12,120
You want to find true peace and happiness that comes through this, to being a better person, to purifying your mind, to purifying your mind through it.

241
00:27:12,120 --> 00:27:20,120
So becoming enlightened, enlightened to the truth of reality, simple stuff.

242
00:27:20,120 --> 00:27:33,120
Learning the truth about your own mind. What is the nature of my mind? Is it pure? Is it defiled? Is it good? Is it bad?

243
00:27:33,120 --> 00:27:49,120
Learning all the various aspects of your mind and refining them. It's a lot of work.

244
00:27:49,120 --> 00:27:55,120
You don't get a clean house from going on the vacation. The meditation isn't a vacation.

245
00:27:55,120 --> 00:28:10,120
It's about cleaning, cleaning house, removing all the dirt and defound from your own mind, so that you end up with a clean place to live.

246
00:28:10,120 --> 00:28:16,120
Abandoning those things that should be abandoning, cultivating those things that should be cultivating.

247
00:28:16,120 --> 00:28:23,120
This is the way. This is the noble heritage.

248
00:28:23,120 --> 00:28:35,120
So that's our dhamma for tonight.

249
00:28:35,120 --> 00:28:39,120
Any questions?

250
00:28:39,120 --> 00:28:42,120
We have questions one thing.

251
00:28:42,120 --> 00:28:47,120
If we are born, we are eventually caught up in Samsara and the suffering.

252
00:28:47,120 --> 00:28:58,120
So if one who has such knowledge, dhamma, shouldn't he or she be inclined not to bring life on earth, thus preventing the newborn from suffering?

253
00:28:58,120 --> 00:29:00,120
Thanks.

254
00:29:00,120 --> 00:29:07,120
We may also say new life gives opportunity to see nabana.

255
00:29:07,120 --> 00:29:13,120
Well, it's not giving new life. Whatever being that is is already in Samsara.

256
00:29:13,120 --> 00:29:19,120
You're just turning them into helping, giving them an opportunity to become a human.

257
00:29:19,120 --> 00:29:33,120
It's like you're creating a position in a company where you put on advertisement and advertise for the position.

258
00:29:33,120 --> 00:29:48,120
So, I mean, there might be a benefit to that.

259
00:29:48,120 --> 00:29:56,120
A scientific study on the meditation technique. Will you see your birthday?

260
00:29:56,120 --> 00:30:03,120
Well, I don't know what they used, but the MBSR has done some studies.

261
00:30:03,120 --> 00:30:06,120
I don't know if they specifically use our technique.

262
00:30:06,120 --> 00:30:11,120
Mahasi Sayada compiled a bunch of stuff unofficially and a lot of anecdotes, really.

263
00:30:11,120 --> 00:30:17,120
But anecdotal cases where meditators had cured themselves of various diseases.

264
00:30:17,120 --> 00:30:25,120
It's quite interesting. Certainly not scientific, but not in the modern sense, but it was certainly interesting to read.

265
00:30:25,120 --> 00:30:35,120
But if you look at the NIH or this government, US government has a page on meditation.

266
00:30:35,120 --> 00:30:41,120
So, I don't expect any of it as in depth is what we practice.

267
00:30:41,120 --> 00:30:54,120
A lot of it's watered down in this simple meditation, but they have some scientific stuff on this sort of approaching this type of meditation.

268
00:30:54,120 --> 00:31:11,120
Well, then, here is a copy of the deep digital hearing, but an image of the cat also comes into my mind after I hear the sound.

269
00:31:11,120 --> 00:31:14,120
Should I not see him seeing?

270
00:31:14,120 --> 00:31:24,120
I mean, it's not a should, it's you can, note whatever's clearest to you. If the hearing is clearest, it's a hearing. Of course, okay, a cat meowing with a meowing is gone.

271
00:31:24,120 --> 00:31:32,120
And so, then, yeah, you move on to seeing. Like, if you hear birds chirping and you both hear it, and you see in your mind an image of a bird,

272
00:31:32,120 --> 00:31:35,120
it doesn't really matter which one will stick to one.

273
00:31:35,120 --> 00:31:43,120
Because it's helps you build concentration, you stick to one, and your mind will learn to grasp things more clearly rather than jump back and forth.

274
00:31:43,120 --> 00:31:51,120
At least a little bit of concentration in that way. Of course, jumping back and forth, it's not technically wrong.

275
00:31:51,120 --> 00:32:02,120
I couldn't say seeing, seeing and going to hearing here. Just don't try to jump too much. It's not really helpful.

276
00:32:02,120 --> 00:32:10,120
Greetings, Dante. Do angels suffer? How? Is it easier to achieve liberation as a human?

277
00:32:10,120 --> 00:32:15,120
Well, yeah, angels have mental suffering. Not a lot, but they do.

278
00:32:15,120 --> 00:32:21,120
They can feel fear and other stories of angels when they realized they were about to die.

279
00:32:21,120 --> 00:32:26,120
They would get a lot of fear because they realized they hadn't done anything to deserve being an angel anymore.

280
00:32:26,120 --> 00:32:31,120
So when they die, they're going to go back to the human realm. That's clear suffering.

281
00:32:31,120 --> 00:32:37,120
They worry, they fear, they get, they even get angry. Now an angel dies if they get really angry.

282
00:32:37,120 --> 00:32:43,120
Rarely anger is one of the causes of death for an angel.

283
00:32:43,120 --> 00:32:48,120
Is it easier to achieve liberation as a human? I don't think so.

284
00:32:48,120 --> 00:32:58,120
Because if you look at the texts, there's this idea that it makes sense that there were countless angels became enlightened listening to the Buddha.

285
00:32:58,120 --> 00:33:05,120
They would come down in droves to listen to the Buddha teach and become enlightened.

286
00:33:05,120 --> 00:33:09,120
Being an angel is distracting.

287
00:33:09,120 --> 00:33:16,120
It's easier to miss Buddhism because it's very brief in the time of an angel.

288
00:33:16,120 --> 00:33:21,120
There's probably lots of angels who have just ignored the rising of the Buddha 2,500 years ago.

289
00:33:21,120 --> 00:33:30,120
It's just going to be over for them and then they'll be like, oh, a Buddha came out too late.

290
00:33:30,120 --> 00:33:36,120
But no being an angel is pretty easy to become enlightened, especially if you've practiced as a human being.

291
00:33:36,120 --> 00:33:42,120
Go be born and haven't, especially because there's lots of Buddhists up in heaven.

292
00:33:42,120 --> 00:33:50,120
Not the pinnika is up in heaven. Any sort up on a, most sort up on a, go up to heaven.

293
00:33:50,120 --> 00:33:59,120
Mostly.

294
00:33:59,120 --> 00:34:03,120
One day we're in doing walking meditation.

295
00:34:03,120 --> 00:34:09,120
Atah, atah, we harness and the table calledness can be felt on the foot.

296
00:34:09,120 --> 00:34:14,120
It can be felt on the foot.

297
00:34:14,120 --> 00:34:19,120
And then on which aspen, is it okay to stick to one, or should it amine pick it naturally?

298
00:34:19,120 --> 00:34:24,120
Yeah, don't worry about those. You'll see them as a part of the walking.

299
00:34:24,120 --> 00:34:31,120
You'll just be aware of the general sense that you're walking.

300
00:34:31,120 --> 00:34:36,120
That's why we stick for like stepping right, stepping left or lifting, placing.

301
00:34:36,120 --> 00:34:40,120
Because as you say lifting, that's what you're aware of that focuses you on the movement,

302
00:34:40,120 --> 00:34:51,120
but the movement includes the dot.

303
00:34:51,120 --> 00:34:56,120
That's it. We're reading something about, yes, it is, but I just had kind of a,

304
00:34:56,120 --> 00:34:58,120
add a question to that one.

305
00:34:58,120 --> 00:35:02,120
I remember reading something about walking meditation, where,

306
00:35:02,120 --> 00:35:07,120
the structure of the inhale, when your foot rises,

307
00:35:07,120 --> 00:35:11,120
you can feel the heat, like heat rises.

308
00:35:11,120 --> 00:35:18,120
And when it moves forward, it's like the air, and when it goes down,

309
00:35:18,120 --> 00:35:22,120
it's the water, you know, kind of the aggregates.

310
00:35:22,120 --> 00:35:25,120
Is that helpful or is that too complicated?

311
00:35:25,120 --> 00:35:29,120
No, the only point there is that you would experience those.

312
00:35:29,120 --> 00:35:32,120
You will experience those as you meditate.

313
00:35:32,120 --> 00:35:35,120
I had one teacher, and it really kind of turned me off.

314
00:35:35,120 --> 00:35:41,120
He asked me to describe walking to him.

315
00:35:41,120 --> 00:35:47,120
I think what he was getting at was, he wanted me to be able to break it up like that,

316
00:35:47,120 --> 00:35:50,120
and to, you know, say, oh, and you lift, it feels like this when you move.

317
00:35:50,120 --> 00:35:53,120
But I was like, you know, it's just walking.

318
00:35:53,120 --> 00:35:55,120
Been really madder to me.

319
00:35:55,120 --> 00:35:58,120
And I still can test that that you really have to know.

320
00:35:58,120 --> 00:36:01,120
I guess the point is, you know, are you really being mindful?

321
00:36:01,120 --> 00:36:04,120
But the question then is how mindful, you know, what does it mean to be mindful?

322
00:36:04,120 --> 00:36:07,120
Should you be aware of the particulars?

323
00:36:07,120 --> 00:36:10,120
Do you have to worry about the particulars?

324
00:36:10,120 --> 00:36:11,120
I don't know.

325
00:36:11,120 --> 00:36:14,120
For me, it's just lifting that this is moving, this is place.

326
00:36:14,120 --> 00:36:17,120
You don't have to actually realize, oh, that's the fire element.

327
00:36:17,120 --> 00:36:18,120
That's the air element.

328
00:36:18,120 --> 00:36:20,120
That's the earth element.

329
00:36:20,120 --> 00:36:22,120
Yeah, of course, it depends who you ask.

330
00:36:22,120 --> 00:36:30,120
There are teachers out there that require it, but certainly not an art in agenda and tongue's tradition.

331
00:36:30,120 --> 00:36:33,120
Thank you, Monte.

332
00:36:33,120 --> 00:36:38,120
Go to this game merit if they are in constant bliss.

333
00:36:38,120 --> 00:36:40,120
Well, bliss doesn't stop you from getting merit.

334
00:36:40,120 --> 00:36:52,120
Angels can meditate, and they can be kind to each other, and they can teach, they can listen to the dumber.

335
00:36:52,120 --> 00:36:55,120
They can be kind to each other.

336
00:36:55,120 --> 00:36:56,120
They can practice.

337
00:36:56,120 --> 00:37:00,120
So, mostly they can practice meditation, all sorts of meditation.

338
00:37:00,120 --> 00:37:04,120
Because they can sit still for a long time.

339
00:37:04,120 --> 00:37:09,120
It's true, they often bemoan not being able to offer to the Buddha.

340
00:37:09,120 --> 00:37:18,120
It's not easy for them to come down to earth and, oh, they can't just offer arms, that kind of thing.

341
00:37:18,120 --> 00:37:24,120
So, there are certain types of good things they can't do.

342
00:37:24,120 --> 00:37:43,120
It's a dropout of a question.

343
00:37:43,120 --> 00:37:44,120
It's done.

344
00:37:44,120 --> 00:37:46,120
Thank you all for coming out.

345
00:37:46,120 --> 00:37:48,120
Thanks for coming for your help.

346
00:37:48,120 --> 00:37:51,120
Have a good night, everyone.

347
00:37:51,120 --> 00:37:54,120
Hey, let me know if the video is better.

348
00:37:54,120 --> 00:37:57,120
I think people are complaining about the video being fuzzy.

349
00:37:57,120 --> 00:37:59,120
I think it should be clear.

350
00:37:59,120 --> 00:38:01,120
I'm thinking it may be clear.

351
00:38:01,120 --> 00:38:25,120
Thanks for letting me know.

