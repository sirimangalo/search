1
00:00:00,000 --> 00:00:12,460
That today is finally an English session.

2
00:00:12,460 --> 00:00:28,400
That's a night I thought I'd go over what we mean by the word meditation and a little

3
00:00:28,400 --> 00:00:41,760
bit about how we practice meditation and those of you who don't speak English, you can

4
00:00:41,760 --> 00:00:56,560
just sit in meditation or try to understand what's being said, it's a chance for us to hear.

5
00:00:56,560 --> 00:01:20,600
The word meditation, as I said before, can mean many different things to many different

6
00:01:20,600 --> 00:01:24,800
people.

7
00:01:24,800 --> 00:01:35,200
For most of us, we get a sense of meditation as a kind of altered state of mind, a heightened

8
00:01:35,200 --> 00:01:47,520
state of awareness or an alternate experience, but really the word meditation just means

9
00:01:47,520 --> 00:02:02,520
to consider, to mull over, to keep in mind, to dwell upon, to solve problems and so on.

10
00:02:02,520 --> 00:02:17,480
It means when we dwell on something, when we consider something, so it can be referring to

11
00:02:17,480 --> 00:02:23,080
the object, that we take into consideration, it can be a problem that we have that we meditate

12
00:02:23,080 --> 00:02:44,360
on, it could be an object of worship, an object of power.

13
00:02:44,360 --> 00:02:50,720
It could be many different things.

14
00:02:50,720 --> 00:02:59,200
With the understanding that this is somehow going to bring about a resolution for a

15
00:02:59,200 --> 00:03:12,480
result of some sort, of many people meditations just simply means this state of consideration

16
00:03:12,480 --> 00:03:19,440
or awareness of an object, or even without an object, simply a state where the mind is

17
00:03:19,440 --> 00:03:26,600
in an altered reality, so we understand that meditation is this practice that makes your

18
00:03:26,600 --> 00:03:30,480
mind very calm and peaceful.

19
00:03:30,480 --> 00:03:37,040
This is just sort of an idea that we've had about, we've developed about meditation.

20
00:03:37,040 --> 00:03:40,800
And for most people, it would be quite surprising for them, or it is quite surprising

21
00:03:40,800 --> 00:03:47,200
that to learn that in meditation, your mind need not be calm and peaceful all the time.

22
00:03:47,200 --> 00:04:05,440
Of course, the meditation just means to consider it to ponder, to take into one's awareness.

23
00:04:05,440 --> 00:04:10,960
Now this is very critical in the practice of repass in the meditation, because repass in

24
00:04:10,960 --> 00:04:17,480
the meditation, in the meditation to see clearly, we have to see clearly many bad things,

25
00:04:17,480 --> 00:04:23,240
especially in Buddhism, the Buddha said, we have to understand suffering, we have to

26
00:04:23,240 --> 00:04:29,280
some do fully and totally, completely understand suffering.

27
00:04:29,280 --> 00:04:33,360
We can't do that when we're in this peaceful calm state.

28
00:04:33,360 --> 00:04:39,080
Because this peaceful calm state is nothing to do with our ordinary state of awareness

29
00:04:39,080 --> 00:04:45,160
or existence.

30
00:04:45,160 --> 00:04:50,320
So in Buddhism, we separate meditation out into two general categories, and there are

31
00:04:50,320 --> 00:04:53,880
different ways of understanding these two general categories.

32
00:04:53,880 --> 00:04:58,880
One way is just simply to say, one way, one type of meditation is for the purpose and

33
00:04:58,880 --> 00:05:11,240
ultimate goal of calm or focused, controlled, contrived states of awareness, sort of like

34
00:05:11,240 --> 00:05:21,560
we call transcendental meditation, and the other is for the purpose of seeing clearly.

35
00:05:21,560 --> 00:05:30,080
And when we split them up in this way, it doesn't mean that insect meditation is going

36
00:05:30,080 --> 00:05:36,840
to not lead to peace and calm, or that the practice of calm you won't come to understand

37
00:05:36,840 --> 00:05:43,480
things, but it means that this is the purpose of the meditation.

38
00:05:43,480 --> 00:05:56,880
Another way of looking at these two types of meditation is the object that they take, because

39
00:05:56,880 --> 00:06:01,720
there are certain objects that when taken under consideration can't lead to real and true

40
00:06:01,720 --> 00:06:07,080
wisdom, real and true understanding.

41
00:06:07,080 --> 00:06:12,120
So many people would say, all meditation is the same, and all these in the same direction,

42
00:06:12,120 --> 00:06:16,280
it's either you're either meditating or you're not, there's no different types.

43
00:06:16,280 --> 00:06:24,040
But as I said, meditation, it takes an object, and the different objects do flavor the

44
00:06:24,040 --> 00:06:26,960
meditation in a different way.

45
00:06:26,960 --> 00:06:31,800
Why are there certain objects that don't lead to wisdom, don't lead to insight, is because

46
00:06:31,800 --> 00:06:35,680
there are certain objects in meditation, many objects, and there's an infinite number

47
00:06:35,680 --> 00:06:39,240
of objects in meditation that you can take that are not real.

48
00:06:39,240 --> 00:06:44,480
There's something that you create in your mind.

49
00:06:44,480 --> 00:06:52,120
Because you create them in your mind, and they can't give you any understanding of reality,

50
00:06:52,120 --> 00:06:59,600
just as our dreams or our fantasies, no matter how much we dream in fantasy, fantasize,

51
00:06:59,600 --> 00:07:07,720
it's not reality, and it can't help us in any way in our life.

52
00:07:07,720 --> 00:07:19,320
So when we focus on a light, and we focus on a ball of light in their minds or a color,

53
00:07:19,320 --> 00:07:26,200
we focus on object or a deity or so on, these are all things that we create in our minds,

54
00:07:26,200 --> 00:07:32,080
even God, because even if they were a God, we wouldn't have any clue what the God would

55
00:07:32,080 --> 00:07:37,560
be like, so we just create this idea in our minds.

56
00:07:37,560 --> 00:07:44,080
And this leads to great peace and great happiness, and it leads many people to take religion

57
00:07:44,080 --> 00:07:51,200
very seriously, because they interpret their experience to be a religious experience, and

58
00:07:51,200 --> 00:07:58,280
an experience of God, or an experience of oneness with everything, or so on, experience

59
00:07:58,280 --> 00:08:05,280
of enlightenment, and yet these experiences are temporary, there's something that comes

60
00:08:05,280 --> 00:08:13,680
and goes, and because there's no reflection on the reality of the situation reflecting

61
00:08:13,680 --> 00:08:24,000
on the concept that we've created in our minds, then there's no, not necessarily any

62
00:08:24,000 --> 00:08:25,000
gaining of insight.

63
00:08:25,000 --> 00:08:29,440
Now, it's certainly possible to look at, say, the states of calm and come to see that

64
00:08:29,440 --> 00:08:35,120
they're impermanent, and you come to realize that you come to realize something about

65
00:08:35,120 --> 00:08:42,640
these states, and then insight can begin there from, but it's important to understand

66
00:08:42,640 --> 00:08:47,440
that there are indeed meditations that won't lead you to come to understand reality because

67
00:08:47,440 --> 00:08:53,720
they are not real, so transcendental meditation might be very nice, but it doesn't tell

68
00:08:53,720 --> 00:09:04,960
you much about how to deal with the everyday situations that we're faced with.

69
00:09:04,960 --> 00:09:10,160
And this is how many people approach problems, of course, when we have a problem with

70
00:09:10,160 --> 00:09:17,760
a person in a place where we try to escape that person, that place, or that thing right

71
00:09:17,760 --> 00:09:19,480
away.

72
00:09:19,480 --> 00:09:23,200
And so this kind of meditation is actually quite appealing to people, because they think

73
00:09:23,200 --> 00:09:28,560
the way to overcome something is to escape it, just to run away from it, just to push

74
00:09:28,560 --> 00:09:36,800
it away, and there's no one out there telling us that we should actually, you know, face

75
00:09:36,800 --> 00:09:44,000
and come to understand and learn something about the unpleasant situation or unpleasant

76
00:09:44,000 --> 00:09:50,200
experience, the problem, and this is where insight meditation comes in.

77
00:09:50,200 --> 00:09:52,640
This is where we really understand the word meditation.

78
00:09:52,640 --> 00:09:55,000
When you have a problem, you should meditate on it.

79
00:09:55,000 --> 00:10:00,920
This is something that they would say in worldly circles.

80
00:10:00,920 --> 00:10:05,120
This is where I believe the word meditation first came into use.

81
00:10:05,120 --> 00:10:11,120
It's not a Buddhist word, it's a word that we use to convey the meaning of kamatana,

82
00:10:11,120 --> 00:10:16,480
which really doesn't mean meditation, or Bhavana, which also doesn't mean meditation.

83
00:10:16,480 --> 00:10:30,600
Kamatana means sort of like a fixed story, firmly established, or specific tasks that

84
00:10:30,600 --> 00:10:35,920
we do, means localizing your activities.

85
00:10:35,920 --> 00:10:41,680
Stop doing so many different things, and then our actions are fixed and focused upon

86
00:10:41,680 --> 00:10:46,920
a certain object.

87
00:10:46,920 --> 00:10:58,000
So it has more to do with our acts and our behavior than it has to do with any consideration.

88
00:10:58,000 --> 00:11:04,560
There are words that mean consideration, but they're not used in this sense.

89
00:11:04,560 --> 00:11:14,000
So the word meditation, it would mean to mull over a problem, so I understand it.

90
00:11:14,000 --> 00:11:20,720
This is really very close to what is meant by insight meditation, because we're going

91
00:11:20,720 --> 00:11:24,960
to be looking at the very problems and difficulties and issues that we would normally want

92
00:11:24,960 --> 00:11:28,000
to run away from.

93
00:11:28,000 --> 00:11:33,640
We have to look at all of the aspects of the problem, and we have to come to see it clearly.

94
00:11:33,640 --> 00:11:44,120
This is why this type of meditation doesn't generally guarantee calm states of mind.

95
00:11:44,120 --> 00:11:49,440
You see, it's after a whole different type of calm, which is this ability to ride the

96
00:11:49,440 --> 00:11:57,040
waves, ability to go with the flow, so that when these unpleasant situations come about,

97
00:11:57,040 --> 00:12:00,200
the mind is not displeased.

98
00:12:00,200 --> 00:12:08,160
It's like there's this analogy that there was Shanti Deva, if I remember correctly, if

99
00:12:08,160 --> 00:12:16,120
he said, if the world is covered in glass, you have two choices.

100
00:12:16,120 --> 00:12:23,080
You can cover it over with leather, or you can wear sandals.

101
00:12:23,080 --> 00:12:31,480
In a way, this is an app comparison with these two types of meditations.

102
00:12:31,480 --> 00:12:38,360
When we have unpleasant situations, we can cover them over and change them and have a

103
00:12:38,360 --> 00:12:47,640
nice situation, or we can change our own minds so that the situations don't bother us.

104
00:12:47,640 --> 00:12:53,640
Instead of having to work hard to make, always be a pleasant situation, always be a happy

105
00:12:53,640 --> 00:12:57,800
situation, a happy feeling in the mind.

106
00:12:57,800 --> 00:13:05,480
When said come to be content with whatever feeling would ever experience comes to us.

107
00:13:05,480 --> 00:13:06,880
This is like wearing sandals.

108
00:13:06,880 --> 00:13:13,600
We're able to deal with every situation, because our mind is, well, it really means

109
00:13:13,600 --> 00:13:21,040
we have wisdom, we have understanding, we understand that everything is simply arising and

110
00:13:21,040 --> 00:13:22,040
ceasing.

111
00:13:22,040 --> 00:13:29,280
I'm getting caught up and it is not going to lead you to happiness, getting attached

112
00:13:29,280 --> 00:13:35,640
and excited about it is not going to lead you to peace, it's only going to lead you to

113
00:13:35,640 --> 00:13:41,440
more bother because it's not stable, it's not sure, and it's not under your control.

114
00:13:41,440 --> 00:13:48,440
So in Buddhism, we really do focus on this second type of meditation.

115
00:13:48,440 --> 00:13:53,680
It doesn't mean that we won't feel calm, it doesn't mean that there's no tranquility

116
00:13:53,680 --> 00:13:58,600
involved in the practice, it just means that's not our goal, even tranquility, instead

117
00:13:58,600 --> 00:14:02,440
of looking at, say, an object that would bring us tranquility, we're going to look at the

118
00:14:02,440 --> 00:14:07,800
tranquility itself and it might come up, it should come up, it's part of the spectrum of

119
00:14:07,800 --> 00:14:08,800
experience.

120
00:14:08,800 --> 00:14:15,120
But when we feel tranquil, we're going to look at the tranquility, we're going to consider

121
00:14:15,120 --> 00:14:24,280
it, we're going to think over on it, upon it, and we're going to be very specific

122
00:14:24,280 --> 00:14:28,840
about this because we're not concerned about the very details of it, we're just concerned

123
00:14:28,840 --> 00:14:32,960
about what it is, what does it mean to say that there's pain, what does it mean to say

124
00:14:32,960 --> 00:14:36,920
that there's happiness, what does it mean to say that there's anger, what does it mean

125
00:14:36,920 --> 00:14:39,960
to say that there's greed, and so we're going to just look at these and understand

126
00:14:39,960 --> 00:14:47,120
them, feel what they are, when we feel happy, we just say to ourselves, happy, happy,

127
00:14:47,120 --> 00:14:54,040
just meditating on this happiness, when we feel pain, we're going to meditate on the

128
00:14:54,040 --> 00:14:59,640
pain, saying to ourselves, pain, pain, and our mind would just no pain, it would just

129
00:14:59,640 --> 00:15:05,640
no happiness, it would just be aware of that one object, there's no room, this is where

130
00:15:05,640 --> 00:15:10,840
we have this nice word called mindfulness, it's kind of interesting how these words that

131
00:15:10,840 --> 00:15:15,400
were totally used for something completely different actually fit right well into what

132
00:15:15,400 --> 00:15:18,680
is thought, even though they don't translate the words that they're supposed to translate

133
00:15:18,680 --> 00:15:27,000
directly, meditation isn't a direct translation for any poly word that we normally use,

134
00:15:27,000 --> 00:15:35,120
that we never translate that way, and mindfulness also is not, but mindfulness, having

135
00:15:35,120 --> 00:15:42,320
this full mind or being fully aware, or keeping fully in mind, or having a full mind

136
00:15:42,320 --> 00:15:50,160
in this one sense, out of the object, so when we say to ourselves, pain, pain, our mind

137
00:15:50,160 --> 00:15:56,000
is full, this is why it's kind of an interesting word, our mind is full, and we're fully

138
00:15:56,000 --> 00:16:04,240
aware of this object, and nothing else, so there's no room for the normal diversification,

139
00:16:04,240 --> 00:16:13,560
making more of it than it actually is, this is bad pain, this is my pain, this is a problem,

140
00:16:13,560 --> 00:16:19,000
when situations arise we are able to break them down to their components, we see something

141
00:16:19,000 --> 00:16:23,520
we don't like, it's just seeing, we hear something we don't like, it's just hearing,

142
00:16:23,520 --> 00:16:30,080
we smell something we don't like and so on, we have no room for anything but the

143
00:16:30,080 --> 00:16:38,400
direct experience, this is what it means to be mindful, in this sense, the original word

144
00:16:38,400 --> 00:16:42,720
of course didn't mean that but it's, this is something that the Buddha was very good at

145
00:16:42,720 --> 00:16:49,200
and I think he'd very much appreciate using, giving words new meaning, because language

146
00:16:49,200 --> 00:16:57,040
is such a cultural thing, we've built up around our ways of looking at the world that

147
00:16:57,040 --> 00:17:01,360
there ends up being no good word for the quality that you're trying to describe and you

148
00:17:01,360 --> 00:17:09,040
have to make do, and there are many words that are used to mean ultimates that we have

149
00:17:09,040 --> 00:17:14,400
to remake, like how the Buddha changed the word dharma because it was such an important

150
00:17:14,400 --> 00:17:20,000
word of karma because it was another important word, and so we had to explain to

151
00:17:20,000 --> 00:17:26,400
be what was the true dharma, what was the true karma, brahmana, samana, all these words

152
00:17:26,400 --> 00:17:37,280
he changed, they were giving this word a new meaning, mindfulness means your mind is

153
00:17:37,280 --> 00:17:49,120
full or you're fully encompassed by this one object and because the object is impermanent

154
00:17:49,120 --> 00:17:54,800
it's coming and going it's changing, you're going to see that and you're going to say that

155
00:17:54,800 --> 00:17:59,520
you're going to realize to yourself that it's not sad, that whatever you thought about this

156
00:18:00,320 --> 00:18:07,920
that you were going to change it or make it good or make it perfect was, was an illusion, was

157
00:18:07,920 --> 00:18:16,320
a delusion and you're going to let go of any idea that it's you or yours

158
00:18:16,320 --> 00:18:24,000
and we'll come to see that it really every experience is just a part of nature, a part of the

159
00:18:24,000 --> 00:18:31,840
reality of the universe and there's no eye involved in seeing or hearing or smelling or tasting

160
00:18:31,840 --> 00:18:38,160
or feeling or thinking all these things just come and go and come and go and then the mind

161
00:18:38,160 --> 00:18:47,200
will begin to let go and loosen up, ease up until it finally really lets go and when the mind

162
00:18:47,200 --> 00:18:53,440
really lets go this is what the Buddha called nirvana, nibvana, this is what we're aiming for

163
00:18:53,440 --> 00:18:58,960
in the practice, it's easing up and loosening up just coming to see things for what they are when

164
00:18:58,960 --> 00:19:04,800
they're seeing it's only seeing and there's hearing it's only hearing, smelling tasting

165
00:19:04,800 --> 00:19:12,640
feeling and thinking it's only an experience and the mind loosens up all of these attachments

166
00:19:12,640 --> 00:19:18,160
and addictions, that's all they are, misunderstanding of the reality of our experience

167
00:19:20,880 --> 00:19:31,200
and when we overcome this, the mind lets go and the mind lets go the mind is free, the mind is

168
00:19:31,200 --> 00:19:44,000
free then you can say to yourself I'm free, nothing left to be done here, no more, no more

169
00:19:44,000 --> 00:19:48,640
running around and chasing after things that are not going to bring us happiness or peace

170
00:19:50,320 --> 00:19:55,120
this is the true freedom that we're looking for, this is what we really mean by meditation

171
00:19:55,120 --> 00:20:07,760
in the Buddhist sense, this consideration of reality or reflection on reality as it is

172
00:20:08,320 --> 00:20:15,360
until we finally see it just simply for what it is, we're not creating anything, it's not an

173
00:20:15,360 --> 00:20:23,360
altered state of mind, in fact it's the unaltered state of mind, the Lord Buddha said,

174
00:20:23,360 --> 00:20:36,160
I'm going to say, I'm going to say this here, mind is radiant, the mind is originally radiant,

175
00:20:37,920 --> 00:20:43,440
the unaltered state of mind is perfect, just knowing that we're sitting, knowing that we're walking

176
00:20:43,440 --> 00:20:55,360
or so on or even the non-arising of any experience, it's perfect, it's just that when we experience

177
00:20:55,360 --> 00:21:03,040
then there gives rise to defilements, there are these Akandukakinesa, these defilements that

178
00:21:03,760 --> 00:21:13,360
are guests, they are visitors, they come in on occasion from time to time, this is what

179
00:21:13,360 --> 00:21:20,080
makes it, makes the mind defile, just like a polished gem you can cover it up with dirt,

180
00:21:21,120 --> 00:21:29,760
you know it looks ugly, but when you wash it, when you clean it, it gets back to its unaltered

181
00:21:29,760 --> 00:21:38,800
form, the mind is very much the same, that actually the mind is perfect, there's no defilement

182
00:21:38,800 --> 00:21:47,040
in the mind, it's a visiting state, defilement is something that comes into the mind, this is just

183
00:21:47,040 --> 00:21:55,280
one way of talking, actually an ultimate reality they say, the mind would be defiled, one mind state

184
00:21:55,280 --> 00:22:02,320
or one moment of defiled, but here we're talking in a general sense, our mind, the mind that

185
00:22:02,320 --> 00:22:09,520
we call us, so we call I, it's actually fine, when you know things, when you're walking around,

186
00:22:10,480 --> 00:22:17,360
when you're talking, when you're doing whatever, it's just that from time to time we take things

187
00:22:17,360 --> 00:22:24,080
seriously, we take things more seriously than we should, we make more of some of the things

188
00:22:24,080 --> 00:22:29,120
than they really are, in fact, through the most of the time we do this, because most of the time

189
00:22:29,120 --> 00:22:38,240
we're in this semi-deluded state, we're not really aware of reality, so there should be a

190
00:22:38,240 --> 00:22:45,680
sort of a good guideline for us of what we're aiming for, it's just to see why is it that we suffer,

191
00:22:45,680 --> 00:22:51,200
why is it that we give rise to defilement, it's no other reason that we don't see things as they are,

192
00:22:52,320 --> 00:22:58,400
so here we are, we practice meditation is to consider these things and come to see them clear,

193
00:22:58,400 --> 00:23:03,760
and then we saw them before, and it doesn't mean that the defilements won't arise, but it means it

194
00:23:03,760 --> 00:23:07,680
will come to understand them more and more until they become weaker and weaker and they don't

195
00:23:07,680 --> 00:23:13,120
know the hold of us, then eventually the defilements will not arise, because we'll be able to see

196
00:23:13,120 --> 00:23:23,120
things perfectly crystal clear, so this is the teaching that I would like to offer tonight,

197
00:23:23,120 --> 00:23:29,360
kind of a sort of a warm up-to-meditation practice, so thanks for coming, that's all,

198
00:23:29,360 --> 00:23:59,200
for today.

