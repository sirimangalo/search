1
00:00:00,000 --> 00:00:28,240
Hello everyone. Welcome to our weekly live Saturday broadcast. We've missed a couple of sessions

2
00:00:28,240 --> 00:00:33,280
recently where our intention is always to do it every week, but we've had some technical difficulties.

3
00:00:33,840 --> 00:00:41,760
What should get better? Hopefully. Now that we've moved to our new center. So as of yesterday,

4
00:00:42,880 --> 00:00:50,720
we are in one location, no more using a rental house to accommodate meditators. We have a one acre

5
00:00:50,720 --> 00:01:01,040
property with trees and a house for the meditators and a big hall permitted big meditation hall in

6
00:01:01,040 --> 00:01:10,320
the back. It's not finished yet, but it's ready for enough for people to move in. There's hot water,

7
00:01:10,320 --> 00:01:22,640
there's bathrooms and we're up and running. Our grand opening is going to be October 31st,

8
00:01:23,920 --> 00:01:30,720
which is a Monday. Specifically because we're inviting a bunch of monks and they have a hard time

9
00:01:30,720 --> 00:01:36,800
coming on the weekends, so I told them we do it on a Monday because we don't have any expected

10
00:01:36,800 --> 00:01:48,480
visitors anyway. That and also there's a group of people from Guelph who have a Monday on a restaurant

11
00:01:48,480 --> 00:01:54,160
and Monday their restaurant is closed, so they will be feeding everyone for the opening grand

12
00:01:54,160 --> 00:02:02,240
opening. Anyway, so the grand opening will be October 31st in the morning, but we are open and

13
00:02:02,240 --> 00:02:07,680
already receiving meditators. We have people lined up to come for October through November,

14
00:02:08,640 --> 00:02:15,680
even some people in November already I think. But because it's because we have this location now,

15
00:02:15,680 --> 00:02:23,600
we can hopefully move this broadcast to local once we get the internet set up properly.

16
00:02:25,280 --> 00:02:31,520
We can have the crew here take over from Chris who has done a great job doing it remotely from

17
00:02:31,520 --> 00:02:37,200
California. So Chris and I are not in the same location. We're doing this over the internet and that

18
00:02:37,200 --> 00:02:45,360
seems to cause some level of challenge. Okay, so as usual at the quarter after the hour,

19
00:02:45,360 --> 00:02:50,960
we'll start. So until quarter after the hour, we will do silent meditation. You can do

20
00:02:50,960 --> 00:02:55,920
meditation as you like walking, sitting, walking and sitting, lying if you can't walk or sit,

21
00:02:55,920 --> 00:03:04,720
standing if you can't walk, sit or lie down, whatever is suitable for you until the quarter

22
00:03:04,720 --> 00:03:26,560
after the hour.

23
00:15:34,720 --> 00:16:04,000
Okay, we're back. We're ready to answer questions. If you have any questions, you can post

24
00:16:04,000 --> 00:16:08,640
to continue to post them in chat. If you don't have questions, you can just sit back and

25
00:16:09,600 --> 00:16:15,840
practice mindfulness with us. Thank you, Bantu. We do have questions.

26
00:16:18,400 --> 00:16:24,240
When walking while very anxious or stressed and when it's not feasible to stop walking and

27
00:16:24,240 --> 00:16:35,440
note the stress, what's best to note? The walking or the emotions? Usually you want to notice any

28
00:16:35,440 --> 00:16:43,200
emotions you have first above anything else. They're just such a problem. I mean, they're there.

29
00:16:43,920 --> 00:16:51,280
Problems may be too strong over there, such an impactful state of mind. They are the

30
00:16:51,280 --> 00:17:00,800
karmically potent mind state, so anxiety is karmically potent and leads to suffering as a result,

31
00:17:00,800 --> 00:17:04,720
so it's important that you know that it gets in the way of your practice, it gets in the way of

32
00:17:04,720 --> 00:17:12,960
clarity, it gets in the way of tranquility. It's called a hindrance, so it's best to focus your attention

33
00:17:12,960 --> 00:17:19,280
on that and try to become more familiar with it and understand it better so that it no longer

34
00:17:19,280 --> 00:17:22,080
consumes you.

35
00:17:26,000 --> 00:17:31,520
When walking in the street, I was instructed to note the walking and also to note all other

36
00:17:31,520 --> 00:17:39,040
predominant phenomena like thinking, seeing, drowsy, etc. Why are the rules different when doing

37
00:17:39,040 --> 00:17:44,880
formal walking meditation practice where you are supposed to ignore all the other phenomenon

38
00:17:44,880 --> 00:17:49,840
if you decide to keep walking or to actually stop to note the other phenomenon,

39
00:17:50,480 --> 00:17:55,760
and why isn't the street walking mindfulness described above considered true mindfulness?

40
00:17:56,400 --> 00:17:58,800
Can my mindfulness still improve from doing it?

41
00:18:02,000 --> 00:18:09,040
So there are no rules about this. I think you, I'm not sure who you learned this from,

42
00:18:09,040 --> 00:18:15,360
if it was me or someone else, but sometimes this is a common thing it seems where

43
00:18:17,280 --> 00:18:20,800
it's like a game of telephone. We say we give some instruction in the meditator,

44
00:18:20,800 --> 00:18:26,720
interprets it, and interprets it in a way that wasn't expressed in the instruction.

45
00:18:31,120 --> 00:18:35,920
Just a few things about this, of course, a very big question, but that's the first thing

46
00:18:35,920 --> 00:18:41,600
is that it's not rules. We suggest meditators during formal meditation to consider

47
00:18:42,880 --> 00:18:49,200
not stopping. So we say you don't have to stop for every little thought. You can just

48
00:18:49,200 --> 00:18:54,400
bring your mind back to the foot and keep walking otherwise you'll never walk. One reason is because

49
00:18:54,400 --> 00:18:59,360
you're walking quite slowly. Another reason is that it's formal meditation.

50
00:18:59,360 --> 00:19:07,600
So there's two answers to the Y1 because we told you to do it that way,

51
00:19:09,360 --> 00:19:13,600
and that's important because it's the questions of why do we do things this way or that way?

52
00:19:14,240 --> 00:19:22,720
In general, can be problematic for focusing. If you're always questioning why does the teacher

53
00:19:22,720 --> 00:19:26,960
tell you to do it this way or that way, you're not actually focused on the present moment.

54
00:19:26,960 --> 00:19:33,040
It's best just to do it the way the teacher explains it, unless in certain cases you have real

55
00:19:33,040 --> 00:19:39,520
issues with it, like it's causing you lots of problems. And in the case where it does seem wrong,

56
00:19:39,520 --> 00:19:42,720
for example, of course, if a teacher teaches you something that seems wrong,

57
00:19:44,160 --> 00:19:49,360
and you consider carefully that you really think it might be a problem, then okay, it's fine to ask.

58
00:19:49,360 --> 00:19:55,360
But generally speaking, asking why is not very helpful best to just because that's the way we

59
00:19:55,360 --> 00:20:02,480
told you to do it. But to actually answer the question, because of the difference in formality,

60
00:20:03,280 --> 00:20:11,840
it's not expected that during the day you are very formally accurate and precise in your mindfulness,

61
00:20:11,840 --> 00:20:18,080
especially when you're not doing formal meditation. So really just try to note whatever you can when

62
00:20:18,080 --> 00:20:23,520
you're walking down the street. You can just focus on walking. But when something else comes up,

63
00:20:23,520 --> 00:20:27,440
yeah, just try to note it. You don't have to stop walking because that would be ridiculous. Maybe

64
00:20:27,440 --> 00:20:31,760
you're walking down the street and there's other people on the street. You can't just stop and

65
00:20:31,760 --> 00:20:40,320
note you can. But it's okay to just make do and note what you can when you can. So this is a common

66
00:20:40,320 --> 00:20:44,720
thinking that there are rules and it has to be this way or it has to be that way in trying to figure

67
00:20:44,720 --> 00:20:49,520
out why because it makes no sense that it should have to be one way or another. They're not rules,

68
00:20:49,520 --> 00:20:55,120
they're guidelines and it's just pretty reasonable. I don't think this is an unreasonable

69
00:20:55,120 --> 00:21:02,240
suggestion. Can your mindfulness still improve? Yeah, your mindfulness can be improved. I don't

70
00:21:02,240 --> 00:21:07,600
know that anyone I've ever said that street walking mindfulness is not true mindfulness.

71
00:21:07,600 --> 00:21:11,120
Mindfulness is mindfulness. It either is or it isn't. There's no true or fake.

72
00:21:11,120 --> 00:21:19,760
No, it certainly is true mindfulness. It's not formal practice as well.

73
00:21:26,080 --> 00:21:32,960
When noting should one use a verb or noun, for example, sometimes when noting thinking,

74
00:21:32,960 --> 00:21:38,160
it's like I'm mindlessly just noted and it may continue. But when I note thought,

75
00:21:38,160 --> 00:21:47,280
I recognize that there is a just thought. That's not an important distinction. It can be

76
00:21:47,280 --> 00:21:56,880
either a verb or a noun. There's no problem with either. And I would caution against fixating too much

77
00:21:56,880 --> 00:22:02,080
on one or the other like one is better than the other. It's kind of missing the point of

78
00:22:02,080 --> 00:22:07,360
fixating on words, obsessing over which words you use is a real dead end. It doesn't lead to any

79
00:22:07,360 --> 00:22:14,720
thinking. There's no benefit to finding quote unquote the right word. It's not really supposed

80
00:22:14,720 --> 00:22:20,320
to be rocket science or linguistics or something. Use a word that encompasses the experience.

81
00:22:23,600 --> 00:22:28,160
Like in English, we say rising and falling, which is kind of ridiculous. The stomach doesn't

82
00:22:28,160 --> 00:22:33,600
rise or fall. It's just an English expression. One that makes it hard to translate it into other

83
00:22:33,600 --> 00:22:38,400
languages, other other people, non native English speakers are confused as to what you mean by

84
00:22:38,400 --> 00:22:44,160
rising and falling. So, you know, if English can get away with something so imprecise, it's not a

85
00:22:44,160 --> 00:22:48,320
problem. It's just a little weird and it's a problem for non native English speakers.

86
00:22:49,120 --> 00:22:54,320
So words are not a big deal. They're just a tool.

87
00:22:54,320 --> 00:23:04,240
During meditation, I sometimes have feelings that I'm not sure how to describe and give a

88
00:23:04,240 --> 00:23:10,160
label to. I've just been noting them as feeling as I become aware of them. Is this okay?

89
00:23:12,240 --> 00:23:18,960
So, if it's a physical sensation, that's when I would recommend noting feeling. For an emotion,

90
00:23:18,960 --> 00:23:25,520
I would recommend trying to understand exactly, but generally what it is. Usually it's

91
00:23:26,080 --> 00:23:30,960
some kind of liking or disliking. I mean, it generally falls into one of the categories of the

92
00:23:30,960 --> 00:23:36,800
hindrances. Whether liking or disliking or else it's worry or anxiety or restlessness.

93
00:23:38,160 --> 00:23:43,120
If you absolutely can't identify it, then it's okay to note feeling. It's not wrong. It's just,

94
00:23:43,760 --> 00:23:48,800
I would recommend trying to be a little more precise than that because all the hindrances are

95
00:23:48,800 --> 00:23:59,280
vastly different. When I note, I become relaxed and somewhat enjoy it sometimes. And I'm not stressed

96
00:23:59,280 --> 00:24:04,160
as if there is a distance between the emotions or experience and witnessing.

97
00:24:05,440 --> 00:24:06,320
Is this possible?

98
00:24:09,920 --> 00:24:16,720
Well, anytime you say suggest as if there's that's not real, right? So asking if it's

99
00:24:16,720 --> 00:24:24,640
possible doesn't make any sense. It's as if there is a distance. I mean, the truth is you're not

100
00:24:24,640 --> 00:24:32,000
stressed. That's all. If you have a perception of some kind of distance between something and

101
00:24:32,000 --> 00:24:39,760
something else, then that's just a perception. You can just note it as that. When you're relaxed,

102
00:24:39,760 --> 00:24:48,080
you should note that. When you enjoy it, you should note liking. But it's understand that there's

103
00:24:48,080 --> 00:24:53,440
a difference between experiencing and interpreting. So you're saying that there's a distance

104
00:24:53,440 --> 00:24:57,680
between it's just an interpretation that may be based on some kind of perception of distance,

105
00:24:57,680 --> 00:24:59,200
which is also just a perception.

106
00:24:59,200 --> 00:25:12,640
When a question arises in relation to Buddhism and the practice, should one note it and leave it,

107
00:25:12,640 --> 00:25:18,880
or should one make an effort to find the answer, even though the questions seem like distractions

108
00:25:18,880 --> 00:25:19,840
or excuses?

109
00:25:19,840 --> 00:25:24,640
Well, they seem like distractions or excuses and it's best to just note them.

110
00:25:24,640 --> 00:25:31,120
If it seems important, you can write it down. I mean, if you're talking about

111
00:25:31,120 --> 00:25:35,920
during a meditation, formal meditation practice, you can maybe just write it down,

112
00:25:35,920 --> 00:25:39,040
get in that habit, writing it down to look up later.

113
00:25:46,560 --> 00:25:51,680
Sometimes in deep meditation, I get a feeling of being sucked in by emptiness.

114
00:25:51,680 --> 00:25:58,560
It's such a scary and strong experience. It feels like dying, that I can't even note it. Any advice?

115
00:26:00,320 --> 00:26:06,000
So, again, this difference between experience and perception, you get a feeling that's

116
00:26:06,000 --> 00:26:12,800
experience. Being sucked in by emptiness is all just interpretation. You don't get a feeling of

117
00:26:12,800 --> 00:26:17,520
being sucked in. You may get a tugging feeling, which would be the wind element or the

118
00:26:17,520 --> 00:26:23,600
wild that to, which is the pressure. So, there would be some kind of pushing or can be pulling

119
00:26:23,600 --> 00:26:29,600
but attention of sorts. So, you get a tense feeling and that's just a sensation.

120
00:26:30,960 --> 00:26:36,880
Thinking of a disemptiness or whatever. I mean, honestly, I may be misinterpreting what you're

121
00:26:36,880 --> 00:26:42,640
feeling, but you have some kind of feeling and it's not being sucked in by emptiness. It may be

122
00:26:42,640 --> 00:26:53,920
some kind of pulling feeling mentally or physically. And it can be strong. That can be an accurate

123
00:26:53,920 --> 00:27:00,080
description of certain experiences in the sense that they are very intense, very strong. It can

124
00:27:00,080 --> 00:27:05,920
be a strength to them. But experiences aren't scary. So, that's your perception of it. That's

125
00:27:05,920 --> 00:27:12,480
your reaction to it. You react to it. You perceive it as something scary, meaning you're

126
00:27:12,480 --> 00:27:17,440
afraid of it and fear arises. I mean, the ultimate reality is there's the experience. There's

127
00:27:17,440 --> 00:27:22,560
then there's the interpretation, then there's the fear. So, fear is distinct from the experience

128
00:27:22,560 --> 00:27:28,640
itself. Experiences are not scary. Are interpretation triggers theorizing of fear,

129
00:27:30,720 --> 00:27:35,600
which can be associated with perceptions that you might be dying or that something is wrong,

130
00:27:35,600 --> 00:27:45,040
that something is strange. Experiences that are stranger or unexpected or novel, new is a sign

131
00:27:45,040 --> 00:27:53,760
of impermanence. And more clearly, it's a broadening of your horizons. It's an opening up your mind

132
00:27:53,760 --> 00:28:00,400
to the potential for the unexpected. It's a becoming accustomed to life being unpredictable,

133
00:28:00,400 --> 00:28:07,120
which helps you to let go. And permanence is important because it frees us from the expectations

134
00:28:07,120 --> 00:28:18,320
and the need for stability. I can't even note it is often caused by trying to note the wrong

135
00:28:18,320 --> 00:28:23,520
thing. Like in this case, you're afraid and the fear is preventing you from noting the other thing.

136
00:28:23,520 --> 00:28:28,000
What you should be doing, of course, is noting the fear. And usually the reason why you can't

137
00:28:28,000 --> 00:28:33,040
notice because you're not noting what is directly in front of you, for instance, maybe in this case,

138
00:28:33,040 --> 00:28:33,520
the fear.

139
00:28:39,920 --> 00:28:45,280
How does one deal with the perspective that there are other sentient beings who are suffering

140
00:28:45,280 --> 00:28:51,440
and leaving them behind on the path of Nirvana is unbearable? Is this just a perspective,

141
00:28:51,440 --> 00:28:59,200
or is there validity to it?

142
00:28:59,200 --> 00:29:10,720
Well, calling it unbearable, I guess, is an interpretation perspective. So the thought, I guess,

143
00:29:10,720 --> 00:29:16,560
will arise that there are other sentient beings who are suffering and that I am leaving them

144
00:29:16,560 --> 00:29:24,160
behind. So you have to appreciate what is meant by the path to Nirvana. The path to Nirvana

145
00:29:24,160 --> 00:29:30,320
is about letting go. So what you're talking about is a holding on. You're concerned,

146
00:29:30,320 --> 00:29:38,480
you're attached to other people's well-being. And so there's never going to be a conflict here

147
00:29:38,480 --> 00:29:48,480
where you go on, even though you are attached to other people's welfare. Nirvana is freedom

148
00:29:48,480 --> 00:30:00,640
from attachment. So when you finally let go. So the path itself is not about deciding to let go

149
00:30:00,640 --> 00:30:06,320
or deciding to give up or deciding to abandon. The path is about seeing clearly.

150
00:30:06,320 --> 00:30:12,400
So the nature of the universe, the nature of experience is such that when you see clearly you let

151
00:30:12,400 --> 00:30:20,160
go. So these kind of philosophical debates don't even enter into it. It's just reality. Reality is

152
00:30:20,160 --> 00:30:27,520
such that seeing clearly causes you to let go. In other words, there is no benefit. There is no reason,

153
00:30:27,520 --> 00:30:36,160
there is no logic behind holding on to anything. It's just intrinsically wrong. And we know this

154
00:30:36,160 --> 00:30:42,160
because we see it. When you see it through the practice, no one can convince you otherwise. It's just

155
00:30:42,160 --> 00:30:48,480
seen as that it becomes clear that that's the truth. So the reason why you're not seeing it as the

156
00:30:48,480 --> 00:30:53,680
truth is simply because of lack of enlightenment. Once you become enlightened and you will see that

157
00:30:53,680 --> 00:30:58,480
nothing is worth clinging to, you will see that for yourself. It won't be because I convinced you

158
00:30:58,480 --> 00:31:03,360
or anyone convinced you or because you convinced yourself. It will because you come to see that

159
00:31:03,360 --> 00:31:10,400
actually that's the truth. And then there is no dilemma about letting go of someone or abandoning

160
00:31:10,400 --> 00:31:10,880
something.

161
00:31:17,440 --> 00:31:23,040
Almost all of my relationships are messed up lately because I've been mentally weak from life

162
00:31:23,040 --> 00:31:29,120
challenges. There's a chance it's going to hurt me financially. What should I focus on to get through

163
00:31:29,120 --> 00:31:38,240
this? Well, relationships are not real. They're just narratives. I mean, something to be clear,

164
00:31:38,240 --> 00:31:44,880
not something not being real, isn't meant to trivialize it. So it's just conceptual and that

165
00:31:44,880 --> 00:31:50,400
doesn't mean it's meaningless. It's just important that you put it in perspective and that

166
00:31:50,400 --> 00:32:02,160
it doesn't actually have any ultimate reality, meaning that there's something bigger going on in

167
00:32:02,160 --> 00:32:08,560
the background. What's going on in the background is your reactions and your judgments and your

168
00:32:08,560 --> 00:32:18,560
interpretations of things. So relationships being messed up is really just a perception of things

169
00:32:18,560 --> 00:32:26,320
being messed up. And our perceptions are largely dependent on our outlook and mindfulness helps

170
00:32:26,320 --> 00:32:33,440
to cultivate a positive or a objective outlook, a wise outlook, positive in the sense of being wise.

171
00:32:34,800 --> 00:32:42,320
And so you're able to move beyond the judgments of things as being messed up,

172
00:32:42,320 --> 00:32:50,640
as life being challenging and so on. It's possible to be very strong in the face of challenges.

173
00:32:51,280 --> 00:32:59,280
It just involves letting go and giving up your reactivity, the judgments of things, the clinging to

174
00:32:59,280 --> 00:33:08,880
things. So when you point out, for example, you give the example of it potentially hurting

175
00:33:08,880 --> 00:33:15,840
you financially shows that even though these things are not real, we could say they can hurt you.

176
00:33:15,840 --> 00:33:22,960
But the reality behind it is that at first there is no you to hurt. Reality is just experiences.

177
00:33:22,960 --> 00:33:31,440
So the you that they hurt just means new experiences, different experiences. But second, the

178
00:33:31,440 --> 00:33:41,280
change, you know, the difference between having a wise outlook and having a deluded outlook

179
00:33:41,280 --> 00:33:48,160
has a very profound impact on all of the conceptual things in our life like relationships.

180
00:33:48,160 --> 00:33:54,160
So rather, so it's important to basically put in very simply,

181
00:33:54,160 --> 00:34:04,160
we, our relationships are something that exists in our minds, our idea of our relationships

182
00:34:04,160 --> 00:34:15,440
with others, how they see us. And we can become very fixated on what we think of our relationship

183
00:34:15,440 --> 00:34:21,200
is. But the reality can be very different and can and more importantly can change very quickly.

184
00:34:21,200 --> 00:34:29,360
And depends very much on our perspective and our outlook. So Brian, focus on mindfulness,

185
00:34:29,360 --> 00:34:37,120
try and cultivate clarity of mind and take things rather than in terms of relationships,

186
00:34:37,120 --> 00:34:44,160
like like suppose my relationship with my parents is messed up and they're very disappointed in me

187
00:34:44,160 --> 00:34:51,120
or even angry at me or not talking to me. That's just as an example. Rather than thinking

188
00:34:51,120 --> 00:34:58,160
of it, like I have a bad relationship with my parents, try and see the present experiences that

189
00:34:58,160 --> 00:35:03,360
you have with them when you interact with them. Try and see them with wisdom and try and go through

190
00:35:03,360 --> 00:35:09,040
them with wisdom, coming out of them with positive choices and beneficial choices, wholesome,

191
00:35:09,600 --> 00:35:16,880
clear-minded choices. And never basically, I'm saying never get bogged down in concepts like

192
00:35:16,880 --> 00:35:23,360
the state of your relationship as an example or any of the concepts in the world. Just try and

193
00:35:23,920 --> 00:35:29,440
separate those conceptual things from your actual experience. So when you meet with someone,

194
00:35:29,440 --> 00:35:35,040
rather than carry around all this baggage of what our relationship is, what I've done in the past,

195
00:35:35,040 --> 00:35:45,280
what they've done in the past. And that has a very real impact on things like relationships and

196
00:35:45,280 --> 00:35:54,560
things like financial stability as well. It can be subtle, but your life changes very much for

197
00:35:54,560 --> 00:36:00,400
the better as you are more mindful and more present. Try and make that separation and just stay

198
00:36:00,400 --> 00:36:06,880
with your experiences, be with your experiences, rather than worrying about the past or the future.

199
00:36:06,880 --> 00:36:18,400
Sometimes I am distracted when I practice and it can stay long and doesn't go away. Should I use

200
00:36:18,400 --> 00:36:39,280
the distraction to see, for instance, non-self? Like, I cannot control this. Sorry, one second.

201
00:36:39,280 --> 00:36:55,520
So you've got the right idea. It's just that that's not really how this whole, this all works.

202
00:36:55,520 --> 00:37:04,160
You're on the right track, but just to be clear, you don't use experiences as an opportunity

203
00:37:04,160 --> 00:37:10,880
say to reflect. So it's a common misconception that you should take time to reflect and think,

204
00:37:10,880 --> 00:37:16,640
oh, yeah, that means impermanence or that means suffering or that means non-self. That kind of

205
00:37:16,640 --> 00:37:22,560
thinking can be useful if you're stuck, if you feel like there's a problem with your practice,

206
00:37:22,560 --> 00:37:29,760
like you have a lot of pain to reflect and say, well, that's the nature of suffering that I'm

207
00:37:29,760 --> 00:37:37,360
suffering because I'm clinging when you're not able to, when your practice is different from time

208
00:37:37,360 --> 00:37:45,200
to time, from session to session, you can reflect and remind yourself, oh, yeah, no, don't worry

209
00:37:45,200 --> 00:37:52,400
about that. That's the nature of impermanence. But the way the practice works is that over time,

210
00:37:52,400 --> 00:37:58,160
you'll start to see that these sorts of distractions and the uncontrollable nature of the mind,

211
00:37:58,160 --> 00:38:04,240
it will just lead to a familiarity with the nature of non-self as opposed to any kind of intellectual

212
00:38:04,240 --> 00:38:13,040
reflection, you see. So those kind of reflections can be useful, but not inherently practically

213
00:38:13,040 --> 00:38:22,080
useful. They're just a sort of a meta-practice, an META practice that allows you to refocus your

214
00:38:22,080 --> 00:38:30,400
attention. It means reassuring you, but that's not we pass in that. So you don't use it to see

215
00:38:30,400 --> 00:38:37,760
non-self. You will start to appreciate the non-self nature of things because of this unavoidable

216
00:38:37,760 --> 00:38:42,640
nature of the mind to not do what you want it to do. That's how it happens. It's just becoming

217
00:38:42,640 --> 00:38:48,400
more familiar with the reality. It's not about using it as an opportunity for it to see this or

218
00:38:48,400 --> 00:38:54,160
see that. You will start to see. That's the whole thing. What you're doing will allow you to see

219
00:38:54,160 --> 00:38:58,400
because of getting distracted when you intend not to get distracted.

220
00:39:05,200 --> 00:39:10,480
I've been sitting cross-legged, or at least a year, and my knees are still high up.

221
00:39:11,120 --> 00:39:17,040
I've been using pillows. Should I start my sitting with some space between my legs and the pillows

222
00:39:17,040 --> 00:39:19,040
like an inch or so?

223
00:39:24,160 --> 00:39:37,040
Some space between, oh, I see. No, that's not a huge issue. I mean, it's much more visible.

224
00:39:37,040 --> 00:39:42,880
The benefits are much more visible if you do intensive practice, but there's no shame

225
00:39:42,880 --> 00:39:49,520
really in using pillows. One thing that I would recommend is to, you don't necessarily need space

226
00:39:49,520 --> 00:39:57,360
between them, but find a happy medium where you are still experiencing some pain, but the pain is

227
00:39:57,360 --> 00:40:03,520
manageable. Of course, it is a temporary pain as you stretch out, but if you don't allow for any

228
00:40:03,520 --> 00:40:10,480
pain, you probably will never really stretch out. So you can start without pillows entirely,

229
00:40:10,480 --> 00:40:15,760
but I assume that's not going to last long. But then, what pillow is just enough so that the pain

230
00:40:15,760 --> 00:40:22,880
is manageable. Try and see it as a challenge to use the pain as a meditation object, as opposed

231
00:40:22,880 --> 00:40:27,840
to finding a way to not experience it. And as a result of that, you should find not only physically,

232
00:40:27,840 --> 00:40:34,000
but mentally, you relax because part of the tension is very mental. The aversion to pain,

233
00:40:34,000 --> 00:40:42,080
which is made worse by the avoiding of pain, by using lots of questions. So yeah,

234
00:40:42,080 --> 00:40:47,360
some sort of happy medium. I don't think it would be leaving a gap between it and between

235
00:40:47,360 --> 00:40:54,560
your legs and the pillows. It just means to have them lightly supported, so the pain is less intense.

236
00:40:55,120 --> 00:40:59,120
I mean, obviously, if you can put up with a pain, you can just go without pillows, but

237
00:40:59,120 --> 00:41:03,920
it's more of a mental thing that you're not generally equipped to deal with that much pain.

238
00:41:09,600 --> 00:41:15,840
Through the practice, I feel much less angry, less stressed, and I worry less, which I am greatly

239
00:41:15,840 --> 00:41:22,000
thankful for. But I wouldn't say that I feel much happiness. Doesn't this practice lead to happiness?

240
00:41:22,000 --> 00:41:31,040
The practice leads to clarity, clarity leads to happiness. So yeah, it does lead to happiness, but

241
00:41:36,320 --> 00:41:42,800
but it's complicated because first of all, you've got a lot of,

242
00:41:42,800 --> 00:41:52,240
let's say, bad karma, you've performed in the past. I mean, I assume pretty much everyone does.

243
00:41:52,240 --> 00:41:56,800
If you were born as a human being, well, that's already some sort of karma because it's attachment.

244
00:41:59,280 --> 00:42:03,360
I mean, it's pretty impossible to go through life unless you're enlightened without

245
00:42:03,360 --> 00:42:08,240
accumulating lots of bad karma, especially if you haven't been inducted into

246
00:42:08,240 --> 00:42:14,560
practice of mindfulness, which is for the purpose of preventing the performance of bad karma.

247
00:42:16,240 --> 00:42:21,840
But then on top of that, depending how much you're practicing, how mindful you are, there's

248
00:42:21,840 --> 00:42:28,160
going to be times where you are still engaging in unmindful activities. So the benefits are not

249
00:42:28,160 --> 00:42:34,960
going to be greatly pronounced unless you engage in some very intensive practice. You have to be

250
00:42:34,960 --> 00:42:46,320
content with mild benefits. I mean, it's kind of silly to describe things. I mean, there's a problem

251
00:42:46,320 --> 00:42:53,360
with the way you've presented things because I can ask you, you being less angry, less stress and

252
00:42:53,360 --> 00:43:02,240
worry less. Does that make you happier? Is that considered a happier state than being very angry,

253
00:43:02,240 --> 00:43:11,360
being more stressed and worrying a lot? You might argue that you don't feel pleasure, but

254
00:43:12,640 --> 00:43:19,760
pleasure is not synonymous with happiness. And I would say there's no question that what

255
00:43:19,760 --> 00:43:30,400
you have described is a far, far happier state than the alternative. And so the benefits may not be

256
00:43:30,400 --> 00:43:35,840
pronounced and you might not be getting this and certainly not necessarily pleasure,

257
00:43:36,560 --> 00:43:42,800
but there is an incredible amount of happiness just being described in your question.

258
00:43:45,280 --> 00:43:51,200
So I guess part of the answer is just reinterpreting what you mean by what should be meant by

259
00:43:51,200 --> 00:43:56,000
happiness, because if happiness is synonymous with pleasure, well, you've got a problem, because

260
00:43:56,000 --> 00:44:00,640
you're not going to always feel pleasure and pleasure, furthermore, leads to liking and attachment

261
00:44:00,640 --> 00:44:09,680
and actual suffering. I mean, pleasure itself doesn't, but it isn't free from that. And of course,

262
00:44:09,680 --> 00:44:15,520
you're liking it is the problem. One thing I do.

263
00:44:18,240 --> 00:44:24,720
People in the invisible world are existing parallelly with us in this visible world. They can

264
00:44:24,720 --> 00:44:31,360
see us, but can't talk to us. I often feel that my deceased mom is present with me. Is this correct?

265
00:44:40,560 --> 00:44:44,720
I mean, it's correct that you feel that way, but I would just try to note the feeling.

266
00:44:45,440 --> 00:44:48,240
If you're thinking about your mother, then you can note that.

267
00:44:48,240 --> 00:44:58,160
I mean, whether they are there with you or not, with you giving sending positive wishes for

268
00:44:58,160 --> 00:44:59,840
their well-being is always good.

269
00:45:06,880 --> 00:45:13,680
How can I become more austere? I sometimes am discouraged when seeing people who are very disciplined,

270
00:45:13,680 --> 00:45:19,120
whether it's in a meditative sense or in other ways, like people in a military bootcamp, etc.

271
00:45:21,360 --> 00:45:27,360
Well, yeah, I mean, that's one of the problems with comparing yourself to others. It's a kind of

272
00:45:27,360 --> 00:45:34,240
conceit that arises, thinking yourself less than someone else, comparing yourself to someone else

273
00:45:34,240 --> 00:45:40,320
and feeling that you're inferior to them, you're inadequate and so on. It's a real trap. It's not

274
00:45:40,320 --> 00:45:47,200
helpful. It's a, it's a bad practice, so you should try to note that and note the discouragement.

275
00:45:50,080 --> 00:45:55,440
The only way to become something that you are not is to, well, not that's not true.

276
00:45:55,440 --> 00:46:01,680
The best way to gain good qualities that you don't have is to focus on what you do have

277
00:46:02,640 --> 00:46:07,440
and work through it, because ultimately all you need to do is get rid of that,

278
00:46:07,440 --> 00:46:12,160
which is preventing you from being a pure and let's say perfect being.

279
00:46:13,600 --> 00:46:18,080
This is why, you know, there's this talk even in Buddhism about us being perfect

280
00:46:20,400 --> 00:46:25,040
by nature and all that's the only problem is that which clouds that perfection.

281
00:46:26,560 --> 00:46:30,240
In Mahayana Buddhism, I think they talk more about it, but the Buddha in the

282
00:46:30,240 --> 00:46:39,280
Torah by the texts he says that the mind is is bright, the mind is luminant, it's a pure

283
00:46:40,000 --> 00:46:47,840
clear, but it's only polluted by visiting defilements and so if you free yourself from the defilements

284
00:46:47,840 --> 00:46:53,520
then you've, you've done all that needs to be done. It's rather than putting that, brushing that

285
00:46:53,520 --> 00:47:02,320
aside, the bad things aside and trying to be something that you're not, like austere, for example,

286
00:47:03,040 --> 00:47:07,280
get rid of the things that are preventing you from being austere, not that you have to be

287
00:47:08,560 --> 00:47:13,200
particularly austere, but to the extent that austere is a good thing,

288
00:47:13,200 --> 00:47:22,880
and you'll be much more inclined towards it if you free yourself from, say, greed and anger and delusion.

289
00:47:27,680 --> 00:47:30,160
How do you know that samsara exists?

290
00:47:30,160 --> 00:47:42,880
I'm sorry, it's just a word. The word exists. I mean, it doesn't actually exist, but the word exists

291
00:47:42,880 --> 00:47:51,760
in our conception of it. The concept of samsara as a concept doesn't exist outside of its

292
00:47:51,760 --> 00:48:00,400
conceptuality, but if by some sorry, I mean reality, well, say that reality doesn't exist is some pretty

293
00:48:00,400 --> 00:48:09,360
strong mental gymnastics. Pretty clear, practically speaking, at least that reality exists.

294
00:48:10,880 --> 00:48:15,520
We're not so much concerned about what exists and what does not exist, but isn't as much more

295
00:48:15,520 --> 00:48:22,400
practical. Suffering exists. The cause of suffering exists. The cessation of suffering exists. And

296
00:48:22,400 --> 00:48:30,080
the path leading to the cessation of suffering exists. So not many meditation questions this week.

297
00:48:30,080 --> 00:48:34,320
I guess we've been away. I don't know if the audience is, well, we don't have as many people today,

298
00:48:35,280 --> 00:48:40,400
but maybe if there's no more meditation questions, we can just end it there. We can end early.

299
00:48:40,400 --> 00:48:49,520
That seems to be the case, but if, okay, well, thank you all for coming. We're tuning in and practicing

300
00:48:49,520 --> 00:48:54,640
with us. I wish you all peace, happiness, freedom from suffering have a good week.

301
00:48:54,640 --> 00:49:15,040
Good afternoon.

