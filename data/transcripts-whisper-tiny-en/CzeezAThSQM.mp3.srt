1
00:00:00,000 --> 00:00:25,280
Evening everyone, broadcasting live, March 20th, and cut my meditation short to see if

2
00:00:25,280 --> 00:00:32,560
it's been a long day, lots of different things. I'm gonna cut up in the

3
00:00:32,560 --> 00:00:50,020
little motion.

4
00:00:50,020 --> 00:01:19,860
homework. Anyway, here on time. So today's quote is quite an interesting one.

5
00:01:19,860 --> 00:01:43,620
Let's see if we can get a 60-1 in there.

6
00:01:43,620 --> 00:01:57,100
7-1, okay.

7
00:01:57,100 --> 00:02:19,780
1-1 is this a 5-1.

8
00:02:19,780 --> 00:02:38,060
as many names per line.

9
00:02:38,060 --> 00:02:47,780
Arhtia jidee wa sa sa cha,that by day and by night.

10
00:02:47,780 --> 00:03:08,060
Anyadi wa-pachati anyanyu rutati Anyadi wa-pachati

11
00:03:08,060 --> 00:03:15,780
Anyadi wa-pachati arises as one thing. Anyanyu rutati ceases as another.

12
00:03:15,780 --> 00:03:22,780
One thing, one mind arises when another ceases.

13
00:03:22,780 --> 00:03:33,780
Sayyatapi bikhui makatou aranyi avanyi tarama no sakangan nati,

14
00:03:33,780 --> 00:03:36,780
when a monkey grabs a branch.

15
00:03:36,780 --> 00:03:39,780
Let's go over another branch.

16
00:03:39,780 --> 00:03:44,780
Thun mochi tu wa any lounge duangan nati,

17
00:03:44,780 --> 00:03:46,780
that oneaturally grabs another one.

18
00:03:46,780 --> 00:03:50,780
They were miracle, bikhui nati pulu rutati maymunks.

19
00:03:50,780 --> 00:03:53,760
Heung yidang naturally titit al continued tap bigu Followy tipi win

20
00:03:53,760 --> 00:03:58,780
nyanyan no natiWebi.

21
00:03:58,780 --> 00:03:48,260
That which is called tita cr Healmah na-pach

22
00:03:48,260 --> 00:04:09,320
Wa kura

23
00:04:09,320 --> 00:04:18,320
Dangratia de Divasasach, and this by day or by night, and by night, Anyadi, Anyadi, Anyadi,

24
00:04:18,320 --> 00:04:22,320
Wachati, Anyadi, Anyadi, Anyadi, Anyadi.

25
00:04:22,320 --> 00:04:31,320
Rises one ceases another.

26
00:04:31,320 --> 00:04:43,320
It's a simple philosophy and a claim, but it has deep meaning.

27
00:04:43,320 --> 00:04:56,320
You see, so science and, say, ordinary people have this idea of a substratum,

28
00:04:56,320 --> 00:05:00,320
most religions as well.

29
00:05:00,320 --> 00:05:08,320
So science is quite convinced that there is a substratum of reality.

30
00:05:08,320 --> 00:05:13,320
Religion is usually quite convinced that there is a substratum of mentality,

31
00:05:13,320 --> 00:05:23,320
so self-god.

32
00:05:23,320 --> 00:05:32,320
And so there is a lot of postulation of various substratum.

33
00:05:32,320 --> 00:05:40,320
And so science cultivates substratum that tend to

34
00:05:40,320 --> 00:05:51,320
accord with reality, religion often not, but religion also does a better job

35
00:05:51,320 --> 00:06:02,320
according with mental reality sometimes than science.

36
00:06:02,320 --> 00:06:09,320
Nonetheless, the tendency is to postulate substratum.

37
00:06:09,320 --> 00:06:15,320
I mean, it's like a framework, like a mind, a body, a three-dimensional,

38
00:06:15,320 --> 00:06:23,320
four-dimensional reality that we live in.

39
00:06:23,320 --> 00:06:28,320
Buddhism doesn't do that.

40
00:06:28,320 --> 00:06:35,320
Buddhism has a very simple take on the universe that this moment is real.

41
00:06:35,320 --> 00:06:44,320
This moment is an experience, and then it ceases, and then this moment is real.

42
00:06:44,320 --> 00:06:46,320
And that's that moment.

43
00:06:46,320 --> 00:06:53,320
So when we talk about mind in teravada Buddhism, we mean that moment of mind.

44
00:06:53,320 --> 00:06:58,320
One moment of mind. One moment of experience.

45
00:06:58,320 --> 00:07:05,320
One moment of awareness or of contemplation of an object.

46
00:07:05,320 --> 00:07:09,320
Observation of an object.

47
00:07:09,320 --> 00:07:15,320
One moment of experience.

48
00:07:15,320 --> 00:07:22,320
And so if you ask the question who's right, it's not really the appropriate question.

49
00:07:22,320 --> 00:07:29,320
Scientists tend to scoff at Buddhism as being overly simple, simplistic.

50
00:07:29,320 --> 00:07:37,320
They'll say, okay, well, maybe you're right, but you haven't said anything about reality.

51
00:07:37,320 --> 00:07:47,320
You haven't figured out subatomic particles or how to build an atom bomb.

52
00:07:47,320 --> 00:07:54,320
Nor have you explored the vast reaches of the universe.

53
00:07:54,320 --> 00:07:58,320
The solar system and the galaxy.

54
00:07:58,320 --> 00:08:00,320
The known universe.

55
00:08:00,320 --> 00:08:05,320
And then you haven't really explored reality.

56
00:08:05,320 --> 00:08:14,320
And we can say, no, no, we haven't.

57
00:08:14,320 --> 00:08:17,320
The question is to what end?

58
00:08:17,320 --> 00:08:22,320
The question of all of this is to what end.

59
00:08:22,320 --> 00:08:27,320
Religion makes all sorts of claims about reality.

60
00:08:27,320 --> 00:08:31,320
And we can ask, what is the result?

61
00:08:31,320 --> 00:08:39,320
I mean, if it were useful and positive to delude yourself

62
00:08:39,320 --> 00:08:48,320
and make claims about reality that were not in accord with reality,

63
00:08:48,320 --> 00:08:49,320
that had no evidence.

64
00:08:49,320 --> 00:08:50,320
They were not based on evidence.

65
00:08:50,320 --> 00:08:59,320
If that were somehow beneficial, then there wouldn't be much of a problem.

66
00:08:59,320 --> 00:09:07,320
Most religions would do fine, but because they tend to not relate to actual experience

67
00:09:07,320 --> 00:09:13,320
and reality, they tend to rely more on belief.

68
00:09:13,320 --> 00:09:22,320
They tend to be quite artificial and stilted or forced.

69
00:09:22,320 --> 00:09:27,320
I mean, if you step back or say you step out of a meditation course

70
00:09:27,320 --> 00:09:34,320
where you've been studying your own mind and what's really here and now.

71
00:09:34,320 --> 00:09:39,320
And then you contemplate one of these religions, pick any religion or random.

72
00:09:39,320 --> 00:09:43,320
And you contemplate what they're actually saying.

73
00:09:43,320 --> 00:09:50,320
It's shocking how absurd it all is.

74
00:09:50,320 --> 00:09:53,320
What are you talking about?

75
00:09:53,320 --> 00:09:58,320
What is this garbage?

76
00:09:58,320 --> 00:10:04,320
Really. I mean, so much belief.

77
00:10:04,320 --> 00:10:06,320
There was this guy.

78
00:10:06,320 --> 00:10:09,320
He said this.

79
00:10:09,320 --> 00:10:10,320
He claimed this.

80
00:10:10,320 --> 00:10:12,320
He did this.

81
00:10:12,320 --> 00:10:16,320
And he will save us.

82
00:10:16,320 --> 00:10:20,320
Or there's this God up there.

83
00:10:20,320 --> 00:10:26,320
I have to do his pray or repeat his name or chant this or chant that.

84
00:10:26,320 --> 00:10:31,320
And he will save you.

85
00:10:31,320 --> 00:10:35,320
So they run into lots of problems.

86
00:10:35,320 --> 00:10:40,320
And wise people would tend to stay away from most of the religions out there

87
00:10:40,320 --> 00:10:48,320
because they tend to be not very much, not very closely aligned to reality.

88
00:10:48,320 --> 00:10:54,320
But science does a good job focusing on reality.

89
00:10:54,320 --> 00:10:56,320
A lining itself with experience.

90
00:10:56,320 --> 00:11:01,320
So they are able to run experiments that actually accord with the way things are.

91
00:11:01,320 --> 00:11:05,320
But they've had a different problem.

92
00:11:05,320 --> 00:11:12,320
And we can argue about what is more real in the substratum, physical,

93
00:11:12,320 --> 00:11:19,320
and even maybe mental reality, or experience.

94
00:11:19,320 --> 00:11:21,320
Which one is more real?

95
00:11:21,320 --> 00:11:24,320
We can argue about that, but it's not an important argument again.

96
00:11:24,320 --> 00:11:29,320
To what end?

97
00:11:29,320 --> 00:11:31,320
What has science given us?

98
00:11:31,320 --> 00:11:33,320
What has it brought at?

99
00:11:33,320 --> 00:11:39,320
It's brought us all sorts of magical, wonderful things, like the internet.

100
00:11:39,320 --> 00:11:46,320
The fact that I'm able to talk to people all around the world is kind of magical.

101
00:11:46,320 --> 00:11:48,320
It's brought us all this.

102
00:11:48,320 --> 00:11:51,320
Very powerful, this kind of knowledge.

103
00:11:51,320 --> 00:11:53,320
But has it brought us happiness?

104
00:11:53,320 --> 00:11:54,320
Has it brought us peace?

105
00:11:54,320 --> 00:11:59,320
Has it brought us understanding?

106
00:11:59,320 --> 00:12:05,320
And I mean a specific, I'm thinking of a specific type of understanding, really.

107
00:12:05,320 --> 00:12:10,320
Has it brought us to understanding of ourselves?

108
00:12:10,320 --> 00:12:12,320
No.

109
00:12:12,320 --> 00:12:18,320
None of the people who study science get angry, become addicted to things.

110
00:12:18,320 --> 00:12:26,320
They have a hard time dealing with ordinary experiences, and so they suffer.

111
00:12:26,320 --> 00:12:28,320
They stress.

112
00:12:28,320 --> 00:12:30,320
They kill themselves.

113
00:12:30,320 --> 00:12:38,320
They drown their, soak their brains in alcohol or drugs.

114
00:12:38,320 --> 00:12:42,320
Because they can't deal with ordinary reality, right?

115
00:12:42,320 --> 00:12:51,320
They study reality so much, but can't deal with it.

116
00:12:51,320 --> 00:13:04,320
And then you have this Buddhist, I'd say, a meditator who focuses on their reality, who looks at their experience.

117
00:13:04,320 --> 00:13:08,320
When they have emotions, they see the most emotions.

118
00:13:08,320 --> 00:13:11,320
When they have pain, they see that it's pain.

119
00:13:11,320 --> 00:13:15,320
The experiences of seeing their hearings.

120
00:13:15,320 --> 00:13:17,320
They see them for what they are.

121
00:13:17,320 --> 00:13:24,320
They put aside any theories or thoughts about a some stratum about reality.

122
00:13:24,320 --> 00:13:29,320
They go here, I'm in this room, they even lose track of being in a room.

123
00:13:29,320 --> 00:13:34,320
They just see the mind arising, and see seeing the mind arises.

124
00:13:34,320 --> 00:13:36,320
There's awareness of something.

125
00:13:36,320 --> 00:13:39,320
And see says, and immediately there's another mind.

126
00:13:39,320 --> 00:13:42,320
But it's totally different.

127
00:13:42,320 --> 00:13:47,320
It's a whole new mind, whole new experience.

128
00:13:47,320 --> 00:13:50,320
And they see this.

129
00:13:50,320 --> 00:13:58,320
They're able to see a reality or a way of looking at reality for themselves.

130
00:13:58,320 --> 00:14:04,320
And that leads to objectivity.

131
00:14:04,320 --> 00:14:11,320
And they're able to fully comprehend reality, I guess you can say.

132
00:14:11,320 --> 00:14:15,320
Because science, you can study it, but you can't fully comprehend it.

133
00:14:15,320 --> 00:14:18,320
You can't look at the wall and comprehend it.

134
00:14:18,320 --> 00:14:23,320
That wall is made up of mostly in the space.

135
00:14:23,320 --> 00:14:31,320
I need them to make it a given example, but moreover, you're not able to comprehend all those things.

136
00:14:31,320 --> 00:14:33,320
I would say it's not even worth it.

137
00:14:33,320 --> 00:14:35,320
It wouldn't even be worth it if it could.

138
00:14:35,320 --> 00:14:40,320
And in fact, our comprehension of things is part of the problem.

139
00:14:40,320 --> 00:14:46,320
Because when you think about concepts, you think about entities, you have to abstract something.

140
00:14:46,320 --> 00:14:54,320
Right? You have this mouse. What happens when I think of this mouse?

141
00:14:54,320 --> 00:14:58,320
There's something going on in my mind.

142
00:14:58,320 --> 00:15:00,320
There's an experience.

143
00:15:00,320 --> 00:15:04,320
There's an experience which says there is a mouse in your hand.

144
00:15:04,320 --> 00:15:10,320
But it has nothing to do directly with the experience of seeing the mouse.

145
00:15:10,320 --> 00:15:14,320
I think the seeing and the feeling that created it.

146
00:15:14,320 --> 00:15:17,320
It's all in the mind at that point.

147
00:15:17,320 --> 00:15:26,320
There are minds arising in safety, leading, therefore, therefore, one to the other.

148
00:15:26,320 --> 00:15:28,320
And I'm not aware of that.

149
00:15:28,320 --> 00:15:33,320
So there's a reality that is being ignored.

150
00:15:33,320 --> 00:15:37,320
Because instead, I'm focused on the idea of the mouse.

151
00:15:37,320 --> 00:15:40,320
There's something that I'm missing.

152
00:15:40,320 --> 00:15:45,320
And as a result, when likes and dislikes come up, I miss them as well.

153
00:15:45,320 --> 00:15:47,320
So I like them now. So I dislike it.

154
00:15:47,320 --> 00:15:53,320
And that's what leads to frustration and addiction and so on.

155
00:15:53,320 --> 00:15:59,320
So it leads us attached to things and people and experiences.

156
00:15:59,320 --> 00:16:09,320
And it leads us to get frustrated and upset and bored and disappointed when we don't get what we want.

157
00:16:09,320 --> 00:16:21,320
So when we pay attention to that, if I'm picking up the mouse, I pay attention to the experience and the thinking and the judging.

158
00:16:21,320 --> 00:16:35,320
And I am in a position to understand and to see the experience clearly and to not react.

159
00:16:35,320 --> 00:16:45,320
Or to understand my reactions and to see how they're hurting me and to slowly give them up.

160
00:16:45,320 --> 00:16:55,320
This is an important, an important quote to help us understand our focus should be.

161
00:16:55,320 --> 00:16:59,320
Our focus should be on experiential reality.

162
00:16:59,320 --> 00:17:03,320
This is what the Buddha talked about.

163
00:17:03,320 --> 00:17:06,320
So here, what do we have? This also, it is interesting.

164
00:17:06,320 --> 00:17:09,320
The uninstracted world learning.

165
00:17:09,320 --> 00:17:12,320
The Guru Chana, right?

166
00:17:12,320 --> 00:17:15,320
The sutua.

167
00:17:15,320 --> 00:17:19,320
The sutua, the kabe-putu-chano,

168
00:17:19,320 --> 00:17:22,320
uninstracted-putu-chanan.

169
00:17:22,320 --> 00:17:26,320
Might experience revulsion towards this body.

170
00:17:26,320 --> 00:17:31,320
You might become dispassionate towards it and be liberated.

171
00:17:31,320 --> 00:17:34,320
Because growth and decline is seen in this body.

172
00:17:34,320 --> 00:17:42,320
And it's seen and taken up.

173
00:17:42,320 --> 00:17:45,320
Oh, okay.

174
00:17:45,320 --> 00:17:50,320
He's saying, might be liberated from the body.

175
00:17:50,320 --> 00:17:52,320
The body is revolved.

176
00:17:52,320 --> 00:17:53,320
It's easy to see.

177
00:17:53,320 --> 00:17:57,320
It's revolting as you get old and if you get sick and so on.

178
00:17:57,320 --> 00:18:01,320
It's possible for ordinary people to let go of it.

179
00:18:01,320 --> 00:18:03,320
But the mind.

180
00:18:03,320 --> 00:18:06,320
The mind we are unable to let go of.

181
00:18:06,320 --> 00:18:08,320
This is the ordinary, uninstracted world.

182
00:18:08,320 --> 00:18:14,320
It is unable to experience revulsion towards it, unable to become dispassionate towards it,

183
00:18:14,320 --> 00:18:17,320
and be liberated from it.

184
00:18:17,320 --> 00:18:20,320
Because for a long time, this has been held by him,

185
00:18:20,320 --> 00:18:28,320
appropriated and grasped us. This is me, mine, this I am, this is myself.

186
00:18:28,320 --> 00:18:49,320
This is me, this is me, this is me, this is me, this is me.

187
00:18:49,320 --> 00:18:51,320
This is what we think of the mind, right?

188
00:18:51,320 --> 00:18:56,320
Let me who doesn't think the mind is me and mine.

189
00:18:56,320 --> 00:18:58,320
The body we can let go.

190
00:18:58,320 --> 00:19:02,320
When the mind is pleased or displeased,

191
00:19:02,320 --> 00:19:12,320
that's me, that's I.

192
00:19:12,320 --> 00:19:22,320
That's my, that's what we do.

193
00:19:22,320 --> 00:19:27,320
We do.

194
00:19:27,320 --> 00:19:30,320
We're using interesting words.

195
00:19:30,320 --> 00:19:33,320
I guess revulsion isn't really the right word.

196
00:19:33,320 --> 00:19:38,320
In be does, a revulsion, it becomes disenchanted, becomes dispassionate,

197
00:19:38,320 --> 00:19:46,320
becomes liberated, that's probably better.

198
00:19:46,320 --> 00:19:49,320
But it would be better to take, this is an interesting,

199
00:19:49,320 --> 00:19:54,320
this is a famous one actually, because we're better for the uninstracted world

200
00:19:54,320 --> 00:20:00,320
linked to take the self as the body, take the body as self.

201
00:20:00,320 --> 00:20:05,320
Because the body is, is standing for one year, for two years,

202
00:20:05,320 --> 00:20:10,320
for three, four, five, or ten years, for 20, 30, 40, 50 years, for a hundred years,

203
00:20:10,320 --> 00:20:12,320
or even longer.

204
00:20:12,320 --> 00:20:17,320
But the mind and mentality and consciousness arises as one thing,

205
00:20:17,320 --> 00:20:20,320
this is the quote, and ceases as another.

206
00:20:20,320 --> 00:20:29,320
Mind is flighty, the mind arises and ceases quickly.

207
00:20:29,320 --> 00:20:36,320
And, and this, this is really what I'm talking about, is the body appears to be a thing,

208
00:20:36,320 --> 00:20:37,320
right?

209
00:20:37,320 --> 00:20:41,320
If you think about the body, it's understandable that you would cling to it,

210
00:20:41,320 --> 00:20:43,320
but how can you cling to the mind?

211
00:20:43,320 --> 00:20:48,320
This is a different, the Buddha is exposing a different way of looking at reality

212
00:20:48,320 --> 00:20:50,320
from the point of view of the mind.

213
00:20:50,320 --> 00:20:58,320
It's from the point of view of the mind, even the body arises, let's say.

214
00:20:58,320 --> 00:21:03,320
And then he says, that could become a sutua, a learned,

215
00:21:03,320 --> 00:21:07,320
ariya savako, the disciple of the noble one,

216
00:21:07,320 --> 00:21:12,320
but it's just a mupada, me vasa, dukan, loni, somaras,

217
00:21:12,320 --> 00:21:16,320
manasikaruti.

218
00:21:16,320 --> 00:21:23,320
Well, with wisdom,

219
00:21:23,320 --> 00:21:29,320
it considers a sutua pada, the dependent origination,

220
00:21:29,320 --> 00:21:35,320
iti emas means ati yi dang ho ti,

221
00:21:35,320 --> 00:21:41,320
thus, I'd say.

222
00:21:41,320 --> 00:21:48,320
Well, when this exists, that comes to be with the arising of this that arises.

223
00:21:48,320 --> 00:21:53,320
Iti emas means ati yi dang ho ti,

224
00:21:53,320 --> 00:21:57,320
emas su pada yi dang ho ti,

225
00:21:57,320 --> 00:22:01,320
when this arises, there is that,

226
00:22:01,320 --> 00:22:06,320
with the arising of this that arises.

227
00:22:06,320 --> 00:22:10,320
Emas means ati with the non-existence of this,

228
00:22:10,320 --> 00:22:14,320
yi dang na ho ti, there is not that.

229
00:22:14,320 --> 00:22:19,320
Emas su niro da emang yi dang na ho ti with the cessation of this,

230
00:22:19,320 --> 00:22:24,320
and that ceases.

231
00:22:24,320 --> 00:22:27,320
Iti yi dang, that is to say,

232
00:22:27,320 --> 00:22:32,320
a vidja patea sankara, sankara pateo yi nang and so on,

233
00:22:32,320 --> 00:22:35,320
this is a pititya sama pada dependent origination.

234
00:22:35,320 --> 00:22:39,320
And so what he's saying here is this is how experiential reality works.

235
00:22:39,320 --> 00:22:42,320
You study in terms of cause and effect.

236
00:22:42,320 --> 00:22:45,320
There is ignorance because we're ignorant,

237
00:22:45,320 --> 00:22:48,320
we give rise to our partialities.

238
00:22:48,320 --> 00:22:50,320
We like some things with dyslexia,

239
00:22:50,320 --> 00:22:52,320
we chase after some things,

240
00:22:52,320 --> 00:22:54,320
we chase away some things.

241
00:22:54,320 --> 00:22:56,320
As we don't see things clearly,

242
00:22:56,320 --> 00:22:59,320
it's a result for born again and again.

243
00:22:59,320 --> 00:23:03,320
And we have anyway leads to suffering.

244
00:23:03,320 --> 00:23:07,320
I won't go into detail with that.

245
00:23:07,320 --> 00:23:11,320
Important point is that we look at reality from the point of view of our experience.

246
00:23:11,320 --> 00:23:13,320
And if you do that, there's really nothing,

247
00:23:13,320 --> 00:23:15,320
you become invincible.

248
00:23:15,320 --> 00:23:17,320
There's nothing that can

249
00:23:17,320 --> 00:23:20,320
empower over you.

250
00:23:20,320 --> 00:23:23,320
Whether it be something pleasant that you want to,

251
00:23:23,320 --> 00:23:25,320
there's no you, there's no thing.

252
00:23:25,320 --> 00:23:30,320
You see it if you see it as an experience, it arises.

253
00:23:30,320 --> 00:23:35,320
There's something that upsets you or frustrates you.

254
00:23:35,320 --> 00:23:38,320
That thing is just an experience.

255
00:23:38,320 --> 00:23:41,320
You see it arising, it's easy to experience.

256
00:23:41,320 --> 00:23:43,320
What does it mean?

257
00:23:43,320 --> 00:23:45,320
It's just an experience.

258
00:23:45,320 --> 00:23:48,320
You see the frustration, you see the upset,

259
00:23:48,320 --> 00:23:53,320
it arises with instances.

260
00:23:53,320 --> 00:23:57,320
You stop hiding, you stop running,

261
00:23:57,320 --> 00:24:01,320
stop living in conceptual reality,

262
00:24:01,320 --> 00:24:04,320
with things and people and places.

263
00:24:04,320 --> 00:24:09,320
Start living in reality or true reality which is your experience.

264
00:24:09,320 --> 00:24:14,320
Start seeing what's going on underneath that.

265
00:24:14,320 --> 00:24:20,320
Chrome plating of beautiful things and desirable things

266
00:24:20,320 --> 00:24:24,320
and ugly things and scary things.

267
00:24:24,320 --> 00:24:30,320
Underneath it's all just an experience.

268
00:24:30,320 --> 00:24:33,320
Anyway.

269
00:24:33,320 --> 00:24:35,320
There's the quote for this evening.

270
00:24:35,320 --> 00:24:39,320
That's the number for this evening.

271
00:24:39,320 --> 00:24:42,320
Now as usual, I'm going to put the quote.

272
00:24:42,320 --> 00:24:45,320
I'll put the link to the hangout.

273
00:24:45,320 --> 00:24:49,320
If you would like to join and ask a question,

274
00:24:49,320 --> 00:24:53,320
you're welcome to click the link.

275
00:24:53,320 --> 00:24:57,320
Only if you have a question and only if you're in a respectful,

276
00:24:57,320 --> 00:25:01,320
like no coming on naked or smoking or drinking alcohol,

277
00:25:01,320 --> 00:25:05,320
or eating food.

278
00:25:05,320 --> 00:25:11,320
You should consider this to be a dumb hangout.

279
00:25:11,320 --> 00:25:34,320
Just scare everybody away.

280
00:25:34,320 --> 00:25:49,320
You can go the next step to it.

281
00:25:49,320 --> 00:26:12,320
Thank you.

282
00:26:12,320 --> 00:26:35,320
All right, nobody with burning questions sleeping to be to join the hangout.

283
00:26:35,320 --> 00:26:44,320
Let me think I'll say good night.

284
00:26:44,320 --> 00:27:00,320
See you all tomorrow.

