1
00:00:00,000 --> 00:00:09,800
So everyone, just an update here on the goings on in my work.

2
00:00:09,800 --> 00:00:17,440
As many of you know, I've closed down the Ask a Month series in the sense of not accepting

3
00:00:17,440 --> 00:00:18,440
any more questions.

4
00:00:18,440 --> 00:00:23,680
I've been trying to answer the questions that are left over.

5
00:00:23,680 --> 00:00:28,080
And I think I'm going to change that.

6
00:00:28,080 --> 00:00:34,960
So there are, I think, still about a hundred questions that people have asked on Google

7
00:00:34,960 --> 00:00:39,200
Moderator via YouTube that I have an answer.

8
00:00:39,200 --> 00:00:48,720
And with your permission, or unless anyone strongly objects, I'm not going to answer them.

9
00:00:48,720 --> 00:00:54,520
Because now, for many of you, you're probably already aware, many of you are already aware,

10
00:00:54,520 --> 00:01:00,640
because you've been looking at my Facebook page, or the website, or so on.

11
00:01:00,640 --> 00:01:11,160
I've started something new, and the idea is to have a replacement for Google Moderator.

12
00:01:11,160 --> 00:01:18,240
The problem with the Ask a Month series was one, or is, it's not a was, it's still alive,

13
00:01:18,240 --> 00:01:21,640
but it's going to be take a little different form.

14
00:01:21,640 --> 00:01:27,360
One, the too many questions, and when I stopped accepting new questions, I assumed I'd

15
00:01:27,360 --> 00:01:32,080
have a set, and I could deal with them and eventually move on.

16
00:01:32,080 --> 00:01:36,160
But when I stopped, when I closed the forum down, then people started emailing me and sending

17
00:01:36,160 --> 00:01:43,960
me private messages, and the questions just more and more and more are actually increasing.

18
00:01:43,960 --> 00:01:46,920
So it's just too many for me to make a video for each.

19
00:01:46,920 --> 00:01:48,560
I mean, not even a hundred left.

20
00:01:48,560 --> 00:01:51,680
How can I make a hundred videos now, by the time I'm finished?

21
00:01:51,680 --> 00:01:57,600
The questions will all be outdated, many of them, people moved on, or whatever.

22
00:01:57,600 --> 00:02:03,960
It'd be much nicer to at least have some feedback to the questions, or to be able to filter

23
00:02:03,960 --> 00:02:11,760
out some of the questions that could be answered with text, some maybe can be answered

24
00:02:11,760 --> 00:02:17,520
by other people, and some will have to be filtered out.

25
00:02:17,520 --> 00:02:22,560
So that's one problem that I think I've addressed with the new project.

26
00:02:22,560 --> 00:02:29,600
And the second one is that it's very difficult to coordinate the answers with the questions,

27
00:02:29,600 --> 00:02:34,320
and to find out which questions I've been answered, and figure out which ones are

28
00:02:34,320 --> 00:02:37,280
voted high.

29
00:02:37,280 --> 00:02:40,680
Google Moderator is a little too simple for that.

30
00:02:40,680 --> 00:02:48,000
So it would be nice to be able to link the video, have the video posted as a response

31
00:02:48,000 --> 00:02:55,880
to the question, and be able to see which questions are recommended, and which questions

32
00:02:55,880 --> 00:03:00,760
are not, and to actually see the answer right there, whether it be a text answer or a video

33
00:03:00,760 --> 00:03:01,760
answer.

34
00:03:01,760 --> 00:03:06,720
So some of you are already aware that I've implemented something on our website that

35
00:03:06,720 --> 00:03:16,720
answers both these questions, and that is the new site called ask.seriamongalo.org,

36
00:03:16,720 --> 00:03:26,320
ASK.SIRI, M-A-N-G-A-L-O-D-O-R-G, if you know our website, then you just add, put an ask in

37
00:03:26,320 --> 00:03:27,320
front of it.

38
00:03:27,320 --> 00:03:35,440
And the website is having problems, it's always fun trying to keep a website up and going.

39
00:03:35,440 --> 00:03:39,280
So sometimes it might be down, but it seems to be fairly stable.

40
00:03:39,280 --> 00:03:46,800
So if you go there, you'll see something that is quite similar to, I don't know, I'm not

41
00:03:46,800 --> 00:03:53,400
really big on the internet, but I do know there's this one network, site network called

42
00:03:53,400 --> 00:03:58,680
Stack Exchange, which is for computer users, and when I have questions about my server,

43
00:03:58,680 --> 00:04:03,480
I can go and ask them, how do I do this, how do I do that?

44
00:04:03,480 --> 00:04:07,080
And it works, it works as a question and answer form.

45
00:04:07,080 --> 00:04:10,600
I don't know if there are other examples out there, maybe everyone's on them.

46
00:04:10,600 --> 00:04:14,120
It's kind of like Facebook, I guess, where you get people post things, and you get the

47
00:04:14,120 --> 00:04:20,600
like or dislike, and you get the post comments to them and so on.

48
00:04:20,600 --> 00:04:24,680
But it's focused on providing answers to people's questions.

49
00:04:24,680 --> 00:04:31,280
One person asks a question, everyone else in the forum can take an answer to it, and

50
00:04:31,280 --> 00:04:35,880
then they can vote on the question, they can vote on the answer, and what we need about

51
00:04:35,880 --> 00:04:44,400
this is they have a plug-in feature that allows you to submit videos as responses.

52
00:04:44,400 --> 00:04:46,920
So if you check it out, you'll be able to see what I'm talking about.

53
00:04:46,920 --> 00:04:51,520
It's all there, you have to sign in, and you ask your question.

54
00:04:51,520 --> 00:04:55,160
And I don't want people to despair about the questions that haven't been answered that

55
00:04:55,160 --> 00:04:58,000
I'm just going to throw away from YouTube.

56
00:04:58,000 --> 00:05:02,920
I want people, if you ask the question, and it didn't get a response, and you really

57
00:05:02,920 --> 00:05:08,560
think it's a valid question, and one that deserves a response, and you really like a response.

58
00:05:08,560 --> 00:05:16,720
Post it, please do me the favor and post it again on the ask.saryomunglow.org.

59
00:05:16,720 --> 00:05:20,440
I could do it myself, but then it's coming from me.

60
00:05:20,440 --> 00:05:25,520
The best thing is if it comes from you yourself, and that you get involved, and try to

61
00:05:25,520 --> 00:05:26,840
help other people, and so on.

62
00:05:26,840 --> 00:05:31,120
So the idea is people can, if I'm not able to answer all the questions people have,

63
00:05:31,120 --> 00:05:33,720
other people can answer them, and it's already taken off.

64
00:05:33,720 --> 00:05:37,160
If you look there, you can see people have already started answering questions, which

65
00:05:37,160 --> 00:05:41,640
is great, because many of these questions are like technical questions that anyone could

66
00:05:41,640 --> 00:05:52,280
or many different people could answer, and it doesn't require me or any specific person.

67
00:05:52,280 --> 00:06:02,520
So I think this will be a great way to bring the benefit that we are trying to get out

68
00:06:02,520 --> 00:06:11,320
of ask among, ask among, without all of the difficulties and the technical challenges.

69
00:06:11,320 --> 00:06:21,040
So from my part, I'm going to try to look from time to time and find questions that I think

70
00:06:21,040 --> 00:06:27,440
people really want to see an answer for, questions that are recommended, and post videos,

71
00:06:27,440 --> 00:06:33,240
and those that I think don't need to require a video, but I can give some textual comments

72
00:06:33,240 --> 00:06:38,960
or give a link, or so on, and I can do that as well.

73
00:06:38,960 --> 00:06:43,880
So I think it's going to be a great thing, please do check it out, sign up, and start

74
00:06:43,880 --> 00:06:47,160
asking questions and voting on other questions.

75
00:06:47,160 --> 00:06:52,040
I think there's something about you need to have been involved for some time before you

76
00:06:52,040 --> 00:06:59,400
can vote or so on, but I'm trying to take a look, it's got a, it's an interesting platform

77
00:06:59,400 --> 00:07:04,920
anyway, and it looks like it's already doing great, it looks like it's going to be a great

78
00:07:04,920 --> 00:07:08,920
tool for spreading the Buddhist teaching.

79
00:07:08,920 --> 00:07:14,680
The real thing you hear about all this is that there is so much in the Buddhist teaching

80
00:07:14,680 --> 00:07:26,840
and so much benefit to be had from just simple technical explanations, not even from profound

81
00:07:26,840 --> 00:07:33,600
realizations, if you just pass on some of the things that the Buddha said, simply by

82
00:07:33,600 --> 00:07:41,280
not hearing these things, people are missing out on quite a bit, so please don't be shy

83
00:07:41,280 --> 00:07:47,280
and give advice, but try to, whatever you've learned about the Buddha's teaching or about

84
00:07:47,280 --> 00:07:55,800
the meditation, and you know, pass on this information, it's, it would be a shame if we

85
00:07:55,800 --> 00:08:05,880
were all just to stick to the old methods of helping out the people around us, and when

86
00:08:05,880 --> 00:08:11,160
the world is looking and is searching and is open to these things, and all you have to do

87
00:08:11,160 --> 00:08:19,720
is post something on the internet, and you can help great many people, so for me it's

88
00:08:19,720 --> 00:08:25,320
just a means of answering the questions that I get, I mean, I never really had the intention

89
00:08:25,320 --> 00:08:29,640
to put myself out there, but it was kind of like, hmm, answering emails is a bit of a

90
00:08:29,640 --> 00:08:33,560
drag, because you answer one email, and then you got three more people asking the same

91
00:08:33,560 --> 00:08:39,040
questions, so how do you answer the same question once, so you publish it, normally in

92
00:08:39,040 --> 00:08:43,080
a book form, okay, now in the internet, when you publish it on the internet, well, you

93
00:08:43,080 --> 00:08:52,360
know, this is how it's evolved, and so this is the final product as of today, just in

94
00:08:52,360 --> 00:09:05,120
just as a means of me being able to respond to the queries and the requests that people have

95
00:09:05,120 --> 00:09:10,800
had, so I hope this helps, and I'd like to apologize for not answering emails and not

96
00:09:10,800 --> 00:09:15,680
answering many of the questions that people have had, but I think from what I've said

97
00:09:15,680 --> 00:09:21,280
it's clear that the reason is not that I'm, you know, that I didn't appreciate your

98
00:09:21,280 --> 00:09:26,760
question or so, and it's just that quite swamped with all the various emails, and obviously

99
00:09:26,760 --> 00:09:31,600
this isn't the only thing I'm doing, I mean, I am here in a meditation center, and we're

100
00:09:31,600 --> 00:09:39,720
trying to get this started, yeah, started in going, if I can take a little bit more time,

101
00:09:39,720 --> 00:09:44,360
because I'd rather just make one video and then then have to come back and make another

102
00:09:44,360 --> 00:09:54,720
one, so let's also give some updates here, what am I doing, tomorrow I'm off to see the

103
00:09:54,720 --> 00:10:02,640
world, I guess, I'm off to Thailand, so on Friday, on Saturday I'll be in Thailand all day

104
00:10:02,640 --> 00:10:09,720
in Bangkok, and I'm going to, I've been most invited for lunch and to go around to see

105
00:10:09,720 --> 00:10:15,560
a meditation center there or whatever, I would like to go see a specific monastery actually,

106
00:10:15,560 --> 00:10:21,800
but I'm not sure if I'll have time, and then flying to America, I'll be in California

107
00:10:21,800 --> 00:10:29,720
for five days, and then off to Canada to Winnipeg for seven days, I was invited to teach,

108
00:10:29,720 --> 00:10:38,680
and to take part in celebration this year, as many of you are aware, is the 2,600th anniversary

109
00:10:38,680 --> 00:10:43,600
according to this Sri Lankan reckoning of the Buddha's enlightenment, so it's a pretty

110
00:10:43,600 --> 00:10:50,520
big deal, it's every 100 years, the 26th Centennial, so many people are trying to celebrate

111
00:10:50,520 --> 00:10:57,800
that, and well, it's nice that I'll be able to take a part in a group of people's

112
00:10:57,800 --> 00:11:04,120
celebration, and then I'll be off to Toronto to visit my father and New York to visit

113
00:11:04,120 --> 00:11:09,040
my mother, but there are people who I'm going to need and people who are interested

114
00:11:09,040 --> 00:11:16,840
in talking or learning about meditation and then I'm coming back here, I'll be back here

115
00:11:16,840 --> 00:11:25,160
for the rainy season, and hopefully in this time we'll have built at least one meditation

116
00:11:25,160 --> 00:11:34,400
hut for female meditators actually, because we have place for male meditators, but

117
00:11:34,400 --> 00:11:42,480
Sri Lankans are quite conservative about women, and it's not just a matter of having

118
00:11:42,480 --> 00:11:50,080
a locked door, and this kind of basic security, they want to have women in a special

119
00:11:50,080 --> 00:11:59,280
section with gait or whatever, in a proper place, and I think staying in a cave for many

120
00:11:59,280 --> 00:12:09,280
Sri Lankans doesn't seem appropriate for a single woman who alone, which may seem absurd

121
00:12:09,280 --> 00:12:16,160
to many of us, and I'm not bad, it's not something that bothers me, but it bothers

122
00:12:16,160 --> 00:12:29,480
them, and well, what can you do, I mean sometimes you can just ignore it and try to pretend

123
00:12:29,480 --> 00:12:38,160
that there's that, just ignore people's complaints and go ahead and do it anyway, but

124
00:12:38,160 --> 00:12:43,560
if possible it's nice to meet halfway and say yes, yes, okay, we understand there could

125
00:12:43,560 --> 00:12:47,800
be dangers and so on, and so we're going to build a kudi anyway, I mean we need kudis,

126
00:12:47,800 --> 00:12:53,760
we need many, many huts, meditation rooms, so we're going to be building them anyway,

127
00:12:53,760 --> 00:12:58,560
but the first one will be a woman's room, because we don't have rooms anyway, I mean

128
00:12:58,560 --> 00:13:03,840
there's the room where the monks, the monks days and so that's an area for male monks

129
00:13:03,840 --> 00:13:08,000
and men, and there's another monk kudi that they're building and there's these two caves

130
00:13:08,000 --> 00:13:15,480
that's all we've got, so probably I'd like to build maybe during the rainy season or after

131
00:13:15,480 --> 00:13:25,520
we'll build a hall and with some rooms attached to it, there is funding for that, but

132
00:13:25,520 --> 00:13:32,160
for now we'll go start with a single kudi I think, and fixing a bathroom, because we

133
00:13:32,160 --> 00:13:42,320
also don't have bathrooms, there's two bathrooms and they can't, by western standards

134
00:13:42,320 --> 00:13:48,720
I don't think they can be called bathrooms, so for me I've seen worse, but I think

135
00:13:48,720 --> 00:13:56,080
for people coming from abroad it's way different, so I'm going to try to fix those

136
00:13:56,080 --> 00:14:05,840
up and get at least one hot build and then we open and people can come to practice

137
00:14:05,840 --> 00:14:12,880
meditation, so keep in touch and stay tuned and thanks to everyone for tuning in and for

138
00:14:12,880 --> 00:14:20,320
all of your feedback, it's so great to see people appreciating the teachings, I'm not doing

139
00:14:20,320 --> 00:14:30,080
this because it brings me great benefit, the benefit that I get is the peace and contentment

140
00:14:30,080 --> 00:14:34,560
knowing that I'm doing a good thing and that the world is becoming a little bit, you know,

141
00:14:34,560 --> 00:14:38,880
not a great thing, but it's becoming a little bit better because of something that I'm doing,

142
00:14:38,880 --> 00:14:44,080
so it's only because of your feedback if people were telling me that it's useless and

143
00:14:44,080 --> 00:14:48,080
everything is already out there and there are other monks teaching, all of this already

144
00:14:48,080 --> 00:14:53,360
and I don't need to the next stop and then go back to my meditation, but it looks like there is a

145
00:14:53,360 --> 00:15:05,280
need and people are looking and have found something here, so thanks for your appreciation and

146
00:15:05,280 --> 00:15:11,600
your feedback and for your practice to help to make the world a better place and help to make

147
00:15:11,600 --> 00:15:20,880
our minds better, help to become free from this chaos that has enveloped humanity, so thanks

148
00:15:20,880 --> 00:15:28,880
a lot for everything and you're welcome as well for all that I have done, may it be a great benefit

149
00:15:28,880 --> 00:15:34,800
to you and for all people around you and for the whole world, so this wish is wishing you all

150
00:15:34,800 --> 00:15:44,800
of these happiness and freedom from suffering, all of this.

