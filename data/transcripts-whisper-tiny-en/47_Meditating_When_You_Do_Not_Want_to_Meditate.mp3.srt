1
00:00:00,000 --> 00:00:04,960
disliking Q&A, meditating when you do not want to meditate.

2
00:00:05,840 --> 00:00:10,720
Question, how do I meditate when I do not want to meditate?

3
00:00:10,720 --> 00:00:15,920
I know it sounds like a frivolous question, but it is a serious issue for me.

4
00:00:15,920 --> 00:00:22,720
When I sit down, I can meditate for a short time, but eventually I get frustrated.

5
00:00:22,720 --> 00:00:29,120
My muscles cramp up and it feels like my head is shaking and it becomes really difficult,

6
00:00:29,120 --> 00:00:34,240
so I end up just getting up. Whatever it is, I make an excuse.

7
00:00:34,800 --> 00:00:41,200
I guess my ego is against it or something. I hope it is not a frivolous question,

8
00:00:41,200 --> 00:00:47,520
because a lot of people recommend just sitting through it. Either I do not have the willpower

9
00:00:47,520 --> 00:00:53,600
for it or I am just lazy, but is there any practical advice for that?

10
00:00:53,600 --> 00:01:01,280
Answer. We sometimes make the mistake of forcing yourself to meditate,

11
00:01:01,280 --> 00:01:06,240
which is not great, because it will cultivate an aversion to meditation.

12
00:01:07,200 --> 00:01:11,520
That being said, in the beginning, you do not know what is good for you.

13
00:01:12,160 --> 00:01:17,600
The untrained mind is like a child. Parents have to push children to learn.

14
00:01:17,600 --> 00:01:25,120
They do not force them, but they have to at least direct them. Forcing does not generally

15
00:01:25,120 --> 00:01:31,280
have the desired effect, and as a result, parents who push their children to hard,

16
00:01:31,280 --> 00:01:40,960
find a rebel or worse. Pushing a child too hard can destroy their lives due to intense stress.

17
00:01:40,960 --> 00:01:49,840
In terms of mental development, most people are like children. They have never truly grown up.

18
00:01:50,560 --> 00:01:58,160
The spiritual part of most of us is quite immature. This is especially true if you are not

19
00:01:58,160 --> 00:02:05,440
comfortable practicing meditation. When first starting out, you have to treat your mind like

20
00:02:05,440 --> 00:02:14,560
a child. In the beginning, you have to be a little bit insistent. Consider how you would teach

21
00:02:14,560 --> 00:02:22,880
a child. Hopefully, you would not force your will on a child to the extent that it would cause them

22
00:02:22,880 --> 00:02:30,160
unwarranted stress and suffering. But on the other hand, you would not just let your child sit

23
00:02:30,160 --> 00:02:37,600
around eating candy or watching call tunes all day. Instead, you would try to direct them

24
00:02:37,600 --> 00:02:45,680
skillfully in a wholesome direction. There is no easy answer to cultivating the desire to

25
00:02:45,680 --> 00:02:54,000
meditate in the beginning stages of the practice. Accept to say, do not simply force yourself to

26
00:02:54,000 --> 00:03:02,480
meditate. And the other hand, do not say, well, then I just want meditates. I am sure you know

27
00:03:02,480 --> 00:03:10,080
this otherwise you would not be asking this question. You want to meditate. You just do not want to

28
00:03:10,080 --> 00:03:17,920
meditate. You know, it is good for you intellectually because intellectually you are all grown up.

29
00:03:17,920 --> 00:03:26,240
But part of you is still a child. A big part of the problem is a disconnect from the actual

30
00:03:26,240 --> 00:03:33,840
benefits of meditation. You are not machines where you can input the benefits of meditation

31
00:03:33,840 --> 00:03:41,600
and have those inputs stored in our programming. You can tell yourself the benefits or listen

32
00:03:41,600 --> 00:03:48,800
to a talk on how great meditation is and think, meditation is awesome. I want to meditate.

33
00:03:48,800 --> 00:03:57,120
But then your mind will change and you will lose this feeling. It is common during beginner

34
00:03:57,120 --> 00:04:04,720
meditation courses where you will be practicing and say, wow, this is so wonderful. Meditation

35
00:04:04,720 --> 00:04:12,560
helps me so much. And the next day you will feel completely different and say, I want to leave.

36
00:04:12,560 --> 00:04:21,440
This is useless. What am I even doing here? The reason for this is that we are organic. Our mind

37
00:04:21,440 --> 00:04:30,160
is part of the organism and it is not like a computer. It is going to act irrationally. So,

38
00:04:30,160 --> 00:04:39,280
a part of the practice is going to be stepping back and saying, okay, yes, I do not want to meditate.

39
00:04:39,280 --> 00:04:48,320
I acknowledge that and not ignoring it but not clinging to it either. When you acknowledge the

40
00:04:48,320 --> 00:04:56,480
mindset that does not want to meditate and you will find that you are able to take the frustration

41
00:04:56,480 --> 00:05:02,960
and disliking as an object and you find it evaporates with patient observation.

42
00:05:04,320 --> 00:05:13,920
Try to be very aware of the aversion to meditation. Understanding that after some time you do grow up

43
00:05:14,960 --> 00:05:22,400
and as you grow up you will want to meditate more. This is not an insult. Most of us

44
00:05:22,400 --> 00:05:31,680
start out in a similar situation. Understand that in the beginning you are going to have

45
00:05:31,680 --> 00:05:38,080
to coax and control and play games with your inner child to help it grow up.

46
00:05:38,880 --> 00:05:45,040
Beyond this, there is room for some amount of philosophical conversation with yourself

47
00:05:45,040 --> 00:05:51,680
about the benefits of meditation. As long as you acknowledge both sides of the argument,

48
00:05:52,480 --> 00:06:00,240
if you do not give the defiaments their fair trial, once the aversion to meditation arises,

49
00:06:00,240 --> 00:06:07,280
it is too late. It is already on wholesome. So, pushing it away is not going to help.

50
00:06:07,280 --> 00:06:16,240
This is why the Buddha said that when aversion arises in the mind, the key is to see that

51
00:06:16,240 --> 00:06:25,440
aversion has a reason in the mind and not be prejudiced against it before you seek clearly its true nature.

52
00:06:26,560 --> 00:06:33,120
This is how a judge works. They do not judge, rather they observe and they come to decision

53
00:06:33,120 --> 00:06:40,800
based on the fact instead of any kind of judgment. You should be able to feel the difference

54
00:06:40,800 --> 00:06:48,000
between forcing yourself to accept something and truly understanding it. Just like some religious

55
00:06:48,000 --> 00:06:55,280
people believing God because they are afraid to go to help or their parents have told them to

56
00:06:56,080 --> 00:07:02,160
or even intellectually as opposed to actually truly understanding whether God exists,

57
00:07:02,160 --> 00:07:09,200
you will feel the difference between accepting meditation because someone told you it was beneficial

58
00:07:09,200 --> 00:07:16,240
or because intellectually you believe it is right and actually getting a sense of how good

59
00:07:16,240 --> 00:07:37,360
meditation is for you. The latter is much more valuable.

