1
00:00:00,000 --> 00:00:03,800
Hello, welcome back to Ask a Monk.

2
00:00:03,800 --> 00:00:08,640
Today some quick questions, you can really do the easy to answer.

3
00:00:08,640 --> 00:00:16,400
The first one is in regards to monks' names, because clearly it's clearly observable that

4
00:00:16,400 --> 00:00:25,240
Buddhist monks have different names, names that were obviously taken when they ordained.

5
00:00:25,240 --> 00:00:31,560
The origin of this is not really clear to me.

6
00:00:31,560 --> 00:00:38,960
I think what I would guess is that it's sort of progressed as time went on.

7
00:00:38,960 --> 00:00:47,200
What we can clearly see is in the Buddhist time most people are dating as monks, kept their

8
00:00:47,200 --> 00:00:52,520
original name, and there were exceptions to this.

9
00:00:52,520 --> 00:01:01,840
And it seems those exceptions were exceptional for the most part, the original names.

10
00:01:01,840 --> 00:01:09,960
That was no problem because the names were already in politics, and this was the language

11
00:01:09,960 --> 00:01:11,760
that they were speaking.

12
00:01:11,760 --> 00:01:18,320
When they did the ordination ceremony, there was never any thought that the name would

13
00:01:18,320 --> 00:01:25,880
have to fit the language of the, not the ceremony, but the act, the formal act of ordination.

14
00:01:25,880 --> 00:01:31,840
The problem was that this act became formalized, or it was set down as a formal act

15
00:01:31,840 --> 00:01:38,520
that had to use a specific language, and so when the Buddhist teaching traveled to other

16
00:01:38,520 --> 00:01:47,920
places, they still used this old language, and so I would assume that that is where the

17
00:01:47,920 --> 00:02:00,080
need to change people's names to poly, or to give them poly names arose, and so how that

18
00:02:00,080 --> 00:02:01,080
worked exactly.

19
00:02:01,080 --> 00:02:04,520
I can't guess, and I'm sure there are people who are more knowledgeable in this than

20
00:02:04,520 --> 00:02:14,320
I am, but how it works today depends very much on where you are, probably the most reasonable

21
00:02:14,320 --> 00:02:23,480
method of choosing a Buddhist monk's name is based on the person's name in whatever

22
00:02:23,480 --> 00:02:28,400
language that they are, where you simply change it to fit the poly language.

23
00:02:28,400 --> 00:02:35,840
And you do see that to some extent, at least in so far as they try to find poly words

24
00:02:35,840 --> 00:02:43,520
that fit with the person's name, so I can't remember some examples, but I remember trying

25
00:02:43,520 --> 00:02:50,920
to figure out what was a good poly name that sounded like, or poly words, a term that sounded

26
00:02:50,920 --> 00:03:02,160
like a person's name, but how it works a lot of the time is that they will pick a name

27
00:03:02,160 --> 00:03:15,960
that has some meaning, and it could be a fairly pretentious or elevated sort of name, and

28
00:03:15,960 --> 00:03:21,960
that's what you find in Thailand, for the greater part.

29
00:03:21,960 --> 00:03:33,560
You'll find them picking names that sound good or that have some greatness to them.

30
00:03:33,560 --> 00:03:42,200
So my name, utadamu, from utadamu, or whatever you want to say it, means according to

31
00:03:42,200 --> 00:03:52,560
the Thai books, one who has the dhamma composed, and it's a difficult one to translate

32
00:03:52,560 --> 00:03:57,920
and the translation is loose as something, but the meaning is one who has mastered the

33
00:03:57,920 --> 00:04:04,440
teaching or so on, or has it not mastered, but has a good grasp of the Buddhist teaching

34
00:04:04,440 --> 00:04:07,640
or the dhamma or of truth or of reality.

35
00:04:07,640 --> 00:04:13,600
So when I asked my teacher about the name, he changed it a little, he said, it means

36
00:04:13,600 --> 00:04:20,880
someone who is composed of dhamma, who is made up of dhamma, which is, which I like a

37
00:04:20,880 --> 00:04:27,040
lot, first of all, because it's less ostentatious, but also because it's true, we're

38
00:04:27,040 --> 00:04:34,400
all made up of realities, and it's a reminder to look inside, to find the truth, and

39
00:04:34,400 --> 00:04:41,680
simply coming to understand ourselves is the understanding of reality.

40
00:04:41,680 --> 00:04:48,200
Another way that people choose the names is based on the teacher, so I know when my

41
00:04:48,200 --> 00:04:56,720
teacher would pick names for nuns, for example, who came and asked him for a name, because

42
00:04:56,720 --> 00:05:00,640
for monks it was often taken in a fairly formal way, and they picked it out of a book

43
00:05:00,640 --> 00:05:07,480
based on, oh that's right, my name was based on this formal system of the day you were

44
00:05:07,480 --> 00:05:08,480
born.

45
00:05:08,480 --> 00:05:12,440
What day of the week you're born on, they have a book, and they know which letters are

46
00:05:12,440 --> 00:05:17,760
going to go well with that day, and it's based on astrology and numerology or whatever,

47
00:05:17,760 --> 00:05:20,240
I don't know, superstition really.

48
00:05:20,240 --> 00:05:27,440
And so my name is a name for a person who was born on a Wednesday night, which I wasn't

49
00:05:27,440 --> 00:05:31,600
actually, they got it wrong, I was born on a Wednesday morning, which would have given

50
00:05:31,600 --> 00:05:35,960
me another name, but that's another story.

51
00:05:35,960 --> 00:05:41,800
But the other way that they do it, which is also quite reasonable, I think, is to give

52
00:05:41,800 --> 00:05:47,280
it based on one's teacher, and you'll find that here in Sri Lanka as well, I think.

53
00:05:47,280 --> 00:05:53,800
So if my name is Yuta Dhamma, I might give all my students names that have the word

54
00:05:53,800 --> 00:06:00,440
Yuta in them, or the word Dhamma in them, to sort of put my mark for my brand on my students,

55
00:06:00,440 --> 00:06:06,920
and I've seen this done, I don't do it, but I haven't done it with people whose names

56
00:06:06,920 --> 00:06:15,640
I've chosen, but I don't think I would do such a thing, but my teacher does that, he gives

57
00:06:15,640 --> 00:06:20,920
them my teacher's name is Siri Mangalow, and he'll give people the words starting with

58
00:06:20,920 --> 00:06:27,800
Siri, so Siri, this Siri that, for the most part, I've seen him give a lot of those

59
00:06:27,800 --> 00:06:28,800
names.

60
00:06:28,800 --> 00:06:32,360
So this is one of those questions that doesn't really, you know, there's nothing

61
00:06:32,360 --> 00:06:38,000
deeper or spiritual about the question, but it's one that crops up and a lot of people

62
00:06:38,000 --> 00:06:58,080
ask and at least wonder, so good to answer, thanks for the question.

