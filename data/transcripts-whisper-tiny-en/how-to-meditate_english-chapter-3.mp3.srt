1
00:00:00,000 --> 00:00:10,480
How to meditate by you to Domobiku, read by Adirokes, chapter 3, walking meditation.

2
00:00:10,480 --> 00:00:15,400
In this chapter, I will explain the technique of walking meditation.

3
00:00:15,400 --> 00:00:20,960
As with sitting meditation, the focus of walking meditation is on keeping the mind in the

4
00:00:20,960 --> 00:00:27,520
present moment and aware of phenomena as they arise in order to create clear awareness

5
00:00:27,520 --> 00:00:29,640
of one's reality.

6
00:00:29,640 --> 00:00:35,240
Even its similarity to sitting meditation, one might wonder what purpose walking meditation

7
00:00:35,240 --> 00:00:36,720
serves.

8
00:00:36,720 --> 00:00:42,160
If one is not able to practice walking meditation, one will still be able to gain benefit

9
00:00:42,160 --> 00:00:44,560
from sitting meditation.

10
00:00:44,560 --> 00:00:50,520
But walking meditation has several unique benefits that make it a good complement and precursor

11
00:00:50,520 --> 00:00:52,640
to sitting meditation.

12
00:00:52,640 --> 00:00:56,400
I will enumerate the five traditional benefits here.

13
00:00:56,400 --> 00:01:05,840
These five benefits are taken from Lichten Kama Sinta in the Angutura Nikaya, 5.1.3.9.

14
00:01:05,840 --> 00:01:09,840
First, walking meditation provides physical fitness.

15
00:01:09,840 --> 00:01:16,960
If we spend all of our time sitting still, our bodies will become weak and incapable of exertion.

16
00:01:16,960 --> 00:01:22,640
Walking meditation maintains basic fitness even for an ardent meditator and can be seen

17
00:01:22,640 --> 00:01:26,880
as a supplement for physical exercise.

18
00:01:26,880 --> 00:01:32,080
Second, walking meditation cultivates patience and endurance.

19
00:01:32,080 --> 00:01:38,520
Since walking meditation is active, it doesn't require as much patience as sitting still.

20
00:01:38,520 --> 00:01:45,200
It is a useful intermediary between ordinary activity and formal sitting meditation.

21
00:01:45,200 --> 00:01:50,480
Third, walking meditation helps to cure sickness in the body.

22
00:01:50,480 --> 00:01:56,280
Whereas sitting meditation puts the body in a state of homeostasis, walking meditation

23
00:01:56,280 --> 00:02:04,240
stimulates blood flow and biological activity while being gentle enough to avoid aggravation.

24
00:02:04,240 --> 00:02:12,600
It also helps to relax the body, reducing tension and stress by moving slowly and methodically.

25
00:02:12,600 --> 00:02:17,680
Walking meditation is thus useful in helping to overcome sicknesses like heart disease

26
00:02:17,680 --> 00:02:22,720
and arthritis, as well as maintaining basic general health.

27
00:02:22,720 --> 00:02:27,920
Fourth, walking meditation aids in healthy digestion.

28
00:02:27,920 --> 00:02:33,560
The greatest disadvantage to sitting meditation is that it can actually inhibit proper digestion

29
00:02:33,560 --> 00:02:35,520
of food.

30
00:02:35,520 --> 00:02:41,440
Walking meditation, on the other hand, stimulates the digestive system, allowing one to

31
00:02:41,440 --> 00:02:48,240
continue one's meditation practice without having to compromise one's physical health.

32
00:02:48,240 --> 00:02:54,080
Fifth, walking meditation helps one to cultivate balanced concentration.

33
00:02:54,080 --> 00:02:59,160
If one were to only practice sitting meditation, one's concentration may be either

34
00:02:59,160 --> 00:03:05,360
too weak or too strong, leading either to distraction or lethargy.

35
00:03:05,360 --> 00:03:12,320
As walking meditation is dynamic, it allows both body and mind to settle naturally.

36
00:03:12,320 --> 00:03:18,080
If done before sitting meditation, walking meditation will help ensure a balanced state of

37
00:03:18,080 --> 00:03:21,640
mind during the subsequent sitting.

38
00:03:21,640 --> 00:03:25,440
The method of walking meditation is as follows.

39
00:03:25,440 --> 00:03:32,520
One, the feet should be close together, almost touching, and should stay side by side throughout

40
00:03:32,520 --> 00:03:34,360
the meditation.

41
00:03:34,360 --> 00:03:39,840
Either one in front of the other, nor with great space between their pads.

42
00:03:39,840 --> 00:03:46,600
The hands should be clasped, right holding left, either in front or behind the body.

43
00:03:46,600 --> 00:03:54,360
Two, the hands should be clasped, right holding left, either in front or behind the body.

44
00:03:54,360 --> 00:04:00,960
Three, the eyes should be open throughout the meditation, and one's gaze should be fixed

45
00:04:00,960 --> 00:04:07,560
on the path ahead, about two meters or six feet in front of the body.

46
00:04:07,560 --> 00:04:14,480
Four, one should walk in a straight line, somewhere between three to five meters, or ten

47
00:04:14,480 --> 00:04:17,400
to fifteen feet in length.

48
00:04:17,400 --> 00:04:23,160
Five, one begins by moving the right foot forward one foot length, with the foot parallel

49
00:04:23,160 --> 00:04:24,960
to the ground.

50
00:04:24,960 --> 00:04:30,280
The entire foot should touch the ground at the same time, with back of the heel in line

51
00:04:30,280 --> 00:04:35,280
width, and to the right of the toes of the left foot.

52
00:04:35,280 --> 00:04:42,480
Six, the movement of each foot should be fluid and natural, a single arcing motion from

53
00:04:42,480 --> 00:04:49,000
beginning to end, with no breaks or abrupt change in direction of any kind.

54
00:04:49,000 --> 00:04:55,360
Seven, one then moves the left foot forward, passing the right foot to come down with the

55
00:04:55,360 --> 00:05:01,920
back of the heel, in line width, and to the left of the toes of the right foot, and

56
00:05:01,920 --> 00:05:06,360
so on, one foot length for each step.

57
00:05:06,360 --> 00:05:14,560
Eight, as one moves each foot, one should make a mental note just as in the sitting meditation,

58
00:05:14,560 --> 00:05:19,680
using a mantra that captures the essence of the movement as it occurs.

59
00:05:19,680 --> 00:05:28,320
The word, in this case, is stepping right when moving the right foot, and stepping left

60
00:05:28,320 --> 00:05:30,680
when moving the left foot.

61
00:05:30,680 --> 00:05:36,720
Nine, one should make the mental note at the exact moment of each movement, neither

62
00:05:36,720 --> 00:05:39,840
before or after the movement.

63
00:05:39,840 --> 00:05:45,800
If the mental note, stepping right, is made before the foot moves, one is noting something

64
00:05:45,800 --> 00:05:48,360
that has not yet occurred.

65
00:05:48,360 --> 00:05:54,520
If one moves the foot first, and then notes, stepping right, one is noting something in

66
00:05:54,520 --> 00:05:56,520
the past.

67
00:05:56,520 --> 00:06:02,080
Either way, this cannot be considered meditation, as there is no awareness of reality in

68
00:06:02,080 --> 00:06:04,240
either case.

69
00:06:04,240 --> 00:06:11,040
To clearly observe the movements as they occur, one should note step at the beginning

70
00:06:11,040 --> 00:06:19,400
of the movement, just as the foot leaves the floor, being as the foot moves forward,

71
00:06:19,400 --> 00:06:24,680
and right the moment when the foot touches the floor again.

72
00:06:24,680 --> 00:06:29,880
The same method should be employed when moving the left foot, and one's awareness should

73
00:06:29,880 --> 00:06:37,280
move between the movements of each foot, from one end of the path to the other.

74
00:06:37,280 --> 00:06:43,400
On reaching the end of the walking path, turn around and walk in the other direction.

75
00:06:43,400 --> 00:06:49,520
The method of turning while maintaining clear awareness is to first stop, bringing whichever

76
00:06:49,520 --> 00:06:57,440
foot is behind to stand next to the foot that is in front, saying to oneself, stopping,

77
00:06:57,440 --> 00:07:02,280
stopping, stopping, as the foot moves.

78
00:07:02,280 --> 00:07:09,280
Once one is standing still, one should become aware of the standing position as standing,

79
00:07:09,280 --> 00:07:15,560
standing, standing, then begin to turn around as follows.

80
00:07:15,560 --> 00:07:22,000
One, lift the right foot completely off the floor and turn it 90 degrees to place it again

81
00:07:22,000 --> 00:07:27,480
on the floor, noting one's turning.

82
00:07:27,480 --> 00:07:35,000
It is important to extend the word to cover the whole of the movement, so the turn should

83
00:07:35,000 --> 00:07:42,160
be at the beginning of the movement, and the ping should be at the end, as the foot touches

84
00:07:42,160 --> 00:07:43,920
the floor.

85
00:07:43,920 --> 00:07:50,800
Two, lift the left foot off the floor and turn it 90 degrees to stand by the right foot,

86
00:07:50,800 --> 00:07:57,800
putting just the same, turning.

87
00:07:57,800 --> 00:08:07,360
Repeat the movements of both feet one more time, turning, right foot, turning, left foot,

88
00:08:07,360 --> 00:08:14,800
and then note, standing, standing, standing.

89
00:08:14,800 --> 00:08:22,160
Four, continue with the walking meditation in the opposite direction, noting, stepping

90
00:08:22,160 --> 00:08:27,240
right, stepping left, as before.

91
00:08:27,240 --> 00:08:33,840
During walking meditation, if thoughts, feelings, or emotions arise, one can choose to

92
00:08:33,840 --> 00:08:41,520
ignore them, bringing the mind back to the feet in order to maintain focus and continuity.

93
00:08:41,520 --> 00:08:48,600
If, however, they become a distraction, one should stop moving, bringing the backfoot forward

94
00:08:48,600 --> 00:08:58,680
to stand with the front foot, saying to oneself, stopping, stopping, stopping, then

95
00:08:58,680 --> 00:09:08,920
standing, standing, standing, and begin to contemplate the distraction as in sitting meditation,

96
00:09:08,920 --> 00:09:25,440
thinking, thinking, thinking, pain, pain, pain, angry, sad, bored, happy, etc., according

97
00:09:25,440 --> 00:09:28,040
to the experience.

98
00:09:28,040 --> 00:09:34,360
Once the object of attention disappears, continue with the walking as before, stepping

99
00:09:34,360 --> 00:09:38,120
right, stepping left.

100
00:09:38,120 --> 00:09:43,640
In this way, one simply paces back and forth, walking in one direction until reaching

101
00:09:43,640 --> 00:09:51,640
the end of the designated path, then turning around and walking in the other direction.

102
00:09:51,640 --> 00:09:57,760
Generally speaking, one should try to balance the amount of time spent in walking meditation,

103
00:09:57,760 --> 00:10:03,760
with the amount of time spent in sitting meditation, to avoid partiality to one or the

104
00:10:03,760 --> 00:10:05,800
other posture.

105
00:10:05,800 --> 00:10:11,280
If one were to practice 10 minutes of walking meditation, for example, one should follow

106
00:10:11,280 --> 00:10:15,080
it with 10 minutes of sitting meditation.

107
00:10:15,080 --> 00:10:19,800
This concludes the explanation of how to practice walking meditation.

108
00:10:19,800 --> 00:10:25,400
Again, I urge you not to be content with simply reading this book.

109
00:10:25,400 --> 00:10:32,800
Please, try the meditation techniques for yourself and see the benefits they bring for yourself.

110
00:10:32,800 --> 00:10:38,880
Thank you for your interest in the meditation practice, and again, I wish you peace, happiness

111
00:10:38,880 --> 00:11:08,720
and freedom from suffering.

