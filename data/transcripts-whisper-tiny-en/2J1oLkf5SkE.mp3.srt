1
00:00:00,000 --> 00:00:09,000
After a psychedelic experience, I realized the ego is not real. I feel this has had a lasting impression on me.

2
00:00:09,000 --> 00:00:16,000
I have tried to use meditation to help me stay tethered to this state, but I feel myself clinging to old patterns.

3
00:00:16,000 --> 00:00:19,000
What should I do?

4
00:00:19,000 --> 00:00:25,000
We should break this up into, I guess, let's say two parts.

5
00:00:25,000 --> 00:00:31,000
I can see two parts here that are essential, two different things that you have to be able to break up.

6
00:00:31,000 --> 00:00:41,000
The Buddha was described as a wee budgeuad, one who distinguishes the Buddha's teaching

7
00:00:41,000 --> 00:00:47,000
is his ability to separate realities and not get the muddled.

8
00:00:47,000 --> 00:00:55,000
Now, you say you had a realization, no, that's momentary. There's a realization that the ego is not real.

9
00:00:55,000 --> 00:00:58,000
Then you say this has a lasting impression on you.

10
00:00:58,000 --> 00:01:03,000
So something resulted from that realization. These are two different things.

11
00:01:03,000 --> 00:01:12,000
So you want to stay tethered to the state, which for sure is the second one, a ladder.

12
00:01:12,000 --> 00:01:21,000
Usually, pleasant, calm, peaceful, something like that, something positive, something that is actually clung to.

13
00:01:21,000 --> 00:01:30,000
That's a result. It's something that is pleasant. It's a relief from suffering, but it's also impermanent suffering in oneself.

14
00:01:30,000 --> 00:01:40,000
So when you cling to that, you feel unpleasant, you feel frustrated, you feel discouraged, because you can't cling to it, because it disappears.

15
00:01:40,000 --> 00:01:46,000
Now, the knowledge, well, it doesn't really matter. The knowledge came and went.

16
00:01:46,000 --> 00:01:51,000
The knowledge was something, the realization was something that most likely came from here.

17
00:01:51,000 --> 00:02:00,000
You say it came from a psychedelic experience, but it was a trigger for some peaceful, pleasant, powerful state,

18
00:02:00,000 --> 00:02:09,000
some state of a state that you equate with a realization, with an enlightenment of some sort.

19
00:02:09,000 --> 00:02:18,000
But that state is a positive state, meaning positive in the sense that it's something that has arisen, and anything that arises ceases.

20
00:02:18,000 --> 00:02:22,000
So that state is not something that should be clinging to.

21
00:02:22,000 --> 00:02:28,000
In fact, what you're clinging to, you're not just clinging to old patterns. You're also clinging to this state.

22
00:02:28,000 --> 00:02:42,000
So you're trying to stay tethered to a state, which is wrong, which is a cause for clinging.

23
00:02:42,000 --> 00:02:49,000
It's something that will create more attachment, partiality, your partial to this state.

24
00:02:49,000 --> 00:02:56,000
So why I'm giving this such a treatment is because this is common for people to have these realizations and then say,

25
00:02:56,000 --> 00:03:01,000
but I couldn't keep it. What do you mean you couldn't keep it? A realization is a moment.

26
00:03:01,000 --> 00:03:07,000
So what you're talking about keeping is this powerful state that's temporary, but comes from a release.

27
00:03:07,000 --> 00:03:17,000
It's something that is a relief to you, and so you cling to that, and as a result wanted to come back.

28
00:03:17,000 --> 00:03:26,000
Realizations are like, you could say like meals along the way, they rejuvenate you.

29
00:03:26,000 --> 00:03:37,000
But they're not, it's not the realization that we're looking for. The final goal is something that doesn't, isn't clung to, can't be clung to.

30
00:03:37,000 --> 00:03:42,000
It's not a positive state in the sense that it doesn't arise. It's the cessation.

31
00:03:42,000 --> 00:03:48,000
And that's nirvana, and there will be no clinging, there can be no clinging to that. It's something different.

32
00:03:48,000 --> 00:03:56,000
So be very careful not to cling to the product of your realizations, these insights that come, don't cling to them.

33
00:03:56,000 --> 00:04:05,000
It's called nyan, that when we cling to knowledge, it's a opaquilase, it's a defilement or detriment to our practice.

34
00:04:05,000 --> 00:04:12,000
Keeps us from progressing.

