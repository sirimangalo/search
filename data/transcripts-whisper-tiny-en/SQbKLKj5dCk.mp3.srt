1
00:00:00,000 --> 00:00:05,520
So my name is Anaton and I have two questions exactly for you and one of them is

2
00:00:05,520 --> 00:00:09,720
do you have any practical advice for getting past bad habits like

3
00:00:09,720 --> 00:00:13,780
convictions for example like sex but I know there's like a whole like

4
00:00:13,780 --> 00:00:17,520
treasure trove advice from these things but I can't seem to find something

5
00:00:17,520 --> 00:00:23,280
that I can actually put into use that I'm not lazy about it you know maybe it's

6
00:00:23,280 --> 00:00:32,520
just me but I don't know so that's my first question I guess well habits are

7
00:00:32,520 --> 00:00:37,320
difficult for that very reason I mean there's no easy answer it could take

8
00:00:37,320 --> 00:00:42,240
you lifetimes to change your bad habits and hopefully it doesn't and it

9
00:00:42,240 --> 00:00:49,200
depends on you how intensive the actual practice I mean clearly it's our point of

10
00:00:49,200 --> 00:00:55,440
you as it's simply the the technique that we follow but specifically for

11
00:00:55,440 --> 00:01:00,640
addiction the Buddha did give certain advice and I've sort of you know

12
00:01:00,640 --> 00:01:05,960
adapted it and pinpointed and so I did this video that was actually a little

13
00:01:05,960 --> 00:01:13,480
bit probably too I think it's a bit too long but it exploded that's one of

14
00:01:13,480 --> 00:01:17,480
my most popular videos so if you look up pornography if you haven't seen my

15
00:01:17,480 --> 00:01:22,080
video on pornography and masturbation I think is it's of course you know those

16
00:01:22,080 --> 00:01:28,120
keywords make it a very very popular video but basically what it says is

17
00:01:28,120 --> 00:01:34,600
there are three things that you have to focus on and the Buddha actually

18
00:01:34,600 --> 00:01:42,320
mentioned these these three as being important anyone who is is any teacher

19
00:01:42,320 --> 00:01:47,640
worth their salt should be teaching these three things the first is the

20
00:01:47,640 --> 00:01:54,200
experience itself you know the the physical seeing or hearing or smelling or

21
00:01:54,200 --> 00:02:01,440
tasting or feeling so if it's a physical a physical now physical contact or

22
00:02:01,440 --> 00:02:05,480
if it's a seeing of something you know so sex for example if you see a

23
00:02:05,480 --> 00:02:10,880
beautiful woman so the first thing is to focus on the scene the try and

24
00:02:10,880 --> 00:02:17,280
experience the aspect that is just seeing forget about the rest of it the

25
00:02:17,280 --> 00:02:22,760
second aspect is the feeling pleasure and with addiction it's going to be

26
00:02:22,760 --> 00:02:26,600
pleasure it can sometimes be calm so you have to watch out for that as well if

27
00:02:26,600 --> 00:02:32,000
you feel just calm you know peaceful but either way one or the other there's

28
00:02:32,000 --> 00:02:37,680
going to be that feeling so you have to try to focus just on that feeling and

29
00:02:37,680 --> 00:02:44,120
the third one is the the craving the desire so this can either be liking or

30
00:02:44,120 --> 00:02:50,280
wanting it's either or if you like something try to focus just on the desire the

31
00:02:50,280 --> 00:02:54,880
liking of it if you want something that you don't have try to focus just on the

32
00:02:54,880 --> 00:03:02,360
wanting now the point is to to separate these three because the ordinary

33
00:03:02,360 --> 00:03:10,080
experience is to take it as one addiction you know or wanting let's say we

34
00:03:10,080 --> 00:03:16,200
take it as one experience you see the object of your desire and there's the

35
00:03:16,200 --> 00:03:20,600
desire and there's the pleasure involved and wanting and you see oh my gosh

36
00:03:20,600 --> 00:03:26,160
you know here I go again and you think of it as a thing this is an entity this

37
00:03:26,160 --> 00:03:31,680
is this is the core problem this is what a part of what is meant by

38
00:03:31,680 --> 00:03:37,000
non-self is that that's wrong that that's not just wrong it's a problem the

39
00:03:37,000 --> 00:03:41,760
problem is that you see it as a problem you see it as a thing you know it's

40
00:03:41,760 --> 00:03:48,280
it's atomic atomic the word atom means unbreakable indivisible and so

41
00:03:48,280 --> 00:03:52,880
atoms don't actually exist this is the atom was proven to be made up of

42
00:03:52,880 --> 00:04:00,120
smaller things so not actually atomic and this is the case with experience as

43
00:04:00,120 --> 00:04:04,640
well it's made up of individual aspects individual experiences and if you

44
00:04:04,640 --> 00:04:10,000
can break it up into its constituent parts it dissolves entirely the

45
00:04:10,000 --> 00:04:14,920
problem disappears so all you have to do and you can try this and you can see

46
00:04:14,920 --> 00:04:20,400
it means stop seeing it as a problem stop feeling guilty about it stop

47
00:04:20,400 --> 00:04:32,160
analyzing it stop you know rationalizing or or trying to fix the problem just

48
00:04:32,160 --> 00:04:37,400
look at the individual mind-states based on these three things the

49
00:04:37,400 --> 00:04:42,480
experience of seeing you know when you see something try to save yourself

50
00:04:42,480 --> 00:04:50,240
seeing see when you feel something feeling when you feel contact feeling feeling

51
00:04:50,240 --> 00:04:56,720
then try to focus on just the feelings when you have a pleasurable

52
00:04:56,720 --> 00:05:00,920
sensation switch to the pleasure and say to yourself have pleasure pleasure

53
00:05:00,920 --> 00:05:06,600
happy happy or feeling feeling calm calm if it's that's the case and when

54
00:05:06,600 --> 00:05:11,400
that when you notice the desire switch that say to yourself wanting wanting

55
00:05:11,400 --> 00:05:16,160
or liking liking in general pick whichever one is most prevalent at any given

56
00:05:16,160 --> 00:05:21,600
moment and that's going to change for moment to moment so you have to be sharp

57
00:05:21,600 --> 00:05:28,320
you have to be systematic about it moving from you have to be flexible jumping

58
00:05:28,320 --> 00:05:31,800
from one to the other at the same time what I didn't mention in that video

59
00:05:31,800 --> 00:05:35,320
there's actually other things again not actually mentioned in this teaching

60
00:05:35,320 --> 00:05:39,200
in the Buddha guilt you're going to have to actually focus on the guilt and be

61
00:05:39,200 --> 00:05:45,560
mindful of it guilty or feeling I guess or or frustrated or just on sad

62
00:05:45,560 --> 00:05:53,000
however it is and thinking so analyzing and and obsessing and so on you have

63
00:05:53,000 --> 00:05:59,200
to focus on thoughts as well these are two other aspects of it that are also

64
00:05:59,200 --> 00:06:02,360
important but they're the ones that are going to obscure these other three so

65
00:06:02,360 --> 00:06:06,560
if you cut through those two you'll find underneath there are basically three

66
00:06:06,560 --> 00:06:10,320
things that you have to focus on as you do that you'll find yourself able to

67
00:06:10,320 --> 00:06:15,640
deconstruct the habit it's simply a matter of doing that systematically and

68
00:06:15,640 --> 00:06:20,640
that's the problem that's why a teacher is important a meditation center is

69
00:06:20,640 --> 00:06:25,160
important intensive practice is important you know this is something that is

70
00:06:25,160 --> 00:06:32,960
quite deep and strong you know so you need something deep and strong or

71
00:06:32,960 --> 00:06:41,360
to counter it okay right thank you so much you know it's a lot of advice that

72
00:06:41,360 --> 00:06:46,160
they can at one point so my second question and I hope it's not too is it

73
00:06:46,160 --> 00:06:50,560
related it's it's a bit different it's about meditation okay I'm gonna I'm

74
00:06:50,560 --> 00:07:06,960
gonna stop

