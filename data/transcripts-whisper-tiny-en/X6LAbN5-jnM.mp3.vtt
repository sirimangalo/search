WEBVTT

00:00.000 --> 00:07.000
Hi, this is a video on the number two reason why everyone should practice meditation.

00:07.000 --> 00:11.000
So counting down we're at number two, the number two reason why everyone should practice meditation

00:11.000 --> 00:16.000
is because meditation allows you to get yourself on the right path.

00:16.000 --> 00:21.000
Now we understand in life that there are many different paths, many different ways that people could go.

00:21.000 --> 00:29.000
You could practice a path which leads to your destruction, leads to your suffering for the future.

00:29.000 --> 00:33.000
You could practice one that is charitable, a life that is charitable.

00:33.000 --> 00:35.000
You could practice a life that is moral.

00:35.000 --> 00:41.000
You could practice meditative lifestyles, yoga, tai chi, many of the various traditions in Hinduism

00:41.000 --> 00:49.000
or even in the Western religions that teach you to fix and focus your mind to the point

00:49.000 --> 00:54.000
where you gain great states of peace and tranquility.

00:54.000 --> 01:02.000
Now, I'm going to go out on a limb and say that all of these are not what I would consider to be the right path.

01:02.000 --> 01:08.000
It's not that any of these, any of the meditative paths which exist out there are wrong in and of themselves,

01:08.000 --> 01:10.000
but that's not what I mean when I say the right path.

01:10.000 --> 01:15.000
Just as it's not what I mean when I say the right path is that you should become a monk or you should become a Buddhist

01:15.000 --> 01:18.000
or you should become a lawyer or a doctor or so on.

01:18.000 --> 01:23.000
All of these paths exist and there's really actually nothing I have to say about them

01:23.000 --> 01:24.000
as either good or bad.

01:24.000 --> 01:30.000
What we're talking about the right path here is a path which knows how to deal with every situation.

01:30.000 --> 01:33.000
Knows how to deal with the present moment as it arises.

01:33.000 --> 01:37.000
So when something comes up knowing the choice to make knowing the way to go.

01:37.000 --> 01:41.000
So whether you're a lawyer or a doctor, whether you're a Christian or a Buddhist

01:41.000 --> 01:49.000
or whether you're of whatever bent, whatever path you're choosing to follow.

01:49.000 --> 01:55.000
Once we create this clarity of mind, once we really understand things as they are,

01:55.000 --> 01:59.000
we don't give rise to greed and attachment which would color our perceptions.

01:59.000 --> 02:02.000
We don't give rise to anger which would inflame our mind.

02:02.000 --> 02:09.000
We don't give rise to delusion and conceit and views which would muddle our mind and confuse us.

02:09.000 --> 02:12.000
We simply understand things as they are.

02:12.000 --> 02:18.000
So when everything arises it's really amazing how this works and you really have to practice to understand it.

02:18.000 --> 02:24.000
But you can at least have a preview of what it's going to be like to meditate

02:24.000 --> 02:29.000
is that you're able to then understand situations as they arise and as they really are.

02:29.000 --> 02:35.000
So if you're confronted with a difficult situation you're able to understand it much better than before.

02:35.000 --> 02:42.000
It's not that after a few weeks of meditation you're going to become brilliant and super and wise.

02:42.000 --> 02:49.000
But slowly, surely bit by bit through the meditation practice you are able to help yourself and help other people.

02:49.000 --> 02:54.000
You're able to do your work much better than you could ever could before in a much more efficient,

02:54.000 --> 02:59.000
much more practical, much more reasonable, a much wiser way.

02:59.000 --> 03:04.000
Simply by understanding things as they are because of course that's what wisdom means is to understand,

03:04.000 --> 03:06.000
understand things as they are.

03:06.000 --> 03:09.000
And that's really all we're doing in the meditation, understanding things as they are.

03:09.000 --> 03:15.000
For what they really are, not as we wish them to be or want them to be, or not want them to be,

03:15.000 --> 03:24.000
or understanding things in terms of our mental defilements or the things which are impurities in our mind.

03:24.000 --> 03:30.000
Once we get rid of these and we really just purely understand things as they are, then we get on the right track.

03:30.000 --> 03:33.000
And it doesn't have to be as this or that kind of person either.

03:33.000 --> 03:38.000
And of course the world is full of many different lifestyles which we can choose.

03:38.000 --> 03:41.000
We do come to see that many of the lifestyles out there are not good.

03:41.000 --> 03:43.000
And there's many things that we do have to give up.

03:43.000 --> 03:45.000
And these are the ones that lead to destruction.

03:45.000 --> 03:52.000
Basically what we're talking about is learning don't know the difference between a path which leads to our development and a path which leads to our destruction.

03:52.000 --> 03:57.000
And this is what is meant by being on the right path, not that we should live our lives in a certain way.

03:57.000 --> 04:04.000
But that when we do live our lives we take the right paths, we take the right forks in the room, and we make the right decisions.

04:04.000 --> 04:07.000
So this is the number two reason why everyone should practice meditation.

04:07.000 --> 04:12.000
Thanks for tuning in and please look forward to the number one reason in the near future.

