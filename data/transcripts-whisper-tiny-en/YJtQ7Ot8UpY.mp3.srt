1
00:00:00,000 --> 00:00:05,000
Okay, hello and welcome back to our study of the Dhamma Padra.

2
00:00:05,000 --> 00:00:15,000
Today we will be continuing on with verses 44 and 45 with a pair of verses which go as follows.

3
00:00:15,000 --> 00:00:25,000
This is the Dhamma Padra.

4
00:00:25,000 --> 00:00:32,000
This is the Dhamma Padra.

5
00:00:32,000 --> 00:00:53,000
This is the Dhamma Padra.

6
00:00:53,000 --> 00:01:19,000
This is the Dhamma Padra.

7
00:01:19,000 --> 00:01:32,000
Who will bring to perfection this path of Dhamma or this practice of Dhamma that is well taught

8
00:01:32,000 --> 00:01:44,000
just as a skillful person might bring to perfection a bouquet of flowers.

9
00:01:44,000 --> 00:01:50,000
And then the answer is Saeko which means a trainee or one who is training.

10
00:01:50,000 --> 00:02:01,000
One who trains themselves will overcome this earth, this world of Yama together with the angels,

11
00:02:01,000 --> 00:02:08,000
together with its angels or all celestial beings.

12
00:02:08,000 --> 00:02:25,000
The trainer will bring to perfection this path of Dhamma which is well-expounded just as a skillful person would bring to perfection a bouquet of flowers.

13
00:02:25,000 --> 00:02:44,000
So this is actually something that is quite a profound question and answer, something that we can use here to expound the Dhamma or as a basis for teaching the Dhamma.

14
00:02:44,000 --> 00:02:58,000
But the story, as there is a story behind all of these, we have to say where this verse came from, the story is that there were, it is actually a two paragraph story.

15
00:02:58,000 --> 00:03:06,000
The story goes that there were a bunch of monks who were sitting around talking about the earth.

16
00:03:06,000 --> 00:03:15,000
And as monks are people as well, their talk tends to wonder if they are not careful.

17
00:03:15,000 --> 00:03:20,000
And so their talk had wandered into worldly subjects where they began to talk about the earth.

18
00:03:20,000 --> 00:03:30,000
And so they were talking about how the earth in this district was black and the earth in that district was brown and how this district was a very muddy earth.

19
00:03:30,000 --> 00:03:38,000
And this district had gravelly and sandy earth.

20
00:03:38,000 --> 00:03:44,000
And the Buddha came in and asked them what they were talking about and they told them and they said that.

21
00:03:44,000 --> 00:03:52,000
He used this verse as a reminder as a means of instilling some sort of sense of urgency in them.

22
00:03:52,000 --> 00:04:06,000
He said, well that is the external earth but it really has no purpose for you to consider, to concern yourselves with when compared to the inner earth.

23
00:04:06,000 --> 00:04:08,000
The earth that is inside of you.

24
00:04:08,000 --> 00:04:23,000
And the real earth is this world of death. The physical realm that is subject to change.

25
00:04:23,000 --> 00:04:42,000
This aging and fading away and this body that will eventually go back to the earth. The death that is ahead of us for this world which is encompassed from the lower realms all the way up to the angel realms.

26
00:04:42,000 --> 00:04:51,000
So yama loca is considered to mean the lower realms of the hell realms, the animal realms, the ghost realms.

27
00:04:51,000 --> 00:05:06,000
The day of a loca, the day of a con means the angel realms means the human realms, the angels and the gods and so on but those three.

28
00:05:06,000 --> 00:05:21,000
So he said, this is what you should be focusing on. Here you have something that is much more important and you should be concerning yourself with how to understand this earth.

29
00:05:21,000 --> 00:05:37,000
And so he gave this verse. He said, who can overcome this earth? I made them think about this. Think about the dhamma in this way and said, who will do this? Who will be able to overcome this?

30
00:05:37,000 --> 00:05:47,000
Certainly not the people who sit around talking about use the subject. Who will be able to overcome this earth? Whether it be the lower realms or the higher realms.

31
00:05:47,000 --> 00:05:54,000
Who will be able to put into practice and bring to perfection this path and dhamma this path of truth.

32
00:05:54,000 --> 00:06:07,000
Path to realize the truth just as a skillful person would bring to perfection or make a perfect bouquet of flowers.

33
00:06:07,000 --> 00:06:34,000
I think this is useful is because it asks the question of who is it or how can we address the problem that we are faced with?

34
00:06:34,000 --> 00:06:51,000
This suffering that we have ahead of us, the old aim, sickness and death. How do we address the issue of being mortal or how do we address the issue of being subject to suffering?

35
00:06:51,000 --> 00:07:06,000
And how do we understand what is the path? What do we need to do in order to perfect the practice and perfect the path to become free from suffering?

36
00:07:06,000 --> 00:07:28,000
And the Buddha makes a direct definite statement as to what it requires, what it will require for us to do that. And this is the training that a person who trains themselves is one who will be able to overcome the earth, will be able to overcome old age sickness and death.

37
00:07:28,000 --> 00:07:45,000
A person who trains their mind, who trains themselves in morality, who trains themselves in concentration, who trains themselves in wisdom. This is a person who will be able to overcome old age sickness and death.

38
00:07:45,000 --> 00:08:05,000
This is a person who will be able to put into practice the Buddha's teaching, will be able to bring to perfection the Buddha's teaching, will be able to bring to perfection this path of dhamma.

39
00:08:05,000 --> 00:08:17,000
The distinction that we have to make is between the mundane world and the ultimate reality. This is where the Buddha points them in the right direction.

40
00:08:17,000 --> 00:08:29,000
An ordinary person will tend to think that there is no way to overcome old age sickness and death. These are part of life. You think, well, that's part of the bargain. You get to enjoy life.

41
00:08:29,000 --> 00:08:38,000
This life that you have and you have to accept the fact that it's going to end in old age sickness and death.

42
00:08:38,000 --> 00:08:51,000
And so we think in conventional sense, just as these monks were thinking about the conventional earth, so it's actually quite an apt story because there's a parallel in how we look at our lives as well.

43
00:08:51,000 --> 00:09:02,000
We look at life as being sort of a one shot. Even people who claim to believe in the afterlife and so on will tend to focus all of their efforts.

44
00:09:02,000 --> 00:09:20,000
For most of their efforts on finding pleasure and avoiding suffering in this life, trying their best to be free from sickness, trying their best to obtain sensual gratification in this life.

45
00:09:20,000 --> 00:09:32,000
Without making any thought of the bigger picture and the ultimate reality, what's really going on and what's really happening.

46
00:09:32,000 --> 00:09:53,000
Realize that actually old age sickness and death is not just an inevitability of life. It's a constant reoccurrence. And it comes with a cause. Or it comes because of a cause. The cause is birth.

47
00:09:53,000 --> 00:10:09,000
Because that's like one wave crashing against the shore. If you're not able to overcome the things that lead you to be born again, then you have to put up with this again and again.

48
00:10:09,000 --> 00:10:25,000
So putting up with it isn't enough or accepting it isn't enough. We create the cause for it to occur again and again for eternity.

49
00:10:25,000 --> 00:10:40,000
With our addiction, with our attachment, with our concern for the mundane. With all of our attachments, all of our obsessions, all of our worries and our cares on a conventional level.

50
00:10:40,000 --> 00:10:58,000
We lose sight of the ultimate reality. We lose sight of what's really going on or what we're really doing. We lose sight of the cycle, the bigger picture that what we're creating here is actually a cycle. It's not a one shot. It's not linear. It's not like birth goes to death in the line and then that's it.

51
00:10:58,000 --> 00:11:12,000
It's a cycle. So birth goes to death, which leads to birth again. Just like waves crashing against the shore one after another, after another forever.

52
00:11:12,000 --> 00:11:32,000
So the Buddha points out that something needs to change about the way we look at the world. We can't let ourselves follow after our habits and are following our heart, for example.

53
00:11:32,000 --> 00:11:47,000
Something has to change about how we look at the world, how we look at our lives. This is what happens when someone comes to practice the Buddha's teaching, when we listen to the dhamma, when we read the Buddha's teaching.

54
00:11:47,000 --> 00:12:01,000
When we start to affect this change, we realize that there's much more going on than meets the eye. That the things that we cling to are not actually bringing us happiness. They're creating a habit of clinging.

55
00:12:01,000 --> 00:12:17,000
That the things that we bear with are not actually, it's not actually out of patience. It's actually with a lot of suffering and cultivating an aversion towards things.

56
00:12:17,000 --> 00:12:32,000
When we escape from pain by taking medication or avoiding it, we actually cultivate greater and greater aversion towards the pain.

57
00:12:32,000 --> 00:12:41,000
We start to realize that this is not the way out of suffering. We realize this when we begin to train ourselves, starting with morality.

58
00:12:41,000 --> 00:12:55,000
We start to realize that, realize these habits are, well, habit-forming. We become magnified the more we cultivate them.

59
00:12:55,000 --> 00:13:12,000
We undertake morality. We undertake to prevent ourselves, to stop ourselves from following after the habits. We train ourselves to be patient when we would otherwise chase after something.

60
00:13:12,000 --> 00:13:19,000
We want something instead of chasing after it. We stop ourselves. We bring ourselves back to the experience of it.

61
00:13:19,000 --> 00:13:28,000
When you want something, you're very little involved with that actual thing, and you're much more involved with the thoughts and the pleasures that arise in the mind.

62
00:13:28,000 --> 00:13:37,000
When you're able to bring yourself back, this is the first step, is bringing the mind back to reality. It's called morality.

63
00:13:37,000 --> 00:13:49,000
The true morality is in the mind. When you bring your mind back to what's really happening, what's really going on, the essence of the experience.

64
00:13:49,000 --> 00:13:57,000
When you see something and you want it, you look at the seeing and you look at the feeling and you look at the wanting.

65
00:13:57,000 --> 00:14:12,000
When you see that it's really just impersonal phenomena arising and ceasing, you break it apart, and you see that it's because for stress, it's for suffering, not a cause for happiness and contentment.

66
00:14:12,000 --> 00:14:22,000
When you become content without you become with or without you create what we call morality.

67
00:14:22,000 --> 00:14:28,000
As a result of the morality, you begin to develop concentrations, so you train yourself further.

68
00:14:28,000 --> 00:14:32,000
Once you've called a bit morality, you train yourself to keep the mind.

69
00:14:32,000 --> 00:14:37,000
Once you've brought the mind back to the present, you train yourself to keep the mind focused on the present.

70
00:14:37,000 --> 00:14:49,000
When you see something, you train yourself focused on seeing. When you hear something hearing, when you smell, you train yourself to focus on the body, on the feelings, on the thoughts, on the emotions and the senses.

71
00:14:49,000 --> 00:14:54,000
You train yourself to focus on reality, so you cultivate concentration.

72
00:14:54,000 --> 00:15:09,000
As a result, not only are you pulling your mind back, but your mind begins to be disinclined to actually leave the present reality.

73
00:15:09,000 --> 00:15:15,000
All of us sitting here now are sitting listening to this dog.

74
00:15:15,000 --> 00:15:21,000
When we have two things going on, we have the ultimate reality, which is where we think we are, where we'd like to be.

75
00:15:21,000 --> 00:15:28,000
We'd like to be here in ultimate reality sounds technical or special.

76
00:15:28,000 --> 00:15:32,000
It actually just means this, what we think we're in at all times.

77
00:15:32,000 --> 00:15:39,000
So the reality of seeing, the reality of hearing, smelling, tasting, feeling, the reality of our experience.

78
00:15:39,000 --> 00:15:49,000
But something else that's going on is all the activity in the mind, the judgments, the projections, the extrapolations, the identifications.

79
00:15:49,000 --> 00:15:56,000
All of the concepts that arise of people and places and things, all of the past and the future, this is all conceptual.

80
00:15:56,000 --> 00:15:59,000
That's nothing to do with the ultimate reality.

81
00:15:59,000 --> 00:16:05,000
So we begin to, the training that we're looking for is to keep ourselves with the reality.

82
00:16:05,000 --> 00:16:10,000
Keep ourselves here. Here we all are sitting together.

83
00:16:10,000 --> 00:16:16,000
The training is to keep ourselves here and now.

84
00:16:16,000 --> 00:16:20,000
Once we do that, this is the training and concentration.

85
00:16:20,000 --> 00:16:27,000
The third training and the training and wisdom is that once we keep ourselves here, we begin to understand here.

86
00:16:27,000 --> 00:16:29,000
We can understand reality.

87
00:16:29,000 --> 00:16:33,000
We can just see the distinction between reality and illusion.

88
00:16:33,000 --> 00:16:38,000
We see how there's nothing essentially good or bad about anything.

89
00:16:38,000 --> 00:16:47,000
We see that all of the people and places and things, even our own body, it's just a concept that experiences arise and sees come and go.

90
00:16:47,000 --> 00:16:57,000
And because of wisdom, we begin to need less and less effort to focus ourselves.

91
00:16:57,000 --> 00:17:03,000
And less and less effort to bring our minds back, our minds are less inclined to move away.

92
00:17:03,000 --> 00:17:11,000
And more inclined towards focusing more inclined towards peace and calm and clarity of mind.

93
00:17:11,000 --> 00:17:23,000
Not just because we pull our minds back or try to keep our minds fixed, but because we understand the uselessness of chasing after illusion.

94
00:17:23,000 --> 00:17:40,000
This is the training and wisdom. We train ourselves not only to bring our minds back, not only to force our minds to stay, but to have our minds understand the benefit of staying.

95
00:17:40,000 --> 00:17:51,000
The benefit of being in the present moment takes ourselves how much stress is involved with running away and chasing after illusions, chasing after concepts.

96
00:17:51,000 --> 00:18:00,000
So these are the three trainings, and this, the Buddha said, is how one overcomes the bad things in life and cultivates the good things.

97
00:18:00,000 --> 00:18:13,000
So the first part is overcomes the world, we change the deep, overcomes the earth, overcomes all the troubles and all the difficulties and all the stresses and sufferings that are inherent in life.

98
00:18:13,000 --> 00:18:24,000
And how one cultivates the dhamabanda, the path of truth and the path of righteousness, and brings it to perfection.

99
00:18:24,000 --> 00:18:30,000
When you have all of the Buddha's teachings or all of the good things in the world, how do you bring them to perfection?

100
00:18:30,000 --> 00:18:37,000
This is the concise summary of how to do that. One becomes a trainee, one trains oneself.

101
00:18:37,000 --> 00:18:48,000
So one way of understanding Buddhism is it's a training. It's not something that you can wish for or hope for or something that you can be when you put on robes or so on.

102
00:18:48,000 --> 00:18:59,000
It's something that you train in, just as we train our bodies and we train our minds in a world they sense to become proficient at this skill or that skill.

103
00:18:59,000 --> 00:19:14,000
So too we train our minds, we train our hearts in the dhamma, in wholesomeness. And we train them out of unwholesomeness unskillful states, states that cause stress and suffering for us and others.

104
00:19:14,000 --> 00:19:36,000
So this is the teaching in these two verses quite a sort of pithy sort of summary of how one brings the Buddha's teaching or brings all good states to perfection, just as one skillful person would skillful flower maker.

105
00:19:36,000 --> 00:20:00,000
The florist would put together a bouquet of flowers. So all of the good things in our mind, flower and may become in a sense perfect through our abandoning of evil and wholesome states and through the cultivation of good states.

106
00:20:00,000 --> 00:20:18,000
This is the teaching in this verse and that's the dhammapanda. So very much a practical teaching. This is obviously something that we have to use in our meditation. It's something that the highest training in Buddhism is here.

107
00:20:18,000 --> 00:20:35,000
It comes from these three, it comes from the meditative practice of morality, the meditative practice of concentration. Keep bringing the mind back, keeping the mind there and the meditative concentration, meditative practice or training of wisdom of understanding.

108
00:20:35,000 --> 00:21:00,000
The understanding that keeps you out of illusion and delusion, keeps the mind from wandering, keeps the mind in tune with reality. So that's the teaching for today on verses 44 and 45. Thank you for tuning in and I hope you're able to put this into practice and follow the path of all the dhammapanda on your own.

109
00:21:00,000 --> 00:21:07,000
Thank you.

