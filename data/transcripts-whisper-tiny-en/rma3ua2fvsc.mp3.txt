So I just graduated high school, quickly all my friends are moving away.
It's been really hard on my soul. How does one accept this change and grow?
Well, this is growing, really.
The suffering that comes from change is teaching you something.
It's teaching you the disadvantages in clinging.
And it's teaching you really from an empirical, from a real, from a reality.
I'm losing my words here. I'm losing my vocabulary.
It's teaching you from a real, sorry, it's been a long night.
It's really teaching you.
As opposed to just thinking about it and saying, yeah, yeah, clinging has causes suffering.
It's not something that's easy to understand.
But when you experience change, when you lose the things that you cling to, then you see leads to suffering.
And you see clearly from an empirical perspective.
So living with it and learning to accept this as reality and learning to see deeper
is really the core of the Buddha's teaching.
Which is the great thing about this.
The problem is people don't think like that.
They think something's wrong.
And they try to give you an answer.
They say, come on, enjoy where you are now.
Start to, you know, don't worry.
You'll find new friends.
Basically setting you up for the exact same suffering later on.
The exact same disappointment.
And until you learn and realize, really, if you cling to something, it's just going to bring you suffering.
You continue to experience the same disappointment.
So the best thing you can do is to accept it.
And to say, you know, the problem is not that I move.
The problem is that I was clinging.
The problem is that I preferred these people.
And these experiences.
I preferred those.
And I really enjoyed them.
As opposed to accepting present moment experience.
And the experience that you have right now, right?
So the best thing for me right now is to accept that now I'm giving a talk.
Or now I'm answering people's questions.
As you can see, tonight I had to give an ordination.
And then I answered some questions, then ordination, and you should have seen the things that have to go through your mind.
Having to print something up for her.
Having to get a razor and, you know, robes.
And all the time considering in my mind, is this proper?
Is this correct?
What is it?
You know, how exciting the present moment is.
If you stay with it, how much is going on right here and right now?
And how it changes so much.
And if you get stuck on one experience, like I like this part, I don't like this part.
You'll immediately set yourself up for suffering.
So the best thing that you can do is to learn to not find a happiness in cling to the present moment,
but to learn to not cling to anything, and to be happy without relying on any experience.
Thank you.
