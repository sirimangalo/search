1
00:00:00,000 --> 00:00:24,080
Okay, good evening everyone, welcome to our evening dumb session.

2
00:00:24,080 --> 00:00:53,680
So I've been invited a friend, a friend of mine, one of the monks here is putting on a big conference in a couple of weeks, and they needed people to speak at it, so I'm giving a talk, and I don't want to make it sound like somehow they sought me out for it, I think I was just available.

3
00:00:55,080 --> 00:01:15,080
But one of the talks is going to be on, one is going to be on wrong mindfulness, and the other is going to be on Buddhism in a digital age.

4
00:01:15,080 --> 00:01:35,080
So I thought maybe in the next week or so I can go over some of the ideas, test them out on you, my captive audience.

5
00:01:35,080 --> 00:01:44,080
So tonight I thought I'd talk about modern Buddhism, Buddhism in a digital age.

6
00:01:44,080 --> 00:02:04,080
Even just generally a modern age, there's one fairly clear debate or division in modern Buddhism,

7
00:02:04,080 --> 00:02:12,080
and that's between engagement and renunciation.

8
00:02:12,080 --> 00:02:23,080
I mean I think most Buddhism is renunciant, is on the side of renunciation, leaving society going off in the forest.

9
00:02:23,080 --> 00:02:32,080
So when you think of Buddhism, I think it's still more common to think of the Buddha off under a tree in the forest.

10
00:02:32,080 --> 00:03:01,080
But there are those who struggle with that, and I give for Buddhism or feel more comfortable practicing in Buddhism that is engaged, that stays in the world and works to better the world, works to engage with the world rather than disengage.

11
00:03:01,080 --> 00:03:25,080
It's a struggle that Buddhists individually go through and it's a division between various Buddhist institutions, some very social oriented and some very reclusive oriented.

12
00:03:25,080 --> 00:03:47,080
And so this is seen as sort of a modern dilemma, I think, but it really is actually an ancient dilemma.

13
00:03:47,080 --> 00:04:02,080
And the time of the Buddha there was this question of whether one should be engaged or one should be secluded.

14
00:04:02,080 --> 00:04:17,080
And so you have Devadatta, the Buddha's cousin who made all sorts of trouble and did all sorts of wicked things.

15
00:04:17,080 --> 00:04:26,080
He tried to entrap the Buddha by proposing five rules, one of which was that monk should always live in the forest.

16
00:04:26,080 --> 00:04:48,080
And he knew that the Buddha wouldn't accept these rules. And so by proposing them, he wanted to show that the Buddha was soft, was unfit to be the leader, that Devadatta was the true, hard core, dedicated leader.

17
00:04:48,080 --> 00:04:57,080
But the Buddha said, of course, no, he said, no, monks live in the city, they should practice Buddhism, monks live in.

18
00:04:57,080 --> 00:05:11,080
In the forest, they should also practice the Dhamma.

19
00:05:11,080 --> 00:05:27,080
And this isn't even originally a Buddhist debate, you know, this is in the context of Brahmanism, which had this sort of question as well of, how do you fit in this idea of going off in the forest?

20
00:05:27,080 --> 00:05:50,080
And that's sort of this, India's interesting because there's this fairly clear meeting of cultures, the indigenous culture and the Aryan culture, whatever the Aryans came from, these fireworshiping, horse-riding barbarians.

21
00:05:50,080 --> 00:06:07,080
It appears that there's this sort of, there arose this high-brow culture and society that was very metropolitan, very urban.

22
00:06:07,080 --> 00:06:34,080
And then there were these mystics, shamans, where the word shaman probably comes from, shamanic, who were from a fairly indigenous culture, but they were these wise men mostly who went off in the forest and had deep mystical experiences, or maybe they were just crazy.

23
00:06:34,080 --> 00:06:40,080
But we're looked upon as holy men.

24
00:06:40,080 --> 00:06:57,080
And so there's, as society developed, there was a question of how these men fit in and what was the proper, you know, as everything, as all these indigenous cultures go, they eventually become institutionalized.

25
00:06:57,080 --> 00:07:16,080
And so eventually they tried at least, it seems to set up some kind of system because it was disruptive for men to just decide and women sometimes that they were going to leave home and go, go off, live in the forest.

26
00:07:16,080 --> 00:07:44,080
And so they made these four ages, the proper way to do it was to wait until you're old and retire, you're a student and then you're a householder, and then you retire, but live at home, and then once everything is settled, passed on to your children, you'll even go off and live in the forest.

27
00:07:44,080 --> 00:08:03,080
And so this is what the Buddha came into and the Buddha was, of course, one of these men, although he didn't wait to retire, he was quite the rebel.

28
00:08:03,080 --> 00:08:25,080
I mean, I've told him when he was 29 apparently, still just preparing to become leader of his clan.

29
00:08:25,080 --> 00:08:39,080
But I think what you can see from the Buddha's teaching, and maybe you can get a sense of where I'm going with this, is that there isn't a clear division in the Buddha's teaching.

30
00:08:39,080 --> 00:08:44,080
There isn't a decision made on one way or the other.

31
00:08:44,080 --> 00:08:55,080
I think clearly the goal is renouncing the world in one cent.

32
00:08:55,080 --> 00:09:07,080
In another sense, it's to some extent taking other people with you in general.

33
00:09:07,080 --> 00:09:19,080
And thereby working with the world, freedom from the world doesn't come without the world.

34
00:09:19,080 --> 00:09:22,080
It's not like a jail cell, you can just break out of it.

35
00:09:22,080 --> 00:09:26,080
So you're learning in the meditation course.

36
00:09:26,080 --> 00:09:29,080
You can't just get away from your problems.

37
00:09:29,080 --> 00:09:32,080
That's not the way out of suffering.

38
00:09:32,080 --> 00:09:37,080
That's your problems that for you from your problems.

39
00:09:37,080 --> 00:09:47,080
The sati patanasu does a very good, very special teaching, very powerful teaching.

40
00:09:47,080 --> 00:09:54,080
Just simple words when you're angry, know that you're angry, when you're in pain, know that it's pain,

41
00:09:54,080 --> 00:10:02,080
when you're walking, know that you're walking, not running away from anything.

42
00:10:02,080 --> 00:10:07,080
And the world either, to some extent the world is part of our practice.

43
00:10:07,080 --> 00:10:10,080
Other people are part of our practice.

44
00:10:10,080 --> 00:10:19,080
And so engagement is a part of our practice, renunciation and engagement.

45
00:10:19,080 --> 00:10:27,080
And so when I look at Buddhism, I look at this division and I think it's an important one.

46
00:10:27,080 --> 00:10:33,080
But rather than division among Buddhists, it's a division among Buddhist practices.

47
00:10:33,080 --> 00:10:37,080
There are social practices and there are spiritual practice.

48
00:10:37,080 --> 00:10:42,080
They're engaged and there are renunciate practices.

49
00:10:42,080 --> 00:10:52,080
And both I think are arguably or should be, or at least can be, part of our past realignment.

50
00:10:52,080 --> 00:11:00,080
So what I'm going to try to do in my talk, in fact this might be exciting.

51
00:11:00,080 --> 00:11:07,080
I'm going to try to figure out the exact time and see if we can do a second life session live.

52
00:11:07,080 --> 00:11:18,080
So have all of you here with me and then about halfway through, just pop it up and show that you've all been sitting there listening.

53
00:11:18,080 --> 00:11:29,080
Just to show the power of second life really is as a communication platform.

54
00:11:29,080 --> 00:11:46,080
But I'm going to try to highlight some of the tools that I've created or some of the means by which I've used the internet used technology.

55
00:11:46,080 --> 00:12:00,080
And some other people have created, I mean talk about the Buddhist Center for example.

56
00:12:00,080 --> 00:12:08,080
But I want to make clear this distinction that our social practice, I mean a good example of a social practice is made that.

57
00:12:08,080 --> 00:12:16,080
It's a meditative practice.

58
00:12:16,080 --> 00:12:26,080
You just sit quietly alone and send love to the whole world.

59
00:12:26,080 --> 00:12:34,080
But it's social as well in the sense of it changes the way we react to others.

60
00:12:34,080 --> 00:12:40,080
It changes the way we interact with others.

61
00:12:40,080 --> 00:12:48,080
We approach other people with love as a result.

62
00:12:48,080 --> 00:12:52,080
I mean when we talk about engaged Buddhism we mean changing the world.

63
00:12:52,080 --> 00:12:59,080
We mean doing something to improve the world not just running away and freeing ourselves.

64
00:12:59,080 --> 00:13:04,080
The fact is improving your own mind does improve the world.

65
00:13:04,080 --> 00:13:08,080
Manta is a very social practice.

66
00:13:08,080 --> 00:13:16,080
Changing yourself is a very social practice to that and renunciation in a Buddhist sense, which of course is internal.

67
00:13:16,080 --> 00:13:23,080
It has nothing really to do with running away to the forest.

68
00:13:23,080 --> 00:13:30,080
It is in fact a social practice.

69
00:13:30,080 --> 00:13:35,080
In the sense that it changes your social interactions.

70
00:13:35,080 --> 00:13:36,080
It makes you wiser.

71
00:13:36,080 --> 00:13:45,080
I mean the most important thing it does is teaches you, teaches you right from wrong good from bad.

72
00:13:45,080 --> 00:13:59,080
It makes your mind clear when dealing with challenges, difficulties, conflict.

73
00:13:59,080 --> 00:14:06,080
I don't know if there really is this division.

74
00:14:06,080 --> 00:14:15,080
Two sides of the practice, you know, the Buddha answering this question of helping yourself or helping others.

75
00:14:15,080 --> 00:14:18,080
He says when you help yourself you help other people.

76
00:14:18,080 --> 00:14:23,080
When you help other people you help yourself.

77
00:14:23,080 --> 00:14:26,080
I don't think there was a equivalence there.

78
00:14:26,080 --> 00:14:33,080
It is very clear that helping yourself is much better thing to focus on.

79
00:14:33,080 --> 00:14:36,080
I certainly teach things to that extent.

80
00:14:36,080 --> 00:14:42,080
And renunciation and going off to live in the forest is a great and wonderful thing.

81
00:14:42,080 --> 00:14:45,080
But it is certainly not all of Buddhism.

82
00:14:45,080 --> 00:14:54,080
I don't think there is a need to create a division of sorts.

83
00:14:54,080 --> 00:14:58,080
That is the first part of my talk I think.

84
00:14:58,080 --> 00:15:03,080
To talk about all the technology I use, of course that is very interesting.

85
00:15:03,080 --> 00:15:08,080
But I don't know that it is a topic for tonight's Dhamma.

86
00:15:08,080 --> 00:15:12,080
So that is sort of the direction I am heading to talk about.

87
00:15:12,080 --> 00:15:17,080
This idea of engaged versus renunciate.

88
00:15:17,080 --> 00:15:24,080
An unseen, I don't know the actual word.

89
00:15:24,080 --> 00:15:29,080
But for us I think that is important to be clear.

90
00:15:29,080 --> 00:15:30,080
This isn't running away.

91
00:15:30,080 --> 00:15:32,080
Coming here isn't running away.

92
00:15:32,080 --> 00:15:35,080
And your practice doesn't end here in the meditation center.

93
00:15:35,080 --> 00:15:39,080
Mindfulness is a tool that you use in your life.

94
00:15:39,080 --> 00:15:50,080
And it is a tool that you use here to help you with your life to better deal with your challenges and conflicts and so on.

95
00:15:50,080 --> 00:15:54,080
So a little bit of food for thought, Dhamma for tonight.

96
00:15:54,080 --> 00:16:00,080
Thank you all for coming up.

97
00:16:00,080 --> 00:16:02,080
And I have got the question page up.

98
00:16:02,080 --> 00:16:09,080
So I can answer some questions here.

99
00:16:09,080 --> 00:16:13,080
So I am completely coincidental.

100
00:16:13,080 --> 00:16:16,080
And the top question, and it is completely coincidental.

101
00:16:16,080 --> 00:16:21,080
And so the top question is about what my thoughts are and important issues like climate change.

102
00:16:21,080 --> 00:16:25,080
And how realization could help ease people's bias against these crises.

103
00:16:25,080 --> 00:16:33,080
In fact, it is a good point to bring up in my talk.

104
00:16:33,080 --> 00:16:39,080
Because of course things like climate change are brought about by nothing more than greed at the root.

105
00:16:39,080 --> 00:16:47,080
If we weren't so greedy for consumption, even just greedy to procreate too many babies.

106
00:16:47,080 --> 00:16:50,080
That is the Dalai Lama said we need more monks.

107
00:16:50,080 --> 00:16:53,080
There are too many people in this world.

108
00:16:53,080 --> 00:16:55,080
I am not sure about that.

109
00:16:55,080 --> 00:17:05,080
I don't know what the ethics are there, but I don't know.

110
00:17:05,080 --> 00:17:09,080
I don't know about population. I won't touch that one.

111
00:17:09,080 --> 00:17:15,080
But certainly for consumption, our consumption is way out of hand.

112
00:17:15,080 --> 00:17:25,080
And our need for products regardless of the cost, the environment.

113
00:17:25,080 --> 00:17:34,080
Our need for instant gratification is completely even just our need for meat, for example.

114
00:17:34,080 --> 00:17:39,080
We don't need to eat meat. We have no reason to eat meat.

115
00:17:39,080 --> 00:17:48,080
And it is incredibly expensive to the climate and even to our individuals.

116
00:17:48,080 --> 00:17:55,080
So I think our practice certainly improves these.

117
00:17:55,080 --> 00:18:01,080
And I have talked about this before, but I think things like climate change are not incredibly worth focusing on.

118
00:18:01,080 --> 00:18:04,080
Because it is like a band-aid solution.

119
00:18:04,080 --> 00:18:07,080
Sure you get people to be concerned about the climate.

120
00:18:07,080 --> 00:18:09,080
But if you don't...

121
00:18:09,080 --> 00:18:17,080
Well, I mean, looking at the climate does make people more aware of their greed.

122
00:18:17,080 --> 00:18:20,080
And forced people to give up their greed.

123
00:18:20,080 --> 00:18:25,080
I mean, let's be clear that the real problem.

124
00:18:25,080 --> 00:18:35,080
It was an important thing to address is our greed.

125
00:18:35,080 --> 00:18:43,080
Did the Buddha possess any supernatural powers or miracles?

126
00:18:43,080 --> 00:18:49,080
I mean, I've never met the Buddha not in this life, obviously.

127
00:18:49,080 --> 00:19:04,080
But I have no problem believing that the Buddha had powers beyond what most of us would believe possible.

128
00:19:04,080 --> 00:19:12,080
Or powers beyond what most of us are capable of.

129
00:19:12,080 --> 00:19:19,080
We're told to hold our hands up to our stomach as opposed to laying them on our laps as either position. Okay, yes.

130
00:19:19,080 --> 00:19:23,080
The body position in our tradition not so important.

131
00:19:23,080 --> 00:19:28,080
It's all about the mind. The mind is what's more important.

132
00:19:28,080 --> 00:19:36,080
In Theravada, it recently read that only those who practice meditative, monastic life can attain spiritual perfection.

133
00:19:36,080 --> 00:19:41,080
Enlightenment is not thought possible for those living in the secular life.

134
00:19:41,080 --> 00:19:44,080
Of course, it's easier that way, but could it be possible? Yes.

135
00:19:44,080 --> 00:19:46,080
If I ever wrote that doesn't know what they're talking about.

136
00:19:46,080 --> 00:19:50,080
There's a queen who became an hour hunt, very famous example.

137
00:19:50,080 --> 00:19:53,080
It's very Theravada Buddhist, a queen.

138
00:19:53,080 --> 00:19:57,080
And the Buddha said, oh, well, either she becomes a beakuni or she just wastes away.

139
00:19:57,080 --> 00:20:01,080
Because there's no way she's going to be able to be a queen.

140
00:20:01,080 --> 00:20:08,080
And so the kings have all the matter become a beakuni.

141
00:20:08,080 --> 00:20:14,080
Okay, this is a long question of what can read it all, but well, you know, maybe it's nice.

142
00:20:14,080 --> 00:20:18,080
Person who suffered extreme stress for many years, ten plus years,

143
00:20:18,080 --> 00:20:22,080
withdrawn into serious depression, debilitating anxiety.

144
00:20:22,080 --> 00:20:28,080
Been seriously studying your YouTube videos for a few months, but I'm new to consistent daily practice.

145
00:20:28,080 --> 00:20:33,080
And today, I don't know if you've picked up my booklet, so there are many ways to practice,

146
00:20:33,080 --> 00:20:36,080
but I recommend reading my booklet if you haven't.

147
00:20:36,080 --> 00:20:39,080
And I experience something wonderful.

148
00:20:39,080 --> 00:20:44,080
So a sudden burst of warm, intense happiness.

149
00:20:44,080 --> 00:20:46,080
Tears of joy.

150
00:20:46,080 --> 00:20:49,080
Can't recall, or such a overwhelming sensation.

151
00:20:49,080 --> 00:20:51,080
Came out of nowhere.

152
00:20:51,080 --> 00:20:57,080
Half of me just wants to share my excitement, and the other half curious wants to know is this normal.

153
00:20:57,080 --> 00:21:00,080
So well, great.

154
00:21:00,080 --> 00:21:10,080
I mean, it's great for you to be able to see that you're not cursed, you're not stuck.

155
00:21:10,080 --> 00:21:21,080
I would say, I mean, you can see now that your brain is capable of pleasure.

156
00:21:21,080 --> 00:21:32,080
And that the meditation clearly helps you break out of this rut, because our behaviors become habits.

157
00:21:32,080 --> 00:21:38,080
And so things like depression for ten years, that's got you stuck in a fairly deep rut.

158
00:21:38,080 --> 00:21:45,080
But miracle of miracles, just a little bit of meditation, and you can pop yourself back out of it.

159
00:21:45,080 --> 00:21:54,080
Well, I mean, because those ten years, it's not like you add up those ten years, and you've got ten years' worth of depression.

160
00:21:54,080 --> 00:21:56,080
Old stuff fades.

161
00:21:56,080 --> 00:21:58,080
We're still only dealing with the present moment.

162
00:21:58,080 --> 00:22:06,080
So if in this moment you start meditating, people wonder how can a few weeks of meditation counteract years of depression or anxieties?

163
00:22:06,080 --> 00:22:08,080
Because those years don't exist.

164
00:22:08,080 --> 00:22:09,080
They're past.

165
00:22:09,080 --> 00:22:11,080
It's just you counting.

166
00:22:11,080 --> 00:22:15,080
The truth is, well, what are you now, and it's based on what you've done before.

167
00:22:15,080 --> 00:22:21,080
There's no question that all of that had an effect, but you're still just dealing with you and your brain.

168
00:22:21,080 --> 00:22:27,080
And so creating new habits of mindfulness, I mean, it works.

169
00:22:27,080 --> 00:22:29,080
It pops you out of it.

170
00:22:29,080 --> 00:22:36,080
So your next step, of course, is to learn to become mindful of the joy, because that's not the goal.

171
00:22:36,080 --> 00:22:47,080
It's a good sign, the sign that you're learning to let go of the depression, and so on, and find ways to be more centered, more peaceful.

172
00:22:47,080 --> 00:22:50,080
But the joy itself is something that you have to know.

173
00:22:50,080 --> 00:22:56,080
Otherwise, you get attached to it, and you'll be looking for it, and you'll even get upset when it doesn't come.

174
00:22:56,080 --> 00:23:03,080
I mean, you'll see this as you practice, but just to give you a bit of a shortcut, so you don't get caught up in it.

175
00:23:03,080 --> 00:23:09,080
You don't get caught up in anything, but it said nothing is worth clinging to, so congratulations good for you.

176
00:23:09,080 --> 00:23:16,080
But the next step, and maybe it won't come again, maybe not for a long time, maybe never.

177
00:23:16,080 --> 00:23:24,080
But learn to deal with it when it comes, pleasure when it comes, and desire for it.

178
00:23:24,080 --> 00:23:28,080
Excitement about it. All of that is part of the practice.

179
00:23:28,080 --> 00:23:33,080
It's not to take away from this experience. It's a great sign.

180
00:23:33,080 --> 00:23:38,080
It's just onward and upward. There's more to do. Good for you.

181
00:23:38,080 --> 00:23:43,080
Really good. Really nice to hear. Thank you for writing that out.

182
00:23:43,080 --> 00:23:48,080
I suffer from low self-esteem, and it can only be boosted through activities.

183
00:23:48,080 --> 00:23:51,080
How can I cultivate it permanently?

184
00:23:51,080 --> 00:23:58,080
Well, we don't need self-esteem. We're worthless. You are worthless. I am worthless.

185
00:23:58,080 --> 00:24:03,080
If you can figure that out, then you don't have to worry about self-esteem.

186
00:24:03,080 --> 00:24:07,080
Stop esteeming yourself. It's useless.

187
00:24:07,080 --> 00:24:11,080
We are worthless. We are worthless means, and all of this is worthless.

188
00:24:11,080 --> 00:24:14,080
Meaningless doesn't, it's what you make of it.

189
00:24:14,080 --> 00:24:22,080
You can pretend that this house and your job and your face and your body has some meaning.

190
00:24:22,080 --> 00:24:26,080
Your brain, your intelligence, your memory.

191
00:24:26,080 --> 00:24:33,080
You can claim that it all has meaning and worth, but it's all just subjective.

192
00:24:33,080 --> 00:24:36,080
It's what you make of it.

193
00:24:36,080 --> 00:24:40,080
And that frees you, seeing that frees you, because then you can look at,

194
00:24:40,080 --> 00:24:51,080
what's really important, and what's really important is happiness and suffering, peace and stress.

195
00:24:51,080 --> 00:24:57,080
I have sense of always being alone, even when, with others and groups, not a sense of loneliness,

196
00:24:57,080 --> 00:25:00,080
but it's like I'm the only one there. Is this normal?

197
00:25:00,080 --> 00:25:04,080
These questions of what is normal are not, I've talked about this before.

198
00:25:04,080 --> 00:25:08,080
It's not proper to ask. Maybe put it in the things.

199
00:25:08,080 --> 00:25:12,080
Please don't ask, is it normal? Please, you know, rephrase it and ask yourself,

200
00:25:12,080 --> 00:25:17,080
what do you really want to know? Do you want to know whether you are a freak?

201
00:25:17,080 --> 00:25:22,080
Is that what you're asking? Because you are you, and that's most important.

202
00:25:22,080 --> 00:25:25,080
It's most important to see what is.

203
00:25:25,080 --> 00:25:30,080
Asking what's normal doesn't do us any good, because who cares?

204
00:25:30,080 --> 00:25:35,080
Maybe normal is a horrible thing to be, right?

205
00:25:35,080 --> 00:25:40,080
We're not trying to be normal, we're trying to be good, and pure, and perfect to some extent,

206
00:25:40,080 --> 00:25:43,080
free, we're trying to be free.

207
00:25:43,080 --> 00:25:48,080
So, good question would be, is that a positive state, is it a negative state?

208
00:25:48,080 --> 00:25:52,080
Those would be better questions to ask. I mean, I'm not criticizing this.

209
00:25:52,080 --> 00:25:57,080
People ask this all the time, but it's just a, I think it may be a little lazy,

210
00:25:57,080 --> 00:26:02,080
because it's this normal, it doesn't, it's not a useful question.

211
00:26:02,080 --> 00:26:06,080
They've got it twice tonight, and I'm not, I'm not attacking you for it.

212
00:26:06,080 --> 00:26:11,080
I think I would probably ask the same thing, but it's, you have to think it through,

213
00:26:11,080 --> 00:26:13,080
what do you really want to know?

214
00:26:13,080 --> 00:26:18,080
So, good question is, is it, is it good? Is it bad?

215
00:26:18,080 --> 00:26:22,080
Is it probably more important? So, it's neither.

216
00:26:22,080 --> 00:26:24,080
I mean, it's an experience.

217
00:26:24,080 --> 00:26:30,080
So, try to note it and be mindful of feeling, feeling, or something.

218
00:26:30,080 --> 00:26:38,080
If you like it or dislike it, you know that, and so on.

219
00:26:38,080 --> 00:26:41,080
Okay, and that's all the questions for tonight.

220
00:26:41,080 --> 00:27:08,080
Thank you all for tuning in. Have a good night.

