1
00:00:00,000 --> 00:00:07,000
Somebody here says very simple question. Can someone who commits murder achieve enlightenment in future lives?

2
00:00:08,400 --> 00:00:13,100
They can achieve enlightenment in this way. Who's gonna pick this one up Owen?

3
00:00:18,640 --> 00:00:25,280
Did you not do a video or some of the recently on this one about

4
00:00:25,280 --> 00:00:31,280
the guy trying to get a thousand murders, and then he came across the Buddha.

5
00:00:35,280 --> 00:00:38,960
Oh yeah, you mean that video the movie that I put the piece together?

6
00:00:40,960 --> 00:00:45,760
I think it was border a while back where there was a guy who was

7
00:00:47,200 --> 00:00:51,680
basically told to get the fingers of a thousand people.

8
00:00:51,680 --> 00:00:53,280
I mean you don't know the story.

9
00:00:53,280 --> 00:00:55,280
You know what I do, but

10
00:00:57,280 --> 00:00:59,280
I'm not worried for it.

11
00:00:59,280 --> 00:01:03,280
But like you don't, you know who that's talking about, or you're not familiar with the

12
00:01:03,280 --> 00:01:05,280
Majimani kaiyasutra, the Anguli Malasutra?

13
00:01:07,280 --> 00:01:09,280
No, not not.

14
00:01:09,280 --> 00:01:13,280
You know what I should find a way to post the movie. There's this movie,

15
00:01:13,280 --> 00:01:19,280
a time movie called Anguli Mala, and it tells the story really well, and the ending is really cool.

16
00:01:19,280 --> 00:01:25,280
If you look up the video idea called forgiveness and redemption in Buddhism,

17
00:01:25,280 --> 00:01:28,280
do you remember the tiger wood scandal?

18
00:01:28,280 --> 00:01:30,280
Yeah, everyone does, no?

19
00:01:30,280 --> 00:01:33,280
The world wide thing.

20
00:01:33,280 --> 00:01:41,280
And you remember how the the Christians started, started saying stuff like,

21
00:01:41,280 --> 00:01:44,280
well if he was only Christian, Jesus would forgive him.

22
00:01:44,280 --> 00:01:47,280
And they said like there's no forgiveness in Buddhism.

23
00:01:47,280 --> 00:01:50,280
Anyway, it's kind of an American thing.

24
00:01:50,280 --> 00:01:53,280
Maybe a certain America, you don't know this.

25
00:01:53,280 --> 00:01:56,280
I'm kind of interested in tiger woods, because he's supposed to be Buddhist.

26
00:01:56,280 --> 00:01:58,280
So it's an interesting topic.

27
00:01:58,280 --> 00:02:02,280
And so I, there was even a Buddhist who was interviewed about this,

28
00:02:02,280 --> 00:02:08,280
you know, talking about, you know, trying, they were trying to get him to argue with this,

29
00:02:08,280 --> 00:02:14,280
but instead of arguing, he just said, you know how sorts of things he thinks tiger woods should do.

30
00:02:14,280 --> 00:02:22,280
And so I put together this video, because the angulimales, the story of angulimales is the perfect response to this question.

31
00:02:22,280 --> 00:02:27,280
So look up that video of forgiveness and redemption in Buddhism.

32
00:02:27,280 --> 00:02:33,280
And you'll see, I stitched it together from this movie and put some subtitles on it.

33
00:02:33,280 --> 00:02:38,280
And the movie, that part of the movie, you know, if you watch the whole part,

34
00:02:38,280 --> 00:02:46,280
it's just, it's just perfect. I mean, they did a really good job and it said that it never became very famous movie.

35
00:02:46,280 --> 00:02:50,280
I think the whole, that whole part, not just the parts that I put in,

36
00:02:50,280 --> 00:02:58,280
where he meets the Buddha and what the Buddha says to him and the result and the impact that it has on him.

37
00:02:58,280 --> 00:03:09,280
It's just very well done, especially if you know the story of angulimales, because it's a famous Buddhist story.

38
00:03:09,280 --> 00:03:17,280
So let's see, a little bit of detail here. Why is it possible that you murder someone and then can become enlightened?

39
00:03:17,280 --> 00:03:27,280
He makes it more difficult. Yeah, it just, it defiles your mind, but it depends on what other good qualities you have inside.

40
00:03:27,280 --> 00:03:32,280
I don't believe not that a person who kills someone could have good qualities inside.

41
00:03:32,280 --> 00:03:36,280
But people can do things on the spur of the moment.

42
00:03:36,280 --> 00:03:39,280
Obviously, angulimala didn't, but that's the thing.

43
00:03:39,280 --> 00:03:44,280
He killed 999 people and he was still able to become enlightened.

44
00:03:44,280 --> 00:03:48,280
I think the key was he wasn't doing it out of hatred or malice.

45
00:03:48,280 --> 00:03:51,280
He was doing it out of misunderstanding in a wrong view.

46
00:03:51,280 --> 00:03:56,280
So once he was able to change that wrong view, he, he was able to progress quite quickly.

47
00:03:56,280 --> 00:04:03,280
But he suffered horribly for it. He was beaten and,

48
00:04:03,280 --> 00:04:06,280
with the Buddha said about it, they said, how is it possibly?

49
00:04:06,280 --> 00:04:14,280
He could become enlightened. It's in the Dhamapadeh. If you read his story, it's in the Dhamapadeh commentary.

50
00:04:14,280 --> 00:04:20,280
There's a Buddhist legends, if you look up this translation of the Dhamapadeh commentary.

51
00:04:20,280 --> 00:04:32,280
Where he says, the Buddha says, who, they make their good,

52
00:04:32,280 --> 00:04:38,280
by using good deeds, make their bad deeds insignificant.

53
00:04:38,280 --> 00:04:41,280
So you have this very, very bad deed.

54
00:04:41,280 --> 00:04:49,280
But the deed of becoming an arahat is so much more profound than even killing lots of people.

55
00:04:49,280 --> 00:05:01,280
That it actually makes even that deed insignificant because they asked, how is it possible that he wouldn't have to go to hell and suffer for countless lifetimes?

56
00:05:01,280 --> 00:05:07,280
And the Buddha said, if he hadn't become an arahat, that's where he would have gone.

57
00:05:07,280 --> 00:05:13,280
There's no hope for him. But because of his ability to become an arahat, which is incredibly surprising,

58
00:05:13,280 --> 00:05:19,280
he only had to suffer torturing this one night.

59
00:05:19,280 --> 00:05:23,280
It doesn't really say what happened to me, probably he was beaten to death or something.

60
00:05:23,280 --> 00:05:28,280
Who knows, probably not, probably it says in the commentaries that he said himself on fire.

61
00:05:28,280 --> 00:05:33,280
A lot of the arahants read stories about how they spontaneously combusted.

62
00:05:33,280 --> 00:05:38,280
An and I said to have done that because they don't want people to have to worry about them.

63
00:05:38,280 --> 00:05:48,280
Because they know if they if they it's funny, it's funny to read an interesting to compare to today's.

64
00:05:48,280 --> 00:05:58,280
The methods of today among Buddhist circles because what they said is they knew that if they didn't spontaneous, if they died and let people have their body,

65
00:05:58,280 --> 00:06:07,280
people would just fight over their their bone relics fight over the body and it would be what they say is it would be a burden to people.

66
00:06:07,280 --> 00:06:10,280
You know, having to bury, having to have a funeral and so on.

67
00:06:10,280 --> 00:06:15,280
And so on and that he had two sets of relatives.

68
00:06:15,280 --> 00:06:21,280
Actually, it's not quite true about the relics is it. He has two sets of relatives and he knew that they'd be fighting over his body.

69
00:06:21,280 --> 00:06:27,280
So he went over the river. I think the Rohini river probably where the Buddha went over and he floated over the river.

70
00:06:27,280 --> 00:06:36,280
And he spontaneously combusted and made a determination that his relics would distribute evenly on both sides of the river.

71
00:06:36,280 --> 00:06:44,280
So and and then this bones were were have to do.

72
00:06:44,280 --> 00:06:48,280
I don't know, I guess it's the.

73
00:06:48,280 --> 00:06:51,280
The sake is and.

74
00:06:51,280 --> 00:06:57,280
Oh, yes, no, I guess the Buddha is two sides of it.

75
00:06:57,280 --> 00:07:00,280
I don't know. It's just cousins.

76
00:07:00,280 --> 00:07:07,280
It's something like that.

77
00:07:07,280 --> 00:07:10,280
Anyway, that's the story I heard able me sort of.

78
00:07:10,280 --> 00:07:14,280
There's a question that I see this a couple of times now I think about.

79
00:07:14,280 --> 00:07:16,280
I'm sorry, I shouldn't stop.

80
00:07:16,280 --> 00:07:22,280
Anyone have any to say on what I just what we're just asking where to know?

81
00:07:22,280 --> 00:07:27,280
If you kill someone, if you kill your parents, you can't become enlightened.

82
00:07:27,280 --> 00:07:30,280
Killing your parents is one of the five garu comments.

83
00:07:30,280 --> 00:07:36,280
If you kill your father, if you kill your mother, if you kill an arahan, if you hurt a Buddha,

84
00:07:36,280 --> 00:07:46,280
or if you create a schism in the Buddhist, Buddhist sangha, these are the five garu comments.

85
00:07:46,280 --> 00:07:51,280
The result of which a person is unable to attain spiritual,

86
00:07:51,280 --> 00:07:58,280
it ain't even sold upon me in this life. So Devadatta is one of those people.

87
00:07:58,280 --> 00:08:06,280
I'm just as up to it was another one.

88
00:08:06,280 --> 00:08:08,280
Those kind of people cannot.

89
00:08:08,280 --> 00:08:11,280
So Mahasi Sayada makes a point here.

90
00:08:11,280 --> 00:08:19,280
He says, in that regard, people who euthanize their parents are guilty of

91
00:08:19,280 --> 00:08:24,280
very, very heinous crime, something to keep in mind from a Buddhist perspective.

92
00:08:24,280 --> 00:08:27,280
But killing a person doesn't disqualify you.

93
00:08:27,280 --> 00:08:31,280
It makes it much more difficult and it makes your practice much more unpleasant

94
00:08:31,280 --> 00:08:35,280
and it does corrupt your mind. It's a very, very bad thing,

95
00:08:35,280 --> 00:08:42,280
but to say that a person is not difficult, it's not unthinkable for a person to become at least

96
00:08:42,280 --> 00:08:49,280
a soda pan now and from the sake of becoming without too much trouble, a lot of suffering,

97
00:08:49,280 --> 00:09:13,280
in this life certainly do, depending on the other qualities of mind that they have.

