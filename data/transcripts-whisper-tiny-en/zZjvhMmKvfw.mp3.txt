Hello and welcome back to our study of the Damapanda.
Today we continue on with verse 114, which reads as follows.
Yocha was a satan, the way a person, a madan, badan.
Aikahan, the Yivitan, seyo, passato, a madan, badan.
Which, again, is very similar to the verses that we've been looking at.
It means better than to live 100 years, not seeing the past to the deathless.
So this verse was taught in response to the story of Kisa go to me.
Kisa go to me was, there's some idea that she was a relative of the Buddha, though I'm not sure.
Because she comes up in an earlier story of when Siddhartha was going forth.
And one of the commentaries, I think.
One of the other commentaries.
But at any rate, we have Kisa means thin, so she was known as thin go to me.
Which apparently is a problem.
It was a problem at the time.
The measure of beauty at the time would have been plumpness and a nice full belly to be able to belly dancing.
It was a big thing.
So thin people, thin women were not attractive, probably not thin men either.
And so she had trouble getting a husband.
But finally, there was some very, very strange thing that allowed her to get a husband.
And I'm not really sure how we should interpret the story.
It doesn't have anything to do with the story, so I'm not going to go into detail.
But something about gold or... I don't really get it.
So I'm not going to try and relate it.
But suffice to say, she was seen as being somehow good luck.
Because she was able to see charcoal as gold or something like that.
So she finally got a husband as the point.
And a big deal for her was having a child.
And so she became pregnant.
And that this was a really big deal for her.
She was very keen to be a mother.
And after 10 months, pregnancy she gave birth to a son.
And this was such a joy to her.
Going through labor was not a chore.
She was very keen for this to occur.
As I would say most people would be having carried a child around for 10 months.
He would grow rather fond and attached to this being that's growing inside of you.
And so once it was born, she was very much attached to this son.
Very much in love with her child.
And he lived his first year growing up as children do.
But around the time you learned to walk, he contracted a sickness.
And because medicine was at an unadvanced stage back then,
he quickly died.
And it's a place to say she's just a quesago to me was devastated because the son of hers was pretty much her whole world.
At the same time, she seems to have led a rather sheltered life because she had never seen death before.
And didn't really understand what death meant.
And as a result, she picked up her dead son when they tried to burn him, burn his body.
She wouldn't let them and she picked them up and carried him on her hip.
And went off to find a doctor, someone to cure her son.
And so she went from house to house trying to find someone who would have medicine who could cure her son.
There must be a way to bring him back.
She had no concept of death of how it's final.
And so like any mother would do anything to find a cure if there were some way.
And everyone turned her away, cursing her and ridiculing her and calling her rightfully
crazy or foolish.
But she didn't listen. She wouldn't listen.
And she stubbornly refused to accept the fact that death is one way true.
And then in fact what she was carrying around wasn't actually her son.
It was just the worn out skin, worn out like a worn out snake skin.
It was the leftover.
And so finally there happened to be a rather wise person who heard her asking again and again for medicine.
And correctly, correctly deduced that this was just a matter of not understanding death.
And so he realized she needed someone to explain to her to help her to understand and gently and wisely find a way to lean her.
Because it's not that people hadn't tried.
It seems that people had been trying to explain to her death is one way.
But she wouldn't listen to them. She was very, very difficult to teach.
It seemed quite impossible to get through to her.
And so this man said, well, I don't know of any medicine that can heal your child.
But I know someone and he knows the cure.
And she was elated, ecstatic. She said, who could that be?
Oh, he lives in, he lives in, he lives in, I see they use very, very archaic language in these stories.
So I fall into it. Now the, let me solve these words.
In Jetamana, in Jetas Grove, go outside of the city and into the forest.
And there's the, you know, the forest of Jetah.
And go there. And that's where this, this doctor is residing.
And so she immediately got up. Thank you. So thank you.
Went off on her way to find this person who could heal her son and restore meaning to her life.
Because for her meaning was her whole attention, you know, how, how attached we can become to, to,
especially people, but to anything really.
And so she went to Jetahana and met with this doctor who, of course, was our beloved teacher and leader, the Buddha.
And she asked him, she said, look, I've heard,
people tell me that you have a way to cure my child.
Is this true? Is it, could it be possible that you have some medicine?
And he said, well, I know, yeah, I, I have to cure.
And she says, what, what is it, what is the cure?
So well, you have to bring to me some mustard seed.
And she said, mustard seed, well, that's easy.
Well, where, where is it like special or, or some special mustard seed?
It can't be just that where, where it will sort of mustard seed you want me to get.
He said, well, no, just any old mustard seed will do.
But you have to get me the mustard seed from a house where no one has ever died.
No son or daughter has ever died.
And she said, oh, well, that's fine.
I'm going to do that right away.
And so she wandered around from house to house.
But everyone she talked to, they all had mustard seed.
It was a common spice in India, still is.
But there was no house where no one had died.
You see, everyone was somebody's son or daughter and everyone's son or daughter.
Every, every house, somebody's son or daughter, of course, and died.
And especially with, with medicine and, and I'm unable to cure very simple sicknesses,
death would have been a common thing.
And this, they would say most, most people that, you know, just, just for miscarriage alone.
Apparently, this is a, one of the, one of the arguments against God
is that a woman's pelvic bone is to, is, or something to do,
something about the pelvic region is the wrong size.
And it's not really meant for childbirth,
or it's that the human head has grown too big.
Because childbirth shouldn't really happen.
It's not really where the body isn't really made for childbirth.
You know, even though it is kind of made for childbirth,
it's, it's a very, very poorly built system.
So the head is, is too big.
And oftentimes requires caesarian section, or else both the mother and the,
and the child can die.
So back when they didn't have this, death in childbirth was, was common.
You know, and even when born still birth, that's what's common.
Miscarriage common.
Birth of a child, you know, under, under five years old would have been common
with all the sicknesses and children's susceptibility to disease.
So death, death is everywhere.
Death takes us all, the death.
We live in the realm of death, somebody said,
because we're all, it's like we are on death row,
just waiting for our sentence.
Of course we've come to accept it and we think of it as natural,
but we are a lot like prisoners on death row,
just waiting for our turn.
And in fact, no one knows when their turn is going to be.
We haven't been told when the execution is.
We haven't even been told what sort of execution it's going to be.
It could be slow, it could be quick, it could be tortuous.
You know, it's not death by lethal injection,
not most likely, not for most of us.
For many of us it's going to be terribly painful.
Fear some.
We're like prisoners in the dungeon waiting to be beheaded or worse.
The realm of the deathless is where we live.
Not to be a total downer.
Whatever house she went to would say this,
and so she would have accepted their mustard seed
and she'd have to give it back to them and say,
no, this mustard seed won't do.
And from house to house and sometimes it was fresh
and people's minds and so they would be sad.
And maybe even cry.
And she felt, she saw this sadness reflected in other people
and it really opened her eyes.
And so this was the way of teaching her
without actually having to tell her,
without trying to argue with her,
was to see that this was a fruitless,
a hopeless task.
And slowly this corpse on her side
lost its attraction, lost its appeal.
And she began to see it for what it was.
It's just an empty husk.
Started to see that death is just really a part of life.
It is natural.
Not a good thing, but certainly natural.
And so time after time going to house after house,
she eventually lost all of her attachment to this quest.
And she looked down at the corpse.
And she made a decision,
went into the forest and dug a grave
and buried her child,
or placed the child in the forest.
Maybe probably didn't bury it.
Just laid it down in the forest.
And then she went into the Buddha
and bowed down to him.
They'd respect and he said,
oh, so did you get the mustard seed?
And she said, no, I didn't.
You never village the dead or far greater than the living.
And the Buddha said, indeed,
it was foolish of you to think
that only you have lost.
So I think that death has only come to you
all beings are subject to death.
And he said, the prince of death,
like a raging torrent,
sweeps away into the sea of ruin,
all living beings,
still are their longings unfulfilled.
And then he again taught a verse
that comes in the later chapter, 287.
But we'll get to that later,
so I'm not going to recite that.
And as he taught her this,
and based on just this ordeal that he had she'd been through
and then the teaching of the Buddha,
she was actually able then and there to apply it
and to become a sotupana through his teachings.
Which is quite remarkable.
And as a result, she requested to become a biquini
and she became a biquini
and fulfilled her duties and practiced meditation.
And then the story goes that one day
every two weeks they'd have a gathering
on the full moon in the empty moon,
all the monks gathered together.
And so if the gathering is done at night,
which in some monasteries it is,
you have to lay the lamp.
So they would go and put the oil
and the wick and light the lamp.
And one day it was her turn to do this.
And so she went and she lit the lamp
and of course she had been meditating.
And so she stood there watching this flame flicker.
And she saw the different flames come up from the lamp
and some flames would last a while
and some would flicker out.
And she watched this arising and ceasing.
And because she was so in tune with this aspect
of cessation and birth
and true birth and death of every moment,
it really hit her that that's really how our lives are.
And she said our lives are just like these flames.
We come into existence and poof
when we're gone, that's it.
There's no getting the flame back once it's gone.
And the Buddha got a sense of this
and came to see her as with the last story.
And that's when he taught her this verse.
He said,
even as it is with these flames.
So also it is with living beings here in the world.
Some flare up whether there's flicker out.
They only that have reached nibana or no more seem.
So our lives are like a flame.
We burn and then out.
Then we come back again and again.
If the flame is still lit,
if the weak is still lit,
the flame will come back again and again,
fades out, comes back.
Some birth and death, birth and death,
incessant.
But when you blow out,
when the fire gets blown out,
then there's no more flame.
And so then he teaches the death lesson.
So this is the idea that this verse is appropriate
because it talks about death.
It's appropriate because the story is all about death.
So the key here was this realization of the prevalence
and the ubiquitousness of death.
And death is everywhere for everyone
that we live in the realm of death.
And so the Buddha taught specifically about the deathless.
Because nibana or freedom are enlightened,
this is the deathless because you never come back.
You don't flicker up and die out.
There's no more flame than become free.
So this relates to our practice.
This really relates to the ultimate goal.
And for many people, it's kind of a foreign concept.
I think many people practice meditation
without much concept of nibana,
even without much desire for it,
because it goes so contrary to our desires.
We can see how some things are causing a suffering
and we do wish to be free from suffering.
But it's a whole other thing to take it to its extreme
to take it to its logical conclusion, though.
And think about a complete cessation.
If I cease, then what's left of all the things that I love and enjoy?
To how this relates directly to our practice,
it's something that you have to, it's on the horizon.
But it's the logical outcome.
It's not like it's something you fall into.
It's something that eventually a person comes to.
Because as long as we have desire for something,
we're going to be reborn again and again and again and again.
There's no question there.
It's just that once you start to look clearly,
and it may take lifetimes to do so,
but eventually you start to become disenchanted.
You see that it's the same thing again and again.
And it's not really as pleasant or as comfortable or as happy
as we think it's going to be.
We just end up wanting more and more and more and just getting disappointed.
We have to learn all these lessons about how we can't ever get what we want,
and that's really unpleasant.
We have to go through many, much unpleasantness.
So eventually, you realize that life after life after life,
it's not really all that it's cracked out to be.
And I think if we looked at this life as just one in a string of countless lives,
we'd come to see that, well, actually, if I have to do this all again,
if the learning isn't cumulative,
I'm not going to forget all these lessons I've learned and have to start over.
It's not really that great of a thing to do again and again.
It's quite stressful and unpleasant.
But again, as long as we cling,
as long as we desire it, as long as we're happy about being alive,
there's no worry.
We're still going to die.
We're going to die again and again and again.
It's only when we become free that we liberate ourselves from death as well.
So that's the path of the deathless,
path to the deathless is to give up our attachments and our desires.
Our attachments to our own life,
our attachment to the lives of others,
our attachment to becoming in general to any kind of a rising.
This is very much what we're into in meditation.
So it goes in stages.
In the beginning, we're only interested in the arising of real suffering,
pain and unpleasantness and our own immorality and that kind of thing.
Once we do away with that,
then it's only a matter of refining it.
So that eventually we come to see that suffering is really much more refined than that,
and it's in everything.
Eventually we give up more and more and we see more and more of our activities are based on greed,
anger and delusion and arrogance and conceit and all that.
So we give up more and more and more until eventually we're free.
We give up everything.
Let go of everything and then we can fly away and never have to come back.
So that's the dhamma for tonight.
It's the teaching and the deathless.
One more verse.
After this we have one more verse.
That's very, very similar.
And then we'll be finished with this chapter.
But these are important stories.
This is a very well-known story of how Kistel got to me found medicine for her son, dead son.
Medicine to help her fix this problem of having a dead son.
And she ended up fixing it by learning to let go.
We're learning to be more flexible because her son went on his own journey.
It's not like she's betraying his memory.
She's just accepting reality that he was gone and her path led elsewhere.
So that's the dhamma path of her tonight.
Thank you all for tuning in.
Good practicing.
