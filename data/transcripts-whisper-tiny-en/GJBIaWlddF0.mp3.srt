1
00:00:00,000 --> 00:00:20,560
Okay, good evening everyone back for more questions and answers, so tonight's question

2
00:00:20,560 --> 00:00:35,280
is on the healing power of meditation. The, it's a common question, the common theme is the

3
00:00:35,280 --> 00:00:46,960
question of whether meditation can heal x, y, or z, can do x, y, or z, or z.

4
00:00:46,960 --> 00:01:02,440
Can do this, can meditation do that? This question actually specifically asks whether, and

5
00:01:02,440 --> 00:01:11,120
it's a good example question, whether meditation and mindfulness can cure the damage

6
00:01:11,120 --> 00:01:19,520
to the brain and the mind caused by psychedelics. That's an interesting question, I'm not

7
00:01:19,520 --> 00:01:25,520
sure whether this person has heard me talk about my experience with psychedelics because

8
00:01:31,600 --> 00:01:36,640
I think my brain was somewhat damaged by psychedelics temporarily permanently, I don't know.

9
00:01:36,640 --> 00:01:44,640
I've done psychedelics before and there have been no problems, but something in this one

10
00:01:46,640 --> 00:01:54,640
really changed the way my brain sort of functioned, there was something wrong. I felt like I had

11
00:01:54,640 --> 00:02:07,120
been pulled away from the experiential world and there was a sort of a lapse or a disconnect

12
00:02:08,560 --> 00:02:13,600
and it was acutely felt every time I then did drugs usually cannabis done.

13
00:02:13,600 --> 00:02:28,080
What's cannabis, cannabis is marijuana, right? I would, I would feel this again, this disconnect,

14
00:02:28,080 --> 00:02:35,840
like I wasn't really there. No cannabis in moderation does not have phenomenal potential.

15
00:02:35,840 --> 00:02:40,960
Your people in chat, you're listening to the dhamma now, if you're not interested in listening,

16
00:02:40,960 --> 00:02:47,600
go elsewhere, not so many comments. I don't need a running comment, do you please?

17
00:02:51,760 --> 00:02:52,240
Thank you.

18
00:02:59,600 --> 00:03:03,920
And it was many, it was, well it turned me off of the drugs really.

19
00:03:05,520 --> 00:03:09,040
And so not to say that drugs do that, drugs are not all the same,

20
00:03:09,040 --> 00:03:14,080
but I couldn't do them anymore, I couldn't get stoned anymore.

21
00:03:16,560 --> 00:03:23,840
I think I was still in the alcohol for a while, whatever. Anyway, I empathize with the potential

22
00:03:26,800 --> 00:03:32,400
problems to the brain. I mean there have been studies done on how alcohol, caffeine, and marijuana

23
00:03:32,400 --> 00:03:38,880
do degrade the brain. I don't know, I mean it's studies I've seen. True, false, I don't know,

24
00:03:38,880 --> 00:03:44,960
I mean science is always learning new things, but there appears to be the potential for some

25
00:03:44,960 --> 00:03:51,360
connection to actual brain damage just by drinking lots of coffee or alcohol or taking lots of

26
00:03:51,360 --> 00:04:00,560
marijuana. But it's more extreme, psychedelics I guess, I mean apparently I'm not the only one who

27
00:04:00,560 --> 00:04:04,800
had this sort of experience where you really feel like you've damaged your brain in some way.

28
00:04:06,640 --> 00:04:11,920
Regardless, this isn't to talk about drugs, if you people are all these chats are talking about

29
00:04:11,920 --> 00:04:19,040
cannabis and so on, I'm not interested, please go elsewhere. It's not what this talk is about.

30
00:04:19,040 --> 00:04:24,000
This talk is about the healing power of meditation. If you suffer brain damage, I mean a really

31
00:04:24,000 --> 00:04:32,160
good question would be, what if you have a partial lobotomy or maybe you get into an accident and

32
00:04:32,160 --> 00:04:51,280
have brain trauma or maybe you, well for some reason another your brain gets damaged, maybe you're

33
00:04:51,280 --> 00:05:01,840
sniffing gasoline, right? That'll do it. And so there's this question of whether meditation

34
00:05:01,840 --> 00:05:05,680
can heal this. I mean I think of all the questions of can meditation do this in

35
00:05:05,680 --> 00:05:11,440
communication do that. This one has a good potential to be answered in the positive, right,

36
00:05:11,440 --> 00:05:18,080
because meditation deals with the mind, which is very closely related to the brain.

37
00:05:18,080 --> 00:05:33,280
And so the healing power of meditation might then extend to this sort of brain damage.

38
00:05:33,280 --> 00:05:38,640
The question was asking whether the damage to the mind can be healed. That's actually a completely

39
00:05:38,640 --> 00:05:44,560
different question. First you have to ask can the mind be damaged? What does it mean to have

40
00:05:44,560 --> 00:05:50,720
mind damage? Something I've actually never been asked before. So let's separate these out. First let's

41
00:05:50,720 --> 00:06:00,320
do away with that one. Mind damage. Mind damage can only mean the cultivation of bad habits, right?

42
00:06:00,320 --> 00:06:06,960
And bad habits can be pretty extreme. If you do something even once, that's quite extreme,

43
00:06:06,960 --> 00:06:15,680
then it can affect the mind in quite an extreme way. The mind can also be affected by physical

44
00:06:15,680 --> 00:06:25,040
changes. So if the brain were to undergo some severe trauma or so on, then the potential result

45
00:06:25,040 --> 00:06:32,960
to the mind might be quite extreme based on the person's reactivity. You're reacting to it

46
00:06:32,960 --> 00:06:41,200
and the inability to adapt to change the expectation and so on. The mind is organic as well.

47
00:06:41,200 --> 00:06:52,240
And so it can be affected by the physical. But it's not damaged per se. It's just caught up in

48
00:06:52,240 --> 00:06:59,120
habits and expectations is the big one. And those expectations are not met because of the changes

49
00:06:59,120 --> 00:07:06,240
to the physical. So that being healed, that absolutely can be healed through meditation.

50
00:07:07,360 --> 00:07:14,480
I mean one of the one of the one of the important aspects of this question is whether or not

51
00:07:14,480 --> 00:07:20,640
you mean in this life or whether you mean in future lives. I mean for the mind healing can go

52
00:07:20,640 --> 00:07:32,000
is ongoing and it can start as soon as you undertake training in meditation, for example. It continues

53
00:07:32,000 --> 00:07:38,640
in this life and it continues into the next life. It's possible that there are certain parameters

54
00:07:38,640 --> 00:07:44,880
under which it's not possible. It's possible that it might be not possible for a person to become

55
00:07:44,880 --> 00:07:50,720
enlightened in this life. So for a person to free themselves from bad habits. It's possible to be

56
00:07:50,720 --> 00:07:55,840
in such a situation. I could think of maybe a lobotomy. I don't know. I don't have any

57
00:07:55,840 --> 00:08:03,200
back any evidence or background for this. If a person has electroshock therapy or lobotomy or

58
00:08:03,200 --> 00:08:09,760
something, it may. I wouldn't say render the mind incapable of becoming enlightened. But it will

59
00:08:09,760 --> 00:08:16,000
make it very difficult or it will render it so difficult that the mind just has

60
00:08:16,000 --> 00:08:23,520
uncapable, incapable of changing its bad habits, of changing its reactions, its interactions

61
00:08:23,520 --> 00:08:29,600
with the physical. I mean changing to the extent of healing, of becoming enlightened really.

62
00:08:29,600 --> 00:08:46,960
The damage to the brain is in a whole other realm. The brain damage can affect the mind but

63
00:08:46,960 --> 00:08:54,400
only involves the stimuli that the mind is fed. So it is a different category. So having dealt

64
00:08:54,400 --> 00:09:01,360
with the mind, I mean most important thing to understand about the mind is it deals with reactions,

65
00:09:01,360 --> 00:09:09,760
interactions and habits of interactions that are affected by the physical. When we turn to the

66
00:09:09,760 --> 00:09:15,760
physical, we're in this realm of asking this question, can meditation do x, y, and z?

67
00:09:17,840 --> 00:09:21,520
And it's actually two different questions when we ask whether meditation can do something

68
00:09:21,520 --> 00:09:25,200
and whether mindfulness can do something. And an important part of this question that,

69
00:09:25,200 --> 00:09:34,000
for my perspective, is a clarification of what mindfulness does. What mindfulness meditation

70
00:09:35,280 --> 00:09:42,000
is for. What it is meant to do. Now it's certainly possible that you might sit down and practice

71
00:09:42,000 --> 00:09:46,720
meditation and mindfulness and you suddenly have all sorts of strange things happen. Maybe you

72
00:09:46,720 --> 00:09:52,560
remember your past life. Maybe you start floating in the air. Apparently this has happened to people.

73
00:09:52,560 --> 00:09:59,120
Not very common, but apparently yes. Many people, this is relatively common, where they'll hear

74
00:09:59,120 --> 00:10:04,240
things that aren't there. And there's a speculation that they're hearing things far away. Some

75
00:10:04,240 --> 00:10:09,840
people have been claimed to hear things far away. See things far away. Leave their body.

76
00:10:10,640 --> 00:10:15,120
That's a common one. Someone will leave their body and look down at themselves. They've been

77
00:10:15,120 --> 00:10:21,280
looking down. Oh, there's me sitting in meditation. This you hear true false. Some people are

78
00:10:21,280 --> 00:10:27,600
probably quite skeptical. But these are the kinds of things that can happen. So could mindfulness

79
00:10:27,600 --> 00:10:36,640
meditation do many, many things? Sure. It's important to understand that none of those things are

80
00:10:36,640 --> 00:10:42,640
what mindfulness meditation is for. It's not a problem. It's interesting to know that there are

81
00:10:42,640 --> 00:10:50,240
byproducts. But the other thing is the desire for those things to happen prevents you from practicing

82
00:10:50,240 --> 00:10:57,840
mindfulness. Even if you desire something like remembering past lives, anytime you want to remember

83
00:10:57,840 --> 00:11:03,360
your past life, you're no longer practicing mindfulness. You're now engaging in a desire for something.

84
00:11:03,360 --> 00:11:07,680
You might even think it's a wholesome desire or wholesome intention, but it's no longer mindfulness.

85
00:11:07,680 --> 00:11:14,640
And so if you desire for, if you're intention, this is the point, if you're intention in practicing

86
00:11:14,640 --> 00:11:19,840
mindfulness meditation, has anything to do with any of those things, healing the body, healing the

87
00:11:19,840 --> 00:11:28,400
brain, and healing your cold, healing your eyesight, making it more beautiful or something like that.

88
00:11:28,400 --> 00:11:33,440
No, anything. Even let's say wholesome things like maybe making me healthier or so on,

89
00:11:33,440 --> 00:11:40,400
that it detracts from your ability to practice mindfulness meditation. It actually makes it no longer

90
00:11:40,400 --> 00:11:46,480
mindfulness. It's an important point to keep in mind that when you ask these questions, the real

91
00:11:46,480 --> 00:11:54,480
problem is that you want for those things to happen. And the asking of the question puts you at

92
00:11:54,480 --> 00:12:00,400
an important dissonance in terms of mindfulness practice because if that's what's bringing you to

93
00:12:00,400 --> 00:12:04,480
meditation, to mindfulness meditation, you're not going to be practicing mindfulness meditation until

94
00:12:04,480 --> 00:12:12,240
you realize that it's getting in the way of not just you practicing this meditation that I teach,

95
00:12:12,240 --> 00:12:18,160
but it's getting in the way of you finding freedom from suffering. Your desire for freedom from

96
00:12:18,160 --> 00:12:24,960
brain damage even is going to get in the way of you finding freedom from suffering. So suppose

97
00:12:24,960 --> 00:12:31,120
brain damage, I got I got really sick in Sri Lanka and I had the feeling that it changed

98
00:12:31,120 --> 00:12:36,080
how my brain works and so some memory isn't quite as good as it used to be. I'm also just

99
00:12:36,080 --> 00:12:43,200
getting older so it could be that. But if I want for that to change, if I have this intention

100
00:12:44,160 --> 00:12:48,720
and this is often the thing you have a desire to remember better, there's even claims that mindfulness

101
00:12:48,720 --> 00:12:53,360
meditation improves your memory. I think for some people it does. I've also met other meditators,

102
00:12:53,360 --> 00:12:59,440
mindfulness meditators who don't have very good memory even though their mindfulness is quite good,

103
00:12:59,440 --> 00:13:06,080
it seems. And I think it does depend to some extent on the capacity of the brain, the functioning

104
00:13:06,080 --> 00:13:28,880
of the brain and so. And so there's this relationship. The mind can in some cases heal the brain,

105
00:13:28,880 --> 00:13:37,360
there might be healing that goes on. But the desire for it, the intention, the concern even

106
00:13:38,080 --> 00:13:45,680
for the healing of the brain is a problem. And so really not as the most important point

107
00:13:45,680 --> 00:13:53,280
because it's not so interesting to me or it's not something that I wish to make interesting. But

108
00:13:53,280 --> 00:14:01,440
I think there are other types of meditation that potentially have the express benefit of healing

109
00:14:01,440 --> 00:14:08,800
the brain, let's say, or healing the body or helping you remember past lives, obviously helping

110
00:14:08,800 --> 00:14:13,840
you float through the air, helping you leave your body. Lots of teachings, even Buddhism, outside

111
00:14:13,840 --> 00:14:19,440
of Buddhism, by Buddhists, you'll hear them talk about these things so many different kinds.

112
00:14:19,440 --> 00:14:24,560
And I think that's all valid. I mean, science would reject a lot of it. Like let's say I say

113
00:14:25,120 --> 00:14:29,040
I can give you a meditation that's going to cure your cancer. Scientists would say that's

114
00:14:29,760 --> 00:14:35,760
ridiculous and reckless to suggest such a thing. That doesn't really have any bearing on me saying

115
00:14:35,760 --> 00:14:42,240
it's possible. It's possible as far as I know. Anything's possible as far as I know because

116
00:14:42,240 --> 00:14:49,600
but partially because I just don't understand science. I'm not a material scientist to the

117
00:14:49,600 --> 00:14:55,520
extent that I can tell you what's physically possible, what's physically impossible. Could meditation

118
00:14:55,520 --> 00:15:03,040
regrow a loss limb? I think yes, it could. Science would, of course, many of your

119
00:15:03,600 --> 00:15:08,720
even here are probably saying that's ridiculous, it's impossible. But that's not really what I mean

120
00:15:08,720 --> 00:15:15,840
by possible. It's possible in the sense that I don't have proof and I don't not really

121
00:15:15,840 --> 00:15:21,280
interested in finding proof. It's not in my realm of what I can prove to say yes or no. It's

122
00:15:21,280 --> 00:15:27,600
just something out there. The more important point is that again, when you are practicing those

123
00:15:27,600 --> 00:15:34,640
meditations, it's to your detriment. It's generally to your detriment to be practicing meditation

124
00:15:34,640 --> 00:15:39,600
to heal this or to heal that because it's creating clinging, because it's creating distraction,

125
00:15:40,240 --> 00:15:47,360
because it's reinforcing your attachment to things like health, to things like the proper functioning

126
00:15:47,360 --> 00:15:52,560
of the brain even. If you want to remember better, it's going to get in the way of your meditation

127
00:15:52,560 --> 00:15:57,600
because you become attached to remembering and it's always temporary. When it goes away,

128
00:15:57,600 --> 00:16:05,120
etc, etc, etc, you're going to be upset and so on. The point of this and the point of me

129
00:16:05,120 --> 00:16:17,600
answering this question that I wanted to get across is that mindfulness requires letting go.

130
00:16:19,040 --> 00:16:25,760
That mindfulness meditation, before foundations of mindfulness, for the purpose of seeing insight,

131
00:16:25,760 --> 00:16:31,440
requires you really to not have any ambition, any desire, even the desire to become enlightened,

132
00:16:31,440 --> 00:16:36,320
is going to get in your way. It has to be about being here now. So people say, well,

133
00:16:36,320 --> 00:16:43,920
then what about wanting to meditate? It's really not a proper question or a proper concern

134
00:16:44,480 --> 00:16:50,000
because of the nature of mindfulness. Mindfulness is actually nothing. It's stopping everything.

135
00:16:50,000 --> 00:16:57,520
Are you here and now? It's not a cult because it's nothing. Me sitting here now when I can just

136
00:16:57,520 --> 00:17:08,400
sit here now without believe, without dogma, without any kind of ambition or desire. That's

137
00:17:08,400 --> 00:17:16,480
enlightenment. When a person can do that, it's not anything. It's not even a religion, really.

138
00:17:16,480 --> 00:17:25,600
It's the religion of losing your religion. So in the beginning, there's many desires and

139
00:17:26,480 --> 00:17:33,040
I think the best we can do and what's important here is to stop with extraneous desires.

140
00:17:33,920 --> 00:17:40,400
If you desire to become enlightened, as I said, it's going to get in your way. But it's better

141
00:17:40,400 --> 00:17:48,320
than desiring for meditation to straighten your teeth or fix your eyesight or even fix your brain,

142
00:17:48,320 --> 00:17:55,760
your memory, and so on. So I hope that was useful. I think we might turn off these coffee.

143
00:17:57,760 --> 00:18:04,160
This is the internet. It's a scary place. I think it's distracting. I mean, I appreciate those of

144
00:18:04,160 --> 00:18:11,680
you who are listening keenly, but I really would like it to be like that. I know the novel

145
00:18:11,680 --> 00:18:16,640
thing about live streaming is you can interact with a broadcaster. It's not really what this is

146
00:18:16,640 --> 00:18:23,840
about. And so maybe we could have a stream where I do interact with the comments. I do talk to

147
00:18:23,840 --> 00:18:29,440
people in the comments. The problem with that, of course, is we get these sorts of comments here

148
00:18:29,440 --> 00:18:42,000
I'll scroll through them. Sounds like a cult. A lot of these are good. No, I mean, it's

149
00:18:42,000 --> 00:18:49,920
a really good audience. It's really incredible. Here's someone asking me, someone called

150
00:18:49,920 --> 00:19:01,200
Truth is you can ask me when I'll be back in Ontario, calling me Noah. That's interesting.

151
00:19:01,200 --> 00:19:07,360
I think you've got some very good comments here. But the point stands, listening to the

152
00:19:07,360 --> 00:19:12,960
demo should be about this thing. So maybe we'll have times where I turn off the comments,

153
00:19:12,960 --> 00:19:22,880
and then we'll have streams where I have the comments on, and we can have a chat with the internet.

154
00:19:22,880 --> 00:19:30,480
What could possibly go wrong? Anyway, thank you all for tuning in. 74 people watching. That's

155
00:19:30,480 --> 00:19:47,200
great. Wish you all the best.

