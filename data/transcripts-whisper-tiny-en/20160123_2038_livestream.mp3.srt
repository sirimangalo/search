1
00:00:00,000 --> 00:00:29,940
Okay, so what happened there is my computer crash.

2
00:00:29,940 --> 00:00:43,940
So that's not good.

3
00:00:43,940 --> 00:00:44,940
Or maybe it is.

4
00:00:44,940 --> 00:00:46,940
It's a good lesson in improvements.

5
00:00:46,940 --> 00:01:00,940
All right.

6
00:01:00,940 --> 00:01:01,940
Hi.

7
00:01:01,940 --> 00:01:06,940
Can I speak?

8
00:01:06,940 --> 00:01:25,940
Oh, hear me again.

9
00:01:25,940 --> 00:01:49,940
Okay.

10
00:01:49,940 --> 00:02:13,940
Okay.

11
00:02:13,940 --> 00:02:32,940
I'm just going to take a few more minutes and I'm going to go on to questions.

12
00:02:32,940 --> 00:02:56,940
Okay.

13
00:02:56,940 --> 00:03:20,940
Okay.

14
00:03:20,940 --> 00:03:39,940
Okay.

15
00:03:39,940 --> 00:03:58,940
Okay.

16
00:03:58,940 --> 00:04:22,940
Okay.

17
00:04:22,940 --> 00:04:36,940
I'm encouraging other people, sorry, helping other people in doing good game.

18
00:04:36,940 --> 00:04:44,940
When someone else wants to give and give a gift, maybe they have a ceremony and giving to

19
00:04:44,940 --> 00:04:58,940
people, or maybe they've set up things, food bankers and things, and work in the giving field.

20
00:04:58,940 --> 00:05:06,940
And you've helped people to come and learn the number and set up a center and the internet

21
00:05:06,940 --> 00:05:19,940
and invite people to come and give talks on the second night, that kind of thing.

22
00:05:19,940 --> 00:05:30,940
But this can all be put under the heading down, if you like, often there's something.

23
00:05:30,940 --> 00:05:32,940
Another big one is see love.

24
00:05:32,940 --> 00:05:37,940
One that you can't really put under down and you see idea of see love.

25
00:05:37,940 --> 00:05:40,940
See that means this is the bad stuff.

26
00:05:40,940 --> 00:05:42,940
See that means not doing bad things.

27
00:05:42,940 --> 00:05:52,940
It's not doing things that harm others, because when you harm others, you harm your own psyche.

28
00:05:52,940 --> 00:06:00,940
This is why looking out for your benefit is actually not selfish.

29
00:06:00,940 --> 00:06:17,940
When you work for your own happiness, I'm automatically involved in working for the happiness.

30
00:06:17,940 --> 00:06:24,940
If you work for the detriment of others, if you do kill or steal or lie or cheap, not only does it harm others,

31
00:06:24,940 --> 00:06:34,940
but it directly harms you.

32
00:06:34,940 --> 00:06:45,940
So keeping ethical precepts, we're framing by, as a rule, we're framing from bad things,

33
00:06:45,940 --> 00:07:08,940
we're seeing another self-goodness, because it means to happiness, breezy from all the trouble that comes from harming others.

34
00:07:08,940 --> 00:07:21,940
And the third one is the one we already talked about.

35
00:07:21,940 --> 00:07:40,940
The other one is the purification of undown mind in various ways, anything that cultivates also my instinct can be through sending love to others, expressing compassion to others.

36
00:07:40,940 --> 00:07:49,940
It can be through contemplation and things like death and the aspects of the body.

37
00:07:49,940 --> 00:08:02,940
A meditation on food, where you think about nature of food, and it helps you lose your habituation and toxicization with different types of food.

38
00:08:02,940 --> 00:08:21,940
Meditators, people who do curious enough when they do intensive meditation practice, it's common for meditators to lose interest in eating and thought arises, or have to look at it again.

39
00:08:21,940 --> 00:08:32,940
It's not because the food has become the true nature of eating, it's actually a burden for us to have to eat.

40
00:08:32,940 --> 00:08:42,940
It's pretty much the opposite of how we normally have people normally look at it.

41
00:08:42,940 --> 00:08:53,940
And of course there's the meditation on the inside.

42
00:08:53,940 --> 00:08:56,940
Mindfulness.

43
00:08:56,940 --> 00:09:20,940
So you can see this as they are.

44
00:09:20,940 --> 00:09:32,940
I'm not really understanding reality and pleasant or stable, controllable.

45
00:09:32,940 --> 00:09:39,940
And so not getting upset when it goes against our expectations.

46
00:09:39,940 --> 00:09:52,940
I'm not getting upset when it turns out that the way we look at the world is all wrong, wrong way of looking up things is bad because people upset you.

47
00:09:52,940 --> 00:09:58,940
Understanding reality and the nature of reality is good, because you'll never be upset.

48
00:09:58,940 --> 00:10:01,940
You'll always be prepared.

49
00:10:01,940 --> 00:10:10,940
This is why understanding is the best.

50
00:10:10,940 --> 00:10:14,940
And so understanding is becoming invincible.

51
00:10:14,940 --> 00:10:22,940
Suffering comes from not having our expectations with us.

52
00:10:22,940 --> 00:10:36,940
I can see everyone is taking to top and chat.

53
00:10:36,940 --> 00:11:00,940
That's all.

54
00:11:00,940 --> 00:11:14,940
What's happening?

55
00:11:14,940 --> 00:11:38,940
I can just sit here and let it take.

56
00:11:38,940 --> 00:12:02,940
I can just sit here and let it go.

57
00:12:02,940 --> 00:12:26,940
I can just sit here and let it go.

58
00:12:26,940 --> 00:12:50,940
I can just sit here and let it go.

59
00:12:50,940 --> 00:13:15,940
I'll try to come back every week and try to have more to say.

60
00:13:15,940 --> 00:13:42,940
But that's where I like to talk with y'all to see where it moves coming and people are like, I can't read your mind so I can't tell what you're thinking.

61
00:13:42,940 --> 00:13:54,940
Yes, I think we've all got really.

62
00:13:54,940 --> 00:14:03,940
I'll try to.

63
00:14:03,940 --> 00:14:22,940
There's an echo happening.

64
00:14:22,940 --> 00:14:41,940
I know what I'm doing.

65
00:14:41,940 --> 00:15:02,940
You know what I was looped back and write into second life which is ridiculous.

66
00:15:02,940 --> 00:15:12,940
I would suggest that if people have questions and they can't voice, they could write them in text.

67
00:15:12,940 --> 00:15:21,940
If they can voice, they could type a one and you could address whatever question someone wants to voice.

68
00:15:21,940 --> 00:15:40,940
I'm not teaching right now.

69
00:15:40,940 --> 00:15:59,940
You can moderate.

70
00:15:59,940 --> 00:16:14,940
Thank you for coming.

71
00:16:14,940 --> 00:16:25,940
When I heard you were coming, it sounded interesting.

72
00:16:25,940 --> 00:16:32,940
I'm not going to talk about it.

73
00:16:32,940 --> 00:16:41,940
I'm not going to talk about it.

74
00:16:41,940 --> 00:17:01,940
That's really not a question but I wanted to give you that feedback.

75
00:17:01,940 --> 00:17:14,940
It's good to have feedback, positive and negative.

76
00:17:14,940 --> 00:17:27,940
As long as it's honest.

77
00:17:27,940 --> 00:17:50,940
If anyone would like to speak next, just go ahead and type a one and you can speak.

78
00:17:50,940 --> 00:18:00,940
What was all the chatter on about when I was talking?

79
00:18:00,940 --> 00:18:26,940
It wasn't sure whether I should start addressing the things you were saying.

80
00:18:26,940 --> 00:18:33,940
Good and bad.

81
00:18:33,940 --> 00:18:40,940
Good and bad are cultural ideas.

82
00:18:40,940 --> 00:19:04,940
I mean, good and bad can be cultural ideas.

83
00:19:04,940 --> 00:19:30,940
Good and bad can be very cultural and there for arbitrary, but we wouldn't consider there

84
00:19:30,940 --> 00:19:37,940
to be really good or bad.

85
00:19:37,940 --> 00:19:43,940
I guess the surprising thing is that people would find contentious is the idea that there are

86
00:19:43,940 --> 00:19:52,940
specific things that need the goodness and that there are specific things that need to happen.

87
00:19:52,940 --> 00:20:03,940
Happiness is subjective.

88
00:20:03,940 --> 00:20:29,940
But that only applies in a mundane and superficial course level.

89
00:20:29,940 --> 00:20:43,940
We don't mean pleasure, but we don't mean pleasure because that brings pleasure to people.

90
00:20:43,940 --> 00:20:57,940
It's conditioned to salivate when they hear a bell because they come to associate the bell with food.

91
00:20:57,940 --> 00:21:03,940
But you can't condition someone to be happy.

92
00:21:03,940 --> 00:21:08,940
That takes deep conditioning.

93
00:21:08,940 --> 00:21:13,940
It takes rational causes and effects.

94
00:21:13,940 --> 00:21:35,940
You need specific causes to do if you happen.

95
00:21:35,940 --> 00:21:46,940
You have to deal with mundane situations.

96
00:21:46,940 --> 00:21:53,940
It fails to capture the conventionality of the world.

97
00:21:53,940 --> 00:22:12,940
It doesn't really capture what's going on.

98
00:22:12,940 --> 00:22:25,940
I'm sorry, I just can't believe all these wonderful people are speaking.

99
00:22:25,940 --> 00:22:29,940
This is such a rare and fabulous opportunity.

100
00:22:29,940 --> 00:22:37,940
I have lots of questions and I have lots of curiosity about your perceptions.

101
00:22:37,940 --> 00:22:56,940
Today I was having a conversation earlier with someone who is here about the very issue of dogs and the bell ringing and salivation, not salivating and the fact that I'm in a deep study of the tarot and my deep study of the tarot.

102
00:22:56,940 --> 00:23:10,940
You see a man, a young man who is very happily just about to step off the cliff and at his heels is a little dog, a little white dog.

103
00:23:10,940 --> 00:23:34,940
The little yappie dog is ego, not capital M mind but is little mind.

104
00:23:34,940 --> 00:23:49,940
I don't know all the Buddhist language but I would say more the divine self, more the enlightened self or more the expanded self, you know, I'm searching for words.

105
00:23:49,940 --> 00:24:05,940
But once you let that little dog be in charge, then then your trouble, then you do what you say is you try to anesthetize that little dog, you try to drink it so that it will calm down, you take a stimulants to bring it back off.

106
00:24:05,940 --> 00:24:09,940
And I think that really relates to what you're talking about.

107
00:24:09,940 --> 00:24:15,940
Thank you.

108
00:24:15,940 --> 00:24:20,940
Yeah, well the ego is one of the root causes of suffering.

109
00:24:20,940 --> 00:24:24,940
It's one of the big evils, ego is pretty evil.

110
00:24:24,940 --> 00:24:36,940
And by ego we don't mean I suppose the Freudian ego, necessarily, I mean ego in terms of being able to stick on.

111
00:24:36,940 --> 00:24:51,940
If you're conceited or arrogant, those play a big part in this conversation to be able to make it worse because if you're angry at someone, someone says something to you that makes you angry.

112
00:24:51,940 --> 00:24:57,940
Well that's one thing but if you then go on to say I don't deserve to be talked to like that.

113
00:24:57,940 --> 00:25:06,940
You see how much worse that is because you've now become conceited, become arrogant, self-righteous.

114
00:25:06,940 --> 00:25:10,940
And so you validated your anger.

115
00:25:10,940 --> 00:25:15,940
I said, yes, I'm angry, I'm angry.

116
00:25:15,940 --> 00:25:21,940
You made me angry, how dare you make me angry.

117
00:25:21,940 --> 00:25:29,940
So you could argue that it's at the root of cultivating these emotions, instigating them perpetuating them.

118
00:25:29,940 --> 00:25:35,940
Whereas when you get angry if you're just to know it as anger, let it go.

119
00:25:35,940 --> 00:25:59,940
Because you become to a better understanding of the anger, eventually be free from it.

120
00:25:59,940 --> 00:26:26,940
That's definitely a big part of it when we get angry rather than focusing on the anger we focus on what is causing the anger.

121
00:26:26,940 --> 00:26:30,940
And that's not the way we do things.

122
00:26:30,940 --> 00:26:41,940
Even Buddhist meditators or meditators are often to the impression that they have to find the cause of things.

123
00:26:41,940 --> 00:26:45,940
And that's not really what we're interested in Buddhism.

124
00:26:45,940 --> 00:26:55,940
We talk about causes but we're much more interested in seeing the way things are causally potent.

125
00:26:55,940 --> 00:26:59,940
As they arise, watching things cause other things.

126
00:26:59,940 --> 00:27:05,940
So when you're angry, we're not interested in the cause of the anger, interested in the nature of the anger.

127
00:27:05,940 --> 00:27:12,940
And then we see that the anger is causally related to suffering.

128
00:27:12,940 --> 00:27:19,940
It's causally related also to the sound.

129
00:27:19,940 --> 00:27:26,940
And then every sound of the voice when someone says something to you then get angry at it.

130
00:27:26,940 --> 00:27:29,940
We're much more interested in the actual anger itself.

131
00:27:29,940 --> 00:27:32,940
What is it like?

132
00:27:32,940 --> 00:27:38,940
Usually what we do is we're angry we try to find the cause of the anger and destroy it.

133
00:27:38,940 --> 00:27:43,940
That's why we cause great conflict.

134
00:27:43,940 --> 00:27:46,940
That's why we cultivate the anger.

135
00:27:46,940 --> 00:27:49,940
Because we think so much about the causes.

136
00:27:49,940 --> 00:27:51,940
That person made me angry.

137
00:27:51,940 --> 00:27:56,940
That person did terrible things to me.

138
00:27:56,940 --> 00:28:00,940
If you just focus on the anger, it's just anger.

139
00:28:00,940 --> 00:28:02,940
It comes and it goes.

140
00:28:02,940 --> 00:28:05,940
And so if you're not familiar to the meditation practice that I teach,

141
00:28:05,940 --> 00:28:12,940
that's the technique that compliments this theory is to remind yourself of the anger.

142
00:28:12,940 --> 00:28:17,940
And that's just anger.

143
00:28:17,940 --> 00:28:20,940
And you say to ourselves like a mantra, anger and anger.

144
00:28:20,940 --> 00:28:22,940
And not trying to get rid of it even.

145
00:28:22,940 --> 00:28:23,940
Just trying to look at it.

146
00:28:23,940 --> 00:28:27,940
And as soon as you're objective with the anger, you'll destroy the anger.

147
00:28:27,940 --> 00:28:28,940
Cause anger isn't objective.

148
00:28:28,940 --> 00:28:31,940
Once your mind is objective, you're not.

149
00:28:31,940 --> 00:28:36,940
The anger can't survive.

150
00:28:36,940 --> 00:29:00,940
Does anyone else have any questions?

151
00:29:00,940 --> 00:29:07,940
Go ahead, Robin.

152
00:29:07,940 --> 00:29:14,940
This was very nice.

153
00:29:14,940 --> 00:29:19,940
I am on a meditation cushion.

154
00:29:19,940 --> 00:29:22,940
You know, that avatar looks shocking.

155
00:29:22,940 --> 00:29:32,940
I am on a meditation cushion.

156
00:29:32,940 --> 00:29:37,940
That avatar looks shockingly like you.

157
00:29:37,940 --> 00:29:38,940
It does.

158
00:29:38,940 --> 00:29:39,940
Just something.

159
00:29:39,940 --> 00:29:40,940
I'm looking.

160
00:29:40,940 --> 00:29:41,940
Yeah.

161
00:29:41,940 --> 00:29:43,940
Did you have a question?

162
00:29:43,940 --> 00:29:45,940
This is your first time on second life.

163
00:29:45,940 --> 00:29:46,940
Robin's quite active.

164
00:29:46,940 --> 00:29:49,940
She's on our board of directors.

165
00:29:49,940 --> 00:29:52,940
So we're good friends.

166
00:29:52,940 --> 00:30:13,940
And she's been to visit me and she's coming to visit again soon.

167
00:30:13,940 --> 00:30:20,940
Hi.

168
00:30:20,940 --> 00:30:21,940
I'm sorry.

169
00:30:21,940 --> 00:30:23,940
I'm not sure if I had my microphone on.

170
00:30:23,940 --> 00:30:24,940
This is new for me.

171
00:30:24,940 --> 00:30:25,940
I apologize.

172
00:30:25,940 --> 00:30:26,940
But just a simple question.

173
00:30:26,940 --> 00:30:31,940
I was wondering if this is going to be the normal spot for the talks and around the same time

174
00:30:31,940 --> 00:30:34,940
or do you think it will change up from week to week?

175
00:30:34,940 --> 00:30:37,940
Well, everything changes.

176
00:30:37,940 --> 00:30:47,940
Well, no, but this is the idea I think is if this works and just do this every week.

177
00:30:47,940 --> 00:30:51,940
Same time, same place.

178
00:30:51,940 --> 00:30:56,940
Thank you, Bhandi.

179
00:30:56,940 --> 00:31:23,940
Oh, thank you, Bhandi.

180
00:31:23,940 --> 00:31:27,940
You're welcome.

181
00:31:27,940 --> 00:31:33,940
Did you want to say something?

182
00:31:33,940 --> 00:31:42,940
Sorry, got no questions.

183
00:31:42,940 --> 00:32:06,940
Anyone with anyone else have questions or comments for Bhandi?

184
00:32:06,940 --> 00:32:27,940
I think I'm going to turn off the video recording so we can stick around and chat for a while.

185
00:32:27,940 --> 00:32:37,940
That's probably a question in response.

186
00:32:37,940 --> 00:32:44,940
Are the people over and the other sim able to hear?

187
00:32:44,940 --> 00:32:53,940
Yes, they have a link to the live stream and they're able to hear.

188
00:32:53,940 --> 00:33:00,940
Can they also chat with us?

189
00:33:00,940 --> 00:33:04,940
I don't think so.

190
00:33:04,940 --> 00:33:10,940
I don't think they're able to.

191
00:33:10,940 --> 00:33:19,940
Because it's a separate, I think it's separate.

192
00:33:19,940 --> 00:33:25,940
Still Annie's out there isn't she isn't Delaney and the other sim?

193
00:33:25,940 --> 00:33:28,940
Yes, I think so.

194
00:33:28,940 --> 00:33:30,940
And I could.

195
00:33:30,940 --> 00:33:33,940
Oh, she is okay.

196
00:33:33,940 --> 00:33:37,940
I don't see her in the wrong room.

197
00:33:37,940 --> 00:33:44,940
Oh, there she is, okay.

198
00:33:44,940 --> 00:33:51,940
That's great.

199
00:33:51,940 --> 00:33:55,940
I do have one thing to say.

200
00:33:55,940 --> 00:34:03,940
I'm so excited and I'm so excited and I'm really happy that you're going to be coming

201
00:34:03,940 --> 00:34:18,940
every week and teach you at the Buddhist Center and also I wanted to ask how, what is, would it be possible to make him conduct the

202
00:34:18,940 --> 00:34:34,940
rest of the market study and second life, do you think how likely would that be?

203
00:34:34,940 --> 00:34:43,940
It's not, I don't think it's ideal because we're studying a text.

204
00:34:43,940 --> 00:34:48,940
Okay.

205
00:34:48,940 --> 00:34:58,940
And I mean, the immediate thought is that we've already, you know, to switch,

206
00:34:58,940 --> 00:35:06,940
it's going to potentially cause people confusion.

207
00:35:06,940 --> 00:35:13,940
And, you know, just the technology some people are not even able to get on second life.

208
00:35:13,940 --> 00:35:18,940
But having a study group, we could do something, we could study something else.

209
00:35:18,940 --> 00:35:26,940
And another thing is that, you know, considering many of the people who would come would be stepping in near the end.

210
00:35:26,940 --> 00:35:29,940
It wouldn't be ideal for those new people.

211
00:35:29,940 --> 00:35:38,940
Every study magazine is just not an ideal sort of mass consumption teaching.

212
00:35:38,940 --> 00:35:45,940
I mean, it's more for a dedicated audience, which I think not necessarily going to get here.

213
00:35:45,940 --> 00:35:48,940
I mean, not everyone coming here is even Buddhist.

214
00:35:48,940 --> 00:35:54,940
Theravada, this isn't like it's a teravada center.

215
00:35:54,940 --> 00:36:00,940
I don't believe you're able for something like, I used to read Jatakatay.

216
00:36:00,940 --> 00:36:01,940
I'll see her even.

217
00:36:01,940 --> 00:36:04,940
I did that for a while.

218
00:36:04,940 --> 00:36:10,940
But, you know, like we could study the Majimani Kai or something.

219
00:36:10,940 --> 00:36:13,940
That would be maybe more useful.

220
00:36:13,940 --> 00:36:27,940
And so many of Jayanta had something to say there.

221
00:36:27,940 --> 00:36:36,940
I didn't want to go ahead if you'd like to speak.

222
00:36:36,940 --> 00:36:54,940
Okay, Banta, I just wanted to say thank you for coming back to second life.

223
00:36:54,940 --> 00:36:59,940
As I was trying to type, but I kept whispering.

224
00:36:59,940 --> 00:37:07,940
Many years ago, when I was just a late disciple first coming into Buddhism and teravada.

225
00:37:07,940 --> 00:37:10,940
And not having any Sangha near me.

226
00:37:10,940 --> 00:37:12,940
I found this, the Buddhist center.

227
00:37:12,940 --> 00:37:15,940
And you were actually the first one asked like, I ever heard speak live.

228
00:37:15,940 --> 00:37:17,940
Like, give a dumb talk live.

229
00:37:17,940 --> 00:37:20,940
And obviously, the rest is history.

230
00:37:20,940 --> 00:37:22,940
So, it's good to have you back one day.

231
00:37:22,940 --> 00:37:23,940
Thank you.

232
00:37:23,940 --> 00:37:24,940
Awesome.

233
00:37:24,940 --> 00:37:27,940
And to have you as well.

234
00:37:27,940 --> 00:37:35,940
It's amazing that we actually have monastics come into second life.

235
00:37:35,940 --> 00:37:39,940
Well, you've been kind of a trend center that I've seen.

236
00:37:39,940 --> 00:37:45,940
As somebody who lived for seven years as a late Buddhist disciple who had,

237
00:37:45,940 --> 00:37:48,940
I was the only Buddhist I knew in my daily life.

238
00:37:48,940 --> 00:37:52,940
I had no Sangha near me within an hour.

239
00:37:52,940 --> 00:37:56,940
So, the Buddhist center became my center.

240
00:37:56,940 --> 00:38:00,940
So, you know, until obviously I moved to Bhavana society.

241
00:38:00,940 --> 00:38:02,940
You know, to live here.

242
00:38:02,940 --> 00:38:04,940
How is it there?

243
00:38:04,940 --> 00:38:05,940
Yeah.

244
00:38:05,940 --> 00:38:08,940
How are things that love in the society?

245
00:38:08,940 --> 00:38:09,940
Things are going well.

246
00:38:09,940 --> 00:38:14,940
Bhanteji is actually in Malaysia right now.

247
00:38:14,940 --> 00:38:17,940
We have our seclusion going on right now.

248
00:38:17,940 --> 00:38:20,940
So, Bhanteji is doing some teachings over there.

249
00:38:20,940 --> 00:38:22,940
And Malaysia and Singapore.

250
00:38:22,940 --> 00:38:25,940
And I think it's going to do.

251
00:38:25,940 --> 00:38:28,940
Some go to Australia for a bit.

252
00:38:28,940 --> 00:38:31,940
He loves the digital poly reader, by the way.

253
00:38:31,940 --> 00:38:33,940
I don't know.

254
00:38:33,940 --> 00:38:36,940
He says a lot of good things about that.

255
00:38:36,940 --> 00:38:40,940
I think we've tried to actually get you to come to Bhavana society one of these days.

256
00:38:40,940 --> 00:38:41,940
He did invite me.

257
00:38:41,940 --> 00:38:42,940
Maybe one day.

258
00:38:42,940 --> 00:38:43,940
He said it.

259
00:38:43,940 --> 00:38:46,940
He gave me a standing invitation.

260
00:38:46,940 --> 00:38:48,940
Yeah.

261
00:38:48,940 --> 00:38:50,940
Good.

262
00:38:50,940 --> 00:38:51,940
Okay, Bhanteji.

263
00:38:51,940 --> 00:38:52,940
Thank you.

264
00:38:52,940 --> 00:38:56,940
Bye bye.

265
00:38:56,940 --> 00:39:01,940
Sounds good.

266
00:39:01,940 --> 00:39:04,940
And Bhanteji's sorrow would like to speak.

267
00:39:04,940 --> 00:39:05,940
Go ahead, please.

268
00:39:05,940 --> 00:39:24,940
Who is Bhante?

269
00:39:24,940 --> 00:39:26,940
Bhanteji's sorrow.

270
00:39:26,940 --> 00:39:28,940
Oh, Bhanteji.

271
00:39:28,940 --> 00:39:29,940
Yeah.

272
00:39:29,940 --> 00:39:31,940
Bhante is actually male, I thought.

273
00:39:31,940 --> 00:39:34,940
Or are they using it for Bhanteji?

274
00:39:34,940 --> 00:39:36,940
I thought you were that school.

275
00:39:36,940 --> 00:39:39,940
Both, but probably wrong.

276
00:39:39,940 --> 00:39:40,940
Oh, yeah.

277
00:39:40,940 --> 00:39:41,940
Okay.

278
00:39:41,940 --> 00:39:42,940
I am.

279
00:39:42,940 --> 00:39:43,940
I'm sorry.

280
00:39:43,940 --> 00:39:44,940
Bhanteji's sorrow.

281
00:39:44,940 --> 00:39:47,940
I was pushing for Bhante.

282
00:39:47,940 --> 00:39:52,940
Bhante works.

283
00:39:52,940 --> 00:39:55,940
Thank you.

284
00:39:55,940 --> 00:39:58,940
Go ahead, please.

285
00:39:58,940 --> 00:40:12,940
Thank you.

286
00:40:12,940 --> 00:40:23,940
Yeah, the digital palm reader.

287
00:40:23,940 --> 00:40:25,940
I knew it would.

288
00:40:25,940 --> 00:40:27,940
I was, that was one thing.

289
00:40:27,940 --> 00:40:32,940
I put years into that thing, ridiculous amounts of time into that tool.

290
00:40:32,940 --> 00:40:36,940
If you go through the code, you can see how crazy it is.

291
00:40:36,940 --> 00:40:40,940
If you ever want to read through it, even if you don't understand program, you can just see

292
00:40:40,940 --> 00:40:43,940
how much went into it.

293
00:40:43,940 --> 00:40:48,940
You know, more than really should have, but it's because I knew this was, you know,

294
00:40:48,940 --> 00:40:50,940
I'm one of you using it.

295
00:40:50,940 --> 00:40:54,940
I was like, this tool, you know,

296
00:40:54,940 --> 00:40:57,940
quite a simple concept.

297
00:40:57,940 --> 00:41:07,940
The core concept was being able to take a word that has been declined or altered or even

298
00:41:07,940 --> 00:41:14,940
pushed together with another word and breaking it apart and giving it a dictionary definition.

299
00:41:14,940 --> 00:41:17,940
So you click on the word and it gives you the definition.

300
00:41:17,940 --> 00:41:23,940
I mean, that's really the, to me, that's the, it's not the only useful thing about it.

301
00:41:23,940 --> 00:41:30,940
Of course, it does a million different things, but that was the core aspect.

302
00:41:30,940 --> 00:41:35,940
And that's so useful for, for, I think for most of us,

303
00:41:35,940 --> 00:41:39,940
rather than having to look something up in a dictionary,

304
00:41:39,940 --> 00:41:45,940
because anyone who studied languages knows half the time you spend flipping pages through a dictionary.

305
00:41:45,940 --> 00:41:49,940
And of course, if it's a language like poly or Sanskrit,

306
00:41:49,940 --> 00:41:54,940
the other good portion of your time is trying to figure out the declension.

307
00:41:54,940 --> 00:42:00,940
If it's in a regular word, you have to remember the irregulars forms.

308
00:42:00,940 --> 00:42:04,940
So being able to just click on a word and give you the meaning of the words.

309
00:42:04,940 --> 00:42:07,940
Pretty awesome.

310
00:42:07,940 --> 00:42:10,940
Yeah, but they, it's been amazing.

311
00:42:10,940 --> 00:42:17,940
Actually, I, I'm told this, this whole debacle with, with them changing it so that we couldn't download it to you.

312
00:42:17,940 --> 00:42:25,940
So I just, I didn't realize how much of, and how much it helped my poly learning until I didn't have it anyway.

313
00:42:25,940 --> 00:42:26,940
Okay.

314
00:42:26,940 --> 00:42:30,940
And you have to do it all yourself again.

315
00:42:30,940 --> 00:42:34,940
Oh, yeah.

316
00:42:34,940 --> 00:42:35,940
Yeah.

317
00:42:35,940 --> 00:42:42,940
And there are a lot of people, a lot of people offering suggestions.

318
00:42:42,940 --> 00:42:48,940
One, one Sri Lankan American monk was persistent,

319
00:42:48,940 --> 00:42:52,940
giving me, he, he must have given me a hundred different suggestions.

320
00:42:52,940 --> 00:42:53,940
Okay.

321
00:42:53,940 --> 00:42:55,940
Okay.

322
00:42:55,940 --> 00:42:56,940
Okay.

323
00:42:56,940 --> 00:42:58,940
I'm a lot of it.

324
00:42:58,940 --> 00:43:07,940
So it's interesting work.

325
00:43:07,940 --> 00:43:10,940
Glad to know that.

326
00:43:10,940 --> 00:43:18,940
Where's that out there?

327
00:43:18,940 --> 00:43:21,940
Our new center.

328
00:43:21,940 --> 00:43:22,940
Yeah, I don't know.

329
00:43:22,940 --> 00:43:29,940
I mean, I'm living in it, but we only have funding at the end of the year up to August.

330
00:43:29,940 --> 00:43:35,940
So in September, I may have to move back to,

331
00:43:35,940 --> 00:43:41,940
move somewhere else.

332
00:43:41,940 --> 00:43:46,940
We might not be able to keep renting out organizations renting house.

333
00:43:46,940 --> 00:43:54,940
I don't know if that's going to continue or not.

334
00:43:54,940 --> 00:44:02,940
Such is a nature.

335
00:44:02,940 --> 00:44:11,940
I thought they were looking for a new area.

336
00:44:11,940 --> 00:44:21,940
I mean, we can't rent a house unless we have the funding.

337
00:44:21,940 --> 00:44:42,940
And then we'll point.

338
00:44:42,940 --> 00:44:53,940
We've got meditators coming regularly.

339
00:44:53,940 --> 00:45:00,940
We would have had three meditators at once, but I asked the third one to come a little bit later.

340
00:45:00,940 --> 00:45:14,940
I think it was enough for one time for now.

341
00:45:14,940 --> 00:45:33,940
I think maybe pulling and the man from Finland is still here and starting at the beginning of February.

342
00:45:33,940 --> 00:45:47,940
And then Robin is coming and another American is coming.

343
00:45:47,940 --> 00:46:02,940
Robin and the rest of the volunteer group are doing such really wonderful work and excellent job in keeping things organized, keeping things going.

344
00:46:02,940 --> 00:46:06,940
And there's such a benefit to everyone.

345
00:46:06,940 --> 00:46:12,940
So thank you for that Robin and everyone else who are volunteering.

346
00:46:12,940 --> 00:46:14,940
I just wanted to say that.

347
00:46:14,940 --> 00:46:16,940
Thank you.

348
00:46:16,940 --> 00:46:25,940
Does anyone else have anything, any question for Dante?

349
00:46:25,940 --> 00:46:34,940
Okay.

350
00:46:34,940 --> 00:46:57,940
Thank you.

