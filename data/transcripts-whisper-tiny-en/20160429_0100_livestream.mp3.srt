1
00:00:00,000 --> 00:00:26,000
Good evening everyone, broadcasting live April, 28th, tonight's talk is about reverence.

2
00:00:26,000 --> 00:00:33,000
This monk comes and asks the Buddha.

3
00:00:33,000 --> 00:00:52,000
I asked him two questions, actually, this quote I only gives one, but he says,

4
00:00:52,000 --> 00:00:59,000
this is God, and he says,

5
00:00:59,000 --> 00:01:03,000
this is the Buddha.

6
00:01:03,000 --> 00:01:07,000
What is the cause?

7
00:01:07,000 --> 00:01:13,000
What is the reason?

8
00:01:13,000 --> 00:01:22,000
And by which, when the tatagata has entered into parinibhana,

9
00:01:22,000 --> 00:01:28,000
the good dhamma doesn't last long, doesn't last.

10
00:01:28,000 --> 00:01:31,000
So this quote only asks, does last?

11
00:01:31,000 --> 00:01:34,000
The next question is, what is the cause?

12
00:01:34,000 --> 00:01:39,000
What is the reason why it does last long?

13
00:01:39,000 --> 00:01:54,000
This is from the Angutranikaya book of seven,

14
00:01:54,000 --> 00:01:58,000
and so it's all this to seven things.

15
00:01:58,000 --> 00:02:06,000
Seven things that when you pay respect to the reverence,

16
00:02:06,000 --> 00:02:07,000
the word is,

17
00:02:07,000 --> 00:02:13,000
the word is,

18
00:02:13,000 --> 00:02:20,000
and gara means heavy, or garu means heavy.

19
00:02:20,000 --> 00:02:27,000
The word guru in Sanskrit,

20
00:02:27,000 --> 00:02:34,000
guru means teacher,

21
00:02:34,000 --> 00:02:40,000
I believe the origin is from the word heavy,

22
00:02:40,000 --> 00:02:47,000
and some garu, garu means to take something seriously,

23
00:02:47,000 --> 00:02:59,000
if we appreciate and think of as worth something,

24
00:02:59,000 --> 00:03:04,000
so we worship in a sense, in a literal sense,

25
00:03:04,000 --> 00:03:15,000
we place in a, we place worth on these things.

26
00:03:15,000 --> 00:03:18,000
Then the dhamma will last long after the Buddha goes,

27
00:03:18,000 --> 00:03:19,000
the Buddha disappears,

28
00:03:19,000 --> 00:03:21,000
because when the Buddha is around,

29
00:03:21,000 --> 00:03:29,000
you know, sattadeva, minusana means a teacher of all beings,

30
00:03:29,000 --> 00:03:31,000
an excelled teacher,

31
00:03:31,000 --> 00:03:35,000
an excelled trainer.

32
00:03:35,000 --> 00:03:38,000
So when he's around, there's no question,

33
00:03:38,000 --> 00:03:41,000
the dhamma will last.

34
00:03:41,000 --> 00:03:45,000
When he's gone, however, we've got left is,

35
00:03:45,000 --> 00:03:48,000
well, the arahan disciples of the Buddha

36
00:03:48,000 --> 00:03:51,000
and then they pass away, and then hopefully,

37
00:03:51,000 --> 00:03:55,000
more arahants come.

38
00:03:55,000 --> 00:03:58,000
The question is, what keeps the lineage going?

39
00:03:58,000 --> 00:04:01,000
What keeps the chain going?

40
00:04:01,000 --> 00:04:04,000
And so it's souptas like this that are of great interest

41
00:04:04,000 --> 00:04:08,000
to those of us in later generations,

42
00:04:08,000 --> 00:04:12,000
because it gives us an idea of how we support,

43
00:04:12,000 --> 00:04:15,000
not only our own practice,

44
00:04:15,000 --> 00:04:21,000
but how we carry on the legacy of the Buddha,

45
00:04:21,000 --> 00:04:26,000
pass on the legacy to future generations as well.

46
00:04:26,000 --> 00:04:30,000
How we take what was given to us

47
00:04:30,000 --> 00:04:33,000
and cherish it and nurture it,

48
00:04:33,000 --> 00:04:36,000
keep it alive.

49
00:04:36,000 --> 00:04:40,000
It's like we're given a seed to a great tree,

50
00:04:40,000 --> 00:04:44,000
and it's up to us to plant it and to cultivate it.

51
00:04:44,000 --> 00:04:48,000
We're future generations.

52
00:04:48,000 --> 00:04:50,000
The dhamma is like that.

53
00:04:50,000 --> 00:04:53,000
It's something, or it's like a fire in ancient times

54
00:04:53,000 --> 00:04:56,000
in cave time, in ancient ancient times.

55
00:04:56,000 --> 00:05:06,000
They had to carry a coal with them from campfire to campfire.

56
00:05:06,000 --> 00:05:10,000
It's just where to keep fire was to carry something.

57
00:05:10,000 --> 00:05:14,000
If the fire went out and in trouble,

58
00:05:14,000 --> 00:05:16,000
it had a fire carrier.

59
00:05:16,000 --> 00:05:20,000
I read a story about it once anyway.

60
00:05:20,000 --> 00:05:27,000
You have to care for dhamma.

61
00:05:27,000 --> 00:05:30,000
So how do we care for dhamma?

62
00:05:30,000 --> 00:05:36,000
What are the ways by which we ensure that

63
00:05:36,000 --> 00:05:41,000
this teaching will continue?

64
00:05:41,000 --> 00:05:44,000
So the Buddha gives seven things.

65
00:05:44,000 --> 00:05:48,000
We take these things seriously and appreciate them

66
00:05:48,000 --> 00:05:51,000
and in a sense, worship them,

67
00:05:51,000 --> 00:05:53,000
but worship, not in the way we use it.

68
00:05:53,000 --> 00:05:56,000
Just assign it some worth.

69
00:05:56,000 --> 00:06:00,000
And of course the first three are the Buddha,

70
00:06:00,000 --> 00:06:01,000
the dhamma, the sanga.

71
00:06:01,000 --> 00:06:03,000
The first one is the sata.

72
00:06:03,000 --> 00:06:06,000
The sata means teacher, but in Buddhism,

73
00:06:06,000 --> 00:06:08,000
it usually refers to the Buddha.

74
00:06:08,000 --> 00:06:10,000
And here that's what it refers to.

75
00:06:10,000 --> 00:06:18,000
It's a special word that is generally only used for the Buddha.

76
00:06:18,000 --> 00:06:21,000
It literally means teacher.

77
00:06:21,000 --> 00:06:29,000
It's something like teacher.

78
00:06:29,000 --> 00:06:34,000
And so if a monk or a bikhu or a bikkhuni,

79
00:06:34,000 --> 00:06:37,000
a male or female monk,

80
00:06:37,000 --> 00:06:41,000
or napasaka or passikka,

81
00:06:41,000 --> 00:06:45,000
a male or female lay disciple,

82
00:06:45,000 --> 00:06:48,000
that's just anyone who's a man or a woman,

83
00:06:48,000 --> 00:06:51,000
and anyone who's in between,

84
00:06:51,000 --> 00:06:58,000
anyone at all, if they do not take the teacher seriously,

85
00:06:58,000 --> 00:07:03,000
and assign worth and appreciation to the teacher,

86
00:07:03,000 --> 00:07:07,000
or the dhamma or the sanga.

87
00:07:07,000 --> 00:07:11,000
They do not dwell agar, agar,

88
00:07:11,000 --> 00:07:14,000
or wow, vhrenati.

89
00:07:14,000 --> 00:07:21,000
They dwell without reverence, without appreciation.

90
00:07:21,000 --> 00:07:26,000
Without taking seriously.

91
00:07:26,000 --> 00:07:31,000
And then the fourth is sika,

92
00:07:31,000 --> 00:07:33,000
the sika,

93
00:07:33,000 --> 00:07:36,000
the sika,

94
00:07:36,000 --> 00:07:43,000
the training sika.

95
00:07:43,000 --> 00:07:47,000
Sika means the trainings of the training,

96
00:07:47,000 --> 00:07:50,000
in a general sense,

97
00:07:50,000 --> 00:07:52,000
the training,

98
00:07:52,000 --> 00:07:59,000
in giving up craving, training to give up.

99
00:07:59,000 --> 00:08:05,000
And the training in morality,

100
00:08:05,000 --> 00:08:11,000
the training in concentration and wisdom.

101
00:08:11,000 --> 00:08:16,000
But here's specifically just the act

102
00:08:16,000 --> 00:08:20,000
of the things that we do like practicing meditation

103
00:08:20,000 --> 00:08:22,000
and keeping precepts,

104
00:08:22,000 --> 00:08:27,000
so the things that we do and the things that we don't do.

105
00:08:27,000 --> 00:08:29,000
We abstain from certain things

106
00:08:29,000 --> 00:08:36,000
and we take on certain behaviors, certain practices.

107
00:08:36,000 --> 00:08:40,000
This is the training and the learning,

108
00:08:40,000 --> 00:08:43,000
studying, listening to the dhamma,

109
00:08:43,000 --> 00:08:45,000
remembering the dhamma,

110
00:08:45,000 --> 00:08:50,000
explaining the dhamma, thinking about the dhamma,

111
00:08:50,000 --> 00:08:51,000
all of these things.

112
00:08:51,000 --> 00:08:53,000
This is all the training that we do.

113
00:08:53,000 --> 00:08:58,000
Taking that series, if we don't take that seriously.

114
00:08:58,000 --> 00:09:01,000
And number five is samadhi.

115
00:09:01,000 --> 00:09:03,000
We don't take concentration seriously,

116
00:09:03,000 --> 00:09:08,000
but it spells it out explicitly, concentration.

117
00:09:08,000 --> 00:09:10,000
Samadhi is,

118
00:09:10,000 --> 00:09:12,000
samadhi is an interesting word.

119
00:09:12,000 --> 00:09:19,000
It's just been focused, samadhi is like same.

120
00:09:19,000 --> 00:09:22,000
It means level or even.

121
00:09:22,000 --> 00:09:25,000
So not too much, not too little.

122
00:09:25,000 --> 00:09:28,000
When your mind gets perfectly focused,

123
00:09:28,000 --> 00:09:32,000
that's why it's kind of focused rather than concentration.

124
00:09:32,000 --> 00:09:35,000
It's like a camera lens, if you focus too much,

125
00:09:35,000 --> 00:09:38,000
it gets blurry, too little also blurry.

126
00:09:38,000 --> 00:09:43,000
You need perfect focus, and then you can see.

127
00:09:43,000 --> 00:09:45,000
Your mind has to be balanced.

128
00:09:45,000 --> 00:09:48,000
There's really what samadhi is all about.

129
00:09:48,000 --> 00:09:52,000
People think of samadhi as being concentrated,

130
00:09:52,000 --> 00:09:57,000
so no thoughts focused only on one thing,

131
00:09:57,000 --> 00:09:59,000
no distractions.

132
00:09:59,000 --> 00:10:01,000
But it's not necessarily that.

133
00:10:01,000 --> 00:10:05,000
It just means the mind that is seeing things clearly.

134
00:10:05,000 --> 00:10:09,000
The mind that is focused on something,

135
00:10:09,000 --> 00:10:12,000
as it is in that moment.

136
00:10:12,000 --> 00:10:15,000
You don't have to block everything out

137
00:10:15,000 --> 00:10:17,000
and just concentrate on a single thing.

138
00:10:17,000 --> 00:10:21,000
You have to be focused on whatever arises,

139
00:10:21,000 --> 00:10:24,000
clearly seeing it clearly.

140
00:10:24,000 --> 00:10:27,000
That's why we use this word when we say

141
00:10:27,000 --> 00:10:30,000
to ourselves seeing or hearing or rising,

142
00:10:30,000 --> 00:10:35,000
or seeing or falling, or trying to focus.

143
00:10:35,000 --> 00:10:39,000
Focus means on this, the core of it.

144
00:10:39,000 --> 00:10:43,000
The core of this is rising, or whatever rising means.

145
00:10:43,000 --> 00:10:44,000
This is falling.

146
00:10:44,000 --> 00:10:48,000
The core of seeing is seeing, the core of pain and pain.

147
00:10:48,000 --> 00:10:52,000
But there's no judgments or reactions or anything.

148
00:10:52,000 --> 00:11:02,000
Let's get rid of all of those.

149
00:11:02,000 --> 00:11:08,000
Number six is a pamade, a pamade, and a darwa.

150
00:11:08,000 --> 00:11:15,000
We don't think a pattis, a pattis, a pamade.

151
00:11:15,000 --> 00:11:25,000
The last words of the Buddha is that we should cultivate a pamade.

152
00:11:25,000 --> 00:11:30,000
Pamade comes from the root mud means to be drunk.

153
00:11:30,000 --> 00:11:34,000
Pamade is like really drunk.

154
00:11:34,000 --> 00:11:39,000
But it's just as prefix that oneifies it, that augments it.

155
00:11:39,000 --> 00:11:52,000
So pamade is like negligent or intoxicated.

156
00:11:52,000 --> 00:11:56,000
So mixed up in the mind.

157
00:11:56,000 --> 00:12:00,000
A pamade is to be clear-minded.

158
00:12:00,000 --> 00:12:04,000
To be unentoxicated, to be sober.

159
00:12:04,000 --> 00:12:09,000
So not drunk on lust, drunk on anger, drunk on delusion,

160
00:12:09,000 --> 00:12:13,000
or arrogance or conceit.

161
00:12:13,000 --> 00:12:17,000
To not be drunk on any of these emotions.

162
00:12:17,000 --> 00:12:19,000
So we should take that seriously.

163
00:12:19,000 --> 00:12:20,000
We should appreciate that.

164
00:12:20,000 --> 00:12:26,000
If we don't appreciate that, then set the Buddha says.

165
00:12:26,000 --> 00:12:33,000
And the seventh one is quite curious.

166
00:12:33,000 --> 00:12:39,000
But curious in a good way.

167
00:12:39,000 --> 00:12:42,000
It's just so surprising, I think.

168
00:12:42,000 --> 00:12:45,000
It's not what you'd expect as the last one.

169
00:12:45,000 --> 00:12:47,000
Patisantara.

170
00:12:47,000 --> 00:12:50,000
Adjan, my teacher talked about it.

171
00:12:50,000 --> 00:12:56,000
He mentions this.

172
00:12:56,000 --> 00:13:02,000
Many times the Buddha talks about Patisantara.

173
00:13:02,000 --> 00:13:06,000
Patisantara, dangarawa.

174
00:13:06,000 --> 00:13:09,000
Patisantara, dangarawa.

175
00:13:09,000 --> 00:13:13,000
If we don't take seriously or appreciate Patisantara.

176
00:13:13,000 --> 00:13:18,000
Patisantara is, I don't quite know the etymology,

177
00:13:18,000 --> 00:13:26,000
but it means hospitality.

178
00:13:26,000 --> 00:13:33,000
Or it could mean friendliness, it could mean goodwill, friendship.

179
00:13:33,000 --> 00:13:40,000
But it's really just hospitality when you welcome people.

180
00:13:40,000 --> 00:13:42,000
But in this context makes perfect sense.

181
00:13:42,000 --> 00:13:46,000
If you have a meditation center where you don't welcome people,

182
00:13:46,000 --> 00:13:52,000
when someone walks in, they don't know.

183
00:13:52,000 --> 00:13:56,000
If someone looks at them, what are you doing here?

184
00:13:56,000 --> 00:14:00,000
Sometimes you go to a monastery and no one wants to talk to you.

185
00:14:00,000 --> 00:14:04,000
Go to a meditation center and maybe no one, everyone's,

186
00:14:04,000 --> 00:14:08,000
if people are kind of putting on errors,

187
00:14:08,000 --> 00:14:12,000
like they're real meditators or something.

188
00:14:12,000 --> 00:14:17,000
Or if they just don't care and they're just concerned about their own practice.

189
00:14:17,000 --> 00:14:23,000
If such a place existed, then how would you ever share?

190
00:14:23,000 --> 00:14:28,000
How would you ever lead to spreading the dangarawa?

191
00:14:28,000 --> 00:14:34,000
Well, there are places where you go into the office,

192
00:14:34,000 --> 00:14:38,000
the meditation center office, and they just yell at you.

193
00:14:38,000 --> 00:14:46,000
They look down upon you and they don't know how to deal with people.

194
00:14:46,000 --> 00:14:51,000
And I've seen, I've talked with other people.

195
00:14:51,000 --> 00:14:55,000
In John Tang, the people in the office would rotate.

196
00:14:55,000 --> 00:14:59,000
And so you had to catch the right person in the office.

197
00:14:59,000 --> 00:15:02,000
Or you never know what was going to happen.

198
00:15:02,000 --> 00:15:07,000
You want to book a room for someone and maybe they yell at you or maybe they're very nice.

199
00:15:07,000 --> 00:15:14,000
You had to find the right person and talk to them.

200
00:15:14,000 --> 00:15:16,000
It's very important.

201
00:15:16,000 --> 00:15:18,000
So it's something for us to remember.

202
00:15:18,000 --> 00:15:20,000
We have to welcome them.

203
00:15:20,000 --> 00:15:24,000
And in general, you could talk about there's something I was thinking about.

204
00:15:24,000 --> 00:15:32,000
It'd be neat someday if our community grows,

205
00:15:32,000 --> 00:15:38,000
if we could have groups of people.

206
00:15:38,000 --> 00:15:46,000
Like maybe once a week, we could have kind of a more formal online session.

207
00:15:46,000 --> 00:15:54,000
And if someone was organizing a group in their home,

208
00:15:54,000 --> 00:15:59,000
then they could join the hangout with their group.

209
00:15:59,000 --> 00:16:02,000
Wow, wouldn't that be neat?

210
00:16:02,000 --> 00:16:04,000
That's what we should do.

211
00:16:04,000 --> 00:16:10,000
Once a week, we could do once a month to start.

212
00:16:10,000 --> 00:16:14,000
We would have groups of people.

213
00:16:14,000 --> 00:16:19,000
Someone puts together a group in their home,

214
00:16:19,000 --> 00:16:21,000
and people come to their home.

215
00:16:21,000 --> 00:16:24,000
And they join the hangout.

216
00:16:24,000 --> 00:16:30,000
We could have up to ten groups.

217
00:16:30,000 --> 00:16:33,000
And we have to talk about that.

218
00:16:33,000 --> 00:16:37,000
So the idea is to welcome people even into your home.

219
00:16:37,000 --> 00:16:39,000
You set up a group.

220
00:16:39,000 --> 00:16:40,000
People do this.

221
00:16:40,000 --> 00:16:42,000
They have a demo group in their home.

222
00:16:42,000 --> 00:16:47,000
They've got space.

223
00:16:47,000 --> 00:16:52,000
And then they set up.

224
00:16:52,000 --> 00:16:55,000
Sometimes they just meditate together.

225
00:16:55,000 --> 00:16:57,000
Sometimes they listen to a talk.

226
00:16:57,000 --> 00:17:05,000
And then they do meditation together.

227
00:17:05,000 --> 00:17:10,000
But we could do something like that.

228
00:17:10,000 --> 00:17:13,000
But it could be live.

229
00:17:13,000 --> 00:17:16,000
Live from all over the world.

230
00:17:16,000 --> 00:17:18,000
Ten different groups.

231
00:17:18,000 --> 00:17:19,000
That's the maximum.

232
00:17:19,000 --> 00:17:22,000
You can have ten people in the hangout.

233
00:17:22,000 --> 00:17:26,000
So if anybody's interested in setting up a demo group,

234
00:17:26,000 --> 00:17:29,000
let me know.

235
00:17:29,000 --> 00:17:34,000
And we'll try and arrange something where we meet together like this.

236
00:17:34,000 --> 00:17:41,000
But we'll have a special day where I'll give a real talk.

237
00:17:41,000 --> 00:17:44,000
I'll give a longer talk.

238
00:17:44,000 --> 00:17:51,000
And then we'll connect.

239
00:17:51,000 --> 00:17:52,000
Right.

240
00:17:52,000 --> 00:18:00,000
So there's one list of seven things that lead to the dumb

241
00:18:00,000 --> 00:18:02,000
lasting.

242
00:18:02,000 --> 00:18:04,000
So if we don't spread it, if we don't share it,

243
00:18:04,000 --> 00:18:07,000
if we're not welcoming of people who want to learn.

244
00:18:07,000 --> 00:18:11,000
And the Buddha wasn't big on spreading the dumb land in terms

245
00:18:11,000 --> 00:18:14,000
of going around teaching people.

246
00:18:14,000 --> 00:18:17,000
But much more about, as far as I can see,

247
00:18:17,000 --> 00:18:21,000
much more about welcoming people who wanted to learn.

248
00:18:21,000 --> 00:18:27,000
If people wanted to learn, it was all about finding ways to accommodate them.

249
00:18:27,000 --> 00:18:29,000
If they do come, if they don't come,

250
00:18:29,000 --> 00:18:32,000
don't have to go out of our way looking for people.

251
00:18:32,000 --> 00:18:40,000
We're not trying to push it this on anybody.

252
00:18:40,000 --> 00:18:41,000
That's kind of how beautiful it is.

253
00:18:41,000 --> 00:18:42,000
We don't need students.

254
00:18:42,000 --> 00:18:44,000
We're not looking for students.

255
00:18:44,000 --> 00:18:48,000
We're just people who are looking for it.

256
00:18:48,000 --> 00:18:52,000
We open the door for them, provide them the opportunity.

257
00:18:52,000 --> 00:18:55,000
Tonight a woman came to visit.

258
00:18:55,000 --> 00:18:57,000
She lived in Cambodia for nine years.

259
00:18:57,000 --> 00:19:01,000
And she just came and she's seen some of my videos.

260
00:19:01,000 --> 00:19:07,000
And I gave her the booklet.

261
00:19:07,000 --> 00:19:09,000
And then she just did meditation.

262
00:19:09,000 --> 00:19:10,000
I didn't meditate with her.

263
00:19:10,000 --> 00:19:12,000
I came upstairs and did my, because I do walking.

264
00:19:12,000 --> 00:19:15,000
She didn't want to do walking.

265
00:19:15,000 --> 00:19:20,000
But it'd be nice if we could have a group here.

266
00:19:20,000 --> 00:19:22,000
Here as well.

267
00:19:22,000 --> 00:19:25,000
I think probably what we do is just have people come up here at nine.

268
00:19:25,000 --> 00:19:28,000
If someone wants to hear the dhamma, they can come at nine.

269
00:19:28,000 --> 00:19:34,000
Come sit up with us here.

270
00:19:34,000 --> 00:19:36,000
For now anyway.

271
00:19:36,000 --> 00:19:40,000
We're looking to get a bigger place.

272
00:19:40,000 --> 00:19:45,000
Anyway, so these seven, the Buddha, the dhamma, the sanga, take them seriously.

273
00:19:45,000 --> 00:19:46,000
That's another thing.

274
00:19:46,000 --> 00:19:49,000
Often people don't take the Buddha too seriously.

275
00:19:49,000 --> 00:19:57,000
They know these talks about people who spit on Buddha images and burn them.

276
00:19:57,000 --> 00:19:59,000
Just treat them like rubbish.

277
00:19:59,000 --> 00:20:02,000
Thinking that's not the real Buddha.

278
00:20:02,000 --> 00:20:04,000
But there's something to it.

279
00:20:04,000 --> 00:20:07,000
If you don't take the Buddha images seriously,

280
00:20:07,000 --> 00:20:10,000
what does that say about how you feel about the Buddha?

281
00:20:10,000 --> 00:20:15,000
Yeah, people say it's just an image in the Buddha's and whatever,

282
00:20:15,000 --> 00:20:17,000
but there's something to images.

283
00:20:17,000 --> 00:20:19,000
They represent something.

284
00:20:19,000 --> 00:20:22,000
In ancient times they wouldn't even make images

285
00:20:22,000 --> 00:20:24,000
because they've revered the Buddha.

286
00:20:24,000 --> 00:20:28,000
It seems because they revered the Buddha so much.

287
00:20:28,000 --> 00:20:32,000
So when we have these images, we try to treat them quite carefully

288
00:20:32,000 --> 00:20:36,000
because we respect the Buddha so much.

289
00:20:36,000 --> 00:20:39,000
If you don't, I mean, this is the thing.

290
00:20:39,000 --> 00:20:43,000
It's the dhamma won't last because there's no figure.

291
00:20:43,000 --> 00:20:47,000
There's none of this sort of religious feeling

292
00:20:47,000 --> 00:20:49,000
that keeps things together.

293
00:20:49,000 --> 00:20:53,000
The sense of urgency, the sense of zeal and interest.

294
00:20:53,000 --> 00:20:58,000
That's so powerful and any religion could be for the purposes of evil.

295
00:20:58,000 --> 00:21:03,000
It could be for the purposes of good, but it's a power.

296
00:21:03,000 --> 00:21:05,000
If you don't take these things seriously,

297
00:21:05,000 --> 00:21:09,000
I don't appreciate them, don't revere them.

298
00:21:09,000 --> 00:21:12,000
Very hard to keep going.

299
00:21:12,000 --> 00:21:16,000
If you talk about secular Buddhism, yeah, it's fine,

300
00:21:16,000 --> 00:21:20,000
but it's hard to get that feeling and appreciation.

301
00:21:20,000 --> 00:21:23,000
Part of religion is the feeling.

302
00:21:23,000 --> 00:21:30,000
The sense of reverence, appreciation,

303
00:21:30,000 --> 00:21:33,000
not just taking something clinically in terms of,

304
00:21:33,000 --> 00:21:36,000
yes, this helps me relieve stress, but yes,

305
00:21:36,000 --> 00:21:40,000
this is the teaching of the perfectly enlightened Buddha.

306
00:21:40,000 --> 00:21:46,000
It carries a lot more weight.

307
00:21:46,000 --> 00:21:51,000
And then the training, concentration,

308
00:21:51,000 --> 00:21:58,000
upper mod, which is vigilance or sobriety

309
00:21:58,000 --> 00:22:02,000
and hospitality.

310
00:22:02,000 --> 00:22:04,000
We have to take the training seriously,

311
00:22:04,000 --> 00:22:05,000
both study and practice.

312
00:22:05,000 --> 00:22:07,000
We have to take concentration,

313
00:22:07,000 --> 00:22:10,000
and remember to try and be concentrated.

314
00:22:10,000 --> 00:22:15,000
Remember in practice, not just study.

315
00:22:15,000 --> 00:22:18,000
And be mindful.

316
00:22:18,000 --> 00:22:20,000
Upper mod actually, being sober,

317
00:22:20,000 --> 00:22:24,000
it's the Buddha said it's a synonym for being mindful,

318
00:22:24,000 --> 00:22:26,000
so this really means using mindfulness.

319
00:22:26,000 --> 00:22:29,000
Seeing things clearly as they are.

320
00:22:29,000 --> 00:22:32,000
Grasping things as they are.

321
00:22:32,000 --> 00:22:35,000
Remembering things as they are.

322
00:22:35,000 --> 00:22:38,000
And finally, we have to be hospitable,

323
00:22:38,000 --> 00:22:41,000
so we have to welcome people to join,

324
00:22:41,000 --> 00:22:43,000
not just practicing for ourselves,

325
00:22:43,000 --> 00:22:46,000
but providing the opportunity

326
00:22:46,000 --> 00:22:49,000
and being friendly and welcoming.

327
00:22:49,000 --> 00:22:52,000
Don't just shy away and say,

328
00:22:52,000 --> 00:22:53,000
oh, I'm not a teacher.

329
00:22:53,000 --> 00:22:54,000
I can't teach you the done.

330
00:22:54,000 --> 00:22:59,000
Anyone can teach you how you are taught, pass it on.

331
00:22:59,000 --> 00:23:02,000
It doesn't mean you have to answer all of their questions

332
00:23:02,000 --> 00:23:04,000
and problems and give them advice.

333
00:23:04,000 --> 00:23:06,000
It just means you have to explain to them

334
00:23:06,000 --> 00:23:10,000
how to do meditation, which is quite simple.

335
00:23:10,000 --> 00:23:13,000
And just reassure them that it has benefits,

336
00:23:13,000 --> 00:23:17,000
and if they try it, they will see the benefits for themselves.

337
00:23:17,000 --> 00:23:22,000
That's all you need to do.

338
00:23:22,000 --> 00:23:29,000
So, well, that's the demo for tonight.

339
00:23:36,000 --> 00:23:41,000
And you guys can go.

340
00:23:41,000 --> 00:23:53,000
Okay, Larry has a question.

341
00:23:53,000 --> 00:23:59,000
Well, noting posture, movements, intentions.

342
00:23:59,000 --> 00:24:02,000
I might lapse into contemplating death

343
00:24:02,000 --> 00:24:04,000
or contemplating my good fortune.

344
00:24:04,000 --> 00:24:08,000
Then I realize I'm contemplating and not noting.

345
00:24:08,000 --> 00:24:11,000
I should once balance the process of noting the process

346
00:24:11,000 --> 00:24:15,000
of wholesome contemplation.

347
00:24:15,000 --> 00:24:19,000
Well, if you are, if that thought arises,

348
00:24:19,000 --> 00:24:20,000
that's fine.

349
00:24:20,000 --> 00:24:21,000
You can do both.

350
00:24:21,000 --> 00:24:23,000
You just have the thought and then you say thinking,

351
00:24:23,000 --> 00:24:31,000
thinking, if you feel happy, you can say happy, happy.

352
00:24:31,000 --> 00:24:35,000
I mean, the thought arises of its own.

353
00:24:35,000 --> 00:24:37,000
We're just trying to be mindful of it.

354
00:24:37,000 --> 00:24:45,000
Because even wholesomeness can be caught up in delusion.

355
00:24:45,000 --> 00:24:53,000
Not directly, but you can become unwholesome about your wholesomeness.

356
00:24:53,000 --> 00:24:55,000
If you start to get attached to it,

357
00:24:55,000 --> 00:25:10,000
patch to the idea of it anyway.

358
00:25:10,000 --> 00:25:13,000
I mean, it won't lead to freedom,

359
00:25:13,000 --> 00:25:18,000
so you can switch back and forth.

360
00:25:18,000 --> 00:25:21,000
For the death one, especially, you know,

361
00:25:21,000 --> 00:25:25,000
if you're mindful of death, that's a useful meditation.

362
00:25:25,000 --> 00:25:29,000
It's good to give you the impetus to practice.

363
00:25:29,000 --> 00:25:31,000
It gives you those religious feeling.

364
00:25:31,000 --> 00:25:35,000
Some way, go, we call it.

365
00:25:35,000 --> 00:25:37,000
So that's mindfulness, a different meditation.

366
00:25:37,000 --> 00:25:39,000
It's useful in practice that,

367
00:25:39,000 --> 00:25:42,000
and then go back to practicing mindfulness.

368
00:25:42,000 --> 00:25:49,000
As far as the contemplation of good fortune,

369
00:25:49,000 --> 00:25:52,000
I'd be careful of that,

370
00:25:52,000 --> 00:25:57,000
because it can slip into complacency.

371
00:25:57,000 --> 00:25:59,000
The angels think like that.

372
00:25:59,000 --> 00:26:03,000
They think everything, they think they're there.

373
00:26:03,000 --> 00:26:06,000
They've got some safe tears on.

374
00:26:06,000 --> 00:26:09,000
Probably you're not having that problem,

375
00:26:09,000 --> 00:26:15,000
but you have to be careful that I'm not convinced that it's wholesome.

376
00:26:15,000 --> 00:26:22,000
We talk about contentment. There's something in there that's probably associated with contentment.

377
00:26:22,000 --> 00:26:27,000
But appreciation is a lot like liking, you know,

378
00:26:27,000 --> 00:26:33,000
and clinging, because things can change at any time, right?

379
00:26:33,000 --> 00:26:37,000
Your safety is completely impermanent.

380
00:26:37,000 --> 00:26:45,000
It's an illusion. There's no safety and some sorrow.

381
00:26:45,000 --> 00:26:49,000
Everything can leave you in a moment,

382
00:26:49,000 --> 00:26:52,000
so in what way is it safe?

383
00:26:52,000 --> 00:27:02,000
In the end, it's all just seeing, hearing, smelling, tasting, feeling, thinking.

384
00:27:02,000 --> 00:27:07,000
I'd be continuing the series on the Damapada.

385
00:27:07,000 --> 00:27:09,000
It's probably the only reason I'd continue to miss,

386
00:27:09,000 --> 00:27:11,000
because people ask me the questions like this,

387
00:27:11,000 --> 00:27:15,000
because I always think, oh, well, maybe nobody wants them anymore.

388
00:27:15,000 --> 00:27:18,000
Nobody's asked about them in a while,

389
00:27:18,000 --> 00:27:25,000
so probably people are sick of them.

390
00:27:25,000 --> 00:27:31,000
I was busy with finals, but it's kind of just, well, now I'm not.

391
00:27:31,000 --> 00:27:39,000
Sure, I can start up the Damapada series again.

392
00:27:39,000 --> 00:27:51,000
I mean, it's not that I stopped. I can continue to do more Damapada if people want it.

393
00:27:51,000 --> 00:27:55,000
How is it that we bow down to and review the meditation practice?

394
00:27:55,000 --> 00:27:57,000
I mean, the practice itself.

395
00:27:57,000 --> 00:28:03,000
Should we hold the triple gem or the practice in mind for reverence?

396
00:28:03,000 --> 00:28:06,000
Well, I don't think you need to hold anything in mind for reverence.

397
00:28:06,000 --> 00:28:10,000
It's just taking that serious thing, really.

398
00:28:10,000 --> 00:28:15,000
I mean, you can do meditations on the Buddha, the Damapada, and the Sangha, obviously.

399
00:28:15,000 --> 00:28:17,000
But that's not what I don't think the Buddha is saying.

400
00:28:17,000 --> 00:28:20,000
It's just if they don't respect.

401
00:28:20,000 --> 00:28:25,000
Because there's a lot of disrespect, you know, disrespect for the Sangha, for example.

402
00:28:25,000 --> 00:28:28,000
Monks get a lot of disrespect.

403
00:28:28,000 --> 00:28:32,000
All of the teachers get a lot of disrespect.

404
00:28:32,000 --> 00:28:35,000
Not a lot, not mostly respect.

405
00:28:35,000 --> 00:28:40,000
But there's always people who have very little respect.

406
00:28:40,000 --> 00:28:46,000
The point is, well, that is harmful to the...

407
00:28:46,000 --> 00:28:51,000
You could say respect has to be earned, sure, fine.

408
00:28:51,000 --> 00:28:55,000
But there's something about being respectful.

409
00:28:55,000 --> 00:29:00,000
If you don't want to practice, you don't have to come practice.

410
00:29:00,000 --> 00:29:05,000
But if someone's teaching, there's a lot of...

411
00:29:05,000 --> 00:29:09,000
And then there's disrespect to the Buddha.

412
00:29:09,000 --> 00:29:15,000
Disrespect to the Damapada.

413
00:29:15,000 --> 00:29:19,000
You know, not taking it seriously.

414
00:29:19,000 --> 00:29:21,000
Disrespect.

415
00:29:21,000 --> 00:29:23,000
Not as a disrespect, right?

416
00:29:23,000 --> 00:29:26,000
But not taking it at practice seriously.

417
00:29:26,000 --> 00:29:30,000
You know, people who do walking meditation, talking on the phone.

418
00:29:30,000 --> 00:29:32,000
Or...

419
00:29:32,000 --> 00:29:35,000
It's not even how disrespectful that is.

420
00:29:35,000 --> 00:29:39,000
It's just, you know, if you don't take it seriously, you're not going to get any results.

421
00:29:39,000 --> 00:29:44,000
So the question, if you do it, hour of meditation, how much are you really meditating?

422
00:29:44,000 --> 00:29:47,000
Are you really taking it seriously?

423
00:29:47,000 --> 00:29:49,000
Are you respect?

424
00:29:49,000 --> 00:29:51,000
And it's not respect in terms of disrespect.

425
00:29:51,000 --> 00:29:53,000
It's like, do you appreciate?

426
00:29:53,000 --> 00:29:55,000
That's the best.

427
00:29:55,000 --> 00:29:56,000
Do you appreciate?

428
00:29:56,000 --> 00:29:57,000
Garro means heavy.

429
00:29:57,000 --> 00:30:01,000
So it's like taking seriously, seeing it as a weighty thing.

430
00:30:01,000 --> 00:30:03,000
Or do you see it, do you take it lightly?

431
00:30:03,000 --> 00:30:04,000
Right?

432
00:30:04,000 --> 00:30:07,000
See how positive taking something lightly?

433
00:30:07,000 --> 00:30:09,000
If you take meditation lightly, it's hard.

434
00:30:09,000 --> 00:30:11,000
You won't get the results.

435
00:30:11,000 --> 00:30:15,000
If you don't get the results, but it's a more pathway.

436
00:30:15,000 --> 00:30:30,000
But yeah, if you want to do this, if you want to pay respect.

437
00:30:30,000 --> 00:30:34,000
There's a quick chant that we do in the opening ceremony.

438
00:30:34,000 --> 00:30:36,000
So I often do it as a...

439
00:30:36,000 --> 00:30:39,000
Kind of like a mantra, really.

440
00:30:39,000 --> 00:30:41,000
We pay respect to the five things.

441
00:30:41,000 --> 00:30:44,000
The Buddha, the dhamma, the sangha, the meditation practice,

442
00:30:44,000 --> 00:30:51,000
and the teacher who offers the meditation practice.

443
00:30:51,000 --> 00:30:55,000
ivistat- going to see the wateriva Spirits.

444
00:30:55,000 --> 00:30:56,000
The know.

445
00:30:56,000 --> 00:31:04,000
Dhamma, cool into your body, bring yourself the

446
00:31:04,000 --> 00:31:05,000
above.

447
00:31:05,000 --> 00:31:14,320
Nama ami kamatana daya kachariyam nibhana mago desa kan sabandos an kamantu no.

448
00:31:14,320 --> 00:31:24,440
The last part sabandos an for all faults, for all faults, kamantu no may they forgive us.

449
00:31:24,440 --> 00:31:37,400
They forgive us all discussions, any wrongdoing.

450
00:31:37,400 --> 00:31:41,240
It's in the opening ceremony, I'm not sure where you get the opening ceremony.

451
00:31:41,240 --> 00:31:48,920
I think it's in our, whereas the opening ceremony, maybe it's not even online.

452
00:31:48,920 --> 00:31:57,040
But it might be taken from the visudhi manga, a lot of that is taken from the visudhi manga.

453
00:31:57,040 --> 00:32:07,880
There may be not.

454
00:32:07,880 --> 00:32:17,520
Respect for the five, it's what we do before we start the minute.

455
00:32:17,520 --> 00:32:23,360
It's the first thing we do in the, in the opening ceremony, almost the first thing, first

456
00:32:23,360 --> 00:32:24,360
big thing.

457
00:32:24,360 --> 00:32:30,480
I don't know, this is the first thing isn't it?

458
00:32:30,480 --> 00:32:31,480
Yeah.

459
00:32:31,480 --> 00:32:33,880
It's how we start the opening ceremony and the closing ceremony.

460
00:32:33,880 --> 00:32:39,960
So I don't do ceremonies here, not yet anyway, but normally we'd have a ceremony

461
00:32:39,960 --> 00:32:48,480
where we'd go through all of this in polyn, it's quite nice, the kind of, it's the thing

462
00:32:48,480 --> 00:32:51,880
you take, it should take it seriously, no, if we were to take it seriously we probably

463
00:32:51,880 --> 00:32:54,640
should do the ceremonies.

464
00:32:54,640 --> 00:32:59,240
But you know in the West sometimes, again, people don't take it seriously enough, it's

465
00:32:59,240 --> 00:33:07,160
hard to push that on them, it's not entirely necessary, it doesn't mean you do a ceremony,

466
00:33:07,160 --> 00:33:13,240
that's the thing, it's just a ceremony, but something to consider once we get established

467
00:33:13,240 --> 00:33:18,560
here, maybe I can teach Michael, because another thing is you need two people, I do part,

468
00:33:18,560 --> 00:33:23,760
but I need a lay person to lead the meditators, I need someone to lead the meditators,

469
00:33:23,760 --> 00:33:31,920
I'll teach Michael how to do it someday, if he sticks around, maybe he'll become a monk,

470
00:33:31,920 --> 00:33:38,520
if he does I get to give him the name Mogharaja again, because it's the closest thing

471
00:33:38,520 --> 00:33:40,480
to Michael, right?

472
00:33:40,480 --> 00:33:47,760
So our last Michael got the name Mogharaja, and boy did that, it caused us to, you know,

473
00:33:47,760 --> 00:33:53,160
the Sri Lankan people said they didn't want to bow down to it, because the word Moghar

474
00:33:53,160 --> 00:34:01,240
means useless or bad or stupid, but Mogharaja was one of the Buddha's chief disciples,

475
00:34:01,240 --> 00:34:17,640
it's one of the 80 great disciples, shame, you know, I have the five reverends of somewhere,

476
00:34:17,640 --> 00:34:33,400
I'm not sure where, but probably right away somewhere in my documents folder, you copy them.

477
00:34:33,400 --> 00:34:42,560
Anyway, that's all for tonight, thank you all for tuning in, have a good night.

478
00:34:42,560 --> 00:35:09,520
.

