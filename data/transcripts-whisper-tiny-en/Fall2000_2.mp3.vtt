WEBVTT

00:00.000 --> 00:15.280
Today, we continue our talk on the four foundations of mindfulness.

00:15.280 --> 00:30.720
Now, the benefit of the practice of mindfulness is shown by the words removing covetousness

00:30.720 --> 00:33.240
and grief in the world.

00:33.240 --> 00:37.280
So this is the benefit of the practice.

00:37.280 --> 00:51.160
So when a yogi practices mindfulness and he is able to remove covetousness and grief, he

00:51.160 --> 01:06.960
is able to prevent covetousness and grief from arising in his mind.

01:06.960 --> 01:20.200
With regard to removing, the commentaries mentioned three kinds of removing.

01:20.200 --> 01:27.240
On the first kind, we will call momentary removing.

01:27.240 --> 01:42.680
In the second, temporary removing and the third total removing.

01:42.680 --> 01:56.920
Momentary removing means removing just at the moment when the mindfulness is present.

01:56.920 --> 02:05.080
Now this removing is called momentary removing because it can remove only at the moment

02:05.080 --> 02:11.040
when the mindfulness is arising.

02:11.040 --> 02:21.080
Now, suppose there is darkness and then you turn on the switch and there is light.

02:21.080 --> 02:28.920
So the moment there is light, darkness is dispelled.

02:28.920 --> 02:33.960
But the moment you turn off the switch again, the darkness comes back.

02:33.960 --> 02:43.280
So in the same way, when the removing is just for a moment, like when you are mindful,

02:43.280 --> 02:51.920
the mental hindrance cannot arise, but when you are not mindful, there is no mindfulness

02:51.920 --> 02:55.800
in your mind, the mental defalments come back.

02:55.800 --> 03:08.800
So such removing by actually substituting the mental defalments with some wholesome mental

03:08.800 --> 03:18.520
states is called momentary removing.

03:18.520 --> 03:30.120
The second kind of removing, temporary removing is preventing the mental defalments from

03:30.120 --> 03:39.920
arising for some time or for a longer period of time, longer than a moment.

03:39.920 --> 03:51.000
Now when, let us see, there is a pond and in the water there are water plants.

03:51.000 --> 04:02.280
Now, it buffalo would wait through the water and through the water plants.

04:02.280 --> 04:15.080
So when the buffalo goes through the water, the plants, the water plants are divided.

04:15.080 --> 04:19.560
And they will not come back together again for some time.

04:19.560 --> 04:24.200
Usually after some time, they will come back slowly and meet together.

04:24.200 --> 04:33.260
So in the same way, the temporary abandonment is abandonment for some time so that the

04:33.260 --> 04:43.320
mental defalments will not come back again for some time.

04:43.320 --> 04:54.400
The third kind of removing is total removing means, removing the mental defalments once

04:54.400 --> 05:05.280
and for all so that they do not arise in the mind of a yogi again or anymore.

05:05.280 --> 05:18.080
The first kind of removing a yogi achieves when he practices mindfulness or wipasana meditation.

05:18.080 --> 05:25.760
Now when he practices mindfulness or wipasana meditation, he makes effort and he tries to

05:25.760 --> 05:40.120
be mindful of the object and as the mindfulness grows in strength, he begins to see the

05:40.120 --> 05:49.520
objects clearly and he begins to see the objects one after another coming and going and

05:49.520 --> 05:59.560
also he begins to see objects arising and disappearing and so he knows that the objects

05:59.560 --> 06:08.040
are impermanent and suffering in the sense that they are afflicted by arising and disappearing

06:08.040 --> 06:15.240
and he also knows that there is no control over these objects arising and disappearing

06:15.240 --> 06:18.400
and also there is no substance in it.

06:18.400 --> 06:29.840
So when he sees in this way, he is preventing the mental defalments from arising at that

06:29.840 --> 06:39.800
moment or when he is seeing in this way, mental defalments do not get chance to arise

06:39.800 --> 06:41.600
in his mind.

06:41.600 --> 06:53.480
So while he is practicing mindfulness and while he experiences the clear comprehension,

06:53.480 --> 07:05.280
he is said to be removing the mental defalments momentarily.

07:05.280 --> 07:15.680
So at every moment of mindfulness practice, a yogi is achieving this first kind of removal

07:15.680 --> 07:21.920
of mental defalments moment by moment.

07:21.920 --> 07:30.440
But as soon as a yogi stops the practice of mindfulness, these mental defalments will get

07:30.440 --> 07:36.280
chance to arise in his mind again.

07:36.280 --> 07:43.920
The second kind of removal, a yogi achieves when he gets Jana.

07:43.920 --> 07:54.840
Now Jana is the highest state of consciousness and with Jana, these mental defalments can

07:54.840 --> 08:02.520
be pushed away for some period of time.

08:02.520 --> 08:12.880
The mental defalments removed by a Jana experience may not come back to the yogi for some

08:12.880 --> 08:19.080
time.

08:19.080 --> 08:28.000
The third kind of removal is achieved when a yogi reaches enlightenment.

08:28.000 --> 08:36.800
Now, at the moment of enlightenment, as I said before, a type of consciousness called

08:36.800 --> 08:47.800
heart consciousness arises and that thought consciousness takes Nibana as object and it removes

08:47.800 --> 08:50.960
or eradicates mental defalments.

08:50.960 --> 09:00.960
Now, there are four stages of enlightenment and at the first stage, some mental defalments

09:00.960 --> 09:10.800
are eradicated and at the second stage, the remaining mental defalments are made weaker

09:10.800 --> 09:17.640
and at the third stage, some more mental defalments are eradicated or removed and at the

09:17.640 --> 09:25.120
fourth stage, all the remaining mental defalments are eradicated.

09:25.120 --> 09:32.120
So once it are eradicated, these mental defalments will not arise again in that person.

09:32.120 --> 09:43.040
So that removal is called the total removal.

09:43.040 --> 09:53.840
So when a yogi practices Vipasana meditation he achieves momentary removal of mental defalments

09:53.840 --> 10:08.600
and when a yogi gains Jana he achieves the temporary removing and when he gains enlightenment

10:08.600 --> 10:12.440
he achieves the total removing.

10:12.440 --> 10:25.920
Now, Mahasi Shiro explained here that during the practice of Vipasana meditation, the

10:25.920 --> 10:30.240
temporary removal can be experienced.

10:30.240 --> 10:38.680
So he explained in this way, first a yogi practices mindfulness on objects and then

10:38.680 --> 10:47.640
he, his mindfulness become stronger and he gains good samadhi and he begins to see the

10:47.640 --> 10:54.680
true nature of things that is he begins to see the impermanent nature, suffering nature

10:54.680 --> 10:58.040
and non-soul nature of things.

10:58.040 --> 11:10.280
So when he sees the objects clearly in this way, he is said to be achieving momentary

11:10.280 --> 11:13.640
removal of mental defilements.

11:13.640 --> 11:25.480
Now when he has gained experience in achieving momentary removal, his mind will become so

11:25.480 --> 11:44.280
refined, his samadhi, his concentration, so strong that even if he, even if he is not

11:44.280 --> 11:56.040
mindful of an object, the mental defilements do not arise on these objects.

11:56.040 --> 12:05.760
Normally if a yogi does not practice mindfulness on an object, then the mental defilements

12:05.760 --> 12:08.360
will arise regarding that object.

12:08.360 --> 12:19.600
But now this yogi has gained so much experience in momentary removal that even if he does

12:19.600 --> 12:29.840
not practice mindfulness on an object, no mental defilements will arise in him.

12:29.840 --> 12:44.240
Even if he stopped meditating, his mind is so pure that no mental defilements will arise

12:44.240 --> 12:45.800
in his mind.

12:45.800 --> 12:53.960
So a yogi may think that I have eradicated mental defilements or no gross mental defilements

12:53.960 --> 12:56.400
can arise in my mind now.

12:56.400 --> 13:14.480
So that the ability to prevent mental defilements from arising even when they are not

13:14.480 --> 13:25.680
made the object of mindfulness is what we call temporary abandonment or temporary removal

13:25.680 --> 13:31.760
in the practice of vipassana.

13:31.760 --> 13:44.280
Again please note that removing here means not giving the mental defilements chance to

13:44.280 --> 13:47.080
arise.

13:47.080 --> 14:00.920
And that is achieved when a yogi is mindful and when he clearly comprehends the objects.

14:00.920 --> 14:13.240
So being mindful and clearly comprehending a yogi is said to be removing the mental defilements.

14:13.240 --> 14:23.440
So that means he does not have to make special effort to remove a mental defilement.

14:23.440 --> 14:34.000
But when he is mindful of the object, when he comprehends the object clearly that means

14:34.000 --> 14:41.960
when he sees the true nature of things, then he is at the same time said to be removing

14:41.960 --> 14:49.080
the mental defilements.

14:49.080 --> 15:00.280
And the words in the world should be understood according to where they are mentioned.

15:00.280 --> 15:08.640
So in the number one sentence, in the world means in the body.

15:08.640 --> 15:14.520
And in number two sentence, in the world means in the feelings, in number three sentence,

15:14.520 --> 15:17.440
in the world means in the consciousness.

15:17.440 --> 15:29.320
And in number four, sentence in the world means in the Dharma objects.

15:29.320 --> 15:36.640
So in number one sentence, Buddha said, if he could dwells contemplating the body in

15:36.640 --> 15:37.840
the body.

15:37.840 --> 15:51.040
So the word body is repeated and that is for the observation to be precise.

15:51.040 --> 15:57.960
So the commentary explains that contemplating or observing the body in the body means, observing

15:57.960 --> 16:03.440
the body in the body and not observing and the feeling in the body, consciousness in the

16:03.440 --> 16:06.920
body and Dharma objects in the body.

16:06.920 --> 16:26.280
So it is to show the precision of observation that the word body is mentioned twice.

16:26.280 --> 16:33.240
In this sentence, also the word body is to be understood according to the context.

16:33.240 --> 16:42.920
Now in the discourse on the four foundations of mindfulness, the body contemplation is described

16:42.920 --> 16:48.720
in 14 different ways.

16:48.720 --> 16:55.240
The first section is the mindfulness of breathing.

16:55.240 --> 17:01.800
Now the second is the mindfulness of the postures of the body.

17:01.800 --> 17:09.800
And the third is called the section on clear comprehension that is mindfulness of smaller

17:09.800 --> 17:20.080
activities like going forward, going back, stretching, bending and so on.

17:20.080 --> 17:31.320
And then there is a section on repulsiveness of the body or 32 parts of the body.

17:31.320 --> 17:36.040
And then there is a section on the four material elements.

17:36.040 --> 17:44.720
And next there is section on actually nine sections on symmetry contemplations.

17:44.720 --> 17:50.760
So all together there are 14 kinds of contemplation of the body.

17:50.760 --> 18:01.720
And so regarding the first section which is a section on breathing, the body means breathing.

18:01.720 --> 18:09.760
In the second section where the postures of the body are mentioned, then the postures of

18:09.760 --> 18:13.400
the body are called body and so on.

18:13.400 --> 18:21.080
So the body may mean sometimes the whole body, sometimes just the breathing in and breathing

18:21.080 --> 18:29.280
out or stepping forward, going back and so on.

18:29.280 --> 18:37.440
So when you are practicing mindfulness of breathing in and out and you are doing the

18:37.440 --> 18:44.800
contemplation on the body and when you are mindful of the postures, sitting, standing,

18:44.800 --> 18:51.040
walking and lying down, you are practicing contemplation on the body.

18:51.040 --> 19:02.040
And when you are mindful of going forward, going back, stretching, bending, eating and

19:02.040 --> 19:06.960
so on, you are practicing contemplation in the body.

19:06.960 --> 19:17.880
So there are all together 14 kinds of contemplation of the body taught in this discourse.

19:17.880 --> 19:28.320
Now the last nine contemplations of the body are called symmetry contemplations.

19:28.320 --> 19:36.880
Actually that is the contemplation on the different conditions of the dead body.

19:36.880 --> 19:47.560
So first it becomes swollen or festered and then there are other conditions of the dead

19:47.560 --> 19:54.840
body until it is reduced to bones and powder.

19:54.840 --> 20:04.880
So the symmetry contemplations mean contemplation on the conditions of the dead body, say

20:04.880 --> 20:10.760
laughter after the person is dead.

20:10.760 --> 20:18.760
Now when you are mindful of the rising and falling movements of the abdomen, you are doing

20:18.760 --> 20:30.080
the contemplation on the body that is mindfulness of the full mindfulness of the air element.

20:30.080 --> 20:36.120
Now you all know that there are four material elements, the element of earth, the element

20:36.120 --> 20:43.120
of water, the element of fire and the element of air.

20:43.120 --> 20:52.000
Among these four, the rising and falling movements belong to the air element.

20:52.000 --> 21:00.560
Because they are actually caused by the breathing in and out and so they are called air

21:00.560 --> 21:14.880
element and air element has the characteristic of extending and it has a function of moving.

21:14.880 --> 21:25.960
So when you concentrate not on the abdomen actually but on the movements then you are concentrating

21:25.960 --> 21:32.600
on the air element, on the function of air element.

21:32.600 --> 21:42.160
So when you practice mindfulness of the rising and falling of the movement, you are actually

21:42.160 --> 22:00.920
practicing the mindfulness of the full element here, mindfulness of the air element.

22:00.920 --> 22:18.360
Now the second contemplation, contemplation of feelings is a mental state.

22:18.360 --> 22:27.120
Whenever we experience an object, whenever we see an object or we hear an object, we smell

22:27.120 --> 22:30.560
an object and so on.

22:30.560 --> 22:37.160
There is the mental state feeling arising along with the consciousness.

22:37.160 --> 22:47.440
So the mental state feeling has the characteristic of experiencing or enjoying the taste

22:47.440 --> 22:50.920
of the object.

22:50.920 --> 22:59.120
So please note that feeling here is a mental state.

22:59.120 --> 23:07.040
But whenever we talk about feeling, our mind is always good to the physical thing.

23:07.040 --> 23:17.560
Say when you say pain, then we always go to the material thing that is called pain.

23:17.560 --> 23:25.480
And when we are mindful of pain, actually we are mindful of the feeling of that pain.

23:25.480 --> 23:29.960
Pain is not mental state, pain is a physical state.

23:29.960 --> 23:36.880
But this contemplation on the feeling is called mental contemplation.

23:36.880 --> 23:47.240
So contemplation of feeling is the contemplation of a mental state which is called feeling.

23:47.240 --> 23:53.920
And that arises in our mind, not in the physical body.

23:53.920 --> 24:07.520
But it is so associated with, so connected with the sensations in the body that in practice,

24:07.520 --> 24:14.720
we just take these two as one.

24:14.720 --> 24:20.840
So when we say, when you have pain, concentrate on the pain and be mindful of it, and

24:20.840 --> 24:29.840
that means concentrate on the experience in your mind of that sensation and not that sensation

24:29.840 --> 24:31.480
actually.

24:31.480 --> 24:38.280
So the sensation I mean something that is in the body.

24:38.280 --> 24:49.760
Now when the material properties in the body are in good state doing fine, then we don't

24:49.760 --> 24:51.760
feel any pain.

24:51.760 --> 24:59.720
But when we feel pain, that means the material properties in the body have become deteriorated

24:59.720 --> 25:01.400
or they have changed.

25:01.400 --> 25:06.680
So that is what we call pain or sensation.

25:06.680 --> 25:15.440
And the experience of that pain in our mind, experience in our mind of that pain is what

25:15.440 --> 25:17.760
we call feeling.

25:17.760 --> 25:26.480
So contemplation on feeling is a mental contemplation or contemplation of a mental object

25:26.480 --> 25:34.080
and not of the physical object.

25:34.080 --> 25:44.200
Basically there are three kinds of feelings, pleasant and pleasant and neutral.

25:44.200 --> 25:51.680
And each of these three can be worldly and unworthy.

25:51.680 --> 25:58.560
So worldly feeling means feelings, you experience when you are doing worldly things, when

25:58.560 --> 26:03.040
you are enjoying good things and so on.

26:03.040 --> 26:09.440
And worldly feeling means the feelings that you experience when you are doing meritorious

26:09.440 --> 26:14.240
tea, when you are practicing meditation and so on.

26:14.240 --> 26:24.480
So with the three basic feelings, we say that there are nine kinds of feeling and there

26:24.480 --> 26:35.760
are nine kinds of feeling taught in the discourse on the foundations of mindfulness.

26:35.760 --> 26:43.320
And whatever feeling there is, we are to be mindful of that feeling.

26:43.320 --> 26:50.760
That is why when there is a feeling of pain, we try to be mindful, saying pain, pain,

26:50.760 --> 26:59.240
pain, when there is stiffness and we try to be mindful of that stiffness, saying stiffness,

26:59.240 --> 27:01.280
stiffness, stiffness and so on.

27:01.280 --> 27:06.960
Actually whatever feeling there is, whether it is a pleasant feeling or unpleasant feeling

27:06.960 --> 27:11.520
or a neutral feeling, we are to be mindful of that feeling.

27:11.520 --> 27:18.240
Because if we do not practice mindfulness on feeling, we may have wrong notions of the feeling,

27:18.240 --> 27:25.920
that feeling lies for some time, that feeling is good and so on.

27:25.920 --> 27:38.480
So in order to see the true nature of feeling, we have to be mindful of the feeling.

27:38.480 --> 27:44.000
And contemplation on feeling is not to get rid of these feelings.

27:44.000 --> 27:51.920
Now when there is pain, you are instructed to be mindful of that pain or to make mental

27:51.920 --> 27:55.520
notes as pain, pain and so on.

27:55.520 --> 28:01.800
And many yogis think that it is to get rid of that pain that you have to be mindful

28:01.800 --> 28:05.000
of it, saying pain, pain, pain.

28:05.000 --> 28:08.560
But what the Buddha taught is different.

28:08.560 --> 28:19.920
So Buddha said, for the full understanding of these three kinds of feelings, you practice

28:19.920 --> 28:22.880
the foundations of mindfulness.

28:22.880 --> 28:28.760
So when there is feeling and you try to be mindful of that feeling, that means you are

28:28.760 --> 28:34.560
trying to see the true nature of that feeling.

28:34.560 --> 28:42.240
You are trying to see what feeling is and you are trying to see that that feeling arises

28:42.240 --> 28:47.880
and disappears and so that feeling is implemented and so on.

28:47.880 --> 28:58.920
So it is in order to understand fully in this way that you have to be mindful of feelings,

28:58.920 --> 29:10.160
not to get rid of pain or some unpleasant sensations.

29:10.160 --> 29:16.840
So when you practice feeling contemplation, you make effort and you try to be mindful

29:16.840 --> 29:27.280
and you get concentration and you see the feelings clearly or you comprehend the feeling

29:27.280 --> 29:28.600
clearly.

29:28.600 --> 29:35.960
So when you are mindful of the feeling, when you are clearly comprehending the feeling, you

29:35.960 --> 29:47.320
are actually removing mental defilements regarding that feeling.

29:47.320 --> 29:53.880
When the feeling is good, coverlessness or craving can arise.

29:53.880 --> 29:58.760
So when you have a good feeling, you want it to last for a long time and you are attached

29:58.760 --> 30:00.000
to it.

30:00.000 --> 30:09.080
And when the feeling is painful and you have what is called grief, you have ill will

30:09.080 --> 30:17.960
or you have anger, that is if you do not practice mindfulness.

30:17.960 --> 30:26.800
So when you practice mindfulness, then they will not get chance to arise in your mind.

30:26.800 --> 30:38.920
Now even when the feeling is bad or unpleasant, craving can arise.

30:38.920 --> 30:48.200
Not with regard to that unpleasant feeling, but when there is an unpleasant feeling,

30:48.200 --> 30:55.400
you long for a pleasant feeling, you want it to go away and you want to have the pleasant

30:55.400 --> 30:57.440
feeling arise in you.

30:57.440 --> 31:05.000
So even when the feeling is unpleasant, the attachment or craving can arise.

31:05.000 --> 31:17.080
That is if you do not practice mindfulness.

31:17.080 --> 31:22.520
Now we come to the third foundation of mindfulness.

31:22.520 --> 31:30.920
Now you are practicing meditation and you try to be mindful of the breath or the movements

31:30.920 --> 31:34.120
of the abdomen.

31:34.120 --> 31:42.440
Then after some time, you are mind wanders and you are aware of it and so you make notes

31:42.440 --> 31:46.760
as wandering, wandering, wandering.

31:46.760 --> 31:54.840
Now, you are doing contemplation of consciousness when you are noting your mind like that,

31:54.840 --> 31:59.000
wandering, wandering, wandering.

31:59.000 --> 32:13.800
Now, in order to understand consciousness as thoughts in Buddhism, you need to know abidhamma.

32:13.800 --> 32:26.560
According to abidhamma, there are two things that are called namma or mind.

32:26.560 --> 32:39.840
And abidhamma is thought to be a combination of consciousness and mental factors.

32:39.840 --> 32:49.440
And it is defined as the awareness of the object.

32:49.440 --> 33:03.000
It is a very simple awareness of the object and not the awareness we use in our talk on

33:03.000 --> 33:10.920
Vipasana meditation, so when we talk about Vipasana meditation, we say we try to be aware

33:10.920 --> 33:16.040
of the object and that awareness is actually a mindfulness.

33:16.040 --> 33:23.040
But here, consciousness, when we say consciousness is the awareness of the object, it is

33:23.040 --> 33:32.840
just a mere awareness of the object, not actually at that moment, not actually knowing

33:32.840 --> 33:41.240
whether it is, it is white or black or whatever, but just the awareness of the object.

33:41.240 --> 33:50.960
It is a very simple awareness of the object and that is called consciousness or chita

33:50.960 --> 33:57.320
in pali and abidhamma.

33:57.320 --> 34:08.800
And this awareness of the object or consciousness is always accompanied by mental factors.

34:08.800 --> 34:17.480
Now, there are many mental factors, actually there are 52 that accompany different types

34:17.480 --> 34:20.040
of consciousness.

34:20.040 --> 34:31.080
Now, attachment is a mental factor, hate or anger is a mental factor, delusion or ignorance

34:31.080 --> 34:40.160
is a mental factor, jealousy is a mental factor.

34:40.160 --> 34:48.280
And on the good side, the confidence is a mental factor, mindfulness is mental factor,

34:48.280 --> 35:00.640
understanding is mental factor, mental factor, so there are many mental factors.

35:00.640 --> 35:07.600
Some are good mental factors, some are bad mental factors and some are common to both

35:07.600 --> 35:09.680
good and bad.

35:09.680 --> 35:18.280
So the consciousness is always accompanied by some of these mental factors.

35:18.280 --> 35:27.200
And when the consciousness is accompanied by good mental factors, then we call that consciousness,

35:27.200 --> 35:31.080
good consciousness or wholesome consciousness.

35:31.080 --> 35:37.840
When the consciousness is accompanied by bad mental factors and it is called unwholesome

35:37.840 --> 35:42.640
consciousness and so on.

35:42.640 --> 35:50.240
Although consciousness and mental factors arise at the same time, consciousness is said

35:50.240 --> 35:54.800
to be their leader.

35:54.800 --> 36:03.080
Consciousness is said to be the full run of these mental factors because when there is

36:03.080 --> 36:10.280
no consciousness there can be no mental factors, no mental factors can arise when there

36:10.280 --> 36:13.200
is no consciousness.

36:13.200 --> 36:21.160
So consciousness is said to be their full run or leader.

36:21.160 --> 36:32.000
This consciousness is always with us, so long as we are living, we are never without consciousness.

36:32.000 --> 36:42.920
Then when we are first asleep in a deep sleep there is still consciousness of some kind.

36:42.920 --> 37:00.760
So we can never be without consciousness as long as we are alive.

37:00.760 --> 37:09.560
So awareness of the consciousness of the consciousness is called contemplating contemplation

37:09.560 --> 37:22.520
on the consciousness or the third foundation of mindfulness.

37:22.520 --> 37:33.560
So when a yogi makes effort and tries to be mindful of the consciousness as his mindfulness

37:33.560 --> 37:43.160
becomes stronger and as he gets concentration he begins to see the consciousness clearly

37:43.160 --> 37:46.080
and its characteristics and so on.

37:46.080 --> 37:56.160
So when he is seeing the consciousness, the true nature of consciousness then he is said

37:56.160 --> 38:02.960
to be removing mental defilements regarding that consciousness.

38:02.960 --> 38:10.440
That means regarding that consciousness he will not have attachment to the consciousness

38:10.440 --> 38:15.640
or anger to the consciousness.

38:15.640 --> 38:31.880
Then fourth foundation of mindfulness is contemplation on the damas, on the damas objects.

38:31.880 --> 38:46.040
Now when we explain the discourses that are originally in Pali and when we explain them

38:46.040 --> 38:56.200
in English sometimes we have problems with the translation.

38:56.200 --> 39:02.760
Some words actually cannot be translated.

39:02.760 --> 39:06.160
One of such words is the word dhamma.

39:06.160 --> 39:09.000
The word dhamma has many meanings.

39:09.000 --> 39:13.000
It means differently in different places.

39:13.000 --> 39:17.840
So we cannot give just one meaning of the word dhamma.

39:17.840 --> 39:33.760
Now when you say dhamma saranan kachami I go to dhamma for refuge then dhamma means the

39:33.760 --> 39:42.360
Buddha realized when he became the Buddha and also his teachings.

39:42.360 --> 39:45.840
Sometimes dhamma means nature.

39:45.840 --> 39:48.760
Sometimes it means learning and so on.

39:48.760 --> 39:57.440
So there are many meanings of the word dhamma and so we cannot have just one English translation

39:57.440 --> 40:02.040
for the word dhamma in all places.

40:02.040 --> 40:10.520
And here also the word dhamma cannot be translated into English.

40:10.520 --> 40:21.840
Many translators translate it as mind object or mental objects but I think that is not

40:21.840 --> 40:26.120
accurate.

40:26.120 --> 40:34.680
Mind object because there are many things that are called dhamma and in this discourses

40:34.680 --> 40:42.880
on the four foundations of mindfulness if you go to the section on the contemplation

40:42.880 --> 40:49.600
of the dhamma you will find that the five hindrances are called dhamma, five aggregates

40:49.600 --> 40:58.040
of clinging are called dhamma and the six external bases and six internal bases are called

40:58.040 --> 41:03.600
dhamma, seven factors of enlightenment are called dhamma and the four noble troops are called

41:03.600 --> 41:04.720
dhamma.

41:04.720 --> 41:12.360
So to cover all these how can we translate the word dhamma?

41:12.360 --> 41:22.200
Some of them are mental and some of them are material so we cannot call them mental objects

41:22.200 --> 41:25.480
or mind objects.

41:25.480 --> 41:37.120
So it is better to leave it as it is and explain when we use the word dhamma in a certain

41:37.120 --> 41:38.360
context.

41:38.360 --> 41:44.160
So here also we cannot translate this word into any language.

41:44.160 --> 41:50.080
So we say contemplation on the dhamma in the dhamma objects.

41:50.080 --> 41:57.040
So if we want to understand what dhamma means here you go to the section on the contemplation

41:57.040 --> 42:02.480
of the dhamma and there we find those mental hindrances and so on.

42:02.480 --> 42:08.440
So in this discourses, mental hindrances and others there are all the five groups.

42:08.440 --> 42:22.680
They are called dhamma like the word dhamma.

42:22.680 --> 42:27.440
The word cheetah which is translated here as consciousness is another difficult word to translate.

42:27.440 --> 42:35.480
But here many others translate the word cheetah as consciousness.

42:35.480 --> 42:48.280
Now here also for one of a better word we have to use the word consciousness for cheetah.

42:48.280 --> 42:51.880
But again it is not accurate.

42:51.880 --> 42:59.200
Now as you now know we are with consciousness always even when we are asleep we are with

42:59.200 --> 43:02.120
consciousness according to dhamma.

43:02.120 --> 43:08.340
But people would say when you are in deep sleep you are unconscious or when you are

43:08.340 --> 43:10.800
fine that you are unconscious.

43:10.800 --> 43:20.160
So this word also is not actually accurate for the word cheetah in pali but we have to use

43:20.160 --> 43:26.200
some English word for this word and so the word consciousness is used.

43:26.200 --> 43:34.120
So when we use the word consciousness it is important that we define it.

43:34.120 --> 43:38.840
We tell people what we mean by the word consciousness.

43:38.840 --> 43:46.560
Just using the word consciousness will not be enough because people may misunderstand.

43:46.560 --> 43:57.920
So here in a bithamma the English word consciousness is used for the word cheetah because

43:57.920 --> 44:11.200
there is no other better word for the word cheetah.

44:11.200 --> 44:24.200
Now the objects represented by the word dhamma are many and the objects that are called

44:24.200 --> 44:35.120
dhamma in this discourse overlap with the other objects of body contemplation, feeling

44:35.120 --> 44:39.360
contemplation and consciousness contemplation.

44:39.360 --> 44:50.320
So suppose you are practicing meditation and somebody make a noise and you are angry.

44:50.320 --> 45:03.720
So how would you note angry or anger?

45:03.720 --> 45:09.880
If you are making notes angry angry I think you are doing the third foundation of mindfulness

45:09.880 --> 45:19.520
because I am angry means my mind is accompanied by anger so it is very delicate.

45:19.520 --> 45:28.320
But when you are contemplating making notes as anger, anger you are doing the dhamma object

45:28.320 --> 45:32.320
contemplation or the fourth foundation of mindfulness.

45:32.320 --> 45:38.160
Because now you are taking anger which is one of the mental hindrances as the object

45:38.160 --> 45:42.080
of your attention.

45:42.080 --> 45:50.720
Although it is not important to try out I mean to find out what contemplation you are doing

45:50.720 --> 45:53.600
at the moment.

45:53.600 --> 46:03.840
It is still good to know say what contemplation you are doing but please do not try to find

46:03.840 --> 46:07.480
out what contemplation you are doing at the moment.

46:07.480 --> 46:17.960
It will take you away from the real practice of mindfulness and drag you into thinking.

46:17.960 --> 46:27.760
So sometimes it is difficult to see what contemplation we are doing, contemplation of consciousness

46:27.760 --> 46:33.000
or contemplation of the dhamma objects.

46:33.000 --> 46:43.720
But whatever object it is so long as you are mindful then you are doing the practice

46:43.720 --> 46:46.240
of mindfulness meditation.

46:46.240 --> 46:52.240
So you do not have to find out what contemplation you are doing at the moment.

46:52.240 --> 47:09.680
Later on you may think about it later on means after the practice of meditation.

47:09.680 --> 47:20.840
Now this is the four foundations of mindfulness in brief so if you want to know more details

47:20.840 --> 47:28.600
I must refer you to the discourse on the foundations of mindfulness.

47:28.600 --> 47:41.880
But what we learn from this passage is that we must make effort, we must practice mindfulness

47:41.880 --> 47:48.520
so that we clearly comprehend or we clearly see the objects.

47:48.520 --> 47:56.840
That means we see the true nature of the objects.

47:56.840 --> 48:08.160
Only when we clearly comprehend the objects, only when we see the objects as impermanence,

48:08.160 --> 48:17.560
suffering and non-soul, will we be able to prevent covetousness and grief from arising

48:17.560 --> 48:20.600
in our minds.

48:20.600 --> 48:31.680
So in order to prevent them from arising in our minds we need to comprehend the objects

48:31.680 --> 48:40.240
clearly and in order to see the objects clearly we need to practice mindfulness and for

48:40.240 --> 48:44.800
the practice of mindfulness we must make an effort.

48:44.800 --> 48:53.600
So if we make effort and if we practice mindfulness we will not fail to comprehend the

48:53.600 --> 48:55.320
objects clearly.

48:55.320 --> 49:02.400
So when we comprehend the objects clearly then we will be removing covetousness and grief

49:02.400 --> 49:07.680
at the same time.

49:07.680 --> 49:23.880
So let us all make effort to be mindful, so as to comprehend the objects clearly and remove

49:23.880 --> 49:32.800
covetousness and grief in the world.

49:32.800 --> 49:40.560
The

