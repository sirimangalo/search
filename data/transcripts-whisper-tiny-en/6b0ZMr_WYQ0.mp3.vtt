WEBVTT

00:00.000 --> 00:07.000
First sex now murder. So now we have a question about killing people. What would you

00:07.000 --> 00:15.000
rather do kill one person to save a thousand or let one live and one thousand must die?

00:15.000 --> 00:20.280
One of these what if questions may be in the books full of these that you're supposed to

00:20.280 --> 00:24.760
torture yourself or it's really silly. I mean there's no I'm not causing a thousand people

00:24.760 --> 00:31.880
to die if I let one live. Yeah I could go around hunting out hunting down hunting out serial

00:31.880 --> 00:39.520
killers and shooting them and so on. I mean it's such a long long and complicated answer

00:39.520 --> 00:44.880
but this is the argument that we had over buying meat and so on. The point is you're

00:44.880 --> 00:52.080
only responsible for the actions that you do. An action is only unwholesome. It's only

00:52.080 --> 00:59.600
bad because of the nature of the mind of the person who performs it. That's it. There's

00:59.600 --> 01:06.080
no God telling us this is wrong. There's no society telling us that just because God tells

01:06.080 --> 01:10.040
you it's wrong is because society tells you it's wrong. It doesn't make anything wrong.

01:10.040 --> 01:18.480
Socrates pointed himself. Socrates said or Plato however but Socrates said that this man was

01:18.480 --> 01:23.920
going to take his parents to to court because he thought it's what the gods wanted and

01:23.920 --> 01:28.360
so Socrates asked him well so is it right to take your parents to court because the gods

01:28.360 --> 01:35.120
say it say it's right or do the gods say it's right because it's right and and either

01:35.120 --> 01:42.520
way you you can't evoke the gods. There is no entity that could tell you that something

01:42.520 --> 01:49.200
is right or wrong. The murder could be perfectly right. There's nothing intrinsic about

01:49.200 --> 01:56.120
the the act of killing someone that is in any way wrong. If one person kills another

01:56.120 --> 02:01.720
it's meaningless to you. If this person kills that person it's totally unrelated to your

02:01.720 --> 02:09.040
state of mind. You can just watch it and say seeing seeing. So there's no relationship

02:09.040 --> 02:12.440
and this is a scientific. This isn't Buddhist theory. I mean if you're a lot if you're

02:12.440 --> 02:17.080
thinking about this logically and removing your emotions from you thinking of it scientifically

02:17.080 --> 02:21.000
this is why science scientists have a real problem with ethics and why they cut up rats

02:21.000 --> 02:30.640
and poison rats and and wind up horrible people because they are unable to develop a clear

02:30.640 --> 02:34.440
code of ethics simply because from a scientific point of view there's nothing wrong

02:34.440 --> 02:39.400
with killing killing is the end of life. What does it mean? It means nothing. They're

02:39.400 --> 02:48.360
only understanding of ethics is a religious concept that has no basis in reality. So Buddhism

02:48.360 --> 02:55.600
just points one thing out that that not only does the the physical exist and go by laws

02:55.600 --> 03:03.920
of physics but the mind also exists and it goes by mind by laws of how the mind works.

03:03.920 --> 03:07.560
At the moment when you kill something you create kill someone you're creating a problem

03:07.560 --> 03:11.120
in your mind. When you let someone kill someone else you're not creating a problem in

03:11.120 --> 03:23.240
your mind. This is the only ethical principle that exists in teravada Buddhism anyway that

03:23.240 --> 03:28.360
the actions that you perform will affect your mind. The choices that you make. So if

03:28.360 --> 03:32.960
you're watching someone kill someone else or kill a thousand people creates anguish

03:32.960 --> 03:38.520
in your mind then you're responsible for the creating of anguish. If you could save those

03:38.520 --> 03:45.120
people and you choose to not save them. If all you had to do is sell them run then

03:45.120 --> 03:50.840
guys got a gun or something like that or someone's coming with a gun go ahead and you

03:50.840 --> 03:55.480
don't do that then you're responsible for that. You're not ever responsible for the killing

03:55.480 --> 04:00.880
unless you're the one pulling the trigger or unless you're the one telling a person to kill.

04:00.880 --> 04:05.040
If you say this person kill that man. I mean you're still not responsible for pulling

04:05.040 --> 04:09.080
the trigger. You're responsible for telling that person to do something. You're responsible

04:09.080 --> 04:15.400
for the ethical qualities of mind, the greed, the anger and the delusion inherent in

04:15.400 --> 04:23.520
the mind, inherent in the mind at that moment. So if you don't kill the person who's

04:23.520 --> 04:27.720
going to kill a thousand people then it may be that unwholesome miserises in your mind

04:27.720 --> 04:35.360
as a result. You think that those people die good, nothing to do, not my problem and so

04:35.360 --> 04:39.160
as a result you create some kind of worry and anguish afterwards. I couldn't save them

04:39.160 --> 04:46.680
all I had to do was kill that one man but on the other hand if you kill that one person

04:46.680 --> 04:51.120
then you have to say what is different. What is worse? This guilt that I have over

04:51.120 --> 04:58.640
that those thousand deaths which weren't my, weren't the result of my actions and so the

04:58.640 --> 05:03.480
only reason it comes up is because there's some intellectualizing or some minor feelings

05:03.480 --> 05:09.000
of guilt or the feeling of pulling a trigger. I think the real reason why people have a problem

05:09.000 --> 05:15.960
with this and a problem with understanding things like hunting in general is because

05:15.960 --> 05:24.040
they've never done it themselves. If you've never killed a significantly large being like

05:24.040 --> 05:30.920
my example is a deer because that's how far I went or even more a human being then you

05:30.920 --> 05:36.360
have no real right to argue this question and I don't have the right to argue but if

05:36.360 --> 05:41.880
you've read the books, if you've read accounts of people who have killed and how it

05:41.880 --> 05:48.040
affects them, any of these books the book I was referring to earlier was crime and punishment.

05:48.040 --> 05:54.000
It's just an excellent, excellent description of the torture that goes through the mind

05:54.000 --> 05:59.000
of someone who kills another human being that we can't even fathom most of us. We think

05:59.000 --> 06:03.480
well it's just intellectualizing so you kill someone what's the problem. Totally different

06:03.480 --> 06:08.720
when you actually try to do it. It's an incredibly powerful act. The only comparison I have

06:08.720 --> 06:14.160
is as I said with hunting with killing a deer, I had no compunction, no problem with killing

06:14.160 --> 06:20.560
and I killed insects and small animals and no problem with hunting. I got my hunting license

06:20.560 --> 06:28.880
trained and so on and then I sat up in the tree and when the deer came raising this crossbow

06:28.880 --> 06:34.440
ready to pull the trigger and suddenly my whole body started to shake. The deer was no threat

06:34.440 --> 06:39.520
to me. There was no fear. There should have been no fear in my mind. It wasn't like there

06:39.520 --> 06:46.920
was some danger to me but I just got this incredible dread and revulsion to it and my whole

06:46.920 --> 06:53.240
body started to shake and I could barely pull the trigger. That's how powerful it is.

06:53.240 --> 07:00.360
This wasn't totally not intellectual because I had no feelings of hesitation. I was perfectly

07:00.360 --> 07:04.920
ready to do this and yet when it came time to do that because I had never done it because

07:04.920 --> 07:11.800
my mind was too pure from that. Pure of that. It had an incredible effect on my mind and

07:11.800 --> 07:15.560
this is what you're looking at if you kill someone. If you're just talking about the guilt

07:15.560 --> 07:21.960
feelings of not having prevented other people's death when actually in the end all those people

07:21.960 --> 07:26.680
are going to have to die anyway. Whether it be from a bullet wound or whether it be from old age,

07:26.680 --> 07:34.680
everyone dies and in horrible ways you can't possibly save even a small portion of the people who

07:34.680 --> 07:42.600
have to die in horrible ways. If it's just a matter of feeling that kind of guilt from not having

07:42.600 --> 07:55.800
prevented the inevitable or put off the inevitable then it's far preferable to the horror of having

07:55.800 --> 08:06.760
killed a human being even an evil human being. There are other accounts of this. I can't remember

08:06.760 --> 08:13.960
where people, police officers who have had to kill humans and they can have the right. The one is

08:13.960 --> 08:20.840
post-traumatic stress disorder who will go to soldiers who go to war and will come back and

08:20.840 --> 08:26.200
just don't want to ever talk about it again. They've just locked off that part of their mind.

08:26.200 --> 08:31.560
It's just so horrific. If you've never been to war you can't imagine the horrors of war and people

08:31.560 --> 08:37.480
who go just have, you know, are not able to sleep at night because of what they've seen and what

08:37.480 --> 08:45.720
they've done. So, me and I was telling us about these caravans that they had to drive from point

08:45.720 --> 08:51.400
A to point B and they weren't allowed to stop for anything. A man, a woman, child standing in the

08:51.400 --> 08:57.560
middle of the road had to be run over basically. This kind of thing. Maybe that's going quite far

08:57.560 --> 09:14.760
or field, but the horror that is involved with these visceral acts or these real acts of body. There's an

09:14.760 --> 09:20.920
incredible power there. But the point is that there's no intellectualizing in Buddhism. Buddhism is

09:20.920 --> 09:26.600
experiential, experientially. It's based on experience. You can't come up with a theory of morality

09:26.600 --> 09:35.240
and call it Buddhism. Buddhism is you are responsible for your own actions and you can never escape

09:35.240 --> 09:42.600
that responsibility and you're responsible for nothing else. And that is scientific. It's

09:44.120 --> 09:51.000
verifiable. You can never verify what is the guilt of letting someone kill someone else.

09:51.000 --> 09:59.320
You can only verify what is the suffering involved in your own actions. So, if you choose to

09:59.320 --> 10:04.840
feel guilty about it, that's going to create suffering. If you choose to kill a person to not feel

10:04.840 --> 10:13.320
guilty, then that's going to cause, I would say as well, or even more intense feelings of horror and

10:13.320 --> 10:25.560
no nightmares, because now you're a murderer. Whether it was justified murder or not, it was murder.

