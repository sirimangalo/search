1
00:00:00,000 --> 00:00:04,400
knowledge of Samata Jana's question.

2
00:00:04,400 --> 00:00:08,800
Would it be helpful to experience the Jana's so that you can help a

3
00:00:08,800 --> 00:00:13,040
meditator to move beyond blocks?

4
00:00:13,040 --> 00:00:17,760
Answer, I think the underlying source of questions like this

5
00:00:17,760 --> 00:00:21,360
is that people really want to learn the Jana's.

6
00:00:21,360 --> 00:00:25,520
Just the thought of deep states of bliss and peace

7
00:00:25,520 --> 00:00:33,040
sound exciting, so there arises a sort of desire for such states.

8
00:00:33,040 --> 00:00:38,160
When you more clearly see the nature of ultimate reality, such

9
00:00:38,160 --> 00:00:44,320
experiences become less exciting, as they are subject to the characteristics

10
00:00:44,320 --> 00:00:48,800
of impermanence, suffering, and non-sal.

11
00:00:48,800 --> 00:00:54,880
What you really need is an understanding of how reality works,

12
00:00:54,880 --> 00:00:59,040
the Jana's are just another part of reality.

13
00:00:59,040 --> 00:01:04,160
Jana's are not anything mysterious or extraordinary,

14
00:01:04,160 --> 00:01:09,680
they are a state where the mind is secluded from the hindrances.

15
00:01:09,680 --> 00:01:16,240
They are just mind states, they can last for extended periods of time,

16
00:01:16,240 --> 00:01:21,760
you can cultivate them in various ways, and you can use them to develop

17
00:01:21,760 --> 00:01:28,800
any number of magical powers, like remembering past lives and so on.

18
00:01:28,800 --> 00:01:33,520
But the best way to understand them is to understand the reality

19
00:01:33,520 --> 00:01:37,280
that underlies all experiences.

20
00:01:37,280 --> 00:01:42,160
This is done through practicing the simple meditation of the four

21
00:01:42,160 --> 00:01:47,360
foundations of mindfulness, to understand the building blocks of

22
00:01:47,360 --> 00:01:51,920
experience because once you understand what makes up

23
00:01:51,920 --> 00:01:58,240
experience, how experience works in terms of cause and effect,

24
00:01:58,240 --> 00:02:02,800
then there is nothing mysterious or hard to understand

25
00:02:02,800 --> 00:02:07,840
or really at all exciting about the Jana's.

26
00:02:07,840 --> 00:02:13,520
So to directly answer your question, I do not think it would be at all

27
00:02:13,520 --> 00:02:18,720
helpful. Direct experience of how the mind works

28
00:02:18,720 --> 00:02:24,320
is far more useful than cultivating the samata Jana's in order to help someone

29
00:02:24,320 --> 00:02:29,520
who experienced them and was having a block with them.

30
00:02:29,520 --> 00:02:34,800
Attachment to peaceful and tranquil states of mind is common

31
00:02:34,800 --> 00:02:38,880
and that is quite difficult to train someone out of.

32
00:02:38,880 --> 00:02:44,320
It can be difficult to win a person who has practiced samata meditation

33
00:02:44,320 --> 00:02:48,800
intensively off of it, especially if they are coming

34
00:02:48,800 --> 00:02:53,360
from a different religious tradition. For example,

35
00:02:53,360 --> 00:02:57,600
if they have practiced meditation in the Hindu tradition,

36
00:02:57,600 --> 00:03:03,600
which has different goals and ideas, or even Buddhist meditation traditions

37
00:03:03,600 --> 00:03:08,400
that emphasize the practice of tranquility and the states of

38
00:03:08,400 --> 00:03:14,080
calm that it produces, which can be sometimes difficult to let go of

39
00:03:14,080 --> 00:03:19,440
winning someone off of their attachment to pleasant states of mind

40
00:03:19,440 --> 00:03:23,200
has nothing to do with knowledge of the Jana's.

41
00:03:23,200 --> 00:03:27,840
It has to do with knowledge of attachments and views,

42
00:03:27,840 --> 00:03:33,920
views about what is true happiness, views about what is self

43
00:03:33,920 --> 00:03:40,080
and views about control and letting go. It has to do with views about

44
00:03:40,080 --> 00:03:45,920
impermanence and as the Buddha said, the Jana's and any kind of

45
00:03:45,920 --> 00:03:51,040
pleasant or even neutral feeling, they bring is not permanent.

46
00:03:51,040 --> 00:03:55,440
Understanding these things does not require you to have a

47
00:03:55,440 --> 00:04:05,440
attain the Jana's yourself.

