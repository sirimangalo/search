I really appreciate your video uploads and that you are informing us about Buddhism.
I meditate regularly and I am an agnostic.
To what extent supports implies Buddhism, the concept of God.
I made a video several years ago about my understanding of God.
God is in the same category as the self in that they can't logically exist.
If you are talking about it intellectually, they can't possibly exist.
Because reality is based on existence.
That reality is only seeing, hearing, smelling, tasting, feeling, thinking that.
That's reality.
Even if God were to come and stand before you, it's impossible that you can be sure
that that's God.
You say because you're only seeing something.
If you hear God, it's impossible that you should know that you're hearing God.
Because it's just hearing.
If God strikes you over that with a lightning bolt or something or lifts you up into
the air and shows you the wonder of His creation, you still can't be sure that that's
God.
Because it's still only seeing, hearing, smelling, tasting, feeling and thinking.
In this sense, you can never know that God exists in the way that people say that they
know that God exists.
Because what they know is an extrapolation of experience.
They don't really know it.
And this is very important because this is how the mind works.
The mind will never react based on something that it doesn't know.
It only reacts based on what it knows, what it means.
The only way the mind can let go is to actually experience that something is unsatisfying,
is cause for stress and suffering.
The only way we want to become free from suffering is to see it as suffering.
So the only way this person can come to an end of some sorrow of craving and clinging
and striving is to see things as they are.
And this has nothing to do with God because seeing God is just seeing.
The soul is the same, self is the same, it has no place in experience.
It's just a concept, an idea that comes up based on our experience that we have some
volition that we can make choices, that at any given moment there is a decision that
is made.
So we therefore give rise to the idea that there is someone behind that volition, which
is important to understand that we're not saying that there is no volition, that there
is no decision.
It's just that reality doesn't admit of entities like God or Self or the doer in any sense.
It's just our understanding, the way we talk about reality, the way we conceive of reality
is so off track that we can't understand when we say that there is the doing, but there
is no doer.
You know, if there is doing, there must be the doer, for example.
Your question is about God, that's a little bit different, but same sort of answer that
entities don't exist in reality, all that exists is experience.
