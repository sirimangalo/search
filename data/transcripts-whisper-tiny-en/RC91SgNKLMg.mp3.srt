1
00:00:00,000 --> 00:00:04,120
I'm currently going through some emotional hard time.

2
00:00:04,120 --> 00:00:10,040
Sometimes I feel hopelessness or being upset for my situation throughout the day.

3
00:00:10,040 --> 00:00:15,680
So should I know it and put my whole thought into that experience until it goes away,

4
00:00:15,680 --> 00:00:20,360
or should I just shift my mind onto the rising and falling of my stomach?

5
00:00:20,360 --> 00:00:25,080
I feel that I still haven't let go of my loss, even though I'm using the meditation as a

6
00:00:25,080 --> 00:00:34,400
replacement. Where did I hear this recently?

7
00:00:34,400 --> 00:00:40,760
Someone said a very similar thing on the chat here. They asked me a question and they

8
00:00:40,760 --> 00:00:50,760
said that meditation was kind of a means for them to escape the problem.

9
00:00:50,760 --> 00:01:01,000
So I was trying to explain their practicing and they keep thinking about these thoughts

10
00:01:01,000 --> 00:01:11,040
keep coming back and the meditation is bringing them up or so on.

11
00:01:11,040 --> 00:01:15,760
They were saying, isn't it great that the meditation allows you to see these things and

12
00:01:15,760 --> 00:01:22,160
they say, well, no meditation, I guess it gives me some kind of relief sometimes.

13
00:01:22,160 --> 00:01:26,800
And kind of as you say, it's like a replacement or a means of shifting the attention away

14
00:01:26,800 --> 00:01:34,760
from the experience or a means of finding peace in hard times.

15
00:01:34,760 --> 00:01:39,720
But I said right away in directly in response that that's not what meditation is for

16
00:01:39,720 --> 00:01:46,000
meditation is for you to learn about your problems and to learn about what it is that

17
00:01:46,000 --> 00:01:51,200
you're experiencing. So it's not quite an answer to your question but something that

18
00:01:51,200 --> 00:01:58,600
I would question in your mind and be sure of yourself that you're not expecting something

19
00:01:58,600 --> 00:02:11,960
of meditation. And in general, that's really one of the big key goal, key points that

20
00:02:11,960 --> 00:02:16,320
we have to understand in terms of meditation is that we really shouldn't expect to get

21
00:02:16,320 --> 00:02:23,600
anything out of meditation, that we shouldn't expect to create anything out of meditation.

22
00:02:23,600 --> 00:02:31,120
We're trying to take what is already there and understand it and straighten it. The

23
00:02:31,120 --> 00:02:36,840
mind, the Buddha said straighten the mind. Well, that means straightened out our habits

24
00:02:36,840 --> 00:02:45,200
and our reactions and our ways of responding and reacting to things as opposed to interacting,

25
00:02:45,200 --> 00:02:52,040
as opposed to being with. That isn't an answer to your question but it's something that

26
00:02:52,040 --> 00:02:58,120
I would talk about. I would try to stress based on the fact that you're saying you're

27
00:02:58,120 --> 00:03:02,680
using meditation as a replacement. Meditation is for the purpose of coming to terms with

28
00:03:02,680 --> 00:03:12,840
your emotional, hard time, the emotional stress and difficulty. Maybe I'll address the

29
00:03:12,840 --> 00:03:22,720
question and then turn it over to the panel. Let me just briefly, I'll give some pointers

30
00:03:22,720 --> 00:03:26,800
here, because the question is quite direct. Should you note it all the time or should

31
00:03:26,800 --> 00:03:32,840
you put it aside and go back to the stomach? You can really do either. There are no

32
00:03:32,840 --> 00:03:41,280
hard and fast rules but you have to assess what is the result of doing one or the other.

33
00:03:41,280 --> 00:03:46,680
If you're able to be mindful of it and actually if you're able to be mindful of it, it

34
00:03:46,680 --> 00:03:51,600
should dissolve quite quickly. It may be that you're not able to be mindful of it and that

35
00:03:51,600 --> 00:04:00,400
after a while it makes you so stressed out that it actually brings on more stress in your

36
00:04:00,400 --> 00:04:06,480
mind and gets you to a point where you feel like you're going crazy. If at that point

37
00:04:06,480 --> 00:04:11,160
you decide you've been acknowledging it for some time, it's not going away, it's overwhelming

38
00:04:11,160 --> 00:04:16,040
and so on, you can come back to the stomach, start with the stomach again and something like

39
00:04:16,040 --> 00:04:21,560
starting over. What you don't want to do is avoid it. You don't want to, I'm not going

40
00:04:21,560 --> 00:04:26,880
to touch that, I'm not going to be mindful of it, it's just too difficult. But you, it's

41
00:04:26,880 --> 00:04:32,920
like getting a running start. Let's start back, back, back up and start here, rising, falling

42
00:04:32,920 --> 00:04:37,680
and then when it comes up again, slowly, slowly catch it piece by piece until you're

43
00:04:37,680 --> 00:04:44,880
able to face it head on. And there are other techniques as well, it doesn't mean you have

44
00:04:44,880 --> 00:04:48,760
to come back to the stomach. One of the greatest techniques when a person is overwhelmed by

45
00:04:48,760 --> 00:04:54,920
something is to do lying meditation because as I said, that's where you're the most relaxed.

46
00:04:54,920 --> 00:04:59,960
When you're stressed, when you're over stressed, you can try doing lying meditation. It's

47
00:04:59,960 --> 00:05:05,280
a way of retreating from the stress, finding something that's easier for you to deal with.

48
00:05:05,280 --> 00:05:09,320
Okay, this is easy. Still, maybe you have some of the emotions, but you'll find that

49
00:05:09,320 --> 00:05:19,080
they're easier to deal with. They're not as intense because you feel more peaceful. You

50
00:05:19,080 --> 00:05:25,360
feel better lying down, more relaxed, lying down. That's an example. You can stop meditating,

51
00:05:25,360 --> 00:05:31,160
take a break, come back and try again and so on. But there are no hard and fast rules in

52
00:05:31,160 --> 00:05:36,760
that sense, but I would like to have answers from other people, so maybe you have some thoughts

53
00:05:36,760 --> 00:06:01,120
on this. Yes, I do. I think you should be very open with the moment to see what is necessary

54
00:06:01,120 --> 00:06:10,840
sometimes when you note upset and hopelessness for a long time and it doesn't go away,

55
00:06:10,840 --> 00:06:19,640
it is not good to stay there because you eventually start to indulge in it without knowing

56
00:06:19,640 --> 00:06:28,000
it. You think you're just noting it, but you're kind of feeding it with it and you feel

57
00:06:28,000 --> 00:06:37,920
good in it and you have not noticed that yet. In such a case when it does not go away,

58
00:06:37,920 --> 00:06:44,360
it might be good to focus on something else because there is always something that can

59
00:06:44,360 --> 00:06:56,360
be prominent as well. When you go back to the rising and the falling, for example, in

60
00:06:56,360 --> 00:07:06,800
my experience, it brings you a good stability. When I'm, for example, lost in emotions,

61
00:07:06,800 --> 00:07:15,920
what of course happens and it's kind of whirling around and I can't really catch it

62
00:07:15,920 --> 00:07:26,400
or it's too much, then going back to the rising and falling of the stomach brings me, I called

63
00:07:26,400 --> 00:07:37,480
it the other day kind of in a position of in the eye of the hurricane, there it's peaceful

64
00:07:37,480 --> 00:07:47,560
and you are alert and you can from that position observe what is going on around without

65
00:07:47,560 --> 00:07:56,920
getting hurt by it or without getting carried away by it. So yeah, I think it's good

66
00:07:56,920 --> 00:08:03,560
from time to time to go back to the rising and falling then.

67
00:08:03,560 --> 00:08:09,240
Yeah, I think there's really no hard and fast rule and you roll with a punch as you consider

68
00:08:09,240 --> 00:08:14,680
you're in a boxing match. There's no answer whether should I punch or should I dodge? Sometimes

69
00:08:14,680 --> 00:08:18,680
you have to dodge, sometimes you have to punch, sometimes you have to even take a punch

70
00:08:18,680 --> 00:08:24,200
and sometimes you get knocked down and you get back up again.

71
00:08:24,200 --> 00:08:33,600
The song goes, but you shouldn't, I think you shouldn't have a hard and fast rule and that's

72
00:08:33,600 --> 00:08:38,640
what I would caution against because suppose someone takes the hard and fast rule that

73
00:08:38,640 --> 00:08:45,480
when these things come up, I'm going to go back to the stomach. If you take that as a rule,

74
00:08:45,480 --> 00:08:53,600
then it's easy to see how it might lead to developing a version towards it or a habit

75
00:08:53,600 --> 00:08:59,680
of avoiding the experience. If you force yourself, or you take it as a rule that you have

76
00:08:59,680 --> 00:09:05,720
to stay with it, then it can drive you crazy and it can lead you to not want to meditate,

77
00:09:05,720 --> 00:09:17,640
it can lead you to feel incapable of finding any benefit in the meditation, even entering

78
00:09:17,640 --> 00:09:22,160
into the Johnus and that sense is good. Mahas if I had said if you've practiced Johnus

79
00:09:22,160 --> 00:09:26,720
before, then it's great because you can go back and feel peaceful again and get your confidence

80
00:09:26,720 --> 00:09:35,680
back up and go out and fight. The other thing I would say is that actually staying with

81
00:09:35,680 --> 00:09:40,920
it and suppose it makes you really angry and upset because this is often what happens,

82
00:09:40,920 --> 00:09:44,560
we give the meditator an entire day to themselves and we're not there sitting with them

83
00:09:44,560 --> 00:09:49,640
telling them what to do so they do everything wrong. They'll go, they'll go, wait, this

84
00:09:49,640 --> 00:09:54,560
extreme way to that extreme and it's actually not that big of a deal as long as you have

85
00:09:54,560 --> 00:09:59,080
a teacher to pull you back on track because it teaches you something. So you can see the

86
00:09:59,080 --> 00:10:03,520
meditator said, oh, I was sitting with it and it was so it just made me more and more angry

87
00:10:03,520 --> 00:10:08,960
and then you can say, well, what's it like to be angry? Now you know what it's like

88
00:10:08,960 --> 00:10:15,720
to be angry for a whole day. So what we're trying to do is learn and the best thing you

89
00:10:15,720 --> 00:10:22,100
can learn from is your mistakes. It takes a long time but you learn from your defilements

90
00:10:22,100 --> 00:10:33,560
really. Sometimes you have to go through these things to really get over them to be

91
00:10:33,560 --> 00:10:41,080
so fed up with them, to know them again and again and then you thought, oh, I got over

92
00:10:41,080 --> 00:10:50,680
it and then it comes up again and you know it again. Then in one at one time at one point

93
00:10:50,680 --> 00:10:55,320
that comes the moment when you really get fed up with it, when you let go of it because

94
00:10:55,320 --> 00:11:02,680
you just saw it enough. But I think the key to all of our all of these answers is that

95
00:11:02,680 --> 00:11:09,720
you have to be dynamic. You should never have a hard and fast rule. It's so, I mean the

96
00:11:09,720 --> 00:11:14,840
path is how long is the path and you've got to know every trick in the book to get through

97
00:11:14,840 --> 00:11:27,320
it, right? Can we say that Samavayama, right effort, is a good measurement for that? Samalayama?

98
00:11:27,320 --> 00:11:34,200
Yeah. I believe the word they use is kusulupaya, do you know this one? Kusulupaya is

99
00:11:34,200 --> 00:11:40,040
knowing the right thing to do at the right time, is having all these tricks, knowing the right

100
00:11:40,040 --> 00:11:47,640
trick to you. Kusulupaya, kusulupaya, it's the word that they use in the Mayana to explain

101
00:11:48,360 --> 00:11:54,360
or in some Buddhists will use to explain doing of evil deeds for a good purpose.

102
00:11:56,120 --> 00:12:01,800
I've heard this argument, this is that argument about lying to save someone's life, for example,

103
00:12:01,800 --> 00:12:07,240
this is considered skillful means doing an evil deed for the purpose of goodness. But that's not

104
00:12:07,240 --> 00:12:15,880
how it's understood in the early text. Kusulupaya, kusulaya means wholesome or skillful,

105
00:12:15,880 --> 00:12:28,680
kupaya means a device or a means, I guess, a strategy, you could say.

106
00:12:28,680 --> 00:12:34,280
And it's this kind of thing where, what do you do when you're really stressed out?

107
00:12:34,280 --> 00:12:38,760
If you know the kusulupaya, it's to lie down, because lying down is actually something we

108
00:12:38,760 --> 00:12:44,200
tell the mediators not to do. It's not a good thing. But in certain instances, it's the right

109
00:12:44,200 --> 00:12:48,200
thing to do when you're really stressed out, do lying meditation. There was one monk,

110
00:12:49,400 --> 00:12:53,720
this crazy monk that I always refer back to, who slithers wrists, let lit himself on fire.

111
00:12:53,720 --> 00:13:03,080
At one point he came to our teacher and he's got a hand to Jan Thong. Nothing can face it.

112
00:13:03,080 --> 00:13:07,800
This monk came up to him and Jan Thong says, how's walking, how's sitting? He says,

113
00:13:07,800 --> 00:13:13,080
Jan, I can't walk, I can't do walking. He says, we'll find them just to sitting and he says,

114
00:13:13,080 --> 00:13:18,360
and I can't sit and find them just do lying meditation. I can't lie down when I'm, and he's explaining

115
00:13:18,360 --> 00:13:22,280
why he can't walk, why he can't sit, why he can't lie down. And the rest of us are like,

116
00:13:22,280 --> 00:13:26,440
what's he going to do that? And he says, then do standing meditation. And he goes,

117
00:13:28,200 --> 00:13:34,440
yeah, I can do standing. And he ended up doing standing meditation. He couldn't lie down,

118
00:13:34,440 --> 00:13:39,080
because he was going crazy, really. He was contorting himself into all these shapes.

119
00:13:39,080 --> 00:13:50,520
The things I had to deal with this guy was an interesting experience in psychology.

120
00:13:53,320 --> 00:14:02,840
May I come back to the Samavayama? Because it is, when I remember correctly,

121
00:14:02,840 --> 00:14:14,440
it is to raise the wholesome states that are there,

122
00:14:16,200 --> 00:14:23,800
and the wholesome states that haven't arisen. Bauana developed them, the ones that are already

123
00:14:23,800 --> 00:14:32,840
there, a new rakana, protect them. The evil states that are already there, Bauana, abandon them.

124
00:14:33,560 --> 00:14:39,480
The evil states that are not yet there. Maybe that's a new rakana. I can't remember

125
00:14:40,840 --> 00:14:47,960
a guard against them. But there's, so I clear what you're saying that from, so from time to time,

126
00:14:47,960 --> 00:14:55,400
you do one or the other is the idea. I thought having that in mind as a measurement for

127
00:14:57,240 --> 00:15:05,000
the meditation in general. But there is a better teaching that is more from time to time-ish,

128
00:15:05,000 --> 00:15:11,240
because the Samavayama, it's difficult, because it actually is supposed to all come together

129
00:15:11,240 --> 00:15:19,640
at the same time. It's all, being mindful really does all of that at once. It can be seen in that

130
00:15:19,640 --> 00:15:24,040
way, but there's one really good teaching that I think is given in several places. Probably,

131
00:15:24,040 --> 00:15:29,080
it's in the end good for any guy. I'd like to find it again. He said, the Buddha says,

132
00:15:29,880 --> 00:15:36,680
a skillful meditator or a skillful monk knows when it's appropriate to encourage the mind,

133
00:15:36,680 --> 00:15:43,640
when it's appropriate to discourage the mind, when it's appropriate to keep the mind

134
00:15:46,040 --> 00:15:53,560
and stay the mind, stop it, and when it's appropriate, see, there are these four

135
00:15:55,080 --> 00:16:03,400
different times. You really feel this as a teacher. When you're teaching people meditation,

136
00:16:03,400 --> 00:16:07,000
you have to be exactly this. Sometimes a meditator will come to you and you have to say,

137
00:16:07,000 --> 00:16:13,640
yes, good, keep going, encourage them, or more like, no, no, you're doing fine. That's really not

138
00:16:13,640 --> 00:16:19,400
a problem. You're learning more, see? You've learned more about yourself encouraging them. Sometimes

139
00:16:19,400 --> 00:16:23,480
you have to discourage them. I'll come to you and say, wow, I had a great meditation. I'll say,

140
00:16:23,480 --> 00:16:28,920
oh, yeah, you like it? Oh, I like it. I really like, I love it. Do you know what liking is?

141
00:16:28,920 --> 00:16:36,440
Liking, well, that's, that's greed. It's greed, a good thing. What is greed lead to? It leads to

142
00:16:36,440 --> 00:16:43,160
addiction. You don't like, no need to like things and so on. So pull them back. Sometimes you

143
00:16:43,160 --> 00:16:49,320
have to be, you know, keep them on course. It's kind of, it's playing this game, but this is how

144
00:16:49,320 --> 00:16:54,680
the Buddha taught us to, to teach us, to train ourselves. When the mind wants to leap out to something,

145
00:16:54,680 --> 00:16:59,800
you have to be able to pull it back. When the mind is lazy or so on, you have to have ways of

146
00:16:59,800 --> 00:17:04,600
encouraging it. For example, contemplation of death reminding yourself, we've only got a

147
00:17:05,800 --> 00:17:11,800
short time to live and so on. That's a really good, good teaching. I'm not, I'm not sure about

148
00:17:11,800 --> 00:17:18,040
some of why I'm, I mean, obviously it can be interpreted in that way as from time to time doing

149
00:17:18,040 --> 00:17:27,240
one or the other. But most important about some of IMS to see the, see our effort is comprehensive.

150
00:17:27,240 --> 00:17:31,960
And also to see effort is not being just push, push, push, right? You have to be able to,

151
00:17:31,960 --> 00:17:39,240
it's about balancing and it's really all about developing mindfulness to overcome the

152
00:17:39,240 --> 00:17:49,240
development, to have a wholesome state of mind.

