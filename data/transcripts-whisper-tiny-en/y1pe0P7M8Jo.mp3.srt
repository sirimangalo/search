1
00:00:00,000 --> 00:00:04,480
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,480 --> 00:00:11,480
Tonight we continue on with verse 122, which reads as follows.

3
00:00:35,480 --> 00:00:42,480
Which is almost exactly the same as the last verse,

4
00:00:42,480 --> 00:00:52,480
which if you remember was in regards to not thinking little evil deeds,

5
00:00:52,480 --> 00:00:54,480
thinking it won't come to me.

6
00:00:54,480 --> 00:00:56,480
So here it's the opposite.

7
00:00:56,480 --> 00:01:08,480
Ma wa mani eta, pune yasa, don't look down upon or don't underestimate.

8
00:01:08,480 --> 00:01:12,480
Pune ya, which is goodness.

9
00:01:12,480 --> 00:01:19,480
Thinking na manang agamesity, it won't come to me.

10
00:01:19,480 --> 00:01:33,480
For, uda bin dunipati na uda kumbo pepurati, a water pot becomes full drop by drop with drops of water,

11
00:01:33,480 --> 00:01:38,480
with drops of water falling.

12
00:01:38,480 --> 00:01:47,480
Pune yasa, a wise person becomes full of goodness.

13
00:01:47,480 --> 00:01:57,480
Tokan tokan be aginan.

14
00:01:57,480 --> 00:02:11,480
And even though it might be dropped by drop, one becomes full with goodness,

15
00:02:11,480 --> 00:02:14,480
or by through goodness.

16
00:02:14,480 --> 00:02:18,480
Full of goodness.

17
00:02:18,480 --> 00:02:26,480
Anyway, here we have the story.

18
00:02:26,480 --> 00:02:32,480
The story goes that the Buddha was talking about different types of giving.

19
00:02:32,480 --> 00:02:38,480
Again, it seems there are many stories of giving,

20
00:02:38,480 --> 00:02:47,480
giving being a fundamental religious practice that you might say gets up.

21
00:02:47,480 --> 00:02:55,480
Maybe some people might say gets a little too much coverage in Buddhist circles.

22
00:02:55,480 --> 00:03:03,480
I think it's fair to say that often it's because there's a concern for getting.

23
00:03:03,480 --> 00:03:07,480
People talk about giving because they're concerned with getting.

24
00:03:07,480 --> 00:03:17,480
Often, monasteries or meditation centers or monks would like to get things or are in need of things.

25
00:03:17,480 --> 00:03:22,480
Realistically, are in need of things.

26
00:03:22,480 --> 00:03:28,480
There's kind of attention there because you have to accept that while there is a need.

27
00:03:28,480 --> 00:03:33,480
It seems a little bit suspicious and I've often commented on this.

28
00:03:33,480 --> 00:03:51,480
A little bit, not suspicious, a little bit uncomfortable to talk about how good it is to give.

29
00:03:51,480 --> 00:03:59,480
When you actually mean it would be good if we could receive, for example.

30
00:03:59,480 --> 00:04:04,480
Anyway, giving is a spiritual practice.

31
00:04:04,480 --> 00:04:05,480
It's a good spiritual practice.

32
00:04:05,480 --> 00:04:07,480
There's nothing wrong with giving.

33
00:04:07,480 --> 00:04:16,480
We recently gave a large donation, our group, many of you who are watching this video perhaps,

34
00:04:16,480 --> 00:04:22,480
to a children's home in Florida.

35
00:04:22,480 --> 00:04:25,480
That was so good.

36
00:04:25,480 --> 00:04:27,480
Even my stepfather got involved.

37
00:04:27,480 --> 00:04:32,480
My mother got involved and everyone was so happy.

38
00:04:32,480 --> 00:04:36,480
My Sri Lankan friends in Florida got involved.

39
00:04:36,480 --> 00:04:41,480
It was a way to really bring people together and it has the potential to bring people together.

40
00:04:41,480 --> 00:04:46,480
Again, so we do this again next time.

41
00:04:46,480 --> 00:04:47,480
We'll even bring more people together.

42
00:04:47,480 --> 00:04:49,480
That's what this story in the Dhampa is about.

43
00:04:49,480 --> 00:04:56,480
The Buddha talks about how some people give themselves are charitable or kind,

44
00:04:56,480 --> 00:04:59,480
but don't encourage other people to do good deeds.

45
00:04:59,480 --> 00:05:01,480
This doesn't just have to do with giving.

46
00:05:01,480 --> 00:05:02,480
Any good deed.

47
00:05:02,480 --> 00:05:06,480
You could say the same about meditation or morality.

48
00:05:06,480 --> 00:05:11,480
If you're moral yourself, but you don't encourage people or that people,

49
00:05:11,480 --> 00:05:20,480
other people know about your views on ethical acts or ethical matters issues.

50
00:05:20,480 --> 00:05:27,480
And another person might encourage others, but not do good deeds themselves.

51
00:05:27,480 --> 00:05:31,480
And then one person does neither and another person does both.

52
00:05:31,480 --> 00:05:35,480
Both does good deeds and encourages others.

53
00:05:35,480 --> 00:05:41,480
And so his teaching is something that we've talked about before.

54
00:05:41,480 --> 00:05:46,480
Is that if you do good deeds, but don't encourage others to do good deeds,

55
00:05:46,480 --> 00:05:53,480
then you'll get great reward yourself.

56
00:05:53,480 --> 00:05:55,480
Your life will get better.

57
00:05:55,480 --> 00:05:57,480
But you won't be alone.

58
00:05:57,480 --> 00:05:59,480
You won't be surrounded by other people.

59
00:05:59,480 --> 00:06:01,480
If you become a good person yourself,

60
00:06:01,480 --> 00:06:09,480
but you haven't made friends with or work together with other people to do good deeds,

61
00:06:09,480 --> 00:06:13,480
then you'll be lacking in friendship and companionship,

62
00:06:13,480 --> 00:06:21,480
which is very important, both in the world and in spiritual practice.

63
00:06:21,480 --> 00:06:27,480
And so as the Buddha was teaching, it turns out there was a certain pandita police set,

64
00:06:27,480 --> 00:06:34,480
a wise man who, having heard this dhammadesana, thought to himself,

65
00:06:34,480 --> 00:06:40,480
well, then that's what I should do, rather than just be generous and kind

66
00:06:40,480 --> 00:06:43,480
and do good deeds on my own.

67
00:06:43,480 --> 00:06:46,480
I should bring people together to do good deeds.

68
00:06:46,480 --> 00:06:48,480
And this is what we did for Florida.

69
00:06:48,480 --> 00:06:50,480
This is why do we do these things?

70
00:06:50,480 --> 00:06:51,480
Bring people together.

71
00:06:51,480 --> 00:06:53,480
Don't just do good deeds yourself.

72
00:06:53,480 --> 00:06:56,480
Do them together as a group.

73
00:06:56,480 --> 00:07:09,480
So he went to the Buddha and he asked for maybe he went to the Buddha

74
00:07:09,480 --> 00:07:13,480
and said to him,

75
00:07:13,480 --> 00:07:25,480
yeah, I would like to invite monks for lunch in the Buddha said, how many monks?

76
00:07:25,480 --> 00:07:32,480
And the man said, all the monks want to invite them all for lunch.

77
00:07:32,480 --> 00:07:34,480
Now, how many monks were there?

78
00:07:34,480 --> 00:07:40,480
There were many, many monks.

79
00:07:40,480 --> 00:07:44,480
I don't know if it says here, how many monks, but it was a lot.

80
00:07:44,480 --> 00:07:46,480
It was the number of monks.

81
00:07:46,480 --> 00:07:51,480
This is in Jetawanda, so there are probably hundreds, if not more,

82
00:07:51,480 --> 00:07:53,480
thousands, even maybe.

83
00:07:53,480 --> 00:07:54,480
I don't know.

84
00:07:54,480 --> 00:07:57,480
Of course, they always offer exaggerate.

85
00:07:57,480 --> 00:08:04,480
But lots, lots of monks more than anyone poor family or even ordinary family

86
00:08:04,480 --> 00:08:07,480
would be able to care for.

87
00:08:07,480 --> 00:08:15,480
So he did this completely on faith that he could get people to join him

88
00:08:15,480 --> 00:08:17,480
in this great deed.

89
00:08:17,480 --> 00:08:25,480
And it was sort of a powerful determination on his part to be so bold.

90
00:08:25,480 --> 00:08:30,480
And so he went into the village, into the city, actually.

91
00:08:30,480 --> 00:08:42,480
And he went, maybe, banging a drum where he went to,

92
00:08:42,480 --> 00:08:51,480
went to his own village, maybe not the city, but he went around saying,

93
00:08:51,480 --> 00:08:54,480
everyone will be banging a drum or something.

94
00:08:54,480 --> 00:08:58,480
I have invited the congregation amongst, presided over by the Buddha

95
00:08:58,480 --> 00:09:00,480
for the meal tomorrow.

96
00:09:00,480 --> 00:09:05,480
Come give what you can provide as much as your means permits.

97
00:09:05,480 --> 00:09:10,480
Let us do all the cooking in one place and give alms in common.

98
00:09:10,480 --> 00:09:16,480
So rather than the ordinary way, which would be, oh, I'll invite a couple of monks to my house,

99
00:09:16,480 --> 00:09:20,480
or do this good deed or that good deed.

100
00:09:20,480 --> 00:09:25,480
It was, the idea was, let's all do one good deed together so that we share

101
00:09:25,480 --> 00:09:33,480
in the deed and that our karma is intertwined in the sense that we have similar futures together.

102
00:09:33,480 --> 00:09:41,480
We move forward and upward together.

103
00:09:41,480 --> 00:09:49,480
And so the people were, for the most part, overjoyed, but there was one rich guy who was not pleased.

104
00:09:49,480 --> 00:09:58,480
And hearing what this guy was saying, preaching, and talking about his intentions became quite angry

105
00:09:58,480 --> 00:10:01,480
with the thought thus.

106
00:10:01,480 --> 00:10:10,480
Rather than look at this guy, rather than invite people, invite monks based on his own means,

107
00:10:10,480 --> 00:10:24,480
how dare he assume that other people should help him fulfill his wishes, fulfill his intentions.

108
00:10:24,480 --> 00:10:35,480
I mean, how dare he just assume or push us or try to persuade us all to do something that really is on him.

109
00:10:35,480 --> 00:10:45,480
You know, since his burden, he's the idiot who bit off more than he can chew, and now he's coming and begging and pleading us.

110
00:10:45,480 --> 00:10:47,480
He was very upset about this.

111
00:10:47,480 --> 00:10:50,480
A rich man, this guy was rich as well.

112
00:10:50,480 --> 00:11:00,480
So when the guy came to his door, he took, it's funny, he took two fingers in his thumb, I guess.

113
00:11:00,480 --> 00:11:06,480
Or maybe three fingers in his thumb, maybe it was like this.

114
00:11:06,480 --> 00:11:12,480
And grabbed just as much rice would fit there and dropped it in the sky's bowl.

115
00:11:12,480 --> 00:11:22,480
And did the same with beans, and jaggery, or sugar, and salt, or whatever whatever he gave,

116
00:11:22,480 --> 00:11:28,480
he gave just a little bit when he was giving ingredients.

117
00:11:28,480 --> 00:11:37,480
So that's significant because he came to be known as the cat foot, rich man, or the cat foot guy.

118
00:11:37,480 --> 00:11:42,480
Because I guess I think because this looks like a cat foot.

119
00:11:42,480 --> 00:11:44,480
Cats have three paws, maybe.

120
00:11:44,480 --> 00:11:48,480
The three paws, and the thumb.

121
00:11:48,480 --> 00:11:51,480
Somehow that had a meaning at the time.

122
00:11:51,480 --> 00:12:01,480
His name, he became known as cat foot, or belalabada.

123
00:12:01,480 --> 00:12:13,480
And so the wise man, the guy who was organizing this, took the offerings, and he placed them apart.

124
00:12:13,480 --> 00:12:18,480
So he took the other, everyone else's offerings and just put them all together.

125
00:12:18,480 --> 00:12:29,480
But the rich man's offerings, he kept them separate and guarded them individually.

126
00:12:29,480 --> 00:12:33,480
And the rich man was sort of watching this guy.

127
00:12:33,480 --> 00:12:38,480
And as he saw this behavior that he hadn't mixed them in with everything else,

128
00:12:38,480 --> 00:12:41,480
that he had kept them separate.

129
00:12:41,480 --> 00:12:45,480
He asked one of his servants, he told him to service to go and spy on him and see what he was doing.

130
00:12:45,480 --> 00:12:51,480
What was up with all this?

131
00:12:51,480 --> 00:12:57,480
And the man followed after this guy, the organizer,

132
00:12:57,480 --> 00:13:02,480
and saw that what he was doing is he was taking like one grain or a few grains

133
00:13:02,480 --> 00:13:12,480
of the rich guy's rice and putting a little bit in each and every thing that they were cooking.

134
00:13:12,480 --> 00:13:24,480
So distributing it evenly among the product.

135
00:13:24,480 --> 00:13:29,480
And so this guy goes back and tells the rich man who's quite puzzled and doesn't know what's going on.

136
00:13:29,480 --> 00:13:34,480
He can't see what angle this guy is playing, but he starts to get suspicious.

137
00:13:34,480 --> 00:13:47,480
Because this guy deliberately paid special attention to the rich man's offering,

138
00:13:47,480 --> 00:13:52,480
which was ridiculously small.

139
00:13:52,480 --> 00:13:56,480
And so he starts to get this thought in his mind.

140
00:13:56,480 --> 00:14:05,480
He knows that guy knows that my offering was was miniscule, was meaningless, was an insult.

141
00:14:05,480 --> 00:14:07,480
And he's going to tell.

142
00:14:07,480 --> 00:14:11,480
He's going to tell the Buddha, where he's going to announce everyone.

143
00:14:11,480 --> 00:14:17,480
And he's going to announce that I gave such and such that I gave very, very little.

144
00:14:17,480 --> 00:14:21,480
So what he does, he takes a knife.

145
00:14:21,480 --> 00:14:26,480
He can cause it gets like a sword or a knife or something, and he sticks it in his robe.

146
00:14:26,480 --> 00:14:33,480
And he goes to the monastery the next morning, or no, he goes to the place where they're going to feed the monks.

147
00:14:33,480 --> 00:14:42,480
The next morning with the thought, if he dies, as soon as he opens his mouth, if he starts to talk about me, I'm going to kill him.

148
00:14:42,480 --> 00:14:50,480
That's what, that's what, how awful this guy really was.

149
00:14:50,480 --> 00:15:04,480
So it is quite odd that he, because later on he actually comes to realize the dhamma, but this is the story.

150
00:15:04,480 --> 00:15:09,480
It's hard to know what's going on underneath, but he gets very, very angry.

151
00:15:09,480 --> 00:15:13,480
And he must really think that this guy is out to get him.

152
00:15:13,480 --> 00:15:34,480
That this guy has set him up and is kind of disappointed or upset or feels self-righteous about the fact that the rich man didn't give what he could give.

153
00:15:34,480 --> 00:15:45,480
So he's there with his knife, sort of standing off to the side, and what does this organizer do?

154
00:15:45,480 --> 00:15:55,480
He proclaims to the Buddha that all the people who gathered there had given something for each, for every bit of the food.

155
00:15:55,480 --> 00:16:02,480
And he said, please vendor will serve.

156
00:16:02,480 --> 00:16:12,480
May everyone here receive a rich reward because they all, they all give according to their ability.

157
00:16:12,480 --> 00:16:22,480
And the rich man heard this, and he was, he was quite moved by this, by the generous.

158
00:16:22,480 --> 00:16:27,480
And he realized that he had had this whole thing misunderstood.

159
00:16:27,480 --> 00:16:35,480
And he misunderstood this man's intentions and started to realize that actually this guy was a really good person.

160
00:16:35,480 --> 00:16:43,480
And it's funny, the translation says, if I don't ask him to pardon me, punishment from the king will fall up on my head.

161
00:16:43,480 --> 00:16:46,480
But I'm pretty sure that's not what it says.

162
00:16:46,480 --> 00:16:56,480
You see, there's something called Deva Danda, which Deva means angel, but it can also mean king.

163
00:16:56,480 --> 00:17:01,480
The king is considered to be a god among men or a deity among men.

164
00:17:01,480 --> 00:17:09,480
So it could mean king, but it really doesn't, because it's talking about actually talking about his head,

165
00:17:09,480 --> 00:17:19,480
but there's no reason that the king would punish him, but it's the angels that will meet out punishment by splitting his head into pieces,

166
00:17:19,480 --> 00:17:21,480
which is a common phrase.

167
00:17:21,480 --> 00:17:37,480
There was this belief that if you did a terribly heinous crime, the angels would meet out punishment on your head by splitting it into pieces.

168
00:17:37,480 --> 00:17:44,480
And so he, actually this rich man who was very arrogant and conceited and was very much on the wrong path,

169
00:17:44,480 --> 00:17:52,480
men did his ways, just standing there, listening and watching and looking and seeing what a wonderful thing he had missed out on,

170
00:17:52,480 --> 00:17:58,480
because of his selfishness and his greed and his stinginess.

171
00:17:58,480 --> 00:18:10,480
And so he bowed down at the feet of this organizing fellow and said, please forgive me, sir.

172
00:18:10,480 --> 00:18:19,480
And the man looked down at him and said, why are you asking me forgiveness totally?

173
00:18:19,480 --> 00:18:27,480
I'm oblivious and having never thought anything bad about the man.

174
00:18:27,480 --> 00:18:32,480
So he was totally surprised as to what was wrong.

175
00:18:32,480 --> 00:18:40,480
And the treasure explained to him, sorry, the rich man explained to him that really he was angry.

176
00:18:40,480 --> 00:18:45,480
He gave that gift out of anger, and he purposefully gave very, very little.

177
00:18:45,480 --> 00:18:53,480
And he actually at this point felt kind of sad that he hadn't given something significant.

178
00:18:53,480 --> 00:18:58,480
And the Buddha saw this, standing right there, sitting there, probably eating.

179
00:18:58,480 --> 00:19:03,480
And asked what was happening, what was the situation was going on.

180
00:19:03,480 --> 00:19:18,480
And so they told him, and the rich man said, well, I was awful, really, and I feel like I missed a really good opportunity to take part in something wholesome.

181
00:19:18,480 --> 00:19:26,480
And the Buddha said, oh no, you didn't, actually, well, the Buddha basically said, don't ever think of a good deed as a trifle.

182
00:19:26,480 --> 00:19:31,480
In the end you gave, in the end you were, you relented.

183
00:19:31,480 --> 00:19:35,480
You could have said no, you could have given nothing.

184
00:19:35,480 --> 00:19:40,480
But in the end you gave something, you were kind, you were generous.

185
00:19:40,480 --> 00:19:42,480
You gave something that belonged to you.

186
00:19:42,480 --> 00:19:48,480
You didn't have no duty or no obligation to give.

187
00:19:48,480 --> 00:19:51,480
So even that is a good deed, the Buddha said.

188
00:19:51,480 --> 00:19:55,480
And he used it as an opportunity to tell this verse.

189
00:19:55,480 --> 00:20:05,480
And it's kind of, I would bet that there's the fact that this was a rich and sort of well-to-do,

190
00:20:05,480 --> 00:20:13,480
affluent individual played a part in being fairly positive about this.

191
00:20:13,480 --> 00:20:18,480
Because you could have warned the guy, you know, don't do evil, be careful, and yes, you've done a bad deed.

192
00:20:18,480 --> 00:20:25,480
But rich people don't tend, rich people who have wealth, who have power,

193
00:20:25,480 --> 00:20:30,480
who attract more flies with honey is the same.

194
00:20:30,480 --> 00:20:35,480
And such people you often have to be careful with.

195
00:20:35,480 --> 00:20:41,480
It's funny, the people who are most virtuous and spiritually uplifted,

196
00:20:41,480 --> 00:20:47,480
you tend to be able to treat them the harshest, tend to be able to be the hardest on them.

197
00:20:47,480 --> 00:20:51,480
If they can take it and it's useful for them and helpful for them.

198
00:20:51,480 --> 00:20:58,480
But for people who are rich, you're better off to go, people who are spoiled perhaps,

199
00:20:58,480 --> 00:21:01,480
you're better off to go positive.

200
00:21:01,480 --> 00:21:12,480
Nonetheless, there is a point here that any amount of goodness should not be disregarded,

201
00:21:12,480 --> 00:21:22,480
should not be underestimated.

202
00:21:22,480 --> 00:21:26,480
And so then he taught this verse.

203
00:21:26,480 --> 00:21:29,480
And this is like the last one, well even more so than the last one.

204
00:21:29,480 --> 00:21:34,480
I think this is really an awesome verse for us to remember as meditators,

205
00:21:34,480 --> 00:21:38,480
as Buddhists, as spiritual practitioners.

206
00:21:38,480 --> 00:21:43,480
Because it's easy to get discouraged when you think of your problems,

207
00:21:43,480 --> 00:21:49,480
when you think of your goals, when you think of spiritual goals.

208
00:21:49,480 --> 00:21:53,480
It's easy to think, well, I'll never be like that, I'll never get there.

209
00:21:53,480 --> 00:21:57,480
I'll never free myself from this.

210
00:21:57,480 --> 00:22:01,480
And we've become discouraged before we've even tried,

211
00:22:01,480 --> 00:22:04,480
because it seems like a mountain.

212
00:22:04,480 --> 00:22:08,480
But even a mountain when you get up close, it's made of rock.

213
00:22:08,480 --> 00:22:15,480
It's something that you can take apart.

214
00:22:15,480 --> 00:22:18,480
All good things come step by step, right?

215
00:22:18,480 --> 00:22:21,480
The journey of a thousand miles starts with the first step.

216
00:22:21,480 --> 00:22:25,480
It's a very important concept, because it's really how it works.

217
00:22:25,480 --> 00:22:27,480
Goodness doesn't disappear.

218
00:22:27,480 --> 00:22:30,480
Our good deeds don't just disappear.

219
00:22:30,480 --> 00:22:40,480
If every good success has to come from the deeds, from individual good deeds.

220
00:22:40,480 --> 00:22:48,480
And so whether it be generosity, and therefore the result being the affluence,

221
00:22:48,480 --> 00:22:57,480
or high status, all the good mundane things that come from being generous.

222
00:22:57,480 --> 00:23:02,480
Whether it be good for having good friends, all these good results.

223
00:23:02,480 --> 00:23:12,480
They all come from small deans, from small acts of kindness.

224
00:23:12,480 --> 00:23:19,480
Because our lives are this way. Our lives work moment by moment.

225
00:23:19,480 --> 00:23:24,480
And every moment we can do wholesomeness, we can do a good deed,

226
00:23:24,480 --> 00:23:26,480
we can do a bad deed.

227
00:23:26,480 --> 00:23:31,480
Every moment there's an opportunity, whether it be through generosity or morality,

228
00:23:31,480 --> 00:23:34,480
or simply through meditation.

229
00:23:34,480 --> 00:23:39,480
So our ethics, we don't have to have lofty ethics.

230
00:23:39,480 --> 00:23:42,480
Our ethics aren't about the principles themselves.

231
00:23:42,480 --> 00:23:46,480
It's about the acts, it's about our state of mind,

232
00:23:46,480 --> 00:23:50,480
and our momentary interaction with the world around us.

233
00:23:50,480 --> 00:23:58,480
In meditation likewise, enlightenment isn't something that you jump to,

234
00:23:58,480 --> 00:24:02,480
or you fall into, or you break open.

235
00:24:02,480 --> 00:24:05,480
It's not an instantaneous thing.

236
00:24:05,480 --> 00:24:10,480
It comes step by step in moment by moment.

237
00:24:10,480 --> 00:24:13,480
Every moment we have the opportunity to do good deeds.

238
00:24:13,480 --> 00:24:15,480
Every moment that we're mindful,

239
00:24:15,480 --> 00:24:17,480
when your mindful of the foot lifting,

240
00:24:17,480 --> 00:24:21,480
when your mindful of the foot moving and mindful of the foot placing,

241
00:24:21,480 --> 00:24:23,480
when your mindful of the stomach rising.

242
00:24:23,480 --> 00:24:26,480
That one moment is a wholesome mind.

243
00:24:26,480 --> 00:24:29,480
When you see that you're angry and you remind yourself,

244
00:24:29,480 --> 00:24:33,480
this is anger, it's not me, it's not mine, it's just anger.

245
00:24:33,480 --> 00:24:36,480
When you see something and you remind yourself,

246
00:24:36,480 --> 00:24:41,480
every time you do a good deed like this,

247
00:24:41,480 --> 00:24:43,480
when you're kind to someone, when you're generous,

248
00:24:43,480 --> 00:24:46,480
when you're compassionate, when you're helpful,

249
00:24:46,480 --> 00:24:50,480
when you're patient, when you're frustrated,

250
00:24:50,480 --> 00:24:52,480
and impatient with people,

251
00:24:52,480 --> 00:24:56,480
and then you remind yourself, impatient or frustrated.

252
00:24:56,480 --> 00:25:00,480
And as a result, become more patient.

253
00:25:00,480 --> 00:25:04,480
The power of this is not to be underestimated.

254
00:25:04,480 --> 00:25:07,480
Because this is how true power comes about,

255
00:25:07,480 --> 00:25:09,480
not by not in a single bound,

256
00:25:09,480 --> 00:25:12,480
not by one great act.

257
00:25:12,480 --> 00:25:16,480
You don't break through to become a good person.

258
00:25:16,480 --> 00:25:18,480
Goodness comes moment by moment.

259
00:25:18,480 --> 00:25:21,480
It's something anyone can do because it's here and now

260
00:25:21,480 --> 00:25:23,480
and it's moment by moment.

261
00:25:23,480 --> 00:25:26,480
This is what's so awesome about this path,

262
00:25:26,480 --> 00:25:29,480
this practice, Buddhism, meditation.

263
00:25:29,480 --> 00:25:33,480
It's not something lofty or difficult.

264
00:25:33,480 --> 00:25:36,480
As difficult as it is, it's just moments.

265
00:25:36,480 --> 00:25:39,480
It's something you can do any moment, every moment

266
00:25:39,480 --> 00:25:43,480
that you do a good deed.

267
00:25:43,480 --> 00:25:46,480
There's one step closer to the goal.

268
00:25:46,480 --> 00:25:50,480
It's one step higher, step up on the ladder,

269
00:25:50,480 --> 00:25:53,480
bringing you higher, making you happier, bringing peace

270
00:25:53,480 --> 00:25:58,480
and clarity and freedom to you.

271
00:25:58,480 --> 00:26:01,480
And to those around you.

272
00:26:01,480 --> 00:26:03,480
I mean, the neat thing about this story that isn't really

273
00:26:03,480 --> 00:26:05,480
doesn't really come out in the verse,

274
00:26:05,480 --> 00:26:07,480
is about encouraging others.

275
00:26:07,480 --> 00:26:11,480
So not only should we encourage others to be generous,

276
00:26:11,480 --> 00:26:13,480
but even more importantly, we should encourage others

277
00:26:13,480 --> 00:26:17,480
to be ethical and spiritual,

278
00:26:17,480 --> 00:26:18,480
contemplative.

279
00:26:18,480 --> 00:26:21,480
We should encourage others to meditate,

280
00:26:21,480 --> 00:26:23,480
to better themselves,

281
00:26:23,480 --> 00:26:25,480
to not just be content, to be an ordinary person,

282
00:26:25,480 --> 00:26:30,480
to get old sick and die, and have the world forget about them,

283
00:26:30,480 --> 00:26:33,480
but to move forward and upward,

284
00:26:33,480 --> 00:26:36,480
and to make use of the time that they have

285
00:26:36,480 --> 00:26:41,480
and the energy that they have and the life that they have

286
00:26:41,480 --> 00:26:49,480
to become, to put it to some use,

287
00:26:49,480 --> 00:26:55,480
to make it, to bring it, to have value, true value.

288
00:26:55,480 --> 00:26:58,480
So in encouraging others,

289
00:26:58,480 --> 00:27:04,480
we gain this extra support

290
00:27:04,480 --> 00:27:06,480
of having good people around us.

291
00:27:06,480 --> 00:27:08,480
If you don't encourage others in goodness,

292
00:27:08,480 --> 00:27:11,480
well, you can be as good as you want.

293
00:27:11,480 --> 00:27:13,480
It's not sure who you'll be surrounded by.

294
00:27:13,480 --> 00:27:15,480
You'll have to be surrounded by people who complain

295
00:27:15,480 --> 00:27:18,480
about it because they don't have the sense

296
00:27:18,480 --> 00:27:20,480
of the importance of goodness,

297
00:27:20,480 --> 00:27:25,480
and they haven't had the taste of the fruit of goodness.

298
00:27:25,480 --> 00:27:29,480
So you end up often being surrounded by people

299
00:27:29,480 --> 00:27:30,480
who don't understand goodness,

300
00:27:30,480 --> 00:27:32,480
who don't appreciate goodness,

301
00:27:32,480 --> 00:27:34,480
who don't have the clarity and the happiness

302
00:27:34,480 --> 00:27:36,480
that comes from goodness,

303
00:27:36,480 --> 00:27:40,480
and therefore surrounded often by miserable people.

304
00:27:40,480 --> 00:27:42,480
We don't want that.

305
00:27:42,480 --> 00:27:44,480
That's why it's very important

306
00:27:44,480 --> 00:27:46,480
to when we do good deeds,

307
00:27:46,480 --> 00:27:48,480
to encourage other people in it,

308
00:27:48,480 --> 00:27:51,480
and to work together,

309
00:27:51,480 --> 00:27:54,480
to perform good deeds.

310
00:27:54,480 --> 00:27:56,480
So anyway,

311
00:27:56,480 --> 00:27:58,480
that's the Dhammapada verse tonight.

312
00:27:58,480 --> 00:28:02,480
One more teaching on good things, bad things,

313
00:28:02,480 --> 00:28:05,480
spiritual things.

314
00:28:05,480 --> 00:28:11,480
Thank you all for tuning in, wishing you all a good practice,

315
00:28:11,480 --> 00:28:15,480
and success in practicing together in a good way.

316
00:28:15,480 --> 00:28:17,480
Thank you, have a good night.

