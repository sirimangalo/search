1
00:00:00,000 --> 00:00:20,640
Okay, believe me, everyone, so I think we'll just do some sort of dumb attack tonight.

2
00:00:20,640 --> 00:00:39,320
I thought I'd talk about the results of meditation practice, maybe we can say the results

3
00:00:39,320 --> 00:00:46,400
of doing the meditation course, but you should expect to get out of it.

4
00:00:46,400 --> 00:00:53,400
It's a trick, trick statement, because you shouldn't expect to get anything out of it.

5
00:00:53,400 --> 00:01:13,400
Expectations are, expectations are bad, right, so you have expectations, well, they get in the way of being mindful.

6
00:01:13,400 --> 00:01:23,400
You shouldn't expect to get anything out of anything, because expecting is a bad state of mind.

7
00:01:23,400 --> 00:01:35,400
Even if you get what you expect, it's still a cause for attachment, for expectation in the future.

8
00:01:35,400 --> 00:01:57,400
So maybe we should say differently, it's what one tends to acquire, or what benefits tend to come from meditation.

9
00:01:57,400 --> 00:02:05,400
Because then when you're thinking about doing a meditation course, you can ask yourself whether those are good things.

10
00:02:05,400 --> 00:02:14,400
Those are things worth getting, and whether you really believe me when I say that these are the things that can come from meditation.

11
00:02:14,400 --> 00:02:19,400
So let's talk about that.

12
00:02:19,400 --> 00:02:27,400
The first thing that comes from meditation is mindfulness. It should be no surprise.

13
00:02:27,400 --> 00:02:40,400
That's the claim that what we're practicing is mindfulness, and what we practice will tend to make you more mindful generally.

14
00:02:40,400 --> 00:02:57,400
And so by mindfulness, we really mean to be able to grasp an object as it is an experience, when you experience something, to be able to experience it clearly without judging it, without reacting to it.

15
00:02:57,400 --> 00:03:23,400
And this paves the way for seeing things clearly. It paves the way for insight, for wisdom, paves the way for letting go, because when you see things clearly as they are you lose any false attachment to the object thinking, hey, this is going to bring me such this or that or the other thing.

16
00:03:23,400 --> 00:03:31,400
Now you see how I know it's not. There's no rationale behind clinging to anything.

17
00:03:31,400 --> 00:03:36,400
When you look at things clearly, you're able to let go.

18
00:03:36,400 --> 00:03:42,400
So this is what mindfulness is good for me. Mindfulness is the first benefit. It's the first thing to get.

19
00:03:42,400 --> 00:03:57,400
It means that you're able to enter into situations in your life and deal with problems, with challenges in your life with a clear mind.

20
00:03:57,400 --> 00:04:15,400
So any occasion for being unhappy is greatly reduced, is mitigated or thwarted or alleviated, because you're not getting caught up.

21
00:04:15,400 --> 00:04:41,400
You're not setting yourself up for disappointment. You're not vulnerable to change, loss, unwelcome experiences, pain, suffering.

22
00:04:41,400 --> 00:04:48,400
Mindfulness, we would say, is the most useful tool in life.

23
00:04:48,400 --> 00:05:03,400
If you're unmindful, it means you come into experience distracted, into an experience distracted, reactionary, biased, and weak.

24
00:05:03,400 --> 00:05:11,400
The mind is weak because immediately, they experience immediately it's off reacting.

25
00:05:11,400 --> 00:05:23,400
It's not able to appreciate and experience and be with this situation.

26
00:05:23,400 --> 00:05:34,400
So there's the first benefit that's what we're developing. When you say to yourself, pain, pain, it's an artificial means of creating this state.

27
00:05:34,400 --> 00:05:46,400
It entices the mind. It encourages the mind to see things clearly, to observe things without reacting to them.

28
00:05:46,400 --> 00:05:53,400
The second benefit is happiness.

29
00:05:53,400 --> 00:06:01,400
That's what we all expect to get from something like meditation.

30
00:06:01,400 --> 00:06:07,400
When this happiness comes from meditation, happiness comes well because you avoid suffering.

31
00:06:07,400 --> 00:06:20,400
But it comes because you solve your problems. You alleviate so much of the burden of worry and stress and uncertainty.

32
00:06:20,400 --> 00:06:34,400
Loss of loss, being at a loss, feeling lost, having no grounding, having no bearing, having no direction.

33
00:06:34,400 --> 00:06:41,400
Mindfulness takes that away. You don't need direction. You just be here.

34
00:06:41,400 --> 00:06:54,400
When you're already here, you don't need direction. You've made it.

35
00:06:54,400 --> 00:07:23,400
It's great happiness that comes. It's great peace and great happiness, such a strength of mind, such a relief to not be subject to the whims of your own delusions and conceits and likes and dislikes, to be a slave or a prisoner of your own mind.

36
00:07:23,400 --> 00:07:32,400
When you're straighten it all out, you become invincible.

37
00:07:32,400 --> 00:07:38,400
Bad things come. What bad things? They're not bad. They're just things.

38
00:07:38,400 --> 00:07:41,400
They're not even things, really. They're experiences.

39
00:07:41,400 --> 00:07:51,400
And experiences, what do they do? They come and they go.

40
00:07:51,400 --> 00:08:01,400
That's the second. The third thing that you get from meditation generally, well, get from meditation is a start.

41
00:08:01,400 --> 00:08:09,400
So this particularly refers to the fact that meditation is about building a habit, just like everything.

42
00:08:09,400 --> 00:08:17,400
Most everything we do build mental habits. If you go to study at school, it's going to build many kinds of habits.

43
00:08:17,400 --> 00:08:28,400
It's going to help you organize your thinking in a certain way. It might be bad habits, like stress and worry and lack of focus or whatever.

44
00:08:28,400 --> 00:08:35,400
But it will build habits. Meditation also builds habits. So it's a start.

45
00:08:35,400 --> 00:08:41,400
And more than that, meditation is powerful in the purity of it.

46
00:08:41,400 --> 00:08:58,400
So it's like streamlining things. So instead of getting caught on the sidelines and being dragged and held back and never just going around in circles and never doing anything anywhere, never going anywhere.

47
00:08:58,400 --> 00:09:06,400
Meditation is like a release where you fly up and out.

48
00:09:06,400 --> 00:09:16,400
And so it puts you on a track. It gives you forces when I'm saying it. It's this inertia.

49
00:09:16,400 --> 00:09:23,400
It breaks the inertia and creates a new kind of inertia, heading in a certain direction.

50
00:09:23,400 --> 00:09:27,400
So the idea here is that not only in this life, but in future lives.

51
00:09:27,400 --> 00:09:35,400
Even though when you die, you'll probably, many of us will forget the things that we learned in this life.

52
00:09:35,400 --> 00:09:47,400
You won't lose the direction. When you get in the strong direction, you don't get pulled away.

53
00:09:47,400 --> 00:09:54,400
You don't lose that. Get it carries with you. There's an idea that it carries with you lifetime after lifetime after lifetime.

54
00:09:54,400 --> 00:10:07,400
And maybe some distant lifetime you'll meet a Buddha or a enlightened being. You quickly understand what they're saying because you're already going in that direction.

55
00:10:07,400 --> 00:10:13,400
Or maybe just somehow by yourself you'll come to see the truth because of the direction you're heading in.

56
00:10:13,400 --> 00:10:19,400
I'm at a man today from Iraq who claims to be enlightened.

57
00:10:19,400 --> 00:10:27,400
I get a lot of people claiming to be enlightened from other traditions. I don't take it completely at face value.

58
00:10:27,400 --> 00:10:32,400
I'm skeptical usually.

59
00:10:32,400 --> 00:10:39,400
But on the other hand, it's just that the word enlightened means many different things of course in different people.

60
00:10:39,400 --> 00:10:45,400
But nonetheless, this sort of thing can happen.

61
00:10:45,400 --> 00:10:51,400
He said, he said something important. He said, it's just that there was no one around to explain to me what happened.

62
00:10:51,400 --> 00:10:54,400
That sort of thing could happen. Hey, maybe he is enlightened.

63
00:10:54,400 --> 00:11:02,400
He seemed very interested in Buddhism. It's quite possible that he was a meditator in his past life.

64
00:11:02,400 --> 00:11:11,400
To some strange karma was born in Iraq, which by his own words was not a very pleasant place to live in.

65
00:11:11,400 --> 00:11:20,400
He had to escape with his family.

66
00:11:20,400 --> 00:11:24,400
The fourth benefit from meditation is a finish.

67
00:11:24,400 --> 00:11:27,400
So a start and a finish.

68
00:11:27,400 --> 00:11:35,400
The real goal in meditation is to finish, to put an end to the problems in life.

69
00:11:35,400 --> 00:11:41,400
So an end to our judging, our attachments, our clinging.

70
00:11:41,400 --> 00:11:45,400
To put an end to wrong views, wrong beliefs.

71
00:11:45,400 --> 00:12:02,400
But to put an end to suffering.

72
00:12:02,400 --> 00:12:08,400
So yes, meditation gives you good habits, it changes and takes away your bad habits.

73
00:12:08,400 --> 00:12:17,400
But it also opens you up to an experience of freedom.

74
00:12:17,400 --> 00:12:21,400
Whereas I've said before, the mind, let's go of everything.

75
00:12:21,400 --> 00:12:27,400
Not only says, not only sees things in this eye, yes, don't hold on to that, but finally he says,

76
00:12:27,400 --> 00:12:33,400
yes, don't hold on to anything. I as nothing is worth clinging to.

77
00:12:33,400 --> 00:12:44,400
There's release in the mind is free.

78
00:12:44,400 --> 00:12:49,400
So I thought I'd talk about this because we had one meditator finish this course today.

79
00:12:49,400 --> 00:12:53,400
And I thought it would be a good introduction to putting him on the spot.

80
00:12:53,400 --> 00:12:57,400
I'm seeing if he'd come and say a few words to us.

81
00:12:57,400 --> 00:13:00,400
Just about your experience.

82
00:13:00,400 --> 00:13:09,400
I'll ask you some questions.

83
00:13:09,400 --> 00:13:25,400
Okay, being a video.

84
00:13:25,400 --> 00:13:44,400
I think today it's hard to say because I still feel like I'm processing everything.

85
00:13:44,400 --> 00:13:58,400
I still feel like there have been a large difference.

86
00:13:58,400 --> 00:14:11,400
I'm not sure where things will end up, so I'm just like observing.

87
00:14:11,400 --> 00:14:27,400
I think it was useful, but I think it's hard to say what I'll get out of it until I actually leave and start living as I would normally do.

88
00:14:27,400 --> 00:14:50,400
I would say I found a lot of calmness that I think she never had for, and I found that to be quite surprising.

89
00:14:50,400 --> 00:14:59,400
Did you find anything inside that you were able to work out in that thing?

90
00:14:59,400 --> 00:15:03,400
Did you have any bad things coming in for a course?

91
00:15:03,400 --> 00:15:12,400
I think we all do, but I think it's just like to see how to leave here and see how things work out.

92
00:15:12,400 --> 00:15:23,400
I guess I wanted to get a sense of after doing a course and it gets quite intense.

93
00:15:23,400 --> 00:15:25,400
How did you feel today?

94
00:15:25,400 --> 00:15:33,400
Was there peace at all today or was it a accomplishment?

95
00:15:33,400 --> 00:15:47,400
For the last four or five days, there was a sense of intense peace and intense calmness.

96
00:15:47,400 --> 00:15:52,400
I think today the intensity is drifting away.

97
00:15:52,400 --> 00:15:56,400
I think the calmness is still there.

98
00:15:56,400 --> 00:16:03,400
That's good, not ecstatic that you've come through a lot.

99
00:16:03,400 --> 00:16:06,400
Something quite intense.

100
00:16:06,400 --> 00:16:09,400
It's a massive experience.

101
00:16:09,400 --> 00:16:14,400
It is definitely I think the kind of thing in the process.

102
00:16:14,400 --> 00:16:21,400
Thank you.

103
00:16:21,400 --> 00:16:26,400
See, it wasn't prepared, so I don't know what he's going to say.

104
00:16:26,400 --> 00:16:27,400
It kind of sucked really.

105
00:16:27,400 --> 00:16:31,400
That was kind of the kind of useless I'm going on.

106
00:16:31,400 --> 00:16:33,400
He didn't say that.

107
00:16:33,400 --> 00:16:38,400
Yes, you can expect peace and calm for sure.

108
00:16:38,400 --> 00:16:43,400
Again, we talk about this and we tell meditators, hey, you don't.

109
00:16:43,400 --> 00:16:46,400
But it is something that comes at the end of the course.

110
00:16:46,400 --> 00:16:52,400
And it really is important because that really is what the goal is, right?

111
00:16:52,400 --> 00:16:56,400
The goal is peace.

112
00:16:56,400 --> 00:17:00,400
It should be a profound sense of peace.

113
00:17:00,400 --> 00:17:02,400
Not something you should expect or look for.

114
00:17:02,400 --> 00:17:07,400
It's not something you usually get during the course because the course is difficult.

115
00:17:07,400 --> 00:17:17,400
It's challenging, but there you have it.

116
00:17:17,400 --> 00:17:18,400
Was it worth it?

117
00:17:18,400 --> 00:17:20,400
Was it good experience?

118
00:17:20,400 --> 00:17:21,400
Yes.

119
00:17:21,400 --> 00:17:23,400
Definitely, yes, good experience.

120
00:17:23,400 --> 00:17:31,400
So that's important.

121
00:17:31,400 --> 00:17:46,400
The other thing I was going to talk about was what it's like to live.

122
00:17:46,400 --> 00:17:51,400
We talked about living as a meditator, so it's not exactly that.

123
00:17:51,400 --> 00:17:55,400
But what sort of a person you are?

124
00:17:55,400 --> 00:18:01,400
Well, a person has become enlightened, really, and I think I've talked about this before.

125
00:18:01,400 --> 00:18:16,400
I think this is something I've talked about before, but I thought I'd go over it for you guys to get some idea about.

126
00:18:16,400 --> 00:18:24,400
Maybe what we're aiming for in a sense or some idea of what it's like to be mindful.

127
00:18:24,400 --> 00:18:27,400
So the Buddha gave this talk.

128
00:18:27,400 --> 00:18:28,400
He spoke this.

129
00:18:28,400 --> 00:18:33,400
I mean, we have these verses that are said to have been spoken by him.

130
00:18:33,400 --> 00:18:43,400
In response to some people criticizing Sariputa, who is the Buddha's chief disciple.

131
00:18:43,400 --> 00:18:56,400
You say I'm such a...

132
00:18:56,400 --> 00:18:59,400
You say I'm enlightened.

133
00:18:59,400 --> 00:19:02,400
Do you say that out of faith?

134
00:19:02,400 --> 00:19:04,400
Do you have faith in me, basically?

135
00:19:04,400 --> 00:19:09,400
And Sariputa said, no, basically he said, no, I don't have faith.

136
00:19:09,400 --> 00:19:14,400
And the monks got really upset about it, and they said, Sariputa has no faith in the Buddha.

137
00:19:14,400 --> 00:19:25,400
What he actually said, according to the text, is that it's not out of faith that he says that the Buddha is enlightened and so on.

138
00:19:25,400 --> 00:19:35,400
So what he meant was, of course, that it was out of knowledge and experience and understanding.

139
00:19:35,400 --> 00:19:40,400
Based on his own practice, based on his own enlightenment.

140
00:19:40,400 --> 00:19:47,400
But the monks came to the Buddha or the Buddha heard about this and they said, yeah, he's got no faith.

141
00:19:47,400 --> 00:19:51,400
And the Buddha said, yes, yes, that's Sariputa, right?

142
00:19:51,400 --> 00:19:56,400
He's faithless and unknowing.

143
00:19:56,400 --> 00:20:06,400
He's a chain, he's a burglar, he's a lockpick.

144
00:20:06,400 --> 00:20:13,400
He's hopeless, and without any...

145
00:20:13,400 --> 00:20:16,400
Without any opportunity.

146
00:20:16,400 --> 00:20:20,400
Without any opportunity and hopeless.

147
00:20:20,400 --> 00:20:26,400
And this is the height of humanity, the Buddha said.

148
00:20:26,400 --> 00:20:32,400
It's one of the rare sort of play on words that you find.

149
00:20:32,400 --> 00:20:40,400
I can't think of any other verse like it, or the Buddha gives us a play on words.

150
00:20:40,400 --> 00:20:44,400
Because faithless is bad, right?

151
00:20:44,400 --> 00:20:46,400
If you're faithless, it means you're skeptical.

152
00:20:46,400 --> 00:20:53,400
But it also can mean that you don't believe things out of faith because you know them.

153
00:20:53,400 --> 00:21:03,400
Sariputa is faithless and that means he had learned the truth for himself.

154
00:21:03,400 --> 00:21:09,400
So our goal is not to believe, you don't have to believe me when I tell you.

155
00:21:09,400 --> 00:21:15,400
Yes, meditation is good for your mindfulness, how's good benefits?

156
00:21:15,400 --> 00:21:19,400
We have evidence here, we have a meditator who's finished and he's still alive.

157
00:21:19,400 --> 00:21:23,400
Seems to say it's a good experience.

158
00:21:23,400 --> 00:21:27,400
But more importantly, you for yourself can see for yourself.

159
00:21:27,400 --> 00:21:30,400
When you've seen for yourself, then it's not about belief.

160
00:21:30,400 --> 00:21:38,400
This isn't a religion like Christianity or Islam or it's another.

161
00:21:38,400 --> 00:21:44,400
Well, those two stick out as really talking about believing and having faith.

162
00:21:44,400 --> 00:21:50,400
Not the criticized, but just to point out a difference, there's no faith.

163
00:21:50,400 --> 00:22:00,400
Faith is okay, faith is good, but it's considered in Buddhism to be inferior to knowledge.

164
00:22:00,400 --> 00:22:12,400
And we would say that if you weren't a criticized, you would say those religions that end on believing are in fear.

165
00:22:12,400 --> 00:22:18,400
They are incomplete in the sense that they don't lead people to knowledge.

166
00:22:18,400 --> 00:22:24,400
Of course, people will argue, I'm not trying to bring this into a religious today.

167
00:22:24,400 --> 00:22:37,400
But to point out, regardless of naming names, faith itself is inferior.

168
00:22:37,400 --> 00:22:46,400
In inferior to knowledge, you believe something versus you know it, knowing it is better, stronger, it's more powerful.

169
00:22:46,400 --> 00:22:48,400
So be faithless.

170
00:22:48,400 --> 00:22:59,400
And unknowing, the word is a kata knew, which has much more meaning in Pauli and in Thailand, if you say a kata knew they tend to know what you're saying.

171
00:22:59,400 --> 00:23:01,400
It means ungrateful.

172
00:23:01,400 --> 00:23:07,400
A kata knew means someone who doesn't know what has been done.

173
00:23:07,400 --> 00:23:15,400
So if someone did something for you and you don't acknowledge it, you're called a kata knew.

174
00:23:15,400 --> 00:23:18,400
Well, gratitude is important, right?

175
00:23:18,400 --> 00:23:23,400
If people do good things for you, it's a wholesome mind state to think of them and say,

176
00:23:23,400 --> 00:23:27,400
hey, yes, that person did something good for me.

177
00:23:27,400 --> 00:23:36,400
If you don't remember that, if it doesn't change the way you act towards this person, you're ungrateful.

178
00:23:36,400 --> 00:23:37,400
Kata means done.

179
00:23:37,400 --> 00:23:42,400
A kata means, kata knew means knows what is done.

180
00:23:42,400 --> 00:23:44,400
A kata knew means doesn't know what's done.

181
00:23:44,400 --> 00:23:47,400
So the Buddha said, sir, put this a kata knew.

182
00:23:47,400 --> 00:23:48,400
What did he mean?

183
00:23:48,400 --> 00:23:50,400
Well, you can break it up differently.

184
00:23:50,400 --> 00:23:55,400
You can say, a kata means not done or not made.

185
00:23:55,400 --> 00:23:58,400
An unnew means, of course, knows one who knows.

186
00:23:58,400 --> 00:24:04,400
So a kata knew means one who knows that which is not made, which is nibana.

187
00:24:04,400 --> 00:24:06,400
So I'll explain this.

188
00:24:06,400 --> 00:24:10,400
Everything else, everything that we experience, is made.

189
00:24:10,400 --> 00:24:12,400
I mean, it has conditions.

190
00:24:12,400 --> 00:24:18,400
And as a result, it's impermanent, unsatisfying, uncontrollable.

191
00:24:18,400 --> 00:24:20,400
It changes.

192
00:24:20,400 --> 00:24:23,400
It's unpredictable, chaotic.

193
00:24:23,400 --> 00:24:26,400
That's the universe, that's the world.

194
00:24:26,400 --> 00:24:33,400
Now, freedom, release cessation is not like that.

195
00:24:33,400 --> 00:24:36,400
It's stable.

196
00:24:36,400 --> 00:24:38,400
You could say it's satisfying.

197
00:24:38,400 --> 00:24:47,400
It's not controllable, but it's meaningful.

198
00:24:47,400 --> 00:24:49,400
It's unmade.

199
00:24:49,400 --> 00:24:50,400
And it's quite important.

200
00:24:50,400 --> 00:24:51,400
It's important to know that.

201
00:24:51,400 --> 00:24:56,400
To say, oh, yes, there is some way to have stability,

202
00:24:56,400 --> 00:25:05,400
to have an experience that is free from chaos,

203
00:25:05,400 --> 00:25:12,400
free from the unpredictable nature of things which are made,

204
00:25:12,400 --> 00:25:18,400
into things which are conditioned.

205
00:25:18,400 --> 00:25:22,400
The third Sunday Jada, Sunday Jada is a word that doesn't make much.

206
00:25:22,400 --> 00:25:26,400
We don't use it in English, but Sunday is a chain.

207
00:25:26,400 --> 00:25:33,400
Sunday Jada means one who cuts.

208
00:25:33,400 --> 00:25:35,400
So a Sunday Jada is one who cuts chains.

209
00:25:35,400 --> 00:25:40,400
And in poly, it's an idiom for, or it's a word.

210
00:25:40,400 --> 00:25:47,400
A euphemism, I guess, for, no, it's just a name for a burglar.

211
00:25:47,400 --> 00:25:50,400
Because they would put chains on their doors or on their gates.

212
00:25:50,400 --> 00:25:52,400
And so someone who breaks chains,

213
00:25:52,400 --> 00:25:57,400
is someone who does breaking in an entry.

214
00:25:57,400 --> 00:25:59,400
You know, burglar's houses.

215
00:25:59,400 --> 00:26:04,400
So he said, sorry, but there's one who cuts chains.

216
00:26:04,400 --> 00:26:06,400
They'll get figured out the chain here, of course,

217
00:26:06,400 --> 00:26:12,400
is the chain of causation, the chain of rebirth,

218
00:26:12,400 --> 00:26:13,400
giving rise to new things.

219
00:26:13,400 --> 00:26:17,400
All of our ambitions, our inclinations,

220
00:26:17,400 --> 00:26:22,400
our partialities, even our simple enjoyment of this or that.

221
00:26:22,400 --> 00:26:25,400
It all has effects.

222
00:26:25,400 --> 00:26:29,400
You enjoy something while it's going to lead you to plot out your life

223
00:26:29,400 --> 00:26:33,400
in order to get and maintain the things that you enjoy.

224
00:26:33,400 --> 00:26:39,400
I'm going to create things you're going to seek out stability,

225
00:26:39,400 --> 00:26:43,400
being able to get what you want all the time,

226
00:26:43,400 --> 00:26:46,400
and never get what you don't want.

227
00:26:46,400 --> 00:26:49,400
This creates a chain, because you have to work,

228
00:26:49,400 --> 00:26:51,400
and you have to constantly work,

229
00:26:51,400 --> 00:26:56,400
and generally, constantly be ill at ease,

230
00:26:56,400 --> 00:26:59,400
out of concern of losing.

231
00:26:59,400 --> 00:27:03,400
And because you do, at times, lose what you like,

232
00:27:03,400 --> 00:27:07,400
and get what you don't like, even rich people.

233
00:27:07,400 --> 00:27:10,400
I don't want people to talk bad about me.

234
00:27:10,400 --> 00:27:12,400
People are going to talk bad about you.

235
00:27:12,400 --> 00:27:17,400
I want people to talk bad to me.

236
00:27:17,400 --> 00:27:20,400
People will say bad things, and so on.

237
00:27:20,400 --> 00:27:22,400
I don't want to get old.

238
00:27:22,400 --> 00:27:23,400
You're going to get old.

239
00:27:23,400 --> 00:27:25,400
I don't want to get sick.

240
00:27:25,400 --> 00:27:28,400
I don't want to die.

241
00:27:28,400 --> 00:27:31,400
You're going to die.

242
00:27:31,400 --> 00:27:41,400
You've got the chain, and you stop seeking,

243
00:27:41,400 --> 00:27:44,400
you stop wandering.

244
00:27:44,400 --> 00:27:47,400
When you're truly present,

245
00:27:47,400 --> 00:27:48,400
there's none of this.

246
00:27:48,400 --> 00:27:49,400
There's peace.

247
00:27:49,400 --> 00:27:50,400
There's release.

248
00:27:50,400 --> 00:27:51,400
There's freedom.

249
00:27:51,400 --> 00:28:02,400
The last two are Hatawakasu.

250
00:28:02,400 --> 00:28:06,400
One who has no occasion,

251
00:28:06,400 --> 00:28:11,400
has lost all occasion, or cut off all occasion,

252
00:28:11,400 --> 00:28:16,400
hunt, hut, it's killed me.

253
00:28:16,400 --> 00:28:20,400
Killed their opportunity.

254
00:28:20,400 --> 00:28:22,400
Someone who's going nowhere.

255
00:28:22,400 --> 00:28:23,400
That is what we say.

256
00:28:23,400 --> 00:28:25,400
Parents say to their kids,

257
00:28:25,400 --> 00:28:28,400
they're going nowhere.

258
00:28:28,400 --> 00:28:30,400
You're a loser.

259
00:28:30,400 --> 00:28:32,400
Do you want to be a loser?

260
00:28:32,400 --> 00:28:34,400
Yes, we want to be a loser.

261
00:28:34,400 --> 00:28:36,400
We want to lose all of our attachments,

262
00:28:36,400 --> 00:28:37,400
all of our separate.

263
00:28:37,400 --> 00:28:40,400
You see, play on words.

264
00:28:40,400 --> 00:28:43,400
But having no occasion means having no occasion

265
00:28:43,400 --> 00:28:47,400
for the arising of defilements.

266
00:28:47,400 --> 00:28:51,400
Someone who has no occasion,

267
00:28:51,400 --> 00:28:55,400
no opportunity.

268
00:28:55,400 --> 00:28:58,400
There's no opportunity for their mind,

269
00:28:58,400 --> 00:29:01,400
for their attachments,

270
00:29:01,400 --> 00:29:07,400
because they don't arise.

271
00:29:07,400 --> 00:29:11,400
Someone who practices meditation becomes pure.

272
00:29:11,400 --> 00:29:13,400
Pureity is to be,

273
00:29:13,400 --> 00:29:15,400
it's the first thing to be expected

274
00:29:15,400 --> 00:29:17,400
from practicing mindfulness.

275
00:29:17,400 --> 00:29:19,400
Sata nangwisudhiya.

276
00:29:19,400 --> 00:29:22,400
Purification of beings.

277
00:29:22,400 --> 00:29:23,400
It's as simple.

278
00:29:23,400 --> 00:29:27,400
It's not something majestic or mysterious.

279
00:29:27,400 --> 00:29:30,400
It's just simply the purity that comes

280
00:29:30,400 --> 00:29:32,400
from being objective,

281
00:29:32,400 --> 00:29:34,400
from being at peace,

282
00:29:34,400 --> 00:29:36,400
content,

283
00:29:36,400 --> 00:29:40,400
and aware of things just as they are,

284
00:29:40,400 --> 00:29:43,400
without needing them to be some other way.

285
00:29:43,400 --> 00:29:49,400
Sata wakasu.

286
00:29:49,400 --> 00:29:50,400
One does so.

287
00:29:50,400 --> 00:29:52,400
The last one is the best.

288
00:29:52,400 --> 00:29:54,400
It's hopeless.

289
00:29:54,400 --> 00:29:55,400
Yes, sir.

290
00:29:55,400 --> 00:29:58,400
Put this hopeless.

291
00:29:58,400 --> 00:30:01,400
You say about someone who tries and tries

292
00:30:01,400 --> 00:30:03,400
but fails all the time.

293
00:30:03,400 --> 00:30:04,400
It keeps failing.

294
00:30:04,400 --> 00:30:06,400
Eventually, they fail enough.

295
00:30:06,400 --> 00:30:07,400
You just say,

296
00:30:07,400 --> 00:30:10,400
you're hopeless.

297
00:30:10,400 --> 00:30:12,400
But here, it's something quite different.

298
00:30:12,400 --> 00:30:13,400
It means,

299
00:30:13,400 --> 00:30:14,400
I don't care if I'm hopeless.

300
00:30:14,400 --> 00:30:16,400
I have no hopes.

301
00:30:16,400 --> 00:30:21,400
I don't hope for anything.

302
00:30:21,400 --> 00:30:23,400
Someone asked me recently.

303
00:30:23,400 --> 00:30:24,400
They said,

304
00:30:24,400 --> 00:30:25,400
not only recently,

305
00:30:25,400 --> 00:30:26,400
they said,

306
00:30:26,400 --> 00:30:31,400
they said there are losers or something like that.

307
00:30:31,400 --> 00:30:32,400
And I said,

308
00:30:32,400 --> 00:30:35,400
well, luckily there's no,

309
00:30:35,400 --> 00:30:40,400
there's no exam for life.

310
00:30:40,400 --> 00:30:44,400
You know, we talk a lot in Buddhism about the dire consequences

311
00:30:44,400 --> 00:30:46,400
of defilements.

312
00:30:46,400 --> 00:30:48,400
And there are consequences.

313
00:30:48,400 --> 00:30:50,400
You can go to hell.

314
00:30:50,400 --> 00:30:52,400
Or you can be such an evil person

315
00:30:52,400 --> 00:30:55,400
that you'll be reborn in a place of great suffering.

316
00:30:55,400 --> 00:30:58,400
Apparently.

317
00:30:58,400 --> 00:31:01,400
I don't want to force you to believe that.

318
00:31:01,400 --> 00:31:03,400
I know many people don't believe that's possible,

319
00:31:03,400 --> 00:31:07,400
but if you break it down,

320
00:31:07,400 --> 00:31:11,400
cause and effect is a real true power in this world.

321
00:31:11,400 --> 00:31:16,400
And bad causes have bad effects.

322
00:31:16,400 --> 00:31:18,400
Evil mind states are called evil

323
00:31:18,400 --> 00:31:20,400
because they lead to evil.

324
00:31:20,400 --> 00:31:44,400
Evil being brought back upon you.

325
00:31:44,400 --> 00:31:48,400
But ultimately that says only something about those states.

326
00:31:48,400 --> 00:31:51,400
And only talks about a causal relationship.

327
00:31:51,400 --> 00:31:54,400
I mean, in ultimate fact, in actual fact,

328
00:31:54,400 --> 00:31:57,400
they're still just experiences.

329
00:31:57,400 --> 00:32:01,400
The real problem hell is that you probably won't like it.

330
00:32:01,400 --> 00:32:03,400
If you were enlightened,

331
00:32:03,400 --> 00:32:06,400
and could somehow be reborn in hell,

332
00:32:06,400 --> 00:32:09,400
it wouldn't happen because you wouldn't be able to give rise

333
00:32:09,400 --> 00:32:10,400
to the states that lead there.

334
00:32:10,400 --> 00:32:13,400
But if you could,

335
00:32:13,400 --> 00:32:14,400
it wouldn't be hell.

336
00:32:14,400 --> 00:32:16,400
It would just be more experiences.

337
00:32:16,400 --> 00:32:21,400
It's more seeing, hearing, smelling, tasting, feeling, thinking.

338
00:32:21,400 --> 00:32:24,400
And so hopeless in this context means

339
00:32:24,400 --> 00:32:27,400
you don't hope for things to be this way or that way.

340
00:32:27,400 --> 00:32:33,400
It's kind of a kind of a reverse catch-22 or something.

341
00:32:33,400 --> 00:32:36,400
There's a...

342
00:32:36,400 --> 00:32:43,400
You give up caring about whether you're going to be happy.

343
00:32:43,400 --> 00:32:46,400
You give up looking for peace.

344
00:32:46,400 --> 00:32:49,400
You give up seeking out happiness.

345
00:32:49,400 --> 00:32:54,400
You give up striving for freedom to some extent.

346
00:32:54,400 --> 00:32:56,400
We don't want to give up striving.

347
00:32:56,400 --> 00:32:58,400
The Buddha said striving is good.

348
00:32:58,400 --> 00:33:00,400
But you're striving to let go,

349
00:33:00,400 --> 00:33:02,400
to let go of everything.

350
00:33:02,400 --> 00:33:06,400
You're striving to not strive in a sense.

351
00:33:09,400 --> 00:33:12,400
So someone is truly hopeless who has no hope.

352
00:33:12,400 --> 00:33:14,400
It doesn't hope for this or that.

353
00:33:14,400 --> 00:33:16,400
It doesn't hope that this will go away

354
00:33:16,400 --> 00:33:18,400
or that will come or so on.

355
00:33:21,400 --> 00:33:24,400
When you no longer want happiness,

356
00:33:24,400 --> 00:33:26,400
that's when you find happiness.

357
00:33:26,400 --> 00:33:28,400
When you no longer want for peace,

358
00:33:28,400 --> 00:33:29,400
you're at peace.

359
00:33:36,400 --> 00:33:38,400
One time, so...

360
00:33:38,400 --> 00:33:43,400
This is the height of humanity.

361
00:33:43,400 --> 00:33:46,400
So...

362
00:33:46,400 --> 00:33:49,400
Thought that would be interesting to talk about.

363
00:33:49,400 --> 00:33:52,400
Give you some frame of reference.

364
00:33:52,400 --> 00:33:54,400
What are we trying to be...

365
00:33:54,400 --> 00:33:56,400
What do we want to be?

366
00:33:56,400 --> 00:33:59,400
What sort of a person are we looking to try to become?

367
00:33:59,400 --> 00:34:01,400
Give you a little direction.

368
00:34:01,400 --> 00:34:03,400
Hopefully some of this is appealing.

369
00:34:03,400 --> 00:34:05,400
Hopefully because this is where we're headed.

370
00:34:05,400 --> 00:34:08,400
If you want to get off now,

371
00:34:08,400 --> 00:34:10,400
and now you know where we're headed,

372
00:34:10,400 --> 00:34:12,400
you can always go home.

373
00:34:15,400 --> 00:34:17,400
I think this is one of the great things about Buddhism.

374
00:34:17,400 --> 00:34:21,400
It tends to be universally agreeable.

375
00:34:21,400 --> 00:34:23,400
It's hard, it's possible,

376
00:34:23,400 --> 00:34:27,400
but it's hard to argue against many of the things that Buddha said.

377
00:34:27,400 --> 00:34:31,400
Because they weren't particular.

378
00:34:31,400 --> 00:34:36,400
Like, do this practice, and then you'll go to that place or something.

379
00:34:36,400 --> 00:34:39,400
Believe this, believe that.

380
00:34:39,400 --> 00:34:41,400
The Buddha said, investigate.

381
00:34:41,400 --> 00:34:45,400
Here are some tools that will help you learn for yourself what is true.

382
00:34:45,400 --> 00:34:47,400
Basically what he said.

383
00:34:47,400 --> 00:34:49,400
He made lots of claims about what is true,

384
00:34:49,400 --> 00:34:51,400
but he always said,

385
00:34:51,400 --> 00:34:53,400
don't believe me.

386
00:34:53,400 --> 00:34:55,400
I didn't actually say, don't believe me,

387
00:34:55,400 --> 00:35:02,400
he said, don't be content with faith.

388
00:35:02,400 --> 00:35:06,400
Don't be content with belief.

389
00:35:06,400 --> 00:35:08,400
You can believe the Buddha you want.

390
00:35:08,400 --> 00:35:10,400
It's not bad.

391
00:35:10,400 --> 00:35:14,400
It's just not enough.

392
00:35:14,400 --> 00:35:18,400
It's not enough.

393
00:35:18,400 --> 00:35:20,400
So there you go.

394
00:35:20,400 --> 00:35:22,400
There's some dhamma for tonight.

395
00:35:22,400 --> 00:35:24,400
Thank you all for coming out.

396
00:35:24,400 --> 00:35:26,400
Thank you for your practice.

397
00:35:26,400 --> 00:35:28,400
Thank you to Sarat for sticking it out

398
00:35:28,400 --> 00:35:32,400
and for successfully completing the foundation course.

399
00:35:32,400 --> 00:35:34,400
Congratulations.

400
00:35:34,400 --> 00:36:01,400
Good night everyone.

