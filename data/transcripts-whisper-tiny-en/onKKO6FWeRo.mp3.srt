1
00:00:00,000 --> 00:00:10,500
There is a thing about the Buddha's teaching I find scary and it was something that you

2
00:00:10,500 --> 00:00:19,420
said utadamu is that when we see things as they are we lose the idea of beauty so I won't

3
00:00:19,420 --> 00:00:27,900
be able to find a female beautiful ever again I find some things about the teaching good

4
00:00:27,900 --> 00:00:34,300
but some things are just wack and scary a lot of wack was a good thing

5
00:00:34,300 --> 00:00:40,580
isn't wack's not a good thing dude that's wack wack is bad

6
00:00:40,580 --> 00:00:44,360
oh wack is bad learn something every day

7
00:00:44,360 --> 00:00:53,640
ah well there's there's this there's this story that lumpo jodok this this one of my teachers

8
00:00:53,640 --> 00:01:01,240
he he was presented with this from a Westerner who said he came to his teacher and came

9
00:01:01,240 --> 00:01:07,280
to lumpo jodok and said oh I'm I'm very afraid I don't want to practice anymore

10
00:01:07,280 --> 00:01:13,040
he said why not well because I love my girlfriend and I'm afraid that if I he's he said

11
00:01:13,040 --> 00:01:20,280
I'm afraid I'm becoming enlightened and if I become enlightened that I won't I won't want

12
00:01:20,280 --> 00:01:26,560
to to stay with my girlfriend and lumpo jodok which is laughed and said you know don't

13
00:01:26,560 --> 00:01:32,240
worry about it there's lots of the filaments left for you to hold on to I mean the the

14
00:01:32,240 --> 00:01:36,560
answer to this question that I like to give is is that you only give up what you want to

15
00:01:36,560 --> 00:01:43,560
give up so you know is is killing good for you you know when you when you kill when

16
00:01:43,560 --> 00:01:47,720
you engage in murder does that make you happy and do you think that's a good thing to

17
00:01:47,720 --> 00:01:53,800
cling to this kind of thing I mean we're dealing at that level so we're trying to understand

18
00:01:53,800 --> 00:01:57,560
how killing is wrong how stealing is wrong how these things are really corrupting our

19
00:01:57,560 --> 00:02:10,840
mind if you still find things beauty it's really really far more far more subtle

20
00:02:10,840 --> 00:02:18,560
defilement and it's something that eventually you you decide for yourself I mean intellectually

21
00:02:18,560 --> 00:02:22,760
it's not that difficult because females and males human beings are not beautiful there's

22
00:02:22,760 --> 00:02:29,760
nothing beautiful about them we're full of blood and pus and and urine and feces and you

23
00:02:29,760 --> 00:02:34,000
think you think females are beautiful but if you if you were to cut them up into pieces

24
00:02:34,000 --> 00:02:39,720
and put them on a plate would you want to would you find it beautiful then I mean what

25
00:02:39,720 --> 00:02:48,280
is it about the female form that you find beautiful or the male if you if you don't

26
00:02:48,280 --> 00:02:53,800
wash it it becomes smelly and so I mean it it's mostly it is really illusion I mean why

27
00:02:53,800 --> 00:02:58,640
do dogs find female dogs attractive do you find female dogs attractive and some people do

28
00:02:58,640 --> 00:03:07,560
I suppose do you find female do you find female rats or you know female dung beetles do you

29
00:03:07,560 --> 00:03:14,120
find them attractive it's illusion it's it's something that we develop in our mind based

30
00:03:14,120 --> 00:03:25,160
on the perception of or the perception of this is being an object that will lead to

31
00:03:25,160 --> 00:03:32,120
our gratification of our central pleasures and of course the brain being being hardwired

32
00:03:32,120 --> 00:03:40,120
hired by wired but being wired to to think in that way and to recognize and to react to

33
00:03:40,120 --> 00:03:45,880
that which is recognized as an object of sexual attraction so this species is attracted

34
00:03:45,880 --> 00:03:52,560
to its own species and not to other species it's but in you know intellectually it's not

35
00:03:52,560 --> 00:03:58,320
hard to see that you know there's nothing or in general that there's there how could

36
00:03:58,320 --> 00:04:03,280
something be beautiful and something be ugly what what does it mean and of course artists

37
00:04:03,280 --> 00:04:08,880
have explored this for generations but because they didn't have the Buddha's teaching they

38
00:04:08,880 --> 00:04:14,720
weren't able to see that there's no real mystery there's just is no beauty there beauty

39
00:04:14,720 --> 00:04:22,000
is is is a concept that arises it's based on in the case of of sexuality it's based

40
00:04:22,000 --> 00:04:27,640
on as I said the the the the brain and the mind and the how it works together sexuality is

41
00:04:27,640 --> 00:04:34,360
something that is remembered by us we could even say from a Buddhist point of view it is a

42
00:04:34,360 --> 00:04:40,040
construct we're not you know humans aren't a natural thing it's something that we've constructed

43
00:04:40,040 --> 00:04:45,320
from generation even from an evolution point of view humans are not natural they have evolved

44
00:04:45,320 --> 00:04:53,160
from a series of as as biologists would say from a series of physical mutations but from a Buddhist

45
00:04:53,160 --> 00:05:02,520
point of view from a series of mental and physical mutations that that include our memories our

46
00:05:02,520 --> 00:05:06,920
perceptions our recognitions of things as this will bring me pleasure this will bring me pain

47
00:05:07,800 --> 00:05:12,520
and so we have these fear instincts and we have these sexual instincts we have these attraction

48
00:05:12,520 --> 00:05:26,280
instincts we have the eating the food instincts and so on

49
00:05:26,280 --> 00:05:32,920
that one we're going to and so

50
00:05:32,920 --> 00:05:42,520
so these these are our things that we have we have developed we have cultivated them over

51
00:05:42,520 --> 00:05:50,760
time the the for the reason that we remember them as being pleasurable and we develop the habit

52
00:05:50,760 --> 00:05:56,040
of remembering them as being pleasurable this leads to we would say leads to instincts

53
00:05:56,040 --> 00:06:04,360
leads to hormones and leads us to to react in the way that we do it's just a a construct it's a

54
00:06:05,400 --> 00:06:13,720
formation that we have developed into a habit and that leads you to think that this is beautiful

55
00:06:13,720 --> 00:06:17,640
but you're actually thinking this is going to bring me pleasure and it does bring you

56
00:06:17,640 --> 00:06:23,560
pleasure as you look at it and so on and it starts the chemical receptors in the brain working and

57
00:06:23,560 --> 00:06:30,040
so on it's an addiction just like any other drug addiction it doesn't lead you to peace happiness

58
00:06:30,040 --> 00:06:36,040
and freedom from suffering but you know that's all the theory behind it as I said it's not the

59
00:06:36,040 --> 00:06:41,800
most important thing the best thing is to take it at like a sweater you know you have this woolen

60
00:06:41,800 --> 00:06:46,840
sweater and you just start pulling on the loose end because you want to get rid of that loose end

61
00:06:46,840 --> 00:06:51,560
and then you just see what happens the secret is that at the end you wind up without a sweater

62
00:06:51,560 --> 00:06:56,040
so at the end you let go of everything as you start letting go of the things that you are clear

63
00:06:56,040 --> 00:07:02,280
bring you suffering you see more and you see more subtle attachments that are also bringing you

64
00:07:02,280 --> 00:07:07,400
suffering and not bringing you any benefit but it's only by seeing things clearly because of our

65
00:07:07,400 --> 00:07:13,400
views and our our beliefs and therefore our fears and our worries we get the idea that

66
00:07:13,400 --> 00:07:21,640
that there's these are intrinsically beneficial in that to give them up would be suffering but

67
00:07:21,640 --> 00:07:27,480
that's just an intellectual view it has no basis in reality the more you give up the more happiness

68
00:07:27,480 --> 00:07:32,040
you have people have the idea that an equanimous person would be boring would be a zombie

69
00:07:32,680 --> 00:07:38,680
and it's this is just an idea if you if you can prove that empirically then that's something else

70
00:07:38,680 --> 00:07:45,400
but in pure in reality shows us something quite different that the more desires the more attachments

71
00:07:45,400 --> 00:07:52,200
link we let go of the more happy and and carefree we are the more the more clear our

72
00:07:52,200 --> 00:08:01,160
mind is the more bright and enlightened our mind is what i'm going to say might sound as if

73
00:08:01,160 --> 00:08:09,240
it is in contrary to what Bantayutadamu said but it is really not i just want to add that

74
00:08:10,520 --> 00:08:22,120
the the conception the perception of beauty might change due the course of during the course

75
00:08:22,120 --> 00:08:36,360
of meditation beauty might still be experienced but it's it can be then different than you see it

76
00:08:36,360 --> 00:08:52,840
now beauty might come then from within your heart and so even a very ugly thing can what

77
00:08:52,840 --> 00:09:01,320
for others might be ugly can for you be beautiful when you let go of the concept of beauty that

78
00:09:01,320 --> 00:09:09,560
you have now and when you see the beauty like wow this is the beautiful woman and follow after

79
00:09:10,680 --> 00:09:21,560
it is not that really there is is no beauty at all but i as i said the beauty might come

80
00:09:21,560 --> 00:09:31,480
from without your your heart or from within your heart and that makes a thing beautiful

81
00:09:36,360 --> 00:09:44,680
yeah i mean it's a good point that well i think the real point is that beauty is just a word

82
00:09:44,680 --> 00:09:49,800
that we use no what do you mean by beautiful in in the sense that it's being used in the by the

83
00:09:49,800 --> 00:09:58,760
question or as i said it has to do with attachment and and really in the end illusion but

84
00:09:59,400 --> 00:10:04,760
it's not to say that you can't use the word beautiful in a correct way and you can use it to describe

85
00:10:05,480 --> 00:10:11,400
wholesome states if a person if a person is generous you can you can see them as beautiful

86
00:10:11,400 --> 00:10:18,120
that's a beautiful person for that reason that that's a beautiful thing they did for example

87
00:10:18,120 --> 00:10:23,240
but you're just using the word what does that word mean i think that's the perfect use of

88
00:10:23,240 --> 00:10:28,440
the word if someone does something like i think my teacher is quite beautiful because i see

89
00:10:28,440 --> 00:10:35,160
him as a very pure being yeah but he's just an old man who no farts and spits and so

90
00:10:37,000 --> 00:10:43,640
but that's you know he's he does it quite beautifully so i i don't think anyone would find

91
00:10:43,640 --> 00:10:49,240
so i don't even think i should say his things like that but he's certainly you know he's an old man

92
00:10:51,480 --> 00:10:56,600
and yet people find him quite beautiful because he is a beautiful person

93
00:10:57,640 --> 00:11:04,920
and the point is to ask what that means in terms of abbedama in terms of what are the

94
00:11:04,920 --> 00:11:11,000
mind states involved with an experience of something that is beautiful in that case i would say

95
00:11:11,000 --> 00:11:15,000
it's an appreciation of what is wholesome which is also a beautiful thing it's beautiful to

96
00:11:15,000 --> 00:11:22,280
appreciate things that are truly beautiful so certainly i didn't want to say that everything is

97
00:11:22,280 --> 00:11:33,720
that there is no beauty but there is no there's no object of the sense that has intrinsic

98
00:11:33,720 --> 00:11:40,280
beauty you know physical beauty is just an illusion i mean you can say something's beautiful it's

99
00:11:40,280 --> 00:11:45,560
in the end meaningless you i don't think you can define that i mean that's the problem i said

100
00:11:45,560 --> 00:11:52,520
that artists have how to define what is beautiful and so they went up stretching the limits and

101
00:11:53,240 --> 00:11:59,000
finding attraction and things that are unattractive and so on and you know in pictures that are ugly

102
00:11:59,000 --> 00:12:04,920
they they they pervert they twisted in such a way that the mind wants to take it as something

103
00:12:04,920 --> 00:12:11,160
beautiful but um but can't and so on and you know like Pablo Picasso or so on finding ways

104
00:12:11,160 --> 00:12:16,840
to to manipulate this so that when you look at his faces they're almost beautiful but then

105
00:12:16,840 --> 00:12:22,040
the eyes out of place or something like that and you come to see that oh the beauty is just

106
00:12:23,240 --> 00:12:28,360
just this concept that we have or it's a recognition because we have the ability to recognize faces

107
00:12:28,360 --> 00:12:30,360
in time

