1
00:00:00,000 --> 00:00:05,000
Okay, so today we'll be continuing to talk about the

2
00:00:07,000 --> 00:00:11,000
The mongala, should we stop sometime?

3
00:00:19,000 --> 00:00:22,000
Just to recap the mongala are those things that

4
00:00:23,000 --> 00:00:24,000
do away with evil

5
00:00:24,000 --> 00:00:33,000
So we've gone through three stanzas

6
00:00:35,000 --> 00:00:39,000
and now we come up to some that are

7
00:00:39,000 --> 00:00:45,000
not so directly related to the meditation practice

8
00:00:45,000 --> 00:00:48,000
so we'll try to skip quickly through them

9
00:00:48,000 --> 00:00:51,000
but in order to go, in order to be complete

10
00:00:51,000 --> 00:00:54,000
we'll go through them in order

11
00:00:54,000 --> 00:01:00,000
and there are things that you can say on these blessings about meditation

12
00:01:04,000 --> 00:01:12,000
These are blessings that generally relate to people who live in the house

13
00:01:12,000 --> 00:01:16,000
people who live ordinary lives and have jobs

14
00:01:16,000 --> 00:01:22,000
make money and employ people or are employed by other people

15
00:01:25,000 --> 00:01:30,000
but the general purpose of these blessings is to create

16
00:01:30,000 --> 00:01:34,000
peace and harmony just the same

17
00:01:35,000 --> 00:01:39,000
and for people who are looking to

18
00:01:39,000 --> 00:01:50,000
make their way closer to a meditative state of life

19
00:01:50,000 --> 00:01:55,000
who find themselves surrounded by obstacles

20
00:01:55,000 --> 00:01:59,000
or surrounded by worldly

21
00:01:59,000 --> 00:02:05,000
activities

22
00:02:06,000 --> 00:02:12,000
These are very important ways to create peace and harmony

23
00:02:12,000 --> 00:02:19,000
and a meditative state

24
00:02:19,000 --> 00:02:22,000
or meditative way of life

25
00:02:22,000 --> 00:02:25,000
that will bring anyone closer to the Buddha's teaching

26
00:02:25,000 --> 00:02:30,000
if someone's goal is to ordain

27
00:02:30,000 --> 00:02:33,000
then it will bring them closer to that goal

28
00:02:33,000 --> 00:02:38,000
because of the peace and the harmony that it brings

29
00:02:38,000 --> 00:02:42,000
if someone's goal is to live a meditative life as a lay person

30
00:02:42,000 --> 00:02:45,000
then these are the sorts of things that will allow

31
00:02:45,000 --> 00:02:47,000
besides the things that we've already talked about here

32
00:02:47,000 --> 00:02:51,000
are more things that are useful for all of us

33
00:02:51,000 --> 00:02:55,000
as monastics they aren't even direct

34
00:02:55,000 --> 00:02:59,000
some of them aren't even directly useful

35
00:02:59,000 --> 00:03:05,000
but they are those things that we have to keep in mind

36
00:03:05,000 --> 00:03:09,000
and the general principles that we have to keep in mind

37
00:03:09,000 --> 00:03:11,000
in terms of creating peace and harmony

38
00:03:11,000 --> 00:03:21,000
the next ones are Mata, Pitu, Upa, Tan and Putata, Dara, Sasango, Hana, Kula, Jaka, Manta, Nita, Mangalamutamu

39
00:03:21,000 --> 00:03:25,000
so caring for one's parents

40
00:03:25,000 --> 00:03:29,000
who careings for one's mother caring for one's father

41
00:03:29,000 --> 00:03:33,000
having good relations with one's relatives

42
00:03:33,000 --> 00:03:38,000
with one's or having a good relationship with one's

43
00:03:38,000 --> 00:03:43,000
spouse and children

44
00:03:43,000 --> 00:03:48,000
and doing work that is uncomplicated

45
00:03:48,000 --> 00:03:51,000
or that is not crooked

46
00:03:51,000 --> 00:03:56,000
having a straight, straight affairs

47
00:03:56,000 --> 00:04:01,000
affairs that are not crooked or not complicated

48
00:04:01,000 --> 00:04:03,000
so this is the next dance

49
00:04:03,000 --> 00:04:04,000
and it goes with the next one

50
00:04:04,000 --> 00:04:06,000
so we might as well add them all in here

51
00:04:06,000 --> 00:04:13,000
the next one is Dan and Jamacharia, Nata, Kana and Chessango

52
00:04:13,000 --> 00:04:18,000
now with Janikaman, Nita, Mangalamutamu

53
00:04:18,000 --> 00:04:25,000
Dan means charity, Jamacharia, the practice of the Jamam

54
00:04:27,000 --> 00:04:30,000
Nata, Kana and Chessango

55
00:04:30,000 --> 00:04:32,000
having good relationships with

56
00:04:32,000 --> 00:04:35,000
all of the one's relatives

57
00:04:35,000 --> 00:04:41,280
And Anavad-dhanikamani, performing deeds that are unblamed,

58
00:04:41,280 --> 00:04:46,480
non-blamed working, that are free from blame.

59
00:04:46,480 --> 00:04:50,680
So we can, what we'll see here is now a progression

60
00:04:50,680 --> 00:04:55,720
and we'll begin in these stanzas by talking about

61
00:04:55,720 --> 00:04:59,920
those things that will be useful specifically in the lay life.

62
00:04:59,920 --> 00:05:04,960
But we can see towards the end it starts, or in the second stanzas it starts to get a little bit

63
00:05:04,960 --> 00:05:09,680
more involved with the meditation.

64
00:05:09,680 --> 00:05:12,240
So we'll go through all of these today.

65
00:05:12,240 --> 00:05:15,120
They're all quite similar actually.

66
00:05:15,120 --> 00:05:19,840
Madapitu Upatana, and this is something looking after your parents is something that

67
00:05:19,840 --> 00:05:29,480
meditators will quite often overlook, forget about, or even

68
00:05:29,480 --> 00:05:35,920
doesn't aim, I've even had arguments with people who have told me that,

69
00:05:35,920 --> 00:05:44,640
have explained their view that, well their parents only gave birth to them out of desire,

70
00:05:44,640 --> 00:05:52,240
out of lust for each other, and they only cared for us in their womb because they wanted

71
00:05:52,240 --> 00:05:58,400
to, and it was all because of their desire to do so.

72
00:05:58,400 --> 00:06:04,080
And therefore we don't owe them anything, we never ask them to give birth to us.

73
00:06:04,080 --> 00:06:08,240
And even when we were growing up, we didn't ask them to take care of us.

74
00:06:08,240 --> 00:06:16,960
Of course, some people have the unfortunate, the misfortune to have bad parents who didn't

75
00:06:16,960 --> 00:06:21,480
look after their children, didn't perform the duties towards their children, didn't take

76
00:06:21,480 --> 00:06:25,160
care of them and maybe even abuse them.

77
00:06:25,160 --> 00:06:31,800
And so these people will argue, or have at least either argued directly or at the very

78
00:06:31,800 --> 00:06:38,880
least have a difficult time appreciating this teaching or figuring out how they themselves,

79
00:06:38,880 --> 00:06:44,480
because of their history, can put into practice this teacher.

80
00:06:44,480 --> 00:06:50,320
With all of these, we have to understand the situation that we find ourselves in scientifically

81
00:06:50,320 --> 00:06:57,480
speaking, it's correct to say that, or biologically speaking, that being, or human beings

82
00:06:57,480 --> 00:07:10,200
procreate based on, on desire, on instinct in the mind, and there's nothing really special

83
00:07:10,200 --> 00:07:15,280
about the relationship between a parent and a child, you know, if you break down the

84
00:07:15,280 --> 00:07:21,800
chemical composition of the parents and the child, and all beings, it's only the genes

85
00:07:21,800 --> 00:07:27,640
that get passed on by our parents, there's no, there's no part of our existence from

86
00:07:27,640 --> 00:07:37,440
a materialist point of view that gives us some responsibility towards our parents.

87
00:07:37,440 --> 00:07:40,760
This is from a materialist point of view, from an experiential point of view, we have

88
00:07:40,760 --> 00:07:50,160
to account for our birth, our conception in the womb of our parents, which from a point

89
00:07:50,160 --> 00:07:57,000
of view of experience is not at all an obvious outcome of the deeds in this life, it's

90
00:07:57,000 --> 00:08:05,440
not obvious to us how, I mean, our goals and our dreams were not preparing ourselves,

91
00:08:05,440 --> 00:08:12,200
but intentionally, to be born in a human womb, are very difficult to understand how it

92
00:08:12,200 --> 00:08:23,960
is that we come to be born in a womb again at all, and the answer to this question is

93
00:08:23,960 --> 00:08:29,840
a very important part of why we have such an important duty towards it, or why we have

94
00:08:29,840 --> 00:08:38,000
a very, such a profound relationship to our parents, even before arguing this, what we

95
00:08:38,000 --> 00:08:43,960
should point out is that it's clearly the case for a person who comes to practice meditation.

96
00:08:43,960 --> 00:08:49,120
This is why this one actually does have some relationship to meditation practice, and

97
00:08:49,120 --> 00:08:53,920
anyone who comes to practice meditation who does have a bad relationship towards their

98
00:08:53,920 --> 00:09:08,120
parents will be struck by the feelings of guilt and shame and even anger or sadness towards

99
00:09:08,120 --> 00:09:12,080
their parents if they've had a bad relationship, anyone who's had a very good relationship

100
00:09:12,080 --> 00:09:17,880
with their parents or who has had very loving and kind parents will come to feel great love

101
00:09:17,880 --> 00:09:28,360
and appreciation for their parents first and foremost, and it will really, for a new

102
00:09:28,360 --> 00:09:33,160
meditator, it would be quite a surprise that this should be the case because they didn't

103
00:09:33,160 --> 00:09:38,960
come with the intention of cultivating love towards their parents directly or for dealing

104
00:09:38,960 --> 00:09:45,400
with the issues involved with their parents directly, perhaps, but they will be shocked

105
00:09:45,400 --> 00:09:55,680
by how deep this relationship really goes.

106
00:09:55,680 --> 00:10:01,160
So it doesn't have to be argued that this relationship is an important one, it just has

107
00:10:01,160 --> 00:10:06,480
to be understood why it's an important one, from the point of view of meditator.

108
00:10:06,480 --> 00:10:11,240
It is important to argue it with people who have never practiced meditation, but perhaps

109
00:10:11,240 --> 00:10:16,800
the easiest way to argue it is to point to people who have meditator and explain that

110
00:10:16,800 --> 00:10:21,920
listen when you do take the time to look at your own mind and you're going to realize

111
00:10:21,920 --> 00:10:32,000
how important how it would have profound effect this relationship has on your mind, but

112
00:10:32,000 --> 00:10:37,000
just briefly to explain the theory behind why this occurs is it has to do with the theory

113
00:10:37,000 --> 00:10:41,240
or the fact of us being born as a human being.

114
00:10:41,240 --> 00:10:46,200
To be born as a human being, you can't just arise in a lotus flower, apparently in the

115
00:10:46,200 --> 00:10:51,440
Buddhist time, there are work cases of people who are just a rose spontaneously in a

116
00:10:51,440 --> 00:10:52,440
lotus flower.

117
00:10:52,440 --> 00:10:58,040
Yes, or stories or legends, whether they're true or not, I don't know, but what we do

118
00:10:58,040 --> 00:11:09,200
see, 99 out of 100 times or 99 million out of 100 million times or more is human being

119
00:11:09,200 --> 00:11:12,160
being born in a womb.

120
00:11:12,160 --> 00:11:15,440
And as I said from an experiential point of view, this doesn't really make a lot of

121
00:11:15,440 --> 00:11:16,440
sense.

122
00:11:16,440 --> 00:11:23,400
Why should we be born into the womb of another, from the point of view of experience?

123
00:11:23,400 --> 00:11:38,520
So the point of this is our minds and our lives are very much geared towards this reproductive

124
00:11:38,520 --> 00:11:39,520
cycle.

125
00:11:39,520 --> 00:11:43,720
This is why shortly after we're born, maybe five or ten years, we begin to have interest

126
00:11:43,720 --> 00:11:53,160
in the opposite gender in general or in an ordinary or for the most part, of course, homosexuality

127
00:11:53,160 --> 00:11:54,160
and so on.

128
00:11:54,160 --> 00:11:58,680
It is a part of the human statement.

129
00:11:58,680 --> 00:12:06,160
For the majority of humanity, there is this heterosexual reproductive desire, even even

130
00:12:06,160 --> 00:12:08,440
for people who are not attracted to the opposite gender.

131
00:12:08,440 --> 00:12:15,880
They will often arise, desire to have children, the male species, the male gender will often

132
00:12:15,880 --> 00:12:24,480
have desires to protect others and the female will often have desires to develop stability

133
00:12:24,480 --> 00:12:26,920
or a nest or a home or so.

134
00:12:26,920 --> 00:12:33,760
So in this way, we behave a lot like animals and the materialist scientists will explain

135
00:12:33,760 --> 00:12:38,760
this in terms of genes and so on.

136
00:12:38,760 --> 00:12:44,240
We explain it in terms of habits that are accumulated from life to life, so our experience

137
00:12:44,240 --> 00:12:45,240
cultivates habit.

138
00:12:45,240 --> 00:12:49,600
From an experiential point of view, looking at the human state, you have to say this is quite

139
00:12:49,600 --> 00:12:55,400
a intense, intensely contrived state.

140
00:12:55,400 --> 00:12:56,400
It's not at all obvious.

141
00:12:56,400 --> 00:13:01,160
You wouldn't think, well, if you have experience, if you start with just basic raw experience

142
00:13:01,160 --> 00:13:05,800
that somehow you're going to get beings being born inside of each other's bodies, that's

143
00:13:05,800 --> 00:13:08,280
quite a contrived state.

144
00:13:08,280 --> 00:13:18,680
And so our very existence, as in this state, relies and depends on two people, depends

145
00:13:18,680 --> 00:13:32,320
on our parents, and this is where our mind is at, if we have parents and they've taken

146
00:13:32,320 --> 00:13:40,840
care of us and so on, and we deny that, we reject that.

147
00:13:40,840 --> 00:13:49,400
We're basically rejecting the very foundation of our very beings, this brain, this body.

148
00:13:49,400 --> 00:13:55,000
This is the implication of what it means to inherit them from your parents, is that we

149
00:13:55,000 --> 00:13:59,840
have a very strong connection to these people.

150
00:13:59,840 --> 00:14:07,000
So this is just some ideas of where this comes from, that really our parents are very

151
00:14:07,000 --> 00:14:15,080
much a part of who we are, and we chose, really, to be born in their womb.

152
00:14:15,080 --> 00:14:25,520
We came through a cultivation of habits and relationships in past life.

153
00:14:25,520 --> 00:14:30,280
When you reject this, you reject harmony and stability, so what I would like to explain

154
00:14:30,280 --> 00:14:37,320
for many of these is whether or not you understand that it's truly wholesome or unwholesome

155
00:14:37,320 --> 00:14:43,720
to do either way, to ignore you, to leave behind your parents or to take care of them.

156
00:14:43,720 --> 00:14:48,120
When you live in the world, if you're going to have children of your own, if you want

157
00:14:48,120 --> 00:14:55,480
to live in the world and have a stable life, you have to play by the rules.

158
00:14:55,480 --> 00:15:03,440
If we become anarchist and reject societal norms of some sort, or the basic norms of what

159
00:15:03,440 --> 00:15:13,000
it means to be a human being, then all that we'll find is chaos and confusion, and many

160
00:15:13,000 --> 00:15:15,000
problems will arise.

161
00:15:15,000 --> 00:15:28,200
So some sort of system of stability has to be appreciated and has to be honored, and our

162
00:15:28,200 --> 00:15:31,840
the care that we take for our parents is a very important part of this, something that

163
00:15:31,840 --> 00:15:33,920
brings stability to the world.

164
00:15:33,920 --> 00:15:40,720
When people stop taking care of their parents, then of course the whole system breaks down.

165
00:15:40,720 --> 00:15:47,360
Students will begin to neglect their children, or neglect their children's education will

166
00:15:47,360 --> 00:15:53,280
not have any interest in taking care of their children and their cycle breaks down,

167
00:15:53,280 --> 00:15:55,360
and of course it goes either.

168
00:15:55,360 --> 00:16:00,120
Both ways our future, when we have children, they will not think to take care of us,

169
00:16:00,120 --> 00:16:06,960
having seen the example that we've said for them and so on.

170
00:16:06,960 --> 00:16:12,680
When society becomes quite upset, of course, then we stop respecting elders in general,

171
00:16:12,680 --> 00:16:15,680
and we stop respecting experience.

172
00:16:15,680 --> 00:16:20,240
Nowadays people will often think it's intelligence that is more important, and we'll

173
00:16:20,240 --> 00:16:25,680
laugh at our parents and their old ways and so on, and it's only when we get old ourselves

174
00:16:25,680 --> 00:16:32,040
that we realize how important wisdom and experience really are.

175
00:16:32,040 --> 00:16:39,800
So care for your parents is not only is it a very wholesome thing to do, to take care

176
00:16:39,800 --> 00:16:43,680
of them to be grateful, but it's also something that leads to stability and harmony in

177
00:16:43,680 --> 00:16:44,680
the world.

178
00:16:44,680 --> 00:16:47,320
On top of that, there is something to be said about gratitude.

179
00:16:47,320 --> 00:16:51,880
This is the argument I've had before, is people don't understand this, we're gratitude.

180
00:16:51,880 --> 00:16:56,280
We don't understand why they should be grateful towards others.

181
00:16:56,280 --> 00:17:04,840
In this sense, you have to just understand what gratitude is like, gratitude and ingratitude.

182
00:17:04,840 --> 00:17:07,480
In gratitude is a shriveling mind state.

183
00:17:07,480 --> 00:17:19,400
It's a mind state full of anger and aversion and discord, and it's a selfish mind state,

184
00:17:19,400 --> 00:17:24,000
because you have this logical one-to-one relationship where someone has done something

185
00:17:24,000 --> 00:17:28,080
for you, and you have the opportunity to do something for them.

186
00:17:28,080 --> 00:17:34,240
When you don't do that, you create friction, you create tension, and you create suffering,

187
00:17:34,240 --> 00:17:35,240
in your mind.

188
00:17:35,240 --> 00:17:40,360
This is something that you have to see, but meditators become more and more grateful as

189
00:17:40,360 --> 00:17:42,200
they practice.

190
00:17:42,200 --> 00:17:46,680
And so if we have gained whatever benefit we have gained from other people, we tend to

191
00:17:46,680 --> 00:17:57,000
be very keen to pay it back, even if the other people are in general mean and evil people.

192
00:17:57,000 --> 00:18:01,480
Even in that case, we think more and more about what the good that they may have done for

193
00:18:01,480 --> 00:18:04,680
us, and less and less about the bad things that they've done.

194
00:18:04,680 --> 00:18:10,480
So if your parents have done bad things for you, the meditator will still find themselves

195
00:18:10,480 --> 00:18:15,080
wishing and having great love towards their parents for whatever good that they didn't

196
00:18:15,080 --> 00:18:16,400
do for them.

197
00:18:16,400 --> 00:18:23,840
This is how the meditators' mind starts to incline, and inclines away from revengeful

198
00:18:23,840 --> 00:18:40,680
and reactionary mind states, inclines towards grateful and loving mind state.

199
00:18:40,680 --> 00:18:44,360
So this is actually quite important for meditators, and it's important for people thinking

200
00:18:44,360 --> 00:18:48,960
to come to practice meditation, who should realize that starting with their parents, they

201
00:18:48,960 --> 00:18:55,400
should clear up all their relationships, they should create a stable life where they are,

202
00:18:55,400 --> 00:19:03,440
at least in terms of settling all their affairs with all of their relationships.

203
00:19:03,440 --> 00:19:08,520
So if you have a problem with your parents, don't run away to come meditators won't work,

204
00:19:08,520 --> 00:19:12,840
many meditators run away for that reason.

205
00:19:12,840 --> 00:19:16,760
Tell your affairs first, and then come meditate so that it won't bother you on your practice

206
00:19:16,760 --> 00:19:18,160
on meditation.

207
00:19:18,160 --> 00:19:24,800
Now there is one sort of exception, I mean monks are not expected to have direct contact

208
00:19:24,800 --> 00:19:27,960
with their parents.

209
00:19:27,960 --> 00:19:34,200
We're not expecting to take care of them when they're old or so, and we're not expected

210
00:19:34,200 --> 00:19:37,040
to have much relationship with anyone at all.

211
00:19:37,040 --> 00:19:45,120
A monk can go off in the forest and leave the world behind.

212
00:19:45,120 --> 00:19:49,800
But even as monastics, we shouldn't take this too literally and think that if our parents

213
00:19:49,800 --> 00:19:58,040
are in need, we should avoid or avoid contact with such worldly people, even if our parents

214
00:19:58,040 --> 00:20:00,560
are worldly certain people.

215
00:20:00,560 --> 00:20:07,000
The Buddha was quite clear on this, and it's a good example of how rather than seeing

216
00:20:07,000 --> 00:20:13,960
relationships as an obstacle to our practice, seeing them as our practice.

217
00:20:13,960 --> 00:20:16,120
Monks are allowed to take care of their parents.

218
00:20:16,120 --> 00:20:25,760
There was a monk who was praised by the Buddha for taking care of his parents.

219
00:20:25,760 --> 00:20:29,920
So anyway, this is the first one, taking care of your parents, of course.

220
00:20:29,920 --> 00:20:34,120
What needs to be said, the core of this is blessing, this is just explaining why it's important.

221
00:20:34,120 --> 00:20:37,440
But the core of the blessing, what does it mean to take care of your parents?

222
00:20:37,440 --> 00:20:40,720
Doesn't necessarily mean that you have to give them medicine and you have to bathe them

223
00:20:40,720 --> 00:20:45,640
and clothe them or however when they get older or at any time.

224
00:20:45,640 --> 00:20:49,000
The Buddha said that in this way, you can never pay back your parents.

225
00:20:49,000 --> 00:20:53,920
If your parents have, even to the extent that your parents have given birth to you, the

226
00:20:53,920 --> 00:20:58,160
extent that they carry your mother carried you and your womb for nine months and your

227
00:20:58,160 --> 00:21:07,120
father was protecting her during that time and the love that they gave and the years and

228
00:21:07,120 --> 00:21:13,200
years that they put into raising you.

229
00:21:13,200 --> 00:21:15,840
The Buddha said it's very difficult to pay back such parents.

230
00:21:15,840 --> 00:21:24,760
Of course, if your father ran away after your mother got pregnant and your mother had no

231
00:21:24,760 --> 00:21:27,920
interest you and abandoned you and your borners on.

232
00:21:27,920 --> 00:21:33,040
So they're more difficult to see, even though we understand this to be karmic and it still

233
00:21:33,040 --> 00:21:40,480
points to the strength of the relationship.

234
00:21:40,480 --> 00:21:45,320
The Buddha said the only way to clear this up is with the dhamma.

235
00:21:45,320 --> 00:21:50,440
Even if your parents are mean and evil people, if you think that your parents have done

236
00:21:50,440 --> 00:22:00,400
nothing for you or so little or have done so much bad to you that the good that they've

237
00:22:00,400 --> 00:22:09,760
done is totally out of weight, even still the teaching applies to administer to them

238
00:22:09,760 --> 00:22:11,520
with the dhamma.

239
00:22:11,520 --> 00:22:15,240
If you still have a relationship with your parents or have an abusive relationship with

240
00:22:15,240 --> 00:22:20,080
anyone, a bad relationship, or if you have a good relationship with your parents have done

241
00:22:20,080 --> 00:22:25,320
great things and you feel somehow you want to pay them back, somehow you want to show

242
00:22:25,320 --> 00:22:28,440
your gratitude or you have the chance.

243
00:22:28,440 --> 00:22:34,480
The gratitude that we should show is in the dhamma that we should practice and we should

244
00:22:34,480 --> 00:22:42,320
teach and we should set an example and we should encourage them in the practice.

245
00:22:42,320 --> 00:22:48,360
We shouldn't be a support for them to find peace, happiness and freedom from suffering and

246
00:22:48,360 --> 00:22:50,680
not just in a material sense.

247
00:22:50,680 --> 00:22:55,720
Of course this goes with anyone, happiness in a material sense can easily be bought,

248
00:22:55,720 --> 00:22:59,920
can easily be found in the world at least for some time, but happiness in a spiritual

249
00:22:59,920 --> 00:23:06,800
sense takes much more, it takes wisdom, it takes mindfulness, it takes diligence, it

250
00:23:06,800 --> 00:23:11,600
takes patience, it takes content, it takes many qualities that can only be gained through

251
00:23:11,600 --> 00:23:12,920
the practice of meditation.

252
00:23:12,920 --> 00:23:17,640
So the best thing you can do for anyone, especially your parents, is to encourage them in

253
00:23:17,640 --> 00:23:23,000
the practice of meditation or help support them in the practice.

254
00:23:23,000 --> 00:23:28,360
So this is the first one, then we have in terms of just lump them all together because

255
00:23:28,360 --> 00:23:35,400
here we have in terms of one's spouse and children and relatives and the commentary even

256
00:23:35,400 --> 00:23:42,400
goes further and says this is even in terms of our employees and our employers and really

257
00:23:42,400 --> 00:23:48,000
everyone that we have relationship, relationships with, it could be our fellow employees,

258
00:23:48,000 --> 00:23:53,960
it could be fellow students who are studying, it could be fellow monastics in the world.

259
00:23:53,960 --> 00:24:03,480
This harmony in the community and on the one hand you, many of these as I said are not

260
00:24:03,480 --> 00:24:11,080
directly related to meditation, but if you look at it another way, these are really an

261
00:24:11,080 --> 00:24:21,000
indication of the mental development of the individuals.

262
00:24:21,000 --> 00:24:31,520
If someone doesn't have good relationships with other people then you might want to assume

263
00:24:31,520 --> 00:24:39,840
that that person has not a very good meditation practice, if someone is constantly causing

264
00:24:39,840 --> 00:24:52,080
friction and strife in their family arguing and bickering and backbiting and so on and jealous

265
00:24:52,080 --> 00:24:57,920
and stingy and so on.

266
00:24:57,920 --> 00:25:03,000
With their relatives, husbands who fight with wives and parents who fight with children

267
00:25:03,000 --> 00:25:11,320
and employers who scold treat their employees poorly, employees who cheat their employers,

268
00:25:11,320 --> 00:25:18,120
friends who backbite and gossip and so on, all of this is because of a lack of mental

269
00:25:18,120 --> 00:25:27,640
development and so the important point I want to make is that we can't separate our meditation

270
00:25:27,640 --> 00:25:35,760
practice from our life, a beginner meditator will often want to discard the whole world

271
00:25:35,760 --> 00:25:40,200
and give up all of their relationships so when a person comes to bother them in their

272
00:25:40,200 --> 00:25:43,560
meditation they might even yell at them or scold them.

273
00:25:43,560 --> 00:25:50,600
I'm trying to meditate, why are you bothering me?

274
00:25:50,600 --> 00:25:54,480
And this can go on for some time until they finally realize that relationships as I said

275
00:25:54,480 --> 00:25:59,600
are not, should not be seen as an obstacle to the practice, they should be seen as the

276
00:25:59,600 --> 00:26:00,600
practice.

277
00:26:00,600 --> 00:26:05,360
When someone comes to you, the Buddha was clear about this, he said, when people come to

278
00:26:05,360 --> 00:26:11,200
me, I think, what can I do to teach them that will make them go away the quickest?

279
00:26:11,200 --> 00:26:16,160
And so when we hear this, we think, well, just tell them to go away, you know?

280
00:26:16,160 --> 00:26:20,400
But it actually doesn't work that way.

281
00:26:20,400 --> 00:26:22,080
We've all tried that.

282
00:26:22,080 --> 00:26:28,920
It's meditators, we've all tried to push people away and we think, well, that's the way.

283
00:26:28,920 --> 00:26:29,920
Get rid of them, tell them.

284
00:26:29,920 --> 00:26:36,880
Look, if you don't like meditating, I don't want to have anything to do with you.

285
00:26:36,880 --> 00:26:39,400
But relationships have to be solved.

286
00:26:39,400 --> 00:26:45,080
They're not, the fact that there is a relationship with parents or relatives or anyone

287
00:26:45,080 --> 00:26:50,720
is because of some past karma that we have with them.

288
00:26:50,720 --> 00:26:54,440
So, well, it might be possible for us to go off in the forest and send loving kindness

289
00:26:54,440 --> 00:26:58,400
and never be bothered by anyone.

290
00:26:58,400 --> 00:27:03,560
If it happens that in the course of our quest to find such peace, we are bothered by other

291
00:27:03,560 --> 00:27:04,560
people.

292
00:27:04,560 --> 00:27:12,320
Then we have to treat them with dignity and respect and give them the time and the support

293
00:27:12,320 --> 00:27:13,320
that they need.

294
00:27:13,320 --> 00:27:15,840
Otherwise, they'll never leave us.

295
00:27:15,840 --> 00:27:18,640
I've even tried this with evil people.

296
00:27:18,640 --> 00:27:23,520
It was one monk who was always causing problems and I tried to just shut him up and it

297
00:27:23,520 --> 00:27:29,760
just fueled the fire and the more, it's like in India when you go to the boat guy or you

298
00:27:29,760 --> 00:27:33,280
go to these Buddhist places and the beggars come.

299
00:27:33,280 --> 00:27:37,120
And the worst thing you can do besides giving them something is to say no.

300
00:27:37,120 --> 00:27:44,200
If you give something, you get a thousand of them swarming and then you're in real trouble.

301
00:27:44,200 --> 00:27:47,720
But if you say no, no, no, then they continue to pester you.

302
00:27:47,720 --> 00:27:54,480
This is the way of people, it's only by being patient and kind and so in India the

303
00:27:54,480 --> 00:27:58,280
example of India is a good one I think because there's such greed in these, even these

304
00:27:58,280 --> 00:28:02,840
little kids who are actually not, they're not without, they're going to school and they

305
00:28:02,840 --> 00:28:05,560
have a little bit of money I think.

306
00:28:05,560 --> 00:28:11,800
But they found how easy it is to get money from from from from from dumb tourists.

307
00:28:11,800 --> 00:28:16,920
So they come up and they ask you for money and money and many people just say no, no,

308
00:28:16,920 --> 00:28:21,800
then they never stop because for them it's a game or else it's serious and they really

309
00:28:21,800 --> 00:28:22,800
do need the money.

310
00:28:22,800 --> 00:28:26,560
But what I found was that when you just give them a big hug and say, what's your name?

311
00:28:26,560 --> 00:28:31,040
Learn a little bit of their language even and suddenly they're a totally different person.

312
00:28:31,040 --> 00:28:35,160
And they say what their name is and you ask them what they're studying in school and so

313
00:28:35,160 --> 00:28:40,240
and then they go away and you like you untied the knot and there was never a problem

314
00:28:40,240 --> 00:28:41,240
in the first place.

315
00:28:41,240 --> 00:28:48,000
Like I talk about Aikido or Jiu-Jitsu or these kind of ways of redirecting energy.

316
00:28:48,000 --> 00:28:54,880
Anyway, relationships are an important part of our practice and we all have them even as

317
00:28:54,880 --> 00:28:55,880
monastics.

318
00:28:55,880 --> 00:29:00,280
At the very least we have a relationship with our teachers or with our students and these

319
00:29:00,280 --> 00:29:16,400
relationships are very important and should be treated as.

