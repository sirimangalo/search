1
00:00:00,000 --> 00:00:02,040
I live in Brooklyn, in Canada and I didn't see that there was no

2
00:00:02,680 --> 00:00:06,100
I realized that it may be 55.

3
00:00:08,200 --> 00:00:16,900
In the same cycle, such as

4
00:00:19,920 --> 00:00:22,960
though we decided we were payable

5
00:00:24,260 --> 00:00:27,440
I said it was ridiculous

6
00:00:27,440 --> 00:00:45,440
It is about the books through which we understand the Dhamma in theory.

7
00:00:45,440 --> 00:01:02,440
Buddha taught for 45 years and whatever he taught during these 45 years,

8
00:01:02,440 --> 00:01:21,440
the venerable Ananda and other disciples learnt them and keep these teachings with them.

9
00:01:21,440 --> 00:01:35,440
Buddha passed away in 544 BC and just about three months after his passing away,

10
00:01:35,440 --> 00:01:42,440
the first Buddhist council was held in the city of Rajagaha.

11
00:01:42,440 --> 00:01:52,440
And the venerable Mahakasabha acted as the president of that council.

12
00:01:52,440 --> 00:02:00,440
So at that council, all the teachings of the Buddha were collected.

13
00:02:00,440 --> 00:02:14,440
And the venerable Ananda, who was the Buddha's personal attendant for 25 years, presented the Dhamma.

14
00:02:14,440 --> 00:02:23,440
That means the discourses and the abitama to be assembly.

15
00:02:23,440 --> 00:02:43,440
And the venerable Upali, who was appointed the foremost among those who knew Vinaya or disciplinary rules, presented the Vinaya.

16
00:02:43,440 --> 00:02:49,440
Then whatever they presented, the assembly scrutinized.

17
00:02:49,440 --> 00:02:57,440
And many questions were put to them regarding the authenticity of these teachings.

18
00:02:57,440 --> 00:03:15,440
And only when all the participants satisfied that a certain piece of teaching was authentic teaching of the Buddha did the accept them.

19
00:03:15,440 --> 00:03:22,440
And then they recorded them by word of mouth.

20
00:03:22,440 --> 00:03:38,440
So when they were satisfied about a teaching, for example, a discourse, then they would recite that in unison.

21
00:03:38,440 --> 00:03:51,440
So that is why that action of recording the Buddha's teachings was known as Sangayana.

22
00:03:51,440 --> 00:04:01,440
Now Sangayana means reciting together.

23
00:04:01,440 --> 00:04:13,440
All the teachings of the Buddha recorded at the first Buddhist council were classified into three divisions.

24
00:04:13,440 --> 00:04:17,440
And these divisions are called Pitakas.

25
00:04:17,440 --> 00:04:30,440
Now, the Pali word Pitakas means what is to be learned or just teaching.

26
00:04:30,440 --> 00:04:36,440
And also it means a receptacle, a basket.

27
00:04:36,440 --> 00:04:46,440
So nowadays people use the meaning basket for Pitakas because I think it is easier to understand, say, three baskets.

28
00:04:46,440 --> 00:04:55,440
Actually, three divisions of his teachings which were to be learned.

29
00:04:55,440 --> 00:05:00,440
And the three divisions are Juan Vinayana.

30
00:05:00,440 --> 00:05:08,440
And two Sutta also Tanda and three Abitama.

31
00:05:08,440 --> 00:05:21,440
Vinayana, Pitaka, contains rules and regulations for monks, nuns, and novices.

32
00:05:21,440 --> 00:05:36,440
And the second division, the Sutta, or Sutanta, Pitaka, contains the discourses given by the Buddha to both monks and non-months.

33
00:05:36,440 --> 00:05:45,440
I use non-munks because they include they gave us a celestial beings also.

34
00:05:45,440 --> 00:05:49,440
And that division was called Abitama Pitaka.

35
00:05:49,440 --> 00:06:03,440
And this consists of analytical expositions of many types of what had been taught in the second division.

36
00:06:03,440 --> 00:06:22,440
So, in Abitama you don't find new subjects, but the subjects that are taught in the Sutta Pitaka.

37
00:06:22,440 --> 00:06:42,440
But the treatment is very different in the Abitama Pitaka, and the topics taught in the Sutta Pitaka were analyzed in a great detail.

38
00:06:42,440 --> 00:06:59,440
Among the three Pitakas, the second one, Sutta or Sutanta Pitaka is most popular because it is for both monks and lay people.

39
00:06:59,440 --> 00:07:12,440
And also since it is taught in the conventional language, it is easier to understand.

40
00:07:12,440 --> 00:07:37,440
And another division of the Buddha's teachings is the Nikaya division.

41
00:07:37,440 --> 00:07:55,440
Among the discourses called the Ghanikaya in Bali, and two collection of middle length discourses, Majima Nikaya, and three collection of associated discourses called Sanyota Nikaya.

42
00:07:55,440 --> 00:08:06,440
And then the collection of discourses whose topics gradually increase in number in succeeding groups of discourses.

43
00:08:06,440 --> 00:08:16,440
That means in book one, only one subject, one topic is treated in each discourse.

44
00:08:16,440 --> 00:08:31,440
But in the second book, two topics were treated in each discourse, and book three topics and so on until the eleventh book.

45
00:08:31,440 --> 00:08:46,440
And then the discourses called Angutara Nikaya, and five is Kudaka Nikaya, collection of small discourses.

46
00:08:46,440 --> 00:08:57,440
I want you to understand these two kinds of divisions correctly and clearly.

47
00:08:57,440 --> 00:09:07,440
Buddha's teachings are divided into three Pitakas, or they are divided into five Nikayas.

48
00:09:07,440 --> 00:09:16,440
So, Nikaya division is not a subdivision of Sutta Pitaka.

49
00:09:16,440 --> 00:09:28,440
That is important because most people think that Nikaya division is a subdivision of Sutta Pitaka.

50
00:09:28,440 --> 00:09:37,440
But that is not true according to the ancient commentaries.

51
00:09:37,440 --> 00:09:48,440
We near Pitaka is included in the Kudaka Nikaya.

52
00:09:48,440 --> 00:09:56,440
And Sutta Pitaka also under Pitaka is included in all five Nikayas.

53
00:09:56,440 --> 00:10:03,440
And Abirma Pitaka is again included in Kudaka Nikaya.

54
00:10:03,440 --> 00:10:08,440
This is from the point of view of Pitaka's.

55
00:10:08,440 --> 00:10:25,440
From the point of view of Nikayas, then Dikandikaya is included in Sutta Pitaka, Major Manicaya, Sanyu Danyicaya, Angutra Nikaya are also included in Sutta Pitaka.

56
00:10:25,440 --> 00:10:42,440
Then all of we near Pitaka, all of Abirama Pitaka, and some portions of Sutta and Abiraka are included in Kudaka Nikaya.

57
00:10:42,440 --> 00:10:56,440
So, a book can have two names according to Pitaka and Nikayas.

58
00:10:56,440 --> 00:11:02,440
Now, the first book of there are five volumes in the Winnia Pitaka.

59
00:11:02,440 --> 00:11:09,440
Now, the first book of Winnia can be called Winnia Pitaka according to Pitaka division.

60
00:11:09,440 --> 00:11:14,440
And Kudaka Nikayas according to Nikayas division.

61
00:11:14,440 --> 00:11:23,440
So, in the same way every book belongs to Pitaka or Nikayas.

62
00:11:23,440 --> 00:11:51,440
Let this information from the ancient commentary written by the venerable Buddha Gosa, especially in the commentary to Winnia, in the introductory chapter of the commentary to Winnia.

63
00:11:51,440 --> 00:12:19,440
The first three Buddhist councils were described and also the division of Buddhas teachings into one, two, three, one, two, three, five, nine, and eighty-four thousand units of Dhamma.

64
00:12:19,440 --> 00:12:38,440
And also, in that portion of the commentary to Winnia, the spread of Buddhas dispensation to Sri Lanka was also given.

65
00:12:38,440 --> 00:13:00,440
So, we know much about the three Buddhist councils and how the Buddha's teachings were carried to the island of Sri Lanka and the King and the people who are converted to Buddhism.

66
00:13:00,440 --> 00:13:09,440
So, many details we learn from the commentary to Winnia.

67
00:13:09,440 --> 00:13:23,440
So, when describing the Kudaka Nikayas there, the venerable Buddha Gosa set, what is the Kudaka Nikayas?

68
00:13:23,440 --> 00:13:41,440
The rest of the world of the Buddha, including the entire Winnia Bittaka, the Abitama Bittaka and the 15 Divisions commencing with Kudaka Bata and you merited earlier, leaving aside the four Nikayas.

69
00:13:41,440 --> 00:14:00,440
So, about from four Nikayas, the Kudaka Nikayas consist of the whole Winnia Bittaka, the whole Abitama Bittaka and 15 books of Kudaka Nikayas beginning with Kudaka Bata.

70
00:14:00,440 --> 00:14:12,440
And also, he wrote it in a verse saying, the rest of the world of the Buddha, excluding these four Nikayas as Diga, is considered the Kudaka Nikayas.

71
00:14:12,440 --> 00:14:29,440
So, it is very clear that the Nikaya division is a separate division of the Buddha's teachings and not a subdivision of Sotanta Bittaka.

72
00:14:29,440 --> 00:14:38,440
Now, there is one thing strange in the Nikaya division and that is the last one, the Kudaka Nikayas.

73
00:14:38,440 --> 00:14:43,440
The meaning of Kudaka is small.

74
00:14:43,440 --> 00:14:51,440
So, Kudaka Nikayas literally means collection of small discourses.

75
00:14:51,440 --> 00:15:07,440
But, the whole Winnia Bittaka, five volumes and the whole of Abitama Bittaka, 12 volumes, are included in Kudaka Nikayas, small collection of small discourses.

76
00:15:07,440 --> 00:15:17,440
They are not small, Winnia Bittaka consists of five volumes, about say 500 pages each and so on.

77
00:15:17,440 --> 00:15:31,440
It is strange, and the Kudaka Bittaka was a small collection.

78
00:15:31,440 --> 00:16:00,440
But, one commentary, a new sub-commentary to Diga Nikayas, written by a Burmese Sayadha in the ancient days, stated that when the commentators describe that the Diga Nikayas and so on, they use the phrase,

79
00:16:00,440 --> 00:16:03,440
use those of long measure.

80
00:16:03,440 --> 00:16:10,440
So, discourses of long measure, discourses of middle or medium measure and so on.

81
00:16:10,440 --> 00:16:21,440
But, when they describe the Kudaka Nikayas, they did not use the phrase of small measure.

82
00:16:21,440 --> 00:16:38,440
So, that commented a part that in the name Kudaka Nikayas, we are not to take Kudaka literally.

83
00:16:38,440 --> 00:16:52,440
So, it is just a name for the collection of teachings that are not included in the four Nikayas.

84
00:16:52,440 --> 00:17:07,440
So, that is why the whole of Winnia Bittaka and the whole of Abitama Bittaka are included in the Kudaka Nikayas collection of small discourses.

85
00:17:07,440 --> 00:17:19,440
And Buddha's teachings can be divided into other divisions as well.

86
00:17:19,440 --> 00:17:30,440
And so, the commentators explained that first, Buddha's words or Buddha's teachings are just one.

87
00:17:30,440 --> 00:17:38,440
And one means it has only one taste, the taste of freedom.

88
00:17:38,440 --> 00:17:55,440
Now, wherever in the ocean, you pick up the water and taste it, you get the salty taste only.

89
00:17:55,440 --> 00:18:01,440
So, there is only one taste of the ocean and that is salt, salty taste.

90
00:18:01,440 --> 00:18:16,440
In the same way, you can read anywhere in the texts and you get the taste of freedom, taste of freedom from mental departments.

91
00:18:16,440 --> 00:18:29,440
So, in that sense, Buddha's words are just one, there are no divisions of the Buddha's teachings according to the taste they contain.

92
00:18:29,440 --> 00:18:40,440
So, there is only one kind of Buddha's teachings which have the taste of freedom.

93
00:18:40,440 --> 00:18:51,440
But, we can divide the Buddha's teachings into true divisions and they are Dhamma and Winnia.

94
00:18:51,440 --> 00:19:01,440
Sometimes, all the teachings of the Buddha or the dispensation of the Buddha are called Dhamma and Winnia.

95
00:19:01,440 --> 00:19:11,440
So, in that case, Dhamma means the Sotanta, Bittaka and Bittaka and Winnia means Winnia, Bittaka.

96
00:19:11,440 --> 00:19:26,440
So, in that case, Buddha's teachings are twofold, Dhamma and Winnia.

97
00:19:26,440 --> 00:19:32,440
So, Buddha's teachings can also be divided into three by way of utterances.

98
00:19:32,440 --> 00:19:41,440
That means the first utterance, the middle utterance and the last utterance.

99
00:19:41,440 --> 00:19:58,440
Now, the first utterance is said to be the verses of joy, Buddha uttered immediately after his enlightenment.

100
00:19:58,440 --> 00:20:13,440
So, those are said to be the first words of the Buddha.

101
00:20:13,440 --> 00:20:38,440
The Buddha admonished his disciples, then Buddha said, I call upon you monks, all conditions, things are subject to the solution.

102
00:20:38,440 --> 00:20:45,440
And, accomplish your training with mindfulness.

103
00:20:45,440 --> 00:20:56,440
So, these are the last words of the Buddha, utterance just before his passing away.

104
00:20:56,440 --> 00:21:06,440
And, those utterances which he gave between these two moments, immediately after the enlightenment,

105
00:21:06,440 --> 00:21:09,440
and immediately before his passing away.

106
00:21:09,440 --> 00:21:17,440
That means during 45 years, those utterances are called middle utterances.

107
00:21:17,440 --> 00:21:28,440
So, by way of utterances, Buddha's teachings can be divided into three.

108
00:21:28,440 --> 00:21:39,440
Again, Buddha's teachings are divided into three pizzas, or they are divided into five Nicayas or collections.

109
00:21:39,440 --> 00:21:51,440
And, also, Buddha's teachings can be divided into nine components in Polybia, Kanga.

110
00:21:51,440 --> 00:22:01,440
So, in that case, there are nine divisions of the Buddha's teachings, and they are Suta, Gia, and so on.

111
00:22:01,440 --> 00:22:08,440
Now, it is difficult to explain all these nine divisions.

112
00:22:08,440 --> 00:22:19,440
And, also, it is involved, so, I will skip the explanation.

113
00:22:19,440 --> 00:22:28,440
And, also, Buddha's teachings are divided into 84,000 by way of units of Dhamma.

114
00:22:28,440 --> 00:22:37,440
And, here also, it is not easy to explain how we decide a unit of Dhamma.

115
00:22:37,440 --> 00:22:45,440
Now, for example, the full of Mahasadeepatana-soda is said to be just one unit,

116
00:22:45,440 --> 00:22:56,440
because it is taught on one occasion only.

117
00:22:56,440 --> 00:23:07,440
But, the Mahaparini-bana-soda, the Soda on the passing way of the Buddha,

118
00:23:07,440 --> 00:23:29,440
it consists of many units of the Dhamma, because it is like the accounts on different occasions connected together.

119
00:23:29,440 --> 00:23:48,440
Although, the Mahaparini-bana-soda is not very, very long, it contains many units of Dhamma,

120
00:23:48,440 --> 00:23:52,440
but the Mahasadeepatana-soda is only one unit.

121
00:23:52,440 --> 00:24:07,440
So, in like that, the units of Dhamma are a certain, but it is very difficult to really calculate

122
00:24:07,440 --> 00:24:18,440
the how many units are in Vigna-bittika, how many units are in certain Abitika and so on.

123
00:24:18,440 --> 00:24:27,440
I hope you have heard about King Asoka.

124
00:24:27,440 --> 00:24:42,440
King Asoka became a Buddhist, and he was so devoted to the dispensation of the Buddha,

125
00:24:42,440 --> 00:24:57,440
that he asked how many units of Dhamma were there, and the Arant elders answered that there are 84,000 units of Dhamma.

126
00:24:57,440 --> 00:25:09,440
Then he said, I will honor each unit of Dhamma with one monastery.

127
00:25:09,440 --> 00:25:19,440
And so, according to the account given in the commentary to the Vigna-bittika,

128
00:25:19,440 --> 00:25:26,440
he built 84,000 monasteries in India.

129
00:25:26,440 --> 00:25:44,440
And in Dhamma, we say not only 84,000 monasteries, but also 84,000 bakodas, 84,000 tanks, 84,000 wells and so on.

130
00:25:44,440 --> 00:25:51,440
I will go back to the three Pittikas.

131
00:25:51,440 --> 00:25:59,440
Now, Vigna-bittika is also called ana-disana.

132
00:25:59,440 --> 00:26:03,440
That means teaching of authority.

133
00:26:03,440 --> 00:26:11,440
Because in the Vigna-bittika, Buddha laid down rule after rule for monks.

134
00:26:11,440 --> 00:26:18,440
So Buddha had the authority to lay down these rules.

135
00:26:18,440 --> 00:26:26,440
And so Vigna-bittika is also known as ana-disana, the teaching of authority.

136
00:26:26,440 --> 00:26:32,440
The Vigna-bittika is also called wahara-disana.

137
00:26:32,440 --> 00:26:35,440
Wahara means conventional language.

138
00:26:35,440 --> 00:26:41,440
So, so the Vigna-bittika is a teaching by conventional language.

139
00:26:41,440 --> 00:26:58,440
So, in the Sota, the Vigna-bittika, Buddha used conventional terms like being a person and individual, a man, a woman and so on.

140
00:26:58,440 --> 00:27:07,440
Bittama-bittika is also called paramata-disana, teaching of ultimate truths.

141
00:27:07,440 --> 00:27:14,440
Now, in the Abirama-bittika, conventional language was not used.

142
00:27:14,440 --> 00:27:21,440
But here, the terms used are of the ultimate truths.

143
00:27:21,440 --> 00:27:34,440
Like five aggregates, 12 bases, 18 elements, four noble truths and so on.

144
00:27:34,440 --> 00:27:38,440
The Vigna-bittika is like a law book.

145
00:27:38,440 --> 00:27:44,440
And the Vigna-bittika is like a textbook of physics, for example.

146
00:27:44,440 --> 00:27:49,440
And so, the Vigna-bittika is like a book on general subject.

147
00:27:49,440 --> 00:27:55,440
So, that is why Sota-bittika is popular with all people.

148
00:27:55,440 --> 00:28:03,440
Again, Vigna-bittika is described as yataparada-sasana.

149
00:28:03,440 --> 00:28:07,440
And in teaching according to offenses.

150
00:28:07,440 --> 00:28:11,440
If you do this thing, you come to this offense.

151
00:28:11,440 --> 00:28:22,440
And if you have this offense, you must try to get out of this offense by doing such and such things like that.

152
00:28:22,440 --> 00:28:29,440
So, it is called teaching according to offenses.

153
00:28:29,440 --> 00:28:36,440
Vigna-bittika is also called yatapar-nu-loma-sasana.

154
00:28:36,440 --> 00:28:41,440
Teaching according to dispositions and so on.

155
00:28:41,440 --> 00:28:58,440
That means, when Buddha thought this sort of discourses, he thought according to the dispositions or lights or dislikes of his listeners.

156
00:28:58,440 --> 00:29:07,440
That is why his teachings are varied in character.

157
00:29:07,440 --> 00:29:12,440
Sometimes, he said there are only two things, Vigna-bittika.

158
00:29:12,440 --> 00:29:15,440
Sometimes, he taught that there are five aggregates.

159
00:29:15,440 --> 00:29:21,440
Again, he taught that there are 12 bases and so on.

160
00:29:21,440 --> 00:29:28,440
So, one and the same thing is taught as having different names.

161
00:29:28,440 --> 00:29:35,440
And that was because some people are familiar with a particular kind of terminology.

162
00:29:35,440 --> 00:29:44,440
And for that person, he used the terminology that would be easily understood by that person.

163
00:29:44,440 --> 00:29:52,440
And for another person, he used another terminology to suit the understanding of that person.

164
00:29:52,440 --> 00:30:13,440
So, in the certain dapitika, the teaching is according to the dispositions or the inner thoughts of our lights and dislikes of his listeners.

165
00:30:13,440 --> 00:30:22,440
Vigna-bittika is also described as yatadhamma-sasana.

166
00:30:22,440 --> 00:30:24,440
Now, dhamma-hia means reality.

167
00:30:24,440 --> 00:30:28,440
So, Vigna-bittika is taught according to reality.

168
00:30:28,440 --> 00:30:42,440
So, when Buddha taught Vigna-bittama, he taught as it should be taught and he did not look for...

169
00:30:42,440 --> 00:30:48,440
You know, what the audience will like or will not like and so on.

170
00:30:48,440 --> 00:31:02,440
So, Vigna-bittika is the presentation according to reality.

171
00:31:02,440 --> 00:31:12,440
And Vigna-bittika is also called Sanvara, Sanvara-kata. That means teaching of different restraints.

172
00:31:12,440 --> 00:31:16,440
There are small restraints and there are big restraints.

173
00:31:16,440 --> 00:31:27,440
And Vigna-bittika is just restraint because the roots, if you read the roots, you will understand this.

174
00:31:27,440 --> 00:31:30,440
Say, you are not to do this, you are not to do that and so on.

175
00:31:30,440 --> 00:31:37,440
So, the restraint is taught in the Vigna-bittika.

176
00:31:37,440 --> 00:31:46,440
So, Vigna-bittika is also called Sanvara, Sanvara-kata.

177
00:31:46,440 --> 00:31:52,440
Restraint means restraint of bodily actions and speech.

178
00:31:52,440 --> 00:32:05,440
So, restraint is to prevent breaking the roots, to prevent transgression.

179
00:32:05,440 --> 00:32:21,440
And the Vigna-bittika is for the feeler of monks.

180
00:32:21,440 --> 00:32:26,440
And so, it involves restraint.

181
00:32:26,440 --> 00:32:35,440
And restraint is said to be the opposite of transgression of breaking the roots.

182
00:32:35,440 --> 00:32:43,440
Vigna-bittika is also called, oh, Sanvara-bittika is also called, we need wheat and nakata.

183
00:32:43,440 --> 00:32:52,440
That means, the discourse of refutation of wrong views.

184
00:32:52,440 --> 00:33:02,440
Now, in the very first book of diganyigaya, Buddha mentioned 62 kinds of wrong views.

185
00:33:02,440 --> 00:33:10,440
And since they are wrong views, they are all to be refuted or they are all to be discarded.

186
00:33:10,440 --> 00:33:21,440
So, certain diganyigaya is called, discourse of refuting wrong views.

187
00:33:21,440 --> 00:33:41,440
Vigna-bittika is also known as Nama-rupa-bri-kittaka, discourse of defining mind and matter.

188
00:33:41,440 --> 00:33:54,440
A thing in the practice of meditation, and it helps the meditator.

189
00:33:54,440 --> 00:34:05,440
I mean, the defining of mind and matter helps a meditator to get rid of defilements.

190
00:34:05,440 --> 00:34:15,440
So, a bit of a bittika is known as discourse of defining nama, I mean, mind and matter.

191
00:34:15,440 --> 00:34:21,440
So, when you understand a bit of a, you know what is mind, what is matter.

192
00:34:21,440 --> 00:34:34,440
And that there are only two things, mind and matter, and no other thing, like soul or self, or being represented and so on.

193
00:34:34,440 --> 00:34:48,440
And Vigna-bittika, the training of higher morality is taught.

194
00:34:48,440 --> 00:34:58,440
Now, the precepts kept by monks are called higher morality or higher sila.

195
00:34:58,440 --> 00:35:05,440
They are higher than the sila, practiced by lay people.

196
00:35:05,440 --> 00:35:26,440
Now, lay people keep five precepts, or eight precepts, or at most ten precepts, but monks have to keep 227 basic rules.

197
00:35:26,440 --> 00:35:31,440
And there are many more rules to be kept.

198
00:35:31,440 --> 00:35:42,440
And so, the precepts or rules kept by the monks are called a training of higher morality.

199
00:35:42,440 --> 00:35:49,440
So, in the Vigna-bittika, the training of higher morality is taught.

200
00:35:49,440 --> 00:36:04,440
So, during the Vittika, the training of higher consciousness or higher concentration is taught.

201
00:36:04,440 --> 00:36:16,440
Higher concentration means the eight kinds of genres.

202
00:36:16,440 --> 00:36:21,440
Vittama-bittika, the training of higher wisdom is taught.

203
00:36:21,440 --> 00:36:34,440
Now, higher wisdom means we must not acknowledge and path knowledge and fruition knowledge.

204
00:36:34,440 --> 00:36:51,440
Yeah, when we say in Vigna-bittika, the training of higher morality is taught, we must understand that the training of higher morality is taught predominantly.

205
00:36:51,440 --> 00:37:01,440
That means the other trainings are also taught, but the emphasis is on the training of higher morality.

206
00:37:01,440 --> 00:37:13,440
And in the Vittika, the training of higher consciousness is predominantly taught, and in the Vittika, the training of higher wisdom is predominantly taught.

207
00:37:13,440 --> 00:37:28,440
In Vigna-bittika, the avoidance of transgression is taught, because Vigna-bittika always prevents monks from transgression.

208
00:37:28,440 --> 00:37:39,440
And in the certain Vittika, the avoidance of the coming-up of Mando-deafilemen is taught.

209
00:37:39,440 --> 00:37:51,440
And in Vittama-bittika, the avoidance of latent defilemen is taught.

210
00:37:51,440 --> 00:38:14,440
They are stated in this way because Silla, Silla can prevent transgression, and somebody can prevent the arising or coming-up of Mando-deafilemen.

211
00:38:14,440 --> 00:38:28,440
And Panya can eradicate the latent tendencies of latent mental defilements.

212
00:38:28,440 --> 00:38:50,440
Vigna-bittika, the momentary abandonment of Mando-deafilemen is taught, and in certain Vittika, the abandonment of Mando-deafilemen is taught.

213
00:38:50,440 --> 00:39:06,440
In the Vittama-bittika, the total abandonment, like cutting of a tree, so total abandonment of Mando-deafilemen is taught.

214
00:39:06,440 --> 00:39:16,440
You practice Silla, you can prevent Mando-deafilements only when you are practicing, and the next movement, they may come back.

215
00:39:16,440 --> 00:39:26,440
But when you get Janas, then you can prevent mental defilements from arising for some longer time.

216
00:39:26,440 --> 00:39:34,440
Then when you reach enlightenment, you are able to eradicate all mental defilements.

217
00:39:34,440 --> 00:39:57,440
So that is why in Vigna-bittika, the avoidance of the momentary abandonment is taught, and in Sautanda-bittika, the temporary abandonment is taught, and in Vittama-bittika, the total abandonment is taught.

218
00:39:57,440 --> 00:40:07,440
In Vittama-bittika, the abandonment of defilements of misdeeds is taught.

219
00:40:07,440 --> 00:40:19,440
That means when you keep precepts, you avoid misdeeds, you do not do any wrongdoing.

220
00:40:19,440 --> 00:40:36,440
So in Vigna-bittika, the avoidance of any wrongdoing is taught, and in Sautanda-bittika, the avoidance of abandonment of craving is taught.

221
00:40:36,440 --> 00:40:58,440
In the Vigna-bittika, the avoidance of defilement of wrong view is taught, because when you study a Vigna-bittama, you come to realize that there are only mind and matter, or only five aggregates and so on.

222
00:40:58,440 --> 00:41:12,440
And so over and above, mind and matter, there is nothing that can be called a permanent entity, or a Italian so on.

223
00:41:12,440 --> 00:41:33,440
Because it is a course, like a study course, I want it to be complete, so I give all information about the Vittika as we find in the commentaries.

224
00:41:33,440 --> 00:41:44,440
But do not worry if you do not remember all of them, even monks are difficulty in remembering all of them.

225
00:41:44,440 --> 00:41:51,440
So now we will go to the Buddhist councils.

226
00:41:51,440 --> 00:42:13,440
If we talk about Buddhist councils, we should understand that besides the T-bittika, there are the commentaries and sub commentaries in Theravada Buddhist literature.

227
00:42:13,440 --> 00:42:36,440
Now, commentaries are those that explain the texts, because sometimes the texts themselves may not be quite understandable, because we are far removed from the time of the Buddha.

228
00:42:36,440 --> 00:42:46,440
And so we need the help of a teacher or an explanation like that.

229
00:42:46,440 --> 00:42:52,440
So there came to be commentaries.

230
00:42:52,440 --> 00:43:00,440
And then as the time went on, even these commentaries became difficult to understand.

231
00:43:00,440 --> 00:43:06,440
And so sub commentaries have to be written on these commentaries.

232
00:43:06,440 --> 00:43:21,440
And so there are three levels of Buddhist books, the texts, or the teachings of the Buddha, and the commentaries, the explanation of the teachings, and then sub commentaries, the explanation of the commentaries.

233
00:43:21,440 --> 00:43:32,440
And the explanations, many explanations, contained in the commentaries, actually go back to the time of the Buddha.

234
00:43:32,440 --> 00:43:47,440
Because Buddha must have explained, must have given explanations when monks do not readily understand his teachings.

235
00:43:47,440 --> 00:43:55,440
And those explanations were preserved by individual monks.

236
00:43:55,440 --> 00:44:05,440
They were not collected at the first Buddhist council, but they were handed down from teacher to pupil, from teacher to pupil.

237
00:44:05,440 --> 00:44:15,440
And so when later on, they were compiled into books.

238
00:44:15,440 --> 00:44:37,440
So there are three levels of books in the Buddhist literature, especially the Thera, the Buddhist literature, and they are the texts, teachings of the Buddha, and the commentaries, explaining the texts, and sub commentaries, explaining the commentaries.

239
00:44:37,440 --> 00:44:44,440
So now there have been six Buddhist councils.

240
00:44:44,440 --> 00:44:50,440
So the first Buddhist council was held at Rajagaha.

241
00:44:50,440 --> 00:44:59,440
Just three months after the death of the Buddha, you already know about that council.

242
00:44:59,440 --> 00:45:10,440
Now the second Buddhist council was held at the city of Wisa Lee, one hundred years after the death of the Buddha.

243
00:45:10,440 --> 00:45:17,440
And the third Buddhist council was held at Patalipoda, the city in India.

244
00:45:17,440 --> 00:45:26,440
Two hundred and then thirty-four years after the death of the Buddha during the time of King Ahsoka.

245
00:45:26,440 --> 00:45:30,440
Ahsoka was a supporter of Patalipoda.

246
00:45:30,440 --> 00:45:49,440
These three councils were held in India during the time when there was not yet division of Buddhism into Thera, Radha, and Mahayana.

247
00:45:49,440 --> 00:46:03,440
After that, the division came into being and Thera Vada Buddhist held their own councils, and Mahayana Buddhist also held their own Mahayana councils.

248
00:46:03,440 --> 00:46:16,440
So since we are talking about Thera Vada principles here, I will tell you about Thera Vada councils only.

249
00:46:16,440 --> 00:46:31,440
So the fourth Buddhist council was held at Allu Vihara, that is in Sri Lanka, four hundred and fifty years after the death of the Buddha.

250
00:46:31,440 --> 00:46:40,440
The first Buddhist council until about four hundred and fifty years after the death of the Buddha,

251
00:46:40,440 --> 00:46:51,440
the typical, was handed down from generation to generation by word of mouth.

252
00:46:51,440 --> 00:47:03,440
So it was not yet written on books or on palm leaves, but about four hundred and fifty years after the death of the Buddha in Sri Lanka,

253
00:47:03,440 --> 00:47:15,440
the elders at that time thought that the future generation might not be able to keep the typical intact.

254
00:47:15,440 --> 00:47:22,440
So they decided to write the typical down on the palm leaves, and that was done.

255
00:47:22,440 --> 00:47:32,440
At a place called Allu Vihara, near modern candy in Sri Lanka.

256
00:47:32,440 --> 00:47:40,440
So at the fourth Buddhist council, the typical was written down on palm leaves.

257
00:47:40,440 --> 00:47:49,440
So it was not the time of paper books yet.

258
00:47:49,440 --> 00:48:01,440
In the late 19th century, during the time of King Mindong in Burma, the fifth Buddhist council was held.

259
00:48:01,440 --> 00:48:10,440
Now, kings want to take care of the previous kings.

260
00:48:10,440 --> 00:48:27,440
So King Mindong thought, if the previous kings or former kings had a typical written on palm leaves,

261
00:48:27,440 --> 00:48:38,440
I will have the typical written on marble slabs, which can last much, much longer than the palm leaves.

262
00:48:38,440 --> 00:48:42,440
So the white ants can eat palm leaves very easily.

263
00:48:42,440 --> 00:48:55,440
So he decided to ask the elders at that time to hold a Buddhist council.

264
00:48:55,440 --> 00:49:03,440
And so the fifth Buddhist council was held at Mendelier in 1871.

265
00:49:03,440 --> 00:49:08,440
So at that council, it takes a number of years.

266
00:49:08,440 --> 00:49:13,440
At that council, the typical was first written down on palm leaves.

267
00:49:13,440 --> 00:49:16,440
And then they were written down on copper plates.

268
00:49:16,440 --> 00:49:21,440
And last, they were written down on marble slabs.

269
00:49:21,440 --> 00:49:30,440
And these marble slabs were put in a house, a brick house eats.

270
00:49:30,440 --> 00:49:42,440
And they were placed in the precincts of the Pagoda Kaur Kubodo in Mendelier at the foot of Mendelier here.

271
00:49:42,440 --> 00:49:52,440
So if you visit Burma and if you visit Mendelier, you can go and see these marble slabs.

272
00:49:52,440 --> 00:49:53,440
They are all intact.

273
00:49:53,440 --> 00:50:01,440
It is very lucky that not a single, what about that bomb was dropped on that place.

274
00:50:01,440 --> 00:50:04,440
So they are still intact.

275
00:50:04,440 --> 00:50:11,440
And these marble slabs are big slabs, about 5 feet tall and 3 and a half feet across,

276
00:50:11,440 --> 00:50:15,440
and about 5 or 6 inches thick.

277
00:50:15,440 --> 00:50:30,440
So one, one bomb is the other, called it the rounds biggest book.

278
00:50:30,440 --> 00:50:38,440
They are all together 729 of those marble slabs.

279
00:50:38,440 --> 00:50:43,440
And each has two pages, two phases.

280
00:50:43,440 --> 00:50:51,440
So it is about 1458 pages of marble slabs.

281
00:50:51,440 --> 00:51:06,440
And on them are written, inscribed the whole of T.B. to God.

282
00:51:06,440 --> 00:51:16,440
Now, King Mindon was succeeded by his son, King P. Bo.

283
00:51:16,440 --> 00:51:24,440
And during his time, Burma fell to the British, and became a British colony.

284
00:51:24,440 --> 00:51:30,440
Then after the Second World War, Burma gained independence.

285
00:51:30,440 --> 00:51:36,440
And so the government of Burma decided to hold another council.

286
00:51:36,440 --> 00:51:45,440
So in 1954, the sixth British council was held in Rangoon, Burma.

287
00:51:45,440 --> 00:51:59,440
Now, in the meantime, many books came into being, and T.B. to God books were printed in Burma.

288
00:51:59,440 --> 00:52:05,440
So there were books, Russian, and then the marble slabs, Russian.

289
00:52:05,440 --> 00:52:23,440
So the government of Burma wanted there to be one, one edited version of T.B. to become a standard version

290
00:52:23,440 --> 00:52:25,440
for all the Arab Buddhists.

291
00:52:25,440 --> 00:52:33,440
So to that council, sixth British council, the elders from the other T.RA.

292
00:52:33,440 --> 00:52:39,440
countries were invited to edit the T.B. to God again.

293
00:52:39,440 --> 00:52:49,440
And so the Leonard elders from Sri Lanka, Thailand, Cambodia, and Laos are invited to Burma.

294
00:52:49,440 --> 00:53:00,440
And they sat together with Burmese monks and re-edited the text, comparing with different versions.

295
00:53:00,440 --> 00:53:12,440
And so in addition of T.B. to God books was cut it out at the sixth British council.

296
00:53:12,440 --> 00:53:25,440
And not only the text, but also the commentaries and sub commentaries were edited and recited and printed and published.

297
00:53:25,440 --> 00:53:41,440
So there are altogether 118 volumes printed and published by the sixth British council.

298
00:53:41,440 --> 00:53:51,440
So King Mindoun created the biggest book in the world.

299
00:53:51,440 --> 00:54:01,440
That was the time of scribes and who had to do with their hands, and later came to printing age.

300
00:54:01,440 --> 00:54:07,440
And now it is a computer age.

301
00:54:07,440 --> 00:54:22,440
So now we are trying to produce the smallest T.B. to God in the world T.B. to God on CD-ROM.

302
00:54:22,440 --> 00:54:38,440
So now the all the text over 118 volumes were entered into the computer.

303
00:54:38,440 --> 00:54:45,440
And I think now they are collecting the type of errors and pretty soon maybe about n of this year or into the next year.

304
00:54:45,440 --> 00:54:57,440
I think we were able to produce that smallest T.B. to God book on CD-ROM.

305
00:54:57,440 --> 00:55:03,440
This is the account of the teachings of the Buddha.

306
00:55:03,440 --> 00:55:13,440
May the T.B. to God endure in the world as long as the world lasts

307
00:55:13,440 --> 00:55:21,440
and from the basis for correct understanding of the teachings of the Buddha.

308
00:55:21,440 --> 00:55:42,440
And also the correct practice of his teachings thus enabling beings to reach the final destination of extinction of all suffering.

309
00:55:42,440 --> 00:55:48,440
Thank you.

