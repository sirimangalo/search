1
00:00:00,000 --> 00:00:07,000
Okay, good evening everyone. Welcome to our daily session.

2
00:00:07,000 --> 00:00:33,000
The topic I wanted to focus on tonight is related to delusion or ignorance.

3
00:00:33,000 --> 00:00:51,000
The curious characteristic quality of samsara,

4
00:00:51,000 --> 00:01:05,000
that it may come to be in a state where we,

5
00:01:05,000 --> 00:01:15,000
where we don't see things as they are.

6
00:01:15,000 --> 00:01:36,000
It might sound insignificant, but it's actually incredibly important statement.

7
00:01:36,000 --> 00:01:41,000
We know this in science, modern material science.

8
00:01:41,000 --> 00:01:52,000
That's why they avoid the best of their ability dependent on perception.

9
00:01:52,000 --> 00:02:00,000
The realization that our perception can be wrong.

10
00:02:00,000 --> 00:02:04,000
It's deceiving how simple that statement is, and you might think,

11
00:02:04,000 --> 00:02:11,000
I get it, I understand that.

12
00:02:11,000 --> 00:02:18,000
It's guaranteed that when you begin to explore the realm of perception through meditation practice,

13
00:02:18,000 --> 00:02:29,000
that you'll be surprised, that you'll be shocked with the realization that you're really just

14
00:02:29,000 --> 00:02:40,000
not even distorted reality, but completely misunderstood it,

15
00:02:40,000 --> 00:02:44,000
and totally.

16
00:02:44,000 --> 00:02:51,000
To the extent that black seemed like white could seem like bad.

17
00:02:51,000 --> 00:02:57,000
Bad seemed good.

18
00:02:57,000 --> 00:03:03,000
So in modern material science, they reject perception for this reason.

19
00:03:03,000 --> 00:03:07,000
You can't rely on your perception.

20
00:03:07,000 --> 00:03:15,000
They recognize this through studies, through experiment.

21
00:03:15,000 --> 00:03:19,000
I think in religion, of course, in spirituality,

22
00:03:19,000 --> 00:03:28,000
where we're prone to succumb to this.

23
00:03:28,000 --> 00:03:34,000
It's how you can come to be so sure of your beliefs,

24
00:03:34,000 --> 00:03:57,000
so sure of your convictions, and only hasn't turned out to be wrong.

25
00:03:57,000 --> 00:04:13,000
I remember once driving around in Shenmai,

26
00:04:13,000 --> 00:04:27,000
and we had it somewhere, our whole troop, when I was teaching, we were heading somewhere to get to the super highway.

27
00:04:27,000 --> 00:04:29,000
I told him to turn left.

28
00:04:29,000 --> 00:04:30,000
I said, you have to turn left.

29
00:04:30,000 --> 00:04:32,000
There's the super highway.

30
00:04:32,000 --> 00:04:34,000
There's the slouch and monkeys.

31
00:04:34,000 --> 00:04:38,000
No, no, that's not the super highway.

32
00:04:38,000 --> 00:04:51,000
And finally, they turned left, and then I looked and I said, oh, this isn't the super highway.

33
00:04:51,000 --> 00:05:07,000
It's very easy to be completely sure of yourself and be wrong.

34
00:05:07,000 --> 00:05:25,000
The problem, of course, with this stance of material science,

35
00:05:25,000 --> 00:05:35,000
is that it ignores a very terrible problem with

36
00:05:35,000 --> 00:05:44,000
our reality and our existence, and attempts to sidestep the problem

37
00:05:44,000 --> 00:06:00,000
by strict reliance on third person's impersonal, not third person, but impersonal observations.

38
00:06:00,000 --> 00:06:10,000
Find ways, double-blind tests.

39
00:06:10,000 --> 00:06:15,000
But that doesn't solve the problem that we are really messed up in our perceptions.

40
00:06:15,000 --> 00:06:23,000
In more ways, the science, even, is, I think, clear about,

41
00:06:23,000 --> 00:06:29,000
able to understand.

42
00:06:29,000 --> 00:06:41,000
And so Buddhism does something bold and perhaps seen as impossible by a material scientist,

43
00:06:41,000 --> 00:06:53,000
and that is to correct our perceptions.

44
00:06:53,000 --> 00:07:00,000
I find a way of being non, I don't know, I used to say objective,

45
00:07:00,000 --> 00:07:03,000
but I think that word is used differently by different people.

46
00:07:03,000 --> 00:07:08,000
I'm going to say objective, but I mean by that is impartial or unbiased,

47
00:07:08,000 --> 00:07:21,000
and even more than unbiased, clear, seeing clearly.

48
00:07:21,000 --> 00:07:40,000
We claim the ability to correct our misperceptions and to dispel the delusion and the anger.

49
00:07:40,000 --> 00:07:50,000
But I want to emphasize this point, and it's both incredibly important to understand

50
00:07:50,000 --> 00:08:01,000
and incredibly important to understand as being the root of the problem

51
00:08:01,000 --> 00:08:07,000
that we can misunderstand, that we can be completely and utterly wrong about something.

52
00:08:07,000 --> 00:08:12,000
Such that there's no hint of it being wrong.

53
00:08:12,000 --> 00:08:15,000
Apart from the consequences, of course.

54
00:08:15,000 --> 00:08:21,000
And this is the thing is, well, if we perceive things in a certain way, what's wrong with that?

55
00:08:21,000 --> 00:08:25,000
If I think ice cream is good, what's wrong with that?

56
00:08:25,000 --> 00:08:29,000
I like chocolate ice cream, you like vanilla ice cream.

57
00:08:29,000 --> 00:08:36,000
We all like certain things, we all have certain partialities, perceptions,

58
00:08:36,000 --> 00:08:41,000
we all have certain, let's not even talk about partialities, but we all have certain character types.

59
00:08:41,000 --> 00:08:49,000
I have a short temper, let's say, or I know I'm this, or I'm that.

60
00:08:49,000 --> 00:08:54,000
I'm A-type personality, B-type personality.

61
00:08:54,000 --> 00:08:56,000
It's just who I am.

62
00:08:56,000 --> 00:09:07,000
And so you find people trying to perform mental gymnastics to try to make sure that everyone

63
00:09:07,000 --> 00:09:15,000
whatever person type of person they are is understood as being equally valid.

64
00:09:15,000 --> 00:09:16,000
Right?

65
00:09:16,000 --> 00:09:23,000
So if our perception is different from someone else's perception, what's wrong with that?

66
00:09:23,000 --> 00:09:31,000
And the problem is that there is, as it turns out, an underlying reality that doesn't change.

67
00:09:31,000 --> 00:09:37,000
You think, well, my reality is different from there as well, actually, it's not.

68
00:09:37,000 --> 00:09:56,000
And there's no one in this world who can escape a certain fairly simple qualities or characteristics of reality.

69
00:09:56,000 --> 00:10:08,000
So greed, for example, greed and craving and desire and thirst and ambition, these are all very good examples of this.

70
00:10:08,000 --> 00:10:29,000
We wonder how a person can be driven to rape another person, for example.

71
00:10:29,000 --> 00:10:45,000
We study addiction, we study passion or desire, sexual desires, big one.

72
00:10:45,000 --> 00:10:53,000
We study addiction to food and so on.

73
00:10:53,000 --> 00:11:00,000
And then deny that there are negative consequences to desire.

74
00:11:00,000 --> 00:11:19,000
And so intellectually, this is easy to accept that, perhaps not clear to us, but should be easy to understand that there is an underlying reality that doesn't allow for addiction.

75
00:11:19,000 --> 00:11:26,000
It doesn't allow for a peaceful and a happy, addicted state.

76
00:11:26,000 --> 00:11:32,000
It's a reality whereby addiction causes suffering.

77
00:11:32,000 --> 00:11:45,000
But this idea of seeing things completely wrong helps us to understand why intellectual understanding isn't enough.

78
00:11:45,000 --> 00:11:49,000
There's intellectual understanding, what is it? It's a thought.

79
00:11:49,000 --> 00:11:52,000
There's no such thing as intellectual understanding.

80
00:11:52,000 --> 00:11:59,000
It's a series of thoughts that lead to a single thought with a lot of conviction.

81
00:11:59,000 --> 00:12:04,000
So you have this thought, which is your premises, at least to this thought, and this thought.

82
00:12:04,000 --> 00:12:08,000
And finally, you come up with a conclusion.

83
00:12:08,000 --> 00:12:11,000
And that has some power to it. It gives you conviction.

84
00:12:11,000 --> 00:12:15,000
So it will, to some extent, affect your behavior.

85
00:12:15,000 --> 00:12:20,000
But it doesn't change the fact that when you see cheese cake, you like it.

86
00:12:20,000 --> 00:12:25,000
You want it. You think you feel that that is going to bring you happiness.

87
00:12:25,000 --> 00:12:31,000
When you see the object of your desire, a beautiful man, a beautiful woman,

88
00:12:31,000 --> 00:12:39,000
and you see the body when you see things.

89
00:12:39,000 --> 00:12:48,000
When you hear music, it doesn't change the fact that we get attached.

90
00:12:48,000 --> 00:12:58,000
And we become addicted.

91
00:12:58,000 --> 00:13:02,000
That it blinds us through any suffering.

92
00:13:02,000 --> 00:13:04,000
That you'll be completely blind.

93
00:13:04,000 --> 00:13:11,000
You can rationalize it. You can examine your life and see how terrible you are.

94
00:13:11,000 --> 00:13:15,000
You are terribly destroying your life.

95
00:13:15,000 --> 00:13:18,000
Read Dostiovsky, some of his stuff.

96
00:13:18,000 --> 00:13:21,000
Pretty hard core.

97
00:13:21,000 --> 00:13:23,000
Suffering. And he could write about it.

98
00:13:23,000 --> 00:13:27,000
He knew what he was doing to his life.

99
00:13:27,000 --> 00:13:32,000
To his mind.

100
00:13:32,000 --> 00:13:35,000
But the addict can't help themselves.

101
00:13:35,000 --> 00:13:39,000
It's a totally different, totally different level.

102
00:13:39,000 --> 00:13:43,000
This is how someone could write how, in the time of the Buddha,

103
00:13:43,000 --> 00:13:49,000
there was this novice Samanera who raped his cousin,

104
00:13:49,000 --> 00:13:56,000
who was a bikkhuni. She was an arahant.

105
00:13:56,000 --> 00:14:05,000
And when he left her kuti, her hut, he Earth swallowed him up, they say.

106
00:14:05,000 --> 00:14:09,000
And I went to hell.

107
00:14:09,000 --> 00:14:13,000
That a person can't see that that's wrong, right?

108
00:14:13,000 --> 00:14:16,000
And yet, there are many people in this world who can't see it's wrong,

109
00:14:16,000 --> 00:14:19,000
and we want to call them monsters.

110
00:14:19,000 --> 00:14:25,000
We want to say that that is the sort of a person very different from us.

111
00:14:25,000 --> 00:14:32,000
That sort of person is nothing like us.

112
00:14:32,000 --> 00:14:35,000
But we all have this in us.

113
00:14:35,000 --> 00:14:43,000
And to some extent, it's incredibly blame-worthy,

114
00:14:43,000 --> 00:14:46,000
but it's also, in a sense, blameless,

115
00:14:46,000 --> 00:14:50,000
because according to their perception, it's not blameless,

116
00:14:50,000 --> 00:14:57,000
but it's, in a sense, what I mean, it's completely purely.

117
00:14:57,000 --> 00:14:59,000
This is going to make me happy.

118
00:14:59,000 --> 00:15:05,000
There's a total ignorance and delusion and lack of clarity.

119
00:15:05,000 --> 00:15:10,000
They're unable to see, as what I mean.

120
00:15:10,000 --> 00:15:13,000
At that moment, the mind is consumed.

121
00:15:13,000 --> 00:15:16,000
The mind is clear that this is going to make me happy.

122
00:15:16,000 --> 00:15:23,000
And, oops, boy, was I wrong.

123
00:15:23,000 --> 00:15:27,000
That's how this, some sorrow works.

124
00:15:27,000 --> 00:15:31,000
That's how we get stuck and why we stay stuck.

125
00:15:31,000 --> 00:15:34,000
And that's how insight meditation works.

126
00:15:34,000 --> 00:15:36,000
That's why insight meditation works.

127
00:15:36,000 --> 00:15:41,000
That's why the answer isn't to force yourself

128
00:15:41,000 --> 00:15:46,000
to follow your intellectual understanding, but it's to see clearly.

129
00:15:46,000 --> 00:15:48,000
Anger is another one.

130
00:15:48,000 --> 00:15:56,000
Anger is, it's incredible what anger will do to you.

131
00:15:56,000 --> 00:16:12,000
There was, uh, time of the Buddha, there was this, his stepfather.

132
00:16:12,000 --> 00:16:20,000
He was so angry at him.

133
00:16:20,000 --> 00:16:27,000
For taking away his, taking for leaving his daughter first of all

134
00:16:27,000 --> 00:16:31,000
when he left home and then for later on for ordaining her.

135
00:16:31,000 --> 00:16:33,000
And taking her away from him.

136
00:16:33,000 --> 00:16:38,000
As well, along with his grandson, Rahul.

137
00:16:38,000 --> 00:16:41,000
And he ended up going to hell as well.

138
00:16:41,000 --> 00:16:43,000
And David Atlas, another example.

139
00:16:43,000 --> 00:16:50,000
David Atlas said to have spouted, vomited blood.

140
00:16:50,000 --> 00:17:01,000
He was so angry.

141
00:17:01,000 --> 00:17:05,000
We get angry and it blinds us.

142
00:17:05,000 --> 00:17:11,000
At that moment, we really think that anger is the best solution.

143
00:17:11,000 --> 00:17:17,000
We don't at that moment have conscious awareness that this is wrong.

144
00:17:17,000 --> 00:17:19,000
We're going to intellectually wait.

145
00:17:19,000 --> 00:17:24,000
We might know and we might try to be a nice, nice people.

146
00:17:24,000 --> 00:17:29,000
It's quite common for Buddhists, you know, Buddhists.

147
00:17:29,000 --> 00:17:31,000
We're nice people, right?

148
00:17:31,000 --> 00:17:34,000
Why? Because we have all these teachings on how to be nice people.

149
00:17:34,000 --> 00:17:38,000
Christians, many Christians are nice people for similar reasons

150
00:17:38,000 --> 00:17:44,000
because Christianity, I think, is particularly concerned with being nice.

151
00:17:44,000 --> 00:17:47,000
You know, turning the other cheek is very powerful saying,

152
00:17:47,000 --> 00:17:49,000
Christians can be really nice.

153
00:17:49,000 --> 00:17:51,000
And that's some really nice Christians.

154
00:17:51,000 --> 00:17:54,000
Of course, they have lots of other crazy ideas.

155
00:17:54,000 --> 00:17:57,000
But the niceness is there.

156
00:17:57,000 --> 00:17:59,000
But as with Buddhists, we can be very nice.

157
00:17:59,000 --> 00:18:02,000
But don't get us upset.

158
00:18:02,000 --> 00:18:06,000
You know, don't poke the hornet's nest

159
00:18:06,000 --> 00:18:09,000
because Buddhists will get just as angry as anyone else.

160
00:18:09,000 --> 00:18:16,000
You know, I've had monks yelling at me.

161
00:18:16,000 --> 00:18:19,000
Remember early on, I often tell the story.

162
00:18:19,000 --> 00:18:23,000
I've come back from Canada.

163
00:18:23,000 --> 00:18:27,000
I spent my first reigns for freedom of Canada.

164
00:18:27,000 --> 00:18:31,000
I came back to Thailand and I hadn't been there two weeks

165
00:18:31,000 --> 00:18:35,000
and we're sitting in the dining hall and suddenly there's a clatter beside us.

166
00:18:35,000 --> 00:18:38,000
And I turn over all the monks sitting in the dining room.

167
00:18:38,000 --> 00:18:42,000
I turn, and one of the monks has gotten up and he's just pounding

168
00:18:42,000 --> 00:18:44,000
on one of the other monks.

169
00:18:44,000 --> 00:18:46,000
His nose is broken.

170
00:18:46,000 --> 00:18:52,000
He's got a blood going everywhere.

171
00:18:52,000 --> 00:18:56,000
So I got up and started and grabbed this.

172
00:18:56,000 --> 00:19:07,000
And remember, of all the monks, I was the only one.

173
00:19:07,000 --> 00:19:13,000
At that moment, he thought punching this other monk was the right thing to do.

174
00:19:13,000 --> 00:19:17,000
Isn't it crazy how he can think, especially as a Buddhist monk,

175
00:19:17,000 --> 00:19:21,000
how he could come to the point where he thought that that was a good idea?

176
00:19:21,000 --> 00:19:23,000
He was totally blind.

177
00:19:23,000 --> 00:19:27,000
And he knew it. He felt awful about anything.

178
00:19:27,000 --> 00:19:29,000
Terribly ashamed.

179
00:19:29,000 --> 00:19:31,000
It's not a bad monk.

180
00:19:31,000 --> 00:19:36,000
He's just strong anger inside.

181
00:19:36,000 --> 00:19:45,000
He's built up.

182
00:19:45,000 --> 00:19:46,000
And delusion.

183
00:19:46,000 --> 00:19:51,000
I mean, David, that does a good example of delusion.

184
00:19:51,000 --> 00:19:55,000
Imagine, this guy wanted to be a desire as well.

185
00:19:55,000 --> 00:19:59,000
He wanted to be a head of the leader.

186
00:19:59,000 --> 00:20:00,000
He wanted the Buddha.

187
00:20:00,000 --> 00:20:02,000
Yeah, it was arrogance.

188
00:20:02,000 --> 00:20:04,000
That's delusion.

189
00:20:04,000 --> 00:20:06,000
We hold ourselves up.

190
00:20:06,000 --> 00:20:11,000
We puff ourselves up how we think how important we are.

191
00:20:11,000 --> 00:20:14,000
You look at these people who are all puffed up.

192
00:20:14,000 --> 00:20:17,000
Anyone who has self-important.

193
00:20:17,000 --> 00:20:22,000
That's kind of ridiculous, really.

194
00:20:22,000 --> 00:20:26,000
They become less attractive, right?

195
00:20:26,000 --> 00:20:30,000
They become less impressive.

196
00:20:30,000 --> 00:20:33,000
They make everybody angry around them.

197
00:20:33,000 --> 00:20:36,000
They make everyone afraid of them.

198
00:20:36,000 --> 00:20:39,000
They create such suffering.

199
00:20:39,000 --> 00:20:46,000
There's no greatness to be had in arrogance and pride.

200
00:20:46,000 --> 00:20:51,000
It's unpleasant.

201
00:20:51,000 --> 00:20:55,000
And the people don't see this, right?

202
00:20:55,000 --> 00:21:00,000
They don't see how silly they look when they puff themselves up.

203
00:21:00,000 --> 00:21:05,000
They blow fish or like a peacock.

204
00:21:05,000 --> 00:21:12,000
We really think that there's good to be had by showing off.

205
00:21:12,000 --> 00:21:16,000
Now, if you ever hear people, there's culture.

206
00:21:16,000 --> 00:21:17,000
I've been in cultures.

207
00:21:17,000 --> 00:21:18,000
I guess I won't say which.

208
00:21:18,000 --> 00:21:26,000
But I've been in cultures where some of the people are incredibly inclined to brag about themselves.

209
00:21:26,000 --> 00:21:27,000
It's become a culture.

210
00:21:27,000 --> 00:21:40,000
In fact, when I tell you how great they are, how people are intent upon

211
00:21:40,000 --> 00:21:43,000
you, how great they are.

212
00:21:43,000 --> 00:21:46,000
Mostly, it just frustrates us.

213
00:21:46,000 --> 00:21:48,000
And it makes us feel jealous.

214
00:21:48,000 --> 00:21:50,000
Because we're caught up and all that.

215
00:21:50,000 --> 00:21:54,000
But if you step back and as a meditative, you think,

216
00:21:54,000 --> 00:21:57,000
who could think that such a thing is a good idea?

217
00:21:57,000 --> 00:22:01,000
But of course, at the moment, for that person,

218
00:22:01,000 --> 00:22:04,000
I think showing off somehow impresses people.

219
00:22:04,000 --> 00:22:08,000
Somehow makes people like you more or something.

220
00:22:08,000 --> 00:22:10,000
That's sort of what we think.

221
00:22:10,000 --> 00:22:11,000
We think we're going to feel good.

222
00:22:11,000 --> 00:22:14,000
It'll make us feel good.

223
00:22:14,000 --> 00:22:17,000
Because inside, we usually hate ourselves.

224
00:22:17,000 --> 00:22:19,000
We have lots of self-esteem issues.

225
00:22:19,000 --> 00:22:26,000
So we compensate by seeing if everybody else thinks I'm great.

226
00:22:26,000 --> 00:22:31,000
And maybe I'll be great.

227
00:22:31,000 --> 00:22:33,000
It's kind of funny to watch.

228
00:22:33,000 --> 00:22:37,000
This is why this is why Buddhas and Arahant have a funny sense of humor.

229
00:22:37,000 --> 00:22:39,000
They smile at the artist's things.

230
00:22:39,000 --> 00:22:41,000
Because they're not odd, really.

231
00:22:41,000 --> 00:22:47,000
It's just that all of us are too blind to see the humor in it.

232
00:22:47,000 --> 00:23:10,000
It's starting around like peacocks, fighting like roosters.

233
00:23:10,000 --> 00:23:19,000
And they don't like dogs in heat, or caught up in greed, anger, and delusion.

234
00:23:19,000 --> 00:23:21,000
You look at things like food.

235
00:23:21,000 --> 00:23:24,000
And if you ever watch people stuffing their face,

236
00:23:24,000 --> 00:23:26,000
you look at even sexual activity.

237
00:23:26,000 --> 00:23:27,000
I mean, how ridiculous is it?

238
00:23:27,000 --> 00:23:30,000
How ridiculous is sexual sex?

239
00:23:30,000 --> 00:23:32,000
What a silly activity.

240
00:23:32,000 --> 00:23:33,000
Kissing.

241
00:23:33,000 --> 00:23:35,000
Kissing the start line.

242
00:23:35,000 --> 00:23:38,000
What is kissing?

243
00:23:38,000 --> 00:23:41,000
Just smack our lips together for a while.

244
00:23:41,000 --> 00:23:44,000
Isn't it absurd?

245
00:23:44,000 --> 00:23:49,000
Yet when you're kissing, it seems like the pinnacle.

246
00:23:49,000 --> 00:23:50,000
Right?

247
00:23:50,000 --> 00:23:57,000
And people have written poems and songs and books about kissing,

248
00:23:57,000 --> 00:24:00,000
about sex, about food.

249
00:24:00,000 --> 00:24:02,000
Rest up in books.

250
00:24:02,000 --> 00:24:05,000
It doesn't make you stop and scratch your head.

251
00:24:05,000 --> 00:24:26,000
We have books with pictures of fat and grained oils and salts and sugars and stuff.

252
00:24:26,000 --> 00:24:28,000
All this stuff that eventually becomes urine and feces.

253
00:24:28,000 --> 00:24:29,000
I mean, that's what we're writing.

254
00:24:29,000 --> 00:24:37,000
And that's what we're making books about and praising and poems and everything.

255
00:24:37,000 --> 00:24:40,000
So, I mean, it's ridiculous.

256
00:24:40,000 --> 00:24:49,000
But the key, the more important point is this understanding of how wrong we can be,

257
00:24:49,000 --> 00:24:54,000
of realizing that we don't see it as ridiculous.

258
00:24:54,000 --> 00:25:04,000
And it's not even that it's unclear to us, it's that it really undeniably seems meaningful.

259
00:25:04,000 --> 00:25:11,000
So, the key out of that is that we're wrong.

260
00:25:11,000 --> 00:25:16,000
And that we can prove to ourselves that we're wrong.

261
00:25:16,000 --> 00:25:18,000
And what we're wrong about.

262
00:25:18,000 --> 00:25:21,000
Very easily, in fact.

263
00:25:21,000 --> 00:25:30,000
All you have to do is look when a person looks, when you look, you see.

264
00:25:30,000 --> 00:25:35,000
When you look, you see, when you see, you know, if you want to know, you have to look.

265
00:25:35,000 --> 00:25:39,000
If you want to know, you have to see, if you want to see, you have to look.

266
00:25:39,000 --> 00:25:43,000
Look, see, you know.

267
00:25:43,000 --> 00:25:53,000
When you practice mindfulness, when you cultivate mindfulness, desire,

268
00:25:53,000 --> 00:25:58,000
and the objects of your desire seem undesirable, they become undesirable.

269
00:25:58,000 --> 00:26:01,000
You can clearly see, geez.

270
00:26:01,000 --> 00:26:05,000
What am I doing, lasting after the human body?

271
00:26:05,000 --> 00:26:12,000
What am I doing, lasting after this taste in my mouth?

272
00:26:12,000 --> 00:26:18,000
And sugar and salt.

273
00:26:18,000 --> 00:26:22,000
It's like you pull away the veil of ignorance.

274
00:26:22,000 --> 00:26:24,000
It's like you turn on the light.

275
00:26:24,000 --> 00:26:27,000
At Jentong, he this monk here.

276
00:26:27,000 --> 00:26:31,000
He said, once I caught it on tape, he said,

277
00:26:31,000 --> 00:26:33,000
mindfulness is like a light.

278
00:26:33,000 --> 00:26:38,000
All the evil things, it was just a really, really good way of putting

279
00:26:38,000 --> 00:26:42,000
all the evil things, they're all dark.

280
00:26:42,000 --> 00:26:43,000
Mindfulness is like a light.

281
00:26:43,000 --> 00:26:47,000
When you turn on the light, the darkness disappear.

282
00:26:47,000 --> 00:26:50,000
That's really true.

283
00:26:50,000 --> 00:27:00,000
In that moment, when you're mindful, the darkness is disappeared.

284
00:27:00,000 --> 00:27:03,000
So, important for us to understand.

285
00:27:03,000 --> 00:27:17,000
And an important reason for us to be very careful about self-confidence being sure of ourselves.

286
00:27:17,000 --> 00:27:22,000
I think one of the things a meditator learns early on is that you can't trust yourself.

287
00:27:22,000 --> 00:27:25,000
You can't trust intuition.

288
00:27:25,000 --> 00:27:31,000
You have no reason to trust yourself.

289
00:27:31,000 --> 00:27:38,000
But unlike material scientists, you can change that.

290
00:27:38,000 --> 00:27:41,000
You don't need to trust yourself.

291
00:27:41,000 --> 00:27:46,000
But that you can come to see through your delusion.

292
00:27:46,000 --> 00:27:53,000
You can come to see your misunderstanding.

293
00:27:53,000 --> 00:27:57,000
You can come to clear them up.

294
00:27:57,000 --> 00:28:02,000
To the point that, I mean, the argument might be made how do you know that then you're seeing things clearly.

295
00:28:02,000 --> 00:28:10,000
But it's the point that they then become in line with reality.

296
00:28:10,000 --> 00:28:18,000
So, you might say, after your practice meditation, how do you know you're still not seeing things incorrectly?

297
00:28:18,000 --> 00:28:22,000
The difference is that now it's in light, now your understanding is in line with reality.

298
00:28:22,000 --> 00:28:23,000
And that's important.

299
00:28:23,000 --> 00:28:25,000
That's clear.

300
00:28:25,000 --> 00:28:29,000
The way we see things, saw things before, was not in line with reality.

301
00:28:29,000 --> 00:28:33,000
These things that couldn't bring us happiness, we thought they could bring us happiness.

302
00:28:33,000 --> 00:28:34,000
They can't.

303
00:28:34,000 --> 00:28:36,000
You see impermanence.

304
00:28:36,000 --> 00:28:37,000
You see suffering.

305
00:28:37,000 --> 00:28:38,000
You see non-self.

306
00:28:38,000 --> 00:28:41,000
You see these qualities of reality that nothing is stable.

307
00:28:41,000 --> 00:28:44,000
Because of that nothing can satisfy us.

308
00:28:44,000 --> 00:28:49,000
There's nothing that is mean that is mine.

309
00:28:49,000 --> 00:28:54,000
There's nothing that is under our control.

310
00:28:54,000 --> 00:28:56,000
So, there you go.

311
00:28:56,000 --> 00:28:58,000
There's the demo for tonight.

312
00:28:58,000 --> 00:29:25,000
Thank you all for joining me.

