1
00:00:00,000 --> 00:00:07,000
Okay, so starting with our announcements.

2
00:00:07,000 --> 00:00:11,000
The first announcement, as I said, is we have...

3
00:00:11,000 --> 00:00:15,000
I've started posting chapters from the book as I edit them.

4
00:00:15,000 --> 00:00:18,000
So you all get to see how slowly I'm editing them.

5
00:00:18,000 --> 00:00:21,000
And they're not really well edited,

6
00:00:21,000 --> 00:00:24,000
but they figure they're good enough to put up on the web blog.

7
00:00:24,000 --> 00:00:28,000
So there's a page now at my web blog.

8
00:00:28,000 --> 00:00:32,000
There's just a couple of chapters from the book.

9
00:00:32,000 --> 00:00:40,000
It's the page under the text teachings called Lessons in Practical Buddhism.

10
00:00:40,000 --> 00:00:44,000
And so if you go there, you'll see there's a couple of talks

11
00:00:44,000 --> 00:00:46,000
that I just actually picked at random.

12
00:00:46,000 --> 00:00:49,000
I was just clicking and started editing.

13
00:00:49,000 --> 00:00:52,000
So it's not like those ones are particularly special.

14
00:00:52,000 --> 00:00:56,000
I'm just going to go through them at random and edit them.

15
00:00:56,000 --> 00:01:01,000
So you can go check that out and give your feedback.

16
00:01:01,000 --> 00:01:08,000
And you're still interested in transcribing one.

17
00:01:08,000 --> 00:01:12,000
More than merrier, we'll see how many we get around to.

18
00:01:12,000 --> 00:01:17,000
And whether I actually end up putting them all in the book,

19
00:01:17,000 --> 00:01:19,000
we'll have to see.

20
00:01:19,000 --> 00:01:25,000
Second announcement, as Paul and Yany, you can give the announcements.

21
00:01:25,000 --> 00:01:30,000
Our meditator has finished her course today.

22
00:01:30,000 --> 00:01:42,000
And hopefully a new future, she will be our next monastic resident here in our monastery.

23
00:01:42,000 --> 00:01:46,000
She was just saying that the future's unsure.

24
00:01:46,000 --> 00:01:47,000
We don't know.

25
00:01:47,000 --> 00:01:50,000
We can't say that she's going to be a monk or so on.

26
00:01:50,000 --> 00:01:52,000
But it looks quite promising.

27
00:01:52,000 --> 00:01:56,000
She came off of her course and one of the first things she asked was,

28
00:01:56,000 --> 00:02:00,000
when is the soonest I'll be able to her day?

29
00:02:00,000 --> 00:02:05,000
But it wasn't actually that she was like chomping at the bit or something.

30
00:02:05,000 --> 00:02:08,000
It was just like a practical, for practical reasons,

31
00:02:08,000 --> 00:02:10,000
because she has to tell her family and so on.

32
00:02:10,000 --> 00:02:12,000
But just the matter of fact,

33
00:02:12,000 --> 00:02:15,000
she said it was kind of encouraging.

34
00:02:15,000 --> 00:02:16,000
There wasn't this.

35
00:02:16,000 --> 00:02:19,000
Should I or day and shouldn't I or day?

36
00:02:19,000 --> 00:02:25,000
Just like it seemed to be quite a sudden done fact.

37
00:02:25,000 --> 00:02:34,000
So she will be becoming a novice, probably at the end of the month.

38
00:02:34,000 --> 00:02:37,000
But it's the 18th day or something.

39
00:02:37,000 --> 00:02:43,000
So maybe in 12 days, 10 or 11 days should be able to her day.

40
00:02:43,000 --> 00:02:52,000
And then you'll get pictures of our newest monastic resident.

41
00:02:52,000 --> 00:02:55,000
And then as far as we're dating as a monk,

42
00:02:55,000 --> 00:02:57,000
we'll have to see how that's going to go.

43
00:02:57,000 --> 00:03:00,000
Because of course we need a community to ordain them.

44
00:03:00,000 --> 00:03:05,000
So that might take some time to arrange.

45
00:03:05,000 --> 00:03:08,000
Okay, so that's another announcement.

46
00:03:08,000 --> 00:03:10,000
Any other announcements?

47
00:03:10,000 --> 00:03:15,000
That's not an announcement.

48
00:03:15,000 --> 00:03:41,000
Okay, that's enough.

