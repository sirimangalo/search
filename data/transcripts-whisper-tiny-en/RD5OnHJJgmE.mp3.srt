1
00:00:00,000 --> 00:00:07,000
Miriam asks, she has three younger brothers who really like to go out on party,

2
00:00:07,000 --> 00:00:12,000
since I don't go out because I don't see the point. They tell me I have no life,

3
00:00:12,000 --> 00:00:17,000
even my parents tell me the same, and I tell them I see no point in doing what they do

4
00:00:17,000 --> 00:00:21,000
because there's no gain from that, and that I'm happy being like this.

5
00:00:21,000 --> 00:00:32,000
I found this is distracting me in meditation, and my two attached to my family.

6
00:00:32,000 --> 00:00:42,000
I think I'm going to the Mangalom, Mangalom suit, Mangalom suit.

7
00:00:42,000 --> 00:00:50,000
I'm going to go through this chair, Shane family, supporting his mother and father,

8
00:00:50,000 --> 00:01:00,000
and taking care for the welfare of your family is one of the highest blessings.

9
00:01:00,000 --> 00:01:15,000
As Buddha says, so that you care that maybe they're doing something that's unhealthy for their body,

10
00:01:15,000 --> 00:01:29,000
and perhaps you're seeing how distracting what they're actually engaging is a huge distraction.

11
00:01:29,000 --> 00:01:40,000
You know, it's self-alcohol, distracts the mind, it creates loss of carelessness and addiction,

12
00:01:40,000 --> 00:01:53,000
but I've had similar experiences, and I would say that what's been really helpful,

13
00:01:53,000 --> 00:02:04,000
rather than kind of getting into too much talk about it would be to simply offer alternatives.

14
00:02:04,000 --> 00:02:21,000
People go out and party and watch TV and do mindless, careless, harmful things often because their culture sort of offers that and doesn't give too many alternatives.

15
00:02:21,000 --> 00:02:29,000
And it seems like you have a meditation practice going, and you could invite them to participate,

16
00:02:29,000 --> 00:02:40,000
whether it's going to some sort of domicock in the area that you think might be of interest to them,

17
00:02:40,000 --> 00:02:47,000
or seeing if they'll actually sit down with you and close their eyes in practice,

18
00:02:47,000 --> 00:03:04,000
just offering time together with them that could make their mind a bit more calm and peaceful so they don't feel like they need to go out so much.

19
00:03:04,000 --> 00:03:19,000
We don't have anything wrong with partying, it's just a specific type of party.

20
00:03:19,000 --> 00:03:36,000
Actually, like two nights ago, I don't know who stayed up all night, the bugs kept me up most of the night, I'd say, but we were at Anarada Pura, so everything.

21
00:03:36,000 --> 00:03:52,000
I don't know how much sleeping was going on, but meditating outside, and it's very Buddhist to do an all-night meditation every half moon.

22
00:03:52,000 --> 00:04:09,000
And it's quite fun, in a sense, even though it can be very challenging to actually stay up with your eyes closed, or you can change to do walking meditation,

23
00:04:09,000 --> 00:04:30,000
there's so much sort of peaceful energy that can arise, very calm and peaceful energy that can arise from feeling lethargy in the body and not just caving in or opening the fridge,

24
00:04:30,000 --> 00:04:45,000
and just feeling it arise and pass away, it can... yeah, it's a whole other type of partying of the night.

25
00:04:45,000 --> 00:04:52,000
Anyone else?

26
00:04:52,000 --> 00:05:05,000
No, I just wanted to say one simple thing is that they tell you you don't have a life, you have no life, tell them you have peace, tell them you have happiness, you have to find something good for yourself.

27
00:05:05,000 --> 00:05:23,000
When I was at home, my parents were yelling at me, well, my stepmother would be feeling at me saying I was brainwashed, I was zombie, and how I was lost, and how I was no fun anymore, not fun to be around.

28
00:05:23,000 --> 00:05:29,000
Because I used to play guitar, and I would joke, and I would play games, and so on, I didn't want to do any of that anymore.

29
00:05:29,000 --> 00:05:44,000
So you're like a zombie. But I knew inside that what I was doing was correct, and it was actually quite torture for me, the torture is to have to listen to this and feel kind of the anguish and the conflict and so on, because I was a new meditator.

30
00:05:44,000 --> 00:05:55,000
But it's not... that's a fact of life. You're surrounded by people who don't want to meditate, and that's what I was, so it was a lot of suffering, didn't change anything.

31
00:05:55,000 --> 00:06:05,000
It didn't make me say, oh, maybe I should go party with them. And there really is no answer. It just is what it is, that's the way it is.

32
00:06:05,000 --> 00:06:15,000
Once you develop in the meditation, then you have no problem, you just smile at them and say, you know, if I'm a zombie, then I'm a zombie, but I have peace, happiness, and freedom from suffering.

33
00:06:15,000 --> 00:06:24,000
What do you have? What do you get from going out, you know, right now in your mind you have a whole bunch of anger and frustration, you know, dissatisfaction with me, and so on.

34
00:06:24,000 --> 00:06:30,000
And worry about me and so on, and all of these things are causing suffering for you. I don't have that.

35
00:06:30,000 --> 00:06:38,000
So, you know, I consider this state to be much more peaceful and happy, but of course you need to attain that yourself.

36
00:06:38,000 --> 00:06:53,000
In the beginning, there is no answer. You're just going through this transition period where your parents haven't come to realize the truth of what you're saying, and you yourself haven't...

37
00:06:53,000 --> 00:07:12,000
Yeah, to experience the fruits are still only cultivating the fruit. It's like you have a field of grain or something, and it hasn't bore fruit yet, and people are saying, oh, what are you wasting on your time?

38
00:07:12,000 --> 00:07:22,000
Because they can't see the fruits, and you don't have anything to show them, because you don't have the fruits yet, but you know, we've planted correct, and you've done everything properly, and it's only a matter of time.

39
00:07:22,000 --> 00:07:27,000
And then when the fruit comes, then you can show them and say, see, see? That was very much worth it.

40
00:07:27,000 --> 00:07:32,000
And you knew people who were like to start the ants in the grasshopper, right?

41
00:07:32,000 --> 00:07:45,000
The grasshopper just lazes around all summer and places fiddle and has fun, and the ants were working and working, and then he said, look at these stupid ants, it's wasting their time working,

42
00:07:45,000 --> 00:08:02,000
working, collecting food, what are they doing? They should enjoy life. And then the winter comes, and then the grasshopper can't find food, and he's freezing and cold, and so he goes and knocks on the ants door and say, and they say, oh, yeah, so we've got lots of food we're doing fine, because we're preparing for the winter.

43
00:08:02,000 --> 00:08:13,000
So that kind of thing. I mean, it's a matter of where your priorities are, do you want to just waste your potential here, and now in enjoying

44
00:08:13,000 --> 00:08:21,000
the base pleasures, or do you want to work and cultivate higher happiness in higher peace?

45
00:08:21,000 --> 00:08:50,000
That's more lasting and more sustainable, but you have to put up with a ridicule of people like the grasshopper.

