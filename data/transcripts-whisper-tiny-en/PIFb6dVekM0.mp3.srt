1
00:00:00,000 --> 00:00:20,600
Okay, no everyone is broadcasting live daily broadcast, say hello, we need to provide some

2
00:00:20,600 --> 00:00:34,080
dhamma for us today, Robin. There's no dhamma, but it is Thanksgiving in the United States,

3
00:00:34,080 --> 00:00:39,400
so everyone is asleep after eating all their turkey, I think. That's three people I wish

4
00:00:39,400 --> 00:00:43,840
me happy Thanksgiving today, and I don't know if it was the right thing to say, but I said

5
00:00:43,840 --> 00:00:50,720
we don't have Thanksgiving again, or we had it last month. Certainly, I mean, not that it would

6
00:00:50,720 --> 00:00:57,760
mean anything to me anyway, probably, but still, I think it's kind of funny that people are

7
00:00:57,760 --> 00:01:08,400
wishing us Thanksgiving. I was in America, I guess people would, it would be proper, yeah.

8
00:01:08,400 --> 00:01:14,160
Thanksgiving in America is one of those holidays that's not controversial. There's so much ridiculous

9
00:01:14,160 --> 00:01:20,320
controversy with other holidays, like Donald Trump has been all up in art. You don't think

10
00:01:20,320 --> 00:01:25,760
Thanksgiving has any controversy associated with people? No, okay, let me step back on that, of course,

11
00:01:25,760 --> 00:01:32,400
it does because of the Native American thing, but I was just thinking of, you know, the more political

12
00:01:32,400 --> 00:01:39,360
type of things, like Donald Trump was up in arms because Starbucks has a plain red cup for their

13
00:01:39,360 --> 00:01:45,280
holiday cup this year. It doesn't say Merry Christmas or Happy Holidays or anything like that,

14
00:01:45,280 --> 00:01:50,000
and he was actually kind of going on and on about it how it was such a terrible thing, and when

15
00:01:50,000 --> 00:01:54,160
he's president, everyone's going to say Merry Christmas, so it gets kind of ridiculous with

16
00:01:54,160 --> 00:01:59,920
holidays, but Thanksgiving, other than the whole Native American thing, is a pretty mild holiday

17
00:01:59,920 --> 00:02:05,920
where everybody's celebrating the same thing, more or less, and I imagine the First Nations people

18
00:02:05,920 --> 00:02:15,280
would disagree with you on that one, I think so too. That's not very pleasant. I mean, the idea

19
00:02:15,280 --> 00:02:21,280
of Thanksgiving is pretty awesome. It's just how it arose, I think, or what it's associated with,

20
00:02:21,280 --> 00:02:31,920
somehow being thankful for having taken over some other people's land, invaded celebration of

21
00:02:31,920 --> 00:02:37,680
an invasion of, I mean, not quite invasion because people here didn't have the idea of land ownership,

22
00:02:37,680 --> 00:02:47,920
but really invasion, invasion like an invasive species that just comes in by force and by

23
00:02:47,920 --> 00:03:06,160
sheer inertia, steam rollers over another culture. I mean, to passively wipe out another species,

24
00:03:06,160 --> 00:03:20,720
I mean, it doesn't seem problematic, but if it's just old, they eventually just naturally

25
00:03:20,720 --> 00:03:26,080
became a dominant group, but that's not really how it happened. There was forced conversion

26
00:03:26,080 --> 00:03:35,760
of really theft and treachery, trickery, to get their lands, to get them to sign horribly

27
00:03:37,680 --> 00:03:47,680
unfair treaties and sort of advanced stuff from what I hear, in the name of imperialism,

28
00:03:47,680 --> 00:04:11,040
the queen, slavery, human beings can be so much more noble than any of the other animals,

29
00:04:11,040 --> 00:04:21,200
and they can be so much more human stuff. They were also convinced that their way was right.

30
00:04:25,120 --> 00:04:29,360
Well, the pilgrims, the settlers, they were so convinced that their way was right that...

31
00:04:31,360 --> 00:04:38,320
Well, I don't know, that's part of it, but I think that sort of from maybe from the just point

32
00:04:38,320 --> 00:04:42,240
of view or our cultural point of view, but there was a lot of greed involved, I think, as well.

33
00:04:42,960 --> 00:04:50,080
Just sheer opportunism of these people are naive in a sense, which I guess you couldn't say

34
00:04:50,080 --> 00:04:55,040
they were, but it just means that they were simple people. We weren't worried about land ownership

35
00:04:55,040 --> 00:05:08,480
and mineral rights and things like forest cutting down trees. They didn't rape and

36
00:05:08,480 --> 00:05:17,840
pillaged land either. When people came and saw these natural resources that were maybe starting

37
00:05:17,840 --> 00:05:29,280
to run short in Europe, I don't know, harder to get at, but oh, gold and whatever, it's land.

38
00:05:35,680 --> 00:05:36,240
I agree.

39
00:05:39,280 --> 00:05:42,080
The greed is the biggest reason for war.

40
00:05:42,080 --> 00:05:49,840
Usually the least publicized because everyone tries to excuse it with other reasons, but

41
00:05:51,600 --> 00:05:57,200
a big deep down, most worse have been, I've been followed a green, it's a fair green.

42
00:06:06,480 --> 00:06:07,920
So on that pleasant note.

43
00:06:07,920 --> 00:06:13,840
So I guess Thanksgiving is a little more controversial than I was thinking.

44
00:06:14,480 --> 00:06:23,200
I think so. I mean, the concept is awesome. It's just, it used to be called Columbus,

45
00:06:23,200 --> 00:06:25,120
they didn't, or is that a different day?

46
00:06:25,120 --> 00:06:26,320
That's a different day.

47
00:06:26,320 --> 00:06:29,680
Different day. You've got a day that celebrates this horrible, horrible person,

48
00:06:31,440 --> 00:06:33,200
but maybe the horrible, pretty bad.

49
00:06:33,200 --> 00:06:38,800
Well, the day that we have this Columbus day, I believe, is the Canadian Thanksgiving.

50
00:06:40,880 --> 00:06:42,720
But our Thanksgiving is a months later.

51
00:06:45,920 --> 00:06:51,280
Yeah, you celebrate the guy who, I mean, he was an opportunist apparently, just kind of

52
00:06:51,280 --> 00:06:56,640
looking for gold and he's thought he reached India, so that's why he called him India.

53
00:06:56,640 --> 00:07:06,480
So, anyway, this is very skirting the edge of what is not damn awesome.

54
00:07:07,680 --> 00:07:12,800
Better jump back in and see what do we have any, the coldest kind of neat,

55
00:07:14,240 --> 00:07:15,760
flexible grasping and worry.

56
00:07:19,200 --> 00:07:22,880
And so we have the five aggregates, and that's what we cling to.

57
00:07:22,880 --> 00:07:29,120
A neat clinging is always done into one of five or a combination of five concepts that have

58
00:07:29,120 --> 00:07:32,240
arisen based on the five aggregates.

59
00:07:33,840 --> 00:07:38,800
So we cling to the body, and when it changes, we worry if we

60
00:07:39,760 --> 00:07:44,480
it upsets us, it disturbs us, not just the body, but other physical forms as well,

61
00:07:44,480 --> 00:07:47,920
our belongings, our possession, my robe,

62
00:07:47,920 --> 00:07:52,720
um, these little ties on the end of the room.

63
00:07:54,560 --> 00:08:04,480
Yeah, there's little ties here, and one of them up here, ripped yesterday, ripped out.

64
00:08:07,440 --> 00:08:08,320
Just so it back.

65
00:08:10,000 --> 00:08:11,920
That didn't upset me, it wasn't a big deal.

66
00:08:12,880 --> 00:08:14,000
It's easy to get upset.

67
00:08:14,000 --> 00:08:17,760
I remember, I think when I first got this robe,

68
00:08:18,960 --> 00:08:22,560
like a few days later, it got some stain on it or something.

69
00:08:27,840 --> 00:08:29,040
When I went to the other,

70
00:08:33,760 --> 00:08:38,000
we cling to things, we cling to our possessions, we cling to people,

71
00:08:38,000 --> 00:08:40,000
cling to,

72
00:08:43,440 --> 00:08:45,680
form, we cling to feelings,

73
00:08:49,040 --> 00:08:54,960
we cling to pleasant feelings, we try to contain them any way possible,

74
00:08:55,920 --> 00:08:59,200
and we're constantly on guard against painful feelings.

75
00:09:02,080 --> 00:09:06,240
Often specific pain feelings, so if someone has maybe a bad back,

76
00:09:06,240 --> 00:09:13,520
and they're constantly worried about, constantly, but again and again and again.

77
00:09:15,440 --> 00:09:21,200
There's a little bit of pain in the media, I don't know, my back problem comes in obsession.

78
00:09:24,560 --> 00:09:27,920
You get very much caught up.

79
00:09:27,920 --> 00:09:33,840
When attachment to feelings,

80
00:09:34,880 --> 00:09:42,640
our attachment to perceptions, recognition, I mean it's this one not always by itself,

81
00:09:42,640 --> 00:09:44,400
but it's recognition that that's

82
00:09:49,120 --> 00:09:53,440
is a trigger for much of our addiction, much of our version,

83
00:09:53,440 --> 00:09:57,840
we recognize something. If you didn't recognize things,

84
00:09:59,760 --> 00:10:04,240
you wouldn't recognize a spider, and people who are afraid of spiders, if you didn't

85
00:10:04,240 --> 00:10:07,280
recognize that that was a spider, it could walk all over you and say,

86
00:10:08,240 --> 00:10:12,320
you'd have no idea what was going on, but you'd feel something and no, it's the feeling.

87
00:10:12,320 --> 00:10:22,800
If we didn't for recognition, if it weren't for our perceptions of things,

88
00:10:26,240 --> 00:10:31,760
we couldn't give rise to liking or disliking because we couldn't associate.

89
00:10:31,760 --> 00:10:42,080
You see a piece of cheesecake, and you associate it with cheesecake, you associate it with

90
00:10:42,080 --> 00:10:48,800
tastes and textures and feelings, and so you feel happy just to see it, but if you didn't know

91
00:10:48,800 --> 00:10:57,120
what it was, you couldn't recognize it. The recognition is actually quite neutral, but

92
00:10:57,120 --> 00:11:06,320
we cling to this, we agree for that simple recognition, because there are many things that aren't

93
00:11:06,320 --> 00:11:12,880
inherently pleasant, seeing a piece of cheesecake isn't inherently pleasant, seeing a spider isn't

94
00:11:12,880 --> 00:11:20,800
inherently unpleasant, but because we react to them, because we cling to that, the recognition

95
00:11:20,800 --> 00:11:28,800
of a spider is the moment that we get upset, and then Sankara, we cling to them,

96
00:11:30,080 --> 00:11:41,360
cling to the Sankara's of comparison when you compare some things, so if you look at your body

97
00:11:41,360 --> 00:11:47,920
and you're really fat like me, and you start to worry about your weight, and like I have to go

98
00:11:47,920 --> 00:11:57,440
to diet, stop eating so much. I'll get fat. Every time I end, everywhere I go, people always tell

99
00:11:57,440 --> 00:12:01,360
me I'm getting thinner. It's funny whenever I see people, I haven't seen them for a while, they keep

100
00:12:01,360 --> 00:12:06,800
telling me I'm getting thinner. People have been telling me for 15 years that I'm getting thinner,

101
00:12:09,840 --> 00:12:16,480
we worry about these things, and to thin them to fat, some people have become an obsession,

102
00:12:16,480 --> 00:12:22,080
and so they become bulimic or anorexic, to tall, and to short,

103
00:12:28,000 --> 00:12:31,280
cling to our thoughts about things, we cling to our emotions,

104
00:12:33,280 --> 00:12:35,840
I'm depressed, and then I identify with it,

105
00:12:40,000 --> 00:12:45,760
or I like something, and it's like I like it, we come attached to this desire, and you say yes,

106
00:12:45,760 --> 00:12:49,600
gotta get that desire, gotta follow that desire whenever it comes.

107
00:12:51,840 --> 00:12:56,240
This needs to worry and stress when you can't get what you want,

108
00:12:57,840 --> 00:13:03,360
when you're denied, it needs to anger, to grief, to sorrow, to suffering.

109
00:13:05,760 --> 00:13:09,440
In consciousness, while consciousness is another one of the neutral ones, there's nothing

110
00:13:09,440 --> 00:13:18,160
ostensibly problematic about consciousness, except that consciousness is

111
00:13:19,680 --> 00:13:23,440
like a capsule for all the rest, if it weren't for consciousness, you couldn't have

112
00:13:25,200 --> 00:13:27,440
judgments, you couldn't have any of the problems.

113
00:13:31,360 --> 00:13:35,840
It's like if you don't have a spider's web, you don't have a spider, no,

114
00:13:35,840 --> 00:13:40,480
or you don't have an ant home, you don't have ants, you don't have

115
00:13:42,240 --> 00:13:52,000
like a breathing ground for, let's say for bacteria or something, like a rusty nail,

116
00:13:53,120 --> 00:13:57,840
you know how everyone tells you, you have to be very careful, you don't step on a rusty nail,

117
00:13:57,840 --> 00:14:02,480
and if you do, you have to get it, you have to get it cleaned out, and you have to get a shot,

118
00:14:02,480 --> 00:14:05,920
or something, you know, you can get a shot in case you step on a rusty nail.

119
00:14:07,280 --> 00:14:11,200
When I was younger, I did step, I lived on a farm and lots of rusty nails around,

120
00:14:11,200 --> 00:14:14,560
I did step on a rusty nail, but I had my tetanus shot.

121
00:14:16,560 --> 00:14:19,120
When I was in California, I stepped on a rusty

122
00:14:20,720 --> 00:14:24,560
something, and that's a metal something, I was walking barefoot in

123
00:14:24,560 --> 00:14:35,360
Tarzanna, I went every day to a French restaurant, the most exotic arms around ever,

124
00:14:35,360 --> 00:14:39,120
every day I would go and arms around and I would go to a French restaurant,

125
00:14:40,160 --> 00:14:48,880
and a Thai restaurant, and I think some Thai people stopped me on the way as well,

126
00:14:48,880 --> 00:14:56,240
and then back at the house, some people at the house would offer arms as well,

127
00:14:57,040 --> 00:15:06,480
so every day I was having waffles or pancakes with mounds of like real, it was high quality,

128
00:15:06,480 --> 00:15:15,840
this is LA sort of Hollywood, high so French restaurant, because it was owned actually by Thai people,

129
00:15:15,840 --> 00:15:20,880
but one day I stepped on a metal piece of metal and they took me right away to the,

130
00:15:21,520 --> 00:15:23,680
it was funny, they took me to the emergency room,

131
00:15:29,120 --> 00:15:32,640
and then it ended up costing like $500 to what?

132
00:15:33,600 --> 00:15:36,720
Can't remember just to have it checked out and then get me a tetanus shot, I think.

133
00:15:38,560 --> 00:15:39,760
It was like $500.

134
00:15:39,760 --> 00:15:45,440
That sounds about right, yes, to walk in, to walk in the door, yes, $500.

135
00:15:47,040 --> 00:15:58,240
Well, Americans, crazy people, crazy country, but my point there was totally off track is

136
00:16:00,240 --> 00:16:05,520
there's nothing wrong with rest, so it got me thinking, I had always thought that rest was

137
00:16:05,520 --> 00:16:11,440
rust give you tetanus, right, rust gives you lock jaw, because if you get rust in your bloodstream,

138
00:16:11,440 --> 00:16:17,600
it must cause what we call lock jaw or tetanus, where your body is a really horrible sickness,

139
00:16:19,520 --> 00:16:24,480
but it's not actually the case, there's a virus known virus bacteria,

140
00:16:25,360 --> 00:16:32,320
and the bacteria lives in the rusty metal, because rust makes holes in metal,

141
00:16:32,320 --> 00:16:36,880
so you have a metal nail, and when the rust creates pockets,

142
00:16:37,920 --> 00:16:43,120
and pockets where bacteria can live, and one of the bacteria that likes to live in those pockets

143
00:16:43,120 --> 00:16:50,400
or is potentially living in those pockets, is the tetanus bacteria, I can't remember what they're

144
00:16:50,400 --> 00:16:56,720
called, and it's just so perfect because it's like injecting yourself with those bacteria

145
00:16:56,720 --> 00:17:01,200
when you get a nail right into your deep into your bloodstream, that's why you get it,

146
00:17:01,200 --> 00:17:05,040
that you don't always get it, and it's certainly not from the rest, so my point being,

147
00:17:05,040 --> 00:17:11,760
consciousness, you could argue, it's not a problem, it's neutral, but consciousness is like this

148
00:17:11,760 --> 00:17:18,320
rust, you know, it's got lots of bad things, potentially in it, or it's a, it's very nature,

149
00:17:19,200 --> 00:17:28,640
allows it to be a vessel for bad things, there's an analogy for you, so that's the dhamma,

150
00:17:28,640 --> 00:17:37,360
there's an dhamma today, you know, good teaching, got it, you've got to stand in awe of the Buddha,

151
00:17:38,080 --> 00:17:45,440
how powerful his teachings are, all right, I always sound like a religious zealot, and I never

152
00:17:45,440 --> 00:17:49,120
thought I wouldn't be a religious zealot, because I always made fun of religious zealots,

153
00:17:49,120 --> 00:18:02,160
but you know, if they were right, then they would be justified to be zealous, we're right,

154
00:18:03,680 --> 00:18:10,320
well, they don't mean it quite like that, it's not with gloating over for sure, but

155
00:18:11,360 --> 00:18:12,320
Buddha is awesome.

156
00:18:12,320 --> 00:18:24,080
Oh, Fernando sent me some subtitles, okay, let me do that now, let's do it while we're online,

157
00:18:24,080 --> 00:18:31,760
sent me Spanish subtitles, this example to everyone, wait, dhamma don't go,

158
00:18:31,760 --> 00:18:40,480
okay, so we're going to do a fine video,

159
00:18:52,960 --> 00:18:55,440
we're still at this old series on how to meditate,

160
00:18:55,440 --> 00:18:59,680
this one is how to meditate,

161
00:19:10,080 --> 00:19:14,960
we should really redo these videos again now, how many years old already,

162
00:19:14,960 --> 00:19:26,640
set a video of language, which language English, English or English, okay, that's funny,

163
00:19:27,680 --> 00:19:32,640
there's two language choices, three language choices, English and English United Kingdom and

164
00:19:32,640 --> 00:19:39,680
French Canada, why is America the default English now? British aren't English enough,

165
00:19:39,680 --> 00:19:46,640
is this through Google? Yeah, English United Kingdom as the default.

166
00:19:51,200 --> 00:19:55,040
Okay, so we've published, we've already published several, I remember I did a thing back

167
00:19:55,040 --> 00:20:01,600
back in the day where I got people to send me some titles, so we've got English, Indonesian, Portuguese,

168
00:20:01,600 --> 00:20:11,040
Swedish, and Vietnamese, so I'm going to add for Nando's Spanish subtitles,

169
00:20:13,120 --> 00:20:16,480
don't know them, stay with them.

170
00:20:20,320 --> 00:20:23,760
How do you access these quantities? Are these on your channel as well?

171
00:20:23,760 --> 00:20:31,120
And you go to CC, we think. Okay, so we've got Spanish, Spanish Latin Americans,

172
00:20:31,120 --> 00:20:37,680
Spanish Mexico and Spanish Spain, Fernando's from Spain I think, right? So I'm going to say

173
00:20:37,680 --> 00:20:45,040
I think Fernando's from Mexico. Oh, no, Fernando there, Fernando, which country is it?

174
00:20:46,000 --> 00:20:49,920
Well, for the flags, meditation, like Mexico,

175
00:20:49,920 --> 00:20:54,560
which language are you using Spanish Spain or Spanish Mexico?

176
00:20:56,400 --> 00:20:57,840
They're not that different, are they?

177
00:21:01,520 --> 00:21:06,640
I don't know, most people in my area that speak Spanish are from Puerto Rico,

178
00:21:06,640 --> 00:21:21,440
about 35% of the people in my town are from Puerto Rico, and that's a little different as well.

179
00:21:21,440 --> 00:21:29,200
Fernando said Latin America. So what, that's different from Spain?

180
00:21:29,200 --> 00:21:33,520
Spain, Latin America, there.

181
00:21:36,800 --> 00:21:45,120
I'm sort of fine.

182
00:21:45,120 --> 00:21:58,000
All I mean, we need those as the city in the videos.

183
00:21:58,000 --> 00:22:05,200
I care. A como medita. And as the premier video,

184
00:22:05,200 --> 00:22:16,320
no hablari, a caracad, the quail, S L significado de la palabra meditasio,

185
00:22:16,320 --> 00:22:26,000
and prima prima dugar, the palabra meditasio and wed, wed, wed,

186
00:22:26,000 --> 00:22:39,200
wed, wed, wed, wed, wed, wed, wed, wed, wed, wed, wed, this prevail.

187
00:22:43,280 --> 00:22:47,520
The 29% of English comes from Latin Remember that this way.

188
00:22:47,520 --> 00:23:03,560
When you write in this song, the possible comes from post, or almost post, and the sumus

189
00:23:03,560 --> 00:23:05,560
sumus.

190
00:23:05,560 --> 00:23:19,560
Okay, it's up, I think, oh no, it's not, uh, publish, okay, it's up, get out, check it out,

191
00:23:19,560 --> 00:23:20,560
everyone.

192
00:23:20,560 --> 00:23:25,560
Go to my first time in meditating, meditating here.

193
00:23:25,560 --> 00:23:53,560
Well, uh, at the end of the video, it's a birthday, it has a thing say talking about the DVD,

194
00:23:53,560 --> 00:23:57,560
and that's why people keep, are still contacting me about the DVD, because these videos

195
00:23:57,560 --> 00:24:02,560
I'll talk about it, I'll have a, I'll have a, I'll have a, I'll have a button.

196
00:24:02,560 --> 00:24:03,560
To remove that.

197
00:24:03,560 --> 00:24:20,560
We're not doing the DVDs at the end of it.

198
00:24:20,560 --> 00:24:25,560
The website may mention the DVD as well.

199
00:24:25,560 --> 00:24:27,560
Can you get rid of it?

200
00:24:27,560 --> 00:24:28,560
Yeah.

201
00:24:28,560 --> 00:24:35,560
You know, some mentions about the download again, I'm a bit torrent, is that still?

202
00:24:35,560 --> 00:24:36,560
I don't know.

203
00:24:36,560 --> 00:24:37,560
Is that still current?

204
00:24:37,560 --> 00:24:39,560
I can get rid of it.

205
00:24:39,560 --> 00:24:46,560
It's all on YouTube, that's good enough, we need to DVD.

206
00:24:46,560 --> 00:24:58,560
Yeah, it says our how to meditate DVD is available via a bit torrent.

207
00:24:58,560 --> 00:25:09,560
So, I'll just get rid of that.

208
00:25:09,560 --> 00:25:26,560
Okay, so how are we doing any questions?

209
00:25:26,560 --> 00:25:30,560
No, you have answered all the questions one day.

210
00:25:30,560 --> 00:25:35,560
We finally come to that point, no?

211
00:25:35,560 --> 00:25:38,560
That's a good thing, right?

212
00:25:38,560 --> 00:25:44,560
It's a good thing.

213
00:25:44,560 --> 00:25:49,560
It's fine.

214
00:25:49,560 --> 00:25:53,560
Tomorrow we're going to try the medmod meditation mob thing again.

215
00:25:53,560 --> 00:25:59,560
We'll see if that works, how that works.

216
00:25:59,560 --> 00:26:02,560
There's time people are actually going to come out.

217
00:26:02,560 --> 00:26:04,560
What else?

218
00:26:04,560 --> 00:26:11,560
Oh, I think I mentioned, I think we're going to mention that the robot arrived.

219
00:26:11,560 --> 00:26:12,560
Sorry?

220
00:26:12,560 --> 00:26:15,560
I believe we're going to mention that the robot arrived?

221
00:26:15,560 --> 00:26:18,560
Yeah, we're going to go ahead.

222
00:26:18,560 --> 00:26:21,560
Robes, I guess it's a set, right?

223
00:26:21,560 --> 00:26:38,560
Okay, well, I'm going to go then.

224
00:26:38,560 --> 00:26:42,560
Thank you all for coming out, for tuning in.

225
00:26:42,560 --> 00:26:47,560
We're practicing with us, big long list of people.

226
00:26:47,560 --> 00:26:51,560
Have a good night, and thank you, Robin, for joining me.

227
00:26:51,560 --> 00:27:18,560
Thank you, Bhante.

