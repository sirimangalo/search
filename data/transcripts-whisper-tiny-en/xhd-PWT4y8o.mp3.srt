1
00:00:00,000 --> 00:00:07,000
So welcome everyone to another episode of Monk Radio.

2
00:00:07,000 --> 00:00:13,000
The main feature is always to answer people's questions

3
00:00:13,000 --> 00:00:19,000
about meditation and Buddhism and the monastic life.

4
00:00:19,000 --> 00:00:25,000
Now, I do have some idea always to add more to the sessions.

5
00:00:25,000 --> 00:00:28,000
First thing always is to do announcements.

6
00:00:28,000 --> 00:00:34,000
One announcement I'd like to make is an intention to maybe do some interviews

7
00:00:34,000 --> 00:00:38,000
or allow guest speakers to come on.

8
00:00:38,000 --> 00:00:45,000
Because it would be nice to have some sort of an international,

9
00:00:45,000 --> 00:00:55,000
maybe even unbiased or non-sectarian for them

10
00:00:55,000 --> 00:01:01,000
for people to talk about the good things that they're doing in Buddhist realm.

11
00:01:01,000 --> 00:01:13,000
So maybe see if we can coerce some prominent Buddhists to come on and talk to people.

12
00:01:13,000 --> 00:01:16,000
Anyway, announcements here.

13
00:01:16,000 --> 00:01:25,000
So today we had a very special ceremony where we invited 30 monks

14
00:01:25,000 --> 00:01:28,000
and gave them lunch.

15
00:01:28,000 --> 00:01:33,000
Last night there was a special talk that they gave for the laypeople

16
00:01:33,000 --> 00:01:37,000
and we gave lunch to 30 monks.

17
00:01:37,000 --> 00:01:41,000
Now, there's something I can do here where I can actually show you pictures.

18
00:01:41,000 --> 00:01:46,000
But I tried it last time and it kind of crashed.

19
00:01:46,000 --> 00:01:48,000
I tried it earlier.

20
00:01:48,000 --> 00:01:55,000
So I'll see if I can find it here.

21
00:01:55,000 --> 00:02:03,000
I'll see if I can do this again and let people let you see.

22
00:02:03,000 --> 00:02:06,000
A little bit of our monastery.

23
00:02:06,000 --> 00:02:09,000
Let's see if this works.

24
00:02:09,000 --> 00:02:19,000
I'm going to share my screen here here where this is a picture of our main hall

25
00:02:19,000 --> 00:02:23,000
and some of the monks and the little monks sitting down.

26
00:02:23,000 --> 00:02:25,000
We didn't get pictures of the whole ceremony.

27
00:02:25,000 --> 00:02:28,000
I just asked someone near the end to take some pictures.

28
00:02:28,000 --> 00:02:30,000
So everyone's already eaten lunch.

29
00:02:30,000 --> 00:02:37,000
I was sitting right here and that's my bowl, actually.

30
00:02:37,000 --> 00:02:40,000
And these are the laypeople.

31
00:02:40,000 --> 00:02:48,000
They've discovered up all the food and they're now listening to this talk given by a monk.

32
00:02:48,000 --> 00:02:51,000
The old monk that you see in a second.

33
00:02:51,000 --> 00:02:56,000
Here are some more of the monks who took part in the ceremony.

34
00:02:56,000 --> 00:02:59,000
There are three, four, and monks who came.

35
00:02:59,000 --> 00:03:03,000
There's one very serious looking one.

36
00:03:03,000 --> 00:03:06,000
And this old monk down here is the old monk in our monastery.

37
00:03:06,000 --> 00:03:14,000
The reason why we did this ceremony was for him because he's getting old and he's been quite sick lately.

38
00:03:14,000 --> 00:03:20,000
So our head monk was concerned that he might be on his way out.

39
00:03:20,000 --> 00:03:28,000
And if he is on his way out, then it would be nice to do something for him before he makes an exit.

40
00:03:28,000 --> 00:03:31,000
Now, we've just given out all these robes.

41
00:03:31,000 --> 00:03:36,000
These ones we brought from Thailand, these ones in boxes, and 30 robes.

42
00:03:36,000 --> 00:03:48,000
I had to carry in suitcases and boxes, or 20 robes that one family kindly donated for the purpose.

43
00:03:48,000 --> 00:03:51,000
Here's the old monk giving the Dhammatak.

44
00:03:51,000 --> 00:03:54,000
It wasn't that much of a Dhammatak.

45
00:03:54,000 --> 00:03:59,000
It was just the standard after giving lots of gifts to say thanks.

46
00:03:59,000 --> 00:04:01,000
He gave a Dhammatak.

47
00:04:01,000 --> 00:04:03,000
And of course, it was in Singhalah, so I'm not quite sure.

48
00:04:03,000 --> 00:04:11,000
It was a lot about doing good deeds and giving gifts and the merit of it.

49
00:04:11,000 --> 00:04:18,000
Because it was all directed towards the old monk and how he should feel good about what he's done here.

50
00:04:18,000 --> 00:04:23,000
Really what we did for him, but on his behalf, here's some more little monks.

51
00:04:23,000 --> 00:04:25,000
And there's me sitting outside.

52
00:04:25,000 --> 00:04:27,000
I didn't want to get involved.

53
00:04:27,000 --> 00:04:32,000
They wanted to offer them to me, but I said, well, we're the ones who gave them my wish and we received them again.

54
00:04:32,000 --> 00:04:37,000
So I sat outside and let the monks get the robes.

55
00:04:37,000 --> 00:04:41,000
Here's the old monk with some of the lay people.

56
00:04:41,000 --> 00:04:43,000
Most of the lay people were outside.

57
00:04:43,000 --> 00:04:52,000
There were actually quite a few people came, but there was very little room because our hall is, as you can see, quite small.

58
00:04:52,000 --> 00:04:54,000
Here's another shot.

59
00:04:54,000 --> 00:04:59,000
And here's some shots outside to give you an idea of the number of people.

60
00:04:59,000 --> 00:05:04,000
So you see there were quite a few people all sitting around outside.

61
00:05:04,000 --> 00:05:07,000
And when they gave lunch, these people all came in and helped to serve the monks.

62
00:05:07,000 --> 00:05:09,000
It's really quite efficient the way they do things.

63
00:05:09,000 --> 00:05:11,000
Quite impressive.

64
00:05:11,000 --> 00:05:15,000
This is our Riharas or some of the boys were hanging out.

65
00:05:15,000 --> 00:05:17,000
These are all good guys.

66
00:05:17,000 --> 00:05:19,000
These are people who were very helpful.

67
00:05:19,000 --> 00:05:29,000
So they were up very late at night working on setting up this ceremony and everything.

68
00:05:29,000 --> 00:05:37,000
And here's some more just random pictures and some of the kids sitting up under the body tree.

69
00:05:37,000 --> 00:05:44,000
Really nice community and really nice ceremony.

70
00:05:44,000 --> 00:05:49,000
So that's all. Let's see if we can now go back.

71
00:05:49,000 --> 00:05:51,000
Okay, we got our video back.

72
00:05:51,000 --> 00:05:52,000
Great.

73
00:05:52,000 --> 00:05:54,000
Okay, so that's one announcement.

74
00:05:54,000 --> 00:05:56,000
Not much else to announce.

75
00:05:56,000 --> 00:06:01,000
I keep announcing the imminent of new cuties coming.

76
00:06:01,000 --> 00:06:08,000
And so not that it's that big of a deal, but there are two new cuties on their way.

77
00:06:08,000 --> 00:06:11,000
Today they finally finished the electricity.

78
00:06:11,000 --> 00:06:14,000
So they should really be usable now.

79
00:06:14,000 --> 00:06:19,000
And that's good because tomorrow we have a man from Finland coming.

80
00:06:19,000 --> 00:06:20,000
And that's about it.

81
00:06:20,000 --> 00:06:22,000
Oh, and there are four time meditators coming.

82
00:06:22,000 --> 00:06:25,000
I don't know if I mentioned that last week.

83
00:06:25,000 --> 00:06:27,000
These are four, three of them I know.

84
00:06:27,000 --> 00:06:29,000
I don't know who the fourth person is.

85
00:06:29,000 --> 00:06:35,000
But there's some of my longtime supporters from Thailand who I actually met in America.

86
00:06:35,000 --> 00:06:41,000
Once they came to America to practice meditation or to visit.

87
00:06:41,000 --> 00:06:52,000
And then ended up getting coerced into doing a meditation course before going to Las Vegas.

88
00:06:52,000 --> 00:06:54,000
So they're becoming.

89
00:06:54,000 --> 00:07:00,000
And then next month on the sixth Peter will be here or the eighth I think sixth or eighth.

90
00:07:00,000 --> 00:07:02,000
Peter will be here from Canada.

91
00:07:02,000 --> 00:07:06,000
And there's another man coming and I can't remember where he's from.

92
00:07:06,000 --> 00:07:08,000
Says he's coming.

93
00:07:08,000 --> 00:07:10,000
And so on and so on.

94
00:07:10,000 --> 00:07:12,000
Lots of good stuff.

95
00:07:12,000 --> 00:07:16,000
Sumeda says she might be leaving and she's sick and she wants to go.

96
00:07:16,000 --> 00:07:21,000
Maybe back to a place where the food is a little bit better for her stomach.

97
00:07:21,000 --> 00:07:22,000
I'm not sure.

98
00:07:22,000 --> 00:07:27,000
And I think she wants to deal with some family issues and so on.

99
00:07:27,000 --> 00:07:32,000
So it might be alone for the wasps for the wasps period.

100
00:07:32,000 --> 00:07:35,000
Apart from meditators coming and going.

101
00:07:35,000 --> 00:07:41,000
And then in November there are already people who are planning to come to Thailand to practice.

102
00:07:41,000 --> 00:07:42,000
Which is really great.

103
00:07:42,000 --> 00:07:50,000
It's great to see such participation that I am actually able to conduct things this way.

104
00:07:50,000 --> 00:07:56,000
Because now I'm a little bit ungrounded, I guess you can say.

105
00:07:56,000 --> 00:07:59,000
But you know you think of it the Buddha as well.

106
00:07:59,000 --> 00:08:06,000
If you read the story of the Buddha he was not so grounded in the sense of staying put.

107
00:08:06,000 --> 00:08:10,000
Of course the Buddha was special and new where to go.

108
00:08:10,000 --> 00:08:13,000
For us we kind of have to do some guesswork.

109
00:08:13,000 --> 00:08:23,000
But being able to travel is quite useful because of the many different audiences.

110
00:08:23,000 --> 00:08:27,000
The benefit that you can be in different spheres.

111
00:08:27,000 --> 00:08:31,000
So in Thailand I can be a benefit also to the Thai people.

112
00:08:31,000 --> 00:08:35,000
Because I speak much better Thai than I do singhaha.

113
00:08:35,000 --> 00:08:40,000
Here I can only be a benefit for people.

114
00:08:40,000 --> 00:08:42,000
And that's not true actually.

115
00:08:42,000 --> 00:08:45,000
I also be a benefit to the singhalese people.

116
00:08:45,000 --> 00:08:51,000
Because I am helping out and some singhalese people who speak English.

117
00:08:51,000 --> 00:08:57,000
Because our planning making plans to come and have actually come to practice meditation.

118
00:08:57,000 --> 00:09:02,000
We had of course Damandina was one woman who became a nun for some time.

119
00:09:02,000 --> 00:09:10,000
And now has disrobed to deal with her family issues.

120
00:09:10,000 --> 00:09:15,000
And has plans to continue this path.

121
00:09:15,000 --> 00:09:19,000
But I have to see how that goes in the future.

122
00:09:19,000 --> 00:09:23,000
Apart from that everything continues as normal.

123
00:09:23,000 --> 00:09:27,000
I think everything is going well.

124
00:09:27,000 --> 00:09:29,000
Our online community is doing great.

125
00:09:29,000 --> 00:09:34,000
Lots of questions and answers and updates and so on for everyone.

126
00:09:34,000 --> 00:09:37,000
If you haven't joined our online community please.

127
00:09:37,000 --> 00:09:41,000
Be welcome at my.ceremungalow.org.

128
00:09:41,000 --> 00:09:44,000
My is a little bit of a...

129
00:09:44,000 --> 00:09:49,000
Well it just means it becomes yours.

130
00:09:49,000 --> 00:09:52,000
You don't have to just sit back and be an audience member.

131
00:09:52,000 --> 00:09:54,000
So it means you can actually take part.

132
00:09:54,000 --> 00:09:57,000
It doesn't mean you have to cling to it like this is me or this is mine.

133
00:09:57,000 --> 00:10:00,000
It is obviously that goes against the Buddhist teaching.

134
00:10:00,000 --> 00:10:05,000
But the meaning is rather that it should be an active, proactive thing.

135
00:10:05,000 --> 00:10:09,000
So you can go to my.ceremungalow.org.

136
00:10:09,000 --> 00:10:16,000
The Buddhist social community network or something like that.

137
00:10:16,000 --> 00:10:21,000
And join up and we've got a question and answer forum at

138
00:10:21,000 --> 00:10:23,000
ask.ceremungalow.org.

139
00:10:23,000 --> 00:10:28,000
If you can't get your questions answered here or if you can't wait for this.

140
00:10:28,000 --> 00:10:33,000
I've been going answer, ask questions there as well.

141
00:10:33,000 --> 00:10:36,000
Yes, all I can think of for now.

142
00:10:36,000 --> 00:10:38,000
So that's announcements for today.

143
00:10:38,000 --> 00:10:42,000
And now we can get on with the question ask.

