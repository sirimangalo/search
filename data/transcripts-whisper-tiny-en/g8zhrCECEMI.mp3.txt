Is an Arahand as enlightened as the Buddha? Should he, she not know as much as the Buddha?
It's an interesting way of phrasing it.
The word Arahand is not the same as the word Buddha.
They're spelled differently for starters.
But the point being that an Arahand is a different thing for the Buddha.
But the Buddha was an Arahand.
So if you have one of these, would it be those Venn diagrams?
The Buddha?
Right.
And I was going to draw a Venn diagram.
Big circle is the Arahand.
And then inside it is a little circle of Buddhas.
So all Buddhas are Arahans.
Just as all sort of rectangles.
But that's a small circle. The number of Arahans is much bigger.
So there are many Arahans that are not Buddhas.
But if you look at the two circles, all Buddhas are Arahans.
So what is the difference?
I hope I'm correct in saying this, but this is how I've often explained it.
And maybe someone can correct me if I'm wrong.
But my understanding of putting the difference into one sentence is that
a Buddha has to know everything.
An Arahand only has to let go of everything.
So in one sentence, that's my answer is to the difference.
And really answering your question from my understanding.
So it depends really what you mean by enlightenment.
If you're meaning of enlightenment, does an Arahand have more greed, anger, and delusion than a Buddha?
No.
Neither one has any greed, anger, and delusion.
They both let go of everything.
And they both come to understand the four noble truths.
But the Buddha on top of that knows everything.
In top of letting go of everything and being an Arahand,
he's also come to know everything and be a Buddha.
To clarify that, that's the one sentence answer, but to clarify it a little bit.
It doesn't mean that the Buddha has a big, brainful of facts.
It means that whatever question the Buddha would ask,
say, what is the answer to this?
He could find the answer to it, or whether there was an answer.
He would find out the truth behind anything that he said is wind on.
That's what it means to be a Buddha, according to our tradition.
So what we should understand from that is it may be that we're wrong,
that the ancient Buddha, the historic Buddha, didn't have that.
That's not what our claim is.
Our claim is that for someone to consider themselves a fully enlightened Buddha,
they have to know everything.
They have to be able to send their mind and have unobstructed knowledge and vision.
So that anywhere they send their mind, they're able to know the truth of it.
That's our understanding.
We may be wrong.
It may be that a Buddha, but that's impossible.
But that's what the texts say.
And that's our understanding of what it means to be a Buddha.
It's important thing to say, because if it's true, then it means becoming a Buddha is not an easy thing.
In comparison to the incredibly difficult thing of becoming an Arahant.
So becoming an Arahant, giving up all greed, anger and delusion,
a very difficult thing.
So how much more difficult would it be to have this on top of that, this unobstructed knowledge and vision,
so that anything you think of, anything you consider, you immediately know the answer to.
And without any reserve, without any qualification,
except for those things that have no answers, then you come to know there's no answer to that and so on.
But as to whether they should, I guess it's just the grammar that you're using.
But an Arahant who is not a Buddha will not have this unobstructed knowledge and vision.
An Arahant who is also a Buddha will have that knowledge unobstructed knowledge and vision.
But as for the should, if you really mean, if you're really using that as a proper for them,
it's proper for them to learn as much as they can and to get as close to becoming fully Buddha as possible in their lifetime.
And it's what they would do, I should think, always studying, always developing themselves.
And their knowledge and their clarity of mind, even though they have no green anchor in delusion,
they would just naturally incline towards that sort of thing.
Or not, maybe they would just sit in meditation until they pass away.
So I hope that helps.
