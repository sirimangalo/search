WEBVTT

00:00.000 --> 00:25.640
Okay, good evening everyone, welcome to our weekly question and answer session.

00:25.640 --> 00:30.960
I think the audio is better.

00:30.960 --> 00:43.480
Let me know if you could, if it's still low, you'll hear it, if it sounds like it's too

00:43.480 --> 00:44.480
slow.

00:44.480 --> 00:50.040
If you compare it the last video and the audio sounded like it was too slow.

00:50.040 --> 00:57.520
Somehow it isn't too slow, so that's kind of confusing, but it sounds very deep.

00:57.520 --> 01:03.560
So hopefully that's been fixed, luckily it didn't change the, didn't make it difficult

01:03.560 --> 01:10.040
to listen to, just kind of distracting, I suppose.

01:10.040 --> 01:21.600
Okay, so without further ado we're answering questions on our meditation site, so no

01:21.600 --> 01:27.240
adding new questions, if you add new questions I may or may not answer them, I may

01:27.240 --> 01:29.280
wait till next week.

01:29.280 --> 01:34.640
We have 14 questions, we had a couple more and I did delete a couple, so again if you feel

01:34.640 --> 01:42.560
like your question was wrongly deleted, you can meditate on it, and if you still feel

01:42.560 --> 01:49.880
like it was wrongly deleted, you're welcome to submit it again.

01:49.880 --> 01:57.760
Is it okay to meditate in a chair or should I challenge myself by sitting on the floor?

01:57.760 --> 02:01.000
What's good to challenge yourself?

02:01.000 --> 02:04.000
It's okay to sit in a chair.

02:04.000 --> 02:10.520
So it's not an either or situation, sitting on floor if it's a challenge, particularly

02:10.520 --> 02:15.400
it's probably a challenge in terms of pain, so the question of how challenging is it

02:15.400 --> 02:20.480
if it's to the point where you just drive yourself crazy.

02:20.480 --> 02:26.760
Then you want to take it slowly, but it also is an indication that you have, I was going

02:26.760 --> 02:31.920
to say mental problems, you have mental problems, but not in the way we say it in English.

02:31.920 --> 02:37.600
So mental problems in the sense that you have an aversion to pain, that's a mental problem

02:37.600 --> 02:40.280
literally speaking.

02:40.280 --> 02:43.920
So it's something that would be great if you could work out because then you wouldn't

02:43.920 --> 02:53.480
have that mental problem of being subjected or vulnerable to pain, so it's a great challenge

02:53.480 --> 03:03.960
to take up and to accomplish, so definitely, I mean what you don't want is to be running

03:03.960 --> 03:09.880
away from pain always, you don't want to cultivate the aversion to pain by running away

03:09.880 --> 03:16.240
from it, that just makes things worse, it makes the pain worse, really, so if you're always

03:16.240 --> 03:23.320
avoiding it by sitting in a chair well, you're going to run into problem in the long run.

03:23.320 --> 03:30.760
Here we have a quote from Mahasiz Hayada, and so the first thing I'll say is that a lot

03:30.760 --> 03:38.880
of the English translations are not that great or not perfect, they're actually quite impressive,

03:38.880 --> 03:45.280
but I wouldn't rely on an English translation of what Mahasiz says entirely in all the

03:45.280 --> 03:48.280
nuances.

03:48.280 --> 03:57.040
But what he's saying here is you have to understand what is meant by suffering, so to

03:57.040 --> 04:01.320
put it simply, we'll talk about two types of suffering.

04:01.320 --> 04:07.520
The suffering that we understand generally is when you react to something, right, in

04:07.520 --> 04:13.040
wisdom this is a valid explanation of suffering, so you experience pain and while the pain

04:13.040 --> 04:16.560
isn't really suffering but you don't like it in that suffering, it's because you don't

04:16.560 --> 04:22.440
like it, sometimes you experience pain when you're doing sports or exercising and you really

04:22.440 --> 04:27.080
don't feel upset about it, you feel actually good about it, so you can see that pain isn't

04:27.080 --> 04:33.160
really the problem, it's totally our mind state, so in that case you're right to question

04:33.160 --> 04:40.480
this and to say, wait a minute, it definitely our craving right now, our reactions right

04:40.480 --> 04:45.240
now, that's what's causing us suffering, but here Mahasiz says, present craving is not

04:45.240 --> 04:50.080
the origin of present suffering, it's the origin of suffering in future life, so what he's

04:50.080 --> 04:58.720
talking about is actually equally important, and equally important understanding of suffering

04:58.720 --> 05:05.600
and that's that everything that arises has the quality in it of suffering, of dukka,

05:05.600 --> 05:15.240
I think I'm suffering is not a great translation, but it's in the ballpark because what it means

05:15.240 --> 05:23.360
is that these things are not valuable in the sense of if I cling to this it's going

05:23.360 --> 05:31.360
to do something good for me, they don't have that quality, they have the quality of it's

05:31.360 --> 05:37.840
like a fire as a quality of heat, but only if you touch it, if you don't touch it you won't

05:37.840 --> 05:46.520
get burned, but you still say that fire's hot, the fire in itself, the heat is only a part

05:46.520 --> 05:51.720
of experience when you go and if you put your hand in it you'll get burned, and so when

05:51.720 --> 05:57.640
we say that everything that arises at everything, every experience as a quality of suffering,

05:57.640 --> 06:03.920
it's still related to experiences, but it's also called suffering in itself, it's called

06:03.920 --> 06:15.320
something that is not capable of making you happy, that's happiness, it won't be happiness,

06:15.320 --> 06:20.040
this is going to make me happy, it's not going to make you happy, so we call it dukka,

06:20.040 --> 06:28.080
so what he means is that in this life our life comes about because of past craving, because

06:28.080 --> 06:32.880
of craving before we were born we can't help that, all the suffering of this life suffering,

06:32.880 --> 06:38.160
the sense of all the experiences that we have to have, and if you want to talk conventionally

06:38.160 --> 06:43.120
also all the bad experiences we're going to have, so not getting what we want, getting

06:43.120 --> 06:50.160
what we don't want, bold age, sickness, death, all these things, all that is not because of

06:50.160 --> 06:56.280
our craving right now, and it's not going to be mitigated by that, you think, okay stop

06:56.280 --> 07:00.640
cravings, that means I don't have to get old sickened, I will of course not, that means

07:00.640 --> 07:05.080
I don't have to meet with all sorts of bad things, no of course not, you'll still meet

07:05.080 --> 07:09.320
with all that suffering, it's just not the suffering you're thinking about, I've kind

07:09.320 --> 07:15.320
of conditioned you all to think of sufferings, our reaction, because that's crucial in

07:15.320 --> 07:22.480
understanding what is conception of suffering, you don't actually suffer from the suffering

07:22.480 --> 07:28.640
if you're enlightened on the one hand, now you do technically suffer because you have

07:28.640 --> 07:34.400
to get sick, you have to get old, you have to die and so on and so on, but that's only

07:34.400 --> 07:41.680
technically, your mind is so much at peace that you're invulnerable to it, you're invincible

07:41.680 --> 07:46.240
even in the face of suffering, so that's, you have to understand it's being used in

07:46.240 --> 07:56.560
different ways, but it's a good question, what is, I mean he's absolutely correct, he's

07:56.560 --> 08:01.360
standing something, he's talking differently, I mean what he's talking about is something

08:01.360 --> 08:06.880
important that we say the cause of suffering is actually in one sense, stuff that happened

08:06.880 --> 08:14.280
in the past life, and so in some sense the only way we can be free from suffering is

08:14.280 --> 08:19.440
when we die in this life if we have no grieving left, then we don't have to come back

08:19.440 --> 08:27.680
and experience all sorts of suffering, but that's only one sense, for practical purposes

08:27.680 --> 08:32.840
we talk in a different way and we say your reactions and if you see things clearly you

08:32.840 --> 08:41.520
can't really call it suffering because you're not suffering from it, again this I think

08:41.520 --> 08:47.080
as soon as many people get on this website it stops working, so clearly there's something

08:47.080 --> 08:52.560
some limit that's being met, and we've talked about this before and my IT team assures

08:52.560 --> 09:01.240
us that they're an old such limit, but I don't believe him, I don't know it's working

09:01.240 --> 09:12.320
again, that's going on here, okay, how do I raise myself as steam, you're in the wrong place,

09:12.320 --> 09:20.320
you're asking the wrong person, I feel like most of my life is affected, well I lost

09:20.320 --> 09:25.280
them, it's affected by what I think of myself, people have told me that I'm smart but

09:25.280 --> 09:31.640
I can't advance in math or any other subject without getting sad and frustrated, but part

09:31.640 --> 09:36.920
of why you get sad and frustrated may be your concern about self, you're most likely

09:36.920 --> 09:41.160
is the fact that you have a sense of self esteem which you should get rid of, you should

09:41.160 --> 09:48.400
have no self esteem because it's useless for you to compare yourself to others, to hold

09:48.400 --> 09:53.080
yourself up to some standard that you have to be and then get frustrated when you don't

09:53.080 --> 09:59.480
need it and so on, or even just stress and stress in order to meet it, I mean what does

09:59.480 --> 10:07.840
it mean, what is the self, yourself in terms of everything in this life is meaningless,

10:07.840 --> 10:12.200
most religions realize that most spiritual practices realize that they still believe in

10:12.200 --> 10:18.840
self but they say all this stuff, good at math, that's not self, so they'll say things

10:18.840 --> 10:24.480
like this self has a higher purpose, of course it has to have a higher purpose because

10:24.480 --> 10:27.560
math is something you're going to forget when you get old and you're going to lose when

10:27.560 --> 10:34.600
you die, so that certainly is not self, but Buddhism goes deeper and says you know any

10:34.600 --> 10:43.360
kind of attachment to the idea of self, self esteem, it's going to lead you to be reactionary

10:43.360 --> 10:48.880
when people, as you say, when you're affected by what people think of you, you're very

10:48.880 --> 10:54.320
happy and pleased when they when they speak good about yourself, when they speak bad about

10:54.320 --> 10:59.360
yourself, you're you're you feel threatened and you feel like you have to defend yourself

10:59.360 --> 11:06.000
or you feel sad or you feel worthless, well guess what, you know what I'm going to say next,

11:06.000 --> 11:13.120
you are worthless, we're all worthless because worth is just a measure based on the individual,

11:13.120 --> 11:23.480
what does it mean to say worth something, you could say in the context of enlightenment,

11:23.480 --> 11:34.400
insight meditation is worthwhile, in context of bliss practice, your teacher is worth something,

11:34.400 --> 11:38.560
someone said that I'm worth something because I'm teaching them, but it's in the context

11:38.560 --> 11:47.160
of your practice, those are only really concepts, reality is not worth anything, so they say

11:47.160 --> 11:55.200
you're not worth anything, the reality of you is just experiences and so to say worth something

11:55.200 --> 12:02.000
is really meaningless in that context, when you see something, is that worth something,

12:02.000 --> 12:09.560
what would it mean to say seeing is worth something, seeing is it has no value attached to it,

12:09.560 --> 12:15.240
it only is an experience, the problem really is that we put values on things, we say this

12:15.240 --> 12:22.240
is worth, this is good, this is bad, then we judge and react and suffer as a result, things

12:22.240 --> 12:27.320
just are, I mean, reality is not, it sounds so dismal the same thing as worth anything,

12:27.320 --> 12:34.360
but it's actually so liberating and broadening because suddenly reality is, you have the

12:34.360 --> 12:42.040
whole spectrum of reality, you're not confined to this small, meaningless life as a human

12:42.040 --> 12:51.560
being that really is just attachment to meaningless things, you have reality, you have truth

12:51.560 --> 13:01.920
that makes any sense, are you aware of people getting off their psychiatric drugs with

13:01.920 --> 13:09.480
the health of mindfulness and mate time, not really, but mindfulness without experiencing

13:09.480 --> 13:14.400
the so many unwanted withdrawal effects, well you can't avoid withdrawal effects, but

13:14.400 --> 13:20.520
you can be free from your reactions to them, I mean, many of the withdrawal effects are

13:20.520 --> 13:25.380
going to be reactions, so because there's a lot of habits associated, mental habits

13:25.380 --> 13:31.480
associated with this, so you can't turn off your mind, your mental habits, so yes, there

13:31.480 --> 13:39.440
is no way to avoid withdrawal effects, as far as getting off the drugs, do have one experience

13:39.440 --> 13:47.960
fairly recently of a man in Canada here who with my help was able to almost get off

13:47.960 --> 13:54.520
them, but then I was suddenly told that I couldn't help him get off the drugs because

13:54.520 --> 14:02.840
I was dangerously close to breaking the law or, you know, it's basically impersonating

14:02.840 --> 14:06.920
physician or something like that, I may have been breaking the law, but it's very dangerous

14:06.920 --> 14:15.080
legally speaking apparently, so I stopped, but I have seen good results and I can't think

14:15.080 --> 14:20.520
of any instance, I do know some people who have talked to me and sent through the practice

14:20.520 --> 14:26.840
on their own, listen watching my videos or practicing with some other methods, they

14:26.840 --> 14:32.280
were able to get off their drugs, but it's highly dependent on the individual, so it's

14:32.280 --> 14:39.520
highly case by case, depending on how serious it is and how serious the person is and

14:39.520 --> 14:51.200
what sort of abilities they have in their mind, how does mindfulness protect the person

14:51.200 --> 15:05.000
from being hurt by others, so it can't stop you from being hurt by others, but it can

15:05.000 --> 15:13.200
stop you from being hurt by others, I mean, you ask, ask, what in another, what can one

15:13.200 --> 15:20.280
person do to another person, if we think in an ultimate sense, the only thing one person

15:20.280 --> 15:27.600
can do to another person is present them with experiences, with a change in their experiences

15:27.600 --> 15:32.680
or with experiences that they wouldn't have experienced otherwise, all of those experiences

15:32.680 --> 15:39.160
are still seeing, hearing, smelling, tasting, feeling or thinking, those experience, those

15:39.160 --> 15:45.400
things people can do to us, now you can close your eyes so you don't see, but if they catch

15:45.400 --> 15:52.800
you, they certainly can make it so that you have to see things, right, or you have to hear

15:52.800 --> 15:58.920
things, or you have to smell or taste or feel or think things, that people can do to

15:58.920 --> 16:07.480
you, they can inflict pain, physical pain, physical pain being a feeling and bodily feeling,

16:07.480 --> 16:17.280
what they can do is hurt you in the sense of make you upset, make you stressed, make you sad.

16:17.280 --> 16:24.560
I think that's a simplistic statement because certainly they very easily can and we all have

16:24.560 --> 16:29.200
experiences of helping people can do this, but we don't get that that's a conditioned

16:29.200 --> 16:35.160
response and this conditioned response can be changed, so mindfulness is your question

16:35.160 --> 16:43.360
of mindfulness protects because mindfulness prevents a reaction, even momentarily you still

16:43.360 --> 16:47.920
have bad habits, so someone says something and you get angry, even though you still have

16:47.920 --> 16:53.640
that habit, at the moment when you say to yourself hearing, hearing, you don't get angry

16:53.640 --> 17:00.520
and so there it protects you from being hurt in terms of getting suffering from it,

17:00.520 --> 17:07.000
or someone says something and you they scare you or they make you sad, but when you

17:07.000 --> 17:13.200
say hearing, hearing, you don't get sad or you don't get afraid or so, and then of course

17:13.200 --> 17:18.320
when you're sad already and you say sad that it doesn't continue because it doesn't

17:18.320 --> 17:27.680
leave to more thoughts, more sadening thoughts, it keeps you present seeing things just

17:27.680 --> 17:33.840
as they are, mindfulness is designed to create objectivity, to cultivate, seeing things

17:33.840 --> 17:42.280
just as they are, okay here's some complicated questions, I don't think I fully understood

17:42.280 --> 17:51.600
everything in this, but I'll make a step at it, so it takes, it appears to take a moment

17:51.600 --> 17:55.520
for me to reestablish the context of my surroundings, you ask what is happening here,

17:55.520 --> 18:02.520
I don't particularly like questions like this, it's not exactly the best because it sounds

18:02.520 --> 18:07.240
like you're asking for some kind of theory and I don't really have one or care for one,

18:07.240 --> 18:11.480
what's interesting is that that happened and I'm not exactly what you're talking about

18:11.480 --> 18:26.040
but kind of, we suddenly jump to 22 question on what's going on, I don't know this, 13, sorry,

18:26.040 --> 18:34.880
I was distracted, interface is changing, it's jumping around, so what's more interesting

18:34.880 --> 18:38.840
is that it is happening, when you notice that you should say knowing, knowing or noticing

18:38.840 --> 18:46.200
because it doesn't really matter what happens or why it's happening, this is happening

18:46.200 --> 18:51.160
and that's what we're interested in, so rather than delving into some kind of, I think

18:51.160 --> 18:58.200
you talk about the brain, your other question is about the brain, where is the attention

18:58.200 --> 19:04.320
when it switches from seeing, seeing, to hearing hearing, again, not really pertinent,

19:04.320 --> 19:11.680
it's interesting is that it is happening and whatever is happening, you note that, so I'm

19:11.680 --> 19:27.280
not really keen to delve into questions like that, this is a new note, wait, oh yeah this

19:27.280 --> 19:34.960
is an old, this was here, oh it's got days, four days ago, are they, are they effects

19:34.960 --> 19:42.240
meditation unconscious, if not are they conscious, but it's an interesting, I mean it sounds

19:42.240 --> 19:46.880
kind of, oh this is the other, but it's not really, I mean it's about retraining a non-aware

19:46.880 --> 19:54.640
mind, whatever that might mean, or is it conscious and that you consciously learn things

19:54.640 --> 20:02.000
and it's the latter, you consciously learn things about reality, I suppose some of it feels unconscious

20:02.000 --> 20:06.640
because suddenly you're changed, but that's just because we aren't clear on cause and effect,

20:06.640 --> 20:13.680
we don't understand how changes in this consciousness lead to changes, change results,

20:14.960 --> 20:21.600
but it very much is conscious in the sense that you're a clear vision, I mean even just saying

20:21.600 --> 20:27.120
it's still, again we don't understand the relationship, but even you're just saying rise, saying

20:27.120 --> 20:34.480
it's going to give results, that one moment is wisdom, it's hard to understand my teachers to say

20:34.480 --> 20:37.680
knowing the rising, knowing the falling, that's wisdom, and you think what,

20:38.880 --> 20:45.440
everyone I think where's the wisdom in that, a very deep question, most people think no,

20:45.440 --> 20:51.520
come on, there's no wisdom there, once you understand that we're saying rising, knowing that this

20:51.520 --> 20:56.560
is rising, knowing that that's rising, once you understand that that wisdom, then you'll get this

20:56.560 --> 21:08.480
relationship, how that is actually changing you, until then it seems kind of magical, like wow,

21:08.480 --> 21:21.920
hey this is somehow in more peaceful and so on. Here's a question that I think we get often

21:21.920 --> 21:27.360
and again I'm not, it's okay, it's an okay question, but it's a little bit speculative, I mean,

21:28.400 --> 21:37.520
so the question is, so because we always cling to things, since you're teaching meditations,

21:37.520 --> 21:43.360
your mind not clinging to the practice of meditation, so again I don't want to talk about myself,

21:43.360 --> 21:51.760
but we get this question about what about attachment of meditation, I mean, not everything we do

21:51.760 --> 22:00.560
is, I just got logged out, this thing has a mind of its own, this is totally,

22:00.560 --> 22:10.080
there's someone behind the scenes playing round with it, or it's sentient, my computer has

22:10.080 --> 22:19.920
gained sentient, this website has anyway, now I can't get back it,

22:19.920 --> 22:30.400
I mean kudos to our IT team, I mean people put this together on a volunteer basis,

22:31.520 --> 22:38.720
this was back in, so I really appreciate what they've done, but it really feels like we

22:38.720 --> 22:44.320
need a better server, some bigger pipe or something like that, more data can,

22:44.320 --> 22:55.040
it really feels like we're backlogged here, anyway, it's a good teaching, it helps us understand

22:55.040 --> 23:04.160
impermanence suffering non-self, maybe it was a design, it was a feature, not flaw,

23:05.280 --> 23:09.520
we don't want it working perfectly, or then we would have a sense of self, we could control,

23:09.520 --> 23:17.040
so again that's a good point, let's not change it, keep it impermanent, so we can all

23:17.760 --> 23:26.720
be mindful of the dangers, the unpredictability of it, so okay can you be attached to meditation,

23:26.720 --> 23:31.840
certainly you can, that's not quite your question, but your question is more like,

23:32.800 --> 23:37.040
well you practice meditation, or you teach meditation, doesn't that mean you're attached,

23:37.040 --> 23:42.160
so again not everything we do is because of attachment, we still do, we still breathe, we still

23:42.160 --> 23:49.120
speak, we still work, we do many things that don't necessarily have to be because of attachment,

23:49.120 --> 23:55.520
attachment is only a small part of our experience, it's an important part, but it's only part of it,

23:55.520 --> 24:04.320
once you have no attachment doesn't mean you stop doing things, so it's really simple answer,

24:04.320 --> 24:13.680
but I think it's important, okay here's a return question, or not satisfied with my answer,

24:14.320 --> 24:25.680
maybe I dismissed it, we're going to try again, so your question is still the same,

24:25.680 --> 24:31.200
they remember the question, it's determining which should be noted, takes up so much time,

24:31.200 --> 24:37.600
that seems so much more useful to just try and stay present without noting, so again useful for

24:37.600 --> 24:46.080
what, I mean the noting has a purpose, it's meant to change the way you look at the experience,

24:46.080 --> 24:55.040
to remind you that the thing is what it is, you talk about during throughout the day,

24:55.040 --> 25:01.040
so throughout the day you don't have to know everything, and then so your clarification helps,

25:01.040 --> 25:08.320
because the point is, you feel the meditation in certain instances gets in the way,

25:08.320 --> 25:14.240
well gets the way of what, living your life okay, then you're going to have to sometimes live your

25:14.240 --> 25:20.160
life, talking to people, working and so on, and sometimes meditate, did it get in the way of being

25:20.160 --> 25:27.200
enlightened, no it leads and it helps and it supports you, but if you want to, you're taking this

25:27.200 --> 25:31.200
to the logical conclusion that we should always be doing that, well yeah if you're enlightened,

25:31.200 --> 25:36.640
or if you're in a situation where you don't really, you're really concerned about living your

25:36.640 --> 25:42.720
life in terms of other practices, then when you talk to people you can say talking, talking,

25:42.720 --> 25:51.120
feeling or hearing or so on, and not have to worry about it, another thing that you might be

25:51.120 --> 26:00.960
experiencing is sort of this beginner force state, because it's new, over time you can really

26:00.960 --> 26:08.720
be mindful and still think and allow your mind to continue, it doesn't dive with your

26:08.720 --> 26:18.960
or the way your mind works as a non-meditator, so it appears to get in the way of just ordinary

26:18.960 --> 26:23.840
functioning and it doesn't really, it shouldn't, but it takes some skill to get to that point

26:23.840 --> 26:33.120
where it doesn't, and I mean really, again not something you should really worry about, do what

26:33.120 --> 26:41.280
you can during the day, but save the constant and repeated and systematic mindfulness for when

26:41.280 --> 26:46.320
you're actually doing a formal practice, until you come to do a meditation course, when you're

26:46.320 --> 26:52.160
in a meditation center of course, there shouldn't be much talking at all, so you have lots of time

26:52.160 --> 27:09.760
to be methodical throughout the day, I hope that's some better answer, if not, come back next week,

27:09.760 --> 27:24.320
we're down to 10 questions, looks like some new ones, yeah, cheaters, well that's a problem also

27:24.320 --> 27:28.080
because they're, well it's not a really problem, but they're not sorted according to likes,

27:28.960 --> 27:33.680
which isn't really a problem, problem is it's never ending and then they'll never leave,

27:33.680 --> 27:45.520
so we have to cut off somewhere. I want to know if I should say to myself thinking,

27:45.520 --> 27:50.000
thinking every time I fall into thoughts, after I already noticed, even though you've noticed too

27:50.000 --> 27:55.280
late, so I remember saying thinking, thinking is to remind you that that was just thinking,

27:55.280 --> 28:02.560
thinking it's not to catch it as it happens per se, it's just after the fact reminder,

28:02.560 --> 28:08.640
it's how the mindfulness works, so absolutely as soon as you know that you were thinking,

28:08.640 --> 28:12.560
you could say thinking, or you can also say knowing, knowing, because that's immediate,

28:12.560 --> 28:16.880
you just knew that you were thinking, so right then the next thing you do is say knowing, knowing,

28:19.120 --> 28:28.560
that also works, but it keeps you from any reactions that might come from it.

28:28.560 --> 28:36.000
Question about body and weight, and I think you're overthinking things, I read through your question.

28:38.480 --> 28:44.400
The way we understand it is, you're technically trying to find nature of things and I think

28:44.400 --> 28:51.200
as a result, it's like quantum physics when you, when you, the observation changes, what I mean

28:51.200 --> 28:57.840
by that is your investigation is changing, you're looking at closer and closer to trying to find,

28:57.840 --> 29:01.520
what is a feeling really? And suddenly it's not a feeling anymore, it's something else,

29:02.400 --> 29:10.880
because of your investigation changes it. So when you feel pain, that's pain, that's under

29:10.880 --> 29:16.080
feelings category. Again, it's only experience, it's not how you investigate, what is this really?

29:16.080 --> 29:20.880
Because by the time you've said that, it's something new, but at the moment you feel pain,

29:20.880 --> 29:25.760
there's no question that is pain, when you feel happy, there's no question that is happiness,

29:25.760 --> 29:31.920
when you feel calm, there's no question that that is calm, doesn't matter what deeper what it is,

29:31.920 --> 29:39.600
there is no deeper, it has no depth to it, it is what it is. When you feel hot, that's physical

29:39.600 --> 29:48.720
sensation, when you feel cold, when you feel tension, when you feel flasted, when you feel

29:48.720 --> 29:56.240
hardness, when you feel softness, those are physical, those are under rupa. So the tension in

29:56.240 --> 30:02.720
the stomach, the tension is rupa, but if it feels, and another thing is every experience has

30:02.720 --> 30:07.600
weighed in, it's technically this, when you say rising, there is weight in that, there is

30:07.600 --> 30:13.040
feeling there, but that's not what we're focused on, sati patana or practical, they're not

30:13.040 --> 30:21.360
ultimate reality, exactly, the more of a convention, so when you feel pain, that's when you say pain,

30:22.720 --> 30:27.600
and thoughts as well are not physical, you're trying to say thoughts or feelings, but that's

30:27.600 --> 30:32.160
only because you're investigating them, and by the time you've investigated, it's no longer

30:32.160 --> 30:38.160
a thought, thought is thought, there's no question, it is thought, it's not anything deeper,

30:38.160 --> 30:45.680
anything behind it, it is what it is, and that's important, in Buddhism we take things as they are

30:46.560 --> 30:53.040
not trying to investigate some kind of deeper knowledge, so I think that's an answer to your question,

30:53.040 --> 31:03.440
but you're welcome to resubmit it. If I could ever get rid of it, I'm not able to get rid of

31:03.440 --> 31:14.000
any questions now, there's just a lot of buttons from your press, okay, they're going, good, gone.

31:16.720 --> 31:22.720
How does someone feel compassion without being attached? I think you're confusing with empathy,

31:23.520 --> 31:29.680
compassion is seeing someone, not as true as both, compassion is when you see someone suffering,

31:29.680 --> 31:38.880
the motivation to help them out of suffering, and in some ways it's just natural, it's a proper

31:38.880 --> 31:44.320
response, but there are ways to cultivate it, I mean the real reason for cultivating compassion is

31:44.320 --> 31:49.600
because you're a cruel person who doesn't care about other people, and it's a problem, so compassion

31:49.600 --> 32:02.320
can help to balance you out. It can also be used as a sort of means of tranquility, so if you just

32:02.320 --> 32:08.640
focus on being compassionate, the state of compassion is very peaceful, some people think they're

32:08.640 --> 32:14.800
enlightened when they have a lot of compassion, it's good, but I'm saying that because there are

32:14.800 --> 32:21.600
schools of Buddhism and there are Buddhists in our tradition even who are very much into compassion,

32:21.600 --> 32:28.800
which is not a bad thing, but it can be, can't overestimate its value. It's a good thing,

32:28.800 --> 32:39.360
it's just not ultimately the ultimate good thing. Here's a question, someone bugging me about

32:39.360 --> 32:45.520
the Dhammapada, no. I haven't been doing the Dhammapada because it's end of term, and I just put

32:45.520 --> 32:55.680
rather do one thing at a time and I'm writing a paper on beyond good and evil and Christian mysticism.

32:58.880 --> 33:09.040
I'm tearing down Christianity, not exactly, but I'm bringing to light some of the, you know,

33:09.040 --> 33:15.280
it's not even a critical paper against that. It's just outlining the philosophy is behind

33:16.880 --> 33:26.560
one Christian mystic that relate to goodness and how there's this idea of going down.

33:26.560 --> 33:32.240
Anyway, there are some interesting Buddhist issues there, so it's not totally off base.

33:32.240 --> 33:40.000
So plans for another series? No, I don't have, I don't make the sorts of plans.

33:44.560 --> 33:50.240
Dhammapada is, I have plans for the Dhammapada only because I know that next week I have to do,

33:50.240 --> 33:58.000
or as soon as I get back to it, the next week I'll have to do the next verse. That's my plan there.

33:58.000 --> 34:06.320
One that's done, well, we'll see, maybe I'll make another plan. When I get to 423, 123, if I ever,

34:06.320 --> 34:13.760
if I don't die first, there will be no next number, so that plan will be done.

34:15.440 --> 34:17.440
And if I die first, it'll be done as well.

34:17.440 --> 34:33.760
In regards to the mindfulness practice in daily life, I don't know if I read this one.

34:35.120 --> 34:40.640
I found a need to direct the noting to Odja's to my body. So my mind doesn't identify and

34:40.640 --> 34:47.520
chase my thoughts. So in daily life, yes, that's a good strategy. You can be mindful of thought,

34:47.520 --> 34:51.920
especially if you're really distracted and you can say distracted distracted, but a good strategy

34:51.920 --> 34:58.240
in daily life is don't worry so much about it. Focus on the body, standing, walking, sitting,

34:58.240 --> 35:04.080
lying. If you can remember those four, that's a great practice during your day. What am I doing?

35:04.080 --> 35:09.360
I'm sitting now, so I know that I'm sitting, say sitting, sitting, and also, you know, if you want

35:09.360 --> 35:14.800
to push whether you can say bending, stretching, reaching, when you brush your teeth, brushing,

35:14.800 --> 35:21.120
when you eat your food, chewing, chewing, and swallowing. But that's advanced. I wouldn't worry too

35:21.120 --> 35:28.160
much about it in the beginning. As you get better at it, of course, start to be concerned with that.

35:29.760 --> 35:36.720
And also, the sense is seeing, hearing, and so on. But as far as getting, if your thoughts is just

35:36.720 --> 35:44.240
too much fine, come back to the body, that's great. It's going to differ for different people as

35:44.240 --> 35:50.480
well, but for some people, the body is a good object, for other people, while the mind is maybe

35:50.480 --> 35:56.400
they find it a better object, but they're all good. None is better than another.

35:56.400 --> 36:08.080
Except the body is easier, so it's going to be stronger during the day. We want to be more

36:08.080 --> 36:14.320
fine during a formal practice, but during the day, your best strategy is to start with the body,

36:14.320 --> 36:23.280
and everything else is about it. I'm starting to answer the new questions. These are all new

36:23.280 --> 36:33.840
questions. Half an hour that's cut off. You had a whole week to ask them, so these ones get

36:33.840 --> 36:46.960
relegated next week. Does that mean of me? I don't care. That's all for tonight. Thank you

36:46.960 --> 36:56.960
all for tuning in. Have a good night. Have a good week.

