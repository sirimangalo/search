1
00:00:00,000 --> 00:00:06,480
When interacting with a female in particular when I see her my mind scatters away

2
00:00:06,840 --> 00:00:08,840
Well, I'm trying to stay mindful

3
00:00:09,400 --> 00:00:19,200
Would it be okay to disregard what I see and only meditate on what I hear when talking to her or would that be cheating and not facing the problem?

4
00:00:21,000 --> 00:00:23,000
I wonder what it is that you're seeing

5
00:00:23,000 --> 00:00:32,600
Yeah, a few meals can do that. It's terrible, really

6
00:00:36,240 --> 00:00:38,240
Yeah, I mean sometimes you want to

7
00:00:40,160 --> 00:00:42,160
Just focus on

8
00:00:42,160 --> 00:00:47,080
One hearing instead it made me think of another part of this comprehensive practice

9
00:00:47,080 --> 00:00:50,840
I can't wait till I get this one out. It's taken me so long because it is a long one

10
00:00:50,840 --> 00:00:55,400
And I made it even longer and I had to totally redo it

11
00:00:56,160 --> 00:01:03,000
And hopefully this week I'll get it out there, but it's comprehensive this talk and so it has everything that I can keep referring back to

12
00:01:05,640 --> 00:01:08,960
And one of the parts of the comprehensive practice, which is actually

13
00:01:10,280 --> 00:01:12,280
commentary on the Sabasa was with

14
00:01:12,280 --> 00:01:19,280
Magi Minikaya number two is

15
00:01:19,280 --> 00:01:23,080
Songwara the guarding of one's faculties, which is a very important part of one's practice

16
00:01:23,080 --> 00:01:29,200
So the Buddha says first that you have to see things clearly he says the taints are to be done away with by seeing

17
00:01:30,000 --> 00:01:37,600
It's not very useful in your in your in your case. Is it because it sounds like seeing is going to just

18
00:01:38,160 --> 00:01:40,160
create more defounds for you

19
00:01:40,160 --> 00:01:42,160
So

20
00:01:42,480 --> 00:01:47,760
The next thing he says is that in you know in order to support your practice of seeing things as they are

21
00:01:47,760 --> 00:01:50,160
You have to guard your senses

22
00:01:50,320 --> 00:01:56,120
And of course seeing doesn't mean looking with the eyes. It means seeing all experiences as they are

23
00:01:57,120 --> 00:01:59,120
when you're unable to

24
00:01:59,760 --> 00:02:03,760
see certain experiences as they are when they immediately blow you away

25
00:02:06,200 --> 00:02:08,200
This is actually a

26
00:02:08,200 --> 00:02:10,200
Just

27
00:02:10,200 --> 00:02:15,000
a hindrance in the practice and so you should guard your senses letting in only

28
00:02:15,400 --> 00:02:19,040
Those things that you're now able to deal with one by one by one

29
00:02:20,000 --> 00:02:23,200
In order to be able to observe them clearly as they are

30
00:02:24,440 --> 00:02:26,440
It's no use trying to overcome

31
00:02:26,800 --> 00:02:32,920
For example not that I'm claiming I'm saying a beauty. You cleared it up for me. Okay

32
00:02:32,920 --> 00:02:36,960
And in the example of beauty

33
00:02:38,720 --> 00:02:45,800
It's not possible or it's not reasonable to expect that someone should overcome once it they're attached from to beauty

34
00:02:46,680 --> 00:02:52,280
Being surrounded immediately by lots and lots of beautiful things. They're going to just give up the practice

35
00:02:52,560 --> 00:02:54,560
It's it's overwhelming for them

36
00:02:54,760 --> 00:02:59,280
You have to be able to guard your senses or you have to have to guard your senses

37
00:02:59,280 --> 00:03:02,360
Until you get to the point that you're able to

38
00:03:02,880 --> 00:03:05,520
Deal mindful mindfully in every situation

39
00:03:06,120 --> 00:03:12,440
The booty even said the monk on and asked him what should we do in regards to women and he said well

40
00:03:12,840 --> 00:03:14,840
Best thing is don't see women and

41
00:03:15,880 --> 00:03:21,040
Ananda said well, which should we do if we have to see women and they said well, then don't talk to them

42
00:03:21,040 --> 00:03:30,680
Ananda said well, what if we have to talk to them would have said well, that'd be very very mindful so I mean it's

43
00:03:33,320 --> 00:03:37,560
It people often take this take this the wrong way and think well

44
00:03:38,200 --> 00:03:41,360
This is a pretty silly teaching that it teaches you to ignore

45
00:03:42,280 --> 00:03:47,360
Experiences and not really investigate experiences, but it doesn't actually it takes into

46
00:03:47,360 --> 00:03:50,360
account the

47
00:03:51,680 --> 00:03:53,680
practical reality

48
00:03:54,680 --> 00:03:56,680
The practical impossibility of

49
00:03:57,560 --> 00:03:59,560
coming to see things as they are

50
00:04:00,160 --> 00:04:02,160
letting things be as they are

51
00:04:02,360 --> 00:04:04,360
you need to

52
00:04:04,360 --> 00:04:07,160
You know come back as I said come back to a

53
00:04:07,640 --> 00:04:14,360
Life that is free from all of the things that that you cling to in order to begin to understand the things you cling to

54
00:04:14,360 --> 00:04:17,800
First you have to throw them away. You have to begin to

55
00:04:19,360 --> 00:04:24,040
You have to begin from a state of an empty state

56
00:04:24,040 --> 00:04:30,400
You have to have an empty slate and then slowly take things back on to the extent that you're able to observe them

57
00:04:32,760 --> 00:04:39,560
It's the you have to think like you're a scientist how difficult it is to be to

58
00:04:39,560 --> 00:04:49,040
Interpreted the data how much more time it takes to interpret data than to just experience it

59
00:04:49,560 --> 00:04:51,560
So in order to be interpretative

60
00:04:52,760 --> 00:04:54,560
Interpretative

61
00:04:54,560 --> 00:04:57,640
We have to take things slower one by one by one we can't

62
00:04:58,280 --> 00:05:03,040
Let everything in and expect to be able to interpret it clearly and properly

63
00:05:03,680 --> 00:05:04,920
so

64
00:05:04,920 --> 00:05:10,280
Sounds like that's the sort of thing that you're trying to grasp your best thing to do is is run away from this woman

65
00:05:10,280 --> 00:05:18,440
That would be ideal right to just not have any contact with it, but that's difficult because you probably really really really really like her

66
00:05:20,080 --> 00:05:22,080
So in that case you

67
00:05:22,320 --> 00:05:26,640
It's not really a problem. It's in fact can be a useful laboratory for you

68
00:05:27,440 --> 00:05:29,440
and you start you can start by

69
00:05:31,680 --> 00:05:33,680
When you think of her

70
00:05:33,680 --> 00:05:35,680
When the image of her comes up in your mind

71
00:05:36,560 --> 00:05:38,560
Begin with that reality

72
00:05:40,360 --> 00:05:46,000
Because it's also important to understood to as I said not take this the wrong way that we're trying to

73
00:05:46,400 --> 00:05:50,720
Suppress or avoid or run away from the issue where we're treating

74
00:05:51,600 --> 00:05:59,280
Making a strategic retreat so that we can slowly come up and a mind in a more you know with once we got our weapons ready

75
00:05:59,920 --> 00:06:01,920
Once we've got our gun loaded

76
00:06:01,920 --> 00:06:03,920
So just speak

77
00:06:07,240 --> 00:06:15,440
So you should take it slowly and and not to go out of your way to see this woman not go out of your way to be in her presence

78
00:06:18,920 --> 00:06:21,560
And not necessarily go out of your way to avoid her

79
00:06:22,240 --> 00:06:24,680
but try to see her in moderation and

80
00:06:26,600 --> 00:06:28,600
Slowly slowly develop

81
00:06:28,600 --> 00:06:33,400
Meditation based on this woman. I mean, it's a great opportunity. Once you've

82
00:06:34,120 --> 00:06:35,800
once you've

83
00:06:35,800 --> 00:06:37,800
excited in your mind that this

84
00:06:38,120 --> 00:06:40,120
object here this woman is

85
00:06:40,960 --> 00:06:42,960
A meditation object for me

86
00:06:42,960 --> 00:06:48,320
Then every time you see here you're going to immediately fall into meditation. We're not immediately but slowly it will become

87
00:06:49,080 --> 00:06:54,600
Once it's like eating. I always tell people think of eating as a meditation session

88
00:06:54,600 --> 00:06:59,960
Right when when you sit down on the meditation mat immediately your mind says now I'm meditating

89
00:07:00,200 --> 00:07:03,320
Right because you've got a meditation mat and you've got a

90
00:07:04,360 --> 00:07:09,440
Your legs are in a funny posture your hands are in a funny posture. You're not sitting in an ordinary way

91
00:07:09,440 --> 00:07:15,360
So you think this is meditation when we eat we think this is eating and so we eat then we lose our mindfulness

92
00:07:15,840 --> 00:07:19,240
But if we thought of eating as a meditation center session

93
00:07:19,240 --> 00:07:24,600
Which in Zen they're really good about this and then they have you eaten a really awkward way or

94
00:07:25,360 --> 00:07:31,880
Not awkward, but novel way that that you're not used to and as a result you're forced to be very very mindful

95
00:07:33,600 --> 00:07:40,200
So you can take this woman as being a meditation session for you. Okay now is a meditation session for me

96
00:07:41,560 --> 00:07:45,280
The other the other thing I would recommend is not to be too hard on yourself

97
00:07:45,280 --> 00:07:52,600
So yeah, you'll fail sometimes and you'll fall into liking her and you'll you'll you wind up being totally blown away and

98
00:07:53,440 --> 00:07:55,640
And and mindful and whatever

99
00:08:00,440 --> 00:08:06,840
But that's an important part of your observation you have to take that as an important part of your

100
00:08:08,680 --> 00:08:11,760
Investigation your investigation has to take everything into account

101
00:08:11,760 --> 00:08:16,520
if you deny yourself this or if you

102
00:08:17,680 --> 00:08:19,680
if you

103
00:08:20,680 --> 00:08:26,880
React to your failures in a negative way you'll learn nothing from them if when you fail

104
00:08:27,120 --> 00:08:29,360
This is why there's those the same's out there that

105
00:08:30,480 --> 00:08:35,760
If you never fail you never learn or something like that or or or you learn we learn from our mistakes

106
00:08:35,760 --> 00:08:40,080
We don't always learn from our mistakes. I gave a talk about this some time ago

107
00:08:40,080 --> 00:08:43,600
we learn from our mistakes when we actually

108
00:08:47,040 --> 00:08:53,440
Look at them when we actually in you know accept that we've made a mistake objectively and and

109
00:08:54,360 --> 00:08:56,360
try to learn from them

110
00:08:57,040 --> 00:09:00,680
We don't learn anything from mistakes if we get angry and upset about them

111
00:09:01,320 --> 00:09:03,320
so at the time when you

112
00:09:04,360 --> 00:09:08,480
Like this woman and are attracted to her beauty and so on

113
00:09:08,480 --> 00:09:13,840
You shouldn't be saying to yourself. Oh, I'm such an evil nasty person because I

114
00:09:15,200 --> 00:09:20,800
You know this is sinful and this is so and you know the western society is so full of this the sin of

115
00:09:21,440 --> 00:09:23,440
Sexuality and the sin of

116
00:09:23,760 --> 00:09:30,880
Masturbation which I got this video on masturbation has got me quite famous in in in internet circles

117
00:09:30,880 --> 00:09:38,880
you know be because you know this is an issue that

118
00:09:39,920 --> 00:09:42,960
Masturbation for example is an issue that people feel utterly

119
00:09:46,560 --> 00:09:50,320
Wretched about they feel horribly guilty like they've committed some

120
00:09:51,120 --> 00:09:52,800
Cardinal sin

121
00:09:52,800 --> 00:09:56,560
By doing something that they actually like doing right we're doing something that actually

122
00:09:56,560 --> 00:10:02,600
They think you know or the deep down their heart says this is good. This is is pleasant

123
00:10:03,280 --> 00:10:06,400
So how can you lie to yourself in that way if you still enjoy it?

124
00:10:06,640 --> 00:10:10,520
How can you say it's bad and so this is what's led many Hindu gurus?

125
00:10:11,000 --> 00:10:15,840
To take people the other way and even there was this this big Zen book that I showed you yesterday

126
00:10:16,160 --> 00:10:18,960
This guy he he talks about how his teacher

127
00:10:19,920 --> 00:10:24,440
Was someone who who along this line taught people to find liberation through sexuality

128
00:10:24,440 --> 00:10:26,440
you know

129
00:10:27,160 --> 00:10:30,040
Tantric Buddhism and so on even even exists

130
00:10:33,720 --> 00:10:39,160
But all these Hindu gurus who encourage it was a you know these western westerners are so

131
00:10:39,800 --> 00:10:43,240
Sexually repressed so he puts them together and tells them to have group orgies

132
00:10:45,080 --> 00:10:47,080
We don't do this

133
00:10:47,240 --> 00:10:49,240
Because that's also not

134
00:10:49,240 --> 00:10:56,600
Because it's reacting and reacting to things as being positive and reacting to things as being negative is no good reacting to

135
00:10:57,080 --> 00:11:00,200
Things as being positive is also a cause for habitual

136
00:11:00,840 --> 00:11:05,640
The cultivation of habitual tendencies which are only going to create further and further

137
00:11:09,480 --> 00:11:15,720
Expectations and and clinging and craving and disappointment and depression and sadness and

138
00:11:15,720 --> 00:11:21,320
Dissatisfaction when they're you're not able to obtain the objects of your desire

139
00:11:24,040 --> 00:11:29,240
So it's important to let you're not let yourself but yeah to

140
00:11:29,800 --> 00:11:37,080
Accept when you make mistakes and to see the experience of making a mistake as it is when you

141
00:11:37,080 --> 00:11:39,080
I

142
00:11:44,760 --> 00:11:46,760
When I see her

143
00:11:46,760 --> 00:11:50,840
I have to prepare to meditate every time I see her when I exit the gym she works

144
00:11:51,320 --> 00:11:56,880
Yes, so we can all get a kind of a picture of what's what the the problem is here

145
00:12:00,200 --> 00:12:05,080
So yeah preparing yourself is one thing but also letting yourself feel these these feelings

146
00:12:05,080 --> 00:12:12,280
You know or you're not letting but acknowledging when you do feel these experiences you can't stop yourself from

147
00:12:13,080 --> 00:12:14,360
Sorry

148
00:12:14,360 --> 00:12:19,080
You can't stop yourself from experiencing them once you've experienced

149
00:12:19,560 --> 00:12:25,080
Last and desire you've experienced it you can't change that in retrospect and yeah

150
00:12:25,080 --> 00:12:29,400
That's what we try to do we pretend that it's not there and so we say we're repressing it

151
00:12:29,400 --> 00:12:33,000
We're actually not repressing it. We're reacting to it negatively

152
00:12:33,000 --> 00:12:42,600
Which which totally nullifies any benefit any learning that that might have come from the experience

153
00:12:43,160 --> 00:12:50,120
So at the time when you're attracted to someone, you know, it can be the case where she's married and so you feel guilty because she's

154
00:12:52,200 --> 00:12:55,640
You're you know, she's untouchable. There's no

155
00:12:55,640 --> 00:13:01,880
It could be that you're married or you're you have you're in a you know in a

156
00:13:03,800 --> 00:13:05,160
Relationship

157
00:13:05,160 --> 00:13:09,800
But you have to accept that that you have these feelings. I mean last doesn't lust is like fire

158
00:13:10,120 --> 00:13:13,080
The Buddha said it's I think he said

159
00:13:13,640 --> 00:13:18,120
Not Tiragasama Agi. There is no fire like lust

160
00:13:19,240 --> 00:13:21,240
I believe that's the quote

161
00:13:21,240 --> 00:13:24,920
And what it means is that

162
00:13:27,400 --> 00:13:30,760
You can't you know like like a fire like a great raging fire

163
00:13:31,560 --> 00:13:34,040
You can't say to it let it stay only here

164
00:13:34,920 --> 00:13:40,520
Let me only have lust for my wife and my girlfriend and not have lust for this person that person

165
00:13:41,400 --> 00:13:44,440
If it's there, it's there and and it's uncontrollable

166
00:13:44,920 --> 00:13:49,880
This is why lust is such a dangerous thing greed and desire in general are so dangerous

167
00:13:49,880 --> 00:13:53,640
We say what's wrong with liking this what's wrong with liking that they're habitual

168
00:13:54,520 --> 00:13:57,640
they're no they're habit forming and

169
00:13:58,520 --> 00:14:04,760
They they lead only to greater and greater of the same type of emotion

170
00:14:06,920 --> 00:14:17,080
Which emotion is uncontrollable and can't be limited in any way shape or form to one specific set of of experiences

171
00:14:17,080 --> 00:14:19,480
it

172
00:14:19,480 --> 00:14:25,160
Like fire burns from the grass to the trees from the trees to the leaves and and and so on

173
00:14:25,720 --> 00:14:27,720
whatever is combustible

174
00:14:28,360 --> 00:14:36,280
It is liable to fall into burning which we can totally destroy this is why men cheat on their wives wives cheat on their husbands

175
00:14:39,320 --> 00:14:40,520
And so on

176
00:14:40,520 --> 00:14:43,000
Why we steal why we cheat?

177
00:14:43,000 --> 00:14:47,800
Because the cultivation of lust is not something that can be controlled

178
00:14:49,160 --> 00:14:53,400
Once you cultivate it, it's there and it's like a fire something that you can't contain

179
00:14:55,320 --> 00:14:59,880
So you have to accept that fact and you have to work to

180
00:15:01,400 --> 00:15:05,320
Undo the habits that's through the the only way to deal with this

181
00:15:05,320 --> 00:15:12,200
Yeah, so start by limiting your interaction with this person to what is

182
00:15:13,320 --> 00:15:16,520
Unnatural when you know that she's there you might not want to look in the beginning

183
00:15:16,920 --> 00:15:21,160
You might just want to avert your eyes when you're not talking to her and then you know

184
00:15:21,480 --> 00:15:24,840
Suffice to to hear and to feel the heart throbbing

185
00:15:25,720 --> 00:15:26,920
agony

186
00:15:26,920 --> 00:15:32,040
Which actually is a lot of suffering. We had the tension that comes from desires is actually a lot of suffering

187
00:15:32,040 --> 00:15:39,880
I'll see to see how what a bother this is how it's actually ruining your physical health and your mental health

188
00:15:40,440 --> 00:15:42,440
and so on

189
00:15:42,440 --> 00:15:43,480
so

190
00:15:43,480 --> 00:15:48,280
Perfectly well within your rights to avoid and to limit

191
00:15:49,000 --> 00:15:54,040
Avoiding is another one if if something is totally dangerous where you know you just can't control yourself

192
00:15:54,040 --> 00:15:58,360
Then you should avoid certain things. We know Denabahatabah and

193
00:15:59,080 --> 00:16:00,680
the other one is

194
00:16:00,680 --> 00:16:02,680
Somewhere about to so

195
00:16:03,880 --> 00:16:05,880
Anybody want to jump in?

196
00:16:05,880 --> 00:16:07,880
Larry you're here. Welcome

197
00:16:07,880 --> 00:16:09,880
You back

198
00:16:10,120 --> 00:16:12,120
Anybody want to say anything to that one?

199
00:16:13,720 --> 00:16:21,000
Yeah, I'm sorry. I missed part of it. I had a neighbor to walk over so I had to visit the entrance. It's okay. You understand

200
00:16:21,000 --> 00:16:31,000
Okay

