1
00:00:00,000 --> 00:00:29,600
Good evening everyone, welcome to Evening Dumber.

2
00:00:29,600 --> 00:00:57,000
Tonight we are looking at tonight we're looking at the postures of the body.

3
00:00:57,000 --> 00:01:14,200
The brief section, the text itself is very short, but there's as usual a lot more to say about it, maybe not as much as last night.

4
00:01:14,200 --> 00:01:44,000
So the Buddha says Gachandova, Gachami T. Pajana T. Biku, Gachandova, Gachami T. Pajana T.

5
00:01:44,000 --> 00:01:51,000
T. Pajana T. He knows, he knows, knows with wisdom, not knows intellectually.

6
00:01:51,000 --> 00:02:08,000
Gachami, I am going. When going, one knows, I am going. When seated, one knows, I am seated.

7
00:02:08,000 --> 00:02:33,000
When lying down, one knows, I am lying down.

8
00:02:33,000 --> 00:02:57,000
So we make a big deal about these four postures. There they are written in the typical, because they make a good, another good basic object of awareness, especially the walking one, right?

9
00:02:57,000 --> 00:03:06,000
As you know, we put a lot of emphasis on actually doing walking meditation, and the Buddha did as well.

10
00:03:06,000 --> 00:03:13,000
It seems to be a thing to do walking and sitting in alternation.

11
00:03:13,000 --> 00:03:26,000
But the four postures in general, they make up a good framework for us to be mindful, not only when we're meditating, but when we're living our lives.

12
00:03:26,000 --> 00:03:39,000
Walking down the street, knowing you're walking down the street, standing in line, sitting on the bus in a car, even when you're lying down to sleep.

13
00:03:39,000 --> 00:03:55,000
As we'll see in the next section, you can be mindful all over, anywhere, whatever you're doing.

14
00:03:55,000 --> 00:04:02,000
So the question is, what does the Buddha mean by Kachami, a kachami deep agility?

15
00:04:02,000 --> 00:04:09,000
One knows with wisdom, or knows fully, I am going.

16
00:04:09,000 --> 00:04:24,000
Kachami is a first person verb, so it's one word that means I go, or I walk is really the point here.

17
00:04:24,000 --> 00:04:34,000
And so it doesn't actually, I don't think, it isn't actually literally saying, one says to oneself,

18
00:04:34,000 --> 00:04:41,000
kachami, kachami, kachami, kachami, that's not literally what it's saying.

19
00:04:41,000 --> 00:04:53,000
And wherever you understand this, it's because fairly clearly the Buddha most often didn't give explicit practical meditation advice.

20
00:04:53,000 --> 00:05:02,000
The way it's worded is always in terms of the outcome of the practice.

21
00:05:02,000 --> 00:05:06,000
Because these are general talks.

22
00:05:06,000 --> 00:05:15,000
When you give a talk on the dhamma, you don't sit there most often telling the meditators how to meditate.

23
00:05:15,000 --> 00:05:22,000
There's a difference between giving a talk on the theory and giving actual meditation instruction.

24
00:05:22,000 --> 00:05:32,000
Now this comes close to giving meditation instruction, but much of the Buddha's teaching is much more theory, the idea behind it.

25
00:05:32,000 --> 00:05:39,000
Which means that there was most likely a lot of practical instruction going on behind the scenes.

26
00:05:39,000 --> 00:05:50,000
We have some hints of this throughout the teachings, but it's fairly clear that there was a lot of more practical instruction

27
00:05:50,000 --> 00:05:58,000
that was just sort of the day to day mundane, how you do walking meditation, how you do sitting meditation.

28
00:05:58,000 --> 00:06:02,000
That didn't actually get recorded in all its intricacies.

29
00:06:02,000 --> 00:06:09,000
There seems to be a favoring of the actual talks that the Buddha gave on ideas.

30
00:06:09,000 --> 00:06:15,000
So he's saying, one knows clearly, I am going.

31
00:06:15,000 --> 00:06:22,000
He doesn't say what one does to accomplish that.

32
00:06:22,000 --> 00:06:34,000
So actually using a mantra is of course for some people it's uncomfortable and it's an odd sort of idea that when you're walking down the street you should actually say to yourself

33
00:06:34,000 --> 00:06:37,000
walking, walking, walking.

34
00:06:37,000 --> 00:06:47,000
But odd idea or not it really is the most concrete, obvious way of cultivating this knowledge, I am going.

35
00:06:47,000 --> 00:06:57,000
The commentary says something interesting and it gets into what's important about this passage and mindfulness in general.

36
00:06:57,000 --> 00:07:08,000
I mean this is a classic description of mindfulness. When walking one knows I'm walking, when standing one knows I'm standing.

37
00:07:08,000 --> 00:07:14,000
It sounds nice, perhaps romantic even.

38
00:07:14,000 --> 00:07:20,000
This idea that there's a person when I first heard this, that's how it impressed me.

39
00:07:20,000 --> 00:07:27,000
It was like okay, that's a fairly benign sort of philosophy.

40
00:07:27,000 --> 00:07:31,000
It's mild but it makes you think well that's kind of poetic.

41
00:07:31,000 --> 00:07:35,000
The idea of when you're walking to know that you're walking.

42
00:07:35,000 --> 00:07:38,000
It's certainly not what I'm looking for.

43
00:07:38,000 --> 00:07:42,000
This can't be the end of spirituality.

44
00:07:42,000 --> 00:07:53,000
I think there's so much more to it and the point is we're not clear about how important this is.

45
00:07:53,000 --> 00:08:00,000
I mean it seems to be something that most of us do anyway, not all the time sure.

46
00:08:00,000 --> 00:08:08,000
But when I walk I know I'm walking, when I say I know I'm sitting, there seems to be the case, right?

47
00:08:08,000 --> 00:08:20,000
And so the commentary is quite clear anticipating this or probably having encountered this idea of how mindfulness doesn't sound that tough.

48
00:08:20,000 --> 00:08:24,000
It sounds like something I do most of the time anyway.

49
00:08:24,000 --> 00:08:29,000
It's not the same as just knowing that you're walking.

50
00:08:29,000 --> 00:08:33,000
It's a special kind of knowing that they claim.

51
00:08:33,000 --> 00:08:38,000
The commentary doesn't actually give too much detail.

52
00:08:38,000 --> 00:08:44,000
Unfortunately they don't give much explanation on how one goes about creating this special kind of knowing either.

53
00:08:44,000 --> 00:08:47,000
Not in this instance.

54
00:08:47,000 --> 00:08:50,000
But in the Visudhi manga they're quite clear.

55
00:08:50,000 --> 00:09:02,000
Visudhi manga has some very explicit actual practical meditation advice, how you are instruction, how you practice mindfulness, how you practice meditation.

56
00:09:02,000 --> 00:09:23,000
And it involves a mantra, not always, but many times focusing on an object and repeating to yourself something that clues your mind in or keeps your mind focused on what it is you're observing.

57
00:09:23,000 --> 00:09:32,000
And the point is that our ordinary knowing of I am walking has very much to do with the eye aspect of it. There's a self involved.

58
00:09:32,000 --> 00:09:37,000
When we walk we think, hey I'm walking. This is me walking.

59
00:09:37,000 --> 00:09:46,000
So when again getting back to the questions we ask beginner meditators, when you're walking right and left, are they one thing or separate things?

60
00:09:46,000 --> 00:10:02,000
No, an ordinary understanding is that they're one thing, it's me, it's my body. A person who says that, not to give away the answers, but a person, such a person is still seeing conventionally.

61
00:10:02,000 --> 00:10:19,000
The knowledge that we're looking for is knowledge that cuts through convention and does away with the view of self, the idea of entities or control to see that walking occurs based on cause and effect.

62
00:10:19,000 --> 00:10:34,000
And the pieces or the constituents of walking action are moments of experience that arise and see.

63
00:10:34,000 --> 00:10:58,000
And we see the manga as well that has a really interesting section that comes from the ancient commentaries on the six parts of the walking steps. So many of you, those of you here have gone through one version of six steps of six parts to a walking step, it's actually an ancient idea that each step is an experience.

64
00:10:58,000 --> 00:11:04,000
But in each step, there are actually moments of experience that are different as well.

65
00:11:04,000 --> 00:11:13,000
So when you lift the foot, the poly word is, udharana. When you move the foot forward, at the atarana.

66
00:11:13,000 --> 00:11:25,000
And then there's this wheaty atarana, which I don't really understand. My teacher said, and I don't know, I'm not sure that he's actually going by the poly, but he called atarana is lifting the heel.

67
00:11:25,000 --> 00:11:34,000
Atarana is lifting the foot, wheaty atarana is moving the foot forward. Then we see the maga doesn't have it exactly like that.

68
00:11:34,000 --> 00:11:51,000
Wosajana is lowering the foot, sunny cape and not touching the floor, sunny rumbana is placing the floor, stabilizing, pressing the foot into the floor.

69
00:11:51,000 --> 00:12:01,000
And the idea behind this is to really help you to see individual experiences, to be able to distinguish between moments and moments and moments.

70
00:12:01,000 --> 00:12:07,000
And break apart this idea that it's a thing that's walking.

71
00:12:07,000 --> 00:12:13,000
Break apart our conception, the way we look at the world, change the way we look at the world.

72
00:12:13,000 --> 00:12:26,000
So that we see it from, so we start to naturally see it from a point of view of experience.

73
00:12:26,000 --> 00:12:34,000
And so as we do this, we are able to overcome the idea of self in its many aspects.

74
00:12:34,000 --> 00:12:39,000
And also it brings up all of our attachments. I mean, you can't attach to reality.

75
00:12:39,000 --> 00:12:46,000
So once you start to focus on, now I'm walking, I'm sitting, lifting the foot, and so on.

76
00:12:46,000 --> 00:12:52,000
It starts to break down any kind of attachment, any kind of clinging that could come.

77
00:12:52,000 --> 00:12:56,000
And it starts to become disjointed. Our likes and dislikes.

78
00:12:56,000 --> 00:13:11,000
We start to see that they're irrational. They have nothing to do with our experience because our experiences are just moments arising and ceasing.

79
00:13:11,000 --> 00:13:20,000
So when we talk about knowing, I think most people who have undertaken walking meditation understand this different.

80
00:13:20,000 --> 00:13:44,000
If they've done it diligently and practiced it continuously or daily, you start to see there's a difference between walking and knowing that you're walking intellectually and actually knowing, lifting, knowing that this is lifting, knowing that this is moving.

81
00:13:44,000 --> 00:13:53,000
It's very easy for the mind to be doing something else. Of course, walking meditation has to take into account all the other aspects of experience.

82
00:13:53,000 --> 00:13:58,000
When the mind is thinking, you've got to be mindful of thinking and then start walking again.

83
00:13:58,000 --> 00:14:07,000
You can't just force yourself to be, to meditate.

84
00:14:07,000 --> 00:14:15,000
And as it clears up, you feel this change or you actually do know that this is moving.

85
00:14:15,000 --> 00:14:21,000
This is walking, this is standing, this is sitting, this is lying down.

86
00:14:21,000 --> 00:14:31,000
So those are the four postures. This is sort of another introductory practice.

87
00:14:31,000 --> 00:14:39,000
Not only do they focus on the rising and falling of the stomach or the breath, but they also focus on the postures of the body.

88
00:14:39,000 --> 00:14:52,000
The next thing the Buddha says is yataya tawa, bandasakayo, bhanihito, hoati, tataya tataya nangpajanati, which is a good center.

89
00:14:52,000 --> 00:14:57,000
It's an important part of this section.

90
00:14:57,000 --> 00:15:10,000
It means, or, however, the body is disposed.

91
00:15:10,000 --> 00:15:17,000
One knows that clearly. One knows it thus clearly.

92
00:15:17,000 --> 00:15:20,000
Meaning that it's not simply about the four postures.

93
00:15:20,000 --> 00:15:26,000
The four postures make up a good framework, but they certainly aren't all that's happening.

94
00:15:26,000 --> 00:15:36,000
So, when you bend, when you stretch, when you reach, when you eat, when you shower, the next section goes into more detail.

95
00:15:36,000 --> 00:15:40,000
This one maybe we were just talking about if you're squatting or something.

96
00:15:40,000 --> 00:15:46,000
Know that you're squatting, if you're leaning, or like Ananda when he was lying down.

97
00:15:46,000 --> 00:15:54,000
He's sitting up and then he lay down, and his feet came off the floor and he was very mindful of this posture.

98
00:15:54,000 --> 00:15:57,000
His head touched the pillow. He became enlightened.

99
00:15:57,000 --> 00:16:04,000
So, he wasn't walking, standing, sitting, or lying down when he became enlightened.

100
00:16:12,000 --> 00:16:16,000
I'm not sure if there was anything else I wanted to say on this.

101
00:16:16,000 --> 00:16:19,000
See what is the commentary goes on and on and on about.

102
00:16:19,000 --> 00:16:25,000
No, very interesting. Again, if you really want to get into it, you should read the commentary.

103
00:16:25,000 --> 00:16:35,000
But it's mainly talking about, oh, the other thing that it talks about is just to talk just to really drive home this idea of non-self.

104
00:16:35,000 --> 00:16:39,000
This idea that it's really not a self that is walking.

105
00:16:39,000 --> 00:16:42,000
It talks about what happens when you walk.

106
00:16:42,000 --> 00:16:50,000
So again, with these six parts to the walking step, let's say lifting heel, lifting, moving, lowering, touching, pressing.

107
00:16:50,000 --> 00:16:53,000
All that it is is the elements.

108
00:16:53,000 --> 00:17:01,000
The elements means that the hardness, softness, the heat, the cold, and the tension and the placidity.

109
00:17:01,000 --> 00:17:05,000
These are the three elements that you experience.

110
00:17:05,000 --> 00:17:18,000
It says that in lifting heel, lifting, and moving, the fire element is strong.

111
00:17:18,000 --> 00:17:24,000
There will be a heating sensation, the color, sort of a burning.

112
00:17:24,000 --> 00:17:37,000
And there will be less earth and air, I believe, right? Let me see.

113
00:17:37,000 --> 00:17:42,000
Oh, no, this is not in this commentary. It's actually the, I'm looking at it.

114
00:17:42,000 --> 00:17:49,000
You see the macro commentary, right? With the six parts.

115
00:17:49,000 --> 00:17:57,000
Yes. And so, and then with lowering, touching, and pressing.

116
00:18:05,000 --> 00:18:08,000
Todernet.

117
00:18:08,000 --> 00:18:27,000
Oh, no, it was, we said in lowering the heat element and the air element are weak, but the earth element is strong.

118
00:18:27,000 --> 00:18:34,000
Maybe the translation is separate, but when you're touching the floor, right?

119
00:18:34,000 --> 00:18:43,000
So lowering, I'm not exactly sure what it means, but there will be a sense of the hardness when you touch the floor, of course.

120
00:18:43,000 --> 00:18:46,000
So there will be the earth element in that one.

121
00:18:46,000 --> 00:18:54,000
And less fire, because there's a release of the energy, bodily energy expended.

122
00:18:54,000 --> 00:19:02,000
So there will be a cooling.

123
00:19:02,000 --> 00:19:09,000
Yes. And there will also be a release of tension.

124
00:19:09,000 --> 00:19:13,000
Right? So on lifting, there's also, sorry, on lifting, there's also a tension.

125
00:19:13,000 --> 00:19:16,000
So the air element is strong.

126
00:19:16,000 --> 00:19:20,000
That tension, you feel, when you lift, that's the air element, the heat that it grew.

127
00:19:20,000 --> 00:19:24,000
And the leg is all, is the fire element.

128
00:19:24,000 --> 00:19:29,000
On lowering, there's a cooling, and there's also a release of pressure.

129
00:19:29,000 --> 00:19:34,000
So there's going to be a low air and fire element.

130
00:19:34,000 --> 00:19:40,000
And there will be a hardness when you touch the floor on that earth element.

131
00:19:40,000 --> 00:19:42,000
Right? Complicated.

132
00:19:42,000 --> 00:19:46,000
I heard this talk once and I thought, wow, you're supposed to sit.

133
00:19:46,000 --> 00:19:50,000
You know, are you really supposed to be aware this is this element, this is that element?

134
00:19:50,000 --> 00:19:52,000
No, that's not really the point.

135
00:19:52,000 --> 00:19:55,000
The point is, this is what you're experiencing.

136
00:19:55,000 --> 00:19:58,000
This is what the real experience is.

137
00:19:58,000 --> 00:20:04,000
There's the experience of this and there are different experiences.

138
00:20:04,000 --> 00:20:06,000
Each one is individual.

139
00:20:06,000 --> 00:20:11,000
The elements are not the same in each aspect, each piece of the movement.

140
00:20:11,000 --> 00:20:16,000
It's more complicated than you think, more involved.

141
00:20:16,000 --> 00:20:21,000
And even more involved is that it also involves the mind.

142
00:20:21,000 --> 00:20:23,000
You know, why do we walk?

143
00:20:23,000 --> 00:20:26,000
How does the walking occur?

144
00:20:26,000 --> 00:20:29,000
So the commentary asks, who walks?

145
00:20:29,000 --> 00:20:30,000
Right?

146
00:20:30,000 --> 00:20:35,000
Who's walking is it?

147
00:20:35,000 --> 00:20:38,000
And these are the questions that an ordinary mind asks.

148
00:20:38,000 --> 00:20:41,000
Is it me walking? Am I walking?

149
00:20:41,000 --> 00:20:49,000
But as you practice continuously, you start to see that, well, no, not really.

150
00:20:49,000 --> 00:20:54,000
The mind wants to walk, intends to walk.

151
00:20:54,000 --> 00:20:57,000
Me, I walk. I'm going to walk.

152
00:20:57,000 --> 00:20:59,000
And then the walking occurs.

153
00:20:59,000 --> 00:21:02,000
So it's cause and effect based on that cause.

154
00:21:02,000 --> 00:21:05,000
The effect is produced.

155
00:21:05,000 --> 00:21:07,000
That's how reality works.

156
00:21:07,000 --> 00:21:10,000
Mind and body work together, cause and effect.

157
00:21:10,000 --> 00:21:16,000
Moment of mind is the intention to do something and the result is the body.

158
00:21:16,000 --> 00:21:19,000
Sometimes the body creates the mind as well, right?

159
00:21:19,000 --> 00:21:23,000
There'll be a bodily experience and then there's the awareness of it.

160
00:21:23,000 --> 00:21:26,000
So they work together in tandem.

161
00:21:26,000 --> 00:21:30,000
It's quite complicated how they work and the brain, how the brain works.

162
00:21:30,000 --> 00:21:33,000
But in principle, it's quite simple.

163
00:21:33,000 --> 00:21:34,000
They work together.

164
00:21:34,000 --> 00:21:35,000
You want to walk.

165
00:21:35,000 --> 00:21:40,000
They're in those walking.

166
00:21:40,000 --> 00:21:50,000
And this is what a meditator starts to understand.

167
00:21:50,000 --> 00:21:56,000
They watch their experience and they start to see that there's the mind and there's the body.

168
00:21:56,000 --> 00:21:59,000
The body is composed of these four elements.

169
00:21:59,000 --> 00:22:03,000
And the mind is composed of many, many different elements.

170
00:22:03,000 --> 00:22:09,000
But together, this is our reality.

171
00:22:09,000 --> 00:22:14,000
And mindfulness is this exploration of reality helping us to see what is the physical.

172
00:22:14,000 --> 00:22:22,000
It's like wandering through a jungle and exploring all the different plants and animals living in the jungle.

173
00:22:22,000 --> 00:22:24,000
You start to see more clearly.

174
00:22:24,000 --> 00:22:34,000
It means you start to understand all the various aspects of reality.

175
00:22:34,000 --> 00:22:37,000
It's quite an important knowledge.

176
00:22:37,000 --> 00:22:39,000
It's important.

177
00:22:39,000 --> 00:22:48,000
Those are that saying. It's the most important work that we can do is to understand reality.

178
00:22:48,000 --> 00:23:01,000
To become familiar with the way the world, the way our life works, the way experience works.

179
00:23:01,000 --> 00:23:05,000
It goes without saying that someone who doesn't understand how things work,

180
00:23:05,000 --> 00:23:13,000
tends to make much, many more mistakes than one who understands how something works.

181
00:23:13,000 --> 00:23:20,000
This is not true or nowhere else than with our own reality and experience.

182
00:23:20,000 --> 00:23:23,000
So this is our practice.

183
00:23:23,000 --> 00:23:28,000
And that's the section on the postures and the new repeats.

184
00:23:28,000 --> 00:23:40,000
The same from last section internally, externally, the arising and ceasing.

185
00:23:40,000 --> 00:23:49,000
A tikayo tiwap anasa or one just knows this is body, knows it as it is.

186
00:23:49,000 --> 00:23:52,000
So there you go. That's the dhamma for tonight.

187
00:23:52,000 --> 00:24:04,000
Thank you all for your attendance.

188
00:24:04,000 --> 00:24:27,000
50 people every night on YouTube, that's great.

189
00:24:27,000 --> 00:24:34,000
And the question site is working.

190
00:24:34,000 --> 00:24:41,000
If during meditation thoughts arise that are emotional issues that you're trying to overcome or make peace with it,

191
00:24:41,000 --> 00:24:54,000
do you allow yourself to think of them during the meditation or should you just acknowledge them briefly and go back to focusing on the rise and follow the breath?

192
00:24:54,000 --> 00:25:04,000
I mean, I think there could be times where you do want to think about things, but generally overwhelmingly know, overwhelmingly,

193
00:25:04,000 --> 00:25:07,000
the thought arises it triggers an emotion.

194
00:25:07,000 --> 00:25:17,000
Now that wants to be your object of meditation and mindfulness, the thought and the emotion and the interplay here.

195
00:25:17,000 --> 00:25:23,000
And what it's like to cultivate that emotion and what is the nature of the thought?

196
00:25:23,000 --> 00:25:27,000
What is the nature of the emotion? What is the nature of the thought?

197
00:25:27,000 --> 00:25:32,000
And understanding the wholesomeness and the unwholesomeness involved in it.

198
00:25:32,000 --> 00:25:41,000
I mean, we're looking at, it's like the difference between watching television

199
00:25:41,000 --> 00:25:46,000
and taking your television apart to understand how that picture gets there.

200
00:25:46,000 --> 00:25:51,000
Two very different things. We're very much more interested in the latter.

201
00:25:51,000 --> 00:25:55,000
We're not interested in the television show.

202
00:25:55,000 --> 00:25:57,000
We're interested in watching TV.

203
00:25:57,000 --> 00:26:02,000
We're interested in understanding taking the television apart and understanding how it works.

204
00:26:02,000 --> 00:26:09,000
So when you talk about investigating the issues, you're just watching the television show.

205
00:26:09,000 --> 00:26:14,000
You're not actually understanding, hey, what's going on when this television show is on?

206
00:26:14,000 --> 00:26:18,000
How does that television show create it?

207
00:26:18,000 --> 00:26:24,000
When you focus on the mind, you're understanding how does this emotional baggage work?

208
00:26:24,000 --> 00:26:28,000
Not mucking around in the emotional, emotional baggage.

209
00:26:28,000 --> 00:26:34,000
For the most part, now, practically speaking, sometimes there's a question to be answered.

210
00:26:34,000 --> 00:26:38,000
Did this person really know, did they do something wrong?

211
00:26:38,000 --> 00:26:42,000
Did I do something wrong? Reflecting in that way can be useful.

212
00:26:42,000 --> 00:26:51,000
Conventionally, it's not the answer. It doesn't solve your problems, but it helps get your head on straight about something.

213
00:26:51,000 --> 00:26:57,000
Often, those questions aren't even that important. Who did what? Who was wrong? Who was right?

214
00:26:57,000 --> 00:27:01,000
They just end up driving a crazy, trying to think about it.

215
00:27:01,000 --> 00:27:11,000
But sometimes you have to do some intellectualizing just to get your head on straight about things.

216
00:27:11,000 --> 00:27:16,000
When not breathing, should the object of meditation be the most prominent sensation?

217
00:27:16,000 --> 00:27:23,000
When not breathing. That's the thing about the breath is it's kind of always there.

218
00:27:23,000 --> 00:27:29,000
We recommend that you stay with the breath and then if something distracts you from it, then focus on that.

219
00:27:29,000 --> 00:27:34,000
But if nothing else is distracting, you stay with the breath, stay with the stomach, rising and falling.

220
00:27:34,000 --> 00:27:38,000
Or just focus on something else that's simple like what we talked about today.

221
00:27:38,000 --> 00:27:45,000
You can just stay sitting, sitting, the postures of the body would ever posture your body as in if you can't feel the breath.

222
00:27:45,000 --> 00:27:50,000
Just stay sitting, sitting.

223
00:27:50,000 --> 00:27:55,000
Our compassion and apathy similar. Do you mean empathy?

224
00:27:55,000 --> 00:28:00,000
Apathy is very different from compassion. I think you mean empathy. I'm hoping you do.

225
00:28:00,000 --> 00:28:10,000
Because apathy means disinterest. Compassion means caring. It's quite different. Compassion means...

226
00:28:10,000 --> 00:28:25,000
Well, compassion technically is just non-cruelty. It's the desire for someone to help some or the inclination to help other beings become free from suffering.

227
00:28:25,000 --> 00:28:30,000
Apathy is pretty quite the opposite in that context.

228
00:28:30,000 --> 00:28:37,000
You mean empathy? Empathy is I think generally reserved for feeling someone else is feeling, right?

229
00:28:37,000 --> 00:28:44,000
If someone is sad, you feel sad with them. That's not the same as compassion.

230
00:28:44,000 --> 00:28:54,000
Compassion doesn't feel sad when someone else is sad. It's just inclined to free another person from sadness.

231
00:28:54,000 --> 00:28:58,000
Okay, there you go. That's the questions for tonight.

232
00:28:58,000 --> 00:29:25,000
Thank you all for coming out.

