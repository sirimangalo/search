1
00:00:00,000 --> 00:00:09,000
I have recently experienced involuntary body vibrations, starting from my lower extremities

2
00:00:09,000 --> 00:00:15,880
during my anepinacity meditation. I have to extend my feet from my meditative posture,

3
00:00:15,880 --> 00:00:28,000
which I felt disturbing my calmness of my body. Could you advise on this?

4
00:00:28,000 --> 00:00:39,000
Could you go into a little bit more detail with what you mean by involuntary body vibrations?

5
00:00:39,000 --> 00:00:48,000
Are you shaking, convulsing in the hips or legs? Or is it kind of like a...

6
00:00:48,000 --> 00:01:01,000
Because sometimes the legs sort of jolts a little bit at the...

7
00:01:01,000 --> 00:01:12,000
My feet were shaking and vibrating.

8
00:01:12,000 --> 00:01:19,000
Well, I don't know what to say. The first thing I say is not to move straight away,

9
00:01:19,000 --> 00:01:25,000
but to acknowledge the vibrations and acknowledge whoever feeling arises,

10
00:01:25,000 --> 00:01:30,000
like if you're frustrated or you're worried about the vibrations

11
00:01:30,000 --> 00:01:36,000
and acknowledge the vibrations, not just to move straight away.

12
00:01:36,000 --> 00:01:52,000
My best girlfriend was sitting here first of us in a course,

13
00:01:52,000 --> 00:01:56,000
and the teacher said, are you feeling any kind of sensations?

14
00:01:56,000 --> 00:02:04,000
She said, yes, what kind? I'm having this shaking, and she was really shaking a lot.

15
00:02:04,000 --> 00:02:09,000
And the teacher said, well, stop.

16
00:02:09,000 --> 00:02:14,000
And I thought to myself, because I could hear the question answers, oh gosh.

17
00:02:14,000 --> 00:02:19,000
And she didn't trust the teacher after that. She didn't want to talk to the teacher after that.

18
00:02:19,000 --> 00:02:23,000
Because she felt that if she couldn't stop, it was involuntary.

19
00:02:23,000 --> 00:02:38,000
And so I would say just to relax, and maybe say, shaking, shaking, shaking, and just know that you're not going to shake forever.

20
00:02:38,000 --> 00:02:45,000
This maybe could go on for months or years even, but lifetimes who knows,

21
00:02:45,000 --> 00:02:52,000
but the shaking probably will come and go. So nothing to worry about.

22
00:02:52,000 --> 00:02:56,000
It is what it is, no?

23
00:02:56,000 --> 00:03:00,000
Actually, I had a question about this, because I actually read,

24
00:03:00,000 --> 00:03:07,000
I had to have had the same problem before with the vibrations from just sitting in a inconvenient posture

25
00:03:07,000 --> 00:03:11,000
and built up some tension, and then after a while the body starts to vibrate.

26
00:03:11,000 --> 00:03:15,000
And I was just acknowledging it and not moving.

27
00:03:15,000 --> 00:03:20,000
And then I recently read in a book which said you should acknowledge it for a while,

28
00:03:20,000 --> 00:03:27,000
and then you should actually adjust your posture because it's just too distracting to meditation,

29
00:03:27,000 --> 00:03:30,000
to just keep shaking, shaking, shaking, shaking.

30
00:03:30,000 --> 00:03:34,000
So I don't know what you think about that.

31
00:03:34,000 --> 00:03:39,000
I think it's one of the examples of how you should never have a hard and fast rule.

32
00:03:39,000 --> 00:03:47,000
Remember what we're doing here. It's not a military exercise.

33
00:03:47,000 --> 00:03:51,000
It's not that you have to do it this way or this way, this way.

34
00:03:51,000 --> 00:03:56,000
It's a learning experience. So if you do it one way, you learn something in one way.

35
00:03:56,000 --> 00:04:01,000
If you do it another way, you learn something in another way.

36
00:04:01,000 --> 00:04:05,000
Where you get wrong is where you say, I will not move, I should not move.

37
00:04:05,000 --> 00:04:09,000
Or where you say, I will move, I have to move right away.

38
00:04:09,000 --> 00:04:18,000
So either you gain an incredible amount of stress from never moving or else you gain a great amount of aversion

39
00:04:18,000 --> 00:04:23,000
and you cultivate a habit of aversion by moving immediately when the problem arises.

40
00:04:23,000 --> 00:04:30,000
Actually, so I would say, don't establish a rule like that at all.

41
00:04:30,000 --> 00:04:33,000
And there's a meditation teacher I wouldn't and I would say, well,

42
00:04:33,000 --> 00:04:38,000
you can try different things with the meditators and say try to acknowledge it first.

43
00:04:38,000 --> 00:04:45,000
And if it doesn't go away, then how we would explain it is if you do move, then be mindful of it.

44
00:04:45,000 --> 00:04:48,000
So we would never say and then move or then don't move.

45
00:04:48,000 --> 00:04:50,000
We would say be mindful of it as much as you can.

46
00:04:50,000 --> 00:04:57,000
And then if you have to move, just say moving, when you move your legs.

47
00:04:57,000 --> 00:05:06,000
And allow the meditator to experience it as they will.

48
00:05:06,000 --> 00:05:09,000
But the original question here is a little bit different.

49
00:05:09,000 --> 00:05:12,000
Well, first of all, because you're practicing it on a pana sati.

50
00:05:12,000 --> 00:05:14,000
Not on a pana sati.

51
00:05:14,000 --> 00:05:16,000
On a pana sati.

52
00:05:16,000 --> 00:05:18,000
On a pana sati.

53
00:05:18,000 --> 00:05:19,000
On a pana sati.

54
00:05:19,000 --> 00:05:23,000
And I mean the in-breath pana, I mean the out-breath or vice versa.

55
00:05:23,000 --> 00:05:25,000
Wonder the other.

56
00:05:25,000 --> 00:05:28,000
And this isn't exactly what we teach.

57
00:05:28,000 --> 00:05:34,000
I mean, if you call it on a pana sati, you're probably practicing some form of tranquility meditation.

58
00:05:34,000 --> 00:05:36,000
I mean, that's what it sounds like.

59
00:05:36,000 --> 00:05:48,000
It's because the question here is how to keep the calm or how to cope with the loss of calm.

60
00:05:48,000 --> 00:05:58,000
And from a Vipass and an insight point of view, the problem isn't that moving disturbs your calm.

61
00:05:58,000 --> 00:06:00,000
The problem is that you don't want the calm to be disturbed.

62
00:06:00,000 --> 00:06:03,000
The problem is that there's a clinging to the calm.

63
00:06:03,000 --> 00:06:15,000
And that's in the technical jargon that we call nikanti, which means this subtle clinging to something.

64
00:06:15,000 --> 00:06:17,000
To something positive.

65
00:06:17,000 --> 00:06:22,000
So when you experience happiness, you'll cling to the happiness and you'll become content with happiness.

66
00:06:22,000 --> 00:06:25,000
If there's calm, you'll become content with the calm.

67
00:06:25,000 --> 00:06:27,000
If there's quiet, you'll become content with the quiet.

68
00:06:27,000 --> 00:06:32,000
If you see lights and colors and pictures, you'll become content with them.

69
00:06:32,000 --> 00:06:36,000
Whatever good experiences arise in meditation, you'll become attached to it.

70
00:06:36,000 --> 00:06:40,000
So the point is not to feel this calm and the point is not for that calm to stay.

71
00:06:40,000 --> 00:06:48,000
The Buddha taught that the path to become free from sufferings to see impermanence, suffering, and non-self.

72
00:06:48,000 --> 00:06:54,000
So if I can ask you when the calm disappears, does that mean it's permanent or impermanent?

73
00:06:54,000 --> 00:07:02,000
Is it satisfying or unsatisfying? I mean, can it really satisfy you or is it unable to satisfy you?

74
00:07:02,000 --> 00:07:07,000
And maybe you still think that it's satisfying, you just have to figure out the way to make it satisfying.

75
00:07:07,000 --> 00:07:08,000
But actually it's not.

76
00:07:08,000 --> 00:07:12,000
Actually what you're seeing is a part of the truth that it is not satisfying.

77
00:07:12,000 --> 00:07:15,000
And non-self that you can't control it.

78
00:07:15,000 --> 00:07:16,000
You can't keep the calm.

79
00:07:16,000 --> 00:07:18,000
You can't move and still have the calm.

80
00:07:18,000 --> 00:07:21,000
So you have to see that the calm is not permanent.

81
00:07:21,000 --> 00:07:25,000
This is really the core of the answer to the question.

82
00:07:25,000 --> 00:07:34,000
Now, in regards to vibrating, dealing with shaking, dealing with swaying and many experiences like it,

83
00:07:34,000 --> 00:07:42,000
the theory that we hold behind this is that it is related to rapture.

84
00:07:42,000 --> 00:07:45,000
Or it comes under the heading of rapture.

85
00:07:45,000 --> 00:07:56,000
So if you ever see Christians, these Christians are evangelical, even evangelists who go into this rapture and start convulsing on the floor.

86
00:07:56,000 --> 00:08:03,000
So that's the sort of phenomenon that we're dealing with.

87
00:08:03,000 --> 00:08:05,000
Some people will sit and shake back and forth.

88
00:08:05,000 --> 00:08:08,000
Some people will, their whole body will vibrate and so on.

89
00:08:08,000 --> 00:08:11,000
There are many things and it can become quite strong.

90
00:08:11,000 --> 00:08:17,000
So in fact, one technique is to stop, is to tell yourself to stop, one teacher in Bangkok.

91
00:08:17,000 --> 00:08:21,000
He said, the way you have to do it is just say to yourself, stop.

92
00:08:21,000 --> 00:08:23,000
Tell yourself to stop.

93
00:08:23,000 --> 00:08:31,000
And the point behind that is not that you're trying to control it, but the point is that even though you say that it's involuntary,

94
00:08:31,000 --> 00:08:39,000
the fact that the fact that you feel calm or so on suggests that there's some kind, there could be

95
00:08:39,000 --> 00:08:48,000
some situation, some kind of propulsion or impulsion intention in the mind that is very subliminal.

96
00:08:48,000 --> 00:08:53,000
And so subliminal that you are unable to detect that it's even there.

97
00:08:53,000 --> 00:08:58,000
So there's the contentment with the feelings and that creates the shaking and the vibrating.

98
00:08:58,000 --> 00:09:03,000
And then you become even caught up in the vibrating.

99
00:09:03,000 --> 00:09:08,000
So when you say to yourself, stop, you're changing that intention.

100
00:09:08,000 --> 00:09:15,000
It's like you're adjusting your volition.

101
00:09:15,000 --> 00:09:17,000
There's power in words.

102
00:09:17,000 --> 00:09:20,000
So when you would say, just say to yourself, stop.

103
00:09:20,000 --> 00:09:27,000
Instead of saying, feeling, feeling or so on, just tell yourself to stop, just say to yourself, stop.

104
00:09:27,000 --> 00:09:33,000
And your mind will change and your mind will wake up and because of the harshness of it.

105
00:09:33,000 --> 00:09:37,000
Your mind will adjust and then you'll be able to be mindful.

106
00:09:37,000 --> 00:09:44,000
Because if you just continue to acknowledge it, because of the underlying attachment and not acknowledging that attachment,

107
00:09:44,000 --> 00:09:46,000
it won't really be an acknowledgement.

108
00:09:46,000 --> 00:09:50,000
It will become a mantra and you'll just get caught up more shaking.

109
00:09:50,000 --> 00:09:59,000
This is one, actually one particular special sort of experience that has to be dealt with a little bit special sometimes.

110
00:09:59,000 --> 00:10:05,000
Because of the repetitive and the habitual nature of it, it can become so,

111
00:10:05,000 --> 00:10:08,000
the mind can become so caught up in it that it just gets out of control.

112
00:10:08,000 --> 00:10:12,000
And as you see these Christians, they just start convulsing and it's totally,

113
00:10:12,000 --> 00:10:14,000
when they say it was uncontrollable.

114
00:10:14,000 --> 00:10:17,000
But the truth is that they're controlling it, they're creating it, they're cultivating it.

115
00:10:17,000 --> 00:10:21,000
And eventually they become so good at it that it seems to come by itself.

116
00:10:21,000 --> 00:10:25,000
But through the whole process, there's a cultivation going on.

117
00:10:25,000 --> 00:10:29,000
And when you make a determination to stop it, that can help.

118
00:10:29,000 --> 00:10:38,000
But the key here, so there are two keys. First of all, you may very well be cultivating the vibrating and the shaking.

119
00:10:38,000 --> 00:10:41,000
And in that case, you have to try different things.

120
00:10:41,000 --> 00:10:46,000
You can try moving is fine, but just be mindful moving, moving.

121
00:10:46,000 --> 00:10:50,000
And if it's really not going away, you can tell it to stop.

122
00:10:50,000 --> 00:10:58,000
Tell your mind to stop, but don't expect it to work all the time, but it can help to remove the clinging in the mind.

123
00:10:58,000 --> 00:11:02,000
And sometimes you can just acknowledge it.

124
00:11:02,000 --> 00:11:06,000
Sometimes if you just acknowledge it and into the point that the mind becomes bored of it,

125
00:11:06,000 --> 00:11:10,000
then the mind will let go of it, and there will be a change to the ground of the mind.

126
00:11:10,000 --> 00:11:14,000
And really in the end, that is the key is to eventually just become bored of it.

127
00:11:14,000 --> 00:11:18,000
And when the mind is truly bored of it, it will disappear.

128
00:11:18,000 --> 00:11:22,000
The mind will stop cultivating and be like, enough of that. It's not really making me happy.

129
00:11:22,000 --> 00:11:25,000
It's not really doing anything for me.

130
00:11:25,000 --> 00:11:29,000
So that's the first part. The other problem you have here is the clinging to the calm.

131
00:11:29,000 --> 00:11:34,000
And the wrong idea that calm somehow has some intrinsic benefit.

132
00:11:34,000 --> 00:11:41,000
The purpose of calm is to see things clearly. Once you're calm, then you have to apply insight to the experience,

133
00:11:41,000 --> 00:11:45,000
whether it be the calmness, or whether it be the object of meditation.

134
00:11:45,000 --> 00:11:48,000
You have to look at that and be able to see impermanent.

135
00:11:48,000 --> 00:11:51,000
So in fact, what you're seeing is a positive thing.

136
00:11:51,000 --> 00:11:56,000
You're seeing impermanence suffering in oneself, but because of the clinging and because of the defilements in the mind,

137
00:11:56,000 --> 00:12:00,000
there's still the attachment and the thought that something's wrong, it's changing.

138
00:12:00,000 --> 00:12:07,000
Something wrong, it's disappeared. That's the truth of life. Everything that arises disappears.

139
00:12:07,000 --> 00:12:23,000
Okay.

