1
00:00:00,000 --> 00:00:14,480
Good evening and this is a short video to talk about a program that we are planning for

2
00:00:14,480 --> 00:00:18,920
January of 2020.

3
00:00:18,920 --> 00:00:28,480
So some years ago I took a trip down the east coast of the United States, starting in Massachusetts

4
00:00:28,480 --> 00:00:31,440
and went all the way down to Florida.

5
00:00:31,440 --> 00:00:37,640
And along the way I stayed with people who were interested in learning about the practice

6
00:00:37,640 --> 00:00:40,000
or sharing it with people in their community.

7
00:00:40,000 --> 00:00:47,240
We set this up as a program and invited people to set something up in their area along that

8
00:00:47,240 --> 00:00:51,520
sort of general route from Massachusetts to Florida.

9
00:00:51,520 --> 00:00:52,520
And it was great.

10
00:00:52,520 --> 00:00:53,520
It worked really well.

11
00:00:53,520 --> 00:00:55,360
I ended up staying at a Catholic monastery.

12
00:00:55,360 --> 00:00:59,160
I stayed in a tent some of the time.

13
00:00:59,160 --> 00:00:59,880
I stayed in Monastery a couple of times.

14
00:00:59,880 --> 00:01:08,520
But it was all people who really had the keen interest for something like this to take

15
00:01:08,520 --> 00:01:12,720
root or to plant the seed in their community.

16
00:01:12,720 --> 00:01:17,600
So in January I have a month and we're thinking I'm doing the same thing in the west coast

17
00:01:17,600 --> 00:01:19,080
of the United States.

18
00:01:19,080 --> 00:01:21,800
So I'm not really sure how it's going to work.

19
00:01:21,800 --> 00:01:27,680
One idea we had maybe from Arizona up to Seattle or Vancouver even.

20
00:01:27,680 --> 00:01:28,680
But something similar.

21
00:01:28,680 --> 00:01:34,720
Taking buses from place to place and staying in tent, I have a tent and maybe going with

22
00:01:34,720 --> 00:01:42,000
someone else if there's anyone who wants to go with me just sort of make it easy safety

23
00:01:42,000 --> 00:01:47,080
in numbers or whatever.

24
00:01:47,080 --> 00:01:54,120
But this is a call today for people about the announcement of this plan and a call for

25
00:01:54,120 --> 00:02:00,440
people who are interested who live in the western of the United States and would like

26
00:02:00,440 --> 00:02:10,240
to set something up in their area for to put in our plan along this trip.

27
00:02:10,240 --> 00:02:12,920
Would like us to visit.

28
00:02:12,920 --> 00:02:17,640
It might be one day or two days or three days if you have the facilities can set something

29
00:02:17,640 --> 00:02:22,880
up even just one day or two days.

30
00:02:22,880 --> 00:02:28,200
But something to do with meditation, it's not just going to be dhamma talks or something

31
00:02:28,200 --> 00:02:29,200
like that.

32
00:02:29,200 --> 00:02:34,320
The idea is that we might set up at least a one day course in your area.

33
00:02:34,320 --> 00:02:40,600
One day, two days, whatever you can manage and if you can we want you to let us know

34
00:02:40,600 --> 00:02:48,840
that we're going to assess sort of the potential to see where we can go and which places

35
00:02:48,840 --> 00:02:50,680
are feasible and so on.

36
00:02:50,680 --> 00:02:53,680
We have a group set set up already.

37
00:02:53,680 --> 00:02:59,200
Our volunteers are aware of this and ready for it and all they've done now, they said

38
00:02:59,200 --> 00:03:10,240
all now left to do is for me to announce it and then you can contact them and get involved.

39
00:03:10,240 --> 00:03:17,240
So the way to get involved is to contact us for now, the way to get involved is to contact

40
00:03:17,240 --> 00:03:20,120
us through our Discord server.

41
00:03:20,120 --> 00:03:24,560
So Discord is this platform we were on Slack before but we've moved to Discord.

42
00:03:24,560 --> 00:03:29,960
It's this platform that allows communities to get together and we've been using it for quite

43
00:03:29,960 --> 00:03:32,520
a while now for our dhamma study group.

44
00:03:32,520 --> 00:03:37,920
We meet once a week and we are able to hear each other and we read a book and we talk about

45
00:03:37,920 --> 00:03:41,160
it, we read some teaching of some sort.

46
00:03:41,160 --> 00:03:46,800
So you're welcome, if you're interested in joining the Discord server anyway, there's greatness

47
00:03:46,800 --> 00:03:52,360
to it, it's our community, sort of the heart of our online community now.

48
00:03:52,360 --> 00:03:57,440
We have a volunteer section for people who are helping the organization run.

49
00:03:57,440 --> 00:04:01,760
We have the dhamma study group, we just have other channels for people who are interested

50
00:04:01,760 --> 00:04:06,360
in communicating or cultivating the sense of an online community.

51
00:04:06,360 --> 00:04:11,240
You're welcome to join anyway but now we've set up a new channel called something like

52
00:04:11,240 --> 00:04:16,360
teaching tour 2020 or something like that and it's for all of January 2020, if you're

53
00:04:16,360 --> 00:04:21,960
interested in getting involved in this, go there, put a link in the description of this

54
00:04:21,960 --> 00:04:27,080
video, go there and let us know that you're interested, let us know what sort of things

55
00:04:27,080 --> 00:04:33,840
you're thinking you might be able to set up and our volunteers will talk to you about

56
00:04:33,840 --> 00:04:39,640
it and we'll consider and figure out what's possible and we're going to plan and then

57
00:04:39,640 --> 00:04:46,040
we'll post that plan and let everyone know and hopefully do some great things just as

58
00:04:46,040 --> 00:04:48,040
last time.

59
00:04:48,040 --> 00:04:55,080
So please do if you're interested, let us know and get involved and hope to see you

60
00:04:55,080 --> 00:05:07,960
in January, if you're able to set something up, have a good night.

