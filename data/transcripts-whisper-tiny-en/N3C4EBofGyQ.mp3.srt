1
00:00:00,000 --> 00:00:08,400
Bunte, I am a Nupasika practicing at a monastery in the USA. I want to ordain and restrain

2
00:00:08,400 --> 00:00:12,920
according to the body mocha to the best of my ability. I am very discouraged with how

3
00:00:12,920 --> 00:00:20,000
most monks in America I have seen practice. Will you please help guide me?

4
00:00:20,000 --> 00:00:31,840
Yes, please help guide you well. Yes, in the sense that this is clear that in today's

5
00:00:31,840 --> 00:00:44,800
age the standard of keeping the Buddhist monastic discipline is pretty low, unfortunately.

6
00:00:44,800 --> 00:00:53,720
So there is a lot of breaking of rules and so on. I am probably getting it in trouble

7
00:00:53,720 --> 00:00:57,840
for even saying that we are not supposed to talk about it, but that is not really true.

8
00:00:57,840 --> 00:01:01,920
We are not supposed to say this monk broke this rule that monk broke that rule, but

9
00:01:01,920 --> 00:01:08,480
because we do not want to be tattletales and cause conflicts. But to just talk about

10
00:01:08,480 --> 00:01:12,920
the state of the sun guiding is very important. It is important that we are not hiding

11
00:01:12,920 --> 00:01:18,320
it, because the point is not to hide it, the point is to be clear about who is the spokesperson

12
00:01:18,320 --> 00:01:26,440
for the sun. I guess it is true that I am not really the spokesperson, but it can't

13
00:01:26,440 --> 00:01:31,720
be denied that when you go to the monasteries, if you spend some time there, if you go,

14
00:01:31,720 --> 00:01:35,120
you are superficial visitor, you often don't see it, but if you stay for a while you start

15
00:01:35,120 --> 00:01:42,800
to see that it is getting shaky. That is not a good thing, and it is not a good environment

16
00:01:42,800 --> 00:01:48,000
for the practice. One thing you have to understand is it is tough being a monk, so breaking

17
00:01:48,000 --> 00:01:55,040
the precepts happens, and you have to understand the levels of breaking the precepts. So

18
00:01:55,040 --> 00:02:01,640
if a monk watches football, for example, which seems to apparently everyone is watching

19
00:02:01,640 --> 00:02:06,120
football, I am not even sure if it is still on, but I think I heard recently they are

20
00:02:06,120 --> 00:02:13,640
near the last game, but I am not quite sure. Germany I think is playing, so that is

21
00:02:13,640 --> 00:02:23,560
as far as I go with that. So if a monk is watching the world cup of football, soccer,

22
00:02:23,560 --> 00:02:31,960
maybe you call it. It is not a good thing, it is not a good sign, but that monk could

23
00:02:31,960 --> 00:02:41,520
still recover. It is not a major preset by any means watching a football game, so that monk

24
00:02:41,520 --> 00:02:46,360
can come back and realize that there are better things to do, and can come to see the

25
00:02:46,360 --> 00:02:51,780
uselessness of that, and can reflect upon watching the game and say to themselves, that

26
00:02:51,780 --> 00:02:57,240
is really not bringing me happiness, and so still has potential to develop, and this sort

27
00:02:57,240 --> 00:03:03,640
of thing is something that goes on the other side. So some people are too forgiving,

28
00:03:03,640 --> 00:03:08,880
and they just let monks do whatever they want, and say whatever, it is fine to each their

29
00:03:08,880 --> 00:03:13,040
own kind of thing. But the other side is where people, every little thing, they begin

30
00:03:13,040 --> 00:03:18,640
to criticize, and they storm out of the monasteries, and right nasty blog posts, because

31
00:03:18,640 --> 00:03:26,320
the monks did something, it can often be, there is one monk who split a community apart,

32
00:03:26,320 --> 00:03:31,240
I have told this before, because he was eating eggs, I think, and because he was eating

33
00:03:31,240 --> 00:03:36,200
meat, and so he split a community apart just over something like that, which isn't even

34
00:03:36,200 --> 00:03:41,000
in the Vinaya. Eating eggs that haven't been fertilizing meat that hasn't been killed

35
00:03:41,000 --> 00:03:46,160
for you, this is not against the Vinaya, the monks roles, and yet it managed to split

36
00:03:46,160 --> 00:03:52,440
an entire community apart, many people wouldn't even go to see him. So that kind of

37
00:03:52,440 --> 00:03:57,120
thing is common in the East and in the West, in all places. Westerners go into Buddhist

38
00:03:57,120 --> 00:04:00,760
monasteries and say, this isn't real. They look at the Buddha image, and they say, that's

39
00:04:00,760 --> 00:04:06,840
not really Buddhism, I spit on your Buddha, that kind of thing. And this is going too far.

40
00:04:06,840 --> 00:04:12,760
I think you have to have compassion and understanding that it's tough being a monk, and

41
00:04:12,760 --> 00:04:20,480
it's especially tough being a monk in the West. If you're only monastic training has been

42
00:04:20,480 --> 00:04:25,560
in a university, which for a lot of these monks it is, and right out of university you're

43
00:04:25,560 --> 00:04:35,480
given this instruction on how to be a missionary, and then you go abroad. There's not the

44
00:04:35,480 --> 00:04:40,240
practical backing to help you reflect upon these things and help you let go of them.

45
00:04:40,240 --> 00:04:45,840
So to some extent you have to be forgiving and understand that it's still possible for

46
00:04:45,840 --> 00:04:53,560
such people to recover. Now if there's a lot of corruption and attachment to money and

47
00:04:53,560 --> 00:04:59,440
like asking late, some of the bad signs are like asking late people for money, over attachment

48
00:04:59,440 --> 00:05:07,760
between the monks and the women, and over familiarity there is a bad sign. Excessive

49
00:05:07,760 --> 00:05:14,960
luxury, where the monks have wide screen televisions, and you see satellite, satellite dishes

50
00:05:14,960 --> 00:05:22,920
are usually a bad sign. But as I say, it's not a huge deal, but not a good sign when

51
00:05:22,920 --> 00:05:30,880
you see satellite dishes and a lot of wide screen televisions and that kind of thing.

52
00:05:30,880 --> 00:05:38,520
If the monks have stopped wearing robes, that's a really bad sign, that kind of thing.

53
00:05:38,520 --> 00:05:46,040
But that being said, there's no question that you will benefit from a traditional monastic

54
00:05:46,040 --> 00:05:52,800
environment where you at least are encouraged and allowed and taught how to practice the

55
00:05:52,800 --> 00:05:57,560
discipline of the Buddha. I didn't have that. When I started out, I was lucky as the

56
00:05:57,560 --> 00:06:04,160
monastery, what they did have was a strong tradition of meditation. So everyone had to start

57
00:06:04,160 --> 00:06:11,640
by practicing meditation and there's an environment that is supportive for the meditative

58
00:06:11,640 --> 00:06:16,120
practice. Now, what I didn't have was a strong monastic training. Actually, I was able

59
00:06:16,120 --> 00:06:22,800
to make that up on my own and by reading the books. Even to this day, I don't mind being

60
00:06:22,800 --> 00:06:27,000
in that monastery surrounded by monks who may not be keeping the rules. It doesn't really

61
00:06:27,000 --> 00:06:33,080
bother me because I have the focus on the meditation, which allows me to stay to myself

62
00:06:33,080 --> 00:06:41,360
and do my own discipline and not care what other people do. So much. So that's one thing

63
00:06:41,360 --> 00:06:47,280
is to focus more on the meditation practice than on the discipline. Does this place have

64
00:06:47,280 --> 00:06:53,360
a strong meditative tradition instead of does it have a strong monastic discipline?

65
00:06:53,360 --> 00:07:00,680
But best is if you can find a place where they teach both. I understand that what pananach

66
00:07:00,680 --> 00:07:06,720
had is a good place, especially to learn when I stick discipline. But the best place is

67
00:07:06,720 --> 00:07:10,920
that I've seen her in Sri Lanka. If you want to ordain as a monk, if anyone wants to ordain

68
00:07:10,920 --> 00:07:16,760
as a monk, your best bet is to go to Sri Lanka. Now, the problem with going to Asia at

69
00:07:16,760 --> 00:07:28,600
all to become a monk is that the general routine is you have to actually go once, practice

70
00:07:28,600 --> 00:07:33,880
with them, and then get a promise from them that they're going to write you a letter or

71
00:07:33,880 --> 00:07:42,600
whatever. And then you actually have to return to your country and apply for a new visa

72
00:07:42,600 --> 00:07:49,400
with a letter of permission to become a monk and then fly back to the country again.

73
00:07:49,400 --> 00:07:58,560
A second time to actually think about ordaining because you need a special visa to ordain

74
00:07:58,560 --> 00:08:03,760
and the only way you can get that visa is with a letter of permission from the monastery and

75
00:08:03,760 --> 00:08:08,640
the only way they're going to give you that permission is if you're already in the country

76
00:08:08,640 --> 00:08:12,760
had practicing with them. You see, it's almost a catch-22, but what it means is you need

77
00:08:12,760 --> 00:08:15,280
to go twice. And so that's what you have to look at there.

78
00:08:15,280 --> 00:08:19,480
I'll turn into this to find one of these places that does keep them in North America.

79
00:08:19,480 --> 00:08:24,080
Another usually full, because obviously everybody wants those. There's what abugiri

80
00:08:24,080 --> 00:08:28,960
in California. There's bow and a society. I don't know how strict they are, but they're

81
00:08:28,960 --> 00:08:39,680
probably fairly good. There's meta-monastery in San Diego. And in general, check out the...

82
00:08:39,680 --> 00:08:44,240
Oh, I don't know. I don't want to say that, but no, I'm not going to say it. Those are

83
00:08:44,240 --> 00:08:50,960
my only suggestions. You're welcome to come here. You're going to see some of the...

84
00:08:50,960 --> 00:08:54,960
But the USA is still difficult for us because you still need a visa coming here, and I don't

85
00:08:54,960 --> 00:09:02,960
even think that really works. Not over the long term. But to find a place that is focused on

86
00:09:02,960 --> 00:09:09,760
meditation, because as I said, that can be another way to center yourself. The Buddha

87
00:09:09,760 --> 00:09:13,680
himself even said, you know, a lot of these rules. If you're practicing meditation, you don't

88
00:09:13,680 --> 00:09:17,120
really have to worry about the rules. If you happen to break one, it's not such a big deal.

89
00:09:17,120 --> 00:09:23,200
But if you're in a place that's not practicing meditation. In fact, if you're in a place that's

90
00:09:23,200 --> 00:09:29,280
not practicing insight meditation, even if they have a strong monastic practice, it's still not

91
00:09:29,280 --> 00:09:35,760
going to lead to enlightenment. So focus more on the practice. That will give you an ability

92
00:09:35,760 --> 00:09:41,680
to cultivate the moral discipline yourself. Without the practice, it's very difficult to cultivate

93
00:09:41,680 --> 00:09:47,440
moral discipline. But vice versa. Without the moral discipline, if you're breaking the moral

94
00:09:47,440 --> 00:09:57,360
discipline, they're also difficult. But I still lean towards the meditation. If you can find

95
00:09:57,360 --> 00:10:03,040
that, you can make up the monastic discipline yourself. Don't rely too much on other people

96
00:10:03,040 --> 00:10:11,840
for that, because it's just rules. So focus more, not completely, but more on the meditation.

