1
00:00:00,000 --> 00:00:07,200
Is there anything outside Samsara? If everything has a cause, does Samsara have it too?

2
00:00:07,200 --> 00:00:15,120
How does one first come into Samsara? Three questions here. I want to answer them individually.

3
00:00:15,120 --> 00:00:27,440
Is there anything outside Samsara? Samsara is a name for the conditioned universe. It encompasses

4
00:00:27,440 --> 00:00:33,520
everything that arises and therefore everything that ceases. It encompasses everything that is

5
00:00:33,520 --> 00:00:44,640
impermanent, unsatisfying, uncontrollable. In its briefest description, it is the five aggregates

6
00:00:45,680 --> 00:00:51,920
or even briefer, I suppose, just body and mind, physical and mental aspects of experience.

7
00:00:51,920 --> 00:01:00,400
And so the word Samsara, if I'm not mistaken, means wandering on, wandering, sara-ti,

8
00:01:01,600 --> 00:01:08,240
sara-ti, sung sara. As I understand, I can't remember the derivation, but it means wandering,

9
00:01:08,240 --> 00:01:17,600
so it's a circle going around in circle, born, getting old, sick and dying. Without any,

10
00:01:17,600 --> 00:01:25,600
the wandering aspect, this means without any goal or any resolution, without getting anywhere,

11
00:01:25,600 --> 00:01:33,200
without actually accomplishing anything, without any destination, most beings without any real goal,

12
00:01:35,040 --> 00:01:43,440
without any purpose, and so on. But that which it is comprised of are the five aggregates

13
00:01:43,440 --> 00:01:50,080
or simply put body and mind, physical and mental aspects of experience. So is there anything

14
00:01:50,080 --> 00:01:56,160
outside of that? Yes. There are two things, I think you can say there are two things that are outside

15
00:01:56,160 --> 00:02:08,000
of that. One, things that don't exist. Concept are not a part of Samsara. I don't think you

16
00:02:08,000 --> 00:02:14,640
could call them a part of Samsara because they're not real, they don't exist. A concept,

17
00:02:14,640 --> 00:02:23,200
the chair doesn't exist. Monk is not something that exists. It's the object or it's the

18
00:02:25,440 --> 00:02:31,680
substance of a thought, of a thought, of a recognition that arises. It's just a word.

19
00:02:31,680 --> 00:02:39,920
The actual chair, the actual monk doesn't exist in reality. So I say that would be one thing

20
00:02:39,920 --> 00:02:49,200
you could argue isn't a part of Samsara, but it may be, anyway, whatever. More importantly,

21
00:02:49,200 --> 00:02:53,440
the one thing that they say that is outside of Samsara because it's unconditioned,

22
00:02:53,440 --> 00:03:02,880
because it's satisfying, controllable. No, sorry, because it's permanent and satisfying.

23
00:03:02,880 --> 00:03:08,960
It's still uncontrollable, but it's permanent and satisfying. So I made a mistake there in the

24
00:03:08,960 --> 00:03:12,800
beginning. I said, Samsara encompasses everything that is uncontrollable. It doesn't,

25
00:03:13,360 --> 00:03:20,640
there's one extra thing that is uncontrollable outside of Samsara. Samedama and that all

26
00:03:20,640 --> 00:03:29,360
damas, including nibana or nirvana are non-self or uncontrollable. So the one thing that is really

27
00:03:29,360 --> 00:03:36,000
and truly outside of Samsara is nibana or nirvana because it's unconditioned, it doesn't arise,

28
00:03:36,000 --> 00:03:44,800
it doesn't cease, it isn't born, it doesn't die, it's permanent and satisfying.

29
00:03:44,800 --> 00:03:51,440
And so that's the one thing outside of Samsara. If everything has a cause,

30
00:03:52,800 --> 00:03:57,520
it's okay, not everything has a cause because nibana doesn't have a cause and

31
00:03:58,720 --> 00:04:02,880
yeah, because nibana doesn't have a cause. Does Samsara have a cause?

32
00:04:03,840 --> 00:04:09,440
Okay, so here's where we have to say that actually Samsara doesn't exist, it's a concept,

33
00:04:09,440 --> 00:04:14,560
it's the name that we have for the process, but the process is actually made up of building

34
00:04:14,560 --> 00:04:18,800
blocks and those building blocks actually exist. Those are the things that have causes.

35
00:04:20,000 --> 00:04:27,440
I think you could say concepts, technically speaking, don't have a cause because by the

36
00:04:27,440 --> 00:04:32,240
word cause you mean something that causes it to arise and concepts do not arise, a concept is

37
00:04:32,240 --> 00:04:43,600
something that doesn't exist. It's a illusion, or you could say it exists but not an ultimate

38
00:04:43,600 --> 00:04:51,120
reality. So it has a cause and a conventional sense, but in reality is uncaused.

39
00:04:51,920 --> 00:04:55,280
I would think, I mean these are all technical words and I'm probably saying

40
00:04:56,240 --> 00:05:01,760
probably the technical answer is a bit different, but there is a distinction to me made.

41
00:05:05,440 --> 00:05:10,240
When you conflate these and you don't make this distinction, you start to say well everything

42
00:05:10,240 --> 00:05:15,600
has a cause to Samsara have as well and you're talking about two different things. In Buddhism,

43
00:05:15,600 --> 00:05:22,720
those things that have a cause are the five aggregates, the physical and mental aspects of

44
00:05:22,720 --> 00:05:31,680
experience. They are causally formed and so that's what we focused on. Now, Samsara is just a name

45
00:05:31,680 --> 00:05:39,120
for all of that. So the third question, how does one first come into Samsara? The implication

46
00:05:39,120 --> 00:05:46,240
is that it has a cause, there was something before it, which is one of those questions not answered

47
00:05:46,240 --> 00:05:53,040
in Buddhism and you can disregard it simply because it's meaningless, purposeless, it's in the past,

48
00:05:54,240 --> 00:06:02,640
but it seems also to be potentially unanswerable or improper as a question because it relies on

49
00:06:02,640 --> 00:06:11,120
the idea that time is linear, the idea that the past actually somehow exists or the idea

50
00:06:11,120 --> 00:06:18,800
that things are the past is the cause of everything and so on. In Buddhism, we don't start with

51
00:06:18,800 --> 00:06:25,360
the past, we start with the present and the present is creating the past and bringing about the

52
00:06:25,360 --> 00:06:33,200
future, but we take the present as our base kind of like ripples in a pool of water, everything

53
00:06:33,200 --> 00:06:39,760
the past and the future expand out from the present moment, which actually may be supported somehow

54
00:06:39,760 --> 00:06:45,440
by things like quantum physics, which seems to suggest that the future can actually affect the

55
00:06:45,440 --> 00:06:54,960
past or seems to not suggest, but that has the potential to show one of the answers is to show

56
00:06:54,960 --> 00:07:01,520
that the future can actually affect the past, which is really strange, but would explain the

57
00:07:01,520 --> 00:07:08,080
ability to predict the future, see the future and that kind of thing. Anyway, one of the questions

58
00:07:08,080 --> 00:07:17,520
that the Buddha declined to answer is the idea of something, how one came into, what was before

59
00:07:17,520 --> 00:07:32,720
some time or something.

