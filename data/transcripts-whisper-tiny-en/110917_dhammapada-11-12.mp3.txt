Hello and welcome back to our study of the Dhamma Pandat.
Today we will continue with verses number 11 and 12, which we just follow.
So, it is a twin pair of verses.
The first one, Assare Saramatino, for a person who understands what is essential to
be unessential.
Or, understand, sorry, what understands what is unessential to be essential.
Saro, tasare, tasare, dasino, and understands what is essential or sees what is essential
as being unessential.
Tasare, dasare, dicatanti, such a person doesn't come to what is essential.
Mita, sankapagotara, because they dwell in wrong thought and a wrong understanding.
Saran, tasare, tauyatua, a person who knows the essential to be essential.
Assare, tasare, tau, and what is unessential to be unessential.
Tasare, dicatanti, sankapagotara, such a person does come to what is essential because
they dwell in right thought.
Now, these two verses were told in relation to the Buddha's two chief disciples.
And their story, in brief, is that they were living in Radhikaha and the time when the Buddha
had just become enlightened and they had left the home life because they had seen that
there was no essence or nothing, it wasn't essential or there was no benefit to be found
in the life that they were living, they would go to these shows and they would laugh
and they would cry and they found that it brought them no real true peace and happiness
because it was bereft of any sort of essence or any depth.
And so they left home and they went to try to find a teacher and they found the first
teacher they found was named Sanjaya.
Sanjaya was a teacher in the time of the Buddha and he taught his students how to avoid
views, how to avoid any kind of teaching actually.
So when people would ask them question, they would say, well, I don't believe that,
but I don't believe otherwise.
And so they would find this way to avoid answering the question and therefore avoid taking
any committing to any stance.
And of course this has the benefit, at least superficially, of keeping a person from developing
wrong views but it becomes a real conceit and attachment in itself.
And of course the mind is always running, running around in circles trying to avoid any
kind of stance on the theory and that you develop a theory as a result.
There's this theory that any stance is untenable, which of course would make sense when
no one has the understanding of reality because all of the people's views in the time were
based simply on their partial or incorrect understanding of reality.
And once you find the truth and they would avoid even that and they would avoid committing
even to the truth.
And so at first they thought this was kind of a neat idea and so they practiced it and
when they realized that it was really quite superficial and shallow and simple actually
all you have to do is just avoid everything, avoid answering these difficult questions
and there you have this teaching.
So they left it behind as well and they went to find another teacher and then it so
happened that one of them, Upatissa, their names were Upatissa and Colita, Upatissa one
day he saw one of the Buddha's disciples walking for arms, it was one of the first five
disciples of the Buddha and it had such a profound effect on him, it's just seeing this
monk walking on arms because obviously this was an enlightened being and so the way he
walked, the way he held his robe, the way he held his bull, the way he looked, the way he
would talk, the way he would keep silent, everything about the way he acted his whole
department said that there was something special about him, it was just so natural and
so clear and so perfect and it wasn't that he was had some heavy kind of focused or
forced way of behaving, it was that it was very natural, very fluid and it spoke volumes
about his inner character.
So he took this as a sign and decided that he would go up and talk to this monk.
He waited for him to finish arms and finish eating and then he asked him, he came up
to him and asked him and said, venerable sir please tell me who is your teacher and what
is his teaching and the monk as he was his name said, my teacher is the great Buddha
and the full enlightened Buddha and he said and his, as for his teaching he said, I've
only recently gone forth, I'm a new, I'm new to this, of course the Buddha's teaching
was new so there were, he was one of the seniors actually but he was still, it was true,
he was still relatively new and he said, so he said to be very difficult for me to teach
it in depth and of course this was a reasonable thing to say but it also had a profound
effect on a participant because it made him think wow if this is what a novice acts like
and then I wonder what it would be like to be someone who fully understands the teaching.
So he said please give me whatever you know because obviously you know something so please
give me the essence whether it be a lot, what you survive for great words and long speeches
just give me the essence.
So I said you gave him the essence and it's so, so core that it's, it's very difficult
for us to understand and even more difficult to understand how such a simple teaching
could have the effect that it did have, have one in Patisa.
The verse goes that the verse that he gave that he said to have given to Upatisa wise,
which means, which means, these things which arise based on the cause, the cause of those
dumbers of those things is what the, the tatagata has taught, the tatagata has taught us
the causes of those things.
This is the first part.
Now when he finished this first part which is half of the verse, he, Upatisa became a
soda planet, he realized the truth just from half of it.
Most of us we listen to this and it's incomprehensible, okay, yeah, those dumbers that
arise based on the cause, the Buddha taught, the cause of those dumbers.
It seems so simple and it'd be very difficult, it's, it's near impossible for us to imagine
how it could lead someone to become enlightened and I just assume that most people listening
probably didn't become enlightened just hearing that.
The second part, they sent to Yo Niro Dotha, Iwanwadimasa, and the cessation of those dumbers.
The great summoner, the great sage of the great records, has, this is his teaching.
His teaching is the cessation of those, those dumbers.
So actually, it, what it, what it's, is teaching here is the four noble truths that
he learned from the Buddha and the original teaching and it's, it's so, it's so simple
and maybe the idea is to humble the Buddhism because a sage he may not know that he would
be able to understand it, but it's also so core that Yo Niro Dotha was able to grasp what
is essential and he was able to see that actually this is the, the essential is that
suffering has the cause, that the problem in life or all of our questions can be answered
based on cause and effect, that every problem that I have and this is, you know, based
on our method, based on his meditation, it wasn't based simply on thinking, but when he
looked inside, he was able to see all of the suffering, all of the delusion, all of the
passion, all of the hatred and all of the conceits and problems that we have inside, they
all have to do with cause and effect and when you see cause and effect, you see this leads
to this leads to that, you're able to break it apart, when you look and at the things
you're looking at, you see how they, they react with each other, you're, then, then you're
able to change that, you're able to refine your behavior as a result and more importantly
is the second part where it talks about the cessation, that actually what the Buddha focused
on was the cessation of these dumbness, that actually there is no realization that there
is nothing that is going to bring true peace and happiness, there's no one reality and
so when you give them up, when you let go of them, that's true peace and true happiness.
This is the meaning of it, very difficult for us to understand, but for what you say
was actually quite simple, he had been training for a long time, in fact the story goes
that he had gone throughout India or throughout that part of India and he would go up to
teachers and ask them questions and he would answer all of their questions, but they
couldn't answer his questions and so he was never able to find a teacher, so he was
actually quite keen, he was able to debate with anyone and to best anyone in an argument,
so so he had a very keen mind and that's why he was able to, he was right ready to accept
this and when he tried to find fault with it and tried to examine it in terms of his own
experience, he actually allowed him to open up and to become free.
So immediately he said stop, that's enough, thank you, please tell me where is our teacher,
at this point he had no doubt and Asaji said he's staying in Wailuana, which is Bambugu.
So immediately Upatissa went immediately back to where his friend was staying and his
friend Coleeta saw him coming and knew right away just as Upatissa had known that this
was an enlightened being this is something of special and so he said something is different
with my friend, he said please tell me Upatissa have you found the truth and Upatissa said
yes I found the truth, he said please share it with me and he told him the same verse
and as a result of this teaching Coleeta as well was able to become a Sotapan which is the
first stage of enlightenment.
And so he said let's go, we have to know and meet our teacher now and Upatissa said
but first we should go and see our old teacher in giving the opportunity to hear this
as well at a perspective for his patronage and they they discussed it and Upatissa said
you know I really think that he'll be able to understand it and appreciate it and if
he does appreciate it it will be able to come with us very quickly realize the truth as
we have.
So they went to see Sanjayan and it was not really as they expected they said they explained
the case to him and Sanjay said oh yes go ahead he said well come with us they said
well come with us you can come and learn as well and Sanjay has said my son's how can
you say such a thing look here I am a great teacher I am my own students and I am famous
you want me to go and become a student again something very big and some very big and important
like myself to go like that he said it's such a thing as impossible and and Upatissa said
that's really not the point you know it really does it really matter that these things
really have any meaning what is essential is that we've now found the truth and Sanjay
has said look let me put it to you this way he said are there more are there more wise
people or more foolish people in the world and Upatissa said or Upatissa and Kolkita
or whoever said oh surely the foolish people are far more the one number wise people in
the world are very few and Sanjay has said well then my sons you go and be with the
wise Gautama which is the family name of the Buddha and all of the foolish people will
come to me and I will be very famous and I have a great following just what he said
and Upatissa and Kolkita you know it was like the last try really you know broke their
faith in their teacher entirely and they said you know well in that case well that's
but by the time to part ways he said how can you know this is crazy if that's all
that's important to you and that you should be surrounded by fools instead of be with
a few wise the few wise people then let's go so they left and what happened when they
left interestingly enough is that Sanjay lost all of his followers that he thought
as a result of his teaching something foolish he would gain a lot of followers what
would happen is actually most of them left with Sankholita and as a result he got
very angry and upset and got very sick and it said to have vomited blood as a result
that's what the story says so Upatissa and Kolkita went to see the Buddha and when they
went to see the Buddha they they got in they got ordained by the Buddha and then eventually
they became enlightened it took them longer than actually most other of the Buddha's
disciples for Kolkita it took him seven days for Upatissa it took him 15 days which
you know considering their perfection of mine is quite a long time but the commentary
says this is because of the profound view of their wish in the past that in past legs
they had made wishes to become the Buddha's chief disciples and so as a result they had
to do all their mind was set on that and they had to therefore go through a very profound
understanding of reality before they could come to their final liberation especially
Upatissa it took him longer now their names changed it Upatissa became sorry put his mother
was sorry so they called him sorry put them Mogulana I believe the village of the family
was called Mogulana so they called him Mogulana I moved the family so they became sorry
put them Mogulana they're very famous in Buddhist circles very well known now after they
became enlightened they they happened to be discussing or they went to ask the Buddha to
talk to him and they said you know look memorable sir we were when we came when we wanted
to come see you we told we told our teacher and this is what he said he said that he would
stay with the fools and that therefore be very famous and you can go with with the Buddha
Gautama and be surrounded by just a few of the wise people and therefore be relatively
insignificant and this is when the Buddha gave this verse he said asari sara matim
so the person who so this is what happens a person who who dwells on what is unessential
they'll never come to the truth but for a person who does see what is essential as essential
and what is unessential as an essential such a person does come to the truth it has to
do with right understanding and obviously Sanjay I had a very wrong understanding now
what this verse teaches us it teaches us many things on many levels the first thing that
it teaches us is about our lives and how we often dwell on things that are unessential
the example of Kolkata Nupatissa is a good one they were living a life of luxury and opulence
and great sensual pleasure so they would go to these shows and it said that at the show
they would have so much fun and be entertained until one day they just realized how pointless
it was and most of us have very different a very difficult time coming to this realization
we might have great suffering and depression and sadness at times in our lives but we're
not able to make the connection we still are living in under the misguided belief that
somehow this sensual enjoyment is going to bring us someday or somehow it's going to bring
us true peace and happiness or we believe that some things just wrong with us the fact
that we can't find happiness in this way is means there's something wrong so often we'll
take medication for our depression or so on thinking that somehow as a result we're going
to be happy and enjoy all of these things that aren't really bringing us true peace
and happiness sorry put in Mogulana we're able to see through that and they were able to
see that you know and it's it's a difficult thing to see because there's so much pressure
on from society society says this is good this is right this is the sort of thing that
we're supposed to enjoy so this is something that we should take to heart and to use to
help to change our view and our understanding that actually it's true that we're not
going to find true peace and happiness through these things and if we expect to we're
only going to be disappointed again and again and eventually we meet with the ultimate
disappointment when we have to leave it all behind and realize that we've wasted our whole
lives on things which are unessential and useless as a result will never come to the truth
will die without coming to the truth and this is how people live their lives we waste our
time so it's important that we should remember the Buddha's teaching not to let the
moment pass us by don't waste your time in this life that we have the second the second
thing it has to teach us is in regards to the unessential and in the religious path now
Sanjay I was someone who you could expect had had gained some of this realization and
therefore had left the home life he may have done it for the wrong reasons but it's possible
that he left the home life on to good intentions trying to find the truth the problem is
that in the end he hit upon that which was totally unessential and he got caught up in what
we say are the the locomotives he got caught up in fame and praise and wealth and pleasure
you know all of these things that he got from being a great teacher and as a result he
saw that these were essential and he thought it was essential to have lots of students
and the be famous and that this was the most important even to the point where he thought
was better to be surrounded by foolish people to be surrounded by wise people which he figured
would be a lot fewer and so you find this often or you do find this in religious circles
and in terms of religious teachers that often people get caught up in teaching and put
aside their own practice or that people get caught up in you see many many people who put on
this robe and then got caught up in as I said before got up in fame got caught up in wealth
and we talked about this in the verses when they were at that it's very easy to get caught
up in these things and you see monks even with the best intentions will eventually get caught
up in luxury and so on and then they need this and that and a nice bed and nice good food
and they'll often start to obsess over these things and they won't they won't be able
to see that these are on essential people who worry become a monk and then worry about
their health and worry about their food who worry about their bedding and so on you know
I've been through all that but I think if you practice meditation you should be able
to overcome it and there were times where I said to myself you know I don't have I don't
even have I didn't even have soap or I didn't have laundry powder and these things just
came it just was such a shock to me it was so such a stress that I didn't have these
things what was I going to do and I said well well then I realized well then I just
won't wash and I won't wash my clothes that's the answer and so I was able to let go of
it and realized that that's really unessential all of these things are in essential when
some days we will go hungry and then you think oh no what a terrible thing what am I
going to do then the answer is why you're going to be hungry that's it really doesn't
mean anything maybe you'll get sick maybe you'll die doesn't really mean anything and when
you put your emphasis on these things only I'll be healthy only I'll be have a full stomach
only I'll be alive even when you when this is your your essence you've missed the point
what is the essence according to the Buddha there are five things and and even the third thing
the teachers is that even in regards to what is essential we have to prioritize or we have to be
complete so the Buddha would always give talks on what is the essence what is the core and there are
five candidates there's morality concentration wisdom release or freedom and the awareness or the
knowledge of freedom and we need all five of these some people take up some people are able to put
aside the worldly dominant worldly pursuits and pursue after at least morality and so as a result
they're able to keep morality they don't touch money they don't eat at the wrong they only eat
once a day and they wear robes and so on they even maybe wear rag robes they're able to do all
these these these practices of morality but they become considered as a result and they see that
as the essence and the Buddha said no this is not the essence this isn't the core it's part of the
essence but it's only the first part if you don't go any further you you still haven't found the essence
so next some people develop concentration so they're able to focus their mind and fix their minds
on one object their minds become very quiet and very focused they can even gain magical powers
they were that they himself had a very focused mind and even gain magical powers at some point
he was able to apparently having flight through the air he was able to change his appearance so
people he looked different to people and so on but so obviously this is not the essence and it
leads people like David that did they become considered and therefore what doesn't lead them
people can still become conceited based on these things and if they have conceived inside and
haven't gone further then they become conceited and they'll use them for the wrong purposes
the next one is wisdom no even people who gain some wisdom in inside can become considered if
they don't get to the to the liberation or freedom their mind was there was to become considered
and they'll believe that they're enlightened just from the insights that they've gained
people who practice even inside meditation if they haven't realized the the the perfect truth
if their minds haven't become free they've liberated they haven't realized in bananas
then they will still fall into conceit and they're still wrong understanding and the wrong
beliefs that they are enlightened as a result they will give up the practicing and think that
they've done good enough and will feel content with what they've gained so this is also not
the essence but finally is the fourth and the fifth one and really they come together but they are
separate things the fourth one is release so when a person does become released in free and
enters into the band over the mind there's no longer coming out there's no longer seeing or hearing
or smelling or tasting or feeling thinking the mind goes inside really and for that time doesn't
come out that there is no experience of seeing hearing the spelling tasting thinking and that's
true release and freedom and the mind has just found its center really and it's not it's not wavering
and at that point the mind is free and this is called freedom this is really the essence and
really there's nothing more that needs to be done but the next the the fifth one is just this
the aspect of coming out again and realizing that what it is that you've experienced and to
actually start thinking about it and the realization that you've become free this Buddhist and
this is the greatest benefit the greatest essence once you become freely the life that you live in
and the existence as someone who is free as someone who has seen the truth and as a result doesn't
have the cringing clinging in regards to that which is unessential or in regards to anything that
is a reason all of the arisen does because you've seen the cessation yay dhamma heath upon the
life you've seen all of these arisen things they center yon yon yon yon you've seen their cessation
so this is the essence and the person shouldn't be inclined towards these things this is really
what we're aiming for and Buddhism and this is kind of a teaching for us to see what is
to see what in our lives maybe we are clinging on to that is unessential and to remind us
of what is truly essential and how we should give up what is unessential because it winds up
it leads us to a waste our times and to lose the opportunity that we have in this life
to put into practice those teachings that lead us to realization of the truth and true peace
happiness and freedom from suffering so that's our verses for today thank you for tuning in
and wish you all the best
