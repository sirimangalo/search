1
00:00:00,000 --> 00:00:07,800
Is it a negative behavior to save yourself, that person is going to get the negative effect

2
00:00:07,800 --> 00:00:13,240
of that action back, or to wish I hope this person goes away and leaves me alone, and

3
00:00:13,240 --> 00:00:19,920
can these intentions actually affect the other person's life?

4
00:00:19,920 --> 00:00:29,960
Well, for the first one, I think that depends, because you can objectively say that

5
00:00:29,960 --> 00:00:36,480
it cause an effect is true, so you see somebody doing something you could say that

6
00:00:36,480 --> 00:00:43,880
that's going to have an effect on them, and so it really makes you about the intention

7
00:00:43,880 --> 00:00:45,520
of the mind.

8
00:00:45,520 --> 00:00:52,520
If you're saying this thing as a hope for some sort of karmic revenge or something like

9
00:00:52,520 --> 00:01:01,880
that, or if it's just an acknowledgement of, yeah, there's cause an effect, and something

10
00:01:01,880 --> 00:01:09,560
is going to happen in return for that, and can these intentions actually have an effect

11
00:01:09,560 --> 00:01:12,400
on another person's life?

12
00:01:12,400 --> 00:01:20,280
I think possibly, in a lot of ways, because if you're even just at the simplest level

13
00:01:20,280 --> 00:01:26,200
of you thinking bad about somebody who's being angry at somebody when they encounter you,

14
00:01:26,200 --> 00:01:31,600
you're going to be acting different than if you're happy to see them, so it's going

15
00:01:31,600 --> 00:01:35,960
to have an effect at some level.

16
00:01:35,960 --> 00:01:44,160
Yeah, the other thing that could be going on when you say that person is going to get

17
00:01:44,160 --> 00:01:48,360
the negative effect of that action back, obviously there's the potential for anger to

18
00:01:48,360 --> 00:01:56,280
arise, the vengefulness, hoping that they call it that harm comes through, but it can

19
00:01:56,280 --> 00:02:04,600
also be more insidious as this Buddhist conceit, where you start to think of yourself

20
00:02:04,600 --> 00:02:10,880
as better than that, and they're going to get there on the wrong path kind of thing,

21
00:02:10,880 --> 00:02:20,840
as in their lesser than me, holding yourself up when someone does bad things and you feel

22
00:02:20,840 --> 00:02:25,360
angry about it, you reassure yourself saying, it's okay, I'm better than them kind of

23
00:02:25,360 --> 00:02:27,640
thing, which isn't so good.

24
00:02:27,640 --> 00:02:36,840
The correct way to use that sentence and we do use it is to help us to develop the desire

25
00:02:36,840 --> 00:02:40,000
to help that person.

26
00:02:40,000 --> 00:02:44,680
Because people often use it, I think, to pitying the person and saying, oh, well, they

27
00:02:44,680 --> 00:02:49,640
were the of our pity there so far below us, for example, but there's this quote, never

28
00:02:49,640 --> 00:02:52,280
looked down on someone unless you're helping them up.

29
00:02:52,280 --> 00:02:56,920
It was Martin Luther King, I can't remember him.

30
00:02:56,920 --> 00:03:01,640
The very good quote, so if you're looking down on someone, it should be because you're

31
00:03:01,640 --> 00:03:15,120
helping them up when that's really the important point.

32
00:03:15,120 --> 00:03:19,360
When we say that, we realize that this is where the real problem is.

33
00:03:19,360 --> 00:03:21,560
The problem isn't that they hurt you.

34
00:03:21,560 --> 00:03:26,200
The problem is that they've created trouble for themselves.

35
00:03:26,200 --> 00:03:35,680
They can't create trouble for you unless you continue it, unless you cling to their actions,

36
00:03:35,680 --> 00:03:44,280
cling to your experience of their actions, unless you cling to it, it doesn't hurt.

37
00:03:44,280 --> 00:03:52,520
But it's horrible for them, and reflecting in that way helps you think, I'm sorry, what

38
00:03:52,520 --> 00:03:57,640
can I do to stop you to make you not want to hit me anymore?

39
00:03:57,640 --> 00:04:00,720
Because that's really bad for you.

40
00:04:00,720 --> 00:04:12,080
Like this story of Upalawana, who was raped, wasn't Upalawana, and she says to this guy,

41
00:04:12,080 --> 00:04:22,160
he's holding her, pinning her down, she says, stop, this is going to be bad for you.

42
00:04:22,160 --> 00:04:26,400
This is going to cause harm for you, harm to you, and that's all she said.

43
00:04:26,400 --> 00:04:33,400
You imagine being in that situation, I certainly can't.

44
00:04:33,400 --> 00:04:38,160
And true enough, sure enough, when he left the good deed, the earth couldn't support his

45
00:04:38,160 --> 00:04:42,000
weight, and he fell into the, he sunk into the earth all the way down to hell, they

46
00:04:42,000 --> 00:04:43,000
say.

47
00:04:43,000 --> 00:04:44,000
True or not, I don't know.

48
00:04:44,000 --> 00:04:49,480
That's just a commentary, I think, he could just be he died and went to hell, but it's

49
00:04:49,480 --> 00:04:52,000
quite vivid to think that the earth couldn't support his weight.

50
00:04:52,000 --> 00:04:56,640
There's only supposed to be five people in the history since the time of the Buddha

51
00:04:56,640 --> 00:05:05,440
who have fallen into the earth, who have, the earth wasn't able to support their weight.

52
00:05:05,440 --> 00:05:07,800
He was one of them.

53
00:05:07,800 --> 00:05:16,160
So she was clear in her mind, what was going on here, and what was the real problem?

54
00:05:16,160 --> 00:05:20,400
She's quite incredible.

55
00:05:20,400 --> 00:05:24,880
I wish that I hope this person goes away and leaves me alone.

56
00:05:24,880 --> 00:05:25,880
That's a problem.

57
00:05:25,880 --> 00:05:30,040
This is something that we have to remind ourselves time and time again.

58
00:05:30,040 --> 00:05:35,920
You're in the situation, you're with the person, and the way to find peace and happiness

59
00:05:35,920 --> 00:05:38,200
is not to get rid of them.

60
00:05:38,200 --> 00:05:40,240
It's not.

61
00:05:40,240 --> 00:05:50,080
Sorry, it's not to be averse to their companionship, they'll come a point where you should

62
00:05:50,080 --> 00:05:58,120
say to them, listen, I've got to go, or listen, I really should be doing meditation,

63
00:05:58,120 --> 00:06:01,280
but you have to know what time that is.

64
00:06:01,280 --> 00:06:06,080
And it's not because of your aversion, it's because you know that this is not leading anywhere.

65
00:06:06,080 --> 00:06:09,600
This has no benefit, there's no benefit in staying.

66
00:06:09,600 --> 00:06:15,440
But the way to find peace and happiness is to meditate while they're there.

67
00:06:15,440 --> 00:06:20,080
If someone's really just rattling on, just sit there and say hearing, hearing, and look

68
00:06:20,080 --> 00:06:25,000
at your own emotions disliking, disliking and taking us a chance to practice meditation

69
00:06:25,000 --> 00:06:30,480
as though you were alone in your room sitting on a cushion.

70
00:06:30,480 --> 00:06:34,680
Really great, wonderful thing, it turns a horrible situation where you have to sit and

71
00:06:34,680 --> 00:06:40,080
listen to these people talk and talk and talk into a wonderful experience of meditation

72
00:06:40,080 --> 00:06:41,080
and enlightenment.

73
00:06:41,080 --> 00:06:45,360
Because this is part of reality, and it's a very important part of our reality.

74
00:06:45,360 --> 00:06:53,600
Our interactions with others and our reception of other people without getting involved

75
00:06:53,600 --> 00:07:03,200
in their defilements, so it's a real good chance for us to learn about how our mind works.

76
00:07:03,200 --> 00:07:09,640
So yeah, and you'll only feel upset, even if you get rid of them, you won't feel better.

77
00:07:09,640 --> 00:07:17,160
You might feel good about yourself for a second, but you've lost the person, but you've

78
00:07:17,160 --> 00:07:21,720
kept your aversion, you've kept your dislike of them, or you've kept your preference to

79
00:07:21,720 --> 00:07:28,280
be alone, for example, your preference to be quiet.

80
00:07:28,280 --> 00:07:33,160
You've kept that preference, and so then when there's more noise, you feel upset at it.

81
00:07:33,160 --> 00:07:38,160
So the only exception is when you know that the time is right, and so you've done your

82
00:07:38,160 --> 00:07:43,240
meditation hearing, hearing you, and then they stopped, and you can really see that this

83
00:07:43,240 --> 00:07:46,720
is going no further, and they really don't have anything more to tell you.

84
00:07:46,720 --> 00:07:50,160
They're just talking because you're not leaving.

85
00:07:50,160 --> 00:07:55,120
Then you tell them, you say, look, I really think I should go.

86
00:07:55,120 --> 00:07:56,120
But you'll feel that.

87
00:07:56,120 --> 00:07:57,520
You'll feel that it's now appropriate.

88
00:07:57,520 --> 00:08:00,640
It won't be like, they're sitting there, and they've got lots and lots of things to

89
00:08:00,640 --> 00:08:04,640
tell you or something, and they're trying to tell you, and you say, I'm sorry, excuse

90
00:08:04,640 --> 00:08:08,880
me, I have to go.

91
00:08:08,880 --> 00:08:13,000
The point being not to do it out of a version, that when you sit down and meditate, you'll

92
00:08:13,000 --> 00:08:17,960
know the right time, and it'll just come to you and you'll say, now, I'm sorry, I have

93
00:08:17,960 --> 00:08:24,320
to go, and then just get up and go, but it has nothing to do with, I hope this person

94
00:08:24,320 --> 00:08:30,560
goes away and leaves me alone, that's negative, for sure, negative.

95
00:08:30,560 --> 00:08:34,600
As to whether they can actually affect the other person's life, magazine already answered

96
00:08:34,600 --> 00:08:43,600
it in a very, very clear way, but I think there's a, the subject, or the mental activity

97
00:08:43,600 --> 00:08:51,840
behind your question, is as to whether it has some psychological or telepathic effect on

98
00:08:51,840 --> 00:08:52,840
the other person's life.

99
00:08:52,840 --> 00:09:02,840
I've heard that actually it can, I've heard people say that they are able to wish bad

100
00:09:02,840 --> 00:09:04,480
things on others.

101
00:09:04,480 --> 00:09:06,600
Some people have this power.

102
00:09:06,600 --> 00:09:10,800
I wouldn't say there's anything in Buddhism that says that that's not possible.

103
00:09:10,800 --> 00:09:16,680
It certainly seems to be, it could be part of this editana, but for me, when someone develops

104
00:09:16,680 --> 00:09:23,560
this, then their intentions are very, become more in line with reality, so they make

105
00:09:23,560 --> 00:09:38,680
this intention and it actually comes true, then kind of thing.

