1
00:00:00,000 --> 00:00:26,120
Good evening, everyone, we're broadcasting live on November 22nd, so I was talking today

2
00:00:26,120 --> 00:00:54,220
with one of the meditators, we're talking about Sasha of course, we're talking about

3
00:00:54,220 --> 00:01:06,280
the damepanda, and he's making a case for how useful it is and how important it is,

4
00:01:06,280 --> 00:01:14,000
beneficial it is, and how I keep dissing it, and how useless it is, how much my other videos

5
00:01:14,000 --> 00:01:17,640
are so much better, and you should all go to sit at my other mall, go watch my other videos

6
00:01:17,640 --> 00:01:31,320
because there's so much more useful to be challenged at, and understand, but I argued back

7
00:01:31,320 --> 00:01:42,880
that his idea was that it's good encouragement to people who are meditating, but my point

8
00:01:42,880 --> 00:01:47,040
was, well, what if that's all people ever get, when new people come to my channel, that's

9
00:01:47,040 --> 00:01:50,800
all they see now, because those are the latest videos, and if they don't ever go back

10
00:01:50,800 --> 00:01:57,200
and watch my old videos, some find that I've got the things I've been teaching, or maybe

11
00:01:57,200 --> 00:02:00,240
they look at them and say, oh, that's the old stuff, let's look at what he's doing now,

12
00:02:00,240 --> 00:02:05,320
which I think that's reason, and this is all they get is the damepanda story, it's not really

13
00:02:05,320 --> 00:02:06,320
enough.

14
00:02:06,320 --> 00:02:09,320
I have the suggestion.

15
00:02:09,320 --> 00:02:17,640
Do you, you started Buddhism 101? Yeah, yeah, that's actually what I just thought today

16
00:02:17,640 --> 00:02:22,160
that I better finish, what I should really do is continue Buddhism 101, because I stopped

17
00:02:22,160 --> 00:02:23,160
at an alert.

18
00:02:23,160 --> 00:02:30,040
Well, maybe I actually got the Buddhism, maybe two damepada and one Buddhism 101 videos

19
00:02:30,040 --> 00:02:37,280
in a week? Yeah, and Buddhism 101 can be quite extended, because there's lots of good

20
00:02:37,280 --> 00:02:45,240
one-on-one topics, but that may have to wait until January, maybe it doesn't have to,

21
00:02:45,240 --> 00:02:52,120
but maybe I will wait until January, because I can put more attention on it then, just

22
00:02:52,120 --> 00:02:59,040
quit school, and really focus on teaching, I mean, this week I have to write an essay about

23
00:02:59,040 --> 00:03:05,680
John Locke and free will, and Locke is pretty good, I mean, as far as being able to understand

24
00:03:05,680 --> 00:03:09,840
them, but the people, I have to write about the people who write about him, and the people

25
00:03:09,840 --> 00:03:14,400
who write about him are really hard to understand, it's like they try to out intellectualize

26
00:03:14,400 --> 00:03:21,240
him or something, so Locke, it's, you know, you have to, it's a bit challenging to

27
00:03:21,240 --> 00:03:28,240
be, and it's like, they feel like they have to be more challenging to read, you know,

28
00:03:28,240 --> 00:03:42,880
it's a school, I was working on that last night, starting my essay, the next weekend,

29
00:03:42,880 --> 00:03:50,600
and then exams, then after exams I'm flying to Florida, and then after Florida I think

30
00:03:50,600 --> 00:03:59,000
that's in our school for me, just not worth it, not worth the change, not with all this

31
00:03:59,000 --> 00:04:08,000
new work, new teaching, and then we can add more hours to the appointments, add some

32
00:04:08,000 --> 00:04:15,160
more slots, maybe complicated, because then I had to figure the only issue with the slots

33
00:04:15,160 --> 00:04:19,840
right now is remembering who has told me they can't make it on a week, so that if they

34
00:04:19,840 --> 00:04:25,680
haven't told me, then I cancel them, and then we don't, and then they lose their slot,

35
00:04:25,680 --> 00:04:31,920
but if they haven't told me, then I keep them, and I'm afraid to cancel because I can't

36
00:04:31,920 --> 00:04:36,720
remember if they told me or not, and sometimes it seems like there's technical difficulties

37
00:04:36,720 --> 00:04:41,480
and a couple of people said they didn't get the button to pop up, but the button hasn't

38
00:04:41,480 --> 00:04:47,280
been a problem lately, I don't think that people may have done that, yeah, Samantha just had

39
00:04:47,280 --> 00:04:53,280
a problem with it this past week, and she was waiting, and it didn't pop up for her.

40
00:04:53,280 --> 00:04:59,280
Well, didn't know that.

41
00:04:59,280 --> 00:05:06,320
Well, the bigger problem has been people calling me, like just now someone called me,

42
00:05:06,320 --> 00:05:10,280
I didn't get a call, I don't think.

43
00:05:10,280 --> 00:05:14,920
Actually it was, and may have been out of the room when they called, but I didn't know

44
00:05:14,920 --> 00:05:27,280
if I made it someone was trying to call me, or had tried to call me, it's really erratic.

45
00:05:27,280 --> 00:05:34,120
That's happened quite a few times.

46
00:05:34,120 --> 00:05:43,240
Nice to know that the computers are reaffirming the three characteristics, the computers

47
00:05:43,240 --> 00:05:48,720
always stable, then we think, oh yes, what is this nonsense about impermanence suffering

48
00:05:48,720 --> 00:05:49,720
and nonsense?

49
00:05:49,720 --> 00:05:53,960
Here we have something that's permanent, that's stable, satisfying, and controllable.

50
00:05:53,960 --> 00:05:59,640
Well, we know that's not the case, so good for us, well done Google.

51
00:05:59,640 --> 00:06:07,040
That's right, teaching us Buddhism, doing a good job.

52
00:06:07,040 --> 00:06:11,400
One day if someone is due for an appointment, but the button doesn't pop up, can they

53
00:06:11,400 --> 00:06:15,760
just create a, just invite you to already do a hangout, would it be any different?

54
00:06:15,760 --> 00:06:20,640
No, that's exactly what it does, the button just creates a hangout and tells you to invite

55
00:06:20,640 --> 00:06:23,880
me, and then you click on invite, you have to actually still invite me, just gives

56
00:06:23,880 --> 00:06:28,840
you my email address, just click invite.

57
00:06:28,840 --> 00:06:33,960
So rather than miss an appointment someone could just start up a hangout and invite you,

58
00:06:33,960 --> 00:06:37,480
it's good to know.

59
00:06:37,480 --> 00:06:46,720
We had two more people, we had probably four more slots every day, we'll see if I can handle

60
00:06:46,720 --> 00:06:47,720
that.

61
00:06:47,720 --> 00:06:51,480
Should be able to, maybe not, you know, because some days there are other things going

62
00:06:51,480 --> 00:06:52,480
on.

63
00:06:52,480 --> 00:06:56,720
It's interesting, can I really add more slots every day, maybe Monday to Friday, maybe

64
00:06:56,720 --> 00:07:00,600
I'll kick the weekends or the more open.

65
00:07:00,600 --> 00:07:09,120
And could you add another tab to the meditation website here, sort of a message board

66
00:07:09,120 --> 00:07:16,920
for, you know, you're not sure when people tell you that they can't make it.

67
00:07:16,920 --> 00:07:22,760
Like for example, you know, similar to the chat, but if somebody put their message in

68
00:07:22,760 --> 00:07:28,440
chat, it would get lost, but if you had a dedicated panel for a message that's strictly

69
00:07:28,440 --> 00:07:34,280
related to the meditation meetings, that might help to keep things organized, I'm just

70
00:07:34,280 --> 00:08:04,120
creating more work for you, but it might help to keep things more organized.

71
00:08:04,120 --> 00:08:16,800
Maybe I can add a notes section to everyone's profile that they can add notes that I

72
00:08:16,800 --> 00:08:23,720
could see, so like when I click on their profile and I'd say that I'm not going to be here

73
00:08:23,720 --> 00:08:28,480
this week this day, I'll tell them to put the note on their profile because that's where

74
00:08:28,480 --> 00:08:38,360
I go right away to see this person calling me, especially what the holiday is coming up

75
00:08:38,360 --> 00:08:58,360
people may be away.

76
00:08:58,360 --> 00:09:12,960
What do we have for Dhamma today, we could quote now, it's not go there, we have any questions.

77
00:09:12,960 --> 00:09:13,960
We have questions.

78
00:09:13,960 --> 00:09:17,760
All right, let's go there.

79
00:09:17,760 --> 00:09:22,720
This is, this is a nice, cute question, venerable, sir, is it better to have a shorter

80
00:09:22,720 --> 00:09:27,120
teaching video to appease a larger group of viewers or better to have a longer teaching

81
00:09:27,120 --> 00:09:31,840
video for the fewer viewers that care about each side story and settle to be and carefully

82
00:09:31,840 --> 00:09:44,840
consider your every movement law conveying the sat-down Dhamma, thank you.

83
00:09:44,840 --> 00:09:54,440
Senator, question, I think it's definitely related to the comment that you relate that

84
00:09:54,440 --> 00:10:06,920
someone said you're your last time I thought it was too long, yeah, we say I'm Thai

85
00:10:06,920 --> 00:10:15,400
nana jitang, it's actually appalling, but in Thailand they say nana jitang means, mind

86
00:10:15,400 --> 00:10:27,000
is, I guess it means, mind is manifold or is various, mind is very, mind varies, like

87
00:10:27,000 --> 00:10:32,480
to each their own basically, everyone has a different opinion, there are many, many different

88
00:10:32,480 --> 00:10:34,520
opinions.

89
00:10:34,520 --> 00:10:53,120
Nana jitang, we've heard, I think they're corrupting the polyaxo, banana jitang, probably.

90
00:10:53,120 --> 00:11:18,040
Next question, yeah, why did I answer that one already, I don't think so, what was the

91
00:11:18,040 --> 00:11:24,440
question is, is it better to have shorter teachings that appear that appeal to a larger

92
00:11:24,440 --> 00:11:30,760
group or longer teachings that appeal to your more dedicated followers?

93
00:11:30,760 --> 00:11:43,520
Well, it's interesting talking last night, I brought up the idea that a doctor isn't

94
00:11:43,520 --> 00:11:48,880
worried about people who are already on the mend, they only concern themselves with people

95
00:11:48,880 --> 00:11:59,760
who will die without their care, so there is that, some extent we need to reach those people

96
00:11:59,760 --> 00:12:12,040
who are not easily impressed, reach those people who are not already hooked, but that's

97
00:12:12,040 --> 00:12:16,880
not entirely fair, because to some extent we have to help those people who are actually

98
00:12:16,880 --> 00:12:20,760
going to take the medicine, you know, don't want to pander to people who aren't even

99
00:12:20,760 --> 00:12:26,000
going to take the medicine, by wasting all your time teaching people who aren't even

100
00:12:26,000 --> 00:12:34,040
going to practice, that's a good point.

101
00:12:34,040 --> 00:12:36,240
Thank you, Monte.

102
00:12:36,240 --> 00:12:40,800
I'm trying to find a good way to respond to cravings when not meditating, right now it

103
00:12:40,800 --> 00:12:45,720
goes like this, there is a site or a thought that starts a craving, the mind locks onto

104
00:12:45,720 --> 00:12:51,080
that site or thought, there is an unpleasant feeling, I remember to be mindful, there

105
00:12:51,080 --> 00:12:57,040
is an awareness that there is an aversion to the craving, I say disliking, disliking or

106
00:12:57,040 --> 00:12:59,880
averting, averting or something.

107
00:12:59,880 --> 00:13:04,680
This goes on for a short as 30 seconds or sometimes for 10 minutes or longer, rather than

108
00:13:04,680 --> 00:13:09,400
coming to an end, usually another site of thought comes along and the mind follows going

109
00:13:09,400 --> 00:13:11,360
in a different direction.

110
00:13:11,360 --> 00:13:22,360
You have often talked about a middle life or responding to cravings, could you elaborate?

111
00:13:22,360 --> 00:13:38,280
I don't quite understand the question is, like as though you have as though something's

112
00:13:38,280 --> 00:13:46,440
wrong with what you're describing, I mean something is wrong, but you know what's wrong,

113
00:13:46,440 --> 00:13:51,400
what's wrong is reality is wrong, it's impermanence suffering in itself and that's what

114
00:13:51,400 --> 00:14:00,200
you experience, the chaos, the unpleasantness, the unwieldiness, the not how I wanted

115
00:14:00,200 --> 00:14:11,320
wanted, wantededness, is it how you want it, is it how you'd like it, no, that's it,

116
00:14:11,320 --> 00:14:12,840
that's reality, why?

117
00:14:12,840 --> 00:14:21,440
Because that will allow you to let go, that will lead you to want to let go, not want

118
00:14:21,440 --> 00:14:34,840
to cling anymore, you're doing fine, but I would recommend going back as often as possible

119
00:14:34,840 --> 00:14:39,520
to the rising and falling, it keeps you grounded, don't jump from one thing to another

120
00:14:39,520 --> 00:14:43,080
unless it's your immediately pulled, if you're not pulled, then go back to the rising

121
00:14:43,080 --> 00:14:48,880
before, and then acknowledge the next thing, but try to always come back to the rising

122
00:14:48,880 --> 00:14:55,040
and falling when you can, after noting whatever you have noted, whatever you have,

123
00:14:55,040 --> 00:14:57,600
I'll explain it.

124
00:14:57,600 --> 00:15:05,680
So the last part of the question was, you have talked about a middle way for responding

125
00:15:05,680 --> 00:15:11,680
to cravings, that's a little bit like the question was, you know if you have to do something

126
00:15:11,680 --> 00:15:18,240
should you try to do with the wholesome version of it, hmm, last night it was a similar

127
00:15:18,240 --> 00:15:22,120
or maybe the night before, there was a similar question about if you have to give into

128
00:15:22,120 --> 00:15:28,040
something should you give into the most wholesome version of it or something similar

129
00:15:28,040 --> 00:15:29,040
like that.

130
00:15:29,040 --> 00:15:35,760
They were saying, remember to be mindful, they know everything is fine, that is the middle

131
00:15:35,760 --> 00:15:39,520
way, I mean, that is the middle way I talk about.

132
00:15:39,520 --> 00:15:49,880
So I don't think, I think there may be, yeah, it may be taking what I said last night and

133
00:15:49,880 --> 00:15:57,160
someone making more of it and no point is to be mindful, if you can, if you find yourself

134
00:15:57,160 --> 00:16:05,840
giving in, well then you give in, it's not the middle way, yeah, but happens, pick yourself

135
00:16:05,840 --> 00:16:20,120
up and try again, or learning, we're trying to learn, trying to study, trying to understand.

136
00:16:20,120 --> 00:16:24,480
Dear Ponte, I have been meditating for a while and I feel like it's making me unenthusiastic

137
00:16:24,480 --> 00:16:30,000
about things I formally like to do, such as working out, watching television, etc.

138
00:16:30,000 --> 00:16:34,520
I want to be clear, I'm not depressed, but I feel fine sitting around all day doing nothing.

139
00:16:34,520 --> 00:16:35,520
Is this natural?

140
00:16:35,520 --> 00:16:36,520
Thanks.

141
00:16:36,520 --> 00:16:53,040
No rape is natural, murder is natural, theft is natural, excruciating suffering is natural, terrible

142
00:16:53,040 --> 00:17:01,640
evil is natural, craving is natural, addiction is natural, so what do you mean by this

143
00:17:01,640 --> 00:17:15,280
question, is it natural, is there answering a question with the question and some elaboration?

144
00:17:15,280 --> 00:17:23,800
Sometimes you have to, when someone asks you a question, you have to, it apart, can't

145
00:17:23,800 --> 00:17:34,840
answer directly, ask, and sometimes you have to ask a question, a counter question, this

146
00:17:34,840 --> 00:17:46,080
is one of those cases.

147
00:17:46,080 --> 00:17:51,000
And to follow up on the earlier question about cravings, I can't follow the craving to the

148
00:17:51,000 --> 00:17:55,120
end, is that a problem?

149
00:17:55,120 --> 00:17:58,160
The end of what?

150
00:17:58,160 --> 00:18:00,160
The end of the craving?

151
00:18:00,160 --> 00:18:05,960
Can't stick it up until the craving goes away?

152
00:18:05,960 --> 00:18:08,840
The craving lasts actually ten minutes?

153
00:18:08,840 --> 00:18:10,840
Apparently, yes.

154
00:18:10,840 --> 00:18:13,240
They don't think so.

155
00:18:13,240 --> 00:18:16,000
You say to yourself, one thing, one thing.

156
00:18:16,000 --> 00:18:20,640
It should go away for a moment, and then you come back to the rising and falling.

157
00:18:20,640 --> 00:18:23,000
If it comes back, then you go back to it.

158
00:18:23,000 --> 00:18:27,680
When it goes away, come back to the rising form.

159
00:18:27,680 --> 00:18:30,720
Nothing lasts for ten minutes.

160
00:18:30,720 --> 00:18:41,680
Maybe it does, and just say, no stick it out for a while, and then ignore it after a while.

161
00:18:41,680 --> 00:18:48,640
One thing, how can I make progress if I can't meet with you to do a course?

162
00:18:48,640 --> 00:18:52,920
Is there something more I can read?

163
00:18:52,920 --> 00:19:09,160
Well, if you are meditating, which looks like you are, good green question mark.

164
00:19:09,160 --> 00:19:12,640
You could read the second book on how to meditate.

165
00:19:12,640 --> 00:19:20,920
It's not quite finished, but mostly finished, for most intense and purposes, it's finished.

166
00:19:20,920 --> 00:19:29,160
That would be a good thing, maybe, to read.

167
00:19:29,160 --> 00:19:41,200
We are all filled up on the meetings, right?

168
00:19:41,200 --> 00:19:43,200
It's the one empty spot.

169
00:19:43,200 --> 00:19:52,680
Tuesday at 2330 UTC time, which is, it looks like 6.30pm, eastern time.

170
00:19:52,680 --> 00:20:06,720
And well, it's been rather before it's gone.

171
00:20:06,720 --> 00:20:31,760
Looking for the link for the how to meditate part 2.

172
00:20:36,720 --> 00:20:44,960
And with that, we are all caught up on questions.

173
00:20:44,960 --> 00:20:49,120
Tomorrow we have a metamob.

174
00:20:49,120 --> 00:20:53,520
We are going to try a metamob, or I am going to, maybe I will be there alone.

175
00:20:53,520 --> 00:20:59,720
We are going to do meditation in public, and see how it goes.

176
00:20:59,720 --> 00:21:02,720
Do you have any signs?

177
00:21:02,720 --> 00:21:03,720
No.

178
00:21:03,720 --> 00:21:10,960
I put a little sandwich board out there to explain what you are doing.

179
00:21:10,960 --> 00:21:14,000
Put a little hat.

180
00:21:14,000 --> 00:21:15,000
No.

181
00:21:15,000 --> 00:21:18,000
No signs.

182
00:21:18,000 --> 00:21:26,160
No, that's the whole thing about a meditate, about a flash wand is, you don't really even

183
00:21:26,160 --> 00:21:47,680
tell people what you are doing, but they just figured out.

184
00:21:47,680 --> 00:21:55,720
So that's all for tonight, thank you all for practicing with us, and for tuning in.

185
00:21:55,720 --> 00:21:56,720
Thanks for joining me.

186
00:21:56,720 --> 00:21:57,720
Thank you.

187
00:21:57,720 --> 00:22:23,720
Have a good night.

