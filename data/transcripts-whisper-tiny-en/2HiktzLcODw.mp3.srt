1
00:00:00,000 --> 00:00:11,600
Hi, I'm just making a video today to give update on the situation with our DVD on how to meditate or the

2
00:00:11,600 --> 00:00:19,400
double DVD set on how and why to meditate. Up until recently I had to make it clear to people that we were

3
00:00:19,400 --> 00:00:25,400
trying to recoup some of the costs that it took to make the DVD which of course had nothing really to do

4
00:00:25,400 --> 00:00:37,800
with me and it was basically a way of recovering the costs incurred by the producers of the video. As it

5
00:00:37,800 --> 00:00:48,500
turns out the producers of the video have expressed their willingness to do away with any fee and any reimbursement

6
00:00:48,500 --> 00:00:54,600
that they might have gotten in the process of producing the video so effectively we can offer the video for

7
00:00:54,600 --> 00:01:01,600
free and as I said before the money has nothing to do with me I myself don't handle money but I felt that it

8
00:01:01,600 --> 00:01:10,360
was necessary to let that be known and now I'm more than happy to say that our videos are now totally

9
00:01:10,360 --> 00:01:17,920
free. We still have the cost of shipping the videos of course and that cost between two and five

10
00:01:17,920 --> 00:01:25,320
dollars again this is nothing to do with me but it has to be paid for somewhere so often my students or friends

11
00:01:25,320 --> 00:01:32,420
or people who request the videos will offer donations which of course you're welcome to do but again let

12
00:01:32,420 --> 00:01:39,020
me make it clear that the videos are free we're fully willing and able to ship the video to anywhere in

13
00:01:39,020 --> 00:01:46,720
the world. It might take a little while to get to you but I should think that you know free is free so

14
00:01:46,720 --> 00:01:54,160
please don't be shy and do contact us and let us know if you'd like one of the DVD sets here it is this is what it

15
00:01:54,160 --> 00:02:02,960
looks like fully professionally done professionally produced professionally made so thanks

16
00:02:02,960 --> 00:02:09,800
everyone who's given their their positive feedback and thank you just for requesting the videos it makes

17
00:02:09,800 --> 00:02:16,120
us feel good to be able to spread the meditation teachings in this way so thanks again for

18
00:02:16,120 --> 00:02:21,840
tuning in and hope to hear from you all in the future thank you have a good day

