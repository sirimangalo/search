1
00:00:00,000 --> 00:00:26,000
Welcome to our evening dhamma tonight. We're talking about right concentration, right concentration and there's been some talk of how the word concentration isn't probably the right translation is probably the best word to use.

2
00:00:26,000 --> 00:00:43,000
I don't think it's that bad. I think obviously when we talk about samadhi, we're talking about it in contrast or in complement to effort.

3
00:00:43,000 --> 00:00:49,000
So if you have lots of effort but no concentration you get distracted.

4
00:00:49,000 --> 00:00:56,000
If you have lots of concentration but not enough effort you fall asleep.

5
00:00:56,000 --> 00:01:03,000
I don't think it's I think it's pretty clear what we're talking about.

6
00:01:03,000 --> 00:01:11,000
And to that extent concentration isn't one of those things that you can't get enough of.

7
00:01:11,000 --> 00:01:22,000
The only mindfulness really works in that way that you want to constantly be developing it.

8
00:01:22,000 --> 00:01:33,000
And so there's quite a bit of debate over the role of concentration and the nature of right concentration.

9
00:01:33,000 --> 00:01:44,000
Something that I could go on about for quite a while and I have talked about something that is hotly debated.

10
00:01:44,000 --> 00:01:55,000
But the gist of it is is that the Buddha talked often about specific types of concentration which he called jhanas.

11
00:01:55,000 --> 00:02:07,000
And jhana means something like meditation. The word itself is used in several different ways.

12
00:02:07,000 --> 00:02:18,000
It's used to describe when the Bodhisattva stopped breathing. He practiced the jhana of not breathing.

13
00:02:18,000 --> 00:02:25,000
He just means he did some sort of meditation probably isn't the right word but we use it in that sense.

14
00:02:25,000 --> 00:02:31,000
It was a development or a jhana.

15
00:02:31,000 --> 00:02:40,000
The entered into this fixed state of he fixed himself on.

16
00:02:40,000 --> 00:02:49,000
But it was just when he wanted the monks to meditate, he would say jhata bikhoi.

17
00:02:49,000 --> 00:02:56,000
Jhata is I think the same root as jhana is the noun.

18
00:02:56,000 --> 00:03:11,000
Jhata is the verb jhata meditate. Focus your attention.

19
00:03:11,000 --> 00:03:22,000
But the Buddha talked about four jhanas and there are eight or nine jhanas in total but most often he talked about four of them.

20
00:03:22,000 --> 00:03:33,000
And so there's a lot of debate over what exactly these mean and I think with good reason because it's not entirely clear or entirely certain.

21
00:03:33,000 --> 00:03:43,000
I mean there's room clearly for interpretation and there's the rub because people interpret them differently.

22
00:03:43,000 --> 00:03:59,000
But briefly the jhana's involve states of wholesome concentration where the mind is removed from worldly affairs.

23
00:03:59,000 --> 00:04:08,000
There's some sense that in certain types of jhana, you know it personally argue that there are many ways of talking about jhana.

24
00:04:08,000 --> 00:04:13,000
In certain types of jhana, one doesn't even hear anything or see anything.

25
00:04:13,000 --> 00:04:19,000
It's totally oblivious to the world around.

26
00:04:19,000 --> 00:04:28,000
One is so focused on a single object and the rest of the world disappears.

27
00:04:28,000 --> 00:04:31,000
So that's really a trance state.

28
00:04:31,000 --> 00:04:38,000
We talk about the jhana as we often refer to these trance-like states.

29
00:04:38,000 --> 00:04:42,000
And why there are four is because they become increasingly subtle.

30
00:04:42,000 --> 00:04:51,000
So the first jhana one is observing the object.

31
00:04:51,000 --> 00:05:02,000
One's mind is intent upon perceiving the object again and again and again and contemplating it.

32
00:05:02,000 --> 00:05:10,000
So there's this sense of approaching the object and kind of sticking with it.

33
00:05:10,000 --> 00:05:12,000
That's the first jhana.

34
00:05:12,000 --> 00:05:15,000
It's also accompanied by rapture.

35
00:05:15,000 --> 00:05:22,000
So there's a sense of ecstasy involved, like excitement in a sense.

36
00:05:22,000 --> 00:05:27,000
Yes, this charge, this energy, if you will.

37
00:05:27,000 --> 00:05:31,000
There's happiness and there's single-pointedness.

38
00:05:31,000 --> 00:05:36,000
So the mind is continuously focused on the single object.

39
00:05:36,000 --> 00:05:37,000
That's the first jhana.

40
00:05:37,000 --> 00:05:43,000
In the second jhana, there's no more thinking about the object in the sense of sending the mind out to the object.

41
00:05:43,000 --> 00:05:46,000
The mind is stuck on the object.

42
00:05:46,000 --> 00:05:53,000
And it's so constant that there's no need for application or contemplation.

43
00:05:53,000 --> 00:05:56,000
The mind is simply with the object.

44
00:05:56,000 --> 00:05:58,000
One with the object, if you will.

45
00:05:58,000 --> 00:05:59,000
It's the second jhana.

46
00:05:59,000 --> 00:06:01,000
So the first two factors disappear.

47
00:06:01,000 --> 00:06:07,000
We talk and we talk and we jhara.

48
00:06:07,000 --> 00:06:11,000
But there's still rapture, there's still happiness and there's still single-pointedness.

49
00:06:11,000 --> 00:06:14,000
In the third jhana, rapture disappears.

50
00:06:14,000 --> 00:06:16,000
So there's not this excitement.

51
00:06:16,000 --> 00:06:23,000
The mind becomes more refined and it's just fixed and focused on the object.

52
00:06:23,000 --> 00:06:25,000
That's the third jhana.

53
00:06:25,000 --> 00:06:31,000
In the fourth jhana, but there's still, in the third jhana, there's still happiness and single-pointedness.

54
00:06:31,000 --> 00:06:36,000
And in the fifth jhana, happiness disappears.

55
00:06:36,000 --> 00:06:38,000
But it doesn't mean there's no feeling.

56
00:06:38,000 --> 00:06:41,000
Happiness is replaced by equanimity.

57
00:06:41,000 --> 00:06:52,000
So there's still these two factors of opaica and equanimity and one-pointedness.

58
00:06:52,000 --> 00:06:57,000
So the idea is that through practicing focused meditation,

59
00:06:57,000 --> 00:07:02,000
what we call summit meditation, generally, you enter into more and more refined states.

60
00:07:02,000 --> 00:07:05,000
And it's usually based on a conceptual object.

61
00:07:05,000 --> 00:07:11,000
And this is why we kind of limit our interest in summit to meditation.

62
00:07:11,000 --> 00:07:13,000
Not because it's not helpful.

63
00:07:13,000 --> 00:07:14,000
It's actually quite powerful.

64
00:07:14,000 --> 00:07:19,000
And the Buddha talked about how this type of meditation can lead to magical powers

65
00:07:19,000 --> 00:07:22,000
because you're very much in control of the mind.

66
00:07:22,000 --> 00:07:24,000
But the key there is control.

67
00:07:24,000 --> 00:07:28,000
It ends up being a bit of a problem because you can get stuck on it.

68
00:07:28,000 --> 00:07:35,000
You can get caught up in these powers.

69
00:07:35,000 --> 00:07:37,000
As Ajahn Chas said, it's based on clinging.

70
00:07:37,000 --> 00:07:40,000
I don't know if I would see a such strong language,

71
00:07:40,000 --> 00:07:44,000
but it's interesting that he would say such a thing when many of his followers

72
00:07:44,000 --> 00:07:51,000
are adamant that it being the path to enlightenment.

73
00:07:51,000 --> 00:07:53,000
So this is one way of practicing.

74
00:07:53,000 --> 00:07:56,000
And it's often referred to as right concentration.

75
00:07:56,000 --> 00:08:01,000
The Buddha certainly, when he talked about right concentration,

76
00:08:01,000 --> 00:08:05,000
he most often talked about these four states of mind.

77
00:08:05,000 --> 00:08:08,000
We'll get back to that in a second.

78
00:08:08,000 --> 00:08:12,000
When we enter into these states in ordinary meditation,

79
00:08:12,000 --> 00:08:15,000
we have to remember again, we talk about the noble path.

80
00:08:15,000 --> 00:08:19,000
We're talking about this moment where your mind is perfect.

81
00:08:19,000 --> 00:08:23,000
That moment can't come through summit to meditation.

82
00:08:23,000 --> 00:08:26,000
And summit to meditation isn't that moment.

83
00:08:26,000 --> 00:08:31,000
So to talk about those states of ordinary meditation,

84
00:08:31,000 --> 00:08:36,000
as though they are right concentration, is just clearly wrong.

85
00:08:36,000 --> 00:08:40,000
Those states are a precursor potentially,

86
00:08:40,000 --> 00:08:44,000
like a practice, if you will.

87
00:08:44,000 --> 00:08:48,000
They certainly get the mind more focused and prepare one

88
00:08:48,000 --> 00:08:52,000
for the ultimate focus on the four noble truths and nibana.

89
00:08:52,000 --> 00:08:58,000
But they aren't and shouldn't be mistaken for enlightenment.

90
00:08:58,000 --> 00:09:01,000
So this is one way of approaching the path

91
00:09:01,000 --> 00:09:05,000
where you develop these first and based on them,

92
00:09:05,000 --> 00:09:07,000
then insight comes after.

93
00:09:07,000 --> 00:09:09,320
And when insight and concentration are summit to

94
00:09:09,320 --> 00:09:15,000
and repass in our both perfect, this enlightened.

95
00:09:15,000 --> 00:09:17,000
That's one way.

96
00:09:17,000 --> 00:09:20,000
The way we do it, and really the way a lot of people talk about it,

97
00:09:20,000 --> 00:09:22,000
we argue, we hear these arguments.

98
00:09:22,000 --> 00:09:24,000
But in the end, if you listen to people,

99
00:09:24,000 --> 00:09:25,000
they're mostly talking about the same thing.

100
00:09:25,000 --> 00:09:27,000
They're mostly not talking about that.

101
00:09:27,000 --> 00:09:32,000
Most traditions, especially the ones that argue about this,

102
00:09:32,000 --> 00:09:36,000
don't practice that type of meditation.

103
00:09:36,000 --> 00:09:39,000
They practice meditation where you're developing insight

104
00:09:39,000 --> 00:09:41,000
and tranquility in union.

105
00:09:41,000 --> 00:09:44,000
And Adjan Cha was big on this, just to use him,

106
00:09:44,000 --> 00:09:46,000
not because I follow his teachings,

107
00:09:46,000 --> 00:09:49,000
but because a lot of people on the other side of this

108
00:09:49,000 --> 00:09:51,000
argument do follow his teachings.

109
00:09:51,000 --> 00:09:54,000
Adjan Cha was clear that they should come together.

110
00:09:54,000 --> 00:09:57,000
And he said various things.

111
00:09:57,000 --> 00:10:00,000
I won't use him as he's not my favorite authority.

112
00:10:00,000 --> 00:10:02,000
Sort of authority.

113
00:10:02,000 --> 00:10:05,000
I mean, there were monks that were far more,

114
00:10:05,000 --> 00:10:08,000
I think, versed in the actual texts,

115
00:10:08,000 --> 00:10:12,000
if you want to find an authority of what's in the text.

116
00:10:12,000 --> 00:10:16,000
And I go a little bit more towards that.

117
00:10:16,000 --> 00:10:26,000
But if you focus on, in this way, if you practice in this way,

118
00:10:26,000 --> 00:10:28,000
then they come together.

119
00:10:28,000 --> 00:10:31,000
You never really enter into these trance states

120
00:10:31,000 --> 00:10:33,000
where you're blissful and peaceful.

121
00:10:33,000 --> 00:10:35,000
They're may come, bliss and peace.

122
00:10:35,000 --> 00:10:41,000
But you're cultivating a different type of Jhana.

123
00:10:41,000 --> 00:10:43,000
And the Jhana doesn't come.

124
00:10:43,000 --> 00:10:46,000
Now, when people talk about the Jhana's as being right concentration,

125
00:10:46,000 --> 00:10:48,000
being necessary, totally agree.

126
00:10:48,000 --> 00:10:51,000
And I think it's a problem that we argue about this.

127
00:10:51,000 --> 00:10:55,000
When, in fact, the orthodoxy is quite clear.

128
00:10:55,000 --> 00:10:58,000
When you get to Nibana, at that moment,

129
00:10:58,000 --> 00:11:00,000
there's no question you're in the Jhana.

130
00:11:00,000 --> 00:11:03,000
There's no question you're in one of the four Jhana's.

131
00:11:03,000 --> 00:11:06,000
Usually, in our case, it would be the first Jhana

132
00:11:06,000 --> 00:11:08,000
or it would be considered the first Jhana

133
00:11:08,000 --> 00:11:12,000
because you haven't cultivated the other Jhana's.

134
00:11:12,000 --> 00:11:17,000
But at that moment, it's a Jhana based on Nibana.

135
00:11:17,000 --> 00:11:21,000
And that's right concentration.

136
00:11:21,000 --> 00:11:27,000
So to be clear, the Buddha seems most often to have talked about

137
00:11:27,000 --> 00:11:30,000
is these mundane Jhana.

138
00:11:30,000 --> 00:11:34,000
And most often, that's understood to be a trance state

139
00:11:34,000 --> 00:11:38,000
that involves a lot of magic and exalted states

140
00:11:38,000 --> 00:11:41,000
can even lead you to the Brahma realms.

141
00:11:41,000 --> 00:11:44,000
So the Buddha had this sort of comprehensive practice

142
00:11:44,000 --> 00:11:49,000
that he got his monks to undertake.

143
00:11:49,000 --> 00:11:53,000
And it was quite powerful, but it was also completely mundane.

144
00:11:53,000 --> 00:11:57,000
And these magical powers certainly aren't enlightenment.

145
00:11:57,000 --> 00:12:01,000
And the states of trance also aren't enlightenment.

146
00:12:01,000 --> 00:12:03,000
But they're powerful, and they're supportive.

147
00:12:03,000 --> 00:12:06,000
They're wholesome.

148
00:12:06,000 --> 00:12:10,000
But if you look at the wording of the various Jhana's,

149
00:12:10,000 --> 00:12:13,000
it seems often as well the Buddha was talking about something

150
00:12:13,000 --> 00:12:14,000
a little different.

151
00:12:14,000 --> 00:12:18,000
He was talking about this practice of samata and we pass in it together

152
00:12:18,000 --> 00:12:20,000
because you're mindful.

153
00:12:20,000 --> 00:12:25,000
So it appears in some sense that one can practice Jhana

154
00:12:25,000 --> 00:12:29,000
based on ultimate reality, which makes sense

155
00:12:29,000 --> 00:12:33,000
because of course the word Jhana just means meditation.

156
00:12:33,000 --> 00:12:37,000
So the commentary picks up on this

157
00:12:37,000 --> 00:12:41,000
and talks about two types of Jhana.

158
00:12:41,000 --> 00:12:42,000
Lachanupani Jhana.

159
00:12:42,000 --> 00:12:44,000
Aramanupani Jhana and Lachanupani Jhana.

160
00:12:44,000 --> 00:12:46,000
And these are the words.

161
00:12:46,000 --> 00:12:49,000
And again and again it refers to these two types of Jhana.

162
00:12:49,000 --> 00:12:51,000
So one is Aramana means an object.

163
00:12:51,000 --> 00:12:56,000
So this is samata meditation, where you're meditating

164
00:12:56,000 --> 00:13:00,000
and you're fixed and focused on a single object, a concept.

165
00:13:00,000 --> 00:13:02,000
And because you're focused on that one object

166
00:13:02,000 --> 00:13:05,000
you'll never see impermanence offering a non-self as a result

167
00:13:05,000 --> 00:13:08,000
you'll never see, just by focusing on that object

168
00:13:08,000 --> 00:13:10,000
you'll never see nibanda.

169
00:13:10,000 --> 00:13:13,000
The other one, Lachanupani Jhana is where you're focused

170
00:13:13,000 --> 00:13:17,000
and fixed on the three characteristics.

171
00:13:17,000 --> 00:13:21,000
It's a focus on reality

172
00:13:21,000 --> 00:13:24,000
and the three characteristics seeing them

173
00:13:24,000 --> 00:13:28,000
means it's a state of complete tranquility as well

174
00:13:28,000 --> 00:13:30,000
because seeing the three characteristics

175
00:13:30,000 --> 00:13:32,000
there is no reaction.

176
00:13:32,000 --> 00:13:34,000
You see that the things that we would normally cling

177
00:13:34,000 --> 00:13:38,000
are not worth clinging to are not worth getting upset about.

178
00:13:38,000 --> 00:13:40,000
So it's also right concentration

179
00:13:40,000 --> 00:13:44,000
and it should also be considered Jhana in a sense.

180
00:13:44,000 --> 00:13:47,000
We have a read with the various teachers to say

181
00:13:47,000 --> 00:13:50,000
on the word Jhana, it's nobody agrees anyway.

182
00:13:50,000 --> 00:13:53,000
So I think this is one case where we can be,

183
00:13:53,000 --> 00:13:56,000
where we see people getting often too caught up

184
00:13:56,000 --> 00:13:59,000
in their own definition of a word.

185
00:13:59,000 --> 00:14:01,000
Sometimes it's true that a word has to be defined

186
00:14:01,000 --> 00:14:03,000
in a specific way but it appears the word Jhana

187
00:14:03,000 --> 00:14:05,000
and Jhana should not be.

188
00:14:05,000 --> 00:14:07,000
There are clearly Jhana's that are unwholesome

189
00:14:07,000 --> 00:14:10,000
and Jhana's that are wholesome, that's clear.

190
00:14:10,000 --> 00:14:12,000
But in regards to those that are wholesome

191
00:14:12,000 --> 00:14:17,000
there appears to be a definite amount of leeway

192
00:14:17,000 --> 00:14:21,000
and those people who argue that your way is wrong

193
00:14:21,000 --> 00:14:23,000
and so on and so on.

194
00:14:23,000 --> 00:14:26,000
Are in this case I would say being overly dogmatic

195
00:14:26,000 --> 00:14:28,000
probably on both sides.

196
00:14:28,000 --> 00:14:32,000
So I don't know, hopefully that's cleared up

197
00:14:32,000 --> 00:14:34,000
this whole idea of right concentration

198
00:14:34,000 --> 00:14:36,000
but those of you who aren't aware of this whole debate

199
00:14:36,000 --> 00:14:40,000
the point here is that your mind has to be fixed and focused.

200
00:14:40,000 --> 00:14:43,000
Your mind has to be in a wholesome state.

201
00:14:43,000 --> 00:14:45,000
The real characteristic that I haven't mentioned

202
00:14:45,000 --> 00:14:50,000
of concentration is that it frees you of the 500 sets.

203
00:14:50,000 --> 00:14:53,000
And so just another point as to

204
00:14:53,000 --> 00:14:56,000
some people say you have to enter into the Jhana's

205
00:14:56,000 --> 00:14:58,000
before you begin to practice mindfulness

206
00:14:58,000 --> 00:15:00,000
and I think that's quite absurd because

207
00:15:00,000 --> 00:15:03,000
you have to be mindful of the hindrances.

208
00:15:03,000 --> 00:15:06,000
But it was quite clear that when anger arises

209
00:15:06,000 --> 00:15:09,000
rather than saying okay get rid of the anger

210
00:15:09,000 --> 00:15:11,000
you should be mindful of it.

211
00:15:11,000 --> 00:15:14,000
So clearly there's some leeway here.

212
00:15:14,000 --> 00:15:16,000
Yes you can enter into states where there is no greed

213
00:15:16,000 --> 00:15:20,000
no anger no greed no anger anyway

214
00:15:20,000 --> 00:15:26,000
but it's clearly not the only way.

215
00:15:26,000 --> 00:15:29,000
If there's anger in the mind the way of mindfulness

216
00:15:29,000 --> 00:15:32,000
it's to be mindful of the anger.

217
00:15:32,000 --> 00:15:34,000
And as a result of that

218
00:15:34,000 --> 00:15:36,000
your concentration improves

219
00:15:36,000 --> 00:15:38,000
and eventually you enter into a Jhana

220
00:15:38,000 --> 00:15:40,000
which means you enter into a state

221
00:15:40,000 --> 00:15:42,000
where there's no more anger

222
00:15:42,000 --> 00:15:43,000
where you're experiencing

223
00:15:43,000 --> 00:15:44,000
you're still experiencing things

224
00:15:44,000 --> 00:15:46,000
seeing hearings but

225
00:15:46,000 --> 00:15:49,000
you have equanimity about them.

226
00:15:49,000 --> 00:15:51,000
Masi Saiyala goes into detail about

227
00:15:51,000 --> 00:15:53,000
it's kind of a bit speculative

228
00:15:53,000 --> 00:15:55,000
but the idea of there being

229
00:15:55,000 --> 00:15:59,000
we pass in on Jhana.

230
00:15:59,000 --> 00:16:01,000
And I think all he's saying is that

231
00:16:01,000 --> 00:16:03,000
hey that's not be quite so dogmatic

232
00:16:03,000 --> 00:16:05,000
when there's no reason to and there's certainly

233
00:16:05,000 --> 00:16:08,000
no backing for it or basis for it

234
00:16:08,000 --> 00:16:11,000
and the Buddha's teaching.

235
00:16:11,000 --> 00:16:13,000
Jhana is something that just means

236
00:16:13,000 --> 00:16:15,000
the wholesome concentration of the mind

237
00:16:15,000 --> 00:16:17,000
and at one point the Buddha says that

238
00:16:17,000 --> 00:16:19,000
he says any concentration

239
00:16:19,000 --> 00:16:21,000
that is accompanied by the other seven

240
00:16:21,000 --> 00:16:23,000
path factors is right concentration

241
00:16:23,000 --> 00:16:25,000
which is of course.

242
00:16:25,000 --> 00:16:27,000
That's the key.

243
00:16:27,000 --> 00:16:29,000
You know your focus but do you also have

244
00:16:29,000 --> 00:16:31,000
right mindfulness?

245
00:16:31,000 --> 00:16:33,000
Right in view.

246
00:16:33,000 --> 00:16:35,000
Right saw it.

247
00:16:35,000 --> 00:16:39,000
And there's lots of ways to accomplish

248
00:16:39,000 --> 00:16:43,000
this but ultimately they must

249
00:16:43,000 --> 00:16:45,000
involve mindfulness.

250
00:16:45,000 --> 00:16:47,000
It means awareness of reality

251
00:16:47,000 --> 00:16:49,000
because they must involve the three

252
00:16:49,000 --> 00:16:51,000
characteristics and must involve

253
00:16:51,000 --> 00:16:55,000
a clear understanding of suffering.

254
00:16:55,000 --> 00:16:57,000
Barinya, let me talk about the first

255
00:16:57,000 --> 00:16:58,000
normal truth.

256
00:16:58,000 --> 00:17:00,000
The path is not to avoid suffering

257
00:17:00,000 --> 00:17:02,000
and enter into blissful states.

258
00:17:02,000 --> 00:17:04,000
It's about letting go of suffering.

259
00:17:04,000 --> 00:17:06,000
It's about understanding suffering

260
00:17:06,000 --> 00:17:08,000
and when you see that it's suffering

261
00:17:08,000 --> 00:17:09,000
you let go of it.

262
00:17:09,000 --> 00:17:13,000
You stop clinging to it.

263
00:17:13,000 --> 00:17:17,000
So, you know,

264
00:17:17,000 --> 00:17:19,000
I haven't confused you all even more

265
00:17:19,000 --> 00:17:21,000
about concentration is something

266
00:17:21,000 --> 00:17:23,000
that comes through the practice,

267
00:17:23,000 --> 00:17:26,000
something that involves seeing things

268
00:17:26,000 --> 00:17:27,000
clearly.

269
00:17:27,000 --> 00:17:30,000
Another way of translating it is right focus

270
00:17:30,000 --> 00:17:32,000
when your mind is in focus

271
00:17:32,000 --> 00:17:34,000
means you can see things clearly

272
00:17:34,000 --> 00:17:35,000
and that's important

273
00:17:35,000 --> 00:17:37,000
because concentration in the three

274
00:17:37,000 --> 00:17:39,000
fold training is what leads to wisdom.

275
00:17:39,000 --> 00:17:41,000
You see the morality

276
00:17:41,000 --> 00:17:43,000
or ethics leads to concentration,

277
00:17:43,000 --> 00:17:46,000
concentration, commodity leads to wisdom.

278
00:17:46,000 --> 00:17:48,000
So it involves focus.

279
00:17:48,000 --> 00:17:51,000
It means you focus your attention

280
00:17:51,000 --> 00:17:53,000
not too hard

281
00:17:53,000 --> 00:17:55,000
just right so that you can see things

282
00:17:55,000 --> 00:17:57,000
just perfect focus

283
00:17:57,000 --> 00:18:00,000
so you can see things as they are.

284
00:18:00,000 --> 00:18:03,000
That's right, concentration.

285
00:18:03,000 --> 00:18:06,000
So, thank you all for tuning in.

286
00:18:06,000 --> 00:18:09,000
That's already evening done.

287
00:18:09,000 --> 00:18:14,000
And that will take questions.

288
00:18:21,000 --> 00:18:23,000
Two questions tonight.

289
00:18:23,000 --> 00:18:25,000
When doing walking meditation

290
00:18:25,000 --> 00:18:27,000
my back foot seems to always start to come up

291
00:18:27,000 --> 00:18:30,000
at the same time I put my front toes down.

292
00:18:30,000 --> 00:18:32,000
Should there be awareness of both actions

293
00:18:32,000 --> 00:18:35,000
at the same time?

294
00:18:35,000 --> 00:18:37,000
Okay, well that shouldn't happen.

295
00:18:37,000 --> 00:18:39,000
Remember when you're doing walking meditation

296
00:18:39,000 --> 00:18:41,000
you're not trying to walk.

297
00:18:41,000 --> 00:18:44,000
You're trying to move one foot

298
00:18:44,000 --> 00:18:47,000
and that movement should be all that's in your mind.

299
00:18:47,000 --> 00:18:51,000
So the technique is to only move one foot at a time.

300
00:18:51,000 --> 00:18:53,000
If you're moving both feet at once

301
00:18:53,000 --> 00:18:56,000
while you're doing something,

302
00:18:56,000 --> 00:18:58,000
it's suboptimal.

303
00:18:58,000 --> 00:19:00,000
Because if two things are moving at once

304
00:19:00,000 --> 00:19:03,000
very hard to focus only on one as you can see.

305
00:19:03,000 --> 00:19:05,000
So do take a little shorter step

306
00:19:05,000 --> 00:19:07,000
or just modify the way you think about it.

307
00:19:07,000 --> 00:19:09,000
It's not about walking.

308
00:19:09,000 --> 00:19:11,000
I'm moving this foot.

309
00:19:11,000 --> 00:19:13,000
That one's done.

310
00:19:13,000 --> 00:19:14,000
Then start moving the other foot.

311
00:19:14,000 --> 00:19:16,000
You're not trying to get anywhere.

312
00:19:16,000 --> 00:19:18,000
You're being aware of a movement.

313
00:19:18,000 --> 00:19:20,000
I've had kids do this standing still.

314
00:19:20,000 --> 00:19:22,000
When I have a room full of kids they can't walk

315
00:19:22,000 --> 00:19:24,000
and you wouldn't want them to do anyway.

316
00:19:24,000 --> 00:19:26,000
Just tell them to lift their feet

317
00:19:26,000 --> 00:19:27,000
and put their foot down.

318
00:19:27,000 --> 00:19:28,000
Lifting.

319
00:19:28,000 --> 00:19:29,000
Placing.

320
00:19:29,000 --> 00:19:30,000
Lifting.

321
00:19:30,000 --> 00:19:31,000
Placing.

322
00:19:31,000 --> 00:19:33,000
Because we're just trying to learn.

323
00:19:33,000 --> 00:19:36,000
We're trying to observe reality

324
00:19:36,000 --> 00:19:38,000
to see how our mind reacts

325
00:19:38,000 --> 00:19:40,000
and to learn about the interaction

326
00:19:40,000 --> 00:19:42,000
between body and mind.

327
00:19:42,000 --> 00:19:47,000
So focus on one experience at a time.

328
00:19:47,000 --> 00:19:51,000
You can't possibly be aware of two things

329
00:19:51,000 --> 00:19:52,000
at once anyway.

330
00:19:52,000 --> 00:19:53,000
It's not technically.

331
00:19:53,000 --> 00:19:54,000
If you think about it,

332
00:19:54,000 --> 00:19:57,000
it would be very strange to think about as possible.

333
00:19:57,000 --> 00:20:01,000
Kind of an onigami experience

334
00:20:01,000 --> 00:20:06,000
Dominasa is a version of prerequisite for unhappiness.

335
00:20:10,000 --> 00:20:12,000
I would say no.

336
00:20:12,000 --> 00:20:16,000
An onigami cannot experience Dominasa

337
00:20:16,000 --> 00:20:21,000
because Dominasa has to do with patina.

338
00:20:21,000 --> 00:20:25,000
And patina is something that an onigami has done away with.

339
00:20:27,000 --> 00:20:30,000
I think, again, very technical questions

340
00:20:30,000 --> 00:20:33,000
and let me know when you become an onigami

341
00:20:33,000 --> 00:20:35,000
and then we'll talk.

342
00:20:38,000 --> 00:20:42,000
How is enlightenment permanent?

343
00:20:42,000 --> 00:20:46,000
It's kind of like a damn crack,

344
00:20:46,000 --> 00:20:48,000
a crack in a dam.

345
00:20:48,000 --> 00:20:51,000
You can't possibly get that water back in.

346
00:20:51,000 --> 00:20:53,000
That's not a great analogy,

347
00:20:53,000 --> 00:20:55,000
but that's how I thought of this as,

348
00:20:55,000 --> 00:20:58,000
you know, think of the one thing that is irreversible.

349
00:20:58,000 --> 00:21:01,000
In the damn cracks, that's it.

350
00:21:01,000 --> 00:21:03,000
You can't fix it.

351
00:21:03,000 --> 00:21:04,000
In the damn breaks,

352
00:21:04,000 --> 00:21:07,000
there's no getting that water back in there.

353
00:21:07,000 --> 00:21:08,000
It's done.

354
00:21:11,000 --> 00:21:14,000
Maybe it is a fairly good analogy.

355
00:21:14,000 --> 00:21:17,000
It's like pulling the plug on a bathtub.

356
00:21:17,000 --> 00:21:19,000
It's only a matter of time.

357
00:21:19,000 --> 00:21:22,000
It's not the greatest because you can repug the bathtub,

358
00:21:22,000 --> 00:21:24,000
but it's really like a crack.

359
00:21:24,000 --> 00:21:25,000
When you become a soda pond

360
00:21:25,000 --> 00:21:26,000
and you've seen the amount,

361
00:21:26,000 --> 00:21:29,000
it puts a crack in Zamsara.

362
00:21:29,000 --> 00:21:32,000
It starts making cracks in the universe

363
00:21:32,000 --> 00:21:34,000
and you see it again and again

364
00:21:34,000 --> 00:21:36,000
and the cracks get bigger and bigger

365
00:21:36,000 --> 00:21:38,000
and you can't fix those cracks.

366
00:21:41,000 --> 00:21:42,000
How is it?

367
00:21:42,000 --> 00:21:44,000
Well, it's a part of reality.

368
00:21:44,000 --> 00:21:46,000
It's a claim we make.

369
00:21:46,000 --> 00:21:49,000
So maybe you disagree or are skeptical,

370
00:21:49,000 --> 00:21:50,000
which is fine.

371
00:21:50,000 --> 00:21:52,000
Practice for yourself and see

372
00:21:52,000 --> 00:21:54,000
if it turns out to be not permanent

373
00:21:54,000 --> 00:21:58,000
then you know.

374
00:21:58,000 --> 00:22:00,000
It's working on improving life

375
00:22:00,000 --> 00:22:02,000
for all beings by using science,

376
00:22:02,000 --> 00:22:05,000
compassion, and education.

377
00:22:05,000 --> 00:22:07,000
Science, compassion, and education.

378
00:22:07,000 --> 00:22:09,000
Holesome.

379
00:22:09,000 --> 00:22:11,000
Yes, very wholesome.

380
00:22:11,000 --> 00:22:16,000
See, technically,

381
00:22:16,000 --> 00:22:19,000
it's not anything to do with wholesomeness.

382
00:22:19,000 --> 00:22:22,000
Holesomeness is your state of mind

383
00:22:22,000 --> 00:22:24,000
and that changes every moment.

384
00:22:24,000 --> 00:22:25,000
Suppose you say,

385
00:22:25,000 --> 00:22:27,000
I'm working to improve people's life

386
00:22:27,000 --> 00:22:30,000
using science, compassion, and education,

387
00:22:30,000 --> 00:22:31,000
but you still get angry

388
00:22:31,000 --> 00:22:33,000
and you get frustrated

389
00:22:33,000 --> 00:22:34,000
and you get burnt out

390
00:22:34,000 --> 00:22:37,000
and you feel ego about your good work and so on.

391
00:22:37,000 --> 00:22:38,000
All of those moments

392
00:22:38,000 --> 00:22:40,000
when those arise are unwholesome,

393
00:22:40,000 --> 00:22:42,000
which is why the greatest wholesomeness

394
00:22:42,000 --> 00:22:43,000
is mindfulness.

395
00:22:43,000 --> 00:22:45,000
The greatest wholesomeness is meditation

396
00:22:45,000 --> 00:22:49,000
because you clear every moment is wholesome

397
00:22:49,000 --> 00:22:52,000
and you're developing habits of wholesomeness.

398
00:22:52,000 --> 00:22:55,000
So no,

399
00:22:55,000 --> 00:22:57,000
the work can never be wholesome.

400
00:22:57,000 --> 00:22:59,000
The question is, what is your intention now?

401
00:22:59,000 --> 00:23:04,000
When your work is to better improve the life of beings,

402
00:23:04,000 --> 00:23:06,000
it's of course far more likely

403
00:23:06,000 --> 00:23:09,000
that wholesome states are going to arise.

404
00:23:09,000 --> 00:23:11,000
Anytime you help someone,

405
00:23:11,000 --> 00:23:13,000
it can be very strong and powerful wholesomeness,

406
00:23:13,000 --> 00:23:14,000
so sure, of course.

407
00:23:14,000 --> 00:23:16,000
But be clear,

408
00:23:16,000 --> 00:23:17,000
it's your state of mind.

409
00:23:17,000 --> 00:23:19,000
I mean, someone says yes,

410
00:23:19,000 --> 00:23:20,000
I'm working to improve the lives of beings

411
00:23:20,000 --> 00:23:23,000
by experimenting using

412
00:23:23,000 --> 00:23:28,000
and testing drugs on lab rats.

413
00:23:28,000 --> 00:23:30,000
Not very wholesome,

414
00:23:30,000 --> 00:23:32,000
because those poor lab rats

415
00:23:32,000 --> 00:23:35,000
are probably a reason,

416
00:23:35,000 --> 00:23:37,000
big reason why there's so much

417
00:23:37,000 --> 00:23:41,000
disease and sickness is because of how we treat each other,

418
00:23:41,000 --> 00:23:43,000
living beings.

419
00:23:43,000 --> 00:23:44,000
We get sick all the time

420
00:23:44,000 --> 00:23:47,000
because we're mean and nasty people.

421
00:23:47,000 --> 00:23:52,000
I mean, from lifetime to lifetime, of course.

422
00:23:52,000 --> 00:23:55,000
So yes,

423
00:23:55,000 --> 00:23:58,000
it's about moments rather than actions

424
00:23:58,000 --> 00:24:03,000
or lifestyles or work or that kind of thing.

425
00:24:03,000 --> 00:24:06,000
Okay, so that's the demo for tonight.

426
00:24:06,000 --> 00:24:15,000
Thank you all for tuning in.

