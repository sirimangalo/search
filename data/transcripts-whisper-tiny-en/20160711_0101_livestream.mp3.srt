1
00:00:00,000 --> 00:00:23,720
Good evening everyone, I'm broadcasting live July 10th, 2016, so the day is quote seems like

2
00:00:23,720 --> 00:00:30,560
we've had this quote, and you remember we've actually had this quote, maybe it's been

3
00:00:30,560 --> 00:00:40,640
a year already, but if I had a quote like this, problem with this quote is it's only part

4
00:00:40,640 --> 00:00:48,720
of a quote, and it's a bit misleading at that.

5
00:00:48,720 --> 00:01:01,520
So the quote that we have is that one can have a sadhan, sealhan, suithan, sadhan, one can have faith

6
00:01:01,520 --> 00:01:10,400
in the Buddha, one can have see that one can have morality, and one can be learned and heard

7
00:01:10,400 --> 00:01:18,640
much of the Buddha's teaching, but one's still lacking in one regard, in one respect.

8
00:01:18,640 --> 00:01:30,280
One is not a teacher of the dhamma, so it sounds according to this that there are these four

9
00:01:30,280 --> 00:01:37,200
aspects.

10
00:01:37,200 --> 00:01:47,480
One has all of these, then one is complete with this, it's not what it says, it's

11
00:01:47,480 --> 00:01:55,880
is a one, so tainang, naparipu, bhuti, it could be translated that way, but it wouldn't

12
00:01:55,880 --> 00:02:06,360
mean it shouldn't be, because what it means is in regards to that, through that one is

13
00:02:06,360 --> 00:02:15,040
more full, or one is full in regards to that, that factor.

14
00:02:15,040 --> 00:02:24,360
You'll see the way it has to be translated, this latest factor makes one more complete,

15
00:02:24,360 --> 00:02:34,560
because there's lots more factors, this is actually in a good journey, a book of 10, there's

16
00:02:34,560 --> 00:02:42,560
actually 10 factors, so this quote is misleading, it's not that the only thing missing

17
00:02:42,560 --> 00:02:48,720
from a good monk and a goodist is that they're not teaching, there's lots of things

18
00:02:48,720 --> 00:02:59,880
missing, so we have one might, it starts up by saying one might be moral, one might have

19
00:02:59,880 --> 00:03:16,840
confidence in me moral, but one may be confident, but not moral, faith in the Buddha, but

20
00:03:16,840 --> 00:03:24,400
not be moral, so here's a person who calls themselves Buddhist and has real faith in

21
00:03:24,400 --> 00:03:30,560
the Buddha, having heard his story, and his teachings, and maybe having met the Buddha,

22
00:03:30,560 --> 00:03:35,440
or having met Buddhist monks, so they're great faith in the Buddha, they don't actually

23
00:03:35,440 --> 00:03:42,320
do anything, so they're still killing and lying and cheating and stealing and all that.

24
00:03:42,320 --> 00:03:55,600
Well, in this regard there's the lacking, but then even if they're moral, they're complete

25
00:03:55,600 --> 00:04:04,880
in that regard, what if they're not well-learned, bhavasupthu means of much learning, having

26
00:04:04,880 --> 00:04:08,400
heard much of the Buddha's teaching, they haven't learned much of the Buddha's teaching,

27
00:04:08,400 --> 00:04:13,280
so they're lacking in that regard, there's many questions that they might have and others

28
00:04:13,280 --> 00:04:21,000
might have that they can't answer, they might be challenged, it's very hard to answer

29
00:04:21,000 --> 00:04:26,440
the challenge, put by others, or even by your own mind when you have doubts, you haven't

30
00:04:26,440 --> 00:04:34,980
learned a lot, haven't learned all the different ins and outs of the teaching, so who

31
00:04:34,980 --> 00:04:42,900
cares to that, when they're still not complete, but then one can be well-learned, no

32
00:04:42,900 --> 00:04:49,040
jedamakatipo, and this is where the part that of the quote that he's pulled out, but one

33
00:04:49,040 --> 00:04:54,040
still doesn't teach the dhamma, there's one doesn't teach the dhamma, one's not really

34
00:04:54,040 --> 00:04:58,760
complete in that regard, in that regard, what is still lacking, so it is true that the Buddha

35
00:04:58,760 --> 00:05:05,080
is saying here that maybe it's just for months, or maybe we can all take a lesson from this

36
00:05:05,080 --> 00:05:09,960
and that it's inferior, if you're not helping others, if you're not sharing the dhamma,

37
00:05:09,960 --> 00:05:18,240
if you're not using it for the benefit of others, well it's somehow lacking, or something

38
00:05:18,240 --> 00:05:27,720
you can't become enlightened for yourself, but there's something to this sort of natural

39
00:05:27,720 --> 00:05:34,400
inclination to teach, it's a natural inclination to help others, and teaching a lot

40
00:05:34,400 --> 00:05:42,640
today, in this morning we went to Mississauga again, in this ordination, today was the

41
00:05:42,640 --> 00:05:49,360
second half of the whole thing, and I was able to give a short talk, I'm really happy

42
00:05:49,360 --> 00:06:00,160
about that, it was really good to be that, to be inspiring, to offer this inspiration, that's

43
00:06:00,160 --> 00:06:08,600
what this suit is about, it's about being an inspiration, inspiring confidence in all

44
00:06:08,600 --> 00:06:15,160
respects and who is complete in all aspects, so we don't get to that until the end, it's

45
00:06:15,160 --> 00:06:23,320
not complete yet, so this quote is therefore misleading, so because one might teach the dhamma

46
00:06:23,320 --> 00:06:40,400
and then, not be one who upholds the winnaya, I'm not sure how that differs from sea

47
00:06:40,400 --> 00:06:47,480
though, but there you have it, winnaya dar, winnaya dar, but there's something in the

48
00:06:47,480 --> 00:06:54,040
commentary about that, what's the difference, but one might be a winnaya, winnaya dar is,

49
00:06:54,040 --> 00:06:59,280
I guess someone who knows the ins and outs of the winnaya, the monastic rules, it's not

50
00:06:59,280 --> 00:07:05,760
the same as sea level, because sea level is real true ethics, but winnaya dar, for a month,

51
00:07:05,760 --> 00:07:10,560
to know the ins and outs of all the artificial rules, like how to wear your robe, or how

52
00:07:10,560 --> 00:07:23,360
to carry your bowl, all the ins and outs of monastic life, that's important to maintain

53
00:07:23,360 --> 00:07:35,440
order and to create a framework for the training that we undertake as put this month.

54
00:07:35,440 --> 00:07:43,600
So this quote actually is pretty specific geared towards months, but you can adapt it

55
00:07:43,600 --> 00:07:50,840
for it, everybody has rules of their community and norms that have to be maintained

56
00:07:50,840 --> 00:08:00,640
and meditation centers have rules, that kind of thing, so but even if one is well versed

57
00:08:00,640 --> 00:08:14,060
in community, our communal harmony and that sort of thing, but one isn't an aranyika,

58
00:08:14,060 --> 00:08:20,960
one doesn't see an arsenal, one doesn't look good well in the forest, it doesn't dwell

59
00:08:20,960 --> 00:08:31,120
in the secluded dwelling, and one doesn't complete in that regard, and a new go-jap on

60
00:08:31,120 --> 00:08:36,200
the sea and arsenal, so there you have it, we should be living in the forest, because

61
00:08:36,200 --> 00:08:41,600
our goal I was just talking to some people today in Mississauga and talking to the monks

62
00:08:41,600 --> 00:08:48,120
and that kind of thing, trying to get a sense of where we can find a place, what can

63
00:08:48,120 --> 00:08:56,880
we do, where can we go in Ontario, in Canada, anywhere, where we can build a forest

64
00:08:56,880 --> 00:09:06,400
or a monastery, more in nature, where there's more room and less, less of the constraints

65
00:09:06,400 --> 00:09:14,320
of being in the city, something for the future, but even if one dwells in seclusion

66
00:09:14,320 --> 00:09:25,600
which is very good, being alone, being in a secluded place, it allows you to enter into

67
00:09:25,600 --> 00:09:31,320
states of common form, but if you go into the forest and don't enter into the four

68
00:09:31,320 --> 00:09:54,040
genres, this is the next one, it's not able to obtain, it's not become one who is able

69
00:09:54,040 --> 00:10:06,640
to obtain with ease this dwelling happily in the here and now, in reality, or in the

70
00:10:06,640 --> 00:10:20,520
here and now, that is the four genres, the Abhijaytasika, the higher dhamma, the higher

71
00:10:20,520 --> 00:10:28,280
higher mind that is the four genres, and still not, still missing, still lacking, if you

72
00:10:28,280 --> 00:10:36,520
can't actually calm the mind, focus the mind, there's something missing, but you can

73
00:10:36,520 --> 00:10:51,760
calm and focus the mind, there's something missing, even then, no just as a one, no dhamma,

74
00:10:51,760 --> 00:10:59,800
dhamma, dhamma, singhang, abhinya, satchikatwa, put some budge, we had it, one can be calm

75
00:10:59,800 --> 00:11:06,200
and tranquil, living in the forest, keeping the moral, the monastic precepts and so on and

76
00:11:06,200 --> 00:11:14,080
even teaching and even have very faith, even of all these things, but if you haven't

77
00:11:14,080 --> 00:11:25,040
cut off the dhamma, the taints, you haven't destroyed the taints, as of asava, nang kaya,

78
00:11:25,040 --> 00:11:33,840
dwelling in the tainthless freedom of mind and freedom of wisdom, you're still missing

79
00:11:33,840 --> 00:11:49,320
something, and you will say just a second, you should fulfill a little dust being incomplete

80
00:11:49,320 --> 00:11:56,680
in regards to one fact, whatever factor, they should fulfill that factor, thinking, how can

81
00:11:56,680 --> 00:12:03,760
I be endowed with faith, how can I be endowed with this and this and this, then basically

82
00:12:03,760 --> 00:12:14,320
you should work to fulfill that which is missing, and you said, but a person who amongst

83
00:12:14,320 --> 00:12:21,600
who has all these ten things, who has confidence in the Buddha, who has morality, who

84
00:12:21,600 --> 00:12:34,920
has well learned, who is a teacher of the dhamma, who is a holder of the discipline, who lives

85
00:12:34,920 --> 00:12:45,360
in a secluded forest dwelling, who can enter at ease the four jhanas, and who has cut

86
00:12:45,360 --> 00:12:53,460
off the taints, to destroy the taints, and wholesome states of mind, that even the

87
00:12:53,460 --> 00:13:22,960
transcendency from wholesome states of mind, and such a person is complete, he may go be

88
00:13:22,960 --> 00:13:33,540
in the proper teaching here, it's unfortunate how misleading this is, although he is making

89
00:13:33,540 --> 00:13:39,980
a fair point, that's interesting how the Buddha talks about teaching as being necessary

90
00:13:39,980 --> 00:13:50,420
for completion, especially for a Buddhist monk, he's not saying that all of this is necessarily

91
00:13:50,420 --> 00:13:59,660
necessary, it's just that it's not perfect, the best kind of monk, and by extension

92
00:13:59,660 --> 00:14:04,420
the best kind of Buddhist meditator is one who fulfills all ten of these, doesn't mean

93
00:14:04,420 --> 00:14:11,780
that you have to be complete and all of them, you have to respect and appreciate all

94
00:14:11,780 --> 00:14:25,860
of these factors, so anyway, that's the dhamma that we're looking at today, missed yesterday,

95
00:14:25,860 --> 00:14:35,980
so let's see if we've got some questions, we have some questions from yesterday, so if

96
00:14:35,980 --> 00:14:42,780
we need a better interface, we should really have a separate interface from questions

97
00:14:42,780 --> 00:14:51,660
probably, I can't see all the questions, because you're not put in the cues, here's

98
00:14:51,660 --> 00:15:05,060
one, I woke up this morning with a strong feeling of or for nothing is really worth it,

99
00:15:05,060 --> 00:15:09,220
I wonder if it is a way to know or not, I can't say I like it or don't like it, but it's

100
00:15:09,220 --> 00:15:17,060
not indifferent either, hours after I thought it might count more as liking because it is

101
00:15:17,060 --> 00:15:23,500
a novel encounter, felt fresh, but with a concomitant feeling and as much to do like now

102
00:15:23,500 --> 00:15:29,900
what, I guess my question is whether the response is of like, just like it indifferent,

103
00:15:29,900 --> 00:15:40,580
or really exhaustive or suffice to categories, all ideas, yeah, it can be subtle, it can

104
00:15:40,580 --> 00:15:49,420
be on mixed, like boredom, what is boredom, well it's mostly anger-based, but it's also

105
00:15:49,420 --> 00:15:55,380
got some delusion in there with laziness and that kind of thing, I can also be mixed

106
00:15:55,380 --> 00:16:04,340
up a greed, boredom can be very much associated with greed, it's just an example, it's

107
00:16:04,340 --> 00:16:11,460
not really important which one it is, it's important exactly what it is, so if you're

108
00:16:11,460 --> 00:16:18,460
thinking or if you're feeling, you can just say thinking, thinking or feeling, feeling,

109
00:16:18,460 --> 00:16:29,980
feeling of nothing being worth it, of indifference in a sense or disinterest, so you

110
00:16:29,980 --> 00:16:38,700
could say disinterested or weary maybe, but feeling, feeling works, as long as you're

111
00:16:38,700 --> 00:16:43,900
going to identify an objective thing, this is this, it's not so important what you call

112
00:16:43,900 --> 00:17:13,580
it.

113
00:17:13,580 --> 00:17:25,720
Someone who, becoming ordained is really out of the picture of a wife and a wife and

114
00:17:25,720 --> 00:17:29,740
12 year old kid being ordained with some unreached.

115
00:17:29,740 --> 00:17:33,500
They've come so far and learned so much.

116
00:17:33,500 --> 00:17:38,860
What is the closest I could become to ordaining?

117
00:17:38,860 --> 00:17:42,300
It's really up to you, you know.

118
00:17:42,300 --> 00:17:49,300
There are, it's an apt question, I suppose, because that's exactly it.

119
00:17:49,300 --> 00:17:58,060
You can get close to ordaining, ordaining is not, it's not, I guess, or no question,

120
00:17:58,060 --> 00:18:02,180
ordained, ordained, it's not a binary thing.

121
00:18:02,180 --> 00:18:10,580
You can undertake many of these, these, these 10 things, many aspects of the monastic

122
00:18:10,580 --> 00:18:13,460
life can be undertaken by late people.

123
00:18:13,460 --> 00:18:14,940
I think that's really important.

124
00:18:14,940 --> 00:18:20,180
As a late person I was doing this, when I, when I started meditating, I didn't have

125
00:18:20,180 --> 00:18:25,220
the opportunity to ordain for around two years.

126
00:18:25,220 --> 00:18:29,460
The during those two years I was doing my best to try and be as monastic as possible as

127
00:18:29,460 --> 00:18:32,220
much of the time as possible.

128
00:18:32,220 --> 00:18:38,740
In fact, at one point I thought I was going to ordain at least temporarily, and then

129
00:18:38,740 --> 00:18:43,380
I found that I, I really couldn't, or just wasn't convenient.

130
00:18:43,380 --> 00:18:49,020
And in protest I went out and bought myself a stainless steel bowl and started eating

131
00:18:49,020 --> 00:18:56,020
food mixed up in a, in a non-monastic, but like a mixing bowl.

132
00:18:56,020 --> 00:18:58,900
And the nuns in the monastery, I remember them looking at me, I hope he's got a big

133
00:18:58,900 --> 00:18:59,900
bowl.

134
00:18:59,900 --> 00:19:07,460
They were all so, such busy bodies, not always, it's a group of, everybody's worried about

135
00:19:07,460 --> 00:19:08,460
anyone else.

136
00:19:08,460 --> 00:19:10,420
So they were looking at my big bowl.

137
00:19:10,420 --> 00:19:16,900
I think thinking that I was being greedy, that I wanted more food, so I needed a big bowl.

138
00:19:16,900 --> 00:19:20,340
And then it was more just to be like a monastic.

139
00:19:20,340 --> 00:19:24,580
So there are many things you can do, you know, you can, some people in Thailand wonder

140
00:19:24,580 --> 00:19:26,580
take to wear white robes.

141
00:19:26,580 --> 00:19:32,900
Maybe that's beyond, but that's about as close as they can get.

142
00:19:32,900 --> 00:19:34,980
There's many things you can do.

143
00:19:34,980 --> 00:19:48,020
And just if you start giving up your luxuries, your extravagant things, start sleeping

144
00:19:48,020 --> 00:19:49,020
on the floor, that kind of thing.

145
00:19:49,020 --> 00:19:55,460
And there's many things you can do, in order to do any or all or any of them, but whenever

146
00:19:55,460 --> 00:20:04,380
you can do to practice renunciation, that's great.

147
00:20:04,380 --> 00:20:12,140
When I get a bad mindset, I say to myself, wrong, wrong, wrong, well that's not really

148
00:20:12,140 --> 00:20:13,140
helpful.

149
00:20:13,140 --> 00:20:17,980
Wrong is too much of a judgment and it'll be very negative from the mind.

150
00:20:17,980 --> 00:20:24,220
Are there other ways to deal with toxic people other than avoiding them all together?

151
00:20:24,220 --> 00:20:31,980
Sidewalk can be a hostile place, I guess, I mean mindfulness of course is the answer.

152
00:20:31,980 --> 00:20:39,580
You're going to be mindful of the side box, not really a hostile place.

153
00:20:39,580 --> 00:20:48,100
You have to remember that, they would have said, you're talking about the use of the word

154
00:20:48,100 --> 00:20:56,500
toxic, the poison only hurts the hand that is wounded, if your hand is unmounded, you can

155
00:20:56,500 --> 00:21:05,060
handle poison in your hands, you can carry it in your hands, and not die from it.

156
00:21:05,060 --> 00:21:13,780
Anything toxic to the mind, only poisons your mind if you let it in, if your mind is strong,

157
00:21:13,780 --> 00:21:28,100
there's nothing that can poison you.

158
00:21:28,100 --> 00:21:34,780
Attachment causes us to be attracted to others that's fallen love, love.

159
00:21:34,780 --> 00:21:39,900
Attachment is one cause of suffering, so how should someone live who is already in a relationship

160
00:21:39,900 --> 00:21:45,980
knowing that someday this feeling or desire for his or a partner will vanish?

161
00:21:45,980 --> 00:21:55,700
I don't know that that's how it works, I mean it doesn't just vanish, I mean you're kind

162
00:21:55,700 --> 00:22:01,940
of putting the cart before the horse, how should I act when I know that someday I might

163
00:22:01,940 --> 00:22:13,460
give up, I mean why don't you cross that bridge when you come to it, yeah I mean it would

164
00:22:13,460 --> 00:22:20,660
be kind of pessimistic, not exactly pessimistic, but it would be getting ahead of yourself

165
00:22:20,660 --> 00:22:29,620
very much, for no reason, even when you give it up there will be no need to prepare

166
00:22:29,620 --> 00:22:34,500
for it, it's not like I've given it up and I wasn't ready to give it up, you only give

167
00:22:34,500 --> 00:22:43,500
something up when you're very much ready to give it up, so in the meantime if you want

168
00:22:43,500 --> 00:22:47,860
to be free from suffering you want to cultivate true peace and happiness, you should

169
00:22:47,860 --> 00:22:54,700
look at the emotion, look at the love, the attachment, the desire, don't judge it, just

170
00:22:54,700 --> 00:23:04,540
learn about it, study it, and if you study it close enough, you start to give it up naturally.

171
00:23:04,540 --> 00:23:08,100
As far as thinking about impermanence that's not really how it works, but what you're going

172
00:23:08,100 --> 00:23:13,100
to see is that it is impermanence, that seeing will come by itself, it'll come naturally,

173
00:23:13,100 --> 00:23:18,580
it won't be an intellectual thing, it's not about thinking about impermanence, but as you

174
00:23:18,580 --> 00:23:28,900
see that it's impermanence, you'll start to give it up.

175
00:23:28,900 --> 00:23:35,500
I wish to study the Buddhist texts, but not sure where to begin, is there a recommended

176
00:23:35,500 --> 00:23:45,020
starting point for a beginner, well, a lot of the Buddhist teaching has been translated

177
00:23:45,020 --> 00:24:12,500
into English and recommend reading Bikubori's translation, a few nights ago you recommended

178
00:24:12,500 --> 00:24:17,220
for us to meditate before listening to Dhamma talk, do you have any more advice how one should

179
00:24:17,220 --> 00:24:22,300
listen to Dhamma talks, or to gain the most benefit from them, I think at the most benefit

180
00:24:22,300 --> 00:24:30,500
you should acknowledge at the ear hearing should be mindful and meditate during the time,

181
00:24:30,500 --> 00:24:35,060
read my booklet how to meditate and try to practice that while you're listening, it's

182
00:24:35,060 --> 00:24:39,060
a great way to meditate, a great way to listen to the Dhamma, of course then you don't

183
00:24:39,060 --> 00:24:43,100
have to listen to so much Dhamma, as you get it, you start to say, well, why do I need

184
00:24:43,100 --> 00:24:49,620
to listen to this when I can just meditate without it, so listening to the Dhamma is actually

185
00:24:49,620 --> 00:24:54,620
in the long one generally overrated, we've listened to too much and we don't practice

186
00:24:54,620 --> 00:25:01,060
enough, that's a fairly general good generalization, not everyone and not all the time,

187
00:25:01,060 --> 00:25:08,060
but in general you tend to listen too much and practice too much.

188
00:25:31,060 --> 00:25:45,260
All right, I'm sorry to let all that much to say tonight, it's been a bit of a day traveling

189
00:25:45,260 --> 00:25:53,860
to Mississauga, but it was nice, you know it's nice, always to meet new people, this

190
00:25:53,860 --> 00:26:01,660
is a new group, Bangladeshi Buddhists, Bangali Buddhists, Bangladeshi, you can't keep it

191
00:26:01,660 --> 00:26:10,660
on straight, from Bangladeshi, Bangladeshi is an incredible situation, lots of violence

192
00:26:10,660 --> 00:26:18,460
there, violence against Buddhists, you know it's a fairly complicated situation but something

193
00:26:18,460 --> 00:26:28,700
complicated about the terrible things that were happening and so they ordained six of their

194
00:26:28,700 --> 00:26:35,260
young boys temporarily, six Bangladeshi boys came to this really good monastery, because

195
00:26:35,260 --> 00:26:41,940
the monk who sort of runs the place or is a catalyst for a lot of the things is from Banggadeshi

196
00:26:41,940 --> 00:26:53,740
from Bangladeshi and these are really good friends, really nice guy and so it was invited

197
00:26:53,740 --> 00:26:59,100
when many of us were invited to take part and I was asked to give a talk but the funny

198
00:26:59,100 --> 00:27:05,380
thing is there were two of us, the first monk was a Bangladeshi monk, he was asked to

199
00:27:05,380 --> 00:27:09,860
give five or ten minutes and he went on for I think 15 or 20 minutes so when it came

200
00:27:09,860 --> 00:27:17,860
to my turn, the head monk said yeah maybe five to eight minutes, we didn't have any

201
00:27:17,860 --> 00:27:23,780
time and I said all right five minutes and so I give a five minute of my talk which

202
00:27:23,780 --> 00:27:28,740
may not sound like much and it probably went over a couple of minutes over but it was

203
00:27:28,740 --> 00:27:33,460
actually quite, I was able to get quite a bit out and many people came up and thanked me

204
00:27:33,460 --> 00:27:38,540
afterwards just for such a short talk it must have been more than five minutes but not

205
00:27:38,540 --> 00:27:43,580
much more but you know it wasn't even, I kept saying I was said to them you know it's not

206
00:27:43,580 --> 00:27:49,180
mind teaching and I can't really take credit for it I'm just saying the things that my

207
00:27:49,180 --> 00:27:58,180
teacher said to all of us, it's very pithy stuff and talking about the four opportunities

208
00:27:58,180 --> 00:28:07,540
that we have, these four opportunities that's what I was teaching today, these six young

209
00:28:07,540 --> 00:28:12,480
men have taken the opportunity, nobody said don't let the opportunity, don't let the moment

210
00:28:12,480 --> 00:28:22,620
pass you by Kanohoma, but the guy who was just of my teaching.

211
00:28:22,620 --> 00:28:35,740
His intuition real, sometimes I can sense something bad happening, and potentially we're

212
00:28:35,740 --> 00:28:40,900
just reading today, we're just in the VCD manga that we did today, we were reading about

213
00:28:40,900 --> 00:28:50,100
Anagatang say knowledge of the future, so prognostication seems to be a thing, it does seem

214
00:28:50,100 --> 00:28:59,660
to be possible, premonitions do seem to be at times based on fact, don't know how it works,

215
00:28:59,660 --> 00:29:08,740
but I always bring up this quantum slit experiment where you have these two entangled particles

216
00:29:08,740 --> 00:29:15,460
such that two particles go off in separate directions, and if you measure this particle,

217
00:29:15,460 --> 00:29:23,020
it affects this particle, which may not sound like much, but it actually implies, the

218
00:29:23,020 --> 00:29:28,980
problem that they had with us with that it seems to imply faster than light travel because

219
00:29:28,980 --> 00:29:33,540
affecting something over here can't, it changes something over here, can't affect something

220
00:29:33,540 --> 00:29:38,420
over here faster than the speed of light because information can't travel faster than

221
00:29:38,420 --> 00:29:45,740
the speed of light, this particle can't know that this particle was measured, but it does,

222
00:29:45,740 --> 00:29:51,460
as soon as this is measured, this one is changed, right, it shows that reality is not

223
00:29:51,460 --> 00:29:56,660
quite what we think it is, but that's not what's interesting, interesting is if you shorten

224
00:29:56,660 --> 00:30:11,060
the distance such that this particle is measured before, nothing, no, this, if you lengthen

225
00:30:11,060 --> 00:30:17,940
this one, so the particle is measured after, the particle is measured, which is the thing

226
00:30:17,940 --> 00:30:27,700
that that affects this one, but it's measured after this particle touches, reaches its destination,

227
00:30:27,700 --> 00:30:33,020
so once it reaches its destination it should be fixed, right, but no, if you measure this

228
00:30:33,020 --> 00:30:38,580
particle after this particle has reached its destination, the measuring of this particle

229
00:30:38,580 --> 00:30:44,740
still affects this particle, and being very general you have to really study it, but this

230
00:30:44,740 --> 00:30:51,980
is actually, from what I've studied this has actually happened, so this particle is actually

231
00:30:51,980 --> 00:30:57,740
affecting something that has already happened, which, I mean, I'm sure quantum physicists

232
00:30:57,740 --> 00:31:02,660
would, well some of them would criticize me for even suggesting it, but it does appear

233
00:31:02,660 --> 00:31:08,780
that that's a sort of a timing travel, the future is affecting the past, it's not exactly

234
00:31:08,780 --> 00:31:17,860
and it's not as simple as that, or maybe it's even more simple than that, but the point

235
00:31:17,860 --> 00:31:23,420
being that the universe is not quite as we think it is, you know, this idea of classical

236
00:31:23,420 --> 00:31:30,860
physics, classical physics is false, this physics of billiard balls of linear time

237
00:31:30,860 --> 00:31:40,060
and so on is simplistic and many aspects of it are simply incorrect, I don't know, that's

238
00:31:40,060 --> 00:31:44,820
a bit of a ten, it's not really important for our practice as Buddhists, except kind of

239
00:31:44,820 --> 00:31:51,460
to understand that the universe is not as it appears, it's very much more based on experience

240
00:31:51,460 --> 00:32:02,940
than it is on billiard balls, atoms, and that kind of thing, particles, and you know

241
00:32:02,940 --> 00:32:11,660
if I brought up the teaching you're going to make me rehash it, right, spooky action

242
00:32:11,660 --> 00:32:17,460
at a distance, I believe it was Einstein, he was the one who was against quantum physics,

243
00:32:17,460 --> 00:32:25,060
he was really turned off by this new thing and said, it's just not, not, there's something

244
00:32:25,060 --> 00:32:30,140
wrong here, it just doesn't sit well, he called it spooky action at a distance of

245
00:32:30,140 --> 00:32:40,500
you, the four opportunities, we have this opportunity that we are born in the time of the

246
00:32:40,500 --> 00:32:50,940
Buddha's teaching, this time period that there is a Buddha in here, we still have the Buddha,

247
00:32:50,940 --> 00:32:56,060
we have his dhamma kai, dhamma kai means the body of teachings, so all the teachings

248
00:32:56,060 --> 00:33:01,700
of the Buddha, the given this quote that we had today, that's part of the dhamma kai, and

249
00:33:01,700 --> 00:33:06,140
that body of teachings is the body of the Buddha, we still hear it, but that's not

250
00:33:06,140 --> 00:33:12,340
always the kai, who's to say that there's going to be another Buddha, when the next Buddha

251
00:33:12,340 --> 00:33:18,780
is going to come, there are many, many periods of time, most, the vast majority of time

252
00:33:18,780 --> 00:33:26,780
in this universe is spent without someone who has seen through the net, seen through

253
00:33:26,780 --> 00:33:34,380
the veil, the vigor, so we have that opportunity, the second opportunity is that we

254
00:33:34,380 --> 00:33:41,460
are born as humans, it's not very useful if you're born as a cow, the Buddha isn't

255
00:33:41,460 --> 00:33:48,540
all that much benefit to most cows, except in terms of not getting them killed, because

256
00:33:48,540 --> 00:33:55,820
pure people are inclined to do killing them, to kill cows, but not really helpful if the

257
00:33:55,820 --> 00:34:02,060
Buddha teaches a profound discourse on insight, meditation, to a cow, cows not going to

258
00:34:02,060 --> 00:34:10,300
be all that benefit, being born a human is a great, and difficult opportunity, much more

259
00:34:10,300 --> 00:34:17,180
often, much more common is the trust of a born as animal, even in the hell arounds,

260
00:34:17,180 --> 00:34:24,660
you'll be born as animals a thousand times before we get the opportunity just by luck,

261
00:34:24,660 --> 00:34:30,780
just by some happenstance that we incline towards good things to such an extent that

262
00:34:30,780 --> 00:34:36,220
we're actually born as a human being, or we interact with human beings in that such that

263
00:34:36,220 --> 00:34:47,900
we're inclined to be born as a human being, so that's the second opportunity, the third

264
00:34:47,900 --> 00:34:54,740
opportunity is that we, is the opportunity to practice the Buddha's teaching, many human

265
00:34:54,740 --> 00:34:59,260
beings never have that opportunity, even being born a human being, physically there are

266
00:34:59,260 --> 00:35:07,900
many obstacles, if someone is sick or someone is crippled, once someone has an illness is

267
00:35:07,900 --> 00:35:13,380
born with debilitating illness, they might die before they get the opportunity, there may

268
00:35:13,380 --> 00:35:18,420
be physical obstacles, such as distance, they may be born in a place where there is

269
00:35:18,420 --> 00:35:27,180
no Buddha's teaching, there's no meditation center, they might have debt or they might

270
00:35:27,180 --> 00:35:33,700
be, have commitments and that get in their way, so many people don't even have the

271
00:35:33,700 --> 00:35:39,620
opportunity to practice the Buddha's teaching for whatever reason, and the fourth opportunity

272
00:35:39,620 --> 00:35:49,460
is the mental opportunity to have right view, to have the good intention, the intention

273
00:35:49,460 --> 00:35:53,860
to ordain the intention to practice meditation, some people have the opportunity and they

274
00:35:53,860 --> 00:36:01,980
never take it, so this is two parts, there the Buddha said it's rare to find a person

275
00:36:01,980 --> 00:36:10,380
who is stirred by truly stirring things, who is moved by that, which is truly moving, that's

276
00:36:10,380 --> 00:36:16,740
rare, but what's even rare are an interesting part of the quote, it's even more rare,

277
00:36:16,740 --> 00:36:26,100
is among those people who are moved by things that are moving, who are stirred, who are

278
00:36:26,100 --> 00:36:34,180
shook shaken up, when they hear about things that are truly life-shaking or earth-shattering

279
00:36:34,180 --> 00:36:39,900
how you say it, but even rare are among those people, people who actually do something

280
00:36:39,900 --> 00:36:46,300
about it, even though it upsets them, many people do not do anything, so even many

281
00:36:46,300 --> 00:36:52,140
Buddhists who are inclined to meditate, never actually get around to doing it, and this

282
00:36:52,140 --> 00:36:59,900
is important to actually have the will and the volition to just get up and do it as

283
00:36:59,900 --> 00:37:19,300
very rare, much more rare, so that's the fourth opportunity, the good intention, and they

284
00:37:19,300 --> 00:37:24,500
have this good intention, so we should seize upon it, and I guess the point is to have

285
00:37:24,500 --> 00:37:29,580
the desire to do good deeds, and that's an opportunity that we shouldn't let pass up,

286
00:37:29,580 --> 00:37:35,660
and then these opportunities, we should not let them pass us by, don't let the moment pass

287
00:37:35,660 --> 00:37:48,220
you by actually become someone who takes seizes the opportunity, sees the day, so that's

288
00:37:48,220 --> 00:37:51,780
basically what I talked about, I think it was a little bit more eloquent earlier on in

289
00:37:51,780 --> 00:38:05,460
the day, but it's the gist of it, do you think someone can consider themselves a Buddhist

290
00:38:05,460 --> 00:38:15,580
without believing in rebirth, we don't have to believe in anything to be a Buddhist, we

291
00:38:15,580 --> 00:38:26,380
have to practice the Buddhist teaching, we need four things, don't let the days and nights

292
00:38:26,380 --> 00:38:37,380
go by, don't just be lazy, don't neglect your solitude, so it will be inclined and

293
00:38:37,380 --> 00:38:48,740
protect your solitude, dwell in solitude is the need, keep to yourself, and cultivate

294
00:38:48,740 --> 00:38:54,420
tranquility and cultivate insight, these four, because it's how you become someone who lives

295
00:38:54,420 --> 00:39:05,380
by the dummy, but if you have a view that there is no rebirth, that's potentially problematic,

296
00:39:05,380 --> 00:39:10,620
there's that view, that belief, you say I don't believe in rebirth, what you mean

297
00:39:10,620 --> 00:39:15,340
is you believe there is no rebirth, I assume, and if you believe that there is no rebirth,

298
00:39:15,340 --> 00:39:25,380
that belief gets in the way because it's against reality, truth is, unless you're free

299
00:39:25,380 --> 00:39:33,020
from causes of rebirth, desire basically, there's going to be rebirth, continues, even

300
00:39:33,020 --> 00:39:38,140
in this life there will be the birth of many things in this life, but even beyond this

301
00:39:38,140 --> 00:39:47,260
life, Sankay, your question doesn't seem that import that practical to me, again, it's

302
00:39:47,260 --> 00:39:54,940
one of these technical questions, and I don't really have a clue about it anyway, and I

303
00:39:54,940 --> 00:40:12,460
just don't see how it's practical, I'm going to have to call you out on it, okay,

304
00:40:12,460 --> 00:40:16,860
look, we're going to ask a question, put the cocu in front of it, we're not doing it,

305
00:40:16,860 --> 00:40:29,540
we're not playing by the rules, so here's a question, this is a question, see it has

306
00:40:29,540 --> 00:40:59,380
the cuba for it, can you get another green question, or yellow if you haven't been meditated?

307
00:40:59,380 --> 00:41:07,260
So you didn't really have to repeat yourself, okay, I'm good to have lesser chats with

308
00:41:07,260 --> 00:41:12,900
the things like PE, music, art, cooking, which are all required to do at my school, I really

309
00:41:12,900 --> 00:41:19,540
don't want to do them especially for PE because I want to continue to eat two times a day,

310
00:41:19,540 --> 00:41:27,180
how should I deal with this, drop out of school, I don't know, are you old enough to drop

311
00:41:27,180 --> 00:41:32,620
out of school, go to an alternative school, there's lots of alternative schools you can do,

312
00:41:32,620 --> 00:41:37,060
correspondence education through the province I think, there's different ways in Canada

313
00:41:37,060 --> 00:41:42,540
you can anyway, so I did, I actually dropped out of school, went to an alternative school,

314
00:41:42,540 --> 00:41:47,980
and it was great to meet with my teacher once a week and did all the work by myself, great

315
00:41:47,980 --> 00:41:52,500
for meditation, it's a really adult sort of thing, if you've grown up enough to consider

316
00:41:52,500 --> 00:41:59,500
meditation, I think you've grown up enough to do events, or to do alternative education,

317
00:41:59,500 --> 00:42:05,260
that's what I would recommend, remember what can I say, tell them you're not going to

318
00:42:05,260 --> 00:42:12,420
do it, sit there in the bleachers, fail it, don't know what I mean, do it mindfully

319
00:42:12,420 --> 00:42:18,380
I suppose, that would be the best compromise, the easiest compromise, you're going to run

320
00:42:18,380 --> 00:42:29,980
mindfully, throw a ball, throw the ball, mindfully, you may not end up, you know, people

321
00:42:29,980 --> 00:42:36,060
throw the ball at you and you're seeing, let the ball hit you, it might happen, you might

322
00:42:36,060 --> 00:42:47,620
not be the best basketball player, but he hits your head, I don't imagine meditators

323
00:42:47,620 --> 00:42:55,700
have the best reflexes, seeing, and tending the catch, and then that time we've already

324
00:42:55,700 --> 00:43:23,540
been hit in the face with the ball, alright, you know, thank you all for coming out tonight

325
00:43:23,540 --> 00:43:38,540
and I'm going to have a regular audience, see you all tomorrow, good night.

326
00:43:38,540 --> 00:43:54,820
Bye bye.

