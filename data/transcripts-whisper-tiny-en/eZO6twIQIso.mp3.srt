1
00:00:00,000 --> 00:00:07,200
What would happen if everybody on planet was a Buddhist?

2
00:00:07,200 --> 00:00:19,000
Would it be perfect peace or is it just impossible so we should just not to ask the question?

3
00:00:19,000 --> 00:00:36,000
This is always fun. Let's ask what if it is kind of a difficult question to ask because it's asking us to be speculative.

4
00:00:36,000 --> 00:00:39,000
And the only answer that we can give is from basic principles.

5
00:00:39,000 --> 00:00:51,000
Well we know that Buddhists are x, y and z and when a Buddhist or when many Buddhists get together they change the world in this way and this way and this way.

6
00:00:51,000 --> 00:01:05,000
So if we were to evolve people on the earth were Buddhists then these qualities would increase and qualities that are not espoused by Buddhists would decrease.

7
00:01:05,000 --> 00:01:16,000
Now of course the other thing you have to qualify is whether you're talking about people who call themselves Buddhists or people who actually follow the Buddhist teaching and then of course to what extent.

8
00:01:16,000 --> 00:01:25,000
If all beings were on earth were our hands it would be a very short lived species because that would be it.

9
00:01:25,000 --> 00:01:35,000
But of course that's ridiculous because then you have all of these as ridiculous how would you get all humans on earth to be in our hands.

10
00:01:35,000 --> 00:01:41,000
It's impossible for the reason that all of those people have karma with all of the other beings.

11
00:01:41,000 --> 00:01:46,000
So all of those other beings would have to be pulled up along with this person.

12
00:01:46,000 --> 00:01:55,000
And you'd end up with probably the destruction of life as we know it because you need all these insects and so on to keep the ecosystem going.

13
00:01:55,000 --> 00:02:00,000
It would be a radical shift it would just totally destroy the framework of the earth.

14
00:02:00,000 --> 00:02:04,000
The earth would maybe explode from the power of it.

15
00:02:04,000 --> 00:02:09,000
Really I think it could happen because in our hand it's a very powerful thing.

16
00:02:09,000 --> 00:02:18,000
The earth can shake. We have this story that we tell in our tradition. I was there on my teacher's 80th birthday he gave up the will to live.

17
00:02:18,000 --> 00:02:21,000
And then the earth shook that night.

18
00:02:21,000 --> 00:02:25,000
So imagine a world full of our hands.

19
00:02:25,000 --> 00:02:30,000
But people who are practicing the Buddhist teaching may not be practicing celibacy.

20
00:02:30,000 --> 00:02:38,000
If everyone was practicing the Buddhist teaching I still think you'd have troubles keeping the world going because

21
00:02:38,000 --> 00:02:42,000
it would be too much wholesome karma for the world to exist as it is.

22
00:02:42,000 --> 00:02:45,000
Like the world is full of good but also full of evil.

23
00:02:45,000 --> 00:02:51,000
It's half and half or maybe whatever 60, 40 whichever way you happen to think.

24
00:02:51,000 --> 00:02:56,000
And if you have too much good in the world the evil has to be broken apart.

25
00:02:56,000 --> 00:03:02,000
And the evil is very much a part of keeping the world in its certain current state of affairs.

26
00:03:02,000 --> 00:03:05,000
Now it kind of sounds like I'm saying there'd be a problem.

27
00:03:05,000 --> 00:03:09,000
It wouldn't really be a problem. It would just be a problem for the world system.

28
00:03:09,000 --> 00:03:13,000
The world would not be able to function the way it functions now.

29
00:03:13,000 --> 00:03:17,000
Because the world is very much dependent on evil.

30
00:03:17,000 --> 00:03:19,000
Evil deeds to function the way it does.

31
00:03:19,000 --> 00:03:22,000
Natural disasters are the result of evil deeds.

32
00:03:22,000 --> 00:03:25,000
Famine and so on.

33
00:03:25,000 --> 00:03:28,000
Insect bites. You could eat it. I don't know.

34
00:03:28,000 --> 00:03:31,000
It's quite debatable.

35
00:03:31,000 --> 00:03:34,000
Insect bites may not be karmically related.

36
00:03:34,000 --> 00:03:42,000
But it's easy to see that if you get dengue or something it might be karmically related.

37
00:03:42,000 --> 00:03:45,000
When you have big swollen insect bites.

38
00:03:45,000 --> 00:03:49,000
When you get bitten by a snake you can see it might be karmic.

39
00:03:49,000 --> 00:03:54,000
There was a story of a monk who happened to be friends with one of his friends in the past life.

40
00:03:54,000 --> 00:03:58,000
He became a snake and he was born as a human.

41
00:03:58,000 --> 00:04:05,000
He became a monk and he got totally fed up with being a monk and so he wanted to kill himself.

42
00:04:05,000 --> 00:04:11,000
He tried various different ways but he could never manage to cut his wrists or so on.

43
00:04:11,000 --> 00:04:16,000
Then he heard that the monk said they'd cut a poisonous snake.

44
00:04:16,000 --> 00:04:18,000
He said, oh, here's the chance.

45
00:04:18,000 --> 00:04:21,000
Where's the poisonous snake? We put it in a bucket over there.

46
00:04:21,000 --> 00:04:24,000
He went to the bucket and stuck his hand in the bucket.

47
00:04:24,000 --> 00:04:26,000
The snake wouldn't bite him.

48
00:04:26,000 --> 00:04:29,000
He'd pushed the snake and the snake still didn't bite him.

49
00:04:29,000 --> 00:04:31,000
He grabbed the snake and the snake still didn't bite him.

50
00:04:31,000 --> 00:04:34,000
He shook the bucket up and the snake still doesn't bite him.

51
00:04:34,000 --> 00:04:35,000
He threw it away.

52
00:04:35,000 --> 00:04:38,000
He went and said the monk said that wasn't a poisonous snake.

53
00:04:38,000 --> 00:04:41,000
That was just some ordinary grass snake.

54
00:04:41,000 --> 00:04:45,000
There all like that was a poisonous cobra or that was a poisonous snake.

55
00:04:45,000 --> 00:04:46,000
The slightest bite.

56
00:04:46,000 --> 00:04:49,000
He said, I put my hand in the bucket.

57
00:04:49,000 --> 00:04:53,000
He asked the Buddha and the Buddha said, oh, they were friends in the past life.

58
00:04:53,000 --> 00:04:55,000
That was his friend.

59
00:04:55,000 --> 00:04:59,000
So he could think snake bite could be karmic and so on.

60
00:04:59,000 --> 00:05:03,000
And without all of that, there was too much good karma.

61
00:05:03,000 --> 00:05:06,000
It all starts to break down.

62
00:05:06,000 --> 00:05:10,000
If you just mean, if all people in the world were to say they were Buddhist

63
00:05:10,000 --> 00:05:15,000
and to carry, to write it down on the census applications and so on,

64
00:05:15,000 --> 00:05:18,000
probably not much.

65
00:05:18,000 --> 00:05:21,000
Yeah, maybe it would be a nicer place

66
00:05:21,000 --> 00:05:26,000
and people would be thinking about good things. No, that would be a better place.

67
00:05:26,000 --> 00:05:28,000
That's something you could imagine.

68
00:05:28,000 --> 00:05:32,000
If the world were all said they were Buddhist, then it would be,

69
00:05:32,000 --> 00:05:34,000
I think that's feasible.

70
00:05:34,000 --> 00:05:37,000
It's still quite ridiculous, isn't it?

71
00:05:37,000 --> 00:05:39,000
But something you could at least imagine.

72
00:05:39,000 --> 00:05:40,000
And you could think, well, that'd be nice.

73
00:05:40,000 --> 00:05:43,000
Because then at least people would know what are the five precepts

74
00:05:43,000 --> 00:05:45,000
and they know what is meditation.

75
00:05:45,000 --> 00:05:48,000
And I mean, in this country, as kind of many people don't practice

76
00:05:48,000 --> 00:05:51,000
and in Buddhist countries, you see that people don't,

77
00:05:51,000 --> 00:05:54,000
but there's still something kind of wonderful.

78
00:05:54,000 --> 00:05:58,000
Because you don't have a problem talking to them about these things.

79
00:05:58,000 --> 00:06:02,000
Mostly they don't have very strong, wrong views.

80
00:06:02,000 --> 00:06:04,000
You do have a problem getting them to actually practice

81
00:06:04,000 --> 00:06:08,000
because they have so much right view that they think they know everything.

82
00:06:08,000 --> 00:06:13,000
They'll say to you, oh, yes, I don't practice, but, you know, we're Buddhist anyway.

83
00:06:13,000 --> 00:06:20,000
And, you know, deep in their minds, they think that they already know everything.

84
00:06:20,000 --> 00:06:24,000
And so it's hard to get them to change.

85
00:06:24,000 --> 00:06:26,000
Because they're already so right.

86
00:06:26,000 --> 00:06:28,000
That could be a problem.

87
00:06:28,000 --> 00:06:33,000
I think for the second question, would it be the perfect place?

88
00:06:33,000 --> 00:06:35,000
Certainly not.

89
00:06:35,000 --> 00:06:38,000
It would be a better place.

90
00:06:38,000 --> 00:06:42,000
But this earth cannot be perfect.

91
00:06:42,000 --> 00:06:45,000
I doubt that very much.

92
00:06:45,000 --> 00:06:53,000
And a Buddhist, not necessarily, is rid of a defilements.

93
00:06:53,000 --> 00:06:56,000
And as long as we have the defilements,

94
00:06:56,000 --> 00:07:01,000
Buddhist or not, we are doing stupid and bad things.

95
00:07:01,000 --> 00:07:06,000
So, yeah.

96
00:07:06,000 --> 00:07:12,000
I mean, you can think that the world is only made up of our experiences, right?

97
00:07:12,000 --> 00:07:17,000
And at least a good percentage of the majority of our experiences are remarkably caused.

98
00:07:17,000 --> 00:07:22,000
And so if without all of the bad karma, the clinging karma,

99
00:07:22,000 --> 00:07:26,000
we wouldn't have been born in this world in the first place.

100
00:07:26,000 --> 00:07:30,000
So it really depends on the degree of what you mean by Buddhism.

101
00:07:30,000 --> 00:07:37,000
But I think the Buddha would have discarded that question as of topic.

102
00:07:37,000 --> 00:07:42,000
Questions that lead not to edification.

103
00:07:42,000 --> 00:07:46,000
Although there are some interesting implications that I think we've gone over.

104
00:07:46,000 --> 00:08:09,000
It's fun to ponder ourselves.

