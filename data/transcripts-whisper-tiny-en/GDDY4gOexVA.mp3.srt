1
00:00:00,000 --> 00:00:02,560
Welcome back to Ask a Monk.

2
00:00:02,560 --> 00:00:08,400
Next question comes from Brad Cole to X.

3
00:00:08,400 --> 00:00:11,400
Dear Yutadamu, I started with Samatam meditation,

4
00:00:11,400 --> 00:00:13,760
as I read a lot that you have to calm your mind

5
00:00:13,760 --> 00:00:16,240
before starting to practice Vipasana.

6
00:00:16,240 --> 00:00:19,120
What is your interpretation of differences between these two

7
00:00:19,120 --> 00:00:21,520
and about which should be first?

8
00:00:21,520 --> 00:00:23,200
Thank you.

9
00:00:23,200 --> 00:00:25,520
Okay, Samatah and Vipasana.

10
00:00:25,520 --> 00:00:27,840
For those of you who don't know the word Samatam means

11
00:00:27,840 --> 00:00:32,480
tranquility, the word Vipasana means seeing clearly or insight.

12
00:00:34,400 --> 00:00:38,240
When you talk about Samatam meditation as something separate

13
00:00:38,240 --> 00:00:41,360
from Vipasana meditation, you're taking the discussion

14
00:00:41,360 --> 00:00:44,320
into a post-canonical realm.

15
00:00:44,320 --> 00:00:48,080
For those of you who are not into Buddhist terminology

16
00:00:48,080 --> 00:00:53,360
or early Buddhist terminology, it means you're taking it out

17
00:00:53,360 --> 00:00:55,520
of the realm of what the Buddha actually said

18
00:00:55,520 --> 00:00:57,920
and into the realm of interpretation.

19
00:00:57,920 --> 00:01:00,800
Now, I follow this interpretation, but only in the sense

20
00:01:00,800 --> 00:01:05,680
that it helps to explain the original words.

21
00:01:05,680 --> 00:01:07,680
So I don't mind talking about this,

22
00:01:07,680 --> 00:01:12,000
but just so that you know that from the Buddha's point of view,

23
00:01:12,000 --> 00:01:15,120
it doesn't seem at all clear that one should separate

24
00:01:15,120 --> 00:01:16,480
the two at all.

25
00:01:16,480 --> 00:01:21,840
And so in that sense, anyone saying that you should calm your mind

26
00:01:21,840 --> 00:01:25,280
before you practice Vipasana is on a little bit of

27
00:01:25,280 --> 00:01:26,880
shaky ground.

28
00:01:26,880 --> 00:01:29,680
Now, you can do it that way.

29
00:01:29,680 --> 00:01:33,120
And there's certainly indication that this is how people meditate

30
00:01:33,120 --> 00:01:35,120
it even in the Buddha's time.

31
00:01:35,120 --> 00:01:39,360
But what that means in a technical sense, if you're following

32
00:01:39,360 --> 00:01:45,760
this dichotomy, the dualistic, or I don't know,

33
00:01:45,760 --> 00:01:48,800
the two kinds of meditation, one being Samatam,

34
00:01:48,800 --> 00:01:53,280
one being Vipasana, then the technique is to not just

35
00:01:53,280 --> 00:01:56,240
spend some time calming your mind, that's not what it means.

36
00:01:56,240 --> 00:01:58,880
And for those people who say, OK, practice calming your mind

37
00:01:58,880 --> 00:02:03,120
and then practice, it means entering into a trance,

38
00:02:03,120 --> 00:02:07,120
fixing and focusing your mind so that you can see one thing

39
00:02:07,120 --> 00:02:08,560
very clearly.

40
00:02:08,560 --> 00:02:10,840
And the only way you can do that is to pick a concept.

41
00:02:10,840 --> 00:02:12,560
And this is why I'm always talking about the differences

42
00:02:12,560 --> 00:02:16,040
between concepts and reality, because in Vipasana practice,

43
00:02:16,040 --> 00:02:19,840
we focus on reality in tranquility meditation.

44
00:02:19,840 --> 00:02:23,000
And there are these kinds of meditation all over the world.

45
00:02:23,000 --> 00:02:26,280
They'll find them, and this is a useful classification.

46
00:02:26,280 --> 00:02:27,720
That's why I follow these teachings,

47
00:02:27,720 --> 00:02:30,840
because it's useful for us to say, that's not Vipasana

48
00:02:30,840 --> 00:02:31,440
meditation.

49
00:02:31,440 --> 00:02:33,320
That meditation is not going to help you see clearly.

50
00:02:33,320 --> 00:02:36,000
And we can say exactly why that is, because what you're

51
00:02:36,000 --> 00:02:38,640
focusing on is something you've created in your mind.

52
00:02:38,640 --> 00:02:39,760
It's not real.

53
00:02:39,760 --> 00:02:43,360
It doesn't have any of the qualities of ultimate reality.

54
00:02:43,360 --> 00:02:46,360
It's not the same as watching your stomach rise and fall.

55
00:02:46,360 --> 00:02:46,640
Why?

56
00:02:46,640 --> 00:02:48,680
Because when you watch your stomach rise and fall,

57
00:02:48,680 --> 00:02:49,840
it's changing all the time.

58
00:02:49,840 --> 00:02:55,960
It's unsatisfying. It helps you to see an objective reality.

59
00:02:55,960 --> 00:02:58,440
It helps you see things from an objective point of view.

60
00:02:58,440 --> 00:02:59,680
You're not creating this.

61
00:02:59,680 --> 00:03:00,600
You're not altering it.

62
00:03:00,600 --> 00:03:01,760
You're seeing it the way it is.

63
00:03:01,760 --> 00:03:03,600
And that's difficult.

64
00:03:03,600 --> 00:03:05,920
It's a lot easier to create something in your mind.

65
00:03:05,920 --> 00:03:08,040
So you have a color here.

66
00:03:08,040 --> 00:03:08,720
Close your eyes.

67
00:03:08,720 --> 00:03:11,440
And in your third eye, you create a color,

68
00:03:11,440 --> 00:03:13,520
or you focus on the chakras or whatever.

69
00:03:13,520 --> 00:03:16,400
There's many kinds of meditation.

70
00:03:16,400 --> 00:03:17,560
And so you focus on the color.

71
00:03:17,560 --> 00:03:22,120
There's a blue, blue, blue, or red, red, or white, or whatever.

72
00:03:22,120 --> 00:03:26,080
Some people will focus on a light, or many different kinds

73
00:03:26,080 --> 00:03:28,000
of meditation like this.

74
00:03:28,000 --> 00:03:30,400
You create that, and you focus your mind on it.

75
00:03:30,400 --> 00:03:35,600
Once you can fix your mind only on that object.

76
00:03:35,600 --> 00:03:37,520
So you're thinking of nothing else.

77
00:03:37,520 --> 00:03:40,200
And you're just totally fixed on that object.

78
00:03:40,200 --> 00:03:44,920
That's considered to be an attainment of summit meditation.

79
00:03:44,920 --> 00:03:47,840
You're actually, you've tranquilized your mind

80
00:03:47,840 --> 00:03:50,880
to a level of attainment.

81
00:03:50,880 --> 00:03:53,680
This is useful in many religious traditions,

82
00:03:53,680 --> 00:03:56,880
especially in India, to gain magical powers.

83
00:03:56,880 --> 00:03:59,040
To remember your past lives, to read people's minds,

84
00:03:59,040 --> 00:04:02,560
to see heaven and hell, to see the future, whatever.

85
00:04:02,560 --> 00:04:03,920
Lots of crazy things can happen.

86
00:04:03,920 --> 00:04:05,400
You can leave your body.

87
00:04:05,400 --> 00:04:08,760
These things are talked about among meditators.

88
00:04:08,760 --> 00:04:11,960
Now, you believe it or not, try it for yourself.

89
00:04:11,960 --> 00:04:14,640
It's not what I teach, but it's certainly out there.

90
00:04:14,640 --> 00:04:18,800
And there are manuals on how to practice it.

91
00:04:18,800 --> 00:04:22,320
Once you've done that, if you want to take the summit

92
00:04:22,320 --> 00:04:25,080
to first leave us in a later track,

93
00:04:25,080 --> 00:04:29,160
then what you do is you take that focused state,

94
00:04:29,160 --> 00:04:32,200
and you start to apply the principles that I talk about

95
00:04:32,200 --> 00:04:33,040
in my videos.

96
00:04:33,040 --> 00:04:36,000
So your mind is very focused.

97
00:04:36,000 --> 00:04:36,680
You acknowledge that.

98
00:04:36,680 --> 00:04:37,960
You said you're so focused.

99
00:04:37,960 --> 00:04:38,720
You feel happy?

100
00:04:38,720 --> 00:04:39,280
You acknowledge that.

101
00:04:39,280 --> 00:04:39,720
Happy.

102
00:04:39,720 --> 00:04:40,320
You feel calm.

103
00:04:40,320 --> 00:04:40,840
You acknowledge that.

104
00:04:40,840 --> 00:04:42,160
Calm, calm, calm.

105
00:04:42,160 --> 00:04:44,400
You see the light or the color.

106
00:04:44,400 --> 00:04:46,880
You say to yourself, seeing, seeing, seeing.

107
00:04:46,880 --> 00:04:48,480
And what's going to happen is it's

108
00:04:48,480 --> 00:04:51,680
going to change from a stable, solid state

109
00:04:51,680 --> 00:04:53,320
to something that's changing.

110
00:04:53,320 --> 00:04:58,280
This helps you to see that even the most stable,

111
00:04:58,280 --> 00:05:00,240
at that point, your mind is so refined

112
00:05:00,240 --> 00:05:02,640
that you can see things incredibly clearly.

113
00:05:02,640 --> 00:05:04,440
When you say to yourself, seeing, seeing, seeing,

114
00:05:04,440 --> 00:05:05,760
you start to get it, and you realize

115
00:05:05,760 --> 00:05:07,440
that this is the nature of reality,

116
00:05:07,440 --> 00:05:09,880
that everything is changing, that nothing is satisfying,

117
00:05:09,880 --> 00:05:12,320
that the things are not under your control,

118
00:05:12,320 --> 00:05:15,320
that they are not self, they are not soul, and so on.

119
00:05:15,320 --> 00:05:18,200
And so you are able to let go of them.

120
00:05:18,200 --> 00:05:20,440
You're able to see your attachments,

121
00:05:20,440 --> 00:05:22,760
and you're able to change the way you think about things.

122
00:05:22,760 --> 00:05:27,880
So because your mind is so clear from the focused state,

123
00:05:27,880 --> 00:05:31,760
once you apply Vipassana meditation on that state,

124
00:05:31,760 --> 00:05:35,600
it's said to be very easy to attain Vipassana insight.

125
00:05:35,600 --> 00:05:39,360
As opposed to someone who doesn't take the time to do that,

126
00:05:39,360 --> 00:05:42,720
the differences are, if you've been practicing the meditation

127
00:05:42,720 --> 00:05:45,120
I teach, it can be more difficult.

128
00:05:46,400 --> 00:05:49,840
Difficult in the sense of painful,

129
00:05:49,840 --> 00:05:51,040
because your mind does not focus,

130
00:05:51,040 --> 00:05:54,800
so you're wandering all over and you have to really,

131
00:05:57,240 --> 00:05:59,880
you have to be really strong to be able to take the pain

132
00:05:59,880 --> 00:06:02,360
and the body of the distractions in the mind,

133
00:06:02,360 --> 00:06:04,480
the emotions as they come up.

134
00:06:04,480 --> 00:06:07,560
But the result is the same, and of the two,

135
00:06:07,560 --> 00:06:09,120
we prefer this one that I teach,

136
00:06:09,120 --> 00:06:12,920
because it's easier for people in your daily life

137
00:06:12,920 --> 00:06:16,920
to practice, it's a way to get started right away.

138
00:06:16,920 --> 00:06:20,560
And it certainly is clear that the Buddha didn't have

139
00:06:20,560 --> 00:06:24,600
preference for an incredibly structured approach,

140
00:06:24,600 --> 00:06:28,520
except in cases where people were already well developed

141
00:06:28,520 --> 00:06:31,040
and practicing as monks in the forest.

142
00:06:31,040 --> 00:06:32,840
It's pretty clear that in general,

143
00:06:32,840 --> 00:06:35,640
he taught a more dynamic meditation

144
00:06:35,640 --> 00:06:37,320
where when you're walking, you know you're walking,

145
00:06:37,320 --> 00:06:38,800
when you're sitting, you know you're sitting,

146
00:06:38,800 --> 00:06:39,640
and so on.

147
00:06:41,320 --> 00:06:45,160
But there are many, many different types of meditation

148
00:06:45,160 --> 00:06:47,480
that the Buddha taught, many different approaches,

149
00:06:47,480 --> 00:06:51,160
generally structured for the individual.

150
00:06:51,160 --> 00:06:54,520
So the Buddha could see, he was a very special person

151
00:06:54,520 --> 00:06:58,920
who could see people's their strengths and weaknesses

152
00:06:58,920 --> 00:07:01,960
and see what they needed to go further.

153
00:07:01,960 --> 00:07:04,080
And so he would give them meditation specific

154
00:07:04,080 --> 00:07:05,240
to their needs.

155
00:07:05,240 --> 00:07:07,440
And that causes a lot of disagreement

156
00:07:07,440 --> 00:07:08,920
over what actually the Buddha taught.

157
00:07:08,920 --> 00:07:12,920
But the most common teaching that the Buddha gave

158
00:07:12,920 --> 00:07:17,440
and by far the most core, the most essential

159
00:07:17,440 --> 00:07:19,680
the Buddha's teaching is a focus on reality,

160
00:07:19,680 --> 00:07:21,840
the physical and the mental,

161
00:07:21,840 --> 00:07:23,240
it's called the five aggregates,

162
00:07:23,240 --> 00:07:25,000
if you know anything about that.

163
00:07:25,000 --> 00:07:27,800
And focusing and doing that by using

164
00:07:27,800 --> 00:07:30,800
the four foundations of mindfulness as we teach them,

165
00:07:30,800 --> 00:07:35,040
the body, the feelings, the mind and the dhammas

166
00:07:35,040 --> 00:07:37,960
or the various groups of teaching that have to do

167
00:07:37,960 --> 00:07:39,400
with meditation.

168
00:07:39,400 --> 00:07:42,920
So your question as to whether

169
00:07:45,000 --> 00:07:48,080
what should be first, it's totally up to you.

170
00:07:48,080 --> 00:07:50,760
But if you're going to practice some at the first

171
00:07:50,760 --> 00:07:52,960
in the sense that I explained it,

172
00:07:54,720 --> 00:07:58,160
you really need a good teacher to lead you through it.

173
00:07:58,160 --> 00:08:03,160
Either that or at worst get a manual that teaches it.

174
00:08:03,160 --> 00:08:06,200
The best one that I know of is the Visudhi Magha,

175
00:08:06,200 --> 00:08:08,360
which is probably the oldest as well.

176
00:08:09,440 --> 00:08:12,000
The path of purification is the translation.

177
00:08:13,240 --> 00:08:16,120
And it goes through all of those types of meditation

178
00:08:16,120 --> 00:08:18,880
and all of the fun magical powers that can come from it.

179
00:08:18,880 --> 00:08:22,200
But don't sue me if you go crazy following them on your own

180
00:08:22,200 --> 00:08:24,240
because I don't teach those things.

181
00:08:24,240 --> 00:08:28,120
Anyway, it's just for your edification.

182
00:08:28,120 --> 00:08:32,880
The difference between the two is quite,

183
00:08:32,880 --> 00:08:35,560
I think, quite profound, one of them.

184
00:08:35,560 --> 00:08:36,920
And you don't hear this a lot.

185
00:08:36,920 --> 00:08:39,120
The samata meditation and Vipasana meditation.

186
00:08:39,120 --> 00:08:39,960
What is the difference?

187
00:08:39,960 --> 00:08:42,680
So I think it should be clear based on what I've said.

188
00:08:42,680 --> 00:08:46,160
Samata meditation is for the purpose of calming the mind.

189
00:08:46,160 --> 00:08:49,240
Vipasana meditation is for the purpose of gaining insight.

190
00:08:49,240 --> 00:08:52,560
It doesn't mean that Vipasana meditation

191
00:08:52,560 --> 00:08:54,520
is not going to lead to tranquility.

192
00:08:54,520 --> 00:08:57,600
And in fact, if you practice the meditation

193
00:08:57,600 --> 00:09:00,640
according to the videos that I teach in this method

194
00:09:00,640 --> 00:09:01,880
as is taught elsewhere,

195
00:09:03,160 --> 00:09:04,880
you will find that your mind comes down.

196
00:09:04,880 --> 00:09:06,160
You'll find that you're much calmer,

197
00:09:06,160 --> 00:09:08,400
you're much stronger as an individual

198
00:09:08,400 --> 00:09:10,440
and your mind is clearer.

199
00:09:12,560 --> 00:09:15,160
The point is it's not called samata meditation

200
00:09:15,160 --> 00:09:16,880
because that's not the goal.

201
00:09:16,880 --> 00:09:19,000
Whether you feel calm or not,

202
00:09:19,000 --> 00:09:20,600
it's not the most important part.

203
00:09:20,600 --> 00:09:23,040
The important part is that you gain insight.

204
00:09:23,040 --> 00:09:25,880
That's why it's given a special name of Vipasana meditation.

205
00:09:25,880 --> 00:09:28,760
People say, what are you doing calling this Vipasana

206
00:09:28,760 --> 00:09:30,760
and saying, that's not Vipasana, that's samata.

207
00:09:30,760 --> 00:09:32,280
How can you separate them?

208
00:09:32,280 --> 00:09:34,160
We're separating them based on the goal.

209
00:09:34,160 --> 00:09:36,680
Samata meditation, these meditations exist.

210
00:09:36,680 --> 00:09:38,800
There's no question about it that there are meditations

211
00:09:38,800 --> 00:09:41,160
out there that will not lead you to enlightenment.

212
00:09:41,160 --> 00:09:44,000
They can't because they're not focused on reality.

213
00:09:44,000 --> 00:09:46,280
They're creating an illusion in the mind.

214
00:09:47,560 --> 00:09:49,440
The only way that they could lead to enlightenment

215
00:09:49,440 --> 00:09:51,720
is as I said, if you use that to gain insight,

216
00:09:51,720 --> 00:09:54,800
if you use that to then look at it in a way similar to

217
00:09:54,800 --> 00:09:58,080
how we do it here as you would anything else

218
00:09:58,080 --> 00:09:59,680
and because of the strength of mind,

219
00:09:59,680 --> 00:10:02,720
you can see it clearer than you would have otherwise.

220
00:10:02,720 --> 00:10:05,560
So it's kind of taking a shortcut,

221
00:10:05,560 --> 00:10:08,160
but having to do a lot of preparation,

222
00:10:08,160 --> 00:10:12,320
so not gaining anything except for these nice states

223
00:10:12,320 --> 00:10:15,560
of peace and calm and maybe some magical powers along the way,

224
00:10:17,120 --> 00:10:19,240
which is probably best suited towards

225
00:10:19,240 --> 00:10:21,080
for someone living in the forest.

226
00:10:21,080 --> 00:10:26,080
So, what should be first, totally up to you?

227
00:10:27,240 --> 00:10:29,560
If you want to go that way, I talked about that.

228
00:10:31,040 --> 00:10:33,640
And if you want to start with just be passionate,

229
00:10:33,640 --> 00:10:37,000
it's very clear that this was the way the Buddha,

230
00:10:38,360 --> 00:10:40,760
the Buddha gave this as the quickest way.

231
00:10:40,760 --> 00:10:42,720
Someone among came up to the Buddha and said,

232
00:10:42,720 --> 00:10:46,480
look, I'm old and I don't have a lot of time

233
00:10:46,480 --> 00:10:49,080
and my memory's not good, I don't wanna learn a lot.

234
00:10:49,080 --> 00:10:51,280
Give me the basics of the path.

235
00:10:51,280 --> 00:10:54,840
And the Buddha said, clear your view,

236
00:10:54,840 --> 00:10:57,000
get an understanding of what is the truth.

237
00:10:57,000 --> 00:10:59,520
And this is just an intellectual understanding.

238
00:10:59,520 --> 00:11:02,320
So learn a little bit about what is suffering,

239
00:11:02,320 --> 00:11:04,080
what is the cause of suffering,

240
00:11:04,080 --> 00:11:06,120
what is the way out of suffering and so on,

241
00:11:06,120 --> 00:11:08,080
from a technical point of view.

242
00:11:11,080 --> 00:11:13,960
And purify your precepts, meaning purify your morality,

243
00:11:13,960 --> 00:11:17,480
meaning don't kill, think of vows not to kill,

244
00:11:17,480 --> 00:11:19,120
not to steal, not to cheat, not to lie,

245
00:11:19,120 --> 00:11:20,920
not to take drugs and alcohol.

246
00:11:20,920 --> 00:11:23,960
Purify your conduct, make sure you're ethical

247
00:11:23,960 --> 00:11:26,160
and moral in your behavior.

248
00:11:26,160 --> 00:11:28,280
And then start practicing the four foundations

249
00:11:28,280 --> 00:11:29,120
of mindfulness.

250
00:11:29,120 --> 00:11:33,920
This is clear, there's no talk about first focusing

251
00:11:33,920 --> 00:11:36,600
your mind entering into any state.

252
00:11:36,600 --> 00:11:39,000
You enter into these states naturally

253
00:11:39,000 --> 00:11:41,440
and you enter into them based on reality.

254
00:11:41,440 --> 00:11:43,800
So even just focusing on the rising and falling,

255
00:11:43,800 --> 00:11:49,680
you can enter into an absorbed state for the moment

256
00:11:49,680 --> 00:11:52,320
that you're watching it when you say step being right,

257
00:11:52,320 --> 00:11:54,080
step being left when you're walking meditation.

258
00:11:54,080 --> 00:11:56,520
You can enter into an absorbed state for that moment

259
00:11:56,520 --> 00:11:59,560
and that allows you to see that reality clearly.

260
00:11:59,560 --> 00:12:02,120
So in that sense, you're practicing both at once.

261
00:12:02,120 --> 00:12:07,120
You're also tranquil and you're also seeing clearly

262
00:12:09,200 --> 00:12:10,920
which is the point.

263
00:12:10,920 --> 00:12:14,240
Okay, hope that helps, thanks for the question, keep practicing.

