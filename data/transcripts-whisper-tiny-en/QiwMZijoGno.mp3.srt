1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with first number 127, which reads as follows.

3
00:00:12,000 --> 00:00:21,000
Nantarikheh nasamudamajheh napapata nangvivarangpavisa.

4
00:00:21,000 --> 00:00:35,000
Navijitheh so shagatibh padeh so yatatitomu jaya papakamma.

5
00:00:35,000 --> 00:01:00,000
Which means not up in the sky or in the middle of the ocean, Nantarikheh nasamudamajheh.

6
00:01:00,000 --> 00:01:09,000
Napapata nangvivarangpavisa, not having entered into a cave, a cavern in a mountain,

7
00:01:09,000 --> 00:01:16,000
entered into the middle of a mountain, in the center of a mountain.

8
00:01:16,000 --> 00:01:27,000
Navijitheh so shagatibh padeh so it cannot be found such a place on this earth.

9
00:01:27,000 --> 00:01:33,000
One cannot find a place on this earth, on the earth.

10
00:01:33,000 --> 00:01:38,000
Yatatitomuir standing mutjaya papakamma.

11
00:01:38,000 --> 00:01:45,000
One might be free from evil karma, evil deal.

12
00:01:45,000 --> 00:01:51,000
There's nowhere, not up in the sky, not under the ocean,

13
00:01:51,000 --> 00:01:56,000
not in the deepest, darkest cave.

14
00:01:56,000 --> 00:01:58,000
There's nowhere on earth.

15
00:01:58,000 --> 00:02:02,000
That's the quote.

16
00:02:02,000 --> 00:02:07,000
So an interesting story, actually three stories,

17
00:02:07,000 --> 00:02:11,000
that goes with this verse.

18
00:02:11,000 --> 00:02:16,000
It seems there were three groups of monks,

19
00:02:16,000 --> 00:02:20,000
and so we have three stories.

20
00:02:20,000 --> 00:02:25,000
The Buddha was living in Jetawana, but

21
00:02:25,000 --> 00:02:30,000
a first group of monks set out to meet the Buddha,

22
00:02:30,000 --> 00:02:34,000
and on their way they entered a village, a certain village for alms,

23
00:02:34,000 --> 00:02:42,000
and while they were sitting, or after they'd eaten,

24
00:02:42,000 --> 00:02:46,000
or while they were waiting for food,

25
00:02:46,000 --> 00:02:51,000
someone who was cooking food in the morning

26
00:02:51,000 --> 00:02:56,000
made their fire too hot and suddenly the fire flame burst up

27
00:02:56,000 --> 00:03:02,000
and lit the satch, roof of their hut,

28
00:03:02,000 --> 00:03:07,000
and the satch went flying up in a ball of flame,

29
00:03:07,000 --> 00:03:11,000
or floating through the air,

30
00:03:11,000 --> 00:03:14,000
being carried away by the wind.

31
00:03:14,000 --> 00:03:17,000
And at that moment a crow was flying,

32
00:03:17,000 --> 00:03:24,000
and so this tuft of thatch caught the crow,

33
00:03:24,000 --> 00:03:27,000
and immediately burst into flames.

34
00:03:27,000 --> 00:03:30,000
It was on fire, so the crow burnt to a crisp,

35
00:03:30,000 --> 00:03:35,000
fell to the ground dead right in front of the monks.

36
00:03:35,000 --> 00:03:38,000
And they thought it was kind of remarkable

37
00:03:38,000 --> 00:03:42,000
because just one tuft of thatch flew up

38
00:03:42,000 --> 00:03:45,000
and caught the crow squarely

39
00:03:45,000 --> 00:03:50,000
as it was flying by.

40
00:03:50,000 --> 00:03:55,000
And so they wondered to themselves sort of monk talk,

41
00:03:55,000 --> 00:03:59,000
wonder what the karma of this bird was?

42
00:03:59,000 --> 00:04:00,000
That this would happen.

43
00:04:00,000 --> 00:04:03,000
And they said, who would know besides the Buddha?

44
00:04:03,000 --> 00:04:09,000
So when we get there we should ask him.

45
00:04:09,000 --> 00:04:11,000
And so they continued on their way.

46
00:04:11,000 --> 00:04:14,000
That's the first group.

47
00:04:14,000 --> 00:04:18,000
The second group of monks also on their way to see the Buddha.

48
00:04:18,000 --> 00:04:20,000
I mean, it seems like probably this was a thing

49
00:04:20,000 --> 00:04:22,000
where monks would come to the Buddha

50
00:04:22,000 --> 00:04:24,000
with these sorts of questions.

51
00:04:24,000 --> 00:04:26,000
So I think we shouldn't be surprised

52
00:04:26,000 --> 00:04:27,000
that there were three groups of them.

53
00:04:27,000 --> 00:04:30,000
The second group, on their way to see the Buddha,

54
00:04:30,000 --> 00:04:35,000
they took a boat across the ocean somehow,

55
00:04:36,000 --> 00:04:37,000
not sure where they were.

56
00:04:37,000 --> 00:04:38,000
Maybe they were in Burma,

57
00:04:38,000 --> 00:04:41,000
or maybe Thailand.

58
00:04:41,000 --> 00:04:43,000
Maybe they were in Sri Lanka,

59
00:04:43,000 --> 00:04:47,000
who knows, but probably not.

60
00:04:47,000 --> 00:04:51,000
It's interesting to think of them being on a ship

61
00:04:51,000 --> 00:04:57,000
because there was not much record of monks outside of India,

62
00:04:57,000 --> 00:05:04,000
but I am really not clear about that sort of thing.

63
00:05:04,000 --> 00:05:08,000
But, happened that in the middle of the ocean

64
00:05:08,000 --> 00:05:13,000
their boat stopped, the wind stopped,

65
00:05:13,000 --> 00:05:15,000
and they couldn't sail anymore.

66
00:05:15,000 --> 00:05:18,000
And this was a thing for sailors

67
00:05:18,000 --> 00:05:23,000
that if the wind stopped for a long period of time,

68
00:05:23,000 --> 00:05:25,000
they would get superstitious.

69
00:05:25,000 --> 00:05:27,000
And they would think somebody on board

70
00:05:27,000 --> 00:05:31,000
is their karma is not allowing us to continue.

71
00:05:31,000 --> 00:05:34,000
And so they'd throw that person overboard

72
00:05:34,000 --> 00:05:36,000
if they could figure out what it was.

73
00:05:36,000 --> 00:05:40,000
And the way they did it was they found the scientific method.

74
00:05:40,000 --> 00:05:42,000
They'd draw lots.

75
00:05:42,000 --> 00:05:44,000
And whoever drew the shortest straw

76
00:05:44,000 --> 00:05:46,000
would get thrown overboard.

77
00:05:46,000 --> 00:05:49,000
It seems reasonable, no?

78
00:05:49,000 --> 00:05:51,000
So they did this.

79
00:05:51,000 --> 00:05:54,000
And we thought, well, whoever gets the shortest straw

80
00:05:54,000 --> 00:05:58,000
gets thrown over, that's just the way karma works.

81
00:05:58,000 --> 00:05:59,000
Guess.

82
00:06:02,000 --> 00:06:05,000
Except lo and behold,

83
00:06:05,000 --> 00:06:07,000
the captain's wife drew the lot.

84
00:06:07,000 --> 00:06:10,000
Now the captain's wife was loved by everyone.

85
00:06:10,000 --> 00:06:14,000
She was young, she was pretty, she was kind,

86
00:06:14,000 --> 00:06:17,000
she was great, just all around.

87
00:06:17,000 --> 00:06:20,000
Not the sort of person you want to throw overboard

88
00:06:20,000 --> 00:06:24,000
to say at least, especially since she was the captain's wife.

89
00:06:24,000 --> 00:06:28,000
And so everyone agreed they couldn't throw her overboard.

90
00:06:28,000 --> 00:06:31,000
So they said, well, we'll draw lots again.

91
00:06:31,000 --> 00:06:36,000
Again, they drew lots, and again, for a second time,

92
00:06:36,000 --> 00:06:39,000
the captain's wife drew the shortest straw.

93
00:06:44,000 --> 00:06:47,000
And they said, there's no way we can't do this.

94
00:06:47,000 --> 00:06:50,000
And so a third time they drew straws,

95
00:06:50,000 --> 00:06:54,000
and a third time the captain's wife drew the shortest straw.

96
00:06:54,000 --> 00:07:02,000
So they went to the captain and they said,

97
00:07:02,000 --> 00:07:06,000
this is what happened three times,

98
00:07:06,000 --> 00:07:10,000
it's got to be her, she's the one with a bad luck.

99
00:07:10,000 --> 00:07:13,000
You have to throw her overboard.

100
00:07:13,000 --> 00:07:16,000
And so they grabbed her, then the captain said,

101
00:07:16,000 --> 00:07:18,000
well then, yes, I guess that's how it has to go.

102
00:07:18,000 --> 00:07:22,000
We've scientifically shown that she's the one,

103
00:07:22,000 --> 00:07:25,000
so she's the blame for the wind.

104
00:07:25,000 --> 00:07:31,000
And so they said throw her overboard.

105
00:07:31,000 --> 00:07:33,000
And as they started to throw overboard,

106
00:07:33,000 --> 00:07:38,000
they, she started screaming reasonably.

107
00:07:38,000 --> 00:07:42,000
She doesn't seem to be that confident in the scientific method

108
00:07:42,000 --> 00:07:45,000
that they used, but it certainly doesn't seem to have wanted

109
00:07:45,000 --> 00:07:49,000
to be thrown overboard.

110
00:07:49,000 --> 00:07:56,000
So interestingly, the captain has them take her jewels away.

111
00:07:56,000 --> 00:07:58,000
He says, well there's no, there's no need to.

112
00:07:58,000 --> 00:08:01,000
When he heard this, he saw and he said, oh, there's no need to.

113
00:08:01,000 --> 00:08:03,000
Or for her jewels to go to waste.

114
00:08:03,000 --> 00:08:06,000
So he took her jewels and had them wrap her up in a cloth

115
00:08:06,000 --> 00:08:11,000
and tie a rope around her neck so that she couldn't,

116
00:08:11,000 --> 00:08:14,000
or wrap her up in a cloth so that she wouldn't scream.

117
00:08:14,000 --> 00:08:17,000
And then tie a rope around her neck and tie it

118
00:08:17,000 --> 00:08:25,000
in a big, heavy pot of sand so that they wouldn't see her,

119
00:08:25,000 --> 00:08:27,000
so that he wouldn't have to see her,

120
00:08:27,000 --> 00:08:33,000
because he was fond of her.

121
00:08:33,000 --> 00:08:34,000
And so they throw overboard.

122
00:08:34,000 --> 00:08:37,000
And as soon as she hit the ocean,

123
00:08:37,000 --> 00:08:40,000
sharks and turtles and fish and so on,

124
00:08:40,000 --> 00:08:44,000
ate her tore her bits and she died.

125
00:08:44,000 --> 00:08:47,000
And the monks on board were watching this,

126
00:08:47,000 --> 00:08:50,000
and of course didn't really have a say in it all.

127
00:08:50,000 --> 00:08:53,000
But they were shocked as well.

128
00:08:53,000 --> 00:08:56,000
They couldn't believe that this sort of thing could happen

129
00:08:56,000 --> 00:09:00,000
that she could really be at the mercy of these people

130
00:09:00,000 --> 00:09:04,000
because it was a bit of a coincidence that she drew

131
00:09:04,000 --> 00:09:07,000
the short straw three times.

132
00:09:07,000 --> 00:09:11,000
That's quite, unless there were only a few people on board

133
00:09:11,000 --> 00:09:14,000
to consider.

134
00:09:14,000 --> 00:09:17,000
And so they said, I wonder what karma she did

135
00:09:17,000 --> 00:09:20,000
to deserve such a horrible fate.

136
00:09:20,000 --> 00:09:29,000
And likewise, they said, well, that's asked the Buddha

137
00:09:29,000 --> 00:09:35,000
to find out.

138
00:09:35,000 --> 00:09:37,000
It's important to point out, as we go along,

139
00:09:37,000 --> 00:09:39,000
because I'm sure the question coming up

140
00:09:39,000 --> 00:09:42,000
was minds is, well, what about the people who did that to her?

141
00:09:42,000 --> 00:09:46,000
I mean, it's not karma, it's those people.

142
00:09:46,000 --> 00:09:49,000
And it has to be mentioned that karma

143
00:09:49,000 --> 00:09:51,000
isn't like one thing in the past.

144
00:09:51,000 --> 00:09:53,000
You blame things in your past life.

145
00:09:53,000 --> 00:09:54,000
That's not true at all.

146
00:09:54,000 --> 00:09:58,000
Those people who threw that woman into the ocean

147
00:09:58,000 --> 00:10:00,000
did a very, very bad thing.

148
00:10:00,000 --> 00:10:02,000
There's no question about it.

149
00:10:02,000 --> 00:10:03,000
That was an evil deed.

150
00:10:03,000 --> 00:10:05,000
Buddhism doesn't condone that.

151
00:10:05,000 --> 00:10:08,000
But how she got herself in that situation,

152
00:10:08,000 --> 00:10:19,000
you see, where the likelihood of her being subject to that,

153
00:10:19,000 --> 00:10:26,000
because these people were not doing it out of hate for her.

154
00:10:26,000 --> 00:10:30,000
They were doing it out of ignorance and superstition.

155
00:10:30,000 --> 00:10:36,000
But they didn't just randomly pick someone.

156
00:10:36,000 --> 00:10:39,000
So how did she get herself in that situation?

157
00:10:39,000 --> 00:10:42,000
The theory is that there's more behind it.

158
00:10:42,000 --> 00:10:46,000
How we are life comes to these points.

159
00:10:46,000 --> 00:10:53,000
Anyway, that was the second one.

160
00:10:53,000 --> 00:10:59,000
The third story, there were seven monks

161
00:10:59,000 --> 00:11:01,000
who likewise set out to see the Buddha.

162
00:11:01,000 --> 00:11:07,000
And on their way, they came to a certain monastery

163
00:11:07,000 --> 00:11:10,000
and they asked to stay the night.

164
00:11:10,000 --> 00:11:13,000
And the seven of them were invited to stay

165
00:11:13,000 --> 00:11:17,000
in a special cave in the side of the mountain.

166
00:11:17,000 --> 00:11:21,000
That was designated for visiting monks.

167
00:11:21,000 --> 00:11:24,000
And so they went there.

168
00:11:24,000 --> 00:11:28,000
And they settled down when they fell asleep for the night.

169
00:11:28,000 --> 00:11:32,000
During the night, a huge boulder, it says the size of a pagoda,

170
00:11:32,000 --> 00:11:37,000
which would be very, very large, fell down the mountain

171
00:11:37,000 --> 00:11:43,000
and covered the entrance to the cave where they were staying.

172
00:11:43,000 --> 00:11:50,000
Just out of the blue, blocking their exit,

173
00:11:50,000 --> 00:11:58,000
making them possible for them to get out.

174
00:11:58,000 --> 00:12:01,000
When the resident monks found out what happened,

175
00:12:01,000 --> 00:12:04,000
they said, we've got to move that rock.

176
00:12:04,000 --> 00:12:06,000
There's monks trapped in there.

177
00:12:06,000 --> 00:12:12,000
And so they gathered men, strong people from all around the countryside,

178
00:12:12,000 --> 00:12:18,000
and they worked tirelessly for seven days

179
00:12:18,000 --> 00:12:22,000
to remove this rock with the rock wooden budge.

180
00:12:22,000 --> 00:12:26,000
Until finally, on the seventh day, after seven days,

181
00:12:26,000 --> 00:12:30,000
the rock moved as though it had never been stuck there.

182
00:12:30,000 --> 00:12:32,000
The rock moved very easily.

183
00:12:32,000 --> 00:12:36,000
I think it even says that it moved by itself,

184
00:12:36,000 --> 00:12:39,000
away from the entrance.

185
00:12:39,000 --> 00:12:46,000
Just suddenly became dislodged.

186
00:12:46,000 --> 00:12:49,000
And so these seven monks had spent seven days

187
00:12:49,000 --> 00:12:53,000
without food, without water, were almost dead.

188
00:12:53,000 --> 00:12:58,000
And yet when they came out, they were able to get water and food and survive.

189
00:12:58,000 --> 00:13:03,000
But they said to themselves, wonder what we did.

190
00:13:03,000 --> 00:13:06,000
It seems very strange sort of thing to happen.

191
00:13:06,000 --> 00:13:09,000
I wonder if this is a cause of past karma.

192
00:13:09,000 --> 00:13:12,000
And so likewise, they decided to ask the Buddha.

193
00:13:12,000 --> 00:13:17,000
So these three groups of monks met up, and this is the story.

194
00:13:17,000 --> 00:13:22,000
And then the Buddha tells three stories about their pasts.

195
00:13:22,000 --> 00:13:25,000
The first, the past of the crow, they go to see the Buddha,

196
00:13:25,000 --> 00:13:34,000
and the Buddha says, the crow is suffering for past deeds.

197
00:13:34,000 --> 00:13:39,000
And it seems like the story is kind of suggesting that it's not just one pasting,

198
00:13:39,000 --> 00:13:43,000
but it's sort of a habit of bad deeds.

199
00:13:43,000 --> 00:13:45,000
But he gives examples.

200
00:13:45,000 --> 00:13:50,000
So he says, for a long time, the crow was a farmer.

201
00:13:50,000 --> 00:13:52,000
And he had an ox.

202
00:13:52,000 --> 00:13:55,000
And he was trying to get this ox to do work for him.

203
00:13:55,000 --> 00:13:58,000
But try as he might, he couldn't get the ox to,

204
00:13:58,000 --> 00:14:01,000
he couldn't tame the ox.

205
00:14:01,000 --> 00:14:04,000
He'd get it to work, and then it would work for a little bit,

206
00:14:04,000 --> 00:14:07,000
and then it would go lie down.

207
00:14:07,000 --> 00:14:12,000
And then he'd get it to move, and it wouldn't move.

208
00:14:12,000 --> 00:14:14,000
This ox was just terribly, terribly stubborn.

209
00:14:14,000 --> 00:14:19,000
And so he got increasingly more and more angry until he finally got angry.

210
00:14:19,000 --> 00:14:24,000
Enough that the ox just lay down,

211
00:14:24,000 --> 00:14:29,000
that he just covered it up in straw and lit it on fire.

212
00:14:29,000 --> 00:14:31,000
And the Buddha said, because of that,

213
00:14:31,000 --> 00:14:35,000
even the cruel, evil deed he was born in hell,

214
00:14:35,000 --> 00:14:37,000
actually, and after being born in hell,

215
00:14:37,000 --> 00:14:39,000
he was born back in the human realm.

216
00:14:39,000 --> 00:14:43,000
And he was born back in the animal realm as a crow,

217
00:14:43,000 --> 00:14:49,000
and still suffering from it to this day.

218
00:14:49,000 --> 00:14:52,000
In fact, it says he was seven times in succession,

219
00:14:52,000 --> 00:14:57,000
more reborn as a crow.

220
00:14:57,000 --> 00:15:01,000
And then we have the story of the woman on the boat.

221
00:15:01,000 --> 00:15:04,000
This woman, in the past, she was a woman.

222
00:15:04,000 --> 00:15:07,000
She lived in Benerys,

223
00:15:07,000 --> 00:15:11,000
the waternessy as it's known now.

224
00:15:11,000 --> 00:15:13,000
And she had a dog.

225
00:15:13,000 --> 00:15:15,000
She was a housewife,

226
00:15:15,000 --> 00:15:17,000
and so she did all these chores,

227
00:15:17,000 --> 00:15:19,000
but there was a dog in the house

228
00:15:19,000 --> 00:15:22,000
that would follow her around everywhere.

229
00:15:22,000 --> 00:15:26,000
And for some reason, people would tease her

230
00:15:26,000 --> 00:15:30,000
because this dog was following her like a shadow,

231
00:15:30,000 --> 00:15:35,000
and very, very much a very, very affectionate,

232
00:15:35,000 --> 00:15:37,000
actually like normal dogs are,

233
00:15:37,000 --> 00:15:39,000
but people were joking about it

234
00:15:39,000 --> 00:15:41,000
because I guess it wasn't a big thing

235
00:15:41,000 --> 00:15:44,000
for women to have dogs following them around.

236
00:15:44,000 --> 00:15:47,000
In fact, it was a common thing for hunters to have dogs,

237
00:15:47,000 --> 00:15:51,000
as we learned in our previous story.

238
00:15:51,000 --> 00:15:55,000
So these young men joked about it and said,

239
00:15:55,000 --> 00:16:00,000
here comes the hunter with their dog,

240
00:16:00,000 --> 00:16:05,000
we're going to have meat to eat tonight.

241
00:16:05,000 --> 00:16:08,000
They'll be meat to come, just joking about her,

242
00:16:08,000 --> 00:16:13,000
looking like a hunter having this big dog go along with.

243
00:16:13,000 --> 00:16:18,000
And this woman was, I guess, of a cruel bent,

244
00:16:18,000 --> 00:16:24,000
and so getting angry and feeling embarrassed and ashamed,

245
00:16:24,000 --> 00:16:28,000
she picked up a stick and beat the dog, almost a death.

246
00:16:28,000 --> 00:16:31,000
But dogs, being the way they are,

247
00:16:31,000 --> 00:16:34,000
have a funny resiliency to these sorts of things,

248
00:16:34,000 --> 00:16:38,000
and so the dog was actually unmoved

249
00:16:38,000 --> 00:16:42,000
and was still very much in love with this woman.

250
00:16:42,000 --> 00:16:46,000
It turns out, actually the commentary says

251
00:16:46,000 --> 00:16:49,000
that this dog used to be her husband.

252
00:16:49,000 --> 00:16:53,000
And so that was a reason, was recently her husband,

253
00:16:53,000 --> 00:16:56,000
in one of her recent births.

254
00:16:56,000 --> 00:16:59,000
And so even though it's impossible to find,

255
00:16:59,000 --> 00:17:02,000
they say, you can't find someone who hasn't been

256
00:17:02,000 --> 00:17:04,000
your husband, your wife, your son, your daughter,

257
00:17:04,000 --> 00:17:06,000
your mother, your father.

258
00:17:06,000 --> 00:17:12,000
But recent births, there tends to still be some sort of affinity

259
00:17:12,000 --> 00:17:21,000
or enmity in cases when there was enmity before.

260
00:17:21,000 --> 00:17:25,000
And so she beat this dog and it still came back.

261
00:17:25,000 --> 00:17:29,000
And so she was increasingly angry,

262
00:17:29,000 --> 00:17:32,000
irrationally angry at this dog.

263
00:17:32,000 --> 00:17:37,000
And so lo and behold, she, when it came close,

264
00:17:37,000 --> 00:17:42,000
she picked up a rope and she made a loop

265
00:17:42,000 --> 00:17:44,000
and waited for the dog.

266
00:17:44,000 --> 00:17:47,000
When the dog came close, she wrapped the loop around the dog

267
00:17:47,000 --> 00:17:49,000
and tied it to a pot full of sand

268
00:17:49,000 --> 00:17:54,000
into this big pool and it rolled down into the pool.

269
00:17:54,000 --> 00:17:58,000
And the dog was pulled and it was dragged after it

270
00:17:58,000 --> 00:18:00,000
into the pool and it drowned.

271
00:18:00,000 --> 00:18:05,000
And that was the karma that caused her

272
00:18:05,000 --> 00:18:08,000
to be thrown overboard.

273
00:18:08,000 --> 00:18:15,000
As well as be spent many years in hell.

274
00:18:15,000 --> 00:18:18,000
That's story number two.

275
00:18:18,000 --> 00:18:21,000
Story number three,

276
00:18:21,000 --> 00:18:26,000
these humongues are also done bad things in the past.

277
00:18:26,000 --> 00:18:31,000
And so at one time you were cowhertz

278
00:18:31,000 --> 00:18:37,000
and you came upon this huge lizard

279
00:18:37,000 --> 00:18:41,000
and I guess it was something that they would like to eat,

280
00:18:41,000 --> 00:18:42,000
people would like to eat.

281
00:18:42,000 --> 00:18:44,000
And so they ran after it,

282
00:18:44,000 --> 00:18:47,000
trying to catch this big lizard.

283
00:18:47,000 --> 00:18:50,000
But it ran into an ant, an ant,

284
00:18:50,000 --> 00:18:52,000
that had seven holes.

285
00:18:52,000 --> 00:18:54,000
Some reason seven is a big number.

286
00:18:54,000 --> 00:18:57,000
I think it probably just means there were a bunch of holes.

287
00:18:57,000 --> 00:19:01,000
And so they plugged up all these holes.

288
00:19:01,000 --> 00:19:05,000
And then they, you know,

289
00:19:05,000 --> 00:19:07,000
thinking that they could catch it at one of the holes,

290
00:19:07,000 --> 00:19:08,000
but then they said, you know,

291
00:19:08,000 --> 00:19:09,000
we just don't have time for this.

292
00:19:09,000 --> 00:19:11,000
We'll, we'll plug up all the holes

293
00:19:11,000 --> 00:19:14,000
and we'll come back tomorrow and we'll catch this lizard

294
00:19:14,000 --> 00:19:16,000
because there's now no way out of this big,

295
00:19:16,000 --> 00:19:17,000
or it might mound or something,

296
00:19:17,000 --> 00:19:19,000
something with lizards.

297
00:19:19,000 --> 00:19:20,000
I don't know.

298
00:19:20,000 --> 00:19:22,000
Something that lizards like to stay in.

299
00:19:22,000 --> 00:19:25,000
Big lizard though.

300
00:19:25,000 --> 00:19:27,000
So we'll come back tomorrow and we'll catch it.

301
00:19:27,000 --> 00:19:28,000
And so they went home,

302
00:19:28,000 --> 00:19:31,000
but then they forgot all about it.

303
00:19:31,000 --> 00:19:33,000
And so for seven days they went

304
00:19:33,000 --> 00:19:35,000
about their business tending cows elsewhere,

305
00:19:35,000 --> 00:19:37,000
but then on the seventh day they came back

306
00:19:37,000 --> 00:19:39,000
and they were tending cow.

307
00:19:39,000 --> 00:19:42,000
And they saw the ant till again and they realized,

308
00:19:42,000 --> 00:19:45,000
oh, I wonder what happened to the head lizard.

309
00:19:45,000 --> 00:19:48,000
And so they opened up the holes

310
00:19:48,000 --> 00:20:13,000
and the lizard at this point

311
00:20:13,000 --> 00:20:15,000
we tortured it terribly.

312
00:20:15,000 --> 00:20:18,000
They nursed it and they actually brought it back to life

313
00:20:18,000 --> 00:20:20,000
and he said, the Buddha said,

314
00:20:20,000 --> 00:20:22,000
see because of that you were able to escape

315
00:20:22,000 --> 00:20:25,000
because you came back for this lizard.

316
00:20:25,000 --> 00:20:29,000
If not, that would have been it for you.

317
00:20:29,000 --> 00:20:32,000
I think these stories are interesting

318
00:20:32,000 --> 00:20:34,000
whether you believe them or not,

319
00:20:34,000 --> 00:20:37,000
but they give some idea of the nature of karma

320
00:20:37,000 --> 00:20:40,000
according to Buddhism and some of the ways.

321
00:20:40,000 --> 00:20:41,000
They're just examples.

322
00:20:41,000 --> 00:20:44,000
They're not law, like this has to be like this.

323
00:20:44,000 --> 00:20:47,000
But they're apparently the way things sometimes turn out.

324
00:20:47,000 --> 00:20:52,000
Like our past deeds influence both in this life and the next.

325
00:20:52,000 --> 00:20:56,000
They influence the things that happened to us.

326
00:20:56,000 --> 00:20:57,000
And then they said,

327
00:20:57,000 --> 00:21:02,000
but is it really that way that you can't escape your karma?

328
00:21:02,000 --> 00:21:04,000
Isn't there some where you can go to escape it?

329
00:21:04,000 --> 00:21:06,000
Couldn't you run away and the Buddha said,

330
00:21:06,000 --> 00:21:07,000
no, you couldn't run away?

331
00:21:07,000 --> 00:21:10,000
There's no place on earth that you can go.

332
00:21:10,000 --> 00:21:12,000
To run away from your karma.

333
00:21:12,000 --> 00:21:16,000
There's another jataka that talks about this.

334
00:21:16,000 --> 00:21:22,000
There's a goat that this Brahmin,

335
00:21:22,000 --> 00:21:24,000
the goat talks to him and the goat says,

336
00:21:24,000 --> 00:21:27,000
it's this crying laughing jataka

337
00:21:27,000 --> 00:21:29,000
where he cries and then he laughs

338
00:21:29,000 --> 00:21:31,000
or he laughs and then he cries.

339
00:21:31,000 --> 00:21:34,000
He's about to be killed and the goat starts laughing

340
00:21:34,000 --> 00:21:36,000
and then he says,

341
00:21:36,000 --> 00:21:37,000
why are you laughing?

342
00:21:37,000 --> 00:21:41,000
He says, because this is my last life, I was a brah.

343
00:21:41,000 --> 00:21:46,000
I'm now, this is the last life that I have to be born as a goat.

344
00:21:46,000 --> 00:21:49,000
For 500 years, I've been a sacrificial goat.

345
00:21:49,000 --> 00:21:50,000
This is it.

346
00:21:50,000 --> 00:21:52,000
And then he starts crying and the guy's,

347
00:21:52,000 --> 00:21:54,000
the Brahmin Brahmin says, why are you crying?

348
00:21:54,000 --> 00:21:57,000
And he's because I'm thinking of you.

349
00:21:57,000 --> 00:22:01,000
Why I was a goat for 500 years being sacrificed

350
00:22:01,000 --> 00:22:03,000
having my head cut off is because before that,

351
00:22:03,000 --> 00:22:06,000
I was a Brahmin just like you who killed goats.

352
00:22:06,000 --> 00:22:09,000
So I know this is where you are going to go.

353
00:22:09,000 --> 00:22:11,000
And the Brahmin said,

354
00:22:11,000 --> 00:22:12,000
oh, then I'll protect you.

355
00:22:12,000 --> 00:22:14,000
I won't let them kill you.

356
00:22:14,000 --> 00:22:17,000
And he said, there's nothing you can do.

357
00:22:17,000 --> 00:22:19,000
There's no way you can stop it.

358
00:22:19,000 --> 00:22:21,000
And sure enough, the Brahmin tried to protect him

359
00:22:21,000 --> 00:22:23,000
and made sure nobody came near him,

360
00:22:23,000 --> 00:22:27,000
but a rock fell actually on this goat.

361
00:22:27,000 --> 00:22:31,000
There's some really strange coincidence he ended up dying.

362
00:22:31,000 --> 00:22:34,000
Karma is like that.

363
00:22:34,000 --> 00:22:38,000
You see this, you see potentially these sorts of things

364
00:22:38,000 --> 00:22:40,000
in the world, very strange things happened.

365
00:22:40,000 --> 00:22:42,000
There was a woman once.

366
00:22:42,000 --> 00:22:46,000
The wife of a top Monsanto exec,

367
00:22:46,000 --> 00:22:48,000
not that that means anything,

368
00:22:48,000 --> 00:22:50,000
but it's interesting.

369
00:22:50,000 --> 00:22:52,000
Walking down the road,

370
00:22:52,000 --> 00:22:55,000
read this in the paper some years ago.

371
00:22:55,000 --> 00:22:56,000
Walking down the road,

372
00:22:56,000 --> 00:22:59,000
and was suddenly hit by a car,

373
00:22:59,000 --> 00:23:04,000
and pulled under the car and dragged screaming

374
00:23:04,000 --> 00:23:09,000
for several blocks before she died.

375
00:23:09,000 --> 00:23:12,000
Dragged under the car.

376
00:23:12,000 --> 00:23:15,000
Turns out the woman who was driving the car

377
00:23:15,000 --> 00:23:18,000
was an old lady who could barely see above the dash

378
00:23:18,000 --> 00:23:20,000
and had no idea what she'd done.

379
00:23:20,000 --> 00:23:23,000
And probably to this day has never been told

380
00:23:23,000 --> 00:23:30,000
which she did.

381
00:23:30,000 --> 00:23:33,000
The story said they hadn't told her.

382
00:23:33,000 --> 00:23:36,000
So it wasn't a bad car,

383
00:23:36,000 --> 00:23:38,000
but she didn't have the intention to kill.

384
00:23:38,000 --> 00:23:40,000
Probably some bad karma involved with driving

385
00:23:40,000 --> 00:23:42,000
when you shouldn't be driving,

386
00:23:42,000 --> 00:23:46,000
but that's a bit different.

387
00:23:46,000 --> 00:23:48,000
But it's the kind of sort of,

388
00:23:48,000 --> 00:23:50,000
I mean, we have no idea why that happened.

389
00:23:50,000 --> 00:23:54,000
Some people, modern people would say it's just a coincidence,

390
00:23:54,000 --> 00:23:59,000
but it's interesting to look and see.

391
00:23:59,000 --> 00:24:09,000
I mean, if you think in terms of sort of cause and effect,

392
00:24:09,000 --> 00:24:12,000
the problem I think is that people focus too much

393
00:24:12,000 --> 00:24:14,000
on physical cause and effect,

394
00:24:14,000 --> 00:24:17,000
and they call the mind,

395
00:24:17,000 --> 00:24:19,000
there's this term epiphenomenon.

396
00:24:19,000 --> 00:24:23,000
The mind is at best just a byproduct

397
00:24:23,000 --> 00:24:25,000
that is ineffectual,

398
00:24:25,000 --> 00:24:27,000
that has no consequence

399
00:24:27,000 --> 00:24:30,000
that the mind can't affect the body,

400
00:24:30,000 --> 00:24:32,000
can't affect reality.

401
00:24:32,000 --> 00:24:35,000
So mind is just this thing that happens,

402
00:24:35,000 --> 00:24:40,000
sort of like just a byproduct of a side product

403
00:24:40,000 --> 00:24:42,000
that is meaningless.

404
00:24:42,000 --> 00:24:44,000
But if you think of the mind as being powerful,

405
00:24:44,000 --> 00:24:49,000
it's being potent, then it makes sense to think

406
00:24:49,000 --> 00:24:54,000
that such a powerful experience

407
00:24:54,000 --> 00:24:58,000
should have some cause.

408
00:24:58,000 --> 00:25:03,000
Like physical things don't just happen coincidentally.

409
00:25:03,000 --> 00:25:07,000
An explosion takes gunpowder,

410
00:25:07,000 --> 00:25:12,000
a supernova takes a lot of energy.

411
00:25:12,000 --> 00:25:16,000
And so the idea that these experiences

412
00:25:16,000 --> 00:25:19,000
of being dragged under a car

413
00:25:19,000 --> 00:25:24,000
should not just happen randomly.

414
00:25:24,000 --> 00:25:26,000
I think there's something to that.

415
00:25:26,000 --> 00:25:28,000
I think there's something that we're missing often.

416
00:25:28,000 --> 00:25:31,000
When we say it's just random, it's just coincidence.

417
00:25:31,000 --> 00:25:33,000
I think there's a much more,

418
00:25:33,000 --> 00:25:36,000
there's an argument,

419
00:25:36,000 --> 00:25:40,000
even not from meditation or so on,

420
00:25:40,000 --> 00:25:43,000
that it would take some kind of structure,

421
00:25:43,000 --> 00:25:45,000
some kind of constant effect,

422
00:25:45,000 --> 00:25:47,000
but from meditative purposes.

423
00:25:47,000 --> 00:25:51,000
I mean, this is a great important to us.

424
00:25:51,000 --> 00:25:54,000
The idea that our intentions,

425
00:25:54,000 --> 00:25:59,000
our minds have consequences.

426
00:25:59,000 --> 00:26:02,000
This is something that moves people to meditate,

427
00:26:02,000 --> 00:26:04,000
moves meditators,

428
00:26:04,000 --> 00:26:07,000
not to do unwholesome deeds.

429
00:26:07,000 --> 00:26:09,000
Meditation for this reason will change your life,

430
00:26:09,000 --> 00:26:12,000
because you start to see how powerful

431
00:26:12,000 --> 00:26:14,000
and how poisonous the mind can be,

432
00:26:14,000 --> 00:26:19,000
how harmful the mind can be when misdirected.

433
00:26:19,000 --> 00:26:22,000
How dangerous it is.

434
00:26:22,000 --> 00:26:24,000
You can see these things building.

435
00:26:24,000 --> 00:26:27,000
You can see how poisonous.

436
00:26:27,000 --> 00:26:30,000
You can imagine what it's like to do these things,

437
00:26:30,000 --> 00:26:34,000
and you can remember the things that you've done

438
00:26:34,000 --> 00:26:39,000
and without any, without any,

439
00:26:39,000 --> 00:26:41,000
without any prompting,

440
00:26:41,000 --> 00:26:44,000
like anyone telling you it's wrong or it's bad,

441
00:26:44,000 --> 00:26:47,000
you just start to feel really bad about the things that you've done,

442
00:26:47,000 --> 00:26:49,000
bad things you've done.

443
00:26:49,000 --> 00:26:52,000
It doesn't take someone to tell you that's bad karma,

444
00:26:52,000 --> 00:26:54,000
that happens as well.

445
00:26:54,000 --> 00:26:58,000
People feel guilty because they're told that things are bad,

446
00:26:58,000 --> 00:27:00,000
but you just can't abide by it,

447
00:27:00,000 --> 00:27:02,000
because it's so powerful.

448
00:27:02,000 --> 00:27:04,000
And there's actually, this is where you start to feel,

449
00:27:04,000 --> 00:27:06,000
the power of these deeds that's karmic

450
00:27:06,000 --> 00:27:09,000
and it's built up inside of you.

451
00:27:09,000 --> 00:27:11,000
It's why our life flashes before our eyes.

452
00:27:11,000 --> 00:27:16,000
Our life doesn't really flash before our eyes when we die.

453
00:27:16,000 --> 00:27:20,000
The things that have had an impact on our mind,

454
00:27:20,000 --> 00:27:26,000
that's karma, those things flash before our eyes.

455
00:27:26,000 --> 00:27:30,000
So this has importance in our practice

456
00:27:30,000 --> 00:27:35,000
and to remind us and to reinforce the things that we see

457
00:27:35,000 --> 00:27:38,000
during our practice, but also to encourage people

458
00:27:38,000 --> 00:27:41,000
to meditate, to learn about these things.

459
00:27:41,000 --> 00:27:43,000
If you want to become a better person,

460
00:27:43,000 --> 00:27:46,000
look at your mind, learn about your mind.

461
00:27:48,000 --> 00:27:52,000
These things occur throughout our lives,

462
00:27:52,000 --> 00:27:54,000
people do evil things,

463
00:27:54,000 --> 00:27:58,000
evil things happen to people who don't seem to deserve them,

464
00:27:58,000 --> 00:28:01,000
they seem to ever have done anything to deserve them.

465
00:28:11,000 --> 00:28:13,000
But when through meditation,

466
00:28:13,000 --> 00:28:18,000
we see that there actually is this cause and effect relationship

467
00:28:18,000 --> 00:28:22,000
and it's quite reasonable to suggest that it continues

468
00:28:22,000 --> 00:28:24,000
into the future.

469
00:28:24,000 --> 00:28:26,000
It's quite reasonable to think

470
00:28:26,000 --> 00:28:29,000
whether you have evidence or proof of it or not.

471
00:28:29,000 --> 00:28:32,000
It's quite reasonable to think that it's going to have an effect

472
00:28:32,000 --> 00:28:33,000
on your next life.

473
00:28:33,000 --> 00:28:37,000
It's going to have an effect on your choices of rebirth.

474
00:28:37,000 --> 00:28:41,000
It's going to have an effect on how people react to you in the future.

475
00:28:43,000 --> 00:28:46,000
That things don't go away, the key to this verse

476
00:28:46,000 --> 00:28:48,000
is that they don't just go away.

477
00:28:48,000 --> 00:28:49,000
They can't outrun it.

478
00:28:49,000 --> 00:28:52,000
The only way to outrun it would be to become an aura hunt

479
00:28:52,000 --> 00:28:55,000
and lightning being before it's going to catch up with you.

480
00:28:55,000 --> 00:28:58,000
And if you die as an aura hunt,

481
00:28:58,000 --> 00:29:00,000
then the future karma can't.

482
00:29:00,000 --> 00:29:03,000
It doesn't have an opportunity to bear fruit.

483
00:29:05,000 --> 00:29:09,000
Anyway, so some stories about karma,

484
00:29:09,000 --> 00:29:11,000
some ideas of karma,

485
00:29:11,000 --> 00:29:14,000
that's the Dhammapanda for tonight.

486
00:29:14,000 --> 00:29:15,000
Thank you all for tuning in.

487
00:29:15,000 --> 00:29:33,000
I wish you all good practice.

