1
00:00:00,000 --> 00:00:12,000
I don't understand how people would know Buddha, attainibhana if he didn't come back to tell people about it.

2
00:00:12,000 --> 00:00:19,000
Well, okay, so the first thing is that he obviously can't come back to tell people about it. This is correct.

3
00:00:19,000 --> 00:00:31,000
The one thing that one could never let another being known.

4
00:00:31,000 --> 00:00:35,000
So we really don't know whether the Buddha, attainibhana.

5
00:00:35,000 --> 00:00:48,000
But on the other hand, we're not looking for facts of this sort. We're not looking to know whether the historic Buddha ever existed

6
00:00:48,000 --> 00:00:56,000
or whether he was truly enlightened, or whether he truly attained, nibhana, or barri nibhana.

7
00:00:56,000 --> 00:01:05,000
Just as we're not looking to try to find out if there are past lives, or whether when we die there is a future life,

8
00:01:05,000 --> 00:01:12,000
or whether the heavenly realms exist or don't exist. We're not interested in this.

9
00:01:12,000 --> 00:01:18,000
What we're interested in is the framework of reality. Our reality works.

10
00:01:18,000 --> 00:01:33,000
And what we come to see in relation to the Buddha's enlightenment is that the things that we have inherited as being the teachings of the Buddha appear to us

11
00:01:33,000 --> 00:01:47,000
to be in line with reality and become verified by us through our practice as being from what we have seen,

12
00:01:47,000 --> 00:02:02,000
from our experience, in line with reality, to the extent that we understand the Buddha to be enlightened.

13
00:02:02,000 --> 00:02:12,000
Or we understand that these teachings to be the teachings of enlightened beings, that if one subscribes to these teachings,

14
00:02:12,000 --> 00:02:30,000
because one is enlightened, that a person who subscribes to these teachings that are a person who understands and relates these teachings as being direct experience, isn't enlightened being.

15
00:02:30,000 --> 00:02:36,000
But we only get to that point once we become an hour ahead, once we become enlightened ourselves.

16
00:02:36,000 --> 00:02:48,000
In the meantime, based on our experience, we begin to appreciate Buddha's teaching.

17
00:02:48,000 --> 00:02:54,000
We see the benefit of the Buddha's teaching, and we see the truth of the Buddha's teaching,

18
00:02:54,000 --> 00:03:03,000
and we come to realize the teachings for ourselves, and the more we do, the more faith we gain in the Buddha.

19
00:03:03,000 --> 00:03:11,000
And finally, we realize the truth for ourselves and understand that the things that the Buddha taught were the truth.

20
00:03:11,000 --> 00:03:22,000
And at the same time, have based on this, have faith in some of the things that the Buddha said.

21
00:03:22,000 --> 00:03:28,000
Like, a person might, I suppose, even become an hour ahead, and not ever remember their past lives.

22
00:03:28,000 --> 00:03:35,000
But they still accept it that there is a past in a future life.

23
00:03:35,000 --> 00:03:44,000
Or, for example, that the Buddha attained Parinibhana, or that when a person enters into, or when an hour ahead passes away,

24
00:03:44,000 --> 00:03:49,000
or Buddha passes away, there is no more returning, there is the entering into Nibhana.

25
00:03:49,000 --> 00:03:58,000
So an hour ahead might take these things on faith, or they might be able to understand them themselves.

26
00:03:58,000 --> 00:04:06,000
But there will be certain things that an hour ahead and enlightened follower will have to take on faith.

27
00:04:06,000 --> 00:04:16,000
Many of the things that the Buddha taught, based on his superior knowledge, knowledge of past lives, knowledge of the future,

28
00:04:16,000 --> 00:04:19,000
what's going to happen in the future, and so on.

29
00:04:19,000 --> 00:04:30,000
That an hour ahead would take on faith, based on their understanding that the Buddha is enlightened,

30
00:04:30,000 --> 00:04:39,000
which they have come to understand, come to accept, based on their realization of the things that the Buddha taught.

31
00:04:39,000 --> 00:04:49,000
So, whether the Buddha himself entered into Parinibhana, or these sorts of questions, is not really the point,

32
00:04:49,000 --> 00:04:58,000
and it's something that you can put aside and eventually take on faith, based on your realization of the core teachings.

33
00:04:58,000 --> 00:05:02,000
Like, what is the truth of suffering? What is the cause of suffering?

34
00:05:02,000 --> 00:05:08,000
What is the cessation of suffering? And what is the path that leads to the cessation of suffering?

35
00:05:08,000 --> 00:05:12,000
When you realize these for yourself, then you will...

36
00:05:12,000 --> 00:05:20,000
I would say you come to realize for yourself what happens when an hour a hundred dies.

37
00:05:20,000 --> 00:05:31,000
When someone who is free from greed, anger, and delusion dies, you come to understand that they have nothing more after that.

38
00:05:31,000 --> 00:05:39,000
So, you can therefore accept that the Buddha, the teacher of all of this is most likely,

39
00:05:39,000 --> 00:06:08,000
though even though we can't verify it for ourselves, entered into Nivana.

