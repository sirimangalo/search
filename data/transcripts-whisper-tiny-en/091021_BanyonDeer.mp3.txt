Today, I thought I would give a retelling of either two or three of the jot to go stories.
Because I think these stories are quite beneficial as allegories, you could say, or as
good examples or things to keep in mind, they're like the Asop's famous, which we can
always call to mind and use as a reminder of some principle or some moral.
But we find about the jot to get tails is actually there.
They can be quite profound and often go far beyond the stories of Asop, which were often
cute or practical, like how to get by in the world.
These ones are much more on the level of virtue and spiritual advancement.
So while they have the same sort of flavors, Asop, they go a lot deeper in their treatment
of morality and virtue and development of the mind and development of the spiritual path.
So I have three here, and I'm not sure if I'll get through them all in the time that I've
allotted myself, but I'll just go until times opened.
Then I'll stop.
So the first one is one that is quite well-known among Buddhist circles.
It's the Negrodemiga-Chatika, which is the Banyan deer, I believe it's translated as
Miga means deer, Chatika is the path that means the past life or the life or the birth,
a certain birth when the Buddha was born as a, the Bodhisattva was born as a deer of
the Negrodem type, and I guess this is a Banyan deer.
So the story starts, keep only with the Banyan deer, and this is the moral, which is going
to come at the end.
So the first, the Jatika's were made up of both prose and verse, and the verse is what
was kept, strictly recited and remembered, and the prose was perhaps modified or even added
to later on, but who knows some of it could be exactly word-for-word, but it seems likely
that it was not recited as strictly as the verses, and so it was just made up based
on the verses, or reconstructed based on the verses, because the verses hold the core of
the story.
So this story has one verse, and the verses keep only with the Banyan deer, and so on.
This story was told by the master, the Buddha, well in Jetawana, about the mother of the
elder named Prince Kasapah.
The daughter we learn of a wealthy merchant of Rajika-ha was deeply rooted in goodness,
and scorned Al-Temporal things.
She had reached her final existence, and within her breast, like a lamp in a picture,
flowed her sure hope of winning Arahant ship, which was in full enlightenment.
As soon as she reached knowledge of herself, as soon as she reached came of age, she took
no joy in a worldly life, but yearned to renounce the world.
With this aim she said to her mother and father, my dear parents, my heart takes no joy
in a worldly life, feign what I embrace the saving doctrine of the Buddha, suffer me to
take the vows, what my dear, ours is a very wealthy family, and you are our only daughter.
You cannot take the vows.
This means becoming then.
Having failed to win her parents' consent, though she asked them again and again, she
thought to herself, be it so, then.
When I am married into another family, I will gain my husband's consent and take the vows.
And when being grown up, she entered another family.
She proved a devoted wife and lived a life of goodness and virtue in her new home.
Now it came to pass that she conceived of a child, though she knew it not.
There was a festival proclaimed in that city and everybody kept holiday, the city being
decked like a city of the gods, but she, even at the height of the festival, neither
anointed herself nor put on any finery, going about in her everyday attire.
So her husband said to her, my dear wife, everybody is in holiday-making, but you do not
put on your bravery.
My lord and master, she replied, the body is filled with two and thirty component parts.
Wherefore should it be adorned?
This bodily frame is not of angelic or archangelic mold.
It is not made of gold, jewels, or yellow sandalwood.
It takes not its birth from the womb of a lotus flower, white or red or blue.
It is not filled with any immortal balsam, nay, it is a bread of corruption, and born
of mortal parents.
The qualities that market are the wearing and wasting away, the decay and destruction
of them merely transient.
It is faded to swell a graveyard and it is devoted to lusts.
It is the source of sorrow and the occasion of lamentation.
It is the abode of all diseases and the repository of the workings of karma.
All within it is always excreting.
Yay, as all the world can see, it's end is death, passing to the chinal house, there to
be the dwelling place of worms.
What should I achieve my bridegroom by tricking out this body?
What not its adornment be like decorating the outside of a closed stool of a toilet?
My dear wife rejoined the young merchant.
If you regard this body as so sinful, why don't you become a nun?
If I am accepted by husband, I will take the vows this very day.
Very good, said he, I will get you admitted into the order.
And after he had shown lavish bounty and hospitality to the order of monks, he escorted her
with a large following to the nunnery and had her admitted as a sister.
Instead of the following of David Datta, who was the archandomy of the Buddha and bent on
creating a schism in the Sangha, great was her joy at the fulfillment of her desire
to become a sister.
As her time for giving birth grew near, the sisters, noticing the change in her person,
the swelling in her hands and feet and her increased size said, lady, you seem about to become
a mother.
What does it mean?
I cannot tell ladies, I only know I have led a virtuous life.
So the sisters brought her before David Datta saying, Lord, this young gentleman who was
admitted a sister with the reluctant consent, reluctant consent of her husband has now proved
to be with child, but whether this dates from before her admission to the order or not,
we cannot say, what are we to do now?
Not being a Buddha and not having any charity love or pity, David Datta thought this.
It will be a damaging report to get a broad that one of my sisters is with child, and
that I condone the offense for the sexual act was a fence entailing expulsion from the order.
My course is clear, I must expel this woman from the order.
Many inquiry, starting forward as if to thrust aside a mast of stone, he said away and
expel this woman.
Receiving this answer, they arose and with reference, salutation, withdrew to their
own nunnery.
But the girl said to those sisters, ladies, David Datta, the elder is not the Buddha.
My vows were not taken under David Datta, but under the Buddha, the foremost of the world.
Love me not of the vocation I won so hardily, but take me before the master at Jaita
Wanda.
So they set out with her for Jaita Wanda and journeying over the 45 leagues thither from
Rajakaha, came and do course to their destination, where with reverence and salutation
to the master they laid the matter before him.
Thought the master, the Buddha, albeit the child was conceived while she was still of the
laity, yet it will give the heredics an occasion to say that the ascetic goetama has taken
a sister, expelled by David Datta.
Therefore, to cut shorts at tauch, such tauch, this case must be heard in the presence of
the king and his court.
So on the morrow, he sent for Passainadi, king of Kosala, the elder and the younger Anatapindika,
the lady Visaka, the great lady disciple, and other well-known personages, and in the evening
when the four classes of the faithful followers were all assembled, the monks, the nuns,
the laymen, and the laywomen.
He said to the elder Upali, who was the expert in the discipline of the monks, go and
clear up this matter of the young sister in the presence of the four groups of my disciples.
It shall be done, reference sirs of the elder, and forth to the assembly he went and he
went, and there sitting himself in his place he called up Visaka, the lay disciple, inside
of the king, and placed the conduct of the inquiry in her hands, saying, first ascertain
the precise day of the precise month on which this girl joined the order, Visaka, and
then's compute whether she conceived before or since that date.
Accordingly, the lady had a curtain put up as a screen behind what she retired with
the girl.
Spectatus Manimus, Piedeba's umbilical, Ypsoventra, Puyle, whatever that means.
The lady found, on comparing the days and the months, that the conception had taken place
before the girl had become a sister.
This she reported to the elder, who proclaimed the sister innocent before all the assembly.
And she, now that her innocence was established, reverently saluted the order and the
matter, and with the sisters returned to her own nunnery.
When her time was come, she bore the sun, strong in spirit, for whom she had prayed at the
feet of the Buddha, Buddha, Padumudra, ages ago.
One day when the king was passing by the nunnery, he heard the cry of an infant and asked
his courtiers what it meant.
Today, knowing the facts, told his majesty that the cry came from the child to which the
young sister had given birth.
Surs at the king, the care of children as a clog on sisters in their religious life.
Let us take charge of him.
So the infant was handed over by the king's command to the ladies of his family and brought
up as a prince.
When the day came for him to be named, he was called Kasypa, but was known as Prince Kasypa
because he was brought up like a prince.
At the age of seven, he was admitted a novice under the master and a full brother when
he was old enough.
As time went on, he waxed famous among the expounders of the truth, so the master gave
him precedent saying brethren.
The first in eloquence among my disciples is Prince Kasypa.
Afterwards by virtue of the vamikasuta, he won Arahanship.
So to his mother, the sister, grew to clear vision and won the supreme fruit.
This cussip of the elder shone in the faith of the Buddha, even as the full moon in the
mid-heaven.
Now one day in the afternoon when the Duttagata, on return from his arms round, had addressed
the brethren, he passed into his perfume chamber.
At the close of his address, the brethren spent the daytime either in their night-quarters
or in their day-quarters till it was evening.
When they assembled in the hall of truth and spoke his follows.
Brethren, David Duttagata, because he was not a Buddha, and because he had no charity,
love or pity, was nigh being the ruin of the elder Prince Kasypa and his reverend mother.
But the all enlightened Buddha being the Lord of truth and being perfect in charity, love
and pity has proved their salvation.
And as they sat there telling the praises of the Buddha, he entered the hall with all
the grace of a Buddha, and asked as he took his seed.
What was it they were talking of is they sat together.
Of your own virtues, venerable Sir, said they, and told him all.
This is not the first time Brethren said he that the Duttagata has proved the salvation
and refuge of these two.
He was the same to them in the past also.
Then on the Brethren asked skiing him to explain this to them, he revealed what rebirth
had hidden from them.
Once upon a time when Brethren Duttagata was reigning in Baneras, the Bodhisattva was
born a deer.
At his birth he was golden of hue, his eyes were like round jewels.
The sheen of his horns were as of silver.
His mouth was red as a bunch of scarlet cloth.
His forehooves were as though lacquered.
His tail was like the yaks, and he was as big as a young fool.
And by five hundred deer he dwelt in the forest under the name of King Banyan deer.
And hard by him dwelt another deer, also with an attendant herd of five hundred deer
who was named Branch deer, and was as golden of hue as the Bodhisattva.
In those days the king of Baneras was passionately fond of hunting, and always had meet
at every meal.
Every day he mustered the whole of his subjects, townsfolk and country folk alike, to
the detriment of their business, and when hunting.
Thought as people this king of ours stops at all our work, suppose we were to sow food
and supply water for the deer in his own pleasant, and having driven in a number of deer
to bar them in and deliver them over to the king.
So they sowed in the pleasant grass for the deer to eat, and supplied water for them
to drink, and opened the gate wide.
When they called out the townsfolk and set out into the forest, armed with sticks
and all manners of weapons to find the deer.
They surrounded about a league of forest in order to catch the deer within their circle,
and in so doing surrounded the hunt of the Banyan and Branch deer.
As soon as they perceive the deer they proceeded to beat the trees, bushes, and ground
with their sticks till they drove the herds out of their layers.
Then they rattled their swords and spears and bows with so great a din that they drove
all the deer into the pleasant, and shot the gate.
Then they went to the king and said, sire, you put a stop to our work by always going
a hunting, so we have driven deer enough from the forest to fill your pleasant, henceforth
feed on them.
Hereupon the king we took himself to the pleasant, and in looking over the herd saw among
them two golden deer, to whom he granted immunity.
Sometimes he would go of his own accord and shoot a deer to bring home, sometimes the
cook would go and shoot one.
At first sight of the bow the deer would dash off trembling for their lives, but after
receiving two or three wounds they grew weary and faint and were slain.
The herd of deer told this to the Bodhisattva who sent for branches at friend.
The deer are being destroyed in great numbers, and though they cannot escape death at
least let them not be heatlessly wounded.
Let the deer go to the block by turns, one day one from my herd, and next day one from yours.
The deer on whom the lot falls to go, to the place of execution and lie down with its head
on your block, must do so.
In this wise the deer will escape wounding.
The other agreed and then forth the deer whose turn it was, used to go and lie down with
its neck, ready on the block.
The cook used to go and carry off only the victim which awaited them.
Now one day the lot fell on a pregnant dove of the herd of branch, and she went to branch
and said, Lord, I am with young.
When I have brought forth my little one there will be two of us to take our turn, or
to me to be passed over this turn.
No, I cannot make your turn another," said he.
You must bear the consequences of your own fortune, be gone.
Having no favor with him, the doe went on to the Bodhisatta and told him her story, and
he answered, very well, you go away, and I will see that the turn passes over you.
And therewith all he went himself to the place of execution and lay down with his own
head on the block.
Crying the cook on seeing him, why?
Here's the king of the deer who was granted immunity, what does this mean?
And off he ran to tell the king.
The moment he heard of it, the king mounted his chariot and arrived with a large following.
My friend, the king of the deer, he said, on beholding the Bodhisatta, did I not promise
you your life how comes it that you are lying here?
Sire the Bodhisatta replied, there came to me a doe, big with young who prayed me to let
her turn fall on another, and as I could not pass the doom of one on to another, I
lay down my own life for her and taking her doom on myself have laid me down here.
Think not that there is anything behind this, your majesty.
My lord, the golden king of the deer is that the king.
Never yet saw I, even among men, one so abounding in charity love and pity is you.
Before I am pleased with you, arise I spare the lives above you end of her.
Though to be spared which other rest do, O king of men, I spare them their lives too,
my lord.
Sire only the deer in your pleasant will thus have gained immunity which shall all the rest
do.
Their lives too I spare, my lord.
Sire, dear will thus be safe, what would have but the rest of the four could it footed
creatures, what would they do?
I spare them their lives too.
Sire, four would have creatures will thus be safe, what would the flocks and birds do?
They too shall be spared.
Sire, birds will thus be safe, but what would the fishes do who live in the water?
I spare their lives also.
After that's interceding with the king for the lives of all creatures, the great being
arose, establishing the king in the five precepts saying, walk in righteousness, great
king, walk in righteousness, and justice towards parents, children, townsmen, and country folk,
so that when this earthly body is dissolved you may enter the bliss of heaven.
Thus with the grace and charm that marks a Buddha did he teach the truth to the king.
A few days he tarried in the pleasant for the king's instruction and then with his attendant
heard he passed to the forest again, and that no brought forth a fawn fair as the opening
bud of the lotus who used to play about with the branched ear.
Seeing this his mother said to him, my child don't go about with him, only go about with
the herd of the banyan deer, and by way of exhortation she repeated the stanza which falls
in this birth.
Keep only to the banyan deer and shunned the branched ears heard, more welcome far as death
my child in banyan's company than in the amplest term of life with branched.
Then forth the deer now in the enjoyment of immunity used to eat men's crops and the men
remembering the immunity granted to them did not dare to hit the deer or drive them away.
So they assembled in the king's courtyard and laid the matter before the king.
Said he, when the banyan deer won my favor I promised him a boon, I will forego my kingdom
rather than my promise, be gone, not a man in my kingdom may harm the deer.
But when this came to the ears of the banyan deer he called his her together and said
hence forth you shall not eat the crops of others.
At having thus forbidden them he sent a message to the men saying from this day forward,
let no husband men fence his field, but merely indicated with leaves tied up around it.
And so we here began a plan of tying up leaves to indicate the fields, and never was
a deer known to trespass on a field so marked.
For thus they had been instructed by the Bodhisattva.
Lasted the Bodhisattva exhort the deer of his herd and thus did he act all his life long
and at the close of life long passed away with them to fare according to his desserts.
The king who abode by the Bodhisattva's teaching and after a life spent in good works passed
away to fare according to his desserts.
At the close of this lesson when the master had repeated that, as now in bygone days
also he had been the salvation of the pair he preached the four noble truths.
He then showed the connection linking together the two stories he had told and identified
the birth by saying, David Atta was the branch deer of those days and his followers
now were the deer's herd.
The nun was the doe and Prince Kasabah was her offspring and Ananda was the king and
I myself was the banyan deer.
