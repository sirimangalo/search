1
00:00:00,000 --> 00:00:09,040
Okay good evening. Welcome to our study of the Dhamapanda. Tonight we're continuing on with

2
00:00:09,040 --> 00:00:19,920
verse 2-13 which reigns as follows.

3
00:00:40,040 --> 00:00:57,360
From affection is born sadness, sorrow. From affection is born danger, fear. For one who is free from

4
00:00:57,360 --> 00:01:15,040
affection, there is no sadness. Once or from where could there be danger? So this

5
00:01:15,040 --> 00:01:27,040
verse first was taught. Suppose they taught in regards to Misaka, the chief female they

6
00:01:27,040 --> 00:01:34,080
discipled the Buddha. She was sort of the number one supporter, female supporter of the

7
00:01:34,080 --> 00:01:40,080
Buddha. There were two of them. One was Anatapindaka, he was male, Misaka was female. So these

8
00:01:40,080 --> 00:01:50,320
two together were the two top supporters of the Buddha. She did much to support the Buddha and

9
00:01:52,160 --> 00:02:04,000
one of the things is when monks would come to Sabati, they would come to her monastery first.

10
00:02:04,000 --> 00:02:10,240
And not her as the monastery she built for them. It's called Pumbarama. So they would come there

11
00:02:10,880 --> 00:02:20,240
from Rajgiri or wherever they come from. So she would constantly go to this monastery and receive

12
00:02:20,240 --> 00:02:30,160
the monks coming in and also receive the monks leaving Sabati to go elsewhere. And when she was

13
00:02:30,160 --> 00:02:38,000
unable to unavailable to do it, she would have her children and her grandchildren do it. So Misaka

14
00:02:38,000 --> 00:02:45,280
was, she became a Sotapana when she was seven years old. She went to the monastery to hear the

15
00:02:45,280 --> 00:02:53,440
Buddha teach and with her father. And at seven years old listening to the Dhamma, she became a Sotapana,

16
00:02:53,440 --> 00:03:07,920
which means she entered into cessation of suffering. And then she grew up and got married and there's

17
00:03:07,920 --> 00:03:16,160
a story about her trials and tribulations. But eventually she had apparently 20 children and

18
00:03:16,160 --> 00:03:21,280
all of her children had children and so she had many grandchildren and wherever she would go,

19
00:03:21,280 --> 00:03:29,600
they would go together like a flock. A very close knit family where they were all very

20
00:03:30,880 --> 00:03:38,800
affectionate towards each other. But one of them in particular, Datta, one of Misaka's grandchildren,

21
00:03:38,800 --> 00:03:52,560
when Misaka was unavailable to take care of the monks, she would have Datta act in her

22
00:03:52,560 --> 00:04:01,200
steed. And the story goes, it's not a long story. The story simply goes that Datta passed away.

23
00:04:01,200 --> 00:04:14,240
I'm not sure how not sure why. But Misaka was crushed, thinking to herself that there was exceptional.

24
00:04:15,360 --> 00:04:24,000
She was a very special and great, wonderful person, compassionate, caring, thoughtful kind,

25
00:04:24,000 --> 00:04:37,040
probably many, seems that she had many wholesome qualities. And so crying, she went to see the

26
00:04:37,040 --> 00:04:45,360
Buddha. And the Buddha asked her what was wrong and Misaka told her the story. And the Buddha's

27
00:04:45,360 --> 00:04:59,840
reply was, well, Misaka, how many people are there in Saudi? And Misaka said, oh, I've heard it

28
00:04:59,840 --> 00:05:06,880
said that there are 70 million people. I don't know if there were actually 70 million people in

29
00:05:06,880 --> 00:05:16,320
Saudi. It doesn't seem big enough, but a lot of people. And the Buddha said,

30
00:05:18,160 --> 00:05:25,440
asked her, so tell me, Misaka, would you like all of the people of Saudi to be as

31
00:05:27,200 --> 00:05:33,040
would you like to have the same amount of affection towards them as you had Datta?

32
00:05:33,040 --> 00:05:39,680
And immediately she said, oh, yes, I'm a Buddha.

33
00:05:44,880 --> 00:05:46,320
And they said, if that were the case,

34
00:05:48,560 --> 00:05:55,920
he said, Asocha Nakalo, nothing, there would be no time when you weren't crying.

35
00:05:55,920 --> 00:06:03,920
He said, oh, yes, how many people die every day? How many people die every day in Saudi?

36
00:06:04,960 --> 00:06:07,520
There must be many thousands.

37
00:06:11,280 --> 00:06:15,600
And he said, then there would be Asocha Nakalo, there would be no

38
00:06:15,600 --> 00:06:30,960
time when you weren't sad, when you weren't sad. If you were affectionate towards all these

39
00:06:30,960 --> 00:06:39,920
people and one of them were to die, how would you possibly stay from being sad? And she said,

40
00:06:39,920 --> 00:06:48,720
oh, to Bandhi. Yeah. And then we'd say, nata nata nata maya.

41
00:06:50,560 --> 00:06:57,200
Said, it must be so fun or better. It is understood by me nata maya nata maya.

42
00:06:57,200 --> 00:07:10,320
And I get it. I get what you're saying. And then the Buddha taught this first.

43
00:07:11,440 --> 00:07:22,400
So this verse is, and the story together are an especially clear example of a very difficult subject,

44
00:07:22,400 --> 00:07:32,400
a contentious subject, a subject that Buddhists and Buddhist teachers are forced to deal with or

45
00:07:32,400 --> 00:07:44,080
forced to grapple with. And that's the affection towards other beings because it is

46
00:07:44,080 --> 00:07:56,080
different from the affection or the desire one might have towards an inanimate object,

47
00:07:59,200 --> 00:08:05,680
towards an idea, an ambition or a goal that we might say, or just towards general

48
00:08:05,680 --> 00:08:15,280
sensual pleasures, sex, food, music. It can be attached to all those things. For attachment to

49
00:08:17,120 --> 00:08:30,720
affection is different. And so much so that it's quite common for people to hear this and think,

50
00:08:30,720 --> 00:08:37,920
that's not true. There was a story that someone was saying,

51
00:08:40,000 --> 00:08:44,640
not at the reason one, the reason the Buddha once said, pia tojaiti. So of course,

52
00:08:44,640 --> 00:08:50,240
someone was saying, pia tojaiti is so cool. It leads to happiness.

53
00:08:50,240 --> 00:09:04,480
So many people would say, no, affection leads to happiness. I think this is a reason why

54
00:09:04,480 --> 00:09:10,480
the Buddha is not so easily quotable as because his teachings are not immediately obvious.

55
00:09:10,480 --> 00:09:19,920
You see, that's the point of this verse is that it's not obvious. What's implicit in what the

56
00:09:19,920 --> 00:09:24,560
Buddha is saying here is not just, this isn't just something you could say, yeah, I agree with.

57
00:09:24,560 --> 00:09:31,360
It's something that actually challenges our belief. It's revolutionary or it's

58
00:09:35,600 --> 00:09:43,840
it's quite radical. It is very unlike how we understand things like affection,

59
00:09:43,840 --> 00:09:53,120
which is common of the Buddha's teaching. It's in general, very unlike how we understand the world,

60
00:09:54,240 --> 00:10:00,640
non-self, right? Just that teaching on non-self is nobody hears that for the first time and says,

61
00:10:00,640 --> 00:10:10,480
oh yeah, that sounds like how I understand reality. It shakes you up and it's very unfamiliar.

62
00:10:10,480 --> 00:10:18,480
The idea, the idea that affection leads to sorrow, it may not be that profound, but there is

63
00:10:18,480 --> 00:10:26,320
profundity there and it is very, very hotly contested by ordinary people, people who,

64
00:10:27,040 --> 00:10:32,640
by ordinary just means people who really never thought about practicing spiritual teachings or

65
00:10:32,640 --> 00:10:45,360
meditation or something. And this is for two reasons because it is true that in two ways

66
00:10:47,520 --> 00:10:56,960
affection leads to happiness. The first way is that like any other addiction, we like

67
00:10:56,960 --> 00:11:02,160
to people we are affectionate towards. And when you get the things you like, you feel happy,

68
00:11:02,880 --> 00:11:10,720
you feel pleasure. Pleasure is happiness. So it seems like there's no reason to contest that.

69
00:11:13,520 --> 00:11:22,240
But the other reason is more difficult, more challenging and that is that affection

70
00:11:22,240 --> 00:11:33,200
leads to goodness. It seems to. When you're affectionate towards someone, what do you do? Do you act

71
00:11:34,000 --> 00:11:41,840
kind towards them? Do you act generously towards them? Do you understanding and thoughtful and

72
00:11:44,000 --> 00:11:49,760
friendly towards them? Yes, all of these things. And what about those who are affectionate towards?

73
00:11:49,760 --> 00:11:54,560
How, what about how they relate to you? Are they that way? Yes, generally they are

74
00:11:54,560 --> 00:12:03,200
affectionate. And so there's an atmosphere of goodness. It seems associated with affection.

75
00:12:06,640 --> 00:12:11,200
And there's great happiness that comes from goodness, right? The Buddha said himself.

76
00:12:11,200 --> 00:12:18,880
And so go up on Yes, it would jail. Happiness is the accumulation of goodness.

77
00:12:22,320 --> 00:12:29,440
So on the face of it, it seems affection leads to so much happiness. It's very wrong to think

78
00:12:29,440 --> 00:12:36,880
that when they decide as that's how many people would approach this. So the first, the first

79
00:12:36,880 --> 00:12:44,800
type of happiness of course is easily challenged by Buddhism. For those who are spiritually

80
00:12:44,800 --> 00:12:53,040
minded, high-minded, thoughtful becomes apparent that our attachment to people

81
00:12:55,920 --> 00:12:59,680
and the liking of our experiences to them and with them

82
00:12:59,680 --> 00:13:11,200
is fraught with great suffering. Not just when they die, but when they do anything we don't like,

83
00:13:13,760 --> 00:13:19,680
do anything we don't like, something happens to them that we don't like because they are ours,

84
00:13:19,680 --> 00:13:27,760
because we take them as ours, then we hurt, we suffer because they suffer, we suffer because they

85
00:13:27,760 --> 00:13:38,080
cause us a free, because they act in ways that we wish they didn't. And so we wish that everyone

86
00:13:38,080 --> 00:13:45,760
could be so affectionate towards each other. We wish that we wish we could have an affectionate

87
00:13:45,760 --> 00:13:52,720
family, but we don't realize that the liking aspect, the attachment aspect, is directly

88
00:13:52,720 --> 00:13:57,680
responsible for all of the fighting that goes on. How many families are not affectionate,

89
00:14:00,000 --> 00:14:04,160
affectionate, and then add each other's throats.

90
00:14:07,040 --> 00:14:15,040
Now if you look, it's very acute in relation to romance, because of course the attachment is

91
00:14:15,040 --> 00:14:21,680
much greater in romance. It's a very carnal, sensual attachment. And so if you look at over time,

92
00:14:21,680 --> 00:14:32,640
help people become bitter because of their attachments and with fight and quarrel and hurt each

93
00:14:32,640 --> 00:14:37,440
other, we always hurt the ones we love. This is where that comes from.

94
00:14:37,440 --> 00:14:55,360
It's because we have attachments to them being in a certain way, but you have to acknowledge

95
00:14:55,360 --> 00:15:01,920
the goodness. And so the first line of defense would for people to say, you're wrong,

96
00:15:01,920 --> 00:15:08,240
affectionally into happiness. And then you point out this is what the Buddha pointed out to them.

97
00:15:09,360 --> 00:15:17,200
Well indeed, not just even if you don't fight with each other, even if you get along famously

98
00:15:17,200 --> 00:15:24,480
perfectly. Like Risaka and her granddaughter probably got along quite well, they're still going to

99
00:15:24,480 --> 00:15:31,840
die. They're still going to get hurt, sick. Bad things are going to happen to them, and your affection,

100
00:15:31,840 --> 00:15:38,000
your attachment to them, even if they leave when they go away and so on. They're sadness there.

101
00:15:38,000 --> 00:15:49,840
And so people would say, okay, granted, granted, and granted when it's worst when a child

102
00:15:49,840 --> 00:15:55,760
dies, and the parent is left to mourn them, which will never happen, they say.

103
00:15:57,360 --> 00:16:09,440
But you can, you can, you can gamble on it. And it's a pretty good gamble because most of the

104
00:16:09,440 --> 00:16:14,240
time it doesn't happen. Most of the time you die before your children do, then your children

105
00:16:14,240 --> 00:16:25,600
are left to grieve. But you would say, granted, there is some suffering involved with affection,

106
00:16:25,600 --> 00:16:32,160
but the good of it still stands. And you admit, we admit that there is goodness in

107
00:16:33,680 --> 00:16:39,920
our relationships with those we are affectionate towards. And so you would say, someone would say,

108
00:16:39,920 --> 00:16:48,480
it's worth it. And it does appear worth it. And if you look at it that way and say, okay,

109
00:16:48,480 --> 00:16:54,800
well, yes, on the balance, affectionate relationships are good. And there may be some fighting,

110
00:16:54,800 --> 00:17:02,400
but if we strive to be affectionate, then we do good for each other. And that's good,

111
00:17:02,400 --> 00:17:12,400
because goodness is good, right? And this is wrong. This is wrong because goodness and affection

112
00:17:12,400 --> 00:17:26,000
are not, goodness is not supported by affection. It is limited by it. You see, the person who

113
00:17:26,000 --> 00:17:35,840
is affectionate, who has a circle of ones they consider affectionate, is going to be limited in

114
00:17:35,840 --> 00:17:41,200
their goodness by their affection, because they will be partial to those individuals, one,

115
00:17:42,800 --> 00:17:50,400
and two, because their reason for being good becomes more and more dependent on their affection.

116
00:17:50,400 --> 00:17:57,440
It becomes more and more dependent because, of course, it's an addiction. There is the

117
00:17:59,600 --> 00:18:09,120
experience of pleasure at the result of one's action, that the results of other people's actions.

118
00:18:09,680 --> 00:18:16,960
So when you are kind to others and you feel good about that, you feel happy about that,

119
00:18:16,960 --> 00:18:25,920
because of your affection, that supports it actually makes worse your attachment to them.

120
00:18:28,400 --> 00:18:34,640
The more kind parents are, the more loving parents are to their children. The more than they

121
00:18:34,640 --> 00:18:38,880
suffer and worry when their children are in trouble or cause trouble.

122
00:18:38,880 --> 00:18:50,000
So the reason for doing good becomes very much tied up with one's own craving for happiness,

123
00:18:50,000 --> 00:19:08,560
one's own craving for the pleasure of seeing their children succeed, seeing our loved ones do well.

124
00:19:09,280 --> 00:19:15,760
And you can see this especially in the Buddha's example. I was thinking if the Buddha had said

125
00:19:15,760 --> 00:19:28,160
instead, would you like to see all, would you like to relate to all the people in sawati? Well,

126
00:19:28,160 --> 00:19:34,960
let's say all the people in the world. Would you like to relate to them as you did towards your

127
00:19:34,960 --> 00:19:41,440
grandchild and have them relate to you as though you were their grandmother?

128
00:19:41,440 --> 00:19:50,000
Then I think the answer could be very well, yes. But it would lead to a very different situation

129
00:19:50,720 --> 00:20:01,200
because affection is not the same as kindness, goodness. If a person was in this sort of,

130
00:20:03,520 --> 00:20:08,480
well, if a person was in this sort of affectionate relationship with all people,

131
00:20:08,480 --> 00:20:14,240
there would be no time for goodness. They would be, of course, insane with sadness.

132
00:20:22,720 --> 00:20:28,560
But if on the other hand, a person was in a relationship of goodness related,

133
00:20:28,560 --> 00:20:34,880
you know, in the same way that we are kind and thoughtful and helpful towards our circle of people,

134
00:20:34,880 --> 00:20:40,080
if we were like that to all beings, there would be no room for affection.

135
00:20:43,600 --> 00:20:50,160
There would be no reason or inclination towards it. One would be,

136
00:20:51,760 --> 00:20:57,280
simply, one would have to be, simply open to relating kindly to the person in front of them.

137
00:20:57,280 --> 00:21:03,920
There would be no differentiation. There would have to be this very Buddhist state of

138
00:21:03,920 --> 00:21:11,520
non-differentiation, which means if I experience this, I treat it this way. But if I experience that,

139
00:21:11,520 --> 00:21:17,280
I treat it the very same way. And when you relate to people that way, it comes from

140
00:21:17,280 --> 00:21:22,640
the capacity, the ability to relate to reality that way. The same way we relate to pain,

141
00:21:22,640 --> 00:21:30,160
the same way we relate to pleasure, we relate to calm, loud noise, quiet. When you're able to

142
00:21:30,160 --> 00:21:36,400
relate to them without differentiating, there's the only way you could possibly be

143
00:21:38,480 --> 00:21:45,760
truly and perfectly carrying and kind and good. And so you see actually affection limits,

144
00:21:45,760 --> 00:21:51,440
goodness, and is therefore quite selfish. We think the affection that we have towards

145
00:21:51,440 --> 00:21:58,000
other, look at me being kind to my children. We think it's very magnanimous of us, very good of us.

146
00:21:58,000 --> 00:22:02,720
It actually is limiting and the more affection that we are. And so you see this,

147
00:22:03,440 --> 00:22:08,800
families around the world, some cultures, family is so important. And you're so kind and

148
00:22:08,800 --> 00:22:15,360
helpful to your children, your family. But you would destroy others to support your children,

149
00:22:15,360 --> 00:22:24,160
to support your family. For other people that's maybe their religion, good to your people in

150
00:22:24,160 --> 00:22:30,720
your religious group, but who cares what you do, do whatever you want to those who are outside of it.

151
00:22:33,120 --> 00:22:36,960
Some people, it's country, our country so many, you hear about

152
00:22:36,960 --> 00:22:40,560
it's none of our business, the suffering that's going on the other side of the world.

153
00:22:45,440 --> 00:22:50,160
Now I think the one thing I could qualify with this, by saying of course,

154
00:22:50,160 --> 00:22:58,880
of course no one is expected to look after the whole world. There is in fact, I think

155
00:22:59,600 --> 00:23:07,360
a goodness and a greatness when people look after their own circle of relations. So looking

156
00:23:07,360 --> 00:23:09,600
after your families, I think there's a greatness there.

157
00:23:09,600 --> 00:23:20,080
But it's much more looking after the people who you interact with, people who you are

158
00:23:20,080 --> 00:23:29,280
responsible for, rather than partiality, affection, which limits it, which is the limiting factor,

159
00:23:29,280 --> 00:23:41,520
to your goodness. And so you can see in fact, it's not a question of

160
00:23:44,080 --> 00:23:50,880
affection, taking the good with it, taking the bad with the good, taking the bad with the good,

161
00:23:50,880 --> 00:23:59,840
taking the good with the bad, when you have good and bad, well, it's good enough. It's good enough,

162
00:23:59,840 --> 00:24:04,800
but it's going to lead to suffering. It's good enough for most people and most people get by

163
00:24:04,800 --> 00:24:11,440
and they suffer and they die and they're born and do it again, so on and so on. We're very quick to

164
00:24:11,440 --> 00:24:21,760
forget. I've seen, I'm sure everyone has seen, people go through such suffering because of family,

165
00:24:21,760 --> 00:24:30,400
such sadness, such anguish, such cruelty to each other, evil, and then forget about it and say,

166
00:24:30,400 --> 00:24:36,960
oh, you know, the best thing I did to have children to have family. Sorry, my mother said that

167
00:24:36,960 --> 00:24:43,680
to me recently, I'm not thinking of her specifically in this case, but it's probably true

168
00:24:45,200 --> 00:24:54,720
in our family. There were experiences like that as well, great suffering, great cruelty to each

169
00:24:54,720 --> 00:25:01,120
other, and now we get along and we think family is great and it's great that we get along.

170
00:25:01,120 --> 00:25:07,920
But we've somehow tempered our affection, which I think happens as you get older, you know.

171
00:25:11,920 --> 00:25:16,800
Nonetheless, it's not a question of, again, it's not one big thing where you say,

172
00:25:17,360 --> 00:25:22,480
I take the affection and I take the good relationship we have and it's one thing,

173
00:25:22,480 --> 00:25:31,200
no, it's very much two things, and it's two sides of the equation or two poles, polar opposites,

174
00:25:32,000 --> 00:25:36,480
and most people are just somewhere in the middle where they are both, but if we can become less

175
00:25:37,360 --> 00:25:44,960
attached and more and more caring, more and more kind, thoughtful, generous,

176
00:25:44,960 --> 00:25:56,480
absolutely, they will be more and more able to be that way towards all beings and to have a

177
00:25:56,480 --> 00:26:04,640
better, happier life. It's a very important underpinning of the Buddhist teaching and so it has

178
00:26:04,640 --> 00:26:09,600
great importance in our practice, of course, because we're very much attached to people that

179
00:26:09,600 --> 00:26:18,400
as it's a good example of how we have to separate out our lives into ultimate reality,

180
00:26:19,840 --> 00:26:25,040
and this works, it's a good example for any problem we might face. Sometimes our problems are so

181
00:26:25,040 --> 00:26:36,000
complicated, but they can all be simplified down to experience. There's no complicated experience,

182
00:26:36,000 --> 00:26:43,440
every experience is a moment, and it's a moment where we can react in a positive way, react

183
00:26:43,440 --> 00:26:48,880
in a negative way, it's a moment where we can see clearly or a moment where we can get lost,

184
00:26:48,880 --> 00:26:57,760
every moment, and so practice of Buddhism is cultivating the ability to see clearly,

185
00:26:57,760 --> 00:27:06,160
so we don't get lost, not that we aren't kind and caring and thoughtful, but that we give up

186
00:27:06,160 --> 00:27:19,920
this idea of affection that we learn to rise above our addiction to the pleasure of objects,

187
00:27:21,760 --> 00:27:26,560
because ultimately that's one reality that's in so much of what we do. It's in our

188
00:27:26,560 --> 00:27:35,920
relationships, it's in our interactions with inanimate objects, our addiction to the pleasure

189
00:27:35,920 --> 00:27:45,200
that comes from certain experiences, and that's very specific, and that's very specifically

190
00:27:45,200 --> 00:27:51,840
what we're focused on. It is in fact thought to be the cause of suffering, and I thought to be

191
00:27:51,840 --> 00:27:57,280
it is from a Buddhist perspective, the cause of suffering, everything else is fine, everything else

192
00:27:57,280 --> 00:28:05,760
would be perfect if you just didn't have this one little thing where we needed to find pleasure,

193
00:28:07,360 --> 00:28:12,720
because it's that which stops us from being happy, it's that which stops us from being at peace,

194
00:28:12,720 --> 00:28:22,320
and it's that which we seek to understand in our practice of insight meditation.

195
00:28:24,960 --> 00:28:28,560
So a very good verse, good one to challenge us with,

196
00:28:30,480 --> 00:28:36,160
what is not very quotable, because you don't hear the Buddhist words and say yeah that

197
00:28:36,160 --> 00:28:44,960
makes sense, but what is very profound and special about the Buddhist teaching is once you

198
00:28:44,960 --> 00:28:54,080
are present, living here, experiencing this, this means this, once you are in tune with your

199
00:28:54,800 --> 00:29:01,200
experiences of seeing and hearing, instead of the present moment, the reality, then it all

200
00:29:01,200 --> 00:29:08,560
makes very much sense. It's like before you practice, if you read the Buddhist teaching,

201
00:29:09,280 --> 00:29:14,160
you have one experience of it, and an experience of the Buddhist teaching after you practice

202
00:29:14,160 --> 00:29:18,880
is so much, it's like night and day different, it's almost like you read something two different

203
00:29:18,880 --> 00:29:27,440
things, because your perspective is completely different, and the power, the fact that that

204
00:29:27,440 --> 00:29:36,480
happens, that the Buddhist teaching makes sense, complete sense to one who sees reality clearly,

205
00:29:36,480 --> 00:29:42,320
without any reference to Buddhism at all, but one who is present, one who is here and now in touch

206
00:29:42,320 --> 00:30:00,400
with reality, Buddhism is only agreeable to people who are in touch with reality, those are the

207
00:30:00,400 --> 00:30:07,680
only people who are going to appreciate everybody else, who want to appreciate it. It sounds like

208
00:30:07,680 --> 00:30:14,480
you're saying it's, it almost sounds like it's something that very, very niche, right, that's the

209
00:30:14,480 --> 00:30:19,680
word I'm looking for, it's a very niche sort of teaching, it's only for people who understand

210
00:30:19,680 --> 00:30:25,440
reality, who appreciate, who are in touch with reality, but that's a very powerful statement,

211
00:30:25,440 --> 00:30:31,200
I mean it's easy to say, but if you look at it, if you examine it, you can see that it's true,

212
00:30:31,200 --> 00:30:40,080
as what we're doing here is not Buddhist in any niche or specific sense, it's Buddhist in the

213
00:30:40,080 --> 00:30:48,880
very simple word Buddha, Buddha is someone who knows, someone who is awake, and awake just means here

214
00:30:48,880 --> 00:30:59,680
now, present. So that's the teaching, Dhammapada verse 2-1-3, thank you all for listening,

215
00:30:59,680 --> 00:31:01,680
I wish you all the best.

