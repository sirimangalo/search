1
00:00:00,000 --> 00:00:07,000
Is Anata something we can cultivate directly or does it come about indirectly?

2
00:00:07,000 --> 00:00:12,000
If directly, how does the self or ego not get involved?

3
00:00:12,000 --> 00:00:17,000
Hmm.

4
00:00:17,000 --> 00:00:22,000
Well, we don't use words like directly or indirectly, I suppose.

5
00:00:22,000 --> 00:00:36,000
And I think what you mean by that do we actively work on an intellectual level,

6
00:00:36,000 --> 00:00:39,000
I would say, to bring that about.

7
00:00:39,000 --> 00:00:45,000
Or do we do something else and the knowledge comes about?

8
00:00:45,000 --> 00:00:51,000
I think it is indirectly, so I'm going to be able to avoid your whole if directly clause.

9
00:00:51,000 --> 00:01:02,000
Because I think it is indirectly, you don't think about non-self.

10
00:01:02,000 --> 00:01:07,000
The practice that we do, let's give a whole overview of the practice that we do and then you'll understand.

11
00:01:07,000 --> 00:01:10,000
Hopefully it will help anyway.

12
00:01:10,000 --> 00:01:17,000
The practice that we follow is called insight meditation based on the four foundations of mindfulness.

13
00:01:17,000 --> 00:01:20,000
So there are two parts to the practice.

14
00:01:20,000 --> 00:01:23,000
We're not practicing Vipasana.

15
00:01:23,000 --> 00:01:32,000
You can't practice Vipasana in an absolute sense or in a literal sense.

16
00:01:32,000 --> 00:01:34,000
You practice mindfulness.

17
00:01:34,000 --> 00:01:37,000
You practice looking at things.

18
00:01:37,000 --> 00:01:40,000
You practice looking at things as they are.

19
00:01:40,000 --> 00:01:42,000
And you practice seeing things as they are.

20
00:01:42,000 --> 00:01:45,000
You practice what is called bhatisatimata.

21
00:01:45,000 --> 00:01:51,000
This bare mindfulness of things specifically as they are.

22
00:01:51,000 --> 00:01:53,000
Bhati means specifically.

23
00:01:53,000 --> 00:01:55,000
Bhati means bare or just.

24
00:01:55,000 --> 00:01:57,000
Bhati means mindful.

25
00:01:57,000 --> 00:02:03,000
So this one word that the Buddha uses to explain the meaning of the word sati in this context.

26
00:02:03,000 --> 00:02:08,000
It means to see things just as they are.

27
00:02:08,000 --> 00:02:12,000
And fully have that be your full attention.

28
00:02:12,000 --> 00:02:16,000
So this is what we try to cultivate.

29
00:02:16,000 --> 00:02:18,000
This state of full awareness.

30
00:02:18,000 --> 00:02:27,000
Or this state of exact awareness or exact remembrance of things as they are.

31
00:02:27,000 --> 00:02:33,000
As a result of doing that based on the body, the feelings, the mind and the dumbness, the four foundations of mindfulness,

32
00:02:33,000 --> 00:02:38,000
there arises knowledge of impermanence, knowledge of suffering and knowledge of nonself.

33
00:02:38,000 --> 00:02:39,000
Why?

34
00:02:39,000 --> 00:02:41,000
Because that's the nature of these things.

35
00:02:41,000 --> 00:02:43,000
That's how these things are.

36
00:02:43,000 --> 00:02:50,000
When we look at things and see things as they are, we can't help but see their characteristics.

37
00:02:50,000 --> 00:02:53,000
And their characteristics happen to be.

38
00:02:53,000 --> 00:02:58,000
You can say it's just by chance or it's just how the world is formed or whatever.

39
00:02:58,000 --> 00:03:00,000
Happened to be, they're impermanent.

40
00:03:00,000 --> 00:03:02,000
They're unsatisfying and they're uncontrollable.

41
00:03:02,000 --> 00:03:05,000
Or they're impermanent suffering and nonself.

42
00:03:05,000 --> 00:03:09,000
And so that's where nonself comes in.

43
00:03:09,000 --> 00:03:14,000
It's about as a realization of seeing things as they are.

44
00:03:14,000 --> 00:03:19,000
Like our teacher said, when you see a tiger, you see it's stripes.

45
00:03:19,000 --> 00:03:28,000
You don't have to look at the tiger and see where it's stripes or how do I find it's stripes or does this tiger have stripes?

46
00:03:28,000 --> 00:03:34,000
As soon as you see the tiger tiger, you can't help but in seeing the tiger to see it's stripes.

47
00:03:34,000 --> 00:03:42,000
So the same goes by seeing the characteristics of reality.

48
00:03:42,000 --> 00:03:49,000
You could still argue that any practice of meditation is egotistical.

49
00:03:49,000 --> 00:03:52,000
It is the ego getting in the way.

50
00:03:52,000 --> 00:03:55,000
Because there is the intention to do it.

51
00:03:55,000 --> 00:03:59,000
And this goes back to the desire, it goes back to forcing.

52
00:03:59,000 --> 00:04:04,000
And there was even one monk who went so far and he's quite a famous monk in Thailand who said,

53
00:04:04,000 --> 00:04:06,000
Mindfulness is nonself.

54
00:04:06,000 --> 00:04:15,000
So the idea of developing it is there for, is there for fallacious?

55
00:04:15,000 --> 00:04:20,000
No, it's not proper for a person to try to develop mindfulness because it's nonself.

56
00:04:20,000 --> 00:04:24,000
This is clear from the Abhidhamma or from the Buddha's teaching.

57
00:04:24,000 --> 00:04:25,000
So you have a big problem there.

58
00:04:25,000 --> 00:04:35,000
So his theory was just to have people sit near him and they become mindful by approximation, by association, with the wise.

59
00:04:35,000 --> 00:04:39,000
And there's kind of something to that.

60
00:04:39,000 --> 00:04:45,000
In terms of thinking, things arising by themselves, but it's such a dangerous thing.

61
00:04:45,000 --> 00:04:54,000
And I can't help but think that it's going to lead his students astray and cause them to be lazy and to waste a lot of time

62
00:04:54,000 --> 00:05:02,000
to get very little results because while they're sitting there next to him waiting for the mindfulness to arise and waiting for wisdom to arise,

63
00:05:02,000 --> 00:05:05,000
they could be actively developing it.

64
00:05:05,000 --> 00:05:10,000
And as a result, getting very good results.

65
00:05:10,000 --> 00:05:13,000
During the practice, you don't want to be pushing it.

66
00:05:13,000 --> 00:05:19,000
You don't want to be actively forcing the mindfulness to arise or so on.

67
00:05:19,000 --> 00:05:28,000
But there is a inclination of mind that has to come about, an investigation.

68
00:05:28,000 --> 00:05:38,000
The mind has to make the choice that instead of following concepts and ideas and judgements and so on.

69
00:05:38,000 --> 00:05:45,000
Instead, it's going to follow the reality and focus on the reality.

70
00:05:45,000 --> 00:05:53,000
To say that there is no self and there is no soul, as I said, is just to denounce those things that have no bearing on reality.

71
00:05:53,000 --> 00:05:56,000
Experience is not automatic.

72
00:05:56,000 --> 00:05:59,000
It's not to say that there is not choice made.

73
00:05:59,000 --> 00:06:01,000
There is in fact choice made at every moment.

74
00:06:01,000 --> 00:06:04,000
We make a choice between many different things at every moment.

75
00:06:04,000 --> 00:06:08,000
What the we is, what the eye is, is irrelevant.

76
00:06:08,000 --> 00:06:12,000
The idea of whether it be a soul or a self or a this or that,

77
00:06:12,000 --> 00:06:15,000
these are all have no basis. It is what it is.

78
00:06:15,000 --> 00:06:18,000
There is the experience and the experience is made up of choices.

79
00:06:18,000 --> 00:06:23,000
If we make a choice to judge something, then there is the choice of judgment arises.

80
00:06:23,000 --> 00:06:27,000
If there is the choice of seeing things as they are, then there is that choice.

81
00:06:27,000 --> 00:06:35,000
If you don't take that choice, then the habits in the mind are going to lead you or you're making a choice anyway.

82
00:06:35,000 --> 00:06:38,000
You make a choice when you choose to do nothing.

83
00:06:38,000 --> 00:06:46,000
If you were to make the argument that the ego gets in the way every time you meditate,

84
00:06:46,000 --> 00:06:52,000
then it's a misunderstanding of what we mean by non-existent to ego.

85
00:06:52,000 --> 00:07:00,000
We don't mean that there is no choice and no effort to be made in the present moment of any kind.

86
00:07:00,000 --> 00:07:10,000
What me mean is that things like ego and self and me and mind don't have any place in reality because there are no entities.

87
00:07:10,000 --> 00:07:12,000
There is only experience.

88
00:07:12,000 --> 00:07:16,000
But that experience is very personal. It is very individual.

89
00:07:16,000 --> 00:07:20,000
And so there are moment to moment choice.

90
00:07:20,000 --> 00:07:25,000
At any moment you can verify this by saying now I'm going to make a choice and choosing something.

91
00:07:25,000 --> 00:07:31,000
At any moment, especially when you practice meditation, you can see that in fact we are quite empowered,

92
00:07:31,000 --> 00:07:38,000
that at every moment we can make a choice to be mindful or to cling.

93
00:07:38,000 --> 00:07:48,000
I would like to point out something about how your question is put.

94
00:07:48,000 --> 00:07:54,000
Is Anata something we can cultivate?

95
00:07:54,000 --> 00:08:08,000
That in itself cannot be said directly or indirectly because it's neither nor.

96
00:08:08,000 --> 00:08:21,000
Anata is not something that we can cultivate. Anata is a state of mind or an insight that arises in your mind,

97
00:08:21,000 --> 00:08:37,000
that you can understand, you can see through, you can understand that something is out of control without being possible to be controlled.

98
00:08:37,000 --> 00:08:49,000
That there is no such thing within yourself or outside yourself that could be controlling anything.

99
00:08:49,000 --> 00:09:03,000
So the way you write your question is already, I don't want to say wrong, but kind of wrong.

100
00:09:03,000 --> 00:09:15,000
Because it's misleading, it's coming from the point that Anata is something, the opposite of Aata.

101
00:09:15,000 --> 00:09:21,000
And it is not that. Anata is that there is nothing.

102
00:09:21,000 --> 00:09:24,000
It's giving up the idea of Aata.

103
00:09:24,000 --> 00:09:28,000
Just like impermanence is not a thing that exists.

104
00:09:28,000 --> 00:09:33,000
So it's giving up the idea that there is a permanent thing that exists.

105
00:09:33,000 --> 00:09:36,000
You see that there is no permanent thing that exists.

106
00:09:36,000 --> 00:09:40,000
And suffering is giving up the idea of finding satisfaction.

107
00:09:40,000 --> 00:09:43,000
So you can't really cultivate it.

108
00:09:43,000 --> 00:09:49,000
What you can do is you can cultivate your mind, you can practice mindfulness,

109
00:09:49,000 --> 00:09:55,000
and you can do the personal meditation and understand Anata.

110
00:09:55,000 --> 00:10:02,000
I mean, in the end, if you want to understand Buddhism simply, it's as Buddha said,

111
00:10:02,000 --> 00:10:04,000
quite simple, giving up clinging.

112
00:10:04,000 --> 00:10:06,000
What are these things?

113
00:10:06,000 --> 00:10:09,000
People always wonder, what are these things impermanence, suffering and oneself?

114
00:10:09,000 --> 00:10:11,000
When am I going to see them?

115
00:10:11,000 --> 00:10:13,000
They're letting go.

116
00:10:13,000 --> 00:10:15,000
There when you see this stuff is garbage.

117
00:10:15,000 --> 00:10:18,000
It's useless, it's meaningless, it has no benefit to me.

118
00:10:18,000 --> 00:10:24,000
There's nothing in the world that when I cling to it is going to bring me happiness.

119
00:10:24,000 --> 00:10:29,000
It's so simple that we miss it, and we want to read more.

120
00:10:29,000 --> 00:10:31,000
Where's the real stuff?

121
00:10:31,000 --> 00:10:35,000
And start reading the Buddha's books and these huge books and all of the Buddha's teaching

122
00:10:35,000 --> 00:10:38,000
that's gathered together, trying to find the essence.

123
00:10:38,000 --> 00:10:39,000
And you miss it.

124
00:10:39,000 --> 00:10:42,000
The Buddha said it on the first page, four noble truths.

125
00:10:42,000 --> 00:10:45,000
What is suffering is to be understood.

126
00:10:45,000 --> 00:10:47,000
And the cause of suffering is to be abandoned.

127
00:10:47,000 --> 00:10:50,000
So all you have to do is look at things and see that they're suffering.

128
00:10:50,000 --> 00:10:53,000
Once you see that they're suffering, you let go of them.

129
00:10:53,000 --> 00:10:59,000
That letting go is the realization of impermanence, suffering and oneself.

130
00:10:59,000 --> 00:11:01,000
It's a realization of a lot of things.

131
00:11:01,000 --> 00:11:05,000
And the Buddha just gave the salient points, these impermanence, suffering and oneself.

132
00:11:05,000 --> 00:11:08,000
It's a realising that things are not beautiful.

133
00:11:08,000 --> 00:11:10,000
It's a realising that things are not stable.

134
00:11:10,000 --> 00:11:13,000
That things are not controllable.

135
00:11:13,000 --> 00:11:18,000
That they're not maintainable.

136
00:11:18,000 --> 00:11:27,000
I mean, all of the, they're not reliable or they're not meaningful.

137
00:11:27,000 --> 00:11:28,000
There's so many different things.

138
00:11:28,000 --> 00:11:31,000
It's a realising that they're useless, basically.

139
00:11:31,000 --> 00:11:37,000
But it's a sub-sabir-dhamma-nalang-a-binee-wa-saya.

140
00:11:37,000 --> 00:11:38,000
This is the first thing.

141
00:11:38,000 --> 00:11:41,000
All you need to know to start practicing the Buddha's teaching,

142
00:11:41,000 --> 00:11:46,000
to start practicing mindfulness, is that no dhammas are worth clinging to.

143
00:11:46,000 --> 00:11:53,000
That's enough theory to get you started on the practice.

