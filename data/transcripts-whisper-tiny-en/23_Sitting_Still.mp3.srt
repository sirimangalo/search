1
00:00:00,000 --> 00:00:07,680
Sitting meditation Q&A. Sitting still. Is there a way to get more motivated while meditating?

2
00:00:08,320 --> 00:00:15,040
Sometimes I want to meditate for an hour, but I end up unable to meditate for longer than 20 minutes.

3
00:00:16,800 --> 00:00:22,640
I think an important part of the answer is that meditation is something that is momentary.

4
00:00:22,640 --> 00:00:27,840
It is something that is to be done at every moment. You cannot meditate for an hour.

5
00:00:27,840 --> 00:00:35,120
You can only meditate for one instant. And that instant is either meditative or it is not.

6
00:00:35,120 --> 00:00:42,400
At this moment, are you mindful or are you not? When you ask how to get motivated during meditation,

7
00:00:42,400 --> 00:00:48,960
a good question to ask yourself might be, how do I best make use of this hour so that it is

8
00:00:48,960 --> 00:00:54,720
generally meditative? If you approach the problem this way, you will not worry so much

9
00:00:54,720 --> 00:01:02,240
about how long you sit or become discouraged by your inability to sit in meditation for a long time.

10
00:01:02,880 --> 00:01:10,000
Our inability to sit for long periods is a part of the reason why we meditate in the first place.

11
00:01:10,000 --> 00:01:16,800
The mental qualities preventing you from sitting for long periods should be a main focus of your

12
00:01:16,800 --> 00:01:23,280
practice. This is something easy to forget because they are the same problems present throughout

13
00:01:23,280 --> 00:01:30,080
our life and we are quick to react to them rather than understand them. The problems in our minds

14
00:01:30,080 --> 00:01:37,040
are a part of who we are. So we meditate in order to overcome them rather than avoid them. One trick

15
00:01:37,040 --> 00:01:43,840
you can use when you want to stop meditating is to ask yourself, what is the problem? What is it

16
00:01:43,840 --> 00:01:50,160
that is causing me to want to stop sitting here to get up and do something else? What is the

17
00:01:50,160 --> 00:01:56,080
difference between sitting here and going somewhere else? It could be a good reason like you

18
00:01:56,080 --> 00:02:02,480
have some important work to do or you are going to injure yourself if you sit in this position

19
00:02:02,480 --> 00:02:09,040
any longer but most likely it is simply because of the reasons why we are meditating in the first

20
00:02:09,040 --> 00:02:17,120
place. Desire, aversion, delusion, etc. These problematic mind states come up in meditation

21
00:02:17,120 --> 00:02:23,360
and present a chance to work them out. Meditation gives us a chance to examine our minds

22
00:02:23,360 --> 00:02:29,280
and to see what is causing us stress and suffering. What is of benefit and what is not?

23
00:02:30,000 --> 00:02:37,520
Understanding this can help improve motivation as well. Other ways of creating motivation include

24
00:02:37,520 --> 00:02:44,480
positive reminders of the benefits of meditation, the benefits to yourself, the peace and clarity

25
00:02:44,480 --> 00:02:50,720
of mind that comes from meditation and how much you can help other people as well. You can think

26
00:02:50,720 --> 00:02:56,720
of friends and family who are stressed and suffering that you would be able to help by learning

27
00:02:56,720 --> 00:03:03,120
this incredible technique of stress reduction. Conversely, you can motivate yourself using negative

28
00:03:03,120 --> 00:03:10,080
reminders of what happens if we do not meditate. The unwholesome mind states that consume our mind,

29
00:03:10,080 --> 00:03:16,800
the hurt we cause towards ourselves, the stress, the addiction and the suffering that we bring

30
00:03:16,800 --> 00:03:23,040
to the world around us. You can remind yourself of the dangers that come from allowing these states

31
00:03:23,040 --> 00:03:30,800
to persist over the long term, the danger of not being able to cope with misfortune, sickness

32
00:03:30,800 --> 00:03:37,760
and old age. When you are not able to deal with simple pains and stresses from sitting still for

33
00:03:37,760 --> 00:03:44,240
more than 20 minutes, what happens if you become invalid and have to stay in bed for days?

34
00:03:44,240 --> 00:03:50,400
How will you be able to cope with that? This sort of reminder can help us appreciate the benefits

35
00:03:50,400 --> 00:04:20,240
of meditation.

