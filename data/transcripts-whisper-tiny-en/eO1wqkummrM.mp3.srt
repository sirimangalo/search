1
00:00:00,000 --> 00:00:06,000
I have interest in mainly eastern religions.

2
00:00:06,000 --> 00:00:11,000
Now I'm planning traveling as an ascetic, going on pilgrimage to holy places.

3
00:00:11,000 --> 00:00:20,000
Wonder what your opinion is about this.

4
00:00:20,000 --> 00:00:29,000
Well, I don't have much interest in asceticism myself.

5
00:00:29,000 --> 00:00:36,000
And traveling is often a hindrance to meditation practice.

6
00:00:36,000 --> 00:00:42,000
Not to put a damper on your plant.

7
00:00:42,000 --> 00:00:50,000
I guess the real problem I see with that is the buffet style.

8
00:00:50,000 --> 00:00:57,000
Or the reinvent the wheel aspect of it.

9
00:00:57,000 --> 00:01:03,000
That sort of thing is the Buddha, the path of a Buddha.

10
00:01:03,000 --> 00:01:10,000
If your intention is to come to know everything, then it's a good path.

11
00:01:10,000 --> 00:01:18,000
If your intention is to let go of everything, then it's probably a bad path.

12
00:01:18,000 --> 00:01:21,000
Or it's not bad, but it's the long way.

13
00:01:21,000 --> 00:01:31,000
Because if you're just looking to let go, then your best bet is to follow the teaching of those who have let go.

14
00:01:31,000 --> 00:01:41,000
Suppose the difficult thing is for you to accept that this teaching or that teaching is the teaching of letting go.

15
00:01:41,000 --> 00:01:56,000
And you talked about this before that if you're not sure which is the path, then picking and choosing or going and exploring many different religions is a good idea.

16
00:01:56,000 --> 00:02:02,000
But if you're asking me all I can say is, this is the path and you have to fall.

17
00:02:02,000 --> 00:02:06,000
And if you want to find peace and happiness, you have to follow this way.

18
00:02:06,000 --> 00:02:18,000
Which of course everyone says. But regardless of whether you're going to go to many different holy places and so on.

19
00:02:18,000 --> 00:02:23,000
Of many different religions.

20
00:02:23,000 --> 00:02:30,000
The point there is that you're not really practicing.

21
00:02:30,000 --> 00:02:34,000
A pilgrimage is for someone who is dedicated to a certain practice.

22
00:02:34,000 --> 00:02:45,000
If you're a Buddhist and your intention is to visit the Buddhist holy sites as a very good idea, wonderful thing.

23
00:02:45,000 --> 00:02:51,000
I've done it twice and now that I'm in Sri Lanka, I think it's probably a good chance to go a third time.

24
00:02:51,000 --> 00:02:56,000
But it should be for someone who is set on that practice.

25
00:02:56,000 --> 00:03:05,000
Otherwise, it's not likely to greatly benefit or it's just the slow way.

26
00:03:05,000 --> 00:03:09,000
It's not a substitute for actually practicing a path.

27
00:03:09,000 --> 00:03:19,000
It's not a substitute for actually practicing people's teachings of a certain religion.

28
00:03:19,000 --> 00:03:29,000
So, I guess all final answer I don't is your life and doesn't sound like an evil thing to do by any means.

29
00:03:29,000 --> 00:03:36,000
But not the way I would recommend for my students.

30
00:03:36,000 --> 00:03:42,000
I would recommend them to cultivate their own minds.

31
00:03:42,000 --> 00:03:51,000
Once they get settled in that, then to consider augmenting their practice by things like pilgrimages.

32
00:03:51,000 --> 00:04:18,000
You know, you've been teaching other people and so on, things that help you to develop your own practice in a more holistic or comprehensive way which pilgrimage can do.

