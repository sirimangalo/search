1
00:00:00,000 --> 00:00:02,560
walking meditation Q&A.

2
00:00:04,240 --> 00:00:07,280
Why does walking meditation come first?

3
00:00:08,320 --> 00:00:12,040
Why does walking meditation come before sitting?

4
00:00:12,040 --> 00:00:14,320
Is it okay to reverse the order?

5
00:00:14,320 --> 00:00:16,200
If that feels more convenient,

6
00:00:16,200 --> 00:00:20,320
it seems to me it is harder to concentrate during walking

7
00:00:20,320 --> 00:00:23,440
and I become distracted much more easily

8
00:00:23,440 --> 00:00:26,480
because there is one more sense included.

9
00:00:26,480 --> 00:00:29,280
I have tried walking with closed eyes,

10
00:00:29,280 --> 00:00:32,080
but it does not work as I lose my balance.

11
00:00:33,920 --> 00:00:36,840
First, do not walk with closed eyes

12
00:00:36,840 --> 00:00:38,760
as you will lose your balance.

13
00:00:38,760 --> 00:00:43,000
Beyond that, this question concerns the goal of meditation.

14
00:00:43,000 --> 00:00:46,960
You describe your goal as concentration and that is wrong.

15
00:00:46,960 --> 00:00:49,440
The goal should not be concentration.

16
00:00:49,440 --> 00:00:50,800
It should be wisdom.

17
00:00:50,800 --> 00:00:54,600
This is something that has to be repeated again and again.

18
00:00:54,600 --> 00:00:58,120
Hoping for concentration or calm in meditation

19
00:00:58,120 --> 00:00:59,720
is the real hindrance.

20
00:00:59,720 --> 00:01:02,000
It is an attachment and a desire,

21
00:01:02,000 --> 00:01:06,120
wanting your experience to be other than what it is.

22
00:01:06,120 --> 00:01:09,000
There is nothing wrong with your walking meditation.

23
00:01:09,000 --> 00:01:12,680
What you are seeing is the impermanence, suffering,

24
00:01:12,680 --> 00:01:14,360
and non-self of the mind.

25
00:01:14,360 --> 00:01:16,560
The mind is changing all the time.

26
00:01:16,560 --> 00:01:18,320
So there is impermanence.

27
00:01:18,320 --> 00:01:20,280
One moment you are with the foot

28
00:01:20,280 --> 00:01:23,880
and then all of a sudden your mind is off somewhere else

29
00:01:23,880 --> 00:01:26,480
and it seems like the two mind states

30
00:01:26,480 --> 00:01:28,680
have no relationship with each other.

31
00:01:28,680 --> 00:01:30,400
This results in suffering.

32
00:01:30,400 --> 00:01:34,680
It is not the way you want it to be according to your desire.

33
00:01:34,680 --> 00:01:36,920
Because you can't control it,

34
00:01:36,920 --> 00:01:39,840
you start to realize that it is non-self.

35
00:01:39,840 --> 00:01:42,800
You cannot make it to be the way you want it to be.

36
00:01:42,800 --> 00:01:44,680
You might make a resolve.

37
00:01:44,680 --> 00:01:48,440
Now I am going to be mindful from here to the wall

38
00:01:48,440 --> 00:01:51,120
and two steps later you have forgotten

39
00:01:51,120 --> 00:01:55,120
because even your mind is not you or yours.

40
00:01:55,120 --> 00:01:57,840
Destruction has to fade away naturally.

41
00:01:57,840 --> 00:02:00,880
You should not try to suppress it

42
00:02:00,880 --> 00:02:05,160
or force it away as this gives rise to more delusion.

43
00:02:05,160 --> 00:02:08,200
The idea that I can control the mind.

44
00:02:08,200 --> 00:02:12,280
Proper focus has to come naturally through mindfulness.

45
00:02:12,280 --> 00:02:16,520
Metitators are often overly concerned with concentration.

46
00:02:16,520 --> 00:02:19,320
Thinking that their minds must be calm

47
00:02:19,320 --> 00:02:22,600
and quiet in order to give rise to wisdom.

48
00:02:22,600 --> 00:02:25,560
This is sometimes due to common translations

49
00:02:25,560 --> 00:02:29,680
of the word samadhi as concentration.

50
00:02:29,680 --> 00:02:33,960
The word samadhi is best translated as focus

51
00:02:33,960 --> 00:02:38,200
because it comes from the same root as the word same.

52
00:02:38,200 --> 00:02:40,640
Meaning level or balanced.

53
00:02:40,640 --> 00:02:43,840
The goal is not merely to become concentrated.

54
00:02:43,840 --> 00:02:47,640
It is to balance concentration with effort.

55
00:02:47,640 --> 00:02:50,120
Practicing walking and sitting together

56
00:02:50,120 --> 00:02:52,800
balances concentration with effort.

57
00:02:52,800 --> 00:02:55,560
Sitting meditation is a great practice,

58
00:02:55,560 --> 00:02:58,280
but until your faculties are balanced,

59
00:02:58,280 --> 00:02:59,920
it can make you drowsy.

60
00:02:59,920 --> 00:03:02,760
If that happens, you should stand up

61
00:03:02,760 --> 00:03:04,560
and practice walking instead.

62
00:03:04,560 --> 00:03:08,120
Walking meditation is great for developing energy.

63
00:03:08,120 --> 00:03:11,600
But it can have the problem of making you distracted.

64
00:03:11,600 --> 00:03:15,280
When you feel distracted, you might decide to sit down

65
00:03:15,280 --> 00:03:17,640
and do sitting meditation instead.

66
00:03:17,640 --> 00:03:21,800
What you are seeing is that in order to balance the faculties,

67
00:03:21,800 --> 00:03:24,160
both walking and sitting are important.

68
00:03:24,160 --> 00:03:27,640
In sitting, you can become drowsy or caught up

69
00:03:27,640 --> 00:03:29,720
in the calm and the tranquility.

70
00:03:29,720 --> 00:03:31,560
You might fall into a trance.

71
00:03:31,560 --> 00:03:35,400
Getting up and walking can help you to break such habits.

72
00:03:35,400 --> 00:03:38,480
It can also help you to overcome attachment

73
00:03:38,480 --> 00:03:42,120
to pleasant states attained during sitting meditation.

74
00:03:42,120 --> 00:03:43,960
Forcing you to be objective.

75
00:03:43,960 --> 00:03:47,400
We like sitting and we like lying even better.

76
00:03:47,400 --> 00:03:51,640
Normally because we can enter into very peaceful states,

77
00:03:51,640 --> 00:03:54,440
lying down, including falling asleep,

78
00:03:54,440 --> 00:03:58,080
which many people are very much attached to.

79
00:03:58,080 --> 00:04:00,320
Why do we do walking first?

80
00:04:00,320 --> 00:04:05,320
Doing walking meditation first is not a hard and fast rule,

81
00:04:05,640 --> 00:04:08,600
but one of the benefits of walking meditation,

82
00:04:08,600 --> 00:04:10,800
enumerated by the Buddha himself,

83
00:04:10,800 --> 00:04:13,840
is that the focus of mind gained from walking

84
00:04:13,840 --> 00:04:17,480
lasts for a long time because it is dynamic.

85
00:04:17,480 --> 00:04:21,200
It has the ability to prepare the mind for sitting meditation.

86
00:04:21,200 --> 00:04:24,720
It is a useful segue from the dynamic behavior

87
00:04:24,720 --> 00:04:28,200
during ordinary life into a static sitting posture.

88
00:04:28,200 --> 00:04:31,800
If you go directly from working and acting in the world

89
00:04:31,800 --> 00:04:35,440
to sitting still, your mind hasn't had a chance to prepare.

90
00:04:35,440 --> 00:04:38,680
This is why people find themselves nodding off

91
00:04:38,680 --> 00:04:43,080
during sitting meditation or falling into trance states.

92
00:04:43,080 --> 00:04:46,280
Walking meditation is halfway between the two,

93
00:04:46,280 --> 00:04:49,560
the long lasting focus and balance of mind

94
00:04:49,560 --> 00:04:51,680
that comes from walking meditation,

95
00:04:51,680 --> 00:04:54,040
augments sitting meditation.

96
00:04:54,040 --> 00:04:56,080
When you are living in ordinary life,

97
00:04:56,080 --> 00:04:59,480
sometimes you want to give up walking meditation entirely

98
00:04:59,480 --> 00:05:03,000
because you are walking all day or you are active all day.

99
00:05:03,000 --> 00:05:07,160
And it can be that you might just do the sitting meditation.

100
00:05:07,160 --> 00:05:11,000
You do not have to maintain walking and sitting in equal parts

101
00:05:11,000 --> 00:05:14,600
as a hard and fast rule, especially when your life involves

102
00:05:14,600 --> 00:05:16,040
physical exertion.

103
00:05:16,040 --> 00:05:18,760
The Buddha's advice regarding walking and sitting

104
00:05:18,760 --> 00:05:22,720
was mostly in regards to intensive meditation practice,

105
00:05:22,720 --> 00:05:26,760
wherein one lies down to sleep for only four hours each day

106
00:05:26,760 --> 00:05:31,280
and splits the remaining 20 hours walking, sitting,

107
00:05:31,280 --> 00:05:35,120
walking, and sitting with minimal chores of eating

108
00:05:35,120 --> 00:05:42,120
and attending to one's bodily needs.

