1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:01:00,000 --> 00:01:02,000
You

4
00:01:30,000 --> 00:01:32,000
You

5
00:02:00,000 --> 00:02:02,000
You

6
00:02:30,000 --> 00:02:32,000
You

7
00:03:00,000 --> 00:03:02,000
You

8
00:03:30,000 --> 00:03:32,000
You

9
00:04:00,000 --> 00:04:02,000
You

10
00:04:30,000 --> 00:04:32,000
You

11
00:05:00,000 --> 00:05:02,000
You

12
00:05:30,000 --> 00:05:32,000
You

13
00:06:00,000 --> 00:06:02,000
You

14
00:06:30,000 --> 00:06:32,000
You

15
00:07:00,000 --> 00:07:02,000
You

16
00:07:30,000 --> 00:07:32,000
You

17
00:08:00,000 --> 00:08:02,000
You

18
00:08:30,000 --> 00:08:32,000
You

19
00:09:00,000 --> 00:09:02,000
You

20
00:09:30,000 --> 00:09:32,000
You

21
00:10:00,000 --> 00:10:02,000
You

22
00:10:30,000 --> 00:10:32,000
You

23
00:11:00,000 --> 00:11:02,000
You

24
00:11:30,000 --> 00:11:32,000
You

25
00:12:00,000 --> 00:12:02,000
You

26
00:12:30,000 --> 00:12:32,000
You

27
00:13:00,000 --> 00:13:02,000
You

28
00:13:30,000 --> 00:13:32,000
You

29
00:14:00,000 --> 00:14:02,000
You

30
00:14:30,000 --> 00:14:32,000
You

31
00:15:00,000 --> 00:15:02,000
You

32
00:15:30,000 --> 00:15:32,000
You

33
00:16:00,000 --> 00:16:02,000
You

34
00:16:30,000 --> 00:16:32,000
You

35
00:17:00,000 --> 00:17:02,000
You

36
00:17:30,000 --> 00:17:32,000
You

37
00:18:00,000 --> 00:18:02,000
You

38
00:18:30,000 --> 00:18:32,000
You

39
00:19:00,000 --> 00:19:02,000
You

40
00:19:30,000 --> 00:19:32,000
You

41
00:20:00,000 --> 00:20:02,000
You

42
00:20:30,000 --> 00:20:32,000
You

43
00:21:00,000 --> 00:21:02,000
You

44
00:21:30,000 --> 00:21:32,000
You

45
00:22:00,000 --> 00:22:02,000
You

46
00:22:30,000 --> 00:22:32,000
You

47
00:22:55,240 --> 00:22:57,240
Very Bright

48
00:22:57,240 --> 00:23:00,740
It's actually it's a little hard to see you step right

49
00:23:03,540 --> 00:23:05,540
Yeah, you're like

50
00:23:05,540 --> 00:23:12,040
That's better. That's much more natural. Yeah, there you go

51
00:23:35,540 --> 00:23:37,540
You

52
00:24:05,540 --> 00:24:07,540
You

53
00:24:23,060 --> 00:24:25,060
I don't know is there an echo

54
00:24:25,060 --> 00:24:27,060
I

55
00:24:27,060 --> 00:24:29,060
You

56
00:24:30,060 --> 00:24:32,060
Yeah, I don't hear anything

57
00:24:32,060 --> 00:24:34,060
You

58
00:24:56,140 --> 00:24:58,140
Test yeah

59
00:24:58,140 --> 00:25:00,140
Okay

60
00:25:00,340 --> 00:25:02,340
Okay

61
00:25:02,340 --> 00:25:04,340
It was probably

62
00:25:04,340 --> 00:25:07,640
20 minutes of silence on the audio stream

63
00:25:13,340 --> 00:25:15,900
Yeah, that's right. I have to go in and change it every time

64
00:25:17,740 --> 00:25:22,140
What's my mic what my can I say it's recording yeah, okay?

65
00:25:22,140 --> 00:25:30,980
Technology you wouldn't believe how much little how many little things come together to make this happen

66
00:25:32,620 --> 00:25:34,620
It's

67
00:25:35,580 --> 00:25:37,580
It's more than I care to think about

68
00:25:39,140 --> 00:25:46,340
Let's if you want to make a little pre-flight checklist I can check with you before you start the download pattern

69
00:25:46,340 --> 00:25:55,940
Make sure everything's all set. Hey, that's a good idea. Way to think now there's too much. It's all convoluted and

70
00:25:57,260 --> 00:26:00,580
Except but okay for pre-flight what we have to check is

71
00:26:00,580 --> 00:26:08,580
is

72
00:26:08,580 --> 00:26:11,460
Check whether I've set up pulse audio and

73
00:26:17,380 --> 00:26:19,380
Whether

74
00:26:19,380 --> 00:26:28,980
The loopback devices are set up correctly

75
00:26:35,140 --> 00:26:37,380
Whether I've turned on dark ice

76
00:26:40,740 --> 00:26:42,740
I think those three things will do it

77
00:26:43,140 --> 00:26:46,260
Okay, and there's much more that could go wrong, but

78
00:26:46,260 --> 00:26:54,420
No, is that only for when you're doing dumapata videos within the hangout or is that for all hangouts?

79
00:26:55,060 --> 00:26:57,380
That should be for all hangouts. Okay

80
00:26:58,420 --> 00:27:00,420
For the dumapata

81
00:27:01,860 --> 00:27:05,060
Then I also have to check but most of the other stuff is working

82
00:27:07,620 --> 00:27:09,620
Most of the other stuff is working

83
00:27:09,620 --> 00:27:13,620
So

84
00:27:13,620 --> 00:27:15,620
Questions? Yes

85
00:27:16,180 --> 00:27:19,060
Does anybody know if it matters what language I meditate in?

86
00:27:19,860 --> 00:27:27,620
If the ING suffix is the ING suffix important for example seeing versus IC

87
00:27:27,620 --> 00:27:37,380
No, no not important in fact the Buddha said IC but because in poly it's one word

88
00:27:44,340 --> 00:27:50,180
And the question was about noting specifically so doesn't matter which language a person notes in

89
00:27:50,180 --> 00:27:59,940
No, certainly doesn't hopefully it's a language you understand you know and understand

90
00:28:02,900 --> 00:28:09,860
It is it's a tool that because we we use language to represent things so it's a it's a way for the

91
00:28:10,580 --> 00:28:14,580
human mind to represent an idea and to to therefore

92
00:28:15,300 --> 00:28:17,540
Be directed towards it like if I shout fire

93
00:28:17,540 --> 00:28:23,140
Everyone's immediately thinking of fire and how to like if there's a fire and someone else fire

94
00:28:23,140 --> 00:28:25,140
Why did they do that?

95
00:28:25,140 --> 00:28:29,460
They shout fire so people will know that there's a fire and we'll direct their attention accordingly

96
00:28:34,740 --> 00:28:40,260
To alert our minds to the reality to bring our minds create alertness

97
00:28:40,260 --> 00:28:48,980
We're in Toronto. Are you giving a talk? This was actually from last night

98
00:28:51,940 --> 00:28:53,940
I'm not

99
00:28:54,980 --> 00:28:57,380
I'm going to Toronto next Saturday

100
00:28:58,580 --> 00:29:02,900
For lunch. I've been invited for lunch. I'm pretty sure I'm not giving a talk but you never know

101
00:29:03,940 --> 00:29:05,940
I do doubt it

102
00:29:05,940 --> 00:29:07,940
I

103
00:29:08,740 --> 00:29:15,780
Bunty, I've noticed that you have an exceptional memory minus really terrible. Could you give some tips on improving my memory?

104
00:29:17,860 --> 00:29:23,380
I mean, I haven't done much to improve it. I think I was born with a pretty good memory. I remember when I was

105
00:29:25,780 --> 00:29:27,780
When I was in high school

106
00:29:28,820 --> 00:29:32,260
They were showing reruns of Monty Python's flying circus

107
00:29:33,620 --> 00:29:35,620
and

108
00:29:35,620 --> 00:29:37,620
There was this one show

109
00:29:38,100 --> 00:29:40,900
Where they all this is really not Buddhism is it?

110
00:29:41,940 --> 00:29:43,940
Anyway, well memory

111
00:29:44,580 --> 00:29:49,540
It has said say something each one on each one by the end of the show each one of them

112
00:29:50,100 --> 00:29:52,100
So it starts off with this sketch where they

113
00:29:52,900 --> 00:30:00,180
Have this name that's just obscenely long and by the end of the show each of the guys had had said the name and it just

114
00:30:00,500 --> 00:30:03,380
Wow, it made me think wow these guys have such good memory

115
00:30:03,380 --> 00:30:09,860
And so I went and and I think this was back when the internet was really really new but somehow I found a website

116
00:30:10,660 --> 00:30:13,060
That was dedicated to Monty Python and

117
00:30:13,860 --> 00:30:15,860
managed to get a transcription of it

118
00:30:16,580 --> 00:30:17,380
and

119
00:30:17,380 --> 00:30:19,380
I spent about

120
00:30:20,500 --> 00:30:21,700
a week

121
00:30:21,700 --> 00:30:24,340
while with it on a piece of paper memorizing it and

122
00:30:24,340 --> 00:30:32,740
And an interesting thing was that after that week I couldn't get it out of my head. This is how habits are formed

123
00:30:33,300 --> 00:30:37,860
It's not and this shows non-self really. I would be sitting there and wow it starts to play itself

124
00:30:38,580 --> 00:30:40,580
back in my head

125
00:30:40,580 --> 00:30:42,580
Because I had been become obsessive about it

126
00:30:45,140 --> 00:30:47,140
The name was

127
00:30:47,140 --> 00:30:53,940
Johan Gambopati D1 Ashford spending spatter crash grand bond fried digger dingle dangle dangle dangle dingle dingle

128
00:30:54,340 --> 00:30:58,980
Bernstein one naka thrasha a pewing a horror with ticklancer grand do not think grand do not

129
00:31:01,220 --> 00:31:05,220
Something called grand let's grab a mask out. It was a curse. Let's him. Himble and sick

130
00:31:05,940 --> 00:31:07,940
Grand do not the curse that

131
00:31:10,180 --> 00:31:12,180
Something of ome

132
00:31:12,180 --> 00:31:18,180
But I mean there is a point there that's actually quite interesting for us and that's

133
00:31:19,620 --> 00:31:26,180
The fact that it's it's just an organic thing. I mean memory isn't something to be proud of or be happy about

134
00:31:26,180 --> 00:31:28,180
They're necessarily

135
00:31:28,180 --> 00:31:42,900
It was my day because you know what there was a phone call and I just unplugged the phone to fix it

136
00:31:45,060 --> 00:31:48,660
I better plug the phone back in let's see who called

137
00:31:50,580 --> 00:31:52,580
They're not gonna be all that happy I suppose

138
00:31:52,580 --> 00:31:58,740
Richard Evans wonder who Richard does anyone know a Richard Evans

139
00:32:02,260 --> 00:32:04,260
You're on the air

140
00:32:08,900 --> 00:32:11,860
Let it ring once and then if they want to come back they can call back

141
00:32:14,740 --> 00:32:18,980
That's how you do it right and tie they call it miss call

142
00:32:18,980 --> 00:32:23,860
But it becomes a verb miss call me they say

143
00:32:25,860 --> 00:32:31,540
So let's call me let it ring once and then they'll call you back. Yeah, yeah because

144
00:32:33,860 --> 00:32:36,500
In in Asia you only pay if you're calling

145
00:32:37,620 --> 00:32:43,780
You don't lose minutes if you're being called so it's a big thing to call someone and just let it ring once

146
00:32:43,780 --> 00:32:49,380
So that you don't have to pay and they pay

147
00:32:50,740 --> 00:32:53,780
Or I mean it's just the way actually it's more used as a way to

148
00:32:54,820 --> 00:32:56,820
Give someone your number

149
00:32:57,220 --> 00:32:59,220
What's your number oh just missed call me

150
00:33:07,380 --> 00:33:09,380
More questions

151
00:33:09,380 --> 00:33:14,500
That's sure right talking about memory anyway

152
00:33:19,620 --> 00:33:24,260
There are ways I mean the point is it's it's not really a Buddhist thing

153
00:33:25,300 --> 00:33:28,340
Memory is something you can build is something you can work at

154
00:33:29,620 --> 00:33:34,660
To some extent I suppose you could argue on the other hand that to some extent it's organic

155
00:33:34,660 --> 00:33:39,700
Yeah, but nonetheless it's not something that I would teach it's not something that I would

156
00:33:40,500 --> 00:33:42,500
encourage you to focus your efforts on

157
00:33:44,820 --> 00:33:48,580
Don't have a good memory don't work in a job that requires you to have a good memory that's all

158
00:33:49,700 --> 00:33:51,700
Find a job that

159
00:33:52,500 --> 00:33:56,820
Allows you to build habits and work based on your skills

160
00:33:56,820 --> 00:34:05,060
You know there was this monk who couldn't remember a single stanza and he still became an R. Hunt

161
00:34:09,860 --> 00:34:12,100
Was that the monk that was given a cloth

162
00:34:14,260 --> 00:34:16,260
What's him

163
00:34:18,820 --> 00:34:25,140
What is the next step after the basic meditation you teach is something added to walking and sitting meditation?

164
00:34:25,140 --> 00:34:28,260
Yeah, we're that's why we're started doing these courses

165
00:34:29,060 --> 00:34:31,060
There's a lot that's added

166
00:34:32,020 --> 00:34:34,020
to both walking and sitting

167
00:34:36,340 --> 00:34:42,740
And so I've started doing that with people but it's based on our weekly meetings

168
00:34:43,380 --> 00:34:45,380
Every week I'll meet with people and

169
00:34:47,140 --> 00:34:49,140
Give them extra meditation

170
00:34:49,140 --> 00:34:55,140
Exercise

171
00:34:56,420 --> 00:35:01,780
One day you made a mention of habit being associated with non-self. Could you please elaborate?

172
00:35:08,740 --> 00:35:10,740
It's a funny question

173
00:35:10,740 --> 00:35:18,740
I mean it's like points to a thirst to understand non-self, which is common

174
00:35:19,460 --> 00:35:24,660
People whenever they hear the word non-self they want to know what does that mean? How can that help me understand non-self?

175
00:35:25,460 --> 00:35:29,060
You can't really understand non-self unless you practice and you shouldn't but

176
00:35:29,700 --> 00:35:32,980
Something that we over intellectualize and therefore get all confused about

177
00:35:34,020 --> 00:35:36,020
It's quite simple habits are

178
00:35:36,020 --> 00:35:41,460
Constructs when their ability you can't just turn them off and therefore they're non-self

179
00:35:42,500 --> 00:35:44,500
It's not a hard concept really

180
00:35:45,300 --> 00:35:48,180
But deeper. I think you have to sort of let go of

181
00:35:49,300 --> 00:35:53,060
Potentially, I don't know who this is or what they're coming from but this kind of question

182
00:35:55,220 --> 00:35:57,220
It points to something the idea of

183
00:35:59,700 --> 00:36:02,180
Our confusion about non-self

184
00:36:02,180 --> 00:36:12,660
Is we well because we have the strong idea of self and so we think in terms of that we think in terms of our view of self

185
00:36:12,660 --> 00:36:14,660
But it's not like that

186
00:36:14,660 --> 00:36:19,300
It's just plain and simple facts and realities, you know habits form

187
00:36:21,860 --> 00:36:24,260
And you can't just turn them on and suddenly have the habit

188
00:36:25,380 --> 00:36:29,700
but through the repeated behavior the habit forms

189
00:36:29,700 --> 00:36:31,700
and

190
00:36:32,340 --> 00:36:34,340
It's only a repeated

191
00:36:35,940 --> 00:36:38,980
Change in behavior that will change the habit

192
00:36:40,420 --> 00:36:43,220
So they're not self in the sense that they can be turned on and off

193
00:36:43,220 --> 00:36:59,140
There's no control over them

194
00:37:03,220 --> 00:37:05,220
I think we're all caught up on questions

195
00:37:06,900 --> 00:37:08,900
Okay

196
00:37:08,900 --> 00:37:16,340
Good night. Thank you all for coming. Thank you Robin for your help. Thank you one day. Good night

