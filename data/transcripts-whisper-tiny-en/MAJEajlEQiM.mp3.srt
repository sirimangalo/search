1
00:00:00,000 --> 00:00:22,720
No, no, everyone did that work and then I do, didn't mean that like to me, no, I think

2
00:00:22,720 --> 00:00:29,080
that works fine, no, yeah, the light seems fine, okay.

3
00:00:29,080 --> 00:00:38,480
So welcome to our live broadcast.

4
00:00:38,480 --> 00:00:46,960
And something to say tonight, oh, you know what I was thinking, I'm going to Florida this

5
00:00:46,960 --> 00:00:56,400
winter and it's kind of, it's kind of personal, it's not really, so I don't know whether

6
00:00:56,400 --> 00:01:04,560
anyone's actually at all interested or what you think, Robin, but I was going to get

7
00:01:04,560 --> 00:01:12,560
presents for my mother and my brother and I was thinking, what could I do, what could

8
00:01:12,560 --> 00:01:18,400
I give them that would, that would both agree was a good present because it was up

9
00:01:18,400 --> 00:01:26,600
to me I'd give them a trip to a meditation center or something. I think that'd be more

10
00:01:26,600 --> 00:01:36,200
about me than about them. So I thought we could go to homeless shelter or something

11
00:01:36,200 --> 00:01:43,600
and make a donation and give out food or you know, that kind of thing. And then I thought

12
00:01:43,600 --> 00:01:49,440
would it be neat if, or maybe it would be neat if our online community got involved

13
00:01:49,440 --> 00:02:01,320
and we did some kind of winter giving thing together where we all, I mean, because I actually

14
00:02:01,320 --> 00:02:07,840
can't give a gift because I got nothing to give, but I have many people who are happy

15
00:02:07,840 --> 00:02:14,640
or want to help me do good things. It's like I'm talking, I'm talking, I'm talking,

16
00:02:14,640 --> 00:02:22,800
has no money, but he has millionaires and billionaires who want to back him up. So he just

17
00:02:22,800 --> 00:02:29,240
says, let's do this and suddenly he's got people helping him. It won't be on that scale,

18
00:02:29,240 --> 00:02:37,280
certainly, but maybe that would be kind of neat and then take pictures of it. I don't

19
00:02:37,280 --> 00:02:41,000
know what there is in Tampa. I was looking a little bit and it's all Christian stuff,

20
00:02:41,000 --> 00:02:49,480
which is fine. There's one organization that's non-sectarian and seems to be secular,

21
00:02:49,480 --> 00:02:55,160
they say secular. So they're probably mostly Christians, but they sound kind of open and

22
00:02:55,160 --> 00:03:03,320
I have no problem working with Christians, as long as they're not pushing God. What do you

23
00:03:03,320 --> 00:03:07,840
think, Robin? I think that sounds like a really cool idea. It doesn't have to be in big

24
00:03:07,840 --> 00:03:17,720
scale or anything, but then I can present it as a gift. I could somehow give receipt

25
00:03:17,720 --> 00:03:23,120
or receipt, what do you call this gift? It's some kind of certificate that you've given,

26
00:03:23,120 --> 00:03:26,160
like on behalf of, I gave on behalf of you, that kind of thing.

27
00:03:26,160 --> 00:03:31,240
Yes, I give certificate. It's not a gift certificate though, it's a donation certificate

28
00:03:31,240 --> 00:03:41,360
or something. Like, you give them the thing that says that you gave $100 or whatever

29
00:03:41,360 --> 00:03:47,160
on their behalf. Okay, I don't know the name. I know what you mean. I don't know the

30
00:03:47,160 --> 00:03:54,640
name of that. That's a nice idea. Yeah, so I actually emailed just to see what was there.

31
00:03:54,640 --> 00:03:59,200
This one group and they haven't gotten back to me yet, although I think it was just yesterday

32
00:03:59,200 --> 00:04:03,160
and probably over the weekend, they were not doing it. So we'll see if by the end of the

33
00:04:03,160 --> 00:04:09,600
week, we'll wait till the end of the week and maybe they'll contact me.

34
00:04:09,600 --> 00:04:16,240
Maybe your friends at the Florida Buddhist Wuhara might know of something.

35
00:04:16,240 --> 00:04:21,520
If Sendamali is watching or if she ever watches this video, maybe she can help out with

36
00:04:21,520 --> 00:04:25,640
that. Give me some good ideas. Yeah, the people, that's a really good idea.

37
00:04:25,640 --> 00:04:30,320
I'll email her if she doesn't email me after this. Sendamali, if you're out there,

38
00:04:30,320 --> 00:04:35,720
email me. Let me know what you think. If there's any sort of charity we can get involved

39
00:04:35,720 --> 00:04:42,040
with, do a charity thing. Because I'd like to, it'd be needed if I could actually get

40
00:04:42,040 --> 00:04:48,920
my family out to take part in that. I'll be there for you. It's won't be doing my

41
00:04:48,920 --> 00:04:56,360
lunch. I'm just sitting around, enjoying it. It's coming in. So it'd be nice if we had

42
00:04:56,360 --> 00:05:03,800
a day when we could do something. It is nice. Sometimes families go and they volunteer

43
00:05:03,800 --> 00:05:10,680
together at a soup kitchen or homeless shelter. I don't know what the situation is in Florida.

44
00:05:10,680 --> 00:05:15,080
I mean, it's not like here where it's very cold that this time in the years that they really

45
00:05:15,080 --> 00:05:19,720
needed, like if we were doing it here, it'd be awesome because there's people who are in

46
00:05:19,720 --> 00:05:25,240
pretty desperate straits as it starts to get cold. But in Florida, it's warmer, so it's not that

47
00:05:25,240 --> 00:05:32,680
sense of urgency. I mean, the real thing is that it's Christmas. One thing we didn't

48
00:05:32,680 --> 00:05:39,880
cut in California was all the time Buddhists got together and I just showed up to

49
00:05:39,880 --> 00:05:48,280
appreciate and take part. But they went into an old age home and gave gifts, just like care

50
00:05:48,280 --> 00:05:55,080
packages. Because a lot of old age, a lot of old people actually, their family doesn't come and

51
00:05:55,080 --> 00:06:03,000
visit them or they don't have family. I don't know about that. I don't know how it works. But

52
00:06:03,000 --> 00:06:14,520
yeah, some old age homes, it's actually quite unpleasant. Or anyway, it's they don't get visitors.

53
00:06:17,480 --> 00:06:22,600
Yeah. Sometimes people end up being placed somewhere far away from their family and

54
00:06:22,600 --> 00:06:34,440
that's something maybe I'll wait and talk to send them later. But I might need help.

55
00:06:35,640 --> 00:06:41,080
Oh, we're we're there. We can definitely discuss that at the volunteer meeting and see how we

56
00:06:41,080 --> 00:06:51,000
can help. Awesome. It's always nice that you can can concerted. Is that the word concerted?

57
00:06:51,000 --> 00:06:55,560
That means focused, concerted like working together.

58
00:06:58,360 --> 00:07:06,040
Interesting word because it's a concert. A concert is concerted effort. Coordinated effort.

59
00:07:09,320 --> 00:07:12,120
The word is concerted, which is collaborative effort.

60
00:07:12,120 --> 00:07:19,560
Concerned is jointly arranged and carried out coordinated. That's a concerted effort.

61
00:07:19,560 --> 00:07:25,560
Right. Interesting word. It refers to a concert or it's it's the same as a concert.

62
00:07:26,120 --> 00:07:33,480
That's what a concert comes from. The concerted giving, no concerted effort or concerted

63
00:07:33,480 --> 00:07:43,480
good deeds is important. It allows us to work together and it brings us closer as a community.

64
00:07:43,880 --> 00:07:48,840
It's an excuse. It's it's an encouraging thing because you see other people doing it.

65
00:07:50,280 --> 00:07:54,120
So you're you feel more confident about it. It's more powerful as a result.

66
00:07:55,080 --> 00:07:59,160
You're able to do bigger things than doing just by yourself and that is encouraging. Like if you

67
00:07:59,160 --> 00:08:03,880
just do a good deed by yourself, it feels like a drop in the bucket. But when you get a lot

68
00:08:03,880 --> 00:08:15,080
of people doing a little good together, it's much more powerful. Awesome. Let's do it. Let's do

69
00:08:15,080 --> 00:08:25,000
something. Yes. We'll do it on for Florida. Do some good deed for Tampa Bay area.

70
00:08:25,000 --> 00:08:34,600
Hopefully my mother is watching. I don't think she watches this. She sometimes tunes in at the

71
00:08:34,600 --> 00:08:46,920
beginning. She likes to see me. She might have tuned in at 9th and so. Hopefully because this

72
00:08:46,920 --> 00:08:57,640
one was a little later, you're secret is safe. Do we have any questions today? Looks like we don't,

73
00:08:57,640 --> 00:09:10,760
actually. Just a couple of nice comments. Just want to say how much I enjoy your

74
00:09:10,760 --> 00:09:16,520
Dhamma Pada talks on YouTube for me the longer, the more teaching I get from them. For me, the

75
00:09:16,520 --> 00:09:23,720
longer, the more teaching I get from them. Please keep up the good work. And guess the keep on

76
00:09:23,720 --> 00:09:32,760
giving. Wonderful. Thank you everyone. I've gotten several encouraging comments. I wasn't really

77
00:09:32,760 --> 00:09:37,240
thinking. I mean, I did say that I don't think there's anything wrong with longer videos either.

78
00:09:37,240 --> 00:09:41,480
And so I'm not, it's not like I'm self-conscious about the length or anything.

79
00:09:42,920 --> 00:09:48,920
It's funny. I remember giving talks and sometimes you're under a time constraint. So someone goes,

80
00:09:50,920 --> 00:09:58,040
and it's so funny. I mean, going over, going over a lot of time span, because there's not

81
00:09:58,040 --> 00:10:05,640
much you can do. It's really teach what you teach. And I remember like looking up and saying,

82
00:10:05,640 --> 00:10:12,200
oh, it's already two, two hours have gone by and people are starting to kind of feel it.

83
00:10:15,400 --> 00:10:21,000
Not easy, not even sometimes. What does depend on the teaching?

84
00:10:23,800 --> 00:10:30,040
How was the flesh, my birthday? That didn't happen. No one showed up. So what actually one man

85
00:10:30,040 --> 00:10:34,360
showed up and just to talk to me, he works at McMaster and he's going to come by on Wednesday.

86
00:10:34,360 --> 00:10:45,160
Nice. So yeah, the flash mob thing, rather disappointing. I'm not very well coordinated.

87
00:10:45,160 --> 00:10:49,800
See, I could have just stayed and meditated on my own, but I was kind of determined not to do that,

88
00:10:49,800 --> 00:10:54,200
because I don't want to give the impression that I'm advertising, because this is

89
00:10:54,200 --> 00:10:59,560
this whole thing about looking, standing out and trying. I don't want to stand out. I don't want

90
00:10:59,560 --> 00:11:12,520
to be about me. Didn't happen. Which is okay. It's not really for me. It was more for people who

91
00:11:12,520 --> 00:11:19,160
wanted to come to meditate. There was one woman who said she would be there and didn't show up.

92
00:11:20,040 --> 00:11:24,520
Maybe she showed up late. I didn't stay. I only stayed for a minute. I stayed 20 minutes,

93
00:11:24,520 --> 00:11:33,000
but I got there 10 minutes earlier. Maybe I should have hung around. But it's been like that's

94
00:11:33,000 --> 00:11:40,600
like the third time that I've done, gone somewhere and no one showed up. Look at these people.

95
00:11:42,280 --> 00:11:46,280
I got a call today from someone in Toronto who wants to come and do a course here.

96
00:11:48,360 --> 00:11:51,480
Don't know how he found out about us. Probably meet up, right? That's the big thing.

97
00:11:51,480 --> 00:11:57,560
Meet up has our telephone number on it, is it? Meetup does have the telephone number, but I don't

98
00:11:57,560 --> 00:12:03,000
think, well, I don't think meetup directly talks about the residential courses, but it does link

99
00:12:03,000 --> 00:12:09,080
in your website and you're all asked if we had courses. Some of you heard about us. Maybe we're

100
00:12:09,080 --> 00:12:13,560
on a few directories I guess now, right? Yeah, there's a lot of links on the meetup page, so

101
00:12:13,560 --> 00:12:19,080
he must have found some information. But are we on like Yelp and Google and what are we on?

102
00:12:19,080 --> 00:12:23,960
We're on Yelp and Google and... So does that mean if I had a Google to go to Google Maps? I

103
00:12:23,960 --> 00:12:30,280
say yes. Actually, you know, I forgot to put the code in that you gave me, so I'm not sure if you

104
00:12:30,280 --> 00:12:36,360
would get their driving directions. But eventually it's going to be on the map, right? Yes.

105
00:12:38,280 --> 00:12:42,200
Right in your TriStar computers and common connection.

106
00:12:42,200 --> 00:12:54,520
I got a... It's not really for this broadcast appropriate, but I got... I don't want to talk about it.

107
00:12:55,480 --> 00:13:03,960
Nothing big, it's just... It's something that... Well, I don't... I guess they just don't want to make

108
00:13:03,960 --> 00:13:13,240
the person. Someone sent me a gift and I'm not sure if I can use it because it was from the US.

109
00:13:14,840 --> 00:13:19,080
And it doesn't mesh well with N and S. So there's this... This isn't the first time this has

110
00:13:19,080 --> 00:13:28,120
happened. I've got another gift from Tim Hortons. So about it's for the US and it doesn't mesh

111
00:13:28,120 --> 00:13:35,320
with Canadian Tim Hortons and a gift. So I'm not even sure if I can use it at Tim Hortons. I haven't

112
00:13:35,320 --> 00:13:40,520
tried, but because it's a little more awkward to do so. But now the same with Starbucks.

113
00:13:42,040 --> 00:13:48,120
Starbucks getting that... I don't know if it'll work or not. Well, you can bring them with you when

114
00:13:48,120 --> 00:13:54,120
you go to Florida. Well, that's right. Yeah. Although we definitely have Starbucks here. Not

115
00:13:54,120 --> 00:13:58,520
so many Tim Hortons other than New York State. Upper New York State is filled with them, I think.

116
00:13:59,320 --> 00:14:03,560
But... I don't know if you can use the American... I think I can maybe use the American

117
00:14:03,560 --> 00:14:08,760
Tim Hortons here. I think there was something about that. That's an email them about it. But the Starbucks

118
00:14:08,760 --> 00:14:18,840
one... It's a little bit different. They can try. I think I'll try. Anyway, I guess I just wanted to

119
00:14:18,840 --> 00:14:29,720
bring it up because if anyone did, I didn't want to do the help. Keep me alive thing. Better to do

120
00:14:29,720 --> 00:14:36,200
it in Canadian terms. The American isn't really... Things are really working. The other thing is I'm

121
00:14:36,200 --> 00:14:42,600
not getting... For some of the... I don't know. I don't know if I want to go into this here, but

122
00:14:42,600 --> 00:14:49,000
I'm not getting... For Peter Pitt, I'm not getting people's names. I have no way of turning.

123
00:14:49,000 --> 00:14:53,560
So if you send me something and I don't get back to it, I may not have the proper information.

124
00:14:55,720 --> 00:14:58,040
Doesn't mean I'm not appreciative. Get it?

125
00:14:59,400 --> 00:15:05,720
No, that's good to know about the American version of cards. Generally, it seems that the

126
00:15:05,720 --> 00:15:14,120
websites are... There's.ca in the web address somewhere, so... Someone went to the wrong Starbucks

127
00:15:14,120 --> 00:15:20,680
address because there's a distinct address and it's a whole... It's exactly the same except

128
00:15:22,360 --> 00:15:27,880
the address. I couldn't register on the.com one because it looked for a zip code.

129
00:15:27,880 --> 00:15:31,960
So I was like, wow, I can't even use this card. And then I found, oh, there's a Canadian

130
00:15:31,960 --> 00:15:39,160
version just left because that's what I actually put on the website. And that works because then

131
00:15:39,160 --> 00:15:41,000
it asks for a postal code.

132
00:15:47,480 --> 00:15:49,400
I'll see if there are Tim Hortons in the attempt.

133
00:15:52,120 --> 00:15:58,200
There's on my burger. Okay, I'll see if there's any in the Tampa Florida area.

134
00:15:58,200 --> 00:16:04,440
No, it's not a big deal. Because I'll be with my mother and she's going to take care of me.

135
00:16:05,400 --> 00:16:06,600
She loves to feed me.

136
00:16:09,560 --> 00:16:11,240
Let's be Italian upbringing, I think.

137
00:16:13,160 --> 00:16:16,120
And you can bring her to Tim Hortons. This is true.

138
00:16:18,440 --> 00:16:23,320
I think with our Starbucks, I got the Starbucks. I can bring the whole family treat them up to a

139
00:16:23,320 --> 00:16:30,280
boxing, any brunch at Starbucks. I don't really have a brunch there.

140
00:16:34,440 --> 00:16:39,080
No, I think I don't go to Starbucks very often. I think they have mostly little pastries to go

141
00:16:39,080 --> 00:16:40,120
with the coffee.

142
00:16:40,120 --> 00:16:42,040
You have the best, I remember it.

143
00:16:42,680 --> 00:16:47,400
Spinach, I mean, that's what this card I get. I'll use for it. It's got these nice

144
00:16:47,400 --> 00:16:54,120
street spaces. Spinach and jeans and stuff.

145
00:17:17,880 --> 00:17:22,520
Anyway, I'm not, I didn't, it's not a big deal. I don't want people to think and

146
00:17:23,640 --> 00:17:24,200
advertising.

147
00:17:25,960 --> 00:17:29,560
No, I think it's useful information because, you know,

148
00:17:30,200 --> 00:17:33,880
but I'm not, I'm not trying to hint at anything because there's people have sent me lots of stuff.

149
00:17:36,760 --> 00:17:41,160
People want to do it, want to help keep me alive. That's great, but it's not a pressing need

150
00:17:41,160 --> 00:17:42,760
at this today about it.

151
00:17:42,760 --> 00:17:48,120
So there is a Tim Hortons at 401 channel side drive in Tampa, Florida.

152
00:17:48,120 --> 00:17:48,680
No kidding.

153
00:17:50,840 --> 00:17:55,560
But we're not in Tampa, we're in Dunedin. So it's even to go to the campus a bit of a chore.

154
00:17:56,440 --> 00:17:57,160
I'll check that.

155
00:18:02,680 --> 00:18:04,040
I'm sorry, how do you spell Dunedin?

156
00:18:04,040 --> 00:18:13,800
Dunedin, Dunedin, D-U-N-E-D-I-N.

157
00:18:16,680 --> 00:18:18,920
No, well, not within five miles anyway.

158
00:18:21,480 --> 00:18:22,440
Maybe Starbucks?

159
00:18:23,880 --> 00:18:25,080
Probably Starbucks.

160
00:18:25,080 --> 00:18:26,920
Oh, I'm sure there will be a Starbucks.

161
00:18:26,920 --> 00:18:34,840
I'll take them for Starbucks coffee. My mum drinks coffee. My stepfather drinks coffee.

162
00:18:34,840 --> 00:18:36,920
My brother probably drinks coffee. I'm sure he does.

163
00:18:37,960 --> 00:18:40,200
Take them for a good Starbucks coffee morning.

164
00:18:40,840 --> 00:18:41,400
There you go.

165
00:18:43,240 --> 00:18:44,360
It's already paid for.

166
00:18:45,320 --> 00:18:47,880
So it's a wonderful opportunity to create

167
00:18:48,840 --> 00:18:49,400
family

168
00:18:49,400 --> 00:18:55,000
family, family goodness, I don't know.

169
00:18:56,120 --> 00:18:56,440
Yes.

170
00:18:58,920 --> 00:19:03,640
There's so many people, one of the things about being back a little bit closer to society is there's

171
00:19:03,640 --> 00:19:14,680
so many people, so many people who need support and there's so little support for people who need

172
00:19:14,680 --> 00:19:20,280
dinner. That's really shameful. I mean, there's really a need for this kind of thing.

173
00:19:20,280 --> 00:19:26,040
Just kind of why it's funny that no one's taking advantage of it or it's slow for people to

174
00:19:26,040 --> 00:19:31,640
and students are so caught up in their studies. I mean, as many of them don't even have to,

175
00:19:31,640 --> 00:19:36,760
wouldn't ever have the time, but this morning I was this woman who's now become probably my best

176
00:19:36,760 --> 00:19:38,840
friend at university. It's kind of funny.

177
00:19:38,840 --> 00:19:46,360
Tomorrow morning we'll be sitting again together at a philosophy and I mean, there's no danger of

178
00:19:46,360 --> 00:19:52,520
anything. She's got a boyfriend, so it's not like there's a danger of getting attached or something.

179
00:19:55,400 --> 00:20:01,960
But she's, she's, I mean, so many people like this. She's got two jobs and

180
00:20:01,960 --> 00:20:08,680
I don't know, I guess I shouldn't broadcast people's lives on the internet, but

181
00:20:10,360 --> 00:20:17,160
she's a wonderful person. She's involved in peace advocacy. She said she's got

182
00:20:17,720 --> 00:20:25,720
both Muslim and Jewish relatives and she's got a half sister I think in Syria or she had a half

183
00:20:25,720 --> 00:20:33,800
sister who died in bombing. So she's very much in peace, peace work and refugee, helping refugees

184
00:20:33,800 --> 00:20:43,240
and she's very involved in a Syrian refugee thing and just a really sensitive and nice and kind

185
00:20:43,240 --> 00:20:51,240
and thoughtful person and struggling, you know, because she can't get a student alone

186
00:20:51,240 --> 00:20:56,920
some reason. I mean, I was talking this morning, why it's kind of be, it's kind of be wrong.

187
00:20:56,920 --> 00:21:01,320
There's got to be some way to repeal it, but I don't know. It's not my business.

188
00:21:03,240 --> 00:21:07,240
And so she started meditating and she was supposed to come out today and

189
00:21:09,880 --> 00:21:12,360
you know, it's just it's amazing that

190
00:21:15,240 --> 00:21:20,920
we're not more together as a society. We've lost our sense of community and supporting each

191
00:21:20,920 --> 00:21:27,720
other and so on. The next students are sensitive to this kind of thing because

192
00:21:27,720 --> 00:21:34,600
you see people hanging out and supporting each other. That's just very, very busy and overstressed.

193
00:21:36,440 --> 00:21:42,680
Now, these people wonder what the, it would be an awesome place for community, the university,

194
00:21:42,680 --> 00:21:51,400
but it seems there's not so much because people are too busy to stress and tired. People

195
00:21:51,400 --> 00:21:56,920
sleep one or two hours a night before our quizzes, people are sleeping three hours because they

196
00:21:57,960 --> 00:22:05,640
had to study for the quiz all night. Right, operating papers. I remember when I was in school

197
00:22:05,640 --> 00:22:12,920
16 years ago stumbling into the office to submit a paper and I was, you know, the world was,

198
00:22:13,880 --> 00:22:20,200
it was like this, it was blurring in front of my eyes and I couldn't really walk because I had

199
00:22:20,200 --> 00:22:25,080
been up for who knows how many of them are. I'm pretty sure that I ended up doing that very well.

200
00:22:25,080 --> 00:22:32,600
Aren't they, did you have any experience with the universities when you were in Buddhist countries?

201
00:22:35,960 --> 00:22:38,920
Very little. I'm just wondering if it's different.

202
00:22:40,760 --> 00:22:47,400
Well, the Buddhist universities are fairly laid back. They're not well accredited.

203
00:22:47,400 --> 00:22:55,320
So, oh, yeah, they don't seem all that rigorous in their training.

204
00:22:57,880 --> 00:23:01,160
I mean, I've heard things about secular universities in Asia that are

205
00:23:01,160 --> 00:23:06,520
make them very difficult, but they've traditionally been fairly poor in terms of creating

206
00:23:06,520 --> 00:23:12,200
comprehension and they've been much more about memorization. So don't prepare for

207
00:23:12,200 --> 00:23:18,520
monastic schools are still very much like that. Like a novice is learned poly and Thailand.

208
00:23:19,240 --> 00:23:23,960
It's ridiculous how little knowledge they come out with. They've memorized everything and they

209
00:23:23,960 --> 00:23:29,320
can pass the exams, but they know very very little. And as a result of teachers, if you ask the

210
00:23:29,320 --> 00:23:34,360
teachers the question, they're like a parent, all they can do is parent back what they've learned.

211
00:23:34,360 --> 00:23:40,200
They can't analyze and give you an explanation. Very few of them, some can. They're the really

212
00:23:40,200 --> 00:23:45,080
good ones and the ones who went on I think. There's a couple that I know who went on and did

213
00:23:45,080 --> 00:23:51,880
the really high poly studies. So they're serious and they're more open-minded and more thoughtful.

214
00:23:58,120 --> 00:24:01,480
But Asian people don't stress like we do. It's interesting. I mean, it's a gross

215
00:24:01,480 --> 00:24:07,160
generalization, but the societies tend to be far less stressed. I don't know Japan and Korea

216
00:24:07,160 --> 00:24:11,160
are probably different. I heard they can get quite stressed. But I think in a different way,

217
00:24:11,880 --> 00:24:18,520
they still see much more together. Japan is weird that way. I've heard terrible things about

218
00:24:19,400 --> 00:24:24,840
how they snap, not snap, but like when they have parties and go to extremes.

219
00:24:25,400 --> 00:24:29,800
People in Japan, from what I've heard, they work a lot of hours and they don't even take off the

220
00:24:29,800 --> 00:24:37,560
days that they're allowed to take off. A lot of stress that way, they're very competitive.

221
00:24:42,040 --> 00:24:47,240
That's probably the exception to the rule. The Southeast Asian maybe is what I'm thinking of.

222
00:24:47,240 --> 00:24:54,600
Thailand, Laos, Cambodia, I don't know about Burma, Sri Lanka, these Buddhist countries.

223
00:24:54,600 --> 00:25:04,440
Buddhist countries, you know, we're pretty laid back. It's not, it's just a generalization. People

224
00:25:04,440 --> 00:25:13,560
do stress, but not the way here. You know, there's not nearly, not nearly the concept of taking

225
00:25:13,560 --> 00:25:20,040
drugs or taking medication, you know, like antidepressants or anti-anxiety. I'm sure there

226
00:25:20,040 --> 00:25:26,920
are people taking them. But as far as I know, I mean, it's just funny because we've just,

227
00:25:26,920 --> 00:25:30,520
we've, in the West, we've come to accept it. It's socially acceptable.

228
00:25:32,280 --> 00:25:37,960
Where in Asia there's a culture, instead, to deal with it, to help people deal with it. We've got

229
00:25:37,960 --> 00:25:44,440
a support group and people are social cues are there to keep people from needing to take the

230
00:25:44,440 --> 00:25:51,880
medication. But in the West, since medication is the acceptable thing, I would argue that it is

231
00:25:51,880 --> 00:25:55,240
more necessary to take it for those people because they don't have the support network. They

232
00:25:55,240 --> 00:26:03,800
don't have the other means. That's an interesting argument that part of keeping people from

233
00:26:03,800 --> 00:26:13,480
needing to take drugs is the society that we live in, having in a society that supports people

234
00:26:13,480 --> 00:26:22,600
who don't need, who are depressed. But we don't. We don't believe about that. And we've come to,

235
00:26:22,600 --> 00:26:27,720
I think pharmaceutical industries have really pushed this idea that better living through chemistry

236
00:26:27,720 --> 00:26:34,840
really. That never really took off in the Asian identity, not in the same way and more.

237
00:26:34,840 --> 00:26:44,120
It's amazing with the drug commercials. I watch one hour of TV a day. It's the news. And every

238
00:26:44,120 --> 00:26:50,920
commercial is for prescription medication. I'm in Florida. My mother's watching the news. I

239
00:26:50,920 --> 00:26:57,080
see it in it. Yeah. Yeah. And they just make it look like so much fun to be having this

240
00:26:57,080 --> 00:27:01,720
medication of that medication. Then they give all the side effects, which are up to an including

241
00:27:01,720 --> 00:27:10,040
death. But obviously people are willing to take that chance. Yeah. I mean, it's like those warnings

242
00:27:10,040 --> 00:27:16,840
labels on on cigarettes. We were talking about this in the other day when I was talking to this

243
00:27:16,840 --> 00:27:23,960
smoker and these smokers. Because he said cigarettes are more prefers cigarettes over this vaporizer

244
00:27:23,960 --> 00:27:30,600
stuff. And so that's like like you become conditioned to like things that are actually kind of

245
00:27:30,600 --> 00:27:36,680
horrible, you know, inhaling smoke. And I said, yeah, that's it. And I said, so even with those labels,

246
00:27:36,680 --> 00:27:40,280
there was something somehow at some point I said, even with those labels. And I said, I bet you

247
00:27:40,280 --> 00:27:44,280
probably look forward to you probably associate the labels with your clinging, you know,

248
00:27:44,920 --> 00:27:48,920
associated with your addiction. So when you see these terrible labels, I don't know if you have them

249
00:27:48,920 --> 00:27:54,040
in America. You have these labels with people with cancer, lip cancer or mouth cancer

250
00:27:54,040 --> 00:28:01,480
from smoking. Do you have those? I don't know. I haven't looked at cigarettes in a very long time.

251
00:28:01,480 --> 00:28:07,080
They have these graphic logos pictures of people on the on the packs on the cigarettes.

252
00:28:07,080 --> 00:28:13,320
There were TV commercials like that with people who had lost their their vocal cords and things and

253
00:28:14,120 --> 00:28:19,640
had all sorts of horrible things happen to them. I don't know if they still have those, but

254
00:28:19,640 --> 00:28:29,240
they were pretty graphic. We have sprayed fire, let us end on that note.

255
00:28:32,440 --> 00:28:39,240
I'll see you about tomorrow or you'll see you'll all see us tomorrow. Thank you Robin for

256
00:28:39,240 --> 00:28:54,520
helping out. Thank you. Thank you, Dante. Good night.

