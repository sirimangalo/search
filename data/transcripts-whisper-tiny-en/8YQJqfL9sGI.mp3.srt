1
00:00:00,000 --> 00:00:05,000
Okay, and we're live.

2
00:00:05,000 --> 00:00:07,000
So welcome, everyone.

3
00:00:07,000 --> 00:00:12,000
We're going to try to study the new opposite today.

4
00:00:12,000 --> 00:00:15,000
I'm just going to try yesterday and failed.

5
00:00:15,000 --> 00:00:20,000
That was a not even a technical failure that was a human error

6
00:00:20,000 --> 00:00:23,000
or part of the internet network.

7
00:00:23,000 --> 00:00:27,000
Let's get a shutdown.

8
00:00:27,000 --> 00:00:35,000
So, yeah, tonight we'll be studying Maji Minikaya's to 25 new opposite to the bait.

9
00:00:35,000 --> 00:00:44,000
And format as always is, we will start by reciting the poly.

10
00:00:44,000 --> 00:00:50,000
Maybe half or all of this we'll see how much time we have.

11
00:00:50,000 --> 00:01:03,000
And then we will study the English reading it and explaining aspects of it that need to be explained.

12
00:01:03,000 --> 00:01:08,000
Maybe in the future we could set up a Q&A,

13
00:01:08,000 --> 00:01:11,000
turn on this Q&A thing, see how that works.

14
00:01:11,000 --> 00:01:16,000
If Q&A for the students that need to be interesting.

15
00:01:16,000 --> 00:01:19,000
Anyway, not tonight, because it's too late.

16
00:01:19,000 --> 00:01:23,000
You can't turn it on after you started to broadcast.

17
00:01:23,000 --> 00:01:28,000
So, here I'm opening it.

18
00:01:28,000 --> 00:01:29,000
Okay, there it is.

19
00:01:29,000 --> 00:01:32,000
So, I turn this feature on and off.

20
00:01:32,000 --> 00:01:34,000
Yeah, so we can't do it yet.

21
00:01:34,000 --> 00:01:36,000
Tomorrow we'll try it.

22
00:01:36,000 --> 00:01:41,000
Tonight we will start with...

23
00:01:41,000 --> 00:01:46,000
Right in with the...

24
00:01:46,000 --> 00:01:49,000
Stick to a standard setup.

25
00:01:49,000 --> 00:01:51,000
Tomorrow we'll try to have a question and answer.

26
00:01:51,000 --> 00:01:56,000
So, if you show up at 7pm, win a big time.

27
00:01:56,000 --> 00:02:02,000
If you're on YouTube or Google Plus, you should be able to see us on my YouTube channel.

28
00:02:02,000 --> 00:02:05,000
Tomorrow, I think, no, tomorrow is Friday.

29
00:02:05,000 --> 00:02:08,000
We won't be having it, I don't think.

30
00:02:08,000 --> 00:02:12,000
And the preferred and besides Friday, anyway.

31
00:02:12,000 --> 00:02:14,000
If not, you'll be seeing this on YouTube.

32
00:02:14,000 --> 00:02:15,000
Okay, so it's 7 o'clock.

33
00:02:15,000 --> 00:02:17,000
We'll set the screen chair.

34
00:02:17,000 --> 00:02:24,000
And here we go with our study session.

35
00:02:24,000 --> 00:02:45,000
We'll start with our study session.

36
00:02:45,000 --> 00:02:55,000
We'll start with our study session.

37
00:02:55,000 --> 00:03:19,000
We'll start with our study session.

38
00:03:19,000 --> 00:03:29,000
We'll start with our study session.

39
00:03:29,000 --> 00:03:40,000
We'll start with our study session.

40
00:03:40,000 --> 00:03:50,000
We'll start with our study session.

41
00:03:50,000 --> 00:04:14,000
We'll start with our study session.

42
00:04:14,000 --> 00:04:21,000
We'll start with our study session.

43
00:04:21,000 --> 00:04:30,000
We'll start with our study session.

44
00:04:30,000 --> 00:04:54,000
We'll start with our study session.

45
00:04:54,000 --> 00:05:04,000
We'll start with our study session.

46
00:05:04,000 --> 00:05:28,000
We'll start with our study session.

47
00:05:28,000 --> 00:05:52,000
We'll start with our study session.

48
00:05:52,000 --> 00:06:16,000
We'll start with our study session.

49
00:06:16,000 --> 00:06:26,000
We'll start with our study session.

50
00:06:26,000 --> 00:06:45,000
We'll start with our study session.

51
00:06:45,000 --> 00:06:55,000
We'll start with our study session.

52
00:06:55,000 --> 00:07:14,000
We'll start with our study session.

53
00:07:14,000 --> 00:07:38,000
We'll start with our study session.

54
00:07:38,000 --> 00:08:02,000
We'll start with our study session.

55
00:08:02,000 --> 00:08:26,000
We'll start with our study session.

56
00:08:26,000 --> 00:08:50,000
We'll start with our study session.

57
00:08:50,000 --> 00:09:14,000
We'll start with our study session.

58
00:09:14,000 --> 00:09:38,000
We'll start with our study session.

59
00:09:38,000 --> 00:10:02,000
We'll start with our study session.

60
00:10:02,000 --> 00:10:26,000
We'll start with our study session.

61
00:10:26,000 --> 00:10:50,000
We'll start with our study session.

62
00:10:50,000 --> 00:11:14,000
We'll start with our study session.

63
00:11:14,000 --> 00:11:38,000
We'll start with our study session.

64
00:11:38,000 --> 00:12:02,000
We'll start with our study session.

65
00:12:02,000 --> 00:12:26,000
We'll start with our study session.

66
00:12:26,000 --> 00:12:50,000
We'll start with our study session.

67
00:12:50,000 --> 00:13:14,000
We'll start with our study session.

68
00:13:14,000 --> 00:13:38,000
We'll start with our study session.

69
00:13:38,000 --> 00:14:02,000
We'll start with our study session.

70
00:14:02,000 --> 00:14:26,000
We'll start with our study session.

71
00:14:26,000 --> 00:14:50,000
We'll start with our study session.

72
00:14:50,000 --> 00:15:14,000
We'll start with our study session.

73
00:15:14,000 --> 00:15:42,000
We'll start with our study session.

74
00:15:42,000 --> 00:16:10,000
We'll start with our study session.

75
00:16:10,000 --> 00:16:34,000
We'll start with our study session.

76
00:16:34,000 --> 00:17:02,000
We'll start with our study session.

77
00:17:02,000 --> 00:17:26,000
We'll start with our study session.

78
00:17:26,000 --> 00:17:50,000
We'll start with our study session.

79
00:17:50,000 --> 00:18:16,000
We'll start with our study session.

80
00:18:16,000 --> 00:18:40,000
We'll start with our study session.

81
00:18:40,000 --> 00:19:08,000
We'll start with our study session.

82
00:19:08,000 --> 00:19:32,000
We'll start with our study session.

83
00:19:32,000 --> 00:19:56,000
We'll start with our study session.

84
00:19:56,000 --> 00:20:20,000
We'll start with our study session.

85
00:20:20,000 --> 00:20:48,000
We'll start with our study session.

86
00:20:48,000 --> 00:21:12,000
We'll start with our study session.

87
00:21:12,000 --> 00:21:36,000
We'll start with our study session.

88
00:21:36,000 --> 00:22:00,000
We'll start with our study session.

89
00:22:00,000 --> 00:22:26,000
We'll start with our study session.

90
00:22:26,000 --> 00:22:52,000
We'll start with our study session.

91
00:22:52,000 --> 00:23:18,000
We'll start with our study session.

92
00:23:18,000 --> 00:23:44,000
We'll start with our study session.

93
00:23:44,000 --> 00:24:10,000
We'll start with our study session.

94
00:24:10,000 --> 00:24:36,000
We'll start with our study session.

95
00:24:36,000 --> 00:25:02,000
We'll start with our study session.

96
00:25:02,000 --> 00:25:28,000
We'll start with our study session.

97
00:25:28,000 --> 00:25:54,000
We'll start with our study session.

98
00:25:54,000 --> 00:26:20,000
We'll start with our study session.

99
00:26:20,000 --> 00:26:46,000
We'll start with our study session.

100
00:26:46,000 --> 00:27:12,000
We'll start with our study session.

101
00:27:12,000 --> 00:27:38,000
We'll start with our study session.

102
00:27:38,000 --> 00:28:04,000
We'll start with our study session.

103
00:28:04,000 --> 00:28:30,000
We'll start with our study session.

104
00:28:30,000 --> 00:28:56,000
We'll start with our study session.

105
00:28:56,000 --> 00:29:22,000
We'll start with our study session.

106
00:29:22,000 --> 00:29:51,000
We'll start with our study session.

107
00:29:51,000 --> 00:30:17,000
We'll start with our study session.

108
00:30:17,000 --> 00:30:43,000
We'll start with our study session.

109
00:30:43,000 --> 00:31:09,000
We'll start with our study session.

110
00:31:09,000 --> 00:31:35,000
We'll start with our study session.

111
00:31:35,000 --> 00:32:01,000
We'll start with our study session.

112
00:32:01,000 --> 00:32:30,000
We'll start with our study session.

113
00:32:30,000 --> 00:32:56,000
We'll start with our study session.

114
00:32:56,000 --> 00:33:22,000
We'll start with our study session.

115
00:33:22,000 --> 00:33:48,000
We'll start with our study session.

116
00:33:48,000 --> 00:34:17,000
We'll start with our study session.

117
00:34:17,000 --> 00:34:43,000
We'll start with our study session.

118
00:34:43,000 --> 00:35:09,000
We'll start with our study session.

119
00:35:09,000 --> 00:35:35,000
We'll start with our study session.

120
00:35:35,000 --> 00:36:01,000
We'll start with our study session.

121
00:36:01,000 --> 00:36:27,000
We'll start with our study session.

122
00:36:27,000 --> 00:36:53,000
We'll start with our study session.

123
00:36:53,000 --> 00:37:19,000
We'll start with our study session.

