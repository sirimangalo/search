1
00:00:00,000 --> 00:00:01,880
Hey, welcome back to Ask A Monk.

2
00:00:01,880 --> 00:00:06,960
Next question comes from BD-951, who asks,

3
00:00:06,960 --> 00:00:10,840
should one restrict the amount of sleep to a certain amount,

4
00:00:10,840 --> 00:00:13,400
or does this occur naturally through the practice?

5
00:00:13,400 --> 00:00:18,680
Can you define what a high and luxurious bed is?

6
00:00:18,680 --> 00:00:21,240
And what is its alternative?

7
00:00:21,240 --> 00:00:23,760
Thanks.

8
00:00:23,760 --> 00:00:26,320
Restrict the amount of sleep, yeah.

9
00:00:26,320 --> 00:00:28,560
In a intensive meditation course,

10
00:00:28,560 --> 00:00:34,040
it's recommended not to sleep more than four hours at night.

11
00:00:34,040 --> 00:00:36,120
And the rules that I generally give to people

12
00:00:36,120 --> 00:00:40,400
are, it's against the rule, it's against the rules

13
00:00:40,400 --> 00:00:41,920
to sleep more than six hours.

14
00:00:44,760 --> 00:00:48,000
But we usually make some leeway for people coming fresh,

15
00:00:48,000 --> 00:00:52,080
especially Western people who are not used to this kind of a regime.

16
00:00:52,080 --> 00:00:53,800
But after a few days, we expect that you

17
00:00:53,800 --> 00:00:57,520
get down to not more than six hours of sleep

18
00:00:57,520 --> 00:01:03,080
with a preference for four or five hours, if you can.

19
00:01:03,080 --> 00:01:08,040
The reason being for this is that our minds are not being

20
00:01:08,040 --> 00:01:10,440
excited, our minds are not being stressed.

21
00:01:10,440 --> 00:01:17,480
During the practice, we're in a fairly static state.

22
00:01:17,480 --> 00:01:20,680
So we don't need our bodies and our brains,

23
00:01:20,680 --> 00:01:24,400
don't need the same amount of rest and recuperation

24
00:01:24,400 --> 00:01:26,480
that we might get otherwise, because in fact,

25
00:01:26,480 --> 00:01:31,040
meditation is a form of rest and recuperation

26
00:01:31,040 --> 00:01:33,440
for the body and the brain.

27
00:01:33,440 --> 00:01:36,320
So in fact, some people will go without sleep,

28
00:01:36,320 --> 00:01:39,320
and people monks have been known to go without sleep

29
00:01:39,320 --> 00:01:45,960
for long periods of time, which is quite a fun thing to do.

30
00:01:45,960 --> 00:01:49,680
Yeah, we monks have to find fun things to do as well.

31
00:01:49,680 --> 00:01:51,920
One of them is to stay up all night in the forest

32
00:01:51,920 --> 00:01:56,640
and practice meditation.

33
00:01:56,640 --> 00:01:59,640
So but the other question, OK, does this

34
00:01:59,640 --> 00:02:02,320
occur naturally through the practice?

35
00:02:02,320 --> 00:02:05,000
Yeah, I'd say it does occur naturally.

36
00:02:05,000 --> 00:02:07,280
You have to be a little bit strict with yourself,

37
00:02:07,280 --> 00:02:12,400
though, because we're very good at waffling.

38
00:02:12,400 --> 00:02:13,640
It's like you can go either way.

39
00:02:13,640 --> 00:02:15,160
If you force yourself to get up, you'll

40
00:02:15,160 --> 00:02:16,920
find, OK, I'm wide awake.

41
00:02:16,920 --> 00:02:20,440
If you, there's a lie down, then you'll fall back

42
00:02:20,440 --> 00:02:22,120
to sleep.

43
00:02:22,120 --> 00:02:25,200
So you can't just expect it to suddenly come.

44
00:02:25,200 --> 00:02:27,400
You have to put the effort in, and you

45
00:02:27,400 --> 00:02:28,800
have to push yourself a little bit.

46
00:02:28,800 --> 00:02:34,280
Give yourself the jumpstart, otherwise it won't happen.

47
00:02:34,280 --> 00:02:37,840
But generally, definitely through the meditation,

48
00:02:37,840 --> 00:02:39,680
the thing is you should listen to what's happening.

49
00:02:39,680 --> 00:02:41,600
You'll find yourself waking up early,

50
00:02:41,600 --> 00:02:44,960
and you think, wow, it's too early, I shouldn't get up.

51
00:02:44,960 --> 00:02:47,440
That's ignoring the fact that it is affecting you.

52
00:02:47,440 --> 00:02:50,880
The meditation will wake you up early, and in fact,

53
00:02:50,880 --> 00:02:53,040
you can wake up without an alarm.

54
00:02:53,040 --> 00:02:56,520
You say, OK, tomorrow I'm going to wake up at 4am.

55
00:02:56,520 --> 00:02:58,240
You wake up at 4am, that kind of thing.

56
00:03:02,600 --> 00:03:06,480
And so the important thing is just to remember

57
00:03:06,480 --> 00:03:09,320
and to recognize and to follow that when you wake up,

58
00:03:09,320 --> 00:03:12,840
go for it, practice meditation, or they sit up and bed

59
00:03:12,840 --> 00:03:14,280
and do some sitting meditation.

60
00:03:14,280 --> 00:03:18,680
Go wash your face, or whatever.

61
00:03:18,680 --> 00:03:21,400
A high and luxurious bed, this is in reference

62
00:03:21,400 --> 00:03:24,840
to the eight precepts for Buddhist meditators.

63
00:03:24,840 --> 00:03:27,480
When we do intensive meditation courses,

64
00:03:27,480 --> 00:03:33,600
we'll keep eight rules of conduct, based on the first five

65
00:03:33,600 --> 00:03:36,320
for ordinary life, which is not to kill, not to steal,

66
00:03:36,320 --> 00:03:39,520
not to cheat, not to lie, not to take drugs in alcohol.

67
00:03:39,520 --> 00:03:44,040
But the number three changes to total celibacy,

68
00:03:44,040 --> 00:03:48,360
having no romantic or sexual engagement or activity.

69
00:03:48,360 --> 00:03:52,240
Number six, seven, and eight, six is not

70
00:03:52,240 --> 00:03:55,440
to eat in the afternoon or eat outside of the morning hour.

71
00:03:55,440 --> 00:03:59,600
So we eat from 6am till noon, and that's it.

72
00:03:59,600 --> 00:04:04,680
Number seven is not to engage in entertainment

73
00:04:04,680 --> 00:04:07,360
on the one side and beautification on the other.

74
00:04:07,360 --> 00:04:12,080
And number eight is to abstain from high and luxurious beds.

75
00:04:12,080 --> 00:04:17,880
Really this basically means anything that's going to support

76
00:04:17,880 --> 00:04:19,080
a longer sleep cycle.

77
00:04:19,080 --> 00:04:25,520
We want something that is going to allow

78
00:04:25,520 --> 00:04:27,760
for the minimum amount of sleep possible.

79
00:04:27,760 --> 00:04:31,600
So when I first went to meditate, we had to sleep on a wooden floor

80
00:04:31,600 --> 00:04:40,680
with a very thin cloth filled cotton filled mattress.

81
00:04:40,680 --> 00:04:44,000
And I couldn't get by because I normally sleep on my side.

82
00:04:44,000 --> 00:04:48,960
I had to sleep on my back because I'm so thin.

83
00:04:48,960 --> 00:04:54,160
And otherwise, my bone would stick in through to the floor.

84
00:04:54,160 --> 00:04:58,000
And that was great because it allows only the minimum amount

85
00:04:58,000 --> 00:04:58,440
of sleep.

86
00:04:58,440 --> 00:05:02,320
You can sleep on, I've slept on concrete.

87
00:05:02,320 --> 00:05:05,160
And I knew it was OK because I knew that when the time comes,

88
00:05:05,160 --> 00:05:06,760
my body's going to fall asleep.

89
00:05:06,760 --> 00:05:09,280
But it doesn't really care about the cold.

90
00:05:09,280 --> 00:05:10,320
It doesn't care about the wind.

91
00:05:10,320 --> 00:05:12,560
It doesn't care about the hardness.

92
00:05:12,560 --> 00:05:13,600
It's going to fall asleep.

93
00:05:13,600 --> 00:05:17,120
But it'll get the minimum amount necessary and then wake up.

94
00:05:17,120 --> 00:05:21,960
So the alternative should be sleeping on the floor.

95
00:05:21,960 --> 00:05:24,640
And even sleeping on the carpet is OK.

96
00:05:24,640 --> 00:05:27,640
And I think sleeping on cement for any period of time

97
00:05:27,640 --> 00:05:33,680
is actually a bad idea because eventually it'll

98
00:05:33,680 --> 00:05:37,640
get through to your lungs and you can make you sick.

99
00:05:37,640 --> 00:05:39,720
So you should have something like plastic

100
00:05:39,720 --> 00:05:43,200
on between you and the some kind of insulation

101
00:05:43,200 --> 00:05:46,600
between you and the stone, sleeping on a wood floor.

102
00:05:46,600 --> 00:05:50,800
But I think carpet is probably the best happy medium.

103
00:05:50,800 --> 00:05:53,880
I was talking to someone about how I used to sleep

104
00:05:53,880 --> 00:05:59,800
on the wooden floor and he said, the carpet is bad enough.

105
00:05:59,800 --> 00:06:05,040
And he thought it was too much for him.

106
00:06:05,040 --> 00:06:07,440
And it was a lot for him to just be able to sleep on the carpet.

107
00:06:07,440 --> 00:06:10,720
So I would suggest that's the best way to go about it.

108
00:06:10,720 --> 00:06:12,960
And we only do this.

109
00:06:12,960 --> 00:06:14,640
Ordinary people will only do this when

110
00:06:14,640 --> 00:06:18,680
they are in an intensive meditation course

111
00:06:18,680 --> 00:06:23,640
or on the quarter moon or the quarters,

112
00:06:23,640 --> 00:06:26,360
the half moon and the full moon and the empty moon.

113
00:06:26,360 --> 00:06:29,160
So the four times of a lunar month

114
00:06:29,160 --> 00:06:31,440
when they will keep these meditative precepts

115
00:06:31,440 --> 00:06:36,640
and try to practice meditation once every week

116
00:06:36,640 --> 00:06:38,880
on a more intensive level.

117
00:06:38,880 --> 00:06:40,400
And so they'll keep these rows from monks.

118
00:06:40,400 --> 00:06:43,760
We keep them ice, sleep on the floor every day.

119
00:06:43,760 --> 00:06:46,280
I've slept on the floor for a long time.

120
00:06:46,280 --> 00:06:52,080
And many monks have come to keep beds.

121
00:06:52,080 --> 00:06:54,480
But it's always the first thing that goes out of my room

122
00:06:54,480 --> 00:06:55,200
is the bed.

123
00:06:55,200 --> 00:06:57,200
It just takes up unnecessary room.

124
00:06:57,200 --> 00:07:00,960
And yeah, it slows you down in your meditation

125
00:07:00,960 --> 00:07:04,320
because you find yourself easily falling,

126
00:07:04,320 --> 00:07:07,840
tipping to the one side of falling back asleep

127
00:07:07,840 --> 00:07:10,640
even when you wake up early.

128
00:07:10,640 --> 00:07:13,640
So nice and comfortable and warm and it's

129
00:07:13,640 --> 00:07:16,240
cold outside of the bed and so on.

130
00:07:16,240 --> 00:07:18,560
So there's the answer to your question.

131
00:07:18,560 --> 00:07:36,280
Thanks for that and keep practicing.

