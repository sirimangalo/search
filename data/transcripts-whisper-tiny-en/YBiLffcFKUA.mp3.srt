1
00:00:00,000 --> 00:00:06,000
How is a layperson supposed to deal with urges to practice down onto the point of perfection?

2
00:00:06,000 --> 00:00:10,000
I read somewhere about Buddhist teachings for laypeople

3
00:00:10,000 --> 00:00:14,000
or dividing wealth to four portions, bhoga vibhanga.

4
00:00:14,000 --> 00:00:17,000
One portion for supporting self-independence.

5
00:00:17,000 --> 00:00:24,000
Two portions for investment and another portion for future needs.

6
00:00:24,000 --> 00:00:30,000
Yes, that's the one teaching.

7
00:00:30,000 --> 00:00:35,000
Maybe that's the signal a lot as if I can remember.

8
00:00:35,000 --> 00:00:46,000
So I believe it's the portion for future needs that can be interpreted as a gift.

9
00:00:46,000 --> 00:00:49,000
You have to understand that it's actually two different things.

10
00:00:49,000 --> 00:00:55,000
They're the teaching on splitting up your wealth.

11
00:00:55,000 --> 00:00:57,000
It's not a darmic teaching.

12
00:00:57,000 --> 00:00:59,000
It's a teaching. It's a practical teaching.

13
00:00:59,000 --> 00:01:04,000
It's a teaching about a manage your household.

14
00:01:04,000 --> 00:01:09,000
Because I think the commentary or the tradition is that the fourth one there,

15
00:01:09,000 --> 00:01:12,000
what are your future needs? Your future needs is to go to heaven.

16
00:01:12,000 --> 00:01:16,000
In order to go to heaven you have to be generous.

17
00:01:16,000 --> 00:01:20,000
Or is to practice meditation and you need to renounce.

18
00:01:20,000 --> 00:01:24,000
So therefore you have to give in order to renounce.

19
00:01:24,000 --> 00:01:26,000
But I don't think that's necessary.

20
00:01:26,000 --> 00:01:29,000
That's one quite proper interpretation.

21
00:01:29,000 --> 00:01:34,000
But another interpretation might be that this refers to the wealth that you keep for yourself.

22
00:01:34,000 --> 00:01:37,000
It doesn't refer to the wealth that you give up.

23
00:01:37,000 --> 00:01:42,000
If you want to manage your household properly, you should,

24
00:01:42,000 --> 00:01:47,000
in the Buddha saying, you should divide your wealth,

25
00:01:47,000 --> 00:01:52,000
that's what you keep for yourself up into four portions.

26
00:01:52,000 --> 00:01:56,000
And so it's up to you how much of your wealth,

27
00:01:56,000 --> 00:01:59,000
how much of your income that is,

28
00:01:59,000 --> 00:02:02,000
and how much of your income you give away

29
00:02:02,000 --> 00:02:12,000
for the two purposes of generosity and renunciation.

30
00:02:12,000 --> 00:02:14,000
But it's an interesting question.

31
00:02:14,000 --> 00:02:19,000
I mean, if a lay person, if it's someone who knows that they don't have the opportunity to ordain,

32
00:02:19,000 --> 00:02:24,000
then they have the urge to give everything away and ordain.

33
00:02:24,000 --> 00:02:26,000
They can't do it.

34
00:02:26,000 --> 00:02:29,000
Or the idea is that they can't do it.

35
00:02:29,000 --> 00:02:34,000
So there are, I think, two different answers, I suppose.

36
00:02:34,000 --> 00:02:37,000
One answer is find a way to do it.

37
00:02:37,000 --> 00:02:42,000
If a person really has the urge to go forth to give up and to go forth,

38
00:02:42,000 --> 00:02:45,000
it's not necessary to become a monk.

39
00:02:45,000 --> 00:02:51,000
One can go forth and live a life of poverty and simplicity

40
00:02:51,000 --> 00:02:56,000
and be devoted to the idea of having nothing,

41
00:02:56,000 --> 00:03:00,000
and of going according to cause and effect,

42
00:03:00,000 --> 00:03:05,000
if one has the, because you'll find that you go rather than by wealth,

43
00:03:05,000 --> 00:03:09,000
we actually live by karma and cause and effect.

44
00:03:09,000 --> 00:03:13,000
And if you have the requisite conditions to be alive as a human being,

45
00:03:13,000 --> 00:03:17,000
and to be fed and so on, you will receive food and sustenance

46
00:03:17,000 --> 00:03:20,000
and the ability to live.

47
00:03:20,000 --> 00:03:22,000
It might not be what you're used to,

48
00:03:22,000 --> 00:03:24,000
but that's really the point.

49
00:03:24,000 --> 00:03:27,000
The point is to give up and to let go.

50
00:03:27,000 --> 00:03:28,000
This is what the Buddha did.

51
00:03:28,000 --> 00:03:30,000
He didn't become a monk or so on.

52
00:03:30,000 --> 00:03:35,000
He just left and went to live in a total poverty,

53
00:03:35,000 --> 00:03:39,000
abject poverty, and this is from being a prince.

54
00:03:39,000 --> 00:03:46,000
And as a result, he went on arms around the first time in the story,

55
00:03:46,000 --> 00:03:50,000
is that the food that he got was so disgusting to him.

56
00:03:50,000 --> 00:03:53,000
It felt as though his stomach was going to come out of his throat.

57
00:03:53,000 --> 00:03:55,000
His stomach was like that revolting.

58
00:03:55,000 --> 00:03:58,000
He was ready to throw up,

59
00:03:58,000 --> 00:04:03,000
but it was so violent that it was like his stomach was going to come out of his throat.

60
00:04:03,000 --> 00:04:06,000
And so he reprimanded himself.

61
00:04:06,000 --> 00:04:08,000
He said, this isn't why you haven't gone forth

62
00:04:08,000 --> 00:04:13,000
for the purpose of your stomach,

63
00:04:13,000 --> 00:04:15,000
for the benefit of your stomach.

64
00:04:15,000 --> 00:04:18,000
You've gone forth for the benefit of your mind.

65
00:04:18,000 --> 00:04:24,000
And so he suppressed this desire to vomit

66
00:04:24,000 --> 00:04:26,000
and actually ate the food,

67
00:04:26,000 --> 00:04:29,000
because it was so repulsive to his body,

68
00:04:29,000 --> 00:04:31,000
having been fed on royal food

69
00:04:31,000 --> 00:04:34,000
and now having the most base of foods,

70
00:04:34,000 --> 00:04:37,000
because in India, before the time of the Buddha,

71
00:04:37,000 --> 00:04:39,000
you can imagine what arms around would have been like.

72
00:04:39,000 --> 00:04:41,000
It wouldn't have been maybe a lot,

73
00:04:41,000 --> 00:04:47,000
maybe a lot like some of the arms or arms I've had in places like Northern Thailand.

74
00:04:47,000 --> 00:04:53,000
The other way is to bite your tongue or bite your tongue,

75
00:04:53,000 --> 00:04:56,000
I suppose, cleanse your teeth and put up with it

76
00:04:56,000 --> 00:04:59,000
and find a halfway,

77
00:04:59,000 --> 00:05:02,000
find a way that you can live in the household life

78
00:05:02,000 --> 00:05:05,000
because for many people it's not possible

79
00:05:05,000 --> 00:05:07,000
for whatever reason.

80
00:05:07,000 --> 00:05:10,000
I think for many people there's not the strong urge to do it.

81
00:05:10,000 --> 00:05:13,000
So the urge to give everything up is not strong enough

82
00:05:13,000 --> 00:05:15,000
to compel them to actually do it.

83
00:05:15,000 --> 00:05:17,000
But for some people their parents,

84
00:05:17,000 --> 00:05:20,000
they have children who are dependent on them and so on.

85
00:05:20,000 --> 00:05:24,000
And so you develop patience and you do as best you can,

86
00:05:24,000 --> 00:05:29,000
and that's where the managing your wealth comes in as an important teaching.

87
00:05:29,000 --> 00:05:32,000
You need wealth, so you find out how much,

88
00:05:32,000 --> 00:05:36,000
another way of looking at this is once you divide it up into four portions

89
00:05:36,000 --> 00:05:38,000
and there's still some left,

90
00:05:38,000 --> 00:05:41,000
that's what you use for charity.

91
00:05:41,000 --> 00:05:44,000
That's what you should use to develop generosity and renunciation.

92
00:05:44,000 --> 00:05:49,000
So you don't have a fifth portion for your entertainment and so on.

93
00:05:49,000 --> 00:05:53,000
Or a fifth portion for vacations or whatever.

94
00:05:53,000 --> 00:05:56,000
If you want to be very strict with yourself,

95
00:05:56,000 --> 00:06:00,000
you have only enough for these three things in four portions

96
00:06:00,000 --> 00:06:05,000
and the rest you rid yourself of.

97
00:06:05,000 --> 00:06:10,000
And you stop from thinking about it.

98
00:06:10,000 --> 00:06:14,000
So I hope that helps.

