1
00:00:00,000 --> 00:00:03,600
Is walking meditation necessary?

2
00:00:03,600 --> 00:00:06,640
Is walking meditation necessary?

3
00:00:06,640 --> 00:00:12,480
No, there is no specific posture that is necessary in meditation.

4
00:00:12,480 --> 00:00:17,040
Walking meditation is a means of keeping the body in shape,

5
00:00:17,040 --> 00:00:22,560
as well as cultivating effort to balance excessive concentration

6
00:00:22,560 --> 00:00:24,560
that can lead to driesiness.

7
00:00:24,560 --> 00:00:29,120
If one is unable to practice walking meditation properly

8
00:00:29,120 --> 00:00:32,880
due to injuries, sickness, or disability,

9
00:00:32,880 --> 00:00:37,760
it is recommended to use a cane or stationary object

10
00:00:37,760 --> 00:00:43,040
such as a wall, railing, or table to balance oneself,

11
00:00:43,040 --> 00:00:46,400
noting the movements of the stabilizing hand.

12
00:00:46,400 --> 00:00:50,720
If one is unable to practice walking meditation at all,

13
00:00:50,720 --> 00:00:55,120
one can improvise by performing other bodily movements,

14
00:00:55,120 --> 00:00:59,840
like moving one's legs, wheeling a wheelchair, etc.

15
00:00:59,840 --> 00:01:02,640
For the purpose of keeping the body healthy

16
00:01:02,640 --> 00:01:12,640
and cultivating effort while still maintaining mindfulness.

