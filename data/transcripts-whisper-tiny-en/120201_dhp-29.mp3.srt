1
00:00:00,000 --> 00:00:05,000
So, and welcome back to our study of the Dhamabanda.

2
00:00:05,000 --> 00:00:12,000
Today, we continue with verse number 29, which reads as follows.

3
00:00:12,000 --> 00:00:38,000
One who is heedful when others are heedless,

4
00:00:38,000 --> 00:00:46,000
who when others sleep is full of effort, full of energy.

5
00:00:46,000 --> 00:00:55,000
Just like a horse with swift, a swift-footed horse leaves behind a weak horse,

6
00:00:55,000 --> 00:00:59,000
or a horse without strength on Balasana.

7
00:00:59,000 --> 00:01:04,000
So, the wise leave behind all others.

8
00:01:04,000 --> 00:01:17,000
This was in regards to a fairly short story, but quite an interesting one.

9
00:01:17,000 --> 00:01:23,000
It has interesting implications on our practice in general,

10
00:01:23,000 --> 00:01:27,000
and our practice in a general sense of why we're doing what we're doing,

11
00:01:27,000 --> 00:01:29,000
and the importance of what we're doing.

12
00:01:29,000 --> 00:01:38,000
So, the story goes that there were two friends who came in and heard the Buddha's teaching,

13
00:01:38,000 --> 00:01:42,000
and became monks as a result of hearing the Buddha's teaching.

14
00:01:42,000 --> 00:01:50,000
They listened to it, and they realized that it was difficult to practice the Buddha's teaching as a layperson.

15
00:01:50,000 --> 00:01:56,000
So, they decided that they would undertake the formal training,

16
00:01:56,000 --> 00:02:03,000
and to dedicate themselves to this, because they realized that it was something of use in a benefit.

17
00:02:03,000 --> 00:02:11,000
And it happened that one of them having taken the meditation teachings of the Buddha

18
00:02:11,000 --> 00:02:15,000
took it quite seriously, and realized that this meditation was something that was a benefit,

19
00:02:15,000 --> 00:02:21,000
and it was for the purposes of actually putting it into practice,

20
00:02:21,000 --> 00:02:24,000
and that it had some result that was a benefit.

21
00:02:24,000 --> 00:02:27,000
And so, he undertook quite strenuous practice.

22
00:02:27,000 --> 00:02:32,000
During the whole day, he would practice walking and sitting,

23
00:02:32,000 --> 00:02:38,000
and then at night he would do walking and sitting for the first watch,

24
00:02:38,000 --> 00:02:41,000
the first four hours until 10 p.m. or so.

25
00:02:41,000 --> 00:02:45,000
And then he would lie down and sleep for four hours,

26
00:02:45,000 --> 00:02:50,000
and then at 2 a.m. he would get up, and the third part of the night from 2 a.m. to 6 a.m.

27
00:02:50,000 --> 00:02:54,000
he would do walking and sitting again after he went for food,

28
00:02:54,000 --> 00:02:58,000
and did his other affairs,

29
00:02:58,000 --> 00:03:00,000
then he would come back and spend the day,

30
00:03:00,000 --> 00:03:03,000
day to day he would spend it in this way.

31
00:03:03,000 --> 00:03:06,000
So, he was quite intensive practice.

32
00:03:06,000 --> 00:03:14,000
The other monk was quite industrious as well, but in a different way.

33
00:03:14,000 --> 00:03:20,000
So, he would collect firewood and sweep the monastery,

34
00:03:20,000 --> 00:03:23,000
and organize this and organize that.

35
00:03:23,000 --> 00:03:29,000
And then in the evening, he would sit by the fire and talk with the lay people in the monastery,

36
00:03:29,000 --> 00:03:34,000
and the novices and all of the people who would come to listen to the teachings,

37
00:03:34,000 --> 00:03:39,000
he would sit around and joke with them and talk with them and chat with them.

38
00:03:39,000 --> 00:03:44,000
And he didn't get what his friend was doing,

39
00:03:44,000 --> 00:03:47,000
so he went to check on his friend later on,

40
00:03:47,000 --> 00:03:52,000
and he saw his friend at around 10 p.m. going into his room,

41
00:03:52,000 --> 00:03:55,000
or in his room lying down to sleep.

42
00:03:55,000 --> 00:03:57,000
And he thought, and he went and said to him,

43
00:03:57,000 --> 00:03:58,000
what is this all?

44
00:03:58,000 --> 00:03:59,000
You do all the time sleeping.

45
00:03:59,000 --> 00:04:01,000
He was all like, because it's all he saw.

46
00:04:01,000 --> 00:04:04,000
He never saw his friend doing anything, but lying down to sleep.

47
00:04:04,000 --> 00:04:06,000
So, at 10 p.m. he went in and he scolded him,

48
00:04:06,000 --> 00:04:11,000
and he said, you say that you're going to practice strenuously,

49
00:04:11,000 --> 00:04:14,000
and here you are lying down to sleep.

50
00:04:14,000 --> 00:04:16,000
And his friend didn't listen to him.

51
00:04:16,000 --> 00:04:19,000
He was saying, all you do is sit around and do nothing,

52
00:04:19,000 --> 00:04:21,000
because here he's thinking of himself like,

53
00:04:21,000 --> 00:04:24,000
I collect firewood, I'd wall this work in the monastery,

54
00:04:24,000 --> 00:04:28,000
and on top of that, I entertained the lay people,

55
00:04:28,000 --> 00:04:29,000
whatever he was thinking.

56
00:04:29,000 --> 00:04:32,000
He thought he was industrious, and his friend wasn't doing anything,

57
00:04:32,000 --> 00:04:35,000
except walking and sitting, and he didn't even really know it.

58
00:04:35,000 --> 00:04:37,000
What his friend was doing.

59
00:04:37,000 --> 00:04:40,000
Obviously, he thought he just lay around and slept all the time.

60
00:04:40,000 --> 00:04:44,000
So, they stayed together like this for some time,

61
00:04:44,000 --> 00:04:46,000
and eventually his story goes,

62
00:04:46,000 --> 00:04:49,000
that the friend who was industrious,

63
00:04:49,000 --> 00:04:52,000
who was dedicated to the meditation practice,

64
00:04:52,000 --> 00:04:53,000
became Naran.

65
00:04:53,000 --> 00:04:54,000
He became enlightened.

66
00:04:54,000 --> 00:04:57,000
He practiced to the point where he was able to free his mind

67
00:04:57,000 --> 00:05:00,000
from greed, from anger, and from delusion.

68
00:05:00,000 --> 00:05:04,000
And it's no wonder, considering the intensity of his practice.

69
00:05:04,000 --> 00:05:12,000
Just as someone who studies science or studies for a master's or a PhD,

70
00:05:12,000 --> 00:05:15,000
if they work hard and they study in it,

71
00:05:15,000 --> 00:05:19,000
and in no long time they'll be able to pass the exams.

72
00:05:19,000 --> 00:05:21,000
In the same way, he put the effort in,

73
00:05:21,000 --> 00:05:24,000
he put the work in, and so as a result,

74
00:05:24,000 --> 00:05:26,000
he was able to become enlightened.

75
00:05:26,000 --> 00:05:29,000
His friend, on the other hand, gained nothing,

76
00:05:29,000 --> 00:05:31,000
but he was very, very industrious.

77
00:05:31,000 --> 00:05:35,000
And so, at the end of the period, this reigns period, I suppose,

78
00:05:35,000 --> 00:05:37,000
and they went back to see the Buddha.

79
00:05:37,000 --> 00:05:38,000
And the Buddha asked them,

80
00:05:38,000 --> 00:05:41,000
so how did you spend your reigns? How did it go?

81
00:05:41,000 --> 00:05:45,000
And did you gain any attainment or something like that?

82
00:05:45,000 --> 00:05:49,000
And the monk who spent all his time sitting around,

83
00:05:49,000 --> 00:05:52,000
sitting around doing nothing, he said,

84
00:05:52,000 --> 00:06:02,000
could no one day, how could this one have gained anything?

85
00:06:02,000 --> 00:06:06,000
And he said, how could he be from, in what way could he be considered heatful?

86
00:06:06,000 --> 00:06:09,000
He just sat around sleeping all the time.

87
00:06:09,000 --> 00:06:12,000
He's talking about the wonders and lighten.

88
00:06:12,000 --> 00:06:16,000
And then the Buddha looked and said, what about you?

89
00:06:16,000 --> 00:06:19,000
And he said, me, I spent all my time sweeping

90
00:06:19,000 --> 00:06:22,000
and gathering firewood and so on and so on,

91
00:06:22,000 --> 00:06:25,000
and then he kind of realized, and he said,

92
00:06:25,000 --> 00:06:30,000
and then I just sat around and talked with the other laypeople.

93
00:06:30,000 --> 00:06:34,000
But I didn't sleep, I didn't spend all my time sleeping.

94
00:06:34,000 --> 00:06:38,000
And the Buddha looked at him and he was a very stern glance and said,

95
00:06:38,000 --> 00:06:51,000
do I'm a Buddha, sir? Or there's a Santike, so on and so on and so on.

96
00:06:51,000 --> 00:06:55,000
He said, you compared, compared with my son,

97
00:06:55,000 --> 00:06:58,000
and he's talking about the aura and the enlightened one.

98
00:06:58,000 --> 00:07:02,000
He's compared with my son, you're like a weak horse of little strength,

99
00:07:02,000 --> 00:07:05,000
who gains nothing, who never gets to the goal.

100
00:07:05,000 --> 00:07:12,000
A person who is heatful, who is actually practicing the meditation,

101
00:07:12,000 --> 00:07:16,000
who is mindful among the people who are mindless,

102
00:07:16,000 --> 00:07:19,000
who when other people sleep is full of energy.

103
00:07:19,000 --> 00:07:24,000
And he leaves this person, such a person leaves behind such people,

104
00:07:24,000 --> 00:07:32,000
just as a swift horse leaves behind a weak horse.

105
00:07:32,000 --> 00:07:38,000
This, such a person, is called Sumi, that's just one who is wise.

106
00:07:38,000 --> 00:07:43,000
So, basically, the story of these two people had quite different ideas of

107
00:07:43,000 --> 00:07:46,000
the proper way to live one's life.

108
00:07:46,000 --> 00:07:50,000
And I was thinking about this, and at first it seems like quite a simple story.

109
00:07:50,000 --> 00:07:53,000
Like, oh yeah, yeah, be the one who will practice this meditation,

110
00:07:53,000 --> 00:07:57,000
but there's just a quite a bit more to this.

111
00:07:57,000 --> 00:08:06,000
And it points to the question of the benefit and the significance of what we're doing here,

112
00:08:06,000 --> 00:08:08,000
because many people will ask, why are you here?

113
00:08:08,000 --> 00:08:11,000
What are you doing? What do you hope to gain from this?

114
00:08:11,000 --> 00:08:15,000
What benefit do you see in walking and sitting?

115
00:08:15,000 --> 00:08:19,000
And people will often be quite critical of what we do in fact,

116
00:08:19,000 --> 00:08:22,000
and say, you're sitting around doing nothing,

117
00:08:22,000 --> 00:08:26,000
wasting your life, and on top of that, living off of the charity of others.

118
00:08:26,000 --> 00:08:31,000
So people will often criticize this fact that, why don't we get a real job?

119
00:08:31,000 --> 00:08:37,000
Why don't you go out and try to work in the world and see what it's like,

120
00:08:37,000 --> 00:08:40,000
and do something of your life, for example.

121
00:08:40,000 --> 00:08:44,000
People, my father telling me to go get an education,

122
00:08:44,000 --> 00:08:48,000
and people telling me to get a job.

123
00:08:48,000 --> 00:08:57,000
So we have this question of, why don't we work like other people,

124
00:08:57,000 --> 00:09:04,000
why don't we do something that is considered to be working

125
00:09:04,000 --> 00:09:08,000
and considered to be work in a worldly sense.

126
00:09:08,000 --> 00:09:11,000
I once had a man stop me and say, so you don't work, do you?

127
00:09:11,000 --> 00:09:14,000
And I said, yes, I work, and what do you do?

128
00:09:14,000 --> 00:09:20,000
Well, I teach, and he wasn't all that impressed.

129
00:09:20,000 --> 00:09:23,000
But there's even more to it than that, even if a person doesn't teach.

130
00:09:23,000 --> 00:09:26,000
The question is, could a life as a meditator,

131
00:09:26,000 --> 00:09:29,000
if a person just simply spends their time meditating?

132
00:09:29,000 --> 00:09:37,000
Could this be considered a valid means of living one's life?

133
00:09:37,000 --> 00:09:41,000
And from a meditator point, from our point of view,

134
00:09:41,000 --> 00:09:44,000
as someone who has undertaken this, it seems like quite a simple question to answer.

135
00:09:44,000 --> 00:09:52,000
But it's one that has the world kind of suspicious of this sort of lifestyle.

136
00:09:52,000 --> 00:09:57,000
And so I think the question that we have to ask is, what does it lead to?

137
00:09:57,000 --> 00:10:00,000
The things that you do in your life, what do you get out of them?

138
00:10:00,000 --> 00:10:02,000
What does the world get out of them?

139
00:10:02,000 --> 00:10:04,000
What fruit do they bring?

140
00:10:04,000 --> 00:10:09,000
The theory behind much of the Buddhist teaching is,

141
00:10:09,000 --> 00:10:12,000
is in regards to the fruit of one's acts.

142
00:10:12,000 --> 00:10:16,000
If one acts in a certain way, the theory is that it will bring a certain fruit,

143
00:10:16,000 --> 00:10:18,000
or the importance.

144
00:10:18,000 --> 00:10:20,000
The important point is that it will bring a certain fruit,

145
00:10:20,000 --> 00:10:24,000
not the quality or not the type of action that one does,

146
00:10:24,000 --> 00:10:26,000
but the fruit that it brings.

147
00:10:26,000 --> 00:10:29,000
So if a person does something, and it brings suffering,

148
00:10:29,000 --> 00:10:35,000
it brings pain, it brings discomfort, it brings stress, it brings conflict.

149
00:10:35,000 --> 00:10:41,000
Then no matter how industrious it is, we consider it to be a bad thing.

150
00:10:41,000 --> 00:10:46,000
And I think the logic there is hard to argue with.

151
00:10:46,000 --> 00:11:00,000
So this argument that has become quite pernicious amongst the ordinary people is that a person is living a good life based on their industry.

152
00:11:00,000 --> 00:11:04,000
And so the Greeks, this was even something that the Greeks had to grapple with.

153
00:11:04,000 --> 00:11:09,000
So they had this story of this man who pushes a boulder up the mountain.

154
00:11:09,000 --> 00:11:11,000
This is his hell, really.

155
00:11:11,000 --> 00:11:16,000
He has to push this boulder up the mountain, and when it gets to the top, it falls back down.

156
00:11:16,000 --> 00:11:23,000
And this is an allegory, or a metaphor for ordinary life,

157
00:11:23,000 --> 00:11:30,000
for people who work in such a way that they're just getting nothing from him, but age.

158
00:11:30,000 --> 00:11:37,000
All that's happening is they're getting older, and they're living a stressful life, and not really making any benefit whatsoever.

159
00:11:37,000 --> 00:11:40,000
So you ask, what benefit is that sort of life?

160
00:11:40,000 --> 00:11:42,000
What benefit is a life of industry?

161
00:11:42,000 --> 00:11:48,000
If all a person does is push a rock up the hill, for it to fall back down.

162
00:11:48,000 --> 00:11:57,000
So the argument that industry makes validity, or makes one's life beneficial,

163
00:11:57,000 --> 00:12:00,000
is obviously a false one.

164
00:12:00,000 --> 00:12:04,000
The question we have to ask is, what good does your life do?

165
00:12:04,000 --> 00:12:09,000
So many people think that their life, they say that their life brings benefit to society.

166
00:12:09,000 --> 00:12:11,000
They say you have to do your part in society.

167
00:12:11,000 --> 00:12:13,000
This is what people will obviously say.

168
00:12:13,000 --> 00:12:17,000
Well, amongst they leave the world, people who go off and do meditation courses,

169
00:12:17,000 --> 00:12:24,000
they're just wasting their time, where they could be out their helping society.

170
00:12:24,000 --> 00:12:30,000
So the question we have to ask is, what is really a benefit to society?

171
00:12:30,000 --> 00:12:32,000
And what is really a benefit to the world?

172
00:12:32,000 --> 00:12:36,000
And even we have to ask, what is really a benefit to oneself?

173
00:12:36,000 --> 00:12:40,000
Because there are people who spend a lot of their time trying to help others,

174
00:12:40,000 --> 00:12:46,000
bringing physical comfort to others, people who are doctors, or who build hospitals,

175
00:12:46,000 --> 00:13:01,000
or who take care of, when there's epidemics, or when there's catastrophes, they go in and do this kind of relief efforts and so on.

176
00:13:01,000 --> 00:13:06,000
And spend all of their time and become stressed and become agitated, and as a result they burn out.

177
00:13:06,000 --> 00:13:09,000
People who do, for example, social work.

178
00:13:09,000 --> 00:13:17,000
Social workers can become quite burned out because they're neglecting their own benefit.

179
00:13:17,000 --> 00:13:21,000
So it is important for us to include our own benefit in this.

180
00:13:21,000 --> 00:13:26,000
But when we really look at what is a benefit both to ourselves and others,

181
00:13:26,000 --> 00:13:32,000
we have to ask whether the one's physical benefit or one's mental benefit is a real benefit.

182
00:13:32,000 --> 00:13:38,000
And what are the things that lead to true happiness and true peace for a person?

183
00:13:38,000 --> 00:13:47,000
What are the things that lead a person to truly find benefit or to the game benefit?

184
00:13:47,000 --> 00:14:00,000
So if we look at things like building up society and doing our part in terms of fighting wars and keeping some kind of physical stability,

185
00:14:00,000 --> 00:14:05,000
or physical peace, then we can see that there's benefit there.

186
00:14:05,000 --> 00:14:12,000
But that is when you compare it to a person's mental health, to a person's mental, well-being,

187
00:14:12,000 --> 00:14:15,000
it is quite limited.

188
00:14:15,000 --> 00:14:24,000
So this is what we mean when we see that meditation has some benefit in the life of a meditator,

189
00:14:24,000 --> 00:14:32,000
and the life of a person who undertakes to do nothing but developing their mind

190
00:14:32,000 --> 00:14:39,000
and developing insight, developing wisdom, and developing understanding about themselves and the world around them.

191
00:14:39,000 --> 00:14:42,000
Why it's a valid sort of lifestyle?

192
00:14:42,000 --> 00:14:50,000
So when people talk about us living off of charity or wasting our lives and not doing anything for the world,

193
00:14:50,000 --> 00:14:57,000
you have to really ask, what is it that brings benefit to the world or brings benefit in general?

194
00:14:57,000 --> 00:15:01,000
To the people who give us charity, when they give us charity, what does it do for them?

195
00:15:01,000 --> 00:15:07,000
Does it bring them suffering? Does it bring them sadness? Does it bring them pain and conflict?

196
00:15:07,000 --> 00:15:10,000
Does it upset their minds to do this?

197
00:15:10,000 --> 00:15:13,000
And I think on that hand you'd have to say certainly it doesn't.

198
00:15:13,000 --> 00:15:21,000
On the other hand, if they found out that all we were doing was sitting around collecting firewood and sweeping and doing meaningless things,

199
00:15:21,000 --> 00:15:27,000
then in fact it might upset them, it might make them think twice about supporting us.

200
00:15:27,000 --> 00:15:34,000
But the reason that people give us the reason that people give us this very little bit of support to give us enough food to live for one day

201
00:15:34,000 --> 00:15:37,000
is because they do see benefit in what they do.

202
00:15:37,000 --> 00:15:43,000
They see that a person who develops themselves in this way is not only a benefit to themselves,

203
00:15:43,000 --> 00:15:45,000
but is a noble example to the world.

204
00:15:45,000 --> 00:15:52,000
And in fact it can be a teacher, it can be someone who brings this sort of teaching together, so as you see, when a person,

205
00:15:52,000 --> 00:15:59,000
when we set up this meditation center, then people come to practice and to continue on the teaching.

206
00:15:59,000 --> 00:16:08,000
And in fact, if you've seen what's happened since the time of the Buddha, there's been quite a profound effect that has come into the world.

207
00:16:08,000 --> 00:16:16,000
Through not just ordaining, not just building up monasteries, not just being industrious, but through the actual development of one's mind,

208
00:16:16,000 --> 00:16:20,000
the development of insight and wisdom and purity of mind.

209
00:16:20,000 --> 00:16:27,000
The point being that in order to help others, as has been said many times, in order to really bring benefit,

210
00:16:27,000 --> 00:16:34,000
you can't just go out and work and say, this what I have inside, I'm going to bring and benefit the world.

211
00:16:34,000 --> 00:16:42,000
So people go out in the world and think, I have to do good deeds, I have to benefit the world and so on.

212
00:16:42,000 --> 00:16:51,000
But because of their own confusion inside, because of their own inner delusion and their own anger, their own greed and attachment, their own views,

213
00:16:51,000 --> 00:16:52,000
they twist things.

214
00:16:52,000 --> 00:16:58,000
So if someone has a certain religious belief, they will impose it on others.

215
00:16:58,000 --> 00:17:02,000
If they have a certain, any sort of belief, they will impose this on others.

216
00:17:02,000 --> 00:17:13,000
If they have attachment, they will twist and instead of helping, they will wind up working only for their own material benefit,

217
00:17:13,000 --> 00:17:20,000
and following after their greed and meeting with a great amount of stress and suffering as well.

218
00:17:20,000 --> 00:17:30,000
If a person on the other hand, refrains from helping others, for the time being develops themselves inside, takes out this attachment,

219
00:17:30,000 --> 00:17:39,000
the aversion, the fear and the worry and the stress that we have inside, and purifies one's mind,

220
00:17:39,000 --> 00:17:52,000
then just as the source of a river, when it becomes purest, is suitable to be drank by all the animals and all the beings down to the end of the river.

221
00:17:52,000 --> 00:18:02,000
So too, when one purifies one's mind, everything that one does, all of one's interactions, then when one goes out in the world,

222
00:18:02,000 --> 00:18:08,000
or interacts with people, people who come to visit the monastery or to visit the meditation center,

223
00:18:08,000 --> 00:18:17,000
one will benefit them immensely, will react to them, will interact with them, will impart upon them.

224
00:18:17,000 --> 00:18:23,000
The importance of this purity, people will be able to see that this is a true path to peace.

225
00:18:23,000 --> 00:18:29,000
It's something that brings them peace, for a could bring them peace happiness and freedom from suffering as well,

226
00:18:29,000 --> 00:18:34,000
and encourages them to develop in that way.

227
00:18:34,000 --> 00:18:39,000
So the point being in brief is that we have to understand what is a real benefit,

228
00:18:39,000 --> 00:18:43,000
and so when people say, oh, you're just sitting around and doing nothing,

229
00:18:43,000 --> 00:18:48,000
well, we have to look quite a little bit deeper than that, and we have to be very careful not to get caught up in this idea,

230
00:18:48,000 --> 00:18:53,000
that industry, somehow, somehow, in and of itself, leads to benefit.

231
00:18:53,000 --> 00:19:01,000
If someone is industrious in killing and in hurting and in harming others, if a person works in these industries that, you know,

232
00:19:01,000 --> 00:19:07,000
chemical industries, pesticide industries, to kill living beings by the millions,

233
00:19:07,000 --> 00:19:21,000
and to harm the environment and so on, if people work in manufacturing weapons or manufacturing military supplies and so on,

234
00:19:21,000 --> 00:19:26,000
then you have to ask whether these sort of things are actually benefiting the world,

235
00:19:26,000 --> 00:19:40,000
whether they work as politicians and work to drive fear into people of foreign powers and so on, and patriotism and so on.

236
00:19:40,000 --> 00:19:51,000
Commercialism encouraging people to, when people work in the entertainment industry, encouraging people to buy music and to listen and to watch movies and so on,

237
00:19:51,000 --> 00:19:58,000
to spend all of their time in idle pursuits that don't truly lead to peace and happiness,

238
00:19:58,000 --> 00:20:03,000
then you have to ask whether this sort of industry is actually of benefit to the world,

239
00:20:03,000 --> 00:20:09,000
whereas it brought the world, and whereas all of the industry that we see in society brought the world,

240
00:20:09,000 --> 00:20:14,000
if you compare to when people were perhaps less industrious, when we were like the monkeys,

241
00:20:14,000 --> 00:20:19,000
just sitting around scratching themselves and eating bananas in the forest,

242
00:20:19,000 --> 00:20:25,000
you know, kind of content with what they had or comparatively,

243
00:20:25,000 --> 00:20:33,000
and compared to where we are now with all of our stresses and concerns and wars and so on,

244
00:20:33,000 --> 00:20:39,000
are we really any better off? I wouldn't say we were better off or the monkeys are better off,

245
00:20:39,000 --> 00:20:47,000
but I will say that living a peaceful life, living a life that is outside of this sort of busyness,

246
00:20:47,000 --> 00:20:59,000
this sort of hectic activity where your measure of success is very much how much stress you develop in your life and in the world around you,

247
00:20:59,000 --> 00:21:06,000
the life that leaves this behind in favor of non-interaction,

248
00:21:06,000 --> 00:21:20,000
not non-participation, where we live in and in and of ourselves, in the world, as a part of nature, instead of our happiness and our peace and our lives,

249
00:21:20,000 --> 00:21:27,000
depending on the environment and the company around us, where we sit around and chat about this month,

250
00:21:27,000 --> 00:21:33,000
sitting around and chatting by the fire place, thinking that somehow this was living his life in a good way

251
00:21:33,000 --> 00:21:39,000
until he realized that, oh, the Buddha didn't, the Buddha, teach that that was the way of wasting your life.

252
00:21:39,000 --> 00:21:44,000
And in fact, it was a waste of his life, because you see, as a result, he changed nothing about himself.

253
00:21:44,000 --> 00:21:49,000
In fact, the first thing he did when they went to the Buddha is scolded as the Arahan,

254
00:21:49,000 --> 00:21:56,000
the one who was enlightened, the one who had no thoughts of greed, no thoughts of anger, no thoughts of delusion,

255
00:21:56,000 --> 00:22:06,000
who had no thoughts of hatred or no bad thoughts towards others at all, he's been sleeping all the time, this guy's useless and thought,

256
00:22:06,000 --> 00:22:14,000
which is quite a sign that he himself is still as far to go.

257
00:22:14,000 --> 00:22:18,000
So this is, I think, something for us to reflect upon.

258
00:22:18,000 --> 00:22:24,000
I mean, it's obviously not going to be of much use when you're just walking back and forth or sitting in meditation,

259
00:22:24,000 --> 00:22:27,000
then you should be focusing on reality.

260
00:22:27,000 --> 00:22:32,000
But when the thought comes up, what am I doing here, or is this something that is a real benefit,

261
00:22:32,000 --> 00:22:38,000
or you think of, oh, I could be off in the world doing this or doing that, to ask yourself, what is the result of that?

262
00:22:38,000 --> 00:22:44,000
There's things that we've done for so long that way we've lived our lives, what is the benefit, what is the meaning,

263
00:22:44,000 --> 00:22:48,000
what has it done for us, and what has it done for the world around us, what has it done for other people,

264
00:22:48,000 --> 00:22:58,000
all of the pleasure and all of that happiness that we've been pursuing, all of the goals that we have been pursuing, trying to change the world and so on.

265
00:22:58,000 --> 00:23:03,000
What has it really done, and what have we been neglecting, what is missing from the picture?

266
00:23:03,000 --> 00:23:09,000
And I think one of the very important things that is missing from the picture a lot of the time is this mental development,

267
00:23:09,000 --> 00:23:16,000
and this is what we have here, this is what we're trying to attain here, this is what we're trying to gain for ourselves.

268
00:23:16,000 --> 00:23:23,000
Because once one's mind is pure, it doesn't matter what one does, where one goes, how one lives one's life.

269
00:23:23,000 --> 00:23:29,000
One brings benefit to oneself and to others and everything one does.

270
00:23:29,000 --> 00:23:39,000
So, so we can feel good about what we're doing, and we can think that this is something that is a real benefit,

271
00:23:39,000 --> 00:23:42,000
and this is important in the practice, it's important to feel good about what you're doing,

272
00:23:42,000 --> 00:23:52,000
it's important to see that we're not here, because someone else said meditation is good, or because somehow we have this concept of meditation being a good thing,

273
00:23:52,000 --> 00:23:56,000
or because we were just running away from the world or something.

274
00:23:56,000 --> 00:24:02,000
We should be confident and happy that actually what we're doing is an incredible profound thing.

275
00:24:02,000 --> 00:24:08,000
We're developing ourselves, we're becoming a more pure individual.

276
00:24:08,000 --> 00:24:17,000
We're developing our minds to a higher level, so that we're able to see things with more clarity and more wisdom.

277
00:24:17,000 --> 00:24:29,000
So, we have this vivid image of first of all these two men who went forth and lived their lives quite differently,

278
00:24:29,000 --> 00:24:34,000
and so we can ask ourselves, and this is a reminder to all of us that even though we have work to do,

279
00:24:34,000 --> 00:24:41,000
we shouldn't let it get in the way of our meditation, because this is not why we came here, what we came here to do.

280
00:24:41,000 --> 00:24:48,000
Then we have this other vivid image of two horses, one horse that is very fast, and one horse that is very weak,

281
00:24:48,000 --> 00:24:59,000
and it's the weak horse that we compare to the person who sleeps all the time, and sits around in chats all the time, and spends all their time in idle pursuits.

282
00:24:59,000 --> 00:25:08,000
Even in terms of work that has to be done in the monastery, if we spend too much time on that, we'll come to a story later of a monk who just swept all the time,

283
00:25:08,000 --> 00:25:16,000
and everybody thought, oh, he must, maybe he's enlightened, because here he is devoting himself so strenuously to sweeping,

284
00:25:16,000 --> 00:25:25,000
and the Buddha said, a person who sweeps, I call him a janitor, not a monk, so then they realize all that.

285
00:25:25,000 --> 00:25:35,000
Probably not a good sign that he sweeps up, so we have to put our effort into the meditation practice so that we can...

286
00:25:35,000 --> 00:25:41,000
Again, this isn't a teaching to compare ourselves to others and look down upon others.

287
00:25:41,000 --> 00:25:48,000
It's a teaching for all of us to catch up, to not get left behind.

288
00:25:48,000 --> 00:25:56,000
Because, obviously, there's Iran, he wasn't the one doing the paddle tailing or doing the scolding or the comparing. It was the monk who was left behind.

289
00:25:56,000 --> 00:26:01,000
We have to work hard to get over that, and we have to be careful not to compare ourselves to others.

290
00:26:01,000 --> 00:26:09,000
This is important as well. Sometimes we think that although he's not meditating, I'll just stop as well.

291
00:26:09,000 --> 00:26:15,000
And then you don't realize that actually he was just meditating, and he just finished or so forth.

292
00:26:15,000 --> 00:26:24,000
We have to be careful to do the work that we have to do, and not get left behind, like the weak horse.

293
00:26:24,000 --> 00:26:29,000
We should exert ourselves, especially in the meditation.

294
00:26:29,000 --> 00:26:32,000
And it doesn't mean that we have to push, push, push really hard.

295
00:26:32,000 --> 00:26:35,000
It means we have to be persistent and we have to be patient.

296
00:26:35,000 --> 00:26:40,000
The story of the tortoise and hair is appropriate.

297
00:26:40,000 --> 00:26:46,000
And there's two horses who want to win the race, but who really wins the race is the one who is slow and steady.

298
00:26:46,000 --> 00:26:49,000
Not the one who pushes, pushes, pushes, and then breaks.

299
00:26:49,000 --> 00:26:52,000
And as to take a break.

300
00:26:52,000 --> 00:26:56,000
But the important point that we have to work and we have to work during the meditation practice.

301
00:26:56,000 --> 00:26:58,000
And we should feel good about what we're doing.

302
00:26:58,000 --> 00:27:01,000
We should know that this is what the Buddha would have us do.

303
00:27:01,000 --> 00:27:09,000
And we should know that this is what is really, I think, objectively, of benefit to ourselves.

304
00:27:09,000 --> 00:27:11,000
Of benefit to ourselves in the world around us.

305
00:27:11,000 --> 00:27:16,000
It's something that is a potential cure for the troubles that exist in the world.

306
00:27:16,000 --> 00:27:21,000
All of the stress and all of the suffering, all of the problems in the world.

307
00:27:21,000 --> 00:27:25,000
They all come from the human mind or the minds of beings in general.

308
00:27:25,000 --> 00:27:32,000
And the meditation practice is that which has the potential to purify our minds and to bring us out of this.

309
00:27:32,000 --> 00:27:38,000
And by us, meaning the whole world, that simply by doing what we're doing here,

310
00:27:38,000 --> 00:27:43,000
we create something that is the way out for the whole world.

311
00:27:43,000 --> 00:27:50,000
And all of the people out there who are wasting away their lives, living their lives in carelessness and uselessness.

312
00:27:50,000 --> 00:27:56,000
They're able to see this and they're able to see that here's something that could actually transform my life into something meaningful.

313
00:27:56,000 --> 00:28:01,000
Where I'm able to see things clearly, where I don't react to things with greed or anger,

314
00:28:01,000 --> 00:28:09,000
where I don't fight and have conflict, where I don't indulge in useless pursuits that bring me back just after pushing it up.

315
00:28:09,000 --> 00:28:12,000
They don't bring me back to the bottom again.

316
00:28:12,000 --> 00:28:18,000
And so these people will take this as an example and the whole world will begin to do things that are benefit.

317
00:28:18,000 --> 00:28:29,000
They will begin to help each other and to develop, help themselves, develop their own minds and develop the world in such a way that brings peace happiness and freedom from suffering.

318
00:28:29,000 --> 00:28:34,000
Whether it will be done or not is another point, another question.

319
00:28:34,000 --> 00:28:38,000
But here we have an answer to the question of why don't we go and get a job?

320
00:28:38,000 --> 00:28:43,000
Why don't we go out and work and work like real people in the real world?

321
00:28:43,000 --> 00:28:46,000
The point is that those people are living in the fake world.

322
00:28:46,000 --> 00:28:48,000
They're living in the world of illusion.

323
00:28:48,000 --> 00:28:52,000
Where you think there's some benefit from pushing a rock up a hill.

324
00:28:52,000 --> 00:28:56,000
And then when it goes back to the bottom, you start to push it up again.

325
00:28:56,000 --> 00:29:00,000
And here we're trying to stop that.

326
00:29:00,000 --> 00:29:06,000
We're trying to come back to the real world.

327
00:29:06,000 --> 00:29:10,000
The world where reality is the object of our attention.

328
00:29:10,000 --> 00:29:13,000
We're able to see things exactly truly as they are.

329
00:29:13,000 --> 00:29:16,000
That pushing a rock up a hill is just pushing a rock up a hill.

330
00:29:16,000 --> 00:29:18,000
There's no benefit from it.

331
00:29:18,000 --> 00:29:23,000
Whatever we do is just whatever we do walking back and forth is just walking back and forth.

332
00:29:23,000 --> 00:29:26,000
Eating is just eating.

333
00:29:26,000 --> 00:29:27,000
Talking is just talking.

334
00:29:27,000 --> 00:29:29,000
Sleeping is just sleeping.

335
00:29:29,000 --> 00:29:31,000
Everything is just what it is.

336
00:29:31,000 --> 00:29:39,000
When you get to that point, you're able to live without any sort of attachment or clinging

337
00:29:39,000 --> 00:29:44,000
anger or depression or frustration or sadness at all,

338
00:29:44,000 --> 00:29:49,000
to live without suffering and to benefit others as well.

339
00:29:49,000 --> 00:29:51,000
So that's the teaching for today.

340
00:29:51,000 --> 00:29:55,000
I think it's a very important significance in this sense.

341
00:29:55,000 --> 00:30:22,000
So thank you all for listening and tune in next time.

