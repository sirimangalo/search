1
00:00:00,000 --> 00:00:05,760
Okay, hello and welcome back to our study of the Dhamapanda.

2
00:00:05,760 --> 00:00:14,040
Today we go on to verse number 47, which reads as follows.

3
00:00:14,040 --> 00:00:31,520
Bhupani-heva-pachinantam bhyasatam anasangwaram sutangam gamangmahogo matru-a-dhyagatatit means.

4
00:00:31,520 --> 00:00:43,040
Bhupani-heva-pachinantam, like one who collects or gathers flowers, bhyasatam anasangangam

5
00:00:43,040 --> 00:00:53,880
man or a person who is with a distracted mind or with a negligent mind, with a heedless

6
00:00:53,880 --> 00:00:56,000
mind.

7
00:00:56,000 --> 00:01:05,120
Sutangamamahogo matru-a-dhyagatit, matru-a-dhyagatit is carried off by death or death carries

8
00:01:05,120 --> 00:01:20,400
away such a person, sutangamamahogo, just as a great flood carries away a sleeping village.

9
00:01:20,400 --> 00:01:29,080
So one who was mind is distracted or was distracted in mind like someone collecting flowers

10
00:01:29,080 --> 00:01:41,560
carried away by death, just as carried away unexpectedly by death, or unready, not ready

11
00:01:41,560 --> 00:01:49,400
for death, just as a sleeping village is carried away unexpectedly without any time for

12
00:01:49,400 --> 00:01:55,280
preparation by a great flood, and that's the verse.

13
00:01:55,280 --> 00:02:01,800
So this was told in regards to a fairly famous story and quite a long story, so I'll

14
00:02:01,800 --> 00:02:10,880
try to a bridge it can be the condensed version.

15
00:02:10,880 --> 00:02:17,480
Basically it starts with the, what doesn't start with, the important part of the story

16
00:02:17,480 --> 00:02:26,200
starts with King Passainadi, who was a king in the time of the Buddha, and he got, he gained

17
00:02:26,200 --> 00:02:32,680
great faith in the Buddha, and as a result invited monks to come to the palace, and for a few

18
00:02:32,680 --> 00:02:39,120
days he looked after them, but then something like after the seventh day he stopped, he

19
00:02:39,120 --> 00:02:49,680
lost interest, and went back to his worldly affairs, and as a result the monks weren't

20
00:02:49,680 --> 00:02:54,920
taken care of because if the king doesn't give the orders no one really cares, and so

21
00:02:54,920 --> 00:02:59,600
the people in the palace when monks came, weren't doing it out of faith, no one was really

22
00:02:59,600 --> 00:03:09,560
interested in the monks, and so they weren't fed or they were fed late, or it was much

23
00:03:09,560 --> 00:03:15,000
confusion and it wasn't really a wholesome scene.

24
00:03:15,000 --> 00:03:23,160
So as a result the monks slowly won by one by one, stopped coming to the palace, until

25
00:03:23,160 --> 00:03:27,560
finally the only one who was left was Ananda, because Ananda had this great quality

26
00:03:27,560 --> 00:03:37,560
about him of protecting the faith of families, so no matter what he would stick to his

27
00:03:37,560 --> 00:03:51,120
duties with the purpose of keeping the faith of the king in this case, and so one day

28
00:03:51,120 --> 00:03:55,160
the king remembered the monks and he came to see, oh, how is it going with the feeding

29
00:03:55,160 --> 00:04:00,600
of the beakers, and he goes there and he sees it on the Ananda, and he says, what's going

30
00:04:00,600 --> 00:04:01,600
on?

31
00:04:01,600 --> 00:04:02,600
We're all the monks over there.

32
00:04:02,600 --> 00:04:14,400
When you stop coming they slowly stopped coming as well, and he went to the Buddha, and

33
00:04:14,400 --> 00:04:26,640
I can't remember what the Buddha said, the Buddha gave him some reasons as to why the monks

34
00:04:26,640 --> 00:04:31,360
wouldn't, the monks weren't coming or something like that.

35
00:04:31,360 --> 00:04:35,280
And the Buddha taught, the Buddha taught as a result of this, he didn't force the monks

36
00:04:35,280 --> 00:04:36,280
to go there.

37
00:04:36,280 --> 00:04:45,280
If this is he gave in the Angutra, any guy who said, if families don't have nine traits,

38
00:04:45,280 --> 00:04:55,520
nine qualities, it's not important to go to, you need not bother with visiting those families.

39
00:04:55,520 --> 00:05:00,880
But if they have the opposite nine qualities, or if they're free from those nine qualities,

40
00:05:00,880 --> 00:05:04,920
if they have the opposite, then they should be visited.

41
00:05:04,920 --> 00:05:13,920
So this is like, if they stand up respectfully when the monks come, if they stand up and

42
00:05:13,920 --> 00:05:23,560
are respectful and are happy to stand up as a respectful gesture, if they hold their hands

43
00:05:23,560 --> 00:05:30,640
up, if they venerate you respectfully, if they do it happily, if they put out seats happily,

44
00:05:30,640 --> 00:05:43,480
if they bring you lots of food, sorry, if they don't do these things, if they give course

45
00:05:43,480 --> 00:05:49,320
food and so on, if they don't listen to the dhamma when you teach it, if they're not

46
00:05:49,320 --> 00:05:55,880
interested in the dhamma, if they don't, when you're talking, they don't listen and so on.

47
00:05:55,880 --> 00:06:00,600
And these are reasons not to go there, so the Buddha was kind of taking the sides of

48
00:06:00,600 --> 00:06:05,360
the side of the monks, but the king went back home and so what it was is the Buddha had

49
00:06:05,360 --> 00:06:13,880
told him that it must be because there was no one there and there was no, there was no

50
00:06:13,880 --> 00:06:17,760
unfamiliar there, they weren't familiar with the king or something like that, because

51
00:06:17,760 --> 00:06:23,120
the point of this story is that the king got it in his head that he would find some way

52
00:06:23,120 --> 00:06:29,800
to make himself dear to the monks and the vicinity, if you know the story of it, he wasn't

53
00:06:29,800 --> 00:06:38,640
the brightest, what do you say, it wasn't the sharpest knife in the drawer, so instead

54
00:06:38,640 --> 00:06:43,240
of thinking maybe I'll practice meditation or maybe I'll actually listen to the monks

55
00:06:43,240 --> 00:06:50,760
when they teach or something, what did he think he thought, I'll marry one of their,

56
00:06:50,760 --> 00:06:57,080
one of the Buddha's relatives, I'll pick one of the Buddha's relatives as my queen and

57
00:06:57,080 --> 00:07:06,760
that will be a sure way of getting in good with Buddha, that's what he thought, and so

58
00:07:06,760 --> 00:07:16,240
he sent a messenger to the sakya's in Kapilawatu and said, please send me a queen, send

59
00:07:16,240 --> 00:07:24,000
me a woman to be my queen, I want to marry into the sakya, the sakya's was the same

60
00:07:24,000 --> 00:07:34,080
family, and you know this isn't going anywhere good, now the sakya's were very proud, it

61
00:07:34,080 --> 00:07:43,840
was one of their failings, they would never mix with another tribe and yet they knew that

62
00:07:43,840 --> 00:07:49,680
if they refused king vicinity he would invade them and he would be quite, he would start

63
00:07:49,680 --> 00:07:54,480
a war with them and they didn't want that because they weren't definitely not as strong

64
00:07:54,480 --> 00:08:03,440
as him, nor as warlike, so they talked about it and Mahanama, this famous sakya and

65
00:08:03,440 --> 00:08:13,320
prince, he suggested that they send his daughter who was a daughter with a slave woman,

66
00:08:13,320 --> 00:08:25,960
so the point being that she wasn't really a legitimate child and so they wouldn't feel

67
00:08:25,960 --> 00:08:35,840
like they were betraying their clan purity or whatever, so they did this and they tricked

68
00:08:35,840 --> 00:08:44,440
the king and the king for a long time thought that this was a real sakya and princess

69
00:08:44,440 --> 00:08:56,520
and he had a son with her and then his son went back after growing up and his son went

70
00:08:56,520 --> 00:09:04,080
back to visit the sakya's and overheard someone talking about him as that bastard child

71
00:09:04,080 --> 00:09:09,320
of the slave woman and so the word got back to the king and the king went to see the

72
00:09:09,320 --> 00:09:13,480
Buddha and said that's not really correct, they shouldn't have done that and he said

73
00:09:13,480 --> 00:09:22,800
but your son is still the son of a prince and so he consoled the king about this, this

74
00:09:22,800 --> 00:09:26,200
is quite a bridge so there's actually a lot more that goes on here but now it starts

75
00:09:26,200 --> 00:09:35,720
to get interesting because the son, we do daba, we do daba his name, he doesn't let

76
00:09:35,720 --> 00:09:39,840
go of it, so the king says well that's fine, yes he's right, my son really is a prince,

77
00:09:39,840 --> 00:09:46,200
there's nothing wrong with him but the son himself is really upset by this and feels

78
00:09:46,200 --> 00:09:53,480
like he has been, I don't know, somehow he has been wronged by the sakya's and so when

79
00:09:53,480 --> 00:10:01,800
the king, the sanity dies and he becomes king, he vows to kill all the sakya, he says

80
00:10:01,800 --> 00:10:11,200
when I'm king I'm going to destroy the sakya and race and I'm going to wash my feet with

81
00:10:11,200 --> 00:10:20,080
their blood, that's what he says and so he builds up this hatred in his mind, he builds

82
00:10:20,080 --> 00:10:29,760
up this vengeance in his mind and one day he thinks of this vengeance, he becomes king

83
00:10:29,760 --> 00:10:36,960
and then he makes plans and finally he gets into his heart, he says now I'm going to

84
00:10:36,960 --> 00:10:46,480
go and he puts together his army and he leaves his home, which would be sovereignty I guess

85
00:10:46,480 --> 00:10:55,000
I'm heading out for Kapilawatu with his army, now the Buddha, Buddhas have various duties,

86
00:10:55,000 --> 00:11:01,120
Buddhas consider to have four duties, I think, duty to oneself, duty to the world, duty

87
00:11:01,120 --> 00:11:07,440
to one's relatives and duty to the lineage of the Buddhas or duty as a Buddha, so whatever

88
00:11:07,440 --> 00:11:13,840
that means, so there are examples of the Buddha looking after his relatives and this is

89
00:11:13,840 --> 00:11:21,800
one prime example where he did so, sitting in his good deed, it came to his mind that

90
00:11:21,800 --> 00:11:29,320
this was happening, that king we do Dumbo was planning to destroy all of his relatives,

91
00:11:29,320 --> 00:11:40,640
his whole family was going to go and put an end to his family and so he made a decision

92
00:11:40,640 --> 00:11:50,160
to travel and to intercept the king and so he went halfway on the path to Kapilawatu

93
00:11:50,160 --> 00:11:58,120
and he found a tree with very little very few leaves on it, almost dead tree with just

94
00:11:58,120 --> 00:12:06,880
a few leaves and he sat under it in the hot sun and we do Dumbo came up and saw oh there's

95
00:12:06,880 --> 00:12:12,920
the Buddha and so he got off his elephant and went up to the Buddha and bowed down to the

96
00:12:12,920 --> 00:12:17,440
Buddha and said learn Buddha why are you here, why are you sitting under this tree without

97
00:12:17,440 --> 00:12:25,600
any shade and the Buddha said oh the shade of my relatives is enough for me, my relatives

98
00:12:25,600 --> 00:12:39,880
provide me all the shade I need and the Buddha is here to protect his relatives and

99
00:12:39,880 --> 00:12:48,280
so he gets on his elephant and turns around and goes back to Savatu.

100
00:12:48,280 --> 00:13:02,760
And you know kings, they're not the most stable sort of beings and so he's unable to

101
00:13:02,760 --> 00:13:11,720
give up this hatred you see people who don't practice meditation, it's funny how he was

102
00:13:11,720 --> 00:13:16,400
so close to the Buddha and he never was able to practice meditation but it's true, it's

103
00:13:16,400 --> 00:13:21,720
as well, the story says it's true and this is the story, this kind of thing happened for

104
00:13:21,720 --> 00:13:28,880
sure though, people can be very close to Buddhism and even study all the Buddha's teachings

105
00:13:28,880 --> 00:13:33,640
and never actually practice and so they're never able to overcome, they're never able

106
00:13:33,640 --> 00:13:38,640
to change and so he can't change because he's harbored this from when he was young

107
00:13:38,640 --> 00:13:44,800
and when he first found out that he had been that's funny because he wouldn't have been

108
00:13:44,800 --> 00:13:55,600
born if his father hadn't married this woman and so he decides again to go out, he gets

109
00:13:55,600 --> 00:14:02,680
angry again and he's unable to suppress it or not able to let go of it and so he heads

110
00:14:02,680 --> 00:14:10,840
out on his elephant with his army and again the Buddha goes, sits under the same tree,

111
00:14:10,840 --> 00:14:23,640
says the same thing and we didn't have the turns around and goes back, I don't think

112
00:14:23,640 --> 00:14:35,080
that's the end of the story, no, no sir, he goes home and like a week passes and a week

113
00:14:35,080 --> 00:14:41,640
later he's out on the elephant again, he just can't handle it and the Buddha looks and

114
00:14:41,640 --> 00:14:52,680
he looks back into the past and he sees the sacchias have actually built up this state

115
00:14:52,680 --> 00:14:59,600
of affairs, they've done some sort of bad karma, I can't remember what it was, I don't

116
00:14:59,600 --> 00:15:08,920
think it may be even says and there's no way he's going to be able to stop, we do

117
00:15:08,920 --> 00:15:22,640
numbers, advance and he says their death is relative to destined to be wiped out and so

118
00:15:22,640 --> 00:15:29,080
he doesn't go, he stays at home and we do double heads out on his elephant and gets to

119
00:15:29,080 --> 00:15:33,960
copy the what too and see the thing about the sacchias is that they become Buddhist, they follow

120
00:15:33,960 --> 00:15:38,840
the Buddhist teaching and they're keeping the five precepts, even the soldiers couldn't

121
00:15:38,840 --> 00:15:51,120
kill, so what did they do, they shouldn't, they shoot arrows over the heads of their

122
00:15:51,120 --> 00:15:58,280
enemies and I think they held up grass, they collect a grass and they held it up like

123
00:15:58,280 --> 00:16:04,920
a weapon or something like that and sticks, I can't remember what it was quite funny

124
00:16:04,920 --> 00:16:11,880
if you read this story, how they fought, how they fought back and they all wiped out,

125
00:16:11,880 --> 00:16:16,800
all were killed and he washed his feet with their blood and so it says the sacchias were

126
00:16:16,800 --> 00:16:21,360
actually wiped out, which is interesting because many people in Nepal claim to be related

127
00:16:21,360 --> 00:16:27,560
to the Buddha sacchias sacchias, so the story seems to suggest otherwise but on the other

128
00:16:27,560 --> 00:16:33,480
hand it's quite difficult to wipe out everyone in a race, no, it's not an easy thing,

129
00:16:33,480 --> 00:16:43,920
so maybe some escaped and we did double was victorious and happy and feeling like he had

130
00:16:43,920 --> 00:16:49,400
won the day and so he had headed back, started heading back home and he got to this river

131
00:16:49,400 --> 00:16:55,560
and he got all his men out and he went down by the river and washed the blood off of his

132
00:16:55,560 --> 00:17:05,480
feet and they laid down to sleep by the river, and they also had Mahanama with him, they

133
00:17:05,480 --> 00:17:09,000
captured Mahanama and they wanted to torture him or something like that so they had him tied

134
00:17:09,000 --> 00:17:20,400
up as well, with him with an army and they slept there overnight and as they were asleep,

135
00:17:20,400 --> 00:17:35,400
Maho, Mahogo and a great flood came and swept them all away in the night and so the monks

136
00:17:35,400 --> 00:17:42,520
were talking about this and they said it's amazing that there is this, how the world goes

137
00:17:42,520 --> 00:17:48,760
and I think you're on top of the world victorious in battle one day and push you're

138
00:17:48,760 --> 00:17:53,240
gone the next day and so this is why the Buddha taught this verse, this is where this

139
00:17:53,240 --> 00:18:09,760
verse comes from, the Buddha said indeed, in English and we think, I think we're invincible,

140
00:18:09,760 --> 00:18:19,880
people become so caught up in their arrogance and in their conceit, in their intoxication

141
00:18:19,880 --> 00:18:33,560
with life, intoxication with health, intoxication with youth, I think I'm still young,

142
00:18:33,560 --> 00:18:44,160
but children who die, some people die with their five years old and happy, true love.

143
00:18:44,160 --> 00:18:51,160
Buddha said, go Chandya, Maranang, Sulay, who knows whether death will come tomorrow.

144
00:18:51,160 --> 00:18:54,840
If you knew you were going to die tomorrow, would you be negligent today?

145
00:18:54,840 --> 00:18:57,560
Would you just go and do whatever you want today?

146
00:18:57,560 --> 00:19:05,680
If you knew this was your last day, what would you do today if you knew it was your last

147
00:19:05,680 --> 00:19:06,680
day?

148
00:19:06,680 --> 00:19:13,160
Do you know how to get a puppy?

149
00:19:13,160 --> 00:19:19,560
Would that make you happy?

150
00:19:19,560 --> 00:19:38,080
The problem is that when we die, the mind continues, if we have clinging in our mind

151
00:19:38,080 --> 00:19:48,560
will die with sadness, will die crying, will die longing, this is why people are born

152
00:19:48,560 --> 00:19:50,640
as ghosts.

153
00:19:50,640 --> 00:19:55,680
If you love your puppy today and then tomorrow you die, you'll be born as a ghost, haunting

154
00:19:55,680 --> 00:19:58,240
your puppy.

155
00:19:58,240 --> 00:20:05,840
It's the danger of clinging.

156
00:20:05,840 --> 00:20:13,480
Don't realize that everything gets cut off.

157
00:20:13,480 --> 00:20:17,760
The only thing we bring with us is our subjectivity, really.

158
00:20:17,760 --> 00:20:25,720
If you look at how there are some new research that I keep talking about, after people

159
00:20:25,720 --> 00:20:34,760
die, people die and then they have these experiences, brain is dead, hard has stopped.

160
00:20:34,760 --> 00:20:43,840
The temperature of the body is dropping and they keep in Japan, they actually pump ice or

161
00:20:43,840 --> 00:20:49,880
cold water into the bloodstream just to keep it from rotting.

162
00:20:49,880 --> 00:20:55,120
And then hours later they bring them back to life.

163
00:20:55,120 --> 00:20:58,800
These people talk about their experiences and it's totally subjective.

164
00:20:58,800 --> 00:21:03,960
It's based on the things that they've done in their life, it's based on their religion

165
00:21:03,960 --> 00:21:09,120
often, it's based on their ideas, it becomes totally subjective.

166
00:21:09,120 --> 00:21:14,680
So imagine what it would be like if you had such strong clinging to something, if there

167
00:21:14,680 --> 00:21:20,960
was something that you were afraid of spiders and you would see spiders.

168
00:21:20,960 --> 00:21:22,560
And then how would you react?

169
00:21:22,560 --> 00:21:27,320
When you now had nobody, there was nobody there to put the spider in a cup and take it

170
00:21:27,320 --> 00:21:28,640
outside.

171
00:21:28,640 --> 00:21:34,840
Suddenly your spider's crawling all over you, this is what you'd see when you die.

172
00:21:34,840 --> 00:21:39,040
And what if you couldn't handle that?

173
00:21:39,040 --> 00:21:42,760
So this is where the Buddha said we're like picking flowers.

174
00:21:42,760 --> 00:21:43,920
Is that preparing yourself?

175
00:21:43,920 --> 00:21:47,800
What if you knew there was a flood coming to win the big and you said, well then I'll

176
00:21:47,800 --> 00:21:55,040
spend my time picking flowers, why is this is what we're like?

177
00:21:55,040 --> 00:22:00,280
It's not like where it's actually not even like a sleeping village.

178
00:22:00,280 --> 00:22:04,640
It's like a village that knows there's a flood coming and all the villagers say well then

179
00:22:04,640 --> 00:22:06,640
let's go pick some flowers.

180
00:22:06,640 --> 00:22:12,800
That's what the Buddha is talking about here.

181
00:22:12,800 --> 00:22:16,480
Like a sleeping village, but it's even worse than a sleeping village, it's like a village

182
00:22:16,480 --> 00:22:19,680
that goes to sleep knowing a flood is coming.

183
00:22:19,680 --> 00:22:24,520
We know there's a flood coming, we know death is coming, we know no end, we know it's

184
00:22:24,520 --> 00:22:26,080
coming.

185
00:22:26,080 --> 00:22:35,760
And we know from Buddhism, we have this, you could say we have a belief that life continues

186
00:22:35,760 --> 00:22:36,760
after death.

187
00:22:36,760 --> 00:22:41,680
But there's many ways it doesn't have to be a belief and one of them is now this kind

188
00:22:41,680 --> 00:22:49,520
of evidence even of people when they die having subjective experiences.

189
00:22:49,520 --> 00:22:54,320
And how it becomes totally subjective, we're totally based on your mind.

190
00:22:54,320 --> 00:23:01,440
But imagine we do double who had just washed his feet in the blood of his enemies.

191
00:23:01,440 --> 00:23:04,400
What do you think he was thinking about when he died?

192
00:23:04,400 --> 00:23:09,720
When his brain stopped, what do you think, what images do you think he conjured up?

193
00:23:09,720 --> 00:23:10,720
Blood.

194
00:23:10,720 --> 00:23:16,080
He's not going to a good place.

195
00:23:16,080 --> 00:23:28,440
So the Buddha said death carries you away, unprepared.

196
00:23:28,440 --> 00:23:32,880
There's something about this that is not obvious unless you've been practicing meditation

197
00:23:32,880 --> 00:23:40,560
and that's that we store up all of our experiences.

198
00:23:40,560 --> 00:23:44,560
Everything that we've done is stored in our mind and you only see this when you meditate.

199
00:23:44,560 --> 00:23:49,480
As soon as you begin to meditate, you realize how much you're keeping inside you.

200
00:23:49,480 --> 00:23:52,560
All of the little obsessions that we have.

201
00:23:52,560 --> 00:23:56,920
Because when you meditate, they come out and you remember things and oh, that was this

202
00:23:56,920 --> 00:23:59,120
bad thing I did that bad thing.

203
00:23:59,120 --> 00:24:03,600
Remember, we realize how you've hurt other people and you realize how you've caused suffering

204
00:24:03,600 --> 00:24:07,960
for others.

205
00:24:07,960 --> 00:24:12,640
And you feel very strongly the things that are causing you suffering.

206
00:24:12,640 --> 00:24:16,400
When you meditate, you'll think about what you want, all the things that you want you

207
00:24:16,400 --> 00:24:21,720
will think about and it will become so strong and so much suffering until you finally

208
00:24:21,720 --> 00:24:29,920
give it up.

209
00:24:29,920 --> 00:24:37,800
This is why some people, when they practice meditation, they think it's torture, think of

210
00:24:37,800 --> 00:24:45,400
meditation of some kind of bad, bad idea because it actually causes suffering.

211
00:24:45,400 --> 00:24:49,720
You have to sit still and go through all of this stuff that you just rather forget about,

212
00:24:49,720 --> 00:24:51,720
right?

213
00:24:51,720 --> 00:24:53,720
I don't want to have to think about this.

214
00:24:53,720 --> 00:24:59,720
I don't have to think about how much I want this, how much I want that.

215
00:24:59,720 --> 00:25:05,080
But that's exactly why we do it.

216
00:25:05,080 --> 00:25:09,040
Because if we don't work these things out now, we're going to have to work them out when

217
00:25:09,040 --> 00:25:10,040
we die.

218
00:25:10,040 --> 00:25:17,720
If we do all come up and it's not even only when we die, then death is just one example.

219
00:25:17,720 --> 00:25:21,760
That's an extreme example, but it's true in all cases.

220
00:25:21,760 --> 00:25:26,600
If we have anger inside, then when anything happens, we'll get anger and we'll say bad

221
00:25:26,600 --> 00:25:29,040
things to others.

222
00:25:29,040 --> 00:25:34,600
If we have greed inside, then anytime we want something, we'll be impossible to deal

223
00:25:34,600 --> 00:25:35,600
with.

224
00:25:35,600 --> 00:25:54,360
I don't want it, I don't want it.

225
00:25:54,360 --> 00:25:59,040
This is called the negligence.

226
00:25:59,040 --> 00:26:01,040
We cling to things.

227
00:26:01,040 --> 00:26:06,080
We always try to get what we want, and that's like picking flowers, because getting what

228
00:26:06,080 --> 00:26:10,520
you want doesn't make you a better person, it doesn't make you a happier person.

229
00:26:10,520 --> 00:26:16,640
We'll have this idea that the more you get the richer your life is.

230
00:26:16,640 --> 00:26:20,840
It's really not true, not any stretch of the imagination.

231
00:26:20,840 --> 00:26:26,720
The more you get what you want, the more wanting you have, the more clinging you have,

232
00:26:26,720 --> 00:26:29,480
the less fun you are to be around.

233
00:26:29,480 --> 00:26:33,000
Who are the nicest people, the people we love the most?

234
00:26:33,000 --> 00:26:38,680
Are the people we love the most, the ones who are the most taking people, the ones that

235
00:26:38,680 --> 00:26:39,680
take all the time?

236
00:26:39,680 --> 00:26:40,680
Oh, I love that person.

237
00:26:40,680 --> 00:26:44,440
Every time I go over to their house, they ask me, they ask me if they can borrow something.

238
00:26:44,440 --> 00:26:46,440
They ask me for money.

239
00:26:46,440 --> 00:26:48,240
I love that person.

240
00:26:48,240 --> 00:26:49,240
Who do we love?

241
00:26:49,240 --> 00:26:54,760
Every time I go to their house, they give me something.

242
00:26:54,760 --> 00:26:58,480
They're always helpful, they're always kind.

243
00:26:58,480 --> 00:27:02,000
These are the kind of people we love.

244
00:27:02,000 --> 00:27:03,440
So what's the kind of person that we want to be?

245
00:27:03,440 --> 00:27:07,320
Why is it that we want to be the person who gets things all the time?

246
00:27:07,320 --> 00:27:08,320
Give me, give me, give me.

247
00:27:08,320 --> 00:27:13,320
Why do we want to be that person when nobody likes such a person?

248
00:27:13,320 --> 00:27:24,400
What is the benefit that we get from the thing that we get?

249
00:27:24,400 --> 00:27:40,520
Do we like what we get, if we want it, if we want it, then we get it, do we like it?

250
00:27:40,520 --> 00:27:47,600
And then are we satisfied with that?

251
00:27:47,600 --> 00:27:53,680
Like when you get a new toy or Christmas, when Christmas comes, how long before your

252
00:27:53,680 --> 00:27:56,760
board of your toys at Christmas?

253
00:27:56,760 --> 00:27:58,880
How many hours?

254
00:27:58,880 --> 00:28:00,720
How many minutes?

255
00:28:00,720 --> 00:28:04,440
I remember Christmas when we get all these wonderful, it's really cool, they're just the

256
00:28:04,440 --> 00:28:05,440
coolest toys.

257
00:28:05,440 --> 00:28:12,920
And then like the next day we're like, okay, next Christmas I want this, Easter, what's

258
00:28:12,920 --> 00:28:13,920
next?

259
00:28:13,920 --> 00:28:14,920
Birthday.

260
00:28:14,920 --> 00:28:16,920
Birthday is coming up.

261
00:28:16,920 --> 00:28:17,920
Right.

262
00:28:17,920 --> 00:28:18,920
Do you have Christmas?

263
00:28:18,920 --> 00:28:21,680
You must have Christmas, yeah.

264
00:28:21,680 --> 00:28:35,000
We had Christmas and Hanukkah, and eight, Hanukkah's eight days of Christmas.

265
00:28:35,000 --> 00:28:42,240
Well that's the theory, it doesn't really happen that way.

266
00:28:42,240 --> 00:28:51,200
But no, these things don't satisfy us, things that we need are useful, but the point

267
00:28:51,200 --> 00:28:54,760
isn't, the point isn't getting what you want, the point is dedicating your life to that.

268
00:28:54,760 --> 00:28:57,280
What is most important in your life?

269
00:28:57,280 --> 00:29:00,000
Usually it's getting what we want, right?

270
00:29:00,000 --> 00:29:04,440
So there's no, there's no question about whether they're useful or not.

271
00:29:04,440 --> 00:29:08,200
The question is, why is that the focus of our lives?

272
00:29:08,200 --> 00:29:12,960
Why is that the kind of person we want to be, someone who gets?

273
00:29:12,960 --> 00:29:16,760
Why is that how we're using our life?

274
00:29:16,760 --> 00:29:21,640
We all know people like this, people who want, people who cling, people who take.

275
00:29:21,640 --> 00:29:25,080
We don't want to be around such people, oh, I don't want to go near that person, I know

276
00:29:25,080 --> 00:29:27,840
they're just going to ask me for this or that, right?

277
00:29:27,840 --> 00:29:33,360
If kids in class, we were always, what's that for lunch?

278
00:29:33,360 --> 00:29:35,600
Or did you do your homework?

279
00:29:35,600 --> 00:29:37,600
Can I copy it?

280
00:29:37,600 --> 00:29:40,600
It's kind of me.

281
00:29:40,600 --> 00:29:42,520
Always, always, always, can I borrow this?

282
00:29:42,520 --> 00:29:43,520
Can I borrow that?

283
00:29:43,520 --> 00:29:44,520
Can I borrow money?

284
00:29:44,520 --> 00:29:52,280
I think these are people we don't want to be.

285
00:29:52,280 --> 00:29:56,640
So we have to, we have to consider carefully, how do we want to live our lives?

286
00:29:56,640 --> 00:30:06,000
And how should we live our lives so that we'll be ready for anything, even death?

287
00:30:06,000 --> 00:30:09,320
Death is a great unknown, and for most of it, most of it's a very sad thing.

288
00:30:09,320 --> 00:30:15,160
I remember when I was young, it was one of the things that impaled me towards spirituality

289
00:30:15,160 --> 00:30:19,360
and finally Buddhism was being in my house and looking.

290
00:30:19,360 --> 00:30:26,080
I remember it's one of the oldest memories I have, or clearest of my young women, walking

291
00:30:26,080 --> 00:30:30,920
in the house and somehow it hit me, I don't remember how I walked downstairs and I looked

292
00:30:30,920 --> 00:30:38,440
at my mother and I said, she's going to die, but I thought about my father and I said,

293
00:30:38,440 --> 00:30:48,280
he's going to die, and when brothers, they're all going to die, what can I do about

294
00:30:48,280 --> 00:30:51,120
them?

295
00:30:51,120 --> 00:30:59,200
It's just this kind of thinking that's important for us, we have to, we have to have an

296
00:30:59,200 --> 00:31:08,640
answer here, we have to have, we have to have an answer to these questions.

297
00:31:08,640 --> 00:31:18,800
Death is an inevitability, something that we all have to face.

298
00:31:18,800 --> 00:31:24,600
It's not, it often sounds kind of depressing actually, I think, well, that's all life

299
00:31:24,600 --> 00:31:32,720
is just for death, but it's actually not, death is not a terminal point, it's a conjunction,

300
00:31:32,720 --> 00:31:43,400
it's a junction point between this life is certain, but when we get to that junction,

301
00:31:43,400 --> 00:31:46,360
we don't know which way we're going, are we going to go left, are we going to go right,

302
00:31:46,360 --> 00:31:48,880
are we going to go left, which way?

303
00:31:48,880 --> 00:31:52,920
That's very important, right?

304
00:31:52,920 --> 00:31:55,480
When you're driving down the street and you come to a junction point, if you don't know

305
00:31:55,480 --> 00:32:04,720
which way you're going to go, this crash into the sidewalk, left, no right, left, how do

306
00:32:04,720 --> 00:32:06,120
you know which way you're going to go?

307
00:32:06,120 --> 00:32:10,560
It's very important, so death isn't like terminal and you think, well, why waste all your

308
00:32:10,560 --> 00:32:12,240
time thinking about that?

309
00:32:12,240 --> 00:32:18,640
No, death is the moment where we become free from this certain life, this life which

310
00:32:18,640 --> 00:32:28,640
is set, nothing is set in stone and death.

311
00:32:28,640 --> 00:32:34,040
So this is, this verse is a call to a wake-up call that we shouldn't be like a sleeping

312
00:32:34,040 --> 00:32:44,520
village, we shouldn't be, we should wake up and take life, take the wheel, make the

313
00:32:44,520 --> 00:32:59,800
maker, take charge, that's a big charge of our lives, so that we don't become a slave

314
00:32:59,800 --> 00:33:06,360
to our desires and a slave to our partialities.

315
00:33:06,360 --> 00:33:10,000
You can see this, if you, I think it'd be really interesting to work in a hospice to work

316
00:33:10,000 --> 00:33:15,240
with dying people and to look and learn so much, I've talked to people who worked in hospice

317
00:33:15,240 --> 00:33:23,280
to care and many things, one of the things is how clear the mind becomes just before death.

318
00:33:23,280 --> 00:33:29,040
People with dementia, even schizophrenia, there are studies done, and probably not enough,

319
00:33:29,040 --> 00:33:34,960
but there are studies, if you look on the internet, of people with, I think schizophrenia,

320
00:33:34,960 --> 00:33:44,760
I think dementia as well, at the last moment, the mind becomes so clear and pure, or

321
00:33:44,760 --> 00:33:54,040
clear anyway, the point being that there is a lucidity at that last moment, and it's

322
00:33:54,040 --> 00:33:59,720
at that moment where the person has to face their karma, they have to face everything

323
00:33:59,720 --> 00:34:06,600
that they've done, some people when they're dying, you can see the fear in their eyes,

324
00:34:06,600 --> 00:34:12,280
people when they're in pain, they're such fear, and you can see the hunted look in their

325
00:34:12,280 --> 00:34:18,200
eyes, if they've killed other beings before, so if they can't come to terms with that

326
00:34:18,200 --> 00:34:27,640
in this life, if they can't work that out, let go of this guilt, this fear, they'll die

327
00:34:27,640 --> 00:34:38,760
with that, they'll certainly go to a bad place, so we have to take charge, and we have

328
00:34:38,760 --> 00:34:43,120
to be ready for this, this is why, excellent reason to meditate, because we've prepared

329
00:34:43,120 --> 00:34:47,920
for anything, when you meditate, you're ready, if it floods, you're ready, it's built

330
00:34:47,920 --> 00:35:08,800
up your things, sand, bags, whatever, embankments, you're ready for anything, so you sleep

331
00:35:08,800 --> 00:35:17,840
well, you sleep soundly, the person who meditates doesn't fear the future, the person

332
00:35:17,840 --> 00:35:22,080
who meditates isn't afraid of what comes, when you understand meditation, nothing makes

333
00:35:22,080 --> 00:35:29,680
you afraid, even when you're afraid, you say, well, I'm afraid of, it's a difference,

334
00:35:29,680 --> 00:35:33,120
really, because the person who meditates might still be angry, might still have greed,

335
00:35:33,120 --> 00:35:37,560
might still get up for afraid, you know, they might not have worked all these things out,

336
00:35:37,560 --> 00:35:42,840
but once they understand the meditation, even when these things come up, they don't go

337
00:35:42,840 --> 00:35:48,800
to that, they don't listen to the emotions, fear comes up and they just say, oh, it's

338
00:35:48,800 --> 00:35:57,640
fear, they don't say, okay, I have to run, they don't follow the emotion, when they're

339
00:35:57,640 --> 00:36:03,040
angry, they know they're angry, when they want something, they know they want something,

340
00:36:03,040 --> 00:36:05,640
why is it that when we want something, we say, well, then I have to get it, why don't

341
00:36:05,640 --> 00:36:16,400
we say, oh, I have to work on the one thing, why have we fallen into the idea that

342
00:36:16,400 --> 00:36:25,960
one thing is a sign that we need something, why do we equate these two, the many questions

343
00:36:25,960 --> 00:36:31,600
like this, that we have to study and we have to understand it is one thing really good

344
00:36:31,600 --> 00:36:41,240
for us, is it really happiness, people who say, the important point of the most important

345
00:36:41,240 --> 00:36:48,560
thing in life is to experience, to have these wonderful, amazing experiences and so on.

346
00:36:48,560 --> 00:36:52,600
But that's just a belief, we don't know if it's true, it sounds good, it's like, yeah,

347
00:36:52,600 --> 00:36:55,320
yeah, just do what you want, right?

348
00:36:55,320 --> 00:37:00,600
Okay, let's be scientific, let's ask ourselves, let's look and see what happens.

349
00:37:00,600 --> 00:37:06,960
When you get what you want, are you really happier, is it really a stable state and what

350
00:37:06,960 --> 00:37:14,560
happens when catastrophe hits, are you able to deal with it, how strong are you?

351
00:37:14,560 --> 00:37:22,520
So many people are, we would say, relatively weak, when you start meditating, you see

352
00:37:22,520 --> 00:37:31,400
how weak people are, weak, this weakness, this inability to deal with simple things and all

353
00:37:31,400 --> 00:37:34,800
these stories of people start meditating and then there's a little bit of noise around

354
00:37:34,800 --> 00:37:42,320
you can angry, it shows you how weak you are in meditation, this is an example of how strong

355
00:37:42,320 --> 00:37:52,080
it makes you, how it works out these weaknesses, remember after I finish our meditation course

356
00:37:52,080 --> 00:37:57,760
in Thailand, first meditation course and I went into the city with this friend, one of

357
00:37:57,760 --> 00:38:04,520
the fellow meditators and we got into the cafe and ordered some coffee or something and

358
00:38:04,520 --> 00:38:10,520
suddenly there was a huge, someone dropped a whole stack of dishes and huge noise and everybody

359
00:38:10,520 --> 00:38:15,320
in the, everybody in the, in the whole restaurant turn and look and we just sat there

360
00:38:15,320 --> 00:38:28,200
both of us like this hearing and then we looked at each other and said, meditate, it's

361
00:38:28,200 --> 00:38:35,240
exciting actually to know that you can, you don't have to follow your instincts, you

362
00:38:35,240 --> 00:38:42,440
don't have to follow the habits, you don't have to slave to your habits, you may follow

363
00:38:42,440 --> 00:38:48,400
them, you may not, but you can choose, you can decide and you can see with wisdom whether

364
00:38:48,400 --> 00:38:51,360
they're going to make you happier.

365
00:38:51,360 --> 00:38:57,840
So this is the important point of this verse that we should not be like the heedless person

366
00:38:57,840 --> 00:39:04,000
gathering flowers because it doesn't prepare you to for the future, it doesn't make

367
00:39:04,000 --> 00:39:09,720
you strong, it doesn't bring you peace and happiness, it just gets you a bunch of dead

368
00:39:09,720 --> 00:39:12,720
flowers.

369
00:39:12,720 --> 00:39:18,240
Alright, so that's the Dhammapada verse tonight, number 47, thank you for tuning in, I hope

370
00:39:18,240 --> 00:39:24,200
that this has been useful and a reminder for us all that we should take, take life seriously,

371
00:39:24,200 --> 00:39:30,000
if you want to be happy you should seriously figure out what is true happiness, happiness

372
00:39:30,000 --> 00:39:36,280
is a very serious topic, if it were not everyone would be happy, so let's take this

373
00:39:36,280 --> 00:39:43,000
seriously and truly find peace, happiness and freedom from suffering, thank you and wish

374
00:39:43,000 --> 00:40:12,840
you all the best.

