1
00:00:00,000 --> 00:00:06,120
Go ahead. This is more for you to domo but would like the other monks opinion also.

2
00:00:06,120 --> 00:00:10,520
Would you agree that some people ordain his monks or nuns because they don't

3
00:00:10,520 --> 00:00:14,640
feel like they actually belong to any social group and the fact that they are

4
00:00:14,640 --> 00:00:19,360
monks slash nuns gives them some sort of comfort knowing that they're part of

5
00:00:19,360 --> 00:00:23,400
something. Also would it be fair to suggest that it gives them some sort of

6
00:00:23,400 --> 00:00:27,440
power and they like the status and attention they get when out in public

7
00:00:27,440 --> 00:00:40,480
especially in the West? I don't know. I haven't encountered that many monks yet.

8
00:00:40,480 --> 00:00:51,040
I mean there's somebody belonging to something. I think that depends because

9
00:00:51,040 --> 00:00:56,680
maybe it's the group that they belong to. It's like any social group.

10
00:00:56,680 --> 00:01:01,480
One person won't belong to every different social group. There's a certain

11
00:01:01,480 --> 00:01:07,640
group that they fit in with better. Some people fit in with a monastic crowd.

12
00:01:07,640 --> 00:01:17,720
I think giving any sort of opinion of why people are day-in or so on is or

13
00:01:17,720 --> 00:01:22,000
conjecturing in this sort of way is quite difficult unless you're actually

14
00:01:22,000 --> 00:01:25,160
a monk yourself and have a sense of what it means to be a monk and what it's

15
00:01:25,160 --> 00:01:33,600
like to be a monk. The idea of ordaining or dating to become a part of

16
00:01:33,600 --> 00:01:42,720
something, I don't know man. I think for sure some people ordain because they

17
00:01:42,720 --> 00:01:47,720
don't fit in with society but I think in general you have to just say that

18
00:01:47,720 --> 00:01:52,200
people ordain because they're suffering because society makes them

19
00:01:52,200 --> 00:01:57,280
suffering because there's a lot of stress and pointless stress. They

20
00:01:57,280 --> 00:02:03,480
don't see the point of it. They feel it's pointless. That people would

21
00:02:03,480 --> 00:02:08,880
actually, I mean there's countless reasons why people ordain some people

22
00:02:08,880 --> 00:02:14,040
ordain from what seems like to me because they like being around other men

23
00:02:14,040 --> 00:02:23,120
wearing skirts. There are people like this who get a kick or even more out of

24
00:02:23,120 --> 00:02:31,960
that kind of thing. I mean you see I mean Buddhism is a huge institution. I

25
00:02:31,960 --> 00:02:36,560
mean there are monks that have dating that date other monks. There's a dating

26
00:02:36,560 --> 00:02:48,960
service in Japan for monks now. To say why people ordain people ordain for

27
00:02:48,960 --> 00:03:06,320
various different reasons. In an ideal for an ideal reason to ordain is for

28
00:03:06,320 --> 00:03:11,320
overcoming suffering, for the finding the way out of suffering, finding the

29
00:03:11,320 --> 00:03:19,280
truth that allows you to become free from suffering. Now I think what I

30
00:03:19,280 --> 00:03:26,160
might agree with is that once monks ordain, no matter what their intentions were

31
00:03:26,160 --> 00:03:31,120
before they ordain and let's assume that we have let's imagine we have a

32
00:03:31,120 --> 00:03:40,520
monk who ordain with the best of intentions. Now in general, this is why I

33
00:03:40,520 --> 00:03:44,880
said you really have to ordain to even make some sort of conjecture or to

34
00:03:44,880 --> 00:03:50,160
even begin to approach what it's like to be a monk because they change

35
00:03:50,160 --> 00:03:55,520
once they ordain their idea of what it was to be a monk or their idea of what

36
00:03:55,520 --> 00:04:00,440
they were going to be like as a monk is never the same as how they actually

37
00:04:00,440 --> 00:04:05,400
are as a monk. Just as our idea of who we are in general is never the same as

38
00:04:05,400 --> 00:04:10,040
how we actually appear to other people or how we actually react or actually

39
00:04:10,040 --> 00:04:16,920
act in reality. So many monks when they ordain the majority of monks when they

40
00:04:16,920 --> 00:04:26,760
ordain begin to or not begin to instantly develop an ego or a in some

41
00:04:26,760 --> 00:04:34,040
cases it's not an ego but like they take on it, take it on as a role like now

42
00:04:34,040 --> 00:04:41,120
I'm a monk and so they start to act in a certain way and to an extent this

43
00:04:41,120 --> 00:04:46,120
is proper. A monk should act differently. They should guard themselves and

44
00:04:46,120 --> 00:04:49,840
they should dedicate them to themselves to the practice but it should be

45
00:04:49,840 --> 00:04:54,680
a totally a practical move. They should be guarded in their senses not looking

46
00:04:54,680 --> 00:05:01,160
around and so on but what happens is often quite often people will take it

47
00:05:01,160 --> 00:05:07,280
into their head that they are a monk and begin to develop a role and the idea

48
00:05:07,280 --> 00:05:16,560
of I being a monk. This gets worse when they are respected and given support by

49
00:05:16,560 --> 00:05:21,680
lay people and maybe even taking up as teachers and they start to give talks

50
00:05:21,680 --> 00:05:29,640
and teach people Buddhism and meditation then they can then they can become

51
00:05:29,640 --> 00:05:34,040
attached to the status and the attention. I don't know that people ordain for

52
00:05:34,040 --> 00:05:37,160
these reasons and as I said people ordain for all sorts of reasons so it's

53
00:05:37,160 --> 00:05:43,440
not really fair but the average person who ordains with good intentions I don't

54
00:05:43,440 --> 00:05:47,520
think it's so much in their mind until after they ordain it's something that's

55
00:05:47,520 --> 00:05:51,560
very easy for a new monk to fall into and then what happens I've talked about

56
00:05:51,560 --> 00:05:55,920
this before what you see happens is that they slowly slowly slowly start to

57
00:05:55,920 --> 00:06:00,360
become a better monk and have a lower opinion of themselves so their opinion is

58
00:06:00,360 --> 00:06:07,400
like this in the beginning and their proficiency or their good ability as a

59
00:06:07,400 --> 00:06:12,000
monk is very much very low because they've just started out and it levels

60
00:06:12,000 --> 00:06:18,480
out and eventually if they continue on the ego disappears and the proficiency

61
00:06:18,480 --> 00:06:22,720
as a monk increases even monks who aren't that dedicated to the teaching or

62
00:06:22,720 --> 00:06:28,080
to meditation once they've been ordained for 10 years 20 years what you

63
00:06:28,080 --> 00:06:32,600
generally tend to see is they're quite humble quite laid back quite

64
00:06:32,600 --> 00:06:41,720
comfortable in just being who they are and that actually is in general a fairly

65
00:06:41,720 --> 00:06:47,480
good well-behaved monk who fits in well with with the system if they don't

66
00:06:47,480 --> 00:06:54,880
then then they tend to disrobe or start their own their own new school of

67
00:06:54,880 --> 00:07:11,520
Buddhism or so and so don't know if that's helpful that's the answer

