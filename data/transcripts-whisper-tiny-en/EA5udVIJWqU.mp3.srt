1
00:00:00,000 --> 00:00:15,120
Okay, good evening everyone broadcasting live December 1st 2015.

2
00:00:15,120 --> 00:00:21,720
So I had two people come up to me this morning after all this changing the color of my

3
00:00:21,720 --> 00:00:23,400
robe and everything.

4
00:00:23,400 --> 00:00:29,720
See, I think before the other color just looked like a stunt and now that I've changed

5
00:00:29,720 --> 00:00:36,120
to a more reasonable color, people are more comfortable coming up and talking to me.

6
00:00:36,120 --> 00:00:37,640
It was really funny.

7
00:00:37,640 --> 00:00:44,560
This man suddenly I'm walking in the student center and suddenly there's a hay or

8
00:00:44,560 --> 00:00:52,520
high or something like that and I turn and sure enough there's someone walking with me.

9
00:00:52,520 --> 00:00:59,600
And then he just says, you can't dress like that and not expect people to come up and ask

10
00:00:59,600 --> 00:01:06,880
you questions and I said, why not?

11
00:01:06,880 --> 00:01:08,680
Why not?

12
00:01:08,680 --> 00:01:17,720
Leave me alone but may stop and try to get me to explain to him Buddhism in the middle

13
00:01:17,720 --> 00:01:27,760
of this crowded place and I said, look, it's not something I can easily tell you in

14
00:01:27,760 --> 00:01:31,920
this short time but I gave him my booklet on how to meditate and just as I was handing

15
00:01:31,920 --> 00:01:42,960
him the booklet, this woman comes up and starts talking to me but she was fairly abrupt.

16
00:01:42,960 --> 00:01:48,600
She said, I thought you were a Hindu monk, I'm a practicing Hindu and if you were a Hindu

17
00:01:48,600 --> 00:01:59,280
monk then you just start to left it hanging like, if you were one of us I would be interested.

18
00:01:59,280 --> 00:02:11,760
Somebody has said it out, I just said, okay, I'm going to go to class now, but I'm slowly

19
00:02:11,760 --> 00:02:16,720
converting the smokers section, actually not the smokers section.

20
00:02:16,720 --> 00:02:23,360
There's friends of friends of friends, so Katya is in my class and she started practicing

21
00:02:23,360 --> 00:02:29,760
meditation and then her friend Sarah joined us and I taught her about the meditate and

22
00:02:29,760 --> 00:02:39,320
then Sarah's friend Nanaling joined us so now I've got three, the three of us, the four of

23
00:02:39,320 --> 00:02:42,640
us meditated together.

24
00:02:42,640 --> 00:02:48,840
So I'm teaching meditations slowly and then this person in my Latin class came up to me.

25
00:02:48,840 --> 00:02:53,040
I think she's in my Latin class, I knew that it was one of the visiting high school students

26
00:02:53,040 --> 00:02:58,440
because we had some high school students visiting, it was the first time someone came

27
00:02:58,440 --> 00:03:05,960
up to me so she said, hey, what's your name, my name's Yupta Damo.

28
00:03:05,960 --> 00:03:13,440
You do, you have some videos on YouTube, it's like I gave her my card, did you want

29
00:03:13,440 --> 00:03:34,960
to learn how to meditate, what else, just that's it.

30
00:03:34,960 --> 00:03:44,360
So the thing in Florida, we just talked about that, so yeah, we were talking about that later

31
00:03:44,360 --> 00:03:58,880
I guess, right, we'll announce it, we're going to give gifts to orphans, I think,

32
00:03:58,880 --> 00:04:05,280
our children who are not as your orphans would foster children or children who are in a home,

33
00:04:05,280 --> 00:04:13,160
homeless kids I guess is maybe, to this charity that looks after kids and not just kids,

34
00:04:13,160 --> 00:04:14,960
but looks after people who are in trouble.

35
00:04:14,960 --> 00:04:21,560
So if you're looking for a charity to give to this winter, or if you'd just like to do

36
00:04:21,560 --> 00:04:28,160
a good deed, we're going to set up, Robin's going to help set up the campaign.

37
00:04:28,160 --> 00:04:33,120
So as she said, she suggested what we knew as we combined it with the book campaign.

38
00:04:33,120 --> 00:04:36,440
So Robin, maybe you want to talk about that for a bit.

39
00:04:36,440 --> 00:04:42,600
Sure, we've been talking about different things that would be nice to do, especially this

40
00:04:42,600 --> 00:04:49,320
time of year, and there's an opportunity to have some of the how to meditate books reprinted,

41
00:04:49,320 --> 00:04:55,920
1000 more copies reprinted, which is great, great way to spread that information.

42
00:04:55,920 --> 00:05:00,840
So we'd like to set up a campaign for anyone who would like to donate towards this because

43
00:05:00,840 --> 00:05:08,280
all the reprinting is done by donation, and we'd also like to, as Dante was saying, support

44
00:05:08,280 --> 00:05:14,360
the children's home in Tampa, Florida with doing something nice for the resident children

45
00:05:14,360 --> 00:05:16,000
at this holiday season.

46
00:05:16,000 --> 00:05:22,480
So I'll have all of that ready tomorrow for anyone who would like to participate in both

47
00:05:22,480 --> 00:05:23,480
of those programs.

48
00:05:23,480 --> 00:05:28,040
There would be one campaign for the two, with the first part would be for what is needed

49
00:05:28,040 --> 00:05:31,720
for the printing of the books, and anything left over after the printing of the books

50
00:05:31,720 --> 00:05:37,000
will be for the benefit of the children at the children's home in Tampa, Florida.

51
00:05:37,000 --> 00:05:42,440
It's just that I mean I'll be in Florida, and I really want to do something on behalf of

52
00:05:42,440 --> 00:05:50,760
my family, so that's my point, but then I thought, well, that'd be a neat thing for

53
00:05:50,760 --> 00:05:55,000
everyone, the other people to do is you want to do something on behalf of someone that's

54
00:05:55,000 --> 00:06:00,560
Christian with this holiday season, you can give a gift on behalf of someone you know.

55
00:06:00,560 --> 00:06:04,960
Instead of giving gifts to each other, you can say, well, our family will go into something

56
00:06:04,960 --> 00:06:08,280
good for others, so the money that we're going to spend on gifts for each other will

57
00:06:08,280 --> 00:06:14,600
spend on gifts for homeless kids, and then you've given each other a gift, you've done

58
00:06:14,600 --> 00:06:18,960
something good on each other's name and made each other feel happy and so on.

59
00:06:18,960 --> 00:06:25,520
So take part in this, rather than go looking for some charity and do it together with us,

60
00:06:25,520 --> 00:06:30,640
and we can, when we pull our resources, we can really make a difference, right?

61
00:06:30,640 --> 00:06:31,640
Definitely.

62
00:06:31,640 --> 00:06:34,640
We have a great community.

63
00:06:34,640 --> 00:06:38,160
So, just that.

64
00:06:38,160 --> 00:06:50,960
I need some more, I can't remember.

65
00:06:50,960 --> 00:06:54,560
We have no questions.

66
00:06:54,560 --> 00:06:57,560
I have a question though.

67
00:06:57,560 --> 00:07:00,680
We didn't post the link to the hangout.

68
00:07:00,680 --> 00:07:02,200
Do you see the link to the hangout?

69
00:07:02,200 --> 00:07:07,440
No, that's only on the on your screen.

70
00:07:07,440 --> 00:07:11,880
Here's the link, if someone wants to come on the hangout and say hello, you can come

71
00:07:11,880 --> 00:07:12,880
live.

72
00:07:12,880 --> 00:07:28,080
If you're audacious, it's my Latin teacher said, Oh, Dax, if you're brave.

73
00:07:28,080 --> 00:07:38,400
Oh, I had a question, Bumpay, we talked a little bit about you potentially restarting

74
00:07:38,400 --> 00:07:44,680
the Buddhism 101, have you given any thought to that?

75
00:07:44,680 --> 00:07:51,520
Well, yeah, if I get more time, I mean, I can do it with that, but I don't mean it's not

76
00:07:51,520 --> 00:07:56,520
like any more time than just replace one of the Dhamapanda with the Buddhism 101 video.

77
00:07:56,520 --> 00:08:04,240
It does take a little bit of thought to get it straight, but it's not that big a video.

78
00:08:04,240 --> 00:08:15,400
So we can do that on, say, Wednesdays, that would be great.

79
00:08:15,400 --> 00:08:26,200
You can really expand it and just start talking about different topics.

80
00:08:26,200 --> 00:08:29,760
I would really like that.

81
00:08:29,760 --> 00:08:35,920
I kind of miss studying the core Buddhist principles, but I mean, when you're talking

82
00:08:35,920 --> 00:08:39,440
about core principles, it's easy to start talking about it, but it would be nice to spend

83
00:08:39,440 --> 00:08:43,640
a little bit of time and make sure I got all the facts straight, and that does take a little

84
00:08:43,640 --> 00:08:45,600
bit of planning.

85
00:08:45,600 --> 00:08:53,960
I mean, the Namapada is one thing, I mean, you have to read the story, but it's the teaching

86
00:08:53,960 --> 00:09:01,240
isn't in depth, I don't, I suppose it's not that big a video, I mean, it's just basic

87
00:09:01,240 --> 00:09:02,240
concepts.

88
00:09:02,240 --> 00:09:11,960
I know all the basic, I know it's like I need to research, really, but it might not last

89
00:09:11,960 --> 00:09:16,760
that long because that I think it's finding the next topic, trying to think, okay, what

90
00:09:16,760 --> 00:09:23,680
should I teach next, see, I can try that.

91
00:09:23,680 --> 00:09:28,120
Let me try that tomorrow, I do know what I need to do to finish it, it was supposed to

92
00:09:28,120 --> 00:09:36,640
be five videos, maybe six in the series, so I know, I know which one comes next, I think,

93
00:09:36,640 --> 00:09:41,920
actually I don't know, I don't remember where I ended, but I know the sequence, I think

94
00:09:41,920 --> 00:09:43,960
I finished morality, right?

95
00:09:43,960 --> 00:09:46,360
I can check on that.

96
00:09:46,360 --> 00:09:52,640
The next one, oh, then it gets a bit difficult, see, it was a, I was planning on doing

97
00:09:52,640 --> 00:10:00,320
it based on the Anupupika tab, but it's teaching in order, and so the next one is supposed

98
00:10:00,320 --> 00:10:16,200
to be what haven't, which really describes the, the benefits of, of good deeds.

99
00:10:16,200 --> 00:10:22,440
So far, Dante, you have generosity, morality, one, morality, two, morality, three, and morality

100
00:10:22,440 --> 00:10:23,440
four.

101
00:10:23,440 --> 00:10:25,560
Yeah, that's all about morality.

102
00:10:25,560 --> 00:10:31,080
So the next will be, it's supposed to be what haven't, and I think that means in regards

103
00:10:31,080 --> 00:10:39,120
to the benefits, it's referring to the benefits of the wholesome karma, so I have to

104
00:10:39,120 --> 00:10:47,280
do a whole video on heaven, which just, you know, how to make it actually about the basics

105
00:10:47,280 --> 00:10:54,480
of Buddhism, because that's part of the Buddhism, or the teaching, so what is meant by that?

106
00:10:54,480 --> 00:10:59,200
Karma, I guess, really that's what it's about, and so that's the next one should be both

107
00:10:59,200 --> 00:11:01,200
karma, I guess.

108
00:11:01,200 --> 00:11:10,200
Hmm, it's going to be a video, of course I already have videos on karma, but I'll do

109
00:11:10,200 --> 00:11:15,880
a new one, basic, no, basic karma, so then I have to think, how do I present it, that's

110
00:11:15,880 --> 00:11:20,680
the other thing you see, I know that teaching, how do I present this to people, and we have

111
00:11:20,680 --> 00:11:29,520
to consider the audience, this is 101, so let's take a little planning, and I've got exams

112
00:11:29,520 --> 00:11:31,320
coming up.

113
00:11:31,320 --> 00:11:36,880
So is this something to start, that's better started after the first of the year, after

114
00:11:36,880 --> 00:11:47,840
my exams are over, so maybe after the first of the year, yeah, except I might be going

115
00:11:47,840 --> 00:12:07,240
back to school, we'll see, I really shouldn't know, we should just quit it.

116
00:12:07,240 --> 00:12:27,920
Would you do things like, in the Buddhism 101, would you do like the Four Noble Truths

117
00:12:27,920 --> 00:12:33,960
in the A-Fold Noble Path, yeah, I mean, it ends up with the Four Noble Truths, that

118
00:12:33,960 --> 00:12:40,760
would be the sixth video, okay, so that's already in there, and then in the Four Noble

119
00:12:40,760 --> 00:12:47,680
Truths, you probably deal with each Noble Truths individually, so you'd have at least

120
00:12:47,680 --> 00:12:55,000
four videos probably, make sense.

121
00:12:55,000 --> 00:13:00,960
And so maybe the fourth video would be eight videos or something, you could expand it

122
00:13:00,960 --> 00:13:05,200
like that, and be neat, turn it into a fractal.

123
00:13:30,960 --> 00:14:00,840
Country, it's not a good time, because it's like the middle of the night, hmm.

124
00:14:00,840 --> 00:14:05,720
Sorry, Monday, your screen froze, and I think we might have missed part of what you were

125
00:14:05,720 --> 00:14:06,720
saying.

126
00:14:06,720 --> 00:14:07,720
Yeah, really?

127
00:14:07,720 --> 00:14:11,160
Yeah, you were frozen for a bit there.

128
00:14:11,160 --> 00:14:15,640
Maybe it could have been you, though, could have been your connection.

129
00:14:15,640 --> 00:14:16,640
Maybe.

130
00:14:16,640 --> 00:14:17,960
Maybe it was mine, no.

131
00:14:17,960 --> 00:14:25,080
Your main internet connection is pretty, pretty good to hear, okay, so I'm surprised to hear

132
00:14:25,080 --> 00:14:26,080
that.

133
00:14:26,080 --> 00:14:28,120
Maybe it was just me.

134
00:14:28,120 --> 00:14:33,760
Hello, Bante, this is stupid, but I'm wondering if you're familiar with Daniel Ingram

135
00:14:33,760 --> 00:14:38,280
and mastering the court teachings of the Buddha, and if so, your thoughts on his method.

136
00:14:38,280 --> 00:14:42,080
Yeah, sorry, don't answer your thoughts on questions.

137
00:14:42,080 --> 00:14:46,360
I'm not my teaching, I don't have thoughts on it.

138
00:14:46,360 --> 00:14:51,400
Let's see for that way.

139
00:14:51,400 --> 00:15:07,000
I know Bambaram, the one question of that, and yes, I am, yeah, I guess I mean, I should

140
00:15:07,000 --> 00:15:12,680
at least say that that's not what I teach, so he does talk about Mahasi Sayadha's teaching,

141
00:15:12,680 --> 00:15:18,640
but it's important to understand our tradition is the Mahasi Sayadha tradition, so if you

142
00:15:18,640 --> 00:15:25,040
want it to ask me something about Mahasi Sayadha tradition, I can talk about that, because

143
00:15:25,040 --> 00:15:27,560
that's us.

144
00:15:27,560 --> 00:15:36,840
Should I learn about meditation first or about Suta's first?

145
00:15:36,840 --> 00:15:45,720
Meditation, I would say.

146
00:15:45,720 --> 00:15:52,600
But where the Suta's, they were talks designed to encourage people in practice, so you

147
00:15:52,600 --> 00:16:19,160
should read about how to practice, read something that allows you to practice.

148
00:16:19,160 --> 00:16:23,760
They didn't have books where they studied.

149
00:16:23,760 --> 00:16:28,600
The monks didn't do studying, they were reciting and listening to talk after talk.

150
00:16:28,600 --> 00:16:34,520
It was that they listened to many, learned lots of different dhamma, but it was more

151
00:16:34,520 --> 00:16:39,240
once they were into it, you know.

152
00:16:39,240 --> 00:16:43,480
So meditation should come first, learning about how to practice, and that's also very

153
00:16:43,480 --> 00:16:48,800
important because you're understanding and your appreciation of the Buddha's teaching,

154
00:16:48,800 --> 00:16:51,360
and just dramatically once you start to practice.

155
00:16:51,360 --> 00:16:57,280
If you haven't been practicing meditation, it's often very difficult to understand and

156
00:16:57,280 --> 00:17:04,200
even appreciate or have any interest in the Buddha's teaching.

157
00:17:04,200 --> 00:17:11,120
Just as I don't know if it's fair to say, but you know, like ordinary animals aren't

158
00:17:11,120 --> 00:17:19,440
able to appreciate things like cell phone, right?

159
00:17:19,440 --> 00:17:26,040
So there is a sense that there is an ordinary person who's not meditating, is missing

160
00:17:26,040 --> 00:17:36,440
some clarity of mind that allows them to see the value of things beyond the material.

161
00:17:36,440 --> 00:17:43,720
It's not quite fair, but most people, I mean, I was, I know I was unable to really see

162
00:17:43,720 --> 00:17:52,200
the value of so many things, but when you meditate, it brings clarity.

163
00:17:52,200 --> 00:17:56,360
And the Buddha's teaching was very interesting to me after that, you know, I don't think

164
00:17:56,360 --> 00:17:59,200
it was something that I was at all keen on.

165
00:17:59,200 --> 00:18:03,640
You know, even when you hear the Buddha say, I read the Buddha said, when you walk, just

166
00:18:03,640 --> 00:18:07,160
walk, when you sit, just sit.

167
00:18:07,160 --> 00:18:12,160
I didn't really appreciate that, I didn't have any meaning to me, but then you meditate

168
00:18:12,160 --> 00:18:23,520
and say, okay, understand, it's meaningful because you're practicing.

169
00:18:23,520 --> 00:18:29,000
So I guess, I mean, the thing is don't, don't study if you're not meditating, don't

170
00:18:29,000 --> 00:18:35,040
go and study without meditating.

171
00:18:35,040 --> 00:18:39,960
That doesn't mean you have to not study and just to meditating, you can do them together.

172
00:18:39,960 --> 00:18:45,280
I mean, ideally, you've got to teach your who's teaching, and you usually don't need

173
00:18:45,280 --> 00:19:03,880
to study at all, you can just follow their instruction.

174
00:19:03,880 --> 00:19:30,760
Thank you.

175
00:19:33,880 --> 00:19:59,880
Okay, well, there's no more questions, and we'll just end it.

176
00:19:59,880 --> 00:20:06,880
Thank you, Dante.

177
00:20:06,880 --> 00:20:09,880
I think, oh, that's what it was.

178
00:20:09,880 --> 00:20:11,880
I knew there was one more thing.

179
00:20:11,880 --> 00:20:16,880
And the owner came by, maybe this isn't really a big deal for the broadcast, but it's something

180
00:20:16,880 --> 00:20:20,880
that we should probably talk about.

181
00:20:20,880 --> 00:20:26,880
He called me last night, or last night, and asked if he could bring someone over to

182
00:20:26,880 --> 00:20:31,880
the house, because they want to buy it.

183
00:20:31,880 --> 00:20:45,880
So, we got me thinking probably we should at least be on the lookout for potential of other

184
00:20:45,880 --> 00:20:46,880
places.

185
00:20:46,880 --> 00:20:47,880
I mean, I don't know what that means.

186
00:20:47,880 --> 00:20:53,880
I should have probably asked whether that means we'd be able to continue renting it another

187
00:20:53,880 --> 00:20:57,880
year, but it may be that we don't want this place.

188
00:20:57,880 --> 00:21:08,880
It's not that big, and maybe we could look for something that's a little more, maybe even

189
00:21:08,880 --> 00:21:14,880
another quieter, you know, because this is on a busy street.

190
00:21:14,880 --> 00:21:22,880
Yeah, I think we had looked into it before the lease was signed, and if they, if the homeowners

191
00:21:22,880 --> 00:21:30,880
sold it, you do have the right to stay through your lease, but maybe you don't want to.

192
00:21:30,880 --> 00:21:35,880
Yeah, I mean, we'd have the right to stay, but at the end of this year, who knows what would

193
00:21:35,880 --> 00:21:36,880
happen?

194
00:21:36,880 --> 00:21:38,880
You know, they have the right to cancel it for the next year.

195
00:21:38,880 --> 00:21:40,880
We don't have a contract.

196
00:21:40,880 --> 00:21:41,880
Right.

197
00:21:41,880 --> 00:21:43,880
They'd have to give us just two months.

198
00:21:43,880 --> 00:21:46,880
We've noticed.

199
00:21:46,880 --> 00:21:48,880
The impermanence, really.

200
00:21:48,880 --> 00:21:53,880
You know, we have a place finally, we're even on Google Maps, so we have something stable

201
00:21:53,880 --> 00:21:56,880
and permanent, lasting.

202
00:21:56,880 --> 00:21:59,880
No, no such thing.

203
00:21:59,880 --> 00:22:01,880
No such thing.

204
00:22:01,880 --> 00:22:06,880
But wherever you end up, there you are, and we'll get that place on Google Maps too.

205
00:22:06,880 --> 00:22:07,880
Yeah.

206
00:22:07,880 --> 00:22:12,880
I think I'm sure there's a change of a location thing.

207
00:22:12,880 --> 00:22:14,880
Oh, definitely.

208
00:22:14,880 --> 00:22:15,880
Okay.

209
00:22:15,880 --> 00:22:17,880
Good night, everyone.

210
00:22:17,880 --> 00:22:18,880
Bravo.

211
00:22:18,880 --> 00:22:19,880
Thank you, Dante.

212
00:22:19,880 --> 00:22:48,880
Good night.

