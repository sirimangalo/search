WEBVTT

00:00.000 --> 00:20.560
Okay, good evening everyone back for more questions and answers, so tonight's question

00:20.560 --> 00:35.280
is on the healing power of meditation. The, it's a common question, the common theme is the

00:35.280 --> 00:46.960
question of whether meditation can heal x, y, or z, can do x, y, or z, or z.

00:46.960 --> 01:02.440
Can do this, can meditation do that? This question actually specifically asks whether, and

01:02.440 --> 01:11.120
it's a good example question, whether meditation and mindfulness can cure the damage

01:11.120 --> 01:19.520
to the brain and the mind caused by psychedelics. That's an interesting question, I'm not

01:19.520 --> 01:25.520
sure whether this person has heard me talk about my experience with psychedelics because

01:31.600 --> 01:36.640
I think my brain was somewhat damaged by psychedelics temporarily permanently, I don't know.

01:36.640 --> 01:44.640
I've done psychedelics before and there have been no problems, but something in this one

01:46.640 --> 01:54.640
really changed the way my brain sort of functioned, there was something wrong. I felt like I had

01:54.640 --> 02:07.120
been pulled away from the experiential world and there was a sort of a lapse or a disconnect

02:08.560 --> 02:13.600
and it was acutely felt every time I then did drugs usually cannabis done.

02:13.600 --> 02:28.080
What's cannabis, cannabis is marijuana, right? I would, I would feel this again, this disconnect,

02:28.080 --> 02:35.840
like I wasn't really there. No cannabis in moderation does not have phenomenal potential.

02:35.840 --> 02:40.960
Your people in chat, you're listening to the dhamma now, if you're not interested in listening,

02:40.960 --> 02:47.600
go elsewhere, not so many comments. I don't need a running comment, do you please?

02:51.760 --> 02:52.240
Thank you.

02:59.600 --> 03:03.920
And it was many, it was, well it turned me off of the drugs really.

03:05.520 --> 03:09.040
And so not to say that drugs do that, drugs are not all the same,

03:09.040 --> 03:14.080
but I couldn't do them anymore, I couldn't get stoned anymore.

03:16.560 --> 03:23.840
I think I was still in the alcohol for a while, whatever. Anyway, I empathize with the potential

03:26.800 --> 03:32.400
problems to the brain. I mean there have been studies done on how alcohol, caffeine, and marijuana

03:32.400 --> 03:38.880
do degrade the brain. I don't know, I mean it's studies I've seen. True, false, I don't know,

03:38.880 --> 03:44.960
I mean science is always learning new things, but there appears to be the potential for some

03:44.960 --> 03:51.360
connection to actual brain damage just by drinking lots of coffee or alcohol or taking lots of

03:51.360 --> 04:00.560
marijuana. But it's more extreme, psychedelics I guess, I mean apparently I'm not the only one who

04:00.560 --> 04:04.800
had this sort of experience where you really feel like you've damaged your brain in some way.

04:06.640 --> 04:11.920
Regardless, this isn't to talk about drugs, if you people are all these chats are talking about

04:11.920 --> 04:19.040
cannabis and so on, I'm not interested, please go elsewhere. It's not what this talk is about.

04:19.040 --> 04:24.000
This talk is about the healing power of meditation. If you suffer brain damage, I mean a really

04:24.000 --> 04:32.160
good question would be, what if you have a partial lobotomy or maybe you get into an accident and

04:32.160 --> 04:51.280
have brain trauma or maybe you, well for some reason another your brain gets damaged, maybe you're

04:51.280 --> 05:01.840
sniffing gasoline, right? That'll do it. And so there's this question of whether meditation

05:01.840 --> 05:05.680
can heal this. I mean I think of all the questions of can meditation do this in

05:05.680 --> 05:11.440
communication do that. This one has a good potential to be answered in the positive, right,

05:11.440 --> 05:18.080
because meditation deals with the mind, which is very closely related to the brain.

05:18.080 --> 05:33.280
And so the healing power of meditation might then extend to this sort of brain damage.

05:33.280 --> 05:38.640
The question was asking whether the damage to the mind can be healed. That's actually a completely

05:38.640 --> 05:44.560
different question. First you have to ask can the mind be damaged? What does it mean to have

05:44.560 --> 05:50.720
mind damage? Something I've actually never been asked before. So let's separate these out. First let's

05:50.720 --> 06:00.320
do away with that one. Mind damage. Mind damage can only mean the cultivation of bad habits, right?

06:00.320 --> 06:06.960
And bad habits can be pretty extreme. If you do something even once, that's quite extreme,

06:06.960 --> 06:15.680
then it can affect the mind in quite an extreme way. The mind can also be affected by physical

06:15.680 --> 06:25.040
changes. So if the brain were to undergo some severe trauma or so on, then the potential result

06:25.040 --> 06:32.960
to the mind might be quite extreme based on the person's reactivity. You're reacting to it

06:32.960 --> 06:41.200
and the inability to adapt to change the expectation and so on. The mind is organic as well.

06:41.200 --> 06:52.240
And so it can be affected by the physical. But it's not damaged per se. It's just caught up in

06:52.240 --> 06:59.120
habits and expectations is the big one. And those expectations are not met because of the changes

06:59.120 --> 07:06.240
to the physical. So that being healed, that absolutely can be healed through meditation.

07:07.360 --> 07:14.480
I mean one of the one of the one of the important aspects of this question is whether or not

07:14.480 --> 07:20.640
you mean in this life or whether you mean in future lives. I mean for the mind healing can go

07:20.640 --> 07:32.000
is ongoing and it can start as soon as you undertake training in meditation, for example. It continues

07:32.000 --> 07:38.640
in this life and it continues into the next life. It's possible that there are certain parameters

07:38.640 --> 07:44.880
under which it's not possible. It's possible that it might be not possible for a person to become

07:44.880 --> 07:50.720
enlightened in this life. So for a person to free themselves from bad habits. It's possible to be

07:50.720 --> 07:55.840
in such a situation. I could think of maybe a lobotomy. I don't know. I don't have any

07:55.840 --> 08:03.200
back any evidence or background for this. If a person has electroshock therapy or lobotomy or

08:03.200 --> 08:09.760
something, it may. I wouldn't say render the mind incapable of becoming enlightened. But it will

08:09.760 --> 08:16.000
make it very difficult or it will render it so difficult that the mind just has

08:16.000 --> 08:23.520
uncapable, incapable of changing its bad habits, of changing its reactions, its interactions

08:23.520 --> 08:29.600
with the physical. I mean changing to the extent of healing, of becoming enlightened really.

08:29.600 --> 08:46.960
The damage to the brain is in a whole other realm. The brain damage can affect the mind but

08:46.960 --> 08:54.400
only involves the stimuli that the mind is fed. So it is a different category. So having dealt

08:54.400 --> 09:01.360
with the mind, I mean most important thing to understand about the mind is it deals with reactions,

09:01.360 --> 09:09.760
interactions and habits of interactions that are affected by the physical. When we turn to the

09:09.760 --> 09:15.760
physical, we're in this realm of asking this question, can meditation do x, y, and z?

09:17.840 --> 09:21.520
And it's actually two different questions when we ask whether meditation can do something

09:21.520 --> 09:25.200
and whether mindfulness can do something. And an important part of this question that,

09:25.200 --> 09:34.000
for my perspective, is a clarification of what mindfulness does. What mindfulness meditation

09:35.280 --> 09:42.000
is for. What it is meant to do. Now it's certainly possible that you might sit down and practice

09:42.000 --> 09:46.720
meditation and mindfulness and you suddenly have all sorts of strange things happen. Maybe you

09:46.720 --> 09:52.560
remember your past life. Maybe you start floating in the air. Apparently this has happened to people.

09:52.560 --> 09:59.120
Not very common, but apparently yes. Many people, this is relatively common, where they'll hear

09:59.120 --> 10:04.240
things that aren't there. And there's a speculation that they're hearing things far away. Some

10:04.240 --> 10:09.840
people have been claimed to hear things far away. See things far away. Leave their body.

10:10.640 --> 10:15.120
That's a common one. Someone will leave their body and look down at themselves. They've been

10:15.120 --> 10:21.280
looking down. Oh, there's me sitting in meditation. This you hear true false. Some people are

10:21.280 --> 10:27.600
probably quite skeptical. But these are the kinds of things that can happen. So could mindfulness

10:27.600 --> 10:36.640
meditation do many, many things? Sure. It's important to understand that none of those things are

10:36.640 --> 10:42.640
what mindfulness meditation is for. It's not a problem. It's interesting to know that there are

10:42.640 --> 10:50.240
byproducts. But the other thing is the desire for those things to happen prevents you from practicing

10:50.240 --> 10:57.840
mindfulness. Even if you desire something like remembering past lives, anytime you want to remember

10:57.840 --> 11:03.360
your past life, you're no longer practicing mindfulness. You're now engaging in a desire for something.

11:03.360 --> 11:07.680
You might even think it's a wholesome desire or wholesome intention, but it's no longer mindfulness.

11:07.680 --> 11:14.640
And so if you desire for, if you're intention, this is the point, if you're intention in practicing

11:14.640 --> 11:19.840
mindfulness meditation, has anything to do with any of those things, healing the body, healing the

11:19.840 --> 11:28.400
brain, and healing your cold, healing your eyesight, making it more beautiful or something like that.

11:28.400 --> 11:33.440
No, anything. Even let's say wholesome things like maybe making me healthier or so on,

11:33.440 --> 11:40.400
that it detracts from your ability to practice mindfulness meditation. It actually makes it no longer

11:40.400 --> 11:46.480
mindfulness. It's an important point to keep in mind that when you ask these questions, the real

11:46.480 --> 11:54.480
problem is that you want for those things to happen. And the asking of the question puts you at

11:54.480 --> 12:00.400
an important dissonance in terms of mindfulness practice because if that's what's bringing you to

12:00.400 --> 12:04.480
meditation, to mindfulness meditation, you're not going to be practicing mindfulness meditation until

12:04.480 --> 12:12.240
you realize that it's getting in the way of not just you practicing this meditation that I teach,

12:12.240 --> 12:18.160
but it's getting in the way of you finding freedom from suffering. Your desire for freedom from

12:18.160 --> 12:24.960
brain damage even is going to get in the way of you finding freedom from suffering. So suppose

12:24.960 --> 12:31.120
brain damage, I got I got really sick in Sri Lanka and I had the feeling that it changed

12:31.120 --> 12:36.080
how my brain works and so some memory isn't quite as good as it used to be. I'm also just

12:36.080 --> 12:43.200
getting older so it could be that. But if I want for that to change, if I have this intention

12:44.160 --> 12:48.720
and this is often the thing you have a desire to remember better, there's even claims that mindfulness

12:48.720 --> 12:53.360
meditation improves your memory. I think for some people it does. I've also met other meditators,

12:53.360 --> 12:59.440
mindfulness meditators who don't have very good memory even though their mindfulness is quite good,

12:59.440 --> 13:06.080
it seems. And I think it does depend to some extent on the capacity of the brain, the functioning

13:06.080 --> 13:28.880
of the brain and so. And so there's this relationship. The mind can in some cases heal the brain,

13:28.880 --> 13:37.360
there might be healing that goes on. But the desire for it, the intention, the concern even

13:38.080 --> 13:45.680
for the healing of the brain is a problem. And so really not as the most important point

13:45.680 --> 13:53.280
because it's not so interesting to me or it's not something that I wish to make interesting. But

13:53.280 --> 14:01.440
I think there are other types of meditation that potentially have the express benefit of healing

14:01.440 --> 14:08.800
the brain, let's say, or healing the body or helping you remember past lives, obviously helping

14:08.800 --> 14:13.840
you float through the air, helping you leave your body. Lots of teachings, even Buddhism, outside

14:13.840 --> 14:19.440
of Buddhism, by Buddhists, you'll hear them talk about these things so many different kinds.

14:19.440 --> 14:24.560
And I think that's all valid. I mean, science would reject a lot of it. Like let's say I say

14:25.120 --> 14:29.040
I can give you a meditation that's going to cure your cancer. Scientists would say that's

14:29.760 --> 14:35.760
ridiculous and reckless to suggest such a thing. That doesn't really have any bearing on me saying

14:35.760 --> 14:42.240
it's possible. It's possible as far as I know. Anything's possible as far as I know because

14:42.240 --> 14:49.600
but partially because I just don't understand science. I'm not a material scientist to the

14:49.600 --> 14:55.520
extent that I can tell you what's physically possible, what's physically impossible. Could meditation

14:55.520 --> 15:03.040
regrow a loss limb? I think yes, it could. Science would, of course, many of your

15:03.600 --> 15:08.720
even here are probably saying that's ridiculous, it's impossible. But that's not really what I mean

15:08.720 --> 15:15.840
by possible. It's possible in the sense that I don't have proof and I don't not really

15:15.840 --> 15:21.280
interested in finding proof. It's not in my realm of what I can prove to say yes or no. It's

15:21.280 --> 15:27.600
just something out there. The more important point is that again, when you are practicing those

15:27.600 --> 15:34.640
meditations, it's to your detriment. It's generally to your detriment to be practicing meditation

15:34.640 --> 15:39.600
to heal this or to heal that because it's creating clinging, because it's creating distraction,

15:40.240 --> 15:47.360
because it's reinforcing your attachment to things like health, to things like the proper functioning

15:47.360 --> 15:52.560
of the brain even. If you want to remember better, it's going to get in the way of your meditation

15:52.560 --> 15:57.600
because you become attached to remembering and it's always temporary. When it goes away,

15:57.600 --> 16:05.120
etc, etc, etc, you're going to be upset and so on. The point of this and the point of me

16:05.120 --> 16:17.600
answering this question that I wanted to get across is that mindfulness requires letting go.

16:19.040 --> 16:25.760
That mindfulness meditation, before foundations of mindfulness, for the purpose of seeing insight,

16:25.760 --> 16:31.440
requires you really to not have any ambition, any desire, even the desire to become enlightened,

16:31.440 --> 16:36.320
is going to get in your way. It has to be about being here now. So people say, well,

16:36.320 --> 16:43.920
then what about wanting to meditate? It's really not a proper question or a proper concern

16:44.480 --> 16:50.000
because of the nature of mindfulness. Mindfulness is actually nothing. It's stopping everything.

16:50.000 --> 16:57.520
Are you here and now? It's not a cult because it's nothing. Me sitting here now when I can just

16:57.520 --> 17:08.400
sit here now without believe, without dogma, without any kind of ambition or desire. That's

17:08.400 --> 17:16.480
enlightenment. When a person can do that, it's not anything. It's not even a religion, really.

17:16.480 --> 17:25.600
It's the religion of losing your religion. So in the beginning, there's many desires and

17:26.480 --> 17:33.040
I think the best we can do and what's important here is to stop with extraneous desires.

17:33.920 --> 17:40.400
If you desire to become enlightened, as I said, it's going to get in your way. But it's better

17:40.400 --> 17:48.320
than desiring for meditation to straighten your teeth or fix your eyesight or even fix your brain,

17:48.320 --> 17:55.760
your memory, and so on. So I hope that was useful. I think we might turn off these coffee.

17:57.760 --> 18:04.160
This is the internet. It's a scary place. I think it's distracting. I mean, I appreciate those of

18:04.160 --> 18:11.680
you who are listening keenly, but I really would like it to be like that. I know the novel

18:11.680 --> 18:16.640
thing about live streaming is you can interact with a broadcaster. It's not really what this is

18:16.640 --> 18:23.840
about. And so maybe we could have a stream where I do interact with the comments. I do talk to

18:23.840 --> 18:29.440
people in the comments. The problem with that, of course, is we get these sorts of comments here

18:29.440 --> 18:42.000
I'll scroll through them. Sounds like a cult. A lot of these are good. No, I mean, it's

18:42.000 --> 18:49.920
a really good audience. It's really incredible. Here's someone asking me, someone called

18:49.920 --> 19:01.200
Truth is you can ask me when I'll be back in Ontario, calling me Noah. That's interesting.

19:01.200 --> 19:07.360
I think you've got some very good comments here. But the point stands, listening to the

19:07.360 --> 19:12.960
demo should be about this thing. So maybe we'll have times where I turn off the comments,

19:12.960 --> 19:22.880
and then we'll have streams where I have the comments on, and we can have a chat with the internet.

19:22.880 --> 19:30.480
What could possibly go wrong? Anyway, thank you all for tuning in. 74 people watching. That's

19:30.480 --> 19:47.200
great. Wish you all the best.

