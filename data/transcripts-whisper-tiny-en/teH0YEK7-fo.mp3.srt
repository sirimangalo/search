1
00:00:00,000 --> 00:00:05,000
Can you make progress and have insights if you practice one hour a day?

2
00:00:05,000 --> 00:00:09,000
Or do you need a retreat to progress and have insights?

3
00:00:09,000 --> 00:00:15,000
Of course it takes longer, but maybe after one or two years of one hour a day.

4
00:00:19,000 --> 00:00:23,000
Can you make progress and have insights if you practice one hour a day?

5
00:00:23,000 --> 00:00:25,000
Well, I think one person had an insight or enlightenment,

6
00:00:25,000 --> 00:00:28,000
after just hearing the Buddha speak.

7
00:00:28,000 --> 00:00:33,000
So, I guess.

8
00:00:33,000 --> 00:00:39,000
So, you're going to practice who you are because for some of us, that doesn't work.

9
00:00:39,000 --> 00:00:45,000
One hour a day, if you're meditating for one hour a day, I would assume

10
00:00:45,000 --> 00:00:49,000
you'd want to keep your practice going throughout the day.

11
00:00:49,000 --> 00:00:53,000
Not just leave it on the cushions.

12
00:00:53,000 --> 00:00:55,000
For sure.

13
00:00:55,000 --> 00:01:00,000
But you're only meditating for one hour a day.

14
00:01:02,000 --> 00:01:07,000
I think at the beginning, that is a long time.

15
00:01:07,000 --> 00:01:12,000
I know when I first started, I didn't think I could meditate for five minutes.

16
00:01:12,000 --> 00:01:16,000
And meditation is something that gets easier and easier,

17
00:01:16,000 --> 00:01:20,000
and at the beginning I used to time myself.

18
00:01:20,000 --> 00:01:27,000
And then you get to the point where you're worried about meditating too long.

19
00:01:27,000 --> 00:01:31,000
So, that's one thing.

20
00:01:31,000 --> 00:01:34,000
And for me at the beginning, one hour, in a day,

21
00:01:34,000 --> 00:01:36,000
would have been a lot of meditating.

22
00:01:36,000 --> 00:01:43,000
It's just something that just got easier and easier for me.

23
00:01:43,000 --> 00:01:47,000
And so, for a beginner, I think if they take small steps,

24
00:01:47,000 --> 00:01:50,000
they're more likely to stick with it.

25
00:01:50,000 --> 00:01:55,000
And also, if they know ahead of time that, hey, it starts off tough.

26
00:01:55,000 --> 00:02:00,000
And I think it's probably tough for most people at the beginning.

27
00:02:03,000 --> 00:02:07,000
I can only say that hours are not so much important,

28
00:02:07,000 --> 00:02:12,000
like moments when you're meditating.

29
00:02:12,000 --> 00:02:17,000
You can sit for hours and sit and don't make progress.

30
00:02:17,000 --> 00:02:20,000
For sure.

31
00:02:20,000 --> 00:02:25,000
And that's sort of how I wanted to approach this,

32
00:02:25,000 --> 00:02:29,000
for sure what Alfred said is perfect,

33
00:02:29,000 --> 00:02:33,000
perfect, good, perfectly good answer, and your answer as well.

34
00:02:33,000 --> 00:02:40,000
But one thing that can reassure us about such situations

35
00:02:40,000 --> 00:02:46,000
where you're only able to do X number of minutes or hours per day,

36
00:02:46,000 --> 00:02:51,000
is that at every moment that you're mindful,

37
00:02:51,000 --> 00:02:53,000
you've done something good for yourself.

38
00:02:53,000 --> 00:02:57,000
You've created what we call a wholesome karma.

39
00:02:57,000 --> 00:02:59,000
One moment of acknowledging,

40
00:02:59,000 --> 00:03:01,000
one time of saying to yourself,

41
00:03:01,000 --> 00:03:02,000
rising or saying to yourself,

42
00:03:02,000 --> 00:03:06,000
one moment of recognizing something for what it is,

43
00:03:06,000 --> 00:03:08,000
and fixing your mind on it as it is.

44
00:03:08,000 --> 00:03:13,000
You're changing something about yourself and about your future

45
00:03:13,000 --> 00:03:15,000
in a positive way.

46
00:03:15,000 --> 00:03:18,000
So if you do that lots of times during the day,

47
00:03:18,000 --> 00:03:21,000
even an hour's worth,

48
00:03:21,000 --> 00:03:23,000
of course you can't do a full hours worth,

49
00:03:23,000 --> 00:03:27,000
but if within an hour you're able to do that many times,

50
00:03:27,000 --> 00:03:32,000
then that's many instances of wholesome karma.

51
00:03:32,000 --> 00:03:35,000
It's purified some aspect of your existence.

52
00:03:35,000 --> 00:03:40,000
So the question, as Bill said,

53
00:03:40,000 --> 00:03:45,000
some people can become enlightened just listening to a teaching.

54
00:03:45,000 --> 00:03:50,000
So as I replied, it's not very much depends on who you are.

55
00:03:50,000 --> 00:03:54,000
There's no guarantee that if you do intensive meditation

56
00:03:54,000 --> 00:03:58,000
for a year or two years that you'll become enlightened.

57
00:03:58,000 --> 00:04:00,000
It's much more likely,

58
00:04:00,000 --> 00:04:03,000
but it's not a guarantee.

59
00:04:03,000 --> 00:04:06,000
It totally depends on who you are.

60
00:04:06,000 --> 00:04:09,000
Of course who your teacher is

61
00:04:09,000 --> 00:04:12,000
and what center you're staying at and all of these things.

62
00:04:12,000 --> 00:04:14,000
What your past karma is,

63
00:04:14,000 --> 00:04:17,000
sometimes your past karma is such that it gets in your way.

64
00:04:17,000 --> 00:04:22,000
If you get sick or somehow you're not able to practice,

65
00:04:22,000 --> 00:04:26,000
some people even go crazy because of intense bad past karma,

66
00:04:26,000 --> 00:04:28,000
it can happen.

67
00:04:28,000 --> 00:04:30,000
So there's many aspects.

68
00:04:30,000 --> 00:04:36,000
You should never be set on a time limit

69
00:04:36,000 --> 00:04:40,000
that I want to become enlightened in the next number of days.

70
00:04:40,000 --> 00:04:43,000
We should try to go as quickly as we can

71
00:04:43,000 --> 00:04:46,000
or to the best of our ability to train ourselves,

72
00:04:46,000 --> 00:04:50,000
but you have to be realistic that you might take lifetime

73
00:04:50,000 --> 00:04:52,000
to become enlightened.

74
00:04:52,000 --> 00:04:56,000
Some people, even in the Buddhist time,

75
00:04:56,000 --> 00:04:58,000
didn't become enlightened,

76
00:04:58,000 --> 00:05:00,000
made a determination to become enlightened

77
00:05:00,000 --> 00:05:04,000
in the time of Mehtayabuddha.

78
00:05:04,000 --> 00:05:07,000
And so they are angels up in heaven right now

79
00:05:07,000 --> 00:05:09,000
or wherever they are.

80
00:05:09,000 --> 00:05:12,000
And because of their determination and their intentions,

81
00:05:12,000 --> 00:05:15,000
they're going to be born sometime in the far, far future

82
00:05:15,000 --> 00:05:18,000
as disciples of Mehtayabuddha, supposedly,

83
00:05:18,000 --> 00:05:22,000
or possibly.

84
00:05:22,000 --> 00:05:27,000
So moments of mindfulness are in and of themselves beneficial,

85
00:05:27,000 --> 00:05:31,000
let alone a daily practice of one hour or two hours a day,

86
00:05:31,000 --> 00:05:34,000
which is a great thing.

87
00:05:34,000 --> 00:05:39,000
If, as Paulina said, you're actually mindful every moment

88
00:05:39,000 --> 00:05:43,000
of that or even if you're mindful for many moments,

89
00:05:43,000 --> 00:05:45,000
it's a great thing.

90
00:05:45,000 --> 00:05:49,000
But I'll tell you what people say when they come here,

91
00:05:49,000 --> 00:05:52,000
especially now that I'm teaching on the internet,

92
00:05:52,000 --> 00:05:56,000
that they are shocked at the difference

93
00:05:56,000 --> 00:06:02,000
between doing one hour, even one hour a day,

94
00:06:02,000 --> 00:06:10,000
and coming here and dedicating their waking hours to it.

95
00:06:10,000 --> 00:06:12,000
That in and of itself has been an obstacle

96
00:06:12,000 --> 00:06:14,000
for people coming here now,

97
00:06:14,000 --> 00:06:17,000
because of their expectations of how easy it is.

98
00:06:17,000 --> 00:06:20,000
Or, of course, it's difficult to do an hour a day,

99
00:06:20,000 --> 00:06:23,000
but they say, okay, well, that kind of difficulty I can handle.

100
00:06:23,000 --> 00:06:25,000
So they now come here expecting it

101
00:06:25,000 --> 00:06:28,000
to be that difficult or a little more difficult.

102
00:06:28,000 --> 00:06:31,000
It's an order of magnitude more difficult.

103
00:06:31,000 --> 00:06:34,000
And I would say it is an order of magnitude more beneficial.

104
00:06:34,000 --> 00:06:39,000
You can't compare one hour or two hours a day of meditation

105
00:06:39,000 --> 00:06:45,000
to eight, ten, twelve hours,

106
00:06:45,000 --> 00:06:48,000
but moreover, just an utter dedication

107
00:06:48,000 --> 00:06:51,000
to the meditation practice with no interruptions.

108
00:06:51,000 --> 00:06:54,000
And the knowledge that when you wake up in the morning,

109
00:06:54,000 --> 00:06:59,000
that's what you're going to start doing until you fall asleep at night.

110
00:06:59,000 --> 00:07:04,000
That's where very quick changes occur.

111
00:07:04,000 --> 00:07:07,000
So the question of whether after a year or two,

112
00:07:07,000 --> 00:07:11,000
one can expect progress in insights.

113
00:07:11,000 --> 00:07:14,000
I guess the question one might be asking is,

114
00:07:14,000 --> 00:07:16,000
or is it futile?

115
00:07:16,000 --> 00:07:19,000
And I would say no, of course, it's not futile.

116
00:07:19,000 --> 00:07:22,000
If we don't do good deeds,

117
00:07:22,000 --> 00:07:25,000
the alternative is only to do bad deeds.

118
00:07:25,000 --> 00:07:30,000
And that leads us definitely to a bad state.

119
00:07:30,000 --> 00:07:35,000
If we inject into our lives of good and badness,

120
00:07:35,000 --> 00:07:38,000
if we inject more goodness and purity,

121
00:07:38,000 --> 00:07:41,000
it can't help but lead us in a good way.

122
00:07:41,000 --> 00:07:43,000
So it will lead us closer to Buddhism,

123
00:07:43,000 --> 00:07:45,000
closer to meditation.

124
00:07:45,000 --> 00:07:46,000
It will make it easier to meditate.

125
00:07:46,000 --> 00:07:48,000
I mean, without question,

126
00:07:48,000 --> 00:07:51,000
people who have started practicing in this tradition

127
00:07:51,000 --> 00:07:54,000
based on the teachings and the internet I put up.

128
00:07:54,000 --> 00:07:55,000
Without question,

129
00:07:55,000 --> 00:07:57,000
it is actually easier for them when they arrive.

130
00:07:57,000 --> 00:08:00,000
They can already do half hour walking, half hour sitting,

131
00:08:00,000 --> 00:08:02,000
which in my day,

132
00:08:02,000 --> 00:08:05,000
we started at 10 minutes walking, 10 minutes sitting.

133
00:08:05,000 --> 00:08:09,000
So to have already started,

134
00:08:09,000 --> 00:08:11,000
it is a great thing.

135
00:08:11,000 --> 00:08:15,000
It leads, it can only lead you closer to enlightenment.

136
00:08:15,000 --> 00:08:20,000
If the question is whether you can become a Sotupana in that way,

137
00:08:20,000 --> 00:08:24,000
I would say it's quite difficult in a couple of years.

138
00:08:24,000 --> 00:08:28,000
Now, it might happen in a couple of lifetimes,

139
00:08:28,000 --> 00:08:32,000
but if you're looking for something quicker than a couple of lifetimes,

140
00:08:32,000 --> 00:08:37,000
you really are advised to, at some point,

141
00:08:37,000 --> 00:08:39,000
take up intensive practice.

142
00:08:39,000 --> 00:08:43,000
So an example we have is of people doing one or two hours a day,

143
00:08:43,000 --> 00:08:46,000
two hours, I think, minimum a day,

144
00:08:46,000 --> 00:08:52,000
and then spending a weekend of intensive practice,

145
00:08:52,000 --> 00:08:54,000
where they put it all together.

146
00:08:54,000 --> 00:08:58,000
And, you know, I've never done this,

147
00:08:58,000 --> 00:09:02,000
but I've heard of good results.

148
00:09:02,000 --> 00:09:06,000
From people who have led their students through it.

149
00:09:06,000 --> 00:09:08,000
Now, you know, they say it's good results.

150
00:09:08,000 --> 00:09:10,000
You have to trust them and how do they really know

151
00:09:10,000 --> 00:09:13,000
because of course the damage to be realized for oneself.

152
00:09:13,000 --> 00:09:17,000
But I think the best we can say

153
00:09:17,000 --> 00:09:20,000
with certainty is that it's certainly beneficial,

154
00:09:20,000 --> 00:09:23,000
and it's absolutely worth doing,

155
00:09:23,000 --> 00:09:25,000
and it will change you.

156
00:09:25,000 --> 00:09:27,000
It will bring you closer to the demo.

157
00:09:27,000 --> 00:09:29,000
It will change many things about your life,

158
00:09:29,000 --> 00:09:33,000
and it will help you prepare for eventual intensive practice,

159
00:09:33,000 --> 00:09:34,000
if you do decide on it.

160
00:09:34,000 --> 00:09:37,000
And it may even lead you to profound insights,

161
00:09:37,000 --> 00:09:39,000
and even enlightenment,

162
00:09:39,000 --> 00:09:42,000
depending on your qualities and perfection

163
00:09:42,000 --> 00:09:45,000
and your dedication to it.

164
00:09:45,000 --> 00:09:47,000
It depends how you're living your life, as Bill said.

165
00:09:47,000 --> 00:09:49,000
No, if you're mindful throughout the day,

166
00:09:49,000 --> 00:09:52,000
it's better than some people who come to meditation centers

167
00:09:52,000 --> 00:10:13,000
and just sit around and fall asleep on their sitting med.

