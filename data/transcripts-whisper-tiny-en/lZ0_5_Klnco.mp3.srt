1
00:00:00,000 --> 00:00:03,800
Hello and welcome back to our study of the Damapada.

2
00:00:03,800 --> 00:00:10,800
Tonight we continue on with verses 87 to 89, which read as follows.

3
00:01:03,800 --> 00:01:13,800
Giving up or abandoning black damas, whip behind it.

4
00:01:13,800 --> 00:01:27,800
Sukang, parvi, tapandito, a wise one cultivates white damas, cultivates the white.

5
00:01:27,800 --> 00:01:38,800
Oka, anokamma, gama, vivaka, yatanduramal, having gone from home to homelessness.

6
00:01:38,800 --> 00:01:56,800
One dwells in a solitude that is hard to enjoy, difficult to enjoy.

7
00:01:56,800 --> 00:02:14,800
And then the next verse, the draviratimitya, one should wish for such wish for great enjoyment or appreciation, such happiness.

8
00:02:14,800 --> 00:02:28,800
Having hitwakami, a kinjano, having destroyed or cut off sensuality, a kinjanoans, has one who has nothing.

9
00:02:28,800 --> 00:02:36,800
But he owed up a yatana, one should cleanse or purify one's self.

10
00:02:36,800 --> 00:02:47,800
Jitakle say he earned it all. There's a wise one, one should cleanse, the mind, one's own mind of all the famines.

11
00:02:47,800 --> 00:03:11,800
Gisang, sambod, yang, gisu, samaj, yitang, subabhaitang, one who has well cultivated one's own mind rightly, in regards to the sambod, yang, the anger, the factors relating to enlightenment.

12
00:03:11,800 --> 00:03:27,800
Dhanapatinisage, having abandoned clinging, Anupadaya, yerata, who dwells happy without clinging.

13
00:03:27,800 --> 00:03:38,800
Kina Sava, who has destroyed the taines or the stains of one's mind, 2-Dmanto, one who is brilliant.

14
00:03:38,800 --> 00:03:51,800
Dayloghe, Barini Buddha, such a one becomes completely calmed or tranquilized or extinguished in the world.

15
00:03:51,800 --> 00:03:59,800
So quite a bit, this is again, as I said, a part of this famous set of verses, well known in Thailand anyway.

16
00:03:59,800 --> 00:04:07,800
It's something that we chant quite often. I haven't chanted it in a while, but still quite familiar.

17
00:04:07,800 --> 00:04:14,800
And as it happens, there is no story to go with these verses.

18
00:04:14,800 --> 00:04:37,800
There is only the story that we have is 50 monks who came to see the Buddha from Kosala.

19
00:04:37,800 --> 00:04:44,800
And the kingdom of Kosala, 50 monks came to see the Buddha.

20
00:04:44,800 --> 00:04:51,800
And when they got there, the Buddha asked them and had them relate to him, the things that went on or their goings on.

21
00:04:51,800 --> 00:04:54,800
And then that was it. He taught them this verse.

22
00:04:54,800 --> 00:04:59,800
So this is just simply an example of teaching the dhamma.

23
00:04:59,800 --> 00:05:02,800
So now my job is to explain this dhamma.

24
00:05:02,800 --> 00:05:09,800
That's really the key here, because this is one verse that has quite a bit of pith to it.

25
00:05:09,800 --> 00:05:11,800
Quite a bit of meaning to it. There's quite a bit in here.

26
00:05:11,800 --> 00:05:17,800
So let's try and break it into pieces and relate it to our practice.

27
00:05:17,800 --> 00:05:20,800
Dhamma, the black dhammas.

28
00:05:20,800 --> 00:05:22,800
So we have this black and white.

29
00:05:22,800 --> 00:05:26,800
This is common, I guess you could say, imagery that the Buddha used.

30
00:05:26,800 --> 00:05:31,800
Black refers to the bad and white refers to the good.

31
00:05:31,800 --> 00:05:38,800
I have a friend who many years ago asked me about this.

32
00:05:38,800 --> 00:05:41,800
He said, so did the Buddha have a problem with black.

33
00:05:41,800 --> 00:05:47,800
I said, this friend of mine was dark skinned, African heritage.

34
00:05:47,800 --> 00:05:51,800
And so he asked me, did the Buddha have a problem with black?

35
00:05:51,800 --> 00:05:56,800
With the color black? I mean, why is, why do meditators wear white?

36
00:05:56,800 --> 00:06:00,800
And the idea is that white is about purity.

37
00:06:00,800 --> 00:06:02,800
That's something to do with purity.

38
00:06:02,800 --> 00:06:08,800
But the squared at charge statement and a charged, called tradition.

39
00:06:08,800 --> 00:06:16,800
Because in India, the idea of light skin was associated with higher cast.

40
00:06:16,800 --> 00:06:23,800
It's actually unclear whether that was truly the case, but there is some sense of that.

41
00:06:23,800 --> 00:06:34,800
Especially, I think, after the British were colonized and controlled the country for so long.

42
00:06:34,800 --> 00:06:41,800
It seems they may have promoted the idea.

43
00:06:41,800 --> 00:06:47,800
But, you know, I don't think, I mean, my response to him was that, well,

44
00:06:47,800 --> 00:06:56,800
I mean, it's nothing to do with skin color because white skin would look, no one has white skin and no one has black skin.

45
00:06:56,800 --> 00:06:58,800
There's no such thing.

46
00:06:58,800 --> 00:07:08,800
We all have various shades of, you know, from pink to tan, I guess.

47
00:07:08,800 --> 00:07:17,800
And this is just, it's more to do with light and dark lightness and darkness.

48
00:07:17,800 --> 00:07:19,800
And anyway, it's just a word.

49
00:07:19,800 --> 00:07:26,800
It's not that the Buddha liked the color white more than the color black, but there is suitable imagery that corresponds with it.

50
00:07:26,800 --> 00:07:31,800
Because white is associated with light, which you can use to see, right?

51
00:07:31,800 --> 00:07:41,800
Black is associated with darkness, which implies the inability to see and corresponding problems and difficulties.

52
00:07:41,800 --> 00:07:43,800
But here it refers to evil and good.

53
00:07:43,800 --> 00:07:50,800
So, one should give up, evil and cultivate good.

54
00:07:50,800 --> 00:07:54,800
Destroy evil in one's heart and cultivate the good.

55
00:07:54,800 --> 00:08:02,800
So, we're talking about unwholesome bodily acts and acts of speech.

56
00:08:02,800 --> 00:08:05,800
And unwholesome states of mind.

57
00:08:05,800 --> 00:08:09,800
And then we're talking about their opposites when we refer to the white.

58
00:08:09,800 --> 00:08:15,800
So, wholesome needs of action and speech and wholesome thoughts.

59
00:08:15,800 --> 00:08:19,800
So, the first two are in regards to morality.

60
00:08:19,800 --> 00:08:24,800
The third one is in regards to concentration and wisdom.

61
00:08:24,800 --> 00:08:26,800
But this is how it starts.

62
00:08:26,800 --> 00:08:37,800
You know, we start by somehow monitoring our body and our speech and watching our movements.

63
00:08:37,800 --> 00:08:40,800
This is how meditation starts, right?

64
00:08:40,800 --> 00:08:48,800
We start by looking at the body and it may not be clear, but simply watching the stomach rising and falling is an example of morality.

65
00:08:48,800 --> 00:08:50,800
It doesn't seem like it's an ethical thing.

66
00:08:50,800 --> 00:08:53,800
It's an ethically charged thing to do, but it actually is.

67
00:08:53,800 --> 00:08:56,800
It's an ethically sound action, which sounds kind of funny.

68
00:08:56,800 --> 00:09:03,800
But it's an action, watching your stomach rise and fall is an action that is free from any unethical.

69
00:09:03,800 --> 00:09:06,800
Or violations of ethics from a Buddhist point of view.

70
00:09:06,800 --> 00:09:12,800
When you walk, stepping right, stepping left, you have ethics and you're monitoring your actions.

71
00:09:12,800 --> 00:09:17,800
And by doing that, I mean, the extension is that when you're doing sitting or walking meditation this way,

72
00:09:17,800 --> 00:09:27,800
you're less likely to commit an infraction or you're less likely to commit an act or a speech that is unwholesome.

73
00:09:27,800 --> 00:09:35,800
Not simply because it's a ritualistic movement that's more to do with protecting you while you're training.

74
00:09:35,800 --> 00:09:45,800
But also because you're mindful and the point is you're cultivating mindfulness so that then when you take that out into the ordinary world,

75
00:09:45,800 --> 00:09:55,800
you'll be able to apply it so that when a mosquito lands on your arm, you want to immediately react by killing, by attacking.

76
00:09:55,800 --> 00:10:01,800
So, I mean, from the very get-go-that's what meditation is all about.

77
00:10:01,800 --> 00:10:10,800
And then later on, it begins to affect your mind, begins to change so that you don't even want to kill, want to hurt,

78
00:10:10,800 --> 00:10:17,800
want to do these such things. You're more inclined to do good things to help people,

79
00:10:17,800 --> 00:10:25,800
because you see how it brings peace and happiness and how it's just a more efficient and productive thing to do.

80
00:10:25,800 --> 00:10:28,800
Do good deeds.

81
00:10:28,800 --> 00:10:43,800
So, going from home to homelessness, this is a big part of the buddhist, monastic path. Now, it's not for everyone, but here he is talking the monks,

82
00:10:43,800 --> 00:10:53,800
and it is a great, considered a great thing to do. You could also look at this sort of symbolically,

83
00:10:53,800 --> 00:10:59,800
because it refers to all guys an interesting word.

84
00:10:59,800 --> 00:11:08,800
You could think of it simply as going from owning things to not owning things in the sense of not letting things own you,

85
00:11:08,800 --> 00:11:17,800
not identifying with your possessions, because obviously monks would live in huts and they live in nowadays living,

86
00:11:17,800 --> 00:11:22,800
even houses or some monks live in apartment buildings and that kind of thing.

87
00:11:22,800 --> 00:11:29,800
So, it's not the case that they don't have a dwelling, it's that they don't have anything that they call their own,

88
00:11:29,800 --> 00:11:35,800
and lay people can do this as well, not in the sense of legally giving up the right to things,

89
00:11:35,800 --> 00:11:47,800
but mentally and in terms of the ego giving up on detachment of things and using things, not having things own you in that sense.

90
00:11:47,800 --> 00:12:10,800
And as a result of this, one attains a solitude, one's mind isn't constantly flitting off to one's belongings.

91
00:12:10,800 --> 00:12:14,800
It's hard to find such solitude, the Buddha said hard to enjoy.

92
00:12:14,800 --> 00:12:19,800
It talks about uses this word often, because most of us would like possessions, right?

93
00:12:19,800 --> 00:12:31,800
And are obsessed with our possessions and getting more possessions and protecting and maintaining and even just looking at and touching and using our possessions.

94
00:12:31,800 --> 00:12:41,800
It's hard to enjoy the solitude of giving things up, akin to know which we see in verse number 88, being with nothing, akin to know means someone who has nothing.

95
00:12:41,800 --> 00:12:48,800
It means no, it could be in regards to physical possessions, it could be in regards to mental attachments.

96
00:12:48,800 --> 00:13:01,800
So, it says tatrabi, rati michiya, one should strive for, one should wish for.

97
00:13:01,800 --> 00:13:14,800
And it's worth worth wanting a sense, such, a sense of enjoyment, enjoying being at peace.

98
00:13:14,800 --> 00:13:24,800
Hitwakami, having destroyed karma, having destroyed sensual pleasure, sensual desire,

99
00:13:24,800 --> 00:13:30,800
the attachment, the sensual pleasure, the objects of sensual pleasure, having given them up.

100
00:13:30,800 --> 00:13:36,800
And giving and purifying one's mind of defoundance.

101
00:13:36,800 --> 00:13:38,800
This is what a wise one does.

102
00:13:38,800 --> 00:13:42,800
You notice the pattern of the talk about a wise one, right?

103
00:13:42,800 --> 00:13:47,800
This whole chapter is the Pandita Bhagavic, with these three verses we've now finished.

104
00:13:47,800 --> 00:13:57,800
But this is how the Dhamapadeh set up each chapter is associated by the mention of a certain specific word.

105
00:13:57,800 --> 00:14:07,800
So, there's, I think, 22 chapters, 20 some, more than 22, I think, 20 some chapters, and 423 verses.

106
00:14:07,800 --> 00:14:14,800
And we've just finished the 6th wag of the 6th chapter, which is the Pandita chapter.

107
00:14:14,800 --> 00:14:23,800
Anyway, so, what clear in our minds in this tradition, in the Buddha's tradition,

108
00:14:23,800 --> 00:14:30,800
that clinging to things is a problem, and that if we can be free from such clinging,

109
00:14:30,800 --> 00:14:37,800
if we can come to just live without this clinging, then we would be free from suffering,

110
00:14:37,800 --> 00:14:39,800
that this would be a better thing.

111
00:14:39,800 --> 00:14:43,800
And so, the Buddha in 88 gives a description,

112
00:14:43,800 --> 00:14:46,800
it's just a very brief description of how we go about this,

113
00:14:46,800 --> 00:14:50,800
and this is by cleansing our minds of the attachment.

114
00:14:50,800 --> 00:14:55,800
So, the answer is not to always get what you want, it's to stop wanting.

115
00:14:55,800 --> 00:14:58,800
To stop wanting for things.

116
00:14:58,800 --> 00:15:00,800
Clear your mind of defilement,

117
00:15:00,800 --> 00:15:05,800
clear your mind of the things that make you want this, and want that,

118
00:15:05,800 --> 00:15:09,800
or make you hate this, or hate that.

119
00:15:09,800 --> 00:15:14,800
And this is what, this is our practice in meditation.

120
00:15:14,800 --> 00:15:16,800
So, we're constantly working on this.

121
00:15:16,800 --> 00:15:19,800
We're not concerned about what we gather, don't get.

122
00:15:19,800 --> 00:15:25,800
And we look at our, we watch our minds, observe our minds, monitor our minds,

123
00:15:25,800 --> 00:15:32,800
as we get, and as we lose, and as change comes to us again and again,

124
00:15:32,800 --> 00:15:41,800
we work out these mental states, slowly changing and purifying our minds.

125
00:15:41,800 --> 00:15:43,800
This is the practice.

126
00:15:43,800 --> 00:15:49,800
This is the Buddha's exhortation, to these 50 monks,

127
00:15:49,800 --> 00:15:54,800
and then finally 89 gives more details we have in regards to the anger of the sambodi,

128
00:15:54,800 --> 00:15:58,800
the factors of enlightenment.

129
00:15:58,800 --> 00:16:03,800
This could be the bhodjanga, or it could also be the bhodipakya dama,

130
00:16:03,800 --> 00:16:06,800
probably I think the commentary says it's the seven bhodjangas.

131
00:16:06,800 --> 00:16:08,800
So, it means cultivating the bhodjangas,

132
00:16:08,800 --> 00:16:14,800
which if you don't know what those are, we have sati starts with, of course sati,

133
00:16:14,800 --> 00:16:19,800
which is the ability to remember, or remind yourself,

134
00:16:19,800 --> 00:16:24,800
the remembrance of things as they are, seeing things as they are,

135
00:16:24,800 --> 00:16:31,800
not how you, as you wish them to be, or how you like them to be,

136
00:16:31,800 --> 00:16:35,800
not based on liking or disliking, not judging them,

137
00:16:35,800 --> 00:16:38,800
or extrapolating them upon them.

138
00:16:38,800 --> 00:16:41,800
And then with sati you have dama vijaya,

139
00:16:41,800 --> 00:16:44,800
which means this investigation that goes on,

140
00:16:44,800 --> 00:16:49,800
or not the investigation, but more like starting to categorize,

141
00:16:49,800 --> 00:16:58,800
and separate, and to figure out, to understand damas,

142
00:16:58,800 --> 00:17:00,800
let's put it that way, to understand reality.

143
00:17:00,800 --> 00:17:03,800
So, as we meditate and we cultivate sati,

144
00:17:03,800 --> 00:17:06,800
we start to see the difference between right and wrong,

145
00:17:06,800 --> 00:17:08,800
we start to see what we're doing wrong,

146
00:17:08,800 --> 00:17:11,800
we start to change our habits naturally,

147
00:17:11,800 --> 00:17:15,800
our minds start to change as we see how we're causing ourselves suffering.

148
00:17:15,800 --> 00:17:18,800
It's like that's not useful if I continue that,

149
00:17:18,800 --> 00:17:22,800
it's going to cause me suffering, better that I change that,

150
00:17:22,800 --> 00:17:24,800
and it does not even intellectual,

151
00:17:24,800 --> 00:17:26,800
it's just naturally through the observation,

152
00:17:26,800 --> 00:17:29,800
through monitoring and seeing what we're doing right and wrong,

153
00:17:29,800 --> 00:17:34,800
we're able to change, so this is dama vijaya.

154
00:17:34,800 --> 00:17:38,800
And then there's the vijaya, which is effort.

155
00:17:38,800 --> 00:17:40,800
This helps us put our effort,

156
00:17:40,800 --> 00:17:42,800
and we also have to put our effort.

157
00:17:42,800 --> 00:17:44,800
Putting our effort is important.

158
00:17:44,800 --> 00:17:46,800
We have to work at it.

159
00:17:46,800 --> 00:17:51,800
We have to constantly bring your mind back to the present moment,

160
00:17:51,800 --> 00:17:54,800
bring your mind back to reality again and again and again.

161
00:17:54,800 --> 00:17:56,800
It's something that has to become habitual.

162
00:17:56,800 --> 00:17:58,800
You have to cultivate the habit, and you have to work at it.

163
00:17:58,800 --> 00:18:00,800
So that's vijaya.

164
00:18:00,800 --> 00:18:02,800
Then viti.

165
00:18:02,800 --> 00:18:05,800
viti means you have to be,

166
00:18:05,800 --> 00:18:10,800
you have to have some kind of stimulation in the sense,

167
00:18:10,800 --> 00:18:15,800
or you have to be into it.

168
00:18:15,800 --> 00:18:17,800
You have to get into a groove,

169
00:18:17,800 --> 00:18:20,800
and you have to work at getting into a routine and a groove,

170
00:18:20,800 --> 00:18:25,800
and getting caught up so that it just becomes natural.

171
00:18:25,800 --> 00:18:29,800
It's viti, and vasati, it's tranquility.

172
00:18:29,800 --> 00:18:31,800
Part of the practice is to calm the mind.

173
00:18:31,800 --> 00:18:37,800
The mind becomes calmer as you sort things out in your mind.

174
00:18:37,800 --> 00:18:39,800
And then samadhi is concentrated.

175
00:18:39,800 --> 00:18:45,800
The mind becomes focused and concentrated seeing things clearly not,

176
00:18:45,800 --> 00:18:47,800
not superficially.

177
00:18:47,800 --> 00:18:51,800
They're not being distracted by everything that comes along.

178
00:18:51,800 --> 00:18:57,800
And finally, upeka, the mind eventually gets to a state where it

179
00:18:57,800 --> 00:18:59,800
sees everything just as it is.

180
00:18:59,800 --> 00:19:04,800
The judgment's fade away, and the mind is simply noting

181
00:19:04,800 --> 00:19:07,800
experience after experience after experience.

182
00:19:07,800 --> 00:19:10,800
So all seven of these are the seven boat jungers

183
00:19:10,800 --> 00:19:12,800
that the Buddha is talking about,

184
00:19:12,800 --> 00:19:16,800
and it's a really good, simple explanation of the various things

185
00:19:16,800 --> 00:19:19,800
that we have to cultivate in our practice.

186
00:19:19,800 --> 00:19:23,800
And this is called samadhi-dang-suba,

187
00:19:23,800 --> 00:19:26,800
the right cultivation,

188
00:19:26,800 --> 00:19:29,800
good, well-cultivated mind,

189
00:19:29,800 --> 00:19:33,800
well and rightly cultivated mind.

190
00:19:33,800 --> 00:19:35,800
More or less.

191
00:19:35,800 --> 00:19:38,800
Adana-patimi-seghi,

192
00:19:38,800 --> 00:19:40,800
one should give up clinging.

193
00:19:40,800 --> 00:19:42,800
So it all comes back to our clinging.

194
00:19:42,800 --> 00:19:44,800
What do we mean by judgment?

195
00:19:44,800 --> 00:19:51,800
Our inability to adapt and to keep up with change,

196
00:19:51,800 --> 00:19:52,800
basically.

197
00:19:52,800 --> 00:19:55,800
Things change, something comes or something goes,

198
00:19:55,800 --> 00:20:01,800
and we cling to what it was or what it is.

199
00:20:01,800 --> 00:20:05,800
So it is right now and that rather than roll with it

200
00:20:05,800 --> 00:20:08,800
and change, we want for it to change,

201
00:20:08,800 --> 00:20:12,800
either to get closer to it or to get farther away from it.

202
00:20:12,800 --> 00:20:16,800
We wanted to come and we wanted to stay or we wanted to go.

203
00:20:16,800 --> 00:20:18,800
And that's called clinging.

204
00:20:18,800 --> 00:20:21,800
And that's what we're giving up.

205
00:20:21,800 --> 00:20:27,800
So the Buddha says we give this up and dwell happily without it.

206
00:20:27,800 --> 00:20:30,800
Kina-suba, having destroyed the asava,

207
00:20:30,800 --> 00:20:36,800
the various taines which relates back to the defilements

208
00:20:36,800 --> 00:20:38,800
that we already talked to.

209
00:20:38,800 --> 00:20:40,800
So it's talked about,

210
00:20:40,800 --> 00:20:45,800
so it's desire for sensuality and desire for becoming

211
00:20:45,800 --> 00:20:48,800
and wanting to be this, wanting to be that.

212
00:20:48,800 --> 00:20:53,800
Thoughts that arise, hey, be great if I was boss or something like that.

213
00:20:53,800 --> 00:20:57,800
President or whatever.

214
00:20:57,800 --> 00:21:00,800
And then views of self and so on.

215
00:21:00,800 --> 00:21:06,800
All of these things that cloud our mind or cloud our judgment.

216
00:21:06,800 --> 00:21:10,800
Judimando brilliant because one is full of wisdom.

217
00:21:10,800 --> 00:21:13,800
That's when practices one comes to understand oneself

218
00:21:13,800 --> 00:21:16,800
and by extension really the whole universe.

219
00:21:16,800 --> 00:21:20,800
And that's a sort of a brilliance.

220
00:21:20,800 --> 00:21:22,800
Such a person becomes free.

221
00:21:22,800 --> 00:21:25,800
So as a result, it's not just a static state where,

222
00:21:25,800 --> 00:21:28,800
okay, now I understand reality, now what?

223
00:21:28,800 --> 00:21:31,800
It's not static because once you understand reality,

224
00:21:31,800 --> 00:21:33,800
you let it go.

225
00:21:33,800 --> 00:21:35,800
And that's how cessation comes about.

226
00:21:35,800 --> 00:21:39,800
It's how the experience of Nirvana or Nirvana comes about.

227
00:21:39,800 --> 00:21:41,800
And eventually that's how one frees oneself

228
00:21:41,800 --> 00:21:44,800
from the rounds of samsara being born,

229
00:21:44,800 --> 00:21:47,800
old, sick and dying.

230
00:21:47,800 --> 00:21:50,800
And that's what is meant by Parini Buddha.

231
00:21:50,800 --> 00:21:53,800
One frees oneself from the cycle

232
00:21:53,800 --> 00:21:56,800
that we find ourselves trapped in.

233
00:21:56,800 --> 00:21:58,800
So that's all.

234
00:21:58,800 --> 00:22:03,800
This is these are teachings for us to remember.

235
00:22:03,800 --> 00:22:06,800
It's like one of those quotables of the Buddha,

236
00:22:06,800 --> 00:22:07,800
something you could quote.

237
00:22:07,800 --> 00:22:09,800
Although as you can see by the translation,

238
00:22:09,800 --> 00:22:13,800
it's not easy to be literal and to quote this literally.

239
00:22:13,800 --> 00:22:16,800
But there are some good translations of the Dhammapana out there.

240
00:22:16,800 --> 00:22:20,800
So, I'm just trying to get the sense across

241
00:22:20,800 --> 00:22:22,800
and give some interesting teachings.

242
00:22:22,800 --> 00:22:25,800
So we have several in here.

243
00:22:25,800 --> 00:22:29,800
And at the heart of it are the seven Bojangas,

244
00:22:29,800 --> 00:22:33,800
they've now explained in brief.

245
00:22:33,800 --> 00:22:37,800
So that's the teaching on the Dhammapana today.

246
00:22:37,800 --> 00:22:39,800
Thank you all for tuning in.

247
00:22:39,800 --> 00:23:03,800
Wishing you all the best and keep practicing.

