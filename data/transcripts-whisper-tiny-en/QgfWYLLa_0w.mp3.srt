1
00:00:00,000 --> 00:00:03,040
Okay, so again here tonight with us is

2
00:00:05,040 --> 00:00:08,000
Soo made that the newest member of our

3
00:00:09,280 --> 00:00:14,920
Monastic community and she has just finished the foundation and

4
00:00:16,240 --> 00:00:18,240
Advanced courses

5
00:00:18,240 --> 00:00:21,840
What does that mean the foundation course is a?

6
00:00:23,640 --> 00:00:25,640
Course for someone who is

7
00:00:25,840 --> 00:00:27,840
new to this tradition

8
00:00:27,840 --> 00:00:32,480
In general, you'd say if someone who is new to the Mahasi Sayadaw tradition

9
00:00:33,240 --> 00:00:40,640
So if someone had practiced in the Mahasi Sayadaw tradition already, we'd kind of accelerate the foundation course

10
00:00:41,920 --> 00:00:47,240
But if anyone if someone hasn't practiced in in the school that

11
00:00:47,840 --> 00:00:50,320
We follow a bad Jantong

12
00:00:51,000 --> 00:00:55,120
Then we'll put them through some sort of foundation course, but we might

13
00:00:55,120 --> 00:01:02,520
Accelerate it and call it an event an intermediate course if they have practiced the same sort of tradition

14
00:01:03,200 --> 00:01:05,680
in the same sort of technique now in

15
00:01:06,120 --> 00:01:13,000
Soo made his case she's practiced in other traditions before but I think never done a formal course in this tradition and

16
00:01:13,760 --> 00:01:20,680
So she got a full almost full foundation course and in fact it was quicker than normal because she's practiced

17
00:01:21,440 --> 00:01:23,440
meditation for many years

18
00:01:23,440 --> 00:01:25,440
And

19
00:01:26,880 --> 00:01:36,560
The advanced course is basically a repeat of the foundation course, but it's only the foundation course generally takes 15 to 20 days to complete

20
00:01:37,200 --> 00:01:42,400
The advanced course only takes 10 days and you skip some in the beginning

21
00:01:43,040 --> 00:01:46,720
Stages because the meditators assume to have already

22
00:01:47,680 --> 00:01:50,640
Become accomplished in at least the the basic stages and

23
00:01:50,640 --> 00:01:57,840
And so she's done both of those and that's really the curriculum that we have and at this point the meditator will continue on with

24
00:01:58,400 --> 00:02:02,880
With advanced course after advanced course at their convenience

25
00:02:03,120 --> 00:02:08,840
So we do 10 day by 10 day courses and there are there are other things that can be done over the long term

26
00:02:08,840 --> 00:02:10,840
But in general that so it goes

27
00:02:12,520 --> 00:02:19,200
So what I want to ask tonight first maybe you can tell us a little bit about your background and what what got you interested in

28
00:02:19,200 --> 00:02:26,240
meditation and Buddhism and a little bit about just just briefly about the courses that you've gone through

29
00:02:30,960 --> 00:02:32,960
I suppose I

30
00:02:34,320 --> 00:02:39,920
picked up a Buddhist book somehow along the line around 2006 and

31
00:02:42,080 --> 00:02:45,760
sparked some interest in meditation, but then it wasn't until

32
00:02:45,760 --> 00:02:47,760
I

33
00:02:48,000 --> 00:02:55,680
Was living in Vancouver British Columbia and my housemates had all set go and go courses and they said oh

34
00:02:56,240 --> 00:02:59,840
You should go. It's wonderful or you'll learn how to deal with paint

35
00:03:02,720 --> 00:03:04,720
And

36
00:03:04,720 --> 00:03:07,840
And then one of the housemates ended up going and saying

37
00:03:07,840 --> 00:03:14,560
It was quite humbling and so I signed myself up and

38
00:03:16,400 --> 00:03:21,200
I thought I was quite happy going into the course and got there and I was very humbled

39
00:03:23,040 --> 00:03:28,000
And found my levels of contentment when I was stripped of all of my

40
00:03:30,400 --> 00:03:32,400
Artistic

41
00:03:32,400 --> 00:03:38,720
Allowances and I've been a painter so I wasn't allowed to paint. I wasn't allowed to you know

42
00:03:39,360 --> 00:03:41,360
I guess at that point I was

43
00:03:41,440 --> 00:03:45,840
drinking and smoking quite regularly so that was taken out and

44
00:03:46,960 --> 00:03:48,960
Just eating afternoon

45
00:03:52,080 --> 00:03:54,080
What else

46
00:03:54,320 --> 00:03:56,320
Sleeping when I want to

47
00:03:56,320 --> 00:04:02,960
The pain of sitting compounds everything. I guess it's about 10 hours a day with a going good tradition and

48
00:04:04,160 --> 00:04:06,160
The the techniques

49
00:04:06,560 --> 00:04:10,080
Like this one just is a emotional exfoliator

50
00:04:10,080 --> 00:04:15,680
So on top of taking every all the comforts of daily life out. It it brings

51
00:04:16,800 --> 00:04:18,800
all of the stuff that

52
00:04:20,240 --> 00:04:22,240
That I had been distracting myself with

53
00:04:22,240 --> 00:04:26,960
The distracting myself from with the central pleasures. So

54
00:04:28,240 --> 00:04:33,520
After seeing that first course I felt like I should really meditate and and stay at centers

55
00:04:34,720 --> 00:04:39,680
until I can really find a sense of contentment with

56
00:04:40,960 --> 00:04:42,720
With being stripped down of all those

57
00:04:46,000 --> 00:04:48,000
Allowances of

58
00:04:48,000 --> 00:04:52,160
Art and music and socializing

59
00:04:55,920 --> 00:04:57,920
I guess extroverting in general

60
00:04:59,120 --> 00:05:01,120
Just finding

61
00:05:02,160 --> 00:05:08,640
Contentment from within and so I just kept sitting course after course. I guess

62
00:05:09,520 --> 00:05:11,520
about

63
00:05:11,520 --> 00:05:17,760
Maybe eight or ten going good courses

64
00:05:19,360 --> 00:05:23,120
Plus some I just went out and going good

65
00:05:23,760 --> 00:05:27,520
Says you can you know once you know the time table. It's pretty simple. You just go and

66
00:05:28,480 --> 00:05:32,160
You can sit self courses. So I went out and sat some in the forest and

67
00:05:32,880 --> 00:05:37,760
One fellow meditator allowed me to sit on our property in Hawaii and

68
00:05:37,760 --> 00:05:46,960
And then I tried to sit a bit longer, which is not really okay in the going good tradition

69
00:05:47,440 --> 00:05:50,480
To sit over ten days by yourself, but I had

70
00:05:51,920 --> 00:05:55,760
My friend dropped me off somewhere where I couldn't really go back on the plan

71
00:05:56,960 --> 00:06:00,800
And it was storm season. So it was hurricane first winds and I was stuck out and

72
00:06:01,600 --> 00:06:03,600
And our cappella go

73
00:06:03,600 --> 00:06:07,600
Hi to goi. It's like

74
00:06:08,720 --> 00:06:15,920
Hundred kilometers off the northern coast of British Columbia and I tried to sit a 20-day course on my own and

75
00:06:18,720 --> 00:06:21,600
A friend had given me a forest tradition book

76
00:06:23,200 --> 00:06:28,880
Which is very inspiring and had actually inspired me to put myself in such a situation and then

77
00:06:29,920 --> 00:06:31,920
I

78
00:06:31,920 --> 00:06:33,920
Had sat ten day courses

79
00:06:34,880 --> 00:06:41,840
Completely alone in the forest before I had no problem, but for some reason that going for 20 days was

80
00:06:44,640 --> 00:06:50,080
Really really difficult. I was having a hard time getting out of my sleeping bag in the morning

81
00:06:50,080 --> 00:06:54,320
I was having you were also doing fasting and I decided to

82
00:06:55,280 --> 00:06:59,520
like just collect seaweed off the shore and dry it out and eat that and

83
00:06:59,520 --> 00:07:03,280
pop corn to clean out the system

84
00:07:05,760 --> 00:07:11,120
And eventually I started adding stuff, but the problem was is I fasted for a couple days in the middle of the course

85
00:07:12,320 --> 00:07:19,280
I'd been really taking very little food and then I fasted and I think I might have drank a bunch of saltwater like a gallon or something

86
00:07:20,160 --> 00:07:23,360
And then I ate some like

87
00:07:23,360 --> 00:07:31,360
a quinoa or lentils or something and I felt like superwoman and I thought okay, I'm going to the cave now and

88
00:07:32,640 --> 00:07:37,200
So I set out with my backpack and the sun had come out and it had been so

89
00:07:38,080 --> 00:07:43,280
Wet and no one lives on the west coast of height of white because it's it's so wet and

90
00:07:44,000 --> 00:07:49,520
the elements it's the longest stretch of untouched forest so the

91
00:07:49,520 --> 00:07:53,200
Jurassic nature of it is

92
00:07:54,160 --> 00:07:56,400
Great for meditation. I guess powerful, but

93
00:07:57,680 --> 00:07:59,680
It's relentless with it's

94
00:08:00,560 --> 00:08:04,320
Weather it's just the wind that the wind gets the rain

95
00:08:05,280 --> 00:08:09,520
Vertical and upside down. There's it was puddling it inside the doors and how

96
00:08:10,880 --> 00:08:12,880
Shaking the cabin with the winds actually

97
00:08:12,880 --> 00:08:20,560
And in the end, did you stay in the 20 days? Well, I ended up getting stuck out there for 26 because of the storms

98
00:08:21,120 --> 00:08:23,680
And I really felt like I

99
00:08:24,880 --> 00:08:27,600
I felt like I that was the longest time of my life

100
00:08:27,600 --> 00:08:35,840
I've had near-death experiences, but that was the the most drawn-out feeling like I might die out here because I ran out of firewood and

101
00:08:36,720 --> 00:08:38,720
ran out of gas in the chainsaw

102
00:08:39,680 --> 00:08:41,200
and then I was

103
00:08:41,200 --> 00:08:47,360
Feeling like I I can't live in the forest anymore because I couldn't I was bucking wood and

104
00:08:48,800 --> 00:08:50,800
I couldn't handle killing the moss

105
00:08:52,080 --> 00:08:58,640
And I'd been farming and I thought of all the weeds and all the weeds I've been killing and I felt like I couldn't do it anymore

106
00:09:01,200 --> 00:09:03,760
And then when I set out for the cave I was

107
00:09:03,760 --> 00:09:09,600
I mean I had been warned that there was landslides along the coast, but I got

108
00:09:11,280 --> 00:09:13,280
I got myself into two landslides and

109
00:09:16,000 --> 00:09:18,800
The fear I mean it just was very humbling of

110
00:09:20,320 --> 00:09:25,120
An ego, I guess my ego got crushed with nature like the entire forest

111
00:09:25,680 --> 00:09:27,680
I didn't understand exactly what a landslide was

112
00:09:28,640 --> 00:09:30,640
up close and personal and how to get through it

113
00:09:30,640 --> 00:09:32,640
but

114
00:09:34,240 --> 00:09:36,240
I ended up

115
00:09:37,040 --> 00:09:39,040
somehow thankfully

116
00:09:39,840 --> 00:09:46,080
Getting back to the cabin alive and I was reading the pickoo boaties discourses

117
00:09:48,240 --> 00:09:53,040
And I'd never actually read like a translation of

118
00:09:53,920 --> 00:09:56,640
Just, you know, a straight sort of translation of the discourses

119
00:09:56,640 --> 00:10:02,880
I had only really heard it through Brango, which is great, but it's not the same and I think it cut through a lot of layers of

120
00:10:03,280 --> 00:10:05,280
delusion and really made me

121
00:10:09,920 --> 00:10:15,120
Consider like what's the best way I can spend my time and

122
00:10:18,800 --> 00:10:22,320
I had been planning to start a meditation center up there

123
00:10:22,320 --> 00:10:24,960
I

124
00:10:24,960 --> 00:10:28,640
just didn't bring teachers there and run it off the grid and

125
00:10:30,080 --> 00:10:35,120
Yoga Center and meditation center and and this you mentioned that this was a

126
00:10:37,760 --> 00:10:41,120
Sort of a turning point where you realize that you you needed to

127
00:10:42,080 --> 00:10:44,080
To do this kind of thing under teacher

128
00:10:44,080 --> 00:10:48,160
Right in the beginning because my mind I had

129
00:10:48,160 --> 00:10:50,160
um

130
00:10:50,160 --> 00:10:55,120
I would I would read like the the sutas or the or the the

131
00:10:55,120 --> 00:10:57,760
Acharya month's biography and I'd feel so inspired

132
00:10:58,480 --> 00:11:00,480
Incredibly inspired and then I would

133
00:11:01,040 --> 00:11:03,920
Sit and my whole body just hurt and I'd never had

134
00:11:04,720 --> 00:11:10,480
Like a recent course at that point it had been about three years of sitting or maybe two years of sitting

135
00:11:10,960 --> 00:11:12,960
years of sitting

136
00:11:12,960 --> 00:11:17,520
and it had been a long time since I had just the entire course like

137
00:11:18,880 --> 00:11:24,880
Scanning the body like it just every part of it seemed to hurt and that was out of the question. It was just I was so agitated

138
00:11:26,160 --> 00:11:28,160
and I was so

139
00:11:29,120 --> 00:11:31,120
my mind was

140
00:11:32,160 --> 00:11:34,160
ruthless with trying to get away with

141
00:11:35,920 --> 00:11:39,520
Things just not you know daydreaming pretty much

142
00:11:39,520 --> 00:11:41,520
um

143
00:11:41,520 --> 00:11:43,600
And I thought oh if I have a teacher that can read my mind

144
00:11:43,600 --> 00:11:48,400
I can't get away with this and then I thought well even if I don't have a teacher that can read my mind if I'm in robes

145
00:11:49,920 --> 00:11:52,160
I don't think there's any way. I'm going to

146
00:11:53,440 --> 00:11:59,760
Really let myself mess around because that's I mean I have so much respect for

147
00:12:01,760 --> 00:12:03,760
Buddha's work and

148
00:12:04,000 --> 00:12:06,000
Like there wouldn't be

149
00:12:06,000 --> 00:12:10,720
You know such requisite available. I mean maybe there would be

150
00:12:11,760 --> 00:12:13,760
in another way but

151
00:12:13,760 --> 00:12:18,160
So that was the time when you you also thought that you you wanted to ordain

152
00:12:18,720 --> 00:12:21,360
Where did you do also reading and gen months?

153
00:12:23,680 --> 00:12:26,400
Okay, so would would let you to to

154
00:12:27,920 --> 00:12:29,920
To our center

155
00:12:30,400 --> 00:12:32,400
Paul and Janie

156
00:12:32,400 --> 00:12:37,520
Went to Aranyam Bodhi and I met Paul and Janie. This is where you in your efforts to find a place to ordain

157
00:12:38,240 --> 00:12:40,160
Yeah, I thought oh I go

158
00:12:42,320 --> 00:12:45,040
I think I actually decided when I was out there. I should go to

159
00:12:46,240 --> 00:12:52,720
Monastery in California and I'd heard of the nunnery if I'd all get some direction for Thailand there

160
00:12:53,760 --> 00:12:55,760
So your intention was to go to Thailand

161
00:12:56,320 --> 00:12:59,760
Yeah, I had I had no idea that it was at all

162
00:12:59,760 --> 00:13:05,040
Even somewhat difficult to ordain is a a female monk

163
00:13:07,280 --> 00:13:09,280
Raven a nun

164
00:13:09,280 --> 00:13:11,280
I had no idea that there's any

165
00:13:11,520 --> 00:13:13,520
Sort of controversy at all

166
00:13:14,800 --> 00:13:16,640
I went to

167
00:13:16,720 --> 00:13:17,920
Birkin

168
00:13:17,920 --> 00:13:24,880
Monastery on the way down and the abbot to see if you could already in there. No just to ask for his advice

169
00:13:24,880 --> 00:13:29,120
And you said oh, there's actually a buffet of ordination that you can choose from

170
00:13:32,080 --> 00:13:34,080
And

171
00:13:36,000 --> 00:13:41,360
He had had one lady who went I guess 10 years ago to be a mochi mochi quin

172
00:13:43,360 --> 00:13:45,360
quin I think and

173
00:13:49,840 --> 00:13:51,840
I don't know where she's at but

174
00:13:51,840 --> 00:13:54,480
If a fellow

175
00:13:54,480 --> 00:13:58,080
Meditator there said she actually met her and said she was having a hard time and

176
00:13:58,720 --> 00:14:02,800
The general feeling I got was that the mochi life was not easy and it was

177
00:14:03,680 --> 00:14:05,280
not

178
00:14:05,280 --> 00:14:10,160
People weren't exactly liking it so much and so it really kind of

179
00:14:12,960 --> 00:14:15,440
And I wasn't really feeling the mochi too much

180
00:14:16,960 --> 00:14:20,240
But you know, I was I'd rather be mochi than a layperson I think

181
00:14:20,240 --> 00:14:23,680
And then one of the Adjans that what pun on a chat

182
00:14:24,800 --> 00:14:28,240
When I told him that I was thinking of ordaining at what pau pang

183
00:14:31,600 --> 00:14:36,320
He said I don't think that's a good idea. I think you'll get disheartened

184
00:14:38,000 --> 00:14:40,880
And maybe that's because they they do a lot of work there

185
00:14:44,240 --> 00:14:47,840
And I feel extremely grateful that

186
00:14:47,840 --> 00:14:53,360
Polling yany invited me here and gave me a little bit of warning because she's actually been a mochi herself as well

187
00:14:53,360 --> 00:14:56,400
And she's been through the ropes. What do you think of Sri Lanka?

188
00:14:57,200 --> 00:14:59,200
The country so far

189
00:15:01,520 --> 00:15:03,520
I think it's uh

190
00:15:04,000 --> 00:15:07,200
I haven't seen much of it but being here

191
00:15:09,920 --> 00:15:12,720
For some for some reason I feel

192
00:15:12,720 --> 00:15:20,800
I feel at home, but I've been living on a lot of islands like this island actually kind of reminds me a bit

193
00:15:20,800 --> 00:15:22,800
A Buddhist version of Puerto Rico

194
00:15:23,440 --> 00:15:25,440
I was living on Puerto Rico

195
00:15:25,440 --> 00:15:28,480
I know I know the feeling of the island. I was born in the island

196
00:15:29,200 --> 00:15:32,880
And and it's amazing to have the same feelings come back here

197
00:15:33,280 --> 00:15:36,480
Hey, you realize that there is something to be on an island

198
00:15:37,520 --> 00:15:41,520
Yeah, it makes a bit of solidarity or homogeneity

199
00:15:41,520 --> 00:15:43,520
This is also true

200
00:15:43,520 --> 00:15:49,680
Okay, but let's let's let's skip ahead a little bit to what I'm really interested in is

201
00:15:51,120 --> 00:15:56,880
Something quite specific and and I want to pull it out of context so

202
00:15:58,800 --> 00:16:06,640
What the question is a very simple question. What did what do you think or how do you feel you have benefited from this course and

203
00:16:06,640 --> 00:16:13,200
What I'd like what I mean by pulling it out of context is I'd like you to just talk about this course and not compare it to your other

204
00:16:13,200 --> 00:16:16,800
Courses in speech and not compared to your other courses at all

205
00:16:17,840 --> 00:16:21,600
If the benefits you gain some of the benefits you gain were the same as other courses

206
00:16:22,240 --> 00:16:27,440
Just just talk about them as the benefits of this course or you know if you think there are benefits which

207
00:16:27,920 --> 00:16:31,600
Assuming you think there are some benefits otherwise you wouldn't still be here

208
00:16:31,600 --> 00:16:37,200
So how you're feeling before you you went into this course

209
00:16:37,920 --> 00:16:39,920
How that is different from your feeling afterwards?

210
00:16:39,920 --> 00:16:41,920
So it should be specifically about this course

211
00:16:42,400 --> 00:16:45,680
But it could be the same sorts of things you've gained another course which is fine

212
00:16:45,680 --> 00:16:48,240
But only only referring to this course

213
00:16:49,520 --> 00:16:52,720
Well, I mean I would say that this course did have its special

214
00:16:54,800 --> 00:16:56,800
kind of advantage

215
00:16:58,080 --> 00:17:00,080
that I could

216
00:17:00,080 --> 00:17:03,120
It's very hard to talk about consciousness. So

217
00:17:06,080 --> 00:17:07,600
I guess

218
00:17:07,600 --> 00:17:10,160
Going into the course. I was quite confident

219
00:17:12,720 --> 00:17:14,720
Having that other courses

220
00:17:16,320 --> 00:17:18,320
and

221
00:17:18,800 --> 00:17:20,320
That

222
00:17:20,320 --> 00:17:22,560
Got a bit shattered just with the

223
00:17:22,560 --> 00:17:29,600
Annoyingness of what I'm getting myself into which kind of started having on day one

224
00:17:34,160 --> 00:17:36,160
It's like I get

225
00:17:38,400 --> 00:17:41,840
I put in a room with a television of my mind and

226
00:17:44,320 --> 00:17:47,600
And it kind of comes back to like contentment levels and

227
00:17:47,600 --> 00:17:52,640
Being told to sit there and just

228
00:17:55,680 --> 00:17:59,440
I guess flip that that I mean that I guess

229
00:18:01,120 --> 00:18:03,120
Let's see

230
00:18:03,120 --> 00:18:06,560
the the the meditation technique here

231
00:18:08,720 --> 00:18:10,720
of

232
00:18:10,720 --> 00:18:18,880
The same thinking thinking thinking when you're thinking was a real

233
00:18:25,440 --> 00:18:28,640
And knife for the ego's head almost it just really

234
00:18:29,680 --> 00:18:31,680
As if it it

235
00:18:34,160 --> 00:18:38,320
That sounds like a great way to put it. I'm inspired. Okay, so

236
00:18:38,320 --> 00:18:43,520
They've to the ego. Well, okay, so I can sit there and

237
00:18:45,840 --> 00:18:48,960
I feel I guess confident the sense that like

238
00:18:50,400 --> 00:18:53,520
I could easily sit with my mind and watch it, I guess

239
00:18:56,000 --> 00:19:00,240
But like a you know TV, but it does get annoying after a few days of

240
00:19:00,240 --> 00:19:07,040
You know not sitting on a couch and watching it or with all your food and drinks and every popcorn

241
00:19:09,600 --> 00:19:14,800
And some of the channels can get quite violent and are just agitating

242
00:19:16,880 --> 00:19:23,920
As the technique goes on and this meditation technique was telling me sitting across the room and saying

243
00:19:23,920 --> 00:19:33,840
Now seeing seeing seeing seeing say to yourself seeing seeing which my mind didn't want to do when it had and

244
00:19:34,640 --> 00:19:37,120
you know this was a very

245
00:19:40,320 --> 00:19:45,360
I guess a big the just having the kind of babysitter of

246
00:19:46,320 --> 00:19:48,320
I'm not letting myself get away with

247
00:19:48,320 --> 00:19:53,280
I'm going along with what was happening

248
00:19:56,320 --> 00:19:58,320
Because I guess

249
00:19:58,720 --> 00:20:05,200
I got to a point where I could be with sensations or be with the breath or some in some way, but still kind of

250
00:20:06,560 --> 00:20:11,040
Be enjoying watching the workings of the mind

251
00:20:13,520 --> 00:20:15,520
So I guess

252
00:20:15,520 --> 00:20:16,880
I

253
00:20:16,880 --> 00:20:26,880
Kind of my mind kind of threw a fit for a while the babysitter of the meditation technique saying now just say thinking thinking thinking just say seeing seeing

254
00:20:30,080 --> 00:20:34,480
Almost kind of like teasing you like I have the remote control and I can turn off this TV if you want

255
00:20:34,640 --> 00:20:38,160
I can turn off this radio of your mind, but you're gonna have to practice since

256
00:20:39,120 --> 00:20:40,480
so I

257
00:20:40,480 --> 00:20:42,480
guess eventually I gave in and

258
00:20:42,480 --> 00:20:44,480
and it's like the

259
00:20:46,000 --> 00:20:48,320
The room disappears and I'm you know in a

260
00:20:49,600 --> 00:20:50,880
great

261
00:20:50,960 --> 00:20:52,720
state of mind

262
00:20:52,880 --> 00:20:56,720
Like a like being you know out on out on the west coast and had a guai

263
00:20:56,720 --> 00:20:58,960
But that's only so great for a little bit

264
00:21:00,000 --> 00:21:04,240
And then I noticed the babysitter still there saying seeing seeing

265
00:21:06,560 --> 00:21:11,120
When you said to me you're feeling very happy and then you hear the babysitter happy

266
00:21:11,120 --> 00:21:13,120
Her calm was it

267
00:21:14,080 --> 00:21:16,080
No

268
00:21:18,080 --> 00:21:22,720
Shatters everything. It's not it's not that fun anymore to have that

269
00:21:24,960 --> 00:21:26,960
Overlooking impartiality

270
00:21:28,080 --> 00:21:30,080
Sit with you

271
00:21:30,720 --> 00:21:34,560
And it's like why isn't it fun? Why like why?

272
00:21:36,320 --> 00:21:38,320
and I kind of

273
00:21:38,320 --> 00:21:43,280
It's sort of tried to examine myself a bit more and it's like well, what's

274
00:21:44,880 --> 00:21:49,840
Like I guess and with the teacher's health like it's really just greed

275
00:21:51,840 --> 00:21:58,320
Like indulgence. I'm not indulging and you know food. I'm not indulging and sleep. I mean sleep deprived of anything

276
00:22:00,560 --> 00:22:05,920
But I'm indulging in my mental states and daydreaming. I'm indulging and

277
00:22:05,920 --> 00:22:11,520
letting myself think letting the ego take hold take reigns and

278
00:22:14,320 --> 00:22:15,840
And that

279
00:22:15,840 --> 00:22:17,840
broke down I guess a whole nother

280
00:22:18,160 --> 00:22:20,160
level to

281
00:22:20,880 --> 00:22:23,600
A state where what would you call that cessation?

282
00:22:25,920 --> 00:22:28,640
That I don't actually remember which is interesting

283
00:22:28,640 --> 00:22:34,720
But what has been important for me with meditation is not so much the experience

284
00:22:36,320 --> 00:22:38,640
as much as like the effects it has and

285
00:22:39,440 --> 00:22:41,440
the effect

286
00:22:41,440 --> 00:22:43,440
coming out of

287
00:22:43,920 --> 00:22:46,240
A state that I don't remember because apparently

288
00:22:48,160 --> 00:22:50,160
His memory is impermanent

289
00:22:52,240 --> 00:22:56,080
Memory is impermanent and if they were impermanent they're going to be suffering no

290
00:22:56,080 --> 00:23:01,920
Right, and if they're suffering. It's not cessation

291
00:23:04,320 --> 00:23:07,360
So I'm I mean, I'm not trying to put words into your mouth and just

292
00:23:08,240 --> 00:23:13,680
Running commentary now go ahead. The most I'm totally with you the most important are the results

293
00:23:13,680 --> 00:23:18,320
So if it the proof of the pudding is under the crest, what was the

294
00:23:18,320 --> 00:23:25,600
The benefit that the change positive or negative coming out of the course very positive

295
00:23:26,080 --> 00:23:29,680
And just a lot of faith and turning away from

296
00:23:31,680 --> 00:23:36,400
Even like experiences in the course. I think I realized I've been clinging to

297
00:23:37,680 --> 00:23:42,640
Oh, wow, this this is like this is a lot of pain. Oh my gosh

298
00:23:43,600 --> 00:23:45,600
Like whoa

299
00:23:45,600 --> 00:23:50,400
What is coming out? Or this like the mental states

300
00:23:51,280 --> 00:23:52,800
I don't know

301
00:23:52,800 --> 00:23:54,800
You know names for

302
00:23:55,120 --> 00:23:59,600
Different types of mental states or revisions coming in and going and

303
00:24:02,480 --> 00:24:07,600
Just making things special. I think when Dhamatak you said

304
00:24:09,120 --> 00:24:11,120
This is not that

305
00:24:11,120 --> 00:24:15,760
And when you said it didn't hit me like it did

306
00:24:16,160 --> 00:24:19,920
I guess maybe a few days later. It's like this is not that because it's no longer

307
00:24:20,320 --> 00:24:23,760
You know, I'm no longer staying in the present moment if I'm

308
00:24:24,800 --> 00:24:27,760
I've had some sort of experience and I'm still

309
00:24:29,600 --> 00:24:34,800
Thinking about it. I'm still walking with it, and I'm not saying thinking thinking thinking

310
00:24:35,760 --> 00:24:36,800
I'm

311
00:24:36,800 --> 00:24:38,800
Creating more I'm proliferating myself

312
00:24:38,800 --> 00:24:44,720
I mean myself really and so I guess having surrender to the technique and

313
00:24:48,560 --> 00:24:53,920
I guess experiencing on some level states of cessation and coming out

314
00:24:55,280 --> 00:24:57,280
experiencing the extreme

315
00:24:58,400 --> 00:25:00,400
sense of calm and peace

316
00:25:01,600 --> 00:25:04,240
And it's a bit more clarity, I guess

317
00:25:04,240 --> 00:25:08,240
It gives a lot of faith of

318
00:25:09,760 --> 00:25:16,720
The danger in proliferating my thoughts and myself and not keeping them in check-not babysitting them and

319
00:25:22,240 --> 00:25:25,920
It gives yeah a lot of inspiration to keep practicing

320
00:25:27,040 --> 00:25:29,040
Yes, wonderful

321
00:25:29,040 --> 00:25:35,040
This, by the way, is the definition the Buddha gave to the word bikhu

322
00:25:36,400 --> 00:25:37,440
bikhu

323
00:25:37,440 --> 00:25:40,560
Sang saadei baiangi katei one who sees the fierciveness

324
00:25:41,360 --> 00:25:43,360
in clinging in

325
00:25:43,360 --> 00:25:45,040
proliferating

326
00:25:45,040 --> 00:25:48,160
And proliferating really this is what the word bikhu means

327
00:25:50,240 --> 00:25:52,240
So very well said

328
00:25:52,480 --> 00:25:54,480
That's the work coming from the book

329
00:25:54,480 --> 00:26:02,320
Well, thank you that was a great thing and it'll be a great resource for people who are in similar situations

330
00:26:02,320 --> 00:26:08,400
And are having similar thoughts to give them encouragement and help them to make the right

331
00:26:08,400 --> 00:26:31,520
choice for their future

