Okay, so today we will be continuing our study of the Satipatanas Buddha.
Yesterday we finished with the Sampaganya Baba, Sampaganya Baba, Sampaganya Baba.
And now we come to the Bharti Kula Manasikara Baba, Baba, which is the section on the contemplation of repulsiveness,
Patti Kula, Patti Kula, Manasikara, and what we can see from this sutta,
if we take a sort of an innocent stance on this sutta, is that,
there are many sorts of practice that we can undertake
for the purpose of coming to understand the body, for example,
for the purpose of coming to understand reality in general, and this is already mostly understood
in terms of some people practicing, some at a meditation, some people practicing,
we pass on the meditation. But this sutta, if we take an innocent view of it,
then we can see that the Buddha is pointing out a sort of a comprehensive list of these
various practices that one might undertake to come closer to understanding reality.
My say an innocent view is because some people take this sutta to be a later creation
where the Buddha brought together many, or the compiler brought together many of the teachings
of the Buddha into one teaching, because they think it's maybe ridiculous to put all
of these teachings together, but for the purposes of this study group in general, just
to be clear, we're taking a fairly innocent view. And I think that's a good way of putting
it rather than saying orthodox or so on, but we're taking it at face value unless something
comes up. We start at face value and then if something really strange comes up that can't
be easily understood or explained, then we try to find a reason. So maybe we end up having
some suspicion later on, but we're going to start by thinking, well, this is something
that the Buddha actually taught, unless we come to some passage that makes us seriously
question this. And I don't think there's anything worth questioning in this regard at all.
It's very easy to understand how the Buddha may have taught all of these together because
it carries out in practice. And if it carries out in practice of a single meditator,
the practice of all of these can be undertaken by a single meditator, or one might pick
and choose. So even if we're to take this as to be a comprehensive teaching for a single
meditator, all of these practices can be undertaken at one time or another. And one can pick
and choose and can tweak actually. All of these tweak the amount that one and the role
that each of these meditations play in one's overall practice. But another thing we have
to keep in mind with this suit and really the suit is in general is that the Buddha would
often be teaching to quite a large audience. And if we look up at the top of this, we know
that the Buddha was dwelling in the near market town, and he was with the bikhus.
And what we hear from the commentary is that actually the bikhus here were not the only ones
in the audience. That the reason he taught this at the Patanasutae in this place is because
the kurus were in general naturally inclined towards mindfulness. So it's clear that this
was probably quite a large audience coming together. This was all the people who were at
would be practicing mindfulness during their work and so on. Even those people who would
be practicing just mindfulness in daily life and so on. And so the point is that not every
meditator is the same and not every meditator will use all of these meditation techniques,
not every meditator will use them all in the same amounts. And so it's quite dogmatic
to think that what is being said in this suit is that one meditator should practice all
of these. I've even heard people say that these should be practiced in order. That
the grammar here, obviously the grammar really doesn't support it. But if one were to
take the Punejaparang literally, which of course one shouldn't, then one might think that
and then the monk should practice this way. So it means start with Anapanasati and then
after that, practice the Iriapata and then practice Sampajanya and then practice this
and so on until finally you're practicing Dhamma Nupasana on the way at the end. And
so these are this sort of interpretation or the interpretation that all of these practices
should be treated separately and can't be practiced in harmony with each other is I think
overly dogmatic. And it arises quite, it's a common thing for meditators to give rise
to this sort of idea that for example, their teacher teaches a specific meditation so they
come to think well that's all you need is only that meditation and you don't ever need
to practice these other meditations. So we fall into that quite often we have meditators
just to we pass on the meditation and we don't pay attention to the other meditations.
But the reason for the teacher doing that is often because that's what the teacher
himself or herself practiced successfully and knows. And so they're not trying to say
that these other practices are useless or unbeneficial. It's just that they understand that
this is a benefit and essential for instance people who teach Anapanasati will say that
that's essential people who practice mindfulness of the elements will say that that's
essential. And we'll leave it up to the meditators to fill in the gaps or in their daily
life to to study and to contemplate these other sort of meditation. So even if we're taking
the sort of meditation that we practice in my tradition as our main meditation practice
or watching the the main the meat and potatoes or the the core of this sutta which is
to watch the body the feelings the mind and the dhamma in sort of a mundane sense or
an absolute sense in terms of absolute reality. So when you move knowing the movement
and and when you feel pain knowing the pain and so on. It doesn't mean that we can't
practice this sort of meditation that's coming up next. In fact this next sort of meditation
can be quite useful even if we're not taking it as our main meditation. But the gulamanasi
kara is actually considered one of the four araka kamatana the meditations that we should
that we use to guard ourselves or to protect our our main meditation. And it's mainly
useful for people who have a great amount of great deal of lust inside of them. And it
can be used in two ways. If you're practicing this meditation in terms of as your main
meditation then in the beginning all it does is repress the feelings. It doesn't help
you to understand them. When you when you just recite these thirty two parts of the body
in the beginning it merely represses them. But once you start to start to
see the reality behind them and start to see your lust in regards to them then it becomes
a practice of for we pass in actually. The commentary says that this practice can lead
all the way up to our hardship. The practice and this is borne out in the suitors as well.
So for someone who who practices the mindfulness of these things well in the one way of
practicing is to just do a recitation and to to fix your mind on one of the objects.
But the we pass in a practice is to then take these and to focus and look at the mind
that is attracted to them. The mind that is repulsed by them and to look at what's the
experience of these things. The experience of the body based on these things and based
on the clarity that has come from the tranquility. The point of using this sort of meditation
is to develop a level of tranquility and peace of mind because this sort of peace of mind
is is disturbed by by lust by our our lust and delusion. The delusion which leads to the
delusion that the body is something beautiful or something handsome or something attractive.
Many people take issue with this sort of a teaching and and say that well it's it's actually
not true that the body is disgusting. The body is just the body it's neither beautiful
nor disgusting and people have if they've developed this sort of view that the body is
something to be respected and they want to have this people have this idea that we shouldn't
see the body is disgusting because that will lead to low self esteem. Then they think
that developing this will will actually be harmful and I think in some people it could
be harmful if they're the sort of person who has a lot of self hatred or so on then they
certainly shouldn't practice this sort of meditation. But if it's a person with a lot
of lust or attachment to the body then it's useful to practice and furthermore the
it's it's it's probably it's most likely quite rare to find someone who who is attached
to the idea that the body is loads some if a person is attached to the idea that the body
is loads some or in some unbeneficial way then they they wouldn't most likely be born
as a as a human being. The reason we're born as human beings is because we find this
form attractive we've gotten used to it as a source of pleasure and enjoyment and so we come back
we become we come to be born in this state and and so really for for ordinary people in
general this is a I think a quite a valid meditation and end a useful meditation because
it it helps to not create repulsion in the mind but to create a wisdom in the mind really
to do to dispel this this delusion that somehow there's something beautiful about the
body. And the Vsudi manga the path of purification does this quite nicely so I thought
I in regards to this one I thought I would just give a sample of what the path of purification
has to say about this. The it goes into quite some detail in the commentary to this
suit that I think does as well but how one should develop mindfulness particular monaz
kara which is also called kaya goddessity mindfulness that is gone to the body. It says
that you'll you'll do them forwards and backwards means you really will chant the Institute
of K. Salomon and then you'll do them in groups and you'll do them based on which is more
clearest and so on. I don't don't think it's so useful to go into detail about the the
method. We just understand that in in general it's a recitation meditation where you focus
on one at a time or you focus on five at a time and you contemplate them. So you're thinking
when you say case how you're thinking of the head hair head hair. You say lo mai you're
thinking of hair of the body nakkaya you're thinking of the nails that you're thinking
of the teach and you're teeth and you actually focus on this part of the body. And
so the commentary recommend whether you see demog actually recommends plucking out one
or two head hairs and placing them in one's hand. When you look at them and you come to
see what hair is like and so you use that as a meditation object which is maybe a good
way to start the practice. But but I wanted to focus more on then then the actual technique
because I think anyone who is interested in practicing this can can go into detail in using
the B.C.D. manga quite easily you know following the meditation practice following the
technique in there. But just to give basic theory I thought to be interesting to talk
about why these things are considered to be loads them and why one might have to admit
that they're loads them. And as I said the B.C.D. manga does a good job of this by explaining
and I think quite objective terms not not subjective at all.
So it talks about head hairs. Firstly head hairs are black in their normal color. The
color of fresh aric ticas seeds whatever those are. As to shape they are the shape of long
round measuring rods. As to direction they lie in the upper direction. As to location
their location is the wet inner skin that envelops the skull. It is bounded on both sides
by the roots of the ears in front by the forehead and behind the nape of the neck and
behind by the nape of the neck. As to limitation they are bounded below by the surface
of their own roots which are fixed by entering to the amount of the tip of a rice grain
into the inner skin that envelops the head. They are bounded above by space and all around
by each other. There are no two hairs together. This is their delimitation by the similar.
Head hairs are not body hairs and body hairs are not head hairs. Being likewise not
intermixed with the remaining 31 parts. The head hairs are a separate part. This is
their delimitation by the dissimilar. We see the mega answering questions here. What
is it's color? What is it? It's got classifications for each part. If for each part it's
going to answer what is it's color? What is it's location? What is it's a delimitation
in terms of what is it's limit or its boundary? There are definition as to repulsiveness
in the five ways that is by color, etc. is as follows. Head hairs are repulsive in color
as well as in shape, owner, habitat and location. For unseeing the color of a head hair
in a bowl of inviting rice grower or cooked rice, people are disgusted and say this
has got hairs in it. Take it away. So they are repulsive in color. Also when people are
eating at night, they are likewise disgusted by the mere sensation of a hair shaped
acabark or makati bark fiber. So they are repulsive in shape. It's an acabark or makati
bark, some kind of food to it. Even when you think that you're eating hair and you're
not eating hair and feel disgusted. If you had a bowl of rice that someone dumped a bunch
of hair in one hair is maybe not so bad, but what if the waitress with long hair or
waiter with long hair stuck there? Their hair in the tip of their head hair was getting
into your food. How would you feel about that? Most people would be quite disgusted. When
you break it up like this, you come to see the nature, but the commentary is actually
willing to accept that hair might be beautiful. We see the magazine as you see in a second.
As to the order of head hairs, unless dressed with a smearing of oil centred with flowers
etc., is most defensive. And it is still worse when they are put in the fire. So I try
to make us imagine burnt hair, which is quite smelly actually. So and then it says even
if head hairs are not directly repulsive in color in shape, some people might argue. Still
their odor is directly repulsive, just as a baby's excrement, as to its color is the color
of turmeric. Turmeric. And as to its shape is the shape of a piece of turmeric root. Turmeric
is this Indian spice that they used to make. And just as the bloated carcass of a black
dog thrown in a rubbish heap, as to its color is the color of a ripe polymera fruit. And
as to its shape is the shape of a drum left face, dirt face down. And its fangs are like
jasmine buds. And so even if both these are not directly repulsive in color in shape, still
their odor is directly repulsive. So too if their head hairs are not directly repulsive
in color in shape, still their odor is directly repulsive. And as to habitat, just as a
potherms, just as potherms that grow on village sewage in a filthy place are disgusting
to civilized people and unusable. There's there are places where they in the world where
they grow, they grow vegetables and so on using human excrement. Apparently in Thailand,
this monk was telling us that in Thailand and in old times, there was this guy who is
growing really big and wonderful vegetables on human feces. But it says most people would
find that disgusting and civilized people would find it unusable. Why? Because of its habitat.
Even the vegetables are beautiful still. They wouldn't eat them thinking this is coming
from human feces. So also head hairs are disgusting since they grow on the sewage of
pus blood, urine, dung, bile, phlam, and the like. So where do the hair grow, they grow
in the skull, in amongst the blood and the blood, not just the blood basically blood
and flesh and hair scalp and so on. And these head hairs grow on the heap of the other
31 parts as fungus do on a dung hill. And going to the filthy place, they grow and they
are quite as unappetizing as vegetables growing on a charnel ground, on a midden, et cetera,
as lotuses or water lilies growing in drains and so on. The drains in India and ancient
herbs. This is the repulsive aspect of their location and so on. And then goes on to define
the rest of them. If you're interested in this, this is what I just quoted, starts on
page 224 of my text and I'm not sure if that's the actual 224 or not. But section 83 of the
chapter, I don't know which chapter it is. Mindfulness occupied with the body. So you
get the point. It's quite an eye opener to read all of that because it's not something
we would have ever thought of. We may intellectually agree when we hear this and think about
it and say, okay, I can understand the body could be seen to be disgusting. But it's
really a shock when you actually look at it and you come to see how steeped in delusion
we actually are. That we could actually think that this is somehow beautiful and it sounds
kind of unpleasant to hear this sort of thing. Our delusion is so deep that we actually
take issue with the idea that the body might be repulsive. We find it offensive for people
to suggest such a thing. We think this sort of teaching must be an evil teaching, a bad
teaching, to that extent. And yet all we have to do is objectively do as the Buddha is showing
us to do here and say to ourselves, case alone or in English as as you understand it.
And you'll come to see that, well, they didn't leave anything out, anything of any importance
out of this list of 32 and going through them back and forth and splitting them into groups
and examining each of these objectively. Do it objectively. That's fine. And look at the
Vasudhi Maga and if you disagree with it, you can be more objective than it. But I think
it's quite objective in its explanation of all of these parts. And then come back and see.
And I think it's quite easy to realize through this practice that the body is actually,
as sorry, Buddha, I believe, was the one who said that he regards this body like a dog
carcass hanging around the neck. Just as some young person, man or woman who is fond
of their body and so on fond of sense and clean clothes would decorate them all up and
then put a dog, dog corpse, hang a dog corpse around their neck. And that's how he looks
at his body. He was using it to explain to counter the accusation that he had hit someone.
This, this monk didn't like Sariputifer. I can't remember the reason, but it's probably
one of you can remember it. Remember the reason why Sariputifer. Yeah, this monk, yeah,
that's right. That's the lion's, sorry, put his lions or this monk would happen. It's
Sariputifer, he didn't like Sariputifer for whatever reason. So Sariputifer once was walking
by him and the corner of his robe touched this monk and he went around saying that Sariput
had struck him and so the Buddha called Sariputifer and asked him, so is it true, Sariputifer
did you conjage you intentionally strike this monk and the Buddha, Sariputifer to give
his lions or anyway, only related to this tangentially in terms of how we should see the
body or how we come to see the body. It's not, it isn't dogma that we have to accept
on faith, but by doing this sort of practice or by even practicing ordinary Vipasana,
one comes to ordinary Satipatana, one comes to see this, this, this truth. And it can
actually be, this is, it can actually be dangerous if you focus on this meditation too
much. As I said, there's a story of these monks who practice this and then got so repulsed
that they decided to kill themselves and they hired someone to kill them like this group
of 30 monks and they had this guy kill them. So the commentary says that well, the Buddha
knew that this was going to happen, but he didn't really have any choice because these
guys had been deer hunters in past lives and were destined to die in a horrible way anyway,
but he figured the best he could do for them is give them some focus and concentration
so that they wouldn't have to come back to be born as humans. Indeed, they were somehow
they benefited from it. But after that, the Buddha said, well, practice, then practice
on upon us that these may be more, more of benefit, but he didn't, he didn't apologize
for it, having taught them, guy I got this at the end, I don't think looking at it innocently,
I don't think we had, he has to apologize because we can understand that it was their karma
that was leading them to it and not the Buddha's instruction. But anyway, point being
you do have to be careful. And in general, you have to be careful with someone to practice,
with practice that is dealing with concepts because if your mind is unstable, you'll go
beyond the established concept. I mean, if you think about any concept, then you understand
that they are subjective. We have 32 parts of the body, but you could just as easily imagine
them being with 33 parts.
It's a good question, doesn't the commentary have, doesn't the commentary you said have
33, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10. I missed it. How many are there?
They count them. And 33 is the brain, the last one is Matalunkang, which is the brain in
the skull. I guess there are 33.
Yeah, it's an interesting question because I wanted to count because our Indology teacher
explained it to us. And I didn't want to agree with her, but she may be right. People
of that time, it's actually said, I don't really know that I could agree with it, but
she's a very famous Indologist, and she's a horrible, very hard to deal with person. Not
a very good professor, but very knowledgeable. She said it's because in those times, they
didn't even know that the brain existed. An odd answer, odd explanation, isn't it, that
they didn't know about the brain, because they would never cut apart bodies. They would
ever break open the skull. They would always burn the bodies. And we could at least understand
that that would be the reason why the Buddha wouldn't mention it, because it wouldn't
be that common. That's how we could understand this, is that, yes, people didn't know
about the brain, but wasn't very common, and people had never really seen it. And they
obviously didn't have these biology books that picture books that show you what the brain
looks like. So it wasn't something that anyone had ever seen, or would see in general.
Now the reason I have a bit of a one reason I have a problem with that is because we're
taught that in the ancient India, the last thing they would do when they were burning
the body is smash open the skull of the dead person. It became a ritual because you want
the whole body to burn evenly. So they actually have a smashing open of the skull. In
which case, you would expect that they wouldn't see the brain. I believe in the Visudhi
Maghade has the, here we are, it has the, it has the brain. Okay, here's interesting.
The brain being included in the bone marrow in this version of the total of only 31 aspect.
Now why are we getting only targeting 32 parts? No, see without the brain, it's only 31
parts. Am I wrong about this? Let's see, let's count them again. Let's see the Paul, you
know, that's it's easy.
Thank you.
I have to find an answer on that one, but with the commentary, with the path of purification
says, is that the one without the brain is just including it in marrow bone marrow.
The commentary says that the brain is included in the atimine gem, the bone marrow, because
technically the brain is something inside the bones, inside the skull.
But the idea in indian india was that the mind was in the heart, so the brain was just
like some big useless fatty thing, maybe it's because people didn't think so much back
then as well, they just sat around and meditated, didn't use computers, didn't have to
figure out why their internet connection wasn't working, so they didn't use the brain
quite as much as we use it today, it's quite possible as well.
Maybe back then they didn't even have brains, and it's something that's just appeared
no because we have fossil evidence, no, probably there were brains back then, well you
have to examine all possibilities, it's quite likely they did have brains, but as my
endologist teacher said, in knowledge he teaches it, they didn't know they existed,
and so it would have been unfamiliar to most people at the very least, and that's why they
don't mention it, but you could easily put the brain in here and ask yourself whether the
brain was exempt from all this, so people think the brain is quite amazing, no, they
think the body and general biologists who study DNA and RNA and genes and so on, they think
how wonderful it is, but this is a subjectivity, if one is objective about the body, even
about genes and DNA and so on, one can see how, you know, the only reason that biologists
say humans evolved is by mistake, when DNA makes a mistake that we develop, that there
is evolution, because sometimes the mistakes work better than the original, the copy incorrectly,
but the point being that it's all just a big mess, it's not a machine that works perfectly,
it's something that is almost up, I mean look at the genes, how genes work, some people
become mentally handicapped as a result of their genes or their growth is standard as a result
of their genes, or they have a chemical imbalance in the brain as a result of their genes
and so on, there was an interesting article that I was reading about, oh yes, on psilocybin,
this drug, you were in, you didn't, Nagasena didn't hear it either, they did the study
on people using psilocybin injecting them with psilocybin, the active ingredient in magic mushrooms
and what they expected to find, or what anyone would have expected them to find is that
it increases brain activity, stimulates certain brain activities that cause you to hallucinate,
it's actually the opposite, according to this study, actually psilocybin inhibits brain
activity, stops the brain from working, not completely, but to a certain extent inhibits
the brain's ability to work and so the conclusion or the hypothesis of the, that came
out of the experiment is that actually the brain serves not as the initiation of thought,
but as a filter of thought, which was a theory that was advanced by the great, I don't
know what you'd call them, Dr. Henry James, William, William James James was his last name,
James, William James Henry James can't remember, but this guy who, and forgot the other
guy's name, but this book irreducible mind talks all about this, William James was his name
and the other one was, forgot his name as well, the irreducible mind, it's a huge book
very hard to read, but very interesting talks about, there's new idea that they had in
the early 20th century, early 1900s, about how the brain might not be the creator of
thought, but actually just a filter, and the reason they came up with it was not because
they were some kind of mystics or had some ideas that they wanted to prove, but that's
what the data suggests, that was what they had to, the conclusion, the only conclusion
that met the fact, fit the facts, based on clinical studies that they were doing at the
time, based on extreme cases where people had these strange experiences.
And this is really the points to the, that's very tangential at this point, but the conflict
between going with quantity of results, as opposed to going with quality of results,
because most scientists will reject these fringe subjects and reject a lot of interesting
material for the study of the brain, because you can't quantify it, you can't pick out
a thousand people who have strange mental experiences and verify it against them all.
So they consider these things to be anecdotal, because, you know, if a scientist studies
a single person, then it's anecdotal evidence, it's not a proper experiment or proper observation.
But the point they make is that this is, this isn't science, this is what they call
scientism, the blind adherence to some method that can't possibly hope to bring the truth,
because it's these extreme cases that show that stretch the borders of our understanding.
And so the interesting thing about this study was that it, and about this whole theory
is that it obviously goes quite well with how we understand reality and Buddhism, that
the brain doesn't create the, and it helps to answer some questions about how we relate
our theory and also our practice to modern science, for example, to modern science.
So we understand that what is the, what does the brain do?
It's more like an office than a home of the mind, the mind comes in and does its work
and uses the brain as a control room in that, so it interacts with the body.
So the point I wanted to make here is in regards to answering the question of how we
can understand the brain to be a, some of how negative aspects is that actually through
the meditation practice, especially become to see the brain as an incredibly negative
thing.
And I would, I would be quite forceful about that observation that the brain is an incredibly
negative thing that without the brain, we, our thought process would be so much freer.
And this is, I think, a revolutionary idea, but we have this backing of this wonderful YouTube
video of this neurobiologist, I think neuroscientist, Jill, can't remember her last name.
See, horrible with names.
Anyway, we talked about this, one of the other study courses or at some point we talked
about this.
So on YouTube, very famous, this doctor has a stroke and she describes how free the
mind became and how pure the mind became when the time when half of her brain wasn't working.
And then there's that UN video that we watched, what was his name?
Careful brain.
He was like Bruce, Bruce, Grayson, and he explains this idea of, or this observable phenomenon
that doctors come across, that people who go brain dead, the brain isn't working.
And can still, and have incredible experiences and clear thought processes that shouldn't
be possible without active brain functioning.
And so if we start to appreciate this apparent truth that is not being appreciated by
neurobiologists or scientists in general, then when we look at what the brain actually
does, if the brain isn't creating consciousness, which would make it kind of a wonderful
thing, magical thing, really, then what is it really doing?
We can see that actually it's keeping us in a cage, this cage of the body.
That's forcing our mind activity to be limited to these six senses.
So we go around saying this is beautiful, that's beautiful, and that's a wonderful sound.
When we could in fact be constantly living a life of bliss, it's possible that to enter
into a state of mind this neurobiologist or neuroscientist, her experience is quite incredible
to listen to, where it was totally beyond any kind of partial sensual experience that
we could possibly have as human beings, or in an ordinary state, because it was relieved
of this cage of the brain and the six senses, sensuality.
So a little bit of a tangent there was the nature of the brain.
He's certainly not leaving the brain out because it's something beautiful or wonderful.
So this is a certain meditation that people practice in, and of course there's a very detailed
explanation of how to practice it.
It also happens to be the meditation practice that traditionally monks learn when they first
or novices learn when they first ordain, and the idea is to give them meditation practice
and it shows that even today there's still a traditional emphasis on meditation.
Even the traditional ordination procedure, the first step is learning meditation.
You know, well now you're a novice, which should you do first learn meditation, and so
this is considered to be mula, it's called mula kamatana, the foundation meditation or
your first meditation.
It's the one that you should take up first and use it as a general means of getting
yourself to a level where you can appreciate the higher teachings of ultimate reality.
Because before we overcome our attachment to being human, and this is really what this
meditation comes down to, it's giving up the attachment to the human state.
This idea that being human is somehow special or a good thing or ultimately real.
Like we are humans in the human race thinking of how important it is and so on.
Thinking as the human race is something substantial or something real and the human state
is being a specific state that happens to be the highest state that there is, right?
Because if you're a materialist, then you look around you and say, human beings are obviously
the highest, the highest that exists, the highest that there is thinking it's somehow special.
So when you practice this meditation, it helps you to see, well what is the really
is experience, the whole body is just, it's not actually anything substantial in there.
You see this by giving up your attachment to it.
Once you see how disgusting it is and then the mind lets go, then you're able to look at
it objectively.
And that's when you switch to practicing Vipassana and looking at it objectively.
But the objective, the objectivity comes about, is helped through the practice of
Kaya Gattasati or Patigulas, Patikulasanya, Patikula, Manasika, that's called, Manasika.
Then the Buddha gives it a simile.
It's just as if one were to have a bag with two openings.
And the bag has two openings.
The body actually has nine openings.
I'm not going to name them, but I think there are places in the sutas, I can't remember
it's been a long time since I read those ones, about as if the sutas are commoners,
then the bag with nine holes, I think of this body as a bag with nine holes that is leaking
and poorly sewn up, so it's splitting at the seams and gets cut and then pours out all
sorts of awful things.
This meditation teacher he was talking about, these people growing crops on human feces,
he said, we're just what we are, we're all just portable toilets.
That's what he called as he said, we're portable, portable bodies, that's what we are, full
of urine and feces and awful stuff.
So actually we are a bag that has nine holes and not these nine holes and even through
the skin is oozing out all sorts of wonderful stuff, gold and jewels and frankincense
and fur and sugar and spice and all things nice, it's just pouring out of these holes
and oozing out of the skin, sugar and spice, cinnamon and so on, no cinnamon and cinnamon
goes into the body, it comes out something quite different, garlic, when garlic goes into
the body it comes out quite powerfully actually, milk, when milk goes into the body it
goes in pure, people actually have milk baths because they think of milk as being so pure,
but after you drink it it comes out quite different.
So there's actually a meditation on the repulsiveness of food in particular, it helps
you see the let go of the attachment, if you food, if you read the VCD manga it talks about
that, it's quite interesting as well to break down the process of eating and see what's
really going on, it gives the simile to help us to understand how we're to do it,
how we're to look at the body and pick out the pieces and look at it one piece at a time
and then he repeats the stanza that we've already gone through in the other sections,
eat the Ajat dangans on, it's the end of that section, so we'll do the next section as
well and then we'll call it quits, for how we're doing it's 9 o'clock, from 7 30, that's
an hour and a half, and I know we were lost some time there with the, the reason why I hesitate
is because the next one I want to talk about in terms of its place and we pass in the
meditation and it's getting a little bit on, I think we'll stop there, let's, apologies
for the technical difficulties but I think we'll stop there and come back next week to
go on with the next section and hopefully finish up Gaiya Sapti, Gaiya Nupasana next week,
so thanks everyone for coming and hopefully we'll be able to start out the technical
difficulties and before next week, before we leave we'll do another homage,
thank you all,
