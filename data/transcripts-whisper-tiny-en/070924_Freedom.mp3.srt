1
00:00:00,000 --> 00:00:29,000
I would like to talk a little bit more about the Buddhist teaching on freedom in the meditation practice that we follow our goal is to attain freedom.

2
00:00:29,000 --> 00:00:43,000
For meditators who come for a short time, we have time enough to finish the foundation course or time even to finish the advanced course.

3
00:00:43,000 --> 00:00:53,000
We expect that they should at the very least be able to get a taste of freedom.

4
00:00:53,000 --> 00:01:11,000
That they should be able to realize for themselves what it means to be free, given on a small scale, even just a taste.

5
00:01:11,000 --> 00:01:20,000
It's important to understand what we mean by the word freedom in Buddhism.

6
00:01:20,000 --> 00:01:34,000
Of course when we hear the word freedom in the West or in the modern world and throughout the world really, we generally think about freedom too.

7
00:01:34,000 --> 00:01:49,000
Freedom to say freedom to act, freedom to think in whatever way we like, in whatever way it appeals to us, in whatever way we think fit.

8
00:01:49,000 --> 00:02:07,000
And so we think being truly free is being able to think whatever it is you intend to think, holding whatever views you think appropriate, thinking whatever thoughts you think appropriate.

9
00:02:07,000 --> 00:02:21,000
You're not having anyone else tell you that you're wrong or not have anyone else try to stop you from thinking and believing what you will.

10
00:02:21,000 --> 00:02:45,000
And we think of freedom as saying whatever it is that you like to say.

11
00:02:45,000 --> 00:02:55,000
Or we can do whatever we like to do.

12
00:02:55,000 --> 00:03:11,000
And we think of this as a sort of a freedom, but if we look at how it plays out really we in the end are none of us free to do or say or think what we will.

13
00:03:11,000 --> 00:03:40,000
That our thoughts and our speech and our actions are all defined by an innate character which is based on our likes and our dislikes, our beliefs and our delusions and our conditioning, social conditioning, parental conditioning, religious conditioning.

14
00:03:40,000 --> 00:03:47,000
And we find ourselves trapped in our freedom.

15
00:03:47,000 --> 00:03:59,000
So we say I have the freedom to believe whatever I like to believe but actually our beliefs come from something which we really have no control of.

16
00:03:59,000 --> 00:04:10,000
Come from our parents or come from our upbringing, come from our society, come from the media and so on.

17
00:04:10,000 --> 00:04:28,000
And moreover come from our own desires and diversions. When we want something we create all sorts of beliefs as to why we deserve to get what we want.

18
00:04:28,000 --> 00:04:47,000
I deserve to have this, I deserve to have that and when bad things come to us we make all sorts of beliefs and views as to why I don't deserve my suffering is not justified.

19
00:04:47,000 --> 00:05:09,000
We have all sorts of thoughts and views which are based on our conditioning and our speech is based on how to get finding ways to get the things which bring us pleasure or how to remove the things which bring us displeasure.

20
00:05:09,000 --> 00:05:21,000
When we see other people and we're experiencing happiness and we feel jealous or upset because they got better than we did or we want the things that they have.

21
00:05:21,000 --> 00:05:30,000
We try to find ways to disparage them, say bad things about them and hurt them because we want what they have.

22
00:05:30,000 --> 00:05:37,000
We want them to not have their position or their wealth or their status.

23
00:05:37,000 --> 00:06:03,000
And so we say and even do things which are often purely based on our desires and our diversions with chasing after central desires, chasing after objects of pleasure, running away, chasing away, fighting off all the time, things which are displeasing.

24
00:06:03,000 --> 00:06:14,000
So we find ourselves not really free no matter how much we say, we should be free that no one should tell us how to act, how to speak, how to think.

25
00:06:14,000 --> 00:06:40,000
So in fact we see this exactly what the Lord would have did is taught us how we should act, how we should speak and how we should even think.

26
00:06:40,000 --> 00:06:46,000
Which were wrong, which should be abandoned.

27
00:06:46,000 --> 00:07:07,000
The Lord would have taught of course freedom from, which is freedom from evil, freedom from suffering, freedom from defilement.

28
00:07:07,000 --> 00:07:12,000
And if we look at it we can see this is a much more worthy sort of freedom.

29
00:07:12,000 --> 00:07:22,000
That really all that all of us want is to be free from any kind of suffering, any kind of upset, any kind of displeasure whatsoever.

30
00:07:22,000 --> 00:07:27,000
When we talk about happiness really what we mean is just to be at peace.

31
00:07:27,000 --> 00:07:35,000
We don't ever, most of us think that we have to experience extreme states of pleasure all the time.

32
00:07:35,000 --> 00:07:55,000
We simply wish that we should not have to face unpleasantness, pain and aching and upset, frustration and so on.

33
00:07:55,000 --> 00:08:09,000
That we should be at us, we should be able to find some sort of peace and a sort of a constant state of happiness in our lives.

34
00:08:09,000 --> 00:08:16,000
So the Lord would have taught freedom from suffering.

35
00:08:16,000 --> 00:08:26,000
And he explained the reason why people suffer is because in a sense they don't understand suffering.

36
00:08:26,000 --> 00:08:29,000
They don't understand the cause of suffering.

37
00:08:29,000 --> 00:08:43,000
Just like why people suffer from obesity or people suffer from heart disease or people suffer from various illnesses, ailments, which really are curable or even preventable.

38
00:08:43,000 --> 00:08:53,000
But because people don't understand these diseases, how to treat them, how to cure them, how to act when one is afflicted by this illness.

39
00:08:53,000 --> 00:09:01,000
And because they don't understand what caused the illness in the first place, they aren't able to change their lifestyles.

40
00:09:01,000 --> 00:09:21,000
You can find people suffering from so many various illnesses, unable to get themselves out of the cycle. Often simply because they don't understand and they don't see the danger in the what they are doing.

41
00:09:21,000 --> 00:09:35,000
As far as suffering goes the Lord Buddha saw that mostly the way we look at suffering is simply a painful feeling or something temporary, which we can remove.

42
00:09:35,000 --> 00:09:45,000
We can see only so far as those unpleasant things, we see this is suffering when we receive something unpleasant.

43
00:09:45,000 --> 00:10:05,000
And because we can see only so far that it is an occurrence which is visible, we look at suffering as something which arises and is treatable by various means.

44
00:10:05,000 --> 00:10:11,000
Because we don't understand that suffering is something which is an intrinsic part of life.

45
00:10:11,000 --> 00:10:20,000
So this is a reason why we suffer.

46
00:10:20,000 --> 00:10:28,000
We chase after all sorts of pleasant, pleasurable sensations.

47
00:10:28,000 --> 00:10:45,000
And whenever pain or suffering comes up, we immediately try to find a way to remove the source of the thing which we dislike.

48
00:10:45,000 --> 00:10:55,000
When we see something unpleasant, we run away from it. When we hear something unpleasant, we close our ears.

49
00:10:55,000 --> 00:11:17,000
When we find ourselves running away from the cause of suffering, when we have a backache, we get a massage, when we have a headache, we take a pill, when we have a heartache, we take drugs or alcohol and find ourselves indulging in all sorts of unwholesome behaviors.

50
00:11:17,000 --> 00:11:26,000
Simply because we don't see that suffering is an intrinsic part of life.

51
00:11:26,000 --> 00:11:42,000
People who aren't able to understand things like meditation or understand how the mind works, aren't able to understand why someone would want to train their mind.

52
00:11:42,000 --> 00:11:51,000
We see them chasing after pleasure, after pleasure.

53
00:11:51,000 --> 00:12:02,000
We can see them performing all sorts of unwholesome deeds and we see their minds becoming weak, becoming unstable.

54
00:12:02,000 --> 00:12:11,000
We see the same people who will tell you how to live for the moment and chase after pleasure in the here and now.

55
00:12:11,000 --> 00:12:37,000
We see the same people running out to find therapists to get therapists and find a masseuse or a chiropractor or a doctor or someone to get rid of their constant effort present, suffering, the onslaught of so many various different kinds of suffering which arise.

56
00:12:37,000 --> 00:12:50,000
Even simply sitting still, you see the average person is unable to sit still for any long period of time unless they have a very comfortable, very plush, expensive chair to sit in.

57
00:12:50,000 --> 00:12:57,000
And even then you watch them and after some time they become bored, they become restless and they have to change position.

58
00:12:57,000 --> 00:13:04,000
They have to get up and go and chase after more pleasure and they're really never at the state of peace and happiness.

59
00:13:04,000 --> 00:13:14,000
They're unable to sit still, unable to rest and so their mind keeps chasing and chasing and chasing and running and running and running.

60
00:13:14,000 --> 00:13:25,000
Lord Buddha taught that in the very end there's no one who can run away from suffering, there's no person who can run to a place where there is only happiness.

61
00:13:25,000 --> 00:13:33,000
In the end we all have to suffer through old ages and death no matter how much happiness or pleasure we might find here and now.

62
00:13:33,000 --> 00:13:36,000
In the end we have to leave it all behind.

63
00:13:36,000 --> 00:13:52,000
And then we see old people on their deathbed crying unable to deal with unpleasantness, unable to deal with the fear and the pain which arises at the moment of death.

64
00:13:52,000 --> 00:14:10,000
And it's really shameful to see because throughout their whole life they were confronted by these very same sensations and they kept running and running away from them seeing that all they had to do was find a doctor so on someone who could cure their illness and then they would be happy again.

65
00:14:10,000 --> 00:14:16,000
Never thinking that in the end it would come to defeat them.

66
00:14:16,000 --> 00:14:27,000
It would become so strong that there would be no way out in the end they would have to face death.

67
00:14:27,000 --> 00:14:32,000
So it takes something very special.

68
00:14:32,000 --> 00:14:45,000
It takes a very strong sense of understanding, something which has perhaps been developed in lifetime after lifetime after lifetime for a person to really think.

69
00:14:45,000 --> 00:14:49,000
Think beyond the ephemeral.

70
00:14:49,000 --> 00:15:00,000
Think beyond the states of happiness which arise and cease incessantly and are none of them really satisfying.

71
00:15:00,000 --> 00:15:13,000
Think through to the point where we no longer wish to run after these states of pleasure.

72
00:15:13,000 --> 00:15:19,000
And we can see that in the end we have to leave them all behind and they're really worthless.

73
00:15:19,000 --> 00:15:23,000
They don't leave us as better people, happier or more peaceful people.

74
00:15:23,000 --> 00:15:28,000
In fact they create disturbance and upset in our minds.

75
00:15:28,000 --> 00:15:32,000
And so to practice meditation.

76
00:15:32,000 --> 00:15:40,000
For someone who has come to this point it's very, very special, very hard to find.

77
00:15:40,000 --> 00:15:50,000
Something which is very encouraging to see that there are still so many people in this world who are interested in keen on coming to practice meditation.

78
00:15:50,000 --> 00:16:00,000
Seeing that these central pleasures out there are really not satisfying.

79
00:16:00,000 --> 00:16:08,000
And when we come to practice so then we can really and truly come to see the truth of suffering and the causes of suffering.

80
00:16:08,000 --> 00:16:13,000
It starts by seeing the characteristic of suffering.

81
00:16:13,000 --> 00:16:18,000
Seeing that every single thing in this world has the characteristic of suffering.

82
00:16:18,000 --> 00:16:23,000
There's nothing inside of ourselves or in the world around us which can truly satisfy us.

83
00:16:23,000 --> 00:16:30,000
Characteristic of suffering means it is something which cannot bring about satisfaction or contentment.

84
00:16:30,000 --> 00:16:33,000
It's not something like we think that it is.

85
00:16:33,000 --> 00:16:45,000
We think that everything which we gain is something which we will be able to put up on a mantle or put on our table or put in our room, put in our home.

86
00:16:45,000 --> 00:16:53,000
Keep as our own and that will perpetually bring us peace and happiness.

87
00:16:53,000 --> 00:17:03,000
In fact we find precisely the opposite that the more we get, the more we obtain, the more distracted and distraught and upset our minds become.

88
00:17:03,000 --> 00:17:08,000
Always constantly thinking about the next pleasure.

89
00:17:08,000 --> 00:17:14,000
Constantly worried or afraid of any sort of suffering that might come.

90
00:17:14,000 --> 00:17:22,000
Our minds flitting here and there if you watch you can see people who are engaged in consumerism.

91
00:17:22,000 --> 00:17:26,000
You can see how their minds work just by watching their faces.

92
00:17:26,000 --> 00:17:35,000
They're unable to sit still, unable to be at peace with themselves.

93
00:17:35,000 --> 00:17:45,000
No matter how much happiness they might find, you can see that in the end when suffering overwhelms them they will be completely at a loss.

94
00:17:45,000 --> 00:17:54,000
The same people end up crying and falling into despair when even the slightest and happiness comes to them.

95
00:17:54,000 --> 00:18:00,000
In the end when all they'd sickness and death come there's no way that they can possibly hope to deal with it.

96
00:18:00,000 --> 00:18:13,000
And when they die with all the defilements and all the upset in their mind there's no way that they can hope to be free from suffering after they pass away.

97
00:18:13,000 --> 00:18:20,000
When we see that this is the nature of all Sankara, all things which exist in the world, all things which arise.

98
00:18:20,000 --> 00:18:33,000
All things which are formed which come into being based on the laws of nature, the laws of physics, the laws of karma, the law of the mind.

99
00:18:33,000 --> 00:18:35,000
The meditator starts to let go.

100
00:18:35,000 --> 00:18:43,000
It starts to become bored and uninterested, disenchanted, they say.

101
00:18:43,000 --> 00:19:01,000
The mind starts to turn away, whereas before the mind was something like sitting on the edge of your seat, our minds, our hearts are sitting waiting, waiting for the next pleasant sight to jump out, jump out at it.

102
00:19:01,000 --> 00:19:10,000
When we see a beautiful person, someone who is the object of our desire, right away the mind leaps out.

103
00:19:10,000 --> 00:19:18,000
When we smell good food, right away our mind, our mouth starts to water.

104
00:19:18,000 --> 00:19:25,000
When we taste something, some good food, right away our minds perk up.

105
00:19:25,000 --> 00:19:37,000
Jumping out, savoring the taste, waiting for the next taste, eating more edeker, and getting more taste, more of the good taste.

106
00:19:37,000 --> 00:19:42,000
When we think, when we start to think pleasant thoughts, our mind jumps out at them.

107
00:19:42,000 --> 00:19:54,000
When the meditator starts to practice and sees this jumping of the mind, which is not at peace, which is not happy, which is not in any way, a wholesome or beneficial.

108
00:19:54,000 --> 00:19:56,000
The meditator starts to become bored.

109
00:19:56,000 --> 00:19:59,000
It starts to see as well that this is not under our control.

110
00:19:59,000 --> 00:20:06,000
Once we establish this and develop it, it's not something we can then control and turn off or on as we wish.

111
00:20:06,000 --> 00:20:10,000
It's something which then envelops us.

112
00:20:10,000 --> 00:20:15,000
Like a great fire, it comes to burn our whole being.

113
00:20:15,000 --> 00:20:19,000
And something we cannot, a fire we cannot easily put out.

114
00:20:19,000 --> 00:20:31,000
And so, like a person who is houses on fire, we become disenchanted and start to say, this house isn't really no longer worth expecting.

115
00:20:31,000 --> 00:20:35,000
The fire has completely enveloped the house.

116
00:20:35,000 --> 00:20:43,000
And the meditator starts to loosen their grip on the body and the mind, and even the whole world around.

117
00:20:43,000 --> 00:20:50,000
The mind starts to let go, so when we see good things or see bad things, the mind is not leaping out anymore.

118
00:20:50,000 --> 00:20:56,000
When we hear good things, we don't like what someone says, we don't like what someone does.

119
00:20:56,000 --> 00:21:00,000
We think this is a bad person, that is a bad person.

120
00:21:00,000 --> 00:21:06,000
Mean people, unfair, so on.

121
00:21:06,000 --> 00:21:15,000
When we feel like this, when we practice, we start to lose these sort of feelings.

122
00:21:15,000 --> 00:21:27,000
When the mind starts to become at peace, we feel like this upset or this anger or this fear or boredom or desire, which we used to carry around like a flag.

123
00:21:27,000 --> 00:21:33,000
It starts to become evident.

124
00:21:33,000 --> 00:21:36,000
Something which we no longer want, we no longer wish for.

125
00:21:36,000 --> 00:21:41,000
When we start to let go and give up, this is the benefit of the practice.

126
00:21:41,000 --> 00:21:44,000
This is how we attain freedom.

127
00:21:44,000 --> 00:21:51,000
Because freedom from suffering is not something that another person can take from you or give to you.

128
00:21:51,000 --> 00:21:54,000
Not like freedom to do this or do that.

129
00:21:54,000 --> 00:21:57,000
People can take this from you.

130
00:21:57,000 --> 00:22:03,000
People can even take your freedom to think if they're clever enough.

131
00:22:03,000 --> 00:22:11,000
But no one can take away or give you freedom from suffering.

132
00:22:11,000 --> 00:22:22,000
It's something which comes to us the time when we let go.

133
00:22:22,000 --> 00:22:30,000
At the time when we see impermanence, at the time when we see suffering, at the time when we're walking and sitting,

134
00:22:30,000 --> 00:22:33,000
we sing to ourselves rising and falling.

135
00:22:33,000 --> 00:22:38,000
And the mind is seeing clearly the nature of the things inside of us and around us.

136
00:22:38,000 --> 00:22:42,000
This is how we attain freedom from suffering.

137
00:22:42,000 --> 00:22:46,000
It comes by seeing the truth of suffering.

138
00:22:46,000 --> 00:22:50,000
That there's really no one holding on to us or holding us down.

139
00:22:50,000 --> 00:22:54,000
No one keeping us in suffering.

140
00:22:54,000 --> 00:23:00,000
This sort of freedom is something which comes when we ourselves let go.

141
00:23:00,000 --> 00:23:08,000
When we ourselves cease to hold on, cease to attach, cease to chase after.

142
00:23:08,000 --> 00:23:16,000
We ourselves are binding ourselves, imprisoning ourselves.

143
00:23:16,000 --> 00:23:21,000
When we see this, when we see that the cause of our suffering is our own minds,

144
00:23:21,000 --> 00:23:24,000
this is called the freedom from suffering.

145
00:23:24,000 --> 00:23:26,000
This is the truth of suffering.

146
00:23:26,000 --> 00:23:28,000
Seeing the truth of suffering.

147
00:23:28,000 --> 00:23:32,000
Seeing the cause of suffering and abandoning the cause.

148
00:23:32,000 --> 00:23:34,000
Letting go.

149
00:23:34,000 --> 00:23:38,000
At the moment when the mind turns away, turns away and finally,

150
00:23:38,000 --> 00:23:42,000
completely abandoned attachment.

151
00:23:42,000 --> 00:23:46,000
This is called the freedom from suffering.

152
00:23:46,000 --> 00:23:57,000
So freedom in this sense is also not something which requires that we go anywhere or leave the world or become anything.

153
00:23:57,000 --> 00:24:02,000
It's a freedom which we can obtain even when living in the world.

154
00:24:02,000 --> 00:24:09,000
Or we can carry with us even living in the world that no one can take away from us.

155
00:24:09,000 --> 00:24:12,000
Because we see that things we see, we hear, we smell, we taste.

156
00:24:12,000 --> 00:24:15,000
These things cannot really make us suffer.

157
00:24:15,000 --> 00:24:19,000
There's only our own craving which can bring about suffering.

158
00:24:19,000 --> 00:24:25,000
The cause of suffering is not unpleasant sights or sounds or smells or tastes or feelings or thoughts.

159
00:24:25,000 --> 00:24:27,000
The cause of suffering is attachment.

160
00:24:27,000 --> 00:24:37,000
So we can live simply as we have been living only without any sort of craving or aversion

161
00:24:37,000 --> 00:24:40,000
to the objects of a sense.

162
00:24:40,000 --> 00:24:43,000
This is what the Lord would have called freedom.

163
00:24:43,000 --> 00:24:49,000
When we practice meditation, we're looking for at least a small taste of freedom.

164
00:24:49,000 --> 00:24:53,000
When we hope that all of the meditators will come to practice with us.

165
00:24:53,000 --> 00:24:57,000
Should be able to obtain at least a small taste of freedom.

166
00:24:57,000 --> 00:25:06,000
And if they continue to practice, we can expect that once they are able to attain what they know for themselves is the right path.

167
00:25:06,000 --> 00:25:14,000
And they're able to save for themselves without anyone else convincing them or coercing them.

168
00:25:14,000 --> 00:25:24,000
Once they can say that this or that is the right path, we can expect that they continue on that path that they should not only be able to taste freedom,

169
00:25:24,000 --> 00:25:28,000
but in the end, be able to become completely free.

170
00:25:28,000 --> 00:25:36,000
This is the goal which the Lord would for this meditator is followers.

171
00:25:36,000 --> 00:25:41,000
And this is the goal which we present to our meditators.

172
00:25:41,000 --> 00:25:50,000
Saying only, we expect that you should gain a taste of freedom once you have obtained the taste of freedom.

173
00:25:50,000 --> 00:26:00,000
It's up to you to look inside yourself and see whether you know for yourself that this path leads to freedom.

174
00:26:00,000 --> 00:26:16,000
Once you have tasted freedom, we expect and we can observe that the meditators who have genuinely tasted freedom that they are then able to save for themselves that they have found the right path.

175
00:26:16,000 --> 00:26:20,000
They have found the path which leads to complete freedom.

176
00:26:20,000 --> 00:26:29,000
The path which will lead them in this very life without leaving the world without going anywhere, being anything will lead them to complete freedom from suffering.

177
00:26:29,000 --> 00:26:39,000
Based on their own practice and their own dedication to the practice.

178
00:26:39,000 --> 00:26:49,000
So this is what is teaching on what it means to be free and this is the demo which I give today.

