1
00:00:00,000 --> 00:00:03,080
Hello and welcome back to our study of the Dhamma Pandat.

2
00:00:03,080 --> 00:00:10,080
Today we will continue with verses number 11 and 12, which we just follow.

3
00:00:33,080 --> 00:00:37,920
So, it is a twin pair of verses.

4
00:00:37,920 --> 00:00:46,280
The first one, Assare Saramatino, for a person who understands what is essential to

5
00:00:46,280 --> 00:00:47,280
be unessential.

6
00:00:47,280 --> 00:00:52,040
Or, understand, sorry, what understands what is unessential to be essential.

7
00:00:52,040 --> 00:00:59,280
Saro, tasare, tasare, dasino, and understands what is essential or sees what is essential

8
00:00:59,280 --> 00:01:01,080
as being unessential.

9
00:01:01,080 --> 00:01:06,280
Tasare, dasare, dicatanti, such a person doesn't come to what is essential.

10
00:01:06,280 --> 00:01:13,360
Mita, sankapagotara, because they dwell in wrong thought and a wrong understanding.

11
00:01:13,360 --> 00:01:21,400
Saran, tasare, tauyatua, a person who knows the essential to be essential.

12
00:01:21,400 --> 00:01:27,320
Assare, tasare, tau, and what is unessential to be unessential.

13
00:01:27,320 --> 00:01:35,640
Tasare, dicatanti, sankapagotara, such a person does come to what is essential because

14
00:01:35,640 --> 00:01:38,040
they dwell in right thought.

15
00:01:38,040 --> 00:01:45,200
Now, these two verses were told in relation to the Buddha's two chief disciples.

16
00:01:45,200 --> 00:01:50,280
And their story, in brief, is that they were living in Radhikaha and the time when the Buddha

17
00:01:50,280 --> 00:01:55,360
had just become enlightened and they had left the home life because they had seen that

18
00:01:55,360 --> 00:02:03,360
there was no essence or nothing, it wasn't essential or there was no benefit to be found

19
00:02:03,360 --> 00:02:07,000
in the life that they were living, they would go to these shows and they would laugh

20
00:02:07,000 --> 00:02:13,600
and they would cry and they found that it brought them no real true peace and happiness

21
00:02:13,600 --> 00:02:20,800
because it was bereft of any sort of essence or any depth.

22
00:02:20,800 --> 00:02:26,600
And so they left home and they went to try to find a teacher and they found the first

23
00:02:26,600 --> 00:02:29,120
teacher they found was named Sanjaya.

24
00:02:29,120 --> 00:02:35,920
Sanjaya was a teacher in the time of the Buddha and he taught his students how to avoid

25
00:02:35,920 --> 00:02:40,640
views, how to avoid any kind of teaching actually.

26
00:02:40,640 --> 00:02:43,400
So when people would ask them question, they would say, well, I don't believe that,

27
00:02:43,400 --> 00:02:45,800
but I don't believe otherwise.

28
00:02:45,800 --> 00:02:51,320
And so they would find this way to avoid answering the question and therefore avoid taking

29
00:02:51,320 --> 00:02:53,160
any committing to any stance.

30
00:02:53,160 --> 00:02:59,800
And of course this has the benefit, at least superficially, of keeping a person from developing

31
00:02:59,800 --> 00:03:04,360
wrong views but it becomes a real conceit and attachment in itself.

32
00:03:04,360 --> 00:03:10,720
And of course the mind is always running, running around in circles trying to avoid any

33
00:03:10,720 --> 00:03:17,360
kind of stance on the theory and that you develop a theory as a result.

34
00:03:17,360 --> 00:03:24,400
There's this theory that any stance is untenable, which of course would make sense when

35
00:03:24,400 --> 00:03:30,480
no one has the understanding of reality because all of the people's views in the time were

36
00:03:30,480 --> 00:03:38,280
based simply on their partial or incorrect understanding of reality.

37
00:03:38,280 --> 00:03:44,160
And once you find the truth and they would avoid even that and they would avoid committing

38
00:03:44,160 --> 00:03:46,640
even to the truth.

39
00:03:46,640 --> 00:03:50,760
And so at first they thought this was kind of a neat idea and so they practiced it and

40
00:03:50,760 --> 00:03:54,600
when they realized that it was really quite superficial and shallow and simple actually

41
00:03:54,600 --> 00:04:00,120
all you have to do is just avoid everything, avoid answering these difficult questions

42
00:04:00,120 --> 00:04:02,280
and there you have this teaching.

43
00:04:02,280 --> 00:04:06,720
So they left it behind as well and they went to find another teacher and then it so

44
00:04:06,720 --> 00:04:12,360
happened that one of them, Upatissa, their names were Upatissa and Colita, Upatissa one

45
00:04:12,360 --> 00:04:18,640
day he saw one of the Buddha's disciples walking for arms, it was one of the first five

46
00:04:18,640 --> 00:04:24,480
disciples of the Buddha and it had such a profound effect on him, it's just seeing this

47
00:04:24,480 --> 00:04:30,400
monk walking on arms because obviously this was an enlightened being and so the way he

48
00:04:30,400 --> 00:04:37,240
walked, the way he held his robe, the way he held his bull, the way he looked, the way he

49
00:04:37,240 --> 00:04:41,320
would talk, the way he would keep silent, everything about the way he acted his whole

50
00:04:41,320 --> 00:04:45,400
department said that there was something special about him, it was just so natural and

51
00:04:45,400 --> 00:04:54,640
so clear and so perfect and it wasn't that he was had some heavy kind of focused or

52
00:04:54,640 --> 00:05:03,440
forced way of behaving, it was that it was very natural, very fluid and it spoke volumes

53
00:05:03,440 --> 00:05:06,320
about his inner character.

54
00:05:06,320 --> 00:05:13,160
So he took this as a sign and decided that he would go up and talk to this monk.

55
00:05:13,160 --> 00:05:16,360
He waited for him to finish arms and finish eating and then he asked him, he came up

56
00:05:16,360 --> 00:05:20,320
to him and asked him and said, venerable sir please tell me who is your teacher and what

57
00:05:20,320 --> 00:05:28,720
is his teaching and the monk as he was his name said, my teacher is the great Buddha

58
00:05:28,720 --> 00:05:33,320
and the full enlightened Buddha and he said and his, as for his teaching he said, I've

59
00:05:33,320 --> 00:05:38,960
only recently gone forth, I'm a new, I'm new to this, of course the Buddha's teaching

60
00:05:38,960 --> 00:05:44,800
was new so there were, he was one of the seniors actually but he was still, it was true,

61
00:05:44,800 --> 00:05:48,320
he was still relatively new and he said, so he said to be very difficult for me to teach

62
00:05:48,320 --> 00:05:53,080
it in depth and of course this was a reasonable thing to say but it also had a profound

63
00:05:53,080 --> 00:06:00,400
effect on a participant because it made him think wow if this is what a novice acts like

64
00:06:00,400 --> 00:06:05,360
and then I wonder what it would be like to be someone who fully understands the teaching.

65
00:06:05,360 --> 00:06:09,920
So he said please give me whatever you know because obviously you know something so please

66
00:06:09,920 --> 00:06:16,400
give me the essence whether it be a lot, what you survive for great words and long speeches

67
00:06:16,400 --> 00:06:19,280
just give me the essence.

68
00:06:19,280 --> 00:06:25,320
So I said you gave him the essence and it's so, so core that it's, it's very difficult

69
00:06:25,320 --> 00:06:29,920
for us to understand and even more difficult to understand how such a simple teaching

70
00:06:29,920 --> 00:06:33,760
could have the effect that it did have, have one in Patisa.

71
00:06:33,760 --> 00:06:39,640
The verse goes that the verse that he gave that he said to have given to Upatisa wise,

72
00:06:39,640 --> 00:07:07,480
which means, which means, these things which arise based on the cause, the cause of those

73
00:07:07,480 --> 00:07:17,280
dumbers of those things is what the, the tatagata has taught, the tatagata has taught us

74
00:07:17,280 --> 00:07:19,480
the causes of those things.

75
00:07:19,480 --> 00:07:20,480
This is the first part.

76
00:07:20,480 --> 00:07:27,560
Now when he finished this first part which is half of the verse, he, Upatisa became a

77
00:07:27,560 --> 00:07:32,480
soda planet, he realized the truth just from half of it.

78
00:07:32,480 --> 00:07:38,960
Most of us we listen to this and it's incomprehensible, okay, yeah, those dumbers that

79
00:07:38,960 --> 00:07:44,680
arise based on the cause, the Buddha taught, the cause of those dumbers.

80
00:07:44,680 --> 00:07:50,920
It seems so simple and it'd be very difficult, it's, it's near impossible for us to imagine

81
00:07:50,920 --> 00:07:55,000
how it could lead someone to become enlightened and I just assume that most people listening

82
00:07:55,000 --> 00:07:58,480
probably didn't become enlightened just hearing that.

83
00:07:58,480 --> 00:08:07,640
The second part, they sent to Yo Niro Dotha, Iwanwadimasa, and the cessation of those dumbers.

84
00:08:07,640 --> 00:08:13,760
The great summoner, the great sage of the great records, has, this is his teaching.

85
00:08:13,760 --> 00:08:17,560
His teaching is the cessation of those, those dumbers.

86
00:08:17,560 --> 00:08:22,040
So actually, it, what it, what it's, is teaching here is the four noble truths that

87
00:08:22,040 --> 00:08:28,240
he learned from the Buddha and the original teaching and it's, it's so, it's so simple

88
00:08:28,240 --> 00:08:32,680
and maybe the idea is to humble the Buddhism because a sage he may not know that he would

89
00:08:32,680 --> 00:08:40,200
be able to understand it, but it's also so core that Yo Niro Dotha was able to grasp what

90
00:08:40,200 --> 00:08:45,960
is essential and he was able to see that actually this is the, the essential is that

91
00:08:45,960 --> 00:08:51,920
suffering has the cause, that the problem in life or all of our questions can be answered

92
00:08:51,920 --> 00:08:56,880
based on cause and effect, that every problem that I have and this is, you know, based

93
00:08:56,880 --> 00:09:00,960
on our method, based on his meditation, it wasn't based simply on thinking, but when he

94
00:09:00,960 --> 00:09:05,360
looked inside, he was able to see all of the suffering, all of the delusion, all of the

95
00:09:05,360 --> 00:09:10,720
passion, all of the hatred and all of the conceits and problems that we have inside, they

96
00:09:10,720 --> 00:09:15,240
all have to do with cause and effect and when you see cause and effect, you see this leads

97
00:09:15,240 --> 00:09:20,000
to this leads to that, you're able to break it apart, when you look and at the things

98
00:09:20,000 --> 00:09:25,520
you're looking at, you see how they, they react with each other, you're, then, then you're

99
00:09:25,520 --> 00:09:30,840
able to change that, you're able to refine your behavior as a result and more importantly

100
00:09:30,840 --> 00:09:35,480
is the second part where it talks about the cessation, that actually what the Buddha focused

101
00:09:35,480 --> 00:09:39,920
on was the cessation of these dumbness, that actually there is no realization that there

102
00:09:39,920 --> 00:09:45,440
is nothing that is going to bring true peace and happiness, there's no one reality and

103
00:09:45,440 --> 00:09:49,400
so when you give them up, when you let go of them, that's true peace and true happiness.

104
00:09:49,400 --> 00:09:54,400
This is the meaning of it, very difficult for us to understand, but for what you say

105
00:09:54,400 --> 00:09:59,040
was actually quite simple, he had been training for a long time, in fact the story goes

106
00:09:59,040 --> 00:10:05,000
that he had gone throughout India or throughout that part of India and he would go up to

107
00:10:05,000 --> 00:10:08,520
teachers and ask them questions and he would answer all of their questions, but they

108
00:10:08,520 --> 00:10:12,960
couldn't answer his questions and so he was never able to find a teacher, so he was

109
00:10:12,960 --> 00:10:21,280
actually quite keen, he was able to debate with anyone and to best anyone in an argument,

110
00:10:21,280 --> 00:10:26,560
so so he had a very keen mind and that's why he was able to, he was right ready to accept

111
00:10:26,560 --> 00:10:31,800
this and when he tried to find fault with it and tried to examine it in terms of his own

112
00:10:31,800 --> 00:10:37,800
experience, he actually allowed him to open up and to become free.

113
00:10:37,800 --> 00:10:43,600
So immediately he said stop, that's enough, thank you, please tell me where is our teacher,

114
00:10:43,600 --> 00:10:50,600
at this point he had no doubt and Asaji said he's staying in Wailuana, which is Bambugu.

115
00:10:50,600 --> 00:10:55,120
So immediately Upatissa went immediately back to where his friend was staying and his

116
00:10:55,120 --> 00:11:00,560
friend Coleeta saw him coming and knew right away just as Upatissa had known that this

117
00:11:00,560 --> 00:11:03,640
was an enlightened being this is something of special and so he said something is different

118
00:11:03,640 --> 00:11:08,880
with my friend, he said please tell me Upatissa have you found the truth and Upatissa said

119
00:11:08,880 --> 00:11:14,240
yes I found the truth, he said please share it with me and he told him the same verse

120
00:11:14,240 --> 00:11:19,080
and as a result of this teaching Coleeta as well was able to become a Sotapan which is the

121
00:11:19,080 --> 00:11:24,040
first stage of enlightenment.

122
00:11:24,040 --> 00:11:29,000
And so he said let's go, we have to know and meet our teacher now and Upatissa said

123
00:11:29,000 --> 00:11:32,800
but first we should go and see our old teacher in giving the opportunity to hear this

124
00:11:32,800 --> 00:11:39,520
as well at a perspective for his patronage and they they discussed it and Upatissa said

125
00:11:39,520 --> 00:11:45,280
you know I really think that he'll be able to understand it and appreciate it and if

126
00:11:45,280 --> 00:11:49,200
he does appreciate it it will be able to come with us very quickly realize the truth as

127
00:11:49,200 --> 00:11:50,560
we have.

128
00:11:50,560 --> 00:11:55,120
So they went to see Sanjayan and it was not really as they expected they said they explained

129
00:11:55,120 --> 00:11:59,160
the case to him and Sanjay said oh yes go ahead he said well come with us they said

130
00:11:59,160 --> 00:12:05,800
well come with us you can come and learn as well and Sanjay has said my son's how can

131
00:12:05,800 --> 00:12:11,760
you say such a thing look here I am a great teacher I am my own students and I am famous

132
00:12:11,760 --> 00:12:18,560
you want me to go and become a student again something very big and some very big and important

133
00:12:18,560 --> 00:12:26,320
like myself to go like that he said it's such a thing as impossible and and Upatissa said

134
00:12:26,320 --> 00:12:29,920
that's really not the point you know it really does it really matter that these things

135
00:12:29,920 --> 00:12:36,080
really have any meaning what is essential is that we've now found the truth and Sanjay

136
00:12:36,080 --> 00:12:42,600
has said look let me put it to you this way he said are there more are there more wise

137
00:12:42,600 --> 00:12:49,320
people or more foolish people in the world and Upatissa said or Upatissa and Kolkita

138
00:12:49,320 --> 00:12:56,120
or whoever said oh surely the foolish people are far more the one number wise people in

139
00:12:56,120 --> 00:13:02,440
the world are very few and Sanjay has said well then my sons you go and be with the

140
00:13:02,440 --> 00:13:08,440
wise Gautama which is the family name of the Buddha and all of the foolish people will

141
00:13:08,440 --> 00:13:15,200
come to me and I will be very famous and I have a great following just what he said

142
00:13:15,200 --> 00:13:19,160
and Upatissa and Kolkita you know it was like the last try really you know broke their

143
00:13:19,160 --> 00:13:23,520
faith in their teacher entirely and they said you know well in that case well that's

144
00:13:23,520 --> 00:13:29,120
but by the time to part ways he said how can you know this is crazy if that's all

145
00:13:29,120 --> 00:13:33,760
that's important to you and that you should be surrounded by fools instead of be with

146
00:13:33,760 --> 00:13:39,360
a few wise the few wise people then let's go so they left and what happened when they

147
00:13:39,360 --> 00:13:43,880
left interestingly enough is that Sanjay lost all of his followers that he thought

148
00:13:43,880 --> 00:13:48,720
as a result of his teaching something foolish he would gain a lot of followers what

149
00:13:48,720 --> 00:13:55,200
would happen is actually most of them left with Sankholita and as a result he got

150
00:13:55,200 --> 00:14:01,800
very angry and upset and got very sick and it said to have vomited blood as a result

151
00:14:01,800 --> 00:14:06,560
that's what the story says so Upatissa and Kolkita went to see the Buddha and when they

152
00:14:06,560 --> 00:14:12,320
went to see the Buddha they they got in they got ordained by the Buddha and then eventually

153
00:14:12,320 --> 00:14:16,120
they became enlightened it took them longer than actually most other of the Buddha's

154
00:14:16,120 --> 00:14:22,080
disciples for Kolkita it took him seven days for Upatissa it took him 15 days which

155
00:14:22,080 --> 00:14:28,440
you know considering their perfection of mine is quite a long time but the commentary

156
00:14:28,440 --> 00:14:33,000
says this is because of the profound view of their wish in the past that in past legs

157
00:14:33,000 --> 00:14:37,520
they had made wishes to become the Buddha's chief disciples and so as a result they had

158
00:14:37,520 --> 00:14:43,640
to do all their mind was set on that and they had to therefore go through a very profound

159
00:14:43,640 --> 00:14:50,020
understanding of reality before they could come to their final liberation especially

160
00:14:50,020 --> 00:14:54,680
Upatissa it took him longer now their names changed it Upatissa became sorry put his mother

161
00:14:54,680 --> 00:14:59,880
was sorry so they called him sorry put them Mogulana I believe the village of the family

162
00:14:59,880 --> 00:15:04,320
was called Mogulana so they called him Mogulana I moved the family so they became sorry

163
00:15:04,320 --> 00:15:11,560
put them Mogulana they're very famous in Buddhist circles very well known now after they

164
00:15:11,560 --> 00:15:15,560
became enlightened they they happened to be discussing or they went to ask the Buddha to

165
00:15:15,560 --> 00:15:21,200
talk to him and they said you know look memorable sir we were when we came when we wanted

166
00:15:21,200 --> 00:15:25,800
to come see you we told we told our teacher and this is what he said he said that he would

167
00:15:25,800 --> 00:15:31,680
stay with the fools and that therefore be very famous and you can go with with the Buddha

168
00:15:31,680 --> 00:15:37,520
Gautama and be surrounded by just a few of the wise people and therefore be relatively

169
00:15:37,520 --> 00:15:43,160
insignificant and this is when the Buddha gave this verse he said asari sara matim

170
00:15:43,160 --> 00:15:51,880
so the person who so this is what happens a person who who dwells on what is unessential

171
00:15:51,880 --> 00:15:57,280
they'll never come to the truth but for a person who does see what is essential as essential

172
00:15:57,280 --> 00:16:02,560
and what is unessential as an essential such a person does come to the truth it has to

173
00:16:02,560 --> 00:16:08,560
do with right understanding and obviously Sanjay I had a very wrong understanding now

174
00:16:08,560 --> 00:16:13,600
what this verse teaches us it teaches us many things on many levels the first thing that

175
00:16:13,600 --> 00:16:19,560
it teaches us is about our lives and how we often dwell on things that are unessential

176
00:16:19,560 --> 00:16:25,280
the example of Kolkata Nupatissa is a good one they were living a life of luxury and opulence

177
00:16:25,280 --> 00:16:31,480
and great sensual pleasure so they would go to these shows and it said that at the show

178
00:16:31,480 --> 00:16:36,200
they would have so much fun and be entertained until one day they just realized how pointless

179
00:16:36,200 --> 00:16:42,480
it was and most of us have very different a very difficult time coming to this realization

180
00:16:42,480 --> 00:16:46,960
we might have great suffering and depression and sadness at times in our lives but we're

181
00:16:46,960 --> 00:16:52,640
not able to make the connection we still are living in under the misguided belief that

182
00:16:52,640 --> 00:16:57,920
somehow this sensual enjoyment is going to bring us someday or somehow it's going to bring

183
00:16:57,920 --> 00:17:02,640
us true peace and happiness or we believe that some things just wrong with us the fact

184
00:17:02,640 --> 00:17:08,240
that we can't find happiness in this way is means there's something wrong so often we'll

185
00:17:08,240 --> 00:17:12,840
take medication for our depression or so on thinking that somehow as a result we're going

186
00:17:12,840 --> 00:17:16,920
to be happy and enjoy all of these things that aren't really bringing us true peace

187
00:17:16,920 --> 00:17:21,600
and happiness sorry put in Mogulana we're able to see through that and they were able to

188
00:17:21,600 --> 00:17:25,840
see that you know and it's it's a difficult thing to see because there's so much pressure

189
00:17:25,840 --> 00:17:31,120
on from society society says this is good this is right this is the sort of thing that

190
00:17:31,120 --> 00:17:37,880
we're supposed to enjoy so this is something that we should take to heart and to use to

191
00:17:37,880 --> 00:17:42,240
help to change our view and our understanding that actually it's true that we're not

192
00:17:42,240 --> 00:17:46,240
going to find true peace and happiness through these things and if we expect to we're

193
00:17:46,240 --> 00:17:50,000
only going to be disappointed again and again and eventually we meet with the ultimate

194
00:17:50,000 --> 00:17:54,160
disappointment when we have to leave it all behind and realize that we've wasted our whole

195
00:17:54,160 --> 00:17:59,880
lives on things which are unessential and useless as a result will never come to the truth

196
00:17:59,880 --> 00:18:04,000
will die without coming to the truth and this is how people live their lives we waste our

197
00:18:04,000 --> 00:18:07,640
time so it's important that we should remember the Buddha's teaching not to let the

198
00:18:07,640 --> 00:18:14,600
moment pass us by don't waste your time in this life that we have the second the second

199
00:18:14,600 --> 00:18:20,000
thing it has to teach us is in regards to the unessential and in the religious path now

200
00:18:20,000 --> 00:18:24,480
Sanjay I was someone who you could expect had had gained some of this realization and

201
00:18:24,480 --> 00:18:28,920
therefore had left the home life he may have done it for the wrong reasons but it's possible

202
00:18:28,920 --> 00:18:33,040
that he left the home life on to good intentions trying to find the truth the problem is

203
00:18:33,040 --> 00:18:38,960
that in the end he hit upon that which was totally unessential and he got caught up in what

204
00:18:38,960 --> 00:18:48,840
we say are the the locomotives he got caught up in fame and praise and wealth and pleasure

205
00:18:48,840 --> 00:18:53,720
you know all of these things that he got from being a great teacher and as a result he

206
00:18:53,720 --> 00:18:56,840
saw that these were essential and he thought it was essential to have lots of students

207
00:18:56,840 --> 00:19:00,960
and the be famous and that this was the most important even to the point where he thought

208
00:19:00,960 --> 00:19:07,240
was better to be surrounded by foolish people to be surrounded by wise people which he figured

209
00:19:07,240 --> 00:19:15,720
would be a lot fewer and so you find this often or you do find this in religious circles

210
00:19:15,720 --> 00:19:21,520
and in terms of religious teachers that often people get caught up in teaching and put

211
00:19:21,520 --> 00:19:27,280
aside their own practice or that people get caught up in you see many many people who put on

212
00:19:27,280 --> 00:19:32,840
this robe and then got caught up in as I said before got up in fame got caught up in wealth

213
00:19:32,840 --> 00:19:37,840
and we talked about this in the verses when they were at that it's very easy to get caught

214
00:19:37,840 --> 00:19:42,520
up in these things and you see monks even with the best intentions will eventually get caught

215
00:19:42,520 --> 00:19:50,360
up in luxury and so on and then they need this and that and a nice bed and nice good food

216
00:19:50,360 --> 00:19:54,560
and they'll often start to obsess over these things and they won't they won't be able

217
00:19:54,560 --> 00:19:58,680
to see that these are on essential people who worry become a monk and then worry about

218
00:19:58,680 --> 00:20:05,560
their health and worry about their food who worry about their bedding and so on you know

219
00:20:05,560 --> 00:20:10,360
I've been through all that but I think if you practice meditation you should be able

220
00:20:10,360 --> 00:20:14,560
to overcome it and there were times where I said to myself you know I don't have I don't

221
00:20:14,560 --> 00:20:19,440
even have I didn't even have soap or I didn't have laundry powder and these things just

222
00:20:19,440 --> 00:20:26,240
came it just was such a shock to me it was so such a stress that I didn't have these

223
00:20:26,240 --> 00:20:29,680
things what was I going to do and I said well well then I realized well then I just

224
00:20:29,680 --> 00:20:35,440
won't wash and I won't wash my clothes that's the answer and so I was able to let go of

225
00:20:35,440 --> 00:20:40,400
it and realized that that's really unessential all of these things are in essential when

226
00:20:40,400 --> 00:20:44,480
some days we will go hungry and then you think oh no what a terrible thing what am I

227
00:20:44,480 --> 00:20:49,120
going to do then the answer is why you're going to be hungry that's it really doesn't

228
00:20:49,120 --> 00:20:54,960
mean anything maybe you'll get sick maybe you'll die doesn't really mean anything and when

229
00:20:54,960 --> 00:21:00,480
you put your emphasis on these things only I'll be healthy only I'll be have a full stomach

230
00:21:00,480 --> 00:21:07,200
only I'll be alive even when you when this is your your essence you've missed the point

231
00:21:08,320 --> 00:21:13,200
what is the essence according to the Buddha there are five things and and even the third thing

232
00:21:13,200 --> 00:21:18,240
the teachers is that even in regards to what is essential we have to prioritize or we have to be

233
00:21:18,240 --> 00:21:24,080
complete so the Buddha would always give talks on what is the essence what is the core and there are

234
00:21:24,080 --> 00:21:34,720
five candidates there's morality concentration wisdom release or freedom and the awareness or the

235
00:21:34,720 --> 00:21:41,200
knowledge of freedom and we need all five of these some people take up some people are able to put

236
00:21:41,200 --> 00:21:51,120
aside the worldly dominant worldly pursuits and pursue after at least morality and so as a result

237
00:21:51,120 --> 00:21:56,000
they're able to keep morality they don't touch money they don't eat at the wrong they only eat

238
00:21:56,000 --> 00:22:01,600
once a day and they wear robes and so on they even maybe wear rag robes they're able to do all

239
00:22:01,600 --> 00:22:09,280
these these these practices of morality but they become considered as a result and they see that

240
00:22:09,280 --> 00:22:13,760
as the essence and the Buddha said no this is not the essence this isn't the core it's part of the

241
00:22:13,760 --> 00:22:18,960
essence but it's only the first part if you don't go any further you you still haven't found the essence

242
00:22:19,840 --> 00:22:25,200
so next some people develop concentration so they're able to focus their mind and fix their minds

243
00:22:25,200 --> 00:22:31,200
on one object their minds become very quiet and very focused they can even gain magical powers

244
00:22:31,200 --> 00:22:36,800
they were that they himself had a very focused mind and even gain magical powers at some point

245
00:22:36,800 --> 00:22:41,280
he was able to apparently having flight through the air he was able to change his appearance so

246
00:22:41,280 --> 00:22:49,280
people he looked different to people and so on but so obviously this is not the essence and it

247
00:22:49,280 --> 00:22:54,560
leads people like David that did they become considered and therefore what doesn't lead them

248
00:22:54,560 --> 00:22:59,200
people can still become conceited based on these things and if they have conceived inside and

249
00:22:59,200 --> 00:23:03,600
haven't gone further then they become conceited and they'll use them for the wrong purposes

250
00:23:03,600 --> 00:23:09,360
the next one is wisdom no even people who gain some wisdom in inside can become considered if

251
00:23:09,360 --> 00:23:14,880
they don't get to the to the liberation or freedom their mind was there was to become considered

252
00:23:14,880 --> 00:23:19,120
and they'll believe that they're enlightened just from the insights that they've gained

253
00:23:19,120 --> 00:23:25,360
people who practice even inside meditation if they haven't realized the the the perfect truth

254
00:23:25,360 --> 00:23:29,440
if their minds haven't become free they've liberated they haven't realized in bananas

255
00:23:29,440 --> 00:23:36,640
then they will still fall into conceit and they're still wrong understanding and the wrong

256
00:23:36,640 --> 00:23:43,280
beliefs that they are enlightened as a result they will give up the practicing and think that

257
00:23:43,280 --> 00:23:50,720
they've done good enough and will feel content with what they've gained so this is also not

258
00:23:50,720 --> 00:23:55,680
the essence but finally is the fourth and the fifth one and really they come together but they are

259
00:23:55,680 --> 00:24:01,040
separate things the fourth one is release so when a person does become released in free and

260
00:24:01,040 --> 00:24:07,200
enters into the band over the mind there's no longer coming out there's no longer seeing or hearing

261
00:24:07,200 --> 00:24:14,000
or smelling or tasting or feeling thinking the mind goes inside really and for that time doesn't

262
00:24:14,000 --> 00:24:19,360
come out that there is no experience of seeing hearing the spelling tasting thinking and that's

263
00:24:19,360 --> 00:24:25,600
true release and freedom and the mind has just found its center really and it's not it's not wavering

264
00:24:25,600 --> 00:24:30,960
and at that point the mind is free and this is called freedom this is really the essence and

265
00:24:30,960 --> 00:24:35,440
really there's nothing more that needs to be done but the next the the fifth one is just this

266
00:24:35,440 --> 00:24:41,040
the aspect of coming out again and realizing that what it is that you've experienced and to

267
00:24:41,040 --> 00:24:45,440
actually start thinking about it and the realization that you've become free this Buddhist and

268
00:24:45,440 --> 00:24:52,400
this is the greatest benefit the greatest essence once you become freely the life that you live in

269
00:24:52,400 --> 00:24:59,360
and the existence as someone who is free as someone who has seen the truth and as a result doesn't

270
00:24:59,360 --> 00:25:06,160
have the cringing clinging in regards to that which is unessential or in regards to anything that

271
00:25:06,160 --> 00:25:11,680
is a reason all of the arisen does because you've seen the cessation yay dhamma heath upon the

272
00:25:11,680 --> 00:25:18,000
life you've seen all of these arisen things they center yon yon yon yon you've seen their cessation

273
00:25:18,000 --> 00:25:24,160
so this is the essence and the person shouldn't be inclined towards these things this is really

274
00:25:24,160 --> 00:25:28,880
what we're aiming for and Buddhism and this is kind of a teaching for us to see what is

275
00:25:30,560 --> 00:25:35,680
to see what in our lives maybe we are clinging on to that is unessential and to remind us

276
00:25:35,680 --> 00:25:40,880
of what is truly essential and how we should give up what is unessential because it winds up

277
00:25:40,880 --> 00:25:46,640
it leads us to a waste our times and to lose the opportunity that we have in this life

278
00:25:46,640 --> 00:25:51,920
to put into practice those teachings that lead us to realization of the truth and true peace

279
00:25:51,920 --> 00:25:57,440
happiness and freedom from suffering so that's our verses for today thank you for tuning in

280
00:25:57,440 --> 00:26:21,200
and wish you all the best

