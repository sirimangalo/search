1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapanda.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse 162 which reads as follows.

3
00:00:12,000 --> 00:00:20,000
Yes, I attend to do see the young Malu as Salami told the town.

4
00:00:20,000 --> 00:00:28,000
Salami wrote the town.

5
00:00:28,000 --> 00:00:45,000
Yes, I attend to do see the young.

6
00:00:45,000 --> 00:01:00,000
The person whose evil behavior knows no bound or is crossed the line.

7
00:01:00,000 --> 00:01:10,000
Malu as Salami told the town like a vine that covers over a great south tree.

8
00:01:10,000 --> 00:01:19,000
Sala tree is very tall trees.

9
00:01:19,000 --> 00:01:25,000
Garote so that satan, the such a person does to themselves.

10
00:01:25,000 --> 00:01:29,000
The atanang, it's a deed.

11
00:01:29,000 --> 00:01:38,000
Just as an enemy would wish for them.

12
00:01:38,000 --> 00:01:45,000
So this verse was taught in relation to David Atan.

13
00:01:45,000 --> 00:01:48,000
The text we have is quite short.

14
00:01:48,000 --> 00:01:56,000
The text, the commentary around this verse is simply that the monks were sitting around

15
00:01:56,000 --> 00:02:04,000
in Wailu on the bamboo grove.

16
00:02:04,000 --> 00:02:08,000
Talking about how evil David Atan was.

17
00:02:08,000 --> 00:02:12,000
David Atan was the Buddhist cousin.

18
00:02:12,000 --> 00:02:14,000
He's the epitome of a bad monk.

19
00:02:14,000 --> 00:02:21,000
Some of his bad is against.

20
00:02:21,000 --> 00:02:23,000
He did a lot of bad things.

21
00:02:23,000 --> 00:02:29,000
So basically they're talking about this and can't believe how far he's gone.

22
00:02:29,000 --> 00:02:36,000
And the Buddha came and asked him, hey, what are you all talking about?

23
00:02:36,000 --> 00:02:42,000
And they told him and the Buddha said, oh, this is the first time.

24
00:02:42,000 --> 00:02:49,000
David Atan has done great, great evil in the past as well.

25
00:02:49,000 --> 00:02:51,000
He did many evil things.

26
00:02:51,000 --> 00:02:56,000
He told many jotica, he told some jotica stories.

27
00:02:56,000 --> 00:02:59,000
There's a lot of jotica stories around David Atan.

28
00:02:59,000 --> 00:03:03,000
A lot of the jotica stories are about the Buddha's greatness.

29
00:03:03,000 --> 00:03:12,000
And often he needs the foil of David Atan to show how great he is in the face of great evil,

30
00:03:12,000 --> 00:03:17,000
how he reacts as a contrast.

31
00:03:17,000 --> 00:03:27,000
So there's 20, 30, 40, there's many, many jotica's.

32
00:03:27,000 --> 00:03:31,000
There's at least 20 or 30 jotica's with David Atan and probably more.

33
00:03:31,000 --> 00:03:34,000
But what is it that David Atan did?

34
00:03:34,000 --> 00:03:40,000
And so it mentions a few things in this story.

35
00:03:40,000 --> 00:03:45,000
So if we're not going to go back, I won't go through a whole list of all the things,

36
00:03:45,000 --> 00:03:47,000
but just briefly to go over it.

37
00:03:47,000 --> 00:03:50,000
In his past lives, he did many evil deals.

38
00:03:50,000 --> 00:03:56,000
He was constantly harassing the Buddha, trying to kill him and killing him.

39
00:03:56,000 --> 00:03:58,000
And other people as well.

40
00:03:58,000 --> 00:04:01,000
David Atan was the evil one.

41
00:04:01,000 --> 00:04:05,000
In the jotica that I told about these two dears,

42
00:04:05,000 --> 00:04:09,000
the two deer, the two heads of the tribes of deer.

43
00:04:09,000 --> 00:04:14,000
David Atan was the one that told this deer that she had to go.

44
00:04:14,000 --> 00:04:20,000
This pregnant deer that she had to go to sacrifice herself,

45
00:04:20,000 --> 00:04:23,000
and the Buddha on the other hand took her place.

46
00:04:23,000 --> 00:04:26,000
And she went to the Buddha and said, oh, you're pregnant.

47
00:04:26,000 --> 00:04:30,000
I said, go back and give birth freely.

48
00:04:30,000 --> 00:04:31,000
Don't worry.

49
00:04:31,000 --> 00:04:39,000
You're your turn has been passed.

50
00:04:39,000 --> 00:04:42,000
I don't even mean to himself.

51
00:04:42,000 --> 00:04:45,000
And there's this story of...

52
00:04:45,000 --> 00:04:50,000
He's fairly prominent in this story of Mahosadat,

53
00:04:50,000 --> 00:04:52,000
which is the longest of the jotica.

54
00:04:52,000 --> 00:05:00,000
It's this long story about how this young boy becomes, anyway.

55
00:05:00,000 --> 00:05:03,000
He ends up dealing...

56
00:05:03,000 --> 00:05:06,000
It's becoming an advisor to the king.

57
00:05:06,000 --> 00:05:10,000
And these other king comes and the other king's advisors,

58
00:05:10,000 --> 00:05:13,000
David Atan, in the past day.

59
00:05:13,000 --> 00:05:17,000
But talking about this life, it's important we know

60
00:05:17,000 --> 00:05:20,000
if you're interested in Buddhists, the background of Buddhists,

61
00:05:20,000 --> 00:05:24,000
culture and religion.

62
00:05:24,000 --> 00:05:28,000
It's important to know about David Atan.

63
00:05:28,000 --> 00:05:33,000
So, it started off, he became a monk,

64
00:05:33,000 --> 00:05:37,000
along with the Buddha's other cousins and relatives.

65
00:05:37,000 --> 00:05:41,000
And the other relatives of the Buddha all became

66
00:05:41,000 --> 00:05:43,000
at least a sotepanda, generally sotepanda,

67
00:05:43,000 --> 00:05:46,000
and then they were working to get to the higher stages.

68
00:05:46,000 --> 00:05:48,000
David Atan didn't get anything.

69
00:05:48,000 --> 00:05:53,000
What he gained from his practice were the Johnus and magical powers.

70
00:05:53,000 --> 00:05:57,000
So, he was able to calm his mind like the other monks,

71
00:05:57,000 --> 00:05:58,000
but he wasn't able to go further.

72
00:05:58,000 --> 00:06:01,000
There's too much evil in his heart, too much.

73
00:06:01,000 --> 00:06:05,000
There's too rotten from all the many lifetimes of evil,

74
00:06:05,000 --> 00:06:07,000
no matter how hard he tried.

75
00:06:07,000 --> 00:06:13,000
And so, he gained magical powers through his practice,

76
00:06:13,000 --> 00:06:18,000
and he was able to apparently change his appearance and so on,

77
00:06:18,000 --> 00:06:21,000
fly through the air.

78
00:06:21,000 --> 00:06:25,000
Lots of neat stuff that he got caught up in and intoxicated by,

79
00:06:25,000 --> 00:06:39,000
and thought somehow he was special because of him.

80
00:06:39,000 --> 00:06:42,000
And then at some point it all came to a head.

81
00:06:42,000 --> 00:06:48,000
He was acting as an ordinary monk and practicing in meditation,

82
00:06:48,000 --> 00:06:51,000
but at some point it got too much from,

83
00:06:51,000 --> 00:06:55,000
I think it's the Buddha's own fame and glory.

84
00:06:55,000 --> 00:06:59,000
It was spread far and wide, everyone was talking about how great the Buddha was,

85
00:06:59,000 --> 00:07:03,000
how great, sorry, put the wise, how great Morgana was.

86
00:07:03,000 --> 00:07:09,000
And there wasn't a lot of greatness about the spread about David Atan,

87
00:07:09,000 --> 00:07:10,000
because he wasn't on it.

88
00:07:10,000 --> 00:07:19,000
Great, and he was jealous of the praise bestowed upon the other monks.

89
00:07:19,000 --> 00:07:23,000
And so, he thought to himself, well, all the chief greatness.

90
00:07:23,000 --> 00:07:26,000
And he went to Ajata Satu.

91
00:07:26,000 --> 00:07:28,000
There's the prince.

92
00:07:28,000 --> 00:07:31,000
The son of Vimbisara, Vimbisara was a follower of the Buddha.

93
00:07:31,000 --> 00:07:33,000
Vimbisara would have nothing to do with David Atan,

94
00:07:33,000 --> 00:07:35,000
but Ajata Satu didn't.

95
00:07:35,000 --> 00:07:37,000
It was a young boy and didn't know anything.

96
00:07:37,000 --> 00:07:40,000
And in fact, Ajata Satu had bad karma as well.

97
00:07:40,000 --> 00:07:44,000
He was destined to kill his father.

98
00:07:44,000 --> 00:07:51,000
He had a bad Upanishaya, his past was really good with bad as well.

99
00:07:51,000 --> 00:07:54,000
And so, they got along quite well together.

100
00:07:54,000 --> 00:07:57,000
David Atan pressed him with his magical powers,

101
00:07:57,000 --> 00:08:03,000
and Ajata Satu was supported David Atan by sending him

102
00:08:03,000 --> 00:08:08,000
an opulent and extravagant meals.

103
00:08:08,000 --> 00:08:15,000
And as a result of these meals, and the support David Atan was able to become big and famous.

104
00:08:15,000 --> 00:08:19,000
And they made a pact that Ajata Satu,

105
00:08:19,000 --> 00:08:21,000
given that they said to Ajata Satu,

106
00:08:21,000 --> 00:08:24,000
you kill your father and become king.

107
00:08:24,000 --> 00:08:26,000
If you kill your father, you'll become king.

108
00:08:26,000 --> 00:08:29,000
And I'll find some way to kill the Buddha.

109
00:08:29,000 --> 00:08:31,000
Or you help me kill the Buddha.

110
00:08:31,000 --> 00:08:35,000
And then I'll become the head of the Buddha's religion.

111
00:08:35,000 --> 00:08:40,000
I'll be the head of the Buddhists.

112
00:08:40,000 --> 00:08:44,000
And together we were ruled India.

113
00:08:44,000 --> 00:08:47,000
We were ruled the world.

114
00:08:47,000 --> 00:08:52,000
And Ajata Satu, there's a long story about how he had killed his father.

115
00:08:52,000 --> 00:08:59,000
He had him imprisoned and eventually killed.

116
00:08:59,000 --> 00:09:05,000
At the last minute, he became remorseful.

117
00:09:05,000 --> 00:09:09,000
He ran to the prison where his father was being tortured,

118
00:09:09,000 --> 00:09:14,000
only to find that he was just a moment too late as father and just died.

119
00:09:14,000 --> 00:09:20,000
So, as a result, Ajata Satu was effectively a

120
00:09:20,000 --> 00:09:23,000
paptor side.

121
00:09:23,000 --> 00:09:28,000
He killed his father, which is one of the great weighty carmas.

122
00:09:28,000 --> 00:09:29,000
You can't recover.

123
00:09:29,000 --> 00:09:34,000
You can't become enlightened if you've killed your father or your mother.

124
00:09:34,000 --> 00:09:37,000
It's one of those things that leads immediately and directly to hell.

125
00:09:37,000 --> 00:09:40,000
Unfortunately, there's no way to mitigate it.

126
00:09:40,000 --> 00:09:48,000
It's one of those few acts in Buddhist, dogma, doctrine.

127
00:09:48,000 --> 00:09:52,000
David Ata went about trying to kill the Buddha in various ways.

128
00:09:52,000 --> 00:09:55,000
And he succeeded in hurting the Buddha.

129
00:09:55,000 --> 00:10:03,000
He dropped, when the Buddha was walking down the mountain, he dropped a large rock on him.

130
00:10:03,000 --> 00:10:06,000
And the rock somehow, because you can't kill a Buddha.

131
00:10:06,000 --> 00:10:07,000
It's not possible.

132
00:10:07,000 --> 00:10:13,000
The rock went the wrong way and hit another rock and a little splinter flew off and

133
00:10:13,000 --> 00:10:15,000
hit the Buddha's foot.

134
00:10:15,000 --> 00:10:18,000
And they say it caused the blood to rise.

135
00:10:18,000 --> 00:10:24,000
He may not have broken his skin because that might be impossible, but it bruised him perhaps.

136
00:10:24,000 --> 00:10:31,000
And the Buddha had a hard time walking and he laid down and they brought a stretcher farm.

137
00:10:31,000 --> 00:10:36,000
And that's another one of the six weighty carmas.

138
00:10:36,000 --> 00:10:47,000
Once you've tried to try, once you've injured the Buddha, you can never become enlightened.

139
00:10:47,000 --> 00:10:49,000
Not in this lifetime.

140
00:10:49,000 --> 00:11:10,000
So David Ata was doomed at that point, along with it at the set.

141
00:11:10,000 --> 00:11:14,000
There was another time, sorry, one part of the story is where David Ata went to the Buddha

142
00:11:14,000 --> 00:11:19,000
and said, oh, you're getting old, you should hand the Buddhist religion over to me.

143
00:11:19,000 --> 00:11:26,000
And the Buddha said, I wouldn't hand my religion over to Sariput or Mogulana, who are great beings.

144
00:11:26,000 --> 00:11:34,000
Why would I give it to you as though I were throwing away if as though I were vomiting up spittle?

145
00:11:34,000 --> 00:11:49,000
To give it a way to you would be this term, like spittle, it would be treating my religion like experiment to give it to you.

146
00:11:49,000 --> 00:11:53,000
That's all your worth basically is what he's saying.

147
00:11:53,000 --> 00:11:55,000
They got very angry.

148
00:11:55,000 --> 00:11:59,000
So that's when David Ata decided to go to kill to try and kill the Buddha.

149
00:11:59,000 --> 00:12:05,000
It didn't work.

150
00:12:05,000 --> 00:12:12,000
And finally, because he tried to kill the Buddha, I just had to eventually realize that,

151
00:12:12,000 --> 00:12:18,000
hey, this David Ata guy is not really worth following in the end.

152
00:12:18,000 --> 00:12:24,000
It's not really any benefit from following his teachings, probably not the person to follow.

153
00:12:24,000 --> 00:12:27,000
At that point, I just had to start to listen to the Buddha's teachings

154
00:12:27,000 --> 00:12:33,000
and became converted to Buddha's teachings, but it wasn't ever able to become a sotupana

155
00:12:33,000 --> 00:12:35,000
because of killing his father.

156
00:12:35,000 --> 00:12:42,000
Buddha said if he hadn't killed his father, he would have easily become a sotupana.

157
00:12:42,000 --> 00:12:47,000
So David Ata tried then to create a schism in the sangha.

158
00:12:47,000 --> 00:12:50,000
He succeeded in creating a schism in the sangha.

159
00:12:50,000 --> 00:12:53,000
He went to the Buddha and it's a famous, interesting story.

160
00:12:53,000 --> 00:12:59,000
He went to the Buddha and demanded that the Buddha institute five rules.

161
00:12:59,000 --> 00:13:02,000
That monks should always live in the forest.

162
00:13:02,000 --> 00:13:09,000
That they should live only on arms, meaning never go for a meal or accept an invitation.

163
00:13:09,000 --> 00:13:15,000
They should wear only ragrobes and never wear robes that were given to them.

164
00:13:15,000 --> 00:13:19,000
They should dwell only in the foot of a tree and never under a roof.

165
00:13:19,000 --> 00:13:26,000
They should abstain completely from meat.

166
00:13:26,000 --> 00:13:30,000
The Buddha's reply was that you could practice these if you wish,

167
00:13:30,000 --> 00:13:36,000
but he wasn't going to institute it as a requirement because at different times in a different situation.

168
00:13:36,000 --> 00:13:40,000
This is an interesting point because on the face of them they seem like good practices

169
00:13:40,000 --> 00:13:42,000
and they are good practices.

170
00:13:42,000 --> 00:13:46,000
Even the one not even meat, if you don't want to eat meat.

171
00:13:46,000 --> 00:13:54,000
Although he said in regards to that one, he's already made a point that you should not eat meat

172
00:13:54,000 --> 00:13:56,000
if it's been killed for you.

173
00:13:56,000 --> 00:14:02,000
If you know or think or even suspect that it's been killed for you, you shouldn't eat it.

174
00:14:02,000 --> 00:14:09,000
But regarding just eating dead flesh, the Buddha was clearly not against him.

175
00:14:09,000 --> 00:14:18,000
I know there's some controversy among Buddhists over this one, but I don't want to get into it.

176
00:14:18,000 --> 00:14:25,000
At any rate, it's interesting that the Buddha was quite clear that this was going too far to force monks to do these things.

177
00:14:25,000 --> 00:14:29,000
It was good reason.

178
00:14:29,000 --> 00:14:37,000
But David got a chance at this point because of this, he was able to convince new and

179
00:14:37,000 --> 00:14:43,000
inexperienced monks that the Buddha was soft.

180
00:14:43,000 --> 00:14:48,000
The Buddha wasn't willing to institute real ascetic practices.

181
00:14:48,000 --> 00:14:53,000
So he managed to convince a whole large group of monks to go with him

182
00:14:53,000 --> 00:14:59,000
and he set himself up to teach and he started to teach.

183
00:14:59,000 --> 00:15:06,000
Whatever he thought was the Buddha's teaching or whatever he thought was the right teaching.

184
00:15:06,000 --> 00:15:14,000
And the Buddha sent Sariput and Mogulana to win back his followers because these monks just didn't know any better.

185
00:15:14,000 --> 00:15:16,000
They weren't really evil.

186
00:15:16,000 --> 00:15:22,000
And so Sariput and Mogulana went to see David that and David that was their preaching, whatever he was preaching.

187
00:15:22,000 --> 00:15:28,000
And when he saw them come, he thought, ah, Mogulana and Sariput, I have come over to my side.

188
00:15:28,000 --> 00:15:30,000
And he said, come, come, you're welcome.

189
00:15:30,000 --> 00:15:36,000
He said, he said, Sariput the Mogulana, you teach in my state.

190
00:15:36,000 --> 00:15:41,000
I will lie down and listen mindfully.

191
00:15:41,000 --> 00:15:46,000
Because that's what the Buddha would have someone else teach and he would lie down because he'd been teaching

192
00:15:46,000 --> 00:15:48,000
long throughout the night.

193
00:15:48,000 --> 00:15:53,000
And when it became time to lie down, he would lie down and listen mindfully.

194
00:15:53,000 --> 00:16:00,000
And David that, when he lay down, he fell right asleep and started snoring, I guess.

195
00:16:00,000 --> 00:16:07,000
And Sariput and Mogulana at that point had an opportunity and they taught the monks the right path and convinced them all to go back to the Buddha.

196
00:16:07,000 --> 00:16:14,000
When David had to woke up, all of his followers were gone, except for a couple of his cronies, his henchmen.

197
00:16:14,000 --> 00:16:19,000
And one of them promptly kicked David that day in the chest, or he kicked them away, I think.

198
00:16:19,000 --> 00:16:24,000
He kicked him in the chest to wake him up and said, hey, wake up.

199
00:16:24,000 --> 00:16:30,000
I told you not to trust Sariput and Mogulana, now they've taken all your followers back.

200
00:16:30,000 --> 00:16:38,000
And being kicked in the chest apparently, he was quite damaging to Devodata and he got quite sick at that point.

201
00:16:38,000 --> 00:16:44,000
He got sick and he had to lie down, he was vomiting blood.

202
00:16:44,000 --> 00:16:51,000
And suddenly he realized with this bad health, he suddenly realized he was dying.

203
00:16:51,000 --> 00:16:56,000
And it shook him up and he wanted to go back and see the Buddha.

204
00:16:56,000 --> 00:17:04,000
And so he sent message ahead that he was coming to see the Buddha and his followers picked him up on a stretcher and carried him.

205
00:17:04,000 --> 00:17:13,000
And the Buddha heard that he was coming and said, they would have to won't be able to make it to see me not in this life, not afterward he's done.

206
00:17:13,000 --> 00:17:25,000
And David out there, they carried him closer and closer and the monks told the Buddha, oh, he's coming, he's outside of the monastery, he's honest right here.

207
00:17:25,000 --> 00:17:29,000
He's a mile away, that kind of thing.

208
00:17:29,000 --> 00:17:35,000
And the Buddha said, let them bring him right up to the gate of Jitawanda.

209
00:17:35,000 --> 00:17:41,000
There's no way that David out there would be able to see me not in this life, not afterward he's done.

210
00:17:41,000 --> 00:17:47,000
And so the monks brought David out to write up to the monastery, to the pool outside.

211
00:17:47,000 --> 00:17:54,000
And they set him down and they went to refresh themselves in the pool because it was hot and hard carrying him.

212
00:17:54,000 --> 00:18:03,000
And when they set him down, David out there looked and he saw that the gate was right there and he thought he was strong enough to walk the rest away and he stood up.

213
00:18:03,000 --> 00:18:17,000
And as soon as he stood on the ground, the earth sunk beneath his feet and he was swallowed up whole and died about to help.

214
00:18:17,000 --> 00:18:23,000
Apparently this is a thing for great evil, you're actually swallowed up by the earth and died.

215
00:18:23,000 --> 00:18:35,000
The flames, the earth opens up and flames, the flames of hell shoot up and your birth to a crystal.

216
00:18:35,000 --> 00:18:38,000
So that's in very brief the story of David.

217
00:18:38,000 --> 00:18:48,000
That if you want a longer version, a bit of a longer version, you can read in the dictionary of polypropylene.

218
00:18:48,000 --> 00:18:56,000
So that's what they were talking about. So this verse he said, for someone who's gone across the line.

219
00:18:56,000 --> 00:19:04,000
And it's an interesting expression, a gentleman who's seen the young one who's evil,

220
00:19:04,000 --> 00:19:13,000
who's a doucilla, who's bad practice has gone to the extreme or perhaps beyond the extreme, across the line.

221
00:19:13,000 --> 00:19:29,000
And that is the end. So that either means it's gone over the border of what is still healable, still fixable.

222
00:19:29,000 --> 00:19:37,000
Or it means it's gone to the extreme. You can't get any worse than that basically.

223
00:19:37,000 --> 00:19:51,000
So a couple of lessons here, that's the first one, is that there's a difference between evil and a gentle evil that has gone to the extreme or has crossed the line.

224
00:19:51,000 --> 00:20:08,000
And that's important because it's easy to become low self-loathing. Consumed by guilt over how evil will be here about the danger of evil.

225
00:20:08,000 --> 00:20:21,000
And we hear how strongly it is denounced in Buddhism. I mean, that's the focus. The focus in Buddhism is on ethics. There's no focus on God or heaven or rebirth or any of that.

226
00:20:21,000 --> 00:20:33,000
The focus is on good and evil. It's on ethical and unethical states, happiness and suffering as a product of goodness and evil.

227
00:20:33,000 --> 00:20:43,000
And so it's easy to become greatly disturbed by the fact that we still have evil inside and there's merit to that.

228
00:20:43,000 --> 00:20:50,000
And the fact that we are disturbed is not a bad thing. The last thing we would want is to become complacent in our evil.

229
00:20:50,000 --> 00:21:04,000
But it's important to be clear the path out of evil as being the comprehension and the understanding rather than the rejection.

230
00:21:04,000 --> 00:21:15,000
If you just reject something outright, it becomes based on aversion. So the way out of evil is to understand it and to see it clearly.

231
00:21:15,000 --> 00:21:25,000
It seems somewhat counterintuitive. You somehow let the things arise. Let the anger arise. Let the greed arise.

232
00:21:25,000 --> 00:21:43,000
But the alternative is only to get consumed by guilt and self-hatred and so on, which is even worse because it's putting something on top of it. It's not solving the underlying problem. It's just making it more complicated.

233
00:21:43,000 --> 00:22:03,000
And so it's important to have some reassurance that evil is not something that can't be dealt with and be clear that there's a difference between evil and being consumed by evil.

234
00:22:03,000 --> 00:22:18,000
So the key is to not be consumed by it. In fact, we should try it every moment and not to be consumed by it. We're wanting something leads to chasing after it or disliking something, means leads to trying to get rid of it.

235
00:22:18,000 --> 00:22:33,000
We dislike something that disliking like you should focus on, and you want something or like something. It's the wanting and the liking that should be focused on. Should be understood. And for understanding it, you're seeing it objectively.

236
00:22:33,000 --> 00:22:41,000
And you see that it's a cause of stress and suffering. It's not beneficial in any way.

237
00:22:41,000 --> 00:23:01,000
If you let it overcome you, it slowly, slowly begins to lead you on. And this is this great danger of being consumed by it, because worse and worse and worse, until they're blinded by it. You're consumed completely by it.

238
00:23:01,000 --> 00:23:14,000
I think they would have a really interesting thing about David Atta is in the end. Being consumed by evil is such suffering. It's such a terrifying state that in the end you can't help.

239
00:23:14,000 --> 00:23:27,000
Or David Atta couldn't help but realize the error in his ways. And in the very last moment, he made a wish that to take refuge in the Buddha.

240
00:23:27,000 --> 00:23:34,000
He held his hands up and took refuge in the Buddha right before he died.

241
00:23:34,000 --> 00:23:44,000
They say as a result, once he's done burning to a crisp inhale, which is where he is right now. They say, he will be reborn as a particular Buddha.

242
00:23:44,000 --> 00:23:53,000
He will be reborn as a human and become a particular Buddha, and surely a private Buddha, because his mind changed.

243
00:23:53,000 --> 00:24:02,000
So even with great evil, the key really is not to be not evil. It's to be mindful of you.

244
00:24:02,000 --> 00:24:10,000
That's where the change comes about, because then you realize how much suffering comes from who you are, from this evil.

245
00:24:10,000 --> 00:24:19,000
Because the problem is that evil is a habitual. You can't suddenly turn off your habit. Suppose you're an evil person, evil in this wave on that way.

246
00:24:19,000 --> 00:24:26,000
In fact, we're all evil inside. But we all have evil.

247
00:24:26,000 --> 00:24:33,000
But if you say, I don't like this, I don't want this, how can I get rid of it?

248
00:24:33,000 --> 00:24:39,000
This is the problem a problem that meditators have is this attempt to try and turn off our habits.

249
00:24:39,000 --> 00:24:46,000
That's not how it works. It's a frustration because a meditator will hear all these things about how meditation helps you overcome bad habits.

250
00:24:46,000 --> 00:24:52,000
Practice, and practice, and say, look, my habits are not turning off. They're not gone.

251
00:24:52,000 --> 00:24:58,000
I'm still... I had one meditator recently. I don't know if they're listening, but very, very stubborn.

252
00:24:58,000 --> 00:25:04,000
I think they know who they are. And just recently, they've started to taste the result.

253
00:25:04,000 --> 00:25:19,000
It just takes a little patience and sort of a perspective. You need to put things in perspective.

254
00:25:19,000 --> 00:25:30,000
That habits are strong and have momentum. And it takes time for that momentum to fade away.

255
00:25:30,000 --> 00:25:36,000
It's not something you can stop by putting your fist in front, your hand in front of it to stop it.

256
00:25:36,000 --> 00:25:45,000
It's something that you're slowly slowly. And in fact, the more patient and the more delicate you are with it,

257
00:25:45,000 --> 00:25:54,000
you find them more quickly. They aren't they changed. The more you try to control and force, the harder and the longer it's going to take.

258
00:25:54,000 --> 00:26:01,000
As you have to realize that that is a bad habit as well, trying to force you to control things.

259
00:26:01,000 --> 00:26:11,000
That's our first lesson. In the degrees of evil and understanding, there's a difference between evil and being consumed by you.

260
00:26:11,000 --> 00:26:19,000
The second one is how... I mean, it's part and parcel is that evil hurts the evil doer.

261
00:26:19,000 --> 00:26:27,000
It's a very Buddhist theory. The ordinary perception of evil is that it hurts other people, right?

262
00:26:27,000 --> 00:26:33,000
You kill someone while they're the one who's suffering. It's in fact not really the case.

263
00:26:33,000 --> 00:26:39,000
It's interesting to think about people who are murdered, abused, tortured.

264
00:26:39,000 --> 00:26:48,000
In many cases, they do suffer terribly. Of course, in Buddhism, we would say it's not actually because of the thing that was done to them that they suffer.

265
00:26:48,000 --> 00:26:53,000
It's because most of us aren't equipped to deal with situations.

266
00:26:53,000 --> 00:27:00,000
That might sound somewhat harsh, but it's unfortunately the case.

267
00:27:00,000 --> 00:27:08,000
Evil is terrible to the victim simply because we're vulnerable,

268
00:27:08,000 --> 00:27:14,000
but evil itself doesn't harm the victim.

269
00:27:14,000 --> 00:27:19,000
If you kill someone, let's say you torture someone, it's even worse, right?

270
00:27:19,000 --> 00:27:22,000
You lock someone in the basement and torture them.

271
00:27:22,000 --> 00:27:28,000
I mean, theoretically, an enlightened person, not even theoretically.

272
00:27:28,000 --> 00:27:32,000
An enlightened person can be completely at peace with all of that.

273
00:27:32,000 --> 00:27:35,000
The Bodhisattva, there's lots of examples.

274
00:27:35,000 --> 00:27:41,000
Even before he was a Buddha where he was tortured and sliced to pieces,

275
00:27:41,000 --> 00:27:50,000
his nose cut off and his hands cut off and his feet cut off and his head cut off.

276
00:27:50,000 --> 00:27:57,000
It was completely at peace with it, even before he was enlightened.

277
00:27:57,000 --> 00:28:04,000
Maybe not completely at peace, but it was quite patient with suffering.

278
00:28:04,000 --> 00:28:14,000
So, what we would argue, how we explain it, is that it's actually evil in us as victims

279
00:28:14,000 --> 00:28:17,000
that causes us to suffer.

280
00:28:17,000 --> 00:28:22,000
And so by this, we mean when someone is torturing you, you react to it with what, with anger,

281
00:28:22,000 --> 00:28:25,000
or with what we caught with butting up with a virgin.

282
00:28:25,000 --> 00:28:27,000
We caught why they use anger.

283
00:28:27,000 --> 00:28:31,000
When I say anger in a Buddhist sense, it usually just means a virgin.

284
00:28:31,000 --> 00:28:35,000
It's this category, it's not actually anger, it's one type.

285
00:28:35,000 --> 00:28:43,000
But it's any state based on a virgin, butting out is the Jaitasika.

286
00:28:43,000 --> 00:28:46,000
And so that a virgin is evil.

287
00:28:46,000 --> 00:28:48,000
It's a virgin that leads you to kill.

288
00:28:48,000 --> 00:28:50,000
It's a virgin that leads you to torture.

289
00:28:50,000 --> 00:28:56,000
A virgin isn't even the right one, but it's a wishing for harm.

290
00:28:56,000 --> 00:29:05,000
So wishing bad, it's a disliking state.

291
00:29:05,000 --> 00:29:08,000
When you don't hurt people you like, right?

292
00:29:08,000 --> 00:29:14,000
You hurt them when you don't like, when you're angry, when you wish them harm, right?

293
00:29:14,000 --> 00:29:22,000
It's a negative state.

294
00:29:22,000 --> 00:29:28,000
And so the same states are exist in the victim when they get upset at the person torturing.

295
00:29:28,000 --> 00:29:32,000
When they get upset at the experience of being tortured, reasonable.

296
00:29:32,000 --> 00:29:37,000
I mean most of us aren't nearly equipped to deal with traumatic experiences.

297
00:29:37,000 --> 00:29:39,000
And so we call them traumatic experiences.

298
00:29:39,000 --> 00:29:45,000
I think as the experiences themselves aren't traumatic.

299
00:29:45,000 --> 00:29:53,000
The trauma lies in reacting violently to our experiences.

300
00:29:53,000 --> 00:29:59,000
But focusing on the evil doer, the evil doer who are really hurting the word, the worst,

301
00:29:59,000 --> 00:30:06,000
is themselves because the aversion and the depth of the evil in their minds,

302
00:30:06,000 --> 00:30:12,000
in their hearts when they do these things, is destroying their minds.

303
00:30:12,000 --> 00:30:20,000
It's making it impossible harder and harder for them to find peace and happiness.

304
00:30:20,000 --> 00:30:25,000
I always come back to this classic Russian novel crime and punishment.

305
00:30:25,000 --> 00:30:28,000
It's a really good book for that purpose.

306
00:30:28,000 --> 00:30:36,000
If you read it, it's about a person who kills two people.

307
00:30:36,000 --> 00:30:42,000
And it's totally unprepared for what it's done to him, the nightmares he has,

308
00:30:42,000 --> 00:30:49,000
how the nightmare his life becomes simply because he thought he would be above the law

309
00:30:49,000 --> 00:30:52,000
if he thought of himself as a great person.

310
00:30:52,000 --> 00:30:54,000
And he didn't realize that nothing to do with the law.

311
00:30:54,000 --> 00:30:57,000
He'd kill him and it makes you a psychopath.

312
00:30:57,000 --> 00:30:59,000
It changes.

313
00:30:59,000 --> 00:31:01,000
Makes you psychotic.

314
00:31:01,000 --> 00:31:10,000
And of course, in the end, he was able, and killing is not even killing a human being.

315
00:31:10,000 --> 00:31:13,000
It doesn't necessarily prevent you from becoming a light,

316
00:31:13,000 --> 00:31:22,000
as long as it's not one of your parents, or the Buddha, or an enlightened being, that could.

317
00:31:22,000 --> 00:31:32,000
But quite clearly, if you hate someone so much that you kill them,

318
00:31:32,000 --> 00:31:42,000
the Buddha says, you've supposed to people hate each other when you commit evil.

319
00:31:42,000 --> 00:31:44,000
You do what they would wish for you.

320
00:31:44,000 --> 00:31:49,000
They want you to suffer, or someone who would want you to suffer

321
00:31:49,000 --> 00:31:54,000
gets what they want when you do evil.

322
00:31:54,000 --> 00:31:58,000
You don't make your situation better.

323
00:31:58,000 --> 00:32:01,000
Make your situation worse.

324
00:32:01,000 --> 00:32:03,000
That's key in meditation.

325
00:32:03,000 --> 00:32:06,000
That's key in what we're trying to see.

326
00:32:06,000 --> 00:32:10,000
I mean, this is conceptual in terms of people, right?

327
00:32:10,000 --> 00:32:14,000
We're talking in terms of people and their enemies.

328
00:32:14,000 --> 00:32:19,000
The essence of it is that evil harms the evil do her,

329
00:32:19,000 --> 00:32:22,000
and that's really what we're trying to see in meditation.

330
00:32:22,000 --> 00:32:25,000
We're not trying to believe this, or have faith in it,

331
00:32:25,000 --> 00:32:27,000
we're trying to experience it.

332
00:32:27,000 --> 00:32:30,000
At every moment of aversion, every moment of desire,

333
00:32:30,000 --> 00:32:34,000
every moment of ignorance, delusion, arrogance, conceit,

334
00:32:34,000 --> 00:32:37,000
we're hurting ourselves.

335
00:32:37,000 --> 00:32:49,000
But these are the cause of suffering, these are in the realm of what is causing us stress and suffering.

336
00:32:49,000 --> 00:32:53,000
That's what we're aiming to understand.

337
00:32:53,000 --> 00:32:57,000
To see evil for what it is, not believe in advance,

338
00:32:57,000 --> 00:33:02,000
but to observe our mind so clearly,

339
00:33:02,000 --> 00:33:08,000
to observe our experience so clearly that we're able to see good evil

340
00:33:08,000 --> 00:33:16,000
for ourselves without any belief or doubt.

341
00:33:16,000 --> 00:33:20,000
So that's the Dhamma part of verse for tonight.

342
00:33:20,000 --> 00:33:22,000
Thank you all for tuning in.

343
00:33:22,000 --> 00:33:38,000
Have a good day.

