1
00:00:00,000 --> 00:00:07,000
All right, good evening everyone. Welcome to our evening dumb session.

2
00:00:07,000 --> 00:00:30,000
We're all together. We have our meditators here in Hamilton.

3
00:00:30,000 --> 00:00:37,000
Doing their best to struggle through the difficulty of intensive meditation practice.

4
00:00:37,000 --> 00:00:40,000
Very much to be commended.

5
00:00:40,000 --> 00:00:52,000
And we have the group of attendees in second life who are brave in another way.

6
00:00:52,000 --> 00:01:01,000
Struggling through the trials of getting virtual reality up and running.

7
00:01:01,000 --> 00:01:14,000
And taking the time every day some of you to come here and listen to the dumb mat.

8
00:01:14,000 --> 00:01:23,000
Then we have the audio, the live audio, if anyone's listening.

9
00:01:23,000 --> 00:01:31,000
The audio is easy because you can listen on your phone or quite low tech, relatively low tech.

10
00:01:31,000 --> 00:01:39,000
You just need the URL and you can listen into the live stream or you can listen to the mp3 later on.

11
00:01:39,000 --> 00:01:44,000
And finally YouTube of course, which is the easiest of all.

12
00:01:44,000 --> 00:01:52,000
Many people just find these videos I think through a search or through a related video or people following

13
00:01:52,000 --> 00:02:02,000
my YouTube channel. So we have a large audience which is always nice.

14
00:02:02,000 --> 00:02:19,000
Thank you all for tuning in and for watching these videos. Tonight, tonight I wanted to talk to you all about right and wrong.

15
00:02:19,000 --> 00:02:23,000
Just question of what is right and what is wrong.

16
00:02:23,000 --> 00:02:33,000
It's a big question in our meditation. It's really what we claim to be able to figure out through the practice.

17
00:02:33,000 --> 00:02:41,000
The first question we have to ask is not what is right and what is wrong but is it there right and wrong?

18
00:02:41,000 --> 00:02:48,000
What does it mean to say something is right and something is wrong?

19
00:02:48,000 --> 00:02:58,000
A lot of people will have you believe that there is no right and wrong or more commonly will say that it's relative.

20
00:02:58,000 --> 00:03:07,000
There is no absolute right and wrong.

21
00:03:07,000 --> 00:03:20,000
So there are two ways we can understand that there exists right and wrong.

22
00:03:20,000 --> 00:03:30,000
From a purely materialistic or sinned physicalist point of view, sure there is no right and wrong.

23
00:03:30,000 --> 00:03:38,000
But that really points more to the problem with the physicalist outlook on life.

24
00:03:38,000 --> 00:04:07,000
As we more commonly understand and impersonal, a non-person or third person impersonal point of view.

25
00:04:07,000 --> 00:04:17,000
And the real problem is that it's an abstraction and that's what you find curious about.

26
00:04:17,000 --> 00:04:23,000
The concept of there being no right or wrong.

27
00:04:23,000 --> 00:04:30,000
The idea goes that atoms, of course, have no right or wrong states.

28
00:04:30,000 --> 00:04:38,000
Rocks don't know right or wrong.

29
00:04:38,000 --> 00:04:49,000
And going by that, building the human beings up from these entities, atoms and cell molecules and cells.

30
00:04:49,000 --> 00:04:52,000
It's hard to say that there could ever be any right or wrong.

31
00:04:52,000 --> 00:04:59,000
Looking at it from that point of view and a superficial understanding of Buddhism might have you seeing things in the same way.

32
00:04:59,000 --> 00:05:04,000
If you look at the Abby Dummley, you just see a bunch of mind states, body states.

33
00:05:04,000 --> 00:05:08,000
It seems very similar to the physicalist outlook.

34
00:05:08,000 --> 00:05:10,000
It's just giving a description.

35
00:05:10,000 --> 00:05:13,000
And at the Buddha was quite clear that there is right and wrong.

36
00:05:13,000 --> 00:05:25,000
He was quite often portrayed in the texts as describing what is right and what is wrong quite often.

37
00:05:25,000 --> 00:05:41,000
So the first thing, the first way we can understand right and wrong is I think very much in still in line with this physicalist point of view or the third person impersonal concept of there being nothing intrinsically right and wrong.

38
00:05:41,000 --> 00:05:43,000
There still is right and wrong.

39
00:05:43,000 --> 00:05:49,000
And we shouldn't gloss over this or miss this.

40
00:05:49,000 --> 00:05:56,000
And that's the right and wrong of claims.

41
00:05:56,000 --> 00:06:12,000
So if you say God created the universe, it may not be investigative, but it certainly is either true or false or true end false or neither true nor false, but much more rarely those last two.

42
00:06:12,000 --> 00:06:17,000
Most claims are usually either true or false.

43
00:06:17,000 --> 00:06:22,000
Sometimes they're more complicated and you can't actually claim can't actually be discerned.

44
00:06:22,000 --> 00:06:36,000
But there are some pretty simple claims that are pretty easily categorized claims as being either right or wrong.

45
00:06:36,000 --> 00:06:41,000
And another way of looking this is true or false.

46
00:06:41,000 --> 00:06:48,000
But if a claim is true, then it's also right and that's an important distinction.

47
00:06:48,000 --> 00:07:03,000
For example, if you say that craving is the cause of suffering or more practically desire will lead me to suffer.

48
00:07:03,000 --> 00:07:05,000
Desire leads one to suffer.

49
00:07:05,000 --> 00:07:14,000
That's right. According to Buddhism, some people might say that's wrong.

50
00:07:14,000 --> 00:07:20,000
But these are important because they define how we look at the world.

51
00:07:20,000 --> 00:07:26,000
They clarify our position or understanding of how the world works.

52
00:07:26,000 --> 00:07:31,000
There's an important part of the meditation practice as I was talking about yesterday.

53
00:07:31,000 --> 00:07:36,000
What is reality? Reality is body and mind. Well, that's a claim.

54
00:07:36,000 --> 00:07:48,000
Some people would claim that reality is made up of atoms and subatomic particles and those are made up of who knows what.

55
00:07:48,000 --> 00:07:57,000
So we would say that our claim is right and the claim that reality is made up of atoms or subatomic particles is wrong, we would say.

56
00:07:57,000 --> 00:08:05,000
Of course, I think it's complicated. We wouldn't be so quite so cut and dry and we would say it really depends how you look at it.

57
00:08:05,000 --> 00:08:18,000
But regardless, there's something more right about saying that reality is made up of experience because experience is something that can be known.

58
00:08:18,000 --> 00:08:23,000
You might say, well, maybe there are things that we can't know.

59
00:08:23,000 --> 00:08:33,000
And so the argument is that the very fact that we can't, cannot possibly, never, ever will be able to know these things.

60
00:08:33,000 --> 00:08:42,000
It's by definition impossible to know these things, renders them less real.

61
00:08:42,000 --> 00:08:48,000
And therefore, less right as a means of understanding reality.

62
00:08:48,000 --> 00:08:57,000
So you see what I'm going with this. We're not yet talking about the quality of things as we're talking about claims or the way we understand reality.

63
00:08:57,000 --> 00:09:03,000
There is, we could argue that there is a right and a wrong way to understand reality.

64
00:09:03,000 --> 00:09:15,000
Because if you, you know, to put a point on it, if you understand reality in a certain way,

65
00:09:15,000 --> 00:09:22,000
the results are more or less likely to be in accordance with that understanding.

66
00:09:22,000 --> 00:09:27,000
So if your understanding is that craving will lead you to happiness,

67
00:09:27,000 --> 00:09:31,000
you know, clinging to things, desiring things, will lead you to happiness.

68
00:09:31,000 --> 00:09:35,000
Well, when, after some time, it leads you only to suffering.

69
00:09:35,000 --> 00:09:38,000
You have to say, well, that was wrong.

70
00:09:38,000 --> 00:09:41,000
I was wrong to think that.

71
00:09:41,000 --> 00:09:46,000
Regardless of whether you say it's right or wrong to suffer.

72
00:09:46,000 --> 00:09:49,000
And you were wrong because your claim turned out to be false.

73
00:09:49,000 --> 00:09:52,000
Your understanding, your outlook.

74
00:09:52,000 --> 00:09:55,000
This is a very important part of Buddhism.

75
00:09:55,000 --> 00:09:59,000
You know, for example, looking at the three characteristics.

76
00:09:59,000 --> 00:10:05,000
When we, we're talking about impermanent suffering and non-self what we mean

77
00:10:05,000 --> 00:10:09,000
is that we have a wrong understanding of reality as being stable, satisfied,

78
00:10:09,000 --> 00:10:12,000
and controllable and belonging to us.

79
00:10:12,000 --> 00:10:22,000
Proper to be a clung to or having some intrinsic nature of self-ness.

80
00:10:22,000 --> 00:10:28,000
And so as we meditate, we start to see this isn't true.

81
00:10:28,000 --> 00:10:31,000
So we can see that our mind is chaotic.

82
00:10:31,000 --> 00:10:33,000
That the body is chaotic.

83
00:10:33,000 --> 00:10:40,000
And nothing is ever predictable.

84
00:10:40,000 --> 00:10:45,000
That things are not stable or predictable or constant.

85
00:10:45,000 --> 00:10:51,000
And realizing that the way we look at the world and the way we expect things to be predictable and stable

86
00:10:51,000 --> 00:10:53,000
is causing us suffering.

87
00:10:53,000 --> 00:10:56,000
And it's hard to see that we were wrong.

88
00:10:56,000 --> 00:11:02,000
That's a suffering because it's, it's, we were simply wrong about what we thought about

89
00:11:02,000 --> 00:11:03,000
reality.

90
00:11:03,000 --> 00:11:05,000
We thought it was like this and it's not like that.

91
00:11:05,000 --> 00:11:08,000
Well, that causes stress and suffering.

92
00:11:08,000 --> 00:11:11,000
You know, not yet saying whether stress and suffering is wrong.

93
00:11:11,000 --> 00:11:14,000
You know, we probably want to say that eventually.

94
00:11:14,000 --> 00:11:16,000
We're just saying we were wrong.

95
00:11:16,000 --> 00:11:18,000
Well, we thought that it was permanent and it wasn't permanent.

96
00:11:18,000 --> 00:11:23,000
Thought it was stable, it was unstable.

97
00:11:23,000 --> 00:11:26,000
And we think things are satisfying and they turn out to be unsatisfied.

98
00:11:26,000 --> 00:11:29,000
I think we can find happiness by clinging to things.

99
00:11:29,000 --> 00:11:31,000
This will make me happy.

100
00:11:31,000 --> 00:11:37,000
If I get this, if I cling to it, if I can build up this, then I'll be happy.

101
00:11:37,000 --> 00:11:41,000
They're wrong.

102
00:11:41,000 --> 00:11:46,000
And finally, softly, we have the, we try to control things.

103
00:11:46,000 --> 00:11:48,000
We possess things.

104
00:11:48,000 --> 00:11:49,000
We identify with things.

105
00:11:49,000 --> 00:11:50,000
This is me.

106
00:11:50,000 --> 00:11:55,000
This is mine.

107
00:11:55,000 --> 00:11:58,000
And so we have the idea that we can, that things are controllable.

108
00:11:58,000 --> 00:12:00,000
Things from me and mine.

109
00:12:00,000 --> 00:12:06,000
So to realize, well, it's not really reasonable to suggest that these are me or mine.

110
00:12:06,000 --> 00:12:09,000
And we suffer as a result that we suffer.

111
00:12:09,000 --> 00:12:13,000
And we suffer because they go against our understanding.

112
00:12:13,000 --> 00:12:18,000
When our understanding is that we can control things and then they aren't under our control.

113
00:12:18,000 --> 00:12:22,000
We realize we were wrong.

114
00:12:22,000 --> 00:12:23,000
We were wrong.

115
00:12:23,000 --> 00:12:24,000
They're not under our control.

116
00:12:24,000 --> 00:12:25,000
We thought they belonged to us.

117
00:12:25,000 --> 00:12:30,000
We realized, well, you could conceive of them as being yours.

118
00:12:30,000 --> 00:12:31,000
That's not wrong.

119
00:12:31,000 --> 00:12:36,000
Yes, I can conceive of it as being so.

120
00:12:36,000 --> 00:12:40,000
But you could still say it's wrong because there's not really much reason.

121
00:12:40,000 --> 00:12:48,000
And the best, you might say, it's unreasonable to suggest that these things are me or mine.

122
00:12:48,000 --> 00:12:54,000
Because they come and they go and their own accord causes and conditions having nothing to do with me or mine.

123
00:12:54,000 --> 00:12:58,000
Me or mine in any way.

124
00:12:58,000 --> 00:13:06,000
So we have to conclude, I think, that it's wrong to say that they're me and mine.

125
00:13:06,000 --> 00:13:09,000
It's a very important part of Buddhism.

126
00:13:09,000 --> 00:13:13,000
You see, because modern times it's all about things being relative.

127
00:13:13,000 --> 00:13:16,000
If it's right for me or it's right for you.

128
00:13:16,000 --> 00:13:21,000
Just because you think it's wrong, doesn't mean it's wrong for me.

129
00:13:21,000 --> 00:13:26,000
So problematic, as you can see, hopefully, from what I've been saying.

130
00:13:26,000 --> 00:13:30,000
My claim is that there are very clearly things that are wrong for everybody.

131
00:13:30,000 --> 00:13:32,000
They're wrong because they're wrong.

132
00:13:32,000 --> 00:13:35,000
Wrong understanding.

133
00:13:35,000 --> 00:13:48,000
Wrong logical assumption of logical conclusion.

134
00:13:48,000 --> 00:13:51,000
I think in general we'd have to agree that there is wrong.

135
00:13:51,000 --> 00:13:53,000
And then there is right.

136
00:13:53,000 --> 00:13:57,000
It's right to say that everything that arises will cease.

137
00:13:57,000 --> 00:14:03,000
Nothing that arises is permanent or stable or satisfying or controllable.

138
00:14:03,000 --> 00:14:06,000
And belonging to us.

139
00:14:06,000 --> 00:14:13,000
But then we get into the interesting, more interesting and more difficult, I think.

140
00:14:13,000 --> 00:14:23,000
This is the more contentious question of whether something, some action or some thought,

141
00:14:23,000 --> 00:14:25,000
some mind state.

142
00:14:25,000 --> 00:14:30,000
We're going to stick mostly to mind states, I suppose, with Buddhism.

143
00:14:30,000 --> 00:14:33,000
It can be right or wrong.

144
00:14:33,000 --> 00:14:35,000
Is it wrong to kill?

145
00:14:35,000 --> 00:14:37,000
Is it wrong to steal?

146
00:14:37,000 --> 00:14:38,000
Is anger wrong?

147
00:14:38,000 --> 00:14:40,000
Is greed wrong?

148
00:14:40,000 --> 00:14:41,000
These are different questions.

149
00:14:41,000 --> 00:14:46,000
This is a different question from whether saying that our understanding of the world is right or wrong.

150
00:14:46,000 --> 00:14:48,000
Though they have to do with that.

151
00:14:48,000 --> 00:14:52,000
We kill and steal with the understanding that it's going to bring us happiness.

152
00:14:52,000 --> 00:14:54,000
It doesn't.

153
00:14:54,000 --> 00:14:57,000
It's going to eliminate suffering.

154
00:14:57,000 --> 00:15:00,000
It doesn't.

155
00:15:00,000 --> 00:15:01,000
There's the question.

156
00:15:01,000 --> 00:15:05,000
Is it right or wrong to suffer?

157
00:15:05,000 --> 00:15:14,000
And I think the real problem here is that we've cornered ourselves or we've shot ourselves in the foot,

158
00:15:14,000 --> 00:15:25,000
I suppose you can say, with our scientific, our impersonal conception of reality.

159
00:15:25,000 --> 00:15:30,000
It's curious, really, because logically you want to think, well, yes.

160
00:15:30,000 --> 00:15:34,000
A rock doesn't know right or wrong.

161
00:15:34,000 --> 00:15:40,000
So if reality is based on all this, there is no right and wrong.

162
00:15:40,000 --> 00:15:49,000
But that's what's curious is that right and wrong in this sense only have anything to do with experiential reality.

163
00:15:49,000 --> 00:15:57,000
And the concept that there is no right or wrong has only to do with conceptual reality.

164
00:15:57,000 --> 00:16:05,000
And it's important to see this distinction, to see that experience very much has right or wrong.

165
00:16:05,000 --> 00:16:14,000
Because experience is very much, I would say intrinsically, intrinsically tied to happiness or suffering.

166
00:16:14,000 --> 00:16:21,000
And intrinsically tied to getting what you want, not getting what you want.

167
00:16:21,000 --> 00:16:27,000
It's intrinsically tied to right or wrong.

168
00:16:27,000 --> 00:16:31,000
There's an intrinsic right or wrong in experience.

169
00:16:31,000 --> 00:16:36,000
So the reason why it's difficult for us to come to this conclusion that there is right or wrong

170
00:16:36,000 --> 00:16:45,000
is because we think that reality is important.

171
00:16:45,000 --> 00:16:55,000
The most egregious form of saying there is no right or wrong is, I think it's egregious in the sense that it mixes up reality and abstraction.

172
00:16:55,000 --> 00:17:05,000
Because any concept we might have of rocks and atoms is conceptual as abstract.

173
00:17:05,000 --> 00:17:21,000
It's not really experienced. It's very much caught up with good and bad and right and wrong.

174
00:17:21,000 --> 00:17:29,000
Another way that it's denied that there is any right and wrong is to say right for me, right for you.

175
00:17:29,000 --> 00:17:41,000
That's fine, I think that's important because there are many things that are not right or wrong.

176
00:17:41,000 --> 00:17:52,000
Like we can say, lighting incense or candles in this way or that way, this way is right.

177
00:17:52,000 --> 00:18:08,000
If you look at religions, most people look at religions in terms of the outward expressions of lighting candles and incense and bowing down and praying and thinking about God generally.

178
00:18:08,000 --> 00:18:22,000
And we could even argue that Buddhism has a God in the sense of karma, in the sense of samsara, samsara is sort of our God.

179
00:18:22,000 --> 00:18:39,000
I know Buddhism is atheistic, we don't pray, in fact, get me away from this thing, but that's God. It's God because it's the system, it's the all, it's the entirety.

180
00:18:39,000 --> 00:18:55,000
And so my point being here is that all religions relate somehow to this concept. And people look at this and want to say, oh well, and the differences are merely cosmetic and to each their own.

181
00:18:55,000 --> 00:19:08,000
This is right for me and this is right for you. And that's fine, lighting candles this way, lighting candles that way, speaking this way, speaking that way.

182
00:19:08,000 --> 00:19:18,000
It's all superficial, there's no right or wrong involved there.

183
00:19:18,000 --> 00:19:29,000
But we go further than that in Buddhism, and this is the idea of praying to God in the first place, or we're veering God in the first place.

184
00:19:29,000 --> 00:19:39,000
But as a will acknowledge, we could acknowledge, based on some fairly broad vague conception of what God is, we could say, okay, yes, there's God.

185
00:19:39,000 --> 00:19:49,000
Yes, you have the universe and you have laws, and we have the laws of nature in that sense.

186
00:19:49,000 --> 00:20:01,000
You have an orderly sense of reality, and it is real, it doesn't exist.

187
00:20:01,000 --> 00:20:08,000
But it's not right to worship it in any way. We worship God in so many ways.

188
00:20:08,000 --> 00:20:20,000
There are the C.S. to worship the concept of God, and then there are the materialists, and by here I mean like the consumerists who worship pleasure.

189
00:20:20,000 --> 00:20:28,000
We worship the body as being, and the be all end all of happiness. We worship sights and sounds and smells.

190
00:20:28,000 --> 00:20:35,000
We worship money, we worship power.

191
00:20:35,000 --> 00:20:43,000
This is all wrong. It's wrong to do so. That's what we would say in Buddhism.

192
00:20:43,000 --> 00:21:01,000
We'd argue for a sense of wrong here. It's wrong to do so.

193
00:21:01,000 --> 00:21:15,000
So the Buddhist sense of wrong, and of course everyone, yes, different people have their ideas of what is right and wrong in this context.

194
00:21:15,000 --> 00:21:25,000
But in Buddhism we have our own specific ideas of what is right and wrong, but the ideas are based on that which brings you suffering.

195
00:21:25,000 --> 00:21:33,000
It's wrong and that which brings happiness is right.

196
00:21:33,000 --> 00:21:51,000
And so the argument here is not merely the idea that certain things do bring you happiness or suffering, but that intrinsically reality, experiential reality, and trinsically has something to say on this topic.

197
00:21:51,000 --> 00:21:56,000
That intrinsically there is something wrong about suffering. It's the very definition of wrong.

198
00:21:56,000 --> 00:22:01,000
And you might disagree with this. This is something that I suppose is controversial.

199
00:22:01,000 --> 00:22:09,000
But I don't think it should be controversial. I think we have this concept of suffering,

200
00:22:09,000 --> 00:22:16,000
this category of experience, this category of reality is that which is suffering.

201
00:22:16,000 --> 00:22:27,000
And I think that is wrong intrinsically. I don't think there is any argument. There should be any argument here.

202
00:22:27,000 --> 00:22:35,000
I was talking to my friend about this, and she was the one who got me onto this sense, and there is no right and wrong.

203
00:22:35,000 --> 00:22:44,000
And she was talking about letting her son suffer, her son was suffering, and she let him, and she didn't console him.

204
00:22:44,000 --> 00:22:55,000
And so it was important to understand what we mean here. We're not just talking about simple suffering, like pain, for example.

205
00:22:55,000 --> 00:23:06,000
We're talking about this, that which better is a person, that which leads a person to greater happiness, not which leads a person to greater suffering.

206
00:23:06,000 --> 00:23:21,000
That which is pure of mind, in the sense of having intentions to bring happiness, having intentions to bring peace.

207
00:23:21,000 --> 00:23:35,000
We would argue that these are right, no matter whether it's through tough love, sometimes you have to let people suffer in order to learn, in order to grow, in order to be free from suffering.

208
00:23:35,000 --> 00:23:42,000
But there is nothing intellectual about this. This is what we start to see in meditation.

209
00:23:42,000 --> 00:23:46,000
And this is how we can know that this is right, that there really is a right and wrong.

210
00:23:46,000 --> 00:23:52,000
Because as you meditate, you meditate. You can't avoid the truth.

211
00:23:52,000 --> 00:23:59,000
You can't avoid the truth if suffering is wrong, and whatever causes suffering is wrong.

212
00:23:59,000 --> 00:24:03,000
It's not intellectual. You just can't possibly convince yourself.

213
00:24:03,000 --> 00:24:17,000
The only way you can convince yourself is by doing what we do, and running away from the truth, or chasing our tails to avoid the reality of the situation.

214
00:24:17,000 --> 00:24:23,000
As you meditate and you start to see reality, you can't avoid it, and you can't force yourself.

215
00:24:23,000 --> 00:24:33,000
If you find yourself unable to force yourself to suffer, to cause suffering,

216
00:24:33,000 --> 00:24:36,000
you start to realize that there is something wrong with me.

217
00:24:36,000 --> 00:24:40,000
There's a lot of things wrong with me, and they're truly wrong.

218
00:24:40,000 --> 00:24:44,000
And the great thing, of course, is that we can change them.

219
00:24:44,000 --> 00:24:50,000
Your power, your great power that we have, is that we can write these wrongs.

220
00:24:50,000 --> 00:24:58,000
And trends are wrong. They're intrinsically wrong, but they're not intrinsic to us.

221
00:24:58,000 --> 00:25:01,000
We can change.

222
00:25:01,000 --> 00:25:11,000
I think another reason why people shy away from the idea of they're being right and wrong is because they don't have this concept that we can change.

223
00:25:11,000 --> 00:25:15,000
If you were to say something is wrong, we'd have to say a person is wrong. You're wrong.

224
00:25:15,000 --> 00:25:19,000
There's something wrong with you. Of course, no one wants to hear that.

225
00:25:19,000 --> 00:25:23,000
And even to suggest that that's a good thing to tell people.

226
00:25:23,000 --> 00:25:29,000
If I tell you there's something wrong with you, you might get very angry at me.

227
00:25:29,000 --> 00:25:32,000
Or at least feel very sad and bad about yourself.

228
00:25:32,000 --> 00:25:35,000
My teacher told me there's something wrong with me.

229
00:25:35,000 --> 00:25:40,000
There's something wrong with all of us. There's lots of things wrong with us.

230
00:25:40,000 --> 00:25:44,000
But it's empowering to know that.

231
00:25:44,000 --> 00:25:47,000
Because if there's something wrong with the engine, you want to know about it.

232
00:25:47,000 --> 00:25:53,000
It's empowering because you don't want to drive the car without fixing it.

233
00:25:53,000 --> 00:25:59,000
What we're doing is we're making mechanics out of us all to use this analogy again.

234
00:25:59,000 --> 00:26:06,000
We're going to fix what's wrong.

235
00:26:06,000 --> 00:26:09,000
And this is why the Buddha taught the eightfold noble path.

236
00:26:09,000 --> 00:26:12,000
We call it the noble path, but it could not easily be called.

237
00:26:12,000 --> 00:26:15,000
That's what it's called. That's the name the Buddha gave it.

238
00:26:15,000 --> 00:26:18,000
But another name for it could be called the right path.

239
00:26:18,000 --> 00:26:20,000
Because that's the word the Buddha uses.

240
00:26:20,000 --> 00:26:22,000
Some are.

241
00:26:22,000 --> 00:26:24,000
We have right view.

242
00:26:24,000 --> 00:26:26,000
Right view is important.

243
00:26:26,000 --> 00:26:28,000
It's important to not have wrong view.

244
00:26:28,000 --> 00:26:29,000
Meet Chad.

245
00:26:29,000 --> 00:26:30,000
Diti.

246
00:26:30,000 --> 00:26:33,000
Wrong view.

247
00:26:33,000 --> 00:26:35,000
Wrong view because it causes suffering.

248
00:26:35,000 --> 00:26:36,000
It's wrong.

249
00:26:36,000 --> 00:26:37,000
It's not good for you.

250
00:26:37,000 --> 00:26:42,000
It's not right.

251
00:26:42,000 --> 00:26:44,000
Wrong thought.

252
00:26:44,000 --> 00:26:45,000
Wrong speech.

253
00:26:45,000 --> 00:26:46,000
Wrong action.

254
00:26:46,000 --> 00:26:47,000
Wrong lie in the head.

255
00:26:47,000 --> 00:26:52,000
They're all wrong, wrong, wrong.

256
00:26:52,000 --> 00:26:53,000
Wrong effort.

257
00:26:53,000 --> 00:26:55,000
Wrong line for this.

258
00:26:55,000 --> 00:27:00,000
Wrong concentration.

259
00:27:00,000 --> 00:27:04,000
So we cultivate what is right.

260
00:27:04,000 --> 00:27:05,000
We don't have to ask.

261
00:27:05,000 --> 00:27:08,000
We don't have to ask me what is right.

262
00:27:08,000 --> 00:27:12,000
The point here, the point with all of this is not to be able to even discern

263
00:27:12,000 --> 00:27:14,000
intellectually what is right and what is wrong.

264
00:27:14,000 --> 00:27:16,000
Not to be able to decide.

265
00:27:16,000 --> 00:27:18,000
It's to look and to see.

266
00:27:18,000 --> 00:27:23,000
And through looking and seeing, you will start to develop right view, right thought.

267
00:27:23,000 --> 00:27:26,000
That which is good for you.

268
00:27:26,000 --> 00:27:30,000
That which is in line with reality.

269
00:27:30,000 --> 00:27:33,000
And that which leads to peace, happiness and freedom for suffering.

270
00:27:33,000 --> 00:27:38,000
That's what's meant by right and wrong.

271
00:27:38,000 --> 00:27:39,000
So there you go.

272
00:27:39,000 --> 00:27:41,000
That's the number for tonight.

273
00:27:41,000 --> 00:27:42,000
Thank you all for tuning in.

274
00:27:42,000 --> 00:27:44,000
I wish you all the best.

275
00:27:44,000 --> 00:28:12,000
Hope you all find the right path.

