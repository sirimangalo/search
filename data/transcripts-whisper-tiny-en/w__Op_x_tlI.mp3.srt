1
00:00:00,000 --> 00:00:06,220
How does one maintain peaceful relationships with people for whom

2
00:00:06,220 --> 00:00:14,040
un-buddhist attitudes, anger, conflict, etc. is the norm and not about to

3
00:00:14,040 --> 00:00:20,000
change any time soon. Work colleagues, family, etc.

4
00:00:20,000 --> 00:00:38,000
I would say to, I can't find the word now, to separate, to seclude yourself from this situation.

5
00:00:38,000 --> 00:00:47,000
If you can't seclude yourself physically, then you have to do it mentally.

6
00:00:47,000 --> 00:00:54,000
Sometimes it's not possible because you are working and you have the family and you have the colleagues.

7
00:00:54,000 --> 00:01:05,000
So you can't get away from it. Then, when you can't get away physically,

8
00:01:05,000 --> 00:01:19,000
get away mentally, try not to get involved too much. Try to note what they bring into the situation,

9
00:01:19,000 --> 00:01:33,000
try to see their defilements as they are. And either you can try to just note it,

10
00:01:33,000 --> 00:01:45,000
to be mindful on it, and then be mindful on your reaction. Try that your reaction is not going into the same direction,

11
00:01:45,000 --> 00:01:58,000
but when there is anger coming towards you that you ward it off and stay peaceful and stay in your mental seclusion.

12
00:01:58,000 --> 00:02:11,000
And the other thing is when the anger, for example, overwhelmed you, you got into a conflict with others,

13
00:02:11,000 --> 00:02:22,000
then step out either physically or mentally and try to develop some whole, some mind state.

14
00:02:22,000 --> 00:02:37,000
Take a deep breath and try to calm down for a second, noting your breath and know that the breath,

15
00:02:37,000 --> 00:02:49,000
no matter what happens, is everything there is always there and is always the next thing that happens as long as you are alive.

16
00:02:49,000 --> 00:03:03,000
So this is one thing you can kind of rely on and this is what calms me down pretty much when I have fear or when I am in a situation of anger.

17
00:03:03,000 --> 00:03:11,000
I know there will always as long as I am alive be the next breath and I can just observe it.

18
00:03:11,000 --> 00:03:23,000
So I can recommend to do that and maybe it has the same effect for you than it has for me, so that brings you a little bit of mental seclusion.

19
00:03:23,000 --> 00:03:32,000
And the rest, but it will explain.

20
00:03:32,000 --> 00:03:42,000
Yeah, I just wanted to back up the whole breath thing. The Buddha was very clear about how useful the breath is in this way. The Buddha said, breath has five benefits.

21
00:03:42,000 --> 00:03:53,000
Santoja, it is peaceful. Panito, it is subtle or refined. It is refined.

22
00:03:53,000 --> 00:04:05,000
I say it is secluded. It is not mixed with anything. It is pure in itself. It is just the breath.

23
00:04:05,000 --> 00:04:12,000
Sukhor, Javi Harlow, at least one to dwell in happiness.

24
00:04:12,000 --> 00:04:25,000
And then Upa Nupaneak was the papa gaiyak was the ladha matanaso and pranapati would move as a maintain. Whatever unholesome things have arisen.

25
00:04:25,000 --> 00:04:32,000
It in the here and now does away with them or gets rid of them. It allows you to see them clearly.

26
00:04:32,000 --> 00:04:42,000
To see them like specks on a pure cloth. So the breath is something that actually should be recommended for sure. I think that is really good.

27
00:04:42,000 --> 00:04:53,000
What I wanted to say was dealing with something a little specific in the question that is kind of implied there.

28
00:04:53,000 --> 00:05:12,000
Any expectation that we might not realize is that even in trying to have mindful relationships with people, it is still an expectation.

29
00:05:12,000 --> 00:05:25,000
The expectation that we are going to have a peaceful calm relationship with these people. I have had relationships where I felt I was reasonably mindful and the other person was freaking out.

30
00:05:25,000 --> 00:05:46,000
You are crazy and shouting at me. I am not trying to say that I was perfect in this situation. In my mind it really wasn't a problem that this person was freaking out.

31
00:05:46,000 --> 00:05:58,000
It was a natural result of their level of stress and mental agitation. There was no good that I could do by trying to placate them.

32
00:05:58,000 --> 00:06:19,000
In fact, it seemed to me the best thing for them to be able to realize and it was in fact because I was calm and this person was yelling at me and not this person, many people. I have had many relationships like this.

33
00:06:19,000 --> 00:06:30,000
I can witness, as anyone ever told you they are going to hit you in their head, they are looking at them and walked away.

34
00:06:30,000 --> 00:06:50,000
He really walked calmly and that made the whole situation so absurd because there was that angry monk and he had a completely different timing and a completely different timing.

35
00:06:50,000 --> 00:07:04,000
You really changed the people by just being mindful and that doesn't mean that they are going to be harmonious with you. They might go crazy.

36
00:07:04,000 --> 00:07:20,000
You find that in work as well, I think. You are surrounded by people who are unmindful. They will have a lot of stress because you are reminding them how uncommon they are. You are making them see their rhythm.

37
00:07:20,000 --> 00:07:35,000
I didn't turn back on the monk and say, why don't you go back to your father's country. You were not born near this. Your father was not born here. Why don't you go back to your father's journey.

38
00:07:35,000 --> 00:07:50,000
My father is in Jamthang. My father is in Jamthang and he sent me here to fix this place. I was not perfectly calm. I was fairly self-righteous at the time.

39
00:07:50,000 --> 00:08:07,000
I felt like this was uncalled for. Some people didn't think I was there. Obviously. That was it.

40
00:08:07,000 --> 00:08:23,000
I decided that that monastery wasn't suitable place. Of course, the other thing is that the Buddha said clearly don't associate with fools. As much as you can seclude yourself, do your work and go home.

41
00:08:23,000 --> 00:08:49,000
We talked about this on the forum. Someone asked a fairly similar question. I think we worked quite well with the various answers. If you do your work, even if other people feel that you are not working well with them or something people feel like you are no longer communicating properly.

42
00:08:49,000 --> 00:09:06,000
What you'll generally find is that the people who in charge respect you because you are no longer trying to cheat them and you are no longer lazy and you are a hard worker and you stick to the task and you are focused and concentrated.

43
00:09:06,000 --> 00:09:22,000
It's not really something to worry about rather than trying to have harmonious relationships with other people. Even better than having a harmonious relationship, be a rock for these people.

44
00:09:22,000 --> 00:09:38,000
There's the disharmony. You be the pillar of strength. Make yourself the good friend for them.

