1
00:00:00,000 --> 00:00:03,880
Hello, welcome back to Askremunk.

2
00:00:03,880 --> 00:00:11,040
This is a response in regards to the question as to whether or not Buddhists must be

3
00:00:11,040 --> 00:00:15,760
vegetarian or what are my thoughts on vegetarianism.

4
00:00:15,760 --> 00:00:24,120
And this comes at an interesting time just as I was intending to reply to this question.

5
00:00:24,120 --> 00:00:35,200
That's now come up in the news that the founder of Facebook, Mark Zuckerberg, has adopted

6
00:00:35,200 --> 00:00:46,560
the philosophy that we're often confronted with that if one is going to consume the

7
00:00:46,560 --> 00:00:56,320
flesh of other animals, then one should undertake to take the life of those animals oneself.

8
00:00:56,320 --> 00:01:04,160
So one should be the killer rather than as the theory goes allowing someone else to kill

9
00:01:04,160 --> 00:01:06,320
for you.

10
00:01:06,320 --> 00:01:22,080
So this makes this question quite topical or quite timely.

11
00:01:22,080 --> 00:01:30,680
So just to give my thoughts on vegetarianism, it's not a simple discussion and this isn't

12
00:01:30,680 --> 00:01:37,080
a question in fact that should be taken lightly.

13
00:01:37,080 --> 00:01:46,640
Surprisingly it might seem like it's not a terribly important topic, but it involves

14
00:01:46,640 --> 00:01:57,960
the very core issue of Buddhist ethics and an understanding of it requires an answer

15
00:01:57,960 --> 00:02:03,800
of this question to this question requires an understanding of what exactly Buddhist ethics

16
00:02:03,800 --> 00:02:11,080
are and the sort of philosophy that the Buddha had.

17
00:02:11,080 --> 00:02:15,840
So first of all I want to say that this is probably one of those questions that's going

18
00:02:15,840 --> 00:02:22,720
to get negative responses from both sides, from the people who believe Buddhists should

19
00:02:22,720 --> 00:02:29,720
be vegetarian and from those who believe, not Buddhist, but those who sort of believe

20
00:02:29,720 --> 00:02:40,960
that people like Mark Zuckerberg are correct, that it's better to kill for, you know, better

21
00:02:40,960 --> 00:02:49,400
to be the person who does the killing rather than be hypocritical and eat meat, eat meat

22
00:02:49,400 --> 00:02:58,000
but don't agree with killing if your stance is, you know, that eating meat is okay

23
00:02:58,000 --> 00:03:03,920
then you are not, therefore, able to hold the stance that killing is not okay and therefore

24
00:03:03,920 --> 00:03:12,240
you should actually be better off to kill for yourself rather than have the recovery.

25
00:03:12,240 --> 00:03:20,320
So I don't know what to say that to be clear that I'm aware that this isn't going

26
00:03:20,320 --> 00:03:24,600
to be universally held and I don't want people to think that my view is the view held

27
00:03:24,600 --> 00:03:37,480
by all Buddhists or by all people who follow the Buddha's teaching or who consider themselves

28
00:03:37,480 --> 00:03:44,920
to be Buddhist, let's put it that way. So let's talk, we have to talk about two things here.

29
00:03:44,920 --> 00:03:57,320
The first one is an understanding of Buddhist ethics. Buddhist ethics are based on ultimate

30
00:03:57,320 --> 00:04:02,920
reality and this shouldn't be a scary term, but if you've been following the meditation

31
00:04:02,920 --> 00:04:14,440
teachings that I've teacher that are presented by the Buddha himself, what it means simply

32
00:04:14,440 --> 00:04:21,080
is the experience of the reality around us in the present moment. So Buddhist ethics

33
00:04:21,080 --> 00:04:29,240
are not intellectual, they are not passed down by some higher authority, they're scientific.

34
00:04:29,240 --> 00:04:36,280
Everything is unethical, not because of specifically the suffering that it brings to the

35
00:04:36,280 --> 00:04:43,240
victim in the case where there is a victim or to other people, they're not unethical

36
00:04:43,240 --> 00:04:53,440
because they call suffering. They're unethical because they change the mind of the person

37
00:04:53,440 --> 00:04:59,160
who undertakes them. They lead specifically, they lead to one's own detriment. Something

38
00:04:59,160 --> 00:05:09,680
is ethical and simply because it, by its very nature, degrades the state of one's mind.

39
00:05:09,680 --> 00:05:22,160
So when Mark Zuckerberg cuts the throat of a goat, immediately what comes to my mind

40
00:05:22,160 --> 00:05:26,560
is not the suffering that the goat is going through because that's actually relatively

41
00:05:26,560 --> 00:05:32,120
minor. It's not a big deal to have your throat slit. That might seem like a horrific

42
00:05:32,120 --> 00:05:38,720
thing to have happened and indeed it is, but relative to the state of mind of a person

43
00:05:38,720 --> 00:05:47,000
who would do that to another living being, I find that far scarier and that's not a

44
00:05:47,000 --> 00:05:52,120
theoretical, it's not an intellectual view. It's one that comes from looking at my own

45
00:05:52,120 --> 00:05:58,720
history and the things that I've done and being able to compare them with these sorts

46
00:05:58,720 --> 00:06:04,200
of action. I cringe at the thought of someone doing that to another living being and that

47
00:06:04,200 --> 00:06:14,200
comes from cringing at my own actions and being able to see the results of my own actions

48
00:06:14,200 --> 00:06:20,280
and how they've affected me and how wrong it was, feeling bad about them from the core

49
00:06:20,280 --> 00:06:28,960
of my being. It comes from an ability to empathize which we gain through the meditation practice

50
00:06:28,960 --> 00:06:35,160
because someone who empathizes with the suffering of others will never do these things and

51
00:06:35,160 --> 00:06:45,360
it's only through pushing away the repression of our empathy for the suffering of others

52
00:06:45,360 --> 00:06:49,960
that we're able to do these sorts of things. It's a horrific thing to kill another

53
00:06:49,960 --> 00:06:59,720
living being and the fact that that's not clear to us is just a sign that we haven't

54
00:06:59,720 --> 00:07:06,120
taken the time to look at the nature of our actions. People who take an intellectual point

55
00:07:06,120 --> 00:07:11,120
of view will often see nothing wrong with killing and in fact will often postulate situations

56
00:07:11,120 --> 00:07:17,960
such as this where killing is the preferable method. Now when you look at it in this

57
00:07:17,960 --> 00:07:23,280
way and indeed if you should understand that this is how the Buddha would have us look

58
00:07:23,280 --> 00:07:29,880
at ethics then to compare the two situations, before we talk about vegetarianism, comparing

59
00:07:29,880 --> 00:07:37,240
the two situations, one who a person who eats meat and a person who kills from this point

60
00:07:37,240 --> 00:07:43,240
of view, putting aside any intellectual arguments and I'll try to get to that in a second,

61
00:07:43,240 --> 00:07:49,360
simply from the experiential point of view which is the true, the proper view according

62
00:07:49,360 --> 00:07:55,520
to the Buddha, whether you agree with it or not. It's a world of difference. You can eat

63
00:07:55,520 --> 00:08:02,800
meat without any unwholesomeness in your mind. It's possible. I think that that should

64
00:08:02,800 --> 00:08:08,160
go without saying that a person can be mindful of something, no matter what it is, putting

65
00:08:08,160 --> 00:08:17,440
it in your mouth, swallowing it and tasting it and making use of the energy without having

66
00:08:17,440 --> 00:08:27,720
any malice or any thoughts of causing suffering to any being at all and it's only when

67
00:08:27,720 --> 00:08:35,160
we become intellectual about it and when we make the assumption, for instance that more

68
00:08:35,160 --> 00:08:40,960
beings are going to have to die or someone is going to have to kill for us or I guess

69
00:08:40,960 --> 00:08:45,400
in the sense of people like Mark Zuckerberg and many people have this view that one

70
00:08:45,400 --> 00:08:53,880
should kill for oneself. The idea that you won't respect the significance of the act,

71
00:08:53,880 --> 00:09:04,560
but really the person who kills cannot help but crush the experience of state of crushing

72
00:09:04,560 --> 00:09:13,440
and repressing our natural empathy for each other as living beings and we'll degrade

73
00:09:13,440 --> 00:09:19,800
the state of their mind and this is empirical. This is something most people can't see.

74
00:09:19,800 --> 00:09:24,320
I have a story that I always tell about when I was young and I was hunting with my father.

75
00:09:24,320 --> 00:09:33,800
I wasn't always a Buddhist. I killed a deer actually once which is my claim to shame,

76
00:09:33,800 --> 00:09:40,080
but it was very difficult. It was a very difficult thing to pull the trigger which was

77
00:09:40,080 --> 00:09:45,640
strange because I had no qualms about it at the time. I wasn't at all afraid of hurting

78
00:09:45,640 --> 00:09:55,280
other beings. I had some empathy but not anything nearly as advanced as it should have

79
00:09:55,280 --> 00:10:02,640
been. None the less my whole body shook and it was a difficult thing to do. After that

80
00:10:02,640 --> 00:10:07,880
it became a lot easier. After the first time as I got into it and got used to it, it came

81
00:10:07,880 --> 00:10:14,120
accustomed to it. It got a lot more difficult and the simply because of the repression that

82
00:10:14,120 --> 00:10:21,040
goes on and the degradation of the mind until you become numb to it. It's also something

83
00:10:21,040 --> 00:10:29,880
that's easy to see when you talk to soldiers or when we took peace studies in university

84
00:10:29,880 --> 00:10:36,360
and we studied war so we studied how it is that you get people to kill. They found that

85
00:10:36,360 --> 00:10:45,920
in the first world war there was an incredibly low accuracy rate for in terms of what do

86
00:10:45,920 --> 00:10:53,960
you call bullets shot and enemies killed or people killed. This was because they found out

87
00:10:53,960 --> 00:10:58,000
that most of the soldiers didn't want to kill their fellow human beings and they would

88
00:10:58,000 --> 00:11:03,720
shoot over each other's heads. Apparently this is a document in fact. This frustrated

89
00:11:03,720 --> 00:11:08,360
the leaders greatly and so they tried to figure out how it is that you can get your

90
00:11:08,360 --> 00:11:16,800
soldiers to kill. How can you teach someone to kill? The way to do it is to destroy the

91
00:11:16,800 --> 00:11:23,200
empathy that they might have for the other human being by creating distance between them.

92
00:11:23,200 --> 00:11:31,360
This is why you have all these derogatory terms for the enemy. You hear that in the

93
00:11:31,360 --> 00:11:36,800
early part of the last century. A lot of propaganda and derogatory terms used to refer

94
00:11:36,800 --> 00:11:44,000
to other, you know, the enemies. The names for the Germans, the names of the Japanese

95
00:11:44,000 --> 00:11:52,160
and so on. The movies that show them vicious and mean and cruel and trying to, so they did

96
00:11:52,160 --> 00:11:58,480
everything they could to create a distance and destroy the ability to empathize with the

97
00:11:58,480 --> 00:12:03,440
other, the other living being and they found that this helped. What they found that helped

98
00:12:03,440 --> 00:12:10,480
even more is when they started using more technologically advanced means of killing each

99
00:12:10,480 --> 00:12:15,960
other, bombing, because bombing then they, with the soldiers would say is it's just like

100
00:12:15,960 --> 00:12:22,440
a video game and so they had very little qualms about dropping bombs and so on. I think

101
00:12:22,440 --> 00:12:31,360
this points quite clearly to the relationship between empathy and the state of killing,

102
00:12:31,360 --> 00:12:36,720
the act of killing and how you need to crush your empathy in order to do it.

103
00:12:36,720 --> 00:12:46,000
So I think people who use the computer a lot, it gets easier for them because if you're

104
00:12:46,000 --> 00:12:52,040
not careful, it's easy for you to build up intense states of concentration that are able

105
00:12:52,040 --> 00:13:01,360
to crush your emotions, crush your sense of reality and empathy and so it's very easy

106
00:13:01,360 --> 00:13:06,080
to think logically that this is somehow a better way of doing things.

107
00:13:06,080 --> 00:13:11,360
So okay, that's on the one side, but this also helps us to understand what the Buddhist

108
00:13:11,360 --> 00:13:20,120
answer to the question, should we be vegetarians? The answer is no, the answer is in regards

109
00:13:20,120 --> 00:13:24,080
to the requirement for being a vegetarian, no, there is no requirement and there shouldn't

110
00:13:24,080 --> 00:13:30,560
be a requirement. And in fact, I would postulate, but this is actually a little bit of

111
00:13:30,560 --> 00:13:37,120
a different argument. I would postulate that it's better not to be totally vegetarian.

112
00:13:37,120 --> 00:13:44,120
But I would advocate is passive vegetarianism and that's in the sense of not desiring

113
00:13:44,120 --> 00:13:48,560
meat, but this is actually a different argument. I want to finish the first argument.

114
00:13:48,560 --> 00:13:57,720
The first argument is pointing out that in terms of the empirical nature or the experiential

115
00:13:57,720 --> 00:14:02,360
nature of eating meat and killing, it's two completely different things. A person who eats

116
00:14:02,360 --> 00:14:08,960
meat can do so free from guilt. This is my understanding, this is what we have in the earliest

117
00:14:08,960 --> 00:14:15,440
Buddhist texts that I'm aware of, not in all Buddhist texts, but this is the understanding

118
00:14:15,440 --> 00:14:22,480
that I follow. And I think in my mind it's quite clear even though many people will disagree.

119
00:14:22,480 --> 00:14:38,680
So the other argument that has to do with the intellectual side of eating meat. So the

120
00:14:38,680 --> 00:14:48,480
argument goes that, irregardless of the states in the person's mind at the time, that

121
00:14:48,480 --> 00:15:00,240
there is an ethical responsibility to refrain from eating meat because animals will

122
00:15:00,240 --> 00:15:07,360
be killed. So this is why people, I think there's an understanding generally that you're

123
00:15:07,360 --> 00:15:12,960
not eating meat viciously thinking, you know, die, die, die, die because you know it's

124
00:15:12,960 --> 00:15:18,080
already dead, but the ethics that they're talking about, which I submit is not really

125
00:15:18,080 --> 00:15:28,280
true Buddhist ethics, it is that it's going to lead to beings being killed. And if we

126
00:15:28,280 --> 00:15:34,080
don't eat meat and fewer beings will be killed. Now, this is why I thought it's important

127
00:15:34,080 --> 00:15:42,280
to explain the nature of Buddhist ethics as I follow it, as I see it, because this

128
00:15:42,280 --> 00:15:52,240
sort of ethics, well this sort of ethics really doesn't hold up in my mind because all

129
00:15:52,240 --> 00:15:57,120
beings have to die. As I said, the real problem isn't that beings are killed or so on because

130
00:15:57,120 --> 00:16:03,120
we all have to die. More suffering, less suffering is really all relative. And it's really

131
00:16:03,120 --> 00:16:08,600
not the biggest problem because eventually we're all going to have to learn to understand

132
00:16:08,600 --> 00:16:14,840
and to let go and to not be affected by suffering. So it's not necessarily the case that

133
00:16:14,840 --> 00:16:19,200
suffering is going to be a bad thing or something we have to worry about. What we really

134
00:16:19,200 --> 00:16:29,400
have to worry about is the state of our minds. So a person who eats meat does so with

135
00:16:29,400 --> 00:16:36,320
a pure mind, a person who kills obviously does so with an impure mind. And that's really

136
00:16:36,320 --> 00:16:47,240
where the difference lies. A person who eats meat is not thinking or has no real connection

137
00:16:47,240 --> 00:16:56,960
with the act of killing that goes on. The idea that somehow you can stop beings from being

138
00:16:56,960 --> 00:17:04,160
killed is really a bit shortsighted, I think, considering the fact that when you look at

139
00:17:04,160 --> 00:17:16,320
the world around us, the amount of killing that goes on is something incredible. The magnitude

140
00:17:16,320 --> 00:17:24,720
of the problem, if you're a solution or if your goal is to find a solution to the problem

141
00:17:24,720 --> 00:17:31,600
of killing, then just stepping out your door is going to cause difficult for you because

142
00:17:31,600 --> 00:17:42,080
of the murder that goes on in every quarter by every type of being on earth, by most

143
00:17:42,080 --> 00:17:48,760
types of beings, if you're not a predator, you're prey for the most part. And so this

144
00:17:48,760 --> 00:17:55,440
points to the second part of the argument that Buddhism is a path towards simplicity.

145
00:17:55,440 --> 00:17:59,520
This sort of intellectual argument has no place in Buddhism because we're not trying

146
00:17:59,520 --> 00:18:04,360
to change the world. We're not trying to fix the world. The world is, by nature, it's

147
00:18:04,360 --> 00:18:11,600
something that we've created out of our ignorance. It's something that we've put together

148
00:18:11,600 --> 00:18:15,440
based on our craving, based on our mission, based on the idea that somehow we're going

149
00:18:15,440 --> 00:18:24,960
to create a stable and lasting state of existence. And what the Buddha realizes that you

150
00:18:24,960 --> 00:18:31,960
can't do it because everything is unstable and changing. And because the more you try

151
00:18:31,960 --> 00:18:41,840
to cling to things is stable, the more you create suffering. This is another part of the

152
00:18:41,840 --> 00:18:48,560
argument. The third thing that I wanted to say, so the second is that we're not, these

153
00:18:48,560 --> 00:18:55,960
sort of intellectual arguments. The point is that if you open up the can of worms of the

154
00:18:55,960 --> 00:19:03,160
intellectual responsibility to not eat meat, then you'll never find a, you'll never find

155
00:19:03,160 --> 00:19:09,120
rest because every part of your life, in some way, causes suffering for other beings.

156
00:19:09,120 --> 00:19:14,520
When you drive a car, you shouldn't drive a car because most likely the beings are going

157
00:19:14,520 --> 00:19:21,960
to die as a result of you driving a car. You shouldn't use most beauty products or razor

158
00:19:21,960 --> 00:19:26,280
blades because they've all been tested on animals. And there are people who think like

159
00:19:26,280 --> 00:19:34,520
this, but it goes to such, there is no, there is no line you can draw. And I would submit

160
00:19:34,520 --> 00:19:42,280
that there is no need to find a line. The line is quite clear in terms of where our intentions

161
00:19:42,280 --> 00:19:46,680
are and this is throughout the Buddhist teaching. What I said, when you step on an end,

162
00:19:46,680 --> 00:19:50,600
if you didn't know you were stepping on the end, you're not guilty of anything. The

163
00:19:50,600 --> 00:19:54,920
point is what goes on in your mind because this is what's going to change who you are

164
00:19:54,920 --> 00:20:03,520
and this is what's going to affect your future. It's what's going to actually, to change

165
00:20:03,520 --> 00:20:09,680
the world because if our minds are pure, we'll never have any desire to kill. If our mind

166
00:20:09,680 --> 00:20:15,080
is pure, we'll change the minds of other people. We will help the people around us

167
00:20:15,080 --> 00:20:21,880
to decide that killing is wrong and so on. And so the problem will never arise. The

168
00:20:21,880 --> 00:20:27,320
point is to stop people from wanting to kill, not to stop people from wanting to eat

169
00:20:27,320 --> 00:20:36,800
meat. So, but why I say I think passive vegetarianism is good and what that means, there

170
00:20:36,800 --> 00:20:43,800
is, so this is the third part, that passive vegetarianism, I think is a good thing and

171
00:20:43,800 --> 00:20:49,600
I don't think this is nearly as important but it's something for us to think about because

172
00:20:49,600 --> 00:20:57,280
a person who wants to eat meat, a person who is actively seeking out meat, is doing something

173
00:20:57,280 --> 00:21:04,560
that should be, we should all be aware of. They are guilty of something and a fairly

174
00:21:04,560 --> 00:21:11,920
extreme form of karma, of this type of karma. The type of karma I'm thinking of is

175
00:21:11,920 --> 00:21:18,800
the karma based on greed, based on desire and it's a prime example of how desire leads

176
00:21:18,800 --> 00:21:29,840
to the degradation of our world. Our desire for meat has no doubt done a lot to change

177
00:21:29,840 --> 00:21:37,120
the nature of this earth. The fact that we do kill, the fact that there is an incredible

178
00:21:37,120 --> 00:21:49,760
amount of killing going on, has degraded the world to a great extent. They say that

179
00:21:49,760 --> 00:21:57,640
the only reason people kill is because people want desire for meat and therefore people

180
00:21:57,640 --> 00:22:03,760
are killing, but that's a simplistic thing to say. People kill because they have

181
00:22:03,760 --> 00:22:07,440
defilements in their mind because they don't understand that killing is wrong. They're

182
00:22:07,440 --> 00:22:11,360
the ones who are guilty of the killing, the person who killed. The people who have

183
00:22:11,360 --> 00:22:21,960
greed, who push them to do it in a sense or make it viable for them to do, have some

184
00:22:21,960 --> 00:22:33,240
responsibility for the situation, but are not guilty of murdering. When a person desires

185
00:22:33,240 --> 00:22:39,280
to eat meat, if you're someone who is going out of your way to find meat and this might

186
00:22:39,280 --> 00:22:45,880
even include buying meat, although I would consider that to be a little more neutral in

187
00:22:45,880 --> 00:22:49,560
the sense that you go to the grocery store and you buy the food that you know is going

188
00:22:49,560 --> 00:22:58,240
to be healthy. When a person is actively seeking it out, when a person is actively looking

189
00:22:58,240 --> 00:23:06,960
for meat or encouraging people asking for it and so on, then there is a problem. There

190
00:23:06,960 --> 00:23:16,280
is the degradation. The only type of vegetarianism that I really wouldn't advise is the

191
00:23:16,280 --> 00:23:25,480
active vegetarianism where you refuse meat when it's given to you. As monks, this is the

192
00:23:25,480 --> 00:23:35,720
type of vegetarianism that we don't subscribe to. As monks, we are guilty if we ask for

193
00:23:35,720 --> 00:23:41,440
meat, if we desire meat, if we're looking for it, then this is considered to be an

194
00:23:41,440 --> 00:23:49,080
offense against our monastic code. But we are expected to eat the food that people give

195
00:23:49,080 --> 00:23:55,880
us. And I think this is the line in the sand, the line that we should draw because at

196
00:23:55,880 --> 00:24:03,800
this point, there is no guilt and no responsibility. The only way I can see that a person

197
00:24:03,800 --> 00:24:09,640
has responsibility is not because intellectually somehow they are contributing. But because

198
00:24:09,640 --> 00:24:16,040
they are actively encouraging, promoting, increasing the demand because they actually

199
00:24:16,040 --> 00:24:22,840
demand it. When a person takes whatever food they are given, and this doesn't just

200
00:24:22,840 --> 00:24:27,840
apply to monks. So when we go to someone's house and they offer us food and they offer

201
00:24:27,840 --> 00:24:41,360
us meat, this is considered. We are a passive recipient of the meat. We're not actively

202
00:24:41,360 --> 00:24:46,720
seeking it out and we have no greed for it and we eat it for our nourishment. I would

203
00:24:46,720 --> 00:25:01,280
say that this is the word ethically neutral, an ethically neutral act. And so for this

204
00:25:01,280 --> 00:25:05,360
reason, I would say that no, it's not necessary to be vegetarian. I would say that there

205
00:25:05,360 --> 00:25:15,720
is an argument that we should not actively seek out meat. But if a person is looking

206
00:25:15,720 --> 00:25:21,760
to practice meditation and follow the Buddhist teaching, I would say that even could

207
00:25:21,760 --> 00:25:26,840
be that buying meat is not a real problem. If your intention is not to specifically buy

208
00:25:26,840 --> 00:25:34,600
meat, but simply to buy food that is going to sustain you with the only object for the

209
00:25:34,600 --> 00:25:45,040
practice of meditation. Because as I said, the point of the Buddhist teaching is the

210
00:25:45,040 --> 00:25:50,920
simplification of our lives, that our lives should be simply about our experience. When

211
00:25:50,920 --> 00:25:54,400
we see it's only seeing, when we hear it's only hearing the Buddha said, this is the

212
00:25:54,400 --> 00:25:59,520
way to enlightenment. The way we should train ourselves is that seeing becomes only seeing,

213
00:25:59,520 --> 00:26:06,800
hearing becomes only, hearing smelling, tasting, feeling, and thinking. So meat is

214
00:26:06,800 --> 00:26:19,520
something that only has significance in an intellectual sense. On an experiential level,

215
00:26:19,520 --> 00:26:25,760
it's something that has been abandoned. There is no mind involved. So there is no empathy

216
00:26:25,760 --> 00:26:35,040
and no sympathy and no relation to ourselves. No connection to ourselves that needs to

217
00:26:35,040 --> 00:26:41,360
arise in the mind. It's the physical that has been abandoned by the being to whom it

218
00:26:41,360 --> 00:26:51,800
once belonged. So that is my understanding of vegetarianism. It's not quite as simple as one

219
00:26:51,800 --> 00:27:04,680
I think. It does show that Buddhist ethics are actually quite logical, scientific, and quite

220
00:27:04,680 --> 00:27:11,120
simple, easy to understand, especially if you're practicing meditation. The only time it would

221
00:27:11,120 --> 00:27:19,560
be unethical is in the same way that anything that has to do with involved greed is unethical.

222
00:27:19,560 --> 00:27:25,240
Anytime we have greed, we can see why the world is being changed in so many ways is totally

223
00:27:25,240 --> 00:27:31,280
about greed. Our greed for oil, our greed for land, our greed for gold, money, our greed

224
00:27:31,280 --> 00:27:38,720
for computers, and electronics. This is a good example, for instance. The electronics industry

225
00:27:38,720 --> 00:27:43,880
and all of the metals and so on is actually creating great conflict in the world. So there

226
00:27:43,880 --> 00:27:50,320
are people who will say that using the electronic devices that we have with the computer

227
00:27:50,320 --> 00:27:57,160
chips and so on with all of their metals that have to be mined and therefore have to

228
00:27:57,160 --> 00:28:02,680
be fought over. So the people are using these are actually contributing to the conflicts

229
00:28:02,680 --> 00:28:10,600
in places like many African nations where they are doing mining and there's not one group,

230
00:28:10,600 --> 00:28:16,520
there's everyone's fighting over them and there's a lot of suffering that goes on.

231
00:28:16,520 --> 00:28:20,880
My point is that it's not direct because a person obviously who uses the electronic

232
00:28:20,880 --> 00:28:26,360
devices has no thought in their mind that they're doing that. The point is those people

233
00:28:26,360 --> 00:28:33,160
who are fighting over these things are, if they were to give up then there would obviously

234
00:28:33,160 --> 00:28:38,840
be no problem. When we let go and the people who are encouraging the fighting who are

235
00:28:38,840 --> 00:28:47,000
pushing to get these minerals and so on. But yet we can see how our greed affects that,

236
00:28:47,000 --> 00:28:59,960
how our need for these high tech gadgets and so on. So the unethical part of our being that we

237
00:28:59,960 --> 00:29:07,000
should give up if we can use things like technology that exists without greed, without attachment,

238
00:29:07,000 --> 00:29:13,800
without need for them, and without need for really everything. Then there would never be any need

239
00:29:13,800 --> 00:29:21,080
to go to war for anything. We'd all be able to use what we have but you can see it's totally

240
00:29:21,080 --> 00:29:29,720
based on greed that we're fighting over, fighting over rocks. As an example and so many other

241
00:29:29,720 --> 00:29:39,080
things, this is a different aspect of the issue of vegetarianism as it is an aspect of all of

242
00:29:39,080 --> 00:29:49,240
our actions, all of our actions, all of our lives that has to do with our desires and our need

243
00:29:49,240 --> 00:29:55,240
for this or that. So I hope this has been interesting and helped to put somehow in perspective

244
00:29:55,240 --> 00:30:01,080
the issue of vegetarianism. I think it's sad that people might feel guilty when they eat meat

245
00:30:02,920 --> 00:30:08,280
unreasonably thinking that somehow they're responsible for some sort of killing.

246
00:30:09,160 --> 00:30:14,840
And I know there are many people who feel that way and actually probably quite disagree with

247
00:30:14,840 --> 00:30:20,520
what I'm saying here. But I would ask those people to look in their own minds and to see the state

248
00:30:20,520 --> 00:30:25,480
of mind, even as they're listening to this video, because it's the state of our mind that is the most

249
00:30:25,480 --> 00:30:31,560
important. It's the only thing that's going to determine our future. If I eat meat, I'm not worried

250
00:30:31,560 --> 00:30:38,280
about it because it means nothing to me. I know that the being has gone. But when I see living beings

251
00:30:38,280 --> 00:30:47,480
even farm animals or so on, knowing that they have to be slaughtered, it's really a totally

252
00:30:47,480 --> 00:30:52,440
different state of affairs. I could never do some of the things that I did before

253
00:30:54,600 --> 00:31:00,520
because I understand the suffering involved. It is quite clear, I think, if you meditate to see the

254
00:31:00,520 --> 00:31:07,640
difference that you would never wish suffering on another being. If you understand the nature of

255
00:31:07,640 --> 00:31:12,520
suffering and the nature of the defilements that lead us to cause suffering for others.

256
00:31:12,520 --> 00:31:19,560
So I think that's enough for this question, thanks for tuning in from all the best.

