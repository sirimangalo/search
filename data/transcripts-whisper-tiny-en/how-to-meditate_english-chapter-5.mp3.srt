1
00:00:00,000 --> 00:00:06,600
How to meditate by you to Domobiku, read by Adirokes.

2
00:00:06,600 --> 00:00:10,400
Chapter 5 Mindful Prostration

3
00:00:10,400 --> 00:00:16,880
In this chapter, I will explain a third technique of meditation used as a preparatory exercise

4
00:00:16,880 --> 00:00:20,160
before walking and sitting meditation.

5
00:00:20,160 --> 00:00:23,960
This technique is called mindful prostration.

6
00:00:23,960 --> 00:00:29,160
It is an optional practice and may be omitted if desired.

7
00:00:29,160 --> 00:00:35,120
Prostration is a practice common to followers of various, prostration is a practice common

8
00:00:35,120 --> 00:00:39,960
to followers of various religious traditions around the world.

9
00:00:39,960 --> 00:00:45,360
In Buddhist countries, for example, prostration is used as a means of paying respect to

10
00:00:45,360 --> 00:00:50,800
one's parents, teachers, or figures of religious reverence.

11
00:00:50,800 --> 00:00:56,080
In other religious traditions, prostration may be used as a form of reverence towards an

12
00:00:56,080 --> 00:01:03,720
object of worship, a God, an angel, or some saintly figure, for example.

13
00:01:03,720 --> 00:01:09,480
Here the prostration is a means of paying respect to the meditation practice itself.

14
00:01:09,480 --> 00:01:17,320
It can be thought of as a means of creating humble and sincere appreciation for the practice.

15
00:01:17,320 --> 00:01:23,240
Reminding one that meditation is not just a hobby or pastime, but rather an important

16
00:01:23,240 --> 00:01:26,600
training worthy of respect.

17
00:01:26,600 --> 00:01:33,040
More importantly, though mindful prostration is a useful preparatory exercise, since it

18
00:01:33,040 --> 00:01:39,760
involves repetitive movements of various parts of the body, forcing one to focus on the

19
00:01:39,760 --> 00:01:42,400
activity at each moment.

20
00:01:42,400 --> 00:01:46,960
The technique of mindful prostration is performed as follows.

21
00:01:46,960 --> 00:01:52,640
You can find these illustrations in the appendix of the online or print version.

22
00:01:52,640 --> 00:01:58,800
Then by sitting on the knees, either on the toes or on the tops of the feet, place the

23
00:01:58,800 --> 00:02:05,040
hands palm down on the thighs with back straight and eyes open.

24
00:02:05,040 --> 00:02:10,680
Begin by turning the right hand 90 degrees on the thigh until it is perpendicular to the

25
00:02:10,680 --> 00:02:16,440
floor, keeping the mind focused on the movement of the hand.

26
00:02:16,440 --> 00:02:23,360
As the hand begins to turn, note turning, when the hand is halfway through the turning

27
00:02:23,360 --> 00:02:31,400
motion, again note turning, and when the hand completes the movement, note a third time

28
00:02:31,400 --> 00:02:33,600
turning.

29
00:02:33,600 --> 00:02:39,480
The word is repeated three times in order to create awareness of the motion throughout

30
00:02:39,480 --> 00:02:46,160
all three stages of motion, beginning, middle, and end.

31
00:02:46,160 --> 00:02:52,960
Next, raise the right hand to the chest, stopping right before the thumb touches the chest,

32
00:02:52,960 --> 00:03:04,080
noting, raising, raising, then touch the edge of the thumb to the chest, noting, touching,

33
00:03:04,080 --> 00:03:10,280
touching, touching, while the thumb touches the chest.

34
00:03:10,280 --> 00:03:19,120
Then repeat this sequence with the left hand, turning, turning, raising, raising,

35
00:03:19,120 --> 00:03:24,520
raising, touching, touching, touching.

36
00:03:24,520 --> 00:03:32,480
The left hand should touch not only the chest, but also the right hand, palm to palm.

37
00:03:32,480 --> 00:03:42,760
Next, bring both hands up to the forehead, noting, raising, raising, raising, then touching,

38
00:03:42,760 --> 00:03:49,820
touching, touching, when the thumb nails touch the forehead, then bring the hands back

39
00:03:49,820 --> 00:04:00,960
down to the chest, noting, lowering, lowering, lowering, touching, touching, touching.

40
00:04:00,960 --> 00:04:09,680
Next comes the actual frustration, first bend the back down to a 45-degree angle, noting,

41
00:04:09,680 --> 00:04:13,760
bending, bending, bending.

42
00:04:13,760 --> 00:04:21,800
Then lower the right hand to the floor in front of the knees, saying, lowering, lowering,

43
00:04:21,800 --> 00:04:30,720
lowering, touching, touching, touching, still keeping it perpendicular to the floor.

44
00:04:30,720 --> 00:04:37,320
This time, with the edge of the little finger touching the floor, finally turn the hand

45
00:04:37,320 --> 00:04:45,560
palm down to cover the floor, noting, covering, covering, covering.

46
00:04:45,560 --> 00:04:55,280
Then repeat this sequence with the left hand, lowering, lowering, lowering, touching, touching,

47
00:04:55,280 --> 00:05:00,880
covering, covering, covering.

48
00:05:00,880 --> 00:05:06,760
The hands should now be side by side, with the thumbs touching, and approximately four

49
00:05:06,760 --> 00:05:10,920
inches between index fingers.

50
00:05:10,920 --> 00:05:18,560
Next lower the head to touch the thumbs, saying, bending, bending, bending.

51
00:05:18,560 --> 00:05:25,640
As you bend the back and touching, touching, touching, when the forehead actually touches

52
00:05:25,640 --> 00:05:33,680
the thumbs, then raise the head back again until the arms are straight, saying, raising,

53
00:05:33,680 --> 00:05:37,120
raising, raising.

54
00:05:37,120 --> 00:05:41,240
This is the first frustration.

55
00:05:41,240 --> 00:05:46,640
Once the arms are straight, start from the beginning to repeat the entire sequence a second

56
00:05:46,640 --> 00:05:55,160
time, except starting with the hands on the floor, noting, turning, turning, turning.

57
00:05:55,160 --> 00:06:04,840
As you turn the right hand, then raising, raising, raising, touching, touching, touching.

58
00:06:04,840 --> 00:06:18,120
When the left hand, turning, turning, raising, raising, raising, touching, touching, touching.

59
00:06:18,120 --> 00:06:24,040
As you raise the left hand this time, you should also raise the back from a 45 degree angle

60
00:06:24,040 --> 00:06:28,400
to a straight upright position.

61
00:06:28,400 --> 00:06:33,600
It is not necessary to acknowledge this movement separately, simply straighten the back

62
00:06:33,600 --> 00:06:37,920
as the left hand comes up to the chest.

63
00:06:37,920 --> 00:06:46,600
Then raise both hands up to the forehead again, noting, raising, raising, raising, touching,

64
00:06:46,600 --> 00:06:56,040
touching, touching, and down to the chest again, lowering, lowering, lowering, touching, touching,

65
00:06:56,040 --> 00:06:57,840
touching.

66
00:06:57,840 --> 00:07:03,840
Then bend the back again, bending, bending, bending.

67
00:07:03,840 --> 00:07:13,640
Finally lower the hands again one by one, lowering, lowering, lowering, touching, touching,

68
00:07:13,640 --> 00:07:27,460
covering, covering, covering, lowering, lowering, lowering, touching, touching, touching, covering,

69
00:07:27,460 --> 00:07:30,040
covering, covering.

70
00:07:30,040 --> 00:07:40,720
Again touch the thumbs with the forehead bending, bending, bending, touching, touching, touching,

71
00:07:40,720 --> 00:07:46,720
and back up again, raising, raising, raising.

72
00:07:46,720 --> 00:07:58,720
This is the second prostration, after which a third prostration should be performed in the exact same manner, repeating the above one more time.

73
00:07:58,720 --> 00:08:13,720
After the third prostration, come up from the floor, starting with the right hand as before, turning, turning, turning, raising, raising, raising, touching, touching, touching.

74
00:08:13,720 --> 00:08:28,140
touching, and the left hand turning, turning, turning, raising, raising, raising, touching, touching, touching, touching.

75
00:08:28,140 --> 00:08:43,660
Then bring the hands up to the forehead again as before, raising, raising, raising, touching, touching, touching, and back down to the chest, lowering,

76
00:08:43,660 --> 00:08:50,580
lowering, lowering, touching, touching, touching.

77
00:08:50,580 --> 00:08:56,900
This time, however, instead of bending to do a fourth prostration, bring the hands down

78
00:08:56,900 --> 00:09:04,340
one at a time to rest on the thighs, returning them to their original position, starting

79
00:09:04,340 --> 00:09:32,820
with the right hand, note, lowering, lowering, lowering, touching, touching, touching, touching,

80
00:09:32,820 --> 00:09:33,820
touching.

81
00:09:33,820 --> 00:09:39,500
One should continue on with the walking and sitting meditations in that order.

82
00:09:39,500 --> 00:09:45,940
It is important that as one changes position, one should maintain mindfulness, not standing

83
00:09:45,940 --> 00:09:50,940
up or sitting down hastily or on mindfully.

84
00:09:50,940 --> 00:10:00,940
Before beginning to stand up, one should note, sitting, sitting, and then, standing, standing.

85
00:10:00,940 --> 00:10:05,060
As one lifts the body to a standing position.

86
00:10:05,060 --> 00:10:11,180
Once standing, continue immediately with the walking meditation, so that clear awareness

87
00:10:11,180 --> 00:10:14,700
of the present moment remains unbroken.

88
00:10:14,700 --> 00:10:21,580
In this way, the mindful prostration will act as a support for the walking meditation, just

89
00:10:21,580 --> 00:10:27,980
as the walking meditation will act as a support for the sitting meditation.

90
00:10:27,980 --> 00:10:33,780
During an intensive meditation course, students are instructed to practice all three techniques

91
00:10:33,780 --> 00:10:35,860
in this manner.

92
00:10:35,860 --> 00:10:43,940
Upon completion, they are instructed to rest for a short time, and then start up again.

93
00:10:43,940 --> 00:10:49,740
Upon completion, they are instructed to rest for a short time, and then start again from

94
00:10:49,740 --> 00:10:56,100
the beginning, practicing round after round for the duration of the lesson.

95
00:10:56,100 --> 00:10:59,620
Say 1-24 hour period.

96
00:10:59,620 --> 00:11:04,700
Once this period is over, one would meet with the teacher to report, and receive the

97
00:11:04,700 --> 00:11:11,540
next lesson, including more intricate walking and sitting techniques.

98
00:11:11,540 --> 00:11:16,460
Since this book is aimed towards giving the basics of meditation, advanced lessons will

99
00:11:16,460 --> 00:11:18,820
not be discussed here.

100
00:11:18,820 --> 00:11:23,620
Once one has mastered these basic techniques, one should seek guidance from a qualified

101
00:11:23,620 --> 00:11:28,340
instructor, if one wishes to pursue the practice further.

102
00:11:28,340 --> 00:11:34,100
If one is unable to enter a meditation course, one may begin by practicing these techniques

103
00:11:34,100 --> 00:11:41,060
once or twice a day, and contacting a teacher on a weekly or monthly basis, to obtain new

104
00:11:41,060 --> 00:11:47,420
lessons at a more gradual pace, according to a regimen agreed upon between teacher and

105
00:11:47,420 --> 00:11:49,780
student.

106
00:11:49,780 --> 00:11:54,300
This concludes the explanation of formal meditation practice.

107
00:11:54,300 --> 00:11:59,940
In the next and final chapter, I will discuss how to incorporate some of the concepts learned

108
00:11:59,940 --> 00:12:03,340
in this book into one's daily life.

109
00:12:03,340 --> 00:12:09,340
Thank you again for your interest, and again, I wish you peace, happiness, and freedom

110
00:12:09,340 --> 00:12:25,940
from suffering.

