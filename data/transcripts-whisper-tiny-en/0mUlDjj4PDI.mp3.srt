1
00:00:00,000 --> 00:00:05,000
Hi, and welcome back to Ask a Monk.

2
00:00:05,000 --> 00:00:09,000
Today's question comes from Johnny from Chicago.

3
00:00:09,000 --> 00:00:12,000
My parents aren't open to me becoming a Buddhist.

4
00:00:12,000 --> 00:00:14,000
I haven't talked to them because I'm nervous

5
00:00:14,000 --> 00:00:16,000
they will not accept my new religion.

6
00:00:16,000 --> 00:00:19,000
What are some tips to have them see eye to eye with me?

7
00:00:19,000 --> 00:00:22,000
Well, as I mentioned in a recent video,

8
00:00:22,000 --> 00:00:24,000
the first thing to say is that being a Buddhist

9
00:00:24,000 --> 00:00:33,000
is not really important to put it in one way.

10
00:00:33,000 --> 00:00:36,000
Being a good Buddhist is not about being Buddhist.

11
00:00:36,000 --> 00:00:40,000
Meaning the label that you give to it isn't so important.

12
00:00:40,000 --> 00:00:44,000
It's not necessary that you tell your parents that you're Buddhist,

13
00:00:44,000 --> 00:00:46,000
at least not in the beginning.

14
00:00:46,000 --> 00:00:50,000
And it's not even necessary that you identify yourself as being Buddhist.

15
00:00:50,000 --> 00:00:53,000
You can think of yourself as a follower of the Buddha,

16
00:00:53,000 --> 00:00:56,000
just like someone who follows the teachings of young

17
00:00:56,000 --> 00:00:59,000
or of Einstein or so on,

18
00:00:59,000 --> 00:01:02,000
is not considered to be following their religion,

19
00:01:02,000 --> 00:01:04,000
but simply their teachings.

20
00:01:04,000 --> 00:01:09,000
The best way to help people come to see eye to eye with you

21
00:01:09,000 --> 00:01:13,000
in regards to things like Buddhism is to live the teachings

22
00:01:13,000 --> 00:01:16,000
and have them see your example

23
00:01:16,000 --> 00:01:22,000
to really show them the benefits, because if your people

24
00:01:22,000 --> 00:01:24,000
like your parents are not seeing eye to eye with you,

25
00:01:24,000 --> 00:01:29,000
it generally means that you're not presenting a positive side

26
00:01:29,000 --> 00:01:31,000
to the Buddhist teaching.

27
00:01:31,000 --> 00:01:35,000
You're not yet at the stage where you can give them confidence

28
00:01:35,000 --> 00:01:36,000
in what you're doing.

29
00:01:36,000 --> 00:01:39,000
I know for longest time my parents really thought I was,

30
00:01:39,000 --> 00:01:43,000
I had joined a cult or I was being brainwashed

31
00:01:43,000 --> 00:01:47,000
was just going crazy because it was really difficult.

32
00:01:47,000 --> 00:01:52,000
It was a struggle in the beginning to assimilate this incredibly different teaching.

33
00:01:52,000 --> 00:01:54,000
But as I became more comfortable in it,

34
00:01:54,000 --> 00:01:57,000
and as I was able to show them that I was really doing good things

35
00:01:57,000 --> 00:02:00,000
for myself and for the people around me,

36
00:02:00,000 --> 00:02:03,000
and that I was really able to do something positive

37
00:02:03,000 --> 00:02:06,000
and what I was doing was actually a really good thing.

38
00:02:06,000 --> 00:02:10,000
The whole thing about being Buddhist wasn't really so important.

39
00:02:10,000 --> 00:02:13,000
On the other hand, there are certain people in my family

40
00:02:13,000 --> 00:02:15,000
who I think will never accept what I'm doing

41
00:02:15,000 --> 00:02:17,000
and simply because of the label,

42
00:02:17,000 --> 00:02:21,000
and because I'm obviously an ordained Buddhist monk.

43
00:02:21,000 --> 00:02:32,000
So I guess that being said that you can get away with just living the teachings,

44
00:02:32,000 --> 00:02:36,000
especially so that people like your parents will be able to accept you

45
00:02:36,000 --> 00:02:38,000
without you having to tell them that you're Buddhist.

46
00:02:38,000 --> 00:02:41,000
In the end, you do have to go on your own.

47
00:02:41,000 --> 00:02:44,000
You do have to go your own way and have to accept that there are certain people

48
00:02:44,000 --> 00:02:48,000
who just aren't going to see eye to eye with you.

49
00:02:48,000 --> 00:02:51,000
And I guess in the end, that's the most important.

50
00:02:51,000 --> 00:02:54,000
The most important is that you follow the right path.

51
00:02:54,000 --> 00:02:59,000
Follow what you understand to be the path to peace, happiness, and freedom from suffering,

52
00:02:59,000 --> 00:03:01,000
and the path to becoming a better person.

53
00:03:01,000 --> 00:03:04,000
If other people can't see it, well, maybe they're not interested

54
00:03:04,000 --> 00:03:07,000
in bettering themselves or becoming a better person

55
00:03:07,000 --> 00:03:12,000
and generally people in this world have a lot of things that are stopping them

56
00:03:12,000 --> 00:03:15,000
from developing things like views and attachments,

57
00:03:15,000 --> 00:03:19,000
their ideas and opinions of the way things should be

58
00:03:19,000 --> 00:03:25,000
that really lead them down a sort of a dead end

59
00:03:25,000 --> 00:03:28,000
and make it very difficult for them to accept a path

60
00:03:28,000 --> 00:03:31,000
which might go in a different direction.

61
00:03:31,000 --> 00:03:35,000
Okay, so first of all, don't worry so much about being Buddhist,

62
00:03:35,000 --> 00:03:40,000
just practice it and try to talk to them about the things which you've gained

63
00:03:40,000 --> 00:03:43,000
from the practice and show to them and make it clear to them

64
00:03:43,000 --> 00:03:46,000
that what you're doing is a benefit of benefit to you

65
00:03:46,000 --> 00:03:49,000
and to other people and not worry so much about what they think of it,

66
00:03:49,000 --> 00:03:51,000
just show them that it's a good thing.

67
00:03:51,000 --> 00:03:57,000
And second, kind of let go and don't worry so much about other people seeing eye to eye with you

68
00:03:57,000 --> 00:04:00,000
once you can at least show them that it's what you want to do

69
00:04:00,000 --> 00:04:02,000
and it's what is right for you.

70
00:04:02,000 --> 00:04:07,000
Then that you've done the best you can do and it's up to them to go the next

71
00:04:07,000 --> 00:04:12,000
to take the step towards accepting the thing that you're doing.

72
00:04:12,000 --> 00:04:14,000
Okay, so hope that helped.

73
00:04:14,000 --> 00:04:15,000
Keep the questions coming.

74
00:04:15,000 --> 00:04:18,000
If you want to post, you can now post on my channel page

75
00:04:18,000 --> 00:04:22,000
so that's youtube.com.com.

76
00:04:22,000 --> 00:04:27,000
Just post directly there and I'll respond to them as I get time.

77
00:04:27,000 --> 00:04:28,000
Okay, thanks.

78
00:04:28,000 --> 00:04:33,000
Bye.

