1
00:00:00,000 --> 00:00:06,000
Okay, good evening, everyone.

2
00:00:06,000 --> 00:00:14,000
Welcome to our evening dumber.

3
00:00:14,000 --> 00:00:33,000
Today, if I got my calendar correct, today is the full moon of the month of manga.

4
00:00:33,000 --> 00:00:45,000
Which makes this manga pouja, manga pouja, manga pouja,

5
00:00:45,000 --> 00:00:55,000
pouna mi, pouna mi means the day of the full moon, manga.

6
00:00:55,000 --> 00:01:03,000
The manga pouja means it's a time of commemoration.

7
00:01:03,000 --> 00:01:08,000
Normally on this day, there is an excuse.

8
00:01:08,000 --> 00:01:15,000
There are big celebrations or special talks and special.

9
00:01:15,000 --> 00:01:24,000
Good doings and monasteries.

10
00:01:24,000 --> 00:01:34,000
Commemorate is what is called the Ovada party mocha.

11
00:01:34,000 --> 00:01:45,000
In the beginning of the dispensation of the Buddha when he began to teach,

12
00:01:45,000 --> 00:01:51,000
there was no established set of rules.

13
00:01:51,000 --> 00:02:04,000
The tradition goes that he would preside over the meeting of the monks every two weeks

14
00:02:04,000 --> 00:02:09,000
on the full moon, on the empty moon, on the new moon,

15
00:02:09,000 --> 00:02:18,000
and would recite the Ovada party mocha.

16
00:02:18,000 --> 00:02:22,000
How it started is on this day.

17
00:02:22,000 --> 00:02:24,000
Apparently, this is a tradition.

18
00:02:24,000 --> 00:02:31,000
We have this story commemorating this event when it's the Jeturanga sanipat,

19
00:02:31,000 --> 00:02:43,000
the fourfold meeting, where a group of 1,250 Arahant gathered together

20
00:02:43,000 --> 00:02:46,000
to listen to the Buddha's teaching.

21
00:02:46,000 --> 00:02:56,000
The conjecture is that this is actually the early on in the Buddha's ministry.

22
00:02:56,000 --> 00:03:04,000
When he went to Rajagaha, and he stopped off on his way there,

23
00:03:04,000 --> 00:03:12,000
he stopped off in Gaiya sea set.

24
00:03:12,000 --> 00:03:14,000
He met with these three fire assignments.

25
00:03:14,000 --> 00:03:21,000
He spent some time with his first fire ascetic and he's ascetic, so worshiped fire.

26
00:03:21,000 --> 00:03:31,000
Through various means and miracles, he converted these three brothers,

27
00:03:31,000 --> 00:03:46,000
along with their 1,250 followers.

28
00:03:46,000 --> 00:03:50,000
Can't remember exactly, something like that.

29
00:03:50,000 --> 00:04:01,000
Anyway, so all of these monks were all of these monks were off meditating,

30
00:04:01,000 --> 00:04:09,000
and suddenly one of them threw his practice, achieved liberation, became Narant.

31
00:04:09,000 --> 00:04:16,000
The story goes that he got up and decided he would go and pay respect to the Buddha

32
00:04:16,000 --> 00:04:22,000
and let the Buddha know of his attainment.

33
00:04:22,000 --> 00:04:28,000
So he walked to where the Buddha was seated up on the hill in the opening

34
00:04:28,000 --> 00:04:30,000
and the clearing.

35
00:04:30,000 --> 00:04:35,000
When he got there, he sat down, they respected the Buddha, and then he turned

36
00:04:35,000 --> 00:04:40,000
just to make sure that it was going to interrupt anything.

37
00:04:40,000 --> 00:04:48,000
And he saw another monk walking towards them,

38
00:04:48,000 --> 00:04:50,000
ostensibly to do the same thing.

39
00:04:50,000 --> 00:04:55,000
And so it's sure enough, the second monk had become Narant.

40
00:04:55,000 --> 00:04:57,000
And did the same thing.

41
00:04:57,000 --> 00:05:02,000
So the first monk said, I'll wait for him to come.

42
00:05:02,000 --> 00:05:04,000
The second monk got there.

43
00:05:04,000 --> 00:05:08,000
He did the same thing turned around and the third monk was walking.

44
00:05:08,000 --> 00:05:12,000
The idea is they all came together.

45
00:05:12,000 --> 00:05:18,000
One of the marvelous things about the meeting is that it was completely

46
00:05:18,000 --> 00:05:20,000
uncoordinated.

47
00:05:20,000 --> 00:05:26,000
There was no prior engagement.

48
00:05:26,000 --> 00:05:33,000
And it all 1,250 of them came together.

49
00:05:33,000 --> 00:05:38,000
And they were all our hunts and so on.

50
00:05:38,000 --> 00:05:44,000
And so the Buddha, on that day, said to have given the Ovanda party monk,

51
00:05:44,000 --> 00:05:48,000
and so it's just a means of really commemorating this teaching,

52
00:05:48,000 --> 00:05:51,000
which is really important for a good summary.

53
00:05:51,000 --> 00:05:59,000
It's quite well known in the Buddhist world.

54
00:05:59,000 --> 00:06:07,000
It's one of those verses that is important to Buddhist.

55
00:06:07,000 --> 00:06:09,000
Tell about Buddhist.

56
00:06:09,000 --> 00:06:11,000
So how does it go?

57
00:06:11,000 --> 00:06:15,000
The Buddha taught the Ovanda party monk a

58
00:06:15,000 --> 00:06:31,000
devotee, Paramahmaan, tapu, tikka.

59
00:06:31,000 --> 00:06:37,000
The highest form of asceticism.

60
00:06:37,000 --> 00:06:41,000
Nibana, Paramahmaan, Ovanda.

61
00:06:41,000 --> 00:06:44,000
Nibana is the highest, say, or is the greatest, say, the Buddha's.

62
00:06:44,000 --> 00:06:47,000
Nahi, Paramahmaan, tikka, Opagati.

63
00:06:47,000 --> 00:06:58,000
No, one is not a, one is not a recluse.

64
00:06:58,000 --> 00:07:01,000
A renunciant.

65
00:07:01,000 --> 00:07:04,000
Winger's others.

66
00:07:04,000 --> 00:07:15,000
Sama-no-ho-ti, Paramahmaan, taiyantu.

67
00:07:15,000 --> 00:07:18,000
One is not a recluse who abuses others.

68
00:07:18,000 --> 00:07:21,000
Sabhupapasa, a karunang.

69
00:07:21,000 --> 00:07:23,000
They're not doing of any evil.

70
00:07:23,000 --> 00:07:29,000
Kustulasa, Upa-sampada, being full of good, full of wholesomeness.

71
00:07:29,000 --> 00:07:36,000
Satya-tapari-ho-dapana, together with the purifying of one's own mind.

72
00:07:36,000 --> 00:07:39,000
Aetang-bu-dhan-asasana.

73
00:07:39,000 --> 00:07:45,000
This is the teaching of all the Buddhas.

74
00:07:45,000 --> 00:07:50,000
Anupavado, anupagato, not speaking badly of anyone,

75
00:07:50,000 --> 00:07:53,000
or harming anyone.

76
00:07:53,000 --> 00:08:01,000
Bhatimokhe, jasangwara, being restrained or guarded in regards to the Bhatimokha,

77
00:08:01,000 --> 00:08:06,000
which is the ways of the community.

78
00:08:06,000 --> 00:08:14,000
Matan-yutaj, a Bhata-shiming, knowing moderation, and eating.

79
00:08:14,000 --> 00:08:24,000
Bhantanta-sayana-asana, dwelling in this occluded lodging,

80
00:08:24,000 --> 00:08:28,000
or having this occluded lodging.

81
00:08:28,000 --> 00:08:33,000
Adhititaj, ayo-do, aetang-bu-dhan-asasana,

82
00:08:33,000 --> 00:08:43,000
and the striving or exerting oneself

83
00:08:43,000 --> 00:08:49,000
in regards to higher minds, higher minds' dates.

84
00:08:49,000 --> 00:08:55,000
This is the teaching of all the Buddhas.

85
00:08:55,000 --> 00:09:01,000
And so this was considered or is considered to be the essence of the Buddha's exhortation,

86
00:09:01,000 --> 00:09:09,000
or pithi exhortation, something that we recite,

87
00:09:09,000 --> 00:09:14,000
something that we remember, it's apparently something that all Buddhas teach,

88
00:09:14,000 --> 00:09:24,000
as their admonishment or exhortation on the holiday.

89
00:09:24,000 --> 00:09:27,000
Kanti-paramang-tapoditika, patience.

90
00:09:27,000 --> 00:09:29,000
This is an important sentence.

91
00:09:29,000 --> 00:09:35,000
I often remind my meditators that meditation can be such torture.

92
00:09:35,000 --> 00:09:37,000
There's nothing wrong with the meditation.

93
00:09:37,000 --> 00:09:44,000
It's not that meditation is painful or stressful, not at all.

94
00:09:44,000 --> 00:09:52,000
It's just that we have relaxed patience.

95
00:09:52,000 --> 00:10:02,000
We possess those qualities of mind that require us to be patient with,

96
00:10:02,000 --> 00:10:05,000
meaning we have greed, we have anger,

97
00:10:05,000 --> 00:10:09,000
and to counter them, we need the patience.

98
00:10:09,000 --> 00:10:13,000
And patience is the greatest form of asceticism, austerity, torture, really.

99
00:10:13,000 --> 00:10:16,000
Isn't the time of the Buddha?

100
00:10:16,000 --> 00:10:20,000
Tup-tup-tup-tup would mean torturing yourself.

101
00:10:20,000 --> 00:10:23,000
Meditation, patience can be quite a torture,

102
00:10:23,000 --> 00:10:26,000
having to sit through and not react.

103
00:10:26,000 --> 00:10:29,000
It's actually a lot easier.

104
00:10:29,000 --> 00:10:32,000
This is why this is the highest, because it's much easier

105
00:10:32,000 --> 00:10:37,320
your pleasure to just beat yourself and get rid of it, right? Self-flagellation, which

106
00:10:37,320 --> 00:10:43,200
is apparently a religious thing that, well, Christians used to do it and Indian ascetics

107
00:10:43,200 --> 00:10:52,680
used to do it. Put a stone to that. We torture ourselves in other ways. We torture ourselves

108
00:10:52,680 --> 00:11:07,280
with nothing by refusing to engage in aversion, addiction, by standing firm in the present

109
00:11:07,280 --> 00:11:17,280
moment, in reality, in peace. And with good things, peace was a bad thing.

110
00:11:17,280 --> 00:11:23,440
Indian Bhannang Paramang would anti-Buddhaya. Indian Bhannaya is the highest, again, an important

111
00:11:23,440 --> 00:11:28,400
point, and it's fairly simple, and there's no lot you can say about Indian Bhannaya.

112
00:11:28,400 --> 00:11:36,320
It's freedom. But putting at the top, putting at the top is important. That's what sort of

113
00:11:36,320 --> 00:11:40,760
distinguishes Buddhism from any other religion. They're religions have their own versions

114
00:11:40,760 --> 00:11:46,040
of similar things, but let's be clear in Buddhism, what is the goal? It's not heaven, it's

115
00:11:46,040 --> 00:11:51,880
not becoming one with God or one with everything or anything. It's freedom. It's the

116
00:11:51,880 --> 00:12:00,160
unbinding, or it's release, the cessation of suffering.

117
00:12:00,160 --> 00:12:07,000
Nahi Pupajitopunupagati, and here's actually one of this, and another section on, this is

118
00:12:07,000 --> 00:12:13,120
really the body mocha part where we shouldn't do. And it's just a simple admonishment

119
00:12:13,120 --> 00:12:21,760
that whatever we do, if it falls under the harming others, speaking ill of them, acting

120
00:12:21,760 --> 00:12:27,640
or speaking in such a way as to cause harm and suffering to them. This isn't the way of

121
00:12:27,640 --> 00:12:41,080
a reckless. I remember once that there was this monk who was saying something.

122
00:12:41,080 --> 00:12:48,280
He was calling me all sorts of awful names. He had mental problems, British men. He had

123
00:12:48,280 --> 00:12:53,880
been beaten as a child, abused as a child, done a lot of drugs through his life, and so

124
00:12:53,880 --> 00:12:59,320
on. He became a monk, and he was a real terror. So he called me all sorts of names and

125
00:12:59,320 --> 00:13:06,080
I have no blood. And something came up about how I didn't treat him like a monk and so on.

126
00:13:06,080 --> 00:13:14,000
And I said, you're not a monk. And he stopped, and he walked away. And then he came back

127
00:13:14,000 --> 00:13:22,160
about an hour later with the rule book, and he accused me of an offense because I had

128
00:13:22,160 --> 00:13:28,520
claimed that he had committed an offense. He somehow saw it that I had accused him of

129
00:13:28,520 --> 00:13:34,480
being no longer a monk, so he had committed a serious offense. Anyway, I said to him,

130
00:13:34,480 --> 00:13:38,720
no, no, I didn't mean it that way. I said, one isn't, and I'm excited as averse. I said,

131
00:13:38,720 --> 00:13:45,520
you're not a monk if you abuse others. And he calmed down actually and we were able

132
00:13:45,520 --> 00:13:51,760
to talk again. He was kind of like that. He just started to roll with it. He would be

133
00:13:51,760 --> 00:13:55,880
threatening to beat you the one minute and then be sitting joking and at the other

134
00:13:55,880 --> 00:14:09,480
the next minute. Yeah, life is a monk in Thailand, ever a dull moment. But this is an

135
00:14:09,480 --> 00:14:16,720
important exhortation for us. Be clear in your minds. This isn't the way of the Buddha.

136
00:14:16,720 --> 00:14:23,160
Whether you're a monk or not a monk, you wish to be a true recluse, a true renunciate.

137
00:14:23,160 --> 00:14:28,960
You don't have to put on robes to do that. Be a follower of the Buddha, follow his way,

138
00:14:28,960 --> 00:14:37,200
follow his guidance. Don't abuse others, don't harm others. Let's speak

139
00:14:37,200 --> 00:14:46,200
you, lift them. Sabapapa, kariranang, don't do any evil. This is the important. This part

140
00:14:46,200 --> 00:14:49,560
is what everyone repeats. This is the essence of it.

141
00:14:49,560 --> 00:14:56,720
Ghousalasa upasampada, before good, before wholesomeness, such a taparirodapanang and purifier

142
00:14:56,720 --> 00:15:02,520
minds. So these three things really make the essence of Buddhism. Do not do any evil to

143
00:15:02,520 --> 00:15:07,760
be full of good and to purify your mind. Now the first two alone aren't enough and this

144
00:15:07,760 --> 00:15:16,000
is important. It distinguishes Buddhism from any other types of religious spiritual or even

145
00:15:16,000 --> 00:15:23,520
good practices because anyone who devotes their time to doing good and avoiding evil is

146
00:15:23,520 --> 00:15:28,760
ignoring the crucial aspect of that which causes us to do evil and which prevents us from

147
00:15:28,760 --> 00:15:37,720
doing good. And that's our mind. If your mind is not pure, if your mind is not well, very

148
00:15:37,720 --> 00:15:47,360
hard to avoid evil, very hard to be full of goodness. And so our main activity really

149
00:15:47,360 --> 00:15:53,080
is the purification of the mind because once your mind is pure, it can't possibly do

150
00:15:53,080 --> 00:16:04,400
evil. Again, possibly you are by nature constantly doing good. And the other goes the other

151
00:16:04,400 --> 00:16:11,240
way, it's not to say that you should just ignore evil and good. You should, you know,

152
00:16:11,240 --> 00:16:17,000
do evil will take away from your practice, prevent you from progressing, and doing

153
00:16:17,000 --> 00:16:23,200
good will support your practice, support the strength of mind needed to become a mind,

154
00:16:23,200 --> 00:16:28,840
to purify your mind. So these three together, a tambourdhaanasasana, this is the teaching

155
00:16:28,840 --> 00:16:35,880
of all the Buddhists. But then he repeats, he gives another list of things that are also

156
00:16:35,880 --> 00:16:44,920
the teaching of the Buddhists. So we think about these as well. Anupawado anupagato, again,

157
00:16:44,920 --> 00:16:52,600
not speaking ill of others, not harming others. Bhati mokai jasangwaro being restrained

158
00:16:52,600 --> 00:17:00,680
by discipline. So there's for monks, it's all the rules of the monks, for lay people,

159
00:17:00,680 --> 00:17:08,320
it's the five or the eight priests, right livelihood and so on. All of this is the teaching

160
00:17:08,320 --> 00:17:13,960
of the Buddhas. Mata nyutajam bhatas mean knowing moderation and eating. Now the Buddha

161
00:17:13,960 --> 00:17:20,320
you see, this is something that we often neglect, be careful. You are consumption and we can

162
00:17:20,320 --> 00:17:25,640
apply this not just to food, but to everything that we consume, whether it be television

163
00:17:25,640 --> 00:17:32,440
or whether it be internet or whether it be music or whatever, be careful about your consumption

164
00:17:32,440 --> 00:17:38,800
because what you take in affects you, it changes who you are and if you know mindful

165
00:17:38,800 --> 00:17:50,520
and lean to addiction, stress and suffering. But certainly food is an important thing. It's

166
00:17:50,520 --> 00:17:58,160
the one thing that we need every day. At the least it acts as a good base reference. If

167
00:17:58,160 --> 00:18:02,600
you're addicted to food, it's a sinum and addictive personality. You're addicted to

168
00:18:02,600 --> 00:18:08,840
a good food, for example, or picky about your food or that kind of thing. You obsess over

169
00:18:08,840 --> 00:18:17,400
food, eating too much, maybe eating not enough, mostly eating too much or eating unhealthy

170
00:18:17,400 --> 00:18:26,760
food because it tastes better. We're moderation and eating. Bandan jasayanas and then another

171
00:18:26,760 --> 00:18:33,840
thing is that these show what a simple life it is. There's not complex rules or rituals

172
00:18:33,840 --> 00:18:40,800
and Buddhism. Another way of looking at this is live this way and you've got Buddhism

173
00:18:40,800 --> 00:18:46,200
in a nutshell. Bandan jasayanas and un-delive in this occluded area to have a secluded

174
00:18:46,200 --> 00:18:53,240
lodging. Of course, the ideal is to be in a forest or in a cave on a mountain or something,

175
00:18:53,240 --> 00:19:01,000
but even here in the city we've found a way, quite remarkable, exceptional, and much appreciated

176
00:19:01,000 --> 00:19:11,720
all about it. We much appreciate all the support that hasn't made this possible. Here

177
00:19:11,720 --> 00:19:18,320
we have rooms that are quiet and we have secluded dwelling here where we don't sit around

178
00:19:18,320 --> 00:19:31,720
chatting all day and spend our time cultivating goodness, doing good things.

179
00:19:31,720 --> 00:19:40,320
Adejitteja ayo go and the devotion to higher mindedness or higher minds. It means going

180
00:19:40,320 --> 00:19:46,680
beyond the mundane states of consciousness or the ordinary states of greed, anger, and

181
00:19:46,680 --> 00:19:56,680
delusion to have clarity of mind and quietude of mind and to cultivate wisdom and this

182
00:19:56,680 --> 00:20:05,720
per mundane state, the janas as well and then the super mundane janas engaged in realisation

183
00:20:05,720 --> 00:20:15,240
of Nibana. It's quite simple. I mean this last one is another really good way of describing

184
00:20:15,240 --> 00:20:21,960
it. Don't do, don't harm others or this is the morality part. Don't harm others, don't

185
00:20:21,960 --> 00:20:33,960
speak yellow feathers, follow the rules of the community and the discipline of a Buddhist,

186
00:20:33,960 --> 00:20:40,880
no moderation in eating, so besides eating all you're doing is cultivating goodness. So

187
00:20:40,880 --> 00:20:51,160
just be careful of the necessities of life and stands occlusion. When you're in occlusion

188
00:20:51,160 --> 00:21:01,720
cultivate higher mind states. Don't just sit around and think or let yourself get distracted.

189
00:21:01,720 --> 00:21:04,120
This is the teaching of all the Buddha's ate, the Buddha's ate, the Buddha and the

190
00:21:04,120 --> 00:21:12,080
janas as well. We have here in the Digganykaya, it says, we passibu, we passibu,

191
00:21:12,080 --> 00:21:19,160
bhagavavar, hang samasabhu, bhukusankhe, ayhuang, patimu kangangi sateh. We passibu as a Buddha in

192
00:21:19,160 --> 00:21:28,760
way in the past. When some other, maybe in some other epoch or eon or something, all

193
00:21:28,760 --> 00:21:33,320
time ago. There's another Buddha. So the idea is this is apparently when all

194
00:21:33,320 --> 00:21:38,960
Buddha's teams, no matter what else they may teach or not teach, they always

195
00:21:38,960 --> 00:21:45,680
teach this and this is what they teach to their assembly. Either way, it's what

196
00:21:45,680 --> 00:21:51,560
we teach to our assembly. I think it's sort of an important milestone for us to

197
00:21:51,560 --> 00:21:58,560
mark. Well, we commemorate on this day the Owadapati Mukha on the full moon of

198
00:21:58,560 --> 00:22:06,520
manga. So there you go. It's a dhamma for tonight. Thank you all for tuning in with

199
00:22:06,520 --> 00:22:13,520
you all good practice.

