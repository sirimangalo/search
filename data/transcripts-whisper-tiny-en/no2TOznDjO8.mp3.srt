1
00:00:00,000 --> 00:00:04,880
Okay, next question, from FiskM's consultant.

2
00:00:04,880 --> 00:00:09,520
When my concentration gets stronger and my mind is more with the rising falling, I feel

3
00:00:09,520 --> 00:00:12,960
like I am falling backwards and thus feel less stable.

4
00:00:12,960 --> 00:00:16,600
Is the half-load is a good way to feel less wobbly?

5
00:00:16,600 --> 00:00:17,600
Yes.

6
00:00:17,600 --> 00:00:22,840
In fact, the full-load is a way to feel even less wobbly, but those of you who are not

7
00:00:22,840 --> 00:00:30,000
aware here, let's set this up.

8
00:00:30,000 --> 00:00:33,200
Okay, let's see.

9
00:00:33,200 --> 00:00:39,880
Half-load is put one leg, one foot on top of the other leg, okay?

10
00:00:39,880 --> 00:00:41,840
Can you get that?

11
00:00:41,840 --> 00:00:44,840
I don't know, I'm zoomed out here.

12
00:00:44,840 --> 00:00:52,120
Okay, full-load is full of the other leg out, put it up here.

13
00:00:52,120 --> 00:01:00,280
This method of sitting is the most stable, it's what yoga practitioners use.

14
00:01:00,280 --> 00:01:06,480
When you're practicing tranquility meditation, when you're focused on a single object

15
00:01:06,480 --> 00:01:15,720
for a long time, your mind becomes removed from the reality of things like pain and discomfort

16
00:01:15,720 --> 00:01:21,320
in the body, and it creates, there's a great power in the mind as well, so you're able

17
00:01:21,320 --> 00:01:26,160
to endure this, and some people are able to sit this way for a long time.

18
00:01:26,160 --> 00:01:30,640
In inside meditation, the sort of meditation that I teach, this isn't the case, your

19
00:01:30,640 --> 00:01:38,000
mind is not removed from the reality, and so there's a lot of flexibility and moving

20
00:01:38,000 --> 00:01:41,440
going on, and this can become quite painful after a while.

21
00:01:41,440 --> 00:01:47,440
This is why I would say that we have to take a different approach in inside meditation,

22
00:01:47,440 --> 00:01:50,800
and I would say this is pretty true of most meditators.

23
00:01:50,800 --> 00:01:59,000
Some people are able to sit through this sort of position without succumbing to the pain,

24
00:01:59,000 --> 00:02:01,200
even for up to an hour.

25
00:02:01,200 --> 00:02:04,840
The problem is, in a meditation course, we're trying to do more than just an hour, we're

26
00:02:04,840 --> 00:02:10,080
trying to do many days of meditation, and so for most people, the pain and discomfort

27
00:02:10,080 --> 00:02:16,320
of sitting in this position without the strong concentration of tranquility meditation

28
00:02:16,320 --> 00:02:27,040
is too much, and so I don't recommend this position, actually, not for bare inside meditation

29
00:02:27,040 --> 00:02:33,920
that is not based on tranquility as well.

30
00:02:33,920 --> 00:02:40,960
The more interesting thing to do is to look at the wobbling and to ask yourself what's

31
00:02:40,960 --> 00:02:46,800
going on there, because I guarantee there's a mental factor as well, which is the concern

32
00:02:46,800 --> 00:02:51,880
about it, worrying that you're going to fall over and so on, and probably in fact, it's

33
00:02:51,880 --> 00:02:56,920
become just an obsession, and our mind is good at this, our mind obsesses over things,

34
00:02:56,920 --> 00:03:03,160
saying that this is a problem, saying that, oh, I'm going to fall over, it feels like

35
00:03:03,160 --> 00:03:07,880
I'm going to fall over until you convince yourself that any moment now you're going to fall

36
00:03:07,880 --> 00:03:14,600
over, but in fact, it's just a feeling that comes from the tension in the back, and in

37
00:03:14,600 --> 00:03:21,240
the legs, and so on, and having to keep this sort of upright position.

38
00:03:21,240 --> 00:03:28,320
I guarantee that you won't fall over, there are cases where people begin to feel a pulling

39
00:03:28,320 --> 00:03:34,320
backwards, some people feel a pulling forward, feel a pulling to the side, some people feel

40
00:03:34,320 --> 00:03:41,440
this rocking sensation, all of these things are caused as you say by strong concentration,

41
00:03:41,440 --> 00:03:47,560
and it's important just to say to yourself feeling, feeling, or swaying, swaying, and bring

42
00:03:47,560 --> 00:03:54,760
yourself back, raising, raising, or coming back, or someone come back to a sitting position.

43
00:03:54,760 --> 00:04:03,440
And again, in regards to all of these things, the more important is whether or not these

44
00:04:03,440 --> 00:04:07,880
experiences arise, but how we react to them.

45
00:04:07,880 --> 00:04:08,880
This is the challenge.

46
00:04:08,880 --> 00:04:13,160
I mean, what you're experiencing is a meditation condition, and this is the way things are

47
00:04:13,160 --> 00:04:15,040
for you at that moment.

48
00:04:15,040 --> 00:04:19,360
It's not important, whether that is or is not, and how to start it or stop it, how to

49
00:04:19,360 --> 00:04:22,440
keep it like that, or make it something else.

50
00:04:22,440 --> 00:04:26,200
The point is what's going on in your mind at that time, and how to change the way you

51
00:04:26,200 --> 00:04:33,840
react to it, so from disappointment, or discomfort, or worry, or so on, which are the real

52
00:04:33,840 --> 00:04:38,480
problems that exist not only in the meditation, but in our life as well, to change to

53
00:04:38,480 --> 00:04:42,240
a state where we're accepting, and we're like, okay, now it's like this, okay, now it's

54
00:04:42,240 --> 00:04:52,320
like this, and neither attaching to it or becoming averse to it, that we're able to

55
00:04:52,320 --> 00:05:03,280
drop it, and let it be the way it is, and simply live and exist in peace and happiness.

56
00:05:03,280 --> 00:05:05,960
Okay, so go for it.

57
00:05:05,960 --> 00:05:10,840
If you want to do the half-lodus, the full-lodus fine, but it's not really the point, and

58
00:05:10,840 --> 00:05:16,320
you probably find it after a while, it's a great amount of discomfort, especially if you

59
00:05:16,320 --> 00:05:20,720
decide to undertake a intensive meditation course, and I know people who are stubborn

60
00:05:20,720 --> 00:05:26,640
about it find actually it's a real, it's a stronger distraction, is the incredible pain

61
00:05:26,640 --> 00:05:31,080
in their legs, they've been told that the half-lodus is good, the full-lodus is better,

62
00:05:31,080 --> 00:05:34,760
and so they try one or the other, and they keep coming back and saying how much pain

63
00:05:34,760 --> 00:05:39,720
they have, and if the teacher's not able to catch it, they won't realize why there is

64
00:05:39,720 --> 00:05:45,240
so much pain, and in fact the meditator is creating it unnecessarily, and being distracted

65
00:05:45,240 --> 00:05:53,360
by the pain, thinking that they're supposed to just sit through it, you can try, but I think

66
00:05:53,360 --> 00:05:58,480
for most people it just becomes a greater distraction, and it becomes kind of an attachment

67
00:05:58,480 --> 00:06:02,280
as well, because you feel like you're doing the right thing, and this pain is good and

68
00:06:02,280 --> 00:06:17,200
so on, it's not a natural state of mind, so that's my take on it.

