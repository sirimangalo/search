Hey good evening everyone, welcome to our Saturday, evening dhamma.
Today we're looking at the Pia Jatikas with Midi Minigai 87.
And it's not a very complicated lesson and there's not much to the lesson, but what is there I think is quite important.
And it's a good story.
It addresses an important issue because there's not a lot I think to say, but I'll say what I can.
And I'll tell the story of course.
So the story goes.
And the Buddha was in sawati and the Buddha spent most of his life, majority of his life in sawati.
It's a city, it was a city in India at that time.
Now when you go to that area all you see is fields and the vague outline of the city wall.
One of the biggest cities of time is disappeared, gone.
2,500 years later, changed, no.
Change gets rid of it, removes everything.
But the Buddha was dwelling in sawati and there was a certain man.
Some kind of, some probably a wealthy, well respected individual.
And his son died and with the death of his son he lost all desire to work or eat.
All he did was, went to the cemetery and cried, cried out, my only son where are you, my only son where are you?
And after some time of this he became so distraught that he started to think,
that's many people do and distress hits.
Who could help me with this?
And of course he thought of the Buddha.
He'd heard good things about this.
Records and sawati, go and see the Buddha.
And he says to the Buddha he says, oh no he goes and sits down and the Buddha says to him,
he says, you don't look so good.
His, your faculties are not those of one who is in control of his own mind.
He appeared deranged.
You don't appear to be in control of your own mind.
And he says, oh, how could I not be deranged?
My dear and beloved son is died and since he died.
I have no more desire to work or eat, I just go to the graveyard and cry.
And the Buddha says, indeed, so it is, so it is.
Soil lamentation, pain, grief and despair are born from those who are dear,
arise from those who are dear.
And that's the thesis of this, the core of this discourse,
is this statement.
We heard it before, I think, you've probably heard me say it.
It's a hard statement to hear, let alone,
agree with and appreciate and accept.
And this man certainly is having nothing of it.
He came all this way to hear the Buddha and the Buddha says this,
and oh boy does he get upset?
He says, who would everything that sorrow, lamentation, despair, pain, grief?
I would think those things are born from who are dear.
And herbal sir, happiness and joy are born from those who are dear.
When we, something that is dear to us, that thing,
how it gives us such happiness, right?
For the side people, we think this sort of thing about all of our possessions.
The story goes on.
He gets very upsetting, he gets up and leaves.
Displeased with the blessed ones words.
It's very rare that someone gets up and leaves having been displeased by what the Buddha says.
But anyway, this is the case.
And he goes and, as he's walking,
he's walking home and sees some gamblers.
Some people playing dice.
Gamblers, I think the connotation is these are sort of immoral,
a shady individual, these are gamblers.
Kind of people who drink alcohol and love a gamble, but the most noble of occupations.
Obsessed with money, perhaps cheating, but certainly manipulative.
There's something to do with a gambling gambling,
as I don't think in general very.
It's certainly given a negative connotation anyway.
They don't go too much into that.
He goes to these gamblers and he asks them.
He says, oh, they went and said this.
And the Buddha said this, and then I said this.
And they say, yeah, happiness and they agree with them.
Happiness and joy come from those who are here.
And how soldier goes home and this man goes home and says,
thinking to himself, I agree with the gamblers.
So eventually the story gets to the king.
The king percenna day, and he's not the most brilliant
if you know anything about this king.
His portrait is not a very brilliant individual to say the least.
But his queen is quite smart.
The king says this to the king.
This is what the king is what I've heard about the Buddha.
The Buddha says, but go to my, he says,
sorrow, lamentation, pain, grief, and despair are born from those who are here.
What it says is that liking things makes you unhappy.
Those things that you like cause you stress and suffering.
Liking things causes stress and suffering.
And the queen says, well, if the Buddha has said it, then it is true.
And the king gets angry at her and he says,
you just agree with whatever he says.
The Buddha says this.
You just say so it is.
The Buddha says that and he says so it is.
And so he expels the queen, he tells her to go away.
And so the queen goes to one of her,
a brahmana, somebody who, an advisor, I guess you'd say.
She says, go and talk to the Buddha and ask him what he meant by.
Ask somebody who meant by that saying.
The brahmana goes to the Buddha and the Buddha gives him the explanation.
It's rather than, that's about it for the story.
But the Buddha gives a simple explanation on why it is that something that is dear to us
is considered to cause stress and suffering.
He says that in this very city,
if you think about the vision of the Buddha,
I'm talking about a much broader vision than simply the situation that each of us finds ourselves in.
And so what we'd say is that most of us are fairly narrow minded.
And a person who is in the process of enjoying,
enjoying possession of the things that they love.
It's hard for them to see any truth in this saying
that things that are dear to you bring suffering,
that the overall result of what is dear is stress and suffering and despair.
But the Buddha said in this city,
that this is, you can find a person, a man, a woman, a young old.
You can find them lamenting parents, children, brothers, sisters, wives, husbands.
You can find people going crazy with despair and grief.
My aunt and uncle right now are, I think, still quite upset to personally
my aunt and uncle, because our cousin passed away.
It's a curious thing when someone dies as a Buddhist,
because there's not this sort of sadness.
It's a hard thing to relate to for someone who has come to practice meditation,
because you see the truth of this,
that a person who is even enjoying the presence and the possession of the things that they love.
Even this person who is on the upswing, you might say,
is engaging in the process of stress and suffering.
So the Buddha talks about how a person who holds someone dear
and then loses that person, there's great suffering that comes from the change.
But in meditator, it sees even the liking of something as stressful.
It sees those people who are clinging that they're caught up in attention,
there's attention to it.
And you see how this tension is stressful.
There's a worry about a mother or a father who loves their child so much.
Because it ultimately comes down to something much simpler than loss or beings.
It's at a much simpler level of losing and being losing a son, losing a daughter, a child, a parent, a loved one.
It's about change.
A fundamental level when we talk about losing someone, we're talking about a state of mind that is stuck
and has been disturbed because of being out of sync with reality.
If you think of reality as like a car,
and then you are in a cart, let's say something simpler, like an ox cart.
And you're riding in an ox cart and suddenly your robe gets stuck on a bramble bush.
And you're riding in the ox cart and your robe gets stuck on it, but the ox cart keeps going.
The ox cart keeps going because that's the nature of the ox cart.
So because you're stuck on the bramble bush, great danger and great danger.
You're out of sync.
You're going to become soon quite out of sync with the ox cart to fall out of it and hurt yourself.
The person who is stuck on something has lost their sync with reality.
It's not even really pleasant for a meditator to like something or to indulge in pleasant sensations
because it's out of sync.
It's disturbed.
It feels unnatural.
It's in and of itself intrinsically.
That's really what the Buddha says. He talks about change.
He says, if someone you love, changes.
This is what the queen says to the king.
She says, do you love your son? He's, oh yes, I love my son.
What if he changed?
What if he died? But not just died. It doesn't take death.
Because it's not really beings that we're attached to.
It's experiences.
And so if your son is, our daughter is always saying nice things to you.
Or it's very cute.
But then they grow up and they get a foul mouth.
They turn 13, 14, and they start saying nasty things.
Talking back, fighting, arguing, so much anger comes, so much anger.
More anger than if someone you didn't know.
More anger than if someone you didn't love were to say or to respond in that way.
Because it's change. Because we're stuck on that.
We're stuck on something that is not real.
We're stuck on things being this way.
But that way is uncertain.
If you could be stuck on things being always the way they actually are,
then that would be something else.
But that's not how desire works.
Desire is not something where you can just like whatever happens.
This liking is a stickiness.
It's nothing like reality.
It's nothing like mindfulness.
The only way to be content or to be happy with whatever comes is to be perfectly in sync with reality.
It's why people will say,
fine and good things that we love can cause us stress and suffering.
But if you give them up, then you give up all the happiness that comes from them.
And they think of Buddhism as kind of this,
it's an option where you give up all happiness in order to give up all suffering.
So no happiness, no suffering.
But it's not really like that.
We have a natural way, a way that is in tune with reality that has none of this stress.
And then you have this stressful way that is really involved with wrong view.
And it's a pernicious and highly deeply ingrained wrong view.
But you have to understand that it's wrong view,
that it is not a given that happiness comes from seeking out things.
No, it's actually wrong from a Buddhist would argue that it's wrong.
Which I think is harder to accept and maybe it even sounds.
Deep down, we have this axiom that if you're looking for happiness, seek out that which makes you happy.
So we take meditation in that way, right?
I'm doing meditation because it's going to make me happy.
I have that mindset even for meditation.
But that whole idea, the idea of seeking something out, the idea of clinging to something,
of liking something, that that might lead to happiness is wrong.
And the state of liking actually takes away your happiness, takes away your peace of mind.
It takes away your connection with reality, puts you in a new frame of reference where the only variables are,
do I have what I want or do I not have what I want?
And the only happiness comes from always getting what you want, which of course is impossible.
And it's not even happiness because it's not real.
It's no longer, I am experiencing this, this is what's happening.
I am getting what I want.
I am liking, I am wanting.
It's an unnatural state, just state of mind that is no longer natural.
So there's more to it than simply saying, well, we're going to give up things.
There's this idea that in Buddhism you have to give up everything that makes you happy.
But that's not really how it works, right?
The point is that's not how it works.
Happiness doesn't come from wanting or liking things.
Happiness is a result of the result of peace of mind.
And so there's two ways to get peace of mind, right?
To be constantly fulfilling the objects of your desire or to stop desiring anything.
And then the point is that even if you're constantly fulfilling the objects of your desire,
the nature of it is such that you're not satisfied.
The nature of desire is to breed for their desire.
The nature of contentment is to breed contentment, so it's quite a different state.
You can't say, I desire something and so that breeds contentment.
It's not like that habits are not static.
They don't lead to other things, habits lead to perpetuating themselves.
So a habit of mindfulness of contentment leads to further mindfulness and contentment.
A habit of wanting and liking leads to more wanting and more liking, stronger and stronger.
Attachment to a specific state and that attached state in and of itself is artificial.
It's artificial.
Well, I'd encourage you to take up meditation and study.
Really the important thing is that when we talk about losing a person,
we have to look at what's actually going on in the mind.
So if you focus your attention on the being, my son died, my daughter died,
my husband, my mother, father died.
It's very hard to see the reality of the situation.
That's not the reality of the situation.
There was a great pleasure of possession and the stability and the certainty of that thing,
that person, that concept is now impossible.
You never will see that person again.
So seeing the experience of seeing is no longer accessible to you.
You will never hear them speak.
You will never feel their touch.
You want to see them smile, we want to see them play, we want to see them laugh.
You want to hear them talk to us and say nice things to us.
Because again, it doesn't take death that person can just stop being nice to you.
You have husbands and wives who love each other and then suddenly start hating each other.
It's worse than if they've died.
So what's really going on is made up of experiences.
The experiences that you were attached to are no longer attainable.
And instead you have experiences that you don't want.
And so if you study those experiences, it really changes your perspective.
I think that people get so caught up in this concept, you know, I lost this person or this person died.
It's really just conceptual.
From a meditator point of view, you hear this and you think, well, why are you crying?
It's like a person is hitting themselves and you say, why are you hitting yourself?
It's really what crying is all about, you know, a person who gets upset and say, oh, I'm so upset.
Yes, like the Buddha says, he says, yes, yes.
It's something that causes, when you like something, it causes something.
It's really like someone hitting themselves, you say, why are you?
I'm suffering.
I'm suffering.
Yes, so why?
Or at least it's a shame you can't see.
It's a shame you can't be more mindful about your stress and suffering.
Because their attention is focused on the being, the concept, not on the reality.
The meditator looks at the world quite differently than rather than thinking, I lost this person, I lost that person.
They will think, here is a state of sadness.
They might still be sad, of course.
But if through the training of meditation, they'll be able to say,
here is sadness, and it's because of not getting what I want.
And they'll be able to note the sadness and let go of it.
They'll be much better equipped to deal with the problem, right?
A person who is, because take two people, one is mourning,
and their only recourse is to, well, it would be to get back the thing that they lost
which often times is impossible.
And the meditator, who is also mourning, but their recourse is to change the way they look at the experience,
the way they look at the object, and to be able to change that habit, they change their perspective.
And they observe, and they see that they're causing themselves suffering.
It's a person who actually sees, oh, I'm hitting myself, I better stop that.
The person who is thinking about the being, they have no concept of what they are doing to themselves
by regurgitating, rehashing these memories, and reaffirming their upset, their sadness.
I say, I'm so sad, I'm so sad.
So again, there's not much to this, but it really is a concept that is very key to Buddhism, of course.
And very hard for people to swallow.
Even the king wasn't up to it, but this man, even going to see the Buddha,
could you imagine sitting in front of the Buddha and saying, you are so wrong.
Hired, it's so hard for us to see because of our, because of the very nature of how we look at happiness.
It's deeply ingrained in us to think that happiness should be found from getting things.
Then we never stop to look at, what is that process of getting things like?
What actually happens when we do that, when is it actually?
What is the actual experience of it?
When we see that, oh, it's not satisfying or pleasing at all, really.
It's quite stressful all around, getting what you want.
Stressful, because it's a constant, an increase of desire.
Not getting what you want is, of course, an incredibly stressful.
So, just some thoughts and some dumb of her tonight.
Thank you all for tuning in. Have a good day.
