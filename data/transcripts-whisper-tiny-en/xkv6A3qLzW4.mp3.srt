1
00:00:00,000 --> 00:00:17,600
And so now for the third question, what makes you laugh?

2
00:00:17,600 --> 00:00:37,600
Well, let me try not to. One of the things that I find really funny, things I do find ordinary things funny.

3
00:00:37,600 --> 00:00:46,600
But something that I find funny which I think people who haven't followed the path that I follow probably wouldn't find funny,

4
00:00:46,600 --> 00:00:52,600
is people's set way of thinking.

5
00:00:52,600 --> 00:00:59,600
So like when I say that monks don't laugh, for most people that's almost offensive,

6
00:00:59,600 --> 00:01:10,600
it's a real turn off because our way of thinking is that if you want to be a proper person,

7
00:01:10,600 --> 00:01:17,600
there's someone who enjoys laughter. And it's really true, monks don't laugh that much.

8
00:01:17,600 --> 00:01:28,600
The Buddha himself has said to never have laughed his way of dealing with a humorous situation would be the smile.

9
00:01:28,600 --> 00:01:36,600
Because his mind was so subtle, so refined that all that would arise is the smile.

10
00:01:36,600 --> 00:01:44,600
And I think this is somewhat worth emulating from my point of view, but I know the point is that a lot of people find that repulsive.

11
00:01:44,600 --> 00:01:56,600
And as with a lot of things, it's interesting to find people who see me go on arms round and they think you can't do that.

12
00:01:56,600 --> 00:02:09,600
It's just somehow offends their sensibilities. Or it seems impossible, the idea of going without food for some people is totally inconceivable.

13
00:02:09,600 --> 00:02:19,600
The idea of wearing robes when I went to visit my mother, she lives in New York.

14
00:02:19,600 --> 00:02:26,600
And it was February, it was the worst weather they'd had in 20, 30 years or something like that.

15
00:02:26,600 --> 00:02:34,600
And when I touched down in the airport, it was like negative 20, which I don't know, I'm from Canada.

16
00:02:34,600 --> 00:02:44,600
It was incredibly cold, like 20 below zero, where I'm from Canada, whatever that 20 degrees Celsius below freezing.

17
00:02:44,600 --> 00:02:51,600
All right, this isn't American anyway, so anyway.

18
00:02:51,600 --> 00:02:57,600
So before I showed up, she said, you know, I'm coming and I'm just going to come to visit you.

19
00:02:57,600 --> 00:03:01,600
And she said, okay, well, I'll prepare a room for you. And I said, well, you know, I don't think it's proper.

20
00:03:01,600 --> 00:03:05,600
I'm a monk now, probably better or not. I'd better for me not to stay in your house.

21
00:03:05,600 --> 00:03:09,600
And she said, well, the more you're going to stay, I said, well, I've got this tent.

22
00:03:09,600 --> 00:03:17,600
And I was thinking of staying in your backyard. Oh, and she got so upset. She said, you can't do that.

23
00:03:17,600 --> 00:03:24,600
You can't. You can't. You can't over and over again. They said, no, I'm listening. You can. I can.

24
00:03:24,600 --> 00:03:36,600
And I did. And it was fun. I had to have used like three, four sleeping bags just to sleep at night.

25
00:03:36,600 --> 00:03:43,600
But I wore these robes and I wore the robes around and I wore them in Canada. I wore them all over.

26
00:03:43,600 --> 00:03:52,600
Things like people's idea of sickness, how sickness is such a terrible thing and it's such an awful experience.

27
00:03:52,600 --> 00:04:04,600
It's a real drag on life. And just my own take on sickness or a Buddhist take on sickness is that, you know, actually, it's kind of an interesting experience.

28
00:04:04,600 --> 00:04:13,600
And I've often said, you know, probably not the, you know, it's something that people don't can't relate to.

29
00:04:13,600 --> 00:04:21,600
And this idea that sickness might be an interesting experience. And I'm not saying this, you know, totally unexperienced.

30
00:04:21,600 --> 00:04:33,600
I've been in India. And every time you go to India, you get sick, very sick. You can even die from the bacteria there.

31
00:04:33,600 --> 00:04:43,600
And so I've been in dire straits. And I found that it was by using it as a meditation object. It can be really enlightening.

32
00:04:43,600 --> 00:04:49,600
It can teach you things about yourself that you didn't know, teach you things about the body and about the mind.

33
00:04:49,600 --> 00:04:54,600
So I think people's fixed ideas about, you know, who you have to be and what you have to do.

34
00:04:54,600 --> 00:04:58,600
I mean, simply the idea of becoming a monk is ludicrous, is absurd to people.

35
00:04:58,600 --> 00:05:05,600
And this isn't how you live your life. You're irresponsible. You're a bomb and so on and so on.

36
00:05:05,600 --> 00:05:09,600
I think that's humorous. I think it's funny to try to challenge these things.

37
00:05:09,600 --> 00:05:18,600
And so I would often make statements that challenge people's ideas.

38
00:05:18,600 --> 00:05:28,600
Type people are especially funny or Asian people in general for this because they're really nice people.

39
00:05:28,600 --> 00:05:33,600
But oftentimes people grow up in a society with a religious culture and they don't really know much about the religion.

40
00:05:33,600 --> 00:05:41,600
So type people think they're all Buddhist and oftentimes their practice is totally against the Buddhist teaching.

41
00:05:41,600 --> 00:05:48,600
You know, if you just try to explain it to them, they don't listen, they think, oh no, what do you know?

42
00:05:48,600 --> 00:05:53,600
I mean, obviously I'm white. What do I know about Buddhism?

43
00:05:53,600 --> 00:05:59,600
And so challenging, you know, the best way to do is to just shock them out of it.

44
00:05:59,600 --> 00:06:02,600
They'll say things like, you can't do that. That's not proper.

45
00:06:02,600 --> 00:06:07,600
And they say, well, watch me. I'm going to do it. And showing them that it's possible.

46
00:06:07,600 --> 00:06:13,600
Not touching money. I haven't touched money for, I don't know, how many years, six, seven years.

47
00:06:13,600 --> 00:06:19,600
And people will tell me even today that I can't do it, that it's not possible.

48
00:06:19,600 --> 00:06:24,600
And I think I'm living proof that it's possible. I don't touch money. I don't use money.

49
00:06:24,600 --> 00:06:33,600
I don't keep food. I don't, you know, and there was one time, this is another good example.

50
00:06:33,600 --> 00:06:40,600
There was a problem with where I was going to stay. And there was a lot of argument about where I was going to stay that night.

51
00:06:40,600 --> 00:06:46,600
I had flown back from Minnesota after teaching there, touched down in Los Angeles.

52
00:06:46,600 --> 00:06:57,600
And on my way back, I found out that I wasn't going to be able to stay at this house, where I thought I was going to stay.

53
00:06:57,600 --> 00:06:59,600
And so I had to go and stay.

54
00:06:59,600 --> 00:07:06,600
Nobody really said where I was going to stay, but I think they just assumed I was going to go to one of my students' house and stay with him and his mother.

55
00:07:06,600 --> 00:07:19,600
And this is the reason why I didn't go and stay with my mother is because a monk can't generally stay in the same building as the same residence unit as a woman.

56
00:07:19,600 --> 00:07:26,600
And so they just assumed that I was going to stay there. When we got to their house, I just walked off.

57
00:07:26,600 --> 00:07:33,600
It was about midnight. And we got to the driveway. I just said, okay, I'll see you tomorrow. And I just walked off down the street.

58
00:07:33,600 --> 00:07:47,600
And this whole group of people just freaked out and they were driving their cars around trying to find me. I didn't realize it until the next morning that there was like a man hunt out for me.

59
00:07:47,600 --> 00:07:58,600
And I just went to a park and laid down under a bench on the cement in North Hollywood and slept under this picnic bench on the cement.

60
00:07:58,600 --> 00:08:06,600
Just put my robes, you know, just flattened my robes out on the cement, kind of bunched them up so the cement wasn't that hard.

61
00:08:06,600 --> 00:08:15,600
And you know, kind of sat some lay some and just spent what I believe to be one of the best nights of my life.

62
00:08:15,600 --> 00:08:20,600
And I think this is unfathomable for many people that this could somehow be.

63
00:08:20,600 --> 00:08:26,600
Why it was one of the best nights of my life is because suddenly I was free from all of this argument and difficulty.

64
00:08:26,600 --> 00:08:32,600
Suddenly I was free. I was me, just me and my robes.

65
00:08:32,600 --> 00:08:40,600
And my ball and I put my ball under the picnic bench and I just got a crawled underneath and just sat there all night.

66
00:08:40,600 --> 00:08:46,600
And sometimes lay down, I did sleep actually, you know, when you're tired enough you'll sleep on anything.

67
00:08:46,600 --> 00:08:54,600
And so this is something that makes me laugh, is challenging people's perceptions and opening their minds.

68
00:08:54,600 --> 00:09:06,600
And even just walking down the street is a real eye opener. I'm told that when, as I said before, I'm told that when I walk down the streets and down town people just go like this, their heads just turn.

69
00:09:06,600 --> 00:09:14,600
I find that sort of thing funny. Funny in a good way, you know, opening people's minds up, letting them see something new.

70
00:09:14,600 --> 00:09:21,600
And that's again one of the real reasons why I'm involved in this video, it is projected all.

71
00:09:21,600 --> 00:09:27,600
Because I think showing people alternative ways of life to their own is a real eye opener.

72
00:09:27,600 --> 00:09:33,600
It's something that can help the world, something that can be great for humankind and hopefully will have a real impact.

73
00:09:33,600 --> 00:09:42,600
Help us to see that we are one and that we have to work together or else we're going to go down the toilet together.

74
00:09:42,600 --> 00:10:05,600
Okay, so that's all for this question. Thanks.

