And before we practice forest dwellers practice, we need to understand which is forest,
that which is village in contrast to forest.
And in order to understand the village, we need to understand the presence of a house.
Well, what is a house, and what is its disease, and then village, and then forest.
So, a village is a dwelling, a habit of human beings.
It may, it may consist of only one house, or many houses.
It may have a wall surrounding it, or there may be no wall.
And human beings may be living there, or they are not living there at the present moment.
Even a caravan is called a village here, which has been inhabited for at least four months.
So, such a place is called a village.
And then the, the, we saw the omega describes the presence of a village.
And you will find, you will find it in paragraph 48.
So, the present of a village is determined, according to whether the village has a wall surrounding it, or not.
So, if it has a wall surrounding it, and if it has two, two door posts or gate posts,
like that of the city of Anudha, Anura Tabura in Ceylon.
Anura Tabura is in ancient city, and it, it was the, the place of,
of the Thera Vada Buddhism, or stronghold of Thera Vada Buddhism in Sri Lanka.
It was, in that city, that the, the great monastery was situated, where a Buddha goes up,
went and, and got permission from the monks there to compile a new set of commentaries.
And it seems that there are two, two gate posts, one inside the other.
So, a person standing, not between the gate post as, as is stated in the, the translation.
But a, a, a person standing at the inner gate post.
So, gate post may be a gate post, may be true, one, the outer gate post, and the,
the other inner gate post.
So, a person standing at the inner gate post, and then throw a stone.
And how, how, how, how, how, how should the throw the stone be thrown,
that there was a difference of opinion here.
So, those who are well versed in Vinya said that they should throw the stone as when,
they want to show the strength, that means at the max, where the maximum strength,
they should throw the stone and that the, the, the place where the stone falls is,
the present of it called present of a gamma, I mean, village.
But those who are well versed in Sotanta said that, that the, the stone should be thrown
not in that way, but just as when you scare away the crows, so it may not,
you may not use much, much strength when you scare away the crow.
So, and that way you, you throw a stone and the place where it falls is,
the present of a, of a village.
Now that, that is with regard to a village, which has a wall around it.
What about the village, which has no wall?
Then the outer moost, there, there must be an outer moost house.
So, from the present of that outer, outer, outer moost house, one must throw a stone
in the same way.
So, a house, present of a house, a village, present of a village.
Now, someone standing at the present of a house, throws a stone and the stone falls
and that is village.
And then another throw of a stone is the present of a village.
That is with regard to villages that have no walls surrounding them.
And outside that, that area is called a forest.
Now, what is a forest?
A forest means a place other than the village and the present of a village.
It is not boundary, it is a, it is some area supposed to be belong to the village.
It is not the village proper, but it is visinity of a village or the area of a village.
And area, outside the village proper, but supposed to belong to the village and outside
that, that area is what we call a forest.
So, forest according to vigna is one thing and to abilma is another and to sotanda
is still another.
So, we have three kinds of forest.
According to vigna, a place other than the village and the village present is called
a forest, and according to abilma, the area outside the gate post, immediately out
of the gate post is called a forest.
So, there is difference.
And according to sotanda, still another and we shall have to follow the sotanda method
here, and sotanda method is a place at least 500 bow lengths from a village or village
present.
And that bow should be, should be the one used by the teachers teaching archery and it
is a strong bow.
So, strong bow is said to be about four qubits in length, so it needs about six feet.
So, six feet in two, six feet multiplied by 500, about 3000 feet.
So, about 3000 feet from the village or from the present of a village is called a forest.
So, such a place is a forest here.
And if the forest, forest monastery has no wall, then from the outermost, we have to measure
from the outermost building.
And then from that building, we have one throw of stone and then another throw of stone.
And then, between these two forces of stone throws, we must have 3000 feet, there must
be 3000 feet.
And the way to throw stone is, according to the, according to that stated by the commentaries
and magi, magi, magi, magi, the middle length, the collection of middle length sayings.
So, roughly speaking, a place called a forest should be away from human, human habitat for
about 3000 feet, about 1,000 yards.
If it is closer than that, then it is not called a forest.
And on page 73, at the end of the 50th chapter, there is a statement, this is the measure
here.
And actually, measure means authority, it is the authority here.
The value of juice is pamana, which can mean measure.
And also, which can mean means of right knowledge.
Now, in hindu logic, there are means of right knowledge.
You get right knowledge by perception, by inference, and by textual authority, and so on.
So here, the measure really means authority.
So this is the authority here, that means we have to follow the magima commentary.
And in the magima commentary, it is that omitting the presence of the monastery and the village.
The distance to be measured is that between where the two stones fall.
That means stone fall from the monastery, and stone fall from the village.
And between these two stone falls, there must be 500 bow lengths.
No, these two 1000 yards from the stone throw.
Stone throw should not be 1000 yards or 500 bow lengths.
So between, should say, you stand at the gatepost of the village and throw a stone.
And then the other person stands at the precinct of the forest monastery and throw a stone.
And then between these two throws of stone, there must be 1000 yards.
What point does a religious document make define a secular, something like distance, which seems to be governed by this?
That's not the governmental measurement.
But I think, if it is about 1,000 yards away, then you get some seclusion.
You don't hear sound noise from the village, but that is, according to, in the olden days.
But nowadays, you can get noise everywhere from the plane, from the loudspeakers.
So it is very difficult now to get a place, which we can really call a forest.
And then, even if the village is closed by, and the sounds of men are audible to people in the monasteries.
Still, if it is not possible to go straight to it because of rocks, rivers, etc., in between the 500 bow lengths can be recognized by that road.
That means by the regular road, even if one has to go by boat.
Maybe the road is winding, but if that road is 500, I'm poor length and it is all right.
But anyone who blocks the path to the village here and there for the purpose of the language,
so has to be able to say that he is taking up the practices, cheating the ascension practices.
The Hollywood used is a thief, a thief of acetic practices.
I think the others are not difficult to understand in this.
So a person who takes up this practice should, the best person, the strict person should always,
should always meet the dawn in the forest.
So he must be in the forest at the time of dawn.
The medium one is allowed to live in the village for the four months of the rain.
So when they are, when they are a rain, he can live in the forest and the monastery.
But the last one, the mild one, could stay in the Vihara for winter months too.
So four months of rain season, four months of winter.
Then he practices this acetic practice only for four months during summer.
So this is the mild one and the benefits are not difficult to understand.
So this is the forest dwellers practice.
And the next practice is tree root dwellers practice.
That means living under a tree, and that person should avoid some kind of tree given here.
A tree near a frontier, because there can be fighting between two kings or two people
who rule those places.
So he must avoid such a tree.
And then he shrines tree because we will gather around that tree and so he could not get
a seclusion.
If gum tree, that's dangerous, if fruit tree, if bed tree, a hollow tree means a tree as
a hollow soul that snakes and other wild animals can live there.
Or if tree is standing in the middle of a monastery, because many people come to the monastery
and so he could not catch the pollution.
He can choose a tree standing on the outskirts of a monastery away from the place where
many people gather.
So that is such a tree he has to choose.
And also three grades.
The first one, the strict one, is not allowed to have a tree that he has chosen tidied
up.
He must live there, just as it is.
He can only move the fallen leaves with his foot while dwelling there.
He must not make that place lovely and attractive.
But the medium one is allowed to get it tidied up only by those who happen to come along.
But the mild one can take up residence there after summoning monastery attendance and minovices
and getting them to clear it up, level it, and then scatter sand and make a fence round
with a gate fixed in it.
So there are always three kinds of people.
And the moment he enters a covered place when he breaks his practice.
The next one, living in the open, open air dwellers practice.
So no building, no tree, but he must live in the open air.
And this is possible, maybe only in tropical countries or in Asia, not in this country.
So here is a lot to enter the Uposeta house, means a house where monks assemble twice a month
and recite the rules of party monk and listen to the recitation.
For the purpose of hearing the dhamma or for the purpose of the Uposeta, if it rains while
he is inside, he can go out when the rain is over.
Instead of going out while it is raining, he is allowed to enter the eating hall or the
fire room, fire room means room where what you call them here, something like so in order
to do the duties or to go under a roof in order to ask elder because in the eating hall
about a meal that means to invite them or a meal or to ask them what they want or when
teaching and taking lessons or to take beds, chairs, etc., inside that have been wrongly
left outside.
So for that purpose, he can enter the place with the roof.
If he is going along a road with the requisite belonging to his senior and it rains, that means
he is taking some things of his senior monk.
And so if he is doing so, he is allowed to go into a website rest house and he could go
into a rest house.
If he has nothing with him, he is not allowed to hurry in order to get to the rest house.
But he can go at his normal pace and enter it and stay there as long as it rains.
And then here also there are three.
So the strict one is not allowed to lift near a tree or a rock or a house.
See, it must be away from there.
That means too close to the tree and so on.
He should make a rope tent right out in the open and lift in there.
That means a tent made of cloth or robes.
It may be used to what of wind and heat and coal.
The medium one is allowed to lift near a tree or a rock or a house so long as he is not covered
by them.
So he could lift near and near the tree.
The mild one is allowed these.
A rock overhang without a drip ledge cut in it.
It is something like a patio, a rock.
And when the rain comes, the rain drops fall and flow away and not into.
So that is such a place.
A heart of branches, a cloth stiffened with face and a tent treated as a fixture that has
been left by feet, watches and so on.
Sometimes they put up such a building and then when they lift, they just lift this building.
So he could take up that building.
Then the next one, channel ground, channel ground dwellers practice.
Now, the channel ground dwellers should not lift in some place just because the people
who build the village have called it, the channel ground for it is not a channel ground unless
it dead body has been burnt on it.
So when the first build village, then the photosite, some space, some site for cemetery.
And if no dead body has yet been buried or cremated, as it is stated here, then it is not
jetty cemetery.
So at least one body must have been burnt there.
But as soon as one has been burnt on it, it becomes channel ground.
And even if it has been neglected for a dozen years, it is still, it is still.
And a person who dwells at the cemetery has to be very careful and he has to follow some
kind of regulations.
So he should not be the sort of person who gets walks, pavilions, etc.
But the build has burnt and chairs set out and drinking and washing water capability
and preaches dhamma, he must not do all these things there.
For this practice is a momentous thing, it is a very important thing.
Whoever goes to live there should be diligent and he should first inform the senior elder
of the order or the King's local representative in order to prevent trouble.
That is sometimes thieves may frequent that place and they may leave something there.
And so when the owners came and the thieves had left, then they may take the monk as a thief.
And so that is the danger of being suspected.
So a monk who is going to live in a cemetery should inform the senior monk at the monastery
and also an official of the government.
When he walks up and down, he should do so looking at the pyre with half an eye.
So he something like looking at the pyre and work and looking at the pyre and so on.
On his way to the channel ground, he should avoid the main roads and take a pipette.
He should define all the objects there while it is day.
So he must go there during the day and then make notes of the things there.
Because if he does make notes of the things and when he goes there at night, he may get
frightened of some things there.
So he has to make notes of all these things.
So that they will not assume frightening shape for him at night.
Even if non-human beings wonder about screeching, he must not hit them with anything.
So Samadri is supposed to be the place of ghosts and spirits and they may make noise.
It is not allowed to miss going to the channel ground even for a single day.
The recitals of the Ankutra say that after spending the middle watch in the channel ground,
he is allowed to leave in the last watch.
So the night is divided into three parts, the first watch, second watch, and third watch.
So spending the middle watch in the channel ground and he is allowed to leave in the last
watch.
It is about from 2 to 6 in the morning.
From 6 to 10 is the first watch.
So 10 to 10 pm to 2 am is the middle watch and 2 am to 6 pm, the last watch.
He should not take such foods as sesame flour, peas pudding, fish, milk, oil, sugar, etc.,
which are lacked by non-human beings.
He should not take means he should not eat.
Not take these things to this imagery.
He cannot do that either.
So he must not eat these too.
What's peas pudding?
I don't know, what bread means.
What is the meaning of peas?
Well, what is the meaning of peas?
P-E-S-E?
It's sesame.
Sesame?
Sesame?
Sesame?
That's the answer I thought.
Please.
I think it is peace.
I think it is peace.
I think it is peace.
That's right.
People.
People.
People.
People.
Yeah, Masa Bhatta here.
Yes.
It may be rice mixed with peas.
In our countries, people, people prepare rice with peas.
So it should be peas.
No, we did this dry, also to dry lentils and beans, things like that.
Mm-hmm.
They may not brush peas so much.
Oh, like split the lentils.
Yeah, yes.
Yeah.
I think it's good.
And then he should not enter the homes of families.
And the reason is given in the footnote.
He should not go into families' houses because he smells of the dead and is followed
by peace as a goblins.
And the strict one should stay where there is constant.
Mm-hmm.
What about that?
There is always burnings and corpses and mornings.
And the medium one is allowed to live where there is one of these three.
Burning, corpses, people crying.
And the mild one is allowed to live in the place that possesses the bare characteristics
of each other on already stated.
So if the place is the one where at least one dead body has been bunched, then that is a place
for him.
Would it be likely for them to see non-human beings?
It just believed that there are non-human beings at the Charno-Grown of Symmetories.
They may, they may show themselves to the monk staying there.
They may, because it is believed that ordinary human beings cannot see, see ghosts.
But if they wish, they can show themselves to the, to the human beings.
The ghosts can.
And they can, they can assume frightening features.
And then frightened the people away from the Charno-Grown.
What is the actual experience?
That monks would frighten to see spirits?
They want to, they want to scare people away from the place.
But if monks must be firm and he must not be afraid of them.
But he must not hit them with anything or whatever.
If he wants to hit, he must hit them with mitta, loving kindness.
So a lot of loving kindness is necessary for monks who live at the Symmetories.
And this is undertaken as a special practice to shake away the defilements.
Then the 12th one.
It sounds like among the three types, the hate type prohibition goes to the Charno-Growns.
It may be a great, great would be it.
That's right.
It is most suitable for those who are greedy.
Okay, the 12th one is what?
Any bad users.
Is that 12th?
Yes.
Any bad users.
Actually, it does not necessarily mean any bad.
Is there any place for him to stay?
It may be a small, small hut for him.
So now, when he was in visit some monastery, then he must go to a monk who is in charge of assigning places or assigning huts to guest monks.
So, if he is assigned a hut, then he must take that.
The strict one must just take that and he must not refuse to take or he must not go and see before accepting it.
So it is called any bad users or any hard users.
So, the any bad users should be content with whatever resting place he gets thus.
This resting place here means just a place for him to stay.
This falls to your lot.
He must not make anyone else shift from his bed.
Sometimes, all the monks have the right to claim a hut given to a younger monk.
But if he is undertaking this practice, then he must not do that.
He just take what is given to him or what falls to him.
And there are three grades.
One who is strict is not allowed to ask about the resting place that has fallen to his lot.
Is it far?
Is it too near or is it infested by human beings, snakes and so on?
He does not have to do any of these things.
But the medium one is allowed to ask but not to go and inspect it.
The mild one is allowed to inspect it and if he does not like it, to choose another.
Now, the last one.
Sitting men.
Sitters practice.
One who is strict is not allowed a bed rest or cloth band or binding strap to prevent falling
while asleep.
The bed rest is obvious.
It is not difficult to understand.
But the cloth band and binding strap are difficult to understand.
So, cloth band means some kind of cloth.
You put around your body when you sit.
Maybe something like this.
So that you do not fall down.
A cloth or a rope.
The other one is a band, not necessarily a big cloth, but a band of cloth wrapped around
the body.
And there is one other thing but it is not mentioned here.
That is hand band that means sitting this way.
It is not allowed for months.
Is the meditation posture already any time?
At any time, it is not allowed for months.
In our tradition, we are so at least from stories in the comments.
There is also something called a chin rest.
There is a stick.
Oh, yes.
No, not the chin rest.
It is a cloth wrap around the body or a band of cloth.
The medium one is allowed.
Any one of these.
And the mild one is allowed a bed rest.
A cloth band, a binding strap, a cushion.
A five limb and a seven limb.
A five limb is a chair made with full legs and a support for the bed.
So whichever, which has a bed rest.
The seven limb is one made with full legs.
It is a support for the bed and an arm support on each side.
So most chairs here nowadays are seven limbs.
Like that four feet, back rest and one on each side for the hand to rest.
So a mild one can use such such a chair to sit on it and sleep on it while in a sitting posture.
He may use a cushion.
They make that it seems for the elder petar bear.
Some people made such a thing.
It's seven limbs.
Please, I mean, chair for the elder petar bear.
And he practiced that ascetic practice.
And he practiced meditation and he became a non-returner.
And then ati nibana.
So it is allowable to use such things when you take up this ascetic practice.
And I think you have heard of the sero at tambulu,
who was the founder of tambulu monastery in Boulder Creek.
And he always undertook this practice.
So he never lies down to sleep or whatever reason.
So he always slept on a chair or on a couch.
And also all of his desirable monks have to undertake this kind of practice.
It's not easy.
Always.
Yeah.
So you don't see a bear there at tambulu monastery.
You see only couches or not couches, what do you call that?
Chairs and something like that.
And this state and this leaf this way, resting on the back rest.
So these are the 13 ascetic practices.
And we just said to shake of defilements.
So in order to shake of defilements, monks have to practice one, two, or more of these ascetic practices.
As and when, they are able to.
Now we come to the explanation on miscellaneous aspects.
And the first one is treating ascetic practices according to abitama.
So the first one is what?
As to the profitable triad.
So that is difficult to understand, right?
Page 79, bottom of the page.
So as to the profitable triad, all the ascetic practices that is to see those of trainers,
ordinary men and men whose kankas have been destroyed may be either profitable or in the
arhans case indeterminate.
Now, at the beginning of abitama, all the ultimate realities are treated in triads, different
triads.
There are 21 such different triads.
So in the first triads, it is said that there are those that are kusala.
There are those that are kusala.
And there are those that are neither kusala nor kusala.
So to treat or to classify the ascetic practices according to this triad,
it is said that the ascetic practices are either kusala or indeterminate neither kusala nor
kusala.
There can be no acusala ascetic practice.
When an ordinary person who has not reached any of the enlightenment stages of enlightenment
and also those who have attained enlightenment but not yet arhans.
So when they practice, their ascetic practices are said to belong to kusala.
But when the arhans practice, then the practices belong to neither kusala nor kusala
or indeterminate.
So there can be no acusala in the ascetic practices.
But there is someone who says, can you practice one of these practices with the evil purpose?
I want to be popular with people, I want to get gifts from them.
And so I pretend to be practicing these ascetic practices.
Then they cannot, they are be acusala then.
So the answer is no.
Even though you can live in a forest with the acusala mind, acusala consciousness.
But the ascetic practice cannot be acusala.
Acetic practice means the practice of an ascetic.
An ascetic is a one who shakes of acusala.
So a person who does not shake, shake of acusala is not called an ascetic.
If he cannot be called an ascetic, his practice cannot be called an ascetic practice.
So ascetic practice cannot be acusala.
It can either be kusala or in Bali it is called abiagada in determinate.
It means neither kusala nor acusala.
And also an ascetic practice is defined as knowledge.
Knowledge is called an ascetic practice also.
Knowledge here means punya, punya means either kusala or in determinate.
And there can be no punya in acusala.
So since ascetic practice is taken to be punya, it cannot be acusala.
It is kusala or abiagada.
So ascetic practice cannot be acusala.
So only two kusala or abiagada holds them all in determinate.
Now there are some people who say that the ascetic practices are out of this triad.
They do not belong to any of this triad.
So ascetic practices do not belong to wholesome or unwholesome or in determinate.
They are out of this triad.
Thank you.
