So, we are on page 782, we are almost at the moment of enlightenment, we are very close to
the moment of enlightenment, so the yogi progresses from one stage of knowledge to another,
and so the last one was an equanimity about formations, so when his knowledge of
equanimity about formations becomes mature, then there will come a time when he gets enlightenment,
so as he repeats develops and cultivates that equanimity about formations, his faith becomes
more resolute, his energy better exerted, his mindfulness better established, his mind better
concentrated, while his equanimity about formations grow more refined, and then he thinks
now the path will arise, so as I have told you, this pali idiom was misunderstood by
all English translators, there was one professor who translated this would be mother in English,
so he also did not get it correct, it is not that he thinks that now the path will arise,
because the yogi doesn't know when the path will arise, but the idiom here used is like
when the path is about to arise, so there is a meaning, so we can say when it should be said
now the path will arise, something like that, when it should be said now the path will arise,
it means the same thing, so when the path is about to arise, that means when the person is about
to get enlightenment, then equanimity about formations after comprehending formations as
impermanent or as painful or as not self sinks into the life continuum, life continuum means
bowanga, there are types of consciousness which are called life continuum, and they are
say inactive movements of consciousness, they are more prominent when we are asleep,
or when we have printed something like that, so when he is about to get enlightenment,
then there must come one thought process, so in order for that thought process to arise it must be
it must be a goal, there must be an interval of life continuum, so the yogi is practicing
meditation and his knowledge of equanimity about formations, group, group and refined, and then
there comes the life continuum, maybe not only one moment, a few moments of life continuum,
then after the life continuum next to the life continuum mind or adverting arises, making formations
its object as the impermanent or as painful or as not self according to the way taken by
equanimity about formations, so after the life continuum there arises what is called mind
or adverting consciousness, that means from that moment on the mind is consciousness is
turns towards the mind or turns towards the turns to the active moments of consciousness,
so there are moments of bhavanga which are inactive moments, then after that the mind or adverting
arises, so at that moment the consciousness is turned towards the object of that consciousness,
so it is called mind or adverting, so from this moment on comes the active moment,
the making formations its object, so that mind or adverting consciousness takes formations as its
object, viewing them as impermanent or as painful or as not self, then next to the functioning
adverting consciousness that arose displacing the life continuum, that means next to the mind or
adverting consciousness, the first impassioned consciousness arises, making formations its object
in the same way, maintaining the continuity of consciousness, so after the mind or adverting consciousness
there arises what? First impassioned consciousness, first bhavanga consciousness,
and there will be four moments of jhavanga consciousness or sometimes three,
so the first impassioned consciousness arises making formations its object,
so it takes the same object as the mind, mind or adverting consciousness,
this is called the preliminary work that consciousness, the impassioned or jhavana consciousness is
called the preliminary work, next to that is second impassioned consciousness arises,
another consciousness, making formations its object in the same way, taking the formations as
object, and this is called the excess, that means approaching or going into the divisinity of
enlightenment, next to that a third impassioned consciousness also arises making formations
its object the same way, so another consciousness, this is called conformity, these are
these are their individual names, so preliminary work, access and conformity are their
individual names, but it is admissible to call all three impassioned repetition or preliminary work
or access or conformity indiscriminately, so you can call these three by only one name, any name,
or you can call them by their individual names, so there is in this thought process,
first a mind of adverting consciousness, and then three moments of jhavana,
right, preliminary work, access and conformity, next is for what is called change of lineage,
but before that we have to understand about conformity, conformity to what,
to what precedes and to what follows, so it conforms to what precedes it and also to what
follows it, for it conforms to the function of true, actually correct function or right
function both in the eight preceding kinds of inside knowledge and in the 37 states
partaking of enlightenment that follow, so this consciousness is it conforms to the preceding
eight we pass on our inside knowledge and also conforming to those that come later and they are
the 37 states pertaining of my partaking of enlightenment, actually they are members of enlightenment,
they arise at the moment of enlightenment, so it is it is in conformity with both preceding
knowledge and the succeeding ones, since it occurrence is contingent upon formations through
comprehending the characteristics of impermanence etcetera, it so to speak say knowledge of
rice and fall in deep so the rice and fall of precisely two states that process rice and fall
and so on, as though this knowledge was saying, it is to show that it is it conforms to the
previous knowledge is, so are these rather unusual in that they are harmonious with both the
condition and the unconditioned, is that what's being said, oh no, okay, in the cities of
moments of consciousness, the conformity is one in the middle, so it conforms to what precedes it,
we pass on our stages of we pass on our knowledge, because a yogi has to go from one stage to another
and then at the moment of enlightenment and there will be the 37 states pertaining to enlightenment
or the members of enlightenment, so it conforms to these states too, so it just like like
a something like a bird, like a bridge, it conforms to both, it is a money of both proceeding
and succeeding, but is it harmonious with the unconditioned or not yet, it takes conditions
or formations still, the object is still the conditioned things or formations, because it is in
the limit of we pass on our knowledge, it has not not yet gone beyond we pass on our knowledge,
so it is still we pass on our knowledge, it must take formations or conditioned things as object,
we pass on our does not take live on as object or unconditioned as object, so the
this paragraph describes how it conforms to the previous knowledge and then succeeding ones
and then it is similarly is given of a righteous king who sits in the palace, a place of judgment
here in the pronouncements of the judges while excluding bias and remaining in partial
conforms pull to the pronouncement and to the ancient royal custom by saying so it is here too,
now excluding bias I want you to make a mark there, that means he does not, that the king does not
act through attachment or through anger or whatever, so we will come to that later on page,
let me see in the next chapter, on page 799 they are called bad ways, actually they are bad ways
being in proper action maybe, that is biasness, when you do something you are biased because
you are a test person or you are angry with that person or you are afraid of some repercussions
or simply you are ignorant, you do not know anything about that then you make
missed it there, so here the king was just so if he was not biased and remaining in partial
conforms pull to the pronouncement and to the ancient royal custom by saying so being
and so on, conformity is like the king and the 8 kinds of knowledge are the 8 judges and so on,
so this is the knowledge of conformity, now I think you remember the inside leading to emergence,
you remember these three, the inside leading to emergence, now paragraph 144, though this
conformity knowledge is the end of the insights leading to emergence that has conformations as its
object, still change of lineage, knowledge is change of lineage knowledge is the last of all
the kinds of insight leading to emergence, inside leading to emergence means
desire to get rid of get out of it and then re-contemplation and
equanimity about formations, these three kinds of knowledge are called emergence, inside
leading to emergence, emergence means enlightenment, it gets out of some sort or something like this,
so there are, so three insights leading to emergence
and then that is the formation such as object, still change of lineage knowledge is the last of
the kind of insight leading to emergence for, the last one is go travel, right?
So I'm sorry, change of lineage, so change of lineage takes nipana as its object,
but the other take, the other take, formations as object, that is the difference, although they are
called insight leading to emergence, so the change of lineage is the last one that could be thought of as a bridge.
Yes, it is something like a gate, a gate to enlightenment.
Oh, do you have this chart? No, yeah, if you look at the second page,
so you see the, you see number 11, Sanha ru pickanyana, knowledge of equanimity about formation
and then 12th, a new low manana, knowledge of conformity and 13, good raguanyana,
knowledge of change of lineage, so these three, not the desire of freedom and so on,
but these three are called go tana gamini, you see the word go tana gamini, that means
insight leading to emergence, so these three are called insights leading to emergence.
Among them, the first two take formations as object and the last one, number 13, kodugunyanana,
I mean, knowledge of change of lineage takes nibana as object, so that is what this paragraph is saying.
So, do this conformity, knowledge is the end of the insight leading to emergence, that's as
the formations as it objects, so two of them takes formations as object and 12th, knowledge of conformity
is the second or the last, so it is set here, it's the end of the insight leading to emergence,
that has formations as its object, still change of lineage, knowledge is the last of all
the kinds of insight leading to emergence, so the insight leading to emergence is the name of
three vipasana knowledge, knowledge of equanimity about formations, knowledge of conformity
and knowledge of change of lineage, we haven't come to change of lineage yet,
and then so the references are given, and in this paragraph, different words used for
the this knowledge, knowledge of conformity are described, so sometimes in some
discourses, it is called aloofness, then in another soldier, it is called dispassion, nibida,
then in another soldier, it is called knowledge of the relationship of states,
then in another soldier, it is called culmination of perception,
then in another soldier, it is called principle factor of purity,
and then in the Bhattisambi-Dhamma, it is called by the three names thus, desire for deliverance,
and the contemplation of reflection and equanimity about formations,
these three are collectively called conformity knowledge in Bhattisambi-Dhamma,
and in the Bhatta-Dhamma, it is called by two names that conformity will change of lineage
and conformity to cleansing, and in the Bhatta-Dhamma, we need a soldier that is called
unification by knowledge and vision of the way, so it has many names, and it is described
by different names in different discourses, so when we read those discourses and when we
see these words, we are to understand that this was referred to the conformity knowledge
here, described here, so after the conformity knowledge comes, change of lineage, next chapter,
actually this change of lineage is next to enlightenment, next to a path consciousness,
so change of lineage knowledge comes next, its position is to avert to the path,
and so it belongs neither to purification by knowledge and vision of the way nor
purification by knowledge and vision, now there are seven stages of purity in Vipasana,
and they are given in this chart, so the knowledge of change of lineage does not belong to
any of these purity, it is just in between, it is the one purity and the other, its position is to
advert to the path, and so it belongs neither to purification by knowledge and vision of the way
that is previous one, nor to purification by knowledge and vision of the succeeding one,
so it is, but being indeterminate, it is unassignable, we cannot say that it is included in
the previous purity or the succeeding purity, so it is free from both purity, still it is
reckoned as insight, because it falls in line with insight, because it falls in the flow of
Vipasana, it is called Vipasana, but strictly speaking it is not Vipasana, because it does not take
formations as objects, it takes Vipasana as objects, and then purification of knowledge and vision, the
last purification, probably consists in knowledge of the food paths, that is to say the path of
string entry, the path of one return, the path of non return, and the path of other
entry, change of lineage knowledge and knowledge of the first path, here in nothing,
further needs to be done by one who wants to achieve firstly the knowledge of the first path,
now he is just very, very close to the path consciousness or enlightenment, so he doesn't have
to do any need more, for what he needs to do has already been done by arousing the inside depth
entrance and conformity knowledge, as soon as conformity knowledge has arisen in him in this way,
and the thick mug that hides the truth has been despaired by the respective force, peculiar to
each of the three kinds of conformity, then his consciousness no longer enters into a settles down on
or resolve upon any field of formations at all, so his consciousness does not want to be all
deformations at all, because he has been seeing formations as a painful and as not self,
so his mind does not cling to anything cleaves of clutches onto it, but retreats,
retreats and requires at what does form a lotus leaf, and every sign as object, every
occurrence as object appears as an impediment, so at that moment he doesn't want to dwell on the
formations or the minor matter which he has been observing all through the personal meditation,
then while every sign and occurrence appears to him as an impediment, when conformity knowledge's
repetition has ended, change of lineage knowledge arises in him, so after the conformity knowledge,
the consciousness which is called conformity knowledge disappears, then it is followed by
the consciousness called change of lineage, so change of lineage knowledge arises in him which
takes its object the signless, no occurrence, no formation, physician, nivana, so it takes nivana as
object, which knowledge passes out of the lineage, the category, the plane of the ordinary man,
and enters the lineage, the category, the plane of noble ones, now these are the meaning of the
word, Baliwa is kotra bhu, g-o-t-r-a-b-h-u, kotra bhu, in that word kotra means lineage, and bhu has two
meanings, the one is to overpower or to go beyond, and the other is to reach into or to go into
to enter, so the word gotra bhu means going beyond the lineage of ordinary persons, utu jana,
and reaching into the lineage of noble persons, so change of lineage really means
overpowering one lineage, and then entering another lineage, so passes out of the
lineage means overcome or go beyond the lineage of ordinary persons, and then enters into the
lineage of noble persons, because after this path will arise, as soon as path arises he becomes
a noble person, he is no longer an ordinary person, so at this point he changes into, not yet
changes, but he prepares to change into another person, that is why it is called lineage.
After the enlightenment, they will come reflecting, reflecting on the path through them, so on,
so that then he knows, but at this moment it's very, very short, you know, just one moment of
consciousness, and then the path consciousness says he has never experienced this in his life before,
and so it's like overwhelming experience, although it lasts for only one, one priest thought moment,
and after this, a priest thought moment, he has a changed person, changed mentally,
so being the first admiring, the first concern, the first reaction to never as object
fulfills the state of a condition for the thought in six ways, so it is a condition for the
thought in six ways, as proximity, continuity, repetition, decided to support absence, and disappear
and conditions, you have to go back to Pachana, in order to understand these conditions,
the Pachana conditions are described in the chapter on dependent origination,
so it serves as a condition for the thought consciousness,
how is it that understanding of emergence and turning away from the external,
is change of lineage knowledge and so on, so these are quotations, now in this quotation,
there is a footnote away from the external, and it is the misunderstanding of the turning away,
that is being affected, which turning away is imagined from the field of emergence, it is
termed external, because the unformed elements exist and it is external, so that is not correct,
now here the formations are called external, turning away from the external means,
turning away from the formations, actually formations could not necessarily be external,
so if you are watching your own breath, if you are watching your own consciousness,
or watching your own feelings, how could they be external, they are actually internal,
but here they are called external, and then the subcominary explain why they are called external,
yeah, so it is called, it is termed, it means deformations, it is termed external, because it is
external to the unformed elements, that means it is not the unformed element, unformed element means
what, they were, yeah, so it is, it is other than never enough, so it is called external,
in fact, it is not external, it is internal, so you are watching your own, own mind or mind or
matter, so it is internal, but they are here called external, because they are, they are out of
nibana, something like that, so the foot note should be, it is termed external, because it is external
to the unformed element, stop, and then the note about the unformed element, because nibana is class
that takes down on another, the internal, the higher of the abido mama, it is not necessary, although,
although it is not wrong, it is not necessary here, in the abido, the first took of abido mama,
nibana is described as external, although we, the yogis, realize, nibana, it is external, not internal,
so that is described in the first book of abido, so nibana is external there, but here,
formations are called external, just because they are not nibana, they are out of nibana,
and then here is the simulated illustrates how conformity and change of lineage occur with different
objects, though occurring in a single cognitive series with a single Edwardian. Now, one,
as a rule, one, one third process must have only one object, now if you look at the thought
process of seeing thought process, there are 17 movements of consciousness in that thought process,
all 17 movements of thought, the consciousness takes the present physical object as objects,
so there should be no, no difference of object in one given thought process,
for different consciousness in one given thought process, but here it is different,
right here, some changes, some types of consciousness take formations as objects,
and some others take nibana as objects, although it is called one thought process,
there is difference of object for some conscious types of consciousness in this thought process.
Now, this thought process begins with mind or Edwardian, and then three movements of impulsion,
and after these three movements change of lineage, so three movements plus change of lineage,
these four are also German movements, impulsions. Now, the mind or Edwardian, and then three
movements following, they take formations as objects, but change of lineage takes nibana as object,
and then thus takes nibana as objects, and then fruition takes nibana as object, so in this
particular thought process, there is a difference of objects.
So here is similarly that illustrates how conformity and change of lineage
occur with different objects, though occurring in a single cognitive series,
that means one thought process with a single Edwardian. Each thought process has one Edwardian
at the beginning of it. Sometimes if the object is through the five senses, then five sense
or Edwardian. If the object is through mind or then there is mind or Edwardian, so each thought process
has one Edwardian, so they did this idea with a single Edwardian, so with this single Edwardian,
then ordinary the object must be the same, but in this particular thought process,
the thought process of enlightenment and there is a difference of object. Some movements take
and formations as objects, and some others take nibana as objects, so in this paragraph,
the other explains this. Suppose a man wanted to leap across a broad stream and establish himself
on the opposite bank. He would run fast, and seizing a rope fast into the branch of a tree
on the streams near bank and hanging down or a pool would leap with his body tending,
inclining and leaning towards the opposite bank, and when he had arrived above the opposite bank,
he would let go, fall on to the opposite bank, staggering first, and then steady himself there.
So do this metadata who wants to establish himself on nibana. The blank opposite to the kinds
of becoming generation, destiny, station, and about runs fast by means of the contemplations of
rice and fall, etc. So that is, he is running fast at the time, and seizing with
conformities, advancing to impermanence, pain, and not self, the rope of materiality
fastens to the branch of his selfhood, and hanging down or one among the poles beginning with
feeling he leads with the first conformally consciousness. So the first conformally consciousness is like
leading without letting go. And with the second he turns, inclines and leans towards nibana,
like the body that was standing, inclining and leaning towards the opposite bank. Then,
being with the third, next to nibana, which is now attainable, like the others arriving above the
opposite bank, now he is on the opposite bank, then he lets go of that formation as objects.
So he no longer takes that, and the formation as object with the seizing of that consciousness,
and with the change of lineage consciousness, he falls on to the unformed nibana, the bank opposite.
But staggering as the mandate for lack of previous repetition, he is not yet properly steady on the
single object, and after that he is studied by third consciousness. So he cross over to the other
bank, and on the other bank, then let go of the rope, or let go of the formations, and then he falls
on the other bank, which is nibana. But the first fall he was not yet steady, and then next moment,
when he had the third consciousness, rather than he was steadily established on the other bank,
or on nibana. And here, conformity is able to disperse the mark of the filaments that
conceives the truth, but it is unable to make nibana its object, it is their difference,
conformity and change of lineage. So conformity can despair the filaments that conceives the truth,
but it is unable to make nibana its object. Change of lineage is only able to make nibana its
object, but it is unable to disperse the mark that conceives the truth, because they have different
functions, and then there are similar fallows. Now we will skip the same way.
And then, it is similarly of the same way. Now in paragraph 12, another similar, and Archer,
it seems, that his target set up at the distance of eight booths about one hundred yards,
and rubbing his face in the cloth and arming himself with an arrow, he still don't know who
will call fragments, a revolving platform. Another method of real call fragments, and when the
target was over the archer, he gave him a sign with the stick, without pausing after the sign,
the archer shot the arrow and hit the target. Now, in the parley, the what one hundred was used,
so it is not one target, one hundred target. So you put say some hundred yards or maybe 200 yards
a week, and then the main is on it, on it, on it, on the corner. We'll call fragments, or revolving
platform, and then the other, the other meant turns and turns the platform, and the archer was blind
food, and then when he is face-to-face with one target, then the man will keep him a sign,
say, maybe something like a bear, and then we'll be there, and he shoots the arrow, and then
arrow gets the target. So there are not one target, but they have a hundred targets. So the
parley what you use here is a luck cut set up, so it is not one, but one hundred. The reason why he
took it to the one, maybe the use of singular number here. Now, in parley, the number is 100,
it is in singular number. So you say, a hundred men, when you use parley, you use singular number,
not plural number, 100 men, 100 men, I don't know how to say in English, it does not know,
it could be written in English. So the word use is luck cut set down in singular number. Although
the grammatical number is singular, the meaning is plural, say, many, 100. So in parley grammar,
they are such peculiarities, a bond, and so we have to understand that too. So 100 targets set up
at the distance of eight use of buttons, so on. Now, the first fruition, let's go to the first
fruition. So after the change of lineage, the fat consciousness arises. Immediately next to that
knowledge, however, they arise either two or three, or not there. I think this could be the
one from Maga. And we are both in other terms, remember the change of lineage knowledge, the fat
follows upon it, yeah, the fat follows. So immediately after a change of lineage, the fat consciousness
arises. And then immediately after the fat consciousness, they arise either two or three
fruition consciousnesses, which are its result. For it is going to this verify that through
promoting profitable consciousness results, immediately that it is set and which he called the
concentration with immediate result, and sluggedly he reaches what is immediate result for the
destruction of the canvas and so on. Now, the fat consciousness is immediately followed by
how many fruition consciousnesses, two or three. Fat consciousness is a cause and fruition consciousness
is the result. So here, the result follows the cause immediately that there is no endeavor between
cause and effect in this particular case. Now, in the description of Dhamma, there is a word called
what? A-kali-ko. And that word was also misunderstood by many persons. A-kali-ko means not
Gala means time. A-kali means having. So having no time or whatever. So that is translated as
timeless or something. But the real meaning or the real interpretation of that word is
not that it is timeless, but it gives immediate results. That is the correct meaning.
That means no interval time between cause and its result.
So because of that, it is that in the red lasso that which he called the concentration with immediate
results, something like that. Somehow I was saying that there are one, two, three, four or five
fruition consciousnesses. So there is a different sort of opinion amount here or whatever it is also.
So the common opinion is there are two movements or three movements of fruition consciousness.
But there are some who say there are only one fruition consciousness or two or three or four or
five fruition consciousnesses. That is in admissible. For change of this knowledge arises at the
end of conformity's repetition. So at the minimum, there must be two conformity consciousness.
In order for it to be repetition, there must be at least two, right? Not one. If it is only one,
we cannot call it repetition. So repetition means at least two. So there must be two
conformity. So at the minimum, there must be two conformity consciousness. Since one alone does not
act as a forbidden condition and the single series of impressions has a maximum of seven consciousness.
When the Johanna arises, it arises seven times, right? Mostly seven times. But at the moment of death,
only five times, or when someone is fainted five times. But under normal conditions,
Johanna's must be how many? Seven. Okay. Consequently, death series which has two conformities
and change of lineage as third and path consciousness as fourth has three fruition consciousnesses.
So three conformity movements, one change of lineage movement. Now we get full.
Five consciousness, five. So how many remain? Two. So in that case, there must be two
fruition consciousnesses. Sometimes there is only two conformity consciousnesses instead of three.
For those who have quick intelligence, there are only two conformity moments. So when there are
two conformity movements, that that is change of lineage, the fourth is Maga. And so there must be
three fruition consciousnesses. So there are either two or three fruition consciousnesses
following immediately following the path consciousnesses. They are going to know
full fruition consciousness, or five fruition consciousness, or one fruition consciousnesses.
