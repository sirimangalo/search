1
00:00:00,000 --> 00:00:24,160
Good evening, everyone, broadcasting live April 27th, 2016, tonight's quote from the

2
00:00:24,160 --> 00:00:39,280
person, Yupta Nikaya, who is, again, not to put a himself. This is a man named JITA. JITA

3
00:00:39,280 --> 00:00:53,520
was a rather remarkable person, not a monk. He was a rich man, and he was

4
00:00:53,520 --> 00:00:58,760
declared by the Buddha to be preeminent among laymen who have preached the

5
00:00:58,760 --> 00:01:10,600
dawn, the dhamma. And so there's a whole section of the JITA Sanyuta is a record of

6
00:01:10,600 --> 00:01:22,800
conversations between him and monks. So he actually cleared up some debates

7
00:01:22,800 --> 00:01:31,000
between the monks, and that's what this quote doesn't actually show. So the

8
00:01:31,000 --> 00:01:42,120
quote is giving an example, and it sounds like he's repeating something that

9
00:01:42,120 --> 00:01:48,080
the monks already know, but the truth is that the monks were divided, these

10
00:01:48,080 --> 00:01:56,040
elder monks, in fact. And the question is, actually an interesting question for

11
00:01:56,040 --> 00:02:02,960
meditators. Question is whether, let me read the Pauli.

12
00:02:02,960 --> 00:02:16,740
Sanyo Janantiwa, I will show some, Sanyo Janiya dhammatiwa. Sanyo Janiya, the

13
00:02:16,740 --> 00:02:31,860
fetter, or a bond, or a bond, or a bind, and the dhammas which are bound, the

14
00:02:31,860 --> 00:02:42,860
dhammas which are joined, conjoined. Are these two things, are these two

15
00:02:42,860 --> 00:02:56,940
concepts? You may dhamma nana nana nana nana, vu dahu, aikatab, yanjana miyu, aikatab,

16
00:02:56,940 --> 00:03:09,100
yanjana miyu, nana nanti. Are they one in, are they separate and different

17
00:03:09,100 --> 00:03:22,580
meaning, and in a letter, in name, or are they one in meaning, and different in

18
00:03:22,580 --> 00:03:34,060
letter, or by name? So the meaning is you've got the bond, the fetter, the

19
00:03:34,060 --> 00:03:39,740
that which binds things, and then you've got the things that are bound

20
00:03:39,740 --> 00:03:44,580
together. And the question is, it's quite a deep question, actually, the

21
00:03:44,580 --> 00:03:48,340
question is whether these are one and the same and just different in name. And

22
00:03:48,340 --> 00:03:53,060
some of the elders thought that the things that were bound together, the things

23
00:03:53,060 --> 00:03:58,220
that are bound together are actually the bind, and some thought they were

24
00:03:58,220 --> 00:04:05,980
separate. And then Jita heard about this, and so he went to see the monks, he said,

25
00:04:05,980 --> 00:04:17,380
sutamitamanday, I've heard that many monks are arguing about this, or split on

26
00:04:17,380 --> 00:04:28,980
this matter. And he says, and they say, a one gahapati, yes, householder, it is so.

27
00:04:28,980 --> 00:04:37,580
And Jita says that they're separate. He says, nana tah ji, one nana miyanjana,

28
00:04:37,580 --> 00:04:43,900
they're different in letter and different in meaning. And then he gives this

29
00:04:43,900 --> 00:04:53,900
simile, a black cow and a white cow, and they're tied together in a rope by a rope.

30
00:04:53,900 --> 00:05:02,100
And wherever the white, when the white cow pulls the black cow has to go as well.

31
00:05:02,100 --> 00:05:06,900
So the question is whether the black cow is fetter is holding the white cow or

32
00:05:06,900 --> 00:05:10,820
the white cow is holding the black cow. And the monks say, well, no, the rope is

33
00:05:10,820 --> 00:05:20,220
holding them both. And they says in the same, just the same, and the eye is not a fetter,

34
00:05:20,220 --> 00:05:30,740
light visions are not a fetter for the eye, nor is the eye a fetter for visions. So it's

35
00:05:30,740 --> 00:05:38,380
not because we see things that just seeing things is not a fetter. It's not getting caught

36
00:05:38,380 --> 00:05:44,380
up. You don't get attached just because you've seen, basically. I mean, this is very deep

37
00:05:44,380 --> 00:05:49,420
and sort of, these are the kind of things that they would debate. And it goes to show

38
00:05:49,420 --> 00:05:57,780
how deep was their thought process, what they were thinking about. And here when you hear

39
00:05:57,780 --> 00:06:07,140
something, it's not the sound that is the bond, the clinging isn't in the sound. When

40
00:06:07,140 --> 00:06:14,820
you smell, taste, feel, think, none of these are the problem. Very simply, it's basically

41
00:06:14,820 --> 00:06:20,500
what we talk about in meditation. Just because you experience something, this isn't

42
00:06:20,500 --> 00:06:26,100
the problem. And when I was giving five minute meditation lessons is how I started off,

43
00:06:26,100 --> 00:06:34,300
I said, this meditation is based on the premise that it's not our experiences that

44
00:06:34,300 --> 00:06:41,340
cause us suffering. It's our reactions to them. So it's the craving. He says,

45
00:06:41,340 --> 00:07:11,180
That's what binds them. That's what binds them.

46
00:07:11,180 --> 00:07:27,060
So that's the bind in this case. So even when you feel pain, it's not the pain, that's

47
00:07:27,060 --> 00:07:30,500
the problem. When someone's yelling at you, it's not the yelling, it's the problem.

48
00:07:30,500 --> 00:07:36,940
When you think bad thoughts, thoughts are not bad. They're just thoughts. And in fact,

49
00:07:36,940 --> 00:07:44,140
neutral. We once had a monk in Thailand. I don't know if I've recently mentioned him,

50
00:07:44,140 --> 00:07:53,140
but I know it was in New York. I was just talking about him. And he told me he was really

51
00:07:53,140 --> 00:08:04,420
crazy. He thought we were all conspiring against him. It was interesting. When he came

52
00:08:04,420 --> 00:08:11,020
up to me and said, they're talking about me over the loudspeakers. They're saying thing.

53
00:08:11,020 --> 00:08:16,620
And I said, well, I really don't think. And he said, oh, you're in with him. He really

54
00:08:16,620 --> 00:08:24,860
thought we were all conspiring to drive him crazy at the monastery. It was quite interesting.

55
00:08:24,860 --> 00:08:28,540
But he wants to send to me. He said, I'm having these thoughts. And I said, well, they're

56
00:08:28,540 --> 00:08:33,820
just thoughts. And he said, oh, no, there's just the most horrible thoughts that you could

57
00:08:33,820 --> 00:08:39,980
possibly have. You know, and the end of thought is a thought. And he drove himself crazy.

58
00:08:39,980 --> 00:08:46,020
He ended up cutting his wrists and lighting himself on fire. It all sorts of crazy things.

59
00:08:46,020 --> 00:08:52,620
Ended up disrobing. But we do this. We all do this to some extent. We beat ourselves

60
00:08:52,620 --> 00:08:57,780
up over our thoughts. Don't think that can't think that. I'm so evil for thinking that.

61
00:08:57,780 --> 00:09:04,100
And actually, you're not. The thoughts are just thought. That's not where evil comes

62
00:09:04,100 --> 00:09:14,940
from. So actually, Jitta was teaching among something here. He was teaching them. And

63
00:09:14,940 --> 00:09:20,180
so they don't say, oh, very good, very good. Like patronizing, they say, wow, la bhati

64
00:09:20,180 --> 00:09:34,940
ghati. It's a great game for you, household. That you have this deep. You have the

65
00:09:34,940 --> 00:09:53,700
eye of wisdom. This is that by you, the eye of wisdom goes to the deep. You have a deep

66
00:09:53,700 --> 00:10:06,660
eye of wisdom in the deep teaching of the Buddhas. Deep words of the Buddhas. Something like

67
00:10:06,660 --> 00:10:18,460
that. So yeah, the bondage. What is it that binds things? We start with ignorance. The

68
00:10:18,460 --> 00:10:25,420
base of all attachment is ignorance. It's just not seen clearly. The ignorance isn't something

69
00:10:25,420 --> 00:10:34,620
hard to understand. It just means you didn't see it clearly enough. And that leads to

70
00:10:34,620 --> 00:10:43,020
misconception. It leads us to like things that are not worth liking and it leads to habits

71
00:10:43,020 --> 00:10:51,380
of preference. Because we're just guessing, you know, when we grow up, we do this even

72
00:10:51,380 --> 00:10:59,660
in this life to some extent. When you ask a kid, which one they want, they, you know, in

73
00:10:59,660 --> 00:11:05,420
many cases, it's all the same. When you ask a meditator, it's hard to ask them, you know,

74
00:11:05,420 --> 00:11:10,500
which do you like this? Or would you like that? And not really able to make a decision

75
00:11:10,500 --> 00:11:17,920
because there's no reason to pick one over the other. But we do. We build up likes and

76
00:11:17,920 --> 00:11:25,860
dislikes often just for, just by chance, you know, just because of that's how the chips fall.

77
00:11:25,860 --> 00:11:33,980
That's where our life leads us. We acquire tastes, you know, some of its genetics and

78
00:11:33,980 --> 00:11:51,100
some of its organic, but some of its just random. And so then the ignorance is a breeding

79
00:11:51,100 --> 00:12:01,540
ground. It's like darkness. Ignorance is a direct parallel to the darkness. It's mental

80
00:12:01,540 --> 00:12:08,980
darkness. So, you know, anything about, if you think about darkness, not only is it impossible

81
00:12:08,980 --> 00:12:15,500
to see what is right, but it's a breeding ground of all sorts of things. It's a breeding

82
00:12:15,500 --> 00:12:24,220
ground of bacteria. It's a breeding ground of viruses and virus. I don't know, bacteria.

83
00:12:24,220 --> 00:12:34,980
You know, rotten things, things rot in the dark, mold grows, fungus grows in the dark. And

84
00:12:34,980 --> 00:12:40,020
so all these rotten things in our minds grow based on our ignorance. And all it takes

85
00:12:40,020 --> 00:12:47,180
is to shine a light in, when you shine a powerful light in darkness disappears. So you don't

86
00:12:47,180 --> 00:12:53,620
have to worry about getting rid of the ignorance. But the bad things start to shrivel

87
00:12:53,620 --> 00:13:04,020
up as well. The rotten things start to dry up and wither away because they rely on darkness

88
00:13:04,020 --> 00:13:13,780
for support. So how do we shine this light in? It's a little more complicated than just

89
00:13:13,780 --> 00:13:19,340
shining a light, a little bit more complicated because there's different aspects. And

90
00:13:19,340 --> 00:13:27,900
I wrote, I did a video of, you know, my video on pornography and pornography and masturbation

91
00:13:27,900 --> 00:13:39,940
and addiction in general. It was that there are, I had for a long time had this concept

92
00:13:39,940 --> 00:13:53,060
of three things, three aspects to an addiction. The object, which is just seeing, hearing,

93
00:13:53,060 --> 00:13:58,860
smelling, tasting, feeling, or thinking, I thought, you know, it can be something you see

94
00:13:58,860 --> 00:14:04,220
that leads to desire, something you hear, something you smell, something you taste, something

95
00:14:04,220 --> 00:14:11,940
you feel, or something you think. So that's the first thing. The second thing is the

96
00:14:11,940 --> 00:14:18,180
pleasure that comes from it. You can do the same with pain as well. So that when something

97
00:14:18,180 --> 00:14:21,900
when you see something and it makes you happy, when you hear something, it makes you happy.

98
00:14:21,900 --> 00:14:27,860
So there's that aspect of it. And then the third is the desire, which is not the feeling,

99
00:14:27,860 --> 00:14:40,180
but it's this attraction, like a magnet, a clinging, like a pressure in the mind and

100
00:14:40,180 --> 00:14:46,820
the stickiness. And going back and forth between those three, this is what I talked about

101
00:14:46,820 --> 00:14:51,820
in this video. And I found this in the Buddha's teaching. The Buddha said, some teachers

102
00:14:51,820 --> 00:15:00,780
teach one or the other, but the best teacher teaches all three. They just, the base teaches

103
00:15:00,780 --> 00:15:09,220
the feeling and teaches the attachment. So this is how we deal with addiction specifically.

104
00:15:09,220 --> 00:15:18,060
If you want to deal with your attachments, go back and forth between these three. And every

105
00:15:18,060 --> 00:15:23,340
time it comes out, just be methodical, systematic. And you start to change those habits.

106
00:15:23,340 --> 00:15:30,060
You'll start to override this thing. It's because you'll see that, oh, yeah, well, it really

107
00:15:30,060 --> 00:15:34,900
isn't anything desirable about that at all. Eventually, once your wisdom gets stronger,

108
00:15:34,900 --> 00:15:40,380
you just don't have any desire for the things you used to desire. And look at it, and

109
00:15:40,380 --> 00:15:54,380
you rightly see that it's not worth desiring. So that's the demo for tonight. Do we have

110
00:15:54,380 --> 00:16:08,900
any questions? You guys can go ahead. So, is there anything you can say about mindfulness

111
00:16:08,900 --> 00:16:19,300
or falling asleep? Similar experience to die, but find it very difficult. Well, you can

112
00:16:19,300 --> 00:16:25,180
try. You know, you try to be mindful up until the moment you fall asleep. If you're really

113
00:16:25,180 --> 00:16:35,060
good, you can know whether you fall asleep on the rising or the falling. But you know,

114
00:16:35,060 --> 00:16:41,660
change is be mindful until you fall asleep. That's not something you should worry about

115
00:16:41,660 --> 00:16:47,020
or strive for. You just work at it. When you lie down at night, you know, try to take

116
00:16:47,020 --> 00:16:52,900
it as lying meditation. Don't just fall asleep. But it works better when you're on a meditation

117
00:16:52,900 --> 00:17:13,740
course. In life, of course, it's very difficult to be that mindful.

118
00:17:13,740 --> 00:17:40,620
It's very difficult to be able to be able to be able to be able to be able to be able

119
00:17:40,620 --> 00:17:58,100
to be able to be able to be able to be able to be able to be able to be able to have

120
00:17:58,100 --> 00:18:01,780
I think there's a bit of a lag between what I say and what comes up in the chat.

121
00:18:04,340 --> 00:18:09,780
Anyway, we have questions. I'll come to post in tomorrow. Should be back.

122
00:18:09,780 --> 00:18:19,780
So have a good night, everyone.

