1
00:00:00,000 --> 00:00:07,000
In the context of my daily meditation practice, I find the concept of repression of emotions, hard to grasp.

2
00:00:07,000 --> 00:00:14,000
Do you think meditation inhibits people from being able to repress emotions or memories?

3
00:00:14,000 --> 00:00:21,000
Yeah, repression is a bit of a difficult concept. It's not really...

4
00:00:21,000 --> 00:00:26,000
It's not dealt with the same in Buddhism as it is in Western psychology.

5
00:00:26,000 --> 00:00:31,000
Although I'm not totally up on Western psychology, especially modern Western psychology.

6
00:00:31,000 --> 00:00:36,000
But the classical view of repression is that it's bad, and it's a negative thing.

7
00:00:36,000 --> 00:00:41,000
It's something that is the cause of all of our problems.

8
00:00:41,000 --> 00:00:47,000
So digging up is considered to be a very important thing to come to terms with these things.

9
00:00:47,000 --> 00:00:57,000
There is some overlap, there are some parallel in Buddhism, but it's not exact.

10
00:00:57,000 --> 00:01:06,000
Momentary repression of emotions is actually technically what we're doing in the meditation practice.

11
00:01:06,000 --> 00:01:11,000
When you practice loving kindness meditation, for example, you're repressing the anger.

12
00:01:11,000 --> 00:01:20,000
You're actually through the force of loving kindness, or you might say suppressing it.

13
00:01:20,000 --> 00:01:28,000
Through the practice of mindfulness, when you say pain, pain or so on, the anger is being suppressed.

14
00:01:28,000 --> 00:01:40,000
So this is dealt with, it's understood more as being suppression, or getting in the way of it.

15
00:01:40,000 --> 00:01:49,000
Getting in the stopping from a rising, because from an abidamic point of view, there is no emotion to suppress,

16
00:01:49,000 --> 00:01:52,000
and there is no suppressor, and there are only momentum of the mind state.

17
00:01:52,000 --> 00:01:57,000
So technically, when you repress something, all that's happening is you're creating a specific mind state.

18
00:01:57,000 --> 00:02:03,000
And so if you watch during meditation, you see that you're not actually repressing it.

19
00:02:03,000 --> 00:02:06,000
You're not actually pushing it down.

20
00:02:06,000 --> 00:02:14,000
You're giving rise to moment to moment mind states, and repression generally has to do with anger, the aversion towards the mind state.

21
00:02:14,000 --> 00:02:16,000
Something arises, you get angry about it.

22
00:02:16,000 --> 00:02:19,000
Now that doesn't repress it, it's already gone actually.

23
00:02:19,000 --> 00:02:24,000
The emotion, whatever it was, that you repressed, was actually not repressed.

24
00:02:24,000 --> 00:02:30,000
It was reacted to with anger, with aversion.

25
00:02:30,000 --> 00:02:32,000
Like, no, no, no.

26
00:02:32,000 --> 00:02:35,000
Don't let it come up.

27
00:02:35,000 --> 00:02:43,000
Or thinking that somehow you're going to catch it, something that's already arisen.

28
00:02:43,000 --> 00:02:47,000
Thinking that it's somehow a problem when it's already passed.

29
00:02:47,000 --> 00:02:53,000
As a result of that, what does happen, and this is what people see as being the repression of emotions,

30
00:02:53,000 --> 00:02:55,000
is it creates a habit.

31
00:02:55,000 --> 00:02:58,000
And so every time the emotion comes up, rather than dealing with it, you get angry.

32
00:02:58,000 --> 00:03:00,000
And you get go off in this anger.

33
00:03:00,000 --> 00:03:02,000
It cuts off the emotion.

34
00:03:02,000 --> 00:03:04,000
Suppose it was lust, let's say.

35
00:03:04,000 --> 00:03:05,000
So you have lust arising.

36
00:03:05,000 --> 00:03:10,000
You see something, you see a beautiful woman, or a beautiful man, and you have lust arise.

37
00:03:10,000 --> 00:03:12,000
And then you get angry at yourself.

38
00:03:12,000 --> 00:03:19,000
You feel guilty, you see how horrible what a sin, what a negative mind state, and you get angry.

39
00:03:19,000 --> 00:03:24,000
So the lust has no potential to continue for the moment.

40
00:03:24,000 --> 00:03:32,000
But you have a bigger problem now, because you have this feeling of guilt and anger and self-hatred and so on.

41
00:03:32,000 --> 00:03:34,000
And that's what happens every time.

42
00:03:34,000 --> 00:03:36,000
So what it looks like is you're repressing it.

43
00:03:36,000 --> 00:03:43,000
But all it is is you're actually changing the focus of the mind.

44
00:03:43,000 --> 00:03:45,000
You're complicating it.

45
00:03:45,000 --> 00:03:50,000
If you want to say rather than suppress it, repressing it, you're complicating the emotion.

46
00:03:50,000 --> 00:03:55,000
The last and the first place is a habit that we've developed over time.

47
00:03:55,000 --> 00:03:59,000
There's no reason to have attraction to the human body.

48
00:03:59,000 --> 00:04:05,000
It's science can explain it away based on genes and hormones and so on.

49
00:04:05,000 --> 00:04:12,000
But in the broader point of view, from the point of view of the or from a Buddhist belief point of view,

50
00:04:12,000 --> 00:04:17,000
the main say, but looking at things experientially in the moment to moment,

51
00:04:17,000 --> 00:04:23,000
mind states, giving up the idea of birth and death and so on.

52
00:04:23,000 --> 00:04:28,000
There's no reason why we should come to this state of finding the human body pleasant.

53
00:04:28,000 --> 00:04:36,000
It's a contrived state, the human body in genes and hormones are all part of this contrived state that we've built up.

54
00:04:36,000 --> 00:04:45,000
We've cultivated as a part of what scientists call evolution.

55
00:04:45,000 --> 00:04:49,000
That's how the habit was originally.

56
00:04:49,000 --> 00:04:52,000
Now modern human society has complicated it.

57
00:04:52,000 --> 00:04:57,000
We find our habits of natural habits, even of guilt.

58
00:04:57,000 --> 00:05:00,000
We naturally feel guilty.

59
00:05:00,000 --> 00:05:08,000
And in fact, you might even think it curious how the Bible legend in Genesis, how Adam and Eve, right?

60
00:05:08,000 --> 00:05:12,000
Why they were fig leaves is because of the knowledge that they gained.

61
00:05:12,000 --> 00:05:17,000
So somehow they gained this, what the Bible sees as being knowledge,

62
00:05:17,000 --> 00:05:25,000
but we would say as being even more delusion, that leads them to feel guilty about it,

63
00:05:25,000 --> 00:05:29,000
to feel guilty about their nudity.

64
00:05:29,000 --> 00:05:32,000
Which complicates things.

65
00:05:32,000 --> 00:05:38,000
And so you might say that this is something that we've cultivated as a race.

66
00:05:38,000 --> 00:05:43,000
So people have innately in them this feeling of guilt towards sexuality.

67
00:05:43,000 --> 00:05:45,000
This is a theory, of course.

68
00:05:45,000 --> 00:05:46,000
Something like that.

69
00:05:46,000 --> 00:05:49,000
But anyway, these habits come up and we're complicating them.

70
00:05:49,000 --> 00:05:51,000
This is how repression works.

71
00:05:51,000 --> 00:05:54,000
This is how what we call repression works.

72
00:05:54,000 --> 00:05:57,000
It's just a more complicated habit.

73
00:05:57,000 --> 00:06:00,000
And so this is what we're dealing with in meditation.

74
00:06:00,000 --> 00:06:05,000
In many cases, incredibly complicated habits in mind states.

75
00:06:05,000 --> 00:06:13,000
You can't simply deal with the lust or even the hatred that we have in the mind.

76
00:06:13,000 --> 00:06:16,000
You have to look at how we're reacting to that.

77
00:06:16,000 --> 00:06:22,000
Because our habits, our minds are so complex, our habits, our habitual tendencies,

78
00:06:22,000 --> 00:06:26,000
are so complex that we get angry.

79
00:06:26,000 --> 00:06:30,000
And then we will be angry about it or we will be afraid of it.

80
00:06:30,000 --> 00:06:36,000
And then we will try to divert ourselves by finding something pleasurable.

81
00:06:36,000 --> 00:06:44,000
And on and on and on, we have these tricks and these methods that we've developed.

82
00:06:44,000 --> 00:06:50,000
So haphazardly that we have to muck through in the meditation practice.

83
00:06:50,000 --> 00:06:59,000
So repression is only a part of that and it's really an inexact explanation.

84
00:06:59,000 --> 00:07:03,000
I don't use the word repression so much when I talk to people.

85
00:07:03,000 --> 00:07:05,000
I think because for this reason, I think it's inexact.

86
00:07:05,000 --> 00:07:08,000
What we should talk about is the overlapping tendencies.

87
00:07:08,000 --> 00:07:13,000
So you have underneath you have lust or ordinary lust or angry.

88
00:07:13,000 --> 00:07:16,000
And then you have your responses to it.

89
00:07:16,000 --> 00:07:21,000
And so you have to be able to skillfully deal with both and unravel it together.

90
00:07:21,000 --> 00:07:25,000
Not just dealing with the lust or what we would call the repression.

91
00:07:25,000 --> 00:07:28,000
But dealing with them both together from time to time, you'll be the lust.

92
00:07:28,000 --> 00:07:32,000
From time to time, you'll be the guilt, the anger, the fear, the worry.

93
00:07:32,000 --> 00:07:35,000
No, I'm going to go to hell for this or whatever.

94
00:07:35,000 --> 00:07:38,000
All of the different mindsets.

95
00:07:38,000 --> 00:07:41,000
And of course, even dealing with a positive mind state.

96
00:07:41,000 --> 00:07:45,000
It's being mindful of everything piece by piece.

97
00:07:45,000 --> 00:07:50,000
And this is a very important topic to understand from a meditative point of view.

98
00:07:50,000 --> 00:07:55,000
To understand that it's not as simple as something like even repression.

99
00:07:55,000 --> 00:07:57,000
It's much more complicated.

100
00:07:57,000 --> 00:08:02,000
And rather than trying to say, I am repressing or I have this,

101
00:08:02,000 --> 00:08:05,000
this is my tendency.

102
00:08:05,000 --> 00:08:07,000
We should look at the tendencies for a moment to moment.

103
00:08:07,000 --> 00:08:12,000
And meditation in one sense is simply getting more and more skillful

104
00:08:12,000 --> 00:08:22,000
at dealing with more and more types of convolutions and knots in the mind.

105
00:08:22,000 --> 00:08:26,000
This is why the Buddha said, the mind is untying the knot.

106
00:08:26,000 --> 00:08:31,000
There was this angel came and said, how it started to be Sunni maga the first verse.

107
00:08:31,000 --> 00:08:50,000
This people today are entangled with a tango, both the inner tango and the outer tango.

108
00:08:50,000 --> 00:08:51,000
Who in this world?

109
00:08:51,000 --> 00:08:56,000
We ask the samanagotama, who in this world can untie the tango?

110
00:08:56,000 --> 00:08:58,000
Untangle the tango.

111
00:08:58,000 --> 00:09:01,000
So the inner tango is what we've been talking about.

112
00:09:01,000 --> 00:09:05,000
The outer tango is actually equally important our relationships with other people.

113
00:09:05,000 --> 00:09:08,000
How we are attached to other people.

114
00:09:08,000 --> 00:09:10,000
And of course, they are also complicated.

115
00:09:10,000 --> 00:09:12,000
These complicated relationships.

116
00:09:12,000 --> 00:09:15,000
We are a complicated relationship with our parents.

117
00:09:15,000 --> 00:09:20,000
Our complicated relationships with our spouses, our children, with everyone.

118
00:09:20,000 --> 00:09:25,000
People who we work with, even going to public school,

119
00:09:25,000 --> 00:09:29,000
going to high school with such a nightmare because you have these cliques

120
00:09:29,000 --> 00:09:33,000
and you have just horrible complexities.

121
00:09:33,000 --> 00:09:34,000
The tango.

122
00:09:34,000 --> 00:09:36,000
How do you untie the tango?

123
00:09:36,000 --> 00:09:40,000
The Buddha said, see, lay, patita, and the rows up.

124
00:09:40,000 --> 00:09:49,000
See, lay, patita, resting on morality.

125
00:09:49,000 --> 00:09:57,000
Chitang, developing concentration, banyanja, and moral and wisdom.

126
00:09:57,000 --> 00:10:02,000
Atapi, nipako, a wise and energetic, be cool.

127
00:10:02,000 --> 00:10:05,000
We'll be able to untangle the tango.

128
00:10:05,000 --> 00:10:08,000
So basically speaking, morality, concentration, and wisdom,

129
00:10:08,000 --> 00:10:11,000
or the practice of the Buddha's teaching.

130
00:10:11,000 --> 00:10:16,000
Which of course, I don't think I have to go into what I mean by morality,

131
00:10:16,000 --> 00:10:18,000
concentration, and wisdom.

132
00:10:18,000 --> 00:10:25,000
And of course, study, and I'm sure you're already fairly alert as to what these mean.

133
00:10:25,000 --> 00:10:29,000
From a meditative point of view, just briefly, I suppose.

134
00:10:29,000 --> 00:10:32,000
The morality is the bringing the mind back to the object.

135
00:10:32,000 --> 00:10:37,000
The concentration is the focus that arises when you do that.

136
00:10:37,000 --> 00:10:40,000
And the wisdom is what you see when your mind is in focus.

137
00:10:40,000 --> 00:10:46,000
So the meditation practice is the development of morality, concentration, and wisdom.

138
00:10:46,000 --> 00:10:51,000
That slowly allows you to simplify your habits,

139
00:10:51,000 --> 00:10:55,000
make your mind work in a one-to-one relationship instead of reacting,

140
00:10:55,000 --> 00:11:19,000
to your reactions, and so on.

