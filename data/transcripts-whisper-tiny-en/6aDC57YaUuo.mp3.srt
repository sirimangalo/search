1
00:00:00,000 --> 00:00:05,840
The Buddhists see life as a kind of mistake that should sort itself out by obtaining

2
00:00:05,840 --> 00:00:07,480
nibana.

3
00:00:07,480 --> 00:00:14,280
If there was a way for life to avoid somehow suffering, or to somehow avoid suffering, would

4
00:00:14,280 --> 00:00:18,720
it be okay for it to continue in the physical realm?

5
00:00:18,720 --> 00:00:34,960
Well, if there is no suffering, then there wouldn't be any point, I guess, but you're

6
00:00:34,960 --> 00:00:38,520
bound to have suffering in the physical realm.

7
00:00:38,520 --> 00:00:44,120
Just because that's just the way it is.

8
00:00:44,120 --> 00:00:49,280
Your body is uncomfortable, having to deal with things they don't like is uncomfortable.

9
00:00:49,280 --> 00:00:56,160
You're bound to experience pain emotionally, mentally, physically.

10
00:00:56,160 --> 00:01:06,360
So this just seems like the loss of fighting about things.

11
00:01:06,360 --> 00:01:11,240
It doesn't really seem to matter if there wasn't suffering in the physical realm, because

12
00:01:11,240 --> 00:01:17,840
there is and there has to be, there's no way that it couldn't be.

13
00:01:17,840 --> 00:01:29,360
If I can get a little bit technical about the question, mistake kind of implies that at

14
00:01:29,360 --> 00:01:33,640
one point, this idea that maybe one point in the past, we were in nibana and then we

15
00:01:33,640 --> 00:01:48,760
oops, we somehow fell out of it, you know, reality or existence is, it exists, no.

16
00:01:48,760 --> 00:01:55,360
And it always has, it is called existence and in an ultimate sense, that's really all

17
00:01:55,360 --> 00:01:58,320
you can say about it is, it exists.

18
00:01:58,320 --> 00:02:05,800
A few more things you can say, existence, the characteristic of existence is to arrive.

19
00:02:05,800 --> 00:02:11,280
Anything that comes into existence, and maybe that's not quite correct, the word existence

20
00:02:11,280 --> 00:02:17,200
isn't quite correct, but experience, no, let's put it this way, because this is what we

21
00:02:17,200 --> 00:02:20,080
always say, reality is experience.

22
00:02:20,080 --> 00:02:30,080
So experience is something that has been, or that exists, and experience, that which is

23
00:02:30,080 --> 00:02:36,320
experience arises, so there is the arising, and that which arises is always also of the nature

24
00:02:36,320 --> 00:02:38,040
disease.

25
00:02:38,040 --> 00:02:40,320
This is actually what we're trying to realize in Buddhism.

26
00:02:40,320 --> 00:02:46,360
If you realize just that fact, you become free from suffering, that, or you enter at least

27
00:02:46,360 --> 00:02:52,480
and become a Sotopan, if you realize that everything young kimchi, samudaya, dhammang, sambantan,

28
00:02:52,480 --> 00:02:53,480
niro, dhammang.

29
00:02:53,480 --> 00:02:57,760
If you realize that, that's the entry into Sotopan, but it doesn't just mean you think

30
00:02:57,760 --> 00:03:02,960
about it philosophically, it means you actually experience the cessation of everything.

31
00:03:02,960 --> 00:03:07,280
So you realize that there's nothing that lasts.

32
00:03:07,280 --> 00:03:11,960
We've never realized that, it's not something that is a part of experience, it's not something

33
00:03:11,960 --> 00:03:20,760
that is, it's not a part of the loop, so when you get caught up in the loop, you'll never

34
00:03:20,760 --> 00:03:23,840
be able to see this arising in sisu.

35
00:03:23,840 --> 00:03:33,960
Because the loop is that of experiencing, clinging, acting, and then experiencing, clinging,

36
00:03:33,960 --> 00:03:34,960
and acting.

37
00:03:34,960 --> 00:03:43,160
The what starts with the kiles, the reacting, then there's the kama, the acting, and then

38
00:03:43,160 --> 00:03:49,600
there's the vipaka, which is the experiencing, the result, and then when you experience

39
00:03:49,600 --> 00:03:51,560
the result, you react and so on.

40
00:03:51,560 --> 00:03:56,880
Because we're doing this, we never stop to actually look at what's making up the experience,

41
00:03:56,880 --> 00:03:59,720
we're constantly reacting.

42
00:03:59,720 --> 00:04:09,240
Because of that, we've always been in some sorry, it's only a person who is able to stop

43
00:04:09,240 --> 00:04:16,520
the ferris wheel or the merry-go-round, and actually begin to look at what they're doing,

44
00:04:16,520 --> 00:04:21,520
look at what's going on, is able to free themselves.

45
00:04:21,520 --> 00:04:31,040
So it's not a mistake, it's an experience, it's the state of experiencing things.

46
00:04:31,040 --> 00:04:37,840
Now Buddhism isn't a religion of complaining and saying, you know, there's something

47
00:04:37,840 --> 00:04:45,960
wrong here, you have to do something, it's pointing out that this can never be stable,

48
00:04:45,960 --> 00:04:58,440
it can never be satisfied, it has the nature to, or it's not static, so it has the

49
00:04:58,440 --> 00:05:06,400
nature to lead in one direction or the other, if a person does good things, they become

50
00:05:06,400 --> 00:05:13,560
a good person and become happy, and then if in when they become negligent, they fall

51
00:05:13,560 --> 00:05:18,640
down and become unhappy, they do bad things and become unhappy.

52
00:05:18,640 --> 00:05:25,640
So this is why it's, the reason why it's never possible to avoid suffering is because

53
00:05:25,640 --> 00:05:34,440
it's not static, so even if you're able to avoid suffering for a billion billion billion

54
00:05:34,440 --> 00:05:41,040
billion years, or a Google years, Googleplex years, let's say, or from the time of a big

55
00:05:41,040 --> 00:05:47,680
bang to a big crunch, or a cold death, or whatever, if you were able, this, this, whatever

56
00:05:47,680 --> 00:05:53,560
they conjecture, even that isn't freedom from suffering because it's still impermanent.

57
00:05:53,560 --> 00:05:59,360
It's still not static, as long as there is the mind creating more karma, you know, based

58
00:05:59,360 --> 00:06:04,760
on it's kile, so it's desires, it's partiality, there will be more karma, when there's

59
00:06:04,760 --> 00:06:08,160
more karma, there will be more results and there will be a change.

60
00:06:08,160 --> 00:06:17,960
The Buddha said, Manopobangamadam reality experience comes from the mind, so the only way

61
00:06:17,960 --> 00:06:21,800
you could have something static is to get rid of the mind, if you get rid of the mind you

62
00:06:21,800 --> 00:06:28,360
could have a robot that was perhaps always happy, but there would be, well, yeah, the robot

63
00:06:28,360 --> 00:06:32,600
would be fine, you wouldn't, you wouldn't say the robot would be unhappy, this computer

64
00:06:32,600 --> 00:06:38,120
is never unhappy, for example, microphone is never unhappy, it's got no problem,

65
00:06:38,120 --> 00:06:46,480
because there's no mind, but the, the mind that experience is has a problem, because it's

66
00:06:46,480 --> 00:06:52,480
clinging and always creating, it's going around in the circle, Buddhism teaches us to,

67
00:06:52,480 --> 00:06:59,440
to stop that and to create a mind or to develop a experience of reality that doesn't

68
00:06:59,440 --> 00:07:06,480
cling and that doesn't create, because it doesn't cling and doesn't create, it, it

69
00:07:06,480 --> 00:07:16,480
becomes stable, it becomes satisfying, it becomes permanent, it becomes constant, so there

70
00:07:16,480 --> 00:07:21,480
is, there is never any suffering and the experiences that have been created in the past

71
00:07:21,480 --> 00:07:27,760
begin to fade away and in the end there is only this basic experience of seeing, hearing,

72
00:07:27,760 --> 00:07:33,360
smelling, tasting, feeling, thinking which also disappears, and so the person who enters

73
00:07:33,360 --> 00:07:40,800
into Nibbana does so because they've entered into a state of non-clinging of non-creating

74
00:07:40,800 --> 00:07:44,520
and if you don't enter into that state you're always going to be creating, you're always

75
00:07:44,520 --> 00:07:52,240
going to be changing things, you have to remember that Buddhism hasn't, hasn't avoided

76
00:07:52,240 --> 00:08:01,520
this, the Buddha didn't, it didn't slip by him this idea of creating a stable and lasting

77
00:08:01,520 --> 00:08:07,880
reality in Samsara, the Buddha actually remembered these kind of states and he talked about

78
00:08:07,880 --> 00:08:12,520
these kind of states, the state of a Brahma for example, so these people talking about

79
00:08:12,520 --> 00:08:19,520
maybe what if we put the human mind into a machine or we altered the brain in such a way,

80
00:08:19,520 --> 00:08:24,440
fixed it up so that it never got old and so that it was constantly in some kind of pleasure,

81
00:08:24,440 --> 00:08:28,920
this kind of pleasurable state and they say well there, there you go, there's something

82
00:08:28,920 --> 00:08:34,960
that's transhumanism that you read about on our form, someone was asking this, but that's

83
00:08:34,960 --> 00:08:40,720
pitiful in comparison with the age of a Brahma, a God that the Buddha was talking about

84
00:08:40,720 --> 00:08:45,040
and remembered and explained how to get to, you don't need robots to do it, just leave

85
00:08:45,040 --> 00:08:52,320
the body behind, develop the mind to the extent that it leaves behind the body and it

86
00:08:52,320 --> 00:09:00,240
can do that for an entire kapa, an entire age, even longer I think, or certain Brahma

87
00:09:00,240 --> 00:09:05,360
realms that are above the Big Bang, so even the Big Bang doesn't affect them, this

88
00:09:05,360 --> 00:09:11,600
or the Big Crunch or whatever, the changing of the kapa doesn't affect them and then

89
00:09:11,600 --> 00:09:19,480
there's maybe, I don't know, cosmology is, it's quite detailed actually, so this robot

90
00:09:19,480 --> 00:09:24,680
is certainly not going to last for that long, but even a Brahma does and even a Brahma

91
00:09:24,680 --> 00:09:28,760
is not free from suffering, you never can be, that's not going to say no to say by very

92
00:09:28,760 --> 00:09:38,920
nature, experience is unsatisfying and uncontrollable, it's not static, it's not static

93
00:09:38,920 --> 00:09:44,940
because there is the intentions, this is why the Buddha said the Kilesar, the problem,

94
00:09:44,940 --> 00:09:50,360
the filaments in our mind, because we want something, because we are not satisfied with

95
00:09:50,360 --> 00:09:56,200
things as they are, we make some attempt to change things, the only way to become satisfied

96
00:09:56,200 --> 00:10:03,240
is to give up our desire for something that doesn't exist, to give up our desire for any

97
00:10:03,240 --> 00:10:08,560
impermanent thing, and once we give up that desire, the mind naturally inclines towards

98
00:10:08,560 --> 00:10:14,640
the Bhana and not towards the arising, how could it, it wouldn't ever give rise to the

99
00:10:14,640 --> 00:10:19,720
intention to create anything at all, because it doesn't give rise to the intention to create,

100
00:10:19,720 --> 00:10:27,720
it can't help, but the things that are created will not increase, there's no feeding

101
00:10:27,720 --> 00:10:53,280
of the fires of the fire eventually goes out, that's the answer.

