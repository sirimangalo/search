1
00:00:00,000 --> 00:00:09,000
It seems in all other human endeavors such as science, music, art, there have been technical evolution over time.

2
00:00:09,000 --> 00:00:13,000
Has the same thing occurred in meditation techniques?

3
00:00:15,000 --> 00:00:19,000
Yes, certainly.

4
00:00:19,000 --> 00:00:29,000
Meditation techniques have been modified all the time.

5
00:00:30,000 --> 00:00:48,000
The Buddha gives meditations, meditation instructions in the sutas, and there are quite clear, but the understanding of them varies widely.

6
00:00:48,000 --> 00:01:07,000
So, many people are very sure about what the Buddha taught and that they are those who follow that teaching, and another group claims just the same, and practice very differently.

7
00:01:07,000 --> 00:01:22,000
So, you can certainly assume that meditation techniques have been modified very much.

8
00:01:22,000 --> 00:01:41,000
From an orthodox point of view, the orthodox, or the accepted explanation of this is that there are different types of people.

9
00:01:41,000 --> 00:01:47,000
Some people you don't even have to give them a teaching. You just tell them what sort of teaching you teach.

10
00:01:47,000 --> 00:01:57,000
So, instead of the Satipatana sutta, instead of even teaching the Satipatana sutta, you would say, there are these four Satipatanas, these four foundations of mindfulness.

11
00:01:57,000 --> 00:02:03,000
You might not even have to, the amazing thing is you might not even have to explain what are the four foundations of mindfulness.

12
00:02:03,000 --> 00:02:06,000
Some people just get it, just like that.

13
00:02:06,000 --> 00:02:21,000
Yankinji Samud, no, what was it? I am sorry. Yea, dhamma, haitupabawat, desang dhamma, tatagata, ahu. Those dhammas that arise based on the cause, the Buddha has taught the cause of those dhammas.

14
00:02:21,000 --> 00:02:29,000
And Saripud realized nibhana as a result of that. He became a sutapana, opatissa, and mogalana as well.

15
00:02:29,000 --> 00:02:40,000
These types of people are very rare, and the theory is that they were much more common in the Buddha's time, but still very rare in the Buddha's time.

16
00:02:40,000 --> 00:02:46,000
Another type of person you have to explain in detail, so you have to say, what do you mean by that?

17
00:02:46,000 --> 00:02:54,000
Whatever dhammas arise based on the cause, the Buddha has taught the cause of those dhammas.

18
00:02:54,000 --> 00:03:03,000
Well, it means suffering, everything all suffering has a cause. And the Buddha has taught the cause of suffering, which is craving.

19
00:03:03,000 --> 00:03:09,000
When there is craving, then there will be, so you explain the four noble truths.

20
00:03:09,000 --> 00:03:21,000
And maybe you have to explain in some detail, like maybe you have to teach the dhamma, takapawatissa, like how gondanya became a sutapana, was by listening to the dhamma, takapawatissa.

21
00:03:21,000 --> 00:03:28,000
So, this type of person, just by listening to the dhamma, they can become enlightened as a result of it.

22
00:03:28,000 --> 00:03:37,000
Or, yes, as another example, just listening to the Buddha's teaching once, he became a sutapana, listening to the same teaching again, he became an aran.

23
00:03:37,000 --> 00:03:48,000
A third type of person has to be trained, not only given the detailed discourse, but has to be trained in it, and has to be guided through it.

24
00:03:48,000 --> 00:03:52,000
This type of person is not enough to teach them even the sutapatana sutapana.

25
00:03:52,000 --> 00:03:57,000
But then you have to say, okay, come and do a course in sutapatana, and then guide them through it.

26
00:03:57,000 --> 00:04:01,000
Okay, then remember to be mindful, remember to be mindful, okay, now be mindful of this.

27
00:04:01,000 --> 00:04:08,000
Are you mindful of this, and keeping them on track, and guiding them through it to be able to understand, oh, this is the sutapatana.

28
00:04:08,000 --> 00:04:13,000
And this is the truth, and this is what happens in realizations.

29
00:04:13,000 --> 00:04:22,000
And a fourth type of person can't be trained, no matter how much you help them and give to them, they don't gain anything out of it,

30
00:04:22,000 --> 00:04:27,000
or they can only just get the meaning of it, the meaning of the words.

31
00:04:27,000 --> 00:04:36,000
So, because of that, the tepitika is not enough, the tepitika is enough, this is the teravada opinion.

32
00:04:36,000 --> 00:04:39,000
The tepitika is not enough for everyone.

33
00:04:39,000 --> 00:04:44,000
You need some explanation of the tepitika, so it's not wrong that people are explaining it.

34
00:04:44,000 --> 00:04:49,000
The problem, of course, is what Andy said, is that one people explain it differently.

35
00:04:49,000 --> 00:04:52,000
Two, they cling to their explanation as being correct.

36
00:04:52,000 --> 00:04:58,000
And there is a standard explanation that teravada tradition is called the commentaries.

37
00:04:58,000 --> 00:05:04,000
There's a huge commentary literature that has a very specific interpretation of the Buddhist teaching.

38
00:05:04,000 --> 00:05:11,000
Now, it doesn't say this is the only interpretation, and any other interpretation is heretic, and is wrong.

39
00:05:11,000 --> 00:05:17,000
In fact, it even talks about different interpretations, and it gives the opinion, this interpretation is not correct,

40
00:05:17,000 --> 00:05:20,000
because it's illogical or because it goes against the sutta.

41
00:05:20,000 --> 00:05:23,000
But it's still just an interpretation.

42
00:05:23,000 --> 00:05:30,000
The problem is when people say, the commentaries are just interpretation, throw them out.

43
00:05:30,000 --> 00:05:33,000
Practice the tepitana sutta in this way, this way, this way, and this way.

44
00:05:33,000 --> 00:05:35,000
It's very clear how the Buddha said.

45
00:05:35,000 --> 00:05:37,000
The Buddha said this, which means this, which means this.

46
00:05:37,000 --> 00:05:42,000
And he, politically, what they do is create another commentary.

47
00:05:42,000 --> 00:05:48,000
Well, at the same time, saying that commentary is just an interpretation, and therefore shouldn't be followed.

48
00:05:48,000 --> 00:05:54,000
Unless you can become enlightened through listening and through understanding clearly the sutta's,

49
00:05:54,000 --> 00:05:59,000
you're going to have to interpret them, and you're going to have to find a way to apply them.

50
00:05:59,000 --> 00:06:05,000
The Buddha said, get chantawagachamitibhajanati, when going among those I am going.

51
00:06:05,000 --> 00:06:10,000
So if you can understand that, and become enlightened, just listening to it,

52
00:06:10,000 --> 00:06:14,000
because you understand then that this hearing, this sound is also just sound,

53
00:06:14,000 --> 00:06:18,000
and you can apply the meditation while you're listening to it, and become enlightened right there,

54
00:06:18,000 --> 00:06:20,000
then there's no need to practice it.

55
00:06:20,000 --> 00:06:25,000
But if you can, then you have to figure, okay, I guess that means I should do some going.

56
00:06:25,000 --> 00:06:33,000
I'm going, and know that I'm going, and then you have to practice that, okay, going, going, or walking, walking.

57
00:06:33,000 --> 00:06:36,000
And maybe you even have to go to the extent that it's not just walking anymore,

58
00:06:36,000 --> 00:06:41,000
but I have to do it one foot at a time, because otherwise I'm not really going to see things as they are in the Buddha.

59
00:06:41,000 --> 00:06:46,000
You know, you have all this Buddha's teaching, and you realize the Buddha was actually talking about seeing the Dottoos, the elements.

60
00:06:46,000 --> 00:06:52,000
So if I'm going to see the elements, I have to do one foot at a time, and wait a minute, even in one step,

61
00:06:52,000 --> 00:06:57,000
there's different elements arising, lifting the foot doesn't feel the same as putting the foot down.

62
00:06:57,000 --> 00:06:59,000
So there's different experiences arising.

63
00:06:59,000 --> 00:07:02,000
So let's lift the foot first, and then put it down.

64
00:07:02,000 --> 00:07:04,000
And then people come along and say, what are you doing?

65
00:07:04,000 --> 00:07:09,000
The Buddha didn't teach six steps walking, and here they're breaking the step up into six parts.

66
00:07:09,000 --> 00:07:11,000
That's not the Buddha's teaching.

67
00:07:11,000 --> 00:07:16,000
But you see, this was necessary, this sort of development was necessary.

68
00:07:16,000 --> 00:07:26,000
So apart from just as Paulini said, the differences of opinion, there is also the cultivation that allows people who are in this third category,

69
00:07:26,000 --> 00:07:31,000
that are unable to become enlightened just by hearing the Buddha's teaching, and need the interpreter,

70
00:07:31,000 --> 00:07:37,000
need the teacher to explain it and to apply it, to give them a technique of seeing this,

71
00:07:37,000 --> 00:07:42,000
which you'll see in the commentaries, you'll see in many stories about how they apply the Buddha's teaching.

72
00:07:42,000 --> 00:07:47,000
And you may even, if we had all of the records and all of the history,

73
00:07:47,000 --> 00:07:51,000
you might even see that in the Buddha's time, they were developing,

74
00:07:51,000 --> 00:07:54,000
they had many, many techniques.

75
00:07:54,000 --> 00:07:57,000
Some of the techniques that we have today, and we see in the Risudimaga,

76
00:07:57,000 --> 00:07:59,000
might have even been practiced in the Buddha's time.

77
00:07:59,000 --> 00:08:02,000
There may have been six parts to the walking step in the Buddha's time,

78
00:08:02,000 --> 00:08:05,000
whether there were or not.

79
00:08:05,000 --> 00:08:18,000
The evolution of meditation techniques as a entity came about not because people were discontent with the Buddha's teaching,

80
00:08:18,000 --> 00:08:26,000
but because people can't understand it, and people can't put it into practice without a regimen and a technique.

81
00:08:26,000 --> 00:08:42,000
And so it's at best, if you're to give it the benefit of the doubt, interpretation at its best is just the extrapolation for the purpose of putting into practice by people,

82
00:08:42,000 --> 00:08:51,000
especially who aren't able to, who don't have very strong faculties and thus aren't able to immediately grasp it.

83
00:08:51,000 --> 00:09:04,000
Maybe the meditation techniques that were known before the Buddha started to teach his kind of meditation.

84
00:09:04,000 --> 00:09:17,000
And our practice, even today, still, like I'm thinking of yoga and their breath, meditation,

85
00:09:17,000 --> 00:09:40,000
and things like that might still be used to come together and make the picture more diffuse,

86
00:09:40,000 --> 00:09:45,000
allowing to see what sorts of practices they had they practiced in the Buddha's time,

87
00:09:45,000 --> 00:09:52,000
and also why the Buddha taught and why breath was so easily.

88
00:09:52,000 --> 00:09:55,000
Because why was breath so prominent in the Buddha's teaching?

89
00:09:55,000 --> 00:10:22,000
Because that's what they were practicing at the time.

