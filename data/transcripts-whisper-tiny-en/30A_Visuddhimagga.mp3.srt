1
00:00:00,000 --> 00:00:08,800
We haven't finished the first aggregate yet, so we are in the middle of the aggregate of

2
00:00:08,800 --> 00:00:12,000
matter, page 497.

3
00:00:12,000 --> 00:00:22,800
We begin with paragraph 61, bodily intimations, verbal intimation, and so on.

4
00:00:22,800 --> 00:00:31,600
And as we said, there is a long footnote and very difficult to understand, and also some

5
00:00:31,600 --> 00:00:44,300
translations are not correct, so bodily intimation is the most confirmation and the alteration

6
00:00:44,300 --> 00:00:50,800
deformation in the consciousness-originated air element that causes the occurrence of movement

7
00:00:50,800 --> 00:00:57,300
forward, et cetera, which mode and alteration are a condition for this stiffening, upholding

8
00:00:57,300 --> 00:01:00,900
and moving of the coldness and material body.

9
00:01:00,900 --> 00:01:07,600
So what is bodily intimation?

10
00:01:07,600 --> 00:01:17,000
After reading this definition, you don't know what it is, so I have said it.

11
00:01:17,000 --> 00:01:29,000
We let other people know our intention by, say, bodily gesture.

12
00:01:29,000 --> 00:01:37,300
Now, that bodily gesture is what we call bodily intimation, but here it is couched in the

13
00:01:37,300 --> 00:01:40,200
language of the books.

14
00:01:40,200 --> 00:01:51,000
And now, bodily intimation is one of the 28 material properties, but bodily intimation

15
00:01:51,000 --> 00:02:00,000
is not air element, because if air element is bodily intimation, then wherever there is

16
00:02:00,000 --> 00:02:06,100
air element, there would be intimation, and there is not.

17
00:02:06,100 --> 00:02:11,000
So air element cannot be bodily intimation.

18
00:02:11,000 --> 00:02:25,600
Then, it is, take the bone, matter bodily intimation, there are other material properties,

19
00:02:25,600 --> 00:02:34,000
too, and they are not intimation, they do not help us to know the intention of people.

20
00:02:34,000 --> 00:02:38,400
So, take the bone, matter is also not bodily intimation.

21
00:02:38,400 --> 00:02:46,700
Then, it is movement, bodily intimation, if it is, then there will be intimation in the movement

22
00:02:46,700 --> 00:02:52,700
of trees, the movement of an animate team, but there is no intimation.

23
00:02:52,700 --> 00:03:03,800
So, bodily intimation is a particular mode of air element, which is caused by data.

24
00:03:03,800 --> 00:03:15,960
Data, which causes going forward, going back and so on.

25
00:03:15,960 --> 00:03:24,400
And this mode is a condition for, there to be a stiffening or upholding of the limbs of

26
00:03:24,400 --> 00:03:26,000
the body and so on.

27
00:03:26,000 --> 00:03:32,000
So, that particular mode is, what is meant by bodily intimation?

28
00:03:32,000 --> 00:03:37,680
So, we cannot pick up bodily intimation and show you this body, the intimation, because

29
00:03:37,680 --> 00:03:46,800
it is just a particular mode of the air element, because here, air element is predominant,

30
00:03:46,800 --> 00:03:50,980
because where there is movement, there is this air element, movement is caused by

31
00:03:50,980 --> 00:03:51,980
air element.

32
00:03:51,980 --> 00:03:55,800
And this air element is caused by data, here.

33
00:03:55,800 --> 00:04:05,320
So, bodily intimation means just a particular mode of the air element, which is caused

34
00:04:05,320 --> 00:04:09,000
by data.

35
00:04:09,000 --> 00:04:16,920
And that data can cause the rising of moving forward, moving going forward, going backward

36
00:04:16,920 --> 00:04:26,000
and so on, the stretching, bending and others.

37
00:04:26,000 --> 00:04:35,640
But there is not intimation in all movements, the only movements of the beings, especially

38
00:04:35,640 --> 00:04:37,480
of the human beings.

39
00:04:37,480 --> 00:04:44,640
Now, if we want to let the other people know that we want him to come and we use a kind

40
00:04:44,640 --> 00:04:51,040
of gesture, in this country, we use this case that we call people.

41
00:04:51,040 --> 00:04:56,400
So, when somebody sees this case, he knows that or he wants me to come.

42
00:04:56,400 --> 00:05:14,480
So, that particular mode in the movement of the hand is what we mean by bodily intimation.

43
00:05:14,480 --> 00:05:30,560
And verbal intimation means a particular mode of air element, there is air element, but

44
00:05:30,560 --> 00:05:38,640
here it is third element, a particular mode of earth element, which is caused by data,

45
00:05:38,640 --> 00:05:41,120
which causes speech.

46
00:05:41,120 --> 00:05:50,040
So, if we want to say something, then there is the theta, or the intention.

47
00:05:50,040 --> 00:06:04,080
And that particular causes the earth element in the place where void is made, see, in

48
00:06:04,080 --> 00:06:13,840
the vocal cords, in the book, it is not mentioned, vocal cords are not used, but let

49
00:06:13,840 --> 00:06:15,880
us say, see, vocal cords.

50
00:06:15,880 --> 00:06:24,880
So, in these vocal cords, there is metal caused by kama, so there is kama born with a

51
00:06:24,880 --> 00:06:30,200
in the vocal cords, along with other kinds of material properties.

52
00:06:30,200 --> 00:06:41,240
And when we want to say something, our mind produces the earth element, many particles

53
00:06:41,240 --> 00:06:49,960
of earth element, and these come together, the earth element caused by cetera and the

54
00:06:49,960 --> 00:06:57,880
material properties in these vocal cords, so they come together, so there is something

55
00:06:57,880 --> 00:07:06,400
like friction, and that friction causes the voice to arise, so there is the voice.

56
00:07:06,400 --> 00:07:15,520
So, here also, verbal indomision is not just the voice, not just earth element, not just

57
00:07:15,520 --> 00:07:27,000
kama born matter, but if particular mode in the earth element, which causes

58
00:07:27,000 --> 00:07:38,480
the voice of speech, and through speech, we understand what the other person intends

59
00:07:38,480 --> 00:07:41,040
or what the other person wants us to know.

60
00:07:41,040 --> 00:07:46,120
So, you hear my voice, and it is not just what these are words, and through these words,

61
00:07:46,120 --> 00:07:48,760
you understand the meaning.

62
00:07:48,760 --> 00:07:58,640
And so, for convenience sake, we can see the articulate words are what we call verbal

63
00:07:58,640 --> 00:08:06,480
intomision, but actually it is a particular mode in the earth element caused by the

64
00:08:06,480 --> 00:08:13,320
cetera, coming together with the gama born matter in the vocal cords, and then we call

65
00:08:13,320 --> 00:08:22,880
verbal intomision, so for our convenience, we can just say the speech to be verbal

66
00:08:22,880 --> 00:08:29,600
intomision, so through speech, through words, we make other people understand, and

67
00:08:29,600 --> 00:08:36,080
in order to understand the other people's intention or the meaning of the words, we need

68
00:08:36,080 --> 00:08:38,040
two conditions.

69
00:08:38,040 --> 00:08:46,800
First, we must hear the word, and second, we must have known the meaning of the word before.

70
00:08:46,800 --> 00:08:53,000
Only when these two conditions are met, there will be understanding, that is why when

71
00:08:53,000 --> 00:08:58,720
you speak quickly, I don't understand you because I don't hear the words distinctly, and

72
00:08:58,720 --> 00:09:04,920
sometimes you may use a word which I do not know the meaning of, then I will not understand

73
00:09:04,920 --> 00:09:05,920
you.

74
00:09:05,920 --> 00:09:14,400
There are two conditions, one is they must come into the voice or the words, must come

75
00:09:14,400 --> 00:09:19,520
into the avenue of your eye, or they must strike your eye, and you must hear clearly

76
00:09:19,520 --> 00:09:24,160
that is one one condition, and the other is you must have known the meaning of the word

77
00:09:24,160 --> 00:09:29,120
before, then you understand, otherwise you do not understand.

78
00:09:29,120 --> 00:09:39,120
You mean the ear, don't you not be eyes, you shouldn't be eyes, I'm sorry, not be eyes.

79
00:09:39,120 --> 00:09:42,280
Thank you.

80
00:09:42,280 --> 00:09:51,920
If I say what in body, then you may not understand, although you hear the word distinctly,

81
00:09:51,920 --> 00:09:58,120
but you have not known this word or the meaning of this word, so you do not understand.

82
00:09:58,120 --> 00:10:12,720
So, just by being present, the sound or voice cannot make us understand, and the word

83
00:10:12,720 --> 00:10:20,280
translated here on page 49 in the footnotes, the second part is not correct, although

84
00:10:20,280 --> 00:10:25,920
the first two lines are correct, sounds that have entered no objective field, do not

85
00:10:25,920 --> 00:10:32,840
awaken any kind of meaning, that is, sounds which we do not hear, will not make us understand

86
00:10:32,840 --> 00:10:44,320
any meaning at all, and then the two lines, I mean, the third and the fourth lines are

87
00:10:44,320 --> 00:10:54,040
not correct, because the animal you misunderstand, one word in the body or the original,

88
00:10:54,040 --> 00:11:08,540
and that word is sub-dah, S-A-T-T-A, long age, the second age is long age, now, the word

89
00:11:08,540 --> 00:11:19,000
S-A-T-T-A, with short A means A being, like bodhisattva, right, sub-dah means A being, but

90
00:11:19,000 --> 00:11:30,480
S-A-T-A, with a long, second age long, means the state of being, or the state of being present,

91
00:11:30,480 --> 00:11:37,760
being existence, being present, and that is the word used in this verse, not the word

92
00:11:37,760 --> 00:11:46,920
S-A-T-A, so that means, not just by being present, will they make us understand, it

93
00:11:46,920 --> 00:11:52,840
is, will these sounds or words make us understand, that means, we may hear the words, but

94
00:11:52,840 --> 00:12:00,400
if we do not understand the meaning of the word, I mean, if we do not, if we have not

95
00:12:00,400 --> 00:12:08,960
learned the meaning of the word before, then we will not understand, so this is verbal

96
00:12:08,960 --> 00:12:19,240
information and others are not so difficult to understand, and these two, bodily

97
00:12:19,240 --> 00:12:26,040
information and verbal information are said to be caused by take-back.

98
00:12:26,040 --> 00:12:33,800
So, there's last two lines, and it would be something I could also words merely recognize

99
00:12:33,800 --> 00:12:43,960
or something like that, if their meaning is not known beforehand, simply by their presence

100
00:12:43,960 --> 00:12:59,800
they cannot make us understand, something like that, let me see, they will not show any

101
00:12:59,800 --> 00:13:18,920
meaning, just by their presence, if their meaning is not known beforehand, and the next

102
00:13:18,920 --> 00:13:27,520
material property is speech element or just speech or a graph 63, and it has a characteristic

103
00:13:27,520 --> 00:13:34,920
of the limiting matter, the limiting matter really means group of matter, not just one

104
00:13:34,920 --> 00:13:40,480
material property, but a group of material property, because material properties arise

105
00:13:40,480 --> 00:13:46,840
in groups, say, eight together, nine together, ten together and so on, and this speech

106
00:13:46,840 --> 00:13:57,720
element is said to be between these groups, say, between one group and the other, and there

107
00:13:57,720 --> 00:14:04,960
is what we call space, that is the limiting, this is one group, this is another group,

108
00:14:04,960 --> 00:14:13,000
this is another group and so on, so that is called space, so the limiting matter means

109
00:14:13,000 --> 00:14:25,680
not one material property from another, but one group of materiality from another, and

110
00:14:25,680 --> 00:14:37,520
then comes three lightness of matter, malleability of matter and wilderness of matter,

111
00:14:37,520 --> 00:14:46,800
and actually some quality of the material properties, say, lightness, malleability and

112
00:14:46,800 --> 00:14:54,480
wilderness, and they are explained in paragraph 65, say, lightness of matter is alteration

113
00:14:54,480 --> 00:15:02,480
of matter, such as any light state and material instances as in one who is healthy, so

114
00:15:02,480 --> 00:15:09,920
when you're healthy, your body is something like light, a child, and so there is lightness

115
00:15:09,920 --> 00:15:18,120
of matter in the body, and the second malleability of matter is compared to a well-founded

116
00:15:18,120 --> 00:15:25,400
height, say, when you want to make leather and you soak the height in water, I mean in

117
00:15:25,400 --> 00:15:33,040
soil water and then you pound it again and again, so that it becomes soft and malleable

118
00:15:33,040 --> 00:15:39,600
to whatever you want to do, so something like that is called malleability in matter, and

119
00:15:39,600 --> 00:15:50,360
the third one is compared to well-refined gold, so you want to make some elements out of

120
00:15:50,360 --> 00:15:59,880
gold, then you first you refine the gold, so when gold is well-refined, that is, without

121
00:15:59,880 --> 00:16:11,080
any other part of all that, mixture or something, then it becomes easy to be made into any

122
00:16:11,080 --> 00:16:20,840
element you want, so something like that, existing in the body or in the matter is called

123
00:16:20,840 --> 00:16:27,800
weirdiness of matter, and these three it is set always arise together, so at the beginning

124
00:16:27,800 --> 00:16:34,400
of the paragraph 65 you see these three however are not found apart from each other, so

125
00:16:34,400 --> 00:16:42,840
when there is one there is the other two, the other two two, so these are three actually

126
00:16:42,840 --> 00:16:51,080
qualities of material property, and then next we have growth of matter and continuity

127
00:16:51,080 --> 00:17:01,360
of matter, now you're familiar with the three phases of existence, right, arising continuation

128
00:17:01,360 --> 00:17:11,400
and dead or disappear, now the first phase arising is described here as two growth of

129
00:17:11,400 --> 00:17:17,440
matter and continuity of matter, so it is just the it is called rebirth here, just the

130
00:17:17,440 --> 00:17:23,440
arising of matter, this arising of matter is here divided into two, one is growth of

131
00:17:23,440 --> 00:17:31,960
matter and the other is continuity of matter, growth of matter is first arising, and continuity

132
00:17:31,960 --> 00:17:41,720
of matter is, as I've said, continuous, so the continuity of matter has the characteristic

133
00:17:41,720 --> 00:17:54,360
of occurrence, but it is not occurrence, it is continuing, if there's such a word as continuous

134
00:17:54,360 --> 00:18:02,760
or we can just say continuing, that's the characteristic of continuing, continuing to exist,

135
00:18:02,760 --> 00:18:14,560
but just occurrence, the body word can mean to occur as well as to continue after arising,

136
00:18:14,560 --> 00:18:23,840
it's function is to anchor, that means to join one with the other, both of these terms

137
00:18:23,840 --> 00:18:34,440
both of these are terms for matter at its birth, so the birth or the arising of matter

138
00:18:34,440 --> 00:18:46,160
is here called growth of matter and continuity of matter, and just one birth or arising

139
00:18:46,160 --> 00:18:54,880
is described in with two names here because it is owing to difference of mode and there

140
00:18:54,880 --> 00:18:58,840
is a little difference between the growth of matter and continuity of matter and also

141
00:18:58,840 --> 00:19:04,600
according to different persons susceptibility to instruction, that means there are some

142
00:19:04,600 --> 00:19:13,920
beings who will understand better when one birth is described as true growth of matter

143
00:19:13,920 --> 00:19:26,200
and continuity of matter, it plays an important role, at least in explaining why Buddha

144
00:19:26,200 --> 00:19:32,240
said, why Buddha taught in this way or why Buddha taught in that way and that is Buddha

145
00:19:32,240 --> 00:19:45,360
always have in mind the susceptibility of listeners, that means if I use this word, will

146
00:19:45,360 --> 00:19:50,680
they understand, then if they are not going to understand then it may use another word,

147
00:19:50,680 --> 00:19:55,760
that is why there are many, many kinds of teachings in the teachings of the Buddha, like

148
00:19:55,760 --> 00:20:05,480
sometimes he taught by we have five aggregates, sometimes he taught by we have 12 basis,

149
00:20:05,480 --> 00:20:11,560
sometimes by 18 elements, but actually they are all the same, different names given to

150
00:20:11,560 --> 00:20:19,320
one and the same thing, so sometimes we may be familiar with one word, but not the other,

151
00:20:19,320 --> 00:20:25,360
so when if familiar what is spoken to as we readily understand, if the word is not familiar

152
00:20:25,360 --> 00:20:30,440
with that and we don't understand, so if we don't understand we cannot penetrate into

153
00:20:30,440 --> 00:20:38,840
the nature of things, so Buddha always exercise this, he taught quite the people, sharpness,

154
00:20:38,840 --> 00:20:51,480
if they were best one or slower, and they are likes and dislikes, right, and this is one ability

155
00:20:51,480 --> 00:21:02,280
not shared by his disciples, even Saribhura has no such ability to enter into the minds

156
00:21:02,280 --> 00:21:11,000
of listeners and funny or what will be suitable for them, that is why Saribhura gave

157
00:21:11,000 --> 00:21:27,840
a wrong, but he didn't subject to his young students, so here, Buddha wanted to teach,

158
00:21:27,840 --> 00:21:36,680
so that it is suitable for their susceptibility, so in teaching, in the summary, that

159
00:21:36,680 --> 00:21:42,680
is in the Abirama, in the Dhamma Sangani is given as cruel and continuity, but since there

160
00:21:42,680 --> 00:21:49,760
is here no difference in meaning, that means difference in reality, consequently in description,

161
00:21:49,760 --> 00:21:55,120
that means a detailed description of these words, the setting up of sense basis is the

162
00:21:55,120 --> 00:21:59,560
group of matter and the group of matter is the continuity of matter, so this is said

163
00:21:59,560 --> 00:22:06,660
in Dhamma Sangani, and in the commentary, it is Genesis that is

164
00:22:06,660 --> 00:22:11,600
called setting up increase, that is called growth, or currents that is called continuity,

165
00:22:11,600 --> 00:22:19,320
so here, three times are given, setting up growth and continuity, and it is similarly

166
00:22:19,320 --> 00:22:24,440
is given here, Genesis is setting up, it is setting up, it is like the time when water comes

167
00:22:24,440 --> 00:22:30,520
up in the whole jug in the river bank, so when the water comes up, that is Genesis, increase

168
00:22:30,520 --> 00:22:37,520
as growth is like the time when it feels the whole, and or currents or continuance as continuity

169
00:22:37,520 --> 00:22:46,660
is like the time when it overflows, so these are just, it is similarly to understand

170
00:22:46,660 --> 00:22:56,280
the two phases of one arising or one birth of material property, growth of matter and continuity

171
00:22:56,280 --> 00:23:06,480
of matter, so according to this, there will be four kinds of, four kinds of matter which

172
00:23:06,480 --> 00:23:18,380
are called character, characteristic matter, because actually they are different, different

173
00:23:18,380 --> 00:23:29,120
uses of matter, say the arising, the continuance, and disappear, so these two are for

174
00:23:29,120 --> 00:23:42,280
arising, and then the next is aging for graph 68, the characteristic of maturing,

175
00:23:42,280 --> 00:23:49,480
the riboning material instances, so this is getting old, this is the continuing phase

176
00:23:49,480 --> 00:23:57,000
of, of existence, the arising, continuing, and disappearing, this is, so it correspond

177
00:23:57,000 --> 00:24:06,800
to the second phase, and here aging, this is said with reference to the kind of aging

178
00:24:06,800 --> 00:24:13,600
that is evident through seeing alteration in teeth, etc., as they are brokenness, and so on.

179
00:24:13,600 --> 00:24:24,920
Now there are, here are given three kinds of aging, evident aging, hidden aging, and

180
00:24:24,920 --> 00:24:32,760
in seventh aging, evident aging is, well, aging which is evident, say when we see someone

181
00:24:32,760 --> 00:24:44,280
with a broken teeth, or someone with eye glasses maybe, then we know that that person

182
00:24:44,280 --> 00:24:53,440
has aged, that person is old, and then for the immaterial states, there is no, no such

183
00:24:53,440 --> 00:25:04,520
evident aging, we cannot see the brokenness of tater and tater figures, so here for them

184
00:25:04,520 --> 00:25:20,640
it is called hidden aging, which cannot be seen, which cannot be seen by the eyes, but

185
00:25:20,640 --> 00:25:29,280
which is seen through wisdom or mind, and in earth water rocks, the moon, the sun, etc.,

186
00:25:29,280 --> 00:25:36,280
the aging is called incessant aging, incessant aging means, it is also a hidden aging,

187
00:25:36,280 --> 00:25:45,200
because you know this book, this book looks, looks new for maybe one month, two months,

188
00:25:45,200 --> 00:25:55,480
or even a year if you don't use it much, but there is aging going on, but we do not see

189
00:25:55,480 --> 00:26:05,080
it getting, getting all of that in aging, so aging in earth water and so on is also a hidden

190
00:26:05,080 --> 00:26:14,320
aging, but here it is called incessant aging, aging without visible difference between

191
00:26:14,320 --> 00:26:28,080
one, one state and another, so here by aging is meant the aging of material properties,

192
00:26:28,080 --> 00:26:35,320
not of immaterial states, then the last one is impermanence of matter, that means

193
00:26:35,320 --> 00:26:44,640
death of matter, disappearing of matter, so it is a characteristic of complete breaking up,

194
00:26:44,640 --> 00:26:53,040
oh, the last one, physical neutral meant, that is the characteristic of nutritious essence,

195
00:26:53,040 --> 00:27:00,720
now its approximate cause is a physical basis that must be fetched with physical food,

196
00:27:00,720 --> 00:27:13,320
here also the translasiness of the look, inaccurate, the pali word is kabalikara, so kabalam

197
00:27:13,320 --> 00:27:22,760
means immersal, so kabalikara means making immersal, that means before you eat you make

198
00:27:22,760 --> 00:27:30,960
immersal, if you eat with hands, so you make immersal and then you put it in the modern

199
00:27:30,960 --> 00:27:43,880
eating, so food is called impali kabalikara, so food which is eaten after making it into

200
00:27:43,880 --> 00:27:58,360
immersal, something like that, so its approximate cause is a physical basis, no, yeah,

201
00:27:58,360 --> 00:28:11,400
basis is not a good word, the pali word is what to, what you can mean a base, like the

202
00:28:11,400 --> 00:28:17,840
eye base, ear base and so on, but what you can also mean is thin, just something, so

203
00:28:17,840 --> 00:28:26,720
here physical basis means just a material thin, which must be swallowed after being made

204
00:28:26,720 --> 00:28:37,600
in more cell, but this is what used in impali, simply it is just what you eat, the food,

205
00:28:37,600 --> 00:28:45,960
the food is not the food itself is not physical, neutral meant here, but some something

206
00:28:45,960 --> 00:29:01,080
in the food, something which helps us, I mean hunger and so on, so that, that a

207
00:29:01,080 --> 00:29:09,720
nutritive essence is what is meant here as physical neutral meant, not the food itself,

208
00:29:09,720 --> 00:29:14,600
but its approximate cause is the food we eat, because we get that neutral meant from the

209
00:29:14,600 --> 00:29:24,800
food we eat, so these are all together 28 material properties, if you call them you will

210
00:29:24,800 --> 00:29:37,320
get 28, full primary elements and 24 dependent ones, so all together 28, so these firstly

211
00:29:37,320 --> 00:29:42,880
are the material instances that have been handed down in the texts, but in the commentary

212
00:29:42,880 --> 00:29:49,760
there, commentary others have been added as follows, so in the commentaries, others are

213
00:29:49,760 --> 00:29:57,200
added, meta is power, power is meta, recreation is meta, but is meta, sickness is meta

214
00:29:57,200 --> 00:30:06,360
and so on, and in the opinion of some meta is total, now you know midha, I mean, slot

215
00:30:06,360 --> 00:30:16,680
and toggle, slot and toggle are two mental states or mental factors, they are included in

216
00:30:16,680 --> 00:30:25,640
the formation aggregate, but there are some teachers who took toggle to be meta, and so

217
00:30:25,640 --> 00:30:31,920
it is mentioned here, in the first place meta is toggle is rejected is not existent by

218
00:30:31,920 --> 00:30:43,240
the words, surely the artisage enlightened, there are no hindrances in V, now the common

219
00:30:43,240 --> 00:30:53,560
opinion is that toggle is not meta, but there are some teachers belonging to another

220
00:30:53,560 --> 00:31:03,240
school, among the people of other school, they took toggle to be meta, and then that

221
00:31:03,240 --> 00:31:17,800
is rejected as non existent, there can be no toggle as meta, and the proof they show is

222
00:31:17,800 --> 00:31:33,040
both as what surely the artisage enlightened, there are no hindrances in V, now this two

223
00:31:33,040 --> 00:31:45,960
lines say that there are no hindrances in the mind of the Buddha, and toggle is included

224
00:31:45,960 --> 00:32:03,000
in the hindrances, but in other, especially in Abhirama, it is said that material properties

225
00:32:03,000 --> 00:32:12,520
cannot be abandoned or cannot be destroyed, say, only the mental defilements can be

226
00:32:12,520 --> 00:32:22,320
destroyed, say, when it becomes enlightened, he eradicates mental defilements, so only

227
00:32:22,320 --> 00:32:28,320
the mental defilements can be eradicated, and not the material properties, but here I

228
00:32:28,320 --> 00:32:42,640
just said that Buddha had no defilements, I mean no hindrances, and topper is included in hindrances,

229
00:32:42,640 --> 00:32:51,440
so topper can be eradicated, if it can be eradicated, it is not rhuba, it is not material

230
00:32:51,440 --> 00:33:00,320
property, and the other, the rest, meta as sickness is included by aging, and by impermanence,

231
00:33:00,320 --> 00:33:06,880
meta as birth, by growth, and continuity, meta as procreation by water elements, because when

232
00:33:06,880 --> 00:33:12,360
there is water elements, something grows, and meta as power by the air element, so taken

233
00:33:12,360 --> 00:33:17,200
separately, not even one of these exists, this was the agreement reached, so this was

234
00:33:17,200 --> 00:33:28,160
the agreement of majority of teachers, so this derived meta of 24 sorts, and the opposite

235
00:33:28,160 --> 00:33:34,760
meta of the primary elements, which is of full sorts, together among 28 sorts, neither

236
00:33:34,760 --> 00:33:41,840
more nor less, so these are the 28 material properties, and then they are described as

237
00:33:41,840 --> 00:33:51,840
of one kind, two kinds, and so on, so the first is what, is of one kind, is not root

238
00:33:51,840 --> 00:33:57,320
cause, because they have no root cause, and they are dissociated from root cause, and

239
00:33:57,320 --> 00:34:01,600
they are with conditions, they are mundane, and they are subject to kangas, according

240
00:34:01,600 --> 00:34:10,160
to this, there is only one kind of material property, like although we are different people,

241
00:34:10,160 --> 00:34:15,920
as human being, we are one, something like that, so although they are at 28 material

242
00:34:15,920 --> 00:34:25,000
properties, as having no root cause, or as having conditions, as being mundane, then there

243
00:34:25,000 --> 00:34:31,720
is only one root, one material property, and then they can be two, it is of two kinds

244
00:34:31,720 --> 00:34:38,960
as internal and external, growth and subtle, far and near, reduced and unproduced, produced

245
00:34:38,960 --> 00:34:49,000
by full causes, gamma, consciousness, temperature and new treatment, produced by full causes

246
00:34:49,000 --> 00:34:54,600
and unproduced, sensitive meta and sensitive meta, faculty and non-fagaldi, going to

247
00:34:54,600 --> 00:35:01,240
a not clung to, and so on, clung to means caused by comma, and not clung to mean, not

248
00:35:01,240 --> 00:35:13,560
caused by comma, and then the detailed explanation of the internal, external and so on,

249
00:35:13,560 --> 00:35:23,880
I mean, they are not difficult to understand, if you have to list in ready at hand, so

250
00:35:23,880 --> 00:35:31,200
you look at the list and then read this paragraph, and so, and then you will understand

251
00:35:31,200 --> 00:35:41,960
easily, at the end of that paragraph, what we shall later describe as gamma born is

252
00:35:41,960 --> 00:35:48,520
clung to, because that is clung to, that is acquired by comma, I think instead of the

253
00:35:48,520 --> 00:36:04,120
acquired which will say, caused, caused by comma, literally clung to by comma, and clung

254
00:36:04,120 --> 00:36:11,600
to by, or crossed by comma means taken by comma as its project, so that simply means

255
00:36:11,600 --> 00:36:24,000
caused by comma, and then meta of three kinds, according to the visible triads, they

256
00:36:24,000 --> 00:36:30,480
come up on triads, etcetera, and as regards to growth, the visible datum is visible

257
00:36:30,480 --> 00:36:39,720
with impact, the rest are visible, invisible with impact, all these subtle kinds are

258
00:36:39,720 --> 00:36:45,640
invisible without impact, so there are three kinds of material properties, visible with

259
00:36:45,640 --> 00:36:55,600
impact, invisible with impact, invisible without impact, so visible with impact is only

260
00:36:55,600 --> 00:37:07,840
one, which is visible datum, that is visible object, and the rest are invisible with

261
00:37:07,840 --> 00:37:17,480
impact, and all these subtle kinds are invisible without impact, subtle and gross are

262
00:37:17,480 --> 00:37:22,800
explained in the above paragraph, paragraph, simply three, then according to comma, bon

263
00:37:22,800 --> 00:37:27,400
triads, etcetera, however, that born from comma is comma born and so on, they are not difficult

264
00:37:27,400 --> 00:37:33,240
in a sense, so comma born, not comma born, neither comma born nor comma born, and the same

265
00:37:33,240 --> 00:37:48,920
with consciousness, temperature, and neutron, and neutron, and temperature, and again it

266
00:37:48,920 --> 00:38:00,880
is of four kinds, paragraph 76, as seen, etcetera, as concrete meta, etcetera, and as

267
00:38:00,880 --> 00:38:09,160
the physical business, they take drugs and so on, so visible datum is seen because it is

268
00:38:09,160 --> 00:38:15,280
the objective field of scene, so it is the object of scene, so visible datum is seen

269
00:38:15,280 --> 00:38:21,240
a call scene, and then hurt, what is that sound, and then the other ones, the three that

270
00:38:21,240 --> 00:38:28,240
is to see odors, flavors, and tangible datum are sensed, literally contacted, because

271
00:38:28,240 --> 00:38:33,360
they are objective fields of about these that take contiguous or objective fields, the

272
00:38:33,360 --> 00:38:38,200
rest are cognised because they are the objective field of consciousness only, so firstly

273
00:38:38,200 --> 00:38:49,280
it is of four kinds, according to the scene, etcetera, and then here, in the footnote, there

274
00:38:49,280 --> 00:39:06,960
is a discussion of whether water elements can be touched, it is not cool, apprehended

275
00:39:06,960 --> 00:39:20,280
by touching, do you think so, and that is water element, suddenly it is apprehended,

276
00:39:20,280 --> 00:39:26,440
but it is not the water element, so the coal is not water element, what is this then,

277
00:39:26,440 --> 00:39:31,920
it is just the fire element, or temperature, for there is a sensation of coal when it

278
00:39:31,920 --> 00:39:37,920
is sluggish, so when there is little temperature, we say it is cold, and when the temperature

279
00:39:37,920 --> 00:39:44,040
rises, we say it is hot, so coal and hot are just the variation in temperature, and so

280
00:39:44,040 --> 00:39:51,120
coal is not water element, there is no quality that is called coal, there is only the

281
00:39:51,120 --> 00:39:56,040
assumption of coalness due to the slightness of the state of it, how is that to be known

282
00:39:56,040 --> 00:40:03,040
because of the unreliability of the sensation of coal, like beyond and not beyond, beyond

283
00:40:03,040 --> 00:40:11,960
and not beyond, it is not a cry-grandering, it means this bank and the other bank, when

284
00:40:11,960 --> 00:40:18,600
we are, on this bank, we call the other bank, but when we cross over with the other

285
00:40:18,600 --> 00:40:24,440
bank, then this bank becomes the other bank, and the other bank becomes this bank, so

286
00:40:24,440 --> 00:40:31,440
here also, when the temperature is low, we call it is cold, and when it is, when the

287
00:40:31,440 --> 00:40:41,400
temperature has risen, we call it is hot, so hot and coal are relative terms.

288
00:40:41,400 --> 00:40:45,440
For in hot weather, while those who stand in the sun and go into the shade of the sensation

289
00:40:45,440 --> 00:40:50,400
of coal yet, those who go to the same place from an underground cave, of the sensation

290
00:40:50,400 --> 00:40:57,080
of heat, and if coalness were the water element, it would be found in this single group

291
00:40:57,080 --> 00:41:04,000
along with heat, but it is not so far, so if coalness is water element, that it must

292
00:41:04,000 --> 00:41:13,000
coexist with heat, but heat and coal do not coexist, so coal is not water element, because

293
00:41:13,000 --> 00:41:20,520
there are four elements everywhere, and if coalness were the water element, it would be

294
00:41:20,520 --> 00:41:24,760
found in this single group along with heat, but it is not so far, that is why it may

295
00:41:24,760 --> 00:41:33,080
be known, actually we should say, it should be known that coalness is not water element,

296
00:41:33,080 --> 00:41:38,760
and that is conclusive for those who agree to the inseparable existence of the primary

297
00:41:38,760 --> 00:41:44,680
elements, and it is conclusive to even for those who do not agree, because it is disproved

298
00:41:44,680 --> 00:41:51,840
by associated existence and so on, that is difficult to understand, and it is conclusive

299
00:41:51,840 --> 00:41:56,600
to for those who say that coalness is the characteristic of the air element for, if coalness

300
00:41:56,600 --> 00:42:00,680
were the air element, coalness would be found in a single group along with heat, that

301
00:42:00,680 --> 00:42:09,240
side is the same argument, and then, but those who hold the opinion that fluidity is the

302
00:42:09,240 --> 00:42:14,560
water element, and that is apprehended by touching should be told, that fluidity is touched

303
00:42:14,560 --> 00:42:20,680
immediately, the venerable ones assumption, in the case with shape, so we think we see the

304
00:42:20,680 --> 00:42:27,920
shape, we look at something and we see the shape, but what we really see is the visible

305
00:42:27,920 --> 00:42:36,040
object here, shape, shape, we do not see, but we think we see, or in other words, we see

306
00:42:36,040 --> 00:42:42,240
the shape in our minds, and not with our eyes, what we see with our eyes is just the

307
00:42:42,240 --> 00:42:50,760
visible, visible data, so three elements coexisting with fluidity together from what constitutes

308
00:42:50,760 --> 00:42:56,360
it tangible, that I succeed in touching this fluidity is a common misconception in the

309
00:42:56,360 --> 00:43:02,880
world, so you put your hand into the water, and you say, it is coal, and then you think

310
00:43:02,880 --> 00:43:11,080
that I can touch coal, as a man who touches elements, and every hand is shaped, then with

311
00:43:11,080 --> 00:43:17,720
his mind, fences, I really have been touching shape, so too fluidity is to be understood,

312
00:43:17,720 --> 00:43:26,960
so you pick up a pencil, and you think, you test the shape of the pencil, but what you

313
00:43:26,960 --> 00:43:33,760
really touch is not the shape of the pencil, but the material properties in the pencil,

314
00:43:33,760 --> 00:43:43,440
so water element cannot be touched, what we touch and think as water element is, and not

315
00:43:43,440 --> 00:43:57,440
actually water element, but it is fire element, it is not coal, now next page here, however,

316
00:43:57,440 --> 00:44:04,160
produce meta is concrete meta, and space element is still emitting meta, and so on, they

317
00:44:04,160 --> 00:44:12,760
are just different names for the one and the same material property, in paragraph 78,

318
00:44:12,760 --> 00:44:19,880
one word is missing in my book, here however, what is called the materiality of the heart

319
00:44:19,880 --> 00:44:27,600
is physical basis, not door, so heart basis, heart basis is basis, but not door, the two

320
00:44:27,600 --> 00:44:36,120
intimations are door, but not physical basis, you say, what not there, you know, please

321
00:44:36,120 --> 00:44:44,520
put not, but not physical basis, two intimations, body decommissioned and rubber intimations

322
00:44:44,520 --> 00:44:58,600
are said to be door, because they are those through which we do come up as come up by

323
00:44:58,600 --> 00:45:05,280
body, come up by speech, so bodyly intimation and rubber intimation are said to be doors

324
00:45:05,280 --> 00:45:15,240
of come up, you may read this in the fifth chapter of the, the manual of the bidhama,

325
00:45:15,240 --> 00:45:21,880
and they are door, but not, not physical basis, not what to, so we should say door, but

326
00:45:21,880 --> 00:45:28,760
not physical basis, sensitive matter is both physical basis and door, like eye sensitivity,

327
00:45:28,760 --> 00:45:35,400
ear sensitivity and so on, the rest are neither physical basis nor door, so it's a four kinds

328
00:45:35,400 --> 00:45:55,760
and the five kinds, ball of one, ball of two and so on, okay, and body end of the paragraph,

329
00:45:55,760 --> 00:46:02,840
at the beginning of the paragraph 18, that is all the rest except matter as characteristic,

330
00:46:02,840 --> 00:46:08,640
but matter as characteristic is called not born of anything, so matter as characteristic

331
00:46:08,640 --> 00:46:22,160
means that the four men mentioned and the previous paragraph, the paragraph 66, growth

332
00:46:22,160 --> 00:46:29,960
of matter, continuity of matter, aging, and impermanence of matter, these four.

